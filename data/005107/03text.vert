<s>
Spitzerův	Spitzerův	k2eAgInSc1d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
či	či	k8xC	či
Space	Space	k1gMnSc1	Space
Infrared	Infrared	k1gMnSc1	Infrared
Telescope	Telescop	k1gInSc5	Telescop
Facility	Facilita	k1gFnPc4	Facilita
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
SIRTF	SIRTF	kA	SIRTF
nebo	nebo	k8xC	nebo
i	i	k9	i
Spitzer	Spitzer	k1gInSc1	Spitzer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
observatoř	observatoř	k1gFnSc1	observatoř
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
infračerveném	infračervený	k2eAgInSc6d1	infračervený
oboru	obor	k1gInSc6	obor
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
infračervený	infračervený	k2eAgInSc4d1	infračervený
teleskop	teleskop	k1gInSc4	teleskop
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vypuštěn	vypustit	k5eAaPmNgInS	vypustit
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
poslední	poslední	k2eAgInSc4d1	poslední
článek	článek	k1gInSc4	článek
projektu	projekt	k1gInSc2	projekt
Velkých	velký	k2eAgFnPc2d1	velká
observatoří	observatoř	k1gFnPc2	observatoř
spadajícího	spadající	k2eAgInSc2d1	spadající
pod	pod	k7c4	pod
americký	americký	k2eAgInSc4d1	americký
Národní	národní	k2eAgInSc4d1	národní
úřad	úřad	k1gInSc4	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
teleskop	teleskop	k1gInSc1	teleskop
<g/>
,	,	kIx,	,
vypuštěný	vypuštěný	k2eAgMnSc1d1	vypuštěný
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
učinil	učinit	k5eAaPmAgInS	učinit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
objevů	objev	k1gInPc2	objev
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
přímé	přímý	k2eAgNnSc1d1	přímé
zachycení	zachycení	k1gNnSc1	zachycení
světla	světlo	k1gNnSc2	světlo
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
HD	HD	kA	HD
209458	[number]	k4	209458
b	b	k?	b
a	a	k8xC	a
TrES-	TrES-	k1gFnSc1	TrES-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
potvrzení	potvrzení	k1gNnSc4	potvrzení
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
galaxie	galaxie	k1gFnSc1	galaxie
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
spirální	spirální	k2eAgFnSc7d1	spirální
galaxií	galaxie	k1gFnSc7	galaxie
s	s	k7c7	s
příčkou	příčka	k1gFnSc7	příčka
nebo	nebo	k8xC	nebo
zmapování	zmapování	k1gNnSc4	zmapování
atmosféry	atmosféra	k1gFnSc2	atmosféra
exoplanety	exoplanet	k1gInPc4	exoplanet
HD	HD	kA	HD
189733	[number]	k4	189733
b.	b.	k?	b.
S	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
fotografická	fotografický	k2eAgFnSc1d1	fotografická
mozaika	mozaika	k1gFnSc1	mozaika
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
samostatných	samostatný	k2eAgInPc2d1	samostatný
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
misi	mise	k1gFnSc4	mise
společně	společně	k6eAd1	společně
řídí	řídit	k5eAaImIp3nS	řídit
Jet	jet	k5eAaImF	jet
Propulsion	Propulsion	k1gInSc4	Propulsion
Laboratory	Laborator	k1gInPc7	Laborator
a	a	k8xC	a
Spitzer	Spitzer	k1gInSc1	Spitzer
Science	Scienec	k1gInSc2	Scienec
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Pasadena	Pasaden	k2eAgFnSc1d1	Pasadena
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
délka	délka	k1gFnSc1	délka
mise	mise	k1gFnSc1	mise
činila	činit	k5eAaImAgFnS	činit
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
prodlužována	prodlužován	k2eAgFnSc1d1	prodlužována
až	až	k9	až
do	do	k7c2	do
stádia	stádium	k1gNnSc2	stádium
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
zásob	zásoba	k1gFnPc2	zásoba
chladicího	chladicí	k2eAgNnSc2d1	chladicí
helia	helium	k1gNnSc2	helium
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
něhož	jenž	k3xRgNnSc2	jenž
není	být	k5eNaImIp3nS	být
nadále	nadále	k6eAd1	nadále
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
spektra	spektrum	k1gNnSc2	spektrum
provádět	provádět	k5eAaImF	provádět
měření	měření	k1gNnSc4	měření
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
konkrétně	konkrétně	k6eAd1	konkrétně
nastala	nastat	k5eAaPmAgFnS	nastat
ke	k	k7c3	k
dni	den	k1gInSc3	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
data	datum	k1gNnSc2	datum
až	až	k9	až
doposud	doposud	k6eAd1	doposud
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Teplá	teplý	k2eAgFnSc1d1	teplá
mise	mise	k1gFnSc1	mise
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
</s>
<s>
Warm	Warm	k1gInSc1	Warm
Mission	Mission	k1gInSc1	Mission
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
dalekohled	dalekohled	k1gInSc1	dalekohled
postupně	postupně	k6eAd1	postupně
zahřál	zahřát	k5eAaPmAgInS	zahřát
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
T	T	kA	T
≈	≈	k?	≈
30	[number]	k4	30
K.	K.	kA	K.
Pozorovací	pozorovací	k2eAgFnSc2d1	pozorovací
možnosti	možnost	k1gFnSc2	možnost
dalekohledu	dalekohled	k1gInSc2	dalekohled
tedy	tedy	k9	tedy
již	již	k6eAd1	již
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
takových	takový	k3xDgInPc2	takový
výkonů	výkon	k1gInPc2	výkon
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
i	i	k8xC	i
přesto	přesto	k8xC	přesto
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
přínosným	přínosný	k2eAgInSc7d1	přínosný
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mise	mise	k1gFnSc1	mise
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
konce	konec	k1gInSc2	konec
září	září	k1gNnSc4	září
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
s	s	k7c7	s
teoretickou	teoretický	k2eAgFnSc7d1	teoretická
možností	možnost	k1gFnSc7	možnost
prodloužit	prodloužit	k5eAaPmF	prodloužit
celou	celý	k2eAgFnSc4d1	celá
misi	mise	k1gFnSc4	mise
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
dle	dle	k7c2	dle
Lymana	Lyman	k1gMnSc2	Lyman
Spitzera	Spitzer	k1gMnSc2	Spitzer
<g/>
,	,	kIx,	,
amerického	americký	k2eAgMnSc2d1	americký
teoretického	teoretický	k2eAgMnSc2d1	teoretický
fyzika	fyzik	k1gMnSc2	fyzik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
umisťovat	umisťovat	k5eAaImF	umisťovat
dalekohledy	dalekohled	k1gInPc4	dalekohled
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
Report	report	k1gInSc4	report
to	ten	k3xDgNnSc1	ten
Project	Project	k2eAgInSc1d1	Project
Rand	rand	k1gInSc1	rand
<g/>
:	:	kIx,	:
Astronomical	Astronomical	k1gMnSc1	Astronomical
Advantages	Advantages	k1gMnSc1	Advantages
of	of	k?	of
an	an	k?	an
Extra-Terrestrial	Extra-Terrestrial	k1gInSc1	Extra-Terrestrial
Observatory	Observator	k1gInPc1	Observator
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
výhody	výhoda	k1gFnPc4	výhoda
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
prostředí	prostředí	k1gNnSc2	prostředí
pro	pro	k7c4	pro
astronomická	astronomický	k2eAgNnPc4d1	astronomické
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
Spitzerova	Spitzerův	k2eAgInSc2d1	Spitzerův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
stála	stát	k5eAaImAgFnS	stát
celkem	celkem	k6eAd1	celkem
670	[number]	k4	670
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
NASA	NASA	kA	NASA
přiřkl	přiřknout	k5eAaPmAgInS	přiřknout
projektu	projekt	k1gInSc3	projekt
SIRTF	SIRTF	kA	SIRTF
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2008	[number]	k4	2008
celkem	celek	k1gInSc7	celek
68,4	[number]	k4	68,4
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
,	,	kIx,	,
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
operovalo	operovat	k5eAaImAgNnS	operovat
s	s	k7c7	s
částkou	částka	k1gFnSc7	částka
71,7	[number]	k4	71,7
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
blížícím	blížící	k2eAgMnSc7d1	blížící
se	se	k3xPyFc4	se
koncem	konec	k1gInSc7	konec
celé	celý	k2eAgFnSc2d1	celá
mise	mise	k1gFnSc2	mise
se	se	k3xPyFc4	se
přidělovaná	přidělovaný	k2eAgFnSc1d1	přidělovaná
částka	částka	k1gFnSc1	částka
postupně	postupně	k6eAd1	postupně
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc2	počátek
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
astronomové	astronom	k1gMnPc1	astronom
zvažovat	zvažovat	k5eAaImF	zvažovat
možnost	možnost	k1gFnSc4	možnost
umístění	umístění	k1gNnSc2	umístění
infračerveného	infračervený	k2eAgInSc2d1	infračervený
teleskopu	teleskop	k1gInSc2	teleskop
mimo	mimo	k7c4	mimo
rušivé	rušivý	k2eAgInPc4d1	rušivý
elementy	element	k1gInPc4	element
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
National	Nationat	k5eAaImAgMnS	Nationat
Research	Research	k1gMnSc1	Research
Council	Council	k1gMnSc1	Council
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc1d1	národní
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
rada	rada	k1gFnSc1	rada
<g/>
)	)	kIx)	)
vydala	vydat	k5eAaPmAgFnS	vydat
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
zprávu	zpráva	k1gFnSc4	zpráva
A	a	k8xC	a
Strategy	Stratega	k1gFnSc2	Stratega
for	forum	k1gNnPc2	forum
Space	Space	k1gMnSc1	Space
Astronomy	astronom	k1gMnPc4	astronom
and	and	k?	and
Astrophysics	Astrophysicsa	k1gFnPc2	Astrophysicsa
for	forum	k1gNnPc2	forum
the	the	k?	the
1980	[number]	k4	1980
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
označila	označit	k5eAaPmAgFnS	označit
Shuttle	Shuttle	k1gFnSc1	Shuttle
Infrared	Infrared	k1gMnSc1	Infrared
Telescope	Telescop	k1gInSc5	Telescop
Facility	Facilit	k1gInPc1	Facilit
(	(	kIx(	(
<g/>
Infračervený	infračervený	k2eAgInSc1d1	infračervený
teleskop	teleskop	k1gInSc1	teleskop
nesený	nesený	k2eAgInSc1d1	nesený
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
)	)	kIx)	)
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
pro	pro	k7c4	pro
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
očekáváním	očekávání	k1gNnSc7	očekávání
výsledků	výsledek	k1gInPc2	výsledek
nové	nový	k2eAgFnSc2d1	nová
družice	družice	k1gFnSc2	družice
COBE	COBE	kA	COBE
se	se	k3xPyFc4	se
zpráva	zpráva	k1gFnSc1	zpráva
zmínila	zmínit	k5eAaPmAgFnS	zmínit
o	o	k7c6	o
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
vývoji	vývoj	k1gInSc6	vývoj
dlouhotrvajících	dlouhotrvající	k2eAgInPc2d1	dlouhotrvající
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
letů	let	k1gInPc2	let
kryogenicky	kryogenicky	k6eAd1	kryogenicky
chlazených	chlazený	k2eAgInPc2d1	chlazený
infračervených	infračervený	k2eAgInPc2d1	infračervený
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Vypuštění	vypuštění	k1gNnSc1	vypuštění
Infračerveného	infračervený	k2eAgInSc2d1	infračervený
astronomického	astronomický	k2eAgInSc2d1	astronomický
satelitu	satelit	k1gInSc2	satelit
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
vyvinutého	vyvinutý	k2eAgInSc2d1	vyvinutý
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
Spojených	spojený	k2eAgInPc2d1	spojený
Států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
provedl	provést	k5eAaPmAgInS	provést
kompletní	kompletní	k2eAgInSc1d1	kompletní
infračervený	infračervený	k2eAgInSc1d1	infračervený
průzkum	průzkum	k1gInSc1	průzkum
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
povzbudilo	povzbudit	k5eAaPmAgNnS	povzbudit
vědce	vědec	k1gMnSc4	vědec
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
technologie	technologie	k1gFnSc2	technologie
infračervených	infračervený	k2eAgInPc2d1	infračervený
detektorů	detektor	k1gInPc2	detektor
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
raných	raný	k2eAgInPc2d1	raný
konceptů	koncept	k1gInPc2	koncept
viděla	vidět	k5eAaImAgFnS	vidět
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
opakovaných	opakovaný	k2eAgNnPc6d1	opakované
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
čekalo	čekat	k5eAaImAgNnS	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
program	program	k1gInSc1	program
raketoplánů	raketoplán	k1gInPc2	raketoplán
bude	být	k5eAaImBp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
podporovat	podporovat	k5eAaImF	podporovat
každotýdenní	každotýdenní	k2eAgInPc4d1	každotýdenní
lety	let	k1gInPc4	let
s	s	k7c7	s
trváním	trvání	k1gNnSc7	trvání
do	do	k7c2	do
třiceti	třicet	k4xCc2	třicet
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
NASA	NASA	kA	NASA
z	z	k7c2	z
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
popisuje	popisovat	k5eAaImIp3nS	popisovat
SIRTF	SIRTF	kA	SIRTF
jako	jako	k8xS	jako
misi	mise	k1gFnSc4	mise
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yQgFnSc2	který
bude	být	k5eAaImBp3nS	být
využito	využít	k5eAaPmNgNnS	využít
k	k	k7c3	k
raketoplánu	raketoplán	k1gInSc3	raketoplán
připevněných	připevněný	k2eAgInPc2d1	připevněný
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
–	–	k?	–
"	"	kIx"	"
<g/>
SIRTF	SIRTF	kA	SIRTF
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zařízení	zařízení	k1gNnSc1	zařízení
velikosti	velikost	k1gFnSc2	velikost
kolem	kolem	k7c2	kolem
1	[number]	k4	1
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
kryogenicky	kryogenicky	k6eAd1	kryogenicky
chlazené	chlazený	k2eAgInPc1d1	chlazený
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgInPc1d1	sestávající
z	z	k7c2	z
teleskopu	teleskop	k1gInSc2	teleskop
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
souvisejících	související	k2eAgInPc2d1	související
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
vyneseno	vynést	k5eAaPmNgNnS	vynést
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
,	,	kIx,	,
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
by	by	kYmCp3nS	by
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
připojeno	připojit	k5eAaPmNgNnS	připojit
jakožto	jakožto	k8xS	jakožto
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
prováděla	provádět	k5eAaImAgFnS	provádět
potřebné	potřebný	k2eAgInPc4d1	potřebný
výzkumy	výzkum	k1gInPc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
společně	společně	k6eAd1	společně
s	s	k7c7	s
raketoplánem	raketoplán	k1gInSc7	raketoplán
vrátit	vrátit	k5eAaPmF	vrátit
opět	opět	k6eAd1	opět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
připravena	připraven	k2eAgFnSc1d1	připravena
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
letu	let	k1gInSc3	let
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Počítalo	počítat	k5eAaImAgNnS	počítat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
lety	let	k1gInPc7	let
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
začít	začít	k5eAaPmF	začít
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
laboratoře	laboratoř	k1gFnSc2	laboratoř
Spacelab-	Spacelab-	k1gFnSc2	Spacelab-
<g/>
2	[number]	k4	2
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Challengeru	Challenger	k1gInSc2	Challenger
(	(	kIx(	(
<g/>
mise	mise	k1gFnSc1	mise
STS-	STS-	k1gFnSc1	STS-
<g/>
51	[number]	k4	51
<g/>
-F	-F	k?	-F
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
však	však	k9	však
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostředí	prostředí	k1gNnSc1	prostředí
raketoplánu	raketoplán	k1gInSc2	raketoplán
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
infračervený	infračervený	k2eAgInSc4d1	infračervený
dalekohled	dalekohled	k1gInSc4	dalekohled
značně	značně	k6eAd1	značně
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
kontaminace	kontaminace	k1gFnSc2	kontaminace
kosmickým	kosmický	k2eAgNnSc7d1	kosmické
smetím	smetí	k1gNnSc7	smetí
<g/>
,	,	kIx,	,
nacházejícím	nacházející	k2eAgMnSc7d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1983	[number]	k4	1983
tedy	tedy	k8xC	tedy
NASA	NASA	kA	NASA
začala	začít	k5eAaPmAgFnS	začít
zvažovat	zvažovat	k5eAaImF	zvažovat
možnost	možnost	k1gFnSc1	možnost
dlouhotrvajícího	dlouhotrvající	k2eAgInSc2d1	dlouhotrvající
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
by	by	kYmCp3nS	by
SIRTF	SIRTF	kA	SIRTF
prováděl	provádět	k5eAaImAgInS	provádět
pozorování	pozorování	k1gNnPc4	pozorování
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
prostoru	prostor	k1gInSc6	prostor
bez	bez	k7c2	bez
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
.	.	kIx.	.
</s>
<s>
Spitzerův	Spitzerův	k2eAgInSc1d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
zařízením	zařízení	k1gNnSc7	zařízení
z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
Velkých	velký	k2eAgFnPc2d1	velká
observatoří	observatoř	k1gFnPc2	observatoř
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nebylo	být	k5eNaImAgNnS	být
vyneseno	vynést	k5eAaPmNgNnS	vynést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
pomocí	pomocí	k7c2	pomocí
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
misi	mise	k1gFnSc6	mise
původně	původně	k6eAd1	původně
počítalo	počítat	k5eAaImAgNnS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
katastrofě	katastrofa	k1gFnSc6	katastrofa
raketoplánu	raketoplán	k1gInSc2	raketoplán
Challenger	Challenger	k1gInSc1	Challenger
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
však	však	k8xC	však
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
používání	používání	k1gNnSc3	používání
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
nosných	nosný	k2eAgFnPc2d1	nosná
raket	raketa	k1gFnPc2	raketa
Atlas	Atlas	k1gMnSc1	Atlas
–	–	k?	–
Centaur	Centaur	k1gMnSc1	Centaur
LH	LH	kA	LH
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
LOX	LOX	kA	LOX
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
dopravení	dopravení	k1gNnSc4	dopravení
nákladu	náklad	k1gInSc2	náklad
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
prodělala	prodělat	k5eAaPmAgFnS	prodělat
mise	mise	k1gFnSc1	mise
sérii	série	k1gFnSc4	série
různých	různý	k2eAgFnPc2d1	různá
úprav	úprava	k1gFnPc2	úprava
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
škrtům	škrt	k1gInPc3	škrt
v	v	k7c6	v
rozpočtu	rozpočet	k1gInSc6	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
sice	sice	k8xC	sice
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc3d2	menší
avšak	avšak	k8xC	avšak
stále	stále	k6eAd1	stále
přínosnou	přínosný	k2eAgFnSc4d1	přínosná
misi	mise	k1gFnSc4	mise
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
jako	jako	k9	jako
nosič	nosič	k1gInSc4	nosič
využít	využít	k5eAaPmF	využít
raketu	raketa	k1gFnSc4	raketa
Delta	delta	k1gNnSc2	delta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
změn	změna	k1gFnPc2	změna
bylo	být	k5eAaImAgNnS	být
využití	využití	k1gNnSc4	využití
heliocentrické	heliocentrický	k2eAgFnSc2d1	heliocentrická
orbity	orbita	k1gFnSc2	orbita
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spitzer	Spitzer	k1gInSc1	Spitzer
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
po	po	k7c6	po
stejné	stejný	k2eAgFnSc6d1	stejná
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
jako	jako	k8xS	jako
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
následuje	následovat	k5eAaImIp3nS	následovat
v	v	k7c6	v
závěsu	závěs	k1gInSc6	závěs
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
libračním	librační	k2eAgNnSc6d1	librační
centru	centrum	k1gNnSc6	centrum
2	[number]	k4	2
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
0,1	[number]	k4	0,1
AU	au	k0	au
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Kryogenicky	kryogenicky	k6eAd1	kryogenicky
chlazené	chlazený	k2eAgFnPc1d1	chlazená
družice	družice	k1gFnPc1	družice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
tekuté	tekutý	k2eAgNnSc4d1	tekuté
helium	helium	k1gNnSc4	helium
(	(	kIx(	(
<g/>
LHe	LHe	k1gMnSc1	LHe
<g/>
,	,	kIx,	,
T	T	kA	T
≈	≈	k?	≈
4	[number]	k4	4
K	K	kA	K
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Země	zem	k1gFnSc2	zem
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
značné	značný	k2eAgFnSc3d1	značná
tepelné	tepelný	k2eAgFnSc3d1	tepelná
zátěži	zátěž	k1gFnSc3	zátěž
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
chlazení	chlazení	k1gNnSc3	chlazení
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
využívat	využívat	k5eAaImF	využívat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
chladicí	chladicí	k2eAgFnSc2d1	chladicí
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poté	poté	k6eAd1	poté
zabírá	zabírat	k5eAaImIp3nS	zabírat
většinu	většina	k1gFnSc4	většina
nákladové	nákladový	k2eAgFnSc2d1	nákladová
kapacity	kapacita	k1gFnSc2	kapacita
a	a	k8xC	a
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
tím	ten	k3xDgNnSc7	ten
maximální	maximální	k2eAgFnSc1d1	maximální
délku	délka	k1gFnSc4	délka
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
Umístěním	umístění	k1gNnSc7	umístění
satelitu	satelit	k1gInSc2	satelit
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Slunce	slunce	k1gNnSc2	slunce
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
inovativní	inovativní	k2eAgFnSc2d1	inovativní
technologie	technologie	k1gFnSc2	technologie
jako	jako	k9	jako
např.	např.	kA	např.
pasivní	pasivní	k2eAgNnSc1d1	pasivní
chlazení	chlazení	k1gNnSc1	chlazení
(	(	kIx(	(
<g/>
solární	solární	k2eAgInSc1d1	solární
štít	štít	k1gInSc1	štít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dramaticky	dramaticky	k6eAd1	dramaticky
snížily	snížit	k5eAaPmAgFnP	snížit
celkové	celkový	k2eAgNnSc4d1	celkové
množství	množství	k1gNnSc4	množství
potřebného	potřebný	k2eAgNnSc2d1	potřebné
helia	helium	k1gNnSc2	helium
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
finanční	finanční	k2eAgInPc4d1	finanční
náklady	náklad	k1gInPc4	náklad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spitzer	Spitzer	k1gInSc1	Spitzer
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
aktuálním	aktuální	k2eAgNnSc6d1	aktuální
umístění	umístění	k1gNnSc6	umístění
odstíněn	odstínit	k5eAaPmNgInS	odstínit
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
i	i	k8xC	i
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
nasměrován	nasměrován	k2eAgInSc1d1	nasměrován
na	na	k7c4	na
opačnou	opačný	k2eAgFnSc4d1	opačná
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
využitím	využití	k1gNnSc7	využití
unikátních	unikátní	k2eAgFnPc2d1	unikátní
vlastností	vlastnost	k1gFnPc2	vlastnost
této	tento	k3xDgFnSc2	tento
orbity	orbita	k1gFnSc2	orbita
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
speciální	speciální	k2eAgInSc1d1	speciální
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
ukládání	ukládání	k1gNnSc4	ukládání
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
telemetrie	telemetrie	k1gFnSc2	telemetrie
teleskopu	teleskop	k1gInSc2	teleskop
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Store-and-Dump	Storend-Dump	k1gInSc1	Store-and-Dump
Telemetry	telemetr	k1gInPc1	telemetr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vypuštění	vypuštění	k1gNnSc3	vypuštění
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
v	v	k7c6	v
5	[number]	k4	5
h	h	k?	h
35	[number]	k4	35
min	min	kA	min
39	[number]	k4	39
sek	sek	k1gInSc4	sek
UT	UT	kA	UT
z	z	k7c2	z
Cape	capat	k5eAaImIp3nS	capat
Canaveral	Canaveral	k1gFnSc1	Canaveral
Air	Air	k1gFnSc1	Air
Force	force	k1gFnSc1	force
Station	station	k1gInSc4	station
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
Delta	delta	k1gFnSc1	delta
II	II	kA	II
(	(	kIx(	(
<g/>
7920	[number]	k4	7920
<g/>
H	H	kA	H
ELV	ELV	kA	ELV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
<g/>
:	:	kIx,	:
<g/>
<g />
.	.	kIx.	.
</s>
<s>
39	[number]	k4	39
UT	UT	kA	UT
–	–	k?	–
start	start	k1gInSc1	start
rakety	raketa	k1gFnSc2	raketa
Delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
7920	[number]	k4	7920
<g/>
H	H	kA	H
ELV	ELV	kA	ELV
<g/>
)	)	kIx)	)
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
–	–	k?	–
dosažena	dosažen	k2eAgFnSc1d1	dosažena
rychlost	rychlost	k1gFnSc1	rychlost
1	[number]	k4	1
Ma	Ma	k1gFnSc1	Ma
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
37	[number]	k4	37
–	–	k?	–
odhození	odhození	k1gNnSc4	odhození
šesti	šest	k4xCc2	šest
startovacích	startovací	k2eAgInPc2d1	startovací
motorů	motor	k1gInPc2	motor
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
–	–	k?	–
odstavení	odstavení	k1gNnSc4	odstavení
motoru	motor	k1gInSc2	motor
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
stupně	stupeň	k1gInPc1	stupeň
<g/>
;	;	kIx,	;
následné	následný	k2eAgNnSc4d1	následné
odhození	odhození	k1gNnSc4	odhození
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
42	[number]	k4	42
–	–	k?	–
výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
144,81	[number]	k4	144,81
km	km	kA	km
<g/>
;	;	kIx,	;
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
25	[number]	k4	25
890	[number]	k4	890
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
43	[number]	k4	43
–	–	k?	–
plánované	plánovaný	k2eAgNnSc4d1	plánované
vypnutí	vypnutí	k1gNnSc4	vypnutí
motoru	motor	k1gInSc2	motor
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
dosažena	dosažen	k2eAgFnSc1d1	dosažena
vyčkávací	vyčkávací	k2eAgFnSc1d1	vyčkávací
orbita	orbita	k1gFnSc1	orbita
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
–	–	k?	–
znovuzažehnutí	znovuzažehnutí	k1gNnSc4	znovuzažehnutí
motoru	motor	k1gInSc2	motor
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
–	–	k?	–
SIRTF	SIRTF	kA	SIRTF
naveden	naveden	k2eAgInSc1d1	naveden
na	na	k7c4	na
únikový	únikový	k2eAgInSc4d1	únikový
orbit	orbita	k1gFnPc2	orbita
rychlostí	rychlost	k1gFnSc7	rychlost
11,05	[number]	k4	11,05
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
–	–	k?	–
SIRTF	SIRTF	kA	SIRTF
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
rakety	raketa	k1gFnSc2	raketa
Delta	delta	k1gFnSc1	delta
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
41	[number]	k4	41
–	–	k?	–
Stanice	stanice	k1gFnSc1	stanice
sítě	síť	k1gFnSc2	síť
Deep	Deep	k1gInSc1	Deep
Space	Space	k1gMnSc1	Space
Network	network	k1gInSc1	network
u	u	k7c2	u
Canberry	Canberra	k1gFnSc2	Canberra
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
definitivně	definitivně	k6eAd1	definitivně
navazuje	navazovat	k5eAaImIp3nS	navazovat
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
SIRTF	SIRTF	kA	SIRTF
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
sada	sada	k1gFnSc1	sada
přístrojů	přístroj	k1gInPc2	přístroj
(	(	kIx(	(
<g/>
teleskop	teleskop	k1gInSc1	teleskop
a	a	k8xC	a
kryogenní	kryogenní	k2eAgFnSc1d1	kryogenní
komora	komora	k1gFnSc1	komora
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
společností	společnost	k1gFnSc7	společnost
Ball	Ball	k1gMnSc1	Ball
Aerospace	Aerospace	k1gFnSc2	Aerospace
&	&	k?	&
Technologies	Technologies	k1gMnSc1	Technologies
Corp	Corp	k1gMnSc1	Corp
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
zařízení	zařízení	k1gNnPc1	zařízení
byla	být	k5eAaImAgNnP	být
vyvinuta	vyvinout	k5eAaPmNgNnP	vyvinout
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
<g/>
,	,	kIx,	,
akademickými	akademický	k2eAgFnPc7d1	akademická
a	a	k8xC	a
vládními	vládní	k2eAgFnPc7d1	vládní
institucemi	instituce	k1gFnPc7	instituce
jako	jako	k8xS	jako
např.	např.	kA	např.
Cornell	Cornell	k1gInSc1	Cornell
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
,	,	kIx,	,
Smithsonian	Smithsonian	k1gInSc1	Smithsonian
Astrophysical	Astrophysical	k1gFnSc2	Astrophysical
Observatory	Observator	k1gInPc1	Observator
(	(	kIx(	(
<g/>
spíš	spíš	k9	spíš
Harvard-Smithsonian	Harvard-Smithsoniany	k1gInPc2	Harvard-Smithsoniany
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Astrophysics	Astrophysics	k1gInSc1	Astrophysics
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ball	Ball	k1gMnSc1	Ball
Aerospace	Aerospace	k1gFnSc2	Aerospace
a	a	k8xC	a
Goddard	Goddard	k1gMnSc1	Goddard
Spaceflight	Spaceflight	k1gMnSc1	Spaceflight
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
Spitzerova	Spitzerův	k2eAgInSc2d1	Spitzerův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
bylo	být	k5eAaImAgNnS	být
zkonstruováno	zkonstruován	k2eAgNnSc1d1	zkonstruováno
společností	společnost	k1gFnSc7	společnost
Lockheed	Lockheed	k1gMnSc1	Lockheed
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Teleskop	teleskop	k1gInSc1	teleskop
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
nepřekračovala	překračovat	k5eNaImAgFnS	překračovat
50	[number]	k4	50
kg	kg	kA	kg
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
konstrukce	konstrukce	k1gFnSc1	konstrukce
dokázala	dokázat	k5eAaPmAgFnS	dokázat
odolat	odolat	k5eAaPmF	odolat
extrémně	extrémně	k6eAd1	extrémně
nízkým	nízký	k2eAgFnPc3d1	nízká
teplotám	teplota	k1gFnPc3	teplota
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
dalekohledu	dalekohled	k1gInSc2	dalekohled
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc1	průměr
85	[number]	k4	85
cm	cm	kA	cm
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
(	(	kIx(	(
<g/>
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
dvanáctinásobkem	dvanáctinásobek	k1gInSc7	dvanáctinásobek
průměru	průměr	k1gInSc2	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
beryllia	beryllium	k1gNnSc2	beryllium
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
výhodu	výhoda	k1gFnSc4	výhoda
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
malá	malý	k2eAgFnSc1d1	malá
tepelná	tepelný	k2eAgFnSc1d1	tepelná
vodivost	vodivost	k1gFnSc1	vodivost
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ochlazováno	ochlazován	k2eAgNnSc1d1	ochlazováno
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
5,5	[number]	k4	5,5
K.	K.	kA	K.
Teleskop	teleskop	k1gInSc1	teleskop
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
třemi	tři	k4xCgNnPc7	tři
zařízeními	zařízení	k1gNnPc7	zařízení
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
snímání	snímání	k1gNnSc4	snímání
a	a	k8xC	a
fotometrii	fotometrie	k1gFnSc4	fotometrie
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
180	[number]	k4	180
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
,	,	kIx,	,
spektroskopii	spektroskopie	k1gFnSc6	spektroskopie
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
40	[number]	k4	40
mikrometrů	mikrometr	k1gInPc2	mikrometr
a	a	k8xC	a
spektrofotometrii	spektrofotometrie	k1gFnSc4	spektrofotometrie
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
100	[number]	k4	100
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
teleskopu	teleskop	k1gInSc2	teleskop
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgInPc1d1	následující
<g/>
:	:	kIx,	:
IRAC	IRAC	kA	IRAC
(	(	kIx(	(
<g/>
Infrared	Infrared	k1gInSc1	Infrared
Array	Arraa	k1gMnSc2	Arraa
Camera	Camer	k1gMnSc2	Camer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
infračervená	infračervený	k2eAgFnSc1d1	infračervená
kamera	kamera	k1gFnSc1	kamera
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
operuje	operovat	k5eAaImIp3nS	operovat
současně	současně	k6eAd1	současně
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
(	(	kIx(	(
<g/>
3,6	[number]	k4	3,6
μ	μ	k?	μ
<g/>
,	,	kIx,	,
4,5	[number]	k4	4,5
μ	μ	k?	μ
<g/>
,	,	kIx,	,
5,8	[number]	k4	5,8
μ	μ	k?	μ
a	a	k8xC	a
8	[number]	k4	8
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišení	rozlišení	k1gNnSc1	rozlišení
je	být	k5eAaImIp3nS	být
256	[number]	k4	256
×	×	k?	×
256	[number]	k4	256
pixelů	pixel	k1gInPc2	pixel
<g/>
.	.	kIx.	.
</s>
<s>
IRS	IRS	kA	IRS
(	(	kIx(	(
<g/>
Infrared	Infrared	k1gMnSc1	Infrared
Spectrograph	Spectrograph	k1gMnSc1	Spectrograph
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
infračervený	infračervený	k2eAgInSc1d1	infračervený
spektrometr	spektrometr	k1gInSc1	spektrometr
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
podmoduly	podmodul	k1gInPc7	podmodul
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
operuje	operovat	k5eAaImIp3nS	operovat
ve	v	k7c6	v
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
5,3	[number]	k4	5,3
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
μ	μ	k?	μ
(	(	kIx(	(
<g/>
nízké	nízký	k2eAgNnSc1d1	nízké
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
19,5	[number]	k4	19,5
μ	μ	k?	μ
(	(	kIx(	(
<g/>
vysoké	vysoký	k2eAgNnSc1d1	vysoké
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
μ	μ	k?	μ
(	(	kIx(	(
<g/>
nízké	nízký	k2eAgNnSc1d1	nízké
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
)	)	kIx)	)
a	a	k8xC	a
19	[number]	k4	19
<g/>
–	–	k?	–
<g/>
37	[number]	k4	37
μ	μ	k?	μ
(	(	kIx(	(
<g/>
vysoké	vysoký	k2eAgNnSc1d1	vysoké
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
MIPS	MIPS	kA	MIPS
(	(	kIx(	(
<g/>
Multiband	Multiband	k1gInSc1	Multiband
Imaging	Imaging	k1gInSc1	Imaging
Photometer	Photometra	k1gFnPc2	Photometra
for	forum	k1gNnPc2	forum
Spitzer	Spitzer	k1gInSc1	Spitzer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
provádí	provádět	k5eAaImIp3nS	provádět
snímání	snímání	k1gNnSc4	snímání
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
spektroskopická	spektroskopický	k2eAgNnPc4d1	spektroskopické
data	datum	k1gNnPc4	datum
z	z	k7c2	z
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
větších	veliký	k2eAgFnPc2d2	veliký
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
snímacích	snímací	k2eAgInPc2d1	snímací
modulů	modul	k1gInPc2	modul
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc1	první
má	mít	k5eAaImIp3nS	mít
rozlišení	rozlišení	k1gNnSc1	rozlišení
128	[number]	k4	128
x	x	k?	x
128	[number]	k4	128
pixelů	pixel	k1gInPc2	pixel
snímající	snímající	k2eAgNnSc1d1	snímající
záření	záření	k1gNnSc1	záření
λ	λ	k?	λ
<g/>
=	=	kIx~	=
24	[number]	k4	24
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
křemíku	křemík	k1gInSc2	křemík
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
obohaceném	obohacený	k2eAgInSc6d1	obohacený
arsenem	arsen	k1gInSc7	arsen
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
modulů	modul	k1gInPc2	modul
má	mít	k5eAaImIp3nS	mít
rozlišení	rozlišení	k1gNnSc1	rozlišení
32	[number]	k4	32
x	x	k?	x
32	[number]	k4	32
pixelů	pixel	k1gInPc2	pixel
a	a	k8xC	a
snímá	snímat	k5eAaImIp3nS	snímat
IR	Ir	k1gMnSc1	Ir
záření	záření	k1gNnSc2	záření
λ	λ	k?	λ
<g/>
=	=	kIx~	=
70	[number]	k4	70
mikrometrů	mikrometr	k1gInPc2	mikrometr
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
modul	modul	k1gInSc1	modul
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
2	[number]	k4	2
x	x	k?	x
20	[number]	k4	20
mikrometrů	mikrometr	k1gInPc2	mikrometr
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
pro	pro	k7c4	pro
snímání	snímání	k1gNnSc4	snímání
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
160	[number]	k4	160
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Snímací	snímací	k2eAgFnSc1d1	snímací
schopnost	schopnost	k1gFnSc1	schopnost
MIPS	MIPS	kA	MIPS
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
5	[number]	k4	5
x	x	k?	x
5	[number]	k4	5
úhlových	úhlový	k2eAgFnPc2d1	úhlová
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
až	až	k9	až
0,5	[number]	k4	0,5
x	x	k?	x
5	[number]	k4	5
(	(	kIx(	(
<g/>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgNnP	být
pozorování	pozorování	k1gNnPc1	pozorování
v	v	k7c6	v
infračerveném	infračervený	k2eAgInSc6d1	infračervený
oboru	obor	k1gInSc6	obor
spektra	spektrum	k1gNnSc2	spektrum
prováděna	provádět	k5eAaImNgNnP	provádět
kombinací	kombinace	k1gFnPc2	kombinace
pozemských	pozemský	k2eAgNnPc2d1	pozemské
a	a	k8xC	a
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Pozemské	pozemský	k2eAgFnPc1d1	pozemská
hvězdárny	hvězdárna	k1gFnPc1	hvězdárna
mají	mít	k5eAaImIp3nP	mít
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemská	zemský	k2eAgFnSc1d1	zemská
atmosféra	atmosféra	k1gFnSc1	atmosféra
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
prodlužovat	prodlužovat	k5eAaImF	prodlužovat
dobu	doba	k1gFnSc4	doba
expozice	expozice	k1gFnSc2	expozice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
značně	značně	k6eAd1	značně
snižuje	snižovat	k5eAaImIp3nS	snižovat
možnost	možnost	k1gFnSc4	možnost
sledovat	sledovat	k5eAaImF	sledovat
slabší	slabý	k2eAgInPc4d2	slabší
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc1d1	předchozí
satelity	satelit	k1gInPc1	satelit
pro	pro	k7c4	pro
IR	Ir	k1gMnSc1	Ir
pozorování	pozorování	k1gNnPc2	pozorování
jako	jako	k8xS	jako
např.	např.	kA	např.
IRAS	IRAS	kA	IRAS
–	–	k?	–
(	(	kIx(	(
<g/>
Infrared	Infrared	k1gMnSc1	Infrared
Astronomical	Astronomical	k1gMnSc1	Astronomical
Satellite	Satellit	k1gInSc5	Satellit
<g/>
)	)	kIx)	)
a	a	k8xC	a
ISO	ISO	kA	ISO
(	(	kIx(	(
<g/>
the	the	k?	the
Infrared	Infrared	k1gInSc1	Infrared
Space	Space	k1gFnSc1	Space
Observatory	Observator	k1gInPc1	Observator
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
v	v	k7c6	v
letech	let	k1gInPc6	let
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
dob	doba	k1gFnPc2	doba
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
astronomických	astronomický	k2eAgFnPc6d1	astronomická
technologiích	technologie	k1gFnPc6	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc1d1	předchozí
infračervené	infračervený	k2eAgInPc1d1	infračervený
teleskopy	teleskop	k1gInPc1	teleskop
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
byly	být	k5eAaImAgFnP	být
zahaleny	zahalen	k2eAgMnPc4d1	zahalen
obrovským	obrovský	k2eAgInSc7d1	obrovský
kryostatem	kryostat	k1gInSc7	kryostat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
supratekuté	supratekutý	k2eAgNnSc4d1	supratekuté
helium	helium	k1gNnSc4	helium
a	a	k8xC	a
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
fungování	fungování	k1gNnSc4	fungování
teleskopu	teleskop	k1gInSc2	teleskop
o	o	k7c6	o
teplotách	teplota	k1gFnPc6	teplota
blízko	blízko	k6eAd1	blízko
absolutní	absolutní	k2eAgFnSc3d1	absolutní
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
konfigurace	konfigurace	k1gFnSc1	konfigurace
vešla	vejít	k5eAaPmAgFnS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
Cold	Cold	k1gMnSc1	Cold
launch	launch	k1gMnSc1	launch
architecture	architectur	k1gMnSc5	architectur
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
teleskopů	teleskop	k1gInPc2	teleskop
IRAS	IRAS	kA	IRAS
a	a	k8xC	a
ISO	ISO	kA	ISO
<g/>
,	,	kIx,	,
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
Spitzera	Spitzero	k1gNnSc2	Spitzero
využito	využít	k5eAaPmNgNnS	využít
inovativní	inovativní	k2eAgFnSc2d1	inovativní
technologie	technologie	k1gFnSc2	technologie
tzv.	tzv.	kA	tzv.
Warm	Warm	k1gMnSc1	Warm
launch	launch	k1gMnSc1	launch
architecture	architectur	k1gInSc5	architectur
<g/>
.	.	kIx.	.
</s>
<s>
Spitzerův	Spitzerův	k2eAgInSc1d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
převážné	převážný	k2eAgFnSc2d1	převážná
části	část	k1gFnSc2	část
ochlazován	ochlazován	k2eAgInSc1d1	ochlazován
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
teplotou	teplota	k1gFnSc7	teplota
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
pasivní	pasivní	k2eAgNnSc1d1	pasivní
chlazení	chlazení	k1gNnSc1	chlazení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
snímací	snímací	k2eAgNnSc1d1	snímací
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dodatečné	dodatečný	k2eAgNnSc1d1	dodatečné
ochlazování	ochlazování	k1gNnSc1	ochlazování
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ukryto	ukrýt	k5eAaPmNgNnS	ukrýt
společně	společně	k6eAd1	společně
s	s	k7c7	s
kryostatem	kryostat	k1gInSc7	kryostat
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
vakuové	vakuový	k2eAgFnSc6d1	vakuová
schráně	schrána	k1gFnSc6	schrána
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
správné	správný	k2eAgNnSc4d1	správné
fungování	fungování	k1gNnSc4	fungování
technologie	technologie	k1gFnSc2	technologie
"	"	kIx"	"
<g/>
warm	warm	k1gMnSc1	warm
launch	launch	k1gMnSc1	launch
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
však	však	k9	však
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
správná	správný	k2eAgFnSc1d1	správná
volba	volba	k1gFnSc1	volba
umístění	umístění	k1gNnSc2	umístění
(	(	kIx(	(
<g/>
orbity	orbita	k1gFnSc2	orbita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
teleskop	teleskop	k1gInSc1	teleskop
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c4	v
dostatečně	dostatečně	k6eAd1	dostatečně
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
schopna	schopen	k2eAgFnSc1d1	schopna
jej	on	k3xPp3gMnSc4	on
ochladit	ochladit	k5eAaPmF	ochladit
již	již	k6eAd1	již
během	během	k7c2	během
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
40	[number]	k4	40
K.	K.	kA	K.
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
chlazení	chlazení	k1gNnSc1	chlazení
zajišťováno	zajišťovat	k5eAaImNgNnS	zajišťovat
vnější	vnější	k2eAgFnSc7d1	vnější
schránou	schrána	k1gFnSc7	schrána
s	s	k7c7	s
tekutým	tekutý	k2eAgNnSc7d1	tekuté
heliem	helium	k1gNnSc7	helium
–	–	k?	–
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
odpařující	odpařující	k2eAgNnSc1d1	odpařující
helium	helium	k1gNnSc1	helium
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
teleskop	teleskop	k1gInSc1	teleskop
na	na	k7c4	na
operační	operační	k2eAgFnSc4d1	operační
teplotu	teplota	k1gFnSc4	teplota
5,5	[number]	k4	5,5
K.	K.	kA	K.
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
přínosů	přínos	k1gInPc2	přínos
této	tento	k3xDgFnSc2	tento
technologie	technologie	k1gFnSc2	technologie
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
redukce	redukce	k1gFnSc1	redukce
velikosti	velikost	k1gFnSc2	velikost
celé	celá	k1gFnSc2	celá
této	tento	k3xDgFnSc2	tento
observatoře	observatoř	k1gFnSc2	observatoř
a	a	k8xC	a
snížení	snížení	k1gNnSc4	snížení
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
dopravu	doprava	k1gFnSc4	doprava
zařízení	zařízení	k1gNnSc2	zařízení
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
<g/>
:	:	kIx,	:
SIRTF	SIRTF	kA	SIRTF
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
360	[number]	k4	360
litrů	litr	k1gInPc2	litr
helia	helium	k1gNnPc1	helium
během	během	k7c2	během
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
jeho	jeho	k3xOp3gMnSc4	jeho
předchůdce	předchůdce	k1gMnSc4	předchůdce
IRAS	IRAS	kA	IRAS
to	ten	k3xDgNnSc1	ten
činilo	činit	k5eAaImAgNnS	činit
520	[number]	k4	520
litrů	litr	k1gInPc2	litr
spotřebovaných	spotřebovaný	k2eAgInPc2d1	spotřebovaný
za	za	k7c2	za
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ISO	ISO	kA	ISO
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spotřeboval	spotřebovat	k5eAaPmAgInS	spotřebovat
2140	[number]	k4	2140
litrů	litr	k1gInPc2	litr
během	během	k7c2	během
2,5	[number]	k4	2,5
roční	roční	k2eAgFnSc2d1	roční
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
této	tento	k3xDgFnSc2	tento
úsporné	úsporný	k2eAgFnSc2d1	úsporná
inovativní	inovativní	k2eAgFnSc2d1	inovativní
technologie	technologie	k1gFnSc2	technologie
se	s	k7c7	s
(	(	kIx(	(
<g/>
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
úpravami	úprava	k1gFnPc7	úprava
<g/>
)	)	kIx)	)
počítá	počítat	k5eAaImIp3nS	počítat
i	i	k9	i
u	u	k7c2	u
nových	nový	k2eAgInPc2d1	nový
projektů	projekt	k1gInPc2	projekt
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
infračervených	infračervený	k2eAgInPc2d1	infračervený
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
dalekohled	dalekohled	k1gInSc4	dalekohled
Jamese	Jamese	k1gFnSc1	Jamese
Webba	Webba	k1gFnSc1	Webba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
inovací	inovace	k1gFnSc7	inovace
použitou	použitý	k2eAgFnSc7d1	použitá
u	u	k7c2	u
Spitzera	Spitzero	k1gNnSc2	Spitzero
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgFnSc1d1	speciální
metoda	metoda	k1gFnSc1	metoda
zpracování	zpracování	k1gNnSc2	zpracování
získávaných	získávaný	k2eAgNnPc2d1	získávané
dat	datum	k1gNnPc2	datum
tzv.	tzv.	kA	tzv.
Store-and-Dump	Storend-Dump	k1gInSc1	Store-and-Dump
Telemetry	telemetr	k1gInPc1	telemetr
(	(	kIx(	(
<g/>
přeložitelné	přeložitelný	k2eAgFnPc1d1	přeložitelná
jako	jako	k8xS	jako
ulož	uložit	k5eAaPmRp2nS	uložit
a	a	k8xC	a
vysyp	vysypat	k5eAaPmRp2nS	vysypat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vysokopaměťový	vysokopaměťový	k2eAgInSc1d1	vysokopaměťový
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
využívá	využívat	k5eAaImIp3nS	využívat
speciální	speciální	k2eAgInSc1d1	speciální
bezeztrátový	bezeztrátový	k2eAgInSc1d1	bezeztrátový
komprimační	komprimační	k2eAgInSc1d1	komprimační
software	software	k1gInSc1	software
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
dat	datum	k1gNnPc2	datum
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
využívána	využíván	k2eAgFnSc1d1	využívána
vysokopříjmová	vysokopříjmový	k2eAgFnSc1d1	vysokopříjmová
nepohyblivá	pohyblivý	k2eNgFnSc1d1	nepohyblivá
anténa	anténa	k1gFnSc1	anténa
uchycená	uchycený	k2eAgFnSc1d1	uchycená
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumné	výzkumný	k2eAgFnPc1d1	výzkumná
práce	práce	k1gFnPc1	práce
prováděné	prováděný	k2eAgFnPc1d1	prováděná
Spitzerem	Spitzer	k1gInSc7	Spitzer
jsou	být	k5eAaImIp3nP	být
přerušeny	přerušit	k5eAaPmNgInP	přerušit
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
či	či	k8xC	či
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
odeslání	odeslání	k1gNnSc2	odeslání
dat	datum	k1gNnPc2	datum
nutné	nutný	k2eAgNnSc1d1	nutné
znovu	znovu	k6eAd1	znovu
zaměřit	zaměřit	k5eAaPmF	zaměřit
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
vysílač	vysílač	k1gInSc4	vysílač
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Telemetrie	telemetrie	k1gFnSc1	telemetrie
je	být	k5eAaImIp3nS	být
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
komunikační	komunikační	k2eAgFnSc7d1	komunikační
sítí	síť	k1gFnSc7	síť
Deep	Deep	k1gMnSc1	Deep
Space	Space	k1gFnSc2	Space
Network	network	k1gInSc1	network
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
přesně	přesně	k6eAd1	přesně
stanovených	stanovený	k2eAgNnPc6d1	stanovené
jednohodinových	jednohodinový	k2eAgNnPc6d1	jednohodinové
časových	časový	k2eAgNnPc6d1	časové
"	"	kIx"	"
<g/>
oknech	okno	k1gNnPc6	okno
<g/>
"	"	kIx"	"
každých	každý	k3xTgNnPc2	každý
12	[number]	k4	12
až	až	k8xS	až
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
operační	operační	k2eAgInPc1d1	operační
úkoly	úkol	k1gInPc1	úkol
jsou	být	k5eAaImIp3nP	být
Spitzeru	Spitzer	k1gMnSc3	Spitzer
obvykle	obvykle	k6eAd1	obvykle
zasílány	zasílán	k2eAgFnPc1d1	zasílána
v	v	k7c6	v
týdenních	týdenní	k2eAgInPc6d1	týdenní
blocích	blok	k1gInPc6	blok
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
tato	tento	k3xDgFnSc1	tento
telemetrie	telemetrie	k1gFnSc1	telemetrie
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
i	i	k9	i
častější	častý	k2eAgFnSc4d2	častější
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
činí	činit	k5eAaImIp3nS	činit
80	[number]	k4	80
kb	kb	kA	kb
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Díky	díky	k7c3	díky
kapacitě	kapacita	k1gFnSc3	kapacita
paměti	paměť	k1gFnSc2	paměť
8	[number]	k4	8
Gb	Gb	k1gFnPc2	Gb
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uložit	uložit	k5eAaPmF	uložit
data	datum	k1gNnPc4	datum
i	i	k8xC	i
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
pozorovací	pozorovací	k2eAgInSc4d1	pozorovací
den	den	k1gInSc4	den
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
promeškání	promeškání	k1gNnSc3	promeškání
přenosového	přenosový	k2eAgNnSc2d1	přenosové
okna	okno	k1gNnSc2	okno
<g/>
,	,	kIx,	,
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
stažení	stažení	k1gNnSc2	stažení
v	v	k7c6	v
okně	okno	k1gNnSc6	okno
následujícím	následující	k2eAgNnSc6d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
teleskopu	teleskop	k1gInSc2	teleskop
provést	provést	k5eAaPmF	provést
až	až	k9	až
o	o	k7c4	o
100	[number]	k4	100
000	[number]	k4	000
pozorování	pozorování	k1gNnPc2	pozorování
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
využil	využít	k5eAaPmAgInS	využít
klasický	klasický	k2eAgInSc1d1	klasický
způsob	způsob	k1gInSc1	způsob
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Pointing	Pointing	k1gInSc1	Pointing
Control	Controla	k1gFnPc2	Controla
Sub-System	Sub-Syst	k1gInSc7	Sub-Syst
(	(	kIx(	(
<g/>
Zaměřovací	zaměřovací	k2eAgFnSc1d1	zaměřovací
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
Sub-systém	Subystý	k2eAgNnSc6d1	Sub-systý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tříosý	tříosý	k2eAgMnSc1d1	tříosý
<g/>
,	,	kIx,	,
celestiálně-inertní	celestiálněnertní	k2eAgInSc1d1	celestiálně-inertní
navigační	navigační	k2eAgInSc1d1	navigační
systém	systém	k1gInSc1	systém
Spitzerova	Spitzerův	k2eAgInSc2d1	Spitzerův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
teleskopu	teleskop	k1gInSc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
koordinačního	koordinační	k2eAgInSc2d1	koordinační
systému	systém	k1gInSc2	systém
J2000	J2000	k1gFnSc2	J2000
(	(	kIx(	(
<g/>
palubní	palubní	k2eAgInSc1d1	palubní
katalog	katalog	k1gInSc1	katalog
hvězd	hvězda	k1gFnPc2	hvězda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
87	[number]	k4	87
000	[number]	k4	000
hvězd	hvězda	k1gFnPc2	hvězda
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
implementován	implementovat	k5eAaImNgInS	implementovat
Sledovačem	Sledovač	k1gInSc7	Sledovač
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Sledovač	Sledovač	k1gInSc1	Sledovač
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
SH	SH	kA	SH
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
zaměřování	zaměřování	k1gNnSc3	zaměřování
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
cílů	cíl	k1gInPc2	cíl
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
±	±	k?	±
<g/>
0,5	[number]	k4	0,5
úhlových	úhlový	k2eAgFnPc2d1	úhlová
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Zorné	zorný	k2eAgNnSc1d1	zorné
pole	pole	k1gNnSc1	pole
zaměřovače	zaměřovač	k1gInSc2	zaměřovač
činí	činit	k5eAaImIp3nS	činit
5	[number]	k4	5
<g/>
°	°	k?	°
x	x	k?	x
5	[number]	k4	5
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Spitzerovi	Spitzer	k1gMnSc3	Spitzer
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zaměření	zaměření	k1gNnSc4	zaměření
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
polohování	polohování	k1gNnSc3	polohování
Spitzera	Spitzero	k1gNnSc2	Spitzero
SH	SH	kA	SH
obvykle	obvykle	k6eAd1	obvykle
využívá	využívat	k5eAaPmIp3nS	využívat
40	[number]	k4	40
hvězd	hvězda	k1gFnPc2	hvězda
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
SH	SH	kA	SH
využíván	využíván	k2eAgInSc1d1	využíván
pro	pro	k7c4	pro
jiný	jiný	k2eAgInSc4d1	jiný
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
správné	správný	k2eAgNnSc4d1	správné
polohování	polohování	k1gNnSc4	polohování
palubní	palubní	k2eAgInPc4d1	palubní
gyroskopy	gyroskop	k1gInPc4	gyroskop
<g/>
.	.	kIx.	.
</s>
<s>
PCS	PCS	kA	PCS
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
počítače	počítač	k1gInSc2	počítač
vybaveného	vybavený	k2eAgInSc2d1	vybavený
letovým	letový	k2eAgInSc7d1	letový
softwarem	software	k1gInSc7	software
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
pro	pro	k7c4	pro
precizní	precizní	k2eAgFnSc4d1	precizní
navigaci	navigace	k1gFnSc4	navigace
<g/>
,	,	kIx,	,
stabilizaci	stabilizace	k1gFnSc3	stabilizace
<g/>
,	,	kIx,	,
natáčení	natáčení	k1gNnSc6	natáčení
a	a	k8xC	a
sledování	sledování	k1gNnSc6	sledování
cílů	cíl	k1gInPc2	cíl
Spitzerova	Spitzerův	k2eAgInSc2d1	Spitzerův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rychlé	rychlý	k2eAgNnSc4d1	rychlé
přenastavení	přenastavení	k1gNnSc4	přenastavení
a	a	k8xC	a
přeorientování	přeorientování	k1gNnSc4	přeorientování
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
řízení	řízení	k1gNnSc1	řízení
drobných	drobný	k2eAgInPc2d1	drobný
korekčních	korekční	k2eAgInPc2d1	korekční
manévrů	manévr	k1gInPc2	manévr
<g/>
.	.	kIx.	.
</s>
<s>
Provádí	provádět	k5eAaImIp3nS	provádět
také	také	k9	také
neustálou	neustálý	k2eAgFnSc4d1	neustálá
osovou	osový	k2eAgFnSc4d1	Osová
kalibraci	kalibrace	k1gFnSc4	kalibrace
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
správnou	správný	k2eAgFnSc4d1	správná
orientaci	orientace	k1gFnSc4	orientace
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
a	a	k8xC	a
směrování	směrování	k1gNnSc2	směrování
vysokopříjmové	vysokopříjmový	k2eAgFnSc2d1	vysokopříjmová
antény	anténa	k1gFnSc2	anténa
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
komunikace	komunikace	k1gFnSc2	komunikace
s	s	k7c7	s
operačním	operační	k2eAgNnSc7d1	operační
střediskem	středisko	k1gNnSc7	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
širokoúhlých	širokoúhlý	k2eAgInPc2d1	širokoúhlý
detektorů	detektor	k1gInPc2	detektor
Slunce	slunce	k1gNnSc2	slunce
také	také	k9	také
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
přílišnému	přílišný	k2eAgNnSc3d1	přílišné
vychýlení	vychýlení	k1gNnSc3	vychýlení
od	od	k7c2	od
stanovené	stanovený	k2eAgFnSc2d1	stanovená
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
snímky	snímek	k1gInPc4	snímek
získané	získaný	k2eAgInPc4d1	získaný
Spitzerovým	Spitzerův	k2eAgInSc7d1	Spitzerův
dalekohledem	dalekohled	k1gInSc7	dalekohled
byly	být	k5eAaImAgFnP	být
sestaveny	sestaven	k2eAgFnPc1d1	sestavena
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zvýraznily	zvýraznit	k5eAaPmAgFnP	zvýraznit
hlavní	hlavní	k2eAgFnPc4d1	hlavní
přednosti	přednost	k1gFnPc4	přednost
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc4	schopnost
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
;	;	kIx,	;
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
žhavé	žhavý	k2eAgNnSc4d1	žhavé
rodiště	rodiště	k1gNnSc4	rodiště
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
disk	disk	k1gInSc4	disk
úlomků	úlomek	k1gInPc2	úlomek
formujících	formující	k2eAgFnPc2d1	formující
se	se	k3xPyFc4	se
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
organické	organický	k2eAgInPc4d1	organický
plyny	plyn	k1gInPc4	plyn
v	v	k7c6	v
hlubokém	hluboký	k2eAgInSc6d1	hluboký
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
odborném	odborný	k2eAgInSc6d1	odborný
i	i	k8xC	i
běžném	běžný	k2eAgInSc6d1	běžný
tisku	tisk	k1gInSc6	tisk
pravidelně	pravidelně	k6eAd1	pravidelně
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
snímky	snímek	k1gInPc1	snímek
ze	z	k7c2	z
Spitzeru	Spitzer	k1gInSc2	Spitzer
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
snímků	snímek	k1gInPc2	snímek
z	z	k7c2	z
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
NASA	NASA	kA	NASA
a	a	k8xC	a
ESA	eso	k1gNnSc2	eso
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
největších	veliký	k2eAgInPc2d3	veliký
úspěchů	úspěch	k1gInPc2	úspěch
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zachytil	zachytit	k5eAaPmAgMnS	zachytit
přímé	přímý	k2eAgNnSc4d1	přímé
světlo	světlo	k1gNnSc4	světlo
z	z	k7c2	z
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
HD	HD	kA	HD
209458	[number]	k4	209458
b	b	k?	b
a	a	k8xC	a
TrES-	TrES-	k1gMnPc1	TrES-
<g/>
1	[number]	k4	1
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
žhavé	žhavý	k2eAgInPc4d1	žhavý
Jupitery	Jupiter	k1gInPc4	Jupiter
(	(	kIx(	(
<g/>
planety	planeta	k1gFnPc4	planeta
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
podobné	podobný	k2eAgFnSc6d1	podobná
jako	jako	k8xS	jako
planeta	planeta	k1gFnSc1	planeta
Jupiter	Jupiter	k1gInSc1	Jupiter
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
pouze	pouze	k6eAd1	pouze
0,05	[number]	k4	0,05
AU	au	k0	au
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
mateřské	mateřský	k2eAgFnSc2d1	mateřská
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
exoplanety	exoplanet	k1gInPc1	exoplanet
přímo	přímo	k6eAd1	přímo
vizuálně	vizuálně	k6eAd1	vizuálně
zobrazeny	zobrazit	k5eAaPmNgFnP	zobrazit
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
umístění	umístění	k1gNnSc4	umístění
exoplanety	exoplanet	k1gInPc4	exoplanet
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
odhadovat	odhadovat	k5eAaImF	odhadovat
<g/>
"	"	kIx"	"
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pohybu	pohyb	k1gInSc2	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
obíhaly	obíhat	k5eAaImAgInP	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2005	[number]	k4	2005
Spitzer	Spitzer	k1gMnSc1	Spitzer
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězda	hvězda	k1gFnSc1	hvězda
Cohen-kuhi	Cohenuh	k1gFnSc2	Cohen-kuh
Tau	tau	k1gNnSc2	tau
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
měla	mít	k5eAaImAgFnS	mít
planetární	planetární	k2eAgInSc4d1	planetární
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
menší	malý	k2eAgFnPc4d2	menší
a	a	k8xC	a
mladší	mladý	k2eAgFnPc4d2	mladší
než	než	k8xS	než
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
poznání	poznání	k1gNnSc3	poznání
o	o	k7c6	o
tvoření	tvoření	k1gNnSc6	tvoření
planetárních	planetární	k2eAgInPc2d1	planetární
disků	disk	k1gInPc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
provozního	provozní	k2eAgInSc2d1	provozní
času	čas	k1gInSc2	čas
dalekohledu	dalekohled	k1gInSc2	dalekohled
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
pro	pro	k7c4	pro
účastnické	účastnický	k2eAgFnPc4d1	účastnická
instituce	instituce	k1gFnPc4	instituce
a	a	k8xC	a
hlavní	hlavní	k2eAgInPc4d1	hlavní
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
astronomové	astronom	k1gMnPc1	astronom
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
možnost	možnost	k1gFnSc4	možnost
předložit	předložit	k5eAaPmF	předložit
vlastní	vlastní	k2eAgInSc4d1	vlastní
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
konkrétní	konkrétní	k2eAgNnPc4d1	konkrétní
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
cíle	cíl	k1gInPc4	cíl
patří	patřit	k5eAaImIp3nP	patřit
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
galaxie	galaxie	k1gFnPc1	galaxie
a	a	k8xC	a
rodící	rodící	k2eAgFnPc1d1	rodící
se	se	k3xPyFc4	se
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
získané	získaný	k2eAgInPc1d1	získaný
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
jsou	být	k5eAaImIp3nP	být
volně	volně	k6eAd1	volně
dostupné	dostupný	k2eAgInPc1d1	dostupný
pro	pro	k7c4	pro
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
i	i	k8xC	i
novinářské	novinářský	k2eAgInPc4d1	novinářský
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spitzer	Spitzer	k1gInSc1	Spitzer
objevil	objevit	k5eAaPmAgInS	objevit
slabé	slabý	k2eAgFnPc4d1	slabá
a	a	k8xC	a
velice	velice	k6eAd1	velice
žhavé	žhavý	k2eAgNnSc1d1	žhavé
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Schopnosti	schopnost	k1gFnPc1	schopnost
dalekohledu	dalekohled	k1gInSc2	dalekohled
byly	být	k5eAaImAgFnP	být
také	také	k6eAd1	také
otestovány	otestovat	k5eAaPmNgFnP	otestovat
na	na	k7c6	na
plynném	plynný	k2eAgNnSc6d1	plynné
jádru	jádro	k1gNnSc6	jádro
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
L	L	kA	L
<g/>
1014	[number]	k4	1014
<g/>
,	,	kIx,	,
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
650	[number]	k4	650
ly	ly	k?	ly
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
pozemským	pozemský	k2eAgInPc3d1	pozemský
dalekohledům	dalekohled	k1gInPc3	dalekohled
a	a	k8xC	a
dalekohledu	dalekohled	k1gInSc3	dalekohled
ISO	ISO	kA	ISO
jevilo	jevit	k5eAaImAgNnS	jevit
jako	jako	k8xC	jako
úplně	úplně	k6eAd1	úplně
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Pokročilá	pokročilý	k2eAgFnSc1d1	pokročilá
technologie	technologie	k1gFnSc1	technologie
Spitzeru	Spitzer	k1gInSc2	Spitzer
odhalila	odhalit	k5eAaPmAgFnS	odhalit
uprostřed	uprostřed	k7c2	uprostřed
tohoto	tento	k3xDgInSc2	tento
objektu	objekt	k1gInSc2	objekt
jasnou	jasný	k2eAgFnSc7d1	jasná
<g/>
,	,	kIx,	,
červeně	červeně	k6eAd1	červeně
zářící	zářící	k2eAgFnSc4d1	zářící
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Texaské	texaský	k2eAgFnSc2d1	texaská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Austinu	Austin	k1gInSc6	Austin
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hvězdu	hvězda	k1gFnSc4	hvězda
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
vývojovém	vývojový	k2eAgNnSc6d1	vývojové
stádiu	stádium	k1gNnSc6	stádium
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
okolní	okolní	k2eAgInPc4d1	okolní
plyny	plyn	k1gInPc4	plyn
a	a	k8xC	a
prach	prach	k1gInSc1	prach
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bližším	blízký	k2eAgInSc6d2	bližší
průzkumu	průzkum	k1gInSc6	průzkum
daného	daný	k2eAgNnSc2d1	dané
místa	místo	k1gNnSc2	místo
dalekohledy	dalekohled	k1gInPc4	dalekohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
bylo	být	k5eAaImAgNnS	být
zachyceno	zachycen	k2eAgNnSc1d1	zachyceno
slabé	slabý	k2eAgNnSc1d1	slabé
záření	záření	k1gNnSc1	záření
vějířovitého	vějířovitý	k2eAgInSc2d1	vějířovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
zjistili	zjistit	k5eAaPmAgMnP	zjistit
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Wisconsinské	wisconsinský	k2eAgFnSc2d1	Wisconsinská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Madisonu	Madison	k1gInSc6	Madison
a	a	k8xC	a
Whitewateru	Whitewater	k1gInSc6	Whitewater
na	na	k7c6	na
základě	základ	k1gInSc6	základ
čtyř	čtyři	k4xCgNnPc2	čtyři
set	sto	k4xCgNnPc2	sto
hodin	hodina	k1gFnPc2	hodina
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
,	,	kIx,	,
že	že	k8xS	že
galaxie	galaxie	k1gFnSc1	galaxie
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
původním	původní	k2eAgMnPc3d1	původní
předpokladům	předpoklad	k1gInPc3	předpoklad
spirální	spirální	k2eAgInPc4d1	spirální
galaxií	galaxie	k1gFnSc7	galaxie
s	s	k7c7	s
příčkou	příčka	k1gFnSc7	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2006	[number]	k4	2006
astronomové	astronom	k1gMnPc1	astronom
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
našli	najít	k5eAaPmAgMnP	najít
mlhovinu	mlhovina	k1gFnSc4	mlhovina
nacházející	nacházející	k2eAgFnSc4d1	nacházející
se	se	k3xPyFc4	se
asi	asi	k9	asi
300	[number]	k4	300
ly	ly	k?	ly
od	od	k7c2	od
obří	obří	k2eAgFnSc2d1	obří
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
Double	double	k2eAgFnSc1d1	double
Helix	Helix	k1gInSc1	Helix
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stočená	stočený	k2eAgFnSc1d1	stočená
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
dvojité	dvojitý	k2eAgFnSc2d1	dvojitá
spirály	spirála	k1gFnSc2	spirála
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
interpretován	interpretovat	k5eAaBmNgInS	interpretovat
jako	jako	k9	jako
důkaz	důkaz	k1gInSc1	důkaz
silných	silný	k2eAgFnPc2d1	silná
magnetických	magnetický	k2eAgFnPc2d1	magnetická
polí	pole	k1gFnPc2	pole
vytvářených	vytvářený	k2eAgFnPc2d1	vytvářená
plynným	plynný	k2eAgInSc7d1	plynný
diskem	disk	k1gInSc7	disk
obíhajícím	obíhající	k2eAgInSc7d1	obíhající
kolem	kolem	k7c2	kolem
obří	obří	k2eAgFnSc2d1	obří
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
300	[number]	k4	300
ly	ly	k?	ly
od	od	k7c2	od
mlhoviny	mlhovina	k1gFnSc2	mlhovina
a	a	k8xC	a
25	[number]	k4	25
tisíc	tisíc	k4xCgInSc4	tisíc
ly	ly	k?	ly
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
Alexanderem	Alexandero	k1gNnSc7	Alexandero
Kashlinskym	Kashlinskymum	k1gNnPc2	Kashlinskymum
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
Matherem	Mather	k1gMnSc7	Mather
učiněn	učiněn	k2eAgInSc4d1	učiněn
další	další	k2eAgInSc4d1	další
objev	objev	k1gInSc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
Oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zachyceno	zachycen	k2eAgNnSc1d1	zachyceno
světlo	světlo	k1gNnSc1	světlo
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
<g/>
,	,	kIx,	,
doposud	doposud	k6eAd1	doposud
známých	známý	k2eAgFnPc2d1	známá
hvězd	hvězda	k1gFnPc2	hvězda
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Obrázek	obrázek	k1gInSc1	obrázek
kvasaru	kvasar	k1gInSc2	kvasar
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Draka	drak	k1gMnSc2	drak
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
sloužit	sloužit	k5eAaImF	sloužit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
kalibraci	kalibrace	k1gFnSc4	kalibrace
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyzařoval	vyzařovat	k5eAaImAgMnS	vyzařovat
infračervené	infračervený	k2eAgNnSc4d1	infračervené
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
značně	značně	k6eAd1	značně
vyniklo	vyniknout	k5eAaPmAgNnS	vyniknout
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
světla	světlo	k1gNnSc2	světlo
ze	z	k7c2	z
známých	známý	k2eAgInPc2d1	známý
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Kashlinsky	Kashlinsky	k6eAd1	Kashlinsky
a	a	k8xC	a
Mather	Mathra	k1gFnPc2	Mathra
jsou	být	k5eAaImIp3nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
početné	početný	k2eAgFnPc4d1	početná
barevné	barevný	k2eAgFnPc4d1	barevná
(	(	kIx(	(
<g/>
oranžové	oranžový	k2eAgFnPc4d1	oranžová
a	a	k8xC	a
červené	červený	k2eAgFnPc4d1	červená
<g/>
)	)	kIx)	)
skvrny	skvrna	k1gFnPc4	skvrna
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
záření	záření	k1gNnSc6	záření
jsou	být	k5eAaImIp3nP	být
světla	světlo	k1gNnPc1	světlo
hvězd	hvězda	k1gFnPc2	hvězda
zformovaných	zformovaný	k2eAgFnPc2d1	zformovaná
v	v	k7c6	v
období	období	k1gNnSc6	období
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
rozpínání	rozpínání	k1gNnSc2	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
značný	značný	k2eAgInSc1d1	značný
červený	červený	k2eAgInSc1d1	červený
posuv	posuv	k1gInSc1	posuv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
také	také	k9	také
úspěšně	úspěšně	k6eAd1	úspěšně
zmapována	zmapován	k2eAgFnSc1d1	zmapována
teplota	teplota	k1gFnSc1	teplota
atmosféry	atmosféra	k1gFnSc2	atmosféra
exoplanety	exoplanet	k1gInPc1	exoplanet
HD	HD	kA	HD
189733	[number]	k4	189733
b	b	k?	b
(	(	kIx(	(
<g/>
mateřská	mateřský	k2eAgFnSc1d1	mateřská
hvězda	hvězda	k1gFnSc1	hvězda
HD	HD	kA	HD
189733	[number]	k4	189733
<g/>
)	)	kIx)	)
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
63	[number]	k4	63
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
získána	získat	k5eAaPmNgFnS	získat
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
mapa	mapa	k1gFnSc1	mapa
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
plynného	plynný	k2eAgMnSc4d1	plynný
obra	obr	k1gMnSc4	obr
–	–	k?	–
tzv.	tzv.	kA	tzv.
horkého	horký	k2eAgMnSc4d1	horký
Jupitera	Jupiter	k1gMnSc4	Jupiter
<g/>
,	,	kIx,	,
u	u	k7c2	u
nějž	jenž	k3xRgNnSc2	jenž
byla	být	k5eAaImAgFnS	být
zmapována	zmapován	k2eAgFnSc1d1	zmapována
teplota	teplota	k1gFnSc1	teplota
plynných	plynný	k2eAgFnPc2d1	plynná
mračen	mračna	k1gFnPc2	mračna
<g/>
,	,	kIx,	,
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
svrchních	svrchní	k2eAgFnPc6d1	svrchní
částech	část	k1gFnPc6	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
HD	HD	kA	HD
189733	[number]	k4	189733
b	b	k?	b
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
svou	svůj	k3xOyFgFnSc4	svůj
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
hvězdu	hvězda	k1gFnSc4	hvězda
jednou	jednou	k6eAd1	jednou
za	za	k7c2	za
2,2	[number]	k4	2,2
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
planeta	planeta	k1gFnSc1	planeta
má	mít	k5eAaImIp3nS	mít
vůči	vůči	k7c3	vůči
ní	on	k3xPp3gFnSc3	on
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
k	k	k7c3	k
hvězdě	hvězda	k1gFnSc3	hvězda
natočena	natočen	k2eAgFnSc1d1	natočena
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc4d1	stejná
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
"	"	kIx"	"
<g/>
horká	horký	k2eAgFnSc1d1	horká
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nejteplejší	teplý	k2eAgFnSc4d3	nejteplejší
oblast	oblast	k1gFnSc4	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ke	k	k7c3	k
hvězdě	hvězda	k1gFnSc3	hvězda
sklon	sklon	k1gInSc1	sklon
30	[number]	k4	30
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
tmavé	tmavý	k2eAgFnSc2d1	tmavá
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
650	[number]	k4	650
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
strany	strana	k1gFnSc2	strana
činí	činit	k5eAaImIp3nS	činit
930	[number]	k4	930
°	°	k?	°
<g/>
C.	C.	kA	C.
Tento	tento	k3xDgInSc4	tento
nepříliš	příliš	k6eNd1	příliš
velký	velký	k2eAgInSc4d1	velký
rozdíl	rozdíl	k1gInSc4	rozdíl
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
působením	působení	k1gNnSc7	působení
silných	silný	k2eAgInPc2d1	silný
větrů	vítr	k1gInPc2	vítr
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rychlostí	rychlost	k1gFnSc7	rychlost
blížících	blížící	k2eAgFnPc2d1	blížící
se	s	k7c7	s
9700	[number]	k4	9700
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
teplou	teplý	k2eAgFnSc4d1	teplá
skvrnu	skvrna	k1gFnSc4	skvrna
postupně	postupně	k6eAd1	postupně
tlačí	tlačit	k5eAaImIp3nP	tlačit
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
rozdíly	rozdíl	k1gInPc1	rozdíl
teplot	teplota	k1gFnPc2	teplota
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
GLIMPSE	GLIMPSE	kA	GLIMPSE
(	(	kIx(	(
<g/>
Galactic	Galactice	k1gFnPc2	Galactice
Legacy	Legaca	k1gFnSc2	Legaca
Infrared	Infrared	k1gMnSc1	Infrared
Mid-Plane	Mid-Plan	k1gMnSc5	Mid-Plan
Survey	Survey	k1gInPc1	Survey
Extraordinaire	Extraordinair	k1gMnSc5	Extraordinair
<g/>
,	,	kIx,	,
glimpse	glimpse	k6eAd1	glimpse
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
zároveň	zároveň	k6eAd1	zároveň
znamená	znamenat	k5eAaImIp3nS	znamenat
záblesk	záblesk	k1gInSc1	záblesk
<g/>
,	,	kIx,	,
letmý	letmý	k2eAgInSc1d1	letmý
pohled	pohled	k1gInSc1	pohled
<g/>
,	,	kIx,	,
mihnutí	mihnutí	k1gNnSc1	mihnutí
<g/>
,	,	kIx,	,
tušení	tušení	k1gNnSc1	tušení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
průzkum	průzkum	k1gInSc4	průzkum
prováděný	prováděný	k2eAgInSc4d1	prováděný
Spitzerovým	Spitzerův	k2eAgInSc7d1	Spitzerův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
snímá	snímat	k5eAaImIp3nS	snímat
300	[number]	k4	300
stupňovou	stupňový	k2eAgFnSc4d1	stupňová
plochu	plocha	k1gFnSc4	plocha
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
části	část	k1gFnSc2	část
galaxie	galaxie	k1gFnSc2	galaxie
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
infračerveném	infračervený	k2eAgInSc6d1	infračervený
oboru	obor	k1gInSc6	obor
spektra	spektrum	k1gNnSc2	spektrum
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
kamery	kamera	k1gFnSc2	kamera
IRAC	IRAC	kA	IRAC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
bylo	být	k5eAaImAgNnS	být
pořízeno	pořídit	k5eAaPmNgNnS	pořídit
celkem	celkem	k6eAd1	celkem
444	[number]	k4	444
000	[number]	k4	000
fotografií	fotografia	k1gFnPc2	fotografia
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
různých	různý	k2eAgFnPc6d1	různá
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zkatalogizováno	zkatalogizovat	k5eAaPmNgNnS	zkatalogizovat
72	[number]	k4	72
miliónů	milión	k4xCgInPc2	milión
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
průzkumu	průzkum	k1gInSc2	průzkum
GLIMPSE3D	GLIMPSE3D	k1gMnSc2	GLIMPSE3D
survey	survea	k1gMnSc2	survea
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zkatalogizováno	zkatalogizovat	k5eAaPmNgNnS	zkatalogizovat
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
fotografií	fotografia	k1gFnPc2	fotografia
byla	být	k5eAaImAgFnS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
obrovská	obrovský	k2eAgFnSc1d1	obrovská
mozaika	mozaika	k1gFnSc1	mozaika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vědcům	vědec	k1gMnPc3	vědec
pomůže	pomoct	k5eAaPmIp3nS	pomoct
lépe	dobře	k6eAd2	dobře
objasnit	objasnit	k5eAaPmF	objasnit
strukturu	struktura	k1gFnSc4	struktura
středu	střed	k1gInSc2	střed
galaxie	galaxie	k1gFnSc2	galaxie
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
spirálních	spirální	k2eAgNnPc2d1	spirální
ramen	rameno	k1gNnPc2	rameno
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průzkumu	průzkum	k1gInSc2	průzkum
GLIMPSE	GLIMPSE	kA	GLIMPSE
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
zkatalogizováno	zkatalogizovat	k5eAaPmNgNnS	zkatalogizovat
na	na	k7c4	na
20	[number]	k4	20
000	[number]	k4	000
"	"	kIx"	"
<g/>
červených	červený	k2eAgInPc2d1	červený
<g/>
"	"	kIx"	"
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
75	[number]	k4	75
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
rodící	rodící	k2eAgFnPc1d1	rodící
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
25	[number]	k4	25
%	%	kIx~	%
hvězdy	hvězda	k1gFnPc1	hvězda
již	již	k6eAd1	již
rozvinuté	rozvinutý	k2eAgFnPc1d1	rozvinutá
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
300	[number]	k4	300
výtrysků	výtrysk	k1gInPc2	výtrysk
hmoty	hmota	k1gFnSc2	hmota
z	z	k7c2	z
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
rodících	rodící	k2eAgFnPc2d1	rodící
masivních	masivní	k2eAgFnPc2d1	masivní
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
59	[number]	k4	59
nových	nový	k2eAgFnPc2d1	nová
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
MIPSGAL	MIPSGAL	kA	MIPSGAL
je	být	k5eAaImIp3nS	být
obdobný	obdobný	k2eAgInSc1d1	obdobný
průzkum	průzkum	k1gInSc1	průzkum
snímající	snímající	k2eAgInSc1d1	snímající
278	[number]	k4	278
<g/>
°	°	k?	°
galaktického	galaktický	k2eAgInSc2d1	galaktický
disku	disk	k1gInSc2	disk
v	v	k7c6	v
delších	dlouhý	k2eAgFnPc6d2	delší
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
212	[number]	k4	212
<g/>
.	.	kIx.	.
konferenci	konference	k1gFnSc6	konference
American	Americana	k1gFnPc2	Americana
Astronomical	Astronomical	k1gFnSc2	Astronomical
Society	societa	k1gFnSc2	societa
(	(	kIx(	(
<g/>
Americká	americký	k2eAgFnSc1d1	americká
astronomická	astronomický	k2eAgFnSc1d1	astronomická
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
v	v	k7c4	v
Saint	Saint	k1gInSc4	Saint
Louis	louis	k1gInSc2	louis
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Missouri	Missouri	k1gFnSc2	Missouri
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zveřejněn	zveřejněn	k2eAgInSc1d1	zveřejněn
zatím	zatím	k6eAd1	zatím
nejdetailnější	detailní	k2eAgInSc1d3	nejdetailnější
infračervený	infračervený	k2eAgInSc1d1	infračervený
portrét	portrét	k1gInSc1	portrét
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
spojením	spojení	k1gNnSc7	spojení
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
samostatných	samostatný	k2eAgFnPc2d1	samostatná
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
vyšla	vyjít	k5eAaPmAgFnS	vyjít
tisková	tiskový	k2eAgFnSc1d1	tisková
zpráva	zpráva	k1gFnSc1	zpráva
oznamující	oznamující	k2eAgInSc1d1	oznamující
objev	objev	k1gInSc1	objev
velice	velice	k6eAd1	velice
aktivního	aktivní	k2eAgNnSc2d1	aktivní
rodiště	rodiště	k1gNnSc2	rodiště
nových	nový	k2eAgFnPc2d1	nová
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Baby	baby	k1gNnSc1	baby
boom	boom	k1gInSc1	boom
galaxy	galax	k1gInPc1	galax
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velice	velice	k6eAd1	velice
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
galaxie	galaxie	k1gFnSc1	galaxie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
4000	[number]	k4	4000
hvězd	hvězda	k1gFnPc2	hvězda
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
vzniká	vznikat	k5eAaImIp3nS	vznikat
ročně	ročně	k6eAd1	ročně
asi	asi	k9	asi
10	[number]	k4	10
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
intenzitou	intenzita	k1gFnSc7	intenzita
vzniku	vznik	k1gInSc2	vznik
hvězd	hvězda	k1gFnPc2	hvězda
stačilo	stačit	k5eAaBmAgNnS	stačit
této	tento	k3xDgFnSc6	tento
galaxii	galaxie	k1gFnSc6	galaxie
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kosmickém	kosmický	k2eAgNnSc6d1	kosmické
měřítku	měřítko	k1gNnSc6	měřítko
velmi	velmi	k6eAd1	velmi
krátká	krátký	k2eAgFnSc1d1	krátká
doba	doba	k1gFnSc1	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
největším	veliký	k2eAgFnPc3d3	veliký
galaxiím	galaxie	k1gFnPc3	galaxie
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc4	jaký
známe	znát	k5eAaImIp1nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
objevu	objev	k1gInSc6	objev
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
Spitzerův	Spitzerův	k2eAgInSc1d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
tak	tak	k6eAd1	tak
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
rodiště	rodiště	k1gNnSc2	rodiště
hvězd	hvězda	k1gFnPc2	hvězda
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
odporuje	odporovat	k5eAaImIp3nS	odporovat
uznávané	uznávaný	k2eAgFnSc3d1	uznávaná
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
hierarchickému	hierarchický	k2eAgInSc3d1	hierarchický
modelu	model	k1gInSc3	model
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
galaxie	galaxie	k1gFnSc1	galaxie
tvoří	tvořit	k5eAaImIp3nS	tvořit
velmi	velmi	k6eAd1	velmi
pozvolna	pozvolna	k6eAd1	pozvolna
pohlcováním	pohlcování	k1gNnSc7	pohlcování
částí	část	k1gFnPc2	část
menších	malý	k2eAgFnPc2d2	menší
galaxií	galaxie	k1gFnPc2	galaxie
–	–	k?	–
tedy	tedy	k8xC	tedy
ne	ne	k9	ne
tak	tak	k6eAd1	tak
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
"	"	kIx"	"
<g/>
explozivně	explozivně	k6eAd1	explozivně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
nově	nově	k6eAd1	nově
objevené	objevený	k2eAgFnSc2d1	objevená
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Capak	Capak	k1gMnSc1	Capak
ze	z	k7c2	z
Spitzer	Spitzero	k1gNnPc2	Spitzero
Science	Science	k1gFnSc2	Science
Center	centrum	k1gNnPc2	centrum
řekl	říct	k5eAaPmAgInS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tato	tento	k3xDgFnSc1	tento
galaxie	galaxie	k1gFnSc1	galaxie
nyní	nyní	k6eAd1	nyní
zažívá	zažívat	k5eAaImIp3nS	zažívat
období	období	k1gNnSc3	období
největšího	veliký	k2eAgNnSc2d3	veliký
'	'	kIx"	'
<g/>
baby	baby	k1gNnSc2	baby
boomu	boom	k1gInSc2	boom
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
většina	většina	k1gFnSc1	většina
jejích	její	k3xOp3gFnPc2	její
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Objevená	objevený	k2eAgFnSc1d1	objevená
galaxie	galaxie	k1gFnSc1	galaxie
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
tzv.	tzv.	kA	tzv.
starburst	starburst	k1gInSc4	starburst
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
hvězdy	hvězda	k1gFnPc1	hvězda
vznikají	vznikat	k5eAaImIp3nP	vznikat
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
než	než	k8xS	než
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
galaxií	galaxie	k1gFnPc2	galaxie
stejné	stejný	k2eAgFnSc2d1	stejná
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejzářivější	zářivý	k2eAgFnSc1d3	nejzářivější
starburst	starburst	k1gFnSc1	starburst
galaxií	galaxie	k1gFnPc2	galaxie
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
vesmíru	vesmír	k1gInSc2	vesmír
–	–	k?	–
vysoká	vysoký	k2eAgFnSc1d1	vysoká
zářivost	zářivost	k1gFnSc1	zářivost
indikuje	indikovat	k5eAaBmIp3nS	indikovat
překotné	překotný	k2eAgNnSc4d1	překotné
vznikání	vznikání	k1gNnSc4	vznikání
nových	nový	k2eAgFnPc2d1	nová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnPc1	první
spatřily	spatřit	k5eAaPmAgFnP	spatřit
tuto	tento	k3xDgFnSc4	tento
galaxii	galaxie	k1gFnSc4	galaxie
Hubbleův	Hubbleův	k2eAgInSc4d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
dalekohled	dalekohled	k1gInSc4	dalekohled
a	a	k8xC	a
japonský	japonský	k2eAgInSc4d1	japonský
Subaru	Subar	k1gMnSc3	Subar
Telescope	Telescop	k1gInSc5	Telescop
na	na	k7c6	na
havajské	havajský	k2eAgFnSc6d1	Havajská
hoře	hora	k1gFnSc6	hora
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
<g/>
.	.	kIx.	.
</s>
<s>
Galaxii	galaxie	k1gFnSc3	galaxie
však	však	k9	však
pozorovaly	pozorovat	k5eAaImAgFnP	pozorovat
ve	v	k7c6	v
viditelném	viditelný	k2eAgInSc6d1	viditelný
oboru	obor	k1gInSc6	obor
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
obrovské	obrovský	k2eAgFnSc3d1	obrovská
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
nešlo	jít	k5eNaImAgNnS	jít
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
její	její	k3xOp3gInSc4	její
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
do	do	k7c2	do
pozorování	pozorování	k1gNnSc2	pozorování
zapojil	zapojit	k5eAaPmAgInS	zapojit
i	i	k9	i
Spitzerův	Spitzerův	k2eAgInSc1d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
a	a	k8xC	a
James	James	k1gInSc1	James
Clerk	Clerk	k1gInSc1	Clerk
Maxwell	maxwell	k1gInSc1	maxwell
Telescope	Telescop	k1gInSc5	Telescop
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgMnPc4d1	pracující
v	v	k7c6	v
infračervených	infračervený	k2eAgFnPc6d1	infračervená
a	a	k8xC	a
submilimetrových	submilimetrový	k2eAgFnPc6d1	submilimetrová
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
<g/>
,	,	kIx,	,
vynikla	vyniknout	k5eAaPmAgFnS	vyniknout
jako	jako	k9	jako
nejzářivější	zářivý	k2eAgInSc4d3	nejzářivější
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
skupiny	skupina	k1gFnSc2	skupina
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
světle	světlo	k1gNnSc6	světlo
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
vidět	vidět	k5eAaImF	vidět
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
rodící	rodící	k2eAgFnPc1d1	rodící
hvězdy	hvězda	k1gFnPc1	hvězda
produkují	produkovat	k5eAaImIp3nP	produkovat
mnoho	mnoho	k4c4	mnoho
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
silné	silný	k2eAgNnSc4d1	silné
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgInSc1d1	okolní
prach	prach	k1gInSc1	prach
záření	záření	k1gNnSc2	záření
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
a	a	k8xC	a
získanou	získaný	k2eAgFnSc4d1	získaná
energii	energie	k1gFnSc4	energie
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
galaxie	galaxie	k1gFnSc2	galaxie
infračerveným	infračervený	k2eAgInPc3d1	infračervený
senzorům	senzor	k1gInPc3	senzor
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k8xC	jako
velice	velice	k6eAd1	velice
zářivý	zářivý	k2eAgInSc4d1	zářivý
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevu	objev	k1gInSc6	objev
této	tento	k3xDgFnSc2	tento
galaxie	galaxie	k1gFnSc2	galaxie
následovala	následovat	k5eAaImAgFnS	následovat
další	další	k2eAgNnSc4d1	další
pozorování	pozorování	k1gNnSc4	pozorování
pomocí	pomocí	k7c2	pomocí
několika	několik	k4yIc2	několik
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Keckových	Keckův	k2eAgInPc2d1	Keckův
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
,	,	kIx,	,
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zjištění	zjištění	k1gNnSc2	zjištění
její	její	k3xOp3gFnSc2	její
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
na	na	k7c4	na
12,3	[number]	k4	12,3
miliardy	miliarda	k4xCgFnSc2	miliarda
světelných	světelný	k2eAgFnPc2d1	světelná
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Diamanty	diamant	k1gInPc1	diamant
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vzácnou	vzácný	k2eAgFnSc7d1	vzácná
surovinou	surovina	k1gFnSc7	surovina
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
NASA	NASA	kA	NASA
Ames	Ames	k1gInSc1	Ames
Research	Research	k1gInSc1	Research
Center	centrum	k1gNnPc2	centrum
v	v	k7c4	v
Moffett	Moffett	k2eAgInSc4d1	Moffett
Field	Field	k1gInSc4	Field
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
naprosto	naprosto	k6eAd1	naprosto
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Spitzerův	Spitzerův	k2eAgInSc1d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
byl	být	k5eAaImAgInS	být
nasazen	nasadit	k5eAaPmNgInS	nasadit
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
potvrdit	potvrdit	k5eAaPmF	potvrdit
tuto	tento	k3xDgFnSc4	tento
hypotézu	hypotéza	k1gFnSc4	hypotéza
a	a	k8xC	a
dozvědět	dozvědět	k5eAaPmF	dozvědět
se	se	k3xPyFc4	se
o	o	k7c6	o
diamantech	diamant	k1gInPc6	diamant
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gNnSc1	jeho
citlivé	citlivý	k2eAgNnSc1d1	citlivé
infračervené	infračervený	k2eAgNnSc1d1	infračervené
vybavení	vybavení	k1gNnSc1	vybavení
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
výborně	výborně	k6eAd1	výborně
hodí	hodit	k5eAaPmIp3nS	hodit
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
pomocí	pomocí	k7c2	pomocí
počítačových	počítačový	k2eAgFnPc2d1	počítačová
simulací	simulace	k1gFnPc2	simulace
vypracovali	vypracovat	k5eAaPmAgMnP	vypracovat
strategii	strategie	k1gFnSc4	strategie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
nalézt	nalézt	k5eAaPmF	nalézt
diamanty	diamant	k1gInPc4	diamant
nanometrových	nanometrový	k2eAgInPc2d1	nanometrový
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
menší	malý	k2eAgInPc4d2	menší
než	než	k8xS	než
zrnko	zrnko	k1gNnSc4	zrnko
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgInPc4	svůj
malé	malý	k2eAgInPc4d1	malý
rozměry	rozměr	k1gInPc4	rozměr
mohou	moct	k5eAaImIp3nP	moct
tyto	tento	k3xDgInPc4	tento
diamanty	diamant	k1gInPc4	diamant
být	být	k5eAaImF	být
velice	velice	k6eAd1	velice
užitečné	užitečný	k2eAgInPc1d1	užitečný
k	k	k7c3	k
hlubšímu	hluboký	k2eAgNnSc3d2	hlubší
poznání	poznání	k1gNnSc3	poznání
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
chovají	chovat	k5eAaImIp3nP	chovat
molekuly	molekula	k1gFnPc1	molekula
bohaté	bohatý	k2eAgFnPc1d1	bohatá
na	na	k7c4	na
uhlík	uhlík	k1gInSc4	uhlík
<g/>
,	,	kIx,	,
základní	základní	k2eAgInSc4d1	základní
stavební	stavební	k2eAgInSc4d1	stavební
prvek	prvek	k1gInSc4	prvek
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
kosmu	kosmos	k1gInSc6	kosmos
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
diamantů	diamant	k1gInPc2	diamant
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vysloven	vyslovit	k5eAaPmNgInS	vyslovit
již	již	k6eAd1	již
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
meteoritech	meteorit	k1gInPc6	meteorit
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nanometrových	nanometrový	k2eAgInPc2d1	nanometrový
diamantů	diamant	k1gInPc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
3	[number]	k4	3
%	%	kIx~	%
uhlíku	uhlík	k1gInSc2	uhlík
obsaženého	obsažený	k2eAgInSc2d1	obsažený
v	v	k7c6	v
meteoritech	meteorit	k1gInPc6	meteorit
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
nanodiamantů	nanodiamant	k1gInPc2	nanodiamant
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
jejich	jejich	k3xOp3gNnSc4	jejich
množství	množství	k1gNnSc4	množství
nelze	lze	k6eNd1	lze
tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
pozorovat	pozorovat	k5eAaImF	pozorovat
běžnými	běžný	k2eAgInPc7d1	běžný
optickými	optický	k2eAgInPc7d1	optický
dalekohledy	dalekohled	k1gInPc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
využít	využít	k5eAaPmF	využít
jejich	jejich	k3xOp3gFnPc4	jejich
specifické	specifický	k2eAgFnPc4d1	specifická
infračervené	infračervený	k2eAgFnPc4d1	infračervená
a	a	k8xC	a
elektrické	elektrický	k2eAgFnPc4d1	elektrická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
uhlíku	uhlík	k1gInSc2	uhlík
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
jako	jako	k8xC	jako
záření	záření	k1gNnSc4	záření
infračervené	infračervený	k2eAgFnSc2d1	infračervená
–	–	k?	–
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tedy	tedy	k9	tedy
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
mnohé	mnohý	k2eAgFnPc1d1	mnohá
jiné	jiný	k2eAgFnPc1d1	jiná
molekuly	molekula	k1gFnPc1	molekula
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
specifické	specifický	k2eAgNnSc1d1	specifické
"	"	kIx"	"
<g/>
infračervené	infračervený	k2eAgInPc1d1	infračervený
otisky	otisk	k1gInPc1	otisk
prstů	prst	k1gInPc2	prst
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
nejvhodnější	vhodný	k2eAgNnSc4d3	nejvhodnější
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nanodiamanty	nanodiamanta	k1gFnPc1	nanodiamanta
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
velice	velice	k6eAd1	velice
horké	horký	k2eAgFnPc4d1	horká
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nanodiamanty	nanodiamanta	k1gFnPc1	nanodiamanta
většinou	většina	k1gFnSc7	většina
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
IR	Ir	k1gMnSc1	Ir
záření	záření	k1gNnSc2	záření
o	o	k7c6	o
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
3,4	[number]	k4	3,4
až	až	k8xS	až
3,5	[number]	k4	3,5
a	a	k8xC	a
6	[number]	k4	6
až	až	k9	až
10	[number]	k4	10
mikronů	mikron	k1gInPc2	mikron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
je	být	k5eAaImIp3nS	být
Spitzer	Spitzer	k1gInSc1	Spitzer
obzvláště	obzvláště	k6eAd1	obzvláště
citlivý	citlivý	k2eAgInSc1d1	citlivý
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
pomoci	pomoct	k5eAaPmF	pomoct
shromáždit	shromáždit	k5eAaPmF	shromáždit
potřebná	potřebný	k2eAgNnPc4d1	potřebné
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
zásob	zásoba	k1gFnPc2	zásoba
tekutého	tekutý	k2eAgNnSc2d1	tekuté
hélia	hélium	k1gNnSc2	hélium
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
Spitzer	Spitzer	k1gInSc1	Spitzer
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
teplé	teplý	k2eAgFnSc2d1	teplá
mise	mise	k1gFnSc2	mise
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Valná	valný	k2eAgFnSc1d1	valná
část	část	k1gFnSc1	část
pozorovacích	pozorovací	k2eAgNnPc2d1	pozorovací
zařízení	zařízení	k1gNnPc2	zařízení
dalekohledu	dalekohled	k1gInSc2	dalekohled
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
mimo	mimo	k7c4	mimo
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Funkčními	funkční	k2eAgInPc7d1	funkční
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
infračervené	infračervený	k2eAgFnPc1d1	infračervená
kamery	kamera	k1gFnPc1	kamera
IRAC	IRAC	kA	IRAC
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgFnSc1d1	pracující
v	v	k7c6	v
krátkých	krátký	k2eAgFnPc6d1	krátká
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
zkalibrovány	zkalibrovat	k5eAaImNgFnP	zkalibrovat
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
tepelném	tepelný	k2eAgInSc6d1	tepelný
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
první	první	k4xOgInPc1	první
snímky	snímek	k1gInPc1	snímek
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kamery	kamera	k1gFnPc1	kamera
IRAC	IRAC	kA	IRAC
podávají	podávat	k5eAaImIp3nP	podávat
dobré	dobrý	k2eAgInPc1d1	dobrý
výkony	výkon	k1gInPc1	výkon
i	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
značné	značný	k2eAgNnSc4d1	značné
zahřátí	zahřátí	k1gNnSc4	zahřátí
–	–	k?	–
podobné	podobný	k2eAgInPc1d1	podobný
snímky	snímek	k1gInPc1	snímek
by	by	kYmCp3nP	by
totiž	totiž	k9	totiž
dokázal	dokázat	k5eAaPmAgInS	dokázat
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
exponovat	exponovat	k5eAaImF	exponovat
pouze	pouze	k6eAd1	pouze
dalekohled	dalekohled	k1gInSc4	dalekohled
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
objektivu	objektiv	k1gInSc2	objektiv
třiceti	třicet	k4xCc2	třicet
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
Spitzerův	Spitzerův	k2eAgInSc1d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
objevil	objevit	k5eAaPmAgInS	objevit
nový	nový	k2eAgInSc1d1	nový
prstenec	prstenec	k1gInSc1	prstenec
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
mimořádně	mimořádně	k6eAd1	mimořádně
velký	velký	k2eAgInSc4d1	velký
prstenec	prstenec	k1gInSc4	prstenec
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc4d1	nazývaný
také	také	k9	také
prstenec	prstenec	k1gInSc4	prstenec
Phoebe	Phoeb	k1gInSc5	Phoeb
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
měsíčku	měsíček	k1gInSc2	měsíček
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
velmi	velmi	k6eAd1	velmi
řídkým	řídký	k2eAgInSc7d1	řídký
prachem	prach	k1gInSc7	prach
<g/>
.	.	kIx.	.
</s>
<s>
Studený	studený	k2eAgInSc1d1	studený
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
matný	matný	k2eAgInSc1d1	matný
na	na	k7c4	na
vizuální	vizuální	k2eAgNnPc4d1	vizuální
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaPmIp3nS	vydávat
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Spitzer	Spitzer	k1gMnSc1	Spitzer
zachytil	zachytit	k5eAaPmAgMnS	zachytit
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Nature	Natur	k1gMnSc5	Natur
článek	článek	k1gInSc1	článek
s	s	k7c7	s
informací	informace	k1gFnSc7	informace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
pozorování	pozorování	k1gNnSc2	pozorování
Spitzerova	Spitzerův	k2eAgInSc2d1	Spitzerův
dalekohledu	dalekohled	k1gInSc2	dalekohled
podařilo	podařit	k5eAaPmAgNnS	podařit
objevit	objevit	k5eAaPmF	objevit
nejstarší	starý	k2eAgInPc4d3	nejstarší
známé	známý	k2eAgInPc4d1	známý
kvasary	kvasar	k1gInPc4	kvasar
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vysoký	vysoký	k2eAgInSc1d1	vysoký
věk	věk	k1gInSc1	věk
se	se	k3xPyFc4	se
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
z	z	k7c2	z
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádný	žádný	k3yNgInSc1	žádný
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
přítomnost	přítomnost	k1gFnSc1	přítomnost
se	se	k3xPyFc4	se
v	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
stádiích	stádium	k1gNnPc6	stádium
vesmíru	vesmír	k1gInSc2	vesmír
nepředpokládá	předpokládat	k5eNaImIp3nS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
opticky	opticky	k6eAd1	opticky
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
kvasary	kvasar	k1gInPc1	kvasar
objeveny	objevit	k5eAaPmNgInP	objevit
již	již	k6eAd1	již
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
SDSS	SDSS	kA	SDSS
(	(	kIx(	(
<g/>
Sloan	Sloan	k1gInSc1	Sloan
Digital	Digital	kA	Digital
Sky	Sky	k1gMnSc2	Sky
Survey	Survea	k1gMnSc2	Survea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
teprve	teprve	k6eAd1	teprve
pozorování	pozorování	k1gNnSc1	pozorování
Spitzera	Spitzero	k1gNnSc2	Spitzero
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
těmto	tento	k3xDgInPc3	tento
kvasarům	kvasar	k1gInPc3	kvasar
chybí	chybět	k5eAaImIp3nS	chybět
tepelné	tepelný	k2eAgNnSc4d1	tepelné
záření	záření	k1gNnSc4	záření
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
dosud	dosud	k6eAd1	dosud
známé	známý	k2eAgMnPc4d1	známý
kvasary	kvasar	k1gInPc4	kvasar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
úkoly	úkol	k1gInPc4	úkol
Spitzera	Spitzero	k1gNnSc2	Spitzero
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
zvládnout	zvládnout	k5eAaPmF	zvládnout
do	do	k7c2	do
konce	konec	k1gInSc2	konec
své	svůj	k3xOyFgFnSc2	svůj
životnosti	životnost	k1gFnSc2	životnost
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
zpřesnění	zpřesnění	k1gNnSc4	zpřesnění
hodnoty	hodnota	k1gFnSc2	hodnota
Hubbleovy	Hubbleův	k2eAgFnSc2d1	Hubbleova
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
,	,	kIx,	,
hledání	hledání	k1gNnSc1	hledání
galaxií	galaxie	k1gFnPc2	galaxie
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
zpřesnění	zpřesnění	k1gNnSc4	zpřesnění
parametrů	parametr	k1gInPc2	parametr
více	hodně	k6eAd2	hodně
než	než	k8xS	než
700	[number]	k4	700
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
či	či	k8xC	či
pozorování	pozorování	k1gNnSc1	pozorování
atmosfér	atmosféra	k1gFnPc2	atmosféra
plynných	plynný	k2eAgFnPc2d1	plynná
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
objevována	objevovat	k5eAaImNgFnS	objevovat
vesmírnou	vesmírný	k2eAgFnSc7d1	vesmírná
observatoří	observatoř	k1gFnSc7	observatoř
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
.	.	kIx.	.
</s>
