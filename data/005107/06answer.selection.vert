<s>
Spitzerův	Spitzerův	k2eAgInSc1d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
či	či	k8xC	či
Space	Space	k1gMnSc1	Space
Infrared	Infrared	k1gMnSc1	Infrared
Telescope	Telescop	k1gInSc5	Telescop
Facility	Facilita	k1gFnPc4	Facilita
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
SIRTF	SIRTF	kA	SIRTF
nebo	nebo	k8xC	nebo
i	i	k9	i
Spitzer	Spitzer	k1gInSc1	Spitzer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
observatoř	observatoř	k1gFnSc1	observatoř
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
infračerveném	infračervený	k2eAgInSc6d1	infračervený
oboru	obor	k1gInSc6	obor
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
