<s>
12.	[number]	k4	12.
července	červenec	k1gInSc2	červenec
1932	[number]	k4	1932
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
zahynul	zahynout	k5eAaPmAgMnS	zahynout
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pilotem	pilot	k1gMnSc7	pilot
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Broučkem	Brouček	k1gMnSc7	Brouček
při	při	k7c6	při
letecké	letecký	k2eAgFnSc6d1	letecká
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
osobním	osobní	k2eAgNnSc6d1	osobní
letadle	letadlo	k1gNnSc6	letadlo
Junkers	Junkersa	k1gFnPc2	Junkersa
F	F	kA	F
13	[number]	k4	13
letěl	letět	k5eAaImAgMnS	letět
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
otevření	otevření	k1gNnSc4	otevření
nové	nový	k2eAgFnSc2d1	nová
pobočky	pobočka	k1gFnSc2	pobočka
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Möhlin	Möhlina	k1gFnPc2	Möhlina
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Rýna	Rýn	k1gInSc2	Rýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
přezdíván	přezdívat	k5eAaImNgInS	přezdívat
"	"	kIx"	"
<g/>
Der	drát	k5eAaImRp2nS	drát
tschechische	tschechische	k1gNnSc4	tschechische
Schuhkönig	Schuhkönig	k1gInSc1	Schuhkönig
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
