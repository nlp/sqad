<s>
Východní	východní	k2eAgFnSc1d1	východní
polokoule	polokoule	k1gFnSc1	polokoule
nebo	nebo	k8xC	nebo
východní	východní	k2eAgFnSc1d1	východní
hemisféra	hemisféra	k1gFnSc1	hemisféra
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
zeměkoule	zeměkoule	k1gFnSc1	zeměkoule
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
východně	východně	k6eAd1	východně
od	od	k7c2	od
základního	základní	k2eAgInSc2d1	základní
poledníku	poledník	k1gInSc2	poledník
a	a	k8xC	a
západně	západně	k6eAd1	západně
od	od	k7c2	od
180	[number]	k4	180
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
leží	ležet	k5eAaImIp3nS	ležet
datová	datový	k2eAgFnSc1d1	datová
hranice	hranice	k1gFnSc1	hranice
<g/>
.	.	kIx.	.
</s>
