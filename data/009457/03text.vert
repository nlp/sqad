<p>
<s>
Východní	východní	k2eAgFnSc1d1	východní
polokoule	polokoule	k1gFnSc1	polokoule
nebo	nebo	k8xC	nebo
východní	východní	k2eAgFnSc1d1	východní
hemisféra	hemisféra	k1gFnSc1	hemisféra
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
zeměkoule	zeměkoule	k1gFnSc1	zeměkoule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
východně	východně	k6eAd1	východně
od	od	k7c2	od
základního	základní	k2eAgInSc2d1	základní
poledníku	poledník	k1gInSc2	poledník
a	a	k8xC	a
západně	západně	k6eAd1	západně
od	od	k7c2	od
180	[number]	k4	180
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
leží	ležet	k5eAaImIp3nS	ležet
datová	datový	k2eAgFnSc1d1	datová
hranice	hranice	k1gFnSc1	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
celá	celá	k1gFnSc1	celá
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
zeměkoule	zeměkoule	k1gFnSc1	zeměkoule
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
západní	západní	k2eAgFnSc2d1	západní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
</p>
