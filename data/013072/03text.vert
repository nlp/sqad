<p>
<s>
Solifugy	Solifug	k1gInPc1	Solifug
Solifugae	Solifuga	k1gFnSc2	Solifuga
jsou	být	k5eAaImIp3nP	být
řádem	řád	k1gInSc7	řád
pavoukovců	pavoukovec	k1gInPc2	pavoukovec
Arachnida	Arachnid	k1gMnSc2	Arachnid
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc1d1	dosahující
velikosti	velikost	k1gFnPc1	velikost
až	až	k9	až
10	[number]	k4	10
cm	cm	kA	cm
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nápadní	nápadní	k2eAgMnPc1d1	nápadní
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
velikým	veliký	k2eAgFnPc3d1	veliká
klepítkám	klepítka	k1gFnPc3	klepítka
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
uchvacují	uchvacovat	k5eAaImIp3nP	uchvacovat
kořist	kořist	k1gFnSc1	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pavouků	pavouk	k1gMnPc2	pavouk
nemají	mít	k5eNaImIp3nP	mít
však	však	k9	však
jed	jed	k1gInSc4	jed
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
hlavohrudi	hlavohruď	k1gFnPc1	hlavohruď
prosomy	prosom	k1gInPc1	prosom
je	být	k5eAaImIp3nS	být
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
pevným	pevný	k2eAgInSc7d1	pevný
<g/>
,	,	kIx,	,
klenutým	klenutý	k2eAgInSc7d1	klenutý
krunýřem	krunýř	k1gInSc7	krunýř
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
zřetelně	zřetelně	k6eAd1	zřetelně
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
zbývající	zbývající	k2eAgFnSc2d1	zbývající
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
s	s	k7c7	s
měkkým	měkký	k2eAgInSc7d1	měkký
pokryvem	pokryv	k1gInSc7	pokryv
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tzv.	tzv.	kA	tzv.
propeltidum	propeltidum	k1gInSc1	propeltidum
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgInSc1d1	nesoucí
pár	pár	k1gInSc1	pár
dobře	dobře	k6eAd1	dobře
patrných	patrný	k2eAgNnPc2d1	patrné
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pohyblivě	pohyblivě	k6eAd1	pohyblivě
spojeno	spojit	k5eAaPmNgNnS	spojit
se	s	k7c7	s
zbývající	zbývající	k2eAgFnSc7d1	zbývající
částí	část	k1gFnSc7	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
suché	suchý	k2eAgInPc1d1	suchý
<g/>
,	,	kIx,	,
stepní	stepní	k2eAgInPc1d1	stepní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pouštní	pouštní	k2eAgFnSc2d1	pouštní
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
noční	noční	k2eAgMnPc1d1	noční
dravci	dravec	k1gMnPc1	dravec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
den	den	k1gInSc4	den
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
pod	pod	k7c4	pod
kameny	kámen	k1gInPc4	kámen
nebo	nebo	k8xC	nebo
v	v	k7c6	v
opuštěných	opuštěný	k2eAgFnPc6d1	opuštěná
norách	nora	k1gFnPc6	nora
různých	různý	k2eAgFnPc2d1	různá
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
termity	termit	k1gInPc7	termit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
větší	veliký	k2eAgInPc1d2	veliký
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
ulovit	ulovit	k5eAaPmF	ulovit
i	i	k9	i
drobnější	drobný	k2eAgMnPc4d2	drobnější
obratlovce	obratlovec	k1gMnPc4	obratlovec
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc4	jejich
kousnutí	kousnutí	k1gNnSc1	kousnutí
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
bolestivé	bolestivý	k2eAgNnSc1d1	bolestivé
<g/>
.	.	kIx.	.
</s>
<s>
Dokážou	dokázat	k5eAaPmIp3nP	dokázat
rychle	rychle	k6eAd1	rychle
běhat	běhat	k5eAaImF	běhat
a	a	k8xC	a
při	při	k7c6	při
ohrožení	ohrožení	k1gNnSc6	ohrožení
vydávat	vydávat	k5eAaImF	vydávat
pískavé	pískavý	k2eAgInPc4d1	pískavý
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Solifugy	Solifug	k1gInPc1	Solifug
jsou	být	k5eAaImIp3nP	být
agresivní	agresivní	k2eAgInPc1d1	agresivní
a	a	k8xC	a
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
obratlovců	obratlovec	k1gMnPc2	obratlovec
představují	představovat	k5eAaImIp3nP	představovat
smrtelné	smrtelný	k2eAgNnSc4d1	smrtelné
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Páření	páření	k1gNnSc1	páření
probíhá	probíhat	k5eAaImIp3nS	probíhat
u	u	k7c2	u
solifug	solifuga	k1gFnPc2	solifuga
velmi	velmi	k6eAd1	velmi
bouřlivě	bouřlivě	k6eAd1	bouřlivě
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInSc1d2	menší
samec	samec	k1gInSc1	samec
se	se	k3xPyFc4	se
vrhne	vrhnout	k5eAaPmIp3nS	vrhnout
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
samici	samice	k1gFnSc4	samice
a	a	k8xC	a
pevně	pevně	k6eAd1	pevně
ji	on	k3xPp3gFnSc4	on
uchopí	uchopit	k5eAaPmIp3nS	uchopit
svými	svůj	k3xOyFgFnPc7	svůj
chelicerami	chelicera	k1gFnPc7	chelicera
<g/>
,	,	kIx,	,
makadly	makadlo	k1gNnPc7	makadlo
a	a	k8xC	a
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
upadne	upadnout	k5eAaPmIp3nS	upadnout
do	do	k7c2	do
strnulého	strnulý	k2eAgInSc2d1	strnulý
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgNnSc2	který
samec	samec	k1gMnSc1	samec
rychle	rychle	k6eAd1	rychle
vypustí	vypustit	k5eAaPmIp3nS	vypustit
spermatofor	spermatofor	k1gInSc4	spermatofor
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
ho	on	k3xPp3gMnSc4	on
vsune	vsunout	k5eAaPmIp3nS	vsunout
do	do	k7c2	do
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
otvoru	otvor	k1gInSc2	otvor
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
samice	samice	k1gFnSc1	samice
probudí	probudit	k5eAaPmIp3nS	probudit
z	z	k7c2	z
letargie	letargie	k1gFnSc2	letargie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
klade	klást	k5eAaImIp3nS	klást
vajíčka	vajíčko	k1gNnPc4	vajíčko
do	do	k7c2	do
podzemního	podzemní	k2eAgInSc2d1	podzemní
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
,	,	kIx,	,
hlídá	hlídat	k5eAaImIp3nS	hlídat
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
vylíhnutí	vylíhnutí	k1gNnSc2	vylíhnutí
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
poté	poté	k6eAd1	poté
nosí	nosit	k5eAaImIp3nP	nosit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
solifug	solifuga	k1gFnPc2	solifuga
dochází	docházet	k5eAaImIp3nS	docházet
často	často	k6eAd1	často
k	k	k7c3	k
partnerskému	partnerský	k2eAgInSc3d1	partnerský
kanibalismu	kanibalismus	k1gInSc3	kanibalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
Solifuga	Solifuga	k1gFnSc1	Solifuga
egejská	egejský	k2eAgFnSc1d1	Egejská
Galeodes	Galeodes	k1gInSc4	Galeodes
graecus	graecus	k1gInSc1	graecus
–	–	k?	–
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
Solifuga	Solifug	k1gMnSc4	Solifug
sluneční	sluneční	k2eAgInSc1d1	sluneční
Paragaleodes	Paragaleodes	k1gInSc1	Paragaleodes
heliophilus	heliophilus	k1gInSc1	heliophilus
–	–	k?	–
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
vylézá	vylézat	k5eAaImIp3nS	vylézat
z	z	k7c2	z
úkrytů	úkryt	k1gInPc2	úkryt
i	i	k9	i
za	za	k7c2	za
dne	den	k1gInSc2	den
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Bellmann	Bellmann	k1gNnSc1	Bellmann
<g/>
,	,	kIx,	,
Heiko	Heiko	k1gNnSc1	Heiko
<g/>
.	.	kIx.	.
</s>
<s>
Pavoukovci	Pavoukovec	k1gMnPc1	Pavoukovec
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
152	[number]	k4	152
s.	s.	k?	s.
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
672	[number]	k4	672
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Solifugy	Solifuga	k1gFnSc2	Solifuga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Solifugy	Solifuga	k1gFnSc2	Solifuga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
