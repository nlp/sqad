<s>
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1724	[number]	k4	1724
Královec	Královec	k1gInSc1	Královec
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1804	[number]	k4	1804
Královec	Královec	k1gInSc1	Královec
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
evropských	evropský	k2eAgMnPc2d1	evropský
myslitelů	myslitel	k1gMnPc2	myslitel
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
představitelů	představitel	k1gMnPc2	představitel
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
