<s>
Beryllium	Beryllium	k1gNnSc1
</s>
<s>
Beryllium	Beryllium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
He	he	k0
<g/>
]	]	kIx)
2	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Be	Be	k?
</s>
<s>
4	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Beryllium	Beryllium	k1gNnSc1
<g/>
,	,	kIx,
Be	Be	k1gFnSc1
<g/>
,	,	kIx,
4	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Beryllium	Beryllium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
s	s	k7c7
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
2,8	2,8	k4
až	až	k6eAd1
10	#num#	k4
ppm	ppm	k?
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
</s>
<s>
přibližně	přibližně	k6eAd1
0,000	0,000	k4
000	#num#	k4
6	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
šedý	šedý	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-41-7	7440-41-7	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
9,012	9,012	k4
<g/>
182	#num#	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
105	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
96	#num#	k4
pm	pm	k?
</s>
<s>
Van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Waalsův	Waalsův	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
153	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
44	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
He	he	k0
<g/>
]	]	kIx)
2	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
2	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,57	1,57	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
899,5	899,5	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
1757,1	1757,1	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
14848.7	14848.7	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Šesterečná	šesterečný	k2eAgFnSc1d1
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
4,85	4,85	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
1	#num#	k4
848	#num#	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
5,5	5,5	k4
</s>
<s>
Tlak	tlak	k1gInSc1
syté	sytý	k2eAgFnSc2d1
páry	pára	k1gFnSc2
</s>
<s>
100	#num#	k4
Pa	Pa	kA
při	při	k7c6
1791K	1791K	k4
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
12	#num#	k4
870	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
200	#num#	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1	#num#	k4
287	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
560,15	560,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
2	#num#	k4
469	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
742,15	742,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
tání	tání	k1gNnSc2
</s>
<s>
10,456	10,456	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
varu	var	k1gInSc2
</s>
<s>
223,764	223,764	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
1825	#num#	k4
Jkg	Jkg	k1gFnSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
2,5	2,5	k4
<g/>
×	×	k?
<g/>
107	#num#	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
36	#num#	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
−	−	k?
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Diamagnetický	diamagnetický	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Vysoce	vysoce	k6eAd1
toxický	toxický	k2eAgMnSc1d1
(	(	kIx(
<g/>
T	T	kA
<g/>
+	+	kIx~
<g/>
)	)	kIx)
</s>
<s>
R-věty	R-věta	k1gFnPc1
</s>
<s>
R	R	kA
<g/>
49	#num#	k4
<g/>
,	,	kIx,
<g/>
R	R	kA
<g/>
25	#num#	k4
<g/>
,	,	kIx,
<g/>
R	R	kA
<g/>
26	#num#	k4
<g/>
,	,	kIx,
<g/>
R	R	kA
<g/>
48	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
,	,	kIx,
<g/>
R	R	kA
<g/>
36	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
<g/>
,	,	kIx,
<g/>
R	R	kA
<g/>
43	#num#	k4
</s>
<s>
S-věty	S-věta	k1gFnPc1
</s>
<s>
S	s	k7c7
<g/>
53	#num#	k4
<g/>
,	,	kIx,
<g/>
S	s	k7c7
<g/>
45	#num#	k4
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
7	#num#	k4
<g/>
Be	Be	k1gFnSc1
</s>
<s>
stopově	stopově	k6eAd1
</s>
<s>
53,12	53,12	k4
dne	den	k1gInSc2
</s>
<s>
ε	ε	k?
</s>
<s>
0,861	0,861	k4
89	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
Li	li	k8xS
</s>
<s>
γ	γ	k?
</s>
<s>
0,477	0,477	k4
</s>
<s>
–	–	k?
</s>
<s>
8	#num#	k4
<g/>
Be	Be	k1gFnSc1
</s>
<s>
stopově	stopově	k6eAd1
</s>
<s>
7	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
17	#num#	k4
<g/>
s	s	k7c7
</s>
<s>
α	α	k?
</s>
<s>
4	#num#	k4
<g/>
He	he	k0
</s>
<s>
9	#num#	k4
<g/>
Be	Be	k1gFnSc1
</s>
<s>
100	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
5	#num#	k4
neutrony	neutron	k1gInPc7
</s>
<s>
10	#num#	k4
<g/>
Be	Be	k1gFnSc1
</s>
<s>
stopově	stopově	k6eAd1
</s>
<s>
1,51	1,51	k4
<g/>
x	x	k?
<g/>
106	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
0,55688	0,55688	k4
</s>
<s>
10B	10B	k4
</s>
<s>
11	#num#	k4
<g/>
Be	Be	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
13,76	13,76	k4
s	s	k7c7
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
11,509	11,509	k4
<g/>
46	#num#	k4
</s>
<s>
11B	11B	k4
</s>
<s>
12	#num#	k4
<g/>
Be	Be	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
21,3	21,3	k4
ms	ms	k?
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
>	>	kIx)
<g/>
99	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
11,708	11,708	k4
<g/>
4	#num#	k4
</s>
<s>
12B	12B	k4
</s>
<s>
β	β	k?
<g/>
−	−	k?
<g/>
n	n	k0
</s>
<s>
<	<	kIx(
<g/>
1	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
3,170	3,170	k4
<g/>
7	#num#	k4
</s>
<s>
11B	11B	k4
</s>
<s>
14	#num#	k4
<g/>
Be	Be	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
4,35	4,35	k4
ms	ms	k?
</s>
<s>
β	β	k?
<g/>
−	−	k?
n	n	k0
</s>
<s>
99,996	99,996	k4
<g/>
%	%	kIx~
</s>
<s>
18,07	18,07	k4
</s>
<s>
13B	13B	k4
</s>
<s>
β	β	k?
<g/>
−	−	k?
α	α	k?
0,004	0,004	k4
<g/>
%	%	kIx~
</s>
<s>
27,96	27,96	k4
</s>
<s>
10	#num#	k4
<g/>
Li	li	k8xS
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Lithium	lithium	k1gNnSc1
≺	≺	k?
<g/>
Be	Be	k1gMnSc1
<g/>
≻	≻	k?
Bor	bor	k1gInSc1
</s>
<s>
⋎	⋎	k?
<g/>
Mg	mg	kA
</s>
<s>
Beryllium	Beryllium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Be	Be	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Beryllium	Beryllium	k1gNnSc1
<g/>
)	)	kIx)
někdy	někdy	k6eAd1
také	také	k9
berylium	berylium	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejlehčím	lehký	k2eAgFnPc3d3
z	z	k7c2
řady	řada	k1gFnSc2
kovů	kov	k1gInPc2
s	s	k7c7
<g/>
2	#num#	k4
<g/>
,	,	kIx,
tvrdý	tvrdý	k2eAgInSc4d1
<g/>
,	,	kIx,
šedý	šedý	k2eAgInSc4d1
kov	kov	k1gInSc4
o	o	k7c6
značně	značně	k6eAd1
vysoké	vysoký	k2eAgFnSc6d1
teplotě	teplota	k1gFnSc6
tání	tání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
propouští	propouštět	k5eAaImIp3nS
rentgenové	rentgenový	k2eAgNnSc1d1
záření	záření	k1gNnSc1
a	a	k8xC
radioaktivní	radioaktivní	k2eAgNnSc1d1
záření	záření	k1gNnSc1
gama	gama	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
soli	sůl	k1gFnSc2
jsou	být	k5eAaImIp3nP
mimořádně	mimořádně	k6eAd1
toxické	toxický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
kov	kov	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
toxický	toxický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
beryllium	beryllium	k1gNnSc1
</s>
<s>
Beryllium	Beryllium	k1gNnSc1
je	být	k5eAaImIp3nS
tvrdý	tvrdý	k2eAgMnSc1d1
(	(	kIx(
<g/>
rýpe	rýpat	k5eAaImIp3nS
do	do	k7c2
skla	sklo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
křehký	křehký	k2eAgInSc1d1
(	(	kIx(
<g/>
za	za	k7c4
normální	normální	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
poměrně	poměrně	k6eAd1
těžce	těžce	k6eAd1
tavitelný	tavitelný	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
červeného	červený	k2eAgInSc2d1
žáru	žár	k1gInSc2
je	být	k5eAaImIp3nS
beryllium	beryllium	k1gNnSc1
tažné	tažný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vede	vést	k5eAaImIp3nS
špatně	špatně	k6eAd1
elektrický	elektrický	k2eAgInSc4d1
proud	proud	k1gInSc4
a	a	k8xC
teplo	teplo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
supravodičem	supravodič	k1gInSc7
I.	I.	kA
typu	typ	k1gInSc2
za	za	k7c2
teplot	teplota	k1gFnPc2
pod	pod	k7c4
0,26	0,26	k4
K.	K.	kA
Práškovité	práškovitý	k2eAgNnSc1d1
beryllium	beryllium	k1gNnSc1
vyvolává	vyvolávat	k5eAaImIp3nS
kožní	kožní	k2eAgInPc4d1
ekzémy	ekzém	k1gInPc4
a	a	k8xC
poškozuje	poškozovat	k5eAaImIp3nS
dýchací	dýchací	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elementární	elementární	k2eAgFnSc1d1
kovové	kovový	k2eAgNnSc4d1
beryllium	beryllium	k1gNnSc4
lze	lze	k6eAd1
dlouhodobě	dlouhodobě	k6eAd1
uchovávat	uchovávat	k5eAaImF
např.	např.	kA
překryté	překrytý	k2eAgFnSc2d1
vrstvou	vrstva	k1gFnSc7
alifatických	alifatický	k2eAgInPc2d1
uhlovodíků	uhlovodík	k1gInPc2
jako	jako	k8xS,k8xC
petrolej	petrolej	k1gInSc1
nebo	nebo	k8xC
nafta	nafta	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
nereaguje	reagovat	k5eNaBmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Beryllium	Beryllium	k1gNnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
suchém	suchý	k2eAgInSc6d1
vzduchu	vzduch	k1gInSc6
stabilní	stabilní	k2eAgFnPc1d1
(	(	kIx(
<g/>
nereaguje	reagovat	k5eNaBmIp3nS
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
za	za	k7c4
pokojové	pokojový	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vodou	voda	k1gFnSc7
reaguje	reagovat	k5eAaBmIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
povrchu	povrch	k1gInSc6
a	a	k8xC
pokrývá	pokrývat	k5eAaImIp3nS
se	se	k3xPyFc4
tenkou	tenký	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
oxidu	oxid	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
<g/>
,	,	kIx,
oxidace	oxidace	k1gFnSc1
dále	daleko	k6eAd2
za	za	k7c4
normální	normální	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
neprobíhá	probíhat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
setkáváme	setkávat	k5eAaImIp1nP
pouze	pouze	k6eAd1
se	s	k7c7
sloučeninami	sloučenina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
beryllium	beryllium	k1gNnSc1
se	se	k3xPyFc4
za	za	k7c2
normální	normální	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
nerozpouští	rozpouštět	k5eNaImIp3nS
v	v	k7c6
kyselině	kyselina	k1gFnSc6
dusičné	dusičný	k2eAgNnSc1d1
(	(	kIx(
<g/>
ve	v	k7c6
zředěné	zředěný	k2eAgFnSc6d1
kyselině	kyselina	k1gFnSc6
se	se	k3xPyFc4
pouze	pouze	k6eAd1
pasivuje	pasivovat	k5eAaBmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
za	za	k7c4
vyšší	vysoký	k2eAgFnPc4d2
teploty	teplota	k1gFnPc4
se	se	k3xPyFc4
oxiduje	oxidovat	k5eAaBmIp3nS
velmi	velmi	k6eAd1
živě	živě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
kyselinami	kyselina	k1gFnPc7
se	se	k3xPyFc4
beryllium	beryllium	k1gNnSc1
slučuje	slučovat	k5eAaImIp3nS
už	už	k9
za	za	k7c4
normální	normální	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beryllium	Beryllium	k1gNnSc1
se	se	k3xPyFc4
také	také	k9
rozpouští	rozpouštět	k5eAaImIp3nS
v	v	k7c6
roztocích	roztok	k1gInPc6
hydroxidů	hydroxid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
zředěnými	zředěný	k2eAgInPc7d1
roztoky	roztok	k1gInPc7
hydroxidů	hydroxid	k1gInPc2
reaguje	reagovat	k5eAaBmIp3nS
teprve	teprve	k6eAd1
za	za	k7c2
tepla	teplo	k1gNnSc2
a	a	k8xC
s	s	k7c7
koncentrovanými	koncentrovaný	k2eAgFnPc7d1
již	již	k9
za	za	k7c4
pokojové	pokojový	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Beryllium	Beryllium	k1gNnSc1
je	být	k5eAaImIp3nS
nejlehčí	lehký	k2eAgInSc1d3
prvek	prvek	k1gInSc1
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
stabilní	stabilní	k2eAgInSc1d1
izotop	izotop	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
9	#num#	k4
<g/>
Be	Be	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Beryllium	Beryllium	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1798	#num#	k4
objevil	objevit	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
Louis	Louis	k1gMnSc1
Nicolas	Nicolas	k1gMnSc1
Vauquelin	Vauquelin	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
minerálu	minerál	k1gInSc2
berylu	beryl	k1gInSc2
a	a	k8xC
ve	v	k7c6
smaragdech	smaragd	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
mají	mít	k5eAaImIp3nP
stejné	stejný	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
(	(	kIx(
<g/>
smaragd	smaragd	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
2	#num#	k4
%	%	kIx~
více	hodně	k6eAd2
chromu	chromat	k5eAaImIp1nS
<g/>
)	)	kIx)
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
zajímavé	zajímavý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
beryl	beryl	k1gInSc1
a	a	k8xC
smaragd	smaragd	k1gInSc1
jsou	být	k5eAaImIp3nP
podobné	podobný	k2eAgFnPc1d1
<g/>
,	,	kIx,
dospěl	dochvít	k5eAaPmAgMnS
přibližně	přibližně	k6eAd1
2000	#num#	k4
let	léto	k1gNnPc2
před	před	k7c7
objevem	objev	k1gInSc7
tohoto	tento	k3xDgInSc2
prvku	prvek	k1gInSc2
i	i	k9
Plinius	Plinius	k1gMnSc1
starší	starší	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
objevu	objev	k1gInSc6
beryllia	beryllium	k1gNnSc2
nebylo	být	k5eNaImAgNnS
jasné	jasný	k2eAgNnSc1d1
jeho	jeho	k3xOp3gNnSc1
oxidační	oxidační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
a	a	k8xC
relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
–	–	k?
uvažovalo	uvažovat	k5eAaImAgNnS
se	se	k3xPyFc4
Be	Be	k1gFnSc7
<g/>
2	#num#	k4
<g/>
+	+	kIx~
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
9,4	9,4	k4
nebo	nebo	k8xC
Be	Be	k1gMnSc3
<g/>
3	#num#	k4
<g/>
+	+	kIx~
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
14,1	14,1	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
nejasnost	nejasnost	k1gFnSc4
objasnil	objasnit	k5eAaPmAgMnS
o	o	k7c4
70	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
teprve	teprve	k6eAd1
Dmitrij	Dmitrij	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
Mendělejev	Mendělejev	k1gMnSc1
na	na	k7c6
základě	základ	k1gInSc6
svého	svůj	k3xOyFgInSc2
periodického	periodický	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něj	on	k3xPp3gMnSc2
nemá	mít	k5eNaImIp3nS
trojmocný	trojmocný	k2eAgInSc1d1
prvek	prvek	k1gInSc1
s	s	k7c7
relativní	relativní	k2eAgFnSc7d1
atomovou	atomový	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
okolo	okolo	k7c2
14	#num#	k4
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
dusíku	dusík	k1gInSc2
v	v	k7c6
periodické	periodický	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
prvků	prvek	k1gInPc2
místo	místo	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
dvojmocný	dvojmocný	k2eAgInSc1d1
prvek	prvek	k1gInSc1
s	s	k7c7
relativní	relativní	k2eAgFnSc7d1
atomovou	atomový	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
okolo	okolo	k7c2
9	#num#	k4
by	by	kYmCp3nS
dokonale	dokonale	k6eAd1
zaplnil	zaplnit	k5eAaPmAgMnS
mezeru	mezera	k1gFnSc4
mezi	mezi	k7c7
lithiem	lithium	k1gNnSc7
a	a	k8xC
borem	bor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
kov	kov	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
mnohých	mnohý	k2eAgFnPc6d1
vlastnostech	vlastnost	k1gFnPc6
podobal	podobat	k5eAaImAgInS
hliníku	hliník	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nebyl	být	k5eNaImAgMnS
schopný	schopný	k2eAgMnSc1d1
tvořit	tvořit	k5eAaImF
kamence	kamenec	k1gInPc4
(	(	kIx(
<g/>
podvojné	podvojný	k2eAgFnPc4d1
sloučeniny	sloučenina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
ve	v	k7c6
středověku	středověk	k1gInSc6
používaly	používat	k5eAaImAgInP
k	k	k7c3
barvení	barvení	k1gNnSc3
látek	látka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kov	kov	k1gInSc1
dostal	dostat	k5eAaPmAgInS
ve	v	k7c6
Francii	Francie	k1gFnSc6
název	název	k1gInSc4
glucinium	glucinium	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
upomíná	upomínat	k5eAaImIp3nS
na	na	k7c4
sladkou	sladký	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
berylnatých	berylnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
(	(	kIx(
<g/>
všechny	všechen	k3xTgFnPc4
berylnaté	berylnatý	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
jsou	být	k5eAaImIp3nP
prudce	prudko	k6eAd1
jedovaté	jedovatý	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
beryllium	beryllium	k1gNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
připraveno	připravit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1828	#num#	k4
nezávisle	závisle	k6eNd1
dvěma	dva	k4xCgInPc7
vědci	vědec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Friedrich	Friedrich	k1gMnSc1
Wöhler	Wöhler	k1gMnSc1
i	i	k9
Antoine	Antoin	k1gInSc5
Bussy	Bussa	k1gFnPc4
provedli	provést	k5eAaPmAgMnP
nezávisle	závisle	k6eNd1
na	na	k7c6
sobě	sebe	k3xPyFc6
redukcí	redukce	k1gFnSc7
chloridu	chlorid	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
kovovým	kovový	k2eAgInSc7d1
draslíkem	draslík	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
čistého	čistý	k2eAgNnSc2d1
beryllia	beryllium	k1gNnSc2
se	se	k3xPyFc4
povedla	povést	k5eAaPmAgFnS
teprve	teprve	k6eAd1
roku	rok	k1gInSc2
1898	#num#	k4
Lebeauovi	Lebeau	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
elektrolyzoval	elektrolyzovat	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
taveninu	tavenina	k1gFnSc4
fluoridu	fluorid	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
s	s	k7c7
fluoridem	fluorid	k1gInSc7
sodným	sodný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
v	v	k7c6
přírodě	příroda	k1gFnSc6
</s>
<s>
Ruda	ruda	k1gFnSc1
beryllia	beryllia	k1gFnSc1
</s>
<s>
Vybroušený	vybroušený	k2eAgInSc1d1
smaragd	smaragd	k1gInSc1
</s>
<s>
Díky	díky	k7c3
jeho	jeho	k3xOp3gFnSc3
poměrně	poměrně	k6eAd1
velké	velký	k2eAgFnSc3d1
reaktivitě	reaktivita	k1gFnSc3
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
setkáváme	setkávat	k5eAaImIp1nP
pouze	pouze	k6eAd1
se	s	k7c7
sloučeninami	sloučenina	k1gFnPc7
beryllia	beryllium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všech	všecek	k3xTgFnPc6
svých	svůj	k3xOyFgFnPc6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Be	Be	k1gFnSc2
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
berylnatých	berylnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
beryllium	beryllium	k1gNnSc1
i	i	k8xC
komplexy	komplex	k1gInPc1
[	[	kIx(
<g/>
BeO	BeO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
−	−	k?
a	a	k8xC
[	[	kIx(
<g/>
Be	Be	k1gFnSc1
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
−	−	k?
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
je	být	k5eAaImIp3nS
beryllium	beryllium	k1gNnSc1
obsaženo	obsáhnout	k5eAaPmNgNnS
v	v	k7c6
množství	množství	k1gNnSc6
3	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
,	,	kIx,
mořská	mořský	k2eAgFnSc1d1
voda	voda	k1gFnSc1
vykazuje	vykazovat	k5eAaImIp3nS
mimořádně	mimořádně	k6eAd1
nízký	nízký	k2eAgInSc1d1
obsah	obsah	k1gInSc1
beryllia	beryllia	k1gFnSc1
–	–	k?
přibližně	přibližně	k6eAd1
0,6	0,6	k4
ppt	ppt	k?
Be	Be	k1gMnSc1
(	(	kIx(
<g/>
nanogram	nanogram	k1gInSc1
<g/>
/	/	kIx~
<g/>
litr	litr	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
a	a	k8xC
ve	v	k7c6
výskytu	výskyt	k1gInSc6
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
na	na	k7c4
stejnou	stejný	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
jako	jako	k8xS,k8xC
cín	cín	k1gInSc4
<g/>
,	,	kIx,
europium	europium	k1gNnSc4
nebo	nebo	k8xC
arsen	arsen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
patří	patřit	k5eAaImIp3nS
beryllium	beryllium	k1gNnSc1
přes	přes	k7c4
svoji	svůj	k3xOyFgFnSc4
velmi	velmi	k6eAd1
nízkou	nízký	k2eAgFnSc4d1
atomovou	atomový	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
mezi	mezi	k7c7
poměrně	poměrně	k6eAd1
vzácné	vzácný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
–	–	k?
na	na	k7c6
jeden	jeden	k4xCgMnSc1
jeho	jeho	k3xOp3gInSc1
atom	atom	k1gInSc4
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
přibližně	přibližně	k6eAd1
4,5	4,5	k4
miliardy	miliarda	k4xCgFnSc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3
minerálem	minerál	k1gInSc7
s	s	k7c7
obsahem	obsah	k1gInSc7
beryllia	beryllium	k1gNnSc2
je	být	k5eAaImIp3nS
aluminosilikát	aluminosilikát	k1gInSc1
beryl	beryl	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
složení	složení	k1gNnSc1
popisuje	popisovat	k5eAaImIp3nS
následující	následující	k2eAgInSc4d1
sumární	sumární	k2eAgInSc4d1
vzorec	vzorec	k1gInSc4
<g/>
:	:	kIx,
Be	Be	k1gFnPc2
<g/>
3	#num#	k4
<g/>
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
SiO	SiO	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mineralogie	mineralogie	k1gFnSc1
zná	znát	k5eAaImIp3nS
mnoho	mnoho	k4c4
různých	různý	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
berylu	beryl	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
nejznámější	známý	k2eAgNnPc1d3
jsou	být	k5eAaImIp3nP
jistě	jistě	k9
drahé	drahý	k2eAgInPc1d1
kameny	kámen	k1gInPc1
zelený	zelený	k2eAgInSc1d1
smaragd	smaragd	k1gInSc4
a	a	k8xC
modrý	modrý	k2eAgInSc1d1
akvamarín	akvamarín	k1gInSc1
(	(	kIx(
<g/>
beryl	beryl	k1gInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
koruny	koruna	k1gFnSc2
britské	britský	k2eAgFnSc2d1
královny	královna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dalších	další	k2eAgInPc2d1
minerálů	minerál	k1gInPc2
s	s	k7c7
větším	veliký	k2eAgInSc7d2
obsahem	obsah	k1gInSc7
berylia	berylium	k1gNnSc2
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
například	například	k6eAd1
chryzoberyl	chryzoberyl	k1gInSc4
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
BeO	BeO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
bromellit	bromellit	k1gInSc1
BeO	BeO	k1gFnSc1
<g/>
,	,	kIx,
herderit	herderit	k1gInSc1
CaBe	CaBe	k1gFnSc1
<g/>
(	(	kIx(
<g/>
F	F	kA
<g/>
,	,	kIx,
OH	OH	kA
<g/>
)	)	kIx)
<g/>
PO	Po	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
euklas	euklas	k1gInSc1
BeAlSiO	BeAlSiO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fenakit	fenakit	k1gMnSc1
Be	Be	k1gFnSc2
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
SiO	SiO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
gadolinit	gadolinit	k5eAaPmF
Y	Y	kA
<g/>
2	#num#	k4
<g/>
Fe	Fe	k1gFnSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
Be	Be	k1gFnSc2
<g/>
2	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
vzácné	vzácný	k2eAgInPc1d1
minerály	minerál	k1gInPc1
beryllia	beryllium	k1gNnSc2
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
helvín	helvín	k1gInSc4
(	(	kIx(
<g/>
Fe	Fe	k1gMnSc1
<g/>
,	,	kIx,
Mn	Mn	k1gMnSc1
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
[	[	kIx(
<g/>
Be	Be	k1gFnSc2
<g/>
3	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
S	s	k7c7
<g/>
,	,	kIx,
danalit	danalit	k1gInSc1
[	[	kIx(
<g/>
Fe	Fe	k1gMnSc1
<g/>
,	,	kIx,
Zn	zn	kA
<g/>
]	]	kIx)
<g/>
4	#num#	k4
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Be	Be	k1gFnSc1
<g/>
3	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
S	s	k7c7
<g/>
,	,	kIx,
leukofán	leukofán	k2eAgInSc1d1
CaNaBe	CaNaBe	k1gInSc1
<g/>
(	(	kIx(
<g/>
Si	se	k3xPyFc3
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
F	F	kA
<g/>
,	,	kIx,
melinofan	melinofan	k1gMnSc1
(	(	kIx(
<g/>
Ca	ca	kA
<g/>
,	,	kIx,
Na	na	k7c6
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
Be	Be	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
Si	se	k3xPyFc3
<g/>
,	,	kIx,
Al	ala	k1gFnPc2
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
O	O	kA
<g/>
,	,	kIx,
F	F	kA
<g/>
)	)	kIx)
<g/>
7	#num#	k4
<g/>
,	,	kIx,
trimerit	trimerit	k1gInSc1
MnBeSiO	MnBeSiO	k1gFnSc1
<g/>
4	#num#	k4
a	a	k8xC
bertrandit	bertrandit	k5eAaImF,k5eAaPmF
Be	Be	k1gFnPc4
<g/>
8	#num#	k4
<g/>
[	[	kIx(
<g/>
Si	se	k3xPyFc3
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
SiO	SiO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Beryl	beryl	k1gInSc1
je	být	k5eAaImIp3nS
lehce	lehko	k6eAd1
získatelný	získatelný	k2eAgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
tvoří	tvořit	k5eAaImIp3nP
povrchová	povrchový	k2eAgNnPc4d1
ložiska	ložisko	k1gNnPc4
a	a	k8xC
těží	těžet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
Norsku	Norsko	k1gNnSc6
<g/>
,	,	kIx,
Španělsku	Španělsko	k1gNnSc6
a	a	k8xC
na	na	k7c6
Urale	Ural	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
beryl	beryl	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
těžitelném	těžitelný	k2eAgNnSc6d1
množství	množství	k1gNnSc6
na	na	k7c6
Šumavě	Šumava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zásoby	zásoba	k1gFnSc2
beryllia	beryllium	k1gNnSc2
jsou	být	k5eAaImIp3nP
odhadovány	odhadovat	k5eAaImNgFnP
na	na	k7c4
4	#num#	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Beryllium	Beryllium	k1gNnSc1
se	se	k3xPyFc4
získává	získávat	k5eAaImIp3nS
pražením	pražení	k1gNnSc7
berylu	beryl	k1gInSc2
s	s	k7c7
hexafluorokřemičitanem	hexafluorokřemičitan	k1gInSc7
sodným	sodný	k2eAgInSc7d1
při	při	k7c6
teplotě	teplota	k1gFnSc6
700	#num#	k4
až	až	k9
750	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
vyloužením	vyloužení	k1gNnSc7
rozpustného	rozpustný	k2eAgInSc2d1
fluoridu	fluorid	k1gInSc2
vodou	voda	k1gFnSc7
a	a	k8xC
následným	následný	k2eAgNnSc7d1
srážením	srážení	k1gNnSc7
hydroxidem	hydroxid	k1gInSc7
barnatým	barnatý	k2eAgInSc7d1
při	při	k7c6
pH	ph	kA
asi	asi	k9
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beryllium	Beryllium	k1gNnSc1
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
připravuje	připravovat	k5eAaImIp3nS
redukcí	redukce	k1gFnSc7
fluoridu	fluorid	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
hořčíkem	hořčík	k1gInSc7
při	při	k7c6
teplotě	teplota	k1gFnSc6
okolo	okolo	k7c2
1300	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
nejčastější	častý	k2eAgFnSc1d3
průmyslová	průmyslový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
kovové	kovový	k2eAgFnSc2d1
berylia	berylium	k1gNnPc1
probíhá	probíhat	k5eAaImIp3nS
elektrolýzou	elektrolýza	k1gFnSc7
směsi	směs	k1gFnSc2
roztaveného	roztavený	k2eAgInSc2d1
chloridu	chlorid	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
a	a	k8xC
sodného	sodný	k2eAgInSc2d1
na	na	k7c6
rtuťové	rtuťový	k2eAgFnSc6d1
katodě	katoda	k1gFnSc6
v	v	k7c6
ochranné	ochranný	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
plynného	plynný	k2eAgInSc2d1
argonu	argon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Beryllium	Beryllium	k1gNnSc1
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
také	také	k9
elektrolýzou	elektrolýza	k1gFnSc7
taveniny	tavenina	k1gFnSc2
směsi	směs	k1gFnSc2
fluoridu	fluorid	k1gInSc2
berylnatosodného	berylnatosodný	k2eAgInSc2d1
NaBeF	NaBeF	k1gFnSc7
<g/>
3	#num#	k4
a	a	k8xC
trifluoroberylnatanu	trifluoroberylnatan	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
Ba	ba	k9
<g/>
[	[	kIx(
<g/>
BeF	BeF	k1gFnSc1
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malé	Malá	k1gFnSc6
množství	množství	k1gNnSc1
beryllia	beryllium	k1gNnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
také	také	k9
připravit	připravit	k5eAaPmF
rozkladem	rozklad	k1gInSc7
azidu	azid	k1gInSc2
berylnatého	berylnatý	k2eAgMnSc4d1
Be	Be	k1gMnSc4
<g/>
2	#num#	k4
<g/>
N	N	kA
<g/>
6	#num#	k4
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Slitina	slitina	k1gFnSc1
beryllia	beryllia	k1gFnSc1
</s>
<s>
Nastavitelný	nastavitelný	k2eAgInSc4d1
klíč	klíč	k1gInSc4
ze	z	k7c2
slitiny	slitina	k1gFnSc2
berylia	berylium	k1gNnSc2
</s>
<s>
Minerály	minerál	k1gInPc1
beryllia	beryllium	k1gNnSc2
se	se	k3xPyFc4
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
ve	v	k7c6
šperkařství	šperkařství	k1gNnSc6
jako	jako	k8xS,k8xC
drahokamy	drahokam	k1gInPc4
a	a	k8xC
polodrahokamy	polodrahokam	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgInPc1d3
a	a	k8xC
největší	veliký	k2eAgInPc1d3
drahokamy	drahokam	k1gInPc1
berylu	beryl	k1gInSc2
jsou	být	k5eAaImIp3nP
usazeny	usadit	k5eAaPmNgFnP
v	v	k7c6
anglické	anglický	k2eAgFnSc6d1
koruně	koruna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Mimořádně	mimořádně	k6eAd1
důležitou	důležitý	k2eAgFnSc7d1
vlastností	vlastnost	k1gFnSc7
kovového	kovový	k2eAgNnSc2d1
berylia	berylium	k1gNnSc2
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
velmi	velmi	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
propustnost	propustnost	k1gFnSc1
pro	pro	k7c4
rentgenové	rentgenový	k2eAgNnSc4d1
záření	záření	k1gNnSc4
a	a	k8xC
nízkoenergetické	nízkoenergetický	k2eAgInPc4d1
neutrony	neutron	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
cenným	cenný	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
především	především	k9
v	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
energetice	energetika	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
slouží	sloužit	k5eAaImIp3nS
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
ke	k	k7c3
konstrukci	konstrukce	k1gFnSc3
neutronových	neutronový	k2eAgNnPc2d1
zrcadel	zrcadlo	k1gNnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
moderátorových	moderátorův	k2eAgFnPc2d1
tyčí	tyč	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
propustnost	propustnost	k1gFnSc1
pro	pro	k7c4
rentgenové	rentgenový	k2eAgNnSc4d1
záření	záření	k1gNnSc4
se	se	k3xPyFc4
úspěšně	úspěšně	k6eAd1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
jak	jak	k6eAd1
při	při	k7c6
kontrole	kontrola	k1gFnSc6
provozu	provoz	k1gInSc2
jaderných	jaderný	k2eAgMnPc2d1
reaktorů	reaktor	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k9
především	především	k9
při	při	k7c6
konstrukci	konstrukce	k1gFnSc6
rentgenových	rentgenový	k2eAgInPc2d1
analyzátorů	analyzátor	k1gInPc2
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzorek	vzorek	k1gInSc1
analyzovaného	analyzovaný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
umístěn	umístit	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jej	on	k3xPp3gMnSc4
od	od	k7c2
zdroje	zdroj	k1gInSc2
rentgenového	rentgenový	k2eAgNnSc2d1
záření	záření	k1gNnSc2
oddělovalo	oddělovat	k5eAaImAgNnS
okénko	okénko	k1gNnSc1
z	z	k7c2
čistého	čistý	k2eAgNnSc2d1
beryllia	beryllium	k1gNnSc2
o	o	k7c6
síle	síla	k1gFnSc6
pouze	pouze	k6eAd1
několika	několik	k4yIc2
mikrometrů	mikrometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
dosahuje	dosahovat	k5eAaImIp3nS
maximálního	maximální	k2eAgInSc2d1
průchodu	průchod	k1gInSc2
všech	všecek	k3xTgInPc2
vysoce	vysoce	k6eAd1
energetických	energetický	k2eAgInPc2d1
fotonů	foton	k1gInPc2
použitého	použitý	k2eAgNnSc2d1
rentgenova	rentgenův	k2eAgNnSc2d1
záření	záření	k1gNnSc2
a	a	k8xC
silně	silně	k6eAd1
tak	tak	k6eAd1
roste	růst	k5eAaImIp3nS
citlivost	citlivost	k1gFnSc4
analýzy	analýza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
metalurgii	metalurgie	k1gFnSc6
jsou	být	k5eAaImIp3nP
slitiny	slitina	k1gFnPc1
beryllia	beryllium	k1gNnSc2
především	především	k9
s	s	k7c7
mědí	mědit	k5eAaImIp3nP
ceněny	cenit	k5eAaImNgFnP
především	především	k9
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
vysokou	vysoký	k2eAgFnSc4d1
tvrdost	tvrdost	k1gFnSc4
<g/>
,	,	kIx,
pevnost	pevnost	k1gFnSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
dobrou	dobrý	k2eAgFnSc4d1
elektrickou	elektrický	k2eAgFnSc4d1
a	a	k8xC
tepelnou	tepelný	k2eAgFnSc4d1
vodivost	vodivost	k1gFnSc4
(	(	kIx(
<g/>
např.	např.	kA
Cu	Cu	k1gFnSc6
+	+	kIx~
2	#num#	k4
%	%	kIx~
Be	Be	k1gFnSc2
=	=	kIx~
beryliový	beryliový	k2eAgInSc4d1
bronz	bronz	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
vyrovná	vyrovnat	k5eAaBmIp3nS,k5eAaPmIp3nS
nejkvalitnější	kvalitní	k2eAgFnSc2d3
nemagnetické	magnetický	k2eNgFnSc2d1
oceli	ocel	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Slitiny	slitina	k1gFnPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
často	často	k6eAd1
v	v	k7c6
elektronice	elektronika	k1gFnSc6
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
odolných	odolný	k2eAgInPc2d1
elektrických	elektrický	k2eAgInPc2d1
kontaktů	kontakt	k1gInPc2
nebo	nebo	k8xC
speciálních	speciální	k2eAgFnPc2d1
elektrod	elektroda	k1gFnPc2
pro	pro	k7c4
obloukové	obloukový	k2eAgNnSc4d1
svařování	svařování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nízká	nízký	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
a	a	k8xC
vysoká	vysoký	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
slitin	slitina	k1gFnPc2
beryllia	beryllia	k1gFnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
využití	využití	k1gNnSc3
pro	pro	k7c4
konstrukci	konstrukce	k1gFnSc4
součástí	součást	k1gFnPc2
letadel	letadlo	k1gNnPc2
a	a	k8xC
kosmických	kosmický	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciální	speciální	k2eAgFnPc4d1
slitiny	slitina	k1gFnPc4
s	s	k7c7
mědí	měď	k1gFnSc7
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
na	na	k7c4
výrobu	výroba	k1gFnSc4
nejiskřivého	jiskřivý	k2eNgNnSc2d1
ručního	ruční	k2eAgNnSc2d1
nářadí	nářadí	k1gNnSc2
<g/>
;	;	kIx,
kladiv	kladivo	k1gNnPc2
ap.	ap.	kA
určených	určený	k2eAgInPc2d1
pro	pro	k7c4
používání	používání	k1gNnSc4
v	v	k7c6
provozech	provoz	k1gInPc6
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
hrozí	hrozit	k5eAaImIp3nS
nebezpečí	nebezpečí	k1gNnSc1
výbuchu	výbuch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
nízkou	nízký	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
(	(	kIx(
<g/>
o	o	k7c4
třetinu	třetina	k1gFnSc4
nižší	nízký	k2eAgFnSc1d2
než	než	k8xS
hliník	hliník	k1gInSc1
a	a	k8xC
o	o	k7c4
60	#num#	k4
<g/>
%	%	kIx~
než	než	k8xS
titan	titan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vysokou	vysoký	k2eAgFnSc4d1
tuhost	tuhost	k1gFnSc4
a	a	k8xC
rychlost	rychlost	k1gFnSc4
zvuku	zvuk	k1gInSc2
v	v	k7c6
tomto	tento	k3xDgInSc6
materiálu	materiál	k1gInSc6
(	(	kIx(
<g/>
vlastní	vlastní	k2eAgFnSc2d1
rezonance	rezonance	k1gFnSc2
membrány	membrána	k1gFnSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
neslyšitelném	slyšitelný	k2eNgNnSc6d1
spektru	spektrum	k1gNnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
pro	pro	k7c4
konstrukci	konstrukce	k1gFnSc4
špičkových	špičkový	k2eAgInPc2d1
elektrodynamických	elektrodynamický	k2eAgInPc2d1
vysokotónových	vysokotónový	k2eAgInPc2d1
reproduktorů	reproduktor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oxid	oxid	k1gInSc1
berylnatý	berylnatý	k2eAgMnSc1d1
BeO	BeO	k1gMnSc1
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
na	na	k7c4
lisování	lisování	k1gNnSc4
žáruvzdorných	žáruvzdorný	k2eAgInPc2d1
tyglíků	tyglík	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
odolávají	odolávat	k5eAaImIp3nP
teplotě	teplota	k1gFnSc3
do	do	k7c2
2500	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
katalyzátorem	katalyzátor	k1gInSc7
při	při	k7c6
výrobě	výroba	k1gFnSc6
některých	některý	k3yIgFnPc2
organických	organický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Anorganické	anorganický	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Hydrid	hydrid	k1gInSc4
berylnatý	berylnatý	k2eAgInSc4d1
BeH	BeH	k1gFnSc7
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
tuhá	tuhý	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
velice	velice	k6eAd1
podobá	podobat	k5eAaImIp3nS
hydridu	hydrid	k1gInSc3
hlinitému	hlinitý	k2eAgInSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodou	voda	k1gFnSc7
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
za	za	k7c2
vzniku	vznik	k1gInSc2
vodíku	vodík	k1gInSc2
a	a	k8xC
hydroxidu	hydroxid	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
a	a	k8xC
methanolem	methanol	k1gInSc7
za	za	k7c2
vzniku	vznik	k1gInSc2
vodíku	vodík	k1gInSc2
a	a	k8xC
methanolátu	methanolát	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
reakcí	reakce	k1gFnSc7
chloridu	chlorid	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
s	s	k7c7
hydridem	hydrid	k1gInSc7
lithným	lithný	k2eAgInSc7d1
nebo	nebo	k8xC
dimethylberyllia	dimethylberyllium	k1gNnPc1
s	s	k7c7
tetrahydridohlinitanem	tetrahydridohlinitan	k1gInSc7
lithným	lithný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc1
berylnatý	berylnatý	k2eAgInSc1d1
BeO	BeO	k1gFnSc4
je	být	k5eAaImIp3nS
bílý	bílý	k2eAgInSc1d1
kyprý	kyprý	k2eAgInSc1d1
prášek	prášek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
obtížně	obtížně	k6eAd1
rozpustný	rozpustný	k2eAgMnSc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žíháním	žíhání	k1gNnSc7
s	s	k7c7
uhlíkem	uhlík	k1gInSc7
vzinká	vzinkat	k5eAaPmIp3nS,k5eAaImIp3nS
karbid	karbid	k1gInSc1
berylnatý	berylnatý	k2eAgInSc1d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
podobá	podobat	k5eAaImIp3nS
karbidu	karbid	k1gInSc2
hlinitému	hlinitý	k2eAgNnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxid	oxid	k1gInSc1
berylnatý	berylnatý	k2eAgInSc1d1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
žíháním	žíhání	k1gNnSc7
hydroxidu	hydroxid	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
nebo	nebo	k8xC
i	i	k9
jiných	jiný	k2eAgFnPc2d1
berylnatých	berylnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
teplem	teplo	k1gNnSc7
rozkládají	rozkládat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1
berylnatý	berylnatý	k2eAgInSc1d1
Be	Be	k1gMnSc7
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
práškovitá	práškovitý	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
vodě	voda	k1gFnSc6
nerozpustná	rozpustný	k2eNgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
rozpouští	rozpouštět	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
kyselinách	kyselina	k1gFnPc6
na	na	k7c4
berylnaté	berylnatý	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
a	a	k8xC
v	v	k7c6
hydroxidech	hydroxid	k1gInPc6
na	na	k7c4
hydroxoberylnatany	hydroxoberylnatan	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	s	k7c7
srážením	srážení	k1gNnSc7
berylnatých	berylnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
roztoky	roztoka	k1gFnSc2
alkalických	alkalický	k2eAgInPc2d1
hydroxidů	hydroxid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Soli	sůl	k1gFnPc1
</s>
<s>
Soli	sůl	k1gFnPc1
a	a	k8xC
sloučeniny	sloučenina	k1gFnPc1
beryllia	beryllium	k1gNnSc2
se	se	k3xPyFc4
velmi	velmi	k6eAd1
podobají	podobat	k5eAaImIp3nP
solím	sůl	k1gFnPc3
a	a	k8xC
sloučeninám	sloučenina	k1gFnPc3
hliníku	hliník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
berylnatých	berylnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
se	se	k3xPyFc4
ve	v	k7c6
vodě	voda	k1gFnSc6
rozpuští	rozpuštit	k5eAaPmIp3nS
a	a	k8xC
jen	jen	k9
část	část	k1gFnSc1
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
hůře	zle	k6eAd2
nebo	nebo	k8xC
vůbec	vůbec	k9
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
soli	sůl	k1gFnPc1
mají	mít	k5eAaImIp3nP
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
bezbarvé	bezbarvý	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
anion	anion	k1gInSc1
soli	sůl	k1gFnSc2
barevný	barevný	k2eAgMnSc1d1
(	(	kIx(
<g/>
manganistany	manganistan	k1gInPc1
<g/>
,	,	kIx,
chromany	chroman	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berylnaté	berylnatý	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
snadno	snadno	k6eAd1
podvojné	podvojný	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
i	i	k8xC
komplexy	komplex	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
díky	díky	k7c3
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
beryllium	beryllium	k1gNnSc1
amfoterní	amfoterní	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
–	–	k?
reaguje	reagovat	k5eAaBmIp3nS
s	s	k7c7
kyselinami	kyselina	k1gFnPc7
za	za	k7c2
vzniku	vznik	k1gInSc2
berylnatých	berylnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
Be	Be	k1gFnSc4
<g/>
2	#num#	k4
<g/>
+	+	kIx~
a	a	k8xC
s	s	k7c7
hydroxidy	hydroxid	k1gInPc7
za	za	k7c2
vzniku	vznik	k1gInSc2
komplexních	komplexní	k2eAgMnPc2d1
anionů	anion	k1gMnPc2
tetrahydroxyberylnatanových	tetrahydroxyberylnatanový	k2eAgMnPc2d1
[	[	kIx(
<g/>
Be	Be	k1gMnPc2
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
−	−	k?
a	a	k8xC
jejich	jejich	k3xOp3gNnSc7
zahříváním	zahřívání	k1gNnSc7
(	(	kIx(
<g/>
„	„	k?
<g/>
rozkladem	rozklad	k1gInSc7
<g/>
“	“	k?
<g/>
)	)	kIx)
lze	lze	k6eAd1
vytvořit	vytvořit	k5eAaPmF
berylnatanový	berylnatanový	k2eAgInSc4d1
anion	anion	k1gInSc4
BeO	BeO	k1gFnPc2
<g/>
22	#num#	k4
<g/>
−	−	k?
<g/>
,	,	kIx,
tyto	tento	k3xDgInPc1
aniony	anion	k1gInPc1
jsou	být	k5eAaImIp3nP
také	také	k9
bezbarvé	bezbarvý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berylnatany	Berylnatan	k1gInPc7
nemají	mít	k5eNaImIp3nP
praktické	praktický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
a	a	k8xC
ve	v	k7c6
vodném	vodné	k1gNnSc6
roztoku	roztok	k1gInSc2
nejsou	být	k5eNaImIp3nP
příliš	příliš	k6eAd1
stabilní	stabilní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Fluorid	fluorid	k1gInSc4
berylnatý	berylnatý	k2eAgInSc4d1
BeF	BeF	k1gFnSc7
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
bezbarvá	bezbarvý	k2eAgNnPc1d1
<g/>
,	,	kIx,
rozpustná	rozpustný	k2eAgNnPc1d1
<g/>
,	,	kIx,
silně	silně	k6eAd1
hygroskopická	hygroskopický	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roztoku	roztok	k1gInSc6
velmi	velmi	k6eAd1
snadno	snadno	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
podvojné	podvojný	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
reakcí	reakce	k1gFnSc7
hydroxidu	hydroxid	k1gInSc2
berylnatého	berylnatý	k2eAgMnSc4d1
s	s	k7c7
kyselinou	kyselina	k1gFnSc7
fluorovodíkovou	fluorovodíkový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Chlorid	chlorid	k1gInSc1
berylnatý	berylnatý	k2eAgInSc4d1
BeCl	BeCl	k1gInSc4
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
sněhobílá	sněhobílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
rozpustná	rozpustný	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
snadno	snadno	k6eAd1
taje	tát	k5eAaImIp3nS
(	(	kIx(
<g/>
teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc1
405	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
vypařuje	vypařovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
ochotně	ochotně	k6eAd1
vytváří	vytvářet	k5eAaImIp3nP,k5eAaPmIp3nP
adiční	adiční	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
s	s	k7c7
organickými	organický	k2eAgFnPc7d1
látkami	látka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	s	k7c7
zahříváním	zahřívání	k1gNnSc7
berylia	berylium	k1gNnSc2
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
chlorovodíku	chlorovodík	k1gInSc2
nebo	nebo	k8xC
chloru	chlor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1
berylnatý	berylnatý	k2eAgInSc1d1
Be	Be	k1gMnSc7
<g/>
(	(	kIx(
<g/>
NO	no	k9
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
bezbarvá	bezbarvý	k2eAgFnSc1d1
<g/>
,	,	kIx,
rozpustná	rozpustný	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používal	používat	k5eAaImAgMnS
se	se	k3xPyFc4
ke	k	k7c3
zpevnění	zpevnění	k1gNnSc3
žárových	žárový	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
v	v	k7c6
plynových	plynový	k2eAgNnPc6d1
svítidlech	svítidlo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
podvojnou	podvojný	k2eAgFnSc7d1
záměnou	záměna	k1gFnSc7
při	při	k7c6
reakci	reakce	k1gFnSc6
síranu	síran	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
s	s	k7c7
dusičnanem	dusičnan	k1gInSc7
barnatým	barnatý	k2eAgInSc7d1
nebo	nebo	k8xC
rozpouštěním	rozpouštění	k1gNnSc7
hydroxidu	hydroxid	k1gInSc2
berylnatého	berylnatý	k2eAgInSc2d1
v	v	k7c6
kyselině	kyselina	k1gFnSc6
dusičné	dusičný	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Uhličitan	uhličitan	k1gInSc4
berylnatý	berylnatý	k2eAgInSc4d1
BeCO	BeCO	k1gFnSc7
<g/>
3	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
práškovitá	práškovitý	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
nerozpustná	rozpustný	k2eNgFnSc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniká	vznikat	k5eAaImIp3nS
reakcí	reakce	k1gFnSc7
hydroxidu	hydroxid	k1gInSc2
berylnatého	berylnatý	k2eAgMnSc4d1
s	s	k7c7
kyselinou	kyselina	k1gFnSc7
uhličitou	uhličitý	k2eAgFnSc7d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
lépe	dobře	k6eAd2
srážením	srážení	k1gNnSc7
roztoku	roztok	k1gInSc2
berylantých	berylantý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
roztoky	roztoka	k1gFnSc2
alkalických	alkalický	k2eAgInPc2d1
uhličitanů	uhličitan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Síran	síran	k1gInSc4
berylnatý	berylnatý	k2eAgInSc4d1
BeSO	BeSO	k1gFnSc7
<g/>
4	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
ve	v	k7c6
vodě	voda	k1gFnSc6
nerozpouští	rozpouštět	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roztoku	roztok	k1gInSc6
velmi	velmi	k6eAd1
snadno	snadno	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
sírany	síran	k1gInPc7
podvojné	podvojný	k2eAgFnSc2d1
sloučeniny	sloučenina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
reakcí	reakce	k1gFnSc7
hydroxidu	hydroxid	k1gInSc2
berylnatého	berylnatý	k2eAgMnSc4d1
s	s	k7c7
kyselinou	kyselina	k1gFnSc7
sírovou	sírový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Organické	organický	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Mezi	mezi	k7c4
organické	organický	k2eAgFnPc4d1
sloučeniny	sloučenina	k1gFnPc4
beryllia	beryllium	k1gNnSc2
patří	patřit	k5eAaImIp3nP
zejména	zejména	k9
berylnaté	berylnatý	k2eAgFnPc1d1
soli	sůl	k1gFnPc1
organických	organický	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
a	a	k8xC
berylnaté	berylnatý	k2eAgInPc4d1
alkoholáty	alkoholát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgFnPc3d1
berylnatým	berylnatý	k2eAgFnPc3d1
sloučeninám	sloučenina	k1gFnPc3
patří	patřit	k5eAaImIp3nS
nejrůznější	různý	k2eAgInPc4d3
organické	organický	k2eAgInPc4d1
komplexy	komplex	k1gInPc4
berylnatých	berylnatý	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
zvláštní	zvláštní	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
organických	organický	k2eAgFnPc2d1
berylnatých	berylnatý	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nP
organokovové	organokovový	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Zdravotní	zdravotní	k2eAgNnPc1d1
rizika	riziko	k1gNnPc1
</s>
<s>
Beryllium	Beryllium	k1gNnSc1
a	a	k8xC
především	především	k9
jeho	jeho	k3xOp3gFnPc1
soli	sůl	k1gFnPc1
jsou	být	k5eAaImIp3nP
ze	z	k7c2
zdravotního	zdravotní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
velmi	velmi	k6eAd1
rizikové	rizikový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
jak	jak	k6eAd1
přímo	přímo	k6eAd1
toxické	toxický	k2eAgNnSc1d1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
potenciálně	potenciálně	k6eAd1
karcinogenní	karcinogenní	k2eAgNnPc1d1
<g/>
,	,	kIx,
tedy	tedy	k9
schopné	schopný	k2eAgNnSc1d1
vyvolat	vyvolat	k5eAaPmF
rakovinu	rakovina	k1gFnSc4
nebo	nebo	k8xC
alespoň	alespoň	k9
zvýšit	zvýšit	k5eAaPmF
riziko	riziko	k1gNnSc4
jejího	její	k3xOp3gInSc2
výskytu	výskyt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoká	vysoký	k2eAgFnSc1d1
toxicita	toxicita	k1gFnSc1
beryllia	beryllium	k1gNnSc2
je	být	k5eAaImIp3nS
nejspíše	nejspíše	k9
způsobena	způsobit	k5eAaPmNgFnS
jeho	jeho	k3xOp3gFnSc7
schopností	schopnost	k1gFnSc7
vytěsnit	vytěsnit	k5eAaPmF
hořčík	hořčík	k1gInSc4
z	z	k7c2
enzymů	enzym	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
dlouhodobém	dlouhodobý	k2eAgNnSc6d1
vdechování	vdechování	k1gNnSc6
zvýšeného	zvýšený	k2eAgNnSc2d1
množství	množství	k1gNnSc2
aerosolu	aerosol	k1gInSc2
a	a	k8xC
mikroskopických	mikroskopický	k2eAgFnPc2d1
částeček	částečka	k1gFnPc2
s	s	k7c7
obsahem	obsah	k1gInSc7
beryllia	beryllium	k1gNnSc2
vzniká	vznikat	k5eAaImIp3nS
plicní	plicní	k2eAgFnSc1d1
choroba	choroba	k1gFnSc1
–	–	k?
chronická	chronický	k2eAgFnSc1d1
berylióza	berylióza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
již	již	k9
z	z	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
prokazatelně	prokazatelně	k6eAd1
postihuje	postihovat	k5eAaImIp3nS
pracovníky	pracovník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
dlouhodobě	dlouhodobě	k6eAd1
vystaveni	vystavit	k5eAaPmNgMnP
pobytu	pobyt	k1gInSc3
v	v	k7c6
prostředí	prostředí	k1gNnSc6
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
prachových	prachový	k2eAgFnPc2d1
částic	částice	k1gFnPc2
na	na	k7c6
bázi	báze	k1gFnSc6
beryllia	beryllium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jisté	jistý	k2eAgNnSc1d1
procento	procento	k1gNnSc4
případů	případ	k1gInPc2
beryliózy	berylióza	k1gFnSc2
obvykle	obvykle	k6eAd1
bohužel	bohužel	k9
přerůstá	přerůstat	k5eAaImIp3nS
v	v	k7c4
plicní	plicní	k2eAgFnSc4d1
rakovinu	rakovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
zdravotní	zdravotní	k2eAgNnSc1d1
riziko	riziko	k1gNnSc1
pro	pro	k7c4
organismus	organismus	k1gInSc4
ale	ale	k8xC
představuje	představovat	k5eAaImIp3nS
příjem	příjem	k1gInSc1
berylnatých	berylnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
v	v	k7c6
potravě	potrava	k1gFnSc6
nebo	nebo	k8xC
pitné	pitný	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvýšený	zvýšený	k2eAgInSc1d1
příjem	příjem	k1gInSc1
solí	sůl	k1gFnPc2
beryllia	beryllium	k1gNnSc2
způsobuje	způsobovat	k5eAaImIp3nS
prokazatelně	prokazatelně	k6eAd1
značné	značný	k2eAgNnSc1d1
riziko	riziko	k1gNnSc1
vzniku	vznik	k1gInSc2
rakovinného	rakovinný	k2eAgNnSc2d1
bujení	bujení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
je	být	k5eAaImIp3nS
beryllium	beryllium	k1gNnSc1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
velmi	velmi	k6eAd1
vážných	vážný	k2eAgInPc2d1
rizikových	rizikový	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
výskyt	výskyt	k1gInSc1
v	v	k7c6
pitné	pitný	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
a	a	k8xC
potravinách	potravina	k1gFnPc6
je	být	k5eAaImIp3nS
neustále	neustále	k6eAd1
monitorován	monitorován	k2eAgInSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
povolené	povolený	k2eAgInPc1d1
limity	limit	k1gInPc1
koncentrací	koncentrace	k1gFnPc2
patří	patřit	k5eAaImIp3nP
k	k	k7c3
nejnižším	nízký	k2eAgFnPc3d3
z	z	k7c2
běžně	běžně	k6eAd1
sledovaných	sledovaný	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
EXCELIA	EXCELIA	kA
HIFI	Hifi	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Focal	Focal	k1gInSc1
JM	JM	kA
Lab	Lab	k1gFnSc1
Electra	Electra	k1gFnSc1
1007	#num#	k4
Beryllium	Beryllium	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Be	Be	k1gMnPc1
Beryllium	Beryllium	k1gNnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Element	element	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Production	Production	k1gInSc1
<g/>
,	,	kIx,
Atom	atom	k1gInSc1
<g/>
,	,	kIx,
Molecules	Molecules	k1gMnSc1
<g/>
,	,	kIx,
Chemical	Chemical	k1gMnSc1
Behavior	Behavior	k1gMnSc1
<g/>
,	,	kIx,
Toxicology	Toxicolog	k1gMnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Springer	Springer	k1gInSc1
Science	Science	k1gFnSc2
&	&	k?
Business	business	k1gInSc1
Media	medium	k1gNnSc2
333	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9783662103173	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Jursík	Jursík	k1gMnSc1
F.	F.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
nekovů	nekov	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7080-504-8	80-7080-504-8	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
beryllium	beryllium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
beryllium	beryllium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Chemický	chemický	k2eAgInSc1d1
vzdělávací	vzdělávací	k2eAgInSc1d1
portál	portál	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
ATSDR	ATSDR	kA
Case	Case	k1gNnSc1
Studies	Studies	k1gMnSc1
in	in	k?
Environmental	Environmental	k1gMnSc1
Medicine	Medicin	k1gInSc5
<g/>
:	:	kIx,
Beryllium	Beryllium	k1gNnSc4
Toxicity	toxicita	k1gFnSc2
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
Health	Health	k1gMnSc1
and	and	k?
Human	Human	k1gMnSc1
Services	Services	k1gMnSc1
</s>
<s>
It	It	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Elemental	Elemental	k1gMnSc1
–	–	k?
Beryllium	Beryllium	k1gNnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4144824-8	4144824-8	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5750	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85013403	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85013403	#num#	k4
</s>
