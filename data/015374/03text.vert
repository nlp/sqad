<s>
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
</s>
<s>
O	o	k7c6
titulní	titulní	k2eAgFnSc6d1
postavě	postava	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
(	(	kIx(
<g/>
postava	postava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
101	#num#	k4
min	mina	k1gFnPc2
Žánry	žánr	k1gInPc1
</s>
<s>
akční	akční	k2eAgInSc1d1
filmfilmový	filmfilmový	k2eAgInSc1d1
thrillercrime	thrillercrimat	k5eAaPmIp3nS
thriller	thriller	k1gInSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Derek	Derek	k6eAd1
Kolstad	Kolstad	k1gInSc1
Režie	režie	k1gFnSc2
</s>
<s>
David	David	k1gMnSc1
Leitch	Leitch	k1gMnSc1
Obsazení	obsazení	k1gNnPc2
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Keanu	Keanout	k5eAaPmIp1nS,k5eAaImIp1nS
ReevesAdrianne	ReevesAdriann	k1gInSc5
PalickiAlfie	PalickiAlfie	k1gFnSc2
AllenBridget	AllenBridget	k1gMnSc1
MoynahanBridget	MoynahanBridget	k1gMnSc1
Regan	Regan	k1gMnSc1
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Produkce	produkce	k1gFnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
LongoriaDavid	LongoriaDavid	k1gInSc1
Leitch	Leitch	k1gMnSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
Tyler	Tyler	k1gMnSc1
Bates	Bates	k1gMnSc1
Kamera	kamera	k1gFnSc1
</s>
<s>
Jonathan	Jonathan	k1gMnSc1
Sela	selo	k1gNnSc2
Střih	střih	k1gInSc1
</s>
<s>
Elísabet	Elísabet	k1gMnSc1
Ronaldsdóttir	Ronaldsdóttir	k1gMnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
(	(	kIx(
<g/>
USA	USA	kA
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
a	a	k8xC
Kanada	Kanada	k1gFnSc1
<g/>
)	)	kIx)
<g/>
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
(	(	kIx(
<g/>
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
Produkční	produkční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Summit	summit	k1gInSc1
Entertainment	Entertainment	k1gInSc1
Distribuce	distribuce	k1gFnSc1
</s>
<s>
Summit	summit	k1gInSc1
EntertainmentInterComNetflixFandangoNow	EntertainmentInterComNetflixFandangoNow	k1gFnPc2
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
20	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
<g/>
$	$	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tržby	tržba	k1gFnPc1
</s>
<s>
88,8	88,8	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
<g/>
$	$	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc4d1
akční	akční	k2eAgInSc4d1
film	film	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
režiséra	režisér	k1gMnSc2
Chada	Chad	k1gMnSc2
Stahelskiho	Stahelski	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
jej	on	k3xPp3gMnSc4
natočil	natočit	k5eAaBmAgMnS
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
David	David	k1gMnSc1
Leitchem	Leitch	k1gMnSc7
podle	podle	k7c2
scénáře	scénář	k1gInSc2
Dereka	Derek	k1gMnSc1
Kolstada	Kolstad	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
titulní	titulní	k2eAgFnSc6d1
roli	role	k1gFnSc6
Johna	John	k1gMnSc2
Wicka	Wick	k1gMnSc2
se	se	k3xPyFc4
představil	představit	k5eAaPmAgMnS
Keanu	Keanu	k1gMnSc1
Reeves	Reeves	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
dalších	další	k2eAgFnPc6d1
rolích	role	k1gFnPc6
se	se	k3xPyFc4
objevili	objevit	k5eAaPmAgMnP
Michael	Michael	k1gMnSc1
Nyqvist	Nyqvist	k1gMnSc1
<g/>
,	,	kIx,
Alfie	Alfie	k1gMnSc1
Allen	Allen	k1gMnSc1
<g/>
,	,	kIx,
Adrianne	Adrianne	k1gFnSc1
Palicki	Palick	k1gFnSc1
<g/>
,	,	kIx,
Bridget	Bridget	k1gFnSc1
Moynahan	Moynahan	k1gFnSc1
<g/>
,	,	kIx,
Dean	Dean	k1gMnSc1
Winters	Winters	k1gMnSc1
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
McShane	McShan	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Leguizamo	Leguizamo	k1gMnSc1
a	a	k8xC
Willem	Willem	k1gMnSc1
Dafoe	Dafoe	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgInSc4
film	film	k1gInSc4
ze	z	k7c2
série	série	k1gFnSc2
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
filmu	film	k1gInSc2
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
Johna	John	k1gMnSc4
Wicka	Wicek	k1gInSc2
<g/>
,	,	kIx,
pátrajícího	pátrající	k2eAgInSc2d1
po	po	k7c6
lupičích	lupič	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
vloupali	vloupat	k5eAaPmAgMnP
do	do	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
domu	dům	k1gInSc2
<g/>
,	,	kIx,
ukradli	ukradnout	k5eAaPmAgMnP
mu	on	k3xPp3gMnSc3
staré	starý	k2eAgNnSc1d1
auto	auto	k1gNnSc1
a	a	k8xC
zabili	zabít	k5eAaPmAgMnP
jeho	jeho	k3xOp3gNnSc4
štěně	štěně	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgMnS
poslední	poslední	k2eAgInSc4d1
dar	dar	k1gInSc4
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
nedávno	nedávno	k6eAd1
zemřelé	zemřelý	k2eAgFnPc1d1
manželky	manželka	k1gFnPc1
(	(	kIx(
<g/>
Moynahan	Moynahan	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kolstad	Kolstad	k6eAd1
dokončil	dokončit	k5eAaPmAgInS
scénář	scénář	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
a	a	k8xC
pro	pro	k7c4
produkční	produkční	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Thunder	Thunder	k1gMnSc1
Road	Road	k1gMnSc1
Pictures	Pictures	k1gMnSc1
ho	on	k3xPp3gMnSc4
dále	daleko	k6eAd2
upravoval	upravovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
produkoval	produkovat	k5eAaImAgInS
Basil	Basil	k1gInSc4
Iwanyk	Iwanyka	k1gFnPc2
z	z	k7c2
Thunder	Thundra	k1gFnPc2
Road	Road	k1gMnSc1
Pictures	Pictures	k1gMnSc1
<g/>
,	,	kIx,
Leitch	Leitch	k1gMnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
Longoria	Longorium	k1gNnSc2
a	a	k8xC
Michael	Michael	k1gMnSc1
Witherill	Witherill	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Stahelskiho	Stahelski	k1gMnSc4
a	a	k8xC
Leitche	Leitche	k1gFnSc4
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
celovečerní	celovečerní	k2eAgInSc4d1
režisérský	režisérský	k2eAgInSc4d1
debut	debut	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
dříve	dříve	k6eAd2
s	s	k7c7
Revessem	Revess	k1gMnSc7
spolupracovali	spolupracovat	k5eAaImAgMnP
jako	jako	k9
koordinátoři	koordinátor	k1gMnPc1
kaskadérů	kaskadér	k1gMnPc2
a	a	k8xC
režiséři	režisér	k1gMnPc1
druhého	druhý	k4xOgInSc2
štábu	štáb	k1gInSc2
na	na	k7c6
trilogii	trilogie	k1gFnSc6
Matrix	Matrix	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Film	film	k1gInSc1
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgInS
s	s	k7c7
pozitivními	pozitivní	k2eAgFnPc7d1
recenzí	recenze	k1gFnSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
ho	on	k3xPp3gInSc4
kritici	kritik	k1gMnPc1
označují	označovat	k5eAaImIp3nP
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejlepších	dobrý	k2eAgInPc2d3
výkonů	výkon	k1gInPc2
Reevese	Reevese	k1gFnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
a	a	k8xC
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejlepších	dobrý	k2eAgInPc2d3
akčních	akční	k2eAgInPc2d1
filmů	film	k1gInPc2
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Film	film	k1gInSc1
celosvětově	celosvětově	k6eAd1
utržil	utržit	k5eAaPmAgInS
86	#num#	k4
milionů	milion	k4xCgInPc2
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
rozpočet	rozpočet	k1gInSc1
činil	činit	k5eAaImAgInS
20	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Film	film	k1gInSc1
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgNnPc4
pokračování	pokračování	k1gNnPc4
<g/>
,	,	kIx,
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
<g/>
:	:	kIx,
Kapitola	kapitola	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
premiérou	premiéra	k1gFnSc7
v	v	k7c6
únoru	únor	k1gInSc6
2017	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
<g/>
:	:	kIx,
Kapitola	kapitola	k1gFnSc1
3	#num#	k4
–	–	k?
Parabellum	Parabellum	k1gInSc1
<g/>
,	,	kIx,
uvedený	uvedený	k2eAgInSc1d1
v	v	k7c6
květnu	květen	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc2
snímek	snímka	k1gFnPc2
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgMnSc1d1
v	v	k7c6
sérii	série	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
distribuován	distribuovat	k5eAaBmNgInS
společností	společnost	k1gFnSc7
Summit	summit	k1gInSc1
Entertainment	Entertainment	k1gInSc4
<g/>
;	;	kIx,
ostatní	ostatní	k2eAgInPc1d1
filmy	film	k1gInPc1
byly	být	k5eAaImAgInP
distribuovány	distribuovat	k5eAaBmNgInP
firmou	firma	k1gFnSc7
Lionsgate	Lionsgat	k1gInSc5
Films	Films	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
ztratí	ztratit	k5eAaPmIp3nS
svou	svůj	k3xOyFgFnSc4
ženu	žena	k1gFnSc4
<g/>
,	,	kIx,
Helen	Helena	k1gFnPc2
<g/>
,	,	kIx,
po	po	k7c4
smrtelné	smrtelný	k2eAgFnPc4d1
nemoci	nemoc	k1gFnPc4
<g/>
,	,	kIx,
dostane	dostat	k5eAaPmIp3nS
po	po	k7c4
zesnulé	zesnulý	k2eAgNnSc4d1
štěňátko	štěňátko	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
Daisy	Daisa	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
pomohlo	pomoct	k5eAaPmAgNnS
vyrovnat	vyrovnat	k5eAaPmF,k5eAaBmF
se	se	k3xPyFc4
s	s	k7c7
jeho	jeho	k3xOp3gInSc7
zármutkem	zármutek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
Johnově	Johnův	k2eAgFnSc6d1
odtažitosti	odtažitost	k1gFnSc6
si	se	k3xPyFc3
štěně	štěně	k1gNnSc4
oblíbí	oblíbit	k5eAaPmIp3nP
a	a	k8xC
tráví	trávit	k5eAaImIp3nP
s	s	k7c7
ním	on	k3xPp3gMnSc7
své	svůj	k3xOyFgInPc4
dny	den	k1gInPc4
jízdnou	jízdný	k2eAgFnSc7d1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
Fordu	ford	k1gInSc6
Mustang	mustang	k1gMnSc1
1969	#num#	k4
Mach	macha	k1gFnPc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
benzínce	benzínka	k1gFnSc6
se	se	k3xPyFc4
potká	potkat	k5eAaPmIp3nS
s	s	k7c7
trojicí	trojice	k1gFnSc7
ruských	ruský	k2eAgMnPc2d1
gangsterů	gangster	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc4
vůdce	vůdce	k1gMnSc1
Josef	Josef	k1gMnSc1
Tarasov	Tarasov	k1gInSc4
trvá	trvat	k5eAaImIp3nS
na	na	k7c6
koupi	koupě	k1gFnSc6
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
vozu	vůz	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
John	John	k1gMnSc1
opakovaně	opakovaně	k6eAd1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
na	na	k7c4
prodej	prodej	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozzlobený	rozzlobený	k2eAgInSc1d1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
mafiáni	mafián	k1gMnPc1
sledují	sledovat	k5eAaImIp3nP
Johna	John	k1gMnSc4
v	v	k7c6
noci	noc	k1gFnSc6
<g/>
,	,	kIx,
srazí	srazit	k5eAaPmIp3nS
ho	on	k3xPp3gInSc4
do	do	k7c2
bezvědomí	bezvědomí	k1gNnSc2
<g/>
,	,	kIx,
ukradnou	ukradnout	k5eAaPmIp3nP
mu	on	k3xPp3gMnSc3
auto	auto	k1gNnSc4
a	a	k8xC
zabijí	zabít	k5eAaPmIp3nP
Daisy	Daisa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
vezme	vzít	k5eAaPmIp3nS
Mustanga	mustang	k1gMnSc2
k	k	k7c3
sektě	sekta	k1gFnSc3
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
řídí	řídit	k5eAaImIp3nS
Aurelio	Aurelio	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
změnil	změnit	k5eAaPmAgInS
VIN	vina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aurelio	Aurelio	k1gNnSc1
si	se	k3xPyFc3
uvědomuje	uvědomovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
Josef	Josef	k1gMnSc1
ukradl	ukradnout	k5eAaPmAgMnS
Johnovi	John	k1gMnSc3
<g/>
,	,	kIx,
propíchne	propíchnout	k5eAaPmIp3nS
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
než	než	k8xS
ho	on	k3xPp3gMnSc4
vyhází	vyházet	k5eAaPmIp3nS
z	z	k7c2
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
navštíví	navštívit	k5eAaPmIp3nS
Aurelia	Aurelius	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
mu	on	k3xPp3gMnSc3
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Josef	Josef	k1gMnSc1
je	být	k5eAaImIp3nS
synem	syn	k1gMnSc7
Viggo	Viggo	k6eAd1
Tarasova	Tarasův	k2eAgNnSc2d1
<g/>
,	,	kIx,
vedoucího	vedoucí	k2eAgMnSc2d1
ruského	ruský	k2eAgMnSc2d1
zločinecké	zločinecký	k2eAgInPc4d1
syndikátu	syndikát	k1gInSc3
v	v	k7c6
New	New	k1gFnSc6
York	York	k1gInSc1
City	City	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viggo	Viggo	k1gMnSc1
<g/>
,	,	kIx,
informovaný	informovaný	k2eAgMnSc1d1
Aureliem	Aurelium	k1gNnSc7
z	z	k7c2
josefovy	josefův	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
s	s	k7c7
Johnem	John	k1gMnSc7
<g/>
,	,	kIx,
vysvětlí	vysvětlit	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
je	být	k5eAaImIp3nS
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
<g/>
:	:	kIx,
vrah	vrah	k1gMnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
přezdívaného	přezdívaný	k2eAgInSc2d1
"	"	kIx"
<g/>
Baba	baba	k1gFnSc1
Yaga	Yaga	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
přeloženo	přeložit	k5eAaPmNgNnS
ve	v	k7c6
filmu	film	k1gInSc6
jako	jako	k8xS,k8xC
Rus	Rus	k1gMnSc1
pro	pro	k7c4
"	"	kIx"
<g/>
strašáka	strašák	k1gMnSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
kvůli	kvůli	k7c3
jeho	jeho	k3xOp3gNnPc3
smrtelným	smrtelný	k2eAgNnPc3d1
dílům	dílo	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
John	John	k1gMnSc1
chtěl	chtít	k5eAaImAgMnS
odejít	odejít	k5eAaPmF
do	do	k7c2
důchodu	důchod	k1gInSc2
a	a	k8xC
vzít	vzít	k5eAaPmF
si	se	k3xPyFc3
Helen	Helena	k1gFnPc2
<g/>
,	,	kIx,
Viggo	Viggo	k6eAd1
mu	on	k3xPp3gNnSc3
dal	dát	k5eAaPmAgMnS
"	"	kIx"
<g/>
nemožný	možný	k2eNgInSc4d1
úkol	úkol	k1gInSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
naznačoval	naznačovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
krátké	krátký	k2eAgFnSc6d1
době	doba	k1gFnSc6
zapojí	zapojit	k5eAaPmIp3nP
mnohonásobné	mnohonásobný	k2eAgInPc1d1
atentáty	atentát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
uspěl	uspět	k5eAaPmAgMnS
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
snaha	snaha	k1gFnSc1
byla	být	k5eAaImAgFnS
klíčem	klíč	k1gInSc7
k	k	k7c3
založení	založení	k1gNnSc3
syndikátu	syndikát	k1gInSc2
Tarasova	Tarasův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Viggo	Viggo	k6eAd1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
Johna	John	k1gMnSc2
vymluvit	vymluvit	k5eAaPmF
z	z	k7c2
hledání	hledání	k1gNnSc2
odplaty	odplata	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
John	John	k1gMnSc1
mlčky	mlčky	k6eAd1
odmítá	odmítat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viggo	Viggo	k1gMnSc1
pošle	poslat	k5eAaPmIp3nS
Johnovi	John	k1gMnSc3
do	do	k7c2
domu	dům	k1gInSc2
12	#num#	k4
<g/>
členné	členný	k2eAgNnSc4d1
družstvo	družstvo	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
John	John	k1gMnSc1
je	on	k3xPp3gInPc4
všechny	všechen	k3xTgInPc4
zabije	zabít	k5eAaPmIp3nS
a	a	k8xC
má	mít	k5eAaImIp3nS
těla	tělo	k1gNnPc1
profesionálně	profesionálně	k6eAd1
odebrána	odebrán	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepřekvapený	překvapený	k2eNgInSc4d1
Viggo	Viggo	k1gNnSc1
klade	klást	k5eAaImIp3nS
$	$	kIx~
<g/>
2	#num#	k4
000	#num#	k4
000	#num#	k4
odměnu	odměna	k1gFnSc4
Johnovi	John	k1gMnSc3
na	na	k7c4
hlavu	hlava	k1gFnSc4
a	a	k8xC
osobně	osobně	k6eAd1
nabídne	nabídnout	k5eAaPmIp3nS
smlouvu	smlouva	k1gFnSc4
Johnovi	John	k1gMnSc3
učiteli	učitel	k1gMnSc3
<g/>
,	,	kIx,
Marcusi	Marcuse	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
hledá	hledat	k5eAaImIp3nS
útočiště	útočiště	k1gNnSc4
v	v	k7c6
kontinentálním	kontinentální	k2eAgInSc6d1
hotelu	hotel	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
výlučně	výlučně	k6eAd1
na	na	k7c4
kriminální	kriminální	k2eAgNnSc4d1
podsvětí	podsvětí	k1gNnSc4
a	a	k8xC
nepřipouští	připouštět	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc4
"	"	kIx"
<g/>
podnikání	podnikání	k1gNnSc4
<g/>
"	"	kIx"
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
prostorách	prostora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viggo	Viggo	k6eAd1
zdvojnásobí	zdvojnásobit	k5eAaPmIp3nS
odměnu	odměna	k1gFnSc4
za	za	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
chtějí	chtít	k5eAaImIp3nP
porušit	porušit	k5eAaPmF
pravidla	pravidlo	k1gNnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zabili	zabít	k5eAaPmAgMnP
Johna	John	k1gMnSc4
v	v	k7c6
kontinentálním	kontinentální	k2eAgInSc6d1
hotelu	hotel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
se	se	k3xPyFc4
učí	učit	k5eAaImIp3nS
od	od	k7c2
Winstona	Winston	k1gMnSc2
<g/>
,	,	kIx,
majitele	majitel	k1gMnSc2
hotelu	hotel	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
Viggo	Viggo	k6eAd1
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
v	v	k7c6
nočním	noční	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Rudém	rudý	k2eAgInSc6d1
kruhu	kruh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
vstoupí	vstoupit	k5eAaPmIp3nS
do	do	k7c2
Rudého	rudý	k2eAgInSc2d1
kruhu	kruh	k1gInSc2
a	a	k8xC
zabije	zabít	k5eAaPmIp3nS
ochranku	ochranka	k1gFnSc4
Jozefa	Jozef	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ale	ale	k9
uniká	unikat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
se	se	k3xPyFc4
zraněný	zraněný	k1gMnSc1
vrací	vracet	k5eAaImIp3nS
do	do	k7c2
hotelu	hotel	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mu	on	k3xPp3gMnSc3
zašili	zašít	k5eAaPmAgMnP
zranění	zranění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paní	paní	k1gFnSc1
Perkinsová	Perkinsová	k1gFnSc1
<g/>
,	,	kIx,
vražedkyně	vražedkyně	k1gFnSc1
a	a	k8xC
bývalá	bývalý	k2eAgFnSc1d1
známá	známá	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
vplíží	vplížit	k5eAaPmIp3nS
do	do	k7c2
Johnova	Johnův	k2eAgInSc2d1
pokoje	pokoj	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ho	on	k3xPp3gInSc4
zabila	zabít	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
Marcus	Marcus	k1gMnSc1
varuje	varovat	k5eAaImIp3nS
Johna	John	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přinutí	přinutit	k5eAaPmIp3nS
Perkinsovou	Perkinsová	k1gFnSc4
odhalit	odhalit	k5eAaPmF
polohu	poloha	k1gFnSc4
Viggo	Viggo	k6eAd1
<g/>
,	,	kIx,
zranit	zranit	k5eAaPmF
ji	on	k3xPp3gFnSc4
do	do	k7c2
bezvědomí	bezvědomí	k1gNnSc2
a	a	k8xC
nechá	nechat	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
u	u	k7c2
spoluvraha	spoluvrah	k1gMnSc2
Harryho	Harry	k1gMnSc2
čekat	čekat	k5eAaImF
na	na	k7c4
trest	trest	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perkinsová	Perkinsová	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
uvolní	uvolnit	k5eAaPmIp3nS
a	a	k8xC
zabije	zabít	k5eAaPmIp3nS
Harryho	Harry	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
cestuje	cestovat	k5eAaImIp3nS
do	do	k7c2
kostela	kostel	k1gInSc2
malého	malý	k2eAgNnSc2d1
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
slouží	sloužit	k5eAaImIp3nS
Viggovi	Vigg	k1gMnSc3
k	k	k7c3
vydírání	vydírání	k1gNnSc3
a	a	k8xC
praní	praní	k1gNnSc3
špinavých	špinavý	k2eAgInPc2d1
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
dorazí	dorazit	k5eAaPmIp3nS
Viggo	Viggo	k6eAd1
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
tým	tým	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
část	část	k1gFnSc4
zabije	zabít	k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
zaražen	zaražen	k2eAgMnSc1d1
a	a	k8xC
zajat	zajat	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
John	John	k1gMnSc1
zabit	zabit	k2eAgMnSc1d1
<g/>
,	,	kIx,
Marcus	Marcus	k1gMnSc1
znovu	znovu	k6eAd1
zasahuje	zasahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
Johnovi	John	k1gMnSc3
osvobodil	osvobodit	k5eAaPmAgMnS
<g/>
,	,	kIx,
zabil	zabít	k5eAaPmAgMnS
Kirilla	Kirilla	k1gMnSc1
a	a	k8xC
zajme	zajmout	k5eAaPmIp3nS
Viggo	Viggo	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
odhalí	odhalit	k5eAaPmIp3nS
místo	místo	k1gNnSc4
Josefa	Josef	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
pak	pak	k9
cestuje	cestovat	k5eAaImIp3nS
do	do	k7c2
bezpečného	bezpečný	k2eAgInSc2d1
domu	dům	k1gInSc2
a	a	k8xC
zabije	zabít	k5eAaPmIp3nS
Josefa	Josef	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
bodyguardy	bodyguard	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viggo	Viggo	k6eAd1
potom	potom	k6eAd1
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
zavražděn	zavražděn	k2eAgMnSc1d1
zavolá	zavolat	k5eAaPmIp3nS
Johnovi	John	k1gMnSc3
že	že	k8xS
zabil	zabít	k5eAaPmAgInS
Josefa	Josef	k1gMnSc4
rychlou	rychlý	k2eAgFnSc7d1
smrtí	smrt	k1gFnSc7
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
ale	ale	k8xC
Johnovi	John	k1gMnSc3
oznamuje	oznamovat	k5eAaImIp3nS
že	že	k8xS
zabije	zabít	k5eAaPmIp3nS
Marcuse	Marcuse	k1gFnSc1
protože	protože	k8xS
nesplnil	splnit	k5eNaPmAgMnS
svůj	svůj	k3xOyFgInSc4
úkol-zabít	úkol-zabít	k5eAaPmF
Johna	John	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
Marcus	Marcus	k1gMnSc1
nezabil	zabít	k5eNaPmAgMnS
Johna	John	k1gMnSc4
<g/>
,	,	kIx,
vloupal	vloupat	k5eAaPmAgMnS
se	se	k3xPyFc4
mu	on	k3xPp3gNnSc3
Viggo	Viggo	k6eAd1
s	s	k7c7
Perkinsonovou	Perkinsonová	k1gFnSc7
a	a	k8xC
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
bodyguardy	bodyguard	k1gMnPc7
do	do	k7c2
Marcusova	Marcusův	k2eAgInSc2d1
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Marcus	Marcus	k1gMnSc1
zjistí	zjistit	k5eAaPmIp3nS
že	že	k8xS
ho	on	k3xPp3gMnSc4
plánují	plánovat	k5eAaImIp3nP
zabít	zabít	k5eAaPmF
<g/>
,	,	kIx,
začne	začít	k5eAaPmIp3nS
se	se	k3xPyFc4
bránit	bránit	k5eAaImF
ale	ale	k8xC
je	být	k5eAaImIp3nS
postřelen	postřelit	k5eAaPmNgInS
Perkinsonovou	Perkinsonový	k2eAgFnSc7d1
a	a	k8xC
následně	následně	k6eAd1
smrtelně	smrtelně	k6eAd1
postřelen	postřelit	k5eAaPmNgMnS
Viggem	Vigg	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Winston	Winston	k1gInSc1
říká	říkat	k5eAaImIp3nS
Johnovi	John	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ho	on	k3xPp3gMnSc4
informoval	informovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Viggo	Viggo	k6eAd1
plánuje	plánovat	k5eAaImIp3nS
utéct	utéct	k5eAaPmF
vrtulníkem	vrtulník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
přijede	přijet	k5eAaPmIp3nS
na	na	k7c4
přistávací	přistávací	k2eAgFnSc4d1
rampu	rampa	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
zabíjí	zabíjet	k5eAaImIp3nS
Vigga	Vigga	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
souboji	souboj	k1gInSc6
byl	být	k5eAaImAgMnS
zraněn	zranit	k5eAaPmNgMnS
nožem	nůž	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
se	se	k3xPyFc4
vloupá	vloupat	k5eAaPmIp3nS
na	na	k7c4
kliniku	klinika	k1gFnSc4
na	na	k7c4
nábřeží	nábřeží	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
ošetřit	ošetřit	k5eAaPmF
ránu	rána	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
klinice	klinika	k1gFnSc6
najde	najít	k5eAaPmIp3nS
psa	pes	k1gMnSc4
a	a	k8xC
vezme	vzít	k5eAaPmIp3nS
si	se	k3xPyFc3
ho	on	k3xPp3gNnSc4
domů	domů	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
se	s	k7c7
psem	pes	k1gMnSc7
jdou	jít	k5eAaImIp3nP
domů	domů	k6eAd1
cestou	cesta	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgMnS
s	s	k7c7
Helenou	Helena	k1gFnSc7
naposled	naposled	k6eAd1
rande	rande	k1gNnSc1
<g/>
.	.	kIx.
<g/>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boxofficemojo	Boxofficemojo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Téma	téma	k1gNnSc1
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
John	John	k1gMnSc1
Wick	Wick	k1gMnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2015009408	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
313499500	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2015009408	#num#	k4
</s>
