<s>
Step	step	k1gFnSc1	step
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
travnaté	travnatý	k2eAgFnPc4d1	travnatá
oblasti	oblast	k1gFnPc4	oblast
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
přes	přes	k7c4	přes
9	[number]	k4	9
mil	míle	k1gFnPc2	míle
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Stepní	stepní	k2eAgNnSc1d1	stepní
klima	klima	k1gNnSc1	klima
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
horkými	horký	k2eAgNnPc7d1	horké
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
chladnými	chladný	k2eAgFnPc7d1	chladná
zimami	zima	k1gFnPc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Celoročně	celoročně	k6eAd1	celoročně
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nedostatek	nedostatek	k1gInSc4	nedostatek
srážek	srážka	k1gFnPc2	srážka
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Vegetační	vegetační	k2eAgNnSc1d1	vegetační
období	období	k1gNnSc1	období
netrvá	trvat	k5eNaImIp3nS	trvat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Stepní	stepní	k2eAgFnPc1d1	stepní
půdy	půda	k1gFnPc1	půda
bývají	bývat	k5eAaImIp3nP	bývat
velmi	velmi	k6eAd1	velmi
úrodné	úrodný	k2eAgFnPc1d1	úrodná
a	a	k8xC	a
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
stepi	step	k1gFnPc4	step
proměněny	proměněn	k2eAgFnPc4d1	proměněna
ve	v	k7c4	v
světové	světový	k2eAgFnPc4d1	světová
obilnice	obilnice	k1gFnPc4	obilnice
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
step	step	k1gInSc1	step
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
pro	pro	k7c4	pro
travnaté	travnatý	k2eAgFnPc4d1	travnatá
formace	formace	k1gFnPc4	formace
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
travnaté	travnatý	k2eAgFnPc4d1	travnatá
formace	formace	k1gFnPc4	formace
tropů	trop	k1gInPc2	trop
a	a	k8xC	a
subtropů	subtropy	k1gInPc2	subtropy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
savana	savana	k1gFnSc1	savana
<g/>
.	.	kIx.	.
</s>
<s>
Stepi	step	k1gFnPc1	step
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
území	území	k1gNnPc4	území
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Mohelenská	Mohelenský	k2eAgFnSc1d1	Mohelenská
hadcová	hadcový	k2eAgFnSc1d1	hadcová
step	step	k1gFnSc1	step
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
step	step	k1gFnSc1	step
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vegetačního	vegetační	k2eAgInSc2d1	vegetační
klidu	klid	k1gInSc2	klid
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vegetačního	vegetační	k2eAgInSc2d1	vegetační
rozkvětu	rozkvět	k1gInSc2	rozkvět
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
suché	suchý	k2eAgInPc1d1	suchý
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc1d1	mnohý
organismy	organismus	k1gInPc1	organismus
ho	on	k3xPp3gMnSc4	on
přečkávají	přečkávat	k5eAaImIp3nP	přečkávat
v	v	k7c6	v
klidových	klidový	k2eAgNnPc6d1	klidové
stádiích	stádium	k1gNnPc6	stádium
(	(	kIx(	(
<g/>
anabióza	anabióza	k1gFnSc1	anabióza
<g/>
,	,	kIx,	,
estivace	estivace	k1gFnSc1	estivace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
geobiomem	geobiom	k1gInSc7	geobiom
opadavého	opadavý	k2eAgInSc2d1	opadavý
listnatého	listnatý	k2eAgInSc2d1	listnatý
lesa	les	k1gInSc2	les
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
přechodná	přechodný	k2eAgFnSc1d1	přechodná
lesostep	lesostep	k1gFnSc1	lesostep
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
lesostepi	lesostep	k1gFnPc4	lesostep
přeměněny	přeměněn	k2eAgFnPc4d1	přeměněna
v	v	k7c4	v
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
step	step	k1gFnSc1	step
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
pusta	pusta	k1gFnSc1	pusta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
antropogenně	antropogenně	k6eAd1	antropogenně
podmíněnou	podmíněný	k2eAgFnSc7d1	podmíněná
stepí	step	k1gFnSc7	step
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
vykáceny	vykácen	k2eAgInPc1d1	vykácen
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
značné	značný	k2eAgFnPc4d1	značná
plochy	plocha	k1gFnPc4	plocha
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
a	a	k8xC	a
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
a	a	k8xC	a
období	období	k1gNnSc1	období
vegetačního	vegetační	k2eAgInSc2d1	vegetační
klidu	klid	k1gInSc2	klid
zde	zde	k6eAd1	zde
panuje	panovat	k5eAaImIp3nS	panovat
od	od	k7c2	od
září	září	k1gNnSc2	září
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
může	moct	k5eAaImIp3nS	moct
chybět	chybět	k5eAaImF	chybět
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
půda	půda	k1gFnSc1	půda
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
suchá	suchý	k2eAgFnSc1d1	suchá
a	a	k8xC	a
k	k	k7c3	k
vegetačnímu	vegetační	k2eAgInSc3d1	vegetační
rozvoji	rozvoj	k1gInSc3	rozvoj
dochází	docházet	k5eAaImIp3nS	docházet
až	až	k9	až
v	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horských	horský	k2eAgInPc6d1	horský
hřbetech	hřbet	k1gInPc6	hřbet
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
pozici	pozice	k1gFnSc4	pozice
horské	horský	k2eAgFnSc2d1	horská
luční	luční	k2eAgFnSc2d1	luční
stepi	step	k1gFnSc2	step
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
přecházejí	přecházet	k5eAaImIp3nP	přecházet
v	v	k7c4	v
březové	březový	k2eAgInPc4d1	březový
lesy	les	k1gInPc4	les
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c4	v
jehličnatou	jehličnatý	k2eAgFnSc4d1	jehličnatá
tajgu	tajga	k1gFnSc4	tajga
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
klima	klima	k1gNnSc1	klima
má	mít	k5eAaImIp3nS	mít
subhumidní	subhumidní	k2eAgInSc4d1	subhumidní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgInPc1d1	průměrný
roční	roční	k2eAgInPc1d1	roční
úhrny	úhrn	k1gInPc1	úhrn
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
500	[number]	k4	500
mm	mm	kA	mm
v	v	k7c6	v
chladnějších	chladný	k2eAgFnPc6d2	chladnější
oblastech	oblast	k1gFnPc6	oblast
až	až	k6eAd1	až
k	k	k7c3	k
1000	[number]	k4	1000
mm	mm	kA	mm
v	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Teplot	teplota	k1gFnPc2	teplota
zde	zde	k6eAd1	zde
ubývá	ubývat	k5eAaImIp3nS	ubývat
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
severu	sever	k1gInSc2	sever
a	a	k8xC	a
srážek	srážka	k1gFnPc2	srážka
ubývá	ubývat	k5eAaImIp3nS	ubývat
od	od	k7c2	od
východu	východ	k1gInSc2	východ
k	k	k7c3	k
západu	západ	k1gInSc3	západ
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
zákonité	zákonitý	k2eAgNnSc1d1	zákonité
rozčlenění	rozčlenění	k1gNnSc1	rozčlenění
severoamerických	severoamerický	k2eAgFnPc2d1	severoamerická
stepí	step	k1gFnPc2	step
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
pásy	pás	k1gInPc4	pás
probíhající	probíhající	k2eAgInSc4d1	probíhající
severojižním	severojižní	k2eAgInSc7d1	severojižní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
východu	východ	k1gInSc2	východ
k	k	k7c3	k
západu	západ	k1gInSc3	západ
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
:	:	kIx,	:
lesostep	lesostep	k1gFnSc1	lesostep
<g/>
,	,	kIx,	,
dlouhostébelnatá	dlouhostébelnatý	k2eAgFnSc1d1	dlouhostébelnatý
prérie	prérie	k1gFnSc1	prérie
<g/>
,	,	kIx,	,
smíšená	smíšený	k2eAgFnSc1d1	smíšená
prérie	prérie	k1gFnSc1	prérie
a	a	k8xC	a
krátkostébelnatá	krátkostébelnatý	k2eAgFnSc1d1	krátkostébelnatý
prérie	prérie	k1gFnSc1	prérie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dlouhostébelnatých	dlouhostébelnatý	k2eAgFnPc6d1	dlouhostébelnatý
neboli	neboli	k8xC	neboli
pravých	pravý	k2eAgFnPc6d1	pravá
prériích	prérie	k1gFnPc6	prérie
činí	činit	k5eAaImIp3nS	činit
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
zhruba	zhruba	k6eAd1	zhruba
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Četné	četný	k2eAgInPc4d1	četný
požáry	požár	k1gInPc4	požár
a	a	k8xC	a
kdysi	kdysi	k6eAd1	kdysi
velká	velký	k2eAgNnPc1d1	velké
stáda	stádo	k1gNnPc1	stádo
bizonů	bizon	k1gMnPc2	bizon
znemožňovaly	znemožňovat	k5eAaImAgInP	znemožňovat
uchycení	uchycení	k1gNnSc4	uchycení
semenáčků	semenáček	k1gMnPc2	semenáček
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Dominujícím	dominující	k2eAgInSc7d1	dominující
druhem	druh	k1gInSc7	druh
v	v	k7c6	v
dlouhostébelnaté	dlouhostébelnatý	k2eAgFnSc6d1	dlouhostébelnatý
prérii	prérie	k1gFnSc6	prérie
je	být	k5eAaImIp3nS	být
až	až	k9	až
2	[number]	k4	2
metry	metr	k1gInPc4	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
tráva	tráva	k1gFnSc1	tráva
vousatka	vousatka	k1gFnSc1	vousatka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přechodných	přechodný	k2eAgFnPc6d1	přechodná
smíšených	smíšený	k2eAgFnPc6d1	smíšená
prériích	prérie	k1gFnPc6	prérie
začínají	začínat	k5eAaImIp3nP	začínat
vysoké	vysoký	k2eAgFnPc1d1	vysoká
traviny	travina	k1gFnPc1	travina
ustupovat	ustupovat	k5eAaImF	ustupovat
travinám	travina	k1gFnPc3	travina
nízkým	nízký	k2eAgFnPc3d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
prérie	prérie	k1gFnPc1	prérie
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgFnP	využívat
jako	jako	k8xS	jako
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
Krátkostébelnaté	Krátkostébelnatý	k2eAgFnPc1d1	Krátkostébelnatý
prérie	prérie	k1gFnPc1	prérie
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
území	území	k1gNnSc4	území
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
Rocky	rock	k1gInPc4	rock
Mountains	Mountainsa	k1gFnPc2	Mountainsa
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
semiaridní	semiaridní	k2eAgInSc1d1	semiaridní
s	s	k7c7	s
úhrnem	úhrn	k1gInSc7	úhrn
srážek	srážka	k1gFnPc2	srážka
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
450	[number]	k4	450
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Půdy	půda	k1gFnPc1	půda
jsou	být	k5eAaImIp3nP	být
kaštanozemě	kaštanozemě	k6eAd1	kaštanozemě
<g/>
.	.	kIx.	.
</s>
<s>
Převládá	převládat	k5eAaImIp3nS	převládat
zde	zde	k6eAd1	zde
nízká	nízký	k2eAgFnSc1d1	nízká
"	"	kIx"	"
<g/>
bizoní	bizoní	k2eAgFnSc1d1	bizoní
<g/>
"	"	kIx"	"
tráva	tráva	k1gFnSc1	tráva
nebo	nebo	k8xC	nebo
tráva	tráva	k1gFnSc1	tráva
"	"	kIx"	"
<g/>
grama	grama	k1gFnSc1	grama
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pampa	pampa	k1gFnSc1	pampa
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
stepmi	step	k1gFnPc7	step
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
zde	zde	k6eAd1	zde
panují	panovat	k5eAaImIp3nP	panovat
příznivější	příznivý	k2eAgFnPc4d2	příznivější
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
prakticky	prakticky	k6eAd1	prakticky
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
mrazy	mráz	k1gInPc1	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
relativně	relativně	k6eAd1	relativně
vysoké	vysoká	k1gFnPc1	vysoká
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
patrně	patrně	k6eAd1	patrně
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
i	i	k9	i
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
činností	činnost	k1gFnSc7	činnost
člověka	člověk	k1gMnSc2	člověk
byly	být	k5eAaImAgFnP	být
odstraněny	odstraněn	k2eAgFnPc1d1	odstraněna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
pampa	pampa	k1gFnSc1	pampa
velmi	velmi	k6eAd1	velmi
hustě	hustě	k6eAd1	hustě
osídlena	osídlit	k5eAaPmNgFnS	osídlit
a	a	k8xC	a
využívána	využívat	k5eAaImNgFnS	využívat
k	k	k7c3	k
zemědělství	zemědělství	k1gNnSc3	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
skupinu	skupina	k1gFnSc4	skupina
představují	představovat	k5eAaImIp3nP	představovat
dobře	dobře	k6eAd1	dobře
pohybliví	pohyblivý	k2eAgMnPc1d1	pohyblivý
kopytníci	kopytník	k1gMnPc1	kopytník
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
drobní	drobný	k2eAgMnPc1d1	drobný
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
šelmy	šelma	k1gFnPc1	šelma
a	a	k8xC	a
draví	dravý	k2eAgMnPc1d1	dravý
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
býložravců	býložravec	k1gMnPc2	býložravec
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
sajga	sajga	k1gFnSc1	sajga
tatarská	tatarský	k2eAgFnSc1d1	tatarská
<g/>
,	,	kIx,	,
osel	osel	k1gMnSc1	osel
kulan	kulan	k1gMnSc1	kulan
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
kůň	kůň	k1gMnSc1	kůň
Převalského	převalský	k2eAgNnSc2d1	převalský
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
stepní	stepní	k2eAgMnSc1d1	stepní
svišť	svišť	k1gMnSc1	svišť
bobak	bobak	k1gMnSc1	bobak
<g/>
,	,	kIx,	,
sysel	sysel	k1gMnSc1	sysel
<g/>
,	,	kIx,	,
křeček	křeček	k1gMnSc1	křeček
<g/>
,	,	kIx,	,
slepec	slepec	k1gMnSc1	slepec
<g/>
,	,	kIx,	,
zajíc	zajíc	k1gMnSc1	zajíc
<g/>
,	,	kIx,	,
krtek	krtek	k1gMnSc1	krtek
<g/>
,	,	kIx,	,
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
stepní	stepní	k2eAgMnPc1d1	stepní
neboli	neboli	k8xC	neboli
manul	manul	k1gMnSc1	manul
<g/>
,	,	kIx,	,
rys	rys	k1gMnSc1	rys
karakal	karakal	k1gMnSc1	karakal
<g/>
,	,	kIx,	,
orel	orel	k1gMnSc1	orel
stepní	stepní	k2eAgMnSc1d1	stepní
<g/>
,	,	kIx,	,
sup	sup	k1gMnSc1	sup
<g/>
,	,	kIx,	,
bažant	bažant	k1gMnSc1	bažant
a	a	k8xC	a
jeřáb	jeřáb	k1gMnSc1	jeřáb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zde	zde	k6eAd1	zde
žila	žít	k5eAaImAgNnP	žít
ohromná	ohromný	k2eAgNnPc1d1	ohromné
stáda	stádo	k1gNnPc1	stádo
bizonů	bizon	k1gMnPc2	bizon
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
přežívají	přežívat	k5eAaImIp3nP	přežívat
v	v	k7c6	v
rezervacích	rezervace	k1gFnPc6	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
prostředí	prostředí	k1gNnSc4	prostředí
typický	typický	k2eAgMnSc1d1	typický
vidloroh	vidloroh	k1gMnSc1	vidloroh
americký	americký	k2eAgMnSc1d1	americký
<g/>
,	,	kIx,	,
kojot	kojot	k1gMnSc1	kojot
prériový	prériový	k2eAgMnSc1d1	prériový
<g/>
,	,	kIx,	,
psoun	psoun	k1gMnSc1	psoun
prériový	prériový	k2eAgMnSc1d1	prériový
<g/>
,	,	kIx,	,
tetřívek	tetřívek	k1gMnSc1	tetřívek
prériový	prériový	k2eAgMnSc1d1	prériový
a	a	k8xC	a
sova	sova	k1gFnSc1	sova
zemní	zemní	k2eAgFnSc2d1	zemní
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
mara	mara	k6eAd1	mara
stepní	stepní	k2eAgMnSc1d1	stepní
neboli	neboli	k8xC	neboli
zajíc	zajíc	k1gMnSc1	zajíc
pampový	pampový	k2eAgMnSc1d1	pampový
<g/>
,	,	kIx,	,
jelenec	jelenec	k1gMnSc1	jelenec
quazny	quazna	k1gFnSc2	quazna
<g/>
,	,	kIx,	,
pudu	pud	k1gInSc2	pud
jižní	jižní	k2eAgMnSc1d1	jižní
neboli	neboli	k8xC	neboli
jelínek	jelínek	k1gMnSc1	jelínek
pudu	pud	k1gInSc2	pud
<g/>
,	,	kIx,	,
nandu	nandu	k1gMnSc1	nandu
pampový	pampový	k2eAgMnSc1d1	pampový
<g/>
,	,	kIx,	,
nandu	nandu	k1gMnSc1	nandu
stepní	stepní	k2eAgMnSc1d1	stepní
a	a	k8xC	a
lama	lama	k1gMnSc1	lama
guanako	guanako	k6eAd1	guanako
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
stepí	step	k1gFnPc2	step
historicky	historicky	k6eAd1	historicky
nepřálo	přát	k5eNaImAgNnS	přát
usedlému	usedlý	k2eAgInSc3d1	usedlý
životnímu	životní	k2eAgInSc3d1	životní
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nenacházelo	nacházet	k5eNaImAgNnS	nacházet
příliš	příliš	k6eAd1	příliš
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
srážky	srážka	k1gFnPc1	srážka
byly	být	k5eAaImAgFnP	být
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
nomádi	nomád	k1gMnPc1	nomád
živící	živící	k2eAgFnSc2d1	živící
se	se	k3xPyFc4	se
pastevectvím	pastevectví	k1gNnSc7	pastevectví
a	a	k8xC	a
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
jako	jako	k8xC	jako
dopravním	dopravní	k2eAgInSc6d1	dopravní
prostředku	prostředek	k1gInSc6	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
snadná	snadný	k2eAgFnSc1d1	snadná
doprava	doprava	k1gFnSc1	doprava
materiálů	materiál	k1gInPc2	materiál
pomocí	pomocí	k7c2	pomocí
železnice	železnice	k1gFnSc2	železnice
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
umělé	umělý	k2eAgNnSc1d1	umělé
zavlažování	zavlažování	k1gNnSc1	zavlažování
umožnily	umožnit	k5eAaPmAgInP	umožnit
většinu	většina	k1gFnSc4	většina
stepních	stepní	k2eAgFnPc2d1	stepní
oblastí	oblast	k1gFnPc2	oblast
zúrodnit	zúrodnit	k5eAaPmF	zúrodnit
<g/>
.	.	kIx.	.
</s>
<s>
Euroasijské	euroasijský	k2eAgFnPc1d1	euroasijská
stepi	step	k1gFnPc1	step
byly	být	k5eAaImAgFnP	být
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
domovem	domov	k1gInSc7	domov
rozličných	rozličný	k2eAgInPc2d1	rozličný
kočovných	kočovný	k2eAgInPc2d1	kočovný
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
často	často	k6eAd1	často
napadaly	napadat	k5eAaBmAgFnP	napadat
zemědělské	zemědělský	k2eAgFnPc1d1	zemědělská
civilizace	civilizace	k1gFnPc1	civilizace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Kimmeriové	Kimmeriový	k2eAgFnPc1d1	Kimmeriový
<g/>
,	,	kIx,	,
Skythové	Skyth	k1gMnPc1	Skyth
a	a	k8xC	a
Sarmati	Sarmat	k1gMnPc1	Sarmat
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
stěhování	stěhování	k1gNnPc2	stěhování
národů	národ	k1gInPc2	národ
Hunové	Hun	k1gMnPc1	Hun
a	a	k8xC	a
Avaři	Avar	k1gMnPc1	Avar
a	a	k8xC	a
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
obávaní	obávaný	k2eAgMnPc1d1	obávaný
Mongolové	Mongol	k1gMnPc1	Mongol
(	(	kIx(	(
<g/>
Tataři	Tatar	k1gMnPc1	Tatar
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
turkičtí	turkický	k2eAgMnPc1d1	turkický
Kumáni	Kumán	k1gMnPc1	Kumán
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
Mongolů	Mongol	k1gMnPc2	Mongol
usadili	usadit	k5eAaPmAgMnP	usadit
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
území	území	k1gNnSc6	území
Malé	Malé	k2eAgFnSc2d1	Malé
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Kumánie	Kumánie	k1gFnSc2	Kumánie
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
osídlení	osídlení	k1gNnSc1	osídlení
jihoruských	jihoruský	k2eAgFnPc2d1	jihoruský
a	a	k8xC	a
ukrajinských	ukrajinský	k2eAgFnPc2d1	ukrajinská
stepí	step	k1gFnPc2	step
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Divoká	divoký	k2eAgFnSc1d1	divoká
pole	pole	k1gFnSc1	pole
<g/>
)	)	kIx)	)
neumožňovaly	umožňovat	k5eNaImAgFnP	umožňovat
až	až	k9	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nájezdy	nájezd	k1gInPc7	nájezd
obávaných	obávaný	k2eAgInPc2d1	obávaný
Krymských	krymský	k2eAgInPc2d1	krymský
Tatarů	tatar	k1gInPc2	tatar
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
historii	historie	k1gFnSc4	historie
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
ba	ba	k9	ba
dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
ještě	ještě	k9	ještě
hůř	zle	k6eAd2	zle
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oblast	oblast	k1gFnSc1	oblast
stepí	step	k1gFnPc2	step
byla	být	k5eAaImAgFnS	být
blíže	blízce	k6eAd2	blízce
<g/>
,	,	kIx,	,
ostřeji	ostro	k6eAd2	ostro
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
dostupná	dostupný	k2eAgFnSc1d1	dostupná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přimělo	přimět	k5eAaPmAgNnS	přimět
starověké	starověký	k2eAgFnPc4d1	starověká
Číňany	Číňan	k1gMnPc4	Číňan
k	k	k7c3	k
postavení	postavení	k1gNnSc3	postavení
slavné	slavný	k2eAgFnSc2d1	slavná
Velké	velký	k2eAgFnSc2d1	velká
čínské	čínský	k2eAgFnSc2d1	čínská
zdi	zeď	k1gFnSc2	zeď
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
kočovnými	kočovný	k2eAgInPc7d1	kočovný
nájezdy	nájezd	k1gInPc7	nájezd
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
byla	být	k5eAaImAgFnS	být
Čína	Čína	k1gFnSc1	Čína
kočovníky	kočovník	k1gMnPc4	kočovník
několikrát	několikrát	k6eAd1	několikrát
ovládnuta	ovládnut	k2eAgFnSc1d1	ovládnuta
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Mongoly	mongol	k1gInPc1	mongol
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
dynastii	dynastie	k1gFnSc4	dynastie
Jüan	jüan	k1gInSc1	jüan
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Čína	Čína	k1gFnSc1	Čína
zničila	zničit	k5eAaPmAgFnS	zničit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
velkých	velký	k2eAgFnPc2d1	velká
nomádských	nomádský	k2eAgFnPc2d1	nomádská
říší	říš	k1gFnPc2	říš
Džúngarský	Džúngarský	k2eAgInSc4d1	Džúngarský
chanát	chanát	k1gInSc4	chanát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
byly	být	k5eAaImAgFnP	být
prérie	prérie	k1gFnPc1	prérie
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
osídleny	osídlen	k2eAgMnPc4d1	osídlen
Indiány	Indián	k1gMnPc4	Indián
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
bělochů	běloch	k1gMnPc2	běloch
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přivezli	přivézt	k5eAaPmAgMnP	přivézt
zvíře	zvíře	k1gNnSc4	zvíře
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgFnPc1d1	důležitá
k	k	k7c3	k
přežití	přežití	k1gNnSc3	přežití
ve	v	k7c6	v
stepi	step	k1gFnSc6	step
-	-	kIx~	-
koně	kůň	k1gMnPc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kultury	kultura	k1gFnPc1	kultura
však	však	k9	však
byly	být	k5eAaImAgFnP	být
o	o	k7c4	o
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
později	pozdě	k6eAd2	pozdě
zničeny	zničit	k5eAaPmNgInP	zničit
rozpínajícími	rozpínající	k2eAgFnPc7d1	rozpínající
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
prérie	prérie	k1gFnSc1	prérie
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
rozorány	rozorán	k2eAgFnPc1d1	rozorána
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
dodnes	dodnes	k6eAd1	dodnes
některé	některý	k3yIgInPc1	některý
kmeny	kmen	k1gInPc1	kmen
přežívají	přežívat	k5eAaImIp3nP	přežívat
v	v	k7c6	v
rezervacích	rezervace	k1gFnPc6	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
část	část	k1gFnSc1	část
eurasijských	eurasijský	k2eAgFnPc2d1	eurasijská
stepí	step	k1gFnPc2	step
–	–	k?	–
nejprve	nejprve	k6eAd1	nejprve
byly	být	k5eAaImAgFnP	být
zúrodněny	zúrodněn	k2eAgFnPc4d1	zúrodněna
stepi	step	k1gFnPc4	step
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
evropském	evropský	k2eAgNnSc6d1	Evropské
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
i	i	k9	i
celiny	celina	k1gFnPc1	celina
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
zúrodnění	zúrodnění	k1gNnSc4	zúrodnění
celin	celina	k1gFnPc2	celina
se	se	k3xPyFc4	se
však	však	k9	však
nesetkal	setkat	k5eNaPmAgMnS	setkat
s	s	k7c7	s
přílišným	přílišný	k2eAgInSc7d1	přílišný
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
suchému	suchý	k2eAgNnSc3d1	suché
klimatu	klima	k1gNnSc3	klima
<g/>
.	.	kIx.	.
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
step	step	k1gFnSc1	step
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
nevyužitelná	využitelný	k2eNgFnSc1d1	nevyužitelná
kvůli	kvůli	k7c3	kvůli
nehostinnému	hostinný	k2eNgNnSc3d1	nehostinné
klimatu	klima	k1gNnSc3	klima
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
si	se	k3xPyFc3	se
některé	některý	k3yIgInPc4	některý
národy	národ	k1gInPc4	národ
eurasijských	eurasijský	k2eAgFnPc2d1	eurasijská
stepí	step	k1gFnPc2	step
dodnes	dodnes	k6eAd1	dodnes
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
kočovný	kočovný	k2eAgInSc4d1	kočovný
styl	styl	k1gInSc4	styl
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
Mongoly	Mongol	k1gMnPc4	Mongol
a	a	k8xC	a
Kazachy	Kazach	k1gMnPc4	Kazach
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
dnes	dnes	k6eAd1	dnes
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
postupné	postupný	k2eAgFnSc3d1	postupná
urbanizaci	urbanizace	k1gFnSc3	urbanizace
-	-	kIx~	-
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
do	do	k7c2	do
měst	město	k1gNnPc2	město
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
příslibem	příslib	k1gInSc7	příslib
lepší	dobrý	k2eAgFnSc2d2	lepší
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Louka	louka	k1gFnSc1	louka
Savana	savana	k1gFnSc1	savana
Stepní	stepní	k2eAgMnPc1d1	stepní
běžci	běžec	k1gMnPc1	běžec
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
step	step	k1gInSc1	step
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
step	step	k1gFnSc1	step
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
