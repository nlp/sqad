<s>
Johann	Johann	k1gMnSc1	Johann
Rudolph	Rudolph	k1gMnSc1	Rudolph
Schellenberg	Schellenberg	k1gMnSc1	Schellenberg
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1740	[number]	k4	1740
Basilej	Basilej	k1gFnSc1	Basilej
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1806	[number]	k4	1806
<g/>
,	,	kIx,	,
Töss	Töss	k1gInSc1	Töss
u	u	k7c2	u
Winterthuru	Winterthur	k1gInSc2	Winterthur
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
a	a	k8xC	a
entomolog	entomolog	k1gMnSc1	entomolog
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
-	-	kIx~	-
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
a	a	k8xC	a
rytec	rytec	k1gMnSc1	rytec
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
hlavně	hlavně	k6eAd1	hlavně
dvoukřídlými	dvoukřídlí	k1gMnPc7	dvoukřídlí
(	(	kIx(	(
<g/>
Diptera	Dipter	k1gMnSc4	Dipter
<g/>
)	)	kIx)	)
a	a	k8xC	a
brouky	brouk	k1gMnPc4	brouk
(	(	kIx(	(
<g/>
Coleoptera	Coleopter	k1gMnSc4	Coleopter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
některé	některý	k3yIgMnPc4	některý
nové	nový	k2eAgMnPc4d1	nový
druhy	druh	k1gMnPc4	druh
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
švýcarským	švýcarský	k2eAgMnPc3d1	švýcarský
ilustrátorům	ilustrátor	k1gMnPc3	ilustrátor
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
švýcarských	švýcarský	k2eAgMnPc2d1	švýcarský
rytců	rytec	k1gMnPc2	rytec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
mnoho	mnoho	k4c4	mnoho
botanických	botanický	k2eAgFnPc2d1	botanická
entomologických	entomologický	k2eAgFnPc2d1	entomologická
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
díla	dílo	k1gNnSc2	dílo
Die	Die	k1gFnSc2	Die
Kennzeichen	Kennzeichna	k1gFnPc2	Kennzeichna
der	drát	k5eAaImRp2nS	drát
Insekten	Insekten	k2eAgInSc4d1	Insekten
<g/>
,	,	kIx,	,
nach	nach	k1gInSc4	nach
Anleitung	Anleitunga	k1gFnPc2	Anleitunga
des	des	k1gNnSc1	des
Königl	Königl	k1gMnSc1	Königl
<g/>
.	.	kIx.	.
</s>
<s>
Schwed	Schwed	k1gMnSc1	Schwed
<g/>
.	.	kIx.	.
</s>
<s>
Ritters	Ritters	k1gInSc1	Ritters
und	und	k?	und
Leibarzts	Leibarzts	k1gInSc1	Leibarzts
Karl	Karl	k1gMnSc1	Karl
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
<g/>
.	.	kIx.	.
</s>
<s>
Mit	Mit	k?	Mit
einer	einer	k1gInSc1	einer
Vorrede	Vorred	k1gMnSc5	Vorred
des	des	k1gNnSc1	des
Herrn	Herrn	k1gInSc1	Herrn
Johannes	Johannes	k1gMnSc1	Johannes
Gessners	Gessners	k1gInSc1	Gessners
od	od	k7c2	od
Johanna	Johann	k1gMnSc2	Johann
Heinricha	Heinrich	k1gMnSc2	Heinrich
Sulzera	Sulzer	k1gMnSc2	Sulzer
<g/>
,	,	kIx,	,
vydaného	vydaný	k2eAgMnSc2d1	vydaný
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1761	[number]	k4	1761
<g/>
;	;	kIx,	;
Genera	Genera	k1gFnSc1	Genera
Insectorum	Insectorum	k1gInSc1	Insectorum
Linnaei	Linnae	k1gFnSc2	Linnae
et	et	k?	et
Fabricii	Fabricie	k1gFnSc3	Fabricie
iconibus	iconibus	k1gMnSc1	iconibus
illustrata	illustrata	k1gFnSc1	illustrata
od	od	k7c2	od
Johann	Johanna	k1gFnPc2	Johanna
Jacob	Jacoba	k1gFnPc2	Jacoba
Roemera	Roemer	k1gMnSc2	Roemer
vydaném	vydaný	k2eAgNnSc6d1	vydané
Steinerem	Steiner	k1gMnSc7	Steiner
ve	v	k7c6	v
Winterthur	Winterthur	k1gMnSc1	Winterthur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
in	in	k?	in
1789	[number]	k4	1789
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
práci	práce	k1gFnSc4	práce
Genres	Genresa	k1gFnPc2	Genresa
des	des	k1gNnPc2	des
mouches	mouches	k1gMnSc1	mouches
Diptè	Diptè	k1gFnSc2	Diptè
représentés	représentés	k1gInSc1	représentés
en	en	k?	en
XLII	XLII	kA	XLII
planches	planches	k1gMnSc1	planches
projettées	projettées	k1gMnSc1	projettées
et	et	k?	et
dessinées	dessinées	k1gMnSc1	dessinées
et	et	k?	et
expliquées	expliquées	k1gInSc1	expliquées
par	para	k1gFnPc2	para
deux	deux	k1gInSc1	deux
amateurs	amateurs	k1gInSc4	amateurs
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
entomologie	entomologie	k1gFnSc1	entomologie
<g/>
,	,	kIx,	,
vydaným	vydaný	k2eAgNnSc7d1	vydané
v	v	k7c6	v
Zurichu	Zurich	k1gInSc6	Zurich
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
má	mít	k5eAaImIp3nS	mít
42	[number]	k4	42
tabulek	tabulka	k1gFnPc2	tabulka
ilustrací	ilustrace	k1gFnPc2	ilustrace
<g/>
.	.	kIx.	.
</s>
<s>
Pour	Pour	k1gMnSc1	Pour
raillerie	raillerie	k1gFnSc2	raillerie
(	(	kIx(	(
<g/>
1772	[number]	k4	1772
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Freund	Freund	k1gMnSc1	Freund
Hein	Hein	k1gMnSc1	Hein
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Erscheinungen	Erscheinungen	k1gInSc1	Erscheinungen
<g/>
,	,	kIx,	,
ein	ein	k?	ein
Todtentanz	Todtentanz	k1gInSc1	Todtentanz
in	in	k?	in
24	[number]	k4	24
Blättern	Blättern	k1gInSc1	Blättern
(	(	kIx(	(
<g/>
Verse	verse	k1gFnSc1	verse
von	von	k1gInSc1	von
Musaeus	Musaeus	k1gInSc1	Musaeus
<g/>
;	;	kIx,	;
1785	[number]	k4	1785
<g/>
)	)	kIx)	)
Plantes	Plantes	k1gInSc1	Plantes
et	et	k?	et
arbustes	arbustes	k1gInSc1	arbustes
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
agrément	agrément	k1gNnSc1	agrément
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
<g/>
-	-	kIx~	-
<g/>
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
Sittenlehre	Sittenlehr	k1gInSc5	Sittenlehr
in	in	k?	in
Fabeln	Fabeln	k1gInSc1	Fabeln
und	und	k?	und
Erzählungen	Erzählungen	k1gInSc1	Erzählungen
für	für	k?	für
die	die	k?	die
Jugend	Jugend	k1gInSc1	Jugend
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
Kurze	kurz	k1gInSc6	kurz
Abhandlung	Abhandlung	k1gMnSc1	Abhandlung
über	über	k1gMnSc1	über
die	die	k?	die
Ätzkunst	Ätzkunst	k1gMnSc1	Ätzkunst
(	(	kIx(	(
<g/>
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
Collection	Collection	k1gInSc1	Collection
choisie	choisie	k1gFnSc2	choisie
de	de	k?	de
plantes	plantes	k1gMnSc1	plantes
et	et	k?	et
arbustes	arbustes	k1gMnSc1	arbustes
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
)	)	kIx)	)
Das	Das	k1gMnSc1	Das
Geschlecht	Geschlecht	k1gMnSc1	Geschlecht
der	drát	k5eAaImRp2nS	drát
Land-	Land-	k1gFnSc2	Land-
und	und	k?	und
Wasserwanzen	Wasserwanzen	k2eAgInSc4d1	Wasserwanzen
nach	nach	k1gInSc4	nach
Familien	Familien	k2eAgInSc1d1	Familien
geordnet	geordnet	k1gInSc1	geordnet
(	(	kIx(	(
<g/>
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
Entomologische	Entomologisch	k1gInSc2	Entomologisch
Beyträge	Beyträge	k1gInSc1	Beyträge
(	(	kIx(	(
<g/>
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
Gattungen	Gattungen	k1gInSc1	Gattungen
der	drát	k5eAaImRp2nS	drát
Fliegen	Fliegen	k1gInSc1	Fliegen
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
</s>
