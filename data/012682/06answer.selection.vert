<s>
Parní	parní	k2eAgInSc1d1
stroj	stroj	k1gInSc1
je	být	k5eAaImIp3nS
pístový	pístový	k2eAgInSc1d1
tepelný	tepelný	k2eAgInSc1d1
stroj	stroj	k1gInSc1
<g/>
,	,	kIx,
spalovací	spalovací	k2eAgInSc1d1
motor	motor	k1gInSc1
s	s	k7c7
vnějším	vnější	k2eAgNnSc7d1
spalováním	spalování	k1gNnSc7
<g/>
,	,	kIx,
přeměňující	přeměňující	k2eAgInPc1d1
tepelnou	tepelný	k2eAgFnSc4d1
energii	energie	k1gFnSc4
vodní	vodní	k2eAgFnSc2d1
páry	pára	k1gFnSc2
na	na	k7c4
energii	energie	k1gFnSc4
mechanickou	mechanický	k2eAgFnSc4d1
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
rotační	rotační	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
<g/>
.	.	kIx.
</s>