<s>
Nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
umožnit	umožnit	k5eAaPmF	umožnit
vytvoření	vytvoření	k1gNnSc1	vytvoření
silnější	silný	k2eAgFnSc2d2	silnější
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1787	[number]	k4	1787
na	na	k7c6	na
ústavním	ústavní	k2eAgInSc6d1	ústavní
konventu	konvent	k1gInSc6	konvent
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
v	v	k7c4	v
Pensylvánii	Pensylvánie	k1gFnSc4	Pensylvánie
a	a	k8xC	a
"	"	kIx"	"
<g/>
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
ratifikována	ratifikován	k2eAgFnSc1d1	ratifikována
konventy	konvent	k1gInPc1	konvent
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
