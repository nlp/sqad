<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Constitution	Constitution	k1gInSc1	Constitution
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc4d1	základní
zákon	zákon	k1gInSc4	zákon
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
rámcem	rámec	k1gInSc7	rámec
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
moci	moc	k1gFnSc2	moc
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
pro	pro	k7c4	pro
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
složkami	složka	k1gFnPc7	složka
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
občany	občan	k1gMnPc7	občan
a	a	k8xC	a
všemi	všecek	k3xTgMnPc7	všecek
lidmi	člověk	k1gMnPc7	člověk
na	na	k7c6	na
území	území	k1gNnSc6	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
Ústava	ústava	k1gFnSc1	ústava
<g/>
)	)	kIx)	)
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
republikánské	republikánský	k2eAgNnSc4d1	republikánské
zřízení	zřízení	k1gNnSc4	zřízení
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
pilíři	pilíř	k1gInPc7	pilíř
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
:	:	kIx,	:
mocí	moc	k1gFnPc2	moc
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
<g/>
,	,	kIx,	,
představovanou	představovaný	k2eAgFnSc4d1	představovaná
dvoukomorovým	dvoukomorový	k2eAgInSc7d1	dvoukomorový
Kongresem	kongres	k1gInSc7	kongres
<g/>
,	,	kIx,	,
mocí	moc	k1gFnSc7	moc
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
mocí	moc	k1gFnSc7	moc
soudní	soudní	k2eAgFnSc1d1	soudní
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
vrcholu	vrchol	k1gInSc6	vrchol
stojí	stát	k5eAaImIp3nS	stát
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
dosah	dosah	k1gInSc4	dosah
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
každé	každý	k3xTgFnSc2	každý
ze	z	k7c2	z
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
složek	složka	k1gFnPc2	složka
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
americkou	americký	k2eAgFnSc7d1	americká
ústavou	ústava	k1gFnSc7	ústava
byly	být	k5eAaImAgFnP	být
tzv.	tzv.	kA	tzv.
Články	článek	k1gInPc1	článek
Konfederace	konfederace	k1gFnSc2	konfederace
a	a	k8xC	a
trvalé	trvalý	k2eAgFnSc2d1	trvalá
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
dávaly	dávat	k5eAaImAgFnP	dávat
centrální	centrální	k2eAgFnSc3d1	centrální
vládě	vláda	k1gFnSc3	vláda
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezenou	omezený	k2eAgFnSc4d1	omezená
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Jimi	on	k3xPp3gInPc7	on
ustanovený	ustanovený	k2eAgInSc4d1	ustanovený
Kontinentální	kontinentální	k2eAgInSc4d1	kontinentální
kongres	kongres	k1gInSc4	kongres
trpěl	trpět	k5eAaImAgMnS	trpět
trvale	trvale	k6eAd1	trvale
nedostatkem	nedostatek	k1gInSc7	nedostatek
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
prakticky	prakticky	k6eAd1	prakticky
znemožňovalo	znemožňovat	k5eAaImAgNnS	znemožňovat
jeho	jeho	k3xOp3gFnSc4	jeho
akceschopnost	akceschopnost	k1gFnSc4	akceschopnost
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
umožnit	umožnit	k5eAaPmF	umožnit
vytvoření	vytvoření	k1gNnSc1	vytvoření
silnější	silný	k2eAgFnSc2d2	silnější
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1787	[number]	k4	1787
na	na	k7c6	na
ústavním	ústavní	k2eAgInSc6d1	ústavní
konventu	konvent	k1gInSc6	konvent
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
v	v	k7c4	v
Pensylvánii	Pensylvánie	k1gFnSc4	Pensylvánie
a	a	k8xC	a
"	"	kIx"	"
<g/>
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
ratifikována	ratifikován	k2eAgFnSc1d1	ratifikována
konventy	konvent	k1gInPc1	konvent
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
přijato	přijmout	k5eAaPmNgNnS	přijmout
celkem	celek	k1gInSc7	celek
27	[number]	k4	27
dodatků	dodatek	k1gInPc2	dodatek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
prvních	první	k4xOgInPc2	první
deset	deset	k4xCc4	deset
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
je	být	k5eAaImIp3nS	být
Ústava	ústava	k1gFnSc1	ústava
včetně	včetně	k7c2	včetně
svých	svůj	k3xOyFgInPc2	svůj
dodatků	dodatek	k1gInPc2	dodatek
chápána	chápat	k5eAaImNgFnS	chápat
<g/>
,	,	kIx,	,
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
závisí	záviset	k5eAaImIp3nS	záviset
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
textu	text	k1gInSc6	text
samotném	samotný	k2eAgInSc6d1	samotný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
učiněných	učiněný	k2eAgInPc2d1	učiněný
v	v	k7c6	v
soudních	soudní	k2eAgInPc6d1	soudní
sporech	spor	k1gInPc6	spor
týkajících	týkající	k2eAgInPc6d1	týkající
se	se	k3xPyFc4	se
jejích	její	k3xOp3gNnPc2	její
ustanovení	ustanovení	k1gNnPc2	ustanovení
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
pravomoc	pravomoc	k1gFnSc4	pravomoc
přezkoumávat	přezkoumávat	k5eAaImF	přezkoumávat
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
rušit	rušit	k5eAaImF	rušit
federální	federální	k2eAgInPc4d1	federální
zákony	zákon	k1gInPc4	zákon
či	či	k8xC	či
zákony	zákon	k1gInPc4	zákon
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	on	k3xPp3gMnPc4	on
shledá	shledat	k5eAaPmIp3nS	shledat
neústavními	ústavní	k2eNgFnPc7d1	neústavní
<g/>
.	.	kIx.	.
<g/>
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejstarší	starý	k2eAgFnSc4d3	nejstarší
stále	stále	k6eAd1	stále
platnou	platný	k2eAgFnSc4d1	platná
psanou	psaný	k2eAgFnSc4d1	psaná
ústavu	ústava	k1gFnSc4	ústava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
bodem	bod	k1gInSc7	bod
amerického	americký	k2eAgNnSc2d1	americké
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
americké	americký	k2eAgFnSc2d1	americká
politické	politický	k2eAgFnSc2d1	politická
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Originál	originál	k1gMnSc1	originál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
perem	pero	k1gNnSc7	pero
napsal	napsat	k5eAaBmAgMnS	napsat
písař	písař	k1gMnSc1	písař
Jacob	Jacoba	k1gFnPc2	Jacoba
Shallus	Shallus	k1gMnSc1	Shallus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
National	National	k1gMnSc1	National
Archives	Archives	k1gMnSc1	Archives
and	and	k?	and
Records	Records	k1gInSc1	Records
Administration	Administration	k1gInSc1	Administration
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc4	období
první	první	k4xOgFnSc2	první
ústavy	ústava	k1gFnSc2	ústava
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
ústavou	ústava	k1gFnSc7	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
byly	být	k5eAaImAgFnP	být
tzv.	tzv.	kA	tzv.
Články	článek	k1gInPc1	článek
Konfederace	konfederace	k1gFnSc2	konfederace
a	a	k8xC	a
trvalé	trvalý	k2eAgFnSc2d1	trvalá
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
Články	článek	k1gInPc1	článek
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
čelily	čelit	k5eAaImAgFnP	čelit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nedostatek	nedostatek	k1gInSc4	nedostatek
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
<g/>
Tzv.	tzv.	kA	tzv.
Kontinentální	kontinentální	k2eAgInSc4d1	kontinentální
kongres	kongres	k1gInSc4	kongres
sice	sice	k8xC	sice
mohl	moct	k5eAaImAgMnS	moct
peníze	peníz	k1gInPc4	peníz
tisknout	tisknout	k5eAaImF	tisknout
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
tato	tento	k3xDgFnSc1	tento
improvizovaná	improvizovaný	k2eAgFnSc1d1	improvizovaná
měna	měna	k1gFnSc1	měna
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
a	a	k8xC	a
roztočila	roztočit	k5eAaPmAgFnS	roztočit
se	se	k3xPyFc4	se
inflace	inflace	k1gFnSc1	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
kongres	kongres	k1gInSc1	kongres
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Článků	článek	k1gInPc2	článek
Konfederace	konfederace	k1gFnSc2	konfederace
požadoval	požadovat	k5eAaImAgMnS	požadovat
platby	platba	k1gFnPc4	platba
od	od	k7c2	od
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
neplatil	platit	k5eNaImAgMnS	platit
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
výši	výše	k1gFnSc6	výše
–	–	k?	–
např.	např.	kA	např.
Georgie	Georgie	k1gFnPc4	Georgie
neplatila	platit	k5eNaImAgFnS	platit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
států	stát	k1gInPc2	stát
platilo	platit	k5eAaImAgNnS	platit
do	do	k7c2	do
federální	federální	k2eAgFnSc2d1	federální
pokladny	pokladna	k1gFnSc2	pokladna
částku	částka	k1gFnSc4	částka
rovnou	rovný	k2eAgFnSc4d1	rovná
úroku	úrok	k1gInSc3	úrok
národního	národní	k2eAgInSc2d1	národní
dluhu	dluh	k1gInSc2	dluh
vůči	vůči	k7c3	vůči
jeho	jeho	k3xOp3gMnPc3	jeho
občanům	občan	k1gMnPc3	občan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nic	nic	k3yNnSc1	nic
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nevydával	vydávat	k5eNaImAgMnS	vydávat
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
splácen	splácen	k2eAgInSc1d1	splácen
úrok	úrok	k1gInSc1	úrok
z	z	k7c2	z
dluhu	dluh	k1gInSc2	dluh
vůči	vůči	k7c3	vůči
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
vládám	vláda	k1gFnPc3	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
oddílů	oddíl	k1gInPc2	oddíl
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
čítala	čítat	k5eAaImAgFnS	čítat
625	[number]	k4	625
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nasazena	nasadit	k5eAaPmNgFnS	nasadit
proti	proti	k7c3	proti
britským	britský	k2eAgFnPc3d1	britská
pevnostem	pevnost	k1gFnPc3	pevnost
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
vojáci	voják	k1gMnPc1	voják
nebyli	být	k5eNaImAgMnP	být
placeni	platit	k5eAaImNgMnP	platit
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
někteří	některý	k3yIgMnPc1	některý
dezertovali	dezertovat	k5eAaBmAgMnP	dezertovat
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
hrozili	hrozit	k5eAaImAgMnP	hrozit
vzpourou	vzpoura	k1gFnSc7	vzpoura
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
přístav	přístav	k1gInSc4	přístav
New	New	k1gMnPc2	New
Orleans	Orleans	k1gInSc1	Orleans
americkému	americký	k2eAgInSc3d1	americký
obchodu	obchod	k1gInSc3	obchod
a	a	k8xC	a
protesty	protest	k1gInPc1	protest
amerických	americký	k2eAgMnPc2d1	americký
představitelů	představitel	k1gMnPc2	představitel
se	se	k3xPyFc4	se
míjely	míjet	k5eAaImAgFnP	míjet
účinkem	účinek	k1gInSc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Berberští	berberský	k2eAgMnPc1d1	berberský
piráti	pirát	k1gMnPc1	pirát
začali	začít	k5eAaPmAgMnP	začít
zajímat	zajímat	k5eAaImF	zajímat
americké	americký	k2eAgFnPc4d1	americká
obchodní	obchodní	k2eAgFnPc4d1	obchodní
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
Kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
kongres	kongres	k1gInSc1	kongres
nedisponoval	disponovat	k5eNaBmAgInS	disponovat
žádnými	žádný	k3yNgInPc7	žádný
finančními	finanční	k2eAgInPc7d1	finanční
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gFnPc4	on
mohl	moct	k5eAaImAgMnS	moct
nechat	nechat	k5eAaPmF	nechat
vykoupit	vykoupit	k5eAaPmF	vykoupit
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
jako	jako	k8xS	jako
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolína	Karolína	k1gFnSc1	Karolína
přestupovaly	přestupovat	k5eAaImAgFnP	přestupovat
mírovou	mírový	k2eAgFnSc4d1	mírová
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
pronásledovaly	pronásledovat	k5eAaImAgFnP	pronásledovat
loyalisty	loyalista	k1gMnPc4	loyalista
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
činnost	činnost	k1gFnSc4	činnost
během	během	k7c2	během
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
letech	let	k1gInPc6	let
1775	[number]	k4	1775
<g/>
–	–	k?	–
<g/>
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
však	však	k9	však
již	již	k6eAd1	již
neměly	mít	k5eNaImAgInP	mít
žádné	žádný	k3yNgFnPc4	žádný
rezervy	rezerva	k1gFnPc4	rezerva
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
situace	situace	k1gFnSc1	situace
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
další	další	k2eAgFnSc4d1	další
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
propuklo	propuknout	k5eAaPmAgNnS	propuknout
tzv.	tzv.	kA	tzv.
Shaysovo	Shaysův	k2eAgNnSc1d1	Shaysův
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Kongres	kongres	k1gInSc1	kongres
neměl	mít	k5eNaImAgInS	mít
žádné	žádný	k3yNgInPc4	žádný
peníze	peníz	k1gInPc4	peníz
<g/>
;	;	kIx,	;
generál	generál	k1gMnSc1	generál
Benjamin	Benjamin	k1gMnSc1	Benjamin
Lincoln	Lincoln	k1gMnSc1	Lincoln
musel	muset	k5eAaImAgMnS	muset
uspořádat	uspořádat	k5eAaPmF	uspořádat
sbírku	sbírka	k1gFnSc4	sbírka
mezi	mezi	k7c7	mezi
bostonskými	bostonský	k2eAgMnPc7d1	bostonský
obchodníky	obchodník	k1gMnPc7	obchodník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohl	moct	k5eAaImAgMnS	moct
zaplatit	zaplatit	k5eAaPmF	zaplatit
dobrovolnické	dobrovolnický	k2eAgFnSc3d1	dobrovolnická
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
<g/>
Kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
kongres	kongres	k1gInSc1	kongres
byl	být	k5eAaImAgInS	být
paralyzován	paralyzovat	k5eAaBmNgInS	paralyzovat
<g/>
.	.	kIx.	.
</s>
<s>
Nemohl	moct	k5eNaImAgMnS	moct
dělat	dělat	k5eAaImF	dělat
nic	nic	k3yNnSc4	nic
významného	významný	k2eAgMnSc2d1	významný
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
sešly	sejít	k5eAaPmAgFnP	sejít
delegace	delegace	k1gFnPc1	delegace
devíti	devět	k4xCc2	devět
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
některé	některý	k3yIgFnPc1	některý
legislativní	legislativní	k2eAgFnPc1d1	legislativní
záležitosti	záležitost	k1gFnPc1	záležitost
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
všech	všecek	k3xTgInPc2	všecek
třináct	třináct	k4xCc1	třináct
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k6eAd1	už
se	se	k3xPyFc4	se
sešly	sejít	k5eAaPmAgFnP	sejít
delegace	delegace	k1gFnPc1	delegace
devíti	devět	k4xCc2	devět
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
nemohly	moct	k5eNaImAgFnP	moct
se	se	k3xPyFc4	se
počítat	počítat	k5eAaImF	počítat
hlasy	hlas	k1gInPc4	hlas
těch	ten	k3xDgMnPc2	ten
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
jen	jen	k9	jen
jedním	jeden	k4xCgMnSc7	jeden
delegátem	delegát	k1gMnSc7	delegát
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
delegací	delegace	k1gFnPc2	delegace
neshodla	shodnout	k5eNaPmAgFnS	shodnout
a	a	k8xC	a
názory	názor	k1gInPc1	názor
uvnitř	uvnitř	k7c2	uvnitř
této	tento	k3xDgFnSc2	tento
delegace	delegace	k1gFnSc2	delegace
se	se	k3xPyFc4	se
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozdělily	rozdělit	k5eAaPmAgInP	rozdělit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
náležitě	náležitě	k6eAd1	náležitě
zaprotokolována	zaprotokolován	k2eAgFnSc1d1	zaprotokolována
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
její	její	k3xOp3gInSc4	její
hlas	hlas	k1gInSc4	hlas
se	se	k3xPyFc4	se
nepočítal	počítat	k5eNaImAgMnS	počítat
do	do	k7c2	do
kvóra	kvórum	k1gNnSc2	kvórum
devíti	devět	k4xCc2	devět
nutných	nutný	k2eAgInPc2d1	nutný
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
přestupovaly	přestupovat	k5eAaImAgInP	přestupovat
ustanovení	ustanovení	k1gNnSc4	ustanovení
Článků	článek	k1gInPc2	článek
Konfederace	konfederace	k1gFnSc2	konfederace
a	a	k8xC	a
uvalovaly	uvalovat	k5eAaImAgFnP	uvalovat
embarga	embargo	k1gNnSc2	embargo
<g/>
,	,	kIx,	,
vyjednávaly	vyjednávat	k5eAaImAgFnP	vyjednávat
jednostranně	jednostranně	k6eAd1	jednostranně
s	s	k7c7	s
cizími	cizí	k2eAgFnPc7d1	cizí
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
vydržovaly	vydržovat	k5eAaImAgFnP	vydržovat
si	se	k3xPyFc3	se
armády	armáda	k1gFnPc4	armáda
a	a	k8xC	a
vedly	vést	k5eAaImAgFnP	vést
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Články	článek	k1gInPc1	článek
Konfederace	konfederace	k1gFnSc2	konfederace
nakonec	nakonec	k6eAd1	nakonec
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kongres	kongres	k1gInSc1	kongres
se	se	k3xPyFc4	se
de	de	k?	de
facto	facto	k1gNnSc1	facto
přestal	přestat	k5eAaPmAgInS	přestat
pokoušet	pokoušet	k5eAaImF	pokoušet
vládnout	vládnout	k5eAaImF	vládnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svolání	svolání	k1gNnSc6	svolání
Ústavního	ústavní	k2eAgInSc2d1	ústavní
konventu	konvent	k1gInSc2	konvent
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1786	[number]	k4	1786
se	se	k3xPyFc4	se
v	v	k7c6	v
marylandském	marylandský	k2eAgInSc6d1	marylandský
přístavu	přístav	k1gInSc6	přístav
Annapolis	Annapolis	k1gFnSc2	Annapolis
setkali	setkat	k5eAaPmAgMnP	setkat
zastupitelé	zastupitel	k1gMnPc1	zastupitel
pěti	pět	k4xCc2	pět
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
prodiskutovali	prodiskutovat	k5eAaPmAgMnP	prodiskutovat
dodatky	dodatek	k1gInPc4	dodatek
k	k	k7c3	k
Článkům	článek	k1gInPc3	článek
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
zlepšily	zlepšit	k5eAaPmAgFnP	zlepšit
možnosti	možnost	k1gFnPc1	možnost
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
pozvali	pozvat	k5eAaPmAgMnP	pozvat
delegáty	delegát	k1gMnPc4	delegát
všech	všecek	k3xTgMnPc2	všecek
států	stát	k1gInPc2	stát
do	do	k7c2	do
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tam	tam	k6eAd1	tam
projednali	projednat	k5eAaPmAgMnP	projednat
opatření	opatření	k1gNnSc4	opatření
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
ústavní	ústavní	k2eAgFnSc2d1	ústavní
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
debaty	debata	k1gFnSc2	debata
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
kongres	kongres	k1gInSc1	kongres
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1787	[number]	k4	1787
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
plán	plán	k1gInSc1	plán
na	na	k7c4	na
revizi	revize	k1gFnSc4	revize
Článků	článek	k1gInPc2	článek
Konfederace	konfederace	k1gFnSc2	konfederace
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
všechny	všechen	k3xTgInPc4	všechen
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyslaly	vyslat	k5eAaPmAgInP	vyslat
své	svůj	k3xOyFgInPc4	svůj
zástupce	zástupce	k1gMnSc1	zástupce
na	na	k7c4	na
konvent	konvent	k1gInSc4	konvent
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
tuto	tento	k3xDgFnSc4	tento
revizi	revize	k1gFnSc4	revize
provést	provést	k5eAaPmF	provést
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Účast	účast	k1gFnSc4	účast
přijalo	přijmout	k5eAaPmAgNnS	přijmout
dvanáct	dvanáct	k4xCc4	dvanáct
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
Rhode	Rhodos	k1gInSc5	Rhodos
Islandu	Island	k1gInSc6	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyslaly	vyslat	k5eAaPmAgInP	vyslat
své	svůj	k3xOyFgInPc4	svůj
zástupce	zástupce	k1gMnSc1	zástupce
na	na	k7c4	na
konvent	konvent	k1gInSc4	konvent
konaný	konaný	k2eAgInSc4d1	konaný
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1787	[number]	k4	1787
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
výzva	výzva	k1gFnSc1	výzva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
byl	být	k5eAaImAgInS	být
Filadelfský	filadelfský	k2eAgInSc1d1	filadelfský
ústavní	ústavní	k2eAgInSc1d1	ústavní
konvent	konvent	k1gInSc1	konvent
svolán	svolat	k5eAaPmNgInS	svolat
<g/>
,	,	kIx,	,
uváděla	uvádět	k5eAaImAgFnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc7	jeho
účelem	účel	k1gInSc7	účel
bude	být	k5eAaImBp3nS	být
navrhnout	navrhnout	k5eAaPmF	navrhnout
dodatky	dodatek	k1gInPc4	dodatek
k	k	k7c3	k
Článkům	článek	k1gInPc3	článek
Konfederace	konfederace	k1gFnSc2	konfederace
a	a	k8xC	a
tyto	tento	k3xDgInPc4	tento
prodebatovat	prodebatovat	k5eAaPmF	prodebatovat
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
června	červen	k1gInSc2	červen
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
místo	místo	k7c2	místo
nich	on	k3xPp3gFnPc2	on
konvent	konvent	k1gInSc1	konvent
nejspíše	nejspíše	k9	nejspíše
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
úplně	úplně	k6eAd1	úplně
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Konvent	konvent	k1gInSc4	konvent
si	se	k3xPyFc3	se
také	také	k9	také
odhlasoval	odhlasovat	k5eAaPmAgInS	odhlasovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
debaty	debata	k1gFnPc1	debata
budou	být	k5eAaImBp3nP	být
tajné	tajný	k2eAgFnPc1d1	tajná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zastupitelé	zastupitel	k1gMnPc1	zastupitel
mohli	moct	k5eAaImAgMnP	moct
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
svobodně	svobodně	k6eAd1	svobodně
bavit	bavit	k5eAaImF	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
znalosti	znalost	k1gFnPc1	znalost
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
navrhování	navrhování	k1gNnSc2	navrhování
a	a	k8xC	a
konstrukce	konstrukce	k1gFnSc2	konstrukce
Ústavy	ústava	k1gFnSc2	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgMnPc2d1	americký
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
většinou	většinou	k6eAd1	většinou
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
denících	deník	k1gInPc6	deník
amerického	americký	k2eAgNnSc2d1	americké
politika	politikum	k1gNnSc2	politikum
Jamese	Jamese	k1gFnSc2	Jamese
Madisona	Madison	k1gMnSc2	Madison
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
Filadelfského	filadelfský	k2eAgInSc2d1	filadelfský
ústavního	ústavní	k2eAgInSc2d1	ústavní
konventu	konvent	k1gInSc2	konvent
vedl	vést	k5eAaImAgMnS	vést
úplné	úplný	k2eAgInPc4d1	úplný
záznamy	záznam	k1gInPc4	záznam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Utváření	utváření	k1gNnSc1	utváření
ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
dopady	dopad	k1gInPc4	dopad
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Filadelfském	filadelfský	k2eAgInSc6d1	filadelfský
ústavním	ústavní	k2eAgInSc6d1	ústavní
konventu	konvent	k1gInSc6	konvent
spolu	spolu	k6eAd1	spolu
zpočátku	zpočátku	k6eAd1	zpočátku
soupeřily	soupeřit	k5eAaImAgFnP	soupeřit
dvě	dva	k4xCgFnPc1	dva
poměrně	poměrně	k6eAd1	poměrně
odlišné	odlišný	k2eAgFnPc4d1	odlišná
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
budoucím	budoucí	k2eAgNnSc6d1	budoucí
uspořádání	uspořádání	k1gNnSc6	uspořádání
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tzv.	tzv.	kA	tzv.
Virginský	virginský	k2eAgInSc4d1	virginský
plán	plán	k1gInSc4	plán
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
jako	jako	k9	jako
neoficiální	neoficiální	k2eAgFnSc4d1	neoficiální
agendu	agenda	k1gFnSc4	agenda
konventu	konvent	k1gInSc2	konvent
zejména	zejména	k9	zejména
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
významný	významný	k2eAgInSc4d1	významný
přínos	přínos	k1gInSc4	přínos
nazýván	nazýván	k2eAgInSc4d1	nazýván
"	"	kIx"	"
<g/>
Otcem	otec	k1gMnSc7	otec
ústavy	ústav	k1gInPc1	ústav
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
upřednostňoval	upřednostňovat	k5eAaImAgInS	upřednostňovat
zájmy	zájem	k1gInPc4	zájem
větších	veliký	k2eAgInPc2d2	veliký
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
následující	následující	k2eAgNnSc4d1	následující
ustanovení	ustanovení	k1gNnSc4	ustanovení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc1d1	tvořený
Sněmovnou	sněmovna	k1gFnSc7	sněmovna
s	s	k7c7	s
volenými	volený	k2eAgMnPc7d1	volený
zástupci	zástupce	k1gMnPc7	zástupce
a	a	k8xC	a
Senátem	senát	k1gInSc7	senát
<g/>
,	,	kIx,	,
sestávajícího	sestávající	k2eAgMnSc4d1	sestávající
ze	z	k7c2	z
starších	starý	k2eAgMnPc2d2	starší
vůdců	vůdce	k1gMnPc2	vůdce
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
volenými	volený	k2eAgFnPc7d1	volená
zákonodárci	zákonodárce	k1gMnPc1	zákonodárce
<g/>
;	;	kIx,	;
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
komorách	komora	k1gFnPc6	komora
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
úměrně	úměrně	k7c3	úměrně
počtu	počet	k1gInSc3	počet
svých	svůj	k3xOyFgMnPc2	svůj
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Orgán	orgán	k1gInSc1	orgán
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
představitelé	představitel	k1gMnPc1	představitel
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
jmenováni	jmenován	k2eAgMnPc1d1	jmenován
zákonodárci	zákonodárce	k1gMnPc1	zákonodárce
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Soudci	soudce	k1gMnPc1	soudce
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
garantována	garantovat	k5eAaBmNgFnS	garantovat
doživotní	doživotní	k2eAgFnSc1d1	doživotní
služba	služba	k1gFnSc1	služba
a	a	k8xC	a
blíže	blízce	k6eAd2	blízce
neurčené	určený	k2eNgFnPc4d1	neurčená
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Celonárodní	celonárodní	k2eAgInSc1d1	celonárodní
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
orgán	orgán	k1gInSc1	orgán
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vetovat	vetovat	k5eAaBmF	vetovat
zákony	zákon	k1gInPc1	zákon
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
<g/>
Alternativní	alternativní	k2eAgInSc1d1	alternativní
návrh	návrh	k1gInSc1	návrh
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
Newjerseyský	Newjerseyský	k2eAgInSc1d1	Newjerseyský
plán	plán	k1gInSc1	plán
Williama	William	k1gMnSc2	William
Pattersona	Patterson	k1gMnSc2	Patterson
<g/>
,	,	kIx,	,
vycházel	vycházet	k5eAaImAgMnS	vycházet
vstříc	vstříc	k6eAd1	vstříc
menším	malý	k2eAgInPc3d2	menší
státům	stát	k1gInPc3	stát
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
zejména	zejména	k9	zejména
následující	následující	k2eAgNnSc4d1	následující
ustanovení	ustanovení	k1gNnSc4	ustanovení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
národní	národní	k2eAgInSc1d1	národní
orgán	orgán	k1gInSc1	orgán
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
by	by	kYmCp3nS	by
každý	každý	k3xTgInSc1	každý
stát	stát	k1gInSc1	stát
vysílal	vysílat	k5eAaImAgInS	vysílat
rovný	rovný	k2eAgInSc4d1	rovný
počet	počet	k1gInSc4	počet
zastupitelů	zastupitel	k1gMnPc2	zastupitel
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Orgán	orgán	k1gInSc1	orgán
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
představitelé	představitel	k1gMnPc1	představitel
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
jmenováni	jmenován	k2eAgMnPc1d1	jmenován
zákonodárci	zákonodárce	k1gMnPc1	zákonodárce
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Soudci	soudce	k1gMnPc1	soudce
jmenovaní	jmenovaný	k2eAgMnPc1d1	jmenovaný
představiteli	představitel	k1gMnPc7	představitel
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
také	také	k9	také
počítal	počítat	k5eAaImAgInS	počítat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
nezbytných	nezbytný	k2eAgInPc2d1	nezbytný
dodatků	dodatek	k1gInPc2	dodatek
ke	k	k7c3	k
Článkům	článek	k1gInPc3	článek
Konfederace	konfederace	k1gFnSc2	konfederace
místo	místo	k1gNnSc4	místo
sepsání	sepsání	k1gNnSc2	sepsání
zcela	zcela	k6eAd1	zcela
nového	nový	k2eAgInSc2d1	nový
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
<g/>
Průlom	průlom	k1gInSc1	průlom
v	v	k7c6	v
jednáních	jednání	k1gNnPc6	jednání
nastal	nastat	k5eAaPmAgMnS	nastat
díky	díky	k7c3	díky
Rogeru	Roger	k1gMnSc3	Roger
Shermanovi	Sherman	k1gMnSc3	Sherman
<g/>
,	,	kIx,	,
politikovi	politik	k1gMnSc3	politik
z	z	k7c2	z
Connecticutu	Connecticut	k1gInSc2	Connecticut
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
tzv.	tzv.	kA	tzv.
Connecticutský	Connecticutský	k2eAgInSc4d1	Connecticutský
kompromis	kompromis	k1gInSc4	kompromis
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgMnSc4	jenž
Sněmovna	sněmovna	k1gFnSc1	sněmovna
zastupovala	zastupovat	k5eAaImAgFnS	zastupovat
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
a	a	k8xC	a
Senát	senát	k1gInSc4	senát
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Dělení	dělení	k1gNnSc1	dělení
moci	moc	k1gFnSc2	moc
====	====	k?	====
</s>
</p>
<p>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
konvent	konvent	k1gInSc1	konvent
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
přijetím	přijetí	k1gNnSc7	přijetí
Ústavy	ústava	k1gFnSc2	ústava
dosud	dosud	k6eAd1	dosud
nevyzkoušenou	vyzkoušený	k2eNgFnSc4d1	nevyzkoušená
formu	forma	k1gFnSc4	forma
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc4	veškerý
předchozí	předchozí	k2eAgInPc4d1	předchozí
vládní	vládní	k2eAgInPc4d1	vládní
systémy	systém	k1gInPc4	systém
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
buď	buď	k8xC	buď
centralizovanou	centralizovaný	k2eAgFnSc4d1	centralizovaná
vládu	vláda	k1gFnSc4	vláda
anebo	anebo	k8xC	anebo
konfederaci	konfederace	k1gFnSc4	konfederace
suverénních	suverénní	k2eAgInPc2d1	suverénní
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
systém	systém	k1gInSc1	systém
sdílení	sdílení	k1gNnSc2	sdílení
moci	moc	k1gFnSc2	moc
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
unikátní	unikátní	k2eAgMnSc1d1	unikátní
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
zdrojem	zdroj	k1gInSc7	zdroj
byly	být	k5eAaImAgInP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
a	a	k8xC	a
veškeré	veškerý	k3xTgFnPc1	veškerý
změny	změna	k1gFnPc1	změna
systému	systém	k1gInSc2	systém
záležely	záležet	k5eAaImAgFnP	záležet
také	také	k9	také
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
měla	mít	k5eAaImAgFnS	mít
celonárodní	celonárodní	k2eAgInSc4d1	celonárodní
dosah	dosah	k1gInSc4	dosah
<g/>
.	.	kIx.	.
<g/>
Aby	aby	k9	aby
tvůrci	tvůrce	k1gMnPc1	tvůrce
Ústavy	ústava	k1gFnSc2	ústava
naplnili	naplnit	k5eAaPmAgMnP	naplnit
své	svůj	k3xOyFgInPc4	svůj
cíle	cíl	k1gInPc4	cíl
ohledně	ohledně	k7c2	ohledně
stmelení	stmelení	k1gNnSc2	stmelení
Unie	unie	k1gFnSc2	unie
a	a	k8xC	a
současně	současně	k6eAd1	současně
zajištění	zajištění	k1gNnSc4	zajištění
práv	právo	k1gNnPc2	právo
jejích	její	k3xOp3gMnPc2	její
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
centrální	centrální	k2eAgFnSc4d1	centrální
vládu	vláda	k1gFnSc4	vláda
na	na	k7c4	na
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
složku	složka	k1gFnSc4	složka
<g/>
,	,	kIx,	,
Senát	senát	k1gInSc4	senát
<g/>
,	,	kIx,	,
Sněmovnu	sněmovna	k1gFnSc4	sněmovna
a	a	k8xC	a
soudní	soudní	k2eAgInSc4d1	soudní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
doplněn	doplnit	k5eAaPmNgInS	doplnit
systémem	systém	k1gInSc7	systém
různých	různý	k2eAgFnPc2d1	různá
brzd	brzda	k1gFnPc2	brzda
a	a	k8xC	a
vyvážení	vyvážení	k1gNnSc4	vyvážení
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
označovaných	označovaný	k2eAgInPc2d1	označovaný
souslovím	sousloví	k1gNnSc7	sousloví
check	check	k1gInSc1	check
and	and	k?	and
balances	balances	k1gInSc1	balances
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
složky	složka	k1gFnPc1	složka
moci	moc	k1gFnSc2	moc
vzájemně	vzájemně	k6eAd1	vzájemně
kontrolovaly	kontrolovat	k5eAaImAgInP	kontrolovat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
členy	člen	k1gMnPc7	člen
kabinetu	kabinet	k1gInSc2	kabinet
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
jako	jako	k8xS	jako
představitel	představitel	k1gMnSc1	představitel
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
souhlas	souhlas	k1gInSc1	souhlas
nejméně	málo	k6eAd3	málo
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
prezident	prezident	k1gMnSc1	prezident
může	moct	k5eAaImIp3nS	moct
vetovat	vetovat	k5eAaBmF	vetovat
zákon	zákon	k1gInSc1	zákon
přijatý	přijatý	k2eAgInSc1d1	přijatý
Kongresem	kongres	k1gInSc7	kongres
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zase	zase	k9	zase
může	moct	k5eAaImIp3nS	moct
jeho	jeho	k3xOp3gNnSc4	jeho
veto	veto	k1gNnSc4	veto
dvoutřetinovou	dvoutřetinový	k2eAgFnSc7d1	dvoutřetinová
většinou	většina	k1gFnSc7	většina
přehlasovat	přehlasovat	k5eAaBmF	přehlasovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
vlád	vláda	k1gFnPc2	vláda
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
nadále	nadále	k6eAd1	nadále
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
moc	moc	k6eAd1	moc
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
sféře	sféra	k1gFnSc6	sféra
a	a	k8xC	a
nad	nad	k7c7	nad
svým	svůj	k3xOyFgNnSc7	svůj
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Konvent	konvent	k1gInSc1	konvent
nevytvářel	vytvářet	k5eNaImAgInS	vytvářet
pravomoci	pravomoc	k1gFnSc3	pravomoc
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
z	z	k7c2	z
ničeho	nic	k3yNnSc2	nic
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vyšel	vyjít	k5eAaPmAgInS	vyjít
z	z	k7c2	z
pravomocí	pravomoc	k1gFnPc2	pravomoc
Kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
přisuzovaly	přisuzovat	k5eAaImAgInP	přisuzovat
Články	článek	k1gInPc1	článek
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
vojenského	vojenský	k2eAgNnSc2d1	vojenské
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
přidala	přidat	k5eAaPmAgFnS	přidat
deset	deset	k4xCc4	deset
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
poměrně	poměrně	k6eAd1	poměrně
nevýznamná	významný	k2eNgFnSc1d1	nevýznamná
a	a	k8xC	a
týkala	týkat	k5eAaImAgFnS	týkat
se	se	k3xPyFc4	se
např.	např.	kA	např.
obchodních	obchodní	k2eAgNnPc2d1	obchodní
a	a	k8xC	a
výrobních	výrobní	k2eAgNnPc2d1	výrobní
ochranářských	ochranářský	k2eAgNnPc2d1	ochranářské
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
významná	významný	k2eAgFnSc1d1	významná
pravomoc	pravomoc	k1gFnSc1	pravomoc
však	však	k9	však
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
Kongresu	kongres	k1gInSc6	kongres
chránit	chránit	k5eAaImF	chránit
státy	stát	k1gInPc4	stát
před	před	k7c7	před
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
agresí	agrese	k1gFnSc7	agrese
a	a	k8xC	a
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
proti	proti	k7c3	proti
pouličním	pouliční	k2eAgFnPc3d1	pouliční
bouřím	bouř	k1gFnPc3	bouř
a	a	k8xC	a
veřejným	veřejný	k2eAgInPc3d1	veřejný
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
žádostí	žádost	k1gFnSc7	žádost
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
významně	významně	k6eAd1	významně
byla	být	k5eAaImAgFnS	být
moc	moc	k1gFnSc1	moc
Kongresu	kongres	k1gInSc2	kongres
posílena	posílit	k5eAaPmNgFnS	posílit
pravomocí	pravomoc	k1gFnSc7	pravomoc
vypisovat	vypisovat	k5eAaImF	vypisovat
daně	daň	k1gFnPc4	daň
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
byla	být	k5eAaImAgNnP	být
stanovena	stanoven	k2eAgNnPc1d1	stanoveno
určitá	určitý	k2eAgNnPc1d1	určité
omezení	omezení	k1gNnPc1	omezení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
uvalovat	uvalovat	k5eAaImF	uvalovat
daně	daň	k1gFnPc4	daň
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
<g/>
,	,	kIx,	,
daně	daň	k1gFnPc1	daň
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
či	či	k8xC	či
vyžadování	vyžadování	k1gNnSc3	vyžadování
některých	některý	k3yIgFnPc2	některý
dovozních	dovozní	k2eAgFnPc2d1	dovozní
povinností	povinnost	k1gFnPc2	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgInP	být
zbaveny	zbavit	k5eAaPmNgInP	zbavit
možnosti	možnost	k1gFnPc4	možnost
uvalovat	uvalovat	k5eAaImF	uvalovat
daně	daň	k1gFnPc4	daň
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejvýnosnější	výnosný	k2eAgInPc4d3	nejvýnosnější
příjmy	příjem	k1gInPc4	příjem
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pro	pro	k7c4	pro
Kongres	kongres	k1gInSc4	kongres
nebyla	být	k5eNaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
žádná	žádný	k3yNgNnPc1	žádný
další	další	k2eAgNnPc1d1	další
omezení	omezení	k1gNnPc1	omezení
týkající	týkající	k2eAgNnPc1d1	týkající
se	se	k3xPyFc4	se
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
například	například	k6eAd1	například
zavádět	zavádět	k5eAaImF	zavádět
ochranná	ochranný	k2eAgNnPc4d1	ochranné
cla	clo	k1gNnPc4	clo
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
potlačil	potlačit	k5eAaPmAgInS	potlačit
moc	moc	k1gFnSc4	moc
států	stát	k1gInPc2	stát
stanovením	stanovení	k1gNnSc7	stanovení
pravidel	pravidlo	k1gNnPc2	pravidlo
jejich	jejich	k3xOp3gInSc2	jejich
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
největší	veliký	k2eAgFnSc7d3	veliký
oblastí	oblast	k1gFnSc7	oblast
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
Ústava	ústava	k1gFnSc1	ústava
znatelně	znatelně	k6eAd1	znatelně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
pravomoci	pravomoc	k1gFnPc4	pravomoc
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
organizace	organizace	k1gFnSc1	organizace
a	a	k8xC	a
vyzbrojování	vyzbrojování	k1gNnSc1	vyzbrojování
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
využití	využití	k1gNnSc4	využití
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
svých	svůj	k3xOyFgInPc2	svůj
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
potlačení	potlačení	k1gNnSc2	potlačení
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
odražení	odražení	k1gNnSc2	odražení
případné	případný	k2eAgFnSc2d1	případná
invaze	invaze	k1gFnSc2	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
dodatek	dodatek	k1gInSc1	dodatek
Ústavy	ústava	k1gFnSc2	ústava
však	však	k9	však
vzápětí	vzápětí	k6eAd1	vzápětí
zajistil	zajistit	k5eAaPmAgMnS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kongres	kongres	k1gInSc1	kongres
nesmí	smět	k5eNaImIp3nS	smět
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
odzbrojení	odzbrojení	k1gNnSc3	odzbrojení
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
složek	složka	k1gFnPc2	složka
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejvágněji	vágně	k6eAd3	vágně
definovanou	definovaný	k2eAgFnSc7d1	definovaná
pravomocí	pravomoc	k1gFnSc7	pravomoc
Kongresu	kongres	k1gInSc2	kongres
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
pověření	pověření	k1gNnSc4	pověření
vydávat	vydávat	k5eAaImF	vydávat
zákony	zákon	k1gInPc1	zákon
nutné	nutný	k2eAgInPc1d1	nutný
k	k	k7c3	k
naplňování	naplňování	k1gNnSc3	naplňování
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
tato	tento	k3xDgFnSc1	tento
Ústava	ústava	k1gFnSc1	ústava
propůjčila	propůjčit	k5eAaPmAgFnS	propůjčit
vládě	vláda	k1gFnSc3	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgNnSc2	tento
rozšíření	rozšíření	k1gNnSc2	rozšíření
moci	moct	k5eAaImF	moct
Kongresu	kongres	k1gInSc3	kongres
Ústava	ústava	k1gFnSc1	ústava
také	také	k9	také
federální	federální	k2eAgFnSc1d1	federální
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
moc	moc	k6eAd1	moc
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
ohledech	ohled	k1gInPc6	ohled
omezovala	omezovat	k5eAaImAgNnP	omezovat
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
omezení	omezení	k1gNnPc1	omezení
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
majetkových	majetkový	k2eAgNnPc2d1	majetkové
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
daní	daň	k1gFnPc2	daň
či	či	k8xC	či
vyvlastňování	vyvlastňování	k1gNnSc1	vyvlastňování
<g/>
,	,	kIx,	,
jiná	jiná	k1gFnSc1	jiná
měla	mít	k5eAaImAgFnS	mít
chránit	chránit	k5eAaImF	chránit
některé	některý	k3yIgFnPc4	některý
svobody	svoboda	k1gFnPc4	svoboda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
zákaz	zákaz	k1gInSc1	zákaz
právní	právní	k2eAgFnSc2d1	právní
retroaktivity	retroaktivita	k1gFnSc2	retroaktivita
nebo	nebo	k8xC	nebo
podmiňování	podmiňování	k1gNnSc2	podmiňování
výkonu	výkon	k1gInSc2	výkon
veřejné	veřejný	k2eAgFnSc2d1	veřejná
funkce	funkce	k1gFnSc2	funkce
náboženským	náboženský	k2eAgNnSc7d1	náboženské
vyznáním	vyznání	k1gNnSc7	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
státy	stát	k1gInPc4	stát
zase	zase	k9	zase
platil	platit	k5eAaImAgInS	platit
kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgInSc2d1	uvedený
zákazu	zákaz	k1gInSc2	zákaz
uvalování	uvalování	k1gNnSc2	uvalování
daní	daň	k1gFnPc2	daň
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
také	také	k9	také
např.	např.	kA	např.
zákaz	zákaz	k1gInSc1	zákaz
uzavírat	uzavírat	k5eAaImF	uzavírat
smlouvy	smlouva	k1gFnPc4	smlouva
a	a	k8xC	a
spojenectví	spojenectví	k1gNnSc4	spojenectví
se	s	k7c7	s
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
mocnostmi	mocnost	k1gFnPc7	mocnost
i	i	k9	i
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
omezení	omezení	k1gNnPc1	omezení
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
představovala	představovat	k5eAaImAgFnS	představovat
pro	pro	k7c4	pro
vládu	vláda	k1gFnSc4	vláda
nový	nový	k2eAgInSc4d1	nový
a	a	k8xC	a
kvalitativně	kvalitativně	k6eAd1	kvalitativně
odlišný	odlišný	k2eAgInSc1d1	odlišný
typ	typ	k1gInSc1	typ
závazku	závazek	k1gInSc2	závazek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dosavadních	dosavadní	k2eAgFnPc6d1	dosavadní
ústavách	ústava	k1gFnPc6	ústava
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
lid	lid	k1gInSc1	lid
nepředával	předávat	k5eNaImAgInS	předávat
vládě	vláda	k1gFnSc3	vláda
přesně	přesně	k6eAd1	přesně
vyjmenované	vyjmenovaný	k2eAgFnPc4d1	vyjmenovaná
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k6eAd1	místo
toho	ten	k3xDgMnSc4	ten
zvolení	zvolený	k2eAgMnPc1d1	zvolený
zástupci	zástupce	k1gMnPc1	zástupce
lidu	lid	k1gInSc2	lid
měli	mít	k5eAaImAgMnP	mít
všechna	všechen	k3xTgNnPc4	všechen
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
výslovně	výslovně	k6eAd1	výslovně
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
samotným	samotný	k2eAgInSc7d1	samotný
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
některých	některý	k3yIgInPc2	některý
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
případů	případ	k1gInPc2	případ
zneužití	zneužití	k1gNnSc2	zneužití
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
zahrnuli	zahrnout	k5eAaPmAgMnP	zahrnout
tvůrci	tvůrce	k1gMnPc1	tvůrce
Ústavy	ústava	k1gFnSc2	ústava
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
dokumentu	dokument	k1gInSc2	dokument
řadu	řada	k1gFnSc4	řada
ustanovení	ustanovení	k1gNnSc2	ustanovení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tomu	ten	k3xDgNnSc3	ten
měly	mít	k5eAaImAgFnP	mít
bránit	bránit	k5eAaImF	bránit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
chránit	chránit	k5eAaImF	chránit
majetková	majetkový	k2eAgNnPc4d1	majetkové
práva	právo	k1gNnPc4	právo
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
mj.	mj.	kA	mj.
výše	výše	k1gFnSc1	výše
uvedené	uvedený	k2eAgNnSc4d1	uvedené
ustanovení	ustanovení	k1gNnSc4	ustanovení
zakazující	zakazující	k2eAgNnSc1d1	zakazující
přijímání	přijímání	k1gNnSc1	přijímání
zákonů	zákon	k1gInPc2	zákon
se	s	k7c7	s
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
obsahem	obsah	k1gInSc7	obsah
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
soudní	soudní	k2eAgInSc4d1	soudní
rozsudek	rozsudek	k1gInSc4	rozsudek
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
předešli	předejít	k5eAaPmAgMnP	předejít
zneužití	zneužití	k1gNnSc4	zneužití
moci	moc	k1gFnSc2	moc
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
hledali	hledat	k5eAaImAgMnP	hledat
autoři	autor	k1gMnPc1	autor
Ústavy	ústava	k1gFnSc2	ústava
také	také	k6eAd1	také
nějaký	nějaký	k3yIgInSc4	nějaký
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
revidovat	revidovat	k5eAaImF	revidovat
či	či	k8xC	či
vetovat	vetovat	k5eAaBmF	vetovat
jejich	jejich	k3xOp3gInPc4	jejich
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
škodu	škoda	k1gFnSc4	škoda
celonárodním	celonárodní	k2eAgInPc3d1	celonárodní
zájmům	zájem	k1gInPc3	zájem
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
občanskými	občanský	k2eAgNnPc7d1	občanské
právy	právo	k1gNnPc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
nakonec	nakonec	k6eAd1	nakonec
zamítnuty	zamítnut	k2eAgFnPc1d1	zamítnuta
a	a	k8xC	a
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústava	ústava	k1gFnSc1	ústava
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
zákonem	zákon	k1gInSc7	zákon
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
právo	právo	k1gNnSc4	právo
přezkumu	přezkum	k1gInSc2	přezkum
zákonů	zákon	k1gInPc2	zákon
států	stát	k1gInPc2	stát
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
zabíraly	zabírat	k5eAaImAgInP	zabírat
již	již	k9	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
tak	tak	k6eAd1	tak
velké	velký	k2eAgNnSc1d1	velké
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednolitá	jednolitý	k2eAgFnSc1d1	jednolitá
vláda	vláda	k1gFnSc1	vláda
nebyla	být	k5eNaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
<g/>
.	.	kIx.	.
</s>
<s>
Nejrůznější	různý	k2eAgFnPc1d3	nejrůznější
pravomoci	pravomoc	k1gFnPc1	pravomoc
byly	být	k5eAaImAgFnP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
nejen	nejen	k6eAd1	nejen
mezi	mezi	k7c4	mezi
různé	různý	k2eAgFnPc4d1	různá
složky	složka	k1gFnPc4	složka
federální	federální	k2eAgFnSc2d1	federální
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
třináct	třináct	k4xCc1	třináct
republikánských	republikánský	k2eAgFnPc2d1	republikánská
státních	státní	k2eAgFnPc2d1	státní
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
původního	původní	k2eAgInSc2d1	původní
záměru	záměr	k1gInSc2	záměr
doplnění	doplnění	k1gNnSc4	doplnění
původních	původní	k2eAgInPc2d1	původní
Článků	článek	k1gInPc2	článek
Konfederace	konfederace	k1gFnSc2	konfederace
nakonec	nakonec	k9	nakonec
delegáti	delegát	k1gMnPc1	delegát
konventu	konvent	k1gInSc2	konvent
vypracovali	vypracovat	k5eAaPmAgMnP	vypracovat
dokument	dokument	k1gInSc4	dokument
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
zásadních	zásadní	k2eAgFnPc6d1	zásadní
otázkách	otázka	k1gFnPc6	otázka
zcela	zcela	k6eAd1	zcela
lišil	lišit	k5eAaImAgMnS	lišit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
následující	následující	k2eAgFnSc2d1	následující
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Otrokářství	otrokářství	k1gNnSc2	otrokářství
====	====	k?	====
</s>
</p>
<p>
<s>
Delegáti	delegát	k1gMnPc1	delegát
Filadelfského	filadelfský	k2eAgInSc2d1	filadelfský
konventu	konvent	k1gInSc2	konvent
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
řešit	řešit	k5eAaImF	řešit
také	také	k9	také
otázku	otázka	k1gFnSc4	otázka
otrokářství	otrokářství	k1gNnSc2	otrokářství
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uspěli	uspět	k5eAaPmAgMnP	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
Články	článek	k1gInPc1	článek
Konfederace	konfederace	k1gFnSc2	konfederace
neumožňovaly	umožňovat	k5eNaImAgInP	umožňovat
otrokářský	otrokářský	k2eAgInSc4d1	otrokářský
systém	systém	k1gInSc4	systém
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Konvent	konvent	k1gInSc1	konvent
hodlal	hodlat	k5eAaImAgInS	hodlat
umožnit	umožnit	k5eAaPmF	umožnit
jeho	jeho	k3xOp3gFnSc4	jeho
regulaci	regulace	k1gFnSc4	regulace
a	a	k8xC	a
konečné	konečný	k2eAgNnSc4d1	konečné
vymýcení	vymýcení	k1gNnSc4	vymýcení
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
cesta	cesta	k1gFnSc1	cesta
kompromisu	kompromis	k1gInSc2	kompromis
<g/>
.	.	kIx.	.
</s>
<s>
Cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
udržení	udržení	k1gNnSc4	udržení
souhlasu	souhlas	k1gInSc2	souhlas
Georgie	Georgie	k1gFnSc2	Georgie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Karolíny	Karolína	k1gFnSc2	Karolína
s	s	k7c7	s
Ústavou	ústava	k1gFnSc7	ústava
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zahrnutí	zahrnutí	k1gNnSc2	zahrnutí
čtyř	čtyři	k4xCgNnPc2	čtyři
ustanovení	ustanovení	k1gNnPc2	ustanovení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
otroctví	otroctví	k1gNnSc1	otroctví
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
ještě	ještě	k6eAd1	ještě
nejméně	málo	k6eAd3	málo
dalších	další	k2eAgNnPc2d1	další
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Oddíl	oddíl	k1gInSc1	oddíl
9	[number]	k4	9
článku	článek	k1gInSc2	článek
I	i	k9	i
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
"	"	kIx"	"
<g/>
dovoz	dovoz	k1gInSc1	dovoz
<g/>
"	"	kIx"	"
takových	takový	k3xDgFnPc2	takový
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
článku	článek	k1gInSc2	článek
IV	IV	kA	IV
zakazoval	zakazovat	k5eAaImAgMnS	zakazovat
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
pomoci	pomoct	k5eAaPmF	pomoct
uprchlým	uprchlý	k2eAgFnPc3d1	uprchlá
osobám	osoba	k1gFnPc3	osoba
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
jejich	jejich	k3xOp3gInSc4	jejich
návrat	návrat	k1gInSc4	návrat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
zadržení	zadržení	k1gNnSc2	zadržení
a	a	k8xC	a
oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
článku	článek	k1gInSc2	článek
I	i	k8xC	i
definoval	definovat	k5eAaBmAgMnS	definovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
osoby	osoba	k1gFnPc1	osoba
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
tři	tři	k4xCgFnPc1	tři
pětiny	pětina	k1gFnPc1	pětina
<g/>
"	"	kIx"	"
osob	osoba	k1gFnPc2	osoba
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
celkové	celkový	k2eAgFnSc2d1	celková
oficiální	oficiální	k2eAgFnSc2d1	oficiální
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
sloužila	sloužit	k5eAaImAgFnS	sloužit
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
počtu	počet	k1gInSc2	počet
zastupitelů	zastupitel	k1gMnPc2	zastupitel
států	stát	k1gInPc2	stát
a	a	k8xC	a
pro	pro	k7c4	pro
výpočty	výpočet	k1gInPc4	výpočet
federálních	federální	k2eAgFnPc2d1	federální
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
V	V	kA	V
zakazoval	zakazovat	k5eAaImAgInS	zakazovat
jakékoliv	jakýkoliv	k3yIgInPc4	jakýkoliv
dodatky	dodatek	k1gInPc4	dodatek
nebo	nebo	k8xC	nebo
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
měnily	měnit	k5eAaImAgFnP	měnit
opatření	opatření	k1gNnSc4	opatření
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
dovozu	dovoz	k1gInSc3	dovoz
otroků	otrok	k1gMnPc2	otrok
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dával	dávat	k5eAaImAgMnS	dávat
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
na	na	k7c4	na
vyřešení	vyřešení	k1gNnSc4	vyřešení
této	tento	k3xDgFnSc2	tento
otázky	otázka	k1gFnSc2	otázka
dalších	další	k2eAgInPc2d1	další
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
textu	text	k1gInSc6	text
se	se	k3xPyFc4	se
však	však	k9	však
tvůrci	tvůrce	k1gMnPc1	tvůrce
Ústavy	ústava	k1gFnSc2	ústava
výrazům	výraz	k1gInPc3	výraz
"	"	kIx"	"
<g/>
otrok	otrok	k1gMnSc1	otrok
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
otroctví	otroctví	k1gNnSc1	otroctví
<g/>
"	"	kIx"	"
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
a	a	k8xC	a
uchylovali	uchylovat	k5eAaImAgMnP	uchylovat
se	se	k3xPyFc4	se
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
opisům	opis	k1gInPc3	opis
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
někteří	některý	k3yIgMnPc1	některý
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
chybu	chyba	k1gFnSc4	chyba
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
dokumentu	dokument	k1gInSc6	dokument
jakkoliv	jakkoliv	k6eAd1	jakkoliv
připustit	připustit	k5eAaPmF	připustit
"	"	kIx"	"
<g/>
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
mohou	moct	k5eAaImIp3nP	moct
představovat	představovat	k5eAaImF	představovat
majetek	majetek	k1gInSc4	majetek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Delegáti	delegát	k1gMnPc1	delegát
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
přáli	přát	k5eAaImAgMnP	přát
zrušení	zrušení	k1gNnSc4	zrušení
otrokářského	otrokářský	k2eAgInSc2d1	otrokářský
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
stavěli	stavět	k5eAaImAgMnP	stavět
na	na	k7c6	na
následných	následný	k2eAgInPc6d1	následný
ratifikačních	ratifikační	k2eAgInPc6d1	ratifikační
konventech	konvent	k1gInPc6	konvent
proti	proti	k7c3	proti
přijetí	přijetí	k1gNnSc3	přijetí
této	tento	k3xDgFnSc2	tento
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
otroctví	otroctví	k1gNnSc1	otroctví
výslovně	výslovně	k6eAd1	výslovně
nezruší	zrušit	k5eNaPmIp3nS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
nechali	nechat	k5eAaPmAgMnP	nechat
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozpad	rozpad	k1gInSc1	rozpad
Unie	unie	k1gFnSc2	unie
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mnohem	mnohem	k6eAd1	mnohem
horší	zlý	k2eAgInPc4d2	horší
následky	následek	k1gInPc4	následek
<g/>
,	,	kIx,	,
než	než	k8xS	než
když	když	k8xS	když
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
otázka	otázka	k1gFnSc1	otázka
ponechá	ponechat	k5eAaPmIp3nS	ponechat
postupnému	postupný	k2eAgNnSc3d1	postupné
řešení	řešení	k1gNnSc3	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nevyřešený	vyřešený	k2eNgInSc1d1	nevyřešený
spor	spor	k1gInSc1	spor
později	pozdě	k6eAd2	pozdě
také	také	k9	také
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ratifikace	ratifikace	k1gFnSc2	ratifikace
Ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
uvedení	uvedení	k1gNnSc2	uvedení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
===	===	k?	===
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1787	[number]	k4	1787
byla	být	k5eAaImAgFnS	být
Ústava	ústava	k1gFnSc1	ústava
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
projev	projev	k1gInSc1	projev
Benjamina	Benjamin	k1gMnSc2	Benjamin
Franklina	Franklina	k1gFnSc1	Franklina
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
Konvent	konvent	k1gInSc1	konvent
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
postačí	postačit	k5eAaPmIp3nS	postačit
souhlas	souhlas	k1gInSc1	souhlas
devíti	devět	k4xCc2	devět
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
apeloval	apelovat	k5eAaImAgInS	apelovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
státy	stát	k1gInPc1	stát
přijaly	přijmout	k5eAaPmAgInP	přijmout
Ústavu	ústava	k1gFnSc4	ústava
jednohlasně	jednohlasně	k6eAd1	jednohlasně
<g/>
.	.	kIx.	.
</s>
<s>
Konvent	konvent	k1gInSc1	konvent
pak	pak	k6eAd1	pak
Ústavu	ústav	k1gInSc6	ústav
postoupil	postoupit	k5eAaPmAgInS	postoupit
Kontinentálnímu	kontinentální	k2eAgMnSc3d1	kontinentální
kongresu	kongres	k1gInSc6	kongres
<g/>
.	.	kIx.	.
<g/>
Massachusettský	massachusettský	k2eAgMnSc1d1	massachusettský
politik	politik	k1gMnSc1	politik
Rufus	Rufus	k1gMnSc1	Rufus
King	King	k1gMnSc1	King
se	se	k3xPyFc4	se
o	o	k7c6	o
Filadelfském	filadelfský	k2eAgInSc6d1	filadelfský
konventu	konvent	k1gInSc6	konvent
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
jako	jako	k8xS	jako
o	o	k7c6	o
výtvoru	výtvor	k1gInSc6	výtvor
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
nezávislém	závislý	k2eNgNnSc6d1	nezávislé
na	na	k7c6	na
Kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
svůj	svůj	k3xOyFgInSc4	svůj
návrh	návrh	k1gInSc4	návrh
poslal	poslat	k5eAaPmAgMnS	poslat
jen	jen	k9	jen
z	z	k7c2	z
procedurálních	procedurální	k2eAgInPc2d1	procedurální
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byly	být	k5eAaImAgInP	být
projednány	projednat	k5eAaPmNgInP	projednat
i	i	k9	i
dodatky	dodatek	k1gInPc1	dodatek
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
byly	být	k5eAaImAgInP	být
zamítnuty	zamítnout	k5eAaPmNgInP	zamítnout
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1787	[number]	k4	1787
Kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
kongres	kongres	k1gInSc1	kongres
jednohlasně	jednohlasně	k6eAd1	jednohlasně
schválil	schválit	k5eAaPmAgInS	schválit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústava	ústava	k1gFnSc1	ústava
bude	být	k5eAaImBp3nS	být
předána	předat	k5eAaPmNgFnS	předat
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
sborům	sbor	k1gInPc3	sbor
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
postoupily	postoupit	k5eAaPmAgFnP	postoupit
svým	svůj	k3xOyFgInPc3	svůj
ratifikačním	ratifikační	k2eAgInPc3d1	ratifikační
konventům	konvent	k1gInPc3	konvent
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
států	stát	k1gInPc2	stát
navýšilo	navýšit	k5eAaPmAgNnS	navýšit
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
zvolení	zvolení	k1gNnSc2	zvolení
ratifikačních	ratifikační	k2eAgMnPc2d1	ratifikační
zastupitelů	zastupitel	k1gMnPc2	zastupitel
počty	počet	k1gInPc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
hlasovat	hlasovat	k5eAaImF	hlasovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
naplněna	naplnit	k5eAaPmNgFnS	naplnit
nová	nový	k2eAgFnSc1d1	nová
společenská	společenský	k2eAgFnSc1d1	společenská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začínala	začínat	k5eAaImAgFnS	začínat
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
lid	lid	k1gInSc1	lid
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Federalistické	federalistický	k2eAgFnPc1d1	federalistická
menšiny	menšina	k1gFnPc1	menšina
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
i	i	k9	i
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
následovaly	následovat	k5eAaImAgInP	následovat
příkladu	příklad	k1gInSc3	příklad
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
konventu	konvent	k1gInSc6	konvent
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
přijetí	přijetí	k1gNnSc4	přijetí
Ústavy	ústava	k1gFnSc2	ústava
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ratifikaci	ratifikace	k1gFnSc4	ratifikace
spojili	spojit	k5eAaPmAgMnP	spojit
s	s	k7c7	s
doporučenými	doporučený	k2eAgInPc7d1	doporučený
dodatky	dodatek	k1gInPc7	dodatek
<g/>
.	.	kIx.	.
</s>
<s>
Menšina	menšina	k1gFnSc1	menšina
kritiků	kritik	k1gMnPc2	kritik
Ústavy	ústava	k1gFnSc2	ústava
však	však	k9	však
stále	stále	k6eAd1	stále
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
odporu	odpor	k1gInSc6	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Marylandský	Marylandský	k2eAgMnSc1d1	Marylandský
politik	politik	k1gMnSc1	politik
Luther	Luthra	k1gFnPc2	Luthra
Martin	Martin	k1gMnSc1	Martin
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Filadelfský	filadelfský	k2eAgInSc1d1	filadelfský
konvent	konvent	k1gInSc1	konvent
překročil	překročit	k5eAaPmAgInS	překročit
svou	svůj	k3xOyFgFnSc4	svůj
pravomoc	pravomoc	k1gFnSc4	pravomoc
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
dožadoval	dožadovat	k5eAaImAgInS	dožadovat
pouhého	pouhý	k2eAgNnSc2d1	pouhé
přijetí	přijetí	k1gNnSc2	přijetí
dodatků	dodatek	k1gInPc2	dodatek
k	k	k7c3	k
původním	původní	k2eAgInPc3d1	původní
Článkům	článek	k1gInPc3	článek
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
13	[number]	k4	13
Článků	článek	k1gInPc2	článek
Konfederace	konfederace	k1gFnSc2	konfederace
říkal	říkat	k5eAaImAgInS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
unie	unie	k1gFnSc1	unie
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
těmito	tento	k3xDgInPc7	tento
Články	článek	k1gInPc7	článek
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
trvalá	trvalý	k2eAgFnSc1d1	trvalá
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
změna	změna	k1gFnSc1	změna
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
odsouhlasena	odsouhlasit	k5eAaPmNgFnS	odsouhlasit
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
poté	poté	k6eAd1	poté
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
zákonodárnými	zákonodárný	k2eAgInPc7d1	zákonodárný
sbory	sbor	k1gInPc7	sbor
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
<g/>
Požadavky	požadavek	k1gInPc1	požadavek
ustanovené	ustanovený	k2eAgInPc1d1	ustanovený
v	v	k7c6	v
Článcích	článek	k1gInPc6	článek
konfederace	konfederace	k1gFnSc2	konfederace
však	však	k8xC	však
činily	činit	k5eAaImAgInP	činit
jakékoliv	jakýkoliv	k3yIgInPc1	jakýkoliv
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
reformu	reforma	k1gFnSc4	reforma
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
<s>
Martinovi	Martinův	k2eAgMnPc1d1	Martinův
spojenci	spojenec	k1gMnPc1	spojenec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
newyorský	newyorský	k2eAgMnSc1d1	newyorský
politik	politik	k1gMnSc1	politik
John	John	k1gMnSc1	John
Lansing	Lansing	k1gInSc1	Lansing
jr	jr	k?	jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vzdali	vzdát	k5eAaPmAgMnP	vzdát
pokusy	pokus	k1gInPc4	pokus
bránit	bránit	k5eAaImF	bránit
ústavnímu	ústavní	k2eAgMnSc3d1	ústavní
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
dožadovali	dožadovat	k5eAaImAgMnP	dožadovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
přijetí	přijetí	k1gNnSc1	přijetí
dodatků	dodatek	k1gInPc2	dodatek
k	k	k7c3	k
navržené	navržený	k2eAgFnSc3d1	navržená
Ústavě	ústava	k1gFnSc3	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
konventech	konvent	k1gInPc6	konvent
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
politici	politik	k1gMnPc1	politik
prosazující	prosazující	k2eAgFnSc2d1	prosazující
přijetí	přijetí	k1gNnSc3	přijetí
dodatků	dodatek	k1gInPc2	dodatek
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
ještě	ještě	k9	ještě
před	před	k7c7	před
její	její	k3xOp3gFnSc7	její
ratifikací	ratifikace	k1gFnSc7	ratifikace
nakonec	nakonec	k6eAd1	nakonec
smířili	smířit	k5eAaPmAgMnP	smířit
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
zachování	zachování	k1gNnSc2	zachování
Unie	unie	k1gFnSc2	unie
budou	být	k5eAaImBp3nP	být
dodatky	dodatek	k1gInPc1	dodatek
přijaty	přijmout	k5eAaPmNgInP	přijmout
až	až	k6eAd1	až
po	po	k7c6	po
ratifikaci	ratifikace	k1gFnSc6	ratifikace
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
zákonodárné	zákonodárný	k2eAgInPc1d1	zákonodárný
sbory	sbor	k1gInPc1	sbor
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
obdržely	obdržet	k5eAaPmAgInP	obdržet
newyorský	newyorský	k2eAgInSc4d1	newyorský
protiratifikační	protiratifikační	k2eAgInSc4d1	protiratifikační
oběžník	oběžník	k1gInSc4	oběžník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
nejprve	nejprve	k6eAd1	nejprve
přijaty	přijmout	k5eAaPmNgInP	přijmout
dodatky	dodatek	k1gInPc7	dodatek
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
a	a	k8xC	a
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
před	před	k7c7	před
ratifikací	ratifikace	k1gFnSc7	ratifikace
počkat	počkat	k5eAaPmF	počkat
<g/>
,	,	kIx,	,
až	až	k9	až
nový	nový	k2eAgInSc4d1	nový
Kongres	kongres	k1gInSc4	kongres
dodatky	dodatek	k1gInPc7	dodatek
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
.	.	kIx.	.
<g/>
Článek	článek	k1gInSc4	článek
VII	VII	kA	VII
navrhované	navrhovaný	k2eAgFnSc2d1	navrhovaná
ústavy	ústava	k1gFnSc2	ústava
stanovoval	stanovovat	k5eAaImAgMnS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
ji	on	k3xPp3gFnSc4	on
ratifikuje	ratifikovat	k5eAaBmIp3nS	ratifikovat
devět	devět	k4xCc1	devět
ze	z	k7c2	z
třinácti	třináct	k4xCc2	třináct
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
vlády	vláda	k1gFnSc2	vláda
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zúčastněné	zúčastněný	k2eAgInPc4d1	zúčastněný
státy	stát	k1gInPc4	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1788	[number]	k4	1788
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
ratifikačních	ratifikační	k2eAgInPc2d1	ratifikační
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Kongres	kongres	k1gInSc1	kongres
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústava	ústava	k1gFnSc1	ústava
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
federální	federální	k2eAgFnSc4d1	federální
vládu	vláda	k1gFnSc4	vláda
přijalo	přijmout	k5eAaPmAgNnS	přijmout
jedenáct	jedenáct	k4xCc1	jedenáct
ze	z	k7c2	z
třinácti	třináct	k4xCc2	třináct
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
zahájí	zahájit	k5eAaPmIp3nS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
první	první	k4xOgFnSc4	první
březnovou	březnový	k2eAgFnSc4d1	březnová
středu	středa	k1gFnSc4	středa
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
City	City	k1gFnPc2	City
<g/>
,	,	kIx,	,
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1789	[number]	k4	1789
se	se	k3xPyFc4	se
tak	tak	k9	tak
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
George	George	k1gInSc1	George
Washington	Washington	k1gInSc1	Washington
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
Filadelfském	filadelfský	k2eAgInSc6d1	filadelfský
ústavním	ústavní	k2eAgInSc6d1	ústavní
konventu	konvent	k1gInSc6	konvent
bránil	bránit	k5eAaImAgMnS	bránit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
vzdát	vzdát	k5eAaPmF	vzdát
svých	svůj	k3xOyFgFnPc2	svůj
suverenit	suverenita	k1gFnPc2	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
účast	účast	k1gFnSc4	účast
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jednohlasně	jednohlasně	k6eAd1	jednohlasně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
konventu	konvent	k1gInSc2	konvent
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
hlasu	hlas	k1gInSc2	hlas
virginského	virginský	k2eAgMnSc2d1	virginský
antifederalisty	antifederalista	k1gMnSc2	antifederalista
Patricka	Patricko	k1gNnSc2	Patricko
Henryho	Henry	k1gMnSc2	Henry
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Kongres	kongres	k1gInSc1	kongres
byl	být	k5eAaImAgInS	být
triumfem	triumf	k1gInSc7	triumf
federalistů	federalista	k1gMnPc2	federalista
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
sestávající	sestávající	k2eAgInSc1d1	sestávající
ze	z	k7c2	z
zastupitelů	zastupitel	k1gMnPc2	zastupitel
11	[number]	k4	11
států	stát	k1gInPc2	stát
tvořilo	tvořit	k5eAaImAgNnS	tvořit
20	[number]	k4	20
federalistů	federalista	k1gMnPc2	federalista
a	a	k8xC	a
jen	jen	k9	jen
2	[number]	k4	2
virginští	virginský	k2eAgMnPc1d1	virginský
antifederalisté	antifederalista	k1gMnPc1	antifederalista
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovnu	sněmovna	k1gFnSc4	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
48	[number]	k4	48
federalistů	federalista	k1gMnPc2	federalista
proti	proti	k7c3	proti
11	[number]	k4	11
antifederalistům	antifederalista	k1gMnPc3	antifederalista
pocházejícím	pocházející	k2eAgFnPc3d1	pocházející
z	z	k7c2	z
pouhých	pouhý	k2eAgInPc2d1	pouhý
čtyř	čtyři	k4xCgInPc2	čtyři
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnPc1	Virginium
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
.	.	kIx.	.
<g/>
Obavy	obava	k1gFnPc1	obava
antifederalistů	antifederalista	k1gMnPc2	antifederalista
z	z	k7c2	z
útlaku	útlak	k1gInSc2	útlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Kongresu	kongres	k1gInSc2	kongres
byly	být	k5eAaImAgFnP	být
rozptýleny	rozptýlit	k5eAaPmNgFnP	rozptýlit
přijetím	přijetí	k1gNnSc7	přijetí
dodatků	dodatek	k1gInPc2	dodatek
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgFnPc4	jenž
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
zasedání	zasedání	k1gNnSc6	zasedání
zasadil	zasadit	k5eAaPmAgMnS	zasadit
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgNnPc2	tento
prvních	první	k4xOgInPc2	první
deset	deset	k4xCc1	deset
dodatků	dodatek	k1gInPc2	dodatek
vešlo	vejít	k5eAaPmAgNnS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k8xC	jako
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Námitky	námitka	k1gFnPc1	námitka
proti	proti	k7c3	proti
potenciálně	potenciálně	k6eAd1	potenciálně
odtažitému	odtažitý	k2eAgNnSc3d1	odtažité
federálnímu	federální	k2eAgNnSc3d1	federální
soudnictví	soudnictví	k1gNnSc3	soudnictví
byly	být	k5eAaImAgFnP	být
utišeny	utišen	k2eAgFnPc1d1	utišena
ustanovením	ustanovení	k1gNnSc7	ustanovení
13	[number]	k4	13
federálních	federální	k2eAgInPc2d1	federální
soudů	soud	k1gInPc2	soud
(	(	kIx(	(
<g/>
11	[number]	k4	11
států	stát	k1gInPc2	stát
plus	plus	k6eAd1	plus
Maine	Main	k1gInSc5	Main
a	a	k8xC	a
Kentucky	Kentucka	k1gFnPc1	Kentucka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
neměly	mít	k5eNaImAgFnP	mít
statut	statut	k1gInSc4	statut
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
a	a	k8xC	a
tří	tři	k4xCgInPc2	tři
federálních	federální	k2eAgInPc2d1	federální
okružních	okružní	k2eAgInPc2d1	okružní
obvodů	obvod	k1gInPc2	obvod
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
:	:	kIx,	:
Východní	východní	k2eAgFnSc1d1	východní
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
mocné	mocný	k2eAgFnSc2d1	mocná
federální	federální	k2eAgFnSc2d1	federální
exekutivy	exekutiva	k1gFnSc2	exekutiva
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
Washingtonova	Washingtonův	k2eAgFnSc1d1	Washingtonova
vláda	vláda	k1gFnSc1	vláda
jmenováním	jmenování	k1gNnSc7	jmenování
bývalých	bývalý	k2eAgMnPc2d1	bývalý
antifederalistů	antifederalista	k1gMnPc2	antifederalista
Edmunda	Edmund	k1gMnSc2	Edmund
Jenningse	Jennings	k1gMnSc2	Jennings
Randolpha	Randolpha	k1gMnSc1	Randolpha
generálním	generální	k2eAgMnSc7d1	generální
prokurátorem	prokurátor	k1gMnSc7	prokurátor
a	a	k8xC	a
Thomase	Thomas	k1gMnSc2	Thomas
Jeffersona	Jefferson	k1gMnSc2	Jefferson
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historické	historický	k2eAgInPc1d1	historický
vlivy	vliv	k1gInPc1	vliv
===	===	k?	===
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
myšlenek	myšlenka	k1gFnPc2	myšlenka
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
nových	nový	k2eAgNnPc2d1	nové
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
převzato	převzít	k5eAaPmNgNnS	převzít
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
republikánské	republikánský	k2eAgFnSc2d1	republikánská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zkušeností	zkušenost	k1gFnPc2	zkušenost
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
a	a	k8xC	a
z	z	k7c2	z
britské	britský	k2eAgFnSc2d1	britská
zkušenosti	zkušenost	k1gFnSc2	zkušenost
se	s	k7c7	s
smíšenou	smíšený	k2eAgFnSc7d1	smíšená
formou	forma	k1gFnSc7	forma
vládnutí	vládnutí	k1gNnSc2	vládnutí
(	(	kIx(	(
<g/>
kombinující	kombinující	k2eAgFnPc1d1	kombinující
demokratické	demokratický	k2eAgFnPc1d1	demokratická
<g/>
,	,	kIx,	,
aristokratické	aristokratický	k2eAgFnPc1d1	aristokratická
a	a	k8xC	a
monarchistické	monarchistický	k2eAgInPc1d1	monarchistický
prvky	prvek	k1gInPc1	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
měli	mít	k5eAaImAgMnP	mít
na	na	k7c4	na
tvůrce	tvůrce	k1gMnSc4	tvůrce
ústavy	ústava	k1gFnSc2	ústava
také	také	k9	také
evropští	evropský	k2eAgMnPc1d1	evropský
myslitelé	myslitel	k1gMnPc1	myslitel
James	James	k1gMnSc1	James
Harrington	Harrington	k1gInSc1	Harrington
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Locke	Lock	k1gFnSc2	Lock
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Montesquieu	Montesquiea	k1gMnSc4	Montesquiea
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
kladli	klást	k5eAaImAgMnP	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
potřebu	potřeba	k1gFnSc4	potřeba
vyvážených	vyvážený	k2eAgFnPc2d1	vyvážená
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
budou	být	k5eAaImBp3nP	být
působit	působit	k5eAaImF	působit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
předejde	předejít	k5eAaPmIp3nS	předejít
tyranii	tyranie	k1gFnSc3	tyranie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
pak	pak	k6eAd1	pak
odráží	odrážet	k5eAaImIp3nS	odrážet
vliv	vliv	k1gInSc4	vliv
<g />
.	.	kIx.	.
</s>
<s>
řeckého	řecký	k2eAgMnSc2d1	řecký
historika	historik	k1gMnSc2	historik
Polybia	Polybius	k1gMnSc2	Polybius
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
myšlenku	myšlenka	k1gFnSc4	myšlenka
dělby	dělba	k1gFnSc2	dělba
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pak	pak	k6eAd1	pak
nalezla	nalézt	k5eAaBmAgFnS	nalézt
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
britský	britský	k2eAgMnSc1d1	britský
politický	politický	k2eAgMnSc1d1	politický
filosof	filosof	k1gMnSc1	filosof
John	John	k1gMnSc1	John
Locke	Locke	k1gFnSc1	Locke
a	a	k8xC	a
právní	právní	k2eAgFnSc1d1	právní
jistota	jistota	k1gFnSc1	jistota
Ústavy	ústava	k1gFnSc2	ústava
byla	být	k5eAaImAgFnS	být
částečně	částečně	k6eAd1	částečně
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
zvykovém	zvykový	k2eAgNnSc6d1	zvykové
právu	právo	k1gNnSc6	právo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
historie	historie	k1gFnSc1	historie
má	mít	k5eAaImIp3nS	mít
počátky	počátek	k1gInPc4	počátek
v	v	k7c6	v
období	období	k1gNnSc6	období
britské	britský	k2eAgFnSc2d1	britská
Magny	Magna	k1gFnSc2	Magna
charty	charta	k1gFnSc2	charta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vlivy	vliv	k1gInPc4	vliv
amerických	americký	k2eAgMnPc2d1	americký
domorodců	domorodec	k1gMnPc2	domorodec
====	====	k?	====
</s>
</p>
<p>
<s>
Určité	určitý	k2eAgInPc1d1	určitý
vlivy	vliv	k1gInPc1	vliv
jak	jak	k8xC	jak
na	na	k7c4	na
původní	původní	k2eAgInPc4d1	původní
Články	článek	k1gInPc4	článek
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
konečnou	konečný	k2eAgFnSc4d1	konečná
podobu	podoba	k1gFnSc4	podoba
Ústavy	ústava	k1gFnSc2	ústava
jsou	být	k5eAaImIp3nP	být
přisuzovány	přisuzovat	k5eAaImNgFnP	přisuzovat
tzv.	tzv.	kA	tzv.
Irokézské	irokézský	k2eAgFnSc6d1	Irokézská
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
politickému	politický	k2eAgMnSc3d1	politický
seskupení	seskupení	k1gNnSc1	seskupení
několika	několik	k4yIc2	několik
irokézských	irokézský	k2eAgInPc2d1	irokézský
kmenů	kmen	k1gInPc2	kmen
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yIgFnSc7	jaký
si	se	k3xPyFc3	se
kolonisté	kolonista	k1gMnPc1	kolonista
vypůjčovali	vypůjčovat	k5eAaImAgMnP	vypůjčovat
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
již	již	k6eAd1	již
existujících	existující	k2eAgInPc2d1	existující
vládních	vládní	k2eAgInPc2d1	vládní
modelů	model	k1gInPc2	model
domorodých	domorodý	k2eAgInPc2d1	domorodý
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
předmětem	předmět	k1gInSc7	předmět
debaty	debata	k1gFnSc2	debata
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
otců	otec	k1gMnPc2	otec
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
indiánskými	indiánský	k2eAgMnPc7d1	indiánský
vůdci	vůdce	k1gMnPc7	vůdce
a	a	k8xC	a
poznali	poznat	k5eAaPmAgMnP	poznat
jejich	jejich	k3xOp3gFnPc4	jejich
formy	forma	k1gFnPc4	forma
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlejší	rozsáhlý	k2eAgInPc4d2	rozsáhlejší
styky	styk	k1gInPc4	styk
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
Irokézské	irokézský	k2eAgFnSc2d1	Irokézská
ligy	liga	k1gFnSc2	liga
měly	mít	k5eAaImAgFnP	mít
i	i	k9	i
takové	takový	k3xDgFnPc1	takový
významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
jako	jako	k8xC	jako
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
a	a	k8xC	a
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklina	k1gFnPc2	Franklina
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Rutledge	Rutledg	k1gInSc2	Rutledg
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Karolíny	Karolína	k1gFnSc2	Karolína
údajně	údajně	k6eAd1	údajně
četl	číst	k5eAaImAgMnS	číst
farmářům	farmář	k1gMnPc3	farmář
obsáhlá	obsáhlý	k2eAgFnSc1d1	obsáhlá
pojednání	pojednání	k1gNnSc1	pojednání
irokézského	irokézský	k2eAgNnSc2d1	irokézský
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začínala	začínat	k5eAaImAgFnS	začínat
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
jednotu	jednota	k1gFnSc4	jednota
<g/>
,	,	kIx,	,
udrželi	udržet	k5eAaPmAgMnP	udržet
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
rovnost	rovnost	k1gFnSc4	rovnost
a	a	k8xC	a
pořádek	pořádek	k1gInSc4	pořádek
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1988	[number]	k4	1988
přijal	přijmout	k5eAaPmAgInS	přijmout
Kongres	kongres	k1gInSc1	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
rezoluci	rezoluce	k1gFnSc3	rezoluce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
uznal	uznat	k5eAaPmAgInS	uznat
vliv	vliv	k1gInSc1	vliv
irokézského	irokézský	k2eAgInSc2d1	irokézský
systému	systém	k1gInSc2	systém
na	na	k7c6	na
Ústavu	ústav	k1gInSc6	ústav
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
Listinu	listina	k1gFnSc4	listina
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vlivy	vliv	k1gInPc1	vliv
na	na	k7c4	na
Listinu	listina	k1gFnSc4	listina
práv	právo	k1gNnPc2	právo
====	====	k?	====
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
deseti	deset	k4xCc2	deset
dodatků	dodatek	k1gInPc2	dodatek
přidaných	přidaná	k1gFnPc2	přidaná
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc4	ten
její	její	k3xOp3gMnPc1	její
podporovatelé	podporovatel	k1gMnPc1	podporovatel
slíbili	slíbit	k5eAaPmAgMnP	slíbit
jejím	její	k3xOp3gMnPc3	její
kritikům	kritik	k1gMnPc3	kritik
během	během	k7c2	během
debat	debata	k1gFnPc2	debata
roku	rok	k1gInSc2	rok
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
listinu	listina	k1gFnSc4	listina
byla	být	k5eAaImAgFnS	být
anglická	anglický	k2eAgFnSc1d1	anglická
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1689	[number]	k4	1689
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
listiny	listina	k1gFnPc1	listina
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
soudními	soudní	k2eAgInPc7d1	soudní
procesy	proces	k1gInPc7	proces
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
právo	právo	k1gNnSc4	právo
držet	držet	k5eAaImF	držet
a	a	k8xC	a
nosit	nosit	k5eAaImF	nosit
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
zamezují	zamezovat	k5eAaImIp3nP	zamezovat
nepřiměřeným	přiměřený	k2eNgFnPc3d1	nepřiměřená
kaucím	kauce	k1gFnPc3	kauce
a	a	k8xC	a
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
"	"	kIx"	"
<g/>
kruté	krutý	k2eAgInPc4d1	krutý
a	a	k8xC	a
neobvyklé	obvyklý	k2eNgInPc4d1	neobvyklý
tresty	trest	k1gInPc4	trest
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
Listiny	listina	k1gFnSc2	listina
práv	právo	k1gNnPc2	právo
bylo	být	k5eAaImAgNnS	být
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
svobod	svoboda	k1gFnPc2	svoboda
zaručených	zaručený	k2eAgInPc2d1	zaručený
ústavami	ústava	k1gFnPc7	ústava
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
a	a	k8xC	a
Virginským	virginský	k2eAgFnPc3d1	virginská
prohlášení	prohlášení	k1gNnSc6	prohlášení
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Články	článek	k1gInPc1	článek
Ústavy	ústava	k1gFnSc2	ústava
==	==	k?	==
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
Ústavy	ústava	k1gFnSc2	ústava
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
preambulí	preambule	k1gFnSc7	preambule
a	a	k8xC	a
sedmi	sedm	k4xCc7	sedm
články	článek	k1gInPc7	článek
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
27	[number]	k4	27
dodatků	dodatek	k1gInPc2	dodatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Preambule	preambule	k1gFnSc2	preambule
===	===	k?	===
</s>
</p>
<p>
<s>
Preambule	preambule	k1gFnSc1	preambule
Ústavy	ústava	k1gFnSc2	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
je	být	k5eAaImIp3nS	být
stručný	stručný	k2eAgInSc4d1	stručný
úvodní	úvodní	k2eAgInSc4d1	úvodní
výrok	výrok	k1gInSc4	výrok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
základní	základní	k2eAgInPc4d1	základní
principy	princip	k1gInPc4	princip
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
Ústava	ústava	k1gFnSc1	ústava
založena	založit	k5eAaPmNgFnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecných	obecný	k2eAgInPc6d1	obecný
termínech	termín	k1gInPc6	termín
uvádí	uvádět	k5eAaImIp3nS	uvádět
původní	původní	k2eAgInPc4d1	původní
záměry	záměr	k1gInPc4	záměr
autorů	autor	k1gMnPc2	autor
Ústavy	ústava	k1gFnSc2	ústava
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
jejího	její	k3xOp3gInSc2	její
významu	význam	k1gInSc2	význam
a	a	k8xC	a
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
čeho	co	k3yRnSc2	co
chtěli	chtít	k5eAaImAgMnP	chtít
jejím	její	k3xOp3gNnSc7	její
přijetím	přijetí	k1gNnSc7	přijetí
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
preambule	preambule	k1gFnSc1	preambule
nepřipisuje	připisovat	k5eNaImIp3nS	připisovat
žádnou	žádný	k3yNgFnSc4	žádný
moc	moc	k1gFnSc1	moc
federální	federální	k2eAgFnSc1d1	federální
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jí	on	k3xPp3gFnSc3	on
nestanovuje	stanovovat	k5eNaImIp3nS	stanovovat
žádná	žádný	k3yNgNnPc4	žádný
konkrétní	konkrétní	k2eAgNnPc4d1	konkrétní
omezení	omezení	k1gNnPc4	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
nebývá	bývat	k5eNaImIp3nS	bývat
soudy	soud	k1gInPc4	soud
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
faktor	faktor	k1gInSc1	faktor
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
soudního	soudní	k2eAgInSc2d1	soudní
sporu	spor	k1gInSc2	spor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Článek	článek	k1gInSc1	článek
I	i	k9	i
<g/>
:	:	kIx,	:
Moc	moc	k6eAd1	moc
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
===	===	k?	===
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
I	I	kA	I
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgMnSc1d1	sestávající
celkem	celkem	k6eAd1	celkem
z	z	k7c2	z
10	[number]	k4	10
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
složku	složka	k1gFnSc4	složka
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Ustanovení	ustanovení	k1gNnSc1	ustanovení
dvou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Kongresu	kongres	k1gInSc2	kongres
====	====	k?	====
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
článku	článek	k1gInSc2	článek
I	i	k9	i
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškerá	veškerý	k3xTgFnSc1	veškerý
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
náleží	náležet	k5eAaImIp3nS	náležet
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
popisuje	popisovat	k5eAaImIp3nS	popisovat
Sněmovnu	sněmovna	k1gFnSc4	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
a	a	k8xC	a
způsob	způsob	k1gInSc4	způsob
jejího	její	k3xOp3gNnSc2	její
obsazování	obsazování	k1gNnSc2	obsazování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
ustanovení	ustanovení	k1gNnSc2	ustanovení
tohoto	tento	k3xDgInSc2	tento
oddílu	oddíl	k1gInSc2	oddíl
jsou	být	k5eAaImIp3nP	být
členové	člen	k1gMnPc1	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
občany	občan	k1gMnPc4	občan
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
volit	volit	k5eAaImF	volit
do	do	k7c2	do
početně	početně	k6eAd1	početně
silnější	silný	k2eAgFnSc2d2	silnější
komory	komora	k1gFnSc2	komora
zákonodárného	zákonodárný	k2eAgInSc2d1	zákonodárný
sboru	sbor	k1gInSc2	sbor
svého	svůj	k3xOyFgInSc2	svůj
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gMnSc1	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
25	[number]	k4	25
let	léto	k1gNnPc2	léto
stár	stár	k2eAgMnSc1d1	stár
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
7	[number]	k4	7
let	léto	k1gNnPc2	léto
občanem	občan	k1gMnSc7	občan
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
současně	současně	k6eAd1	současně
obyvatelem	obyvatel	k1gMnSc7	obyvatel
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
<g/>
.	.	kIx.	.
</s>
<s>
Místa	místo	k1gNnPc1	místo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
komoře	komora	k1gFnSc6	komora
a	a	k8xC	a
přímé	přímý	k2eAgFnSc2d1	přímá
daně	daň	k1gFnSc2	daň
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
mezi	mezi	k7c4	mezi
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
státy	stát	k1gInPc4	stát
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
jejich	jejich	k3xOp3gMnPc2	jejich
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
jako	jako	k9	jako
součet	součet	k1gInSc1	součet
všech	všecek	k3xTgFnPc2	všecek
"	"	kIx"	"
<g/>
svobodných	svobodný	k2eAgFnPc2d1	svobodná
osob	osoba	k1gFnPc2	osoba
<g/>
"	"	kIx"	"
a	a	k8xC	a
tří	tři	k4xCgFnPc2	tři
pětin	pětina	k1gFnPc2	pětina
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
osob	osoba	k1gFnPc2	osoba
kromě	kromě	k7c2	kromě
Indiánů	Indián	k1gMnPc2	Indián
neplatících	platící	k2eNgMnPc2d1	neplatící
daně	daň	k1gFnSc2	daň
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
především	především	k9	především
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
ustanovení	ustanovení	k1gNnSc1	ustanovení
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
14	[number]	k4	14
<g/>
.	.	kIx.	.
dodatkem	dodatek	k1gInSc7	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
přijatým	přijatý	k2eAgInSc7d1	přijatý
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
poprvé	poprvé	k6eAd1	poprvé
sečteno	sečten	k2eAgNnSc1d1	sečteno
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
zasedání	zasedání	k1gNnSc6	zasedání
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
každých	každý	k3xTgInPc2	každý
dalších	další	k2eAgInPc2d1	další
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každých	každý	k3xTgInPc2	každý
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
připadá	připadat	k5eAaPmIp3nS	připadat
maximálně	maximálně	k6eAd1	maximálně
jeden	jeden	k4xCgMnSc1	jeden
člen	člen	k1gMnSc1	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každý	každý	k3xTgInSc1	každý
stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
komoře	komora	k1gFnSc6	komora
zastoupen	zastoupen	k2eAgMnSc1d1	zastoupen
nejméně	málo	k6eAd3	málo
jedním	jeden	k4xCgMnSc7	jeden
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
ustanovení	ustanovení	k1gNnPc2	ustanovení
také	také	k9	také
určilo	určit	k5eAaPmAgNnS	určit
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
zastoupení	zastoupení	k1gNnSc2	zastoupení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
budou	být	k5eAaImBp3nP	být
známy	znám	k2eAgFnPc1d1	známa
výsledky	výsledek	k1gInPc4	výsledek
prvního	první	k4xOgNnSc2	první
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
<g/>
:	:	kIx,	:
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
3	[number]	k4	3
zastupitele	zastupitel	k1gMnSc2	zastupitel
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
8	[number]	k4	8
<g/>
,	,	kIx,	,
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
a	a	k8xC	a
osada	osada	k1gFnSc1	osada
Providence	providence	k1gFnSc2	providence
1	[number]	k4	1
<g/>
,	,	kIx,	,
Connecticut	Connecticut	k1gInSc1	Connecticut
5	[number]	k4	5
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
6	[number]	k4	6
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
4	[number]	k4	4
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
8	[number]	k4	8
<g/>
,	,	kIx,	,
Delaware	Delawar	k1gMnSc5	Delawar
1	[number]	k4	1
<g/>
,	,	kIx,	,
Maryland	Maryland	k1gInSc1	Maryland
6	[number]	k4	6
<g/>
,	,	kIx,	,
Virginie	Virginie	k1gFnSc1	Virginie
10	[number]	k4	10
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
5	[number]	k4	5
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolína	Karolína	k1gFnSc1	Karolína
5	[number]	k4	5
a	a	k8xC	a
Georgie	Georgie	k1gFnPc1	Georgie
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
uvolnění	uvolnění	k1gNnSc2	uvolnění
některého	některý	k3yIgMnSc2	některý
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
proběhnou	proběhnout	k5eAaPmIp3nP	proběhnout
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
státě	stát	k1gInSc6	stát
doplňovací	doplňovací	k2eAgFnSc2d1	doplňovací
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
posledního	poslední	k2eAgNnSc2d1	poslední
ustanovení	ustanovení	k1gNnSc2	ustanovení
tohoto	tento	k3xDgInSc2	tento
oddílu	oddíl	k1gInSc2	oddíl
má	mít	k5eAaImIp3nS	mít
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gInPc2	reprezentant
výhradní	výhradní	k2eAgNnSc4d1	výhradní
právo	právo	k1gNnSc4	právo
podávat	podávat	k5eAaImF	podávat
návrhy	návrh	k1gInPc4	návrh
na	na	k7c4	na
zbavení	zbavení	k1gNnSc4	zbavení
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
popisuje	popisovat	k5eAaImIp3nS	popisovat
Senát	senát	k1gInSc4	senát
a	a	k8xC	a
způsob	způsob	k1gInSc4	způsob
jeho	jeho	k3xOp3gNnSc2	jeho
obsazování	obsazování	k1gNnSc2	obsazování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
dvěma	dva	k4xCgInPc7	dva
senátory	senátor	k1gMnPc7	senátor
volenými	volená	k1gFnPc7	volená
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Uprázdněná	uprázdněný	k2eAgNnPc4d1	uprázdněné
křesla	křeslo	k1gNnPc4	křeslo
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
dočasně	dočasně	k6eAd1	dočasně
výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
příslušného	příslušný	k2eAgInSc2d1	příslušný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
zasedne	zasednout	k5eAaPmIp3nS	zasednout
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
orgán	orgán	k1gInSc1	orgán
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
uvolněné	uvolněný	k2eAgNnSc1d1	uvolněné
místo	místo	k1gNnSc1	místo
znovu	znovu	k6eAd1	znovu
obsadí	obsadit	k5eAaPmIp3nS	obsadit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tato	tento	k3xDgFnSc1	tento
ustanovení	ustanovení	k1gNnSc3	ustanovení
byla	být	k5eAaImAgFnS	být
změněna	změněn	k2eAgFnSc1d1	změněna
17	[number]	k4	17
<g/>
.	.	kIx.	.
dodatkem	dodatek	k1gInSc7	dodatek
Ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
senátoři	senátor	k1gMnPc1	senátor
budou	být	k5eAaImBp3nP	být
nadále	nadále	k6eAd1	nadále
voleni	volit	k5eAaImNgMnP	volit
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Volební	volební	k2eAgNnSc1d1	volební
období	období	k1gNnSc1	období
každého	každý	k3xTgMnSc2	každý
senátora	senátor	k1gMnSc2	senátor
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
na	na	k7c4	na
zcela	zcela	k6eAd1	zcela
prvním	první	k4xOgNnSc6	první
zasedání	zasedání	k1gNnSc6	zasedání
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
ustanovení	ustanovení	k1gNnSc2	ustanovení
tohoto	tento	k3xDgInSc2	tento
oddílu	oddíl	k1gInSc2	oddíl
senátoři	senátor	k1gMnPc1	senátor
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
početné	početný	k2eAgFnSc2d1	početná
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
ukončila	ukončit	k5eAaPmAgFnS	ukončit
své	svůj	k3xOyFgNnSc4	svůj
období	období	k1gNnSc4	období
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
třetí	třetí	k4xOgFnPc1	třetí
po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
konaných	konaný	k2eAgFnPc6d1	konaná
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
obsazována	obsazovat	k5eAaImNgFnS	obsazovat
vždy	vždy	k6eAd1	vždy
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
křesel	křeslo	k1gNnPc2	křeslo
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Senátor	senátor	k1gMnSc1	senátor
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
35	[number]	k4	35
let	léto	k1gNnPc2	léto
stár	stár	k2eAgMnSc1d1	stár
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
9	[number]	k4	9
let	léto	k1gNnPc2	léto
občanem	občan	k1gMnSc7	občan
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
současně	současně	k6eAd1	současně
obyvatelem	obyvatel	k1gMnSc7	obyvatel
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
Senátu	senát	k1gInSc2	senát
je	být	k5eAaImIp3nS	být
viceprezident	viceprezident	k1gMnSc1	viceprezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
hlasovat	hlasovat	k5eAaImF	hlasovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rovnosti	rovnost	k1gFnSc2	rovnost
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Senátoři	senátor	k1gMnPc1	senátor
si	se	k3xPyFc3	se
volí	volit	k5eAaImIp3nP	volit
tzv.	tzv.	kA	tzv.
prozatímního	prozatímní	k2eAgMnSc4d1	prozatímní
předsedu	předseda	k1gMnSc4	předseda
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Senát	senát	k1gInSc1	senát
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
všech	všecek	k3xTgInPc6	všecek
návrzích	návrh	k1gInPc6	návrh
na	na	k7c4	na
zbavení	zbavení	k1gNnSc4	zbavení
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
souhlas	souhlas	k1gInSc1	souhlas
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
přítomných	přítomný	k1gMnPc2	přítomný
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
Senát	senát	k1gInSc1	senát
soudí	soudit	k5eAaImIp3nS	soudit
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
jednání	jednání	k1gNnSc4	jednání
předseda	předseda	k1gMnSc1	předseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
4	[number]	k4	4
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podrobnosti	podrobnost	k1gFnPc1	podrobnost
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Kongresu	kongres	k1gInSc2	kongres
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
určeny	určit	k5eAaPmNgInP	určit
zákony	zákon	k1gInPc1	zákon
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Kongres	kongres	k1gInSc1	kongres
může	moct	k5eAaImIp3nS	moct
tato	tento	k3xDgNnPc4	tento
ustanovení	ustanovení	k1gNnPc4	ustanovení
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
určení	určení	k1gNnSc4	určení
míst	místo	k1gNnPc2	místo
volby	volba	k1gFnSc2	volba
senátorů	senátor	k1gMnPc2	senátor
<g/>
)	)	kIx)	)
kdykoliv	kdykoliv	k6eAd1	kdykoliv
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Procesní	procesní	k2eAgFnPc4d1	procesní
záležitosti	záležitost	k1gFnPc4	záležitost
====	====	k?	====
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
5	[number]	k4	5
článku	článek	k1gInSc2	článek
I	i	k8xC	i
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
pravidly	pravidlo	k1gNnPc7	pravidlo
zasedání	zasedání	k1gNnSc6	zasedání
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
ustanovení	ustanovení	k1gNnPc2	ustanovení
se	se	k3xPyFc4	se
schůze	schůze	k1gFnSc1	schůze
žádné	žádný	k3yNgFnSc2	žádný
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
nesmí	smět	k5eNaImIp3nS	smět
konat	konat	k5eAaImF	konat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
přítomna	přítomen	k2eAgFnSc1d1	přítomna
většina	většina	k1gFnSc1	většina
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
<g/>
;	;	kIx,	;
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
může	moct	k5eAaImIp3nS	moct
daná	daný	k2eAgFnSc1d1	daná
komora	komora	k1gFnSc1	komora
schůzi	schůze	k1gFnSc4	schůze
odložit	odložit	k5eAaPmF	odložit
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
komora	komora	k1gFnSc1	komora
si	se	k3xPyFc3	se
stanoví	stanovit	k5eAaPmIp3nS	stanovit
svůj	svůj	k3xOyFgInSc4	svůj
jednací	jednací	k2eAgInSc4d1	jednací
řád	řád	k1gInSc4	řád
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
trestat	trestat	k5eAaImF	trestat
své	svůj	k3xOyFgMnPc4	svůj
členy	člen	k1gMnPc4	člen
za	za	k7c2	za
provinění	provinění	k1gNnSc2	provinění
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgInSc3	tento
řádu	řád	k1gInSc3	řád
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyloučení	vyloučení	k1gNnSc3	vyloučení
člena	člen	k1gMnSc2	člen
kterékoliv	kterýkoliv	k3yIgFnSc2	kterýkoliv
komory	komora	k1gFnSc2	komora
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
souhlas	souhlas	k1gInSc1	souhlas
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
všech	všecek	k3xTgInPc2	všecek
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
pořizují	pořizovat	k5eAaImIp3nP	pořizovat
záznamy	záznam	k1gInPc1	záznam
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
zveřejňovány	zveřejňovat	k5eAaImNgInP	zveřejňovat
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
musí	muset	k5eAaImIp3nP	muset
dle	dle	k7c2	dle
mínění	mínění	k1gNnSc2	mínění
komory	komora	k1gFnSc2	komora
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
záznamech	záznam	k1gInPc6	záznam
jsou	být	k5eAaImIp3nP	být
uváděny	uvádět	k5eAaImNgInP	uvádět
také	také	k9	také
hlasy	hlas	k1gInPc1	hlas
pro	pro	k7c4	pro
a	a	k8xC	a
proti	proti	k7c3	proti
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc1	ten
nejméně	málo	k6eAd3	málo
jedna	jeden	k4xCgFnSc1	jeden
pětina	pětina	k1gFnSc1	pětina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
přeje	přát	k5eAaImIp3nS	přát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
6	[number]	k4	6
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
právy	právo	k1gNnPc7	právo
členů	člen	k1gMnPc2	člen
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
službu	služba	k1gFnSc4	služba
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
náhrady	náhrada	k1gFnPc4	náhrada
placené	placený	k2eAgFnPc4d1	placená
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
přítomnosti	přítomnost	k1gFnSc2	přítomnost
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
i	i	k9	i
během	během	k7c2	během
příjezdu	příjezd	k1gInSc2	příjezd
a	a	k8xC	a
návratu	návrat	k1gInSc2	návrat
požívají	požívat	k5eAaImIp3nP	požívat
poslanecké	poslanecký	k2eAgFnPc1d1	Poslanecká
imunity	imunita	k1gFnPc1	imunita
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
případu	případ	k1gInSc2	případ
velezrady	velezrada	k1gFnSc2	velezrada
<g/>
,	,	kIx,	,
těžkého	těžký	k2eAgInSc2d1	těžký
zločinu	zločin	k1gInSc2	zločin
a	a	k8xC	a
porušení	porušení	k1gNnSc2	porušení
veřejného	veřejný	k2eAgInSc2d1	veřejný
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgInPc4	jenž
byli	být	k5eAaImAgMnP	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
zastávat	zastávat	k5eAaImF	zastávat
žádný	žádný	k3yNgInSc4	žádný
státní	státní	k2eAgInSc4d1	státní
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
7	[number]	k4	7
určuje	určovat	k5eAaImIp3nS	určovat
způsob	způsob	k1gInSc1	způsob
schvalování	schvalování	k1gNnSc2	schvalování
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
zákonů	zákon	k1gInPc2	zákon
na	na	k7c6	na
zvýšení	zvýšení	k1gNnSc6	zvýšení
příjmů	příjem	k1gInPc2	příjem
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výhradní	výhradní	k2eAgFnSc6d1	výhradní
pravomoci	pravomoc	k1gFnSc6	pravomoc
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Senát	senát	k1gInSc1	senát
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
navrhnout	navrhnout	k5eAaPmF	navrhnout
jejich	jejich	k3xOp3gNnSc4	jejich
doplnění	doplnění	k1gNnSc4	doplnění
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovnou	sněmovna	k1gFnSc7	sněmovna
a	a	k8xC	a
Senátem	senát	k1gInSc7	senát
schválený	schválený	k2eAgInSc1d1	schválený
návrh	návrh	k1gInSc1	návrh
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
zákonem	zákon	k1gInSc7	zákon
až	až	k9	až
po	po	k7c6	po
podpisu	podpis	k1gInSc6	podpis
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
ho	on	k3xPp3gInSc4	on
s	s	k7c7	s
námitkami	námitka	k1gFnPc7	námitka
vrátit	vrátit	k5eAaPmF	vrátit
komoře	komora	k1gFnSc3	komora
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vzešel	vzejít	k5eAaPmAgInS	vzejít
<g/>
.	.	kIx.	.
</s>
<s>
Veto	veto	k1gNnSc1	veto
prezidenta	prezident	k1gMnSc2	prezident
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přehlasováno	přehlasován	k2eAgNnSc1d1	přehlasováno
dvěma	dva	k4xCgFnPc7	dva
třetinami	třetina	k1gFnPc7	třetina
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Nevrátí	vrátit	k5eNaPmIp3nS	vrátit
<g/>
-li	i	k?	-li
prezident	prezident	k1gMnSc1	prezident
návrh	návrh	k1gInSc4	návrh
do	do	k7c2	do
10	[number]	k4	10
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
nepočítaje	nepočítaje	k7c4	nepočítaje
neděle	neděle	k1gFnPc4	neděle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
i	i	k9	i
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gInSc2	jeho
podpisu	podpis	k1gInSc2	podpis
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Kongres	kongres	k1gInSc1	kongres
vrácení	vrácení	k1gNnSc2	vrácení
návrhu	návrh	k1gInSc2	návrh
znemožní	znemožnit	k5eAaPmIp3nS	znemožnit
odročením	odročení	k1gNnSc7	odročení
svého	svůj	k3xOyFgNnSc2	svůj
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pravomoci	pravomoc	k1gFnSc3	pravomoc
Kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
omezení	omezení	k1gNnSc2	omezení
====	====	k?	====
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
8	[number]	k4	8
článku	článek	k1gInSc2	článek
I	i	k9	i
vyjmenovává	vyjmenovávat	k5eAaImIp3nS	vyjmenovávat
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
náleží	náležet	k5eAaImIp3nP	náležet
výlučně	výlučně	k6eAd1	výlučně
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
uvedeny	uveden	k2eAgFnPc1d1	uvedena
pravomoci	pravomoc	k1gFnPc1	pravomoc
vybírat	vybírat	k5eAaImF	vybírat
cla	clo	k1gNnPc4	clo
<g/>
,	,	kIx,	,
dávky	dávka	k1gFnPc4	dávka
a	a	k8xC	a
poplatky	poplatek	k1gInPc4	poplatek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
půjčovat	půjčovat	k5eAaImF	půjčovat
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
účet	účet	k1gInSc4	účet
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
řídit	řídit	k5eAaImF	řídit
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
cizími	cizí	k2eAgInPc7d1	cizí
národy	národ	k1gInPc7	národ
či	či	k8xC	či
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
vydávat	vydávat	k5eAaImF	vydávat
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
zřizovat	zřizovat	k5eAaImF	zřizovat
soudy	soud	k1gInPc4	soud
<g />
.	.	kIx.	.
</s>
<s>
podřízené	podřízená	k1gFnSc2	podřízená
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
udílet	udílet	k5eAaImF	udílet
povolení	povolení	k1gNnPc4	povolení
k	k	k7c3	k
zajímání	zajímání	k1gNnSc3	zajímání
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
odvetným	odvetný	k2eAgNnSc7d1	odvetné
opatřením	opatření	k1gNnSc7	opatření
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
povolávat	povolávat	k5eAaImF	povolávat
a	a	k8xC	a
vydržovat	vydržovat	k5eAaImF	vydržovat
vojska	vojsko	k1gNnPc4	vojsko
<g/>
,	,	kIx,	,
zřizovat	zřizovat	k5eAaImF	zřizovat
a	a	k8xC	a
udržoval	udržovat	k5eAaImAgMnS	udržovat
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
,	,	kIx,	,
svolávat	svolávat	k5eAaImF	svolávat
domobranu	domobrana	k1gFnSc4	domobrana
<g/>
,	,	kIx,	,
spravovat	spravovat	k5eAaImF	spravovat
obvod	obvod	k1gInSc4	obvod
nepřesahující	přesahující	k2eNgFnSc2d1	nepřesahující
10	[number]	k4	10
čtverečních	čtvereční	k2eAgFnPc2d1	čtvereční
mil	míle	k1gFnPc2	míle
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
sídlem	sídlo	k1gNnSc7	sídlo
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgNnSc2	tento
ustanovení	ustanovení	k1gNnSc2	ustanovení
byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
District	District	k1gInSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Washington	Washington	k1gInSc1	Washington
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
bod	bod	k1gInSc4	bod
tohoto	tento	k3xDgInSc2	tento
seznamu	seznam	k1gInSc2	seznam
pak	pak	k6eAd1	pak
dává	dávat	k5eAaImIp3nS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
výčet	výčet	k1gInSc1	výčet
není	být	k5eNaImIp3nS	být
úplný	úplný	k2eAgInSc1d1	úplný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kongres	kongres	k1gInSc1	kongres
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
právo	právo	k1gNnSc4	právo
vydávat	vydávat	k5eAaPmF	vydávat
všechny	všechen	k3xTgInPc1	všechen
zákony	zákon	k1gInPc1	zákon
nutné	nutný	k2eAgInPc1d1	nutný
k	k	k7c3	k
uskutečňování	uskutečňování	k1gNnSc3	uskutečňování
"	"	kIx"	"
<g/>
shora	shora	k6eAd1	shora
uvedených	uvedený	k2eAgFnPc2d1	uvedená
pravomocí	pravomoc	k1gFnPc2	pravomoc
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
tato	tento	k3xDgFnSc1	tento
Ústava	ústava	k1gFnSc1	ústava
propůjčila	propůjčit	k5eAaPmAgFnS	propůjčit
vládě	vláda	k1gFnSc3	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
9	[number]	k4	9
uvádí	uvádět	k5eAaImIp3nS	uvádět
seznam	seznam	k1gInSc4	seznam
konkrétních	konkrétní	k2eAgNnPc2d1	konkrétní
omezení	omezení	k1gNnPc2	omezení
moci	moc	k1gFnSc2	moc
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
nesmí	smět	k5eNaImIp3nS	smět
zakázat	zakázat	k5eAaPmF	zakázat
přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
nebo	nebo	k8xC	nebo
dovoz	dovoz	k1gInSc1	dovoz
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
uzná	uznat	k5eAaPmIp3nS	uznat
za	za	k7c4	za
nutný	nutný	k2eAgInSc4d1	nutný
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
může	moct	k5eAaImIp3nS	moct
na	na	k7c4	na
takový	takový	k3xDgInSc4	takový
dovoz	dovoz	k1gInSc4	dovoz
uvalit	uvalit	k5eAaPmF	uvalit
clo	clo	k1gNnSc4	clo
nebo	nebo	k8xC	nebo
daň	daň	k1gFnSc4	daň
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
10	[number]	k4	10
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
osobu	osoba	k1gFnSc4	osoba
(	(	kIx(	(
<g/>
tímto	tento	k3xDgInSc7	tento
dovozem	dovoz	k1gInSc7	dovoz
byl	být	k5eAaImAgInS	být
myšlen	myslet	k5eAaImNgInS	myslet
především	především	k9	především
dovoz	dovoz	k1gInSc1	dovoz
otroků	otrok	k1gMnPc2	otrok
<g/>
;	;	kIx,	;
ustanovení	ustanovení	k1gNnSc1	ustanovení
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
platnost	platnost	k1gFnSc4	platnost
přijetím	přijetí	k1gNnSc7	přijetí
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
povstání	povstání	k1gNnSc2	povstání
nebo	nebo	k8xC	nebo
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
vpádu	vpád	k1gInSc2	vpád
nesmí	smět	k5eNaImIp3nS	smět
porušit	porušit	k5eAaPmF	porušit
výsady	výsada	k1gFnPc4	výsada
zaručené	zaručený	k2eAgFnPc4d1	zaručená
výnosem	výnos	k1gInSc7	výnos
habeas	habeas	k1gInSc4	habeas
corpus	corpus	k1gInSc2	corpus
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
nesmí	smět	k5eNaImIp3nS	smět
vydat	vydat	k5eAaPmF	vydat
žádný	žádný	k3yNgInSc4	žádný
zákon	zákon	k1gInSc4	zákon
se	s	k7c7	s
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
působností	působnost	k1gFnSc7	působnost
či	či	k8xC	či
zákon	zákon	k1gInSc4	zákon
obsahující	obsahující	k2eAgInSc4d1	obsahující
soudní	soudní	k2eAgInSc4d1	soudní
rozsudek	rozsudek	k1gInSc4	rozsudek
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
vypisovat	vypisovat	k5eAaImF	vypisovat
žádnou	žádný	k3yNgFnSc4	žádný
daň	daň	k1gFnSc4	daň
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
či	či	k8xC	či
jinou	jiný	k2eAgFnSc4d1	jiná
přímou	přímý	k2eAgFnSc4d1	přímá
daň	daň	k1gFnSc4	daň
<g/>
,	,	kIx,	,
leda	leda	k8xS	leda
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
soupisu	soupis	k1gInSc3	soupis
nebo	nebo	k8xC	nebo
sčítání	sčítání	k1gNnSc4	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pokyny	pokyn	k1gInPc7	pokyn
oddílu	oddíl	k1gInSc2	oddíl
2	[number]	k4	2
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
dovoleno	dovolen	k2eAgNnSc1d1	dovoleno
uvalovat	uvalovat	k5eAaImF	uvalovat
daň	daň	k1gFnSc4	daň
či	či	k8xC	či
clo	clo	k1gNnSc4	clo
na	na	k7c4	na
výrobky	výrobek	k1gInPc4	výrobek
vyvezené	vyvezený	k2eAgNnSc1d1	vyvezené
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
státu	stát	k1gInSc2	stát
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Přístavy	přístav	k1gInPc1	přístav
žádného	žádný	k1gMnSc2	žádný
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
zvýhodňovány	zvýhodňován	k2eAgFnPc4d1	zvýhodňována
a	a	k8xC	a
lodě	loď	k1gFnPc4	loď
navštěvující	navštěvující	k2eAgInSc1d1	navštěvující
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
státu	stát	k1gInSc6	stát
nesmí	smět	k5eNaImIp3nP	smět
být	být	k5eAaImF	být
nuceny	nucen	k2eAgFnPc1d1	nucena
přistát	přistát	k5eAaImF	přistát
i	i	k9	i
v	v	k7c6	v
přístavech	přístav	k1gInPc6	přístav
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
pokladna	pokladna	k1gFnSc1	pokladna
může	moct	k5eAaImIp3nS	moct
vydávat	vydávat	k5eAaImF	vydávat
peníze	peníz	k1gInPc4	peníz
jen	jen	k9	jen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
povolení	povolení	k1gNnSc2	povolení
stanoveného	stanovený	k2eAgInSc2d1	stanovený
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
ustanovení	ustanovení	k1gNnSc1	ustanovení
podtrhuje	podtrhovat	k5eAaImIp3nS	podtrhovat
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
povahu	povaha	k1gFnSc4	povaha
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukládá	ukládat	k5eAaImIp3nS	ukládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
nesmí	smět	k5eNaImIp3nP	smět
propůjčovat	propůjčovat	k5eAaImF	propůjčovat
šlechtické	šlechtický	k2eAgInPc4d1	šlechtický
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
Kongresu	kongres	k1gInSc2	kongres
nesmí	smět	k5eNaImIp3nS	smět
žádná	žádný	k3yNgFnSc1	žádný
osoba	osoba	k1gFnSc1	osoba
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
službách	služba	k1gFnPc6	služba
přijmout	přijmout	k5eAaPmF	přijmout
dar	dar	k1gInSc4	dar
<g/>
,	,	kIx,	,
odměnu	odměna	k1gFnSc4	odměna
<g/>
,	,	kIx,	,
úřad	úřad	k1gInSc4	úřad
či	či	k8xC	či
titul	titul	k1gInSc4	titul
od	od	k7c2	od
žádného	žádný	k3yNgMnSc2	žádný
cizího	cizí	k2eAgMnSc2d1	cizí
panovníka	panovník	k1gMnSc2	panovník
nebo	nebo	k8xC	nebo
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
10	[number]	k4	10
omezuje	omezovat	k5eAaImIp3nS	omezovat
práva	právo	k1gNnSc2	právo
států	stát	k1gInPc2	stát
vůči	vůči	k7c3	vůči
Kongresu	kongres	k1gInSc3	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgNnPc2	tento
ustanovení	ustanovení	k1gNnPc2	ustanovení
nesmí	smět	k5eNaImIp3nS	smět
žádný	žádný	k3yNgInSc4	žádný
stát	stát	k1gInSc4	stát
uzavírat	uzavírat	k5eAaImF	uzavírat
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
povolovat	povolovat	k5eAaImF	povolovat
zajímání	zajímání	k1gNnSc4	zajímání
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
vydávat	vydávat	k5eAaPmF	vydávat
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
uznávat	uznávat	k5eAaImF	uznávat
pro	pro	k7c4	pro
placení	placení	k1gNnSc4	placení
dluhů	dluh	k1gInPc2	dluh
cokoliv	cokoliv	k3yInSc4	cokoliv
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
zlato	zlato	k1gNnSc4	zlato
či	či	k8xC	či
stříbro	stříbro	k1gNnSc4	stříbro
(	(	kIx(	(
<g/>
další	další	k2eAgNnSc4d1	další
ustanovení	ustanovení	k1gNnSc4	ustanovení
bránící	bránící	k2eAgNnSc4d1	bránící
státům	stát	k1gInPc3	stát
v	v	k7c6	v
zavedení	zavedení	k1gNnSc6	zavedení
čehokoliv	cokoliv	k3yInSc2	cokoliv
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
připomínalo	připomínat	k5eAaImAgNnS	připomínat
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
měnu	měna	k1gFnSc4	měna
a	a	k8xC	a
nutící	nutící	k2eAgInSc1d1	nutící
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
užívat	užívat	k5eAaImF	užívat
měnu	měna	k1gFnSc4	měna
uznanou	uznaný	k2eAgFnSc4d1	uznaná
Kongresem	kongres	k1gInSc7	kongres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
zákonodárné	zákonodárný	k2eAgInPc1d1	zákonodárný
orgány	orgán	k1gInPc1	orgán
států	stát	k1gInPc2	stát
nemohou	moct	k5eNaImIp3nP	moct
přijímat	přijímat	k5eAaImF	přijímat
zákony	zákon	k1gInPc4	zákon
se	s	k7c7	s
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
platností	platnost	k1gFnSc7	platnost
nebo	nebo	k8xC	nebo
obsahující	obsahující	k2eAgInSc4d1	obsahující
rozsudek	rozsudek	k1gInSc4	rozsudek
<g/>
,	,	kIx,	,
či	či	k8xC	či
udělovat	udělovat	k5eAaImF	udělovat
šlechtické	šlechtický	k2eAgInPc4d1	šlechtický
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Dovozní	dovozní	k2eAgNnPc1d1	dovozní
a	a	k8xC	a
vývozní	vývozní	k2eAgNnPc1d1	vývozní
cla	clo	k1gNnPc1	clo
mohou	moct	k5eAaImIp3nP	moct
státy	stát	k1gInPc4	stát
zavádět	zavádět	k5eAaImF	zavádět
jen	jen	k9	jen
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
výnos	výnos	k1gInSc1	výnos
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
odveden	odvést	k5eAaPmNgInS	odvést
do	do	k7c2	do
federálního	federální	k2eAgInSc2d1	federální
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Souhlas	souhlas	k1gInSc4	souhlas
Kongresu	kongres	k1gInSc2	kongres
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
státy	stát	k1gInPc1	stát
i	i	k9	i
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
cla	clo	k1gNnSc2	clo
z	z	k7c2	z
tonáže	tonáž	k1gFnSc2	tonáž
<g/>
,	,	kIx,	,
zřízení	zřízení	k1gNnSc4	zřízení
vojska	vojsko	k1gNnSc2	vojsko
či	či	k8xC	či
válečného	válečný	k2eAgNnSc2d1	válečné
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
uzavírání	uzavírání	k1gNnSc2	uzavírání
smluv	smlouva	k1gFnPc2	smlouva
a	a	k8xC	a
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
daný	daný	k2eAgInSc1d1	daný
stát	stát	k1gInSc1	stát
již	již	k9	již
byl	být	k5eAaImAgInS	být
napaden	napadnout	k5eAaPmNgMnS	napadnout
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
neudržitelně	udržitelně	k6eNd1	udržitelně
nebezpečné	bezpečný	k2eNgFnSc6d1	nebezpečná
situaci	situace	k1gFnSc6	situace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Interpretace	interpretace	k1gFnSc1	interpretace
dvou	dva	k4xCgInPc2	dva
ustanovení	ustanovení	k1gNnSc4	ustanovení
oddílu	oddíl	k1gInSc2	oddíl
8	[number]	k4	8
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
vleklých	vleklý	k2eAgInPc2d1	vleklý
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
pravomoci	pravomoc	k1gFnSc2	pravomoc
Kongresu	kongres	k1gInSc2	kongres
"	"	kIx"	"
<g/>
řídit	řídit	k5eAaImF	řídit
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
cizími	cizí	k2eAgInPc7d1	cizí
národy	národ	k1gInPc7	národ
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
státy	stát	k1gInPc7	stát
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
indiánskými	indiánský	k2eAgInPc7d1	indiánský
kmeny	kmen	k1gInPc7	kmen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
zase	zase	k9	zase
uvádělo	uvádět	k5eAaImAgNnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
v	v	k7c6	v
pravomoci	pravomoc	k1gFnSc6	pravomoc
Kongresu	kongres	k1gInSc2	kongres
je	být	k5eAaImIp3nS	být
...	...	k?	...
vydávat	vydávat	k5eAaImF	vydávat
všechny	všechen	k3xTgInPc1	všechen
zákony	zákon	k1gInPc1	zákon
nutné	nutný	k2eAgInPc1d1	nutný
k	k	k7c3	k
uskutečňování	uskutečňování	k1gNnSc3	uskutečňování
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
pravomocí	pravomoc	k1gFnPc2	pravomoc
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
,	,	kIx,	,
propůjčených	propůjčený	k2eAgFnPc2d1	propůjčená
touto	tento	k3xDgFnSc7	tento
ústavou	ústava	k1gFnSc7	ústava
vládě	vláda	k1gFnSc3	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gNnPc3	její
ministerstvům	ministerstvo	k1gNnPc3	ministerstvo
nebo	nebo	k8xC	nebo
úředníkům	úředník	k1gMnPc3	úředník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgNnPc1	tento
ustanovení	ustanovení	k1gNnPc1	ustanovení
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
Kongresu	kongres	k1gInSc2	kongres
přijímat	přijímat	k5eAaImF	přijímat
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
přímo	přímo	k6eAd1	přímo
výslovně	výslovně	k6eAd1	výslovně
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
jeho	jeho	k3xOp3gFnPc2	jeho
pravomocí	pravomoc	k1gFnPc2	pravomoc
a	a	k8xC	a
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
současně	současně	k6eAd1	současně
nejsou	být	k5eNaImIp3nP	být
výslovně	výslovně	k6eAd1	výslovně
upřeny	upřen	k2eAgFnPc1d1	upřena
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
omezení	omezení	k1gNnSc2	omezení
v	v	k7c6	v
oddílu	oddíl	k1gInSc6	oddíl
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soudním	soudní	k2eAgInSc6d1	soudní
sporu	spor	k1gInSc6	spor
McCulloch	McCulloch	k1gInSc1	McCulloch
v.	v.	k?	v.
Maryland	Maryland	k1gInSc1	Maryland
roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
právě	právě	k9	právě
tu	ten	k3xDgFnSc4	ten
část	část	k1gFnSc4	část
ustanovení	ustanovení	k1gNnSc2	ustanovení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
"	"	kIx"	"
<g/>
uskutečňování	uskutečňování	k1gNnSc4	uskutečňování
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
pravomocí	pravomoc	k1gFnPc2	pravomoc
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
pravomoci	pravomoc	k1gFnPc4	pravomoc
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
nepřímo	přímo	k6eNd1	přímo
zahrnuté	zahrnutý	k2eAgFnPc4d1	zahrnutá
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
implied	implied	k1gInSc1	implied
powers	powers	k1gInSc1	powers
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kongres	kongres	k1gInSc1	kongres
jich	on	k3xPp3gFnPc2	on
využil	využít	k5eAaPmAgMnS	využít
například	například	k6eAd1	například
při	při	k7c6	při
schvalování	schvalování	k1gNnSc6	schvalování
zákonů	zákon	k1gInPc2	zákon
týkajících	týkající	k2eAgInPc6d1	týkající
se	se	k3xPyFc4	se
regulace	regulace	k1gFnSc2	regulace
mezd	mzda	k1gFnPc2	mzda
a	a	k8xC	a
pracovní	pracovní	k2eAgFnSc2d1	pracovní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
radiových	radiový	k2eAgFnPc2d1	radiová
<g/>
,	,	kIx,	,
telefonních	telefonní	k2eAgFnPc2d1	telefonní
či	či	k8xC	či
televizních	televizní	k2eAgFnPc2d1	televizní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
,	,	kIx,	,
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
či	či	k8xC	či
založení	založení	k1gNnSc2	založení
národních	národní	k2eAgFnPc2d1	národní
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Článek	článek	k1gInSc1	článek
II	II	kA	II
<g/>
:	:	kIx,	:
Moc	moc	k6eAd1	moc
výkonná	výkonný	k2eAgFnSc1d1	výkonná
===	===	k?	===
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
II	II	kA	II
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgFnSc1d1	sestávající
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
složku	složka	k1gFnSc4	složka
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
====	====	k?	====
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
článku	článek	k1gInSc2	článek
II	II	kA	II
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
přisouzena	přisoudit	k5eAaPmNgFnS	přisoudit
přímo	přímo	k6eAd1	přímo
prezidentovi	prezident	k1gMnSc3	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
je	být	k5eAaImIp3nS	být
totožné	totožný	k2eAgNnSc1d1	totožné
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
tento	tento	k3xDgInSc1	tento
oddíl	oddíl	k1gInSc1	oddíl
stanovoval	stanovovat	k5eAaImAgInS	stanovovat
i	i	k9	i
způsob	způsob	k1gInSc1	způsob
volby	volba	k1gFnSc2	volba
do	do	k7c2	do
obou	dva	k4xCgInPc2	dva
těchto	tento	k3xDgInPc2	tento
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
způsobem	způsob	k1gInSc7	způsob
novým	nový	k2eAgInSc7d1	nový
<g/>
,	,	kIx,	,
popsaným	popsaný	k2eAgNnSc7d1	popsané
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
se	se	k3xPyFc4	se
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
následujících	následující	k2eAgFnPc6d1	následující
otázkách	otázka	k1gFnPc6	otázka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Způsobilost	způsobilost	k1gFnSc1	způsobilost
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
občanem	občan	k1gMnSc7	občan
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
alespoň	alespoň	k9	alespoň
35	[number]	k4	35
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
a	a	k8xC	a
bydlet	bydlet	k5eAaImF	bydlet
nejméně	málo	k6eAd3	málo
14	[number]	k4	14
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nevyužitelnou	využitelný	k2eNgFnSc4d1	nevyužitelná
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
uchazeč	uchazeč	k1gMnSc1	uchazeč
o	o	k7c4	o
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
úřad	úřad	k1gInSc4	úřad
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k8xC	jako
občan	občan	k1gMnSc1	občan
jiného	jiný	k2eAgInSc2d1	jiný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
postačí	postačit	k5eAaPmIp3nS	postačit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
<g/>
-li	i	k?	-li
tímto	tento	k3xDgMnSc7	tento
občanem	občan	k1gMnSc7	občan
v	v	k7c6	v
době	doba	k1gFnSc6	doba
přijetí	přijetí	k1gNnSc2	přijetí
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
mělo	mít	k5eAaImAgNnS	mít
umožnit	umožnit	k5eAaPmF	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
o	o	k7c4	o
úřad	úřad	k1gInSc4	úřad
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
desetiletích	desetiletí	k1gNnPc6	desetiletí
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
Ústavy	ústava	k1gFnSc2	ústava
ucházet	ucházet	k5eAaImF	ucházet
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
jako	jako	k9	jako
občané	občan	k1gMnPc1	občan
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
ještě	ještě	k9	ještě
před	před	k7c7	před
založením	založení	k1gNnSc7	založení
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
první	první	k4xOgMnPc1	první
uchazeči	uchazeč	k1gMnPc1	uchazeč
stali	stát	k5eAaPmAgMnP	stát
způsobilými	způsobilý	k2eAgFnPc7d1	způsobilá
až	až	k9	až
35	[number]	k4	35
let	léto	k1gNnPc2	léto
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
této	tento	k3xDgFnSc2	tento
nové	nový	k2eAgFnSc2d1	nová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
Nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
<g/>
.	.	kIx.	.
</s>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
také	také	k9	také
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
zbaven	zbavit	k5eAaPmNgInS	zbavit
úřadu	úřad	k1gInSc3	úřad
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
výkonu	výkon	k1gInSc2	výkon
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
povinností	povinnost	k1gFnPc2	povinnost
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
nebo	nebo	k8xC	nebo
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
rezignuje	rezignovat	k5eAaBmIp3nS	rezignovat
<g/>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
úřad	úřad	k1gInSc1	úřad
na	na	k7c4	na
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
textu	text	k1gInSc2	text
nebylo	být	k5eNaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
viceprezident	viceprezident	k1gMnSc1	viceprezident
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
bude	být	k5eAaImBp3nS	být
prezidenta	prezident	k1gMnSc4	prezident
pouze	pouze	k6eAd1	pouze
zastupovat	zastupovat	k5eAaImF	zastupovat
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
pouze	pouze	k6eAd1	pouze
převezme	převzít	k5eAaPmIp3nS	převzít
jeho	jeho	k3xOp3gFnPc4	jeho
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
prezidentem	prezident	k1gMnSc7	prezident
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Williama	William	k1gMnSc2	William
Henryho	Henry	k1gMnSc2	Henry
Harrisona	Harrison	k1gMnSc2	Harrison
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
nahradil	nahradit	k5eAaPmAgMnS	nahradit
John	John	k1gMnSc1	John
Tyler	Tyler	k1gMnSc1	Tyler
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
precedens	precedens	k1gNnSc7	precedens
plnohodnotného	plnohodnotný	k2eAgNnSc2d1	plnohodnotné
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
<g/>
,	,	kIx,	,
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
byla	být	k5eAaImAgFnS	být
naplňována	naplňovat	k5eAaImNgFnS	naplňovat
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
případech	případ	k1gInPc6	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
prezidentů	prezident	k1gMnPc2	prezident
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
tuto	tento	k3xDgFnSc4	tento
záležitost	záležitost	k1gFnSc4	záležitost
řeší	řešit	k5eAaImIp3nS	řešit
25	[number]	k4	25
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
viceprezident	viceprezident	k1gMnSc1	viceprezident
v	v	k7c6	v
případě	případ	k1gInSc6	případ
smrti	smrt	k1gFnSc2	smrt
nebo	nebo	k8xC	nebo
neschopnosti	neschopnost	k1gFnSc2	neschopnost
prezidenta	prezident	k1gMnSc2	prezident
sám	sám	k3xTgMnSc1	sám
stává	stávat	k5eAaImIp3nS	stávat
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
<g/>
Plat	plat	k1gInSc1	plat
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
dostává	dostávat	k5eAaImIp3nS	dostávat
za	za	k7c4	za
výkon	výkon	k1gInSc4	výkon
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
"	"	kIx"	"
<g/>
náhradu	náhrada	k1gFnSc4	náhrada
<g/>
"	"	kIx"	"
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
náhrada	náhrada	k1gFnSc1	náhrada
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jeho	on	k3xPp3gNnSc2	on
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
ani	ani	k8xC	ani
snížena	snížit	k5eAaPmNgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
nemůže	moct	k5eNaImIp3nS	moct
dostávat	dostávat	k5eAaImF	dostávat
od	od	k7c2	od
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
ani	ani	k8xC	ani
od	od	k7c2	od
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
jiné	jiný	k2eAgFnSc2d1	jiná
náhrady	náhrada	k1gFnSc2	náhrada
<g/>
.	.	kIx.	.
<g/>
Přísaha	přísaha	k1gFnSc1	přísaha
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
ustanovení	ustanovení	k1gNnSc1	ustanovení
oddílu	oddíl	k1gInSc2	oddíl
1	[number]	k4	1
uvádí	uvádět	k5eAaImIp3nS	uvádět
text	text	k1gInSc4	text
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
přísahy	přísaha	k1gFnSc2	přísaha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
zachovávat	zachovávat	k5eAaImF	zachovávat
<g/>
,	,	kIx,	,
střežit	střežit	k5eAaImF	střežit
a	a	k8xC	a
chránit	chránit	k5eAaImF	chránit
Ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pravomoci	pravomoc	k1gFnSc2	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
zbavení	zbavení	k1gNnSc1	zbavení
úřadu	úřad	k1gInSc2	úřad
====	====	k?	====
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
článku	článek	k1gInSc2	článek
II	II	kA	II
garantuje	garantovat	k5eAaBmIp3nS	garantovat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
omezuje	omezovat	k5eAaImIp3nS	omezovat
prezidentovi	prezident	k1gMnSc3	prezident
jeho	jeho	k3xOp3gFnSc2	jeho
pravomoci	pravomoc	k1gFnSc2	pravomoc
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
amerických	americký	k2eAgFnPc2d1	americká
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
také	také	k9	také
domobrany	domobrana	k1gFnPc4	domobrana
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
svolána	svolat	k5eAaPmNgFnS	svolat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
vyžádat	vyžádat	k5eAaPmF	vyžádat
názory	názor	k1gInPc4	názor
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
úředníků	úředník	k1gMnPc2	úředník
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
udělovat	udělovat	k5eAaImF	udělovat
milost	milost	k1gFnSc4	milost
či	či	k8xC	či
amnestii	amnestie	k1gFnSc4	amnestie
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
případů	případ	k1gInPc2	případ
velezrady	velezrada	k1gFnSc2	velezrada
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
současně	současně	k6eAd1	současně
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
omilostnit	omilostnit	k5eAaPmF	omilostnit
ani	ani	k8xC	ani
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
vyšetřován	vyšetřovat	k5eAaImNgInS	vyšetřovat
Kongresem	kongres	k1gInSc7	kongres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
může	moct	k5eAaImIp3nS	moct
uzavírat	uzavírat	k5eAaImF	uzavírat
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
a	a	k8xC	a
se	se	k3xPyFc4	se
souhlasem	souhlas	k1gInSc7	souhlas
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Souhlas	souhlas	k1gInSc4	souhlas
musí	muset	k5eAaImIp3nP	muset
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
přítomných	přítomný	k2eAgMnPc2d1	přítomný
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
a	a	k8xC	a
se	se	k3xPyFc4	se
souhlasem	souhlas	k1gInSc7	souhlas
Senátu	senát	k1gInSc2	senát
prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgMnPc4d1	jiný
zástupce	zástupce	k1gMnPc4	zástupce
státu	stát	k1gInSc2	stát
a	a	k8xC	a
konzuly	konzul	k1gMnPc4	konzul
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc1	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
všechny	všechen	k3xTgMnPc4	všechen
další	další	k2eAgMnPc4d1	další
úředníky	úředník	k1gMnPc4	úředník
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
úřad	úřad	k1gInSc1	úřad
je	být	k5eAaImIp3nS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
jejich	jejich	k3xOp3gNnSc4	jejich
jmenování	jmenování	k1gNnSc4	jmenování
upraveno	upravit	k5eAaPmNgNnS	upravit
Ústavou	ústava	k1gFnSc7	ústava
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kongres	kongres	k1gInSc1	kongres
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
pověřit	pověřit	k5eAaPmF	pověřit
jmenováním	jmenování	k1gNnSc7	jmenování
nižších	nízký	k2eAgMnPc2d2	nižší
úředníků	úředník	k1gMnPc2	úředník
samotného	samotný	k2eAgMnSc2d1	samotný
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
soudy	soud	k1gInPc4	soud
nebo	nebo	k8xC	nebo
představitele	představitel	k1gMnPc4	představitel
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Kongres	kongres	k1gInSc1	kongres
nezasedá	zasedat	k5eNaImIp3nS	zasedat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
prezident	prezident	k1gMnSc1	prezident
úředníky	úředník	k1gMnPc4	úředník
jmenovat	jmenovat	k5eAaImF	jmenovat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
jmenování	jmenování	k1gNnPc1	jmenování
však	však	k9	však
na	na	k7c6	na
konci	konec	k1gInSc6	konec
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
zasedání	zasedání	k1gNnSc2	zasedání
Kongresu	kongres	k1gInSc2	kongres
vyprší	vypršet	k5eAaPmIp3nS	vypršet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
proto	proto	k8xC	proto
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
prezident	prezident	k1gMnSc1	prezident
žádá	žádat	k5eAaImIp3nS	žádat
Kongres	kongres	k1gInSc4	kongres
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
potvrzení	potvrzení	k1gNnSc4	potvrzení
<g/>
.	.	kIx.	.
<g/>
Oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
další	další	k2eAgNnSc1d1	další
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
významné	významný	k2eAgFnPc4d1	významná
pravomoci	pravomoc	k1gFnPc4	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
podává	podávat	k5eAaImIp3nS	podávat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
může	moct	k5eAaImIp3nS	moct
svolat	svolat	k5eAaPmF	svolat
zasedání	zasedání	k1gNnSc2	zasedání
kterékoliv	kterýkoliv	k3yIgFnSc2	kterýkoliv
komory	komora	k1gFnSc2	komora
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
obou	dva	k4xCgFnPc2	dva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
komory	komora	k1gFnPc1	komora
Kongresu	kongres	k1gInSc2	kongres
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
shodnout	shodnout	k5eAaBmF	shodnout
na	na	k7c6	na
délce	délka	k1gFnSc6	délka
doby	doba	k1gFnSc2	doba
odročení	odročení	k1gNnSc2	odročení
svého	svůj	k3xOyFgNnSc2	svůj
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
,	,	kIx,	,
odročí	odročit	k5eAaPmIp3nP	odročit
jej	on	k3xPp3gMnSc4	on
prezident	prezident	k1gMnSc1	prezident
dle	dle	k7c2	dle
svého	svůj	k3xOyFgNnSc2	svůj
uvážení	uvážení	k1gNnSc2	uvážení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
přijímá	přijímat	k5eAaImIp3nS	přijímat
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
zástupce	zástupce	k1gMnPc4	zástupce
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
zákony	zákon	k1gInPc1	zákon
svědomitě	svědomitě	k6eAd1	svědomitě
naplňovány	naplňován	k2eAgInPc1d1	naplňován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
všechny	všechen	k3xTgMnPc4	všechen
úředníky	úředník	k1gMnPc4	úředník
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
Oddíl	oddíl	k1gInSc1	oddíl
4	[number]	k4	4
vyjmenovává	vyjmenovávat	k5eAaImIp3nS	vyjmenovávat
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
federální	federální	k2eAgMnPc1d1	federální
úředníci	úředník	k1gMnPc1	úředník
zproštěni	zproštěn	k2eAgMnPc1d1	zproštěn
svých	svůj	k3xOyFgMnPc2	svůj
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
usvědčení	usvědčení	k1gNnSc4	usvědčení
nebo	nebo	k8xC	nebo
obžalobu	obžaloba	k1gFnSc4	obžaloba
z	z	k7c2	z
velezrady	velezrada	k1gFnSc2	velezrada
<g/>
,	,	kIx,	,
úplatkářství	úplatkářství	k1gNnSc2	úplatkářství
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
těžkých	těžký	k2eAgInPc2d1	těžký
zločinů	zločin	k1gInPc2	zločin
a	a	k8xC	a
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Článek	článek	k1gInSc1	článek
III	III	kA	III
<g/>
:	:	kIx,	:
Moc	moc	k6eAd1	moc
soudní	soudní	k2eAgInSc1d1	soudní
===	===	k?	===
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
III	III	kA	III
Ústavy	ústav	k1gInPc1	ústav
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgInPc1d1	sestávající
ze	z	k7c2	z
třech	tři	k4xCgInPc2	tři
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
oddíly	oddíl	k1gInPc4	oddíl
uvádí	uvádět	k5eAaImIp3nS	uvádět
zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
soud	soud	k1gInSc1	soud
může	moct	k5eAaImIp3nS	moct
nést	nést	k5eAaImF	nést
název	název	k1gInSc1	název
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
a	a	k8xC	a
že	že	k8xS	že
nižší	nízký	k2eAgInPc4d2	nižší
soudy	soud	k1gInPc4	soud
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
vyjmenovány	vyjmenovat	k5eAaPmNgInP	vyjmenovat
typy	typ	k1gInPc1	typ
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
spadají	spadat	k5eAaImIp3nP	spadat
pod	pod	k7c4	pod
federální	federální	k2eAgFnSc4d1	federální
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
instancí	instance	k1gFnSc7	instance
v	v	k7c6	v
případech	případ	k1gInPc6	případ
soudních	soudní	k2eAgInPc6d1	soudní
řízení	řízení	k1gNnSc2	řízení
týkajících	týkající	k2eAgMnPc2d1	týkající
zástupců	zástupce	k1gMnPc2	zástupce
Unie	unie	k1gFnSc2	unie
nebo	nebo	k8xC	nebo
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
sporu	spor	k1gInSc2	spor
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
případy	případ	k1gInPc1	případ
nejprve	nejprve	k6eAd1	nejprve
posuzují	posuzovat	k5eAaImIp3nP	posuzovat
nižší	nízký	k2eAgInPc1d2	nižší
soudy	soud	k1gInPc1	soud
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
instance	instance	k1gFnSc1	instance
odvolací	odvolací	k2eAgFnSc1d1	odvolací
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
stanovených	stanovený	k2eAgMnPc2d1	stanovený
Kongresem	kongres	k1gInSc7	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Obviněným	obviněný	k1gMnPc3	obviněný
se	se	k3xPyFc4	se
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
soudní	soudní	k2eAgInSc4d1	soudní
proces	proces	k1gInSc4	proces
před	před	k7c7	před
porotou	porota	k1gFnSc7	porota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
definuje	definovat	k5eAaBmIp3nS	definovat
zločin	zločin	k1gInSc4	zločin
velezrady	velezrada	k1gFnSc2	velezrada
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
"	"	kIx"	"
<g/>
vyvolání	vyvolání	k1gNnSc3	vyvolání
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
nebo	nebo	k8xC	nebo
spolčení	spolčení	k1gNnSc3	spolčení
se	se	k3xPyFc4	se
s	s	k7c7	s
jejich	jejich	k3xOp3gMnSc7	jejich
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
poskytování	poskytování	k1gNnSc1	poskytování
mu	on	k3xPp3gMnSc3	on
pomoci	pomoct	k5eAaPmF	pomoct
a	a	k8xC	a
podpory	podpora	k1gFnSc2	podpora
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
Kongresu	kongres	k1gInSc2	kongres
právo	právo	k1gNnSc4	právo
stanovit	stanovit	k5eAaPmF	stanovit
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Článek	článek	k1gInSc1	článek
IV	Iva	k1gFnPc2	Iva
<g/>
:	:	kIx,	:
Pravomoci	pravomoc	k1gFnPc4	pravomoc
států	stát	k1gInPc2	stát
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
omezení	omezení	k1gNnPc2	omezení
===	===	k?	===
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
IV	Iva	k1gFnPc2	Iva
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgFnSc1d1	sestávající
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
tvořícími	tvořící	k2eAgInPc7d1	tvořící
federaci	federace	k1gFnSc6	federace
a	a	k8xC	a
federální	federální	k2eAgFnSc7d1	federální
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
řeší	řešit	k5eAaImIp3nP	řešit
také	také	k9	také
další	další	k2eAgFnPc4d1	další
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
problematiku	problematika	k1gFnSc4	problematika
vznikání	vznikání	k1gNnSc2	vznikání
či	či	k8xC	či
přijímání	přijímání	k1gNnSc2	přijímání
nových	nový	k2eAgInPc2d1	nový
států	stát	k1gInPc2	stát
nebo	nebo	k8xC	nebo
změnu	změna	k1gFnSc4	změna
hranic	hranice	k1gFnPc2	hranice
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
veřejné	veřejný	k2eAgInPc1d1	veřejný
akty	akt	k1gInPc1	akt
<g/>
,	,	kIx,	,
dokumenty	dokument	k1gInPc1	dokument
a	a	k8xC	a
soudní	soudní	k2eAgInPc1d1	soudní
rozsudky	rozsudek	k1gInPc1	rozsudek
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
byly	být	k5eAaImAgFnP	být
plně	plně	k6eAd1	plně
uznávány	uznávat	k5eAaImNgFnP	uznávat
i	i	k9	i
ostatními	ostatní	k2eAgInPc7d1	ostatní
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
je	být	k5eAaImIp3nS	být
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
určit	určit	k5eAaPmF	určit
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgNnSc7	jaký
bude	být	k5eAaImBp3nS	být
přezkoumána	přezkoumat	k5eAaPmNgFnS	přezkoumat
jejich	jejich	k3xOp3gFnSc1	jejich
právní	právní	k2eAgFnSc1d1	právní
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
ustanovení	ustanovení	k1gNnSc1	ustanovení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
vládám	vláda	k1gFnPc3	vláda
států	stát	k1gInPc2	stát
zvýhodňovat	zvýhodňovat	k5eAaImF	zvýhodňovat
své	svůj	k3xOyFgMnPc4	svůj
občany	občan	k1gMnPc4	občan
vůči	vůči	k7c3	vůči
občanům	občan	k1gMnPc3	občan
ostatních	ostatní	k2eAgInPc2d1	ostatní
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
různě	různě	k6eAd1	různě
přísnými	přísný	k2eAgInPc7d1	přísný
tresty	trest	k1gInPc7	trest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
také	také	k9	také
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
vydávání	vydávání	k1gNnSc1	vydávání
stíhaných	stíhaný	k2eAgFnPc2d1	stíhaná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
právo	právo	k1gNnSc4	právo
svobody	svoboda	k1gFnSc2	svoboda
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
cestování	cestování	k1gNnSc1	cestování
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
právo	právo	k1gNnSc1	právo
bere	brát	k5eAaImIp3nS	brát
za	za	k7c4	za
samozřejmé	samozřejmý	k2eAgNnSc4d1	samozřejmé
<g/>
,	,	kIx,	,
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
platnosti	platnost	k1gFnSc2	platnost
Článků	článek	k1gInPc2	článek
Konfederace	konfederace	k1gFnSc2	konfederace
bylo	být	k5eAaImAgNnS	být
překračování	překračování	k1gNnSc1	překračování
státních	státní	k2eAgFnPc2d1	státní
hranic	hranice	k1gFnPc2	hranice
často	často	k6eAd1	často
poměrně	poměrně	k6eAd1	poměrně
náročným	náročný	k2eAgInSc7d1	náročný
a	a	k8xC	a
nákladným	nákladný	k2eAgInSc7d1	nákladný
procesem	proces	k1gInSc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
také	také	k9	také
právo	práv	k2eAgNnSc1d1	právo
disponovat	disponovat	k5eAaBmF	disponovat
federálním	federální	k2eAgInSc7d1	federální
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
ovládat	ovládat	k5eAaImF	ovládat
ta	ten	k3xDgNnPc4	ten
teritoria	teritorium	k1gNnPc4	teritorium
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nepatří	patřit	k5eNaImIp3nS	patřit
žádnému	žádný	k1gMnSc3	žádný
ze	z	k7c2	z
států	stát	k1gInPc2	stát
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
oddíl	oddíl	k1gInSc1	oddíl
článku	článek	k1gInSc2	článek
pak	pak	k6eAd1	pak
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
každému	každý	k3xTgInSc3	každý
státu	stát	k1gInSc3	stát
federace	federace	k1gFnSc2	federace
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
formu	forma	k1gFnSc4	forma
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
agresi	agrese	k1gFnSc3	agrese
i	i	k8xC	i
vnitřnímu	vnitřní	k2eAgNnSc3d1	vnitřní
násilí	násilí	k1gNnSc3	násilí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Článek	článek	k1gInSc1	článek
V	V	kA	V
<g/>
:	:	kIx,	:
Dodatky	dodatek	k1gInPc1	dodatek
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
V	V	kA	V
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
připojovány	připojován	k2eAgInPc4d1	připojován
dodatky	dodatek	k1gInPc4	dodatek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
následujícími	následující	k2eAgInPc7d1	následující
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
dodatek	dodatek	k1gInSc1	dodatek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
schválen	schválit	k5eAaPmNgInS	schválit
dvěma	dva	k4xCgFnPc7	dva
třetinami	třetina	k1gFnPc7	třetina
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
odeslán	odeslat	k5eAaPmNgInS	odeslat
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
státům	stát	k1gInPc3	stát
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
zákonodárných	zákonodárný	k2eAgInPc2d1	zákonodárný
orgánů	orgán	k1gInPc2	orgán
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
mohou	moct	k5eAaImIp3nP	moct
požádat	požádat	k5eAaPmF	požádat
Kongres	kongres	k1gInSc4	kongres
o	o	k7c4	o
svolání	svolání	k1gNnSc4	svolání
ústavního	ústavní	k2eAgInSc2d1	ústavní
konventu	konvent	k1gInSc2	konvent
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zváží	zvážit	k5eAaPmIp3nS	zvážit
a	a	k8xC	a
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
případné	případný	k2eAgInPc4d1	případný
dodatky	dodatek	k1gInPc4	dodatek
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
státy	stát	k1gInPc4	stát
mohou	moct	k5eAaImIp3nP	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
dodatky	dodatek	k1gInPc7	dodatek
buď	buď	k8xC	buď
jejich	jejich	k3xOp3gInPc4	jejich
vlastní	vlastní	k2eAgInPc4d1	vlastní
zákonodárné	zákonodárný	k2eAgInPc4d1	zákonodárný
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
speciálně	speciálně	k6eAd1	speciálně
svolané	svolaný	k2eAgInPc4d1	svolaný
konventy	konvent	k1gInPc4	konvent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
získat	získat	k5eAaPmF	získat
souhlas	souhlas	k1gInSc4	souhlas
tří	tři	k4xCgFnPc2	tři
čtvrtin	čtvrtina	k1gFnPc2	čtvrtina
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
schvalování	schvalování	k1gNnSc2	schvalování
dodatku	dodatek	k1gInSc2	dodatek
konventy	konvent	k1gInPc1	konvent
byl	být	k5eAaImAgMnS	být
dosud	dosud	k6eAd1	dosud
zvolen	zvolit	k5eAaPmNgMnS	zvolit
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
při	při	k7c6	při
schvalování	schvalování	k1gNnSc6	schvalování
21	[number]	k4	21
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
zrušena	zrušen	k2eAgFnSc1d1	zrušena
prohibice	prohibice	k1gFnSc1	prohibice
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
V	V	kA	V
také	také	k9	také
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
omezení	omezení	k1gNnSc4	omezení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgNnSc2	jenž
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
žádným	žádný	k3yNgInSc7	žádný
dodatkem	dodatek	k1gInSc7	dodatek
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
zbavit	zbavit	k5eAaPmF	zbavit
žádný	žádný	k3yNgInSc1	žádný
stát	stát	k1gInSc1	stát
bez	bez	k7c2	bez
jeho	on	k3xPp3gInSc2	on
souhlasu	souhlas	k1gInSc2	souhlas
jeho	jeho	k3xOp3gFnSc7	jeho
Ústavou	ústava	k1gFnSc7	ústava
daného	daný	k2eAgNnSc2d1	dané
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
rovné	rovný	k2eAgNnSc4d1	rovné
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
omezení	omezení	k1gNnSc1	omezení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zakazovalo	zakazovat	k5eAaImAgNnS	zakazovat
měnit	měnit	k5eAaImF	měnit
některá	některý	k3yIgNnPc1	některý
ustanovení	ustanovení	k1gNnPc1	ustanovení
článku	článek	k1gInSc2	článek
I	i	k8xC	i
oddílu	oddíl	k1gInSc2	oddíl
9	[number]	k4	9
dotýkající	dotýkající	k2eAgFnSc1d1	dotýkající
se	se	k3xPyFc4	se
otroctví	otroctví	k1gNnPc2	otroctví
a	a	k8xC	a
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
toto	tento	k3xDgNnSc1	tento
omezení	omezení	k1gNnSc1	omezení
platilo	platit	k5eAaImAgNnS	platit
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Článek	článek	k1gInSc1	článek
VI	VI	kA	VI
<g/>
:	:	kIx,	:
Moc	moc	k1gFnSc1	moc
federace	federace	k1gFnSc2	federace
===	===	k?	===
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
VI	VI	kA	VI
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
vydané	vydaný	k2eAgMnPc4d1	vydaný
zákony	zákon	k1gInPc4	zákon
či	či	k8xC	či
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
dohody	dohoda	k1gFnPc4	dohoda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vrcholným	vrcholný	k2eAgNnSc7d1	vrcholné
právem	právo	k1gNnSc7	právo
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
všichni	všechen	k3xTgMnPc1	všechen
soudci	soudce	k1gMnPc1	soudce
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yIgInSc6	jaký
soudí	soudit	k5eAaImIp3nS	soudit
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gMnPc7	on
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnPc4	výjimka
vázáni	vázat	k5eAaImNgMnP	vázat
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
zákony	zákon	k1gInPc7	zákon
či	či	k8xC	či
ústavami	ústava	k1gFnPc7	ústava
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
nositelé	nositel	k1gMnPc1	nositel
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
<g/>
,	,	kIx,	,
výkonné	výkonný	k2eAgFnSc2d1	výkonná
i	i	k8xC	i
soudní	soudní	k2eAgFnSc2d1	soudní
moci	moc	k1gFnSc2	moc
jak	jak	k8xC	jak
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
zavazovali	zavazovat	k5eAaImAgMnP	zavazovat
přísahou	přísaha	k1gFnSc7	přísaha
či	či	k8xC	či
slavnostním	slavnostní	k2eAgInSc7d1	slavnostní
slibem	slib	k1gInSc7	slib
Ústavu	ústav	k1gInSc2	ústav
dodržovat	dodržovat	k5eAaImF	dodržovat
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
VI	VI	kA	VI
rovněž	rovněž	k9	rovněž
výslovně	výslovně	k6eAd1	výslovně
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
přípustné	přípustný	k2eAgNnSc1d1	přípustné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
úřadu	úřad	k1gInSc2	úřad
nebo	nebo	k8xC	nebo
veřejné	veřejný	k2eAgFnSc2d1	veřejná
funkce	funkce	k1gFnSc2	funkce
byla	být	k5eAaImAgFnS	být
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
nějakému	nějaký	k3yIgNnSc3	nějaký
náboženskému	náboženský	k2eAgNnSc3d1	náboženské
vyznání	vyznání	k1gNnSc3	vyznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
také	také	k6eAd1	také
uznaly	uznat	k5eAaPmAgInP	uznat
všechny	všechen	k3xTgInPc1	všechen
dluhy	dluh	k1gInPc1	dluh
a	a	k8xC	a
závazky	závazek	k1gInPc1	závazek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
učiněny	učiněn	k2eAgInPc1d1	učiněn
ještě	ještě	k9	ještě
před	před	k7c7	před
přijetím	přijetí	k1gNnSc7	přijetí
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
době	doba	k1gFnSc6	doba
platnosti	platnost	k1gFnSc2	platnost
Článků	článek	k1gInPc2	článek
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Článek	článek	k1gInSc1	článek
VII	VII	kA	VII
<g/>
:	:	kIx,	:
Ratifikace	ratifikace	k1gFnSc1	ratifikace
===	===	k?	===
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
VII	VII	kA	VII
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
za	za	k7c2	za
nichž	jenž	k3xRgInPc2	jenž
má	mít	k5eAaImIp3nS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
ustanovení	ustanovení	k1gNnSc2	ustanovení
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
schválení	schválení	k1gNnSc3	schválení
svolat	svolat	k5eAaPmF	svolat
konventy	konvent	k1gInPc4	konvent
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
federace	federace	k1gFnSc1	federace
a	a	k8xC	a
Ústava	ústava	k1gFnSc1	ústava
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
teprve	teprve	k6eAd1	teprve
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
až	až	k9	až
ji	on	k3xPp3gFnSc4	on
schválí	schválit	k5eAaPmIp3nP	schválit
konventy	konvent	k1gInPc1	konvent
nejméně	málo	k6eAd3	málo
devíti	devět	k4xCc2	devět
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Ústavu	ústav	k1gInSc3	ústav
ratifikovaly	ratifikovat	k5eAaBmAgInP	ratifikovat
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc3	on
budou	být	k5eAaImBp3nP	být
také	také	k6eAd1	také
vázány	vázat	k5eAaImNgFnP	vázat
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c7	za
článkem	článek	k1gInSc7	článek
VII	VII	kA	VII
následuje	následovat	k5eAaImIp3nS	následovat
nenadepsané	nadepsaný	k2eNgNnSc4d1	nadepsaný
prohlášení	prohlášení	k1gNnSc4	prohlášení
o	o	k7c6	o
jednohlasném	jednohlasný	k2eAgInSc6d1	jednohlasný
souhlasu	souhlas	k1gInSc6	souhlas
delegátů	delegát	k1gMnPc2	delegát
Ústavního	ústavní	k2eAgInSc2d1	ústavní
konventu	konvent	k1gInSc2	konvent
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
návrh	návrh	k1gInSc1	návrh
ústavy	ústava	k1gFnSc2	ústava
přijat	přijat	k2eAgInSc1d1	přijat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
podepsal	podepsat	k5eAaPmAgInS	podepsat
George	George	k1gFnSc4	George
Washington	Washington	k1gInSc1	Washington
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
konventu	konvent	k1gInSc2	konvent
a	a	k8xC	a
současně	současně	k6eAd1	současně
zástupce	zástupce	k1gMnSc1	zástupce
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
následující	následující	k2eAgMnPc1d1	následující
zástupci	zástupce	k1gMnPc1	zástupce
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Soudní	soudní	k2eAgNnSc1d1	soudní
přezkoumání	přezkoumání	k1gNnSc1	přezkoumání
ústavnosti	ústavnost	k1gFnSc2	ústavnost
==	==	k?	==
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
je	být	k5eAaImIp3nS	být
Ústava	ústava	k1gFnSc1	ústava
chápána	chápat	k5eAaImNgFnS	chápat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
soudními	soudní	k2eAgFnPc7d1	soudní
rozhodnutími	rozhodnutí	k1gNnPc7	rozhodnutí
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
rozhodnutími	rozhodnutí	k1gNnPc7	rozhodnutí
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
jsou	být	k5eAaImIp3nP	být
označována	označován	k2eAgNnPc1d1	označováno
jako	jako	k8xS	jako
precedenty	precedent	k1gInPc7	precedent
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1803	[number]	k4	1803
si	se	k3xPyFc3	se
William	William	k1gInSc4	William
Marbury	Marbura	k1gFnSc2	Marbura
<g/>
,	,	kIx,	,
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
právě	právě	k6eAd1	právě
končícím	končící	k2eAgMnSc7d1	končící
prezidentem	prezident	k1gMnSc7	prezident
Johnem	John	k1gMnSc7	John
Adamsem	Adams	k1gMnSc7	Adams
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
smírčího	smírčí	k1gMnSc2	smírčí
soudce	soudce	k1gMnSc2	soudce
<g/>
,	,	kIx,	,
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
na	na	k7c4	na
ministra	ministr	k1gMnSc4	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Jamese	Jamese	k1gFnSc2	Jamese
Madisona	Madisona	k1gFnSc1	Madisona
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabránil	zabránit	k5eAaPmAgInS	zabránit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
jmenování	jmenování	k1gNnSc4	jmenování
doručeno	doručen	k2eAgNnSc1d1	doručeno
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemohlo	moct	k5eNaImAgNnS	moct
vstoupit	vstoupit	k5eAaPmF	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
stížnost	stížnost	k1gFnSc4	stížnost
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c6	o
ustanovení	ustanovení	k1gNnSc6	ustanovení
tzv.	tzv.	kA	tzv.
Judiciary	Judiciar	k1gInPc1	Judiciar
Act	Act	k1gFnSc2	Act
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
soud	soud	k1gInSc1	soud
ji	on	k3xPp3gFnSc4	on
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
revolučním	revoluční	k2eAgNnSc7d1	revoluční
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
toto	tento	k3xDgNnSc4	tento
ustanovení	ustanovení	k1gNnSc4	ustanovení
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
protiústavní	protiústavní	k2eAgFnPc4d1	protiústavní
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
položil	položit	k5eAaPmAgMnS	položit
základ	základ	k1gInSc4	základ
doktríny	doktrína	k1gFnSc2	doktrína
soudního	soudní	k2eAgNnSc2d1	soudní
přezkoumání	přezkoumání	k1gNnSc2	přezkoumání
ústavnosti	ústavnost	k1gFnSc2	ústavnost
<g/>
.	.	kIx.	.
<g/>
Právo	právo	k1gNnSc1	právo
soudního	soudní	k2eAgNnSc2d1	soudní
přezkoumání	přezkoumání	k1gNnSc2	přezkoumání
dává	dávat	k5eAaImIp3nS	dávat
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
moc	moc	k6eAd1	moc
přezkoumat	přezkoumat	k5eAaPmF	přezkoumat
federální	federální	k2eAgInPc4d1	federální
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
nařízení	nařízení	k1gNnSc1	nařízení
exekutivních	exekutivní	k2eAgInPc2d1	exekutivní
orgánů	orgán	k1gInPc2	orgán
i	i	k9	i
zákony	zákon	k1gInPc4	zákon
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
a	a	k8xC	a
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
neodporují	odporovat	k5eNaImIp3nP	odporovat
ústavě	ústava	k1gFnSc3	ústava
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zrušit	zrušit	k5eAaPmF	zrušit
jejich	jejich	k3xOp3gFnSc4	jejich
platnost	platnost	k1gFnSc4	platnost
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	on	k3xPp3gMnPc4	on
shledá	shledat	k5eAaPmIp3nS	shledat
neústavními	ústavní	k2eNgNnPc7d1	neústavní
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgNnSc1d1	soudní
přezkoumání	přezkoumání	k1gNnSc1	přezkoumání
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
pravomoc	pravomoc	k1gFnSc4	pravomoc
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
vysvětlovat	vysvětlovat	k5eAaImF	vysvětlovat
význam	význam	k1gInSc4	význam
ustanovení	ustanovení	k1gNnSc2	ustanovení
Ústavy	ústava	k1gFnSc2	ústava
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
aplikaci	aplikace	k1gFnSc6	aplikace
na	na	k7c4	na
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
případy	případ	k1gInPc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
během	během	k7c2	během
zkoumání	zkoumání	k1gNnSc2	zkoumání
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
od	od	k7c2	od
vládní	vládní	k2eAgFnSc2d1	vládní
regulace	regulace	k1gFnSc2	regulace
rádií	rádio	k1gNnPc2	rádio
a	a	k8xC	a
televizí	televize	k1gFnPc2	televize
po	po	k7c4	po
práva	právo	k1gNnPc4	právo
obviněných	obviněný	k2eAgMnPc2d1	obviněný
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
mnohokrát	mnohokrát	k6eAd1	mnohokrát
změnil	změnit	k5eAaPmAgInS	změnit
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
interpretována	interpretován	k2eAgNnPc1d1	interpretováno
ustanovení	ustanovení	k1gNnPc1	ustanovení
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
k	k	k7c3	k
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
textu	text	k1gInSc3	text
Ústavy	ústava	k1gFnSc2	ústava
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
dodatek	dodatek	k1gInSc1	dodatek
<g/>
.	.	kIx.	.
<g/>
Různé	různý	k2eAgInPc1d1	různý
zákony	zákon	k1gInPc1	zákon
přijímané	přijímaný	k2eAgInPc1d1	přijímaný
k	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
ustanovení	ustanovení	k1gNnSc2	ustanovení
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
měnícím	měnící	k2eAgFnPc3d1	měnící
se	se	k3xPyFc4	se
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
a	a	k8xC	a
nepatrným	patrný	k2eNgInSc7d1	patrný
způsobem	způsob	k1gInSc7	způsob
také	také	k9	také
mění	měnit	k5eAaImIp3nS	měnit
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
je	být	k5eAaImIp3nS	být
přikládán	přikládán	k2eAgInSc1d1	přikládán
ústavnímu	ústavní	k2eAgInSc3d1	ústavní
textu	text	k1gInSc3	text
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
mají	mít	k5eAaImIp3nP	mít
podobný	podobný	k2eAgInSc4d1	podobný
účinek	účinek	k1gInSc4	účinek
také	také	k9	také
vyhlášky	vyhláška	k1gFnPc1	vyhláška
a	a	k8xC	a
nařízení	nařízení	k1gNnSc1	nařízení
mnoha	mnoho	k4c2	mnoho
federálních	federální	k2eAgInPc2d1	federální
vládních	vládní	k2eAgInPc2d1	vládní
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zpochybnění	zpochybnění	k1gNnSc3	zpochybnění
ať	ať	k8xS	ať
už	už	k6eAd1	už
Kongresem	kongres	k1gInSc7	kongres
přijatého	přijatý	k2eAgInSc2d1	přijatý
zákona	zákon	k1gInSc2	zákon
nebo	nebo	k8xC	nebo
vládního	vládní	k2eAgNnSc2d1	vládní
nařízení	nařízení	k1gNnSc2	nařízení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k6eAd1	právě
soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Ústavou	ústava	k1gFnSc7	ústava
přípustný	přípustný	k2eAgInSc1d1	přípustný
akt	akt	k1gInSc1	akt
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pravomoc	pravomoc	k1gFnSc1	pravomoc
není	být	k5eNaImIp3nS	být
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
dána	dán	k2eAgFnSc1d1	dána
Ústavou	ústava	k1gFnSc7	ústava
výslovně	výslovně	k6eAd1	výslovně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
dále	daleko	k6eAd2	daleko
posílena	posílit	k5eAaPmNgFnS	posílit
zvyklostmi	zvyklost	k1gFnPc7	zvyklost
a	a	k8xC	a
tradicí	tradice	k1gFnSc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
označovaný	označovaný	k2eAgInSc1d1	označovaný
termínem	termín	k1gInSc7	termín
"	"	kIx"	"
<g/>
živá	živý	k2eAgFnSc1d1	živá
ústava	ústava	k1gFnSc1	ústava
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
living	living	k1gInSc1	living
constitution	constitution	k1gInSc1	constitution
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
výklad	výklad	k1gInSc1	výklad
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
zcela	zcela	k6eAd1	zcela
jiným	jiný	k2eAgInSc7d1	jiný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
než	než	k8xS	než
jak	jak	k6eAd1	jak
zamýšleli	zamýšlet	k5eAaImAgMnP	zamýšlet
její	její	k3xOp3gMnPc1	její
tvůrci	tvůrce	k1gMnPc1	tvůrce
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
soudce	soudce	k1gMnSc1	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Antonin	Antonin	k1gInSc1	Antonin
Scalia	Scalium	k1gNnSc2	Scalium
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Madison	Madison	k1gMnSc1	Madison
vykládá	vykládat	k5eAaImIp3nS	vykládat
text	text	k1gInSc4	text
Ústavy	ústava	k1gFnSc2	ústava
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byl	být	k5eAaImAgInS	být
zamýšlen	zamýšlet	k5eAaImNgInS	zamýšlet
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
napsání	napsání	k1gNnSc2	napsání
<g/>
.	.	kIx.	.
<g/>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
také	také	k9	také
dal	dát	k5eAaPmAgInS	dát
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakmile	jakmile	k8xS	jakmile
jednou	jednou	k6eAd1	jednou
byl	být	k5eAaImAgInS	být
přijat	přijat	k2eAgInSc1d1	přijat
názor	názor	k1gInSc1	názor
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
už	už	k9	už
Kongresem	kongres	k1gInSc7	kongres
nebo	nebo	k8xC	nebo
soudy	soud	k1gInPc7	soud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ustanovení	ustanovení	k1gNnSc2	ustanovení
Ústavy	ústava	k1gFnSc2	ústava
týkají	týkat	k5eAaImIp3nP	týkat
určité	určitý	k2eAgFnPc4d1	určitá
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
nezvratné	zvratný	k2eNgFnPc1d1	nezvratná
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
složky	složka	k1gFnPc1	složka
politické	politický	k2eAgFnSc2d1	politická
moci	moc	k1gFnSc2	moc
mohly	moct	k5eAaImAgInP	moct
platnost	platnost	k1gFnSc4	platnost
Ústavy	ústava	k1gFnSc2	ústava
libovolně	libovolně	k6eAd1	libovolně
uznávat	uznávat	k5eAaImF	uznávat
či	či	k8xC	či
rušit	rušit	k5eAaImF	rušit
<g/>
,	,	kIx,	,
vedlo	vést	k5eAaImAgNnS	vést
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
by	by	kYmCp3nP	by
právě	právě	k6eAd1	právě
ony	onen	k3xDgFnPc1	onen
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
rozhodovaly	rozhodovat	k5eAaImAgFnP	rozhodovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
zákonné	zákonný	k2eAgNnSc1d1	zákonné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dodatky	dodatek	k1gInPc4	dodatek
==	==	k?	==
</s>
</p>
<p>
<s>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
Ústavy	ústava	k1gFnSc2	ústava
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
vědomi	vědom	k2eAgMnPc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
setrvat	setrvat	k5eAaPmF	setrvat
i	i	k9	i
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
období	období	k1gNnPc2	období
vývoje	vývoj	k1gInSc2	vývoj
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
potřebné	potřebný	k2eAgFnPc1d1	potřebná
její	její	k3xOp3gFnPc1	její
změny	změna	k1gFnPc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
by	by	kYmCp3nP	by
změny	změna	k1gFnPc4	změna
neměly	mít	k5eNaImAgFnP	mít
být	být	k5eAaImF	být
příliš	příliš	k6eAd1	příliš
snadné	snadný	k2eAgNnSc1d1	snadné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nebyly	být	k5eNaImAgInP	být
přijímány	přijímat	k5eAaImNgInP	přijímat
nedomyšlené	domyšlený	k2eNgInPc1d1	nedomyšlený
a	a	k8xC	a
narychlo	narychlo	k6eAd1	narychlo
schválené	schválený	k2eAgInPc4d1	schválený
dodatky	dodatek	k1gInPc4	dodatek
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
příliš	příliš	k6eAd1	příliš
přísný	přísný	k2eAgInSc1d1	přísný
požadavek	požadavek	k1gInSc1	požadavek
jednomyslnosti	jednomyslnost	k1gFnSc2	jednomyslnost
nezablokoval	zablokovat	k5eNaPmAgInS	zablokovat
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
zásah	zásah	k1gInSc1	zásah
požadovaný	požadovaný	k2eAgInSc1d1	požadovaný
velkou	velký	k2eAgFnSc7d1	velká
většinou	většina	k1gFnSc7	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Zvoleným	zvolený	k2eAgNnSc7d1	zvolené
řešením	řešení	k1gNnSc7	řešení
byl	být	k5eAaImAgInS	být
dvoustupňový	dvoustupňový	k2eAgInSc1d1	dvoustupňový
proces	proces	k1gInSc1	proces
navrhování	navrhování	k1gNnSc2	navrhování
a	a	k8xC	a
schvalování	schvalování	k1gNnSc2	schvalování
nových	nový	k2eAgInPc2d1	nový
dodatků	dodatek	k1gInPc2	dodatek
<g/>
.	.	kIx.	.
<g/>
Dodatky	dodatek	k1gInPc1	dodatek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
dvěma	dva	k4xCgInPc7	dva
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
tak	tak	k9	tak
vždy	vždy	k6eAd1	vždy
stalo	stát	k5eAaPmAgNnS	stát
prosazením	prosazení	k1gNnSc7	prosazení
návrhu	návrh	k1gInSc2	návrh
dvoutřetinovou	dvoutřetinový	k2eAgFnSc7d1	dvoutřetinová
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
možným	možný	k2eAgInSc7d1	možný
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
zákonodárců	zákonodárce	k1gMnPc2	zákonodárce
odhlasují	odhlasovat	k5eAaPmIp3nP	odhlasovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Kongres	kongres	k1gInSc1	kongres
svolal	svolat	k5eAaPmAgInS	svolat
ústavní	ústavní	k2eAgInSc1d1	ústavní
konvent	konvent	k1gInSc1	konvent
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
navrhnout	navrhnout	k5eAaPmF	navrhnout
potřebné	potřebný	k2eAgInPc4d1	potřebný
dodatky	dodatek	k1gInPc4	dodatek
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zatím	zatím	k6eAd1	zatím
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
přesným	přesný	k2eAgInSc7d1	přesný
způsobem	způsob	k1gInSc7	způsob
by	by	kYmCp3nS	by
takový	takový	k3xDgInSc1	takový
konvent	konvent	k1gInSc1	konvent
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
se	se	k3xPyFc4	se
Kongres	kongres	k1gInSc1	kongres
zabýval	zabývat	k5eAaImAgInS	zabývat
asi	asi	k9	asi
10	[number]	k4	10
tisíci	tisíc	k4xCgInPc7	tisíc
požadovanými	požadovaný	k2eAgInPc7d1	požadovaný
ústavními	ústavní	k2eAgInPc7d1	ústavní
dodatky	dodatek	k1gInPc7	dodatek
(	(	kIx(	(
<g/>
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
100	[number]	k4	100
až	až	k9	až
200	[number]	k4	200
požadovaných	požadovaný	k2eAgInPc2d1	požadovaný
dodatků	dodatek	k1gInPc2	dodatek
během	během	k7c2	během
zasedacího	zasedací	k2eAgNnSc2d1	zasedací
období	období	k1gNnSc2	období
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgInPc2	tento
nápadů	nápad	k1gInPc2	nápad
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
neprojde	projít	k5eNaPmIp3nS	projít
přes	přes	k7c4	přes
příslušný	příslušný	k2eAgInSc4d1	příslušný
výbor	výbor	k1gInSc4	výbor
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
a	a	k8xC	a
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
jich	on	k3xPp3gMnPc2	on
nakonec	nakonec	k6eAd1	nakonec
Kongres	kongres	k1gInSc4	kongres
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
<g/>
.	.	kIx.	.
<g/>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
dodatek	dodatek	k1gInSc1	dodatek
navržen	navrhnout	k5eAaPmNgInS	navrhnout
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ratifikován	ratifikovat	k5eAaBmNgInS	ratifikovat
třemi	tři	k4xCgFnPc7	tři
čtvrtinami	čtvrtina	k1gFnPc7	čtvrtina
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
určí	určit	k5eAaPmIp3nS	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
ho	on	k3xPp3gMnSc4	on
mají	mít	k5eAaImIp3nP	mít
ratifikovat	ratifikovat	k5eAaBmF	ratifikovat
zákonodárné	zákonodárný	k2eAgInPc4d1	zákonodárný
orgány	orgán	k1gInPc4	orgán
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
speciálně	speciálně	k6eAd1	speciálně
svolané	svolaný	k2eAgInPc4d1	svolaný
státní	státní	k2eAgInPc4d1	státní
konventy	konvent	k1gInPc4	konvent
<g/>
.	.	kIx.	.
</s>
<s>
Prozatím	prozatím	k6eAd1	prozatím
jediný	jediný	k2eAgInSc1d1	jediný
dodatek	dodatek	k1gInSc1	dodatek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
projednávaly	projednávat	k5eAaImAgFnP	projednávat
(	(	kIx(	(
<g/>
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
také	také	k6eAd1	také
schválily	schválit	k5eAaPmAgInP	schválit
<g/>
)	)	kIx)	)
konventy	konvent	k1gInPc1	konvent
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
zrušena	zrušen	k2eAgFnSc1d1	zrušena
prohibice	prohibice	k1gFnSc1	prohibice
<g/>
,	,	kIx,	,
schválená	schválený	k2eAgFnSc1d1	schválená
o	o	k7c4	o
třináct	třináct	k4xCc4	třináct
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
změn	změna	k1gFnPc2	změna
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
u	u	k7c2	u
ústav	ústava	k1gFnPc2	ústava
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
dodatky	dodatek	k1gInPc4	dodatek
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgMnPc2d1	americký
jsou	být	k5eAaImIp3nP	být
připojovány	připojovat	k5eAaImNgFnP	připojovat
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
textu	text	k1gInSc3	text
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
bylo	být	k5eAaImAgNnS	být
cokoliv	cokoliv	k3yInSc4	cokoliv
změněno	změněn	k2eAgNnSc1d1	změněno
nebo	nebo	k8xC	nebo
odstraněno	odstraněn	k2eAgNnSc1d1	odstraněno
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nový	nový	k2eAgInSc4d1	nový
text	text	k1gInSc4	text
zcela	zcela	k6eAd1	zcela
jasně	jasně	k6eAd1	jasně
odporuje	odporovat	k5eAaImIp3nS	odporovat
staršímu	starší	k1gMnSc3	starší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
upřednostněna	upřednostněn	k2eAgFnSc1d1	upřednostněna
nová	nový	k2eAgFnSc1d1	nová
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
21	[number]	k4	21
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
technického	technický	k2eAgNnSc2d1	technické
hlediska	hledisko	k1gNnSc2	hledisko
tak	tak	k9	tak
nic	nic	k3yNnSc1	nic
nebrání	bránit	k5eNaImIp3nS	bránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nějaký	nějaký	k3yIgInSc4	nějaký
dodatek	dodatek	k1gInSc4	dodatek
namísto	namísto	k7c2	namísto
doplnění	doplnění	k1gNnSc2	doplnění
původního	původní	k2eAgInSc2d1	původní
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
text	text	k1gInSc1	text
zcela	zcela	k6eAd1	zcela
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ratifikované	ratifikovaný	k2eAgInPc4d1	ratifikovaný
dodatky	dodatek	k1gInPc4	dodatek
===	===	k?	===
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgMnPc2d1	americký
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
27	[number]	k4	27
dodatků	dodatek	k1gInPc2	dodatek
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
deset	deset	k4xCc4	deset
<g/>
,	,	kIx,	,
známých	známý	k1gMnPc2	známý
pod	pod	k7c7	pod
souhrnným	souhrnný	k2eAgInSc7d1	souhrnný
názvem	název	k1gInSc7	název
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ratifikováno	ratifikovat	k5eAaBmNgNnS	ratifikovat
současně	současně	k6eAd1	současně
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
velmi	velmi	k6eAd1	velmi
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
původního	původní	k2eAgInSc2d1	původní
textu	text	k1gInSc2	text
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgMnPc2d1	následující
17	[number]	k4	17
bylo	být	k5eAaImAgNnS	být
ratifikováno	ratifikovat	k5eAaBmNgNnS	ratifikovat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
následujících	následující	k2eAgInPc2d1	následující
dvou	dva	k4xCgInPc2	dva
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
dodatky	dodatek	k1gInPc1	dodatek
I	i	k9	i
až	až	k9	až
X	X	kA	X
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgFnSc1d1	původní
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
se	se	k3xPyFc4	se
neměla	mít	k5eNaImAgFnS	mít
vztahovat	vztahovat	k5eAaImF	vztahovat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
textu	text	k1gInSc2	text
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgNnSc2	takový
nevyplývá	vyplývat	k5eNaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
těch	ten	k3xDgNnPc2	ten
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dodatky	dodatek	k1gInPc7	dodatek
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
výslovně	výslovně	k6eAd1	výslovně
o	o	k7c6	o
federální	federální	k2eAgFnSc6d1	federální
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
takovým	takový	k3xDgInSc7	takový
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Kongres	kongres	k1gInSc1	kongres
nesmí	smět	k5eNaImIp3nS	smět
vydávat	vydávat	k5eAaPmF	vydávat
zákony	zákon	k1gInPc4	zákon
zavádějící	zavádějící	k2eAgInPc4d1	zavádějící
nějaké	nějaký	k3yIgNnSc1	nějaký
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
letech	let	k1gInPc6	let
platnosti	platnost	k1gFnSc2	platnost
Listiny	listina	k1gFnSc2	listina
využily	využít	k5eAaPmAgInP	využít
některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
a	a	k8xC	a
zavedly	zavést	k5eAaPmAgFnP	zavést
státní	státní	k2eAgNnSc4d1	státní
náboženství	náboženství	k1gNnSc4	náboženství
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
různé	různý	k2eAgInPc4d1	různý
náboženské	náboženský	k2eAgInPc4d1	náboženský
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
<g/>
Otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
ustanovení	ustanovení	k1gNnSc1	ustanovení
Listiny	listina	k1gFnSc2	listina
práv	právo	k1gNnPc2	právo
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
pouze	pouze	k6eAd1	pouze
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
států	stát	k1gInPc2	stát
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
řešil	řešit	k5eAaImAgInS	řešit
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
poprvé	poprvé	k6eAd1	poprvé
během	během	k7c2	během
sporu	spor	k1gInSc2	spor
Barron	Barron	k1gInSc1	Barron
v.	v.	k?	v.
Baltimore	Baltimore	k1gInSc1	Baltimore
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
a	a	k8xC	a
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Listina	listina	k1gFnSc1	listina
omezuje	omezovat	k5eAaImIp3nS	omezovat
pouze	pouze	k6eAd1	pouze
federální	federální	k2eAgFnSc1d1	federální
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
však	však	k9	však
zvrátilo	zvrátit	k5eAaPmAgNnS	zvrátit
přijetí	přijetí	k1gNnSc1	přijetí
14	[number]	k4	14
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
Ústavy	ústava	k1gFnSc2	ústava
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
výslovně	výslovně	k6eAd1	výslovně
uváděl	uvádět	k5eAaImAgMnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
žádný	žádný	k3yNgInSc1	žádný
stát	stát	k1gInSc1	stát
nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
vydat	vydat	k5eAaPmF	vydat
nebo	nebo	k8xC	nebo
prosazovat	prosazovat	k5eAaImF	prosazovat
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
omezoval	omezovat	k5eAaImAgMnS	omezovat
svobody	svoboda	k1gFnSc2	svoboda
nebo	nebo	k8xC	nebo
výsady	výsada	k1gFnSc2	výsada
občanů	občan	k1gMnPc2	občan
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
;	;	kIx,	;
žádný	žádný	k3yNgInSc1	žádný
stát	stát	k1gInSc1	stát
nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
zbavit	zbavit	k5eAaPmF	zbavit
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
<g />
.	.	kIx.	.
</s>
<s>
osobu	osoba	k1gFnSc4	osoba
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc2d1	osobní
svobody	svoboda	k1gFnSc2	svoboda
nebo	nebo	k8xC	nebo
majetku	majetek	k1gInSc2	majetek
bez	bez	k7c2	bez
řádného	řádný	k2eAgInSc2d1	řádný
soudního	soudní	k2eAgInSc2d1	soudní
procesu	proces	k1gInSc2	proces
<g/>
;	;	kIx,	;
nemá	mít	k5eNaImIp3nS	mít
také	také	k9	také
právo	právo	k1gNnSc1	právo
zbavit	zbavit	k5eAaPmF	zbavit
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
podléhající	podléhající	k2eAgFnSc4d1	podléhající
jeho	jeho	k3xOp3gFnSc3	jeho
pravomoci	pravomoc	k1gFnSc3	pravomoc
<g/>
,	,	kIx,	,
stejné	stejná	k1gFnSc3	stejná
ochrany	ochrana	k1gFnSc2	ochrana
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
že	že	k8xS	že
"	"	kIx"	"
<g/>
Kongres	kongres	k1gInSc1	kongres
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc1	právo
provést	provést	k5eAaPmF	provést
tento	tento	k3xDgInSc4	tento
článek	článek	k1gInSc4	článek
přijetím	přijetí	k1gNnSc7	přijetí
odpovídajících	odpovídající	k2eAgFnPc2d1	odpovídající
norem	norma	k1gFnPc2	norma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
řešil	řešit	k5eAaImAgInS	řešit
řadu	řada	k1gFnSc4	řada
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
nabyla	nabýt	k5eAaPmAgFnS	nabýt
postupně	postupně	k6eAd1	postupně
vrchu	vrch	k1gInSc2	vrch
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
Listiny	listina	k1gFnSc2	listina
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
např.	např.	kA	např.
5	[number]	k4	5
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc3	dodatek
požadujícího	požadující	k2eAgNnSc2d1	požadující
souzení	souzení	k1gNnSc2	souzení
vážných	vážný	k2eAgInPc2d1	vážný
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
před	před	k7c7	před
velkou	velký	k2eAgFnSc7d1	velká
porotou	porota	k1gFnSc7	porota
<g/>
)	)	kIx)	)
týká	týkat	k5eAaImIp3nS	týkat
také	také	k9	také
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vyváženost	vyváženost	k1gFnSc1	vyváženost
státní	státní	k2eAgFnSc2d1	státní
a	a	k8xC	a
federální	federální	k2eAgFnSc2d1	federální
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
doktríny	doktrína	k1gFnSc2	doktrína
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
otázkou	otázka	k1gFnSc7	otázka
a	a	k8xC	a
u	u	k7c2	u
federálních	federální	k2eAgInPc2d1	federální
soudů	soud	k1gInPc2	soud
neustále	neustále	k6eAd1	neustále
probíhají	probíhat	k5eAaImIp3nP	probíhat
soudní	soudní	k2eAgInPc4d1	soudní
spory	spor	k1gInPc4	spor
o	o	k7c4	o
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
návrhu	návrh	k1gInSc2	návrh
dvanácti	dvanáct	k4xCc2	dvanáct
dodatků	dodatek	k1gInPc2	dodatek
<g/>
,	,	kIx,	,
představeného	představený	k2eAgInSc2d1	představený
Kongresem	kongres	k1gInSc7	kongres
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ratifikováno	ratifikovat	k5eAaBmNgNnS	ratifikovat
jen	jen	k9	jen
posledních	poslední	k2eAgFnPc2d1	poslední
deset	deset	k4xCc1	deset
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
dělení	dělení	k1gNnSc4	dělení
míst	místo	k1gNnPc2	místo
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
konaného	konaný	k2eAgInSc2d1	konaný
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
I	i	k8xC	i
Ústavy	ústava	k1gFnSc2	ústava
každých	každý	k3xTgInPc2	každý
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
neschválil	schválit	k5eNaPmAgInS	schválit
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
počet	počet	k1gInSc1	počet
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
technického	technický	k2eAgNnSc2d1	technické
hlediska	hledisko	k1gNnSc2	hledisko
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zákonodárnými	zákonodárný	k2eAgInPc7d1	zákonodárný
orgány	orgán	k1gInPc7	orgán
států	stát	k1gInPc2	stát
i	i	k8xC	i
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
stech	sto	k4xCgNnPc6	sto
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
teoreticky	teoreticky	k6eAd1	teoreticky
ratifikován	ratifikován	k2eAgInSc4d1	ratifikován
<g/>
.	.	kIx.	.
<g/>
Původní	původní	k2eAgInSc4d1	původní
druhý	druhý	k4xOgInSc4	druhý
dodatek	dodatek	k1gInSc4	dodatek
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgInSc4d1	týkající
se	se	k3xPyFc4	se
náhrad	náhrada	k1gFnPc2	náhrada
členům	člen	k1gMnPc3	člen
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ratifikován	ratifikovat	k5eAaBmNgInS	ratifikovat
dostatečným	dostatečný	k2eAgInSc7d1	dostatečný
počtem	počet	k1gInSc7	počet
států	stát	k1gInPc2	stát
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tohoto	tento	k3xDgNnSc2	tento
dvousetletého	dvousetletý	k2eAgNnSc2d1	dvousetleté
zpoždění	zpoždění	k1gNnSc2	zpoždění
byl	být	k5eAaImAgInS	být
přidán	přidat	k5eAaPmNgInS	přidat
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
až	až	k6eAd1	až
jako	jako	k8xS	jako
27	[number]	k4	27
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
není	být	k5eNaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Listiny	listina	k1gFnSc2	listina
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
dodatek	dodatek	k1gInSc1	dodatek
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
(	(	kIx(	(
<g/>
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
Kongresu	kongres	k1gInSc2	kongres
ustanovovat	ustanovovat	k5eAaImF	ustanovovat
státní	státní	k2eAgNnSc4d1	státní
náboženství	náboženství	k1gNnSc4	náboženství
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
vyznání	vyznání	k1gNnSc4	vyznání
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
shromažďování	shromažďování	k1gNnSc2	shromažďování
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
práva	práv	k2eAgFnSc1d1	práva
žádat	žádat	k5eAaImF	žádat
státní	státní	k2eAgInPc4d1	státní
orgány	orgán	k1gInPc4	orgán
o	o	k7c4	o
nápravu	náprava	k1gFnSc4	náprava
křivd	křivda	k1gFnPc2	křivda
<g/>
.	.	kIx.	.
<g/>
Druhý	druhý	k4xOgInSc1	druhý
dodatek	dodatek	k1gInSc1	dodatek
garantuje	garantovat	k5eAaBmIp3nS	garantovat
právo	právo	k1gNnSc4	právo
občanů	občan	k1gMnPc2	občan
vlastnit	vlastnit	k5eAaImF	vlastnit
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
interpretace	interpretace	k1gFnSc1	interpretace
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
soudního	soudní	k2eAgInSc2d1	soudní
sporu	spor	k1gInSc2	spor
McDonald	McDonald	k1gMnSc1	McDonald
v.	v.	k?	v.
Chicago	Chicago	k1gNnSc4	Chicago
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
soud	soud	k1gInSc1	soud
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
dodatek	dodatek	k1gInSc1	dodatek
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
dodatkem	dodatek	k1gInSc7	dodatek
<g/>
,	,	kIx,	,
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
i	i	k9	i
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
<g/>
Třetí	třetí	k4xOgInSc1	třetí
dodatek	dodatek	k1gInSc1	dodatek
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
vládě	vláda	k1gFnSc3	vláda
užívat	užívat	k5eAaImF	užívat
soukromé	soukromý	k2eAgInPc4d1	soukromý
domy	dům	k1gInPc4	dům
pro	pro	k7c4	pro
ubytování	ubytování	k1gNnSc4	ubytování
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
jejich	jejich	k3xOp3gMnPc2	jejich
majitelů	majitel	k1gMnPc2	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
soudní	soudní	k2eAgInSc1d1	soudní
případ	případ	k1gInSc1	případ
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
případu	případ	k1gInSc2	případ
Engblom	Engblom	k1gInSc1	Engblom
v.	v.	k?	v.
Carey	Carea	k1gFnSc2	Carea
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
guvernér	guvernér	k1gMnSc1	guvernér
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
povolal	povolat	k5eAaPmAgInS	povolat
Národní	národní	k2eAgFnSc4d1	národní
gardu	garda	k1gFnSc4	garda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zastala	zastat	k5eAaPmAgFnS	zastat
některé	některý	k3yIgFnPc4	některý
povinnosti	povinnost	k1gFnPc4	povinnost
stávkujících	stávkující	k2eAgMnPc2d1	stávkující
vězeňských	vězeňský	k2eAgMnPc2d1	vězeňský
dozorců	dozorce	k1gMnPc2	dozorce
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
gardistů	gardista	k1gMnPc2	gardista
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
některé	některý	k3yIgMnPc4	některý
stávkující	stávkující	k1gMnPc4	stávkující
ze	z	k7c2	z
zaměstnanecké	zaměstnanecký	k2eAgFnSc2d1	zaměstnanecká
ubytovny	ubytovna	k1gFnSc2	ubytovna
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
dodatek	dodatek	k1gInSc1	dodatek
citován	citován	k2eAgInSc1d1	citován
také	také	k9	také
v	v	k7c6	v
případu	případ	k1gInSc6	případ
Griswold	Griswold	k1gMnSc1	Griswold
v.	v.	k?	v.
Connecticut	Connecticut	k1gMnSc1	Connecticut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jím	jíst	k5eAaImIp1nS	jíst
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
podpořil	podpořit	k5eAaPmAgInS	podpořit
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústava	ústava	k1gFnSc1	ústava
chrání	chránit	k5eAaImIp3nS	chránit
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
osobní	osobní	k2eAgNnSc4d1	osobní
soukromí	soukromí	k1gNnSc4	soukromí
<g/>
.	.	kIx.	.
<g/>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
dodatek	dodatek	k1gInSc1	dodatek
chrání	chránit	k5eAaImIp3nS	chránit
občany	občan	k1gMnPc4	občan
před	před	k7c7	před
prohledáváním	prohledávání	k1gNnSc7	prohledávání
<g/>
,	,	kIx,	,
zatýkáním	zatýkání	k1gNnSc7	zatýkání
a	a	k8xC	a
zabavováním	zabavování	k1gNnSc7	zabavování
majetku	majetek	k1gInSc2	majetek
bez	bez	k7c2	bez
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
soudního	soudní	k2eAgInSc2d1	soudní
příkazu	příkaz	k1gInSc2	příkaz
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
důvodu	důvod	k1gInSc2	důvod
opravňujícího	opravňující	k2eAgNnSc2d1	opravňující
věřit	věřit	k5eAaImF	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
zločin	zločin	k1gInSc1	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
odvodil	odvodit	k5eAaPmAgMnS	odvodit
některá	některý	k3yIgNnPc4	některý
práva	právo	k1gNnPc4	právo
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
soukromí	soukromí	k1gNnSc1	soukromí
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
<g/>
Pátý	pátý	k4xOgInSc1	pátý
dodatek	dodatek	k1gInSc1	dodatek
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
soudit	soudit	k5eAaImF	soudit
za	za	k7c4	za
vážný	vážný	k2eAgInSc4d1	vážný
zločin	zločin	k1gInSc4	zločin
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
podána	podat	k5eAaPmNgFnS	podat
řádná	řádný	k2eAgFnSc1d1	řádná
žaloba	žaloba	k1gFnSc1	žaloba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
velké	velký	k2eAgFnSc2d1	velká
poroty	porota	k1gFnSc2	porota
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
občanů	občan	k1gMnPc2	občan
sloužících	sloužící	k2eAgMnPc2d1	sloužící
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
nebo	nebo	k8xC	nebo
domobraně	domobrana	k1gFnSc6	domobrana
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
nebo	nebo	k8xC	nebo
vážného	vážný	k2eAgNnSc2d1	vážné
ohrožení	ohrožení	k1gNnSc2	ohrožení
veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
obviňovat	obviňovat	k5eAaImF	obviňovat
opakovaně	opakovaně	k6eAd1	opakovaně
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
vážného	vážný	k2eAgInSc2d1	vážný
zločinu	zločin	k1gInSc2	zločin
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
obviněným	obviněný	k1gMnPc3	obviněný
právo	právo	k1gNnSc4	právo
odmítnout	odmítnout	k5eAaPmF	odmítnout
svědectví	svědectví	k1gNnSc4	svědectví
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
samým	samý	k3xTgMnPc3	samý
a	a	k8xC	a
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
řádný	řádný	k2eAgInSc4d1	řádný
soudní	soudní	k2eAgInSc4d1	soudní
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
ustanovení	ustanovení	k1gNnSc1	ustanovení
pak	pak	k6eAd1	pak
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
zabavovat	zabavovat	k5eAaImF	zabavovat
majetek	majetek	k1gInSc4	majetek
bez	bez	k7c2	bez
spravedlivé	spravedlivý	k2eAgFnSc2d1	spravedlivá
náhrady	náhrada	k1gFnSc2	náhrada
<g/>
;	;	kIx,	;
tohle	tenhle	k3xDgNnSc1	tenhle
ustanovení	ustanovení	k1gNnSc1	ustanovení
slouží	sloužit	k5eAaImIp3nS	sloužit
za	za	k7c4	za
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
zákony	zákon	k1gInPc4	zákon
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
vyvlastňování	vyvlastňování	k1gNnSc4	vyvlastňování
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
<g/>
Šestý	šestý	k4xOgInSc1	šestý
dodatek	dodatek	k1gInSc1	dodatek
garantuje	garantovat	k5eAaBmIp3nS	garantovat
obviněným	obviněný	k1gMnPc3	obviněný
z	z	k7c2	z
kriminálních	kriminální	k2eAgInPc2d1	kriminální
deliktů	delikt	k1gInPc2	delikt
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
rychlý	rychlý	k2eAgInSc4d1	rychlý
veřejný	veřejný	k2eAgInSc4d1	veřejný
proces	proces	k1gInSc4	proces
před	před	k7c7	před
porotou	porota	k1gFnSc7	porota
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
obžalovaný	obžalovaný	k1gMnSc1	obžalovaný
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
právní	právní	k2eAgFnSc4d1	právní
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
na	na	k7c4	na
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
příčině	příčina	k1gFnSc6	příčina
a	a	k8xC	a
povaze	povaha	k1gFnSc6	povaha
žaloby	žaloba	k1gFnSc2	žaloba
a	a	k8xC	a
na	na	k7c6	na
konfrontaci	konfrontace	k1gFnSc6	konfrontace
se	s	k7c7	s
svědky	svědek	k1gMnPc7	svědek
obžaloby	obžaloba	k1gFnSc2	obžaloba
<g/>
.	.	kIx.	.
</s>
<s>
Šestý	šestý	k4xOgInSc1	šestý
dodatek	dodatek	k1gInSc1	dodatek
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
citován	citován	k2eAgInSc1d1	citován
během	během	k7c2	během
soudních	soudní	k2eAgInPc2d1	soudní
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ustanovení	ustanovení	k1gNnSc1	ustanovení
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
svědectví	svědectví	k1gNnSc2	svědectví
proti	proti	k7c3	proti
sama	sám	k3xTgFnSc1	sám
sobě	se	k3xPyFc3	se
a	a	k8xC	a
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
právní	právní	k2eAgFnSc4d1	právní
pomoc	pomoc	k1gFnSc4	pomoc
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dávána	dávat	k5eAaImNgFnS	dávat
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
všem	všecek	k3xTgMnPc3	všecek
zatčeným	zatčený	k1gMnPc3	zatčený
<g/>
.	.	kIx.	.
<g/>
Sedmý	sedmý	k4xOgInSc1	sedmý
dodatek	dodatek	k1gInSc1	dodatek
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
v	v	k7c6	v
občanských	občanský	k2eAgInPc6d1	občanský
soudních	soudní	k2eAgInPc6d1	soudní
sporech	spor	k1gInPc6	spor
při	při	k7c6	při
pohledávkách	pohledávka	k1gFnPc6	pohledávka
nad	nad	k7c4	nad
dvacet	dvacet	k4xCc4	dvacet
dolarů	dolar	k1gInPc2	dolar
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
projednání	projednání	k1gNnSc4	projednání
před	před	k7c7	před
porotou	porota	k1gFnSc7	porota
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnosti	skutečnost	k1gFnPc1	skutečnost
přezkoumané	přezkoumaný	k2eAgMnPc4d1	přezkoumaný
porotním	porotní	k2eAgInSc7d1	porotní
soudem	soud	k1gInSc7	soud
již	již	k6eAd1	již
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
znovu	znovu	k6eAd1	znovu
přezkoumávány	přezkoumáván	k2eAgInPc4d1	přezkoumáván
jinými	jiný	k2eAgInPc7d1	jiný
soudy	soud	k1gInPc7	soud
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tak	tak	k9	tak
stanoví	stanovit	k5eAaPmIp3nS	stanovit
obecné	obecný	k2eAgNnSc4d1	obecné
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
<g/>
Osmý	osmý	k4xOgInSc1	osmý
dodatek	dodatek	k1gInSc1	dodatek
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
stanovovat	stanovovat	k5eAaImF	stanovovat
příliš	příliš	k6eAd1	příliš
vysoké	vysoký	k2eAgFnPc4d1	vysoká
kauce	kauce	k1gFnPc4	kauce
a	a	k8xC	a
ukládat	ukládat	k5eAaImF	ukládat
příliš	příliš	k6eAd1	příliš
vysoké	vysoký	k2eAgFnPc4d1	vysoká
pokuty	pokuta	k1gFnPc4	pokuta
nebo	nebo	k8xC	nebo
kruté	krutý	k2eAgInPc4d1	krutý
a	a	k8xC	a
neobvyklé	obvyklý	k2eNgInPc4d1	neobvyklý
tresty	trest	k1gInPc4	trest
<g/>
.	.	kIx.	.
<g/>
Devátý	devátý	k4xOgInSc1	devátý
dodatek	dodatek	k1gInSc1	dodatek
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výčet	výčet	k1gInSc4	výčet
práv	právo	k1gNnPc2	právo
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
zaručených	zaručený	k2eAgInPc2d1	zaručený
Ústavou	ústava	k1gFnSc7	ústava
a	a	k8xC	a
Listinou	listina	k1gFnSc7	listina
práv	právo	k1gNnPc2	právo
není	být	k5eNaImIp3nS	být
úplný	úplný	k2eAgInSc1d1	úplný
<g/>
.	.	kIx.	.
</s>
<s>
Práva	práv	k2eAgFnSc1d1	práva
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dokumentech	dokument	k1gInPc6	dokument
totiž	totiž	k9	totiž
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
vykládána	vykládán	k2eAgFnSc1d1	vykládána
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
důsledku	důsledek	k1gInSc6	důsledek
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
popírána	popírán	k2eAgNnPc1d1	popíráno
nebo	nebo	k8xC	nebo
zlehčována	zlehčován	k2eAgNnPc1d1	zlehčováno
ostatní	ostatní	k2eAgNnPc1d1	ostatní
práva	právo	k1gNnPc1	právo
náležející	náležející	k2eAgNnPc1d1	náležející
lidu	lid	k1gInSc3	lid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Desátý	desátý	k4xOgInSc1	desátý
dodatek	dodatek	k1gInSc1	dodatek
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
státům	stát	k1gInPc3	stát
a	a	k8xC	a
lidu	lid	k1gInSc2	lid
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
Ústava	ústava	k1gFnSc1	ústava
výslovně	výslovně	k6eAd1	výslovně
nevyhradila	vyhradit	k5eNaPmAgFnS	vyhradit
Unii	unie	k1gFnSc4	unie
nebo	nebo	k8xC	nebo
které	který	k3yQgFnPc4	který
výslovně	výslovně	k6eAd1	výslovně
z	z	k7c2	z
pravomoci	pravomoc	k1gFnSc2	pravomoc
států	stát	k1gInPc2	stát
nevyjmula	vyjmout	k5eNaPmAgNnP	vyjmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Další	další	k2eAgInPc1d1	další
dodatky	dodatek	k1gInPc1	dodatek
(	(	kIx(	(
<g/>
XI	XI	kA	XI
až	až	k8xS	až
XXVII	XXVII	kA	XXVII
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Dodatky	dodatek	k1gInPc1	dodatek
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
následovaly	následovat	k5eAaImAgInP	následovat
po	po	k7c4	po
přijetí	přijetí	k1gNnSc4	přijetí
Listiny	listina	k1gFnSc2	listina
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
mnoho	mnoho	k4c4	mnoho
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
17	[number]	k4	17
dodatků	dodatek	k1gInPc2	dodatek
jsou	být	k5eAaImIp3nP	být
občanské	občanský	k2eAgFnPc1d1	občanská
a	a	k8xC	a
politické	politický	k2eAgFnPc1d1	politická
svobody	svoboda	k1gFnPc1	svoboda
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
několik	několik	k4yIc1	několik
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
také	také	k9	také
pozměňuje	pozměňovat	k5eAaImIp3nS	pozměňovat
základní	základní	k2eAgFnSc4d1	základní
konstrukci	konstrukce	k1gFnSc4	konstrukce
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
schváleno	schválit	k5eAaPmNgNnS	schválit
27	[number]	k4	27
dodatků	dodatek	k1gInPc2	dodatek
<g/>
,	,	kIx,	,
uplatňováno	uplatňovat	k5eAaImNgNnS	uplatňovat
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
26	[number]	k4	26
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
21	[number]	k4	21
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
zrušil	zrušit	k5eAaPmAgInS	zrušit
platnost	platnost	k1gFnSc4	platnost
dodatku	dodatek	k1gInSc2	dodatek
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedenáctý	jedenáctý	k4xOgInSc1	jedenáctý
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
ratifikován	ratifikován	k2eAgInSc1d1	ratifikován
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
vyjasňuje	vyjasňovat	k5eAaImIp3nS	vyjasňovat
pravomoci	pravomoc	k1gFnPc4	pravomoc
soudů	soud	k1gInPc2	soud
nad	nad	k7c7	nad
cizími	cizí	k2eAgMnPc7d1	cizí
státními	státní	k2eAgMnPc7d1	státní
příslušníky	příslušník	k1gMnPc7	příslušník
a	a	k8xC	a
omezuje	omezovat	k5eAaImIp3nS	omezovat
možnosti	možnost	k1gFnPc4	možnost
občana	občan	k1gMnSc2	občan
jednoho	jeden	k4xCgMnSc4	jeden
státu	stát	k1gInSc3	stát
žalovat	žalovat	k5eAaImF	žalovat
jiný	jiný	k2eAgInSc4d1	jiný
stát	stát	k1gInSc4	stát
u	u	k7c2	u
federálních	federální	k2eAgInPc2d1	federální
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
<g/>
Dvanáctý	dvanáctý	k4xOgInSc1	dvanáctý
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
mění	měnit	k5eAaImIp3nS	měnit
způsob	způsob	k1gInSc4	způsob
průběhu	průběh	k1gInSc2	průběh
volby	volba	k1gFnSc2	volba
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
volitelé	volitel	k1gMnPc1	volitel
mají	mít	k5eAaImIp3nP	mít
nadále	nadále	k6eAd1	nadále
odevzdávat	odevzdávat	k5eAaImF	odevzdávat
hlasovací	hlasovací	k2eAgInPc4d1	hlasovací
lístky	lístek	k1gInPc4	lístek
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
prezidenta	prezident	k1gMnSc4	prezident
a	a	k8xC	a
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
(	(	kIx(	(
<g/>
namísto	namísto	k7c2	namísto
dvou	dva	k4xCgInPc2	dva
hlasů	hlas	k1gInPc2	hlas
pro	pro	k7c4	pro
prezidenta	prezident	k1gMnSc4	prezident
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
osoba	osoba	k1gFnSc1	osoba
s	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
obdržených	obdržený	k2eAgInPc2d1	obdržený
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
nesplňují	splňovat	k5eNaImIp3nP	splňovat
ústavní	ústavní	k2eAgInPc1d1	ústavní
požadavky	požadavek	k1gInPc1	požadavek
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
nejsou	být	k5eNaImIp3nP	být
nadále	nadále	k6eAd1	nadále
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
způsobilé	způsobilý	k2eAgNnSc4d1	způsobilé
ani	ani	k8xC	ani
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
funkce	funkce	k1gFnSc2	funkce
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
nezískal	získat	k5eNaPmAgMnS	získat
většinu	většina	k1gFnSc4	většina
<g/>
,	,	kIx,	,
zvolí	zvolit	k5eAaPmIp3nS	zvolit
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
kandidátů	kandidát	k1gMnPc2	kandidát
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
dřívějších	dřívější	k2eAgInPc2d1	dřívější
pěti	pět	k4xCc6	pět
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
využito	využít	k5eAaPmNgNnS	využít
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
Johna	John	k1gMnSc2	John
Q.	Q.	kA	Q.
Adamse	Adamse	k1gFnSc2	Adamse
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Nastane	nastat	k5eAaPmIp3nS	nastat
<g/>
-li	i	k?	-li
taková	takový	k3xDgNnPc4	takový
situace	situace	k1gFnPc4	situace
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
<g/>
,	,	kIx,	,
zvolí	zvolit	k5eAaPmIp3nS	zvolit
ho	on	k3xPp3gInSc4	on
Senát	senát	k1gInSc4	senát
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
kandidátů	kandidát	k1gMnPc2	kandidát
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
sněmovna	sněmovna	k1gFnSc1	sněmovna
nezvolila	zvolit	k5eNaPmAgFnS	zvolit
prezidenta	prezident	k1gMnSc4	prezident
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
počátek	počátek	k1gInSc4	počátek
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
převezme	převzít	k5eAaPmIp3nS	převzít
jeho	jeho	k3xOp3gFnPc4	jeho
pravomoci	pravomoc	k1gFnPc4	pravomoc
zvolený	zvolený	k2eAgMnSc1d1	zvolený
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
prezidenta	prezident	k1gMnSc2	prezident
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc2	jeho
neschopnosti	neschopnost	k1gFnSc2	neschopnost
vykonávat	vykonávat	k5eAaImF	vykonávat
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
změno	změna	k1gFnSc5	změna
ve	v	k7c6	v
20	[number]	k4	20
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dodatku	dodatek	k1gInSc2	dodatek
<g/>
,	,	kIx,	,
oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
prezident	prezident	k1gMnSc1	prezident
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
(	(	kIx(	(
<g/>
nový	nový	k2eAgInSc1d1	nový
počátek	počátek	k1gInSc1	počátek
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
období	období	k1gNnSc2	období
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
danou	daný	k2eAgFnSc4d1	daná
dobu	doba	k1gFnSc4	doba
není	být	k5eNaImIp3nS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
ani	ani	k8xC	ani
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
určí	určit	k5eAaPmIp3nS	určit
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
převezme	převzít	k5eAaPmIp3nS	převzít
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
zákonem	zákon	k1gInSc7	zákon
Kongres	kongres	k1gInSc1	kongres
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Takové	takový	k3xDgNnSc1	takový
převedení	převedení	k1gNnSc1	převedení
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
pravomocí	pravomoc	k1gFnPc2	pravomoc
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
dočasné	dočasný	k2eAgNnSc1d1	dočasné
a	a	k8xC	a
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
platnost	platnost	k1gFnSc4	platnost
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
řádného	řádný	k2eAgMnSc2d1	řádný
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
<g/>
Třináctý	třináctý	k4xOgInSc1	třináctý
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
ruší	rušit	k5eAaImIp3nS	rušit
otrokářský	otrokářský	k2eAgInSc4d1	otrokářský
systém	systém	k1gInSc4	systém
a	a	k8xC	a
pověřuje	pověřovat	k5eAaImIp3nS	pověřovat
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
schválil	schválit	k5eAaPmAgInS	schválit
příslušné	příslušný	k2eAgInPc4d1	příslušný
zákony	zákon	k1gInPc4	zákon
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
tohoto	tento	k3xDgNnSc2	tento
ustanovení	ustanovení	k1gNnSc2	ustanovení
<g/>
.	.	kIx.	.
<g/>
Čtrnáctý	čtrnáctý	k4xOgInSc1	čtrnáctý
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc1	pět
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
definuje	definovat	k5eAaBmIp3nS	definovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
občanem	občan	k1gMnSc7	občan
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
všechny	všechen	k3xTgFnPc4	všechen
osoby	osoba	k1gFnPc4	osoba
narozené	narozený	k2eAgFnPc4d1	narozená
nebo	nebo	k8xC	nebo
naturalizované	naturalizovaný	k2eAgFnPc4d1	naturalizovaná
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
podřízené	podřízená	k1gFnSc6	podřízená
jejich	jejich	k3xOp3gFnSc4	jejich
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
předchozí	předchozí	k2eAgNnSc4d1	předchozí
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgMnSc4	jenž
nebyli	být	k5eNaImAgMnP	být
za	za	k7c4	za
občany	občan	k1gMnPc4	občan
považováni	považován	k2eAgMnPc1d1	považován
černoši	černoch	k1gMnPc1	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
také	také	k9	také
nesmí	smět	k5eNaImIp3nS	smět
žádný	žádný	k3yNgInSc4	žádný
stát	stát	k1gInSc4	stát
omezovat	omezovat	k5eAaImF	omezovat
svobody	svoboda	k1gFnSc2	svoboda
nebo	nebo	k8xC	nebo
výsady	výsada	k1gFnSc2	výsada
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
zbavovat	zbavovat	k5eAaImF	zbavovat
je	být	k5eAaImIp3nS	být
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc2d1	osobní
svobody	svoboda	k1gFnSc2	svoboda
nebo	nebo	k8xC	nebo
majetku	majetek	k1gInSc2	majetek
bez	bez	k7c2	bez
řádného	řádný	k2eAgInSc2d1	řádný
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
množství	množství	k1gNnSc1	množství
zastupitelů	zastupitel	k1gMnPc2	zastupitel
každého	každý	k3xTgInSc2	každý
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
nadále	nadále	k6eAd1	nadále
odvíjet	odvíjet	k5eAaImF	odvíjet
od	od	k7c2	od
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc4	jenž
byli	být	k5eAaImAgMnP	být
vyjmuti	vyjmut	k2eAgMnPc1d1	vyjmut
pouze	pouze	k6eAd1	pouze
Indiáni	Indián	k1gMnPc1	Indián
neplatící	platící	k2eNgFnSc2d1	neplatící
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
zrušil	zrušit	k5eAaPmAgMnS	zrušit
ustanovení	ustanovení	k1gNnSc2	ustanovení
článku	článek	k1gInSc2	článek
I	i	k8xC	i
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
lidé	člověk	k1gMnPc1	člověk
dělili	dělit	k5eAaImAgMnP	dělit
na	na	k7c4	na
svobodné	svobodný	k2eAgFnPc4d1	svobodná
a	a	k8xC	a
"	"	kIx"	"
<g/>
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
otroky	otrok	k1gMnPc7	otrok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
započítávali	započítávat	k5eAaImAgMnP	započítávat
jen	jen	k9	jen
jako	jako	k9	jako
tři	tři	k4xCgFnPc4	tři
pětiny	pětina	k1gFnPc4	pětina
obyvatele	obyvatel	k1gMnSc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
nějaký	nějaký	k3yIgInSc4	nějaký
stát	stát	k1gInSc4	stát
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
některým	některý	k3yIgMnPc3	některý
obyvatelům	obyvatel	k1gMnPc3	obyvatel
právo	právo	k1gNnSc4	právo
volit	volit	k5eAaImF	volit
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
příslušným	příslušný	k2eAgInSc7d1	příslušný
způsobem	způsob	k1gInSc7	způsob
zmenšilo	zmenšit	k5eAaPmAgNnS	zmenšit
také	také	k9	také
jeho	jeho	k3xOp3gNnSc1	jeho
zastoupení	zastoupení	k1gNnSc1	zastoupení
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
zvolení	zvolení	k1gNnSc4	zvolení
do	do	k7c2	do
Kongresu	kongres	k1gInSc2	kongres
osobám	osoba	k1gFnPc3	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
složily	složit	k5eAaPmAgFnP	složit
přísahu	přísaha	k1gFnSc4	přísaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
dodržovat	dodržovat	k5eAaImF	dodržovat
Ústavu	ústav	k1gInSc3	ústav
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
provinily	provinit	k5eAaPmAgFnP	provinit
účastí	účast	k1gFnSc7	účast
na	na	k7c4	na
povstání	povstání	k1gNnSc4	povstání
nebo	nebo	k8xC	nebo
vzpourou	vzpoura	k1gFnSc7	vzpoura
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
Ústavě	ústava	k1gFnSc3	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
takové	takový	k3xDgNnSc4	takový
omezení	omezení	k1gNnSc4	omezení
práv	právo	k1gNnPc2	právo
dvěma	dva	k4xCgFnPc7	dva
třetinami	třetina	k1gFnPc7	třetina
hlasů	hlas	k1gInPc2	hlas
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
4	[number]	k4	4
reagoval	reagovat	k5eAaBmAgInS	reagovat
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
je	být	k5eAaImIp3nS	být
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
závaznost	závaznost	k1gFnSc1	závaznost
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
včetně	včetně	k7c2	včetně
dluhů	dluh	k1gInPc2	dluh
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
plateb	platba	k1gFnPc2	platba
za	za	k7c4	za
služby	služba	k1gFnPc4	služba
vykonané	vykonaný	k2eAgFnPc4d1	vykonaná
při	při	k7c6	při
potlačení	potlačení	k1gNnSc6	potlačení
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
odmítnuty	odmítnut	k2eAgInPc4d1	odmítnut
dluhy	dluh	k1gInPc4	dluh
učiněné	učiněný	k2eAgInPc4d1	učiněný
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
podpory	podpora	k1gFnSc2	podpora
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
a	a	k8xC	a
současně	současně	k6eAd1	současně
jsou	být	k5eAaImIp3nP	být
odmítnuty	odmítnut	k2eAgFnPc1d1	odmítnuta
výplaty	výplata	k1gFnPc1	výplata
náhrad	náhrada	k1gFnPc2	náhrada
za	za	k7c4	za
ztráty	ztráta	k1gFnPc4	ztráta
či	či	k8xC	či
osvobození	osvobození	k1gNnSc4	osvobození
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
5	[number]	k4	5
pověřuje	pověřovat	k5eAaImIp3nS	pověřovat
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ustanovení	ustanovení	k1gNnSc1	ustanovení
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
naplnil	naplnit	k5eAaPmAgInS	naplnit
přijetím	přijetí	k1gNnSc7	přijetí
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patnáctý	patnáctý	k4xOgInSc1	patnáctý
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
federální	federální	k2eAgFnSc1d1	federální
vládě	vláda	k1gFnSc3	vláda
i	i	k8xC	i
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
státům	stát	k1gInPc3	stát
podmiňovat	podmiňovat	k5eAaImF	podmiňovat
volební	volební	k2eAgNnSc1d1	volební
právo	právo	k1gNnSc1	právo
rasou	rasa	k1gFnSc7	rasa
<g/>
,	,	kIx,	,
barvou	barva	k1gFnSc7	barva
pleti	pleť	k1gFnSc2	pleť
nebo	nebo	k8xC	nebo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
osoba	osoba	k1gFnSc1	osoba
byla	být	k5eAaImAgFnS	být
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
otroka	otrok	k1gMnSc2	otrok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
pověřuje	pověřovat	k5eAaImIp3nS	pověřovat
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ustanovení	ustanovení	k1gNnSc1	ustanovení
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
naplnil	naplnit	k5eAaPmAgInS	naplnit
přijetím	přijetí	k1gNnSc7	přijetí
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
<g/>
Šestnáctý	šestnáctý	k4xOgInSc1	šestnáctý
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
dává	dávat	k5eAaImIp3nS	dávat
Kongresu	kongres	k1gInSc2	kongres
právo	právo	k1gNnSc4	právo
nerozdělovat	rozdělovat	k5eNaImF	rozdělovat
mezi	mezi	k7c4	mezi
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
státy	stát	k1gInPc4	stát
vybrané	vybraný	k2eAgFnSc2d1	vybraná
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
zdroj	zdroj	k1gInSc4	zdroj
a	a	k8xC	a
na	na	k7c4	na
výsledky	výsledek	k1gInPc4	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
<g/>
Sedmnáctý	sedmnáctý	k4xOgInSc1	sedmnáctý
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
změnil	změnit	k5eAaPmAgInS	změnit
způsob	způsob	k1gInSc1	způsob
volby	volba	k1gFnSc2	volba
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
I	i	k8xC	i
oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
původně	původně	k6eAd1	původně
stanovoval	stanovovat	k5eAaImAgMnS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
senátory	senátor	k1gMnPc4	senátor
za	za	k7c4	za
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
státy	stát	k1gInPc4	stát
volí	volit	k5eAaImIp3nP	volit
zákonodárné	zákonodárný	k2eAgInPc1d1	zákonodárný
sbory	sbor	k1gInPc1	sbor
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
17	[number]	k4	17
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
jsou	být	k5eAaImIp3nP	být
senátoři	senátor	k1gMnPc1	senátor
nadále	nadále	k6eAd1	nadále
voleni	volit	k5eAaImNgMnP	volit
v	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
hlasování	hlasování	k1gNnSc6	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
procedura	procedura	k1gFnSc1	procedura
zaplňování	zaplňování	k1gNnSc2	zaplňování
uvolněných	uvolněný	k2eAgNnPc2d1	uvolněné
míst	místo	k1gNnPc2	místo
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
novému	nový	k2eAgInSc3d1	nový
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
místo	místo	k1gNnSc1	místo
senátora	senátor	k1gMnSc2	senátor
<g/>
,	,	kIx,	,
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
orgán	orgán	k1gInSc1	orgán
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
<g/>
,	,	kIx,	,
zvolil	zvolit	k5eAaPmAgInS	zvolit
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
nejbližším	blízký	k2eAgNnSc6d3	nejbližší
zasedání	zasedání	k1gNnSc6	zasedání
nového	nový	k2eAgMnSc2d1	nový
senátora	senátor	k1gMnSc2	senátor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
guvernér	guvernér	k1gMnSc1	guvernér
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
mohl	moct	k5eAaImAgInS	moct
někoho	někdo	k3yInSc4	někdo
sám	sám	k3xTgInSc1	sám
jmenovat	jmenovat	k5eAaImF	jmenovat
na	na	k7c4	na
překlenutí	překlenutí	k1gNnSc4	překlenutí
období	období	k1gNnSc2	období
zbývajícího	zbývající	k2eAgNnSc2d1	zbývající
do	do	k7c2	do
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
pověřuje	pověřovat	k5eAaImIp3nS	pověřovat
zákonodárné	zákonodárný	k2eAgInPc4d1	zákonodárný
orgány	orgán	k1gInPc4	orgán
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
případě	případ	k1gInSc6	případ
uvolnění	uvolnění	k1gNnSc2	uvolnění
senátorského	senátorský	k2eAgNnSc2d1	senátorské
místa	místo	k1gNnSc2	místo
vypsaly	vypsat	k5eAaPmAgFnP	vypsat
lidové	lidový	k2eAgFnPc1d1	lidová
volby	volba	k1gFnPc1	volba
a	a	k8xC	a
případně	případně	k6eAd1	případně
takového	takový	k3xDgMnSc4	takový
senátora	senátor	k1gMnSc4	senátor
dočasně	dočasně	k6eAd1	dočasně
jmenovaly	jmenovat	k5eAaBmAgInP	jmenovat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
znám	znám	k2eAgInSc1d1	znám
výsledek	výsledek	k1gInSc1	výsledek
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
<g/>
Osmnáctý	osmnáctý	k4xOgInSc1	osmnáctý
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
oddíly	oddíl	k1gInPc1	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
prodej	prodej	k1gInSc4	prodej
<g/>
,	,	kIx,	,
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
dovoz	dovoz	k1gInSc4	dovoz
i	i	k8xC	i
vývoz	vývoz	k1gInSc4	vývoz
omamných	omamný	k2eAgInPc2d1	omamný
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
pověřuje	pověřovat	k5eAaImIp3nS	pověřovat
Kongres	kongres	k1gInSc4	kongres
i	i	k9	i
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ustanovení	ustanovení	k1gNnSc1	ustanovení
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
naplnily	naplnit	k5eAaPmAgInP	naplnit
přijetím	přijetí	k1gNnSc7	přijetí
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
do	do	k7c2	do
kdy	kdy	k6eAd1	kdy
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ratifikován	ratifikovat	k5eAaBmNgInS	ratifikovat
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
nenabude	nabýt	k5eNaPmIp3nS	nabýt
platnosti	platnost	k1gFnSc3	platnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
od	od	k7c2	od
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Kongresem	kongres	k1gInSc7	kongres
předložen	předložit	k5eAaPmNgInS	předložit
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
dodatek	dodatek	k1gInSc1	dodatek
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc6	první
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
ratifikace	ratifikace	k1gFnSc1	ratifikace
byla	být	k5eAaImAgFnS	být
časově	časově	k6eAd1	časově
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgInPc1d1	dřívější
návrhy	návrh	k1gInPc1	návrh
dodatků	dodatek	k1gInPc2	dodatek
takové	takový	k3xDgNnSc4	takový
omezení	omezení	k1gNnSc4	omezení
nemívaly	mívat	k5eNaImAgFnP	mívat
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
neprošly	projít	k5eNaPmAgInP	projít
ratifikačním	ratifikační	k2eAgInSc7d1	ratifikační
procesem	proces	k1gInSc7	proces
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
teoreticky	teoreticky	k6eAd1	teoreticky
ratifikovány	ratifikován	k2eAgFnPc1d1	ratifikována
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Platnost	platnost	k1gFnSc1	platnost
ustanovení	ustanovení	k1gNnPc2	ustanovení
18	[number]	k4	18
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
přijetím	přijetí	k1gNnSc7	přijetí
21	[number]	k4	21
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Devatenáctý	devatenáctý	k4xOgInSc1	devatenáctý
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
federální	federální	k2eAgFnSc1d1	federální
vládě	vláda	k1gFnSc3	vláda
i	i	k8xC	i
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
státům	stát	k1gInPc3	stát
omezovat	omezovat	k5eAaImF	omezovat
občanům	občan	k1gMnPc3	občan
výkon	výkon	k1gInSc4	výkon
jejich	jejich	k3xOp3gNnSc2	jejich
hlasovacího	hlasovací	k2eAgNnSc2d1	hlasovací
práva	právo	k1gNnSc2	právo
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
pověřuje	pověřovat	k5eAaImIp3nS	pověřovat
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ustanovení	ustanovení	k1gNnSc1	ustanovení
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
naplnil	naplnit	k5eAaPmAgInS	naplnit
přijetím	přijetí	k1gNnSc7	přijetí
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
<g/>
Dvacátý	dvacátý	k4xOgInSc1	dvacátý
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
mění	měnit	k5eAaImIp3nS	měnit
některé	některý	k3yIgFnPc4	některý
podrobnosti	podrobnost	k1gFnPc4	podrobnost
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
funkčních	funkční	k2eAgInPc2d1	funkční
období	období	k1gNnSc4	období
členů	člen	k1gMnPc2	člen
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
a	a	k8xC	a
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
mění	měnit	k5eAaImIp3nS	měnit
datum	datum	k1gNnSc4	datum
konce	konec	k1gInSc2	konec
některých	některý	k3yIgNnPc2	některý
funkčních	funkční	k2eAgNnPc2d1	funkční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
končí	končit	k5eAaImIp3nS	končit
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
ve	v	k7c4	v
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
členů	člen	k1gInPc2	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gInPc2	reprezentant
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
ve	v	k7c4	v
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
;	;	kIx,	;
rok	rok	k1gInSc1	rok
konce	konec	k1gInSc2	konec
těchto	tento	k3xDgNnPc2	tento
volebních	volební	k2eAgNnPc2d1	volební
období	období	k1gNnPc2	období
nebyl	být	k5eNaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
vyplýval	vyplývat	k5eAaImAgInS	vyplývat
z	z	k7c2	z
příslušných	příslušný	k2eAgNnPc2d1	příslušné
ustanovení	ustanovení	k1gNnPc2	ustanovení
článků	článek	k1gInPc2	článek
I	i	k8xC	i
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
jejich	jejich	k3xOp3gMnPc2	jejich
nástupců	nástupce	k1gMnPc2	nástupce
začíná	začínat	k5eAaImIp3nS	začínat
stejným	stejný	k2eAgInSc7d1	stejný
okamžikem	okamžik	k1gInSc7	okamžik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kongres	kongres	k1gInSc1	kongres
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
sejít	sejít	k5eAaPmF	sejít
nejméně	málo	k6eAd3	málo
jedenkrát	jedenkrát	k6eAd1	jedenkrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
nestanoví	stanovit	k5eNaPmIp3nS	stanovit
jiné	jiný	k2eAgNnSc4d1	jiné
datum	datum	k1gNnSc4	datum
počátku	počátek	k1gInSc2	počátek
svého	svůj	k3xOyFgNnSc2	svůj
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
tak	tak	k9	tak
vždy	vždy	k6eAd1	vždy
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
prezidentským	prezidentský	k2eAgNnSc7d1	prezidentské
nástupnictvím	nástupnictví	k1gNnSc7	nástupnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
smrti	smrt	k1gFnSc2	smrt
prezidenta	prezident	k1gMnSc2	prezident
se	se	k3xPyFc4	se
novým	nový	k2eAgMnSc7d1	nový
prezidentem	prezident	k1gMnSc7	prezident
stane	stanout	k5eAaPmIp3nS	stanout
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Viceprezident	viceprezident	k1gMnSc1	viceprezident
také	také	k9	také
dočasně	dočasně	k6eAd1	dočasně
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
tento	tento	k3xDgInSc1	tento
nebyl	být	k5eNaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
nebo	nebo	k8xC	nebo
ještě	ještě	k6eAd1	ještě
nepřevzal	převzít	k5eNaPmAgMnS	převzít
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
nepřevzal	převzít	k5eNaPmAgMnS	převzít
ani	ani	k8xC	ani
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
ani	ani	k8xC	ani
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
určí	určit	k5eAaPmIp3nS	určit
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
ujme	ujmout	k5eAaPmIp3nS	ujmout
funkce	funkce	k1gFnSc1	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
zákonem	zákon	k1gInSc7	zákon
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
opět	opět	k6eAd1	opět
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
ujme	ujmout	k5eAaPmIp3nS	ujmout
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
nebo	nebo	k8xC	nebo
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
4	[number]	k4	4
dává	dávat	k5eAaImIp3nS	dávat
Kongresu	kongres	k1gInSc2	kongres
pravomoc	pravomoc	k1gFnSc4	pravomoc
vyřešit	vyřešit	k5eAaPmF	vyřešit
zákonem	zákon	k1gInSc7	zákon
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
měla	mít	k5eAaImAgFnS	mít
volit	volit	k5eAaImF	volit
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
přešlo	přejít	k5eAaPmAgNnS	přejít
takové	takový	k3xDgNnSc1	takový
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
i	i	k8xC	i
případ	případ	k1gInSc4	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
měl	mít	k5eAaImAgMnS	mít
volit	volit	k5eAaImF	volit
Senát	senát	k1gInSc4	senát
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
přešlo	přejít	k5eAaPmAgNnS	přejít
takové	takový	k3xDgNnSc1	takový
právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
12	[number]	k4	12
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
5	[number]	k4	5
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oddíly	oddíl	k1gInPc1	oddíl
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
nabudou	nabýt	k5eAaPmIp3nP	nabýt
účinnosti	účinnost	k1gFnPc1	účinnost
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
po	po	k7c6	po
ratifikaci	ratifikace	k1gFnSc6	ratifikace
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
6	[number]	k4	6
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
nejzazší	zadní	k2eAgInSc4d3	nejzazší
termín	termín	k1gInSc4	termín
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
dodatku	dodatek	k1gInSc2	dodatek
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Dvacátý	dvacátý	k4xOgInSc4	dvacátý
první	první	k4xOgInSc4	první
dodatek	dodatek	k1gInSc4	dodatek
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
ruší	rušit	k5eAaImIp3nS	rušit
18	[number]	k4	18
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
dává	dávat	k5eAaImIp3nS	dávat
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
státům	stát	k1gInPc3	stát
možnost	možnost	k1gFnSc4	možnost
zakázat	zakázat	k5eAaPmF	zakázat
převoz	převoz	k1gInSc4	převoz
či	či	k8xC	či
dovoz	dovoz	k1gInSc4	dovoz
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
území	území	k1gNnSc1	území
vlastními	vlastní	k2eAgInPc7d1	vlastní
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
nejzazší	zadní	k2eAgInSc4d3	nejzazší
termín	termín	k1gInSc4	termín
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
dodatku	dodatek	k1gInSc2	dodatek
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Dvacátý	dvacátý	k4xOgInSc4	dvacátý
druhý	druhý	k4xOgInSc4	druhý
dodatek	dodatek	k1gInSc4	dodatek
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nějaká	nějaký	k3yIgFnSc1	nějaký
osoba	osoba	k1gFnSc1	osoba
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
během	během	k7c2	během
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
jiná	jiný	k2eAgFnSc1d1	jiná
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
již	již	k6eAd1	již
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
se	se	k3xPyFc4	se
však	však	k9	však
výslovně	výslovně	k6eAd1	výslovně
netýkalo	týkat	k5eNaImAgNnS	týkat
prezidenta	prezident	k1gMnSc4	prezident
úřadujícího	úřadující	k2eAgMnSc4d1	úřadující
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnSc1	jeho
navržení	navržení	k1gNnSc1	navržení
Kongresem	kongres	k1gInSc7	kongres
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
prezidentu	prezident	k1gMnSc3	prezident
Harrymu	Harrym	k1gInSc2	Harrym
S.	S.	kA	S.
Trumanovi	Truman	k1gMnSc6	Truman
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
Franklina	Franklin	k2eAgFnSc1d1	Franklina
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc4	Roosevelt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
nakonec	nakonec	k6eAd1	nakonec
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
)	)	kIx)	)
kandidovat	kandidovat	k5eAaImF	kandidovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
nejzazší	zadní	k2eAgInSc4d3	nejzazší
termín	termín	k1gInSc4	termín
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
dodatku	dodatek	k1gInSc2	dodatek
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Dvacátý	dvacátý	k4xOgInSc4	dvacátý
třetí	třetí	k4xOgInSc4	třetí
dodatek	dodatek	k1gInSc4	dodatek
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
své	své	k1gNnSc4	své
volitele	volitel	k1gMnSc2	volitel
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
také	také	k9	také
District	District	k1gMnSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
okrsek	okrsek	k1gInSc1	okrsek
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
leží	ležet	k5eAaImIp3nS	ležet
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
součtu	součet	k1gInSc6	součet
senátorů	senátor	k1gMnPc2	senátor
a	a	k8xC	a
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
nárok	nárok	k1gInSc4	nárok
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
volitelů	volitel	k1gMnPc2	volitel
státu	stát	k1gInSc2	stát
s	s	k7c7	s
nejmenším	malý	k2eAgInSc7d3	nejmenší
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
nejzazší	zadní	k2eAgInSc4d3	nejzazší
termín	termín	k1gInSc4	termín
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
dodatku	dodatek	k1gInSc2	dodatek
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Dvacátý	dvacátý	k4xOgInSc4	dvacátý
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
dodatek	dodatek	k1gInSc4	dodatek
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
federální	federální	k2eAgFnSc1d1	federální
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podmiňovala	podmiňovat	k5eAaImAgFnS	podmiňovat
výkon	výkon	k1gInSc4	výkon
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
zaplacením	zaplacení	k1gNnSc7	zaplacení
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
volební	volební	k2eAgFnSc2d1	volební
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
opravňuje	opravňovat	k5eAaImIp3nS	opravňovat
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ustanovení	ustanovení	k1gNnSc1	ustanovení
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
naplnil	naplnit	k5eAaPmAgInS	naplnit
přijetím	přijetí	k1gNnSc7	přijetí
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvacátý	dvacátý	k4xOgInSc4	dvacátý
pátý	pátý	k4xOgInSc4	pátý
dodatek	dodatek	k1gInSc4	dodatek
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
pozměňuje	pozměňovat	k5eAaImIp3nS	pozměňovat
některé	některý	k3yIgFnPc4	některý
záležitosti	záležitost	k1gFnPc4	záležitost
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
a	a	k8xC	a
zastupování	zastupování	k1gNnSc2	zastupování
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
byl	být	k5eAaImAgMnS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
nebo	nebo	k8xC	nebo
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
úřadující	úřadující	k2eAgMnSc1d1	úřadující
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
úřad	úřad	k1gInSc1	úřad
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
nového	nový	k2eAgInSc2d1	nový
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
schválen	schválit	k5eAaPmNgInS	schválit
většinou	většina	k1gFnSc7	většina
hlasů	hlas	k1gInPc2	hlas
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
3	[number]	k4	3
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
prezidentovi	prezident	k1gMnSc3	prezident
dočasně	dočasně	k6eAd1	dočasně
odstoupit	odstoupit	k5eAaPmF	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
učinit	učinit	k5eAaImF	učinit
předáním	předání	k1gNnSc7	předání
písemného	písemný	k2eAgNnSc2d1	písemné
prohlášení	prohlášení	k1gNnSc2	prohlášení
prozatímnímu	prozatímní	k2eAgMnSc3d1	prozatímní
předsedovi	předseda	k1gMnSc3	předseda
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
předsedovi	předseda	k1gMnSc3	předseda
Sněmovny	sněmovna	k1gFnPc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
způsobilý	způsobilý	k2eAgMnSc1d1	způsobilý
vykonávat	vykonávat	k5eAaImF	vykonávat
pravomoci	pravomoc	k1gFnPc4	pravomoc
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
stává	stávat	k5eAaImIp3nS	stávat
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
nepředá	předat	k5eNaPmIp3nS	předat
zmíněným	zmíněný	k2eAgMnPc3d1	zmíněný
předsedům	předseda	k1gMnPc3	předseda
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvody	důvod	k1gInPc1	důvod
jeho	on	k3xPp3gNnSc2	on
dočasného	dočasný	k2eAgNnSc2d1	dočasné
odstoupení	odstoupení	k1gNnSc2	odstoupení
pominuly	pominout	k5eAaPmAgInP	pominout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
4	[number]	k4	4
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
dočasně	dočasně	k6eAd1	dočasně
zbaven	zbavit	k5eAaPmNgMnS	zbavit
úřadu	úřad	k1gInSc3	úřad
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
prohlášení	prohlášení	k1gNnSc1	prohlášení
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
nezpůsobilosti	nezpůsobilost	k1gFnSc6	nezpůsobilost
nepředá	předat	k5eNaPmIp3nS	předat
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
učiní	učinit	k5eAaImIp3nS	učinit
tak	tak	k9	tak
viceprezident	viceprezident	k1gMnSc1	viceprezident
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
buď	buď	k8xC	buď
většina	většina	k1gFnSc1	většina
vedoucích	vedoucí	k2eAgFnPc2d1	vedoucí
sekcí	sekce	k1gFnPc2	sekce
exekutivy	exekutiva	k1gFnSc2	exekutiva
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
orgánu	orgán	k1gInSc2	orgán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
Kongres	kongres	k1gInSc4	kongres
zřídit	zřídit	k5eAaPmF	zřídit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
opět	opět	k6eAd1	opět
stává	stávat	k5eAaImIp3nS	stávat
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
předá	předat	k5eAaPmIp3nS	předat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvody	důvod	k1gInPc1	důvod
jeho	jeho	k3xOp3gFnSc2	jeho
nezpůsobilosti	nezpůsobilost	k1gFnSc2	nezpůsobilost
pominuly	pominout	k5eAaPmAgInP	pominout
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
ujmout	ujmout	k5eAaPmF	ujmout
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
viceprezident	viceprezident	k1gMnSc1	viceprezident
s	s	k7c7	s
vedoucími	vedoucí	k1gMnPc7	vedoucí
sekcí	sekce	k1gFnPc2	sekce
exekutivy	exekutiva	k1gFnSc2	exekutiva
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
orgánu	orgán	k1gInSc2	orgán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
Kongres	kongres	k1gInSc1	kongres
zřídit	zřídit	k5eAaPmF	zřídit
<g/>
,	,	kIx,	,
nepředá	předat	k5eNaPmIp3nS	předat
opětovné	opětovný	k2eAgNnSc1d1	opětovné
prohlášení	prohlášení	k1gNnSc1	prohlášení
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
nezpůsobilosti	nezpůsobilost	k1gFnSc6	nezpůsobilost
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
spor	spor	k1gInSc1	spor
potom	potom	k6eAd1	potom
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ke	k	k7c3	k
zbavení	zbavení	k1gNnSc3	zbavení
prezidenta	prezident	k1gMnSc2	prezident
způsobilosti	způsobilost	k1gFnSc2	způsobilost
vykonávat	vykonávat	k5eAaImF	vykonávat
úřad	úřad	k1gInSc4	úřad
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
dvoutřetinové	dvoutřetinový	k2eAgFnSc2d1	dvoutřetinová
většiny	většina	k1gFnSc2	většina
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
jeho	jeho	k3xOp3gFnPc6	jeho
komorách	komora	k1gFnPc6	komora
<g/>
.	.	kIx.	.
<g/>
Dvacátý	dvacátý	k4xOgInSc4	dvacátý
šestý	šestý	k4xOgInSc4	šestý
dodatek	dodatek	k1gInSc4	dodatek
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
1	[number]	k4	1
snižuje	snižovat	k5eAaImIp3nS	snižovat
hranici	hranice	k1gFnSc4	hranice
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
18	[number]	k4	18
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
(	(	kIx(	(
<g/>
namísto	namísto	k7c2	namísto
předchozích	předchozí	k2eAgNnPc2d1	předchozí
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
2	[number]	k4	2
opravňuje	opravňovat	k5eAaImIp3nS	opravňovat
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ustanovení	ustanovení	k1gNnSc1	ustanovení
tohoto	tento	k3xDgInSc2	tento
dodatku	dodatek	k1gInSc2	dodatek
naplnil	naplnit	k5eAaPmAgInS	naplnit
přijetím	přijetí	k1gNnSc7	přijetí
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
<g/>
Dvacátý	dvacátý	k4xOgInSc4	dvacátý
sedmý	sedmý	k4xOgInSc4	sedmý
dodatek	dodatek	k1gInSc4	dodatek
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
zvýšení	zvýšení	k1gNnSc4	zvýšení
nebo	nebo	k8xC	nebo
snížení	snížení	k1gNnSc4	snížení
náhrad	náhrada	k1gFnPc2	náhrada
pobíraných	pobíraný	k2eAgFnPc2d1	pobíraná
senátory	senátor	k1gMnPc7	senátor
nebo	nebo	k8xC	nebo
členy	člen	k1gMnPc7	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
až	až	k9	až
po	po	k7c6	po
nejbližších	blízký	k2eAgFnPc6d3	nejbližší
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc4d1	poslední
ratifikovaný	ratifikovaný	k2eAgInSc4d1	ratifikovaný
dodatek	dodatek	k1gInSc4	dodatek
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Neratifikované	ratifikovaný	k2eNgInPc4d1	ratifikovaný
dodatky	dodatek	k1gInPc4	dodatek
===	===	k?	===
</s>
</p>
<p>
<s>
Kongres	kongres	k1gInSc1	kongres
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
platnosti	platnost	k1gFnSc2	platnost
Ústavy	ústava	k1gFnSc2	ústava
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
celkem	celkem	k6eAd1	celkem
33	[number]	k4	33
dodatků	dodatek	k1gInPc2	dodatek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
6	[number]	k4	6
nebylo	být	k5eNaImAgNnS	být
ratifikováno	ratifikovat	k5eAaBmNgNnS	ratifikovat
potřebnou	potřebný	k2eAgFnSc7d1	potřebná
většinou	většina	k1gFnSc7	většina
třech	tři	k4xCgFnPc6	tři
čtvrtin	čtvrtina	k1gFnPc2	čtvrtina
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
šesti	šest	k4xCc2	šest
stále	stále	k6eAd1	stále
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
teoreticky	teoreticky	k6eAd1	teoreticky
přijaty	přijmout	k5eAaPmNgFnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
18	[number]	k4	18
<g/>
.	.	kIx.	.
dodatkem	dodatek	k1gInSc7	dodatek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
součástí	součást	k1gFnSc7	součást
navržených	navržený	k2eAgInPc2d1	navržený
dodatků	dodatek	k1gInPc2	dodatek
klauzule	klauzule	k1gFnSc2	klauzule
stanovující	stanovující	k2eAgMnSc1d1	stanovující
<g/>
,	,	kIx,	,
dokdy	dokdy	k6eAd1	dokdy
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
schválen	schválen	k2eAgMnSc1d1	schválen
<g/>
;	;	kIx,	;
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
datu	datum	k1gNnSc6	datum
ratifikace	ratifikace	k1gFnSc2	ratifikace
ukončena	ukončen	k2eAgNnPc1d1	ukončeno
jako	jako	k8xS	jako
neúspěšná	úspěšný	k2eNgNnPc1d1	neúspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
novějších	nový	k2eAgInPc2d2	novější
dodatků	dodatek	k1gInPc2	dodatek
tuto	tento	k3xDgFnSc4	tento
klauzuli	klauzule	k1gFnSc4	klauzule
neměly	mít	k5eNaImAgFnP	mít
pouze	pouze	k6eAd1	pouze
19	[number]	k4	19
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc4	dodatek
(	(	kIx(	(
<g/>
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc4	dodatek
(	(	kIx(	(
<g/>
volební	volební	k2eAgInPc4d1	volební
hlasy	hlas	k1gInPc4	hlas
pro	pro	k7c4	pro
District	District	k1gMnSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
zákaz	zákaz	k1gInSc1	zákaz
volební	volební	k2eAgFnSc2d1	volební
daně	daň	k1gFnSc2	daň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dodatek	dodatek	k1gInSc1	dodatek
(	(	kIx(	(
<g/>
prezidentské	prezidentský	k2eAgNnSc1d1	prezidentské
nástupnictví	nástupnictví	k1gNnSc1	nástupnictví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc4	dodatek
(	(	kIx(	(
<g/>
věk	věk	k1gInSc4	věk
opravňující	opravňující	k2eAgInSc4d1	opravňující
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
volbách	volba	k1gFnPc6	volba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dodatek	dodatek	k1gInSc1	dodatek
o	o	k7c6	o
dětské	dětský	k2eAgFnSc6d1	dětská
práci	práce	k1gFnSc6	práce
(	(	kIx(	(
<g/>
navržen	navržen	k2eAgInSc1d1	navržen
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
a	a	k8xC	a
stále	stále	k6eAd1	stále
neratifikován	ratifikován	k2eNgMnSc1d1	ratifikován
<g/>
)	)	kIx)	)
a	a	k8xC	a
dodatek	dodatek	k1gInSc1	dodatek
o	o	k7c6	o
rovných	rovný	k2eAgNnPc6d1	rovné
právech	právo	k1gNnPc6	právo
(	(	kIx(	(
<g/>
navržen	navržen	k2eAgInSc1d1	navržen
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
a	a	k8xC	a
stále	stále	k6eAd1	stále
neratifikován	ratifikován	k2eNgMnSc1d1	ratifikován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Neratifikované	ratifikovaný	k2eNgInPc4d1	ratifikovaný
dodatky	dodatek	k1gInPc4	dodatek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
lhůtu	lhůta	k1gFnSc4	lhůta
ratifikace	ratifikace	k1gFnSc2	ratifikace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
navržený	navržený	k2eAgInSc4d1	navržený
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1789	[number]	k4	1789
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
upřesňoval	upřesňovat	k5eAaImAgInS	upřesňovat
výpočet	výpočet	k1gInSc4	výpočet
počtu	počet	k1gInSc2	počet
členů	člen	k1gMnPc2	člen
zasedajících	zasedající	k2eAgMnPc2d1	zasedající
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
po	po	k7c6	po
každém	každý	k3xTgNnSc6	každý
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
konaném	konaný	k2eAgInSc6d1	konaný
každých	každý	k3xTgNnPc2	každý
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
nejprve	nejprve	k6eAd1	nejprve
10	[number]	k4	10
ze	z	k7c2	z
14	[number]	k4	14
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
těsně	těsně	k6eAd1	těsně
neprošel	projít	k5eNaPmAgInS	projít
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
ještě	ještě	k6eAd1	ještě
Kentucky	Kentucka	k1gFnSc2	Kentucka
(	(	kIx(	(
<g/>
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1792	[number]	k4	1792
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
získalo	získat	k5eAaPmAgNnS	získat
status	status	k1gInSc4	status
státu	stát	k1gInSc2	stát
Unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
nestačilo	stačit	k5eNaBmAgNnS	stačit
(	(	kIx(	(
<g/>
nový	nový	k2eAgInSc1d1	nový
poměr	poměr	k1gInSc1	poměr
byl	být	k5eAaImAgInS	být
11	[number]	k4	11
z	z	k7c2	z
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
o	o	k7c6	o
šlechtických	šlechtický	k2eAgInPc6d1	šlechtický
titulech	titul	k1gInPc6	titul
<g/>
,	,	kIx,	,
navržený	navržený	k2eAgInSc4d1	navržený
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1810	[number]	k4	1810
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
v	v	k7c6	v
případě	případ	k1gInSc6	případ
schválení	schválení	k1gNnSc2	schválení
znamenal	znamenat	k5eAaImAgInS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
Američan	Američan	k1gMnSc1	Američan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přijme	přijmout	k5eAaPmIp3nS	přijmout
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
čestný	čestný	k2eAgInSc4d1	čestný
titul	titul	k1gInSc4	titul
cizí	cizí	k2eAgFnSc2d1	cizí
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
amerického	americký	k2eAgNnSc2d1	americké
občanství	občanství	k1gNnSc2	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc4	dodatek
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
12	[number]	k4	12
ze	z	k7c2	z
17	[number]	k4	17
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
těsně	těsně	k6eAd1	těsně
neprošel	projít	k5eNaPmAgInS	projít
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
byl	být	k5eAaImAgInS	být
dodatek	dodatek	k1gInSc4	dodatek
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
schválen	schválen	k2eAgMnSc1d1	schválen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musel	muset	k5eAaImAgMnS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
vlivu	vliv	k1gInSc3	vliv
právníků	právník	k1gMnPc2	právník
<g/>
,	,	kIx,	,
bankéřů	bankéř	k1gMnPc2	bankéř
a	a	k8xC	a
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konspirační	konspirační	k2eAgFnSc1d1	konspirační
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
především	především	k9	především
na	na	k7c6	na
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
projednání	projednání	k1gNnSc6	projednání
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
dodatek	dodatek	k1gInSc1	dodatek
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
publikacích	publikace	k1gFnPc6	publikace
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
však	však	k9	však
byla	být	k5eAaImAgFnS	být
důkladně	důkladně	k6eAd1	důkladně
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tzv.	tzv.	kA	tzv.
Corwinův	Corwinův	k2eAgInSc4d1	Corwinův
dodatek	dodatek	k1gInSc4	dodatek
<g/>
,	,	kIx,	,
navržený	navržený	k2eAgInSc4d1	navržený
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1861	[number]	k4	1861
na	na	k7c4	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
Kongresu	kongres	k1gInSc2	kongres
by	by	kYmCp3nS	by
znamenal	znamenat	k5eAaImAgInS	znamenat
zákaz	zákaz	k1gInSc1	zákaz
jakýchkoliv	jakýkoliv	k3yIgInPc2	jakýkoliv
pokusů	pokus	k1gInPc2	pokus
přijímat	přijímat	k5eAaImF	přijímat
k	k	k7c3	k
ústavě	ústava	k1gFnSc3	ústava
dodatky	dodatek	k1gInPc1	dodatek
umožňující	umožňující	k2eAgFnSc2d1	umožňující
federální	federální	k2eAgFnSc2d1	federální
vládě	vláda	k1gFnSc3	vláda
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
zřízení	zřízení	k1gNnSc2	zřízení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
dodatku	dodatek	k1gInSc2	dodatek
tímto	tento	k3xDgInSc7	tento
hodlali	hodlat	k5eAaImAgMnP	hodlat
zachovat	zachovat	k5eAaPmF	zachovat
otroctví	otroctví	k1gNnSc4	otroctví
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc4	dodatek
stihli	stihnout	k5eAaPmAgMnP	stihnout
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
ratifikovat	ratifikovat	k5eAaBmF	ratifikovat
pouze	pouze	k6eAd1	pouze
zákonodárci	zákonodárce	k1gMnPc1	zákonodárce
států	stát	k1gInPc2	stát
Ohio	Ohio	k1gNnSc1	Ohio
a	a	k8xC	a
Maryland	Maryland	k1gInSc1	Maryland
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
schválen	schválen	k2eAgInSc1d1	schválen
také	také	k9	také
na	na	k7c6	na
ústavním	ústavní	k2eAgInSc6d1	ústavní
konventu	konvent	k1gInSc6	konvent
státu	stát	k1gInSc2	stát
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
přijaty	přijat	k2eAgInPc1d1	přijat
dodatky	dodatek	k1gInPc1	dodatek
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
úplného	úplný	k2eAgNnSc2d1	úplné
opaku	opak	k1gInSc6	opak
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
zamýšlel	zamýšlet	k5eAaImAgInS	zamýšlet
Corwinův	Corwinův	k2eAgInSc1d1	Corwinův
dodatek	dodatek	k1gInSc1	dodatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
o	o	k7c6	o
dětské	dětský	k2eAgFnSc6d1	dětská
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
navržen	navržen	k2eAgMnSc1d1	navržen
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1924	[number]	k4	1924
na	na	k7c4	na
68	[number]	k4	68
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc2	zasedání
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
ustanovení	ustanovení	k1gNnPc2	ustanovení
by	by	kYmCp3nS	by
Kongres	kongres	k1gInSc1	kongres
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
omezovat	omezovat	k5eAaImF	omezovat
<g/>
,	,	kIx,	,
regulovat	regulovat	k5eAaImF	regulovat
a	a	k8xC	a
zakazovat	zakazovat	k5eAaImF	zakazovat
práci	práce	k1gFnSc4	práce
osob	osoba	k1gFnPc2	osoba
mladších	mladý	k2eAgFnPc2d2	mladší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
tento	tento	k3xDgInSc1	tento
dodatek	dodatek	k1gInSc1	dodatek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
stále	stále	k6eAd1	stále
teoreticky	teoreticky	k6eAd1	teoreticky
ratifikován	ratifikován	k2eAgInSc1d1	ratifikován
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Kongres	kongres	k1gInSc1	kongres
již	již	k6eAd1	již
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
přijal	přijmout	k5eAaPmAgMnS	přijmout
řadu	řada	k1gFnSc4	řada
zákonů	zákon	k1gInPc2	zákon
na	na	k7c6	na
základě	základ	k1gInSc6	základ
oprávnění	oprávnění	k1gNnSc2	oprávnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
dávají	dávat	k5eAaImIp3nP	dávat
některá	některý	k3yIgNnPc1	některý
ustanovení	ustanovení	k1gNnPc1	ustanovení
oddílu	oddíl	k1gInSc2	oddíl
8	[number]	k4	8
článku	článek	k1gInSc2	článek
I	i	k8xC	i
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
dodatků	dodatek	k1gInPc2	dodatek
byly	být	k5eAaImAgFnP	být
Kongresem	kongres	k1gInSc7	kongres
schváleny	schválit	k5eAaPmNgFnP	schválit
ještě	ještě	k9	ještě
dva	dva	k4xCgInPc4	dva
další	další	k2eAgInPc4d1	další
dodatky	dodatek	k1gInPc4	dodatek
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
však	však	k9	však
vypršela	vypršet	k5eAaPmAgFnS	vypršet
lhůta	lhůta	k1gFnSc1	lhůta
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
o	o	k7c6	o
rovných	rovný	k2eAgNnPc6d1	rovné
právech	právo	k1gNnPc6	právo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
stanovoval	stanovovat	k5eAaImAgMnS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
ani	ani	k8xC	ani
žádný	žádný	k3yNgInSc1	žádný
jednotlivý	jednotlivý	k2eAgInSc1d1	jednotlivý
stát	stát	k1gInSc1	stát
Unie	unie	k1gFnSc2	unie
nesmí	smět	k5eNaImIp3nS	smět
nikomu	nikdo	k3yNnSc3	nikdo
upřít	upřít	k5eAaPmF	upřít
nebo	nebo	k8xC	nebo
krátit	krátit	k5eAaImF	krátit
žádná	žádný	k3yNgNnPc1	žádný
práva	právo	k1gNnPc1	právo
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	on	k3xPp3gNnSc2	on
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dodatek	dodatek	k1gInSc1	dodatek
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
představen	představit	k5eAaPmNgInS	představit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nakonec	nakonec	k9	nakonec
Kongresem	kongres	k1gInSc7	kongres
prošel	projít	k5eAaPmAgMnS	projít
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgNnSc4	žádný
ustanovení	ustanovení	k1gNnSc4	ustanovení
o	o	k7c6	o
lhůtě	lhůta	k1gFnSc6	lhůta
pro	pro	k7c4	pro
ratifikaci	ratifikace	k1gFnSc4	ratifikace
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
letá	letý	k2eAgFnSc1d1	letá
lhůta	lhůta	k1gFnSc1	lhůta
však	však	k9	však
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
usnesení	usnesení	k1gNnSc2	usnesení
Kongresu	kongres	k1gInSc2	kongres
doprovázejícího	doprovázející	k2eAgInSc2d1	doprovázející
návrh	návrh	k1gInSc1	návrh
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgInPc2	první
30	[number]	k4	30
států	stát	k1gInPc2	stát
dodatek	dodatek	k1gInSc4	dodatek
přijalo	přijmout	k5eAaPmAgNnS	přijmout
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
však	však	k9	však
vlivem	vlivem	k7c2	vlivem
antifeministických	antifeministický	k2eAgNnPc2d1	antifeministické
hnutí	hnutí	k1gNnPc2	hnutí
tempo	tempo	k1gNnSc4	tempo
zpomalilo	zpomalit	k5eAaPmAgNnS	zpomalit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
35	[number]	k4	35
<g/>
.	.	kIx.	.
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
(	(	kIx(	(
<g/>
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
poslední	poslední	k2eAgInSc1d1	poslední
<g/>
)	)	kIx)	)
stát	stát	k1gInSc1	stát
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
dodatek	dodatek	k1gInSc4	dodatek
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
Idaho	Ida	k1gMnSc4	Ida
<g/>
.	.	kIx.	.
</s>
<s>
Nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
ani	ani	k9	ani
<g/>
,	,	kIx,	,
když	když	k8xS	když
Kongres	kongres	k1gInSc1	kongres
schválil	schválit	k5eAaPmAgInS	schválit
3	[number]	k4	3
<g/>
leté	letý	k2eAgFnPc1d1	letá
prodloužení	prodloužení	k1gNnSc4	prodloužení
lhůty	lhůta	k1gFnSc2	lhůta
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
o	o	k7c4	o
zastoupení	zastoupení	k1gNnSc4	zastoupení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Washingtonu	Washington	k1gInSc2	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
navržený	navržený	k2eAgMnSc1d1	navržený
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
na	na	k7c4	na
95	[number]	k4	95
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc2	zasedání
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
dodatek	dodatek	k1gInSc1	dodatek
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
by	by	kYmCp3nS	by
District	District	k1gMnSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
stejné	stejný	k2eAgNnSc4d1	stejné
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
státem	stát	k1gInSc7	stát
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
dva	dva	k4xCgMnPc4	dva
senátory	senátor	k1gMnPc4	senátor
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
jednoho	jeden	k4xCgMnSc4	jeden
člena	člen	k1gMnSc4	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
pouze	pouze	k6eAd1	pouze
16	[number]	k4	16
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
požadovaných	požadovaný	k2eAgNnPc2d1	požadované
38	[number]	k4	38
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
schvalovací	schvalovací	k2eAgFnSc1d1	schvalovací
lhůta	lhůta	k1gFnSc1	lhůta
vypršela	vypršet	k5eAaPmAgFnS	vypršet
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritické	kritický	k2eAgInPc1d1	kritický
ohlasy	ohlas	k1gInPc1	ohlas
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
přijetí	přijetí	k1gNnSc2	přijetí
americké	americký	k2eAgFnSc2d1	americká
demokratické	demokratický	k2eAgFnSc2d1	demokratická
ústavy	ústava	k1gFnSc2	ústava
zaznívaly	zaznívat	k5eAaImAgFnP	zaznívat
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
některých	některý	k3yIgMnPc2	některý
jejích	její	k3xOp3gMnPc2	její
možných	možný	k2eAgMnPc2d1	možný
dopadů	dopad	k1gInPc2	dopad
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
od	od	k7c2	od
jejích	její	k3xOp3gMnPc2	její
zastánců	zastánce	k1gMnPc2	zastánce
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
francouzský	francouzský	k2eAgMnSc1d1	francouzský
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
myslitel	myslitel	k1gMnSc1	myslitel
Alexis	Alexis	k1gFnSc2	Alexis
de	de	k?	de
Tocqueville	Tocqueville	k1gFnSc2	Tocqueville
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
zastánce	zastánce	k1gMnSc1	zastánce
reformních	reformní	k2eAgFnPc2d1	reformní
demokratických	demokratický	k2eAgFnPc2d1	demokratická
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
nestabilitou	nestabilita	k1gFnSc7	nestabilita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
plynula	plynout	k5eAaImAgFnS	plynout
z	z	k7c2	z
neustálé	neustálý	k2eAgFnSc2d1	neustálá
výměny	výměna	k1gFnSc2	výměna
zastupitelů	zastupitel	k1gMnPc2	zastupitel
představujících	představující	k2eAgMnPc2d1	představující
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
před	před	k7c7	před
diktátem	diktát	k1gInSc7	diktát
většiny	většina	k1gFnSc2	většina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
De	De	k?	De
la	la	k1gNnSc2	la
démocratie	démocratie	k1gFnSc2	démocratie
en	en	k?	en
Amérique	Amériqu	k1gFnSc2	Amériqu
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Demokracie	demokracie	k1gFnSc2	demokracie
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
)	)	kIx)	)
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
psal	psát	k5eAaImAgMnS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Většina	většina	k1gFnSc1	většina
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
faktickou	faktický	k2eAgFnSc4d1	faktická
moc	moc	k1gFnSc4	moc
...	...	k?	...
a	a	k8xC	a
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
otázce	otázka	k1gFnSc6	otázka
ustaví	ustavit	k5eAaPmIp3nS	ustavit
<g/>
,	,	kIx,	,
neexistují	existovat	k5eNaImIp3nP	existovat
takřka	takřka	k6eAd1	takřka
překážky	překážka	k1gFnPc4	překážka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
ne	ne	k9	ne
snad	snad	k9	snad
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
zpomalit	zpomalit	k5eAaPmF	zpomalit
její	její	k3xOp3gInSc4	její
postup	postup	k1gInSc4	postup
a	a	k8xC	a
poskytnout	poskytnout	k5eAaPmF	poskytnout
jí	on	k3xPp3gFnSc7	on
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyslechla	vyslechnout	k5eAaPmAgFnS	vyslechnout
stesky	stesk	k1gInPc4	stesk
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
drtí	drtit	k5eAaImIp3nS	drtit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Různá	různý	k2eAgFnSc1d1	různá
existující	existující	k2eAgFnSc1d1	existující
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
chybějící	chybějící	k2eAgNnSc4d1	chybějící
ustanovení	ustanovení	k1gNnSc4	ustanovení
Ústavy	ústava	k1gFnSc2	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgMnPc2d1	americký
bývají	bývat	k5eAaImIp3nP	bývat
i	i	k9	i
dnes	dnes	k6eAd1	dnes
občas	občas	k6eAd1	občas
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
některými	některý	k3yIgMnPc7	některý
americkými	americký	k2eAgMnPc7d1	americký
akademiky	akademik	k1gMnPc7	akademik
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
námitek	námitka	k1gFnPc2	námitka
shrnul	shrnout	k5eAaPmAgMnS	shrnout
americký	americký	k2eAgMnSc1d1	americký
politolog	politolog	k1gMnSc1	politolog
Larry	Larr	k1gInPc4	Larr
Sabato	Sabat	k2eAgNnSc1d1	Sabato
z	z	k7c2	z
Virginské	virginský	k2eAgFnSc2d1	virginská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
A	a	k9	a
More	mor	k1gInSc5	mor
Perfect	Perfect	k1gMnSc1	Perfect
Constitution	Constitution	k1gInSc1	Constitution
<g/>
.	.	kIx.	.
</s>
<s>
Sabato	Sabato	k6eAd1	Sabato
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
žádá	žádat	k5eAaImIp3nS	žádat
přijetí	přijetí	k1gNnSc4	přijetí
dodatku	dodatek	k1gInSc2	dodatek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
organizoval	organizovat	k5eAaBmAgInS	organizovat
primární	primární	k2eAgFnPc4d1	primární
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
státy	stát	k1gInPc1	stát
předhánějí	předhánět	k5eAaImIp3nP	předhánět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
proběhnou	proběhnout	k5eAaPmIp3nP	proběhnout
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
tyto	tento	k3xDgFnPc4	tento
volby	volba	k1gFnPc1	volba
připadají	připadat	k5eAaImIp3nP	připadat
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
volebního	volební	k2eAgInSc2d1	volební
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
vlastními	vlastní	k2eAgFnPc7d1	vlastní
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
uspěchaným	uspěchaný	k2eAgFnPc3d1	uspěchaná
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
voličů	volič	k1gMnPc2	volič
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
snižovat	snižovat	k5eAaImF	snižovat
volební	volební	k2eAgFnSc4d1	volební
účast	účast	k1gFnSc4	účast
<g/>
.	.	kIx.	.
</s>
<s>
Ústavu	ústav	k1gInSc3	ústav
také	také	k9	také
viní	vinit	k5eAaImIp3nS	vinit
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
americkým	americký	k2eAgMnPc3d1	americký
prezidentům	prezident	k1gMnPc3	prezident
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
nepopulárních	populární	k2eNgFnPc6d1	nepopulární
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
požadavek	požadavek	k1gInSc4	požadavek
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
občanem	občan	k1gMnSc7	občan
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nelíbí	líbit	k5eNaImIp3nS	líbit
definitiva	definitiva	k1gFnSc1	definitiva
soudců	soudce	k1gMnPc2	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
výsledkem	výsledek	k1gInSc7	výsledek
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc4	on
staří	starý	k2eAgMnPc1d1	starý
soudci	soudce	k1gMnPc1	soudce
s	s	k7c7	s
názory	názor	k1gInPc7	názor
odpovídajícími	odpovídající	k2eAgInPc7d1	odpovídající
spíše	spíše	k9	spíše
minulým	minulý	k2eAgFnPc3d1	minulá
generacím	generace	k1gFnPc3	generace
než	než	k8xS	než
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nežádoucí	žádoucí	k2eNgNnSc1d1	nežádoucí
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
také	také	k9	také
fakt	faktum	k1gNnPc2	faktum
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
stejné	stejný	k2eAgNnSc4d1	stejné
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
mají	mít	k5eAaImIp3nP	mít
obyvatel	obyvatel	k1gMnSc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
26	[number]	k4	26
nejméně	málo	k6eAd3	málo
lidnatých	lidnatý	k2eAgInPc2d1	lidnatý
států	stát	k1gInPc2	stát
domluvilo	domluvit	k5eAaPmAgNnS	domluvit
a	a	k8xC	a
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
jednotně	jednotně	k6eAd1	jednotně
<g/>
,	,	kIx,	,
získalo	získat	k5eAaPmAgNnS	získat
by	by	kYmCp3nP	by
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
celý	celý	k2eAgInSc4d1	celý
Senát	senát	k1gInSc4	senát
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
žije	žít	k5eAaImIp3nS	žít
jen	jen	k9	jen
17	[number]	k4	17
procent	procento	k1gNnPc2	procento
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
<g/>
Sanford	Sanford	k1gMnSc1	Sanford
Levinson	Levinson	k1gMnSc1	Levinson
<g/>
,	,	kIx,	,
specialista	specialista	k1gMnSc1	specialista
na	na	k7c4	na
americké	americký	k2eAgNnSc4d1	americké
ústavní	ústavní	k2eAgNnSc4d1	ústavní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
také	také	k9	také
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
malé	malý	k2eAgInPc1d1	malý
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
stejné	stejný	k2eAgNnSc4d1	stejné
zastoupení	zastoupení	k1gNnSc4	zastoupení
jako	jako	k8xC	jako
státy	stát	k1gInPc4	stát
velké	velký	k2eAgInPc4d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
stát	stát	k1gInSc1	stát
Wyoming	Wyoming	k1gInSc1	Wyoming
disponuje	disponovat	k5eAaBmIp3nS	disponovat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
komoře	komora	k1gFnSc6	komora
Kongresu	kongres	k1gInSc2	kongres
stejným	stejný	k2eAgInSc7d1	stejný
počtem	počet	k1gInSc7	počet
hlasů	hlas	k1gInPc2	hlas
jako	jako	k8xS	jako
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nepoměr	nepoměr	k1gInSc1	nepoměr
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vytrvalé	vytrvalý	k2eAgNnSc4d1	vytrvalé
přerozdělování	přerozdělování	k1gNnSc4	přerozdělování
zdrojů	zdroj	k1gInPc2	zdroj
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
států	stát	k1gInPc2	stát
do	do	k7c2	do
malých	malý	k2eAgFnPc2d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
kritiku	kritika	k1gFnSc4	kritika
směřoval	směřovat	k5eAaImAgInS	směřovat
na	na	k7c4	na
ustanovení	ustanovení	k1gNnSc4	ustanovení
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
sboru	sbor	k1gInSc3	sbor
volitelů	volitel	k1gMnPc2	volitel
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
stal	stát	k5eAaPmAgMnS	stát
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nezískal	získat	k5eNaPmAgInS	získat
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sbor	sbor	k1gInSc4	sbor
volitelů	volitel	k1gMnPc2	volitel
zvolil	zvolit	k5eAaPmAgMnS	zvolit
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
lidové	lidový	k2eAgNnSc4d1	lidové
hlasování	hlasování	k1gNnSc4	hlasování
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
během	během	k7c2	během
historie	historie	k1gFnSc2	historie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
již	již	k9	již
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
:	:	kIx,	:
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
(	(	kIx(	(
<g/>
Rutherford	Rutherford	k1gMnSc1	Rutherford
B.	B.	kA	B.
Hayes	Hayes	k1gMnSc1	Hayes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1888	[number]	k4	1888
(	(	kIx(	(
<g/>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
George	Georg	k1gFnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
)	)	kIx)	)
a	a	k8xC	a
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
americká	americký	k2eAgFnSc1d1	americká
ústava	ústava	k1gFnSc1	ústava
podle	podle	k7c2	podle
Levinsona	Levinson	k1gMnSc2	Levinson
nedává	dávat	k5eNaImIp3nS	dávat
lidem	člověk	k1gMnPc3	člověk
možnost	možnost	k1gFnSc4	možnost
dostatečně	dostatečně	k6eAd1	dostatečně
rychle	rychle	k6eAd1	rychle
odstranit	odstranit	k5eAaPmF	odstranit
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
neschopné	schopný	k2eNgInPc4d1	neschopný
nebo	nebo	k8xC	nebo
nemocné	mocný	k2eNgMnPc4d1	nemocný
prezidenty	prezident	k1gMnPc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
zase	zase	k9	zase
často	často	k6eAd1	často
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
tzv.	tzv.	kA	tzv.
gerrymandering	gerrymandering	k1gInSc1	gerrymandering
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
snaží	snažit	k5eAaImIp3nP	snažit
získat	získat	k5eAaPmF	získat
výhodu	výhoda	k1gFnSc4	výhoda
pomocí	pomocí	k7c2	pomocí
změn	změna	k1gFnPc2	změna
geografických	geografický	k2eAgFnPc2d1	geografická
hranic	hranice	k1gFnPc2	hranice
volebních	volební	k2eAgInPc2d1	volební
okrsků	okrsek	k1gInPc2	okrsek
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgMnSc1d1	další
americký	americký	k2eAgMnSc1d1	americký
politolog	politolog	k1gMnSc1	politolog
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
A.	A.	kA	A.
Dahl	dahl	k1gInSc1	dahl
z	z	k7c2	z
Yaleovy	Yaleův	k2eAgFnSc2d1	Yaleova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
How	How	k1gMnSc1	How
Democratic	Democratice	k1gFnPc2	Democratice
Is	Is	k1gMnSc1	Is
the	the	k?	the
American	American	k1gInSc1	American
Constitution	Constitution	k1gInSc1	Constitution
<g/>
?	?	kIx.	?
</s>
<s>
také	také	k9	také
popisuje	popisovat	k5eAaImIp3nS	popisovat
některé	některý	k3yIgInPc4	některý
aspekty	aspekt	k1gInPc4	aspekt
amerického	americký	k2eAgInSc2d1	americký
vládního	vládní	k2eAgInSc2d1	vládní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
"	"	kIx"	"
<g/>
neobvyklé	obvyklý	k2eNgInPc4d1	neobvyklý
a	a	k8xC	a
potenciálně	potenciálně	k6eAd1	potenciálně
nedemokratické	demokratický	k2eNgInPc4d1	nedemokratický
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
federální	federální	k2eAgInSc1d1	federální
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
dvoukomorové	dvoukomorový	k2eAgNnSc1d1	dvoukomorové
zákonodárství	zákonodárství	k1gNnSc1	zákonodárství
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgNnSc1d1	soudní
přezkoumání	přezkoumání	k1gNnSc1	přezkoumání
ústavnosti	ústavnost	k1gFnSc2	ústavnost
<g/>
,	,	kIx,	,
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
systém	systém	k1gInSc1	systém
či	či	k8xC	či
systém	systém	k1gInSc1	systém
volby	volba	k1gFnSc2	volba
prezidenta	prezident	k1gMnSc2	prezident
sborem	sborem	k6eAd1	sborem
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
problém	problém	k1gInSc1	problém
však	však	k9	však
vidí	vidět	k5eAaImIp3nS	vidět
spíše	spíše	k9	spíše
tendenci	tendence	k1gFnSc4	tendence
Američanů	Američan	k1gMnPc2	Američan
Ústavu	ústav	k1gInSc2	ústav
příliš	příliš	k6eAd1	příliš
uctívat	uctívat	k5eAaImF	uctívat
<g/>
.	.	kIx.	.
<g/>
Ozývají	ozývat	k5eAaImIp3nP	ozývat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
hlasy	hlas	k1gInPc1	hlas
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústava	ústava	k1gFnSc1	ústava
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
reformu	reforma	k1gFnSc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Sabata	Sabat	k1gMnSc2	Sabat
přitom	přitom	k6eAd1	přitom
sama	sám	k3xTgFnSc1	sám
nabízí	nabízet	k5eAaImIp3nS	nabízet
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
reformy	reforma	k1gFnSc2	reforma
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
–	–	k?	–
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
ustanovení	ustanovení	k1gNnSc2	ustanovení
článku	článek	k1gInSc2	článek
V	V	kA	V
totiž	totiž	k9	totiž
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
svolat	svolat	k5eAaPmF	svolat
další	další	k2eAgInSc4d1	další
ústavní	ústavní	k2eAgInSc4d1	ústavní
konvent	konvent	k1gInSc4	konvent
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stejné	stejný	k2eAgNnSc4d1	stejné
ustanovení	ustanovení	k1gNnSc4	ustanovení
se	se	k3xPyFc4	se
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
také	také	k9	také
Richard	Richard	k1gMnSc1	Richard
Labunski	Labunsk	k1gFnSc2	Labunsk
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
knihy	kniha	k1gFnSc2	kniha
The	The	k1gMnSc1	The
Second	Second	k1gMnSc1	Second
Constitutional	Constitutional	k1gMnSc1	Constitutional
Convention	Convention	k1gInSc4	Convention
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
,	,	kIx,	,
nutné	nutný	k2eAgFnPc1d1	nutná
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
zaběhnutého	zaběhnutý	k2eAgInSc2d1	zaběhnutý
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Politiky	politika	k1gFnPc1	politika
by	by	kYmCp3nP	by
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
ke	k	k7c3	k
svolání	svolání	k1gNnSc3	svolání
nového	nový	k2eAgInSc2d1	nový
ústavního	ústavní	k2eAgInSc2d1	ústavní
konventu	konvent	k1gInSc2	konvent
mohl	moct	k5eAaImAgInS	moct
přinutit	přinutit	k5eAaPmF	přinutit
pouze	pouze	k6eAd1	pouze
soustředěný	soustředěný	k2eAgInSc4d1	soustředěný
nátlak	nátlak	k1gInSc4	nátlak
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
hlasy	hlas	k1gInPc4	hlas
však	však	k9	však
například	například	k6eAd1	například
Dahl	dahl	k1gInSc4	dahl
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
vůči	vůči	k7c3	vůči
Ústavě	ústava	k1gFnSc3	ústava
kritický	kritický	k2eAgInSc4d1	kritický
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
volit	volit	k5eAaImF	volit
takové	takový	k3xDgNnSc4	takový
krajní	krajní	k2eAgNnSc4d1	krajní
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
vliv	vliv	k1gInSc4	vliv
==	==	k?	==
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
tvůrce	tvůrce	k1gMnPc4	tvůrce
ústav	ústava	k1gFnPc2	ústava
dalších	další	k2eAgInPc2d1	další
nových	nový	k2eAgInPc2d1	nový
demokratických	demokratický	k2eAgInPc2d1	demokratický
režimů	režim	k1gInPc2	režim
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
modelem	model	k1gInSc7	model
republikánské	republikánský	k2eAgFnSc2d1	republikánská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
oddělením	oddělení	k1gNnSc7	oddělení
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
státu	stát	k1gInSc3	stát
vládnou	vládnout	k5eAaImIp3nP	vládnout
<g/>
,	,	kIx,	,
od	od	k7c2	od
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
mu	on	k3xPp3gMnSc3	on
dávají	dávat	k5eAaImIp3nP	dávat
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Nejenže	nejenže	k6eAd1	nejenže
některé	některý	k3yIgInPc1	některý
ústavy	ústav	k1gInPc1	ústav
mají	mít	k5eAaImIp3nP	mít
podobný	podobný	k2eAgInSc4d1	podobný
styl	styl	k1gInSc4	styl
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
celé	celý	k2eAgFnPc4d1	celá
pasáže	pasáž	k1gFnPc4	pasáž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
kladou	klást	k5eAaImIp3nP	klást
podobný	podobný	k2eAgInSc4d1	podobný
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
práv	právo	k1gNnPc2	právo
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
.	.	kIx.	.
<g/>
Americký	americký	k2eAgMnSc1d1	americký
historik	historik	k1gMnSc1	historik
George	George	k1gNnSc2	George
Athan	Athan	k1gMnSc1	Athan
Billias	Billias	k1gMnSc1	Billias
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
American	American	k1gMnSc1	American
constitutionalism	constitutionalism	k1gMnSc1	constitutionalism
heard	heard	k1gMnSc1	heard
round	round	k1gInSc1	round
the	the	k?	the
world	world	k1gInSc1	world
<g/>
,	,	kIx,	,
1776	[number]	k4	1776
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
a	a	k8xC	a
global	globat	k5eAaImAgMnS	globat
perspective	perspectiv	k1gInSc5	perspectiv
popisuje	popisovat	k5eAaImIp3nS	popisovat
sedm	sedm	k4xCc1	sedm
vln	vlna	k1gFnPc2	vlna
demokratizace	demokratizace	k1gFnSc2	demokratizace
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
americká	americký	k2eAgFnSc1d1	americká
ústava	ústava	k1gFnSc1	ústava
některé	některý	k3yIgInPc4	některý
státy	stát	k1gInPc4	stát
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
V	v	k7c6	v
letech	let	k1gInPc6	let
1776	[number]	k4	1776
<g/>
–	–	k?	–
<g/>
1800	[number]	k4	1800
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
měla	mít	k5eAaImAgFnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
ústavy	ústav	k1gInPc4	ústav
nových	nový	k2eAgFnPc2d1	nová
republik	republika	k1gFnPc2	republika
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Karibiku	Karibikum	k1gNnSc6	Karibikum
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
inspirace	inspirace	k1gFnSc2	inspirace
americkou	americký	k2eAgFnSc7d1	americká
ústavou	ústava	k1gFnSc7	ústava
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
Evropou	Evropa	k1gFnSc7	Evropa
během	během	k7c2	během
revolučního	revoluční	k2eAgInSc2d1	revoluční
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
pak	pak	k6eAd1	pak
Asií	Asie	k1gFnSc7	Asie
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
Latinskou	latinský	k2eAgFnSc7d1	Latinská
Amerikou	Amerika	k1gFnSc7	Amerika
po	po	k7c6	po
vyhrané	vyhraný	k2eAgFnSc6d1	vyhraná
španělsko-americké	španělskomerický	k2eAgFnSc6d1	španělsko-americká
válce	válka	k1gFnSc6	válka
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
stanuly	stanout	k5eAaPmAgInP	stanout
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
světové	světový	k2eAgFnSc2d1	světová
velmoci	velmoc	k1gFnSc2	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgFnPc1d1	následná
dvě	dva	k4xCgFnPc1	dva
vlny	vlna	k1gFnPc1	vlna
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
ústavami	ústava	k1gFnPc7	ústava
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
států	stát	k1gInPc2	stát
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
světových	světový	k2eAgFnPc6d1	světová
válkách	válka	k1gFnPc6	válka
a	a	k8xC	a
poslední	poslední	k2eAgNnSc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
letech	let	k1gInPc6	let
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
autoritativními	autoritativní	k2eAgInPc7d1	autoritativní
režimy	režim	k1gInPc7	režim
přeměnilo	přeměnit	k5eAaPmAgNnS	přeměnit
na	na	k7c4	na
ústavní	ústavní	k2eAgFnPc4d1	ústavní
demokracie	demokracie	k1gFnPc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
ústav	ústava	k1gFnPc2	ústava
byla	být	k5eAaImAgFnS	být
také	také	k9	také
např.	např.	kA	např.
Ústavní	ústavní	k2eAgFnSc1d1	ústavní
listina	listina	k1gFnSc1	listina
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
inspiraci	inspirace	k1gFnSc4	inspirace
americkým	americký	k2eAgInSc7d1	americký
vzorem	vzor	k1gInSc7	vzor
nezapře	zapřít	k5eNaPmIp3nS	zapřít
především	především	k9	především
formulace	formulace	k1gFnSc1	formulace
její	její	k3xOp3gFnSc2	její
preambule	preambule	k1gFnSc2	preambule
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nelze	lze	k6eNd1	lze
vliv	vliv	k1gInSc4	vliv
americké	americký	k2eAgFnSc2d1	americká
ústavy	ústava	k1gFnSc2	ústava
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
procesy	proces	k1gInPc4	proces
přeceňovat	přeceňovat	k5eAaImF	přeceňovat
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
byl	být	k5eAaImAgInS	být
spíše	spíše	k9	spíše
omezený	omezený	k2eAgInSc1d1	omezený
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
nepřímý	přímý	k2eNgMnSc1d1	nepřímý
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
demokratizující	demokratizující	k2eAgFnPc1d1	demokratizující
se	se	k3xPyFc4	se
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
často	často	k6eAd1	často
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
také	také	k6eAd1	také
britským	britský	k2eAgInSc7d1	britský
či	či	k8xC	či
francouzským	francouzský	k2eAgInSc7d1	francouzský
modelem	model	k1gInSc7	model
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
<g/>
Britský	britský	k2eAgMnSc1d1	britský
historik	historik	k1gMnSc1	historik
Paul	Paul	k1gMnSc1	Paul
Johnson	Johnson	k1gMnSc1	Johnson
zase	zase	k9	zase
poukázal	poukázat	k5eAaPmAgMnS	poukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
usilují	usilovat	k5eAaImIp3nP	usilovat
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
federálního	federální	k2eAgInSc2d1	federální
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
od	od	k7c2	od
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
inspirovat	inspirovat	k5eAaBmF	inspirovat
spíše	spíše	k9	spíše
způsobem	způsob	k1gInSc7	způsob
vytváření	vytváření	k1gNnSc2	vytváření
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
škodě	škoda	k1gFnSc3	škoda
často	často	k6eAd1	často
přehlížely	přehlížet	k5eAaImAgFnP	přehlížet
či	či	k8xC	či
přehlížejí	přehlížet	k5eAaImIp3nP	přehlížet
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
tvorbu	tvorba	k1gFnSc4	tvorba
ústavy	ústava	k1gFnSc2	ústava
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
Středoafrické	středoafrický	k2eAgFnSc2d1	Středoafrická
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
Malajsijské	malajsijský	k2eAgFnSc2d1	malajsijská
federace	federace	k1gFnSc2	federace
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Překlady	překlad	k1gInPc4	překlad
===	===	k?	===
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
překlady	překlad	k1gInPc1	překlad
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
veřejně	veřejně	k6eAd1	veřejně
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
knihovnách	knihovna	k1gFnPc6	knihovna
či	či	k8xC	či
na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
různých	různý	k2eAgInPc2d1	různý
vládních	vládní	k2eAgInPc2d1	vládní
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
univerzit	univerzita	k1gFnPc2	univerzita
či	či	k8xC	či
soukromých	soukromý	k2eAgFnPc2d1	soukromá
nadací	nadace	k1gFnPc2	nadace
a	a	k8xC	a
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americký	americký	k2eAgInSc1d1	americký
úřad	úřad	k1gInSc1	úřad
Federal	Federal	k1gMnSc1	Federal
Judicial	Judicial	k1gMnSc1	Judicial
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
také	také	k6eAd1	také
historií	historie	k1gFnSc7	historie
amerického	americký	k2eAgNnSc2d1	americké
federálního	federální	k2eAgNnSc2d1	federální
soudnictví	soudnictví	k1gNnSc2	soudnictví
<g/>
,	,	kIx,	,
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
stránkách	stránka	k1gFnPc6	stránka
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
materiály	materiál	k1gInPc4	materiál
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
vládního	vládní	k2eAgInSc2d1	vládní
a	a	k8xC	a
soudního	soudní	k2eAgInSc2d1	soudní
systému	systém	k1gInSc2	systém
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
také	také	k9	také
překlady	překlad	k1gInPc4	překlad
Ústavy	ústava	k1gFnSc2	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
do	do	k7c2	do
arabštiny	arabština	k1gFnSc2	arabština
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
japonštiny	japonština	k1gFnSc2	japonština
<g/>
,	,	kIx,	,
korejštiny	korejština	k1gFnSc2	korejština
<g/>
,	,	kIx,	,
portugalštiny	portugalština	k1gFnSc2	portugalština
<g/>
,	,	kIx,	,
ruštiny	ruština	k1gFnSc2	ruština
<g/>
,	,	kIx,	,
čínština	čínština	k1gFnSc1	čínština
a	a	k8xC	a
španělštiny	španělština	k1gFnPc1	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
filadelfská	filadelfský	k2eAgFnSc1d1	Filadelfská
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
Historical	Historical	k1gFnSc1	Historical
Society	societa	k1gFnSc2	societa
of	of	k?	of
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
několik	několik	k4yIc4	několik
publikací	publikace	k1gFnPc2	publikace
s	s	k7c7	s
překlady	překlad	k1gInPc7	překlad
Ústavy	ústava	k1gFnSc2	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
vydalo	vydat	k5eAaPmAgNnS	vydat
oficiální	oficiální	k2eAgInSc4d1	oficiální
španělský	španělský	k2eAgInSc4d1	španělský
překlad	překlad	k1gInSc4	překlad
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
doplněný	doplněný	k2eAgInSc1d1	doplněný
poznámkami	poznámka	k1gFnPc7	poznámka
vysvětlujícími	vysvětlující	k2eAgFnPc7d1	vysvětlující
různé	různý	k2eAgInPc4d1	různý
problémy	problém	k1gInPc1	problém
a	a	k8xC	a
odlišnosti	odlišnost	k1gFnPc1	odlišnost
jazykového	jazykový	k2eAgInSc2d1	jazykový
překladu	překlad	k1gInSc2	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
překlady	překlad	k1gInPc1	překlad
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
často	často	k6eAd1	často
také	také	k9	také
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
velvyslanecká	velvyslanecký	k2eAgNnPc4d1	velvyslanecký
zastoupení	zastoupení	k1gNnPc4	zastoupení
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
některé	některý	k3yIgInPc4	některý
odkazy	odkaz	k1gInPc4	odkaz
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
ústava	ústava	k1gFnSc1	ústava
vydána	vydán	k2eAgFnSc1d1	vydána
knižně	knižně	k6eAd1	knižně
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Reflex	reflex	k1gInSc4	reflex
nebo	nebo	k8xC	nebo
také	také	k9	také
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
českého	český	k2eAgInSc2d1	český
překladu	překlad	k1gInSc2	překlad
knihy	kniha	k1gFnSc2	kniha
George	Georg	k1gMnSc2	Georg
Tindalla	Tindall	k1gMnSc2	Tindall
a	a	k8xC	a
Davida	David	k1gMnSc2	David
Shi	Shi	k1gMnSc2	Shi
A	a	k9	a
Narrative	Narrativ	k1gInSc5	Narrativ
History	Histor	k1gInPc1	Histor
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
edice	edice	k1gFnSc2	edice
Dějiny	dějiny	k1gFnPc1	dějiny
států	stát	k1gInPc2	stát
vydalo	vydat	k5eAaPmAgNnS	vydat
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
překladů	překlad	k1gInPc2	překlad
dostupných	dostupný	k2eAgInPc2d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
United	United	k1gInSc4	United
States	States	k1gInSc1	States
Constitution	Constitution	k1gInSc1	Constitution
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
ze	z	k7c2	z
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1787	[number]	k4	1787
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Blahož	Blahož	k1gFnSc1	Blahož
a	a	k8xC	a
Miloš	Miloš	k1gMnSc1	Miloš
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Reflex	reflex	k1gInSc1	reflex
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
155	[number]	k4	155
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TINDALL	TINDALL	kA	TINDALL
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
B.	B.	kA	B.
<g/>
;	;	kIx,	;
SHI	SHI	kA	SHI
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
B.	B.	kA	B.
Dějiny	dějiny	k1gFnPc1	dějiny
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Alena	Alena	k1gFnSc1	Alena
Faltýsková	Faltýsková	k1gFnSc1	Faltýsková
<g/>
,	,	kIx,	,
Alena	Alena	k1gFnSc1	Alena
Komárková	Komárková	k1gFnSc1	Komárková
a	a	k8xC	a
Markéta	Markéta	k1gFnSc1	Markéta
Macháčková	Macháčková	k1gFnSc1	Macháčková
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
119	[number]	k4	119
<g/>
–	–	k?	–
<g/>
135	[number]	k4	135
<g/>
,	,	kIx,	,
792	[number]	k4	792
<g/>
–	–	k?	–
<g/>
804	[number]	k4	804
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOCQUEVILLE	TOCQUEVILLE	kA	TOCQUEVILLE
<g/>
,	,	kIx,	,
Alexis	Alexis	k1gFnSc1	Alexis
de	de	k?	de
<g/>
.	.	kIx.	.
</s>
<s>
Demokracie	demokracie	k1gFnSc1	demokracie
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Vladimír	Vladimír	k1gMnSc1	Vladimír
Jochman	Jochman	k1gMnSc1	Jochman
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
52	[number]	k4	52
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JOHNSON	JOHNSON	kA	JOHNSON
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
amerického	americký	k2eAgInSc2d1	americký
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Věra	Věra	k1gFnSc1	Věra
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Lamperovi	Lamper	k1gMnSc3	Lamper
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
799	[number]	k4	799
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
145	[number]	k4	145
<g/>
–	–	k?	–
<g/>
154	[number]	k4	154
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHWEIGL	SCHWEIGL	kA	SCHWEIGL
<g/>
,	,	kIx,	,
Johan	Johan	k1gMnSc1	Johan
<g/>
;	;	kIx,	;
TAUCHEN	TAUCHEN	kA	TAUCHEN
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
dodatek	dodatek	k1gInSc1	dodatek
americké	americký	k2eAgFnSc2d1	americká
Ústavy	ústava	k1gFnSc2	ústava
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Mekon	Mekon	k1gInSc1	Mekon
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
CD	CD	kA	CD
Příspěvků	příspěvek	k1gInPc2	příspěvek
X.	X.	kA	X.
ročníku	ročník	k1gInSc3	ročník
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
konference	konference	k1gFnSc2	konference
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
fakulty	fakulta	k1gFnSc2	fakulta
VŠB-TU	VŠB-TU	k1gFnSc1	VŠB-TU
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
VŠB	VŠB	kA	VŠB
–	–	k?	–
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
248	[number]	k4	248
<g/>
-	-	kIx~	-
<g/>
1704	[number]	k4	1704
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
federalistů	federalista	k1gMnPc2	federalista
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
dodatků	dodatek	k1gInPc2	dodatek
Ústavy	ústava	k1gFnSc2	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Constitution	Constitution	k1gInSc1	Constitution
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
Bill	Bill	k1gMnSc1	Bill
of	of	k?	of
Rights	Rights	k1gInSc1	Rights
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Additional	Additional	k1gFnSc2	Additional
amendments	amendmentsa	k1gFnPc2	amendmentsa
to	ten	k3xDgNnSc1	ten
the	the	k?	the
United	United	k1gInSc1	United
States	States	k1gInSc1	States
Constitution	Constitution	k1gInSc1	Constitution
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
