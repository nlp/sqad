<p>
<s>
Čerlinka	Čerlinka	k1gFnSc1	Čerlinka
je	být	k5eAaImIp3nS	být
zdroj	zdroj	k1gInSc4	zdroj
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
==	==	k?	==
</s>
</p>
<p>
<s>
Necelý	celý	k2eNgInSc4d1	necelý
kilometr	kilometr	k1gInSc4	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
Litovel	Litovel	k1gFnSc1	Litovel
v	v	k7c6	v
ploché	plochý	k2eAgFnSc6d1	plochá
nivě	niva	k1gFnSc6	niva
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
v	v	k7c6	v
CHKO	CHKO	kA	CHKO
Litovelské	litovelský	k2eAgNnSc1d1	Litovelské
Pomoraví	Pomoraví	k1gNnSc1	Pomoraví
<g/>
.	.	kIx.	.
</s>
<s>
Jímá	jímat	k5eAaImIp3nS	jímat
podzemní	podzemní	k2eAgFnSc4d1	podzemní
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
původních	původní	k2eAgInPc2d1	původní
pramenních	pramenní	k2eAgInPc2d1	pramenní
vývěrů	vývěr	k1gInPc2	vývěr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geologie	geologie	k1gFnSc1	geologie
<g/>
,	,	kIx,	,
hydrogeologie	hydrogeologie	k1gFnSc1	hydrogeologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jímací	jímací	k2eAgInPc1d1	jímací
vrty	vrt	k1gInPc1	vrt
a	a	k8xC	a
studny	studna	k1gFnPc1	studna
vodního	vodní	k2eAgInSc2d1	vodní
zdroje	zdroj	k1gInSc2	zdroj
Čerlinka	Čerlinka	k1gFnSc1	Čerlinka
procházejí	procházet	k5eAaImIp3nP	procházet
kvartérními	kvartérní	k2eAgInPc7d1	kvartérní
sedimenty	sediment	k1gInPc7	sediment
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
podložních	podložní	k2eAgInPc2d1	podložní
devonských	devonský	k2eAgInPc2d1	devonský
vápenců	vápenec	k1gInPc2	vápenec
třesínského	třesínský	k2eAgInSc2d1	třesínský
prahu	práh	k1gInSc2	práh
<g/>
.	.	kIx.	.
</s>
<s>
Stopovacími	stopovací	k2eAgInPc7d1	stopovací
experimenty	experiment	k1gInPc7	experiment
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
prováděl	provádět	k5eAaImAgMnS	provádět
Vladimír	Vladimír	k1gMnSc1	Vladimír
Panoš	panoš	k1gMnSc1	panoš
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
souvislost	souvislost	k1gFnSc1	souvislost
vyvěrající	vyvěrající	k2eAgFnSc2d1	vyvěrající
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
ponory	ponor	k1gInPc7	ponor
potoka	potok	k1gInSc2	potok
Špraněk	Špraňka	k1gFnPc2	Špraňka
v	v	k7c6	v
Javoříčsko-mladečském	Javoříčskoladečský	k2eAgInSc6d1	Javoříčsko-mladečský
krasu	kras	k1gInSc6	kras
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Provozovatel	provozovatel	k1gMnSc1	provozovatel
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastní	vlastnit	k5eAaImIp3nS	vlastnit
jej	on	k3xPp3gMnSc4	on
VHS	VHS	kA	VHS
Olomouc	Olomouc	k1gFnSc1	Olomouc
a.s.	a.s.	k?	a.s.
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
jej	on	k3xPp3gInSc4	on
Moravská	moravský	k2eAgFnSc1d1	Moravská
Vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
www.smv.cz	www.smv.cz	k1gMnSc1	www.smv.cz
</s>
</p>
