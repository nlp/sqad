<p>
<s>
Citrín	Citrín	k?	Citrín
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
zlatožlutá	zlatožlutý	k2eAgFnSc1d1	zlatožlutá
až	až	k8xS	až
žlutozelená	žlutozelený	k2eAgFnSc1d1	žlutozelená
varianta	varianta	k1gFnSc1	varianta
křemene	křemen	k1gInSc2	křemen
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
také	také	k9	také
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
citrón	citrón	k1gInSc1	citrón
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
zabarvení	zabarvení	k1gNnSc2	zabarvení
stále	stále	k6eAd1	stále
nebyl	být	k5eNaImAgInS	být
uspokojivě	uspokojivě	k6eAd1	uspokojivě
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
části	část	k1gFnSc2	část
citrínů	citrínů	k?	citrínů
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
způsobena	způsobit	k5eAaPmNgFnS	způsobit
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
Al	ala	k1gFnPc2	ala
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
-	-	kIx~	-
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
u	u	k7c2	u
záhnědy	záhněda	k1gFnSc2	záhněda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mechanizmus	mechanizmus	k1gInSc1	mechanizmus
vzniku	vznik	k1gInSc2	vznik
zabarvení	zabarvení	k1gNnSc2	zabarvení
je	být	k5eAaImIp3nS	být
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
dlouho	dlouho	k6eAd1	dlouho
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
teorie	teorie	k1gFnSc1	teorie
o	o	k7c4	o
zbarvení	zbarvení	k1gNnSc4	zbarvení
vlivem	vlivem	k7c2	vlivem
trojmocného	trojmocný	k2eAgNnSc2d1	trojmocné
železa	železo	k1gNnSc2	železo
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
jako	jako	k9	jako
nepravdivá	pravdivý	k2eNgFnSc1d1	nepravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
železem	železo	k1gNnSc7	železo
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
oranžové	oranžový	k2eAgNnSc1d1	oranžové
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
žluté	žlutý	k2eAgFnPc4d1	žlutá
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgFnPc4d1	zlatá
či	či	k8xC	či
žlutozelené	žlutozelený	k2eAgFnPc4d1	žlutozelená
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
u	u	k7c2	u
pravých	pravý	k2eAgFnPc2d1	pravá
citrínů	citrínů	k?	citrínů
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
nejsou	být	k5eNaImIp3nP	být
křemeny	křemen	k1gInPc4	křemen
s	s	k7c7	s
povlaky	povlak	k1gInPc7	povlak
či	či	k8xC	či
inkluzemi	inkluze	k1gFnPc7	inkluze
oxidů	oxid	k1gInPc2	oxid
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
hematit	hematit	k1gInSc1	hematit
<g/>
,	,	kIx,	,
limonit	limonit	k1gInSc1	limonit
<g/>
)	)	kIx)	)
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
citrín	citrín	k?	citrín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
a	a	k8xC	a
užití	užití	k1gNnSc4	užití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nacházené	nacházený	k2eAgFnSc6d1	nacházená
citríny	citríny	k?	citríny
tíhnou	tíhnout	k5eAaImIp3nP	tíhnout
k	k	k7c3	k
bledým	bledý	k2eAgInPc3d1	bledý
odstínům	odstín	k1gInPc3	odstín
žluté	žlutý	k2eAgFnSc2d1	žlutá
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
kouřovým	kouřový	k2eAgNnSc7d1	kouřové
či	či	k8xC	či
lehce	lehko	k6eAd1	lehko
nazelenalým	nazelenalý	k2eAgNnSc7d1	nazelenalé
zabarvením	zabarvení	k1gNnSc7	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Sytost	sytost	k1gFnSc1	sytost
tónů	tón	k1gInPc2	tón
kamene	kámen	k1gInSc2	kámen
je	být	k5eAaImIp3nS	být
ovlivňovaná	ovlivňovaný	k2eAgFnSc1d1	ovlivňovaná
množstvím	množství	k1gNnSc7	množství
barevných	barevný	k2eAgNnPc2d1	barevné
center	centrum	k1gNnPc2	centrum
–	–	k?	–
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
narůstajícím	narůstající	k2eAgNnSc7d1	narůstající
množstvím	množství	k1gNnSc7	množství
se	se	k3xPyFc4	se
barva	barva	k1gFnSc1	barva
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
<g/>
.	.	kIx.	.
</s>
<s>
Citrín	Citrín	k?	Citrín
se	se	k3xPyFc4	se
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
objevuje	objevovat	k5eAaImIp3nS	objevovat
od	od	k7c2	od
nejníže	nízce	k6eAd3	nízce
ceněných	ceněný	k2eAgInPc2d1	ceněný
citrónových	citrónový	k2eAgInPc2d1	citrónový
odstínů	odstín	k1gInPc2	odstín
po	po	k7c6	po
drahé	drahý	k2eAgFnSc6d1	drahá
zlatooranžové	zlatooranžový	k2eAgFnSc6d1	zlatooranžová
a	a	k8xC	a
nádherně	nádherně	k6eAd1	nádherně
zlatohnědé	zlatohnědý	k2eAgFnPc4d1	zlatohnědá
variace	variace	k1gFnPc4	variace
<g/>
.	.	kIx.	.
</s>
<s>
Sytě	sytě	k6eAd1	sytě
oranžové	oranžový	k2eAgInPc1d1	oranžový
kameny	kámen	k1gInPc1	kámen
s	s	k7c7	s
červenými	červený	k2eAgInPc7d1	červený
odlesky	odlesk	k1gInPc7	odlesk
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
madeiry	madeira	k1gFnSc2	madeira
<g/>
"	"	kIx"	"
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejžádanějším	žádaný	k2eAgMnPc3d3	nejžádanější
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dražším	drahý	k2eAgMnPc3d2	dražší
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
palmeiry	palmeira	k1gMnSc2	palmeira
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zaujmou	zaujmout	k5eAaPmIp3nP	zaujmout
oranžově	oranžově	k6eAd1	oranžově
jantarovým	jantarový	k2eAgNnSc7d1	jantarové
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
přírodní	přírodní	k2eAgInPc1d1	přírodní
citríny	citríny	k?	citríny
mohou	moct	k5eAaImIp3nP	moct
přecházet	přecházet	k5eAaImF	přecházet
do	do	k7c2	do
záhnědy	záhněda	k1gFnSc2	záhněda
či	či	k8xC	či
mohou	moct	k5eAaImIp3nP	moct
srůstat	srůstat	k5eAaImF	srůstat
se	s	k7c7	s
záhnědami	záhněda	k1gFnPc7	záhněda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
<g/>
,	,	kIx,	,
bledě	bledě	k6eAd1	bledě
žlutý	žlutý	k2eAgInSc1d1	žlutý
citrín	citrín	k?	citrín
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
citrínů	citrínů	k?	citrínů
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
jsou	být	k5eAaImIp3nP	být
tepelně	tepelně	k6eAd1	tepelně
upravované	upravovaný	k2eAgInPc1d1	upravovaný
ametysty	ametyst	k1gInPc1	ametyst
<g/>
.	.	kIx.	.
</s>
<s>
Vypálený	vypálený	k2eAgInSc1d1	vypálený
ametyst	ametyst	k1gInSc1	ametyst
má	mít	k5eAaImIp3nS	mít
typicky	typicky	k6eAd1	typicky
oranžové	oranžový	k2eAgNnSc1d1	oranžové
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
použité	použitý	k2eAgInPc1d1	použitý
ametysty	ametyst	k1gInPc1	ametyst
obvykle	obvykle	k6eAd1	obvykle
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
vulkanických	vulkanický	k2eAgFnPc2d1	vulkanická
geod	geoda	k1gFnPc2	geoda
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
Uruguayi	Uruguay	k1gFnSc6	Uruguay
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravé	pravý	k2eAgFnPc1d1	pravá
citríny	citríny	k?	citríny
se	se	k3xPyFc4	se
v	v	k7c6	v
podobných	podobný	k2eAgFnPc6d1	podobná
geodách	geoda	k1gFnPc6	geoda
nikdy	nikdy	k6eAd1	nikdy
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Žíhané	žíhaný	k2eAgInPc4d1	žíhaný
ametysty	ametyst	k1gInPc4	ametyst
nebo	nebo	k8xC	nebo
synteticky	synteticky	k6eAd1	synteticky
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
křemeny	křemen	k1gInPc1	křemen
dopované	dopovaný	k2eAgFnSc2d1	dopovaná
Fe	Fe	k1gFnSc2	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
dichroizmus	dichroizmus	k1gInSc4	dichroizmus
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgInSc4d1	přírodní
citríny	citríny	k?	citríny
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Citrín	Citrín	k?	Citrín
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
jako	jako	k8xS	jako
náhražka	náhražka	k1gFnSc1	náhražka
topazu	topaz	k1gInSc2	topaz
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
brazilským	brazilský	k2eAgInSc7d1	brazilský
topazem	topaz	k1gInSc7	topaz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Citríny	Citríny	k?	Citríny
v	v	k7c6	v
drahokamové	drahokamový	k2eAgFnSc6d1	drahokamová
kvalitě	kvalita	k1gFnSc6	kvalita
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
materiál	materiál	k1gInSc4	materiál
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Bobrůvky	Bobrůvka	k1gFnSc2	Bobrůvka
<g/>
,	,	kIx,	,
Rousměrova	Rousměrov	k1gInSc2	Rousměrov
a	a	k8xC	a
Kněževse	Kněževse	k1gFnSc2	Kněževse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Cally	Call	k1gInPc1	Call
Hallová	Hallová	k1gFnSc1	Hallová
<g/>
:	:	kIx,	:
Drahé	drahý	k2eAgInPc1d1	drahý
kameny	kámen	k1gInPc1	kámen
<g/>
,	,	kIx,	,
Osveta	Osveto	k1gNnPc1	Osveto
<g/>
,	,	kIx,	,
Martin	Martin	k1gInSc1	Martin
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-8063-033-X	[number]	k4	80-8063-033-X
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
–	–	k?	–
svazek	svazek	k1gInSc4	svazek
2	[number]	k4	2
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
899	[number]	k4	899
–	–	k?	–
heslo	heslo	k1gNnSc4	heslo
Citrín	Citrín	k?	Citrín
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Křemen	křemen	k1gInSc1	křemen
</s>
</p>
<p>
<s>
Křišťál	křišťál	k1gInSc1	křišťál
</s>
</p>
<p>
<s>
Záhněda	záhněda	k1gFnSc1	záhněda
</s>
</p>
<p>
<s>
Ametyst	ametyst	k1gInSc1	ametyst
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
citrín	citrín	k?	citrín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
citrín	citrín	k?	citrín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Citrín	Citrín	k?	Citrín
na	na	k7c4	na
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podvody	podvod	k1gInPc1	podvod
s	s	k7c7	s
minerály	minerál	k1gInPc7	minerál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
