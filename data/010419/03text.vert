<p>
<s>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Bádensko-Württembersko	Bádensko-Württembersko	k1gNnSc1	Bádensko-Württembersko
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
kopcovité	kopcovitý	k2eAgFnSc6d1	kopcovitá
krajině	krajina	k1gFnSc6	krajina
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Neckar	Neckara	k1gFnPc2	Neckara
<g/>
,	,	kIx,	,
přítoku	přítok	k1gInSc2	přítok
Rýna	Rýn	k1gInSc2	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
624	[number]	k4	624
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
šestým	šestý	k4xOgMnSc7	šestý
největším	veliký	k2eAgMnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
čítá	čítat	k5eAaImIp3nS	čítat
5,3	[number]	k4	5,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnSc1	obyvatel
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
firmy	firma	k1gFnPc1	firma
Porsche	Porsche	k1gNnSc2	Porsche
<g/>
,	,	kIx,	,
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
či	či	k8xC	či
Robert	Robert	k1gMnSc1	Robert
Bosch	Bosch	kA	Bosch
GmbH	GmbH	k1gFnSc1	GmbH
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
center	centrum	k1gNnPc2	centrum
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
svým	svůj	k3xOyFgInSc7	svůj
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
charakterem	charakter	k1gInSc7	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
40	[number]	k4	40
%	%	kIx~	%
místních	místní	k2eAgMnPc2d1	místní
rezidentů	rezident	k1gMnPc2	rezident
nemá	mít	k5eNaImIp3nS	mít
německý	německý	k2eAgInSc4d1	německý
původ	původ	k1gInSc4	původ
a	a	k8xC	a
přes	přes	k7c4	přes
20	[number]	k4	20
%	%	kIx~	%
ani	ani	k8xC	ani
německý	německý	k2eAgInSc4d1	německý
pas	pas	k1gInSc4	pas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
německého	německý	k2eAgNnSc2d1	německé
slova	slovo	k1gNnSc2	slovo
Stutengarten	Stutengartno	k1gNnPc2	Stutengartno
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
česky	česky	k6eAd1	česky
hřebčinec	hřebčinec	k1gInSc4	hřebčinec
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
hledat	hledat	k5eAaImF	hledat
u	u	k7c2	u
vévody	vévoda	k1gMnSc2	vévoda
Liudolfa	Liudolf	k1gMnSc2	Liudolf
Švábského	švábský	k2eAgMnSc2d1	švábský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
osadu	osada	k1gFnSc4	osada
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
chovat	chovat	k5eAaImF	chovat
koně	kůň	k1gMnSc4	kůň
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
i	i	k9	i
do	do	k7c2	do
znaku	znak	k1gInSc2	znak
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Schwabenmetropole	Schwabenmetropole	k1gFnSc2	Schwabenmetropole
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Švábská	švábský	k2eAgFnSc1d1	Švábská
metropole	metropole	k1gFnSc1	metropole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
historického	historický	k2eAgNnSc2d1	historické
území	území	k1gNnSc2	území
zvaného	zvaný	k2eAgInSc2d1	zvaný
Švábsko	Švábsko	k1gNnSc4	Švábsko
a	a	k8xC	a
na	na	k7c4	na
místní	místní	k2eAgInSc4d1	místní
dialekt	dialekt	k1gInSc4	dialekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgInS	udržet
mezi	mezi	k7c7	mezi
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
požívaly	požívat	k5eAaImAgInP	požívat
tvary	tvar	k1gInPc1	tvar
Štukart	Štukarta	k1gFnPc2	Štukarta
<g/>
,	,	kIx,	,
Štokart	Štokarta	k1gFnPc2	Štokarta
<g/>
,	,	kIx,	,
Štíhrad	Štíhrada	k1gFnPc2	Štíhrada
či	či	k8xC	či
Stavihrad	Stavihrada	k1gFnPc2	Stavihrada
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
narazíme	narazit	k5eAaPmIp1nP	narazit
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
na	na	k7c4	na
německy	německy	k6eAd1	německy
psanou	psaný	k2eAgFnSc4d1	psaná
formu	forma	k1gFnSc4	forma
názvu	název	k1gInSc2	název
tj.	tj.	kA	tj.
Stuttgart	Stuttgart	k1gInSc4	Stuttgart
nebo	nebo	k8xC	nebo
její	její	k3xOp3gInSc4	její
fonetický	fonetický	k2eAgInSc4d1	fonetický
přepis	přepis	k1gInSc4	přepis
Štutgart	Štutgarta	k1gFnPc2	Štutgarta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Prvními	první	k4xOgInPc7	první
obyvateli	obyvatel	k1gMnPc7	obyvatel
byli	být	k5eAaImAgMnP	být
římští	římský	k2eAgMnPc1d1	římský
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
městské	městský	k2eAgFnSc2d1	městská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Bad	Bad	k1gFnPc2	Bad
Cannstatt	Cannstatta	k1gFnPc2	Cannstatta
obranná	obranný	k2eAgFnSc1d1	obranná
castra	castra	k1gFnSc1	castra
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
90	[number]	k4	90
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
Římany	Říman	k1gMnPc4	Říman
germánský	germánský	k2eAgInSc4d1	germánský
kmen	kmen	k1gInSc4	kmen
Alamanů	Alaman	k1gMnPc2	Alaman
a	a	k8xC	a
území	území	k1gNnSc2	území
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
neosídlené	osídlený	k2eNgNnSc1d1	neosídlené
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
950	[number]	k4	950
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
vévoda	vévoda	k1gMnSc1	vévoda
Liudolf	Liudolf	k1gMnSc1	Liudolf
Švábský	švábský	k2eAgMnSc1d1	švábský
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
císaře	císař	k1gMnSc2	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgMnPc4d1	římský
Oty	Ota	k1gMnPc4	Ota
I.	I.	kA	I.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
na	na	k7c6	na
opuštěném	opuštěný	k2eAgNnSc6d1	opuštěné
území	území	k1gNnSc6	území
založit	založit	k5eAaPmF	založit
hřebčinec	hřebčinec	k1gInSc1	hřebčinec
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
měla	mít	k5eAaImAgFnS	mít
chovat	chovat	k5eAaImF	chovat
koně	kůň	k1gMnSc4	kůň
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bojovala	bojovat	k5eAaImAgFnS	bojovat
proti	proti	k7c3	proti
maďarské	maďarský	k2eAgFnSc3d1	maďarská
invazi	invaze	k1gFnSc3	invaze
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
je	být	k5eAaImIp3nS	být
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
v	v	k7c6	v
kronikách	kronika	k1gFnPc6	kronika
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1108	[number]	k4	1108
a	a	k8xC	a
1160	[number]	k4	1160
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
jen	jen	k9	jen
jako	jako	k9	jako
pár	pár	k4xCyI	pár
stavení	stavení	k1gNnPc2	stavení
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
po	po	k7c4	po
předání	předání	k1gNnSc4	předání
oblasti	oblast	k1gFnSc2	oblast
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
rodu	rod	k1gInSc2	rod
Württemberků	Württemberka	k1gMnPc2	Württemberka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1251	[number]	k4	1251
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
rozrůstat	rozrůstat	k5eAaImF	rozrůstat
a	a	k8xC	a
následně	následně	k6eAd1	následně
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
hrabě	hrabě	k1gMnSc1	hrabě
Eberhard	Eberharda	k1gFnPc2	Eberharda
I.	I.	kA	I.
přenesl	přenést	k5eAaPmAgInS	přenést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1320	[number]	k4	1320
sídlo	sídlo	k1gNnSc1	sídlo
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
hradby	hradba	k1gFnSc2	hradba
a	a	k8xC	a
přiřkl	přiřknout	k5eAaPmAgMnS	přiřknout
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
městská	městský	k2eAgNnPc1d1	Městské
práva	právo	k1gNnPc1	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novověk	novověk	k1gInSc4	novověk
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1495	[number]	k4	1495
získali	získat	k5eAaPmAgMnP	získat
Württemberkové	Württemberkové	k2eAgInSc4d1	Württemberkové
titul	titul	k1gInSc4	titul
vévody	vévoda	k1gMnSc2	vévoda
a	a	k8xC	a
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
Württemberského	Württemberský	k2eAgNnSc2d1	Württemberský
vévodství	vévodství	k1gNnSc2	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
výrazně	výrazně	k6eAd1	výrazně
přestavěno	přestavět	k5eAaPmNgNnS	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
například	například	k6eAd1	například
nové	nový	k2eAgNnSc1d1	nové
hlavní	hlavní	k2eAgNnSc1d1	hlavní
náměstí	náměstí	k1gNnSc1	náměstí
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Schillerplatz	Schillerplatz	k1gInSc1	Schillerplatz
<g/>
)	)	kIx)	)
a	a	k8xC	a
starý	starý	k2eAgInSc1d1	starý
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
renezanční	renezanční	k2eAgInSc4d1	renezanční
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
oblast	oblast	k1gFnSc4	oblast
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
síle	síla	k1gFnSc6	síla
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
opakovaně	opakovaně	k6eAd1	opakovaně
dobyli	dobýt	k5eAaPmAgMnP	dobýt
město	město	k1gNnSc4	město
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1634	[number]	k4	1634
a	a	k8xC	a
1636	[number]	k4	1636
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1637	[number]	k4	1637
<g/>
)	)	kIx)	)
udeřila	udeřit	k5eAaPmAgFnS	udeřit
morová	morový	k2eAgFnSc1d1	morová
nákaza	nákaza	k1gFnSc1	nákaza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zdecimovala	zdecimovat	k5eAaPmAgFnS	zdecimovat
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
(	(	kIx(	(
<g/>
populace	populace	k1gFnSc1	populace
vévodství	vévodství	k1gNnSc2	vévodství
klesla	klesnout	k5eAaPmAgFnS	klesnout
z	z	k7c2	z
350	[number]	k4	350
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1618	[number]	k4	1618
na	na	k7c4	na
120	[number]	k4	120
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obnova	obnova	k1gFnSc1	obnova
města	město	k1gNnSc2	město
trvala	trvat	k5eAaImAgFnS	trvat
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1688	[number]	k4	1688
se	se	k3xPyFc4	se
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
města	město	k1gNnSc2	město
objevila	objevit	k5eAaPmAgFnS	objevit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
Devítiletá	devítiletý	k2eAgFnSc1d1	devítiletá
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
díky	díky	k7c3	díky
diplomacii	diplomacie	k1gFnSc3	diplomacie
ušetřeno	ušetřen	k2eAgNnSc4d1	ušetřeno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přenesl	přenést	k5eAaPmAgMnS	přenést
vévoda	vévoda	k1gMnSc1	vévoda
Eberhart	Eberharta	k1gFnPc2	Eberharta
Ludvík	Ludvík	k1gMnSc1	Ludvík
Württemberský	Württemberský	k2eAgInSc1d1	Württemberský
sídlo	sídlo	k1gNnSc4	sídlo
rodu	rod	k1gInSc2	rod
ze	z	k7c2	z
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
do	do	k7c2	do
nedalekého	daleký	k2eNgInSc2d1	nedaleký
Ludwigsburgu	Ludwigsburg	k1gInSc2	Ludwigsburg
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1718	[number]	k4	1718
-	-	kIx~	-
1723	[number]	k4	1723
tam	tam	k6eAd1	tam
nechal	nechat	k5eAaPmAgInS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
největší	veliký	k2eAgInSc1d3	veliký
barokní	barokní	k2eAgInSc1d1	barokní
palác	palác	k1gInSc1	palác
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
Švábské	švábský	k2eAgFnPc4d1	Švábská
Versailles	Versailles	k1gFnPc4	Versailles
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1737	[number]	k4	1737
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
vévoda	vévoda	k1gMnSc1	vévoda
Karel	Karel	k1gMnSc1	Karel
Evžen	Evžen	k1gMnSc1	Evžen
Württemberský	Württemberský	k2eAgMnSc1d1	Württemberský
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
začíná	začínat	k5eAaImIp3nS	začínat
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Evžen	Evžen	k1gMnSc1	Evžen
vrátil	vrátit	k5eAaPmAgMnS	vrátit
sídlo	sídlo	k1gNnSc4	sídlo
rodu	rod	k1gInSc2	rod
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
vybudovat	vybudovat	k5eAaPmF	vybudovat
mnoho	mnoho	k4c4	mnoho
zámků	zámek	k1gInPc2	zámek
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
i	i	k9	i
jeho	jeho	k3xOp3gNnSc1	jeho
okolí	okolí	k1gNnSc1	okolí
<g/>
:	:	kIx,	:
nový	nový	k2eAgInSc1d1	nový
sídelní	sídelní	k2eAgInSc1d1	sídelní
zámek	zámek	k1gInSc1	zámek
(	(	kIx(	(
<g/>
Neues	Neues	k1gInSc1	Neues
Schloss	Schloss	k1gInSc1	Schloss
<g/>
)	)	kIx)	)
v	v	k7c4	v
1746	[number]	k4	1746
<g/>
,	,	kIx,	,
zámeček	zámeček	k1gInSc4	zámeček
Solitude	Solitud	k1gInSc5	Solitud
v	v	k7c4	v
1763	[number]	k4	1763
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Hohenheim	Hohenheim	k1gInSc1	Hohenheim
v	v	k7c6	v
1785	[number]	k4	1785
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
Karlsschule	Karlsschule	k1gFnSc2	Karlsschule
v	v	k7c4	v
1770	[number]	k4	1770
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
i	i	k9	i
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
německého	německý	k2eAgMnSc2d1	německý
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
dramatika	dramatik	k1gMnSc2	dramatik
Friedricha	Friedrich	k1gMnSc2	Friedrich
Schillera	Schiller	k1gMnSc2	Schiller
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
byl	být	k5eAaImAgInS	být
Karel	Karel	k1gMnSc1	Karel
Eugen	Eugna	k1gFnPc2	Eugna
patronem	patron	k1gInSc7	patron
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Stuttgart	Stuttgart	k1gInSc4	Stuttgart
i	i	k9	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
rozvoj	rozvoj	k1gInSc4	rozvoj
zůstal	zůstat	k5eAaPmAgInS	zůstat
převážně	převážně	k6eAd1	převážně
zemědělským	zemědělský	k2eAgNnSc7d1	zemědělské
městečkem	městečko	k1gNnSc7	městečko
s	s	k7c7	s
20	[number]	k4	20
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
podepsán	podepsán	k2eAgInSc4d1	podepsán
tzv.	tzv.	kA	tzv.
Prešpurský	prešpurský	k2eAgInSc4d1	prešpurský
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
povýšil	povýšit	k5eAaPmAgMnS	povýšit
Württemberské	Württemberský	k2eAgNnSc4d1	Württemberský
vévodství	vévodství	k1gNnSc4	vévodství
na	na	k7c6	na
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Království	království	k1gNnSc1	království
a	a	k8xC	a
Německé	německý	k2eAgNnSc1d1	německé
císařství	císařství	k1gNnSc1	císařství
===	===	k?	===
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
I.	I.	kA	I.
Württemberský	Württemberský	k2eAgInSc1d1	Württemberský
byl	být	k5eAaImAgInS	být
korunován	korunován	k2eAgInSc1d1	korunován
v	v	k7c6	v
1816	[number]	k4	1816
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhým	druhý	k4xOgNnSc7	druhý
králem	král	k1gMnSc7	král
a	a	k8xC	a
započal	započnout	k5eAaPmAgInS	započnout
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
výstavbou	výstavba	k1gFnSc7	výstavba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
například	například	k6eAd1	například
Vilémův	Vilémův	k2eAgInSc1d1	Vilémův
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Wilhelmspalais	Wilhelmspalais	k1gFnSc1	Wilhelmspalais
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
stuttgartské	stuttgartský	k2eAgFnSc2d1	Stuttgartská
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
katastrofální	katastrofální	k2eAgFnSc1d1	katastrofální
neúroda	neúroda	k1gFnSc1	neúroda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
král	král	k1gMnSc1	král
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Pavlovnou	Pavlovna	k1gFnSc7	Pavlovna
zmírnit	zmírnit	k5eAaPmF	zmírnit
sponzorováním	sponzorování	k1gNnSc7	sponzorování
oslav	oslava	k1gFnPc2	oslava
sklizně	sklizeň	k1gFnSc2	sklizeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
vznik	vznik	k1gInSc4	vznik
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
pivních	pivní	k2eAgInPc2d1	pivní
festivalů	festival	k1gInPc2	festival
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
tzv.	tzv.	kA	tzv.
Cannstatter	Cannstatter	k1gInSc1	Cannstatter
Volksfest	Volksfest	k1gInSc1	Volksfest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
dovedena	doveden	k2eAgFnSc1d1	dovedena
železnice	železnice	k1gFnSc1	železnice
a	a	k8xC	a
ve	v	k7c6	v
městě	město	k1gNnSc6	město
propukla	propuknout	k5eAaPmAgFnS	propuknout
naplno	naplno	k6eAd1	naplno
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vznikajícím	vznikající	k2eAgFnPc3d1	vznikající
továrnám	továrna	k1gFnPc3	továrna
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
začali	začít	k5eAaPmAgMnP	začít
stěhovat	stěhovat	k5eAaImF	stěhovat
dělníci	dělník	k1gMnPc1	dělník
s	s	k7c7	s
rodinami	rodina	k1gFnPc7	rodina
a	a	k8xC	a
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
chvíle	chvíle	k1gFnSc2	chvíle
ztrojnásobil	ztrojnásobit	k5eAaPmAgInS	ztrojnásobit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
1834	[number]	k4	1834
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
35	[number]	k4	35
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
přes	přes	k7c4	přes
91	[number]	k4	91
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
průkopníků	průkopník	k1gMnPc2	průkopník
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
jako	jako	k8xC	jako
Karl	Karla	k1gFnPc2	Karla
Benz	Benza	k1gFnPc2	Benza
<g/>
,	,	kIx,	,
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Daimler	Daimler	k1gMnSc1	Daimler
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Maybach	Maybach	k1gMnSc1	Maybach
či	či	k8xC	či
Robert	Robert	k1gMnSc1	Robert
Bosch	Bosch	kA	Bosch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
začlenění	začlenění	k1gNnSc2	začlenění
království	království	k1gNnSc2	království
do	do	k7c2	do
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
Německého	německý	k2eAgNnSc2d1	německé
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
První	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
podnikli	podniknout	k5eAaPmAgMnP	podniknout
spojenci	spojenec	k1gMnPc1	spojenec
dva	dva	k4xCgInPc4	dva
nálety	nálet	k1gInPc4	nálet
na	na	k7c4	na
město	město	k1gNnSc4	město
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
na	na	k7c4	na
místní	místní	k2eAgFnPc4d1	místní
kasárny	kasárny	k1gFnPc4	kasárny
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Německa	Německo	k1gNnSc2	Německo
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
tzv.	tzv.	kA	tzv.
Listopadová	listopadový	k2eAgFnSc1d1	listopadová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
království	království	k1gNnSc4	království
a	a	k8xC	a
zavedla	zavést	k5eAaPmAgFnS	zavést
novou	nový	k2eAgFnSc4d1	nová
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
stal	stát	k5eAaPmAgInS	stát
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
sídlem	sídlo	k1gNnSc7	sídlo
německé	německý	k2eAgFnSc2d1	německá
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
probíhal	probíhat	k5eAaImAgInS	probíhat
tzv.	tzv.	kA	tzv.
Kappův	Kappův	k2eAgInSc4d1	Kappův
puč	puč	k1gInSc4	puč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
nacismu	nacismus	k1gInSc2	nacismus
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
chopili	chopit	k5eAaPmAgMnP	chopit
moci	moct	k5eAaImF	moct
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nacisté	nacista	k1gMnPc1	nacista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zcela	zcela	k6eAd1	zcela
okleštili	okleštit	k5eAaPmAgMnP	okleštit
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
stuttgartské	stuttgartský	k2eAgFnSc2d1	Stuttgartská
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
průmyslu	průmysl	k1gInSc2	průmysl
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dařilo	dařit	k5eAaImAgNnS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
továrně	továrna	k1gFnSc6	továrna
podle	podle	k7c2	podle
modelu	model	k1gInSc2	model
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Porscheho	Porsche	k1gMnSc2	Porsche
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
první	první	k4xOgInSc1	první
prototyp	prototyp	k1gInSc1	prototyp
známého	známý	k2eAgInSc2d1	známý
Volkswagenu	volkswagen	k1gInSc2	volkswagen
Brouka	brouk	k1gMnSc4	brouk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Křišťálové	křišťálový	k2eAgFnSc2d1	Křišťálová
noci	noc	k1gFnSc2	noc
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
vypálena	vypálen	k2eAgFnSc1d1	vypálena
místní	místní	k2eAgFnSc1d1	místní
synagoga	synagoga	k1gFnSc1	synagoga
a	a	k8xC	a
zničen	zničen	k2eAgInSc1d1	zničen
židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
všichni	všechen	k3xTgMnPc1	všechen
židovští	židovský	k2eAgMnPc1d1	židovský
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
posíláni	posílat	k5eAaImNgMnP	posílat
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
táboru	tábor	k1gInSc2	tábor
v	v	k7c6	v
Dachau	Dachaus	k1gInSc6	Dachaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
parku	park	k1gInSc2	park
Killesberg	Killesberg	k1gMnSc1	Killesberg
židovské	židovský	k2eAgNnSc4d1	Židovské
gheto	gheto	k1gNnSc4	gheto
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
Židé	Žid	k1gMnPc1	Žid
putovali	putovat	k5eAaImAgMnP	putovat
kromě	kromě	k7c2	kromě
Osvětimi	Osvětim	k1gFnSc2	Osvětim
i	i	k9	i
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
cílem	cíl	k1gInSc7	cíl
častého	častý	k2eAgNnSc2d1	časté
bombardování	bombardování	k1gNnSc2	bombardování
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
spolehnout	spolehnout	k5eAaPmF	spolehnout
na	na	k7c4	na
velice	velice	k6eAd1	velice
efektivní	efektivní	k2eAgFnSc4d1	efektivní
protiletadlovou	protiletadlový	k2eAgFnSc4d1	protiletadlová
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
letci	letec	k1gMnSc3	letec
RAF	raf	k0	raf
považovali	považovat	k5eAaImAgMnP	považovat
denní	denní	k2eAgInSc4d1	denní
útok	útok	k1gInSc4	útok
na	na	k7c4	na
město	město	k1gNnSc4	město
za	za	k7c4	za
jistou	jistý	k2eAgFnSc4d1	jistá
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spousta	spousta	k1gFnSc1	spousta
mužů	muž	k1gMnPc2	muž
musela	muset	k5eAaImAgFnS	muset
odejít	odejít	k5eAaPmF	odejít
bojovat	bojovat	k5eAaImF	bojovat
na	na	k7c4	na
Východní	východní	k2eAgFnSc4d1	východní
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
celé	celá	k1gFnSc6	celá
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
lehlo	lehnout	k5eAaPmAgNnS	lehnout
popelem	popel	k1gInSc7	popel
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
na	na	k7c4	na
Stuttgart	Stuttgart	k1gInSc4	Stuttgart
53	[number]	k4	53
náletů	nálet	k1gInPc2	nálet
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
srovnaly	srovnat	k5eAaPmAgInP	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
<g/>
%	%	kIx~	%
budov	budova	k1gFnPc2	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
obsadili	obsadit	k5eAaPmAgMnP	obsadit
město	město	k1gNnSc4	město
francouzské	francouzský	k2eAgFnSc2d1	francouzská
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
trosek	troska	k1gFnPc2	troska
uměle	uměle	k6eAd1	uměle
navýšen	navýšit	k5eAaPmNgInS	navýšit
kopec	kopec	k1gInSc1	kopec
Birkenkopf	Birkenkopf	k1gInSc1	Birkenkopf
(	(	kIx(	(
<g/>
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
470	[number]	k4	470
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
na	na	k7c4	na
současných	současný	k2eAgFnPc2d1	současná
511	[number]	k4	511
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k9	jako
památník	památník	k1gInSc4	památník
obyvatelům	obyvatel	k1gMnPc3	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
při	při	k7c6	při
náletech	nálet	k1gInPc6	nálet
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
a	a	k8xC	a
období	období	k1gNnSc1	období
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
===	===	k?	===
</s>
</p>
<p>
<s>
Bývalé	bývalý	k2eAgNnSc1d1	bývalé
Württemberské	Württemberský	k2eAgNnSc1d1	Württemberský
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
rozděleno	rozdělen	k2eAgNnSc4d1	rozděleno
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
jižní	jižní	k2eAgFnSc4d1	jižní
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
a	a	k8xC	a
severní	severní	k2eAgFnSc4d1	severní
americkou	americký	k2eAgFnSc4d1	americká
okupační	okupační	k2eAgFnSc4d1	okupační
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
připadl	připadnout	k5eAaPmAgInS	připadnout
do	do	k7c2	do
zóny	zóna	k1gFnSc2	zóna
americké	americký	k2eAgFnSc2d1	americká
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Württembersko-Bádensko	Württembersko-Bádensko	k1gNnSc4	Württembersko-Bádensko
pod	pod	k7c7	pod
americkou	americký	k2eAgFnSc7d1	americká
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Poválečná	poválečný	k2eAgNnPc1d1	poválečné
léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
obnovou	obnova	k1gFnSc7	obnova
zničeného	zničený	k2eAgNnSc2d1	zničené
města	město	k1gNnSc2	město
a	a	k8xC	a
postavou	postava	k1gFnSc7	postava
starosty	starosta	k1gMnSc2	starosta
Arnulfa	Arnulf	k1gMnSc2	Arnulf
Kletta	Klett	k1gMnSc2	Klett
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
udělat	udělat	k5eAaPmF	udělat
ze	z	k7c2	z
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
moderní	moderní	k2eAgNnSc1d1	moderní
město	město	k1gNnSc1	město
automobilismu	automobilismus	k1gInSc2	automobilismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
koncept	koncept	k1gInSc1	koncept
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
funkcionalistické	funkcionalistický	k2eAgFnSc2d1	funkcionalistická
Athénské	athénský	k2eAgFnSc2d1	Athénská
charty	charta	k1gFnSc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Klett	Klett	k1gMnSc1	Klett
nechal	nechat	k5eAaPmAgMnS	nechat
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
koncept	koncept	k1gInSc4	koncept
města	město	k1gNnSc2	město
odklidit	odklidit	k5eAaPmF	odklidit
celé	celý	k2eAgFnSc3d1	celá
čtvrti	čtvrt	k1gFnSc3	čtvrt
trosek	troska	k1gFnPc2	troska
a	a	k8xC	a
zbourat	zbourat	k5eAaPmF	zbourat
i	i	k9	i
neponičené	poničený	k2eNgFnPc4d1	neponičená
předválečné	předválečný	k2eAgFnPc4d1	předválečná
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
kontroverzní	kontroverzní	k2eAgNnSc1d1	kontroverzní
se	se	k3xPyFc4	se
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlediska	hledisko	k1gNnSc2	hledisko
jeví	jevit	k5eAaImIp3nS	jevit
demolice	demolice	k1gFnSc1	demolice
původní	původní	k2eAgFnSc2d1	původní
radnice	radnice	k1gFnSc2	radnice
vystavěné	vystavěný	k2eAgFnSc2d1	vystavěná
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
vlámské	vlámský	k2eAgFnSc2d1	vlámská
gotiky	gotika	k1gFnSc2	gotika
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
postavené	postavený	k2eAgFnPc1d1	postavená
budovy	budova	k1gFnPc1	budova
již	již	k6eAd1	již
nedostaly	dostat	k5eNaPmAgFnP	dostat
předválečný	předválečný	k2eAgInSc4d1	předválečný
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
městě	město	k1gNnSc6	město
začal	začít	k5eAaPmAgInS	začít
dominovat	dominovat	k5eAaImF	dominovat
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vládního	vládní	k2eAgNnSc2d1	vládní
období	období	k1gNnSc2	období
A.	A.	kA	A.
Kletta	Kletta	k1gFnSc1	Kletta
byla	být	k5eAaImAgFnS	být
výrazně	výrazně	k6eAd1	výrazně
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
navázána	navázán	k2eAgFnSc1d1	navázána
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
francouzským	francouzský	k2eAgInSc7d1	francouzský
Štrasburkem	Štrasburk	k1gInSc7	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
Německo-francouzská	německorancouzský	k2eAgFnSc1d1	německo-francouzská
spolupráce	spolupráce	k1gFnSc1	spolupráce
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
zavítal	zavítat	k5eAaPmAgMnS	zavítat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Gaulle	Gaulle	k1gFnSc2	Gaulle
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
v	v	k7c6	v
Ludwigsburgu	Ludwigsburg	k1gInSc6	Ludwigsburg
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
ho	on	k3xPp3gInSc4	on
následovala	následovat	k5eAaImAgFnS	následovat
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
SRN	srna	k1gFnPc2	srna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
neúspěšně	úspěšně	k6eNd1	úspěšně
ucházel	ucházet	k5eAaImAgInS	ucházet
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
,	,	kIx,	,
Kasselu	Kassel	k1gInSc2	Kassel
a	a	k8xC	a
Bonnu	Bonn	k1gInSc2	Bonn
o	o	k7c4	o
post	post	k1gInSc4	post
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
spolková	spolkový	k2eAgFnSc1d1	spolková
země	země	k1gFnSc1	země
Bádensko-Württembersko	Bádensko-Württembersko	k1gNnSc1	Bádensko-Württembersko
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
sloučením	sloučení	k1gNnSc7	sloučení
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc2d1	francouzská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
a	a	k8xC	a
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jejím	její	k3xOp3gNnSc7	její
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
města	město	k1gNnSc2	město
začala	začít	k5eAaPmAgFnS	začít
narůstat	narůstat	k5eAaImF	narůstat
hned	hned	k6eAd1	hned
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
díky	díky	k7c3	díky
příchodu	příchod	k1gInSc3	příchod
vysídlených	vysídlený	k2eAgMnPc2d1	vysídlený
německých	německý	k2eAgMnPc2d1	německý
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
další	další	k2eAgMnPc1d1	další
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
místních	místní	k2eAgFnPc2d1	místní
továren	továrna	k1gFnPc2	továrna
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
Hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
zázraku	zázrak	k1gInSc2	zázrak
dělníci	dělník	k1gMnPc1	dělník
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
ekonomickým	ekonomický	k2eAgMnPc3d1	ekonomický
migrantům	migrant	k1gMnPc3	migrant
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
Gastarbeiteři	Gastarbeiter	k1gMnPc1	Gastarbeiter
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
již	již	k6eAd1	již
zůstali	zůstat	k5eAaPmAgMnP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
640	[number]	k4	640
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
dějištěm	dějiště	k1gNnSc7	dějiště
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejkontroverznější	kontroverzní	k2eAgFnSc2d3	nejkontroverznější
události	událost	k1gFnSc2	událost
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
věznici	věznice	k1gFnSc4	věznice
Stammheim	Stammheima	k1gFnPc2	Stammheima
byli	být	k5eAaImAgMnP	být
umístěni	umístěn	k2eAgMnPc1d1	umístěn
členové	člen	k1gMnPc1	člen
komunistické	komunistický	k2eAgFnSc2d1	komunistická
teroristické	teroristický	k2eAgFnSc2d1	teroristická
organizace	organizace	k1gFnSc2	organizace
Frakce	frakce	k1gFnSc2	frakce
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
soud	soud	k1gInSc1	soud
s	s	k7c7	s
předními	přední	k2eAgMnPc7d1	přední
představiteli	představitel	k1gMnPc7	představitel
této	tento	k3xDgFnSc6	tento
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
Ulrike	Ulrike	k1gFnSc1	Ulrike
Meinhof	Meinhof	k1gMnSc1	Meinhof
<g/>
,	,	kIx,	,
Andreas	Andreas	k1gMnSc1	Andreas
Baader	Baader	k1gMnSc1	Baader
<g/>
,	,	kIx,	,
Gudrun	Gudrun	k1gMnSc1	Gudrun
Ensslin	Ensslin	k2eAgMnSc1d1	Ensslin
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jan-Carl	Jan-Carl	k1gInSc1	Jan-Carl
Raspe	Rasp	k1gInSc5	Rasp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozsudku	rozsudek	k1gInSc3	rozsudek
se	se	k3xPyFc4	se
ale	ale	k9	ale
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
jmenovaných	jmenovaný	k1gMnPc2	jmenovaný
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
spáchali	spáchat	k5eAaPmAgMnP	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
událost	událost	k1gFnSc1	událost
vešla	vejít	k5eAaPmAgFnS	vejít
do	do	k7c2	do
známosti	známost	k1gFnSc2	známost
jako	jako	k8xC	jako
Todesnacht	Todesnacht	k2eAgInSc1d1	Todesnacht
von	von	k1gInSc1	von
Stammheim	Stammheima	k1gFnPc2	Stammheima
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Noc	noc	k1gFnSc1	noc
smrti	smrt	k1gFnSc2	smrt
ve	v	k7c6	v
Stammheimu	Stammheim	k1gInSc6	Stammheim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
hostil	hostit	k5eAaImAgInS	hostit
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
domácí	domácí	k2eAgInPc1d1	domácí
tým	tým	k1gInSc1	tým
získal	získat	k5eAaPmAgInS	získat
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
Michail	Michail	k1gMnSc1	Michail
Gorbačov	Gorbačov	k1gInSc4	Gorbačov
navštívil	navštívit	k5eAaPmAgMnS	navštívit
město	město	k1gNnSc1	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
Západním	západní	k2eAgNnSc7d1	západní
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
události	událost	k1gFnPc1	událost
hýbaly	hýbat	k5eAaImAgFnP	hýbat
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
města	město	k1gNnSc2	město
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
neúspěšně	úspěšně	k6eNd1	úspěšně
ucházel	ucházet	k5eAaImAgInS	ucházet
o	o	k7c6	o
pořádání	pořádání	k1gNnSc6	pořádání
LOH	LOH	kA	LOH
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Německo	Německo	k1gNnSc4	Německo
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
kandidát	kandidát	k1gMnSc1	kandidát
nakonec	nakonec	k6eAd1	nakonec
vybráno	vybrán	k2eAgNnSc4d1	vybráno
Lipsko	Lipsko	k1gNnSc4	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
opět	opět	k6eAd1	opět
z	z	k7c2	z
části	část	k1gFnSc2	část
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
místním	místní	k2eAgInSc7d1	místní
politikou	politika	k1gFnSc7	politika
hýbat	hýbat	k5eAaImF	hýbat
velký	velký	k2eAgInSc4d1	velký
urbanistický	urbanistický	k2eAgInSc4d1	urbanistický
projekt	projekt	k1gInSc4	projekt
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
21	[number]	k4	21
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
přetavbu	přetavba	k1gFnSc4	přetavba
celého	celý	k2eAgInSc2d1	celý
železničního	železniční	k2eAgInSc2d1	železniční
uzlu	uzel	k1gInSc2	uzel
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
vyšplhá	vyšplhat	k5eAaPmIp3nS	vyšplhat
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
121	[number]	k4	121
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
plánu	plán	k1gInSc3	plán
se	se	k3xPyFc4	se
postavilo	postavit	k5eAaPmAgNnS	postavit
mnoho	mnoho	k4c1	mnoho
odpůrců	odpůrce	k1gMnPc2	odpůrce
a	a	k8xC	a
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
měly	mít	k5eAaImAgFnP	mít
demonstrace	demonstrace	k1gFnPc1	demonstrace
čtvrt	čtvrt	k1xP	čtvrt
miliónu	milión	k4xCgInSc2	milión
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Dík	dík	k7c3	dík
protestům	protest	k1gInPc3	protest
padla	padnout	k5eAaImAgFnS	padnout
místní	místní	k2eAgFnSc1d1	místní
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
dostala	dostat	k5eAaPmAgFnS	dostat
Strana	strana	k1gFnSc1	strana
zelených	zelený	k2eAgMnPc2d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
pozměněn	pozměněn	k2eAgInSc1d1	pozměněn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
úplnému	úplný	k2eAgNnSc3d1	úplné
zastavení	zastavení	k1gNnSc3	zastavení
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Silvestra	Silvestr	k1gMnSc4	Silvestr
2015	[number]	k4	2015
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
nevyhnulo	vyhnout	k5eNaPmAgNnS	vyhnout
hromadné	hromadný	k2eAgNnSc4d1	hromadné
sexuální	sexuální	k2eAgNnSc4d1	sexuální
obtěžování	obtěžování	k1gNnSc4	obtěžování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
páchaly	páchat	k5eAaImAgFnP	páchat
skupiny	skupina	k1gFnPc1	skupina
mužů	muž	k1gMnPc2	muž
arabského	arabský	k2eAgMnSc2d1	arabský
a	a	k8xC	a
severoafrického	severoafrický	k2eAgInSc2d1	severoafrický
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
těžce	těžce	k6eAd1	těžce
bombardováno	bombardován	k2eAgNnSc1d1	bombardováno
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
památky	památka	k1gFnPc1	památka
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
zrekonstruovány	zrekonstruován	k2eAgInPc1d1	zrekonstruován
<g/>
.	.	kIx.	.
</s>
<s>
Drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
jich	on	k3xPp3gFnPc2	on
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
okolo	okolo	k7c2	okolo
náměstí	náměstí	k1gNnSc2	náměstí
Schlossplatz	Schlossplatza	k1gFnPc2	Schlossplatza
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
může	moct	k5eAaImIp3nS	moct
nabídnout	nabídnout	k5eAaPmF	nabídnout
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
architektonických	architektonický	k2eAgInPc2d1	architektonický
skvostů	skvost	k1gInPc2	skvost
i	i	k9	i
z	z	k7c2	z
poválečné	poválečný	k2eAgFnSc2d1	poválečná
architektury	architektura	k1gFnSc2	architektura
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
funkcionalistickém	funkcionalistický	k2eAgInSc6d1	funkcionalistický
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
znalce	znalec	k1gMnPc4	znalec
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nachází	nacházet	k5eAaImIp3nS	nacházet
památka	památka	k1gFnSc1	památka
UNESCO	UNESCO	kA	UNESCO
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
kolonie	kolonie	k1gFnSc1	kolonie
Weissenhof	Weissenhof	k1gInSc4	Weissenhof
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
zrekonstruované	zrekonstruovaný	k2eAgFnPc4d1	zrekonstruovaná
památky	památka	k1gFnPc4	památka
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Stiftskirche	Stiftskirche	k1gInSc1	Stiftskirche
–	–	k?	–
protestanský	protestanský	k2eAgInSc1d1	protestanský
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
zrekonstruovaný	zrekonstruovaný	k2eAgInSc1d1	zrekonstruovaný
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
</s>
</p>
<p>
<s>
Altes	Altes	k1gInSc1	Altes
Schloss	Schloss	k1gInSc1	Schloss
(	(	kIx(	(
<g/>
Starý	starý	k2eAgInSc1d1	starý
zámek	zámek	k1gInSc1	zámek
<g/>
)	)	kIx)	)
–	–	k?	–
renesanční	renesanční	k2eAgFnSc1d1	renesanční
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
muzeum	muzeum	k1gNnSc1	muzeum
</s>
</p>
<p>
<s>
Alte	alt	k1gInSc5	alt
Kanzelei	Kanzelei	k1gNnPc1	Kanzelei
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgNnPc1d1	Staré
kancléřství	kancléřství	k1gNnPc1	kancléřství
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neues	Neues	k1gInSc1	Neues
Schloss	Schloss	k1gInSc1	Schloss
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
hrad	hrad	k1gInSc1	hrad
<g/>
)	)	kIx)	)
–	–	k?	–
barokní	barokní	k2eAgInSc4d1	barokní
palác	palác	k1gInSc4	palác
s	s	k7c7	s
modernizovaným	modernizovaný	k2eAgInSc7d1	modernizovaný
interiérem	interiér	k1gInSc7	interiér
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
<s>
Großes	Großes	k1gInSc1	Großes
Haus	Hausa	k1gFnPc2	Hausa
stuttgartského	stuttgartský	k2eAgNnSc2d1	stuttgartské
divadlaK	divadlaK	k?	divadlaK
příkladům	příklad	k1gInPc3	příklad
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
ve	v	k7c6	v
městě	město	k1gNnSc6	město
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
čtvrť	čtvrť	k1gFnSc1	čtvrť
Weissenhof	Weissenhof	k1gInSc1	Weissenhof
–	–	k?	–
památka	památka	k1gFnSc1	památka
UNESCO	UNESCO	kA	UNESCO
od	od	k7c2	od
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Mies	Mies	k1gInSc4	Mies
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Rohe	Roh	k1gFnSc2	Roh
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc1	Tugendhat
</s>
</p>
<p>
<s>
Hauptbahnhof	Hauptbahnhof	k1gInSc1	Hauptbahnhof
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
vlakové	vlakový	k2eAgNnSc1d1	vlakové
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rathaus	rathaus	k1gInSc1	rathaus
(	(	kIx(	(
<g/>
radnice	radnice	k1gFnSc1	radnice
<g/>
)	)	kIx)	)
–	–	k?	–
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
</s>
</p>
<p>
<s>
Fernsehturm	Fernsehturm	k1gInSc1	Fernsehturm
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgFnSc1d1	televizní
věž	věž	k1gFnSc1	věž
<g/>
)	)	kIx)	)
–	–	k?	–
první	první	k4xOgFnSc1	první
věž	věž	k1gFnSc1	věž
na	na	k7c6	na
světě	svět	k1gInSc6	svět
postavená	postavený	k2eAgFnSc1d1	postavená
z	z	k7c2	z
betonu	beton	k1gInSc2	beton
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956V	[number]	k4	1956V
blízkosti	blízkost	k1gFnSc2	blízkost
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
krále	král	k1gMnSc2	král
Viléma	Vilém	k1gMnSc2	Vilém
I.	I.	kA	I.
postaveny	postaven	k2eAgInPc1d1	postaven
paláce	palác	k1gInPc1	palác
a	a	k8xC	a
zámky	zámek	k1gInPc1	zámek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Barokně-rokokový	Barokněokokový	k2eAgInSc4d1	Barokně-rokokový
zámeček	zámeček	k1gInSc4	zámeček
Solitude	Solitud	k1gMnSc5	Solitud
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
palác	palác	k1gInSc1	palác
Ludwigsburg	Ludwigsburg	k1gInSc1	Ludwigsburg
se	s	k7c7	s
zahradou	zahrada	k1gFnSc7	zahrada
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Hohenheim	Hohenheima	k1gFnPc2	Hohenheima
</s>
</p>
<p>
<s>
Württemberské	Württemberský	k2eAgNnSc1d1	Württemberský
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
(	(	kIx(	(
<g/>
Grabkapelle	Grabkapelle	k1gFnSc1	Grabkapelle
auf	auf	k?	auf
dem	dem	k?	dem
Württemberg	Württemberg	k1gInSc1	Württemberg
<g/>
)	)	kIx)	)
–	–	k?	–
hrobka	hrobka	k1gFnSc1	hrobka
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Pavlovny	Pavlovna	k1gFnSc2	Pavlovna
a	a	k8xC	a
Viléma	Viléma	k1gFnSc1	Viléma
I.	I.	kA	I.
položená	položená	k1gFnSc1	položená
mezi	mezi	k7c7	mezi
vinohrady	vinohrad	k1gInPc7	vinohrad
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Populace	populace	k1gFnPc4	populace
a	a	k8xC	a
menšiny	menšina	k1gFnPc4	menšina
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
zdevastované	zdevastovaný	k2eAgNnSc1d1	zdevastované
válkou	válka	k1gFnSc7	válka
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
i	i	k8xC	i
velké	velký	k2eAgFnPc1d1	velká
ztráty	ztráta	k1gFnPc1	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
klesl	klesnout	k5eAaPmAgInS	klesnout
z	z	k7c2	z
půl	půl	k1xP	půl
miliónu	milión	k4xCgInSc2	milión
na	na	k7c4	na
necelých	celý	k2eNgInPc2d1	necelý
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
ale	ale	k8xC	ale
začala	začít	k5eAaPmAgFnS	začít
populace	populace	k1gFnSc1	populace
razantně	razantně	k6eAd1	razantně
narůstat	narůstat	k5eAaImF	narůstat
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
vysídlených	vysídlený	k2eAgMnPc2d1	vysídlený
německých	německý	k2eAgMnPc2d1	německý
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zlepšující	zlepšující	k2eAgFnSc7d1	zlepšující
se	se	k3xPyFc4	se
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
situací	situace	k1gFnSc7	situace
nebylo	být	k5eNaImAgNnS	být
pracovní	pracovní	k2eAgFnPc4d1	pracovní
síly	síla	k1gFnPc4	síla
nazbyt	nazbyt	k6eAd1	nazbyt
a	a	k8xC	a
do	do	k7c2	do
místních	místní	k2eAgFnPc2d1	místní
továren	továrna	k1gFnPc2	továrna
začali	začít	k5eAaPmAgMnP	začít
přicházet	přicházet	k5eAaImF	přicházet
dělníci	dělník	k1gMnPc1	dělník
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
pak	pak	k6eAd1	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
(	(	kIx(	(
<g/>
637	[number]	k4	637
539	[number]	k4	539
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
586	[number]	k4	586
978	[number]	k4	978
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
klesající	klesající	k2eAgFnSc4d1	klesající
tendenci	tendence	k1gFnSc4	tendence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
před	před	k7c7	před
etnickými	etnický	k2eAgFnPc7d1	etnická
čistkami	čistka	k1gFnPc7	čistka
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
domovině	domovina	k1gFnSc6	domovina
do	do	k7c2	do
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
mnoho	mnoho	k4c1	mnoho
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Bosňáků	Bosňáků	k?	Bosňáků
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
nabídka	nabídka	k1gFnSc1	nabídka
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
a	a	k8xC	a
lepšího	dobrý	k2eAgNnSc2d2	lepší
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
na	na	k7c6	na
místních	místní	k2eAgFnPc6d1	místní
školách	škola	k1gFnPc6	škola
trend	trend	k1gInSc4	trend
v	v	k7c6	v
úbytku	úbytek	k1gInSc6	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
obrátila	obrátit	k5eAaPmAgFnS	obrátit
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
přistěhovává	přistěhovávat	k5eAaImIp3nS	přistěhovávat
do	do	k7c2	do
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
mladí	mladý	k2eAgMnPc1d1	mladý
Němci	Němec	k1gMnPc1	Němec
narození	narození	k1gNnSc2	narození
v	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
Východním	východní	k2eAgNnSc6d1	východní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
velkých	velký	k2eAgNnPc6d1	velké
německých	německý	k2eAgNnPc6d1	německé
městech	město	k1gNnPc6	město
i	i	k9	i
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
jsou	být	k5eAaImIp3nP	být
umístěni	umístěn	k2eAgMnPc1d1	umístěn
uprchlíci	uprchlík	k1gMnPc1	uprchlík
ze	z	k7c2	z
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgFnSc7d3	veliký
národnostní	národnostní	k2eAgFnSc7d1	národnostní
menšinou	menšina	k1gFnSc7	menšina
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ale	ale	k9	ale
sečetl	sečíst	k5eAaPmAgInS	sečíst
počet	počet	k1gInSc1	počet
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
(	(	kIx(	(
<g/>
Chorvati	Chorvat	k1gMnPc1	Chorvat
<g/>
,	,	kIx,	,
Srbové	Srb	k1gMnPc1	Srb
<g/>
,	,	kIx,	,
Bosňáci	Bosňáci	k?	Bosňáci
a	a	k8xC	a
Kosovští	kosovský	k2eAgMnPc1d1	kosovský
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
převýšil	převýšit	k5eAaPmAgInS	převýšit
by	by	kYmCp3nS	by
i	i	k8xC	i
počet	počet	k1gInSc1	počet
Turků	Turek	k1gMnPc2	Turek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
místního	místní	k2eAgNnSc2d1	místní
křesťanství	křesťanství	k1gNnSc2	křesťanství
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
reformací	reformace	k1gFnSc7	reformace
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
celé	celý	k2eAgNnSc4d1	celé
Württembersko	Württembersko	k1gNnSc4	Württembersko
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
protestanské	protestanský	k2eAgFnSc2d1	protestanská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
nejvýrazněji	výrazně	k6eAd3	výrazně
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
protestanty	protestant	k1gMnPc7	protestant
začal	začít	k5eAaPmAgInS	začít
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
k	k	k7c3	k
protestanství	protestanství	k1gNnSc3	protestanství
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
25,3	[number]	k4	25,3
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
a	a	k8xC	a
23,6	[number]	k4	23,6
%	%	kIx~	%
tvořili	tvořit	k5eAaImAgMnP	tvořit
katolíci	katolík	k1gMnPc1	katolík
(	(	kIx(	(
<g/>
zbytek	zbytek	k1gInSc1	zbytek
tvořili	tvořit	k5eAaImAgMnP	tvořit
nevěřící	nevěřící	k1gFnPc4	nevěřící
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protestanská	Protestanský	k2eAgFnSc1d1	Protestanská
církev	církev	k1gFnSc1	církev
je	být	k5eAaImIp3nS	být
zastřešena	zastřešit	k5eAaPmNgFnS	zastřešit
Protestansko-luteránskou	Protestanskouteránský	k2eAgFnSc7d1	Protestansko-luteránský
církví	církev	k1gFnSc7	církev
Württemberska	Württembersko	k1gNnSc2	Württembersko
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
stánkem	stánek	k1gInSc7	stánek
je	být	k5eAaImIp3nS	být
Stiftskirche	Stiftskirche	k1gFnSc1	Stiftskirche
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
je	být	k5eAaImIp3nS	být
reprezentována	reprezentovat	k5eAaImNgFnS	reprezentovat
diecézí	diecéze	k1gFnSc7	diecéze
Rottenburg-Stuttgart	Rottenburg-Stuttgart	k1gInSc1	Rottenburg-Stuttgart
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Rottenburgu	Rottenburg	k1gInSc6	Rottenburg
<g/>
,	,	kIx,	,
50	[number]	k4	50
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1498	[number]	k4	1498
-	-	kIx~	-
1805	[number]	k4	1805
nesměli	smět	k5eNaImAgMnP	smět
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pracovat	pracovat	k5eAaImF	pracovat
ani	ani	k8xC	ani
permanentně	permanentně	k6eAd1	permanentně
bydlet	bydlet	k5eAaImF	bydlet
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
nemohla	moct	k5eNaImAgFnS	moct
nijak	nijak	k6eAd1	nijak
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
tedy	tedy	k9	tedy
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nijak	nijak	k6eAd1	nijak
silná	silný	k2eAgFnSc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
nacismu	nacismus	k1gInSc2	nacismus
většina	většina	k1gFnSc1	většina
Židů	Žid	k1gMnPc2	Žid
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
okolo	okolo	k7c2	okolo
1	[number]	k4	1
200	[number]	k4	200
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
komunity	komunita	k1gFnSc2	komunita
4	[number]	k4	4
500	[number]	k4	500
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
jich	on	k3xPp3gFnPc2	on
našlo	najít	k5eAaPmAgNnS	najít
smrt	smrt	k1gFnSc4	smrt
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
synagog	synagoga	k1gFnPc2	synagoga
v	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
komunita	komunita	k1gFnSc1	komunita
začala	začít	k5eAaPmAgFnS	začít
opět	opět	k6eAd1	opět
rozrůstat	rozrůstat	k5eAaImF	rozrůstat
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
okolo	okolo	k7c2	okolo
4	[number]	k4	4
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muslimská	muslimský	k2eAgFnSc1d1	muslimská
komunita	komunita	k1gFnSc1	komunita
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
gastarbeiterů	gastarbeiter	k1gInPc2	gastarbeiter
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
uprchlíků	uprchlík	k1gMnPc2	uprchlík
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
cca	cca	kA	cca
65	[number]	k4	65
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c4	na
21	[number]	k4	21
mešit	mešita	k1gFnPc2	mešita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Místní	místní	k2eAgFnSc1d1	místní
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
politiky	politik	k1gMnPc4	politik
poválečného	poválečný	k2eAgInSc2d1	poválečný
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
pochyby	pochyba	k1gFnSc2	pochyba
řadí	řadit	k5eAaImIp3nS	řadit
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
starosta	starosta	k1gMnSc1	starosta
Arnulf	Arnulf	k1gMnSc1	Arnulf
Klett	Klett	k1gMnSc1	Klett
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
starosty	starosta	k1gMnSc2	starosta
města	město	k1gNnSc2	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Poválečná	poválečný	k2eAgNnPc4d1	poválečné
léta	léto	k1gNnPc4	léto
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
politického	politický	k2eAgNnSc2d1	politické
hlediska	hledisko	k1gNnSc2	hledisko
léty	léto	k1gNnPc7	léto
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
desetkrát	desetkrát	k6eAd1	desetkrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
místní	místní	k2eAgFnPc4d1	místní
volby	volba	k1gFnPc4	volba
křesťanští	křesťanštět	k5eAaImIp3nP	křesťanštět
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
vyhrávali	vyhrávat	k5eAaImAgMnP	vyhrávat
místní	místní	k2eAgFnPc4d1	místní
volby	volba	k1gFnPc4	volba
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
vystřídáni	vystřídán	k2eAgMnPc1d1	vystřídán
stranou	stranou	k6eAd1	stranou
Zelených	Zelených	k2eAgMnPc1d1	Zelených
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
úplně	úplně	k6eAd1	úplně
první	první	k4xOgInSc4	první
úspěch	úspěch	k1gInSc4	úspěch
Zelených	Zelených	k2eAgInSc4d1	Zelených
ve	v	k7c6	v
městě	město	k1gNnSc6	město
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgMnSc3	který
Zeleným	zelený	k2eAgMnPc3d1	zelený
pomohl	pomoct	k5eAaPmAgInS	pomoct
jejich	jejich	k3xOp3gInSc4	jejich
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
volební	volební	k2eAgInSc1d1	volební
úspěch	úspěch	k1gInSc1	úspěch
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
opět	opět	k6eAd1	opět
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
křesťanským	křesťanský	k2eAgMnPc3d1	křesťanský
demokratům	demokrat	k1gMnPc3	demokrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
23	[number]	k4	23
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
a	a	k8xC	a
vnější	vnější	k2eAgFnSc6d1	vnější
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
obvodů	obvod	k1gInPc2	obvod
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
a	a	k8xC	a
vnějších	vnější	k2eAgNnPc2d1	vnější
osmnáct	osmnáct	k4xCc4	osmnáct
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
obvod	obvod	k1gInSc1	obvod
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
volenou	volený	k2eAgFnSc4d1	volená
radu	rada	k1gFnSc4	rada
(	(	kIx(	(
<g/>
Bezirksbeirat	Bezirksbeirat	k1gInSc4	Bezirksbeirat
<g/>
)	)	kIx)	)
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Bezirksvorsteher	Bezirksvorstehra	k1gFnPc2	Bezirksvorstehra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
obdoba	obdoba	k1gFnSc1	obdoba
českého	český	k2eAgMnSc2d1	český
starosty	starosta	k1gMnSc2	starosta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
počty	počet	k1gInPc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
obvodech	obvod	k1gInPc6	obvod
v	v	k7c6	v
tisících	tisící	k4xOgInPc6	tisící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
obvody	obvod	k1gInPc1	obvod
<g/>
:	:	kIx,	:
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
-	-	kIx~	-
Centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
21,2	[number]	k4	21,2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
-	-	kIx~	-
Sever	sever	k1gInSc1	sever
(	(	kIx(	(
<g/>
24,7	[number]	k4	24,7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
-	-	kIx~	-
Východ	východ	k1gInSc1	východ
(	(	kIx(	(
<g/>
45,8	[number]	k4	45,8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
-	-	kIx~	-
Jih	jih	k1gInSc1	jih
(	(	kIx(	(
<g/>
42,5	[number]	k4	42,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
-	-	kIx~	-
Západ	západ	k1gInSc1	západ
(	(	kIx(	(
<g/>
49,6	[number]	k4	49,6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vnější	vnější	k2eAgInPc1d1	vnější
obvody	obvod	k1gInPc1	obvod
<g/>
:	:	kIx,	:
Bad	Bad	k1gFnSc1	Bad
Cannstatt	Cannstatt	k1gMnSc1	Cannstatt
(	(	kIx(	(
<g/>
66,1	[number]	k4	66,1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Birkach	Birkach	k1gMnSc1	Birkach
(	(	kIx(	(
<g/>
6,5	[number]	k4	6,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Botnang	Botnang	k1gMnSc1	Botnang
(	(	kIx(	(
<g/>
12,7	[number]	k4	12,7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Degerloch	Degerloch	k1gMnSc1	Degerloch
(	(	kIx(	(
<g/>
16,1	[number]	k4	16,1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Feuerbach	Feuerbach	k1gMnSc1	Feuerbach
(	(	kIx(	(
<g/>
27,4	[number]	k4	27,4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hedelfingen	Hedelfingen	k1gInSc1	Hedelfingen
(	(	kIx(	(
<g/>
9,1	[number]	k4	9,1
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Möhringen	Möhringen	k1gInSc1	Möhringen
(	(	kIx(	(
<g/>
29,5	[number]	k4	29,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mühlhausen	Mühlhausen	k1gInSc1	Mühlhausen
(	(	kIx(	(
<g/>
25,2	[number]	k4	25,2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Münster	Münster	k1gMnSc1	Münster
(	(	kIx(	(
<g/>
6,3	[number]	k4	6,3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Obertürkheim	Obertürkheim	k1gMnSc1	Obertürkheim
(	(	kIx(	(
<g/>
8,1	[number]	k4	8,1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Plieningen	Plieningen	k1gInSc1	Plieningen
(	(	kIx(	(
<g/>
12,5	[number]	k4	12,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sillenbuch	Sillenbuch	k1gMnSc1	Sillenbuch
(	(	kIx(	(
<g/>
23,2	[number]	k4	23,2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stammheim	Stammheim	k1gMnSc1	Stammheim
(	(	kIx(	(
<g/>
11,7	[number]	k4	11,7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Untertürkheim	Untertürkheim	k1gMnSc1	Untertürkheim
(	(	kIx(	(
<g/>
15,9	[number]	k4	15,9
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vaihingen	Vaihingen	k1gInSc1	Vaihingen
(	(	kIx(	(
<g/>
43,5	[number]	k4	43,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wangen	Wangen	k1gInSc1	Wangen
(	(	kIx(	(
<g/>
8,5	[number]	k4	8,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Weilimdorf	Weilimdorf	k1gMnSc1	Weilimdorf
(	(	kIx(	(
<g/>
30,7	[number]	k4	30,7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zuffenhausen	Zuffenhausen	k1gInSc1	Zuffenhausen
(	(	kIx(	(
<g/>
35,6	[number]	k4	35,6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Stuttgart	Stuttgart	k1gInSc4	Stuttgart
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jezdilo	jezdit	k5eAaImAgNnS	jezdit
taxi	taxe	k1gFnSc4	taxe
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vůz	vůz	k1gInSc4	vůz
Daimler	Daimler	k1gInSc1	Daimler
Victoria	Victorium	k1gNnSc2	Victorium
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zdejší	zdejší	k2eAgNnSc4d1	zdejší
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
zajíždějí	zajíždět	k5eAaImIp3nP	zajíždět
linky	linka	k1gFnPc1	linka
ICE	ICE	kA	ICE
(	(	kIx(	(
<g/>
Intercity-Express	Intercity-Express	k1gInSc1	Intercity-Express
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
také	také	k9	také
příměstské	příměstský	k2eAgInPc1d1	příměstský
vlaky	vlak	k1gInPc1	vlak
S-Bahn	S-Bahna	k1gFnPc2	S-Bahna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Německa	Německo	k1gNnSc2	Německo
i	i	k9	i
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
sportem	sport	k1gInSc7	sport
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc4d1	místní
klub	klub	k1gInSc4	klub
VfB	VfB	k1gMnPc2	VfB
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
je	být	k5eAaImIp3nS	být
pětinásobným	pětinásobný	k2eAgMnSc7d1	pětinásobný
mistrem	mistr	k1gMnSc7	mistr
německé	německý	k2eAgFnSc2d1	německá
Bundesligy	bundesliga	k1gFnSc2	bundesliga
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
titul	titul	k1gInSc1	titul
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
VfB	VfB	k?	VfB
hraje	hrát	k5eAaImIp3nS	hrát
své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
v	v	k7c6	v
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
Aréně	aréna	k1gFnSc3	aréna
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
60	[number]	k4	60
441	[number]	k4	441
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Aréna	aréna	k1gFnSc1	aréna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Bad	Bad	k1gMnSc1	Bad
Cannstatt	Cannstatt	k1gMnSc1	Cannstatt
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgNnSc7d1	časté
dějištěm	dějiště	k1gNnSc7	dějiště
kvalifikačních	kvalifikační	k2eAgInPc2d1	kvalifikační
zápasů	zápas	k1gInPc2	zápas
německé	německý	k2eAgFnSc2d1	německá
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhým	druhý	k4xOgInSc7	druhý
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
sportem	sport	k1gInSc7	sport
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
házená	házená	k1gFnSc1	házená
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
TV	TV	kA	TV
Bittenfeld	Bittenfeld	k1gInSc4	Bittenfeld
hrající	hrající	k2eAgInSc1d1	hrající
svá	svůj	k3xOyFgNnPc4	svůj
domácí	domácí	k2eAgNnPc4d1	domácí
utkání	utkání	k1gNnPc4	utkání
v	v	k7c6	v
Scharreně	Scharrena	k1gFnSc6	Scharrena
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vybraná	vybraný	k2eAgNnPc4d1	vybrané
utkání	utkání	k1gNnPc4	utkání
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
do	do	k7c2	do
větší	veliký	k2eAgNnSc1d2	veliký
Porsche	Porsche	k1gNnPc1	Porsche
Arény	aréna	k1gFnSc2	aréna
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
pořádalo	pořádat	k5eAaImAgNnS	pořádat
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
mužů	muž	k1gMnPc2	muž
2007	[number]	k4	2007
a	a	k8xC	a
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
stolním	stolní	k2eAgInSc6d1	stolní
tenise	tenis	k1gInSc6	tenis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
hraje	hrát	k5eAaImIp3nS	hrát
tenisový	tenisový	k2eAgInSc1d1	tenisový
turnaj	turnaj	k1gInSc1	turnaj
Porsche	Porsche	k1gNnSc1	Porsche
Tennis	Tennis	k1gInSc1	Tennis
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stuttgartský	stuttgartský	k2eAgInSc4d1	stuttgartský
hokej	hokej	k1gInSc4	hokej
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
tým	tým	k1gInSc1	tým
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
Rebels	Rebels	k1gInSc1	Rebels
EC	EC	kA	EC
hrájící	hrájící	k1gFnSc1	hrájící
Landesligu	Landeslig	k1gInSc2	Landeslig
(	(	kIx(	(
<g/>
německá	německý	k2eAgFnSc1d1	německá
4	[number]	k4	4
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stuttgartské	stuttgartský	k2eAgFnSc6d1	Stuttgartská
části	část	k1gFnSc6	část
Degerloch	Degerloch	k1gInSc4	Degerloch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Georg	Georg	k1gMnSc1	Georg
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Friedrich	Friedrich	k1gMnSc1	Friedrich
Hegel	Hegel	k1gMnSc1	Hegel
(	(	kIx(	(
<g/>
1770	[number]	k4	1770
<g/>
–	–	k?	–
<g/>
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Horkheimer	Horkheimer	k1gMnSc1	Horkheimer
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
</s>
</p>
<p>
<s>
Žofie	Žofie	k1gFnSc1	Žofie
Chotková	Chotková	k1gFnSc1	Chotková
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Františka	František	k1gMnSc2	František
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
von	von	k1gInSc4	von
Weizsäcker	Weizsäcker	k1gInSc1	Weizsäcker
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
prezident	prezident	k1gMnSc1	prezident
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
St	St	kA	St
Helens	Helens	k1gInSc1	Helens
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
</s>
</p>
<p>
<s>
Cardiff	Cardiff	k1gInSc1	Cardiff
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
</s>
</p>
<p>
<s>
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
Missouri	Missouri	k1gFnSc1	Missouri
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
</s>
</p>
<p>
<s>
Štrasburk	Štrasburk	k1gInSc1	Štrasburk
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
Bombaj	Bombaj	k1gFnSc1	Bombaj
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
Menzel	Menzet	k5eAaImAgMnS	Menzet
Bourguiba	Bourguiba	k1gMnSc1	Bourguiba
<g/>
,	,	kIx,	,
Tunisko	Tunisko	k1gNnSc1	Tunisko
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
</s>
</p>
<p>
<s>
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
Lodž	Lodž	k1gFnSc1	Lodž
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Samara	Samara	k1gFnSc1	Samara
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
StadtWiki	StadtWik	k1gFnPc1	StadtWik
Stuttgart	Stuttgart	k1gInSc4	Stuttgart
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
</s>
</p>
