<s>
Ve	v	k7c6
fyzice	fyzika	k1gFnSc6
je	být	k5eAaImIp3nS
proton	proton	k1gInSc1
subatomární	subatomární	k2eAgFnSc2d1
částice	částice	k1gFnSc2
s	s	k7c7
kladným	kladný	k2eAgInSc7d1
elementárním	elementární	k2eAgInSc7d1
elektrickým	elektrický	k2eAgInSc7d1
nábojem	náboj	k1gInSc7
tj.	tj.	kA
1,602	1,602	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
19	#num#	k4
C	C	kA
a	a	k8xC
klidovou	klidový	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
938	#num#	k4
MeV	MeV	k1gFnPc2
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
2	#num#	k4
(	(	kIx(
<g/>
1,67	1,67	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
27	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
odpovídající	odpovídající	k2eAgFnSc1d1
přibližně	přibližně	k6eAd1
hmotnosti	hmotnost	k1gFnSc3
1836	#num#	k4
elektronů	elektron	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>