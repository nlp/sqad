<s>
Proton	proton	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
částici	částice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Proton	proton	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Proton	proton	k1gInSc1
(	(	kIx(
<g/>
p	p	k?
<g/>
+	+	kIx~
<g/>
)	)	kIx)
Vnitřní	vnitřní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
protonu	proton	k1gInSc2
<g/>
:	:	kIx,
dva	dva	k4xCgInPc1
kvarky	kvark	k1gInPc1
u	u	k7c2
a	a	k8xC
jeden	jeden	k4xCgInSc4
kvark	kvark	k1gInSc4
d.	d.	k?
<g/>
Obecné	obecný	k2eAgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
Klasifikace	klasifikace	k1gFnSc2
</s>
<s>
HadronyBaryonyNukleony	HadronyBaryonyNukleona	k1gFnPc1
Složení	složení	k1gNnSc1
</s>
<s>
2	#num#	k4
kvarky	kvark	k1gInPc7
u	u	k7c2
<g/>
,	,	kIx,
1	#num#	k4
kvark	kvark	k1gInSc1
d	d	k?
Antičástice	antičástice	k1gFnSc2
</s>
<s>
antiproton	antiproton	k1gInSc1
Fyzikální	fyzikální	k2eAgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
Klidová	klidový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
938,272	938,272	k4
088	#num#	k4
16	#num#	k4
<g/>
(	(	kIx(
<g/>
29	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
MeV	MeV	k1gFnSc2
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
2	#num#	k4
1,672	1,672	k4
621	#num#	k4
923	#num#	k4
69	#num#	k4
<g/>
(	(	kIx(
<g/>
51	#num#	k4
<g/>
)	)	kIx)
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
27	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
kg	kg	kA
Elektrický	elektrický	k2eAgInSc1d1
náboj	náboj	k1gInSc1
</s>
<s>
+1	+1	k4
e	e	k0
1,602	1,602	k4
176	#num#	k4
634	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
19	#num#	k4
přesně	přesně	k6eAd1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
C	C	kA
Magnetický	magnetický	k2eAgInSc1d1
moment	moment	k1gInSc1
</s>
<s>
2,792	2,792	k4
847	#num#	k4
344	#num#	k4
63	#num#	k4
<g/>
(	(	kIx(
<g/>
82	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
μ	μ	k?
Dipólový	Dipólový	k2eAgInSc4d1
moment	moment	k1gInSc4
</s>
<s>
<	<	kIx(
2,1	2,1	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
25	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
e	e	k0
<g/>
·	·	k?
<g/>
cm	cm	kA
Spin	spin	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
⁄	⁄	k?
<g/>
2	#num#	k4
Izospin	Izospin	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
⁄	⁄	k?
<g/>
2	#num#	k4
Stř	Stř	k1gFnSc1
<g/>
.	.	kIx.
doba	doba	k1gFnSc1
života	život	k1gInSc2
</s>
<s>
>	>	kIx)
1031	#num#	k4
roků	rok	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
stabilní	stabilní	k2eAgFnSc1d1
<g/>
)	)	kIx)
Interakce	interakce	k1gFnSc1
</s>
<s>
elektromagnetická	elektromagnetický	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
,	,	kIx,
slabá	slabý	k2eAgFnSc1d1
interakce	interakce	k1gFnSc1
<g/>
,	,	kIx,
silná	silný	k2eAgFnSc1d1
interakce	interakce	k1gFnSc1
Historie	historie	k1gFnSc1
Předpověď	předpověď	k1gFnSc1
</s>
<s>
William	William	k1gInSc1
Prout	Prout	k1gInSc1
(	(	kIx(
<g/>
1815	#num#	k4
<g/>
)	)	kIx)
Objev	objev	k1gInSc1
</s>
<s>
Ernest	Ernest	k1gMnSc1
Rutherford	Rutherford	k1gMnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
fyzice	fyzika	k1gFnSc6
je	být	k5eAaImIp3nS
proton	proton	k1gInSc1
subatomární	subatomární	k2eAgFnSc2d1
částice	částice	k1gFnSc2
s	s	k7c7
kladným	kladný	k2eAgInSc7d1
elementárním	elementární	k2eAgInSc7d1
elektrickým	elektrický	k2eAgInSc7d1
nábojem	náboj	k1gInSc7
tj.	tj.	kA
1,602	1,602	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
19	#num#	k4
C	C	kA
a	a	k8xC
klidovou	klidový	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
938	#num#	k4
MeV	MeV	k1gFnPc2
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
2	#num#	k4
(	(	kIx(
<g/>
1,67	1,67	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
27	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
odpovídající	odpovídající	k2eAgFnSc1d1
přibližně	přibližně	k6eAd1
hmotnosti	hmotnost	k1gFnSc3
1836	#num#	k4
elektronů	elektron	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dosud	dosud	k6eAd1
nejpřesnějším	přesný	k2eAgNnSc6d3
měření	měření	k1gNnSc6
z	z	k7c2
r.	r.	kA
2017	#num#	k4
byla	být	k5eAaImAgFnS
zjištěna	zjistit	k5eAaPmNgFnS
relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
protonu	proton	k1gInSc3
1,007	1,007	k4
276	#num#	k4
466	#num#	k4
583	#num#	k4
<g/>
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
29	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
první	první	k4xOgFnSc1
závorka	závorka	k1gFnSc1
udává	udávat	k5eAaImIp3nS
statistickou	statistický	k2eAgFnSc7d1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
systematickou	systematický	k2eAgFnSc4d1
chybu	chyba	k1gFnSc4
určení	určení	k1gNnSc2
posledních	poslední	k2eAgFnPc2d1
dvou	dva	k4xCgFnPc2
platných	platný	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
chemii	chemie	k1gFnSc6
a	a	k8xC
biochemii	biochemie	k1gFnSc6
je	být	k5eAaImIp3nS
proton	proton	k1gInSc1
označení	označení	k1gNnSc2
pro	pro	k7c4
ion	ion	k1gInSc4
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antičásticí	antičástice	k1gFnSc7
protonu	proton	k1gInSc2
je	být	k5eAaImIp3nS
antiproton	antiproton	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
stejně	stejně	k6eAd1
veliký	veliký	k2eAgInSc1d1
náboj	náboj	k1gInSc1
opačného	opačný	k2eAgNnSc2d1
znaménka	znaménko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nukleony	nukleon	k1gInPc1
a	a	k8xC
elektrony	elektron	k1gInPc1
</s>
<s>
Proton	proton	k1gInSc1
je	být	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
neutronem	neutron	k1gInSc7
a	a	k8xC
elektronem	elektron	k1gInSc7
základní	základní	k2eAgFnSc7d1
stavební	stavební	k2eAgFnSc7d1
částicí	částice	k1gFnSc7
veškeré	veškerý	k3xTgFnSc2
známé	známý	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
jader	jádro	k1gNnPc2
všech	všecek	k3xTgInPc2
atomů	atom	k1gInPc2
běžné	běžný	k2eAgFnSc2d1
látky	látka	k1gFnSc2
(	(	kIx(
<g/>
tedy	tedy	k9
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
pouze	pouze	k6eAd1
některých	některý	k3yIgInPc2
exotických	exotický	k2eAgInPc2d1
atomů	atom	k1gInPc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
krátkodobě	krátkodobě	k6eAd1
existujících	existující	k2eAgInPc2d1
pouze	pouze	k6eAd1
v	v	k7c6
extrémních	extrémní	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
čí	čí	k3xOyQgFnSc2,k3xOyRgFnSc2
experimentech	experiment	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
je	on	k3xPp3gInPc4
napodobují	napodobovat	k5eAaImIp3nP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protony	proton	k1gInPc1
a	a	k8xC
neutrony	neutron	k1gInPc1
se	se	k3xPyFc4
proto	proto	k8xC
označují	označovat	k5eAaImIp3nP
společným	společný	k2eAgInSc7d1
názvem	název	k1gInSc7
nukleony	nukleon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Protonové	protonový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Model	model	k1gInSc1
atomu	atom	k1gInSc2
a	a	k8xC
molekuly	molekula	k1gFnSc2
vodíku	vodík	k1gInSc2
</s>
<s>
Jádro	jádro	k1gNnSc1
nejběžnějšího	běžný	k2eAgInSc2d3
izotopu	izotop	k1gInSc2
vodíku	vodík	k1gInSc2
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
proton	proton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k1gNnPc1
atomární	atomární	k2eAgNnPc1d1
jádra	jádro	k1gNnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
protonů	proton	k1gInPc2
a	a	k8xC
neutronů	neutron	k1gInPc2
přitahovaných	přitahovaný	k2eAgInPc2d1
k	k	k7c3
sobě	se	k3xPyFc3
zbytkovou	zbytkový	k2eAgFnSc7d1
silnou	silný	k2eAgFnSc7d1
jadernou	jaderný	k2eAgFnSc7d1
interakcí	interakce	k1gFnSc7
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgFnPc4d1
<g/>
,	,	kIx,
řádově	řádově	k6eAd1
silnější	silný	k2eAgFnSc1d2
silná	silný	k2eAgFnSc1d1
interakce	interakce	k1gFnSc1
působí	působit	k5eAaImIp3nS
mezi	mezi	k7c7
stavebními	stavební	k2eAgInPc7d1
kvarky	kvark	k1gInPc7
protonu	proton	k1gInSc2
či	či	k8xC
neutronu	neutron	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protonové	protonový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
určuje	určovat	k5eAaImIp3nS
chemické	chemický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
atomu	atom	k1gInSc2
jako	jako	k8xC,k8xS
chemického	chemický	k2eAgInSc2d1
prvku	prvek	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
rovná	rovnat	k5eAaImIp3nS
počet	počet	k1gInSc1
elektronů	elektron	k1gInPc2
v	v	k7c6
obalu	obal	k1gInSc6
neutrálního	neutrální	k2eAgInSc2d1
atomu	atom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Stabilní	stabilní	k2eAgFnSc1d1
částice	částice	k1gFnSc1
</s>
<s>
Proton	proton	k1gInSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
současných	současný	k2eAgInPc2d1
poznatků	poznatek	k1gInPc2
částicí	částice	k1gFnSc7
stabilní	stabilní	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgFnPc2
teorií	teorie	k1gFnPc2
se	se	k3xPyFc4
však	však	k9
může	moct	k5eAaImIp3nS
rozpadat	rozpadat	k5eAaPmF,k5eAaImF
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
přes	přes	k7c4
1031	#num#	k4
roků	rok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpad	rozpad	k1gInSc1
protonu	proton	k1gInSc2
nebyl	být	k5eNaImAgInS
dosud	dosud	k6eAd1
pozorován	pozorovat	k5eAaImNgInS
<g/>
,	,	kIx,
stále	stále	k6eAd1
se	se	k3xPyFc4
prodlužující	prodlužující	k2eAgFnSc1d1
experimentálně	experimentálně	k6eAd1
zjištěná	zjištěný	k2eAgFnSc1d1
minimální	minimální	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
střední	střední	k2eAgFnSc2d1
doby	doba	k1gFnSc2
života	život	k1gInSc2
protonu	proton	k1gInSc2
(	(	kIx(
<g/>
τ	τ	k?
>	>	kIx)
1031	#num#	k4
roků	rok	k1gInPc2
obecně	obecně	k6eAd1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
pro	pro	k7c4
jednotlivé	jednotlivý	k2eAgInPc4d1
modely	model	k1gInPc4
a	a	k8xC
kanály	kanál	k1gInPc4
rozpadu	rozpad	k1gInSc2
až	až	k6eAd1
v	v	k7c6
řádu	řád	k1gInSc6
1034	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
již	již	k6eAd1
vyloučila	vyloučit	k5eAaPmAgFnS
mnohé	mnohé	k1gNnSc4
z	z	k7c2
nejjednodušších	jednoduchý	k2eAgFnPc2d3
variant	varianta	k1gFnPc2
teorie	teorie	k1gFnSc2
velkého	velký	k2eAgNnSc2d1
sjednocení	sjednocení	k1gNnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
pokud	pokud	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
varianty	varianta	k1gFnPc4
bez	bez	k7c2
předpokladu	předpoklad	k1gInSc2
supersymetrie	supersymetrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
střední	střední	k2eAgFnSc2d1
doby	doba	k1gFnSc2
života	život	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
proton	proton	k1gInSc1
skutečně	skutečně	k6eAd1
nestabilní	stabilní	k2eNgInSc1d1
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
podle	podle	k7c2
teorií	teorie	k1gFnPc2
až	až	k9
v	v	k7c6
řádu	řád	k1gInSc6
1039	#num#	k4
roků	rok	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
detekci	detekce	k1gFnSc4
rozpadu	rozpad	k1gInSc2
s	s	k7c7
podobným	podobný	k2eAgInSc7d1
poločasem	poločas	k1gInSc7
jsou	být	k5eAaImIp3nP
současná	současný	k2eAgNnPc4d1
experimentální	experimentální	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
vzhledem	vzhledem	k7c3
k	k	k7c3
počtu	počet	k1gInSc3
pozorovaných	pozorovaný	k2eAgFnPc2d1
částic	částice	k1gFnPc2
a	a	k8xC
době	doba	k1gFnSc3
jejich	jejich	k3xOp3gNnSc2
sledování	sledování	k1gNnSc2
nedostatečná	dostatečný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zařazení	zařazení	k1gNnSc1
protonu	proton	k1gInSc2
</s>
<s>
Proton	proton	k1gInSc4
řadíme	řadit	k5eAaImIp1nP
mezi	mezi	k7c7
baryony	baryon	k1gInPc7
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
baryony	baryona	k1gFnSc2
N	N	kA
v	v	k7c6
základním	základní	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
do	do	k7c2
nukleonů	nukleon	k1gInPc2
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
neutronem	neutron	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomu	ten	k3xDgNnSc3
odpovídá	odpovídat	k5eAaImIp3nS
i	i	k9
spin	spin	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
</s>
<s>
(	(	kIx(
<g/>
proton	proton	k1gInSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
fermion	fermion	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
izospin	izospin	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
</s>
<s>
(	(	kIx(
<g/>
s	s	k7c7
neutronem	neutron	k1gInSc7
tvoří	tvořit	k5eAaImIp3nP
izospinový	izospinový	k2eAgInSc4d1
dublet	dubleta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
(	(	kIx(
<g/>
jako	jako	k9
u	u	k7c2
každého	každý	k3xTgInSc2
baryonu	baryon	k1gInSc2
N	N	kA
<g/>
)	)	kIx)
o	o	k7c4
částici	částice	k1gFnSc4
složenou	složený	k2eAgFnSc4d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ze	z	k7c2
dvou	dva	k4xCgInPc2
kvarků	kvark	k1gInPc2
u	u	k7c2
a	a	k8xC
jednoho	jeden	k4xCgInSc2
kvarku	kvark	k1gInSc2
d	d	k?
vázaných	vázaný	k2eAgFnPc2d1
silnou	silný	k2eAgFnSc7d1
interakcí	interakce	k1gFnSc7
<g/>
,	,	kIx,
zprostředkovanou	zprostředkovaný	k2eAgFnSc7d1
gluony	gluon	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
také	také	k9
důvodem	důvod	k1gInSc7
experimentálně	experimentálně	k6eAd1
zjištěné	zjištěný	k2eAgFnPc4d1
fluktuace	fluktuace	k1gFnPc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
tvaru	tvar	k1gInSc2
(	(	kIx(
<g/>
resp.	resp.	kA
účinného	účinný	k2eAgInSc2d1
průřezu	průřez	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Měření	měření	k1gNnSc3
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
kvarky	kvark	k1gInPc1
i	i	k8xC
gluony	gluon	k1gInPc1
přispívají	přispívat	k5eAaImIp3nP
k	k	k7c3
celkovému	celkový	k2eAgInSc3d1
spinu	spin	k1gInSc3
protonu	proton	k1gInSc2
přibližně	přibližně	k6eAd1
po	po	k7c6
25	#num#	k4
<g/>
%	%	kIx~
a	a	k8xC
chybějící	chybějící	k2eAgInSc1d1
příspěvek	příspěvek	k1gInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
krizí	krize	k1gFnSc7
protonového	protonový	k2eAgInSc2d1
spinu	spin	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Proton	proton	k1gInSc4
objevil	objevit	k5eAaPmAgMnS
Ernest	Ernest	k1gMnSc1
Rutherford	Rutherford	k1gMnSc1
v	v	k7c6
r.	r.	kA
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozoroval	pozorovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
částice	částice	k1gFnSc2
alfa	alfa	k1gNnSc1
vystřelované	vystřelovaný	k2eAgNnSc1d1
do	do	k7c2
plynného	plynný	k2eAgInSc2d1
dusíku	dusík	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
scintilačním	scintilační	k2eAgInSc6d1
detektoru	detektor	k1gInSc6
jeví	jevit	k5eAaImIp3nS
stejně	stejně	k6eAd1
jako	jako	k9
jádra	jádro	k1gNnSc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rutherford	Rutherford	k1gInSc1
určil	určit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
zdrojem	zdroj	k1gInSc7
jader	jádro	k1gNnPc2
vodíku	vodík	k1gInSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
dusík	dusík	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
musí	muset	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
jádra	jádro	k1gNnSc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myslel	myslet	k5eAaImAgInS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
jádra	jádro	k1gNnPc1
vodíku	vodík	k1gInSc2
<g/>
,	,	kIx,
o	o	k7c6
nichž	jenž	k3xRgFnPc6
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
mají	mít	k5eAaImIp3nP
atomové	atomový	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
1	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
elementární	elementární	k2eAgFnPc1d1
částice	částice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
pojmenoval	pojmenovat	k5eAaPmAgMnS
proton	proton	k1gInSc4
<g/>
,	,	kIx,
dle	dle	k7c2
řeckého	řecký	k2eAgNnSc2d1
protos	protos	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s>
Technologické	technologický	k2eAgNnSc1d1
užití	užití	k1gNnSc1
</s>
<s>
Protony	proton	k1gInPc1
mají	mít	k5eAaImIp3nP
vlastnost	vlastnost	k1gFnSc4
spin	spina	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
objevena	objevit	k5eAaPmNgFnS
při	při	k7c6
nukleární	nukleární	k2eAgFnSc6d1
magnetické	magnetický	k2eAgFnSc6d1
resonanční	resonanční	k2eAgFnSc6d1
spektroskopii	spektroskopie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
NMR	NMR	kA
se	se	k3xPyFc4
magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
užívá	užívat	k5eAaImIp3nS
k	k	k7c3
detekci	detekce	k1gFnSc3
stínění	stínění	k1gNnSc2
okolo	okolo	k7c2
protonů	proton	k1gInPc2
v	v	k7c6
jádře	jádro	k1gNnSc6
látky	látka	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
způsobuje	způsobovat	k5eAaImIp3nS
okolní	okolní	k2eAgInSc4d1
mrak	mrak	k1gInSc4
elektronů	elektron	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
z	z	k7c2
něj	on	k3xPp3gNnSc2
mohou	moct	k5eAaImIp3nP
získat	získat	k5eAaPmF
informace	informace	k1gFnPc4
o	o	k7c4
uspořádání	uspořádání	k1gNnSc4
molekulární	molekulární	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
látky	látka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Nukleon	nukleon	k1gInSc1
</s>
<s>
Baryon	Baryon	k1gMnSc1
</s>
<s>
Elektron	elektron	k1gInSc1
</s>
<s>
Neutron	neutron	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Fundamental	Fundamental	k1gMnSc1
Physical	Physical	k1gFnSc4
Constants	Constantsa	k1gFnPc2
<g/>
;	;	kIx,
2018	#num#	k4
CODATA	CODATA	kA
recommended	recommended	k1gMnSc1
values	values	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NIST	NIST	kA
<g/>
,	,	kIx,
květen	květen	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
PDF	PDF	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
PATRIGNANI	PATRIGNANI	kA
<g/>
,	,	kIx,
C.	C.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Particle	Particle	k1gNnSc1
Data	datum	k1gNnSc2
Group	Group	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
Review	Review	k1gFnSc2
of	of	k?
Particle	Particle	k1gFnSc2
Physics	Physicsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Baryon	Baryona	k1gFnPc2
Summary	Summara	k1gFnSc2
Tables	Tables	k1gInSc1
<g/>
:	:	kIx,
p	p	k?
<g/>
,	,	kIx,
n	n	k0
<g/>
,	,	kIx,
N	N	kA
resonances	resonances	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chin	China	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phys	Physa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
40	#num#	k4
<g/>
:	:	kIx,
100001	#num#	k4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
and	and	k?
2017	#num#	k4
update	update	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
pdf	pdf	k?
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
tedy	tedy	k8xC
hodnota	hodnota	k1gFnSc1
hmotnosti	hmotnost	k1gFnSc2
vyjádřená	vyjádřený	k2eAgFnSc1d1
v	v	k7c6
atomových	atomový	k2eAgFnPc6d1
hmotnostních	hmotnostní	k2eAgFnPc6d1
jednotkách	jednotka	k1gFnPc6
u	u	k7c2
<g/>
↑	↑	k?
HEIßE	HEIßE	k1gMnSc1
<g/>
,	,	kIx,
Fabian	Fabian	k1gMnSc1
<g/>
;	;	kIx,
KÖHLER-LANGES	KÖHLER-LANGES	k1gMnSc1
<g/>
,	,	kIx,
Florian	Florian	k1gMnSc1
<g/>
;	;	kIx,
RAU	RAU	kA
<g/>
,	,	kIx,
Sascha	Sascha	k1gFnSc1
<g/>
;	;	kIx,
HOU	hou	k0
<g/>
,	,	kIx,
Jamin	Jamin	k1gMnSc1
<g/>
;	;	kIx,
JUNCK	JUNCK	kA
<g/>
,	,	kIx,
Sven	Sven	k1gMnSc1
<g/>
;	;	kIx,
KRACKE	KRACKE	kA
<g/>
,	,	kIx,
Anke	Anke	k1gFnSc1
<g/>
;	;	kIx,
MOOSER	MOOSER	kA
<g/>
,	,	kIx,
Andreas	Andreas	k1gMnSc1
<g/>
,	,	kIx,
QUINT	QUINT	kA
<g/>
,	,	kIx,
Wolfgang	Wolfgang	k1gMnSc1
<g/>
;	;	kIx,
ULMER	ULMER	kA
<g/>
,	,	kIx,
Stefan	Stefan	k1gMnSc1
<g/>
;	;	kIx,
WERTH	WERTH	kA
<g/>
,	,	kIx,
Günter	Günter	k1gMnSc1
<g/>
;	;	kIx,
BLAUM	BLAUM	kA
<g/>
,	,	kIx,
Klaus	Klaus	k1gMnSc1
<g/>
;	;	kIx,
STURM	STURM	kA
<g/>
,	,	kIx,
Sven	Sven	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
High-precision	High-precision	k1gInSc1
measurement	measurement	k1gInSc4
of	of	k?
the	the	k?
proton	proton	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
atomic	atomic	k1gMnSc1
mass	mass	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
dostupné	dostupný	k2eAgNnSc1d1
na	na	k7c6
<g/>
:	:	kIx,
.	.	kIx.
arXiv	arXiv	k6eAd1
<g/>
:	:	kIx,
<g/>
1706.06780	1706.06780	k4
<g/>
v	v	k7c4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JOHNSTON	JOHNSTON	kA
<g/>
,	,	kIx,
Hamish	Hamish	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Is	Is	k1gFnSc1
the	the	k?
proton	proton	k1gInSc1
lighter	lighter	k1gMnSc1
than	than	k1gMnSc1
we	we	k?
thought	thought	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physics	Physics	k1gInSc1
World	World	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IOP	IOP	kA
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BAJC	BAJC	kA
<g/>
,	,	kIx,
Borut	Borut	k1gMnSc1
<g/>
;	;	kIx,
HISANO	HISANO	kA
<g/>
,	,	kIx,
Junji	Junje	k1gFnSc4
<g/>
;	;	kIx,
KUWAHARA	KUWAHARA	kA
<g/>
,	,	kIx,
Takumi	Taku	k1gFnPc7
<g/>
;	;	kIx,
OMURA	OMURA	kA
<g/>
,	,	kIx,
Yuji	Yuji	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Threshold	Thresholda	k1gFnPc2
corrections	corrections	k6eAd1
to	ten	k3xDgNnSc4
dimension-six	dimension-six	k1gInSc1
proton	proton	k1gInSc1
decay	decaa	k1gFnSc2
operators	operatorsa	k1gFnPc2
in	in	k?
non-minimal	non-minimal	k1gMnSc1
SUSY	SUSY	kA
SU	SU	k?
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
GUTs	GUTsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Introduction	Introduction	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nuclear	Nuclear	k1gInSc1
Physics	Physics	k1gInSc4
B	B	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
910	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
arXiv	arXiv	k6eAd1
<g/>
:	:	kIx,
<g/>
1603.03568	1603.03568	k4
<g/>
v	v	k7c4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
nuclphysb	nuclphysb	k1gInSc1
<g/>
.2016	.2016	k4
<g/>
.06	.06	k4
<g/>
.017	.017	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NISHINO	NISHINO	kA
<g/>
,	,	kIx,
Haruki	Haruki	k1gNnSc2
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Super-Kamiokande	Super-Kamiokand	k1gInSc5
Collaboration	Collaboration	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Search	Search	k1gInSc1
for	forum	k1gNnPc2
Proton	proton	k1gInSc1
Decay	Decaa	k1gFnSc2
via	via	k7c4
p	p	k?
<g/>
→	→	k?
<g/>
e	e	k0
<g/>
+	+	kIx~
<g/>
π	π	k?
<g/>
0	#num#	k4
and	and	k?
p	p	k?
<g/>
→	→	k?
<g/>
μ	μ	k?
<g/>
+	+	kIx~
<g/>
π	π	k?
<g/>
0	#num#	k4
in	in	k?
a	a	k8xC
Large	Larg	k1gInSc2
Water	Water	k1gInSc4
Cherenkov	Cherenkov	k1gInSc1
Detector	Detector	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gFnSc1
Review	Review	k1gFnSc2
Letters	Lettersa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
102	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
14	#num#	k4
<g/>
:	:	kIx,
141801	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1079	#num#	k4
<g/>
-	-	kIx~
<g/>
7114	#num#	k4
<g/>
.	.	kIx.
arXiv	arXiv	k6eAd1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
903.067	903.067	k4
<g/>
6	#num#	k4
<g/>
v	v	k7c4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
PhysRevLett	PhysRevLett	k1gInSc1
<g/>
.102	.102	k4
<g/>
.141801	.141801	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
19392425	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NATH	NATH	kA
<g/>
,	,	kIx,
Pran	Pran	k1gMnSc1
<g/>
;	;	kIx,
PEREZ	PEREZ	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Fileviez	Fileviez	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proton	proton	k1gInSc1
Stability	stabilita	k1gFnSc2
in	in	k?
Grand	grand	k1gMnSc1
Unified	Unified	k1gMnSc1
Theories	Theories	k1gMnSc1
<g/>
,	,	kIx,
in	in	k?
Strings	Strings	k1gInSc1
and	and	k?
in	in	k?
Branes	Branes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
arXiv	arXivit	k5eAaPmRp2nS
<g/>
:	:	kIx,
<g/>
hep-ph	hep-ph	k1gMnSc1
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
601023	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.117.052301	http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.117.052301	k4
-	-	kIx~
Evidence	evidence	k1gFnSc1
of	of	k?
Strong	Strong	k1gInSc1
Proton	proton	k1gInSc1
Shape	Shap	k1gInSc5
Fluctuations	Fluctuationsa	k1gFnPc2
from	from	k1gInSc1
Incoherent	Incoherent	k1gMnSc1
Diffraction	Diffraction	k1gInSc1
<g/>
↑	↑	k?
http://www.sciencedaily.com/releases/2014/11/141104111150.htm	http://www.sciencedaily.com/releases/2014/11/141104111150.htm	k1gInSc1
-	-	kIx~
Physicists	Physicists	k1gInSc1
narrow	narrow	k?
search	search	k1gInSc1
for	forum	k1gNnPc2
solution	solution	k1gInSc1
to	ten	k3xDgNnSc1
proton	proton	k1gInSc1
spin	spina	k1gFnPc2
puzzle	puzzle	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
proton	proton	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
proton	proton	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Částice	částice	k1gFnSc1
Elementární	elementární	k2eAgFnSc2d1
částice	částice	k1gFnSc2
</s>
<s>
částice	částice	k1gFnSc1
hmoty	hmota	k1gFnSc2
<g/>
(	(	kIx(
<g/>
fermiony	fermion	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
kvarky	kvark	k1gInPc1
</s>
<s>
kvark	kvark	k1gInSc1
u	u	k7c2
·	·	k?
antikvark	antikvark	k1gInSc1
u	u	k7c2
·	·	k?
kvark	kvark	k1gInSc1
d	d	k?
·	·	k?
antikvark	antikvark	k1gInSc1
d	d	k?
·	·	k?
kvark	kvark	k1gInSc1
s	s	k7c7
·	·	k?
antikvark	antikvark	k1gInSc1
s	s	k7c7
·	·	k?
kvark	kvark	k1gInSc1
c	c	k0
·	·	k?
antikvark	antikvark	k1gInSc1
c	c	k0
·	·	k?
kvark	kvark	k1gInSc1
t	t	k?
·	·	k?
antikvark	antikvark	k1gInSc1
t	t	k?
·	·	k?
kvark	kvark	k1gInSc1
b	b	k?
·	·	k?
antikvark	antikvark	k1gInSc4
b	b	k?
leptony	lepton	k1gInPc4
</s>
<s>
elektron	elektron	k1gInSc1
·	·	k?
pozitron	pozitron	k1gInSc1
·	·	k?
mion	mion	k1gInSc1
·	·	k?
antimion	antimion	k1gInSc1
·	·	k?
tauon	tauon	k1gMnSc1
·	·	k?
antitauon	antitauon	k1gMnSc1
·	·	k?
elektronové	elektronový	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
elektronové	elektronový	k2eAgNnSc4d1
antineutrino	antineutrino	k1gNnSc4
·	·	k?
mionové	mionový	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
mionové	mionový	k2eAgNnSc4d1
antineutrino	antineutrino	k1gNnSc4
·	·	k?
tauonové	tauonový	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
tauonové	tauonová	k1gFnSc2
antineutrino	antineutrin	k2eAgNnSc1d1
</s>
<s>
částice	částice	k1gFnSc1
interakcí	interakce	k1gFnPc2
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
foton	foton	k1gInSc1
·	·	k?
gluon	gluon	k1gInSc1
·	·	k?
bosony	bosona	k1gFnSc2
W	W	kA
a	a	k8xC
Z	z	k7c2
·	·	k?
Higgsův	Higgsův	k2eAgMnSc1d1
boson	boson	k1gMnSc1
hypotetické	hypotetický	k2eAgFnSc2d1
</s>
<s>
částice	částice	k1gFnSc1
interakcí	interakce	k1gFnPc2
</s>
<s>
graviton	graviton	k1gInSc1
·	·	k?
bosony	bosona	k1gFnSc2
X	X	kA
a	a	k8xC
Y	Y	kA
·	·	k?
bosony	bosona	k1gFnPc4
W	W	kA
<g/>
'	'	kIx"
a	a	k8xC
Z	z	k7c2
<g/>
'	'	kIx"
·	·	k?
majoron	majoron	k1gInSc1
·	·	k?
duální	duální	k2eAgInSc1d1
graviton	graviton	k1gInSc1
superpartneři	superpartner	k1gMnPc1
</s>
<s>
gluino	gluino	k1gNnSc1
·	·	k?
gravitino	gravitin	k2eAgNnSc1d1
·	·	k?
chargino	chargino	k1gNnSc1
·	·	k?
neutralino	utralin	k2eNgNnSc1d1
·	·	k?
(	(	kIx(
<g/>
fotino	fotino	k1gNnSc1
·	·	k?
higgsino	higgsin	k2eAgNnSc1d1
·	·	k?
wino	wino	k1gNnSc1
·	·	k?
zino	zino	k1gMnSc1
<g/>
)	)	kIx)
·	·	k?
slepton	slepton	k1gInSc1
·	·	k?
skvark	skvark	k1gInSc1
·	·	k?
axino	axino	k6eAd1
ostatní	ostatní	k2eAgFnPc1d1
</s>
<s>
axion	axion	k1gInSc1
·	·	k?
dilaton	dilaton	k1gInSc1
·	·	k?
magnetický	magnetický	k2eAgInSc1d1
monopól	monopól	k1gInSc1
·	·	k?
Planckova	Planckov	k1gInSc2
částice	částice	k1gFnSc2
·	·	k?
preon	preon	k1gMnSc1
·	·	k?
sterilní	sterilní	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
tachyon	tachyon	k1gMnSc1
</s>
<s>
Složené	složený	k2eAgFnPc1d1
částice	částice	k1gFnPc1
</s>
<s>
hadrony	hadron	k1gInPc4
</s>
<s>
baryony	baryon	k1gInPc1
<g/>
(	(	kIx(
<g/>
fermiony	fermion	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
nukleony	nukleon	k1gInPc1
</s>
<s>
proton	proton	k1gInSc1
·	·	k?
antiproton	antiproton	k1gInSc1
·	·	k?
neutron	neutron	k1gInSc1
·	·	k?
antineutron	antineutron	k1gInSc1
hyperony	hyperon	k1gMnPc4
</s>
<s>
Δ	Δ	k?
·	·	k?
Λ	Λ	k?
·	·	k?
Σ	Σ	k?
<g/>
,	,	kIx,
Σ	Σ	k?
<g/>
*	*	kIx~
·	·	k?
Ξ	Ξ	k?
<g/>
,	,	kIx,
Ξ	Ξ	k?
<g/>
*	*	kIx~
·	·	k?
Ω	Ω	k?
ostatní	ostatní	k2eAgFnSc2d1
baryonové	baryonový	k2eAgFnSc2d1
rezonance	rezonance	k1gFnSc2
</s>
<s>
N	N	kA
·	·	k?
Δ	Δ	k?
·	·	k?
Λ	Λ	k?
<g/>
,	,	kIx,
Λ	Λ	k1gInSc1
<g/>
,	,	kIx,
Λ	Λ	k1gInSc1
·	·	k?
Σ	Σ	k?
<g/>
,	,	kIx,
Σ	Σ	k1gInSc1
<g/>
,	,	kIx,
Σ	Σ	k1gInSc1
·	·	k?
Ξ	Ξ	k?
<g/>
,	,	kIx,
Ξ	Ξ	k1gInSc1
<g/>
,	,	kIx,
Ξ	Ξ	k1gInSc1
<g/>
,	,	kIx,
Ξ	Ξ	k1gInSc1
·	·	k?
Ω	Ω	k?
<g/>
,	,	kIx,
Ω	Ω	k1gInSc1
<g/>
,	,	kIx,
Ω	Ω	k1gInSc1
</s>
<s>
mezony	mezon	k1gInPc1
<g/>
/	/	kIx~
<g/>
kvarkonia	kvarkonium	k1gNnPc1
<g/>
(	(	kIx(
<g/>
bosony	boson	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
pion	pion	k1gInSc1
·	·	k?
kaon	kaon	k1gInSc1
·	·	k?
ρ	ρ	k?
·	·	k?
η	η	k?
·	·	k?
φ	φ	k?
·	·	k?
ω	ω	k?
·	·	k?
J	J	kA
<g/>
/	/	kIx~
<g/>
ψ	ψ	k?
·	·	k?
ϒ	ϒ	k?
·	·	k?
θ	θ	k?
·	·	k?
B	B	kA
·	·	k?
D	D	kA
·	·	k?
T	T	kA
exotické	exotický	k2eAgInPc4d1
hadrony	hadron	k1gInPc4
</s>
<s>
tetrakvarky	tetrakvarka	k1gFnPc1
<g/>
/	/	kIx~
<g/>
dvoumezonové	dvoumezonový	k2eAgFnPc1d1
molekuly	molekula	k1gFnPc1
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
X	X	kA
<g/>
(	(	kIx(
<g/>
2900	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
3872	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
4140	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
s	s	k7c7
excitacemi	excitace	k1gFnPc7
X	X	kA
<g/>
(	(	kIx(
<g/>
4274	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
X	X	kA
<g/>
(	(	kIx(
<g/>
4500	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
X	X	kA
<g/>
(	(	kIx(
<g/>
4700	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
))	))	k?
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
4630	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
4685	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
5568	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
6900	#num#	k4
<g/>
)	)	kIx)
<g/>
Zc	Zc	k1gFnSc2
<g/>
(	(	kIx(
<g/>
3900	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Zc	Zc	k1gFnSc1
<g/>
(	(	kIx(
<g/>
4430	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Zcs	Zcs	k1gFnSc1
<g/>
(	(	kIx(
<g/>
4000	#num#	k4
<g/>
)	)	kIx)
<g/>
+	+	kIx~
·	·	k?
Zcs	Zcs	k1gFnSc1
<g/>
(	(	kIx(
<g/>
4220	#num#	k4
<g/>
)	)	kIx)
<g/>
+	+	kIx~
pentakvarky	pentakvark	k1gInPc1
<g/>
(	(	kIx(
<g/>
fermiony	fermion	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Pc	Pc	k?
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4312	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Pc	Pc	k1gFnSc1
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4380	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Pc	Pc	k1gFnSc1
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4440	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Pc	Pc	k1gFnSc1
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4457	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
hexakvarky	hexakvarka	k1gFnPc1
<g/>
/	/	kIx~
<g/>
dibaryony	dibaryona	k1gFnPc1
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
d	d	k?
<g/>
*	*	kIx~
<g/>
(	(	kIx(
<g/>
2380	#num#	k4
<g/>
)	)	kIx)
glueballs	glueballs	k1gInSc1
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
odderon	odderon	k1gMnSc1
·	·	k?
pomeron	pomeron	k1gMnSc1
(	(	kIx(
<g/>
hypotetický	hypotetický	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
další	další	k2eAgFnPc1d1
částice	částice	k1gFnPc1
</s>
<s>
atomové	atomový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
(	(	kIx(
<g/>
deuteron	deuteron	k1gInSc1
·	·	k?
triton	triton	k1gInSc1
·	·	k?
helion	helion	k1gInSc1
·	·	k?
částice	částice	k1gFnSc1
alfa	alfa	k1gFnSc1
·	·	k?
hyperjádro	hyperjádro	k6eAd1
<g/>
)	)	kIx)
·	·	k?
atom	atom	k1gInSc1
·	·	k?
superatom	superatom	k1gInSc1
·	·	k?
exotické	exotický	k2eAgInPc1d1
atomy	atom	k1gInPc1
(	(	kIx(
<g/>
pozitronium	pozitronium	k1gNnSc1
·	·	k?
mionium	mionium	k1gNnSc1
·	·	k?
onium	onium	k1gNnSc1
<g/>
)	)	kIx)
·	·	k?
molekula	molekula	k1gFnSc1
hypotetické	hypotetický	k2eAgFnPc1d1
</s>
<s>
mezony	mezon	k1gInPc1
<g/>
/	/	kIx~
<g/>
kvarkonia	kvarkonium	k1gNnPc1
</s>
<s>
mezon	mezon	k1gInSc1
théta	théta	k1gNnSc2
·	·	k?
mezon	mezon	k1gInSc1
T	T	kA
exotické	exotický	k2eAgInPc1d1
mezony	mezon	k1gInPc1
</s>
<s>
kvark-antikvark-gluonové	kvark-antikvark-gluonové	k2eAgNnPc7d1
kompozity	kompozitum	k1gNnPc7
ostatní	ostatní	k2eAgMnPc4d1
</s>
<s>
dikvarky	dikvarka	k1gFnPc1
·	·	k?
leptokvarky	leptokvarka	k1gFnSc2
·	·	k?
pomeron	pomeron	k1gMnSc1
</s>
<s>
Kvazičástice	Kvazičástika	k1gFnSc3
</s>
<s>
Davydovův	Davydovův	k2eAgInSc1d1
soliton	soliton	k1gInSc1
·	·	k?
dropleton	dropleton	k1gInSc1
·	·	k?
elektronová	elektronový	k2eAgFnSc1d1
díra	díra	k1gFnSc1
·	·	k?
exciton	exciton	k1gInSc1
·	·	k?
fonon	fonon	k1gMnSc1
·	·	k?
magnon	magnon	k1gMnSc1
·	·	k?
plazmaron	plazmaron	k1gMnSc1
·	·	k?
plazmon	plazmon	k1gMnSc1
·	·	k?
polariton	polariton	k1gInSc1
·	·	k?
polaron	polaron	k1gInSc1
·	·	k?
roton	roton	k1gInSc1
·	·	k?
skyrmion	skyrmion	k1gInSc1
·	·	k?
trion	trion	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4137643-2	4137643-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
3710	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85107796	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85107796	#num#	k4
</s>
