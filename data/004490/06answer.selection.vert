<s>
Gerlachovský	Gerlachovský	k2eAgInSc4d1	Gerlachovský
štít	štít	k1gInSc4	štít
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc4d1	zvaný
též	též	k9	též
Gerlach	Gerlach	k1gInSc4	Gerlach
(	(	kIx(	(
<g/>
pol.	pol.	k?	pol.
Gerlach	Gerlach	k1gInSc1	Gerlach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
Tater	Tatra	k1gFnPc2	Tatra
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
celých	celý	k2eAgInPc2d1	celý
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
