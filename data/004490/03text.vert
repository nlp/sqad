<p>
<s>
Gerlachovský	Gerlachovský	k2eAgInSc4d1	Gerlachovský
štít	štít	k1gInSc4	štít
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc4d1	zvaný
též	též	k9	též
Gerlach	Gerlach	k1gInSc4	Gerlach
(	(	kIx(	(
<g/>
pol.	pol.	k?	pol.
Gerlach	Gerlach	k1gInSc1	Gerlach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
Tater	Tatra	k1gFnPc2	Tatra
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
celých	celý	k2eAgInPc2d1	celý
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
hora	hora	k1gFnSc1	hora
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
horou	hora	k1gFnSc7	hora
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
podle	podle	k7c2	podle
Gerlachova	Gerlachův	k2eAgNnSc2d1	Gerlachův
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
a	a	k8xC	a
do	do	k7c2	do
jehož	jehož	k3xOyRp3gNnPc2	jehož
území	území	k1gNnPc2	území
patří	patřit	k5eAaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
rozsoše	rozsocha	k1gFnSc6	rozsocha
Zadného	Zadný	k2eAgInSc2d1	Zadný
Gerlachu	Gerlach	k1gInSc2	Gerlach
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgNnSc2	který
ho	on	k3xPp3gInSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Gerlachovské	Gerlachovský	k2eAgNnSc1d1	Gerlachovský
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
hřeben	hřeben	k1gInSc1	hřeben
na	na	k7c4	na
Kotlový	kotlový	k2eAgInSc4d1	kotlový
štít	štít	k1gInSc4	štít
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozvětvuje	rozvětvovat	k5eAaImIp3nS	rozvětvovat
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
známý	známý	k2eAgInSc4d1	známý
Gerlachovský	Gerlachovský	k2eAgInSc4d1	Gerlachovský
kotel	kotel	k1gInSc4	kotel
<g/>
.	.	kIx.	.
</s>
<s>
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
dostupnost	dostupnost	k1gFnSc4	dostupnost
a	a	k8xC	a
krásnou	krásný	k2eAgFnSc4d1	krásná
horskou	horský	k2eAgFnSc4d1	horská
scenérii	scenérie	k1gFnSc4	scenérie
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejnavštěvovanějších	navštěvovaný	k2eAgInPc2d3	nejnavštěvovanější
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Měření	měření	k1gNnSc1	měření
výšek	výška	k1gFnPc2	výška
tatranských	tatranský	k2eAgInPc2d1	tatranský
štítů	štít	k1gInPc2	štít
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1763	[number]	k4	1763
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
dvůr	dvůr	k1gInSc1	dvůr
nařídil	nařídit	k5eAaPmAgInS	nařídit
tzv.	tzv.	kA	tzv.
josefínské	josefínský	k2eAgNnSc1d1	josefínské
mapování	mapování	k1gNnSc1	mapování
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kartografické	kartografický	k2eAgNnSc1d1	kartografické
zpracování	zpracování	k1gNnSc1	zpracování
celé	celý	k2eAgFnSc2d1	celá
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
prováděli	provádět	k5eAaImAgMnP	provádět
vojenští	vojenský	k2eAgMnPc1d1	vojenský
měřiči	měřič	k1gMnPc1	měřič
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Waldaufa	Waldauf	k1gMnSc2	Waldauf
a	a	k8xC	a
Fleischera	Fleischer	k1gMnSc2	Fleischer
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInPc1	jejich
výsledky	výsledek	k1gInPc1	výsledek
nebyly	být	k5eNaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
měření	měření	k1gNnPc4	měření
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roku	rok	k1gInSc6	rok
1780	[number]	k4	1780
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Geographie	Geographie	k1gFnSc1	Geographie
des	des	k1gNnSc2	des
Königreichs	Königreichsa	k1gFnPc2	Königreichsa
Ungarn	Ungarna	k1gFnPc2	Ungarna
<g/>
"	"	kIx"	"
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
štítem	štít	k1gInSc7	štít
celých	celý	k2eAgFnPc2d1	celá
Tater	Tatra	k1gFnPc2	Tatra
je	být	k5eAaImIp3nS	být
Kriváň	Kriváň	k1gInSc1	Kriváň
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stejnému	stejný	k2eAgInSc3d1	stejný
závěru	závěr	k1gInSc3	závěr
dospěl	dochvít	k5eAaPmAgInS	dochvít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
i	i	k8xC	i
bratislavský	bratislavský	k2eAgMnSc1d1	bratislavský
mineralog	mineralog	k1gMnSc1	mineralog
Ján	Ján	k1gMnSc1	Ján
Fichtel	Fichtel	k1gMnSc1	Fichtel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
výšku	výška	k1gFnSc4	výška
Kriváně	Kriváň	k1gInSc2	Kriváň
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
na	na	k7c4	na
3800	[number]	k4	3800
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
cestovatel	cestovatel	k1gMnSc1	cestovatel
Robert	Robert	k1gMnSc1	Robert
Townson	Townson	k1gMnSc1	Townson
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
štít	štít	k1gInSc4	štít
Tater	Tatra	k1gFnPc2	Tatra
Lomnický	lomnický	k2eAgInSc4d1	lomnický
štít	štít	k1gInSc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
vědci	vědec	k1gMnPc1	vědec
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
známý	známý	k1gMnSc1	známý
švédsky	švédsky	k6eAd1	švédsky
botanik	botanik	k1gMnSc1	botanik
Göran	Göran	k1gMnSc1	Göran
Wahlenberg	Wahlenberg	k1gMnSc1	Wahlenberg
<g/>
,	,	kIx,	,
neurčili	určit	k5eNaPmAgMnP	určit
výšku	výška	k1gFnSc4	výška
tatranských	tatranský	k2eAgInPc2d1	tatranský
vrcholů	vrchol	k1gInPc2	vrchol
správně	správně	k6eAd1	správně
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
uváděli	uvádět	k5eAaImAgMnP	uvádět
Lomnický	lomnický	k2eAgInSc4d1	lomnický
anebo	anebo	k8xC	anebo
Ľadový	Ľadový	k2eAgInSc4d1	Ľadový
štít	štít	k1gInSc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Gerlach	Gerlach	k1gMnSc1	Gerlach
svojí	svůj	k3xOyFgFnSc7	svůj
výškou	výška	k1gFnSc7	výška
figuroval	figurovat	k5eAaImAgInS	figurovat
až	až	k9	až
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1837	[number]	k4	1837
a	a	k8xC	a
1838	[number]	k4	1838
prováděl	provádět	k5eAaImAgInS	provádět
měření	měření	k1gNnSc4	měření
tatranských	tatranský	k2eAgInPc2d1	tatranský
štítů	štít	k1gInPc2	štít
Ľudovít	Ľudovít	k1gMnSc1	Ľudovít
Greiner	Greiner	k1gMnSc1	Greiner
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
jelšavské	jelšavský	k2eAgFnSc2d1	jelšavský
lesní	lesní	k2eAgFnSc2d1	lesní
správy	správa	k1gFnSc2	správa
koburgovského	koburgovský	k2eAgInSc2d1	koburgovský
velkostatku	velkostatek	k1gInSc2	velkostatek
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
on	on	k3xPp3gMnSc1	on
správně	správně	k6eAd1	správně
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
štítem	štít	k1gInSc7	štít
Tater	Tatra	k1gFnPc2	Tatra
je	být	k5eAaImIp3nS	být
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnost	veřejnost	k1gFnSc1	veřejnost
však	však	k9	však
Greinerovo	Greinerův	k2eAgNnSc4d1	Greinerův
objevné	objevný	k2eAgNnSc4d1	objevné
měření	měření	k1gNnSc4	měření
nepřijala	přijmout	k5eNaPmAgFnS	přijmout
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
až	až	k9	až
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Názvy	název	k1gInPc1	název
Gerlachu	Gerlach	k1gInSc2	Gerlach
==	==	k?	==
</s>
</p>
<p>
<s>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
:	:	kIx,	:
Gerlach	Gerlach	k1gMnSc1	Gerlach
<g/>
,	,	kIx,	,
Gerlsdorfer	Gerlsdorfer	k1gMnSc1	Gerlsdorfer
Spitze	Spitze	k1gFnSc1	Spitze
<g/>
,	,	kIx,	,
Gerlachspitze	Gerlachspitze	k1gFnSc1	Gerlachspitze
<g/>
,	,	kIx,	,
Gerlachfálvi-csúcs	Gerlachfálvisúcs	k1gInSc1	Gerlachfálvi-csúcs
<g/>
,	,	kIx,	,
Gierlach	Gierlach	k1gInSc1	Gierlach
<g/>
,	,	kIx,	,
Gerlachovka	Gerlachovka	k1gFnSc1	Gerlachovka
</s>
</p>
<p>
<s>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
:	:	kIx,	:
Štít	štít	k1gInSc1	štít
Františka	František	k1gMnSc2	František
Jozefa	Jozef	k1gMnSc2	Jozef
<g/>
,	,	kIx,	,
Ferencz	Ferencz	k1gInSc1	Ferencz
József-csúcs	Józsefsúcs	k1gInSc1	József-csúcs
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Josef	Josef	k1gMnSc1	Josef
Spitze	Spitze	k1gFnSc1	Spitze
</s>
</p>
<p>
<s>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
:	:	kIx,	:
Gerlach	Gerlach	k1gInSc1	Gerlach
<g/>
,	,	kIx,	,
Gerlachovka	Gerlachovka	k1gFnSc1	Gerlachovka
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
<g/>
:	:	kIx,	:
Štít	štít	k1gInSc1	štít
Legionárov	Legionárov	k1gInSc1	Legionárov
<g/>
,	,	kIx,	,
Štít	štít	k1gInSc1	štít
Legionářů	legionář	k1gMnPc2	legionář
<g/>
,	,	kIx,	,
Spitze	Spitze	k1gFnPc1	Spitze
der	drát	k5eAaImRp2nS	drát
Legionäre	Legionär	k1gMnSc5	Legionär
(	(	kIx(	(
<g/>
neoficiálně	neoficiálně	k6eAd1	neoficiálně
stále	stále	k6eAd1	stále
používaný	používaný	k2eAgInSc1d1	používaný
název	název	k1gInSc1	název
Gerlach	Gerlacha	k1gFnPc2	Gerlacha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
:	:	kIx,	:
Gerlachovka	Gerlachovka	k1gFnSc1	Gerlachovka
<g/>
,	,	kIx,	,
Gerlach	Gerlach	k1gInSc1	Gerlach
<g/>
,	,	kIx,	,
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
:	:	kIx,	:
Slovenský	slovenský	k2eAgInSc1d1	slovenský
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
místním	místní	k2eAgNnSc7d1	místní
německým	německý	k2eAgNnSc7d1	německé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
nazývaný	nazývaný	k2eAgInSc4d1	nazývaný
Slowakische	Slowakische	k1gInSc4	Slowakische
Spitze	Spitze	k1gFnSc2	Spitze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
<g/>
:	:	kIx,	:
Gerlach	Gerlach	k1gInSc1	Gerlach
<g/>
,	,	kIx,	,
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
:	:	kIx,	:
Stalinov	Stalinov	k1gInSc1	Stalinov
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Štít	štít	k1gInSc1	štít
J.	J.	kA	J.
V.	V.	kA	V.
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
od	od	k7c2	od
1959	[number]	k4	1959
<g/>
:	:	kIx,	:
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
</s>
</p>
<p>
<s>
==	==	k?	==
Výstupy	výstup	k1gInPc4	výstup
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
výstupy	výstup	k1gInPc1	výstup
na	na	k7c4	na
Gerlachovský	Gerlachovský	k2eAgInSc4d1	Gerlachovský
štít	štít	k1gInSc4	štít
vykonali	vykonat	k5eAaPmAgMnP	vykonat
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
botanici	botanik	k1gMnPc1	botanik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
až	až	k6eAd1	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prokazatelně	prokazatelně	k6eAd1	prokazatelně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
stál	stát	k5eAaImAgMnS	stát
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
spišský	spišský	k2eAgMnSc1d1	spišský
Němec	Němec	k1gMnSc1	Němec
Ján	Ján	k1gMnSc1	Ján
Still	Still	k1gMnSc1	Still
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
na	na	k7c6	na
katolické	katolický	k2eAgFnSc6d1	katolická
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Novej	Novej	k?	Novej
Lesnej	Lesnej	k?	Lesnej
<g/>
.	.	kIx.	.
</s>
<s>
Výpravy	výprava	k1gFnPc4	výprava
na	na	k7c4	na
Gerlach	Gerlach	k1gInSc4	Gerlach
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
švagr	švagr	k1gMnSc1	švagr
Gellhof	Gellhof	k1gMnSc1	Gellhof
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
z	z	k7c2	z
Veľkej	Veľkej	k1gFnSc2	Veľkej
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Urban	Urban	k1gMnSc1	Urban
Spitzkopf	Spitzkopf	k1gMnSc1	Spitzkopf
<g/>
,	,	kIx,	,
mlynář	mlynář	k1gMnSc1	mlynář
z	z	k7c2	z
Novej	Novej	k?	Novej
Lesnej	Lesnej	k?	Lesnej
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
další	další	k2eAgMnPc1d1	další
neznámí	známý	k2eNgMnPc1d1	neznámý
lovci	lovec	k1gMnPc1	lovec
kamzíků	kamzík	k1gMnPc2	kamzík
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
z	z	k7c2	z
Gerlachovského	Gerlachovský	k2eAgInSc2d1	Gerlachovský
kotle	kotel	k1gInSc2	kotel
Z.	Z.	kA	Z.
Bośniacki	Bośniack	k1gFnSc2	Bośniack
a	a	k8xC	a
V.	V.	kA	V.
Grzegorzek	Grzegorzek	k1gInSc1	Grzegorzek
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
s	s	k7c7	s
horskými	horský	k2eAgMnPc7d1	horský
vůdci	vůdce	k1gMnPc7	vůdce
ze	z	k7c2	z
Zakopaného	Zakopané	k1gNnSc2	Zakopané
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
zimní	zimní	k2eAgInSc4d1	zimní
výstup	výstup	k1gInSc4	výstup
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
J.	J.	kA	J.
Chmielowski	Chmielowsk	k1gFnSc2	Chmielowsk
a	a	k8xC	a
K.	K.	kA	K.
Jordán	Jordán	k1gInSc1	Jordán
s	s	k7c7	s
horskými	horský	k2eAgMnPc7d1	horský
vůdci	vůdce	k1gMnPc7	vůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turismus	turismus	k1gInSc4	turismus
==	==	k?	==
</s>
</p>
<p>
<s>
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Gerlachovský	Gerlachovský	k2eAgInSc4d1	Gerlachovský
štít	štít	k1gInSc4	štít
je	být	k5eAaImIp3nS	být
povolen	povolit	k5eAaPmNgInS	povolit
jen	jen	k9	jen
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
horského	horský	k2eAgMnSc2d1	horský
vůdce	vůdce	k1gMnSc2	vůdce
nebo	nebo	k8xC	nebo
osobám	osoba	k1gFnPc3	osoba
organizovaným	organizovaný	k2eAgFnPc3d1	organizovaná
v	v	k7c6	v
horolezeckých	horolezecký	k2eAgInPc6d1	horolezecký
svazech	svaz	k1gInPc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
doprovod	doprovod	k1gInSc4	doprovod
horského	horský	k2eAgMnSc2d1	horský
vůdce	vůdce	k1gMnSc2	vůdce
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
230	[number]	k4	230
eur	euro	k1gNnPc2	euro
za	za	k7c4	za
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Berou	brát	k5eAaImIp3nP	brát
s	s	k7c7	s
sebou	se	k3xPyFc7	se
maximálně	maximálně	k6eAd1	maximálně
tři	tři	k4xCgFnPc4	tři
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Organizovaní	organizovaný	k2eAgMnPc1d1	organizovaný
horolezci	horolezec	k1gMnPc1	horolezec
nesmí	smět	k5eNaImIp3nP	smět
vystoupit	vystoupit	k5eAaPmF	vystoupit
lehčí	lehký	k2eAgFnSc7d2	lehčí
cestou	cesta	k1gFnSc7	cesta
než	než	k8xS	než
2	[number]	k4	2
<g/>
+	+	kIx~	+
UIAA	UIAA	kA	UIAA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
hranice	hranice	k1gFnSc1	hranice
určená	určený	k2eAgFnSc1d1	určená
návštěvním	návštěvní	k2eAgInSc7d1	návštěvní
řádem	řád	k1gInSc7	řád
Tatranského	tatranský	k2eAgInSc2d1	tatranský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gerlachovský	Gerlachovský	k2eAgInSc4d1	Gerlachovský
štít	štít	k1gInSc4	štít
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Gerlachovský	Gerlachovský	k2eAgMnSc1d1	Gerlachovský
štít	štít	k1gInSc4	štít
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Gerlach	Gerlach	k1gInSc4	Gerlach
cestou	cestou	k7c2	cestou
Martinka	Martinek	k1gMnSc2	Martinek
</s>
</p>
<p>
<s>
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Gerlach	Gerlach	k1gInSc4	Gerlach
Krčmárovým	Krčmárův	k2eAgInSc7d1	Krčmárův
žlabem	žlab	k1gInSc7	žlab
</s>
</p>
<p>
<s>
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Gerlach	Gerlach	k1gInSc4	Gerlach
na	na	k7c4	na
iDnes	iDnes	k1gInSc4	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Gerlach	Gerlach	k1gInSc4	Gerlach
na	na	k7c4	na
HoryEvropy	HoryEvrop	k1gInPc4	HoryEvrop
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Spolok	Spolok	k1gInSc1	Spolok
horských	horský	k2eAgNnPc2d1	horské
vodcov	vodcovo	k1gNnPc2	vodcovo
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Tatry	Tatra	k1gFnSc2	Tatra
</s>
</p>
