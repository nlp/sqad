<s>
Nukleotidy	nukleotid	k1gInPc1	nukleotid
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
cukru	cukr	k1gInSc2	cukr
deoxyribózy	deoxyribóza	k1gFnSc2	deoxyribóza
<g/>
,	,	kIx,	,
fosfátové	fosfátový	k2eAgFnSc2d1	fosfátová
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
