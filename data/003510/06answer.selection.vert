<s>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
textů	text	k1gInPc2	text
se	se	k3xPyFc4	se
Epica	Epica	k1gFnSc1	Epica
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
různými	různý	k2eAgNnPc7d1	různé
náboženstvími	náboženství	k1gNnPc7	náboženství
<g/>
,	,	kIx,	,
kulturami	kultura	k1gFnPc7	kultura
<g/>
,	,	kIx,	,
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
přírodními	přírodní	k2eAgFnPc7d1	přírodní
katastrofami	katastrofa	k1gFnPc7	katastrofa
a	a	k8xC	a
finanční	finanční	k2eAgFnSc7d1	finanční
krizí	krize	k1gFnSc7	krize
<g/>
.	.	kIx.	.
</s>
