<s>
Herbert	Herbert	k1gInSc1	Herbert
von	von	k1gInSc1	von
Karajan	Karajan	k1gInSc4	Karajan
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Anif	Anif	k1gInSc1	Anif
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
a	a	k8xC	a
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
dirigentů	dirigent	k1gMnPc2	dirigent
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
dominantní	dominantní	k2eAgFnSc7d1	dominantní
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
klasické	klasický	k2eAgFnSc6d1	klasická
hudbě	hudba	k1gFnSc6	hudba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
prestižními	prestižní	k2eAgInPc7d1	prestižní
symfonickými	symfonický	k2eAgInPc7d1	symfonický
orchestry	orchestr	k1gInPc7	orchestr
–	–	k?	–
35	[number]	k4	35
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
dirigentem	dirigent	k1gMnSc7	dirigent
Berlínských	berlínský	k2eAgMnPc2d1	berlínský
filharmoniků	filharmonik	k1gMnPc2	filharmonik
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
při	při	k7c6	při
významných	významný	k2eAgInPc6d1	významný
operních	operní	k2eAgInPc6d1	operní
domech	dům	k1gInPc6	dům
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
řadu	řada	k1gFnSc4	řada
nahrávek	nahrávka	k1gFnPc2	nahrávka
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Aranžoval	aranžovat	k5eAaImAgMnS	aranžovat
také	také	k9	také
hymnu	hymna	k1gFnSc4	hymna
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Karajanové	Karajan	k1gMnPc1	Karajan
byli	být	k5eAaImAgMnP	být
rodem	rod	k1gInSc7	rod
řeckého	řecký	k2eAgInSc2d1	řecký
nebo	nebo	k8xC	nebo
arumunského	arumunský	k2eAgInSc2d1	arumunský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pra-pra-dědeček	praraědeček	k1gInSc1	pra-pra-dědeček
Georg	Georg	k1gMnSc1	Georg
Karajan	Karajan	k1gMnSc1	Karajan
(	(	kIx(	(
<g/>
Georgios	Georgios	k1gMnSc1	Georgios
Karajánnis	Karajánnis	k1gFnSc2	Karajánnis
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Γ	Γ	k?	Γ
Κ	Κ	k?	Κ
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
Kozani	Kozaň	k1gFnSc6	Kozaň
<g/>
,	,	kIx,	,
provincii	provincie	k1gFnSc6	provincie
Západní	západní	k2eAgFnSc2d1	západní
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
(	(	kIx(	(
<g/>
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
provincie	provincie	k1gFnSc1	provincie
Rumélie	Rumélie	k1gFnSc2	Rumélie
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1767	[number]	k4	1767
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
následně	následně	k6eAd1	následně
do	do	k7c2	do
Saské	saský	k2eAgFnSc2d1	saská
Kamenice	Kamenice	k1gFnSc2	Kamenice
(	(	kIx(	(
<g/>
Chemnitzu	Chemnitz	k1gInSc2	Chemnitz
<g/>
)	)	kIx)	)
v	v	k7c6	v
Saském	saský	k2eAgNnSc6d1	Saské
kurfiřtství	kurfiřtství	k1gNnSc6	kurfiřtství
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
saského	saský	k2eAgInSc2d1	saský
textilního	textilní	k2eAgInSc2d1	textilní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
za	za	k7c2	za
své	svůj	k3xOyFgFnSc2	svůj
služby	služba	k1gFnSc2	služba
Fridrichem	Fridrich	k1gMnSc7	Fridrich
Augustem	August	k1gMnSc7	August
I.	I.	kA	I.
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1792	[number]	k4	1792
povýšeni	povýšit	k5eAaPmNgMnP	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
předpona	předpona	k1gFnSc1	předpona
von	von	k1gInSc4	von
do	do	k7c2	do
příjmení	příjmení	k1gNnSc2	příjmení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
příjmení	příjmení	k1gNnSc2	příjmení
Karajánnis	Karajánnis	k1gInSc1	Karajánnis
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Karajan	Karajan	k1gInSc1	Karajan
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
tradiční	tradiční	k2eAgInPc1d1	tradiční
životopisy	životopis	k1gInPc1	životopis
připisují	připisovat	k5eAaImIp3nP	připisovat
srbský	srbský	k2eAgInSc4d1	srbský
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
slovanský	slovanský	k2eAgInSc4d1	slovanský
původ	původ	k1gInSc4	původ
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Karajanovi	Karajanův	k2eAgMnPc1d1	Karajanův
předkové	předek	k1gMnPc1	předek
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
jeho	on	k3xPp3gMnSc4	on
dědečka	dědeček	k1gMnSc4	dědeček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Mojstrana	Mojstran	k1gMnSc2	Mojstran
v	v	k7c6	v
Kraňsku	Kraňsko	k1gNnSc6	Kraňsko
(	(	kIx(	(
<g/>
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Slovinci	Slovinec	k1gMnPc1	Slovinec
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
linii	linie	k1gFnSc4	linie
byl	být	k5eAaImAgMnS	být
Karajan	Karajan	k1gMnSc1	Karajan
příbuzný	příbuzný	k1gMnSc1	příbuzný
s	s	k7c7	s
rakouským	rakouský	k2eAgMnSc7d1	rakouský
skladatelem	skladatel	k1gMnSc7	skladatel
slovinského	slovinský	k2eAgInSc2d1	slovinský
původu	původ	k1gInSc2	původ
Hugo	Hugo	k1gMnSc1	Hugo
Wolfem	Wolf	k1gMnSc7	Wolf
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karajan	Karajan	k1gMnSc1	Karajan
trochu	trochu	k6eAd1	trochu
hovořil	hovořit	k5eAaImAgMnS	hovořit
slovinsky	slovinsky	k6eAd1	slovinsky
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Salzburku	Salzburk	k1gInSc6	Salzburk
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6	Rakousku-Uhersek
jako	jako	k8xS	jako
Heribert	Heribert	k1gMnSc1	Heribert
rytíř	rytíř	k1gMnSc1	rytíř
von	von	k1gInSc4	von
Karajan	Karajany	k1gInPc2	Karajany
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zázračným	zázračný	k2eAgNnSc7d1	zázračné
dítětem	dítě	k1gNnSc7	dítě
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
do	do	k7c2	do
1926	[number]	k4	1926
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Mozarteu	Mozarteus	k1gInSc6	Mozarteus
v	v	k7c6	v
Salcburku	Salcburk	k1gInSc6	Salcburk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
učitel	učitel	k1gMnSc1	učitel
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgInS	soustředit
na	na	k7c6	na
dirigentství	dirigentství	k1gNnSc6	dirigentství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
operu	opera	k1gFnSc4	opera
Salome	Salom	k1gInSc5	Salom
ve	v	k7c6	v
Festspielhausu	Festspielhaus	k1gInSc6	Festspielhaus
v	v	k7c6	v
Salcburku	Salcburk	k1gInSc6	Salcburk
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
kapelník	kapelník	k1gMnSc1	kapelník
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Ulmu	Ulmus	k1gInSc6	Ulmus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
Karajan	Karajan	k1gMnSc1	Karajan
debutoval	debutovat	k5eAaBmAgMnS	debutovat
jako	jako	k9	jako
dirigent	dirigent	k1gMnSc1	dirigent
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Salcburku	Salcburk	k1gInSc6	Salcburk
s	s	k7c7	s
Walpurgisnacht	Walpurgisnachta	k1gFnPc2	Walpurgisnachta
Scene	Scen	k1gInSc5	Scen
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
Fausta	Faust	k1gMnSc2	Faust
Maxe	Max	k1gMnSc2	Max
Reinhardta	Reinhardt	k1gMnSc2	Reinhardt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
také	také	k9	také
Karajan	Karajan	k1gMnSc1	Karajan
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
v	v	k7c6	v
Salcburku	Salcburk	k1gInSc6	Salcburk
Karajan	Karajan	k1gMnSc1	Karajan
poprvé	poprvé	k6eAd1	poprvé
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
filharmonii	filharmonie	k1gFnSc4	filharmonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934	[number]	k4	1934
až	až	k9	až
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
angažován	angažovat	k5eAaBmNgMnS	angažovat
jako	jako	k8xS	jako
dirigent	dirigent	k1gMnSc1	dirigent
operních	operní	k2eAgInPc2d1	operní
a	a	k8xC	a
symfonických	symfonický	k2eAgInPc2d1	symfonický
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Karajanovu	Karajanův	k2eAgFnSc4d1	Karajanova
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
kariéru	kariéra	k1gFnSc4	kariéra
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgInSc7d1	významný
mezníkem	mezník	k1gInSc7	mezník
jeho	jeho	k3xOp3gNnSc4	jeho
jmenování	jmenování	k1gNnSc4	jmenování
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
generálním	generální	k2eAgMnSc7d1	generální
hudebním	hudební	k2eAgMnSc7d1	hudební
ředitelem	ředitel	k1gMnSc7	ředitel
(	(	kIx(	(
<g/>
Generalmusikdirektor	Generalmusikdirektor	k1gMnSc1	Generalmusikdirektor
<g/>
)	)	kIx)	)
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hostující	hostující	k2eAgMnSc1d1	hostující
dirigent	dirigent	k1gMnSc1	dirigent
pak	pak	k6eAd1	pak
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
<g/>
,	,	kIx,	,
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
,	,	kIx,	,
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
,	,	kIx,	,
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
či	či	k8xC	či
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
Karajan	Karajan	k1gInSc1	Karajan
debutoval	debutovat	k5eAaBmAgInS	debutovat
s	s	k7c7	s
Berlínskou	berlínský	k2eAgFnSc7d1	Berlínská
filharmonií	filharmonie	k1gFnSc7	filharmonie
a	a	k8xC	a
Berlínskou	berlínský	k2eAgFnSc7d1	Berlínská
státní	státní	k2eAgFnSc7d1	státní
operou	opera	k1gFnSc7	opera
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Beethovenova	Beethovenův	k2eAgMnSc4d1	Beethovenův
Fidelia	Fidelius	k1gMnSc4	Fidelius
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zažil	zažít	k5eAaPmAgMnS	zažít
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
Státní	státní	k2eAgFnSc6d1	státní
opeře	opera	k1gFnSc6	opera
s	s	k7c7	s
Wagnerovou	Wagnerův	k2eAgFnSc7d1	Wagnerova
operou	opera	k1gFnSc7	opera
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Isolda	Isolda	k1gFnSc1	Isolda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
jeho	jeho	k3xOp3gInSc1	jeho
výkon	výkon	k1gInSc1	výkon
kritiky	kritika	k1gFnSc2	kritika
oceněn	ocenit	k5eAaPmNgInS	ocenit
jako	jako	k9	jako
das	das	k?	das
Wunder	Wunder	k1gMnSc1	Wunder
Karajan	Karajan	k1gMnSc1	Karajan
(	(	kIx(	(
<g/>
zázrak	zázrak	k1gInSc1	zázrak
Karajan	Karajan	k1gInSc1	Karajan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
prohlašovala	prohlašovat	k5eAaImAgFnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karajanův	Karajanův	k2eAgInSc1d1	Karajanův
"	"	kIx"	"
<g/>
úspěch	úspěch	k1gInSc1	úspěch
s	s	k7c7	s
Wagnerovým	Wagnerův	k2eAgInSc7d1	Wagnerův
náročným	náročný	k2eAgInSc7d1	náročný
dílem	díl	k1gInSc7	díl
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Isolda	Isolda	k1gFnSc1	Isolda
jej	on	k3xPp3gMnSc4	on
staví	stavit	k5eAaBmIp3nS	stavit
vedle	vedle	k7c2	vedle
Furtwänglera	Furtwängler	k1gMnSc2	Furtwängler
a	a	k8xC	a
Victora	Victor	k1gMnSc2	Victor
de	de	k?	de
Sabata	Sabat	k1gMnSc2	Sabat
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnSc4d3	veliký
operní	operní	k2eAgFnSc4d1	operní
dirigenty	dirigent	k1gMnPc7	dirigent
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Německa	Německo	k1gNnSc2	Německo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
Karajan	Karajan	k1gMnSc1	Karajan
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Deutsche	Deutsche	k1gFnSc7	Deutsche
Grammophon	Grammophona	k1gFnPc2	Grammophona
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
nahrál	nahrát	k5eAaBmAgMnS	nahrát
první	první	k4xOgMnSc1	první
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
,	,	kIx,	,
předehru	předehra	k1gFnSc4	předehra
ke	k	k7c3	k
Kouzelné	kouzelný	k2eAgFnSc3d1	kouzelná
flétně	flétna	k1gFnSc3	flétna
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Staatskapelle	Staatskapelle	k1gFnSc2	Staatskapelle
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1938	[number]	k4	1938
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
operetní	operetní	k2eAgFnSc7d1	operetní
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Elmy	Elma	k1gFnSc2	Elma
Holgerloef	Holgerloef	k1gInSc1	Holgerloef
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1942	[number]	k4	1942
se	se	k3xPyFc4	se
Karajan	Karajan	k1gMnSc1	Karajan
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Maria	Mario	k1gMnSc2	Mario
"	"	kIx"	"
<g/>
Anitou	Anitá	k1gFnSc4	Anitá
<g/>
"	"	kIx"	"
Sauestovou	Sauestová	k1gFnSc7	Sauestová
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
Gütermannovou	Gütermannová	k1gFnSc7	Gütermannová
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
známého	známý	k2eAgMnSc4d1	známý
výrobce	výrobce	k1gMnSc4	výrobce
přízí	příz	k1gFnSc7	příz
pro	pro	k7c4	pro
šicí	šicí	k2eAgInPc4d1	šicí
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
židovského	židovský	k2eAgMnSc4d1	židovský
dědečka	dědeček	k1gMnSc4	dědeček
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c2	za
Vierteljüdin	Vierteljüdina	k1gFnPc2	Vierteljüdina
(	(	kIx(	(
<g/>
čtvrtinová	čtvrtinový	k2eAgFnSc1d1	čtvrtinová
Židovka	Židovka	k1gFnSc1	Židovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Karajan	Karajan	k1gInSc1	Karajan
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
přízeň	přízeň	k1gFnSc4	přízeň
nacistického	nacistický	k2eAgNnSc2d1	nacistické
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
koncerty	koncert	k1gInPc7	koncert
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
Berlíně	Berlín	k1gInSc6	Berlín
ještě	ještě	k9	ještě
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
v	v	k7c6	v
závěrečných	závěrečný	k2eAgFnPc6d1	závěrečná
fázích	fáze	k1gFnPc6	fáze
války	válka	k1gFnSc2	válka
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
s	s	k7c7	s
Anitou	Anita	k1gFnSc7	Anita
do	do	k7c2	do
Milána	Milán	k1gInSc2	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
jim	on	k3xPp3gMnPc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
Victor	Victor	k1gMnSc1	Victor
de	de	k?	de
Sabata	Sabata	k1gFnSc1	Sabata
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Karajan	Karajan	k1gMnSc1	Karajan
a	a	k8xC	a
Anita	Anita	k1gMnSc1	Anita
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Karajan	Karajan	k1gMnSc1	Karajan
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Salcburku	Salcburk	k1gInSc6	Salcburk
8	[number]	k4	8
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
členské	členský	k2eAgNnSc1d1	členské
číslo	číslo	k1gNnSc1	číslo
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
607	[number]	k4	607
525	[number]	k4	525
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1933	[number]	k4	1933
byla	být	k5eAaImAgFnS	být
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
vládou	vláda	k1gFnSc7	vláda
nacistická	nacistický	k2eAgFnSc1d1	nacistická
strana	strana	k1gFnSc1	strana
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Karajanovo	Karajanův	k2eAgNnSc1d1	Karajanův
členství	členství	k1gNnSc1	členství
bylo	být	k5eAaImAgNnS	být
platné	platný	k2eAgNnSc1d1	platné
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
po	po	k7c4	po
připojení	připojení	k1gNnSc4	připojení
Rakouska	Rakousko	k1gNnSc2	Rakousko
k	k	k7c3	k
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
někdejší	někdejší	k2eAgMnPc1d1	někdejší
rakouští	rakouský	k2eAgMnPc1d1	rakouský
členové	člen	k1gMnPc1	člen
ověřeni	ověřen	k2eAgMnPc1d1	ověřen
úřady	úřad	k1gInPc4	úřad
(	(	kIx(	(
<g/>
německé	německý	k2eAgFnPc4d1	německá
<g/>
)	)	kIx)	)
nacistické	nacistický	k2eAgFnPc4d1	nacistická
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Členství	členství	k1gNnSc4	členství
Karajana	Karajan	k1gMnSc2	Karajan
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
za	za	k7c4	za
neplatné	platný	k2eNgNnSc4d1	neplatné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
zpětně	zpětně	k6eAd1	zpětně
určen	určit	k5eAaPmNgInS	určit
ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1933	[number]	k4	1933
v	v	k7c6	v
Ulmu	Ulmus	k1gInSc6	Ulmus
s	s	k7c7	s
členským	členský	k2eAgNnSc7d1	členské
číslem	číslo	k1gNnSc7	číslo
3	[number]	k4	3
430	[number]	k4	430
914	[number]	k4	914
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
muzikolog	muzikolog	k1gMnSc1	muzikolog
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
Richard	Richard	k1gMnSc1	Richard
Osborne	Osborn	k1gInSc5	Osborn
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
Karajanova	Karajanův	k2eAgFnSc1d1	Karajanova
výtečnost	výtečnost	k1gFnSc1	výtečnost
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1933	[number]	k4	1933
a	a	k8xC	a
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
spekulacím	spekulace	k1gFnPc3	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
výhradně	výhradně	k6eAd1	výhradně
kvůli	kvůli	k7c3	kvůli
postupu	postup	k1gInSc3	postup
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
jako	jako	k9	jako
Jim	on	k3xPp3gMnPc3	on
Svejda	Svejda	k1gFnSc1	Svejda
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgMnPc1d1	další
významní	významný	k2eAgMnPc1d1	významný
dirigenti	dirigent	k1gMnPc1	dirigent
jako	jako	k8xC	jako
Arturo	Artura	k1gFnSc5	Artura
Toscanini	Toscanin	k2eAgMnPc1d1	Toscanin
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Klemperer	Klemperer	k1gMnSc1	Klemperer
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
Kleiber	Kleiber	k1gMnSc1	Kleiber
a	a	k8xC	a
Fritz	Fritz	k1gMnSc1	Fritz
Busch	Busch	k1gMnSc1	Busch
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
z	z	k7c2	z
fašistické	fašistický	k2eAgFnSc2d1	fašistická
Evropy	Evropa	k1gFnSc2	Evropa
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Richard	Richard	k1gMnSc1	Richard
Osborne	Osborn	k1gInSc5	Osborn
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgMnPc2d1	významný
dirigentů	dirigent	k1gMnPc2	dirigent
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
válečných	válečný	k2eAgNnPc2d1	válečné
let	léto	k1gNnPc2	léto
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Furtwängler	Furtwängler	k1gMnSc1	Furtwängler
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
Ansermet	Ansermet	k1gMnSc1	Ansermet
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Schuricht	Schuricht	k1gMnSc1	Schuricht
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Böhm	Böhm	k1gMnSc1	Böhm
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Knappertsbusch	Knappertsbusch	k1gMnSc1	Knappertsbusch
<g/>
,	,	kIx,	,
Clemens	Clemens	k1gInSc1	Clemens
Krauss	Krauss	k1gInSc1	Krauss
či	či	k8xC	či
Karl	Karl	k1gMnSc1	Karl
Elmendorff	Elmendorff	k1gMnSc1	Elmendorff
<g/>
.	.	kIx.	.
</s>
<s>
Karajan	Karajan	k1gMnSc1	Karajan
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgMnPc2d3	nejmladší
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
měl	mít	k5eAaImAgMnS	mít
nejmenší	malý	k2eAgFnSc4d3	nejmenší
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Karajanovi	Karajan	k1gMnSc3	Karajan
bylo	být	k5eAaImAgNnS	být
umožněno	umožněn	k2eAgNnSc1d1	umožněno
dirigovat	dirigovat	k5eAaImF	dirigovat
různé	různý	k2eAgInPc4d1	různý
orchestry	orchestr	k1gInPc4	orchestr
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
volně	volně	k6eAd1	volně
cestovat	cestovat	k5eAaImF	cestovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
i	i	k9	i
do	do	k7c2	do
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
dirigoval	dirigovat	k5eAaImAgInS	dirigovat
a	a	k8xC	a
nahrával	nahrávat	k5eAaImAgInS	nahrávat
s	s	k7c7	s
Concertgebouw	Concertgebouw	k1gFnSc7	Concertgebouw
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
.	.	kIx.	.
</s>
<s>
Karajan	Karajan	k1gInSc1	Karajan
byl	být	k5eAaImAgInS	být
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1946	[number]	k4	1946
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
denacifikační	denacifikační	k2eAgFnSc7d1	denacifikační
zkušební	zkušební	k2eAgFnSc7d1	zkušební
komisí	komise	k1gFnSc7	komise
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dirigentské	dirigentský	k2eAgFnSc6d1	dirigentská
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
měl	mít	k5eAaImAgInS	mít
Karajan	Karajan	k1gInSc1	Karajan
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
poválečný	poválečný	k2eAgInSc4d1	poválečný
koncert	koncert	k1gInSc4	koncert
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
s	s	k7c7	s
Vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
filharmonií	filharmonie	k1gFnSc7	filharmonie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dostal	dostat	k5eAaPmAgInS	dostat
zákaz	zákaz	k1gInSc4	zákaz
dalšího	další	k2eAgNnSc2d1	další
dirigování	dirigování	k1gNnSc2	dirigování
sovětskými	sovětský	k2eAgInPc7d1	sovětský
okupačními	okupační	k2eAgInPc7d1	okupační
úřady	úřad	k1gInPc7	úřad
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc2	svůj
nacistické	nacistický	k2eAgFnSc2d1	nacistická
stranické	stranický	k2eAgFnSc2d1	stranická
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
léta	léto	k1gNnSc2	léto
se	se	k3xPyFc4	se
anonymně	anonymně	k6eAd1	anonymně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Salcburského	salcburský	k2eAgInSc2d1	salcburský
festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1947	[number]	k4	1947
měl	mít	k5eAaImAgInS	mít
Karajan	Karajan	k1gInSc1	Karajan
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
veřejný	veřejný	k2eAgInSc4d1	veřejný
koncert	koncert	k1gInSc4	koncert
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
zákazu	zákaz	k1gInSc2	zákaz
dirigování	dirigování	k1gNnSc2	dirigování
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
filharmonií	filharmonie	k1gFnSc7	filharmonie
a	a	k8xC	a
Gesellschaft	Gesellschaft	k1gMnSc1	Gesellschaft
der	drát	k5eAaImRp2nS	drát
Musikfreunde	Musikfreund	k1gMnSc5	Musikfreund
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
pro	pro	k7c4	pro
gramofonou	gramofona	k1gFnSc7	gramofona
nahrál	nahrát	k5eAaBmAgInS	nahrát
Brahmsovo	Brahmsův	k2eAgNnSc1d1	Brahmsovo
Německé	německý	k2eAgNnSc1d1	německé
requiem	requius	k1gMnSc7	requius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
Karajan	Karajan	k1gMnSc1	Karajan
stal	stát	k5eAaPmAgMnS	stát
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
ředitelem	ředitel	k1gMnSc7	ředitel
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
Společnosti	společnost	k1gFnSc2	společnost
přátel	přítel	k1gMnPc2	přítel
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
Gesellschaft	Gesellschaft	k1gInSc1	Gesellschaft
der	drát	k5eAaImRp2nS	drát
Musikfreunde	Musikfreund	k1gMnSc5	Musikfreund
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
v	v	k7c4	v
milánské	milánský	k2eAgNnSc4d1	Milánské
La	la	k1gNnSc4	la
Scale	Scale	k1gFnSc2	Scale
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
aktivitou	aktivita	k1gFnSc7	aktivita
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
nahrávání	nahrávání	k1gNnSc1	nahrávání
s	s	k7c7	s
nově	nově	k6eAd1	nově
vzniklým	vzniklý	k2eAgInSc7d1	vzniklý
souborem	soubor	k1gInSc7	soubor
Philharmonia	Philharmonium	k1gNnSc2	Philharmonium
Orchestra	orchestra	k1gFnSc1	orchestra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
pomohl	pomoct	k5eAaPmAgMnS	pomoct
mu	on	k3xPp3gMnSc3	on
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
světových	světový	k2eAgInPc2d1	světový
orchestrů	orchestr	k1gInPc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
také	také	k6eAd1	také
začala	začít	k5eAaPmAgFnS	začít
Karajanova	Karajanův	k2eAgFnSc1d1	Karajanova
celoživotní	celoživotní	k2eAgFnSc1d1	celoživotní
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
Lucernském	Lucernský	k2eAgInSc6d1	Lucernský
festivalu	festival	k1gInSc6	festival
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
a	a	k8xC	a
1952	[number]	k4	1952
dirigoval	dirigovat	k5eAaImAgInS	dirigovat
ve	v	k7c6	v
Festivalovém	festivalový	k2eAgNnSc6d1	festivalové
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
doživotním	doživotní	k2eAgMnSc7d1	doživotní
hudebním	hudební	k2eAgMnSc7d1	hudební
ředitelem	ředitel	k1gMnSc7	ředitel
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
filharmonie	filharmonie	k1gFnSc2	filharmonie
jako	jako	k8xC	jako
nástupce	nástupce	k1gMnSc4	nástupce
Wilhelma	Wilhelm	k1gMnSc4	Wilhelm
Furtwänglera	Furtwängler	k1gMnSc4	Furtwängler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957	[number]	k4	1957
až	až	k9	až
1964	[number]	k4	1964
byl	být	k5eAaImAgInS	být
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
ředitelem	ředitel	k1gMnSc7	ředitel
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
státní	státní	k2eAgFnSc2d1	státní
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Karajan	Karajan	k1gInSc1	Karajan
byl	být	k5eAaImAgInS	být
úzce	úzko	k6eAd1	úzko
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
Vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
filharmonií	filharmonie	k1gFnSc7	filharmonie
a	a	k8xC	a
Salcburským	salcburský	k2eAgInSc7d1	salcburský
festivalem	festival	k1gInSc7	festival
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založil	založit	k5eAaPmAgInS	založit
velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
zůstat	zůstat	k5eAaPmF	zůstat
svázán	svázán	k2eAgInSc1d1	svázán
s	s	k7c7	s
hudebním	hudební	k2eAgMnSc7d1	hudební
ředitelem	ředitel	k1gMnSc7	ředitel
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
filharmonie	filharmonie	k1gFnSc2	filharmonie
i	i	k9	i
po	po	k7c6	po
konci	konec	k1gInSc6	konec
jeho	jeho	k3xOp3gNnSc2	jeho
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1958	[number]	k4	1958
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
třetí	třetí	k4xOgFnSc7	třetí
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
modelkou	modelka	k1gFnSc7	modelka
Eliette	Eliett	k1gInSc5	Eliett
Mouretovou	Mouretový	k2eAgFnSc4d1	Mouretová
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
rodiči	rodič	k1gMnPc7	rodič
dvou	dva	k4xCgFnPc2	dva
dcer	dcera	k1gFnPc2	dcera
<g/>
,	,	kIx,	,
Isabely	Isabela	k1gFnSc2	Isabela
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
a	a	k8xC	a
Arabely	Arabela	k1gFnSc2	Arabela
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
široká	široký	k2eAgFnSc1d1	široká
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karajan	Karajan	k1gMnSc1	Karajan
měl	mít	k5eAaImAgMnS	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
dar	dar	k1gInSc4	dar
pro	pro	k7c4	pro
vytěžení	vytěžení	k1gNnSc4	vytěžení
krásných	krásný	k2eAgInPc2d1	krásný
zvuků	zvuk	k1gInPc2	zvuk
z	z	k7c2	z
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Stanoviska	stanovisko	k1gNnPc1	stanovisko
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
o	o	k7c6	o
výši	výše	k1gFnSc6	výše
estetického	estetický	k2eAgNnSc2d1	estetické
zakončení	zakončení	k1gNnSc2	zakončení
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgNnSc4	který
"	"	kIx"	"
<g/>
Karajanův	Karajanův	k2eAgInSc4d1	Karajanův
zvuk	zvuk	k1gInSc4	zvuk
<g/>
"	"	kIx"	"
aplikujeme	aplikovat	k5eAaBmIp1nP	aplikovat
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
kritik	kritik	k1gMnSc1	kritik
Harvey	Harvea	k1gFnSc2	Harvea
Sachs	Sachs	k1gInSc1	Sachs
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
přístup	přístup	k1gInSc1	přístup
Karajana	Karajan	k1gMnSc4	Karajan
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karajan	Karajan	k1gMnSc1	Karajan
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
účely	účel	k1gInPc4	účel
vybral	vybrat	k5eAaPmAgInS	vybrat
všestranný	všestranný	k2eAgInSc1d1	všestranný
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
rafinovaný	rafinovaný	k2eAgInSc1d1	rafinovaný
<g/>
,	,	kIx,	,
lakovaný	lakovaný	k2eAgInSc1d1	lakovaný
<g/>
,	,	kIx,	,
vykalkulovaný	vykalkulovaný	k2eAgInSc1d1	vykalkulovaný
smyslný	smyslný	k2eAgInSc1d1	smyslný
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
se	s	k7c7	s
stylistickými	stylistický	k2eAgFnPc7d1	stylistická
úpravami	úprava	k1gFnPc7	úprava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
sám	sám	k3xTgMnSc1	sám
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
vhodné	vhodný	k2eAgNnSc4d1	vhodné
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
aplikován	aplikovat	k5eAaBmNgInS	aplikovat
na	na	k7c6	na
Bacha	Bacha	k?	Bacha
a	a	k8xC	a
Pucciniho	Puccini	k1gMnSc2	Puccini
<g/>
,	,	kIx,	,
Mozarta	Mozart	k1gMnSc2	Mozart
a	a	k8xC	a
Mahlera	Mahler	k1gMnSc2	Mahler
<g/>
,	,	kIx,	,
Beethovena	Beethoven	k1gMnSc2	Beethoven
a	a	k8xC	a
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
,	,	kIx,	,
Schumanna	Schumann	k1gMnSc2	Schumann
a	a	k8xC	a
Stravinského	Stravinský	k2eAgMnSc2d1	Stravinský
<g/>
.	.	kIx.	.
..	..	k?	..
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
vystoupení	vystoupení	k1gNnPc2	vystoupení
mělo	mít	k5eAaImAgNnS	mít
prefabrikované	prefabrikovaný	k2eAgNnSc1d1	prefabrikované
<g/>
,	,	kIx,	,
umělé	umělý	k2eAgFnPc1d1	umělá
kvality	kvalita	k1gFnPc1	kvalita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Toscanini	Toscanin	k2eAgMnPc1d1	Toscanin
<g/>
,	,	kIx,	,
Furtwängler	Furtwängler	k1gMnSc1	Furtwängler
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
nikdy	nikdy	k6eAd1	nikdy
nedosáhli	dosáhnout	k5eNaPmAgMnP	dosáhnout
...	...	k?	...
většina	většina	k1gFnSc1	většina
Karajanových	Karajanův	k2eAgFnPc2d1	Karajanova
nahrávek	nahrávka	k1gFnPc2	nahrávka
je	být	k5eAaImIp3nS	být
přehnaně	přehnaně	k6eAd1	přehnaně
leštěná	leštěný	k2eAgFnSc1d1	leštěná
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jakýsi	jakýsi	k3yIgInSc4	jakýsi
zvukový	zvukový	k2eAgInSc4d1	zvukový
protějšek	protějšek	k1gInSc4	protějšek
k	k	k7c3	k
filmům	film	k1gInPc3	film
a	a	k8xC	a
fotografiím	fotografia	k1gFnPc3	fotografia
Leni	Leni	k?	Leni
Riefenstahlové	Riefenstahlové	k2eAgMnSc2d1	Riefenstahlové
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Komentátor	komentátor	k1gMnSc1	komentátor
Jim	on	k3xPp3gMnPc3	on
Svejda	Svejda	k1gMnSc1	Svejda
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
nicméně	nicméně	k8xC	nicméně
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karajan	Karajan	k1gInSc1	Karajan
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1970	[number]	k4	1970
nezní	znět	k5eNaImIp3nS	znět
tak	tak	k6eAd1	tak
leštěně	leštěně	k6eAd1	leštěně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
má	mít	k5eAaImIp3nS	mít
údajně	údajně	k6eAd1	údajně
znít	znít	k5eAaImF	znít
v	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
recenze	recenze	k1gFnPc1	recenze
z	z	k7c2	z
publikace	publikace	k1gFnSc2	publikace
Penguin	Penguina	k1gFnPc2	Penguina
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
Compact	Compact	k2eAgInSc1d1	Compact
Discs	Discs	k1gInSc1	Discs
to	ten	k3xDgNnSc4	ten
ilustrují	ilustrovat	k5eAaBmIp3nP	ilustrovat
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nahrávku	nahrávka	k1gFnSc4	nahrávka
Wagnerova	Wagnerův	k2eAgMnSc2d1	Wagnerův
Tristana	Tristan	k1gMnSc2	Tristan
a	a	k8xC	a
Isoldy	Isolda	k1gFnSc2	Isolda
<g/>
,	,	kIx,	,
kanonického	kanonický	k2eAgNnSc2d1	kanonické
romantického	romantický	k2eAgNnSc2d1	romantické
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
autoři	autor	k1gMnPc1	autor
Penguina	Penguino	k1gNnSc2	Penguino
napsali	napsat	k5eAaBmAgMnP	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Karajanovo	Karajanův	k2eAgNnSc1d1	Karajanův
smyslné	smyslný	k2eAgNnSc1d1	smyslné
představení	představení	k1gNnSc1	představení
Wagnerova	Wagnerův	k2eAgNnSc2d1	Wagnerovo
mistrovského	mistrovský	k2eAgNnSc2d1	mistrovské
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
mazlivě	mazlivě	k6eAd1	mazlivě
krásné	krásný	k2eAgNnSc1d1	krásné
a	a	k8xC	a
skvěle	skvěle	k6eAd1	skvěle
rafinovaně	rafinovaně	k6eAd1	rafinovaně
hrané	hraný	k2eAgInPc4d1	hraný
Berlínskými	berlínský	k2eAgMnPc7d1	berlínský
filharmoniky	filharmonik	k1gMnPc7	filharmonik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Karajanově	Karajanův	k2eAgFnSc6d1	Karajanova
nahrávce	nahrávka	k1gFnSc6	nahrávka
Haydnovy	Haydnův	k2eAgFnPc1d1	Haydnova
"	"	kIx"	"
<g/>
Pařížské	pařížský	k2eAgFnPc1d1	Pařížská
<g/>
"	"	kIx"	"
symfonie	symfonie	k1gFnPc1	symfonie
<g/>
,	,	kIx,	,
titíž	týž	k3xTgMnPc1	týž
autoři	autor	k1gMnPc1	autor
napsali	napsat	k5eAaBmAgMnP	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
big-band	bigand	k1gInSc1	big-band
Haydn	Haydn	k1gInSc1	Haydn
s	s	k7c7	s
pomstou	pomsta	k1gFnSc7	pomsta
...	...	k?	...
Je	být	k5eAaImIp3nS	být
samozřejmé	samozřejmý	k2eAgNnSc1d1	samozřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvalita	kvalita	k1gFnSc1	kvalita
orchestrální	orchestrální	k2eAgFnSc2d1	orchestrální
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
vynikající	vynikající	k2eAgMnSc1d1	vynikající
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jsou	být	k5eAaImIp3nP	být
tam	tam	k6eAd1	tam
přehnaně	přehnaně	k6eAd1	přehnaně
tvrdé	tvrdý	k2eAgInPc4d1	tvrdý
(	(	kIx(	(
<g/>
účty	účet	k1gInPc4	účet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
bližší	bližší	k1gNnSc4	bližší
k	k	k7c3	k
imperiálnímu	imperiální	k2eAgInSc3d1	imperiální
Berlínu	Berlín	k1gInSc3	Berlín
než	než	k8xS	než
k	k	k7c3	k
Paříži	Paříž	k1gFnSc3	Paříž
...	...	k?	...
menuety	menueta	k1gFnPc1	menueta
jsou	být	k5eAaImIp3nP	být
opravdu	opravdu	k6eAd1	opravdu
velmi	velmi	k6eAd1	velmi
pomalé	pomalý	k2eAgNnSc1d1	pomalé
...	...	k?	...
Tyto	tento	k3xDgInPc1	tento
výkony	výkon	k1gInPc1	výkon
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
nezajímavé	zajímavý	k2eNgFnPc1d1	nezajímavá
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
chtějí	chtít	k5eAaImIp3nP	chtít
milostivě	milostivě	k6eAd1	milostivě
být	být	k5eAaImF	být
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
srdce	srdce	k1gNnSc2	srdce
doporučovány	doporučovat	k5eAaImNgFnP	doporučovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Stejný	stejný	k2eAgInSc4d1	stejný
Penguin	Penguin	k1gInSc4	Penguin
Guide	Guid	k1gInSc5	Guid
přesto	přesto	k8xC	přesto
vysekl	vyseknout	k5eAaPmAgMnS	vyseknout
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
poklonu	poklona	k1gFnSc4	poklona
Karajanovým	Karajanův	k2eAgFnPc3d1	Karajanova
nahrávkám	nahrávka	k1gFnPc3	nahrávka
obou	dva	k4xCgNnPc2	dva
Haydnových	Haydnův	k2eAgNnPc2d1	Haydnovo
oratorií	oratorium	k1gNnPc2	oratorium
<g/>
,	,	kIx,	,
Stvoření	stvoření	k1gNnSc1	stvoření
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Schöpfung	Schöpfung	k1gMnSc1	Schöpfung
<g/>
)	)	kIx)	)
i	i	k8xC	i
Roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc7	Die
Jahreszeiten	Jahreszeiten	k2eAgInSc4d1	Jahreszeiten
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
muzikolog	muzikolog	k1gMnSc1	muzikolog
HC	HC	kA	HC
Robbins	Robbinsa	k1gFnPc2	Robbinsa
Landon	Landon	k1gMnSc1	Landon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
Haydnem	Haydn	k1gInSc7	Haydn
zabýval	zabývat	k5eAaImAgInS	zabývat
a	a	k8xC	a
který	který	k3yRgMnSc1	který
psal	psát	k5eAaImAgMnS	psát
poznámky	poznámka	k1gFnPc4	poznámka
pro	pro	k7c4	pro
Karajanovo	Karajanův	k2eAgNnSc4d1	Karajanův
nahrávání	nahrávání	k1gNnSc4	nahrávání
Haydnových	Haydnových	k2eAgNnSc4d1	Haydnových
12	[number]	k4	12
londýnských	londýnský	k2eAgFnPc2d1	londýnská
symfonií	symfonie	k1gFnPc2	symfonie
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karajanovy	Karajanův	k2eAgFnPc1d1	Karajanova
nahrávky	nahrávka	k1gFnPc1	nahrávka
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zná	znát	k5eAaImIp3nS	znát
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Karajan	Karajan	k1gMnSc1	Karajan
dával	dávat	k5eAaImAgMnS	dávat
z	z	k7c2	z
hudebních	hudební	k2eAgNnPc2d1	hudební
děl	dělo	k1gNnPc2	dělo
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
velkou	velký	k2eAgFnSc4d1	velká
přednost	přednost	k1gFnSc4	přednost
provádění	provádění	k1gNnSc2	provádění
a	a	k8xC	a
nahrávání	nahrávání	k1gNnSc2	nahrávání
kompozic	kompozice	k1gFnPc2	kompozice
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1945	[number]	k4	1945
takových	takový	k3xDgMnPc2	takový
skladatelů	skladatel	k1gMnPc2	skladatel
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Mahler	Mahler	k1gMnSc1	Mahler
<g/>
,	,	kIx,	,
Schönberg	Schönberg	k1gMnSc1	Schönberg
<g/>
,	,	kIx,	,
Berg	Berg	k1gMnSc1	Berg
<g/>
,	,	kIx,	,
Webern	Webern	k1gMnSc1	Webern
<g/>
,	,	kIx,	,
Bartók	Bartók	k1gMnSc1	Bartók
<g/>
,	,	kIx,	,
Sibelius	Sibelius	k1gMnSc1	Sibelius
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Strauss	Straussa	k1gFnPc2	Straussa
<g/>
,	,	kIx,	,
Puccini	Puccin	k1gMnPc1	Puccin
<g/>
,	,	kIx,	,
Pizzeti	Pizzeti	k1gMnSc1	Pizzeti
<g/>
,	,	kIx,	,
Honegger	Honegger	k1gMnSc1	Honegger
<g/>
,	,	kIx,	,
Prokofjev	Prokofjev	k1gMnSc1	Prokofjev
<g/>
,	,	kIx,	,
Debussy	Debussa	k1gFnPc1	Debussa
<g/>
,	,	kIx,	,
Ravel	Ravel	k1gMnSc1	Ravel
<g/>
,	,	kIx,	,
Hindemith	Hindemith	k1gMnSc1	Hindemith
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Werner	Werner	k1gMnSc1	Werner
Henze	Henze	k1gFnSc1	Henze
<g/>
,	,	kIx,	,
Nielsen	Nielsen	k1gInSc1	Nielsen
a	a	k8xC	a
Stravinskij	Stravinskij	k1gFnSc1	Stravinskij
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
uvedl	uvést	k5eAaPmAgInS	uvést
s	s	k7c7	s
Berlínskou	berlínský	k2eAgFnSc7d1	Berlínská
filharmonií	filharmonie	k1gFnSc7	filharmonie
Henzovu	Henzův	k2eAgFnSc4d1	Henzův
Sonata	Sonat	k2eAgFnSc1d1	Sonata
per	pero	k1gNnPc2	pero
Archii	Archie	k1gFnSc6	Archie
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karajan	Karajan	k1gInSc1	Karajan
také	také	k9	také
dvakrát	dvakrát	k6eAd1	dvakrát
nahrál	nahrát	k5eAaBmAgMnS	nahrát
Šostakovičovu	Šostakovičův	k2eAgFnSc4d1	Šostakovičova
Desátou	desátá	k1gFnSc4	desátá
symfonii	symfonie	k1gFnSc4	symfonie
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
premiéroval	premiérovat	k5eAaBmAgInS	premiérovat
s	s	k7c7	s
WDR	WDR	kA	WDR
Sinfonieorchester	Sinfonieorchester	k1gInSc1	Sinfonieorchester
Köln	Köln	k1gInSc1	Köln
(	(	kIx(	(
<g/>
Rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestru	orchestra	k1gFnSc4	orchestra
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
)	)	kIx)	)
operu	opera	k1gFnSc4	opera
De	De	k?	De
temporum	temporum	k1gInSc1	temporum
fine	finat	k5eAaPmIp3nS	finat
comoedia	comoedium	k1gNnPc4	comoedium
Carla	Carl	k1gMnSc2	Carl
Orffa	Orff	k1gMnSc2	Orff
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Rockwell	Rockwell	k1gMnSc1	Rockwell
z	z	k7c2	z
deníku	deník	k1gInSc2	deník
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Měl	mít	k5eAaImAgInS	mít
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
dar	dar	k1gInSc1	dar
pro	pro	k7c4	pro
Wagnera	Wagner	k1gMnSc4	Wagner
a	a	k8xC	a
především	především	k9	především
pro	pro	k7c4	pro
Brucknera	Bruckner	k1gMnSc4	Bruckner
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
hudbu	hudba	k1gFnSc4	hudba
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
se	s	k7c7	s
suverénním	suverénní	k2eAgNnSc7d1	suverénní
vedením	vedení	k1gNnSc7	vedení
a	a	k8xC	a
vznešeným	vznešený	k2eAgInSc7d1	vznešený
citem	cit	k1gInSc7	cit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
televizní	televizní	k2eAgFnSc7d1	televizní
společností	společnost	k1gFnSc7	společnost
ZDF	ZDF	kA	ZDF
Karajan	Karajan	k1gMnSc1	Karajan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nP	kdyby
byl	být	k5eAaImAgInS	být
skladatelem	skladatel	k1gMnSc7	skladatel
místo	místo	k7c2	místo
dirigentem	dirigent	k1gMnSc7	dirigent
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gFnSc1	jeho
hudba	hudba	k1gFnSc1	hudba
zněla	znět	k5eAaImAgFnS	znět
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Šostakovičova	Šostakovičův	k2eAgMnSc2d1	Šostakovičův
<g/>
,	,	kIx,	,
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
který	který	k3yRgMnSc1	který
identifikoval	identifikovat	k5eAaBmAgMnS	identifikovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
potkal	potkat	k5eAaPmAgMnS	potkat
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
s	s	k7c7	s
Berlínskou	berlínský	k2eAgFnSc7d1	Berlínská
filharmonií	filharmonie	k1gFnSc7	filharmonie
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
vrcholilo	vrcholit	k5eAaImAgNnS	vrcholit
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Karajan	Karajan	k1gMnSc1	Karajan
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c4	v
dirigování	dirigování	k1gNnSc4	dirigování
a	a	k8xC	a
nahrávání	nahrávání	k1gNnSc4	nahrávání
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
Anifu	Anif	k1gInSc6	Anif
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
s	s	k7c7	s
Berlínskou	berlínský	k2eAgFnSc7d1	Berlínská
filharmonií	filharmonie	k1gFnSc7	filharmonie
a	a	k8xC	a
Vídeňskými	vídeňský	k2eAgMnPc7d1	vídeňský
filharmoniky	filharmonik	k1gMnPc7	filharmonik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
Karajan	Karajan	k1gInSc4	Karajan
trpěl	trpět	k5eAaImAgMnS	trpět
srdečními	srdeční	k2eAgInPc7d1	srdeční
problémy	problém	k1gInPc7	problém
a	a	k8xC	a
problémy	problém	k1gInPc1	problém
se	s	k7c7	s
zády	záda	k1gNnPc7	záda
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
dostával	dostávat	k5eAaImAgMnS	dostávat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
tendence	tendence	k1gFnPc4	tendence
k	k	k7c3	k
všekontrolujícímu	všekontrolující	k2eAgMnSc3d1	všekontrolující
až	až	k8xS	až
diktátorskému	diktátorský	k2eAgInSc3d1	diktátorský
stylu	styl	k1gInSc3	styl
dirigování	dirigování	k1gNnSc2	dirigování
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
už	už	k6eAd1	už
jinde	jinde	k6eAd1	jinde
zmizel	zmizet	k5eAaPmAgMnS	zmizet
z	z	k7c2	z
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Karajan	Karajan	k1gMnSc1	Karajan
oficiálně	oficiálně	k6eAd1	oficiálně
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
výslužby	výslužba	k1gFnSc2	výslužba
z	z	k7c2	z
vedení	vedení	k1gNnSc2	vedení
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
filharmonie	filharmonie	k1gFnSc2	filharmonie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
prováděl	provádět	k5eAaImAgInS	provádět
zkoušky	zkouška	k1gFnSc2	zkouška
Verdiho	Verdi	k1gMnSc2	Verdi
Maškarního	maškarní	k2eAgInSc2d1	maškarní
plesu	ples	k1gInSc2	ples
pro	pro	k7c4	pro
každoroční	každoroční	k2eAgInSc4d1	každoroční
Salcburský	salcburský	k2eAgInSc4d1	salcburský
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
selhání	selhání	k1gNnSc4	selhání
srdce	srdce	k1gNnSc2	srdce
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Anifu	Anif	k1gInSc6	Anif
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1989	[number]	k4	1989
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
81	[number]	k4	81
let	léto	k1gNnPc2	léto
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
tamním	tamní	k2eAgInSc6d1	tamní
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Praktikující	praktikující	k2eAgMnSc1d1	praktikující
zenový	zenový	k2eAgMnSc1d1	zenový
buddhista	buddhista	k1gMnSc1	buddhista
Karajan	Karajan	k1gMnSc1	Karajan
silně	silně	k6eAd1	silně
věřil	věřit	k5eAaImAgMnS	věřit
v	v	k7c6	v
reinkarnaci	reinkarnace	k1gFnSc6	reinkarnace
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
znovuzrozen	znovuzrodit	k5eAaPmNgMnS	znovuzrodit
jako	jako	k9	jako
orel	orel	k1gMnSc1	orel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
stoupat	stoupat	k5eAaImF	stoupat
nad	nad	k7c4	nad
své	svůj	k3xOyFgFnPc4	svůj
milované	milovaný	k2eAgFnPc4d1	milovaná
Alpy	Alpy	k1gFnPc4	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Karajan	Karajan	k1gMnSc1	Karajan
byl	být	k5eAaImAgMnS	být
příjemcem	příjemce	k1gMnSc7	příjemce
mnoha	mnoho	k4c2	mnoho
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
a	a	k8xC	a
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1960	[number]	k4	1960
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velkodůstojníkem	velkodůstojník	k1gInSc7	velkodůstojník
Řádu	řád	k1gInSc2	řád
zásluh	zásluha	k1gFnPc2	zásluha
o	o	k7c4	o
Italskou	italský	k2eAgFnSc4d1	italská
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
získal	získat	k5eAaPmAgInS	získat
rakouský	rakouský	k2eAgInSc1d1	rakouský
čestný	čestný	k2eAgInSc1d1	čestný
odznak	odznak	k1gInSc1	odznak
Za	za	k7c4	za
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
velký	velký	k2eAgInSc1d1	velký
záslužný	záslužný	k2eAgInSc1d1	záslužný
kříž	kříž	k1gInSc1	kříž
(	(	kIx(	(
<g/>
Grosses	Grosses	k1gMnSc1	Grosses
Bundesverdienstkreuz	Bundesverdienstkreuz	k1gMnSc1	Bundesverdienstkreuz
<g/>
)	)	kIx)	)
Řádu	řád	k1gInSc2	řád
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
hudební	hudební	k2eAgFnSc1d1	hudební
cena	cena	k1gFnSc1	cena
Ernsta	Ernst	k1gMnSc2	Ernst
Siemense	siemens	k1gInSc5	siemens
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1978	[number]	k4	1978
obdržel	obdržet	k5eAaPmAgInS	obdržet
čestný	čestný	k2eAgInSc1d1	čestný
titul	titul	k1gInSc1	titul
Doctor	Doctor	k1gMnSc1	Doctor
of	of	k?	of
Music	Music	k1gMnSc1	Music
na	na	k7c6	na
Oxfordské	oxfordský	k2eAgFnSc6d1	Oxfordská
univerzitě	univerzita	k1gFnSc6	univerzita
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
poctěn	poctít	k5eAaPmNgInS	poctít
"	"	kIx"	"
<g/>
Medaille	Medaille	k1gInSc1	Medaille
de	de	k?	de
Vermeil	Vermeil	k1gInSc1	Vermeil
<g/>
"	"	kIx"	"
na	na	k7c4	na
Académie	Académie	k1gFnPc4	Académie
française	française	k1gFnSc2	française
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
Královské	královský	k2eAgFnSc2d1	královská
filharmonické	filharmonický	k2eAgFnSc2d1	filharmonická
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
cenou	cena	k1gFnSc7	cena
Onassisovy	Onassisův	k2eAgFnSc2d1	Onassisova
nadace	nadace	k1gFnSc2	nadace
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
hudební	hudební	k2eAgFnSc7d1	hudební
cenou	cena	k1gFnSc7	cena
od	od	k7c2	od
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
dvě	dva	k4xCgNnPc4	dva
Gramophone	Gramophon	k1gInSc5	Gramophon
Award	Award	k1gInSc1	Award
za	za	k7c4	za
nahrávky	nahrávka	k1gFnPc4	nahrávka
Mahlerovy	Mahlerův	k2eAgFnPc4d1	Mahlerova
Deváté	devátý	k4xOgFnPc4	devátý
symfonie	symfonie	k1gFnPc4	symfonie
a	a	k8xC	a
kompletní	kompletní	k2eAgFnSc4d1	kompletní
nahrávku	nahrávka	k1gFnSc4	nahrávka
Parsifala	Parsifala	k1gFnSc2	Parsifala
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
přijal	přijmout	k5eAaPmAgInS	přijmout
čestný	čestný	k2eAgInSc1d1	čestný
prsten	prsten	k1gInSc1	prsten
od	od	k7c2	od
německé	německý	k2eAgFnSc2d1	německá
nadace	nadace	k1gFnSc2	nadace
Eduard-Rhein-Stiftung	Eduard-Rhein-Stiftunga	k1gFnPc2	Eduard-Rhein-Stiftunga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
založena	založen	k2eAgFnSc1d1	založena
Hudební	hudební	k2eAgFnSc1d1	hudební
cena	cena	k1gFnSc1	cena
Herberta	Herberta	k1gFnSc1	Herberta
von	von	k1gInSc1	von
Karajana	Karajana	k1gFnSc1	Karajana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgNnSc7	první
držitelkou	držitelka	k1gFnSc7	držitelka
tohoto	tento	k3xDgNnSc2	tento
ocenění	ocenění	k1gNnSc2	ocenění
stala	stát	k5eAaPmAgFnS	stát
německá	německý	k2eAgFnSc1d1	německá
houslistka	houslistka	k1gFnSc1	houslistka
Anne-Sophie	Anne-Sophie	k1gFnSc2	Anne-Sophie
Mutter	Muttra	k1gFnPc2	Muttra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
debutovala	debutovat	k5eAaBmAgFnS	debutovat
s	s	k7c7	s
Karajanem	Karajan	k1gInSc7	Karajan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Gramofonové	gramofonový	k2eAgFnSc2d1	gramofonová
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
UNESCO	UNESCO	kA	UNESCO
získal	získat	k5eAaPmAgInS	získat
Picassovu	Picassův	k2eAgFnSc4d1	Picassova
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
v	v	k7c6	v
Baden-Badenském	Baden-Badenský	k2eAgNnSc6d1	Baden-Badenský
festivalovém	festivalový	k2eAgNnSc6d1	festivalové
divadle	divadlo	k1gNnSc6	divadlo
koná	konat	k5eAaImIp3nS	konat
"	"	kIx"	"
<g/>
Herbert	Herbert	k1gInSc1	Herbert
von	von	k1gInSc1	von
Karajanův	Karajanův	k2eAgInSc1d1	Karajanův
svatodušní	svatodušní	k2eAgInSc1d1	svatodušní
festival	festival	k1gInSc1	festival
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Karajan	Karajan	k1gMnSc1	Karajan
je	být	k5eAaImIp3nS	být
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
Salcburku	Salcburk	k1gInSc2	Salcburk
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Berlína	Berlín	k1gInSc2	Berlín
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vídně	Vídeň	k1gFnSc2	Vídeň
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
