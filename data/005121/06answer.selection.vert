<s>
Byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejznámějších	známý	k2eAgMnPc2d3
a	a	k8xC
nejvýznamnějších	významný	k2eAgMnPc2d3
dirigentů	dirigent	k1gMnPc2
20	[number]	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
dominantní	dominantní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
v	v	k7c6
evropské	evropský	k2eAgFnSc6d1
klasické	klasický	k2eAgFnSc6d1
hudbě	hudba	k1gFnSc6
od	od	k7c2
roku	rok	k1gInSc2
1960	[number]	k4
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>