<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
vrstva	vrstva	k1gFnSc1	vrstva
plynů	plyn	k1gInPc2	plyn
obklopující	obklopující	k2eAgFnSc4d1	obklopující
planetu	planeta	k1gFnSc4	planeta
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zemskou	zemský	k2eAgFnSc7d1	zemská
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
78	[number]	k4	78
%	%	kIx~	%
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
21	[number]	k4	21
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
se	s	k7c7	s
stopovým	stopový	k2eAgNnSc7d1	stopové
množstvím	množství	k1gNnSc7	množství
dalších	další	k2eAgInPc2d1	další
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
chrání	chránit	k5eAaImIp3nS	chránit
pozemský	pozemský	k2eAgInSc4d1	pozemský
život	život	k1gInSc4	život
před	před	k7c7	před
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
sluneční	sluneční	k2eAgFnSc7d1	sluneční
a	a	k8xC	a
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
radiací	radiace	k1gFnSc7	radiace
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
setrvačností	setrvačnost	k1gFnSc7	setrvačnost
snižuje	snižovat	k5eAaImIp3nS	snižovat
teplotní	teplotní	k2eAgInPc4d1	teplotní
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
dnem	den	k1gInSc7	den
a	a	k8xC	a
nocí	noc	k1gFnSc7	noc
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
nemá	mít	k5eNaImIp3nS	mít
jednoznačnou	jednoznačný	k2eAgFnSc4d1	jednoznačná
vrchní	vrchní	k2eAgFnSc4d1	vrchní
hranici	hranice	k1gFnSc4	hranice
-	-	kIx~	-
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
plynule	plynule	k6eAd1	plynule
řídne	řídnout	k5eAaImIp3nS	řídnout
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
hmoty	hmota	k1gFnSc2	hmota
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
11	[number]	k4	11
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
NASA	NASA	kA	NASA
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
pohybující	pohybující	k2eAgMnSc1d1	pohybující
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
výšce	výška	k1gFnSc6	výška
než	než	k8xS	než
přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
km	km	kA	km
(	(	kIx(	(
<g/>
50	[number]	k4	50
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
astronautem	astronaut	k1gMnSc7	astronaut
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
vnější	vnější	k2eAgFnSc7d1	vnější
hranicí	hranice	k1gFnSc7	hranice
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
také	také	k9	také
Karmanova	Karmanův	k2eAgFnSc1d1	Karmanův
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
100	[number]	k4	100
km	km	kA	km
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
hranice	hranice	k1gFnSc2	hranice
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
termínu	termín	k1gInSc2	termín
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
již	již	k6eAd1	již
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
s	s	k7c7	s
dnešním	dnešní	k2eAgNnSc7d1	dnešní
složením	složení	k1gNnSc7	složení
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
výsledek	výsledek	k1gInSc4	výsledek
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
soustavně	soustavně	k6eAd1	soustavně
přetvářena	přetvářit	k5eAaPmNgFnS	přetvářit
živými	živý	k2eAgInPc7d1	živý
organismy	organismus	k1gInPc7	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
složení	složení	k1gNnSc1	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
po	po	k7c6	po
zformování	zformování	k1gNnSc6	zformování
planety	planeta	k1gFnSc2	planeta
bylo	být	k5eAaImAgNnS	být
chemicky	chemicky	k6eAd1	chemicky
zcela	zcela	k6eAd1	zcela
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
směs	směs	k1gFnSc1	směs
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
uvolnily	uvolnit	k5eAaPmAgInP	uvolnit
z	z	k7c2	z
odplynění	odplynění	k1gNnSc2	odplynění
magmatu	magma	k1gNnSc2	magma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
rozprostíralo	rozprostírat	k5eAaImAgNnS	rozprostírat
v	v	k7c6	v
ohromném	ohromný	k2eAgInSc6d1	ohromný
magmatickém	magmatický	k2eAgInSc6d1	magmatický
oceánu	oceán	k1gInSc6	oceán
po	po	k7c6	po
většině	většina	k1gFnSc6	většina
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
zanesly	zanést	k5eAaPmAgFnP	zanést
kolize	kolize	k1gFnPc1	kolize
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
tělesy	těleso	k1gNnPc7	těleso
bombardujícími	bombardující	k2eAgNnPc7d1	bombardující
povrch	povrch	k7c2wR	povrch
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
atmosféra	atmosféra	k1gFnSc1	atmosféra
pro	pro	k7c4	pro
život	život	k1gInSc4	život
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
podobě	podoba	k1gFnSc6	podoba
toxická	toxický	k2eAgFnSc1d1	toxická
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc4	rozšíření
zelených	zelený	k2eAgFnPc2d1	zelená
řas	řasa	k1gFnPc2	řasa
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
nastal	nastat	k5eAaPmAgInS	nastat
proces	proces	k1gInSc1	proces
změny	změna	k1gFnSc2	změna
složení	složení	k1gNnSc2	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
jako	jako	k8xS	jako
odpadní	odpadní	k2eAgInSc4d1	odpadní
plyn	plyn	k1gInSc4	plyn
dostávat	dostávat	k5eAaImF	dostávat
toxický	toxický	k2eAgMnSc1d1	toxický
a	a	k8xC	a
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
životních	životní	k2eAgFnPc2d1	životní
forem	forma	k1gFnPc2	forma
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
vyšší	vysoký	k2eAgFnPc1d2	vyšší
vrstvy	vrstva	k1gFnPc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
již	již	k6eAd1	již
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zoxidování	zoxidování	k1gNnSc6	zoxidování
hornin	hornina	k1gFnPc2	hornina
jeho	on	k3xPp3gInSc2	on
procentuální	procentuální	k2eAgNnSc1d1	procentuální
zastoupení	zastoupení	k1gNnSc1	zastoupení
postupně	postupně	k6eAd1	postupně
narůstalo	narůstat	k5eAaImAgNnS	narůstat
(	(	kIx(	(
<g/>
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
kambrické	kambrický	k2eAgFnSc3d1	kambrická
explozi	exploze	k1gFnSc3	exploze
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnoty	hodnota	k1gFnPc4	hodnota
okolo	okolo	k7c2	okolo
21	[number]	k4	21
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
složení	složení	k1gNnSc1	složení
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
;	;	kIx,	;
konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
úměra	úměra	k1gFnSc1	úměra
mezi	mezi	k7c7	mezi
výškou	výška	k1gFnSc7	výška
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
se	se	k3xPyFc4	se
však	však	k9	však
rovněž	rovněž	k9	rovněž
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
vztahu	vztah	k1gInSc2	vztah
tedy	tedy	k9	tedy
dělíme	dělit	k5eAaImIp1nP	dělit
zemskou	zemský	k2eAgFnSc4d1	zemská
atmosféru	atmosféra	k1gFnSc4	atmosféra
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
vrstvy	vrstva	k1gFnPc4	vrstva
(	(	kIx(	(
<g/>
uváděné	uváděný	k2eAgFnSc2d1	uváděná
výšky	výška	k1gFnSc2	výška
jsou	být	k5eAaImIp3nP	být
přibližné	přibližný	k2eAgFnPc1d1	přibližná
-	-	kIx~	-
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
fyzikálních	fyzikální	k2eAgInPc6d1	fyzikální
parametrech	parametr	k1gInPc6	parametr
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Troposféra	troposféra	k1gFnSc1	troposféra
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
tropos	tropos	k1gInSc1	tropos
<g/>
"	"	kIx"	"
-	-	kIx~	-
mísit	mísit	k5eAaImF	mísit
<g/>
.	.	kIx.	.
</s>
<s>
Troposféra	troposféra	k1gFnSc1	troposféra
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
až	až	k6eAd1	až
do	do	k7c2	do
7	[number]	k4	7
km	km	kA	km
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
17	[number]	k4	17
km	km	kA	km
okolo	okolo	k7c2	okolo
rovníku	rovník	k1gInSc2	rovník
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
vrstvou	vrstva	k1gFnSc7	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
troposféry	troposféra	k1gFnSc2	troposféra
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Stratosféra	stratosféra	k1gFnSc1	stratosféra
Sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
troposféry	troposféra	k1gFnSc2	troposféra
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
50	[number]	k4	50
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
zde	zde	k6eAd1	zde
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Mezosféra	Mezosféra	k1gFnSc1	Mezosféra
Sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
stratosféry	stratosféra	k1gFnSc2	stratosféra
do	do	k7c2	do
80	[number]	k4	80
až	až	k9	až
85	[number]	k4	85
km	km	kA	km
<g/>
;	;	kIx,	;
teplota	teplota	k1gFnSc1	teplota
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Termosféra	Termosféra	k1gFnSc1	Termosféra
Sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
mezosféry	mezosféra	k1gFnSc2	mezosféra
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
cca	cca	kA	cca
640	[number]	k4	640
km	km	kA	km
od	od	k7c2	od
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
stoupá	stoupat	k5eAaImIp3nS	stoupat
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Exosféra	exosféra	k1gFnSc1	exosféra
Sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
termosféry	termosféra	k1gFnSc2	termosféra
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
sluneční	sluneční	k2eAgFnSc6d1	sluneční
aktivitě	aktivita	k1gFnSc6	aktivita
od	od	k7c2	od
500	[number]	k4	500
do	do	k7c2	do
1000	[number]	k4	1000
km	km	kA	km
<g/>
)	)	kIx)	)
do	do	k7c2	do
takové	takový	k3xDgFnSc2	takový
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k9	ještě
převažuje	převažovat	k5eAaImIp3nS	převažovat
gravitační	gravitační	k2eAgNnSc4d1	gravitační
působení	působení	k1gNnSc4	působení
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
hranice	hranice	k1gFnSc1	hranice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
vliv	vliv	k1gInSc4	vliv
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
na	na	k7c4	na
atomární	atomární	k2eAgInSc4d1	atomární
vodík	vodík	k1gInSc4	vodík
převáží	převážit	k5eAaPmIp3nS	převážit
nad	nad	k7c7	nad
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
působením	působení	k1gNnSc7	působení
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
(	(	kIx(	(
<g/>
190	[number]	k4	190
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Exosféra	exosféra	k1gFnSc1	exosféra
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
z	z	k7c2	z
kosmu	kosmos	k1gInSc2	kosmos
jako	jako	k8xC	jako
geokoróna	geokoróna	k1gFnSc1	geokoróna
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
minimálně	minimálně	k6eAd1	minimálně
10	[number]	k4	10
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
vrstvami	vrstva	k1gFnPc7	vrstva
jsou	být	k5eAaImIp3nP	být
nazývány	nazýván	k2eAgFnPc1d1	nazývána
tropopauza	tropopauza	k1gFnSc1	tropopauza
<g/>
,	,	kIx,	,
stratopauza	stratopauza	k1gFnSc1	stratopauza
<g/>
,	,	kIx,	,
mezopauza	mezopauza	k1gFnSc1	mezopauza
a	a	k8xC	a
termopauza	termopauza	k1gFnSc1	termopauza
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
atmosféry	atmosféra	k1gFnSc2	atmosféra
u	u	k7c2	u
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
14	[number]	k4	14
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
klesá	klesat	k5eAaImIp3nS	klesat
průměrně	průměrně	k6eAd1	průměrně
o	o	k7c4	o
0,65	[number]	k4	0,65
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c4	na
každých	každý	k3xTgFnPc2	každý
100	[number]	k4	100
m	m	kA	m
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféru	atmosféra	k1gFnSc4	atmosféra
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
vrstvy	vrstva	k1gFnPc4	vrstva
i	i	k8xC	i
podle	podle	k7c2	podle
jiných	jiný	k2eAgNnPc2d1	jiné
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
vrstvy	vrstva	k1gFnPc1	vrstva
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
mohou	moct	k5eAaImIp3nP	moct
překrývat	překrývat	k5eAaImF	překrývat
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
koncentrace	koncentrace	k1gFnSc2	koncentrace
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
volných	volný	k2eAgInPc2d1	volný
elektronů	elektron	k1gInPc2	elektron
dělíme	dělit	k5eAaImIp1nP	dělit
atmosféru	atmosféra	k1gFnSc4	atmosféra
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
<g/>
:	:	kIx,	:
Neutrosféra	Neutrosféra	k1gFnSc1	Neutrosféra
Sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
po	po	k7c4	po
spodní	spodní	k2eAgFnSc4d1	spodní
vrstvu	vrstva	k1gFnSc4	vrstva
ionosféry	ionosféra	k1gFnSc2	ionosféra
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
asi	asi	k9	asi
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
60	[number]	k4	60
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
získala	získat	k5eAaPmAgFnS	získat
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
převážně	převážně	k6eAd1	převážně
neutrální	neutrální	k2eAgFnSc1d1	neutrální
částice	částice	k1gFnSc1	částice
(	(	kIx(	(
<g/>
atomy	atom	k1gInPc1	atom
a	a	k8xC	a
molekuly	molekula	k1gFnPc1	molekula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ionosféra	ionosféra	k1gFnSc1	ionosféra
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
elektricky	elektricky	k6eAd1	elektricky
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
ionty	ion	k1gInPc1	ion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
leží	ležet	k5eAaImIp3nS	ležet
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
60	[number]	k4	60
km	km	kA	km
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
vrstvy	vrstva	k1gFnPc4	vrstva
F2	F2	k1gFnSc2	F2
(	(	kIx(	(
<g/>
350	[number]	k4	350
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
F1	F1	k1gMnSc1	F1
(	(	kIx(	(
<g/>
180	[number]	k4	180
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
E	E	kA	E
(	(	kIx(	(
<g/>
120	[number]	k4	120
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
D	D	kA	D
(	(	kIx(	(
<g/>
90	[number]	k4	90
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
odraz	odraz	k1gInSc1	odraz
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
složení	složení	k1gNnSc2	složení
<g/>
:	:	kIx,	:
Homosféra	Homosféra	k1gFnSc1	Homosféra
Vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
0	[number]	k4	0
až	až	k9	až
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
převážně	převážně	k6eAd1	převážně
stabilní	stabilní	k2eAgNnSc1d1	stabilní
složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
99	[number]	k4	99
%	%	kIx~	%
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
dohromady	dohromady	k6eAd1	dohromady
<g/>
;	;	kIx,	;
kolísá	kolísat	k5eAaImIp3nS	kolísat
jen	jen	k9	jen
obsah	obsah	k1gInSc4	obsah
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
CO2	CO2	k1gMnPc2	CO2
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
<s>
Heterosféra	Heterosféra	k1gFnSc1	Heterosféra
Vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
90	[number]	k4	90
až	až	k9	až
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
750	[number]	k4	750
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
zde	zde	k6eAd1	zde
podíl	podíl	k1gInSc4	podíl
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
helia	helium	k1gNnSc2	helium
a	a	k8xC	a
lehkých	lehký	k2eAgInPc2d1	lehký
plynů	plyn	k1gInPc2	plyn
vůči	vůči	k7c3	vůči
homosféře	homosféra	k1gFnSc3	homosféra
<g/>
;	;	kIx,	;
nad	nad	k7c4	nad
200	[number]	k4	200
km	km	kA	km
je	být	k5eAaImIp3nS	být
dusíku	dusík	k1gInSc2	dusík
méně	málo	k6eAd2	málo
než	než	k8xS	než
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
atomární	atomární	k2eAgFnSc6d1	atomární
formě	forma	k1gFnSc6	forma
-	-	kIx~	-
tedy	tedy	k9	tedy
O	O	kA	O
a	a	k8xC	a
ne	ne	k9	ne
jako	jako	k9	jako
molekula	molekula	k1gFnSc1	molekula
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Samostatné	samostatný	k2eAgNnSc1d1	samostatné
<g/>
:	:	kIx,	:
Magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
Část	část	k1gFnSc1	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
zemské	zemský	k2eAgFnSc6d1	zemská
magnetické	magnetický	k2eAgFnSc6d1	magnetická
pole	pola	k1gFnSc6	pola
reaguje	reagovat	k5eAaBmIp3nS	reagovat
se	s	k7c7	s
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
nad	nad	k7c4	nad
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
asymetrická	asymetrický	k2eAgFnSc1d1	asymetrická
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
do	do	k7c2	do
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
větší	veliký	k2eAgFnSc2d2	veliký
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
přivrácené	přivrácený	k2eAgFnPc1d1	přivrácená
<g/>
.	.	kIx.	.
</s>
<s>
Ozonová	ozonový	k2eAgFnSc1d1	ozonová
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
též	též	k9	též
ozonosféra	ozonosféra	k1gFnSc1	ozonosféra
<g/>
)	)	kIx)	)
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
od	od	k7c2	od
20	[number]	k4	20
do	do	k7c2	do
40	[number]	k4	40
km	km	kA	km
<g/>
,	,	kIx,	,
koncentrace	koncentrace	k1gFnSc1	koncentrace
ozónu	ozón	k1gInSc2	ozón
vůči	vůči	k7c3	vůči
běžnému	běžný	k2eAgInSc3d1	běžný
dvouatomárnímu	dvouatomární	k2eAgInSc3d1	dvouatomární
kyslíku	kyslík	k1gInSc3	kyslík
O2	O2	k1gFnSc2	O2
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
částech	část	k1gFnPc6	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
tlak	tlak	k1gInSc1	tlak
přímo	přímo	k6eAd1	přímo
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
množství	množství	k1gNnSc1	množství
(	(	kIx(	(
<g/>
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
<g/>
)	)	kIx)	)
vzduchu	vzduch	k1gInSc2	vzduch
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
stejný	stejný	k2eAgInSc4d1	stejný
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tlak	tlak	k1gInSc1	tlak
klesne	klesnout	k5eAaPmIp3nS	klesnout
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
101,3	[number]	k4	101,3
kPa	kPa	k?	kPa
=	=	kIx~	=
1013	[number]	k4	1013
hPa	hPa	k?	hPa
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
atmosféra	atmosféra	k1gFnSc1	atmosféra
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
množství	množství	k1gNnSc1	množství
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
svou	svůj	k3xOyFgFnSc7	svůj
činností	činnost	k1gFnSc7	činnost
významně	významně	k6eAd1	významně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
zemskou	zemský	k2eAgFnSc4d1	zemská
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
několik	několik	k4yIc4	několik
příkladů	příklad	k1gInPc2	příklad
<g/>
:	:	kIx,	:
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
pokles	pokles	k1gInSc4	pokles
množství	množství	k1gNnSc1	množství
ozonu	ozon	k1gInSc2	ozon
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
(	(	kIx(	(
<g/>
ozonová	ozonový	k2eAgFnSc1d1	ozonová
díra	díra	k1gFnSc1	díra
-	-	kIx~	-
nejvíce	hodně	k6eAd3	hodně
působí	působit	k5eAaImIp3nP	působit
freony	freon	k1gInPc1	freon
<g/>
,	,	kIx,	,
proudová	proudový	k2eAgNnPc1d1	proudové
letadla	letadlo	k1gNnPc1	letadlo
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
