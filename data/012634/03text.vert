<p>
<s>
Kulík	kulík	k1gMnSc1	kulík
říční	říční	k2eAgMnSc1d1	říční
(	(	kIx(	(
<g/>
Charadrius	Charadrius	k1gMnSc1	Charadrius
dubius	dubius	k1gMnSc1	dubius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
druh	druh	k1gInSc1	druh
bahňáka	bahňák	k1gMnSc2	bahňák
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
kulíkovitých	kulíkovitý	k2eAgFnPc2d1	kulíkovitý
(	(	kIx(	(
<g/>
Charadriidae	Charadriidae	k1gFnPc2	Charadriidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
42	[number]	k4	42
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
25-55	[number]	k4	25-55
g.	g.	k?	g.
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
hnědý	hnědý	k2eAgInSc4d1	hnědý
hřbet	hřbet	k1gInSc4	hřbet
a	a	k8xC	a
temeno	temeno	k1gNnSc4	temeno
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgFnPc4d1	tmavá
letky	letka	k1gFnPc4	letka
<g/>
,	,	kIx,	,
světlou	světlý	k2eAgFnSc4d1	světlá
spodinu	spodina	k1gFnSc4	spodina
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
pruh	pruh	k1gInSc1	pruh
na	na	k7c6	na
krku	krk	k1gInSc6	krk
a	a	k8xC	a
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
je	být	k5eAaImIp3nS	být
též	též	k9	též
žlutý	žlutý	k2eAgInSc1d1	žlutý
kroužek	kroužek	k1gInSc1	kroužek
kolem	kolem	k7c2	kolem
tmavého	tmavý	k2eAgNnSc2d1	tmavé
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Končetiny	končetina	k1gFnPc4	končetina
má	mít	k5eAaImIp3nS	mít
růžovo-hnědé	růžovonědý	k2eAgNnSc1d1	růžovo-hnědý
a	a	k8xC	a
krátký	krátký	k2eAgInSc1d1	krátký
zobák	zobák	k1gInSc1	zobák
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
se	s	k7c7	s
zbarvením	zbarvení	k1gNnSc7	zbarvení
nijak	nijak	k6eAd1	nijak
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
celkově	celkově	k6eAd1	celkově
světlejší	světlý	k2eAgInSc1d2	světlejší
s	s	k7c7	s
vpředu	vpředu	k6eAd1	vpředu
přerušeným	přerušený	k2eAgInSc7d1	přerušený
pruhem	pruh	k1gInSc7	pruh
na	na	k7c6	na
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
porovnáním	porovnání	k1gNnSc7	porovnání
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
podobným	podobný	k2eAgMnSc7d1	podobný
kulíkem	kulík	k1gMnSc7	kulík
písečným	písečný	k2eAgNnSc7d1	písečné
(	(	kIx(	(
<g/>
Charadrius	Charadrius	k1gInSc1	Charadrius
hiaticula	hiaticulum	k1gNnSc2	hiaticulum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kulík	kulík	k1gMnSc1	kulík
říční	říční	k2eAgMnSc1d1	říční
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgInSc1d2	menší
a	a	k8xC	a
štíhlejší	štíhlý	k2eAgInSc1d2	štíhlejší
<g/>
.	.	kIx.	.
</s>
<s>
Kulík	kulík	k1gMnSc1	kulík
písečný	písečný	k2eAgMnSc1d1	písečný
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
liší	lišit	k5eAaImIp3nP	lišit
také	také	k9	také
převážně	převážně	k6eAd1	převážně
oranžovým	oranžový	k2eAgInSc7d1	oranžový
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
výrazným	výrazný	k2eAgInSc7d1	výrazný
bílým	bílý	k2eAgInSc7d1	bílý
pruhem	pruh	k1gInSc7	pruh
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
patrným	patrný	k2eAgInSc7d1	patrný
v	v	k7c6	v
letu	let	k1gInSc6	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
klesavým	klesavý	k2eAgInSc7d1	klesavý
"	"	kIx"	"
<g/>
piu	piu	k?	piu
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
svižným	svižný	k2eAgInSc7d1	svižný
"	"	kIx"	"
<g/>
pri	pri	k?	pri
<g/>
"	"	kIx"	"
(	(	kIx(	(
nahrávka	nahrávka	k1gFnSc1	nahrávka
s	s	k7c7	s
hlasem	hlas	k1gInSc7	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
severních	severní	k2eAgFnPc2d1	severní
oblastí	oblast	k1gFnPc2	oblast
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
února	únor	k1gInSc2	únor
táhne	táhnout	k5eAaImIp3nS	táhnout
na	na	k7c4	na
zimoviště	zimoviště	k1gNnSc4	zimoviště
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
jak	jak	k8xC	jak
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
severní	severní	k2eAgFnSc2d1	severní
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
subsaharské	subsaharský	k2eAgFnPc1d1	subsaharská
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pobřeží	pobřeží	k1gNnSc2	pobřeží
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Zaire	Zair	k1gMnSc5	Zair
<g/>
,	,	kIx,	,
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
a	a	k8xC	a
Keni	Keňa	k1gFnSc2	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
Zpět	zpět	k6eAd1	zpět
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
již	již	k9	již
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
<g/>
Žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
bahnitých	bahnitý	k2eAgInPc6d1	bahnitý
<g/>
,	,	kIx,	,
písečných	písečný	k2eAgInPc6d1	písečný
i	i	k8xC	i
štěrkovitých	štěrkovitý	k2eAgInPc6d1	štěrkovitý
březích	břeh	k1gInPc6	břeh
mělkých	mělký	k2eAgFnPc2d1	mělká
vod	voda	k1gFnPc2	voda
(	(	kIx(	(
<g/>
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
pískoven	pískovna	k1gFnPc2	pískovna
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
až	až	k9	až
po	po	k7c4	po
850	[number]	k4	850
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
rychle	rychle	k6eAd1	rychle
cupitá	cupitat	k5eAaImIp3nS	cupitat
<g/>
,	,	kIx,	,
náhle	náhle	k6eAd1	náhle
se	se	k3xPyFc4	se
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
je	být	k5eAaImIp3nS	být
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
přímý	přímý	k2eAgInSc1d1	přímý
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hnízdění	hnízdění	k1gNnSc2	hnízdění
je	být	k5eAaImIp3nS	být
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
<g/>
,	,	kIx,	,
při	při	k7c6	při
tazích	tag	k1gInPc6	tag
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
podniká	podnikat	k5eAaImIp3nS	podnikat
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
často	často	k6eAd1	často
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
hejnech	hejno	k1gNnPc6	hejno
<g/>
.	.	kIx.	.
<g/>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
především	především	k9	především
červy	červ	k1gMnPc7	červ
<g/>
,	,	kIx,	,
pavouky	pavouk	k1gMnPc7	pavouk
<g/>
,	,	kIx,	,
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc7	jeho
larvami	larva	k1gFnPc7	larva
a	a	k8xC	a
měkkýši	měkkýš	k1gMnPc1	měkkýš
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
často	často	k6eAd1	často
sbírá	sbírat	k5eAaImIp3nS	sbírat
i	i	k9	i
z	z	k7c2	z
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
1	[number]	k4	1
<g/>
×	×	k?	×
až	až	k9	až
2	[number]	k4	2
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
důlek	důlek	k1gInSc4	důlek
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
otevřeném	otevřený	k2eAgInSc6d1	otevřený
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
vegetaci	vegetace	k1gFnSc6	vegetace
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
nedaleko	nedaleko	k7c2	nedaleko
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
námluv	námluva	k1gFnPc2	námluva
samec	samec	k1gMnSc1	samec
hloubí	hloubí	k1gNnSc1	hloubí
hnízd	hnízdo	k1gNnPc2	hnízdo
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
samice	samice	k1gFnPc1	samice
sama	sám	k3xTgFnSc1	sám
jedno	jeden	k4xCgNnSc1	jeden
vybírá	vybírat	k5eAaImIp3nS	vybírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
snůšce	snůška	k1gFnSc6	snůška
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
4	[number]	k4	4
29,8	[number]	k4	29,8
x	x	k?	x
22,2	[number]	k4	22,2
mm	mm	kA	mm
velká	velký	k2eAgNnPc4d1	velké
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc4	jejich
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
bývá	bývat	k5eAaImIp3nS	bývat
bílé	bílý	k2eAgInPc1d1	bílý
až	až	k6eAd1	až
šedé	šedý	k2eAgInPc1d1	šedý
s	s	k7c7	s
drobným	drobný	k2eAgNnSc7d1	drobné
tmavým	tmavý	k2eAgNnSc7d1	tmavé
skvrněním	skvrnění	k1gNnSc7	skvrnění
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
maskovací	maskovací	k2eAgInSc4d1	maskovací
účel	účel	k1gInSc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
24-25	[number]	k4	24-25
dnů	den	k1gInPc2	den
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
cítí	cítit	k5eAaImIp3nP	cítit
ohrožena	ohrozit	k5eAaPmNgNnP	ohrozit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tisknou	tisknout	k5eAaImIp3nP	tisknout
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
spoléhají	spoléhat	k5eAaImIp3nP	spoléhat
tak	tak	k9	tak
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
ochranné	ochranný	k2eAgNnSc4d1	ochranné
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
ohrožení	ohrožení	k1gNnSc6	ohrožení
také	také	k9	také
často	často	k6eAd1	často
předstírá	předstírat	k5eAaImIp3nS	předstírat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
zranění	zranění	k1gNnSc2	zranění
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
predátora	predátor	k1gMnSc4	predátor
odlákat	odlákat	k5eAaPmF	odlákat
do	do	k7c2	do
bezpečné	bezpečný	k2eAgFnSc2d1	bezpečná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
průměrně	průměrně	k6eAd1	průměrně
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Početnost	početnost	k1gFnSc4	početnost
==	==	k?	==
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
druhu	druh	k1gInSc2	druh
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
110-240	[number]	k4	110-240
tisíc	tisíc	k4xCgInPc2	tisíc
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počtu	počet	k1gInSc6	počet
větším	veliký	k2eAgInSc6d2	veliký
jak	jak	k8xS	jak
5	[number]	k4	5
000	[number]	k4	000
párů	pár	k1gInPc2	pár
přitom	přitom	k6eAd1	přitom
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
území	území	k1gNnSc6	území
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
středoevropská	středoevropský	k2eAgFnSc1d1	středoevropská
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
13	[number]	k4	13
000-19	[number]	k4	000-19
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
na	na	k7c6	na
území	území	k1gNnSc2	území
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
1400	[number]	k4	1400
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
populační	populační	k2eAgInSc1d1	populační
trend	trend	k1gInSc1	trend
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
stabilní	stabilní	k2eAgMnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Charadrius	Charadrius	k1gMnSc1	Charadrius
dubius	dubius	k1gMnSc1	dubius
curonicus	curonicus	k1gMnSc1	curonicus
Gmelin	Gmelina	k1gFnPc2	Gmelina
<g/>
,	,	kIx,	,
1789	[number]	k4	1789
-	-	kIx~	-
většina	většina	k1gFnSc1	většina
Eurasie	Eurasie	k1gFnSc1	Eurasie
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Charadrius	Charadrius	k1gInSc1	Charadrius
dubius	dubius	k1gInSc1	dubius
jerdoni	jerdoň	k1gFnSc3	jerdoň
(	(	kIx(	(
<g/>
Legge	Legge	k1gNnSc1	Legge
<g/>
,	,	kIx,	,
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
-	-	kIx~	-
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Charadrius	Charadrius	k1gMnSc1	Charadrius
dubius	dubius	k1gMnSc1	dubius
dubius	dubius	k1gMnSc1	dubius
Scopoli	Scopole	k1gFnSc4	Scopole
<g/>
,	,	kIx,	,
1786	[number]	k4	1786
-	-	kIx~	-
Filipíny	Filipíny	k1gFnPc4	Filipíny
jižně	jižně	k6eAd1	jižně
po	po	k7c4	po
Novou	nový	k2eAgFnSc4d1	nová
Guineu	Guinea	k1gFnSc4	Guinea
a	a	k8xC	a
Bismarckovo	Bismarckův	k2eAgNnSc4d1	Bismarckovo
souostroví	souostroví	k1gNnSc4	souostroví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kulík	kulík	k1gMnSc1	kulík
říční	říční	k2eAgMnSc1d1	říční
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
kulík	kulík	k1gMnSc1	kulík
říční	říční	k2eAgMnSc1d1	říční
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Charadrius	Charadrius	k1gInSc1	Charadrius
dubius	dubius	k1gInSc1	dubius
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
