<s>
Topologie	topologie	k1gFnSc1	topologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
topos	topos	k1gInSc1	topos
-	-	kIx~	-
místo	místo	k1gNnSc1	místo
a	a	k8xC	a
logos	logos	k1gInSc1	logos
-	-	kIx~	-
studie	studie	k1gFnSc1	studie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
opírající	opírající	k2eAgFnPc1d1	opírající
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
obecný	obecný	k2eAgInSc4d1	obecný
výklad	výklad	k1gInSc4	výklad
pojmu	pojem	k1gInSc2	pojem
prostor	prostor	k1gInSc1	prostor
(	(	kIx(	(
<g/>
topologický	topologický	k2eAgInSc1d1	topologický
prostor	prostor	k1gInSc1	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
