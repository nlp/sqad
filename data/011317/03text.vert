<p>
<s>
Topologie	topologie	k1gFnSc1	topologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
topos	topos	k1gInSc1	topos
-	-	kIx~	-
místo	místo	k1gNnSc1	místo
a	a	k8xC	a
logos	logos	k1gInSc1	logos
-	-	kIx~	-
studie	studie	k1gFnSc1	studie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
opírající	opírající	k2eAgFnPc1d1	opírající
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
obecný	obecný	k2eAgInSc4d1	obecný
výklad	výklad	k1gInSc4	výklad
pojmu	pojem	k1gInSc2	pojem
prostor	prostor	k1gInSc1	prostor
(	(	kIx(	(
<g/>
topologický	topologický	k2eAgInSc1d1	topologický
prostor	prostor	k1gInSc1	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studuje	studovat	k5eAaImIp3nS	studovat
takové	takový	k3xDgFnPc4	takový
vlastnosti	vlastnost	k1gFnPc4	vlastnost
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nP	měnit
při	při	k7c6	při
oboustranně	oboustranně	k6eAd1	oboustranně
spojitých	spojitý	k2eAgFnPc6d1	spojitá
transformacích	transformace	k1gFnPc6	transformace
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
blízké	blízký	k2eAgFnSc6d1	blízká
<g/>
"	"	kIx"	"
body	bod	k1gInPc1	bod
se	se	k3xPyFc4	se
transformují	transformovat	k5eAaBmIp3nP	transformovat
opět	opět	k6eAd1	opět
v	v	k7c6	v
"	"	kIx"	"
<g/>
blízké	blízký	k2eAgFnSc6d1	blízká
<g/>
"	"	kIx"	"
body	bod	k1gInPc1	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
topologii	topologie	k1gFnSc6	topologie
nezáleží	záležet	k5eNaImIp3nS	záležet
na	na	k7c6	na
geometrických	geometrický	k2eAgFnPc6d1	geometrická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
<g/>
,	,	kIx,	,
závislých	závislý	k2eAgInPc2d1	závislý
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
křivosti	křivost	k1gFnSc6	křivost
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
topologie	topologie	k1gFnSc2	topologie
lze	lze	k6eAd1	lze
například	například	k6eAd1	například
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
považovat	považovat	k5eAaImF	považovat
čtverec	čtverec	k1gInSc4	čtverec
a	a	k8xC	a
kruh	kruh	k1gInSc4	kruh
za	za	k7c4	za
rovnocenné	rovnocenný	k2eAgNnSc4d1	rovnocenné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úsečku	úsečka	k1gFnSc4	úsečka
a	a	k8xC	a
kružnici	kružnice	k1gFnSc4	kružnice
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
kterými	který	k3yQgNnPc7	který
topologie	topologie	k1gFnSc1	topologie
studuje	studovat	k5eAaImIp3nS	studovat
topologické	topologický	k2eAgInPc4d1	topologický
útvary	útvar	k1gInPc4	útvar
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
topologie	topologie	k1gFnSc1	topologie
algebraická	algebraický	k2eAgFnSc1d1	algebraická
(	(	kIx(	(
<g/>
též	též	k9	též
kombinatorická	kombinatorický	k2eAgFnSc1d1	kombinatorická
<g/>
)	)	kIx)	)
a	a	k8xC	a
topologie	topologie	k1gFnSc1	topologie
množinová	množinový	k2eAgFnSc1d1	množinová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
vědě	věda	k1gFnSc6	věda
jménem	jméno	k1gNnSc7	jméno
topologie	topologie	k1gFnSc2	topologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
topologické	topologický	k2eAgInPc4d1	topologický
prostory	prostor	k1gInPc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Pojmem	pojem	k1gInSc7	pojem
topologie	topologie	k1gFnSc2	topologie
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
označuje	označovat	k5eAaImIp3nS	označovat
topologická	topologický	k2eAgFnSc1d1	topologická
struktura	struktura	k1gFnSc1	struktura
množiny	množina	k1gFnSc2	množina
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc2	tau
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
topologický	topologický	k2eAgInSc1d1	topologický
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
tau	tau	k1gNnPc3	tau
}	}	kIx)	}
</s>
</p>
<p>
<s>
nazývá	nazývat	k5eAaImIp3nS	nazývat
topologie	topologie	k1gFnSc1	topologie
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Topologie	topologie	k1gFnSc1	topologie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
důsledek	důsledek	k1gInSc4	důsledek
zkoumání	zkoumání	k1gNnSc2	zkoumání
některých	některý	k3yIgInPc2	některý
problémů	problém	k1gInPc2	problém
v	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
Leonarda	Leonardo	k1gMnSc2	Leonardo
Eulera	Euler	k1gMnSc2	Euler
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
popsán	popsán	k2eAgInSc1d1	popsán
problém	problém	k1gInSc1	problém
sedmi	sedm	k4xCc2	sedm
mostů	most	k1gInPc2	most
v	v	k7c6	v
Königsbergu	Königsberg	k1gInSc6	Königsberg
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
první	první	k4xOgInSc4	první
topologický	topologický	k2eAgInSc4d1	topologický
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
topologie	topologie	k1gFnPc1	topologie
<g/>
"	"	kIx"	"
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
ho	on	k3xPp3gMnSc4	on
Johann	Johann	k1gMnSc1	Johann
Benedict	Benedict	k1gMnSc1	Benedict
Listing	Listing	k1gInSc4	Listing
použil	použít	k5eAaPmAgMnS	použít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
Vorstudien	Vorstudien	k1gInSc4	Vorstudien
zur	zur	k?	zur
Topologie	topologie	k1gFnSc2	topologie
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jej	on	k3xPp3gNnSc4	on
již	již	k9	již
10	[number]	k4	10
let	léto	k1gNnPc2	léto
používal	používat	k5eAaImAgMnS	používat
v	v	k7c6	v
korespondenci	korespondence	k1gFnSc6	korespondence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
topologie	topologie	k1gFnSc1	topologie
nicméně	nicméně	k8xC	nicméně
nestaví	stavit	k5eNaImIp3nS	stavit
na	na	k7c4	na
geometrii	geometrie	k1gFnSc4	geometrie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teorii	teorie	k1gFnSc4	teorie
množin	množina	k1gFnPc2	množina
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
Georgem	Georg	k1gMnSc7	Georg
Cantorem	Cantor	k1gMnSc7	Cantor
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Cantor	Cantor	k1gInSc1	Cantor
totiž	totiž	k9	totiž
kromě	kromě	k7c2	kromě
základních	základní	k2eAgInPc2d1	základní
pojmů	pojem	k1gInPc2	pojem
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
i	i	k9	i
množiny	množina	k1gFnSc2	množina
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
euklidovských	euklidovský	k2eAgInPc6d1	euklidovský
prostorech	prostor	k1gInPc6	prostor
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
svého	svůj	k3xOyFgInSc2	svůj
výzkumu	výzkum	k1gInSc2	výzkum
Fourierových	Fourierův	k2eAgFnPc2d1	Fourierova
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Henri	Henr	k1gFnPc1	Henr
Poincaré	Poincarý	k2eAgFnPc1d1	Poincarý
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
publikaci	publikace	k1gFnSc6	publikace
Analysis	Analysis	k1gFnSc4	Analysis
Situs	Situs	k1gMnSc1	Situs
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
zavedl	zavést	k5eAaPmAgInS	zavést
pojmy	pojem	k1gInPc4	pojem
homotopie	homotopie	k1gFnSc2	homotopie
a	a	k8xC	a
spojitá	spojitý	k2eAgFnSc1d1	spojitá
deformace	deformace	k1gFnSc1	deformace
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc1	základ
algebraické	algebraický	k2eAgFnSc2d1	algebraická
topologie	topologie	k1gFnSc2	topologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
zavedl	zavést	k5eAaPmAgInS	zavést
Maurice	Maurika	k1gFnSc3	Maurika
Fréchet	Fréchet	k1gInSc4	Fréchet
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
sjednotit	sjednotit	k5eAaPmF	sjednotit
práce	práce	k1gFnPc4	práce
Cantora	Cantor	k1gMnSc2	Cantor
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
pojem	pojem	k1gInSc1	pojem
metrický	metrický	k2eAgInSc4d1	metrický
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Metrické	metrický	k2eAgFnPc1d1	metrická
prostory	prostora	k1gFnPc1	prostora
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
speciální	speciální	k2eAgInSc4d1	speciální
případ	případ	k1gInSc4	případ
topologických	topologický	k2eAgInPc2d1	topologický
prostorů	prostor	k1gInPc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
Felix	Felix	k1gMnSc1	Felix
Hausdorff	Hausdorff	k1gMnSc1	Hausdorff
zavedl	zavést	k5eAaPmAgMnS	zavést
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
topologický	topologický	k2eAgInSc4d1	topologický
prostor	prostor	k1gInSc4	prostor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
tím	ten	k3xDgNnSc7	ten
však	však	k9	však
nazýval	nazývat	k5eAaImAgMnS	nazývat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nazývá	nazývat	k5eAaImIp3nS	nazývat
Hausdorffův	Hausdorffův	k2eAgInSc4d1	Hausdorffův
prostor	prostor	k1gInSc4	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
za	za	k7c4	za
"	"	kIx"	"
<g/>
topologické	topologický	k2eAgFnPc4d1	topologická
prostory	prostora	k1gFnPc4	prostora
<g/>
"	"	kIx"	"
považují	považovat	k5eAaImIp3nP	považovat
zobecnění	zobecnění	k1gNnSc4	zobecnění
Hausdorffových	Hausdorffův	k2eAgInPc2d1	Hausdorffův
prostorů	prostor	k1gInPc2	prostor
definované	definovaný	k2eAgNnSc1d1	definované
Kazimierzem	Kazimierz	k1gInSc7	Kazimierz
Kuratowskim	Kuratowskim	k1gInSc4	Kuratowskim
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úvod	úvod	k1gInSc1	úvod
==	==	k?	==
</s>
</p>
<p>
<s>
Topologické	topologický	k2eAgFnPc1d1	topologická
prostory	prostora	k1gFnPc1	prostora
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
odvětví	odvětví	k1gNnSc2	odvětví
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
topologie	topologie	k1gFnSc1	topologie
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
sjednocujících	sjednocující	k2eAgFnPc2d1	sjednocující
disciplín	disciplína	k1gFnPc2	disciplína
matematiky	matematika	k1gFnSc2	matematika
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
teorie	teorie	k1gFnSc1	teorie
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
topologie	topologie	k1gFnSc1	topologie
studuje	studovat	k5eAaImIp3nS	studovat
některé	některý	k3yIgFnPc4	některý
vlastnosti	vlastnost	k1gFnPc4	vlastnost
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
souvislost	souvislost	k1gFnSc1	souvislost
<g/>
,	,	kIx,	,
kompaktnost	kompaktnost	k1gFnSc1	kompaktnost
a	a	k8xC	a
spojitost	spojitost	k1gFnSc1	spojitost
<g/>
.	.	kIx.	.
</s>
<s>
Algebraická	algebraický	k2eAgFnSc1d1	algebraická
topologie	topologie	k1gFnSc1	topologie
potom	potom	k6eAd1	potom
využívá	využívat	k5eAaImIp3nS	využívat
algebru	algebra	k1gFnSc4	algebra
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
grupy	grupa	k1gFnPc1	grupa
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
topologických	topologický	k2eAgInPc2d1	topologický
prostorů	prostor	k1gInPc2	prostor
a	a	k8xC	a
zobrazení	zobrazení	k1gNnSc2	zobrazení
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Motivací	motivace	k1gFnSc7	motivace
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
geometrických	geometrický	k2eAgInPc2d1	geometrický
problémů	problém	k1gInPc2	problém
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
přesném	přesný	k2eAgInSc6d1	přesný
tvaru	tvar	k1gInSc6	tvar
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
na	na	k7c6	na
vztazích	vztah	k1gInPc6	vztah
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
objekty	objekt	k1gInPc4	objekt
mají	mít	k5eAaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
kružnice	kružnice	k1gFnSc2	kružnice
a	a	k8xC	a
čtverec	čtverec	k1gInSc4	čtverec
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgFnPc4	některý
společné	společný	k2eAgFnPc4d1	společná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jednodimenzionální	jednodimenzionální	k2eAgInPc1d1	jednodimenzionální
objekty	objekt	k1gInPc1	objekt
(	(	kIx(	(
<g/>
z	z	k7c2	z
topologického	topologický	k2eAgInSc2d1	topologický
pohledu	pohled	k1gInSc2	pohled
<g/>
)	)	kIx)	)
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nP	dělit
plochu	plocha	k1gFnSc4	plocha
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
topologických	topologický	k2eAgInPc2d1	topologický
článků	článek	k1gInPc2	článek
napsal	napsat	k5eAaPmAgMnS	napsat
Leonhard	Leonhard	k1gMnSc1	Leonhard
Euler	Euler	k1gMnSc1	Euler
<g/>
.	.	kIx.	.
</s>
<s>
Ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
Königsbergu	Königsberg	k1gInSc6	Königsberg
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
procházela	procházet	k5eAaImAgFnS	procházet
přes	přes	k7c4	přes
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
tamních	tamní	k2eAgInPc2d1	tamní
sedmi	sedm	k4xCc2	sedm
mostů	most	k1gInPc2	most
právě	právě	k6eAd1	právě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Důkaz	důkaz	k1gInSc1	důkaz
byl	být	k5eAaImAgInS	být
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
na	na	k7c6	na
délce	délka	k1gFnSc6	délka
mostů	most	k1gInPc2	most
a	a	k8xC	a
na	na	k7c6	na
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ostrovy	ostrov	k1gInPc1	ostrov
mosty	most	k1gInPc4	most
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Zobecnění	zobecnění	k1gNnSc1	zobecnění
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
dalo	dát	k5eAaPmAgNnS	dát
základ	základ	k1gInSc4	základ
dalšímu	další	k2eAgNnSc3d1	další
odvětví	odvětví	k1gNnSc3	odvětví
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc3	teorie
grafů	graf	k1gInPc2	graf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
chceme	chtít	k5eAaImIp1nP	chtít
abstrahovat	abstrahovat	k5eAaBmF	abstrahovat
od	od	k7c2	od
přesných	přesný	k2eAgFnPc2d1	přesná
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
<g/>
,	,	kIx,	,
nutně	nutně	k6eAd1	nutně
musíme	muset	k5eAaImIp1nP	muset
najít	najít	k5eAaPmF	najít
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
je	být	k5eAaImIp3nS	být
řešení	řešení	k1gNnSc1	řešení
problému	problém	k1gInSc2	problém
závislé	závislý	k2eAgNnSc1d1	závislé
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
dojdeme	dojít	k5eAaPmIp1nP	dojít
k	k	k7c3	k
pojmu	pojem	k1gInSc3	pojem
topologicky	topologicky	k6eAd1	topologicky
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Intuitivně	intuitivně	k6eAd1	intuitivně
řečeno	říct	k5eAaPmNgNnS	říct
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
prostory	prostor	k1gInPc4	prostor
topologicky	topologicky	k6eAd1	topologicky
ekvivalentní	ekvivalentní	k2eAgInPc4d1	ekvivalentní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jeden	jeden	k4xCgInSc1	jeden
deformován	deformovat	k5eAaImNgInS	deformovat
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
roztrhl	roztrhnout	k5eAaPmAgMnS	roztrhnout
nebo	nebo	k8xC	nebo
spojil	spojit	k5eAaPmAgMnS	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Obrázek	obrázek	k1gInSc1	obrázek
vpravo	vpravo	k6eAd1	vpravo
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
lze	lze	k6eAd1	lze
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
deformovat	deformovat	k5eAaImF	deformovat
hrníček	hrníček	k1gInSc4	hrníček
na	na	k7c4	na
pneumatiku	pneumatika	k1gFnSc4	pneumatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgInPc1d1	hlavní
výsledky	výsledek	k1gInPc1	výsledek
obecné	obecný	k2eAgFnSc2d1	obecná
topologie	topologie	k1gFnSc2	topologie
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
konečný	konečný	k2eAgInSc1d1	konečný
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
interval	interval	k1gInSc1	interval
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
kompaktní	kompaktní	k2eAgNnSc1d1	kompaktní
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
podprostor	podprostor	k1gMnSc1	podprostor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
kompaktní	kompaktní	k2eAgNnSc1d1	kompaktní
<g/>
,	,	kIx,	,
právě	právě	k9	právě
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
a	a	k8xC	a
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojitý	spojitý	k2eAgInSc1d1	spojitý
obraz	obraz	k1gInSc1	obraz
kompaktního	kompaktní	k2eAgInSc2d1	kompaktní
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tichonovova	Tichonovův	k2eAgFnSc1d1	Tichonovův
věta	věta	k1gFnSc1	věta
<g/>
:	:	kIx,	:
Jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
součin	součin	k1gInSc1	součin
kompaktních	kompaktní	k2eAgInPc2d1	kompaktní
prostorů	prostor	k1gInPc2	prostor
je	být	k5eAaImIp3nS	být
kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
otevřeně	otevřeně	k6eAd1	otevřeně
množiny	množina	k1gFnPc1	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
<s>
⊂	⊂	k?	⊂
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U	u	k7c2	u
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
⊂	⊂	k?	⊂
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
homeomorfní	homeomorfní	k2eAgFnSc1d1	homeomorfní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
m	m	kA	m
(	(	kIx(	(
<g/>
t.j.	t.j.	k?	t.j.
dimenze	dimenze	k1gFnSc2	dimenze
je	být	k5eAaImIp3nS	být
topologický	topologický	k2eAgInSc1d1	topologický
pojem	pojem	k1gInSc1	pojem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kompaktní	kompaktní	k2eAgMnSc1d1	kompaktní
podprostor	podprostor	k1gMnSc1	podprostor
Hausdorffova	Hausdorffův	k2eAgInSc2d1	Hausdorffův
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MUNKRES	MUNKRES	kA	MUNKRES
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
R.	R.	kA	R.
Topology	Topolog	k1gMnPc4	Topolog
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Prentice	Prentice	k1gFnSc1	Prentice
hall	halla	k1gFnPc2	halla
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
181629	[number]	k4	181629
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PULTR	PULTR	kA	PULTR
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
topologie	topologie	k1gFnSc2	topologie
a	a	k8xC	a
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Algebraická	algebraický	k2eAgFnSc1d1	algebraická
topologie	topologie	k1gFnSc1	topologie
</s>
</p>
<p>
<s>
Geometrie	geometrie	k1gFnSc1	geometrie
</s>
</p>
<p>
<s>
Topologický	topologický	k2eAgInSc1d1	topologický
prostor	prostor	k1gInSc1	prostor
</s>
</p>
<p>
<s>
Topologické	topologický	k2eAgNnSc1d1	topologické
zobrazení	zobrazení	k1gNnSc1	zobrazení
</s>
</p>
<p>
<s>
Spojité	spojitý	k2eAgNnSc1d1	spojité
zobrazení	zobrazení	k1gNnSc1	zobrazení
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
topologie	topologie	k1gFnSc2	topologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
topologie	topologie	k1gFnSc2	topologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
