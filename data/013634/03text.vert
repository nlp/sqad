<s>
ACID	ACID	kA
</s>
<s>
ACID	ACID	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
z	z	k7c2
oboru	obor	k1gInSc2
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
z	z	k7c2
oblasti	oblast	k1gFnSc2
databází	databáze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
první	první	k4xOgNnPc4
písmena	písmeno	k1gNnPc4
čtyř	čtyři	k4xCgNnPc2
anglických	anglický	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
vystihujících	vystihující	k2eAgNnPc2d1
čtyři	čtyři	k4xCgFnPc4
žádoucí	žádoucí	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
databázových	databázový	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
atomicita	atomicita	k1gFnSc1
(	(	kIx(
<g/>
atomicity	atomicita	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
konzistence	konzistence	k1gFnSc1
(	(	kIx(
<g/>
consistency	consistency	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
izolovanost	izolovanost	k1gFnSc1
(	(	kIx(
<g/>
isolation	isolation	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
trvalost	trvalost	k1gFnSc1
(	(	kIx(
<g/>
durability	durabilita	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Zkratku	zkratka	k1gFnSc4
zavedli	zavést	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
Andreas	Andreas	k1gMnSc1
Reuter	Reuter	k1gMnSc1
a	a	k8xC
Theo	Thea	k1gFnSc5
Härder	Härder	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
článku	článek	k1gInSc6
Principles	Principles	k1gMnSc1
of	of	k?
transaction-oriented	transaction-oriented	k1gMnSc1
database	database	k6eAd1
recovery	recovera	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vycházeli	vycházet	k5eAaImAgMnP
přitom	přitom	k6eAd1
ze	z	k7c2
starší	starý	k2eAgFnSc2d2
práce	práce	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
Jim	on	k3xPp3gMnPc3
Gray	Graum	k1gNnPc7
zavedl	zavést	k5eAaPmAgInS
pouze	pouze	k6eAd1
atomicitu	atomicita	k1gFnSc4
<g/>
,	,	kIx,
konzistenci	konzistence	k1gFnSc4
a	a	k8xC
trvalost	trvalost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Atomicita	Atomicita	k1gFnSc1
</s>
<s>
Atomicita	Atomicita	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
u	u	k7c2
databázových	databázový	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
totéž	týž	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k8xS
atomicita	atomicita	k1gFnSc1
v	v	k7c6
informatice	informatika	k1gFnSc6
obecně	obecně	k6eAd1
<g/>
,	,	kIx,
totiž	totiž	k9
že	že	k8xS
se	se	k3xPyFc4
operace	operace	k1gFnSc1
provede	provést	k5eAaPmIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
uživatele	uživatel	k1gMnSc2
buď	buď	k8xC
celá	celý	k2eAgFnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
vůbec	vůbec	k9
–	–	k?
tedy	tedy	k8xC
výsledkem	výsledek	k1gInSc7
nebude	být	k5eNaImBp3nS
žádný	žádný	k3yNgMnSc1
nedefinovaný	definovaný	k2eNgMnSc1d1
mezistav	mezistat	k5eAaPmDgMnS
<g/>
.	.	kIx.
</s>
<s>
Konzistence	konzistence	k1gFnSc1
</s>
<s>
Konzistencí	konzistence	k1gFnSc7
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
požadavek	požadavek	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
transakce	transakce	k1gFnSc1
nesmí	smět	k5eNaImIp3nS
narušit	narušit	k5eAaPmF
databázovou	databázový	k2eAgFnSc4d1
integritu	integrita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nevylučuje	vylučovat	k5eNaImIp3nS
ztrátu	ztráta	k1gFnSc4
konzistence	konzistence	k1gFnSc2
na	na	k7c6
vyšší	vysoký	k2eAgFnSc6d2
<g/>
,	,	kIx,
aplikační	aplikační	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
požadavek	požadavek	k1gInSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
pouze	pouze	k6eAd1
zachování	zachování	k1gNnSc4
podmínek	podmínka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
definovány	definovat	k5eAaBmNgFnP
na	na	k7c6
databázové	databázový	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Izolace	izolace	k1gFnSc1
</s>
<s>
Požadavek	požadavek	k1gInSc1
izolace	izolace	k1gFnSc2
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vícero	vícero	k1gNnSc1
současně	současně	k6eAd1
probíhajících	probíhající	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
nesmí	smět	k5eNaImIp3nS
ovlivnit	ovlivnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Trvalost	trvalost	k1gFnSc1
</s>
<s>
Trvalost	trvalost	k1gFnSc1
transakce	transakce	k1gFnSc2
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jakmile	jakmile	k8xS
je	být	k5eAaImIp3nS
již	již	k9
transakce	transakce	k1gFnSc1
dokončena	dokončen	k2eAgFnSc1d1
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
zaznamenána	zaznamenat	k5eAaPmNgFnS
trvalým	trvalý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
takže	takže	k8xS
ji	on	k3xPp3gFnSc4
nevymaže	vymazat	k5eNaPmIp3nS
například	například	k6eAd1
následný	následný	k2eAgInSc4d1
výpadek	výpadek	k1gInSc4
napájení	napájení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
ACID	ACID	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
REUTER	REUTER	kA
<g/>
,	,	kIx,
Adreas	Adreas	k1gMnSc1
<g/>
;	;	kIx,
HÄRDER	HÄRDER	kA
<g/>
,	,	kIx,
Theo	Thea	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Principles	Principles	k1gMnSc1
of	of	k?
transaction-oriented	transaction-oriented	k1gMnSc1
database	database	k6eAd1
recovery	recovera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ACM	ACM	kA
Computing	Computing	k1gInSc1
Surveys	Surveys	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1983	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GRAY	GRAY	kA
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Transaction	Transaction	k1gInSc4
Concept	Concept	k1gInSc1
<g/>
:	:	kIx,
Virtues	Virtues	k1gInSc1
and	and	k?
Limitations	Limitations	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
7	#num#	k4
<g/>
th	th	k?
International	International	k1gFnSc2
Conference	Conference	k1gFnSc2
on	on	k3xPp3gMnSc1
Very	Ver	k1gMnPc7
Large	Large	k1gFnSc4
Databases	Databasesa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
