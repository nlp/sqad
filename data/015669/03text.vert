<s>
Detektivka	detektivka	k1gFnSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Detektívka	Detektívka	k1gFnSc1
<g/>
,	,	kIx,
album	album	k1gNnSc1
poprockové	poprockový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Elán	elán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Detektivka	detektivka	k1gFnSc1
nebo	nebo	k8xC
detektivní	detektivní	k2eAgFnSc1d1
fikce	fikce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
detektivní	detektivní	k2eAgInSc4d1
román	román	k1gInSc4
<g/>
,	,	kIx,
novelu	novela	k1gFnSc4
a	a	k8xC
povídku	povídka	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejoblíbenějších	oblíbený	k2eAgInPc2d3
žánrů	žánr	k1gInPc2
zábavné	zábavný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
detegere	detegrat	k5eAaPmIp3nS
=	=	kIx~
odkrývat	odkrývat	k5eAaImF
<g/>
,	,	kIx,
odhalovat	odhalovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápletka	zápletka	k1gFnSc1
detektivních	detektivní	k2eAgInPc2d1
příběhů	příběh	k1gInPc2
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
zločinu	zločin	k1gInSc6
(	(	kIx(
<g/>
nejčastěji	často	k6eAd3
vraždě	vražda	k1gFnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
jehož	jehož	k3xOyRp3gMnSc6
pachateli	pachatel	k1gMnSc6
<g/>
,	,	kIx,
detailech	detail	k1gInPc6
způsobu	způsob	k1gInSc2
provedení	provedení	k1gNnSc1
nebo	nebo	k8xC
motivu	motiv	k1gInSc2
pátrá	pátrat	k5eAaImIp3nS
detektiv	detektiv	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příbuzným	příbuzný	k2eAgInSc7d1
typem	typ	k1gInSc7
kriminální	kriminální	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
jsou	být	k5eAaImIp3nP
pitavaly	pitaval	k1gInPc1
a	a	k8xC
paměti	paměť	k1gFnSc2
slavných	slavný	k2eAgMnPc2d1
detektivů	detektiv	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
však	však	k9
popisují	popisovat	k5eAaImIp3nP
zločiny	zločin	k1gInPc1
skutečné	skutečný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Detektivka	detektivka	k1gFnSc1
má	mít	k5eAaImIp3nS
rovněž	rovněž	k9
blízko	blízko	k6eAd1
k	k	k7c3
thrilleru	thriller	k1gInSc2
nebo	nebo	k8xC
špionážnímu	špionážní	k2eAgInSc3d1
románu	román	k1gInSc3
<g/>
,	,	kIx,
často	často	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
kombinaci	kombinace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historická	historický	k2eAgFnSc1d1
detektivka	detektivka	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
děj	děj	k1gInSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
ve	v	k7c6
vzdálenější	vzdálený	k2eAgFnSc6d2
minulosti	minulost	k1gFnSc6
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
též	též	k9
za	za	k7c4
subžánr	subžánr	k1gInSc4
historického	historický	k2eAgInSc2d1
románu	román	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
byla	být	k5eAaImAgFnS
detektivka	detektivka	k1gFnSc1
pojímána	pojímán	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
složitá	složitý	k2eAgFnSc1d1
hádanka	hádanka	k1gFnSc1
<g/>
,	,	kIx,
intelektuální	intelektuální	k2eAgFnSc1d1
hra	hra	k1gFnSc1
a	a	k8xC
výzva	výzva	k1gFnSc1
čtenářovým	čtenářův	k2eAgFnPc3d1
logickým	logický	k2eAgFnPc3d1
a	a	k8xC
deduktivním	deduktivní	k2eAgFnPc3d1
schopnostem	schopnost	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozuzlení	rozuzlení	k1gNnPc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
racionálně	racionálně	k6eAd1
zdůvodněné	zdůvodněný	k2eAgFnPc1d1
a	a	k8xC
vycházející	vycházející	k2eAgFnPc1d1
z	z	k7c2
indicií	indicie	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
překvapivé	překvapivý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
musí	muset	k5eAaImIp3nS
postupovat	postupovat	k5eAaImF
logicky	logicky	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
čtenář	čtenář	k1gMnSc1
mohl	moct	k5eAaImAgMnS
sledovat	sledovat	k5eAaImF
postup	postup	k1gInSc4
dedukce	dedukce	k1gFnSc2
ze	z	k7c2
stop	stopa	k1gFnPc2
–	–	k?
za	za	k7c7
tím	ten	k3xDgInSc7
účelem	účel	k1gInSc7
vznikala	vznikat	k5eAaImAgFnS
i	i	k9
striktní	striktní	k2eAgNnPc4d1
žánrová	žánrový	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
(	(	kIx(
<g/>
např.	např.	kA
desatero	desatero	k1gNnSc1
Ronalda	Ronaldo	k1gNnSc2
Knoxe	Knoxe	k1gFnSc2
<g/>
,	,	kIx,
pravidlo	pravidlo	k1gNnSc1
tří	tři	k4xCgFnPc2
stop	stopa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vykonstruovanost	vykonstruovanost	k1gFnSc1
zápletek	zápletka	k1gFnPc2
však	však	k9
od	od	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
postupně	postupně	k6eAd1
ustupovala	ustupovat	k5eAaImAgFnS
a	a	k8xC
pravidla	pravidlo	k1gNnPc1
se	se	k3xPyFc4
rozvolňovala	rozvolňovat	k5eAaImAgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důrazem	důraz	k1gInSc7
na	na	k7c6
psychologii	psychologie	k1gFnSc6
postav	postava	k1gFnPc2
se	se	k3xPyFc4
některé	některý	k3yIgFnPc1
pozdější	pozdní	k2eAgFnPc1d2
detektivky	detektivka	k1gFnPc1
výrazně	výrazně	k6eAd1
přibližují	přibližovat	k5eAaImIp3nP
psychologickému	psychologický	k2eAgInSc3d1
románu	román	k1gInSc3
(	(	kIx(
<g/>
Georges	Georges	k1gMnSc1
Simenon	Simenon	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
se	se	k3xPyFc4
zase	zase	k9
barvitým	barvitý	k2eAgNnSc7d1
zachycením	zachycení	k1gNnSc7
prostředí	prostředí	k1gNnSc2
přibližují	přibližovat	k5eAaImIp3nP
románu	román	k1gInSc2
sociálnímu	sociální	k2eAgNnSc3d1
(	(	kIx(
<g/>
např.	např.	kA
u	u	k7c2
představitelů	představitel	k1gMnPc2
tzv.	tzv.	kA
drsné	drsný	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
detektivky	detektivka	k1gFnSc2
je	být	k5eAaImIp3nS
detektiv	detektiv	k1gMnSc1
<g/>
,	,	kIx,
řešitel	řešitel	k1gMnSc1
záhady	záhada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
romantickou	romantický	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
mstitelů	mstitel	k1gMnPc2
bezpráví	bezpráví	k1gNnSc2
<g/>
,	,	kIx,
často	často	k6eAd1
je	být	k5eAaImIp3nS
obdařen	obdařit	k5eAaPmNgInS
mimořádnými	mimořádný	k2eAgFnPc7d1
pozorovacími	pozorovací	k2eAgFnPc7d1
a	a	k8xC
kombinačními	kombinační	k2eAgFnPc7d1
schopnostmi	schopnost	k1gFnPc7
<g/>
,	,	kIx,
později	pozdě	k6eAd2
též	též	k9
znalostmi	znalost	k1gFnPc7
lidské	lidský	k2eAgFnSc2d1
psychiky	psychika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
bývá	bývat	k5eAaImIp3nS
výstředníkem	výstředník	k1gMnSc7
až	až	k8xS
podivínem	podivín	k1gMnSc7
<g/>
,	,	kIx,
jindy	jindy	k6eAd1
má	mít	k5eAaImIp3nS
působit	působit	k5eAaImF
jako	jako	k9
okolím	okolí	k1gNnSc7
podceňovaný	podceňovaný	k2eAgMnSc1d1
outsider	outsider	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
obvyklé	obvyklý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
řadě	řada	k1gFnSc6
děl	dělo	k1gNnPc2
jednoho	jeden	k4xCgMnSc2
autora	autor	k1gMnSc2
vystupuje	vystupovat	k5eAaImIp3nS
postava	postava	k1gFnSc1
téhož	týž	k3xTgMnSc2
detektiva	detektiv	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
je	být	k5eAaImIp3nS
obvyklejší	obvyklý	k2eAgInSc1d2
typ	typ	k1gInSc1
obyčejného	obyčejný	k2eAgMnSc2d1
detektiva	detektiv	k1gMnSc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
již	již	k9
detektiva	detektiv	k1gMnSc2
profesionálního	profesionální	k2eAgMnSc2d1
(	(	kIx(
<g/>
policista	policista	k1gMnSc1
<g/>
,	,	kIx,
soukromý	soukromý	k2eAgMnSc1d1
detektiv	detektiv	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
soukromé	soukromý	k2eAgFnPc1d1
osoby	osoba	k1gFnPc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
zločin	zločin	k1gInSc1
zasáhl	zasáhnout	k5eAaPmAgInS
do	do	k7c2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgFnPc4d1
detektivky	detektivka	k1gFnPc4
často	často	k6eAd1
popisují	popisovat	k5eAaImIp3nP
rutinní	rutinní	k2eAgNnSc4d1
vyšetřování	vyšetřování	k1gNnSc4
celých	celý	k2eAgInPc2d1
policejních	policejní	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Deduktivní	deduktivní	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
s	s	k7c7
kriminálními	kriminální	k2eAgInPc7d1
náměty	námět	k1gInPc7
nacházíme	nacházet	k5eAaImIp1nP
už	už	k6eAd1
v	v	k7c6
biblických	biblický	k2eAgInPc6d1
apokryfech	apokryf	k1gInPc6
<g/>
,	,	kIx,
Ezopových	Ezopův	k2eAgFnPc6d1
bajkách	bajka	k1gFnPc6
či	či	k8xC
orientálních	orientální	k2eAgFnPc6d1
pohádkách	pohádka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	své	k1gNnSc1
předchůdce	předchůdce	k1gMnSc2
má	mít	k5eAaImIp3nS
detektivka	detektivka	k1gFnSc1
v	v	k7c6
tzv.	tzv.	kA
pikareskním	pikareskní	k2eAgMnSc6d1
(	(	kIx(
<g/>
šibalském	šibalský	k2eAgInSc6d1
<g/>
)	)	kIx)
a	a	k8xC
loupežnickém	loupežnický	k2eAgInSc6d1
románu	román	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
thriller	thriller	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
tzv.	tzv.	kA
gotickém	gotický	k2eAgNnSc6d1
románu	román	k1gInSc6
–	–	k?
jde	jít	k5eAaImIp3nS
také	také	k9
o	o	k7c4
typ	typ	k1gInSc4
prózy	próza	k1gFnSc2
s	s	k7c7
tajemstvím	tajemství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
samostatný	samostatný	k2eAgInSc4d1
žánr	žánr	k1gInSc4
se	se	k3xPyFc4
detektivní	detektivní	k2eAgInSc1d1
román	román	k1gInSc1
konstituoval	konstituovat	k5eAaBmAgInS
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozšíření	rozšíření	k1gNnSc1
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
literatury	literatura	k1gFnSc2
pak	pak	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
vytvářením	vytváření	k1gNnSc7
moderní	moderní	k2eAgFnSc2d1
velkoměstské	velkoměstský	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
s	s	k7c7
jejím	její	k3xOp3gNnSc7
podsvětím	podsvětí	k1gNnSc7
a	a	k8xC
policejním	policejní	k2eAgInSc7d1
aparátem	aparát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
první	první	k4xOgFnSc4
detektivní	detektivní	k2eAgFnSc4d1
prózu	próza	k1gFnSc4
v	v	k7c6
historii	historie	k1gFnSc6
bývá	bývat	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
povídka	povídka	k1gFnSc1
Edgara	Edgar	k1gMnSc2
Allana	Allan	k1gMnSc2
Poea	Poeus	k1gMnSc2
Vraždy	vražda	k1gFnSc2
v	v	k7c6
ulici	ulice	k1gFnSc6
Morgue	Morgu	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1841	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Poeově	Poeův	k2eAgInSc6d1
stylu	styl	k1gInSc6
logické	logický	k2eAgFnSc2d1
hádanky	hádanka	k1gFnSc2
pokračuje	pokračovat	k5eAaImIp3nS
pak	pak	k6eAd1
A.C.	A.C.	k1gFnSc1
Doyle	Doyle	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
(	(	kIx(
<g/>
Gilbert	Gilbert	k1gMnSc1
Keith	Keitha	k1gFnPc2
Chesterton	Chesterton	k1gInSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Austin	Austin	k1gMnSc1
Freeman	Freeman	k1gMnSc1
<g/>
,	,	kIx,
S.	S.	kA
<g/>
S.	S.	kA
<g/>
Van	vana	k1gFnPc2
Dine	din	k1gInSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
Dorothy	Dorotha	k1gFnPc1
Sayersová	Sayersová	k1gFnSc1
<g/>
,	,	kIx,
Agatha	Agatha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Christie	Christie	k1gFnSc1
<g/>
,	,	kIx,
Ngaio	Ngaio	k6eAd1
Marshová	Marshový	k2eAgFnSc1d1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
D.	D.	kA
Jamesová	Jamesový	k2eAgFnSc1d1
aj.	aj.	kA
Na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
detektivky	detektivka	k1gFnSc2
a	a	k8xC
thrilleru	thriller	k1gInSc2
jsou	být	k5eAaImIp3nP
díla	dílo	k1gNnPc1
Edgara	Edgar	k1gMnSc2
Wallace	Wallace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
USA	USA	kA
zrodil	zrodit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
typ	typ	k1gInSc1
detektivky	detektivka	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
drsná	drsný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
protagonistou	protagonista	k1gMnSc7
je	být	k5eAaImIp3nS
osamělý	osamělý	k2eAgMnSc1d1
bojovník	bojovník	k1gMnSc1
za	za	k7c4
spravedlnost	spravedlnost	k1gFnSc4
typu	typ	k1gInSc2
westernového	westernový	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
(	(	kIx(
<g/>
díla	dílo	k1gNnSc2
Dashiella	Dashiello	k1gNnSc2
Hammetta	Hammett	k1gMnSc2
<g/>
,	,	kIx,
Raymonda	Raymond	k1gMnSc2
Chandlera	Chandler	k1gMnSc2
<g/>
,	,	kIx,
Rexe	Rex	k1gMnSc2
Stouta	Stout	k1gMnSc2
<g/>
,	,	kIx,
P.	P.	kA
Cheyneyho	Cheyney	k1gMnSc2
<g/>
,	,	kIx,
Mickeyho	Mickey	k1gMnSc2
Spillana	Spillan	k1gMnSc2
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
též	též	k9
Erle	Erle	k1gInSc1
Stanleye	Stanley	k1gMnSc2
Gardnera	Gardner	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
detektivky	detektivka	k1gFnSc2
i	i	k8xC
policejní	policejní	k2eAgMnSc1d1
profesionál	profesionál	k1gMnSc1
(	(	kIx(
<g/>
např.	např.	kA
díla	dílo	k1gNnSc2
G.	G.	kA
Simenona	Simenon	k1gMnSc4
nebo	nebo	k8xC
Eda	Eda	k1gMnSc1
McBaina	McBaina	k1gMnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
celý	celý	k2eAgInSc4d1
policejní	policejní	k2eAgInSc4d1
tým	tým	k1gInSc4
(	(	kIx(
<g/>
Maj	Maja	k1gFnPc2
Sjöwallová	Sjöwallová	k1gFnSc1
<g/>
–	–	k?
<g/>
Per	pero	k1gNnPc2
Wahlöö	Wahlöö	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
ze	z	k7c2
severní	severní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
prosazuje	prosazovat	k5eAaImIp3nS
severská	severský	k2eAgFnSc1d1
detektivka	detektivka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Detektivky	detektivka	k1gFnPc1
bývají	bývat	k5eAaImIp3nP
častým	častý	k2eAgInSc7d1
námětem	námět	k1gInSc7
pro	pro	k7c4
další	další	k2eAgNnSc4d1
audiovizuální	audiovizuální	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
jejich	jejich	k3xOp3gInSc2
námětu	námět	k1gInSc2
v	v	k7c6
rozhlase	rozhlas	k1gInSc6
<g/>
,	,	kIx,
filmu	film	k1gInSc6
a	a	k8xC
televizi	televize	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
oblíbené	oblíbený	k2eAgFnPc4d1
detektivky	detektivka	k1gFnPc4
zaměřené	zaměřený	k2eAgFnPc4d1
na	na	k7c4
moderní	moderní	k2eAgFnPc4d1
vyšetřovací	vyšetřovací	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c4
psychologické	psychologický	k2eAgNnSc4d1
profilování	profilování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3
světoví	světový	k2eAgMnPc1d1
autoři	autor	k1gMnPc1
</s>
<s>
Arthur	Arthur	k1gMnSc1
Conan	Conan	k1gMnSc1
Doyle	Doyle	k1gInSc4
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
Dürrenmatt	Dürrenmatt	k1gMnSc1
</s>
<s>
Dick	Dick	k1gInSc1
Francis	Francis	k1gFnSc2
</s>
<s>
Nicolas	Nicolas	k1gInSc1
Freeling	Freeling	k1gInSc1
</s>
<s>
Erle	Erle	k1gFnSc1
Stanley	Stanlea	k1gFnSc2
Gardner	Gardnra	k1gFnPc2
</s>
<s>
Dashiell	Dashiell	k1gMnSc1
Hammett	Hammett	k1gMnSc1
</s>
<s>
Raymond	Raymond	k1gMnSc1
Chandler	Chandler	k1gMnSc1
</s>
<s>
James	James	k1gInSc1
Hadley	Hadlea	k1gFnSc2
Chase	chasa	k1gFnSc3
</s>
<s>
G.K.	G.K.	k?
Chesterton	Chesterton	k1gInSc1
</s>
<s>
Peter	Peter	k1gMnSc1
Cheyney	Cheynea	k1gFnSc2
</s>
<s>
Lee	Lea	k1gFnSc3
Child	Child	k1gMnSc1
</s>
<s>
Agatha	Agatha	k1gFnSc1
Christie	Christie	k1gFnSc2
</s>
<s>
P.	P.	kA
<g/>
D.	D.	kA
Jamesová	Jamesový	k2eAgNnPc5d1
</s>
<s>
Ed	Ed	k?
McBain	McBain	k1gInSc1
</s>
<s>
Edgar	Edgar	k1gMnSc1
Allan	Allan	k1gMnSc1
Poe	Poe	k1gMnSc1
</s>
<s>
Dorothy	Doroth	k1gInPc1
Sayersová	Sayersová	k1gFnSc1
</s>
<s>
Georges	Georges	k1gMnSc1
Simenon	Simenon	k1gMnSc1
</s>
<s>
Rex	Rex	k?
Stout	Stout	k1gInSc1
</s>
<s>
S.	S.	kA
S.	S.	kA
Van	vana	k1gFnPc2
Dine	din	k1gInSc5
</s>
<s>
Edgar	Edgar	k1gMnSc1
Wallace	Wallace	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
detektivka	detektivka	k1gFnSc1
</s>
<s>
V	v	k7c6
české	český	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
jsou	být	k5eAaImIp3nP
za	za	k7c2
zakladatele	zakladatel	k1gMnSc2
detektivního	detektivní	k2eAgInSc2d1
žánru	žánr	k1gInSc2
považováni	považován	k2eAgMnPc1d1
Emil	Emil	k1gMnSc1
Vachek	Vachek	k1gMnSc1
a	a	k8xC
Eduard	Eduard	k1gMnSc1
Fiker	Fiker	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
začínají	začínat	k5eAaImIp3nP
publikovat	publikovat	k5eAaBmF
v	v	k7c6
období	období	k1gNnSc6
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgMnPc1d1
významnější	významný	k2eAgMnPc1d2
autoři	autor	k1gMnPc1
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
až	až	k9
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Škvorecký	Škvorecký	k2eAgMnSc1d1
(	(	kIx(
<g/>
některé	některý	k3yIgFnPc4
detektivky	detektivka	k1gFnPc4
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
společně	společně	k6eAd1
s	s	k7c7
Janem	Jan	k1gMnSc7
Zábranou	zábrana	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Erben	Erben	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
Prošková	Prošková	k1gFnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Hejcman	Hejcman	k1gMnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
např.	např.	kA
Jan	Jan	k1gMnSc1
Cimický	Cimický	k2eAgMnSc1d1
či	či	k8xC
Jaroslav	Jaroslav	k1gMnSc1
Velinský	velinský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Lipšanský	Lipšanský	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
naděje	naděje	k1gFnPc4
českého	český	k2eAgInSc2d1
detektivního	detektivní	k2eAgInSc2d1
románu	román	k1gInSc2
patří	patřit	k5eAaImIp3nS
Michaela	Michaela	k1gFnSc1
Klevisová	Klevisový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kroky	krok	k1gInPc4
vraha	vrah	k1gMnSc4
<g/>
,	,	kIx,
Zlodějka	zlodějka	k1gFnSc1
příběhů	příběh	k1gInPc2
<g/>
,	,	kIx,
Dům	dům	k1gInSc1
na	na	k7c6
samotě	samota	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Sýkora	Sýkora	k1gMnSc1
(	(	kIx(
<g/>
Případ	případ	k1gInSc1
pro	pro	k7c4
exorcistu	exorcista	k1gMnSc4
<g/>
,	,	kIx,
Modré	modrý	k2eAgInPc1d1
stíny	stín	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nela	Nela	k1gFnSc1
Rywiková	Rywikový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Dům	dům	k1gInSc1
číslo	číslo	k1gNnSc1
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Březina	Březina	k1gMnSc1
(	(	kIx(
<g/>
Na	na	k7c6
kopci	kopec	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
Martin	Martin	k1gMnSc1
Goffa	Goff	k1gMnSc2
(	(	kIx(
<g/>
Muž	muž	k1gMnSc1
s	s	k7c7
unavenýma	unavený	k2eAgNnPc7d1
očima	oko	k1gNnPc7
<g/>
,	,	kIx,
Vánoční	vánoční	k2eAgFnSc4d1
zpověď	zpověď	k1gFnSc4
<g/>
,	,	kIx,
Bez	bez	k7c2
těla	tělo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
MOCNÁ	mocný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Dagmar	Dagmar	k1gFnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
literárních	literární	k2eAgInPc2d1
žánrů	žánr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
106	#num#	k4
<g/>
-	-	kIx~
<g/>
114	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mandys	Mandys	k1gInSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
dnes	dnes	k6eAd1
v	v	k7c6
Česku	Česko	k1gNnSc6
píše	psát	k5eAaImIp3nS
krimi	krimi	k1gNnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Škvorecký	Škvorecký	k2eAgMnSc1d1
<g/>
:	:	kIx,
Nápady	nápad	k1gInPc1
čtenáře	čtenář	k1gMnPc4
detektivek	detektivka	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1965	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
doplněné	doplněný	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
1967	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc4d1
online	onlinout	k5eAaPmIp3nS
jako	jako	k9
e-kniha	e-kniha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mandys	Mandys	k1gInSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
dnes	dnes	k6eAd1
v	v	k7c6
Česku	Česko	k1gNnSc6
píše	psát	k5eAaImIp3nS
krimi	krimi	k1gNnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
na	na	k7c6
stránce	stránka	k1gFnSc6
iLiteratura	iLiteratura	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
</s>
<s>
Kokeš	Kokeš	k1gMnSc1
<g/>
,	,	kIx,
Radomír	Radomír	k1gMnSc1
D.	D.	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Fikční	fikční	k2eAgInPc1d1
světy	svět	k1gInPc1
(	(	kIx(
<g/>
kriminálního	kriminální	k2eAgInSc2d1
<g/>
)	)	kIx)
televizního	televizní	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iluminace	iluminace	k1gFnSc1
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
21	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
-	-	kIx~
<g/>
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Online	Onlin	k1gInSc5
<g/>
:	:	kIx,
ke	k	k7c3
stažení	stažení	k1gNnSc3
v	v	k7c6
elektronické	elektronický	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
zde	zde	k6eAd1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
100	#num#	k4
nejlepších	dobrý	k2eAgMnPc2d3
detektivních	detektivní	k2eAgMnPc2d1
románů	román	k1gInPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
podle	podle	k7c2
britské	britský	k2eAgFnSc2d1
a	a	k8xC
americké	americký	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
autorů	autor	k1gMnPc2
detektivních	detektivní	k2eAgInPc2d1
románů	román	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85037260	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85037260	#num#	k4
</s>
