<s>
Zápletka	zápletka	k1gFnSc1
jakých	jaký	k3yQgInPc2,k3yIgInPc2,k3yRgInPc2
příběhů	příběh	k1gInPc2
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
zločinu	zločin	k1gInSc6
<g/>
,	,	kIx,
po	po	k7c6
jehož	jehož	k3xOyRp3gMnSc6
pachateli	pachatel	k1gMnSc6
<g/>
,	,	kIx,
detailech	detail	k1gInPc6
způsobu	způsob	k1gInSc2
provedení	provedení	k1gNnSc2
nebo	nebo	k8xC
motivu	motiv	k1gInSc2
pátrá	pátrat	k5eAaImIp3nS
detektiv	detektiv	k1gMnSc1
<g/>
?	?	kIx.
</s>