<s>
Sava	Sava	k1gFnSc1
<g/>
'	'	kIx"
<g/>
id	idy	k1gFnPc2
Chamrija	Chamrij	k1gInSc2
</s>
<s>
Sava	Sava	k1gFnSc1
<g/>
'	'	kIx"
<g/>
id	idy	k1gFnPc2
Chamrija	Chamrijum	k1gNnSc2
ס	ס	k?
ח	ח	k?
ح	ح	k?
Celkový	celkový	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
Sava	Sava	k1gMnSc1
<g/>
'	'	kIx"
<g/>
id	idy	k1gFnPc2
ChamrijaPoloha	ChamrijaPoloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
32	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
35	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
153	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
distrikt	distrikt	k1gInSc4
</s>
<s>
Severní	severní	k2eAgFnSc1d1
oblastní	oblastní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Jizre	Jizr	k1gMnSc5
<g/>
'	'	kIx"
<g/>
elské	elská	k1gFnSc3
údolí	údolí	k1gNnSc2
</s>
<s>
Sava	Sava	k1gFnSc1
<g/>
'	'	kIx"
<g/>
id	idy	k1gFnPc2
Chamrija	Chamrij	k1gInSc2
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
144	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sava	Sava	k1gFnSc1
<g/>
'	'	kIx"
<g/>
id	ido	k1gNnPc2
Chamrija	Chamrijum	k1gNnSc2
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ס	ס	k?
ח	ח	k?
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
س	س	k?
ح	ح	k?
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Suweid	Suweid	k1gInSc1
Hamira	Hamir	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
seznamu	seznam	k1gInSc6
sídel	sídlo	k1gNnPc2
Sawaid	Sawaida	k1gFnPc2
Hamriyye	Hamriyye	k1gFnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
též	též	k9
Sawa	Saw	k2eAgFnSc1d1
<g/>
'	'	kIx"
<g/>
id	ido	k1gNnPc2
(	(	kIx(
<g/>
Hamriyye	Hamriyye	k1gInSc1
<g/>
))	))	k?
je	být	k5eAaImIp3nS
arabská	arabský	k2eAgFnSc1d1
beduínská	beduínský	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Severním	severní	k2eAgInSc6d1
distriktu	distrikt	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Oblastní	oblastní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
Jizre	Jizr	k1gInSc5
<g/>
'	'	kIx"
<g/>
elské	elská	k1gFnSc3
údolí	údolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
153	#num#	k4
metrů	metr	k1gInPc2
pahorcích	pahorek	k1gInPc6
Dolní	dolní	k2eAgFnSc6d1
Galileji	Galilea	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vesnice	vesnice	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
cca	cca	kA
3	#num#	k4
kilometry	kilometr	k1gInPc7
jižně	jižně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Šfaram	Šfaram	k1gInSc1
<g/>
,	,	kIx,
cca	cca	kA
85	#num#	k4
kilometrů	kilometr	k1gInPc2
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Tel	tel	kA
Avivu	Aviv	k1gInSc2
a	a	k8xC
cca	cca	kA
18	#num#	k4
kilometrů	kilometr	k1gInPc2
východně	východně	k6eAd1
od	od	k7c2
Haify	Haifa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sava	Sav	k1gInSc1
<g/>
'	'	kIx"
<g/>
id	id	k1gFnSc1
Chamrija	Chamrija	k1gNnSc1
obývají	obývat	k5eAaImIp3nP
izraelští	izraelský	k2eAgMnPc1d1
Arabové	Arab	k1gMnPc1
<g/>
,	,	kIx,
respektive	respektive	k9
Beduíni	Beduín	k1gMnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
osídlení	osídlení	k1gNnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
je	být	k5eAaImIp3nS
převážně	převážně	k6eAd1
arabské	arabský	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vesnice	vesnice	k1gFnSc1
ale	ale	k9
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
židovským	židovský	k2eAgInSc7d1
kibucem	kibuc	k1gInSc7
Harduf	Harduf	k1gInSc1
a	a	k8xC
cca	cca	kA
5	#num#	k4
kilometrů	kilometr	k1gInPc2
západním	západní	k2eAgInSc7d1
směrem	směr	k1gInSc7
začíná	začínat	k5eAaImIp3nS
při	při	k7c6
Haifském	haifský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
oblast	oblast	k1gFnSc1
s	s	k7c7
židovskou	židovský	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Sava	Sava	k1gFnSc1
<g/>
'	'	kIx"
<g/>
id	idy	k1gFnPc2
Chamrija	Chamrij	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c4
dopravní	dopravní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
napojen	napojen	k2eAgMnSc1d1
pomocí	pomocí	k7c2
lokální	lokální	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
na	na	k7c6
severu	sever	k1gInSc6
vede	vést	k5eAaImIp3nS
k	k	k7c3
městu	město	k1gNnSc3
Šfaram	Šfaram	k1gInSc1
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yQgInSc2,k3yIgInSc2,k3yRgInSc2
ústí	ústit	k5eAaImIp3nS
do	do	k7c2
dálnice	dálnice	k1gFnSc2
číslo	číslo	k1gNnSc1
79	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Sava	Sava	k1gFnSc1
<g/>
'	'	kIx"
<g/>
id	idy	k1gFnPc2
Chamrija	Chamrij	k1gInSc2
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
uznán	uznat	k5eAaPmNgMnS
za	za	k7c4
oficiální	oficiální	k2eAgFnSc4d1
obec	obec	k1gFnSc4
a	a	k8xC
připojil	připojit	k5eAaPmAgMnS
se	se	k3xPyFc4
k	k	k7c3
Oblastní	oblastní	k2eAgFnSc3d1
radě	rada	k1gFnSc3
Jizre	Jizr	k1gInSc5
<g/>
'	'	kIx"
<g/>
elské	elská	k1gFnSc3
údolí	údolí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Už	už	k6eAd1
předtím	předtím	k6eAd1
zde	zde	k6eAd1
existovalo	existovat	k5eAaImAgNnS
beduínské	beduínský	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
mění	měnit	k5eAaImIp3nS
z	z	k7c2
dočasného	dočasný	k2eAgNnSc2d1
stanoviště	stanoviště	k1gNnSc2
kočovných	kočovný	k2eAgMnPc2d1
pastevců	pastevec	k1gMnPc2
v	v	k7c4
trvalé	trvalý	k2eAgNnSc4d1
vesnické	vesnický	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
tvořili	tvořit	k5eAaImAgMnP
obyvatelstvo	obyvatelstvo	k1gNnSc4
v	v	k7c6
Sava	Sava	k1gMnSc1
<g/>
'	'	kIx"
<g/>
id	idy	k1gFnPc2
Chamrija	Chamrij	k1gInSc2
izraelští	izraelský	k2eAgMnPc1d1
Arabové	Arab	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jde	jít	k5eAaImIp3nS
o	o	k7c4
menší	malý	k2eAgNnSc4d2
sídlo	sídlo	k1gNnSc4
vesnického	vesnický	k2eAgInSc2d1
typu	typ	k1gInSc2
se	s	k7c7
stagnující	stagnující	k2eAgFnSc7d1
populací	populace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2014	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
144	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
populace	populace	k1gFnSc1
stoupla	stoupnout	k5eAaPmAgFnS
o	o	k7c4
4,3	4,3	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
jiného	jiný	k2eAgInSc2d1
pramene	pramen	k1gInSc2
ale	ale	k8xC
v	v	k7c6
obci	obec	k1gFnSc6
žije	žít	k5eAaImIp3nS
200	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
územní	územní	k2eAgInSc4d1
plán	plán	k1gInSc4
tu	tu	k6eAd1
počítá	počítat	k5eAaImIp3nS
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
80	#num#	k4
rodin	rodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
Sava	Sav	k1gInSc2
<g/>
'	'	kIx"
<g/>
id	idy	k1gFnPc2
Chamrija	Chamrijum	k1gNnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
464646132134131118124138144	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
י	י	k?
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
י	י	k?
2013	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
ס	ס	k?
ח	ח	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.emekyizrael.org.il	www.emekyizrael.org.il	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ס	ס	k?
ח	ח	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
galil-net	galil-net	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
il	il	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
izraelští	izraelský	k2eAgMnPc1d1
Arabové	Arab	k1gMnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
profil	profil	k1gInSc1
obce	obec	k1gFnSc2
na	na	k7c6
portálu	portál	k1gInSc6
Galil	Galila	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
profil	profil	k1gInSc1
obce	obec	k1gFnSc2
na	na	k7c6
portálu	portál	k1gInSc6
Rom	Rom	k1gMnSc1
Galil	Galil	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Oblastní	oblastní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
Jizre	Jizr	k1gInSc5
<g/>
'	'	kIx"
<g/>
elské	elská	k1gFnSc3
údolí	údolí	k1gNnSc2
Kibucy	kibuc	k1gInPc4
</s>
<s>
Alonim	Alonim	k1gMnSc1
·	·	k?
Dovrat	Dovrat	k1gMnSc1
·	·	k?
Ejn	Ejn	k1gFnSc2
Dor	Dora	k1gFnPc2
·	·	k?
Gazit	Gazit	k1gMnSc1
·	·	k?
Ginegar	Ginegar	k1gMnSc1
·	·	k?
Gvat	Gvat	k1gMnSc1
·	·	k?
Harduf	Harduf	k1gMnSc1
·	·	k?
ha-Solelim	ha-Solelim	k1gMnSc1
·	·	k?
Chanaton	Chanaton	k1gInSc1
·	·	k?
Jif	Jif	k1gFnSc1
<g/>
'	'	kIx"
<g/>
at	at	k?
·	·	k?
Kfar	Kfar	k1gInSc1
ha-Choreš	ha-Chorat	k5eAaBmIp2nS,k5eAaPmIp2nS
·	·	k?
Merchavja	Merchavja	k1gMnSc1
·	·	k?
Mizra	Mizra	k1gMnSc1
·	·	k?
Ramat	Ramat	k2eAgMnSc1d1
David	David	k1gMnSc1
·	·	k?
Sarid	Sarid	k1gInSc1
Mošavy	Mošava	k1gFnSc2
</s>
<s>
Alonej	Alonat	k5eAaPmRp2nS,k5eAaImRp2nS
Abba	Abba	k1gFnSc1
·	·	k?
Balfourija	Balfourija	k1gFnSc1
·	·	k?
Bejt	Bejt	k?
Lechem	lech	k1gInSc7
ha-Glilit	ha-Glilita	k1gFnPc2
(	(	kIx(
<g/>
Galilejský	galilejský	k2eAgInSc1d1
Betlém	Betlém	k1gInSc1
<g/>
)	)	kIx)
·	·	k?
Bejt	Bejt	k?
Še	Še	k1gFnSc1
<g/>
'	'	kIx"
<g/>
arim	arim	k1gInSc1
·	·	k?
Bejt	Bejt	k?
Zaid	Zaid	k1gInSc1
·	·	k?
Cipori	Cipor	k1gFnSc2
·	·	k?
ha-Jogev	ha-Jogev	k1gFnSc1
·	·	k?
Kfar	Kfar	k1gMnSc1
Baruch	Baruch	k1gMnSc1
·	·	k?
Kfar	Kfar	k1gMnSc1
Gid	Gid	k1gMnSc1
<g/>
'	'	kIx"
<g/>
on	on	k3xPp3gMnSc1
·	·	k?
Kfar	Kfar	k1gMnSc1
Jehošua	Jehošua	k1gMnSc1
·	·	k?
Merchavja	Merchavja	k1gMnSc1
·	·	k?
Nahalal	Nahalal	k1gMnSc1
·	·	k?
Sde	Sde	k1gMnSc1
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akov	akov	k1gMnSc1
·	·	k?
Tel	tel	kA
Adašim	Adašim	k1gMnSc1
Společné	společný	k2eAgFnSc2d1
osady	osada	k1gFnSc2
</s>
<s>
Adi	Adi	k?
·	·	k?
Achuzat	Achuzat	k1gMnSc1
Barak	Barak	k1gMnSc1
·	·	k?
Alon	Alon	k1gMnSc1
ha-Galil	ha-Galit	k5eAaPmAgMnS,k5eAaImAgMnS
·	·	k?
Giv	Giv	k1gMnSc1
<g/>
'	'	kIx"
<g/>
at	at	k?
Ela	Ela	k1gFnSc1
·	·	k?
Hoša	Hoša	k?
<g/>
'	'	kIx"
<g/>
aja	aja	k?
·	·	k?
Šimšit	Šimšit	k1gFnSc1
·	·	k?
Timrat	Timrat	k1gInSc4
Arabské	arabský	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
</s>
<s>
Manšija	Manšija	k1gMnSc1
Zabda	Zabda	k1gMnSc1
·	·	k?
Sava	Sava	k1gMnSc1
<g/>
'	'	kIx"
<g/>
id	idy	k1gFnPc2
Chamrija	Chamrij	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
