<s>
Český	český	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
institut	institut	k1gInSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
ČMI	ČMI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
metrologický	metrologický	k2eAgInSc4d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
služby	služba	k1gFnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
metrologie	metrologie	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>