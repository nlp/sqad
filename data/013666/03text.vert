<s>
Český	český	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
institut	institut	k1gInSc1
Budova	budova	k1gFnSc1
ČMI	ČMI	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
-	-	kIx~
Chodově	Chodov	k1gInSc6
Vznik	vznik	k1gInSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1991	#num#	k4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
příspěvková	příspěvkový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Okružní	okružní	k2eAgNnSc4d1
772	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
638	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Oficiální	oficiální	k2eAgNnSc1d1
web	web	k1gInSc4
</s>
<s>
www.cmi.cz	www.cmi.cz	k1gInSc1
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
65	#num#	k4
<g/>
msw	msw	k?
<g/>
6	#num#	k4
<g/>
w	w	k?
IČO	IČO	kA
</s>
<s>
00177016	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
institut	institut	k1gInSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
ČMI	ČMI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
metrologický	metrologický	k2eAgInSc4d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
služby	služba	k1gFnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
metrologie	metrologie	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
příspěvkovou	příspěvkový	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
zřízenou	zřízený	k2eAgFnSc7d1
Ministerstvem	ministerstvo	k1gNnSc7
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
kterým	který	k3yQgFnPc3,k3yIgFnPc3,k3yRgFnPc3
je	být	k5eAaImIp3nS
řízen	řídit	k5eAaImNgInS
<g/>
;	;	kIx,
jde	jít	k5eAaImIp3nS
tedy	tedy	k9
o	o	k7c4
státní	státní	k2eAgFnSc4d1
instituci	instituce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
rozdělil	rozdělit	k5eAaPmAgInS
Československý	československý	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
(	(	kIx(
<g/>
ČSMÚ	ČSMÚ	kA
<g/>
)	)	kIx)
na	na	k7c6
ČMI	ČMI	kA
a	a	k8xC
Slovenský	slovenský	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
(	(	kIx(
<g/>
SMÚ	SMÚ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pod	pod	k7c7
ČMI	ČMI	kA
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc1d1
oblastní	oblastní	k2eAgInPc1d1
inspektoráty	inspektorát	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
,	,	kIx,
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
Liberci	Liberec	k1gInSc6
<g/>
,	,	kIx,
Mostu	most	k1gInSc2
<g/>
,	,	kIx,
Pardubicích	Pardubice	k1gInPc6
<g/>
,	,	kIx,
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Jihlavě	Jihlava	k1gFnSc6
<g/>
,	,	kIx,
Kroměříži	Kroměříž	k1gFnSc6
<g/>
,	,	kIx,
Opavě	Opava	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
Olomouci	Olomouc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k6eAd1
to	ten	k3xDgNnSc1
pod	pod	k7c4
něj	on	k3xPp3gMnSc4
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
ještě	ještě	k9
Inspektorát	inspektorát	k1gInSc1
pro	pro	k7c4
ionizující	ionizující	k2eAgNnSc4d1
záření	záření	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
TESTCOM	TESTCOM	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Laboratoře	laboratoř	k1gFnSc2
primární	primární	k2eAgFnSc2d1
metrologie	metrologie	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
primární-fundamentální	primární-fundamentální	k2eAgFnSc2d1
metrologie	metrologie	k1gFnSc2
</s>
<s>
Vědecká	vědecký	k2eAgFnSc1d1
metrologie	metrologie	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
organizaci	organizace	k1gFnSc4
a	a	k8xC
vývoj	vývoj	k1gInSc4
etalonů	etalon	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
uchovávání	uchovávání	k1gNnSc4
na	na	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
zabezpečuje	zabezpečovat	k5eAaImIp3nS
budování	budování	k1gNnSc4
<g/>
,	,	kIx,
uchovávaní	uchovávaný	k2eAgMnPc1d1
a	a	k8xC
zdokonalování	zdokonalování	k1gNnSc2
státních	státní	k2eAgInPc2d1
etalonů	etalon	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
mezinárodní	mezinárodní	k2eAgNnSc4d1
porovnávání	porovnávání	k1gNnSc4
a	a	k8xC
návaznost	návaznost	k1gFnSc4
<g/>
,	,	kIx,
také	také	k9
tvoří	tvořit	k5eAaImIp3nP
metodiky	metodika	k1gFnSc2
pro	pro	k7c4
přenos	přenos	k1gInSc4
hodnot	hodnota	k1gFnPc2
jednotek	jednotka	k1gFnPc2
na	na	k7c4
sekundární	sekundární	k2eAgInPc4d1
etalony	etalon	k1gInPc4
</s>
<s>
provádí	provádět	k5eAaImIp3nS
vědeckou	vědecký	k2eAgFnSc4d1
<g/>
,	,	kIx,
vývojovou	vývojový	k2eAgFnSc4d1
a	a	k8xC
výzkumnou	výzkumný	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
metrologie	metrologie	k1gFnSc2
</s>
<s>
zabezpečuje	zabezpečovat	k5eAaImIp3nS
delegáty	delegát	k1gMnPc4
na	na	k7c4
mezinárodní	mezinárodní	k2eAgFnSc4d1
metrologickou	metrologický	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
</s>
<s>
zabezpečuje	zabezpečovat	k5eAaImIp3nS
přenos	přenos	k1gInSc4
(	(	kIx(
<g/>
metrologickou	metrologický	k2eAgFnSc4d1
návaznost	návaznost	k1gFnSc4
<g/>
)	)	kIx)
na	na	k7c4
etalony	etalon	k1gInPc4
nižších	nízký	k2eAgInPc2d2
řádů	řád	k1gInPc2
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
legální	legální	k2eAgFnSc2d1
metrologie	metrologie	k1gFnSc2
</s>
<s>
Oblast	oblast	k1gFnSc1
legální	legální	k2eAgFnSc2d1
metrologie	metrologie	k1gFnSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
přesnost	přesnost	k1gFnSc4
měření	měření	k1gNnSc2
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
měřidla	měřidlo	k1gNnPc1
mají	mít	k5eAaImIp3nP
vliv	vliv	k1gInSc4
na	na	k7c4
průhlednost	průhlednost	k1gFnSc4
ekonomických	ekonomický	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
<g/>
,	,	kIx,
zdraví	zdraví	k1gNnSc4
a	a	k8xC
bezpečnost	bezpečnost	k1gFnSc4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
a	a	k8xC
jiné	jiný	k2eAgInPc4d1
obecné	obecný	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
zabezpečuje	zabezpečovat	k5eAaImIp3nS
státní	státní	k2eAgFnSc2d1
metrologické	metrologický	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
měřidel	měřidlo	k1gNnPc2
<g/>
,	,	kIx,
schvalování	schvalování	k1gNnSc1
typu	typ	k1gInSc2
a	a	k8xC
ověřování	ověřování	k1gNnSc4
měřidel	měřidlo	k1gNnPc2
</s>
<s>
řídí	řídit	k5eAaImIp3nS
tvorbu	tvorba	k1gFnSc4
certifikovaných	certifikovaný	k2eAgInPc2d1
referenčních	referenční	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
</s>
<s>
provádí	provádět	k5eAaImIp3nS
státní	státní	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
dozor	dozor	k1gInSc1
<g/>
,	,	kIx,
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
opatření	opatření	k1gNnSc1
obecné	obecný	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
podle	podle	k7c2
zákona	zákon	k1gInSc2
505	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
metrologii	metrologie	k1gFnSc6
</s>
<s>
zpracovává	zpracovávat	k5eAaImIp3nS
návrhy	návrh	k1gInPc4
Výměrů	výměr	k1gInPc2
o	o	k7c6
stanovených	stanovený	k2eAgNnPc6d1
měřidlech	měřidlo	k1gNnPc6
</s>
<s>
posuzuje	posuzovat	k5eAaImIp3nS
způsobilost	způsobilost	k1gFnSc4
žadatelů	žadatel	k1gMnPc2
o	o	k7c6
autorizaci	autorizace	k1gFnSc6
k	k	k7c3
ověřování	ověřování	k1gNnSc3
měřidla	měřidlo	k1gNnSc2
k	k	k7c3
úřednímu	úřední	k2eAgNnSc3d1
měření	měření	k1gNnSc3
</s>
<s>
registruje	registrovat	k5eAaBmIp3nS
výrobce	výrobce	k1gMnPc4
a	a	k8xC
opravny	opravna	k1gFnPc4
měřidel	měřidlo	k1gNnPc2
a	a	k8xC
organizace	organizace	k1gFnSc2
provádějící	provádějící	k2eAgFnSc4d1
montáž	montáž	k1gFnSc4
měřidel	měřidlo	k1gNnPc2
</s>
<s>
V	v	k7c6
neregulované	regulovaný	k2eNgFnSc6d1
oblasti	oblast	k1gFnSc6
</s>
<s>
V	v	k7c6
průmyslové	průmyslový	k2eAgFnSc6d1
metrologii	metrologie	k1gFnSc6
zajišťuje	zajišťovat	k5eAaImIp3nS
náležité	náležitý	k2eAgNnSc4d1
fungování	fungování	k1gNnSc4
měřidel	měřidlo	k1gNnPc2
používaných	používaný	k2eAgNnPc2d1
v	v	k7c6
průmyslu	průmysl	k1gInSc6
a	a	k8xC
ve	v	k7c6
výrobních	výrobní	k2eAgInPc6d1
a	a	k8xC
zkušebních	zkušební	k2eAgInPc6d1
procesech	proces	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
provádí	provádět	k5eAaImIp3nS
kalibraci	kalibrace	k1gFnSc4
měřidel	měřidlo	k1gNnPc2
</s>
<s>
provádí	provádět	k5eAaImIp3nS
školení	školení	k1gNnSc1
pracovníků	pracovník	k1gMnPc2
metrologie	metrologie	k1gFnSc2
a	a	k8xC
vystavuje	vystavovat	k5eAaImIp3nS
osvědčení	osvědčení	k1gNnSc4
o	o	k7c6
odborné	odborný	k2eAgFnSc6d1
způsobilosti	způsobilost	k1gFnSc6
</s>
<s>
poskytuje	poskytovat	k5eAaImIp3nS
metrologické	metrologický	k2eAgFnPc4d1
expertízy	expertíza	k1gFnPc4
a	a	k8xC
konzultace	konzultace	k1gFnPc4
pro	pro	k7c4
metrologická	metrologický	k2eAgNnPc4d1
pracoviště	pracoviště	k1gNnPc4
</s>
<s>
zabezpečuje	zabezpečovat	k5eAaImIp3nS
systém	systém	k1gInSc1
vědeckotechnických	vědeckotechnický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
metrologie	metrologie	k1gFnSc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Metrologie	metrologie	k1gFnSc1
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
cmi	cmi	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20040203012	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
9371	#num#	k4
1864	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
137681280	#num#	k4
</s>
