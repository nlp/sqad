<s>
Norsko	Norsko	k1gNnSc1	Norsko
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
Norvežsko	Norvežsko	k1gNnSc1	Norvežsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
jazykové	jazykový	k2eAgFnSc6d1	jazyková
verzi	verze	k1gFnSc6	verze
bokmå	bokmå	k?	bokmå
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
Kongeriket	Kongeriketa	k1gFnPc2	Kongeriketa
Norge	Norge	k1gFnPc2	Norge
(	(	kIx(	(
<g/>
Norské	norský	k2eAgNnSc1d1	norské
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
nynorsk	nynorsk	k1gInSc1	nynorsk
Kongeriket	Kongeriketa	k1gFnPc2	Kongeriketa
Noreg	Norega	k1gFnPc2	Norega
<g/>
,	,	kIx,	,
v	v	k7c6	v
řeči	řeč	k1gFnSc6	řeč
národnostní	národnostní	k2eAgFnSc2d1	národnostní
menšiny	menšina	k1gFnSc2	menšina
Sámů	Sámo	k1gMnPc2	Sámo
Norka	Norka	k1gFnSc1	Norka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
.	.	kIx.	.
</s>
