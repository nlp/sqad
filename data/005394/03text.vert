<s>
Conquista	conquista	k1gFnSc1	conquista
[	[	kIx(	[
<g/>
konkista	konkista	k1gMnSc1	konkista
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
španělské	španělský	k2eAgNnSc1d1	španělské
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
dobývání	dobývání	k1gNnSc4	dobývání
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
i	i	k9	i
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
dobývání	dobývání	k1gNnSc1	dobývání
určitého	určitý	k2eAgNnSc2d1	určité
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
myšleno	myslet	k5eAaImNgNnS	myslet
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
vojenském	vojenský	k2eAgInSc6d1	vojenský
i	i	k8xC	i
náboženském	náboženský	k2eAgMnSc6d1	náboženský
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bylo	být	k5eAaImAgNnS	být
domorodé	domorodý	k2eAgNnSc1d1	domorodé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
násilím	násilí	k1gNnSc7	násilí
nuceno	nutit	k5eAaImNgNnS	nutit
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgNnSc2	svůj
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
přejít	přejít	k5eAaPmF	přejít
na	na	k7c4	na
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
conquisty	conquista	k1gFnPc4	conquista
patří	patřit	k5eAaImIp3nP	patřit
dobývání	dobývání	k1gNnSc3	dobývání
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
dobývání	dobývání	k1gNnSc4	dobývání
Aztécké	aztécký	k2eAgFnSc2d1	aztécká
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
dobývání	dobývání	k1gNnSc4	dobývání
Říše	říš	k1gFnSc2	říš
Inků	Ink	k1gMnPc2	Ink
a	a	k8xC	a
dobývání	dobývání	k1gNnSc4	dobývání
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
-	-	kIx~	-
reconquista	reconquista	k1gMnSc1	reconquista
<g/>
.	.	kIx.	.
</s>
<s>
Zvratovým	zvratový	k2eAgInSc7d1	zvratový
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
španělské	španělský	k2eAgNnSc4d1	španělské
dobyvatele	dobyvatel	k1gMnSc4	dobyvatel
rok	rok	k1gInSc4	rok
1492	[number]	k4	1492
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skončilo	skončit	k5eAaPmAgNnS	skončit
dobývání	dobývání	k1gNnSc4	dobývání
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
vytlačením	vytlačení	k1gNnSc7	vytlačení
Arabů	Arab	k1gMnPc2	Arab
a	a	k8xC	a
s	s	k7c7	s
objevením	objevení	k1gNnSc7	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
Kryštofem	Kryštof	k1gMnSc7	Kryštof
Kolumbem	Kolumbus	k1gMnSc7	Kolumbus
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
dobývání	dobývání	k1gNnSc4	dobývání
tohoto	tento	k3xDgInSc2	tento
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc4	označení
conquista	conquista	k1gFnSc1	conquista
a	a	k8xC	a
conquistador	conquistador	k1gMnSc1	conquistador
se	se	k3xPyFc4	se
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
jen	jen	k9	jen
na	na	k7c4	na
španělské	španělský	k2eAgMnPc4d1	španělský
dobyvatele	dobyvatel	k1gMnPc4	dobyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
ostatní	ostatní	k2eAgInPc4d1	ostatní
národy	národ	k1gInPc4	národ
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
uskutečňovaly	uskutečňovat	k5eAaImAgFnP	uskutečňovat
objevné	objevný	k2eAgFnPc1d1	objevná
a	a	k8xC	a
dobyvatelské	dobyvatelský	k2eAgFnPc1d1	dobyvatelská
cesty	cesta	k1gFnPc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Hernán	Hernán	k2eAgInSc1d1	Hernán
Cortés	Cortés	k1gInSc1	Cortés
–	–	k?	–
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
Mexika	Mexiko	k1gNnSc2	Mexiko
Francisco	Francisco	k1gMnSc1	Francisco
Vásquez	Vásquez	k1gMnSc1	Vásquez
de	de	k?	de
Coronado	Coronada	k1gFnSc5	Coronada
–	–	k?	–
podnikl	podniknout	k5eAaPmAgMnS	podniknout
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
Juan	Juan	k1gMnSc1	Juan
Ponce	Ponce	k1gMnSc1	Ponce
de	de	k?	de
León	León	k1gMnSc1	León
–	–	k?	–
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
Portorika	Portorico	k1gNnSc2	Portorico
a	a	k8xC	a
objevitel	objevitel	k1gMnSc1	objevitel
Floridy	Florida	k1gFnSc2	Florida
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Orellana	Orellana	k1gFnSc1	Orellana
–	–	k?	–
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
splul	splout	k5eAaPmAgMnS	splout
Amazonku	Amazonka	k1gFnSc4	Amazonka
Hernando	Hernanda	k1gFnSc5	Hernanda
de	de	k?	de
Soto	Soto	k1gMnSc1	Soto
–	–	k?	–
podnikl	podniknout	k5eAaPmAgMnS	podniknout
dobyvatelskou	dobyvatelský	k2eAgFnSc4d1	dobyvatelská
výpravu	výprava	k1gFnSc4	výprava
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
Francisco	Francisco	k6eAd1	Francisco
Pizarro	Pizarro	k1gNnSc1	Pizarro
–	–	k?	–
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
Peru	Peru	k1gNnSc1	Peru
<g />
.	.	kIx.	.
</s>
<s>
Diego	Diego	k6eAd1	Diego
de	de	k?	de
Almagro	Almagro	k1gNnSc4	Almagro
–	–	k?	–
podnikl	podniknout	k5eAaPmAgMnS	podniknout
dobyvatelskou	dobyvatelský	k2eAgFnSc4d1	dobyvatelská
cestu	cesta	k1gFnSc4	cesta
přes	přes	k7c4	přes
Andy	Anda	k1gFnPc4	Anda
a	a	k8xC	a
poušť	poušť	k1gFnSc4	poušť
Atacama	Atacamum	k1gNnSc2	Atacamum
Gonzalo	Gonzalo	k1gFnPc2	Gonzalo
Jimenéz	Jimenéza	k1gFnPc2	Jimenéza
de	de	k?	de
Quesada	Quesada	k1gFnSc1	Quesada
–	–	k?	–
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Orinoko	Orinoko	k1gNnSc4	Orinoko
najít	najít	k5eAaPmF	najít
bajné	bajné	k1gNnSc4	bajné
Eldorado	Eldorada	k1gFnSc5	Eldorada
Domingo	Domingo	k1gMnSc1	Domingo
Martínez	Martíneza	k1gFnPc2	Martíneza
de	de	k?	de
Irala	Irala	k1gMnSc1	Irala
–	–	k?	–
založil	založit	k5eAaPmAgMnS	založit
španělské	španělský	k2eAgNnSc4d1	španělské
panství	panství	k1gNnSc4	panství
v	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
Pedro	Pedro	k1gNnSc1	Pedro
de	de	k?	de
Valdivia	Valdivius	k1gMnSc2	Valdivius
–	–	k?	–
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
Chile	Chile	k1gNnSc2	Chile
Miguel	Miguel	k1gMnSc1	Miguel
López	López	k1gMnSc1	López
de	de	k?	de
Legazpi	Legazp	k1gFnSc2	Legazp
–	–	k?	–
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
Filipín	Filipíny	k1gFnPc2	Filipíny
Cabeza	Cabeza	k1gFnSc1	Cabeza
de	de	k?	de
Vaca	Vac	k2eAgMnSc4d1	Vac
–	–	k?	–
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
Texas	Texas	k1gInSc1	Texas
a	a	k8xC	a
jih	jih	k1gInSc1	jih
Brazílie	Brazílie	k1gFnSc2	Brazílie
</s>
