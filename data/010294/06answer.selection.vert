<s>
Flemingovo	Flemingův	k2eAgNnSc1d1	Flemingovo
pravidlo	pravidlo	k1gNnSc1	pravidlo
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
používáme	používat	k5eAaImIp1nP	používat
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
směru	směr	k1gInSc2	směr
magnetické	magnetický	k2eAgFnSc2d1	magnetická
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
vodič	vodič	k1gInSc4	vodič
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
