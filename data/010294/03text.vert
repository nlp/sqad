<p>
<s>
Flemingovo	Flemingův	k2eAgNnSc1d1	Flemingovo
pravidlo	pravidlo	k1gNnSc1	pravidlo
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
používáme	používat	k5eAaImIp1nP	používat
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
směru	směr	k1gInSc2	směr
magnetické	magnetický	k2eAgFnSc2d1	magnetická
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
vodič	vodič	k1gInSc4	vodič
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c4	po
J.	J.	kA	J.
A.	A.	kA	A.
Flemingovi	Flemingův	k2eAgMnPc1d1	Flemingův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Znění	znění	k1gNnPc4	znění
Flemingova	Flemingův	k2eAgNnPc4d1	Flemingovo
pravidla	pravidlo	k1gNnPc4	pravidlo
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Položíme	položit	k5eAaPmIp1nP	položit
<g/>
-li	i	k?	-li
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
dlaň	dlaň	k1gFnSc4	dlaň
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
na	na	k7c4	na
vodič	vodič	k1gInSc4	vodič
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
protéká	protékat	k5eAaImIp3nS	protékat
proud	proud	k1gInSc1	proud
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
prsty	prst	k1gInPc1	prst
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
směr	směr	k1gInSc4	směr
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
indukční	indukční	k2eAgFnPc1d1	indukční
čáry	čára	k1gFnPc1	čára
vstupovaly	vstupovat	k5eAaImAgFnP	vstupovat
do	do	k7c2	do
dlaně	dlaň	k1gFnSc2	dlaň
<g/>
,	,	kIx,	,
odtažený	odtažený	k2eAgInSc1d1	odtažený
palec	palec	k1gInSc1	palec
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
směr	směr	k1gInSc4	směr
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
působí	působit	k5eAaImIp3nS	působit
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
na	na	k7c4	na
vodič	vodič	k1gInSc4	vodič
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
prsty	prst	k1gInPc1	prst
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
směr	směr	k1gInSc4	směr
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
indukční	indukční	k2eAgFnSc2d1	indukční
čáry	čára	k1gFnSc2	čára
vnějšího	vnější	k2eAgNnSc2d1	vnější
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
dlaně	dlaň	k1gFnSc2	dlaň
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
palec	palec	k1gInSc1	palec
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
směr	směr	k1gInSc4	směr
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
působí	působit	k5eAaImIp3nS	působit
vnější	vnější	k2eAgNnSc1d1	vnější
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
na	na	k7c4	na
vodič	vodič	k1gInSc4	vodič
s	s	k7c7	s
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
zaměňuje	zaměňovat	k5eAaImIp3nS	zaměňovat
jako	jako	k8xC	jako
Flemingovo	Flemingův	k2eAgNnSc1d1	Flemingovo
pravidlo	pravidlo	k1gNnSc1	pravidlo
"	"	kIx"	"
<g/>
levé	levý	k2eAgFnSc2d1	levá
<g/>
"	"	kIx"	"
ruky	ruka	k1gFnSc2	ruka
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Ampérův	Ampérův	k2eAgInSc1d1	Ampérův
silový	silový	k2eAgInSc1d1	silový
zákon	zákon	k1gInSc1	zákon
<g/>
#	#	kIx~	#
<g/>
Ampérův	Ampérův	k2eAgInSc1d1	Ampérův
zákon	zákon	k1gInSc1	zákon
pro	pro	k7c4	pro
sílu	síla	k1gFnSc4	síla
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
</s>
</p>
