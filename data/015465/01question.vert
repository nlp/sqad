<s>
Jaká	jaký	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
Národní	národní	k2eAgInSc4d1
institut	institut	k1gInSc4
standardů	standard	k1gInPc2
a	a	k8xC
technologie	technologie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
laboratoří	laboratoř	k1gFnSc7
měřících	měřící	k2eAgInPc2d1
standardů	standard	k1gInPc2
při	při	k7c6
ministerstvu	ministerstvo	k1gNnSc6
obchodu	obchod	k1gInSc2
USA	USA	kA
<g/>
?	?	kIx.
</s>