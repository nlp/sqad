<s>
Vlasovec	vlasovec	k1gMnSc1	vlasovec
oční	oční	k2eAgMnSc1d1	oční
(	(	kIx(	(
<g/>
Loa	Loa	k1gMnSc1	Loa
loa	loa	k?	loa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
hlístice	hlístice	k1gFnSc2	hlístice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
parazituje	parazitovat	k5eAaImIp3nS	parazitovat
ve	v	k7c6	v
spojivkovém	spojivkový	k2eAgInSc6d1	spojivkový
vaku	vak	k1gInSc6	vak
mezi	mezi	k7c7	mezi
spojivkou	spojivka	k1gFnSc7	spojivka
a	a	k8xC	a
bělmem	bělmo	k1gNnSc7	bělmo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
svědění	svědění	k1gNnSc2	svědění
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
zánětu	zánět	k1gInSc2	zánět
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vlasovec	vlasovec	k1gMnSc1	vlasovec
mízní	mízní	k2eAgMnSc1d1	mízní
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
zejména	zejména	k9	zejména
bodavým	bodavý	k2eAgInSc7d1	bodavý
hmyzem	hmyz	k1gInSc7	hmyz
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
loalózu	loalóza	k1gFnSc4	loalóza
a	a	k8xC	a
kožní	kožní	k2eAgFnPc4d1	kožní
boule	boule	k1gFnPc4	boule
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kamerunské	kamerunský	k2eAgFnPc1d1	kamerunská
boule	boule	k1gFnPc1	boule
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlasovec	vlasovec	k1gMnSc1	vlasovec
oční	oční	k2eAgMnSc1d1	oční
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
