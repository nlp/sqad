<s>
Sardel	sardel	k1gFnSc1
obecná	obecná	k1gFnSc1
</s>
<s>
Sardel	sardel	k1gFnSc1
obecná	obecná	k1gFnSc1
Sardel	sardel	k1gFnSc1
obecná	obecná	k1gFnSc1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
paprskoploutví	paprskoploutvit	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
Actinopterygii	Actinopterygie	k1gFnSc3
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
bezostní	bezostnit	k5eAaPmIp3nS
(	(	kIx(
<g/>
Clupeiformes	Clupeiformes	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
sardelovití	sardelovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Engraulidae	Engraulidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
sardel	sardel	k1gFnSc1
(	(	kIx(
<g/>
Engraulis	Engraulis	k1gFnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Engraulis	Engraulis	k1gFnSc1
encrasicolus	encrasicolus	k1gMnSc1
<g/>
(	(	kIx(
<g/>
L.	L.	kA
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sardel	sardel	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Engraulis	Engraulis	k1gFnSc1
encrasicolus	encrasicolus	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mořská	mořský	k2eAgFnSc1d1
rybka	rybka	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
sardelovitých	sardelovitý	k2eAgFnPc2d1
žijicí	žijice	k1gFnPc2
v	v	k7c6
Atlantiku	Atlantik	k1gInSc6
<g/>
,	,	kIx,
Středozemním	středozemní	k2eAgNnSc6d1
a	a	k8xC
Černém	černý	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Široká	široký	k2eAgNnPc1d1
ústa	ústa	k1gNnPc1
sardele	sardel	k1gFnSc2
obecné	obecný	k2eAgFnSc2d1
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
dorůstají	dorůstat	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
délky	délka	k1gFnPc1
10	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
někteří	některý	k3yIgMnPc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
mohou	moct	k5eAaImIp3nP
dosahovat	dosahovat	k5eAaImF
délky	délka	k1gFnSc2
až	až	k9
20	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Mají	mít	k5eAaImIp3nP
protáhlé	protáhlý	k2eAgFnPc1d1
<g/>
,	,	kIx,
úzké	úzký	k2eAgNnSc1d1
a	a	k8xC
zploštělé	zploštělý	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc1
boky	bok	k1gInPc1
jsou	být	k5eAaImIp3nP
pokryty	pokrýt	k5eAaPmNgInP
cykloidními	cykloidní	k2eAgFnPc7d1
šupinami	šupina	k1gFnPc7
stříbrné	stříbrný	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
hřbet	hřbet	k1gInSc4
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
modrozelený	modrozelený	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Podél	podél	k7c2
těla	tělo	k1gNnSc2
se	se	k3xPyFc4
někdy	někdy	k6eAd1
táhne	táhnout	k5eAaImIp3nS
pruh	pruh	k1gInSc1
kovové	kovový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Horní	horní	k2eAgFnSc1d1
čelist	čelist	k1gFnSc1
je	být	k5eAaImIp3nS
špičatá	špičatý	k2eAgFnSc1d1
a	a	k8xC
podlouhlá	podlouhlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
zcela	zcela	k6eAd1
přesahuje	přesahovat	k5eAaImIp3nS
úzkou	úzký	k2eAgFnSc4d1
čelist	čelist	k1gFnSc4
dolní	dolní	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ústa	ústa	k1gNnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c4
velikost	velikost	k1gFnSc4
této	tento	k3xDgFnSc2
ryby	ryba	k1gFnSc2
poměrně	poměrně	k6eAd1
velká	velký	k2eAgNnPc1d1
a	a	k8xC
široká	široký	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
ploutevních	ploutevní	k2eAgInPc2d1
paprsků	paprsek	k1gInPc2
na	na	k7c6
hřbetní	hřbetní	k2eAgFnSc6d1
ploutvi	ploutev	k1gFnSc6
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
15	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
,	,	kIx,
u	u	k7c2
řitní	řitní	k2eAgFnSc2d1
20	#num#	k4
<g/>
–	–	k?
<g/>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
potravou	potrava	k1gFnSc7
těchto	tento	k3xDgFnPc2
ryb	ryba	k1gFnPc2
jsou	být	k5eAaImIp3nP
planktonoví	planktonový	k2eAgMnPc1d1
korýši	korýš	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Migrace	migrace	k1gFnSc1
sardelí	sardel	k1gFnPc2
je	být	k5eAaImIp3nS
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
dobře	dobře	k6eAd1
odhadnout	odhadnout	k5eAaPmF
právě	právě	k9
díky	díky	k7c3
planktonu	plankton	k1gInSc3
a	a	k8xC
závisí	záviset	k5eAaImIp3nS
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
množství	množství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Sardel	sardel	k1gFnSc1
obecná	obecná	k1gFnSc1
je	být	k5eAaImIp3nS
významná	významný	k2eAgFnSc1d1
hospodářská	hospodářský	k2eAgFnSc1d1
ryba	ryba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kuchyni	kuchyně	k1gFnSc6
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
delikatesa	delikatesa	k1gFnSc1
pod	pod	k7c7
názvem	název	k1gInSc7
ančovičky	ančovička	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
rostlinném	rostlinný	k2eAgInSc6d1
oleji	olej	k1gInSc6
ve	v	k7c6
formě	forma	k1gFnSc6
filetů	filet	k1gInPc2
či	či	k8xC
oček	očko	k1gNnPc2
s	s	k7c7
kapary	kapary	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obchodní	obchodní	k2eAgFnSc6d1
síti	síť	k1gFnSc6
jsou	být	k5eAaImIp3nP
nejčastěji	často	k6eAd3
prodávány	prodávat	k5eAaImNgInP
pod	pod	k7c7
názvem	název	k1gInSc7
sardelová	sardelový	k2eAgNnPc4d1
očka	očko	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Sardel	sardel	k1gFnSc1
obecná	obecná	k1gFnSc1
|	|	kIx~
Moulík	Moulík	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.moulik.cz	www.moulik.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Sardel	sardel	k1gFnSc1
obecná	obecná	k1gFnSc1
Engraulis	Engraulis	k1gFnSc1
encrasicolus	encrasicolus	k1gInSc4
European	European	k1gInSc1
anchovy	anchova	k1gFnSc2
-	-	kIx~
Mořské	mořský	k2eAgFnPc1d1
ryby	ryba	k1gFnPc1
<g/>
.	.	kIx.
zivazeme	zivazeme	k5eAaPmIp1nP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Е	Е	k?
а	а	k?
<g/>
,	,	kIx,
х	х	k?
(	(	kIx(
<g/>
Engraulis	Engraulis	k1gFnSc1
encrasicholus	encrasicholus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.cnshb.ru	www.cnshb.ra	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FAO	FAO	kA
Fisheries	Fisheries	k1gMnSc1
&	&	k?
Aquaculture	Aquacultur	k1gMnSc5
FAO	FAO	kA
FishFinder	FishFinder	k1gInSc1
<g/>
,	,	kIx,
publications	publications	k1gInSc1
-	-	kIx~
Catalogues	Catalogues	k1gInSc1
<g/>
.	.	kIx.
www.fao.org	www.fao.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Е	Е	k?
а	а	k?
и	и	k?
х	х	k?
<g/>
.	.	kIx.
myanapa	myanapa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MUUS	MUUS	kA
<g/>
,	,	kIx,
Bent	Bent	k1gMnSc1
J.	J.	kA
Die	Die	k1gMnSc1
Meeresfische	Meeresfisch	k1gInSc2
Europas	Europas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
Nordsee	Nordsee	k1gFnSc1
<g/>
,	,	kIx,
Ostsee	Ostsee	k1gFnSc1
und	und	k?
Atlantik	Atlantik	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Franckh-Kosmos	Franckh-Kosmos	k1gMnSc1
Verlag	Verlag	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
440	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7804	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
sardel	sardel	k1gFnSc1
obecná	obecný	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Engraulis	Engraulis	k1gInSc1
encrasicolus	encrasicolus	k1gInSc1
(	(	kIx(
<g/>
sardel	sardel	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
