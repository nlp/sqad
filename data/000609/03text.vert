<s>
Josefodol	Josefodol	k1gInSc1	Josefodol
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Josefsthal	Josefsthal	k1gMnSc1	Josefsthal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
Světlá	světlý	k2eAgFnSc1d1	světlá
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
od	od	k7c2	od
Světlé	světlý	k2eAgFnSc2d1	světlá
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
34	[number]	k4	34
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
žilo	žít	k5eAaImAgNnS	žít
131	[number]	k4	131
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
tudy	tudy	k6eAd1	tudy
říčka	říčka	k1gFnSc1	říčka
Sázavka	Sázavka	k1gFnSc1	Sázavka
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
levostranný	levostranný	k2eAgInSc1d1	levostranný
přítok	přítok	k1gInSc1	přítok
Zbožský	Zbožský	k2eAgInSc4d1	Zbožský
potok	potok	k1gInSc4	potok
<g/>
.	.	kIx.	.
</s>
<s>
Josefodol	Josefodol	k1gInSc1	Josefodol
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Horní	horní	k2eAgFnSc2d1	horní
Bohušice	Bohušice	k1gFnSc2	Bohušice
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
1,6	[number]	k4	1,6
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nP	procházet
zde	zde	k6eAd1	zde
silnice	silnice	k1gFnPc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
347	[number]	k4	347
a	a	k8xC	a
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
zastávka	zastávka	k1gFnSc1	zastávka
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c4	nad
Sázavou-Josefodol	Sázavou-Josefodol	k1gInSc4	Sázavou-Josefodol
(	(	kIx(	(
<g/>
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
též	též	k9	též
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Světlá	světlý	k2eAgFnSc1d1	světlá
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
katastru	katastr	k1gInSc2	katastr
Horní	horní	k2eAgFnSc1d1	horní
Bohušice	Bohušice	k1gFnSc1	Bohušice
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
