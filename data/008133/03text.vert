<s>
Tatarský	tatarský	k2eAgInSc1d1	tatarský
biftek	biftek	k1gInSc1	biftek
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
tatarák	tatarák	k1gInSc1	tatarák
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jídlo	jídlo	k1gNnSc4	jídlo
ze	z	k7c2	z
syrového	syrový	k2eAgNnSc2d1	syrové
hovězího	hovězí	k1gNnSc2	hovězí
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
dobrého	dobrý	k2eAgInSc2d1	dobrý
tatarského	tatarský	k2eAgInSc2d1	tatarský
bifteku	biftek	k1gInSc2	biftek
je	být	k5eAaImIp3nS	být
maso	maso	k1gNnSc4	maso
z	z	k7c2	z
hovězí	hovězí	k2eAgFnSc2d1	hovězí
svíčkové	svíčková	k1gFnSc2	svíčková
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
najemno	najemno	k6eAd1	najemno
naškrábe	naškrábat	k5eAaBmIp3nS	naškrábat
špičkou	špička	k1gFnSc7	špička
ostrého	ostrý	k2eAgInSc2d1	ostrý
nože	nůž	k1gInSc2	nůž
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
namele	namlít	k5eAaBmIp3nS	namlít
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
nakrájenou	nakrájený	k2eAgFnSc7d1	nakrájená
cibulí	cibule	k1gFnSc7	cibule
<g/>
,	,	kIx,	,
hořčicí	hořčice	k1gFnSc7	hořčice
<g/>
,	,	kIx,	,
solí	sůl	k1gFnSc7	sůl
<g/>
,	,	kIx,	,
pepřem	pepř	k1gInSc7	pepř
a	a	k8xC	a
worcestrovou	worcestrový	k2eAgFnSc7d1	worcestrový
omáčkou	omáčka	k1gFnSc7	omáčka
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
chuti	chuť	k1gFnSc2	chuť
se	se	k3xPyFc4	se
často	často	k6eAd1	často
přidává	přidávat	k5eAaImIp3nS	přidávat
sladká	sladký	k2eAgFnSc1d1	sladká
paprika	paprika	k1gFnSc1	paprika
<g/>
,	,	kIx,	,
chilli	chilli	k1gNnSc1	chilli
<g/>
,	,	kIx,	,
kečup	kečup	k1gInSc1	kečup
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
koření	koření	k1gNnPc1	koření
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
završí	završit	k5eAaPmIp3nS	završit
syrovým	syrový	k2eAgInSc7d1	syrový
žloutkem	žloutek	k1gInSc7	žloutek
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
ingredience	ingredience	k1gFnPc1	ingredience
se	se	k3xPyFc4	se
smíchají	smíchat	k5eAaPmIp3nP	smíchat
a	a	k8xC	a
podávají	podávat	k5eAaImIp3nP	podávat
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
topince	topinka	k1gFnSc6	topinka
potřené	potřený	k2eAgFnSc2d1	potřená
česnekem	česnek	k1gInSc7	česnek
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
složení	složení	k1gNnSc4	složení
tatarského	tatarský	k2eAgInSc2d1	tatarský
bifteku	biftek	k1gInSc2	biftek
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
nejen	nejen	k6eAd1	nejen
dle	dle	k7c2	dle
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
dle	dle	k7c2	dle
každého	každý	k3xTgMnSc2	každý
strávníka	strávník	k1gMnSc2	strávník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
ochucovadly	ochucovadlo	k1gNnPc7	ochucovadlo
jako	jako	k8xC	jako
například	například	k6eAd1	například
s	s	k7c7	s
ančovičkami	ančovička	k1gFnPc7	ančovička
<g/>
,	,	kIx,	,
kaparami	kapara	k1gFnPc7	kapara
<g/>
,	,	kIx,	,
tabaskem	tabasek	k1gInSc7	tabasek
<g/>
,	,	kIx,	,
koňakem	koňak	k1gInSc7	koňak
nebo	nebo	k8xC	nebo
nakládanými	nakládaný	k2eAgFnPc7d1	nakládaná
okurkami	okurka	k1gFnPc7	okurka
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
klasické	klasický	k2eAgFnSc2d1	klasická
české	český	k2eAgFnSc2d1	Česká
topinky	topinka	k1gFnSc2	topinka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
spíše	spíše	k9	spíše
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
toastovaným	toastovaný	k2eAgInSc7d1	toastovaný
světlým	světlý	k2eAgInSc7d1	světlý
chlebem	chléb	k1gInSc7	chléb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
k	k	k7c3	k
tatarskému	tatarský	k2eAgInSc3d1	tatarský
bifteku	biftek	k1gInSc3	biftek
podávají	podávat	k5eAaImIp3nP	podávat
smažené	smažený	k2eAgInPc1d1	smažený
bramborové	bramborový	k2eAgInPc1d1	bramborový
hranolky	hranolek	k1gInPc1	hranolek
<g/>
.	.	kIx.	.
</s>
<s>
Tatarský	tatarský	k2eAgInSc1d1	tatarský
biftek	biftek	k1gInSc1	biftek
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
podává	podávat	k5eAaImIp3nS	podávat
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
oválném	oválný	k2eAgInSc6d1	oválný
talíři	talíř	k1gInSc6	talíř
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
dřevěném	dřevěný	k2eAgNnSc6d1	dřevěné
prkénku	prkénko	k1gNnSc6	prkénko
<g/>
.	.	kIx.	.
</s>
<s>
Suroviny	surovina	k1gFnPc1	surovina
na	na	k7c6	na
dochucení	dochucení	k1gNnSc6	dochucení
se	se	k3xPyFc4	se
podávají	podávat	k5eAaImIp3nP	podávat
buď	buď	k8xC	buď
okolo	okolo	k7c2	okolo
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
miskách	miska	k1gFnPc6	miska
po	po	k7c6	po
určitém	určitý	k2eAgNnSc6d1	určité
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Topinky	topinka	k1gFnPc1	topinka
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
křupavé	křupavý	k2eAgFnPc1d1	křupavá
a	a	k8xC	a
čerstvě	čerstvě	k6eAd1	čerstvě
usmažené	usmažený	k2eAgNnSc1d1	usmažené
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
tatarák	tatarák	k1gInSc1	tatarák
již	již	k6eAd1	již
připraven	připravit	k5eAaPmNgInS	připravit
"	"	kIx"	"
<g/>
a	a	k8xC	a
la	la	k1gNnSc1	la
chef	chef	k1gMnSc1	chef
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dle	dle	k7c2	dle
chuti	chuť	k1gFnSc2	chuť
šéfkuchaře	šéfkuchař	k1gMnSc2	šéfkuchař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
končinách	končina	k1gFnPc6	končina
se	se	k3xPyFc4	se
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
úprava	úprava	k1gFnSc1	úprava
nesetkává	setkávat	k5eNaImIp3nS	setkávat
s	s	k7c7	s
přílišnou	přílišný	k2eAgFnSc7d1	přílišná
oblibou	obliba	k1gFnSc7	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Známými	známý	k2eAgFnPc7d1	známá
obdobami	obdoba	k1gFnPc7	obdoba
tatarského	tatarský	k2eAgInSc2d1	tatarský
bifteku	biftek	k1gInSc2	biftek
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
tatarák	tatarák	k1gInSc4	tatarák
z	z	k7c2	z
tuňáka	tuňák	k1gMnSc2	tuňák
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
chutí	chuť	k1gFnSc7	chuť
poněkud	poněkud	k6eAd1	poněkud
podobná	podobný	k2eAgFnSc1d1	podobná
hovězímu	hovězí	k2eAgNnSc3d1	hovězí
masu	masa	k1gFnSc4	masa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lososa	losos	k1gMnSc4	losos
<g/>
,	,	kIx,	,
kapra	kapr	k1gMnSc4	kapr
nebo	nebo	k8xC	nebo
vegetariánský	vegetariánský	k2eAgInSc4d1	vegetariánský
rajčatový	rajčatový	k2eAgInSc4d1	rajčatový
tatarák	tatarák	k1gInSc4	tatarák
(	(	kIx(	(
<g/>
na	na	k7c4	na
jemno	jemno	k1gNnSc4	jemno
nakrájená	nakrájený	k2eAgFnSc1d1	nakrájená
rajská	rajský	k2eAgFnSc1d1	rajská
<g/>
,	,	kIx,	,
bazalka	bazalka	k1gFnSc1	bazalka
<g/>
,	,	kIx,	,
olivový	olivový	k2eAgInSc1d1	olivový
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
cibule	cibule	k1gFnSc1	cibule
a	a	k8xC	a
citronová	citronový	k2eAgFnSc1d1	citronová
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
tepelně	tepelně	k6eAd1	tepelně
neupravené	upravený	k2eNgNnSc4d1	neupravené
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
vejce	vejce	k1gNnSc4	vejce
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
riziko	riziko	k1gNnSc4	riziko
přenosu	přenos	k1gInSc2	přenos
některých	některý	k3yIgNnPc2	některý
parazitárních	parazitární	k2eAgNnPc2d1	parazitární
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
trichinelóza	trichinelóza	k1gFnSc1	trichinelóza
<g/>
,	,	kIx,	,
cysticerkóza	cysticerkóza	k1gFnSc1	cysticerkóza
nebo	nebo	k8xC	nebo
echinokokóza	echinokokóza	k1gFnSc1	echinokokóza
<g/>
.	.	kIx.	.
biftek	biftek	k1gInSc4	biftek
mleté	mletý	k2eAgNnSc4d1	mleté
maso	maso	k1gNnSc4	maso
svíčková	svíčkový	k2eAgFnSc1d1	svíčková
topinka	topinka	k1gFnSc1	topinka
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tatarský	tatarský	k2eAgInSc4d1	tatarský
biftek	biftek	k1gInSc4	biftek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Spor	spora	k1gFnPc2	spora
o	o	k7c4	o
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
tatarák	tatarák	k1gInSc4	tatarák
nemůže	moct	k5eNaImIp3nS	moct
mít	mít	k5eAaImF	mít
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
chutná	chutnat	k5eAaImIp3nS	chutnat
belgická	belgický	k2eAgFnSc1d1	belgická
variace	variace	k1gFnSc1	variace
Recepty	recept	k1gInPc4	recept
na	na	k7c4	na
tatarský	tatarský	k2eAgInSc4d1	tatarský
biftek	biftek	k1gInSc4	biftek
podle	podle	k7c2	podle
někdejších	někdejší	k2eAgFnPc2d1	někdejší
ČSN	ČSN	kA	ČSN
norem	norma	k1gFnPc2	norma
ČSN	ČSN	kA	ČSN
11606	[number]	k4	11606
a	a	k8xC	a
11606	[number]	k4	11606
<g/>
/	/	kIx~	/
<g/>
A	a	k8xC	a
</s>
