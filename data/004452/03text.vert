<s>
Davis	Davis	k1gInSc1	Davis
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Davisův	Davisův	k2eAgInSc1d1	Davisův
pohár	pohár	k1gInSc1	pohár
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tenisová	tenisový	k2eAgFnSc1d1	tenisová
soutěž	soutěž	k1gFnSc1	soutěž
mužských	mužský	k2eAgNnPc2d1	mužské
reprezentačních	reprezentační	k2eAgNnPc2d1	reprezentační
družstev	družstvo	k1gNnPc2	družstvo
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
každoročně	každoročně	k6eAd1	každoročně
hraná	hraný	k2eAgFnSc1d1	hraná
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
mužském	mužský	k2eAgInSc6d1	mužský
sportu	sport	k1gInSc6	sport
<g/>
,	,	kIx,	,
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
tenisovou	tenisový	k2eAgFnSc7d1	tenisová
federací	federace	k1gFnSc7	federace
(	(	kIx(	(
<g/>
ITF	ITF	kA	ITF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
zúčastnily	zúčastnit	k5eAaPmAgInP	zúčastnit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvícekrát	Nejvícekrát	k6eAd1	Nejvícekrát
triumfovaly	triumfovat	k5eAaBmAgInP	triumfovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
32	[number]	k4	32
ročníků	ročník	k1gInPc2	ročník
a	a	k8xC	a
29	[number]	k4	29
<g/>
krát	krát	k6eAd1	krát
odešly	odejít	k5eAaPmAgInP	odejít
jako	jako	k8xC	jako
poražený	poražený	k2eAgMnSc1d1	poražený
finalista	finalista	k1gMnSc1	finalista
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
probíhá	probíhat	k5eAaImIp3nS	probíhat
106	[number]	k4	106
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
třicet	třicet	k4xCc4	třicet
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Obhájcem	obhájce	k1gMnSc7	obhájce
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
záhřebském	záhřebský	k2eAgNnSc6d1	Záhřebské
finále	finále	k1gNnSc6	finále
porazila	porazit	k5eAaPmAgFnS	porazit
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
jihoamerickým	jihoamerický	k2eAgMnSc7d1	jihoamerický
šampionem	šampion	k1gMnSc7	šampion
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Ženskou	ženský	k2eAgFnSc7d1	ženská
obdobou	obdoba	k1gFnSc7	obdoba
je	být	k5eAaImIp3nS	být
týmová	týmový	k2eAgFnSc1d1	týmová
soutěž	soutěž	k1gFnSc1	soutěž
Fed	Fed	k1gMnSc1	Fed
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
50	[number]	k4	50
<g/>
.	.	kIx.	.
jubilea	jubileum	k1gNnSc2	jubileum
založení	založení	k1gNnSc2	založení
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
tenisové	tenisový	k2eAgFnSc2d1	tenisová
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
a	a	k8xC	a
méně	málo	k6eAd2	málo
prestižní	prestižní	k2eAgInSc1d1	prestižní
je	být	k5eAaImIp3nS	být
turnaj	turnaj	k1gInSc1	turnaj
smíšených	smíšený	k2eAgInPc2d1	smíšený
celků	celek	k1gInPc2	celek
Hopmanův	Hopmanův	k2eAgInSc4d1	Hopmanův
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
Fed	Fed	k1gFnPc6	Fed
Cup	cup	k1gInSc1	cup
a	a	k8xC	a
Davis	Davis	k1gInSc1	Davis
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
kalendářním	kalendářní	k2eAgInSc6d1	kalendářní
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
Česko	Česko	k1gNnSc1	Česko
pak	pak	k6eAd1	pak
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
sezóně	sezóna	k1gFnSc6	sezóna
přidalo	přidat	k5eAaPmAgNnS	přidat
i	i	k9	i
triumf	triumf	k1gInSc4	triumf
na	na	k7c4	na
Hopman	Hopman	k1gMnSc1	Hopman
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historie	historie	k1gFnSc1	historie
Davis	Davis	k1gInSc1	Davis
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
týmového	týmový	k2eAgNnSc2d1	týmové
střetnutí	střetnutí	k1gNnSc2	střetnutí
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
v	v	k7c6	v
tenisovém	tenisový	k2eAgInSc6d1	tenisový
klubu	klub	k1gInSc6	klub
na	na	k7c6	na
Harvardově	Harvardův	k2eAgFnSc6d1	Harvardova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Dwight	Dwight	k1gInSc1	Dwight
F.	F.	kA	F.
Davis	Davis	k1gInSc1	Davis
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
hrací	hrací	k2eAgNnSc4d1	hrací
schéma	schéma	k1gNnSc4	schéma
tohoto	tento	k3xDgNnSc2	tento
střetnutí	střetnutí	k1gNnSc2	střetnutí
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
pohár	pohár	k1gInSc1	pohár
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgInSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
salátová	salátový	k2eAgFnSc1d1	salátová
mísa	mísa	k1gFnSc1	mísa
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
první	první	k4xOgNnSc1	první
utkání	utkání	k1gNnSc1	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
se	se	k3xPyFc4	se
soutěž	soutěž	k1gFnSc1	soutěž
otevřela	otevřít	k5eAaPmAgFnS	otevřít
dalším	další	k2eAgInPc3d1	další
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yQgFnPc7	který
významné	významný	k2eAgInPc4d1	významný
místo	místo	k1gNnSc4	místo
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
společná	společný	k2eAgFnSc1d1	společná
reprezentace	reprezentace	k1gFnSc1	reprezentace
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Australasie	Australasie	k1gFnSc2	Australasie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Davisově	Davisův	k2eAgFnSc6d1	Davisova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
tenisová	tenisový	k2eAgFnSc1d1	tenisová
soutěž	soutěž	k1gFnSc1	soutěž
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
podle	podle	k7c2	podle
zakladatele	zakladatel	k1gMnSc2	zakladatel
na	na	k7c6	na
Davis	Davis	k1gFnSc6	Davis
Cup	cup	k1gInSc4	cup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
také	také	k6eAd1	také
přišla	přijít	k5eAaPmAgFnS	přijít
tzv.	tzv.	kA	tzv.
Hopmanova	Hopmanův	k2eAgFnSc1d1	Hopmanův
éra	éra	k1gFnSc1	éra
<g/>
,	,	kIx,	,
když	když	k8xS	když
Austrálie	Austrálie	k1gFnSc1	Austrálie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
téhož	týž	k3xTgMnSc2	týž
kapitána	kapitán	k1gMnSc2	kapitán
prošla	projít	k5eAaPmAgFnS	projít
dvaadvacetkrát	dvaadvacetkrát	k6eAd1	dvaadvacetkrát
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
15	[number]	k4	15
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
se	se	k3xPyFc4	se
vítězství	vítězství	k1gNnSc1	vítězství
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
včetně	včetně	k7c2	včetně
dočkala	dočkat	k5eAaPmAgFnS	dočkat
ještě	ještě	k9	ještě
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nakonec	nakonec	k6eAd1	nakonec
finále	finále	k1gNnSc1	finále
neuskutečnilo	uskutečnit	k5eNaPmAgNnS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
totiž	totiž	k9	totiž
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
nastoupit	nastoupit	k5eAaPmF	nastoupit
proti	proti	k7c3	proti
Jihoafrické	jihoafrický	k2eAgFnSc3d1	Jihoafrická
republice	republika	k1gFnSc3	republika
kvůli	kvůli	k7c3	kvůli
tamnímu	tamní	k2eAgInSc3d1	tamní
apartheidu	apartheid	k1gInSc3	apartheid
a	a	k8xC	a
o	o	k7c6	o
vítězi	vítěz	k1gMnSc6	vítěz
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
zápasů	zápas	k1gInPc2	zápas
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
začleněn	začleněn	k2eAgInSc1d1	začleněn
tie-break	tiereak	k1gInSc1	tie-break
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
byla	být	k5eAaImAgFnS	být
utvořena	utvořen	k2eAgFnSc1d1	utvořena
šestnáctičlenná	šestnáctičlenný	k2eAgFnSc1d1	šestnáctičlenná
světová	světový	k2eAgFnSc1d1	světová
skupina	skupina	k1gFnSc1	skupina
a	a	k8xC	a
systém	systém	k1gInSc1	systém
podskupin	podskupina	k1gFnPc2	podskupina
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
se	se	k3xPyFc4	se
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
skupině	skupina	k1gFnSc6	skupina
bez	bez	k7c2	bez
sestupu	sestup	k1gInSc2	sestup
udrželo	udržet	k5eAaPmAgNnS	udržet
Československo	Československo	k1gNnSc1	Československo
a	a	k8xC	a
jako	jako	k9	jako
nástupce	nástupce	k1gMnPc4	nástupce
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ji	on	k3xPp3gFnSc4	on
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
naposledy	naposledy	k6eAd1	naposledy
opustilo	opustit	k5eAaPmAgNnS	opustit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
od	od	k7c2	od
zavedení	zavedení	k1gNnSc2	zavedení
světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
chyběly	chybět	k5eAaImAgInP	chybět
ještě	ještě	k6eAd1	ještě
týmy	tým	k1gInPc1	tým
USA	USA	kA	USA
a	a	k8xC	a
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
101	[number]	k4	101
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
a	a	k8xC	a
jubilejní	jubilejní	k2eAgFnSc1d1	jubilejní
100	[number]	k4	100
<g/>
.	.	kIx.	.
finále	finále	k1gNnSc6	finále
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Obhájcem	obhájce	k1gMnSc7	obhájce
titulu	titul	k1gInSc2	titul
bylo	být	k5eAaImAgNnS	být
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
pošesté	pošesté	k4xO	pošesté
za	za	k7c4	za
uplynulých	uplynulý	k2eAgNnPc2d1	uplynulé
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
probojovalo	probojovat	k5eAaPmAgNnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
však	však	k9	však
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
posledním	poslední	k2eAgMnSc6d1	poslední
pátém	pátý	k4xOgInSc6	pátý
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
oba	dva	k4xCgInPc4	dva
týmy	tým	k1gInPc1	tým
vstupovaly	vstupovat	k5eAaImAgInP	vstupovat
za	za	k7c2	za
nerozhodnutého	rozhodnutý	k2eNgInSc2d1	nerozhodnutý
stavu	stav	k1gInSc2	stav
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
dvojka	dvojka	k1gFnSc1	dvojka
Radek	Radek	k1gMnSc1	Radek
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
však	však	k9	však
dokázal	dokázat	k5eAaPmAgMnS	dokázat
porazit	porazit	k5eAaPmF	porazit
světovou	světový	k2eAgFnSc4d1	světová
jedenáctku	jedenáctka	k1gFnSc4	jedenáctka
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
Almagra	Almagr	k1gInSc2	Almagr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
sety	set	k1gInPc4	set
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pravidelném	pravidelný	k2eAgNnSc6d1	pravidelné
každoročním	každoroční	k2eAgNnSc6d1	každoroční
zasedání	zasedání	k1gNnSc6	zasedání
shromáždění	shromáždění	k1gNnSc2	shromáždění
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
tenisové	tenisový	k2eAgFnSc2d1	tenisová
federace	federace	k1gFnSc2	federace
v	v	k7c6	v
chilské	chilský	k2eAgFnSc6d1	chilská
metropoli	metropol	k1gFnSc6	metropol
Santiagu	Santiago	k1gNnSc6	Santiago
padlo	padnout	k5eAaImAgNnS	padnout
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
týkající	týkající	k2eAgMnSc1d1	týkající
se	se	k3xPyFc4	se
koncovky	koncovka	k1gFnSc2	koncovka
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
setu	set	k1gInSc6	set
zaveden	zavést	k5eAaPmNgInS	zavést
tiebreak	tiebreak	k1gInSc1	tiebreak
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Skončil	skončit	k5eAaPmAgInS	skončit
tak	tak	k9	tak
tradiční	tradiční	k2eAgInSc1d1	tradiční
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
k	k	k7c3	k
zisku	zisk	k1gInSc2	zisk
této	tento	k3xDgFnSc2	tento
sady	sada	k1gFnSc2	sada
rozdílu	rozdíl	k1gInSc6	rozdíl
dvou	dva	k4xCgInPc2	dva
gamů	game	k1gInPc2	game
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
omezení	omezení	k1gNnSc1	omezení
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
maratonských	maratonský	k2eAgNnPc2d1	maratonské
utkání	utkání	k1gNnPc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Davis	Davis	k1gInSc1	Davis
Cup	cup	k1gInSc1	cup
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
grandslamů	grandslam	k1gInPc2	grandslam
uplatňoval	uplatňovat	k5eAaImAgInS	uplatňovat
toto	tento	k3xDgNnSc4	tento
pojetí	pojetí	k1gNnSc4	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
týmem	tým	k1gInSc7	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odehrál	odehrát	k5eAaPmAgInS	odehrát
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
všech	všecek	k3xTgInPc2	všecek
105	[number]	k4	105
ročníků	ročník	k1gInPc2	ročník
soutěže	soutěž	k1gFnSc2	soutěž
byla	být	k5eAaImAgFnS	být
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
Davisově	Davisův	k2eAgInSc6d1	Davisův
poháru	pohár	k1gInSc6	pohár
se	se	k3xPyFc4	se
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
bojuje	bojovat	k5eAaImIp3nS	bojovat
v	v	k7c6	v
šestnáctičlenné	šestnáctičlenný	k2eAgFnSc6d1	šestnáctičlenná
světové	světový	k2eAgFnSc6d1	světová
skupině	skupina	k1gFnSc6	skupina
vyřazovacím	vyřazovací	k2eAgInSc7d1	vyřazovací
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zisku	zisk	k1gInSc3	zisk
salátové	salátový	k2eAgFnSc2d1	salátová
mísy	mísa	k1gFnSc2	mísa
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
postupně	postupně	k6eAd1	postupně
vyhrát	vyhrát	k5eAaPmF	vyhrát
čtyři	čtyři	k4xCgInPc4	čtyři
mezistátní	mezistátní	k2eAgInPc4d1	mezistátní
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existují	existovat	k5eAaImIp3nP	existovat
3	[number]	k4	3
regionální	regionální	k2eAgFnPc1d1	regionální
zóny	zóna	k1gFnPc1	zóna
(	(	kIx(	(
<g/>
Americká	americký	k2eAgFnSc1d1	americká
<g/>
,	,	kIx,	,
Evroafrická	Evroafrický	k2eAgFnSc1d1	Evroafrický
a	a	k8xC	a
Asijskooceánská	asijskooceánský	k2eAgFnSc1d1	asijskooceánský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vnitřně	vnitřně	k6eAd1	vnitřně
rozčleněny	rozčlenit	k5eAaPmNgInP	rozčlenit
do	do	k7c2	do
4	[number]	k4	4
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Poražení	poražený	k1gMnPc1	poražený
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
svoji	svůj	k3xOyFgFnSc4	svůj
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
skupině	skupina	k1gFnSc3	skupina
potom	potom	k6eAd1	potom
obhajují	obhajovat	k5eAaImIp3nP	obhajovat
v	v	k7c6	v
baráži	baráž	k1gFnSc6	baráž
proti	proti	k7c3	proti
nejlepším	dobrý	k2eAgInPc3d3	nejlepší
týmům	tým	k1gInPc3	tým
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
úrovní	úroveň	k1gFnSc7	úroveň
regionálních	regionální	k2eAgFnPc2d1	regionální
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc4	utkání
dvou	dva	k4xCgInPc2	dva
týmů	tým	k1gInPc2	tým
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
za	za	k7c4	za
sebou	se	k3xPyFc7	se
jdoucích	jdoucí	k2eAgInPc6d1	jdoucí
dnech	den	k1gInPc6	den
(	(	kIx(	(
<g/>
pátek	pátek	k1gInSc1	pátek
<g/>
,	,	kIx,	,
sobota	sobota	k1gFnSc1	sobota
<g/>
,	,	kIx,	,
neděle	neděle	k1gFnSc1	neděle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
do	do	k7c2	do
utkání	utkání	k1gNnSc2	utkání
nominuje	nominovat	k5eAaBmIp3nS	nominovat
čtyři	čtyři	k4xCgMnPc4	čtyři
hráče	hráč	k1gMnPc4	hráč
a	a	k8xC	a
den	den	k1gInSc4	den
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
zápasu	zápas	k1gInSc2	zápas
se	se	k3xPyFc4	se
rozlosuje	rozlosovat	k5eAaPmIp3nS	rozlosovat
pořadí	pořadí	k1gNnSc1	pořadí
a	a	k8xC	a
obsazení	obsazení	k1gNnSc1	obsazení
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
uskuteční	uskutečnit	k5eAaPmIp3nP	uskutečnit
2	[number]	k4	2
zápasy	zápas	k1gInPc7	zápas
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
(	(	kIx(	(
<g/>
singly	singl	k1gInPc1	singl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
1	[number]	k4	1
zápas	zápas	k1gInSc4	zápas
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
(	(	kIx(	(
<g/>
debl	debl	k1gInSc1	debl
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
den	den	k1gInSc4	den
opět	opět	k6eAd1	opět
2	[number]	k4	2
singly	singl	k1gInPc4	singl
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
opačném	opačný	k2eAgNnSc6d1	opačné
obsazení	obsazení	k1gNnSc6	obsazení
než	než	k8xS	než
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
dnu	den	k1gInSc6	den
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
kapitán	kapitán	k1gMnSc1	kapitán
právo	právo	k1gNnSc4	právo
měnit	měnit	k5eAaImF	měnit
v	v	k7c4	v
den	den	k1gInSc4	den
utkání	utkání	k1gNnSc4	utkání
obsazení	obsazení	k1gNnSc2	obsazení
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c4	na
3	[number]	k4	3
vítězné	vítězný	k2eAgInPc4d1	vítězný
sety	set	k1gInPc4	set
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nemusí	muset	k5eNaImIp3nS	muset
dodržet	dodržet	k5eAaPmF	dodržet
třetí	třetí	k4xOgInSc4	třetí
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
utkání	utkání	k1gNnSc1	utkání
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
rozhodnuté	rozhodnutý	k2eAgFnPc1d1	rozhodnutá
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
dohody	dohoda	k1gFnSc2	dohoda
lze	lze	k6eAd1	lze
potom	potom	k6eAd1	potom
hrát	hrát	k5eAaImF	hrát
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
2	[number]	k4	2
vítězné	vítězný	k2eAgInPc4d1	vítězný
sety	set	k1gInPc4	set
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
alespoň	alespoň	k9	alespoň
3	[number]	k4	3
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
soupeři	soupeř	k1gMnPc1	soupeř
se	se	k3xPyFc4	se
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Davis	Davis	k1gFnSc1	Davis
Cupu	cup	k1gInSc2	cup
vzájemně	vzájemně	k6eAd1	vzájemně
střídají	střídat	k5eAaImIp3nP	střídat
v	v	k7c6	v
pořadatelství	pořadatelství	k1gNnSc6	pořadatelství
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
daní	daný	k2eAgMnPc1d1	daný
soupeři	soupeř	k1gMnPc1	soupeř
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
ještě	ještě	k9	ještě
neutkali	utkat	k5eNaPmAgMnP	utkat
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
o	o	k7c6	o
pořadateli	pořadatel	k1gMnSc6	pořadatel
prvního	první	k4xOgNnSc2	první
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
utkání	utkání	k1gNnSc2	utkání
los	los	k1gInSc1	los
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatel	pořadatel	k1gMnSc1	pořadatel
utkání	utkání	k1gNnSc2	utkání
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vybrat	vybrat	k5eAaPmF	vybrat
nejenom	nejenom	k6eAd1	nejenom
místo	místo	k7c2	místo
utkání	utkání	k1gNnSc2	utkání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
volbě	volba	k1gFnSc6	volba
je	být	k5eAaImIp3nS	být
i	i	k9	i
povrch	povrch	k1gInSc4	povrch
kurtu	kurt	k1gInSc2	kurt
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
zápasy	zápas	k1gInPc1	zápas
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
zápas	zápas	k1gInSc1	zápas
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Davis	Davis	k1gFnSc4	Davis
Cupu	cup	k1gInSc2	cup
trval	trvat	k5eAaImAgInS	trvat
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odehrán	odehrát	k5eAaPmNgInS	odehrát
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
24	[number]	k4	24
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
český	český	k2eAgInSc1d1	český
pár	pár	k1gInSc1	pár
Tomáš	Tomáš	k1gMnSc1	Tomáš
Berdych	Berdych	k1gMnSc1	Berdych
a	a	k8xC	a
Lukáš	Lukáš	k1gMnSc1	Lukáš
Rosol	Rosol	k1gMnSc1	Rosol
domácí	domácí	k1gMnSc1	domácí
hráče	hráč	k1gMnSc4	hráč
Stanislase	Stanislas	k1gInSc6	Stanislas
Wawrinku	Wawrink	k1gInSc6	Wawrink
a	a	k8xC	a
Marca	Marc	k2eAgMnSc4d1	Marc
Chiudinelliho	Chiudinelli	k1gMnSc4	Chiudinelli
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historie	historie	k1gFnSc1	historie
Davis	Davis	k1gInSc1	Davis
Cupu	cup	k1gInSc2	cup
<g/>
#	#	kIx~	#
<g/>
Historické	historický	k2eAgInPc1d1	historický
rekordy	rekord	k1gInPc1	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
tým	tým	k1gInSc1	tým
měl	mít	k5eAaImAgInS	mít
v	v	k7c4	v
danou	daný	k2eAgFnSc4d1	daná
dobu	doba	k1gFnSc4	doba
nejvíce	hodně	k6eAd3	hodně
prvních	první	k4xOgNnPc2	první
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pořadí	pořadí	k1gNnSc1	pořadí
shodné	shodný	k2eAgNnSc1d1	shodné
<g/>
,	,	kIx,	,
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tým	tým	k1gInSc1	tým
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
ITF	ITF	kA	ITF
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
zavedení	zavedení	k1gNnSc1	zavedení
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2001	[number]	k4	2001
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
osm	osm	k4xCc1	osm
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc4	bod
každého	každý	k3xTgInSc2	každý
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
Davis	Davis	k1gFnSc6	Davis
Cupu	cup	k1gInSc2	cup
jsou	být	k5eAaImIp3nP	být
kumulovány	kumulován	k2eAgInPc1d1	kumulován
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
posledních	poslední	k2eAgInPc2d1	poslední
ročníků	ročník	k1gInPc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc4d2	veliký
váhu	váha	k1gFnSc4	váha
mají	mít	k5eAaImIp3nP	mít
body	bod	k1gInPc1	bod
získané	získaný	k2eAgInPc1d1	získaný
nověji	nově	k6eAd2	nově
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
váha	váha	k1gFnSc1	váha
bodů	bod	k1gInPc2	bod
dosažených	dosažený	k2eAgInPc2d1	dosažený
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
každém	každý	k3xTgNnSc6	každý
odehraném	odehraný	k2eAgNnSc6d1	odehrané
kole	kolo	k1gNnSc6	kolo
je	být	k5eAaImIp3nS	být
žebříček	žebříček	k1gInSc1	žebříček
aktualizován	aktualizován	k2eAgInSc1d1	aktualizován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
sumě	suma	k1gFnSc6	suma
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
100	[number]	k4	100
<g/>
%	%	kIx~	%
váhu	váha	k1gFnSc4	váha
body	bod	k1gInPc1	bod
získané	získaný	k2eAgInPc1d1	získaný
v	v	k7c6	v
posledním	poslední	k2eAgMnSc6d1	poslední
<g/>
,	,	kIx,	,
předešlém	předešlý	k2eAgInSc6d1	předešlý
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
počítané	počítaný	k2eAgFnSc2d1	počítaná
k	k	k7c3	k
datu	datum	k1gNnSc3	datum
daného	daný	k2eAgNnSc2d1	dané
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
součtu	součet	k1gInSc2	součet
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
přičteny	přičíst	k5eAaPmNgInP	přičíst
i	i	k9	i
body	bod	k1gInPc4	bod
získané	získaný	k2eAgInPc4d1	získaný
před	před	k7c7	před
dvěma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
třemi	tři	k4xCgNnPc7	tři
a	a	k8xC	a
čtyřmi	čtyři	k4xCgNnPc7	čtyři
lety	léto	k1gNnPc7	léto
k	k	k7c3	k
danému	daný	k2eAgNnSc3d1	dané
datu	datum	k1gNnSc3	datum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
váhou	váha	k1gFnSc7	váha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
váha	váha	k1gFnSc1	váha
bodů	bod	k1gInPc2	bod
dosažených	dosažený	k2eAgInPc2d1	dosažený
před	před	k7c7	před
2	[number]	k4	2
lety	léto	k1gNnPc7	léto
je	být	k5eAaImIp3nS	být
75	[number]	k4	75
%	%	kIx~	%
<g/>
,	,	kIx,	,
před	před	k7c7	před
3	[number]	k4	3
lety	let	k1gInPc7	let
50	[number]	k4	50
%	%	kIx~	%
a	a	k8xC	a
před	před	k7c7	před
4	[number]	k4	4
lety	let	k1gInPc7	let
25	[number]	k4	25
%	%	kIx~	%
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
původní	původní	k2eAgFnSc2d1	původní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc1	bod
jsou	být	k5eAaImIp3nP	být
přidělovány	přidělovat	k5eAaImNgInP	přidělovat
pouze	pouze	k6eAd1	pouze
vítězným	vítězný	k2eAgInPc3d1	vítězný
týmům	tým	k1gInPc3	tým
daného	daný	k2eAgNnSc2d1	dané
kola	kolo	k1gNnSc2	kolo
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
kol	kolo	k1gNnPc2	kolo
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
skupině	skupina	k1gFnSc6	skupina
jsou	být	k5eAaImIp3nP	být
bodováni	bodován	k2eAgMnPc1d1	bodován
štědřeji	štědro	k6eAd2	štědro
než	než	k8xS	než
v	v	k7c6	v
kontinentálních	kontinentální	k2eAgFnPc6d1	kontinentální
zónách	zóna	k1gFnPc6	zóna
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
následná	následný	k2eAgNnPc1d1	následné
kola	kolo	k1gNnPc1	kolo
mají	mít	k5eAaImIp3nP	mít
vzestupnou	vzestupný	k2eAgFnSc4d1	vzestupná
bodovou	bodový	k2eAgFnSc4d1	bodová
gradaci	gradace	k1gFnSc4	gradace
<g/>
.	.	kIx.	.
</s>
<s>
Zvýhodněny	zvýhodněn	k2eAgInPc1d1	zvýhodněn
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyhrají	vyhrát	k5eAaPmIp3nP	vyhrát
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
domácím	domácí	k2eAgFnPc3d1	domácí
výhrám	výhra	k1gFnPc3	výhra
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
fázi	fáze	k1gFnSc6	fáze
a	a	k8xC	a
úrovni	úroveň	k1gFnSc6	úroveň
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Bonusové	bonusový	k2eAgInPc1d1	bonusový
body	bod	k1gInPc1	bod
náleží	náležet	k5eAaImIp3nP	náležet
družstvům	družstvo	k1gNnPc3	družstvo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
porazí	porazit	k5eAaPmIp3nP	porazit
výše	vysoce	k6eAd2	vysoce
postaveného	postavený	k2eAgMnSc4d1	postavený
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
soupeř	soupeř	k1gMnSc1	soupeř
umístěn	umístit	k5eAaPmNgMnS	umístit
do	do	k7c2	do
64	[number]	k4	64
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
bonus	bonus	k1gInSc1	bonus
si	se	k3xPyFc3	se
odváží	odvážit	k5eAaPmIp3nS	odvážit
vítězný	vítězný	k2eAgInSc4d1	vítězný
celek	celek	k1gInSc4	celek
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdyby	kdyby	kYmCp3nS	kdyby
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Bonus	bonus	k1gInSc1	bonus
však	však	k9	však
není	být	k5eNaImIp3nS	být
přidělován	přidělovat	k5eAaImNgInS	přidělovat
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
skupinách	skupina	k1gFnPc6	skupina
kontinentálních	kontinentální	k2eAgFnPc2d1	kontinentální
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
počet	počet	k1gInSc1	počet
týmů	tým	k1gInPc2	tým
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
skupině	skupina	k1gFnSc3	skupina
zóny	zóna	k1gFnSc2	zóna
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
překoná	překonat	k5eAaPmIp3nS	překonat
16	[number]	k4	16
účastníků	účastník	k1gMnPc2	účastník
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
8	[number]	k4	8
účastníků	účastník	k1gMnPc2	účastník
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
skupinách	skupina	k1gFnPc6	skupina
zóny	zóna	k1gFnSc2	zóna
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Oceánie	Oceánie	k1gFnSc2	Oceánie
či	či	k8xC	či
Americké	americký	k2eAgFnSc2d1	americká
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
týmu	tým	k1gInSc3	tým
za	za	k7c4	za
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
připsáno	připsat	k5eAaPmNgNnS	připsat
200	[number]	k4	200
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
výhru	výhra	k1gFnSc4	výhra
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
400	[number]	k4	400
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
za	za	k7c4	za
výhru	výhra	k1gFnSc4	výhra
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
600	[number]	k4	600
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
nejsou	být	k5eNaImIp3nP	být
ze	z	k7c2	z
zápasů	zápas	k1gInPc2	zápas
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
přidělovány	přidělován	k2eAgInPc1d1	přidělován
body	bod	k1gInPc1	bod
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
ATP.	atp.	kA	atp.
</s>
