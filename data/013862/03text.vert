<s>
Výkonné	výkonný	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
č.	č.	k?
9066	#num#	k4
</s>
<s>
Vyvěšený	vyvěšený	k2eAgInSc1d1
leták	leták	k1gInSc1
s	s	k7c7
rozhodnutím	rozhodnutí	k1gNnSc7
vyzývajícím	vyzývající	k2eAgNnSc7d1
osoby	osoba	k1gFnPc1
japonského	japonský	k2eAgInSc2d1
původu	původ	k1gInSc2
k	k	k7c3
evakuaci	evakuace	k1gFnSc3
z	z	k7c2
území	území	k1gNnSc2
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Prezident	prezident	k1gMnSc1
Gerald	Gerald	k1gMnSc1
Ford	ford	k1gInSc4
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1976	#num#	k4
podepisující	podepisující	k2eAgFnSc1d1
formální	formální	k2eAgNnSc4d1
zrušení	zrušení	k1gNnSc4
výkonného	výkonný	k2eAgNnSc2d1
nařízení	nařízení	k1gNnSc2
č.	č.	k?
9066	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Výkonné	výkonný	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
č.	č.	k?
9066	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Executive	Executive	k2eAgInSc1d1
Order	Order	k1gInSc1
9066	#num#	k4
<g/>
,	,	kIx,
česky	česky	k6eAd1
též	též	k6eAd1
exekutivní	exekutivní	k2eAgInSc4d1
příkaz	příkaz	k1gInSc4
č.	č.	kA
9066	#num#	k4
či	či	k8xC
prezidentský	prezidentský	k2eAgInSc4d1
dekret	dekret	k1gInSc1
č.	č.	k?
9066	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nařízení	nařízení	k1gNnSc1
vydané	vydaný	k2eAgNnSc1d1
v	v	k7c6
období	období	k1gNnSc6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
americkým	americký	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Franklinem	Franklin	k1gInSc7
D.	D.	kA
Rooseveltem	Roosevelt	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
jej	on	k3xPp3gMnSc4
podepsal	podepsat	k5eAaPmAgInS
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
japonský	japonský	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nařízení	nařízení	k1gNnSc1
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
prováděcí	prováděcí	k2eAgInSc1d1
předpis	předpis	k1gInSc1
k	k	k7c3
zákonu	zákon	k1gInSc3
z	z	k7c2
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1918	#num#	k4
ohledně	ohledně	k7c2
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmocňovalo	zmocňovat	k5eAaImAgNnS
ministra	ministr	k1gMnSc2
války	válka	k1gFnSc2
k	k	k7c3
vymezení	vymezení	k1gNnSc3
tzv.	tzv.	kA
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
(	(	kIx(
<g/>
military	militar	k1gInPc4
areas	areas	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
vykázány	vykázat	k5eAaPmNgFnP
všechny	všechen	k3xTgMnPc4
či	či	k8xC
jen	jen	k6eAd1
vybrané	vybraný	k2eAgFnPc1d1
osoby	osoba	k1gFnPc1
<g/>
,	,	kIx,
místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
i	i	k8xC
cizinci	cizinec	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
praxi	praxe	k1gFnSc6
bylo	být	k5eAaImAgNnS
uplatňováno	uplatňovat	k5eAaImNgNnS
na	na	k7c4
příslušníky	příslušník	k1gMnPc4
tzv.	tzv.	kA
nepřátelských	přátelský	k2eNgInPc2d1
národů	národ	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
osoby	osoba	k1gFnSc2
německé	německý	k2eAgFnSc2d1
<g/>
,	,	kIx,
italské	italský	k2eAgFnSc2d1
a	a	k8xC
japonské	japonský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
poslední	poslední	k2eAgFnSc1d1
jmenovaná	jmenovaná	k1gFnSc1
byla	být	k5eAaImAgFnS
dotčena	dotknout	k5eAaPmNgFnS
daleko	daleko	k6eAd1
nejpočetněji	početně	k6eAd3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vedlo	vést	k5eAaImAgNnS
to	ten	k3xDgNnSc4
až	až	k9
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
umístění	umístění	k1gNnSc1
do	do	k7c2
amerických	americký	k2eAgInPc2d1
internačních	internační	k2eAgInPc2d1
táborů	tábor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Navazující	navazující	k2eAgInPc4d1
právní	právní	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
a	a	k8xC
akty	akt	k1gInPc4
</s>
<s>
Dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
pověřil	pověřit	k5eAaPmAgMnS
ministr	ministr	k1gMnSc1
války	válka	k1gFnSc2
Henry	Henry	k1gMnSc1
Stimson	Stimson	k1gMnSc1
generálporučíka	generálporučík	k1gMnSc4
Johna	John	k1gMnSc4
L.	L.	kA
DeWitta	DeWitt	k1gMnSc4
provedením	provedení	k1gNnSc7
prezidentova	prezidentův	k2eAgInSc2d1
dekretu	dekret	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
vydal	vydat	k5eAaPmAgInS
vyhlášku	vyhláška	k1gFnSc4
č.	č.	k?
1	#num#	k4
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
změnil	změnit	k5eAaPmAgInS
celé	celý	k2eAgNnSc4d1
pobřežní	pobřežní	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
potenciálně	potenciálně	k6eAd1
ohrožené	ohrožený	k2eAgNnSc4d1
útokem	útok	k1gInSc7
Japonska	Japonsko	k1gNnSc2
na	na	k7c4
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
De	De	k?
facto	facto	k1gNnSc1
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
týkalo	týkat	k5eAaImAgNnS
celých	celý	k2eAgInPc2d1
států	stát	k1gInPc2
Oregon	Oregon	k1gInSc1
a	a	k8xC
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
a	a	k8xC
Arizony	Arizona	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
samotné	samotný	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
okruh	okruh	k1gInSc4
národností	národnost	k1gFnPc2
nespecifikovalo	specifikovat	k5eNaBmAgNnS
<g/>
,	,	kIx,
v	v	k7c6
této	tento	k3xDgFnSc6
vyhlášce	vyhláška	k1gFnSc6
již	již	k6eAd1
byly	být	k5eAaImAgFnP
vymezeny	vymezen	k2eAgFnPc1d1
povinnosti	povinnost	k1gFnPc1
cizincům	cizinec	k1gMnPc3
z	z	k7c2
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
Německa	Německo	k1gNnSc2
a	a	k8xC
Itálie	Itálie	k1gFnSc2
i	i	k9
Američanům	Američan	k1gMnPc3
japonského	japonský	k2eAgInSc2d1
původu	původ	k1gInSc2
(	(	kIx(
<g/>
nikoli	nikoli	k9
však	však	k9
analogicky	analogicky	k6eAd1
evropského	evropský	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
předem	předem	k6eAd1
hlásili	hlásit	k5eAaImAgMnP
změnu	změna	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
bydliště	bydliště	k1gNnSc2
příslušnému	příslušný	k2eAgInSc3d1
úřadu	úřad	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
DeWitt	DeWitt	k1gMnSc1
navíc	navíc	k6eAd1
vyzýval	vyzývat	k5eAaImAgMnS
zprvu	zprvu	k6eAd1
k	k	k7c3
dobrovolnému	dobrovolný	k2eAgNnSc3d1
vystěhování	vystěhování	k1gNnSc3
osob	osoba	k1gFnPc2
japonského	japonský	k2eAgInSc2d1
původu	původ	k1gInSc2
z	z	k7c2
určených	určený	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
jinam	jinam	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
výkonném	výkonný	k2eAgNnSc6d1
nařízení	nařízení	k1gNnSc6
č.	č.	k?
9066	#num#	k4
vydal	vydat	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
Roosevelt	Roosevelt	k1gMnSc1
11	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1942	#num#	k4
další	další	k2eAgInSc4d1
dekret	dekret	k1gInSc4
<g/>
,	,	kIx,
označený	označený	k2eAgMnSc1d1
číslem	číslo	k1gNnSc7
9095	#num#	k4
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
byla	být	k5eAaImAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
krizové	krizový	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
zřízena	zřízen	k2eAgFnSc1d1
Kancelář	kancelář	k1gFnSc1
správce	správce	k1gMnSc2
cizineckého	cizinecký	k2eAgInSc2d1
majetku	majetek	k1gInSc2
(	(	kIx(
<g/>
Alien	Alien	k1gInSc1
Property	Propert	k1gInPc4
Custodian	Custodian	k1gInSc1
<g/>
)	)	kIx)
s	s	k7c7
poměrně	poměrně	k6eAd1
širokými	široký	k2eAgFnPc7d1
pravomocemi	pravomoc	k1gFnPc7
ohledně	ohledně	k7c2
správy	správa	k1gFnSc2
majetku	majetek	k1gInSc2
nepřátelských	přátelský	k2eNgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gMnPc2
státních	státní	k2eAgMnPc2d1
příslušníků	příslušník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1942	#num#	k4
vydal	vydat	k5eAaPmAgMnS
generálporučík	generálporučík	k1gMnSc1
DeWitt	DeWitt	k1gMnSc1
vyhlášku	vyhláška	k1gFnSc4
č.	č.	k?
2	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vymezovala	vymezovat	k5eAaImAgFnS
vojenské	vojenský	k2eAgInPc4d1
újezdy	újezd	k1gInPc4
ve	v	k7c6
státech	stát	k1gInPc6
Nevada	Nevada	k1gFnSc1
<g/>
,	,	kIx,
Montana	Montana	k1gFnSc1
<g/>
,	,	kIx,
Utah	Utah	k1gInSc1
a	a	k8xC
Idaho	Ida	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
rozšíření	rozšíření	k1gNnSc2
o	o	k7c4
další	další	k2eAgNnSc4d1
území	území	k1gNnSc4
též	též	k9
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
změně	změna	k1gFnSc3
přístupu	přístup	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dobrovolné	dobrovolný	k2eAgNnSc1d1
vystěhování	vystěhování	k1gNnSc1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
nahrazeno	nahradit	k5eAaPmNgNnS
již	již	k6eAd1
povinnými	povinný	k2eAgInPc7d1
přesuny	přesun	k1gInPc7
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezident	prezident	k1gMnSc1
Roosevelt	Roosevelt	k1gMnSc1
též	též	k9
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1942	#num#	k4
vydal	vydat	k5eAaPmAgInS
výkonné	výkonný	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
č.	č.	k?
9102	#num#	k4
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
pod	pod	k7c7
Úřadem	úřad	k1gInSc7
pro	pro	k7c4
krizové	krizový	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
zřídil	zřídit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
Úřad	úřad	k1gInSc1
pro	pro	k7c4
přesídlování	přesídlování	k1gNnSc4
v	v	k7c6
době	doba	k1gFnSc6
války	válka	k1gFnSc2
(	(	kIx(
<g/>
War	War	k1gFnPc1
Relocation	Relocation	k1gInSc4
Authority	Authorita	k1gFnSc2
<g/>
,	,	kIx,
WRA	WRA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ředitelem	ředitel	k1gMnSc7
tohoto	tento	k3xDgInSc2
úřadu	úřad	k1gInSc2
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
jmenován	jmenovat	k5eAaImNgInS,k5eAaBmNgInS
Milton	Milton	k1gInSc1
S.	S.	kA
Eisenhower	Eisenhower	k1gInSc1
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
budoucího	budoucí	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Dwighta	Dwight	k1gMnSc2
D.	D.	kA
Eisenhowera	Eisenhower	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1942	#num#	k4
přijal	přijmout	k5eAaPmAgInS
Kongres	kongres	k1gInSc1
zákon	zákon	k1gInSc1
č.	č.	k?
503	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
umožnil	umožnit	k5eAaPmAgInS
trestat	trestat	k5eAaImF
porušování	porušování	k1gNnSc4
předpisů	předpis	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
neuposlechnutí	neuposlechnutí	k1gNnSc2
výzvy	výzva	k1gFnSc2
k	k	k7c3
opuštění	opuštění	k1gNnSc3
prostoru	prostor	k1gInSc2
či	či	k8xC
naopak	naopak	k6eAd1
vycestování	vycestování	k1gNnSc3
oproti	oproti	k7c3
zákazu	zákaz	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trestalo	trestat	k5eAaImAgNnS
se	se	k3xPyFc4
pokutou	pokuta	k1gFnSc7
i	i	k8xC
vězením	vězení	k1gNnSc7
do	do	k7c2
1	#num#	k4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Generálporučík	generálporučík	k1gMnSc1
DeWitt	DeWitt	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1942	#num#	k4
vyhlášku	vyhláška	k1gFnSc4
č.	č.	k?
3	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
ve	v	k7c6
všech	všecek	k3xTgNnPc6
osmi	osm	k4xCc6
dříve	dříve	k6eAd2
ustanovených	ustanovený	k2eAgInPc6d1
vojenských	vojenský	k2eAgInPc6d1
újezdech	újezd	k1gInPc6
zavedla	zavést	k5eAaPmAgFnS
zákaz	zákaz	k1gInSc4
nočního	noční	k2eAgNnSc2d1
vycházení	vycházení	k1gNnSc2
pro	pro	k7c4
osoby	osoba	k1gFnPc4
z	z	k7c2
nepřátelských	přátelský	k2eNgInPc2d1
států	stát	k1gInPc2
i	i	k9
pro	pro	k7c4
japonské	japonský	k2eAgMnPc4d1
Američany	Američan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
mimo	mimo	k7c4
noční	noční	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
se	se	k3xPyFc4
pak	pak	k6eAd1
měli	mít	k5eAaImAgMnP
zdržovat	zdržovat	k5eAaImF
v	v	k7c6
podstatě	podstata	k1gFnSc6
jen	jen	k9
v	v	k7c6
okruhu	okruh	k1gInSc6
5	#num#	k4
km	km	kA
od	od	k7c2
svýho	svýho	k?
bydliště	bydliště	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
místě	místo	k1gNnSc6
svého	svůj	k3xOyFgNnSc2
pracoviště	pracoviště	k1gNnSc2
nebo	nebo	k8xC
na	na	k7c6
cestě	cesta	k1gFnSc6
mezi	mezi	k7c7
těmito	tento	k3xDgNnPc7
místy	místo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
jim	on	k3xPp3gMnPc3
zakázáno	zakázán	k2eAgNnSc1d1
také	také	k9
vlastnit	vlastnit	k5eAaImF
střelné	střelný	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
<g/>
,	,	kIx,
munici	munice	k1gFnSc4
<g/>
,	,	kIx,
bomby	bomba	k1gFnPc4
a	a	k8xC
další	další	k2eAgInSc4d1
vojenský	vojenský	k2eAgInSc4d1
materiál	materiál	k1gInSc4
i	i	k8xC
zařízení	zařízení	k1gNnPc4
využitelná	využitelný	k2eAgNnPc4d1
ke	k	k7c3
špionáži	špionáž	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
posledním	poslední	k2eAgInSc6d1
březnovém	březnový	k2eAgInSc6d1
týdnu	týden	k1gInSc6
DeWitt	DeWitt	k1gInSc1
začal	začít	k5eAaPmAgInS
vydávat	vydávat	k5eAaImF,k5eAaPmF
velké	velká	k1gFnPc4
množství	množství	k1gNnSc2
příkazů	příkaz	k1gInPc2
k	k	k7c3
vystěhování	vystěhování	k1gNnSc3
z	z	k7c2
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotnému	samotný	k2eAgNnSc3d1
vystěhování	vystěhování	k1gNnSc3
obvykle	obvykle	k6eAd1
předcházela	předcházet	k5eAaImAgFnS
registrace	registrace	k1gFnSc1
u	u	k7c2
tzv.	tzv.	kA
stanic	stanice	k1gFnPc2
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
občanů	občan	k1gMnPc2
(	(	kIx(
<g/>
Civil	civil	k1gMnSc1
Control	Controla	k1gFnPc2
Station	station	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Vyhláškou	vyhláška	k1gFnSc7
č.	č.	k?
4	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1942	#num#	k4
generálporučík	generálporučík	k1gMnSc1
DeWitt	DeWitt	k1gMnSc1
stanovil	stanovit	k5eAaPmAgInS
obecný	obecný	k2eAgInSc4d1
zákaz	zákaz	k1gInSc4
opuštění	opuštění	k1gNnSc2
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
č.	č.	k?
1	#num#	k4
(	(	kIx(
<g/>
160	#num#	k4
<g/>
km	km	kA
pásma	pásmo	k1gNnSc2
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dokud	dokud	k8xS
to	ten	k3xDgNnSc1
nebude	být	k5eNaImBp3nS
dovoleno	dovolit	k5eAaPmNgNnS
či	či	k8xC
přímo	přímo	k6eAd1
přikázáno	přikázat	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Internace	internace	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Internace	internace	k1gFnSc1
Američanů	Američan	k1gMnPc2
japonského	japonský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Přesídlení	přesídlení	k1gNnSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
praxi	praxe	k1gFnSc6
spočívalo	spočívat	k5eAaImAgNnS
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
internaci	internace	k1gFnSc6
v	v	k7c6
tzv.	tzv.	kA
shromažďovacích	shromažďovací	k2eAgNnPc6d1
střediscích	středisko	k1gNnPc6
(	(	kIx(
<g/>
Assembly	Assembly	k1gFnSc1
Centers	Centersa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
předstupněm	předstupeň	k1gInSc7
k	k	k7c3
přemístění	přemístění	k1gNnSc3
do	do	k7c2
tzv.	tzv.	kA
relokačních	relokační	k2eAgInPc2d1
táborů	tábor	k1gInPc2
<g/>
/	/	kIx~
<g/>
center	centrum	k1gNnPc2
(	(	kIx(
<g/>
Relocation	Relocation	k1gInSc1
Centers	Centers	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Shromažďovací	shromažďovací	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
podléhala	podléhat	k5eAaImAgNnP
Úřadu	úřad	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
pro	pro	k7c4
civilní	civilní	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
v	v	k7c6
době	doba	k1gFnSc6
války	válka	k1gFnSc2
(	(	kIx(
<g/>
Wartime	Wartim	k1gInSc5
Civil	civil	k1gMnSc1
Control	Controla	k1gFnPc2
Administration	Administration	k1gInSc1
<g/>
,	,	kIx,
WCCA	WCCA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nad	nad	k7c7
relokačními	relokační	k2eAgNnPc7d1
centry	centrum	k1gNnPc7
vykonávaly	vykonávat	k5eAaImAgFnP
dohled	dohled	k1gInSc4
dvě	dva	k4xCgFnPc4
instituce	instituce	k1gFnPc1
<g/>
:	:	kIx,
dvě	dva	k4xCgNnPc4
centra	centrum	k1gNnPc4
v	v	k7c6
Arizoně	Arizona	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
jako	jako	k9
první	první	k4xOgFnSc1
a	a	k8xC
zahrnovala	zahrnovat	k5eAaImAgFnS
více	hodně	k6eAd2
než	než	k8xS
30	#num#	k4
000	#num#	k4
internovaných	internovaný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
podléhala	podléhat	k5eAaImAgFnS
Kanceláři	kancelář	k1gFnSc3
pro	pro	k7c4
záležitosti	záležitost	k1gFnPc4
indiánů	indián	k1gMnPc2
<g/>
,	,	kIx,
již	již	k6eAd1
vedl	vést	k5eAaImAgMnS
John	John	k1gMnSc1
Collier	Collier	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgInSc1d1
centra	centrum	k1gNnSc2
se	s	k7c7
zhruba	zhruba	k6eAd1
80	#num#	k4
000	#num#	k4
osobami	osoba	k1gFnPc7
podléhala	podléhat	k5eAaImAgFnS
Úřadu	úřada	k1gMnSc4
pro	pro	k7c4
přesídlování	přesídlování	k1gNnSc4
v	v	k7c6
době	doba	k1gFnSc6
války	válka	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
vedl	vést	k5eAaImAgInS
zprvu	zprvu	k6eAd1
Milton	Milton	k1gInSc1
S.	S.	kA
Eisenhower	Eisenhower	k1gInSc1
a	a	k8xC
později	pozdě	k6eAd2
Dillon	Dillon	k1gInSc1
Myer	Myera	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přibližně	přibližně	k6eAd1
90	#num#	k4
000	#num#	k4
osob	osoba	k1gFnPc2
prošlo	projít	k5eAaPmAgNnS
do	do	k7c2
relokačních	relokační	k2eAgNnPc2d1
center	centrum	k1gNnPc2
přes	přes	k7c4
shromažďovací	shromažďovací	k2eAgNnPc4d1
střediska	středisko	k1gNnPc4
<g/>
,	,	kIx,
asi	asi	k9
20	#num#	k4
000	#num#	k4
jich	on	k3xPp3gNnPc2
tam	tam	k6eAd1
bylo	být	k5eAaImAgNnS
evakuováno	evakuovat	k5eAaBmNgNnS
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
další	další	k2eAgInPc4d1
asi	asi	k9
2	#num#	k4
000	#num#	k4
se	se	k3xPyFc4
tam	tam	k6eAd1
dostaly	dostat	k5eAaPmAgFnP
z	z	k7c2
improvizovaných	improvizovaný	k2eAgInPc2d1
detenčních	detenční	k2eAgInPc2d1
či	či	k8xC
zadržovacích	zadržovací	k2eAgInPc2d1
táborů	tábor	k1gInPc2
a	a	k8xC
téměř	téměř	k6eAd1
6	#num#	k4
000	#num#	k4
dětí	dítě	k1gFnPc2
se	se	k3xPyFc4
narodilo	narodit	k5eAaPmAgNnS
přímo	přímo	k6eAd1
v	v	k7c6
relokačních	relokační	k2eAgNnPc6d1
centrech	centrum	k1gNnPc6
během	během	k7c2
pobytu	pobyt	k1gInSc2
jejich	jejich	k3xOp3gMnPc2
rodičů	rodič	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
internovaných	internovaný	k2eAgMnPc2d1
německých	německý	k2eAgMnPc2d1
Američanů	Američan	k1gMnPc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
10	#num#	k4
000	#num#	k4
a	a	k8xC
italských	italský	k2eAgMnPc2d1
Američanů	Američan	k1gMnPc2
ještě	ještě	k9
méně	málo	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ze	z	k7c2
strany	strana	k1gFnSc2
internovaných	internovaný	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
nebyl	být	k5eNaImAgInS
zaznamenán	zaznamenán	k2eAgInSc1d1
větší	veliký	k2eAgInSc1d2
odpor	odpor	k1gInSc1
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
zachovávala	zachovávat	k5eAaImAgFnS
rezignovaný	rezignovaný	k2eAgInSc4d1
postoj	postoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
vzhledem	vzhledem	k7c3
k	k	k7c3
protijaponským	protijaponský	k2eAgFnPc3d1
náladám	nálada	k1gFnPc3
ve	v	k7c6
společnosti	společnost	k1gFnSc6
řada	řada	k1gFnSc1
lidí	člověk	k1gMnPc2
brala	brát	k5eAaImAgFnS
internaci	internace	k1gFnSc3
i	i	k9
jako	jako	k9
úkryt	úkryt	k1gInSc4
před	před	k7c7
útoky	útok	k1gInPc7
ze	z	k7c2
strany	strana	k1gFnSc2
ostatního	ostatní	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
malé	malý	k2eAgNnSc1d1
procento	procento	k1gNnSc1
se	se	k3xPyFc4
odmítlo	odmítnout	k5eAaPmAgNnS
nařízeními	nařízení	k1gNnPc7
řídit	řídit	k5eAaImF
nebo	nebo	k8xC
se	se	k3xPyFc4
domáhalo	domáhat	k5eAaImAgNnS
svých	svůj	k3xOyFgNnPc2
práv	právo	k1gNnPc2
soudně	soudně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KŘÍŽEK	Křížek	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právní	právní	k2eAgInSc4d1
postavení	postavení	k1gNnSc1
Američanů	Američan	k1gMnPc2
japonského	japonský	k2eAgInSc2d1
původu	původ	k1gInSc2
v	v	k7c6
USA	USA	kA
v	v	k7c6
období	období	k1gNnSc6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
49	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
David	David	k1gMnSc1
Labus	Labus	k1gMnSc1
<g/>
.	.	kIx.
s.	s.	k?
11	#num#	k4
an	an	k?
<g/>
..	..	k?
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Křížek	Křížek	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Křížek	křížek	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
12	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Křížek	křížek	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
13.1	13.1	k4
2	#num#	k4
Křížek	Křížek	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
14.1	14.1	k4
2	#num#	k4
Křížek	Křížek	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Křížek	křížek	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
16	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Křížek	křížek	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
16	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Křížek	křížek	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
17.1	17.1	k4
2	#num#	k4
Křížek	Křížek	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
18	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Křížek	křížek	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
19	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Křížek	křížek	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
20.1	20.1	k4
2	#num#	k4
Křížek	Křížek	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
výkonné	výkonný	k2eAgFnSc2d1
nařízení	nařízení	k1gNnSc1
č.	č.	k?
9066	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
