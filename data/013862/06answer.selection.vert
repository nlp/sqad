<s>
Výkonné	výkonný	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
č.	č.	k?
9066	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Executive	Executive	k2eAgInSc1d1
Order	Order	k1gInSc1
9066	#num#	k4
<g/>
,	,	kIx,
česky	česky	k6eAd1
též	též	k6eAd1
exekutivní	exekutivní	k2eAgInSc4d1
příkaz	příkaz	k1gInSc4
č.	č.	kA
9066	#num#	k4
či	či	k8xC
prezidentský	prezidentský	k2eAgInSc4d1
dekret	dekret	k1gInSc1
č.	č.	k?
9066	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nařízení	nařízení	k1gNnSc1
vydané	vydaný	k2eAgNnSc1d1
v	v	k7c6
období	období	k1gNnSc6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
americkým	americký	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Franklinem	Franklin	k1gInSc7
D.	D.	kA
Rooseveltem	Roosevelt	k1gMnSc7
<g/>
.	.	kIx.
</s>