<s>
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
aneb	aneb	k?	aneb
Koňská	koňský	k2eAgFnSc1d1	koňská
opera	opera	k1gFnSc1	opera
je	být	k5eAaImIp3nS	být
československá	československý	k2eAgFnSc1d1	Československá
filmová	filmový	k2eAgFnSc1d1	filmová
hudební	hudební	k2eAgFnSc1d1	hudební
komedie	komedie	k1gFnSc1	komedie
resp.	resp.	kA	resp.
osobitá	osobitý	k2eAgFnSc1d1	osobitá
parodie	parodie	k1gFnSc1	parodie
westernu	western	k1gInSc2	western
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
Jiřího	Jiří	k1gMnSc2	Jiří
Brdečky	Brdečka	k1gMnSc2	Brdečka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lipský	lipský	k2eAgMnSc1d1	lipský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
filmu	film	k1gInSc2	film
–	–	k?	–
pistolník	pistolník	k1gMnSc1	pistolník
popíjející	popíjející	k2eAgMnSc1d1	popíjející
zásadně	zásadně	k6eAd1	zásadně
jen	jen	k6eAd1	jen
Kolalokovu	Kolalokův	k2eAgFnSc4d1	Kolalokova
limonádu	limonáda	k1gFnSc4	limonáda
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
)	)	kIx)	)
–	–	k?	–
potírá	potírat	k5eAaImIp3nS	potírat
zlo	zlo	k1gNnSc4	zlo
na	na	k7c6	na
Divokém	divoký	k2eAgInSc6d1	divoký
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
pistolníky	pistolník	k1gMnPc4	pistolník
popíjející	popíjející	k2eAgFnSc2d1	popíjející
whisky	whisky	k1gFnSc2	whisky
<g/>
.	.	kIx.	.
</s>
<s>
Protihráčem	protihráč	k1gMnSc7	protihráč
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
gangster	gangster	k1gMnSc1	gangster
hledaný	hledaný	k2eAgMnSc1d1	hledaný
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neštítící	štítící	k2eNgFnSc4d1	neštítící
se	se	k3xPyFc4	se
ani	ani	k9	ani
těch	ten	k3xDgMnPc2	ten
nejhanebnějších	hanebný	k2eAgMnPc2d3	nejhanebnější
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
na	na	k7c6	na
nevinných	vinný	k2eNgFnPc6d1	nevinná
dívkách	dívka	k1gFnPc6	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Joe	Joe	k1gFnPc1	Joe
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
anglicky	anglicky	k6eAd1	anglicky
Džou	Džoa	k1gFnSc4	Džoa
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
kontextu	kontext	k1gInSc6	kontext
nabízí	nabízet	k5eAaImIp3nS	nabízet
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
nás	my	k3xPp1nPc4	my
zavádí	zavádět	k5eAaImIp3nS	zavádět
na	na	k7c4	na
Divoký	divoký	k2eAgInSc4d1	divoký
západ	západ	k1gInSc4	západ
do	do	k7c2	do
arizonského	arizonský	k2eAgMnSc2d1	arizonský
Stetson	Stetson	k1gInSc4	Stetson
City	city	k1gNnSc1	city
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
žije	žít	k5eAaImIp3nS	žít
typickým	typický	k2eAgInSc7d1	typický
životem	život	k1gInSc7	život
osady	osada	k1gFnSc2	osada
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
a	a	k8xC	a
penězích	peníze	k1gInPc6	peníze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
plynoucích	plynoucí	k2eAgFnPc2d1	plynoucí
<g/>
.	.	kIx.	.
</s>
<s>
Schází	scházet	k5eAaImIp3nS	scházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pestrá	pestrý	k2eAgFnSc1d1	pestrá
společnost	společnost	k1gFnSc1	společnost
kovbojů	kovboj	k1gMnPc2	kovboj
<g/>
,	,	kIx,	,
honáků	honák	k1gMnPc2	honák
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
dobytkářů	dobytkář	k1gMnPc2	dobytkář
<g/>
,	,	kIx,	,
dobrodruhů	dobrodruh	k1gMnPc2	dobrodruh
i	i	k8xC	i
podnikavců	podnikavec	k1gMnPc2	podnikavec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
scházejí	scházet	k5eAaImIp3nP	scházet
ve	v	k7c6	v
vyhlášeném	vyhlášený	k2eAgInSc6d1	vyhlášený
podniku	podnik	k1gInSc6	podnik
nazývaném	nazývaný	k2eAgInSc6d1	nazývaný
Trigger-Whiskey-Saloon	Trigger-Whiskey-Saloon	k1gNnSc4	Trigger-Whiskey-Saloon
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
majitelem	majitel	k1gMnSc7	majitel
je	být	k5eAaImIp3nS	být
Doug	Doug	k1gMnSc1	Doug
Badman	Badman	k1gMnSc1	Badman
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Deyl	Deyl	k1gMnSc1	Deyl
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikavý	podnikavý	k2eAgInSc1d1	podnikavý
a	a	k8xC	a
–	–	k?	–
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
odporně	odporně	k6eAd1	odporně
bohatý	bohatý	k2eAgMnSc1d1	bohatý
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
člověk	člověk	k1gMnSc1	člověk
alespoň	alespoň	k9	alespoň
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
omezené	omezený	k2eAgFnSc2d1	omezená
míry	míra	k1gFnSc2	míra
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
ctižádostí	ctižádost	k1gFnSc7	ctižádost
je	být	k5eAaImIp3nS	být
udržovat	udržovat	k5eAaImF	udržovat
svému	svůj	k3xOyFgInSc3	svůj
podniku	podnik	k1gInSc3	podnik
pověst	pověst	k1gFnSc1	pověst
kulturního	kulturní	k2eAgInSc2d1	kulturní
stánku	stánek	k1gInSc2	stánek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
si	se	k3xPyFc3	se
též	též	k9	též
vydržuje	vydržovat	k5eAaImIp3nS	vydržovat
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Tornádo	tornádo	k1gNnSc4	tornádo
Lou	Lou	k1gFnSc2	Lou
(	(	kIx(	(
<g/>
Tornádo	tornádo	k1gNnSc1	tornádo
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
Tornádo	tornádo	k1gNnSc1	tornádo
lvice	lvice	k1gFnSc1	lvice
<g/>
,	,	kIx,	,
Arizonská	arizonský	k2eAgFnSc1d1	Arizonská
pěnice	pěnice	k1gFnSc1	pěnice
Květa	Květa	k1gFnSc1	Květa
Fialová	Fialová	k1gFnSc1	Fialová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
přítomné	přítomný	k2eAgMnPc4d1	přítomný
muže	muž	k1gMnPc4	muž
včetně	včetně	k7c2	včetně
Douga	Doug	k1gMnSc2	Doug
magický	magický	k2eAgInSc4d1	magický
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
jejich	jejich	k3xOp3gFnSc2	jejich
touhy	touha	k1gFnSc2	touha
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
je	být	k5eAaImIp3nS	být
však	však	k9	však
podobnými	podobný	k2eAgInPc7d1	podobný
typy	typ	k1gInPc7	typ
mužů	muž	k1gMnPc2	muž
přesycena	přesytit	k5eAaPmNgFnS	přesytit
<g/>
,	,	kIx,	,
hledá	hledat	k5eAaImIp3nS	hledat
muže	muž	k1gMnPc4	muž
svých	svůj	k3xOyFgMnPc2	svůj
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ji	on	k3xPp3gFnSc4	on
učiní	učinit	k5eAaImIp3nS	učinit
jinou	jiný	k2eAgFnSc4d1	jiná
<g/>
,	,	kIx,	,
lepší	lepšit	k5eAaImIp3nS	lepšit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
stále	stále	k6eAd1	stále
sním	snít	k5eAaImIp1nS	snít
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přijde	přijít	k5eAaPmIp3nS	přijít
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
mého	můj	k1gMnSc2	můj
srdce	srdce	k1gNnSc1	srdce
šampión	šampión	k1gMnSc1	šampión
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
ve	v	k7c4	v
Stetson	Stetson	k1gNnSc4	Stetson
City	City	k1gFnSc2	City
reprezentovaném	reprezentovaný	k2eAgInSc6d1	reprezentovaný
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
TWS	TWS	kA	TWS
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
divoký	divoký	k2eAgInSc1d1	divoký
<g/>
.	.	kIx.	.
</s>
<s>
Neustálé	neustálý	k2eAgFnPc1d1	neustálá
rvačky	rvačka	k1gFnPc1	rvačka
<g/>
,	,	kIx,	,
přestřelky	přestřelek	k1gInPc1	přestřelek
<g/>
,	,	kIx,	,
pistolnické	pistolnický	k2eAgInPc1d1	pistolnický
souboje	souboj	k1gInPc1	souboj
a	a	k8xC	a
tříštící	tříštící	k2eAgMnSc1d1	tříštící
se	se	k3xPyFc4	se
nábytek	nábytek	k1gInSc1	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
samozřejmě	samozřejmě	k6eAd1	samozřejmě
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
proti	proti	k7c3	proti
tomuto	tento	k3xDgMnSc3	tento
démonu	démon	k1gMnSc3	démon
zde	zde	k6eAd1	zde
vede	vést	k5eAaImIp3nS	vést
misionářský	misionářský	k2eAgInSc4d1	misionářský
a	a	k8xC	a
marný	marný	k2eAgInSc4d1	marný
boj	boj	k1gInSc4	boj
pan	pan	k1gMnSc1	pan
Ezra	Ezra	k1gMnSc1	Ezra
Goodman	Goodman	k1gMnSc1	Goodman
(	(	kIx(	(
<g/>
Bohuš	Bohuš	k1gMnSc1	Bohuš
Záhorský	záhorský	k2eAgMnSc1d1	záhorský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
spolku	spolek	k1gInSc2	spolek
Arizonská	arizonský	k2eAgFnSc1d1	Arizonská
obroda	obroda	k1gFnSc1	obroda
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
Winnifred	Winnifred	k1gMnSc1	Winnifred
(	(	kIx(	(
<g/>
Olga	Olga	k1gFnSc1	Olga
Schoberová	Schoberová	k1gFnSc1	Schoberová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
snaha	snaha	k1gFnSc1	snaha
se	se	k3xPyFc4	se
v	v	k7c6	v
drsném	drsný	k2eAgNnSc6d1	drsné
prostředí	prostředí	k1gNnSc6	prostředí
setkává	setkávat	k5eAaImIp3nS	setkávat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
výsměchem	výsměch	k1gInSc7	výsměch
a	a	k8xC	a
pistolníci	pistolník	k1gMnPc1	pistolník
často	často	k6eAd1	často
neváhají	váhat	k5eNaImIp3nP	váhat
sáhnout	sáhnout	k5eAaPmF	sáhnout
též	též	k9	též
k	k	k7c3	k
hrubostem	hrubost	k1gFnPc3	hrubost
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
verbálním	verbální	k2eAgInSc7d1	verbální
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
dentálním	dentální	k2eAgMnPc3d1	dentální
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mé	můj	k3xOp1gFnPc4	můj
housle	housle	k1gFnPc4	housle
<g/>
!!	!!	k?	!!
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
jednoho	jeden	k4xCgInSc2	jeden
takového	takový	k3xDgInSc2	takový
konfliktu	konflikt	k1gInSc2	konflikt
prořízne	proříznout	k5eAaPmIp3nS	proříznout
husté	hustý	k2eAgNnSc4d1	husté
ovzduší	ovzduší	k1gNnSc4	ovzduší
obhroublé	obhroublý	k2eAgFnSc2d1	obhroublá
zábavy	zábava	k1gFnSc2	zábava
jasný	jasný	k2eAgInSc1d1	jasný
hlas	hlas	k1gInSc1	hlas
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
A	a	k8xC	a
mně	já	k3xPp1nSc3	já
sklenici	sklenice	k1gFnSc4	sklenice
Kolalokovy	Kolalokův	k2eAgFnPc4d1	Kolalokova
limonády	limonáda	k1gFnPc4	limonáda
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
muž	muž	k1gMnSc1	muž
pečlivě	pečlivě	k6eAd1	pečlivě
oděný	oděný	k2eAgInSc4d1	oděný
do	do	k7c2	do
čistě	čistě	k6eAd1	čistě
bílého	bílý	k2eAgInSc2d1	bílý
obleku	oblek	k1gInSc2	oblek
<g/>
,	,	kIx,	,
tvář	tvář	k1gFnSc4	tvář
dílem	dílem	k6eAd1	dílem
božské	božský	k2eAgNnSc1d1	božské
Afrodíté	Afrodítý	k2eAgNnSc1d1	Afrodíté
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
vysochána	vysochán	k2eAgFnSc1d1	vysochán
uměním	umění	k1gNnSc7	umění
Héfaistovým	Héfaistův	k2eAgMnPc3d1	Héfaistův
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Říkal	říkat	k5eAaImAgMnS	říkat
jste	být	k5eAaImIp2nP	být
sklenici	sklenice	k1gFnSc4	sklenice
Kolalokovy	Kolalokův	k2eAgFnSc2d1	Kolalokova
limonády	limonáda	k1gFnSc2	limonáda
<g/>
,	,	kIx,	,
cizinče	cizinec	k1gMnSc5	cizinec
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Pak	pak	k6eAd1	pak
musíte	muset	k5eAaImIp2nP	muset
být	být	k5eAaImF	být
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgMnSc1d1	nový
muž	muž	k1gMnSc1	muž
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
jisté	jistý	k2eAgFnSc3d1	jistá
pověsti	pověst	k1gFnSc3	pověst
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Limonádník	Limonádník	k1gMnSc1	Limonádník
si	se	k3xPyFc3	se
hned	hned	k6eAd1	hned
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
začátku	začátek	k1gInSc6	začátek
dobude	dobýt	k5eAaPmIp3nS	dobýt
sympatie	sympatie	k1gFnPc4	sympatie
<g/>
,	,	kIx,	,
když	když	k8xS	když
zesměšní	zesměšnit	k5eAaPmIp3nP	zesměšnit
hrubiána	hrubián	k1gMnSc4	hrubián
Grimpa	Grimpa	k1gFnSc1	Grimpa
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Hlinomaz	hlinomaz	k1gMnSc1	hlinomaz
<g/>
)	)	kIx)	)
bravurním	bravurní	k2eAgNnSc7d1	bravurní
sestřelením	sestřelení	k1gNnSc7	sestřelení
jeho	on	k3xPp3gInSc2	on
opasku	opasek	k1gInSc2	opasek
a	a	k8xC	a
současného	současný	k2eAgMnSc2d1	současný
"	"	kIx"	"
<g/>
odkalhotění	odkalhotění	k1gNnSc2	odkalhotění
<g/>
"	"	kIx"	"
tohoto	tento	k3xDgMnSc2	tento
primitivního	primitivní	k2eAgMnSc2d1	primitivní
násilníka	násilník	k1gMnSc2	násilník
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
zanechá	zanechat	k5eAaPmIp3nS	zanechat
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
ránu	rána	k1gFnSc4	rána
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
Lou	Lou	k1gFnSc2	Lou
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
mého	můj	k3xOp1gNnSc2	můj
srdce	srdce	k1gNnSc2	srdce
šampión	šampión	k1gMnSc1	šampión
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Následuje	následovat	k5eAaImIp3nS	následovat
odchod	odchod	k1gInSc4	odchod
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
vrátím	vrátit	k5eAaPmIp1nS	vrátit
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
přijde	přijít	k5eAaPmIp3nS	přijít
zákon	zákon	k1gInSc1	zákon
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Joe	Joe	k1gFnSc1	Joe
chystá	chystat	k5eAaImIp3nS	chystat
odcválat	odcválat	k5eAaPmF	odcválat
na	na	k7c6	na
svém	svůj	k3xOyFgMnSc6	svůj
běloušovi	bělouš	k1gMnSc6	bělouš
<g/>
,	,	kIx,	,
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	se	k3xPyFc4	se
blíže	blízce	k6eAd2	blízce
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Goodmanem	Goodman	k1gMnSc7	Goodman
a	a	k8xC	a
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gFnSc7	jeho
dcerou	dcera	k1gFnSc7	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
seznámení	seznámení	k1gNnSc2	seznámení
dojde	dojít	k5eAaPmIp3nS	dojít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
k	k	k7c3	k
přepadení	přepadení	k1gNnSc3	přepadení
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
bouřlivé	bouřlivý	k2eAgNnSc1d1	bouřlivé
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bezvýsledné	bezvýsledný	k2eAgFnSc3d1	bezvýsledná
přestřelce	přestřelka	k1gFnSc3	přestřelka
<g/>
.	.	kIx.	.
</s>
<s>
Neschopnost	neschopnost	k1gFnSc1	neschopnost
střelců	střelec	k1gMnPc2	střelec
komentuje	komentovat	k5eAaBmIp3nS	komentovat
Joe	Joe	k1gFnSc1	Joe
s	s	k7c7	s
nadhledem	nadhled	k1gInSc7	nadhled
a	a	k8xC	a
pohrdáním	pohrdání	k1gNnSc7	pohrdání
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Žabaři	žabař	k1gMnSc3	žabař
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
mrtvola	mrtvola	k1gFnSc1	mrtvola
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
pak	pak	k6eAd1	pak
během	během	k7c2	během
lehké	lehký	k2eAgFnSc2d1	lehká
konverzace	konverzace	k1gFnSc2	konverzace
bandity	bandita	k1gMnSc2	bandita
zneškodní	zneškodnit	k5eAaPmIp3nS	zneškodnit
<g/>
.	.	kIx.	.
</s>
<s>
Winnifred	Winnifred	k1gInSc1	Winnifred
je	být	k5eAaImIp3nS	být
okouzlena	okouzlen	k2eAgFnSc1d1	okouzlena
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
Joeho	Joe	k1gMnSc2	Joe
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
odjezdu	odjezd	k1gInSc6	odjezd
Joe	Joe	k1gMnSc1	Joe
přislíbí	přislíbit	k5eAaPmIp3nS	přislíbit
dodávku	dodávka	k1gFnSc4	dodávka
Kolalokovy	Kolalokův	k2eAgFnSc2d1	Kolalokova
limonády	limonáda	k1gFnSc2	limonáda
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
pana	pan	k1gMnSc2	pan
Goodmana	Goodman	k1gMnSc2	Goodman
dovolávají	dovolávat	k5eAaImIp3nP	dovolávat
i	i	k9	i
okouzlení	okouzlený	k2eAgMnPc1d1	okouzlený
pistolníci	pistolník	k1gMnPc1	pistolník
<g/>
.	.	kIx.	.
</s>
<s>
Krátké	Krátké	k2eAgNnSc1d1	Krátké
období	období	k1gNnSc1	období
slávy	sláva	k1gFnSc2	sláva
zázračného	zázračný	k2eAgInSc2d1	zázračný
nápoje	nápoj	k1gInSc2	nápoj
však	však	k9	však
brzy	brzy	k6eAd1	brzy
opět	opět	k6eAd1	opět
opadává	opadávat	k5eAaImIp3nS	opadávat
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
starých	starý	k2eAgFnPc2d1	stará
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Whiskey	Whiske	k1gMnPc4	Whiske
teče	téct	k5eAaImIp3nS	téct
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
opilci	opilec	k1gMnPc1	opilec
se	se	k3xPyFc4	se
motají	motat	k5eAaImIp3nP	motat
<g/>
,	,	kIx,	,
pistolnické	pistolnický	k2eAgInPc1d1	pistolnický
souboje	souboj	k1gInPc1	souboj
skomírají	skomírat	k5eAaImIp3nP	skomírat
na	na	k7c6	na
neschopnosti	neschopnost	k1gFnSc6	neschopnost
střelců	střelec	k1gMnPc2	střelec
byť	byť	k8xS	byť
jen	jen	k9	jen
tasit	tasit	k5eAaPmF	tasit
kolt	kolt	k1gInSc4	kolt
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
hrůzu	hrůza	k1gFnSc4	hrůza
sleduje	sledovat	k5eAaImIp3nS	sledovat
pan	pan	k1gMnSc1	pan
Goodman	Goodman	k1gMnSc1	Goodman
s	s	k7c7	s
žalostí	žalost	k1gFnSc7	žalost
a	a	k8xC	a
modlí	modlit	k5eAaImIp3nS	modlit
se	se	k3xPyFc4	se
za	za	k7c2	za
Kolaloku	Kolalok	k1gInSc2	Kolalok
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tu	tu	k6eAd1	tu
přijde	přijít	k5eAaPmIp3nS	přijít
telegram	telegram	k1gInSc4	telegram
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Goodman	Goodman	k1gMnSc1	Goodman
je	být	k5eAaImIp3nS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
zastupitelstvím	zastupitelství	k1gNnSc7	zastupitelství
firmy	firma	k1gFnSc2	firma
Kolalok	Kolalok	k1gInSc1	Kolalok
&	&	k?	&
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
zakládá	zakládat	k5eAaImIp3nS	zakládat
Kolaloka	Kolaloka	k1gFnSc1	Kolaloka
Saloon	saloon	k1gInSc4	saloon
<g/>
,	,	kIx,	,
zařízení	zařízení	k1gNnSc4	zařízení
vzhledem	vzhled	k1gInSc7	vzhled
i	i	k8xC	i
odvázaností	odvázanost	k1gFnSc7	odvázanost
hostů	host	k1gMnPc2	host
evokujících	evokující	k2eAgMnPc2d1	evokující
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
úžasného	úžasný	k2eAgNnSc2d1	úžasné
umění	umění	k1gNnSc2	umění
Joeova	Joeov	k1gInSc2	Joeov
přebírá	přebírat	k5eAaImIp3nS	přebírat
veškerou	veškerý	k3xTgFnSc4	veškerý
klientelu	klientela	k1gFnSc4	klientela
TWS	TWS	kA	TWS
a	a	k8xC	a
zásadně	zásadně	k6eAd1	zásadně
kultivuje	kultivovat	k5eAaImIp3nS	kultivovat
pistolnickou	pistolnický	k2eAgFnSc4d1	pistolnická
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
žádné	žádný	k3yNgInPc1	žádný
souboje	souboj	k1gInPc1	souboj
bez	bez	k7c2	bez
mrtvol	mrtvola	k1gFnPc2	mrtvola
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Když	když	k8xS	když
střílí	střílet	k5eAaImIp3nS	střílet
konzument	konzument	k1gMnSc1	konzument
KL	kl	kA	kl
<g/>
,	,	kIx,	,
netřeba	netřeba	k6eAd1	netřeba
volat	volat	k5eAaImF	volat
lékaře	lékař	k1gMnPc4	lékař
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Doug	Doug	k1gMnSc1	Doug
Badman	Badman	k1gMnSc1	Badman
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lou	Lou	k1gFnSc7	Lou
a	a	k8xC	a
Grimpem	Grimp	k1gInSc7	Grimp
obývají	obývat	k5eAaImIp3nP	obývat
prázdný	prázdný	k2eAgInSc4d1	prázdný
saloon	saloon	k1gInSc4	saloon
<g/>
,	,	kIx,	,
Doug	Doug	k1gInSc1	Doug
sní	sníst	k5eAaPmIp3nS	sníst
o	o	k7c6	o
turné	turné	k1gNnSc6	turné
s	s	k7c7	s
Lou	Lou	k1gFnSc7	Lou
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Lou	Lou	k1gFnSc1	Lou
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
limonádníkovi	limonádník	k1gMnSc6	limonádník
a	a	k8xC	a
Grimpo	Grimpa	k1gFnSc5	Grimpa
spí	spát	k5eAaImIp3nP	spát
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
když	když	k8xS	když
by	by	kYmCp3nS	by
těžko	těžko	k6eAd1	těžko
někdo	někdo	k3yInSc1	někdo
čekal	čekat	k5eAaImAgMnS	čekat
zvrat	zvrat	k1gInSc4	zvrat
<g/>
,	,	kIx,	,
vchází	vcházet	k5eAaImIp3nS	vcházet
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
nová	nový	k2eAgFnSc1d1	nová
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Horác	Horác	k1gMnSc1	Horác
alias	alias	k9	alias
Hogofogo	Hogofogo	k1gMnSc1	Hogofogo
(	(	kIx(	(
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
)	)	kIx)	)
přináší	přinášet	k5eAaImIp3nS	přinášet
Dougovi	Doug	k1gMnSc3	Doug
důkaz	důkaz	k1gInSc4	důkaz
sourozeneckého	sourozenecký	k2eAgNnSc2d1	sourozenecké
pouta	pouto	k1gNnSc2	pouto
a	a	k8xC	a
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
obnovit	obnovit	k5eAaPmF	obnovit
slávu	sláva	k1gFnSc4	sláva
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
Joe	Joe	k1gFnSc1	Joe
v	v	k7c6	v
TWS	TWS	kA	TWS
<g/>
,	,	kIx,	,
také	také	k9	také
Horác	Horác	k1gMnSc1	Horác
užije	užít	k5eAaPmIp3nS	užít
naprosto	naprosto	k6eAd1	naprosto
stejných	stejná	k1gFnPc2	stejná
(	(	kIx(	(
<g/>
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
drsnějších	drsný	k2eAgInPc2d2	drsnější
<g/>
)	)	kIx)	)
způsobů	způsob	k1gInPc2	způsob
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Odhalí	odhalit	k5eAaPmIp3nS	odhalit
se	se	k3xPyFc4	se
jako	jako	k9	jako
Hogofogo	Hogofogo	k6eAd1	Hogofogo
se	s	k7c7	s
dvanácti	dvanáct	k4xCc7	dvanáct
zářezy	zářez	k1gInPc7	zářez
na	na	k7c6	na
pažbě	pažba	k1gFnSc6	pažba
svého	svůj	k3xOyFgMnSc4	svůj
derringeru	derringrat	k5eAaPmIp1nS	derringrat
hledaný	hledaný	k2eAgMnSc1d1	hledaný
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
státech	stát	k1gInPc6	stát
a	a	k8xC	a
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
potíží	potíž	k1gFnPc2	potíž
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zvedli	zvednout	k5eAaPmAgMnP	zvednout
od	od	k7c2	od
sklenic	sklenice	k1gFnPc2	sklenice
s	s	k7c7	s
brčky	brček	k1gInPc7	brček
a	a	k8xC	a
vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
podniku	podnik	k1gInSc2	podnik
Douga	Doug	k1gMnSc2	Doug
Badmana	Badman	k1gMnSc2	Badman
(	(	kIx(	(
<g/>
Do	do	k7c2	do
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
"	"	kIx"	"
<g/>
zlomené	zlomený	k2eAgNnSc1d1	zlomené
srdce	srdce	k1gNnSc1	srdce
vyhojí	vyhojit	k5eAaPmIp3nS	vyhojit
svůj	svůj	k3xOyFgInSc4	svůj
bol	bol	k1gInSc4	bol
sklenkou	sklenka	k1gFnSc7	sklenka
té	ten	k3xDgFnSc2	ten
zázračné	zázračný	k2eAgFnSc2d1	zázračná
Trigger	Trigger	k1gInSc4	Trigger
Whiskey	Whiskeum	k1gNnPc7	Whiskeum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
se	se	k3xPyFc4	se
neopomene	opomenout	k5eNaPmIp3nS	opomenout
zamilovat	zamilovat	k5eAaPmF	zamilovat
do	do	k7c2	do
panenské	panenský	k2eAgFnSc2d1	panenská
Winnifred	Winnifred	k1gInSc4	Winnifred
<g/>
.	.	kIx.	.
</s>
<s>
Hogofogo	Hogofogo	k1gMnSc1	Hogofogo
není	být	k5eNaImIp3nS	být
zvyklý	zvyklý	k2eAgMnSc1d1	zvyklý
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
a	a	k8xC	a
při	při	k7c6	při
první	první	k4xOgFnSc6	první
příležitosti	příležitost	k1gFnSc6	příležitost
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
Win	Win	k1gFnSc1	Win
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
zmocnit	zmocnit	k5eAaPmF	zmocnit
<g/>
.	.	kIx.	.
</s>
<s>
Dílem	dílem	k6eAd1	dílem
neskutečně	skutečně	k6eNd1	skutečně
šťastné	šťastný	k2eAgFnSc2d1	šťastná
náhody	náhoda	k1gFnSc2	náhoda
právě	právě	k6eAd1	právě
cválá	cválat	k5eAaImIp3nS	cválat
Joe	Joe	k1gFnSc1	Joe
místem	místem	k6eAd1	místem
zvaným	zvaný	k2eAgInSc7d1	zvaný
Fatamorgána	Fatamorgán	k2eAgFnSc1d1	Fatamorgán
Valley	Valley	k1gInPc7	Valley
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Pyramid	pyramida	k1gFnPc2	pyramida
a	a	k8xC	a
Tower	Towra	k1gFnPc2	Towra
Bridge	Bridge	k1gFnPc2	Bridge
zde	zde	k6eAd1	zde
spatřuje	spatřovat	k5eAaImIp3nS	spatřovat
výjev	výjev	k1gInSc4	výjev
ze	z	k7c2	z
stetsoncityského	stetsoncityský	k2eAgInSc2d1	stetsoncityský
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Obrací	obracet	k5eAaImIp3nS	obracet
svého	svůj	k3xOyFgMnSc4	svůj
koníka	koník	k1gMnSc4	koník
a	a	k8xC	a
zběsilým	zběsilý	k2eAgNnSc7d1	zběsilé
tempem	tempo	k1gNnSc7	tempo
cválá	cválat	k5eAaImIp3nS	cválat
zachránit	zachránit	k5eAaPmF	zachránit
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
chlípníkem	chlípník	k1gMnSc7	chlípník
zatočí	zatočit	k5eAaPmIp3nS	zatočit
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
Winnifred	Winnifred	k1gInSc1	Winnifred
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostává	dostávat	k5eAaImIp3nS	dostávat
slibu	slib	k1gInSc2	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
samozřejmě	samozřejmě	k6eAd1	samozřejmě
až	až	k9	až
po	po	k7c6	po
řádných	řádný	k2eAgFnPc6d1	řádná
oddavkách	oddavky	k1gFnPc6	oddavky
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
o	o	k7c6	o
konci	konec	k1gInSc6	konec
TWS	TWS	kA	TWS
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
situaci	situace	k1gFnSc4	situace
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
přestřelce	přestřelka	k1gFnSc3	přestřelka
mezi	mezi	k7c7	mezi
Joem	Joe	k1gNnSc7	Joe
a	a	k8xC	a
Horácem	Horác	k1gMnSc7	Horác
lstivě	lstivě	k6eAd1	lstivě
převlečeným	převlečený	k2eAgNnSc7d1	převlečené
za	za	k7c4	za
černocha	černoch	k1gMnSc4	černoch
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
má	mít	k5eAaImIp3nS	mít
nakonec	nakonec	k6eAd1	nakonec
samozřejmě	samozřejmě	k6eAd1	samozřejmě
navrch	navrch	k6eAd1	navrch
Joe	Joe	k1gMnSc1	Joe
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodně	rozhodně	k6eAd1	rozhodně
však	však	k9	však
nehodlá	hodlat	k5eNaImIp3nS	hodlat
prolévat	prolévat	k5eAaImF	prolévat
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
protivníka	protivník	k1gMnSc4	protivník
potupit	potupit	k5eAaPmF	potupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
kritický	kritický	k2eAgInSc4d1	kritický
moment	moment	k1gInSc4	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
být	být	k5eAaImF	být
vše	všechen	k3xTgNnSc4	všechen
vyřešeno	vyřešen	k2eAgNnSc4d1	vyřešeno
<g/>
,	,	kIx,	,
nešťastně	šťastně	k6eNd1	šťastně
čichne	čichnout	k5eAaPmIp3nS	čichnout
k	k	k7c3	k
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
upadá	upadat	k5eAaImIp3nS	upadat
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Zachraňuje	zachraňovat	k5eAaImIp3nS	zachraňovat
jej	on	k3xPp3gInSc4	on
obětavá	obětavý	k2eAgFnSc1d1	obětavá
Lou	Lou	k1gFnSc1	Lou
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přijde	přijít	k5eAaPmIp3nS	přijít
Joe	Joe	k1gFnSc1	Joe
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
vyjeví	vyjevit	k5eAaPmIp3nS	vyjevit
mu	on	k3xPp3gMnSc3	on
ubohá	ubohý	k2eAgFnSc1d1	ubohá
pěnice	pěnice	k1gFnSc1	pěnice
své	svůj	k3xOyFgFnSc2	svůj
city	city	k1gFnSc2	city
<g/>
.	.	kIx.	.
</s>
<s>
Pistolník	pistolník	k1gMnSc1	pistolník
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zcela	zcela	k6eAd1	zcela
chladný	chladný	k2eAgInSc1d1	chladný
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ženu	žena	k1gFnSc4	žena
jako	jako	k8xC	jako
Lou	Lou	k1gFnSc4	Lou
nemá	mít	k5eNaImIp3nS	mít
než	než	k8xS	než
slova	slovo	k1gNnPc1	slovo
lítosti	lítost	k1gFnSc2	lítost
a	a	k8xC	a
pohrdání	pohrdání	k1gNnSc2	pohrdání
<g/>
.	.	kIx.	.
</s>
<s>
Odchází	odcházet	k5eAaImIp3nS	odcházet
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
nese	nést	k5eAaImIp3nS	nést
přísaha	přísaha	k1gFnSc1	přísaha
pomsty	pomsta	k1gFnSc2	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
Horác	Horác	k1gMnSc1	Horác
<g/>
,	,	kIx,	,
Doug	Doug	k1gMnSc1	Doug
a	a	k8xC	a
Lou	Lou	k1gMnSc1	Lou
vymyslí	vymyslet	k5eAaPmIp3nS	vymyslet
ďábelský	ďábelský	k2eAgInSc4d1	ďábelský
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Horác	Horác	k1gMnSc1	Horác
převlečen	převlečen	k2eAgMnSc1d1	převlečen
za	za	k7c2	za
starého	starý	k2eAgInSc2d1	starý
slepého	slepý	k2eAgInSc2d1	slepý
ladiče	ladič	k1gMnPc4	ladič
pian	piano	k1gNnPc2	piano
unese	unést	k5eAaPmIp3nS	unést
Winnifred	Winnifred	k1gInSc4	Winnifred
a	a	k8xC	a
sdělí	sdělit	k5eAaPmIp3nP	sdělit
Joeovi	Joeův	k2eAgMnPc1d1	Joeův
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
dostavit	dostavit	k5eAaPmF	dostavit
do	do	k7c2	do
opuštěného	opuštěný	k2eAgInSc2d1	opuštěný
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Joe	Joe	k?	Joe
neváhá	váhat	k5eNaImIp3nS	váhat
<g/>
,	,	kIx,	,
obleče	obléct	k5eAaPmIp3nS	obléct
si	se	k3xPyFc3	se
teplé	teplý	k2eAgNnSc4d1	teplé
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
(	(	kIx(	(
<g/>
neboť	neboť	k8xC	neboť
noci	noc	k1gFnPc1	noc
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
chladné	chladný	k2eAgNnSc1d1	chladné
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyrazí	vyrazit	k5eAaPmIp3nP	vyrazit
osvobodit	osvobodit	k5eAaPmF	osvobodit
Win	Win	k1gFnSc4	Win
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
hlídají	hlídat	k5eAaImIp3nP	hlídat
Grimpo	Grimpa	k1gFnSc5	Grimpa
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Hlinomaz	hlinomaz	k1gMnSc1	hlinomaz
<g/>
)	)	kIx)	)
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
Kidové	Kidus	k1gMnPc1	Kidus
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Effa	Effa	k1gMnSc1	Effa
a	a	k8xC	a
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
Lou	Lou	k1gFnSc1	Lou
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
chce	chtít	k5eAaImIp3nS	chtít
být	být	k5eAaImF	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
konci	konec	k1gInSc3	konec
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
city	cit	k1gInPc4	cit
a	a	k8xC	a
pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
zavrhl	zavrhnout	k5eAaPmAgInS	zavrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Joe	Joe	k?	Joe
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
skokanským	skokanský	k2eAgFnPc3d1	skokanská
schopnostem	schopnost	k1gFnPc3	schopnost
společnost	společnost	k1gFnSc4	společnost
překvapí	překvapit	k5eAaPmIp3nS	překvapit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neprohlédne	prohlédnout	k5eNaPmIp3nS	prohlédnout
lest	lest	k1gFnSc1	lest
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
protivníci	protivník	k1gMnPc1	protivník
totiž	totiž	k9	totiž
využili	využít	k5eAaPmAgMnP	využít
Joeovy	Joeův	k2eAgFnPc4d1	Joeova
slabosti	slabost	k1gFnPc4	slabost
ke	k	k7c3	k
Kolalokově	Kolalokův	k2eAgFnSc3d1	Kolalokova
limonádě	limonáda	k1gFnSc3	limonáda
a	a	k8xC	a
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
láhev	láhev	k1gFnSc4	láhev
naplnili	naplnit	k5eAaPmAgMnP	naplnit
pravou	pravá	k1gFnSc7	pravá
whiskey	whiskea	k1gMnSc2	whiskea
<g/>
.	.	kIx.	.
</s>
<s>
Joe	Joe	k?	Joe
skutečně	skutečně	k6eAd1	skutečně
neodolá	odolat	k5eNaPmIp3nS	odolat
a	a	k8xC	a
napije	napít	k5eAaBmIp3nS	napít
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
upadá	upadat	k5eAaImIp3nS	upadat
opět	opět	k6eAd1	opět
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
promptně	promptně	k6eAd1	promptně
přivázán	přivázat	k5eAaPmNgMnS	přivázat
k	k	k7c3	k
mučícímu	mučící	k2eAgInSc3d1	mučící
kůlu	kůl	k1gInSc3	kůl
a	a	k8xC	a
bídáci	bídák	k1gMnPc1	bídák
chystají	chystat	k5eAaImIp3nP	chystat
děsivá	děsivý	k2eAgNnPc4d1	děsivé
muka	muka	k1gNnPc4	muka
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
má	mít	k5eAaImIp3nS	mít
přihlížet	přihlížet	k5eAaImF	přihlížet
i	i	k9	i
Win	Win	k1gMnSc1	Win
<g/>
.	.	kIx.	.
</s>
<s>
Grimpo	Grimpa	k1gFnSc5	Grimpa
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
kumpány	kumpány	k?	kumpány
zapojí	zapojit	k5eAaPmIp3nS	zapojit
veškerou	veškerý	k3xTgFnSc4	veškerý
svou	svůj	k3xOyFgFnSc4	svůj
fantazii	fantazie	k1gFnSc4	fantazie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
překvapivě	překvapivě	k6eAd1	překvapivě
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
limonádník	limonádník	k1gInSc1	limonádník
trpí	trpět	k5eAaImIp3nS	trpět
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mučeníčko	mučeníčko	k1gNnSc1	mučeníčko
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
moje	můj	k3xOp1gNnSc1	můj
potěšeníčko	potěšeníčko	k1gNnSc1	potěšeníčko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
na	na	k7c6	na
čem	co	k3yRnSc6	co
si	se	k3xPyFc3	se
bezesporu	bezesporu	k9	bezesporu
nejvíce	hodně	k6eAd3	hodně
zakládal	zakládat	k5eAaImAgInS	zakládat
–	–	k?	–
jeho	on	k3xPp3gInSc4	on
upravený	upravený	k2eAgInSc4d1	upravený
zjev	zjev	k1gInSc4	zjev
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
ničeno	ničen	k2eAgNnSc1d1	ničeno
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
mučení	mučení	k1gNnSc2	mučení
si	se	k3xPyFc3	se
Lou	Lou	k1gMnSc1	Lou
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Joea	Joea	k1gMnSc1	Joea
i	i	k9	i
přes	přes	k7c4	přes
všechno	všechen	k3xTgNnSc4	všechen
zažité	zažitý	k2eAgNnSc4d1	zažité
příkoří	příkoří	k1gNnSc4	příkoří
stále	stále	k6eAd1	stále
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Nastraží	nastražit	k5eAaPmIp3nS	nastražit
tyranům	tyran	k1gMnPc3	tyran
léčku	léčka	k1gFnSc4	léčka
a	a	k8xC	a
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Joe	Joe	k?	Joe
je	být	k5eAaImIp3nS	být
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
obejmout	obejmout	k5eAaPmF	obejmout
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
snoubenkou	snoubenka	k1gFnSc7	snoubenka
<g/>
.	.	kIx.	.
</s>
<s>
Lou	Lou	k?	Lou
trpí	trpět	k5eAaImIp3nS	trpět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žehná	žehnat	k5eAaImIp3nS	žehnat
jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
Lou	Lou	k1gFnSc1	Lou
pochopila	pochopit	k5eAaPmAgFnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
žít	žít	k5eAaImF	žít
zlem	zlo	k1gNnSc7	zlo
a	a	k8xC	a
příkořími	příkoří	k1gNnPc7	příkoří
na	na	k7c6	na
ostatních	ostatní	k2eAgFnPc6d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Doug	Doug	k1gInSc1	Doug
se	se	k3xPyFc4	se
trápí	trápit	k5eAaImIp3nS	trápit
výčitkami	výčitka	k1gFnPc7	výčitka
<g/>
,	,	kIx,	,
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
je	být	k5eAaImIp3nS	být
Horác	Horác	k1gMnSc1	Horác
netvor	netvor	k1gMnSc1	netvor
<g/>
.	.	kIx.	.
</s>
<s>
Nedokáže	dokázat	k5eNaPmIp3nS	dokázat
se	se	k3xPyFc4	se
však	však	k9	však
vzbouřit	vzbouřit	k5eAaPmF	vzbouřit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
chystá	chystat	k5eAaImIp3nS	chystat
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
Winnifred	Winnifred	k1gInSc1	Winnifred
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
hodlá	hodlat	k5eAaImIp3nS	hodlat
učinit	učinit	k5eAaPmF	učinit
obětí	oběť	k1gFnPc2	oběť
své	svůj	k3xOyFgFnSc2	svůj
chlípné	chlípný	k2eAgFnSc2d1	chlípná
touhy	touha	k1gFnSc2	touha
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k6eAd1	místo
očekávané	očekávaný	k2eAgInPc4d1	očekávaný
Winnifred	Winnifred	k1gInSc4	Winnifred
však	však	k9	však
vchází	vcházet	k5eAaImIp3nS	vcházet
do	do	k7c2	do
pečlivě	pečlivě	k6eAd1	pečlivě
připraveného	připravený	k2eAgNnSc2d1	připravené
hnízdečka	hnízdečko	k1gNnSc2	hnízdečko
lásky	láska	k1gFnSc2	láska
obávaný	obávaný	k2eAgMnSc1d1	obávaný
pistolník	pistolník	k1gMnSc1	pistolník
oděn	odět	k5eAaPmNgInS	odět
celý	celý	k2eAgInSc1d1	celý
do	do	k7c2	do
černé	černá	k1gFnSc2	černá
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mstiteli	mstitel	k1gMnSc3	mstitel
sluší	slušet	k5eAaImIp3nS	slušet
lépe	dobře	k6eAd2	dobře
tato	tento	k3xDgFnSc1	tento
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Joe	Joe	k?	Joe
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
Horáce	Horác	k1gMnSc2	Horác
k	k	k7c3	k
partii	partie	k1gFnSc3	partie
pokeru	poker	k1gInSc2	poker
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
svého	svůj	k3xOyFgMnSc4	svůj
soupeře	soupeř	k1gMnSc4	soupeř
naprosto	naprosto	k6eAd1	naprosto
znemožní	znemožnit	k5eAaPmIp3nS	znemožnit
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
Horáce	Horác	k1gMnSc4	Horác
zcela	zcela	k6eAd1	zcela
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
zmáčknout	zmáčknout	k5eAaPmF	zmáčknout
spoušť	spoušť	k1gFnSc4	spoušť
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
spokojuje	spokojovat	k5eAaImIp3nS	spokojovat
s	s	k7c7	s
jediným	jediné	k1gNnSc7	jediné
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
předává	předávat	k5eAaImIp3nS	předávat
padouchovi	padouch	k1gMnSc3	padouch
papír	papír	k1gInSc4	papír
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kolaloka	Kolaloka	k1gFnSc1	Kolaloka
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc4d1	jediný
nápoj	nápoj	k1gInSc4	nápoj
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Horác	Horác	k1gMnSc1	Horác
<g/>
,	,	kIx,	,
místo	místo	k6eAd1	místo
aby	aby	kYmCp3nP	aby
pod	pod	k7c7	pod
tíhou	tíha	k1gFnSc7	tíha
dojetí	dojetí	k1gNnSc2	dojetí
nad	nad	k7c7	nad
šlechetností	šlechetnost	k1gFnSc7	šlechetnost
svého	svůj	k3xOyFgMnSc2	svůj
soka	sok	k1gMnSc2	sok
odprosil	odprosit	k5eAaPmAgMnS	odprosit
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
další	další	k2eAgInSc1d1	další
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
špinavých	špinavý	k2eAgInPc2d1	špinavý
triků	trik	k1gInPc2	trik
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
poněkolikáté	poněkolikáté	k6eAd1	poněkolikáté
tak	tak	k9	tak
Joe	Joe	k1gFnSc1	Joe
padá	padat	k5eAaImIp3nS	padat
<g/>
,	,	kIx,	,
zasažen	zasažen	k2eAgMnSc1d1	zasažen
tentokrát	tentokrát	k6eAd1	tentokrát
proudem	proud	k1gInSc7	proud
dobře	dobře	k6eAd1	dobře
vychlazeného	vychlazený	k2eAgNnSc2d1	vychlazené
šampaňského	šampaňské	k1gNnSc2	šampaňské
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
těla	tělo	k1gNnSc2	tělo
pak	pak	k9	pak
Horác	Horác	k1gMnSc1	Horác
vyprázdní	vyprázdnit	k5eAaPmIp3nS	vyprázdnit
oba	dva	k4xCgInPc4	dva
Joeovy	Joeov	k1gInPc4	Joeov
Smith	Smith	k1gMnSc1	Smith
and	and	k?	and
Wessony	Wessona	k1gFnSc2	Wessona
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
šťastném	šťastný	k2eAgNnSc6d1	šťastné
vítězství	vítězství	k1gNnSc6	vítězství
spěchá	spěchat	k5eAaImIp3nS	spěchat
Horác	Horác	k1gMnSc1	Horác
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
právě	právě	k6eAd1	právě
nachází	nacházet	k5eAaImIp3nS	nacházet
Winnifred	Winnifred	k1gInSc4	Winnifred
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
matičkou	matička	k1gFnSc7	matička
podělila	podělit	k5eAaPmAgFnS	podělit
o	o	k7c4	o
novinky	novinka	k1gFnPc4	novinka
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
pro	pro	k7c4	pro
špinavé	špinavý	k2eAgInPc4d1	špinavý
úmysly	úmysl	k1gInPc4	úmysl
Horácovy	Horácův	k2eAgInPc4d1	Horácův
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
volná	volný	k2eAgFnSc1d1	volná
<g/>
.	.	kIx.	.
</s>
<s>
Vrhne	vrhnout	k5eAaImIp3nS	vrhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
Win	Win	k1gFnSc4	Win
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přispěchavší	přispěchavší	k2eAgFnPc1d1	přispěchavší
Lou	Lou	k1gFnPc1	Lou
mu	on	k3xPp3gNnSc3	on
hatí	hatit	k5eAaImIp3nP	hatit
plány	plán	k1gInPc1	plán
<g/>
.	.	kIx.	.
</s>
<s>
Horác	Horác	k1gMnSc1	Horác
ji	on	k3xPp3gFnSc4	on
bez	bez	k7c2	bez
milosti	milost	k1gFnSc2	milost
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Náhodou	náhodou	k6eAd1	náhodou
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
i	i	k9	i
Doug	Doug	k1gInSc1	Doug
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vida	vida	k?	vida
hrůzný	hrůzný	k2eAgInSc4d1	hrůzný
čin	čin	k1gInSc4	čin
bratrův	bratrův	k2eAgInSc4d1	bratrův
<g/>
,	,	kIx,	,
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
vývrtku	vývrtka	k1gFnSc4	vývrtka
a	a	k8xC	a
vrazí	vrazit	k5eAaPmIp3nS	vrazit
ji	on	k3xPp3gFnSc4	on
lotrovi	lotrův	k2eAgMnPc1d1	lotrův
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
<s>
Odměnou	odměna	k1gFnSc7	odměna
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
kulka	kulka	k1gFnSc1	kulka
z	z	k7c2	z
derringeru	derringer	k1gInSc2	derringer
umírajícího	umírající	k2eAgInSc2d1	umírající
<g/>
.	.	kIx.	.
</s>
<s>
Horáce	Horác	k1gMnSc4	Horác
ani	ani	k8xC	ani
na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
neopouštějí	opouštět	k5eNaImIp3nP	opouštět
černé	černý	k2eAgFnPc1d1	černá
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
obrátí	obrátit	k5eAaPmIp3nP	obrátit
zbraň	zbraň	k1gFnSc4	zbraň
i	i	k9	i
proti	proti	k7c3	proti
bezbranné	bezbranný	k2eAgFnSc3d1	bezbranná
Win	Win	k1gFnSc3	Win
<g/>
.	.	kIx.	.
</s>
<s>
Nervydrásající	nervydrásající	k2eAgFnSc4d1	nervydrásající
scénu	scéna	k1gFnSc4	scéna
přeruší	přerušit	k5eAaPmIp3nS	přerušit
až	až	k9	až
výstřel	výstřel	k1gInSc1	výstřel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
je	být	k5eAaImIp3nS	být
prosté	prostý	k2eAgNnSc1d1	prosté
<g/>
,	,	kIx,	,
stačilo	stačit	k5eAaBmAgNnS	stačit
pár	pár	k4xCyI	pár
kapek	kapka	k1gFnPc2	kapka
Kolaloky	Kolalok	k1gInPc1	Kolalok
(	(	kIx(	(
<g/>
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
že	že	k8xS	že
se	se	k3xPyFc4	se
dokázal	dokázat	k5eAaPmAgMnS	dokázat
polít	polít	k5eAaPmF	polít
<g/>
,	,	kIx,	,
pistolník	pistolník	k1gMnSc1	pistolník
decentně	decentně	k6eAd1	decentně
pomlčí	pomlčet	k5eAaPmIp3nS	pomlčet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
dostává	dostávat	k5eAaImIp3nS	dostávat
děsivého	děsivý	k2eAgNnSc2d1	děsivé
zjištění	zjištění	k1gNnSc2	zjištění
<g/>
.	.	kIx.	.
</s>
<s>
Hromada	hromada	k1gFnSc1	hromada
mrtvých	mrtvý	k2eAgNnPc2d1	mrtvé
těl	tělo	k1gNnPc2	tělo
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
prozrazují	prozrazovat	k5eAaImIp3nP	prozrazovat
to	ten	k3xDgNnSc1	ten
kakaové	kakaový	k2eAgFnPc1d1	kakaová
skvrny	skvrna	k1gFnPc1	skvrna
velikosti	velikost	k1gFnSc2	velikost
mexického	mexický	k2eAgInSc2d1	mexický
dolaru	dolar	k1gInSc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Joe	Joe	k?	Joe
tedy	tedy	k9	tedy
polévá	polévat	k5eAaImIp3nS	polévat
bezvládná	bezvládný	k2eAgFnSc1d1	bezvládná
těla	tělo	k1gNnSc2	tělo
Kolalokou	Kolaloký	k2eAgFnSc4d1	Kolaloký
a	a	k8xC	a
ta	ten	k3xDgNnPc1	ten
opět	opět	k6eAd1	opět
radostně	radostně	k6eAd1	radostně
vstávají	vstávat	k5eAaImIp3nP	vstávat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
šťastné	šťastný	k2eAgFnSc2d1	šťastná
scény	scéna	k1gFnSc2	scéna
přichází	přicházet	k5eAaImIp3nS	přicházet
též	též	k9	též
tatíček	tatíček	k1gMnSc1	tatíček
Kolalok	Kolalok	k1gInSc1	Kolalok
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
všech	všecek	k3xTgFnPc2	všecek
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
kromě	kromě	k7c2	kromě
Win	Win	k1gFnSc2	Win
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nálezu	nález	k1gInSc3	nález
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
navrch	navrch	k6eAd1	navrch
přijde	přijít	k5eAaPmIp3nS	přijít
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
výhře	výhra	k1gFnSc6	výhra
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
<g/>
.	.	kIx.	.
</s>
<s>
Nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
Horác	Horác	k1gMnSc1	Horác
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
potlačit	potlačit	k5eAaPmF	potlačit
lítost	lítost	k1gFnSc4	lítost
nad	nad	k7c7	nad
svým	svůj	k3xOyFgNnSc7	svůj
notorickým	notorický	k2eAgNnSc7d1	notorické
bídáctvím	bídáctví	k1gNnSc7	bídáctví
<g/>
.	.	kIx.	.
</s>
<s>
Dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
útěchy	útěcha	k1gFnPc1	útěcha
od	od	k7c2	od
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
Joea	Joeus	k1gMnSc2	Joeus
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
praví	pravý	k2eAgMnPc1d1	pravý
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ne	ne	k9	ne
tak	tak	k9	tak
synu	syn	k1gMnSc3	syn
<g/>
!	!	kIx.	!
</s>
<s>
Náš	náš	k3xOp1gInSc1	náš
obchod	obchod	k1gInSc1	obchod
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
talenty	talent	k1gInPc4	talent
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
<g/>
!	!	kIx.	!
</s>
<s>
Padouch	padouch	k1gMnSc1	padouch
nebo	nebo	k8xC	nebo
hrdina	hrdina	k1gMnSc1	hrdina
–	–	k?	–
my	my	k3xPp1nPc1	my
jsme	být	k5eAaImIp1nP	být
jedna	jeden	k4xCgNnPc4	jeden
rodina	rodina	k1gFnSc1	rodina
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
šťastný	šťastný	k2eAgInSc4d1	šťastný
konec	konec	k1gInSc4	konec
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Brdečka	Brdečka	k1gMnSc1	Brdečka
–	–	k?	–
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
román	román	k1gInSc4	román
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Brdečka	Brdečka	k1gMnSc1	Brdečka
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lipský	lipský	k2eAgMnSc1d1	lipský
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Novotný	Novotný	k1gMnSc1	Novotný
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Rychlík	Rychlík	k1gMnSc1	Rychlík
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Hála	Hála	k1gMnSc1	Hála
Hraje	hrát	k5eAaImIp3nS	hrát
<g/>
:	:	kIx,	:
Orchestr	orchestr	k1gInSc1	orchestr
Karla	Karel	k1gMnSc2	Karel
Vlacha	Vlach	k1gMnSc2	Vlach
<g/>
,	,	kIx,	,
Filmový	filmový	k2eAgInSc1d1	filmový
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Dr	dr	kA	dr
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
Koníček	Koníček	k1gMnSc1	Koníček
Texty	text	k1gInPc1	text
písní	píseň	k1gFnPc2	píseň
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Brdečka	Brdečka	k1gMnSc1	Brdečka
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Kopta	Kopt	k1gMnSc2	Kopt
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rychlík	Rychlík	k1gMnSc1	Rychlík
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
–	–	k?	–
obchodní	obchodní	k2eAgMnSc1d1	obchodní
cestující	cestující	k1gMnSc1	cestující
firmy	firma	k1gFnSc2	firma
Kolalok	Kolalok	k1gInSc1	Kolalok
&	&	k?	&
syn	syn	k1gMnSc1	syn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
(	(	kIx(	(
<g/>
Horác	Horác	k1gMnSc1	Horác
Badman	Badman	k1gMnSc1	Badman
alias	alias	k9	alias
Hogofogo	Hogofogo	k1gMnSc1	Hogofogo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Květa	Květa	k1gFnSc1	Květa
Fialová	Fialová	k1gFnSc1	Fialová
(	(	kIx(	(
<g/>
Arizonská	arizonský	k2eAgFnSc1d1	Arizonská
pěnice	pěnice	k1gFnSc1	pěnice
Tornádo	tornádo	k1gNnSc1	tornádo
Lou	Lou	k1gFnSc2	Lou
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Schoberová	Schoberová	k1gFnSc1	Schoberová
(	(	kIx(	(
<g/>
Winnifred	Winnifred	k1gInSc1	Winnifred
Goodmanová	Goodmanová	k1gFnSc1	Goodmanová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Deyl	Deyl	k1gMnSc1	Deyl
ml.	ml.	kA	ml.
(	(	kIx(	(
<g/>
Doug	Doug	k1gMnSc1	Doug
Badman	Badman	k1gMnSc1	Badman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bohuš	Bohuš	k1gMnSc1	Bohuš
Záhorský	záhorský	k2eAgMnSc1d1	záhorský
(	(	kIx(	(
<g/>
Ezra	Ezra	k1gMnSc1	Ezra
Goodman	Goodman	k1gMnSc1	Goodman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hlinomaz	hlinomaz	k1gMnSc1	hlinomaz
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Grimpo	Grimpa	k1gFnSc5	Grimpa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Effa	Effa	k1gMnSc1	Effa
(	(	kIx(	(
<g/>
Pancho	Pancha	k1gMnSc5	Pancha
Kid	Kid	k1gMnSc5	Kid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
(	(	kIx(	(
<g/>
Kojot	kojot	k1gMnSc1	kojot
Kid	Kid	k1gMnSc1	Kid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eman	Eman	k1gMnSc1	Eman
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
pianista	pianista	k1gMnSc1	pianista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Menšík	Menšík	k1gMnSc1	Menšík
(	(	kIx(	(
<g/>
barman	barman	k1gMnSc1	barman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Lír	lír	k1gInSc1	lír
(	(	kIx(	(
<g/>
barman	barman	k1gMnSc1	barman
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Steimar	Steimar	k1gMnSc1	Steimar
(	(	kIx(	(
<g/>
pan	pan	k1gMnSc1	pan
Kolalok	Kolalok	k1gInSc1	Kolalok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Štercl	Štercl	k1gMnSc1	Štercl
(	(	kIx(	(
<g/>
poštmistr	poštmistr	k1gMnSc1	poštmistr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lukeš	Lukeš	k1gMnSc1	Lukeš
(	(	kIx(	(
<g/>
šerif	šerif	k1gMnSc1	šerif
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Dvorský	Dvorský	k1gMnSc1	Dvorský
(	(	kIx(	(
<g/>
hluchý	hluchý	k2eAgMnSc1d1	hluchý
stařík	stařík	k1gMnSc1	stařík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Nedbal	Nedbal	k1gMnSc1	Nedbal
(	(	kIx(	(
<g/>
karbaník	karbaník	k1gMnSc1	karbaník
<g/>
)	)	kIx)	)
Zpěv	zpěv	k1gInSc1	zpěv
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Yvetta	Yvetta	k1gFnSc1	Yvetta
Simonová	Simonová	k1gFnSc1	Simonová
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
Veselá	Veselá	k1gFnSc1	Veselá
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
,	,	kIx,	,
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
Choreografie	choreografie	k1gFnSc1	choreografie
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Koníček	Koníček	k1gMnSc1	Koníček
Střih	střih	k1gInSc1	střih
<g/>
:	:	kIx,	:
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hájek	Hájek	k1gMnSc1	Hájek
Zvuk	zvuk	k1gInSc1	zvuk
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Vlček	Vlček	k1gMnSc1	Vlček
Zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
<g/>
:	:	kIx,	:
Bohumír	Bohumír	k1gMnSc1	Bohumír
Brunclík	Brunclík	k1gMnSc1	Brunclík
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Jedlička	Jedlička	k1gMnSc1	Jedlička
Výprava	výprava	k1gMnSc1	výprava
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Rulík	rulík	k1gInSc4	rulík
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Osvald	Osvald	k1gMnSc1	Osvald
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Krbec	krbec	k1gInSc1	krbec
Návrhy	návrh	k1gInPc1	návrh
kostýmů	kostým	k1gInPc2	kostým
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Brdečka	Brdečka	k1gMnSc1	Brdečka
<g/>
,	,	kIx,	,
Fernand	Fernand	k1gInSc4	Fernand
Vácha	Vácha	k1gMnSc1	Vácha
<g/>
,	,	kIx,	,
Nita	Nit	k2eAgFnSc1d1	Nita
Romanečová	Romanečová	k1gFnSc1	Romanečová
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Lackingerová	Lackingerová	k1gFnSc1	Lackingerová
Výtvarník	výtvarník	k1gMnSc1	výtvarník
dekorací	dekorace	k1gFnPc2	dekorace
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Škvor	Škvor	k1gMnSc1	Škvor
Animace	animace	k1gFnSc1	animace
<g/>
:	:	kIx,	:
Břetislav	Břetislav	k1gMnSc1	Břetislav
Pojar	Pojar	k1gMnSc1	Pojar
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Brdečka	Brdečka	k1gMnSc1	Brdečka
Vedoucí	vedoucí	k1gMnSc1	vedoucí
výroby	výroba	k1gFnSc2	výroba
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g />
.	.	kIx.	.
</s>
<s>
Jílovec	jílovec	k1gInSc1	jílovec
<g/>
,	,	kIx,	,
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
Další	další	k2eAgInPc1d1	další
údaje	údaj	k1gInPc1	údaj
<g/>
:	:	kIx,	:
černobílý	černobílý	k2eAgMnSc1d1	černobílý
<g/>
,	,	kIx,	,
virážovaný	virážovaný	k2eAgMnSc1d1	virážovaný
<g/>
,	,	kIx,	,
87	[number]	k4	87
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
western	western	k1gInSc1	western
Výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
scény	scéna	k1gFnPc1	scéna
filmu	film	k1gInSc2	film
byly	být	k5eAaImAgFnP	být
charakteristicky	charakteristicky	k6eAd1	charakteristicky
zabarveny	zabarvit	k5eAaPmNgInP	zabarvit
–	–	k?	–
tzv.	tzv.	kA	tzv.
virážování	virážování	k1gNnSc4	virážování
Počet	počet	k1gInSc1	počet
diváků	divák	k1gMnPc2	divák
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
4	[number]	k4	4
556	[number]	k4	556
352	[number]	k4	352
Počet	počet	k1gInSc1	počet
diváků	divák	k1gMnPc2	divák
televizních	televizní	k2eAgMnPc2d1	televizní
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
majitelů	majitel	k1gMnPc2	majitel
DVD	DVD	kA	DVD
či	či	k8xC	či
videokazet	videokazeta	k1gFnPc2	videokazeta
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
pouze	pouze	k6eAd1	pouze
odhadovat	odhadovat	k5eAaImF	odhadovat
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
dodnes	dodnes	k6eAd1	dodnes
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
filmovým	filmový	k2eAgFnPc3d1	filmová
perlám	perla	k1gFnPc3	perla
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
všemi	všecek	k3xTgFnPc7	všecek
českými	český	k2eAgFnPc7d1	Česká
televizemi	televize	k1gFnPc7	televize
často	často	k6eAd1	často
reprízován	reprízovat	k5eAaImNgInS	reprízovat
Maketa	maketa	k1gFnSc1	maketa
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
filmového	filmový	k2eAgNnSc2d1	filmové
městečka	městečko	k1gNnSc2	městečko
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
na	na	k7c6	na
louce	louka	k1gFnSc6	louka
za	za	k7c7	za
barrandovskými	barrandovský	k2eAgInPc7d1	barrandovský
ateliéry	ateliér	k1gInPc7	ateliér
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
úsporných	úsporný	k2eAgInPc2d1	úsporný
důvodů	důvod	k1gInPc2	důvod
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
pouze	pouze	k6eAd1	pouze
čelní	čelní	k2eAgInPc1d1	čelní
stěny	stěn	k1gInPc1	stěn
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
celé	celý	k2eAgInPc4d1	celý
domy	dům	k1gInPc4	dům
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
kritické	kritický	k2eAgFnSc6d1	kritická
době	doba	k1gFnSc6	doba
nebyl	být	k5eNaImAgInS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dostatek	dostatek	k1gInSc1	dostatek
hřebíků	hřebík	k1gInPc2	hřebík
<g/>
,	,	kIx,	,
v	v	k7c6	v
kulisách	kulisa	k1gFnPc6	kulisa
westernového	westernový	k2eAgNnSc2d1	westernové
městečka	městečko	k1gNnSc2	městečko
posléze	posléze	k6eAd1	posléze
natáčeli	natáčet	k5eAaImAgMnP	natáčet
západoněmečtí	západoněmecký	k2eAgMnPc1d1	západoněmecký
filmaři	filmař	k1gMnPc1	filmař
film	film	k1gInSc4	film
Zlatokopové	zlatokop	k1gMnPc1	zlatokop
z	z	k7c2	z
Arkansasu	Arkansas	k1gInSc2	Arkansas
s	s	k7c7	s
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
světově	světově	k6eAd1	světově
proslulou	proslulý	k2eAgFnSc7d1	proslulá
filmovou	filmový	k2eAgFnSc7d1	filmová
hvězdou	hvězda	k1gFnSc7	hvězda
Olinkou	Olinka	k1gFnSc7	Olinka
Schoberovou	Schoberová	k1gFnSc7	Schoberová
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vrcholné	vrcholný	k2eAgFnSc6d1	vrcholná
scéně	scéna	k1gFnSc6	scéna
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
pak	pak	k6eAd1	pak
celé	celý	k2eAgNnSc1d1	celé
westernové	westernový	k2eAgNnSc1d1	westernové
městečko	městečko	k1gNnSc1	městečko
shoří	shořet	k5eAaPmIp3nS	shořet
<g/>
.	.	kIx.	.
</s>
<s>
Diváky	divák	k1gMnPc4	divák
velmi	velmi	k6eAd1	velmi
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
i	i	k9	i
celková	celkový	k2eAgFnSc1d1	celková
vysoká	vysoký	k2eAgFnSc1d1	vysoká
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
stránka	stránka	k1gFnSc1	stránka
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
jednak	jednak	k8xC	jednak
historicky	historicky	k6eAd1	historicky
velmi	velmi	k6eAd1	velmi
věrná	věrný	k2eAgFnSc1d1	věrná
i	i	k8xC	i
zcela	zcela	k6eAd1	zcela
přesná	přesný	k2eAgNnPc4d1	přesné
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
sama	sám	k3xTgNnPc4	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
hned	hned	k6eAd1	hned
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
humorných	humorný	k2eAgInPc2d1	humorný
či	či	k8xC	či
úsměvných	úsměvný	k2eAgInPc2d1	úsměvný
prvků	prvek	k1gInPc2	prvek
plně	plně	k6eAd1	plně
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
žánru	žánr	k1gInSc6	žánr
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
osobně	osobně	k6eAd1	osobně
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
vytvořit	vytvořit	k5eAaPmF	vytvořit
autor	autor	k1gMnSc1	autor
námětu	námět	k1gInSc2	námět
a	a	k8xC	a
scénáře	scénář	k1gInSc2	scénář
Jiří	Jiří	k1gMnSc1	Jiří
Brdečka	Brdečka	k1gMnSc1	Brdečka
<g/>
;	;	kIx,	;
řada	řada	k1gFnSc1	řada
výroku	výrok	k1gInSc2	výrok
z	z	k7c2	z
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
vžila	vžít	k5eAaPmAgFnS	vžít
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
často	často	k6eAd1	často
užívanými	užívaný	k2eAgInPc7d1	užívaný
–	–	k?	–
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Padouch	padouch	k1gMnSc1	padouch
nebo	nebo	k8xC	nebo
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
jedna	jeden	k4xCgFnSc1	jeden
rodina	rodina	k1gFnSc1	rodina
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
mučeníčko	mučeníčko	k1gNnSc1	mučeníčko
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
moje	můj	k3xOp1gNnSc1	můj
potěšeníčko	potěšeníčko	k1gNnSc1	potěšeníčko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
mého	můj	k1gMnSc2	můj
srdce	srdce	k1gNnSc1	srdce
šampión	šampión	k1gMnSc1	šampión
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Když	když	k8xS	když
střílí	střílet	k5eAaImIp3nS	střílet
konzument	konzument	k1gMnSc1	konzument
KL	kl	kA	kl
<g/>
,	,	kIx,	,
netřeba	netřeba	k6eAd1	netřeba
volat	volat	k5eAaImF	volat
lékaře	lékař	k1gMnPc4	lékař
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
vrátím	vrátit	k5eAaPmIp1nS	vrátit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
přijde	přijít	k5eAaPmIp3nS	přijít
zákon	zákon	k1gInSc1	zákon
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Diváky	Diváky	k1gInPc4	Diváky
dále	daleko	k6eAd2	daleko
také	také	k9	také
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
kvalitně	kvalitně	k6eAd1	kvalitně
provedené	provedený	k2eAgInPc1d1	provedený
filmové	filmový	k2eAgInPc1d1	filmový
triky	trik	k1gInPc1	trik
–	–	k?	–
opět	opět	k6eAd1	opět
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
humorně	humorně	k6eAd1	humorně
laděné	laděný	k2eAgFnPc1d1	laděná
Ve	v	k7c6	v
vedlejších	vedlejší	k2eAgFnPc6d1	vedlejší
a	a	k8xC	a
epizodních	epizodní	k2eAgFnPc6d1	epizodní
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
mihlo	mihnout	k5eAaPmAgNnS	mihnout
mnoho	mnoho	k4c1	mnoho
známých	známý	k2eAgMnPc2d1	známý
herců	herec	k1gMnPc2	herec
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Menšík	Menšík	k1gMnSc1	Menšík
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Štercl	Štercl	k1gMnSc1	Štercl
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Lír	lír	k1gInSc1	lír
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Eman	Eman	k1gMnSc1	Eman
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Srstka	srstka	k1gFnSc1	srstka
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Nedbal	Nedbal	k1gMnSc1	Nedbal
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Štekl	Štekl	k?	Štekl
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Steimar	Steimar	k1gMnSc1	Steimar
<g/>
,	,	kIx,	,
Stela	Stela	k1gFnSc1	Stela
Zázvorková	zázvorkový	k2eAgFnSc1d1	Zázvorková
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Cortéz	Cortéza	k1gFnPc2	Cortéza
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
,	,	kIx,	,
Juraj	Juraj	k1gMnSc1	Juraj
Herz	Herz	k1gMnSc1	Herz
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
Řada	řada	k1gFnSc1	řada
exteriérů	exteriér	k1gInPc2	exteriér
byla	být	k5eAaImAgFnS	být
natáčena	natáčen	k2eAgFnSc1d1	natáčena
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
vápencovém	vápencový	k2eAgInSc6d1	vápencový
lomu	lom	k1gInSc6	lom
u	u	k7c2	u
Mořiny	Mořina	k1gFnSc2	Mořina
poblíž	poblíž	k7c2	poblíž
Karlštejna	Karlštejn	k1gInSc2	Karlštejn
(	(	kIx(	(
<g/>
asi	asi	k9	asi
25	[number]	k4	25
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
romantický	romantický	k2eAgInSc1d1	romantický
lom	lom	k1gInSc1	lom
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Velká	velký	k2eAgFnSc1d1	velká
Amerika	Amerika	k1gFnSc1	Amerika
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
místem	místo	k1gNnSc7	místo
filmařů	filmař	k1gMnPc2	filmař
<g/>
,	,	kIx,	,
trempů	tremp	k1gMnPc2	tremp
a	a	k8xC	a
nudistů	nudista	k1gMnPc2	nudista
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zejména	zejména	k9	zejména
z	z	k7c2	z
rekreačních	rekreační	k2eAgInPc2d1	rekreační
a	a	k8xC	a
zábavních	zábavní	k2eAgInPc2d1	zábavní
důvodů	důvod	k1gInPc2	důvod
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
své	svůj	k3xOyFgFnSc3	svůj
době	doba	k1gFnSc3	doba
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
i	i	k9	i
velkým	velký	k2eAgNnSc7d1	velké
nasazením	nasazení	k1gNnSc7	nasazení
mnoha	mnoho	k4c2	mnoho
kaskadérů	kaskadér	k1gMnPc2	kaskadér
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
při	při	k7c6	při
rvačkách	rvačka	k1gFnPc6	rvačka
<g/>
,	,	kIx,	,
pistolových	pistolový	k2eAgInPc6d1	pistolový
soubojích	souboj	k1gInPc6	souboj
a	a	k8xC	a
jezdeckých	jezdecký	k2eAgFnPc6d1	jezdecká
scénách	scéna	k1gFnPc6	scéna
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
řadu	řada	k1gFnSc4	řada
amatérských	amatérský	k2eAgMnPc2d1	amatérský
sportovců	sportovec	k1gMnPc2	sportovec
zejména	zejména	k9	zejména
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
tzv.	tzv.	kA	tzv.
úpolových	úpolův	k2eAgInPc2d1	úpolův
sportů	sport	k1gInPc2	sport
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čeští	český	k2eAgMnPc1d1	český
kaskadéři	kaskadér	k1gMnPc1	kaskadér
dnes	dnes	k6eAd1	dnes
právě	právě	k9	právě
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
úplný	úplný	k2eAgInSc1d1	úplný
prvopočátek	prvopočátek	k1gInSc1	prvopočátek
českého	český	k2eAgNnSc2d1	české
profesionálního	profesionální	k2eAgNnSc2d1	profesionální
kaskadérství	kaskadérství	k1gNnSc2	kaskadérství
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
promítnut	promítnut	k2eAgInSc4d1	promítnut
i	i	k8xC	i
slavnému	slavný	k2eAgMnSc3d1	slavný
americkému	americký	k2eAgMnSc3d1	americký
westernovému	westernový	k2eAgMnSc3d1	westernový
herci	herec	k1gMnSc3	herec
Henry	Henry	k1gMnSc1	Henry
Fondovi	Fonda	k1gMnSc3	Fonda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
touto	tento	k3xDgFnSc7	tento
parodií	parodie	k1gFnSc7	parodie
na	na	k7c4	na
filmy	film	k1gInPc4	film
z	z	k7c2	z
divokého	divoký	k2eAgInSc2d1	divoký
západu	západ	k1gInSc2	západ
nadšen	nadšen	k2eAgMnSc1d1	nadšen
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
použila	použít	k5eAaPmAgFnS	použít
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
fotografii	fotografia	k1gFnSc4	fotografia
z	z	k7c2	z
filmu	film	k1gInSc2	film
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
kampaň	kampaň	k1gFnSc4	kampaň
proti	proti	k7c3	proti
neplatičům	neplatič	k1gMnPc3	neplatič
koncesionářských	koncesionářský	k2eAgInPc2d1	koncesionářský
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
na	na	k7c4	na
fotografii	fotografia	k1gFnSc4	fotografia
byl	být	k5eAaImAgInS	být
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
<g/>
)	)	kIx)	)
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
zažaloval	zažalovat	k5eAaPmAgInS	zažalovat
<g/>
,	,	kIx,	,
soud	soud	k1gInSc1	soud
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
a	a	k8xC	a
vysoudil	vysoudit	k5eAaPmAgInS	vysoudit
150	[number]	k4	150
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
filmu	film	k1gInSc2	film
náležely	náležet	k5eAaImAgFnP	náležet
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
mezi	mezi	k7c7	mezi
hity	hit	k1gInPc7	hit
a	a	k8xC	a
stále	stále	k6eAd1	stále
si	se	k3xPyFc3	se
drží	držet	k5eAaImIp3nS	držet
popularitu	popularita	k1gFnSc4	popularita
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gMnPc7	jejich
interprety	interpret	k1gMnPc7	interpret
náleží	náležet	k5eAaImIp3nS	náležet
i	i	k9	i
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
či	či	k8xC	či
Yvetta	Yvetta	k1gFnSc1	Yvetta
Simonová	Simonová	k1gFnSc1	Simonová
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
Tornádo	tornádo	k1gNnSc1	tornádo
Lou	Lou	k1gFnSc1	Lou
"	"	kIx"	"
<g/>
Když	když	k8xS	když
v	v	k7c6	v
baru	bar	k1gInSc6	bar
houstne	houstnout	k5eAaImIp3nS	houstnout
dým	dým	k1gInSc1	dým
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Whisky	whisky	k1gFnSc4	whisky
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
moje	můj	k3xOp1gNnSc4	můj
gusto	gusto	k1gNnSc4	gusto
<g/>
"	"	kIx"	"
nezpívala	zpívat	k5eNaImAgFnS	zpívat
Květa	Květa	k1gFnSc1	Květa
Fialová	Fialová	k1gFnSc1	Fialová
<g/>
,	,	kIx,	,
hlas	hlas	k1gInSc1	hlas
jí	on	k3xPp3gFnSc2	on
propůjčila	propůjčit	k5eAaPmAgFnS	propůjčit
Yvetta	Yvetta	k1gFnSc1	Yvetta
Simonová	Simonová	k1gFnSc1	Simonová
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
,	,	kIx,	,
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ve	v	k7c6	v
filmu	film	k1gInSc6	film
zpívá	zpívat	k5eAaImIp3nS	zpívat
Winnifred	Winnifred	k1gInSc1	Winnifred
(	(	kIx(	(
<g/>
Olga	Olga	k1gFnSc1	Olga
Schoberová	Schoberová	k1gFnSc1	Schoberová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
Jarmila	Jarmila	k1gFnSc1	Jarmila
Veselá	Veselá	k1gFnSc1	Veselá
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
Stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
mušli	mušle	k1gFnSc4	mušle
na	na	k7c4	na
XII	XII	kA	XII
<g/>
.	.	kIx.	.
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
San	San	k1gMnSc6	San
Sebastianu	Sebastian	k1gMnSc6	Sebastian
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Panamě	Panama	k1gFnSc6	Panama
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
cenu	cena	k1gFnSc4	cena
Sfinga	sfinga	k1gFnSc1	sfinga
za	za	k7c4	za
roli	role	k1gFnSc4	role
padoucha	padouch	k1gMnSc2	padouch
Horáce	Horác	k1gMnSc2	Horác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hudebním	hudební	k2eAgNnSc6d1	hudební
divadle	divadlo	k1gNnSc6	divadlo
Karlín	Karlín	k1gInSc4	Karlín
se	s	k7c7	s
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
konaly	konat	k5eAaImAgFnP	konat
dvě	dva	k4xCgFnPc1	dva
premiéry	premiéra	k1gFnPc1	premiéra
nového	nový	k2eAgInSc2d1	nový
českého	český	k2eAgInSc2d1	český
muzikálu	muzikál	k1gInSc2	muzikál
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přímo	přímo	k6eAd1	přímo
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ovšem	ovšem	k9	ovšem
mírně	mírně	k6eAd1	mírně
upravené	upravený	k2eAgInPc1d1	upravený
texty	text	k1gInPc1	text
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
drobné	drobný	k2eAgFnPc1d1	drobná
změny	změna	k1gFnPc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
řada	řada	k1gFnSc1	řada
českých	český	k2eAgInPc2d1	český
herců	herc	k1gInPc2	herc
a	a	k8xC	a
hereček	herečka	k1gFnPc2	herečka
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Brožovou	Brožová	k1gFnSc7	Brožová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
postavu	postav	k1gInSc3	postav
Tornádo	tornádo	k1gNnSc4	tornádo
Lou	Lou	k1gMnSc2	Lou
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
ČR	ČR	kA	ČR
i	i	k8xC	i
malá	malý	k2eAgFnSc1d1	malá
soukromá	soukromý	k2eAgFnSc1d1	soukromá
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
stanice	stanice	k1gFnSc1	stanice
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Rádio	rádio	k1gNnSc1	rádio
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
humoristicky	humoristicky	k6eAd1	humoristicky
laděnou	laděný	k2eAgFnSc4d1	laděná
rozhlasovou	rozhlasový	k2eAgFnSc4d1	rozhlasová
stanici	stanice	k1gFnSc4	stanice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kromě	kromě	k7c2	kromě
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
byla	být	k5eAaImAgFnS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
především	především	k9	především
na	na	k7c4	na
inteligentní	inteligentní	k2eAgFnSc4d1	inteligentní
recesi	recese	k1gFnSc4	recese
<g/>
,	,	kIx,	,
vkusnou	vkusný	k2eAgFnSc4d1	vkusná
legraci	legrace	k1gFnSc4	legrace
a	a	k8xC	a
společenskou	společenský	k2eAgFnSc4d1	společenská
satiru	satira	k1gFnSc4	satira
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
pojetím	pojetí	k1gNnSc7	pojetí
a	a	k8xC	a
programovým	programový	k2eAgNnSc7d1	programové
zaměřením	zaměření	k1gNnSc7	zaměření
se	se	k3xPyFc4	se
právě	právě	k9	právě
toto	tento	k3xDgNnSc1	tento
rádio	rádio	k1gNnSc1	rádio
dosti	dosti	k6eAd1	dosti
výrazně	výrazně	k6eAd1	výrazně
odlišovalo	odlišovat	k5eAaImAgNnS	odlišovat
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
malých	malá	k1gFnPc2	malá
soukromých	soukromý	k2eAgFnPc2d1	soukromá
rádií	rádio	k1gNnPc2	rádio
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
v	v	k7c6	v
ČR	ČR	kA	ČR
působilo	působit	k5eAaImAgNnS	působit
po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
Popularitu	popularita	k1gFnSc4	popularita
nápoje	nápoj	k1gInSc2	nápoj
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
využít	využít	k5eAaPmF	využít
dvě	dva	k4xCgFnPc1	dva
firmy	firma	k1gFnPc1	firma
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
Kola	Kola	k1gFnSc1	Kola
loka	lokum	k1gNnPc4	lokum
v	v	k7c6	v
názvu	název	k1gInSc6	název
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
firmy	firma	k1gFnPc1	firma
sídlí	sídlet	k5eAaImIp3nP	sídlet
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
zabývají	zabývat	k5eAaImIp3nP	zabývat
se	se	k3xPyFc4	se
výrobou	výroba	k1gFnSc7	výroba
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
krátce	krátce	k6eAd1	krátce
Kola	Kola	k1gFnSc1	Kola
loku	lok	k1gInSc2	lok
také	také	k9	také
firma	firma	k1gFnSc1	firma
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
aneb	aneb	k?	aneb
Koňská	koňský	k2eAgFnSc1d1	koňská
opera	opera	k1gFnSc1	opera
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
aneb	aneb	k?	aneb
Koňská	koňský	k2eAgFnSc1d1	koňská
opera	opera	k1gFnSc1	opera
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gFnSc7	Joe
aneb	aneb	k?	aneb
Koňská	koňský	k2eAgFnSc1d1	koňská
opera	opera	k1gFnSc1	opera
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
aneb	aneb	k?	aneb
Koňská	koňský	k2eAgFnSc1d1	koňská
opera	opera	k1gFnSc1	opera
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc1	Movie
Database	Databasa	k1gFnSc3	Databasa
MeteleskuBlesku	MeteleskuBlesk	k1gInSc2	MeteleskuBlesk
–	–	k?	–
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
Hlášky	hláška	k1gFnSc2	hláška
z	z	k7c2	z
filmu	film	k1gInSc2	film
v	v	k7c6	v
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
obrázky	obrázek	k1gInPc4	obrázek
Český	český	k2eAgInSc4d1	český
muzikál	muzikál	k1gInSc4	muzikál
–	–	k?	–
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gFnSc7	Joe
aneb	aneb	k?	aneb
Koňská	koňský	k2eAgFnSc1d1	koňská
opera	opera	k1gFnSc1	opera
</s>
