<s>
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
aneb	aneb	k?	aneb
Koňská	koňský	k2eAgFnSc1d1	koňská
opera	opera	k1gFnSc1	opera
je	být	k5eAaImIp3nS	být
československá	československý	k2eAgFnSc1d1	Československá
filmová	filmový	k2eAgFnSc1d1	filmová
hudební	hudební	k2eAgFnSc1d1	hudební
komedie	komedie	k1gFnSc1	komedie
resp.	resp.	kA	resp.
osobitá	osobitý	k2eAgFnSc1d1	osobitá
parodie	parodie	k1gFnSc1	parodie
westernu	western	k1gInSc2	western
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
Jiřího	Jiří	k1gMnSc2	Jiří
Brdečky	Brdečka	k1gMnSc2	Brdečka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lipský	lipský	k2eAgMnSc1d1	lipský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
