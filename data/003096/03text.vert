<s>
Korýši	korýš	k1gMnPc1	korýš
(	(	kIx(	(
<g/>
Crustacea	Crustacea	k1gMnSc1	Crustacea
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
velká	velký	k2eAgFnSc1d1	velká
skupina	skupina	k1gFnSc1	skupina
členovců	členovec	k1gMnPc2	členovec
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
klasifikovaná	klasifikovaný	k2eAgFnSc1d1	klasifikovaná
jako	jako	k8xS	jako
podkmen	podkmen	k1gInSc1	podkmen
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
fylogenetickými	fylogenetický	k2eAgFnPc7d1	fylogenetická
analýzami	analýza	k1gFnPc7	analýza
byla	být	k5eAaImAgFnS	být
odhalena	odhalen	k2eAgFnSc1d1	odhalena
její	její	k3xOp3gFnSc4	její
nepřirozenost	nepřirozenost	k1gFnSc4	nepřirozenost
<g/>
)	)	kIx)	)
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
asi	asi	k9	asi
52	[number]	k4	52
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
i	i	k9	i
velmi	velmi	k6eAd1	velmi
známé	známý	k2eAgInPc1d1	známý
druhy	druh	k1gInPc1	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
krabi	krab	k1gMnPc1	krab
<g/>
,	,	kIx,	,
humři	humr	k1gMnPc1	humr
či	či	k8xC	či
krevety	kreveta	k1gFnPc1	kreveta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
některé	některý	k3yIgInPc4	některý
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgInPc4d1	malý
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
buchanka	buchanka	k1gFnSc1	buchanka
či	či	k8xC	či
perloočka	perloočka	k1gFnSc1	perloočka
<g/>
.	.	kIx.	.
</s>
<s>
Korýši	korýš	k1gMnPc1	korýš
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
sladké	sladký	k2eAgFnSc6d1	sladká
či	či	k8xC	či
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
adaptovaly	adaptovat	k5eAaBmAgFnP	adaptovat
i	i	k9	i
k	k	k7c3	k
životu	život	k1gInSc3	život
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
(	(	kIx(	(
<g/>
svinka	svinka	k1gFnSc1	svinka
<g/>
,	,	kIx,	,
stínka	stínka	k1gFnSc1	stínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
korýši	korýš	k1gMnPc7	korýš
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
karcinologie	karcinologie	k1gFnSc1	karcinologie
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
malakostratologie	malakostratologie	k1gFnSc2	malakostratologie
či	či	k8xC	či
krustaceologie	krustaceologie	k1gFnSc2	krustaceologie
<g/>
.	.	kIx.	.
</s>
<s>
Korýši	korýš	k1gMnPc1	korýš
mají	mít	k5eAaImIp3nP	mít
jeden	jeden	k4xCgInSc4	jeden
pár	pár	k1gInSc4	pár
složených	složený	k2eAgNnPc2d1	složené
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
2	[number]	k4	2
páry	pára	k1gFnSc2	pára
tykadel	tykadlo	k1gNnPc2	tykadlo
<g/>
,	,	kIx,	,
1	[number]	k4	1
pár	pár	k4xCyI	pár
kusadel	kusadla	k1gNnPc2	kusadla
a	a	k8xC	a
2	[number]	k4	2
páry	pára	k1gFnSc2	pára
čelistí	čelist	k1gFnPc2	čelist
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc1	tři
páry	pár	k1gInPc1	pár
ústních	ústní	k2eAgFnPc2d1	ústní
částí	část	k1gFnPc2	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hruď	hruď	k1gFnSc1	hruď
a	a	k8xC	a
zadeček	zadeček	k1gInSc1	zadeček
nese	nést	k5eAaImIp3nS	nést
množství	množství	k1gNnSc1	množství
přívěsků	přívěsek	k1gInPc2	přívěsek
(	(	kIx(	(
<g/>
končetin	končetina	k1gFnPc2	končetina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
žaberních	žaberní	k2eAgFnPc2d1	žaberní
nožek	nožka	k1gFnPc2	nožka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
telson	telson	k1gInSc1	telson
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
korýšů	korýš	k1gMnPc2	korýš
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
hrudi	hruď	k1gFnSc2	hruď
(	(	kIx(	(
<g/>
thorax	thorax	k1gInSc1	thorax
<g/>
)	)	kIx)	)
a	a	k8xC	a
zadečku	zadeček	k1gInSc2	zadeček
(	(	kIx(	(
<g/>
abdomen	abdomen	k1gInSc1	abdomen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
jmenované	jmenovaná	k1gFnPc4	jmenovaná
části	část	k1gFnPc4	část
však	však	k9	však
často	často	k6eAd1	často
srůstají	srůstat	k5eAaImIp3nP	srůstat
v	v	k7c6	v
hlavohruď	hlavohruď	k1gFnSc1	hlavohruď
(	(	kIx(	(
<g/>
cephalothorax	cephalothorax	k1gInSc1	cephalothorax
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
naupliová	naupliový	k2eAgFnSc1d1	naupliový
larva	larva	k1gFnSc1	larva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prochází	procházet	k5eAaImIp3nS	procházet
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
víceméně	víceméně	k9	víceméně
kryto	krýt	k5eAaImNgNnS	krýt
krunýřem	krunýř	k1gInSc7	krunýř
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
prostoupen	prostoupit	k5eAaPmNgInS	prostoupit
vápenatými	vápenatý	k2eAgFnPc7d1	vápenatá
solemi	sůl	k1gFnPc7	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Krunýř	krunýř	k1gInSc4	krunýř
svlékají	svlékat	k5eAaImIp3nP	svlékat
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
ekdyze	ekdyze	k1gFnSc2	ekdyze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
růst	růst	k5eAaImF	růst
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
hlavy	hlava	k1gFnSc2	hlava
mají	mít	k5eAaImIp3nP	mít
korýši	korýš	k1gMnPc1	korýš
jeden	jeden	k4xCgInSc4	jeden
či	či	k8xC	či
dva	dva	k4xCgInPc4	dva
páry	pár	k1gInPc4	pár
tykadel	tykadlo	k1gNnPc2	tykadlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
při	při	k7c6	při
průzkumu	průzkum	k1gInSc6	průzkum
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
zabudovány	zabudován	k2eAgInPc4d1	zabudován
chemoreceptory	chemoreceptor	k1gInPc4	chemoreceptor
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
látky	látka	k1gFnPc1	látka
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
kterými	který	k3yRgInPc7	který
hledají	hledat	k5eAaImIp3nP	hledat
kořist	kořist	k1gFnSc1	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
pár	pár	k4xCyI	pár
silných	silný	k2eAgNnPc2d1	silné
kusadel	kusadla	k1gNnPc2	kusadla
(	(	kIx(	(
<g/>
mandibul	mandibula	k1gFnPc2	mandibula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c6	na
osmi	osm	k4xCc6	osm
článcích	článek	k1gInPc6	článek
trupu	trup	k1gInSc2	trup
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
páru	pára	k1gFnSc4	pára
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgInSc4d1	přední
pár	pár	k4xCyI	pár
nožek	nožka	k1gFnPc2	nožka
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rozmělnění	rozmělnění	k1gNnSc3	rozmělnění
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
pět	pět	k4xCc4	pět
páru	pár	k1gInSc2	pár
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
určeno	určit	k5eAaPmNgNnS	určit
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejich	jejich	k3xOp3gFnSc1	jejich
anatomie	anatomie	k1gFnSc1	anatomie
i	i	k8xC	i
využití	využití	k1gNnSc1	využití
se	se	k3xPyFc4	se
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
skupin	skupina	k1gFnPc2	skupina
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
korýšů	korýš	k1gMnPc2	korýš
je	být	k5eAaImIp3nS	být
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
parazitickým	parazitický	k2eAgInSc7d1	parazitický
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
tráví	trávit	k5eAaImIp3nP	trávit
život	život	k1gInSc4	život
uvnitř	uvnitř	k7c2	uvnitř
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
dospělí	dospělý	k2eAgMnPc1d1	dospělý
svijonožci	svijonožec	k1gMnPc1	svijonožec
jsou	být	k5eAaImIp3nP	být
nepohybliví	pohyblivý	k2eNgMnPc1d1	nepohyblivý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
hlavovou	hlavový	k2eAgFnSc7d1	hlavová
částí	část	k1gFnSc7	část
ukotveni	ukotven	k2eAgMnPc1d1	ukotven
na	na	k7c6	na
podkladu	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgMnPc1d2	menší
korýši	korýš	k1gMnPc1	korýš
dýchají	dýchat	k5eAaImIp3nP	dýchat
díky	díky	k7c3	díky
difuzi	difuze	k1gFnSc3	difuze
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInPc1d2	veliký
mívají	mívat	k5eAaImIp3nP	mívat
žábry	žábry	k1gFnPc1	žábry
nebo	nebo	k8xC	nebo
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
určitý	určitý	k2eAgInSc1d1	určitý
typ	typ	k1gInSc1	typ
plic	plíce	k1gFnPc2	plíce
(	(	kIx(	(
<g/>
u	u	k7c2	u
kraba	krab	k1gMnSc2	krab
palmového	palmový	k2eAgMnSc2d1	palmový
(	(	kIx(	(
<g/>
Birgus	Birgus	k1gInSc1	Birgus
latro	latro	k1gNnSc1	latro
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Korýši	korýš	k1gMnPc1	korýš
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
odděleného	oddělený	k2eAgNnSc2d1	oddělené
pohlaví	pohlaví	k1gNnSc2	pohlaví
(	(	kIx(	(
<g/>
gonochoristé	gonochorista	k1gMnPc1	gonochorista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
několik	několik	k4yIc1	několik
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
hermafroditických	hermafroditický	k2eAgFnPc2d1	hermafroditická
<g/>
.	.	kIx.	.
</s>
<s>
Larválním	larvální	k2eAgNnSc7d1	larvální
stádiem	stádium	k1gNnSc7	stádium
korýšů	korýš	k1gMnPc2	korýš
je	být	k5eAaImIp3nS	být
nauplius	nauplius	k1gMnSc1	nauplius
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
toto	tento	k3xDgNnSc4	tento
stádium	stádium	k1gNnSc4	stádium
potlačeno	potlačen	k2eAgNnSc4d1	potlačeno
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
embryonálním	embryonální	k2eAgInSc6d1	embryonální
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgMnPc1d2	nižší
korýši	korýš	k1gMnPc1	korýš
mají	mít	k5eAaImIp3nP	mít
nepřímý	přímý	k2eNgInSc4d1	nepřímý
vývoj	vývoj	k1gInSc4	vývoj
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
larvu	larva	k1gFnSc4	larva
-	-	kIx~	-
nauplius	nauplius	k1gMnSc1	nauplius
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
neustálený	ustálený	k2eNgInSc4d1	neustálený
počet	počet	k1gInSc4	počet
článků	článek	k1gInPc2	článek
vývojově	vývojově	k6eAd1	vývojově
starší	starý	k2eAgMnSc1d2	starší
a	a	k8xC	a
méně	málo	k6eAd2	málo
dokonalí	dokonalit	k5eAaImIp3nS	dokonalit
př	př	kA	př
<g/>
.	.	kIx.	.
buchanka	buchanka	k1gFnSc1	buchanka
<g/>
,	,	kIx,	,
perloočka	perloočka	k1gFnSc1	perloočka
<g/>
,	,	kIx,	,
listonoh	listonoh	k1gMnSc1	listonoh
americký	americký	k2eAgMnSc1d1	americký
<g/>
,	,	kIx,	,
žábronožka	žábronožka	k1gFnSc1	žábronožka
solná	solný	k2eAgFnSc1d1	solná
<g/>
,	,	kIx,	,
vilejši	vilejš	k1gMnPc1	vilejš
Vyšší	vysoký	k2eAgMnPc1d2	vyšší
korýši	korýš	k1gMnPc1	korýš
mají	mít	k5eAaImIp3nP	mít
přímý	přímý	k2eAgInSc4d1	přímý
nebo	nebo	k8xC	nebo
nepřímý	přímý	k2eNgInSc4d1	nepřímý
vývoj	vývoj	k1gInSc4	vývoj
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
<g />
.	.	kIx.	.
</s>
<s>
larvu	larva	k1gFnSc4	larva
-	-	kIx~	-
zoea	zoea	k6eAd1	zoea
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
stálý	stálý	k2eAgInSc4d1	stálý
počet	počet	k1gInSc4	počet
článků	článek	k1gInPc2	článek
jsou	být	k5eAaImIp3nP	být
mladší	mladý	k2eAgInPc1d2	mladší
a	a	k8xC	a
vývojově	vývojově	k6eAd1	vývojově
dokonalejší	dokonalý	k2eAgInPc1d2	dokonalejší
př	př	kA	př
<g/>
.	.	kIx.	.
raci	rak	k1gMnPc1	rak
(	(	kIx(	(
<g/>
Astacus	Astacus	k1gMnSc1	Astacus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krabi	krab	k1gMnPc1	krab
<g/>
,	,	kIx,	,
langusta	langusta	k1gFnSc1	langusta
<g/>
,	,	kIx,	,
krevety	kreveta	k1gFnPc1	kreveta
<g/>
,	,	kIx,	,
stínka	stínka	k1gFnSc1	stínka
třída	třída	k1gFnSc1	třída
lupenonožci	lupenonožec	k1gMnPc5	lupenonožec
(	(	kIx(	(
<g/>
Branchiopoda	Branchiopoda	k1gFnSc1	Branchiopoda
<g/>
)	)	kIx)	)
listonožky	listonožka	k1gFnPc1	listonožka
(	(	kIx(	(
<g/>
Notostraca	Notostraca	k1gFnSc1	Notostraca
<g/>
)	)	kIx)	)
žábronožky	žábronožka	k1gFnPc1	žábronožka
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Anostraca	Anostraca	k1gFnSc1	Anostraca
<g/>
)	)	kIx)	)
perloočky	perloočka	k1gFnPc1	perloočka
(	(	kIx(	(
<g/>
Cladocera	Cladocera	k1gFnSc1	Cladocera
<g/>
)	)	kIx)	)
třída	třída	k1gFnSc1	třída
veslonožci	veslonožec	k1gMnPc1	veslonožec
(	(	kIx(	(
<g/>
Remipedia	Remipedium	k1gNnSc2	Remipedium
<g/>
)	)	kIx)	)
Enantiopoda	Enantiopoda	k1gFnSc1	Enantiopoda
jeskyňovky	jeskyňovka	k1gFnSc2	jeskyňovka
(	(	kIx(	(
<g/>
Nectiopoda	Nectiopoda	k1gFnSc1	Nectiopoda
<g/>
)	)	kIx)	)
třída	třída	k1gFnSc1	třída
volnohlavci	volnohlavec	k1gMnPc5	volnohlavec
(	(	kIx(	(
<g/>
Cephalocarida	Cephalocarida	k1gFnSc1	Cephalocarida
<g/>
)	)	kIx)	)
Brachypoda	Brachypoda	k1gFnSc1	Brachypoda
třída	třída	k1gFnSc1	třída
Maxillopoda	Maxillopoda	k1gFnSc1	Maxillopoda
(	(	kIx(	(
<g/>
nepřirozená	přirozený	k2eNgFnSc1d1	nepřirozená
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
<g/>
rakovčíci	rakovčík	k1gMnPc1	rakovčík
(	(	kIx(	(
<g/>
Mystacocarida	Mystacocarida	k1gFnSc1	Mystacocarida
<g/>
)	)	kIx)	)
klanonožci	klanonožec	k1gMnPc1	klanonožec
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Copepoda	Copepoda	k1gFnSc1	Copepoda
<g/>
)	)	kIx)	)
kapřivci	kapřivec	k1gMnPc1	kapřivec
(	(	kIx(	(
<g/>
Branchiura	Branchiura	k1gFnSc1	Branchiura
<g/>
)	)	kIx)	)
jazyčnatky	jazyčnatka	k1gFnPc1	jazyčnatka
(	(	kIx(	(
<g/>
Pentastomida	Pentastomida	k1gFnSc1	Pentastomida
<g/>
)	)	kIx)	)
Tantulocarida	Tantulocarida	k1gFnSc1	Tantulocarida
Thecostraca	Thecostrac	k2eAgFnSc1d1	Thecostrac
svijonožci	svijonožec	k1gMnPc7	svijonožec
(	(	kIx(	(
<g/>
Cirripedia	Cirripedium	k1gNnSc2	Cirripedium
<g/>
)	)	kIx)	)
třída	třída	k1gFnSc1	třída
lasturnatky	lasturnatka	k1gFnSc2	lasturnatka
(	(	kIx(	(
<g/>
Ostracoda	Ostracoda	k1gFnSc1	Ostracoda
<g/>
)	)	kIx)	)
Metacopina	Metacopin	k2eAgFnSc1d1	Metacopin
Myodocopa	Myodocopa	k1gFnSc1	Myodocopa
Podocopa	Podocopa	k1gFnSc1	Podocopa
třída	třída	k1gFnSc1	třída
rakovci	rakovec	k1gMnPc5	rakovec
(	(	kIx(	(
<g/>
Malacostraca	Malacostraca	k1gFnSc1	Malacostraca
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
"	"	kIx"	"
<g/>
vyšší	vysoký	k2eAgMnPc1d2	vyšší
korýši	korýš	k1gMnPc1	korýš
<g/>
"	"	kIx"	"
podtřída	podtřída	k1gFnSc1	podtřída
Eumalacostraca	Eumalacostraca	k1gFnSc1	Eumalacostraca
řád	řád	k1gInSc4	řád
různonožci	různonožec	k1gMnPc1	různonožec
(	(	kIx(	(
<g/>
Amphipoda	Amphipoda	k1gMnSc1	Amphipoda
<g/>
)	)	kIx)	)
podtřída	podtřída	k1gFnSc1	podtřída
Hoplocarida	Hoplocarida	k1gFnSc1	Hoplocarida
podtřída	podtřída	k1gFnSc1	podtřída
Phyllocarida	Phyllocarida	k1gFnSc1	Phyllocarida
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Crustacean	Crustaceana	k1gFnPc2	Crustaceana
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Korýši	korýš	k1gMnPc1	korýš
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
korýš	korýš	k1gMnSc1	korýš
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Crustacea	Crustace	k1gInSc2	Crustace
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
