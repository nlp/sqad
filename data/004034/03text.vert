<s>
Anchorage	Anchorage	k6eAd1	Anchorage
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
obvod	obvod	k1gInSc1	obvod
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Aljaška	Aljaška	k1gFnSc1	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
298	[number]	k4	298
610	[number]	k4	610
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
když	když	k8xS	když
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
dorazila	dorazit	k5eAaPmAgFnS	dorazit
Aljašská	aljašský	k2eAgFnSc1d1	aljašská
železnice	železnice	k1gFnSc1	železnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anchorage	Anchorage	k1gFnSc6	Anchorage
žijí	žít	k5eAaImIp3nP	žít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
pětiny	pětina	k1gFnPc1	pětina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
ale	ale	k8xC	ale
již	již	k9	již
před	před	k7c7	před
5000	[number]	k4	5000
lety	léto	k1gNnPc7	léto
osídlili	osídlit	k5eAaPmAgMnP	osídlit
Athabasští	Athabasský	k2eAgMnPc1d1	Athabasský
eskymáci	eskymák	k1gMnPc1	eskymák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sem	sem	k6eAd1	sem
dorazili	dorazit	k5eAaPmAgMnP	dorazit
první	první	k4xOgMnPc1	první
průzkumníci	průzkumník	k1gMnPc1	průzkumník
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
s	s	k7c7	s
eskymáky	eskymák	k1gMnPc7	eskymák
obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
–	–	k?	–
měď	měď	k1gFnSc4	měď
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc4	ryba
a	a	k8xC	a
kožešiny	kožešina	k1gFnPc4	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
zde	zde	k6eAd1	zde
přistál	přistát	k5eAaPmAgMnS	přistát
kapitán	kapitán	k1gMnSc1	kapitán
Cook	Cook	k1gMnSc1	Cook
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
domnělé	domnělý	k2eAgFnPc1d1	domnělá
severozápadní	severozápadní	k2eAgFnPc1d1	severozápadní
zkratky	zkratka	k1gFnPc1	zkratka
mezi	mezi	k7c7	mezi
Pacifikem	Pacifik	k1gInSc7	Pacifik
a	a	k8xC	a
Atlantikem	Atlantik	k1gInSc7	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
byly	být	k5eAaImAgFnP	být
rusové	rus	k1gMnPc1	rus
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
zabydleni	zabydlen	k2eAgMnPc1d1	zabydlen
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Eklutny	Eklutna	k1gFnSc2	Eklutna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
kongres	kongres	k1gInSc1	kongres
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vybudovat	vybudovat	k5eAaPmF	vybudovat
zde	zde	k6eAd1	zde
železniční	železniční	k2eAgFnSc4d1	železniční
spojku	spojka	k1gFnSc4	spojka
se	s	k7c7	s
Sewardem	Seward	k1gInSc7	Seward
kvůli	kvůli	k7c3	kvůli
zpřístupnění	zpřístupnění	k1gNnSc3	zpřístupnění
vnitrozemských	vnitrozemský	k2eAgNnPc2d1	vnitrozemské
nalezišť	naleziště	k1gNnPc2	naleziště
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
začátek	začátek	k1gInSc1	začátek
rychlého	rychlý	k2eAgInSc2d1	rychlý
rozvoje	rozvoj	k1gInSc2	rozvoj
původně	původně	k6eAd1	původně
stanového	stanový	k2eAgNnSc2d1	stanové
městečka	městečko	k1gNnSc2	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1964	[number]	k4	1964
město	město	k1gNnSc1	město
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
nejsilnější	silný	k2eAgNnSc1d3	nejsilnější
naměřené	naměřený	k2eAgNnSc1d1	naměřené
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
naměřené	naměřený	k2eAgFnSc2d1	naměřená
zemětřesení	zemětřesení	k1gNnPc4	zemětřesení
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
9,2	[number]	k4	9,2
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
totálně	totálně	k6eAd1	totálně
zničilo	zničit	k5eAaPmAgNnS	zničit
některé	některý	k3yIgFnPc4	některý
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
a	a	k8xC	a
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
zničující	zničující	k2eAgFnSc4d1	zničující
vlnu	vlna	k1gFnSc4	vlna
tsunami	tsunami	k1gNnSc2	tsunami
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zálivu	záliv	k1gInSc2	záliv
prince	princ	k1gMnSc2	princ
Williama	William	k1gMnSc2	William
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevu	objev	k1gInSc6	objev
bohatých	bohatý	k2eAgNnPc2d1	bohaté
nalezišť	naleziště	k1gNnPc2	naleziště
ropy	ropa	k1gFnSc2	ropa
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
si	se	k3xPyFc3	se
v	v	k7c6	v
Anchorage	Anchorage	k1gFnSc6	Anchorage
otevřely	otevřít	k5eAaPmAgFnP	otevřít
své	své	k1gNnSc4	své
pobočky	pobočka	k1gFnSc2	pobočka
významné	významný	k2eAgFnSc2d1	významná
ropné	ropný	k2eAgFnSc2d1	ropná
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
významně	významně	k6eAd1	významně
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Anchorage	Anchorage	k6eAd1	Anchorage
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
uprostřed	uprostřed	k7c2	uprostřed
jižního	jižní	k2eAgInSc2d1	jižní
regionu	region	k1gInSc2	region
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
576	[number]	k4	576
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Fairbanks	Fairbanksa	k1gFnPc2	Fairbanksa
<g/>
,	,	kIx,	,
97	[number]	k4	97
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Whittieru	Whittier	k1gInSc2	Whittier
a	a	k8xC	a
204	[number]	k4	204
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Sewardu	Seward	k1gInSc2	Seward
<g/>
.	.	kIx.	.
</s>
<s>
Anchorage	Anchorage	k6eAd1	Anchorage
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
2	[number]	k4	2
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
nejbližším	blízký	k2eAgNnSc7d3	nejbližší
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
je	být	k5eAaImIp3nS	být
kanadský	kanadský	k2eAgInSc1d1	kanadský
Vancouver	Vancouver	k1gInSc1	Vancouver
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
čarou	čára	k1gFnSc7	čára
2	[number]	k4	2
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
ze	z	k7c2	z
Seattlu	Seattl	k1gInSc2	Seattl
do	do	k7c2	do
Anchorage	Anchorage	k1gNnSc2	Anchorage
trvá	trvat	k5eAaImIp3nS	trvat
tři	tři	k4xCgInPc4	tři
a	a	k8xC	a
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
něž	jenž	k3xRgFnPc4	jenž
8000	[number]	k4	8000
hotelových	hotelový	k2eAgInPc2d1	hotelový
a	a	k8xC	a
motelových	motelův	k2eAgInPc2d1	motelův
pokojů	pokoj	k1gInPc2	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
600	[number]	k4	600
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Anchorage	Anchorag	k1gInSc2	Anchorag
denně	denně	k6eAd1	denně
přilétá	přilétat	k5eAaImIp3nS	přilétat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
280	[number]	k4	280
letových	letový	k2eAgNnPc2d1	letové
spojení	spojení	k1gNnPc2	spojení
domácích	domácí	k1gMnPc2	domácí
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
aerolinek	aerolinka	k1gFnPc2	aerolinka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Anchorage	Anchorag	k1gInSc2	Anchorag
můžete	moct	k5eAaImIp2nP	moct
dorazit	dorazit	k5eAaPmF	dorazit
odkudkoliv	odkudkoliv	k6eAd1	odkudkoliv
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
díky	díky	k7c3	díky
Aljašské	aljašský	k2eAgFnSc3d1	aljašská
dálnici	dálnice	k1gFnSc3	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
sem	sem	k6eAd1	sem
jezdí	jezdit	k5eAaImIp3nP	jezdit
vlak	vlak	k1gInSc4	vlak
z	z	k7c2	z
města	město	k1gNnSc2	město
Fairbanks	Fairbanksa	k1gFnPc2	Fairbanksa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
291	[number]	k4	291
826	[number]	k4	826
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
66,0	[number]	k4	66,0
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
5,6	[number]	k4	5,6
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
7,9	[number]	k4	7,9
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
8,1	[number]	k4	8,1
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
2,0	[number]	k4	2,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
2,3	[number]	k4	2,3
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
8,1	[number]	k4	8,1
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
7,6	[number]	k4	7,6
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Anchorage	Anchorage	k6eAd1	Anchorage
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
<g/>
,	,	kIx,	,
klimatu	klima	k1gNnSc3	klima
a	a	k8xC	a
dopravnímu	dopravní	k2eAgNnSc3d1	dopravní
spojení	spojení	k1gNnSc3	spojení
hlavním	hlavní	k2eAgMnSc7d1	hlavní
základním	základní	k2eAgInSc7d1	základní
táborem	tábor	k1gInSc7	tábor
pro	pro	k7c4	pro
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
Aljašce	Aljaška	k1gFnSc3	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
množství	množství	k1gNnSc4	množství
sportovních	sportovní	k2eAgFnPc2d1	sportovní
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
nespočet	nespočet	k1gInSc1	nespočet
festivalů	festival	k1gInPc2	festival
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Anchorage	Anchorag	k1gFnSc2	Anchorag
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xC	jako
vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
křižovatka	křižovatka	k1gFnSc1	křižovatka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
užít	užít	k5eAaPmF	užít
rekreaci	rekreace	k1gFnSc4	rekreace
<g/>
,	,	kIx,	,
divokou	divoký	k2eAgFnSc4d1	divoká
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
ledovce	ledovec	k1gInPc1	ledovec
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
aj.	aj.	kA	aj.
Mírné	mírný	k2eAgNnSc4d1	mírné
zimní	zimní	k2eAgNnSc4d1	zimní
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
zasněžené	zasněžený	k2eAgFnPc4d1	zasněžená
hory	hora	k1gFnPc4	hora
a	a	k8xC	a
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
oceněný	oceněný	k2eAgInSc4d1	oceněný
systém	systém	k1gInSc4	systém
stezek	stezka	k1gFnPc2	stezka
a	a	k8xC	a
míle	míle	k1gFnSc1	míle
nezmapovaného	zmapovaný	k2eNgInSc2d1	nezmapovaný
sněhu	sníh	k1gInSc2	sníh
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
ideální	ideální	k2eAgFnPc1d1	ideální
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
alpské	alpský	k2eAgNnSc4d1	alpské
a	a	k8xC	a
severské	severský	k2eAgNnSc4d1	severské
lyžování	lyžování	k1gNnSc4	lyžování
<g/>
,	,	kIx,	,
snowboarding	snowboarding	k1gInSc1	snowboarding
<g/>
,	,	kIx,	,
psí	psí	k2eAgNnSc1d1	psí
spřežení	spřežení	k1gNnSc1	spřežení
<g/>
,	,	kIx,	,
rybaření	rybařený	k2eAgMnPc1d1	rybařený
<g/>
,	,	kIx,	,
výlety	výlet	k1gInPc1	výlet
na	na	k7c6	na
sněžnicích	sněžnice	k1gFnPc6	sněžnice
a	a	k8xC	a
další	další	k2eAgFnSc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Čitose	Čitosa	k1gFnSc3	Čitosa
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
Inčchon	Inčchon	k1gInSc1	Inčchon
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Magadan	Magadan	k1gInSc1	Magadan
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Tromsø	Tromsø	k1gFnSc2	Tromsø
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
Whitby	Whitba	k1gFnSc2	Whitba
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Anchorage	Anchorag	k1gFnSc2	Anchorag
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Municipality	municipalita	k1gFnSc2	municipalita
of	of	k?	of
Anchorage	Anchorag	k1gFnSc2	Anchorag
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Borough	Borough	k1gInSc1	Borough
map	mapa	k1gFnPc2	mapa
<g/>
:	:	kIx,	:
Alaska	Alaska	k1gFnSc1	Alaska
Department	department	k1gInSc1	department
of	of	k?	of
Labor	Labor	k1gInSc1	Labor
</s>
