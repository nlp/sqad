<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Savojská	savojský	k2eAgFnSc1d1	Savojská
(	(	kIx(	(
<g/>
1306	[number]	k4	1306
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
1365	[number]	k4	1365
<g/>
,	,	kIx,	,
Soluň	Soluň	k1gFnSc1	Soluň
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
byzantská	byzantský	k2eAgFnSc1d1	byzantská
císařovna	císařovna	k1gFnSc1	císařovna
a	a	k8xC	a
regentka	regentka	k1gFnSc1	regentka
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
savojského	savojský	k2eAgMnSc4d1	savojský
hraběte	hrabě	k1gMnSc4	hrabě
Amadea	Amadeus	k1gMnSc2	Amadeus
V.	V.	kA	V.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
manželky	manželka	k1gFnPc1	manželka
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
sestry	sestra	k1gFnSc2	sestra
římské	římský	k2eAgFnSc2d1	římská
královny	královna	k1gFnSc2	královna
Markéty	Markéta	k1gFnSc2	Markéta
Brabantské	Brabantský	k2eAgFnSc2d1	Brabantská
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1326	[number]	k4	1326
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
ovdovělého	ovdovělý	k2eAgMnSc4d1	ovdovělý
byzantského	byzantský	k2eAgMnSc4d1	byzantský
císaře	císař	k1gMnSc4	císař
Andronika	Andronik	k1gMnSc2	Andronik
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
porodila	porodit	k5eAaPmAgFnS	porodit
mu	on	k3xPp3gMnSc3	on
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
manželově	manželův	k2eAgFnSc6d1	manželova
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1341	[number]	k4	1341
se	se	k3xPyFc4	se
císařská	císařský	k2eAgFnSc1d1	císařská
vdova	vdova	k1gFnSc1	vdova
stala	stát	k5eAaPmAgFnS	stát
regentkou	regentka	k1gFnSc7	regentka
za	za	k7c4	za
nezletilého	zletilý	k2eNgMnSc4d1	nezletilý
syna	syn	k1gMnSc4	syn
Jana	Jan	k1gMnSc4	Jan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
regentství	regentství	k1gNnSc6	regentství
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
společně	společně	k6eAd1	společně
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1347	[number]	k4	1347
na	na	k7c4	na
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
stal	stát	k5eAaPmAgInS	stát
synovým	synův	k2eAgMnSc7d1	synův
spoluvládcem	spoluvládce	k1gMnSc7	spoluvládce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1351	[number]	k4	1351
se	se	k3xPyFc4	se
císařovna	císařovna	k1gFnSc1	císařovna
vdova	vdova	k1gFnSc1	vdova
uchýlila	uchýlit	k5eAaPmAgFnS	uchýlit
do	do	k7c2	do
klášterního	klášterní	k2eAgNnSc2d1	klášterní
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
jeptiškou	jeptiška	k1gFnSc7	jeptiška
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Anna	Anna	k1gFnSc1	Anna
Savojská	savojský	k2eAgFnSc1d1	Savojská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
