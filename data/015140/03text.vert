<s>
Drátěné	drátěný	k2eAgNnSc1d1
pletivo	pletivo	k1gNnSc1
</s>
<s>
Drátěné	drátěný	k2eAgNnSc1d1
pletivo	pletivo	k1gNnSc1
</s>
<s>
Drátěné	drátěný	k2eAgNnSc1d1
pletivo	pletivo	k1gNnSc1
je	být	k5eAaImIp3nS
pletivo	pletivo	k1gNnSc1
vytvořené	vytvořený	k2eAgNnSc1d1
z	z	k7c2
ohýbaných	ohýbaný	k2eAgInPc2d1
drátů	drát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgInPc1d1
druhy	druh	k1gInPc1
drátěného	drátěný	k2eAgNnSc2d1
pletiva	pletivo	k1gNnSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
nejčastěji	často	k6eAd3
k	k	k7c3
výrobě	výroba	k1gFnSc3
drátěných	drátěný	k2eAgInPc2d1
plotů	plot	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
k	k	k7c3
výrobě	výroba	k1gFnSc3
klecí	klec	k1gFnPc2
<g/>
,	,	kIx,
sít	sít	k5eAaImF
(	(	kIx(
<g/>
zednická	zednický	k2eAgNnPc1d1
síta	síto	k1gNnPc1
<g/>
,	,	kIx,
např.	např.	kA
prohazovací	prohazovací	k2eAgNnSc1d1
síto	síto	k1gNnSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
kuchyňské	kuchyňský	k2eAgNnSc4d1
sítko	sítko	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
filtr	filtr	k1gInSc1
bránící	bránící	k2eAgInSc1d1
průchodu	průchod	k1gInSc3
kusových	kusový	k2eAgFnPc2d1
nečistot	nečistota	k1gFnPc2
ve	v	k7c6
vodě	voda	k1gFnSc6
či	či	k8xC
vzduchu	vzduch	k1gInSc6
<g/>
,	,	kIx,
atd.	atd.	kA
Drátěné	drátěný	k2eAgNnSc1d1
pletivo	pletivo	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
výplň	výplň	k1gFnSc4
drátěného	drátěný	k2eAgInSc2d1
roštu	rošt	k1gInSc2
postelí	postel	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jemné	jemný	k2eAgNnSc1d1
stočené	stočený	k2eAgNnSc1d1
drátěné	drátěný	k2eAgNnSc1d1
pletivo	pletivo	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
i	i	k9
drátěnku	drátěnka	k1gFnSc4
k	k	k7c3
umývání	umývání	k1gNnSc3
nádobí	nádobí	k1gNnSc2
nebo	nebo	k8xC
jinému	jiný	k2eAgNnSc3d1
očišťování	očišťování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Sítko	sítko	k1gNnSc4
z	z	k7c2
drátěného	drátěný	k2eAgNnSc2d1
pletiva	pletivo	k1gNnSc2
</s>
<s>
Detail	detail	k1gInSc1
navázání	navázání	k1gNnSc2
drátů	drát	k1gInPc2
v	v	k7c6
pletivu	pletivo	k1gNnSc6
</s>
<s>
Různé	různý	k2eAgFnPc1d1
typy	typa	k1gFnPc1
pletiv	pletivo	k1gNnPc2
se	se	k3xPyFc4
pojmenovávají	pojmenovávat	k5eAaImIp3nP
podle	podle	k7c2
účelu	účel	k1gInSc2
(	(	kIx(
<g/>
lesnické	lesnický	k2eAgFnSc2d1
<g/>
,	,	kIx,
oborní	oborní	k2eAgFnSc2d1
<g/>
,	,	kIx,
pastevecké	pastevecký	k2eAgFnSc2d1
<g/>
,	,	kIx,
chovatelské	chovatelský	k2eAgFnSc2d1
<g/>
,	,	kIx,
sportovní	sportovní	k2eAgFnSc2d1
<g/>
,	,	kIx,
průmyslové	průmyslový	k2eAgFnSc2d1
<g/>
,	,	kIx,
okrasné	okrasný	k2eAgFnSc2d1
<g/>
,	,	kIx,
ozdobné	ozdobný	k2eAgFnSc2d1
a	a	k8xC
dekorační	dekorační	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
provedení	provedení	k1gNnSc2
(	(	kIx(
<g/>
šestihranné	šestihranný	k2eAgInPc1d1
<g/>
,	,	kIx,
poplastované	poplastovaný	k2eAgInPc1d1
<g/>
,	,	kIx,
pozinkované	pozinkovaný	k2eAgInPc1d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
obchodními	obchodní	k2eAgInPc7d1
názvy	název	k1gInPc7
(	(	kIx(
<g/>
česká	český	k2eAgFnSc1d1
pletenka	pletenka	k1gFnSc1
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
oplocenka	oplocenka	k1gFnSc1
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
ohrazenka	ohrazenka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rabicová	rabicový	k2eAgFnSc1d1
tkanina	tkanina	k1gFnSc1
je	být	k5eAaImIp3nS
drátěné	drátěný	k2eAgNnSc4d1
pletivo	pletivo	k1gNnSc4
určené	určený	k2eAgNnSc4d1
pod	pod	k7c4
omítku	omítka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesnická	lesnický	k2eAgNnPc1d1
pletiva	pletivo	k1gNnPc1
mívají	mívat	k5eAaImIp3nP
čtvercovou	čtvercový	k2eAgFnSc4d1
síť	síť	k1gFnSc4
ve	v	k7c6
svislo-vodorovném	svislo-vodorovný	k2eAgInSc6d1
směru	směr	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
hustší	hustý	k2eAgFnSc6d2
kvůli	kvůli	k7c3
drobné	drobná	k1gFnSc3
zvěři	zvěř	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sportovní	sportovní	k2eAgInSc1d1
pletiva	pletivo	k1gNnSc2
by	by	kYmCp3nS
neměla	mít	k5eNaImAgFnS
obsahovat	obsahovat	k5eAaImF
spoje	spoj	k1gFnSc2
ohrožující	ohrožující	k2eAgMnPc4d1
sportovce	sportovec	k1gMnPc4
či	či	k8xC
míče	míč	k1gInPc4
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
jsou	být	k5eAaImIp3nP
potažena	potáhnout	k5eAaPmNgFnS
PVC	PVC	kA
<g/>
.	.	kIx.
</s>
<s>
Kvůli	kvůli	k7c3
odolnosti	odolnost	k1gFnSc3
vůči	vůči	k7c3
korozi	koroze	k1gFnSc3
jsou	být	k5eAaImIp3nP
některá	některý	k3yIgNnPc1
pletiva	pletivo	k1gNnPc1
pozinkovaná	pozinkovaný	k2eAgNnPc1d1
<g/>
,	,	kIx,
natíraná	natíraný	k2eAgNnPc1d1
barvou	barva	k1gFnSc7
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
dráty	drát	k1gInPc1
obaleny	obalen	k2eAgInPc1d1
plastem	plast	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
pletiva	pletivo	k1gNnPc1
jsou	být	k5eAaImIp3nP
svařovaná	svařovaný	k2eAgNnPc1d1
<g/>
,	,	kIx,
jiná	jiný	k2eAgNnPc1d1
jen	jen	k8xS
pletená	pletený	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Různé	různý	k2eAgInPc1d1
typy	typ	k1gInPc1
pletiva	pletivo	k1gNnSc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
s	s	k7c7
trojitým	trojitý	k2eAgInSc7d1
ohybem	ohyb	k1gInSc7
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
s	s	k7c7
jednoduchým	jednoduchý	k2eAgInSc7d1
ohybem	ohyb	k1gInSc7
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
vlnité	vlnitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Claude	Claud	k1gInSc5
Augé	Augý	k2eAgFnSc3d1
<g/>
:	:	kIx,
Larousse	Laroussa	k1gFnSc3
universel	universela	k1gFnPc2
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Drátěné	drátěný	k2eAgInPc1d1
ploty	plot	k1gInPc1
jsou	být	k5eAaImIp3nP
budovány	budován	k2eAgInPc1d1
buď	buď	k8xC
natažením	natažení	k1gNnPc3
souvislého	souvislý	k2eAgInSc2d1
pásu	pás	k1gInSc2
pletiva	pletivo	k1gNnSc2
<g/>
,	,	kIx,
dodávaného	dodávaný	k2eAgInSc2d1
v	v	k7c6
rolích	role	k1gFnPc6
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
po	po	k7c6
50	#num#	k4
metrech	metr	k1gInPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
skládány	skládán	k2eAgInPc1d1
z	z	k7c2
rámových	rámový	k2eAgInPc2d1
dílců	dílec	k1gInPc2
s	s	k7c7
pletivovou	pletivový	k2eAgFnSc7d1
výplní	výplň	k1gFnSc7
(	(	kIx(
<g/>
plotové	plotový	k2eAgInPc1d1
rámy	rám	k1gInPc1
<g/>
,	,	kIx,
plotové	plotový	k2eAgInPc1d1
výplety	výplet	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drátěné	drátěný	k2eAgNnSc1d1
pletivo	pletivo	k1gNnSc1
je	být	k5eAaImIp3nS
oproti	oproti	k7c3
jiným	jiný	k2eAgInPc3d1
druhům	druh	k1gInPc3
plotů	plot	k1gInPc2
specifické	specifický	k2eAgInPc1d1
relativně	relativně	k6eAd1
nízkou	nízký	k2eAgFnSc7d1
cenou	cena	k1gFnSc7
(	(	kIx(
<g/>
díky	díky	k7c3
možnosti	možnost	k1gFnSc3
strojové	strojový	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vysokou	vysoký	k2eAgFnSc7d1
průhledností	průhlednost	k1gFnSc7
<g/>
,	,	kIx,
možností	možnost	k1gFnSc7
rychle	rychle	k6eAd1
plot	plot	k1gInSc4
postavit	postavit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
architektonicky	architektonicky	k6eAd1
bývá	bývat	k5eAaImIp3nS
méně	málo	k6eAd2
hodnotné	hodnotný	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ploty	plot	k1gInPc7
ze	z	k7c2
souvislého	souvislý	k2eAgInSc2d1
pletivového	pletivový	k2eAgInSc2d1
pásu	pás	k1gInSc2
jsou	být	k5eAaImIp3nP
však	však	k9
málo	málo	k6eAd1
odolné	odolný	k2eAgFnPc1d1
proti	proti	k7c3
průniku	průnik	k1gInSc3
(	(	kIx(
<g/>
např.	např.	kA
podlezení	podlezení	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
poškození	poškození	k1gNnSc1
(	(	kIx(
<g/>
protržení	protržení	k1gNnSc1
<g/>
,	,	kIx,
přestříhání	přestříhání	k1gNnSc1
<g/>
,	,	kIx,
proreznutí	proreznutí	k1gNnSc1
v	v	k7c6
ohybech	ohyb	k1gInPc6
drátů	drát	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
k	k	k7c3
zajištění	zajištění	k1gNnSc3
bezpečnosti	bezpečnost	k1gFnSc2
doplňuje	doplňovat	k5eAaImIp3nS
ostnatým	ostnatý	k2eAgInSc7d1
drátem	drát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Šestihranné	šestihranný	k2eAgNnSc1d1
pletivo	pletivo	k1gNnSc1
klece	klec	k1gFnSc2
pro	pro	k7c4
drůbež	drůbež	k1gFnSc4
</s>
<s>
Poškozené	poškozený	k2eAgNnSc1d1
pletivo	pletivo	k1gNnSc1
drátěného	drátěný	k2eAgInSc2d1
plotu	plot	k1gInSc2
</s>
<s>
Plot	plot	k1gInSc1
proti	proti	k7c3
zvěři	zvěř	k1gFnSc3
u	u	k7c2
dálnice	dálnice	k1gFnSc2
A72	A72	k1gFnSc2
</s>
<s>
Nepletený	pletený	k2eNgInSc1d1
drátěný	drátěný	k2eAgInSc1d1
plot	plot	k1gInSc1
</s>
<s>
Close-up	Close-up	k1gInSc1
of	of	k?
a	a	k8xC
chain	chain	k2eAgInSc1d1
link	link	k1gInSc1
fence	fenka	k1gFnSc3
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
drátosklo	drátosknout	k5eAaPmAgNnS
</s>
<s>
železobeton	železobeton	k1gInSc1
</s>
<s>
drát	drát	k1gInSc1
</s>
<s>
drátěnka	drátěnka	k1gFnSc1
</s>
<s>
plot	plot	k1gInSc4
</s>
<s>
ostnatý	ostnatý	k2eAgInSc1d1
drát	drát	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Pletivo	pletivo	k1gNnSc4
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
drátěné	drátěný	k2eAgFnSc2d1
pletivo	pletivo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
