<s>
Třikrát	třikrát	k6eAd1
a	a	k8xC
dost	dost	k6eAd1
</s>
<s>
Třikrát	třikrát	k6eAd1
a	a	k8xC
dost	dost	k6eAd1
je	být	k5eAaImIp3nS
trestněprávní	trestněprávní	k2eAgFnSc1d1
zásada	zásada	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
soud	soud	k1gInSc1
je	být	k5eAaImIp3nS
povinen	povinen	k2eAgMnSc1d1
pachatele	pachatel	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
trestán	trestat	k5eAaImNgMnS
za	za	k7c4
</s>
<s>
dva	dva	k4xCgInPc4
trestné	trestný	k2eAgInPc4d1
činy	čin	k1gInPc4
<g/>
,	,	kIx,
za	za	k7c4
třetí	třetí	k4xOgInSc4
trestný	trestný	k2eAgInSc4d1
čin	čin	k1gInSc4
odsoudit	odsoudit	k5eAaPmF
na	na	k7c4
doživotí	doživotí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Nejblíže	blízce	k6eAd3
České	český	k2eAgFnSc3d1
republice	republika	k1gFnSc3
platí	platit	k5eAaImIp3nS
obdoba	obdoba	k1gFnSc1
tohoto	tento	k3xDgInSc2
angloamerického	angloamerický	k2eAgInSc2d1
institutu	institut	k1gInSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
jsou	být	k5eAaImIp3nP
pachatelé	pachatel	k1gMnPc1
vybraných	vybraný	k2eAgInPc2d1
trestných	trestný	k2eAgInPc2d1
činů	čin	k1gInPc2
odsuzování	odsuzování	k1gNnSc2
k	k	k7c3
výjimečnému	výjimečný	k2eAgInSc3d1
trestu	trest	k1gInSc3
odnětí	odnětí	k1gNnSc2
svobody	svoboda	k1gFnSc2
v	v	k7c6
délce	délka	k1gFnSc6
25	#num#	k4
let	léto	k1gNnPc2
bez	bez	k7c2
možnosti	možnost	k1gFnSc2
předčasného	předčasný	k2eAgNnSc2d1
propuštění	propuštění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opatření	opatření	k1gNnSc1
samozřejmě	samozřejmě	k6eAd1
vede	vést	k5eAaImIp3nS
k	k	k7c3
nárůstu	nárůst	k1gInSc3
počtu	počet	k1gInSc2
osob	osoba	k1gFnPc2
ve	v	k7c6
výkonu	výkon	k1gInSc6
trestu	trest	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
IDnes	IDnes	k1gInSc1
<g/>
:	:	kIx,
Doživotní	doživotní	k2eAgInSc1d1
trest	trest	k1gInSc1
za	za	k7c4
krádež	krádež	k1gFnSc4
bundy	bunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
