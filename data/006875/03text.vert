<s>
Dušan	Dušan	k1gMnSc1	Dušan
Swalens	Swalensa	k1gFnPc2	Swalensa
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
filozofem	filozof	k1gMnSc7	filozof
a	a	k8xC	a
fotografem	fotograf	k1gMnSc7	fotograf
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Růžičkou	Růžička	k1gMnSc7	Růžička
<g/>
,	,	kIx,	,
zakladatelem	zakladatel	k1gMnSc7	zakladatel
umělecké	umělecký	k2eAgFnSc2d1	umělecká
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
žákem	žák	k1gMnSc7	žák
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
Dušan	Dušan	k1gMnSc1	Dušan
Polášek	Polášek	k1gMnSc1	Polášek
a	a	k8xC	a
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Vyšší	vysoký	k2eAgFnSc4d2	vyšší
odbornou	odborný	k2eAgFnSc4d1	odborná
školu	škola	k1gFnSc4	škola
filmovou	filmový	k2eAgFnSc4d1	filmová
<g/>
,	,	kIx,	,
obor	obor	k1gInSc4	obor
fotografie	fotografia	k1gFnSc2	fotografia
a	a	k8xC	a
kamera	kamera	k1gFnSc1	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
regionální	regionální	k2eAgFnSc4d1	regionální
televizi	televize	k1gFnSc4	televize
i	i	k9	i
jako	jako	k8xS	jako
kameraman	kameraman	k1gMnSc1	kameraman
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
fotografem	fotograf	k1gMnSc7	fotograf
několika	několik	k4yIc2	několik
ročníků	ročník	k1gInPc2	ročník
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
festivalu	festival	k1gInSc2	festival
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Dušan	Dušan	k1gMnSc1	Dušan
Swalens	Swalensa	k1gFnPc2	Swalensa
existenčně	existenčně	k6eAd1	existenčně
zakotvil	zakotvit	k5eAaPmAgMnS	zakotvit
jako	jako	k9	jako
koordinátor	koordinátor	k1gMnSc1	koordinátor
a	a	k8xC	a
manažer	manažer	k1gMnSc1	manažer
firmy	firma	k1gFnSc2	firma
Chocoland	Chocolanda	k1gFnPc2	Chocolanda
v	v	k7c6	v
belgickém	belgický	k2eAgInSc6d1	belgický
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Fotografuje	fotografovat	k5eAaImIp3nS	fotografovat
externě	externě	k6eAd1	externě
pro	pro	k7c4	pro
evropské	evropský	k2eAgFnPc4d1	Evropská
instituce	instituce	k1gFnPc4	instituce
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
volné	volný	k2eAgFnSc6d1	volná
tvorbě	tvorba	k1gFnSc6	tvorba
chce	chtít	k5eAaImIp3nS	chtít
Dušan	Dušan	k1gMnSc1	Dušan
Swalens	Swalens	k1gInSc4	Swalens
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
způsobem	způsob	k1gInSc7	způsob
provokovat	provokovat	k5eAaImF	provokovat
abstraktní	abstraktní	k2eAgNnSc4d1	abstraktní
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
makrofotografie	makrofotografie	k1gFnSc1	makrofotografie
má	mít	k5eAaImIp3nS	mít
snahu	snaha	k1gFnSc4	snaha
zachytit	zachytit	k5eAaPmF	zachytit
kreační	kreační	k2eAgInSc4d1	kreační
impulz	impulz	k1gInSc4	impulz
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
objektiv	objektiv	k1gInSc1	objektiv
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
prolnutí	prolnutí	k1gNnSc4	prolnutí
hmot	hmota	k1gFnPc2	hmota
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
zrod	zrod	k1gInSc4	zrod
nového	nový	k2eAgInSc2d1	nový
obsahu	obsah	k1gInSc2	obsah
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
abstraktního	abstraktní	k2eAgNnSc2d1	abstraktní
a	a	k8xC	a
fantaskního	fantaskní	k2eAgNnSc2d1	fantaskní
<g/>
,	,	kIx,	,
umožňujícího	umožňující	k2eAgMnSc2d1	umožňující
individuální	individuální	k2eAgFnSc7d1	individuální
interpretaci	interpretace	k1gFnSc4	interpretace
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Makrofotografii	makrofotografie	k1gFnSc4	makrofotografie
si	se	k3xPyFc3	se
nelze	lze	k6eNd1	lze
přesně	přesně	k6eAd1	přesně
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Probouzí	probouzet	k5eAaImIp3nS	probouzet
naši	náš	k3xOp1gFnSc4	náš
obraznost	obraznost	k1gFnSc4	obraznost
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
ji	on	k3xPp3gFnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
jinýma	jiný	k2eAgNnPc7d1	jiné
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
jiný	jiný	k2eAgInSc4d1	jiný
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Vnímatel	vnímatel	k1gMnSc1	vnímatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
konfrontován	konfrontován	k2eAgMnSc1d1	konfrontován
s	s	k7c7	s
makrofotografií	makrofotografie	k1gFnSc7	makrofotografie
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
mění	měnit	k5eAaImIp3nS	měnit
rozměrem	rozměr	k1gInSc7	rozměr
své	svůj	k3xOyFgFnSc2	svůj
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Makrofotografie	makrofotografie	k1gFnSc2	makrofotografie
Dušan	Dušan	k1gMnSc1	Dušan
Swalens	Swalensa	k1gFnPc2	Swalensa
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
(	(	kIx(	(
<g/>
Brusel	Brusel	k1gInSc1	Brusel
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
Valašské	valašský	k2eAgInPc1d1	valašský
Klobouky	Klobouky	k1gInPc1	Klobouky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrovaly	ilustrovat	k5eAaBmAgInP	ilustrovat
také	také	k9	také
sbírku	sbírka	k1gFnSc4	sbírka
vánočních	vánoční	k2eAgInPc2d1	vánoční
textů	text	k1gInPc2	text
Šťastikování	Šťastikování	k1gNnSc2	Šťastikování
(	(	kIx(	(
<g/>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vydal	vydat	k5eAaPmAgInS	vydat
knihu	kniha	k1gFnSc4	kniha
Elementy	element	k1gInPc4	element
<g/>
.	.	kIx.	.
</s>
