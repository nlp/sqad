<s>
PuTTY	PuTTY	k?	PuTTY
je	být	k5eAaImIp3nS	být
klient	klient	k1gMnSc1	klient
protokolů	protokol	k1gInPc2	protokol
SSH	SSH	kA	SSH
<g/>
,	,	kIx,	,
Telnet	Telneta	k1gFnPc2	Telneta
<g/>
,	,	kIx,	,
rlogin	rlogina	k1gFnPc2	rlogina
a	a	k8xC	a
holého	holý	k2eAgInSc2d1	holý
TCP	TCP	kA	TCP
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
jen	jen	k9	jen
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
i	i	k9	i
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
UNIXové	unixový	k2eAgFnPc4d1	unixová
platformy	platforma	k1gFnPc4	platforma
(	(	kIx(	(
<g/>
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
jiných	jiný	k2eAgFnPc2d1	jiná
platforem	platforma	k1gFnPc2	platforma
jako	jako	k8xC	jako
neoficiální	neoficiální	k2eAgInSc1d1	neoficiální
port	port	k1gInSc1	port
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
ho	on	k3xPp3gMnSc4	on
napsal	napsat	k5eAaBmAgMnS	napsat
a	a	k8xC	a
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
Simon	Simon	k1gMnSc1	Simon
Tatham	Tatham	k1gInSc4	Tatham
<g/>
.	.	kIx.	.
</s>
<s>
PuTTY	PuTTY	k?	PuTTY
je	být	k5eAaImIp3nS	být
svobodný	svobodný	k2eAgInSc1d1	svobodný
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
šířený	šířený	k2eAgInSc1d1	šířený
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
MIT	MIT	kA	MIT
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
0.58	[number]	k4	0.58
(	(	kIx(	(
<g/>
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc1	několik
nových	nový	k2eAgFnPc2d1	nová
vlastností	vlastnost	k1gFnPc2	vlastnost
jako	jako	k8xS	jako
vylepšena	vylepšen	k2eAgFnSc1d1	vylepšena
podpora	podpora	k1gFnSc1	podpora
Unicode	Unicod	k1gInSc5	Unicod
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
písma	písmo	k1gNnPc4	písmo
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
nebo	nebo	k8xC	nebo
oběma	dva	k4xCgInPc7	dva
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
0.58	[number]	k4	0.58
byly	být	k5eAaImAgFnP	být
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
tři	tři	k4xCgFnPc1	tři
verze	verze	k1gFnPc1	verze
(	(	kIx(	(
<g/>
0.55	[number]	k4	0.55
<g/>
-	-	kIx~	-
<g/>
0.57	[number]	k4	0.57
<g/>
)	)	kIx)	)
opravující	opravující	k2eAgFnSc2d1	opravující
významné	významný	k2eAgFnSc2d1	významná
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
díry	díra	k1gFnSc2	díra
předcházejících	předcházející	k2eAgFnPc2d1	předcházející
verzí	verze	k1gFnPc2	verze
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
umožňující	umožňující	k2eAgFnPc4d1	umožňující
kompromitaci	kompromitace	k1gFnSc4	kompromitace
klienta	klient	k1gMnSc2	klient
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
autentifikací	autentifikace	k1gFnSc7	autentifikace
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
PuTTY	PuTTY	k1gFnSc2	PuTTY
<g/>
:	:	kIx,	:
ukládání	ukládání	k1gNnSc1	ukládání
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
serverech	server	k1gInPc6	server
a	a	k8xC	a
nastavení	nastavení	k1gNnSc6	nastavení
výběr	výběr	k1gInSc4	výběr
protokolu	protokol	k1gInSc2	protokol
SSH	SSH	kA	SSH
a	a	k8xC	a
šifrovacího	šifrovací	k2eAgInSc2d1	šifrovací
klíče	klíč	k1gInSc2	klíč
klienti	klient	k1gMnPc1	klient
příkazového	příkazový	k2eAgInSc2d1	příkazový
řádku	řádek	k1gInSc2	řádek
SCP	SCP	kA	SCP
a	a	k8xC	a
SFTP	SFTP	kA	SFTP
(	(	kIx(	(
<g/>
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
pscp	pscp	k1gInSc1	pscp
a	a	k8xC	a
psftp	psftp	k1gInSc1	psftp
<g/>
)	)	kIx)	)
možnost	možnost	k1gFnSc1	možnost
forwardování	forwardování	k1gNnSc2	forwardování
portů	port	k1gInPc2	port
s	s	k7c7	s
SSH	SSH	kA	SSH
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vestavěného	vestavěný	k2eAgNnSc2d1	vestavěné
forwardování	forwardování	k1gNnSc2	forwardování
X	X	kA	X
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádření	vyjádření	k1gNnSc1	vyjádření
autora	autor	k1gMnSc2	autor
v	v	k7c6	v
FAQ	FAQ	kA	FAQ
k	k	k7c3	k
významu	význam	k1gInSc3	význam
názvu	název	k1gInSc2	název
PuTTY	PuTTY	k1gFnSc2	PuTTY
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
PuTTY	PuTTY	k1gFnSc1	PuTTY
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
populárního	populární	k2eAgNnSc2d1	populární
SSH	SSH	kA	SSH
a	a	k8xC	a
Telnet	Telnet	k1gMnSc1	Telnet
klienta	klient	k1gMnSc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Libovolný	libovolný	k2eAgInSc1d1	libovolný
jiný	jiný	k2eAgInSc1d1	jiný
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pozorovateli	pozorovatel	k1gMnSc6	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
PuTTY	PuTTY	k1gFnSc1	PuTTY
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
antonymem	antonymum	k1gNnSc7	antonymum
"	"	kIx"	"
<g/>
getty	getta	k1gFnSc2	getta
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dělá	dělat	k5eAaImIp3nS	dělat
Windows	Windows	kA	Windows
užitečnými	užitečný	k2eAgInPc7d1	užitečný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
plutoniový	plutoniový	k2eAgInSc1d1	plutoniový
dálnopis	dálnopis	k1gInSc1	dálnopis
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
takováto	takovýto	k3xDgNnPc4	takovýto
vyjádření	vyjádření	k1gNnPc4	vyjádření
komentovali	komentovat	k5eAaBmAgMnP	komentovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
funkce	funkce	k1gFnPc1	funkce
programu	program	k1gInSc2	program
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
několik	několik	k4yIc1	několik
programů	program	k1gInPc2	program
<g/>
:	:	kIx,	:
PuTTY	PuTTY	k1gMnPc2	PuTTY
-	-	kIx~	-
vlastní	vlastní	k2eAgInSc1d1	vlastní
Telnet	Telnet	k1gInSc1	Telnet
a	a	k8xC	a
SSH	SSH	kA	SSH
klient	klient	k1gMnSc1	klient
PSCP	PSCP	kA	PSCP
-	-	kIx~	-
SCP	SCP	kA	SCP
klient	klient	k1gMnSc1	klient
PSFTP	PSFTP	kA	PSFTP
-	-	kIx~	-
SFTP	SFTP	kA	SFTP
klient	klient	k1gMnSc1	klient
PuTTYtel	PuTTYtel	k1gMnSc1	PuTTYtel
-	-	kIx~	-
Telnet	Telnet	k1gMnSc1	Telnet
klient	klient	k1gMnSc1	klient
Plink	Plink	k1gMnSc1	Plink
-	-	kIx~	-
program	program	k1gInSc1	program
zajišťující	zajišťující	k2eAgFnSc1d1	zajišťující
šifrované	šifrovaný	k2eAgNnSc4d1	šifrované
spojení	spojení	k1gNnSc4	spojení
Pageant	Pageant	k1gMnSc1	Pageant
-	-	kIx~	-
agent	agent	k1gMnSc1	agent
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
autentizačních	autentizační	k2eAgInPc2d1	autentizační
klíčů	klíč	k1gInPc2	klíč
PuTTYgen	PuTTYgen	k1gInSc1	PuTTYgen
-	-	kIx~	-
program	program	k1gInSc1	program
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
RSA	RSA	kA	RSA
a	a	k8xC	a
DSA	DSA	kA	DSA
klíčů	klíč	k1gInPc2	klíč
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
PuTTY	PuTTY	k1gFnSc2	PuTTY
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
projektu	projekt	k1gInSc2	projekt
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
