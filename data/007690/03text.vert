<s>
Vyměřený	vyměřený	k2eAgInSc1d1	vyměřený
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
In	In	k1gFnSc2	In
Time	Tim	k1gFnSc2	Tim
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
dystopický	dystopický	k2eAgMnSc1d1	dystopický
sci-fi	scii	k1gNnPc2	sci-fi
akční	akční	k2eAgInSc1d1	akční
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
režisérem	režisér	k1gMnSc7	režisér
Andrewem	Andrew	k1gMnSc7	Andrew
Niccolem	Niccol	k1gMnSc7	Niccol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2169	[number]	k4	2169
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
geneticky	geneticky	k6eAd1	geneticky
upraveni	upraven	k2eAgMnPc1d1	upraven
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
měli	mít	k5eAaImAgMnP	mít
perfektní	perfektní	k2eAgNnSc4d1	perfektní
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
podkožní	podkožní	k2eAgFnSc2d1	podkožní
svítící	svítící	k2eAgFnSc2d1	svítící
digitální	digitální	k2eAgFnSc2d1	digitální
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
25	[number]	k4	25
letech	let	k1gInPc6	let
přestanou	přestat	k5eAaPmIp3nP	přestat
stárnout	stárnout	k5eAaImF	stárnout
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
hodiny	hodina	k1gFnPc4	hodina
začnou	začít	k5eAaPmIp3nP	začít
odpočítávat	odpočítávat	k5eAaImF	odpočítávat
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
člověku	člověk	k1gMnSc3	člověk
vyprší	vypršet	k5eAaPmIp3nS	vypršet
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
univerzální	univerzální	k2eAgFnSc7d1	univerzální
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
platit	platit	k5eAaImF	platit
za	za	k7c4	za
denní	denní	k2eAgInPc4d1	denní
výdaje	výdaj	k1gInPc4	výdaj
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
převádět	převádět	k5eAaImF	převádět
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
nebo	nebo	k8xC	nebo
časovými	časový	k2eAgFnPc7d1	časová
kapslemi	kapsle	k1gFnPc7	kapsle
(	(	kIx(	(
<g/>
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
peněženek	peněženka	k1gFnPc2	peněženka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zem	zem	k1gFnSc1	zem
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
časové	časový	k2eAgFnPc4d1	časová
zóny	zóna	k1gFnPc4	zóna
podle	podle	k7c2	podle
majetku	majetek	k1gInSc2	majetek
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
kontrastní	kontrastní	k2eAgFnPc4d1	kontrastní
zóny	zóna	k1gFnPc4	zóna
<g/>
:	:	kIx,	:
Dayton	Dayton	k1gInSc1	Dayton
-	-	kIx~	-
chudá	chudý	k2eAgFnSc1d1	chudá
oblast	oblast	k1gFnSc1	oblast
pracujících	pracující	k2eAgMnPc2d1	pracující
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
většinou	většina	k1gFnSc7	většina
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
méně	málo	k6eAd2	málo
-	-	kIx~	-
a	a	k8xC	a
New	New	k1gFnSc1	New
Greenwich	Greenwich	k1gInSc1	Greenwich
-	-	kIx~	-
majetná	majetný	k2eAgFnSc1d1	majetná
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
žili	žít	k5eAaImAgMnP	žít
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
století	století	k1gNnSc4	století
<g/>
.	.	kIx.	.
</s>
<s>
Will	Will	k1gMnSc1	Will
Salas	Salas	k1gMnSc1	Salas
je	být	k5eAaImIp3nS	být
28	[number]	k4	28
<g/>
-letý	etý	k2eAgMnSc1d1	-letý
tovární	tovární	k2eAgMnSc1d1	tovární
dělník	dělník	k1gMnSc1	dělník
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Daytonu	Dayton	k1gInSc6	Dayton
se	se	k3xPyFc4	se
svojí	svojit	k5eAaImIp3nS	svojit
50	[number]	k4	50
letou	letý	k2eAgFnSc7d1	letá
matkou	matka	k1gFnSc7	matka
Rachel	Rachela	k1gFnPc2	Rachela
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc3	jeden
noci	noc	k1gFnSc3	noc
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
baru	bar	k1gInSc6	bar
zachrání	zachránit	k5eAaPmIp3nP	zachránit
opilého	opilý	k2eAgMnSc4d1	opilý
105	[number]	k4	105
letého	letý	k2eAgInSc2d1	letý
muže	muž	k1gMnSc4	muž
jménem	jméno	k1gNnSc7	jméno
Henry	Henry	k1gMnSc1	Henry
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
oloupit	oloupit	k5eAaPmF	oloupit
Minutemeni	Minutemen	k2eAgMnPc1d1	Minutemen
(	(	kIx(	(
<g/>
zloději	zloděj	k1gMnPc1	zloděj
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
s	s	k7c7	s
75	[number]	k4	75
letým	letý	k2eAgInSc7d1	letý
Fortisem	Fortis	k1gInSc7	Fortis
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
téže	týž	k3xTgFnSc2	týž
noci	noc	k1gFnSc2	noc
na	na	k7c6	na
utajeném	utajený	k2eAgNnSc6d1	utajené
místě	místo	k1gNnSc6	místo
Hamilton	Hamilton	k1gInSc1	Hamilton
odhalí	odhalit	k5eAaPmIp3nS	odhalit
Willovi	Will	k1gMnSc3	Will
pravdu	pravda	k1gFnSc4	pravda
o	o	k7c6	o
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
času	čas	k1gInSc2	čas
<g/>
:	:	kIx,	:
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všichni	všechen	k3xTgMnPc1	všechen
mohli	moct	k5eAaImAgMnP	moct
žít	žít	k5eAaImF	žít
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Hamilton	Hamilton	k1gInSc1	Hamilton
dále	daleko	k6eAd2	daleko
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
že	že	k8xS	že
obyvatelé	obyvatel	k1gMnPc1	obyvatel
New	New	k1gFnSc2	New
Greenwich	Greenwich	k1gInSc1	Greenwich
hromadí	hromadit	k5eAaImIp3nS	hromadit
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
žít	žít	k5eAaImF	žít
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zvyšováním	zvyšování	k1gNnSc7	zvyšování
životních	životní	k2eAgInPc2d1	životní
nákladů	náklad	k1gInPc2	náklad
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
chudší	chudý	k2eAgMnPc4d2	chudší
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
Hamilton	Hamilton	k1gInSc1	Hamilton
převede	převést	k5eAaPmIp3nS	převést
spícímu	spící	k2eAgMnSc3d1	spící
Willovi	Will	k1gMnSc3	Will
116	[number]	k4	116
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
a	a	k8xC	a
sobě	se	k3xPyFc3	se
nechá	nechat	k5eAaPmIp3nS	nechat
jen	jen	k9	jen
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Will	Will	k1gInSc1	Will
se	se	k3xPyFc4	se
vzbudí	vzbudit	k5eAaPmIp3nS	vzbudit
a	a	k8xC	a
pospíchá	pospíchat	k5eAaImIp3nS	pospíchat
k	k	k7c3	k
blízkému	blízký	k2eAgInSc3d1	blízký
mostu	most	k1gInSc3	most
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
zídce	zídka	k1gFnSc6	zídka
sedí	sedit	k5eAaImIp3nS	sedit
Hamilton	Hamilton	k1gInSc4	Hamilton
<g/>
.	.	kIx.	.
</s>
<s>
Hamiltonovi	Hamilton	k1gMnSc3	Hamilton
dojde	dojít	k5eAaPmIp3nS	dojít
čas	čas	k1gInSc4	čas
a	a	k8xC	a
spadne	spadnout	k5eAaPmIp3nS	spadnout
z	z	k7c2	z
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Raymond	Raymond	k1gMnSc1	Raymond
Leon	Leona	k1gFnPc2	Leona
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
strážců	strážce	k1gMnPc2	strážce
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
policie	policie	k1gFnSc1	policie
<g/>
)	)	kIx)	)
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
smrt	smrt	k1gFnSc4	smrt
Hamilton	Hamilton	k1gInSc1	Hamilton
a	a	k8xC	a
chybně	chybně	k6eAd1	chybně
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
že	že	k8xS	že
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
hraje	hrát	k5eAaImIp3nS	hrát
Will	Will	k1gInSc1	Will
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Will	Wilnout	k5eAaPmAgMnS	Wilnout
navštíví	navštívit	k5eAaPmIp3nS	navštívit
svého	svůj	k3xOyFgMnSc4	svůj
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
kamaráda	kamarád	k1gMnSc4	kamarád
Borela	Borel	k1gMnSc4	Borel
a	a	k8xC	a
převede	převést	k5eAaPmIp3nS	převést
mu	on	k3xPp3gMnSc3	on
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Plánuje	plánovat	k5eAaImIp3nS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
přestěhuje	přestěhovat	k5eAaPmIp3nS	přestěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Greenwich	Greenwich	k1gInSc1	Greenwich
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
po	po	k7c6	po
práci	práce	k1gFnSc6	práce
utratí	utratit	k5eAaPmIp3nS	utratit
až	až	k9	až
na	na	k7c4	na
90	[number]	k4	90
minut	minuta	k1gFnPc2	minuta
všechen	všechen	k3xTgInSc4	všechen
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
za	za	k7c4	za
2	[number]	k4	2
denní	denní	k2eAgFnSc4d1	denní
půjčku	půjčka	k1gFnSc4	půjčka
nezbude	zbýt	k5eNaPmIp3nS	zbýt
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
domů	domů	k6eAd1	domů
autobusem	autobus	k1gInSc7	autobus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přes	přes	k7c4	přes
den	den	k1gInSc4	den
zdražila	zdražit	k5eAaPmAgFnS	zdražit
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc4	její
prosby	prosba	k1gFnPc4	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
jsou	být	k5eAaImIp3nP	být
ignorovány	ignorován	k2eAgInPc1d1	ignorován
jak	jak	k6eAd1	jak
řidičem	řidič	k1gMnSc7	řidič
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
pasažéry	pasažér	k1gMnPc7	pasažér
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
začne	začít	k5eAaPmIp3nS	začít
utíkat	utíkat	k5eAaImF	utíkat
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zastávce	zastávka	k1gFnSc6	zastávka
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
čeká	čekat	k5eAaImIp3nS	čekat
Will	Will	k1gInSc1	Will
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
přijede	přijet	k5eAaPmIp3nS	přijet
autobus	autobus	k1gInSc1	autobus
bez	bez	k7c2	bez
Rachel	Rachela	k1gFnPc2	Rachela
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
jí	on	k3xPp3gFnSc3	on
utíkat	utíkat	k5eAaImF	utíkat
naproti	naproti	k7c3	naproti
<g/>
.	.	kIx.	.
</s>
<s>
Rachel	Rachel	k1gInSc1	Rachel
dojde	dojít	k5eAaPmIp3nS	dojít
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
spadne	spadnout	k5eAaPmIp3nS	spadnout
Willovi	Will	k1gMnSc3	Will
do	do	k7c2	do
náručí	náručí	k1gNnSc2	náručí
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
ráno	ráno	k1gNnSc1	ráno
jede	jet	k5eAaImIp3nS	jet
Will	Will	k1gInSc4	Will
do	do	k7c2	do
New	New	k1gFnSc2	New
Greenwich	Greenwich	k1gInSc1	Greenwich
a	a	k8xC	a
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
si	se	k3xPyFc3	se
apartmá	apartmá	k1gNnSc4	apartmá
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Navštíví	navštívit	k5eAaPmIp3nP	navštívit
místní	místní	k2eAgMnPc1d1	místní
kasíno	kasína	k1gFnSc5	kasína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
se	s	k7c7	s
110	[number]	k4	110
letým	letý	k2eAgMnSc7d1	letý
podnikatelem	podnikatel	k1gMnSc7	podnikatel
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
půjčování	půjčování	k1gNnSc2	půjčování
času	čas	k1gInSc2	čas
Philippem	Philipp	k1gMnSc7	Philipp
Weisem	Weis	k1gMnSc7	Weis
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
27	[number]	k4	27
letou	letý	k2eAgFnSc7d1	letá
dcerou	dcera	k1gFnSc7	dcera
Sylvií	Sylvie	k1gFnPc2	Sylvie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hraní	hraní	k1gNnSc6	hraní
pokeru	poker	k1gInSc2	poker
s	s	k7c7	s
Weisem	Weis	k1gInSc7	Weis
Willovi	Will	k1gMnSc6	Will
málem	málem	k6eAd1	málem
dojde	dojít	k5eAaPmIp3nS	dojít
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
1100	[number]	k4	1100
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Sylvie	Sylvie	k1gFnSc1	Sylvie
ho	on	k3xPp3gMnSc4	on
pozve	pozvat	k5eAaPmIp3nS	pozvat
na	na	k7c4	na
party	party	k1gFnSc4	party
ve	v	k7c6	v
Weisově	Weisův	k2eAgFnSc6d1	Weisova
vile	vila	k1gFnSc6	vila
<g/>
.	.	kIx.	.
</s>
<s>
Will	Will	k1gMnSc1	Will
si	se	k3xPyFc3	se
koupí	koupit	k5eAaPmIp3nS	koupit
auto	auto	k1gNnSc4	auto
a	a	k8xC	a
jede	jet	k5eAaImIp3nS	jet
na	na	k7c4	na
party	party	k1gFnSc4	party
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
po	po	k7c4	po
chvíli	chvíle	k1gFnSc4	chvíle
přijedou	přijet	k5eAaPmIp3nP	přijet
zatknout	zatknout	k5eAaPmF	zatknout
strážci	strážce	k1gMnPc7	strážce
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Zamítnutím	zamítnutí	k1gNnSc7	zamítnutí
Willova	Willův	k2eAgNnSc2d1	Willovo
tvrzení	tvrzení	k1gNnSc2	tvrzení
o	o	k7c6	o
nevinnosti	nevinnost	k1gFnSc6	nevinnost
mu	on	k3xPp3gMnSc3	on
Raymond	Raymond	k1gInSc1	Raymond
zabaví	zabavit	k5eAaPmIp3nS	zabavit
všechen	všechen	k3xTgInSc4	všechen
čas	čas	k1gInSc4	čas
kromě	kromě	k7c2	kromě
dvou	dva	k4xCgFnPc2	dva
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Will	Will	k1gMnSc1	Will
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
Sylvii	Sylvie	k1gFnSc4	Sylvie
jako	jako	k8xC	jako
rukojmí	rukojmí	k1gMnPc4	rukojmí
a	a	k8xC	a
jede	jet	k5eAaImIp3nS	jet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Daytonu	Dayton	k1gInSc2	Dayton
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
přepadeni	přepadnout	k5eAaPmNgMnP	přepadnout
gangem	gang	k1gInSc7	gang
Fortise	Fortise	k1gFnSc2	Fortise
a	a	k8xC	a
oběma	dva	k4xCgMnPc7	dva
zbude	zbýt	k5eAaPmIp3nS	zbýt
jen	jen	k9	jen
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Will	Will	k1gInSc1	Will
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
dostat	dostat	k5eAaPmF	dostat
zpět	zpět	k6eAd1	zpět
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
od	od	k7c2	od
Borela	Borel	k1gMnSc2	Borel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Greta	Gret	k1gInSc2	Gret
mu	on	k3xPp3gMnSc3	on
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Borel	Borel	k1gInSc1	Borel
upil	upít	k5eAaPmAgInS	upít
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
se	s	k7c7	s
zbývajícími	zbývající	k2eAgNnPc7d1	zbývající
9	[number]	k4	9
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Sylvie	Sylvie	k1gFnSc1	Sylvie
dá	dát	k5eAaPmIp3nS	dát
svoje	svůj	k3xOyFgFnPc4	svůj
náušnice	náušnice	k1gFnPc4	náušnice
do	do	k7c2	do
zastavárny	zastavárna	k1gFnSc2	zastavárna
a	a	k8xC	a
obdrží	obdržet	k5eAaPmIp3nS	obdržet
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Will	Wilnout	k5eAaPmAgMnS	Wilnout
zavolá	zavolat	k5eAaPmIp3nS	zavolat
Weisovi	Weis	k1gMnSc3	Weis
a	a	k8xC	a
požaduje	požadovat	k5eAaImIp3nS	požadovat
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
výkupného	výkupné	k1gNnSc2	výkupné
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Weis	Weis	k1gInSc1	Weis
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
Will	Willum	k1gNnPc2	Willum
propustit	propustit	k5eAaPmF	propustit
Sylvii	Sylvie	k1gFnSc3	Sylvie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
poté	poté	k6eAd1	poté
volá	volat	k5eAaImIp3nS	volat
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
z	z	k7c2	z
telefonní	telefonní	k2eAgFnSc2d1	telefonní
budky	budka	k1gFnSc2	budka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
všimne	všimnout	k5eAaPmIp3nS	všimnout
Raymonda	Raymonda	k1gFnSc1	Raymonda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
přepadnout	přepadnout	k5eAaPmF	přepadnout
Willa	Will	k1gMnSc4	Will
a	a	k8xC	a
omylem	omylem	k6eAd1	omylem
ho	on	k3xPp3gMnSc4	on
postřelí	postřelit	k5eAaPmIp3nS	postřelit
<g/>
.	.	kIx.	.
</s>
<s>
Will	Will	k1gInSc1	Will
a	a	k8xC	a
Sylvie	Sylvie	k1gFnPc1	Sylvie
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
spojí	spojit	k5eAaPmIp3nP	spojit
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
přepadat	přepadat	k5eAaImF	přepadat
Weisovy	Weisův	k2eAgFnPc1d1	Weisova
banky	banka	k1gFnPc1	banka
s	s	k7c7	s
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Přebývající	přebývající	k2eAgFnPc1d1	přebývající
časové	časový	k2eAgFnPc1d1	časová
kapsle	kapsle	k1gFnPc1	kapsle
rozdávají	rozdávat	k5eAaImIp3nP	rozdávat
chudým	chudý	k2eAgInSc7d1	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
nabídnuta	nabídnout	k5eAaPmNgFnS	nabídnout
odměna	odměna	k1gFnSc1	odměna
10	[number]	k4	10
let	léto	k1gNnPc2	léto
za	za	k7c4	za
jejich	jejich	k3xOp3gNnSc4	jejich
dopadení	dopadení	k1gNnSc4	dopadení
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
vystopuje	vystopovat	k5eAaPmIp3nS	vystopovat
Fortisův	Fortisův	k2eAgInSc1d1	Fortisův
gang	gang	k1gInSc1	gang
do	do	k7c2	do
jejich	jejich	k3xOp3gNnSc2	jejich
hotelového	hotelový	k2eAgNnSc2d1	hotelové
apartmá	apartmá	k1gNnSc2	apartmá
<g/>
.	.	kIx.	.
</s>
<s>
Will	Will	k1gMnSc1	Will
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
Fortise	Fortise	k1gFnPc4	Fortise
k	k	k7c3	k
časovému	časový	k2eAgInSc3d1	časový
duelu	duel	k1gInSc3	duel
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgMnSc6	který
Fortisovi	Fortis	k1gMnSc6	Fortis
dojde	dojít	k5eAaPmIp3nS	dojít
čas	čas	k1gInSc1	čas
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
jeho	jeho	k3xOp3gInSc2	jeho
gangu	gang	k1gInSc2	gang
Will	Will	k1gInSc1	Will
zastřelí	zastřelit	k5eAaPmIp3nP	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
si	se	k3xPyFc3	se
dvoje	dvoje	k4xRgInPc1	dvoje
uvědomí	uvědomit	k5eAaPmIp3nP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
ukrást	ukrást	k5eAaPmF	ukrást
dost	dost	k6eAd1	dost
času	čas	k1gInSc2	čas
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	k9	aby
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
změnili	změnit	k5eAaPmAgMnP	změnit
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
New	New	k1gFnSc1	New
Greenwich	Greenwich	k1gInSc1	Greenwich
jednoduše	jednoduše	k6eAd1	jednoduše
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vykompenzovali	vykompenzovat	k5eAaPmAgMnP	vykompenzovat
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
občané	občan	k1gMnPc1	občan
mají	mít	k5eAaImIp3nP	mít
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
Páru	pár	k1gInSc2	pár
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
vykrást	vykrást	k5eAaPmF	vykrást
Weisův	Weisův	k2eAgInSc4d1	Weisův
osobní	osobní	k2eAgInSc4d1	osobní
trezor	trezor	k1gInSc4	trezor
a	a	k8xC	a
obdrží	obdržet	k5eAaPmIp3nP	obdržet
kapsli	kapsle	k1gFnSc4	kapsle
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
milionem	milion	k4xCgInSc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Raymond	Raymond	k1gMnSc1	Raymond
je	on	k3xPp3gInPc4	on
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Daytonu	Dayton	k1gInSc2	Dayton
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
už	už	k6eAd1	už
pozdě	pozdě	k6eAd1	pozdě
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
překazil	překazit	k5eAaPmAgInS	překazit
rozdělení	rozdělení	k1gNnSc4	rozdělení
ukradeného	ukradený	k2eAgInSc2d1	ukradený
času	čas	k1gInSc2	čas
chudým	chudý	k1gMnSc7	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
je	on	k3xPp3gFnPc4	on
znovu	znovu	k6eAd1	znovu
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
až	až	k9	až
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jim	on	k3xPp3gMnPc3	on
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Daytonu	Dayton	k1gInSc2	Dayton
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zapomněl	zapomnět	k5eAaImAgMnS	zapomnět
vybrat	vybrat	k5eAaPmF	vybrat
svůj	svůj	k3xOyFgInSc4	svůj
denní	denní	k2eAgInSc4d1	denní
příděl	příděl	k1gInSc4	příděl
času	čas	k1gInSc2	čas
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Willovi	Will	k1gMnSc3	Will
a	a	k8xC	a
Sylvii	Sylvie	k1gFnSc3	Sylvie
zbývá	zbývat	k5eAaImIp3nS	zbývat
jen	jen	k9	jen
něco	něco	k3yInSc4	něco
málo	málo	k1gNnSc4	málo
přes	přes	k7c4	přes
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozeběhnou	rozeběhnout	k5eAaPmIp3nP	rozeběhnout
k	k	k7c3	k
Raymondovu	Raymondův	k2eAgInSc3d1	Raymondův
hlídkovému	hlídkový	k2eAgInSc3d1	hlídkový
autu	aut	k1gInSc3	aut
<g/>
.	.	kIx.	.
</s>
<s>
Will	Wilnout	k5eAaPmAgMnS	Wilnout
doběhne	doběhnout	k5eAaPmIp3nS	doběhnout
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
vezme	vzít	k5eAaPmIp3nS	vzít
si	se	k3xPyFc3	se
Raymondův	Raymondův	k2eAgInSc4d1	Raymondův
denní	denní	k2eAgInSc4d1	denní
příděl	příděl	k1gInSc4	příděl
času	čas	k1gInSc2	čas
a	a	k8xC	a
běží	běžet	k5eAaImIp3nS	běžet
Sylvii	Sylvie	k1gFnSc4	Sylvie
naproti	naproti	k6eAd1	naproti
ve	v	k7c6	v
scéně	scéna	k1gFnSc6	scéna
připomínající	připomínající	k2eAgFnSc4d1	připomínající
smrt	smrt	k1gFnSc4	smrt
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Rachel	Rachela	k1gFnPc2	Rachela
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
Sylvii	Sylvie	k1gFnSc4	Sylvie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zbývá	zbývat	k5eAaImIp3nS	zbývat
jen	jen	k9	jen
pár	pár	k4xCyI	pár
vteřin	vteřina	k1gFnPc2	vteřina
<g/>
,	,	kIx,	,
zachránit	zachránit	k5eAaPmF	zachránit
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
postupné	postupný	k2eAgNnSc4d1	postupné
zavírání	zavírání	k1gNnSc4	zavírání
Daytonských	daytonský	k2eAgFnPc2d1	Daytonská
továren	továrna	k1gFnPc2	továrna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
mají	mít	k5eAaImIp3nP	mít
najednou	najednou	k6eAd1	najednou
dost	dost	k6eAd1	dost
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
hromadně	hromadně	k6eAd1	hromadně
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Greenwich	Greenwich	k1gInSc1	Greenwich
<g/>
.	.	kIx.	.
</s>
<s>
Will	Will	k1gInSc1	Will
a	a	k8xC	a
Sylvie	Sylvie	k1gFnPc1	Sylvie
dále	daleko	k6eAd2	daleko
vykrádají	vykrádat	k5eAaImIp3nP	vykrádat
banky	banka	k1gFnPc4	banka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
zhroutit	zhroutit	k5eAaPmF	zhroutit
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
bohatí	bohatý	k2eAgMnPc1d1	bohatý
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
s	s	k7c7	s
náhlým	náhlý	k2eAgInSc7d1	náhlý
nárůstem	nárůst	k1gInSc7	nárůst
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
zóně	zóna	k1gFnSc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
В	В	k?	В
(	(	kIx(	(
<g/>
ф	ф	k?	ф
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Recenze	recenze	k1gFnSc1	recenze
filmu	film	k1gInSc2	film
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Sedmá	sedmý	k4xOgFnSc1	sedmý
generace	generace	k1gFnSc1	generace
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
