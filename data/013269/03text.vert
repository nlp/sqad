<p>
<s>
Chřástal	chřástal	k1gMnSc1	chřástal
malý	malý	k1gMnSc1	malý
(	(	kIx(	(
<g/>
Porzana	Porzana	k1gFnSc1	Porzana
parva	parva	k?	parva
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
druh	druh	k1gInSc1	druh
chřástala	chřástal	k1gMnSc2	chřástal
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
krátkokřídlých	krátkokřídlí	k1gMnPc2	krátkokřídlí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Monotypický	monotypický	k2eAgInSc1d1	monotypický
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikosti	velikost	k1gFnPc1	velikost
špačka	špaček	k1gMnSc2	špaček
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
zespodu	zespodu	k6eAd1	zespodu
modrošedý	modrošedý	k2eAgInSc1d1	modrošedý
<g/>
,	,	kIx,	,
shora	shora	k6eAd1	shora
hnědý	hnědý	k2eAgInSc1d1	hnědý
s	s	k7c7	s
černohnědými	černohnědý	k2eAgInPc7d1	černohnědý
proužky	proužek	k1gInPc7	proužek
<g/>
,	,	kIx,	,
podocasní	podocasní	k2eAgFnPc4d1	podocasní
krovky	krovka	k1gFnPc4	krovka
jsou	být	k5eAaImIp3nP	být
černobíle	černobíle	k6eAd1	černobíle
proužkované	proužkovaný	k2eAgFnPc1d1	proužkovaná
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
a	a	k8xC	a
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
zespodu	zespodu	k6eAd1	zespodu
béžoví	béžový	k2eAgMnPc1d1	béžový
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podobného	podobný	k2eAgMnSc2d1	podobný
chřástala	chřástal	k1gMnSc2	chřástal
nejmenšího	malý	k2eAgInSc2d3	nejmenší
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
následujícími	následující	k2eAgInPc7d1	následující
znaky	znak	k1gInPc7	znak
<g/>
:	:	kIx,	:
velkým	velký	k2eAgInSc7d1	velký
přesahem	přesah	k1gInSc7	přesah
ručních	ruční	k2eAgFnPc2d1	ruční
letek	letka	k1gFnPc2	letka
<g/>
,	,	kIx,	,
delším	dlouhý	k2eAgInSc7d2	delší
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
červeným	červený	k2eAgNnSc7d1	červené
zbarvením	zbarvení	k1gNnSc7	zbarvení
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
žádnou	žádný	k3yNgFnSc4	žádný
bílou	bílý	k2eAgFnSc4d1	bílá
kresbu	kresba	k1gFnSc4	kresba
na	na	k7c6	na
svrchní	svrchní	k2eAgFnSc6d1	svrchní
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
proužkováním	proužkování	k1gNnSc7	proužkování
omezeným	omezený	k2eAgInSc7d1	omezený
jen	jen	k9	jen
na	na	k7c4	na
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
boků	bok	k1gInPc2	bok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
palearktický	palearktický	k2eAgInSc4d1	palearktický
typ	typ	k1gInSc4	typ
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tažný	tažný	k2eAgMnSc1d1	tažný
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
zimoviště	zimoviště	k1gNnSc1	zimoviště
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
zimuje	zimovat	k5eAaImIp3nS	zimovat
i	i	k9	i
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
rákosí	rákosí	k1gNnSc6	rákosí
na	na	k7c6	na
hlubší	hluboký	k2eAgFnSc6d2	hlubší
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
odhadem	odhad	k1gInSc7	odhad
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
středním	střední	k2eAgNnSc6d1	střední
Polabí	Polabí	k1gNnSc6	Polabí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
a	a	k8xC	a
teritoriálně	teritoriálně	k6eAd1	teritoriálně
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
ze	z	k7c2	z
suchých	suchý	k2eAgInPc2d1	suchý
listů	list	k1gInPc2	list
staví	stavit	k5eAaImIp3nP	stavit
zřejmě	zřejmě	k6eAd1	zřejmě
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
dobře	dobře	k6eAd1	dobře
skryté	skrytý	k2eAgFnPc4d1	skrytá
nízko	nízko	k6eAd1	nízko
v	v	k7c6	v
hustém	hustý	k2eAgInSc6d1	hustý
porostu	porost	k1gInSc6	porost
nedaleko	nedaleko	k7c2	nedaleko
volné	volný	k2eAgFnSc2d1	volná
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Snůška	snůška	k1gFnSc1	snůška
čítá	čítat	k5eAaImIp3nS	čítat
obvykle	obvykle	k6eAd1	obvykle
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
žlutavých	žlutavý	k2eAgFnPc2d1	žlutavá
nebo	nebo	k8xC	nebo
našedlých	našedlý	k2eAgFnPc2d1	našedlá
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
hnědě	hnědě	k6eAd1	hnědě
skvrnitých	skvrnitý	k2eAgNnPc2d1	skvrnité
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
31,6	[number]	k4	31,6
x	x	k?	x
22,0	[number]	k4	22,0
mm	mm	kA	mm
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Sedí	sedit	k5eAaImIp3nP	sedit
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
mohou	moct	k5eAaImIp3nP	moct
hnízdo	hnízdo	k1gNnSc4	hnízdo
při	při	k7c6	při
vyrušení	vyrušení	k1gNnSc6	vyrušení
opustit	opustit	k5eAaPmF	opustit
již	již	k6eAd1	již
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
krmena	krmen	k2eAgFnSc1d1	krmena
<g/>
.	.	kIx.	.
</s>
<s>
Vzletnosti	vzletnost	k1gFnPc1	vzletnost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mezi	mezi	k7c7	mezi
40	[number]	k4	40
<g/>
.	.	kIx.	.
až	až	k9	až
50	[number]	k4	50
<g/>
.	.	kIx.	.
dnem	den	k1gInSc7	den
věku	věk	k1gInSc2	věk
a	a	k8xC	a
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
koncem	koncem	k7c2	koncem
1	[number]	k4	1
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Potrava	potrava	k1gFnSc1	potrava
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
živočišná	živočišný	k2eAgFnSc1d1	živočišná
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
hlavně	hlavně	k9	hlavně
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc7	jeho
larvami	larva	k1gFnPc7	larva
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pavoukovci	pavoukovec	k1gMnPc1	pavoukovec
a	a	k8xC	a
malými	malý	k2eAgMnPc7d1	malý
měkkýši	měkkýš	k1gMnPc7	měkkýš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chřástal	chřástal	k1gMnSc1	chřástal
malý	malý	k2eAgMnSc1d1	malý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
chřástal	chřástal	k1gMnSc1	chřástal
malý	malý	k2eAgMnSc1d1	malý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Porzana	Porzana	k1gFnSc1	Porzana
parva	parva	k?	parva
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
