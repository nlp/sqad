<s>
Jedle	jedle	k6eAd1
pod	pod	k7c7
Špičákem	špičák	k1gInSc7
</s>
<s>
Památný	památný	k2eAgInSc1d1
strom	strom	k1gInSc1
(	(	kIx(
<g/>
chráněný	chráněný	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
Jedle	jedle	k6eAd1
pod	pod	k7c7
Špičákem	špičák	k1gInSc7
v	v	k7c6
srpnu	srpen	k1gInSc6
2020	#num#	k4
<g/>
Druh	druh	k1gInSc1
</s>
<s>
Jedle	jedle	k6eAd1
bělokoráAbies	bělokoráAbies	k1gInSc1
alba	album	k1gNnSc2
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Evidenční	evidenční	k2eAgFnSc1d1
č.	č.	k?
</s>
<s>
106150	#num#	k4
Ochrana	ochrana	k1gFnSc1
</s>
<s>
30.09	30.09	k4
<g/>
.2016	.2016	k4
Poloha	poloha	k1gFnSc1
Kraj	kraj	k7c2
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Sokolov	Sokolov	k1gInSc1
Obec	obec	k1gFnSc1
</s>
<s>
Šindelová	šindelový	k2eAgFnSc1d1
Katastr	katastr	k1gInSc1
</s>
<s>
Obora	obora	k1gFnSc1
u	u	k7c2
Šindelové	šindelový	k2eAgFnSc2d1
Nadmořská	nadmořský	k2eAgNnPc5d1
výška	výška	k1gFnSc1
</s>
<s>
885	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
29,2	29,2	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
17,45	17,45	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Jedle	jedle	k6eAd1
pod	pod	k7c7
Špičákem	špičák	k1gInSc7
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jedle	jedle	k6eAd1
pod	pod	k7c7
Špičákem	špičák	k1gInSc7
je	být	k5eAaImIp3nS
památný	památný	k2eAgInSc4d1
strom	strom	k1gInSc4
<g/>
,	,	kIx,
jedle	jedle	k6eAd1
bělokorá	bělokorý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Abies	Abies	k1gInSc1
alba	album	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
roste	růst	k5eAaImIp3nS
na	na	k7c6
svahu	svah	k1gInSc6
Špičáku	špičák	k1gInSc2
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnPc1d3
hory	hora	k1gFnPc1
okresu	okres	k1gInSc2
Sokolov	Sokolov	k1gInSc1
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
1,2	1,2	k4
km	km	kA
jižně	jižně	k6eAd1
od	od	k7c2
vrcholu	vrchol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Strom	strom	k1gInSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
několik	několik	k4yIc4
posledních	poslední	k2eAgFnPc2d1
jedlí	jedle	k1gFnPc2
na	na	k7c4
Kraslicku	Kraslicka	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
mála	málo	k4c2
vzrostlých	vzrostlý	k2eAgMnPc2d1
a	a	k8xC
současně	současně	k6eAd1
plodných	plodný	k2eAgFnPc2d1
jedlí	jedle	k1gFnPc2
v	v	k7c6
Krušných	krušný	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obvod	obvod	k1gInSc1
přímého	přímý	k2eAgInSc2d1
<g/>
,	,	kIx,
k	k	k7c3
jihu	jih	k1gInSc3
mírně	mírně	k6eAd1
vykloněného	vykloněný	k2eAgInSc2d1
kmene	kmen	k1gInSc2
měří	měřit	k5eAaImIp3nS
238	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
vysoko	vysoko	k6eAd1
nasazená	nasazený	k2eAgFnSc1d1
a	a	k8xC
ve	v	k7c6
spodní	spodní	k2eAgFnSc6d1
části	část	k1gFnSc6
mírně	mírně	k6eAd1
proschlá	proschlý	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
sahá	sahat	k5eAaImIp3nS
do	do	k7c2
výšky	výška	k1gFnSc2
31	#num#	k4
m	m	kA
(	(	kIx(
<g/>
měření	měření	k1gNnSc4
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ukončena	ukončit	k5eAaPmNgFnS
hustým	hustý	k2eAgNnSc7d1
seskupením	seskupení	k1gNnSc7
bočních	boční	k2eAgFnPc2d1
podvrcholových	podvrcholový	k2eAgFnPc2d1
větví	větev	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
stáčejí	stáčet	k5eAaImIp3nP
vzhůru	vzhůru	k6eAd1
a	a	k8xC
přerůstají	přerůstat	k5eAaImIp3nP
terminální	terminální	k2eAgInSc4d1
vrchol	vrchol	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jedle	jedle	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
dobrém	dobrý	k2eAgInSc6d1
zdravotním	zdravotní	k2eAgInSc6d1
stavu	stav	k1gInSc6
a	a	k8xC
protože	protože	k8xS
dosud	dosud	k6eAd1
plodí	plodit	k5eAaImIp3nS
je	být	k5eAaImIp3nS
oplocená	oplocený	k2eAgFnSc1d1
pro	pro	k7c4
zdárný	zdárný	k2eAgInSc4d1
růst	růst	k1gInSc4
semenáčků	semenáček	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
památný	památný	k2eAgInSc4d1
byl	být	k5eAaImAgInS
strom	strom	k1gInSc1
vyhlášen	vyhlásit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
jako	jako	k8xS,k8xC
autochtonní	autochtonní	k2eAgInSc1d1
druh	druh	k1gInSc1
a	a	k8xC
strom	strom	k1gInSc1
s	s	k7c7
významným	významný	k2eAgInSc7d1
vzrůstem	vzrůst	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stromy	strom	k1gInPc1
v	v	k7c6
okolí	okolí	k1gNnSc6
</s>
<s>
Jíva	jíva	k1gFnSc1
v	v	k7c6
Horní	horní	k2eAgFnSc6d1
Oboře	obora	k1gFnSc6
</s>
<s>
Modřín	modřín	k1gInSc1
v	v	k7c6
Horní	horní	k2eAgFnSc6d1
Oboře	obora	k1gFnSc6
</s>
<s>
Klen	klen	k1gInSc1
v	v	k7c6
Horní	horní	k2eAgFnSc6d1
Oboře	obora	k1gFnSc6
</s>
<s>
Jedle	jedle	k1gFnSc1
pod	pod	k7c7
skálou	skála	k1gFnSc7
v	v	k7c6
Nancy	Nancy	k1gFnSc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Registr	registr	k1gInSc4
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
AOPK	AOPK	kA
ČR	ČR	kA
drusop	drusop	k1gInSc4
<g/>
.	.	kIx.
<g/>
nature	natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
</s>
<s>
Informace	informace	k1gFnPc1
na	na	k7c6
tabulce	tabulka	k1gFnSc6
památného	památný	k2eAgInSc2d1
stromu	strom	k1gInSc2
<g/>
↑	↑	k?
MICHÁLEK	Michálek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památné	památný	k2eAgInPc1d1
stromy	strom	k1gInPc1
Karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolov	Sokolov	k1gInSc1
<g/>
:	:	kIx,
Muzeum	muzeum	k1gNnSc1
Sokolov	Sokolovo	k1gNnPc2
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
93	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
88017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
<g/>
,	,	kIx,
návrh	návrh	k1gInSc1
PS	PS	kA
<g/>
.	.	kIx.
↑	↑	k?
Odborná	odborný	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jedle	jedle	k1gFnSc2
pod	pod	k7c7
Špičákem	špičák	k1gInSc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Stromy	strom	k1gInPc1
</s>
