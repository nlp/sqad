<p>
<s>
Oceánické	oceánický	k2eAgNnSc1d1	oceánické
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
oceánské	oceánský	k2eAgNnSc1d1	oceánské
klima	klima	k1gNnSc1	klima
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
malými	malý	k2eAgInPc7d1	malý
denními	denní	k2eAgInPc7d1	denní
rozdíly	rozdíl	k1gInPc7	rozdíl
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
měsíční	měsíční	k2eAgFnSc7d1	měsíční
teplotou	teplota	k1gFnSc7	teplota
v	v	k7c6	v
roce	rok	k1gInSc6	rok
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
15	[number]	k4	15
°	°	k?	°
<g/>
C.	C.	kA	C.
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
podnebí	podnebí	k1gNnSc2	podnebí
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
typická	typický	k2eAgFnSc1d1	typická
vysoká	vysoký	k2eAgFnSc1d1	vysoká
oblačnost	oblačnost	k1gFnSc1	oblačnost
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozloženy	rozložit	k5eAaPmNgInP	rozložit
do	do	k7c2	do
všech	všecek	k3xTgNnPc2	všecek
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
podnebí	podnebí	k1gNnSc2	podnebí
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
např.	např.	kA	např.
pro	pro	k7c4	pro
západní	západní	k2eAgNnSc4d1	západní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Evropy	Evropa	k1gFnSc2	Evropa
nebo	nebo	k8xC	nebo
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
se	se	k3xPyFc4	se
na	na	k7c4	na
2	[number]	k4	2
podtypy	podtyp	k1gInPc4	podtyp
<g/>
,	,	kIx,	,
Cfb	Cfb	k1gFnPc4	Cfb
v	v	k7c6	v
mírných	mírný	k2eAgFnPc6d1	mírná
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
a	a	k8xC	a
Cfc	Cfc	k1gFnPc6	Cfc
v	v	k7c6	v
subpolárních	subpolární	k2eAgFnPc6d1	subpolární
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mírné	mírný	k2eAgNnSc4d1	mírné
oceánické	oceánický	k2eAgNnSc4d1	oceánické
podnebí	podnebí	k1gNnSc4	podnebí
(	(	kIx(	(
<g/>
Cfb	Cfb	k1gFnSc1	Cfb
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
podnebí	podnebí	k1gNnSc1	podnebí
nemá	mít	k5eNaImIp3nS	mít
příliš	příliš	k6eAd1	příliš
teplá	teplý	k2eAgNnPc4d1	teplé
léta	léto	k1gNnPc4	léto
ani	ani	k8xC	ani
moc	moc	k1gFnSc4	moc
studené	studený	k2eAgFnSc2d1	studená
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
charakteristiky	charakteristika	k1gFnPc4	charakteristika
patří	patřit	k5eAaImIp3nS	patřit
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
nejteplejšího	teplý	k2eAgInSc2d3	nejteplejší
měsíce	měsíc	k1gInSc2	měsíc
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
22	[number]	k4	22
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
toho	ten	k3xDgNnSc2	ten
nejchladnějšího	chladný	k2eAgNnSc2d3	nejchladnější
vyšší	vysoký	k2eAgFnSc3d2	vyšší
než	než	k8xS	než
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
nebo	nebo	k8xC	nebo
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
se	se	k3xPyFc4	se
meteorologové	meteorolog	k1gMnPc1	meteorolog
neshodují	shodovat	k5eNaImIp3nP	shodovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
průměrné	průměrný	k2eAgFnSc2d1	průměrná
teploty	teplota	k1gFnSc2	teplota
nejteplejšího	teplý	k2eAgInSc2d3	nejteplejší
a	a	k8xC	a
nejstudenějšího	studený	k2eAgInSc2d3	nejstudenější
měsíce	měsíc	k1gInSc2	měsíc
bývá	bývat	k5eAaImIp3nS	bývat
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejsilněji	silně	k6eAd3	silně
oceánských	oceánský	k2eAgNnPc6d1	oceánské
místech	místo	k1gNnPc6	místo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
nebo	nebo	k8xC	nebo
i	i	k9	i
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozložené	rozložený	k2eAgFnPc1d1	rozložená
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teploty	teplota	k1gFnPc1	teplota
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
letní	letní	k2eAgFnPc1d1	letní
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
15	[number]	k4	15
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
35	[number]	k4	35
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
návětrnosti	návětrnost	k1gFnSc2	návětrnost
a	a	k8xC	a
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnPc1d3	nejnižší
zimní	zimní	k2eAgFnPc1d1	zimní
teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
mezi	mezi	k7c7	mezi
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
5	[number]	k4	5
°	°	k?	°
<g/>
C.	C.	kA	C.
Pod	pod	k7c7	pod
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
klesají	klesat	k5eAaImIp3nP	klesat
hlavně	hlavně	k9	hlavně
hluboko	hluboko	k6eAd1	hluboko
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
nebo	nebo	k8xC	nebo
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
kontinentálním	kontinentální	k2eAgNnSc7d1	kontinentální
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Srážky	srážka	k1gFnPc1	srážka
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
srážek	srážka	k1gFnPc2	srážka
napadá	napadat	k5eAaBmIp3nS	napadat
při	při	k7c6	při
středně	středně	k6eAd1	středně
silném	silný	k2eAgInSc6d1	silný
dešti	dešť	k1gInSc6	dešť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
mohou	moct	k5eAaImIp3nP	moct
občas	občas	k6eAd1	občas
přijít	přijít	k5eAaPmF	přijít
bouřky	bouřka	k1gFnPc4	bouřka
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
zasněží	zasněžit	k5eAaPmIp3nS	zasněžit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
naprší	napršet	k5eAaPmIp3nS	napršet
hrubým	hrubý	k2eAgInSc7d1	hrubý
odhadem	odhad	k1gInSc7	odhad
od	od	k7c2	od
500	[number]	k4	500
mm	mm	kA	mm
do	do	k7c2	do
2500	[number]	k4	2500
mm	mm	kA	mm
(	(	kIx(	(
<g/>
nejsou	být	k5eNaImIp3nP	být
započítány	započítat	k5eAaPmNgFnP	započítat
vysoké	vysoký	k2eAgFnPc1d1	vysoká
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
Sněžit	sněžit	k5eAaImF	sněžit
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
nemusí	muset	k5eNaImIp3nS	muset
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
míst	místo	k1gNnPc2	místo
nasněží	nasněžit	k5eAaPmIp3nS	nasněžit
alespoň	alespoň	k9	alespoň
někdy	někdy	k6eAd1	někdy
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
pak	pak	k6eAd1	pak
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
maximálně	maximálně	k6eAd1	maximálně
60	[number]	k4	60
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvíce	nejvíce	k6eAd1	nejvíce
oceánských	oceánský	k2eAgNnPc6d1	oceánské
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
sněží	sněžit	k5eAaImIp3nS	sněžit
jen	jen	k9	jen
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc1	galerie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Subpolární	subpolární	k2eAgNnSc1d1	subpolární
oceánické	oceánický	k2eAgNnSc1d1	oceánické
podnebí	podnebí	k1gNnSc1	podnebí
(	(	kIx(	(
<g/>
Cfc	Cfc	k1gFnSc1	Cfc
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Subpolární	subpolární	k2eAgNnSc1d1	subpolární
oceánické	oceánický	k2eAgNnSc1d1	oceánické
podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgNnSc1d3	nejčastější
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
pobřežích	pobřeží	k1gNnPc6	pobřeží
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc7	jeho
základními	základní	k2eAgFnPc7d1	základní
charakteristikami	charakteristika	k1gFnPc7	charakteristika
jsou	být	k5eAaImIp3nP	být
jeden	jeden	k4xCgMnSc1	jeden
až	až	k8xS	až
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
měsíční	měsíční	k2eAgFnSc7d1	měsíční
teplotou	teplota	k1gFnSc7	teplota
nad	nad	k7c7	nad
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
nejstudenějšího	studený	k2eAgInSc2d3	nejstudenější
měsíce	měsíc	k1gInSc2	měsíc
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgInPc4d1	malý
výkyvy	výkyv	k1gInPc4	výkyv
teploty	teplota	k1gFnSc2	teplota
jak	jak	k8xS	jak
přes	přes	k7c4	přes
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
mezi	mezi	k7c7	mezi
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
zimou	zima	k1gFnSc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozložené	rozložený	k2eAgFnPc1d1	rozložená
<g/>
,	,	kIx,	,
sněžení	sněžení	k1gNnSc1	sněžení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
časté	častý	k2eAgNnSc1d1	časté
a	a	k8xC	a
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
udržet	udržet	k5eAaPmF	udržet
i	i	k9	i
dlouhodobější	dlouhodobý	k2eAgFnSc1d2	dlouhodobější
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Adak	Adaka	k1gFnPc2	Adaka
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
výška	výška	k1gFnSc1	výška
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
do	do	k7c2	do
10	[number]	k4	10
cm	cm	kA	cm
a	a	k8xC	a
v	v	k7c6	v
Reykjavíku	Reykjavík	k1gInSc6	Reykjavík
leží	ležet	k5eAaImIp3nS	ležet
sníh	sníh	k1gInSc1	sníh
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
49	[number]	k4	49
dní	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
deště	dešť	k1gInSc2	dešť
naprší	napršet	k5eAaPmIp3nS	napršet
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
lehkého	lehký	k2eAgInSc2d1	lehký
deště	dešť	k1gInSc2	dešť
nebo	nebo	k8xC	nebo
lehkého	lehký	k2eAgNnSc2d1	lehké
sněžení	sněžení	k1gNnSc2	sněžení
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
napadaných	napadaný	k2eAgFnPc2d1	napadaná
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
různorodé	různorodý	k2eAgNnSc1d1	různorodé
<g/>
,	,	kIx,	,
od	od	k7c2	od
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
mm	mm	kA	mm
až	až	k9	až
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1500	[number]	k4	1500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teploty	teplota	k1gFnPc1	teplota
===	===	k?	===
</s>
</p>
<p>
<s>
Absolutně	absolutně	k6eAd1	absolutně
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
letní	letní	k2eAgFnPc1d1	letní
teploty	teplota	k1gFnPc1	teplota
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
15	[number]	k4	15
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
nocích	nok	k1gInPc6	nok
obvykle	obvykle	k6eAd1	obvykle
5	[number]	k4	5
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejnižší	nízký	k2eAgFnPc1d3	nejnižší
zimní	zimní	k2eAgFnPc1d1	zimní
teploty	teplota	k1gFnPc1	teplota
klesají	klesat	k5eAaImIp3nP	klesat
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
až	až	k8xS	až
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
přes	přes	k7c4	přes
den	den	k1gInSc4	den
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nebývá	bývat	k5eNaImIp3nS	bývat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc1	galerie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Oceanic	Oceanice	k1gFnPc2	Oceanice
climate	climat	k1gInSc5	climat
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
