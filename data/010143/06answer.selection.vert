<s>
Oceánické	oceánický	k2eAgNnSc1d1	oceánické
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
oceánské	oceánský	k2eAgNnSc1d1	oceánské
klima	klima	k1gNnSc1	klima
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
malými	malý	k2eAgInPc7d1	malý
denními	denní	k2eAgInPc7d1	denní
rozdíly	rozdíl	k1gInPc7	rozdíl
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
