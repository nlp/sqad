<s>
Charles	Charles	k1gMnSc1	Charles
Dickens	Dickensa	k1gFnPc2	Dickensa
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
Portsmouth	Portsmouth	k1gInSc1	Portsmouth
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
Gadshill	Gadshill	k1gMnSc1	Gadshill
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
romanopisců	romanopisec	k1gMnPc2	romanopisec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
bídě	bída	k1gFnSc6	bída
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
přestěhování	přestěhování	k1gNnSc6	přestěhování
rodiny	rodina	k1gFnSc2	rodina
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
dokonce	dokonce	k9	dokonce
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
pro	pro	k7c4	pro
dlužníky	dlužník	k1gMnPc4	dlužník
<g/>
.	.	kIx.	.
</s>
<s>
Dickens	Dickens	k6eAd1	Dickens
si	se	k3xPyFc3	se
musel	muset	k5eAaImAgMnS	muset
sám	sám	k3xTgInSc4	sám
vydělávat	vydělávat	k5eAaImF	vydělávat
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
desetiletý	desetiletý	k2eAgMnSc1d1	desetiletý
chlapec	chlapec	k1gMnSc1	chlapec
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
leštidla	leštidlo	k1gNnPc4	leštidlo
na	na	k7c4	na
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
zvládl	zvládnout	k5eAaPmAgMnS	zvládnout
těsnopis	těsnopis	k1gInSc4	těsnopis
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
písař	písař	k1gMnSc1	písař
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
listu	list	k1gInSc6	list
Morning	Morning	k1gInSc1	Morning
Chronicle	Chronicle	k1gInSc1	Chronicle
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
literární	literární	k2eAgFnSc4d1	literární
dráhu	dráha	k1gFnSc4	dráha
zahájil	zahájit	k5eAaPmAgMnS	zahájit
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
drobnými	drobný	k2eAgFnPc7d1	drobná
časopiseckými	časopisecký	k2eAgFnPc7d1	časopisecká
črtami	črta	k1gFnPc7	črta
a	a	k8xC	a
povídkami	povídka	k1gFnPc7	povídka
<g/>
,	,	kIx,	,
vydávanými	vydávaný	k2eAgMnPc7d1	vydávaný
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Boz	Boz	k1gFnSc2	Boz
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
zachycoval	zachycovat	k5eAaImAgInS	zachycovat
londýnské	londýnský	k2eAgNnSc4d1	Londýnské
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
postavy	postava	k1gFnPc4	postava
i	i	k9	i
z	z	k7c2	z
nižších	nízký	k2eAgFnPc2d2	nižší
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
jeho	jeho	k3xOp3gFnPc1	jeho
práce	práce	k1gFnPc1	práce
plné	plný	k2eAgFnPc1d1	plná
humoru	humor	k1gInSc2	humor
a	a	k8xC	a
vtipu	vtip	k1gInSc2	vtip
si	se	k3xPyFc3	se
čtenáři	čtenář	k1gMnPc1	čtenář
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pustit	pustit	k5eAaPmF	pustit
se	se	k3xPyFc4	se
do	do	k7c2	do
větších	veliký	k2eAgFnPc2d2	veliký
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
své	svůj	k3xOyFgFnSc2	svůj
črty	črta	k1gFnSc2	črta
vydal	vydat	k5eAaPmAgMnS	vydat
knižně	knižně	k6eAd1	knižně
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
nakladatele	nakladatel	k1gMnSc2	nakladatel
objednávku	objednávka	k1gFnSc4	objednávka
na	na	k7c4	na
Kroniku	kronika	k1gFnSc4	kronika
Pickwickova	Pickwickův	k2eAgInSc2d1	Pickwickův
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
dílo	dílo	k1gNnSc1	dílo
a	a	k8xC	a
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
jej	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Dickens	Dickens	k1gInSc1	Dickens
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Catherine	Catherin	k1gInSc5	Catherin
Hogarthovou	Hogarthová	k1gFnSc7	Hogarthová
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
George	Georg	k1gMnSc2	Georg
Hogartha	Hogarth	k1gMnSc2	Hogarth
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
deset	deset	k4xCc4	deset
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nuzných	nuzný	k2eAgInPc2d1	nuzný
poměrů	poměr	k1gInPc2	poměr
se	se	k3xPyFc4	se
Dickens	Dickens	k1gInSc1	Dickens
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
houževnatosti	houževnatost	k1gFnSc3	houževnatost
a	a	k8xC	a
vytrvalosti	vytrvalost	k1gFnSc3	vytrvalost
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
na	na	k7c4	na
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
hvězdu	hvězda	k1gFnSc4	hvězda
literárního	literární	k2eAgNnSc2d1	literární
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
slavný	slavný	k2eAgInSc1d1	slavný
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
i	i	k8xC	i
v	v	k7c6	v
USA	USA	kA	USA
pořádal	pořádat	k5eAaImAgMnS	pořádat
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgFnPc6	který
předčítal	předčítat	k5eAaImAgMnS	předčítat
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
Dickens	Dickens	k1gInSc1	Dickens
již	již	k6eAd1	již
necítil	cítit	k5eNaImAgInS	cítit
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
paměť	paměť	k1gFnSc4	paměť
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
první	první	k4xOgInPc4	první
příznaky	příznak	k1gInPc4	příznak
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
a	a	k8xC	a
nejpopulárnějším	populární	k2eAgMnSc7d3	nejpopulárnější
představitelem	představitel	k1gMnSc7	představitel
viktoriánského	viktoriánský	k2eAgInSc2d1	viktoriánský
románu	román	k1gInSc2	román
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
realistických	realistický	k2eAgMnPc2d1	realistický
autorů	autor	k1gMnPc2	autor
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgNnPc6d1	mnohé
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
zkušenosti	zkušenost	k1gFnPc4	zkušenost
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
celoživotním	celoživotní	k2eAgNnSc7d1	celoživotní
tématem	téma	k1gNnSc7	téma
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
tragický	tragický	k2eAgInSc1d1	tragický
osud	osud	k1gInSc1	osud
chudých	chudý	k2eAgFnPc2d1	chudá
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
romány	román	k1gInPc1	román
mají	mít	k5eAaImIp3nP	mít
promyšlenou	promyšlený	k2eAgFnSc4d1	promyšlená
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
Dickens	Dickens	k1gInSc4	Dickens
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc2	on
nevyhýbal	vyhýbat	k5eNaImAgMnS	vyhýbat
ani	ani	k9	ani
policejním	policejní	k2eAgMnSc7d1	policejní
a	a	k8xC	a
kriminálním	kriminální	k2eAgInPc3d1	kriminální
námětům	námět	k1gInPc3	námět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
mistrem	mistr	k1gMnSc7	mistr
dějových	dějový	k2eAgFnPc2d1	dějová
zápletek	zápletka	k1gFnPc2	zápletka
<g/>
,	,	kIx,	,
výstižných	výstižný	k2eAgFnPc2d1	výstižná
osobních	osobní	k2eAgFnPc2d1	osobní
charakteristik	charakteristika	k1gFnPc2	charakteristika
a	a	k8xC	a
usiloval	usilovat	k5eAaImAgInS	usilovat
i	i	k9	i
o	o	k7c4	o
citovou	citový	k2eAgFnSc4d1	citová
působivost	působivost	k1gFnSc4	působivost
<g/>
.	.	kIx.	.
</s>
<s>
Bozovy	Bozův	k2eAgFnPc1d1	Bozův
črty	črta	k1gFnPc1	črta
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
Sketches	Sketches	k1gInSc4	Sketches
by	by	kYmCp3nS	by
Boz	Boz	k1gMnSc1	Boz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
autorových	autorův	k2eAgFnPc2d1	autorova
prací	práce	k1gFnPc2	práce
napsaných	napsaný	k2eAgFnPc2d1	napsaná
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Boz	Boz	k1gFnSc2	Boz
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
Pickwickova	Pickwickův	k2eAgInSc2d1	Pickwickův
klubu	klub	k1gInSc2	klub
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Posthumous	Posthumous	k1gMnSc1	Posthumous
Papers	Papers	k1gInSc1	Papers
of	of	k?	of
the	the	k?	the
Pickwick	Pickwick	k1gInSc1	Pickwick
Club	club	k1gInSc1	club
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
humoristický	humoristický	k2eAgInSc1d1	humoristický
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ze	z	k7c2	z
série	série	k1gFnSc2	série
povídek	povídka	k1gFnPc2	povídka
vydávaných	vydávaný	k2eAgFnPc2d1	vydávaná
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vyprávění	vyprávění	k1gNnSc4	vyprávění
příhod	příhoda	k1gFnPc2	příhoda
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc7	jejichž
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
jsou	být	k5eAaImIp3nP	být
pan	pan	k1gMnSc1	pan
Samuel	Samuel	k1gMnSc1	Samuel
Pickwick	Pickwick	k1gMnSc1	Pickwick
(	(	kIx(	(
<g/>
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
překladu	překlad	k1gInSc6	překlad
Silvestr	Silvestr	k1gMnSc1	Silvestr
Papučka	papučka	k1gFnSc1	papučka
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
sluha	sluha	k1gMnSc1	sluha
Sam	Sam	k1gMnSc1	Sam
Weller	Weller	k1gMnSc1	Weller
(	(	kIx(	(
<g/>
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
překladu	překlad	k1gInSc6	překlad
Véna	Véna	k1gMnSc1	Véna
Příhoda	Příhoda	k1gMnSc1	Příhoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oliver	Oliver	k1gInSc1	Oliver
Twist	twist	k1gInSc1	twist
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
-	-	kIx~	-
<g/>
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Adventures	Adventures	k1gInSc1	Adventures
of	of	k?	of
Oliver	Oliver	k1gInSc1	Oliver
Twist	twist	k1gInSc1	twist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
neradostný	radostný	k2eNgInSc1d1	neradostný
příběh	příběh	k1gInSc1	příběh
chudého	chudý	k2eAgMnSc2d1	chudý
chlapce	chlapec	k1gMnSc2	chlapec
žijícího	žijící	k2eAgMnSc2d1	žijící
v	v	k7c6	v
chudobinci	chudobinec	k1gInSc6	chudobinec
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
chce	chtít	k5eAaImIp3nS	chtít
zlodějská	zlodějský	k2eAgFnSc1d1	zlodějská
banda	banda	k1gFnSc1	banda
zneužít	zneužít	k5eAaPmF	zneužít
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
loupeže	loupež	k1gFnPc4	loupež
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
líčí	líčit	k5eAaImIp3nS	líčit
s	s	k7c7	s
otřesnou	otřesný	k2eAgFnSc7d1	otřesná
otevřeností	otevřenost	k1gFnSc7	otevřenost
těžké	těžký	k2eAgInPc1d1	těžký
sociální	sociální	k2eAgInPc1d1	sociální
poměry	poměr	k1gInPc1	poměr
londýnské	londýnský	k2eAgFnSc2d1	londýnská
chudiny	chudina	k1gFnSc2	chudina
a	a	k8xC	a
spodiny	spodina	k1gFnSc2	spodina
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Črty	črta	k1gFnPc4	črta
o	o	k7c6	o
mladých	mladý	k2eAgMnPc6d1	mladý
pánech	pan	k1gMnPc6	pan
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
Sketches	Sketches	k1gMnSc1	Sketches
of	of	k?	of
Young	Young	k1gMnSc1	Young
Gentlemen	Gentlemen	k2eAgMnSc1d1	Gentlemen
<g/>
)	)	kIx)	)
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Nickleby	Nickleba	k1gMnSc2	Nickleba
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Life	Lif	k1gFnSc2	Lif
and	and	k?	and
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Nicholas	Nicholas	k1gMnSc1	Nicholas
Nickleby	Nickleba	k1gFnSc2	Nickleba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
líčící	líčící	k2eAgInPc1d1	líčící
osudy	osud	k1gInPc1	osud
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Nicklebyho	Nickleby	k1gMnSc2	Nickleby
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
hledá	hledat	k5eAaImIp3nS	hledat
pomoc	pomoc	k1gFnSc4	pomoc
u	u	k7c2	u
strýce	strýc	k1gMnSc2	strýc
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
o	o	k7c4	o
chudé	chudý	k2eAgMnPc4d1	chudý
příbuzné	příbuzný	k1gMnPc4	příbuzný
nestojí	stát	k5eNaImIp3nP	stát
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Starožitníkův	starožitníkův	k2eAgInSc1d1	starožitníkův
krám	krám	k1gInSc1	krám
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Old	Olda	k1gFnPc2	Olda
Curiosity	Curiosita	k1gFnSc2	Curiosita
Shop	shop	k1gInSc1	shop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
starého	starý	k2eAgMnSc2d1	starý
majitele	majitel	k1gMnSc2	majitel
londýnského	londýnský	k2eAgInSc2d1	londýnský
obchůdku	obchůdek	k1gInSc2	obchůdek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
žije	žít	k5eAaImIp3nS	žít
jen	jen	k9	jen
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vnučku	vnučka	k1gFnSc4	vnučka
Nell	Nella	k1gFnPc2	Nella
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
odolat	odolat	k5eAaPmF	odolat
svůdnému	svůdný	k2eAgNnSc3d1	svůdné
vábení	vábení	k1gNnSc3	vábení
hazardu	hazard	k1gInSc2	hazard
<g/>
.	.	kIx.	.
</s>
<s>
Půjčuje	půjčovat	k5eAaImIp3nS	půjčovat
si	se	k3xPyFc3	se
proto	proto	k8xC	proto
tajně	tajně	k6eAd1	tajně
od	od	k7c2	od
zlomyslného	zlomyslný	k2eAgMnSc2d1	zlomyslný
lichváře	lichvář	k1gMnSc2	lichvář
Quilpa	Quilp	k1gMnSc2	Quilp
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
musí	muset	k5eAaImIp3nS	muset
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
pronásledováním	pronásledování	k1gNnSc7	pronásledování
prchnout	prchnout	k5eAaPmF	prchnout
i	i	k9	i
s	s	k7c7	s
vnučkou	vnučka	k1gFnSc7	vnučka
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Barnabáš	Barnabat	k5eAaImIp2nS	Barnabat
Rudge	Rudge	k1gFnSc1	Rudge
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
Barnaby	Barnaba	k1gFnPc1	Barnaba
Rudge	Rudg	k1gInSc2	Rudg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
protikatolických	protikatolický	k2eAgInPc2d1	protikatolický
nepokojů	nepokoj	k1gInPc2	nepokoj
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
poznámky	poznámka	k1gFnPc1	poznámka
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
American	American	k1gInSc1	American
Notes	notes	k1gInSc1	notes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
napsané	napsaný	k2eAgNnSc1d1	napsané
po	po	k7c6	po
první	první	k4xOgFnSc6	první
návštěvě	návštěva	k1gFnSc6	návštěva
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
americkou	americký	k2eAgFnSc4d1	americká
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
viděl	vidět	k5eAaImAgMnS	vidět
její	její	k3xOp3gInPc4	její
nedostatky	nedostatek	k1gInPc4	nedostatek
a	a	k8xC	a
citlivě	citlivě	k6eAd1	citlivě
vystihl	vystihnout	k5eAaPmAgMnS	vystihnout
kde	kde	k6eAd1	kde
a	a	k8xC	a
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc4d1	americký
život	život	k1gInSc4	život
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
pokřiven	pokřivit	k5eAaPmNgMnS	pokřivit
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
koleda	koleda	k1gFnSc1	koleda
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
,	,	kIx,	,
A	a	k8xC	a
Christmas	Christmas	k1gInSc1	Christmas
Carol	Carola	k1gFnPc2	Carola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
autorových	autorův	k2eAgFnPc2d1	autorova
tzv.	tzv.	kA	tzv.
vánočních	vánoční	k2eAgFnPc2d1	vánoční
povídek	povídka	k1gFnPc2	povídka
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c4	o
o	o	k7c6	o
nelítostném	lítostný	k2eNgMnSc6d1	nelítostný
lichváři	lichvář	k1gMnSc6	lichvář
Ebenezeru	Ebenezer	k1gMnSc6	Ebenezer
Scroogeovi	Scroogeus	k1gMnSc6	Scroogeus
(	(	kIx(	(
<g/>
Vydřigrošovi	vydřigroš	k1gMnSc6	vydřigroš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
zjeví	zjevit	k5eAaPmIp3nS	zjevit
duch	duch	k1gMnSc1	duch
bývalého	bývalý	k2eAgMnSc2d1	bývalý
společníka	společník	k1gMnSc2	společník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
osudem	osud	k1gInSc7	osud
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
Martina	Martin	k1gMnSc2	Martin
Chuzzlewita	Chuzzlewit	k1gMnSc2	Chuzzlewit
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
Life	Life	k1gFnSc1	Life
And	Anda	k1gFnPc2	Anda
Adventures	Adventures	k1gMnSc1	Adventures
Of	Of	k1gMnSc1	Of
Martin	Martin	k1gMnSc1	Martin
Chuzzlewit	Chuzzlewit	k1gMnSc1	Chuzzlewit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
románová	románový	k2eAgFnSc1d1	románová
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
nedostatky	nedostatek	k1gInPc4	nedostatek
americké	americký	k2eAgFnSc2d1	americká
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Zvony	zvon	k1gInPc1	zvon
novoroční	novoroční	k2eAgFnSc2d1	novoroční
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Chimes	Chimes	k1gMnSc1	Chimes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
z	z	k7c2	z
autorových	autorův	k2eAgNnPc2d1	autorovo
tzv.	tzv.	kA	tzv.
vánočních	vánoční	k2eAgFnPc2d1	vánoční
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Cvrček	Cvrček	k1gMnSc1	Cvrček
u	u	k7c2	u
krbu	krb	k1gInSc2	krb
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
Cricket	Cricket	k1gInSc1	Cricket
on	on	k3xPp3gInSc1	on
the	the	k?	the
Heart	Heart	k1gInSc1	Heart
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
z	z	k7c2	z
autorových	autorův	k2eAgNnPc2d1	autorovo
tzv.	tzv.	kA	tzv.
vánočních	vánoční	k2eAgFnPc2d1	vánoční
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
Pictures	Pictures	k1gInSc1	Pictures
from	from	k6eAd1	from
Italy	Ital	k1gMnPc7	Ital
<g/>
)	)	kIx)	)
Dombey	Dombe	k1gMnPc7	Dombe
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
Dombey	Dombea	k1gFnPc1	Dombea
and	and	k?	and
Son	son	k1gInSc1	son
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrcholný	vrcholný	k2eAgInSc1d1	vrcholný
autorův	autorův	k2eAgInSc1d1	autorův
román	román	k1gInSc1	román
líčí	líčit	k5eAaImIp3nS	líčit
osudy	osud	k1gInPc4	osud
nelítostného	lítostný	k2eNgMnSc2d1	nelítostný
podnikatele	podnikatel	k1gMnPc4	podnikatel
Dombeyho	Dombey	k1gMnSc4	Dombey
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
nelidské	lidský	k2eNgNnSc4d1	nelidské
chování	chování	k1gNnSc4	chování
k	k	k7c3	k
vlastním	vlastní	k2eAgFnPc3d1	vlastní
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Dombey	Dombea	k1gFnPc4	Dombea
je	být	k5eAaImIp3nS	být
majitel	majitel	k1gMnSc1	majitel
obchodní	obchodní	k2eAgFnSc2d1	obchodní
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obětuje	obětovat	k5eAaBmIp3nS	obětovat
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
lidské	lidský	k2eAgFnPc4d1	lidská
emoce	emoce	k1gFnPc4	emoce
a	a	k8xC	a
ohledy	ohled	k1gInPc4	ohled
jedinému	jediný	k2eAgMnSc3d1	jediný
přání	přání	k1gNnPc4	přání
-	-	kIx~	-
mít	mít	k5eAaImF	mít
dědice	dědic	k1gMnSc4	dědic
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malém	malý	k2eAgMnSc6d1	malý
Pavlíčkovi	Pavlíček	k1gMnSc6	Pavlíček
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
potlačit	potlačit	k5eAaPmF	potlačit
všechno	všechen	k3xTgNnSc4	všechen
lidské	lidský	k2eAgNnSc4d1	lidské
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obstál	obstát	k5eAaPmAgMnS	obstát
v	v	k7c6	v
tvrdé	tvrdý	k2eAgFnSc6d1	tvrdá
konkurenci	konkurence	k1gFnSc6	konkurence
a	a	k8xC	a
udržel	udržet	k5eAaPmAgMnS	udržet
jméno	jméno	k1gNnSc4	jméno
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
synova	synův	k2eAgFnSc1d1	synova
předčasná	předčasný	k2eAgFnSc1d1	předčasná
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
neprobudí	probudit	k5eNaPmIp3nP	probudit
lidské	lidský	k2eAgInPc4d1	lidský
city	cit	k1gInPc4	cit
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
ožení	oženit	k5eAaPmIp3nS	oženit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žena	žena	k1gFnSc1	žena
mu	on	k3xPp3gMnSc3	on
uteče	utéct	k5eAaPmIp3nS	utéct
s	s	k7c7	s
ředitelem	ředitel	k1gMnSc7	ředitel
jeho	jeho	k3xOp3gFnSc2	jeho
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
ožebračeného	ožebračený	k2eAgMnSc4d1	ožebračený
Dombeyho	Dombey	k1gMnSc4	Dombey
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stará	starat	k5eAaImIp3nS	starat
zavrhovaná	zavrhovaný	k2eAgFnSc1d1	zavrhovaná
dcera	dcera	k1gFnSc1	dcera
Florence	Florenc	k1gFnSc2	Florenc
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Copperfield	Copperfield	k1gMnSc1	Copperfield
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
život	život	k1gInSc4	život
malého	malý	k2eAgMnSc2d1	malý
chlapce	chlapec	k1gMnSc2	chlapec
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
muže	muž	k1gMnSc2	muž
Davida	David	k1gMnSc2	David
Copperfielda	Copperfield	k1gMnSc2	Copperfield
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
autobiografickým	autobiografický	k2eAgNnSc7d1	autobiografické
dílem	dílo	k1gNnSc7	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
měšťácká	měšťácký	k2eAgFnSc1d1	měšťácká
bezohlednost	bezohlednost	k1gFnSc1	bezohlednost
a	a	k8xC	a
nenasytnost	nenasytnost	k1gFnSc1	nenasytnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
lidská	lidský	k2eAgFnSc1d1	lidská
dobrosrdečnost	dobrosrdečnost	k1gFnSc1	dobrosrdečnost
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgFnPc1d1	anglická
dějiny	dějiny	k1gFnPc1	dějiny
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
-	-	kIx~	-
<g/>
1853	[number]	k4	1853
<g/>
,	,	kIx,	,
A	a	k8xC	a
Child	Child	k1gMnSc1	Child
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
History	Histor	k1gInPc7	Histor
of	of	k?	of
England	England	k1gInSc1	England
<g/>
)	)	kIx)	)
Ponurý	ponurý	k2eAgInSc1d1	ponurý
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
,	,	kIx,	,
Bleak	Bleak	k1gInSc1	Bleak
House	house	k1gNnSc1	house
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
kolem	kolem	k7c2	kolem
složitého	složitý	k2eAgInSc2d1	složitý
dědického	dědický	k2eAgInSc2d1	dědický
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
projednávaného	projednávaný	k2eAgMnSc2d1	projednávaný
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
před	před	k7c7	před
londýnským	londýnský	k2eAgInSc7d1	londýnský
kancléřským	kancléřský	k2eAgInSc7d1	kancléřský
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
byrokratickou	byrokratický	k2eAgFnSc7d1	byrokratická
a	a	k8xC	a
parazitní	parazitní	k2eAgFnSc7d1	parazitní
právní	právní	k2eAgFnSc7d1	právní
institucí	instituce	k1gFnSc7	instituce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
drtí	drtit	k5eAaImIp3nS	drtit
fyzicky	fyzicky	k6eAd1	fyzicky
i	i	k8xC	i
psychicky	psychicky	k6eAd1	psychicky
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jsou	být	k5eAaImIp3nP	být
vtaženi	vtáhnout	k5eAaPmNgMnP	vtáhnout
do	do	k7c2	do
soukolí	soukolí	k1gNnSc2	soukolí
její	její	k3xOp3gFnSc2	její
absurdní	absurdní	k2eAgFnSc2d1	absurdní
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Zlé	zlý	k2eAgInPc1d1	zlý
časy	čas	k1gInPc1	čas
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
,	,	kIx,	,
Hard	Hard	k1gMnSc1	Hard
Times	Times	k1gMnSc1	Times
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgInSc4d1	sociální
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
autor	autor	k1gMnSc1	autor
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
události	událost	k1gFnPc4	událost
spjaté	spjatý	k2eAgNnSc1d1	spjaté
s	s	k7c7	s
tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
dělnickým	dělnický	k2eAgNnSc7d1	dělnické
hnutím	hnutí	k1gNnSc7	hnutí
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
stanovisko	stanovisko	k1gNnSc4	stanovisko
znepokojeného	znepokojený	k2eAgMnSc2d1	znepokojený
kritika	kritik	k1gMnSc2	kritik
a	a	k8xC	a
sociálního	sociální	k2eAgMnSc2d1	sociální
reformátora	reformátor	k1gMnSc2	reformátor
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
představuje	představovat	k5eAaImIp3nS	představovat
románovou	románový	k2eAgFnSc4d1	románová
studii	studie	k1gFnSc4	studie
rozvoje	rozvoj	k1gInSc2	rozvoj
prumyslu	prumysl	k1gInSc2	prumysl
autorovy	autorův	k2eAgFnSc2d1	autorova
epochy	epocha	k1gFnSc2	epocha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
lidských	lidský	k2eAgFnPc2d1	lidská
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Dorritka	Dorritka	k1gFnSc1	Dorritka
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
Little	Little	k1gFnSc1	Little
Dorrit	Dorrita	k1gFnPc2	Dorrita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgInSc4d1	sociální
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
autor	autor	k1gMnSc1	autor
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chudoba	chudoba	k1gFnSc1	chudoba
i	i	k8xC	i
bohatství	bohatství	k1gNnSc4	bohatství
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
osobnost	osobnost	k1gFnSc4	osobnost
stejně	stejně	k6eAd1	stejně
rozkladný	rozkladný	k2eAgInSc4d1	rozkladný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Poctivý	poctivý	k2eAgMnSc1d1	poctivý
a	a	k8xC	a
čestný	čestný	k2eAgMnSc1d1	čestný
člověk	člověk	k1gMnSc1	člověk
nemůže	moct	k5eNaImIp3nS	moct
v	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
společnosti	společnost	k1gFnSc6	společnost
vyniknout	vyniknout	k5eAaPmF	vyniknout
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
své	svůj	k3xOyFgNnSc4	svůj
nadání	nadání	k1gNnSc4	nadání
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
většinu	většina	k1gFnSc4	většina
energie	energie	k1gFnSc2	energie
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
k	k	k7c3	k
boji	boj	k1gInSc3	boj
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
holé	holý	k2eAgFnSc2d1	holá
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
dvou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
A	a	k9	a
Tale	Tal	k1gInSc2	Tal
of	of	k?	of
Two	Two	k1gMnSc1	Two
Cities	Cities	k1gMnSc1	Cities
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
z	z	k7c2	z
období	období	k1gNnSc2	období
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
uváděno	uvádět	k5eAaImNgNnS	uvádět
jako	jako	k8xS	jako
největší	veliký	k2eAgInSc1d3	veliký
bestseller	bestseller	k1gInSc1	bestseller
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nadějné	nadějný	k2eAgFnPc1d1	nadějná
vyhlídky	vyhlídka	k1gFnPc1	vyhlídka
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
Great	Great	k2eAgInSc1d1	Great
Expectations	Expectations	k1gInSc1	Expectations
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
o	o	k7c6	o
chudém	chudý	k1gMnSc6	chudý
chlapci	chlapec	k1gMnSc6	chlapec
Pipovi	Pipa	k1gMnSc6	Pipa
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
vděčnost	vděčnost	k1gFnSc1	vděčnost
galejníka	galejník	k1gMnSc2	galejník
přinese	přinést	k5eAaPmIp3nS	přinést
velké	velký	k2eAgFnPc4d1	velká
naděje	naděje	k1gFnPc4	naděje
do	do	k7c2	do
života	život	k1gInSc2	život
a	a	k8xC	a
kariéry	kariéra	k1gFnSc2	kariéra
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgNnSc4d1	velké
zklamání	zklamání	k1gNnSc4	zklamání
a	a	k8xC	a
vystřízlivění	vystřízlivění	k1gNnSc4	vystřízlivění
<g/>
.	.	kIx.	.
</s>
<s>
Náš	náš	k3xOp1gMnSc1	náš
vzájemný	vzájemný	k2eAgMnSc1d1	vzájemný
přítel	přítel	k1gMnSc1	přítel
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
Our	Our	k1gMnSc1	Our
Mutual	Mutual	k1gMnSc1	Mutual
Friend	Friend	k1gMnSc1	Friend
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
autorův	autorův	k2eAgInSc4d1	autorův
dokončený	dokončený	k2eAgInSc4d1	dokončený
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Hlídač	hlídač	k1gMnSc1	hlídač
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Signal-Man	Signal-Man	k1gMnSc1	Signal-Man
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
-	-	kIx~	-
horor	horor	k1gInSc1	horor
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Mugby	Mugba	k1gFnSc2	Mugba
Junction	Junction	k1gInSc1	Junction
-	-	kIx~	-
vánočního	vánoční	k2eAgNnSc2d1	vánoční
čísla	číslo	k1gNnSc2	číslo
časopisu	časopis	k1gInSc2	časopis
All	All	k1gFnSc2	All
the	the	k?	the
Year	Year	k1gInSc1	Year
Round	round	k1gInSc1	round
Čarovná	čarovný	k2eAgFnSc1d1	čarovná
rybí	rybí	k2eAgFnSc1d1	rybí
kostička	kostička	k1gFnSc1	kostička
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Magic	Magic	k1gMnSc1	Magic
Fishbone	Fishbon	k1gMnSc5	Fishbon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohádka	pohádka	k1gFnSc1	pohádka
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Holiday	Holidaa	k1gFnSc2	Holidaa
Romance	romance	k1gFnSc2	romance
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
Záhada	záhada	k1gFnSc1	záhada
Edwina	Edwina	k1gFnSc1	Edwina
Drooda	Drooda	k1gFnSc1	Drooda
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Mystery	Myster	k1gInPc1	Myster
of	of	k?	of
Edwin	Edwin	k1gInSc1	Edwin
Drood	Drood	k1gInSc1	Drood
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
román	román	k1gInSc1	román
s	s	k7c7	s
napínavou	napínavý	k2eAgFnSc7d1	napínavá
detektivní	detektivní	k2eAgFnSc7d1	detektivní
zápletkou	zápletka	k1gFnSc7	zápletka
<g/>
.	.	kIx.	.
</s>
