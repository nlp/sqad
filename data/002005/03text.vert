<s>
Fotbal	fotbal	k1gInSc1	fotbal
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
football	footbalnout	k5eAaPmAgMnS	footbalnout
<g/>
,	,	kIx,	,
foot	foot	k1gInSc1	foot
<g/>
=	=	kIx~	=
<g/>
noha	noha	k1gFnSc1	noha
<g/>
,	,	kIx,	,
ball	ball	k1gInSc1	ball
<g/>
=	=	kIx~	=
<g/>
míč	míč	k1gInSc1	míč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
kopaná	kopaná	k1gFnSc1	kopaná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
míčová	míčový	k2eAgFnSc1d1	Míčová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nejpopulárnějším	populární	k2eAgMnSc7d3	nejpopulárnější
kolektivním	kolektivní	k2eAgInSc7d1	kolektivní
sportem	sport	k1gInSc7	sport
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
hrají	hrát	k5eAaImIp3nP	hrát
dvě	dva	k4xCgNnPc1	dva
družstva	družstvo	k1gNnPc1	družstvo
po	po	k7c6	po
jedenácti	jedenáct	k4xCc6	jedenáct
hráčích	hráč	k1gMnPc6	hráč
na	na	k7c6	na
obdélníkovém	obdélníkový	k2eAgMnSc6d1	obdélníkový
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
travnatém	travnatý	k2eAgNnSc6d1	travnaté
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
více	hodně	k6eAd2	hodně
branek	branka	k1gFnPc2	branka
(	(	kIx(	(
<g/>
gólů	gól	k1gInPc2	gól
<g/>
)	)	kIx)	)
než	než	k8xS	než
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Branky	branka	k1gFnSc2	branka
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
míč	míč	k1gInSc1	míč
přejde	přejít	k5eAaPmIp3nS	přejít
brankovou	brankový	k2eAgFnSc4d1	branková
čáru	čára	k1gFnSc4	čára
mezi	mezi	k7c7	mezi
tyčemi	tyč	k1gFnPc7	tyč
branky	branka	k1gFnSc2	branka
celým	celý	k2eAgInSc7d1	celý
objemem	objem	k1gInSc7	objem
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hráči	hráč	k1gMnPc1	hráč
mohou	moct	k5eAaImIp3nP	moct
k	k	k7c3	k
hraní	hraní	k1gNnSc3	hraní
míčem	míč	k1gInSc7	míč
používat	používat	k5eAaImF	používat
libovolné	libovolný	k2eAgFnPc4d1	libovolná
části	část	k1gFnPc4	část
těla	tělo	k1gNnSc2	tělo
kromě	kromě	k7c2	kromě
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
paží	paže	k1gFnPc2	paže
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
brankář	brankář	k1gMnSc1	brankář
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
odlišený	odlišený	k2eAgInSc4d1	odlišený
barvou	barva	k1gFnSc7	barva
dresu	dres	k1gInSc2	dres
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vlastní	vlastní	k2eAgFnSc2d1	vlastní
branky	branka	k1gFnSc2	branka
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
dva	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
o	o	k7c6	o
jedenácti	jedenáct	k4xCc6	jedenáct
hráčích	hráč	k1gMnPc6	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hru	hra	k1gFnSc4	hra
dohlíží	dohlížet	k5eAaImIp3nP	dohlížet
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgMnSc1d1	hlavní
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
hráči	hráč	k1gMnPc7	hráč
na	na	k7c6	na
hrací	hrací	k2eAgFnSc6d1	hrací
ploše	plocha	k1gFnSc6	plocha
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
plnou	plný	k2eAgFnSc4d1	plná
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
zápasu	zápas	k1gInSc2	zápas
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
píšťalkou	píšťalka	k1gFnSc7	píšťalka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
zahájení	zahájení	k1gNnSc1	zahájení
a	a	k8xC	a
přerušení	přerušení	k1gNnSc1	přerušení
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
za	za	k7c7	za
okrajem	okraj	k1gInSc7	okraj
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
na	na	k7c6	na
delších	dlouhý	k2eAgNnPc6d2	delší
okrajích	okrají	k1gNnPc6	okrají
hřiště	hřiště	k1gNnSc2	hřiště
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
dva	dva	k4xCgMnPc1	dva
asistenti	asistent	k1gMnPc1	asistent
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
rozhodčímu	rozhodčí	k1gMnSc3	rozhodčí
praporkem	praporek	k1gInSc7	praporek
signalizují	signalizovat	k5eAaImIp3nP	signalizovat
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
porušení	porušení	k1gNnSc2	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
však	však	k9	však
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
rozhodovací	rozhodovací	k2eAgFnSc4d1	rozhodovací
pravomoc	pravomoc	k1gFnSc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
soutěžích	soutěž	k1gFnPc6	soutěž
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
dva	dva	k4xCgMnPc1	dva
další	další	k2eAgMnPc1d1	další
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
brankoví	brankový	k2eAgMnPc1d1	brankový
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgMnSc1d1	stojící
mimo	mimo	k7c4	mimo
hřiště	hřiště	k1gNnPc4	hřiště
těsně	těsně	k6eAd1	těsně
vedle	vedle	k7c2	vedle
každé	každý	k3xTgFnSc2	každý
branky	branka	k1gFnSc2	branka
<g/>
;	;	kIx,	;
ti	ten	k3xDgMnPc1	ten
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
přímo	přímo	k6eAd1	přímo
nezasahují	zasahovat	k5eNaImIp3nP	zasahovat
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
ani	ani	k8xC	ani
praporek	praporek	k1gInSc4	praporek
<g/>
,	,	kIx,	,
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
poradci	poradce	k1gMnPc1	poradce
hlavního	hlavní	k2eAgMnSc2d1	hlavní
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
spojení	spojení	k1gNnSc4	spojení
minivysílačkou	minivysílačka	k1gFnSc7	minivysílačka
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
kulatým	kulatý	k2eAgInSc7d1	kulatý
míčem	míč	k1gInSc7	míč
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
hráči	hráč	k1gMnPc1	hráč
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
na	na	k7c6	na
hrací	hrací	k2eAgFnSc6d1	hrací
ploše	plocha	k1gFnSc6	plocha
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
driblováním	driblování	k1gNnSc7	driblování
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
běháním	běhání	k1gNnSc7	běhání
s	s	k7c7	s
míčem	míč	k1gInSc7	míč
ovládaným	ovládaný	k2eAgInSc7d1	ovládaný
jemnými	jemný	k2eAgInPc7d1	jemný
kopy	kop	k1gInPc7	kop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přihrávkami	přihrávka	k1gFnPc7	přihrávka
spoluhráčům	spoluhráč	k1gMnPc3	spoluhráč
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
střelbou	střelba	k1gFnSc7	střelba
na	na	k7c4	na
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
branku	branka	k1gFnSc4	branka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
brání	bránit	k5eAaImIp3nS	bránit
protivníkův	protivníkův	k2eAgMnSc1d1	protivníkův
brankář	brankář	k1gMnSc1	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Družstvo	družstvo	k1gNnSc1	družstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zrovna	zrovna	k6eAd1	zrovna
nemá	mít	k5eNaImIp3nS	mít
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
míč	míč	k1gInSc1	míč
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
snaží	snažit	k5eAaImIp3nS	snažit
získat	získat	k5eAaPmF	získat
<g/>
:	:	kIx,	:
zachycením	zachycení	k1gNnSc7	zachycení
soupeřovy	soupeřův	k2eAgFnSc2d1	soupeřova
přihrávky	přihrávka	k1gFnSc2	přihrávka
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
napadáním	napadání	k1gNnPc3	napadání
<g/>
"	"	kIx"	"
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgInSc2	který
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc1	míč
<g/>
;	;	kIx,	;
dovolený	dovolený	k2eAgInSc1d1	dovolený
fyzický	fyzický	k2eAgInSc1d1	fyzický
kontakt	kontakt	k1gInSc1	kontakt
mezi	mezi	k7c7	mezi
hráči	hráč	k1gMnPc7	hráč
je	být	k5eAaImIp3nS	být
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
zápas	zápas	k1gInSc1	zápas
obecně	obecně	k6eAd1	obecně
probíhá	probíhat	k5eAaImIp3nS	probíhat
plynule	plynule	k6eAd1	plynule
<g/>
,	,	kIx,	,
přerušení	přerušení	k1gNnPc1	přerušení
jsou	být	k5eAaImIp3nP	být
způsobena	způsoben	k2eAgNnPc1d1	způsobeno
porušením	porušení	k1gNnSc7	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
když	když	k8xS	když
se	se	k3xPyFc4	se
míč	míč	k1gInSc1	míč
dostane	dostat	k5eAaPmIp3nS	dostat
mimo	mimo	k7c4	mimo
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
když	když	k8xS	když
hráč	hráč	k1gMnSc1	hráč
mimo	mimo	k7c4	mimo
brankáře	brankář	k1gMnPc4	brankář
zahraje	zahrát	k5eAaPmIp3nS	zahrát
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
když	když	k8xS	když
při	při	k7c6	při
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
odebrání	odebrání	k1gNnSc4	odebrání
míče	míč	k1gInSc2	míč
soupeři	soupeř	k1gMnSc3	soupeř
hráč	hráč	k1gMnSc1	hráč
protivníka	protivník	k1gMnSc2	protivník
kopne	kopnout	k5eAaPmIp3nS	kopnout
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
překročí	překročit	k5eAaPmIp3nP	překročit
dovolené	dovolený	k2eAgInPc4d1	dovolený
způsoby	způsob	k1gInPc4	způsob
napadání	napadání	k1gNnSc2	napadání
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
hra	hra	k1gFnSc1	hra
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
většinou	většinou	k6eAd1	většinou
jen	jen	k6eAd1	jen
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
zdržením	zdržení	k1gNnSc7	zdržení
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
devadesát	devadesát	k4xCc4	devadesát
minut	minuta	k1gFnPc2	minuta
hrubého	hrubý	k2eAgInSc2d1	hrubý
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
čas	čas	k1gInSc1	čas
běží	běžet	k5eAaImIp3nS	běžet
i	i	k9	i
v	v	k7c6	v
přerušené	přerušený	k2eAgFnSc6d1	přerušená
hře	hra	k1gFnSc6	hra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
patnáctiminutovou	patnáctiminutový	k2eAgFnSc7d1	patnáctiminutová
přestávkou	přestávka	k1gFnSc7	přestávka
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yRgInPc4	který
si	se	k3xPyFc3	se
soupeři	soupeř	k1gMnPc1	soupeř
vymění	vyměnit	k5eAaPmIp3nP	vyměnit
poloviny	polovina	k1gFnPc4	polovina
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
obou	dva	k4xCgInPc2	dva
poločasů	poločas	k1gInPc2	poločas
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
kolik	kolika	k1gFnPc2	kolika
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
nad	nad	k7c4	nad
rámec	rámec	k1gInSc4	rámec
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
čas	čas	k1gInSc4	čas
ztracený	ztracený	k2eAgInSc4d1	ztracený
např.	např.	kA	např.
ošetřováním	ošetřování	k1gNnSc7	ošetřování
zraněných	zraněný	k2eAgMnPc2d1	zraněný
hráčů	hráč	k1gMnPc2	hráč
či	či	k8xC	či
střídáním	střídání	k1gNnSc7	střídání
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
minutu	minuta	k1gFnSc4	minuta
na	na	k7c6	na
konci	konec	k1gInSc6	konec
prvního	první	k4xOgInSc2	první
poločasu	poločas	k1gInSc2	poločas
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
minuty	minuta	k1gFnPc4	minuta
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhého	druhý	k4xOgInSc2	druhý
poločasu	poločas	k1gInSc2	poločas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhého	druhý	k4xOgInSc2	druhý
poločasu	poločas	k1gInSc2	poločas
je	být	k5eAaImIp3nS	být
vítězem	vítěz	k1gMnSc7	vítěz
zápasu	zápas	k1gInSc2	zápas
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
soupeři	soupeř	k1gMnSc3	soupeř
vstřelilo	vstřelit	k5eAaPmAgNnS	vstřelit
víc	hodně	k6eAd2	hodně
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgNnPc4	dva
družstva	družstvo	k1gNnPc4	družstvo
obdržela	obdržet	k5eAaPmAgFnS	obdržet
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
branek	branka	k1gFnPc2	branka
<g/>
,	,	kIx,	,
končí	končit	k5eAaImIp3nS	končit
zápas	zápas	k1gInSc1	zápas
remízou	remíza	k1gFnSc7	remíza
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
soutěžích	soutěž	k1gFnPc6	soutěž
je	být	k5eAaImIp3nS	být
však	však	k9	však
remíza	remíza	k1gFnSc1	remíza
nepřípustná	přípustný	k2eNgFnSc1d1	nepřípustná
a	a	k8xC	a
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
prodloužením	prodloužení	k1gNnSc7	prodloužení
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ani	ani	k8xC	ani
poté	poté	k6eAd1	poté
není	být	k5eNaImIp3nS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
penaltový	penaltový	k2eAgInSc4d1	penaltový
rozstřel	rozstřel	k1gInSc4	rozstřel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
fotbale	fotbal	k1gInSc6	fotbal
padá	padat	k5eAaImIp3nS	padat
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k4c1	málo
branek	branka	k1gFnPc2	branka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c4	na
MS	MS	kA	MS
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
průměr	průměr	k1gInSc1	průměr
2,3	[number]	k4	2,3
gólu	gól	k1gInSc2	gól
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
hráčů	hráč	k1gMnPc2	hráč
každého	každý	k3xTgNnSc2	každý
družstva	družstvo	k1gNnSc2	družstvo
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
určen	určen	k2eAgMnSc1d1	určen
jako	jako	k8xS	jako
brankář	brankář	k1gMnSc1	brankář
<g/>
:	:	kIx,	:
má	mít	k5eAaImIp3nS	mít
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
barvu	barva	k1gFnSc4	barva
dresu	dres	k1gInSc2	dres
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
ta	ten	k3xDgFnSc1	ten
výjimka	výjimka	k1gFnSc1	výjimka
<g/>
,	,	kIx,	,
že	že	k8xS	že
smí	smět	k5eAaImIp3nS	smět
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vlastní	vlastní	k2eAgFnSc2d1	vlastní
branky	branka	k1gFnSc2	branka
hrát	hrát	k5eAaImF	hrát
míč	míč	k1gInSc4	míč
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pravidel	pravidlo	k1gNnPc2	pravidlo
jsou	být	k5eAaImIp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
hráči	hráč	k1gMnPc1	hráč
stejní	stejný	k2eAgMnPc1d1	stejný
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
však	však	k9	však
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
funkce	funkce	k1gFnPc1	funkce
dané	daný	k2eAgFnPc1d1	daná
konkrétními	konkrétní	k2eAgInPc7d1	konkrétní
schopnostmi	schopnost	k1gFnPc7	schopnost
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
taktickými	taktický	k2eAgInPc7d1	taktický
záměry	záměr	k1gInPc7	záměr
a	a	k8xC	a
úkoly	úkol	k1gInPc7	úkol
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
plní	plnit	k5eAaImIp3nS	plnit
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
brankář	brankář	k1gMnSc1	brankář
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
bránit	bránit	k5eAaImF	bránit
soupeři	soupeř	k1gMnPc1	soupeř
ve	v	k7c6	v
vstřelení	vstřelení	k1gNnSc6	vstřelení
gólu	gól	k1gInSc2	gól
<g/>
;	;	kIx,	;
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
proto	proto	k8xC	proto
setrvává	setrvávat	k5eAaImIp3nS	setrvávat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vlastní	vlastní	k2eAgFnSc2d1	vlastní
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
podobný	podobný	k2eAgInSc4d1	podobný
cíl	cíl	k1gInSc4	cíl
mají	mít	k5eAaImIp3nP	mít
hráči	hráč	k1gMnPc1	hráč
označovaní	označovaný	k2eAgMnPc1d1	označovaný
jako	jako	k8xC	jako
obránci	obránce	k1gMnPc1	obránce
(	(	kIx(	(
<g/>
slangově	slangově	k6eAd1	slangově
"	"	kIx"	"
<g/>
beci	bek	k1gMnPc1	bek
<g/>
"	"	kIx"	"
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
back	back	k6eAd1	back
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
polovině	polovina	k1gFnSc6	polovina
hřiště	hřiště	k1gNnSc2	hřiště
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
pokutovém	pokutový	k2eAgNnSc6d1	pokutové
území	území	k1gNnSc6	území
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zejména	zejména	k9	zejména
zastavit	zastavit	k5eAaPmF	zastavit
přicházející	přicházející	k2eAgInSc4d1	přicházející
soupeřův	soupeřův	k2eAgInSc4d1	soupeřův
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
pojetí	pojetí	k1gNnSc6	pojetí
hry	hra	k1gFnSc2	hra
však	však	k9	však
zejména	zejména	k9	zejména
krajní	krajní	k2eAgMnPc1d1	krajní
obránci	obránce	k1gMnPc1	obránce
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
i	i	k9	i
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
<g/>
.	.	kIx.	.
</s>
<s>
Záložníci	záložník	k1gMnPc1	záložník
tvoří	tvořit	k5eAaImIp3nP	tvořit
středovou	středový	k2eAgFnSc4d1	středová
část	část	k1gFnSc4	část
družstva	družstvo	k1gNnSc2	družstvo
<g/>
:	:	kIx,	:
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
ploše	plocha	k1gFnSc6	plocha
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
se	se	k3xPyFc4	se
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
do	do	k7c2	do
obrany	obrana	k1gFnSc2	obrana
i	i	k8xC	i
útoku	útok	k1gInSc2	útok
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
specializace	specializace	k1gFnSc1	specializace
na	na	k7c4	na
defenzivní	defenzivní	k2eAgInPc4d1	defenzivní
a	a	k8xC	a
ofenzivní	ofenzivní	k2eAgMnPc4d1	ofenzivní
záložníky	záložník	k1gMnPc4	záložník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
středoví	středový	k2eAgMnPc1d1	středový
záložníci	záložník	k1gMnPc1	záložník
mívají	mívat	k5eAaImIp3nP	mívat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
"	"	kIx"	"
<g/>
tvořit	tvořit	k5eAaImF	tvořit
hru	hra	k1gFnSc4	hra
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
rozehrávat	rozehrávat	k5eAaImF	rozehrávat
útočné	útočný	k2eAgFnPc4d1	útočná
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
přihrávkami	přihrávka	k1gFnPc7	přihrávka
míče	míč	k1gInSc2	míč
spoluhráčům	spoluhráč	k1gMnPc3	spoluhráč
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
záložníky	záložník	k1gMnPc7	záložník
proto	proto	k8xC	proto
bývají	bývat	k5eAaImIp3nP	bývat
technicky	technicky	k6eAd1	technicky
velmi	velmi	k6eAd1	velmi
schopní	schopný	k2eAgMnPc1d1	schopný
hráči	hráč	k1gMnPc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Útočníci	útočník	k1gMnPc1	útočník
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zakončovat	zakončovat	k5eAaImF	zakončovat
útočné	útočný	k2eAgFnPc4d1	útočná
akce	akce	k1gFnPc4	akce
a	a	k8xC	a
střílet	střílet	k5eAaImF	střílet
soupeři	soupeř	k1gMnSc3	soupeř
góly	gól	k1gInPc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
Pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nejdále	daleko	k6eAd3	daleko
od	od	k7c2	od
vlastní	vlastní	k2eAgFnSc2d1	vlastní
branky	branka	k1gFnSc2	branka
a	a	k8xC	a
do	do	k7c2	do
obrany	obrana	k1gFnSc2	obrana
se	se	k3xPyFc4	se
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
jen	jen	k9	jen
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
hrají	hrát	k5eAaImIp3nP	hrát
hráči	hráč	k1gMnPc1	hráč
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
rozestavení	rozestavení	k1gNnSc6	rozestavení
4-4-2	[number]	k4	4-4-2
(	(	kIx(	(
<g/>
4	[number]	k4	4
obránci	obránce	k1gMnPc7	obránce
<g/>
,	,	kIx,	,
4	[number]	k4	4
záložníci	záložník	k1gMnPc1	záložník
a	a	k8xC	a
2	[number]	k4	2
útočníci	útočník	k1gMnPc1	útočník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
však	však	k9	však
hrávalo	hrávat	k5eAaImAgNnS	hrávat
v	v	k7c6	v
rozestaveních	rozestavení	k1gNnPc6	rozestavení
3-2-3-2	[number]	k4	3-2-3-2
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4-2-4	[number]	k4	4-2-4
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3-2-2-3	[number]	k4	3-2-2-3
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
<g/>
)	)	kIx)	)
i	i	k9	i
1-2-7	[number]	k4	1-2-7
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
posty	post	k1gInPc4	post
a	a	k8xC	a
stanovení	stanovení	k1gNnSc4	stanovení
jejich	jejich	k3xOp3gInPc2	jejich
úkolů	úkol	k1gInPc2	úkol
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
herní	herní	k2eAgFnSc2d1	herní
taktiky	taktika	k1gFnSc2	taktika
je	být	k5eAaImIp3nS	být
zodpovědností	zodpovědnost	k1gFnSc7	zodpovědnost
trenéra	trenér	k1gMnSc2	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Trenéři	trenér	k1gMnPc1	trenér
obou	dva	k4xCgFnPc2	dva
týmů	tým	k1gInPc2	tým
při	při	k7c6	při
zápase	zápas	k1gInSc6	zápas
pobývají	pobývat	k5eAaImIp3nP	pobývat
nedaleko	nedaleko	k7c2	nedaleko
okraje	okraj	k1gInSc2	okraj
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
středu	střed	k1gInSc2	střed
jednoho	jeden	k4xCgInSc2	jeden
delšího	dlouhý	k2eAgInSc2d2	delší
okraje	okraj	k1gInSc2	okraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
udílejí	udílet	k5eAaImIp3nP	udílet
rady	rada	k1gFnPc1	rada
a	a	k8xC	a
pokyny	pokyn	k1gInPc1	pokyn
hráčům	hráč	k1gMnPc3	hráč
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
a	a	k8xC	a
případně	případně	k6eAd1	případně
vyměňují	vyměňovat	k5eAaImIp3nP	vyměňovat
hráče	hráč	k1gMnPc4	hráč
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
za	za	k7c4	za
náhradníky	náhradník	k1gMnPc4	náhradník
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
lavičkami	lavička	k1gFnPc7	lavička
náhradníků	náhradník	k1gMnPc2	náhradník
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
střídání	střídání	k1gNnSc1	střídání
<g/>
,	,	kIx,	,
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
dodržování	dodržování	k1gNnSc1	dodržování
pravidel	pravidlo	k1gNnPc2	pravidlo
osobami	osoba	k1gFnPc7	osoba
na	na	k7c6	na
lavičkách	lavička	k1gFnPc6	lavička
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případech	případ	k1gInPc6	případ
zranění	zranění	k1gNnSc2	zranění
některého	některý	k3yIgMnSc2	některý
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
může	moct	k5eAaImIp3nS	moct
zraněného	zraněný	k2eAgMnSc4d1	zraněný
rozhodčího	rozhodčí	k1gMnSc4	rozhodčí
vystřídat	vystřídat	k5eAaPmF	vystřídat
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
hry	hra	k1gFnSc2	hra
podobné	podobný	k2eAgFnSc2d1	podobná
fotbalu	fotbal	k1gInSc2	fotbal
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
až	až	k6eAd1	až
do	do	k7c2	do
období	období	k1gNnSc2	období
starověku	starověk	k1gInSc2	starověk
<g/>
:	:	kIx,	:
nejstarší	starý	k2eAgMnSc1d3	nejstarší
takovou	takový	k3xDgFnSc7	takový
hrou	hra	k1gFnSc7	hra
bylo	být	k5eAaImAgNnS	být
čínské	čínský	k2eAgNnSc1d1	čínské
cchu-ťü	cchu-ťü	k?	cchu-ťü
<g/>
,	,	kIx,	,
hrané	hraný	k2eAgFnSc6d1	hraná
již	již	k6eAd1	již
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
mnoho	mnoho	k4c1	mnoho
míčových	míčový	k2eAgFnPc2d1	Míčová
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
vzdálených	vzdálený	k2eAgMnPc2d1	vzdálený
předchůdců	předchůdce	k1gMnPc2	předchůdce
fotbalu	fotbal	k1gInSc2	fotbal
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
např.	např.	kA	např.
římská	římský	k2eAgFnSc1d1	římská
hra	hra	k1gFnSc1	hra
harpastum	harpastum	k1gNnSc4	harpastum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
různé	různý	k2eAgFnPc1d1	různá
hry	hra	k1gFnPc1	hra
podobné	podobný	k2eAgFnPc1d1	podobná
fotbalu	fotbal	k1gInSc2	fotbal
hrály	hrát	k5eAaImAgFnP	hrát
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc1	jejich
pravidla	pravidlo	k1gNnPc1	pravidlo
se	se	k3xPyFc4	se
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
lišila	lišit	k5eAaImAgFnS	lišit
místo	místo	k1gNnSc4	místo
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
i	i	k9	i
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
dnešního	dnešní	k2eAgInSc2d1	dnešní
fotbalu	fotbal	k1gInSc2	fotbal
má	mít	k5eAaImIp3nS	mít
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tamní	tamní	k2eAgFnPc1d1	tamní
soukromé	soukromý	k2eAgFnPc1d1	soukromá
střední	střední	k2eAgFnPc1d1	střední
školy	škola	k1gFnPc1	škola
(	(	kIx(	(
<g/>
public	publicum	k1gNnPc2	publicum
schools	schoolsa	k1gFnPc2	schoolsa
<g/>
)	)	kIx)	)
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začaly	začít	k5eAaPmAgFnP	začít
žáky	žák	k1gMnPc7	žák
nutit	nutit	k5eAaImF	nutit
ke	k	k7c3	k
sportu	sport	k1gInSc3	sport
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
ze	z	k7c2	z
škol	škola	k1gFnPc2	škola
však	však	k9	však
používala	používat	k5eAaImAgNnP	používat
vlastní	vlastní	k2eAgNnPc1d1	vlastní
pravidla	pravidlo	k1gNnPc1	pravidlo
odrážející	odrážející	k2eAgFnSc2d1	odrážející
místní	místní	k2eAgFnSc2d1	místní
podmínky	podmínka	k1gFnSc2	podmínka
(	(	kIx(	(
<g/>
velikost	velikost	k1gFnSc1	velikost
hřiště	hřiště	k1gNnSc2	hřiště
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravidla	pravidlo	k1gNnPc1	pravidlo
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
postupně	postupně	k6eAd1	postupně
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
vykrystalizovaly	vykrystalizovat	k5eAaPmAgInP	vykrystalizovat
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
formy	forma	k1gFnPc4	forma
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
:	:	kIx,	:
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
verzi	verze	k1gFnSc6	verze
pravidel	pravidlo	k1gNnPc2	pravidlo
(	(	kIx(	(
<g/>
používané	používaný	k2eAgFnPc4d1	používaná
např.	např.	kA	např.
v	v	k7c6	v
Rugby	rugby	k1gNnSc6	rugby
<g/>
,	,	kIx,	,
Marlborough	Marlborougha	k1gFnPc2	Marlborougha
či	či	k8xC	či
Cheltenhamu	Cheltenham	k1gInSc2	Cheltenham
<g/>
)	)	kIx)	)
hráči	hráč	k1gMnSc3	hráč
míč	míč	k1gInSc1	míč
po	po	k7c6	po
hřišti	hřiště	k1gNnSc6	hřiště
přenášeli	přenášet	k5eAaImAgMnP	přenášet
rukama	ruka	k1gFnPc7	ruka
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
verzi	verze	k1gFnSc6	verze
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
Etonu	Etono	k1gNnSc6	Etono
<g/>
,	,	kIx,	,
Harrow	Harrow	k1gFnSc6	Harrow
<g/>
,	,	kIx,	,
Westminsteru	Westminster	k1gInSc6	Westminster
či	či	k8xC	či
Charterhouse	Charterhous	k1gInSc6	Charterhous
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
upřednostňovalo	upřednostňovat	k5eAaImAgNnS	upřednostňovat
kopání	kopání	k1gNnSc1	kopání
do	do	k7c2	do
míče	míč	k1gInSc2	míč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Anglii	Anglie	k1gFnSc6	Anglie
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
boom	boom	k1gInSc4	boom
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
technologický	technologický	k2eAgInSc1d1	technologický
rozvoj	rozvoj	k1gInSc1	rozvoj
umožnil	umožnit	k5eAaPmAgInS	umožnit
pořádání	pořádání	k1gNnSc4	pořádání
soutěžních	soutěžní	k2eAgInPc2d1	soutěžní
zápasů	zápas	k1gInPc2	zápas
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
školami	škola	k1gFnPc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zatímco	zatímco	k8xS	zatímco
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
místních	místní	k2eAgNnPc6d1	místní
pravidlech	pravidlo	k1gNnPc6	pravidlo
atletiky	atletika	k1gFnSc2	atletika
byly	být	k5eAaImAgInP	být
nevýznamné	významný	k2eNgInPc1d1	nevýznamný
<g/>
,	,	kIx,	,
meziškolní	meziškolní	k2eAgInPc1d1	meziškolní
zápasy	zápas	k1gInPc1	zápas
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
byly	být	k5eAaImAgInP	být
kvůli	kvůli	k7c3	kvůli
zásadním	zásadní	k2eAgInPc3d1	zásadní
rozdílům	rozdíl	k1gInPc3	rozdíl
v	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
hry	hra	k1gFnSc2	hra
prakticky	prakticky	k6eAd1	prakticky
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
sešlo	sejít	k5eAaPmAgNnS	sejít
14	[number]	k4	14
zástupců	zástupce	k1gMnPc2	zástupce
škol	škola	k1gFnPc2	škola
na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
ucelená	ucelený	k2eAgFnSc1d1	ucelená
sada	sada	k1gFnSc1	sada
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Cambridgeská	cambridgeský	k2eAgNnPc4d1	cambridgeský
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
upřednostňovala	upřednostňovat	k5eAaImAgNnP	upřednostňovat
kopání	kopání	k1gNnPc1	kopání
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
také	také	k6eAd1	také
čisté	čistý	k2eAgNnSc1d1	čisté
zachycení	zachycení	k1gNnSc1	zachycení
míče	míč	k1gInSc2	míč
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocující	sjednocující	k2eAgFnPc1d1	sjednocující
tendence	tendence	k1gFnPc1	tendence
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c6	v
založení	založení	k1gNnSc6	založení
The	The	k1gFnSc2	The
Football	Footballa	k1gFnPc2	Footballa
Association	Association	k1gInSc1	Association
<g/>
,	,	kIx,	,
prvního	první	k4xOgNnSc2	první
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
fotbalového	fotbalový	k2eAgNnSc2d1	fotbalové
sdružení	sdružení	k1gNnSc2	sdružení
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
setkáních	setkání	k1gNnPc6	setkání
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
asociace	asociace	k1gFnSc1	asociace
sadu	sad	k1gInSc2	sad
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nakonec	nakonec	k6eAd1	nakonec
vypustila	vypustit	k5eAaPmAgFnS	vypustit
pravidla	pravidlo	k1gNnPc4	pravidlo
dovolující	dovolující	k2eAgInSc4d1	dovolující
běh	běh	k1gInSc4	běh
s	s	k7c7	s
míčem	míč	k1gInSc7	míč
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
a	a	k8xC	a
držení	držení	k1gNnSc6	držení
a	a	k8xC	a
podrážení	podrážení	k1gNnSc6	podrážení
protivníka	protivník	k1gMnSc2	protivník
s	s	k7c7	s
míčem	míč	k1gInSc7	míč
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
zástupce	zástupce	k1gMnSc1	zástupce
Blackheathu	Blackheatha	k1gFnSc4	Blackheatha
asociaci	asociace	k1gFnSc4	asociace
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgInPc7d1	další
kluby	klub	k1gInPc7	klub
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
založil	založit	k5eAaPmAgMnS	založit
Rugby	rugby	k1gNnSc4	rugby
Football	Football	k1gInSc1	Football
Union	union	k1gInSc4	union
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ragby	ragby	k1gNnSc1	ragby
jako	jako	k8xS	jako
sport	sport	k1gInSc1	sport
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
kopané	kopaná	k1gFnSc2	kopaná
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
pravidlech	pravidlo	k1gNnPc6	pravidlo
FA	fa	kA	fa
však	však	k9	však
byla	být	k5eAaImAgFnS	být
některá	některý	k3yIgNnPc4	některý
ustanovení	ustanovení	k1gNnPc4	ustanovení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
dnes	dnes	k6eAd1	dnes
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jen	jen	k9	jen
ve	v	k7c6	v
sportech	sport	k1gInPc6	sport
jako	jako	k8xS	jako
ragby	ragby	k1gNnSc1	ragby
či	či	k8xC	či
americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
pravidla	pravidlo	k1gNnPc4	pravidlo
také	také	k9	také
například	například	k6eAd1	například
ani	ani	k8xC	ani
nestanovovala	stanovovat	k5eNaImAgFnS	stanovovat
počet	počet	k1gInSc4	počet
hráčů	hráč	k1gMnPc2	hráč
či	či	k8xC	či
tvar	tvar	k1gInSc1	tvar
míče	míč	k1gInSc2	míč
<g/>
;	;	kIx,	;
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
soupeři	soupeř	k1gMnPc1	soupeř
dohodnout	dohodnout	k5eAaPmF	dohodnout
před	před	k7c7	před
každým	každý	k3xTgInSc7	každý
zápasem	zápas	k1gInSc7	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
několik	několik	k4yIc1	několik
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přispívaly	přispívat	k5eAaImAgFnP	přispívat
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
jednotných	jednotný	k2eAgNnPc2d1	jednotné
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
zápas	zápas	k1gInSc1	zápas
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Skotskem	Skotsko	k1gNnSc7	Skotsko
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1872	[number]	k4	1872
a	a	k8xC	a
skončil	skončit	k5eAaPmAgInS	skončit
bezbrankovou	bezbrankový	k2eAgFnSc7d1	bezbranková
remízou	remíza	k1gFnSc7	remíza
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
mimoevropské	mimoevropský	k2eAgNnSc4d1	mimoevropské
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
fotbalové	fotbalový	k2eAgNnSc4d1	fotbalové
utkání	utkání	k1gNnSc4	utkání
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1885	[number]	k4	1885
v	v	k7c6	v
Newarku	Newark	k1gInSc6	Newark
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Fotbal	fotbal	k1gInSc1	fotbal
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
na	na	k7c4	na
kontinent	kontinent	k1gInSc4	kontinent
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgNnSc7d3	nejstarší
fotbalovým	fotbalový	k2eAgNnSc7d1	fotbalové
mužstvem	mužstvo	k1gNnSc7	mužstvo
patrně	patrně	k6eAd1	patrně
švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
Lausanne	Lausanne	k1gNnSc1	Lausanne
Football	Football	k1gMnSc1	Football
and	and	k?	and
Cricket	Cricket	k1gInSc1	Cricket
Club	club	k1gInSc1	club
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
anglickým	anglický	k2eAgMnPc3d1	anglický
dělníkům	dělník	k1gMnPc3	dělník
pracujícím	pracující	k2eAgMnPc3d1	pracující
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
železnice	železnice	k1gFnSc2	železnice
se	se	k3xPyFc4	se
fotbal	fotbal	k1gInSc1	fotbal
dostal	dostat	k5eAaPmAgInS	dostat
až	až	k9	až
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
rozšíření	rozšíření	k1gNnSc2	rozšíření
fotbalu	fotbal	k1gInSc2	fotbal
na	na	k7c4	na
jihoamerický	jihoamerický	k2eAgInSc4d1	jihoamerický
kontinent	kontinent	k1gInSc4	kontinent
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
doložena	doložit	k5eAaPmNgFnS	doložit
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
fotbal	fotbal	k1gInSc4	fotbal
hrají	hrát	k5eAaImIp3nP	hrát
profesionální	profesionální	k2eAgMnPc1d1	profesionální
fotbalisté	fotbalista	k1gMnPc1	fotbalista
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
věnuje	věnovat	k5eAaPmIp3nS	věnovat
na	na	k7c6	na
amatérské	amatérský	k2eAgFnSc6d1	amatérská
či	či	k8xC	či
rekreační	rekreační	k2eAgFnSc6d1	rekreační
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
,	,	kIx,	,
uspořádaného	uspořádaný	k2eAgInSc2d1	uspořádaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
federací	federace	k1gFnSc7	federace
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
pravidelně	pravidelně	k6eAd1	pravidelně
fotbal	fotbal	k1gInSc1	fotbal
nejméně	málo	k6eAd3	málo
240	[number]	k4	240
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
příčinami	příčina	k1gFnPc7	příčina
jeho	jeho	k3xOp3gFnSc2	jeho
popularity	popularita	k1gFnSc2	popularita
jsou	být	k5eAaImIp3nP	být
bezesporu	bezesporu	k9	bezesporu
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
pravidla	pravidlo	k1gNnPc1	pravidlo
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
minimální	minimální	k2eAgFnSc4d1	minimální
náročnost	náročnost	k1gFnSc4	náročnost
na	na	k7c4	na
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnPc1d1	oficiální
pravidla	pravidlo	k1gNnPc1	pravidlo
fotbalu	fotbal	k1gInSc2	fotbal
(	(	kIx(	(
<g/>
Laws	Laws	k1gInSc1	Laws
of	of	k?	of
the	the	k?	the
Game	game	k1gInSc1	game
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
soutěžních	soutěžní	k2eAgNnPc6d1	soutěžní
utkáních	utkání	k1gNnPc6	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
sedmnácti	sedmnáct	k4xCc2	sedmnáct
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Fotbal	fotbal	k1gInSc1	fotbal
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
hrací	hrací	k2eAgFnSc6d1	hrací
ploše	plocha	k1gFnSc6	plocha
tvaru	tvar	k1gInSc2	tvar
obdélníku	obdélník	k1gInSc2	obdélník
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
120	[number]	k4	120
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
m	m	kA	m
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
utkání	utkání	k1gNnSc4	utkání
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
110	[number]	k4	110
×	×	k?	×
64	[number]	k4	64
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrací	hrací	k2eAgFnSc1d1	hrací
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
bílými	bílý	k2eAgFnPc7d1	bílá
<g/>
)	)	kIx)	)
čárami	čára	k1gFnPc7	čára
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
šířka	šířka	k1gFnSc1	šířka
čáry	čára	k1gFnSc2	čára
12	[number]	k4	12
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
pomezní	pomezní	k2eAgFnPc1d1	pomezní
čáry	čára	k1gFnPc1	čára
vymezují	vymezovat	k5eAaImIp3nP	vymezovat
delší	dlouhý	k2eAgFnSc4d2	delší
hranu	hrana	k1gFnSc4	hrana
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgInPc1d2	kratší
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
brankové	brankový	k2eAgFnPc4d1	branková
čáry	čára	k1gFnPc4	čára
<g/>
.	.	kIx.	.
</s>
<s>
Hrací	hrací	k2eAgFnSc1d1	hrací
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
poloviny	polovina	k1gFnSc2	polovina
středovou	středový	k2eAgFnSc7d1	středová
čárou	čára	k1gFnSc7	čára
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
středová	středový	k2eAgFnSc1d1	středová
značka	značka	k1gFnSc1	značka
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
vyznačen	vyznačen	k2eAgInSc1d1	vyznačen
středový	středový	k2eAgInSc1d1	středový
kruh	kruh	k1gInSc1	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
polovině	polovina	k1gFnSc6	polovina
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
u	u	k7c2	u
příslušné	příslušný	k2eAgFnSc2d1	příslušná
branky	branka	k1gFnSc2	branka
vyznačena	vyznačen	k2eAgNnPc4d1	vyznačeno
pokutová	pokutový	k2eAgNnPc4d1	pokutové
území	území	k1gNnPc4	území
a	a	k8xC	a
menší	malý	k2eAgFnPc4d2	menší
branková	brankový	k2eAgNnPc4d1	brankové
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
každého	každý	k3xTgNnSc2	každý
pokutového	pokutový	k2eAgNnSc2d1	pokutové
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
pokutová	pokutový	k2eAgFnSc1d1	Pokutová
značka	značka	k1gFnSc1	značka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
vně	vně	k7c2	vně
pokutového	pokutový	k2eAgNnSc2d1	pokutové
území	území	k1gNnSc2	území
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
část	část	k1gFnSc1	část
kruhového	kruhový	k2eAgInSc2d1	kruhový
oblouku	oblouk	k1gInSc2	oblouk
se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
pokutové	pokutový	k2eAgFnSc6d1	Pokutová
značce	značka	k1gFnSc6	značka
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
9,15	[number]	k4	9,15
m	m	kA	m
(	(	kIx(	(
<g/>
10	[number]	k4	10
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
rohu	roh	k1gInSc6	roh
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ohebné	ohebný	k2eAgFnSc6d1	ohebná
tyči	tyč	k1gFnSc6	tyč
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
nejméně	málo	k6eAd3	málo
1,5	[number]	k4	1,5
m	m	kA	m
umístěn	umístěn	k2eAgInSc4d1	umístěn
praporek	praporek	k1gInSc4	praporek
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgInPc1d1	stejný
praporky	praporek	k1gInPc1	praporek
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
i	i	k9	i
uprostřed	uprostřed	k7c2	uprostřed
obou	dva	k4xCgFnPc2	dva
pomezních	pomezní	k2eAgFnPc2d1	pomezní
čar	čára	k1gFnPc2	čára
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
každého	každý	k3xTgInSc2	každý
rohového	rohový	k2eAgInSc2d1	rohový
praporku	praporek	k1gInSc2	praporek
se	se	k3xPyFc4	se
na	na	k7c6	na
hrací	hrací	k2eAgFnSc6d1	hrací
ploše	plocha	k1gFnSc6	plocha
vyznačí	vyznačit	k5eAaPmIp3nS	vyznačit
čtvrtkruh	čtvrtkruh	k1gInSc4	čtvrtkruh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
brankových	brankový	k2eAgFnPc6d1	branková
čárách	čára	k1gFnPc6	čára
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
také	také	k6eAd1	také
krátkými	krátký	k2eAgFnPc7d1	krátká
značkami	značka	k1gFnPc7	značka
vyznačí	vyznačit	k5eAaPmIp3nS	vyznačit
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
9,15	[number]	k4	9,15
m	m	kA	m
od	od	k7c2	od
rohového	rohový	k2eAgInSc2d1	rohový
praporku	praporek	k1gInSc2	praporek
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
obou	dva	k4xCgFnPc2	dva
brankových	brankový	k2eAgFnPc2d1	branková
čar	čára	k1gFnPc2	čára
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgFnPc1d1	umístěna
branky	branka	k1gFnPc1	branka
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgFnPc1d1	tvořená
dvěma	dva	k4xCgFnPc7	dva
svislými	svislý	k2eAgFnPc7d1	svislá
brankovými	brankový	k2eAgFnPc7d1	branková
tyčemi	tyč	k1gFnPc7	tyč
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nahoře	nahoře	k6eAd1	nahoře
spojeny	spojit	k5eAaPmNgInP	spojit
břevnem	břevno	k1gNnSc7	břevno
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyče	tyč	k1gFnPc1	tyč
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
šířku	šířka	k1gFnSc4	šířka
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
12	[number]	k4	12
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
šířka	šířka	k1gFnSc1	šířka
brankové	brankový	k2eAgFnSc2d1	branková
čáry	čára	k1gFnSc2	čára
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
rozměry	rozměr	k1gInPc1	rozměr
branky	branka	k1gFnSc2	branka
činí	činit	k5eAaImIp3nS	činit
7,32	[number]	k4	7,32
<g/>
×	×	k?	×
<g/>
2,44	[number]	k4	2,44
m	m	kA	m
(	(	kIx(	(
<g/>
8	[number]	k4	8
yardů	yard	k1gInPc2	yard
<g/>
×	×	k?	×
<g/>
8	[number]	k4	8
stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
brankové	brankový	k2eAgFnSc6d1	branková
konstrukci	konstrukce	k1gFnSc6	konstrukce
je	být	k5eAaImIp3nS	být
připevněna	připevněn	k2eAgFnSc1d1	připevněna
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
upevněna	upevnit	k5eAaPmNgFnS	upevnit
k	k	k7c3	k
tyčím	tyčit	k5eAaImIp1nS	tyčit
<g/>
,	,	kIx,	,
břevnům	břevno	k1gNnPc3	břevno
a	a	k8xC	a
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
míč	míč	k1gInSc1	míč
nemohl	moct	k5eNaImAgInS	moct
projít	projít	k5eAaPmF	projít
skrz	skrz	k6eAd1	skrz
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
vypnuta	vypnout	k5eAaPmNgFnS	vypnout
dozadu	dozadu	k6eAd1	dozadu
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neomezovala	omezovat	k5eNaImAgFnS	omezovat
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
brankáře	brankář	k1gMnSc2	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
kulatým	kulatý	k2eAgInSc7d1	kulatý
míčem	míč	k1gInSc7	míč
o	o	k7c6	o
obvodu	obvod	k1gInSc6	obvod
mezi	mezi	k7c7	mezi
68	[number]	k4	68
a	a	k8xC	a
70	[number]	k4	70
cm	cm	kA	cm
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
cca	cca	kA	cca
22	[number]	k4	22
cm	cm	kA	cm
<g/>
)	)	kIx)	)
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
mezi	mezi	k7c7	mezi
410	[number]	k4	410
a	a	k8xC	a
450	[number]	k4	450
g	g	kA	g
(	(	kIx(	(
<g/>
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
utkání	utkání	k1gNnSc2	utkání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zápasech	zápas	k1gInPc6	zápas
na	na	k7c6	na
profesionální	profesionální	k2eAgFnSc6d1	profesionální
úrovni	úroveň	k1gFnSc6	úroveň
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
připraveno	připravit	k5eAaPmNgNnS	připravit
více	hodně	k6eAd2	hodně
míčů	míč	k1gInPc2	míč
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zamezuje	zamezovat	k5eAaImIp3nS	zamezovat
zdržování	zdržování	k1gNnSc3	zdržování
hry	hra	k1gFnSc2	hra
způsobeném	způsobený	k2eAgInSc6d1	způsobený
např.	např.	kA	např.
hledáním	hledání	k1gNnSc7	hledání
zakopnutého	zakopnutý	k2eAgInSc2d1	zakopnutý
míče	míč	k1gInSc2	míč
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
prodá	prodat	k5eAaPmIp3nS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
míčů	míč	k1gInPc2	míč
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnPc1	utkání
hrají	hrát	k5eAaImIp3nP	hrát
dvě	dva	k4xCgNnPc1	dva
družstva	družstvo	k1gNnPc1	družstvo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každé	každý	k3xTgNnSc1	každý
má	mít	k5eAaImIp3nS	mít
nejvýše	nejvýše	k6eAd1	nejvýše
jedenáct	jedenáct	k4xCc1	jedenáct
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
hráč	hráč	k1gMnSc1	hráč
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k9	jako
brankář	brankář	k1gMnSc1	brankář
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zápasu	zápas	k1gInSc3	zápas
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nominovat	nominovat	k5eAaBmF	nominovat
také	také	k9	také
náhradníky	náhradník	k1gMnPc4	náhradník
(	(	kIx(	(
<g/>
v	v	k7c6	v
soutěžních	soutěžní	k2eAgNnPc6d1	soutěžní
mezinárodních	mezinárodní	k2eAgNnPc6d1	mezinárodní
utkáních	utkání	k1gNnPc6	utkání
nejvýše	nejvýše	k6eAd1	nejvýše
šest	šest	k4xCc4	šest
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
pravidlech	pravidlo	k1gNnPc6	pravidlo
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
soutěže	soutěž	k1gFnSc2	soutěž
či	či	k8xC	či
předzápasové	předzápasový	k2eAgFnSc3d1	předzápasová
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
nasazeni	nasadit	k5eAaPmNgMnP	nasadit
nejvýše	vysoce	k6eAd3	vysoce
tři	tři	k4xCgMnPc1	tři
<g/>
.	.	kIx.	.
</s>
<s>
Střídání	střídání	k1gNnSc1	střídání
hráčů	hráč	k1gMnPc2	hráč
probíhá	probíhat	k5eAaImIp3nS	probíhat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
přerušené	přerušený	k2eAgFnSc6d1	přerušená
hře	hra	k1gFnSc6	hra
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
vystřídaný	vystřídaný	k2eAgMnSc1d1	vystřídaný
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
v	v	k7c6	v
témže	týž	k3xTgNnSc6	týž
utkání	utkání	k1gNnSc6	utkání
už	už	k9	už
nesmí	smět	k5eNaImIp3nS	smět
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
libovolném	libovolný	k2eAgNnSc6d1	libovolné
přerušení	přerušení	k1gNnSc6	přerušení
hry	hra	k1gFnSc2	hra
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
libovolný	libovolný	k2eAgMnSc1d1	libovolný
hráč	hráč	k1gMnSc1	hráč
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
vyměnit	vyměnit	k5eAaPmF	vyměnit
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
brankářem	brankář	k1gMnSc7	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
klesne	klesnout	k5eAaPmIp3nS	klesnout
z	z	k7c2	z
libovolných	libovolný	k2eAgInPc2d1	libovolný
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
vyloučení	vyloučení	k1gNnSc1	vyloučení
<g/>
,	,	kIx,	,
zranění	zranění	k1gNnSc1	zranění
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
počet	počet	k1gInSc1	počet
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
mužstvu	mužstvo	k1gNnSc6	mužstvo
pod	pod	k7c4	pod
sedm	sedm	k4xCc4	sedm
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
utkání	utkání	k1gNnSc1	utkání
předčasně	předčasně	k6eAd1	předčasně
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
jsou	být	k5eAaImIp3nP	být
povinni	povinen	k2eAgMnPc1d1	povinen
mít	mít	k5eAaImF	mít
následující	následující	k2eAgFnSc4d1	následující
výstroj	výstroj	k1gFnSc4	výstroj
<g/>
:	:	kIx,	:
dres	dres	k1gInSc4	dres
nebo	nebo	k8xC	nebo
tričko	tričko	k1gNnSc4	tričko
-	-	kIx~	-
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
v	v	k7c6	v
jakém	jaký	k3yIgInSc6	jaký
týmu	tým	k1gInSc6	tým
je	být	k5eAaImIp3nS	být
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
dresů	dres	k1gInPc2	dres
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
podle	podle	k7c2	podle
znaku	znak	k1gInSc2	znak
týmu	tým	k1gInSc2	tým
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
náhodně	náhodně	k6eAd1	náhodně
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
týmů	tým	k1gInPc2	tým
má	mít	k5eAaImIp3nS	mít
alespoň	alespoň	k9	alespoň
dvě	dva	k4xCgFnPc4	dva
sady	sada	k1gFnPc4	sada
dresů	dres	k1gInPc2	dres
<g/>
.	.	kIx.	.
trenýrky	trenýrky	k1gFnPc1	trenýrky
štulpny	štulpna	k1gFnPc1	štulpna
(	(	kIx(	(
<g/>
podkolenky	podkolenka	k1gFnPc1	podkolenka
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zakrývají	zakrývat	k5eAaImIp3nP	zakrývat
chrániče	chránič	k1gInPc4	chránič
chrániče	chránič	k1gInSc2	chránič
holení	holení	k1gNnPc2	holení
-	-	kIx~	-
chrání	chránit	k5eAaImIp3nS	chránit
holeň	holeň	k1gFnSc1	holeň
před	před	k7c4	před
kopnutí	kopnutí	k1gNnSc4	kopnutí
protihráčů	protihráč	k1gMnPc2	protihráč
<g/>
.	.	kIx.	.
</s>
<s>
Brankář	brankář	k1gMnSc1	brankář
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
chrániče	chránič	k1gInPc1	chránič
kolenou	koleno	k1gNnPc2	koleno
kopačky	kopačka	k1gFnSc2	kopačka
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
normálních	normální	k2eAgFnPc2d1	normální
bot	bota	k1gFnPc2	bota
mají	mít	k5eAaImIp3nP	mít
kolíky	kolík	k1gInPc1	kolík
nebo	nebo	k8xC	nebo
špunty	špunty	k?	špunty
<g/>
.	.	kIx.	.
</s>
<s>
Kolíky	kolík	k1gInPc1	kolík
nebo	nebo	k8xC	nebo
špunty	špunty	k?	špunty
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
aby	aby	kYmCp3nS	aby
hráč	hráč	k1gMnSc1	hráč
neuklouzl	uklouznout	k5eNaPmAgMnS	uklouznout
na	na	k7c6	na
mokrém	mokrý	k2eAgInSc6d1	mokrý
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Brankáři	brankář	k1gMnPc1	brankář
mají	mít	k5eAaImIp3nP	mít
barvu	barva	k1gFnSc4	barva
dresů	dres	k1gInPc2	dres
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc2	jeho
asistentů	asistent	k1gMnPc2	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
utkání	utkání	k1gNnSc1	utkání
řídí	řídit	k5eAaImIp3nS	řídit
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
neomezenou	omezený	k2eNgFnSc4d1	neomezená
pravomoc	pravomoc	k1gFnSc4	pravomoc
k	k	k7c3	k
uplatňování	uplatňování	k1gNnSc3	uplatňování
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
píšťalkou	píšťalka	k1gFnSc7	píšťalka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
může	moct	k5eAaImIp3nS	moct
přerušit	přerušit	k5eAaPmF	přerušit
hru	hra	k1gFnSc4	hra
v	v	k7c6	v
případě	případ	k1gInSc6	případ
porušení	porušení	k1gNnSc2	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
nebo	nebo	k8xC	nebo
padnutí	padnutí	k1gNnSc2	padnutí
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
dvě	dva	k4xCgFnPc1	dva
plastové	plastový	k2eAgFnPc1d1	plastová
kartičky	kartička	k1gFnPc1	kartička
žluté	žlutý	k2eAgFnPc1d1	žlutá
a	a	k8xC	a
červené	červený	k2eAgFnPc1d1	červená
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
potrestání	potrestání	k1gNnSc4	potrestání
hráče	hráč	k1gMnSc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Každého	každý	k3xTgNnSc2	každý
utkání	utkání	k1gNnSc2	utkání
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
dva	dva	k4xCgMnPc1	dva
asistenti	asistent	k1gMnPc1	asistent
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
na	na	k7c6	na
pomezních	pomezní	k2eAgFnPc6d1	pomezní
čarách	čára	k1gFnPc6	čára
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
signalizovat	signalizovat	k5eAaImF	signalizovat
rozhodčímu	rozhodčí	k1gMnSc3	rozhodčí
<g/>
:	:	kIx,	:
když	když	k8xS	když
míč	míč	k1gInSc1	míč
opustí	opustit	k5eAaPmIp3nS	opustit
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
družstvo	družstvo	k1gNnSc1	družstvo
má	mít	k5eAaImIp3nS	mít
provádět	provádět	k5eAaImF	provádět
kop	kop	k1gInSc4	kop
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
<g/>
,	,	kIx,	,
kop	kop	k1gInSc4	kop
od	od	k7c2	od
branky	branka	k1gFnSc2	branka
nebo	nebo	k8xC	nebo
vhazování	vhazování	k1gNnSc2	vhazování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
<g />
.	.	kIx.	.
</s>
<s>
hráč	hráč	k1gMnSc1	hráč
potrestán	potrestat	k5eAaPmNgMnS	potrestat
za	za	k7c4	za
ofsajd	ofsajd	k1gInSc4	ofsajd
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
nějaké	nějaký	k3yIgNnSc1	nějaký
mužstvo	mužstvo	k1gNnSc1	mužstvo
připraveno	připravit	k5eAaPmNgNnS	připravit
střídat	střídat	k5eAaImF	střídat
<g/>
,	,	kIx,	,
nesportovní	sportovní	k2eNgNnSc1d1	nesportovní
chování	chování	k1gNnSc1	chování
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
mimo	mimo	k7c4	mimo
zorné	zorný	k2eAgNnSc4d1	zorné
pole	pole	k1gNnSc4	pole
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
o	o	k7c6	o
nějakém	nějaký	k3yIgInSc6	nějaký
přestupku	přestupek	k1gInSc6	přestupek
lepší	dobrý	k2eAgInSc4d2	lepší
přehled	přehled	k1gInSc4	přehled
než	než	k8xS	než
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
,	,	kIx,	,
při	při	k7c6	při
pokutovém	pokutový	k2eAgInSc6d1	pokutový
kopu	kop	k1gInSc6	kop
přestupky	přestupek	k1gInPc4	přestupek
brankáře	brankář	k1gMnSc2	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
poločasy	poločas	k1gInPc4	poločas
o	o	k7c6	o
45	[number]	k4	45
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
poločasy	poločas	k1gInPc7	poločas
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
<g/>
minutová	minutový	k2eAgFnSc1d1	minutová
přestávka	přestávka	k1gFnSc1	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
včetně	včetně	k7c2	včetně
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
zameškanou	zameškaný	k2eAgFnSc4d1	zameškaná
střídáním	střídání	k1gNnSc7	střídání
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
zdržováním	zdržování	k1gNnSc7	zdržování
hry	hra	k1gFnSc2	hra
apod.	apod.	kA	apod.
může	moct	k5eAaImIp3nS	moct
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
každý	každý	k3xTgInSc4	každý
poločas	poločas	k1gInSc4	poločas
prodloužit	prodloužit	k5eAaPmF	prodloužit
<g/>
.	.	kIx.	.
</s>
<s>
Poločas	poločas	k1gInSc1	poločas
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
prodloužit	prodloužit	k5eAaPmF	prodloužit
také	také	k9	také
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mužstev	mužstvo	k1gNnPc2	mužstvo
kopat	kopat	k5eAaImF	kopat
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětských	dětský	k2eAgFnPc6d1	dětská
a	a	k8xC	a
mládežnických	mládežnický	k2eAgFnPc6d1	mládežnická
kategoriích	kategorie	k1gFnPc6	kategorie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kratší	krátký	k2eAgFnSc1d2	kratší
doba	doba	k1gFnSc1	doba
hry	hra	k1gFnSc2	hra
-	-	kIx~	-
u	u	k7c2	u
mladšího	mladý	k2eAgInSc2d2	mladší
dorostu	dorost	k1gInSc2	dorost
a	a	k8xC	a
dorostenek	dorostenka	k1gFnPc2	dorostenka
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgInSc1	každý
poločas	poločas	k1gInSc1	poločas
40	[number]	k4	40
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
u	u	k7c2	u
staršího	starý	k2eAgInSc2d2	starší
dorostu	dorost	k1gInSc2	dorost
se	se	k3xPyFc4	se
již	již	k6eAd1	již
hraje	hrát	k5eAaImIp3nS	hrát
plná	plný	k2eAgFnSc1d1	plná
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
žáků	žák	k1gMnPc2	žák
a	a	k8xC	a
u	u	k7c2	u
žákyň	žákyně	k1gFnPc2	žákyně
35	[number]	k4	35
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnPc1d2	mladší
žáci	žák	k1gMnPc1	žák
hrají	hrát	k5eAaImIp3nP	hrát
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
×	×	k?	×
<g/>
35	[number]	k4	35
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
starší	starý	k2eAgInPc1d2	starší
přípravky	přípravek	k1gInPc1	přípravek
hrají	hrát	k5eAaImIp3nP	hrát
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
25	[number]	k4	25
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
pak	pak	k6eAd1	pak
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
utkání	utkání	k1gNnSc2	utkání
je	být	k5eAaImIp3nS	být
losováním	losování	k1gNnSc7	losování
mincí	mince	k1gFnPc2	mince
určeno	určit	k5eAaPmNgNnS	určit
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
družstvo	družstvo	k1gNnSc1	družstvo
si	se	k3xPyFc3	se
zvolí	zvolit	k5eAaPmIp3nS	zvolit
polovinu	polovina	k1gFnSc4	polovina
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
družstvo	družstvo	k1gNnSc1	družstvo
bude	být	k5eAaImBp3nS	být
zahajovat	zahajovat	k5eAaImF	zahajovat
hru	hra	k1gFnSc4	hra
výkopem	výkop	k1gInSc7	výkop
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poločasové	poločasový	k2eAgFnSc6d1	poločasová
přestávce	přestávka	k1gFnSc6	přestávka
si	se	k3xPyFc3	se
družstva	družstvo	k1gNnPc1	družstvo
poloviny	polovina	k1gFnSc2	polovina
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
a	a	k8xC	a
výkop	výkop	k1gInSc1	výkop
provádí	provádět	k5eAaImIp3nS	provádět
to	ten	k3xDgNnSc4	ten
družstvo	družstvo	k1gNnSc4	družstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
ho	on	k3xPp3gNnSc4	on
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
poločase	poločas	k1gInSc6	poločas
neprovádělo	provádět	k5eNaImAgNnS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
každého	každý	k3xTgInSc2	každý
poločasu	poločas	k1gInSc2	poločas
a	a	k8xC	a
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
každé	každý	k3xTgFnSc2	každý
branky	branka	k1gFnSc2	branka
se	se	k3xPyFc4	se
hra	hra	k1gFnSc1	hra
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
či	či	k8xC	či
navazuje	navazovat	k5eAaImIp3nS	navazovat
pomocí	pomocí	k7c2	pomocí
výkopu	výkop	k1gInSc2	výkop
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
všichni	všechen	k3xTgMnPc1	všechen
hráči	hráč	k1gMnPc1	hráč
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
polovině	polovina	k1gFnSc6	polovina
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
míč	míč	k1gInSc1	míč
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c4	na
středovou	středový	k2eAgFnSc4d1	středová
značku	značka	k1gFnSc4	značka
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
dá	dát	k5eAaPmIp3nS	dát
znamení	znamení	k1gNnSc4	znamení
<g/>
,	,	kIx,	,
rozehraje	rozehrát	k5eAaPmIp3nS	rozehrát
hráč	hráč	k1gMnSc1	hráč
příslušného	příslušný	k2eAgNnSc2d1	příslušné
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
soupeřova	soupeřův	k2eAgNnSc2d1	soupeřovo
mužstva	mužstvo	k1gNnSc2	mužstvo
musí	muset	k5eAaImIp3nS	muset
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zůstat	zůstat	k5eAaPmF	zůstat
mimo	mimo	k7c4	mimo
středový	středový	k2eAgInSc4d1	středový
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
branky	branka	k1gFnSc2	branka
rozehrává	rozehrávat	k5eAaImIp3nS	rozehrávat
výkopem	výkop	k1gInSc7	výkop
to	ten	k3xDgNnSc4	ten
mužstvo	mužstvo	k1gNnSc4	mužstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
branku	branka	k1gFnSc4	branka
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byla	být	k5eAaImAgFnS	být
hra	hra	k1gFnSc1	hra
přerušena	přerušit	k5eAaPmNgFnS	přerušit
ze	z	k7c2	z
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zranění	zranění	k1gNnSc4	zranění
hráče	hráč	k1gMnSc4	hráč
<g/>
,	,	kIx,	,
vniknutí	vniknutí	k1gNnSc4	vniknutí
diváka	divák	k1gMnSc2	divák
na	na	k7c4	na
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
se	se	k3xPyFc4	se
míčem	míč	k1gInSc7	míč
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
nechá	nechat	k5eAaPmIp3nS	nechat
spadnout	spadnout	k5eAaPmF	spadnout
míč	míč	k1gInSc4	míč
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
přerušení	přerušení	k1gNnSc2	přerušení
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
uvnitř	uvnitř	k7c2	uvnitř
brankového	brankový	k2eAgNnSc2d1	brankové
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
rozehrává	rozehrávat	k5eAaImIp3nS	rozehrávat
se	se	k3xPyFc4	se
z	z	k7c2	z
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
delším	dlouhý	k2eAgInSc6d2	delší
okraji	okraj	k1gInSc6	okraj
brankového	brankový	k2eAgNnSc2d1	brankové
území	území	k1gNnSc2	území
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
míč	míč	k1gInSc1	míč
dotkne	dotknout	k5eAaPmIp3nS	dotknout
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Míč	míč	k1gInSc1	míč
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
buď	buď	k8xC	buď
přešel	přejít	k5eAaPmAgInS	přejít
(	(	kIx(	(
<g/>
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
)	)	kIx)	)
úplně	úplně	k6eAd1	úplně
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
objemem	objem	k1gInSc7	objem
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
úplně	úplně	k6eAd1	úplně
celý	celý	k2eAgInSc4d1	celý
míč	míč	k1gInSc4	míč
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zcela	zcela	k6eAd1	zcela
za	za	k7c7	za
vnějším	vnější	k2eAgInSc7d1	vnější
okrajem	okraj	k1gInSc7	okraj
příslušné	příslušný	k2eAgFnSc2d1	příslušná
čáry	čára	k1gFnSc2	čára
<g/>
)	)	kIx)	)
brankovou	brankový	k2eAgFnSc4d1	branková
nebo	nebo	k8xC	nebo
pomezní	pomezní	k2eAgFnSc4d1	pomezní
čáru	čára	k1gFnSc4	čára
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
přeruší	přerušit	k5eAaPmIp3nS	přerušit
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
každou	každý	k3xTgFnSc4	každý
jinou	jiný	k2eAgFnSc4d1	jiná
dobu	doba	k1gFnSc4	doba
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Branky	branka	k1gFnSc2	branka
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
míč	míč	k1gInSc4	míč
úplně	úplně	k6eAd1	úplně
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
objemem	objem	k1gInSc7	objem
<g/>
)	)	kIx)	)
přejde	přejít	k5eAaPmIp3nS	přejít
brankovou	brankový	k2eAgFnSc4d1	branková
čáru	čára	k1gFnSc4	čára
mezi	mezi	k7c7	mezi
brankovými	brankový	k2eAgFnPc7d1	branková
tyčemi	tyč	k1gFnPc7	tyč
a	a	k8xC	a
pod	pod	k7c7	pod
břevnem	břevno	k1gNnSc7	břevno
a	a	k8xC	a
útočící	útočící	k2eAgNnSc4d1	útočící
mužstvo	mužstvo	k1gNnSc4	mužstvo
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
neporušilo	porušit	k5eNaPmAgNnS	porušit
žádné	žádný	k3yNgNnSc1	žádný
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Mužstvo	mužstvo	k1gNnSc1	mužstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
utkání	utkání	k1gNnSc2	utkání
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
vyššího	vysoký	k2eAgInSc2d2	vyšší
počtu	počet	k1gInSc2	počet
branek	branka	k1gFnPc2	branka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vítězem	vítěz	k1gMnSc7	vítěz
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
obě	dva	k4xCgNnPc4	dva
mužstva	mužstvo	k1gNnPc4	mužstvo
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
stejného	stejný	k2eAgInSc2d1	stejný
počtu	počet	k1gInSc2	počet
branek	branka	k1gFnPc2	branka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výsledek	výsledek	k1gInSc1	výsledek
utkání	utkání	k1gNnPc2	utkání
nerozhodný	rozhodný	k2eNgInSc1d1	nerozhodný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
soutěžích	soutěž	k1gFnPc6	soutěž
není	být	k5eNaImIp3nS	být
nerozhodný	rozhodný	k2eNgInSc1d1	nerozhodný
výsledek	výsledek	k1gInSc1	výsledek
přípustný	přípustný	k2eAgInSc1d1	přípustný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
příslušný	příslušný	k2eAgInSc1d1	příslušný
soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
řád	řád	k1gInSc1	řád
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
postupuje	postupovat	k5eAaImIp3nS	postupovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prodloužením	prodloužení	k1gNnSc7	prodloužení
doby	doba	k1gFnSc2	doba
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
prováděním	provádění	k1gNnSc7	provádění
pokutových	pokutový	k2eAgMnPc2d1	pokutový
kopů	kop	k1gInPc2	kop
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hodnotou	hodnota	k1gFnSc7	hodnota
branek	branka	k1gFnPc2	branka
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
mužstvem	mužstvo	k1gNnSc7	mužstvo
hostí	host	k1gMnPc2wK	host
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ofsajd	ofsajd	k1gInSc1	ofsajd
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
některý	některý	k3yIgMnSc1	některý
hráč	hráč	k1gMnSc1	hráč
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
soupeřově	soupeřův	k2eAgFnSc6d1	soupeřova
polovině	polovina	k1gFnSc6	polovina
<g/>
,	,	kIx,	,
blíže	blízce	k6eAd2	blízce
soupeřově	soupeřův	k2eAgFnSc6d1	soupeřova
brankové	brankový	k2eAgFnSc6d1	branková
čáře	čára	k1gFnSc6	čára
než	než	k8xS	než
míč	míč	k1gInSc4	míč
a	a	k8xC	a
než	než	k8xS	než
předposlední	předposlední	k2eAgMnSc1d1	předposlední
hráč	hráč	k1gMnSc1	hráč
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ofsajdové	ofsajdový	k2eAgFnSc6d1	ofsajdová
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
není	být	k5eNaImIp3nS	být
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
porušením	porušení	k1gNnSc7	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
však	však	k9	však
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
ofsajdové	ofsajdový	k2eAgFnSc6d1	ofsajdová
pozici	pozice	k1gFnSc6	pozice
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
míče	míč	k1gInSc2	míč
dotkne	dotknout	k5eAaPmIp3nS	dotknout
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
spoluhráčů	spoluhráč	k1gMnPc2	spoluhráč
<g/>
,	,	kIx,	,
aktivně	aktivně	k6eAd1	aktivně
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
hraním	hranit	k5eAaImIp1nS	hranit
míče	míč	k1gInSc2	míč
<g/>
,	,	kIx,	,
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
soupeře	soupeř	k1gMnPc4	soupeř
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
získává	získávat	k5eAaImIp3nS	získávat
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
výhodu	výhoda	k1gFnSc4	výhoda
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hraje	hrát	k5eAaImIp3nS	hrát
odražený	odražený	k2eAgInSc4d1	odražený
míč	míč	k1gInSc4	míč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ofsajdu	ofsajd	k1gInSc6	ofsajd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
situaci	situace	k1gFnSc6	situace
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
základě	základ	k1gInSc6	základ
signalizace	signalizace	k1gFnSc2	signalizace
asistenta	asistent	k1gMnSc2	asistent
<g/>
)	)	kIx)	)
přeruší	přerušit	k5eAaPmIp3nS	přerušit
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
naváže	navázat	k5eAaPmIp3nS	navázat
soupeř	soupeř	k1gMnSc1	soupeř
nepřímým	přímý	k2eNgInSc7d1	nepřímý
volným	volný	k2eAgInSc7d1	volný
kopem	kop	k1gInSc7	kop
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
provinivší	provinivší	k2eAgMnPc4d1	provinivší
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
přestupku	přestupek	k1gInSc2	přestupek
<g/>
.	.	kIx.	.
</s>
<s>
Ofsajd	ofsajd	k1gInSc1	ofsajd
se	se	k3xPyFc4	se
netrestá	trestat	k5eNaImIp3nS	trestat
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hráč	hráč	k1gMnSc1	hráč
dostane	dostat	k5eAaPmIp3nS	dostat
míč	míč	k1gInSc4	míč
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
kopu	kop	k1gInSc2	kop
od	od	k7c2	od
branky	branka	k1gFnSc2	branka
<g/>
,	,	kIx,	,
z	z	k7c2	z
vhazování	vhazování	k1gNnSc2	vhazování
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
kopu	kop	k1gInSc2	kop
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
některý	některý	k3yIgMnSc1	některý
hráč	hráč	k1gMnSc1	hráč
na	na	k7c6	na
hrací	hrací	k2eAgFnSc6d1	hrací
ploše	plocha	k1gFnSc6	plocha
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
dopustí	dopustit	k5eAaPmIp3nS	dopustit
některého	některý	k3yIgNnSc2	některý
z	z	k7c2	z
dále	daleko	k6eAd2	daleko
vyjmenovaných	vyjmenovaný	k2eAgInPc2d1	vyjmenovaný
přestupků	přestupek	k1gInPc2	přestupek
<g/>
,	,	kIx,	,
nařídí	nařídit	k5eAaPmIp3nS	nařídit
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gNnSc3	jeho
mužstvu	mužstvo	k1gNnSc3	mužstvo
přímý	přímý	k2eAgInSc4d1	přímý
volný	volný	k2eAgInSc4d1	volný
kop	kop	k1gInSc4	kop
<g/>
:	:	kIx,	:
kopne	kopnout	k5eAaPmIp3nS	kopnout
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
kopnout	kopnout	k5eAaPmF	kopnout
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
,	,	kIx,	,
podrazí	podrazit	k5eAaPmIp3nS	podrazit
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
podrazit	podrazit	k5eAaPmF	podrazit
soupeře	soupeř	k1gMnPc4	soupeř
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
skočí	skočit	k5eAaPmIp3nP	skočit
na	na	k7c4	na
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
,	,	kIx,	,
vrazí	vrazit	k5eAaPmIp3nS	vrazit
do	do	k7c2	do
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
,	,	kIx,	,
udeří	udeřit	k5eAaPmIp3nS	udeřit
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
udeřit	udeřit	k5eAaPmF	udeřit
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
,	,	kIx,	,
strčí	strčit	k5eAaPmIp3nS	strčit
do	do	k7c2	do
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
,	,	kIx,	,
při	při	k7c6	při
odebírání	odebírání	k1gNnSc6	odebírání
míče	míč	k1gInSc2	míč
skluzem	skluz	k1gInSc7	skluz
se	se	k3xPyFc4	se
soupeře	soupeř	k1gMnSc4	soupeř
dotkne	dotknout	k5eAaPmIp3nS	dotknout
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
míče	míč	k1gInPc4	míč
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nP	držet
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
,	,	kIx,	,
plivne	plivnout	k5eAaPmIp3nS	plivnout
na	na	k7c4	na
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
,	,	kIx,	,
úmyslně	úmyslně	k6eAd1	úmyslně
hraje	hrát	k5eAaImIp3nS	hrát
míč	míč	k1gInSc4	míč
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
brankáře	brankář	k1gMnSc2	brankář
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
vlastním	vlastní	k2eAgNnSc6d1	vlastní
pokutovém	pokutový	k2eAgNnSc6d1	pokutové
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
přímý	přímý	k2eAgInSc1d1	přímý
volný	volný	k2eAgInSc1d1	volný
kop	kop	k1gInSc1	kop
provádí	provádět	k5eAaImIp3nS	provádět
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
k	k	k7c3	k
přestupku	přestupek	k1gInSc3	přestupek
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
dopustí	dopustit	k5eAaPmIp3nS	dopustit
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
přestupků	přestupek	k1gInPc2	přestupek
ve	v	k7c6	v
vlastním	vlastní	k2eAgNnSc6d1	vlastní
pokutovém	pokutový	k2eAgNnSc6d1	pokutové
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
míč	míč	k1gInSc1	míč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nařídí	nařídit	k5eAaPmIp3nS	nařídit
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gNnSc3	jeho
mužstvu	mužstvo	k1gNnSc3	mužstvo
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
brankář	brankář	k1gMnSc1	brankář
ve	v	k7c6	v
vlastním	vlastní	k2eAgNnSc6d1	vlastní
pokutovém	pokutový	k2eAgNnSc6d1	pokutové
území	území	k1gNnSc6	území
dopustí	dopustit	k5eAaPmIp3nS	dopustit
některého	některý	k3yIgMnSc2	některý
z	z	k7c2	z
dále	daleko	k6eAd2	daleko
vyjmenovaných	vyjmenovaný	k2eAgInPc2d1	vyjmenovaný
přestupků	přestupek	k1gInPc2	přestupek
<g/>
,	,	kIx,	,
nařídí	nařídit	k5eAaPmIp3nS	nařídit
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gNnSc3	jeho
mužstvu	mužstvo	k1gNnSc3	mužstvo
nepřímý	přímý	k2eNgInSc4d1	nepřímý
volný	volný	k2eAgInSc4d1	volný
kop	kop	k1gInSc4	kop
<g/>
:	:	kIx,	:
má	mít	k5eAaImIp3nS	mít
míč	míč	k1gInSc4	míč
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
šest	šest	k4xCc4	šest
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
rukou	ruka	k1gFnSc7	ruka
se	se	k3xPyFc4	se
míče	míč	k1gInSc2	míč
dotkne	dotknout	k5eAaPmIp3nS	dotknout
znovu	znovu	k6eAd1	znovu
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
zbavil	zbavit	k5eAaPmAgMnS	zbavit
a	a	k8xC	a
míče	míč	k1gInPc1	míč
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
nedotkl	dotknout	k5eNaPmAgMnS	dotknout
jiný	jiný	k2eAgMnSc1d1	jiný
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
rukou	ruka	k1gFnSc7	ruka
se	se	k3xPyFc4	se
dotkne	dotknout	k5eAaPmIp3nS	dotknout
míče	míč	k1gInSc2	míč
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
mu	on	k3xPp3gMnSc3	on
jej	on	k3xPp3gInSc4	on
úmyslně	úmyslně	k6eAd1	úmyslně
nohou	noha	k1gFnSc7	noha
přihraje	přihrát	k5eAaPmIp3nS	přihrát
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
<g/>
,	,	kIx,	,
dotkne	dotknout	k5eAaPmIp3nS	dotknout
se	se	k3xPyFc4	se
rukou	ruka	k1gFnSc7	ruka
míče	míč	k1gInSc2	míč
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
jeho	jeho	k3xOp3gMnSc1	jeho
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
provedl	provést	k5eAaPmAgMnS	provést
vhazování	vhazování	k1gNnSc4	vhazování
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímý	přímý	k2eNgInSc4d1	nepřímý
volný	volný	k2eAgInSc4d1	volný
kop	kop	k1gInSc4	kop
nařídí	nařídit	k5eAaPmIp3nS	nařídit
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
také	také	k9	také
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
dopustí	dopustit	k5eAaPmIp3nS	dopustit
některého	některý	k3yIgNnSc2	některý
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
přestupků	přestupek	k1gInPc2	přestupek
<g/>
:	:	kIx,	:
hraje	hrát	k5eAaImIp3nS	hrát
nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
brání	bránit	k5eAaImIp3nP	bránit
soupeři	soupeř	k1gMnPc1	soupeř
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
,	,	kIx,	,
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
brankáři	brankář	k1gMnSc3	brankář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
míč	míč	k1gInSc4	míč
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
míče	míč	k1gInPc4	míč
zbavil	zbavit	k5eAaPmAgInS	zbavit
<g/>
,	,	kIx,	,
dopustí	dopustit	k5eAaPmIp3nP	dopustit
se	se	k3xPyFc4	se
nějakého	nějaký	k3yIgInSc2	nějaký
jiného	jiný	k2eAgInSc2d1	jiný
přestupku	přestupek	k1gInSc2	přestupek
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yQgInSc4	který
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
udělí	udělit	k5eAaPmIp3nP	udělit
osobní	osobní	k2eAgInSc4d1	osobní
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímý	přímý	k2eNgInSc4d1	nepřímý
volný	volný	k2eAgInSc4d1	volný
kop	kop	k1gInSc4	kop
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
přestupku	přestupek	k1gInSc3	přestupek
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc4d1	volný
kop	kop	k1gInSc4	kop
(	(	kIx(	(
<g/>
přímý	přímý	k2eAgInSc4d1	přímý
či	či	k8xC	či
nepřímý	přímý	k2eNgInSc4d1	nepřímý
<g/>
)	)	kIx)	)
nařízený	nařízený	k2eAgMnSc1d1	nařízený
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
mužstva	mužstvo	k1gNnSc2	mužstvo
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
vlastním	vlastní	k2eAgNnSc6d1	vlastní
brankovém	brankový	k2eAgNnSc6d1	brankové
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nS	smět
provést	provést	k5eAaPmF	provést
z	z	k7c2	z
libovolného	libovolný	k2eAgNnSc2d1	libovolné
místa	místo	k1gNnSc2	místo
uvnitř	uvnitř	k7c2	uvnitř
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímý	přímý	k2eNgInSc4d1	nepřímý
volný	volný	k2eAgInSc4d1	volný
kop	kop	k1gInSc4	kop
nařízený	nařízený	k2eAgInSc4d1	nařízený
proti	proti	k7c3	proti
mužstvu	mužstvo	k1gNnSc3	mužstvo
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
brankovém	brankový	k2eAgNnSc6d1	brankové
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
z	z	k7c2	z
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
delší	dlouhý	k2eAgFnSc6d2	delší
hraně	hrana	k1gFnSc6	hrana
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Hráče	hráč	k1gMnPc4	hráč
nebo	nebo	k8xC	nebo
náhradníka	náhradník	k1gMnSc4	náhradník
může	moct	k5eAaImIp3nS	moct
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
napomenout	napomenout	k5eAaPmF	napomenout
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ukáže	ukázat	k5eAaPmIp3nS	ukázat
žlutou	žlutý	k2eAgFnSc4d1	žlutá
kartu	karta	k1gFnSc4	karta
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
dopustí	dopustit	k5eAaPmIp3nS	dopustit
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
následujících	následující	k2eAgMnPc2d1	následující
přestupků	přestupek	k1gInPc2	přestupek
<g/>
:	:	kIx,	:
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
nesportovně	sportovně	k6eNd1	sportovně
<g/>
,	,	kIx,	,
slovy	slovo	k1gNnPc7	slovo
nebo	nebo	k8xC	nebo
jednáním	jednání	k1gNnSc7	jednání
projevuje	projevovat	k5eAaImIp3nS	projevovat
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
nebo	nebo	k8xC	nebo
protestuje	protestovat	k5eAaBmIp3nS	protestovat
<g/>
,	,	kIx,	,
soustavně	soustavně	k6eAd1	soustavně
porušuje	porušovat	k5eAaImIp3nS	porušovat
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
navázání	navázání	k1gNnSc4	navázání
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
nedodrží	dodržet	k5eNaPmIp3nS	dodržet
předepsanou	předepsaný	k2eAgFnSc4d1	předepsaná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
volného	volný	k2eAgInSc2d1	volný
kopu	kop	k1gInSc2	kop
nebo	nebo	k8xC	nebo
kopu	kop	k1gInSc2	kop
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
na	na	k7c4	na
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
úmyslně	úmyslně	k6eAd1	úmyslně
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
opustí	opustit	k5eAaPmIp3nP	opustit
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
musí	muset	k5eAaImIp3nP	muset
vyloučit	vyloučit	k5eAaPmF	vyloučit
hráče	hráč	k1gMnPc4	hráč
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dopustí	dopustit	k5eAaPmIp3nS	dopustit
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
přestupků	přestupek	k1gInPc2	přestupek
<g/>
:	:	kIx,	:
hraje	hrát	k5eAaImIp3nS	hrát
surově	surově	k6eAd1	surově
<g/>
,	,	kIx,	,
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
hrubě	hrubě	k6eAd1	hrubě
nesportovně	sportovně	k6eNd1	sportovně
<g/>
,	,	kIx,	,
plivne	plivnout	k5eAaPmIp3nS	plivnout
na	na	k7c4	na
soupeře	soupeř	k1gMnSc4	soupeř
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc4d1	jiná
osobu	osoba	k1gFnSc4	osoba
zabrání	zabránit	k5eAaPmIp3nS	zabránit
soupeřovu	soupeřův	k2eAgNnSc3d1	soupeřovo
družstvu	družstvo	k1gNnSc3	družstvo
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
branky	branka	k1gFnPc4	branka
nebo	nebo	k8xC	nebo
zmaří	zmařit	k5eAaPmIp3nP	zmařit
jeho	jeho	k3xOp3gFnSc4	jeho
zjevnou	zjevný	k2eAgFnSc4d1	zjevná
možnost	možnost	k1gFnSc4	možnost
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
branky	branka	k1gFnSc2	branka
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
že	že	k8xS	že
zahraje	zahrát	k5eAaPmIp3nS	zahrát
míč	míč	k1gInSc4	míč
rukou	ruka	k1gFnPc2	ruka
(	(	kIx(	(
<g/>
netýká	týkat	k5eNaImIp3nS	týkat
se	se	k3xPyFc4	se
brankáře	brankář	k1gMnSc2	brankář
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
pokutovém	pokutový	k2eAgNnSc6d1	pokutové
území	území	k1gNnSc6	území
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zmaří	zmařit	k5eAaPmIp3nS	zmařit
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
zjevnou	zjevný	k2eAgFnSc4d1	zjevná
možnost	možnost	k1gFnSc4	možnost
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
branky	branka	k1gFnSc2	branka
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
soupeře	soupeř	k1gMnSc4	soupeř
útočícího	útočící	k2eAgMnSc4d1	útočící
na	na	k7c4	na
branku	branka	k1gFnSc4	branka
zastaví	zastavit	k5eAaPmIp3nS	zastavit
přestupkem	přestupek	k1gInSc7	přestupek
<g/>
,	,	kIx,	,
za	za	k7c4	za
nějž	jenž	k3xRgMnSc4	jenž
se	se	k3xPyFc4	se
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
volný	volný	k2eAgInSc4d1	volný
přímý	přímý	k2eAgInSc4d1	přímý
kop	kop	k1gInSc4	kop
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
pohoršujících	pohoršující	k2eAgInPc2d1	pohoršující
či	či	k8xC	či
urážlivých	urážlivý	k2eAgInPc2d1	urážlivý
výroků	výrok	k1gInPc2	výrok
nebo	nebo	k8xC	nebo
gest	gesto	k1gNnPc2	gesto
<g/>
,	,	kIx,	,
během	během	k7c2	během
utkání	utkání	k1gNnSc2	utkání
je	být	k5eAaImIp3nS	být
podruhé	podruhé	k6eAd1	podruhé
napomenut	napomenout	k5eAaPmNgMnS	napomenout
žlutou	žlutý	k2eAgFnSc7d1	žlutá
kartou	karta	k1gFnSc7	karta
<g/>
.	.	kIx.	.
</s>
<s>
Vyloučený	vyloučený	k2eAgMnSc1d1	vyloučený
hráč	hráč	k1gMnSc1	hráč
musí	muset	k5eAaImIp3nS	muset
opustit	opustit	k5eAaPmF	opustit
prostor	prostor	k1gInSc4	prostor
hřiště	hřiště	k1gNnSc2	hřiště
(	(	kIx(	(
<g/>
i	i	k9	i
lavičku	lavička	k1gFnSc4	lavička
náhradníků	náhradník	k1gMnPc2	náhradník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
volných	volný	k2eAgInPc2d1	volný
kopů	kop	k1gInPc2	kop
<g/>
:	:	kIx,	:
přímé	přímý	k2eAgInPc4d1	přímý
volné	volný	k2eAgInPc4d1	volný
kopy	kop	k1gInPc4	kop
a	a	k8xC	a
nepřímé	přímý	k2eNgInPc4d1	nepřímý
volné	volný	k2eAgInPc4d1	volný
kopy	kop	k1gInPc4	kop
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
přímého	přímý	k2eAgInSc2d1	přímý
volného	volný	k2eAgInSc2d1	volný
kopu	kop	k1gInSc2	kop
dostane	dostat	k5eAaPmIp3nS	dostat
míč	míč	k1gInSc4	míč
do	do	k7c2	do
soupeřovy	soupeřův	k2eAgFnSc2d1	soupeřova
branky	branka	k1gFnSc2	branka
<g/>
,	,	kIx,	,
branka	branka	k1gFnSc1	branka
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
vlastní	vlastní	k2eAgFnSc2d1	vlastní
branky	branka	k1gFnSc2	branka
<g/>
,	,	kIx,	,
branka	branka	k1gFnSc1	branka
neplatí	platit	k5eNaImIp3nS	platit
a	a	k8xC	a
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
naváže	navázat	k5eAaPmIp3nS	navázat
kopem	kop	k1gInSc7	kop
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
soupeřova	soupeřův	k2eAgNnSc2d1	soupeřovo
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímý	přímý	k2eNgInSc4d1	nepřímý
volný	volný	k2eAgInSc4d1	volný
kop	kop	k1gInSc4	kop
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
paží	paže	k1gFnSc7	paže
zvednutou	zvednutý	k2eAgFnSc7d1	zvednutá
nad	nad	k7c4	nad
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tam	tam	k6eAd1	tam
drží	držet	k5eAaImIp3nS	držet
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
kopu	kop	k1gInSc2	kop
dotkne	dotknout	k5eAaPmIp3nS	dotknout
míče	míč	k1gInSc2	míč
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Branky	branka	k1gFnPc4	branka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
nepřímého	přímý	k2eNgInSc2d1	nepřímý
volného	volný	k2eAgInSc2d1	volný
kopu	kop	k1gInSc2	kop
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
dotkl	dotknout	k5eAaPmAgMnS	dotknout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
míče	míč	k1gInPc1	míč
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
rozehrání	rozehrání	k1gNnSc6	rozehrání
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
další	další	k2eAgMnSc1d1	další
hráč	hráč	k1gMnSc1	hráč
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
míč	míč	k1gInSc1	míč
přejde	přejít	k5eAaPmIp3nS	přejít
do	do	k7c2	do
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
míč	míč	k1gInSc1	míč
skončí	skončit	k5eAaPmIp3nS	skončit
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
nepřímého	přímý	k2eNgInSc2d1	nepřímý
volného	volný	k2eAgInSc2d1	volný
kopu	kop	k1gInSc2	kop
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
kdokoli	kdokoli	k3yInSc1	kdokoli
další	další	k2eAgFnSc2d1	další
dotkl	dotknout	k5eAaPmAgMnS	dotknout
<g/>
,	,	kIx,	,
branka	branka	k1gFnSc1	branka
neplatí	platit	k5eNaImIp3nS	platit
a	a	k8xC	a
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
naváže	navázat	k5eAaPmIp3nS	navázat
kopem	kop	k1gInSc7	kop
od	od	k7c2	od
branky	branka	k1gFnSc2	branka
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kopem	kop	k1gInSc7	kop
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
míč	míč	k1gInSc1	míč
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
kop	kop	k1gInSc4	kop
provádělo	provádět	k5eAaImAgNnS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
provedením	provedení	k1gNnSc7	provedení
volného	volný	k2eAgInSc2d1	volný
kopu	kop	k1gInSc2	kop
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
všichni	všechen	k3xTgMnPc1	všechen
hráči	hráč	k1gMnPc1	hráč
soupeře	soupeř	k1gMnSc2	soupeř
vzdáleni	vzdálit	k5eAaPmNgMnP	vzdálit
od	od	k7c2	od
míče	míč	k1gInSc2	míč
nejméně	málo	k6eAd3	málo
9,15	[number]	k4	9,15
m	m	kA	m
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
míč	míč	k1gInSc1	míč
nepohne	pohnout	k5eNaPmIp3nS	pohnout
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
volný	volný	k2eAgInSc1d1	volný
kop	kop	k1gInSc1	kop
prováděn	provádět	k5eAaImNgInS	provádět
z	z	k7c2	z
vlastního	vlastní	k2eAgNnSc2d1	vlastní
pokutového	pokutový	k2eAgNnSc2d1	pokutové
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
až	až	k9	až
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
opustí	opustit	k5eAaPmIp3nP	opustit
pokutové	pokutový	k2eAgNnSc4d1	pokutové
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nepřímý	přímý	k2eNgInSc4d1	nepřímý
volný	volný	k2eAgInSc4d1	volný
kop	kop	k1gInSc4	kop
prováděn	provádět	k5eAaImNgInS	provádět
v	v	k7c6	v
soupeřově	soupeřův	k2eAgNnSc6d1	soupeřovo
pokutovém	pokutový	k2eAgNnSc6d1	pokutové
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nP	muset
ti	ten	k3xDgMnPc1	ten
hráči	hráč	k1gMnPc1	hráč
soupeře	soupeř	k1gMnPc4	soupeř
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
brankové	brankový	k2eAgFnSc6d1	branková
čáře	čára	k1gFnSc6	čára
mezi	mezi	k7c7	mezi
tyčemi	tyč	k1gFnPc7	tyč
branky	branka	k1gFnSc2	branka
<g/>
,	,	kIx,	,
dodržovat	dodržovat	k5eAaImF	dodržovat
předepsanou	předepsaný	k2eAgFnSc4d1	předepsaná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
9,15	[number]	k4	9,15
m.	m.	k?	m.
Pokud	pokud	k8xS	pokud
hráči	hráč	k1gMnPc1	hráč
nedodrží	dodržet	k5eNaPmIp3nP	dodržet
předepsanou	předepsaný	k2eAgFnSc4d1	předepsaná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
nechá	nechat	k5eAaPmIp3nS	nechat
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
kop	kop	k1gInSc4	kop
opakovat	opakovat	k5eAaImF	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
rozehrává	rozehrávat	k5eAaImIp3nS	rozehrávat
volný	volný	k2eAgInSc4d1	volný
kop	kop	k1gInSc4	kop
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
míče	míč	k1gInPc1	míč
nesmí	smět	k5eNaImIp3nP	smět
znovu	znovu	k6eAd1	znovu
dotknout	dotknout	k5eAaPmF	dotknout
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
než	než	k8xS	než
míčem	míč	k1gInSc7	míč
zahraje	zahrát	k5eAaPmIp3nS	zahrát
jiný	jiný	k2eAgMnSc1d1	jiný
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
nařídí	nařídit	k5eAaPmIp3nS	nařídit
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gNnSc3	jeho
mužstvu	mužstvo	k1gNnSc3	mužstvo
nepřímý	přímý	k2eNgInSc4d1	nepřímý
volný	volný	k2eAgInSc4d1	volný
kop	kop	k1gInSc4	kop
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
<g/>
.	.	kIx.	.
</s>
<s>
Pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
mužstvu	mužstvo	k1gNnSc3	mužstvo
nařídí	nařídit	k5eAaPmIp3nS	nařídit
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
tohoto	tento	k3xDgNnSc2	tento
mužstva	mužstvo	k1gNnSc2	mužstvo
dopustí	dopustit	k5eAaPmIp3nS	dopustit
ve	v	k7c6	v
vlastním	vlastní	k2eAgNnSc6d1	vlastní
pokutovém	pokutový	k2eAgNnSc6d1	pokutové
území	území	k1gNnSc6	území
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
přestupků	přestupek	k1gInPc2	přestupek
jinak	jinak	k6eAd1	jinak
trestaných	trestaný	k2eAgInPc2d1	trestaný
přímým	přímý	k2eAgInSc7d1	přímý
volným	volný	k2eAgInSc7d1	volný
kopem	kop	k1gInSc7	kop
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pokutového	pokutový	k2eAgInSc2d1	pokutový
kopu	kop	k1gInSc2	kop
lze	lze	k6eAd1	lze
přímo	přímo	k6eAd1	přímo
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
branky	branka	k1gFnPc4	branka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
nařízen	nařídit	k5eAaPmNgInS	nařídit
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
poločasu	poločas	k1gInSc2	poločas
<g/>
,	,	kIx,	,
nastaví	nastavit	k5eAaPmIp3nS	nastavit
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
dobu	doba	k1gFnSc4	doba
hry	hra	k1gFnSc2	hra
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tento	tento	k3xDgInSc4	tento
kop	kop	k1gInSc4	kop
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
proveden	provést	k5eAaPmNgInS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
míč	míč	k1gInSc1	míč
umístí	umístit	k5eAaPmIp3nS	umístit
na	na	k7c4	na
pokutovou	pokutový	k2eAgFnSc4d1	Pokutová
značku	značka	k1gFnSc4	značka
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
mužstva	mužstvo	k1gNnSc2	mužstvo
provádějícího	provádějící	k2eAgMnSc2d1	provádějící
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
je	být	k5eAaImIp3nS	být
jasně	jasně	k6eAd1	jasně
a	a	k8xC	a
zřetelně	zřetelně	k6eAd1	zřetelně
určen	určit	k5eAaPmNgMnS	určit
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vykonání	vykonání	k1gNnSc3	vykonání
<g/>
.	.	kIx.	.
</s>
<s>
Brankář	brankář	k1gMnSc1	brankář
bránícího	bránící	k2eAgNnSc2d1	bránící
mužstva	mužstvo	k1gNnSc2	mužstvo
musí	muset	k5eAaImIp3nS	muset
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
na	na	k7c6	na
brankové	brankový	k2eAgFnSc6d1	branková
čáře	čára	k1gFnSc6	čára
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
hráči	hráč	k1gMnPc1	hráč
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
na	na	k7c6	na
hrací	hrací	k2eAgFnSc6d1	hrací
ploše	plocha	k1gFnSc6	plocha
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
pokutové	pokutový	k2eAgNnSc4d1	pokutové
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
9,15	[number]	k4	9,15
m	m	kA	m
od	od	k7c2	od
míče	míč	k1gInSc2	míč
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
mimo	mimo	k7c4	mimo
vyznačený	vyznačený	k2eAgInSc4d1	vyznačený
oblouk	oblouk	k1gInSc4	oblouk
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c7	za
pokutovou	pokutový	k2eAgFnSc7d1	Pokutová
značkou	značka	k1gFnSc7	značka
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
brankové	brankový	k2eAgFnSc2d1	branková
čáry	čára	k1gFnSc2	čára
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
pokutová	pokutový	k2eAgFnSc1d1	Pokutová
značka	značka	k1gFnSc1	značka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
pokutového	pokutový	k2eAgInSc2d1	pokutový
kopu	kop	k1gInSc2	kop
dá	dát	k5eAaPmIp3nS	dát
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
signál	signál	k1gInSc1	signál
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
provádějící	provádějící	k2eAgFnSc2d1	provádějící
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
v	v	k7c6	v
rozběhu	rozběh	k1gInSc6	rozběh
zastavit	zastavit	k5eAaPmF	zastavit
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgInSc7d1	jiný
obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
oklamat	oklamat	k5eAaPmF	oklamat
soupeřova	soupeřův	k2eAgMnSc4d1	soupeřův
brankáře	brankář	k1gMnSc4	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Míč	míč	k1gInSc1	míč
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
kopu	kop	k1gInSc2	kop
pohne	pohnout	k5eAaPmIp3nS	pohnout
kupředu	kupředu	k6eAd1	kupředu
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
provádějící	provádějící	k2eAgFnSc2d1	provádějící
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
se	se	k3xPyFc4	se
míče	míč	k1gInSc2	míč
nesmí	smět	k5eNaImIp3nS	smět
podruhé	podruhé	k6eAd1	podruhé
dotknout	dotknout	k5eAaPmF	dotknout
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
dotkne	dotknout	k5eAaPmIp3nS	dotknout
jiný	jiný	k2eAgMnSc1d1	jiný
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
kopu	kop	k1gInSc2	kop
porušena	porušen	k2eAgNnPc1d1	porušeno
pravidla	pravidlo	k1gNnPc1	pravidlo
hráčem	hráč	k1gMnSc7	hráč
bránícího	bránící	k2eAgNnSc2d1	bránící
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
uzná	uznat	k5eAaPmIp3nS	uznat
branku	branka	k1gFnSc4	branka
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
dosažena	dosáhnout	k5eAaPmNgFnS	dosáhnout
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
nechá	nechat	k5eAaPmIp3nS	nechat
kop	kop	k1gInSc4	kop
opakovat	opakovat	k5eAaImF	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pravidla	pravidlo	k1gNnPc4	pravidlo
poruší	porušit	k5eAaPmIp3nS	porušit
hráč	hráč	k1gMnSc1	hráč
útočícího	útočící	k2eAgNnSc2d1	útočící
mužstva	mužstvo	k1gNnSc2	mužstvo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
branky	branka	k1gFnSc2	branka
<g/>
,	,	kIx,	,
nechá	nechat	k5eAaPmIp3nS	nechat
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
kop	kop	k1gInSc4	kop
opakovat	opakovat	k5eAaImF	opakovat
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
nechá	nechat	k5eAaPmIp3nS	nechat
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pravidla	pravidlo	k1gNnPc1	pravidlo
poruší	porušit	k5eAaPmIp3nP	porušit
hráči	hráč	k1gMnPc7	hráč
obou	dva	k4xCgNnPc2	dva
mužstev	mužstvo	k1gNnPc2	mužstvo
<g/>
,	,	kIx,	,
nechá	nechat	k5eAaPmIp3nS	nechat
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
kop	kop	k1gInSc4	kop
opakovat	opakovat	k5eAaImF	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
přerušena	přerušen	k2eAgFnSc1d1	přerušena
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
míč	míč	k1gInSc1	míč
přešel	přejít	k5eAaPmAgInS	přejít
pomezní	pomezní	k2eAgFnSc4d1	pomezní
čáru	čára	k1gFnSc4	čára
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
navázána	navázán	k2eAgFnSc1d1	navázána
vhazováním	vhazování	k1gNnSc7	vhazování
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
provede	provést	k5eAaPmIp3nS	provést
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
míč	míč	k1gInSc4	míč
čáru	čár	k1gInSc2	čár
přešel	přejít	k5eAaPmAgMnS	přejít
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
družstva	družstvo	k1gNnSc2	družstvo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
míče	míč	k1gInSc2	míč
nedotkl	dotknout	k5eNaPmAgInS	dotknout
jako	jako	k9	jako
poslední	poslední	k2eAgInSc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
vhazování	vhazování	k1gNnSc2	vhazování
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Rozehrávající	rozehrávající	k2eAgMnSc1d1	rozehrávající
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
postaví	postavit	k5eAaPmIp3nS	postavit
oběma	dva	k4xCgMnPc7	dva
nohama	noha	k1gFnPc7	noha
za	za	k7c4	za
pomezní	pomezní	k2eAgFnSc4d1	pomezní
čáru	čára	k1gFnSc4	čára
a	a	k8xC	a
oběma	dva	k4xCgFnPc7	dva
rukama	ruka	k1gFnPc7	ruka
hodí	hodit	k5eAaImIp3nP	hodit
přes	přes	k7c4	přes
hlavu	hlava	k1gFnSc4	hlava
míč	míč	k1gInSc1	míč
do	do	k7c2	do
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
vhazování	vhazování	k1gNnSc1	vhazování
provede	provést	k5eAaPmIp3nS	provést
nesprávně	správně	k6eNd1	správně
<g/>
,	,	kIx,	,
provede	provést	k5eAaPmIp3nS	provést
vhazování	vhazování	k1gNnSc1	vhazování
hráč	hráč	k1gMnSc1	hráč
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
míče	míč	k1gInSc2	míč
dotknout	dotknout	k5eAaPmF	dotknout
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
dotkne	dotknout	k5eAaPmIp3nS	dotknout
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
další	další	k2eAgMnSc1d1	další
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Míč	míč	k1gInSc1	míč
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
soupeře	soupeř	k1gMnSc2	soupeř
musí	muset	k5eAaImIp3nP	muset
při	při	k7c6	při
vhazování	vhazování	k1gNnSc6	vhazování
stát	stát	k1gInSc1	stát
nejméně	málo	k6eAd3	málo
2	[number]	k4	2
metry	metr	k1gInPc4	metr
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
vhazování	vhazování	k1gNnSc1	vhazování
provádí	provádět	k5eAaImIp3nS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
přerušena	přerušen	k2eAgFnSc1d1	přerušena
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
míč	míč	k1gInSc1	míč
přešel	přejít	k5eAaPmAgInS	přejít
brankovou	brankový	k2eAgFnSc4d1	branková
čáru	čára	k1gFnSc4	čára
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
naposledy	naposledy	k6eAd1	naposledy
dotkl	dotknout	k5eAaPmAgMnS	dotknout
hráč	hráč	k1gMnSc1	hráč
útočícího	útočící	k2eAgNnSc2d1	útočící
družstva	družstvo	k1gNnSc2	družstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
nebylo	být	k5eNaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
branky	branka	k1gFnPc4	branka
<g/>
,	,	kIx,	,
naváže	navázat	k5eAaPmIp3nS	navázat
se	se	k3xPyFc4	se
hra	hra	k1gFnSc1	hra
kopem	kop	k1gInSc7	kop
od	od	k7c2	od
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
kopu	kop	k1gInSc2	kop
od	od	k7c2	od
branky	branka	k1gFnSc2	branka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
branky	branka	k1gFnPc4	branka
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
vlastní	vlastní	k2eAgFnSc3d1	vlastní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kop	kop	k1gInSc1	kop
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
míč	míč	k1gInSc1	míč
položí	položit	k5eAaPmIp3nS	položit
na	na	k7c4	na
libovolné	libovolný	k2eAgNnSc4d1	libovolné
místo	místo	k1gNnSc4	místo
uvnitř	uvnitř	k7c2	uvnitř
brankového	brankový	k2eAgNnSc2d1	brankové
území	území	k1gNnSc2	území
a	a	k8xC	a
libovolný	libovolný	k2eAgMnSc1d1	libovolný
hráč	hráč	k1gMnSc1	hráč
bránícího	bránící	k2eAgNnSc2d1	bránící
mužstva	mužstvo	k1gNnSc2	mužstvo
jej	on	k3xPp3gMnSc4	on
rozehraje	rozehrát	k5eAaPmIp3nS	rozehrát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
míče	míč	k1gInSc2	míč
podruhé	podruhé	k6eAd1	podruhé
dotknout	dotknout	k5eAaPmF	dotknout
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
dotkne	dotknout	k5eAaPmIp3nS	dotknout
jiný	jiný	k2eAgMnSc1d1	jiný
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
míč	míč	k1gInSc1	míč
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
všichni	všechen	k3xTgMnPc1	všechen
hráči	hráč	k1gMnPc1	hráč
soupeře	soupeř	k1gMnSc4	soupeř
zůstat	zůstat	k5eAaPmF	zůstat
mimo	mimo	k7c4	mimo
pokutové	pokutový	k2eAgNnSc4d1	pokutové
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Míč	míč	k1gInSc1	míč
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
opustí	opustit	k5eAaPmIp3nP	opustit
pokutové	pokutový	k2eAgNnSc4d1	pokutové
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
přerušena	přerušen	k2eAgFnSc1d1	přerušena
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
míč	míč	k1gInSc1	míč
přešel	přejít	k5eAaPmAgInS	přejít
brankovou	brankový	k2eAgFnSc4d1	branková
čáru	čára	k1gFnSc4	čára
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
naposledy	naposledy	k6eAd1	naposledy
dotkl	dotknout	k5eAaPmAgMnS	dotknout
hráč	hráč	k1gMnSc1	hráč
bránícího	bránící	k2eAgNnSc2d1	bránící
družstva	družstvo	k1gNnSc2	družstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
nebylo	být	k5eNaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
branky	branka	k1gFnPc4	branka
<g/>
,	,	kIx,	,
naváže	navázat	k5eAaPmIp3nS	navázat
se	se	k3xPyFc4	se
hra	hra	k1gFnSc1	hra
kopem	kop	k1gInSc7	kop
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
kopu	kop	k1gInSc2	kop
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
branky	branka	k1gFnPc4	branka
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
vlastní	vlastní	k2eAgFnSc3d1	vlastní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kop	kop	k1gInSc1	kop
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
míč	míč	k1gInSc1	míč
položí	položit	k5eAaPmIp3nS	položit
na	na	k7c4	na
libovolné	libovolný	k2eAgNnSc4d1	libovolné
místo	místo	k1gNnSc4	místo
uvnitř	uvnitř	k7c2	uvnitř
rohového	rohový	k2eAgInSc2d1	rohový
čtvrtkruhu	čtvrtkruh	k1gInSc2	čtvrtkruh
u	u	k7c2	u
nejbližšího	blízký	k2eAgInSc2d3	Nejbližší
rohového	rohový	k2eAgInSc2d1	rohový
praporku	praporek	k1gInSc2	praporek
a	a	k8xC	a
libovolný	libovolný	k2eAgMnSc1d1	libovolný
hráč	hráč	k1gMnSc1	hráč
útočícího	útočící	k2eAgNnSc2d1	útočící
mužstva	mužstvo	k1gNnSc2	mužstvo
jej	on	k3xPp3gMnSc4	on
rozehraje	rozehrát	k5eAaPmIp3nS	rozehrát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
míče	míč	k1gInSc2	míč
znovu	znovu	k6eAd1	znovu
dotknout	dotknout	k5eAaPmF	dotknout
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
dotkne	dotknout	k5eAaPmIp3nS	dotknout
jiný	jiný	k2eAgMnSc1d1	jiný
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
míč	míč	k1gInSc1	míč
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
žádný	žádný	k3yNgMnSc1	žádný
hráč	hráč	k1gMnSc1	hráč
soupeře	soupeř	k1gMnSc4	soupeř
přiblížit	přiblížit	k5eAaPmF	přiblížit
k	k	k7c3	k
míči	míč	k1gInSc3	míč
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
9,15	[number]	k4	9,15
m.	m.	k?	m.
Míč	míč	k1gInSc1	míč
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
kopu	kop	k1gInSc2	kop
pohne	pohnout	k5eAaPmIp3nS	pohnout
<g/>
.	.	kIx.	.
</s>
<s>
Fotbal	fotbal	k1gInSc1	fotbal
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
některé	některý	k3yIgInPc4	některý
příbuzné	příbuzný	k2eAgInPc4d1	příbuzný
sporty	sport	k1gInPc4	sport
jako	jako	k9	jako
futsal	futsat	k5eAaPmAgInS	futsat
a	a	k8xC	a
plážový	plážový	k2eAgInSc1d1	plážový
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
spravuje	spravovat	k5eAaImIp3nS	spravovat
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
federace	federace	k1gFnSc1	federace
FIFA	FIFA	kA	FIFA
(	(	kIx(	(
<g/>
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gFnSc2	Internationale
de	de	k?	de
Football	Football	k1gInSc1	Football
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
národní	národní	k2eAgFnPc4d1	národní
fotbalové	fotbalový	k2eAgFnPc4d1	fotbalová
asociace	asociace	k1gFnPc4	asociace
<g/>
.	.	kIx.	.
</s>
<s>
FIFA	FIFA	kA	FIFA
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
regionálních	regionální	k2eAgFnPc2d1	regionální
konfederací	konfederace	k1gFnPc2	konfederace
<g/>
:	:	kIx,	:
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
:	:	kIx,	:
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
Union	union	k1gInSc1	union
of	of	k?	of
European	European	k1gInSc1	European
Football	Football	k1gMnSc1	Football
Associations	Associations	k1gInSc1	Associations
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
:	:	kIx,	:
CONMEBOL	CONMEBOL	kA	CONMEBOL
(	(	kIx(	(
<g/>
Confederación	Confederación	k1gMnSc1	Confederación
Sudamericana	Sudamerican	k1gMnSc2	Sudamerican
de	de	k?	de
Fútbol	Fútbol	k1gInSc1	Fútbol
<g/>
)	)	kIx)	)
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
a	a	k8xC	a
Karibik	Karibik	k1gMnSc1	Karibik
<g/>
:	:	kIx,	:
CONCACAF	CONCACAF	kA	CONCACAF
(	(	kIx(	(
<g/>
Confederation	Confederation	k1gInSc1	Confederation
of	of	k?	of
North	North	k1gInSc1	North
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Central	Central	k1gMnPc7	Central
American	Americany	k1gInPc2	Americany
and	and	k?	and
Caribbean	Caribbean	k1gInSc1	Caribbean
Association	Association	k1gInSc1	Association
Football	Football	k1gInSc1	Football
<g/>
)	)	kIx)	)
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
:	:	kIx,	:
CAF	CAF	kA	CAF
(	(	kIx(	(
<g/>
Confederation	Confederation	k1gInSc1	Confederation
of	of	k?	of
African	African	k1gInSc1	African
Football	Football	k1gInSc1	Football
<g/>
)	)	kIx)	)
Asie	Asie	k1gFnSc1	Asie
<g/>
:	:	kIx,	:
AFC	AFC	kA	AFC
(	(	kIx(	(
<g/>
Asian	Asian	k1gMnSc1	Asian
Football	Football	k1gMnSc1	Football
Confederation	Confederation	k1gInSc1	Confederation
<g/>
)	)	kIx)	)
Oceánie	Oceánie	k1gFnSc1	Oceánie
<g/>
:	:	kIx,	:
OFC	OFC	kA	OFC
(	(	kIx(	(
<g/>
Oceania	Oceanium	k1gNnPc1	Oceanium
Football	Footballa	k1gFnPc2	Footballa
Confederation	Confederation	k1gInSc1	Confederation
<g/>
)	)	kIx)	)
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
či	či	k8xC	či
jejich	jejich	k3xOp3gFnPc6	jejich
částech	část	k1gFnPc6	část
<g/>
)	)	kIx)	)
fotbal	fotbal	k1gInSc4	fotbal
spravují	spravovat	k5eAaImIp3nP	spravovat
příslušné	příslušný	k2eAgFnPc1d1	příslušná
národní	národní	k2eAgFnPc1d1	národní
asociace	asociace	k1gFnPc1	asociace
<g/>
;	;	kIx,	;
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
orgánem	orgán	k1gInSc7	orgán
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
asociace	asociace	k1gFnSc1	asociace
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
FAČR	FAČR	kA	FAČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgNnPc4d1	samotné
pravidla	pravidlo	k1gNnPc4	pravidlo
fotbalu	fotbal	k1gInSc2	fotbal
neudržuje	udržovat	k5eNaImIp3nS	udržovat
přímo	přímo	k6eAd1	přímo
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
komise	komise	k1gFnSc1	komise
IFAB	IFAB	kA	IFAB
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
Football	Football	k1gInSc4	Football
Association	Association	k1gInSc1	Association
Board	Board	k1gInSc1	Board
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fotbalu	fotbal	k1gInSc6	fotbal
probíhá	probíhat	k5eAaImIp3nS	probíhat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
soutěží	soutěž	k1gFnPc2	soutěž
jak	jak	k8xC	jak
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
či	či	k8xC	či
jejich	jejich	k3xOp3gFnPc6	jejich
částech	část	k1gFnPc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
spolu	spolu	k6eAd1	spolu
zápasí	zápasit	k5eAaImIp3nS	zápasit
jednak	jednak	k8xC	jednak
reprezentační	reprezentační	k2eAgNnPc4d1	reprezentační
družstva	družstvo	k1gNnPc4	družstvo
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgFnSc2d1	vlastní
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
soukromé	soukromý	k2eAgInPc4d1	soukromý
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
kluby	klub	k1gInPc4	klub
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
soutěží	soutěž	k1gFnSc7	soutěž
reprezentací	reprezentace	k1gFnSc7	reprezentace
je	být	k5eAaImIp3nS	být
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
turnaji	turnaj	k1gInSc6	turnaj
soutěží	soutěž	k1gFnPc2	soutěž
32	[number]	k4	32
družstev	družstvo	k1gNnPc2	družstvo
<g/>
,	,	kIx,	,
vybraných	vybraný	k2eAgFnPc2d1	vybraná
kvalifikací	kvalifikace	k1gFnPc2	kvalifikace
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
turnaj	turnaj	k1gInSc1	turnaj
je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
programu	program	k1gInSc2	program
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
;	;	kIx,	;
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
však	však	k9	však
tohoto	tento	k3xDgInSc2	tento
turnaje	turnaj	k1gInSc2	turnaj
účastní	účastnit	k5eAaImIp3nP	účastnit
mládežnická	mládežnický	k2eAgNnPc1d1	mládežnické
mužstva	mužstvo	k1gNnPc1	mužstvo
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
turnaj	turnaj	k1gInSc1	turnaj
nemá	mít	k5eNaImIp3nS	mít
tak	tak	k6eAd1	tak
vysokou	vysoký	k2eAgFnSc4d1	vysoká
prestiž	prestiž	k1gFnSc4	prestiž
jako	jako	k8xC	jako
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
reprezentačními	reprezentační	k2eAgFnPc7d1	reprezentační
soutěžemi	soutěž	k1gFnPc7	soutěž
jsou	být	k5eAaImIp3nP	být
regionální	regionální	k2eAgNnSc4d1	regionální
mistrovství	mistrovství	k1gNnSc4	mistrovství
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
konfederacemi	konfederace	k1gFnPc7	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
EURO	euro	k1gNnSc1	euro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Copa	Cop	k2eAgFnSc1d1	Copa
América	América	k1gFnSc1	América
<g/>
,	,	kIx,	,
Africký	africký	k2eAgInSc1d1	africký
pohár	pohár	k1gInSc1	pohár
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Asie	Asie	k1gFnSc2	Asie
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc1d1	zlatý
pohár	pohár	k1gInSc1	pohár
CONCACAF	CONCACAF	kA	CONCACAF
a	a	k8xC	a
Oceánský	oceánský	k2eAgInSc4d1	oceánský
pohár	pohár	k1gInSc4	pohár
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
konfederace	konfederace	k1gFnSc1	konfederace
také	také	k9	také
pořádají	pořádat	k5eAaImIp3nP	pořádat
nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
klubové	klubový	k2eAgFnSc2d1	klubová
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Těch	ten	k3xDgMnPc2	ten
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
vítězové	vítěz	k1gMnPc1	vítěz
národních	národní	k2eAgFnPc2d1	národní
lig	liga	k1gFnPc2	liga
a	a	k8xC	a
pohárů	pohár	k1gInPc2	pohár
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
také	také	k9	také
účastníci	účastník	k1gMnPc1	účastník
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
a	a	k8xC	a
Evropská	evropský	k2eAgFnSc1d1	Evropská
liga	liga	k1gFnSc1	liga
UEFA	UEFA	kA	UEFA
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Pohár	pohár	k1gInSc1	pohár
osvoboditelů	osvoboditel	k1gMnPc2	osvoboditel
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
AFC	AFC	kA	AFC
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
CAF	CAF	kA	CAF
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
CONCACAF	CONCACAF	kA	CONCACAF
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
OFC	OFC	kA	OFC
v	v	k7c6	v
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
a	a	k8xC	a
Poháru	pohár	k1gInSc2	pohár
osvoboditelů	osvoboditel	k1gMnPc2	osvoboditel
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
utkávali	utkávat	k5eAaImAgMnP	utkávat
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
Interkontinentální	interkontinentální	k2eAgInSc4d1	interkontinentální
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
jej	on	k3xPp3gInSc4	on
však	však	k9	však
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
národních	národní	k2eAgFnPc2d1	národní
asociací	asociace	k1gFnPc2	asociace
pořádá	pořádat	k5eAaImIp3nS	pořádat
národní	národní	k2eAgFnPc4d1	národní
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větších	veliký	k2eAgInPc6d2	veliký
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
jedná	jednat	k5eAaImIp3nS	jednat
nejméně	málo	k6eAd3	málo
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
soutěže	soutěž	k1gFnPc4	soutěž
lišící	lišící	k2eAgFnPc4d1	lišící
se	se	k3xPyFc4	se
formátem	formát	k1gInSc7	formát
<g/>
:	:	kIx,	:
jednou	jednou	k6eAd1	jednou
je	být	k5eAaImIp3nS	být
ligová	ligový	k2eAgFnSc1d1	ligová
soutěž	soutěž	k1gFnSc1	soutěž
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
podle	podle	k7c2	podle
výkonnosti	výkonnost	k1gFnSc2	výkonnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
výkonnostní	výkonnostní	k2eAgFnSc6d1	výkonnostní
skupině	skupina	k1gFnSc6	skupina
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
systémem	systém	k1gInSc7	systém
"	"	kIx"	"
<g/>
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
každým	každý	k3xTgNnSc7	každý
<g/>
"	"	kIx"	"
a	a	k8xC	a
o	o	k7c6	o
vítězi	vítěz	k1gMnSc6	vítěz
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
body	bod	k1gInPc4	bod
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
zápasů	zápas	k1gInPc2	zápas
<g/>
;	;	kIx,	;
nejvýše	nejvýše	k6eAd1	nejvýše
umístěné	umístěný	k2eAgInPc1d1	umístěný
týmy	tým	k1gInPc1	tým
mohou	moct	k5eAaImIp3nP	moct
postoupit	postoupit	k5eAaPmF	postoupit
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgMnSc1d3	nejnižší
naopak	naopak	k6eAd1	naopak
sestoupit	sestoupit	k5eAaPmF	sestoupit
o	o	k7c4	o
úroveň	úroveň	k1gFnSc4	úroveň
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
soutěží	soutěž	k1gFnSc7	soutěž
bývá	bývat	k5eAaImIp3nS	bývat
národní	národní	k2eAgInSc4d1	národní
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
za	za	k7c2	za
rovných	rovný	k2eAgFnPc2d1	rovná
podmínek	podmínka	k1gFnPc2	podmínka
mohou	moct	k5eAaImIp3nP	moct
účastnit	účastnit	k5eAaImF	účastnit
všechny	všechen	k3xTgInPc1	všechen
týmy	tým	k1gInPc1	tým
splňující	splňující	k2eAgFnSc4d1	splňující
jistou	jistý	k2eAgFnSc4d1	jistá
minimální	minimální	k2eAgFnSc4d1	minimální
výkonnostní	výkonnostní	k2eAgFnSc4d1	výkonnostní
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
vyřazovacím	vyřazovací	k2eAgInSc7d1	vyřazovací
systémem	systém	k1gInSc7	systém
-	-	kIx~	-
vítěz	vítěz	k1gMnSc1	vítěz
postupuje	postupovat	k5eAaImIp3nS	postupovat
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
kola	kolo	k1gNnSc2	kolo
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prohrál	prohrát	k5eAaPmAgInS	prohrát
<g/>
,	,	kIx,	,
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
ročník	ročník	k1gInSc4	ročník
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
ligovou	ligový	k2eAgFnSc7d1	ligová
soutěží	soutěžit	k5eAaImIp3nS	soutěžit
první	první	k4xOgFnSc1	první
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
MOL	mol	k1gInSc1	mol
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejslavnější	slavný	k2eAgFnPc4d3	nejslavnější
ligové	ligový	k2eAgFnPc4d1	ligová
soutěže	soutěž	k1gFnPc4	soutěž
světa	svět	k1gInSc2	svět
patří	patřit	k5eAaImIp3nS	patřit
anglická	anglický	k2eAgFnSc1d1	anglická
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gInSc2	Leagu
<g/>
,	,	kIx,	,
španělská	španělský	k2eAgFnSc1d1	španělská
Primera	primera	k1gFnSc1	primera
división	división	k1gInSc1	división
<g/>
,	,	kIx,	,
italská	italský	k2eAgFnSc1d1	italská
Serie	serie	k1gFnSc1	serie
A	A	kA	A
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
Bundesliga	bundesliga	k1gFnSc1	bundesliga
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
Ligue	Ligue	k1gFnSc1	Ligue
1	[number]	k4	1
Orange	Orange	k1gFnPc2	Orange
či	či	k8xC	či
brazilská	brazilský	k2eAgFnSc1d1	brazilská
Série	série	k1gFnSc1	série
A.	A.	kA	A.
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgInPc4d1	známý
národní	národní	k2eAgInPc4d1	národní
poháry	pohár	k1gInPc4	pohár
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
anglický	anglický	k2eAgMnSc1d1	anglický
FA	fa	kA	fa
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Soutěže	soutěž	k1gFnPc1	soutěž
mužských	mužský	k2eAgInPc2d1	mužský
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
úrovni	úroveň	k1gFnSc6	úroveň
jsou	být	k5eAaImIp3nP	být
celostátní	celostátní	k2eAgFnPc1d1	celostátní
profesionální	profesionální	k2eAgFnPc1d1	profesionální
soutěže	soutěž	k1gFnPc1	soutěž
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
úrovni	úroveň	k1gFnSc6	úroveň
jsou	být	k5eAaImIp3nP	být
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
IV	IV	kA	IV
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
organizované	organizovaný	k2eAgFnPc1d1	organizovaná
regionálně	regionálně	k6eAd1	regionálně
(	(	kIx(	(
<g/>
okresními	okresní	k2eAgInPc7d1	okresní
fotbalovými	fotbalový	k2eAgInPc7d1	fotbalový
svazy	svaz	k1gInPc7	svaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohárovou	pohárový	k2eAgFnSc7d1	pohárová
soutěží	soutěž	k1gFnSc7	soutěž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
MOL	mol	k1gInSc1	mol
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Pohár	pohár	k1gInSc1	pohár
ČMFS	ČMFS	kA	ČMFS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bernd	Bernd	k1gMnSc1	Bernd
Rohr	Rohr	k1gMnSc1	Rohr
<g/>
,	,	kIx,	,
Günter	Günter	k1gMnSc1	Günter
Simon	Simon	k1gMnSc1	Simon
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
Jeřábek	Jeřábek	k1gMnSc1	Jeřábek
<g/>
:	:	kIx,	:
Fotbal	fotbal	k1gInSc1	fotbal
-	-	kIx~	-
velký	velký	k2eAgInSc1d1	velký
lexikon	lexikon	k1gInSc1	lexikon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Grada	Grada	k1gFnSc1	Grada
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-247-1158-3	[number]	k4	80-247-1158-3
Ian	Ian	k1gFnSc2	Ian
Douglas	Douglasa	k1gFnPc2	Douglasa
<g/>
:	:	kIx,	:
Fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
Fraus	fraus	k1gFnSc1	fraus
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7238-808-0	[number]	k4	978-80-7238-808-0
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Dlabáček	Dlabáček	k1gMnSc1	Dlabáček
<g/>
:	:	kIx,	:
Abeceda	abeceda	k1gFnSc1	abeceda
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Gaudeamus	Gaudeamus	k1gInSc1	Gaudeamus
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7041-922-9	[number]	k4	978-80-7041-922-9
Ivan	Ivan	k1gMnSc1	Ivan
Truchlik	Truchlik	k1gMnSc1	Truchlik
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
,	,	kIx,	,
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7451-245-2	[number]	k4	978-80-7451-245-2
Ragby	ragby	k1gNnPc2	ragby
Australský	australský	k2eAgInSc1d1	australský
fotbal	fotbal	k1gInSc1	fotbal
Americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
Nohejbal	nohejbal	k1gInSc4	nohejbal
Sálová	sálový	k2eAgNnPc4d1	sálové
kopaná	kopané	k1gNnPc4	kopané
Plážový	plážový	k2eAgInSc4d1	plážový
fotbal	fotbal	k1gInSc4	fotbal
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
fotbal	fotbal	k1gInSc1	fotbal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Fotbal	fotbal	k1gInSc1	fotbal
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc4	heslo
fotbal	fotbal	k1gInSc1	fotbal
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kategorie	kategorie	k1gFnSc1	kategorie
Fotbal	fotbal	k1gInSc1	fotbal
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
www.fifa.com	www.fifa.com	k1gInSc1	www.fifa.com
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
federace	federace	k1gFnSc2	federace
FIFA	FIFA	kA	FIFA
www.uefa.com	www.uefa.com	k1gInSc1	www.uefa.com
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
evropské	evropský	k2eAgFnSc2d1	Evropská
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
federace	federace	k1gFnSc2	federace
UEFA	UEFA	kA	UEFA
www.fotbal.cz	www.fotbal.cz	k1gMnSc1	www.fotbal.cz
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
asociace	asociace	k1gFnSc2	asociace
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
FAČR	FAČR	kA	FAČR
<g/>
)	)	kIx)	)
</s>
