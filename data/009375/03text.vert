<p>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
218	[number]	k4	218
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
obce	obec	k1gFnSc2	obec
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
175	[number]	k4	175
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
6	[number]	k4	6
km	km	kA	km
západně	západně	k6eAd1	západně
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
,	,	kIx,	,
11	[number]	k4	11
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
statutární	statutární	k2eAgNnSc4d1	statutární
město	město	k1gNnSc4	město
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
20	[number]	k4	20
km	km	kA	km
východně	východně	k6eAd1	východně
město	město	k1gNnSc1	město
Vysoké	vysoká	k1gFnSc2	vysoká
Mýto	mýto	k1gNnSc1	mýto
a	a	k8xC	a
22	[number]	k4	22
km	km	kA	km
jižně	jižně	k6eAd1	jižně
město	město	k1gNnSc1	město
Hlinsko	Hlinsko	k1gNnSc1	Hlinsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1318	[number]	k4	1318
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
připomíná	připomínat	k5eAaImIp3nS	připomínat
založení	založení	k1gNnSc2	založení
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Majitelé	majitel	k1gMnPc1	majitel
Bezděkova	Bezděkův	k2eAgNnSc2d1	Bezděkovo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
střídali	střídat	k5eAaImAgMnP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
vladykové	vladyka	k1gMnPc1	vladyka
z	z	k7c2	z
Bezděkova	Bezděkův	k2eAgMnSc2d1	Bezděkův
<g/>
,	,	kIx,	,
Malovcové	Malovcový	k2eAgNnSc1d1	Malovcový
z	z	k7c2	z
Chýnova	Chýnov	k1gInSc2	Chýnov
<g/>
,	,	kIx,	,
Slepotičtí	Slepotický	k2eAgMnPc1d1	Slepotický
ze	z	k7c2	z
Sulic	Sulice	k1gInPc2	Sulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
králů	král	k1gMnPc2	král
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
a	a	k8xC	a
Václava	Václav	k1gMnSc4	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
připomíná	připomínat	k5eAaImIp3nS	připomínat
Jarek	Jarek	k1gMnSc1	Jarek
z	z	k7c2	z
Bezděkova	Bezděkův	k2eAgInSc2d1	Bezděkův
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Benešově	Benešov	k1gInSc6	Benešov
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1406	[number]	k4	1406
připadl	připadnout	k5eAaPmAgInS	připadnout
Bezděkov	Bezděkov	k1gInSc4	Bezděkov
jako	jako	k8xS	jako
odúmrť	odúmrť	k1gFnSc4	odúmrť
králi	král	k1gMnSc3	král
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ho	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
Janovi	Jan	k1gMnSc3	Jan
z	z	k7c2	z
Nismburka	Nismburek	k1gMnSc2	Nismburek
k	k	k7c3	k
vyšehradskému	vyšehradský	k2eAgNnSc3d1	Vyšehradské
manství	manství	k1gNnSc3	manství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
patřil	patřit	k5eAaImAgInS	patřit
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
Janovi	Jan	k1gMnSc3	Jan
a	a	k8xC	a
Jiřímu	Jiří	k1gMnSc3	Jiří
z	z	k7c2	z
Rejhradu	Rejhrad	k1gInSc2	Rejhrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
patřil	patřit	k5eAaImAgMnS	patřit
Malovcům	Malovec	k1gMnPc3	Malovec
z	z	k7c2	z
Chýnova	Chýnov	k1gInSc2	Chýnov
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
sourozencům	sourozenec	k1gMnPc3	sourozenec
Petrovi	Petr	k1gMnSc6	Petr
a	a	k8xC	a
Kateřině	Kateřina	k1gFnSc6	Kateřina
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
statek	statek	k1gInSc4	statek
prodali	prodat	k5eAaPmAgMnP	prodat
roku	rok	k1gInSc2	rok
1543	[number]	k4	1543
Petrovi	Petr	k1gMnSc3	Petr
Slepotickému	Slepotický	k2eAgInSc3d1	Slepotický
ze	z	k7c2	z
Sulic	Sulice	k1gInPc2	Sulice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
patřil	patřit	k5eAaImAgInS	patřit
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
<g/>
,	,	kIx,	,
Vejvanovice	Vejvanovice	k1gFnSc1	Vejvanovice
<g/>
,	,	kIx,	,
Třtě	Třtě	k1gMnPc4	Třtě
<g/>
,	,	kIx,	,
Časy	čas	k1gInPc1	čas
<g/>
,	,	kIx,	,
Chatče	Chatec	k1gMnSc5	Chatec
a	a	k8xC	a
Dvakačovice	Dvakačovice	k1gFnPc1	Dvakačovice
Petrovu	Petrův	k2eAgMnSc3d1	Petrův
synovi	syn	k1gMnSc3	syn
Markvartovi	Markvart	k1gMnSc3	Markvart
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
postavit	postavit	k5eAaPmF	postavit
tvrz	tvrz	k1gFnSc4	tvrz
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
zpráva	zpráva	k1gFnSc1	zpráva
z	z	k7c2	z
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
<g/>
.	.	kIx.	.
</s>
<s>
Markvart	Markvart	k1gInSc1	Markvart
tento	tento	k3xDgInSc4	tento
statek	statek	k1gInSc4	statek
zanechal	zanechat	k5eAaPmAgMnS	zanechat
roku	rok	k1gInSc2	rok
1569	[number]	k4	1569
svým	svůj	k3xOyFgInSc7	svůj
dcerám	dcera	k1gFnPc3	dcera
Anně	Anna	k1gFnSc6	Anna
a	a	k8xC	a
Kateřině	Kateřina	k1gFnSc6	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
Kateřiny	Kateřina	k1gFnSc2	Kateřina
s	s	k7c7	s
Jetřichem	Jetřich	k1gMnSc7	Jetřich
mladším	mladý	k2eAgMnSc7d2	mladší
z	z	k7c2	z
Lukavce	Lukavec	k1gMnSc2	Lukavec
Kateřina	Kateřina	k1gFnSc1	Kateřina
statek	statek	k1gInSc1	statek
odkázala	odkázat	k5eAaPmAgFnS	odkázat
svému	svůj	k3xOyFgMnSc3	svůj
muži	muž	k1gMnSc3	muž
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
Janu	Jan	k1gMnSc3	Jan
Lukavskému	Lukavský	k2eAgMnSc3d1	Lukavský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1584	[number]	k4	1584
prodal	prodat	k5eAaPmAgMnS	prodat
Jan	Jan	k1gMnSc1	Jan
Lukavský	Lukavský	k2eAgMnSc1d1	Lukavský
ves	ves	k1gFnSc4	ves
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
s	s	k7c7	s
tvrzí	tvrz	k1gFnSc7	tvrz
<g/>
,	,	kIx,	,
dvorem	dvůr	k1gInSc7	dvůr
<g/>
,	,	kIx,	,
krčmou	krčma	k1gFnSc7	krčma
a	a	k8xC	a
všemi	všecek	k3xTgInPc7	všecek
pozemky	pozemek	k1gInPc7	pozemek
Mikuláši	mikuláš	k1gInPc7	mikuláš
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
a	a	k8xC	a
Nových	Nových	k2eAgInPc2d1	Nových
Hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
majitelem	majitel	k1gMnSc7	majitel
Hrochova	Hrochův	k2eAgInSc2d1	Hrochův
Týnce	Týnec	k1gInSc2	Týnec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
sdílela	sdílet	k5eAaImAgFnS	sdílet
ves	ves	k1gFnSc1	ves
osudy	osud	k1gInPc4	osud
Hrochova	Hrochův	k2eAgInSc2d1	Hrochův
Týnce	Týnec	k1gInSc2	Týnec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
hrochovotýneckého	hrochovotýnecký	k2eAgNnSc2d1	hrochovotýnecký
panství	panství	k1gNnSc2	panství
byl	být	k5eAaImAgInS	být
Bezděkov	Bezděkov	k1gInSc4	Bezděkov
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
měl	mít	k5eAaImAgInS	mít
42	[number]	k4	42
domů	dům	k1gInPc2	dům
a	a	k8xC	a
222	[number]	k4	222
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dvůr	Dvůr	k1gInSc1	Dvůr
panský	panský	k2eAgInSc1d1	panský
byl	být	k5eAaImAgInS	být
rozdělen	rozdělen	k2eAgInSc1d1	rozdělen
familiantům	familiant	k1gMnPc3	familiant
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Bezděkově	Bezděkův	k2eAgFnSc6d1	Bezděkova
55	[number]	k4	55
domů	dům	k1gInPc2	dům
a	a	k8xC	a
352	[number]	k4	352
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
měl	mít	k5eAaImAgInS	mít
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
323	[number]	k4	323
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
75	[number]	k4	75
domech	dům	k1gInPc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
rozloha	rozloha	k1gFnSc1	rozloha
činila	činit	k5eAaImAgFnS	činit
175	[number]	k4	175
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
měla	mít	k5eAaImAgFnS	mít
obec	obec	k1gFnSc1	obec
název	název	k1gInSc4	název
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
období	období	k1gNnSc6	období
bylo	být	k5eAaImAgNnS	být
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
několik	několik	k4yIc4	několik
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
průtahy	průtah	k1gInPc1	průtah
s	s	k7c7	s
doručováním	doručování	k1gNnSc7	doručování
hlavně	hlavně	k6eAd1	hlavně
úředních	úřední	k2eAgInPc2d1	úřední
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
upraven	upravit	k5eAaPmNgMnS	upravit
na	na	k7c4	na
Dolní	dolní	k2eAgInSc4d1	dolní
Bezděkov	Bezděkov	k1gInSc4	Bezděkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
na	na	k7c6	na
okrese	okres	k1gInSc6	okres
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
slučování	slučování	k1gNnSc2	slučování
menších	malý	k2eAgFnPc2d2	menší
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
střediskovými	střediskový	k2eAgFnPc7d1	středisková
sloučena	sloučit	k5eAaPmNgFnS	sloučit
s	s	k7c7	s
obcí	obec	k1gFnSc7	obec
Hrochův	Hrochův	k2eAgInSc1d1	Hrochův
Týnec	Týnec	k1gInSc1	Týnec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
politických	politický	k2eAgFnPc6d1	politická
změnách	změna	k1gFnPc6	změna
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
opět	opět	k6eAd1	opět
plně	plně	k6eAd1	plně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
nejsou	být	k5eNaImIp3nP	být
evidovány	evidovat	k5eAaImNgFnP	evidovat
řádné	řádný	k2eAgFnPc1d1	řádná
nemovité	movitý	k2eNgFnPc1d1	nemovitá
kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
objekty	objekt	k1gInPc1	objekt
s	s	k7c7	s
historickou	historický	k2eAgFnSc7d1	historická
a	a	k8xC	a
architektonickou	architektonický	k2eAgFnSc7d1	architektonická
hodnotou	hodnota	k1gFnSc7	hodnota
místního	místní	k2eAgInSc2d1	místní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
kapli	kaple	k1gFnSc6	kaple
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
křížek	křížek	k1gInSc4	křížek
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Kočí	Kočí	k1gFnSc4	Kočí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
režimu	režim	k1gInSc6	režim
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kaple	kaple	k1gFnSc2	kaple
===	===	k?	===
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
a	a	k8xC	a
zvon	zvon	k1gInSc1	zvon
zakoupen	zakoupen	k2eAgInSc1d1	zakoupen
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
darů	dar	k1gInPc2	dar
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Vysvěcena	vysvěcen	k2eAgFnSc1d1	vysvěcena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zvon	zvon	k1gInSc1	zvon
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1942	[number]	k4	1942
sundán	sundat	k5eAaPmNgInS	sundat
k	k	k7c3	k
válečným	válečný	k2eAgInPc3d1	válečný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zvon	zvon	k1gInSc1	zvon
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
slavnostně	slavnostně	k6eAd1	slavnostně
zavěšen	zavěšen	k2eAgInSc1d1	zavěšen
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
kaple	kaple	k1gFnSc2	kaple
jsou	být	k5eAaImIp3nP	být
osazeny	osazen	k2eAgFnPc4d1	osazena
pamětní	pamětní	k2eAgFnPc4d1	pamětní
desky	deska	k1gFnPc4	deska
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
padlých	padlý	k1gMnPc2	padlý
občanů	občan	k1gMnPc2	občan
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
celkově	celkově	k6eAd1	celkově
opravena	opraven	k2eAgFnSc1d1	opravena
(	(	kIx(	(
<g/>
nová	nový	k2eAgFnSc1d1	nová
fasáda	fasáda	k1gFnSc1	fasáda
<g/>
,	,	kIx,	,
oprava	oprava	k1gFnSc1	oprava
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
prostorů	prostor	k1gInPc2	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opravu	oprava	k1gFnSc4	oprava
hradila	hradit	k5eAaImAgFnS	hradit
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
rozpočtu	rozpočet	k1gInSc2	rozpočet
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byly	být	k5eAaImAgFnP	být
zhotoveny	zhotovit	k5eAaPmNgFnP	zhotovit
nové	nový	k2eAgFnPc1d1	nová
vchodové	vchodový	k2eAgFnPc1d1	vchodová
dveře	dveře	k1gFnPc1	dveře
a	a	k8xC	a
okna	okno	k1gNnPc1	okno
a	a	k8xC	a
opravena	opraven	k2eAgFnSc1d1	opravena
střecha	střecha	k1gFnSc1	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
</s>
</p>
<p>
<s>
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
veřejná	veřejný	k2eAgFnSc1d1	veřejná
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kříž	Kříž	k1gMnSc1	Kříž
===	===	k?	===
</s>
</p>
<p>
<s>
Kříž	kříž	k1gInSc1	kříž
zhotovený	zhotovený	k2eAgInSc1d1	zhotovený
z	z	k7c2	z
pískovce	pískovec	k1gInSc2	pískovec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
za	za	k7c2	za
obcí	obec	k1gFnPc2	obec
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
obec	obec	k1gFnSc4	obec
Kočí	Kočí	k1gFnSc2	Kočí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
ho	on	k3xPp3gInSc4	on
obci	obec	k1gFnSc3	obec
postavil	postavit	k5eAaPmAgInS	postavit
řád	řád	k1gInSc1	řád
Premonstrátů	premonstrát	k1gMnPc2	premonstrát
z	z	k7c2	z
nedalekých	daleký	k2eNgInPc2d1	nedaleký
Slatiňan	Slatiňany	k1gInPc2	Slatiňany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ochranné	ochranný	k2eAgFnSc6d1	ochranná
péči	péče	k1gFnSc6	péče
jsou	být	k5eAaImIp3nP	být
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
i	i	k9	i
blízké	blízký	k2eAgInPc1d1	blízký
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
i	i	k8xC	i
samotné	samotný	k2eAgNnSc1d1	samotné
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
pravidelně	pravidelně	k6eAd1	pravidelně
udržováno	udržován	k2eAgNnSc1d1	udržováno
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
oprava	oprava	k1gFnSc1	oprava
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
SDH	SDH	kA	SDH
Dolní	dolní	k2eAgInSc1d1	dolní
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
==	==	k?	==
</s>
</p>
<p>
<s>
SDH	SDH	kA	SDH
Dolní	dolní	k2eAgInSc1d1	dolní
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
kroužek	kroužek	k1gInSc1	kroužek
malých	malý	k2eAgMnPc2d1	malý
hasičů	hasič	k1gMnPc2	hasič
Bohumilem	Bohumil	k1gMnSc7	Bohumil
Linhartem	Linhart	k1gMnSc7	Linhart
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
obnoven	obnovit	k5eAaPmNgInS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
sboru	sbor	k1gInSc2	sbor
v	v	k7c6	v
Dolním	dolní	k2eAgInSc6d1	dolní
Bezděkově	Bezděkův	k2eAgInSc6d1	Bezděkův
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
5	[number]	k4	5
vedoucích	vedoucí	k1gMnPc2	vedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
SDH	SDH	kA	SDH
Dolní	dolní	k2eAgInSc1d1	dolní
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
pořádá	pořádat	k5eAaImIp3nS	pořádat
tradiční	tradiční	k2eAgInSc1d1	tradiční
hasičský	hasičský	k2eAgInSc1d1	hasičský
ples	ples	k1gInSc1	ples
<g/>
,	,	kIx,	,
dětský	dětský	k2eAgInSc1d1	dětský
karneval	karneval	k1gInSc1	karneval
<g/>
,	,	kIx,	,
pohádkový	pohádkový	k2eAgInSc1d1	pohádkový
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
mikulášskou	mikulášský	k2eAgFnSc4d1	Mikulášská
besídku	besídka	k1gFnSc4	besídka
a	a	k8xC	a
čištění	čištění	k1gNnSc4	čištění
koupaliště	koupaliště	k1gNnSc2	koupaliště
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
Dolní	dolní	k2eAgInSc1d1	dolní
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c2	za
čtvrt	čtvrt	k1gFnSc4	čtvrt
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
výborová	výborový	k2eAgFnSc1d1	výborová
schůze	schůze	k1gFnSc1	schůze
hasičů	hasič	k1gMnPc2	hasič
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
)	)	kIx)	)
výroční	výroční	k2eAgFnSc1d1	výroční
schůze	schůze	k1gFnSc1	schůze
hasičů	hasič	k1gMnPc2	hasič
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
mladším	mladý	k2eAgMnPc3d2	mladší
hasičům	hasič	k1gMnPc3	hasič
založena	založit	k5eAaPmNgFnS	založit
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
www.soptici.banda.cz	www.soptici.banda.cza	k1gFnPc2	www.soptici.banda.cza
Jiřím	Jiří	k1gMnSc7	Jiří
Linhartem	Linhart	k1gMnSc7	Linhart
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
se	s	k7c7	s
SDH	SDH	kA	SDH
Dolní	dolní	k2eAgInSc1d1	dolní
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
okrskových	okrskový	k2eAgNnPc2d1	okrskové
kol	kolo	k1gNnPc2	kolo
hasičských	hasičský	k2eAgInPc2d1	hasičský
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
rodáci	rodák	k1gMnPc1	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Rabas	Rabas	k1gMnSc1	Rabas
-	-	kIx~	-
varhanní	varhanní	k2eAgMnSc1d1	varhanní
virtuos	virtuos	k1gMnSc1	virtuos
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dolní	dolní	k2eAgInSc4d1	dolní
Bezděkov	Bezděkov	k1gInSc4	Bezděkov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc4	obec
Dolní	dolní	k2eAgInSc4d1	dolní
Bezděkov	Bezděkov	k1gInSc4	Bezděkov
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dynaste	Dynasit	k5eAaImRp2nP	Dynasit
rodu	rod	k1gInSc2	rod
Bezděk	Bezděk	k1gMnSc1	Bezděk
a	a	k8xC	a
zajímavosti	zajímavost	k1gFnPc1	zajímavost
okolo	okolo	k7c2	okolo
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
</s>
</p>
