<s>
Algebra	algebra	k1gFnSc1	algebra
=	=	kIx~	=
jak	jak	k8xS	jak
počítat	počítat	k5eAaImF	počítat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc1	odvětví
matematiky	matematika	k1gFnSc2	matematika
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
abstrakcí	abstrakce	k1gFnSc7	abstrakce
pojmů	pojem	k1gInPc2	pojem
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
elementárních	elementární	k2eAgInPc2d1	elementární
matematických	matematický	k2eAgInPc2d1	matematický
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
polynomy	polynom	k1gInPc1	polynom
<g/>
,	,	kIx,	,
matice	matice	k1gFnPc1	matice
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Historicky	historicky	k6eAd1	historicky
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
elementární	elementární	k2eAgFnSc4d1	elementární
algebru	algebra	k1gFnSc4	algebra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
konkrétních	konkrétní	k2eAgMnPc2d1	konkrétní
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
zabývala	zabývat	k5eAaImAgFnS	zabývat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
manipulací	manipulace	k1gFnPc2	manipulace
s	s	k7c7	s
výrazy	výraz	k1gInPc7	výraz
a	a	k8xC	a
řešením	řešení	k1gNnSc7	řešení
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Abstraktní	abstraktní	k2eAgFnSc1d1	abstraktní
algebra	algebra	k1gFnSc1	algebra
(	(	kIx(	(
<g/>
též	též	k9	též
moderní	moderní	k2eAgFnSc1d1	moderní
algebra	algebra	k1gFnSc1	algebra
<g/>
)	)	kIx)	)
studuje	studovat	k5eAaImIp3nS	studovat
obecné	obecný	k2eAgFnPc4d1	obecná
algebraické	algebraický	k2eAgFnPc4d1	algebraická
struktury	struktura	k1gFnPc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
algebra	algebra	k1gFnSc1	algebra
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
ا	ا	k?	ا
(	(	kIx(	(
<g/>
al-džabr	alžabr	k1gMnSc1	al-džabr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
přejato	přejmout	k5eAaPmNgNnS	přejmout
z	z	k7c2	z
názvu	název	k1gInSc2	název
knihy	kniha	k1gFnSc2	kniha
al-Kitáb	al-Kitáb	k1gMnSc1	al-Kitáb
al-Džabr	al-Džabr	k1gMnSc1	al-Džabr
wa-l-Muqabala	wa-Muqabal	k1gMnSc2	wa-l-Muqabal
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c4	o
počítání	počítání	k1gNnSc4	počítání
pomocí	pomocí	k7c2	pomocí
doplňování	doplňování	k1gNnSc2	doplňování
a	a	k8xC	a
vyrovnávání	vyrovnávání	k1gNnSc2	vyrovnávání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
perského	perský	k2eAgMnSc2d1	perský
matematika	matematik	k1gMnSc2	matematik
Muhammada	Muhammada	k1gFnSc1	Muhammada
al-Chwā	al-Chwā	k?	al-Chwā
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
obecný	obecný	k2eAgInSc4d1	obecný
postup	postup	k1gInSc4	postup
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
lineárních	lineární	k2eAgFnPc2d1	lineární
a	a	k8xC	a
kvadratických	kvadratický	k2eAgFnPc2d1	kvadratická
rovnic	rovnice	k1gFnPc2	rovnice
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
neznámých	známý	k2eNgMnPc2d1	neznámý
<g/>
)	)	kIx)	)
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
operací	operace	k1gFnPc2	operace
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
symboly	symbol	k1gInPc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Algebra	algebra	k1gFnSc1	algebra
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
aritmetiky	aritmetika	k1gFnSc2	aritmetika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
otce	otec	k1gMnSc2	otec
oboru	obora	k1gFnSc4	obora
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
perského	perský	k2eAgMnSc4d1	perský
matematika	matematik	k1gMnSc4	matematik
Al-Chorezmího	Al-Chorezmí	k1gMnSc4	Al-Chorezmí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
poprvé	poprvé	k6eAd1	poprvé
formuloval	formulovat	k5eAaImAgMnS	formulovat
obecný	obecný	k2eAgInSc4d1	obecný
postup	postup	k1gInSc4	postup
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
kvadratických	kvadratický	k2eAgFnPc2d1	kvadratická
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
algebrou	algebra	k1gFnSc7	algebra
rozuměla	rozumět	k5eAaImAgFnS	rozumět
teorie	teorie	k1gFnSc1	teorie
řešení	řešení	k1gNnSc2	řešení
rovnic	rovnice	k1gFnPc2	rovnice
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
polynomiálních	polynomiální	k2eAgNnPc2d1	polynomiální
<g/>
)	)	kIx)	)
a	a	k8xC	a
symbolická	symbolický	k2eAgFnSc1d1	symbolická
manipulace	manipulace	k1gFnSc1	manipulace
s	s	k7c7	s
výrazy	výraz	k1gInPc7	výraz
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
algebry	algebra	k1gFnSc2	algebra
nazýváme	nazývat	k5eAaImIp1nP	nazývat
elementární	elementární	k2eAgInPc1d1	elementární
algebrou	algebra	k1gFnSc7	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgInPc7d1	důležitý
mezníky	mezník	k1gInPc7	mezník
teorie	teorie	k1gFnSc2	teorie
rovnic	rovnice	k1gFnPc2	rovnice
bylo	být	k5eAaImAgNnS	být
nalezení	nalezení	k1gNnSc4	nalezení
postupů	postup	k1gInPc2	postup
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
kubických	kubický	k2eAgFnPc2d1	kubická
a	a	k8xC	a
kvartických	kvartický	k2eAgFnPc2d1	kvartický
rovnic	rovnice	k1gFnPc2	rovnice
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
přelom	přelom	k1gInSc4	přelom
mezi	mezi	k7c7	mezi
elementární	elementární	k2eAgFnSc7d1	elementární
a	a	k8xC	a
abstraktní	abstraktní	k2eAgFnSc7d1	abstraktní
algebrou	algebra	k1gFnSc7	algebra
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
práci	práce	k1gFnSc4	práce
francouzského	francouzský	k2eAgMnSc2d1	francouzský
matematika	matematik	k1gMnSc2	matematik
Évarista	Évarista	k1gMnSc1	Évarista
Galoise	Galoise	k1gFnSc1	Galoise
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
Galois	Galois	k1gFnSc6	Galois
elegantně	elegantně	k6eAd1	elegantně
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
neexistuje	existovat	k5eNaImIp3nS	existovat
vzorec	vzorec	k1gInSc4	vzorec
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
rovnic	rovnice	k1gFnPc2	rovnice
pátého	pátý	k4xOgInSc2	pátý
a	a	k8xC	a
vyššího	vysoký	k2eAgInSc2d2	vyšší
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
algebra	algebra	k1gFnSc1	algebra
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
terminologii	terminologie	k1gFnSc4	terminologie
byla	být	k5eAaImAgFnS	být
definována	definovat	k5eAaBmNgFnS	definovat
přelomovou	přelomový	k2eAgFnSc7d1	přelomová
knihou	kniha	k1gFnSc7	kniha
Moderne	Modern	k1gInSc5	Modern
algebra	algebra	k1gFnSc1	algebra
německého	německý	k2eAgMnSc2d1	německý
matematika	matematik	k1gMnSc2	matematik
Bartela	Bartela	k1gFnSc1	Bartela
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Waerdena	Waerden	k2eAgMnSc4d1	Waerden
<g/>
.	.	kIx.	.
</s>
<s>
Algebra	algebra	k1gFnSc1	algebra
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
široký	široký	k2eAgInSc4d1	široký
obor	obor	k1gInSc4	obor
a	a	k8xC	a
člení	členit	k5eAaImIp3nS	členit
se	se	k3xPyFc4	se
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
disciplín	disciplína	k1gFnPc2	disciplína
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
motivací	motivace	k1gFnSc7	motivace
<g/>
,	,	kIx,	,
různým	různý	k2eAgInSc7d1	různý
cílem	cíl	k1gInSc7	cíl
a	a	k8xC	a
různými	různý	k2eAgFnPc7d1	různá
metodami	metoda	k1gFnPc7	metoda
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgFnSc1d1	elementární
algebra	algebra	k1gFnSc1	algebra
–	–	k?	–
elementární	elementární	k2eAgFnSc7d1	elementární
algebrou	algebra	k1gFnSc7	algebra
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
rozumí	rozumět	k5eAaImIp3nS	rozumět
zejména	zejména	k9	zejména
symbolická	symbolický	k2eAgFnSc1d1	symbolická
manipulace	manipulace	k1gFnSc1	manipulace
s	s	k7c7	s
výrazy	výraz	k1gInPc7	výraz
(	(	kIx(	(
<g/>
tak	tak	k9	tak
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
např.	např.	kA	např.
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
a	a	k8xC	a
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
<g/>
)	)	kIx)	)
Lineární	lineární	k2eAgFnSc1d1	lineární
algebra	algebra	k1gFnSc1	algebra
–	–	k?	–
motivací	motivace	k1gFnPc2	motivace
je	být	k5eAaImIp3nS	být
lineární	lineární	k2eAgFnSc1d1	lineární
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
,	,	kIx,	,
objekty	objekt	k1gInPc1	objekt
studia	studio	k1gNnSc2	studio
vektorové	vektorový	k2eAgFnSc2d1	vektorová
prostory	prostora	k1gFnSc2	prostora
<g/>
,	,	kIx,	,
matice	matice	k1gFnSc1	matice
atd.	atd.	kA	atd.
Komutativní	komutativní	k2eAgFnSc1d1	komutativní
algebra	algebra	k1gFnSc1	algebra
–	–	k?	–
motivací	motivace	k1gFnPc2	motivace
jsou	být	k5eAaImIp3nP	být
společné	společný	k2eAgInPc1d1	společný
<g />
.	.	kIx.	.
</s>
<s>
vlastnosti	vlastnost	k1gFnPc1	vlastnost
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
polynomů	polynom	k1gInPc2	polynom
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
objekty	objekt	k1gInPc7	objekt
studia	studio	k1gNnSc2	studio
okruhy	okruh	k1gInPc4	okruh
<g/>
,	,	kIx,	,
obory	obor	k1gInPc4	obor
integrity	integrita	k1gFnSc2	integrita
<g/>
,	,	kIx,	,
tělesa	těleso	k1gNnSc2	těleso
atd.	atd.	kA	atd.
Teorie	teorie	k1gFnSc1	teorie
grup	grupa	k1gFnPc2	grupa
–	–	k?	–
motivací	motivace	k1gFnPc2	motivace
jsou	být	k5eAaImIp3nP	být
společné	společný	k2eAgFnPc4d1	společná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
regulárních	regulární	k2eAgFnPc2d1	regulární
matic	matice	k1gFnPc2	matice
<g/>
,	,	kIx,	,
permutací	permutace	k1gFnPc2	permutace
<g/>
,	,	kIx,	,
geometrických	geometrický	k2eAgInPc2d1	geometrický
symetrií	symetrie	k1gFnSc7	symetrie
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
základním	základní	k2eAgInSc7d1	základní
objektem	objekt	k1gInSc7	objekt
je	být	k5eAaImIp3nS	být
grupa	grupa	k1gFnSc1	grupa
Teorie	teorie	k1gFnSc2	teorie
reprezentací	reprezentace	k1gFnPc2	reprezentace
–	–	k?	–
motivací	motivace	k1gFnPc2	motivace
jsou	být	k5eAaImIp3nP	být
abstraktní	abstraktní	k2eAgFnPc4d1	abstraktní
geometrické	geometrický	k2eAgFnPc4d1	geometrická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
symetrií	symetrie	k1gFnPc2	symetrie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
lineární	lineární	k2eAgFnSc2d1	lineární
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
,	,	kIx,	,
základním	základní	k2eAgInSc7d1	základní
objektem	objekt	k1gInSc7	objekt
je	být	k5eAaImIp3nS	být
okruh	okruh	k1gInSc1	okruh
a	a	k8xC	a
modul	modul	k1gInSc1	modul
Univerzální	univerzální	k2eAgFnSc1d1	univerzální
algebra	algebra	k1gFnSc1	algebra
–	–	k?	–
motivací	motivace	k1gFnPc2	motivace
jsou	být	k5eAaImIp3nP	být
společné	společný	k2eAgFnPc4d1	společná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
algebraických	algebraický	k2eAgFnPc2d1	algebraická
struktur	struktura	k1gFnPc2	struktura
atd.	atd.	kA	atd.
Algebra	algebra	k1gFnSc1	algebra
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
aplikací	aplikace	k1gFnPc2	aplikace
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
i	i	k8xC	i
jiných	jiný	k2eAgFnPc6d1	jiná
vědách	věda	k1gFnPc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
hraničních	hraniční	k2eAgFnPc2d1	hraniční
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Algebraická	algebraický	k2eAgFnSc1d1	algebraická
geometrie	geometrie	k1gFnSc1	geometrie
-	-	kIx~	-
předmětem	předmět	k1gInSc7	předmět
studia	studio	k1gNnSc2	studio
jsou	být	k5eAaImIp3nP	být
geometrické	geometrický	k2eAgInPc1d1	geometrický
objekty	objekt	k1gInPc1	objekt
definované	definovaný	k2eAgInPc1d1	definovaný
pomocí	pomocí	k7c2	pomocí
polynomiálních	polynomiální	k2eAgFnPc2d1	polynomiální
rovnic	rovnice	k1gFnPc2	rovnice
Algebraická	algebraický	k2eAgFnSc1d1	algebraická
topologie	topologie	k1gFnSc1	topologie
-	-	kIx~	-
využití	využití	k1gNnSc1	využití
algebraických	algebraický	k2eAgFnPc2d1	algebraická
metod	metoda	k1gFnPc2	metoda
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
homotopických	homotopický	k2eAgInPc2d1	homotopický
invariantů	invariant	k1gInPc2	invariant
topologických	topologický	k2eAgInPc2d1	topologický
prostorů	prostor	k1gInPc2	prostor
Algebraická	algebraický	k2eAgFnSc1d1	algebraická
teorie	teorie	k1gFnSc1	teorie
čísel	číslo	k1gNnPc2	číslo
-	-	kIx~	-
aplikace	aplikace	k1gFnSc2	aplikace
komutativní	komutativní	k2eAgFnSc2d1	komutativní
algebry	algebra	k1gFnSc2	algebra
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
číselné	číselný	k2eAgInPc4d1	číselný
obory	obor	k1gInPc4	obor
Algebraická	algebraický	k2eAgFnSc1d1	algebraická
kombinatorika	kombinatorika	k1gFnSc1	kombinatorika
Výpočetní	výpočetní	k2eAgFnSc1d1	výpočetní
algebra	algebra	k1gFnSc1	algebra
Mezi	mezi	k7c4	mezi
vědy	věda	k1gFnPc4	věda
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
které	který	k3yRgInPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
algebracké	algebracký	k2eAgInPc1d1	algebracký
výsledky	výsledek	k1gInPc1	výsledek
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
zcela	zcela	k6eAd1	zcela
jistě	jistě	k9	jistě
fyzika	fyzika	k1gFnSc1	fyzika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
výsledky	výsledek	k1gInPc1	výsledek
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
symetrií	symetrie	k1gFnPc2	symetrie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
informatika	informatika	k1gFnSc1	informatika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
abstraktní	abstraktní	k2eAgFnSc1d1	abstraktní
specifikace	specifikace	k1gFnSc1	specifikace
databází	databáze	k1gFnPc2	databáze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kryptografie	kryptografie	k1gFnSc1	kryptografie
(	(	kIx(	(
<g/>
kryptosystémy	kryptosystém	k1gInPc1	kryptosystém
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
eliptických	eliptický	k2eAgFnPc6d1	eliptická
křivkách	křivka	k1gFnPc6	křivka
<g/>
,	,	kIx,	,
algebraická	algebraický	k2eAgFnSc1d1	algebraická
kryptoanalýza	kryptoanalýza	k1gFnSc1	kryptoanalýza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
biologie	biologie	k1gFnSc1	biologie
(	(	kIx(	(
<g/>
využití	využití	k1gNnSc1	využití
v	v	k7c6	v
sekvenční	sekvenční	k2eAgFnSc6d1	sekvenční
analýze	analýza	k1gFnSc6	analýza
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
