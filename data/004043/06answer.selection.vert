<s>
Algebra	algebra	k1gFnSc1	algebra
=	=	kIx~	=
jak	jak	k8xC	jak
počítat	počítat	k5eAaImF	počítat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc1	odvětví
matematiky	matematika	k1gFnSc2	matematika
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
abstrakcí	abstrakce	k1gFnSc7	abstrakce
pojmů	pojem	k1gInPc2	pojem
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
elementárních	elementární	k2eAgInPc2d1	elementární
matematických	matematický	k2eAgInPc2d1	matematický
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
polynomy	polynom	k1gInPc1	polynom
<g/>
,	,	kIx,	,
matice	matice	k1gFnPc1	matice
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Historicky	historicky	k6eAd1	historicky
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
elementární	elementární	k2eAgFnSc4d1	elementární
algebru	algebra	k1gFnSc4	algebra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
konkrétních	konkrétní	k2eAgMnPc2d1	konkrétní
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
zabývala	zabývat	k5eAaImAgFnS	zabývat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
manipulací	manipulace	k1gFnPc2	manipulace
s	s	k7c7	s
výrazy	výraz	k1gInPc7	výraz
a	a	k8xC	a
řešením	řešení	k1gNnSc7	řešení
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
