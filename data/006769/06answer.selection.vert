<s>
Nejúčinnějším	účinný	k2eAgInSc7d3	nejúčinnější
a	a	k8xC	a
nejhrůznějším	hrůzný	k2eAgInSc7d3	nejhrůznější
prostředkem	prostředek	k1gInSc7	prostředek
německého	německý	k2eAgInSc2d1	německý
holocaustu	holocaust	k1gInSc2	holocaust
byly	být	k5eAaImAgInP	být
vyhlazovací	vyhlazovací	k2eAgInPc1d1	vyhlazovací
tábory	tábor	k1gInPc1	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hromadně	hromadně	k6eAd1	hromadně
popravovalo	popravovat	k5eAaImAgNnS	popravovat
(	(	kIx(	(
<g/>
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
fázích	fáze	k1gFnPc6	fáze
<g/>
)	)	kIx)	)
zastřelením	zastřelení	k1gNnSc7	zastřelení
<g/>
,	,	kIx,	,
oběšením	oběšení	k1gNnSc7	oběšení
a	a	k8xC	a
jedovatým	jedovatý	k2eAgInSc7d1	jedovatý
plynem	plyn	k1gInSc7	plyn
(	(	kIx(	(
<g/>
nejpoužívanější	používaný	k2eAgInSc1d3	nejpoužívanější
byl	být	k5eAaImAgInS	být
Cyklon	cyklon	k1gInSc1	cyklon
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
