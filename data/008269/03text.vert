<p>
<s>
Uprchlík	uprchlík	k1gMnSc1	uprchlík
na	na	k7c6	na
ptačím	ptačí	k2eAgInSc6d1	ptačí
stromě	strom	k1gInSc6	strom
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
a	a	k8xC	a
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Ondřej	Ondřej	k1gMnSc1	Ondřej
Sekora	Sekor	k1gMnSc2	Sekor
<g/>
.	.	kIx.	.
</s>
<s>
Autorovi	autor	k1gMnSc3	autor
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zajímavě	zajímavě	k6eAd1	zajímavě
sloučit	sloučit	k5eAaPmF	sloučit
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
životě	život	k1gInSc6	život
ptáků	pták	k1gMnPc2	pták
s	s	k7c7	s
napínavým	napínavý	k2eAgInSc7d1	napínavý
příběhem	příběh	k1gInSc7	příběh
o	o	k7c6	o
přátelství	přátelství	k1gNnSc6	přátelství
a	a	k8xC	a
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnPc4	jeho
nejzdařilejší	zdařilý	k2eAgNnPc4d3	nejzdařilejší
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
Expo	Expo	k1gNnSc1	Expo
58	[number]	k4	58
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
ji	on	k3xPp3gFnSc4	on
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Josef	Josef	k1gMnSc1	Josef
Hokr	Hokr	k1gMnSc1	Hokr
v	v	k7c6	v
r.	r.	kA	r.
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
prozatím	prozatím	k6eAd1	prozatím
naposledy	naposledy	k6eAd1	naposledy
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
r.	r.	kA	r.
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Shrnutí	shrnutí	k1gNnSc2	shrnutí
příběhu	příběh	k1gInSc2	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Chovateli	chovatel	k1gMnSc3	chovatel
cizokrajného	cizokrajný	k2eAgNnSc2d1	cizokrajné
ptactva	ptactvo	k1gNnSc2	ptactvo
<g/>
,	,	kIx,	,
panu	pan	k1gMnSc3	pan
Tichému	Tichý	k1gMnSc3	Tichý
<g/>
,	,	kIx,	,
nedopatřením	nedopatření	k1gNnSc7	nedopatření
uletí	uletět	k5eAaPmIp3nS	uletět
z	z	k7c2	z
klece	klec	k1gFnSc2	klec
jeden	jeden	k4xCgMnSc1	jeden
malý	malý	k1gMnSc1	malý
modroušek	modroušek	k1gMnSc1	modroušek
<g/>
.	.	kIx.	.
</s>
<s>
Dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
ptačí	ptačí	k2eAgInSc4d1	ptačí
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgMnPc2d1	různý
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
–	–	k?	–
starostliví	starostlivý	k2eAgMnPc1d1	starostlivý
brhlíci	brhlík	k1gMnPc1	brhlík
<g/>
,	,	kIx,	,
silák	silák	k1gMnSc1	silák
strakapoud	strakapoud	k1gMnSc1	strakapoud
<g/>
,	,	kIx,	,
vtipálek	vtipálek	k1gMnSc1	vtipálek
zvonek	zvonek	k1gInSc1	zvonek
<g/>
,	,	kIx,	,
křehká	křehký	k2eAgFnSc1d1	křehká
červenka	červenka	k1gFnSc1	červenka
<g/>
,	,	kIx,	,
pyšná	pyšný	k2eAgFnSc1d1	pyšná
pěnice	pěnice	k1gFnSc1	pěnice
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
hlavním	hlavní	k2eAgMnSc7d1	hlavní
pomocníkem	pomocník	k1gMnSc7	pomocník
a	a	k8xC	a
poradcem	poradce	k1gMnSc7	poradce
je	být	k5eAaImIp3nS	být
Kubík	Kubík	k1gMnSc1	Kubík
–	–	k?	–
velký	velký	k2eAgMnSc1d1	velký
zelený	zelený	k2eAgMnSc1d1	zelený
papoušek	papoušek	k1gMnSc1	papoušek
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
a	a	k8xC	a
po	po	k7c6	po
mnohých	mnohý	k2eAgNnPc6d1	mnohé
cestováních	cestování	k1gNnPc6	cestování
žije	žít	k5eAaImIp3nS	žít
nedaleko	daleko	k6eNd1	daleko
ptačího	ptačí	k2eAgInSc2d1	ptačí
stromu	strom	k1gInSc2	strom
u	u	k7c2	u
zahradníka	zahradník	k1gMnSc2	zahradník
Moulíka	Moulík	k1gMnSc2	Moulík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
nechává	nechávat	k5eAaImIp3nS	nechávat
volně	volně	k6eAd1	volně
poletovat	poletovat	k5eAaImF	poletovat
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
Modrouška	Modroušek	k1gMnSc2	Modroušek
přijmou	přijmout	k5eAaPmIp3nP	přijmout
mezi	mezi	k7c4	mezi
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
prožije	prožít	k5eAaPmIp3nS	prožít
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
celé	celý	k2eAgNnSc1d1	celé
léto	léto	k1gNnSc4	léto
a	a	k8xC	a
mnohá	mnohý	k2eAgNnPc4d1	mnohé
veselá	veselý	k2eAgNnPc4d1	veselé
i	i	k8xC	i
nebezpečná	bezpečný	k2eNgNnPc4d1	nebezpečné
dobrodružství	dobrodružství	k1gNnPc4	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přijde	přijít	k5eAaPmIp3nS	přijít
podzim	podzim	k1gInSc1	podzim
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
odlétnou	odlétnout	k5eAaPmIp3nP	odlétnout
do	do	k7c2	do
teplých	teplý	k2eAgFnPc2d1	teplá
krajin	krajina	k1gFnPc2	krajina
a	a	k8xC	a
Modroušek	Modroušek	k1gMnSc1	Modroušek
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
obtížněji	obtížně	k6eAd2	obtížně
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
se	s	k7c7	s
zhoršujícím	zhoršující	k2eAgMnSc7d1	zhoršující
se	se	k3xPyFc4	se
počasím	počasí	k1gNnSc7	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
jej	on	k3xPp3gMnSc4	on
ornitolog	ornitolog	k1gMnSc1	ornitolog
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zná	znát	k5eAaImIp3nS	znát
pana	pan	k1gMnSc4	pan
Tichého	Tichý	k1gMnSc4	Tichý
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
Modrouška	Modroušek	k1gMnSc4	Modroušek
odchytí	odchytit	k5eAaPmIp3nP	odchytit
a	a	k8xC	a
odvezou	odvézt	k5eAaPmIp3nP	odvézt
zpět	zpět	k6eAd1	zpět
domů	dům	k1gInPc2	dům
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
klece	klec	k1gFnSc2	klec
<g/>
.	.	kIx.	.
</s>
<s>
Modroušek	Modroušek	k1gMnSc1	Modroušek
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgMnPc7d1	ostatní
ptáky	pták	k1gMnPc7	pták
pana	pan	k1gMnSc2	pan
Tichého	Tichý	k1gMnSc2	Tichý
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
prožitých	prožitý	k2eAgNnPc6d1	prožité
dobrodružstvích	dobrodružství	k1gNnPc6	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
překvapení	překvapení	k1gNnSc3	překvapení
přiveze	přivézt	k5eAaPmIp3nS	přivézt
panu	pan	k1gMnSc3	pan
Tichému	Tichý	k1gMnSc3	Tichý
zahradník	zahradník	k1gMnSc1	zahradník
Moulík	Moulík	k1gMnSc1	Moulík
papouška	papoušek	k1gMnSc4	papoušek
Kubíka	Kubík	k1gMnSc4	Kubík
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
už	už	k6eAd1	už
nemůže	moct	k5eNaImIp3nS	moct
žít	žít	k5eAaImF	žít
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
denní	denní	k2eAgFnSc4d1	denní
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Modrouškovi	Modroušek	k1gMnSc3	Modroušek
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vrací	vracet	k5eAaImIp3nS	vracet
starý	starý	k2eAgMnSc1d1	starý
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
ptačí	ptačí	k2eAgFnSc1d1	ptačí
klec	klec	k1gFnSc1	klec
získá	získat	k5eAaPmIp3nS	získat
zábavného	zábavný	k2eAgNnSc2d1	zábavné
a	a	k8xC	a
moudrého	moudrý	k2eAgMnSc2d1	moudrý
společníka	společník	k1gMnSc2	společník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Sekora	Sekor	k1gMnSc2	Sekor
</s>
</p>
