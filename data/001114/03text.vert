<s>
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1808	[number]	k4	1808
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
úředních	úřední	k2eAgInPc6d1	úřední
záznamech	záznam	k1gInPc6	záznam
psán	psán	k2eAgInSc1d1	psán
též	též	k9	též
jako	jako	k9	jako
Till	Till	k1gInSc4	Till
<g/>
,	,	kIx,	,
Tille	Tille	k1gInSc4	Tille
<g/>
,	,	kIx,	,
Tylli	Tylle	k1gFnSc4	Tylle
nebo	nebo	k8xC	nebo
Týl	týl	k1gInSc4	týl
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
hudebníkem	hudebník	k1gMnSc7	hudebník
u	u	k7c2	u
28	[number]	k4	28
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Barbora	Barbora	k1gFnSc1	Barbora
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
mlynáře	mlynář	k1gMnSc2	mlynář
Ignáce	Ignác	k1gMnSc2	Ignác
Králíka	Králík	k1gMnSc2	Králík
<g/>
,	,	kIx,	,
vzali	vzít	k5eAaPmAgMnP	vzít
se	s	k7c7	s
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1807	[number]	k4	1807
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
odešel	odejít	k5eAaPmAgMnS	odejít
od	od	k7c2	od
vojska	vojsko	k1gNnSc2	vojsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
krejčař	krejčař	k1gMnSc1	krejčař
<g/>
.	.	kIx.	.
</s>
<s>
Rodné	rodný	k2eAgNnSc1d1	rodné
jméno	jméno	k1gNnSc1	jméno
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Till	Till	k1gMnSc1	Till
změnil	změnit	k5eAaPmAgMnS	změnit
na	na	k7c4	na
"	"	kIx"	"
<g/>
Týl	týl	k1gInSc4	týl
<g/>
"	"	kIx"	"
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
a	a	k8xC	a
na	na	k7c4	na
"	"	kIx"	"
<g/>
Tyl	tyl	k1gInSc4	tyl
<g/>
"	"	kIx"	"
až	až	k9	až
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
studoval	studovat	k5eAaImAgMnS	studovat
staroměstské	staroměstský	k2eAgNnSc4d1	Staroměstské
Akademické	akademický	k2eAgNnSc4d1	akademické
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
roku	rok	k1gInSc6	rok
1827	[number]	k4	1827
odešel	odejít	k5eAaPmAgMnS	odejít
pokračovat	pokračovat	k5eAaImF	pokračovat
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Klimentem	Kliment	k1gMnSc7	Kliment
Klicperou	Klicpera	k1gMnSc7	Klicpera
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
gymnázia	gymnázium	k1gNnSc2	gymnázium
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
Filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1	Karlo-Ferdinandova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc4	studium
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
Hilmerovy	Hilmerův	k2eAgFnSc2d1	Hilmerův
kočovné	kočovný	k2eAgFnSc2d1	kočovná
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
blíže	blízce	k6eAd2	blízce
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Magdalenou	Magdalena	k1gFnSc7	Magdalena
Forchheimovou	Forchheimová	k1gFnSc7	Forchheimová
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
-	-	kIx~	-
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečkou	herečka	k1gFnSc7	herečka
a	a	k8xC	a
operní	operní	k2eAgFnSc7d1	operní
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
působení	působení	k1gNnSc2	působení
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
získal	získat	k5eAaPmAgMnS	získat
místo	místo	k7c2	místo
účetního	účetní	k1gMnSc2	účetní
(	(	kIx(	(
<g/>
fourier	fourier	k1gMnSc1	fourier
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
kanceláři	kancelář	k1gFnSc6	kancelář
28	[number]	k4	28
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
věnoval	věnovat	k5eAaPmAgMnS	věnovat
divadlu	divadlo	k1gNnSc3	divadlo
a	a	k8xC	a
novinařině	novinařina	k1gFnSc3	novinařina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
prakticky	prakticky	k6eAd1	prakticky
vedl	vést	k5eAaImAgInS	vést
redakci	redakce	k1gFnSc4	redakce
časopisu	časopis	k1gInSc2	časopis
Jindy	jindy	k6eAd1	jindy
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
časopis	časopis	k1gInSc1	časopis
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
změnil	změnit	k5eAaPmAgMnS	změnit
na	na	k7c4	na
Květy	Květa	k1gFnPc4	Květa
české	český	k2eAgFnPc4d1	Česká
a	a	k8xC	a
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
zjednodušil	zjednodušit	k5eAaPmAgMnS	zjednodušit
na	na	k7c4	na
Květy	Květa	k1gFnPc4	Květa
<g/>
.	.	kIx.	.
</s>
<s>
Redigoval	redigovat	k5eAaImAgMnS	redigovat
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1840	[number]	k4	1840
<g/>
-	-	kIx~	-
<g/>
1845	[number]	k4	1845
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vydával	vydávat	k5eAaImAgInS	vydávat
časopis	časopis	k1gInSc1	časopis
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
-	-	kIx~	-
<g/>
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1846	[number]	k4	1846
<g/>
-	-	kIx~	-
<g/>
1849	[number]	k4	1849
redigoval	redigovat	k5eAaImAgMnS	redigovat
Pražského	pražský	k2eAgMnSc4d1	pražský
Posla	posel	k1gMnSc4	posel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
založit	založit	k5eAaPmF	založit
noviny	novina	k1gFnPc4	novina
pro	pro	k7c4	pro
venkov	venkov	k1gInSc4	venkov
-	-	kIx~	-
Sedlské	sedlský	k2eAgFnPc1d1	sedlská
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
pokus	pokus	k1gInSc1	pokus
brzy	brzy	k6eAd1	brzy
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Slavojem	Slavoj	k1gMnSc7	Slavoj
Amerlingem	Amerling	k1gInSc7	Amerling
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Dittrichem	Dittrich	k1gMnSc7	Dittrich
<g/>
,	,	kIx,	,
skupinou	skupina	k1gFnSc7	skupina
literátů	literát	k1gMnPc2	literát
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
Květů	květ	k1gInPc2	květ
V.	V.	kA	V.
Filípkem	Filípek	k1gMnSc7	Filípek
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Hejnišem	Hejniš	k1gMnSc7	Hejniš
<g/>
,	,	kIx,	,
Františkem	František	k1gMnSc7	František
Krumlovským	krumlovský	k2eAgMnSc7d1	krumlovský
<g/>
,	,	kIx,	,
Karlem	Karel	k1gMnSc7	Karel
Hynkem	Hynek	k1gMnSc7	Hynek
Máchou	Mácha	k1gMnSc7	Mácha
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
Kajetánské	kajetánský	k2eAgNnSc4d1	Kajetánské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
hrálo	hrát	k5eAaImAgNnS	hrát
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
domě	dům	k1gInSc6	dům
pana	pan	k1gMnSc2	pan
Arbeita	Arbeit	k1gMnSc2	Arbeit
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
vzdělanější	vzdělaný	k2eAgFnPc4d2	vzdělanější
vrstvy	vrstva	k1gFnPc4	vrstva
a	a	k8xC	a
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
čtyřleté	čtyřletý	k2eAgFnSc2d1	čtyřletá
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konalo	konat	k5eAaImAgNnS	konat
25	[number]	k4	25
představení	představení	k1gNnPc2	představení
především	především	k6eAd1	především
domácích	domácí	k2eAgMnPc2d1	domácí
dramatiků	dramatik	k1gMnPc2	dramatik
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Kliment	Kliment	k1gMnSc1	Kliment
Klicpera	Klicpera	k1gFnSc1	Klicpera
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
dramaturg	dramaturg	k1gMnSc1	dramaturg
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Růžové	růžový	k2eAgFnSc6d1	růžová
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1834	[number]	k4	1834
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
jeho	jeho	k3xOp3gInSc1	jeho
divadelní	divadelní	k2eAgFnPc1d1	divadelní
hra	hra	k1gFnSc1	hra
Fidlovačka	Fidlovačka	k1gFnSc1	Fidlovačka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zpívána	zpíván	k2eAgFnSc1d1	zpívána
píseň	píseň	k1gFnSc1	píseň
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc1	můj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
dal	dát	k5eAaPmAgInS	dát
dohromady	dohromady	k6eAd1	dohromady
ochotnickou	ochotnický	k2eAgFnSc4d1	ochotnická
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hrála	hrát	k5eAaImAgFnS	hrát
české	český	k2eAgFnPc4d1	Česká
hry	hra	k1gFnPc4	hra
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
Hofmanem	Hofman	k1gMnSc7	Hofman
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
řízením	řízení	k1gNnSc7	řízení
českých	český	k2eAgNnPc2d1	české
představení	představení	k1gNnPc2	představení
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
všechny	všechen	k3xTgInPc1	všechen
jeho	jeho	k3xOp3gInPc7	jeho
spisy	spis	k1gInPc7	spis
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
Matice	matika	k1gFnSc3	matika
české	český	k2eAgFnSc3d1	Česká
a	a	k8xC	a
zlatý	zlatý	k2eAgInSc1d1	zlatý
prsten	prsten	k1gInSc1	prsten
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nazýván	nazývat	k5eAaImNgMnS	nazývat
miláčkem	miláček	k1gMnSc7	miláček
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
jako	jako	k8xS	jako
organizátor	organizátor	k1gMnSc1	organizátor
českého	český	k2eAgInSc2d1	český
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
organizoval	organizovat	k5eAaBmAgMnS	organizovat
plesy	ples	k1gInPc4	ples
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
revolučního	revoluční	k2eAgInSc2d1	revoluční
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
i	i	k9	i
do	do	k7c2	do
politického	politický	k2eAgNnSc2d1	politické
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
Svatováclavského	svatováclavský	k2eAgInSc2d1	svatováclavský
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
účastníkem	účastník	k1gMnSc7	účastník
Slovanského	slovanský	k2eAgInSc2d1	slovanský
sjezdu	sjezd	k1gInSc2	sjezd
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
založení	založení	k1gNnSc3	založení
Slovanské	slovanský	k2eAgFnSc2d1	Slovanská
lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c4	na
rakouský	rakouský	k2eAgInSc4d1	rakouský
ústavodárný	ústavodárný	k2eAgInSc4d1	ústavodárný
Říšský	říšský	k2eAgInSc4d1	říšský
sněm	sněm	k1gInSc4	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Zastupoval	zastupovat	k5eAaImAgInS	zastupovat
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
Unhošť	Unhošť	k1gFnSc4	Unhošť
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
jako	jako	k9	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
ke	k	k7c3	k
sněmovní	sněmovní	k2eAgFnSc3d1	sněmovní
pravici	pravice	k1gFnSc3	pravice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
na	na	k7c6	na
Riegerově	Riegerův	k2eAgNnSc6d1	Riegerovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
konání	konání	k1gNnSc2	konání
Říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgNnSc1d1	připomínající
nejen	nejen	k6eAd1	nejen
jeho	jeho	k3xOp3gFnSc4	jeho
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
napsal	napsat	k5eAaBmAgMnS	napsat
své	svůj	k3xOyFgNnSc4	svůj
drama	drama	k1gNnSc4	drama
Krvavé	krvavý	k2eAgNnSc4d1	krvavé
křtiny	křtiny	k1gFnPc4	křtiny
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
Drahomíra	drahomíra	k1gMnSc1	drahomíra
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
synové	syn	k1gMnPc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
uživil	uživit	k5eAaPmAgMnS	uživit
početnou	početný	k2eAgFnSc4d1	početná
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
úřadům	úřada	k1gMnPc3	úřada
zavděčit	zavděčit	k5eAaPmF	zavděčit
oslavnými	oslavný	k2eAgInPc7d1	oslavný
texty	text	k1gInPc7	text
na	na	k7c4	na
mocnáře	mocnář	k1gMnPc4	mocnář
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
například	například	k6eAd1	například
napsal	napsat	k5eAaBmAgMnS	napsat
oslavné	oslavný	k2eAgNnSc4d1	oslavné
díkuvzdání	díkuvzdání	k1gNnSc4	díkuvzdání
za	za	k7c4	za
nezdařený	zdařený	k2eNgInSc4d1	nezdařený
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
atentát	atentát	k1gInSc1	atentát
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
a	a	k8xC	a
dluhy	dluh	k1gInPc1	dluh
nadále	nadále	k6eAd1	nadále
narůstaly	narůstat	k5eAaImAgInP	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
udělal	udělat	k5eAaPmAgMnS	udělat
spoustu	spousta	k1gFnSc4	spousta
nepřátel	nepřítel	k1gMnPc2	nepřítel
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
propuštěn	propustit	k5eAaPmNgMnS	propustit
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
k	k	k7c3	k
Zollnerově	Zollnerův	k2eAgFnSc3d1	Zollnerův
kočovné	kočovný	k2eAgFnSc3d1	kočovná
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
v	v	k7c6	v
bídě	bída	k1gFnSc6	bída
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
české	český	k2eAgNnSc4d1	české
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
hry	hra	k1gFnSc2	hra
pouze	pouze	k6eAd1	pouze
překládal	překládat	k5eAaImAgMnS	překládat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
začal	začít	k5eAaPmAgMnS	začít
i	i	k9	i
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byla	být	k5eAaImAgNnP	být
Tylova	Tylův	k2eAgNnPc1d1	Tylovo
díla	dílo	k1gNnPc1	dílo
aktualizována	aktualizován	k2eAgFnSc1d1	aktualizována
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
dobové	dobový	k2eAgFnSc2d1	dobová
ideologie	ideologie	k1gFnSc2	ideologie
a	a	k8xC	a
koncepcí	koncepce	k1gFnSc7	koncepce
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Nejedlého	Nejedlý	k1gMnSc2	Nejedlý
<g/>
.	.	kIx.	.
</s>
<s>
Tylova	Tylův	k2eAgNnPc4d1	Tylovo
dramata	drama	k1gNnPc4	drama
byla	být	k5eAaImAgFnS	být
dávána	dáván	k2eAgFnSc1d1	dávána
za	za	k7c4	za
příklad	příklad	k1gInSc4	příklad
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
lidovost	lidovost	k1gFnSc4	lidovost
<g/>
,	,	kIx,	,
realismus	realismus	k1gInSc4	realismus
a	a	k8xC	a
společenskou	společenský	k2eAgFnSc4d1	společenská
angažovanost	angažovanost	k1gFnSc4	angažovanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
a	a	k8xC	a
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Magdalenou	Magdalena	k1gFnSc7	Magdalena
Forchheimovou	Forchheimová	k1gFnSc7	Forchheimová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
čekal	čekat	k5eAaImAgMnS	čekat
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
ale	ale	k9	ale
narodilo	narodit	k5eAaPmAgNnS	narodit
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
a	a	k8xC	a
Magdalena	Magdalena	k1gFnSc1	Magdalena
již	již	k6eAd1	již
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
těžkém	těžký	k2eAgInSc6d1	těžký
porodu	porod	k1gInSc6	porod
další	další	k2eAgFnPc4d1	další
děti	dítě	k1gFnPc4	dítě
mít	mít	k5eAaImF	mít
nemohla	moct	k5eNaImAgFnS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
navázal	navázat	k5eAaPmAgInS	navázat
intimní	intimní	k2eAgInSc4d1	intimní
poměr	poměr	k1gInSc4	poměr
s	s	k7c7	s
Magdaleninou	Magdalenin	k2eAgFnSc7d1	Magdalenina
mladší	mladý	k2eAgFnSc7d2	mladší
sestrou	sestra	k1gFnSc7	sestra
Annou	Anna	k1gFnSc7	Anna
Forchheimovou-Rajskou	Forchheimovou-Rajský	k2eAgFnSc7d1	Forchheimovou-Rajský
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
-	-	kIx~	-
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podivný	podivný	k2eAgInSc1d1	podivný
manželský	manželský	k2eAgInSc1d1	manželský
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
sestrami	sestra	k1gFnPc7	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
milenkou	milenka	k1gFnSc7	milenka
Annou	Anna	k1gFnSc7	Anna
nakonec	nakonec	k6eAd1	nakonec
zplodil	zplodit	k5eAaPmAgMnS	zplodit
devět	devět	k4xCc4	devět
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
manželka	manželka	k1gFnSc1	manželka
Magdalena	Magdalena	k1gFnSc1	Magdalena
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
K.	K.	kA	K.
Tyl	Tyl	k1gMnSc1	Tyl
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
úmrtí	úmrtí	k1gNnSc2	úmrtí
čtyři	čtyři	k4xCgFnPc1	čtyři
nezletilé	zletilý	k2eNgFnPc1d1	nezletilá
dcery	dcera	k1gFnPc1	dcera
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Otakar	Otakar	k1gMnSc1	Otakar
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Stanislav	Stanislav	k1gMnSc1	Stanislav
1845	[number]	k4	1845
<g/>
-	-	kIx~	-
<g/>
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
1853	[number]	k4	1853
<g/>
-	-	kIx~	-
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
syn	syn	k1gMnSc1	syn
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Tylově	Tylův	k2eAgFnSc6d1	Tylova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
dcer	dcera	k1gFnPc2	dcera
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
Marie	Marie	k1gFnSc1	Marie
Eleonora	Eleonora	k1gFnSc1	Eleonora
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1848	[number]	k4	1848
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1868	[number]	k4	1868
Morkovice	Morkovice	k1gFnSc1	Morkovice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eliška	Eliška	k1gFnSc1	Eliška
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
též	též	k9	též
herečky	herečka	k1gFnPc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Anna	Anna	k1gFnSc1	Anna
Forcheimová-Rajská	Forcheimová-Rajský	k2eAgFnSc1d1	Forcheimová-Rajský
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
spisovatele	spisovatel	k1gMnPc4	spisovatel
a	a	k8xC	a
žurnalistu	žurnalista	k1gMnSc4	žurnalista
Josefa	Josef	k1gMnSc4	Josef
Ladislava	Ladislav	k1gMnSc4	Ladislav
Turnovského	turnovský	k2eAgInSc2d1	turnovský
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
ředitele	ředitel	k1gMnSc2	ředitel
Ústřední	ústřední	k2eAgFnSc2d1	ústřední
Matice	matice	k1gFnSc2	matice
Školské	školská	k1gFnSc2	školská
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedl	vést	k5eAaImAgInS	vést
divadelní	divadelní	k2eAgFnSc4d1	divadelní
společnost	společnost	k1gFnSc4	společnost
po	po	k7c6	po
řediteli	ředitel	k1gMnSc6	ředitel
Prokopovi	Prokop	k1gMnSc6	Prokop
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
také	také	k9	také
jako	jako	k9	jako
divadelní	divadelní	k2eAgMnSc1d1	divadelní
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
negativně	negativně	k6eAd1	negativně
hodnotil	hodnotit	k5eAaImAgMnS	hodnotit
kvalitu	kvalita	k1gFnSc4	kvalita
inscenací	inscenace	k1gFnPc2	inscenace
uváděných	uváděný	k2eAgFnPc2d1	uváděná
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nelíbil	líbit	k5eNaImAgMnS	líbit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
výběr	výběr	k1gInSc1	výběr
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podléhal	podléhat	k5eAaImAgInS	podléhat
komerčním	komerční	k2eAgInPc3d1	komerční
cílům	cíl	k1gInPc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Odsuzoval	odsuzovat	k5eAaImAgMnS	odsuzovat
také	také	k9	také
nízkou	nízký	k2eAgFnSc4d1	nízká
estetickou	estetický	k2eAgFnSc4d1	estetická
hodnotu	hodnota	k1gFnSc4	hodnota
hraných	hraný	k2eAgInPc2d1	hraný
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Tyl	Tyl	k1gMnSc1	Tyl
postrádal	postrádat	k5eAaImAgMnS	postrádat
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
divadle	divadlo	k1gNnSc6	divadlo
původní	původní	k2eAgFnSc2d1	původní
hry	hra	k1gFnSc2	hra
psané	psaný	k2eAgFnSc2d1	psaná
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
programech	program	k1gInPc6	program
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
zejména	zejména	k9	zejména
překlady	překlad	k1gInPc1	překlad
cizojazyčných	cizojazyčný	k2eAgNnPc2d1	cizojazyčné
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
však	však	k9	však
také	také	k9	také
hojně	hojně	k6eAd1	hojně
překládal	překládat	k5eAaImAgMnS	překládat
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Jindy	jindy	k6eAd1	jindy
a	a	k8xC	a
Nyní	nyní	k6eAd1	nyní
kritickou	kritický	k2eAgFnSc4d1	kritická
rubriku	rubrika	k1gFnSc4	rubrika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
analyzoval	analyzovat	k5eAaImAgInS	analyzovat
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
divadelní	divadelní	k2eAgInPc4d1	divadelní
poměry	poměr	k1gInPc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
přeložil	přeložit	k5eAaPmAgInS	přeložit
nebo	nebo	k8xC	nebo
adaptoval	adaptovat	k5eAaBmAgInS	adaptovat
přes	přes	k7c4	přes
50	[number]	k4	50
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
působil	působit	k5eAaImAgMnS	působit
Tyl	Tyl	k1gMnSc1	Tyl
jako	jako	k8xS	jako
překladatel	překladatel	k1gMnSc1	překladatel
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
tlumočil	tlumočit	k5eAaImAgMnS	tlumočit
především	především	k9	především
romantické	romantický	k2eAgFnPc4d1	romantická
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
orientoval	orientovat	k5eAaBmAgInS	orientovat
na	na	k7c4	na
repertoár	repertoár	k1gInSc4	repertoár
vídeňského	vídeňský	k2eAgNnSc2d1	Vídeňské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
Tyl	tyl	k1gInSc1	tyl
přeložil	přeložit	k5eAaPmAgInS	přeložit
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
Dvě	dva	k4xCgFnPc1	dva
šelmy	šelma	k1gFnPc1	šelma
od	od	k7c2	od
Ignanze	Ignanze	k1gFnSc2	Ignanze
Franze	Franze	k1gFnSc2	Franze
Castelliho	Castelli	k1gMnSc2	Castelli
nebo	nebo	k8xC	nebo
Domácí	domácí	k2eAgFnSc2d1	domácí
rozepře	rozepře	k1gFnSc2	rozepře
od	od	k7c2	od
Augusta	August	k1gMnSc2	August
von	von	k1gInSc4	von
Kotzebueho	Kotzebue	k1gMnSc2	Kotzebue
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
německý	německý	k2eAgMnSc1d1	německý
dramatik	dramatik	k1gMnSc1	dramatik
patřil	patřit	k5eAaImAgMnS	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Raimundem	Raimund	k1gMnSc7	Raimund
a	a	k8xC	a
Johannem	Johann	k1gMnSc7	Johann
Nepomukem	Nepomuk	k1gMnSc7	Nepomuk
Nestroyem	Nestroyem	k1gInSc4	Nestroyem
mezi	mezi	k7c4	mezi
autory	autor	k1gMnPc4	autor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
Tyl	Tyl	k1gMnSc1	Tyl
překládal	překládat	k5eAaImAgMnS	překládat
nejčastěji	často	k6eAd3	často
<g/>
.	.	kIx.	.
</s>
<s>
Tyl	Tyl	k1gMnSc1	Tyl
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
frašky	fraška	k1gFnSc2	fraška
zbavovat	zbavovat	k5eAaImF	zbavovat
nevkusu	nevkus	k1gInSc2	nevkus
<g/>
,	,	kIx,	,
obohacoval	obohacovat	k5eAaImAgMnS	obohacovat
hry	hra	k1gFnPc4	hra
o	o	k7c4	o
nové	nový	k2eAgFnPc4d1	nová
zápletky	zápletka	k1gFnPc4	zápletka
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
zkracoval	zkracovat	k5eAaImAgMnS	zkracovat
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
překlady	překlad	k1gInPc1	překlad
her	hra	k1gFnPc2	hra
lokalizoval	lokalizovat	k5eAaBmAgMnS	lokalizovat
do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
přiblížit	přiblížit	k5eAaPmF	přiblížit
domácímu	domácí	k2eAgNnSc3d1	domácí
obecenstvu	obecenstvo	k1gNnSc3	obecenstvo
a	a	k8xC	a
poměrům	poměr	k1gInPc3	poměr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gNnSc7	on
panovaly	panovat	k5eAaImAgFnP	panovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyl	Tyl	k1gMnSc1	Tyl
si	se	k3xPyFc3	se
během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
ujasňoval	ujasňovat	k5eAaImAgMnS	ujasňovat
svérázné	svérázný	k2eAgInPc4d1	svérázný
rysy	rys	k1gInPc4	rys
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
překladatelská	překladatelský	k2eAgFnSc1d1	překladatelská
činnost	činnost	k1gFnSc1	činnost
tak	tak	k6eAd1	tak
postupně	postupně	k6eAd1	postupně
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
samostatných	samostatný	k2eAgNnPc2d1	samostatné
děl	dělo	k1gNnPc2	dělo
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
dramatickými	dramatický	k2eAgInPc7d1	dramatický
obrazy	obraz	k1gInPc7	obraz
ze	z	k7c2	z
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
také	také	k9	také
jako	jako	k9	jako
hry	hra	k1gFnPc1	hra
o	o	k7c4	o
polepšení	polepšení	k1gNnSc4	polepšení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Tyl	Tyl	k1gMnSc1	Tyl
umístil	umístit	k5eAaPmAgMnS	umístit
do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
překlad	překlad	k1gInSc1	překlad
hry	hra	k1gFnSc2	hra
Fortunatus	Fortunatus	k1gMnSc1	Fortunatus
Abenteuer	Abenteuer	k1gMnSc1	Abenteuer
zu	zu	k?	zu
Wasser	Wasser	k1gMnSc1	Wasser
und	und	k?	und
zu	zu	k?	zu
Lande	Land	k1gInSc5	Land
Johanna	Johann	k1gMnSc4	Johann
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Lemberta	Lembert	k1gMnSc2	Lembert
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Václavík	Václavík	k1gMnSc1	Václavík
Outrata	outrata	k1gMnSc1	outrata
a	a	k8xC	a
podivné	podivný	k2eAgFnSc2d1	podivná
příhody	příhoda	k1gFnSc2	příhoda
jeho	jeho	k3xOp3gMnSc1	jeho
na	na	k7c4	na
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tyl	Tyl	k1gMnSc1	Tyl
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
pokusil	pokusit	k5eAaPmAgMnS	pokusit
přebásnit	přebásnit	k5eAaPmF	přebásnit
také	také	k9	také
Krále	Král	k1gMnSc2	Král
Leara	Learo	k1gNnSc2	Learo
od	od	k7c2	od
Williama	Williamum	k1gNnSc2	Williamum
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
o	o	k7c4	o
první	první	k4xOgInSc4	první
český	český	k2eAgInSc4d1	český
veršovaný	veršovaný	k2eAgInSc4d1	veršovaný
překlad	překlad	k1gInSc4	překlad
ze	z	k7c2	z
Shakespearova	Shakespearův	k2eAgNnSc2d1	Shakespearovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Dědičná	dědičný	k2eAgFnSc1d1	dědičná
smlouva	smlouva	k1gFnSc1	smlouva
-	-	kIx~	-
drama	drama	k1gNnSc1	drama
s	s	k7c7	s
historickým	historický	k2eAgInSc7d1	historický
námětem	námět	k1gInSc7	námět
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
jednáních	jednání	k1gNnPc6	jednání
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
provedeno	provést	k5eAaPmNgNnS	provést
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
Výhoň	Výhoň	k1gFnSc1	Výhoň
dub	dub	k1gInSc1	dub
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
příliš	příliš	k6eAd1	příliš
neuspěla	uspět	k5eNaPmAgFnS	uspět
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
hra	hra	k1gFnSc1	hra
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
neúspěch	neúspěch	k1gInSc1	neúspěch
ho	on	k3xPp3gNnSc4	on
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
odradil	odradit	k5eAaPmAgInS	odradit
od	od	k7c2	od
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Fidlovačka	Fidlovačka	k1gFnSc1	Fidlovačka
aneb	aneb	k?	aneb
Žádný	žádný	k3yNgInSc4	žádný
hněv	hněv	k1gInSc4	hněv
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
rvačka	rvačka	k1gFnSc1	rvačka
-	-	kIx~	-
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
hra	hra	k1gFnSc1	hra
se	s	k7c7	s
zpěvy	zpěv	k1gInPc7	zpěv
<g/>
,	,	kIx,	,
z	z	k7c2	z
venkovského	venkovský	k2eAgNnSc2d1	venkovské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zapadla	zapadnout	k5eAaPmAgFnS	zapadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poprvé	poprvé	k6eAd1	poprvé
zde	zde	k6eAd1	zde
zazněla	zaznět	k5eAaImAgFnS	zaznět
budoucí	budoucí	k2eAgFnSc1d1	budoucí
česká	český	k2eAgFnSc1d1	Česká
hymna	hymna	k1gFnSc1	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
textu	text	k1gInSc2	text
písně	píseň	k1gFnSc2	píseň
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc4	domov
můj	můj	k3xOp1gInSc1	můj
se	se	k3xPyFc4	se
Tyl	Tyl	k1gMnSc1	Tyl
nechal	nechat	k5eAaPmAgMnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
svými	svůj	k3xOyFgInPc7	svůj
dojmy	dojem	k1gInPc7	dojem
z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Vrchlice	vrchlice	k1gFnSc2	vrchlice
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdohlavá	tvrdohlavý	k2eAgFnSc1d1	tvrdohlavá
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Slepý	slepý	k2eAgMnSc1d1	slepý
mládenec	mládenec	k1gMnSc1	mládenec
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Marjánka	Marjánka	k1gFnSc1	Marjánka
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
pluku	pluk	k1gInSc2	pluk
Zeman	Zeman	k1gMnSc1	Zeman
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
času	čas	k1gInSc2	čas
aneb	aneb	k?	aneb
Cop	cop	k1gInSc1	cop
a	a	k8xC	a
frak	frak	k1gInSc1	frak
-	-	kIx~	-
komický	komický	k2eAgInSc1d1	komický
obraz	obraz	k1gInSc1	obraz
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
jednáních	jednání	k1gNnPc6	jednání
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
provedeno	provést	k5eAaPmNgNnS	provést
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
Tyl	Tyl	k1gMnSc1	Tyl
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
rakouskými	rakouský	k2eAgMnPc7d1	rakouský
divadelníky	divadelník	k1gMnPc7	divadelník
Johannem	Johann	k1gMnSc7	Johann
Nepomukem	Nepomuk	k1gMnSc7	Nepomuk
Nestoyem	Nestoy	k1gMnSc7	Nestoy
<g/>
,	,	kIx,	,
Augustem	August	k1gMnSc7	August
von	von	k1gInSc1	von
Kotzebuem	Kotzebu	k1gInSc7	Kotzebu
a	a	k8xC	a
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Raimundem	Raimund	k1gMnSc7	Raimund
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
snahy	snaha	k1gFnSc2	snaha
spojit	spojit	k5eAaPmF	spojit
komerční	komerční	k2eAgInSc4d1	komerční
záměr	záměr	k1gInSc4	záměr
s	s	k7c7	s
výchovnými	výchovný	k2eAgInPc7d1	výchovný
cíli	cíl	k1gInPc7	cíl
byly	být	k5eAaImAgFnP	být
Tylovi	Tyl	k1gMnSc3	Tyl
blízké	blízký	k2eAgNnSc1d1	blízké
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
střetem	střet	k1gInSc7	střet
osobních	osobní	k2eAgFnPc2d1	osobní
ambicí	ambice	k1gFnPc2	ambice
hrdiny	hrdina	k1gMnSc2	hrdina
a	a	k8xC	a
pocitem	pocit	k1gInSc7	pocit
zodpovědnosti	zodpovědnost	k1gFnSc2	zodpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
končí	končit	k5eAaImIp3nS	končit
přivedením	přivedení	k1gNnSc7	přivedení
jedince	jedinec	k1gMnSc2	jedinec
k	k	k7c3	k
pokoře	pokora	k1gFnSc3	pokora
a	a	k8xC	a
skromnosti	skromnost	k1gFnSc3	skromnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vídeňských	vídeňský	k2eAgFnPc2d1	Vídeňská
předloh	předloha	k1gFnPc2	předloha
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
diváka	divák	k1gMnSc4	divák
pobavit	pobavit	k5eAaPmF	pobavit
<g/>
,	,	kIx,	,
Tyl	Tyl	k1gMnSc1	Tyl
se	se	k3xPyFc4	se
směřoval	směřovat	k5eAaImAgMnS	směřovat
pozornost	pozornost	k1gFnSc4	pozornost
k	k	k7c3	k
morálnímu	morální	k2eAgMnSc3d1	morální
a	a	k8xC	a
psychologickému	psychologický	k2eAgInSc3d1	psychologický
aspektu	aspekt	k1gInSc3	aspekt
<g/>
.	.	kIx.	.
</s>
<s>
Marjánka	Marjánka	k1gFnSc1	Marjánka
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
pluku	pluk	k1gInSc2	pluk
Pražský	pražský	k2eAgMnSc1d1	pražský
flamendr	flamendr	k1gMnSc1	flamendr
Bankrotář	bankrotář	k1gMnSc1	bankrotář
Chudý	Chudý	k1gMnSc1	Chudý
kejklíř	kejklíř	k1gMnSc1	kejklíř
Paličova	paličův	k2eAgFnSc1d1	Paličova
dcera	dcera	k1gFnSc1	dcera
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
-	-	kIx~	-
Rozárka	Rozárka	k1gFnSc1	Rozárka
je	být	k5eAaImIp3nS	být
dcera	dcera	k1gFnSc1	dcera
venkovského	venkovský	k2eAgMnSc2d1	venkovský
paliče	palič	k1gMnSc2	palič
Valenty	Valenta	k1gMnSc2	Valenta
<g/>
.	.	kIx.	.
</s>
<s>
Valenta	Valenta	k1gMnSc1	Valenta
se	se	k3xPyFc4	se
ze	z	k7c2	z
zoufalství	zoufalství	k1gNnSc2	zoufalství
a	a	k8xC	a
zlosti	zlost	k1gFnSc2	zlost
dopustí	dopustit	k5eAaPmIp3nS	dopustit
žhářství	žhářství	k1gNnSc1	žhářství
<g/>
.	.	kIx.	.
</s>
<s>
Rozárka	Rozárka	k1gFnSc1	Rozárka
bere	brát	k5eAaImIp3nS	brát
vinu	vina	k1gFnSc4	vina
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
její	její	k3xOp3gMnPc1	její
sourozenci	sourozenec	k1gMnPc1	sourozenec
měli	mít	k5eAaImAgMnP	mít
otce	otec	k1gMnSc4	otec
paliče	palič	k1gMnSc4	palič
<g/>
.	.	kIx.	.
</s>
<s>
Strakonický	strakonický	k2eAgMnSc1d1	strakonický
dudák	dudák	k1gMnSc1	dudák
aneb	aneb	k?	aneb
Hody	hod	k1gInPc1	hod
divých	divý	k2eAgFnPc2d1	divá
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
-	-	kIx~	-
dudák	dudák	k1gMnSc1	dudák
Švanda	Švanda	k1gMnSc1	Švanda
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
za	za	k7c7	za
vyšším	vysoký	k2eAgInSc7d2	vyšší
výdělkem	výdělek	k1gInSc7	výdělek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
domov	domov	k1gInSc4	domov
znamená	znamenat	k5eAaImIp3nS	znamenat
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Tyl	Tyl	k1gMnSc1	Tyl
chtěl	chtít	k5eAaImAgMnS	chtít
odsoudit	odsoudit	k5eAaPmF	odsoudit
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Pokouší	pokoušet	k5eAaImIp3nS	pokoušet
se	se	k3xPyFc4	se
agitovat	agitovat	k5eAaImF	agitovat
pro	pro	k7c4	pro
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lidé	člověk	k1gMnPc1	člověk
pracovali	pracovat	k5eAaImAgMnP	pracovat
doma	doma	k6eAd1	doma
-	-	kIx~	-
pro	pro	k7c4	pro
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Lesní	lesní	k2eAgFnSc1d1	lesní
panna	panna	k1gFnSc1	panna
aneb	aneb	k?	aneb
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
-	-	kIx~	-
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stejná	stejný	k2eAgFnSc1d1	stejná
myšlenka	myšlenka	k1gFnSc1	myšlenka
jako	jako	k8xC	jako
ve	v	k7c6	v
Strakonickém	strakonický	k2eAgMnSc6d1	strakonický
dudákovi	dudák	k1gMnSc6	dudák
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdohlavá	tvrdohlavý	k2eAgFnSc1d1	tvrdohlavá
žena	žena	k1gFnSc1	žena
aneb	aneb	k?	aneb
Zamilovaný	zamilovaný	k2eAgMnSc1d1	zamilovaný
školní	školní	k2eAgMnSc1d1	školní
mládenec	mládenec	k1gMnSc1	mládenec
-	-	kIx~	-
reagovala	reagovat	k5eAaBmAgFnS	reagovat
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
narážek	narážka	k1gFnPc2	narážka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
hrdinkou	hrdinka	k1gFnSc7	hrdinka
je	být	k5eAaImIp3nS	být
mlynářka	mlynářka	k1gFnSc1	mlynářka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nechce	chtít	k5eNaImIp3nS	chtít
dát	dát	k5eAaPmF	dát
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
školnímu	školní	k2eAgMnSc3d1	školní
mládenci	mládenec	k1gMnSc3	mládenec
<g/>
.	.	kIx.	.
</s>
<s>
Horský	Horský	k1gMnSc1	Horský
duch	duch	k1gMnSc1	duch
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
spálí	spálit	k5eAaPmIp3nS	spálit
mlýn	mlýn	k1gInSc1	mlýn
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
ji	on	k3xPp3gFnSc4	on
bloudit	bloudit	k5eAaImF	bloudit
<g/>
,	,	kIx,	,
až	až	k8xS	až
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
svou	svůj	k3xOyFgFnSc4	svůj
chybu	chyba	k1gFnSc4	chyba
a	a	k8xC	a
sebe	sebe	k3xPyFc4	sebe
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
provdá	provdat	k5eAaPmIp3nS	provdat
<g/>
.	.	kIx.	.
</s>
<s>
Jiříkovo	Jiříkův	k2eAgNnSc4d1	Jiříkovo
vidění	vidění	k1gNnSc4	vidění
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Čert	čert	k1gMnSc1	čert
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Krvavý	krvavý	k2eAgInSc1d1	krvavý
soud	soud	k1gInSc1	soud
aneb	aneb	k?	aneb
Kutnohorští	kutnohorský	k2eAgMnPc1d1	kutnohorský
havíři	havíř	k1gMnPc1	havíř
-	-	kIx~	-
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
zde	zde	k6eAd1	zde
potlačení	potlačení	k1gNnSc1	potlačení
vzpoury	vzpoura	k1gFnSc2	vzpoura
kutnohorských	kutnohorský	k2eAgMnPc2d1	kutnohorský
havířů	havíř	k1gMnPc2	havíř
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Inspirací	inspirace	k1gFnPc2	inspirace
mu	on	k3xPp3gNnSc3	on
byla	být	k5eAaImAgFnS	být
stávka	stávka	k1gFnSc1	stávka
dělníků	dělník	k1gMnPc2	dělník
smíchovské	smíchovský	k2eAgFnSc2d1	Smíchovská
kartounky	kartounka	k1gFnSc2	kartounka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
<g/>
.	.	kIx.	.
</s>
<s>
Opat	opat	k1gMnSc1	opat
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vyřešit	vyřešit	k5eAaPmF	vyřešit
spory	spor	k1gInPc1	spor
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
-	-	kIx~	-
hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
namířena	namířit	k5eAaPmNgFnS	namířit
proti	proti	k7c3	proti
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
poměrně	poměrně	k6eAd1	poměrně
mnoho	mnoho	k4c4	mnoho
historických	historický	k2eAgFnPc2d1	historická
nepřesností	nepřesnost	k1gFnPc2	nepřesnost
<g/>
,	,	kIx,	,
Hus	Hus	k1gMnSc1	Hus
se	se	k3xPyFc4	se
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Žižkou	Žižka	k1gMnSc7	Žižka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Tyla	týt	k5eAaImAgFnS	týt
ideálním	ideální	k2eAgMnSc7d1	ideální
lidovým	lidový	k2eAgMnSc7d1	lidový
vůdcem	vůdce	k1gMnSc7	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Tyl	Tyl	k1gMnSc1	Tyl
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
Husa	Hus	k1gMnSc2	Hus
bojovníka	bojovník	k1gMnSc2	bojovník
za	za	k7c4	za
ideály	ideál	k1gInPc4	ideál
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
a	a	k8xC	a
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
Krvavé	krvavý	k2eAgFnPc1d1	krvavá
křtiny	křtiny	k1gFnPc1	křtiny
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
Drahomíra	drahomíra	k1gMnSc1	drahomíra
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
synové	syn	k1gMnPc1	syn
Měšťané	měšťan	k1gMnPc1	měšťan
a	a	k8xC	a
študenti	študenti	k?	študenti
Dekret	dekret	k1gInSc1	dekret
kutnohorský	kutnohorský	k2eAgInSc1d1	kutnohorský
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
-	-	kIx~	-
novela	novela	k1gFnSc1	novela
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Rozina	Rozina	k1gFnSc1	Rozina
Ruthardova	Ruthardův	k2eAgFnSc1d1	Ruthardova
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
-	-	kIx~	-
novela	novela	k1gFnSc1	novela
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Vojta	Vojta	k1gMnSc1	Vojta
chudý	chudý	k1gMnSc1	chudý
čeledín	čeledín	k1gMnSc1	čeledín
-	-	kIx~	-
adaptace	adaptace	k1gFnSc1	adaptace
románu	román	k1gInSc2	román
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
autora	autor	k1gMnSc2	autor
Jeremiase	Jeremiasa	k1gFnSc6	Jeremiasa
Gotthelfa	Gotthelf	k1gMnSc2	Gotthelf
<g/>
.	.	kIx.	.
</s>
<s>
Vyšla	vyjít	k5eAaPmAgFnS	vyjít
za	za	k7c2	za
finanční	finanční	k2eAgFnSc2d1	finanční
podpory	podpora	k1gFnSc2	podpora
pojizerského	pojizerský	k2eAgMnSc2d1	pojizerský
vlastence	vlastenec	k1gMnSc2	vlastenec
a	a	k8xC	a
mecenáše	mecenáš	k1gMnSc2	mecenáš
Jana	Jan	k1gMnSc2	Jan
Krouského	Krouský	k2eAgMnSc2d1	Krouský
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
-	-	kIx~	-
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
obrací	obracet	k5eAaImIp3nS	obracet
se	se	k3xPyFc4	se
k	k	k7c3	k
odnárodněné	odnárodněný	k2eAgFnSc3d1	odnárodněný
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
Velenský	Velenský	k2eAgMnSc1d1	Velenský
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c2	za
posledního	poslední	k2eAgMnSc2d1	poslední
Čecha	Čech	k1gMnSc2	Čech
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
pro	pro	k7c4	pro
národ	národ	k1gInSc4	národ
získat	získat	k5eAaPmF	získat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
národu	národ	k1gInSc3	národ
vzdálil	vzdálit	k5eAaPmAgMnS	vzdálit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Řeší	řešit	k5eAaImIp3nP	řešit
zde	zde	k6eAd1	zde
generační	generační	k2eAgInPc4d1	generační
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
převažují	převažovat	k5eAaImIp3nP	převažovat
romantické	romantický	k2eAgInPc4d1	romantický
prvky	prvek	k1gInPc4	prvek
nad	nad	k7c7	nad
realistickými	realistický	k2eAgFnPc7d1	realistická
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
terčem	terč	k1gInSc7	terč
Havlíčkovy	Havlíčkův	k2eAgFnSc2d1	Havlíčkova
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
garanáty	garanát	k1gInPc1	garanát
-	-	kIx~	-
satira	satira	k1gFnSc1	satira
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Pomněnky	pomněnka	k1gFnPc1	pomněnka
z	z	k7c2	z
roztěže	roztěž	k1gFnSc2	roztěž
-	-	kIx~	-
satira	satira	k1gFnSc1	satira
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kusy	kus	k1gInPc1	kus
mého	můj	k3xOp1gNnSc2	můj
srdce	srdce	k1gNnSc2	srdce
-	-	kIx~	-
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
autobiografie	autobiografie	k1gFnSc1	autobiografie
<g/>
)	)	kIx)	)
S	s	k7c7	s
poctivostí	poctivost	k1gFnSc7	poctivost
nejdál	daleko	k6eAd3	daleko
dojdeš	dojít	k5eAaPmIp2nS	dojít
-	-	kIx~	-
povídka	povídka	k1gFnSc1	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Zloděj	zloděj	k1gMnSc1	zloděj
-	-	kIx~	-
povídka	povídka	k1gFnSc1	povídka
Karbaník	karbaník	k1gMnSc1	karbaník
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
milá	milá	k1gFnSc1	milá
-	-	kIx~	-
povídka	povídka	k1gFnSc1	povídka
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Braniboři	Branibor	k1gMnPc1	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
-	-	kIx~	-
povídka	povídka	k1gFnSc1	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Rozervanec	rozervanec	k1gMnSc1	rozervanec
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
-	-	kIx~	-
odsouzení	odsouzení	k1gNnSc1	odsouzení
romantických	romantický	k2eAgInPc2d1	romantický
životních	životní	k2eAgInPc2d1	životní
postojů	postoj	k1gInPc2	postoj
<g/>
,	,	kIx,	,
kritika	kritika	k1gFnSc1	kritika
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
,	,	kIx,	,
především	především	k9	především
jeho	jeho	k3xOp3gFnSc1	jeho
Máje	máje	k1gFnSc1	máje
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
Plzeň	Plzeň	k1gFnSc1	Plzeň
-	-	kIx~	-
Lochotín	Lochotín	k1gInSc1	Lochotín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
přesunutý	přesunutý	k2eAgInSc1d1	přesunutý
na	na	k7c4	na
Mikulášské	mikulášský	k2eAgNnSc4d1	Mikulášské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Panteon	panteon	k1gInSc1	panteon
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
BLAHNÍK	BLAHNÍK	kA	BLAHNÍK
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kristian	Kristian	k1gMnSc1	Kristian
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
K.	K.	kA	K.
Tyla	Tyl	k1gMnSc2	Tyl
had	had	k1gMnSc1	had
z	z	k7c2	z
ráje	ráj	k1gInSc2	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Prometheus	Prometheus	k1gMnSc1	Prometheus
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Monografie	monografie	k1gFnSc1	monografie
o	o	k7c6	o
pohnutém	pohnutý	k2eAgInSc6d1	pohnutý
životě	život	k1gInSc6	život
a	a	k8xC	a
díle	dílo	k1gNnSc6	dílo
J.	J.	kA	J.
K.	K.	kA	K.
Tyla	tyla	k1gFnSc1	tyla
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
divadelního	divadelní	k2eAgMnSc2d1	divadelní
historika	historik	k1gMnSc2	historik
V.	V.	kA	V.
K.	K.	kA	K.
Blahníka	Blahník	k1gMnSc2	Blahník
<g/>
.	.	kIx.	.
</s>
<s>
FILÍPEK	Filípek	k1gMnSc1	Filípek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Jos	Jos	k?	Jos
<g/>
.	.	kIx.	.
</s>
<s>
Kaj	kát	k5eAaImRp2nS	kát
<g/>
.	.	kIx.	.
</s>
<s>
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
snažení	snažení	k1gNnSc2	snažení
a	a	k8xC	a
působení	působení	k1gNnSc2	působení
:	:	kIx,	:
životopisný	životopisný	k2eAgInSc1d1	životopisný
nástin	nástin	k1gInSc1	nástin
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Kober	kobra	k1gFnPc2	kobra
a	a	k8xC	a
Markgraf	Markgraf	k1gInSc1	Markgraf
<g/>
,	,	kIx,	,
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Životopisný	životopisný	k2eAgInSc1d1	životopisný
nástin	nástin	k1gInSc1	nástin
o	o	k7c4	o
Josefu	Josefa	k1gFnSc4	Josefa
Kajetánu	Kajetán	k1gMnSc3	Kajetán
Tylovi	Tyl	k1gMnSc3	Tyl
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
autor	autor	k1gMnSc1	autor
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
působení	působení	k1gNnSc6	působení
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Květy	Květa	k1gFnSc2	Květa
a	a	k8xC	a
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Růžové	růžový	k2eAgFnSc6d1	růžová
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
MERHAUT	MERHAUT	kA	MERHAUT
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
S	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
T.	T.	kA	T.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
1082	[number]	k4	1082
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1670	[number]	k4	1670
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
1068	[number]	k4	1068
<g/>
-	-	kIx~	-
<g/>
1078	[number]	k4	1078
<g/>
.	.	kIx.	.
</s>
<s>
ŠTECH	ŠTECH	kA	ŠTECH
<g/>
,	,	kIx,	,
V.	V.	kA	V.
V.	V.	kA	V.
V	v	k7c6	v
zamlženém	zamlžený	k2eAgNnSc6d1	zamlžené
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
s.	s.	k?	s.
66	[number]	k4	66
TURNOVSKÝ	turnovský	k2eAgInSc1d1	turnovský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
Josefa	Josef	k1gMnSc2	Josef
Kajetana	Kajetan	k1gMnSc2	Kajetan
Tyla	Tyl	k1gMnSc2	Tyl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
VONDRA	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
české	český	k2eAgFnSc2d1	Česká
minulosti	minulost	k1gFnSc2	minulost
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
(	(	kIx(	(
<g/>
1808	[number]	k4	1808
-	-	kIx~	-
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
:	:	kIx,	:
časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
popularizaci	popularizace	k1gFnSc4	popularizace
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
22	[number]	k4	22
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
135	[number]	k4	135
<g/>
-	-	kIx~	-
<g/>
138	[number]	k4	138
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgNnSc1d1	Československé
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
</s>
<s>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
sešit	sešit	k1gInSc1	sešit
10	[number]	k4	10
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1926	[number]	k4	1926
a	a	k8xC	a
sešit	sešit	k1gInSc1	sešit
11	[number]	k4	11
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
F.	F.	kA	F.
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
s.	s.	k?	s.
150	[number]	k4	150
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
165	[number]	k4	165
<g/>
-	-	kIx~	-
<g/>
194	[number]	k4	194
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
307	[number]	k4	307
<g/>
-	-	kIx~	-
<g/>
314	[number]	k4	314
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Literatura	literatura	k1gFnSc1	literatura
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
/	/	kIx~	/
Redaktor	redaktor	k1gMnSc1	redaktor
svazku	svazek	k1gInSc2	svazek
Felix	Felix	k1gMnSc1	Felix
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
684	[number]	k4	684
s.	s.	k?	s.
S.	S.	kA	S.
399	[number]	k4	399
<g/>
-	-	kIx~	-
<g/>
431	[number]	k4	431
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
/	/	kIx~	/
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Augusta	Augusta	k1gMnSc1	Augusta
...	...	k?	...
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
571	[number]	k4	571
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
431	[number]	k4	431
<g/>
-	-	kIx~	-
<g/>
432	[number]	k4	432
<g/>
.	.	kIx.	.
</s>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
předchůdci	předchůdce	k1gMnPc7	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
s.	s.	k?	s.
536	[number]	k4	536
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
HÝSEK	Hýsek	k1gMnSc1	Hýsek
<g/>
,	,	kIx,	,
MILOSLAV	Miloslav	k1gMnSc1	Miloslav
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
K.	K.	kA	K.
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
.	.	kIx.	.
</s>
<s>
Matice	matice	k1gFnSc1	matice
divadelní	divadelní	k2eAgFnSc1d1	divadelní
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
LEHÁR	LEHÁR	kA	LEHÁR
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
STICH	STICH	kA	STICH
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
;	;	kIx,	;
JANÁČKOVÁ	Janáčková	k1gFnSc1	Janáčková
<g/>
,	,	kIx,	,
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
<g/>
;	;	kIx,	;
HOLÝ	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
k	k	k7c3	k
dnešku	dnešek	k1gInSc3	dnešek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
1048	[number]	k4	1048
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
308	[number]	k4	308
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
OTRUBA	otruba	k1gFnSc1	otruba
<g/>
,	,	kIx,	,
<g/>
Mojmír	Mojmír	k1gMnSc1	Mojmír
a	a	k8xC	a
KAČER	kačer	k1gMnSc1	kačer
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrčí	tvůrčí	k2eAgFnSc1d1	tvůrčí
cesta	cesta	k1gFnSc1	cesta
J.	J.	kA	J.
K.	K.	kA	K.
Tyla	Tyl	k1gMnSc2	Tyl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNDKLHU	SNDKLHU	kA	SNDKLHU
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
TUREČEK	Tureček	k1gMnSc1	Tureček
<g/>
,	,	kIx,	,
Dalibor	Dalibor	k1gMnSc1	Dalibor
<g/>
.	.	kIx.	.
</s>
<s>
Rozporuplná	rozporuplný	k2eAgFnSc1d1	rozporuplná
sounáležitost	sounáležitost	k1gFnSc1	sounáležitost
<g/>
:	:	kIx,	:
německojazyčné	německojazyčný	k2eAgInPc1d1	německojazyčný
kontexty	kontext	k1gInPc1	kontext
obrozenského	obrozenský	k2eAgNnSc2d1	obrozenské
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Divadelní	divadelní	k2eAgInSc1d1	divadelní
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
<g/>
České	český	k2eAgNnSc1d1	české
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
Unnumbered	Unnumbered	k1gInSc1	Unnumbered
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7008-122-8	[number]	k4	80-7008-122-8
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1815	[number]	k4	1815
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
Seznam	seznam	k1gInSc4	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
České	český	k2eAgNnSc1d1	české
divadlo	divadlo	k1gNnSc1	divadlo
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	týt	k5eAaImAgMnS	týt
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
<g />
.	.	kIx.	.
</s>
<s>
Volně	volně	k6eAd1	volně
dostupná	dostupný	k2eAgNnPc1d1	dostupné
díla	dílo	k1gNnPc1	dílo
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	Tyl	k1gMnSc2	Tyl
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
NK	NK	kA	NK
ČR	ČR	kA	ČR
Veřejně	veřejně	k6eAd1	veřejně
dostupná	dostupný	k2eAgNnPc4d1	dostupné
díla	dílo	k1gNnPc4	dílo
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	Tyl	k1gMnSc2	Tyl
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
MZK	MZK	kA	MZK
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
v	v	k7c6	v
souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
<g />
.	.	kIx.	.
</s>
<s>
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
adaptace	adaptace	k1gFnSc2	adaptace
vybraných	vybraný	k2eAgNnPc2d1	vybrané
děl	dělo	k1gNnPc2	dělo
k	k	k7c3	k
bezplatnému	bezplatný	k2eAgNnSc3d1	bezplatné
stáhnutí	stáhnutí	k1gNnSc3	stáhnutí
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
mp	mp	k?	mp
<g/>
3	[number]	k4	3
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
3	[number]	k4	3
<g/>
-Vltava	-Vltava	k1gFnSc1	-Vltava
Několik	několik	k4yIc4	několik
<g />
.	.	kIx.	.
</s>
<s>
volně	volně	k6eAd1	volně
dostupných	dostupný	k2eAgNnPc2d1	dostupné
zdigitalizovaých	zdigitalizovaých	k2eAgNnPc2d1	zdigitalizovaých
děl	dělo	k1gNnPc2	dělo
J.K.	J.K.	k1gMnSc2	J.K.
<g/>
Tyla	Tyl	k1gMnSc2	Tyl
v	v	k7c6	v
Krameriovi	Kramerius	k1gMnSc6	Kramerius
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnPc1	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
K	k	k7c3	k
jazykové	jazykový	k2eAgFnSc3d1	jazyková
a	a	k8xC	a
slohové	slohový	k2eAgFnSc3d1	slohová
stránce	stránka	k1gFnSc3	stránka
Tylových	Tylových	k2eAgFnPc2d1	Tylových
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
Tylův	Tylův	k2eAgInSc1d1	Tylův
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
živého	živý	k2eAgInSc2d1	živý
pramene	pramen	k1gInSc2	pramen
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
-	-	kIx~	-
životopis	životopis	k1gInSc1	životopis
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
:	:	kIx,	:
Poslední	poslední	k2eAgMnSc1d1	poslední
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
Květy	květ	k1gInPc1	květ
-	-	kIx~	-
digitalizovaný	digitalizovaný	k2eAgInSc1d1	digitalizovaný
časopis	časopis	k1gInSc1	časopis
<g/>
,	,	kIx,	,
roky	rok	k1gInPc1	rok
1834-1888	[number]	k4	1834-1888
(	(	kIx(	(
<g/>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
</s>
