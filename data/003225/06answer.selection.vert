<s>
Třetí	třetí	k4xOgFnSc1	třetí
teorie	teorie	k1gFnSc1	teorie
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
když	když	k8xS	když
Quorthon	Quorthon	k1gMnSc1	Quorthon
<g/>
,	,	kIx,	,
zesnulý	zesnulý	k1gMnSc1	zesnulý
zakladatel	zakladatel	k1gMnSc1	zakladatel
blackmetalové	blackmetal	k1gMnPc1	blackmetal
skupiny	skupina	k1gFnSc2	skupina
Bathory	Bathora	k1gFnSc2	Bathora
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
novinářem	novinář	k1gMnSc7	novinář
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
kapela	kapela	k1gFnSc1	kapela
nikdy	nikdy	k6eAd1	nikdy
k	k	k7c3	k
death	death	k1gInSc4	death
metalu	metal	k1gInSc2	metal
nepatřila	patřit	k5eNaImAgFnS	patřit
<g/>
.	.	kIx.	.
</s>
