<s>
Ogam	ogam	k1gInSc1	ogam
je	být	k5eAaImIp3nS	být
irské	irský	k2eAgNnSc4d1	irské
hláskové	hláskový	k2eAgNnSc4d1	hláskové
písmo	písmo	k1gNnSc4	písmo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zářezů	zářez	k1gInPc2	zářez
rytých	rytý	k2eAgInPc2d1	rytý
do	do	k7c2	do
hran	hrana	k1gFnPc2	hrana
kamenných	kamenný	k2eAgInPc2d1	kamenný
kvádrů	kvádr	k1gInPc2	kvádr
nebo	nebo	k8xC	nebo
do	do	k7c2	do
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
irštiny	irština	k1gFnSc2	irština
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Ogam	ogam	k1gInSc1	ogam
se	se	k3xPyFc4	se
psal	psát	k5eAaImAgInS	psát
buď	buď	k8xC	buď
na	na	k7c4	na
vertikální	vertikální	k2eAgFnSc4d1	vertikální
nebo	nebo	k8xC	nebo
na	na	k7c4	na
horizontální	horizontální	k2eAgFnSc4d1	horizontální
linii	linie	k1gFnSc4	linie
(	(	kIx(	(
<g/>
hranu	hrana	k1gFnSc4	hrana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vertikální	vertikální	k2eAgNnSc1d1	vertikální
se	se	k3xPyFc4	se
psalo	psát	k5eAaImAgNnS	psát
zdola	zdola	k6eAd1	zdola
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
horizontální	horizontální	k2eAgNnSc1d1	horizontální
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
jsou	být	k5eAaImIp3nP	být
uspořádána	uspořádat	k5eAaPmNgNnP	uspořádat
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
pěti	pět	k4xCc6	pět
znacích	znak	k1gInPc6	znak
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
každé	každý	k3xTgFnSc2	každý
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
čarami	čára	k1gFnPc7	čára
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
do	do	k7c2	do
pěti	pět	k4xCc2	pět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
různě	různě	k6eAd1	různě
orientovány	orientovat	k5eAaBmNgFnP	orientovat
k	k	k7c3	k
pomyslné	pomyslný	k2eAgFnSc3d1	pomyslná
základní	základní	k2eAgFnSc3d1	základní
linii	linie	k1gFnSc3	linie
či	či	k8xC	či
hraně	hrana	k1gFnSc3	hrana
opracovaného	opracovaný	k2eAgInSc2d1	opracovaný
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
B	B	kA	B
se	s	k7c7	s
souhláskami	souhláska	k1gFnPc7	souhláska
B	B	kA	B
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
N	N	kA	N
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
delší	dlouhý	k2eAgFnPc1d2	delší
čáry	čára	k1gFnPc1	čára
vedené	vedený	k2eAgFnPc1d1	vedená
napravo	napravo	k6eAd1	napravo
(	(	kIx(	(
<g/>
dolů	dolů	k6eAd1	dolů
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
H	H	kA	H
obsahující	obsahující	k2eAgFnSc2d1	obsahující
souhlásky	souhláska	k1gFnSc2	souhláska
H	H	kA	H
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
Q	Q	kA	Q
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
delší	dlouhý	k2eAgFnPc1d2	delší
čáry	čára	k1gFnPc1	čára
vedené	vedený	k2eAgFnPc1d1	vedená
vlevo	vlevo	k6eAd1	vlevo
(	(	kIx(	(
<g/>
nahoru	nahoru	k6eAd1	nahoru
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
M	M	kA	M
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
souhlásky	souhláska	k1gFnSc2	souhláska
M	M	kA	M
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
Ng	Ng	k1gFnSc1	Ng
<g/>
,	,	kIx,	,
Z	Z	kA	Z
<g/>
,	,	kIx,	,
R	R	kA	R
se	se	k3xPyFc4	se
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
pomocí	pomocí	k7c2	pomocí
delších	dlouhý	k2eAgFnPc2d2	delší
čar	čára	k1gFnPc2	čára
vedených	vedený	k2eAgFnPc2d1	vedená
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
přes	přes	k7c4	přes
osu	osa	k1gFnSc4	osa
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
kolmo	kolmo	k6eAd1	kolmo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
šikmo	šikmo	k6eAd1	šikmo
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
A	a	k9	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
samohlásky	samohláska	k1gFnPc4	samohláska
A	A	kA	A
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
U	U	kA	U
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
I	i	k8xC	i
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
krátké	krátký	k2eAgFnPc4d1	krátká
čáry	čára	k1gFnPc4	čára
kolmo	kolmo	k6eAd1	kolmo
přes	přes	k7c4	přes
<g/>
.	.	kIx.	.
</s>
