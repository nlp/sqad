<p>
<s>
Klášter	klášter	k1gInSc1	klášter
Montecassino	Montecassin	k2eAgNnSc1d1	Montecassino
je	on	k3xPp3gNnSc4	on
benediktinské	benediktinský	k2eAgNnSc4d1	benediktinské
územní	územní	k2eAgNnSc4d1	územní
opatství	opatství	k1gNnSc4	opatství
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
založené	založený	k2eAgFnSc2d1	založená
cca	cca	kA	cca
v	v	k7c6	v
roce	rok	k1gInSc6	rok
529	[number]	k4	529
svatým	svatý	k2eAgInSc7d1	svatý
Benediktem	benedikt	k1gInSc7	benedikt
z	z	k7c2	z
Nursie	Nursie	k1gFnSc2	Nursie
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c4	na
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
<g/>
,	,	kIx,	,
520	[number]	k4	520
metrů	metr	k1gInPc2	metr
vysoké	vysoký	k2eAgFnSc3d1	vysoká
hoře	hora	k1gFnSc3	hora
asi	asi	k9	asi
130	[number]	k4	130
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
necelé	celý	k2eNgFnPc1d1	necelá
2	[number]	k4	2
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
Cassino	Cassin	k2eAgNnSc1d1	Cassino
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
klášterem	klášter	k1gInSc7	klášter
západního	západní	k2eAgNnSc2d1	západní
mnišství	mnišství	k1gNnSc2	mnišství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
vzor	vzor	k1gInSc4	vzor
pro	pro	k7c4	pro
budování	budování	k1gNnSc4	budování
dalších	další	k2eAgInPc2d1	další
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
strategický	strategický	k2eAgInSc4d1	strategický
význam	význam	k1gInSc4	význam
vyvýšeniny	vyvýšenina	k1gFnSc2	vyvýšenina
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Monte	Mont	k1gInSc5	Mont
Cassino	Cassin	k2eAgNnSc1d1	Cassino
několikráte	několikráte	k6eAd1	několikráte
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
jej	on	k3xPp3gMnSc4	on
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
a	a	k8xC	a
pobořili	pobořit	k5eAaPmAgMnP	pobořit
Langobardi	Langobard	k1gMnPc1	Langobard
v	v	k7c6	v
roce	rok	k1gInSc6	rok
584	[number]	k4	584
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
podobná	podobný	k2eAgFnSc1d1	podobná
katastrofa	katastrofa	k1gFnSc1	katastrofa
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
prakticky	prakticky	k6eAd1	prakticky
totálně	totálně	k6eAd1	totálně
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Monte	Mont	k1gInSc5	Mont
Cassina	Cassin	k2eAgMnSc4d1	Cassin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
generál	generál	k1gMnSc1	generál
Senger	Senger	k1gMnSc1	Senger
<g/>
)	)	kIx)	)
chtěli	chtít	k5eAaImAgMnP	chtít
původně	původně	k6eAd1	původně
klášter	klášter	k1gInSc4	klášter
bojů	boj	k1gInPc2	boj
ušetřit	ušetřit	k5eAaPmF	ušetřit
a	a	k8xC	a
nevyužili	využít	k5eNaPmAgMnP	využít
při	při	k7c6	při
budování	budování	k1gNnSc6	budování
svých	svůj	k3xOyFgFnPc2	svůj
obranných	obranný	k2eAgFnPc2d1	obranná
pozic	pozice	k1gFnPc2	pozice
klášter	klášter	k1gInSc1	klášter
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gNnSc4	jeho
nejbližší	blízký	k2eAgNnSc4d3	nejbližší
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Spojenci	spojenec	k1gMnPc1	spojenec
si	se	k3xPyFc3	se
nebyli	být	k5eNaImAgMnP	být
jisti	jist	k2eAgMnPc1d1	jist
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
to	ten	k3xDgNnSc1	ten
myslí	myslet	k5eAaImIp3nS	myslet
upřímně	upřímně	k6eAd1	upřímně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
navedli	navést	k5eAaPmAgMnP	navést
na	na	k7c4	na
klášter	klášter	k1gInSc4	klášter
několik	několik	k4yIc1	několik
vln	vlna	k1gFnPc2	vlna
strategických	strategický	k2eAgInPc2d1	strategický
bombardérů	bombardér	k1gInPc2	bombardér
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jej	on	k3xPp3gInSc4	on
srovnaly	srovnat	k5eAaPmAgFnP	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
poté	poté	k6eAd1	poté
ruiny	ruina	k1gFnPc4	ruina
obsadili	obsadit	k5eAaPmAgMnP	obsadit
a	a	k8xC	a
využili	využít	k5eAaPmAgMnP	využít
jako	jako	k9	jako
výhodnou	výhodný	k2eAgFnSc4d1	výhodná
obrannou	obranný	k2eAgFnSc4d1	obranná
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zničení	zničení	k1gNnSc1	zničení
kláštera	klášter	k1gInSc2	klášter
bylo	být	k5eAaImAgNnS	být
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
především	především	k9	především
řadou	řada	k1gFnSc7	řada
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
představitelů	představitel	k1gMnPc2	představitel
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
kardinálem	kardinál	k1gMnSc7	kardinál
státním	státní	k2eAgMnSc7d1	státní
sekretářem	sekretář	k1gMnSc7	sekretář
Maglionim	Maglionima	k1gFnPc2	Maglionima
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgInPc1d1	americký
úřady	úřad	k1gInPc1	úřad
dlouho	dlouho	k6eAd1	dlouho
setrvávaly	setrvávat	k5eAaImAgInP	setrvávat
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
že	že	k8xS	že
měly	mít	k5eAaImAgInP	mít
nezvratné	zvratný	k2eNgInPc1d1	nezvratný
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
<g/>
;	;	kIx,	;
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
připustily	připustit	k5eAaPmAgInP	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
bombardování	bombardování	k1gNnSc2	bombardování
německé	německý	k2eAgFnSc2d1	německá
jednotky	jednotka	k1gFnSc2	jednotka
dosud	dosud	k6eAd1	dosud
nenacházely	nacházet	k5eNaImAgFnP	nacházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
vojenských	vojenský	k2eAgMnPc2d1	vojenský
historiků	historik	k1gMnPc2	historik
podotýká	podotýkat	k5eAaImIp3nS	podotýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bombardování	bombardování	k1gNnSc1	bombardování
kláštera	klášter	k1gInSc2	klášter
byl	být	k5eAaImAgInS	být
veliký	veliký	k2eAgInSc1d1	veliký
nesmysl	nesmysl	k1gInSc1	nesmysl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jako	jako	k8xS	jako
stojící	stojící	k2eAgMnSc1d1	stojící
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
využitelný	využitelný	k2eAgInSc1d1	využitelný
(	(	kIx(	(
<g/>
stojící	stojící	k2eAgFnPc1d1	stojící
budovy	budova	k1gFnPc1	budova
představují	představovat	k5eAaImIp3nP	představovat
pro	pro	k7c4	pro
obránce	obránce	k1gMnPc4	obránce
spíše	spíše	k9	spíše
riziko	riziko	k1gNnSc4	riziko
než	než	k8xS	než
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
)	)	kIx)	)
–	–	k?	–
teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgMnS	změnit
v	v	k7c4	v
ruiny	ruina	k1gFnPc4	ruina
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
vynikající	vynikající	k2eAgFnSc7d1	vynikající
obrannou	obranný	k2eAgFnSc7d1	obranná
pozicí	pozice	k1gFnSc7	pozice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Němci	Němec	k1gMnPc1	Němec
obsadili	obsadit	k5eAaPmAgMnP	obsadit
a	a	k8xC	a
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
drželi	držet	k5eAaImAgMnP	držet
celé	celý	k2eAgInPc4d1	celý
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc4	jejich
postavení	postavení	k1gNnSc4	postavení
nakonec	nakonec	k6eAd1	nakonec
rozbil	rozbít	k5eAaPmAgInS	rozbít
polský	polský	k2eAgInSc1d1	polský
armádní	armádní	k2eAgInSc1d1	armádní
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
motivovaný	motivovaný	k2eAgInSc1d1	motivovaný
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
odplatě	odplata	k1gFnSc6	odplata
za	za	k7c4	za
válečné	válečný	k2eAgNnSc4d1	válečné
rozdělení	rozdělení	k1gNnSc4	rozdělení
Polska	Polsko	k1gNnSc2	Polsko
mezi	mezi	k7c4	mezi
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
–	–	k?	–
po	po	k7c6	po
několika	několik	k4yIc6	několik
krvavých	krvavý	k2eAgInPc6d1	krvavý
dnech	den	k1gInPc6	den
bojů	boj	k1gInPc2	boj
klášter	klášter	k1gInSc4	klášter
obsadili	obsadit	k5eAaPmAgMnP	obsadit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
obnoven	obnovit	k5eAaPmNgInS	obnovit
(	(	kIx(	(
<g/>
většinu	většina	k1gFnSc4	většina
prostředků	prostředek	k1gInPc2	prostředek
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
italský	italský	k2eAgInSc1d1	italský
stát	stát	k1gInSc1	stát
<g/>
)	)	kIx)	)
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
jej	on	k3xPp3gMnSc4	on
znovu	znovu	k6eAd1	znovu
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Terra	Terra	k6eAd1	Terra
Sancti	Sanct	k2eAgMnPc1d1	Sanct
Benedicti	Benedict	k1gMnPc1	Benedict
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Monte	Mont	k1gMnSc5	Mont
Cassino	Cassin	k2eAgNnSc4d1	Cassino
</s>
</p>
<p>
<s>
Władysław	Władysław	k?	Władysław
Anders	Anders	k1gInSc1	Anders
</s>
</p>
<p>
<s>
Beneventsko-montecassinské	Beneventskoontecassinský	k2eAgNnSc1d1	Beneventsko-montecassinský
písmo	písmo	k1gNnSc1	písmo
</s>
</p>
<p>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
sakrální	sakrální	k2eAgFnSc1d1	sakrální
architektura	architektura	k1gFnSc1	architektura
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Klášter	klášter	k1gInSc4	klášter
Montecassino	Montecassin	k2eAgNnSc1d1	Montecassino
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
