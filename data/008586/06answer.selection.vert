<s>
Klášter	klášter	k1gInSc1	klášter
Montecassino	Montecassin	k2eAgNnSc1d1	Montecassino
je	on	k3xPp3gNnSc4	on
benediktinské	benediktinský	k2eAgNnSc4d1	benediktinské
územní	územní	k2eAgNnSc4d1	územní
opatství	opatství	k1gNnSc4	opatství
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
založené	založený	k2eAgFnSc2d1	založená
cca	cca	kA	cca
v	v	k7c6	v
roce	rok	k1gInSc6	rok
529	[number]	k4	529
svatým	svatý	k2eAgInSc7d1	svatý
Benediktem	benedikt	k1gInSc7	benedikt
z	z	k7c2	z
Nursie	Nursie	k1gFnSc2	Nursie
<g/>
.	.	kIx.	.
</s>
