<s>
Společenstvo	společenstvo	k1gNnSc1	společenstvo
neboli	neboli	k8xC	neboli
biocenóza	biocenóza	k1gFnSc1	biocenóza
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
bios	bios	k6eAd1	bios
=	=	kIx~	=
život	život	k1gInSc1	život
+	+	kIx~	+
koinos	koinos	k1gInSc1	koinos
=	=	kIx~	=
společný	společný	k2eAgInSc1d1	společný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
populací	populace	k1gFnPc2	populace
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
hub	houba	k1gFnPc2	houba
a	a	k8xC	a
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
biotopu	biotop	k1gInSc6	biotop
<g/>
;	;	kIx,	;
existují	existovat	k5eAaImIp3nP	existovat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
určité	určitý	k2eAgInPc1d1	určitý
vztahy	vztah	k1gInPc1	vztah
<g/>
.	.	kIx.	.
</s>
