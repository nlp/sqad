<p>
<s>
Společenstvo	společenstvo	k1gNnSc1	společenstvo
neboli	neboli	k8xC	neboli
biocenóza	biocenóza	k1gFnSc1	biocenóza
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
bios	bios	k6eAd1	bios
=	=	kIx~	=
život	život	k1gInSc1	život
+	+	kIx~	+
koinos	koinos	k1gInSc1	koinos
=	=	kIx~	=
společný	společný	k2eAgInSc1d1	společný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
populací	populace	k1gFnPc2	populace
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
hub	houba	k1gFnPc2	houba
a	a	k8xC	a
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
biotopu	biotop	k1gInSc6	biotop
<g/>
;	;	kIx,	;
existují	existovat	k5eAaImIp3nP	existovat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
určité	určitý	k2eAgInPc1d1	určitý
vztahy	vztah	k1gInPc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
živá	živý	k2eAgFnSc1d1	živá
část	část	k1gFnSc1	část
ekosystému	ekosystém	k1gInSc2	ekosystém
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
samoregulace	samoregulace	k1gFnSc1	samoregulace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
biotop	biotop	k1gInSc1	biotop
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
společenstvo	společenstvo	k1gNnSc1	společenstvo
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
zákonitostmi	zákonitost	k1gFnPc7	zákonitost
vztahů	vztah	k1gInPc2	vztah
ve	v	k7c6	v
společenstvu	společenstvo	k1gNnSc6	společenstvo
je	být	k5eAaImIp3nS	být
biocenologie	biocenologie	k1gFnSc1	biocenologie
<g/>
.	.	kIx.	.
</s>
<s>
Společenstvo	společenstvo	k1gNnSc1	společenstvo
má	mít	k5eAaImIp3nS	mít
určitý	určitý	k2eAgInSc4d1	určitý
ráz	ráz	k1gInSc4	ráz
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc4d1	určený
dominantními	dominantní	k2eAgInPc7d1	dominantní
populacemi	populace	k1gFnPc7	populace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
populace	populace	k1gFnSc2	populace
dubu	dub	k1gInSc2	dub
v	v	k7c6	v
lužním	lužní	k2eAgInSc6d1	lužní
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
trávy	tráva	k1gFnPc4	tráva
na	na	k7c6	na
louce	louka	k1gFnSc6	louka
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc4	význam
populace	populace	k1gFnSc2	populace
určíme	určit	k5eAaPmIp1nP	určit
z	z	k7c2	z
četnosti	četnost	k1gFnSc2	četnost
jejího	její	k3xOp3gInSc2	její
výskytu	výskyt	k1gInSc2	výskyt
nebo	nebo	k8xC	nebo
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
biocenóza	biocenóza	k1gFnSc1	biocenóza
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vymezen	vymezit	k5eAaPmNgInS	vymezit
německým	německý	k2eAgMnSc7d1	německý
zoologem	zoolog	k1gMnSc7	zoolog
a	a	k8xC	a
hydrobiologem	hydrobiolog	k1gMnSc7	hydrobiolog
<g/>
,	,	kIx,	,
zakladatelem	zakladatel	k1gMnSc7	zakladatel
biocenologie	biocenologie	k1gFnSc2	biocenologie
Karlem	Karel	k1gMnSc7	Karel
Augustem	August	k1gMnSc7	August
Möbiem	Möbius	k1gMnSc7	Möbius
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Biocenóza	biocenóza	k1gFnSc1	biocenóza
je	být	k5eAaImIp3nS	být
průměrným	průměrný	k2eAgFnPc3d1	průměrná
vnějším	vnější	k2eAgFnPc3d1	vnější
životním	životní	k2eAgFnPc3d1	životní
podmínkám	podmínka	k1gFnPc3	podmínka
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
výběr	výběr	k1gInSc4	výběr
a	a	k8xC	a
počet	počet	k1gInSc4	počet
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
individuí	individuum	k1gNnPc2	individuum
<g/>
,	,	kIx,	,
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
podmiňujících	podmiňující	k2eAgFnPc2d1	podmiňující
a	a	k8xC	a
ve	v	k7c6	v
vymezeném	vymezený	k2eAgNnSc6d1	vymezené
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
trvale	trvale	k6eAd1	trvale
udržujících	udržující	k2eAgFnPc2d1	udržující
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
geologii	geologie	k1gFnSc6	geologie
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
společný	společný	k2eAgInSc4d1	společný
výskyt	výskyt	k1gInSc4	výskyt
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
kombinace	kombinace	k1gFnSc2	kombinace
fosilních	fosilní	k2eAgInPc2d1	fosilní
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
hornině	hornina	k1gFnSc6	hornina
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc4	název
kongregace	kongregace	k1gFnSc2	kongregace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
společenstva	společenstvo	k1gNnSc2	společenstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
skupinu	skupina	k1gFnSc4	skupina
organismů	organismus	k1gInPc2	organismus
popisuje	popisovat	k5eAaImIp3nS	popisovat
</s>
</p>
<p>
<s>
Zoocenóza	zoocenóza	k1gFnSc1	zoocenóza
-	-	kIx~	-
živočišná	živočišný	k2eAgFnSc1d1	živočišná
část	část	k1gFnSc1	část
</s>
</p>
<p>
<s>
Fytocenóza	fytocenóza	k1gFnSc1	fytocenóza
-	-	kIx~	-
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
část	část	k1gFnSc1	část
</s>
</p>
<p>
<s>
Mikrobiocenóza	Mikrobiocenóza	k1gFnSc1	Mikrobiocenóza
-	-	kIx~	-
mikrobní	mikrobní	k2eAgNnSc1d1	mikrobní
částTyto	částTyto	k1gNnSc1	částTyto
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
dílčí	dílčí	k2eAgNnPc4d1	dílčí
společenstva	společenstvo	k1gNnPc4	společenstvo
(	(	kIx(	(
<g/>
společenstvo	společenstvo	k1gNnSc1	společenstvo
mechů	mech	k1gInPc2	mech
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
původu	původ	k1gInSc2	původ
a	a	k8xC	a
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
člověkem	člověk	k1gMnSc7	člověk
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc4d1	přírodní
-	-	kIx~	-
společenstva	společenstvo	k1gNnPc4	společenstvo
bez	bez	k7c2	bez
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
vlivů	vliv	k1gInPc2	vliv
a	a	k8xC	a
zásahů	zásah	k1gInPc2	zásah
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
téměř	téměř	k6eAd1	téměř
nesetkáváme	setkávat	k5eNaImIp1nP	setkávat
</s>
</p>
<p>
<s>
Přirozené	přirozený	k2eAgNnSc1d1	přirozené
-	-	kIx~	-
společenstva	společenstvo	k1gNnPc4	společenstvo
do	do	k7c2	do
různé	různý	k2eAgFnSc2d1	různá
míry	míra	k1gFnSc2	míra
ovlivněny	ovlivněn	k2eAgInPc1d1	ovlivněn
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
druhovým	druhový	k2eAgNnSc7d1	druhové
složením	složení	k1gNnSc7	složení
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
přírodnímu	přírodní	k2eAgInSc3d1	přírodní
stav	stav	k1gInSc1	stav
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
</s>
</p>
<p>
<s>
Umělá	umělý	k2eAgNnPc1d1	umělé
-	-	kIx~	-
společenstva	společenstvo	k1gNnPc1	společenstvo
vytvářeny	vytvářet	k5eAaImNgInP	vytvářet
člověkem	člověk	k1gMnSc7	člověk
záměrně	záměrně	k6eAd1	záměrně
nebo	nebo	k8xC	nebo
vznikají	vznikat	k5eAaImIp3nP	vznikat
neřízeně	řízeně	k6eNd1	řízeně
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
lidských	lidský	k2eAgFnPc2d1	lidská
činností	činnost	k1gFnPc2	činnost
<g/>
;	;	kIx,	;
společenstva	společenstvo	k1gNnSc2	společenstvo
výhradně	výhradně	k6eAd1	výhradně
lidských	lidský	k2eAgNnPc2d1	lidské
sídlišť	sídliště	k1gNnPc2	sídliště
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
synantropníZ	synantropníZ	k?	synantropníZ
hlediska	hledisko	k1gNnSc2	hledisko
prostorového	prostorový	k2eAgNnSc2d1	prostorové
uspořádání	uspořádání	k1gNnSc2	uspořádání
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
lese	les	k1gInSc6	les
můžeme	moct	k5eAaImIp1nP	moct
vertikálně	vertikálně	k6eAd1	vertikálně
rozlišit	rozlišit	k5eAaPmF	rozlišit
patrovitost	patrovitost	k1gFnSc4	patrovitost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
NADZEMNÍ	nadzemní	k2eAgNnPc1d1	nadzemní
PATRA	patro	k1gNnPc1	patro
</s>
</p>
<p>
<s>
stromové	stromový	k2eAgFnSc3d1	stromová
(	(	kIx(	(
<g/>
korunové	korunový	k2eAgFnSc3d1	korunová
<g/>
)	)	kIx)	)
patro	patro	k1gNnSc1	patro
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
m	m	kA	m
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
keřové	keřový	k2eAgNnSc1d1	keřové
patro	patro	k1gNnSc1	patro
(	(	kIx(	(
<g/>
1	[number]	k4	1
-	-	kIx~	-
3	[number]	k4	3
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
bylinné	bylinný	k2eAgNnSc1d1	bylinné
patro	patro	k1gNnSc1	patro
(	(	kIx(	(
<g/>
0	[number]	k4	0
-	-	kIx~	-
1	[number]	k4	1
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
mechové	mechový	k2eAgNnSc1d1	mechové
patro	patro	k1gNnSc1	patro
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PODZEMNÍ	podzemní	k2eAgNnPc1d1	podzemní
PATRA	patro	k1gNnPc1	patro
</s>
</p>
<p>
<s>
svrchní	svrchní	k2eAgNnSc1d1	svrchní
kořenové	kořenový	k2eAgNnSc1d1	kořenové
patro	patro	k1gNnSc1	patro
(	(	kIx(	(
<g/>
0	[number]	k4	0
-	-	kIx~	-
20	[number]	k4	20
<g/>
cm	cm	kA	cm
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
střední	střední	k2eAgNnSc1d1	střední
kořenové	kořenový	k2eAgNnSc1d1	kořenové
patro	patro	k1gNnSc1	patro
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
cm	cm	kA	cm
-	-	kIx~	-
1	[number]	k4	1
<g/>
m	m	kA	m
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
spodní	spodní	k2eAgNnSc1d1	spodní
kořenové	kořenový	k2eAgNnSc1d1	kořenové
patro	patro	k1gNnSc1	patro
(	(	kIx(	(
<g/>
1	[number]	k4	1
a	a	k8xC	a
více	hodně	k6eAd2	hodně
m	m	kA	m
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
)	)	kIx)	)
<g/>
Prostorově	prostorově	k6eAd1	prostorově
vyhraněnou	vyhraněný	k2eAgFnSc7d1	vyhraněná
součástí	součást	k1gFnSc7	součást
biocenózy	biocenóza	k1gFnSc2	biocenóza
je	být	k5eAaImIp3nS	být
také	také	k9	také
pedocenóza	pedocenóza	k1gFnSc1	pedocenóza
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
společenstvo	společenstvo	k1gNnSc1	společenstvo
půdních	půdní	k2eAgInPc2d1	půdní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
edafon	edafon	k1gInSc1	edafon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekoton	Ekoton	k1gInSc4	Ekoton
==	==	k?	==
</s>
</p>
<p>
<s>
Ekoton	Ekoton	k1gInSc1	Ekoton
je	být	k5eAaImIp3nS	být
přechodná	přechodný	k2eAgFnSc1d1	přechodná
zóna	zóna	k1gFnSc1	zóna
mezi	mezi	k7c7	mezi
společenstvy	společenstvo	k1gNnPc7	společenstvo
(	(	kIx(	(
<g/>
biocenózami	biocenóza	k1gFnPc7	biocenóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
plynulé	plynulý	k2eAgFnSc3d1	plynulá
přeměně	přeměna	k1gFnSc6	přeměna
jedné	jeden	k4xCgFnSc2	jeden
biocenózy	biocenóza	k1gFnSc2	biocenóza
v	v	k7c4	v
druhou	druhý	k4xOgFnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
překrývání	překrývání	k1gNnSc2	překrývání
vzniká	vznikat	k5eAaImIp3nS	vznikat
přechodové	přechodový	k2eAgNnSc1d1	přechodové
společenstvo	společenstvo	k1gNnSc1	společenstvo
(	(	kIx(	(
<g/>
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
několik	několik	k4yIc4	několik
cm	cm	kA	cm
až	až	k9	až
desítky	desítka	k1gFnPc1	desítka
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
zde	zde	k6eAd1	zde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
ekotonálnímu	ekotonální	k2eAgInSc3d1	ekotonální
efektu	efekt	k1gInSc3	efekt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
ekotonu	ekoton	k1gInSc6	ekoton
u	u	k7c2	u
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgInSc4d2	vyšší
počet	počet	k1gInSc4	počet
druhů	druh	k1gInPc2	druh
než	než	k8xS	než
v	v	k7c6	v
kterékoli	kterýkoli	k3yIgFnSc6	kterýkoli
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
sousedních	sousední	k2eAgFnPc2d1	sousední
biocenóz	biocenóza	k1gFnPc2	biocenóza
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
druhy	druh	k1gInPc1	druh
specifické	specifický	k2eAgInPc1d1	specifický
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
přechodovou	přechodový	k2eAgFnSc4d1	přechodová
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
ekotonu	ekoton	k1gInSc2	ekoton
je	být	k5eAaImIp3nS	být
křovinatý	křovinatý	k2eAgInSc1d1	křovinatý
lem	lem	k1gInSc1	lem
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stýká	stýkat	k5eAaImIp3nS	stýkat
lesní	lesní	k2eAgNnSc1d1	lesní
a	a	k8xC	a
luční	luční	k2eAgNnSc1d1	luční
společenstvo	společenstvo	k1gNnSc1	společenstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztahy	vztah	k1gInPc1	vztah
ve	v	k7c6	v
společenstvu	společenstvo	k1gNnSc6	společenstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
organismy	organismus	k1gInPc7	organismus
a	a	k8xC	a
populacemi	populace	k1gFnPc7	populace
existují	existovat	k5eAaImIp3nP	existovat
složité	složitý	k2eAgNnSc1d1	složité
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
<g/>
,	,	kIx,	,
závislosti	závislost	k1gFnPc1	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
vztahy	vztah	k1gInPc1	vztah
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
<g/>
,	,	kIx,	,
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
z	z	k7c2	z
jednodušší	jednoduchý	k2eAgFnSc2d2	jednodušší
na	na	k7c4	na
složitější	složitý	k2eAgFnSc4d2	složitější
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přibývají	přibývat	k5eAaImIp3nP	přibývat
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
patra	patro	k1gNnPc1	patro
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
-	-	kIx~	-
probíhá	probíhat	k5eAaImIp3nS	probíhat
ekologická	ekologický	k2eAgFnSc1d1	ekologická
sukcese	sukcese	k1gFnSc1	sukcese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vztahy	vztah	k1gInPc1	vztah
(	(	kIx(	(
<g/>
interakce	interakce	k1gFnSc1	interakce
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vztahy	vztah	k1gInPc1	vztah
vnitrodruhové	vnitrodruhový	k2eAgInPc1d1	vnitrodruhový
</s>
</p>
<p>
<s>
vztahy	vztah	k1gInPc1	vztah
mezidruhové	mezidruhový	k2eAgInPc1d1	mezidruhový
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgFnPc4d1	související
stránky	stránka	k1gFnPc4	stránka
==	==	k?	==
</s>
</p>
<p>
<s>
Biogeocenóza	Biogeocenóza	k1gFnSc1	Biogeocenóza
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Kendeigh	Kendeigh	k1gMnSc1	Kendeigh
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Charles	Charles	k1gMnSc1	Charles
<g/>
.	.	kIx.	.
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Animal	animal	k1gMnSc1	animal
Ecology	Ecologa	k1gFnSc2	Ecologa
<g/>
.	.	kIx.	.
</s>
<s>
Prentice-Hall	Prentice-Hall	k1gMnSc1	Prentice-Hall
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Englewood	Englewood	k1gInSc1	Englewood
Cliffs	Cliffs	k1gInSc1	Cliffs
<g/>
,	,	kIx,	,
N.	N.	kA	N.
<g/>
J.	J.	kA	J.
<g/>
,	,	kIx,	,
468	[number]	k4	468
p.	p.	k?	p.
</s>
</p>
<p>
<s>
Möbius	Möbius	k1gMnSc1	Möbius
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
<g/>
.	.	kIx.	.
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Auster	Auster	k1gInSc1	Auster
und	und	k?	und
die	die	k?	die
Austernwirtschaft	Austernwirtschaft	k1gInSc1	Austernwirtschaft
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tansley	Tansley	k1gInPc1	Tansley
<g/>
,	,	kIx,	,
A.	A.	kA	A.
G.	G.	kA	G.
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
use	usus	k1gInSc5	usus
and	and	k?	and
abuse	abusus	k1gInSc5	abusus
of	of	k?	of
vegetational	vegetationat	k5eAaPmAgInS	vegetationat
concepts	concepts	k1gInSc1	concepts
and	and	k?	and
terms	terms	k1gInSc1	terms
<g/>
.	.	kIx.	.
</s>
<s>
Ecology	Ecolog	k1gInPc1	Ecolog
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
284	[number]	k4	284
<g/>
-	-	kIx~	-
<g/>
307	[number]	k4	307
<g/>
.	.	kIx.	.
</s>
</p>
