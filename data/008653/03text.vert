<p>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
jednotka	jednotka	k1gFnSc1	jednotka
425.95	[number]	k4	425.95
je	být	k5eAaImIp3nS	být
elektrická	elektrický	k2eAgFnSc1d1	elektrická
jednotka	jednotka	k1gFnSc1	jednotka
používaná	používaný	k2eAgFnSc1d1	používaná
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
Tatranských	tatranský	k2eAgFnPc2d1	Tatranská
elektrických	elektrický	k2eAgFnPc2d1	elektrická
železnic	železnice	k1gFnPc2	železnice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotky	jednotka	k1gFnPc4	jednotka
řady	řada	k1gFnSc2	řada
425.95	[number]	k4	425.95
měly	mít	k5eAaImAgInP	mít
nahradit	nahradit	k5eAaPmF	nahradit
dosluhující	dosluhující	k2eAgFnSc4d1	dosluhující
řadu	řada	k1gFnSc4	řada
420.95	[number]	k4	420.95
(	(	kIx(	(
<g/>
ex	ex	k6eAd1	ex
EMU	Ema	k1gFnSc4	Ema
89.0	[number]	k4	89.0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zajišťovaly	zajišťovat	k5eAaImAgFnP	zajišťovat
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
síti	síť	k1gFnSc6	síť
tatranských	tatranský	k2eAgFnPc2d1	Tatranská
elektrických	elektrický	k2eAgFnPc2d1	elektrická
železnic	železnice	k1gFnPc2	železnice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
prototyp	prototyp	k1gInSc1	prototyp
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
na	na	k7c6	na
dodání	dodání	k1gNnSc6	dodání
14	[number]	k4	14
kusů	kus	k1gInPc2	kus
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
konzorcium	konzorcium	k1gNnSc1	konzorcium
GTW	GTW	kA	GTW
Vysoké	vysoká	k1gFnSc2	vysoká
Tatry	Tatra	k1gFnSc2	Tatra
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
firmami	firma	k1gFnPc7	firma
ŽOS	ŽOS	kA	ŽOS
Vrútky	Vrútka	k1gFnPc1	Vrútka
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
Stadler	Stadler	k1gMnSc1	Stadler
Fahrzeuge	Fahrzeug	k1gFnSc2	Fahrzeug
AG	AG	kA	AG
a	a	k8xC	a
Daimler-Chrysler	Daimler-Chrysler	k1gMnSc1	Daimler-Chrysler
Rail	Rail	k1gMnSc1	Rail
Systems	Systemsa	k1gFnPc2	Systemsa
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ADtranz	ADtranz	k1gInSc1	ADtranz
<g/>
)	)	kIx)	)
<g/>
Vůz	vůz	k1gInSc1	vůz
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
jednotky	jednotka	k1gFnSc2	jednotka
Stadler	Stadler	k1gInSc1	Stadler
GTW	GTW	kA	GTW
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
provozované	provozovaný	k2eAgFnSc2d1	provozovaná
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vůz	vůz	k1gInSc1	vůz
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
vlakového	vlakový	k2eAgNnSc2d1	vlakové
depa	depo	k1gNnSc2	depo
Poprad	Poprad	k1gInSc1	Poprad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
podroben	podrobit	k5eAaPmNgInS	podrobit
zkouškám	zkouška	k1gFnPc3	zkouška
<g/>
,	,	kIx,	,
dodán	dodat	k5eAaPmNgInS	dodat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
sérii	série	k1gFnSc6	série
testů	test	k1gInPc2	test
úspěšně	úspěšně	k6eAd1	úspěšně
schválen	schválit	k5eAaPmNgInS	schválit
a	a	k8xC	a
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
a	a	k8xC	a
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
ještě	ještě	k9	ještě
13	[number]	k4	13
souprav	souprava	k1gFnPc2	souprava
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
motorový	motorový	k2eAgInSc4d1	motorový
článek	článek	k1gInSc4	článek
(	(	kIx(	(
<g/>
prostřední	prostřední	k2eAgFnSc4d1	prostřední
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
na	na	k7c4	na
náhradní	náhradní	k2eAgInPc4d1	náhradní
díly	díl	k1gInPc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zkompletován	zkompletovat	k5eAaPmNgMnS	zkompletovat
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Souprava	souprava	k1gFnSc1	souprava
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
článků	článek	k1gInPc2	článek
<g/>
:	:	kIx,	:
prostředního	prostřední	k2eAgNnSc2d1	prostřední
ocelového	ocelový	k2eAgNnSc2d1	ocelové
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
spojovací	spojovací	k2eAgFnSc2d1	spojovací
uličky	ulička	k1gFnSc2	ulička
osazen	osadit	k5eAaPmNgInS	osadit
motor	motor	k1gInSc1	motor
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
krajních	krajní	k2eAgFnPc2d1	krajní
<g/>
,	,	kIx,	,
vyrobených	vyrobený	k2eAgFnPc2d1	vyrobená
z	z	k7c2	z
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
se	s	k7c7	s
stanovištěm	stanoviště	k1gNnSc7	stanoviště
strojvedoucího	strojvedoucí	k1gMnSc2	strojvedoucí
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Vozidlo	vozidlo	k1gNnSc1	vozidlo
je	být	k5eAaImIp3nS	být
šestinápravové	šestinápravový	k2eAgNnSc1d1	šestinápravový
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
každou	každý	k3xTgFnSc7	každý
částí	část	k1gFnSc7	část
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
dvě	dva	k4xCgFnPc1	dva
nápravy	náprava	k1gFnPc1	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Vozidla	vozidlo	k1gNnPc1	vozidlo
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
vícejazyčným	vícejazyčný	k2eAgInSc7d1	vícejazyčný
vizuálním	vizuální	k2eAgInSc7d1	vizuální
i	i	k8xC	i
hlasovým	hlasový	k2eAgInSc7d1	hlasový
informačním	informační	k2eAgInSc7d1	informační
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vyvedeny	vyvést	k5eAaPmNgFnP	vyvést
v	v	k7c6	v
červenobílém	červenobílý	k2eAgNnSc6d1	červenobílé
barevném	barevný	k2eAgNnSc6d1	barevné
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nedostatečné	dostatečný	k2eNgFnSc3d1	nedostatečná
kapacitě	kapacita	k1gFnSc3	kapacita
bývají	bývat	k5eAaImIp3nP	bývat
na	na	k7c6	na
frekventovaných	frekventovaný	k2eAgFnPc6d1	frekventovaná
spojích	spoj	k1gFnPc6	spoj
jednotky	jednotka	k1gFnSc2	jednotka
spřahány	spřahat	k5eAaImNgInP	spřahat
do	do	k7c2	do
dvojic	dvojice	k1gFnPc2	dvojice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
Tatry	Tatra	k1gFnPc1	Tatra
</s>
</p>
<p>
<s>
Tatranské	tatranský	k2eAgFnPc1d1	Tatranská
elektrické	elektrický	k2eAgFnPc1d1	elektrická
železnice	železnice	k1gFnPc1	železnice
</s>
</p>
<p>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
jednotka	jednotka	k1gFnSc1	jednotka
420.95	[number]	k4	420.95
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
jednotka	jednotka	k1gFnSc1	jednotka
425.95	[number]	k4	425.95
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
jednotce	jednotka	k1gFnSc6	jednotka
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
www.imhd.sk	www.imhd.sk	k1gInSc1	www.imhd.sk
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
Tatranských	tatranský	k2eAgFnPc6d1	Tatranská
elektrických	elektrický	k2eAgFnPc6d1	elektrická
železniciach	železnicia	k1gFnPc6	železnicia
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
spz	spz	k?	spz
<g/>
.	.	kIx.	.
<g/>
logout	logout	k1gMnSc1	logout
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
jednotka	jednotka	k1gFnSc1	jednotka
425.95	[number]	k4	425.95
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
firmy	firma	k1gFnSc2	firma
ŽOS	ŽOS	kA	ŽOS
Vrútky	Vrútka	k1gFnSc2	Vrútka
(	(	kIx(	(
<g/>
technický	technický	k2eAgInSc1d1	technický
popis	popis	k1gInSc1	popis
<g/>
,	,	kIx,	,
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
<g/>
)	)	kIx)	)
</s>
</p>
