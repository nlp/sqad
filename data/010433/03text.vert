<p>
<s>
Papacha	papacha	k1gFnSc1	papacha
(	(	kIx(	(
<g/>
п	п	k?	п
<g/>
́	́	k?	́
<g/>
х	х	k?	х
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čepice	čepice	k1gFnPc4	čepice
tvořící	tvořící	k2eAgFnPc4d1	tvořící
součást	součást	k1gFnSc4	součást
kroje	kroj	k1gInSc2	kroj
kavkazských	kavkazský	k2eAgMnPc2d1	kavkazský
horalů	horal	k1gMnPc2	horal
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zhotovena	zhotovit	k5eAaPmNgFnS	zhotovit
z	z	k7c2	z
ovčí	ovčí	k2eAgFnSc2d1	ovčí
vlny	vlna	k1gFnSc2	vlna
(	(	kIx(	(
<g/>
dražší	drahý	k2eAgInPc1d2	dražší
typy	typ	k1gInPc1	typ
z	z	k7c2	z
karakulské	karakulský	k2eAgFnSc2d1	karakulský
vlny	vlna	k1gFnSc2	vlna
nebo	nebo	k8xC	nebo
z	z	k7c2	z
vlny	vlna	k1gFnSc2	vlna
kašmírských	kašmírský	k2eAgFnPc2d1	kašmírská
koz	koza	k1gFnPc2	koza
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
tvar	tvar	k1gInSc4	tvar
vysokého	vysoký	k2eAgInSc2d1	vysoký
válce	válec	k1gInSc2	válec
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
také	také	k9	také
papachy	papacha	k1gFnPc4	papacha
zašpičatělé	zašpičatělý	k2eAgFnPc4d1	zašpičatělá
nebo	nebo	k8xC	nebo
baňaté	baňatý	k2eAgFnPc4d1	baňatá
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
opatřeny	opatřen	k2eAgInPc1d1	opatřen
stahovacími	stahovací	k2eAgFnPc7d1	stahovací
klapkami	klapka	k1gFnPc7	klapka
na	na	k7c4	na
uši	ucho	k1gNnPc4	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Novodobé	novodobý	k2eAgFnPc1d1	novodobá
papachy	papacha	k1gFnPc1	papacha
mívají	mívat	k5eAaImIp3nP	mívat
zpravidla	zpravidla	k6eAd1	zpravidla
látkovou	látkový	k2eAgFnSc4d1	látková
podšívku	podšívka	k1gFnSc4	podšívka
a	a	k8xC	a
dýnko	dýnko	k1gNnSc4	dýnko
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
papacha	papacha	k1gFnSc1	papacha
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
kubáňka	kubáňka	k1gFnSc1	kubáňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
se	se	k3xPyFc4	se
papachy	papacha	k1gFnPc1	papacha
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
také	také	k9	také
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
oblastí	oblast	k1gFnPc2	oblast
ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
do	do	k7c2	do
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
ujaly	ujmout	k5eAaPmAgFnP	ujmout
se	se	k3xPyFc4	se
také	také	k9	také
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
Polesí	Polesí	k1gNnSc2	Polesí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	on	k3xPp3gNnPc4	on
začali	začít	k5eAaPmAgMnP	začít
nosit	nosit	k5eAaImF	nosit
Kozáci	Kozák	k1gMnPc1	Kozák
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yQgFnPc2	který
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
zimní	zimní	k2eAgFnSc2d1	zimní
uniformy	uniforma	k1gFnSc2	uniforma
převzaly	převzít	k5eAaPmAgFnP	převzít
i	i	k9	i
další	další	k2eAgFnPc1d1	další
složky	složka	k1gFnPc1	složka
carské	carský	k2eAgFnSc2d1	carská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
byly	být	k5eAaImAgFnP	být
zavrženy	zavrhnout	k5eAaPmNgFnP	zavrhnout
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
starého	starý	k2eAgInSc2d1	starý
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
znovu	znovu	k6eAd1	znovu
nařídil	nařídit	k5eAaPmAgMnS	nařídit
jejich	jejich	k3xOp3gNnSc4	jejich
používání	používání	k1gNnSc4	používání
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
pořizovací	pořizovací	k2eAgFnSc3d1	pořizovací
ceně	cena	k1gFnSc3	cena
je	on	k3xPp3gMnPc4	on
nosili	nosit	k5eAaImAgMnP	nosit
pouze	pouze	k6eAd1	pouze
důstojníci	důstojník	k1gMnPc1	důstojník
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
další	další	k2eAgMnPc1d1	další
sovětští	sovětský	k2eAgMnPc1d1	sovětský
prominenti	prominent	k1gMnPc1	prominent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
ušankou	ušanka	k1gFnSc7	ušanka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
plukovnické	plukovnický	k2eAgFnPc1d1	plukovnická
a	a	k8xC	a
generálské	generálský	k2eAgFnPc1d1	generálská
uniformy	uniforma	k1gFnPc1	uniforma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Papacha	papacha	k1gFnSc1	papacha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
В	В	k?	В
Ж	Ж	k?	Ж
</s>
</p>
<p>
<s>
Т	Т	k?	Т
н	н	k?	н
К	К	k?	К
</s>
</p>
