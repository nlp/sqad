<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
třemi	tři	k4xCgInPc7	tři
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
tmavomodrým	tmavomodrý	k2eAgInSc7d1	tmavomodrý
<g/>
,	,	kIx,	,
žlutým	žlutý	k2eAgInSc7d1	žlutý
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiální	neoficiální	k2eAgInSc1d1	neoficiální
výklad	výklad	k1gInSc1	výklad
symboliky	symbolika	k1gFnSc2	symbolika
barev	barva	k1gFnPc2	barva
na	na	k7c6	na
rumunské	rumunský	k2eAgFnSc6d1	rumunská
vlajce	vlajka	k1gFnSc6	vlajka
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
bezmračnou	bezmračný	k2eAgFnSc4d1	bezmračná
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
žlutá	žlutat	k5eAaImIp3nS	žlutat
nerostné	nerostný	k2eAgNnSc1d1	nerostné
bohatství	bohatství	k1gNnSc1	bohatství
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
udatnost	udatnost	k1gFnSc1	udatnost
rumunského	rumunský	k2eAgInSc2d1	rumunský
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
dvě	dva	k4xCgNnPc4	dva
knížectví	knížectví	k1gNnPc4	knížectví
<g/>
:	:	kIx,	:
Moldavské	moldavský	k2eAgNnSc4d1	moldavské
knížectví	knížectví	k1gNnSc4	knížectví
a	a	k8xC	a
Valašské	valašský	k2eAgNnSc4d1	Valašské
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stala	stát	k5eAaPmAgFnS	stát
osmanskými	osmanský	k2eAgInPc7d1	osmanský
vazalskými	vazalský	k2eAgInPc7d1	vazalský
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rusko-turecké	ruskourecký	k2eAgFnSc6d1	rusko-turecká
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
–	–	k?	–
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
obě	dva	k4xCgNnPc4	dva
knížectví	knížectví	k1gNnPc2	knížectví
protektorátem	protektorát	k1gInSc7	protektorát
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgInP	být
opět	opět	k6eAd1	opět
otevřeny	otevřen	k2eAgInPc1d1	otevřen
přístavy	přístav	k1gInPc1	přístav
a	a	k8xC	a
vytvořeno	vytvořen	k2eAgNnSc1d1	vytvořeno
moldavsko-valašské	moldavskoalašský	k2eAgNnSc1d1	moldavsko-valašský
loďstvo	loďstvo	k1gNnSc1	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
užívalo	užívat	k5eAaImAgNnS	užívat
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
červeno-modré	červenoodrý	k2eAgNnSc1d1	červeno-modré
a	a	k8xC	a
pro	pro	k7c4	pro
Valašsko	Valašsko	k1gNnSc4	Valašsko
žluto-modré	žlutoodrý	k2eAgInPc4d1	žluto-modrý
praporce	praporec	k1gInPc4	praporec
s	s	k7c7	s
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
první	první	k4xOgFnSc4	první
rumunskou	rumunský	k2eAgFnSc4d1	rumunská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.24	.24	k4	.24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1834	[number]	k4	1834
dovolil	dovolit	k5eAaPmAgInS	dovolit
výnosem	výnos	k1gInSc7	výnos
sultán	sultán	k1gMnSc1	sultán
Mahmut	Mahmut	k1gMnSc1	Mahmut
II	II	kA	II
<g/>
.	.	kIx.	.
valašskému	valašský	k2eAgMnSc3d1	valašský
princi	princ	k1gMnSc3	princ
Alexandru	Alexandr	k1gMnSc3	Alexandr
Ghicovi	Ghiec	k1gMnSc3	Ghiec
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
-	-	kIx~	-
en	en	k?	en
wiki	wik	k1gFnSc2	wik
-	-	kIx~	-
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Ghica	Ghica	k1gMnSc1	Ghica
princem	princ	k1gMnSc7	princ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
moldavským	moldavský	k2eAgInPc3d1	moldavský
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
přijmout	přijmout	k5eAaPmF	přijmout
vlajku	vlajka	k1gFnSc4	vlajka
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlediska	hledisko	k1gNnSc2	hledisko
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c7	za
knížecí	knížecí	k2eAgFnSc7d1	knížecí
i	i	k8xC	i
státní	státní	k2eAgFnSc7d1	státní
<g/>
.	.	kIx.	.
</s>
<s>
Tvořil	tvořit	k5eAaImAgMnS	tvořit
ji	on	k3xPp3gFnSc4	on
list	list	k1gInSc1	list
o	o	k7c4	o
poměru	poměra	k1gFnSc4	poměra
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
-	-	kIx~	-
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
modrým	modrý	k2eAgInSc7d1	modrý
a	a	k8xC	a
žlutým	žlutý	k2eAgInSc7d1	žlutý
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
šířek	šířka	k1gFnPc2	šířka
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horním	horní	k2eAgMnSc6d1	horní
<g/>
,	,	kIx,	,
červeném	červený	k2eAgInSc6d1	červený
pruhu	pruh	k1gInSc6	pruh
bylo	být	k5eAaImAgNnS	být
osm	osm	k4xCc1	osm
bílých	bílý	k1gMnPc2	bílý
<g/>
,	,	kIx,	,
osmicípých	osmicípý	k2eAgFnPc2d1	osmicípá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
oblasti	oblast	k1gFnPc4	oblast
horního	horní	k2eAgNnSc2d1	horní
Valašska	Valašsko	k1gNnSc2	Valašsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolním	dolní	k2eAgMnSc6d1	dolní
<g/>
,	,	kIx,	,
žlutém	žlutý	k2eAgInSc6d1	žlutý
pruhu	pruh	k1gInSc6	pruh
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
sedm	sedm	k4xCc1	sedm
bílých	bílý	k2eAgFnPc2d1	bílá
osmicípých	osmicípý	k2eAgFnPc2d1	osmicípá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
symbolizujících	symbolizující	k2eAgFnPc2d1	symbolizující
oblasti	oblast	k1gFnSc3	oblast
dolního	dolní	k2eAgNnSc2d1	dolní
Valašska	Valašsko	k1gNnSc2	Valašsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředním	prostřední	k2eAgMnSc6d1	prostřední
<g/>
,	,	kIx,	,
modrém	modrý	k2eAgInSc6d1	modrý
pruhu	pruh	k1gInSc6	pruh
byla	být	k5eAaImAgFnS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
korunovaná	korunovaný	k2eAgFnSc1d1	korunovaná
valašská	valašský	k2eAgFnSc1d1	Valašská
orlice	orlice	k1gFnSc1	orlice
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
vlajky	vlajka	k1gFnSc2	vlajka
bylo	být	k5eAaImAgNnS	být
změněno	změnit	k5eAaPmNgNnS	změnit
pořadí	pořadí	k1gNnSc4	pořadí
barev	barva	k1gFnPc2	barva
na	na	k7c4	na
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
žlutou	žlutý	k2eAgFnSc4d1	žlutá
a	a	k8xC	a
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
.	.	kIx.	.
<g/>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
červeno-žluto-modré	červeno-žlutoodrý	k2eAgFnPc1d1	červeno-žluto-modrý
vlajky	vlajka	k1gFnPc1	vlajka
s	s	k7c7	s
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
širokými	široký	k2eAgInPc7d1	široký
pruhy	pruh	k1gInPc7	pruh
<g/>
.14	.14	k4	.14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1848	[number]	k4	1848
přijala	přijmout	k5eAaPmAgFnS	přijmout
revoluční	revoluční	k2eAgFnSc1d1	revoluční
valašská	valašský	k2eAgFnSc1d1	Valašská
vláda	vláda	k1gFnSc1	vláda
vlajku	vlajka	k1gFnSc4	vlajka
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
žlutým	žlutý	k2eAgInSc7d1	žlutý
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
prostředním	prostřední	k2eAgMnSc6d1	prostřední
<g/>
,	,	kIx,	,
žlutém	žlutý	k2eAgInSc6d1	žlutý
pruhu	pruh	k1gInSc6	pruh
byl	být	k5eAaImAgInS	být
nápis	nápis	k1gInSc1	nápis
DREPTATE	DREPTATE	kA	DREPTATE
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
(	(	kIx(	(
<g/>
valašsky	valašsky	k6eAd1	valašsky
spravedlivost	spravedlivost	k1gFnSc1	spravedlivost
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
nápis	nápis	k1gInSc1	nápis
v	v	k7c6	v
cyrilici	cyrilice	k1gFnSc6	cyrilice
Ф	Ф	k?	Ф
(	(	kIx(	(
<g/>
FRATIE	FRATIE	kA	FRATIE
<g/>
,	,	kIx,	,
moldavsky	moldavsky	k6eAd1	moldavsky
bratrství	bratrství	k1gNnSc1	bratrství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1848	[number]	k4	1848
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
změněna	změnit	k5eAaPmNgFnS	změnit
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgInSc2d1	francouzský
vzoru	vzor	k1gInSc2	vzor
na	na	k7c4	na
trikolóru	trikolóra	k1gFnSc4	trikolóra
s	s	k7c7	s
modro-žluto-červenými	modro-žluto-červený	k2eAgInPc7d1	modro-žluto-červený
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgFnSc1d1	populární
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
později	pozdě	k6eAd2	pozdě
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
rumunskou	rumunský	k2eAgFnSc4d1	rumunská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
(	(	kIx(	(
<g/>
stále	stále	k6eAd1	stále
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
užívání	užívání	k1gNnSc1	užívání
předchozí	předchozí	k2eAgFnPc1d1	předchozí
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
byl	být	k5eAaImAgMnS	být
knížetem	kníže	k1gMnSc7	kníže
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
i	i	k8xC	i
Valašska	Valašsko	k1gNnSc2	Valašsko
(	(	kIx(	(
<g/>
neformálně	formálně	k6eNd1	formálně
označovaná	označovaný	k2eAgNnPc1d1	označované
jako	jako	k8xS	jako
Dunajská	dunajský	k2eAgNnPc1d1	Dunajské
knížectví	knížectví	k1gNnPc1	knížectví
<g/>
)	)	kIx)	)
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Alexandr	Alexandr	k1gMnSc1	Alexandr
Ioan	Ioan	k1gMnSc1	Ioan
Cuza	Cuza	k1gMnSc1	Cuza
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
obě	dva	k4xCgFnPc1	dva
knížectví	knížectví	k1gNnSc2	knížectví
spojil	spojit	k5eAaPmAgMnS	spojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
do	do	k7c2	do
útvaru	útvar	k1gInSc2	útvar
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Sjednocená	sjednocený	k2eAgNnPc4d1	sjednocené
knížectví	knížectví	k1gNnPc4	knížectví
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
a	a	k8xC	a
Valašska	Valašsko	k1gNnSc2	Valašsko
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1861	[number]	k4	1861
se	se	k3xPyFc4	se
národní	národní	k2eAgFnSc7d1	národní
vlajkou	vlajka	k1gFnSc7	vlajka
stal	stát	k5eAaPmAgInS	stát
list	list	k1gInSc1	list
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
stran	strana	k1gFnPc2	strana
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
a	a	k8xC	a
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
žlutým	žlutý	k2eAgInSc7d1	žlutý
a	a	k8xC	a
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
listem	list	k1gInSc7	list
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
modrý	modrý	k2eAgInSc1d1	modrý
plamen	plamen	k1gInSc1	plamen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
na	na	k7c6	na
obrázcích	obrázek	k1gInPc6	obrázek
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Válečná	válečný	k2eAgFnSc1d1	válečná
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
možno	možno	k6eAd1	možno
ji	on	k3xPp3gFnSc4	on
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
za	za	k7c4	za
státní	státní	k2eAgInSc4d1	státní
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
navíc	navíc	k6eAd1	navíc
uprostřed	uprostřed	k6eAd1	uprostřed
znak	znak	k1gInSc1	znak
reprezentující	reprezentující	k2eAgNnPc4d1	reprezentující
obě	dva	k4xCgNnPc4	dva
knížectví	knížectví	k1gNnPc4	knížectví
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
knížecí	knížecí	k2eAgFnSc4d1	knížecí
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
orlicí	orlice	k1gFnSc7	orlice
s	s	k7c7	s
křížem	kříž	k1gInSc7	kříž
v	v	k7c6	v
zobáku	zobák	k1gInSc6	zobák
<g/>
,	,	kIx,	,
mečem	meč	k1gInSc7	meč
a	a	k8xC	a
žezlem	žezlo	k1gNnSc7	žezlo
(	(	kIx(	(
<g/>
Valašsko	Valašsko	k1gNnSc1	Valašsko
<g/>
)	)	kIx)	)
v	v	k7c6	v
pařátech	pařát	k1gInPc6	pařát
a	a	k8xC	a
zubří	zubří	k2eAgFnSc7d1	zubří
hlavou	hlava	k1gFnSc7	hlava
s	s	k7c7	s
šesticípou	šesticípý	k2eAgFnSc7d1	šesticípá
hvězdou	hvězda	k1gFnSc7	hvězda
(	(	kIx(	(
<g/>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
šesti	šest	k4xCc7	šest
(	(	kIx(	(
<g/>
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
národními	národní	k2eAgInPc7d1	národní
prapory	prapor	k1gInPc7	prapor
se	s	k7c7	s
žerděmi	žerď	k1gFnPc7	žerď
zkříženými	zkřížený	k2eAgFnPc7d1	zkřížená
pod	pod	k7c7	pod
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1861	[number]	k4	1861
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Rumunské	rumunský	k2eAgNnSc4d1	rumunské
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
knížetem	kníže	k1gNnSc7wR	kníže
Karel	Karla	k1gFnPc2	Karla
I.	I.	kA	I.
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Hohenzollern-Sigmaringen	Hohenzollern-Sigmaringen	k1gInSc1	Hohenzollern-Sigmaringen
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vlajka	vlajka	k1gFnSc1	vlajka
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
žlutým	žlutý	k2eAgInSc7d1	žlutý
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
doplněna	doplnit	k5eAaPmNgFnS	doplnit
knížecím	knížecí	k2eAgInSc7d1	knížecí
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
změněn	změněn	k2eAgInSc4d1	změněn
<g/>
.9	.9	k4	.9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1877	[number]	k4	1877
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
na	na	k7c6	na
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
drobné	drobný	k2eAgFnSc3d1	drobná
změně	změna	k1gFnSc3	změna
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodně	mezinárodně	k6eAd1	mezinárodně
byla	být	k5eAaImAgFnS	být
nezávislost	nezávislost	k1gFnSc1	nezávislost
uznána	uznán	k2eAgFnSc1d1	uznána
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1881	[number]	k4	1881
bylo	být	k5eAaImAgNnS	být
Rumunské	rumunský	k2eAgNnSc1d1	rumunské
knížectví	knížectví	k1gNnSc1	knížectví
povýšeno	povýšen	k2eAgNnSc1d1	povýšeno
na	na	k7c6	na
království	království	k1gNnSc6	království
ale	ale	k8xC	ale
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
Za	za	k7c2	za
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
získalo	získat	k5eAaPmAgNnS	získat
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
další	další	k2eAgNnSc1d1	další
území	území	k1gNnSc1	území
<g/>
:	:	kIx,	:
Bukovinu	Bukovina	k1gFnSc4	Bukovina
<g/>
,	,	kIx,	,
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
Banátu	Banát	k1gInSc2	Banát
a	a	k8xC	a
Besarábii	Besarábie	k1gFnSc4	Besarábie
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tak	tak	k9	tak
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgNnSc1d1	velké
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
zaveden	zavést	k5eAaPmNgInS	zavést
nový	nový	k2eAgInSc1d1	nový
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc4d1	velký
<g/>
,	,	kIx,	,
střední	střední	k2eAgInSc4d1	střední
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
se	se	k3xPyFc4	se
užíval	užívat	k5eAaImAgInS	užívat
běžně	běžně	k6eAd1	běžně
znak	znak	k1gInSc1	znak
střední	střední	k2eAgInSc1d1	střední
i	i	k8xC	i
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgMnS	být
rumunský	rumunský	k2eAgMnSc1d1	rumunský
král	král	k1gMnSc1	král
Michal	Michal	k1gMnSc1	Michal
I.	I.	kA	I.
donucen	donutit	k5eAaPmNgMnS	donutit
k	k	k7c3	k
abdikaci	abdikace	k1gFnSc3	abdikace
a	a	k8xC	a
k	k	k7c3	k
emigraci	emigrace	k1gFnSc3	emigrace
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
prosovětská	prosovětský	k2eAgFnSc1d1	prosovětská
Rumunská	rumunský	k2eAgFnSc1d1	rumunská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ji	on	k3xPp3gFnSc4	on
znovu	znovu	k6eAd1	znovu
list	list	k1gInSc1	list
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
žlutým	žlutý	k2eAgInSc7d1	žlutý
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.24	.24	k4	.24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1952	[number]	k4	1952
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
znaku	znak	k1gInSc2	znak
byl	být	k5eAaImAgInS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
symbol	symbol	k1gInSc1	symbol
komunistické	komunistický	k2eAgFnSc2d1	komunistická
moci	moc	k1gFnSc2	moc
-	-	kIx~	-
rudá	rudý	k2eAgFnSc1d1	rudá
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.21	.21	k4	.21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1965	[number]	k4	1965
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
na	na	k7c6	na
Rumunská	rumunský	k2eAgFnSc1d1	rumunská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
zmenšena	zmenšen	k2eAgFnSc1d1	zmenšena
rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
na	na	k7c6	na
stuze	stuha	k1gFnSc6	stuha
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
lidového	lidový	k2eAgNnSc2d1	lidové
povstání	povstání	k1gNnSc2	povstání
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
vystřiženým	vystřižený	k2eAgInSc7d1	vystřižený
znakem	znak	k1gInSc7	znak
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
komunistickým	komunistický	k2eAgInSc7d1	komunistický
znakem	znak	k1gInSc7	znak
užívala	užívat	k5eAaImAgFnS	užívat
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Rady	rada	k1gFnSc2	rada
Fronty	fronta	k1gFnSc2	fronta
národní	národní	k2eAgFnSc2d1	národní
spásy	spása	k1gFnSc2	spása
byl	být	k5eAaImAgInS	být
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
data	datum	k1gNnSc2	datum
ze	z	k7c2	z
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
vypuštěn	vypuštěn	k2eAgInSc1d1	vypuštěn
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
č.	č.	k?	č.
12	[number]	k4	12
<g/>
,	,	kIx,	,
hlavy	hlava	k1gFnPc1	hlava
I.	I.	kA	I.
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
přijaté	přijatý	k2eAgNnSc1d1	přijaté
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
Čadem	Čad	k1gInSc7	Čad
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
znaku	znak	k1gInSc2	znak
z	z	k7c2	z
rumunské	rumunský	k2eAgFnSc2d1	rumunská
vlajky	vlajka	k1gFnSc2	vlajka
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spor	spor	k1gInSc1	spor
s	s	k7c7	s
africkým	africký	k2eAgInSc7d1	africký
Čadem	Čad	k1gInSc7	Čad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
shodnou	shodný	k2eAgFnSc4d1	shodná
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
užívá	užívat	k5eAaImIp3nS	užívat
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
o	o	k7c4	o
něco	něco	k3yInSc4	něco
tmavší	tmavý	k2eAgInSc1d2	tmavší
odstín	odstín	k1gInSc1	odstín
modrého	modrý	k2eAgInSc2d1	modrý
pruhu	pruh	k1gInSc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Čad	Čad	k1gInSc1	Čad
přednesl	přednést	k5eAaPmAgInS	přednést
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
v	v	k7c6	v
OSN	OSN	kA	OSN
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
k	k	k7c3	k
vlajce	vlajka	k1gFnSc3	vlajka
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
se	se	k3xPyFc4	se
nepředpokládá	předpokládat	k5eNaImIp3nS	předpokládat
pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
rumunské	rumunský	k2eAgFnSc2d1	rumunská
veřejnosti	veřejnost	k1gFnSc2	veřejnost
(	(	kIx(	(
<g/>
vlajka	vlajka	k1gFnSc1	vlajka
by	by	kYmCp3nS	by
příliš	příliš	k6eAd1	příliš
připomínala	připomínat	k5eAaImAgFnS	připomínat
vlajky	vlajka	k1gFnPc4	vlajka
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
komunismu	komunismus	k1gInSc2	komunismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
</s>
</p>
<p>
<s>
Rumunská	rumunský	k2eAgFnSc1d1	rumunská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rumunská	rumunský	k2eAgFnSc1d1	rumunská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
