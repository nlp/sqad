<s>
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
AC	AC	kA
Milán	Milán	k1gInSc1
Název	název	k1gInSc1
</s>
<s>
Associazione	Associazion	k1gInSc5
Calcio	Calcio	k1gMnSc1
Milan	Milan	k1gMnSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Rossoneri	Rossoner	k1gMnPc1
(	(	kIx(
<g/>
Červenočerní	červenočerný	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Il	Il	k1gMnSc1
Diavolo	Diavola	k1gFnSc5
Země	zem	k1gFnSc2
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Milán	Milán	k1gInSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1899	#num#	k4
(	(	kIx(
<g/>
Milan	Milan	k1gMnSc1
Foot-Ball	Foot-Ball	k1gMnSc1
and	and	k?
Cricket	Cricket	k1gInSc1
Club	club	k1gInSc1
<g/>
)	)	kIx)
Asociace	asociace	k1gFnSc1
</s>
<s>
FIGC	FIGC	kA
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_acmilan	_acmilan	k1gInSc1
<g/>
1920	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_acmilan	_acmilan	k1gMnSc1
<g/>
1920	#num#	k4
<g/>
a	a	k8xC
<g/>
|	|	kIx~
<g/>
link	link	k1gInSc1
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Chyba	chyba	k1gFnSc1
v	v	k7c6
šabloně	šablona	k1gFnSc6
{{	{{	k?
<g/>
Fotbalový	fotbalový	k2eAgInSc4d1
dres	dres	k1gInSc4
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
:	:	kIx,
Hodnota	hodnota	k1gFnSc1
parametru	parametr	k1gInSc2
"	"	kIx"
<g/>
vzor_trenýrek	vzor_trenýrka	k1gFnPc2
<g/>
"	"	kIx"
nerozpoznána	rozpoznán	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Chyba	chyba	k1gFnSc1
v	v	k7c6
šabloně	šablona	k1gFnSc6
{{	{{	k?
<g/>
Fotbalový	fotbalový	k2eAgInSc4d1
dres	dres	k1gInSc4
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
:	:	kIx,
Hodnota	hodnota	k1gFnSc1
parametru	parametr	k1gInSc2
"	"	kIx"
<g/>
vzor_ponožek	vzor_ponožka	k1gFnPc2
<g/>
"	"	kIx"
nerozpoznána	rozpoznán	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_acmilan	_acmilan	k1gInSc1
<g/>
1920	#num#	k4
<g/>
t	t	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
italská	italský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Stadion	stadion	k1gInSc1
</s>
<s>
Stadio	Stadia	k1gMnSc5
Giuseppe	Giusepp	k1gMnSc5
MeazzaMilán	MeazzaMilán	k1gInSc1
<g/>
,	,	kIx,
San	San	k1gFnSc1
Siro	siro	k6eAd1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
9	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
26	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
80	#num#	k4
018	#num#	k4
Vedení	vedení	k1gNnPc2
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Elliott	Elliott	k2eAgInSc1d1
Management	management	k1gInSc1
Corporation	Corporation	k1gInSc1
Předseda	předseda	k1gMnSc1
</s>
<s>
Paolo	Paola	k1gFnSc5
Scaroni	Scaroň	k1gFnSc5
Trenér	trenér	k1gMnSc1
</s>
<s>
Stefano	Stefana	k1gFnSc5
Pioli	Piol	k1gFnSc5
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
Ligové	ligový	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
18	#num#	k4
<g/>
x	x	k?
Serie	serie	k1gFnSc2
A	a	k8xC
</s>
<s>
2	#num#	k4
<g/>
x	x	k?
Serie	serie	k1gFnSc2
B	B	kA
Domácí	domácí	k2eAgFnPc4d1
trofeje	trofej	k1gFnPc4
</s>
<s>
5	#num#	k4
<g/>
x	x	k?
Pohár	pohár	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
x	x	k?
Superpohár	superpohár	k1gInSc4
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
7	#num#	k4
<g/>
x	x	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
</s>
<s>
2	#num#	k4
<g/>
x	x	k?
Pohár	pohár	k1gInSc1
PVP	PVP	kA
</s>
<s>
3	#num#	k4
<g/>
x	x	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
1	#num#	k4
<g/>
xMS	xMS	k?
klubů	klub	k1gInPc2
</s>
<s>
5	#num#	k4
<g/>
x	x	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
x	x	k?
Středoevropský	středoevropský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
Serie	serie	k1gFnSc1
A	a	k9
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
AC	AC	kA
Milán	Milán	k1gInSc1
je	být	k5eAaImIp3nS
italský	italský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
hrající	hrající	k2eAgMnSc1d1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
ve	v	k7c4
nejvyšší	vysoký	k2eAgFnSc4d3
ligu	liga	k1gFnSc4
sídlící	sídlící	k2eAgFnSc1d1
ve	v	k7c6
městě	město	k1gNnSc6
Milán	Milán	k1gInSc1
v	v	k7c6
regionu	region	k1gInSc6
Lombardie	Lombardie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Klub	klub	k1gInSc1
hraje	hrát	k5eAaImIp3nS
již	již	k9
109	#num#	k4
sezonu	sezona	k1gFnSc4
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
lize	liga	k1gFnSc6
a	a	k8xC
nepřetržitě	přetržitě	k6eNd1
od	od	k7c2
sezony	sezona	k1gFnSc2
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
lize	liga	k1gFnSc6
hráli	hrát	k5eAaImAgMnP
jen	jen	k9
ve	v	k7c6
dvou	dva	k4xCgFnPc6
sezonách	sezona	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
kontě	konto	k1gNnSc6
již	již	k6eAd1
18	#num#	k4
prvenství	prvenství	k1gNnSc2
v	v	k7c6
lize	liga	k1gFnSc6
což	což	k3yRnSc1,k3yQnSc1
jej	on	k3xPp3gMnSc4
řadí	řadit	k5eAaImIp3nS
na	na	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
společně	společně	k6eAd1
s	s	k7c7
rivalem	rival	k1gMnSc7
z	z	k7c2
Interu	Inter	k1gInSc2
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
počtu	počet	k1gInSc3
prvenství	prvenství	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
je	být	k5eAaImIp3nS
nejúspěšnějším	úspěšný	k2eAgInSc7d3
klubem	klub	k1gInSc7
z	z	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokázalo	dokázat	k5eAaPmAgNnS
získat	získat	k5eAaPmF
sedm	sedm	k4xCc1
trofejí	trofej	k1gFnPc2
v	v	k7c6
lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
z	z	k7c2
jedenácti	jedenáct	k4xCc2
účastí	účast	k1gFnPc2
ve	v	k7c6
finále	finále	k1gNnSc6
<g/>
,	,	kIx,
pět	pět	k4xCc4
trofejí	trofej	k1gFnPc2
v	v	k7c6
evropském	evropský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
což	což	k3yQnSc4,k3yRnSc4
je	být	k5eAaImIp3nS
společně	společně	k6eAd1
ze	z	k7c2
Španělským	španělský	k2eAgInSc7d1
klubem	klub	k1gInSc7
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
rekord	rekord	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgMnS
také	také	k9
tři	tři	k4xCgFnPc1
trofeje	trofej	k1gFnPc1
interkontinentálního	interkontinentální	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgInS
48	#num#	k4
trofejí	trofej	k1gFnPc2
(	(	kIx(
<g/>
30	#num#	k4
národních	národní	k2eAgFnPc2d1
a	a	k8xC
18	#num#	k4
mezinárodních	mezinárodní	k2eAgFnPc2d1
<g/>
)	)	kIx)
což	což	k3yRnSc1,k3yQnSc1
jej	on	k3xPp3gMnSc4
řadí	řadit	k5eAaImIp3nS
na	na	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
za	za	k7c7
klubem	klub	k1gInSc7
Juventus	Juventus	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
67	#num#	k4
trofejí	trofej	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
prvním	první	k4xOgInSc7
klubem	klub	k1gInSc7
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
vyhrál	vyhrát	k5eAaPmAgMnS
titul	titul	k1gInSc4
bez	bez	k7c2
porážky	porážka	k1gFnSc2
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Změny	změna	k1gFnPc1
názvu	název	k1gInSc2
klubu	klub	k1gInSc2
</s>
<s>
1900	#num#	k4
–	–	k?
1918	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
–	–	k?
Milán	Milán	k1gInSc1
FBCC	FBCC	kA
(	(	kIx(
<g/>
Milan	Milan	k1gMnSc1
Foot-Ball	Foot-Ball	k1gMnSc1
and	and	k?
Cricket	Cricket	k1gInSc1
Club	club	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
–	–	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
–	–	k?
Milán	Milán	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
Milan	Milan	k1gMnSc1
Football	Football	k1gMnSc1
Club	club	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
–	–	k?
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
–	–	k?
Milán	Milán	k1gInSc1
AS	as	k9
(	(	kIx(
<g/>
Milan	Milan	k1gMnSc1
Associazione	Associazion	k1gInSc5
Sportiva	Sportivum	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
–	–	k?
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
–	–	k?
AC	AC	kA
Miláno	Milána	k1gFnSc5
(	(	kIx(
<g/>
Associazione	Associazion	k1gInSc5
Calcio	Calcia	k1gFnSc5
Milano	Milana	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
–	–	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
–	–	k?
AC	AC	kA
Milán	Milán	k1gInSc1
(	(	kIx(
<g/>
Associazione	Associazion	k1gInSc5
Calcio	Calcio	k1gNnSc1
Milan	Milan	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
–	–	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
–	–	k?
Milán	Milán	k1gInSc1
AC	AC	kA
(	(	kIx(
<g/>
Milan	Milan	k1gMnSc1
Associazione	Associazion	k1gInSc5
Calcio	Calcio	k1gNnSc5
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
–	–	k?
AC	AC	kA
Milán	Milán	k1gInSc1
(	(	kIx(
<g/>
Associazione	Associazion	k1gInSc5
Calcio	Calcio	k1gNnSc1
Milan	Milan	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Získané	získaný	k2eAgFnPc4d1
trofeje	trofej	k1gFnPc4
</s>
<s>
Vyhrané	vyhraný	k2eAgFnPc1d1
domácí	domácí	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
italská	italský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
(	(	kIx(
18	#num#	k4
<g/>
x	x	k?
)	)	kIx)
</s>
<s>
1901	#num#	k4
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
,	,	kIx,
1907	#num#	k4
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
italská	italský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
(	(	kIx(
2	#num#	k4
<g/>
x	x	k?
)	)	kIx)
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Italský	italský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
5	#num#	k4
<g/>
x	x	k?
)	)	kIx)
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
(	(	kIx(
7	#num#	k4
<g/>
x	x	k?
)	)	kIx)
</s>
<s>
1988	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
</s>
<s>
Vyhrané	vyhraný	k2eAgFnPc1d1
mezinárodní	mezinárodní	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
(	(	kIx(
7	#num#	k4
<g/>
x	x	k?
)	)	kIx)
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
PVP	PVP	kA
(	(	kIx(
2	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
3	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1969	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
(	(	kIx(
5	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1989	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
MS	MS	kA
klubů	klub	k1gInPc2
(	(	kIx(
1	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
2007	#num#	k4
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
1	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Medailové	medailový	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
</s>
<s>
Medaile	medaile	k1gFnSc1
umístění	umístění	k1gNnSc2
</s>
<s>
Sezona	sezona	k1gFnSc1
</s>
<s>
Serie	serie	k1gFnSc1
A	a	k9
</s>
<s>
1901	#num#	k4
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
,	,	kIx,
1907	#num#	k4
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Serie	serie	k1gFnSc1
A	a	k9
</s>
<s>
1902	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Serie	serie	k1gFnSc1
A	a	k9
</s>
<s>
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
63	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Italský	italský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Italský	italský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
1988	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
</s>
<s>
Italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
1996	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
PVP	PVP	kA
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
PVP	PVP	kA
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
1989	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
1973	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
1969	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
1963	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
</s>
<s>
MS	MS	kA
klubů	klub	k1gInPc2
</s>
<s>
2007	#num#	k4
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Historie	historie	k1gFnSc1
klubu	klub	k1gInSc2
</s>
<s>
Zrod	zrod	k1gInSc1
a	a	k8xC
začátek	začátek	k1gInSc1
</s>
<s>
Budeme	být	k5eAaImBp1nP
týmem	tým	k1gInSc7
ďáblů	ďábel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gFnPc1
barvy	barva	k1gFnPc1
budou	být	k5eAaImBp3nP
červené	červený	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
oheň	oheň	k1gInSc1
a	a	k8xC
černé	černý	k2eAgInPc1d1
jako	jako	k8xC,k8xS
strach	strach	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
budeme	být	k5eAaImBp1nP
podněcovat	podněcovat	k5eAaImF
k	k	k7c3
našim	náš	k3xOp1gMnPc3
oponentům	oponent	k1gMnPc3
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
Herbert	Herbert	k1gMnSc1
Kilpin	Kilpin	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
zakladatelů	zakladatel	k1gMnPc2
klubu	klub	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Herbert	Herbert	k1gMnSc1
Kilpin	Kilpin	k1gMnSc1
</s>
<s>
Zakladající	Zakladající	k2eAgMnPc1d1
členové	člen	k1gMnPc1
klubu	klub	k1gInSc2
</s>
<s>
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1899	#num#	k4
díky	díky	k7c3
podnětu	podnět	k1gInSc2
lidí	člověk	k1gMnPc2
anglické	anglický	k2eAgFnSc2d1
a	a	k8xC
italské	italský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfred	Alfred	k1gMnSc1
Edwards	Edwards	k1gInSc1
(	(	kIx(
<g/>
první	první	k4xOgMnSc1
prezident	prezident	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
Berra	Berra	k1gMnSc1
Nathan	Nathan	k1gMnSc1
(	(	kIx(
<g/>
viceprezident	viceprezident	k1gMnSc1
a	a	k8xC
kapitán	kapitán	k1gMnSc1
družstva	družstvo	k1gNnSc2
kriketu	kriket	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sekretář	sekretář	k1gMnSc1
Samuel	Samuel	k1gMnSc1
Richard	Richard	k1gMnSc1
Davies	Davies	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Allison	Allison	k1gMnSc1
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
týmu	tým	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Daniele	Daniela	k1gFnSc3
Angeloni	Angeloň	k1gFnSc3
<g/>
,	,	kIx,
Giannino	Giannin	k2eAgNnSc1d1
Camperio	Camperio	k1gNnSc1
<g/>
,	,	kIx,
Antonio	Antonio	k1gMnSc1
Dubini	Dubin	k1gMnPc1
<g/>
,	,	kIx,
Guido	Guido	k1gNnSc1
Valerio	Valerio	k1gMnSc1
a	a	k8xC
Herbert	Herbert	k1gMnSc1
Kilpin	Kilpin	k1gMnSc1
se	se	k3xPyFc4
sešli	sejít	k5eAaPmAgMnP
v	v	k7c6
hotelu	hotel	k1gInSc6
Du	Du	k?
Nord	Nord	k1gInSc1
e	e	k0
des	des	k1gNnPc1
Anglai	Angla	k1gFnSc2
aby	aby	kYmCp3nP
založili	založit	k5eAaPmAgMnP
klub	klub	k1gInSc4
Milan	Milan	k1gMnSc1
Foot-Ball	Foot-Ball	k1gMnSc1
&	&	k?
Cricket	Cricket	k1gInSc1
Club	club	k1gInSc1
</s>
<s>
Alfred	Alfred	k1gMnSc1
Edwards	Edwardsa	k1gFnPc2
byl	být	k5eAaImAgMnS
bývalý	bývalý	k2eAgMnSc1d1
vice	vika	k1gFnSc6
konzul	konzul	k1gMnSc1
Británie	Británie	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Milán	Milán	k1gInSc4
a	a	k8xC
známá	známý	k2eAgFnSc1d1
osobnost	osobnost	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
první	první	k4xOgMnSc1
zvolený	zvolený	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
zpočátku	zpočátku	k6eAd1
zahrnoval	zahrnovat	k5eAaImAgInS
kromě	kromě	k7c2
fotbalové	fotbalový	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
ovládané	ovládaný	k2eAgFnSc2d1
Allisonem	Allison	k1gInSc7
také	také	k9
kriketovou	kriketový	k2eAgFnSc4d1
sekci	sekce	k1gFnSc4
svěřenou	svěřený	k2eAgFnSc4d1
Berrem	Berr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herbert	Herbert	k1gMnSc1
Kilpin	Kilpin	k1gMnSc1
je	být	k5eAaImIp3nS
první	první	k4xOgMnSc1
trenér	trenér	k1gMnSc1
a	a	k8xC
první	první	k4xOgMnSc1
kapitán	kapitán	k1gMnSc1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
přináší	přinášet	k5eAaImIp3nS
s	s	k7c7
sebou	se	k3xPyFc7
řadu	řada	k1gFnSc4
inovací	inovace	k1gFnPc2
z	z	k7c2
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
bude	být	k5eAaImBp3nS
klub	klub	k1gInSc1
nosit	nosit	k5eAaImF
klasické	klasický	k2eAgNnSc4d1
červeno-černé	červeno-černý	k2eAgNnSc4d1
svislé	svislý	k2eAgNnSc4d1
pruhované	pruhovaný	k2eAgNnSc4d1
tričko	tričko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgNnSc7
hřištěm	hřiště	k1gNnSc7
je	být	k5eAaImIp3nS
v	v	k7c4
Piazza	Piazz	k1gMnSc4
Doria	Dorium	k1gNnSc2
v	v	k7c6
centru	centrum	k1gNnSc6
Milána	Milán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
1900	#num#	k4
je	být	k5eAaImIp3nS
klub	klub	k1gInSc1
registrován	registrovat	k5eAaBmNgInS
k	k	k7c3
Italské	italský	k2eAgFnSc3d1
fotbalové	fotbalový	k2eAgFnSc3d1
federaci	federace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
hraje	hrát	k5eAaImIp3nS
okamžitě	okamžitě	k6eAd1
v	v	k7c6
ročníku	ročník	k1gInSc6
1900	#num#	k4
nejvyšší	vysoký	k2eAgFnSc4d3
soutěž	soutěž	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
prohrává	prohrávat	k5eAaImIp3nS
v	v	k7c6
semifinále	semifinále	k1gNnSc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
s	s	k7c7
klubem	klub	k1gInSc7
FC	FC	kA
Torinese	Torinese	k1gFnSc1
<g/>
:	:	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgInSc4
oficiální	oficiální	k2eAgInSc4d1
zápas	zápas	k1gInSc4
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
titul	titul	k1gInSc4
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
získává	získávat	k5eAaImIp3nS
v	v	k7c6
ročníku	ročník	k1gInSc6
1901	#num#	k4
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
poráží	porážet	k5eAaImIp3nS
obhájce	obhájce	k1gMnSc1
titulu	titul	k1gInSc2
z	z	k7c2
Janova	Janov	k1gInSc2
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
obhájce	obhájce	k1gMnSc1
titulu	titul	k1gInSc2
byl	být	k5eAaImAgMnS
přímo	přímo	k6eAd1
zařazen	zařadit	k5eAaPmNgMnS
do	do	k7c2
finále	finále	k1gNnSc2
ročníku	ročník	k1gInSc2
1902	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gMnSc6
prohrál	prohrát	k5eAaPmAgInS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
klubem	klub	k1gInSc7
z	z	k7c2
Janova	Janov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
titul	titul	k1gInSc4
si	se	k3xPyFc3
klub	klub	k1gInSc4
připsal	připsat	k5eAaPmAgInS
v	v	k7c6
ročníku	ročník	k1gInSc6
1906	#num#	k4
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
porazil	porazit	k5eAaPmAgInS
klub	klub	k1gInSc1
Juventus	Juventus	k1gInSc1
ve	v	k7c6
dvou	dva	k4xCgNnPc2
hraných	hraný	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročník	ročník	k1gInSc1
1907	#num#	k4
vyhráli	vyhrát	k5eAaPmAgMnP
finálovou	finálový	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
a	a	k8xC
obhájili	obhájit	k5eAaPmAgMnP
tak	tak	k6eAd1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1908	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
interních	interní	k2eAgFnPc6d1
neshodách	neshoda	k1gFnPc6
týkajících	týkající	k2eAgFnPc6d1
se	se	k3xPyFc4
registrování	registrování	k1gNnSc1
či	či	k8xC
nezaregistrování	nezaregistrování	k1gNnSc1
zahraničních	zahraniční	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
vedení	vedení	k1gNnSc1
oddělilo	oddělit	k5eAaPmAgNnS
od	od	k7c2
klubu	klub	k1gInSc2
Rossoneri	Rossoneri	k1gNnSc2
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
založil	založit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
klub	klub	k1gInSc1
s	s	k7c7
názvem	název	k1gInSc7
FC	FC	kA
Internazionale	Internazionale	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
se	se	k3xPyFc4
klub	klub	k1gInSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
ostatní	ostatní	k2eAgInPc1d1
kluby	klub	k1gInPc1
<g/>
,	,	kIx,
nezúčastnili	zúčastnit	k5eNaPmAgMnP
ročníku	ročník	k1gInSc2
1908	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ročníku	ročník	k1gInSc6
1909	#num#	k4
již	již	k6eAd1
opět	opět	k6eAd1
hrají	hrát	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgFnSc4d3
soutěž	soutěž	k1gFnSc4
a	a	k8xC
vyhrají	vyhrát	k5eAaPmIp3nP
první	první	k4xOgNnPc1
derby	derby	k1gNnPc1
s	s	k7c7
klubem	klub	k1gInSc7
FC	FC	kA
Internazionale	Internazionale	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
předáno	předán	k2eAgNnSc1d1
předsednictví	předsednictví	k1gNnSc1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piero	Piero	k1gNnSc1
Pirelli	Pirelle	k1gFnSc3
střídá	střídat	k5eAaImIp3nS
Edwardse	Edwardse	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
změně	změna	k1gFnSc3
sídla	sídlo	k1gNnSc2
klubu	klub	k1gInSc2
<g/>
:	:	kIx,
z	z	k7c2
Fiaschetteria	Fiaschetterium	k1gNnSc2
Toscana	Toscan	k1gMnSc2
ve	v	k7c6
Via	via	k7c4
Berchet	Berchet	k1gInSc4
1	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
pivovaru	pivovar	k1gInSc2
Spatenbräu	Spatenbräus	k1gInSc2
ve	v	k7c6
Via	via	k7c4
Foscolo	Foscola	k1gFnSc5
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
klubu	klub	k1gInSc2
belgický	belgický	k2eAgMnSc1d1
Louis	Louis	k1gMnSc1
Van	vana	k1gFnPc2
Hege	Hege	k1gFnPc2
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
velmi	velmi	k6eAd1
úspěšný	úspěšný	k2eAgMnSc1d1
střelec	střelec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
klubu	klub	k1gInSc6
do	do	k7c2
roku	rok	k1gInSc2
1915	#num#	k4
a	a	k8xC
zaznamenal	zaznamenat	k5eAaPmAgMnS
97	#num#	k4
branek	branka	k1gFnPc2
z	z	k7c2
88	#num#	k4
zápasech	zápas	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
začátku	začátek	k1gInSc2
první	první	k4xOgFnSc2
války	válka	k1gFnSc2
se	se	k3xPyFc4
klub	klub	k1gInSc1
nejlépe	dobře	k6eAd3
umístil	umístit	k5eAaPmAgInS
v	v	k7c6
sezoně	sezona	k1gFnSc6
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
v	v	k7c6
semifinálové	semifinálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
na	na	k7c4
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
zaručující	zaručující	k2eAgInSc1d1
postup	postup	k1gInSc1
do	do	k7c2
finále	finále	k1gNnSc2
chyběl	chybět	k5eAaImAgMnS
jeden	jeden	k4xCgInSc4
bod	bod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
války	válka	k1gFnSc2
klub	klub	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
Federální	federální	k2eAgInSc1d1
pohár	pohár	k1gInSc1
v	v	k7c6
sezoně	sezona	k1gFnSc6
1915	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
nahradil	nahradit	k5eAaPmAgInS
nejvyšší	vysoký	k2eAgFnSc4d3
soutěž	soutěž	k1gFnSc4
<g/>
,	,	kIx,
pozastavený	pozastavený	k2eAgMnSc1d1
kvůli	kvůli	k7c3
válce	válka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
však	však	k9
o	o	k7c4
trofej	trofej	k1gFnSc4
oficiálně	oficiálně	k6eAd1
uznanou	uznaný	k2eAgFnSc4d1
federaci	federace	k1gFnSc4
jako	jako	k8xS,k8xC
titul	titul	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
není	být	k5eNaImIp3nS
zahrnuta	zahrnout	k5eAaPmNgFnS
do	do	k7c2
výpočtu	výpočet	k1gInSc2
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Desetiletí	desetiletí	k1gNnSc1
bez	bez	k7c2
úspěchu	úspěch	k1gInSc2
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
-	-	kIx~
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klub	klub	k1gInSc1
se	se	k3xPyFc4
po	po	k7c6
válce	válka	k1gFnSc6
přejmenovává	přejmenovávat	k5eAaImIp3nS
na	na	k7c4
fotbalovější	fotbalový	k2eAgInSc4d2
název	název	k1gInSc4
Milan	Milan	k1gMnSc1
Football	Football	k1gMnSc1
Club	club	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
do	do	k7c2
klubu	klub	k1gInSc2
přichází	přicházet	k5eAaImIp3nS
první	první	k4xOgMnSc1
zahraniční	zahraniční	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
na	na	k7c6
lavičce	lavička	k1gFnSc6
Rossoneri	Rossoner	k1gFnSc2
Ferdi	Ferd	k1gMnPc1
Oppenheim	Oppenheima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvou	dva	k4xCgFnPc6
sezónách	sezóna	k1gFnPc6
je	být	k5eAaImIp3nS
nahrazen	nahradit	k5eAaPmNgInS
slavným	slavný	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
Pozzou	Pozza	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
v	v	k7c6
následujícím	následující	k2eAgNnSc6d1
desetiletí	desetiletí	k1gNnSc6
vedl	vést	k5eAaImAgInS
národní	národní	k2eAgInSc1d1
tým	tým	k1gInSc1
k	k	k7c3
triumfu	triumf	k1gInSc3
na	na	k7c4
MS.	MS.	k1gFnPc4
Jenže	jenže	k8xC
výsledky	výsledek	k1gInPc1
se	se	k3xPyFc4
nekonají	konat	k5eNaImIp3nP
a	a	k8xC
trenér	trenér	k1gMnSc1
odchází	odcházet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
díky	díky	k7c3
tehdejšímu	tehdejší	k2eAgMnSc3d1
prezidentu	prezident	k1gMnSc3
Pirellim	Pirellim	k1gMnSc1
stadion	stadion	k1gNnSc1
Stadio	Stadio	k1gMnSc1
San	San	k1gMnSc1
Siro	siro	k6eAd1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
zůstal	zůstat	k5eAaPmAgMnS
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
klubu	klub	k1gInSc2
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1935	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
prodán	prodat	k5eAaPmNgInS
městu	město	k1gNnSc3
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
slavnostně	slavnostně	k6eAd1
otevřen	otevřen	k2eAgMnSc1d1
s	s	k7c7
městským	městský	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
FC	FC	kA
Internazionale	Internazionale	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
střelcem	střelec	k1gMnSc7
na	na	k7c6
novém	nový	k2eAgInSc6d1
stadionu	stadion	k1gInSc6
byl	být	k5eAaImAgInS
Giuseppe	Giusepp	k1gInSc5
Santagostino	Santagostin	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
soutěži	soutěž	k1gFnSc6
se	se	k3xPyFc4
klubu	klub	k1gInSc2
do	do	k7c2
sezony	sezona	k1gFnSc2
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
nedaří	dařit	k5eNaImIp3nS
a	a	k8xC
nejlépe	dobře	k6eAd3
se	se	k3xPyFc4
umístí	umístit	k5eAaPmIp3nS
na	na	k7c6
šestém	šestý	k4xOgNnSc6
místě	místo	k1gNnSc6
ve	v	k7c6
finálové	finálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
v	v	k7c6
sezonách	sezona	k1gFnPc6
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
a	a	k8xC
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
odchází	odcházet	k5eAaImIp3nS
Pirelli	Pirelle	k1gFnSc4
z	z	k7c2
předsednictví	předsednictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
klubu	klub	k1gInSc2
bude	být	k5eAaImBp3nS
za	za	k7c4
více	hodně	k6eAd2
než	než	k8xS
deset	deset	k4xCc4
let	léto	k1gNnPc2
následovat	následovat	k5eAaImF
pět	pět	k4xCc4
prezidentů	prezident	k1gMnPc2
(	(	kIx(
<g/>
Ravasco	Ravasco	k1gMnSc1
<g/>
,	,	kIx,
Benazzoli	Benazzole	k1gFnSc4
<g/>
,	,	kIx,
Annoni	Annoň	k1gFnSc5
<g/>
,	,	kIx,
Colombo	Colomba	k1gFnSc5
a	a	k8xC
Invernizzi	Invernizze	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přichází	přicházet	k5eAaImIp3nS
i	i	k9
změna	změna	k1gFnSc1
systému	systém	k1gInSc2
hraní	hraní	k1gNnSc2
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
založena	založen	k2eAgFnSc1d1
Serie	serie	k1gFnSc1
A	a	k9
s	s	k7c7
18	#num#	k4
kluby	klub	k1gInPc7
a	a	k8xC
hrálo	hrát	k5eAaImAgNnS
systémem	systém	k1gInSc7
každý	každý	k3xTgMnSc1
s	s	k7c7
každým	každý	k3xTgNnSc7
doma	doma	k6eAd1
a	a	k8xC
venku	venku	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
končí	končit	k5eAaImIp3nS
sezonu	sezona	k1gFnSc4
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
na	na	k7c4
11	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
sezony	sezona	k1gFnSc2
klub	klub	k1gInSc1
zasáhne	zasáhnout	k5eAaPmIp3nS
předčasná	předčasný	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
kapitána	kapitán	k1gMnSc2
Sgarbiho	Sgarbi	k1gMnSc2
na	na	k7c4
tyfus	tyfus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
utrpěl	utrpět	k5eAaPmAgMnS
15	#num#	k4
porážek	porážka	k1gFnPc2
<g/>
:	:	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
negativní	negativní	k2eAgInSc4d1
rekord	rekord	k1gInSc4
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
na	na	k7c4
dvě	dva	k4xCgFnPc4
sezony	sezona	k1gFnPc4
změní	změnit	k5eAaPmIp3nS
název	název	k1gInSc1
na	na	k7c4
Milan	Milan	k1gMnSc1
Associazione	Associazion	k1gInSc5
Sportiva	Sportiva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
téhle	tenhle	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
klub	klub	k1gInSc1
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
lize	liga	k1gFnSc6
nejlépe	dobře	k6eAd3
umístil	umístit	k5eAaPmAgMnS
na	na	k7c6
děleném	dělený	k2eAgNnSc6d1
čtvrtém	čtvrtý	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
sezoně	sezona	k1gFnSc6
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezoně	sezona	k1gFnSc6
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
umisťuje	umisťovat	k5eAaImIp3nS
na	na	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
a	a	k8xC
hraje	hrát	k5eAaImIp3nS
i	i	k9
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
kde	kde	k6eAd1
dokráčí	dokráčet	k5eAaPmIp3nS
do	do	k7c2
osmi	osm	k4xCc2
finále	finále	k1gNnPc2
<g/>
,	,	kIx,
když	když	k8xS
jej	on	k3xPp3gNnSc4
vyřadil	vyřadit	k5eAaPmAgInS
rumunský	rumunský	k2eAgInSc1d1
klub	klub	k1gInSc1
FC	FC	kA
Ripensia	Ripensia	k1gFnSc1
Temešvár	Temešvár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
s	s	k7c7
novým	nový	k2eAgInSc7d1
názvem	název	k1gInSc7
Associazione	Associazion	k1gInSc5
Calcio	Calcia	k1gFnSc5
Milano	Milana	k1gFnSc5
se	se	k3xPyFc4
klub	klub	k1gInSc1
v	v	k7c6
sezoně	sezona	k1gFnSc6
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
umisťuje	umisťovat	k5eAaImIp3nS
na	na	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
jíž	jenž	k3xRgFnSc7
za	za	k7c4
Rossoneri	Rossonere	k1gFnSc4
hraje	hrát	k5eAaImIp3nS
miláček	miláček	k1gMnSc1
národa	národ	k1gInSc2
Giuseppe	Giusepp	k1gInSc5
Meazza	Meazza	k1gFnSc1
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
přichází	přicházet	k5eAaImIp3nS
po	po	k7c6
zranění	zranění	k1gNnSc6
z	z	k7c2
klubu	klub	k1gInSc2
Inter	Intra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
hráč	hráč	k1gMnSc1
Aldo	Aldo	k1gMnSc1
Boffi	Boffi	k1gNnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
klubu	klub	k1gInSc6
také	také	k6eAd1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1936	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
během	během	k7c2
svého	svůj	k3xOyFgNnSc2
působení	působení	k1gNnSc2
v	v	k7c6
klubu	klub	k1gInSc6
tři	tři	k4xCgInPc1
krát	krát	k6eAd1
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
a	a	k8xC
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
let	léto	k1gNnPc2
1943	#num#	k4
až	až	k8xS
1945	#num#	k4
se	se	k3xPyFc4
nehrálo	hrát	k5eNaImAgNnS
a	a	k8xC
na	na	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
se	se	k3xPyFc4
klub	klub	k1gInSc1
vrací	vracet	k5eAaImIp3nS
ke	k	k7c3
starému	starý	k2eAgNnSc3d1
jménu	jméno	k1gNnSc3
Milan	Milan	k1gMnSc1
Football	Footballa	k1gFnPc2
Club	club	k1gInSc4
</s>
<s>
Trojka	trojka	k1gFnSc1
Gre-No	Gre-No	k1gMnSc1
<g/>
-Li	-Li	k?
</s>
<s>
Ve	v	k7c6
čtyřech	čtyři	k4xCgFnPc6
sezonách	sezona	k1gFnPc6
po	po	k7c6
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
dominoval	dominovat	k5eAaImAgInS
tým	tým	k1gInSc1
Grande	grand	k1gMnSc5
Torino	Torina	k1gMnSc5
<g/>
,	,	kIx,
se	se	k3xPyFc4
tým	tým	k1gInSc1
s	s	k7c7
trenérem	trenér	k1gMnSc7
Bigognem	Bigogn	k1gMnSc7
a	a	k8xC
prezidentem	prezident	k1gMnSc7
Trabattonim	Trabattonima	k1gFnPc2
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
první	první	k4xOgInPc4
tři	tři	k4xCgInPc4
nejlepší	dobrý	k2eAgInPc4d3
kluby	klub	k1gInPc4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
čtvrtého	čtvrtý	k4xOgNnSc2
místa	místo	k1gNnSc2
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
jsou	být	k5eAaImIp3nP
v	v	k7c6
Rossoneri	Rossoner	k1gInSc6
hráči	hráč	k1gMnPc1
jako	jako	k8xC,k8xS
kapitán	kapitán	k1gMnSc1
Giuseppe	Giusepp	k1gInSc5
Antonini	Antonin	k2eAgMnPc1d1
<g/>
,	,	kIx,
<g/>
Andrea	Andrea	k1gFnSc1
Bonomi	Bono	k1gFnPc7
<g/>
,	,	kIx,
Renzo	Renza	k1gFnSc5
Burini	Burin	k1gMnPc1
<g/>
,	,	kIx,
Mario	Mario	k1gMnSc1
Foglia	Foglium	k1gNnSc2
<g/>
,	,	kIx,
Aredio	Aredio	k6eAd1
Gimona	Gimona	k1gFnSc1
<g/>
,	,	kIx,
Omero	Omero	k1gNnSc1
Tognon	Tognona	k1gFnPc2
<g/>
,	,	kIx,
Carlo	Carlo	k1gNnSc4
Annovazzi	Annovazze	k1gFnSc4
<g/>
,	,	kIx,
Paolo	Paolo	k1gNnSc4
Todeschini	Todeschin	k1gMnPc1
<g/>
,	,	kIx,
Riccardo	Riccardo	k1gNnSc1
Carapellese	Carapellese	k1gFnPc1
<g/>
,	,	kIx,
Paddy	Padd	k1gInPc1
Sloan	Sloan	k1gInSc1
a	a	k8xC
Héctor	Héctor	k1gInSc1
Puricelli	Puricelle	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
sezónu	sezóna	k1gFnSc4
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
klubu	klub	k1gInSc2
kupuje	kupovat	k5eAaImIp3nS
Švédské	švédský	k2eAgMnPc4d1
fotbalisty	fotbalista	k1gMnPc4
a	a	k8xC
olympijské	olympijský	k2eAgMnPc4d1
vítěze	vítěz	k1gMnPc4
Liedholma	Liedholm	k1gMnSc4
a	a	k8xC
Grena	Gren	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnSc7
krajan	krajan	k1gMnSc1
Gunnar	Gunnar	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
již	již	k6eAd1
v	v	k7c6
klubu	klub	k1gInSc6
od	od	k7c2
ledna	leden	k1gInSc2
1949	#num#	k4
hraje	hrát	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédské	švédský	k2eAgNnSc4d1
útočné	útočný	k2eAgNnSc4d1
trio	trio	k1gNnSc4
se	se	k3xPyFc4
formuje	formovat	k5eAaImIp3nS
a	a	k8xC
je	být	k5eAaImIp3nS
jim	on	k3xPp3gInPc3
vytvořena	vytvořit	k5eAaPmNgFnS
přezdívka	přezdívka	k1gFnSc1
Gre-No	Gre-No	k6eAd1
<g/>
-Li	-Li	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střelec	Střelec	k1gMnSc1
Gunnar	Gunnar	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
vstřelil	vstřelit	k5eAaPmAgMnS
35	#num#	k4
branek	branka	k1gFnPc2
a	a	k8xC
do	do	k7c2
sezony	sezona	k1gFnSc2
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
rekord	rekord	k1gInSc1
Serie	serie	k1gFnSc2
A.	A.	kA
Klub	klub	k1gInSc1
vstřelí	vstřelit	k5eAaPmIp3nS
v	v	k7c6
sezoně	sezona	k1gFnSc6
118	#num#	k4
branek	branka	k1gFnPc2
(	(	kIx(
<g/>
nejvíce	nejvíce	k6eAd1,k6eAd3
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
končí	končit	k5eAaImIp3nS
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
této	tento	k3xDgFnSc3
změně	změna	k1gFnSc3
samozřejmě	samozřejmě	k6eAd1
jak	jak	k8xC,k8xS
úspěchům	úspěch	k1gInPc3
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
příchodům	příchod	k1gInPc3
nových	nový	k2eAgInPc6d1
hráčů	hráč	k1gMnPc2
které	který	k3yIgFnSc3,k3yRgFnSc3,k3yQgFnSc3
byli	být	k5eAaImAgMnP
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
počet	počet	k1gInSc1
fanoušků	fanoušek	k1gMnPc2
Rossoneri	Rossoneri	k1gNnSc2
znásobil	znásobit	k5eAaPmAgInS
<g/>
,	,	kIx,
překročil	překročit	k5eAaPmAgMnS
hranice	hranice	k1gFnPc4
Lombardie	Lombardie	k1gFnSc2
a	a	k8xC
nabude	nabýt	k5eAaPmIp3nS
světových	světový	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Návrat	návrat	k1gInSc1
k	k	k7c3
vítězství	vítězství	k1gNnSc3
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
-	-	kIx~
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Příchody	příchod	k1gInPc1
těchto	tento	k3xDgMnPc2
tří	tři	k4xCgMnPc2
hráčů	hráč	k1gMnPc2
znamená	znamenat	k5eAaImIp3nS
zlom	zlom	k1gInSc4
v	v	k7c6
klubu	klub	k1gInSc6
<g/>
:	:	kIx,
po	po	k7c6
44	#num#	k4
letech	léto	k1gNnPc6
klub	klub	k1gInSc1
získává	získávat	k5eAaImIp3nS
v	v	k7c6
sezoně	sezona	k1gFnSc6
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
titul	titul	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
získali	získat	k5eAaPmAgMnP
od	od	k7c2
zavedení	zavedení	k1gNnSc2
Serie	serie	k1gFnSc2
A.	A.	kA
V	v	k7c6
červnu	červen	k1gInSc6
1951	#num#	k4
klub	klub	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
Latinský	latinský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
nejdůležitější	důležitý	k2eAgFnSc4d3
evropskou	evropský	k2eAgFnSc4d1
fotbalovou	fotbalový	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
:	:	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgFnSc1
získaná	získaný	k2eAgFnSc1d1
mezinárodní	mezinárodní	k2eAgFnSc1d1
trofej	trofej	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titul	titul	k1gInSc1
v	v	k7c6
sezoně	sezona	k1gFnSc6
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
se	se	k3xPyFc4
obhájit	obhájit	k5eAaPmF
nepodaří	podařit	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Končí	končit	k5eAaImIp3nS
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
o	o	k7c6
sedm	sedm	k4xCc4
bodů	bod	k1gInPc2
za	za	k7c7
Juventusem	Juventus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příštích	příští	k2eAgFnPc6d1
třech	tři	k4xCgFnPc6
sezonách	sezona	k1gFnPc6
se	se	k3xPyFc4
klub	klub	k1gInSc1
umístí	umístit	k5eAaPmIp3nS
na	na	k7c6
medailových	medailový	k2eAgFnPc6d1
příčkách	příčka	k1gFnPc6
a	a	k8xC
v	v	k7c6
Latinském	latinský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
v	v	k7c6
ročníku	ročník	k1gInSc6
1953	#num#	k4
prohrává	prohrávat	k5eAaImIp3nS
ve	v	k7c6
finále	finále	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezona	sezona	k1gFnSc1
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgNnPc4d1
tria	trio	k1gNnPc4
Gre-No	Gre-No	k6eAd1
<g/>
-Li	-Li	k?
<g/>
,	,	kIx,
protože	protože	k8xS
Gunnar	Gunnar	k1gMnSc1
Gren	Gren	k1gMnSc1
odchází	odcházet	k5eAaImIp3nS
po	po	k7c6
sezoně	sezona	k1gFnSc6
do	do	k7c2
Fiorentiny	Fiorentina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cesare	Cesar	k1gMnSc5
Maldini	Maldin	k2eAgMnPc1d1
</s>
<s>
Na	na	k7c4
sezonu	sezona	k1gFnSc4
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
klub	klub	k1gInSc1
koupí	koupit	k5eAaPmIp3nS
budoucí	budoucí	k2eAgFnSc4d1
ikonu	ikona	k1gFnSc4
nejen	nejen	k6eAd1
klubu	klub	k1gInSc2
ale	ale	k8xC
také	také	k9
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
fotbalovou	fotbalový	k2eAgFnSc4d1
Itálii	Itálie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
mladý	mladý	k2eAgInSc1d1
talent	talent	k1gInSc1
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaBmAgInS,k5eAaImAgInS
Cesare	Cesar	k1gMnSc5
Maldini	Maldin	k2eAgMnPc5d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
do	do	k7c2
klubu	klub	k1gInSc2
přichází	přicházet	k5eAaImIp3nS
i	i	k9
uruguayský	uruguayský	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
Juan	Juan	k1gMnSc1
Alberto	Alberta	k1gFnSc5
Schiaffino	Schiaffin	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rossoneri	Rossoneri	k1gNnSc1
získává	získávat	k5eAaImIp3nS
pátý	pátý	k4xOgInSc4
titul	titul	k1gInSc4
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
lize	liga	k1gFnSc6
a	a	k8xC
zajistil	zajistit	k5eAaPmAgMnS
si	se	k3xPyFc3
účast	účast	k1gFnSc4
prvního	první	k4xOgInSc2
ročníku	ročník	k1gInSc2
o	o	k7c4
pohár	pohár	k1gInSc4
PMEZ	PMEZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	V	kA
nové	nový	k2eAgFnSc2d1
Evropské	evropský	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
dokráčel	dokráčet	k5eAaPmAgInS
do	do	k7c2
semifinále	semifinále	k1gNnSc2
když	když	k8xS
jel	jet	k5eAaImAgMnS
vyřadil	vyřadit	k5eAaPmAgMnS
budoucí	budoucí	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
ročníku	ročník	k1gInSc2
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
se	se	k3xPyFc4
klub	klub	k1gInSc1
umístil	umístit	k5eAaPmAgInS
v	v	k7c6
sezoně	sezona	k1gFnSc6
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
o	o	k7c4
12	#num#	k4
bodů	bod	k1gInPc2
od	od	k7c2
vítěze	vítěz	k1gMnSc2
ligy	liga	k1gFnSc2
Fiorentiny	Fiorentina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
1956	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Miláně	Milán	k1gInSc6
konalo	konat	k5eAaImAgNnS
předposlední	předposlední	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
Latinského	latinský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
a	a	k8xC
v	v	k7c6
něm	on	k3xPp3gInSc6
Rossoneri	Rossoner	k1gInSc6
zvítězili	zvítězit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
před	před	k7c7
sezonou	sezona	k1gFnSc7
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
klub	klub	k1gInSc1
opouští	opouštět	k5eAaImIp3nS
Gunnar	Gunnar	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
odchází	odcházet	k5eAaImIp3nS
do	do	k7c2
AS	as	k1gNnSc2
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
klub	klub	k1gInSc1
získává	získávat	k5eAaImIp3nS
pošesté	pošesté	k4xO
titul	titul	k1gInSc4
když	když	k8xS
vyhrává	vyhrávat	k5eAaImIp3nS
před	před	k7c7
klubem	klub	k1gInSc7
ACF	ACF	kA
Fiorentina	Fiorentin	k2eAgFnSc1d1
o	o	k7c4
šest	šest	k4xCc4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tímto	tento	k3xDgInSc7
výsledkem	výsledek	k1gInSc7
Rossoneri	Rossonere	k1gFnSc4
uzavřeli	uzavřít	k5eAaPmAgMnP
období	období	k1gNnSc4
deseti	deset	k4xCc2
po	po	k7c6
sobě	sebe	k3xPyFc6
jdoucích	jdoucí	k2eAgFnPc2d1
sezon	sezona	k1gFnPc2
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
-	-	kIx~
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
skončil	skončit	k5eAaPmAgInS
ligu	liga	k1gFnSc4
na	na	k7c6
prvních	první	k4xOgInPc6
třech	tři	k4xCgInPc6
místech	místo	k1gNnPc6
tabulky	tabulka	k1gFnSc2
<g/>
,	,	kIx,
zaznamenal	zaznamenat	k5eAaPmAgMnS
tak	tak	k6eAd1
historicky	historicky	k6eAd1
počin	počin	k1gInSc1
klubu	klub	k1gInSc2
a	a	k8xC
také	také	k9
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
druhý	druhý	k4xOgInSc1
nejlepší	dobrý	k2eAgInSc1d3
po	po	k7c6
sobě	se	k3xPyFc3
jdoucích	jdoucí	k2eAgNnPc2d1
"	"	kIx"
<g/>
pódií	pódium	k1gNnPc2
<g/>
"	"	kIx"
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
,	,	kIx,
překonal	překonat	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
pouze	pouze	k6eAd1
Juventus	Juventus	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
-	-	kIx~
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sezona	sezona	k1gFnSc1
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
lize	liga	k1gFnSc6
nepovede	povést	k5eNaPmIp3nS,k5eNaImIp3nS
<g/>
,	,	kIx,
klub	klub	k1gInSc1
končí	končit	k5eAaImIp3nS
na	na	k7c6
devátém	devátý	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
hrají	hrát	k5eAaImIp3nP
o	o	k7c4
pohár	pohár	k1gInSc4
PMEZ	PMEZ	kA
a	a	k8xC
dokráčejí	dokráčet	k5eAaPmIp3nP
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gMnSc6
prohrávají	prohrávat	k5eAaImIp3nP
s	s	k7c7
Realem	Real	k1gInSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
novou	nový	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
vyzbrojí	vyzbrojit	k5eAaPmIp3nS
o	o	k7c4
brazilského	brazilský	k2eAgMnSc4d1
hráče	hráč	k1gMnSc4
Altafiniho	Altafini	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
získává	získávat	k5eAaImIp3nS
již	již	k9
sedmí	sedmý	k4xOgMnPc1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
získal	získat	k5eAaPmAgMnS
o	o	k7c4
tři	tři	k4xCgInPc4
body	bod	k1gInPc4
více	hodně	k6eAd2
než	než	k8xS
druhá	druhý	k4xOgFnSc1
Fiorentina	Fiorentina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titul	titul	k1gInSc1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
v	v	k7c6
sezoně	sezona	k1gFnSc6
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
obhájit	obhájit	k5eAaPmF
nepodaří	podařit	k5eNaPmIp3nS
a	a	k8xC
skončí	skončit	k5eAaPmIp3nS
na	na	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poháru	pohár	k1gInSc6
PMEZ	PMEZ	kA
se	se	k3xPyFc4
klub	klub	k1gInSc1
probojoval	probojovat	k5eAaPmAgInS
do	do	k7c2
osmi	osm	k4xCc2
finále	finále	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Úspěšné	úspěšný	k2eAgInPc1d1
roky	rok	k1gInPc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
-	-	kIx~
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Juan	Juan	k1gMnSc1
Alberto	Alberta	k1gFnSc5
Schiaffino	Schiaffino	k1gNnSc1
+	+	kIx~
Gianni	Giann	k1gMnPc1
Rivera	Rivero	k1gNnSc2
</s>
<s>
Nereo	Nereo	k1gMnSc1
Rocco	Rocco	k1gMnSc1
</s>
<s>
Letní	letní	k2eAgInSc1d1
trh	trh	k1gInSc1
před	před	k7c7
sezonou	sezona	k1gFnSc7
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
vede	vést	k5eAaImIp3nS
k	k	k7c3
revoluci	revoluce	k1gFnSc3
<g/>
:	:	kIx,
klub	klub	k1gInSc1
získává	získávat	k5eAaImIp3nS
teprve	teprve	k6eAd1
šestnáctiletého	šestnáctiletý	k2eAgInSc2d1
talentu	talent	k1gInSc2
Riveru	River	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
svou	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
spojí	spojit	k5eAaPmIp3nS
jen	jen	k9
s	s	k7c7
Rossoneri	Rossoneri	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
klubu	klub	k1gInSc2
odchází	odcházet	k5eAaImIp3nS
Juan	Juan	k1gMnSc1
Alberto	Alberta	k1gFnSc5
Schiaffino	Schiaffin	k2eAgNnSc1d1
(	(	kIx(
<g/>
AS	as	k9
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezonu	sezona	k1gFnSc4
ukončí	ukončit	k5eAaPmIp3nP
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
za	za	k7c7
šampionem	šampion	k1gMnSc7
Juventusem	Juventus	k1gInSc7
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
body	bod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
sezóny	sezóna	k1gFnSc2
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
je	být	k5eAaImIp3nS
najat	najat	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Nereo	Nereo	k1gMnSc1
Rocco	Rocco	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ním	on	k3xPp3gNnSc7
klub	klub	k1gInSc1
slaví	slavit	k5eAaImIp3nS
osmé	osmý	k4xOgNnSc4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
před	před	k7c7
svými	svůj	k3xOyFgMnPc7
rivaly	rival	k1gMnPc7
Nerazzurri	Nerazzurr	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
novou	nový	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
vyzbrojí	vyzbrojit	k5eAaPmIp3nS
o	o	k7c4
dalšího	další	k2eAgMnSc4d1
brazilského	brazilský	k2eAgMnSc4d1
hráče	hráč	k1gMnSc4
Amarilda	Amarild	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
i	i	k9
s	s	k7c7
posilou	posila	k1gFnSc7
se	se	k3xPyFc4
klubu	klub	k1gInSc2
obhájit	obhájit	k5eAaPmF
titul	titul	k1gInSc4
nepodaří	podařit	k5eNaPmIp3nS
a	a	k8xC
končí	končit	k5eAaImIp3nS
na	na	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zato	zato	k6eAd1
v	v	k7c6
Evropském	evropský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
o	o	k7c4
pohár	pohár	k1gInSc4
PMEZ	PMEZ	kA
se	se	k3xPyFc4
daří	dařit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probojují	probojovat	k5eAaPmIp3nP
se	se	k3xPyFc4
do	do	k7c2
finále	finále	k1gNnSc2
a	a	k8xC
v	v	k7c6
něm	on	k3xPp3gInSc6
poráží	porážet	k5eAaImIp3nS
portugalský	portugalský	k2eAgInSc1d1
klub	klub	k1gInSc1
Benfiku	Benfik	k1gInSc2
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
získávají	získávat	k5eAaImIp3nP
tak	tak	k9
první	první	k4xOgFnSc4
trofej	trofej	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
již	již	k6eAd1
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
pohár	pohár	k1gInSc4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Altafini	Altafin	k2eAgMnPc5d1
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc4
Rossoneri	Rossonere	k1gFnSc4
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c6
tabulce	tabulka	k1gFnSc6
střelců	střelec	k1gMnPc2
této	tento	k3xDgFnSc2
soutěže	soutěž	k1gFnSc2
díky	díky	k7c3
14	#num#	k4
brankám	branka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
počet	počet	k1gInSc1
branek	branka	k1gFnPc2
vstřelené	vstřelený	k2eAgInPc1d1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
sezoně	sezona	k1gFnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
zůstane	zůstat	k5eAaPmIp3nS
absolutním	absolutní	k2eAgInSc7d1
rekordem	rekord	k1gInSc7
soutěže	soutěž	k1gFnSc2
až	až	k6eAd1
do	do	k7c2
sezóny	sezóna	k1gFnSc2
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bude	být	k5eAaImBp3nS
vyrovnán	vyrovnat	k5eAaBmNgInS,k5eAaPmNgInS
Messim	Messim	k1gInSc1
a	a	k8xC
nakonec	nakonec	k6eAd1
překonán	překonat	k5eAaPmNgInS
Ronaldem	Ronald	k1gInSc7
v	v	k7c6
sezoně	sezona	k1gFnSc6
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xS,k8xC
obhájci	obhájce	k1gMnPc1
trofeje	trofej	k1gFnSc2
PMEZ	PMEZ	kA
se	se	k3xPyFc4
klub	klub	k1gInSc1
zúčastnil	zúčastnit	k5eAaPmAgInS
i	i	k9
sezony	sezona	k1gFnPc4
PMEZ	PMEZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probojoval	probojovat	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
a	a	k8xC
tam	tam	k6eAd1
jej	on	k3xPp3gMnSc4
vyřadil	vyřadit	k5eAaPmAgInS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
sehrál	sehrát	k5eAaPmAgInS
i	i	k9
zápasy	zápas	k1gInPc4
o	o	k7c4
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
s	s	k7c7
brazilským	brazilský	k2eAgInSc7d1
klubem	klub	k1gInSc7
FC	FC	kA
Santos	Santos	k1gInSc1
i	i	k8xC
s	s	k7c7
Pelém	Pelý	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
utkání	utkání	k1gNnPc2
klub	klub	k1gInSc1
hrál	hrát	k5eAaImAgInS
doma	doma	k6eAd1
a	a	k8xC
zvítězil	zvítězit	k5eAaPmAgMnS
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgNnSc1
utkání	utkání	k1gNnPc2
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
na	na	k7c6
stadionu	stadion	k1gInSc6
Maracana	Maracan	k1gMnSc2
a	a	k8xC
prohráli	prohrát	k5eAaPmAgMnP
2	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc4
rozhodující	rozhodující	k2eAgInSc4d1
utkání	utkání	k1gNnPc2
prohráli	prohrát	k5eAaPmAgMnP
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
též	též	k6eAd1
hrané	hraný	k2eAgFnPc4d1
na	na	k7c6
stadionu	stadion	k1gInSc6
Maracana	Maracana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomhle	tenhle	k3xDgInSc6
roce	rok	k1gInSc6
poprvé	poprvé	k6eAd1
vyšel	vyjít	k5eAaPmAgMnS
klubový	klubový	k2eAgInSc4d1
časopis	časopis	k1gInSc4
Forza	Forza	k1gFnSc1
Milan	Milana	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
se	se	k3xPyFc4
Rossoneri	Rossoner	k1gMnPc1
se	se	k3xPyFc4
umístili	umístit	k5eAaPmAgMnP
na	na	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc4
sezony	sezona	k1gFnPc4
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
a	a	k8xC
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
se	se	k3xPyFc4
trenérské	trenérský	k2eAgFnSc2d1
lavičky	lavička	k1gFnSc2
ujímá	ujímat	k5eAaImIp3nS
po	po	k7c6
odchodu	odchod	k1gInSc6
Rocca	Rocca	k1gFnSc1
bývalí	bývalý	k2eAgMnPc1d1
hráč	hráč	k1gMnSc1
Nils	Nilsa	k1gFnPc2
Liedholm	Liedholm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
sezoně	sezona	k1gFnSc6
se	se	k3xPyFc4
klubu	klub	k1gInSc2
daří	dařit	k5eAaImIp3nS
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
vede	vést	k5eAaImIp3nS
tabulku	tabulka	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
ztrácí	ztrácet	k5eAaImIp3nS
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
umístí	umístit	k5eAaPmIp3nS
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
za	za	k7c7
Interem	Inter	k1gInSc7
o	o	k7c4
tři	tři	k4xCgInPc4
body	bod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátká	Krátká	k1gFnSc1
je	být	k5eAaImIp3nS
cesta	cesta	k1gFnSc1
v	v	k7c6
Evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
(	(	kIx(
<g/>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
končí	končit	k5eAaImIp3nS
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Sezonu	sezona	k1gFnSc4
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
končí	končit	k5eAaImIp3nS
na	na	k7c6
sedmé	sedmý	k4xOgFnSc6
příčce	příčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
už	už	k6eAd1
za	za	k7c4
Rossoneri	Rossonere	k1gFnSc4
nehraje	hrát	k5eNaImIp3nS
José	José	k1gNnSc1
Altafini	Altafin	k1gMnPc1
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Neapole	Neapol	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
i	i	k9
o	o	k7c4
Veletržní	veletržní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
a	a	k8xC
dostává	dostávat	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
osmi	osm	k4xCc2
finále	finále	k1gNnPc2
kde	kde	k6eAd1
prohrává	prohrávat	k5eAaImIp3nS
po	po	k7c6
třech	tři	k4xCgInPc6
utkání	utkání	k1gNnPc2
nad	nad	k7c4
FC	FC	kA
Chelsea	Chelsea	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypadnutí	vypadnutí	k1gNnSc1
ale	ale	k8xC
došlo	dojít	k5eAaPmAgNnS
losem	los	k1gInSc7
hodem	hod	k1gInSc7
mincí	mince	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
se	se	k3xPyFc4
znovu	znovu	k6eAd1
přestěhoval	přestěhovat	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
sídlo	sídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ulici	ulice	k1gFnSc6
Via	via	k7c4
Filippo	Filippa	k1gFnSc5
Turati	Turat	k2eAgMnPc1d1
3	#num#	k4
zůstane	zůstat	k5eAaPmIp3nS
do	do	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Prvního	první	k4xOgNnSc2
vítězství	vítězství	k1gNnSc2
v	v	k7c6
italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
se	se	k3xPyFc4
klub	klub	k1gInSc1
dočkal	dočkat	k5eAaPmAgInS
v	v	k7c6
sezoně	sezona	k1gFnSc6
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
poráží	porážet	k5eAaImIp3nS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
klub	klub	k1gInSc1
z	z	k7c2
Padovy	Padova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
již	již	k6eAd1
za	za	k7c4
klub	klub	k1gInSc4
nehraje	hrát	k5eNaImIp3nS
Cesare	Cesar	k1gMnSc5
Maldini	Maldin	k2eAgMnPc1d1
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
odešel	odejít	k5eAaPmAgMnS
dohrát	dohrát	k5eAaPmF
kariéru	kariéra	k1gFnSc4
do	do	k7c2
Turína	Turín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
novou	nový	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
po	po	k7c6
minulé	minulý	k2eAgFnSc6d1
sezoně	sezona	k1gFnSc6
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
skončilo	skončit	k5eAaPmAgNnS
na	na	k7c6
osmém	osmý	k4xOgInSc6
místě	místo	k1gNnSc6
vyměnit	vyměnit	k5eAaPmF
Amarilda	Amarilda	k1gFnSc1
do	do	k7c2
Fiorentiny	Fiorentina	k1gFnSc2
za	za	k7c4
Hamrina	Hamrin	k1gMnSc4
a	a	k8xC
trenéra	trenér	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrací	vracet	k5eAaImIp3nS
se	se	k3xPyFc4
Nereo	Nereo	k6eAd1
Rocco	Rocco	k6eAd1
a	a	k8xC
díky	díky	k7c3
15	#num#	k4
brankám	branka	k1gFnPc3
Pratiho	Prati	k1gMnSc4
získávají	získávat	k5eAaImIp3nP
o	o	k7c4
devět	devět	k4xCc4
bodů	bod	k1gInPc2
před	před	k7c7
SSC	SSC	kA
Neapol	Neapol	k1gFnSc4
devátý	devátý	k4xOgInSc4
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrají	hrát	k5eAaImIp3nP
i	i	k9
o	o	k7c4
pohár	pohár	k1gInSc4
PVP	PVP	kA
a	a	k8xC
díky	díky	k7c3
vítězství	vítězství	k1gNnSc3
ve	v	k7c6
finále	finále	k1gNnSc6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
nad	nad	k7c7
německým	německý	k2eAgInSc7d1
klubem	klub	k1gInSc7
Hamburger	hamburger	k1gInSc1
SV	sv	kA
jej	on	k3xPp3gMnSc4
získávají	získávat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	V	kA
italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
končí	končit	k5eAaImIp3nS
ve	v	k7c6
finálové	finálový	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Ginni	Ginn	k1gMnPc1
Rivera	Rivero	k1gNnSc2
</s>
<s>
Sezonu	sezona	k1gFnSc4
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
končí	končit	k5eAaImIp3nS
na	na	k7c6
děleném	dělený	k2eAgNnSc6d1
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
s	s	k7c7
Cagliarim	Cagliari	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rossoneri	Rossoneri	k1gNnPc7
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nS
o	o	k7c4
trofej	trofej	k1gFnSc4
PMEZ	PMEZ	kA
a	a	k8xC
dokráčejí	dokráčet	k5eAaPmIp3nP
do	do	k7c2
finále	finále	k1gNnSc2
kde	kde	k6eAd1
poráží	porážet	k5eAaImIp3nS
nizozemský	nizozemský	k2eAgInSc1d1
klub	klub	k1gInSc1
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
díky	díky	k7c3
třem	tři	k4xCgFnPc3
brankám	branka	k1gFnPc3
Pratiho	Prati	k1gMnSc2
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šedesátá	šedesátý	k4xOgNnPc1
léta	léto	k1gNnPc1
Rossoneri	Rossoner	k1gFnSc2
uzavřou	uzavřít	k5eAaPmIp3nP
vítězstvím	vítězství	k1gNnSc7
prvního	první	k4xOgInSc2
Interkontinentálního	interkontinentální	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poráží	porážet	k5eAaImIp3nS
argentinský	argentinský	k2eAgInSc1d1
klub	klub	k1gInSc1
Estudiantes	Estudiantesa	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
Serie	serie	k1gFnPc4
A	A	kA
<g/>
|	|	kIx~
<g/>
lize	liga	k1gFnSc3
sezonu	sezona	k1gFnSc4
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
dokončuje	dokončovat	k5eAaImIp3nS
na	na	k7c6
čtvrtém	čtvrtý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
obhájce	obhájce	k1gMnSc1
titulu	titul	k1gInSc2
klub	klub	k1gInSc1
hraje	hrát	k5eAaImIp3nS
PMEZ	PMEZ	kA
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
osmi	osm	k4xCc6
finále	finále	k1gNnSc2
jej	on	k3xPp3gMnSc4
vyřadí	vyřadit	k5eAaPmIp3nS
budoucí	budoucí	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
Feyenoord	Feyenoord	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
získává	získávat	k5eAaImIp3nS
Gianni	Gianeň	k1gFnSc3
Rivera	Rivero	k1gNnSc2
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
Ital	Ital	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vše	všechen	k3xTgNnSc1
pro	pro	k7c4
desátý	desátý	k4xOgInSc4
titul	titul	k1gInSc4
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
-	-	kIx~
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Získat	získat	k5eAaPmF
desátý	desátý	k4xOgInSc4
titul	titul	k1gInSc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
nosit	nosit	k5eAaImF
zlatou	zlatý	k2eAgFnSc4d1
hvězdu	hvězda	k1gFnSc4
na	na	k7c6
dresu	dres	k1gInSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
jako	jako	k9
Juventus	Juventus	k1gInSc1
FC	FC	kA
a	a	k8xC
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
Rossoneri	Rossoner	k1gFnSc2
jí	on	k3xPp3gFnSc7
chtělo	chtít	k5eAaImAgNnS
získat	získat	k5eAaPmF
již	již	k6eAd1
na	na	k7c6
začátku	začátek	k1gInSc6
desetiletí	desetiletí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
sezoně	sezona	k1gFnSc6
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
vyhrává	vyhrávat	k5eAaImIp3nS
zimní	zimní	k2eAgFnSc4d1
tabulku	tabulka	k1gFnSc4
<g/>
,	,	kIx,
jenže	jenže	k8xC
na	na	k7c6
jaře	jaro	k1gNnSc6
prohrává	prohrávat	k5eAaImIp3nS
pár	pár	k4xCyI
zápasů	zápas	k1gInPc2
a	a	k8xC
městský	městský	k2eAgMnSc1d1
rival	rival	k1gMnSc1
Inter	Inter	k1gMnSc1
vyhrává	vyhrávat	k5eAaImIp3nS
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
body	bod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
se	se	k3xPyFc4
dostávají	dostávat	k5eAaImIp3nP
do	do	k7c2
finále	finále	k1gNnSc2
ale	ale	k8xC
prohrají	prohrát	k5eAaPmIp3nP
v	v	k7c6
něm	on	k3xPp3gNnSc6
s	s	k7c7
Turínem	Turín	k1gInSc7
smolně	smolně	k6eAd1
na	na	k7c4
penalty	penalta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezonu	sezona	k1gFnSc4
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
ligu	liga	k1gFnSc4
prohrává	prohrávat	k5eAaImIp3nS
o	o	k7c4
jeden	jeden	k4xCgInSc4
bod	bod	k1gInSc4
s	s	k7c7
Juventusem	Juventus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naštěstí	naštěstí	k6eAd1
si	se	k3xPyFc3
vyhrávají	vyhrávat	k5eAaImIp3nP
Italský	italský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
nad	nad	k7c7
SSC	SSC	kA
Neapol	Neapol	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
hraje	hrát	k5eAaImIp3nS
i	i	k9
první	první	k4xOgInSc1
ročník	ročník	k1gInSc1
poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokráčí	dokráčet	k5eAaPmIp3nS
do	do	k7c2
semifinále	semifinále	k1gNnSc2
a	a	k8xC
tam	tam	k6eAd1
jej	on	k3xPp3gMnSc4
vyřadí	vyřadit	k5eAaPmIp3nS
budoucí	budoucí	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspur	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Maldinim	Maldini	k1gNnSc7
a	a	k8xC
Roccem	Rocce	k1gNnSc7
(	(	kIx(
<g/>
technický	technický	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
<g/>
)	)	kIx)
vedl	vést	k5eAaImAgInS
v	v	k7c6
březnu	březno	k1gNnSc6
tabulku	tabulka	k1gFnSc4
ligy	liga	k1gFnSc2
o	o	k7c4
tři	tři	k4xCgInPc4
body	bod	k1gInPc4
před	před	k7c7
konkurenty	konkurent	k1gMnPc7
<g/>
,	,	kIx,
přišla	přijít	k5eAaPmAgFnS
ale	ale	k9
porážka	porážka	k1gFnSc1
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
z	z	k7c2
Laziem	Lazius	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
přišla	přijít	k5eAaPmAgFnS
remíza	remíza	k1gFnSc1
s	s	k7c7
Turínem	Turín	k1gInSc7
a	a	k8xC
před	před	k7c7
posledním	poslední	k2eAgNnSc7d1
kolem	kolo	k1gNnSc7
hráli	hrát	k5eAaImAgMnP
finálový	finálový	k2eAgInSc4d1
zápas	zápas	k1gInSc4
poháru	pohár	k1gInSc2
PVP	PVP	kA
proti	proti	k7c3
Leedsu	Leeds	k1gInSc3
kde	kde	k6eAd1
uspěli	uspět	k5eAaPmAgMnP
výhrou	výhra	k1gFnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
finálovém	finálový	k2eAgInSc6d1
zápasu	zápas	k1gInSc6
jsou	být	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
unavení	unavení	k1gNnSc2
a	a	k8xC
v	v	k7c6
lize	liga	k1gFnSc6
tak	tak	k6eAd1
prohráli	prohrát	k5eAaPmAgMnP
ve	v	k7c6
Veroně	Verona	k1gFnSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rival	rival	k1gMnSc1
s	s	k7c7
Juventus	Juventus	k1gInSc1
FC	FC	kA
tak	tak	k9
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
musel	muset	k5eAaImAgMnS
vyhrát	vyhrát	k5eAaPmF
aby	aby	kYmCp3nS
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
když	když	k8xS
v	v	k7c6
89	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
vstřelil	vstřelit	k5eAaPmAgMnS
branku	branka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juventus	Juventus	k1gInSc1
FC	FC	kA
poráží	porážet	k5eAaImIp3nS
ve	v	k7c6
finále	finále	k1gNnSc6
italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
po	po	k7c6
penaltovém	penaltový	k2eAgInSc6d1
rozstřelu	rozstřel	k1gInSc6
a	a	k8xC
vyhrávají	vyhrávat	k5eAaImIp3nP
svůj	svůj	k3xOyFgInSc4
třetí	třetí	k4xOgInSc4
Italský	italský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
nebyla	být	k5eNaImAgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
Nereo	Nereo	k6eAd1
Rocco	Rocco	k6eAd1
<g/>
,	,	kIx,
opět	opět	k6eAd1
jako	jako	k9
trenér	trenér	k1gMnSc1
od	od	k7c2
začátku	začátek	k1gInSc2
sezóny	sezóna	k1gFnSc2
<g/>
,	,	kIx,
opustil	opustit	k5eAaPmAgMnS
lavičku	lavička	k1gFnSc4
po	po	k7c6
sedmi	sedm	k4xCc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
nahrazen	nahradit	k5eAaPmNgMnS
nejprve	nejprve	k6eAd1
Maldinim	Maldinim	k1gMnSc1
a	a	k8xC
poté	poté	k6eAd1
Trapattonim	Trapattonima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rossoneri	Rossoneri	k1gNnPc1
končí	končit	k5eAaImIp3nS
na	na	k7c6
sedmém	sedmý	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
klub	klub	k1gInSc1
hraje	hrát	k5eAaImIp3nS
dva	dva	k4xCgInPc4
krát	krát	k6eAd1
finále	finále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
prohrává	prohrávat	k5eAaImIp3nS
s	s	k7c7
Ajaxem	Ajax	k1gInSc7
Evropský	evropský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poháru	pohár	k1gInSc6
PVP	PVP	kA
prohrává	prohrávat	k5eAaImIp3nS
s	s	k7c7
Magdeburgem	Magdeburg	k1gInSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finále	finále	k1gNnSc1
si	se	k3xPyFc3
zahrají	zahrát	k5eAaPmIp3nP
i	i	k9
italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
v	v	k7c6
sezoně	sezona	k1gFnSc6
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
prohrávají	prohrávat	k5eAaImIp3nP
s	s	k7c7
Fiorentinou	Fiorentina	k1gFnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Velmi	velmi	k6eAd1
špatnou	špatný	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
v	v	k7c6
lize	liga	k1gFnSc6
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
posledních	poslední	k2eAgNnPc2d1
14	#num#	k4
kol	kolo	k1gNnPc2
je	být	k5eAaImIp3nS
nahrazen	nahrazen	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Giuseppe	Giusepp	k1gInSc5
Marchioro	Marchiora	k1gFnSc5
navrátilcem	navrátilec	k1gMnSc7
Roccem	Rocce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
i	i	k9
ten	ten	k3xDgInSc1
nezabrání	zabránit	k5eNaPmIp3nS
špatné	špatný	k2eAgFnSc3d1
hře	hra	k1gFnSc3
Rossoneri	Rossoner	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
se	se	k3xPyFc4
naštěstí	naštěstí	k6eAd1
vyhýbá	vyhýbat	k5eAaImIp3nS
sestupu	sestup	k1gInSc2
do	do	k7c2
druhé	druhý	k4xOgFnSc2
ligy	liga	k1gFnSc2
o	o	k7c4
tři	tři	k4xCgInPc4
body	bod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc4
alespoň	alespoň	k9
vyhrává	vyhrávat	k5eAaImIp3nS
opět	opět	k6eAd1
Italský	italský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
když	když	k8xS
poráží	porážet	k5eAaImIp3nS
ve	v	k7c6
finále	finále	k1gNnSc6
Inter	Intero	k1gNnPc2
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
trenéra	trenér	k1gMnSc2
Rocca	Roccus	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
končí	končit	k5eAaImIp3nS
nadobro	nadobro	k6eAd1
v	v	k7c6
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
něj	on	k3xPp3gInSc2
je	být	k5eAaImIp3nS
angažován	angažován	k2eAgMnSc1d1
bývalí	bývalý	k2eAgMnPc1d1
hráč	hráč	k1gMnSc1
Nils	Nilsa	k1gFnPc2
Liedholm	Liedholm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenér	trenér	k1gMnSc1
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
na	na	k7c6
konci	konec	k1gInSc6
sezony	sezona	k1gFnSc2
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
nasadit	nasadit	k5eAaPmF
poprvé	poprvé	k6eAd1
do	do	k7c2
zápasu	zápas	k1gInSc2
budoucí	budoucí	k2eAgFnSc4d1
legendu	legenda	k1gFnSc4
Baresiho	Baresi	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
ten	ten	k3xDgMnSc1
nepomůže	pomoct	k5eNaPmIp3nS
klubu	klub	k1gInSc2
k	k	k7c3
lepšímu	dobrý	k2eAgNnSc3d2
místu	místo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Končí	končit	k5eAaImIp3nS
na	na	k7c6
čtvrtém	čtvrtý	k4xOgNnSc6
místě	místo	k1gNnSc6
se	se	k3xPyFc4
vstupenkou	vstupenka	k1gFnSc7
do	do	k7c2
poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
se	se	k3xPyFc4
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
osmi	osm	k4xCc2
finále	finále	k1gNnPc2
když	když	k8xS
jej	on	k3xPp3gInSc4
vyřadí	vyřadit	k5eAaPmIp3nS
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
klub	klub	k1gInSc1
získává	získávat	k5eAaImIp3nS
vytoužený	vytoužený	k2eAgInSc4d1
desátý	desátý	k4xOgInSc4
titul	titul	k1gInSc4
a	a	k8xC
může	moct	k5eAaImIp3nS
na	na	k7c6
dresu	dres	k1gInSc6
nosit	nosit	k5eAaImF
zlatou	zlatý	k2eAgFnSc4d1
hvězdu	hvězda	k1gFnSc4
nad	nad	k7c7
logem	log	k1gInSc7
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vítězné	vítězný	k2eAgFnSc6d1
sezoně	sezona	k1gFnSc6
končí	končit	k5eAaImIp3nS
kariéru	kariéra	k1gFnSc4
Gianni	Gianeň	k1gFnSc3
Rivera	River	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
liga	liga	k1gFnSc1
a	a	k8xC
návrat	návrat	k1gInSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
-	-	kIx~
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
sezóny	sezóna	k1gFnSc2
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
odhalen	odhalit	k5eAaPmNgInS
sázkařský	sázkařský	k2eAgInSc1d1
skandál	skandál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
klub	klub	k1gInSc1
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
třetím	třetí	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
skandálu	skandál	k1gInSc6
zapleten	zapleten	k2eAgMnSc1d1
a	a	k8xC
byl	být	k5eAaImAgMnS
potrestán	potrestat	k5eAaPmNgMnS
sestupem	sestup	k1gInSc7
do	do	k7c2
druhé	druhý	k4xOgFnSc2
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tresty	trest	k1gInPc7
padli	padnout	k5eAaImAgMnP,k5eAaPmAgMnP
i	i	k9
na	na	k7c4
hráče	hráč	k1gMnSc4
i	i	k8xC
presidenta	president	k1gMnSc4
klubu	klub	k1gInSc2
Colomba	Colomba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rossoneri	Rossoneri	k1gNnPc6
hraje	hrát	k5eAaImIp3nS
Pohár	pohár	k1gInSc1
PMEZ	PMEZ	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nedostává	dostávat	k5eNaImIp3nS
přes	přes	k7c4
první	první	k4xOgNnSc4
kolo	kolo	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
jej	on	k3xPp3gMnSc4
vyřadil	vyřadit	k5eAaPmAgMnS
portugalský	portugalský	k2eAgInSc4d1
klub	klub	k1gInSc4
FC	FC	kA
Porto	porto	k1gNnSc1
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sezona	sezona	k1gFnSc1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
lize	liga	k1gFnSc6
dopadla	dopadnout	k5eAaPmAgFnS
na	na	k7c4
výbornou	výborná	k1gFnSc4
<g/>
.	.	kIx.
vyhráli	vyhrát	k5eAaPmAgMnP
soutěž	soutěž	k1gFnSc4
a	a	k8xC
postoupili	postoupit	k5eAaPmAgMnP
zpět	zpět	k6eAd1
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
sezonu	sezona	k1gFnSc4
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
trenérem	trenér	k1gMnSc7
Luigi	Luig	k1gFnSc2
Radice	Radice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
po	po	k7c6
16	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
je	být	k5eAaImIp3nS
nahrazen	nahrazen	k2eAgInSc1d1
Galbiatim	Galbiatim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgInS
na	na	k7c4
14	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
a	a	k8xC
sestoupil	sestoupit	k5eAaPmAgMnS
do	do	k7c2
druhé	druhý	k4xOgFnSc2
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc4
alespoň	alespoň	k9
vyhraje	vyhrát	k5eAaPmIp3nS
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgNnSc1
sezona	sezona	k1gFnSc1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
lize	liga	k1gFnSc6
byla	být	k5eAaImAgFnS
též	též	k9
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
jej	on	k3xPp3gMnSc4
vyhrál	vyhrát	k5eAaPmAgInS
a	a	k8xC
postoupil	postoupit	k5eAaPmAgInS
zpět	zpět	k6eAd1
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasloužil	zasloužit	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
to	ten	k3xDgNnSc4
trenér	trenér	k1gMnSc1
Ilario	Ilario	k1gMnSc1
Castagner	Castagner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
ale	ale	k8xC
propuštěn	propustit	k5eAaPmNgInS
po	po	k7c6
24	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
v	v	k7c6
sezoně	sezona	k1gFnSc6
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
a	a	k8xC
nahrazen	nahradit	k5eAaPmNgInS
opět	opět	k6eAd1
Galbiatim	Galbiatim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
osmém	osmý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
za	za	k7c4
Rossoneri	Rossonere	k1gFnSc4
hráli	hrát	k5eAaImAgMnP
posily	posila	k1gFnPc4
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
jako	jako	k8xS,k8xC
Luther	Luthra	k1gFnPc2
Blissett	Blissetta	k1gFnPc2
(	(	kIx(
<g/>
Watford	Watford	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
a	a	k8xC
Eric	Eric	k1gFnSc1
Gerets	Geretsa	k1gFnPc2
(	(	kIx(
<g/>
Standard	standard	k1gInSc1
Liè	Liè	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sezoně	sezona	k1gFnSc6
odešli	odejít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
novou	nový	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
trenérem	trenér	k1gMnSc7
po	po	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
strávené	strávený	k2eAgFnSc2d1
v	v	k7c6
AS	as	k1gNnSc6
Řím	Řím	k1gInSc1
opět	opět	k6eAd1
Nils	Nils	k1gInSc4
Liedholm	Liedholma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kupují	kupovat	k5eAaImIp3nP
se	se	k3xPyFc4
hráči	hráč	k1gMnPc1
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
:	:	kIx,
Mark	Mark	k1gMnSc1
Hateley	Hatelea	k1gFnSc2
(	(	kIx(
<g/>
Portsmouth	Portsmouth	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
a	a	k8xC
Ray	Ray	k1gMnSc1
Wilkins	Wilkins	k1gInSc1
(	(	kIx(
<g/>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
Udinese	Udinese	k1gFnSc2
Pietro	Pietro	k1gNnSc1
Paolo	Paolo	k1gNnSc1
Virdis	Virdis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jeden	jeden	k4xCgInSc4
zápas	zápas	k1gInSc4
dostává	dostávat	k5eAaImIp3nS
šanci	šance	k1gFnSc4
i	i	k8xC
mladík	mladík	k1gMnSc1
Paolo	Paolo	k1gNnSc4
Maldini	Maldin	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezona	sezona	k1gFnSc1
se	se	k3xPyFc4
dokončí	dokončit	k5eAaPmIp3nS
na	na	k7c6
pátém	pátý	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zaručuje	zaručovat	k5eAaImIp3nS
účast	účast	k1gFnSc4
v	v	k7c6
poháru	pohár	k1gInSc6
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
se	se	k3xPyFc4
dostávají	dostávat	k5eAaImIp3nP
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
UC	UC	kA
Sampdoria	Sampdorium	k1gNnPc1
je	on	k3xPp3gNnSc4
poráží	porážet	k5eAaImIp3nP
po	po	k7c6
výsledcích	výsledek	k1gInPc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
sezona	sezona	k1gFnSc1
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
zimy	zima	k1gFnSc2
klub	klub	k1gInSc1
umísťoval	umísťovat	k5eAaImAgInS
do	do	k7c2
třetího	třetí	k4xOgNnSc2
místa	místo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
1985	#num#	k4
se	se	k3xPyFc4
prezident	prezident	k1gMnSc1
Giuseppe	Giusepp	k1gInSc5
Farina	Farino	k1gNnPc1
po	po	k7c4
vyřazení	vyřazení	k1gNnSc4
v	v	k7c6
osmi	osm	k4xCc6
finále	finále	k1gNnPc6
poháru	pohár	k1gInSc2
UEFA	UEFA	kA
s	s	k7c7
belgickým	belgický	k2eAgInSc7d1
klubem	klub	k1gInSc7
Waregemem	Waregem	k1gInSc7
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
tvrdými	tvrdý	k2eAgInPc7d1
protesty	protest	k1gInPc7
fanoušků	fanoušek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
příchodem	příchod	k1gInSc7
nového	nový	k2eAgInSc2d1
roku	rok	k1gInSc2
se	se	k3xPyFc4
zjistí	zjistit	k5eAaPmIp3nS
těžká	těžký	k2eAgFnSc1d1
ekonomická	ekonomický	k2eAgFnSc1d1
situace	situace	k1gFnSc1
<g/>
:	:	kIx,
klub	klub	k1gInSc1
je	být	k5eAaImIp3nS
plná	plný	k2eAgFnSc1d1
dluhů	dluh	k1gInPc2
a	a	k8xC
riskuje	riskovat	k5eAaBmIp3nS
bankrot	bankrot	k1gInSc1
<g/>
,	,	kIx,
pokud	pokud	k8xS
vše	všechen	k3xTgNnSc1
nebude	být	k5eNaImBp3nS
splaceno	splatit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
situaci	situace	k1gFnSc6
odstoupí	odstoupit	k5eAaPmIp3nS
Giuseppe	Giusepp	k1gInSc5
Farina	Farin	k2eAgNnPc1d1
a	a	k8xC
střídají	střídat	k5eAaImIp3nP
se	se	k3xPyFc4
prezident	prezident	k1gMnSc1
<g/>
:	:	kIx,
Rosario	Rosaria	k1gMnSc5
Lo	Lo	k1gMnSc5
Verde	Verd	k1gMnSc5
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
prezidentem	prezident	k1gMnSc7
po	po	k7c4
dobu	doba	k1gFnSc4
51	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
přichází	přicházet	k5eAaImIp3nS
milánský	milánský	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
Silvio	Silvio	k1gMnSc1
Berlusconi	Berluscon	k1gMnPc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
klub	klub	k1gInSc1
kupuje	kupovat	k5eAaImIp3nS
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1986	#num#	k4
a	a	k8xC
stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnSc7
prezidentem	prezident	k1gMnSc7
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
klubu	klub	k1gInSc2
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
Adriano	Adriana	k1gFnSc5
Galliani	Gallian	k1gMnPc1
a	a	k8xC
sportovním	sportovní	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
Ariedo	Ariedo	k1gNnSc1
Braida	Braida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
končí	končit	k5eAaImIp3nS
klub	klub	k1gInSc4
na	na	k7c6
sedmém	sedmý	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Vítězná	vítězný	k2eAgFnSc1d1
éra	éra	k1gFnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
-	-	kIx~
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nový	nový	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Silvio	Silvio	k1gMnSc1
Berlusconi	Berluscoň	k1gFnSc3
omlazuje	omlazovat	k5eAaImIp3nS
klub	klub	k1gInSc1
a	a	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
první	první	k4xOgInSc4
nákup	nákup	k1gInSc4
je	být	k5eAaImIp3nS
z	z	k7c2
Atalanty	Atalant	k1gMnPc4
talentovaný	talentovaný	k2eAgMnSc1d1
záložník	záložník	k1gMnSc1
Roberto	Roberta	k1gFnSc5
Donadoni	Donadon	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přichází	přicházet	k5eAaImIp3nS
i	i	k9
Galli	Galle	k1gFnSc4
<g/>
,	,	kIx,
Bonetti	Bonetti	k1gNnSc4
<g/>
,	,	kIx,
Galderisi	Galderise	k1gFnSc4
a	a	k8xC
Massaro	Massara	k1gFnSc5
<g/>
,	,	kIx,
Po	po	k7c6
pouze	pouze	k6eAd1
jedné	jeden	k4xCgFnSc6
sezóně	sezóna	k1gFnSc6
v	v	k7c6
Rossoneri	Rossoner	k1gFnSc6
končí	končit	k5eAaImIp3nS
Paolo	Paolo	k1gNnSc1
Rossi	Rosse	k1gFnSc4
výměnou	výměna	k1gFnSc7
za	za	k7c4
Galderisiho	Galderisi	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezona	sezona	k1gFnSc1
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
končí	končit	k5eAaImIp3nS
pátým	pátý	k4xOgNnSc7
místem	místo	k1gNnSc7
zaručující	zaručující	k2eAgFnSc4d1
účast	účast	k1gFnSc4
v	v	k7c6
poháru	pohár	k1gInSc6
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenér	trenér	k1gMnSc1
Nils	Nilsa	k1gFnPc2
Liedholm	Liedholm	k1gMnSc1
po	po	k7c6
24	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
odchází	odcházet	k5eAaImIp3nS
a	a	k8xC
místo	místo	k7c2
něj	on	k3xPp3gNnSc2
je	být	k5eAaImIp3nS
dočasně	dočasně	k6eAd1
nahrazen	nahrazen	k2eAgInSc1d1
Fabio	Fabio	k6eAd1
Capello	Capello	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezóna	sezóna	k1gFnSc1
je	být	k5eAaImIp3nS
úspěšná	úspěšný	k2eAgFnSc1d1
jen	jen	k9
jako	jako	k8xC,k8xS
vítězství	vítězství	k1gNnSc1
Virdise	Virdise	k1gFnSc2
v	v	k7c6
tabulce	tabulka	k1gFnSc6
střelců	střelec	k1gMnPc2
se	s	k7c7
17	#num#	k4
brankami	branka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
období	období	k1gNnSc4
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
si	se	k3xPyFc3
vedení	vedení	k1gNnSc1
vybere	vybrat	k5eAaPmIp3nS
nového	nový	k2eAgMnSc4d1
trenéra	trenér	k1gMnSc4
v	v	k7c6
Parmě	Parma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arrigo	Arrigo	k1gMnSc1
Sacchi	Sacche	k1gFnSc4
je	být	k5eAaImIp3nS
cesta	cesta	k1gFnSc1
k	k	k7c3
úspěchu	úspěch	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přicházejí	přicházet	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
jako	jako	k8xC,k8xS
je	on	k3xPp3gFnPc4
Ancelotti	Ancelotť	k1gFnPc4
(	(	kIx(
<g/>
AS	as	k9
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nizozemské	nizozemský	k2eAgNnSc1d1
duo	duo	k1gNnSc1
Gullit	Gullit	k1gInSc1
(	(	kIx(
<g/>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
van	van	k1gInSc1
Basten	Bastno	k1gNnPc2
(	(	kIx(
<g/>
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Roberto	Roberta	k1gFnSc5
Mussi	Muss	k1gMnSc3
(	(	kIx(
<g/>
Parma	Parma	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
hraje	hrát	k5eAaImIp3nS
totální	totální	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
na	na	k7c4
rychle	rychle	k6eAd1
kombinaci	kombinace	k1gFnSc4
s	s	k7c7
míčem	míč	k1gInSc7
a	a	k8xC
útočným	útočný	k2eAgInSc7d1
stylem	styl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
úspěchy	úspěch	k1gInPc1
se	se	k3xPyFc4
dostaví	dostavit	k5eAaPmIp3nP
ihned	ihned	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
vyhrává	vyhrávat	k5eAaImIp3nS
titul	titul	k1gInSc1
o	o	k7c4
3	#num#	k4
body	bod	k1gInPc4
před	před	k7c7
SSC	SSC	kA
Neapol	Neapol	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
v	v	k7c6
poháru	pohár	k1gInSc6
UEFA	UEFA	kA
je	být	k5eAaImIp3nS
vyřazen	vyřazen	k2eAgInSc1d1
již	již	k6eAd1
ve	v	k7c6
druhém	druhý	k4xOgInSc6
kole	kolo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
se	se	k3xPyFc4
k	k	k7c3
nizozemskému	nizozemský	k2eAgNnSc3d1
duu	duo	k1gNnSc3
přidává	přidávat	k5eAaImIp3nS
třetí	třetí	k4xOgMnSc1
nizozemec	nizozemec	k1gMnSc1
<g/>
:	:	kIx,
Frank	Frank	k1gMnSc1
Rijkaard	Rijkaard	k1gMnSc1
<g/>
,	,	kIx,
záložník	záložník	k1gMnSc1
převzatý	převzatý	k2eAgMnSc1d1
ze	z	k7c2
Zaragozy	Zaragoz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
po	po	k7c6
čtyřiceti	čtyřicet	k4xCc6
letech	léto	k1gNnPc6
po	po	k7c4
Gre-No	Gre-No	k1gMnSc1
<g/>
-Li	-Li	k?
se	se	k3xPyFc4
formuje	formovat	k5eAaImIp3nS
další	další	k2eAgNnSc1d1
trio	trio	k1gNnSc1
zahraničních	zahraniční	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umísťuje	umísťovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
třetím	třetí	k4xOgNnSc6
místě	místo	k1gNnSc6
o	o	k7c4
12	#num#	k4
bodů	bod	k1gInPc2
za	za	k7c7
Interem	Inter	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastní	účastnit	k5eAaImIp3nS
se	se	k3xPyFc4
poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
a	a	k8xC
celkově	celkově	k6eAd1
jej	on	k3xPp3gMnSc4
vyhraje	vyhrát	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhrává	vyhrávat	k5eAaImIp3nS
jí	on	k3xPp3gFnSc7
celkově	celkově	k6eAd1
potřetí	potřetí	k4xO
a	a	k8xC
ve	v	k7c6
finále	finále	k1gNnSc6
poráží	porážet	k5eAaImIp3nS
rumunský	rumunský	k2eAgInSc1d1
klub	klub	k1gInSc1
z	z	k7c2
Bukurešťe	Bukurešť	k1gInSc2
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvou	dva	k4xCgFnPc6
brankách	branka	k1gFnPc6
se	se	k3xPyFc4
podělilo	podělit	k5eAaPmAgNnS
nizozemské	nizozemský	k2eAgNnSc1d1
duo	duo	k1gNnSc1
Gullit	Gullita	k1gFnPc2
a	a	k8xC
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhrávají	vyhrávat	k5eAaImIp3nP
i	i	k8xC
první	první	k4xOgInSc1
ročník	ročník	k1gInSc1
v	v	k7c6
italském	italský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poráží	porážet	k5eAaImIp3nS
klub	klub	k1gInSc1
Sampdorii	Sampdorie	k1gFnSc4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
Rossoneri	Rossoneri	k1gNnPc2
se	se	k3xPyFc4
umísťují	umísťovat	k5eAaImIp3nP
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
o	o	k7c4
2	#num#	k4
body	bod	k1gInPc4
za	za	k7c7
SSC	SSC	kA
Neapol	Neapol	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
síly	síla	k1gFnPc1
jsou	být	k5eAaImIp3nP
ale	ale	k9
věnovány	věnovat	k5eAaPmNgInP,k5eAaImNgInP
pro	pro	k7c4
obhajobu	obhajoba	k1gFnSc4
titulu	titul	k1gInSc2
v	v	k7c6
Poháru	pohár	k1gInSc6
PMEZ	PMEZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ji	on	k3xPp3gFnSc4
taky	taky	k6eAd1
vyhrávají	vyhrávat	k5eAaImIp3nP
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
poráží	porážet	k5eAaImIp3nS
Benfiku	Benfika	k1gFnSc4
Rijkaardovou	Rijkaardův	k2eAgFnSc7d1
brankou	branka	k1gFnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
nejlepší	dobrý	k2eAgInSc1d3
klub	klub	k1gInSc1
na	na	k7c6
světě	svět	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
klub	klub	k1gInSc1
potvrdí	potvrdit	k5eAaPmIp3nS
ve	v	k7c6
finále	finále	k1gNnSc6
o	o	k7c4
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poráží	porážet	k5eAaImIp3nS
kolumbijský	kolumbijský	k2eAgInSc1d1
klub	klub	k1gInSc1
Atlético	Atlético	k6eAd1
Nacional	Nacional	k1gFnSc4
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhrává	vyhrávat	k5eAaImIp3nS
též	též	k9
Evropský	evropský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
když	když	k8xS
poráží	porážet	k5eAaImIp3nS
Barcelonu	Barcelona	k1gFnSc4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postoupí	postoupit	k5eAaPmIp3nS
i	i	k9
do	do	k7c2
finále	finále	k1gNnSc2
italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
,	,	kIx,
jenže	jenže	k8xC
lepším	dobrý	k2eAgInSc7d2
klubem	klub	k1gInSc7
byl	být	k5eAaImAgInS
Juventus	Juventus	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
chybělo	chybět	k5eAaImAgNnS
pět	pět	k4xCc4
bodů	bod	k1gInPc2
do	do	k7c2
vítězství	vítězství	k1gNnSc2
<g/>
,	,	kIx,
jenže	jenže	k8xC
lepší	dobrý	k2eAgMnSc1d2
byl	být	k5eAaImAgInS
klub	klub	k1gInSc1
UC	UC	kA
Sampdoria	Sampdorium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráli	hrát	k5eAaImAgMnP
o	o	k7c4
Evropský	evropský	k2eAgInSc4d1
superpohár	superpohár	k1gInSc4
a	a	k8xC
o	o	k7c4
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
obě	dva	k4xCgFnPc4
trofeje	trofej	k1gFnPc4
získali	získat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnPc1
získali	získat	k5eAaPmAgMnP
po	po	k7c6
výhrách	výhra	k1gFnPc6
nad	nad	k7c7
Sampdorií	Sampdorie	k1gFnSc7
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
druhou	druhý	k4xOgFnSc4
trofej	trofej	k1gFnSc4
klub	klub	k1gInSc1
získal	získat	k5eAaPmAgInS
díky	díky	k7c3
vítězství	vítězství	k1gNnSc3
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
nad	nad	k7c7
paraguayským	paraguayský	k2eAgInSc7d1
klubem	klub	k1gInSc7
Olimpia	Olimpium	k1gNnSc2
Asunción	Asunción	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Poháru	pohár	k1gInSc6
PMEZ	PMEZ	kA
kde	kde	k6eAd1
útočí	útočit	k5eAaImIp3nS
na	na	k7c4
třetí	třetí	k4xOgInSc4
titul	titul	k1gInSc4
v	v	k7c6
řadě	řada	k1gFnSc6
jsou	být	k5eAaImIp3nP
Rossoneri	Rossoner	k1gMnPc1
vyřazeni	vyřazen	k2eAgMnPc1d1
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
francouzským	francouzský	k2eAgInSc7d1
klubem	klub	k1gInSc7
Olympique	Olympique	k1gNnSc4
de	de	k?
Marseille	Marseille	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
zápas	zápas	k1gInSc1
byl	být	k5eAaImAgInS
zkontumován	zkontumovat	k5eAaImNgInS,k5eAaPmNgInS,k5eAaBmNgInS
protože	protože	k8xS
když	když	k8xS
běžela	běžet	k5eAaImAgFnS
88	#num#	k4
minuta	minuta	k1gFnSc1
hry	hra	k1gFnSc2
za	za	k7c2
stavu	stav	k1gInSc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
pro	pro	k7c4
Marseille	Marseille	k1gFnSc2
zhasla	zhasnout	k5eAaPmAgNnP
světla	světlo	k1gNnPc1
na	na	k7c6
stadionu	stadion	k1gInSc6
a	a	k8xC
na	na	k7c4
popud	popud	k1gInSc4
generálního	generální	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
Gallianiho	Galliani	k1gMnSc2
jsou	být	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
vyvedeni	vyvést	k5eAaPmNgMnP
ze	z	k7c2
hřiště	hřiště	k1gNnSc2
do	do	k7c2
šaten	šatna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
světla	světlo	k1gNnSc2
opět	opět	k6eAd1
rozsvítí	rozsvítit	k5eAaPmIp3nP
hráči	hráč	k1gMnPc1
již	již	k6eAd1
na	na	k7c4
hřiště	hřiště	k1gNnSc4
nepřijdou	přijít	k5eNaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
incidentu	incident	k1gInSc6
je	být	k5eAaImIp3nS
klub	klub	k1gInSc1
diskvalifikován	diskvalifikovat	k5eAaBmNgInS
na	na	k7c4
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
ze	z	k7c2
všech	všecek	k3xTgInPc2
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
čtyřech	čtyři	k4xCgFnPc6
úspěšných	úspěšný	k2eAgFnPc6d1
sezónách	sezóna	k1gFnPc6
se	se	k3xPyFc4
Arrigo	Arrigo	k1gMnSc1
Sacchi	Sacch	k1gFnSc2
rozhodl	rozhodnout	k5eAaPmAgMnS
přijmout	přijmout	k5eAaPmF
trénovat	trénovat	k5eAaImF
reprezentaci	reprezentace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Náhrada	náhrada	k1gFnSc1
za	za	k7c4
Sacchiho	Sacchi	k1gMnSc4
je	být	k5eAaImIp3nS
vybrán	vybrán	k2eAgInSc1d1
Fabio	Fabio	k6eAd1
Capello	Capello	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
klub	klub	k1gInSc1
nemohl	moct	k5eNaImAgInS
hrát	hrát	k5eAaImF
evropské	evropský	k2eAgInPc4d1
poháry	pohár	k1gInPc4
po	po	k7c6
událostech	událost	k1gFnPc6
v	v	k7c6
předchozím	předchozí	k2eAgInSc6d1
ročníku	ročník	k1gInSc6
poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
<g/>
,	,	kIx,
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
se	se	k3xPyFc4
lize	liga	k1gFnSc3
a	a	k8xC
v	v	k7c6
ní	on	k3xPp3gFnSc6
vyhrál	vyhrát	k5eAaPmAgMnS
titul	titul	k1gInSc4
bez	bez	k7c2
prohry	prohra	k1gFnSc2
a	a	k8xC
s	s	k7c7
náskokem	náskok	k1gInSc7
o	o	k7c6
osm	osm	k4xCc4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	V	kA
Coppa	Coppa	k1gFnSc1
Italia	Italia	k1gFnSc1
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
|	|	kIx~
<g/>
italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
klub	klub	k1gInSc1
končí	končit	k5eAaImIp3nS
v	v	k7c6
semifinále	semifinále	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
prohraje	prohrát	k5eAaPmIp3nS
s	s	k7c7
Juventusem	Juventus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tom	ten	k3xDgInSc6
se	se	k3xPyFc4
klub	klub	k1gInSc1
stane	stanout	k5eAaPmIp3nS
neporazitelným	porazitelný	k2eNgInSc7d1
(	(	kIx(
<g/>
Invincibles	Invincibles	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
klubová	klubový	k2eAgFnSc1d1
přezdívka	přezdívka	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Capella	Capell	k1gMnSc2
do	do	k7c2
sezony	sezona	k1gFnSc2
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
sezonou	sezona	k1gFnSc7
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
klub	klub	k1gInSc1
posiluje	posilovat	k5eAaImIp3nS
<g/>
:	:	kIx,
Papin	Papin	k1gMnSc1
<g/>
,	,	kIx,
Boban	Boban	k1gMnSc1
<g/>
,	,	kIx,
Savićević	Savićević	k1gMnSc1
a	a	k8xC
hlavně	hlavně	k9
nejdražší	drahý	k2eAgInSc4d3
přestup	přestup	k1gInSc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
Gianluigi	Gianluig	k1gFnSc2
Lentini	Lentin	k2eAgMnPc1d1
(	(	kIx(
<g/>
32	#num#	k4
miliard	miliarda	k4xCgFnPc2
lir	lira	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
natáhne	natáhnout	k5eAaPmIp3nS
sérii	série	k1gFnSc4
v	v	k7c6
lize	liga	k1gFnSc6
bez	bez	k7c2
porážky	porážka	k1gFnSc2
až	až	k9
na	na	k7c4
58	#num#	k4
zápasů	zápas	k1gInPc2
a	a	k8xC
nastaví	nastavět	k5eAaBmIp3nP,k5eAaPmIp3nP
tak	tak	k6eAd1
rekord	rekord	k1gInSc4
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získává	získávat	k5eAaImIp3nS
13	#num#	k4
titul	titul	k1gInSc4
když	když	k8xS
poráží	porážet	k5eAaImIp3nS
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
body	bod	k1gInPc4
své	svůj	k3xOyFgFnSc2
rivaly	rival	k1gMnPc7
z	z	k7c2
města	město	k1gNnSc2
Inter	Intra	k1gFnPc2
<g/>
,	,	kIx,
získává	získávat	k5eAaImIp3nS
též	též	k9
italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
i	i	k9
po	po	k7c6
roce	rok	k1gInSc6
vyhnanství	vyhnanství	k1gNnSc2
evropské	evropský	k2eAgInPc4d1
poháry	pohár	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zúčastní	zúčastnit	k5eAaPmIp3nS
se	se	k3xPyFc4
druhého	druhý	k4xOgInSc2
ročníku	ročník	k1gInSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ní	on	k3xPp3gFnSc6
exceluje	excelovat	k5eAaImIp3nS
<g/>
,	,	kIx,
z	z	k7c2
deseti	deset	k4xCc2
utkání	utkání	k1gNnPc2
vyhrává	vyhrávat	k5eAaImIp3nS
všechny	všechen	k3xTgInPc4
a	a	k8xC
skore	skor	k1gInSc5
je	být	k5eAaImIp3nS
23	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
!	!	kIx.
</s>
<s desamb="1">
Postupuje	postupovat	k5eAaImIp3nS
do	do	k7c2
finále	finále	k1gNnSc2
a	a	k8xC
narazí	narazit	k5eAaPmIp3nS
na	na	k7c4
klub	klub	k1gInSc4
Olympique	Olympiqu	k1gFnSc2
de	de	k?
Marseille	Marseille	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
stadionu	stadion	k1gInSc6
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rossoneri	Rossoneri	k1gNnSc7
utkání	utkání	k1gNnSc2
prohrává	prohrávat	k5eAaImIp3nS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
jedinou	jediný	k2eAgFnSc4d1
branku	branka	k1gFnSc4
vstřelil	vstřelit	k5eAaPmAgMnS
Basile	Basila	k1gFnSc3
Boli	Bole	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
fotbalistu	fotbalista	k1gMnSc4
van	van	k1gInSc4
Bastena	Basten	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
poslední	poslední	k2eAgInSc1d1
zápas	zápas	k1gInSc1
kariéry	kariéra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
sezona	sezona	k1gFnSc1
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
je	být	k5eAaImIp3nS
již	již	k6eAd1
bez	bez	k7c2
van	vana	k1gFnPc2
Bastena	Basten	k2eAgFnSc1d1
<g/>
,	,	kIx,
Lentiniho	Lentiniha	k1gMnSc5
(	(	kIx(
<g/>
zranění	zranění	k1gNnPc4
z	z	k7c2
těžké	těžký	k2eAgFnSc2d1
autonehody	autonehoda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rijkaarda	Rijkaarda	k1gFnSc1
(	(	kIx(
<g/>
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Gullita	Gullita	k1gFnSc1
(	(	kIx(
<g/>
UC	UC	kA
Sampdoria	Sampdorium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přichází	přicházet	k5eAaImIp3nS
mladý	mladý	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
Christian	Christian	k1gMnSc1
Panucci	Panucce	k1gFnSc4
(	(	kIx(
<g/>
Janov	Janov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Florin	Florin	k1gInSc1
Răducioiu	Răducioius	k1gMnSc3
<g/>
,	,	kIx,
první	první	k4xOgInSc1
rumunský	rumunský	k2eAgInSc1d1
fotbalisty	fotbalista	k1gMnPc7
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
Marcel	Marcel	k1gMnSc1
Desailly	Desailla	k1gFnSc2
(	(	kIx(
<g/>
Olympique	Olympique	k1gFnSc1
de	de	k?
Marseille	Marseille	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
dánský	dánský	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
Brian	Brian	k1gMnSc1
Laudrup	Laudrup	k1gMnSc1
(	(	kIx(
<g/>
Fiorentina	Fiorentina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získává	získávat	k5eAaImIp3nS
titul	titul	k1gInSc1
číslo	číslo	k1gNnSc1
14	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
tabulku	tabulka	k1gFnSc4
vyhrál	vyhrát	k5eAaPmAgMnS
o	o	k7c4
tři	tři	k4xCgInPc4
body	bod	k1gInPc4
před	před	k7c7
Juventusem	Juventus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
korupci	korupce	k1gFnSc3
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
klubu	klub	k1gInSc6
Olympique	Olympiqu	k1gFnSc2
de	de	k?
Marseille	Marseille	k1gFnSc1
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
směly	smět	k5eAaImAgInP
Rossoneri	Rossoneri	k1gNnSc4
hrát	hrát	k5eAaImF
o	o	k7c4
Evropský	evropský	k2eAgInSc4d1
superpohár	superpohár	k1gInSc4
a	a	k8xC
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
nezvládl	zvládnout	k5eNaPmAgInS
vyhrát	vyhrát	k5eAaPmF
žádnou	žádný	k3yNgFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
prohráli	prohrát	k5eAaPmAgMnP
až	až	k9
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
s	s	k7c7
klubem	klub	k1gInSc7
AC	AC	kA
Parma	Parma	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
ve	v	k7c6
druhém	druhý	k4xOgInSc6
je	být	k5eAaImIp3nS
porazil	porazit	k5eAaPmAgMnS
brazilský	brazilský	k2eAgMnSc1d1
obhájce	obhájce	k1gMnSc1
titulu	titul	k1gInSc2
Sã	Sã	k1gMnSc1
Paulo	Paula	k1gFnSc5
FC	FC	kA
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
opět	opět	k6eAd1
dominuje	dominovat	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
z	z	k7c2
deseti	deset	k4xCc2
zápasů	zápas	k1gInPc2
má	mít	k5eAaImIp3nS
pět	pět	k4xCc4
výher	výhra	k1gFnPc2
a	a	k8xC
pět	pět	k4xCc4
remíz	remíza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
semifinále	semifinále	k1gNnSc6
poráží	porážet	k5eAaImIp3nS
AS	as	k9
Monaco	Monaco	k6eAd1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
ve	v	k7c6
finále	finále	k1gNnSc6
poráží	porážet	k5eAaImIp3nP
FC	FC	kA
Barcelonu	Barcelona	k1gFnSc4
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
pěti	pět	k4xCc3
násobným	násobný	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěch	úspěch	k1gInSc1
je	být	k5eAaImIp3nS
doprovázen	doprovázet	k5eAaImNgInS
dalšími	další	k2eAgFnPc7d1
rekordními	rekordní	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgFnPc7
vynikají	vynikat	k5eAaImIp3nP
dva	dva	k4xCgMnPc1
<g/>
:	:	kIx,
brankář	brankář	k1gMnSc1
Sebastiano	Sebastiana	k1gFnSc5
Rossi	Rosse	k1gFnSc3
<g/>
,	,	kIx,
ustanovil	ustanovit	k5eAaPmAgMnS
nový	nový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
bez	bez	k7c2
obdržených	obdržený	k2eAgFnPc2d1
branek	branka	k1gFnPc2
(	(	kIx(
<g/>
929	#num#	k4
minut	minuta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
rekord	rekord	k1gInSc1
vydrží	vydržet	k5eAaPmIp3nS
22	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
překoná	překonat	k5eAaPmIp3nS
jej	on	k3xPp3gNnSc4
Gianluigi	Gianluigi	k1gNnSc4
Buffon	Buffon	k1gInSc1
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
druhý	druhý	k4xOgInSc4
rekord	rekord	k1gInSc4
je	být	k5eAaImIp3nS
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
počet	počet	k1gInSc1
obdržených	obdržený	k2eAgFnPc2d1
branek	branka	k1gFnPc2
v	v	k7c6
sezoně	sezona	k1gFnSc6
(	(	kIx(
<g/>
15	#num#	k4
ze	z	k7c2
34	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AC	AC	kA
Milán	Milán	k1gInSc1
a	a	k8xC
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
a	a	k8xC
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
jediné	jediný	k2eAgInPc1d1
italské	italský	k2eAgInPc1d1
kluby	klub	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
v	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
vyhrály	vyhrát	k5eAaPmAgInP
ligu	liga	k1gFnSc4
a	a	k8xC
Ligu	liga	k1gFnSc4
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
ještě	ještě	k6eAd1
vítězí	vítězit	k5eAaImIp3nS
v	v	k7c6
italském	italský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
poráží	porážet	k5eAaImIp3nS
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s>
Ligová	ligový	k2eAgFnSc1d1
sezona	sezona	k1gFnSc1
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
je	být	k5eAaImIp3nS
jíž	jenž	k3xRgFnSc3
bodována	bodovat	k5eAaImNgFnS
třemi	tři	k4xCgInPc7
body	bod	k1gInPc7
za	za	k7c4
výhru	výhra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubu	klub	k1gInSc2
se	se	k3xPyFc4
nedaří	dařit	k5eNaImIp3nS
i	i	k9
přes	přes	k7c4
odchody	odchod	k1gInPc4
hráčů	hráč	k1gMnPc2
jako	jako	k8xC,k8xS
Papin	Papin	k1gMnSc1
a	a	k8xC
Laudrup	Laudrup	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrté	čtvrtý	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
tabulce	tabulka	k1gFnSc6
je	být	k5eAaImIp3nS
nejhorší	zlý	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
od	od	k7c2
sezony	sezona	k1gFnSc2
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
sezony	sezona	k1gFnSc2
klub	klub	k1gInSc1
poráží	porážet	k5eAaImIp3nS
UC	UC	kA
Sampdoria	Sampdorium	k1gNnSc2
ve	v	k7c4
finále	finále	k1gNnSc4
italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
na	na	k7c4
penalty	penalta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utkání	utkání	k1gNnSc1
skončilo	skončit	k5eAaPmAgNnS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
jedinou	jediný	k2eAgFnSc4d1
branku	branka	k1gFnSc4
vstřelil	vstřelit	k5eAaPmAgMnS
navrátilec	navrátilec	k1gMnSc1
Ruud	Ruud	k1gMnSc1
Gullit	Gullit	k1gInSc4
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
s	s	k7c7
klubem	klub	k1gInSc7
vyhrál	vyhrát	k5eAaPmAgMnS
poslední	poslední	k2eAgFnSc4d1
trofej	trofej	k1gFnSc4
v	v	k7c6
kariéře	kariéra	k1gFnSc6
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
jej	on	k3xPp3gInSc4
klub	klub	k1gInSc4
v	v	k7c6
listopadu	listopad	k1gInSc6
1994	#num#	k4
prodal	prodat	k5eAaPmAgMnS
do	do	k7c2
UC	UC	kA
Sampdoria	Sampdorium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
1994	#num#	k4
v	v	k7c6
Tokiu	Tokio	k1gNnSc6
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
o	o	k7c4
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
proti	proti	k7c3
argentinském	argentinský	k2eAgInSc6d1
klubu	klub	k1gInSc6
CA	ca	kA
Vélez	Vélez	k1gMnSc1
Sarsfield	Sarsfield	k1gMnSc1
a	a	k8xC
prohrává	prohrávat	k5eAaImIp3nS
s	s	k7c7
nimi	on	k3xPp3gInPc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
oslavují	oslavovat	k5eAaImIp3nP
v	v	k7c6
Evropském	evropský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
poráží	porážet	k5eAaImIp3nS
ve	v	k7c6
druhém	druhý	k4xOgInSc6
utkání	utkání	k1gNnSc6
Arsenal	Arsenal	k1gFnSc2
FC	FC	kA
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
klub	klub	k1gInSc1
dosáhne	dosáhnout	k5eAaPmIp3nS
pátého	pátý	k4xOgNnSc2
finále	finále	k1gNnSc2
za	za	k7c2
posledních	poslední	k2eAgInPc2d1
sedmi	sedm	k4xCc2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
třetí	třetí	k4xOgFnPc4
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stadionu	stadion	k1gInSc6
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
klub	klub	k1gInSc1
prohrává	prohrávat	k5eAaImIp3nS
díky	díky	k7c3
brance	branka	k1gFnSc3
Kluiverta	Kluiverta	k1gFnSc1
v	v	k7c6
85	#num#	k4
minutě	minuta	k1gFnSc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
sezonou	sezona	k1gFnSc7
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
je	být	k5eAaImIp3nS
klub	klub	k1gInSc1
posílen	posílen	k2eAgInSc1d1
o	o	k7c4
dva	dva	k4xCgMnPc4
špičkové	špičkový	k2eAgMnPc4d1
útočníky	útočník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přichází	přicházet	k5eAaImIp3nS
Roberto	Roberta	k1gFnSc5
Baggio	Baggio	k1gNnSc1
(	(	kIx(
<g/>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
a	a	k8xC
George	Georg	k1gMnSc2
Weah	Weaha	k1gFnPc2
(	(	kIx(
<g/>
PSG	PSG	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1995	#num#	k4
získává	získávat	k5eAaImIp3nS
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získávají	získávat	k5eAaImIp3nP
již	již	k9
15	#num#	k4
titul	titul	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
titul	titul	k1gInSc1
za	za	k7c4
5	#num#	k4
sezon	sezona	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrají	hrát	k5eAaImIp3nP
o	o	k7c4
Pohár	pohár	k1gInSc4
UEFA	UEFA	kA
a	a	k8xC
dokráčejí	dokráčet	k5eAaPmIp3nP
až	až	k9
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
kde	kde	k6eAd1
podlehnou	podlehnout	k5eAaPmIp3nP
Bordeaux	Bordeaux	k1gNnSc4
ze	z	k7c2
Zidanem	Zidan	k1gInSc7
v	v	k7c6
sestavě	sestava	k1gFnSc6
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
sezony	sezona	k1gFnSc2
se	se	k3xPyFc4
Capello	Capello	k1gNnSc1
rozhodl	rozhodnout	k5eAaPmAgInS
odejít	odejít	k5eAaPmF
do	do	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
a	a	k8xC
končí	končit	k5eAaImIp3nS
tak	tak	k6eAd1
nejúspěšnější	úspěšný	k2eAgMnSc1d3
trenér	trenér	k1gMnSc1
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
a	a	k8xC
též	též	k9
končí	končit	k5eAaImIp3nS
nejúspěšnější	úspěšný	k2eAgNnSc1d3
období	období	k1gNnSc1
klubu	klub	k1gInSc2
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
trvalo	trvat	k5eAaImAgNnS
devět	devět	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
-	-	kIx~
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Novým	nový	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
pro	pro	k7c4
sezonu	sezona	k1gFnSc4
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
uruguayský	uruguayský	k2eAgInSc1d1
Óscar	Óscar	k1gInSc1
Tabárez	Tabáreza	k1gFnPc2
(	(	kIx(
<g/>
naposled	naposled	k6eAd1
Cagliari	Cagliari	k1gNnSc1
Calcio	Calcio	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
klubu	klub	k1gInSc2
odchází	odcházet	k5eAaImIp3nS
talent	talent	k1gInSc1
Patrick	Patrick	k1gMnSc1
Vieira	Vieira	k1gMnSc1
(	(	kIx(
<g/>
Arsenal	Arsenal	k1gMnSc1
FC	FC	kA
<g/>
)	)	kIx)
a	a	k8xC
Roberto	Roberta	k1gFnSc5
Donadoni	Donadoň	k1gFnSc3
(	(	kIx(
<g/>
Metrostars	Metrostars	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
povedené	povedený	k2eAgInPc4d1
příchody	příchod	k1gInPc4
měli	mít	k5eAaImAgMnP
bít	bít	k5eAaImF
i	i	k9
nizozemské	nizozemský	k2eAgInPc1d1
příchody	příchod	k1gInPc1
z	z	k7c2
klubu	klub	k1gInSc2
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
<g/>
:	:	kIx,
Edgar	Edgar	k1gMnSc1
Davids	Davids	k1gInSc1
a	a	k8xC
Michael	Michael	k1gMnSc1
Reiziger	Reiziger	k1gMnSc1
na	na	k7c6
základě	základ	k1gInSc6
Bosmanově	Bosmanův	k2eAgInSc6d1
pravidlu	pravidlo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
prohrává	prohrávat	k5eAaImIp3nS
klub	klub	k1gInSc1
s	s	k7c7
Fiorentinou	Fiorentina	k1gFnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
v	v	k7c6
italském	italský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jedenáctém	jedenáctý	k4xOgInSc6
kole	kolo	k1gNnSc6
po	po	k7c6
porážce	porážka	k1gFnSc6
s	s	k7c7
Piacenzou	Piacenza	k1gFnSc7
je	být	k5eAaImIp3nS
odvolán	odvolat	k5eAaPmNgMnS
trenér	trenér	k1gMnSc1
Óscar	Óscar	k1gMnSc1
Tabárez	Tabárez	k1gMnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
nahrazen	nahradit	k5eAaPmNgInS
Sacchim	Sacchim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
dní	den	k1gInPc2
po	po	k7c6
návratu	návrat	k1gInSc6
na	na	k7c4
lavičku	lavička	k1gFnSc4
Rossoneri	Rossoner	k1gFnSc2
byl	být	k5eAaImAgInS
klub	klub	k1gInSc1
vyřazen	vyřadit	k5eAaPmNgInS
z	z	k7c2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
ve	v	k7c6
skupinové	skupinový	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
kvůli	kvůli	k7c3
porážce	porážka	k1gFnSc3
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
v	v	k7c6
rozhodující	rozhodující	k2eAgFnSc6d1
domácím	domácí	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
proti	proti	k7c3
norskému	norský	k2eAgInSc3d1
klubu	klub	k1gInSc3
Rosenborg	Rosenborg	k1gInSc4
BK	BK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
končí	končit	k5eAaImIp3nS
na	na	k7c4
11	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
(	(	kIx(
<g/>
nejhorší	zlý	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
od	od	k7c2
sezony	sezona	k1gFnSc2
Serie	serie	k1gFnSc2
A	a	k9
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
|	|	kIx~
<g/>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
sezóny	sezóna	k1gFnSc2
opouštějí	opouštět	k5eAaImIp3nP
klub	klub	k1gInSc4
ikony	ikona	k1gFnSc2
Rossoneri	Rossoner	k1gFnSc2
<g/>
,	,	kIx,
kapitán	kapitán	k1gMnSc1
Franco	Franco	k6eAd1
Baresi	Barese	k1gFnSc4
a	a	k8xC
Mauro	Mauro	k1gNnSc4
Tassotti	Tassotť	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
po	po	k7c6
20	#num#	k4
a	a	k8xC
17	#num#	k4
letech	let	k1gInPc6
nepřetržité	přetržitý	k2eNgFnSc2d1
kariéry	kariéra	k1gFnSc2
v	v	k7c6
dresu	dres	k1gInSc6
Rossoneri	Rossoner	k1gFnSc2
rozloučili	rozloučit	k5eAaPmAgMnP
s	s	k7c7
fotbalem	fotbal	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
italského	italský	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
na	na	k7c4
počest	počest	k1gFnSc4
kapitána	kapitán	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
strávil	strávit	k5eAaPmAgInS
celou	celý	k2eAgFnSc4d1
svou	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
jako	jako	k8xC,k8xS
Rossoneri	Rossoneri	k1gNnSc4
<g/>
,	,	kIx,
vyřazeno	vyřazen	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Místo	místo	k7c2
trenéra	trenér	k1gMnSc2
Sacchiho	Sacchi	k1gMnSc2
je	být	k5eAaImIp3nS
povolán	povolat	k5eAaPmNgMnS
další	další	k2eAgMnSc1d1
navrátilec	navrátilec	k1gMnSc1
z	z	k7c2
vítězné	vítězný	k2eAgFnSc2d1
éry	éra	k1gFnSc2
Fabio	Fabio	k6eAd1
Capello	Capello	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
i	i	k9
příchod	příchod	k1gInSc4
talentu	talent	k1gInSc2
z	z	k7c2
klubu	klub	k1gInSc2
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
Kluiverta	Kluiverta	k1gFnSc1
se	se	k3xPyFc4
nedaří	dařit	k5eNaImIp3nS
navázat	navázat	k5eAaPmF
na	na	k7c4
úspěšné	úspěšný	k2eAgInPc4d1
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
končí	končit	k5eAaImIp3nS
na	na	k7c4
10	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
a	a	k8xC
jediné	jediný	k2eAgInPc1d1
světlé	světlý	k2eAgInPc1d1
okamžiky	okamžik	k1gInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gNnSc6
postoupí	postoupit	k5eAaPmIp3nP
do	do	k7c2
finále	finále	k1gNnSc2
kde	kde	k6eAd1
prohrává	prohrávat	k5eAaImIp3nS
po	po	k7c6
výsledcích	výsledek	k1gInPc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
s	s	k7c7
Laziem	Lazium	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sezoně	sezona	k1gFnSc6
Fabio	Fabio	k6eAd1
Capello	Capello	k1gNnSc1
končí	končit	k5eAaImIp3nS
na	na	k7c6
lavičce	lavička	k1gFnSc6
Rossoneri	Rossoner	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Novým	nový	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
pro	pro	k7c4
sezonu	sezona	k1gFnSc4
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
je	být	k5eAaImIp3nS
vybrán	vybrat	k5eAaPmNgMnS
Alberto	Alberta	k1gFnSc5
Zaccheroni	Zaccheron	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
sním	sníst	k5eAaPmIp1nS,k5eAaImIp1nS
z	z	k7c2
klubu	klub	k1gInSc2
Udinese	Udinese	k1gFnSc2
přicházejí	přicházet	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
Oliver	Oliver	k1gMnSc1
Bierhoff	Bierhoff	k1gMnSc1
a	a	k8xC
Thomas	Thomas	k1gMnSc1
Helveg	Helveg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	Nový	k1gMnSc1
trenér	trenér	k1gMnSc1
<g/>
,	,	kIx,
pevný	pevný	k2eAgMnSc1d1
zastánce	zastánce	k1gMnSc1
modelu	model	k1gInSc2
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
senzačně	senzačně	k6eAd1
vyhraje	vyhrát	k5eAaPmIp3nS
titul	titul	k1gInSc4
<g/>
,	,	kIx,
šestnáctého	šestnáctý	k4xOgNnSc2
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
navíc	navíc	k6eAd1
ke	k	k7c3
stému	stý	k4xOgNnSc3
výročí	výročí	k1gNnSc3
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
díky	díky	k7c3
sedmi	sedm	k4xCc3
vítězství	vítězství	k1gNnPc2
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
sedmi	sedm	k4xCc2
zápasů	zápas	k1gInPc2
a	a	k8xC
pár	pár	k4xCyI
proher	prohra	k1gFnPc2
klubu	klub	k1gInSc2
SS	SS	kA
Lazio	Lazio	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
květnu	květen	k1gInSc6
1999	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
dohodl	dohodnout	k5eAaPmAgInS
s	s	k7c7
Dynamem	dynamo	k1gNnSc7
Kyjev	Kyjev	k1gInSc1
o	o	k7c6
přestupu	přestup	k1gInSc6
mladého	mladý	k2eAgMnSc2d1
ukrajinského	ukrajinský	k2eAgMnSc2d1
útočníka	útočník	k1gMnSc2
Ševčenka	Ševčenka	k1gFnSc1
za	za	k7c4
23	#num#	k4
000	#num#	k4
000	#num#	k4
Euro	euro	k1gNnSc4
<g/>
,	,	kIx,
nejlepšího	dobrý	k2eAgMnSc2d3
střelce	střelec	k1gMnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezona	sezona	k1gFnSc1
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
začala	začít	k5eAaPmAgFnS
porážkou	porážka	k1gFnSc7
v	v	k7c6
italském	italský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
s	s	k7c7
Parmou	Parma	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
se	se	k3xPyFc4
klub	klub	k1gInSc1
umístil	umístit	k5eAaPmAgInS
na	na	k7c6
dně	dno	k1gNnSc6
své	svůj	k3xOyFgFnSc2
skupiny	skupina	k1gFnSc2
když	když	k8xS
jen	jen	k9
jednou	jeden	k4xCgFnSc7
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozhodujícím	rozhodující	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
proti	proti	k7c3
Galatasaray	Galatasaraum	k1gNnPc7
SK	Sk	kA
<g/>
,	,	kIx,
vede	vést	k5eAaImIp3nS
2	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
mohl	moct	k5eAaImAgMnS
postoupit	postoupit	k5eAaPmF
do	do	k7c2
dalšího	další	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
<g/>
,	,	kIx,
jenže	jenže	k8xC
v	v	k7c6
87	#num#	k4
minutě	minuta	k1gFnSc6
je	být	k5eAaImIp3nS
srovnáno	srovnat	k5eAaPmNgNnS
a	a	k8xC
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
alespoň	alespoň	k9
zaručena	zaručen	k2eAgFnSc1d1
účast	účast	k1gFnSc1
v	v	k7c6
Poháru	pohár	k1gInSc6
UEFA	UEFA	kA
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
90	#num#	k4
minutě	minuta	k1gFnSc6
dává	dávat	k5eAaImIp3nS
branku	branka	k1gFnSc4
Ümit	Ümita	k1gFnPc2
Davala	Davala	k1gFnSc1
a	a	k8xC
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
nakonec	nakonec	k6eAd1
vítězí	vítězit	k5eAaImIp3nS
a	a	k8xC
postoupí	postoupit	k5eAaPmIp3nS
do	do	k7c2
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
ji	on	k3xPp3gFnSc4
i	i	k9
celou	celá	k1gFnSc4
vyhraje	vyhrát	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
Ševčenka	Ševčenka	k1gFnSc1
přichází	přicházet	k5eAaImIp3nS
do	do	k7c2
klubu	klub	k1gInSc2
i	i	k9
další	další	k2eAgFnSc1d1
budoucí	budoucí	k2eAgFnSc1d1
ikona	ikona	k1gFnSc1
Gennaro	Gennara	k1gFnSc5
Gattuso	Gattusa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezona	sezona	k1gFnSc1
se	se	k3xPyFc4
zakončí	zakončit	k5eAaPmIp3nS
na	na	k7c6
třetím	třetí	k4xOgNnSc6
místě	místo	k1gNnSc6
s	s	k7c7
11	#num#	k4
bodovou	bodový	k2eAgFnSc7d1
ztrátou	ztráta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
pěti	pět	k4xCc6
sezonách	sezona	k1gFnPc6
už	už	k6eAd1
neoblékne	obléknout	k5eNaPmIp3nS
dres	dres	k1gInSc4
Rossoneri	Rossoner	k1gFnSc2
George	George	k1gFnPc2
Weah	Weah	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
před	před	k7c7
sezonou	sezona	k1gFnSc7
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
odejít	odejít	k5eAaPmF
do	do	k7c2
Manchester	Manchester	k1gInSc1
City	city	k1gNnSc4
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ligu	liga	k1gFnSc4
mistrů	mistr	k1gMnPc2
vyhraje	vyhrát	k5eAaPmIp3nS
svoji	svůj	k3xOyFgFnSc4
skupinu	skupina	k1gFnSc4
ale	ale	k8xC
v	v	k7c6
osmi	osm	k4xCc6
finálové	finálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
skončí	skončit	k5eAaPmIp3nS
na	na	k7c6
nepostupové	postupový	k2eNgFnSc6d1
třetí	třetí	k4xOgFnSc6
příčce	příčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
neuspěchá	uspěchat	k5eNaPmIp3nS
je	on	k3xPp3gNnSc4
odvolán	odvolán	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Alberto	Alberta	k1gFnSc5
Zaccheroni	Zaccheroň	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
nahrazen	nahradit	k5eAaPmNgInS
Cesarem	Cesar	k1gInSc7
Maldinim	Maldinimo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
i	i	k9
tak	tak	k9
na	na	k7c4
lepší	dobrý	k2eAgFnPc4d2
než	než	k8xS
šesté	šestý	k4xOgNnSc1
místo	místo	k1gNnSc1
v	v	k7c6
lize	liga	k1gFnSc6
to	ten	k3xDgNnSc1
nabylo	nabýt	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rossoneri	Rossoneri	k1gNnPc7
vyhráli	vyhrát	k5eAaPmAgMnP
v	v	k7c6
derby	derby	k1gNnSc6
proti	proti	k7c3
Interu	Inter	k1gInSc3
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
(	(	kIx(
<g/>
nejvíc	hodně	k6eAd3,k6eAd1
od	od	k7c2
sezony	sezona	k1gFnSc2
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
sezonu	sezona	k1gFnSc4
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
jsou	být	k5eAaImIp3nP
koupeni	koupen	k2eAgMnPc1d1
špičkový	špičkový	k2eAgMnSc1d1
hráči	hráč	k1gMnPc1
<g/>
:	:	kIx,
Inzaghi	Inzagh	k1gMnPc1
(	(	kIx(
<g/>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pirlo	Pirlo	k1gNnSc1
(	(	kIx(
<g/>
Inter	Inter	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Rui	Rui	k1gMnSc1
Costa	Costa	k1gMnSc1
s	s	k7c7
trenérem	trenér	k1gMnSc7
Terimem	Terim	k1gMnSc7
(	(	kIx(
<g/>
Fiorentina	Fiorentina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
po	po	k7c6
desátém	desátý	k4xOgNnSc6
kole	kolo	k1gNnSc6
je	být	k5eAaImIp3nS
odvolán	odvolán	k2eAgMnSc1d1
a	a	k8xC
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
je	být	k5eAaImIp3nS
přiveden	přiveden	k2eAgInSc1d1
bývalí	bývalý	k2eAgMnPc1d1
hráč	hráč	k1gMnSc1
klubu	klub	k1gInSc3
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
Rossoneri	Rossonere	k1gFnSc4
dovede	dovést	k5eAaPmIp3nS
do	do	k7c2
semifinále	semifinále	k1gNnSc2
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
kde	kde	k6eAd1
nestačí	stačit	k5eNaBmIp3nS
na	na	k7c4
Dortmund	Dortmund	k1gInSc4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
končí	končit	k5eAaImIp3nS
na	na	k7c6
čtvrtém	čtvrtý	k4xOgNnSc6
místě	místo	k1gNnSc6
s	s	k7c7
odstupem	odstup	k1gInSc7
16	#num#	k4
bodů	bod	k1gInPc2
nad	nad	k7c7
vítězným	vítězný	k2eAgInSc7d1
klubem	klub	k1gInSc7
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1
éra	éra	k1gFnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezona	sezona	k1gFnSc1
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
návrat	návrat	k1gInSc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
koupí	koupit	k5eAaPmIp3nS
skvělého	skvělý	k2eAgMnSc4d1
obránce	obránce	k1gMnSc4
Nestu	Nesta	k1gMnSc4
(	(	kIx(
<g/>
SS	SS	kA
Lazio	Lazio	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
záložníka	záložník	k1gMnSc4
Seedorfa	Seedorf	k1gMnSc4
(	(	kIx(
<g/>
Inter	Inter	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
klubu	klub	k1gInSc2
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
přichází	přicházet	k5eAaImIp3nS
též	též	k9
útočník	útočník	k1gMnSc1
Rivaldo	Rivalda	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
se	se	k3xPyFc4
po	po	k7c6
skvělém	skvělý	k2eAgInSc6d1
startu	start	k1gInSc6
ocitá	ocitat	k5eAaImIp3nS
na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
jenže	jenže	k8xC
v	v	k7c6
jarních	jarní	k2eAgInPc6d1
zápasech	zápas	k1gInPc6
ztrácí	ztrácet	k5eAaImIp3nS
a	a	k8xC
nakonec	nakonec	k6eAd1
skončí	skončit	k5eAaPmIp3nS
na	na	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
klub	klub	k1gInSc1
táhne	táhnout	k5eAaImIp3nS
skvělí	skvělý	k2eAgMnPc1d1
útočníci	útočník	k1gMnPc1
Inzaghi	Inzagh	k1gFnSc2
a	a	k8xC
Ševčenko	Ševčenka	k1gFnSc5
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
semifinále	semifinále	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
setkávají	setkávat	k5eAaImIp3nP
s	s	k7c7
Interem	Inter	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgNnSc1
derby	derby	k1gNnSc1
v	v	k7c6
historii	historie	k1gFnSc6
v	v	k7c6
Evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
výsledcích	výsledek	k1gInPc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
Rossoneri	Rossoneri	k1gNnPc2
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
finále	finále	k1gNnSc2
kde	kde	k6eAd1
se	se	k3xPyFc4
střetávají	střetávat	k5eAaImIp3nP
ze	z	k7c2
sokem	sok	k1gMnSc7
z	z	k7c2
ligy	liga	k1gFnSc2
Juventusem	Juventus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
penaltovém	penaltový	k2eAgInSc6d1
rozstřelu	rozstřel	k1gInSc6
vyhrává	vyhrávat	k5eAaImIp3nS
již	již	k6eAd1
šestý	šestý	k4xOgInSc4
titul	titul	k1gInSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
26	#num#	k4
letech	léto	k1gNnPc6
klub	klub	k1gInSc1
vítězí	vítězit	k5eAaImIp3nS
i	i	k9
v	v	k7c6
italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
poráží	porážet	k5eAaImIp3nS
AS	as	k9
Řím	Řím	k1gInSc1
po	po	k7c6
výsledcích	výsledek	k1gInPc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Klub	klub	k1gInSc1
prohrává	prohrávat	k5eAaImIp3nS
v	v	k7c6
italském	italský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
jej	on	k3xPp3gMnSc4
poráží	porážet	k5eAaImIp3nS
Juventus	Juventus	k1gInSc1
FC	FC	kA
po	po	k7c6
penaltovém	penaltový	k2eAgInSc6d1
roztřelu	roztřel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropském	evropský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
poráží	porážet	k5eAaImIp3nS
FC	FC	kA
Porto	porto	k1gNnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
získává	získávat	k5eAaImIp3nS
trofej	trofej	k1gFnSc4
již	již	k9
počtvrté	počtvrté	k4xO
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
Tokio	Tokio	k1gNnSc1
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
o	o	k7c4
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
proti	proti	k7c3
argentinském	argentinský	k2eAgInSc6d1
klubu	klub	k1gInSc6
CA	ca	kA
Boca	Boca	k1gMnSc1
Juniors	Juniors	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utkání	utkání	k1gNnSc1
končí	končit	k5eAaImIp3nS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
nakonec	nakonec	k6eAd1
Rossoneri	Rossoner	k1gMnPc1
prohrávají	prohrávat	k5eAaImIp3nP
zase	zase	k9
na	na	k7c4
penalty	penalta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
Ševčenkovi	Ševčenkův	k2eAgMnPc1d1
nejlepšímu	dobrý	k2eAgMnSc3d3
střelci	střelec	k1gMnPc7
sezony	sezona	k1gFnSc2
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
s	s	k7c7
24	#num#	k4
brankami	branka	k1gFnPc7
klub	klub	k1gInSc4
získává	získávat	k5eAaImIp3nS
titul	titul	k1gInSc1
číslo	číslo	k1gNnSc1
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
sezonu	sezona	k1gFnSc4
v	v	k7c6
dresu	dres	k1gInSc6
Rossoneri	Rossoner	k1gFnSc2
hraje	hrát	k5eAaImIp3nS
budoucí	budoucí	k2eAgMnSc1d1
držitel	držitel	k1gMnSc1
Zlatého	zlatý	k2eAgInSc2d1
míče	míč	k1gInSc2
Kaká	kakat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
obhájce	obhájce	k1gMnSc1
trofeje	trofej	k1gFnSc2
se	se	k3xPyFc4
zúčastní	zúčastnit	k5eAaPmIp3nS
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
a	a	k8xC
dostává	dostávat	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Než	než	k8xS
začala	začít	k5eAaPmAgFnS
sezona	sezona	k1gFnSc1
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
klub	klub	k1gInSc1
poráží	porážet	k5eAaImIp3nS
SS	SS	kA
Lazio	Lazio	k6eAd1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
v	v	k7c6
italském	italský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
(	(	kIx(
<g/>
prvně	prvně	k?
20	#num#	k4
klubů	klub	k1gInPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
umísťuje	umísťovat	k5eAaImIp3nS
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
ze	z	k7c2
ztrátou	ztráta	k1gFnSc7
sedmi	sedm	k4xCc2
bodů	bod	k1gInPc2
na	na	k7c4
Juventus	Juventus	k1gInSc4
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titul	titul	k1gInSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
zruší	zrušit	k5eAaPmIp3nS
po	po	k7c6
skandálu	skandál	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
dosáhl	dosáhnout	k5eAaPmAgInS
klub	klub	k1gInSc1
opět	opět	k6eAd1
finále	finále	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
opět	opět	k6eAd1
vyřadil	vyřadit	k5eAaPmAgMnS
rivala	rival	k1gMnSc2
Inter	Inter	k1gMnSc1
po	po	k7c6
výsledcích	výsledek	k1gInPc6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
kontumačně	kontumačně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finále	finále	k1gNnPc7
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
na	na	k7c6
stadionu	stadion	k1gInSc6
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
45	#num#	k4
minutách	minuta	k1gFnPc6
hry	hra	k1gFnSc2
byl	být	k5eAaImAgInS
stav	stav	k1gInSc1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
por	por	k?
Rossoneri	Rossoner	k1gFnSc2
<g/>
,	,	kIx,
jenže	jenže	k8xC
na	na	k7c4
druhý	druhý	k4xOgInSc4
poločas	poločas	k1gInSc4
se	se	k3xPyFc4
klub	klub	k1gInSc4
z	z	k7c2
Liverpoolu	Liverpool	k1gInSc2
zabral	zabrat	k5eAaPmAgMnS
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
během	během	k7c2
šesti	šest	k4xCc2
minut	minuta	k1gFnPc2
tři	tři	k4xCgFnPc4
branky	branka	k1gFnPc4
a	a	k8xC
srovnal	srovnat	k5eAaPmAgMnS
stav	stav	k1gInSc4
na	na	k7c4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rossoneri	Rossoneri	k1gNnSc7
penalty	penalta	k1gFnSc2
nezvládli	zvládnout	k5eNaPmAgMnP
a	a	k8xC
prohráli	prohrát	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andrij	Andrij	k1gFnSc1
Ševčenko	Ševčenka	k1gFnSc5
získal	získat	k5eAaPmAgMnS
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
pro	pro	k7c4
rok	rok	k1gInSc4
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
sezoně	sezona	k1gFnSc6
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
vypukl	vypuknout	k5eAaPmAgMnS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
fotbalové	fotbalový	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
korupční	korupční	k2eAgInSc1d1
skandál	skandál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
končí	končit	k5eAaImIp3nS
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
o	o	k7c4
tři	tři	k4xCgInPc4
body	bod	k1gInPc4
méně	málo	k6eAd2
než	než	k8xS
Juventus	Juventus	k1gInSc4
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
po	po	k7c6
soudu	soud	k1gInSc6
se	se	k3xPyFc4
rozhodlo	rozhodnout	k5eAaPmAgNnS
že	že	k8xS
se	se	k3xPyFc4
klubu	klub	k1gInSc2
odečte	odečíst	k5eAaPmIp3nS
30	#num#	k4
bodů	bod	k1gInPc2
a	a	k8xC
končí	končit	k5eAaImIp3nS
tak	tak	k6eAd1
na	na	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
jej	on	k3xPp3gMnSc4
vyřadí	vyřadit	k5eAaPmIp3nP
v	v	k7c6
semifinále	semifinále	k1gNnSc6
budoucí	budoucí	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezona	sezona	k1gFnSc1
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
začíná	začínat	k5eAaImIp3nS
odečtem	odečet	k1gInSc7
osmi	osm	k4xCc2
bodů	bod	k1gInPc2
za	za	k7c4
korupční	korupční	k2eAgInSc4d1
skandál	skandál	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
a	a	k8xC
také	také	k9
bez	bez	k7c2
fotbalisty	fotbalista	k1gMnSc2
Ševčenka	Ševčenka	k1gFnSc1
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
prodán	prodat	k5eAaPmNgInS
do	do	k7c2
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ligu	liga	k1gFnSc4
ukončí	ukončit	k5eAaPmIp3nS
na	na	k7c6
čtvrtém	čtvrtý	k4xOgInSc6
místě	místo	k1gNnSc6
když	když	k8xS
na	na	k7c6
vítězným	vítězný	k2eAgInSc7d1
Interem	Inter	k1gInSc7
ztrácí	ztrácet	k5eAaImIp3nS
propastných	propastný	k2eAgInPc2d1
36	#num#	k4
bodů	bod	k1gInPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Ligu	liga	k1gFnSc4
mistrů	mistr	k1gMnPc2
musí	muset	k5eAaImIp3nS
hrát	hrát	k5eAaImF
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
předkola	předkolo	k1gNnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
zvládá	zvládat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupuje	postupovat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
Rossoneri	Rossoneri	k1gNnSc4
jedenácté	jedenáctý	k4xOgFnSc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
historii	historie	k1gFnSc6
a	a	k8xC
třetí	třetí	k4xOgNnSc1
za	za	k7c2
posledních	poslední	k2eAgNnPc2d1
pěti	pět	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
na	na	k7c6
stadionu	stadion	k1gInSc6
v	v	k7c6
Aténách	Atény	k1gFnPc6
se	se	k3xPyFc4
utkají	utkat	k5eAaPmIp3nP
opět	opět	k6eAd1
s	s	k7c7
Liverpoolem	Liverpool	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
je	on	k3xPp3gMnPc4
poráží	porážet	k5eAaImIp3nS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
získává	získávat	k5eAaImIp3nS
tak	tak	k6eAd1
již	již	k9
sedmou	sedma	k1gFnSc7
tuhle	tuhle	k6eAd1
trofej	trofej	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
vítěz	vítěz	k1gMnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
hraje	hrát	k5eAaImIp3nS
o	o	k7c4
Evropský	evropský	k2eAgInSc4d1
superpohár	superpohár	k1gInSc4
s	s	k7c7
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
klub	klub	k1gInSc1
poráží	porážet	k5eAaImIp3nS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c4
MS	MS	kA
klubů	klub	k1gInPc2
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
se	se	k3xPyFc4
Kaká	kakat	k5eAaImIp3nS
stává	stávat	k5eAaImIp3nS
šestým	šestý	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
Rossoneri	Rossoneri	k1gNnSc4
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
získá	získat	k5eAaPmIp3nS
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
získává	získávat	k5eAaImIp3nS
i	i	k9
ocenění	ocenění	k1gNnSc4
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
za	za	k7c4
rok	rok	k1gInSc4
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
MS	MS	kA
klubů	klub	k1gInPc2
postoupí	postoupit	k5eAaPmIp3nP
do	do	k7c2
finále	finále	k1gNnSc2
kde	kde	k6eAd1
narazí	narazit	k5eAaPmIp3nS
na	na	k7c4
argentinský	argentinský	k2eAgInSc4d1
klub	klub	k1gInSc4
CA	ca	kA
Boca	Boc	k1gInSc2
Juniors	Juniorsa	k1gFnPc2
a	a	k8xC
poráží	porážet	k5eAaImIp3nS
jej	on	k3xPp3gNnSc4
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
je	být	k5eAaImIp3nS
vyřazen	vyřadit	k5eAaPmNgInS
v	v	k7c6
osmi	osm	k4xCc6
finále	finále	k1gNnSc6
klubem	klub	k1gInSc7
Arsenal	Arsenal	k1gMnSc2
FC	FC	kA
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
klub	klub	k1gInSc1
končí	končit	k5eAaImIp3nS
na	na	k7c6
pátém	pátý	k4xOgNnSc6
místě	místo	k1gNnSc6
zaručující	zaručující	k2eAgInSc1d1
postup	postup	k1gInSc1
do	do	k7c2
poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poháru	pohár	k1gInSc6
UEFA	UEFA	kA
je	být	k5eAaImIp3nS
vyřazen	vyřadit	k5eAaPmNgMnS
jíž	jenž	k3xRgFnSc3
v	v	k7c6
šestnácti	šestnáct	k4xCc6
finále	finále	k1gNnPc6
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
<g/>
)	)	kIx)
německým	německý	k2eAgInSc7d1
klubem	klub	k1gInSc7
Werder	Werdra	k1gFnPc2
Brémy	Brémy	k1gFnPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
nakonec	nakonec	k6eAd1
postoupil	postoupit	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
klubu	klub	k1gInSc2
přichází	přicházet	k5eAaImIp3nS
brazilský	brazilský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
Ronaldinho	Ronaldin	k1gMnSc2
z	z	k7c2
klubu	klub	k1gInSc2
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
a	a	k8xC
od	od	k7c2
ledna	leden	k1gInSc2
i	i	k8xC
David	David	k1gMnSc1
Beckham	Beckham	k1gInSc4
z	z	k7c2
klubu	klub	k1gInSc2
Los	los	k1gInSc4
Angeles	Angeles	k1gInSc1
Galaxy	Galax	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
díky	díky	k7c3
nim	on	k3xPp3gFnPc3
se	se	k3xPyFc4
klub	klub	k1gInSc1
v	v	k7c6
lize	liga	k1gFnSc6
umístí	umístit	k5eAaPmIp3nS
na	na	k7c6
třetí	třetí	k4xOgFnSc6
příčce	příčka	k1gFnSc6
zaručující	zaručující	k2eAgFnSc4d1
účast	účast	k1gFnSc4
opět	opět	k6eAd1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sezoně	sezona	k1gFnSc6
končí	končit	k5eAaImIp3nS
trenér	trenér	k1gMnSc1
Carlo	Carlo	k1gNnSc4
Ancelotti	Ancelott	k2eAgMnPc1d1
a	a	k8xC
kapitán	kapitán	k1gMnSc1
Paolo	Paolo	k1gNnSc4
Maldini	Maldin	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedení	vedení	k1gNnSc1
číslo	číslo	k1gNnSc1
3	#num#	k4
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
nosil	nosit	k5eAaImAgMnS
kapitán	kapitán	k1gMnSc1
vyřadí	vyřadit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
novou	nový	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
se	se	k3xPyFc4
vedení	vedení	k1gNnSc2
rozhodne	rozhodnout	k5eAaPmIp3nS
angažovat	angažovat	k5eAaBmF
na	na	k7c4
trenérskou	trenérský	k2eAgFnSc4d1
židli	židle	k1gFnSc4
bývalého	bývalý	k2eAgMnSc2d1
hráče	hráč	k1gMnSc2
nezkušeného	zkušený	k2eNgMnSc2d1
Leonarda	Leonardo	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
několik	několik	k4yIc4
dní	den	k1gInPc2
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
oznámen	oznámen	k2eAgInSc1d1
odchod	odchod	k1gInSc1
fotbalisty	fotbalista	k1gMnSc2
Kaká	kakat	k5eAaImIp3nS
do	do	k7c2
Realu	Real	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodej	prodej	k1gInSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
za	za	k7c7
účelem	účel	k1gInSc7
vyvážení	vyvážení	k1gNnSc1
rovnováhy	rovnováha	k1gFnSc2
klubu	klub	k1gInSc2
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
vyvolává	vyvolávat	k5eAaImIp3nS
široký	široký	k2eAgInSc4d1
nesouhlas	nesouhlas	k1gInSc4
mezi	mezi	k7c7
fanoušky	fanoušek	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedení	vedení	k1gNnSc1
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
neinvestovat	investovat	k5eNaBmF
částku	částka	k1gFnSc4
do	do	k7c2
příchodu	příchod	k1gInSc2
nových	nový	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
a	a	k8xC
přiřadí	přiřadit	k5eAaPmIp3nS
jej	on	k3xPp3gMnSc4
k	k	k7c3
rozpočtu	rozpočet	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jarní	jarní	k2eAgFnSc4d1
část	část	k1gFnSc4
opět	opět	k6eAd1
dorazí	dorazit	k5eAaPmIp3nS
David	David	k1gMnSc1
Beckham	Beckham	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
klub	klub	k1gInSc1
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
když	když	k8xS
ztratil	ztratit	k5eAaPmAgMnS
na	na	k7c4
vítěze	vítěz	k1gMnSc4
12	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
je	být	k5eAaImIp3nS
vyřazen	vyřadit	k5eAaPmNgInS
i	i	k9
Beckhamem	Beckham	k1gInSc7
na	na	k7c6
hřišti	hřiště	k1gNnSc6
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
v	v	k7c6
osmi	osm	k4xCc6
finále	finále	k1gNnPc6
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Místo	místo	k7c2
odvolaného	odvolaný	k1gMnSc2
trenéra	trenér	k1gMnSc2
Leonarda	Leonardo	k1gMnSc2
se	se	k3xPyFc4
najímá	najímat	k5eAaImIp3nS
Massimiliano	Massimiliana	k1gFnSc5
Allegri	Allegri	k1gNnPc3
(	(	kIx(
<g/>
Cagliari	Cagliar	k1gMnSc3
Calcio	Calcio	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
klubu	klub	k1gInSc2
přicházejí	přicházet	k5eAaImIp3nP
hvězdy	hvězda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
přichází	přicházet	k5eAaImIp3nS
na	na	k7c4
roční	roční	k2eAgNnSc4d1
hostování	hostování	k1gNnSc4
Zlatan	Zlatan	k1gInSc4
Ibrahimović	Ibrahimović	k1gFnSc7
a	a	k8xC
z	z	k7c2
Manchesteru	Manchester	k1gInSc2
City	city	k1gNnSc1
brazilský	brazilský	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
Robinho	Robin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zimní	zimní	k2eAgFnSc6d1
přestávce	přestávka	k1gFnSc6
odchází	odcházet	k5eAaImIp3nS
do	do	k7c2
rodné	rodný	k2eAgFnSc2d1
Brazílie	Brazílie	k1gFnSc2
Ronaldinho	Ronaldin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezona	sezona	k1gFnSc1
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
začíná	začínat	k5eAaImIp3nS
skvěle	skvěle	k6eAd1
a	a	k8xC
od	od	k7c2
listopadu	listopad	k1gInSc2
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
sezony	sezona	k1gFnSc2
vedou	vést	k5eAaImIp3nP
tabulku	tabulka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhrají	vyhrát	k5eAaPmIp3nP
již	již	k6eAd1
18	#num#	k4
titul	titul	k1gInSc4
šesti	šest	k4xCc6
bodovým	bodový	k2eAgInSc7d1
náskokem	náskok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
je	být	k5eAaImIp3nS
vyřazen	vyřadit	k5eAaPmNgInS
už	už	k6eAd1
v	v	k7c6
osmi	osm	k4xCc6
finále	finále	k1gNnPc6
klubem	klub	k1gInSc7
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspura	k1gFnPc2
FC	FC	kA
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xS,k8xC
obhájce	obhájce	k1gMnSc1
trofeje	trofej	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c4
sezonu	sezona	k1gFnSc4
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
klub	klub	k1gInSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
z	z	k7c2
hostování	hostování	k1gNnSc2
učinit	učinit	k5eAaImF,k5eAaPmF
přestup	přestup	k1gInSc4
na	na	k7c4
Ibrahimoviće	Ibrahimoviće	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Juventusu	Juventus	k1gInSc2
odchází	odcházet	k5eAaImIp3nS
po	po	k7c6
deseti	deset	k4xCc6
sezonách	sezona	k1gFnPc6
Andrea	Andrea	k1gFnSc1
Pirlo	Pirlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátek	začátek	k1gInSc4
sezony	sezona	k1gFnSc2
hrají	hrát	k5eAaImIp3nP
o	o	k7c4
italský	italský	k2eAgInSc4d1
superpohár	superpohár	k1gInSc4
s	s	k7c7
Interem	Inter	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poráží	porážet	k5eAaImIp3nS
jej	on	k3xPp3gInSc4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
získává	získávat	k5eAaImIp3nS
tak	tak	k6eAd1
po	po	k7c6
šesté	šestý	k4xOgFnSc6
tuhle	tuhle	k9
trofej	trofej	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
klub	klub	k1gInSc4
obsadí	obsadit	k5eAaPmIp3nS
druhé	druhý	k4xOgNnSc1
místo	místo	k1gNnSc1
za	za	k7c7
Juventusem	Juventus	k1gInSc7
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
body	bod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
je	být	k5eAaImIp3nS
vyřazen	vyřadit	k5eAaPmNgMnS
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
klubem	klub	k1gInSc7
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sezona	sezona	k1gFnSc1
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
je	být	k5eAaImIp3nS
pro	pro	k7c4
Rossoneri	Rossonere	k1gFnSc4
velice	velice	k6eAd1
těžká	těžkat	k5eAaImIp3nS
hlavně	hlavně	k9
pro	pro	k7c4
fanoušky	fanoušek	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
klubu	klub	k1gInSc2
odchází	odcházet	k5eAaImIp3nS
mnoho	mnoho	k4c1
hráčů	hráč	k1gMnPc2
<g/>
:	:	kIx,
Thiago	Thiago	k6eAd1
Silva	Silva	k1gFnSc1
a	a	k8xC
Ibrahimović	Ibrahimović	k1gFnSc1
(	(	kIx(
<g/>
PSG	PSG	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Cassano	Cassana	k1gFnSc5
(	(	kIx(
<g/>
Inter	Intero	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nesta	Nesta	k1gFnSc1
(	(	kIx(
<g/>
Montreal	Montreal	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gattuso	Gattusa	k1gFnSc5
(	(	kIx(
<g/>
FC	FC	kA
Sion	Sion	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Seedorf	Seedorf	k1gMnSc1
(	(	kIx(
<g/>
Botafogo	Botafogo	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
konec	konec	k1gInSc4
kariéry	kariéra	k1gFnSc2
ohlásili	ohlásit	k5eAaPmAgMnP
<g/>
:	:	kIx,
Zambrotta	Zambrotta	k1gFnSc1
s	s	k7c7
Inzaghi	Inzaghi	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
do	do	k7c2
klubu	klub	k1gInSc2
přichází	přicházet	k5eAaImIp3nS
Pazzini	Pazzin	k1gMnPc1
(	(	kIx(
<g/>
Inter	Inter	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Montolivo	Montolivo	k1gNnSc1
(	(	kIx(
<g/>
Fiorentina	Fiorentina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jarní	jarní	k2eAgFnSc4d1
část	část	k1gFnSc4
přichází	přicházet	k5eAaImIp3nS
Mario	Mario	k1gMnSc1
Balotelli	Balotelle	k1gFnSc4
(	(	kIx(
<g/>
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
<g/>
)	)	kIx)
za	za	k7c2
Pata	pata	k1gFnSc1
(	(	kIx(
<g/>
Corinthians	Corinthians	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
končí	končit	k5eAaImIp3nS
v	v	k7c6
lize	liga	k1gFnSc6
na	na	k7c6
skvělém	skvělý	k2eAgInSc6d1
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
a	a	k8xC
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
dokráčí	dokráčet	k5eAaPmIp3nP
do	do	k7c2
osmi	osm	k4xCc2
finále	finále	k1gNnPc2
kde	kde	k6eAd1
jej	on	k3xPp3gMnSc4
vyřadí	vyřadit	k5eAaPmIp3nS
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Těžké	těžký	k2eAgInPc1d1
časy	čas	k1gInPc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klub	klub	k1gInSc1
se	se	k3xPyFc4
díky	díky	k7c3
třetímu	třetí	k4xOgMnSc3
místu	místo	k1gNnSc3
z	z	k7c2
ligy	liga	k1gFnSc2
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
4	#num#	k4
<g/>
.	.	kIx.
předkola	předkolo	k1gNnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
narazil	narazit	k5eAaPmAgMnS
na	na	k7c6
PSV	PSV	kA
Eindhoven	Eindhovna	k1gFnPc2
a	a	k8xC
vyřadil	vyřadit	k5eAaPmAgInS
jej	on	k3xPp3gNnSc4
po	po	k7c6
výsledcích	výsledek	k1gInPc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
jej	on	k3xPp3gMnSc4
vyřadí	vyřadit	k5eAaPmIp3nP
v	v	k7c6
osmi	osm	k4xCc6
finále	finále	k1gNnPc6
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
klubu	klub	k1gInSc2
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
zadarmo	zadarmo	k6eAd1
brazilský	brazilský	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
Kaká	kakat	k5eAaImIp3nS
ale	ale	k8xC
i	i	k9
s	s	k7c7
ním	on	k3xPp3gInSc7
se	se	k3xPyFc4
sezona	sezona	k1gFnSc1
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
hraje	hrát	k5eAaImIp3nS
velmi	velmi	k6eAd1
špatně	špatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc7
jsou	být	k5eAaImIp3nP
špatné	špatný	k2eAgInPc1d1
a	a	k8xC
v	v	k7c6
lednu	leden	k1gInSc6
to	ten	k3xDgNnSc1
odskáče	odskákat	k5eAaPmIp3nS
trenér	trenér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
propuštěn	propuštěn	k2eAgInSc1d1
když	když	k8xS
klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
od	od	k7c2
sestupu	sestup	k1gInSc2
jen	jen	k9
šest	šest	k4xCc1
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nahrazen	nahradit	k5eAaPmNgInS
bývalým	bývalý	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
a	a	k8xC
nezkušeným	zkušený	k2eNgMnSc7d1
trenérem	trenér	k1gMnSc7
Seedorfem	Seedorf	k1gMnSc7
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zrovna	zrovna	k6eAd1
ukončil	ukončit	k5eAaPmAgMnS
hráčskou	hráčský	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezonu	sezona	k1gFnSc4
ukončí	ukončit	k5eAaPmIp3nP
s	s	k7c7
57	#num#	k4
body	bod	k1gInPc7
až	až	k8xS
na	na	k7c6
osmém	osmý	k4xOgInSc6
místě	místo	k1gNnSc6
(	(	kIx(
<g/>
nejhorší	zlý	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
od	od	k7c2
sezony	sezona	k1gFnSc2
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
patnácti	patnáct	k4xCc6
sezonách	sezona	k1gFnPc6
se	se	k3xPyFc4
nekvalifikoval	kvalifikovat	k5eNaBmAgMnS
do	do	k7c2
žádné	žádný	k3yNgFnSc2
Evropské	evropský	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
byla	být	k5eAaImAgFnS
slavnostně	slavnostně	k6eAd1
otevřena	otevřít	k5eAaPmNgFnS
„	„	k?
Casa	Casa	k1gFnSc1
Milan	Milan	k1gMnSc1
“	“	k?
<g/>
,	,	kIx,
nové	nový	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
klubu	klub	k1gInSc2
Rossoneri	Rossoneri	k1gNnPc2
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
Portello	Portello	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Trend	trend	k1gInSc1
trenérů	trenér	k1gMnPc2
z	z	k7c2
bývalých	bývalý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
klubu	klub	k1gInSc2
je	být	k5eAaImIp3nS
neúspěšný	úspěšný	k2eNgInSc1d1
i	i	k9
v	v	k7c6
sezoně	sezona	k1gFnSc6
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
když	když	k8xS
klub	klub	k1gInSc1
vede	vést	k5eAaImIp3nS
Filippo	Filippa	k1gFnSc5
Inzaghi	Inzaghi	k1gNnPc1
(	(	kIx(
<g/>
přišel	přijít	k5eAaPmAgInS
z	z	k7c2
juniorky	juniorka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získají	získat	k5eAaPmIp3nP
jen	jen	k9
52	#num#	k4
bodů	bod	k1gInPc2
a	a	k8xC
v	v	k7c6
tabulce	tabulka	k1gFnSc6
končí	končit	k5eAaImIp3nS
na	na	k7c6
desátém	desátý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezonu	sezona	k1gFnSc4
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
začíná	začínat	k5eAaImIp3nS
trénovat	trénovat	k5eAaImF
Siniša	Siniš	k2eAgFnSc1d1
Mihajlović	Mihajlović	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
neuspokojivých	uspokojivý	k2eNgInPc6d1
výsledcích	výsledek	k1gInPc6
je	být	k5eAaImIp3nS
před	před	k7c7
koncem	konec	k1gInSc7
sezony	sezona	k1gFnSc2
nahrazen	nahradit	k5eAaPmNgInS
Brocchim	Brocchim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
klub	klub	k1gInSc1
končí	končit	k5eAaImIp3nS
na	na	k7c6
sedmém	sedmý	k4xOgNnSc6
místě	místo	k1gNnSc6
s	s	k7c7
57	#num#	k4
body	bod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
třetí	třetí	k4xOgMnSc1
za	za	k7c7
sebou	se	k3xPyFc7
nehrají	hrát	k5eNaImIp3nP
Evropskou	evropský	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
naposled	naposled	k6eAd1
stalo	stát	k5eAaPmAgNnS
v	v	k7c6
sezoně	sezona	k1gFnSc6
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
když	když	k8xS
nehrálo	hrát	k5eNaImAgNnS
dokonce	dokonce	k9
pět	pět	k4xCc1
sezon	sezona	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malý	malý	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
je	být	k5eAaImIp3nS
zaznamenán	zaznamenat	k5eAaPmNgInS
v	v	k7c6
italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
když	když	k8xS
dokráčí	dokráčet	k5eAaPmIp3nS
do	do	k7c2
finále	finále	k1gNnSc2
a	a	k8xC
v	v	k7c6
něm	on	k3xPp3gMnSc6
se	se	k3xPyFc4
utká	utkat	k5eAaPmIp3nS
s	s	k7c7
Juventusem	Juventus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohrává	prohrávat	k5eAaImIp3nS
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
smí	smět	k5eAaImIp3nS
se	se	k3xPyFc4
utkat	utkat	k5eAaPmF
se	s	k7c7
stejným	stejný	k2eAgInSc7d1
klubem	klub	k1gInSc7
o	o	k7c4
italský	italský	k2eAgInSc4d1
superpohár	superpohár	k1gInSc4
protože	protože	k8xS
Juventus	Juventus	k1gInSc4
FC	FC	kA
získal	získat	k5eAaPmAgMnS
i	i	k9
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
vyhrávají	vyhrávat	k5eAaImIp3nP
na	na	k7c4
penalty	penalta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
už	už	k6eAd1
klub	klub	k1gInSc1
vede	vést	k5eAaImIp3nS
trenér	trenér	k1gMnSc1
Vincenzo	Vincenza	k1gFnSc5
Montella	Montella	k1gMnSc1
(	(	kIx(
<g/>
UC	UC	kA
Sampdoria	Sampdorium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
čestný	čestný	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Silvio	Silvio	k1gMnSc1
Berlusconi	Berluscoň	k1gFnSc3
schvaluje	schvalovat	k5eAaImIp3nS
předběžnou	předběžný	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
Li	li	k8xS
Yonghong	Yonghong	k1gInSc4
<g/>
,	,	kIx,
o	o	k7c6
prodeji	prodej	k1gInSc6
celého	celý	k2eAgInSc2d1
podílu	podíl	k1gInSc2
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
99,93	99,93	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
vlastní	vlastnit	k5eAaImIp3nP
Fininvest	Fininvest	k1gInSc4
za	za	k7c4
740	#num#	k4
milionů	milion	k4xCgInPc2
Euro	euro	k1gNnSc4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1909	#num#	k4
je	být	k5eAaImIp3nS
vlastníkem	vlastník	k1gMnSc7
klubu	klub	k1gInSc2
první	první	k4xOgMnSc1
cizinec	cizinec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Končí	končit	k5eAaImIp3nS
i	i	k9
po	po	k7c6
30	#num#	k4
letech	léto	k1gNnPc6
Adriano	Adriana	k1gFnSc5
Galliani	Galliaň	k1gFnSc6
a	a	k8xC
místo	místo	k7c2
něj	on	k3xPp3gInSc2
je	být	k5eAaImIp3nS
klub	klub	k1gInSc1
svěřen	svěřit	k5eAaPmNgMnS
Fassonem	Fasson	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezona	sezona	k1gFnSc1
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
je	být	k5eAaImIp3nS
lepší	dobrý	k2eAgFnSc1d2
než	než	k8xS
minulá	minulý	k2eAgFnSc1d1
a	a	k8xC
končí	končit	k5eAaImIp3nS
na	na	k7c6
šestém	šestý	k4xOgNnSc6
místě	místo	k1gNnSc6
zaručující	zaručující	k2eAgInSc1d1
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Klub	klub	k1gInSc1
investuje	investovat	k5eAaBmIp3nS
na	na	k7c4
novou	nový	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
astronomických	astronomický	k2eAgInPc2d1
230	#num#	k4
milionů	milion	k4xCgInPc2
Euro	euro	k1gNnSc1
do	do	k7c2
posil	posila	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přichází	přicházet	k5eAaImIp3nS
mnoho	mnoho	k4c4
hráčů	hráč	k1gMnPc2
<g/>
:	:	kIx,
Bonucci	Bonucce	k1gMnPc1
(	(	kIx(
<g/>
Juventus	Juventus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Conti	Conti	k1gNnPc2
a	a	k8xC
Franck	Francka	k1gFnPc2
Kessié	Kessiá	k1gFnSc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Atalanta	Atalant	k1gMnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
André	André	k1gMnSc1
Silva	Silva	k1gFnSc1
(	(	kIx(
<g/>
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hakan	Hakan	k1gInSc1
Çalhanoğ	Çalhanoğ	k1gInSc2
(	(	kIx(
<g/>
Leverkusen	Leverkusen	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kalinić	Kalinić	k1gFnSc1
(	(	kIx(
<g/>
Fiorentina	Fiorentina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Biglia	Biglia	k1gFnSc1
(	(	kIx(
<g/>
Lazio	Lazio	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Musacchio	Musacchio	k1gMnSc1
(	(	kIx(
<g/>
Villarreal	Villarreal	k1gMnSc1
CF	CF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ricardo	Ricardo	k1gNnSc1
Rodríguez	Rodrígueza	k1gFnPc2
(	(	kIx(
<g/>
Wolfsburg	Wolfsburg	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
další	další	k2eAgMnPc1d1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
výsledky	výsledek	k1gInPc1
jsou	být	k5eAaImIp3nP
špatné	špatný	k2eAgInPc1d1
a	a	k8xC
v	v	k7c6
listopadu	listopad	k1gInSc6
je	být	k5eAaImIp3nS
odvolán	odvolán	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
a	a	k8xC
nahrazen	nahrazen	k2eAgMnSc1d1
opět	opět	k6eAd1
bývalým	bývalý	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
Rossoneri	Rossoner	k1gFnSc2
Gattusem	Gattus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
se	se	k3xPyFc4
klub	klub	k1gInSc1
probojuje	probojovat	k5eAaPmIp3nS
do	do	k7c2
osmi	osm	k4xCc2
finále	finále	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
narazí	narazit	k5eAaPmIp3nS
na	na	k7c4
Arsenal	Arsenal	k1gFnSc4
FC	FC	kA
a	a	k8xC
ten	ten	k3xDgMnSc1
jej	on	k3xPp3gMnSc4
po	po	k7c6
výsledcích	výsledek	k1gInPc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
vyřadí	vyřadit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malý	malý	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
je	být	k5eAaImIp3nS
prohrané	prohraný	k2eAgNnSc4d1
finále	finále	k1gNnSc4
v	v	k7c6
italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
nad	nad	k7c7
Juventusem	Juventus	k1gInSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
v	v	k7c6
tabulce	tabulka	k1gFnSc6
umístí	umístit	k5eAaPmIp3nS
na	na	k7c6
šestém	šestý	k4xOgNnSc6
místě	místo	k1gNnSc6
zaručující	zaručující	k2eAgFnSc4d1
účast	účast	k1gFnSc4
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
porušení	porušení	k1gNnSc3
finanční	finanční	k2eAgFnSc2d1
fair	fair	k2eAgFnSc2d1
play	play	k0
je	být	k5eAaImIp3nS
vyřazen	vyřadit	k5eAaPmNgMnS
ze	z	k7c2
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
arbitráži	arbitráž	k1gFnSc6
je	být	k5eAaImIp3nS
povolána	povolán	k2eAgFnSc1d1
zpět	zpět	k6eAd1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
Elliott	Elliott	k2eAgInSc1d1
Management	management	k1gInSc1
Corporation	Corporation	k1gInSc1
světu	svět	k1gInSc3
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
převzala	převzít	k5eAaPmAgFnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
klubem	klub	k1gInSc7
po	po	k7c6
nezaplacení	nezaplacení	k1gNnSc6
závazků	závazek	k1gInPc2
vůči	vůči	k7c3
americkému	americký	k2eAgInSc3d1
investičnímu	investiční	k2eAgInSc3d1
fondu	fond	k1gInSc3
od	od	k7c2
prezidenta	prezident	k1gMnSc2
klubu	klub	k1gInSc2
Li	li	k8xS
Yonghong	Yonghonga	k1gFnPc2
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
i	i	k9
zvolen	zvolen	k2eAgMnSc1d1
nový	nový	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Paolo	Paolo	k1gNnSc4
Scaroni	Scaroň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
po	po	k7c6
jednom	jeden	k4xCgInSc6
roce	rok	k1gInSc6
opouští	opouštět	k5eAaImIp3nS
Bonucci	Bonucce	k1gFnSc4
(	(	kIx(
<g/>
Juventus	Juventus	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kalinić	Kalinić	k1gFnSc1
(	(	kIx(
<g/>
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c4
hostování	hostování	k1gNnSc4
André	André	k1gMnPc2
Silva	Silva	k1gFnSc1
(	(	kIx(
<g/>
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
přichází	přicházet	k5eAaImIp3nS
na	na	k7c4
hostování	hostování	k1gNnSc4
střelec	střelec	k1gMnSc1
Gonzalo	Gonzalo	k1gMnSc1
Higuaín	Higuaín	k1gMnSc1
(	(	kIx(
<g/>
Juventus	Juventus	k1gInSc1
<g/>
)	)	kIx)
ale	ale	k8xC
vydrží	vydržet	k5eAaPmIp3nS
v	v	k7c6
klubu	klub	k1gInSc6
jen	jen	k9
půl	půl	k1xP
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jarní	jarní	k2eAgFnSc4d1
část	část	k1gFnSc4
klub	klub	k1gInSc1
kupuje	kupovat	k5eAaImIp3nS
Piąteka	Piąteek	k1gMnSc4
(	(	kIx(
<g/>
CFC	CFC	kA
Janov	Janov	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Paquetu	Paqueta	k1gFnSc4
(	(	kIx(
<g/>
Flamengo	flamengo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrají	hrát	k5eAaImIp3nP
o	o	k7c4
italský	italský	k2eAgInSc4d1
superpohár	superpohár	k1gInSc4
s	s	k7c7
klubem	klub	k1gInSc7
Juventus	Juventus	k1gInSc1
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
prohrají	prohrát	k5eAaPmIp3nP
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
se	se	k3xPyFc4
klub	klub	k1gInSc1
neprobojoval	probojovat	k5eNaPmAgInS
do	do	k7c2
play	play	k0
off	off	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
zakončil	zakončit	k5eAaPmAgInS
nejlepší	dobrý	k2eAgFnSc4d3
sezonu	sezona	k1gFnSc4
za	za	k7c7
posledním	poslední	k2eAgInSc7d1
šest	šest	k4xCc1
sezon	sezona	k1gFnPc2
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
s	s	k7c7
68	#num#	k4
body	bod	k1gInPc7
<g/>
,	,	kIx,
když	když	k8xS
chyběl	chybět	k5eAaImAgMnS
jeden	jeden	k4xCgInSc4
bod	bod	k1gInSc4
od	od	k7c2
kvalifikace	kvalifikace	k1gFnSc2
pro	pro	k7c4
Ligu	liga	k1gFnSc4
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
musel	muset	k5eAaImAgMnS
obejít	obejít	k5eAaPmF
bez	bez	k7c2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
UEFA	UEFA	kA
je	být	k5eAaImIp3nS
potrestala	potrestat	k5eAaPmAgFnS
za	za	k7c4
špatné	špatný	k2eAgNnSc4d1
hospodaření	hospodaření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
neodvolal	odvolat	k5eNaPmAgMnS
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kronika	kronika	k1gFnSc1
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
Kronika	kronika	k1gFnSc1
historie	historie	k1gFnSc2
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
1899	#num#	k4
•	•	k?
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
:	:	kIx,
založení	založení	k1gNnSc2
klubu	klub	k1gInSc2
Milan	Milan	k1gMnSc1
Foot-Ball	Foot-Ball	k1gMnSc1
and	and	k?
Cricket	Cricket	k1gInSc1
Club	club	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
1900	#num#	k4
•	•	k?
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1901	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1902	#num#	k4
•	•	k?
Finalista	finalista	k1gMnSc1
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1903	#num#	k4
•	•	k?
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1904	#num#	k4
•	•	k?
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1905	#num#	k4
•	•	k?
Prohra	prohra	k1gFnSc1
ve	v	k7c6
skupině	skupina	k1gFnSc6
Lombardie	Lombardie	k1gFnSc2
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1906	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1907	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1908	#num#	k4
•	•	k?
Nezúčastnil	zúčastnit	k5eNaPmAgMnS
se	se	k3xPyFc4
Italského	italský	k2eAgInSc2d1
mistrovství	mistrovství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
1909	#num#	k4
•	•	k?
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
Lombardie	Lombardie	k1gFnSc2
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k7c2
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
1	#num#	k4
skupině	skupina	k1gFnSc6
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
1	#num#	k4
skupině	skupina	k1gFnSc6
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
finálové	finálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
Lombardie	Lombardie	k1gFnSc2
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1914	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
finálové	finálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1915	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
Vítěz	vítěz	k1gMnSc1
Federální	federální	k2eAgMnSc1d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1916	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
•	•	k?
Národní	národní	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
jsou	být	k5eAaImIp3nP
přerušeny	přerušit	k5eAaPmNgInP
kvůli	kvůli	k7c3
první	první	k4xOgInPc4
světově	světově	k6eAd1
válce	válec	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
1919	#num#	k4
•	•	k?
Klub	klub	k1gInSc1
mění	měnit	k5eAaImIp3nS
název	název	k1gInSc4
na	na	k7c4
Milan	Milan	k1gMnSc1
Football	Footballa	k1gFnPc2
Club	club	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
semifinálové	semifinálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
semifinálové	semifinálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
A	a	k9
Italského	italský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
•	•	k?
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
finálové	finálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
První	první	k4xOgFnSc1
divize	divize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nezúčastnil	zúčastnit	k5eNaPmAgMnS
se	se	k3xPyFc4
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
</s>
<s>
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
A	a	k8xC
Lega	lego	k1gNnSc2
Nord	Nord	k1gMnSc1
První	první	k4xOgFnSc2
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
•	•	k?
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
A	a	k8xC
Lega	lego	k1gNnSc2
Nord	Nord	k1gMnSc1
První	první	k4xOgFnSc2
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
•	•	k?
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
A	a	k8xC
Lega	lego	k1gNnSc2
Nord	Nord	k1gMnSc1
První	první	k4xOgFnSc2
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
•	•	k?
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
A	a	k8xC
Lega	lego	k1gNnSc2
Nord	Nord	k1gMnSc1
První	první	k4xOgFnSc2
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
finálové	finálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přerušený	přerušený	k2eAgInSc1d1
ročník	ročník	k1gInSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
</s>
<s>
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
finálové	finálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
A	A	kA
Národní	národní	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1929	#num#	k4
•	•	k?
Změna	změna	k1gFnSc1
v	v	k7c6
herním	herní	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
Serie	serie	k1gFnSc1
A.	A.	kA
</s>
<s>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
12	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
•	•	k?
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
</s>
<s>
1936	#num#	k4
•	•	k?
Klub	klub	k1gInSc1
mění	měnit	k5eAaImIp3nS
název	název	k1gInSc4
na	na	k7c4
Milan	Milan	k1gMnSc1
Associazione	Associazion	k1gInSc5
Sportiva	Sportiva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
</s>
<s>
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Středoevropského	středoevropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1939	#num#	k4
•	•	k?
Klub	klub	k1gInSc1
mění	měnit	k5eAaImIp3nS
název	název	k1gInSc4
na	na	k7c4
Associazione	Associazion	k1gInSc5
Calcio	Calcia	k1gFnSc5
Milano	Milana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
•	•	k?
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Finalista	finalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1944	#num#	k4
•	•	k?
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
divizi	divize	k1gFnSc6
</s>
<s>
1943	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
•	•	k?
Národní	národní	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
jsou	být	k5eAaImIp3nP
přerušeny	přerušit	k5eAaPmNgInP
kvůli	kvůli	k7c3
druhé	druhý	k4xOgFnSc3
světové	světový	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
1945	#num#	k4
•	•	k?
Klub	klub	k1gInSc1
mění	měnit	k5eAaImIp3nS
název	název	k1gInSc4
na	na	k7c4
Associazione	Associazion	k1gInSc5
Calcio	Calcio	k1gNnSc1
Milan	Milan	k1gMnSc1
</s>
<s>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
•	•	k?
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
ve	v	k7c6
finálové	finálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Latinského	latinský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Finalista	finalista	k1gMnSc1
Latinského	latinský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Latinském	latinský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Latinského	latinský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
</s>
<s>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Latinském	latinský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Poháru	pohár	k1gInSc6
PMEZ	PMEZ	kA
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
ve	v	k7c6
Veletržním	veletržní	k2eAgInSc6d1
poháru	pohár	k1gInSc6
</s>
<s>
1962	#num#	k4
•	•	k?
Klub	klub	k1gInSc1
mění	měnit	k5eAaImIp3nS
název	název	k1gInSc4
na	na	k7c4
Milan	Milan	k1gMnSc1
Associazione	Associazion	k1gInSc5
Calcio	Calcio	k6eAd1
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
</s>
<s>
v	v	k7c4
Interkontinentálního	interkontinentální	k2eAgMnSc4d1
poháru	pohár	k1gInSc6
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
ve	v	k7c6
Veletržním	veletržní	k2eAgInSc6d1
poháru	pohár	k1gInSc6
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Veletržního	veletržní	k2eAgInSc2d1
poháru	pohár	k1gInSc2
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Středoevropského	středoevropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Poháru	pohár	k1gInSc2
PVP	PVP	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Interkontinentálního	interkontinentální	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
v	v	k7c6
Italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Poháru	pohár	k1gInSc2
PVP	PVP	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinálová	semifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Poháru	pohár	k1gInSc6
PVP	PVP	kA
</s>
<s>
v	v	k7c6
Evropském	evropský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinálová	semifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinálová	semifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Poháru	pohár	k1gInSc6
PVP	PVP	kA
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
15	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
Sestup	sestup	k1gInSc1
do	do	k7c2
Serie	serie	k1gFnSc2
B	B	kA
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Poháru	pohár	k1gInSc6
PMEZ	PMEZ	kA
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
Vítěz	vítěz	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
B	B	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc1
<g/>
)	)	kIx)
Postup	postup	k1gInSc1
do	do	k7c2
Serie	serie	k1gFnSc2
A	a	k9
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
14	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
Sestup	sestup	k1gInSc1
do	do	k7c2
Serie	serie	k1gFnSc2
B	B	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Středoevropského	středoevropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
Vítěz	vítěz	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
B	B	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc1
<g/>
)	)	kIx)
Postup	postup	k1gInSc1
do	do	k7c2
Serie	serie	k1gFnSc2
A	a	k9
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Poháru	pohár	k1gInSc6
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
ve	v	k7c6
skupině	skupina	k1gFnSc6
1	#num#	k4
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Evropského	evropský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Interkontinentálního	interkontinentální	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
PMEZ	PMEZ	kA
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Evropského	evropský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Interkontinentálního	interkontinentální	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Evropském	evropský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Interkontinentálním	interkontinentální	k2eAgInSc6d1
poháru	pohár	k1gInSc6
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Evropského	evropský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Interkontinentálním	interkontinentální	k2eAgInSc6d1
poháru	pohár	k1gInSc6
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
sica	sica	k6eAd1
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
•	•	k?
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
AC	AC	kA
Milán	Milán	k1gInSc1
byl	být	k5eAaImAgInS
klasifikován	klasifikovat	k5eAaImNgInS
jako	jako	k9
11	#num#	k4
<g/>
.	.	kIx.
nejúspěšnější	úspěšný	k2eAgInSc1d3
klub	klub	k1gInSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
podle	podle	k7c2
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
v	v	k7c6
Poháru	pohár	k1gInSc6
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Italském	italský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
•	•	k?
Klub	klub	k1gInSc1
mění	měnit	k5eAaImIp3nS
název	název	k1gInSc4
na	na	k7c4
Associazione	Associazion	k1gInSc5
Calcio	Calcio	k1gMnSc1
Milan	Milan	k1gMnSc1
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Evropského	evropský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Interkontinentálním	interkontinentální	k2eAgInSc6d1
poháru	pohár	k1gInSc6
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Evropského	evropský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
MS	MS	kA
klubů	klub	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
v	v	k7c6
Poháru	pohár	k1gInSc6
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
Mistr	mistr	k1gMnSc1
italské	italský	k2eAgFnSc2d1
Serie	serie	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Čtvrtfinalista	čtvrtfinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
v	v	k7c6
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osmifinalista	Osmifinalista	k1gMnSc1
Evropské	evropský	k2eAgFnSc3d1
lize	liga	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Serii	serie	k1gFnSc6
A.	A.	kA
</s>
<s>
Semifinalista	semifinalista	k1gMnSc1
Italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Barvy	barva	k1gFnPc4
a	a	k8xC
symboly	symbol	k1gInPc4
</s>
<s>
Červenočerné	červenočerný	k2eAgInPc4d1
dresy	dres	k1gInPc4
</s>
<s>
Barvy	barva	k1gFnPc1
</s>
<s>
V	v	k7c6
celé	celá	k1gFnSc6
své	svůj	k3xOyFgFnSc6
historii	historie	k1gFnSc6
měl	mít	k5eAaImAgInS
klub	klub	k1gInSc1
červenou	červený	k2eAgFnSc4d1
a	a	k8xC
černou	černý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
jako	jako	k8xS
své	svůj	k3xOyFgFnPc4
výrazné	výrazný	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
barvy	barva	k1gFnPc1
byly	být	k5eAaImAgFnP
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
řekl	říct	k5eAaPmAgMnS
Herbert	Herbert	k1gMnSc1
Kilpin	Kilpin	k1gMnSc1
<g/>
,	,	kIx,
vybrány	vybrán	k2eAgFnPc1d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
představovaly	představovat	k5eAaImAgInP
oheň	oheň	k1gInSc4
ďáblů	ďábel	k1gMnPc2
(	(	kIx(
<g/>
červená	červený	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
strach	strach	k1gInSc1
u	u	k7c2
protivníků	protivník	k1gMnPc2
(	(	kIx(
<g/>
černá	černý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hráči	hráč	k1gMnPc1
nosí	nosit	k5eAaImIp3nS
červenočerné	červenočerný	k2eAgNnSc4d1
pruhovaný	pruhovaný	k2eAgInSc4d1
dres	dres	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
dostali	dostat	k5eAaPmAgMnP
přezdívku	přezdívka	k1gFnSc4
Rossoneri	Rossoneri	k1gNnSc2
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnPc1d1
trenky	trenky	k1gFnPc1
a	a	k8xC
černé	černý	k2eAgFnPc1d1
ponožky	ponožka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
obvyklý	obvyklý	k2eAgInSc4d1
dres	dres	k1gInSc4
je	být	k5eAaImIp3nS
v	v	k7c6
bílé	bílý	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
<g/>
;	;	kIx,
s	s	k7c7
tomto	tento	k3xDgInSc6
dresu	dres	k1gInSc6
klub	klub	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgMnS
šest	šest	k4xCc1
z	z	k7c2
osmi	osm	k4xCc2
finále	finále	k1gNnPc2
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
prohrál	prohrát	k5eAaPmAgMnS
pouze	pouze	k6eAd1
s	s	k7c7
Ajaxem	Ajax	k1gInSc7
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
proti	proti	k7c3
Liverpoolu	Liverpool	k1gInSc3
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
s	s	k7c7
dresem	dres	k1gInSc7
Rossoneri	Rossoner	k1gFnSc2
vyhrál	vyhrát	k5eAaPmAgMnS
pouze	pouze	k6eAd1
jedno	jeden	k4xCgNnSc4
ze	z	k7c2
tří	tři	k4xCgNnPc2
finále	finále	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc1
dres	dres	k1gInSc1
byl	být	k5eAaImAgInS
naopak	naopak	k6eAd1
převážně	převážně	k6eAd1
černý	černý	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
žlutý	žlutý	k2eAgInSc1d1
<g/>
,	,	kIx,
modrý	modrý	k2eAgInSc1d1
<g/>
,	,	kIx,
červený	červený	k2eAgInSc1d1
nebo	nebo	k8xC
stříbrný	stříbrný	k2eAgInSc1d1
a	a	k8xC
dokonce	dokonce	k9
zelený	zelený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Symboly	symbol	k1gInPc1
</s>
<s>
První	první	k4xOgInSc1
znak	znak	k1gInSc1
klubu	klub	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
</s>
<s>
Znak	znak	k1gInSc1
klubu	klub	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
</s>
<s>
Po	po	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
bylo	být	k5eAaImAgNnS
logo	logo	k1gNnSc1
klubu	klub	k1gInSc2
jednoduše	jednoduše	k6eAd1
vlajkou	vlajka	k1gFnSc7
města	město	k1gNnSc2
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
přezdívka	přezdívka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
barev	barva	k1gFnPc2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Il	Il	k1gMnSc1
Diavolo	Diavola	k1gFnSc5
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
byl	být	k5eAaImAgInS
jako	jako	k9
logo	logo	k1gNnSc4
klubu	klub	k1gInSc2
použit	použít	k5eAaPmNgInS
také	také	k9
obrázek	obrázek	k1gInSc1
stylizovaného	stylizovaný	k2eAgNnSc2d1
červeného	červené	k1gNnSc2
ďábla	ďábel	k1gMnSc2
na	na	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
hvězdy	hvězda	k1gFnSc2
(	(	kIx(
<g/>
uznání	uznání	k1gNnSc1
za	za	k7c4
10	#num#	k4
titulů	titul	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
ukazuje	ukazovat	k5eAaImIp3nS
červenou	červený	k2eAgFnSc4d1
a	a	k8xC
černou	černý	k2eAgFnSc4d1
sociální	sociální	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
vlevo	vlevo	k6eAd1
a	a	k8xC
vlajku	vlajka	k1gFnSc4
města	město	k1gNnSc2
Milán	Milán	k1gInSc1
(	(	kIx(
<g/>
červený	červený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
na	na	k7c6
bílém	bílý	k2eAgNnSc6d1
pozadí	pozadí	k1gNnSc6
<g/>
)	)	kIx)
vpravo	vpravo	k6eAd1
<g/>
,	,	kIx,
s	s	k7c7
zkratkou	zkratka	k1gFnSc7
ACM	ACM	kA
nahoře	nahoře	k6eAd1
a	a	k8xC
rokem	rok	k1gInSc7
založení	založení	k1gNnSc2
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
níže	nízce	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Maskot	maskot	k1gInSc1
</s>
<s>
Maskot	maskot	k1gInSc1
Milanello	Milanello	k1gNnSc1
</s>
<s>
Od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2006	#num#	k4
je	být	k5eAaImIp3nS
oficiálním	oficiální	k2eAgInSc7d1
maskotem	maskot	k1gInSc7
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
navržené	navržený	k2eAgNnSc1d1
společností	společnost	k1gFnSc7
Warner	Warnra	k1gFnPc2
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milanello	Milanello	k1gNnSc1
<g/>
,	,	kIx,
červený	červený	k2eAgMnSc1d1
ďábel	ďábel	k1gMnSc1
v	v	k7c6
dresu	dres	k1gInSc6
Rossoneri	Rossoner	k1gFnSc2
a	a	k8xC
s	s	k7c7
fotbalovým	fotbalový	k2eAgInSc7d1
míčem	míč	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
jméno	jméno	k1gNnSc1
také	také	k9
ctí	ctít	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
sportovního	sportovní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Hymna	hymna	k1gFnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
jsou	být	k5eAaImIp3nP
interpretem	interpret	k1gMnSc7
oficiální	oficiální	k2eAgFnSc2d1
milánské	milánský	k2eAgFnSc2d1
hymny	hymna	k1gFnSc2
rapper	rapper	k1gMnSc1
Emis	Emisa	k1gFnPc2
Killa	Killa	k1gMnSc1
a	a	k8xC
basista	basista	k1gMnSc1
Saturnino	Saturnin	k2eAgNnSc1d1
píseň	píseň	k1gFnSc4
Rossoneri	Rossoneri	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
116	#num#	k4
<g/>
.	.	kIx.
narozenin	narozeniny	k1gFnPc2
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nahradil	nahradit	k5eAaPmAgInS
píseň	píseň	k1gFnSc4
Milán	Milán	k1gInSc1
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
<g/>
,	,	kIx,
napsaný	napsaný	k2eAgInSc1d1
skladatelem	skladatel	k1gMnSc7
Renisem	Renis	k1gInSc7
<g/>
,	,	kIx,
fanouškem	fanoušek	k1gMnSc7
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
Guantinim	Guantinim	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
malými	malý	k2eAgInPc7d1
příspěvky	příspěvek	k1gInPc7
v	v	k7c6
textu	text	k1gInSc6
byl	být	k5eAaImAgMnS
i	i	k9
prezident	prezident	k1gMnSc1
klubu	klub	k1gInSc2
Silvio	Silvio	k6eAd1
Berlusconi	Berluscon	k1gMnPc1
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Klubové	klubový	k2eAgNnSc1d1
zázemí	zázemí	k1gNnSc1
</s>
<s>
Stadion	stadion	k1gInSc1
</s>
<s>
V	v	k7c6
prvních	první	k4xOgNnPc6
letech	léto	k1gNnPc6
po	po	k7c6
vzniku	vznik	k1gInSc6
klubu	klub	k1gInSc2
hráli	hrát	k5eAaImAgMnP
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
na	na	k7c6
různých	různý	k2eAgNnPc6d1
hřištích	hřiště	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
dubna	duben	k1gInSc2
1900	#num#	k4
do	do	k7c2
února	únor	k1gInSc2
1902	#num#	k4
hráli	hrát	k5eAaImAgMnP
na	na	k7c6
stadionu	stadion	k1gInSc6
Campo	Campa	k1gFnSc5
Trotter	Trotter	k1gInSc1
v	v	k7c4
Piazza	Piazz	k1gMnSc4
Doria	Dorius	k1gMnSc4
(	(	kIx(
<g/>
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
poté	poté	k6eAd1
postavena	postaven	k2eAgFnSc1d1
hlavní	hlavní	k2eAgFnSc1d1
vlaková	vlakový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
v	v	k7c6
Miláně	Milán	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
března	březen	k1gInSc2
1903	#num#	k4
do	do	k7c2
února	únor	k1gInSc2
1905	#num#	k4
hráli	hrát	k5eAaImAgMnP
na	na	k7c4
Campo	Campa	k1gFnSc5
Acquabella	Acquabell	k1gMnSc4
v	v	k7c4
Indipendenza	Indipendenz	k1gMnSc4
<g/>
,	,	kIx,
od	od	k7c2
ledna	leden	k1gInSc2
1906	#num#	k4
do	do	k7c2
března	březen	k1gInSc2
1914	#num#	k4
se	se	k3xPyFc4
<g />
.	.	kIx.
</s>
<s hack="1">
hrálo	hrát	k5eAaImAgNnS
na	na	k7c4
Campo	Campa	k1gFnSc5
Milan	Milan	k1gMnSc1
di	di	k?
Porta	porta	k1gFnSc1
Monforte	Monfort	k1gInSc5
<g/>
,	,	kIx,
od	od	k7c2
listopadu	listopad	k1gInSc2
1914	#num#	k4
do	do	k7c2
března	březen	k1gInSc2
1919	#num#	k4
na	na	k7c6
Velodromo	Velodroma	k1gFnSc5
Sempione	Sempion	k1gInSc5
<g/>
,	,	kIx,
od	od	k7c2
října	říjen	k1gInSc2
až	až	k6eAd1
do	do	k7c2
listopadu	listopad	k1gInSc2
1919	#num#	k4
na	na	k7c6
Campo	Campa	k1gFnSc5
Pirelli	Pirell	k1gMnPc7
(	(	kIx(
<g/>
v	v	k7c6
oblasti	oblast	k1gFnSc6
Bicocca	Bicocc	k1gInSc2
<g/>
))	))	k?
<g/>
,	,	kIx,
od	od	k7c2
listopadu	listopad	k1gInSc2
do	do	k7c2
prosince	prosinec	k1gInSc2
1919	#num#	k4
klub	klub	k1gInSc1
hrál	hrát	k5eAaImAgInS
na	na	k7c4
různých	různý	k2eAgInPc2d1
městských	městský	k2eAgInPc2d1
stadionů	stadion	k1gInPc2
protože	protože	k8xS
čekali	čekat	k5eAaImAgMnP
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
nového	nový	k2eAgInSc2d1
stadionu	stadion	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
1920	#num#	k4
začali	začít	k5eAaPmAgMnP
hrát	hrát	k5eAaImF
na	na	k7c4
Campo	Campa	k1gFnSc5
di	di	k?
viale	viale	k1gNnSc1
Lombardia	Lombardium	k1gNnSc2
až	až	k9
do	do	k7c2
července	červenec	k1gInSc2
1926	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
klubu	klub	k1gInSc2
Piero	Piero	k1gNnSc4
Pirelli	Pirelle	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
slavnostně	slavnostně	k6eAd1
přestřihl	přestřihnout	k5eAaPmAgInS
pásku	páska	k1gFnSc4
nového	nový	k2eAgInSc2d1
stadionu	stadion	k1gInSc2
Stadio	Stadio	k1gMnSc1
San	San	k1gMnSc1
Siro	siro	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
něm	on	k3xPp3gMnSc6
Rossoneri	Rossoner	k1gMnPc1
hráli	hrát	k5eAaImAgMnP
do	do	k7c2
roku	rok	k1gInSc2
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
|	|	kIx~
<g/>
války	válka	k1gFnSc2
klub	klub	k1gInSc1
hrál	hrát	k5eAaImAgInS
v	v	k7c4
Arena	Aren	k1gMnSc4
Civica	Civicus	k1gMnSc4
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
se	se	k3xPyFc4
pro	pro	k7c4
klub	klub	k1gInSc4
stal	stát	k5eAaPmAgMnS
domovem	domov	k1gInSc7
Stadio	Stadio	k1gNnSc4
San	San	k1gFnPc2
Siro	siro	k6eAd1
a	a	k8xC
je	on	k3xPp3gFnPc4
jím	jíst	k5eAaImIp1nS
dodnes	dodnes	k6eAd1
i	i	k9
s	s	k7c7
klubem	klub	k1gInSc7
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
byl	být	k5eAaImAgInS
stadion	stadion	k1gInSc1
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
80	#num#	k4
018	#num#	k4
míst	místo	k1gNnPc2
pojmenován	pojmenovat	k5eAaPmNgInS
po	po	k7c6
bývalém	bývalý	k2eAgInSc6d1
fotbalistovi	fotbalista	k1gMnSc3
obou	dva	k4xCgInPc2
milánských	milánský	k2eAgInPc2d1
klubů	klub	k1gInPc2
a	a	k8xC
reprezentace	reprezentace	k1gFnSc2
Meazzovi	Meazzův	k2eAgMnPc1d1
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Stadio	Stadia	k1gMnSc5
Giuseppe	Giusepp	k1gMnSc5
Meazza	Meazza	k1gFnSc1
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
klub	klub	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
postavit	postavit	k5eAaPmF
nový	nový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
a	a	k8xC
vyvinula	vyvinout	k5eAaPmAgFnS
jej	on	k3xPp3gInSc4
společnost	společnost	k1gFnSc1
Arup	Arup	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
postavila	postavit	k5eAaPmAgFnS
Allianz	Allianz	k1gInSc4
Arena	Aren	k1gInSc2
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
a	a	k8xC
stadion	stadion	k1gInSc4
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
postavena	postaven	k2eAgFnSc1d1
hned	hned	k6eAd1
vedle	vedle	k7c2
sídla	sídlo	k1gNnSc2
klubu	klub	k1gInSc2
Casa	Casa	k1gMnSc1
Milan	Milan	k1gMnSc1
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
bylo	být	k5eAaImAgNnS
od	od	k7c2
projektu	projekt	k1gInSc2
upuštěno	upuštěn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
oba	dva	k4xCgInPc4
milánské	milánský	k2eAgInPc4d1
kluby	klub	k1gInPc4
rozhodli	rozhodnout	k5eAaPmAgMnP
že	že	k8xS
nový	nový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
je	být	k5eAaImIp3nS
nutný	nutný	k2eAgInSc1d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
</s>
<s>
Sportovní	sportovní	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Milanello	Milanello	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c4
Provincie	provincie	k1gFnPc4
Varese	Varese	k1gFnSc2
mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
Carnago	Carnago	k1gMnSc1
<g/>
,	,	kIx,
Cassano	Cassana	k1gFnSc5
Magnago	Magnaga	k1gMnSc5
a	a	k8xC
Cairate	Cairat	k1gMnSc5
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
slavnostně	slavnostně	k6eAd1
otevřen	otevřít	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
ponořen	ponořen	k2eAgMnSc1d1
do	do	k7c2
zelené	zelený	k2eAgFnSc2d1
oázy	oáza	k1gFnSc2
o	o	k7c6
rozloze	rozloha	k1gFnSc6
160	#num#	k4
000	#num#	k4
m²	m²	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
také	také	k9
borový	borový	k2eAgInSc4d1
les	les	k1gInSc4
a	a	k8xC
rybník	rybník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
šest	šest	k4xCc4
pravidelných	pravidelný	k2eAgNnPc2d1
fotbalových	fotbalový	k2eAgNnPc2d1
hřišť	hřiště	k1gNnPc2
<g/>
,	,	kIx,
umělé	umělý	k2eAgNnSc4d1
travnaté	travnatý	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
<g/>
,	,	kIx,
zastřešené	zastřešený	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
se	s	k7c7
syntetickým	syntetický	k2eAgInSc7d1
dnem	den	k1gInSc7
a	a	k8xC
travnaté	travnatý	k2eAgNnSc1d1
hřiště	hřiště	k1gNnSc1
obklopené	obklopený	k2eAgNnSc1d1
zdí	zeď	k1gFnSc7
<g/>
,	,	kIx,
zvané	zvaný	k2eAgFnPc1d1
klec	klec	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
sám	sám	k3xTgMnSc1
zatrénovat	zatrénovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Součástí	součást	k1gFnSc7
sportovního	sportovní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
tělocvičny	tělocvična	k1gFnSc2
<g/>
,	,	kIx,
lékařské	lékařský	k2eAgFnSc2d1
místnosti	místnost	k1gFnSc2
<g/>
,	,	kIx,
tisková	tiskový	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
<g/>
,	,	kIx,
studijní	studijní	k2eAgInPc4d1
programy	program	k1gInPc4
a	a	k8xC
penzion	penzion	k1gInSc4
s	s	k7c7
cílem	cíl	k1gInSc7
ubytovat	ubytovat	k5eAaPmF
týmy	tým	k1gInPc4
mládeže	mládež	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
posilovny	posilovna	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
bazén	bazén	k1gInSc1
sloužící	sloužící	k2eAgInSc1d1
k	k	k7c3
zotavení	zotavení	k1gNnSc3
zraněných	zraněný	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
Miláně	Milán	k1gInSc6
také	také	k9
přítomno	přítomen	k2eAgNnSc1d1
specializované	specializovaný	k2eAgNnSc1d1
zdravotnické	zdravotnický	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
MilanLab	MilanLaba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Laboratoř	laboratoř	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
prostřednictvím	prostřednictvím	k7c2
analýzy	analýza	k1gFnSc2
dat	datum	k1gNnPc2
a	a	k8xC
individuálních	individuální	k2eAgInPc2d1
fyzických	fyzický	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
zaměřených	zaměřený	k2eAgInPc2d1
na	na	k7c4
vytvoření	vytvoření	k1gNnSc4
konkrétních	konkrétní	k2eAgFnPc2d1
přizpůsobených	přizpůsobený	k2eAgFnPc2d1
tréninkových	tréninkový	k2eAgFnPc2d1
relací	relace	k1gFnPc2
snaží	snažit	k5eAaImIp3nS
zabránit	zabránit	k5eAaPmF
zraněním	zranění	k1gNnSc7
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Trenéři	trenér	k1gMnPc1
a	a	k8xC
prezidenti	prezident	k1gMnPc1
</s>
<s>
Trenéři	trenér	k1gMnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Trenéři	trenér	k1gMnPc1
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1899	#num#	k4
klub	klub	k1gInSc4
vedlo	vést	k5eAaImAgNnS
60	#num#	k4
trenérů	trenér	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
trenér	trenér	k1gMnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
kapitán	kapitán	k1gMnSc1
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Herbert	Herbert	k1gMnSc1
Kilpin	Kilpin	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
týmu	tým	k1gInSc2
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1906	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
prvním	první	k4xOgMnPc3
profesionálním	profesionální	k2eAgMnPc3d1
trenérem	trenér	k1gMnSc7
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
Ferdi	Ferd	k1gMnPc1
Oppenheim	Oppenheimo	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
také	také	k9
první	první	k4xOgMnSc1
zahraniční	zahraniční	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
dvě	dva	k4xCgFnPc1
sezóny	sezóna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnPc3d3
a	a	k8xC
dlouhodobě	dlouhodobě	k6eAd1
trvajícím	trvající	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
AC	AC	kA
Milán	Milán	k1gInSc1
byl	být	k5eAaImAgMnS
Nereo	Nereo	k1gMnSc1
Rocco	Rocco	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k1gMnSc1
jako	jako	k8xS,k8xC
Parò	Parò	k1gMnSc1
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
459	#num#	k4
zápasů	zápas	k1gInPc2
vedl	vést	k5eAaImAgMnS
z	z	k7c2
lavičky	lavička	k1gFnSc2
ve	v	k7c6
13	#num#	k4
sezónách	sezóna	k1gFnPc6
(	(	kIx(
<g/>
9	#num#	k4
úplných	úplný	k2eAgFnPc2d1
a	a	k8xC
další	další	k2eAgInPc1d1
4	#num#	k4
částečně	částečně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rossoneri	Rossoneri	k1gNnSc1
pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
vyhrálo	vyhrát	k5eAaPmAgNnS
10	#num#	k4
trofejí	trofej	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
x	x	k?
ligu	liga	k1gFnSc4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
x	x	k?
domácí	domácí	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
x	x	k?
PMEZ	PMEZ	kA
<g/>
,	,	kIx,
2	#num#	k4
<g/>
x	x	k?
PVP	PVP	kA
a	a	k8xC
1	#num#	k4
<g/>
x	x	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgMnSc7d1
úspěšným	úspěšný	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
byl	být	k5eAaImAgMnS
Arrigo	Arrigo	k1gMnSc1
Sacchi	Sacch	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
1987	#num#	k4
<g/>
–	–	k?
<g/>
91	#num#	k4
pro	pro	k7c4
klub	klub	k1gInSc4
získal	získat	k5eAaPmAgMnS
8	#num#	k4
trofejí	trofej	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
x	x	k?
ligu	liga	k1gFnSc4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
x	x	k?
PMEZ	PMEZ	kA
<g/>
,	,	kIx,
2	#num#	k4
<g/>
x	x	k?
Evropské	evropský	k2eAgInPc1d1
superpoháry	superpohár	k1gInPc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
x	x	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
a	a	k8xC
1	#num#	k4
<g/>
x	x	k?
italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
jeho	jeho	k3xOp3gNnSc4
působení	působení	k1gNnSc4
patřil	patřit	k5eAaImAgInS
klub	klub	k1gInSc1
k	k	k7c3
nejsilnějším	silný	k2eAgFnPc3d3
klubovým	klubový	k2eAgFnPc3d1
týmem	tým	k1gInSc7
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Arrigo	Arrigo	k1gNnSc1
Sacchi	Sacchi	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
říjnu	říjen	k1gInSc6
1991	#num#	k4
stal	stát	k5eAaPmAgMnS
trenérem	trenér	k1gMnSc7
Italské	italský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
a	a	k8xC
v	v	k7c6
klubu	klub	k1gInSc6
byl	být	k5eAaImAgMnS
nahrazen	nahradit	k5eAaPmNgMnS
bývalým	bývalý	k2eAgMnSc7d1
fotbalistou	fotbalista	k1gMnSc7
Rossoneri	Rossoner	k1gFnSc2
Capellem	Capello	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
trenér	trenér	k1gMnSc1
mládežnických	mládežnický	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pěti	pět	k4xCc2
sezónách	sezóna	k1gFnPc6
vyhrál	vyhrát	k5eAaPmAgMnS
pro	pro	k7c4
klub	klub	k1gInSc4
9	#num#	k4
trofejí	trofej	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
x	x	k?
ligu	liga	k1gFnSc4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
x	x	k?
LM	LM	kA
<g/>
,	,	kIx,
3	#num#	k4
<g/>
x	x	k?
Evropské	evropský	k2eAgInPc1d1
superpoháry	superpohár	k1gInPc1
a	a	k8xC
1	#num#	k4
<g/>
x	x	k?
italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubu	klub	k1gInSc2
se	se	k3xPyFc4
přezdívalo	přezdívat	k5eAaImAgNnS
neporazitelný	porazitelný	k2eNgMnSc1d1
<g/>
,	,	kIx,
také	také	k9
založil	založit	k5eAaPmAgMnS
rekord	rekord	k1gInSc4
58	#num#	k4
zápasů	zápas	k1gInPc2
bez	bez	k7c2
porážky	porážka	k1gFnSc2
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Druhým	druhý	k4xOgInSc7
s	s	k7c7
nejvíce	nejvíce	k6eAd1,k6eAd3
odtrénovaných	odtrénovaný	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
má	mít	k5eAaImIp3nS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
kontě	konto	k1gNnSc6
trenér	trenér	k1gMnSc1
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotti	k1gNnSc1
<g/>
:	:	kIx,
trenér	trenér	k1gMnSc1
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
převzal	převzít	k5eAaPmAgMnS
klub	klub	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
a	a	k8xC
vedl	vést	k5eAaImAgInS
jej	on	k3xPp3gMnSc4
do	do	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtrénoval	odtrénovat	k5eAaPmAgMnS
420	#num#	k4
zápasů	zápas	k1gInPc2
z	z	k7c2
lavičky	lavička	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vyhrál	vyhrát	k5eAaPmAgInS
dva	dva	k4xCgInPc1
krát	krát	k6eAd1
LM	LM	kA
jako	jako	k9
hráč	hráč	k1gMnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
)	)	kIx)
i	i	k9
jako	jako	k8xC,k8xS
trenér	trenér	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
klub	klub	k1gInSc4
vyhrál	vyhrát	k5eAaPmAgMnS
8	#num#	k4
trofejí	trofej	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
x	x	k?
ligu	liga	k1gFnSc4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
x	x	k?
domácí	domácí	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
x	x	k?
italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
x	x	k?
LM	LM	kA
<g/>
,	,	kIx,
2	#num#	k4
<g/>
x	x	k?
Evropské	evropský	k2eAgInPc1d1
superpoháry	superpohár	k1gInPc1
a	a	k8xC
1	#num#	k4
<g/>
x	x	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Prezidenti	prezident	k1gMnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Prezidenti	prezident	k1gMnPc1
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
let	léto	k1gNnPc2
své	svůj	k3xOyFgFnSc2
klubové	klubový	k2eAgFnSc2d1
historie	historie	k1gFnSc2
se	se	k3xPyFc4
vystřídalo	vystřídat	k5eAaPmAgNnS
23	#num#	k4
prezidentů	prezident	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
prezidentem	prezident	k1gMnSc7
klubu	klub	k1gInSc2
byl	být	k5eAaImAgMnS
Alfred	Alfred	k1gMnSc1
Edwards	Edwardsa	k1gFnPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
zakládajících	zakládající	k2eAgMnPc2d1
členů	člen	k1gMnPc2
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
tuto	tento	k3xDgFnSc4
pozici	pozice	k1gFnSc4
zastává	zastávat	k5eAaImIp3nS
Paolo	Paolo	k1gNnSc4
Scaroni	Scaroň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Nejdéle	dlouho	k6eAd3
sloužící	sloužící	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
byl	být	k5eAaImAgInS
Silvio	Silvio	k6eAd1
Berlusconi	Berluscon	k1gMnPc1
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klubu	klub	k1gInSc6
zastával	zastávat	k5eAaImAgMnS
funkci	funkce	k1gFnSc4
celkem	celkem	k6eAd1
31	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1986	#num#	k4
do	do	k7c2
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozice	pozice	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
formálně	formálně	k6eAd1
neobsazená	obsazený	k2eNgFnSc1d1
od	od	k7c2
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2004	#num#	k4
do	do	k7c2
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2006	#num#	k4
a	a	k8xC
od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
do	do	k7c2
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2012	#num#	k4
z	z	k7c2
důvodu	důvod	k1gInSc2
rezignace	rezignace	k1gFnSc2
Berlusconiho	Berlusconi	k1gMnSc2
po	po	k7c6
schválení	schválení	k1gNnSc6
zákona	zákon	k1gInSc2
o	o	k7c6
střetu	střet	k1gInSc6
zájmů	zájem	k1gInPc2
v	v	k7c6
období	období	k1gNnSc6
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
předsednictví	předsednictví	k1gNnSc2
státu	stát	k1gInSc2
Itálie	Itálie	k1gFnSc2
a	a	k8xC
od	od	k7c2
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2012	#num#	k4
do	do	k7c2
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zastával	zastávat	k5eAaImAgInS
funkci	funkce	k1gFnSc4
čestného	čestný	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Berlusconi	Berluscon	k1gMnPc1
je	on	k3xPp3gInPc4
také	také	k9
nejúspěšnější	úspěšný	k2eAgFnSc1d3
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
trofejí	trofej	k1gFnSc7
<g/>
:	:	kIx,
klub	klub	k1gInSc1
pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
stal	stát	k5eAaPmAgInS
8	#num#	k4
<g/>
x	x	k?
mistr	mistr	k1gMnSc1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
x	x	k?
vyhrál	vyhrát	k5eAaPmAgMnS
LM	LM	kA
<g/>
,	,	kIx,
2	#num#	k4
<g/>
x	x	k?
vyhrál	vyhrát	k5eAaPmAgMnS
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
a	a	k8xC
1	#num#	k4
<g/>
x	x	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
též	též	k9
vyhrál	vyhrát	k5eAaPmAgMnS
také	také	k9
7	#num#	k4
<g/>
x	x	k?
italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
x	x	k?
Evropské	evropský	k2eAgInPc1d1
superpoháry	superpohár	k1gInPc1
a	a	k8xC
jednou	jednou	k6eAd1
domácí	domácí	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
29	#num#	k4
oficiálních	oficiální	k2eAgFnPc2d1
trofejí	trofej	k1gFnPc2
za	za	k7c4
31	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
26	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
během	během	k7c2
20	#num#	k4
let	léto	k1gNnPc2
předsednictví	předsednictví	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Fotbalisté	fotbalista	k1gMnPc1
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Fotbalisté	fotbalista	k1gMnPc1
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Síň	síň	k1gFnSc1
slávy	sláva	k1gFnSc2
(	(	kIx(
<g/>
Hall	Hall	k1gInSc1
of	of	k?
Fame	Fame	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Níže	nízce	k6eAd2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
seznam	seznam	k1gInSc1
54	#num#	k4
fotbalistů	fotbalista	k1gMnPc2
zařazených	zařazený	k2eAgMnPc2d1
do	do	k7c2
síně	síň	k1gFnSc2
slávy	sláva	k1gFnSc2
na	na	k7c6
oficiálním	oficiální	k2eAgInSc6d1
webu	web	k1gInSc6
AC	AC	kA
Milán	Milán	k1gInSc1
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slavní	slavný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
Demetrio	Demetrio	k6eAd1
Albertini	Albertin	k2eAgMnPc1d1
·	·	k?
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
José	José	k6eAd1
Altafini	Altafin	k1gMnPc1
·	·	k?
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotť	k1gFnSc2
·	·	k?
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
-	-	kIx~
<g/>
92	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Roberto	Roberta	k1gFnSc5
Baggio	Baggia	k1gFnSc5
·	·	k?
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
-	-	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Franco	Franco	k6eAd1
Baresi	Barese	k1gFnSc4
·	·	k?
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
-	-	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Oliver	Oliver	k1gMnSc1
Bierhoff	Bierhoff	k1gMnSc1
·	·	k?
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zvonimir	Zvonimir	k1gMnSc1
Boban	Boban	k1gMnSc1
·	·	k?
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ruben	ruben	k2eAgMnSc1d1
Buriani	Burian	k1gMnPc1
·	·	k?
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
-	-	kIx~
<g/>
82	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cafu	Cafu	k6eAd1
·	·	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fabio	Fabio	k6eAd1
Capello	Capello	k1gNnSc1
·	·	k?
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alessandro	Alessandra	k1gFnSc5
Costacurta	Costacurt	k1gMnSc4
·	·	k?
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fabio	Fabio	k6eAd1
Cudicini	Cudicin	k2eAgMnPc1d1
·	·	k?
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
-	-	kIx~
<g/>
72	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marcel	Marcel	k1gMnSc1
Desailly	Desailla	k1gFnSc2
·	·	k?
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dida	Dida	k1gFnSc1
·	·	k?
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Roberto	Roberta	k1gFnSc5
Donadoni	Donadoň	k1gFnSc5
·	·	k?
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
-	-	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
-	-	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alberigo	Alberigo	k6eAd1
Evani	Evan	k1gMnPc1
·	·	k?
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
-	-	kIx~
<g/>
93	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Filippo	Filippa	k1gFnSc5
Galli	Gall	k1gFnSc5
·	·	k?
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
-	-	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Giovanni	Giovanen	k2eAgMnPc1d1
Galli	Gall	k1gMnPc1
·	·	k?
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gunnar	Gunnar	k1gMnSc1
Gren	Gren	k1gMnSc1
·	·	k?
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ruud	Ruud	k1gMnSc1
Gullit	Gullita	k1gFnPc2
·	·	k?
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
-	-	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Filippo	Filippa	k1gFnSc5
Inzaghi	Inzagh	k1gFnSc5
·	·	k?
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kaká	kakat	k5eAaImIp3nS
·	·	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Leonardo	Leonardo	k1gMnSc1
·	·	k?
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nils	Nils	k6eAd1
Liedholm	Liedholm	k1gInSc1
·	·	k?
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
-	-	kIx~
<g/>
61	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Giovanni	Giovanen	k2eAgMnPc1d1
Lodetti	Lodett	k1gMnPc1
·	·	k?
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
-	-	kIx~
<g/>
70	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Aldo	Aldo	k1gMnSc1
Maldera	Maldero	k1gNnSc2
·	·	k?
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
-	-	kIx~
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
-	-	kIx~
<g/>
82	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cesare	Cesar	k1gMnSc5
Maldini	Maldin	k2eAgMnPc1d1
·	·	k?
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
-	-	kIx~
<g/>
66	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Paolo	Paolo	k1gNnSc1
Maldini	Maldin	k2eAgMnPc1d1
·	·	k?
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Daniele	Daniela	k1gFnSc3
Massaro	Massara	k1gFnSc5
·	·	k?
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
-	-	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
-	-	kIx~
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alessandro	Alessandra	k1gFnSc5
Nesta	Nesto	k1gNnPc1
·	·	k?
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gunnar	Gunnar	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
·	·	k?
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
-	-	kIx~
<g/>
56	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Andrea	Andrea	k1gFnSc1
Pirlo	Pirlo	k1gNnSc1
·	·	k?
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pierino	Pierin	k2eAgNnSc1d1
Prati	Prati	k1gNnSc1
·	·	k?
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
-	-	kIx~
<g/>
73	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Luigi	Luigi	k1gNnSc1
Radice	Radice	k1gFnSc2
·	·	k?
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
-	-	kIx~
<g/>
59	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Frank	Frank	k1gMnSc1
Rijkaard	Rijkaard	k1gMnSc1
·	·	k?
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
-	-	kIx~
<g/>
93	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gianni	Gianeň	k1gFnSc3
Rivera	Rivero	k1gNnSc2
·	·	k?
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ronaldinho	Ronaldinze	k6eAd1
·	·	k?
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Roberto	Roberta	k1gFnSc5
Rosato	Rosata	k1gFnSc5
·	·	k?
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
-	-	kIx~
<g/>
73	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sebastiano	Sebastiana	k1gFnSc5
Rossi	Ross	k1gMnSc3
·	·	k?
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rui	Rui	k?
Costa	Costa	k1gFnSc1
·	·	k?
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dino	Dino	k6eAd1
Sani	saně	k1gFnSc4
·	·	k?
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dejan	Dejan	k1gMnSc1
Savićević	Savićević	k1gMnSc1
·	·	k?
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
-	-	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Juan	Juan	k1gMnSc1
Alberto	Alberta	k1gFnSc5
Schiaffino	Schiaffino	k1gNnSc1
·	·	k?
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
-	-	kIx~
<g/>
60	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karl-Heinz	Karl-Heinz	k1gMnSc1
Schnellinger	Schnellinger	k1gMnSc1
·	·	k?
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
-	-	kIx~
<g/>
74	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Clarence	Clarence	k1gFnSc1
Seedorf	Seedorf	k1gInSc1
·	·	k?
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Serginho	Serginze	k6eAd1
·	·	k?
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Andrij	Andrít	k5eAaPmRp2nS
Ševčenko	Ševčenka	k1gFnSc5
·	·	k?
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marco	Marca	k1gMnSc5
Simone	Simon	k1gMnSc5
·	·	k?
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
-	-	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Angelo	Angela	k1gFnSc5
Benedicto	Benedicta	k1gFnSc5
Sormani	Sorman	k1gMnPc1
·	·	k?
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
-	-	kIx~
<g/>
70	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mauro	Mauro	k1gNnSc1
Tassotti	Tassotť	k1gFnSc2
·	·	k?
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
-	-	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Giovanni	Giovanen	k2eAgMnPc1d1
Trapattoni	Trapatton	k1gMnPc1
·	·	k?
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
·	·	k?
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
-	-	kIx~
<g/>
95	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pietro	Pietro	k6eAd1
Paolo	Paolo	k1gNnSc1
Virdis	Virdis	k1gFnSc2
·	·	k?
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
George	Georg	k1gMnSc2
Weah	Weah	k1gInSc1
·	·	k?
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kapitáni	kapitán	k1gMnPc1
</s>
<s>
Níže	nízce	k6eAd2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
seznam	seznam	k1gInSc1
44	#num#	k4
fotbalistů	fotbalista	k1gMnPc2
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
nosili	nosit	k5eAaImAgMnP
kapitánskou	kapitánský	k2eAgFnSc4d1
pásku	páska	k1gFnSc4
od	od	k7c2
začátku	začátek	k1gInSc2
sezony	sezona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsou	být	k5eNaImIp3nP
zde	zde	k6eAd1
uvedeni	uveden	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
ji	on	k3xPp3gFnSc4
nosili	nosit	k5eAaImAgMnP
jako	jako	k8xC,k8xS
zástupci	zástupce	k1gMnPc1
kapitána	kapitán	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Kapitáni	kapitán	k1gMnPc1
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
·	·	k?
David	David	k1gMnSc1
Allison	Allison	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Herbert	Herbert	k1gMnSc1
Kilpin	Kilpin	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Gerolamo	Gerolama	k1gFnSc5
Radice	Radika	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Guido	Guido	k1gNnSc1
Moda	Mod	k1gInSc2
</s>
<s>
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Max	Max	k1gMnSc1
Tobias	Tobias	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Giuseppe	Giusepp	k1gMnSc5
Rizzi	Rizze	k1gFnSc3
</s>
<s>
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Louis	louis	k1gInSc1
Van	vana	k1gFnPc2
Hege	Heg	k1gInSc2
</s>
<s>
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Marco	Marco	k1gMnSc1
Sala	Sala	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Aldo	Aldo	k6eAd1
Cevenini	Cevenin	k2eAgMnPc5d1
</s>
<s>
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Alessandro	Alessandra	k1gFnSc5
Scarioni	Scarion	k1gMnPc5
</s>
<s>
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Cesare	Cesar	k1gInSc5
Lovati	Lovat	k1gMnPc5
</s>
<s>
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Francesco	Francesco	k6eAd1
Soldera	Soldero	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Pietro	Pietro	k1gNnSc1
Bronzini	Bronzin	k1gMnPc5
</s>
<s>
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Gianangelo	Gianangela	k1gFnSc5
Barzan	Barzan	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Abdon	Abdon	k1gInSc1
Sgarbi	Sgarb	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Alessandro	Alessandra	k1gFnSc5
Schienoni	Schienon	k1gMnPc5
</s>
<s>
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Mario	Mario	k1gMnSc1
Magnozzi	Magnozze	k1gFnSc4
</s>
<s>
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
-	-	kIx~
<g/>
34	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Carlo	Carlo	k1gNnSc4
Rigotti	Rigotť	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
-	-	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Giuseppe	Giusepp	k1gMnSc5
Bonizzoni	Bonizzoň	k1gFnSc3
</s>
<s>
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
-	-	kIx~
<g/>
38	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Luigi	Luig	k1gFnSc2
Perversi	perverse	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
39	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Giuseppe	Giusepp	k1gMnSc5
Bonizzoni	Bonizzon	k1gMnPc1
</s>
<s>
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
-	-	kIx~
<g/>
40	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Antonio	Antonio	k1gMnSc1
Bortoletti	Bortoletť	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
-	-	kIx~
<g/>
41	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Bruno	Bruno	k1gMnSc1
Arcari	Arcar	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
-	-	kIx~
<g/>
38	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Luigi	Luig	k1gFnSc2
Perversi	perverse	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
39	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Giuseppe	Giusepp	k1gMnSc5
Bonizzoni	Bonizzoň	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
-	-	kIx~
<g/>
42	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Giuseppe	Giusepp	k1gInSc5
Meazza	Meazz	k1gMnSc4
</s>
<s>
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
-	-	kIx~
<g/>
44	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Giuseppe	Giusepp	k1gInSc5
Antonini	Antonin	k2eAgMnPc5d1
</s>
<s>
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
-	-	kIx~
<g/>
45	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Paolo	Paolo	k1gNnSc1
Todeschini	Todeschin	k1gMnPc5
</s>
<s>
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
-	-	kIx~
<g/>
49	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Giuseppe	Giusepp	k1gMnSc5
Antonini	Antonin	k2eAgMnPc5d1
</s>
<s>
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Héctor	Héctor	k1gMnSc1
Puricelli	Puricell	k1gMnPc1
</s>
<s>
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
-	-	kIx~
<g/>
52	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Andrea	Andrea	k1gFnSc1
Bonomi	Bono	k1gFnPc7
</s>
<s>
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Carlo	Carlo	k1gNnSc1
Annovazzi	Annovazze	k1gFnSc3
</s>
<s>
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
-	-	kIx~
<g/>
54	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Omero	Omero	k1gNnSc1
Tognon	Tognon	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
-	-	kIx~
<g/>
56	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Gunnar	Gunnar	k1gMnSc1
Nordahl	Nordahl	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
-	-	kIx~
<g/>
61	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Nils	Nils	k1gInSc1
Liedholm	Liedholm	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Francesco	Francesco	k1gNnSc4
Zagatti	Zagatť	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
-	-	kIx~
<g/>
66	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Cesare	Cesar	k1gInSc5
Maldini	Maldin	k2eAgMnPc5d1
</s>
<s>
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
-	-	kIx~
<g/>
75	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Gianni	Giann	k1gMnPc1
Rivera	Rivero	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
-	-	kIx~
<g/>
76	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Romeo	Romeo	k1gMnSc1
Benetti	Benetť	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Gianni	Giann	k1gMnPc1
Rivera	Rivero	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Alberto	Alberta	k1gFnSc5
Bigon	Bigon	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
-	-	kIx~
<g/>
81	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Aldo	Aldo	k6eAd1
Maldera	Maldero	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
-	-	kIx~
<g/>
82	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Fulvio	Fulvio	k1gMnSc1
Collovati	Collovati	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
-	-	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Franco	Franco	k6eAd1
Baresi	Barese	k1gFnSc4
</s>
<s>
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Paolo	Paolo	k1gNnSc1
Maldini	Maldin	k2eAgMnPc1d1
</s>
<s>
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Massimo	Massima	k1gFnSc5
Ambrosini	Ambrosin	k2eAgMnPc1d1
</s>
<s>
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Riccardo	Riccardo	k1gNnSc1
Montolivo	Montoliva	k1gFnSc5
</s>
<s>
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Leonardo	Leonardo	k1gMnSc5
Bonucci	Bonucc	k1gMnSc5
</s>
<s>
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
)	)	kIx)
·	·	k?
Alessio	Alessio	k6eAd1
Romagnoli	Romagnole	k1gFnSc4
</s>
<s>
Rossoneri	Rossoneri	k6eAd1
v	v	k7c6
reprezentaci	reprezentace	k1gFnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Fotbalisté	fotbalista	k1gMnPc1
AC	AC	kA
Milán	Milán	k1gInSc1
v	v	k7c6
reprezentci	reprezentec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2019	#num#	k4
bylo	být	k5eAaImAgNnS
povoláno	povolat	k5eAaPmNgNnS
do	do	k7c2
své	svůj	k3xOyFgFnSc2
reprezentace	reprezentace	k1gFnSc2
již	již	k6eAd1
109	#num#	k4
fotbalistů	fotbalista	k1gMnPc2
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
patřilo	patřit	k5eAaImAgNnS
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
99	#num#	k4
skutečně	skutečně	k6eAd1
nastoupilo	nastoupit	k5eAaPmAgNnS
alespoň	alespoň	k9
na	na	k7c4
jeden	jeden	k4xCgInSc4
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
Juventus	Juventus	k1gInSc1
FC	FC	kA
má	mít	k5eAaImIp3nS
více	hodně	k6eAd2
hráčů	hráč	k1gMnPc2
v	v	k7c6
reprezentaci	reprezentace	k1gFnSc6
(	(	kIx(
<g/>
145	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Inter	Inter	k1gMnSc1
(	(	kIx(
<g/>
114	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
počet	počet	k1gInSc1
zápasů	zápas	k1gInPc2
za	za	k7c4
reprezentaci	reprezentace	k1gFnSc4
má	mít	k5eAaImIp3nS
Paolo	Paolo	k1gNnSc1
Maldini	Maldin	k2eAgMnPc1d1
(	(	kIx(
<g/>
126	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgMnPc1
fotbalisté	fotbalista	k1gMnPc1
Rossoneri	Rossoner	k1gFnSc2
co	co	k9
nastoupili	nastoupit	k5eAaPmAgMnP
za	za	k7c4
reprezentaci	reprezentace	k1gFnSc4
byli	být	k5eAaImAgMnP
Aldo	Aldo	k6eAd1
Cevenini	Cevenin	k2eAgMnPc1d1
a	a	k8xC
Pietro	Pietro	k1gNnSc1
Lana	lano	k1gNnSc2
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1910	#num#	k4
v	v	k7c6
Miláně	Milán	k1gInSc6
proti	proti	k7c3
Francii	Francie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápas	zápas	k1gInSc1
skončil	skončit	k5eAaPmAgInS
6	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
Lana	lano	k1gNnPc4
byl	být	k5eAaImAgMnS
autorem	autor	k1gMnSc7
prvního	první	k4xOgInSc2
gólu	gól	k1gInSc2
v	v	k7c6
historii	historie	k1gFnSc6
Itálie	Itálie	k1gFnSc2
a	a	k8xC
prvního	první	k4xOgInSc2
hattricku	hattrick	k1gInSc2
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
MS	MS	kA
</s>
<s>
Vítězové	vítěz	k1gMnPc1
ME	ME	kA
</s>
<s>
Vítězové	vítěz	k1gMnPc1
ME	ME	kA
21	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
OH	OH	kA
</s>
<s>
Vítězové	vítěz	k1gMnPc1
CA	ca	kA
</s>
<s>
Pietro	Pietro	k1gNnSc1
Arcari	Arcar	k1gFnSc2
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Angelo	Angela	k1gFnSc5
Anquilletti	Anquilletť	k1gFnSc5
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Demetrio	Demetrio	k6eAd1
Albertini	Albertin	k2eAgMnPc1d1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fabricio	Fabricio	k6eAd1
Coloccini	Coloccin	k1gMnPc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Serginho	Serginze	k6eAd1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Franco	Franco	k6eAd1
Baresi	Barese	k1gFnSc4
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Roberto	Roberta	k1gFnSc5
Rosato	Rosata	k1gFnSc5
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Francesco	Francesco	k6eAd1
Antonioli	Antoniole	k1gFnSc4
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lucas	Lucasa	k1gFnPc2
Paquetá	Paquetý	k2eAgFnSc1d1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fulvio	Fulvio	k1gMnSc1
Collovati	Collovati	k1gMnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Giovanni	Giovanen	k2eAgMnPc1d1
Lodetti	Lodett	k1gMnPc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Christian	Christian	k1gMnSc1
Panucci	Panucce	k1gFnSc4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marcel	Marcel	k1gMnSc1
Desailly	Desailla	k1gFnSc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pierino	Pierin	k2eAgNnSc1d1
Prati	Prati	k1gNnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Christian	Christian	k1gMnSc1
Panucci	Panucce	k1gFnSc4
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Roque	Roque	k1gFnSc1
Junior	junior	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gianni	Giannit	k5eAaPmRp2nS,k5eAaImRp2nS
Rivera	Rivera	k1gFnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gennaro	Gennara	k1gFnSc5
Gattuso	Gattusa	k1gFnSc5
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alberto	Alberta	k1gFnSc5
Gilardino	Gilardina	k1gFnSc5
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ruud	Ruud	k1gMnSc1
Gullit	Gullita	k1gFnPc2
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Christian	Christian	k1gMnSc1
Abbiati	Abbiat	k2eAgMnPc1d1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alessandro	Alessandra	k1gFnSc5
Nesta	Nesto	k1gNnPc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marco	Marco	k6eAd1
van	van	k1gInSc1
Basten	Bastno	k1gNnPc2
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dídac	Dídac	k1gFnSc1
Vilà	Vilà	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Filippo	Filippa	k1gFnSc5
Inzaghi	Inzagh	k1gFnSc5
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gennaro	Gennara	k1gFnSc5
Gattuso	Gattusa	k1gFnSc5
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Andrea	Andrea	k1gFnSc1
Pirlo	Pirlo	k1gNnSc4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Soupiska	soupiska	k1gFnSc1
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
</s>
<s>
Aktuální	aktuální	k2eAgFnSc1d1
soupiska	soupiska	k1gFnSc1
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Asmir	Asmir	k1gMnSc1
Begović	Begović	k1gMnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Davide	David	k1gMnSc5
Calabria	Calabrium	k1gNnPc5
</s>
<s>
4	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Ismaël	Ismaël	k1gMnSc1
Bennacer	Bennacer	k1gMnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Giacomo	Giacomo	k6eAd1
Bonaventura	Bonaventura	k1gFnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Samu	sám	k3xTgFnSc4
Castillejo	Castillejo	k6eAd1
</s>
<s>
10	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Hakan	Hakan	k1gInSc1
Çalhanoğ	Çalhanoğ	k1gInSc2
</s>
<s>
12	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Andrea	Andrea	k1gFnSc1
Conti	Conť	k1gFnSc2
</s>
<s>
13	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Alessio	Alessio	k6eAd1
Romagnoli	Romagnole	k1gFnSc3
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
17	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Rafael	Rafael	k1gMnSc1
Leã	Leã	k1gMnSc1
</s>
<s>
18	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Ante	Ant	k1gMnSc5
Rebić	Rebić	k1gMnSc5
(	(	kIx(
<g/>
na	na	k7c4
hostování	hostování	k1gNnSc4
z	z	k7c2
týmu	tým	k1gInSc2
Eintracht	Eintracht	k2eAgInSc1d1
Frankfurt	Frankfurt	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
19	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Theo	Thea	k1gFnSc5
Hernández	Hernández	k1gInSc1
</s>
<s>
20	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Lucas	Lucas	k1gMnSc1
Biglia	Biglium	k1gNnSc2
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
21	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Zlatan	Zlatan	k1gInSc1
Ibrahimovič	Ibrahimovič	k1gInSc1
</s>
<s>
22	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Mateo	Mateo	k1gMnSc1
Musacchio	Musacchio	k1gMnSc1
</s>
<s>
24	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Simon	Simon	k1gMnSc1
Kjæ	Kjæ	k1gMnSc1
</s>
<s>
33	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Rade	rad	k1gInSc5
Krunić	Krunić	k1gFnPc5
</s>
<s>
39	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Lucas	Lucas	k1gInSc1
Paquetá	Paquetý	k2eAgFnSc1d1
</s>
<s>
43	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Léo	Léo	k?
Duarte	Duart	k1gMnSc5
</s>
<s>
46	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Matteo	Matteo	k6eAd1
Gabbia	Gabbia	k1gFnSc1
</s>
<s>
56	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Alexis	Alexis	k1gFnSc1
Saelemaekers	Saelemaekersa	k1gFnPc2
</s>
<s>
79	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Franck	Franck	k1gInSc1
Kessié	Kessiá	k1gFnSc2
</s>
<s>
90	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Antonio	Antonio	k1gMnSc1
Donnarumma	Donnarummum	k1gNnSc2
</s>
<s>
93	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Diego	Diego	k1gMnSc1
Laxalt	Laxalt	k1gMnSc1
</s>
<s>
99	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Gianluigi	Gianluigi	k6eAd1
Donnarumma	Donnarumma	k1gFnSc1
</s>
<s>
Hráči	hráč	k1gMnPc1
na	na	k7c6
hostování	hostování	k1gNnSc6
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
B	B	kA
</s>
<s>
Alessandro	Alessandra	k1gFnSc5
Plizzari	Plizzar	k1gFnSc5
(	(	kIx(
<g/>
v	v	k7c4
AS	as	k1gNnSc4
Livorno	Livorno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Ricardo	Ricardo	k1gNnSc1
Rodríguez	Rodrígueza	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Alen	Alena	k1gFnPc2
Halilović	Halilović	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c4
Standard	standard	k1gInSc4
Liè	Liè	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Ú	Ú	kA
</s>
<s>
Suso	Suso	k1gNnSc1
(	(	kIx(
<g/>
v	v	k7c6
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
Ú	Ú	kA
</s>
<s>
André	André	k1gMnSc1
Silva	Silva	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c4
Eintracht	Eintracht	k2eAgInSc4d1
Frankfurt	Frankfurt	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Sponzoři	sponzor	k1gMnPc1
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
Výrobce	výrobce	k1gMnSc1
dresů	dres	k1gInPc2
</s>
<s>
Sponzor	sponzor	k1gMnSc1
na	na	k7c6
dresu	dres	k1gInSc6
</s>
<s>
Značka	značka	k1gFnSc1
</s>
<s>
Firma	firma	k1gFnSc1
</s>
<s>
1981	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
</s>
<s>
Linea	linea	k1gFnSc1
Milan	Milan	k1gMnSc1
</s>
<s>
Pooh	Pooh	k1gInSc1
Jeans	Jeansa	k1gFnPc2
</s>
<s>
Italiana	Italiana	k1gFnSc1
Manifatture	Manifattur	k1gMnSc5
S.	S.	kA
<g/>
p.	p.	k?
<g/>
A.	A.	kA
</s>
<s>
1982	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
</s>
<s>
NR	NR	kA
</s>
<s>
Hitachi	Hitachi	k6eAd1
</s>
<s>
Hitachi	Hitach	k1gMnSc5
Europe	Europ	k1gMnSc5
Srl	Srl	k1gMnSc5
</s>
<s>
1983	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
</s>
<s>
Cuore	Cuor	k1gMnSc5
</s>
<s>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
</s>
<s>
Rolly	Rolla	k1gFnPc1
Go	Go	k1gFnSc2
</s>
<s>
Oscar	Oscar	k1gInSc1
Mondadori	Mondador	k1gFnSc2
</s>
<s>
Arnoldo	Arnolda	k1gMnSc5
Mondadori	Mondador	k1gMnSc5
Editore	editor	k1gMnSc5
S.	S.	kA
<g/>
p.	p.	k?
<g/>
A.	A.	kA
</s>
<s>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
</s>
<s>
Gianni	Giann	k1gMnPc1
Rivera	Rivero	k1gNnSc2
</s>
<s>
Fotorex	Fotorex	k1gInSc1
U-Bix	U-Bix	k1gInSc1
</s>
<s>
Olivetti	Olivetť	k1gFnPc1
S.	S.	kA
<g/>
p.	p.	k?
<g/>
A.	A.	kA
</s>
<s>
1986	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
</s>
<s>
Kappa	kappa	k1gNnSc1
</s>
<s>
1987	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
</s>
<s>
Mediolanum	Mediolanum	k1gInSc1
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
</s>
<s>
Adidas	Adidas	k1gMnSc1
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
</s>
<s>
Motta	motto	k1gNnPc1
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
</s>
<s>
Lotto	Lotto	k1gNnSc1
</s>
<s>
1994	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
</s>
<s>
Opel	opel	k1gInSc1
</s>
<s>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
</s>
<s>
Adidas	Adidas	k1gMnSc1
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
</s>
<s>
Bwin	Bwin	k1gMnSc1
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
</s>
<s>
Fly	Fly	k?
Emirates	Emirates	k1gInSc1
</s>
<s>
The	The	k?
Emirates	Emirates	k1gMnSc1
Group	Group	k1gMnSc1
</s>
<s>
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
????	????	k?
</s>
<s>
Puma	puma	k1gFnSc1
</s>
<s>
Fly	Fly	k?
Emirates	Emirates	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Associazione	Associazion	k1gInSc5
Calcio	Calcio	k1gNnSc1
Milan	Milan	k1gMnSc1
na	na	k7c6
italské	italský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
https://www.milannews.it/news/il-papa-del-milan-33884//	https://www.milannews.it/news/il-papa-del-milan-33884//	k4
<g/>
↑	↑	k?
http://tdifh.blogspot.com/2012/03/20-march-1991-night-lights-went-out-in.html	http://tdifh.blogspot.com/2012/03/20-march-1991-night-lights-went-out-in.html	k1gMnSc1
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/pohary/jak-olympique-z-trunu-padl-na-fotbalove-dno.A060914_070536_fot_pohary_ot	https://www.idnes.cz/fotbal/pohary/jak-olympique-z-trunu-padl-na-fotbalove-dno.A060914_070536_fot_pohary_ot	k1gMnSc1
<g/>
↑	↑	k?
https://sport.ceskatelevize.cz/clanek/fotbal/buffon-prekonal-rekord-a-juventus-ovladl-derby-vyhrala-i-neapol/5bdb80f70d663b6fe8a5c46f	https://sport.ceskatelevize.cz/clanek/fotbal/buffon-prekonal-rekord-a-juventus-ovladl-derby-vyhrala-i-neapol/5bdb80f70d663b6fe8a5c46f	k1gMnSc1
<g/>
↑	↑	k?
https://www.tyden.cz/rubriky/sport/fotbal/zahranicni-souteze/megaprestup-se-blizi-kaka-je-na-prodej-priznal-ac-milan_123043.html	https://www.tyden.cz/rubriky/sport/fotbal/zahranicni-souteze/megaprestup-se-blizi-kaka-je-na-prodej-priznal-ac-milan_123043.html	k1gMnSc1
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/zahranici/berslusconi-prodal-ac-milan-cinske-spolecnosti-majitele-meni-i-west-bromwich.A160805_164845_fot_zahranici_min	https://www.idnes.cz/fotbal/zahranici/berslusconi-prodal-ac-milan-cinske-spolecnosti-majitele-meni-i-west-bromwich.A160805_164845_fot_zahranici_min	k1gInSc1
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/zahranici/utraceni-cinskych-majitelu-v-ac-milan.A170717_135335_fot_zahranici_jic	https://www.idnes.cz/fotbal/zahranici/utraceni-cinskych-majitelu-v-ac-milan.A170717_135335_fot_zahranici_jice	k1gInPc2
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/zahranici/ac-milan-evropska-liga-trest-arbitraz-cas.A180720_150302_fot_zahranici_rou	https://www.idnes.cz/fotbal/zahranici/ac-milan-evropska-liga-trest-arbitraz-cas.A180720_150302_fot_zahranici_ra	k1gMnSc7
<g/>
↑	↑	k?
https://www.efotbal.cz/clanek-192246-AC-Milan-je-opet-v-novych-rukou-Kdo-zustane-koho-se-muze-tykat-odchod-a-co-mozny-rozpocet-na-posily.html	https://www.efotbal.cz/clanek-192246-AC-Milan-je-opet-v-novych-rukou-Kdo-zustane-koho-se-muze-tykat-odchod-a-co-mozny-rozpocet-na-posily.html	k1gMnSc1
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/pohary/ac-milan-evropska-liga-poruseni-pravidel-financniho-fair-play-prisel.A190628_142233_fot_pohary_par	https://www.idnes.cz/fotbal/pohary/ac-milan-evropska-liga-poruseni-pravidel-financniho-fair-play-prisel.A190628_142233_fot_pohary_par	k1gMnSc1
<g/>
↑	↑	k?
Skandál	skandál	k1gInSc1
v	v	k7c6
italské	italský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
1980	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gMnSc1
FIFA	FIFA	kA
Clubs	Clubs	k1gInSc1
of	of	k?
the	the	k?
20	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Korupční	korupční	k2eAgInSc1d1
skandál	skandál	k1gInSc1
v	v	k7c6
italské	italský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://www.youtube.com/watch?v=OamOrfzbg-o	https://www.youtube.com/watch?v=OamOrfzbg-o	k1gNnSc4
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/giuseppe-meazza-peppino-67476/	https://www.eurofotbal.cz/clanky/giuseppe-meazza-peppino-67476/	k4
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/zahranici/fotbalovy-ac-milan-se-presune-na-novy-stadion.A150204_122133_fot_zahranici_dc	https://www.idnes.cz/fotbal/zahranici/fotbalovy-ac-milan-se-presune-na-novy-stadion.A150204_122133_fot_zahranici_dc	k1gInSc1
<g/>
↑	↑	k?
https://www.efotbal.cz/clanek-200206-Radost-z-noveho-stadionu-byla-predcasna-Proc-hlava-Milana-zastavila-projekt-za-700-milionu-eur.html	https://www.efotbal.cz/clanek-200206-Radost-z-noveho-stadionu-byla-predcasna-Proc-hlava-Milana-zastavila-projekt-za-700-milionu-eur.html	k1gInSc1
<g/>
↑	↑	k?
https://isport.blesk.cz/clanek/fotbal-zahranici-italie-serie-a/369363/san-siro-ma-jit-k-zemi-co-ho-nahradi-derby-architektu-jde-do-finale.html	https://isport.blesk.cz/clanek/fotbal-zahranici-italie-serie-a/369363/san-siro-ma-jit-k-zemi-co-ho-nahradi-derby-architektu-jde-do-finale.html	k1gInSc1
<g/>
↑	↑	k?
https://www.sport.cz/fotbal/liga-mistru/clanek/191718-milanello-luno-uspechu-ac-milan-kde-fotbalove-hvezdy-metaji-sisky.html	https://www.sport.cz/fotbal/liga-mistru/clanek/191718-milanello-luno-uspechu-ac-milan-kde-fotbalove-hvezdy-metaji-sisky.html	k1gInSc1
<g/>
↑	↑	k?
https://www.e15.cz/byznys/obchod-a-sluzby/berlusconi-prodal-ac-milan-za-740-milionu-eur-cinanum-1331262	https://www.e15.cz/byznys/obchod-a-sluzby/berlusconi-prodal-ac-milan-za-740-milionu-eur-cinanum-1331262	k4
<g/>
↑	↑	k?
https://www.sport7.sk/18713/silvio-berlusconi-sa-stal-cestnym-prezidentom-ac-milano	https://www.sport7.sk/18713/silvio-berlusconi-sa-stal-cestnym-prezidentom-ac-milana	k1gFnSc5
<g/>
↑	↑	k?
https://www.acmilan.com/en/club/hall-of-fame-list	https://www.acmilan.com/en/club/hall-of-fame-list	k1gInSc1
<g/>
↑	↑	k?
https://www.figc.it/it/nazionali/nazionali-in-cifre/convocati-di-una-societ%C3%A0/?squadraId=12&	https://www.figc.it/it/nazionali/nazionali-in-cifre/convocati-di-una-societ%C3%A0/?squadraId=12&	k?
<g/>
↑	↑	k?
https://web.archive.org/web/20160303040314/http://www.figc.it/nazionali/TabellinoGara?squadra=1&	https://web.archive.org/web/20160303040314/http://www.figc.it/nazionali/TabellinoGara?squadra=1&	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
AC	AC	kA
Milán	Milán	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Profil	profil	k1gInSc1
na	na	k7c4
transfermarkt	transfermarkt	k1gInSc4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
stadionu	stadion	k1gInSc6
-	-	kIx~
FotbaloveStadiony	FotbaloveStadion	k1gInPc7
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
České	český	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
AC	AC	kA
Milán	Milán	k1gInSc1
–	–	k?
jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
Klubové	klubový	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1997	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Serie	serie	k1gFnSc1
A	A	kA
–	–	k?
italská	italský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
ročníky	ročník	k1gInPc1
<g/>
)	)	kIx)
Kluby	klub	k1gInPc1
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Atalanta	Atalant	k1gMnSc2
BC	BC	kA
•	•	k?
Cagliari	Cagliari	k1gNnSc1
Calcio	Calcio	k1gMnSc1
•	•	k?
FC	FC	kA
Crotone	Croton	k1gInSc5
•	•	k?
Bologna	Bologna	k1gFnSc1
FC	FC	kA
1909	#num#	k4
•	•	k?
Hellas	Hellas	k1gFnSc1
Verona	Verona	k1gFnSc1
FC	FC	kA
•	•	k?
Benevento	Benevento	k1gNnSc1
Calcio	Calcio	k1gMnSc1
•	•	k?
ACF	ACF	kA
Fiorentina	Fiorentin	k2eAgMnSc4d1
•	•	k?
Janov	Janov	k1gInSc1
CFC	CFC	kA
•	•	k?
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
SS	SS	kA
Lazio	Lazio	k1gMnSc1
•	•	k?
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
SSC	SSC	kA
Neapol	Neapol	k1gFnSc1
•	•	k?
Parma	Parma	k1gFnSc1
Calcio	Calcio	k6eAd1
1913	#num#	k4
•	•	k?
AS	as	k1gInSc1
Řím	Řím	k1gInSc1
•	•	k?
UC	UC	kA
Sampdoria	Sampdorium	k1gNnSc2
•	•	k?
US	US	kA
Sassuolo	Sassuola	k1gFnSc5
Calcio	Calcio	k6eAd1
•	•	k?
Turín	Turín	k1gInSc1
FC	FC	kA
•	•	k?
Udinese	Udinese	k1gFnSc2
Calcio	Calcio	k1gMnSc1
•	•	k?
Spezia	Spezia	k1gFnSc1
Calcio	Calcio	k6eAd1
Stadiony	stadion	k1gInPc7
</s>
<s>
Atalanta	Atalant	k1gMnSc2
Bergamo	Bergamo	k1gNnSc1
•	•	k?
Cagliari	Cagliari	k1gNnSc1
Calcio	Calcio	k1gMnSc1
•	•	k?
Crotone	Croton	k1gInSc5
•	•	k?
Bologna	Bologna	k1gFnSc1
•	•	k?
Verona	Verona	k1gFnSc1
•	•	k?
Benevento	Benevento	k1gNnSc1
•	•	k?
Fiorentina	Fiorentina	k1gFnSc1
•	•	k?
CFC	CFC	kA
Janov	Janov	k1gInSc1
•	•	k?
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
Juventus	Juventus	k1gInSc1
Turín	Turín	k1gInSc1
•	•	k?
Lazio	Lazio	k6eAd1
Řím	Řím	k1gInSc1
•	•	k?
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
Neapol	Neapol	k1gFnSc1
•	•	k?
Parma	Parma	k1gFnSc1
•	•	k?
AS	as	k9
Řím	Řím	k1gInSc1
•	•	k?
Sampdoria	Sampdorium	k1gNnSc2
Janov	Janov	k1gInSc1
•	•	k?
Sassuolo	Sassuola	k1gFnSc5
Calcio	Calcio	k6eAd1
•	•	k?
Turín	Turín	k1gInSc1
•	•	k?
Udine	Udin	k1gInSc5
•	•	k?
Spezia	Spezium	k1gNnSc2
Campionato	Campionat	k2eAgNnSc1d1
Nazionale	Nazionale	k1gFnPc4
di	di	k?
Football	Football	k1gMnSc1
</s>
<s>
1898	#num#	k4
•	•	k?
1899	#num#	k4
•	•	k?
1900	#num#	k4
Campionato	Campionat	k2eAgNnSc1d1
Italiano	Italiana	k1gFnSc5
di	di	k?
Football	Footballa	k1gFnPc2
</s>
<s>
1901	#num#	k4
•	•	k?
1902	#num#	k4
•	•	k?
1903	#num#	k4
Prima	prima	k1gFnSc1
Categoria	Categorium	k1gNnSc2
</s>
<s>
1904	#num#	k4
•	•	k?
1905	#num#	k4
•	•	k?
1906	#num#	k4
•	•	k?
1907	#num#	k4
•	•	k?
1908	#num#	k4
•	•	k?
1909	#num#	k4
•	•	k?
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
1914	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
•	•	k?
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
Prima	primo	k1gNnSc2
Divisione	Division	k1gInSc5
</s>
<s>
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
•	•	k?
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
•	•	k?
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
•	•	k?
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
•	•	k?
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
Divisione	Division	k1gInSc5
Nazionale	Nazionale	k1gMnSc6
</s>
<s>
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
•	•	k?
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
•	•	k?
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
Serie	serie	k1gFnSc2
A	a	k8xC
</s>
<s>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
1937	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
•	•	k?
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
•	•	k?
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
•	•	k?
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
•	•	k?
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
•	•	k?
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
•	•	k?
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
•	•	k?
1956	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
57	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
<g/>
:	:	kIx,
Italský	italský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
•	•	k?
Historická	historický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
Serie	serie	k1gFnSc2
A	a	k9
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Supercoppa	Supercoppa	k1gFnSc1
italiana	italiana	k1gFnSc1
</s>
<s>
1988	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1989	#num#	k4
Inter	Intero	k1gNnPc2
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
SSC	SSC	kA
Neapol	Neapol	k1gFnSc1
•	•	k?
1991	#num#	k4
UC	UC	kA
Sampdoria	Sampdorium	k1gNnSc2
•	•	k?
1992	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1993	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1994	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1995	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1996	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1997	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1998	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1999	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2000	#num#	k4
SS	SS	kA
Lazio	Lazio	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2001	#num#	k4
AS	as	k9
Řím	Řím	k1gInSc1
•	•	k?
2002	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
2003	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
2004	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2005	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
2006	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2007	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2009	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2010	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2011	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2012	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
2013	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
2014	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
2015	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
2016	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
2017	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
2018	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
2019	#num#	k4
SS	SS	kA
Lazio	Lazio	k6eAd1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1972	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1973	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1975	#num#	k4
FK	FK	kA
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
•	•	k?
1976	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1977	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
1978	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1979	#num#	k4
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gInSc4
FC	FC	kA
•	•	k?
1980	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
1982	#num#	k4
Aston	Aston	k1gNnSc4
Villa	Villo	k1gNnSc2
FC	FC	kA
•	•	k?
1983	#num#	k4
Aberdeen	Aberdeen	k1gInSc1
FC	FC	kA
•	•	k?
1984	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
KV	KV	kA
Mechelen	Mechelna	k1gFnPc2
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
1992	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1993	#num#	k4
AC	AC	kA
Parma	Parma	k1gFnSc1
•	•	k?
1994	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1995	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1998	#num#	k4
Chelsea	Chelseus	k1gMnSc2
FC	FC	kA
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1999	#num#	k4
Lazio	Lazio	k6eAd1
Řím	Řím	k1gInSc1
•	•	k?
2000	#num#	k4
Galatasaray	Galatasaraa	k1gMnSc2
SK	Sk	kA
•	•	k?
2001	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2004	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
2005	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2006	#num#	k4
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Zenit	zenit	k1gInSc1
Petrohrad	Petrohrad	k1gInSc4
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2013	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2021	#num#	k4
•	•	k?
2022	#num#	k4
•	•	k?
2023	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
-	-	kIx~
předchůdce	předchůdce	k1gMnSc2
</s>
<s>
1960	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1961	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1962	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1963	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1964	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1965	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1966	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1967	#num#	k4
Racing	Racing	k1gInSc1
Club	club	k1gInSc4
•	•	k?
1968	#num#	k4
Estudiantes	Estudiantes	k1gInSc1
de	de	k?
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
•	•	k?
1969	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1970	#num#	k4
Feyenoord	Feyenoord	k1gInSc1
•	•	k?
1971	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1972	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1973	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
•	•	k?
1974	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
1975	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1976	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
1977	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
1978	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1979	#num#	k4
Club	club	k1gInSc1
Olimpia	Olimpium	k1gNnSc2
•	•	k?
1980	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1981	#num#	k4
Flamengo	flamengo	k1gNnSc4
•	•	k?
1982	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1983	#num#	k4
Grê	Grê	k1gNnSc4
•	•	k?
1984	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1985	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
CA	ca	kA
River	Rivra	k1gFnPc2
Plate	plat	k1gInSc5
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
FK	FK	kA
Crvena	Crven	k2eAgFnSc1d1
zvezda	zvezda	k1gFnSc1
•	•	k?
1992	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1993	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1994	#num#	k4
CA	ca	kA
Vélez	Vélez	k1gInSc1
Sarsfield	Sarsfield	k1gInSc1
•	•	k?
1995	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc1
•	•	k?
1998	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1999	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2000	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2001	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2004	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
Mistrovství	mistrovství	k1gNnPc2
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
</s>
<s>
2000	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2001-2004	2001-2004	k4
•	•	k?
2005	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
2006	#num#	k4
Sport	sport	k1gInSc1
Club	club	k1gInSc4
Internacional	Internacional	k1gFnSc2
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2013	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7536347-1	7536347-1	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0551	#num#	k4
3462	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79099445	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
158266146	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79099445	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Itálie	Itálie	k1gFnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
</s>
