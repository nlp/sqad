<p>
<s>
Grigore	Grigor	k1gMnSc5	Grigor
Ureche	Urechus	k1gMnSc5	Urechus
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1590	[number]	k4	1590
<g/>
–	–	k?	–
<g/>
1647	[number]	k4	1647
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
moldavský	moldavský	k2eAgMnSc1d1	moldavský
kronikář	kronikář	k1gMnSc1	kronikář
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
šlechtické	šlechtický	k2eAgFnSc2d1	šlechtická
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc2d1	žijící
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
zastával	zastávat	k5eAaImAgMnS	zastávat
různé	různý	k2eAgInPc4d1	různý
vysoké	vysoký	k2eAgInPc4d1	vysoký
úřady	úřad	k1gInPc4	úřad
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
spis	spis	k1gInSc1	spis
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Letopiseț	Letopiseț	k1gFnSc1	Letopiseț
Ț	Ț	k?	Ț
Moldovei	Moldove	k1gFnSc2	Moldove
(	(	kIx(	(
<g/>
Kroniky	kronika	k1gFnSc2	kronika
země	zem	k1gFnSc2	zem
moldavské	moldavský	k2eAgFnSc2d1	Moldavská
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
časové	časový	k2eAgNnSc4d1	časové
období	období	k1gNnSc4	období
asi	asi	k9	asi
1359	[number]	k4	1359
<g/>
-	-	kIx~	-
<g/>
1594	[number]	k4	1594
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
moldavsky	moldavsky	k6eAd1	moldavsky
píšících	píšící	k2eAgMnPc2d1	píšící
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
moldavští	moldavský	k2eAgMnPc1d1	moldavský
a	a	k8xC	a
rumunští	rumunský	k2eAgMnPc1d1	rumunský
autoři	autor	k1gMnPc1	autor
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dávali	dávat	k5eAaImAgMnP	dávat
přednost	přednost	k1gFnSc4	přednost
staroslověnštině	staroslověnština	k1gFnSc3	staroslověnština
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zakladatelem	zakladatel	k1gMnSc7	zakladatel
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
římském	římský	k2eAgInSc6d1	římský
původu	původ	k1gInSc6	původ
Rumunů	Rumun	k1gMnPc2	Rumun
a	a	k8xC	a
Moldavanů	Moldavan	k1gMnPc2	Moldavan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Grigore	Grigor	k1gInSc5	Grigor
Ureche	Ureche	k1gNnPc2	Ureche
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
