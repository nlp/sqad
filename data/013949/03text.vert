<s>
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
(	(	kIx(
<g/>
lední	lední	k2eAgMnSc1d1
hokejista	hokejista	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
HolíkOsobní	HolíkOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1942	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Německý	německý	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
,	,	kIx,
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2015	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
72	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Česko	Česko	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Československo	Československo	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
182	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
83	#num#	k4
kg	kg	kA
Držení	držení	k1gNnSc1
hole	hole	k6eAd1
</s>
<s>
vpravo	vpravo	k6eAd1
Děti	dítě	k1gFnPc1
</s>
<s>
Bobby	Bobba	k1gFnPc1
HolíkAndrea	HolíkAndrea	k1gFnSc1
Holíková	Holíková	k1gFnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
František	František	k1gMnSc1
Musil	Musil	k1gMnSc1
(	(	kIx(
<g/>
zeť	zeť	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
(	(	kIx(
<g/>
synovec	synovec	k1gMnSc1
<g/>
)	)	kIx)
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Současný	současný	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
ukončil	ukončit	k5eAaPmAgMnS
kariéru	kariéra	k1gFnSc4
Pozice	pozice	k1gFnSc2
</s>
<s>
centr	centr	k1gInSc1
Předchozí	předchozí	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
na	na	k7c6
ZOH	ZOH	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
1972	#num#	k4
Sapporo	Sappora	k1gFnSc5
</s>
<s>
ČSSR	ČSSR	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
1965	#num#	k4
</s>
<s>
ČSSR	ČSSR	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
1966	#num#	k4
</s>
<s>
ČSSR	ČSSR	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
1969	#num#	k4
</s>
<s>
ČSSR	ČSSR	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
1970	#num#	k4
</s>
<s>
ČSSR	ČSSR	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
1972	#num#	k4
</s>
<s>
ČSSR	ČSSR	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
1973	#num#	k4
</s>
<s>
ČSSR	ČSSR	kA
</s>
<s>
Československá	československý	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
1962	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
1964	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
1966	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
1975	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
1976	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
1977	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
-	-	kIx~
trenér	trenér	k1gMnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
-	-	kIx~
trenér	trenér	k1gMnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
-	-	kIx~
trenér	trenér	k1gMnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
-	-	kIx~
trenér	trenér	k1gMnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
-	-	kIx~
trenér	trenér	k1gMnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
1988	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
-	-	kIx~
trenér	trenér	k1gMnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
-	-	kIx~
trenér	trenér	k1gMnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MSJ	MSJ	kA
2000	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
20	#num#	k4
trenér	trenér	k1gMnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MSJ	MSJ	kA
2001	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
20	#num#	k4
trenér	trenér	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1942	#num#	k4
Německý	německý	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
,	,	kIx,
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2015	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
československý	československý	k2eAgMnSc1d1
hokejista	hokejista	k1gMnSc1
(	(	kIx(
<g/>
střední	střední	k1gMnSc1
útočník	útočník	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
trenér	trenér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
spoluhráči	spoluhráč	k1gMnPc7
byl	být	k5eAaImAgMnS
přezdívaný	přezdívaný	k2eAgMnSc1d1
„	„	k?
<g/>
Starej	starat	k5eAaImRp2nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byl	být	k5eAaImAgMnS
uveden	uvést	k5eAaPmNgMnS
do	do	k7c2
Síně	síň	k1gFnSc2
slávy	sláva	k1gFnSc2
českého	český	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1961	#num#	k4
<g/>
–	–	kIx~
<g/>
1979	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k8
hráč	hráč	k1gMnSc1
armádního	armádní	k2eAgInSc2d1
klubu	klub	k1gInSc2
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yRgFnSc4
nastoupil	nastoupit	k5eAaPmAgInS
k	k	k7c3
602	#num#	k4c3
utkáním	utkání	k1gNnPc3
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
nasbíral	nasbírat	k5eAaPmAgMnS
641	#num#	k4
kanadských	kanadský	k2eAgMnPc2d1
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
266	#num#	k4
<g/>
+	+	kIx~
<g/>
375	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tímto	tento	k3xDgInSc7
klubem	klub	k1gInSc7
vybojoval	vybojovat	k5eAaPmAgInS
sedm	sedm	k4xCc4
titulů	titul	k1gInPc2
mistra	mistr	k1gMnSc4
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
čtyři	čtyři	k4xCgNnPc4
ligová	ligový	k2eAgNnPc4d1
prvenství	prvenství	k1gNnPc4
později	pozdě	k6eAd2
přidal	přidat	k5eAaPmAgMnS
jako	jako	k9
trenér	trenér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravidelně	pravidelně	k6eAd1
startoval	startovat	k5eAaBmAgInS
za	za	k7c4
československou	československý	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
na	na	k7c6
světových	světový	k2eAgInPc6d1
šampionátech	šampionát	k1gInPc6
získal	získat	k5eAaPmAgInS
šest	šest	k4xCc4
medailí	medaile	k1gFnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
zlaté	zlatá	k1gFnSc2
z	z	k7c2
MS	MS	kA
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
Zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
1972	#num#	k4
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
si	se	k3xPyFc3
československé	československý	k2eAgNnSc4d1
družstvo	družstvo	k1gNnSc4
odvezlo	odvézt	k5eAaPmAgNnS
bronz	bronz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
Klubu	klub	k1gInSc2
hokejových	hokejový	k2eAgMnPc2d1
střelců	střelec	k1gMnPc2
deníku	deník	k1gInSc2
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
přivedl	přivést	k5eAaPmAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
2000	#num#	k4
a	a	k8xC
2001	#num#	k4
českou	český	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
do	do	k7c2
20	#num#	k4
let	léto	k1gNnPc2
k	k	k7c3
titulům	titul	k1gInPc3
mistrů	mistr	k1gMnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byl	být	k5eAaImAgInS
vyšetřován	vyšetřovat	k5eAaImNgInS
policií	policie	k1gFnSc7
kvůli	kvůli	k7c3
údajné	údajný	k2eAgFnSc3d1
korupci	korupce	k1gFnSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
měl	mít	k5eAaImAgInS
vyžadovat	vyžadovat	k5eAaImF
jako	jako	k9
trenér	trenér	k1gMnSc1
reprezentačního	reprezentační	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
do	do	k7c2
20	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
50	#num#	k4
000	#num#	k4
korun	koruna	k1gFnPc2
za	za	k7c4
zařazení	zařazení	k1gNnSc4
obránce	obránce	k1gMnSc2
Jana	Jan	k1gMnSc2
Platila	platit	k5eAaImAgFnS
do	do	k7c2
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
Sport	sport	k1gInSc4
také	také	k9
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
získal	získat	k5eAaPmAgInS
nahrávku	nahrávka	k1gFnSc4
inkriminovaného	inkriminovaný	k2eAgInSc2d1
rozhovoru	rozhovor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Policie	policie	k1gFnSc1
následně	následně	k6eAd1
případ	případ	k1gInSc4
odložila	odložit	k5eAaPmAgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
nezjistila	zjistit	k5eNaPmAgFnS
skutečnosti	skutečnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
nasvědčovaly	nasvědčovat	k5eAaImAgFnP
spáchání	spáchání	k1gNnSc4
trestného	trestný	k2eAgInSc2d1
činu	čin	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
2012	#num#	k4
převzal	převzít	k5eAaPmAgMnS
v	v	k7c6
Praze	Praha	k1gFnSc6
od	od	k7c2
Českého	český	k2eAgInSc2d1
klubu	klub	k1gInSc2
fair	fair	k6eAd1
play	play	k0
Hlavní	hlavní	k2eAgNnSc1d1
cenu	cena	k1gFnSc4
za	za	k7c4
celoživotní	celoživotní	k2eAgInSc4d1
postoj	postoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
trpěl	trpět	k5eAaImAgInS
gangrénou	gangréna	k1gFnSc7
<g/>
,	,	kIx,
přišel	přijít	k5eAaPmAgMnS
kvůli	kvůli	k7c3
ní	on	k3xPp3gFnSc7
o	o	k7c4
část	část	k1gFnSc4
nohy	noha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zemřel	zemřít	k5eAaPmAgMnS
po	po	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
nemoci	nemoc	k1gFnSc6
ve	v	k7c6
věku	věk	k1gInSc6
72	#num#	k4
let	léto	k1gNnPc2
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7
bratrem	bratr	k1gMnSc7
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
hokejista	hokejista	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
syna	syn	k1gMnSc4
Roberta	Robert	k1gMnSc4
(	(	kIx(
<g/>
rovněž	rovněž	k9
bývalého	bývalý	k2eAgMnSc4d1
hokejistu	hokejista	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
dceru	dcera	k1gFnSc4
Andreu	Andrea	k1gFnSc4
(	(	kIx(
<g/>
bývalou	bývalý	k2eAgFnSc4d1
tenistku	tenistka	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holík	Holík	k1gMnSc1
požadoval	požadovat	k5eAaImAgMnS
jako	jako	k9
trenér	trenér	k1gMnSc1
hokejové	hokejový	k2eAgFnSc2d1
dvacítky	dvacítka	k1gFnSc2
úplatek	úplatek	k1gInSc4
za	za	k7c4
nominaci	nominace	k1gFnSc4
hráče	hráč	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idnes	Idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2010-09-11	2010-09-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MICHAEL	Michael	k1gMnSc1
<g/>
,	,	kIx,
Mareš	Mareš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holíkovi	Holík	k1gMnSc3
trest	trest	k1gInSc4
za	za	k7c4
úplatek	úplatek	k1gInSc4
už	už	k6eAd1
nehrozí	hrozit	k5eNaImIp3nS
<g/>
,	,	kIx,
policie	policie	k1gFnSc1
zastavila	zastavit	k5eAaPmAgFnS
vyšetřování	vyšetřování	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-01-03	2011-01-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odešla	odejít	k5eAaPmAgFnS
hokejová	hokejový	k2eAgFnSc1d1
legenda	legenda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
72	#num#	k4
letech	léto	k1gNnPc6
zemřel	zemřít	k5eAaPmAgMnS
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-04-17	2015-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hokejové	hokejový	k2eAgFnSc6d1
legendě	legenda	k1gFnSc6
bylo	být	k5eAaImAgNnS
72	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-04-17	2015-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
je	být	k5eAaImIp3nS
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
:	:	kIx,
91	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
:	:	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
federální	federální	k2eAgInPc4d1
orgány	orgán	k1gInPc4
ČSFR	ČSFR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
–	–	k?
<g/>
M.	M.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
636	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901103	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
287	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
=	=	kIx~
Who	Who	k1gMnPc1
is	is	k?
who	who	k?
:	:	kIx,
osobnosti	osobnost	k1gFnPc4
české	český	k2eAgFnSc2d1
současnosti	současnost	k1gFnSc2
:	:	kIx,
5000	#num#	k4
životopisů	životopis	k1gInPc2
/	/	kIx~
(	(	kIx(
<g/>
Michael	Michael	k1gMnSc1
Třeštík	Třeštík	k1gMnSc1
editor	editor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
Kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
je	být	k5eAaImIp3nS
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
775	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902586	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
203	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
-	-	kIx~
Česko	Česko	k1gNnSc1
:	:	kIx,
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
823	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7360	#num#	k4
<g/>
-	-	kIx~
<g/>
796	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
232	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnPc2
:	:	kIx,
I.	I.	kA
díl	díl	k1gInSc1
:	:	kIx,
A	a	k9
<g/>
–	–	k?
<g/>
J.	J.	kA
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
;	;	kIx,
Petr	Petr	k1gMnSc1
Meissner	Meissner	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
634	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
245	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
483	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VRBECKÝ	VRBECKÝ	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
1956	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
:	:	kIx,
Půl	půl	k6eAd1
století	století	k1gNnSc1
legendy	legenda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihlava	Jihlava	k1gFnSc1
<g/>
:	:	kIx,
Parola	parola	k1gFnSc1
2006	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Olympijští	olympijský	k2eAgMnPc1d1
medailisté	medailista	k1gMnPc1
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
–	–	k?
turnaj	turnaj	k1gInSc1
1972	#num#	k4
v	v	k7c6
Sapporu	Sappor	k1gInSc6
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Blinov	Blinov	k1gInSc1
•	•	k?
Gennadij	Gennadij	k1gFnSc1
Cygankov	Cygankov	k1gInSc1
•	•	k?
Anatolij	Anatolij	k1gFnSc1
Firsov	Firsov	k1gInSc1
•	•	k?
Vitalij	Vitalij	k1gFnSc1
Davydov	Davydov	k1gInSc1
•	•	k?
Valerij	Valerij	k1gFnSc1
Charlamov	Charlamovo	k1gNnPc2
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Jakušev	Jakušev	k1gMnSc1
•	•	k?
Viktor	Viktor	k1gMnSc1
Kuzkin	Kuzkin	k1gMnSc1
•	•	k?
Vladimir	Vladimir	k1gMnSc1
Ljutčenko	Ljutčenka	k1gFnSc5
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Malcev	Malcva	k1gFnPc2
•	•	k?
Boris	Boris	k1gMnSc1
Michajlov	Michajlov	k1gInSc1
•	•	k?
Jevgenij	Jevgenij	k1gFnSc1
Mišakov	Mišakov	k1gInSc1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Paškov	Paškov	k1gInSc1
•	•	k?
Vladimir	Vladimir	k1gInSc1
Petrov	Petrov	k1gInSc1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Ragulin	Ragulin	k2eAgMnSc1d1
•	•	k?
Igor	Igor	k1gMnSc1
Romiševskij	Romiševskij	k1gMnSc1
•	•	k?
Vladimir	Vladimir	k1gInSc1
Šadrin	Šadrin	k1gInSc1
•	•	k?
Vladislav	Vladislav	k1gMnSc1
Treťjak	Treťjak	k1gMnSc1
•	•	k?
Valerij	Valerij	k1gMnSc1
Vasiljev	Vasiljev	k1gMnSc1
•	•	k?
Vladimir	Vladimir	k1gInSc1
Vikulov	Vikulov	k1gInSc1
•	•	k?
Jevgenij	Jevgenij	k1gMnSc1
Zimin	Zimin	k1gMnSc1
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Arkadij	Arkadij	k1gMnSc1
Černyšev	Černyšev	k1gMnSc1
•	•	k?
Anatolij	Anatolij	k1gMnSc4
Tarasov	Tarasov	k1gInSc1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc7
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Kevin	Kevin	k1gMnSc1
Ahearn	Ahearn	k1gMnSc1
•	•	k?
Larry	Larra	k1gFnSc2
Bader	Bader	k1gMnSc1
•	•	k?
Henry	Henry	k1gMnSc1
Boucha	Boucha	k1gMnSc1
•	•	k?
Charles	Charles	k1gMnSc1
Brown	Brown	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Curran	Curran	k1gInSc1
•	•	k?
Robbie	Robbie	k1gFnSc2
Ftorek	Ftorek	k1gMnSc1
•	•	k?
Mark	Mark	k1gMnSc1
Howe	How	k1gFnSc2
•	•	k?
Keith	Keith	k1gInSc1
Christiansen	Christiansen	k1gInSc1
•	•	k?
Stuart	Stuart	k1gInSc1
Irving	Irving	k1gInSc1
•	•	k?
James	James	k1gMnSc1
McElmury	McElmura	k1gFnSc2
•	•	k?
Richard	Richard	k1gMnSc1
McGlynn	McGlynn	k1gMnSc1
•	•	k?
Bruce	Bruce	k1gMnSc1
McIntosh	McIntosh	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Mellor	Mellor	k1gMnSc1
•	•	k?
Ronald	Ronald	k1gMnSc1
Naslund	Naslund	k1gMnSc1
•	•	k?
Walter	Walter	k1gMnSc1
Olds	Olds	k1gInSc1
•	•	k?
Frank	frank	k1gInSc1
Sanders	Sanders	k1gInSc1
•	•	k?
Craig	Craig	k1gInSc1
Sarner	Sarner	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Sears	Searsa	k1gFnPc2
•	•	k?
Timothy	Timotha	k1gMnSc2
Sheehy	Sheeha	k1gMnSc2
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Murray	Murray	k1gInPc1
Williamson	Williamson	k1gNnSc1
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
TCH	tch	k0
<g/>
)	)	kIx)
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Bednář	Bednář	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Černý	Černý	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Daněk	Daněk	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Dzurilla	Dzurilla	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Farda	Farda	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Havel	Havel	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holeček	Holeček	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Horešovský	Horešovský	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Kochta	Kochta	k1gMnSc1
•	•	k?
Oldřich	Oldřich	k1gMnSc1
Machač	Machač	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Martinec	Martinec	k1gMnSc1
•	•	k?
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
Eduard	Eduard	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Tajcnár	Tajcnár	k1gMnSc1
•	•	k?
Bohuslav	Bohuslav	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Vohralík	Vohralík	k1gMnSc1
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Vladimír	Vladimír	k1gMnSc1
Kostka	Kostka	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
Trenéři	trenér	k1gMnPc1
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
uvedeni	uveden	k2eAgMnPc1d1
pouze	pouze	k6eAd1
pro	pro	k7c4
úplnost	úplnost	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
olympijské	olympijský	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
nedostávají	dostávat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
MS	MS	kA
1965	#num#	k4
–	–	k?
ČSSR	ČSSR	kA
Brankáři	brankář	k1gMnSc3
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Dzurilla	Dzurilla	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Nadrchal	nadrchat	k5eAaPmAgMnS
Obránci	obránce	k1gMnSc3
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Potsch	Potsch	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Tikal	Tikal	k1gMnSc1
–	–	k?
•	•	k?
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Jozef	Jozef	k1gInSc1
Čapla	čapnout	k5eAaPmAgFnS
•	•	k?
Jaromír	Jaromír	k1gMnSc1
Meixner	Meixner	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Prýl	Prýl	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Černý	Černý	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Ševčík	Ševčík	k1gMnSc1
•	•	k?
Jozef	Jozef	k1gInSc1
Golonka	Golonka	k1gFnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Jiřík	Jiřík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Kepák	Kepák	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Bouzek	Bouzka	k1gFnPc2
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Kostka	Kostka	k1gMnSc1
</s>
<s>
MS	MS	kA
1966	#num#	k4
–	–	k?
ČSSR	ČSSR	kA
Brankáři	brankář	k1gMnSc3
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Dzurilla	Dzurilla	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holeček	Holeček	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Potsch	Potsch	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Tikal	Tikal	k1gMnSc1
–	–	k?
•	•	k?
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
Jaromír	Jaromír	k1gMnSc1
Meixner	Meixner	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Prýl	Prýl	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Černý	Černý	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Ševčík	Ševčík	k1gMnSc1
•	•	k?
Jozef	Jozef	k1gInSc1
Golonka	Golonka	k1gFnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Jiřík	Jiřík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Kokš	Kokš	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Bouzek	Bouzka	k1gFnPc2
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Kostka	Kostka	k1gMnSc1
</s>
<s>
MS	MS	kA
1969	#num#	k4
–	–	k?
ČSSR	ČSSR	kA
Brankáři	brankář	k1gMnSc3
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Dzurilla	Dzurilla	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Lacký	Lacký	k2eAgMnSc1d1
Obránci	obránce	k1gMnSc3
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Machač	Machač	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Bednář	Bednář	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Horešovský	Horešovský	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
Richard	Richard	k1gMnSc1
Farda	Farda	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Černý	Černý	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Havel	Havel	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
František	František	k1gMnSc1
Ševčík	Ševčík	k1gMnSc1
•	•	k?
Jozef	Jozef	k1gInSc1
Golonka	Golonka	k1gFnSc1
–	–	k?
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Jiřík	Jiřík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
•	•	k?
Josef	Josef	k1gMnSc1
Augusta	Augusta	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Kostka	Kostka	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
</s>
<s>
MS	MS	kA
1970	#num#	k4
–	–	k?
ČSSR	ČSSR	kA
Brankáři	brankář	k1gMnSc3
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Dzurilla	Dzurilla	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Lacký	Lacký	k2eAgMnSc1d1
Obránci	obránce	k1gMnPc1
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Machač	Machač	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Bednář	Bednář	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Horešovský	Horešovský	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Ľubomír	Ľubomír	k1gMnSc1
Ujváry	Ujvár	k1gInPc4
Útočníci	útočník	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Martinec	Martinec	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Farda	Farda	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Černý	Černý	k1gMnSc1
–	–	k?
•	•	k?
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Kochta	Kochta	k1gMnSc1
•	•	k?
Július	Július	k1gInSc1
Haas	Haas	k1gInSc1
•	•	k?
František	František	k1gMnSc1
Ševčík	Ševčík	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Prýl	Prýl	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Kostka	Kostka	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
</s>
<s>
MS	MS	kA
1972	#num#	k4
–	–	k?
ČSSR	ČSSR	kA
Brankáři	brankář	k1gMnSc3
</s>
<s>
Jiří	Jiří	k1gMnSc1
Holeček	Holeček	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Dzurilla	Dzurilla	k1gMnSc1
•	•	k?
Marcel	Marcel	k1gMnSc1
Sakáč	Sakáč	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Machač	Machač	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
–	–	k?
•	•	k?
Jiří	Jiří	k1gMnSc1
Bubla	Bubla	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Horešovský	Horešovský	k1gMnSc1
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Tajcnár	Tajcnár	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Kužela	Kužel	k1gMnSc2
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Bednář	Bednář	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Martinec	Martinec	k1gMnSc1
•	•	k?
Bohuslav	Bohuslav	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Farda	Farda	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Július	Július	k1gInSc1
Haas	Haas	k1gInSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Kochta	Kochta	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Paleček	Paleček	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Kostka	Kostka	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
</s>
<s>
MS	MS	kA
1973	#num#	k4
–	–	k?
ČSSR	ČSSR	kA
Brankáři	brankář	k1gMnSc3
</s>
<s>
Jiří	Jiří	k1gMnSc1
Holeček	Holeček	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Crha	Crha	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Machač	Machač	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
–	–	k?
•	•	k?
Jiří	Jiří	k1gMnSc1
Bubla	Bubla	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Horešovský	Horešovský	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Vohralík	Vohralík	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Kužela	Kužel	k1gMnSc2
•	•	k?
Petr	Petr	k1gMnSc1
Adamík	Adamík	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Martinec	Martinec	k1gMnSc1
•	•	k?
Bohuslav	Bohuslav	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Farda	Farda	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Jiří	Jiří	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Kochta	Kochta	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Paleček	Paleček	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Kostka	Kostka	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnSc5
</s>
<s>
Josef	Josef	k1gMnSc1
Hovora	hovora	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Podhorský	Podhorský	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Jurka	Jurka	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Trachta	Trachta	k?
•	•	k?
Jiří	Jiří	k1gMnSc1
Neubauer	Neubauer	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Panchártek	Panchártek	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
•	•	k?
Jiří	Jiří	k1gMnSc1
Kochta	Kochta	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Vorlíček	Vorlíček	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Balun	Balun	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Augusta	Augusta	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Štrojza	Štrojz	k1gMnSc2
•	•	k?
Pavel	Pavel	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Pavel	Pavel	k1gMnSc1
Všianský	Všianský	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnPc1
</s>
<s>
Josef	Josef	k1gMnSc1
Hovora	hovora	k1gMnSc1
•	•	k?
Radomír	Radomír	k1gMnSc1
Daněk	Daněk	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Turner	turner	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Trachta	Trachta	k?
•	•	k?
Jiří	Jiří	k1gMnSc1
Neubauer	Neubauer	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Bednář	Bednář	k1gMnSc1
•	•	k?
Marek	Marek	k1gMnSc1
Eysselt	Eysselt	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Vokáč	Vokáč	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
•	•	k?
Jiří	Jiří	k1gMnSc1
Kochta	Kochta	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Augusta	Augusta	k1gMnSc1
•	•	k?
Václav	Václav	k1gMnSc1
Střílka	Střílek	k1gMnSc2
•	•	k?
Milan	Milan	k1gMnSc1
Lano	lano	k1gNnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Vorlíček	Vorlíček	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Nikl	Nikl	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Balun	Balun	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Václav	Václav	k1gMnSc1
Mařík	Mařík	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnSc5
</s>
<s>
Marcel	Marcel	k1gMnSc1
Sakáč	Sakáč	k1gMnSc1
•	•	k?
Pavol	Pavol	k1gInSc1
Svitana	Svitan	k1gMnSc2
•	•	k?
Radomír	Radomír	k1gMnSc1
Daněk	Daněk	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Vinš	Vinš	k1gMnSc1
•	•	k?
Lubomír	Lubomír	k1gMnSc1
Bauer	Bauer	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Bednář	Bednář	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Zelenický	Zelenický	k2eAgMnSc1d1
•	•	k?
Jiří	Jiří	k1gMnSc1
Vokáč	Vokáč	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Václav	Václav	k1gMnSc1
Mařík	Mařík	k1gMnSc1
•	•	k?
Bohuslav	Bohuslav	k1gMnSc1
Ebermann	Ebermann	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Augusta	Augusta	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Vorlíček	Vorlíček	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Mráz	Mráz	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Lano	lano	k1gNnSc1
•	•	k?
Václav	Václav	k1gMnSc1
Střílka	Střílek	k1gMnSc2
•	•	k?
Kamil	Kamil	k1gMnSc1
Konečný	Konečný	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Nikl	Nikl	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Vodák	Vodák	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Balun	Balun	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnPc1
</s>
<s>
Marcel	Marcel	k1gMnSc1
Sakáč	Sakáč	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Crha	Crha	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Zima	Zima	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Horešovský	Horešovský	k1gMnSc1
•	•	k?
Lubomír	Lubomír	k1gMnSc1
Bauer	Bauer	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Bubla	Bubla	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Hanačík	Hanačík	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Vinš	Vinš	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Josef	Josef	k1gMnSc1
Augusta	Augusta	k1gMnSc1
•	•	k?
Bohuslav	Bohuslav	k1gMnSc1
Ebermann	Ebermann	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Balun	Balun	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Mec	Mec	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Vorlíček	Vorlíček	k1gMnSc1
•	•	k?
Dušan	Dušan	k1gMnSc1
Žiška	Žiška	k1gMnSc1
•	•	k?
Marek	Marek	k1gMnSc1
Sýkora	Sýkora	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Vodák	Vodák	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Mráz	Mráz	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnPc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Crha	Crha	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Hronek	Hronek	k1gMnSc1
•	•	k?
Roman	Roman	k1gMnSc1
Lelek	lelek	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Horešovský	Horešovský	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Eiselt	Eiselt	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Bubla	Bubla	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Kajkl	Kajkl	k1gInSc1
•	•	k?
Karel	Karel	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Lyčka	Lyčka	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
•	•	k?
Josef	Josef	k1gMnSc1
Augusta	Augusta	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Vorlíček	Vorlíček	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Balun	Balun	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Mec	Mec	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Janák	Janák	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Hlaváček	Hlaváček	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Čech	Čech	k1gMnSc1
•	•	k?
Václav	Václav	k1gMnSc1
Honc	Honc	k1gFnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Bartoň	Bartoň	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnPc7
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Krása	krása	k1gFnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Šaršoň	Šaršoň	k1gMnSc1
•	•	k?
Roman	Roman	k1gMnSc1
Lelek	lelek	k1gMnSc1
•	•	k?
Luděk	Luděk	k1gMnSc1
Brož	Brož	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Eiselt	Eiselt	k1gMnSc1
•	•	k?
Otakar	Otakar	k1gMnSc1
Vejvoda	Vejvoda	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Kajkl	Kajkl	k1gInSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Lyčka	Lyčka	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Rykl	ryknout	k5eAaPmAgMnS
•	•	k?
Petr	Petr	k1gMnSc1
Adamík	Adamík	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
•	•	k?
Jan	Jan	k1gMnSc1
Balun	Balun	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Vorlíček	Vorlíček	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Augusta	Augusta	k1gMnSc1
•	•	k?
Ján	Ján	k1gMnSc1
Bačo	bača	k1gMnSc5
•	•	k?
Jiří	Jiří	k1gMnSc1
Janák	Janák	k1gMnSc1
•	•	k?
Štefan	Štefan	k1gMnSc1
Onofrej	Onofrej	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Bartoň	Bartoň	k1gMnSc1
•	•	k?
Václav	Václav	k1gMnSc1
Honc	Honc	k1gFnSc4
Trenér	trenér	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnPc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Hnídek	Hnídek	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Bakus	Bakus	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Adamík	Adamík	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Horáček	Horáček	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Chalupa	Chalupa	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Neliba	Neliba	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Kaberle	Kaberle	k1gFnSc2
•	•	k?
Karel	Karel	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
•	•	k?
Milan	Milan	k1gMnSc1
Nový	nový	k2eAgMnSc1d1
•	•	k?
Josef	Josef	k1gMnSc1
Augusta	Augusta	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Titz	Titz	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Výborný	Výborný	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Kupec	kupec	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Kousek	kousek	k6eAd1
•	•	k?
Jan	Jan	k1gMnSc1
Novotný	Novotný	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Pitner	Pitner	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnSc5
</s>
<s>
Jiří	Jiří	k1gMnSc1
Králík	Králík	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Steklík	Steklík	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Milan	Milan	k1gMnSc1
Chalupa	Chalupa	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Benák	Benák	k1gMnSc1
•	•	k?
Radoslav	Radoslav	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
•	•	k?
Eduard	Eduard	k1gMnSc1
Uvíra	Uvíra	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Adamík	Adamík	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Horáček	Horáček	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Souček	Souček	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Otta	Otta	k1gMnSc1
Klapka	Klapka	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Kořený	kořený	k2eAgMnSc1d1
•	•	k?
Miloš	Miloš	k1gMnSc1
Kupec	kupec	k1gMnSc1
•	•	k?
Igor	Igor	k1gMnSc1
Liba	Liba	k1gFnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Liška	Liška	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Výborný	Výborný	k1gMnSc1
•	•	k?
Ondřej	Ondřej	k1gMnSc1
Weissmann	Weissmann	k1gMnSc1
•	•	k?
Antonín	Antonín	k1gMnSc1
Micka	micka	k1gFnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Micka	micka	k1gFnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Netík	Netík	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
Oldřich	Oldřich	k1gMnSc1
Válek	Válek	k1gMnSc1
•	•	k?
Rostislav	Rostislav	k1gMnSc1
Vlach	Vlach	k1gMnSc1
•	•	k?
Augustin	Augustin	k1gMnSc1
Žák	Žák	k1gMnSc1
•	•	k?
Ján	Ján	k1gMnSc1
Jurčišin	Jurčišin	k2eAgMnSc1d1
•	•	k?
Libor	Libor	k1gMnSc1
Dolana	dolan	k1gMnSc2
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Jehlička	jehlička	k1gFnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Juříček	Juříček	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnPc5
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Šindel	šindel	k1gInSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Steklík	Steklík	k1gMnSc1
•	•	k?
Marek	Marek	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Milan	Milan	k1gMnSc1
Chalupa	Chalupa	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Benák	Benák	k1gMnSc1
•	•	k?
Radoslav	Radoslav	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
•	•	k?
Miloslav	Miloslav	k1gMnSc1
Hořava	Hořava	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Adamík	Adamík	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Horáček	Horáček	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Mečiar	Mečiar	k1gMnSc1
•	•	k?
Kamil	Kamil	k1gMnSc1
Prachař	Prachař	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Souček	Souček	k1gMnSc1
•	•	k?
Bedřich	Bedřich	k1gMnSc1
Ščerban	Ščerban	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Dudáček	dudáček	k1gMnSc1
•	•	k?
Rostislav	Rostislav	k1gMnSc1
Vlach	Vlach	k1gMnSc1
•	•	k?
Igor	Igor	k1gMnSc1
Liba	Liba	k1gFnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Rosol	Rosol	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Kameš	Kameš	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Klíma	Klíma	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Kupec	kupec	k1gMnSc1
•	•	k?
Ondřej	Ondřej	k1gMnSc1
Weissmann	Weissmann	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Výborný	Výborný	k1gMnSc1
•	•	k?
Oldřich	Oldřich	k1gMnSc1
Válek	Válek	k1gMnSc1
•	•	k?
Augustin	Augustin	k1gMnSc1
Žák	Žák	k1gMnSc1
•	•	k?
Antonín	Antonín	k1gMnSc1
Micka	micka	k1gFnSc1
•	•	k?
Libor	Libor	k1gMnSc1
Dolana	dolan	k1gMnSc2
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Hauer	Hauer	k1gMnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Micka	micka	k1gFnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
ml.	ml.	kA
Trenér	trenér	k1gMnSc1
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnPc5
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Šindel	šindel	k1gInSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Steklík	Steklík	k1gMnSc1
•	•	k?
Marek	Marek	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Štorek	Štorek	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Benák	Benák	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Horáček	Horáček	k1gMnSc1
•	•	k?
Miloslav	Miloslav	k1gMnSc1
Hořava	Hořava	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Höfer	Höfer	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Kolek	Kolek	k1gMnSc1
•	•	k?
Vojtěch	Vojtěch	k1gMnSc1
Kučera	Kučera	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Mareš	Mareš	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Musil	Musil	k1gMnSc1
•	•	k?
Kamil	Kamil	k1gMnSc1
Prachař	Prachař	k1gMnSc1
•	•	k?
Radoslav	Radoslav	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
•	•	k?
Bedřich	Bedřich	k1gMnSc1
Ščerban	Ščerban	k1gMnSc1
•	•	k?
Rostislav	Rostislav	k1gMnSc1
Urban	Urban	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Roman	Roman	k1gMnSc1
Božek	Božek	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Dudáček	dudáček	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Hrbatý	hrbatý	k2eAgInSc1d1
ml.	ml.	kA
•	•	k?
Ota	Ota	k1gMnSc1
Jungwirth	Jungwirth	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Kameš	Kameš	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Klíma	Klíma	k1gMnSc1
•	•	k?
Luděk	Luděk	k1gMnSc1
Konopčík	Konopčík	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Kupec	kupec	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Mareš	Mareš	k1gMnSc1
•	•	k?
Antonín	Antonín	k1gMnSc1
Micka	micka	k1gFnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Novotný	Novotný	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Pata	pata	k1gFnSc1
•	•	k?
Michal	Michal	k1gMnSc1
Pivoňka	Pivoňka	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Rosol	Rosol	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Šejba	Šejba	k1gMnSc1
•	•	k?
Oldřich	Oldřich	k1gMnSc1
Válek	Válek	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Vlk	Vlk	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Výborný	Výborný	k1gMnSc1
•	•	k?
Augustin	Augustin	k1gMnSc1
Žák	Žák	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Neveselý	Neveselý	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
–	–	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
Brankáři	brankář	k1gMnSc5
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
•	•	k?
Roman	Roman	k1gMnSc1
Čechmánek	Čechmánek	k1gMnSc1
•	•	k?
Marek	Marek	k1gMnSc1
Novotný	Novotný	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Bílek	Bílek	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Richard	Richard	k1gMnSc1
Adam	Adam	k1gMnSc1
•	•	k?
Roman	Roman	k1gMnSc1
Čech	Čech	k1gMnSc1
•	•	k?
Bedřich	Bedřich	k1gMnSc1
Ščerban	Ščerban	k1gMnSc1
•	•	k?
Roman	Roman	k1gMnSc1
Kaňkovský	Kaňkovský	k2eAgMnSc1d1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Kolek	Kolek	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Kuchyňa	Kuchyňa	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Kuntoš	Kuntoš	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Šmehlík	Šmehlík	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Vyhlídal	vyhlídat	k5eAaImAgMnS,k5eAaPmAgMnS
•	•	k?
Ivo	Ivo	k1gMnSc1
Dostál	Dostál	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Patrik	Patrik	k1gMnSc1
Augusta	Augusta	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Cihlář	Cihlář	k1gMnSc1
•	•	k?
Libor	Libor	k1gMnSc1
Dolana	dolan	k1gMnSc2
•	•	k?
Radek	Radek	k1gMnSc1
Haut	Haut	k1gMnSc1
•	•	k?
Radek	Radek	k1gMnSc1
Haman	Haman	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Kaňkovský	Kaňkovský	k2eAgMnSc1d1
•	•	k?
Milan	Milan	k1gMnSc1
Kastner	Kastner	k1gMnSc1
•	•	k?
Ervin	Ervin	k1gMnSc1
Mašek	Mašek	k1gMnSc1
•	•	k?
Roman	Roman	k1gMnSc1
Mejzlík	Mejzlík	k1gMnSc1
•	•	k?
Aleš	Aleš	k1gMnSc1
Polcar	Polcar	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Poukar	Poukar	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Vlk	Vlk	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Kucharčík	Kucharčík	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Chlubna	Chlubna	k1gMnSc1
•	•	k?
Leoš	Leoš	k1gMnSc1
Pípa	pípa	k1gFnSc1
•	•	k?
Marek	marka	k1gFnPc2
Zadina	zadina	k1gFnSc1
•	•	k?
Viktor	Viktor	k1gMnSc1
Ujčík	Ujčík	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Augusta	Augusta	k1gMnSc1
</s>
<s>
Klub	klub	k1gInSc1
hokejových	hokejový	k2eAgMnPc2d1
střelců	střelec	k1gMnPc2
deníku	deník	k1gInSc2
Sport	sport	k1gInSc1
–	–	k?
250	#num#	k4
branek	branka	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milan	Milan	k1gMnSc1
Nový	Nový	k1gMnSc1
•	•	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vladimír	Vladimír	k1gMnSc1
Růžička	Růžička	k1gMnSc1
•	•	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vladimír	Vladimír	k1gMnSc1
Martinec	Martinec	k1gMnSc1
•	•	k?
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ivan	Ivan	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Černý	Černý	k1gMnSc1
•	•	k?
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Ujčík	Ujčík	k1gMnSc1
•	•	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vladimír	Vladimír	k1gMnSc1
Zábrodský	Zábrodský	k2eAgMnSc1d1
•	•	k?
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vincent	Vincent	k1gMnSc1
Lukáč	Lukáč	k1gMnSc1
•	•	k?
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastimil	Vlastimil	k1gMnSc1
Bubník	Bubník	k1gMnSc1
•	•	k?
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Martin	Martin	k1gMnSc1
Procházka	Procházka	k1gMnSc1
•	•	k?
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Sýkora	Sýkora	k1gMnSc1
•	•	k?
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Lála	Lála	k1gMnSc1
•	•	k?
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Ton	Ton	k1gMnSc1
•	•	k?
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Dopita	dopit	k2eAgFnSc1d1
•	•	k?
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Jiřík	Jiřík	k1gMnSc1
•	•	k?
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jozef	Jozef	k1gMnSc1
Golonka	Golonka	k1gFnSc1
•	•	k?
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Hübl	Hübl	k1gMnSc1
•	•	k?
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
Král	Král	k1gMnSc1
•	•	k?
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ladislav	Ladislav	k1gMnSc1
Lubina	Lubin	k2eAgNnSc2d1
•	•	k?
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Pouzar	Pouzar	k1gMnSc1
•	•	k?
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ondřej	Ondřej	k1gMnSc1
Kratěna	Kratěna	k1gMnSc1
•	•	k?
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Patera	Patera	k1gMnSc1
•	•	k?
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Burger	Burger	k1gMnSc1
•	•	k?
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Balaštík	Balaštík	k1gMnSc1
•	•	k?
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohuslav	Bohuslav	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
•	•	k?
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomáš	Tomáš	k1gMnSc1
Vlasák	Vlasák	k1gMnSc1
•	•	k?
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Bednář	Bednář	k1gMnSc1
•	•	k?
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Havel	Havel	k1gMnSc1
•	•	k?
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Vimmer	Vimmer	k1gMnSc1
•	•	k?
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
David	David	k1gMnSc1
Hruška	Hruška	k1gMnSc1
•	•	k?
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Kudrna	Kudrna	k1gMnSc1
•	•	k?
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ján	Ján	k1gMnSc1
Starší	starší	k1gMnSc1
•	•	k?
39	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Rosol	Rosol	k1gMnSc1
•	•	k?
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Igor	Igor	k1gMnSc1
Liba	Liba	k1gFnSc1
•	•	k?
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Zelenka	Zelenka	k1gMnSc1
•	•	k?
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Kumstát	Kumstát	k1gInSc1
•	•	k?
43	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohuslav	Bohuslav	k1gMnSc1
Ebermann	Ebermann	k1gMnSc1
•	•	k?
44	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marián	Marián	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
•	•	k?
45	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Beránek	Beránek	k1gMnSc1
•	•	k?
46	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robert	Robert	k1gMnSc1
Reichel	Reichel	k1gMnSc1
•	•	k?
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Peterek	Peterka	k1gFnPc2
•	•	k?
48	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Leška	Leška	k1gMnSc1
•	•	k?
49	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Janků	Janků	k1gMnSc1
•	•	k?
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
Žemlička	Žemlička	k1gMnSc1
•	•	k?
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robert	Robert	k1gMnSc1
Kysela	Kysela	k1gMnSc1
•	•	k?
52	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
•	•	k?
53	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radek	Radek	k1gMnSc1
Bělohlav	Bělohlav	k1gMnSc1
•	•	k?
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Černík	Černík	k1gMnSc1
•	•	k?
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oldřich	Oldřich	k1gMnSc1
Válek	Válek	k1gMnSc1
•	•	k?
56	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanislav	Stanislav	k1gMnSc1
Prýl	Prýl	k1gMnSc1
•	•	k?
57	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Pantůček	Pantůček	k1gMnSc1
•	•	k?
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Pletka	pletka	k1gFnSc1
•	•	k?
59	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
David	David	k1gMnSc1
Výborný	Výborný	k1gMnSc1
•	•	k?
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Dolana	dolan	k1gMnSc2
•	•	k?
61	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostislav	Rostislav	k1gMnSc1
Vlach	Vlach	k1gMnSc1
•	•	k?
62	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Šejba	Šejba	k1gMnSc1
•	•	k?
63	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radek	Radek	k1gMnSc1
Ťoupal	Ťoupal	k1gMnSc1
•	•	k?
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bronislav	Bronislav	k1gMnSc1
Danda	Danda	k1gMnSc1
•	•	k?
65	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dušan	Dušan	k1gMnSc1
Pašek	Pašek	k1gMnSc1
•	•	k?
66	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomáš	Tomáš	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
•	•	k?
67	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dárius	Dárius	k1gInSc1
Rusnák	Rusnák	k1gInSc4
•	•	k?
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michal	Michal	k1gMnSc1
Sup	sup	k1gMnSc1
</s>
<s>
Nejproduktivnější	produktivní	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
Tipsport	Tipsport	k1gInSc4
ELH	ELH	kA
(	(	kIx(
<g/>
nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
v	v	k7c6
kanadském	kanadský	k2eAgNnSc6d1
bodování	bodování	k1gNnSc6
<g/>
)	)	kIx)
-	-	kIx~
Extraliga	extraliga	k1gFnSc1
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
Československo	Československo	k1gNnSc1
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
1962	#num#	k4
Jozef	Jozef	k1gMnSc1
Golonka	Golonka	k1gFnSc1
•	•	k?
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
1963	#num#	k4
Jiří	Jiří	k1gMnSc2
Dolana	dolan	k1gMnSc2
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
1964	#num#	k4
Josef	Josef	k1gMnSc1
Černý	Černý	k1gMnSc1
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
1965	#num#	k4
Jan	Jan	k1gMnSc1
Klapáč	Klapáč	k?
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
1966	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Holík	Holík	k1gMnSc1
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1968	#num#	k4
Jan	Jan	k1gMnSc1
Havel	Havel	k1gMnSc1
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
Jan	Jan	k1gMnSc1
Suchý	Suchý	k1gMnSc1
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
Jiří	Jiří	k1gMnSc1
Kochta	Kocht	k1gInSc2
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
Milan	Milan	k1gMnSc1
Nový	Nový	k1gMnSc1
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Václav	Václav	k1gMnSc1
Nedomanský	Nedomanský	k2eAgMnSc1d1
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
1975	#num#	k4
Ivan	Ivan	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
1976	#num#	k4
Milan	Milan	k1gMnSc1
Nový	Nový	k1gMnSc1
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
1977	#num#	k4
Milan	Milan	k1gMnSc1
Nový	Nový	k1gMnSc1
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
Milan	Milan	k1gMnSc1
Nový	Nový	k1gMnSc1
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
Marián	Marián	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
1980	#num#	k4
Vincent	Vincent	k1gMnSc1
Lukáč	Lukáč	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
1981	#num#	k4
Milan	Milan	k1gMnSc1
Nový	Nový	k1gMnSc1
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
1982	#num#	k4
Milan	Milan	k1gMnSc1
Nový	Nový	k1gMnSc1
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
Vincent	Vincent	k1gMnSc1
Lukáč	Lukáč	k1gMnSc1
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Růžička	Růžička	k1gMnSc1
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
Miroslav	Miroslav	k1gMnSc1
Ihnačák	Ihnačák	k1gMnSc1
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Růžička	Růžička	k1gMnSc1
•	•	k?
1986	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
David	David	k1gMnSc1
Volek	Volek	k1gMnSc1
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
1988	#num#	k4
Jiří	Jiří	k1gMnSc1
Lála	Lála	k1gMnSc1
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Růžička	Růžička	k1gMnSc1
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Robert	Robert	k1gMnSc1
Reichel	Reichel	k1gMnSc1
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Radek	Radek	k1gMnSc1
Ťoupal	Ťoupal	k1gMnSc1
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
Žigmund	Žigmund	k1gInSc1
Pálffy	Pálff	k1gInPc1
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
Žigmund	Žigmunda	k1gFnPc2
Pálffy	Pálff	k1gInPc1
Česko	Česko	k1gNnSc1
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
Pavel	Pavel	k1gMnSc1
Patera	Patera	k1gMnSc1
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
Pavel	Pavel	k1gMnSc1
Patera	Patera	k1gMnSc1
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Růžička	Růžička	k1gMnSc1
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Roman	Roman	k1gMnSc1
Horák	Horák	k1gMnSc1
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
David	David	k1gMnSc1
Moravec	Moravec	k1gMnSc1
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
David	David	k1gMnSc1
Výborný	Výborný	k1gMnSc1
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2000	#num#	k4
Richard	Richard	k1gMnSc1
Král	Král	k1gMnSc1
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
Patrik	Patrik	k1gMnSc1
Martinec	Martinec	k1gMnSc1
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Petr	Petr	k1gMnSc1
Leška	Leška	k1gMnSc1
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
Richard	Richard	k1gMnSc1
Král	Král	k1gMnSc1
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Josef	Josef	k1gMnSc1
Beránek	Beránek	k1gMnSc1
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
Michal	Michala	k1gFnPc2
Mikeska	Mikeska	k1gFnSc1
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Jan	Jan	k1gMnSc1
Marek	Marek	k1gMnSc1
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
Petr	Petr	k1gMnSc1
Ton	Ton	k1gMnSc1
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Bednář	Bednář	k1gMnSc1
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
Roman	Roman	k1gMnSc1
Červenka	Červenka	k1gMnSc1
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Vlasák	Vlasák	k1gMnSc1
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
Petr	Petr	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
Martin	Martin	k1gMnSc1
Růžička	Růžička	k1gMnSc1
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
Petr	Petr	k1gMnSc1
Ton	Ton	k1gMnSc1
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Viktor	Viktor	k1gMnSc1
Hübl	Hübl	k1gMnSc1
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
Roman	Roman	k1gMnSc1
Červenka	Červenka	k1gMnSc1
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
Lukáš	Lukáš	k1gMnSc1
Pech	Pech	k1gMnSc1
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
Milan	Milan	k1gMnSc1
Gulaš	Gulaš	k1gMnSc1
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
Milan	Milan	k1gMnSc1
Gulaš	Gulaš	k1gMnSc1
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
Milan	Milan	k1gMnSc1
Gulaš	Gulaš	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jo	jo	k9
<g/>
20000074805	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84113174	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
</s>
