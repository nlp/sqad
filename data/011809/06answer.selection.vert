<s>
Hektór	Hektór	k1gInSc1	Hektór
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ἕ	Ἕ	k?	Ἕ
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Hector	Hector	k1gMnSc1	Hector
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
trojského	trojský	k2eAgMnSc2d1	trojský
krále	král	k1gMnSc2	král
Priama	Priamos	k1gMnSc2	Priamos
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Hekabé	Hekabý	k2eAgInPc1d1	Hekabý
<g/>
.	.	kIx.	.
</s>
