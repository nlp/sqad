<p>
<s>
Hektór	Hektór	k1gInSc1	Hektór
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ἕ	Ἕ	k?	Ἕ
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Hector	Hector	k1gMnSc1	Hector
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
trojského	trojský	k2eAgMnSc2d1	trojský
krále	král	k1gMnSc2	král
Priama	Priamos	k1gMnSc2	Priamos
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Hekabé	Hekabý	k2eAgInPc1d1	Hekabý
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
trojských	trojský	k2eAgNnPc2d1	Trojské
vojsk	vojsko	k1gNnPc2	vojsko
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
Achájcům	Achájce	k1gMnPc3	Achájce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
miláčkem	miláček	k1gMnSc7	miláček
Trójanů	Trójan	k1gMnPc2	Trójan
<g/>
,	,	kIx,	,
nazývali	nazývat	k5eAaImAgMnP	nazývat
ho	on	k3xPp3gMnSc4	on
štítem	štít	k1gInSc7	štít
svého	svůj	k3xOyFgNnSc2	svůj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nejstatečnější	statečný	k2eAgInSc1d3	nejstatečnější
a	a	k8xC	a
nejsilnější	silný	k2eAgMnSc1d3	nejsilnější
bojovník	bojovník	k1gMnSc1	bojovník
<g/>
,	,	kIx,	,
vynikal	vynikat	k5eAaImAgInS	vynikat
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
postavou	postava	k1gFnSc7	postava
<g/>
,	,	kIx,	,
krásou	krása	k1gFnSc7	krása
i	i	k8xC	i
duchem	duch	k1gMnSc7	duch
<g/>
.	.	kIx.	.
</s>
<s>
Předčil	předčit	k5eAaBmAgMnS	předčit
všemi	všecek	k3xTgFnPc7	všecek
svými	svůj	k3xOyFgMnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
svého	svůj	k3xOyFgMnSc4	svůj
protivníka	protivník	k1gMnSc4	protivník
<g/>
,	,	kIx,	,
velitele	velitel	k1gMnPc4	velitel
Achájců	Achájce	k1gMnPc2	Achájce
<g/>
,	,	kIx,	,
mykénského	mykénský	k2eAgMnSc4d1	mykénský
krále	král	k1gMnSc4	král
Agamemnóna	Agamemnón	k1gMnSc4	Agamemnón
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
druhý	druhý	k4xOgMnSc1	druhý
z	z	k7c2	z
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
hrdina	hrdina	k1gMnSc1	hrdina
Achilleus	Achilleus	k1gMnSc1	Achilleus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nemohl	moct	k5eNaImAgMnS	moct
rovnat	rovnat	k5eAaImF	rovnat
<g/>
,	,	kIx,	,
jedině	jedině	k6eAd1	jedině
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Devět	devět	k4xCc1	devět
let	léto	k1gNnPc2	léto
obléhali	obléhat	k5eAaImAgMnP	obléhat
Achájci	Achájce	k1gMnPc1	Achájce
bohaté	bohatý	k2eAgFnSc2d1	bohatá
a	a	k8xC	a
kvetoucí	kvetoucí	k2eAgNnSc4d1	kvetoucí
město	město	k1gNnSc4	město
Tróju	Trója	k1gFnSc4	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Připluli	připlout	k5eAaPmAgMnP	připlout
na	na	k7c6	na
1186	[number]	k4	1186
lodích	loď	k1gFnPc6	loď
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
v	v	k7c6	v
síle	síla	k1gFnSc6	síla
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
válku	válka	k1gFnSc4	válka
jako	jako	k8xS	jako
pomstu	pomsta	k1gFnSc4	pomsta
za	za	k7c4	za
únos	únos	k1gInSc4	únos
krásné	krásný	k2eAgFnSc2d1	krásná
Heleny	Helena	k1gFnSc2	Helena
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnSc2	manželka
spartského	spartský	k2eAgMnSc2d1	spartský
krále	král	k1gMnSc2	král
Meneláa	Meneláus	k1gMnSc2	Meneláus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Unesl	unést	k5eAaPmAgMnS	unést
ji	on	k3xPp3gFnSc4	on
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc4	král
Priama	Priamos	k1gMnSc4	Priamos
Paris	Paris	k1gMnSc1	Paris
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
rozsoudil	rozsoudit	k5eAaPmAgMnS	rozsoudit
tři	tři	k4xCgFnPc4	tři
nejkrásnější	krásný	k2eAgFnPc4d3	nejkrásnější
bohyně	bohyně	k1gFnPc4	bohyně
Héru	Héra	k1gFnSc4	Héra
<g/>
,	,	kIx,	,
Athénu	Athéna	k1gFnSc4	Athéna
a	a	k8xC	a
Afrodíté	Afrodítý	k2eAgInPc4d1	Afrodítý
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
mu	on	k3xPp3gMnSc3	on
slibovala	slibovat	k5eAaImAgFnS	slibovat
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
Asií	Asie	k1gFnSc7	Asie
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
slávu	sláva	k1gFnSc4	sláva
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
válečném	válečný	k2eAgNnSc6d1	válečné
a	a	k8xC	a
Afrodíté	Afrodítý	k2eAgFnPc1d1	Afrodítý
mu	on	k3xPp3gMnSc3	on
slíbila	slíbit	k5eAaPmAgFnS	slíbit
lásku	láska	k1gFnSc4	láska
nejkrásnější	krásný	k2eAgFnSc4d3	nejkrásnější
z	z	k7c2	z
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojenské	vojenský	k2eAgInPc1d1	vojenský
síly	síl	k1gInPc1	síl
Tróje	Trója	k1gFnSc2	Trója
byly	být	k5eAaImAgInP	být
sotva	sotva	k6eAd1	sotva
poloviční	poloviční	k2eAgInPc1d1	poloviční
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
však	však	k9	však
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
neválčili	válčit	k5eNaImAgMnP	válčit
jenom	jenom	k9	jenom
pro	pro	k7c4	pro
žold	žold	k1gInSc4	žold
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odhodlaně	odhodlaně	k6eAd1	odhodlaně
bránili	bránit	k5eAaImAgMnP	bránit
své	svůj	k3xOyFgNnSc4	svůj
rodné	rodný	k2eAgNnSc4d1	rodné
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
těch	ten	k3xDgFnPc2	ten
bylo	být	k5eAaImAgNnS	být
jenom	jenom	k9	jenom
asi	asi	k9	asi
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
celá	celý	k2eAgNnPc4d1	celé
léta	léto	k1gNnPc4	léto
vzdorovali	vzdorovat	k5eAaImAgMnP	vzdorovat
<g/>
,	,	kIx,	,
nevyhýbali	vyhýbat	k5eNaImAgMnP	vyhýbat
se	se	k3xPyFc4	se
útokům	útok	k1gInPc3	útok
<g/>
,	,	kIx,	,
působili	působit	k5eAaImAgMnP	působit
Řekům	Řek	k1gMnPc3	Řek
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
srážkách	srážka	k1gFnPc6	srážka
a	a	k8xC	a
šarvátkách	šarvátka	k1gFnPc6	šarvátka
Hektór	Hektór	k1gInSc1	Hektór
vynikal	vynikat	k5eAaImAgMnS	vynikat
statečností	statečnost	k1gFnSc7	statečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
tak	tak	k6eAd1	tak
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
obléhání	obléhání	k1gNnSc1	obléhání
města	město	k1gNnSc2	město
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
armády	armáda	k1gFnPc1	armáda
unavené	unavený	k2eAgFnPc1d1	unavená
dlouhotrvajícími	dlouhotrvající	k2eAgInPc7d1	dlouhotrvající
boji	boj	k1gInPc7	boj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nevedly	vést	k5eNaImAgInP	vést
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
žádné	žádný	k3yNgFnSc2	žádný
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Achájci	Achájce	k1gMnPc1	Achájce
se	se	k3xPyFc4	se
toužili	toužit	k5eAaImAgMnP	toužit
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
smírně	smírně	k6eAd1	smírně
<g/>
.	.	kIx.	.
</s>
<s>
Unaveni	unavit	k5eAaPmNgMnP	unavit
byli	být	k5eAaImAgMnP	být
i	i	k9	i
obránci	obránce	k1gMnPc1	obránce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Hektór	Hektór	k1gInSc1	Hektór
využil	využít	k5eAaPmAgInS	využít
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
mykénským	mykénský	k2eAgMnSc7d1	mykénský
králem	král	k1gMnSc7	král
Agamemnónem	Agamemnón	k1gMnSc7	Agamemnón
a	a	k8xC	a
hrdinou	hrdina	k1gMnSc7	hrdina
Achilleem	Achilleus	k1gMnSc7	Achilleus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Hektór	Hektór	k1gMnSc1	Hektór
</s>
</p>
<p>
<s>
přinutil	přinutit	k5eAaPmAgMnS	přinutit
Achajce	Achajka	k1gFnSc3	Achajka
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
<g/>
,	,	kIx,	,
vrhl	vrhnout	k5eAaPmAgInS	vrhnout
se	se	k3xPyFc4	se
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
svých	svůj	k3xOyFgMnPc2	svůj
mužů	muž	k1gMnPc2	muž
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
opevnění	opevnění	k1gNnSc4	opevnění
a	a	k8xC	a
probojoval	probojovat	k5eAaPmAgMnS	probojovat
se	se	k3xPyFc4	se
až	až	k9	až
k	k	k7c3	k
achajským	achajský	k2eAgFnPc3d1	achajský
lodím	loď	k1gFnPc3	loď
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
spálit	spálit	k5eAaPmF	spálit
<g/>
.	.	kIx.	.
</s>
<s>
Nečekaně	nečekaně	k6eAd1	nečekaně
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
Achilleův	Achilleův	k2eAgMnSc1d1	Achilleův
přítel	přítel	k1gMnSc1	přítel
Patroklos	Patroklos	k1gMnSc1	Patroklos
<g/>
,	,	kIx,	,
oděný	oděný	k2eAgMnSc1d1	oděný
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
zbroji	zbroj	k1gFnSc6	zbroj
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
oklamal	oklamat	k5eAaPmAgMnS	oklamat
Trójany	Trójan	k1gMnPc4	Trójan
<g/>
.	.	kIx.	.
</s>
<s>
Hektór	Hektór	k1gMnSc1	Hektór
sešikoval	sešikovat	k5eAaPmAgMnS	sešikovat
své	svůj	k3xOyFgMnPc4	svůj
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
Patroklovi	Patrokl	k1gMnSc3	Patrokl
a	a	k8xC	a
v	v	k7c6	v
lítém	lítý	k2eAgInSc6d1	lítý
boji	boj	k1gInSc6	boj
ho	on	k3xPp3gInSc4	on
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Achilleus	Achilleus	k1gMnSc1	Achilleus
zuřivě	zuřivě	k6eAd1	zuřivě
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
rozehnal	rozehnat	k5eAaPmAgMnS	rozehnat
trojské	trojský	k2eAgNnSc4d1	Trojské
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
vrhnout	vrhnout	k5eAaImF	vrhnout
hlavní	hlavní	k2eAgFnSc7d1	hlavní
branou	brána	k1gFnSc7	brána
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
si	se	k3xPyFc3	se
netroufal	troufat	k5eNaImAgMnS	troufat
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
postavit	postavit	k5eAaPmF	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Hektór	Hektór	k1gInSc1	Hektór
statečně	statečně	k6eAd1	statečně
vyšel	vyjít	k5eAaPmAgInS	vyjít
před	před	k7c4	před
bránu	brána	k1gFnSc4	brána
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Achillea	Achilleus	k1gMnSc4	Achilleus
k	k	k7c3	k
boji	boj	k1gInSc3	boj
na	na	k7c4	na
život	život	k1gInSc4	život
a	a	k8xC	a
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vítěz	vítěz	k1gMnSc1	vítěz
slíbil	slíbit	k5eAaPmAgMnS	slíbit
vydat	vydat	k5eAaPmF	vydat
mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
tělo	tělo	k1gNnSc4	tělo
poraženého	poražený	k2eAgMnSc2d1	poražený
k	k	k7c3	k
řádnému	řádný	k2eAgInSc3d1	řádný
pohřbu	pohřeb	k1gInSc3	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Achilles	Achilles	k1gMnSc1	Achilles
přijal	přijmout	k5eAaPmAgMnS	přijmout
jenom	jenom	k9	jenom
výzvu	výzva	k1gFnSc4	výzva
k	k	k7c3	k
boji	boj	k1gInSc3	boj
a	a	k8xC	a
vrhl	vrhnout	k5eAaPmAgMnS	vrhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
oba	dva	k4xCgMnPc1	dva
oběhli	oběhnout	k5eAaPmAgMnP	oběhnout
hradby	hradba	k1gFnPc4	hradba
<g/>
,	,	kIx,	,
po	po	k7c6	po
čtvrté	čtvrtá	k1gFnSc6	čtvrtá
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
bůh	bůh	k1gMnSc1	bůh
Zeus	Zeusa	k1gFnPc2	Zeusa
na	na	k7c6	na
zlatých	zlatý	k2eAgFnPc6d1	zlatá
vahách	váha	k1gFnPc6	váha
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hektór	Hektór	k1gInSc1	Hektór
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Bohyně	bohyně	k1gFnSc1	bohyně
Athéna	Athéna	k1gFnSc1	Athéna
<g/>
,	,	kIx,	,
ikdyž	ikdyž	k1gFnSc1	ikdyž
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
Achajcům	Achajce	k1gMnPc3	Achajce
<g/>
,	,	kIx,	,
dodávala	dodávat	k5eAaImAgFnS	dodávat
Hektorovi	Hektor	k1gMnSc6	Hektor
odvahy	odvaha	k1gFnSc2	odvaha
k	k	k7c3	k
souboji	souboj	k1gInSc3	souboj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svou	svůj	k3xOyFgFnSc4	svůj
marnou	marný	k2eAgFnSc4d1	marná
snahu	snaha	k1gFnSc4	snaha
brzy	brzy	k6eAd1	brzy
vzdala	vzdát	k5eAaPmAgFnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Hektór	Hektór	k1gMnSc1	Hektór
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
bohové	bůh	k1gMnPc1	bůh
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
bojoval	bojovat	k5eAaImAgMnS	bojovat
statečně	statečně	k6eAd1	statečně
až	až	k9	až
do	do	k7c2	do
samého	samý	k3xTgInSc2	samý
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
hrdinskou	hrdinský	k2eAgFnSc7d1	hrdinská
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Achilleus	Achilleus	k1gMnSc1	Achilleus
přivázal	přivázat	k5eAaPmAgMnS	přivázat
jeho	jeho	k3xOp3gFnSc4	jeho
mrtvolu	mrtvola	k1gFnSc4	mrtvola
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
válečnému	válečný	k2eAgInSc3d1	válečný
vozu	vůz	k1gInSc3	vůz
<g/>
,	,	kIx,	,
vláčel	vláčet	k5eAaImAgMnS	vláčet
ji	on	k3xPp3gFnSc4	on
kolem	kolem	k7c2	kolem
hradeb	hradba	k1gFnPc2	hradba
Tróje	Trója	k1gFnSc2	Trója
a	a	k8xC	a
potom	potom	k6eAd1	potom
v	v	k7c6	v
achajském	achajský	k2eAgInSc6d1	achajský
táboře	tábor	k1gInSc6	tábor
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c4	na
pospas	pospas	k1gInSc4	pospas
psům	pes	k1gMnPc3	pes
a	a	k8xC	a
supům	sup	k1gMnPc3	sup
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
však	však	k9	však
vložili	vložit	k5eAaPmAgMnP	vložit
opět	opět	k6eAd1	opět
bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
když	když	k8xS	když
starý	starý	k2eAgMnSc1d1	starý
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Hektórův	Hektórův	k2eAgMnSc1d1	Hektórův
otec	otec	k1gMnSc1	otec
Priamos	Priamos	k1gMnSc1	Priamos
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
k	k	k7c3	k
Achilleovi	Achilleus	k1gMnSc3	Achilleus
s	s	k7c7	s
prosbami	prosba	k1gFnPc7	prosba
a	a	k8xC	a
dary	dar	k1gInPc7	dar
<g/>
,	,	kIx,	,
Achilleus	Achilleus	k1gMnSc1	Achilleus
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
obměkčit	obměkčit	k5eAaPmF	obměkčit
a	a	k8xC	a
mrtvolu	mrtvola	k1gFnSc4	mrtvola
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
králi	král	k1gMnSc3	král
vydal	vydat	k5eAaPmAgMnS	vydat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Netrvalo	trvat	k5eNaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
válečnou	válečná	k1gFnSc7	válečná
lstí	lest	k1gFnPc2	lest
se	se	k3xPyFc4	se
Řekové	Řek	k1gMnPc1	Řek
dostali	dostat	k5eAaPmAgMnP	dostat
za	za	k7c4	za
hradby	hradba	k1gFnPc4	hradba
města	město	k1gNnSc2	město
a	a	k8xC	a
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
je	on	k3xPp3gNnSc4	on
vypálili	vypálit	k5eAaPmAgMnP	vypálit
a	a	k8xC	a
proměnili	proměnit	k5eAaPmAgMnP	proměnit
v	v	k7c4	v
hromadu	hromada	k1gFnSc4	hromada
rozvalin	rozvalina	k1gFnPc2	rozvalina
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
však	však	k9	však
ještě	ještě	k6eAd1	ještě
položilo	položit	k5eAaPmAgNnS	položit
život	život	k1gInSc4	život
mnoho	mnoho	k4c1	mnoho
mužů	muž	k1gMnPc2	muž
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
válčících	válčící	k2eAgFnPc2d1	válčící
stran	strana	k1gFnPc2	strana
včetně	včetně	k7c2	včetně
Parida	Paris	k1gMnSc2	Paris
a	a	k8xC	a
Achillea	Achilleus	k1gMnSc2	Achilleus
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nejen	nejen	k6eAd1	nejen
to	ten	k3xDgNnSc4	ten
-	-	kIx~	-
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Tróje	Trója	k1gFnSc2	Trója
byli	být	k5eAaImAgMnP	být
většinou	většina	k1gFnSc7	většina
pobiti	pobit	k2eAgMnPc1d1	pobit
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc4	žena
odvlečeny	odvlečen	k2eAgFnPc4d1	odvlečena
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Hektorova	Hektorův	k2eAgFnSc1d1	Hektorova
manželka	manželka	k1gFnSc1	manželka
Andromaché	Andromachý	k2eAgFnSc2d1	Andromachý
stala	stát	k5eAaPmAgFnS	stát
kořistí	kořist	k1gFnSc7	kořist
Achilleova	Achilleův	k2eAgMnSc4d1	Achilleův
syna	syn	k1gMnSc4	syn
Neoptolema	Neoptolem	k1gMnSc4	Neoptolem
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
zneuctil	zneuctít	k5eAaPmAgMnS	zneuctít
a	a	k8xC	a
odvedl	odvést	k5eAaPmAgMnS	odvést
jako	jako	k9	jako
otrokyni	otrokyně	k1gFnSc4	otrokyně
do	do	k7c2	do
Fthíe	Fthí	k1gFnSc2	Fthí
<g/>
.	.	kIx.	.
</s>
<s>
Jejího	její	k3xOp3gMnSc4	její
malého	malý	k2eAgMnSc4d1	malý
syna	syn	k1gMnSc4	syn
Astyanakta	Astyanakt	k1gMnSc4	Astyanakt
jí	jíst	k5eAaImIp3nS	jíst
vytrhl	vytrhnout	k5eAaPmAgMnS	vytrhnout
jakýsi	jakýsi	k3yIgMnSc1	jakýsi
surový	surový	k2eAgMnSc1d1	surový
voják	voják	k1gMnSc1	voják
z	z	k7c2	z
náruče	náruč	k1gFnSc2	náruč
a	a	k8xC	a
shodil	shodit	k5eAaPmAgMnS	shodit
ho	on	k3xPp3gMnSc4	on
z	z	k7c2	z
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
</s>
</p>
<p>
<s>
Graves	Graves	k1gMnSc1	Graves
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
Řecké	řecký	k2eAgInPc4d1	řecký
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7309-153-4	[number]	k4	80-7309-153-4
</s>
</p>
<p>
<s>
Houtzager	Houtzager	k1gInSc1	Houtzager
<g/>
,	,	kIx,	,
Guus	Guus	k1gInSc1	Guus
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7234-287-8	[number]	k4	80-7234-287-8
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Löwe	Löw	k1gInSc2	Löw
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnPc1	antika
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Trojská	trojský	k2eAgFnSc1d1	Trojská
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hektór	Hektóra	k1gFnPc2	Hektóra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
