<s>
Biologická	biologický	k2eAgFnSc1d1	biologická
ochrana	ochrana	k1gFnSc1	ochrana
letišť	letiště	k1gNnPc2	letiště
(	(	kIx(	(
<g/>
ornitologické	ornitologický	k2eAgNnSc1d1	ornitologické
zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
letišť	letiště	k1gNnPc2	letiště
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrn	souhrn	k1gInSc4	souhrn
preventivních	preventivní	k2eAgNnPc2d1	preventivní
pasivních	pasivní	k2eAgNnPc2d1	pasivní
a	a	k8xC	a
aktivních	aktivní	k2eAgNnPc2d1	aktivní
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
zaměřených	zaměřený	k2eAgNnPc2d1	zaměřené
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
migrace	migrace	k1gFnSc2	migrace
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
zvěrstva	zvěrstvo	k1gNnSc2	zvěrstvo
v	v	k7c6	v
letištních	letištní	k2eAgInPc6d1	letištní
prostorech	prostor	k1gInPc6	prostor
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
minimalizaci	minimalizace	k1gFnSc3	minimalizace
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
střetu	střet	k1gInSc2	střet
letadel	letadlo	k1gNnPc2	letadlo
s	s	k7c7	s
ptactvem	ptactvo	k1gNnSc7	ptactvo
a	a	k8xC	a
zvěří	zvěř	k1gFnSc7	zvěř
<g/>
.	.	kIx.	.
</s>
