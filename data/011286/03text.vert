<p>
<s>
Biologická	biologický	k2eAgFnSc1d1	biologická
ochrana	ochrana	k1gFnSc1	ochrana
letišť	letiště	k1gNnPc2	letiště
(	(	kIx(	(
<g/>
ornitologické	ornitologický	k2eAgNnSc1d1	ornitologické
zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
letišť	letiště	k1gNnPc2	letiště
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrn	souhrn	k1gInSc4	souhrn
preventivních	preventivní	k2eAgNnPc2d1	preventivní
pasivních	pasivní	k2eAgNnPc2d1	pasivní
a	a	k8xC	a
aktivních	aktivní	k2eAgNnPc2d1	aktivní
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
zaměřených	zaměřený	k2eAgNnPc2d1	zaměřené
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
migrace	migrace	k1gFnSc2	migrace
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
zvěrstva	zvěrstvo	k1gNnSc2	zvěrstvo
v	v	k7c6	v
letištních	letištní	k2eAgInPc6d1	letištní
prostorech	prostor	k1gInPc6	prostor
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
minimalizaci	minimalizace	k1gFnSc3	minimalizace
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
střetu	střet	k1gInSc2	střet
letadel	letadlo	k1gNnPc2	letadlo
s	s	k7c7	s
ptactvem	ptactvo	k1gNnSc7	ptactvo
a	a	k8xC	a
zvěří	zvěř	k1gFnSc7	zvěř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
letištích	letiště	k1gNnPc6	letiště
se	se	k3xPyFc4	se
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
úkolů	úkol	k1gInPc2	úkol
ornitologického	ornitologický	k2eAgNnSc2d1	ornitologické
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
skupina	skupina	k1gFnSc1	skupina
pracovníků	pracovník	k1gMnPc2	pracovník
podle	podle	k7c2	podle
potřeb	potřeba	k1gFnPc2	potřeba
daného	daný	k2eAgNnSc2d1	dané
letiště	letiště	k1gNnSc2	letiště
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
pracovníků	pracovník	k1gMnPc2	pracovník
podle	podle	k7c2	podle
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
ornitologické	ornitologický	k2eAgFnSc2d1	ornitologická
situace	situace	k1gFnSc2	situace
na	na	k7c6	na
daném	daný	k2eAgNnSc6d1	dané
letišti	letiště	k1gNnSc6	letiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
ornitologického	ornitologický	k2eAgNnSc2d1	ornitologické
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
plní	plnit	k5eAaImIp3nP	plnit
tyto	tento	k3xDgInPc1	tento
úkoly	úkol	k1gInPc1	úkol
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
včas	včas	k6eAd1	včas
získává	získávat	k5eAaImIp3nS	získávat
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
správně	správně	k6eAd1	správně
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
ornitologickou	ornitologický	k2eAgFnSc4d1	ornitologická
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
realizuje	realizovat	k5eAaBmIp3nS	realizovat
preventivní	preventivní	k2eAgInSc1d1	preventivní
pasivní	pasivní	k2eAgInSc1d1	pasivní
a	a	k8xC	a
aktivní	aktivní	k2eAgNnSc1d1	aktivní
opatření	opatření	k1gNnSc1	opatření
obecného	obecný	k2eAgInSc2d1	obecný
i	i	k8xC	i
specifického	specifický	k2eAgInSc2d1	specifický
charakteru	charakter	k1gInSc2	charakter
,	,	kIx,	,
která	který	k3yQgNnPc1	který
minimalizují	minimalizovat	k5eAaBmIp3nP	minimalizovat
výskyt	výskyt	k1gInSc4	výskyt
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
zvěře	zvěř	k1gFnSc2	zvěř
na	na	k7c6	na
letištích	letiště	k1gNnPc6	letiště
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
realizuje	realizovat	k5eAaBmIp3nS	realizovat
aktivní	aktivní	k2eAgNnSc1d1	aktivní
opatření	opatření	k1gNnSc1	opatření
k	k	k7c3	k
plašení	plašení	k1gNnSc3	plašení
ptactva	ptactvo	k1gNnSc2	ptactvo
v	v	k7c6	v
ochranných	ochranný	k2eAgNnPc6d1	ochranné
ornitologických	ornitologický	k2eAgNnPc6d1	ornitologické
pásmech	pásmo	k1gNnPc6	pásmo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
ornitologické	ornitologický	k2eAgFnSc6d1	ornitologická
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
příčin	příčina	k1gFnPc2	příčina
vzniku	vznik	k1gInSc2	vznik
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
situací	situace	k1gFnPc2	situace
zaviněných	zaviněný	k2eAgFnPc2d1	zaviněná
ornitologickou	ornitologický	k2eAgFnSc7d1	ornitologická
situací	situace	k1gFnSc7	situace
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
výsledky	výsledek	k1gInPc4	výsledek
ornitologického	ornitologický	k2eAgInSc2d1	ornitologický
průzkumu	průzkum	k1gInSc2	průzkum
do	do	k7c2	do
tabulek	tabulka	k1gFnPc2	tabulka
,	,	kIx,	,
grafů	graf	k1gInPc2	graf
a	a	k8xC	a
map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
odběr	odběr	k1gInSc1	odběr
zbytků	zbytek	k1gInPc2	zbytek
ptáků	pták	k1gMnPc2	pták
po	po	k7c6	po
střetu	střet	k1gInSc6	střet
letadel	letadlo	k1gNnPc2	letadlo
s	s	k7c7	s
ptactvem	ptactvo	k1gNnSc7	ptactvo
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
následnou	následný	k2eAgFnSc4d1	následná
identifikaci	identifikace	k1gFnSc4	identifikace
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
aktivnímu	aktivní	k2eAgNnSc3d1	aktivní
plašení	plašení	k1gNnSc3	plašení
ptactva	ptactvo	k1gNnSc2	ptactvo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pyrotechnická	pyrotechnický	k2eAgFnSc1d1	pyrotechnická
–	–	k?	–
použití	použití	k1gNnSc3	použití
výbušek	výbuška	k1gFnPc2	výbuška
k	k	k7c3	k
plašení	plašení	k1gNnSc3	plašení
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc1	použití
brokových	brokový	k2eAgFnPc2d1	broková
zbraní	zbraň	k1gFnPc2	zbraň
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Biologická	biologický	k2eAgFnSc1d1	biologická
–	–	k?	–
použití	použití	k1gNnSc4	použití
loveckých	lovecký	k2eAgMnPc2d1	lovecký
dravců	dravec	k1gMnPc2	dravec
a	a	k8xC	a
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostatní	ostatní	k2eAgMnSc1d1	ostatní
–	–	k?	–
využívání	využívání	k1gNnSc6	využívání
dalších	další	k2eAgInPc2d1	další
prostředků	prostředek	k1gInPc2	prostředek
k	k	k7c3	k
rušení	rušení	k1gNnSc3	rušení
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://biologicka-ochrana-letist.cz/	[url]	k?	http://biologicka-ochrana-letist.cz/
</s>
</p>
