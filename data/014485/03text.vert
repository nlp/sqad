<s>
Mordechaj	Mordechaj	k1gFnSc1
Cipori	Cipori	k1gFnSc2
</s>
<s>
Mordechaj	Mordechaj	k1gMnSc1
Ciporiמ	Ciporiמ	k1gMnSc1
צ	צ	k?
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
ministr	ministr	k1gMnSc1
komunikací	komunikace	k1gFnPc2
Izraele	Izrael	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1981	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1984	#num#	k4
Předchůdce	předchůdce	k1gMnPc4
</s>
<s>
Joram	Joram	k6eAd1
Aridor	Aridor	k1gMnSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Amnon	Amnon	k1gMnSc1
Rubinstein	Rubinstein	k1gMnSc1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Likud	Likud	k6eAd1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1924	#num#	k4
Petach	Petach	k1gInSc1
TikvaBritský	TikvaBritský	k2eAgInSc4d1
mandát	mandát	k1gInSc4
Palestina	Palestina	k1gFnSc1
Britský	britský	k2eAgInSc1d1
mandát	mandát	k1gInSc1
Palestina	Palestina	k1gFnSc1
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Izrael	Izrael	k1gInSc1
<g/>
)	)	kIx)
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
92	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Ramat	Ramat	k1gInSc1
GanIzrael	GanIzrael	k1gMnSc1
Izrael	Izrael	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Hřbitov	hřbitov	k1gInSc1
v	v	k7c6
Sgule	Sgula	k1gFnSc6
Kneset	Kneseta	k1gFnPc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Telavivská	telavivský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Profese	profese	k1gFnSc1
</s>
<s>
politik	politik	k1gMnSc1
a	a	k8xC
důstojník	důstojník	k1gMnSc1
Commons	Commonsa	k1gFnPc2
</s>
<s>
Mordechai	Mordechai	k1gNnSc1
Tzipori	Tzipor	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mordechaj	Mordechaj	k1gFnSc1
Cipori	Cipor	k1gFnSc2
<g/>
,	,	kIx,
hebrejsky	hebrejsky	k6eAd1
מ	מ	k?
צ	צ	k?
<g/>
,	,	kIx,
rodným	rodný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Mordechaj	Mordechaj	k1gMnSc1
Hankovič-Hendin	Hankovič-Hendin	k2eAgMnSc1d1
<g/>
,	,	kIx,
hebrejsky	hebrejsky	k6eAd1
מ	מ	k?
ה	ה	k?
<g/>
׳	׳	k?
<g/>
-ה	-ה	k?
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1924	#num#	k4
Petach	Petacha	k1gFnPc2
Tikva	Tikva	k1gFnSc1
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
Ramat	Ramat	k1gInSc4
Gan	Gan	k1gFnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
izraelský	izraelský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1981	#num#	k4
až	až	k9
1984	#num#	k4
zastával	zastávat	k5eAaImAgMnS
post	post	k1gInSc4
ministra	ministr	k1gMnSc2
komunikací	komunikace	k1gFnPc2
v	v	k7c6
izraelské	izraelský	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Petach	Peta	k1gFnPc6
Tikvě	Tikva	k1gFnSc6
ještě	ještě	k9
během	během	k7c2
období	období	k1gNnSc2
britské	britský	k2eAgFnSc2d1
mandátní	mandátní	k2eAgFnSc2d1
Palestiny	Palestina	k1gFnSc2
a	a	k8xC
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
náboženské	náboženský	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
Irgunu	Irgun	k1gInSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
byl	být	k5eAaImAgMnS
zatčen	zatknout	k5eAaPmNgMnS
britskou	britský	k2eAgFnSc7d1
správou	správa	k1gFnSc7
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
členy	člen	k1gMnPc7
Irgunu	Irgun	k1gInSc2
a	a	k8xC
Lechi	Lech	k1gFnSc2
internován	internovat	k5eAaBmNgInS
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
Eritreji	Eritrea	k1gFnSc6
<g/>
,	,	kIx,
Keni	Keňa	k1gFnSc6
a	a	k8xC
Súdánu	Súdán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
internačního	internační	k2eAgInSc2d1
tábora	tábor	k1gInSc2
v	v	k7c6
Eritreji	Eritrea	k1gFnSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
uprchnout	uprchnout	k5eAaPmF
<g/>
,	,	kIx,
posléze	posléze	k6eAd1
však	však	k9
byl	být	k5eAaImAgInS
chycen	chytit	k5eAaPmNgInS
a	a	k8xC
zadržován	zadržovat	k5eAaImNgInS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Izrael	Izrael	k1gInSc1
vyhlásil	vyhlásit	k5eAaPmAgInS
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vzniku	vznik	k1gInSc6
státu	stát	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
profesionálním	profesionální	k2eAgMnSc7d1
vojákem	voják	k1gMnSc7
Izraelských	izraelský	k2eAgFnPc2d1
obranných	obranný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
(	(	kIx(
<g/>
IOS	IOS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
působil	působit	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jeho	jeho	k3xOp3gFnSc2
služby	služba	k1gFnSc2
v	v	k7c6
armádě	armáda	k1gFnSc6
studoval	studovat	k5eAaImAgMnS
na	na	k7c4
britské	britský	k2eAgNnSc4d1
College	College	k1gNnSc4
for	forum	k1gNnPc2
Command	Commanda	k1gFnPc2
and	and	k?
Staff	Staff	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
úspěšně	úspěšně	k6eAd1
absolvoval	absolvovat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
Telavivské	telavivský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1962	#num#	k4
až	až	k9
1965	#num#	k4
byl	být	k5eAaImAgMnS
velitelem	velitel	k1gMnSc7
obrněného	obrněný	k2eAgInSc2d1
praporu	prapor	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
velitelem	velitel	k1gMnSc7
vojenské	vojenský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
zástupcem	zástupce	k1gMnSc7
velitelem	velitel	k1gMnSc7
obrněných	obrněný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
velitelem	velitel	k1gMnSc7
velitelství	velitelství	k1gNnSc2
a	a	k8xC
štábu	štáb	k1gInSc2
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
zástupcem	zástupce	k1gMnSc7
velitele	velitel	k1gMnSc2
operací	operace	k1gFnPc2
při	při	k7c6
Generálním	generální	k2eAgInSc6d1
štábu	štáb	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
armády	armáda	k1gFnSc2
v	v	k7c6
hodnosti	hodnost	k1gFnSc6
generálporučíka	generálporučík	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Následující	následující	k2eAgInSc1d1
rok	rok	k1gInSc1
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
v	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
poslancem	poslanec	k1gMnSc7
za	za	k7c4
stranu	strana	k1gFnSc4
Likud	Likuda	k1gFnPc2
a	a	k8xC
v	v	k7c6
červnu	červen	k1gInSc6
1977	#num#	k4
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
náměstkem	náměstek	k1gMnSc7
ministra	ministr	k1gMnSc2
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dalších	další	k2eAgFnPc6d1
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
ministrem	ministr	k1gMnSc7
komunikací	komunikace	k1gFnPc2
a	a	k8xC
mezi	mezi	k7c7
srpnem	srpen	k1gInSc7
1981	#num#	k4
a	a	k8xC
říjnem	říjen	k1gInSc7
1983	#num#	k4
byl	být	k5eAaImAgInS
krátce	krátce	k6eAd1
opět	opět	k6eAd1
náměstkem	náměstek	k1gMnSc7
ministra	ministr	k1gMnSc2
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
svůj	svůj	k3xOyFgInSc4
poslanecký	poslanecký	k2eAgInSc4d1
mandát	mandát	k1gInSc4
přišel	přijít	k5eAaPmAgMnS
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Mordechai	Mordechai	k1gNnSc2
Tzipori	Tzipor	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mordechaj	Mordechaj	k1gMnSc1
Cipori	Cipor	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kneset	Kneset	k1gMnSc1
–	–	k?
Mordechaj	Mordechaj	k1gMnSc1
Cipori	Cipor	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Ministři	ministr	k1gMnPc1
komunikací	komunikace	k1gFnPc2
Izraele	Izrael	k1gInSc2
</s>
<s>
Nurok	Nurok	k1gInSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Burg	Burg	k1gInSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
–	–	k?
<g/>
58	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Barzilaj	Barzilaj	k1gFnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
–	–	k?
<g/>
59	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Minc	Minc	k1gFnSc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
61	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sason	Sason	k1gNnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
–	–	k?
<g/>
67	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ješa	Ješa	k1gFnSc1
<g/>
'	'	kIx"
<g/>
jahu	jahu	k6eAd1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
–	–	k?
<g/>
69	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rimalt	Rimalt	k1gInSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Peres	Peres	k1gMnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
–	–	k?
<g/>
74	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Uzan	Uzan	k1gNnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rabin	Rabin	k1gInSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Uzan	Uzan	k1gNnSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
–	–	k?
<g/>
77	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Begin	Begin	k1gInSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Amit	Amit	k1gInSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
–	–	k?
<g/>
78	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Moda	Moda	k1gFnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
80	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Aridor	Aridor	k1gInSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cipori	Cipori	k1gNnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
–	–	k?
<g/>
84	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rubinstein	Rubinstein	k1gInSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
–	–	k?
<g/>
87	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akovi	akoev	k1gFnSc6
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pinchasi	Pinchas	k1gMnPc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
92	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Šachal	Šachal	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
93	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Aloniová	Aloniový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Livnat	Livnat	k5eAaImF,k5eAaPmF
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
99	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ben	Ben	k1gInSc1
Eli	Eli	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ezer	ezer	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rivlin	Rivlin	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Šaron	Šaron	k1gNnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Olmert	Olmert	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Icik	Icik	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hirschson	Hirschson	k1gNnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Atias	Atias	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kachlon	Kachlon	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Erdan	Erdan	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Netanjahu	Netanjaha	k1gFnSc4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Qará	Qará	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Amsalem	Amsal	k1gInSc7
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hendel	Hendlo	k1gNnPc2
(	(	kIx(
<g/>
od	od	k7c2
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Členové	člen	k1gMnPc1
desátého	desátý	k4xOgInSc2
Knesetu	Kneset	k1gInSc2
zvoleného	zvolený	k2eAgInSc2d1
roku	rok	k1gInSc2
1981	#num#	k4
Likud	Likuda	k1gFnPc2
</s>
<s>
Arens	Arens	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Kleiner	Kleiner	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Aridor	Aridor	k1gInSc1
•	•	k?
Begin	Begin	k1gInSc1
•	•	k?
Ben	Ben	k1gInSc1
Elisar	Elisar	k1gMnSc1
•	•	k?
Berman	Berman	k1gMnSc1
•	•	k?
Cipori	Cipor	k1gFnSc2
•	•	k?
Dekel	Dekel	k1gMnSc1
•	•	k?
Doron	Doron	k1gMnSc1
•	•	k?
Erlich	Erlich	k1gMnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Hirschson	Hirschson	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Glazer-Ta	Glazer-Ta	k1gFnSc1
<g/>
'	'	kIx"
<g/>
asa	asa	k?
•	•	k?
Goldstein	Goldstein	k1gMnSc1
•	•	k?
Gruper	Gruper	k1gMnSc1
•	•	k?
Kacav	Kacav	k1gInSc1
•	•	k?
Kaufman	Kaufman	k1gMnSc1
•	•	k?
Kohen-Avidov	Kohen-Avidov	k1gInSc1
•	•	k?
Kohen-Orgad	Kohen-Orgad	k1gInSc1
•	•	k?
Kohen	Kohen	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Korfu	Korfu	k1gNnSc4
•	•	k?
Kulas	Kulas	k1gInSc1
•	•	k?
Levy	Levy	k?
•	•	k?
Lin	Lina	k1gFnPc2
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Ma	Ma	k1gFnSc2
<g/>
'	'	kIx"
<g/>
arach	arach	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Livni	Liveň	k1gFnSc6
•	•	k?
Magen	Magna	k1gFnPc2
•	•	k?
Meridor	Meridor	k1gMnSc1
•	•	k?
Milo	milo	k6eAd1
•	•	k?
Moda	Mod	k1gInSc2
<g/>
'	'	kIx"
<g/>
i	i	k9
•	•	k?
Nasruddín	Nasruddín	k1gMnSc1
•	•	k?
Nisim	Nisim	k1gMnSc1
•	•	k?
Nof	Nof	k1gMnSc1
•	•	k?
Olmert	Olmert	k1gMnSc1
•	•	k?
Pat	pat	k1gInSc1
•	•	k?
Perach	Perach	k1gInSc1
•	•	k?
Perec	Perec	k1gInSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgInS
do	do	k7c2
Ma	Ma	k1gFnSc2
<g/>
'	'	kIx"
<g/>
arach	arach	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Reisser	Reisser	k1gMnSc1
•	•	k?
Rener	Rener	k1gMnSc1
•	•	k?
Rom	Rom	k1gMnSc1
•	•	k?
Savidor	Savidor	k1gMnSc1
•	•	k?
Seiger	Seiger	k1gMnSc1
•	•	k?
Šalita	Šalita	k1gMnSc1
•	•	k?
Šamir	Šamir	k1gMnSc1
•	•	k?
Šarir	Šarir	k1gMnSc1
•	•	k?
Šaron	Šaron	k1gMnSc1
•	•	k?
Šifman	Šifman	k1gMnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Weinstein	Weinstein	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Šilansky	Šilansky	k1gFnSc1
•	•	k?
Šítrit	Šítrita	k1gFnPc2
•	•	k?
Šostak	Šostak	k1gMnSc1
•	•	k?
Tichon	Tichon	k1gMnSc1
•	•	k?
Zeigerman	Zeigerman	k1gMnSc1
Ma	Ma	k1gMnSc1
<g/>
'	'	kIx"
<g/>
arach	arach	k1gMnSc1
</s>
<s>
Amir	Amir	k1gInSc1
•	•	k?
Amora	Amor	k1gMnSc2
<g/>
'	'	kIx"
<g/>
i	i	k9
•	•	k?
Arad	Arad	k1gMnSc1
•	•	k?
Arbeli-Almozlino	Arbeli-Almozlin	k2eAgNnSc1d1
•	•	k?
Bar	bar	k1gInSc1
Lev	Lev	k1gMnSc1
•	•	k?
Bar	bar	k1gInSc1
Zohar	Zohar	k1gInSc1
•	•	k?
Bar	bar	k1gInSc1
<g/>
'	'	kIx"
<g/>
am	am	k?
•	•	k?
Ben	Ben	k1gInSc1
Me	Me	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ir	ir	k?
•	•	k?
Blumenthal	Blumenthal	k1gMnSc1
•	•	k?
Caban	Caban	k1gMnSc1
•	•	k?
Cur	Cur	k1gMnSc1
•	•	k?
Eban	Eban	k1gMnSc1
•	•	k?
Edri	Edr	k1gFnSc2
•	•	k?
Ešel	Ešel	k1gMnSc1
•	•	k?
Feder	Feder	k1gMnSc1
•	•	k?
Gil	Gil	k1gFnSc1
•	•	k?
Granot	Granota	k1gFnPc2
•	•	k?
Gur	Gur	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Hakohen	Hakohen	k2eAgInSc1d1
•	•	k?
Harel	Harel	k1gInSc1
•	•	k?
Herzog	Herzog	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k9
Raz	razit	k5eAaImRp2nS
<g/>
)	)	kIx)
•	•	k?
Hilel	Hilel	k1gMnSc1
•	•	k?
Chalájla	Chalájla	k1gMnSc1
•	•	k?
Charif	Charif	k1gMnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Solodar	Solodar	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Chariš	Chariš	k1gInSc1
•	•	k?
Chaša	Chaš	k1gInSc2
<g/>
'	'	kIx"
<g/>
i	i	k9
•	•	k?
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akobi	akob	k1gMnSc3
•	•	k?
Kac-Oz	Kac-Oz	k1gMnSc1
•	•	k?
Mešel	Mešel	k1gMnSc1
•	•	k?
Nachmi	Nach	k1gFnPc7
<g/>
'	'	kIx"
<g/>
as	as	k1gInSc1
•	•	k?
Na	na	k7c4
<g/>
'	'	kIx"
<g/>
im	im	k?
•	•	k?
Namir	Namir	k1gMnSc1
•	•	k?
Nechemkin	Nechemkin	k1gMnSc1
•	•	k?
Peres	Peres	k1gMnSc1
•	•	k?
Rabin	Rabin	k1gMnSc1
•	•	k?
Ron	Ron	k1gMnSc1
•	•	k?
Rosolio	Rosolio	k1gMnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Ramon	Ramona	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Sarid	Sarid	k1gInSc1
•	•	k?
Sebag	Sebag	k1gMnSc1
•	•	k?
Šachal	Šachal	k1gMnSc1
•	•	k?
Šem-Tov	Šem-Tov	k1gInSc1
•	•	k?
Speiser	Speiser	k1gInSc1
•	•	k?
Suissa	Suissa	k1gFnSc1
•	•	k?
Vatad	Vatad	k1gInSc1
•	•	k?
Weiss	Weiss	k1gMnSc1
•	•	k?
Zakaj	Zakaj	k1gMnSc1
•	•	k?
Zakin	Zakin	k1gMnSc1
Mafdal	Mafdal	k1gMnSc1
</s>
<s>
Avtabi	Avtabi	k6eAd1
•	•	k?
Ben	Ben	k1gInSc1
Me	Me	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ir	ir	k?
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Gešer-Merkaz	Gešer-Merkaz	k1gInSc4
Cijoni	Cijoň	k1gFnSc3
Dati	Dati	k1gNnPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
zpět	zpět	k6eAd1
<g/>
)	)	kIx)
•	•	k?
Burg	Burg	k1gMnSc1
•	•	k?
Drukman	Drukman	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgMnPc4d1
<g/>
)	)	kIx)
•	•	k?
Hammer	Hammer	k1gInSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgInS
do	do	k7c2
Gešer-Merkaz	Gešer-Merkaz	k1gInSc4
Cijoni	Cijoň	k1gFnSc3
Dati	Dati	k1gNnPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
zpět	zpět	k6eAd1
<g/>
)	)	kIx)
•	•	k?
Melamed	Melamed	k1gMnSc1
Agudat	Agudat	k1gMnSc1
Jisra	Jisra	k1gMnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Halpert	Halpert	k1gInSc1
•	•	k?
Lorinc	Lorinc	k1gInSc1
•	•	k?
Poruš	porušit	k5eAaPmRp2nS
•	•	k?
Šapira	Šapira	k1gMnSc1
Chadaš	Chadaš	k1gMnSc1
</s>
<s>
Biton	Biton	k1gMnSc1
•	•	k?
Túbí	Túbí	k1gMnSc1
•	•	k?
Vilner	Vilner	k1gMnSc1
•	•	k?
Zi	Zi	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ad	ad	k7c4
Techija	Techijus	k1gMnSc4
</s>
<s>
Kohen	Kohen	k2eAgInSc1d1
•	•	k?
Ne	ne	k9
<g/>
'	'	kIx"
<g/>
eman	eman	k1gInSc1
•	•	k?
Porat	Porat	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Šiloach	Šiloach	k1gInSc1
<g/>
)	)	kIx)
Tami	Tami	k1gNnSc1
</s>
<s>
Abuchacira	Abuchacira	k1gMnSc1
•	•	k?
Rubin	Rubin	k1gMnSc1
•	•	k?
Uzan	Uzan	k1gInSc1
Telem	Telem	k1gInSc1
</s>
<s>
Dajan	Dajan	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Hurvic	Hurvic	k1gMnSc1
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Rafi-Rešima	Rafi-Rešim	k1gMnSc2
Mamlachtit	Mamlachtit	k1gMnSc2
<g/>
)	)	kIx)
•	•	k?
Ben	Ben	k1gInSc1
Porat	Porat	k1gInSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgInS
do	do	k7c2
Tnu	tnout	k5eAaPmIp1nS,k5eAaBmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
le-hitchadšut	le-hitchadšut	k2eAgInSc1d1
cijonit	cijonit	k1gInSc1
chevratit	chevratit	k5eAaPmF,k5eAaImF
<g/>
)	)	kIx)
Šinuj	Šinuj	k1gFnSc4
</s>
<s>
Rubinstein	Rubinstein	k1gInSc1
•	•	k?
Viršubski	Viršubsk	k1gFnSc2
Rac	Rac	k1gFnSc2
</s>
<s>
Aloni	Alon	k1gMnPc1
(	(	kIx(
<g/>
sloučeno	sloučit	k5eAaPmNgNnS
s	s	k7c7
Ma	Ma	k1gFnSc7
<g/>
'	'	kIx"
<g/>
arach	arach	k1gInSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
opětovné	opětovný	k2eAgNnSc1d1
osamostatnění	osamostatnění	k1gNnSc1
<g/>
)	)	kIx)
poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
abecední	abecední	k2eAgNnSc1d1
řazení	řazení	k1gNnSc1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
podle	podle	k7c2
pozice	pozice	k1gFnSc2
na	na	k7c6
kandidátní	kandidátní	k2eAgFnSc6d1
listině	listina	k1gFnSc6
</s>
<s>
Členové	člen	k1gMnPc1
devátého	devátý	k4xOgInSc2
Knesetu	Kneset	k1gInSc2
zvoleného	zvolený	k2eAgInSc2d1
roku	rok	k1gInSc2
1977	#num#	k4
Likud	Likuda	k1gFnPc2
</s>
<s>
Arens	Arens	k6eAd1
</s>
<s>
Aridor	Aridor	k1gMnSc1
</s>
<s>
Badi	Badi	k1gNnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
</s>
<s>
Begin	Begin	k1gMnSc1
</s>
<s>
Berman	Berman	k1gMnSc1
</s>
<s>
Cipori	Cipori	k6eAd1
</s>
<s>
Dekel	Dekel	k1gMnSc1
</s>
<s>
Doron	Doron	k1gMnSc1
</s>
<s>
Erlich	Erlich	k1gMnSc1
</s>
<s>
Flomin	Flomin	k1gInSc1
</s>
<s>
Gruper	Gruper	k1gMnSc1
</s>
<s>
Hurvic	Hurvic	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Rafi	Raf	k1gFnSc2
–	–	k?
Rešima	Rešima	k1gFnSc1
mamlachtit	mamlachtit	k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Telem	Telum	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Kacav	Kacav	k1gInSc1
</s>
<s>
Kac	Kac	k?
</s>
<s>
Kaufman	Kaufman	k1gMnSc1
</s>
<s>
Ge	Ge	k?
<g/>
'	'	kIx"
<g/>
ula	ula	k?
Kohen	Kohen	k1gInSc1
(	(	kIx(
<g/>
odešla	odejít	k5eAaPmAgFnS
do	do	k7c2
Techija	Techij	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Kohen-Avidov	Kohen-Avidov	k1gInSc1
</s>
<s>
Kohen-Orgad	Kohen-Orgad	k6eAd1
</s>
<s>
Kohen	Kohen	k1gInSc1
</s>
<s>
Korfu	Korfu	k1gNnSc1
</s>
<s>
Levy	Levy	k?
</s>
<s>
Lin	Lina	k1gFnPc2
</s>
<s>
Livni	Livn	k1gMnPc1
</s>
<s>
Meron	Meron	k1gMnSc1
</s>
<s>
Milo	milo	k6eAd1
</s>
<s>
Moda	Moda	k1gFnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
</s>
<s>
Nasruddín	Nasruddín	k1gMnSc1
</s>
<s>
Nisim	Nisim	k6eAd1
</s>
<s>
Olmert	Olmert	k1gMnSc1
</s>
<s>
Pat	pat	k1gInSc1
</s>
<s>
Perec	Perec	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Rafi	Raf	k1gFnSc2
–	–	k?
Rešima	Rešima	k1gFnSc1
mamlachtit	mamlachtit	k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
návrat	návrat	k1gInSc1
do	do	k7c2
Likudu	Likud	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Rechtman	Rechtman	k1gMnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Stern	sternum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Rom	Rom	k1gMnSc1
</s>
<s>
Savidor	Savidor	k1gMnSc1
</s>
<s>
Seidel	Seidel	k1gMnSc1
</s>
<s>
Moše	mocha	k1gFnSc3
Šamir	Šamir	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Techija	Techij	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Jicchak	Jicchak	k1gMnSc1
Šamir	Šamir	k1gMnSc1
</s>
<s>
Šarir	Šarir	k1gMnSc1
</s>
<s>
Šilansky	Šilansky	k6eAd1
</s>
<s>
Šostak	Šostak	k6eAd1
</s>
<s>
Šoval	Šoval	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Rafi	Raf	k1gFnSc2
–	–	k?
Rešima	Rešima	k1gFnSc1
mamlachtit	mamlachtit	k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Telem	Tel	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
Tamir	Tamir	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Weizman	Weizman	k1gMnSc1
Ma	Ma	k1gMnSc1
<g/>
'	'	kIx"
<g/>
arach	arach	k1gMnSc1
</s>
<s>
Alon	Alon	k1gNnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Chaša	Chašus	k1gMnSc2
<g/>
'	'	kIx"
<g/>
i	i	k9
<g/>
)	)	kIx)
</s>
<s>
Amar	Amar	k1gMnSc1
</s>
<s>
Amir	Amir	k1gMnSc1
</s>
<s>
Amora	Amor	k1gMnSc4
<g/>
'	'	kIx"
<g/>
i	i	k9
</s>
<s>
Arbeli-Almozlino	Arbeli-Almozlin	k2eAgNnSc1d1
</s>
<s>
Bar	bar	k1gInSc1
Lev	lev	k1gInSc1
</s>
<s>
Baram	Baram	k6eAd1
</s>
<s>
Cadok	Cadok	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Ron	ron	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Dajan	Dajan	k1gInSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgInS
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgNnSc4d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Telem	Telum	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Eban	Eban	k1gMnSc1
</s>
<s>
Ešel	Ešel	k1gMnSc1
</s>
<s>
Feder	Feder	k1gMnSc1
</s>
<s>
Grossman	Grossman	k1gMnSc1
</s>
<s>
Hakohen	Hakohen	k1gInSc1
</s>
<s>
Hadar	Hadar	k1gMnSc1
</s>
<s>
Hilel	Hilel	k1gMnSc1
</s>
<s>
Chariš	Chariš	k5eAaPmIp2nS
</s>
<s>
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akobi	akobi	k6eAd1
</s>
<s>
Jadlin	Jadlin	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Kac	Kac	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Mešel	Mešel	k1gMnSc1
</s>
<s>
Moj	Moj	k?
<g/>
'	'	kIx"
<g/>
al	ala	k1gFnPc2
</s>
<s>
Namir	Namir	k1gMnSc1
</s>
<s>
Navon	Navon	k1gNnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Kac-Oz	Kac-Oz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Peres	Peres	k1gMnSc1
</s>
<s>
Rabin	Rabin	k1gMnSc1
</s>
<s>
Rabinovic	Rabinovice	k1gFnPc2
(	(	kIx(
<g/>
pak	pak	k6eAd1
Herlic	Herlice	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Rosolio	Rosolio	k6eAd1
</s>
<s>
Sarid	Sarid	k1gInSc1
</s>
<s>
Šachal	Šachal	k1gMnSc1
</s>
<s>
Speiser	Speiser	k1gMnSc1
</s>
<s>
Talmi	talmi	k2eAgFnSc1d1
</s>
<s>
Zakaj	Zakaj	k1gMnSc1
Daš	Daš	k1gMnSc1
</s>
<s>
Amit	Amit	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Ma	Ma	k1gFnSc2
<g/>
'	'	kIx"
<g/>
arach	arach	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Asad	Asad	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaBmIp1nS,k5eAaPmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Achva	Achvo	k1gNnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Telem	Tel	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
Ataší	Atašit	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Elgrabli	Elgrabnout	k5eAaPmAgMnP
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaBmIp1nS,k5eAaPmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgNnSc4d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Mifleget	Miflegeta	k1gFnPc2
ha-ichud	ha-ichuda	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Golomb	Golomb	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Ma	Ma	k1gFnSc2
<g/>
'	'	kIx"
<g/>
arach	arach	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Halevy	Haleva	k1gFnPc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgInS
do	do	k7c2
Tnu	tnout	k5eAaPmIp1nS,k5eAaBmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
Jadin	Jadin	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaBmIp1nS,k5eAaPmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
pak	pak	k6eAd1
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
Jaguri	Jaguri	k6eAd1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
ad	ad	k7c4
<g/>
)	)	kIx)
</s>
<s>
Nof	Nof	k?
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaPmIp1nS,k5eAaBmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Achva	Achvo	k1gNnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Likudu	Likud	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Rubinstein	Rubinstein	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Tamir	Tamir	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaBmIp1nS,k5eAaPmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
pak	pak	k6eAd1
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
Toledano	Toledana	k1gFnSc5
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Viršubski	Viršubski	k6eAd1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Wertheimer	Wertheimer	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pak	pak	k6eAd1
Levy	Levy	k?
<g/>
)	)	kIx)
</s>
<s>
Zorea	Zorea	k1gFnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Elijahu	Elijaha	k1gFnSc4
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaPmIp1nS,k5eAaBmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Achva	Achvo	k1gNnSc2
<g/>
)	)	kIx)
Mafdal	Mafdal	k1gFnSc1
</s>
<s>
Abuchacira	Abuchacira	k6eAd1
</s>
<s>
Avtabi	Avtabi	k6eAd1
</s>
<s>
Ben	Ben	k1gInSc1
Me	Me	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ir	ir	k?
</s>
<s>
Burg	Burg	k1gMnSc1
</s>
<s>
Drukman	Drukman	k1gMnSc1
</s>
<s>
Glass	Glass	k6eAd1
</s>
<s>
Hammer	Hammer	k1gMnSc1
</s>
<s>
Melamed	Melamed	k1gMnSc1
</s>
<s>
Rubin	Rubin	k1gMnSc1
</s>
<s>
Scheinman	Scheinman	k1gMnSc1
</s>
<s>
Stern-Katan	Stern-Katan	k1gInSc1
</s>
<s>
Warhaftig	Warhaftig	k1gMnSc1
Chadaš	Chadaš	k1gMnSc1
</s>
<s>
Biton	Biton	k1gMnSc1
</s>
<s>
Mújás	Mújás	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Levenbraun	Levenbraun	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Túbí	Túbí	k6eAd1
</s>
<s>
Vilner	Vilner	k1gMnSc1
</s>
<s>
Zi	Zi	k?
<g/>
'	'	kIx"
<g/>
ad	ad	k7c4
Agudat	Agudat	k1gFnSc4
Jisra	Jisr	k1gInSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Abramovič	Abramovič	k1gMnSc1
</s>
<s>
Gross	Gross	k1gMnSc1
</s>
<s>
Lorinc	Lorinc	k6eAd1
</s>
<s>
Poruš	porušit	k5eAaPmRp2nS
Machane	Machan	k1gMnSc5
smol	smolit	k5eAaImRp2nS
le-Jisra	le-Jisra	k1gMnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Eli	Eli	k?
<g/>
'	'	kIx"
<g/>
av	av	k?
(	(	kIx(
<g/>
pak	pak	k6eAd1
Avnery	Avnera	k1gFnPc4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pak	pak	k6eAd1
Jahjá	Jahjá	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pa	Pa	kA
<g/>
'	'	kIx"
<g/>
il	il	k?
(	(	kIx(
<g/>
pak	pak	k6eAd1
Marci	Marek	k1gMnPc1
<g/>
'	'	kIx"
<g/>
ano	ano	k9
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Mifleget	Miflegeta	k1gFnPc2
ha-ichud	ha-ichuda	k1gFnPc2
<g/>
)	)	kIx)
Šlomcijon	Šlomcijon	k1gInSc1
</s>
<s>
Šaron	Šaron	k1gInSc1
(	(	kIx(
<g/>
sloučeno	sloučit	k5eAaPmNgNnS
s	s	k7c7
Likudem	Likud	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
Jicchaki	Jicchak	k1gFnPc1
(	(	kIx(
<g/>
sloučeno	sloučit	k5eAaPmNgNnS
s	s	k7c7
Likud	Likud	k1gInSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
Jisra	Jisra	k1gFnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
achat	achat	k5eAaImF
<g/>
)	)	kIx)
Pituach	Pituach	k1gInSc1
ve-šalom	ve-šalom	k1gInSc1
</s>
<s>
Flatto-Šaron	Flatto-Šaron	k1gMnSc1
Rac	Rac	k1gMnSc1
</s>
<s>
Aloni	Alon	k1gMnPc1
Liberalim	Liberalim	k1gMnSc1
acma	acma	k1gMnSc1
<g/>
'	'	kIx"
<g/>
im	im	k?
</s>
<s>
Hausner	Hausner	k1gMnSc1
Po	po	k7c4
<g/>
'	'	kIx"
<g/>
alej	alej	k1gFnSc4
Agudat	Agudat	k1gFnSc2
Jisra	Jisr	k1gInSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Kahana	Kahana	k1gFnSc1
Sjednoc	Sjednoc	k1gFnSc1
<g/>
.	.	kIx.
arab	arab	k1gMnSc1
<g/>
.	.	kIx.
kandidátka	kandidátka	k1gFnSc1
</s>
<s>
az-Zuabí	az-Zuabit	k5eAaPmIp3nP
(	(	kIx(
<g/>
pak	pak	k6eAd1
Abú	abú	k1gMnSc1
Rabía	Rabía	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pak	pak	k6eAd1
Muadí	Muadí	k1gNnSc1
<g/>
)	)	kIx)
*	*	kIx~
abecední	abecední	k2eAgNnSc1d1
řazení	řazení	k1gNnSc1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
podle	podle	k7c2
pozice	pozice	k1gFnSc2
na	na	k7c6
kandidátní	kandidátní	k2eAgFnSc6d1
listině	listina	k1gFnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
123687438	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1445	#num#	k4
8117	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
98013262	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
61457882	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
98013262	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
