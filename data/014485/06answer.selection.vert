<s>
Mordechaj	Mordechaj	k1gFnSc1
Cipori	Cipor	k1gFnSc2
<g/>
,	,	kIx,
hebrejsky	hebrejsky	k6eAd1
מ	מ	k?
צ	צ	k?
<g/>
,	,	kIx,
rodným	rodný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Mordechaj	Mordechaj	k1gMnSc1
Hankovič-Hendin	Hankovič-Hendin	k2eAgMnSc1d1
<g/>
,	,	kIx,
hebrejsky	hebrejsky	k6eAd1
מ	מ	k?
ה	ה	k?
<g/>
׳	׳	k?
<g/>
-ה	-ה	k?
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1924	#num#	k4
Petach	Petacha	k1gFnPc2
Tikva	Tikva	k1gFnSc1
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
Ramat	Ramat	k1gInSc4
Gan	Gan	k1gFnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
izraelský	izraelský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1981	#num#	k4
až	až	k9
1984	#num#	k4
zastával	zastávat	k5eAaImAgMnS
post	post	k1gInSc4
ministra	ministr	k1gMnSc2
komunikací	komunikace	k1gFnPc2
v	v	k7c6
izraelské	izraelský	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
