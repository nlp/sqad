<p>
<s>
Devětsil	Devětsil	k1gInSc1	Devětsil
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc4d1	český
levicový	levicový	k2eAgInSc4d1	levicový
umělecký	umělecký	k2eAgInSc4d1	umělecký
svaz	svaz	k1gInSc4	svaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
až	až	k9	až
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
Devětsilu	Devětsil	k1gInSc2	Devětsil
==	==	k?	==
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1920	[number]	k4	1920
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Umělecký	umělecký	k2eAgInSc1d1	umělecký
svaz	svaz	k1gInSc1	svaz
Devětsil	Devětsil	k1gInSc1	Devětsil
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
skupinou	skupina	k1gFnSc7	skupina
socialisticky	socialisticky	k6eAd1	socialisticky
orientovaných	orientovaný	k2eAgMnPc2d1	orientovaný
výtvarníků	výtvarník	k1gMnPc2	výtvarník
a	a	k8xC	a
literátů	literát	k1gMnPc2	literát
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
poznali	poznat	k5eAaPmAgMnP	poznat
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
školním	školní	k2eAgNnSc6d1	školní
prostředí	prostředí	k1gNnSc6	prostředí
gymnázií	gymnázium	k1gNnPc2	gymnázium
v	v	k7c6	v
Křemencově	křemencově	k6eAd1	křemencově
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvními	první	k4xOgInPc7	první
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
Artuš	Artuš	k1gMnSc1	Artuš
Černík	Černík	k1gMnSc1	Černík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Frič	Frič	k1gMnSc1	Frič
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Hoffmeister	Hoffmeister	k1gMnSc1	Hoffmeister
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Prox	Prox	k1gInSc1	Prox
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Suk	Suk	k1gMnSc1	Suk
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Süss	Süss	k1gInSc1	Süss
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Štulc	Štulc	k1gFnSc1	Štulc
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gFnSc2	Teig
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Vaněk	Vaněk	k1gMnSc1	Vaněk
(	(	kIx(	(
<g/>
malíř	malíř	k1gMnSc1	malíř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Veselík	Veselík	k1gMnSc1	Veselík
a	a	k8xC	a
Alois	Alois	k1gMnSc1	Alois
Wachsman	Wachsman	k1gMnSc1	Wachsman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
brněnská	brněnský	k2eAgFnSc1d1	brněnská
pobočka	pobočka	k1gFnSc1	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
Devětsil	Devětsil	k1gInSc1	Devětsil
je	být	k5eAaImIp3nS	být
seskupení	seskupení	k1gNnSc1	seskupení
českých	český	k2eAgMnPc2d1	český
avantgardních	avantgardní	k2eAgMnPc2d1	avantgardní
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
však	však	k9	však
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
literáty	literát	k1gMnPc4	literát
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c4	o
herce	herec	k1gMnPc4	herec
<g/>
,	,	kIx,	,
výtvarníky	výtvarník	k1gMnPc4	výtvarník
<g/>
,	,	kIx,	,
hudebníky	hudebník	k1gMnPc4	hudebník
apod.	apod.	kA	apod.
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
Devětsilu	Devětsil	k1gInSc2	Devětsil
věnovali	věnovat	k5eAaImAgMnP	věnovat
proletářskému	proletářský	k2eAgNnSc3d1	proletářské
umění	umění	k1gNnSc3	umění
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
magickému	magický	k2eAgInSc3d1	magický
realismu	realismus	k1gInSc3	realismus
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
angažovali	angažovat	k5eAaBmAgMnP	angažovat
v	v	k7c6	v
poetismu	poetismus	k1gInSc6	poetismus
<g/>
.	.	kIx.	.
</s>
<s>
Klíčové	klíčový	k2eAgInPc1d1	klíčový
sborníky	sborník	k1gInPc1	sborník
Devětsil	Devětsil	k1gInSc1	Devětsil
a	a	k8xC	a
Život	život	k1gInSc1	život
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Svaz	svaz	k1gInSc4	svaz
moderní	moderní	k2eAgFnSc2d1	moderní
kultury	kultura	k1gFnSc2	kultura
Devětsil	Devětsil	k1gInSc1	Devětsil
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnPc2	součást
spolku	spolek	k1gInSc2	spolek
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
správou	správa	k1gFnSc7	správa
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1925	[number]	k4	1925
stalo	stát	k5eAaPmAgNnS	stát
Osvobozené	osvobozený	k2eAgNnSc1d1	osvobozené
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
<g/>
Svoji	svůj	k3xOyFgFnSc4	svůj
premiéru	premiéra	k1gFnSc4	premiéra
si	se	k3xPyFc3	se
Devětsil	Devětsil	k1gInSc1	Devětsil
odbyl	odbýt	k5eAaPmAgInS	odbýt
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Mozarteu	Mozarteus	k1gInSc6	Mozarteus
<g/>
.	.	kIx.	.
</s>
<s>
Představil	představit	k5eAaPmAgMnS	představit
zde	zde	k6eAd1	zde
svůj	svůj	k3xOyFgInSc4	svůj
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
členy	člen	k1gMnPc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Devětsilu	Devětsil	k1gInSc2	Devětsil
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
umělecké	umělecký	k2eAgFnSc3d1	umělecká
revoluci	revoluce	k1gFnSc3	revoluce
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
organizovanou	organizovaný	k2eAgFnSc4d1	organizovaná
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nemůže	moct	k5eNaImIp3nS	moct
nic	nic	k3yNnSc1	nic
změnit	změnit	k5eAaPmF	změnit
<g/>
,	,	kIx,	,
ba	ba	k9	ba
ani	ani	k8xC	ani
umělecky	umělecky	k6eAd1	umělecky
něčeho	něco	k3yInSc2	něco
velkého	velký	k2eAgMnSc2d1	velký
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
–	–	k?	–
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
být	být	k5eAaImF	být
organizován	organizován	k2eAgMnSc1d1	organizován
<g/>
.	.	kIx.	.
</s>
<s>
Staví	stavit	k5eAaBmIp3nS	stavit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
staré	starý	k2eAgFnSc3d1	stará
<g/>
"	"	kIx"	"
literatuře	literatura	k1gFnSc3	literatura
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
pro	pro	k7c4	pro
bohatou	bohatý	k2eAgFnSc4d1	bohatá
vrstvu	vrstva	k1gFnSc4	vrstva
a	a	k8xC	a
jdou	jít	k5eAaImIp3nP	jít
s	s	k7c7	s
revoluční	revoluční	k2eAgFnSc7d1	revoluční
vrstvou	vrstva	k1gFnSc7	vrstva
–	–	k?	–
s	s	k7c7	s
dělnictvem	dělnictvo	k1gNnSc7	dělnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
Devětsilu	Devětsil	k1gInSc2	Devětsil
je	být	k5eAaImIp3nS	být
vcelku	vcelku	k6eAd1	vcelku
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
a	a	k8xC	a
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
přiblížit	přiblížit	k5eAaPmF	přiblížit
umění	umění	k1gNnSc3	umění
co	co	k8xS	co
nejširšímu	široký	k2eAgNnSc3d3	nejširší
publiku	publikum	k1gNnSc3	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
co	co	k9	co
nejpopulárnější	populární	k2eAgFnPc1d3	nejpopulárnější
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
nejpřístupnější	přístupný	k2eAgMnPc1d3	nejpřístupnější
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
nejrozličnější	rozličný	k2eAgFnPc1d3	nejrozličnější
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
těchto	tento	k3xDgInPc2	tento
snů	sen	k1gInPc2	sen
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zapotřebí	zapotřebí	k6eAd1	zapotřebí
finance	finance	k1gFnPc4	finance
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
platil	platit	k5eAaImAgMnS	platit
členský	členský	k2eAgInSc4d1	členský
příspěvek	příspěvek	k1gInSc4	příspěvek
20	[number]	k4	20
korun	koruna	k1gFnPc2	koruna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
příspěvků	příspěvek	k1gInPc2	příspěvek
byly	být	k5eAaImAgFnP	být
financovány	financován	k2eAgFnPc1d1	financována
pořádané	pořádaný	k2eAgFnPc1d1	pořádaná
akce	akce	k1gFnPc1	akce
(	(	kIx(	(
<g/>
přednášky	přednáška	k1gFnPc1	přednáška
o	o	k7c6	o
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
recitační	recitační	k2eAgInPc1d1	recitační
večery	večer	k1gInPc1	večer
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
peněz	peníze	k1gInPc2	peníze
vyplácely	vyplácet	k5eAaImAgFnP	vyplácet
tzv.	tzv.	kA	tzv.
umělecké	umělecký	k2eAgFnPc4d1	umělecká
prémie	prémie	k1gFnPc4	prémie
a	a	k8xC	a
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
zavést	zavést	k5eAaPmF	zavést
členské	členský	k2eAgFnPc4d1	členská
slevy	sleva	k1gFnPc4	sleva
na	na	k7c4	na
Devětsilem	Devětsil	k1gInSc7	Devětsil
pořádané	pořádaný	k2eAgFnPc1d1	pořádaná
akce	akce	k1gFnPc1	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
spolupodepsal	spolupodepsat	k5eAaPmAgMnS	spolupodepsat
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
spisovateli	spisovatel	k1gMnPc7	spisovatel
Manifest	manifest	k1gInSc4	manifest
sedmi	sedm	k4xCc2	sedm
<g/>
,	,	kIx,	,
sepsali	sepsat	k5eAaPmAgMnP	sepsat
Teige	Teige	k1gFnSc4	Teige
<g/>
,	,	kIx,	,
Nezval	Nezval	k1gMnSc1	Nezval
a	a	k8xC	a
Halas	Halas	k1gMnSc1	Halas
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgMnPc7d1	další
autory	autor	k1gMnPc7	autor
protimanifest	protimanifest	k5eAaPmF	protimanifest
a	a	k8xC	a
Seifert	Seifert	k1gMnSc1	Seifert
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Julia	Julius	k1gMnSc2	Julius
Fučíka	Fučík	k1gMnSc2	Fučík
z	z	k7c2	z
Devětsilu	Devětsil	k1gInSc2	Devětsil
vyloučen	vyloučen	k2eAgInSc1d1	vyloučen
<g/>
.	.	kIx.	.
<g/>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
odcházeli	odcházet	k5eAaImAgMnP	odcházet
z	z	k7c2	z
Devětsilu	Devětsil	k1gInSc2	Devětsil
mnozí	mnohý	k2eAgMnPc1d1	mnohý
členové	člen	k1gMnPc1	člen
<g/>
,	,	kIx,	,
architekti	architekt	k1gMnPc1	architekt
<g/>
,	,	kIx,	,
filmoví	filmový	k2eAgMnPc1d1	filmový
a	a	k8xC	a
divadelní	divadelní	k2eAgMnPc1d1	divadelní
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
hudebníci	hudebník	k1gMnPc1	hudebník
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
zakládající	zakládající	k2eAgMnPc1d1	zakládající
umělci	umělec	k1gMnPc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Teige	Teige	k6eAd1	Teige
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
věnoval	věnovat	k5eAaPmAgMnS	věnovat
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
především	především	k6eAd1	především
architektuře	architektura	k1gFnSc3	architektura
a	a	k8xC	a
teorii	teorie	k1gFnSc3	teorie
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odcházejí	odcházet	k5eAaImIp3nP	odcházet
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
umělecké	umělecký	k2eAgFnSc2d1	umělecká
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
názvem	název	k1gInSc7	název
Literární	literární	k2eAgFnSc1d1	literární
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Devětsil	Devětsil	k1gInSc1	Devětsil
a	a	k8xC	a
Literární	literární	k2eAgFnSc1d1	literární
skupina	skupina	k1gFnSc1	skupina
sice	sice	k8xC	sice
měly	mít	k5eAaImAgFnP	mít
podobné	podobný	k2eAgNnSc4d1	podobné
zaměření	zaměření	k1gNnSc4	zaměření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lišily	lišit	k5eAaImAgInP	lišit
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInPc1	jejich
názory	názor	k1gInPc1	názor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
odchod	odchod	k1gInSc4	odchod
Teigeho	Teige	k1gMnSc2	Teige
a	a	k8xC	a
Seiferta	Seifert	k1gMnSc2	Seifert
reagoval	reagovat	k5eAaBmAgMnS	reagovat
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
krátké	krátký	k2eAgFnSc6d1	krátká
básni	báseň	k1gFnSc6	báseň
Žák	Žák	k1gMnSc1	Žák
Devětsilu	Devětsil	k1gInSc2	Devětsil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
skupina	skupina	k1gFnSc1	skupina
vydávala	vydávat	k5eAaPmAgFnS	vydávat
časopisy	časopis	k1gInPc4	časopis
Revue	revue	k1gFnPc2	revue
Devětsilu	Devětsil	k1gInSc2	Devětsil
(	(	kIx(	(
<g/>
ReD	ReD	k1gFnSc1	ReD
<g/>
,	,	kIx,	,
měsíčník	měsíčník	k1gInSc1	měsíčník
pro	pro	k7c4	pro
moderní	moderní	k2eAgFnSc4d1	moderní
kulturu	kultura	k1gFnSc4	kultura
<g/>
;	;	kIx,	;
vycházel	vycházet	k5eAaImAgMnS	vycházet
v	v	k7c6	v
letech	let	k1gInPc6	let
1927	[number]	k4	1927
–	–	k?	–
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
a	a	k8xC	a
Disk	disk	k1gInSc1	disk
a	a	k8xC	a
brněnská	brněnský	k2eAgFnSc1d1	brněnská
skupina	skupina	k1gFnSc1	skupina
vydávala	vydávat	k5eAaPmAgFnS	vydávat
časopis	časopis	k1gInSc4	časopis
Pásmo	pásmo	k1gNnSc1	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
členové	člen	k1gMnPc1	člen
Devětsilu	Devětsil	k1gInSc2	Devětsil
přispívali	přispívat	k5eAaImAgMnP	přispívat
i	i	k9	i
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
vydávaného	vydávaný	k2eAgInSc2d1	vydávaný
Literární	literární	k2eAgFnSc7d1	literární
skupinou	skupina	k1gFnSc7	skupina
Host	host	k1gMnSc1	host
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svoje	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
brněnská	brněnský	k2eAgFnSc1d1	brněnská
skupina	skupina	k1gFnSc1	skupina
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
padla	padnout	k5eAaPmAgFnS	padnout
i	i	k9	i
poslední	poslední	k2eAgFnSc1d1	poslední
bašta	bašta	k1gFnSc1	bašta
Devětsilu	Devětsil	k1gInSc2	Devětsil
tj.	tj.	kA	tj.
pražská	pražský	k2eAgFnSc1d1	Pražská
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Citát	citát	k1gInSc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Představitelé	představitel	k1gMnPc1	představitel
==	==	k?	==
</s>
</p>
<p>
<s>
Zakladatelé	zakladatel	k1gMnPc1	zakladatel
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Artuš	Artuš	k1gMnSc1	Artuš
Černík	Černík	k1gMnSc1	Černík
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Frič	Frič	k1gMnSc1	Frič
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Havlíček	Havlíček	k1gMnSc1	Havlíček
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Hoffmeister	Hoffmeister	k1gMnSc1	Hoffmeister
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Prox	Prox	k1gInSc4	Prox
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Suk	Suk	k1gMnSc1	Suk
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Süss	Süssa	k1gFnPc2	Süssa
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Štulc	Štulc	k1gInSc4	Štulc
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gFnSc2	Teig
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Vaněk	Vaněk	k1gMnSc1	Vaněk
(	(	kIx(	(
<g/>
malíř	malíř	k1gMnSc1	malíř
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Veselík	Veselík	k1gMnSc1	Veselík
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
WachsmanVůdčí	WachsmanVůdčí	k2eAgFnSc2d1	WachsmanVůdčí
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gFnSc2	Teig
–	–	k?	–
literární	literární	k2eAgMnSc1d1	literární
teoretik	teoretik	k1gMnSc1	teoretik
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
</s>
</p>
<p>
<s>
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
–	–	k?	–
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
–	–	k?	–
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnPc1d1	zakládající
členČestní	členČestný	k2eAgMnPc1d1	členČestný
členové	člen	k1gMnPc1	člen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplina	k1gFnPc2	Chaplina
</s>
</p>
<p>
<s>
Douglas	Douglas	k1gInSc1	Douglas
FairbanksArchitekti	FairbanksArchitekť	k1gFnSc2	FairbanksArchitekť
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Fragner	Fragner	k1gMnSc1	Fragner
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Gillar	Gillar	k1gMnSc1	Gillar
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Havlíček	Havlíček	k1gMnSc1	Havlíček
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Honzík	Honzík	k1gMnSc1	Honzík
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Chochol	Chochol	k1gMnSc1	Chochol
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Koula	Koula	k1gMnSc1	Koula
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Krejcar	krejcar	k1gInSc4	krejcar
</s>
</p>
<p>
<s>
Evžen	Evžen	k1gMnSc1	Evžen
Linhart	Linhart	k1gMnSc1	Linhart
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Smetana	Smetana	k1gMnSc1	Smetana
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
KranzBásníci	KranzBásník	k1gMnPc1	KranzBásník
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
Biebl	Biebl	k1gMnSc1	Biebl
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Halas	Halas	k1gMnSc1	Halas
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Hořejší	Hořejší	k1gMnSc1	Hořejší
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc3	Jiří
WolkerHerci	WolkerHerce	k1gMnSc3	WolkerHerce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
–	–	k?	–
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Pohádka	pohádka	k1gFnSc1	pohádka
máje	máje	k1gFnSc1	máje
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ostatním	ostatní	k2eAgMnPc3d1	ostatní
členům	člen	k1gMnPc3	člen
zdála	zdát	k5eAaImAgNnP	zdát
nedůstojná	důstojný	k2eNgFnSc1d1	nedůstojná
revolučního	revoluční	k2eAgMnSc4d1	revoluční
umělce	umělec	k1gMnSc4	umělec
<g/>
,	,	kIx,	,
předlohu	předloha	k1gFnSc4	předloha
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
filmu	film	k1gInSc3	film
napsal	napsat	k5eAaPmAgMnS	napsat
Vilém	Vilém	k1gMnSc1	Vilém
Mrštík	Mrštík	k1gMnSc1	Mrštík
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
–	–	k?	–
později	pozdě	k6eAd2	pozdě
své	svůj	k3xOyFgNnSc4	svůj
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
popíral	popírat	k5eAaImAgMnS	popírat
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
v	v	k7c6	v
Táto	táta	k1gMnSc5	táta
povídej	povídat	k5eAaImRp2nS	povídat
–	–	k?	–
rozhovoru	rozhovor	k1gInSc6	rozhovor
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
Janou	Jana	k1gFnSc7	Jana
<g/>
)	)	kIx)	)
<g/>
Hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
JežekRežiséři	JežekRežiséř	k1gMnSc3	JežekRežiséř
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Frejka	Frejka	k1gFnSc1	Frejka
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
František	František	k1gMnSc1	František
Burian	Burian	k1gMnSc1	Burian
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
HonzlSpisovatelé	HonzlSpisovatel	k1gMnPc1	HonzlSpisovatel
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Konrád	Konrád	k1gMnSc1	Konrád
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
–	–	k?	–
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
</s>
</p>
<p>
<s>
Julius	Julius	k1gMnSc1	Julius
FučíkVýtvarníci	FučíkVýtvarník	k1gMnPc1	FučíkVýtvarník
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Heythum	Heythum	k1gInSc4	Heythum
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Hoffmeister	Hoffmeister	k1gMnSc1	Hoffmeister
–	–	k?	–
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Mrkvička	mrkvička	k1gFnSc1	mrkvička
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Muzika	muzika	k1gFnSc1	muzika
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Štyrský	Štyrský	k2eAgMnSc1d1	Štyrský
</s>
</p>
<p>
<s>
Toyen	Toyen	k1gInSc1	Toyen
–	–	k?	–
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
jménem	jméno	k1gNnSc7	jméno
Marie	Maria	k1gFnSc2	Maria
Čermínová	Čermínová	k1gFnSc1	Čermínová
</s>
</p>
<p>
<s>
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
VondráčkováFotografové	VondráčkováFotograf	k1gMnPc1	VondráčkováFotograf
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rössler	Rössler	k1gMnSc1	Rössler
</s>
</p>
<p>
<s>
Evžen	Evžen	k1gMnSc1	Evžen
Markalous	Markalous	k1gMnSc1	Markalous
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
ŠtyrskýTeoretici	ŠtyrskýTeoretik	k1gMnPc1	ŠtyrskýTeoretik
/	/	kIx~	/
literární	literární	k2eAgMnPc1d1	literární
vědci	vědec	k1gMnPc1	vědec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gFnSc2	Teig
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Frejka	Frejka	k1gFnSc1	Frejka
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Václavek	Václavek	k1gMnSc1	Václavek
</s>
</p>
<p>
<s>
Artuš	Artuš	k1gMnSc1	Artuš
Černík	Černík	k1gMnSc1	Černík
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Poetismus	poetismus	k1gInSc1	poetismus
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	let	k1gInPc6	let
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Literární	literární	k2eAgFnSc1d1	literární
skupina	skupina	k1gFnSc1	skupina
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A-	A-	k?	A-
<g/>
G.	G.	kA	G.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
900	[number]	k4	900
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
797	[number]	k4	797
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
:	:	kIx,	:
Meziválečná	meziválečný	k2eAgFnSc1d1	meziválečná
avantgarda	avantgarda	k1gFnSc1	avantgarda
a	a	k8xC	a
Osvobozené	osvobozený	k2eAgNnSc1d1	osvobozené
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
kulturně	kulturně	k6eAd1	kulturně
výchovnou	výchovný	k2eAgFnSc4d1	výchovná
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
247	[number]	k4	247
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ŠEBOROVÁ	Šeborová	k1gFnSc1	Šeborová
<g/>
,	,	kIx,	,
Silvie	Silvie	k1gFnSc1	Silvie
<g/>
.	.	kIx.	.
</s>
<s>
Devětsil	Devětsil	k1gInSc1	Devětsil
<g/>
:	:	kIx,	:
parta	parta	k1gFnSc1	parta
kamarádů	kamarád	k1gMnPc2	kamarád
ze	z	k7c2	z
školních	školní	k2eAgFnPc2d1	školní
lavic	lavice	k1gFnPc2	lavice
<g/>
.	.	kIx.	.
</s>
<s>
Živá	živý	k2eAgFnSc1d1	živá
historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
</s>
<s>
Duben	duben	k1gInSc1	duben
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
s.	s.	k?	s.
78	[number]	k4	78
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
<g/>
.	.	kIx.	.
</s>
</p>
