<s>
Ed	Ed	k?	Ed
Wood	Wood	k1gInSc1	Wood
je	být	k5eAaImIp3nS	být
americké	americký	k2eAgNnSc4d1	americké
komediální	komediální	k2eAgNnSc4d1	komediální
drama	drama	k1gNnSc4	drama
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
životopisný	životopisný	k2eAgInSc4d1	životopisný
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
režíroval	režírovat	k5eAaImAgMnS	režírovat
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
produkoval	produkovat	k5eAaImAgMnS	produkovat
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc4	Burton
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Johnny	Johnna	k1gMnSc2	Johnna
Depp	Depp	k1gMnSc1	Depp
jako	jako	k8xS	jako
kultovní	kultovní	k2eAgMnSc1d1	kultovní
režisér	režisér	k1gMnSc1	režisér
Edward	Edward	k1gMnSc1	Edward
D.	D.	kA	D.
Wood	Wood	k1gMnSc1	Wood
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nejhorší	zlý	k2eAgMnSc1d3	nejhorší
režisér	režisér	k1gMnSc1	režisér
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
