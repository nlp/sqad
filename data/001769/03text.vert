<s>
Ed	Ed	k?	Ed
Wood	Wood	k1gInSc1	Wood
je	být	k5eAaImIp3nS	být
americké	americký	k2eAgNnSc4d1	americké
komediální	komediální	k2eAgNnSc4d1	komediální
drama	drama	k1gNnSc4	drama
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
životopisný	životopisný	k2eAgInSc4d1	životopisný
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
režíroval	režírovat	k5eAaImAgMnS	režírovat
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
produkoval	produkovat	k5eAaImAgMnS	produkovat
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc4	Burton
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Johnny	Johnna	k1gMnSc2	Johnna
Depp	Depp	k1gMnSc1	Depp
jako	jako	k8xC	jako
kultovní	kultovní	k2eAgMnSc1d1	kultovní
režisér	režisér	k1gMnSc1	režisér
Edward	Edward	k1gMnSc1	Edward
D.	D.	kA	D.
Wood	Wood	k1gMnSc1	Wood
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nejhorší	zlý	k2eAgMnSc1d3	nejhorší
režisér	režisér	k1gMnSc1	režisér
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
je	být	k5eAaImIp3nS	být
prací	práce	k1gFnSc7	práce
Scotta	Scott	k1gMnSc2	Scott
Alexandera	Alexander	k1gMnSc2	Alexander
a	a	k8xC	a
Larryho	Larry	k1gMnSc2	Larry
Karaszewského	Karaszewský	k1gMnSc2	Karaszewský
<g/>
,	,	kIx,	,
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
studenty	student	k1gMnPc7	student
na	na	k7c6	na
USC	USC	kA	USC
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Cinematic	Cinematice	k1gFnPc2	Cinematice
Arts	Artsa	k1gFnPc2	Artsa
<g/>
.	.	kIx.	.
</s>
<s>
Provokovalo	provokovat	k5eAaImAgNnS	provokovat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
svou	svůj	k3xOyFgFnSc7	svůj
tvorbou	tvorba	k1gFnSc7	tvorba
na	na	k7c4	na
Problem	Probl	k1gInSc7	Probl
Child	Childo	k1gNnPc2	Childo
a	a	k8xC	a
následujících	následující	k2eAgInPc6d1	následující
filmech	film	k1gInPc6	film
považováni	považován	k2eAgMnPc1d1	považován
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
autory	autor	k1gMnPc4	autor
rodinných	rodinný	k2eAgInPc2d1	rodinný
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
a	a	k8xC	a
Karaszewski	Karaszewsk	k1gMnPc1	Karaszewsk
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
Burtonem	Burton	k1gInSc7	Burton
a	a	k8xC	a
Denisem	Denis	k1gInSc7	Denis
Di	Di	k1gFnSc2	Di
Novi	Nov	k1gFnSc2	Nov
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
životopisného	životopisný	k2eAgInSc2d1	životopisný
filmu	film	k1gInSc2	film
Ed	Ed	k1gMnSc1	Ed
Wood	Wood	k1gMnSc1	Wood
<g/>
,	,	kIx,	,
a	a	k8xC	a
Michaelem	Michael	k1gMnSc7	Michael
Lehmannem	Lehmann	k1gMnSc7	Lehmann
jako	jako	k8xS	jako
režisérem	režisér	k1gMnSc7	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
s	s	k7c7	s
Airheads	Airheads	k1gInSc1	Airheads
<g/>
,	,	kIx,	,
Lehmann	Lehmann	k1gInSc1	Lehmann
musel	muset	k5eAaImAgInS	muset
vyklidit	vyklidit	k5eAaPmF	vyklidit
pozici	pozice	k1gFnSc4	pozice
režiséra	režisér	k1gMnSc2	režisér
a	a	k8xC	a
tu	tu	k6eAd1	tu
převzal	převzít	k5eAaPmAgInS	převzít
Burton	Burton	k1gInSc1	Burton
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
Wood	Wood	k1gMnSc1	Wood
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
prací	prací	k2eAgNnPc4d1	prací
studia	studio	k1gNnPc4	studio
Columbia	Columbia	k1gFnSc1	Columbia
Pictures	Pictures	k1gInSc4	Pictures
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
studio	studio	k1gNnSc1	studio
dalo	dát	k5eAaPmAgNnS	dát
film	film	k1gInSc4	film
do	do	k7c2	do
odběhu	odběh	k1gInSc2	odběh
přes	přes	k7c4	přes
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Burtona	Burton	k1gMnSc2	Burton
natočit	natočit	k5eAaBmF	natočit
film	film	k1gInSc4	film
černobíle	černobíle	k6eAd1	černobíle
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
Wood	Wood	k1gInSc1	Wood
byl	být	k5eAaImAgInS	být
převzat	převzít	k5eAaPmNgInS	převzít
společností	společnost	k1gFnSc7	společnost
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gMnSc2	Disnea
Picture	Pictur	k1gMnSc5	Pictur
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
uznáván	uznávat	k5eAaImNgInS	uznávat
kritikou	kritika	k1gFnSc7	kritika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
kasovní	kasovní	k2eAgInSc4d1	kasovní
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Landau	Landau	k6eAd1	Landau
a	a	k8xC	a
Rick	Rick	k1gMnSc1	Rick
Baker	Baker	k1gMnSc1	Baker
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
Landauovy	Landauův	k2eAgFnPc4d1	Landauův
masky	maska	k1gFnPc4	maska
<g/>
,	,	kIx,	,
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
filmu	film	k1gInSc6	film
Academy	Academa	k1gFnSc2	Academa
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
D.	D.	kA	D.
Wood	Wood	k1gMnSc1	Wood
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
slyšel	slyšet	k5eAaImAgMnS	slyšet
oznámení	oznámení	k1gNnSc4	oznámení
časopisu	časopis	k1gInSc2	časopis
Variety	varieta	k1gFnSc2	varieta
<g/>
,	,	kIx,	,
že	že	k8xS	že
producent	producent	k1gMnSc1	producent
George	George	k1gFnPc2	George
Weiss	Weiss	k1gMnSc1	Weiss
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
koupit	koupit	k5eAaPmF	koupit
životní	životní	k2eAgInSc4d1	životní
příběh	příběh	k1gInSc4	příběh
Christine	Christin	k1gInSc5	Christin
Jorgensen	Jorgensen	k2eAgInSc1d1	Jorgensen
,	,	kIx,	,
inspiruje	inspirovat	k5eAaBmIp3nS	inspirovat
to	ten	k3xDgNnSc1	ten
Eda	Eda	k1gMnSc1	Eda
setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Weissem	Weiss	k1gMnSc7	Weiss
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Weiss	Weiss	k1gMnSc1	Weiss
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oznámení	oznámení	k1gNnSc1	oznámení
magazínu	magazín	k1gInSc2	magazín
Variety	varieta	k1gFnSc2	varieta
byl	být	k5eAaImAgInS	být
únik	únik	k1gInSc1	únik
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
koupit	koupit	k5eAaPmF	koupit
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
zfilmování	zfilmování	k1gNnSc4	zfilmování
životního	životní	k2eAgInSc2d1	životní
příběhu	příběh	k1gInSc2	příběh
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Jorgensenové	Jorgensenová	k1gFnSc2	Jorgensenová
<g/>
.	.	kIx.	.
</s>
<s>
Producent	producent	k1gMnSc1	producent
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
pro	pro	k7c4	pro
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
název	název	k1gInSc4	název
'	'	kIx"	'
<g/>
'	'	kIx"	'
<g/>
I	i	k9	i
Changed	Changed	k1gInSc1	Changed
My	my	k3xPp1nPc1	my
Sex	sex	k1gInSc1	sex
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
Ed	Ed	k1gMnSc1	Ed
setká	setkat	k5eAaPmIp3nS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
dávným	dávný	k2eAgInSc7d1	dávný
idolem	idol	k1gInSc7	idol
Bélou	Béla	k1gMnSc7	Béla
Lugosim	Lugosima	k1gFnPc2	Lugosima
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
veze	vézt	k5eAaImIp3nS	vézt
Bélu	Béla	k1gMnSc4	Béla
domů	domů	k6eAd1	domů
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Ed	Ed	k1gMnSc1	Ed
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
obsadit	obsadit	k5eAaPmF	obsadit
Bélu	Béla	k1gMnSc4	Béla
do	do	k7c2	do
filmu	film	k1gInSc2	film
a	a	k8xC	a
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
Weisse	Weiss	k1gMnSc4	Weiss
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgFnSc1d1	ideální
pro	pro	k7c4	pro
režii	režie	k1gFnSc4	režie
I	i	k8xC	i
Changed	Changed	k1gInSc4	Changed
my	my	k3xPp1nPc1	my
Sex	sex	k1gInSc1	sex
<g/>
!	!	kIx.	!
</s>
<s>
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
transvestita	transvestita	k1gMnSc1	transvestita
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
a	a	k8xC	a
Weiss	Weiss	k1gMnSc1	Weiss
se	se	k3xPyFc4	se
dohadují	dohadovat	k5eAaImIp3nP	dohadovat
ohledně	ohledně	k7c2	ohledně
názvu	název	k1gInSc2	název
filmu	film	k1gInSc2	film
<g/>
:	:	kIx,	:
Weiss	Weiss	k1gMnSc1	Weiss
měl	mít	k5eAaImAgMnS	mít
již	již	k6eAd1	již
vytištěný	vytištěný	k2eAgInSc4d1	vytištěný
plakát	plakát	k1gInSc4	plakát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Ed	Ed	k1gMnSc1	Ed
změnil	změnit	k5eAaPmAgMnS	změnit
na	na	k7c4	na
Glen	Glen	k1gNnSc4	Glen
or	or	k?	or
Glenda	Glenda	k1gFnSc1	Glenda
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
končí	končit	k5eAaImIp3nS	končit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Glen	Glen	k1gMnSc1	Glen
or	or	k?	or
Glenda	Glenda	k1gFnSc1	Glenda
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ed	Ed	k1gMnSc1	Ed
je	být	k5eAaImIp3nS	být
nadšený	nadšený	k2eAgMnSc1d1	nadšený
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrál	hrát	k5eAaImAgInS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
režíroval	režírovat	k5eAaImAgMnS	režírovat
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
a	a	k8xC	a
produkoval	produkovat	k5eAaImAgMnS	produkovat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
hrdina	hrdina	k1gMnSc1	hrdina
Orson	Orson	k1gMnSc1	Orson
Welles	Welles	k1gMnSc1	Welles
,	,	kIx,	,
když	když	k8xS	když
dělal	dělat	k5eAaImAgMnS	dělat
Citizen	Citizen	kA	Citizen
Kane	kanout	k5eAaImIp3nS	kanout
<g/>
/	/	kIx~	/
<g/>
Občan	občan	k1gMnSc1	občan
Kane	kanout	k5eAaImIp3nS	kanout
<g/>
.	.	kIx.	.
</s>
<s>
Glen	Glen	k1gMnSc1	Glen
or	or	k?	or
Glenda	Glenda	k1gFnSc1	Glenda
znamená	znamenat	k5eAaImIp3nS	znamenat
neúspěch	neúspěch	k1gInSc4	neúspěch
finanční	finanční	k2eAgInSc4d1	finanční
i	i	k9	i
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
nemá	mít	k5eNaImIp3nS	mít
úspěch	úspěch	k1gInSc4	úspěch
při	při	k7c6	při
snaze	snaha	k1gFnSc6	snaha
získat	získat	k5eAaPmF	získat
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
u	u	k7c2	u
Warner	Warnra	k1gFnPc2	Warnra
Bros	Brosa	k1gFnPc2	Brosa
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc4	jejich
producent	producent	k1gMnSc1	producent
mu	on	k3xPp3gMnSc3	on
tam	tam	k6eAd1	tam
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Glen	Glen	k1gMnSc1	Glen
or	or	k?	or
Glenda	Glenda	k1gFnSc1	Glenda
je	být	k5eAaImIp3nS	být
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kdy	kdy	k6eAd1	kdy
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Edova	Edův	k2eAgFnSc1d1	Edova
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
<g/>
,	,	kIx,	,
Dolores	Dolores	k1gMnSc1	Dolores
Fuller	Fuller	k1gMnSc1	Fuller
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
není	být	k5eNaImIp3nS	být
"	"	kIx"	"
<g/>
materiál	materiál	k1gInSc1	materiál
studia	studio	k1gNnSc2	studio
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
příště	příště	k6eAd1	příště
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
nezávislými	závislý	k2eNgMnPc7d1	nezávislý
producenty	producent	k1gMnPc7	producent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
baru	bar	k1gInSc6	bar
se	se	k3xPyFc4	se
Ed	Ed	k1gMnSc1	Ed
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
Lorettou	Loretta	k1gFnSc7	Loretta
Kingovou	Kingův	k2eAgFnSc7d1	Kingova
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
dost	dost	k6eAd1	dost
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
financování	financování	k1gNnSc4	financování
The	The	k1gFnSc2	The
Bride	Brid	k1gInSc5	Brid
of	of	k?	of
the	the	k?	the
Atom	atom	k1gInSc1	atom
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
přesvědčí	přesvědčit	k5eAaPmIp3nP	přesvědčit
magnáta	magnát	k1gMnSc4	magnát
masného	masný	k2eAgInSc2d1	masný
průmyslu	průmysl	k1gInSc2	průmysl
,	,	kIx,	,
Dona	Don	k1gMnSc4	Don
McCoye	McCoy	k1gMnSc4	McCoy
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
převzal	převzít	k5eAaPmAgInS	převzít
financování	financování	k1gNnSc4	financování
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
McCoy	McCo	k1gMnPc4	McCo
to	ten	k3xDgNnSc1	ten
udělá	udělat	k5eAaPmIp3nS	udělat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
skončí	skončit	k5eAaPmIp3nS	skončit
obřím	obří	k2eAgInSc7d1	obří
výbuchem	výbuch	k1gInSc7	výbuch
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Tony	Tony	k1gMnSc1	Tony
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
trochu	trochu	k6eAd1	trochu
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
Bride	Brid	k1gInSc5	Brid
o	o	k7c6	o
the	the	k?	the
Atom	atom	k1gInSc1	atom
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Dolores	Dolores	k1gMnSc1	Dolores
a	a	k8xC	a
Ed	Ed	k1gMnSc1	Ed
se	se	k3xPyFc4	se
po	po	k7c6	po
večírku	večírek	k1gInSc6	večírek
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
kvůli	kvůli	k7c3	kvůli
Edovým	Edův	k2eAgMnPc3d1	Edův
přátelům	přítel	k1gMnPc3	přítel
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
transvestismu	transvestismus	k1gInSc2	transvestismus
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Béla	Béla	k1gMnSc1	Béla
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
deprimovaný	deprimovaný	k2eAgMnSc1d1	deprimovaný
a	a	k8xC	a
užívá	užívat	k5eAaImIp3nS	užívat
morfium	morfium	k1gNnSc1	morfium
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
s	s	k7c7	s
Edem	Edem	k?	Edem
spáchat	spáchat	k5eAaPmF	spáchat
dvojí	dvojí	k4xRgFnSc4	dvojí
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
rozmluvena	rozmluven	k2eAgFnSc1d1	rozmluvena
<g/>
.	.	kIx.	.
</s>
<s>
Béla	Béla	k1gMnSc1	Béla
odchází	odcházet	k5eAaImIp3nS	odcházet
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ed	Ed	k1gMnSc1	Ed
nakonec	nakonec	k6eAd1	nakonec
najde	najít	k5eAaPmIp3nS	najít
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
Kathy	Kath	k1gMnPc7	Kath
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Hara	Harum	k1gNnPc4	Harum
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
ji	on	k3xPp3gFnSc4	on
vezme	vzít	k5eAaPmIp3nS	vzít
na	na	k7c4	na
rande	rande	k1gNnSc4	rande
a	a	k8xC	a
odhalí	odhalit	k5eAaPmIp3nS	odhalit
jí	on	k3xPp3gFnSc3	on
svůj	svůj	k3xOyFgInSc4	svůj
transvestismus	transvestismus	k1gInSc1	transvestismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
akceptuje	akceptovat	k5eAaBmIp3nS	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
začíná	začínat	k5eAaImIp3nS	začínat
natáčet	natáčet	k5eAaImF	natáčet
film	film	k1gInSc4	film
s	s	k7c7	s
Bélou	Béla	k1gMnSc7	Béla
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nP	zúčastnit
premiéry	premiéra	k1gFnPc1	premiéra
Bride	Brid	k1gInSc5	Brid
and	and	k?	and
the	the	k?	the
monster	monstrum	k1gNnPc2	monstrum
<g/>
/	/	kIx~	/
<g/>
Nevěsta	nevěsta	k1gFnSc1	nevěsta
a	a	k8xC	a
netvor	netvor	k1gMnSc1	netvor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozzlobený	rozzlobený	k2eAgInSc1d1	rozzlobený
dav	dav	k1gInSc1	dav
jej	on	k3xPp3gMnSc4	on
vyžene	vyhnat	k5eAaPmIp3nS	vyhnat
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
Béla	Béla	k1gMnSc1	Béla
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
církevního	církevní	k2eAgMnSc4d1	církevní
hodnostáře	hodnostář	k1gMnSc4	hodnostář
jménem	jméno	k1gNnSc7	jméno
Reynolds	Reynoldsa	k1gFnPc2	Reynoldsa
<g/>
,	,	kIx,	,
že	že	k8xS	že
financování	financování	k1gNnSc1	financování
jeho	on	k3xPp3gInSc2	on
scénáře	scénář	k1gInSc2	scénář
filmu	film	k1gInSc2	film
"	"	kIx"	"
Grave	grave	k1gNnSc1	grave
Robbers	Robbersa	k1gFnPc2	Robbersa
from	froma	k1gFnPc2	froma
Outer	Outer	k1gInSc1	Outer
Space	Spaec	k1gInSc2	Spaec
<g/>
"	"	kIx"	"
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
finanční	finanční	k2eAgInSc4d1	finanční
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
vydělalo	vydělat	k5eAaPmAgNnS	vydělat
by	by	kYmCp3nS	by
dost	dost	k6eAd1	dost
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
vysněný	vysněný	k2eAgInSc4d1	vysněný
Reynoldsův	Reynoldsův	k2eAgInSc4d1	Reynoldsův
projekt	projekt	k1gInSc4	projekt
(	(	kIx(	(
<g/>
filmy	film	k1gInPc1	film
o	o	k7c6	o
dvanácti	dvanáct	k4xCc6	dvanáct
apoštolech	apoštol	k1gMnPc6	apoštol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Tom	Tom	k1gMnSc1	Tom
Mason	mason	k1gMnSc1	mason
<g/>
,	,	kIx,	,
chiropraktik	chiropraktik	k1gMnSc1	chiropraktik
Kathy	Katha	k1gFnSc2	Katha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
jako	jako	k8xC	jako
Bélův	Bélův	k2eAgInSc1d1	Bélův
záskok	záskok	k1gInSc1	záskok
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mezi	mezi	k7c7	mezi
Edem	Edem	k?	Edem
a	a	k8xC	a
baptisty	baptista	k1gMnSc2	baptista
začíná	začínat	k5eAaImIp3nS	začínat
konflikt	konflikt	k1gInSc4	konflikt
o	o	k7c4	o
obsah	obsah	k1gInSc4	obsah
scénáře	scénář	k1gInSc2	scénář
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
chtějí	chtít	k5eAaImIp3nP	chtít
změnit	změnit	k5eAaPmF	změnit
na	na	k7c4	na
Plan	plan	k1gInSc4	plan
9	[number]	k4	9
from	from	k1gMnSc1	from
Outer	Outer	k1gMnSc1	Outer
Space	Space	k1gMnSc1	Space
<g/>
.	.	kIx.	.
</s>
<s>
Zoufalý	zoufalý	k2eAgMnSc1d1	zoufalý
Ed	Ed	k1gMnSc1	Ed
odjede	odjet	k5eAaPmIp3nS	odjet
do	do	k7c2	do
nejbližšího	blízký	k2eAgInSc2d3	Nejbližší
baru	bar	k1gInSc2	bar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
idolem	idol	k1gInSc7	idol
Orsonem	Orson	k1gMnSc7	Orson
Wellesem	Welles	k1gMnSc7	Welles
<g/>
.	.	kIx.	.
</s>
<s>
Welles	Welles	k1gInSc1	Welles
říká	říkat	k5eAaImIp3nS	říkat
Edovi	Eda	k1gMnSc3	Eda
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
za	za	k7c4	za
vize	vize	k1gFnPc4	vize
stojí	stát	k5eAaImIp3nS	stát
bojovat	bojovat	k5eAaImF	bojovat
<g/>
"	"	kIx"	"
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Edem	Edem	k?	Edem
natočí	natočit	k5eAaBmIp3nP	natočit
film	film	k1gInSc4	film
Plan	plan	k1gInSc1	plan
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
končí	končit	k5eAaImIp3nS	končit
premiérou	premiéra	k1gFnSc7	premiéra
Plan	plan	k1gInSc1	plan
9	[number]	k4	9
<g/>
,	,	kIx,	,
Ed	Ed	k1gFnPc1	Ed
a	a	k8xC	a
Kathy	Katha	k1gFnPc1	Katha
odjíždějí	odjíždět	k5eAaImIp3nP	odjíždět
do	do	k7c2	do
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
v	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Johnny	Johnna	k1gMnSc2	Johnna
Depp	Depp	k1gMnSc1	Depp
jako	jako	k8xC	jako
Edward	Edward	k1gMnSc1	Edward
D.	D.	kA	D.
Wood	Wood	k1gMnSc1	Wood
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Burton	Burton	k1gInSc1	Burton
oslovil	oslovit	k5eAaPmAgInS	oslovit
Deppa	Depp	k1gMnSc4	Depp
-	-	kIx~	-
"	"	kIx"	"
a	a	k8xC	a
během	během	k7c2	během
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
projektu	projekt	k1gInSc6	projekt
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
"	"	kIx"	"
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
Depp	Depp	k1gInSc1	Depp
deprimovaný	deprimovaný	k2eAgInSc1d1	deprimovaný
z	z	k7c2	z
filmů	film	k1gInPc2	film
celého	celý	k2eAgNnSc2d1	celé
filmování	filmování	k1gNnSc2	filmování
<g/>
..	..	k?	..
Přijetí	přijetí	k1gNnSc1	přijetí
této	tento	k3xDgFnSc2	tento
role	role	k1gFnSc2	role
<g/>
,	,	kIx,	,
mu	on	k3xPp3gInSc3	on
dalo	dát	k5eAaPmAgNnS	dát
"	"	kIx"	"
<g/>
příležitost	příležitost	k1gFnSc1	příležitost
vypnout	vypnout	k5eAaPmF	vypnout
a	a	k8xC	a
bavit	bavit	k5eAaImF	bavit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
a	a	k8xC	a
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
Martinem	Martin	k1gMnSc7	Martin
Landauerem	landauer	k1gInSc7	landauer
"	"	kIx"	"
<g/>
omladila	omladit	k5eAaPmAgFnS	omladit
moji	můj	k3xOp1gFnSc4	můj
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
herectví	herectví	k1gNnSc3	herectví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Depp	Depp	k1gMnSc1	Depp
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
obeznámen	obeznámit	k5eAaPmNgMnS	obeznámit
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
Woodovými	Woodův	k2eAgInPc7d1	Woodův
filmy	film	k1gInPc7	film
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Johna	John	k1gMnSc2	John
Waterse	Waterse	k1gFnSc2	Waterse
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
ukázal	ukázat	k5eAaPmAgMnS	ukázat
Plan	plan	k1gInSc4	plan
9	[number]	k4	9
from	from	k1gMnSc1	from
Outer	Outer	k1gMnSc1	Outer
Space	Space	k1gMnSc1	Space
a	a	k8xC	a
Glen	Glen	k1gMnSc1	Glen
or	or	k?	or
Glenda	Glenda	k1gFnSc1	Glenda
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Landau	Landaus	k1gInSc2	Landaus
jako	jako	k8xS	jako
Béla	Béla	k1gMnSc1	Béla
Lugosi	Lugose	k1gFnSc4	Lugose
<g/>
:	:	kIx,	:
Rick	Rick	k1gMnSc1	Rick
Baker	Baker	k1gMnSc1	Baker
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
vzory	vzor	k1gInPc4	vzor
masek	maska	k1gFnPc2	maska
<g/>
.	.	kIx.	.
</s>
<s>
Baker	Baker	k1gMnSc1	Baker
nepoužíval	používat	k5eNaImAgMnS	používat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
masky	maska	k1gFnPc4	maska
<g/>
,	,	kIx,	,
jen	jen	k9	jen
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připomínal	připomínat	k5eAaImAgInS	připomínat
Lugosiho	Lugosi	k1gMnSc4	Lugosi
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
umožnil	umožnit	k5eAaPmAgInS	umožnit
Landauovi	Landau	k1gMnSc3	Landau
využít	využít	k5eAaPmF	využít
svou	svůj	k3xOyFgFnSc4	svůj
tvář	tvář	k1gFnSc4	tvář
ke	k	k7c3	k
hraní	hraní	k1gNnSc3	hraní
a	a	k8xC	a
vyjádření	vyjádření	k1gNnSc3	vyjádření
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Jessica	Jessica	k1gMnSc1	Jessica
Parker	Parker	k1gMnSc1	Parker
jakoDolores	jakoDolores	k1gMnSc1	jakoDolores
Fuller	Fuller	k1gMnSc1	Fuller
<g/>
:	:	kIx,	:
Edova	Edův	k2eAgFnSc1d1	Edova
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
vztahem	vztah	k1gInSc7	vztah
s	s	k7c7	s
Kathy	Kath	k1gMnPc7	Kath
<g/>
.	.	kIx.	.
</s>
<s>
Dolores	Dolores	k1gInSc1	Dolores
je	být	k5eAaImIp3nS	být
vyvedená	vyvedený	k2eAgFnSc1d1	vyvedená
z	z	k7c2	z
míry	míra	k1gFnSc2	míra
Edovým	Edův	k2eAgInSc7d1	Edův
transvestismem	transvestismus	k1gInSc7	transvestismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
rozchodu	rozchod	k1gInSc3	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
Dolores	Dolores	k1gMnSc1	Dolores
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stane	stanout	k5eAaPmIp3nS	stanout
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
textařkou	textařka	k1gFnSc7	textařka
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
Elvise	Elvis	k1gMnSc4	Elvis
Presleyho	Presley	k1gMnSc4	Presley
<g/>
.	.	kIx.	.
</s>
<s>
Patricia	Patricius	k1gMnSc4	Patricius
Arquette	Arquett	k1gInSc5	Arquett
jako	jako	k8xC	jako
Kathy	Kath	k1gMnPc7	Kath
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Hara	Har	k2eAgFnSc1d1	Hara
<g/>
:	:	kIx,	:
Edova	Edův	k2eAgFnSc1d1	Edova
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Dolores	Doloresa	k1gFnPc2	Doloresa
<g/>
.	.	kIx.	.
</s>
<s>
Kathy	Katha	k1gFnPc4	Katha
nemá	mít	k5eNaImIp3nS	mít
problém	problém	k1gInSc1	problém
s	s	k7c7	s
Edovým	Edův	k2eAgInSc7d1	Edův
transvestismem	transvestismus	k1gInSc7	transvestismus
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
Eda	Eda	k1gMnSc1	Eda
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
trvá	trvat	k5eAaImIp3nS	trvat
až	až	k9	až
do	do	k7c2	do
Edovy	Edův	k2eAgFnSc2d1	Edova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
nevdá	vdát	k5eNaPmIp3nS	vdát
<g/>
.	.	kIx.	.
</s>
<s>
Lisa	Lisa	k1gFnSc1	Lisa
Marie	Maria	k1gFnSc2	Maria
jako	jako	k8xS	jako
Vampira	Vampir	k1gMnSc2	Vampir
Jeffrey	Jeffrea	k1gMnSc2	Jeffrea
Jones	Jones	k1gMnSc1	Jones
jako	jako	k8xC	jako
The	The	k1gFnSc1	The
Amazing	Amazing	k1gInSc1	Amazing
Criswell	Criswell	k1gMnSc1	Criswell
Max	Max	k1gMnSc1	Max
Casella	Casella	k1gMnSc1	Casella
jako	jako	k8xS	jako
Brent	Brent	k?	Brent
Hinkley	Hinklea	k1gFnPc4	Hinklea
ztvárňují	ztvárňovat	k5eAaImIp3nP	ztvárňovat
Paul	Paul	k1gMnSc1	Paul
Marco	Marco	k1gMnSc1	Marco
a	a	k8xC	a
Conrad	Conrad	k1gInSc1	Conrad
Brooks	Brooksa	k1gFnPc2	Brooksa
Bill	Bill	k1gMnSc1	Bill
Murray	Murraa	k1gFnSc2	Murraa
jako	jako	k8xS	jako
Bunny	Bunna	k1gMnSc2	Bunna
Breckinridge	Breckinridg	k1gMnSc2	Breckinridg
George	Georg	k1gMnSc2	Georg
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Animal	animal	k1gMnSc1	animal
<g/>
"	"	kIx"	"
Steele	Steel	k1gInPc1	Steel
jako	jako	k8xS	jako
Tor	Tor	k1gMnSc1	Tor
Johnson	Johnson	k1gMnSc1	Johnson
Juliet	Juliet	k1gMnSc1	Juliet
Landau	Landaa	k1gMnSc4	Landaa
jako	jako	k9	jako
Loretta	Lorett	k1gMnSc4	Lorett
King	King	k1gMnSc1	King
Ned	Ned	k1gFnSc2	Ned
Bellamy	Bellam	k1gInPc4	Bellam
jako	jako	k8xS	jako
Tom	Tom	k1gMnSc1	Tom
Mason	mason	k1gMnSc1	mason
Mike	Mike	k1gFnPc2	Mike
Starr	Starr	k1gMnSc1	Starr
jako	jako	k8xS	jako
George	George	k1gFnSc1	George
Weiss	Weiss	k1gMnSc1	Weiss
Film	film	k1gInSc1	film
Ed	Ed	k1gMnSc1	Ed
Wood	Wood	k1gMnSc1	Wood
měl	mít	k5eAaImAgMnS	mít
svou	svůj	k3xOyFgFnSc4	svůj
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c4	na
32	[number]	k4	32
<g/>
.	.	kIx.	.
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
Lincoln	Lincoln	k1gMnSc1	Lincoln
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
krátce	krátce	k6eAd1	krátce
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Telluride	tellurid	k1gInSc5	tellurid
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
soutěžil	soutěžit	k5eAaImAgMnS	soutěžit
o	o	k7c4	o
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
palmu	palma	k1gFnSc4	palma
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
Wood	Wood	k1gMnSc1	Wood
byl	být	k5eAaImAgMnS	být
nejdříve	dříve	k6eAd3	dříve
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
malého	malý	k2eAgInSc2d1	malý
počtu	počet	k1gInSc2	počet
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
623	[number]	k4	623
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
Wood	Wood	k1gInSc1	Wood
vydělal	vydělat	k5eAaPmAgInS	vydělat
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
víkendu	víkend	k1gInSc2	víkend
1.903.768	[number]	k4	1.903.768
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vydělal	vydělat	k5eAaPmAgInS	vydělat
brutto	brutto	k2eAgInSc4d1	brutto
5887457	[number]	k4	5887457
dolarů	dolar	k1gInPc2	dolar
na	na	k7c6	na
domácím	domácí	k2eAgInSc6d1	domácí
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
než	než	k8xS	než
činil	činit	k5eAaImAgInS	činit
výrobní	výrobní	k2eAgInSc1d1	výrobní
rozpočet	rozpočet	k1gInSc1	rozpočet
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
18	[number]	k4	18
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
EUR	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
dvěma	dva	k4xCgInPc7	dva
Oscary	Oscar	k1gInPc7	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
vedlejší	vedlejší	k2eAgMnSc1d1	vedlejší
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
Martin	Martin	k1gMnSc1	Martin
Landau	Landaa	k1gFnSc4	Landaa
<g/>
)	)	kIx)	)
a	a	k8xC	a
masky	maska	k1gFnSc2	maska
(	(	kIx(	(
<g/>
Rick	Rick	k1gMnSc1	Rick
Baker	Baker	k1gMnSc1	Baker
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
dalších	další	k2eAgFnPc2d1	další
19	[number]	k4	19
cen	cena	k1gFnPc2	cena
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Zlatých	zlatý	k2eAgInPc2d1	zlatý
glóbů	glóbus	k1gInPc2	glóbus
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
11	[number]	k4	11
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ed	Ed	k1gMnSc1	Ed
Wood	Wood	k1gMnSc1	Wood
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
