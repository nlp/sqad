<p>
<s>
80	[number]	k4	80
<g/>
.	.	kIx.	.
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelskou	pořadatelský	k2eAgFnSc7d1	pořadatelská
zemí	zem	k1gFnSc7	zem
bylo	být	k5eAaImAgNnS	být
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
kongres	kongres	k1gInSc1	kongres
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
federace	federace	k1gFnSc2	federace
konaný	konaný	k2eAgInSc4d1	konaný
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Protikandidáti	protikandidát	k1gMnPc1	protikandidát
Dánsko	Dánsko	k1gNnSc4	Dánsko
a	a	k8xC	a
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
se	se	k3xPyFc4	se
kandidatury	kandidatura	k1gFnSc2	kandidatura
vzdali	vzdát	k5eAaPmAgMnP	vzdát
a	a	k8xC	a
odsunuli	odsunout	k5eAaPmAgMnP	odsunout
své	svůj	k3xOyFgInPc4	svůj
plány	plán	k1gInPc4	plán
na	na	k7c4	na
uspořádání	uspořádání	k1gNnSc4	uspořádání
turnaje	turnaj	k1gInSc2	turnaj
na	na	k7c4	na
pozdější	pozdní	k2eAgInPc4d2	pozdější
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
MS	MS	kA	MS
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
pošesté	pošesté	k4xO	pošesté
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
podruhé	podruhé	k6eAd1	podruhé
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šampionát	šampionát	k1gInSc1	šampionát
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
oslav	oslava	k1gFnPc2	oslava
70	[number]	k4	70
let	léto	k1gNnPc2	léto
hokeje	hokej	k1gInSc2	hokej
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
<g/>
Turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
celkem	celkem	k6eAd1	celkem
16	[number]	k4	16
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
14	[number]	k4	14
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
z	z	k7c2	z
minulého	minulý	k2eAgNnSc2d1	Minulé
mistrovství	mistrovství	k1gNnSc2	mistrovství
a	a	k8xC	a
2	[number]	k4	2
postupující	postupující	k2eAgFnSc7d1	postupující
z	z	k7c2	z
minulého	minulý	k2eAgInSc2d1	minulý
ročníku	ročník	k1gInSc2	ročník
skupiny	skupina	k1gFnSc2	skupina
A	a	k8xC	a
divize	divize	k1gFnSc1	divize
1	[number]	k4	1
-	-	kIx~	-
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
dvě	dva	k4xCgNnPc1	dva
mužstva	mužstvo	k1gNnPc1	mužstvo
nahradila	nahradit	k5eAaPmAgFnS	nahradit
týmy	tým	k1gInPc4	tým
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
sestoupily	sestoupit	k5eAaPmAgInP	sestoupit
na	na	k7c6	na
šampionátu	šampionát	k1gInSc6	šampionát
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
zápasem	zápas	k1gInSc7	zápas
českého	český	k2eAgNnSc2d1	české
družstva	družstvo	k1gNnSc2	družstvo
bylo	být	k5eAaImAgNnS	být
utkání	utkání	k1gNnSc4	utkání
s	s	k7c7	s
domácím	domácí	k2eAgNnSc7d1	domácí
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c4	v
den	den	k1gInSc4	den
zahájení	zahájení	k1gNnSc4	zahájení
mistrovství	mistrovství	k1gNnSc2	mistrovství
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
<g/>
Turnaj	turnaj	k1gInSc4	turnaj
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výběr	výběr	k1gInSc1	výběr
pořadatelské	pořadatelský	k2eAgFnSc2d1	pořadatelská
země	zem	k1gFnSc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
pořádání	pořádání	k1gNnSc6	pořádání
80	[number]	k4	80
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hlasování	hlasování	k1gNnSc2	hlasování
kongresu	kongres	k1gInSc2	kongres
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
IIHF	IIHF	kA	IIHF
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
kandidátem	kandidát	k1gMnSc7	kandidát
bylo	být	k5eAaImAgNnS	být
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Dánsko	Dánsko	k1gNnSc1	Dánsko
a	a	k8xC	a
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
své	svůj	k3xOyFgFnPc4	svůj
žádosti	žádost	k1gFnPc4	žádost
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
podání	podání	k1gNnSc3	podání
žádostí	žádost	k1gFnPc2	žádost
v	v	k7c6	v
budoucích	budoucí	k2eAgInPc6d1	budoucí
letech	let	k1gInPc6	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stadiony	stadion	k1gInPc4	stadion
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Soupisky	soupiska	k1gFnSc2	soupiska
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
národní	národní	k2eAgInSc1d1	národní
tým	tým	k1gInSc1	tým
musel	muset	k5eAaImAgInS	muset
mít	mít	k5eAaImF	mít
družstvo	družstvo	k1gNnSc4	družstvo
složené	složený	k2eAgNnSc4d1	složené
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
17	[number]	k4	17
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
15	[number]	k4	15
bruslařů	bruslař	k1gMnPc2	bruslař
a	a	k8xC	a
2	[number]	k4	2
brankářů	brankář	k1gMnPc2	brankář
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
z	z	k7c2	z
25	[number]	k4	25
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
22	[number]	k4	22
bruslařů	bruslař	k1gMnPc2	bruslař
a	a	k8xC	a
3	[number]	k4	3
brankářů	brankář	k1gMnPc2	brankář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
zúčastněná	zúčastněný	k2eAgNnPc1d1	zúčastněné
mužstva	mužstvo	k1gNnPc1	mužstvo
musela	muset	k5eAaImAgNnP	muset
skrze	skrze	k?	skrze
potvrzení	potvrzení	k1gNnSc4	potvrzení
svých	svůj	k3xOyFgFnPc2	svůj
příslušných	příslušný	k2eAgFnPc2d1	příslušná
národních	národní	k2eAgFnPc2d1	národní
asociací	asociace	k1gFnPc2	asociace
předložit	předložit	k5eAaPmF	předložit
potvrzenou	potvrzená	k1gFnSc4	potvrzená
soupisku	soupiska	k1gFnSc4	soupiska
nejdříve	dříve	k6eAd3	dříve
ředitelství	ředitelství	k1gNnSc2	ředitelství
IIHF	IIHF	kA	IIHF
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
následně	následně	k6eAd1	následně
schválila	schválit	k5eAaPmAgFnS	schválit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Maskot	maskot	k1gInSc4	maskot
==	==	k?	==
</s>
</p>
<p>
<s>
Maskotem	maskot	k1gInSc7	maskot
šampionátu	šampionát	k1gInSc2	šampionát
byl	být	k5eAaImAgMnS	být
vesmírný	vesmírný	k2eAgMnSc1d1	vesmírný
pes	pes	k1gMnSc1	pes
Lajka	lajka	k1gFnSc1	lajka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
oděný	oděný	k2eAgMnSc1d1	oděný
do	do	k7c2	do
barev	barva	k1gFnPc2	barva
ruské	ruský	k2eAgFnSc2d1	ruská
vlajky	vlajka	k1gFnSc2	vlajka
s	s	k7c7	s
hokejkou	hokejka	k1gFnSc7	hokejka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
a	a	k8xC	a
pukem	puk	k1gInSc7	puk
mířícím	mířící	k2eAgInSc7d1	mířící
na	na	k7c6	na
bránu	brána	k1gFnSc4	brána
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
plemeno	plemeno	k1gNnSc4	plemeno
Sibiřský	sibiřský	k2eAgInSc4d1	sibiřský
husky	husk	k1gInPc4	husk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
patřilo	patřit	k5eAaImAgNnS	patřit
mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgNnSc4d3	nejrozšířenější
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
využívané	využívaný	k2eAgNnSc1d1	využívané
především	především	k9	především
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Maskot	maskot	k1gInSc1	maskot
šampionátu	šampionát	k1gInSc2	šampionát
byl	být	k5eAaImAgInS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
společně	společně	k6eAd1	společně
zástupci	zástupce	k1gMnSc3	zástupce
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
IIHF	IIHF	kA	IIHF
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ruské	ruský	k2eAgFnSc2d1	ruská
hokejové	hokejový	k2eAgFnSc2d1	hokejová
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
FHR	FHR	kA	FHR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vítězka	vítězka	k1gFnSc1	vítězka
návrhu	návrh	k1gInSc2	návrh
získala	získat	k5eAaPmAgFnS	získat
odměnou	odměna	k1gFnSc7	odměna
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
200	[number]	k4	200
000	[number]	k4	000
rublů	rubl	k1gInPc2	rubl
(	(	kIx(	(
<g/>
asi	asi	k9	asi
95	[number]	k4	95
000	[number]	k4	000
českých	český	k2eAgFnPc2d1	Česká
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konečný	konečný	k2eAgInSc1d1	konečný
výběr	výběr	k1gInSc1	výběr
se	se	k3xPyFc4	se
prováděl	provádět	k5eAaImAgInS	provádět
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Odmítnutými	odmítnutý	k2eAgInPc7d1	odmítnutý
se	se	k3xPyFc4	se
tak	tak	k9	tak
nakonec	nakonec	k6eAd1	nakonec
stali	stát	k5eAaPmAgMnP	stát
kocour	kocour	k1gMnSc1	kocour
Matros	Matrosa	k1gFnPc2	Matrosa
<g/>
,	,	kIx,	,
samovar	samovar	k1gInSc1	samovar
Dymok	Dymok	k1gInSc1	Dymok
a	a	k8xC	a
ruský	ruský	k2eAgMnSc1d1	ruský
bohatýr	bohatýr	k1gMnSc1	bohatýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
nominovala	nominovat	k5eAaBmAgFnS	nominovat
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
16	[number]	k4	16
hlavních	hlavní	k2eAgMnPc2d1	hlavní
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
a	a	k8xC	a
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
čárových	čárový	k2eAgFnPc2d1	čárová
sudích	sudí	k1gFnPc2	sudí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgInSc1d1	herní
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Šestnáct	šestnáct	k4xCc1	šestnáct
účastníků	účastník	k1gMnPc2	účastník
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
8	[number]	k4	8
týmech	tým	k1gInPc6	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
utkal	utkat	k5eAaPmAgMnS	utkat
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
každým	každý	k3xTgMnSc7	každý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
udělovaly	udělovat	k5eAaImAgFnP	udělovat
3	[number]	k4	3
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
či	či	k8xC	či
samostatných	samostatný	k2eAgInPc6d1	samostatný
nájezdech	nájezd	k1gInPc6	nájezd
2	[number]	k4	2
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
či	či	k8xC	či
samostatných	samostatný	k2eAgInPc6d1	samostatný
nájezdech	nájezd	k1gInPc6	nájezd
1	[number]	k4	1
bod	bod	k1gInSc4	bod
a	a	k8xC	a
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
0	[number]	k4	0
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
skupiny	skupina	k1gFnSc2	skupina
postoupila	postoupit	k5eAaPmAgFnS	postoupit
čtveřice	čtveřice	k1gFnSc1	čtveřice
týmů	tým	k1gInPc2	tým
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
playoff	playoff	k1gInSc1	playoff
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
týmy	tým	k1gInPc4	tým
na	na	k7c6	na
pátých	pátá	k1gFnPc6	pátá
až	až	k9	až
osmých	osmý	k4xOgNnPc6	osmý
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
turnaj	turnaj	k1gInSc1	turnaj
skončil	skončit	k5eAaPmAgMnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
osmých	osmý	k4xOgNnPc2	osmý
míst	místo	k1gNnPc2	místo
automaticky	automaticky	k6eAd1	automaticky
sestoupily	sestoupit	k5eAaPmAgFnP	sestoupit
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
(	(	kIx(	(
<g/>
neplatilo	platit	k5eNaImAgNnS	platit
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
pořadatele	pořadatel	k1gMnPc4	pořadatel
MS	MS	kA	MS
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritéria	kritérion	k1gNnPc4	kritérion
při	při	k7c6	při
rovnosti	rovnost	k1gFnSc6	rovnost
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
skupinách	skupina	k1gFnPc6	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
získaly	získat	k5eAaPmAgInP	získat
po	po	k7c6	po
konci	konec	k1gInSc6	konec
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
dva	dva	k4xCgInPc4	dva
týmy	tým	k1gInPc4	tým
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
o	o	k7c6	o
postupujícím	postupující	k2eAgNnSc6d1	postupující
nebo	nebo	k8xC	nebo
o	o	k7c6	o
lépe	dobře	k6eAd2	dobře
nasazeném	nasazený	k2eAgInSc6d1	nasazený
týmu	tým	k1gInSc6	tým
pro	pro	k7c4	pro
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
výsledek	výsledek	k1gInSc1	výsledek
jejich	jejich	k3xOp3gInSc2	jejich
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
rovnost	rovnost	k1gFnSc1	rovnost
nastala	nastat	k5eAaPmAgFnS	nastat
mezi	mezi	k7c7	mezi
třemi	tři	k4xCgInPc7	tři
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
týmy	tým	k1gInPc4	tým
<g/>
,	,	kIx,	,
postupovalo	postupovat	k5eAaImAgNnS	postupovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
následujících	následující	k2eAgNnPc2d1	následující
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
nezbyly	zbýt	k5eNaPmAgInP	zbýt
dva	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
výsledek	výsledek	k1gInSc4	výsledek
ze	z	k7c2	z
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
zápasu	zápas	k1gInSc2	zápas
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Body	bod	k1gInPc4	bod
z	z	k7c2	z
minitabulky	minitabulka	k1gFnSc2	minitabulka
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
</s>
</p>
<p>
<s>
Brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
z	z	k7c2	z
minitabulky	minitabulka	k1gFnSc2	minitabulka
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
v	v	k7c6	v
minitabulce	minitabulka	k1gFnSc6	minitabulka
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
proti	proti	k7c3	proti
nejbližšímu	blízký	k2eAgInSc3d3	Nejbližší
nejvýše	nejvýše	k6eAd1	nejvýše
umístěnému	umístěný	k2eAgInSc3d1	umístěný
týmu	tým	k1gInSc3	tým
mimo	mimo	k7c4	mimo
týmy	tým	k1gInPc4	tým
v	v	k7c6	v
minitabulce	minitabulka	k1gFnSc6	minitabulka
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc1	pořadí
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
více	hodně	k6eAd2	hodně
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
<g/>
)	)	kIx)	)
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgNnSc3	tento
družstvu	družstvo	k1gNnSc3	družstvo
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
proti	proti	k7c3	proti
nejbližšímu	blízký	k2eAgInSc3d3	Nejbližší
druhému	druhý	k4xOgInSc3	druhý
nejvýše	vysoce	k6eAd3	vysoce
umístěnému	umístěný	k2eAgInSc3d1	umístěný
týmu	tým	k1gInSc3	tým
mimo	mimo	k7c4	mimo
týmů	tým	k1gInPc2	tým
v	v	k7c6	v
minitabulce	minitabulka	k1gFnSc6	minitabulka
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc1	pořadí
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
více	hodně	k6eAd2	hodně
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
<g/>
)	)	kIx)	)
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgNnSc3	tento
družstvu	družstvo	k1gNnSc3	družstvo
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
IIHF	IIHF	kA	IIHF
před	před	k7c7	před
startem	start	k1gInSc7	start
mistrovstvíV	mistrovstvíV	k?	mistrovstvíV
průběhu	průběh	k1gInSc2	průběh
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nP	by
ještě	ještě	k9	ještě
nebyly	být	k5eNaImAgInP	být
sehrány	sehrát	k5eAaPmNgInP	sehrát
všechny	všechen	k3xTgInPc1	všechen
zápasy	zápas	k1gInPc1	zápas
<g/>
,	,	kIx,	,
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
bodové	bodový	k2eAgFnSc2d1	bodová
rovnosti	rovnost	k1gFnSc2	rovnost
tato	tento	k3xDgNnPc1	tento
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nižší	nízký	k2eAgInSc1d2	nižší
počet	počet	k1gInSc1	počet
odehraných	odehraný	k2eAgNnPc2d1	odehrané
utkání	utkání	k1gNnPc2	utkání
</s>
</p>
<p>
<s>
Brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
IIHF	IIHF	kA	IIHF
před	před	k7c7	před
startem	start	k1gInSc7	start
mistrovstvíČtvrtfinále	mistrovstvíČtvrtfinále	k1gNnSc2	mistrovstvíČtvrtfinále
bylo	být	k5eAaImAgNnS	být
sehráno	sehrát	k5eAaPmNgNnS	sehrát
křížovým	křížový	k2eAgInSc7d1	křížový
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
1	[number]	k4	1
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
proti	proti	k7c3	proti
4	[number]	k4	4
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
proti	proti	k7c3	proti
3	[number]	k4	3
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
proti	proti	k7c3	proti
4	[number]	k4	4
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	A	kA	A
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
proti	proti	k7c3	proti
3	[number]	k4	3
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
poražené	poražený	k1gMnPc4	poražený
čtvrtfinalisty	čtvrtfinalista	k1gMnPc4	čtvrtfinalista
turnaj	turnaj	k1gInSc4	turnaj
skončil	skončit	k5eAaPmAgMnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
se	se	k3xPyFc4	se
dvojice	dvojice	k1gFnSc1	dvojice
utkaly	utkat	k5eAaPmAgInP	utkat
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
vzorci	vzorec	k1gInSc6	vzorec
<g/>
:	:	kIx,	:
vítěz	vítěz	k1gMnSc1	vítěz
utkání	utkání	k1gNnSc4	utkání
mezi	mezi	k7c4	mezi
1A	[number]	k4	1A
-	-	kIx~	-
4B	[number]	k4	4B
vs	vs	k?	vs
vítěz	vítěz	k1gMnSc1	vítěz
2B	[number]	k4	2B
-	-	kIx~	-
3	[number]	k4	3
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
1B	[number]	k4	1B
-	-	kIx~	-
4A	[number]	k4	4A
vs	vs	k?	vs
vítěz	vítěz	k1gMnSc1	vítěz
2A	[number]	k4	2A
-	-	kIx~	-
3	[number]	k4	3
<g/>
B.	B.	kA	B.
Oba	dva	k4xCgInPc1	dva
semifinálové	semifinálový	k2eAgInPc1d1	semifinálový
zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
ve	v	k7c4	v
větší	veliký	k2eAgFnSc4d2	veliký
VTB	VTB	kA	VTB
Ice	Ice	k1gFnSc4	Ice
Palace	Palace	k1gFnSc2	Palace
<g/>
.	.	kIx.	.
</s>
<s>
Vítězní	vítězný	k2eAgMnPc1d1	vítězný
semifinalisté	semifinalista	k1gMnPc1	semifinalista
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c4	o
držitelích	držitel	k1gMnPc6	držitel
zlatých	zlatý	k2eAgFnPc2d1	zlatá
a	a	k8xC	a
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
poražení	poražený	k2eAgMnPc1d1	poražený
semifinalisté	semifinalista	k1gMnPc1	semifinalista
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
</s>
</p>
<p>
<s>
====	====	k?	====
Systém	systém	k1gInSc1	systém
prodloužení	prodloužení	k1gNnSc2	prodloužení
v	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vyrovnaného	vyrovnaný	k2eAgInSc2d1	vyrovnaný
stavu	stav	k1gInSc2	stav
i	i	k9	i
po	po	k7c6	po
šedesáti	šedesát	k4xCc6	šedesát
minutách	minuta	k1gFnPc6	minuta
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
semifinále	semifinále	k1gNnSc2	semifinále
nebo	nebo	k8xC	nebo
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
zápas	zápas	k1gInSc4	zápas
prodlužoval	prodlužovat	k5eAaImAgInS	prodlužovat
o	o	k7c4	o
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
tříminutové	tříminutový	k2eAgFnSc6d1	tříminutová
přestávce	přestávka	k1gFnSc6	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
by	by	kYmCp3nS	by
následovalo	následovat	k5eAaImAgNnS	následovat
dvacetiminutové	dvacetiminutový	k2eAgNnSc1d1	dvacetiminutové
prodloužení	prodloužení	k1gNnSc1	prodloužení
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterým	který	k3yQgNnSc7	který
by	by	kYmCp3nS	by
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
patnáctiminutová	patnáctiminutový	k2eAgFnSc1d1	patnáctiminutová
přestávka	přestávka	k1gFnSc1	přestávka
s	s	k7c7	s
úpravou	úprava	k1gFnSc7	úprava
ledové	ledový	k2eAgFnSc2d1	ledová
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nP	by
během	během	k7c2	během
prodloužení	prodloužení	k1gNnSc2	prodloužení
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
týmů	tým	k1gInPc2	tým
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
branky	branka	k1gFnPc4	branka
<g/>
,	,	kIx,	,
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
samostatné	samostatný	k2eAgInPc1d1	samostatný
nájezdy	nájezd	k1gInPc1	nájezd
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
určily	určit	k5eAaPmAgFnP	určit
vítěze	vítěz	k1gMnPc4	vítěz
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritéria	kritérion	k1gNnPc4	kritérion
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
konečného	konečný	k2eAgNnSc2d1	konečné
pořadí	pořadí	k1gNnSc2	pořadí
týmů	tým	k1gInPc2	tým
===	===	k?	===
</s>
</p>
<p>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
čtyřech	čtyři	k4xCgNnPc6	čtyři
místech	místo	k1gNnPc6	místo
určil	určit	k5eAaPmAgInS	určit
výsledek	výsledek	k1gInSc1	výsledek
finálového	finálový	k2eAgInSc2d1	finálový
zápasu	zápas	k1gInSc2	zápas
a	a	k8xC	a
utkání	utkání	k1gNnSc2	utkání
o	o	k7c4	o
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
konečném	konečný	k2eAgNnSc6d1	konečné
umístění	umístění	k1gNnSc6	umístění
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
tato	tento	k3xDgNnPc4	tento
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgNnSc1d2	vyšší
postavení	postavení	k1gNnSc1	postavení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
</s>
</p>
<p>
<s>
Lepší	dobrý	k2eAgInSc1d2	lepší
brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
IIHF	IIHF	kA	IIHF
před	před	k7c7	před
startem	start	k1gInSc7	start
mistrovstvíPoznámka	mistrovstvíPoznámka	k1gFnSc1	mistrovstvíPoznámka
<g/>
:	:	kIx,	:
Poražení	poražený	k2eAgMnPc1d1	poražený
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1	čtvrtfinalista
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
automaticky	automaticky	k6eAd1	automaticky
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
seřazeni	seřadit	k5eAaPmNgMnP	seřadit
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
skupinách	skupina	k1gFnPc6	skupina
dle	dle	k7c2	dle
kritérií	kritérion	k1gNnPc2	kritérion
uvedených	uvedený	k2eAgFnPc2d1	uvedená
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legenda	legenda	k1gFnSc1	legenda
==	==	k?	==
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
vysvětlivek	vysvětlivka	k1gFnPc2	vysvětlivka
použitých	použitý	k2eAgInPc2d1	použitý
v	v	k7c6	v
souhrnech	souhrn	k1gInPc6	souhrn
odehraných	odehraný	k2eAgInPc2d1	odehraný
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
časy	čas	k1gInPc1	čas
zápasů	zápas	k1gInPc2	zápas
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
ve	v	k7c6	v
středoevropském	středoevropský	k2eAgInSc6d1	středoevropský
letním	letní	k2eAgInSc6d1	letní
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
+2	+2	k4	+2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
A	a	k8xC	a
–	–	k?	–
Moskva	Moskva	k1gFnSc1	Moskva
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
–	–	k?	–
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Play	play	k0	play
off	off	k?	off
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pavouk	pavouk	k1gMnSc1	pavouk
===	===	k?	===
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
časy	čas	k1gInPc1	čas
zápasů	zápas	k1gInPc2	zápas
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
ve	v	k7c6	v
středoevropském	středoevropský	k2eAgInSc6d1	středoevropský
letním	letní	k2eAgInSc6d1	letní
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
+2	+2	k4	+2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Semifinále	semifinále	k1gNnPc3	semifinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Konečné	Konečné	k2eAgNnSc1d1	Konečné
pořadí	pořadí	k1gNnSc1	pořadí
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčské	hráčský	k2eAgFnPc1d1	hráčská
statistiky	statistika	k1gFnPc1	statistika
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc1	hodnocení
hráčů	hráč	k1gMnPc2	hráč
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
hráči	hráč	k1gMnPc1	hráč
podle	podle	k7c2	podle
direktoriátu	direktoriát	k1gInSc2	direktoriát
IIHF	IIHF	kA	IIHF
===	===	k?	===
</s>
</p>
<p>
<s>
Reference	reference	k1gFnSc1	reference
<g/>
:	:	kIx,	:
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
===	===	k?	===
All	All	k1gFnPc2	All
Stars	Stars	k1gInSc1	Stars
===	===	k?	===
</s>
</p>
<p>
<s>
Reference	reference	k1gFnSc1	reference
<g/>
:	:	kIx,	:
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
===	===	k?	===
Kanadské	kanadský	k2eAgNnSc4d1	kanadské
bodování	bodování	k1gNnSc4	bodování
===	===	k?	===
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
pořadí	pořadí	k1gNnSc1	pořadí
hráčů	hráč	k1gMnPc2	hráč
podle	podle	k7c2	podle
dosažených	dosažený	k2eAgInPc2d1	dosažený
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
vstřelený	vstřelený	k2eAgInSc4d1	vstřelený
gól	gól	k1gInSc4	gól
nebo	nebo	k8xC	nebo
přihrávku	přihrávka	k1gFnSc4	přihrávka
na	na	k7c4	na
gól	gól	k1gInSc4	gól
hráč	hráč	k1gMnSc1	hráč
získává	získávat	k5eAaImIp3nS	získávat
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záp	Záp	k?	Záp
<g/>
.	.	kIx.	.
=	=	kIx~	=
Odehrané	odehraný	k2eAgInPc1d1	odehraný
zápasy	zápas	k1gInPc1	zápas
<g/>
;	;	kIx,	;
G	G	kA	G
=	=	kIx~	=
Góly	gól	k1gInPc1	gól
<g/>
;	;	kIx,	;
A	a	k8xC	a
=	=	kIx~	=
Přihrávky	přihrávka	k1gFnSc2	přihrávka
na	na	k7c4	na
gól	gól	k1gInSc4	gól
<g/>
;	;	kIx,	;
Body	bod	k1gInPc4	bod
=	=	kIx~	=
Body	bod	k1gInPc4	bod
<g/>
;	;	kIx,	;
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
−	−	k?	−
=	=	kIx~	=
Plus	plus	k1gInSc1	plus
<g/>
/	/	kIx~	/
<g/>
Minus	minus	k1gInSc1	minus
<g/>
;	;	kIx,	;
PTM	PTM	kA	PTM
=	=	kIx~	=
Počet	počet	k1gInSc1	počet
trestných	trestný	k2eAgFnPc2d1	trestná
minut	minuta	k1gFnPc2	minuta
<g/>
;	;	kIx,	;
Poz	Poz	k1gFnSc1	Poz
<g/>
.	.	kIx.	.
=	=	kIx~	=
Pozice	pozice	k1gFnSc1	pozice
</s>
</p>
<p>
<s>
===	===	k?	===
Hodnocení	hodnocení	k1gNnSc1	hodnocení
brankářů	brankář	k1gMnPc2	brankář
===	===	k?	===
</s>
</p>
<p>
<s>
Pořadí	pořadí	k1gNnSc4	pořadí
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
pěti	pět	k4xCc2	pět
brankářů	brankář	k1gMnPc2	brankář
podle	podle	k7c2	podle
úspěšnosti	úspěšnost	k1gFnSc2	úspěšnost
zásahů	zásah	k1gInPc2	zásah
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
.	.	kIx.	.
</s>
<s>
Brankář	brankář	k1gMnSc1	brankář
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
odehráno	odehrát	k5eAaPmNgNnS	odehrát
minimálně	minimálně	k6eAd1	minimálně
40	[number]	k4	40
<g/>
%	%	kIx~	%
hrací	hrací	k2eAgFnSc2d1	hrací
doby	doba	k1gFnSc2	doba
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čas	čas	k1gInSc1	čas
=	=	kIx~	=
Čas	čas	k1gInSc1	čas
na	na	k7c6	na
ledě	led	k1gInSc6	led
(	(	kIx(	(
<g/>
minuty	minuta	k1gFnSc2	minuta
<g/>
/	/	kIx~	/
<g/>
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
OG	OG	kA	OG
=	=	kIx~	=
Obdržené	obdržený	k2eAgInPc1d1	obdržený
góly	gól	k1gInPc1	gól
<g/>
;	;	kIx,	;
POG	POG	kA	POG
=	=	kIx~	=
Průměr	průměr	k1gInSc1	průměr
obdržených	obdržený	k2eAgInPc2d1	obdržený
gólů	gól	k1gInPc2	gól
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
<g/>
;	;	kIx,	;
SNB	SNB	kA	SNB
=	=	kIx~	=
Počet	počet	k1gInSc1	počet
střel	střela	k1gFnPc2	střela
na	na	k7c4	na
bránu	brána	k1gFnSc4	brána
<g/>
;	;	kIx,	;
Úsp	Úsp	k1gFnSc4	Úsp
<g/>
%	%	kIx~	%
=	=	kIx~	=
Procento	procento	k1gNnSc1	procento
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
zásahů	zásah	k1gInPc2	zásah
<g/>
;	;	kIx,	;
ČK	ČK	kA	ČK
=	=	kIx~	=
Čistá	čistá	k1gFnSc1	čistá
konta	konto	k1gNnSc2	konto
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
I	I	kA	I
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
II	II	kA	II
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
III	III	kA	III
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2016	[number]	k4	2016
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
MS	MS	kA	MS
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2016	[number]	k4	2016
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
