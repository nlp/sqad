<p>
<s>
Bělásek	bělásek	k1gMnSc1	bělásek
východní	východní	k2eAgMnSc1d1	východní
(	(	kIx(	(
<g/>
Leptidea	Leptidea	k1gMnSc1	Leptidea
morsei	morse	k1gFnSc2	morse
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
motýla	motýl	k1gMnSc2	motýl
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
běláskovitých	běláskovitý	k2eAgMnPc2d1	běláskovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
se	se	k3xPyFc4	se
roztroušeně	roztroušeně	k6eAd1	roztroušeně
až	až	k9	až
vzácně	vzácně	k6eAd1	vzácně
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgNnSc1d1	jihovýchodní
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
již	již	k6eAd1	již
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
nebyl	být	k5eNaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
Sibiř	Sibiř	k1gFnSc4	Sibiř
až	až	k9	až
po	po	k7c4	po
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
řídké	řídký	k2eAgInPc4d1	řídký
<g/>
,	,	kIx,	,
prosvětlené	prosvětlený	k2eAgInPc4d1	prosvětlený
lesní	lesní	k2eAgInPc4d1	lesní
porosty	porost	k1gInPc4	porost
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
lesních	lesní	k2eAgFnPc2d1	lesní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Housenky	housenka	k1gFnPc1	housenka
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
na	na	k7c4	na
Lathyrus	Lathyrus	k1gInSc4	Lathyrus
niger	niger	k1gMnSc1	niger
a	a	k8xC	a
Lathyrus	Lathyrus	k1gMnSc1	Lathyrus
vernus	vernus	k1gMnSc1	vernus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Mlynárik	Mlynárika	k1gFnPc2	Mlynárika
východný	východný	k2eAgMnSc1d1	východný
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bělásek	bělásek	k1gMnSc1	bělásek
východní	východní	k2eAgMnSc1d1	východní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Leptidea	Leptide	k1gInSc2	Leptide
morsei	morsei	k6eAd1	morsei
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Bělásek	bělásek	k1gMnSc1	bělásek
východní-	východní-	k?	východní-
NATURA	NATURA	kA	NATURA
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Leptidea	Leptidea	k6eAd1	Leptidea
morsei	morsei	k6eAd1	morsei
Fenton	Fenton	k1gInSc1	Fenton
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
-	-	kIx~	-
bělásek	bělásek	k1gMnSc1	bělásek
východní	východní	k2eAgFnSc2d1	východní
-	-	kIx~	-
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
lokality	lokalita	k1gFnSc2	lokalita
výskytu	výskyt	k1gInSc2	výskyt
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
bělásek	bělásek	k1gMnSc1	bělásek
východní	východní	k2eAgFnSc2d1	východní
</s>
</p>
