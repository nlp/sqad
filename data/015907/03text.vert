<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
Kolping	Kolping	k1gInSc4
ArenaNázev	ArenaNázev	k1gFnSc2
</s>
<s>
Eishockeyclub	Eishockeyclub	k1gInSc1
Kloten	Kloten	k2eAgInSc1d1
Stát	stát	k1gInSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Kloten	Kloten	k2eAgInSc1d1
Založen	založen	k2eAgInSc1d1
</s>
<s>
1934	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Asociace	asociace	k1gFnSc2
</s>
<s>
SIHF	SIHF	kA
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Swiss	Swiss	k1gInSc1
League	Leagu	k1gFnSc2
Klubové	klubový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
a	a	k8xC
červená	červený	k2eAgFnSc1d1
Stadion	stadion	k1gNnSc4
Název	název	k1gInSc1
</s>
<s>
Kolping	Kolping	k1gInSc1
Arena	Aren	k1gInSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Kloten	Kloten	k2eAgInSc1d1
Kapacita	kapacita	k1gFnSc1
</s>
<s>
7	#num#	k4
624	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Klubové	klubový	k2eAgInPc1d1
úspěchy	úspěch	k1gInPc1
Tituly	titul	k1gInPc4
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Švýcarska	Švýcarsko	k1gNnSc2
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Championnat	Championnat	k5eAaPmF,k5eAaImF
/	/	kIx~
National	National	k1gFnSc4
League	Leagu	k1gInSc2
A	a	k9
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
1982	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
1988	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc4d1
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
</s>
<s>
Kloten	Kloten	k2eAgInSc1d1
Flyers	Flyers	k1gInSc1
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
</s>
<s>
Kloten	Kloten	k2eAgInSc1d1
Flyers	Flyers	k1gInSc1
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
</s>
<s>
Kloten	Kloten	k2eAgInSc1d1
Flyers	Flyers	k1gInSc1
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
</s>
<s>
Kloten	Kloten	k2eAgInSc1d1
Flyers	Flyers	k1gInSc1
</s>
<s>
EHC	EHC	kA
Kloten	Kloten	k1gInSc1
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Eishockeyclub	Eishockeyclub	k1gInSc1
Kloten	Kloten	k2eAgInSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
švýcarský	švýcarský	k2eAgInSc4d1
klub	klub	k1gInSc4
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
sídlí	sídlet	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
Kloten	Klotno	k1gNnPc2
v	v	k7c6
kantonu	kanton	k1gInSc6
Curych	Curych	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založen	založen	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2000	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
působil	působit	k5eAaImAgInS
pod	pod	k7c7
názvem	název	k1gInSc7
Kloten	Kloten	k1gInSc1
Flyers	Flyers	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švýcarským	švýcarský	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
celkem	celkem	k6eAd1
pětkrát	pětkrát	k6eAd1
<g/>
,	,	kIx,
poslední	poslední	k2eAgInSc4d1
titul	titul	k1gInSc4
získal	získat	k5eAaPmAgInS
Kloten	Kloten	k2eAgMnSc1d1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1962	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
by	by	kYmCp3nS
Kloten	Klotno	k1gNnPc2
nepřetržitým	přetržitý	k2eNgMnSc7d1
účastníkem	účastník	k1gMnSc7
švýcarské	švýcarský	k2eAgFnSc2d1
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
působí	působit	k5eAaImIp3nS
ve	v	k7c4
Swiss	Swiss	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
,	,	kIx,
druhé	druhý	k4xOgFnSc6
švýcarské	švýcarský	k2eAgFnSc6d1
nejvyšší	vysoký	k2eAgFnSc6d3
soutěži	soutěž	k1gFnSc6
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubové	klubový	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgNnPc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgNnPc1d1
a	a	k8xC
červená	červený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c4
Kolping	Kolping	k1gInSc4
Areně	Areeň	k1gFnSc2
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
7	#num#	k4
624	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1934	#num#	k4
–	–	k?
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc1d1
(	(	kIx(
<g/>
Eishockeyclub	Eishockeyclub	k1gInSc1
Kloten	Kloten	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
–	–	k?
Kloten	Kloten	k2eAgInSc4d1
Flyers	Flyers	k1gInSc4
</s>
<s>
2016	#num#	k4
–	–	k?
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc1d1
(	(	kIx(
<g/>
Eishockeyclub	Eishockeyclub	k1gInSc1
Kloten	Kloten	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Získané	získaný	k2eAgFnPc4d1
trofeje	trofej	k1gFnPc4
</s>
<s>
Championnat	Championnat	k5eAaPmF,k5eAaImF
/	/	kIx~
National	National	k1gFnSc4
League	Leagu	k1gMnSc2
A	A	kA
(	(	kIx(
5	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Schweizer	Schweizer	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
1	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Hokejisté	hokejista	k1gMnPc1
Československa	Československo	k1gNnSc2
/	/	kIx~
Česka	Česko	k1gNnSc2
a	a	k8xC
Slovenska	Slovensko	k1gNnSc2
v	v	k7c6
dresu	dres	k1gInSc6
EHC	EHC	kA
Kloten	Kloten	k2eAgMnSc1d1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
Sezona	sezona	k1gFnSc1
</s>
<s>
Národnost	národnost	k1gFnSc1
</s>
<s>
Michal	Michal	k1gMnSc1
Bláha	Bláha	k1gMnSc1
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
</s>
<s>
Peter	Peter	k1gMnSc1
Ihnačák	Ihnačák	k1gMnSc1
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kučera	Kučera	k1gMnSc1
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Hybler	Hybler	k1gMnSc1
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
</s>
<s>
Patrick	Patrick	k1gMnSc1
Kučera	Kučera	k1gMnSc1
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
</s>
<s>
David	David	k1gMnSc1
Hruška	Hruška	k1gMnSc1
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
</s>
<s>
Petr	Petr	k1gMnSc1
Pavlas	Pavlas	k1gMnSc1
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
</s>
<s>
Radek	Radek	k1gMnSc1
Hamr	hamr	k1gInSc4
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
</s>
<s>
Přehled	přehled	k1gInSc1
ligové	ligový	k2eAgFnSc2d1
účasti	účast	k1gFnSc2
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1940	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
:	:	kIx,
Serie	serie	k1gFnSc2
C	C	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
ligová	ligový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
:	:	kIx,
Serie	serie	k1gFnSc2
B	B	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
ligová	ligový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
1947	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
:	:	kIx,
National	National	k1gFnSc2
League	Leagu	k1gFnSc2
B	B	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
ligová	ligový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liga	liga	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
ligová	ligový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
:	:	kIx,
National	National	k1gFnSc2
League	Leagu	k1gFnSc2
B	B	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
ligová	ligový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
<g/>
:	:	kIx,
National	National	k1gFnSc2
League	Leagu	k1gFnSc2
B	B	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
ligová	ligový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
1955	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
:	:	kIx,
National	National	k1gFnSc2
League	Leagu	k1gFnSc2
B	B	kA
Ost	Ost	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
ligová	ligový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
1962	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
:	:	kIx,
National	National	k1gFnSc2
League	Leagu	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
ligová	ligový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
:	:	kIx,
Swiss	Swiss	k1gInSc1
League	Leagu	k1gInSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
ligová	ligový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
mezinárodních	mezinárodní	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
SP	SP	kA
-	-	kIx~
Spenglerův	Spenglerův	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
EHP	EHP	kA
-	-	kIx~
Evropský	evropský	k2eAgInSc1d1
hokejový	hokejový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
,	,	kIx,
EHL	EHL	kA
-	-	kIx~
Evropská	evropský	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
,	,	kIx,
SSix	SSix	k1gInSc1
-	-	kIx~
Super	super	k2eAgInSc1d1
six	six	k?
<g/>
,	,	kIx,
IIHFSup	IIHFSup	k1gMnSc1
-	-	kIx~
IIHF	IIHF	kA
Superpohár	superpohár	k1gInSc1
<g/>
,	,	kIx,
VC	VC	kA
-	-	kIx~
Victoria	Victorium	k1gNnSc2
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
HLMI	HLMI	kA
-	-	kIx~
Hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
IIHF	IIHF	kA
<g/>
,	,	kIx,
ET	ET	kA
-	-	kIx~
European	European	k1gMnSc1
Trophy	Tropha	k1gFnSc2
<g/>
,	,	kIx,
HLM	HLM	kA
-	-	kIx~
Hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
KP	KP	kA
–	–	k?
Kontinentální	kontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
EHP	EHP	kA
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
SP	SP	kA
1990	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
EHP	EHP	kA
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
–	–	k?
Semifinálová	semifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
EHP	EHP	kA
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
–	–	k?
Semifinálová	semifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
EHP	EHP	kA
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
–	–	k?
Semifinálová	semifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
EHP	EHP	kA
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
–	–	k?
Semifinálová	semifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
SP	SP	kA
2011	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
HLM	HLM	kA
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
I	I	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Chronik	chronik	k1gMnSc1
des	des	k1gNnSc2
EHC	EHC	kA
Kloten	Kloten	k2eAgMnSc1d1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
kloten-flyers	kloten-flyers	k6eAd1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
"	"	kIx"
<g/>
Kolping	Kolping	k1gInSc1
Arena	Aren	k1gInSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hockeyarenas	hockeyarenas	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Hockey	Hockea	k1gFnSc2
Archives	Archives	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hockeyarchives	hockeyarchives	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
EHC	EHC	kA
Kloten	Kloten	k2eAgInSc1d1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hockeyarenas	hockeyarenas	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Werner	Werner	k1gMnSc1
Schweizer	Schweizer	k1gMnSc1
<g/>
,	,	kIx,
Jürg	Jürg	k1gMnSc1
Vogel	Vogel	k1gMnSc1
<g/>
,	,	kIx,
Klaus	Klaus	k1gMnSc1
Zaugg	Zaugg	k1gMnSc1
<g/>
:	:	kIx,
Eiszeiten	Eiszeiten	k2eAgMnSc1d1
<g/>
:	:	kIx,
Das	Das	k1gMnSc1
Jahrhundert	Jahrhundert	k1gInSc4
des	des	k1gNnSc2
Schweizer	Schweizra	k1gFnPc2
Eishockeys	Eishockeysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schwanden	Schwandna	k1gFnPc2
<g/>
,	,	kIx,
Kilchberg	Kilchberg	k1gInSc1
1999	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
9520363	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
EHC	EHC	kA
Kloten	Klotno	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
National	National	k1gMnSc1
League	Leagu	k1gMnSc2
A	A	kA
–	–	k?
švýcarská	švýcarský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
(	(	kIx(
<g/>
ročníky	ročník	k1gInPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
ocenění	ocenění	k1gNnSc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
Kluby	klub	k1gInPc7
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
HC	HC	kA
Ambrì	Ambrì	k1gFnSc1
•	•	k?
SC	SC	kA
Bern	Bern	k1gInSc1
•	•	k?
EHC	EHC	kA
Biel	Biel	k1gInSc1
•	•	k?
HC	HC	kA
Davos	Davos	k1gMnSc1
•	•	k?
Fribourg-Gottéron	Fribourg-Gottéron	k1gMnSc1
•	•	k?
HC	HC	kA
Servette	Servett	k1gInSc5
Ženeva	Ženeva	k1gFnSc1
•	•	k?
HC	HC	kA
Lausanne	Lausanne	k1gNnSc1
•	•	k?
HC	HC	kA
Lugano	Lugana	k1gFnSc5
•	•	k?
SCL	SCL	kA
Tigers	Tigers	k1gInSc1
•	•	k?
SC	SC	kA
Rapperswil-Jona	Rapperswil-Jona	k1gFnSc1
Lakers	Lakers	k1gInSc1
•	•	k?
EV	Eva	k1gFnPc2
Zug	Zug	k1gFnSc1
•	•	k?
ZSC	ZSC	kA
Lions	Lions	k1gInSc4
Sezóny	sezóna	k1gFnSc2
</s>
<s>
Championnat	Championnat	k5eAaImF,k5eAaPmF
National	National	k1gFnSc4
<g/>
:	:	kIx,
1908	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
1914	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
1915	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
16	#num#	k4
•	•	k?
1916	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
1917	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
1918	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
•	•	k?
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
•	•	k?
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
•	•	k?
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
•	•	k?
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
•	•	k?
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
•	•	k?
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
•	•	k?
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
•	•	k?
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
National	National	k1gMnSc1
League	League	k1gFnSc3
A	A	kA
<g/>
:	:	kIx,
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
•	•	k?
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
•	•	k?
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
•	•	k?
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
•	•	k?
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
•	•	k?
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
•	•	k?
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
•	•	k?
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
•	•	k?
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
•	•	k?
1956	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
57	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
•	•	k?
2021	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
HC	HC	kA
Les	les	k1gInSc1
Diablerets	Diablerets	k1gInSc1
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leysin	Leysin	k1gInSc1
SC	SC	kA
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Les	les	k1gInSc1
Avants	Avants	k1gInSc1
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Bern	Bern	k1gInSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SC	SC	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Engelberg	Engelberg	k1gInSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
La	la	k1gNnSc4
Villa	Villo	k1gNnSc2
Lausanne	Lausanne	k1gNnSc2
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Bellerive	Belleriev	k1gFnSc2
Vevey	Vevea	k1gFnSc2
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Club	club	k1gInSc1
des	des	k1gNnSc1
Patineurs	Patineursa	k1gFnPc2
de	de	k?
Lausanne	Lausanne	k1gNnSc6
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Caux	Caux	k1gInSc1
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Rosey	Rosea	k1gFnSc2
Gstaad	Gstaad	k1gInSc1
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lycée	Lycée	k1gInSc1
Jaccard	Jaccard	k1gInSc1
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gymnastische	Gymnastische	k1gInSc1
Gesellschaft	Gesellschaft	k1gMnSc1
Bern	Bern	k1gInSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Star	Star	kA
Lausanne	Lausanne	k1gNnPc2
HC	HC	kA
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Château-d	Château-d	k1gInSc1
<g/>
'	'	kIx"
<g/>
Œ	Œ	k1gInSc1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Akademischer	Akademischra	k1gFnPc2
EHC	EHC	kA
Zürich	Zürich	k1gMnSc1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
)	)	kIx)
•	•	k?
NEHC	NEHC	kA
Basel	Basel	k1gInSc1
(	(	kIx(
<g/>
1937	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
38	#num#	k4
<g/>
)	)	kIx)
•	•	k?
EHC	EHC	kA
St.	st.	kA
Moritz	Moritz	k1gMnSc1
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Neuchâtel	Neuchâtel	k1gMnSc1
Young	Young	k1gMnSc1
Sprinters	Sprintersa	k1gFnPc2
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
)	)	kIx)
•	•	k?
GCK	GCK	kA
Lions	Lions	k1gInSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
)	)	kIx)
•	•	k?
EHC	EHC	kA
Visp	Visp	k1gInSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Villars	Villars	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
)	)	kIx)
•	•	k?
EHC	EHC	kA
Arosa	Arosa	k1gFnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Sierre	Sierr	k1gInSc5
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Ajoie	Ajoie	k1gFnSc2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
93	#num#	k4
<g/>
)	)	kIx)
•	•	k?
EHC	EHC	kA
Olten	Olten	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SC	SC	kA
Herisau	Herisaus	k1gInSc6
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
La	la	k1gNnSc4
Chaux-de-Fonds	Chaux-de-Fonds	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
EHC	EHC	kA
Chur	Chur	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
EHC	EHC	kA
Basel	Basel	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
EHC	EHC	kA
Kloten	Kloten	k2eAgMnSc1d1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
Přehled	přehled	k1gInSc1
vítězůindividuálních	vítězůindividuální	k2eAgFnPc2d1
statistik	statistika	k1gFnPc2
</s>
<s>
Nejproduktivnější	produktivní	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
nahrávač	nahrávač	k1gMnSc1
•	•	k?
Nejtrestanější	trestaný	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgFnSc1d3
statistika	statistika	k1gFnSc1
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
•	•	k?
Nejvyšší	vysoký	k2eAgFnSc1d3
úspěšnost	úspěšnost	k1gFnSc1
zákroků	zákrok	k1gInPc2
Ocenění	oceněný	k2eAgMnPc1d1
</s>
<s>
Brankář	brankář	k1gMnSc1
roku	rok	k1gInSc2
•	•	k?
Hráč	hráč	k1gMnSc1
roku	rok	k1gInSc2
</s>
<s>
Swiss	Swiss	k1gInSc1
League	League	k1gNnSc1
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
HC	HC	kA
Ajoie	Ajoie	k1gFnSc1
•	•	k?
GCK	GCK	kA
Lions	Lions	k1gInSc1
•	•	k?
EHC	EHC	kA
Kloten	Klotno	k1gNnPc2
•	•	k?
SC	SC	kA
Langenthal	Langenthal	k1gMnSc1
•	•	k?
HC	HC	kA
La	la	k1gNnSc1
Chaux-de-Fonds	Chaux-de-Fonds	k1gInSc1
•	•	k?
EHC	EHC	kA
Olten	Olten	k1gInSc1
•	•	k?
HC	HC	kA
Thurgau	Thurga	k1gMnSc3
•	•	k?
EHC	EHC	kA
Visp	Visp	k1gInSc1
•	•	k?
EHC	EHC	kA
Winterthur	Winterthur	k1gMnSc1
•	•	k?
HCB	HCB	kA
Ticino	Ticino	k1gNnSc1
Rockets	Rockets	k1gInSc1
•	•	k?
EVZ	EVZ	kA
Academy	Academa	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
|	|	kIx~
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
</s>
