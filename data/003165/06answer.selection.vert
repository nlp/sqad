<s>
Věznice	věznice	k1gFnPc1	věznice
nebo	nebo	k8xC	nebo
také	také	k9	také
–	–	k?	–
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
či	či	k8xC	či
dobového	dobový	k2eAgInSc2d1	dobový
kontextu	kontext	k1gInSc2	kontext
a	a	k8xC	a
terminologie	terminologie	k1gFnSc2	terminologie
–	–	k?	–
vězení	vězení	k1gNnSc1	vězení
<g/>
,	,	kIx,	,
káznice	káznice	k1gFnSc1	káznice
<g/>
,	,	kIx,	,
šatlava	šatlava	k1gFnSc1	šatlava
<g/>
,	,	kIx,	,
žalář	žalář	k1gInSc1	žalář
<g/>
,	,	kIx,	,
nápravně	nápravně	k6eAd1	nápravně
výchovné	výchovný	k2eAgNnSc4d1	výchovné
zařízení	zařízení	k1gNnSc4	zařízení
či	či	k8xC	či
dříve	dříve	k6eAd2	dříve
nápravně	nápravně	k6eAd1	nápravně
výchovný	výchovný	k2eAgInSc1d1	výchovný
ústav	ústav	k1gInSc1	ústav
(	(	kIx(	(
<g/>
NVÚ	NVÚ	kA	NVÚ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
trest	trest	k1gInSc1	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
