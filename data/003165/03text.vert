<s>
Věznice	věznice	k1gFnPc1	věznice
nebo	nebo	k8xC	nebo
také	také	k9	také
–	–	k?	–
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
či	či	k8xC	či
dobového	dobový	k2eAgInSc2d1	dobový
kontextu	kontext	k1gInSc2	kontext
a	a	k8xC	a
terminologie	terminologie	k1gFnSc2	terminologie
–	–	k?	–
vězení	vězení	k1gNnSc1	vězení
<g/>
,	,	kIx,	,
káznice	káznice	k1gFnSc1	káznice
<g/>
,	,	kIx,	,
šatlava	šatlava	k1gFnSc1	šatlava
<g/>
,	,	kIx,	,
žalář	žalář	k1gInSc1	žalář
<g/>
,	,	kIx,	,
nápravně	nápravně	k6eAd1	nápravně
výchovné	výchovný	k2eAgNnSc4d1	výchovné
zařízení	zařízení	k1gNnSc4	zařízení
či	či	k8xC	či
dříve	dříve	k6eAd2	dříve
nápravně	nápravně	k6eAd1	nápravně
výchovný	výchovný	k2eAgInSc1d1	výchovný
ústav	ústav	k1gInSc1	ústav
(	(	kIx(	(
<g/>
NVÚ	NVÚ	kA	NVÚ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
trest	trest	k1gInSc1	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vězení	vězení	k1gNnSc2	vězení
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
vnější	vnější	k2eAgFnSc2d1	vnější
ostrahy	ostraha	k1gFnSc2	ostraha
<g/>
,	,	kIx,	,
zajištění	zajištění	k1gNnSc1	zajištění
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
uplatňování	uplatňování	k1gNnSc2	uplatňování
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
programu	program	k1gInSc2	program
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
věznice	věznice	k1gFnSc1	věznice
s	s	k7c7	s
dohledem	dohled	k1gInSc7	dohled
věznice	věznice	k1gFnSc2	věznice
s	s	k7c7	s
dozorem	dozor	k1gInSc7	dozor
věznice	věznice	k1gFnSc2	věznice
s	s	k7c7	s
ostrahou	ostraha	k1gFnSc7	ostraha
věznice	věznice	k1gFnSc2	věznice
se	s	k7c7	s
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
ostrahou	ostraha	k1gFnSc7	ostraha
Pro	pro	k7c4	pro
mladistvé	mladistvý	k1gMnPc4	mladistvý
je	být	k5eAaImIp3nS	být
zřizován	zřizován	k2eAgInSc1d1	zřizován
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
typ	typ	k1gInSc1	typ
vězení	vězení	k1gNnSc2	vězení
-	-	kIx~	-
věznice	věznice	k1gFnSc2	věznice
pro	pro	k7c4	pro
mladistvé	mladistvý	k1gMnPc4	mladistvý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
výše	vysoce	k6eAd2	vysoce
zmíněným	zmíněný	k2eAgInSc7d1	zmíněný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
pro	pro	k7c4	pro
mladistvé	mladistvý	k1gMnPc4	mladistvý
jsou	být	k5eAaImIp3nP	být
umisťováni	umisťován	k2eAgMnPc1d1	umisťován
odsouzení	odsouzený	k1gMnPc1	odsouzený
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
ještě	ještě	k9	ještě
není	být	k5eNaImIp3nS	být
osmnáct	osmnáct	k4xCc4	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
tak	tak	k6eAd1	tak
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
typu	typ	k1gInSc2	typ
vězení	vězení	k1gNnSc2	vězení
je	být	k5eAaImIp3nS	být
odsouzený	odsouzený	k1gMnSc1	odsouzený
zařazen	zařadit	k5eAaPmNgMnS	zařadit
<g/>
,	,	kIx,	,
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
soud	soud	k1gInSc1	soud
podle	podle	k7c2	podle
závažnosti	závažnost	k1gFnSc2	závažnost
spáchaného	spáchaný	k2eAgInSc2d1	spáchaný
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
a	a	k8xC	a
podle	podle	k7c2	podle
osoby	osoba	k1gFnSc2	osoba
pachatele	pachatel	k1gMnSc2	pachatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
36	[number]	k4	36
věznic	věznice	k1gFnPc2	věznice
<g/>
,	,	kIx,	,
z	z	k7c2	z
toto	tento	k3xDgNnSc4	tento
10	[number]	k4	10
vazebních	vazební	k2eAgFnPc2d1	vazební
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Vězeňská	vězeňský	k2eAgFnSc1d1	vězeňská
služba	služba	k1gFnSc1	služba
ČR	ČR	kA	ČR
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
přeplněností	přeplněnost	k1gFnSc7	přeplněnost
svých	svůj	k3xOyFgFnPc2	svůj
kapacit	kapacita	k1gFnPc2	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
vězněných	vězněný	k2eAgFnPc2d1	vězněná
osob	osoba	k1gFnPc2	osoba
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
22	[number]	k4	22
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
2,5	[number]	k4	2,5
tisíce	tisíc	k4xCgInSc2	tisíc
obviněných	obviněný	k1gMnPc2	obviněný
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
platné	platný	k2eAgInPc4d1	platný
k	k	k7c3	k
únoru	únor	k1gInSc3	únor
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
věznice	věznice	k1gFnSc1	věznice
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgMnSc2	svůj
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc4d1	centrální
správu	správa	k1gFnSc4	správa
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
generální	generální	k2eAgMnSc1d1	generální
ředitelství	ředitelství	k1gNnSc4	ředitelství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Vězeňská	vězeňský	k2eAgFnSc1d1	vězeňská
služba	služba	k1gFnSc1	služba
provoz	provoz	k1gInSc1	provoz
detenčního	detenční	k2eAgInSc2d1	detenční
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečovací	zabezpečovací	k2eAgFnSc1d1	zabezpečovací
detence	detence	k1gFnSc1	detence
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
ochranného	ochranný	k2eAgNnSc2d1	ochranné
opatření	opatření	k1gNnSc2	opatření
pro	pro	k7c4	pro
pachatele	pachatel	k1gMnSc4	pachatel
závažné	závažný	k2eAgFnSc2d1	závažná
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
osoby	osoba	k1gFnPc4	osoba
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
nebezpečnosti	nebezpečnost	k1gFnSc2	nebezpečnost
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
skýtá	skýtat	k5eAaImIp3nS	skýtat
48	[number]	k4	48
lůžek	lůžko	k1gNnPc2	lůžko
<g/>
,	,	kIx,	,
dalších	další	k2eAgNnPc2d1	další
150	[number]	k4	150
míst	místo	k1gNnPc2	místo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
Věznice	věznice	k1gFnSc2	věznice
Mírov	Mírov	k1gInSc1	Mírov
(	(	kIx(	(
<g/>
vězeň	vězeň	k1gMnSc1	vězeň
Jiří	Jiří	k1gMnSc1	Jiří
Kajínek	Kajínek	k1gMnSc1	Kajínek
<g/>
)	)	kIx)	)
Věznice	věznice	k1gFnSc1	věznice
Pankrác	Pankrác	k1gFnSc4	Pankrác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Věznice	věznice	k1gFnSc2	věznice
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Věznice	věznice	k1gFnSc2	věznice
Minkovice	Minkovice	k1gFnSc2	Minkovice
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
(	(	kIx(	(
<g/>
bachař	bachař	k1gMnSc1	bachař
Josef	Josef	k1gMnSc1	Josef
Vondruška	Vondruška	k1gMnSc1	Vondruška
<g/>
)	)	kIx)	)
Věznice	věznice	k1gFnSc1	věznice
Bory	bor	k1gInPc1	bor
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
Valdická	valdický	k2eAgFnSc1d1	Valdická
kartouza	kartouza	k1gFnSc1	kartouza
Věznice	věznice	k1gFnSc2	věznice
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
(	(	kIx(	(
<g/>
vyšetřovatel	vyšetřovatel	k1gMnSc1	vyšetřovatel
Alois	Alois	k1gMnSc1	Alois
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
<g/>
)	)	kIx)	)
svět	svět	k1gInSc1	svět
Věznice	věznice	k1gFnSc2	věznice
Leopoldov	Leopoldov	k1gInSc1	Leopoldov
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
Věznice	věznice	k1gFnSc1	věznice
Abu-Ghraib	Abu-Ghraiba	k1gFnPc2	Abu-Ghraiba
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
Alcatraz	Alcatraz	k1gInSc4	Alcatraz
u	u	k7c2	u
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
Bastilla	Bastilla	k1gFnSc1	Bastilla
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgNnSc1d1	bývalé
francouzské	francouzský	k2eAgNnSc1d1	francouzské
státní	státní	k2eAgNnSc1d1	státní
vězení	vězení	k1gNnSc1	vězení
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
věznice	věznice	k1gFnSc2	věznice
Guantánamo	Guantánama	k1gFnSc5	Guantánama
Věznice	věznice	k1gFnPc1	věznice
Cherche-Midi	Cherche-Mid	k1gMnPc1	Cherche-Mid
Pevnost	pevnost	k1gFnSc1	pevnost
Boyard	Boyard	k1gMnSc1	Boyard
San	San	k1gMnSc1	San
Quentin	Quentin	k1gMnSc1	Quentin
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
v	v	k7c6	v
USA	USA	kA	USA
Sing	Sing	k1gMnSc1	Sing
Sing	Sing	k1gMnSc1	Sing
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
pracovní	pracovní	k2eAgInSc1d1	pracovní
tábor	tábor	k1gInSc1	tábor
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
vězeňská	vězeňský	k2eAgFnSc1d1	vězeňská
loď	loď	k1gFnSc1	loď
galeje	galej	k1gFnSc2	galej
koncentrační	koncentrační	k2eAgInSc1d1	koncentrační
tábor	tábor	k1gInSc1	tábor
hladomorna	hladomorna	k1gFnSc1	hladomorna
Dozorce	dozorce	k1gMnSc1	dozorce
Seznam	seznam	k1gInSc1	seznam
věznic	věznice	k1gFnPc2	věznice
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
FOUCAULT	FOUCAULT	kA	FOUCAULT
<g/>
,	,	kIx,	,
Michel	Michel	k1gMnSc1	Michel
<g/>
.	.	kIx.	.
</s>
<s>
Dohlížet	dohlížet	k5eAaImF	dohlížet
a	a	k8xC	a
trestat	trestat	k5eAaImF	trestat
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
zrodu	zrod	k1gInSc6	zrod
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Čestmír	Čestmír	k1gMnSc1	Čestmír
Pelikán	Pelikán	k1gMnSc1	Pelikán
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Dauphin	dauphin	k1gMnSc1	dauphin
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
427	[number]	k4	427
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Studie	studie	k1gFnSc1	studie
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86019	[number]	k4	86019
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
věznice	věznice	k1gFnSc2	věznice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
věznice	věznice	k1gFnSc2	věznice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
