<s desamb="1">
Založeno	založen	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
patentem	patent	k1gInSc7
Jindřicha	Jindřich	k1gMnSc2
VIII	VIII	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1534	#num#	k4
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nepřetržitě	přetržitě	k6eNd1
funguje	fungovat	k5eAaImIp3nS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jej	on	k3xPp3gMnSc4
činí	činit	k5eAaImIp3nS
nejstarším	starý	k2eAgNnSc7d3
nakladatelstvím	nakladatelství	k1gNnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>