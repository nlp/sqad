<s>
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
</s>
<s>
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1534	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tudor	tudor	k1gInSc4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
sídla	sídlo	k1gNnSc2
</s>
<s>
52	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
55	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnSc2
Mateřská	mateřský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInPc1d1
web	web	k1gInSc4
</s>
<s>
www.cambridge.org	www.cambridge.org	k1gInSc1
LEI	lei	k1gInSc2
</s>
<s>
9845008ED9D56E38A424	9845008ED9D56E38A424	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
je	být	k5eAaImIp3nS
akademické	akademický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
a	a	k8xC
vydavatelství	vydavatelství	k1gNnSc1
oficiálních	oficiální	k2eAgInPc2d1
dokumentů	dokument	k1gInPc2
Univerzity	univerzita	k1gFnSc2
v	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založeno	založen	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
patentem	patent	k1gInSc7
Jindřicha	Jindřich	k1gMnSc2
VIII	VIII	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1534	#num#	k4
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nepřetržitě	přetržitě	k6eNd1
funguje	fungovat	k5eAaImIp3nS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jej	on	k3xPp3gMnSc4
činí	činit	k5eAaImIp3nS
nejstarším	starý	k2eAgNnSc7d3
nakladatelstvím	nakladatelství	k1gNnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
produkuje	produkovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
200	#num#	k4
časopisů	časopis	k1gInPc2
a	a	k8xC
2000	#num#	k4
knih	kniha	k1gFnPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
též	též	k9
bible	bible	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Cambridge	Cambridge	k1gFnSc2
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
</s>
<s>
A	a	k9
Brief	Brief	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
Cambridge	Cambridge	k1gFnSc2
University	universita	k1gFnSc2
Press	Press	k1gInSc1
<g/>
/	/	kIx~
<g/>
Strucna	Strucna	k1gFnSc1
historie	historie	k1gFnSc2
vydavatelstvi	vydavatelstev	k1gFnSc3
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Cambridge	Cambridge	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2002102052	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1008744-8	1008744-8	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1088	#num#	k4
0337	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50059133	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
127979069	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50059133	#num#	k4
</s>
