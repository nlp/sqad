<s>
Beltain	Beltain	k1gInSc1	Beltain
či	či	k8xC	či
Beltine	Beltin	k1gInSc5	Beltin
je	být	k5eAaImIp3nS	být
keltský	keltský	k2eAgInSc1d1	keltský
svátek	svátek	k1gInSc1	svátek
jara	jaro	k1gNnSc2	jaro
slavený	slavený	k2eAgInSc1d1	slavený
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
každý	každý	k3xTgInSc1	každý
keltský	keltský	k2eAgInSc1d1	keltský
den	den	k1gInSc1	den
začíná	začínat	k5eAaImIp3nS	začínat
večerem	večer	k1gInSc7	večer
<g/>
.	.	kIx.	.
</s>
<s>
Oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
příchod	příchod	k1gInSc1	příchod
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
zrození	zrození	k1gNnSc2	zrození
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
po	po	k7c6	po
zimě	zima	k1gFnSc6	zima
zase	zase	k9	zase
hospodařit	hospodařit	k5eAaImF	hospodařit
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Slaví	slavit	k5eAaImIp3nS	slavit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jako	jako	k9	jako
protipól	protipól	k1gInSc4	protipól
Samhainu	Samhain	k1gInSc2	Samhain
(	(	kIx(	(
<g/>
Halloweenu	Halloween	k1gInSc2	Halloween
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
po	po	k7c4	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
je	být	k5eAaImIp3nS	být
slavený	slavený	k2eAgInSc1d1	slavený
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
keltského	keltský	k2eAgMnSc2d1	keltský
boha	bůh	k1gMnSc2	bůh
Belena	Belen	k1gMnSc2	Belen
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
léčivých	léčivý	k2eAgFnPc2d1	léčivá
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Beltine	Beltin	k1gMnSc5	Beltin
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
zářící	zářící	k2eAgInSc1d1	zářící
oheň	oheň	k1gInSc1	oheň
či	či	k8xC	či
Belenův	Belenův	k2eAgInSc1d1	Belenův
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Zapalovaly	zapalovat	k5eAaImAgFnP	zapalovat
se	se	k3xPyFc4	se
velké	velká	k1gFnSc2	velká
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
oheň	oheň	k1gInSc4	oheň
symbolicky	symbolicky	k6eAd1	symbolicky
očistil	očistit	k5eAaPmAgMnS	očistit
lidi	člověk	k1gMnPc4	člověk
i	i	k8xC	i
zvířata	zvíře	k1gNnPc4	zvíře
od	od	k7c2	od
neduhů	neduh	k1gInPc2	neduh
a	a	k8xC	a
nečistých	čistý	k2eNgFnPc2d1	nečistá
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nashromáždily	nashromáždit	k5eAaPmAgFnP	nashromáždit
v	v	k7c6	v
temném	temný	k2eAgNnSc6d1	temné
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Ohně	oheň	k1gInPc1	oheň
na	na	k7c6	na
návrších	návrš	k1gFnPc6	návrš
měly	mít	k5eAaImAgFnP	mít
také	také	k6eAd1	také
ochranný	ochranný	k2eAgInSc4d1	ochranný
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Ohně	oheň	k1gInPc1	oheň
v	v	k7c6	v
domácích	domácí	k2eAgInPc6d1	domácí
krbech	krb	k1gInPc6	krb
se	se	k3xPyFc4	se
uhasily	uhasit	k5eAaPmAgFnP	uhasit
a	a	k8xC	a
zapálily	zapálit	k5eAaPmAgFnP	zapálit
se	se	k3xPyFc4	se
nové	nový	k2eAgFnPc1d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
Beltainu	Beltain	k1gInSc2	Beltain
se	se	k3xPyFc4	se
milenci	milenec	k1gMnPc1	milenec
vydávali	vydávat	k5eAaPmAgMnP	vydávat
do	do	k7c2	do
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
květen	květen	k1gInSc1	květen
potom	potom	k6eAd1	potom
byl	být	k5eAaImAgInS	být
svátkem	svátek	k1gInSc7	svátek
milostných	milostný	k2eAgFnPc2d1	milostná
her	hra	k1gFnPc2	hra
a	a	k8xC	a
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
možná	možná	k9	možná
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
líbání	líbání	k1gNnSc1	líbání
pod	pod	k7c7	pod
rozkvetlým	rozkvetlý	k2eAgInSc7d1	rozkvetlý
stromem	strom	k1gInSc7	strom
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
máje	máj	k1gInSc2	máj
<g/>
.	.	kIx.	.
</s>
<s>
Pálení	pálení	k1gNnSc1	pálení
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
Samhain	Samhaina	k1gFnPc2	Samhaina
</s>
