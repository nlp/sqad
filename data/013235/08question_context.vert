<s>
Eurythmics	Eurythmics	k6eAd1	Eurythmics
bylo	být	k5eAaImAgNnS	být
britské	britský	k2eAgNnSc1d1	Britské
duo	duo	k1gNnSc1	duo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
členy	člen	k1gMnPc7	člen
skupiny	skupina	k1gFnSc2	skupina
byli	být	k5eAaImAgMnP	být
Annie	Annie	k1gFnSc2	Annie
Lennox	Lennox	k1gInSc1	Lennox
a	a	k8xC	a
Dave	Dav	k1gInSc5	Dav
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
.	.	kIx.	.
</s>
