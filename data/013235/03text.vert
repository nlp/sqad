<p>
<s>
Eurythmics	Eurythmics	k6eAd1	Eurythmics
bylo	být	k5eAaImAgNnS	být
britské	britský	k2eAgNnSc1d1	Britské
duo	duo	k1gNnSc1	duo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
členy	člen	k1gMnPc7	člen
skupiny	skupina	k1gFnSc2	skupina
byli	být	k5eAaImAgMnP	být
Annie	Annie	k1gFnSc2	Annie
Lennox	Lennox	k1gInSc1	Lennox
a	a	k8xC	a
Dave	Dav	k1gInSc5	Dav
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gInPc3	jejich
největším	veliký	k2eAgInPc3d3	veliký
hitům	hit	k1gInPc3	hit
patří	patřit	k5eAaImIp3nS	patřit
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Sweet	Sweet	k1gInSc1	Sweet
Dreams	Dreams	k1gInSc1	Dreams
(	(	kIx(	(
<g/>
Are	ar	k1gInSc5	ar
Made	Made	k1gNnPc7	Made
of	of	k?	of
This	Thisa	k1gFnPc2	Thisa
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Who	Who	k1gFnSc1	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
That	That	k1gInSc1	That
Girl	girl	k1gFnSc1	girl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sexcrime	Sexcrim	k1gInSc5	Sexcrim
(	(	kIx(	(
<g/>
Nineteen	Nineteen	k2eAgMnSc1d1	Nineteen
Eighty-Four	Eighty-Four	k1gMnSc1	Eighty-Four
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Sisters	Sisters	k1gInSc1	Sisters
Are	ar	k1gInSc5	ar
Doin	Doina	k1gFnPc2	Doina
<g/>
'	'	kIx"	'
It	It	k1gFnPc2	It
For	forum	k1gNnPc2	forum
Themselves	Themselves	k1gInSc1	Themselves
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
hudební	hudební	k2eAgInSc1d1	hudební
pár	pár	k1gInSc1	pár
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c6	na
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
výrazného	výrazný	k2eAgInSc2d1	výrazný
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
uznání	uznání	k1gNnSc4	uznání
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
hudebních	hudební	k2eAgFnPc2d1	hudební
kritik	kritika	k1gFnPc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
několika	několik	k4yIc2	několik
hudebních	hudební	k2eAgNnPc2d1	hudební
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
několik	několik	k4yIc4	několik
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
koncertních	koncertní	k2eAgNnPc2d1	koncertní
vystoupení	vystoupení	k1gNnPc2	vystoupení
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
prodejem	prodej	k1gInSc7	prodej
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
75	[number]	k4	75
milionů	milion	k4xCgInPc2	milion
hudebních	hudební	k2eAgInPc2d1	hudební
nosičů	nosič	k1gInPc2	nosič
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejprodávanější	prodávaný	k2eAgFnPc4d3	nejprodávanější
hudební	hudební	k2eAgFnPc4d1	hudební
a	a	k8xC	a
pěvecké	pěvecký	k2eAgFnPc4d1	pěvecká
dvojice	dvojice	k1gFnPc4	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
popové	popový	k2eAgFnPc1d1	popová
nahrávky	nahrávka	k1gFnPc1	nahrávka
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgMnPc4d1	charakteristický
dynamickým	dynamický	k2eAgInSc7d1	dynamický
altovým	altový	k2eAgInSc7d1	altový
zpěvem	zpěv	k1gInSc7	zpěv
Annie	Annie	k1gFnSc2	Annie
Lenox	Lenox	k1gInSc1	Lenox
ve	v	k7c6	v
skladbách	skladba	k1gFnPc6	skladba
z	z	k7c2	z
inovativní	inovativní	k2eAgFnSc2d1	inovativní
produkce	produkce	k1gFnSc2	produkce
Dave	Dav	k1gInSc5	Dav
Stewarta	Stewart	k1gMnSc2	Stewart
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Annie	Annie	k1gFnSc1	Annie
Lenox	Lenox	k1gInSc1	Lenox
a	a	k8xC	a
Dave	Dav	k1gInSc5	Dav
Stewart	Stewart	k1gMnSc1	Stewart
začali	začít	k5eAaPmAgMnP	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
v	v	k7c6	v
punk	punk	k1gMnSc1	punk
rockové	rockový	k2eAgFnSc3d1	rocková
kapele	kapela	k1gFnSc3	kapela
The	The	k1gFnSc2	The
Catch	Catcha	k1gFnPc2	Catcha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
jednoho	jeden	k4xCgInSc2	jeden
singlu	singl	k1gInSc2	singl
změnila	změnit	k5eAaPmAgFnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
The	The	k1gFnSc4	The
Tourists	Touristsa	k1gFnPc2	Touristsa
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Tourists	Tourists	k1gInSc1	Tourists
se	se	k3xPyFc4	se
nestali	stát	k5eNaPmAgMnP	stát
nijak	nijak	k6eAd1	nijak
výrazněji	výrazně	k6eAd2	výrazně
úspěšným	úspěšný	k2eAgNnSc7d1	úspěšné
seskupením	seskupení	k1gNnSc7	seskupení
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Annie	Annie	k1gFnSc1	Annie
a	a	k8xC	a
Dave	Dav	k1gInSc5	Dav
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
spolu	spolu	k6eAd1	spolu
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
chodili	chodit	k5eAaImAgMnP	chodit
<g/>
,	,	kIx,	,
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
kombinací	kombinace	k1gFnSc7	kombinace
popové	popový	k2eAgFnSc2d1	popová
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
Eurythmics	Eurythmics	k1gInSc4	Eurythmics
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
tanečního	taneční	k2eAgInSc2d1	taneční
stylu	styl	k1gInSc2	styl
nazvaného	nazvaný	k2eAgInSc2d1	nazvaný
"	"	kIx"	"
<g/>
Eurythmy	Eurythm	k1gInPc4	Eurythm
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Annie	Annie	k1gFnSc1	Annie
znala	znát	k5eAaImAgFnS	znát
ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
dvojice	dvojice	k1gFnSc2	dvojice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc1	The
Tourists	Tourists	k1gInSc1	Tourists
odčlenila	odčlenit	k5eAaPmAgFnS	odčlenit
svým	svůj	k3xOyFgNnSc7	svůj
hudebním	hudební	k2eAgNnSc7d1	hudební
směrováním	směrování	k1gNnSc7	směrování
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
podepsat	podepsat	k5eAaPmF	podepsat
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
hudebním	hudební	k2eAgMnSc7d1	hudební
vydavatelem	vydavatel	k1gMnSc7	vydavatel
RCA	RCA	kA	RCA
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgNnSc7	první
albem	album	k1gNnSc7	album
uskupení	uskupení	k1gNnPc2	uskupení
bylo	být	k5eAaImAgNnS	být
In	In	k1gFnSc7	In
the	the	k?	the
Garden	Gardna	k1gFnPc2	Gardna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
směs	směs	k1gFnSc4	směs
psychedelické	psychedelický	k2eAgFnSc2d1	psychedelická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
krautrocku	krautrock	k1gInSc2	krautrock
a	a	k8xC	a
elektropopu	elektropop	k1gInSc2	elektropop
<g/>
.	.	kIx.	.
</s>
<s>
Ohlasy	ohlas	k1gInPc1	ohlas
kritik	kritika	k1gFnPc2	kritika
i	i	k9	i
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
byly	být	k5eAaImAgFnP	být
nevýrazné	výrazný	k2eNgFnPc1d1	nevýrazná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
singly	singl	k1gInPc4	singl
byly	být	k5eAaImAgInP	být
propadáky	propadák	k1gInPc1	propadák
<g/>
,	,	kIx,	,
až	až	k9	až
třetí	třetí	k4xOgInSc1	třetí
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Never	Never	k1gMnSc1	Never
Gonna	Gonno	k1gNnSc2	Gonno
Cry	Cry	k1gMnSc1	Cry
Again	Again	k1gMnSc1	Again
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
britských	britský	k2eAgInPc2d1	britský
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
seskupení	seskupení	k1gNnSc1	seskupení
Eurythmics	Eurythmicsa	k1gFnPc2	Eurythmicsa
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
svým	svůj	k3xOyFgInSc7	svůj
druhým	druhý	k4xOgNnSc7	druhý
albem	album	k1gNnSc7	album
Sweet	Sweet	k1gInSc1	Sweet
Dreams	Dreams	k1gInSc1	Dreams
(	(	kIx(	(
<g/>
Are	ar	k1gInSc5	ar
Made	Made	k1gNnPc7	Made
of	of	k?	of
This	Thisa	k1gFnPc2	Thisa
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
a	a	k8xC	a
především	především	k9	především
jeho	jeho	k3xOp3gFnSc7	jeho
titulní	titulní	k2eAgFnSc7d1	titulní
skladbou	skladba	k1gFnSc7	skladba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
vrcholy	vrchol	k1gInPc4	vrchol
britských	britský	k2eAgInPc2d1	britský
a	a	k8xC	a
potom	potom	k6eAd1	potom
i	i	k9	i
amerických	americký	k2eAgFnPc2d1	americká
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Popularitu	popularita	k1gFnSc4	popularita
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
jejich	jejich	k3xOp3gInSc1	jejich
předešlý	předešlý	k2eAgInSc1d1	předešlý
singl	singl	k1gInSc1	singl
a	a	k8xC	a
videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Is	Is	k1gFnPc3	Is
A	a	k9	a
Stranger	Stranger	k1gInSc1	Stranger
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Annie	Annie	k1gFnSc1	Annie
Lennox	Lennox	k1gInSc4	Lennox
tváří	tvář	k1gFnSc7	tvář
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
obálce	obálka	k1gFnSc6	obálka
časopisu	časopis	k1gInSc2	časopis
Rolling	Rolling	k1gInSc4	Rolling
Stone	ston	k1gInSc5	ston
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
album	album	k1gNnSc4	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
s	s	k7c7	s
názvem	název	k1gInSc7	název
Touch	Toucha	k1gFnPc2	Toucha
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgMnPc3	první
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
první	první	k4xOgNnPc4	první
místa	místo	k1gNnPc4	místo
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
byly	být	k5eAaImAgFnP	být
i	i	k9	i
tři	tři	k4xCgInPc4	tři
singly	singl	k1gInPc4	singl
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Who	Who	k1gFnSc1	Who
'	'	kIx"	'
<g/>
s	s	k7c7	s
That	That	k1gInSc1	That
Girl	girl	k1gFnSc4	girl
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Right	Right	k1gInSc1	Right
by	by	kYmCp3nS	by
Your	Your	k1gInSc4	Your
Side	Sid	k1gFnSc2	Sid
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Here	Here	k1gFnSc1	Here
Comes	Comes	k1gMnSc1	Comes
the	the	k?	the
Rain	Rain	k1gMnSc1	Rain
Again	Again	k1gMnSc1	Again
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
In	In	k?	In
the	the	k?	the
Garden	Gardna	k1gFnPc2	Gardna
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sweet	Sweet	k1gInSc1	Sweet
Dreams	Dreams	k1gInSc1	Dreams
(	(	kIx(	(
<g/>
Are	ar	k1gInSc5	ar
Made	Made	k1gNnPc7	Made
of	of	k?	of
This	Thisa	k1gFnPc2	Thisa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Touch	Touch	k1gInSc1	Touch
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
For	forum	k1gNnPc2	forum
the	the	k?	the
Love	lov	k1gInSc5	lov
of	of	k?	of
Big	Big	k1gFnPc4	Big
Brother	Brother	kA	Brother
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Be	Be	k?	Be
Yourself	Yourself	k1gMnSc1	Yourself
Tonight	Tonight	k1gMnSc1	Tonight
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Revenge	Revenge	k1gFnSc1	Revenge
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Savage	Savage	k1gFnSc1	Savage
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
We	We	k?	We
Too	Too	k1gFnSc1	Too
Are	ar	k1gInSc5	ar
One	One	k1gMnSc6	One
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Peace	Peace	k1gFnSc1	Peace
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eurythmics	Eurythmicsa	k1gFnPc2	Eurythmicsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
