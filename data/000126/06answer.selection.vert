<s>
Největší	veliký	k2eAgMnSc1d3	veliký
a	a	k8xC	a
nejznámější	známý	k2eAgMnSc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Podkova	podkova	k1gFnSc1	podkova
(	(	kIx(	(
<g/>
Horseshoe	Horseshoe	k1gNnSc1	Horseshoe
Falls	Fallsa	k1gFnPc2	Fallsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
52	[number]	k4	52
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
