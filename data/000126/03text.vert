<s>
Niagarské	niagarský	k2eAgInPc1d1	niagarský
vodopády	vodopád	k1gInPc1	vodopád
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Niagara	Niagara	k1gFnSc1	Niagara
Falls	Fallsa	k1gFnPc2	Fallsa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc4d1	souhrnné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc4	tři
sousedící	sousedící	k2eAgInPc4d1	sousedící
vodopády	vodopád	k1gInPc4	vodopád
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Niagara	Niagara	k1gFnSc1	Niagara
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytéká	vytékat	k5eAaImIp3nS	vytékat
z	z	k7c2	z
Erijského	Erijský	k2eAgNnSc2d1	Erijské
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
a	a	k8xC	a
nejznámější	známý	k2eAgMnSc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Podkova	podkova	k1gFnSc1	podkova
(	(	kIx(	(
<g/>
Horseshoe	Horseshoe	k1gNnSc1	Horseshoe
Falls	Fallsa	k1gFnPc2	Fallsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
52	[number]	k4	52
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
Podkovy	podkova	k1gFnSc2	podkova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
kanadské	kanadský	k2eAgFnSc2d1	kanadská
provincie	provincie	k1gFnSc2	provincie
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
<g/>
,	,	kIx,	,
menší	malý	k2eAgInSc1d2	menší
Americký	americký	k2eAgInSc1d1	americký
vodopád	vodopád	k1gInSc1	vodopád
(	(	kIx(	(
<g/>
American	American	k1gInSc1	American
Falls	Falls	k1gInSc1	Falls
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgInSc4d3	nejmenší
Nevěstin	nevěstin	k2eAgInSc4d1	nevěstin
závoj	závoj	k1gInSc4	závoj
(	(	kIx(	(
<g/>
Bridal	Bridal	k1gFnSc1	Bridal
Veil	Veil	k1gMnSc1	Veil
Falls	Falls	k1gInSc1	Falls
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
celé	celý	k2eAgNnSc1d1	celé
na	na	k7c6	na
území	území	k1gNnSc6	území
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hlavní	hlavní	k2eAgFnSc2d1	hlavní
turistické	turistický	k2eAgFnSc2d1	turistická
sezóny	sezóna	k1gFnSc2	sezóna
vodopády	vodopád	k1gInPc1	vodopád
protéká	protékat	k5eAaImIp3nS	protékat
2	[number]	k4	2
800	[number]	k4	800
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
90	[number]	k4	90
%	%	kIx~	%
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
přes	přes	k7c4	přes
Podkovu	podkova	k1gFnSc4	podkova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Niagara	Niagara	k1gFnSc1	Niagara
a	a	k8xC	a
Velká	velká	k1gFnSc1	velká
jezera	jezero	k1gNnSc2	jezero
jsou	být	k5eAaImIp3nP	být
dědictvím	dědictví	k1gNnSc7	dědictví
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
18	[number]	k4	18
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
bylo	být	k5eAaImAgNnS	být
jižní	jižní	k2eAgNnSc4d1	jižní
Ontario	Ontario	k1gNnSc4	Ontario
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
km	km	kA	km
tlustou	tlustý	k2eAgFnSc7d1	tlustá
vrstvou	vrstva	k1gFnSc7	vrstva
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
led	led	k1gInSc1	led
postupně	postupně	k6eAd1	postupně
roztával	roztávat	k5eAaImAgInS	roztávat
<g/>
,	,	kIx,	,
uvolňovalo	uvolňovat	k5eAaImAgNnS	uvolňovat
se	se	k3xPyFc4	se
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
usazovala	usazovat	k5eAaImAgFnS	usazovat
v	v	k7c6	v
pánvi	pánev	k1gFnSc6	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgInPc1d1	samotný
Niagarské	niagarský	k2eAgInPc1d1	niagarský
vodopády	vodopád	k1gInPc1	vodopád
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
12	[number]	k4	12
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nacházely	nacházet	k5eAaImAgInP	nacházet
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
o	o	k7c6	o
asi	asi	k9	asi
11	[number]	k4	11
km	km	kA	km
níže	nízce	k6eAd2	nízce
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
řeky	řeka	k1gFnSc2	řeka
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Vodopády	vodopád	k1gInPc1	vodopád
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
posouvaly	posouvat	k5eAaImAgFnP	posouvat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
1	[number]	k4	1
m	m	kA	m
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
<g/>
,	,	kIx,	,
současné	současný	k2eAgNnSc1d1	současné
tempo	tempo	k1gNnSc1	tempo
eroze	eroze	k1gFnSc2	eroze
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
cm	cm	kA	cm
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ode	ode	k7c2	ode
dneška	dnešek	k1gInSc2	dnešek
za	za	k7c4	za
50	[number]	k4	50
000	[number]	k4	000
let	léto	k1gNnPc2	léto
vodopády	vodopád	k1gInPc1	vodopád
překonají	překonat	k5eAaPmIp3nP	překonat
zbývajících	zbývající	k2eAgInPc2d1	zbývající
32	[number]	k4	32
km	km	kA	km
k	k	k7c3	k
Erijskému	Erijský	k2eAgNnSc3d1	Erijské
jezeru	jezero	k1gNnSc3	jezero
a	a	k8xC	a
přestanou	přestat	k5eAaPmIp3nP	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgFnPc1d1	různá
teorie	teorie	k1gFnPc1	teorie
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
názvu	název	k1gInSc2	název
Niagara	Niagara	k1gFnSc1	Niagara
<g/>
,	,	kIx,	,
jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jméno	jméno	k1gNnSc4	jméno
indiánského	indiánský	k2eAgInSc2d1	indiánský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
přišli	přijít	k5eAaPmAgMnP	přijít
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgInPc1d1	samotný
vodopády	vodopád	k1gInPc1	vodopád
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
popsány	popsat	k5eAaPmNgInP	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1677	[number]	k4	1677
belgickým	belgický	k2eAgMnSc7d1	belgický
misionářem	misionář	k1gMnSc7	misionář
Louisem	Louis	k1gMnSc7	Louis
Hennepinem	Hennepin	k1gMnSc7	Hennepin
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vodopády	vodopád	k1gInPc7	vodopád
staly	stát	k5eAaPmAgFnP	stát
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
bratr	bratr	k1gMnSc1	bratr
Napoleona	Napoleon	k1gMnSc2	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
Jérôme	Jérôm	k1gMnSc5	Jérôm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1848	[number]	k4	1848
vodopády	vodopád	k1gInPc1	vodopád
na	na	k7c4	na
40	[number]	k4	40
hodin	hodina	k1gFnPc2	hodina
vyschly	vyschnout	k5eAaPmAgFnP	vyschnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
proud	proud	k1gInSc1	proud
zablokoval	zablokovat	k5eAaPmAgInS	zablokovat
led	led	k1gInSc4	led
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
první	první	k4xOgInSc1	první
most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Roeblingovým	Roeblingový	k2eAgInSc7d1	Roeblingový
visutým	visutý	k2eAgInSc7d1	visutý
mostem	most	k1gInSc7	most
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
visutým	visutý	k2eAgInSc7d1	visutý
ocelovým	ocelový	k2eAgInSc7d1	ocelový
mostem	most	k1gInSc7	most
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yRgInSc4	který
dodnes	dodnes	k6eAd1	dodnes
jezdí	jezdit	k5eAaImIp3nP	jezdit
vlaky	vlak	k1gInPc1	vlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
přibyl	přibýt	k5eAaPmAgInS	přibýt
další	další	k2eAgInSc1d1	další
most	most	k1gInSc1	most
spojující	spojující	k2eAgFnSc4d1	spojující
Kanadu	Kanada	k1gFnSc4	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Whirlpool	Whirlpool	k1gInSc4	Whirlpool
Rapids	Rapidsa	k1gFnPc2	Rapidsa
Bridge	Bridg	k1gInSc2	Bridg
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
Rainbow	Rainbow	k1gFnSc7	Rainbow
Bridge	Bridge	k1gFnPc2	Bridge
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
opět	opět	k6eAd1	opět
začali	začít	k5eAaPmAgMnP	začít
hrnout	hrnout	k5eAaImF	hrnout
turisté	turist	k1gMnPc1	turist
<g/>
;	;	kIx,	;
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
snaha	snaha	k1gFnSc1	snaha
využít	využít	k5eAaPmF	využít
vodní	vodní	k2eAgFnSc4d1	vodní
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
regulovat	regulovat	k5eAaImF	regulovat
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
krásu	krása	k1gFnSc4	krása
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Rozpoznání	rozpoznání	k1gNnSc1	rozpoznání
potenciálu	potenciál	k1gInSc2	potenciál
Niagarských	niagarský	k2eAgInPc2d1	niagarský
vodopádů	vodopád	k1gInPc2	vodopád
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
energie	energie	k1gFnSc2	energie
sahá	sahat	k5eAaImIp3nS	sahat
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
využil	využít	k5eAaPmAgMnS	využít
sílu	síla	k1gFnSc4	síla
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Daniel	Daniel	k1gMnSc1	Daniel
Joncaire	Joncair	k1gInSc5	Joncair
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1759	[number]	k4	1759
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
malý	malý	k2eAgInSc1d1	malý
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
pohánění	pohánění	k1gNnSc3	pohánění
jeho	jeho	k3xOp3gFnSc2	jeho
pily	pila	k1gFnSc2	pila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jacoba	Jacoba	k1gFnSc1	Jacoba
F.	F.	kA	F.
Schoellkopfa	Schoellkopf	k1gMnSc2	Schoellkopf
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
první	první	k4xOgFnSc1	první
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
na	na	k7c6	na
Niagaře	Niagara	k1gFnSc6	Niagara
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dodávala	dodávat	k5eAaImAgFnS	dodávat
elektřinu	elektřina	k1gFnSc4	elektřina
vodním	vodní	k2eAgInPc3d1	vodní
mlýnům	mlýn	k1gInPc3	mlýn
a	a	k8xC	a
pouličnímu	pouliční	k2eAgNnSc3d1	pouliční
osvětlení	osvětlení	k1gNnSc3	osvětlení
několika	několik	k4yIc3	několik
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
u	u	k7c2	u
vodopádů	vodopád	k1gInPc2	vodopád
postavil	postavit	k5eAaPmAgMnS	postavit
první	první	k4xOgFnSc4	první
významnou	významný	k2eAgFnSc4d1	významná
hydroelektrárnu	hydroelektrárna	k1gFnSc4	hydroelektrárna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
straně	strana	k1gFnSc6	strana
připomíná	připomínat	k5eAaImIp3nS	připomínat
jeho	jeho	k3xOp3gFnSc1	jeho
bronzová	bronzový	k2eAgFnSc1d1	bronzová
socha	socha	k1gFnSc1	socha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
začaly	začít	k5eAaPmAgInP	začít
budovat	budovat	k5eAaImF	budovat
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
vodní	vodní	k2eAgFnSc4d1	vodní
elektrárnu	elektrárna	k1gFnSc4	elektrárna
Roberta	Robert	k1gMnSc2	Robert
Mosese	Mosese	k1gFnSc2	Mosese
se	se	k3xPyFc4	se
13	[number]	k4	13
generátory	generátor	k1gInPc7	generátor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
spuštěna	spustit	k5eAaPmNgFnS	spustit
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
vodní	vodní	k2eAgFnSc4d1	vodní
elektrárnu	elektrárna	k1gFnSc4	elektrárna
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
elektřiny	elektřina	k1gFnSc2	elektřina
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
musí	muset	k5eAaImIp3nP	muset
ve	v	k7c6	v
dne	den	k1gInSc2	den
během	během	k7c2	během
hlavní	hlavní	k2eAgFnSc2d1	hlavní
turistické	turistický	k2eAgFnSc2d1	turistická
sezóny	sezóna	k1gFnSc2	sezóna
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
vodopády	vodopád	k1gInPc1	vodopád
protékat	protékat	k5eAaImF	protékat
2	[number]	k4	2
800	[number]	k4	800
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
či	či	k8xC	či
mimo	mimo	k7c4	mimo
turistickou	turistický	k2eAgFnSc4d1	turistická
sezónu	sezóna	k1gFnSc4	sezóna
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgInSc1d1	minimální
průtok	průtok	k1gInSc1	průtok
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c4	na
1	[number]	k4	1
400	[number]	k4	400
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
elektrárny	elektrárna	k1gFnPc1	elektrárna
na	na	k7c6	na
Niagaře	Niagara	k1gFnSc6	Niagara
mohou	moct	k5eAaImIp3nP	moct
dohromady	dohromady	k6eAd1	dohromady
generovat	generovat	k5eAaImF	generovat
až	až	k9	až
5	[number]	k4	5
GW	GW	kA	GW
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vodopády	vodopád	k1gInPc1	vodopád
budou	být	k5eAaImBp3nP	být
určeny	určit	k5eAaPmNgInP	určit
výhradně	výhradně	k6eAd1	výhradně
k	k	k7c3	k
průmyslovému	průmyslový	k2eAgNnSc3d1	průmyslové
využití	využití	k1gNnSc3	využití
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
ochranu	ochrana	k1gFnSc4	ochrana
posléze	posléze	k6eAd1	posléze
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
hnutí	hnutí	k1gNnSc1	hnutí
Free	Free	k1gNnSc7	Free
Niagara	Niagara	k1gFnSc1	Niagara
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
vytvoření	vytvoření	k1gNnSc1	vytvoření
prvního	první	k4xOgNnSc2	první
chráněného	chráněný	k2eAgNnSc2d1	chráněné
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
-	-	kIx~	-
Niagara	Niagara	k1gFnSc1	Niagara
Falls	Fallsa	k1gFnPc2	Fallsa
State	status	k1gInSc5	status
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
chráněna	chránit	k5eAaImNgFnS	chránit
také	také	k9	také
kanadská	kanadský	k2eAgFnSc1d1	kanadská
strana	strana	k1gFnSc1	strana
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
nejen	nejen	k6eAd1	nejen
ochránit	ochránit	k5eAaPmF	ochránit
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
umožnit	umožnit	k5eAaPmF	umožnit
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
vychutnat	vychutnat	k5eAaPmF	vychutnat
si	se	k3xPyFc3	se
krásů	krás	k1gMnPc2	krás
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ochrana	ochrana	k1gFnSc1	ochrana
vodopádů	vodopád	k1gInPc2	vodopád
týkala	týkat	k5eAaImAgFnS	týkat
zejména	zejména	k9	zejména
regulace	regulace	k1gFnSc1	regulace
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
stanovením	stanovení	k1gNnSc7	stanovení
minimálního	minimální	k2eAgInSc2d1	minimální
průtoku	průtok	k1gInSc2	průtok
a	a	k8xC	a
snahy	snaha	k1gFnSc2	snaha
zpomalit	zpomalit	k5eAaPmF	zpomalit
erozi	eroze	k1gFnSc3	eroze
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
vodopádů	vodopád	k1gInPc2	vodopád
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgNnPc4	dva
města	město	k1gNnPc4	město
po	po	k7c6	po
Niagarských	niagarský	k2eAgInPc6d1	niagarský
vodopádech	vodopád	k1gInPc6	vodopád
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Niagara	Niagara	k1gFnSc1	Niagara
Falls	Fallsa	k1gFnPc2	Fallsa
v	v	k7c6	v
kanadské	kanadský	k2eAgFnSc6d1	kanadská
provincii	provincie	k1gFnSc6	provincie
Ontario	Ontario	k1gNnSc1	Ontario
a	a	k8xC	a
Niagara	Niagara	k1gFnSc1	Niagara
Falls	Fallsa	k1gFnPc2	Fallsa
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
federálním	federální	k2eAgInSc6d1	federální
státě	stát	k1gInSc6	stát
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
turistickou	turistický	k2eAgFnSc4d1	turistická
atrakci	atrakce	k1gFnSc4	atrakce
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vodopádů	vodopád	k1gInPc2	vodopád
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
řada	řada	k1gFnSc1	řada
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
vztahujících	vztahující	k2eAgFnPc2d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
britsko-americké	britskomerický	k2eAgFnSc3d1	britsko-americká
válce	válka	k1gFnSc3	válka
(	(	kIx(	(
<g/>
válce	válka	k1gFnSc3	válka
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
Niagary	Niagara	k1gFnSc2	Niagara
na	na	k7c6	na
kanadské	kanadský	k2eAgFnSc6d1	kanadská
straně	strana	k1gFnSc6	strana
stojí	stát	k5eAaImIp3nS	stát
pevnost	pevnost	k1gFnSc1	pevnost
Fort	Fort	k?	Fort
Erie	Erie	k1gInSc1	Erie
<g/>
,	,	kIx,	,
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
Niagary	Niagara	k1gFnSc2	Niagara
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
Ontario	Ontario	k1gNnSc1	Ontario
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
kanadské	kanadský	k2eAgFnSc6d1	kanadská
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Niagara-on-the-lake	Niagaranheak	k1gInSc2	Niagara-on-the-lak
<g/>
)	)	kIx)	)
pevnost	pevnost	k1gFnSc1	pevnost
Fort	Fort	k?	Fort
George	Georg	k1gMnSc2	Georg
a	a	k8xC	a
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
straně	strana	k1gFnSc6	strana
stojí	stát	k5eAaImIp3nS	stát
Fort	Fort	k?	Fort
Niagara	Niagara	k1gFnSc1	Niagara
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
atrakcí	atrakce	k1gFnSc7	atrakce
u	u	k7c2	u
vodopádů	vodopád	k1gInPc2	vodopád
je	být	k5eAaImIp3nS	být
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
projížďka	projížďka	k1gFnSc1	projížďka
lodí	loď	k1gFnPc2	loď
společnosti	společnost	k1gFnSc2	společnost
Maid	Maid	k1gMnSc1	Maid
of	of	k?	of
the	the	k?	the
Mist	Mist	k1gInSc1	Mist
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
Maid	Maid	k1gInSc1	Maid
of	of	k?	of
the	the	k?	the
Mist	Mistum	k1gNnPc2	Mistum
VI	VI	kA	VI
a	a	k8xC	a
Maid	Maid	k1gInSc1	Maid
of	of	k?	of
the	the	k?	the
Mist	Mistum	k1gNnPc2	Mistum
VII	VII	kA	VII
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
600	[number]	k4	600
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
,	,	kIx,	,
vozí	vozit	k5eAaImIp3nP	vozit
turisty	turist	k1gMnPc4	turist
kolem	kolem	k7c2	kolem
Amerického	americký	k2eAgInSc2d1	americký
vodopádu	vodopád	k1gInSc2	vodopád
a	a	k8xC	a
Nevěstina	nevěstin	k2eAgInSc2d1	nevěstin
závoje	závoj	k1gInSc2	závoj
až	až	k9	až
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
Podkovy	podkova	k1gFnSc2	podkova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
burácející	burácející	k2eAgInPc1d1	burácející
vodopády	vodopád	k1gInPc1	vodopád
a	a	k8xC	a
stříká	stříkat	k5eAaImIp3nS	stříkat
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
vodní	vodní	k2eAgFnSc1d1	vodní
tříšť	tříšť	k1gFnSc1	tříšť
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
loď	loď	k1gFnSc1	loď
Maid	Maida	k1gFnPc2	Maida
of	of	k?	of
the	the	k?	the
Mist	Mist	k1gInSc1	Mist
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
jezdit	jezdit	k5eAaImF	jezdit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
parní	parní	k2eAgFnSc1d1	parní
a	a	k8xC	a
převážela	převážet	k5eAaImAgFnS	převážet
cestující	cestující	k1gFnPc4	cestující
<g/>
,	,	kIx,	,
dostavníky	dostavník	k1gInPc4	dostavník
a	a	k8xC	a
koně	kůň	k1gMnSc4	kůň
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
po	po	k7c6	po
dostavbě	dostavba	k1gFnSc6	dostavba
mostu	most	k1gInSc2	most
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
z	z	k7c2	z
převozu	převoz	k1gInSc2	převoz
staly	stát	k5eAaPmAgFnP	stát
vyhlídkové	vyhlídkový	k2eAgFnPc1d1	vyhlídková
plavby	plavba	k1gFnPc1	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kanadské	kanadský	k2eAgFnSc6d1	kanadská
straně	strana	k1gFnSc6	strana
projížďky	projížďka	k1gFnSc2	projížďka
lodí	loď	k1gFnPc2	loď
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
společnost	společnost	k1gFnSc1	společnost
Hornblower	Hornblower	k1gInSc4	Hornblower
Cruises	Cruises	k1gInSc1	Cruises
&	&	k?	&
Events	Events	k1gInSc1	Events
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
atrakcí	atrakce	k1gFnSc7	atrakce
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
Cave	Cave	k1gFnSc1	Cave
of	of	k?	of
the	the	k?	the
Winds	Winds	k1gInSc1	Winds
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
se	se	k3xPyFc4	se
svezou	svézt	k5eAaPmIp3nP	svézt
výtahem	výtah	k1gInSc7	výtah
dolů	dolů	k6eAd1	dolů
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
za	za	k7c2	za
hlasitého	hlasitý	k2eAgNnSc2d1	hlasité
hučení	hučení	k1gNnSc2	hučení
vody	voda	k1gFnSc2	voda
procházejí	procházet	k5eAaImIp3nP	procházet
pod	pod	k7c7	pod
vodopády	vodopád	k1gInPc7	vodopád
po	po	k7c6	po
dřevěných	dřevěný	k2eAgFnPc6d1	dřevěná
lávkách	lávka	k1gFnPc6	lávka
<g/>
,	,	kIx,	,
schodech	schod	k1gInPc6	schod
a	a	k8xC	a
plošinách	plošina	k1gFnPc6	plošina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
pokaždé	pokaždé	k6eAd1	pokaždé
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
a	a	k8xC	a
každé	každý	k3xTgNnSc4	každý
jaro	jaro	k1gNnSc4	jaro
znovu	znovu	k6eAd1	znovu
staví	stavit	k5eAaPmIp3nS	stavit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
tam	tam	k6eAd1	tam
panuje	panovat	k5eAaImIp3nS	panovat
extrémní	extrémní	k2eAgNnSc1d1	extrémní
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
Nevěstinu	nevěstin	k2eAgInSc3d1	nevěstin
závoji	závoj	k1gInSc3	závoj
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
plošin	plošina	k1gFnPc2	plošina
<g/>
,	,	kIx,	,
postavena	postaven	k2eAgFnSc1d1	postavena
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nechat	nechat	k5eAaPmF	nechat
se	se	k3xPyFc4	se
osvěžit	osvěžit	k5eAaPmF	osvěžit
sprchou	sprcha	k1gFnSc7	sprcha
z	z	k7c2	z
vodopádu	vodopád	k1gInSc2	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kanadské	kanadský	k2eAgFnSc6d1	kanadská
straně	strana	k1gFnSc6	strana
mohou	moct	k5eAaImIp3nP	moct
turisté	turist	k1gMnPc1	turist
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
podobnou	podobný	k2eAgFnSc4d1	podobná
atrakci	atrakce	k1gFnSc4	atrakce
Journey	Journea	k1gFnSc2	Journea
Behind	Behind	k1gInSc1	Behind
the	the	k?	the
Falls	Falls	k1gInSc1	Falls
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gInPc4	on
zavede	zavést	k5eAaPmIp3nS	zavést
na	na	k7c4	na
vyhlídkovou	vyhlídkový	k2eAgFnSc4d1	vyhlídková
plošinu	plošina	k1gFnSc4	plošina
blízko	blízko	k6eAd1	blízko
úpatí	úpatí	k1gNnSc3	úpatí
Podkovy	podkova	k1gFnSc2	podkova
a	a	k8xC	a
do	do	k7c2	do
tunelů	tunel	k1gInPc2	tunel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
otevřenými	otevřený	k2eAgInPc7d1	otevřený
portály	portál	k1gInPc7	portál
pozorovat	pozorovat	k5eAaImF	pozorovat
padající	padající	k2eAgFnSc4d1	padající
masu	masa	k1gFnSc4	masa
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
této	tento	k3xDgFnSc2	tento
atrakce	atrakce	k1gFnSc2	atrakce
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
postaveno	postaven	k2eAgNnSc4d1	postaveno
první	první	k4xOgNnSc4	první
schodiště	schodiště	k1gNnSc4	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
první	první	k4xOgInSc1	první
výtah	výtah	k1gInSc1	výtah
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
první	první	k4xOgInPc1	první
tunely	tunel	k1gInPc1	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kanadské	kanadský	k2eAgFnSc6d1	kanadská
straně	strana	k1gFnSc6	strana
mohou	moct	k5eAaImIp3nP	moct
turisté	turist	k1gMnPc1	turist
také	také	k9	také
navštívit	navštívit	k5eAaPmF	navštívit
dvě	dva	k4xCgFnPc1	dva
obrovské	obrovský	k2eAgFnPc1d1	obrovská
vyhlídkové	vyhlídkový	k2eAgFnPc1d1	vyhlídková
věže	věž	k1gFnPc1	věž
-	-	kIx~	-
Skylon	Skylon	k1gInSc1	Skylon
Tower	Towero	k1gNnPc2	Towero
a	a	k8xC	a
Minolta	Minolta	kA	Minolta
Tower	Tower	k1gInSc1	Tower
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místním	místní	k2eAgInSc6d1	místní
hřbitově	hřbitov	k1gInSc6	hřbitov
je	být	k5eAaImIp3nS	být
pohřben	pohřben	k2eAgMnSc1d1	pohřben
česko-kanadský	českoanadský	k2eAgMnSc1d1	česko-kanadský
kaskadér	kaskadér	k1gMnSc1	kaskadér
Karel	Karel	k1gMnSc1	Karel
Souček	Souček	k1gMnSc1	Souček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1984	[number]	k4	1984
úspěšně	úspěšně	k6eAd1	úspěšně
proplul	proplout	k5eAaPmAgMnS	proplout
vodopády	vodopád	k1gInPc7	vodopád
v	v	k7c6	v
sudu	sud	k1gInSc6	sud
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
podobný	podobný	k2eAgInSc1d1	podobný
kaskadérský	kaskadérský	k2eAgInSc1d1	kaskadérský
kousek	kousek	k1gInSc1	kousek
zopakovat	zopakovat	k5eAaPmF	zopakovat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
stál	stát	k5eAaImAgInS	stát
život	život	k1gInSc1	život
-	-	kIx~	-
sud	sud	k1gInSc1	sud
se	se	k3xPyFc4	se
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
předčasně	předčasně	k6eAd1	předčasně
a	a	k8xC	a
při	při	k7c6	při
nekontrolovaném	kontrolovaný	k2eNgInSc6d1	nekontrolovaný
pádu	pád	k1gInSc6	pád
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgMnSc1d1	slavný
francouzský	francouzský	k2eAgMnSc1d1	francouzský
provazochodec	provazochodec	k1gMnSc1	provazochodec
a	a	k8xC	a
akrobat	akrobat	k1gMnSc1	akrobat
Blondin	blondin	k2eAgMnSc1d1	blondin
(	(	kIx(	(
<g/>
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Jean	Jean	k1gMnSc1	Jean
François	François	k1gFnPc2	François
Gravelet	Gravelet	k1gInSc1	Gravelet
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
překonal	překonat	k5eAaPmAgMnS	překonat
Niagaru	Niagara	k1gFnSc4	Niagara
po	po	k7c6	po
laně	lano	k1gNnSc6	lano
<g/>
,	,	kIx,	,
asi	asi	k9	asi
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Rainbow	Rainbow	k1gFnSc7	Rainbow
Bridge	Bridge	k1gNnPc2	Bridge
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ne	ne	k9	ne
přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
vodopády	vodopád	k1gInPc7	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Lano	lano	k1gNnSc1	lano
bylo	být	k5eAaImAgNnS	být
přibližně	přibližně	k6eAd1	přibližně
340	[number]	k4	340
m	m	kA	m
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
viselo	viset	k5eAaImAgNnS	viset
50	[number]	k4	50
m	m	kA	m
nad	nad	k7c7	nad
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
kousek	kousek	k1gInSc4	kousek
několikrát	několikrát	k6eAd1	několikrát
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
a	a	k8xC	a
obměnil	obměnit	k5eAaPmAgMnS	obměnit
-	-	kIx~	-
řeku	řeka	k1gFnSc4	řeka
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
přešel	přejít	k5eAaPmAgMnS	přejít
se	s	k7c7	s
zavázanýma	zavázaný	k2eAgNnPc7d1	zavázané
očima	oko	k1gNnPc7	oko
nebo	nebo	k8xC	nebo
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manažerem	manažer	k1gMnSc7	manažer
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
je	být	k5eAaImIp3nS	být
přecházení	přecházení	k1gNnSc4	přecházení
vodopádů	vodopád	k1gInPc2	vodopád
po	po	k7c6	po
laně	lano	k1gNnSc6	lano
ilegální	ilegální	k2eAgMnSc1d1	ilegální
<g/>
.	.	kIx.	.
</s>
<s>
Výjimka	výjimka	k1gFnSc1	výjimka
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
americkému	americký	k2eAgMnSc3d1	americký
provazochodci	provazochodec	k1gMnSc3	provazochodec
Nikovi	Nika	k1gMnSc3	Nika
Wallendovi	Wallend	k1gMnSc3	Wallend
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2012	[number]	k4	2012
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kdy	kdy	k6eAd1	kdy
přešel	přejít	k5eAaPmAgMnS	přejít
po	po	k7c6	po
laně	lano	k1gNnSc6	lano
přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
vodopády	vodopád	k1gInPc7	vodopád
-	-	kIx~	-
Wallenda	Wallend	k1gMnSc2	Wallend
úspěšně	úspěšně	k6eAd1	úspěšně
překonal	překonat	k5eAaPmAgMnS	překonat
550	[number]	k4	550
m	m	kA	m
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
na	na	k7c6	na
laně	lano	k1gNnSc6	lano
visícím	visící	k2eAgNnSc6d1	visící
60	[number]	k4	60
m	m	kA	m
nad	nad	k7c7	nad
hranou	hrana	k1gFnSc7	hrana
Podkovy	podkova	k1gFnSc2	podkova
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
první	první	k4xOgNnSc4	první
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
překonání	překonání	k1gNnSc4	překonání
vodopádů	vodopád	k1gInPc2	vodopád
v	v	k7c6	v
sudu	sud	k1gInSc6	sud
se	s	k7c7	s
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1901	[number]	k4	1901
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
63	[number]	k4	63
<g/>
letá	letý	k2eAgFnSc1d1	letá
učitelka	učitelka	k1gFnSc1	učitelka
z	z	k7c2	z
Michiganu	Michigan	k1gInSc2	Michigan
Annie	Annie	k1gFnSc2	Annie
Edson	Edson	k1gMnSc1	Edson
Taylorová	Taylorová	k1gFnSc1	Taylorová
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
lehce	lehko	k6eAd1	lehko
krvácející	krvácející	k2eAgFnSc4d1	krvácející
ránu	rána	k1gFnSc4	rána
u	u	k7c2	u
ucha	ucho	k1gNnSc2	ucho
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
nic	nic	k3yNnSc1	nic
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ze	z	k7c2	z
sudu	sud	k1gInSc2	sud
vylezla	vylézt	k5eAaPmAgFnS	vylézt
<g/>
,	,	kIx,	,
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tohle	tenhle	k3xDgNnSc1	tenhle
už	už	k9	už
by	by	kYmCp3nS	by
nikdy	nikdy	k6eAd1	nikdy
nikdo	nikdo	k3yNnSc1	nikdo
neměl	mít	k5eNaImAgMnS	mít
zkoušet	zkoušet	k5eAaImF	zkoušet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ještě	ještě	k9	ještě
o	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
Taylorová	Taylorová	k1gFnSc1	Taylorová
vodopády	vodopád	k1gInPc4	vodopád
překonala	překonat	k5eAaPmAgFnS	překonat
kočka	kočka	k1gFnSc1	kočka
jménem	jméno	k1gNnSc7	jméno
Iagara	Iagara	k1gFnSc1	Iagara
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Taylorová	Taylorová	k1gFnSc1	Taylorová
poslala	poslat	k5eAaPmAgFnS	poslat
dolů	dolů	k6eAd1	dolů
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
sudu	sud	k1gInSc6	sud
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyzkoušela	vyzkoušet	k5eAaPmAgFnS	vyzkoušet
jeho	jeho	k3xOp3gFnSc4	jeho
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
fámám	fáma	k1gFnPc3	fáma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tehdy	tehdy	k6eAd1	tehdy
kolovaly	kolovat	k5eAaImAgFnP	kolovat
<g/>
,	,	kIx,	,
Iagara	Iagar	k1gMnSc4	Iagar
přežila	přežít	k5eAaPmAgFnS	přežít
nezraněna	zraněn	k2eNgFnSc1d1	nezraněna
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dokonce	dokonce	k9	dokonce
pózovala	pózovat	k5eAaImAgFnS	pózovat
s	s	k7c7	s
Taylorovou	Taylorová	k1gFnSc7	Taylorová
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
historického	historický	k2eAgInSc2d1	historický
prvního	první	k4xOgNnSc2	první
proplutí	proplutí	k1gNnSc6	proplutí
vodopádů	vodopád	k1gInPc2	vodopád
v	v	k7c6	v
sudu	sud	k1gInSc6	sud
se	se	k3xPyFc4	se
o	o	k7c4	o
podobný	podobný	k2eAgInSc4d1	podobný
čin	čin	k1gInSc4	čin
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
čtrnáct	čtrnáct	k4xCc1	čtrnáct
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
nejrůznějších	různý	k2eAgNnPc2d3	nejrůznější
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
se	se	k3xPyFc4	se
nestalo	stát	k5eNaPmAgNnS	stát
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
se	se	k3xPyFc4	se
utopili	utopit	k5eAaPmAgMnP	utopit
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vážně	vážně	k6eAd1	vážně
zranili	zranit	k5eAaPmAgMnP	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Přeživší	přeživší	k2eAgFnSc1d1	přeživší
takových	takový	k3xDgInPc2	takový
kaskadérských	kaskadérský	k2eAgInPc2d1	kaskadérský
kousků	kousek	k1gInPc2	kousek
čeká	čekat	k5eAaImIp3nS	čekat
obvinění	obvinění	k1gNnSc1	obvinění
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pokuta	pokuta	k1gFnSc1	pokuta
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
hranice	hranice	k1gFnSc2	hranice
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
pokusy	pokus	k1gInPc1	pokus
překonat	překonat	k5eAaPmF	překonat
vodopády	vodopád	k1gInPc4	vodopád
ilegální	ilegální	k2eAgInPc4d1	ilegální
<g/>
.	.	kIx.	.
</s>
<s>
Iagara	Iagara	k1gFnSc1	Iagara
není	být	k5eNaImIp3nS	být
jediné	jediný	k2eAgNnSc4d1	jediné
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
překonalo	překonat	k5eAaPmAgNnS	překonat
vodopády	vodopád	k1gInPc4	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1930	[number]	k4	1930
se	se	k3xPyFc4	se
o	o	k7c6	o
proplutí	proplutí	k1gNnSc6	proplutí
Podkovou	podkovat	k5eAaPmIp3nP	podkovat
v	v	k7c6	v
sudu	sud	k1gInSc6	sud
pokusil	pokusit	k5eAaPmAgMnS	pokusit
George	George	k1gFnSc4	George
Stathakis	Stathakis	k1gFnSc2	Stathakis
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vzal	vzít	k5eAaPmAgMnS	vzít
svou	svůj	k3xOyFgFnSc4	svůj
150	[number]	k4	150
let	léto	k1gNnPc2	léto
starou	starý	k2eAgFnSc4d1	stará
želvu	želva	k1gFnSc4	želva
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Stathakis	Stathakis	k1gFnSc1	Stathakis
pád	pád	k1gInSc4	pád
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
udusil	udusit	k5eAaPmAgMnS	udusit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
téměř	téměř	k6eAd1	téměř
tunu	tuna	k1gFnSc4	tuna
vážící	vážící	k2eAgInSc1d1	vážící
sud	sud	k1gInSc1	sud
zůstal	zůstat	k5eAaPmAgInS	zůstat
zachycený	zachycený	k2eAgInSc1d1	zachycený
za	za	k7c7	za
vodopádem	vodopád	k1gInSc7	vodopád
a	a	k8xC	a
následujících	následující	k2eAgInPc2d1	následující
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvacet	dvacet	k4xCc4	dvacet
hodin	hodina	k1gFnPc2	hodina
jej	on	k3xPp3gMnSc4	on
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vyprostit	vyprostit	k5eAaPmF	vyprostit
<g/>
.	.	kIx.	.
</s>
<s>
Stathakisově	Stathakisově	k6eAd1	Stathakisově
želvě	želva	k1gFnSc6	želva
jménem	jméno	k1gNnSc7	jméno
Sonny	Sonna	k1gFnSc2	Sonna
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1960	[number]	k4	1960
pád	pád	k1gInSc1	pád
do	do	k7c2	do
Podkovy	podkova	k1gFnSc2	podkova
jako	jako	k8xS	jako
zázrakem	zázrak	k1gInSc7	zázrak
přežil	přežít	k5eAaPmAgMnS	přežít
7	[number]	k4	7
<g/>
letý	letý	k2eAgInSc4d1	letý
Roger	Roger	k1gInSc4	Roger
Woodward	Woodward	k1gMnSc1	Woodward
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
jen	jen	k6eAd1	jen
záchrannou	záchranný	k2eAgFnSc4d1	záchranná
vestu	vesta	k1gFnSc4	vesta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
17	[number]	k4	17
<g/>
letá	letý	k2eAgFnSc1d1	letá
sestra	sestra	k1gFnSc1	sestra
Deanne	Deann	k1gInSc5	Deann
vodopádu	vodopád	k1gInSc3	vodopád
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
unikla	uniknout	k5eAaPmAgFnS	uniknout
-	-	kIx~	-
dva	dva	k4xCgMnPc1	dva
turisté	turist	k1gMnPc1	turist
ji	on	k3xPp3gFnSc4	on
vytáhli	vytáhnout	k5eAaPmAgMnP	vytáhnout
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
pouhých	pouhý	k2eAgInPc2d1	pouhý
6	[number]	k4	6
m	m	kA	m
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
hrany	hrana	k1gFnSc2	hrana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1985	[number]	k4	1985
se	s	k7c7	s
22	[number]	k4	22
<g/>
letý	letý	k2eAgMnSc1d1	letý
Steve	Steve	k1gMnSc1	Steve
Trotter	Trotter	k1gMnSc1	Trotter
<g/>
,	,	kIx,	,
začínající	začínající	k2eAgMnSc1d1	začínající
kaskadér	kaskadér	k1gMnSc1	kaskadér
z	z	k7c2	z
Rhode	Rhodos	k1gInSc5	Rhodos
Islandu	Island	k1gInSc6	Island
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vodopády	vodopád	k1gInPc4	vodopád
překonal	překonat	k5eAaPmAgMnS	překonat
v	v	k7c6	v
sudu	sud	k1gInSc6	sud
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
Trotter	Trotter	k1gInSc1	Trotter
znovu	znovu	k6eAd1	znovu
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proplutí	proplutí	k1gNnSc3	proplutí
vodopády	vodopád	k1gInPc1	vodopád
přežila	přežít	k5eAaPmAgFnS	přežít
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvnímu	první	k4xOgNnSc3	první
překonání	překonání	k1gNnSc3	překonání
vodopádů	vodopád	k1gInPc2	vodopád
ve	v	k7c6	v
dvoumístném	dvoumístný	k2eAgInSc6d1	dvoumístný
sudu	sud	k1gInSc6	sud
Peterem	Peter	k1gMnSc7	Peter
DeBernardim	DeBernardima	k1gFnPc2	DeBernardima
a	a	k8xC	a
Jefferym	Jefferym	k1gInSc4	Jefferym
Jamesem	James	k1gMnSc7	James
Petkovichem	Petkovich	k1gMnSc7	Petkovich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2003	[number]	k4	2003
se	se	k3xPyFc4	se
Kirk	Kirk	k1gMnSc1	Kirk
Jones	Jones	k1gMnSc1	Jones
z	z	k7c2	z
Michiganu	Michigan	k1gInSc2	Michigan
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
známým	známý	k2eAgMnSc7d1	známý
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
přežil	přežít	k5eAaPmAgMnS	přežít
pád	pád	k1gInSc4	pád
do	do	k7c2	do
Podkovy	podkova	k1gFnSc2	podkova
bez	bez	k7c2	bez
vesty	vesta	k1gFnSc2	vesta
či	či	k8xC	či
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
nadnášecího	nadnášecí	k2eAgNnSc2d1	nadnášecí
náčiní	náčiní	k1gNnSc2	náčiní
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
měl	mít	k5eAaImAgMnS	mít
Jones	Jones	k1gMnSc1	Jones
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
52	[number]	k4	52
m	m	kA	m
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
pádu	pád	k1gInSc2	pád
vyvázl	vyváznout	k5eAaPmAgInS	vyváznout
jen	jen	k9	jen
s	s	k7c7	s
otlučenými	otlučený	k2eAgInPc7d1	otlučený
žebry	žebr	k1gInPc7	žebr
<g/>
,	,	kIx,	,
odřeninami	odřenina	k1gFnPc7	odřenina
a	a	k8xC	a
pohmožděninami	pohmožděnina	k1gFnPc7	pohmožděnina
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
pád	pád	k1gInSc4	pád
později	pozdě	k6eAd2	pozdě
přežili	přežít	k5eAaPmAgMnP	přežít
ještě	ještě	k9	ještě
další	další	k2eAgMnPc1d1	další
dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
vodopádů	vodopád	k1gInPc2	vodopád
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
film	film	k1gInSc1	film
Niagara	Niagara	k1gFnSc1	Niagara
s	s	k7c7	s
Marilyn	Marilyn	k1gFnSc7	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
scény	scéna	k1gFnPc1	scéna
na	na	k7c6	na
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
schodech	schod	k1gInPc6	schod
<g/>
,	,	kIx,	,
natáčení	natáčení	k1gNnSc1	natáčení
dole	dole	k6eAd1	dole
u	u	k7c2	u
Niagary	Niagara	k1gFnSc2	Niagara
pod	pod	k7c7	pod
Americkým	americký	k2eAgInSc7d1	americký
vodopádem	vodopád	k1gInSc7	vodopád
a	a	k8xC	a
scény	scéna	k1gFnPc4	scéna
z	z	k7c2	z
Bell	bell	k1gInSc1	bell
Tower	Towero	k1gNnPc2	Towero
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
prvního	první	k4xOgInSc2	první
mostu	most	k1gInSc2	most
pod	pod	k7c7	pod
vodopády	vodopád	k1gInPc7	vodopád
na	na	k7c6	na
kanadské	kanadský	k2eAgFnSc6d1	kanadská
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
extrémních	extrémní	k2eAgInPc2d1	extrémní
mrazů	mráz	k1gInPc2	mráz
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
vodopádů	vodopád	k1gInPc2	vodopád
zamrzla	zamrznout	k5eAaPmAgFnS	zamrznout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vylezl	vylézt	k5eAaPmAgMnS	vylézt
po	po	k7c6	po
zamrzlém	zamrzlý	k2eAgInSc6d1	zamrzlý
okraji	okraj	k1gInSc6	okraj
vodopádu	vodopád	k1gInSc2	vodopád
horolezec	horolezec	k1gMnSc1	horolezec
Will	Will	k1gMnSc1	Will
Gadd	Gadd	k1gMnSc1	Gadd
<g/>
.	.	kIx.	.
</s>
<s>
Kuriózní	kuriózní	k2eAgFnSc1d1	kuriózní
nehoda	nehoda	k1gFnSc1	nehoda
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednomu	jeden	k4xCgNnSc3	jeden
turistovi	turist	k1gMnSc3	turist
na	na	k7c4	na
atrakci	atrakce	k1gFnSc4	atrakce
Cave	Cav	k1gInSc2	Cav
of	of	k?	of
the	the	k?	the
Winds	Winds	k1gInSc1	Winds
-	-	kIx~	-
trefil	trefit	k5eAaPmAgMnS	trefit
jej	on	k3xPp3gMnSc4	on
losos	losos	k1gMnSc1	losos
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nečekaně	nečekaně	k6eAd1	nečekaně
vyletěl	vyletět	k5eAaPmAgMnS	vyletět
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
obešlo	obejít	k5eAaPmAgNnS	obejít
bez	bez	k7c2	bez
zranění	zranění	k1gNnSc2	zranění
a	a	k8xC	a
turista	turista	k1gMnSc1	turista
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
chycenou	chycený	k2eAgFnSc4d1	chycená
trofej	trofej	k1gFnSc4	trofej
vzít	vzít	k5eAaPmF	vzít
domů	domů	k6eAd1	domů
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
