<s>
Miami	Miami	k1gNnSc1	Miami
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Floridy	Florida	k1gFnSc2	Florida
a	a	k8xC	a
druhé	druhý	k4xOgFnSc3	druhý
největší	veliký	k2eAgFnSc3d3	veliký
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
mezi	mezi	k7c7	mezi
bažinami	bažina	k1gFnPc7	bažina
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Everglades	Evergladesa	k1gFnPc2	Evergladesa
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
433	[number]	k4	433
136	[number]	k4	136
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
5,2	[number]	k4	5,2
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnSc4d3	veliký
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
populace	populace	k1gFnSc2	populace
tvoří	tvořit	k5eAaImIp3nP	tvořit
Hispánci	Hispánek	k1gMnPc1	Hispánek
a	a	k8xC	a
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
samotného	samotný	k2eAgNnSc2d1	samotné
je	být	k5eAaImIp3nS	být
143,15	[number]	k4	143,15
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
přes	přes	k7c4	přes
50	[number]	k4	50
km	km	kA	km
<g/>
2	[number]	k4	2
zabírá	zabírat	k5eAaImIp3nS	zabírat
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
centrem	centr	k1gInSc7	centr
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
významu	význam	k1gInSc2	význam
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
zábavy	zábava	k1gFnSc2	zábava
a	a	k8xC	a
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
významný	významný	k2eAgInSc1d1	významný
přístav	přístav	k1gInSc1	přístav
a	a	k8xC	a
letiště	letiště	k1gNnSc1	letiště
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgFnPc3	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
říká	říkat	k5eAaImIp3nS	říkat
Brána	brána	k1gFnSc1	brána
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
koncentrace	koncentrace	k1gFnSc1	koncentrace
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
bank	banka	k1gFnPc2	banka
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
masivnímu	masivní	k2eAgInSc3d1	masivní
růstu	růst	k1gInSc3	růst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
došlo	dojít	k5eAaPmAgNnS	dojít
především	především	k9	především
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
50	[number]	k4	50
letech	let	k1gInPc6	let
díky	díky	k7c3	díky
emigrantům	emigrant	k1gMnPc3	emigrant
z	z	k7c2	z
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
65	[number]	k4	65
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
mezi	mezi	k7c7	mezi
jazyky	jazyk	k1gInPc7	jazyk
převládá	převládat	k5eAaImIp3nS	převládat
španělština	španělština	k1gFnSc1	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgMnPc7	první
obyvateli	obyvatel	k1gMnPc7	obyvatel
oblasti	oblast	k1gFnSc2	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Miami	Miami	k1gNnSc2	Miami
byli	být	k5eAaImAgMnP	být
původní	původní	k2eAgMnPc1d1	původní
Američané	Američan	k1gMnPc1	Američan
již	již	k6eAd1	již
před	před	k7c7	před
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
při	při	k7c6	při
příchodu	příchod	k1gInSc6	příchod
prvních	první	k4xOgInPc2	první
Evropanů	Evropan	k1gMnPc2	Evropan
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgInS	žít
kmen	kmen	k1gInSc1	kmen
Tequestů	Tequest	k1gInPc2	Tequest
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
stálým	stálý	k2eAgMnSc7d1	stálý
osadníkem	osadník	k1gMnSc7	osadník
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Pedro	Pedro	k1gNnSc4	Pedro
Fornells	Fornellsa	k1gFnPc2	Fornellsa
z	z	k7c2	z
Menorky	Menorka	k1gFnSc2	Menorka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zakladatelku	zakladatelka	k1gFnSc4	zakladatelka
Miami	Miami	k1gNnPc2	Miami
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgMnSc4d1	považován
Julia	Julius	k1gMnSc4	Julius
Tuttle	Tuttle	k1gFnSc2	Tuttle
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
plantáže	plantáž	k1gFnPc4	plantáž
a	a	k8xC	a
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obrovskému	obrovský	k2eAgInSc3d1	obrovský
růstu	růst	k1gInSc3	růst
populace	populace	k1gFnSc2	populace
města	město	k1gNnSc2	město
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
aglomerace	aglomerace	k1gFnSc2	aglomerace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Miami	Miami	k1gNnSc2	Miami
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
napravila	napravit	k5eAaPmAgFnS	napravit
až	až	k6eAd1	až
nová	nový	k2eAgFnSc1d1	nová
správa	správa	k1gFnSc1	správa
zvolená	zvolený	k2eAgFnSc1d1	zvolená
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
Evropanem	Evropan	k1gMnSc7	Evropan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spatřil	spatřit	k5eAaPmAgMnS	spatřit
oblast	oblast	k1gFnSc4	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Miami	Miami	k1gNnSc2	Miami
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Španěl	Španěl	k1gMnSc1	Španěl
Juan	Juan	k1gMnSc1	Juan
Ponce	Ponce	k1gMnSc1	Ponce
de	de	k?	de
León	León	k1gMnSc1	León
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
deníku	deník	k1gInSc2	deník
si	se	k3xPyFc3	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
doplul	doplout	k5eAaPmAgMnS	doplout
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
Chequescha	Chequesch	k1gMnSc2	Chequesch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc4	první
dochované	dochovaný	k2eAgNnSc4d1	dochované
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
Miami	Miami	k1gNnSc1	Miami
dostalo	dostat	k5eAaPmAgNnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Pedro	Pedro	k6eAd1	Pedro
Menéndez	Menéndez	k1gInSc1	Menéndez
de	de	k?	de
Avilés	Avilés	k1gInSc1	Avilés
učinil	učinit	k5eAaPmAgMnS	učinit
první	první	k4xOgNnSc4	první
zaznamenané	zaznamenaný	k2eAgNnSc4d1	zaznamenané
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1566	[number]	k4	1566
<g/>
,	,	kIx,	,
když	když	k8xS	když
hledal	hledat	k5eAaImAgInS	hledat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zahájili	zahájit	k5eAaPmAgMnP	zahájit
španělští	španělský	k2eAgMnPc1d1	španělský
vojáci	voják	k1gMnPc1	voják
vedení	vedení	k1gNnSc6	vedení
otcem	otec	k1gMnSc7	otec
Francisco	Francisco	k6eAd1	Francisco
Villarealem	Villareal	k1gInSc7	Villareal
z	z	k7c2	z
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
řádu	řád	k1gInSc2	řád
misijní	misijní	k2eAgFnSc1d1	misijní
činnost	činnost	k1gFnSc1	činnost
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Miami	Miami	k1gNnSc2	Miami
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
najít	najít	k5eAaPmF	najít
jiné	jiný	k2eAgNnSc4d1	jiné
působiště	působiště	k1gNnSc4	působiště
mimo	mimo	k7c4	mimo
Floridu	Florida	k1gFnSc4	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
poslali	poslat	k5eAaPmAgMnP	poslat
další	další	k2eAgFnSc4d1	další
misii	misie	k1gFnSc4	misie
do	do	k7c2	do
Biscayne	Biscayn	k1gInSc5	Biscayn
Bay	Bay	k1gMnSc6	Bay
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1743	[number]	k4	1743
a	a	k8xC	a
postavili	postavit	k5eAaPmAgMnP	postavit
zde	zde	k6eAd1	zde
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Misionáři	misionář	k1gMnPc1	misionář
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
trvalé	trvalý	k2eAgNnSc4d1	trvalé
osídlení	osídlení	k1gNnSc4	osídlení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
zamítnut	zamítnout	k5eAaPmNgInS	zamítnout
jako	jako	k8xS	jako
nepraktický	praktický	k2eNgInSc1d1	nepraktický
a	a	k8xC	a
misie	misie	k1gFnSc1	misie
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
stažena	stažen	k2eAgFnSc1d1	stažena
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
stálým	stálý	k2eAgMnSc7d1	stálý
osadníkem	osadník	k1gMnSc7	osadník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Miami	Miami	k1gNnSc2	Miami
byl	být	k5eAaImAgInS	být
Pedro	Pedro	k1gNnSc4	Pedro
Fornells	Fornellsa	k1gFnPc2	Fornellsa
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Menorca	Menorcus	k1gMnSc2	Menorcus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Key	Key	k1gFnSc2	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
přišel	přijít	k5eAaPmAgInS	přijít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Fornells	Fornells	k6eAd1	Fornells
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
přítomnost	přítomnost	k1gFnSc4	přítomnost
squatterů	squatter	k1gMnPc2	squatter
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Cape	capat	k5eAaImIp3nS	capat
Florida	Florida	k1gFnSc1	Florida
Settlement	settlement	k1gInSc4	settlement
americký	americký	k2eAgMnSc1d1	americký
maršál	maršál	k1gMnSc1	maršál
Waters	Watersa	k1gFnPc2	Watersa
Smith	Smith	k1gMnSc1	Smith
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
squatterům	squatter	k1gMnPc3	squatter
zisk	zisk	k1gInSc4	zisk
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
obývali	obývat	k5eAaImAgMnP	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Key	Key	k1gFnSc2	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
postaven	postaven	k2eAgInSc4d1	postaven
maják	maják	k1gInSc4	maják
Cape	capat	k5eAaImIp3nS	capat
Florida	Florida	k1gFnSc1	Florida
Light	Light	k1gInSc1	Light
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
prvním	první	k4xOgMnSc7	první
strážcem	strážce	k1gMnSc7	strážce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
John	John	k1gMnSc1	John
Dubose	Dubosa	k1gFnSc3	Dubosa
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
druhou	druhý	k4xOgFnSc7	druhý
seminolskou	seminolský	k2eAgFnSc7d1	seminolský
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
prudké	prudký	k2eAgNnSc4d1	prudké
vylidnění	vylidnění	k1gNnSc4	vylidnění
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
se	se	k3xPyFc4	se
Miami	Miami	k1gNnSc2	Miami
stalo	stát	k5eAaPmAgNnS	stát
správním	správní	k2eAgNnSc7d1	správní
střediskem	středisko	k1gNnSc7	středisko
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
seminolská	seminolský	k2eAgFnSc1d1	seminolský
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1855	[number]	k4	1855
až	až	k9	až
1858	[number]	k4	1858
zpomalila	zpomalit	k5eAaPmAgFnS	zpomalit
osidlování	osidlování	k1gNnSc4	osidlování
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Floridy	Florida	k1gFnSc2	Florida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
koupila	koupit	k5eAaPmAgFnS	koupit
Julia	Julius	k1gMnSc4	Julius
Tuttle	Tuttle	k1gFnSc1	Tuttle
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
jako	jako	k8xC	jako
Matka	matka	k1gFnSc1	matka
Miami	Miami	k1gNnSc2	Miami
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Mother	Mothra	k1gFnPc2	Mothra
of	of	k?	of
Miami	Miami	k1gNnSc6	Miami
<g/>
)	)	kIx)	)
velkou	velký	k2eAgFnSc4d1	velká
citrusovou	citrusový	k2eAgFnSc4d1	citrusová
plantáž	plantáž	k1gFnSc4	plantáž
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1895	[number]	k4	1895
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
Floridu	Florida	k1gFnSc4	Florida
veliký	veliký	k2eAgInSc4d1	veliký
mráz	mráz	k1gInSc4	mráz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zničil	zničit	k5eAaPmAgMnS	zničit
všechnu	všechen	k3xTgFnSc4	všechen
úrodu	úroda	k1gFnSc4	úroda
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
však	však	k9	však
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nedotčené	dotčený	k2eNgNnSc1d1	nedotčené
a	a	k8xC	a
citrusy	citrus	k1gInPc1	citrus
Julie	Julie	k1gFnSc2	Julie
Tuttleové	Tuttleová	k1gFnSc2	Tuttleová
byly	být	k5eAaImAgInP	být
jediné	jediný	k2eAgInPc1d1	jediný
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
multimilionáře	multimilionář	k1gMnSc4	multimilionář
Henryho	Henry	k1gMnSc4	Henry
Flaglera	Flagler	k1gMnSc4	Flagler
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
až	až	k9	až
do	do	k7c2	do
Miami	Miami	k1gNnSc2	Miami
a	a	k8xC	a
postavil	postavit	k5eAaPmAgInS	postavit
zde	zde	k6eAd1	zde
hotel	hotel	k1gInSc1	hotel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
budováním	budování	k1gNnSc7	budování
železnice	železnice	k1gFnSc2	železnice
se	se	k3xPyFc4	se
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1896	[number]	k4	1896
se	se	k3xPyFc4	se
Miami	Miami	k1gNnSc2	Miami
oficiálně	oficiálně	k6eAd1	oficiálně
stalo	stát	k5eAaPmAgNnS	stát
městem	město	k1gNnSc7	město
s	s	k7c7	s
502	[number]	k4	502
voliči	volič	k1gMnPc7	volič
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
stovky	stovka	k1gFnSc2	stovka
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
černých	černý	k2eAgMnPc2d1	černý
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
Miami	Miami	k1gNnSc2	Miami
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
zde	zde	k6eAd1	zde
přibylo	přibýt	k5eAaPmAgNnS	přibýt
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zvýšením	zvýšení	k1gNnSc7	zvýšení
populace	populace	k1gFnSc2	populace
samozřejmě	samozřejmě	k6eAd1	samozřejmě
přišla	přijít	k5eAaPmAgFnS	přijít
potřeba	potřeba	k1gFnSc1	potřeba
dalšího	další	k2eAgNnSc2d1	další
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
úřady	úřad	k1gInPc1	úřad
Miami	Miami	k1gNnSc2	Miami
povolily	povolit	k5eAaPmAgInP	povolit
hazardní	hazardní	k2eAgMnPc4d1	hazardní
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
laxní	laxní	k2eAgInPc1d1	laxní
v	v	k7c6	v
regulaci	regulace	k1gFnSc6	regulace
zákazů	zákaz	k1gInPc2	zákaz
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
sem	sem	k6eAd1	sem
migrovalo	migrovat	k5eAaImAgNnS	migrovat
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
populace	populace	k1gFnSc1	populace
Miami	Miami	k1gNnSc2	Miami
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1925	[number]	k4	1925
byly	být	k5eAaImAgFnP	být
blízké	blízký	k2eAgFnPc1d1	blízká
oblasti	oblast	k1gFnPc1	oblast
Lemon	lemon	k1gInSc4	lemon
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
Coconut	Coconut	k1gMnSc1	Coconut
Grove	Groev	k1gFnSc2	Groev
a	a	k8xC	a
Allapattah	Allapattaha	k1gFnPc2	Allapattaha
připojeny	připojit	k5eAaPmNgInP	připojit
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tak	tak	k6eAd1	tak
Větší	veliký	k2eAgNnSc1d2	veliký
Miami	Miami	k1gNnSc1	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
se	se	k3xPyFc4	se
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
kvůli	kvůli	k7c3	kvůli
průtahům	průtah	k1gInPc3	průtah
při	při	k7c6	při
stavbách	stavba	k1gFnPc6	stavba
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
také	také	k9	také
přetíženým	přetížený	k2eAgInSc7d1	přetížený
dopravním	dopravní	k2eAgInSc7d1	dopravní
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
byl	být	k5eAaImAgInS	být
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
zablokován	zablokovat	k5eAaPmNgInS	zablokovat
dánskou	dánský	k2eAgFnSc7d1	dánská
lodí	loď	k1gFnSc7	loď
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
železniční	železniční	k2eAgFnPc1d1	železniční
společnosti	společnost	k1gFnPc1	společnost
uvalily	uvalit	k5eAaPmAgFnP	uvalit
embargo	embargo	k1gNnSc4	embargo
na	na	k7c4	na
všechen	všechen	k3xTgInSc4	všechen
dovoz	dovoz	k1gInSc4	dovoz
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
žití	žití	k1gNnSc2	žití
zde	zde	k6eAd1	zde
rychle	rychle	k6eAd1	rychle
vystoupala	vystoupat	k5eAaPmAgFnS	vystoupat
a	a	k8xC	a
najít	najít	k5eAaPmF	najít
cenově	cenově	k6eAd1	cenově
dostupné	dostupný	k2eAgNnSc4d1	dostupné
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
životu	život	k1gInSc3	život
bylo	být	k5eAaImAgNnS	být
téměř	téměř	k6eAd1	téměř
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
Miami	Miami	k1gNnSc4	Miami
silný	silný	k2eAgInSc1d1	silný
hurikán	hurikán	k1gInSc1	hurikán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podle	podle	k7c2	podle
Červeného	Červeného	k2eAgInSc2d1	Červeného
kříže	kříž	k1gInSc2	kříž
způsobil	způsobit	k5eAaPmAgInS	způsobit
373	[number]	k4	373
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
nehod	nehoda	k1gFnPc2	nehoda
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
nezvěstných	zvěstný	k2eNgNnPc2d1	nezvěstné
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bouře	bouře	k1gFnSc1	bouře
4	[number]	k4	4
<g/>
.	.	kIx.	.
kategorie	kategorie	k1gFnSc1	kategorie
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
nejhorší	zlý	k2eAgNnSc1d3	nejhorší
v	v	k7c6	v
USA	USA	kA	USA
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
co	co	k9	co
do	do	k7c2	do
množství	množství	k1gNnSc2	množství
škod	škoda	k1gFnPc2	škoda
a	a	k8xC	a
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yRgFnSc3	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
otevřen	otevřít	k5eAaPmNgInS	otevřít
tábor	tábor	k1gInSc1	tábor
CCC	CCC	kA	CCC
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1933	[number]	k4	1933
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
F.	F.	kA	F.
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgMnSc6	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
starosta	starosta	k1gMnSc1	starosta
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
měst	město	k1gNnPc2	město
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
finančně	finančně	k6eAd1	finančně
zruinovala	zruinovat	k5eAaPmAgFnS	zruinovat
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
Miami	Miami	k1gNnSc1	Miami
relativně	relativně	k6eAd1	relativně
nezasaženo	zasáhnout	k5eNaPmNgNnS	zasáhnout
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
před	před	k7c7	před
německými	německý	k2eAgFnPc7d1	německá
ponorkami	ponorka	k1gFnPc7	ponorka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
vojenské	vojenský	k2eAgInPc4d1	vojenský
distrikty	distrikt	k1gInPc4	distrikt
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
sílila	sílit	k5eAaImAgFnS	sílit
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
ponorkami	ponorka	k1gFnPc7	ponorka
<g/>
,	,	kIx,	,
přibývaly	přibývat	k5eAaImAgInP	přibývat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Miami	Miami	k1gNnSc2	Miami
vojenské	vojenský	k2eAgFnSc2d1	vojenská
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgNnSc1d1	americké
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
převzalo	převzít	k5eAaPmAgNnS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
přístavem	přístav	k1gInSc7	přístav
v	v	k7c6	v
Miami	Miami	k1gNnSc1	Miami
a	a	k8xC	a
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
si	se	k3xPyFc3	se
na	na	k7c6	na
místních	místní	k2eAgNnPc6d1	místní
letištích	letiště	k1gNnPc6	letiště
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
základny	základna	k1gFnPc1	základna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Floridě	Florida	k1gFnSc6	Florida
bylo	být	k5eAaImAgNnS	být
vycvičeno	vycvičit	k5eAaPmNgNnS	vycvičit
přes	přes	k7c4	přes
500	[number]	k4	500
000	[number]	k4	000
vojínů	vojín	k1gMnPc2	vojín
a	a	k8xC	a
50	[number]	k4	50
000	[number]	k4	000
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
sesazení	sesazení	k1gNnSc2	sesazení
generála	generál	k1gMnSc4	generál
Batisty	batist	k1gInPc4	batist
po	po	k7c6	po
kubánské	kubánský	k2eAgFnSc6d1	kubánská
revoluci	revoluce	k1gFnSc6	revoluce
byl	být	k5eAaImAgInS	být
návrat	návrat	k1gInSc4	návrat
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
Kubánců	Kubánec	k1gMnPc2	Kubánec
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
však	však	k9	však
lidé	člověk	k1gMnPc1	člověk
zklamaní	zklamaný	k2eAgMnPc1d1	zklamaný
novým	nový	k2eAgInSc7d1	nový
režimem	režim	k1gInSc7	režim
začali	začít	k5eAaPmAgMnP	začít
stěhovat	stěhovat	k5eAaImF	stěhovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
vlně	vlna	k1gFnSc6	vlna
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k6eAd1	mnoho
vysoce	vysoce	k6eAd1	vysoce
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
utekli	utéct	k5eAaPmAgMnP	utéct
za	za	k7c7	za
úspěšnější	úspěšný	k2eAgFnSc7d2	úspěšnější
kariérou	kariéra	k1gFnSc7	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
Dade	Dade	k1gFnSc4	Dade
County	Counta	k1gFnSc2	Counta
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Miami-Dade	Miami-Dad	k1gInSc5	Miami-Dad
County	Counta	k1gFnPc4	Counta
<g/>
)	)	kIx)	)
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
000	[number]	k4	000
kubánských	kubánský	k2eAgMnPc2d1	kubánský
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nebylo	být	k5eNaImAgNnS	být
Miami	Miami	k1gNnSc4	Miami
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
centrem	centrum	k1gNnSc7	centrum
hnutí	hnutí	k1gNnPc2	hnutí
za	za	k7c4	za
občanská	občanský	k2eAgNnPc4d1	občanské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
mělo	mít	k5eAaImAgNnS	mít
značnou	značný	k2eAgFnSc4d1	značná
populaci	populace	k1gFnSc4	populace
afroameričanů	afroameričan	k1gMnPc2	afroameričan
a	a	k8xC	a
černochů	černoch	k1gMnPc2	černoch
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
otevřena	otevřít	k5eAaPmNgFnS	otevřít
Florida	Florida	k1gFnSc1	Florida
International	International	k1gFnSc2	International
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1979	[number]	k4	1979
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
skandálu	skandál	k1gInSc3	skandál
<g/>
.	.	kIx.	.
</s>
<s>
Policisté	policista	k1gMnPc1	policista
stíhali	stíhat	k5eAaImAgMnP	stíhat
motocyklistu	motocyklista	k1gMnSc4	motocyklista
Arthura	Arthur	k1gMnSc4	Arthur
McDuffieho	McDuffie	k1gMnSc4	McDuffie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
poté	poté	k6eAd1	poté
havaroval	havarovat	k5eAaPmAgMnS	havarovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
policie	policie	k1gFnSc1	policie
dostihla	dostihnout	k5eAaPmAgFnS	dostihnout
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
zraněný	zraněný	k2eAgMnSc1d1	zraněný
<g/>
.	.	kIx.	.
</s>
<s>
Policisté	policista	k1gMnPc1	policista
mu	on	k3xPp3gMnSc3	on
sundali	sundat	k5eAaPmAgMnP	sundat
helmu	helma	k1gFnSc4	helma
a	a	k8xC	a
ubili	ubít	k5eAaPmAgMnP	ubít
ho	on	k3xPp3gMnSc4	on
obušky	obušek	k1gInPc7	obušek
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
helmu	helma	k1gFnSc4	helma
opět	opět	k6eAd1	opět
nasadili	nasadit	k5eAaPmAgMnP	nasadit
a	a	k8xC	a
zavolali	zavolat	k5eAaPmAgMnP	zavolat
ambulanci	ambulance	k1gFnSc4	ambulance
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
motocyklové	motocyklový	k2eAgFnSc3d1	motocyklová
nehodě	nehoda	k1gFnSc3	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
McDuffieho	McDuffieze	k6eAd1	McDuffieze
matka	matka	k1gFnSc1	matka
řekla	říct	k5eAaPmAgFnS	říct
novinám	novina	k1gFnPc3	novina
Miami	Miami	k1gNnSc2	Miami
Herald	Herald	k1gInSc4	Herald
o	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
toto	tento	k3xDgNnSc4	tento
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zbili	zbít	k5eAaPmAgMnP	zbít
mého	můj	k3xOp1gMnSc4	můj
syna	syn	k1gMnSc4	syn
jako	jako	k8xS	jako
psa	pes	k1gMnSc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Zbili	zbít	k5eAaPmAgMnP	zbít
ho	on	k3xPp3gMnSc4	on
jen	jen	k6eAd1	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jel	jet	k5eAaImAgMnS	jet
na	na	k7c6	na
motorce	motorka	k1gFnSc6	motorka
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
černý	černý	k2eAgMnSc1d1	černý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Porota	porota	k1gFnSc1	porota
však	však	k9	však
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
uvážení	uvážení	k1gNnSc2	uvážení
policisty	policista	k1gMnPc4	policista
zprostila	zprostit	k5eAaPmAgFnS	zprostit
viny	vina	k1gFnSc2	vina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
rozpoutalo	rozpoutat	k5eAaPmAgNnS	rozpoutat
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
nepokojů	nepokoj	k1gInPc2	nepokoj
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nepokoje	nepokoj	k1gInPc1	nepokoj
skončily	skončit	k5eAaPmAgInP	skončit
až	až	k9	až
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
během	během	k7c2	během
nich	on	k3xPp3gMnPc2	on
nejméně	málo	k6eAd3	málo
18	[number]	k4	18
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
přes	přes	k7c4	přes
850	[number]	k4	850
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Přibližná	přibližný	k2eAgFnSc1d1	přibližná
škoda	škoda	k1gFnSc1	škoda
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Miami	Miami	k1gNnSc1	Miami
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
amerických	americký	k2eAgNnPc2d1	americké
překladišť	překladiště	k1gNnPc2	překladiště
kokainu	kokain	k1gInSc2	kokain
z	z	k7c2	z
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc2	Bolívie
a	a	k8xC	a
Peru	Peru	k1gNnSc2	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Drogy	droga	k1gFnPc1	droga
přinesly	přinést	k5eAaPmAgFnP	přinést
Miami	Miami	k1gNnSc4	Miami
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgFnPc3	který
zde	zde	k6eAd1	zde
začaly	začít	k5eAaPmAgFnP	začít
vyrůstat	vyrůstat	k5eAaImF	vyrůstat
prodejny	prodejna	k1gFnPc1	prodejna
luxusních	luxusní	k2eAgInPc2d1	luxusní
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
pětihvězdičkové	pětihvězdičkový	k2eAgInPc4d1	pětihvězdičkový
hotely	hotel	k1gInPc4	hotel
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
znaky	znak	k1gInPc4	znak
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Marielský	Marielský	k2eAgInSc1d1	Marielský
exodus	exodus	k1gInSc1	exodus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
přivedl	přivést	k5eAaPmAgInS	přivést
do	do	k7c2	do
Miami	Miami	k1gNnPc2	Miami
dalších	další	k2eAgNnPc2d1	další
více	hodně	k6eAd2	hodně
než	než	k8xS	než
125	[number]	k4	125
000	[number]	k4	000
Kubánců	Kubánec	k1gMnPc2	Kubánec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
velké	velký	k2eAgFnSc2d1	velká
migrace	migrace	k1gFnSc2	migrace
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
však	však	k9	však
nyní	nyní	k6eAd1	nyní
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
mnoho	mnoho	k4c1	mnoho
chudých	chudý	k2eAgMnPc2d1	chudý
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
bílému	bílý	k2eAgInSc3d1	bílý
útěku	útěk	k1gInSc3	útěk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
starostou	starosta	k1gMnSc7	starosta
Miami	Miami	k1gNnSc2	Miami
Xavier	Xavier	k1gMnSc1	Xavier
Suarez	Suarez	k1gMnSc1	Suarez
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
kubánským	kubánský	k2eAgMnSc7d1	kubánský
starostou	starosta	k1gMnSc7	starosta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
Miami	Miami	k1gNnSc1	Miami
mnoho	mnoho	k6eAd1	mnoho
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
prezident	prezident	k1gMnSc1	prezident
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
jih	jih	k1gInSc1	jih
Miami-Dade	Miami-Dad	k1gInSc5	Miami-Dad
County	Count	k1gInPc4	Count
hurikán	hurikán	k1gInSc1	hurikán
Andrew	Andrew	k1gMnPc2	Andrew
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
způsobil	způsobit	k5eAaPmAgMnS	způsobit
škody	škoda	k1gFnPc4	škoda
za	za	k7c4	za
20	[number]	k4	20
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
velká	velký	k2eAgFnSc1d1	velká
migrace	migrace	k1gFnSc1	migrace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
Clintonově	Clintonův	k2eAgFnSc3d1	Clintonova
nové	nový	k2eAgFnSc3d1	nová
politice	politika	k1gFnSc3	politika
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
výrazná	výrazný	k2eAgFnSc1d1	výrazná
<g/>
.	.	kIx.	.
</s>
<s>
Kubánce	Kubánec	k1gMnPc4	Kubánec
chycené	chycený	k2eAgMnPc4d1	chycený
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
totiž	totiž	k9	totiž
americká	americký	k2eAgFnSc1d1	americká
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
stráž	stráž	k1gFnSc1	stráž
odvážela	odvážet	k5eAaImAgFnS	odvážet
do	do	k7c2	do
Guantánama	Guantánamum	k1gNnSc2	Guantánamum
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Panamy	Panama	k1gFnSc2	Panama
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
emigrace	emigrace	k1gFnSc2	emigrace
opustilo	opustit	k5eAaPmAgNnS	opustit
své	svůj	k3xOyFgInPc4	svůj
domovy	domov	k1gInPc4	domov
téměř	téměř	k6eAd1	téměř
38	[number]	k4	38
000	[number]	k4	000
Kubánců	Kubánec	k1gMnPc2	Kubánec
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1994	[number]	k4	1994
se	se	k3xPyFc4	se
USA	USA	kA	USA
a	a	k8xC	a
Kuba	Kuba	k1gFnSc1	Kuba
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
normalizaci	normalizace	k1gFnSc6	normalizace
migrace	migrace	k1gFnSc2	migrace
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
Miami	Miami	k1gNnSc2	Miami
kvůli	kvůli	k7c3	kvůli
finančním	finanční	k2eAgInPc3d1	finanční
skandálům	skandál	k1gInPc3	skandál
umístilo	umístit	k5eAaPmAgNnS	umístit
jako	jako	k9	jako
4	[number]	k4	4
<g/>
.	.	kIx.	.
nejchudší	chudý	k2eAgNnSc1d3	nejchudší
město	město	k1gNnSc1	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInPc1d1	finanční
problémy	problém	k1gInPc1	problém
města	město	k1gNnSc2	město
přetrvávaly	přetrvávat	k5eAaImAgInP	přetrvávat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
starostou	starosta	k1gMnSc7	starosta
Miami	Miami	k1gNnSc2	Miami
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Manny	Mann	k1gMnPc4	Mann
Diaz	Diaz	k1gInSc4	Diaz
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
pouhých	pouhý	k2eAgInPc2d1	pouhý
92	[number]	k4	92
km	km	kA	km
<g/>
2	[number]	k4	2
má	mít	k5eAaImIp3nS	mít
Miami	Miami	k1gNnSc4	Miami
nejmenší	malý	k2eAgNnSc4d3	nejmenší
území	území	k1gNnSc4	území
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
významných	významný	k2eAgNnPc2d1	významné
měst	město	k1gNnPc2	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
Miami	Miami	k1gNnSc2	Miami
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
143,1	[number]	k4	143,1
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
ovšem	ovšem	k9	ovšem
50,7	[number]	k4	50,7
km	km	kA	km
<g/>
2	[number]	k4	2
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
Miami	Miami	k1gNnSc4	Miami
mezi	mezi	k7c4	mezi
nejhustěji	husto	k6eAd3	husto
zalidněná	zalidněný	k2eAgNnPc4d1	zalidněné
města	město	k1gNnPc4	město
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
oblasti	oblast	k1gFnSc2	oblast
není	být	k5eNaImIp3nS	být
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
12	[number]	k4	12
m	m	kA	m
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
1,8	[number]	k4	1,8
m.	m.	k?	m.
Hladina	hladina	k1gFnSc1	hladina
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc1d1	jediné
významné	významný	k2eAgNnSc1d1	významné
americké	americký	k2eAgNnSc1d1	americké
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
hraničí	hraničit	k5eAaImIp3nS	hraničit
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
národními	národní	k2eAgInPc7d1	národní
parky	park	k1gInPc7	park
-	-	kIx~	-
Národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
Everglades	Evergladesa	k1gFnPc2	Evergladesa
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
Biscayne	Biscayn	k1gInSc5	Biscayn
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
předměstí	předměstí	k1gNnSc6	předměstí
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
mezi	mezi	k7c4	mezi
mokřady	mokřad	k1gInPc4	mokřad
Everglades	Evergladesa	k1gFnPc2	Evergladesa
a	a	k8xC	a
lagunou	laguna	k1gFnSc7	laguna
Biscayne	Biscayn	k1gInSc5	Biscayn
Bay	Bay	k1gMnSc6	Bay
<g/>
.	.	kIx.	.
145	[number]	k4	145
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jezero	jezero	k1gNnSc1	jezero
Okeechobee	Okeechobe	k1gFnSc2	Okeechobe
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Biscayne	Biscayn	k1gInSc5	Biscayn
Bay	Bay	k1gFnSc1	Bay
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k6eAd1	mnoho
set	set	k1gInSc4	set
přírodně	přírodně	k6eAd1	přírodně
i	i	k8xC	i
nepřirozeně	přirozeně	k6eNd1	přirozeně
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
bariérových	bariérový	k2eAgInPc2d1	bariérový
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgFnSc4d3	veliký
jsou	být	k5eAaImIp3nP	být
Miami	Miami	k1gNnSc4	Miami
Beach	Beacha	k1gFnPc2	Beacha
a	a	k8xC	a
South	Southa	k1gFnPc2	Southa
Beach	Beacha	k1gFnPc2	Beacha
<g/>
.	.	kIx.	.
</s>
<s>
Golfský	golfský	k2eAgInSc1d1	golfský
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
teplý	teplý	k2eAgInSc1d1	teplý
mořský	mořský	k2eAgInSc1d1	mořský
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
proudí	proudit	k5eAaPmIp3nP	proudit
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
pouhých	pouhý	k2eAgInPc2d1	pouhý
24	[number]	k4	24
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klima	klima	k1gNnSc1	klima
zde	zde	k6eAd1	zde
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
teplé	teplý	k2eAgNnSc1d1	teplé
a	a	k8xC	a
mírné	mírný	k2eAgNnSc1d1	mírné
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Skalní	skalní	k2eAgNnSc1d1	skalní
podloží	podloží	k1gNnSc1	podloží
pod	pod	k7c7	pod
oblastí	oblast	k1gFnSc7	oblast
Miami	Miami	k1gNnSc2	Miami
je	být	k5eAaImIp3nS	být
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Miami	Miami	k1gNnSc1	Miami
limestone	limeston	k1gInSc5	limeston
nebo	nebo	k8xC	nebo
Miami	Miami	k1gNnSc2	Miami
oolite	oolit	k1gInSc5	oolit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
podloží	podloží	k1gNnSc1	podloží
<g/>
,	,	kIx,	,
silné	silný	k2eAgNnSc1d1	silné
ne	ne	k9	ne
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
m	m	kA	m
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
limestone	limeston	k1gInSc5	limeston
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
důsledkem	důsledek	k1gInSc7	důsledek
drastických	drastický	k2eAgFnPc2d1	drastická
změn	změna	k1gFnPc2	změna
hladiny	hladina	k1gFnSc2	hladina
moře	moře	k1gNnSc2	moře
spojených	spojený	k2eAgInPc2d1	spojený
se	s	k7c7	s
zaledněním	zalednění	k1gNnSc7	zalednění
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
rovinou	rovina	k1gFnSc7	rovina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zvodeň	zvodeň	k1gFnSc1	zvodeň
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
laguny	laguna	k1gFnSc2	laguna
<g/>
)	)	kIx)	)
Biscayne	Biscayn	k1gInSc5	Biscayn
Aquifer	Aquifero	k1gNnPc2	Aquifero
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zvodeň	zvodeň	k1gFnSc1	zvodeň
je	být	k5eAaImIp3nS	být
přírodním	přírodní	k2eAgInSc7d1	přírodní
podzemním	podzemní	k2eAgInSc7d1	podzemní
zdrojem	zdroj	k1gInSc7	zdroj
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
ji	on	k3xPp3gFnSc4	on
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
zvodeň	zvodeň	k1gFnSc1	zvodeň
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
stavbu	stavba	k1gFnSc4	stavba
podzemních	podzemní	k2eAgFnPc2d1	podzemní
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
má	mít	k5eAaImIp3nS	mít
tropické	tropický	k2eAgNnSc4d1	tropické
monzunové	monzunový	k2eAgNnSc4d1	monzunové
klima	klima	k1gNnSc4	klima
(	(	kIx(	(
<g/>
Köppenova	Köppenův	k2eAgFnSc1d1	Köppenova
klasifikace	klasifikace	k1gFnSc1	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
-	-	kIx~	-
Am	Am	k1gFnSc1	Am
<g/>
)	)	kIx)	)
s	s	k7c7	s
horkými	horký	k2eAgNnPc7d1	horké
a	a	k8xC	a
vlhkými	vlhký	k2eAgNnPc7d1	vlhké
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
krátkými	krátký	k2eAgFnPc7d1	krátká
<g/>
,	,	kIx,	,
teplými	teplý	k2eAgInPc7d1	teplý
a	a	k8xC	a
značně	značně	k6eAd1	značně
suššími	suchý	k2eAgFnPc7d2	sušší
zimami	zima	k1gFnPc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhé	druhý	k4xOgNnSc4	druhý
nejvlhčí	vlhký	k2eAgNnSc4d3	nejvlhčí
město	město	k1gNnSc4	město
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podobu	podoba	k1gFnSc4	podoba
podnebí	podnebí	k1gNnSc2	podnebí
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeho	jeho	k3xOp3gFnSc1	jeho
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
poloha	poloha	k1gFnSc1	poloha
<g/>
,	,	kIx,	,
blízkost	blízkost	k1gFnSc1	blízkost
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
pozice	pozice	k1gFnSc2	pozice
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
obratníkem	obratník	k1gInSc7	obratník
Raka	rak	k1gMnSc2	rak
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
lednovou	lednový	k2eAgFnSc7d1	lednová
teplotou	teplota	k1gFnSc7	teplota
19,6	[number]	k4	19,6
°	°	k?	°
<g/>
C	C	kA	C
zimu	zima	k1gFnSc4	zima
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
mírné	mírný	k2eAgFnPc1d1	mírná
až	až	k8xS	až
vyšší	vysoký	k2eAgFnPc1d2	vyšší
teploty	teplota	k1gFnPc1	teplota
a	a	k8xC	a
chladný	chladný	k2eAgInSc1d1	chladný
vzduch	vzduch	k1gInSc1	vzduch
snášející	snášející	k2eAgInSc1d1	snášející
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
průchodu	průchod	k1gInSc6	průchod
studené	studený	k2eAgFnSc2d1	studená
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
hodně	hodně	k6eAd1	hodně
drobných	drobný	k2eAgFnPc2d1	drobná
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Deštivá	deštivý	k2eAgFnSc1d1	deštivá
sezóna	sezóna	k1gFnSc1	sezóna
začíná	začínat	k5eAaImIp3nS	začínat
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
uprostřed	uprostřed	k7c2	uprostřed
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
teploty	teplota	k1gFnPc1	teplota
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
29	[number]	k4	29
až	až	k9	až
35	[number]	k4	35
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
velkou	velký	k2eAgFnSc7d1	velká
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
odpolední	odpolední	k2eAgFnPc1d1	odpolední
bouřky	bouřka	k1gFnPc1	bouřka
a	a	k8xC	a
brízy	bríza	k1gFnPc1	bríza
rozšiřující	rozšiřující	k2eAgFnPc1d1	rozšiřující
se	se	k3xPyFc4	se
od	od	k7c2	od
Atlantiku	Atlantik	k1gInSc2	Atlantik
snižují	snižovat	k5eAaImIp3nP	snižovat
teplotu	teplota	k1gFnSc4	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
ročních	roční	k2eAgInPc2d1	roční
cca	cca	kA	cca
1420	[number]	k4	1420
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
-3	-3	k4	-3
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
37	[number]	k4	37
°	°	k?	°
<g/>
C.	C.	kA	C.
Existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc1d1	jediný
záznam	záznam	k1gInSc1	záznam
sněhových	sněhový	k2eAgFnPc2d1	sněhová
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Správu	správa	k1gFnSc4	správa
Miami	Miami	k1gNnSc2	Miami
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
starosty	starosta	k1gMnSc2	starosta
a	a	k8xC	a
pěti	pět	k4xCc2	pět
komisařů	komisař	k1gMnPc2	komisař
(	(	kIx(	(
<g/>
městská	městský	k2eAgFnSc1d1	městská
komise	komise	k1gFnSc1	komise
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
městská	městský	k2eAgFnSc1d1	městská
správa	správa	k1gFnSc1	správa
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
volená	volená	k1gFnSc1	volená
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
řídící	řídící	k2eAgInSc4d1	řídící
orgán	orgán	k1gInSc4	orgán
s	s	k7c7	s
pravomocí	pravomoc	k1gFnSc7	pravomoc
vydávat	vydávat	k5eAaImF	vydávat
vyhlášky	vyhláška	k1gFnPc4	vyhláška
<g/>
,	,	kIx,	,
přijímat	přijímat	k5eAaImF	přijímat
nařízení	nařízení	k1gNnSc4	nařízení
a	a	k8xC	a
vykonávat	vykonávat	k5eAaImF	vykonávat
všechny	všechen	k3xTgFnPc4	všechen
pravomoci	pravomoc	k1gFnPc4	pravomoc
uvedené	uvedený	k2eAgFnPc4d1	uvedená
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
chartě	charta	k1gFnSc6	charta
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
voleni	volen	k2eAgMnPc1d1	volen
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
distriktech	distrikt	k1gInPc6	distrikt
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
<g/>
,	,	kIx,	,
volen	volen	k2eAgInSc1d1	volen
obyvateli	obyvatel	k1gMnPc7	obyvatel
města	město	k1gNnPc4	město
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
správce	správce	k1gMnSc1	správce
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
radnice	radnice	k1gFnSc1	radnice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Coconut	Coconut	k1gMnSc1	Coconut
Grove	Groev	k1gFnSc2	Groev
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
starostů	starosta	k1gMnPc2	starosta
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnPc4d3	nejdůležitější
finanční	finanční	k2eAgNnPc4d1	finanční
centra	centrum	k1gNnPc4	centrum
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
centrem	centr	k1gMnSc7	centr
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
sídlem	sídlo	k1gNnSc7	sídlo
mnoha	mnoho	k4c2	mnoho
korporací	korporace	k1gFnPc2	korporace
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
pyšnit	pyšnit	k5eAaImF	pyšnit
silnými	silný	k2eAgInPc7d1	silný
mezinárodními	mezinárodní	k2eAgInPc7d1	mezinárodní
obchodními	obchodní	k2eAgInPc7d1	obchodní
kruhy	kruh	k1gInPc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hodnocení	hodnocení	k1gNnSc2	hodnocení
světových	světový	k2eAgNnPc2d1	světové
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
provedeného	provedený	k2eAgNnSc2d1	provedené
střediskem	středisko	k1gNnSc7	středisko
Globalization	Globalization	k1gInSc1	Globalization
and	and	k?	and
World	World	k1gMnSc1	World
Cities	Cities	k1gMnSc1	Cities
Study	stud	k1gInPc1	stud
Group	Group	k1gInSc1	Group
&	&	k?	&
Network	network	k1gInSc1	network
(	(	kIx(	(
<g/>
GaWC	GaWC	k1gFnSc1	GaWC
<g/>
)	)	kIx)	)
a	a	k8xC	a
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c4	na
přispění	přispění	k1gNnSc4	přispění
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
světové	světový	k2eAgFnSc2d1	světová
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Miami	Miami	k1gNnSc1	Miami
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
Beta-	Beta-	k1gMnSc2	Beta-
Global	globat	k5eAaImAgMnS	globat
City	city	k1gNnSc4	city
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
přínosem	přínos	k1gInSc7	přínos
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
i	i	k9	i
turistika	turistika	k1gFnSc1	turistika
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yIgFnSc3	který
Miami	Miami	k1gNnSc2	Miami
získává	získávat	k5eAaImIp3nS	získávat
miliardy	miliarda	k4xCgFnPc4	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc1	sídlo
mnoho	mnoho	k4c4	mnoho
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těchto	tento	k3xDgInPc2	tento
<g/>
:	:	kIx,	:
Alienware	Alienwar	k1gMnSc5	Alienwar
<g/>
,	,	kIx,	,
Arrow	Arrow	k1gMnSc5	Arrow
Air	Air	k1gMnSc5	Air
<g/>
,	,	kIx,	,
Bacardi	Bacard	k1gMnPc1	Bacard
<g/>
,	,	kIx,	,
Burger	Burger	k1gMnSc1	Burger
King	King	k1gMnSc1	King
<g/>
,	,	kIx,	,
CompUSA	CompUSA	k1gMnSc1	CompUSA
<g/>
,	,	kIx,	,
Fizber	Fizber	k1gMnSc1	Fizber
<g/>
,	,	kIx,	,
Norwegian	Norwegian	k1gMnSc1	Norwegian
Cruise	Cruise	k1gFnSc2	Cruise
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
RCTV	RCTV	kA	RCTV
International	International	k1gMnSc1	International
<g/>
,	,	kIx,	,
Royal	Royal	k1gMnSc1	Royal
Caribbean	Caribbean	k1gMnSc1	Caribbean
International	International	k1gMnSc1	International
<g/>
,	,	kIx,	,
Telefónica	Telefónica	k1gMnSc1	Telefónica
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Century	Centura	k1gFnPc4	Centura
Bank	bank	k1gInSc1	bank
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
blízkosti	blízkost	k1gFnSc3	blízkost
k	k	k7c3	k
Latinské	latinský	k2eAgFnSc3d1	Latinská
Americe	Amerika	k1gFnSc3	Amerika
je	být	k5eAaImIp3nS	být
také	také	k9	také
sídlem	sídlo	k1gNnSc7	sídlo
působení	působení	k1gNnSc2	působení
více	hodně	k6eAd2	hodně
než	než	k8xS	než
170	[number]	k4	170
nadnárodních	nadnárodní	k2eAgFnPc2d1	nadnárodní
korporací	korporace	k1gFnPc2	korporace
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
AIG	AIG	kA	AIG
<g/>
,	,	kIx,	,
American	American	k1gMnSc1	American
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
,	,	kIx,	,
Cisco	Cisco	k1gMnSc1	Cisco
<g/>
,	,	kIx,	,
Disney	Disnea	k1gFnPc1	Disnea
<g/>
,	,	kIx,	,
Exxon	Exxon	k1gInSc1	Exxon
<g/>
,	,	kIx,	,
FedEx	FedEx	k1gInSc1	FedEx
<g/>
,	,	kIx,	,
Kraft	Kraft	k2eAgInSc1d1	Kraft
Foods	Foods	k1gInSc1	Foods
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
Oracle	Oracle	k1gNnSc2	Oracle
<g/>
,	,	kIx,	,
SBC	SBC	kA	SBC
Communications	Communications	k1gInSc1	Communications
<g/>
,	,	kIx,	,
Sony	Sony	kA	Sony
<g/>
,	,	kIx,	,
Visa	viso	k1gNnSc2	viso
International	International	k1gFnPc2	International
nebo	nebo	k8xC	nebo
Walmart	Walmarta	k1gFnPc2	Walmarta
<g/>
.	.	kIx.	.
</s>
<s>
Miamské	miamský	k2eAgNnSc1d1	miamské
letiště	letiště	k1gNnSc1	letiště
a	a	k8xC	a
přístav	přístav	k1gInSc1	přístav
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejzatíženější	zatížený	k2eAgFnPc4d3	nejzatíženější
vstupní	vstupní	k2eAgFnPc4d1	vstupní
brány	brána	k1gFnPc4	brána
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
náklady	náklad	k1gInPc4	náklad
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Miami	Miami	k1gNnSc2	Miami
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
finanční	finanční	k2eAgFnSc6d1	finanční
městské	městský	k2eAgFnSc6d1	městská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Brickell	Brickella	k1gFnPc2	Brickella
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
koncentrace	koncentrace	k1gFnSc1	koncentrace
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
bank	banka	k1gFnPc2	banka
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
Miami	Miami	k1gNnSc1	Miami
hostujícím	hostující	k2eAgNnSc7d1	hostující
městem	město	k1gNnSc7	město
obchodních	obchodní	k2eAgNnPc2d1	obchodní
jednání	jednání	k1gNnPc2	jednání
zóny	zóna	k1gFnSc2	zóna
Free	Free	k1gFnSc1	Free
Trade	Trad	k1gInSc5	Trad
Area	area	k1gFnSc1	area
of	of	k?	of
Americas	Americas	k1gInSc1	Americas
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
turistický	turistický	k2eAgInSc1d1	turistický
průmysl	průmysl	k1gInSc1	průmysl
přináší	přinášet	k5eAaImIp3nS	přinášet
do	do	k7c2	do
kasy	kasa	k1gFnSc2	kasa
města	město	k1gNnSc2	město
mnoho	mnoho	k6eAd1	mnoho
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Pláže	pláž	k1gFnPc1	pláž
<g/>
,	,	kIx,	,
konference	konference	k1gFnPc1	konference
<g/>
,	,	kIx,	,
festivaly	festival	k1gInPc1	festival
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
akce	akce	k1gFnPc1	akce
lákají	lákat	k5eAaImIp3nP	lákat
do	do	k7c2	do
Miami	Miami	k1gNnSc2	Miami
více	hodně	k6eAd2	hodně
než	než	k8xS	než
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zde	zde	k6eAd1	zde
návštěvníci	návštěvník	k1gMnPc1	návštěvník
utratili	utratit	k5eAaPmAgMnP	utratit
17,1	[number]	k4	17,1
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
čtvrť	čtvrť	k1gFnSc1	čtvrť
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
South	South	k1gMnSc1	South
Beach	Beach	k1gMnSc1	Beach
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejpůvabnějších	půvabný	k2eAgFnPc2d3	nejpůvabnější
čtvrtí	čtvrt	k1gFnPc2	čtvrt
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svými	svůj	k3xOyFgInPc7	svůj
nočními	noční	k2eAgInPc7d1	noční
kluby	klub	k1gInPc7	klub
<g/>
,	,	kIx,	,
plážemi	pláž	k1gFnPc7	pláž
<g/>
,	,	kIx,	,
obchody	obchod	k1gInPc7	obchod
a	a	k8xC	a
historickými	historický	k2eAgFnPc7d1	historická
budovami	budova	k1gFnPc7	budova
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
South	South	k1gMnSc1	South
Beach	Beach	k1gMnSc1	Beach
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
Miami	Miami	k1gNnSc2	Miami
Beach	Beach	k1gMnSc1	Beach
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
s	s	k7c7	s
Miami	Miami	k1gNnSc7	Miami
sousedí	sousedit	k5eAaImIp3nP	sousedit
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
také	také	k9	také
centrem	centrum	k1gNnSc7	centrum
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
zde	zde	k6eAd1	zde
převládá	převládat	k5eAaImIp3nS	převládat
těžba	těžba	k1gFnSc1	těžba
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
skladování	skladování	k1gNnSc2	skladování
nejrůznějšího	různý	k2eAgNnSc2d3	nejrůznější
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
zde	zde	k6eAd1	zde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
velikému	veliký	k2eAgInSc3d1	veliký
rozmachu	rozmach	k1gInSc3	rozmach
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
zde	zde	k6eAd1	zde
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
přes	přes	k7c4	přes
50	[number]	k4	50
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
vyšších	vysoký	k2eAgInPc2d2	vyšší
než	než	k8xS	než
122	[number]	k4	122
m.	m.	k?	m.
Městská	městský	k2eAgFnSc1d1	městská
silueta	silueta	k1gFnSc1	silueta
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
nejpůsobivější	působivý	k2eAgFnSc1d3	nejpůsobivější
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c4	za
New	New	k1gFnSc4	New
Yorkem	York	k1gInSc7	York
a	a	k8xC	a
Chicagem	Chicago	k1gNnSc7	Chicago
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
Almanac	Almanac	k1gFnSc1	Almanac
of	of	k?	of
Architecture	Architectur	k1gMnSc5	Architectur
and	and	k?	and
Design	design	k1gInSc1	design
devatenáctá	devatenáctý	k4xOgFnSc1	devatenáctý
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc1	město
8	[number]	k4	8
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
ve	v	k7c6	v
státu	stát	k1gInSc6	stát
Florida	Florida	k1gFnSc1	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
Four	Four	k1gInSc1	Four
Seasons	Seasons	k1gInSc1	Seasons
Hotel	hotel	k1gInSc1	hotel
&	&	k?	&
Tower	Tower	k1gInSc4	Tower
měřící	měřící	k2eAgFnSc2d1	měřící
240	[number]	k4	240
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zjištění	zjištění	k1gNnSc2	zjištění
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Census	census	k1gInSc4	census
Bureau	Bureaus	k1gInSc2	Bureaus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
třetí	třetí	k4xOgMnSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
výskyt	výskyt	k1gInSc1	výskyt
rodin	rodina	k1gFnPc2	rodina
s	s	k7c7	s
příjmem	příjem	k1gInSc7	příjem
pod	pod	k7c7	pod
celostátní	celostátní	k2eAgFnSc7d1	celostátní
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
třetí	třetí	k4xOgNnSc4	třetí
nejchudší	chudý	k2eAgNnSc4d3	nejchudší
město	město	k1gNnSc4	město
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Detroitu	Detroit	k1gInSc2	Detroit
a	a	k8xC	a
El	Ela	k1gFnPc2	Ela
Pasu	pas	k1gInSc2	pas
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
patří	patřit	k5eAaImIp3nS	patřit
Miami	Miami	k1gNnSc1	Miami
mezi	mezi	k7c4	mezi
pár	pár	k4xCyI	pár
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
místní	místní	k2eAgFnSc1d1	místní
správa	správa	k1gFnSc1	správa
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zažívá	zažívat	k5eAaImIp3nS	zažívat
město	město	k1gNnSc1	město
oživení	oživení	k1gNnSc2	oživení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
magazínu	magazín	k1gInSc2	magazín
Forbes	forbes	k1gInSc4	forbes
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
nejčistším	čistý	k2eAgNnSc7d3	nejčistší
americkým	americký	k2eAgNnSc7d1	americké
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
celoročně	celoročně	k6eAd1	celoročně
dobré	dobrý	k2eAgFnSc3d1	dobrá
kvalitě	kvalita	k1gFnSc3	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
,	,	kIx,	,
rozsáhlým	rozsáhlý	k2eAgFnPc3d1	rozsáhlá
plochám	plocha	k1gFnPc3	plocha
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
,	,	kIx,	,
čisté	čistý	k2eAgFnSc3d1	čistá
pitné	pitný	k2eAgFnSc3d1	pitná
vodě	voda	k1gFnSc3	voda
<g/>
,	,	kIx,	,
čistým	čistý	k2eAgFnPc3d1	čistá
ulicím	ulice	k1gFnPc3	ulice
a	a	k8xC	a
recyklaci	recyklace	k1gFnSc3	recyklace
odpadů	odpad	k1gInPc2	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
ohodnotila	ohodnotit	k5eAaPmAgFnS	ohodnotit
UBS	UBS	kA	UBS
studie	studie	k1gFnSc1	studie
73	[number]	k4	73
světových	světový	k2eAgNnPc2d1	světové
měst	město	k1gNnPc2	město
Miami	Miami	k1gNnSc2	Miami
jako	jako	k8xC	jako
nejbohatší	bohatý	k2eAgNnSc4d3	nejbohatší
město	město	k1gNnSc4	město
USA	USA	kA	USA
(	(	kIx(	(
<g/>
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
uvedených	uvedený	k2eAgInPc2d1	uvedený
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
<g/>
)	)	kIx)	)
a	a	k8xC	a
páté	pátá	k1gFnSc2	pátá
světové	světový	k2eAgNnSc4d1	světové
nejbohatší	bohatý	k2eAgNnSc4d3	nejbohatší
město	město	k1gNnSc4	město
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
Miami	Miami	k1gNnSc1	Miami
svědkem	svědek	k1gMnSc7	svědek
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
na	na	k7c6	na
realitním	realitní	k2eAgInSc6d1	realitní
trhu	trh	k1gInSc6	trh
od	od	k7c2	od
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Midtown	Midtown	k1gNnSc1	Midtown
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
přes	přes	k7c4	přes
stovku	stovka	k1gFnSc4	stovka
schválených	schválený	k2eAgInPc2d1	schválený
stavebních	stavební	k2eAgInPc2d1	stavební
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
však	však	k9	však
bytový	bytový	k2eAgInSc1d1	bytový
trh	trh	k1gInSc1	trh
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
23	[number]	k4	23
000	[number]	k4	000
družstevních	družstevní	k2eAgInPc2d1	družstevní
domů	dům	k1gInPc2	dům
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgFnP	být
zkonfiskovány	zkonfiskovat	k5eAaPmNgFnP	zkonfiskovat
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
je	být	k5eAaImIp3nS	být
43	[number]	k4	43
<g/>
.	.	kIx.	.
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
aglomerace	aglomerace	k1gFnSc1	aglomerace
Jižní	jižní	k2eAgFnSc1d1	jižní
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInPc1d1	zahrnující
okresy	okres	k1gInPc1	okres
Miami-Dade	Miami-Dad	k1gInSc5	Miami-Dad
<g/>
,	,	kIx,	,
Broward	Broward	k1gMnSc1	Broward
a	a	k8xC	a
Palm	Palm	k1gMnSc1	Palm
Beach	Beach	k1gMnSc1	Beach
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5,5	[number]	k4	5,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nS	činit
devátou	devátý	k4xOgFnSc7	devátý
největší	veliký	k2eAgFnSc7d3	veliký
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
ohodnotila	ohodnotit	k5eAaPmAgFnS	ohodnotit
OSN	OSN	kA	OSN
celou	celá	k1gFnSc4	celá
aglomeraci	aglomerace	k1gFnSc4	aglomerace
jako	jako	k9	jako
44	[number]	k4	44
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
362	[number]	k4	362
470	[number]	k4	470
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
83	[number]	k4	83
336	[number]	k4	336
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
134	[number]	k4	134
198	[number]	k4	198
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
3	[number]	k4	3
923,5	[number]	k4	923,5
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
Podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
American	American	k1gInSc1	American
Community	Communita	k1gFnSc2	Communita
Survey	Survea	k1gFnSc2	Survea
<g/>
,	,	kIx,	,
probíhajícího	probíhající	k2eAgInSc2d1	probíhající
v	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
až	až	k9	až
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
rasová	rasový	k2eAgFnSc1d1	rasová
struktura	struktura	k1gFnSc1	struktura
Miami	Miami	k1gNnSc1	Miami
následující	následující	k2eAgFnSc2d1	následující
<g/>
:	:	kIx,	:
Roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
etnické	etnický	k2eAgNnSc1d1	etnické
složení	složení	k1gNnSc1	složení
podle	podle	k7c2	podle
národnosti	národnost	k1gFnSc2	národnost
takovéto	takovýto	k3xDgFnSc2	takovýto
<g/>
:	:	kIx,	:
34,1	[number]	k4	34,1
%	%	kIx~	%
tvořili	tvořit	k5eAaImAgMnP	tvořit
Kubánci	Kubánec	k1gMnPc1	Kubánec
<g/>
,	,	kIx,	,
5,6	[number]	k4	5,6
%	%	kIx~	%
Nikaraguánci	Nikaraguánek	k1gMnPc1	Nikaraguánek
<g/>
,	,	kIx,	,
5,5	[number]	k4	5,5
%	%	kIx~	%
Haiťané	Haiťan	k1gMnPc1	Haiťan
<g/>
,	,	kIx,	,
3,3	[number]	k4	3,3
%	%	kIx~	%
Hondurasci	Hondurasek	k1gMnPc1	Hondurasek
<g/>
,	,	kIx,	,
1,7	[number]	k4	1,7
%	%	kIx~	%
Dominikánci	Dominikánek	k1gMnPc1	Dominikánek
a	a	k8xC	a
1,6	[number]	k4	1,6
%	%	kIx~	%
Kolumbijci	Kolumbijec	k1gMnPc1	Kolumbijec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
134	[number]	k4	134
198	[number]	k4	198
domácností	domácnost	k1gFnPc2	domácnost
jich	on	k3xPp3gNnPc2	on
26,3	[number]	k4	26,3
%	%	kIx~	%
mělo	mít	k5eAaImAgNnS	mít
dítě	dítě	k1gNnSc1	dítě
pod	pod	k7c4	pod
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
36,6	[number]	k4	36,6
%	%	kIx~	%
byly	být	k5eAaImAgInP	být
páry	pár	k1gInPc1	pár
žijící	žijící	k2eAgInPc1d1	žijící
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
18,7	[number]	k4	18,7
%	%	kIx~	%
domácností	domácnost	k1gFnPc2	domácnost
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
ženskou	ženský	k2eAgFnSc7d1	ženská
hlavou	hlava	k1gFnSc7	hlava
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
a	a	k8xC	a
37,9	[number]	k4	37,9
%	%	kIx~	%
domácností	domácnost	k1gFnPc2	domácnost
nebylo	být	k5eNaImAgNnS	být
obýváno	obývat	k5eAaImNgNnS	obývat
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
30,4	[number]	k4	30,4
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
domácností	domácnost	k1gFnPc2	domácnost
tvořili	tvořit	k5eAaImAgMnP	tvořit
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
a	a	k8xC	a
12,5	[number]	k4	12,5
%	%	kIx~	%
lidé	člověk	k1gMnPc1	člověk
starší	starý	k2eAgMnPc1d2	starší
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
domácnost	domácnost	k1gFnSc4	domácnost
vycházelo	vycházet	k5eAaImAgNnS	vycházet
2,61	[number]	k4	2,61
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
na	na	k7c4	na
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
rodinu	rodina	k1gFnSc4	rodina
3,25	[number]	k4	3,25
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Věková	věkový	k2eAgFnSc1d1	věková
struktura	struktura	k1gFnSc1	struktura
populace	populace	k1gFnSc2	populace
byla	být	k5eAaImAgFnS	být
následovná	následovný	k2eAgFnSc1d1	následovná
<g/>
:	:	kIx,	:
24,2	[number]	k4	24,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
mělo	mít	k5eAaImAgNnS	mít
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
21,3	[number]	k4	21,3
%	%	kIx~	%
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
20-34	[number]	k4	20-34
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
27,6	[number]	k4	27,6
%	%	kIx~	%
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
35-54	[number]	k4	35-54
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
9,9	[number]	k4	9,9
%	%	kIx~	%
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
55-64	[number]	k4	55-64
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
14,7	[number]	k4	14,7
%	%	kIx~	%
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
65-84	[number]	k4	65-84
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
2,3	[number]	k4	2,3
%	%	kIx~	%
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
85	[number]	k4	85
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
vyšším	vysoký	k2eAgInSc7d2	vyšší
a	a	k8xC	a
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
byl	být	k5eAaImAgInS	být
37,7	[number]	k4	37,7
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
domácnosti	domácnost	k1gFnSc2	domácnost
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
byl	být	k5eAaImAgMnS	být
23	[number]	k4	23
483	[number]	k4	483
a	a	k8xC	a
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
rodiny	rodina	k1gFnSc2	rodina
27	[number]	k4	27
225	[number]	k4	225
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
měli	mít	k5eAaImAgMnP	mít
průměrný	průměrný	k2eAgInSc4d1	průměrný
příjem	příjem	k1gInSc4	příjem
24	[number]	k4	24
0	[number]	k4	0
<g/>
90	[number]	k4	90
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
ženám	žena	k1gFnPc3	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
průměrně	průměrně	k6eAd1	průměrně
20	[number]	k4	20
115	[number]	k4	115
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
23,5	[number]	k4	23,5
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
28,5	[number]	k4	28,5
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
tvořili	tvořit	k5eAaImAgMnP	tvořit
38,2	[number]	k4	38,2
%	%	kIx~	%
lidé	člověk	k1gMnPc1	člověk
mladší	mladý	k2eAgMnPc1d2	mladší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
29,3	[number]	k4	29,3
%	%	kIx~	%
lidé	člověk	k1gMnPc1	člověk
starší	starý	k2eAgMnPc1d2	starší
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Obrovský	obrovský	k2eAgInSc1d1	obrovský
růst	růst	k1gInSc1	růst
populace	populace	k1gFnSc2	populace
Miami	Miami	k1gNnSc2	Miami
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
migrací	migrace	k1gFnSc7	migrace
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
imigrací	imigrace	k1gFnPc2	imigrace
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
především	především	k6eAd1	především
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
spíše	spíše	k9	spíše
za	za	k7c4	za
multikulturní	multikulturní	k2eAgNnSc4d1	multikulturní
<g/>
,	,	kIx,	,
než	než	k8xS	než
za	za	k7c4	za
místo	místo	k1gNnSc4	místo
smývání	smývání	k1gNnSc2	smývání
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
entitami	entita	k1gFnPc7	entita
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
kultura	kultura	k1gFnSc1	kultura
města	město	k1gNnSc2	město
velmi	velmi	k6eAd1	velmi
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
velikou	veliký	k2eAgFnSc7d1	veliká
populací	populace	k1gFnSc7	populace
Hispánců	Hispánec	k1gMnPc2	Hispánec
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
američanů	američan	k1gMnPc2	američan
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
a	a	k8xC	a
černochů	černoch	k1gMnPc2	černoch
<g/>
,	,	kIx,	,
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
karibských	karibský	k2eAgInPc2d1	karibský
ostrovů	ostrov	k1gInPc2	ostrov
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Jamajka	Jamajka	k1gFnSc1	Jamajka
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc1	Haiti
nebo	nebo	k8xC	nebo
Bahamy	Bahamy	k1gFnPc1	Bahamy
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnPc1d1	státní
školy	škola	k1gFnPc1	škola
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
školního	školní	k2eAgInSc2d1	školní
obvodu	obvod	k1gInSc2	obvod
Miami-Dade	Miami-Dad	k1gInSc5	Miami-Dad
County	Counta	k1gFnPc4	Counta
Public	publicum	k1gNnPc2	publicum
Schools	Schoolsa	k1gFnPc2	Schoolsa
(	(	kIx(	(
<g/>
M-DCPS	M-DCPS	k1gFnSc1	M-DCPS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
obvodem	obvod	k1gInSc7	obvod
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
a	a	k8xC	a
čtvrtým	čtvrtá	k1gFnPc3	čtvrtá
největším	veliký	k2eAgFnPc3d3	veliký
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
studovalo	studovat	k5eAaImAgNnS	studovat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
obvodu	obvod	k1gInSc6	obvod
353	[number]	k4	353
283	[number]	k4	283
studentů	student	k1gMnPc2	student
v	v	k7c6	v
celkem	celkem	k6eAd1	celkem
378	[number]	k4	378
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
60	[number]	k4	60
%	%	kIx~	%
studentů	student	k1gMnPc2	student
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
tvořili	tvořit	k5eAaImAgMnP	tvořit
Hispánci	Hispánek	k1gMnPc1	Hispánek
<g/>
,	,	kIx,	,
28	[number]	k4	28
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
<g/>
,	,	kIx,	,
10	[number]	k4	10
%	%	kIx~	%
běloši	běloch	k1gMnPc1	běloch
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
Hispánci	Hispánec	k1gMnPc7	Hispánec
<g/>
,	,	kIx,	,
a	a	k8xC	a
zbylá	zbylý	k2eAgFnSc1d1	zbylá
2	[number]	k4	2
%	%	kIx~	%
připadají	připadat	k5eAaPmIp3nP	připadat
na	na	k7c4	na
ostatní	ostatní	k2eAgFnPc4d1	ostatní
rasy	rasa	k1gFnPc4	rasa
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Demografie	demografie	k1gFnSc2	demografie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Design	design	k1gInSc1	design
and	and	k?	and
Architecture	Architectur	k1gMnSc5	Architectur
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
MAST	mast	k1gFnSc1	mast
Academy	Academa	k1gFnSc2	Academa
<g/>
,	,	kIx,	,
Coral	Coral	k1gMnSc1	Coral
Reef	Reef	k1gMnSc1	Reef
High	High	k1gMnSc1	High
School	School	k1gInSc4	School
<g/>
,	,	kIx,	,
Miami	Miami	k1gNnSc4	Miami
Palmetto	Palmetto	k1gNnSc1	Palmetto
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
nebo	nebo	k8xC	nebo
New	New	k1gFnPc2	New
World	Worlda	k1gFnPc2	Worlda
School	School	k1gInSc1	School
of	of	k?	of
the	the	k?	the
Arts	Arts	k1gInSc1	Arts
<g/>
.	.	kIx.	.
</s>
<s>
M-DCPS	M-DCPS	k?	M-DCPS
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
mezi	mezi	k7c4	mezi
jedny	jeden	k4xCgInPc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
státních	státní	k2eAgFnPc2d1	státní
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
dvojjazyčné	dvojjazyčný	k2eAgNnSc1d1	dvojjazyčné
vyučování	vyučování	k1gNnSc1	vyučování
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
<g/>
,	,	kIx,	,
haitské	haitský	k2eAgFnSc6d1	Haitská
kreolštině	kreolština	k1gFnSc6	kreolština
nebo	nebo	k8xC	nebo
mandarínštině	mandarínština	k1gFnSc6	mandarínština
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
mnoha	mnoho	k4c2	mnoho
prestižních	prestižní	k2eAgFnPc2d1	prestižní
římskokatolických	římskokatolický	k2eAgFnPc2d1	Římskokatolická
<g/>
,	,	kIx,	,
židovských	židovský	k2eAgFnPc2d1	židovská
a	a	k8xC	a
non-denominačních	nonenominační	k2eAgFnPc2d1	non-denominační
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
miamská	miamský	k2eAgFnSc1d1	Miamská
spravuje	spravovat	k5eAaImIp3nS	spravovat
katolické	katolický	k2eAgFnSc2d1	katolická
soukromé	soukromý	k2eAgFnSc2d1	soukromá
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
třeba	třeba	k6eAd1	třeba
Our	Our	k1gFnSc1	Our
Lady	lady	k1gFnSc1	lady
of	of	k?	of
Lourdes	Lourdes	k1gInSc4	Lourdes
Academy	Academa	k1gFnSc2	Academa
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Hugh	Hugh	k1gInSc4	Hugh
Catholic	Catholice	k1gFnPc2	Catholice
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Agatha	Agatha	k1gFnSc1	Agatha
Catholic	Catholice	k1gFnPc2	Catholice
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Theresa	Theresa	k1gFnSc1	Theresa
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Salle	Salle	k1gFnSc2	Salle
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Monsignor	Monsignor	k1gMnSc1	Monsignor
Edward	Edward	k1gMnSc1	Edward
Pace	Paka	k1gFnSc6	Paka
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Carrollton	Carrollton	k1gInSc1	Carrollton
School	School	k1gInSc1	School
of	of	k?	of
the	the	k?	the
Sacred	Sacred	k1gInSc1	Sacred
Heart	Hearta	k1gFnPc2	Hearta
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Columbus	Columbus	k1gInSc1	Columbus
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Archbishop	Archbishop	k1gInSc1	Archbishop
Curley-Notre	Curley-Notr	k1gInSc5	Curley-Notr
Dame	Dame	k1gFnSc1	Dame
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Brendan	Brendan	k1gInSc1	Brendan
High	High	k1gInSc1	High
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
základních	základní	k2eAgFnPc2d1	základní
a	a	k8xC	a
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
non-denominační	nonenominační	k2eAgFnPc4d1	non-denominační
soukromé	soukromý	k2eAgFnPc4d1	soukromá
školy	škola	k1gFnPc4	škola
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
patří	patřit	k5eAaImIp3nS	patřit
Ransom	Ransom	k1gInSc1	Ransom
Everglades	Evergladesa	k1gFnPc2	Evergladesa
<g/>
,	,	kIx,	,
Gulliver	Gullivra	k1gFnPc2	Gullivra
Preparatory	Preparator	k1gMnPc4	Preparator
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
Miami	Miami	k1gNnSc2	Miami
Country	country	k2eAgFnSc2d1	country
Day	Day	k1gFnSc2	Day
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xS	jako
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
soukromé	soukromý	k2eAgFnPc1d1	soukromá
školy	škola	k1gFnPc1	škola
v	v	k7c6	v
odlehlejších	odlehlý	k2eAgFnPc6d2	odlehlejší
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
Belen	Belen	k2eAgInSc4d1	Belen
Jesuit	Jesuit	k1gInSc4	Jesuit
Preparatory	Preparator	k1gInPc7	Preparator
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Scheck	Schecka	k1gFnPc2	Schecka
Hillel	Hillel	k1gMnSc1	Hillel
Community	Communita	k1gFnSc2	Communita
Day	Day	k1gMnSc1	Day
School	School	k1gInSc1	School
a	a	k8xC	a
Dade	Dade	k1gInSc1	Dade
Christian	Christian	k1gMnSc1	Christian
School	School	k1gInSc1	School
<g/>
.	.	kIx.	.
</s>
<s>
Univerzity	univerzita	k1gFnPc1	univerzita
a	a	k8xC	a
koleje	kolej	k1gFnPc1	kolej
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Barry	Barr	k1gInPc1	Barr
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
Carlos	Carlos	k1gMnSc1	Carlos
Albizu	Albiza	k1gFnSc4	Albiza
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
Florida	Florida	k1gFnSc1	Florida
International	International	k1gFnSc2	International
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
FIU	FIU	kA	FIU
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
)	)	kIx)	)
Florida	Florida	k1gFnSc1	Florida
Memorial	Memorial	k1gInSc4	Memorial
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
Johnson	Johnson	k1gMnSc1	Johnson
and	and	k?	and
Wales	Wales	k1gInSc1	Wales
<g />
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc1	universita
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
Fortis	Fortis	k1gFnSc1	Fortis
College	College	k1gFnSc1	College
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
Keiser	Keiser	k1gMnSc1	Keiser
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
Manchester	Manchester	k1gInSc1	Manchester
Business	business	k1gInSc1	business
School	School	k1gInSc1	School
(	(	kIx(	(
<g/>
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
<g/>
)	)	kIx)	)
Miami	Miami	k1gNnSc7	Miami
Dade	Dad	k1gFnSc2	Dad
College	Colleg	k1gFnSc2	Colleg
(	(	kIx(	(
<g/>
veřejná	veřejný	k2eAgNnPc4d1	veřejné
<g/>
)	)	kIx)	)
Miami	Miami	k1gNnPc4	Miami
International	International	k1gFnSc2	International
University	universita	k1gFnSc2	universita
of	of	k?	of
Art	Art	k1gFnSc2	Art
&	&	k?	&
Design	design	k1gInSc1	design
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
St.	st.	kA	st.
Thomas	Thomas	k1gMnSc1	Thomas
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
Talmudic	Talmudic	k1gMnSc1	Talmudic
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
University	universita	k1gFnPc1	universita
of	of	k?	of
Miami	Miami	k1gNnSc1	Miami
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
Městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
Miami-Dade	Miami-Dad	k1gInSc5	Miami-Dad
Transit	transit	k1gInSc4	transit
a	a	k8xC	a
South	South	k1gInSc4	South
Florida	Florida	k1gFnSc1	Florida
Regional	Regional	k1gFnSc1	Regional
Transportation	Transportation	k1gInSc4	Transportation
Authority	Authorita	k1gFnSc2	Authorita
<g/>
.	.	kIx.	.
</s>
<s>
Dopravními	dopravní	k2eAgInPc7d1	dopravní
prostředky	prostředek	k1gInPc7	prostředek
jsou	být	k5eAaImIp3nP	být
regionální	regionální	k2eAgInPc1d1	regionální
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
visuté	visutý	k2eAgInPc1d1	visutý
osobní	osobní	k2eAgInPc1d1	osobní
dopravníky	dopravník	k1gInPc1	dopravník
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgFnPc2d1	poslední
statistik	statistika	k1gFnPc2	statistika
se	s	k7c7	s
12,2	[number]	k4	12,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
dopravuje	dopravovat	k5eAaImIp3nS	dopravovat
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
právě	právě	k6eAd1	právě
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Metrorail	Metrorait	k5eAaPmAgMnS	Metrorait
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
miamské	miamský	k2eAgNnSc1d1	miamské
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
36	[number]	k4	36
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
22	[number]	k4	22
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
západních	západní	k2eAgFnPc2d1	západní
předměstí	předměstí	k1gNnPc2	předměstí
Medley	Medlea	k1gFnSc2	Medlea
a	a	k8xC	a
Hialeah	Hialeah	k1gInSc4	Hialeah
přes	přes	k7c4	přes
Civic	Civice	k1gFnPc2	Civice
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Brickell	Brickell	k1gMnSc1	Brickell
<g/>
,	,	kIx,	,
Coconut	Coconut	k1gMnSc1	Coconut
Grove	Groev	k1gFnSc2	Groev
<g/>
,	,	kIx,	,
Coral	Coral	k1gMnSc1	Coral
Gables	Gables	k1gMnSc1	Gables
<g/>
,	,	kIx,	,
South	South	k1gMnSc1	South
Miami	Miami	k1gNnSc2	Miami
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
okrajové	okrajový	k2eAgFnSc6d1	okrajová
čtvrti	čtvrt	k1gFnSc6	čtvrt
Dadeland	Dadelanda	k1gFnPc2	Dadelanda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
otevření	otevření	k1gNnSc1	otevření
přímého	přímý	k2eAgNnSc2d1	přímé
spojení	spojení	k1gNnSc2	spojení
metra	metro	k1gNnSc2	metro
až	až	k8xS	až
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
Miami	Miami	k1gNnSc2	Miami
International	International	k1gFnSc2	International
Airport	Airport	k1gInSc1	Airport
<g/>
.	.	kIx.	.
</s>
<s>
Metromover	Metromover	k1gInSc1	Metromover
je	být	k5eAaImIp3nS	být
dopravní	dopravní	k2eAgInSc4d1	dopravní
systém	systém	k1gInSc4	systém
využívající	využívající	k2eAgInPc4d1	využívající
visuté	visutý	k2eAgInPc4d1	visutý
osobní	osobní	k2eAgInPc4d1	osobní
dopravníky	dopravník	k1gInPc4	dopravník
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
20	[number]	k4	20
stanic	stanice	k1gFnPc2	stanice
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
různých	různý	k2eAgFnPc6d1	různá
trasách	trasa	k1gFnPc6	trasa
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Miami	Miami	k1gNnSc2	Miami
a	a	k8xC	a
Brickellu	Brickell	k1gInSc2	Brickell
<g/>
.	.	kIx.	.
</s>
<s>
Tri-Rail	Tri-Rail	k1gInSc1	Tri-Rail
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc4	systém
regionálních	regionální	k2eAgInPc2d1	regionální
vlaků	vlak	k1gInPc2	vlak
spravovaný	spravovaný	k2eAgInSc1d1	spravovaný
federálním	federální	k2eAgInSc7d1	federální
úřadem	úřad	k1gInSc7	úřad
South	Southa	k1gFnPc2	Southa
Florida	Florida	k1gFnSc1	Florida
Regional	Regional	k1gFnSc1	Regional
Transportation	Transportation	k1gInSc4	Transportation
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
SFRTA	SFRTA	kA	SFRTA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
od	od	k7c2	od
miamského	miamský	k2eAgNnSc2d1	miamské
letiště	letiště	k1gNnSc2	letiště
do	do	k7c2	do
West	Westa	k1gFnPc2	Westa
Palm	Palm	k1gMnSc1	Palm
Beach	Beach	k1gMnSc1	Beach
a	a	k8xC	a
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
18	[number]	k4	18
zastávek	zastávka	k1gFnPc2	zastávka
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
Miami-Dade	Miami-Dad	k1gInSc5	Miami-Dad
<g/>
,	,	kIx,	,
Broward	Broward	k1gMnSc1	Broward
a	a	k8xC	a
Palm	Palm	k1gMnSc1	Palm
Beach	Beach	k1gMnSc1	Beach
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
International	International	k1gMnPc2	International
Airport	Airport	k1gInSc4	Airport
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
nepřičleněném	přičleněný	k2eNgNnSc6d1	přičleněný
území	území	k1gNnSc6	území
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
primární	primární	k2eAgNnSc1d1	primární
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejvytíženějších	vytížený	k2eAgNnPc2d3	nejvytíženější
letišť	letiště	k1gNnPc2	letiště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
odbaví	odbavit	k5eAaPmIp3nS	odbavit
přes	přes	k7c4	přes
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Identifikační	identifikační	k2eAgInSc1d1	identifikační
kód	kód	k1gInSc1	kód
letiště	letiště	k1gNnSc2	letiště
podle	podle	k7c2	podle
IATA	IATA	kA	IATA
je	být	k5eAaImIp3nS	být
MIA	MIA	kA	MIA
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
ICAO	ICAO	kA	ICAO
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
KMIA	KMIA	kA	KMIA
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
letecké	letecký	k2eAgInPc4d1	letecký
uzly	uzel	k1gInPc4	uzel
společnosti	společnost	k1gFnSc2	společnost
American	Americana	k1gFnPc2	Americana
Airlines	Airlines	k1gInSc1	Airlines
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
letových	letový	k2eAgMnPc2d1	letový
dopravců	dopravce	k1gMnPc2	dopravce
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
MIA	MIA	kA	MIA
je	být	k5eAaImIp3nS	být
nejvytíženější	vytížený	k2eAgNnSc4d3	nejvytíženější
letiště	letiště	k1gNnSc4	letiště
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
cestující	cestující	k1gMnPc4	cestující
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
sedmá	sedmý	k4xOgFnSc1	sedmý
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
podle	podle	k7c2	podle
stejných	stejný	k2eAgFnPc2d1	stejná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
letištních	letištní	k2eAgInPc2d1	letištní
spojů	spoj	k1gInPc2	spoj
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nonstop	nonstop	k2eAgNnPc7d1	nonstop
lety	léto	k1gNnPc7	léto
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
světových	světový	k2eAgNnPc2d1	světové
měst	město	k1gNnPc2	město
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Eventuálně	eventuálně	k6eAd1	eventuálně
blízko	blízko	k6eAd1	blízko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Fort	Fort	k?	Fort
Lauderdale-Hollywood	Lauderdale-Hollywood	k1gInSc1	Lauderdale-Hollywood
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc1	Airport
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
také	také	k9	také
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Opa-Locka	Opa-Locka	k1gFnSc1	Opa-Locka
Airport	Airport	k1gInSc1	Airport
a	a	k8xC	a
Kendall-Tamiami	Kendall-Tamia	k1gFnPc7	Kendall-Tamia
Airport	Airport	k1gInSc1	Airport
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c6	na
území	území	k1gNnSc6	území
mimo	mimo	k7c4	mimo
aglomeraci	aglomerace	k1gFnSc4	aglomerace
<g/>
,	,	kIx,	,
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
především	především	k6eAd1	především
leteckou	letecký	k2eAgFnSc4d1	letecká
dopravu	doprava	k1gFnSc4	doprava
uvnitř	uvnitř	k7c2	uvnitř
oblasti	oblast	k1gFnSc2	oblast
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Miami	Miami	k1gNnSc1	Miami
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
přístavů	přístav	k1gInPc2	přístav
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
přístavu	přístav	k1gInSc2	přístav
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
přístav	přístav	k1gInSc4	přístav
výletních	výletní	k2eAgFnPc2d1	výletní
lodí	loď	k1gFnPc2	loď
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
Cruise	Cruise	k1gFnPc1	Cruise
Capital	Capital	k1gMnSc1	Capital
of	of	k?	of
the	the	k?	the
World	World	k1gMnSc1	World
nebo	nebo	k8xC	nebo
Cargo	Cargo	k1gMnSc1	Cargo
Gateway	Gatewaa	k1gFnSc2	Gatewaa
of	of	k?	of
the	the	k?	the
Americas	Americas	k1gInSc1	Americas
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
přispívá	přispívat	k5eAaImIp3nS	přispívat
do	do	k7c2	do
ekonomiky	ekonomika	k1gFnSc2	ekonomika
Jižní	jižní	k2eAgFnSc2d1	jižní
Floridy	Florida	k1gFnSc2	Florida
přes	přes	k7c4	přes
17	[number]	k4	17
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
přístav	přístav	k1gInSc1	přístav
sloužil	sloužit	k5eAaImAgInS	sloužit
4	[number]	k4	4
110	[number]	k4	110
100	[number]	k4	100
cestujícím	cestující	k1gFnPc3	cestující
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
Port	port	k1gInSc1	port
of	of	k?	of
Miami	Miami	k1gNnSc2	Miami
také	také	k9	také
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nezatíženějších	zatížený	k2eNgInPc2d2	zatížený
nákladních	nákladní	k2eAgInPc2d1	nákladní
přístavů	přístav	k1gInPc2	přístav
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
naloženo	naložit	k5eAaPmNgNnS	naložit
<g/>
/	/	kIx~	/
<g/>
vyloženo	vyložit	k5eAaPmNgNnS	vyložit
6	[number]	k4	6
831	[number]	k4	831
496	[number]	k4	496
tun	tuna	k1gFnPc2	tuna
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
severoamerickými	severoamerický	k2eAgInPc7d1	severoamerický
přístavy	přístav	k1gInPc7	přístav
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgInS	umístit
druhý	druhý	k4xOgInSc4	druhý
co	co	k8xS	co
do	do	k7c2	do
hmotnosti	hmotnost	k1gFnSc2	hmotnost
nákladu	náklad	k1gInSc2	náklad
přepraveného	přepravený	k2eAgInSc2d1	přepravený
z	z	k7c2	z
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
přístavu	přístav	k1gInSc2	přístav
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
počet	počet	k1gInSc1	počet
osobních	osobní	k2eAgInPc2d1	osobní
terminálů	terminál	k1gInPc2	terminál
sedm	sedm	k4xCc4	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
nákladu	náklad	k1gInSc2	náklad
je	být	k5eAaImIp3nS	být
přijímáno	přijímat	k5eAaImNgNnS	přijímat
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
odesíláno	odesílán	k2eAgNnSc1d1	odesíláno
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
do	do	k7c2	do
Hondurasu	Honduras	k1gInSc2	Honduras
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
sídel	sídlo	k1gNnPc2	sídlo
výletních	výletní	k2eAgFnPc2d1	výletní
námořních	námořní	k2eAgFnPc2d1	námořní
společnosti	společnost	k1gFnPc4	společnost
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těchto	tento	k3xDgFnPc2	tento
<g/>
:	:	kIx,	:
Carnival	Carnival	k1gFnSc2	Carnival
Cruise	Cruise	k1gFnSc2	Cruise
Lines	Linesa	k1gFnPc2	Linesa
<g/>
,	,	kIx,	,
Celebrity	celebrita	k1gFnPc1	celebrita
Cruises	Cruises	k1gMnSc1	Cruises
<g/>
,	,	kIx,	,
Costa	Costa	k1gMnSc1	Costa
Cruises	Cruises	k1gMnSc1	Cruises
<g/>
,	,	kIx,	,
Crystal	Crystal	k1gMnSc1	Crystal
Cruises	Cruises	k1gMnSc1	Cruises
<g/>
,	,	kIx,	,
Norwegian	Norwegian	k1gMnSc1	Norwegian
Cruise	Cruise	k1gFnSc2	Cruise
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
Oceania	Oceanium	k1gNnSc2	Oceanium
Cruises	Cruisesa	k1gFnPc2	Cruisesa
<g/>
,	,	kIx,	,
Royal	Royal	k1gMnSc1	Royal
Caribbean	Caribbean	k1gMnSc1	Caribbean
International	International	k1gMnSc1	International
a	a	k8xC	a
Windjammer	Windjammer	k1gMnSc1	Windjammer
Barefoot	Barefoot	k1gMnSc1	Barefoot
Cruises	Cruises	k1gMnSc1	Cruises
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
začala	začít	k5eAaPmAgFnS	začít
stavba	stavba	k1gFnSc1	stavba
tunelu	tunel	k1gInSc2	tunel
Miami	Miami	k1gNnSc2	Miami
port	porta	k1gFnPc2	porta
tunnel	tunnela	k1gFnPc2	tunnela
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
miliarda	miliarda	k4xCgFnSc1	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
partnerských	partnerský	k2eAgNnPc2d1	partnerské
měst	město	k1gNnPc2	město
Miami	Miami	k1gNnSc2	Miami
<g/>
:	:	kIx,	:
</s>
