<s>
Reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
taková	takový	k3xDgNnPc4	takový
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
můžeme	moct	k5eAaImIp1nP	moct
jednoznačně	jednoznačně	k6eAd1	jednoznačně
přiřadit	přiřadit	k5eAaPmF	přiřadit
body	bod	k1gInPc4	bod
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
přímky	přímka	k1gFnSc2	přímka
(	(	kIx(	(
<g/>
číselné	číselný	k2eAgFnSc2d1	číselná
osy	osa	k1gFnSc2	osa
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
popisovala	popisovat	k5eAaImAgNnP	popisovat
"	"	kIx"	"
<g/>
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
"	"	kIx"	"
od	od	k7c2	od
nějakého	nějaký	k3yIgInSc2	nějaký
vybraného	vybraný	k2eAgInSc2d1	vybraný
bodu	bod	k1gInSc2	bod
(	(	kIx(	(
<g/>
nuly	nula	k1gFnSc2	nula
<g/>
)	)	kIx)	)
na	na	k7c6	na
takové	takový	k3xDgFnSc6	takový
přímce	přímka	k1gFnSc6	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nula	nula	k1gFnSc1	nula
pak	pak	k6eAd1	pak
přirozeně	přirozeně	k6eAd1	přirozeně
dělí	dělit	k5eAaImIp3nP	dělit
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
na	na	k7c4	na
kladná	kladný	k2eAgNnPc4d1	kladné
a	a	k8xC	a
záporná	záporný	k2eAgNnPc4d1	záporné
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
představy	představa	k1gFnSc2	představa
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
jsou	být	k5eAaImIp3nP	být
desetinné	desetinný	k2eAgInPc1d1	desetinný
rozvoje	rozvoj	k1gInPc1	rozvoj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
konečné	konečný	k2eAgNnSc4d1	konečné
i	i	k8xC	i
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgMnSc1d3	Nejběžnější
matematicky	matematicky	k6eAd1	matematicky
přesný	přesný	k2eAgInSc4d1	přesný
způsob	způsob	k1gInSc4	způsob
definice	definice	k1gFnSc2	definice
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
Dedekindovy	Dedekindův	k2eAgInPc4d1	Dedekindův
řezy	řez	k1gInPc4	řez
<g/>
.	.	kIx.	.
</s>
<s>
Reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
algebraickém	algebraický	k2eAgInSc6d1	algebraický
smyslu	smysl	k1gInSc6	smysl
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
speciálně	speciálně	k6eAd1	speciálně
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
můžeme	moct	k5eAaImIp1nP	moct
sčítat	sčítat	k5eAaImF	sčítat
<g/>
,	,	kIx,	,
odčítat	odčítat	k5eAaImF	odčítat
<g/>
,	,	kIx,	,
násobit	násobit	k5eAaImF	násobit
a	a	k8xC	a
dělit	dělit	k5eAaImF	dělit
a	a	k8xC	a
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dělení	dělení	k1gNnSc2	dělení
nulou	nula	k1gFnSc7	nula
nám	my	k3xPp1nPc3	my
vždy	vždy	k6eAd1	vždy
vyjde	vyjít	k5eAaPmIp3nS	vyjít
nějaké	nějaký	k3yIgNnSc1	nějaký
reálné	reálný	k2eAgNnSc1d1	reálné
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Dělíme	dělit	k5eAaImIp1nP	dělit
je	on	k3xPp3gNnSc4	on
na	na	k7c4	na
racionální	racionální	k2eAgNnPc4d1	racionální
(	(	kIx(	(
<g/>
vyjádřitelná	vyjádřitelný	k2eAgNnPc4d1	vyjádřitelné
zlomkem	zlomek	k1gInSc7	zlomek
<g/>
)	)	kIx)	)
a	a	k8xC	a
iracionální	iracionální	k2eAgMnPc1d1	iracionální
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
algebraická	algebraický	k2eAgNnPc4d1	algebraické
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
jako	jako	k8xC	jako
kořeny	kořen	k1gInPc4	kořen
mnohočlenu	mnohočlen	k1gInSc2	mnohočlen
s	s	k7c7	s
celočíselnými	celočíselný	k2eAgInPc7d1	celočíselný
koeficienty	koeficient	k1gInPc7	koeficient
<g/>
)	)	kIx)	)
a	a	k8xC	a
transcendentní	transcendentní	k2eAgMnPc4d1	transcendentní
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
ústřední	ústřední	k2eAgInSc4d1	ústřední
objekt	objekt	k1gInSc4	objekt
zkoumání	zkoumání	k1gNnSc2	zkoumání
reálné	reálný	k2eAgFnSc2d1	reálná
analýzy	analýza	k1gFnSc2	analýza
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Množina	množina	k1gFnSc1
všech	všecek	k3xTgNnPc2
reálných	reálný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
R	R	kA
nebo	nebo	k8xC
ℝ	ℝ	k?
Zápis	zápis	k1gInSc1
ℝ	ℝ	k?
označuje	označovat	k5eAaImIp3nS
n-rozměrný	n-rozměrný	k2eAgInSc1d1
vektorový	vektorový	k2eAgInSc1d1
prostor	prostor	k1gInSc1
reálných	reálný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
při	pře	k1gFnSc3	pře
označení	označení	k1gNnSc2	označení
nějakého	nějaký	k3yIgInSc2	nějaký
matematického	matematický	k2eAgInSc2d1	matematický
objektu	objekt	k1gInSc2	objekt
přívlastek	přívlastek	k1gInSc1	přívlastek
reálný	reálný	k2eAgInSc1d1	reálný
<g/>
,	,	kIx,	,
myslí	myslet	k5eAaImIp3nS	myslet
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
objektem	objekt	k1gInSc7	objekt
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
tělese	těleso	k1gNnSc6	těleso
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
reálná	reálný	k2eAgFnSc1d1	reálná
matice	matice	k1gFnSc1	matice
<g/>
,	,	kIx,	,
reálný	reálný	k2eAgInSc1d1	reálný
polynom	polynom	k1gInSc1	polynom
či	či	k8xC	či
reálná	reálný	k2eAgFnSc1d1	reálná
Lieova	Lieův	k2eAgFnSc1d1	Lieova
algebra	algebra	k1gFnSc1	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
reálné	reálný	k2eAgNnSc4d1	reálné
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jeho	jeho	k3xOp3gFnSc1	jeho
absolutní	absolutní	k2eAgFnSc1d1	absolutní
hodnota	hodnota	k1gFnSc1	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
a	a	k8xC	a
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
nezáporné	záporný	k2eNgFnPc4d1	nezáporná
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-a	-a	k?	-a
<g/>
}	}	kIx)	}
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
záporné	záporný	k2eAgInPc4d1	záporný
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
geometrický	geometrický	k2eAgInSc4d1	geometrický
smysl	smysl	k1gInSc4	smysl
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
obrazu	obraz	k1gInSc2	obraz
čísla	číslo	k1gNnSc2	číslo
od	od	k7c2	od
obrazu	obraz	k1gInSc2	obraz
nuly	nula	k1gFnSc2	nula
na	na	k7c6	na
číselné	číselný	k2eAgFnSc6d1	číselná
ose	osa	k1gFnSc6	osa
<g/>
.	.	kIx.	.
</s>
<s>
Zlomky	zlomek	k1gInPc1	zlomek
byly	být	k5eAaImAgInP	být
používány	používán	k2eAgMnPc4d1	používán
Egypťany	Egypťan	k1gMnPc4	Egypťan
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
si	se	k3xPyFc3	se
řečtí	řecký	k2eAgMnPc1d1	řecký
matematici	matematik	k1gMnPc1	matematik
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Pythagorem	Pythagoras	k1gMnSc7	Pythagoras
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
potřebu	potřeba	k1gFnSc4	potřeba
iracionálních	iracionální	k2eAgNnPc2d1	iracionální
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Záporná	záporný	k2eAgNnPc1d1	záporné
čísla	číslo	k1gNnPc1	číslo
byla	být	k5eAaImAgNnP	být
objevena	objevit	k5eAaPmNgNnP	objevit
indickými	indický	k2eAgFnPc7d1	indická
matematiky	matematik	k1gMnPc4	matematik
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
600	[number]	k4	600
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
znovuobjevena	znovuobjeven	k2eAgFnSc1d1	znovuobjeven
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nebyla	být	k5eNaImAgFnS	být
obecně	obecně	k6eAd1	obecně
přijata	přijmout	k5eAaPmNgFnS	přijmout
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
kalkulu	kalkul	k1gInSc2	kalkul
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
znamenal	znamenat	k5eAaImAgInS	znamenat
využívání	využívání	k1gNnSc4	využívání
celé	celý	k2eAgFnSc2d1	celá
množiny	množina	k1gFnSc2	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gNnSc2	jejich
přesného	přesný	k2eAgNnSc2d1	přesné
zavedení	zavedení	k1gNnSc2	zavedení
<g/>
.	.	kIx.	.
</s>
<s>
Rigorózně	rigorózně	k6eAd1	rigorózně
definoval	definovat	k5eAaBmAgInS	definovat
reálná	reálný	k2eAgNnPc4d1	reálné
čísla	číslo	k1gNnPc4	číslo
poprvé	poprvé	k6eAd1	poprvé
Georg	Georg	k1gMnSc1	Georg
Cantor	Cantor	k1gMnSc1	Cantor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Axiomaticky	axiomaticky	k6eAd1	axiomaticky
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
zavedena	zaveden	k2eAgNnPc1d1	zavedeno
jako	jako	k8xC	jako
úplně	úplně	k6eAd1	úplně
uspořádané	uspořádaný	k2eAgNnSc1d1	uspořádané
těleso	těleso	k1gNnSc1	těleso
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
neprázdná	prázdný	k2eNgFnSc1d1	neprázdná
shora	shora	k6eAd1	shora
omezená	omezený	k2eAgFnSc1d1	omezená
podmnožina	podmnožina	k1gFnSc1	podmnožina
R	R	kA	R
má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgFnSc4d3	nejmenší
horní	horní	k2eAgFnSc4d1	horní
závoru	závora	k1gFnSc4	závora
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
supremum	supremum	k1gInSc1	supremum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Dedekindova	Dedekindův	k2eAgFnSc1d1	Dedekindův
úplnost	úplnost	k1gFnSc1	úplnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
konstrukce	konstrukce	k1gFnSc2	konstrukce
je	být	k5eAaImIp3nS	být
zúplnění	zúplnění	k1gNnSc4	zúplnění
množiny	množina	k1gFnSc2	množina
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
x	x	k?	x
−	−	k?	−
y	y	k?	y
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
x-y	x	k?	x-y
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc4d1	hlavní
důvod	důvod	k1gInSc4	důvod
zavedení	zavedení	k1gNnSc4	zavedení
množiny	množina	k1gFnSc2	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
vlastnost	vlastnost	k1gFnSc1	vlastnost
je	být	k5eAaImIp3nS	být
úplnost	úplnost	k1gFnSc4	úplnost
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
úplného	úplný	k2eAgInSc2d1	úplný
metrického	metrický	k2eAgInSc2d1	metrický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
vlastnosti	vlastnost	k1gFnSc3	vlastnost
každá	každý	k3xTgFnSc1	každý
reálná	reálný	k2eAgFnSc1d1	reálná
cauchyovská	cauchyovský	k2eAgFnSc1d1	cauchyovský
posloupnost	posloupnost	k1gFnSc1	posloupnost
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
má	mít	k5eAaImIp3nS	mít
limitu	limit	k1gInSc2	limit
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
reálné	reálný	k2eAgFnPc1d1	reálná
posloupnosti	posloupnost	k1gFnPc1	posloupnost
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
libovolně	libovolně	k6eAd1	libovolně
blízko	blízko	k6eAd1	blízko
sobě	se	k3xPyFc3	se
jak	jak	k6eAd1	jak
posloupnost	posloupnost	k1gFnSc4	posloupnost
postupuje	postupovat	k5eAaImIp3nS	postupovat
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
limitu	limita	k1gFnSc4	limita
v	v	k7c6	v
množině	množina	k1gFnSc6	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
nespočetná	spočetný	k2eNgFnSc1d1	nespočetná
<g/>
,	,	kIx,	,
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
mnohem	mnohem	k6eAd1	mnohem
<g/>
"	"	kIx"	"
více	hodně	k6eAd2	hodně
než	než	k8xS	než
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
obě	dva	k4xCgFnPc1	dva
množiny	množina	k1gFnPc1	množina
jsou	být	k5eAaImIp3nP	být
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
kardinalita	kardinalita	k1gFnSc1	kardinalita
množiny	množina	k1gFnSc2	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
kardinalita	kardinalita	k1gFnSc1	kardinalita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
}}	}}	k?	}}
,	,	kIx,	,
množiny	množina	k1gFnSc2	množina
všech	všecek	k3xTgFnPc2	všecek
podmnožin	podmnožina	k1gFnPc2	podmnožina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
podmnožina	podmnožina	k1gFnSc1	podmnožina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
s	s	k7c7	s
kardinalitou	kardinalita	k1gFnSc7	kardinalita
mezi	mezi	k7c7	mezi
kardinalitami	kardinalita	k1gFnPc7	kardinalita
množin	množina	k1gFnPc2	množina
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xC	jako
hypotéza	hypotéza	k1gFnSc1	hypotéza
kontinua	kontinuum	k1gNnSc2	kontinuum
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
předpokladů	předpoklad	k1gInPc2	předpoklad
bezespornosti	bezespornost	k1gFnSc2	bezespornost
běžně	běžně	k6eAd1	běžně
používané	používaný	k2eAgFnSc2d1	používaná
Zermelo-Fraenklovy	Zermelo-Fraenklův	k2eAgFnSc2d1	Zermelo-Fraenklův
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
dokázána	dokázat	k5eAaPmNgFnS	dokázat
ani	ani	k8xC	ani
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
uvnitř	uvnitř	k7c2	uvnitř
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
tvoří	tvořit	k5eAaImIp3nS	tvořit
metrický	metrický	k2eAgInSc1d1	metrický
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
(	(	kIx(	(
<g/>
metrika	metrika	k1gFnSc1	metrika
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
x	x	k?	x
a	a	k8xC	a
y	y	k?	y
je	být	k5eAaImIp3nS	být
definovaná	definovaný	k2eAgFnSc1d1	definovaná
pomocí	pomocí	k7c2	pomocí
absolutní	absolutní	k2eAgFnSc2d1	absolutní
hodnoty	hodnota	k1gFnSc2	hodnota
rozdílu	rozdíl	k1gInSc6	rozdíl
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
souvislý	souvislý	k2eAgInSc1d1	souvislý
i	i	k9	i
jednoduše	jednoduše	k6eAd1	jednoduše
souvislý	souvislý	k2eAgMnSc1d1	souvislý
<g/>
,	,	kIx,	,
lokálně	lokálně	k6eAd1	lokálně
kompaktní	kompaktní	k2eAgInPc4d1	kompaktní
a	a	k8xC	a
separabilní	separabilní	k2eAgInPc4d1	separabilní
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
kompaktní	kompaktní	k2eAgNnSc1d1	kompaktní
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
těleso	těleso	k1gNnSc1	těleso
má	mít	k5eAaImIp3nS	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
jediný	jediný	k2eAgInSc1d1	jediný
automorfismus	automorfismus	k1gInSc1	automorfismus
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
identitu	identita	k1gFnSc4	identita
(	(	kIx(	(
<g/>
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
kladnost	kladnost	k1gFnSc4	kladnost
a	a	k8xC	a
zápornost	zápornost	k1gFnSc4	zápornost
jsou	být	k5eAaImIp3nP	být
vlastnosti	vlastnost	k1gFnPc1	vlastnost
vnitřně	vnitřně	k6eAd1	vnitřně
definované	definovaný	k2eAgInPc1d1	definovaný
samotnou	samotný	k2eAgFnSc7d1	samotná
strukturou	struktura	k1gFnSc7	struktura
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejpřirozenějším	přirozený	k2eAgNnSc7d3	nejpřirozenější
rozšířením	rozšíření	k1gNnSc7	rozšíření
jsou	být	k5eAaImIp3nP	být
komplexní	komplexní	k2eAgNnPc4d1	komplexní
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
řešení	řešení	k1gNnSc4	řešení
všech	všecek	k3xTgFnPc2	všecek
algebraických	algebraický	k2eAgFnPc2d1	algebraická
rovnic	rovnice	k1gFnPc2	rovnice
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nP	tvořit
algebraický	algebraický	k2eAgInSc4d1	algebraický
uzávěr	uzávěr	k1gInSc4	uzávěr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
však	však	k9	však
nelze	lze	k6eNd1	lze
přirozeným	přirozený	k2eAgInSc7d1	přirozený
způsobem	způsob	k1gInSc7	způsob
uspořádat	uspořádat	k5eAaPmF	uspořádat
<g/>
.	.	kIx.	.
</s>
<s>
Nadtělesem	Nadtěles	k1gInSc7	Nadtěles
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
i	i	k9	i
těleso	těleso	k1gNnSc1	těleso
kvaternionů	kvaternion	k1gInPc2	kvaternion
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
komutativní	komutativní	k2eAgNnSc1d1	komutativní
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
oktoniony	oktonion	k1gInPc1	oktonion
<g/>
,	,	kIx,	,
sedeniony	sedenion	k1gInPc1	sedenion
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
normované	normovaný	k2eAgFnPc1d1	normovaná
reálné	reálný	k2eAgFnPc1d1	reálná
algebry	algebra	k1gFnPc1	algebra
dimenze	dimenze	k1gFnSc2	dimenze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
Cayley-Dicksonovým	Cayley-Dicksonův	k2eAgInSc7d1	Cayley-Dicksonův
procesem	proces	k1gInSc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Samoadjungované	Samoadjungovaný	k2eAgInPc4d1	Samoadjungovaný
(	(	kIx(	(
<g/>
hermiteovské	hermiteovský	k2eAgInPc4d1	hermiteovský
<g/>
)	)	kIx)	)
operátory	operátor	k1gInPc4	operátor
na	na	k7c6	na
Hilbertových	Hilbertův	k2eAgInPc6d1	Hilbertův
prostorech	prostor	k1gInPc6	prostor
zobecňují	zobecňovat	k5eAaImIp3nP	zobecňovat
reálná	reálný	k2eAgNnPc4d1	reálné
čísla	číslo	k1gNnPc4	číslo
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
<g/>
:	:	kIx,	:
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uspořádány	uspořádán	k2eAgInPc1d1	uspořádán
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
totálně	totálně	k6eAd1	totálně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
úplné	úplný	k2eAgFnPc1d1	úplná
<g/>
,	,	kIx,	,
všechna	všechen	k3xTgFnSc1	všechen
jejich	jejich	k3xOp3gMnSc4	jejich
vlastní	vlastní	k2eAgNnPc1d1	vlastní
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
reálnou	reálný	k2eAgFnSc4d1	reálná
asociativní	asociativní	k2eAgFnSc4d1	asociativní
algebru	algebra	k1gFnSc4	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Positivně	positivně	k6eAd1	positivně
definitní	definitní	k2eAgInPc1d1	definitní
operátory	operátor	k1gInPc1	operátor
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
kladným	kladný	k2eAgNnSc7d1	kladné
číslům	číslo	k1gNnPc3	číslo
a	a	k8xC	a
normální	normální	k2eAgMnPc4d1	normální
operátory	operátor	k1gMnPc4	operátor
komplexním	komplexní	k2eAgNnPc3d1	komplexní
číslům	číslo	k1gNnPc3	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Nadreálná	nadreálný	k2eAgNnPc1d1	nadreálné
čísla	číslo	k1gNnPc1	číslo
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
o	o	k7c4	o
nekonečné	konečný	k2eNgFnPc4d1	nekonečná
ordinály	ordinála	k1gFnPc4	ordinála
a	a	k8xC	a
infinitezimální	infinitezimální	k2eAgFnPc4d1	infinitezimální
veličiny	veličina	k1gFnPc4	veličina
<g/>
.	.	kIx.	.
</s>
