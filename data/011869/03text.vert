<p>
<s>
Staňkovský	Staňkovský	k2eAgInSc1d1	Staňkovský
rybník	rybník	k1gInSc1	rybník
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgInSc1d1	známý
pod	pod	k7c7	pod
názvy	název	k1gInPc7	název
Velký	velký	k2eAgMnSc1d1	velký
Soused	soused	k1gMnSc1	soused
a	a	k8xC	a
Velký	velký	k2eAgInSc1d1	velký
Bystřický	bystřický	k2eAgInSc1d1	bystřický
rybník	rybník	k1gInSc1	rybník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
osmý	osmý	k4xOgMnSc1	osmý
největší	veliký	k2eAgInSc1d3	veliký
rybník	rybník	k1gInSc1	rybník
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
Staňkov	Staňkov	k1gInSc4	Staňkov
u	u	k7c2	u
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
Dolní	dolní	k2eAgInPc1d1	dolní
Rakousy	Rakousy	k1gInPc1	Rakousy
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
v	v	k7c6	v
různě	různě	k6eAd1	různě
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
241	[number]	k4	241
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejhlubším	hluboký	k2eAgInSc7d3	nejhlubší
a	a	k8xC	a
nejobjemnějším	objemný	k2eAgInSc7d3	nejobjemnější
rybníkem	rybník	k1gInSc7	rybník
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
údaje	údaj	k1gInSc2	údaj
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
hloubce	hloubka	k1gFnSc6	hloubka
se	se	k3xPyFc4	se
však	však	k9	však
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nS	lišit
-	-	kIx~	-
8,5	[number]	k4	8,5
m	m	kA	m
nebo	nebo	k8xC	nebo
16	[number]	k4	16
m.	m.	k?	m.
Zemní	zemní	k2eAgFnSc1d1	zemní
sypaná	sypaný	k2eAgFnSc1d1	sypaná
hráz	hráz	k1gFnSc1	hráz
je	být	k5eAaImIp3nS	být
160	[number]	k4	160
m	m	kA	m
<g/>
,	,	kIx,	,
170	[number]	k4	170
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
14	[number]	k4	14
m	m	kA	m
<g/>
,	,	kIx,	,
16	[number]	k4	16
m	m	kA	m
vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
asi	asi	k9	asi
6,63	[number]	k4	6,63
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
<g/>
,	,	kIx,	,
20	[number]	k4	20
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
6	[number]	k4	6
km	km	kA	km
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
obvod	obvod	k1gInSc1	obvod
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
124	[number]	k4	124
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
469	[number]	k4	469
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Pobřeží	pobřeží	k1gNnSc2	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
Břehy	břeh	k1gInPc1	břeh
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
západě	západ	k1gInSc6	západ
travnaté	travnatý	k2eAgFnSc2d1	travnatá
a	a	k8xC	a
u	u	k7c2	u
hráze	hráz	k1gFnSc2	hráz
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
k	k	k7c3	k
rybníku	rybník	k1gInSc3	rybník
domovní	domovní	k2eAgFnSc1d1	domovní
zástavba	zástavba	k1gFnSc1	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
plocha	plocha	k1gFnSc1	plocha
rybníku	rybník	k1gInSc3	rybník
rozčleněna	rozčlenit	k5eAaPmNgFnS	rozčlenit
zalesněnými	zalesněný	k2eAgInPc7d1	zalesněný
poloostrovy	poloostrov	k1gInPc7	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
na	na	k7c6	na
Koštěnickém	Koštěnický	k2eAgInSc6d1	Koštěnický
potoce	potok	k1gInSc6	potok
nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
Staňkov	Staňkov	k1gInSc1	Staňkov
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
vzdutí	vzdutí	k1gNnSc2	vzdutí
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
4	[number]	k4	4
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
napájen	napájet	k5eAaImNgInS	napájet
mnoha	mnoho	k4c7	mnoho
drobnými	drobný	k2eAgInPc7d1	drobný
potoky	potok	k1gInPc7	potok
od	od	k7c2	od
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
-	-	kIx~	-
například	například	k6eAd1	například
z	z	k7c2	z
rybníku	rybník	k1gInSc3	rybník
Rubitzkoteich	Rubitzkoteich	k1gMnSc1	Rubitzkoteich
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
větší	veliký	k2eAgInSc1d2	veliký
o	o	k7c4	o
rozlohu	rozloha	k1gFnSc4	rozloha
současného	současný	k2eAgInSc2d1	současný
rybníka	rybník	k1gInSc2	rybník
Špačkov	Špačkov	k1gInSc1	Špačkov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
později	pozdě	k6eAd2	pozdě
oddělen	oddělit	k5eAaPmNgInS	oddělit
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
výnosnějšímu	výnosný	k2eAgInSc3d2	výnosnější
chovu	chov	k1gInSc3	chov
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Výpusť	výpusť	k1gFnSc1	výpusť
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgFnSc1d1	unikátní
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
trouby	trouba	k1gFnPc4	trouba
a	a	k8xC	a
štolu	štola	k1gFnSc4	štola
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
250	[number]	k4	250
<g/>
×	×	k?	×
<g/>
300	[number]	k4	300
cm	cm	kA	cm
společně	společně	k6eAd1	společně
hrazené	hrazený	k2eAgInPc1d1	hrazený
dřevěnými	dřevěný	k2eAgFnPc7d1	dřevěná
lopatami	lopata	k1gFnPc7	lopata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
využitím	využití	k1gNnSc7	využití
je	být	k5eAaImIp3nS	být
sportovní	sportovní	k2eAgInSc1d1	sportovní
rybolov	rybolov	k1gInSc1	rybolov
a	a	k8xC	a
rekreace	rekreace	k1gFnSc1	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
koupání	koupání	k1gNnSc3	koupání
slouží	sloužit	k5eAaImIp3nS	sloužit
písčitá	písčitý	k2eAgFnSc1d1	písčitá
pláž	pláž	k1gFnSc1	pláž
s	s	k7c7	s
mírně	mírně	k6eAd1	mírně
se	s	k7c7	s
svažujícím	svažující	k2eAgInSc7d1	svažující
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rekreaci	rekreace	k1gFnSc3	rekreace
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
autokempy	autokemp	k1gInPc4	autokemp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Rybník	rybník	k1gInSc1	rybník
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Mikulášem	Mikuláš	k1gMnSc7	Mikuláš
Ruthardem	Ruthard	k1gMnSc7	Ruthard
z	z	k7c2	z
Malešova	Malešův	k2eAgInSc2d1	Malešův
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1550	[number]	k4	1550
především	především	k6eAd1	především
kvůli	kvůli	k7c3	kvůli
povodním	povodeň	k1gFnPc3	povodeň
na	na	k7c6	na
Koštěnickém	Koštěnický	k2eAgInSc6d1	Koštěnický
potoce	potok	k1gInSc6	potok
<g/>
.	.	kIx.	.
</s>
<s>
Plní	plnit	k5eAaImIp3nS	plnit
tedy	tedy	k9	tedy
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
retenční	retenční	k2eAgFnSc4d1	retenční
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xC	jako
Rožmberk	Rožmberk	k1gInSc4	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
rybník	rybník	k1gInSc1	rybník
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
Soused	soused	k1gMnSc1	soused
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Velký	velký	k2eAgMnSc1d1	velký
Bystřický	Bystřický	k1gMnSc1	Bystřický
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
původně	původně	k6eAd1	původně
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
jezero	jezero	k1gNnSc1	jezero
přehrazené	přehrazený	k2eAgNnSc1d1	přehrazené
skálou	skála	k1gFnSc7	skála
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
byla	být	k5eAaImAgFnS	být
vylámána	vylámán	k2eAgFnSc1d1	vylámána
štola	štola	k1gFnSc1	štola
pro	pro	k7c4	pro
výpust	výpust	k1gFnSc4	výpust
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
rybníka	rybník	k1gInSc2	rybník
Dvořiště	dvořiště	k1gNnSc2	dvořiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Štefáček	Štefáček	k1gMnSc1	Štefáček
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
Staňkovský	Staňkovský	k2eAgInSc1d1	Staňkovský
rybník	rybník	k1gInSc1	rybník
(	(	kIx(	(
<g/>
s.	s.	k?	s.
251	[number]	k4	251
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Libri	Libr	k1gFnSc2	Libr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
ISBN	ISBN	kA	ISBN
978-80-7277-440-1	[number]	k4	978-80-7277-440-1
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
popularizaci	popularizace	k1gFnSc4	popularizace
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Staňkovský	Staňkovský	k2eAgInSc1d1	Staňkovský
rybník	rybník	k1gInSc1	rybník
</s>
</p>
