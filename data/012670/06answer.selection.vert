<s>
Okluzivy	Okluziva	k1gFnPc1	Okluziva
–	–	k?	–
závěrové	závěrový	k2eAgFnPc1d1	závěrová
souhlásky	souhláska	k1gFnPc1	souhláska
–	–	k?	–
vznikají	vznikat	k5eAaImIp3nP	vznikat
přechodným	přechodný	k2eAgNnSc7d1	přechodné
vytvořením	vytvoření	k1gNnSc7	vytvoření
překážky	překážka	k1gFnSc2	překážka
(	(	kIx(	(
<g/>
okluze	okluze	k1gFnSc1	okluze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
proudění	proudění	k1gNnSc4	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
