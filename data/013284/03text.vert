<p>
<s>
Ostrovánky	Ostrovánka	k1gFnPc1	Ostrovánka
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Ostrowanek	Ostrowanka	k1gFnPc2	Ostrowanka
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Ostrawsko	Ostrawsko	k1gNnSc1	Ostrawsko
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
5	[number]	k4	5
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Kyjova	Kyjov	k1gInSc2	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
227	[number]	k4	227
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Mikroregionu	mikroregion	k1gInSc2	mikroregion
Babí	babit	k5eAaImIp3nS	babit
lom	lom	k1gInSc1	lom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgInPc7	svůj
kroji	kroj	k1gInPc7	kroj
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
hanácko-slovácké	hanáckolovácký	k2eAgFnSc2d1	hanácko-slovácký
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgFnPc1	čtyři
archeologické	archeologický	k2eAgFnPc1d1	archeologická
lokality	lokalita	k1gFnPc1	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
biskupa	biskup	k1gMnSc2	biskup
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Zdíka	Zdík	k1gMnSc2	Zdík
připomíná	připomínat	k5eAaImIp3nS	připomínat
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1141	[number]	k4	1141
jako	jako	k8xS	jako
majetek	majetek	k1gInSc4	majetek
břeclavského	břeclavský	k2eAgInSc2d1	břeclavský
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
užíval	užívat	k5eAaImAgInS	užívat
i	i	k9	i
název	název	k1gInSc1	název
Ostrovany	ostrovan	k1gMnPc4	ostrovan
nebo	nebo	k8xC	nebo
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1510	[number]	k4	1510
<g/>
–	–	k?	–
<g/>
1515	[number]	k4	1515
se	se	k3xPyFc4	se
Ostrovánky	Ostrovánka	k1gFnPc1	Ostrovánka
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jako	jako	k9	jako
pusté	pustý	k2eAgFnPc1d1	pustá
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
začleněny	začlenit	k5eAaPmNgFnP	začlenit
do	do	k7c2	do
ždánického	ždánický	k2eAgNnSc2d1	ždánický
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
místo	místo	k1gNnSc1	místo
opět	opět	k6eAd1	opět
pusté	pustý	k2eAgNnSc4d1	pusté
a	a	k8xC	a
nazýváno	nazýván	k2eAgNnSc4d1	nazýváno
Ostrovsko	Ostrovsko	k1gNnSc4	Ostrovsko
a	a	k8xC	a
při	při	k7c6	při
následném	následný	k2eAgNnSc6d1	následné
osidlování	osidlování	k1gNnSc6	osidlování
i	i	k9	i
jako	jako	k9	jako
Ostrovská	ostrovský	k2eAgFnSc1d1	Ostrovská
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ždánické	ždánický	k2eAgFnSc2d1	Ždánická
matriky	matrika	k1gFnSc2	matrika
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začali	začít	k5eAaPmAgMnP	začít
lidé	člověk	k1gMnPc1	člověk
znovu	znovu	k6eAd1	znovu
usazovat	usazovat	k5eAaImF	usazovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1675	[number]	k4	1675
<g/>
–	–	k?	–
<g/>
1681	[number]	k4	1681
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dochovány	dochován	k2eAgFnPc4d1	dochována
pečeti	pečeť	k1gFnPc4	pečeť
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1679	[number]	k4	1679
a	a	k8xC	a
1821	[number]	k4	1821
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
samostatná	samostatný	k2eAgFnSc1d1	samostatná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1888	[number]	k4	1888
byla	být	k5eAaImAgFnS	být
přičleněna	přičleněn	k2eAgFnSc1d1	přičleněna
k	k	k7c3	k
sousednímu	sousední	k2eAgInSc3d1	sousední
Nechvalínu	Nechvalín	k1gInSc3	Nechvalín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Nechvalínem	Nechvalín	k1gInSc7	Nechvalín
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
s	s	k7c7	s
Bukovany	Bukovan	k1gMnPc7	Bukovan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
obcí	obec	k1gFnSc7	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obecní	obecní	k2eAgInPc4d1	obecní
symboly	symbol	k1gInPc4	symbol
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2001	[number]	k4	2001
obec	obec	k1gFnSc1	obec
užívá	užívat	k5eAaImIp3nS	užívat
znak	znak	k1gInSc4	znak
a	a	k8xC	a
prapor	prapor	k1gInSc4	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horním	horní	k2eAgNnSc6d1	horní
bílém	bílé	k1gNnSc6	bílé
je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
žalud	žalud	k1gInSc1	žalud
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
listy	list	k1gInPc7	list
podle	podle	k7c2	podle
pečeti	pečeť	k1gFnSc2	pečeť
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1679	[number]	k4	1679
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
zelené	zelený	k2eAgFnSc6d1	zelená
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
žlutý	žlutý	k2eAgInSc1d1	žlutý
vinný	vinný	k2eAgInSc1d1	vinný
hrozen	hrozen	k1gInSc1	hrozen
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
vinařskou	vinařský	k2eAgFnSc4d1	vinařská
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
převzat	převzít	k5eAaPmNgMnS	převzít
z	z	k7c2	z
razítka	razítko	k1gNnSc2	razítko
obce	obec	k1gFnSc2	obec
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
horního	horní	k2eAgNnSc2d1	horní
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
odvozena	odvozen	k2eAgFnSc1d1	odvozena
od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
je	být	k5eAaImIp3nS	být
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
místní	místní	k2eAgFnSc1d1	místní
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	Zelená	k1gFnSc1	Zelená
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc1	zemědělství
a	a	k8xC	a
pastevectví	pastevectví	k1gNnSc1	pastevectví
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
praporu	prapor	k1gInSc6	prapor
jsou	být	k5eAaImIp3nP	být
zobrazeny	zobrazen	k2eAgInPc4d1	zobrazen
motivy	motiv	k1gInPc4	motiv
obecního	obecní	k2eAgInSc2d1	obecní
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
zelenými	zelený	k2eAgInPc7d1	zelený
postranními	postranní	k2eAgInPc7d1	postranní
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
středu	středa	k1gFnSc4	středa
pak	pak	k6eAd1	pak
žalud	žalud	k1gInSc1	žalud
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
kyjovským	kyjovský	k2eAgMnSc7d1	kyjovský
stavitelem	stavitel	k1gMnSc7	stavitel
Františkem	František	k1gMnSc7	František
Říhovským	Říhovský	k1gMnSc7	Říhovský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Oltářní	oltářní	k2eAgInSc4d1	oltářní
obraz	obraz	k1gInSc4	obraz
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Jano	Jano	k1gMnSc1	Jano
Köhler	Köhler	k1gMnSc1	Köhler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
</s>
</p>
<p>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
u	u	k7c2	u
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc4	Václav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
207	[number]	k4	207
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
se	s	k7c7	s
154	[number]	k4	154
(	(	kIx(	(
<g/>
74,4	[number]	k4	74,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
42	[number]	k4	42
(	(	kIx(	(
<g/>
20,3	[number]	k4	20,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
k	k	k7c3	k
moravské	moravský	k2eAgFnSc3d1	Moravská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
je	být	k5eAaImIp3nS	být
82	[number]	k4	82
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
324	[number]	k4	324
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
nejvíce	hodně	k6eAd3	hodně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	s	k7c7	s
124	[number]	k4	124
(	(	kIx(	(
<g/>
59,9	[number]	k4	59,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
označilo	označit	k5eAaPmAgNnS	označit
za	za	k7c4	za
věřící	věřící	k1gFnSc4	věřící
<g/>
,	,	kIx,	,
111	[number]	k4	111
(	(	kIx(	(
<g/>
53,6	[number]	k4	53,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
Římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
7	[number]	k4	7
k	k	k7c3	k
Církvi	církev	k1gFnSc3	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
1	[number]	k4	1
ke	k	k7c3	k
Svědkům	svědek	k1gMnPc3	svědek
Jehovovým	Jehovův	k2eAgMnPc3d1	Jehovův
<g/>
.	.	kIx.	.
69	[number]	k4	69
(	(	kIx(	(
<g/>
33,3	[number]	k4	33,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
označilo	označit	k5eAaPmAgNnS	označit
jako	jako	k9	jako
bez	bez	k7c2	bez
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
14	[number]	k4	14
(	(	kIx(	(
<g/>
6,8	[number]	k4	6,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
víry	víra	k1gFnSc2	víra
neodpovědělo	odpovědět	k5eNaPmAgNnS	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
farnosti	farnost	k1gFnSc2	farnost
Lovčice	Lovčice	k1gFnSc2	Lovčice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hodonínského	hodonínský	k2eAgInSc2d1	hodonínský
děkanátu	děkanát	k1gInSc2	děkanát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ostrovánky	Ostrovánka	k1gFnSc2	Ostrovánka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Ostrovánky	Ostrovánka	k1gFnSc2	Ostrovánka
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
a	a	k8xC	a
obecní	obecní	k2eAgFnSc1d1	obecní
statistika	statistika	k1gFnSc1	statistika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
ČSÚ	ČSÚ	kA	ČSÚ
</s>
</p>
<p>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
Ostrovánek	Ostrovánka	k1gFnPc2	Ostrovánka
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
<p>
<s>
Císařské	císařský	k2eAgInPc1d1	císařský
otisky	otisk	k1gInPc1	otisk
stabilního	stabilní	k2eAgInSc2d1	stabilní
katastru	katastr	k1gInSc2	katastr
–	–	k?	–
historická	historický	k2eAgFnSc1d1	historická
mapa	mapa	k1gFnSc1	mapa
Ostrovánek	Ostrovánka	k1gFnPc2	Ostrovánka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
</s>
</p>
