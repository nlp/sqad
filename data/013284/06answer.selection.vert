<s>
Ostrovánky	Ostrovánka	k1gFnPc1	Ostrovánka
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Ostrowanek	Ostrowanka	k1gFnPc2	Ostrowanka
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Ostrawsko	Ostrawsko	k1gNnSc1	Ostrawsko
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
5	[number]	k4	5
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Kyjova	Kyjov	k1gInSc2	Kyjov
<g/>
.	.	kIx.	.
</s>
