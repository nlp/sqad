<s>
Phobos	Phobos	k1gInSc1
(	(	kIx(
<g/>
měsíc	měsíc	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
substituovaný	substituovaný	k2eAgInSc1d1
infobox	infobox	k1gInSc1
<g/>
.	.	kIx.
<g/>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
převedete	převést	k5eAaPmIp2nP
na	na	k7c4
standardní	standardní	k2eAgFnSc4d1
šablonu	šablona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Phobos	Phobos	k1gMnSc1
</s>
<s>
Marsovy	Marsův	k2eAgInPc1d1
měsíce	měsíc	k1gInPc1
<g/>
:	:	kIx,
<g/>
Phobos	Phobos	k1gInSc1
</s>
<s>
Phobos	Phobos	k1gInSc1
na	na	k7c6
snímku	snímek	k1gInSc6
sondy	sonda	k1gFnSc2
Mars	Mars	k1gInSc1
Reconnaissance	Reconnaissance	k1gFnSc2
Orbiter	Orbitra	k1gFnPc2
z	z	k7c2
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
</s>
<s>
Objev	objev	k1gInSc1
</s>
<s>
Objevitel	objevitel	k1gMnSc1
</s>
<s>
Asaph	Asaph	k1gMnSc1
Hall	Hall	k1gMnSc1
</s>
<s>
Datum	datum	k1gNnSc1
objevu	objev	k1gInSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1877	#num#	k4
</s>
<s>
Předběžné	předběžný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
</s>
<s>
žádné	žádný	k3yNgInPc1
</s>
<s>
Definitivní	definitivní	k2eAgNnSc1d1
označení	označení	k1gNnSc1
</s>
<s>
Mars	Mars	k1gInSc1
I	i	k9
</s>
<s>
Elementy	element	k1gInPc1
dráhy	dráha	k1gFnSc2
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
poloosa	poloosa	k1gFnSc1
</s>
<s>
9	#num#	k4
380	#num#	k4
km	km	kA
0,000	0,000	k4
063	#num#	k4
AU	au	k0
</s>
<s>
Excentricita	excentricita	k1gFnSc1
</s>
<s>
0,0151	0,0151	k4
</s>
<s>
Pericentrum	Pericentrum	k1gNnSc1
</s>
<s>
9	#num#	k4
522	#num#	k4
km	km	kA
</s>
<s>
Apocentrum	Apocentrum	k1gNnSc1
</s>
<s>
9	#num#	k4
238	#num#	k4
km	km	kA
</s>
<s>
Perioda	perioda	k1gFnSc1
(	(	kIx(
<g/>
oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
0,319	0,319	k4
dne	den	k1gInSc2
</s>
<s>
Sklon	sklon	k1gInSc1
dráhy	dráha	k1gFnSc2
k	k	k7c3
rovníku	rovník	k1gInSc3
</s>
<s>
1,093	1,093	k4
<g/>
°	°	k?
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1
charakteristiky	charakteristika	k1gFnPc1
</s>
<s>
Rozměry	rozměra	k1gFnPc1
</s>
<s>
27,0	27,0	k4
<g/>
×	×	k?
<g/>
21,6	21,6	k4
<g/>
×	×	k?
<g/>
19,8	19,8	k4
(	(	kIx(
<g/>
22,2	22,2	k4
±	±	k?
0,30	0,30	k4
<g/>
)	)	kIx)
km	km	kA
</s>
<s>
Gravitační	gravitační	k2eAgInSc1d1
parametr	parametr	k1gInSc1
</s>
<s>
0,000	0,000	k4
713	#num#	k4
8	#num#	k4
±	±	k?
0,000	0,000	k4
001	#num#	k4
9	#num#	k4
km	km	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
1.070	1.070	k4
±	±	k?
0,085	0,085	k4
<g/>
)	)	kIx)
<g/>
×	×	k?
<g/>
1016	#num#	k4
kg	kg	kA
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
</s>
<s>
1,876	1,876	k4
±	±	k?
0,020	0,020	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gravitační	gravitační	k2eAgNnSc1d1
zrychlení	zrychlení	k1gNnSc1
</s>
<s>
0,0019	0,0019	k4
až	až	k6eAd1
0,0084	0,0084	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
2	#num#	k4
<g/>
(	(	kIx(
0,00058	0,00058	k4
<g/>
g	g	kA
<g/>
)	)	kIx)
</s>
<s>
Úniková	únikový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
</s>
<s>
0,011	0,011	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Perioda	perioda	k1gFnSc1
rotace	rotace	k1gFnSc1
</s>
<s>
7,66	7,66	k4
h	h	k?
(	(	kIx(
<g/>
vázaná	vázaný	k2eAgFnSc1d1
rotace	rotace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Sklon	sklon	k1gInSc1
rotační	rotační	k2eAgFnSc2d1
osy	osa	k1gFnSc2
</s>
<s>
~	~	kIx~
0	#num#	k4
<g/>
°	°	k?
</s>
<s>
Albedo	Albedo	k1gNnSc1
</s>
<s>
0,071	0,071	k4
±	±	k?
0,012	0,012	k4
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
–	–	k?
min	mina	k1gFnPc2
–	–	k?
průměr	průměr	k1gInSc1
–	–	k?
max	max	kA
</s>
<s>
—	—	k?
<g/>
~	~	kIx~
−	−	k?
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
tj.	tj.	kA
~	~	kIx~
233	#num#	k4
K	k	k7c3
—	—	k?
</s>
<s>
Charakteristiky	charakteristika	k1gFnPc1
atmosféry	atmosféra	k1gFnSc2
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1
tlak	tlak	k1gInSc1
</s>
<s>
neměřitelný	měřitelný	k2eNgInSc1d1
</s>
<s>
Složení	složení	k1gNnSc1
atmosféry	atmosféra	k1gFnSc2
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1
pára	pára	k1gFnSc1
?	?	kIx.
</s>
<s>
stopy	stopa	k1gFnPc1
</s>
<s>
Phobos	Phobos	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
psáno	psán	k2eAgNnSc1d1
Fobos	Fobos	k1gInSc4
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
Φ	Φ	k?
<g/>
,	,	kIx,
česky	česky	k6eAd1
Děs	děs	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vnitřním	vnitřní	k2eAgInSc7d1
a	a	k8xC
větším	veliký	k2eAgInSc7d2
ze	z	k7c2
dvou	dva	k4xCgInPc2
známých	známý	k2eAgInPc2d1
měsíců	měsíc	k1gInPc2
planety	planeta	k1gFnSc2
Marsu	Mars	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Objev	objev	k1gInSc1
</s>
<s>
Phobos	Phobos	k1gInSc1
objevil	objevit	k5eAaPmAgInS
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1877	#num#	k4
Asaph	Asaph	k1gMnSc1
Hall	Hall	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
pouhých	pouhý	k2eAgInPc2d1
šest	šest	k4xCc4
dní	den	k1gInPc2
po	po	k7c6
objevu	objev	k1gInSc6
prvního	první	k4xOgInSc2
měsíce	měsíc	k1gInSc2
Marsu	Mars	k1gInSc2
<g/>
,	,	kIx,
Deimosu	Deimosa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objev	objev	k1gInSc1
byl	být	k5eAaImAgInS
zveřejněn	zveřejnit	k5eAaPmNgInS
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zajímavé	zajímavý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
existenci	existence	k1gFnSc4
měsíců	měsíc	k1gInPc2
Marsu	Mars	k1gInSc2
předpověděl	předpovědět	k5eAaPmAgMnS
již	již	k6eAd1
Johannes	Johannes	k1gMnSc1
Kepler	Kepler	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1610	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Předpověď	předpověď	k1gFnSc1
měsíce	měsíc	k1gInSc2
Phobos	Phobosa	k1gFnPc2
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
dostupných	dostupný	k2eAgFnPc6d1
znalostech	znalost	k1gFnPc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
Venuše	Venuše	k1gFnSc1
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
měsíc	měsíc	k1gInSc4
<g/>
,	,	kIx,
Země	země	k1gFnSc1
má	mít	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
a	a	k8xC
Jupiter	Jupiter	k1gMnSc1
čtyři	čtyři	k4xCgInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
této	tento	k3xDgFnSc2
posloupnosti	posloupnost	k1gFnSc2
se	se	k3xPyFc4
vyvozovalo	vyvozovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Mars	Mars	k1gInSc1
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
měsíce	měsíc	k1gInPc4
dva	dva	k4xCgInPc4
a	a	k8xC
že	že	k9
se	se	k3xPyFc4
mezi	mezi	k7c7
Marsem	Mars	k1gInSc7
a	a	k8xC
Jupiterem	Jupiter	k1gMnSc7
ukrývá	ukrývat	k5eAaImIp3nS
ještě	ještě	k9
jedna	jeden	k4xCgFnSc1
planeta	planeta	k1gFnSc1
se	s	k7c7
třemi	tři	k4xCgInPc7
měsíci	měsíc	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
dostala	dostat	k5eAaPmAgFnS
dvojice	dvojice	k1gFnSc1
měsíců	měsíc	k1gInPc2
i	i	k8xC
do	do	k7c2
knihy	kniha	k1gFnSc2
Jonathana	Jonathan	k1gMnSc2
Swifta	Swift	k1gMnSc2
Gulliverovy	Gulliverův	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1726	#num#	k4
popisující	popisující	k2eAgInSc1d1
objev	objev	k1gInSc1
dvou	dva	k4xCgInPc2
měsíčků	měsíček	k1gInPc2
Marsu	Mars	k1gInSc6
hvězdáři	hvězdář	k1gMnPc1
vymyšlené	vymyšlený	k2eAgFnSc2d1
země	zem	k1gFnSc2
Laputa	Laput	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
měsíce	měsíc	k1gInSc2
</s>
<s>
Protáhlý	protáhlý	k2eAgInSc1d1
fazolovitý	fazolovitý	k2eAgInSc1d1
tvar	tvar	k1gInSc1
měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
blížící	blížící	k2eAgNnPc4d1
se	se	k3xPyFc4
trojosému	trojosý	k2eAgInSc3d1
elipsoidu	elipsoid	k1gInSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
pravděpodobností	pravděpodobnost	k1gFnSc7
zachycenou	zachycený	k2eAgFnSc7d1
planetkou	planetka	k1gFnSc7
<g/>
,	,	kIx,
pocházející	pocházející	k2eAgFnSc7d1
z	z	k7c2
oblasti	oblast	k1gFnSc2
hlavního	hlavní	k2eAgInSc2d1
pásu	pás	k1gInSc2
planetek	planetka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachycena	zachycen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
zřejmě	zřejmě	k6eAd1
vzájemnou	vzájemný	k2eAgFnSc7d1
kombinací	kombinace	k1gFnSc7
gravitačních	gravitační	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
působených	působený	k2eAgFnPc2d1
Jupiterem	Jupiter	k1gInSc7
a	a	k8xC
samotným	samotný	k2eAgInSc7d1
Marsem	Mars	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiné	jiný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
oba	dva	k4xCgInPc1
měsíce	měsíc	k1gInPc1
vyraženy	vyražen	k2eAgInPc1d1
z	z	k7c2
povrchu	povrch	k1gInSc2
Protomarsu	Protomars	k1gInSc2
v	v	k7c6
době	doba	k1gFnSc6
tvorby	tvorba	k1gFnSc2
planety	planeta	k1gFnSc2
akrecí	akrece	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
dopadech	dopad	k1gInPc6
velkých	velký	k2eAgFnPc2d1
planetesimál	planetesimála	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
spektroskopických	spektroskopický	k2eAgNnPc2d1
měření	měření	k1gNnSc2
se	se	k3xPyFc4
podobá	podobat	k5eAaImIp3nS
planetkám	planetka	k1gFnPc3
typu	typ	k1gInSc2
C	C	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
složení	složení	k1gNnSc1
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
uhlíkatým	uhlíkatý	k2eAgInPc3d1
chondritům	chondrit	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
očekávat	očekávat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
obsahovat	obsahovat	k5eAaImF
značné	značný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
uhlíku	uhlík	k1gInSc2
a	a	k8xC
uhlíkatých	uhlíkatý	k2eAgFnPc2d1
(	(	kIx(
<g/>
organických	organický	k2eAgFnPc2d1
<g/>
)	)	kIx)
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimořádně	mimořádně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
tohoto	tento	k3xDgInSc2
měsíce	měsíc	k1gInSc2
se	se	k3xPyFc4
dříve	dříve	k6eAd2
vysvětlovala	vysvětlovat	k5eAaImAgFnS
domněnkami	domněnka	k1gFnPc7
<g/>
,	,	kIx,
že	že	k8xS
Phobos	Phobos	k1gInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
dutý	dutý	k2eAgInSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
že	že	k8xS
obsahuje	obsahovat	k5eAaImIp3nS
značný	značný	k2eAgInSc4d1
podíl	podíl	k1gInSc4
zmrzlé	zmrzlý	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
i	i	k9
jiných	jiný	k2eAgFnPc2d1
těkavých	těkavý	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
složení	složení	k1gNnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
zřetelně	zřetelně	k6eAd1
lišilo	lišit	k5eAaImAgNnS
od	od	k7c2
složení	složení	k1gNnSc2
Deimosu	Deimos	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
by	by	kYmCp3nS
šlo	jít	k5eAaImAgNnS
usuzovat	usuzovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgInPc1
měsíce	měsíc	k1gInPc1
nemají	mít	k5eNaImIp3nP
společný	společný	k2eAgInSc4d1
původ	původ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
snižovalo	snižovat	k5eAaImAgNnS
pravděpodobnost	pravděpodobnost	k1gFnSc4
impaktního	impaktní	k2eAgInSc2d1
původu	původ	k1gInSc2
těchto	tento	k3xDgInPc2
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Novějším	nový	k2eAgNnSc7d2
objasněním	objasnění	k1gNnSc7
nízké	nízký	k2eAgFnSc2d1
hustoty	hustota	k1gFnSc2
<g/>
,	,	kIx,
vytvořeným	vytvořený	k2eAgNnSc7d1
na	na	k7c6
základě	základ	k1gInSc6
přeletů	přelet	k1gInPc2
sondy	sonda	k1gFnSc2
Mars	Mars	k1gInSc1
Express	express	k1gInSc1
je	být	k5eAaImIp3nS
předpoklad	předpoklad	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
těleso	těleso	k1gNnSc1
není	být	k5eNaImIp3nS
hmotou	hmota	k1gFnSc7
zcela	zcela	k6eAd1
vyplněné	vyplněný	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
dutiny	dutina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
výpočtů	výpočet	k1gInPc2
činí	činit	k5eAaImIp3nS
podíl	podíl	k1gInSc4
dutin	dutina	k1gFnPc2
30	#num#	k4
±	±	k?
5	#num#	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dutiny	dutina	k1gFnPc1
mezi	mezi	k7c7
zrny	zrno	k1gNnPc7
a	a	k8xC
balvany	balvan	k1gInPc7
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
mít	mít	k5eAaImF
velikost	velikost	k1gFnSc4
řádově	řádově	k6eAd1
od	od	k7c2
1	#num#	k4
milimetru	milimetr	k1gInSc2
do	do	k7c2
1	#num#	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Phobos	Phobos	k1gMnSc1
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
měřitelnou	měřitelný	k2eAgFnSc4d1
atmosféru	atmosféra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
sovětská	sovětský	k2eAgFnSc1d1
kosmická	kosmický	k2eAgFnSc1d1
sonda	sonda	k1gFnSc1
Fobos	Fobos	k1gInSc1
2	#num#	k4
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
povrchu	povrch	k1gInSc2
měsíce	měsíc	k1gInSc2
unikají	unikat	k5eAaImIp3nP
plyny	plyn	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gNnSc4
přesné	přesný	k2eAgNnSc4d1
složení	složení	k1gNnSc4
nestačila	stačit	k5eNaBmAgFnS
zjistit	zjistit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
převážně	převážně	k6eAd1
o	o	k7c4
vodní	vodní	k2eAgFnSc4d1
páru	pára	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Povrch	povrch	k1gInSc1
měsíce	měsíc	k1gInSc2
je	být	k5eAaImIp3nS
pokryt	pokrýt	k5eAaPmNgInS
krátery	kráter	k1gInPc7
<g/>
,	,	kIx,
pozůstatky	pozůstatek	k1gInPc1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
bombardování	bombardování	k1gNnSc2
v	v	k7c6
minulosti	minulost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pokryt	pokrýt	k5eAaPmNgInS
regolitem	regolit	k1gInSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
u	u	k7c2
druhého	druhý	k4xOgInSc2
měsíce	měsíc	k1gInSc2
Deimosu	Deimos	k1gInSc2
a	a	k8xC
zřejmě	zřejmě	k6eAd1
jemnějším	jemný	k2eAgInPc3d2
<g/>
,	,	kIx,
podobným	podobný	k2eAgInPc3d1
prachu	prach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přítomnost	přítomnost	k1gFnSc1
asi	asi	k9
metrové	metrový	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
prachu	prach	k1gInSc2
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
povrchu	povrch	k1gInSc6
odhalila	odhalit	k5eAaPmAgFnS
sonda	sonda	k1gFnSc1
Mars	Mars	k1gInSc1
Global	globat	k5eAaImAgMnS
Surveyor	Surveyor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
jsou	být	k5eAaImIp3nP
krátery	kráter	k1gInPc1
mnohem	mnohem	k6eAd1
zřetelnější	zřetelný	k2eAgMnSc1d2
než	než	k8xS
na	na	k7c4
Deimosu	Deimosa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnPc4d3
z	z	k7c2
nich	on	k3xPp3gInPc2
jsou	být	k5eAaImIp3nP
Stickney	Sticknea	k1gFnPc1
(	(	kIx(
<g/>
průměr	průměr	k1gInSc1
10	#num#	k4
km	km	kA
<g/>
)	)	kIx)
a	a	k8xC
Hall	Hall	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
impaktu	impakt	k1gInSc6
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
se	se	k3xPyFc4
vytvořil	vytvořit	k5eAaPmAgInS
Stickney	Sticknea	k1gFnSc2
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozlomení	rozlomení	k1gNnSc3
tohoto	tento	k3xDgInSc2
měsíce	měsíc	k1gInSc2
<g/>
;	;	kIx,
při	při	k7c6
jeho	jeho	k3xOp3gInSc6
vzniku	vznik	k1gInSc6
se	se	k3xPyFc4
také	také	k9
vytvořila	vytvořit	k5eAaPmAgFnS
řada	řada	k1gFnSc1
lineárních	lineární	k2eAgFnPc2d1
rýh	rýha	k1gFnPc2
<g/>
,	,	kIx,
dlouhých	dlouhý	k2eAgFnPc2d1
až	až	k9
10	#num#	k4
km	km	kA
a	a	k8xC
až	až	k9
100	#num#	k4
m	m	kA
hlubokých	hluboký	k2eAgFnPc2d1
a	a	k8xC
až	až	k9
800	#num#	k4
m	m	kA
širokých	široký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
pojmenovanými	pojmenovaný	k2eAgInPc7d1
objekty	objekt	k1gInPc7
na	na	k7c6
povrchu	povrch	k1gInSc6
Phobosu	Phobos	k1gInSc2
jsou	být	k5eAaImIp3nP
krátery	kráter	k1gInPc1
D	D	kA
<g/>
'	'	kIx"
<g/>
Arrest	Arrest	k1gMnSc1
<g/>
,	,	kIx,
Roche	Roche	k1gFnSc1
<g/>
,	,	kIx,
Sharpless	Sharpless	k1gInSc1
<g/>
,	,	kIx,
Todd	Todd	k1gInSc1
<g/>
,	,	kIx,
Limtoc	Limtoc	k1gInSc1
<g/>
,	,	kIx,
Clustril	Clustril	k1gMnSc1
<g/>
,	,	kIx,
Flimnap	Flimnap	k1gMnSc1
<g/>
,	,	kIx,
Gulliver	Gulliver	k1gMnSc1
<g/>
,	,	kIx,
Glirdrig	Glirdrig	k1gMnSc1
<g/>
,	,	kIx,
Reldresal	Reldresal	k1gFnSc1
<g/>
,	,	kIx,
Drunlo	Drunlo	k1gNnSc1
<g/>
,	,	kIx,
Skyresh	Skyresh	k1gInSc1
<g/>
,	,	kIx,
Wendell	Wendell	k1gInSc1
<g/>
,	,	kIx,
hřeben	hřeben	k1gInSc1
Kepler	Kepler	k1gMnSc1
Dorsum	Dorsum	k1gInSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
rozhodnutí	rozhodnutí	k1gNnSc2
IAU	IAU	kA
byly	být	k5eAaImAgInP
útvary	útvar	k1gInPc1
na	na	k7c6
Phobosu	Phobos	k1gInSc6
pojmenovány	pojmenovat	k5eAaPmNgInP
po	po	k7c6
osobách	osoba	k1gFnPc6
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
objevem	objev	k1gInSc7
měsíců	měsíc	k1gInPc2
Marsu	Mars	k1gInSc2
(	(	kIx(
<g/>
největší	veliký	k2eAgMnSc1d3
z	z	k7c2
kráterů	kráter	k1gInPc2
podle	podle	k7c2
rodného	rodný	k2eAgNnSc2d1
příjmení	příjmení	k1gNnSc2
manželky	manželka	k1gFnSc2
objevitele	objevitel	k1gMnSc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
v	v	k7c6
pořadí	pořadí	k1gNnSc6
podle	podle	k7c2
objevitele	objevitel	k1gMnSc2
samotného	samotný	k2eAgMnSc2d1
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
literárních	literární	k2eAgFnPc6d1
postavách	postava	k1gFnPc6
z	z	k7c2
díla	dílo	k1gNnSc2
Gulliverovy	Gulliverův	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
od	od	k7c2
Jonathana	Jonathan	k1gMnSc2
Swifta	Swift	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rotace	rotace	k1gFnSc1
měsíce	měsíc	k1gInSc2
je	být	k5eAaImIp3nS
vázaná	vázaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
planetě	planeta	k1gFnSc3
přivrací	přivracet	k5eAaImIp3nP
stále	stále	k6eAd1
jednu	jeden	k4xCgFnSc4
tvář	tvář	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
orientován	orientovat	k5eAaBmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
nejdelší	dlouhý	k2eAgFnSc1d3
osa	osa	k1gFnSc1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
tělesa	těleso	k1gNnSc2
směřuje	směřovat	k5eAaImIp3nS
do	do	k7c2
středu	střed	k1gInSc2
Marsu	Mars	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Gravitační	gravitační	k2eAgFnSc1d1
přitažlivost	přitažlivost	k1gFnSc1
na	na	k7c6
povrchu	povrch	k1gInSc6
Phobosu	Phobos	k1gInSc2
je	být	k5eAaImIp3nS
vzhledem	vzhled	k1gInSc7
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
značně	značně	k6eAd1
protáhlému	protáhlý	k2eAgInSc3d1
tvaru	tvar	k1gInSc3
značně	značně	k6eAd1
proměnná	proměnný	k2eAgFnSc1d1
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
o	o	k7c4
210	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
na	na	k7c6
obou	dva	k4xCgInPc6
protáhlých	protáhlý	k2eAgInPc6d1
koncích	konec	k1gInPc6
je	být	k5eAaImIp3nS
efektivní	efektivní	k2eAgFnSc1d1
přitažlivost	přitažlivost	k1gFnSc1
ještě	ještě	k6eAd1
zmenšována	zmenšovat	k5eAaImNgFnS
vlivem	vlivem	k7c2
slapových	slapový	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
pozoruhodného	pozoruhodný	k2eAgInSc2d1
meteoritu	meteorit	k1gInSc2
Kaidun	Kaidun	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1980	#num#	k4
spadl	spadnout	k5eAaPmAgInS
na	na	k7c4
sovětskou	sovětský	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
v	v	k7c6
Jemenu	Jemen	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
bere	brát	k5eAaImIp3nS
v	v	k7c4
úvahu	úvaha	k1gFnSc4
že	že	k8xS
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Phobosu	Phobos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
ale	ale	k8xC
těžké	těžký	k2eAgNnSc4d1
s	s	k7c7
určitostí	určitost	k1gFnSc7
potvrdit	potvrdit	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
o	o	k7c4
složení	složení	k1gNnSc4
tohoto	tento	k3xDgInSc2
měsíce	měsíc	k1gInSc2
toho	ten	k3xDgNnSc2
zatím	zatím	k6eAd1
víme	vědět	k5eAaImIp1nP
jen	jen	k9
málo	málo	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
oběžné	oběžný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
</s>
<s>
Oběh	oběh	k1gInSc1
Phobosu	Phobos	k1gInSc2
a	a	k8xC
Deimosu	Deimos	k1gInSc2
kolem	kolem	k7c2
Marsu	Mars	k1gInSc2
(	(	kIx(
<g/>
měřítko	měřítko	k1gNnSc1
zachováno	zachován	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Působením	působení	k1gNnSc7
slapových	slapový	k2eAgFnPc2d1
sil	síla	k1gFnPc2
se	se	k3xPyFc4
stále	stále	k6eAd1
zkracuje	zkracovat	k5eAaImIp3nS
doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
Phobosu	Phobos	k1gInSc2
kolem	kolem	k7c2
Marsu	Mars	k1gInSc2
a	a	k8xC
snižuje	snižovat	k5eAaImIp3nS
se	se	k3xPyFc4
její	její	k3xOp3gFnSc1
průměrná	průměrný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
(	(	kIx(
<g/>
velikost	velikost	k1gFnSc1
velké	velký	k2eAgFnSc2d1
poloosy	poloosa	k1gFnSc2
oběžné	oběžný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
rychlostí	rychlost	k1gFnSc7
1,8	1,8	k4
metru	metr	k1gInSc2
za	za	k7c2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
buď	buď	k8xC
zhruba	zhruba	k6eAd1
za	za	k7c4
50	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
zřítí	zřítit	k5eAaPmIp3nS
na	na	k7c4
povrch	povrch	k1gInSc4
Marsu	Mars	k1gInSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
mnohem	mnohem	k6eAd1
pravděpodobněji	pravděpodobně	k6eAd2
po	po	k7c6
poklesu	pokles	k1gInSc6
velké	velký	k2eAgFnSc2d1
poloosy	poloosa	k1gFnSc2
pod	pod	k7c4
přibližně	přibližně	k6eAd1
8	#num#	k4
400	#num#	k4
km	km	kA
(	(	kIx(
<g/>
pod	pod	k7c4
tzv.	tzv.	kA
Rocheovu	Rocheův	k2eAgFnSc4d1
mez	mez	k1gFnSc4
<g/>
)	)	kIx)
bude	být	k5eAaImBp3nS
slapovými	slapový	k2eAgFnPc7d1
silami	síla	k1gFnPc7
roztrhán	roztrhán	k2eAgInSc4d1
a	a	k8xC
vytvoří	vytvořit	k5eAaPmIp3nS
kolem	kolem	k7c2
planety	planeta	k1gFnSc2
prstenec	prstenec	k1gInSc1
<g/>
,	,	kIx,
podobný	podobný	k2eAgInSc1d1
Saturnovu	Saturnův	k2eAgInSc3d1
<g/>
.	.	kIx.
</s>
<s>
Koncem	koncem	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ruský	ruský	k2eAgMnSc1d1
astrofyzik	astrofyzik	k1gMnSc1
Josif	Josif	k1gMnSc1
Samuilovič	Samuilovič	k1gMnSc1
Šklovskij	Šklovskij	k1gMnSc1
vyšel	vyjít	k5eAaPmAgMnS
z	z	k7c2
nepřesných	přesný	k2eNgInPc2d1
údajů	údaj	k1gInPc2
o	o	k7c6
velikosti	velikost	k1gFnSc6
Phobosu	Phobos	k1gInSc2
<g/>
,	,	kIx,
o	o	k7c4
zrychlování	zrychlování	k1gNnSc4
oběhu	oběh	k1gInSc2
měsíce	měsíc	k1gInSc2
kolem	kolem	k7c2
planety	planeta	k1gFnSc2
a	a	k8xC
z	z	k7c2
předpokladů	předpoklad	k1gInPc2
o	o	k7c6
hustotě	hustota	k1gFnSc6
marsovské	marsovský	k2eAgFnSc2d1
atmosféry	atmosféra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospěl	dochvít	k5eAaPmAgMnS
k	k	k7c3
závěru	závěr	k1gInSc3
o	o	k7c6
mimořádně	mimořádně	k6eAd1
nízké	nízký	k2eAgFnSc3d1
průměrné	průměrný	k2eAgFnSc3d1
hustotě	hustota	k1gFnSc3
tohoto	tento	k3xDgNnSc2
tělesa	těleso	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
dala	dát	k5eAaPmAgFnS
vysvětlit	vysvětlit	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
měsíc	měsíc	k1gInSc1
je	být	k5eAaImIp3nS
dutý	dutý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
průměru	průměr	k1gInSc6
asi	asi	k9
16	#num#	k4
km	km	kA
by	by	k9
tloušťka	tloušťka	k1gFnSc1
ocelové	ocelový	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
činila	činit	k5eAaImAgFnS
asi	asi	k9
6	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
a	a	k8xC
umělém	umělý	k2eAgInSc6d1
původu	původ	k1gInSc6
Phobosu	Phobos	k1gInSc2
vyslovili	vyslovit	k5eAaPmAgMnP
i	i	k9
poradce	poradce	k1gMnSc1
prezidenta	prezident	k1gMnSc2
D.	D.	kA
D.	D.	kA
Eisenhowera	Eisenhowera	k1gFnSc1
Siegfried	Siegfried	k1gMnSc1
F.	F.	kA
Singer	Singer	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
později	pozdě	k6eAd2
též	též	k9
matematik	matematik	k1gMnSc1
Raymond	Raymond	k1gMnSc1
H.	H.	kA
Wilson	Wilson	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
z	z	k7c2
NASA	NASA	kA
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
jednoznačně	jednoznačně	k6eAd1
prokázáno	prokázat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
přirozené	přirozený	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pozorování	pozorování	k1gNnSc1
na	na	k7c6
místě	místo	k1gNnSc6
</s>
<s>
Přechod	přechod	k1gInSc1
Phobosu	Phobos	k1gInSc2
přes	přes	k7c4
Slunce	slunce	k1gNnSc4
snímaný	snímaný	k2eAgMnSc1d1
z	z	k7c2
vozítka	vozítko	k1gNnSc2
Opportunity	Opportunita	k1gFnSc2
</s>
<s>
Při	při	k7c6
pohledu	pohled	k1gInSc6
z	z	k7c2
Phobosu	Phobos	k1gInSc2
by	by	kYmCp3nS
Mars	Mars	k1gInSc1
vyhlížel	vyhlížet	k5eAaImAgInS
6400	#num#	k4
<g/>
krát	krát	k6eAd1
větší	veliký	k2eAgFnSc4d2
a	a	k8xC
2500	#num#	k4
<g/>
krát	krát	k6eAd1
jasnější	jasný	k2eAgFnSc1d2
než	než	k8xS
úplněk	úplněk	k1gInSc1
pozemského	pozemský	k2eAgInSc2d1
Měsíce	měsíc	k1gInSc2
při	při	k7c6
pozorování	pozorování	k1gNnSc6
ze	z	k7c2
Země	zem	k1gFnSc2
a	a	k8xC
zabíral	zabírat	k5eAaImAgInS
by	by	kYmCp3nS
téměř	téměř	k6eAd1
čtvrtinu	čtvrtina	k1gFnSc4
oblohy	obloha	k1gFnSc2
měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Naopak	naopak	k6eAd1
Phobos	Phobos	k1gInSc1
pozorovaný	pozorovaný	k2eAgInSc1d1
z	z	k7c2
povrchu	povrch	k1gInSc2
Marsu	Mars	k1gInSc2
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
pouze	pouze	k6eAd1
do	do	k7c2
areografických	areografický	k2eAgFnPc2d1
šířek	šířka	k1gFnPc2
přibližně	přibližně	k6eAd1
70,4	70,4	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
zdánlivý	zdánlivý	k2eAgInSc4d1
průměr	průměr	k1gInSc4
přibližně	přibližně	k6eAd1
třetiny	třetina	k1gFnPc1
průměru	průměr	k1gInSc2
pozemského	pozemský	k2eAgInSc2d1
Měsíce	měsíc	k1gInSc2
(	(	kIx(
<g/>
pozorováno	pozorovat	k5eAaImNgNnS
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
)	)	kIx)
<g/>
;	;	kIx,
protože	protože	k8xS
obíhá	obíhat	k5eAaImIp3nS
kolem	kolem	k7c2
planety	planeta	k1gFnSc2
v	v	k7c6
průměrné	průměrný	k2eAgFnSc6d1
výši	výše	k1gFnSc6,k1gFnSc6wB
pouhých	pouhý	k2eAgInPc2d1
5	#num#	k4
982	#num#	k4
km	km	kA
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
zdánlivý	zdánlivý	k2eAgInSc1d1
průměr	průměr	k1gInSc1
se	se	k3xPyFc4
značně	značně	k6eAd1
mění	měnit	k5eAaImIp3nS
se	s	k7c7
vzdáleností	vzdálenost	k1gFnSc7
od	od	k7c2
pozorovatele	pozorovatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
při	při	k7c6
jeho	jeho	k3xOp3gInSc6
východu	východ	k1gInSc6
nad	nad	k7c4
obzor	obzor	k1gInSc4
má	mít	k5eAaImIp3nS
zdánlivý	zdánlivý	k2eAgInSc1d1
průměr	průměr	k1gInSc1
0,14	0,14	k4
<g/>
°	°	k?
pro	pro	k7c4
pozorovatele	pozorovatel	k1gMnPc4
na	na	k7c6
rovníku	rovník	k1gInSc6
<g/>
,	,	kIx,
při	při	k7c6
dosažení	dosažení	k1gNnSc6
zenitu	zenit	k1gInSc2
se	se	k3xPyFc4
zvětší	zvětšit	k5eAaPmIp3nS
až	až	k9
na	na	k7c4
0,20	0,20	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
obíhá	obíhat	k5eAaImIp3nS
kolem	kolem	k7c2
planety	planeta	k1gFnSc2
rychleji	rychle	k6eAd2
než	než	k8xS
je	být	k5eAaImIp3nS
její	její	k3xOp3gFnSc1
rotace	rotace	k1gFnSc1
<g/>
,	,	kIx,
vychází	vycházet	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
dvakrát	dvakrát	k6eAd1
za	za	k7c4
marsovský	marsovský	k2eAgInSc4d1
den	den	k1gInSc4
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
západě	západ	k1gInSc6
a	a	k8xC
zapadá	zapadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c6
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doba	doba	k1gFnSc1
setrvání	setrvání	k1gNnSc2
na	na	k7c6
obloze	obloha	k1gFnSc6
je	být	k5eAaImIp3nS
nejvýše	nejvýše	k6eAd1,k6eAd3
4,25	4,25	k4
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
oběh	oběh	k1gInSc1
Phobosu	Phobos	k1gInSc2
vůči	vůči	k7c3
pozorovateli	pozorovatel	k1gMnSc3
na	na	k7c6
povrchu	povrch	k1gInSc6
Marsu	Mars	k1gInSc2
trvá	trvat	k5eAaImIp3nS
11,1	11,1	k4
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Kosmický	kosmický	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
</s>
<s>
Záhadný	záhadný	k2eAgInSc1d1
monolit	monolit	k1gInSc1
na	na	k7c4
Phobosu	Phobosa	k1gFnSc4
vyfocený	vyfocený	k2eAgMnSc1d1
sondou	sonda	k1gFnSc7
Mars	Mars	k1gInSc1
Global	globat	k5eAaImAgMnS
Surveyor	Surveyor	k1gMnSc1
</s>
<s>
Phobos	Phobos	k1gInSc1
na	na	k7c6
snímku	snímek	k1gInSc6
sondy	sonda	k1gFnSc2
Viking	Viking	k1gMnSc1
1	#num#	k4
<g/>
,	,	kIx,
vlevo	vlevo	k6eAd1
nahoře	nahoře	k6eAd1
kráter	kráter	k1gInSc1
Stickney	Sticknea	k1gFnSc2
</s>
<s>
První	první	k4xOgInPc1
snímky	snímek	k1gInPc1
tohoto	tento	k3xDgInSc2
měsíce	měsíc	k1gInSc2
z	z	k7c2
blízka	blízko	k1gNnSc2
pořídily	pořídit	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
sonda	sonda	k1gFnSc1
Mariner	Marinero	k1gNnPc2
9	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
Viking	Viking	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jeho	jeho	k3xOp3gInSc3
detailnímu	detailní	k2eAgInSc3d1
průzkumu	průzkum	k1gInSc3
byly	být	k5eAaImAgFnP
určeny	určit	k5eAaPmNgFnP
sovětské	sovětský	k2eAgFnPc1d1
kosmické	kosmický	k2eAgFnPc1d1
sondy	sonda	k1gFnPc1
Fobos	Fobos	k1gMnSc1
1	#num#	k4
a	a	k8xC
Fobos	Fobos	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
měly	mít	k5eAaImAgInP
vysadit	vysadit	k5eAaPmF
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
povrchu	povrch	k1gInSc6
malé	malý	k2eAgInPc4d1
přistávací	přistávací	k2eAgInPc4d1
moduly	modul	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
první	první	k4xOgFnSc7
z	z	k7c2
nich	on	k3xPp3gFnPc2
bylo	být	k5eAaImAgNnS
ztraceno	ztratit	k5eAaPmNgNnS
spojení	spojení	k1gNnSc1
ještě	ještě	k6eAd1
během	během	k7c2
přeletu	přelet	k1gInSc2
od	od	k7c2
Země	zem	k1gFnSc2
k	k	k7c3
Marsu	Mars	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fobos	Fobos	k1gInSc1
2	#num#	k4
se	se	k3xPyFc4
přiblížil	přiblížit	k5eAaPmAgMnS
k	k	k7c3
měsíci	měsíc	k1gInSc3
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1989	#num#	k4
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
jen	jen	k9
191	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
přibližování	přibližování	k1gNnSc2
byla	být	k5eAaImAgFnS
pořízena	pořízen	k2eAgFnSc1d1
řada	řada	k1gFnSc1
snímků	snímek	k1gInPc2
Phobosu	Phobos	k1gInSc2
a	a	k8xC
uskutečnil	uskutečnit	k5eAaPmAgInS
se	se	k3xPyFc4
i	i	k9
jeho	jeho	k3xOp3gInSc1
spektroskopický	spektroskopický	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
poté	poté	k6eAd1
bylo	být	k5eAaImAgNnS
spojení	spojení	k1gNnSc1
se	s	k7c7
sondou	sonda	k1gFnSc7
ztraceno	ztratit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
snímkovaly	snímkovat	k5eAaImAgFnP
tento	tento	k3xDgInSc4
měsíc	měsíc	k1gInSc4
další	další	k2eAgFnSc2d1
sondy	sonda	k1gFnSc2
<g/>
:	:	kIx,
americký	americký	k2eAgInSc1d1
Mars	Mars	k1gInSc1
Global	globat	k5eAaImAgInS
Surveyor	Surveyor	k1gInSc4
v	v	k7c6
létech	léto	k1gNnPc6
1998	#num#	k4
a	a	k8xC
2003	#num#	k4
<g/>
,	,	kIx,
evropský	evropský	k2eAgInSc1d1
Mars	Mars	k1gInSc1
Express	express	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
a	a	k8xC
poté	poté	k6eAd1
opět	opět	k6eAd1
americký	americký	k2eAgInSc1d1
Mars	Mars	k1gInSc1
Reconnaissance	Reconnaissance	k1gFnSc2
Orbiter	Orbitra	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
Kanadská	kanadský	k2eAgFnSc1d1
kosmická	kosmický	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
financovala	financovat	k5eAaBmAgFnS
studii	studie	k1gFnSc4
pro	pro	k7c4
bezpilotní	bezpilotní	k2eAgFnSc4d1
misi	mise	k1gFnSc4
na	na	k7c4
Phobos	Phobos	k1gInSc4
známou	známá	k1gFnSc4
jako	jako	k8xS,k8xC
PRIME	prim	k1gInSc5
(	(	kIx(
<g/>
Phobos	Phobos	k1gInSc1
Reconnaissance	Reconnaissance	k1gFnSc2
and	and	k?
International	International	k1gFnSc1
Mars	Mars	k1gInSc1
Exploration	Exploration	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhované	navrhovaný	k2eAgNnSc1d1
místo	místo	k1gNnSc4
byl	být	k5eAaImAgInS
Phoboský	Phoboský	k2eAgInSc1d1
monolit	monolit	k1gInSc1
blízko	blízko	k7c2
Stickney	Sticknea	k1gFnSc2
kráteru	kráter	k1gInSc2
<g/>
,	,	kIx,
světlé	světlý	k2eAgNnSc1d1
místo	místo	k1gNnSc1
s	s	k7c7
nápadným	nápadný	k2eAgInSc7d1
stínem	stín	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronaut	astronaut	k1gMnSc1
a	a	k8xC
druhý	druhý	k4xOgMnSc1
muž	muž	k1gMnSc1
na	na	k7c6
Měsíci	měsíc	k1gInSc6
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc1
o	o	k7c6
něm	on	k3xPp3gInSc6
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Musíme	muset	k5eAaImIp1nP
navštívit	navštívit	k5eAaPmF
měsíce	měsíc	k1gInPc4
Marsu	Mars	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
Phobos	Phobos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
na	na	k7c6
něm	on	k3xPp3gNnSc6
‚	‚	k?
<g/>
Monolit	monolit	k1gInSc1
<g/>
‘	‘	k?
–	–	k?
velmi	velmi	k6eAd1
zvláštní	zvláštní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
na	na	k7c6
takhle	takhle	k6eAd1
malé	malý	k2eAgFnSc6d1
‚	‚	k?
<g/>
bramboře	brambora	k1gFnSc6
<g/>
‘	‘	k?
o	o	k7c6
průměru	průměr	k1gInSc6
něco	něco	k3yInSc4
přes	přes	k7c4
20	#num#	k4
kilometrů	kilometr	k1gInPc2
nemá	mít	k5eNaImIp3nS
co	co	k9
dělat	dělat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
vesmír	vesmír	k1gInSc1
nebo	nebo	k8xC
Bůh	bůh	k1gMnSc1
<g/>
,	,	kIx,
chcete	chtít	k5eAaImIp2nP
<g/>
-li	-li	k?
<g/>
…	…	k?
<g/>
“	“	k?
Mise	mise	k1gFnSc1
PRIME	prim	k1gInSc5
se	se	k3xPyFc4
ale	ale	k8xC
potýká	potýkat	k5eAaImIp3nS
s	s	k7c7
finančními	finanční	k2eAgInPc7d1
problémy	problém	k1gInPc7
a	a	k8xC
stále	stále	k6eAd1
nemá	mít	k5eNaImIp3nS
stanovený	stanovený	k2eAgInSc4d1
termín	termín	k1gInSc4
startu	start	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
rok	rok	k1gInSc4
2011	#num#	k4
Rusko	Rusko	k1gNnSc4
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
organizací	organizace	k1gFnSc7
ESA	eso	k1gNnSc2
plánovalo	plánovat	k5eAaImAgNnS
vyslání	vyslání	k1gNnSc4
sondy	sonda	k1gFnSc2
Fobos	Fobos	k1gMnSc1
Grunt	Grunt	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
odebrat	odebrat	k5eAaPmF
vzorky	vzorek	k1gInPc4
půdy	půda	k1gFnSc2
z	z	k7c2
povrchu	povrch	k1gInSc2
Phobosu	Phobos	k1gInSc2
a	a	k8xC
dopravit	dopravit	k5eAaPmF
je	být	k5eAaImIp3nS
k	k	k7c3
analýze	analýza	k1gFnSc3
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mise	mise	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
neúspěchem	neúspěch	k1gInSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
sonda	sonda	k1gFnSc1
2	#num#	k4
měsíce	měsíc	k1gInSc2
po	po	k7c6
startu	start	k1gInSc6
zanikla	zaniknout	k5eAaPmAgNnP
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
jména	jméno	k1gNnSc2
</s>
<s>
Phobos	Phobos	k1gInSc1
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
podle	podle	k7c2
Foba	Fobus	k1gMnSc2
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
ze	z	k7c2
synů	syn	k1gMnPc2
boha	bůh	k1gMnSc2
války	válka	k1gFnSc2
Área	Áres	k1gMnSc2
(	(	kIx(
<g/>
Marta	Marta	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Afrodity	Afrodita	k1gFnSc2
(	(	kIx(
<g/>
Venuše	Venuše	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Deimos	Deimos	k1gMnSc1
společně	společně	k6eAd1
stále	stále	k6eAd1
doprovázeli	doprovázet	k5eAaImAgMnP
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
boha	bůh	k1gMnSc4
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jména	jméno	k1gNnPc1
obou	dva	k4xCgInPc6
měsíců	měsíc	k1gInPc2
navrhl	navrhnout	k5eAaPmAgMnS
Henry	Henry	k1gMnSc1
Madan	Madan	k1gMnSc1
(	(	kIx(
<g/>
1838	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
Etonu	Eton	k1gInSc2
<g/>
,	,	kIx,
podle	podle	k7c2
citátu	citát	k1gInSc2
z	z	k7c2
XV	XV	kA
<g/>
.	.	kIx.
knihy	kniha	k1gFnSc2
eposu	epos	k1gInSc2
Ilias	Ilias	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
bůh	bůh	k1gMnSc1
války	válka	k1gFnSc2
povolává	povolávat	k5eAaImIp3nS
Strach	strach	k1gInSc1
(	(	kIx(
<g/>
Fobos	Fobos	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Hrůzu	hrůza	k1gFnSc4
(	(	kIx(
<g/>
Deimos	Deimos	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiných	jiný	k2eAgFnPc2d1
mytologických	mytologický	k2eAgFnPc2d1
pověstí	pověst	k1gFnPc2
byli	být	k5eAaImAgMnP
Deimos	Deimos	k1gMnSc1
a	a	k8xC
Fobos	Fobos	k1gMnSc1
koně	kůň	k1gMnPc1
<g/>
,	,	kIx,
zapřažení	zapřažený	k2eAgMnPc1d1
do	do	k7c2
Áreova	Áreův	k2eAgInSc2d1
válečného	válečný	k2eAgInSc2d1
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Phobos	Phobos	k1gInSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Jonathan	Jonathan	k1gMnSc1
Swift	Swift	k1gMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
Gulliverových	Gulliverův	k2eAgFnPc6d1
cestách	cesta	k1gFnPc6
popisuje	popisovat	k5eAaImIp3nS
objev	objev	k1gInSc4
dvou	dva	k4xCgInPc2
měsíčků	měsíček	k1gInPc2
Marsu	Mars	k1gInSc6
hvězdáři	hvězdář	k1gMnPc1
vymyšlené	vymyšlený	k2eAgFnSc2d1
země	zem	k1gFnSc2
Laputa	Laput	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Swiftův	Swiftův	k2eAgInSc1d1
satirický	satirický	k2eAgInSc1d1
román	román	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1726	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
půldruhého	půldruhý	k2eAgNnSc2d1
století	století	k1gNnSc2
před	před	k7c7
skutečným	skutečný	k2eAgInSc7d1
objevem	objev	k1gInSc7
Phobosu	Phobos	k1gInSc2
a	a	k8xC
Deimosu	Deimos	k1gInSc2
a	a	k8xC
žádný	žádný	k3yNgInSc1
tehdejší	tehdejší	k2eAgInSc1d1
hvězdářský	hvězdářský	k2eAgInSc1d1
dalekohled	dalekohled	k1gInSc1
nebyl	být	k5eNaImAgInS
dostatečně	dostatečně	k6eAd1
výkonný	výkonný	k2eAgInSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
skutečné	skutečný	k2eAgInPc4d1
měsíce	měsíc	k1gInPc4
odhalit	odhalit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Český	český	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
sci-fi	sci-fi	k1gNnSc2
J.	J.	kA
M.	M.	kA
Troska	troska	k1gFnSc1
v	v	k7c6
prvním	první	k4xOgInSc6
dílu	díl	k1gInSc6
trilogie	trilogie	k1gFnSc2
Zápas	zápas	k1gInSc4
s	s	k7c7
nebem	nebe	k1gNnSc7
(	(	kIx(
<g/>
první	první	k4xOgNnPc4
vydání	vydání	k1gNnPc4
1940	#num#	k4
<g/>
)	)	kIx)
popisuje	popisovat	k5eAaImIp3nS
let	let	k1gInSc4
svých	svůj	k3xOyFgMnPc2
hrdinů	hrdina	k1gMnPc2
k	k	k7c3
průzkumu	průzkum	k1gInSc3
Phobosu	Phobos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Měsíc	měsíc	k1gInSc1
Phobos	Phobos	k1gInSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
též	též	k9
ve	v	k7c6
sci-fi	sci-fi	k1gFnSc6
trilogii	trilogie	k1gFnSc6
o	o	k7c6
Marsu	Mars	k1gInSc6
spisovatele	spisovatel	k1gMnSc2
Kima	Kimus	k1gMnSc2
Stanley	Stanlea	k1gMnSc2
Robinsona	Robinson	k1gMnSc2
(	(	kIx(
<g/>
první	první	k4xOgMnPc1
angl.	angl.	k?
vydání	vydání	k1gNnSc6
v	v	k7c6
r.	r.	kA
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	on	k3xPp3gInPc4
vzbouřenci	vzbouřenec	k1gMnPc1
naveden	naveden	k2eAgMnSc1d1
na	na	k7c4
povrch	povrch	k1gInSc4
planety	planeta	k1gFnSc2
ve	v	k7c6
snaze	snaha	k1gFnSc6
zamezit	zamezit	k5eAaPmF
jeho	jeho	k3xOp3gNnPc4
využití	využití	k1gNnPc4
jako	jako	k8xC,k8xS
základnu	základna	k1gFnSc4
pro	pro	k7c4
útočící	útočící	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
vyslané	vyslaný	k2eAgFnPc4d1
ze	z	k7c2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
románu	román	k1gInSc6
Rajské	rajský	k2eAgFnSc2d1
fontány	fontána	k1gFnSc2
britského	britský	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Arthura	Arthur	k1gMnSc2
C.	C.	kA
Clarka	Clarek	k1gMnSc2
řeší	řešit	k5eAaImIp3nS
inženýři	inženýr	k1gMnPc1
na	na	k7c6
Marsu	Mars	k1gInSc6
problém	problém	k1gInSc1
pravidelné	pravidelný	k2eAgFnSc2d1
kolize	kolize	k1gFnSc2
Phobosu	Phobos	k1gInSc2
s	s	k7c7
plánovanou	plánovaný	k2eAgFnSc7d1
orbitální	orbitální	k2eAgFnSc7d1
geosynchronní	geosynchronní	k2eAgFnSc7d1
věží	věž	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
elegantní	elegantní	k2eAgNnSc1d1
řešení	řešení	k1gNnSc1
je	být	k5eAaImIp3nS
zvoleno	zvolit	k5eAaPmNgNnS
řízené	řízený	k2eAgNnSc1d1
rozkmitání	rozkmitání	k1gNnSc1
celé	celý	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
věže	věž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Povídka	povídka	k1gFnSc1
Střílejte	střílet	k5eAaImRp2nP
veverky	veverka	k1gFnPc4
(	(	kIx(
<g/>
rovněž	rovněž	k9
od	od	k7c2
A.	A.	kA
C.	C.	kA
Clarka	Clarka	k1gMnSc1
<g/>
)	)	kIx)
popisuje	popisovat	k5eAaImIp3nS
dobrodružství	dobrodružství	k1gNnSc4
agenta-astronauta	agenta-astronaut	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
na	na	k7c6
pustém	pustý	k2eAgInSc6d1
Phobosu	Phobos	k1gInSc6
ukrývá	ukrývat	k5eAaImIp3nS
před	před	k7c7
nepřátelským	přátelský	k2eNgInSc7d1
kosmickým	kosmický	k2eAgInSc7d1
křižníkem	křižník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Phobos	Phobos	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
zmíněn	zmínit	k5eAaPmNgInS
v	v	k7c6
povídce	povídka	k1gFnSc6
„	„	k?
<g/>
Marťanský	marťanský	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
“	“	k?
spisovatele	spisovatel	k1gMnSc2
Isaaca	Isaacus	k1gMnSc2
Asimova	Asimův	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Měsíc	měsíc	k1gInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
děje	děj	k1gInSc2
počítačové	počítačový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
Doom	Dooma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mark	Mark	k1gMnSc1
Watney	Watnea	k1gFnSc2
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
románu	román	k1gInSc2
Andyho	Andy	k1gMnSc2
Weira	Weiro	k1gNnSc2
Marťan	Marťan	k1gMnSc1
<g/>
,	,	kIx,
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
Phobos	Phobos	k1gMnSc1
jako	jako	k9
pomocníka	pomocník	k1gMnSc4
pro	pro	k7c4
navigaci	navigace	k1gFnSc4
při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
cestě	cesta	k1gFnSc6
ze	z	k7c2
základny	základna	k1gFnSc2
mise	mise	k1gFnSc2
ARES	Ares	k1gMnSc1
3	#num#	k4
na	na	k7c4
základnu	základna	k1gFnSc4
mise	mise	k1gFnSc2
ARES	Ares	k1gMnSc1
4	#num#	k4
kde	kde	k6eAd1
může	moct	k5eAaImIp3nS
využít	využít	k5eAaPmF
odletový	odletový	k2eAgInSc1d1
modul	modul	k1gInSc1
k	k	k7c3
opuštění	opuštění	k1gNnSc3
Marsu	Mars	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Precise	Precise	k1gFnSc1
mass	massa	k1gFnPc2
determination	determination	k1gInSc4
and	and	k?
the	the	k?
nature	natur	k1gMnSc5
of	of	k?
Phobos	Phobos	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geophysical	Geophysical	k1gFnSc1
Research	Researcha	k1gFnPc2
Letters	Lettersa	k1gFnPc2
<g/>
,	,	kIx,
Vol	vol	k6eAd1
<g/>
.	.	kIx.
37	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
SHAYLER	SHAYLER	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
J.	J.	kA
<g/>
;	;	kIx,
SALMON	SALMON	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
<g/>
;	;	kIx,
SHAYLER	SHAYLER	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
D.	D.	kA
Marswalk	Marswalk	k1gMnSc1
One	One	k1gMnSc1
-	-	kIx~
First	First	k1gMnSc1
steps	steps	k6eAd1
on	on	k3xPp3gMnSc1
a	a	k8xC
new	new	k?
planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
:	:	kIx,
Springer	Springer	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
85233	#num#	k4
<g/>
-	-	kIx~
<g/>
792	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Destination	Destination	k1gInSc4
Mars	Mars	k1gInSc1
<g/>
:	:	kIx,
Phobos	Phobos	k1gInSc1
and	and	k?
Deimos	Deimos	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CLARK	CLARK	kA
<g/>
,	,	kIx,
Stuart	Stuart	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cheap	Cheap	k1gInSc1
flights	flights	k6eAd1
to	ten	k3xDgNnSc4
Phobos	Phobos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Scientist	Scientist	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leden	leden	k1gInSc1
2010	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2745	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
28	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
262	#num#	k4
<g/>
-	-	kIx~
<g/>
4079	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
2	#num#	k4
Krátery	kráter	k1gInPc7
na	na	k7c4
Phobosu	Phobosa	k1gFnSc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://www.newscientist.com/article/dn4902	http://www.newscientist.com/article/dn4902	k4
<g/>
↑	↑	k?
solarsystem	solarsyst	k1gInSc7
<g/>
.	.	kIx.
<g/>
nasa	nasa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
procproto	procprota	k1gFnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.spaceref.com/news/viewpr.html?pid=22554	http://www.spaceref.com/news/viewpr.html?pid=22554	k4
<g/>
↑	↑	k?
www.astro.pef.zcu.cz	www.astro.pef.zcu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ASIMOV	ASIMOV	kA
<g/>
,	,	kIx,
Isaac	Isaac	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sny	sen	k1gInPc4
robotů	robot	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Mustang	mustang	k1gMnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7191	#num#	k4
<g/>
-	-	kIx~
<g/>
144	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Marťanský	marťanský	k2eAgInSc1d1
styl	styl	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
149	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Čeman	Čeman	k1gMnSc1
<g/>
,	,	kIx,
Róbert	Róbert	k1gMnSc1
a	a	k8xC
Pittich	Pittich	k1gMnSc1
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
Vesmír	vesmír	k1gInSc1
–	–	k?
1	#num#	k4
Sluneční	sluneční	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
,	,	kIx,
Mapa	mapa	k1gFnSc1
Slovakia	Slovakia	k1gFnSc1
Bratislava	Bratislava	k1gFnSc1
2002	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-8067-072-2	80-8067-072-2	k4
</s>
<s>
Vesmír	vesmír	k1gInSc1
<g/>
,	,	kIx,
Grygar	Grygar	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
Horský	Horský	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
<g/>
,	,	kIx,
Mayer	Mayer	k1gMnSc1
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Fobos	Fobos	k1gInSc1
1	#num#	k4
a	a	k8xC
Fobos	Fobos	k1gInSc1
2	#num#	k4
–	–	k?
ruské	ruský	k2eAgFnPc4d1
sondy	sonda	k1gFnPc4
určené	určený	k2eAgFnPc1d1
k	k	k7c3
průzkumu	průzkum	k1gInSc3
Phobosu	Phobos	k1gInSc2
</s>
<s>
Deimos	Deimos	k1gInSc4
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
měsíc	měsíc	k1gInSc4
planety	planeta	k1gFnSc2
Mars	Mars	k1gInSc1
</s>
<s>
Měsíce	měsíc	k1gInSc2
Marsu	Mars	k1gInSc2
</s>
<s>
Mars	Mars	k1gInSc1
(	(	kIx(
<g/>
planeta	planeta	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Phobos	Phobosa	k1gFnPc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Phobos	Phobosa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Let	let	k1gInSc1
kolem	kolem	k7c2
Phobosu	Phobos	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Animace	animace	k1gFnSc1
Phobosu	Phobos	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Záhadný	záhadný	k2eAgInSc1d1
monolit	monolit	k1gInSc1
na	na	k7c6
Marsově	Marsův	k2eAgInSc6d1
měsíci	měsíc	k1gInSc6
Phobos	Phobosa	k1gFnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Curiosity	Curiosita	k1gFnSc2
odfotilo	odfotit	k5eAaImAgNnS,k5eAaPmAgNnS
pôsobivé	pôsobivý	k2eAgFnPc4d1
zatmenie	zatmenie	k1gFnPc4
Slnka	Slnko	k1gNnSc2
na	na	k7c4
Marse	Mars	k1gInSc5
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mars	Mars	k1gInSc1
Povrch	povrch	k1gInSc1
</s>
<s>
Obecně	obecně	k6eAd1
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1
•	•	k?
Geologie	geologie	k1gFnSc1
•	•	k?
Podnebí	podnebí	k1gNnPc2
•	•	k?
Polární	polární	k2eAgFnSc2d1
čepičky	čepička	k1gFnSc2
•	•	k?
Stratigrafie	stratigrafie	k1gFnSc2
•	•	k?
Voda	Voda	k1gMnSc1
•	•	k?
Život	život	k1gInSc1
Oblasti	oblast	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
plání	pláň	k1gFnPc2
•	•	k?
Seznam	seznam	k1gInSc1
údolí	údolí	k1gNnSc2
•	•	k?
Seznam	seznam	k1gInSc4
kaňonů	kaňon	k1gInPc2
•	•	k?
Planum	Planum	k1gInSc1
Boreum	Boreum	k1gInSc1
•	•	k?
Planum	Planum	k1gInSc1
Australe	Austral	k1gMnSc5
•	•	k?
Hemisféra	hemisféra	k1gFnSc1
Cerberus	Cerberus	k1gMnSc1
•	•	k?
Vastitas	Vastitas	k1gInSc1
Borealis	Borealis	k1gFnSc1
•	•	k?
Iani	Iani	k1gNnPc2
Chaos	chaos	k1gInSc1
•	•	k?
Tharsis	Tharsis	k1gInSc1
•	•	k?
Ultimi	Ulti	k1gFnPc7
Scopuli	Scopule	k1gFnSc4
•	•	k?
Olympia	Olympia	k1gFnSc1
Undae	Unda	k1gFnSc2
Hory	hora	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
•	•	k?
Echus	Echus	k1gInSc1
Montes	Montes	k1gInSc1
•	•	k?
Elysium	elysium	k1gNnSc1
Planitia	Planitia	k1gFnSc1
Vulkány	vulkán	k1gInPc4
</s>
<s>
Alba	alba	k1gFnSc1
Patera	Patera	k1gMnSc1
•	•	k?
Albor	Albor	k1gMnSc1
Tholus	Tholus	k1gMnSc1
•	•	k?
Arsia	Arsia	k1gFnSc1
Mons	Mons	k1gInSc1
•	•	k?
Ascraeus	Ascraeus	k1gInSc1
Mons	Mons	k1gInSc1
•	•	k?
Biblis	Biblis	k1gInSc1
Tholus	Tholus	k1gInSc1
•	•	k?
Elysium	elysium	k1gNnSc1
Mons	Mons	k1gInSc1
•	•	k?
Hecates	Hecates	k1gInSc1
Tholus	Tholus	k1gInSc1
•	•	k?
Olympus	Olympus	k1gInSc1
Mons	Mons	k1gInSc1
•	•	k?
Pavonis	Pavonis	k1gInSc1
Mons	Mons	k1gInSc1
•	•	k?
Syrtis	Syrtis	k1gInSc1
Major	major	k1gMnSc1
•	•	k?
Tharsis	Tharsis	k1gInSc1
•	•	k?
Tharsis	Tharsis	k1gInSc1
Montes	Montes	k1gInSc1
</s>
<s>
Mars	Mars	k1gInSc1
as	as	k1gInSc1
seen	seena	k1gFnPc2
by	by	kYmCp3nS
the	the	k?
Hubble	Hubble	k1gMnSc5
Space	Space	k1gMnSc5
Telescope	Telescop	k1gMnSc5
Měsíce	měsíc	k1gInSc2
</s>
<s>
Phobos	Phobos	k1gInSc1
•	•	k?
Deimos	Deimos	k1gInSc1
Výzkum	výzkum	k1gInSc1
</s>
<s>
Kolonizace	kolonizace	k1gFnSc1
•	•	k?
Mars	Mars	k1gInSc1
Exploration	Exploration	k1gInSc1
Program	program	k1gInSc1
•	•	k?
Program	program	k1gInSc1
Fobos	Fobos	k1gInSc1
•	•	k?
Program	program	k1gInSc1
Viking	Viking	k1gMnSc1
•	•	k?
Mars	Mars	k1gMnSc1
Pathfinder	Pathfinder	k1gMnSc1
•	•	k?
Mars	Mars	k1gMnSc1
Science	Science	k1gFnSc2
Laboratory	Laborator	k1gInPc4
•	•	k?
InSight	InSight	k1gInSc1
•	•	k?
Mars	Mars	k1gInSc1
Exploration	Exploration	k1gInSc1
Rover	rover	k1gMnSc1
•	•	k?
Perseverance	perseverance	k1gFnSc2
•	•	k?
Mars	Mars	k1gInSc1
Orbiter	Orbiter	k1gInSc1
Camera	Camer	k1gMnSc2
•	•	k?
HiRISE	HiRISE	k1gMnSc2
•	•	k?
Hazcam	Hazcam	k1gInSc1
•	•	k?
Navcam	Navcam	k1gInSc1
•	•	k?
Pancam	Pancam	k1gInSc1
•	•	k?
High	High	k1gInSc1
Resolution	Resolution	k1gInSc1
Stereo	stereo	k2eAgMnSc2d1
Camera	Camer	k1gMnSc2
•	•	k?
Mars	Mars	k1gInSc1
Hand	Hand	k1gMnSc1
Lens	Lensa	k1gFnPc2
Imager	Imager	k1gMnSc1
•	•	k?
Terraformace	Terraformace	k1gFnSc2
•	•	k?
RIMFAX	RIMFAX	kA
•	•	k?
SuperCam	SuperCam	k1gInSc1
Meteority	meteorit	k1gInPc1
</s>
<s>
ALH	ALH	kA
84001	#num#	k4
•	•	k?
Chassigny	Chassigna	k1gFnSc2
•	•	k?
Kaidun	Kaidun	k1gMnSc1
•	•	k?
Shergotty	Shergotta	k1gFnSc2
•	•	k?
Nakla	Nakla	k1gMnSc1
Související	související	k2eAgInPc4d1
články	článek	k1gInPc4
</s>
<s>
Knihy	kniha	k1gFnPc1
o	o	k7c6
Marsu	Mars	k1gInSc6
•	•	k?
Sol	sol	k1gInSc1
•	•	k?
Mars	Mars	k1gInSc1
(	(	kIx(
<g/>
mytologie	mytologie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Planetární	planetární	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4284687-0	4284687-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85101019	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
237373955	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85101019	#num#	k4
</s>
