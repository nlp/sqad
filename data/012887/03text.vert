<p>
<s>
Plena	plena	k1gFnSc1	plena
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
oděvu	oděv	k1gInSc2	oděv
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
zachycování	zachycování	k1gNnSc3	zachycování
výkalů	výkal	k1gInPc2	výkal
a	a	k8xC	a
moče	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
ji	on	k3xPp3gFnSc4	on
používají	používat	k5eAaImIp3nP	používat
kojenci	kojenec	k1gMnPc1	kojenec
<g/>
,	,	kIx,	,
batolata	batole	k1gNnPc4	batole
<g/>
,	,	kIx,	,
a	a	k8xC	a
dospělí	dospělý	k2eAgMnPc1d1	dospělý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
trpící	trpící	k2eAgMnPc1d1	trpící
inkontinencí	inkontinence	k1gFnPc2	inkontinence
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zvláštních	zvláštní	k2eAgInPc6d1	zvláštní
případech	případ	k1gInPc6	případ
i	i	k9	i
zdraví	zdravý	k2eAgMnPc1d1	zdravý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
záchodu	záchod	k1gInSc2	záchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
plena	pleno	k1gNnSc2	pleno
původně	původně	k6eAd1	původně
označovalo	označovat	k5eAaImAgNnS	označovat
plátno	plátno	k1gNnSc1	plátno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
==	==	k?	==
</s>
</p>
<p>
<s>
Pleny	plena	k1gFnPc1	plena
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
absorbujících	absorbující	k2eAgFnPc2d1	absorbující
vrstev	vrstva	k1gFnPc2	vrstva
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
ručníkové	ručníkový	k2eAgFnSc2d1	ručníková
tkaniny	tkanina	k1gFnSc2	tkanina
či	či	k8xC	či
z	z	k7c2	z
jednorázových	jednorázový	k2eAgInPc2d1	jednorázový
absorbujících	absorbující	k2eAgInPc2d1	absorbující
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
použití	použití	k1gNnSc2	použití
látkových	látkový	k2eAgFnPc2d1	látková
nebo	nebo	k8xC	nebo
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
plenek	plenka	k1gFnPc2	plenka
je	být	k5eAaImIp3nS	být
individuální	individuální	k2eAgFnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
látkové	látkový	k2eAgFnPc1d1	látková
plenky	plenka	k1gFnPc1	plenka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dlouhodobější	dlouhodobý	k2eAgFnSc6d2	dlouhodobější
perspektivě	perspektiva	k1gFnSc6	perspektiva
určitě	určitě	k6eAd1	určitě
levnější	levný	k2eAgMnSc1d2	levnější
než	než	k8xS	než
jednorázové	jednorázový	k2eAgNnSc1d1	jednorázové
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc1	vliv
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
tvoří	tvořit	k5eAaImIp3nP	tvořit
také	také	k9	také
významné	významný	k2eAgInPc1d1	významný
faktory	faktor	k1gInPc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
odstartovaly	odstartovat	k5eAaPmAgFnP	odstartovat
polemiky	polemika	k1gFnPc4	polemika
počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
financovala	financovat	k5eAaBmAgFnS	financovat
firma	firma	k1gFnSc1	firma
Procter	Procter	k1gMnSc1	Procter
&	&	k?	&
Gamble	Gamble	k1gFnSc1	Gamble
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
čelila	čelit	k5eAaImAgFnS	čelit
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
kritice	kritika	k1gFnSc3	kritika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
produkovala	produkovat	k5eAaImAgFnS	produkovat
většinou	většinou	k6eAd1	většinou
jednorázové	jednorázový	k2eAgFnPc4d1	jednorázová
plenky	plenka	k1gFnPc4	plenka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
drtivě	drtivě	k6eAd1	drtivě
převládá	převládat	k5eAaImIp3nS	převládat
trh	trh	k1gInSc1	trh
s	s	k7c7	s
jednorázovými	jednorázový	k2eAgFnPc7d1	jednorázová
plenami	plena	k1gFnPc7	plena
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
dětských	dětský	k2eAgFnPc2d1	dětská
plen	plena	k1gFnPc2	plena
je	být	k5eAaImIp3nS	být
poměr	poměr	k1gInSc1	poměr
vyrovnanější	vyrovnaný	k2eAgInSc1d2	vyrovnanější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Látkové	látkový	k2eAgMnPc4d1	látkový
===	===	k?	===
</s>
</p>
<p>
<s>
Látkové	látkový	k2eAgFnPc1d1	látková
pleny	plena	k1gFnPc1	plena
jsou	být	k5eAaImIp3nP	být
pratelné	pratelný	k2eAgFnPc1d1	pratelná
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
použitelné	použitelný	k2eAgNnSc1d1	použitelné
a	a	k8xC	a
méně	málo	k6eAd2	málo
zatěžují	zatěžovat	k5eAaImIp3nP	zatěžovat
odpadní	odpadní	k2eAgFnPc4d1	odpadní
skládky	skládka	k1gFnPc4	skládka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejích	její	k3xOp3gInPc6	její
praní	praný	k2eAgMnPc1d1	praný
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
prací	prací	k2eAgInPc1d1	prací
prostředky	prostředek	k1gInPc1	prostředek
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
používající	používající	k2eAgFnSc2d1	používající
látkové	látkový	k2eAgFnSc2d1	látková
plenky	plenka	k1gFnSc2	plenka
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
začít	začít	k5eAaPmF	začít
používat	používat	k5eAaImF	používat
toaletu	toaleta	k1gFnSc4	toaleta
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
látka	látka	k1gFnSc1	látka
udržuje	udržovat	k5eAaImIp3nS	udržovat
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
dítě	dítě	k1gNnSc4	dítě
vyrušovat	vyrušovat	k5eAaImF	vyrušovat
nebo	nebo	k8xC	nebo
aspoň	aspoň	k9	aspoň
připomíná	připomínat	k5eAaImIp3nS	připomínat
tělesné	tělesný	k2eAgFnPc4d1	tělesná
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
děti	dítě	k1gFnPc1	dítě
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
průměrně	průměrně	k6eAd1	průměrně
6000	[number]	k4	6000
výměn	výměna	k1gFnPc2	výměna
plenek	plenka	k1gFnPc2	plenka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vyhodí	vyhodit	k5eAaPmIp3nS	vyhodit
na	na	k7c4	na
odpadní	odpadní	k2eAgFnSc4d1	odpadní
skládku	skládka	k1gFnSc4	skládka
<g/>
,	,	kIx,	,
bavlněné	bavlněný	k2eAgFnPc1d1	bavlněná
plenky	plenka	k1gFnPc1	plenka
se	se	k3xPyFc4	se
rozloží	rozložit	k5eAaPmIp3nP	rozložit
během	během	k7c2	během
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Látkové	látkový	k2eAgFnPc1d1	látková
plenky	plenka	k1gFnPc1	plenka
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
dostupnější	dostupný	k2eAgFnSc1d2	dostupnější
<g/>
.	.	kIx.	.
</s>
<s>
Naformované	naformovaný	k2eAgFnPc1d1	naformovaný
látkové	látkový	k2eAgFnPc1d1	látková
plenky	plenka	k1gFnPc1	plenka
s	s	k7c7	s
uzávěry	uzávěr	k1gInPc7	uzávěr
nebo	nebo	k8xC	nebo
plenky	plenka	k1gFnPc1	plenka
"	"	kIx"	"
<g/>
všechno	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
all-in-one	allnn	k1gMnSc5	all-in-on
<g/>
)	)	kIx)	)
s	s	k7c7	s
obalem	obal	k1gInSc7	obal
chránícím	chránící	k2eAgFnPc3d1	chránící
před	před	k7c7	před
vlhkostí	vlhkost	k1gFnSc7	vlhkost
jsou	být	k5eAaImIp3nP	být
teď	teď	k6eAd1	teď
dostupné	dostupný	k2eAgFnPc1d1	dostupná
jako	jako	k8xC	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
starším	starý	k2eAgFnPc3d2	starší
překládaným	překládaný	k2eAgFnPc3d1	překládaná
a	a	k8xC	a
sepnutým	sepnutý	k2eAgFnPc3d1	sepnutá
plenkám	plenka	k1gFnPc3	plenka
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
americká	americký	k2eAgNnPc1d1	americké
města	město	k1gNnPc1	město
nabízejí	nabízet	k5eAaImIp3nP	nabízet
plenkový	plenkový	k2eAgInSc4d1	plenkový
servis	servis	k1gInSc4	servis
–	–	k?	–
za	za	k7c4	za
poplatek	poplatek	k1gInSc4	poplatek
jsou	být	k5eAaImIp3nP	být
dodávány	dodáván	k2eAgFnPc1d1	dodávána
čisté	čistý	k2eAgFnPc1d1	čistá
plenky	plenka	k1gFnPc1	plenka
a	a	k8xC	a
sbírány	sbírán	k2eAgFnPc1d1	sbírána
špinavé	špinavý	k2eAgFnPc1d1	špinavá
<g/>
.	.	kIx.	.
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1	látková
plenky	plenka	k1gFnPc1	plenka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
používané	používaný	k2eAgInPc1d1	používaný
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bezplínkovou	bezplínkův	k2eAgFnSc7d1	bezplínkův
komunikační	komunikační	k2eAgFnSc7d1	komunikační
metodou	metoda	k1gFnSc7	metoda
jako	jako	k8xS	jako
pojistka	pojistka	k1gFnSc1	pojistka
proti	proti	k7c3	proti
nehodě	nehoda	k1gFnSc3	nehoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jednorázové	jednorázový	k2eAgMnPc4d1	jednorázový
===	===	k?	===
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
plenky	plenka	k1gFnPc1	plenka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
látce	látka	k1gFnSc6	látka
podobného	podobný	k2eAgInSc2d1	podobný
vodotěsného	vodotěsný	k2eAgInSc2d1	vodotěsný
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
vlhkost	vlhkost	k1gFnSc1	vlhkost
odvádějící	odvádějící	k2eAgFnSc2d1	odvádějící
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
absorbujícího	absorbující	k2eAgNnSc2d1	absorbující
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
jednorázové	jednorázový	k2eAgFnSc6d1	jednorázová
plence	plenka	k1gFnSc6	plenka
udělal	udělat	k5eAaPmAgMnS	udělat
Pauli	Paule	k1gFnSc4	Paule
Strǒ	Strǒ	k1gMnSc1	Strǒ
ze	z	k7c2	z
Švédska	Švédsko	k1gNnSc2	Švédsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
plenky	plenka	k1gFnPc1	plenka
měly	mít	k5eAaImAgFnP	mít
vnitřek	vnitřek	k1gInSc4	vnitřek
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
vrstev	vrstva	k1gFnPc2	vrstva
kapesníkového	kapesníkový	k2eAgInSc2d1	kapesníkový
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
schopny	schopen	k2eAgInPc1d1	schopen
udržet	udržet	k5eAaPmF	udržet
100	[number]	k4	100
ml	ml	kA	ml
moče	moč	k1gFnSc2	moč
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
jedno	jeden	k4xCgNnSc4	jeden
močení	močení	k1gNnSc4	močení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roku	rok	k1gInSc6	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
pro	pro	k7c4	pro
absorpční	absorpční	k2eAgNnSc4d1	absorpční
jádro	jádro	k1gNnSc4	jádro
používat	používat	k5eAaImF	používat
celulóza	celulóza	k1gFnSc1	celulóza
a	a	k8xC	a
jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
plenky	plenka	k1gFnPc1	plenka
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
populárnější	populární	k2eAgFnPc1d2	populárnější
u	u	k7c2	u
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
mohly	moct	k5eAaImAgInP	moct
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
západě	západ	k1gInSc6	západ
začaly	začít	k5eAaPmAgFnP	začít
masově	masově	k6eAd1	masově
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
postkomunistických	postkomunistický	k2eAgFnPc2d1	postkomunistická
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
pleny	plena	k1gFnPc1	plena
dostaly	dostat	k5eAaPmAgFnP	dostat
až	až	k6eAd1	až
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
pleny	plena	k1gFnPc1	plena
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
při	při	k7c6	při
běžném	běžný	k2eAgNnSc6d1	běžné
nošení	nošení	k1gNnSc6	nošení
pojmout	pojmout	k5eAaPmF	pojmout
až	až	k9	až
okolo	okolo	k7c2	okolo
jednoho	jeden	k4xCgInSc2	jeden
litru	litr	k1gInSc2	litr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dospělí	dospělí	k1gMnPc1	dospělí
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
diskrétnější	diskrétní	k2eAgFnSc4d2	diskrétnější
<g/>
,	,	kIx,	,
lehčí	lehký	k2eAgFnSc4d2	lehčí
<g/>
,	,	kIx,	,
nezapáchají	zapáchat	k5eNaImIp3nP	zapáchat
<g/>
,	,	kIx,	,
vydrží	vydržet	k5eAaPmIp3nP	vydržet
déle	dlouho	k6eAd2	dlouho
suché	suchý	k2eAgFnPc1d1	suchá
a	a	k8xC	a
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
nosit	nosit	k5eAaImF	nosit
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
plenky	plenka	k1gFnPc1	plenka
předstihly	předstihnout	k5eAaPmAgFnP	předstihnout
prakticky	prakticky	k6eAd1	prakticky
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
vyspělém	vyspělý	k2eAgInSc6d1	vyspělý
světě	svět	k1gInSc6	svět
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
látkové	látkový	k2eAgFnSc2d1	látková
plenky	plenka	k1gFnSc2	plenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
jen	jen	k9	jen
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
přibližně	přibližně	k6eAd1	přibližně
18	[number]	k4	18
miliard	miliarda	k4xCgFnPc2	miliarda
kusů	kus	k1gInPc2	kus
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
plenek	plenka	k1gFnPc2	plenka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
plenky	plenka	k1gFnPc1	plenka
se	se	k3xPyFc4	se
těžko	těžko	k6eAd1	těžko
zpracují	zpracovat	k5eAaPmIp3nP	zpracovat
a	a	k8xC	a
jejích	její	k3xOp3gMnPc2	její
materiály	materiál	k1gInPc4	materiál
se	se	k3xPyFc4	se
v	v	k7c6	v
odpadní	odpadní	k2eAgFnSc6d1	odpadní
skládce	skládka	k1gFnSc6	skládka
podle	podle	k7c2	podle
některých	některý	k3yIgNnPc2	některý
studií	studio	k1gNnPc2	studio
rozloží	rozložit	k5eAaPmIp3nS	rozložit
až	až	k6eAd1	až
za	za	k7c4	za
asi	asi	k9	asi
500	[number]	k4	500
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
plenky	plenka	k1gFnPc1	plenka
odvádějí	odvádět	k5eAaImIp3nP	odvádět
vlhkost	vlhkost	k1gFnSc4	vlhkost
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
někdy	někdy	k6eAd1	někdy
nepoznají	poznat	k5eNaPmIp3nP	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
vlhké	vlhký	k2eAgInPc1d1	vlhký
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
děti	dítě	k1gFnPc1	dítě
učí	učit	k5eAaImIp3nP	učit
na	na	k7c4	na
toaletu	toaleta	k1gFnSc4	toaleta
až	až	k6eAd1	až
od	od	k7c2	od
3	[number]	k4	3
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
proto	proto	k8xC	proto
potřebovat	potřebovat	k5eAaImF	potřebovat
až	až	k9	až
8000	[number]	k4	8000
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
plenek	plenka	k1gFnPc2	plenka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
látkové	látkový	k2eAgFnPc1d1	látková
plenky	plenka	k1gFnPc1	plenka
stojí	stát	k5eAaImIp3nP	stát
víc	hodně	k6eAd2	hodně
za	za	k7c4	za
kus	kus	k1gInSc4	kus
<g/>
,	,	kIx,	,
jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
plenky	plenka	k1gFnPc1	plenka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
perspektivě	perspektiva	k1gFnSc6	perspektiva
mnohem	mnohem	k6eAd1	mnohem
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
plenky	plenka	k1gFnPc1	plenka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
chemikálie	chemikálie	k1gFnPc1	chemikálie
zahrnuté	zahrnutý	k2eAgFnPc1d1	zahrnutá
nechtěně	chtěně	k6eNd1	chtěně
během	během	k7c2	během
produkce	produkce	k1gFnSc2	produkce
i	i	k9	i
záměrně	záměrně	k6eAd1	záměrně
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
absorpce	absorpce	k1gFnSc2	absorpce
a	a	k8xC	a
sání	sání	k1gNnSc2	sání
vlhkosti	vlhkost	k1gFnSc2	vlhkost
od	od	k7c2	od
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
udržuje	udržovat	k5eAaImIp3nS	udržovat
kůži	kůže	k1gFnSc4	kůže
suchou	suchý	k2eAgFnSc4d1	suchá
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
podráždit	podráždit	k5eAaPmF	podráždit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnSc2	užití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Děti	dítě	k1gFnPc1	dítě
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
dětí	dítě	k1gFnPc2	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
výchově	výchova	k1gFnSc6	výchova
<g/>
,	,	kIx,	,
typu	typ	k1gInSc3	typ
plenek	plenka	k1gFnPc2	plenka
<g/>
,	,	kIx,	,
rodičovských	rodičovský	k2eAgInPc6d1	rodičovský
zvycích	zvyk	k1gInPc6	zvyk
a	a	k8xC	a
osobnosti	osobnost	k1gFnPc1	osobnost
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
plenky	plenka	k1gFnPc4	plenka
<g/>
,	,	kIx,	,
když	když	k8xS	když
nespí	spát	k5eNaImIp3nS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
některé	některý	k3yIgFnPc1	některý
děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
denní	denní	k2eAgFnSc7d1	denní
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
s	s	k7c7	s
noční	noční	k2eAgFnSc7d1	noční
kontrolou	kontrola	k1gFnSc7	kontrola
močení	močení	k1gNnSc2	močení
až	až	k6eAd1	až
do	do	k7c2	do
8	[number]	k4	8
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
i	i	k9	i
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
mnoha	mnoho	k4c7	mnoho
důvody	důvod	k1gInPc7	důvod
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
produkcí	produkce	k1gFnSc7	produkce
antidiuretického	antidiuretický	k2eAgInSc2d1	antidiuretický
hormonu	hormon	k1gInSc2	hormon
(	(	kIx(	(
<g/>
ADH	ADH	kA	ADH
<g/>
)	)	kIx)	)
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
důvody	důvod	k1gInPc1	důvod
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
ovládáním	ovládání	k1gNnSc7	ovládání
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
a	a	k8xC	a
emocionální	emocionální	k2eAgInPc4d1	emocionální
problémy	problém	k1gInPc4	problém
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
emocionální	emocionální	k2eAgInPc1d1	emocionální
problémy	problém	k1gInPc1	problém
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
častá	častý	k2eAgFnSc1d1	častá
příčina	příčina	k1gFnSc1	příčina
než	než	k8xS	než
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
starší	starý	k2eAgFnPc1d2	starší
děti	dítě	k1gFnPc1	dítě
též	též	k9	též
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
plenky	plenka	k1gFnPc1	plenka
během	během	k7c2	během
cestování	cestování	k1gNnSc2	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
děti	dítě	k1gFnPc1	dítě
mohou	moct	k5eAaImIp3nP	moct
použít	použít	k5eAaPmF	použít
větší	veliký	k2eAgFnSc4d2	veliký
verzi	verze	k1gFnSc4	verze
standardních	standardní	k2eAgFnPc2d1	standardní
plenek	plenka	k1gFnPc2	plenka
(	(	kIx(	(
<g/>
junior	junior	k1gMnSc1	junior
plenky	plenka	k1gFnSc2	plenka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
speciální	speciální	k2eAgFnPc4d1	speciální
plenky	plenka	k1gFnPc4	plenka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
a	a	k8xC	a
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
upevnění	upevnění	k1gNnSc4	upevnění
nebo	nebo	k8xC	nebo
pomoc	pomoc	k1gFnSc4	pomoc
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dospělí	dospělí	k1gMnPc1	dospělí
===	===	k?	===
</s>
</p>
<p>
<s>
Dospělí	dospělí	k1gMnPc1	dospělí
pleny	plena	k1gFnSc2	plena
nosí	nosit	k5eAaImIp3nP	nosit
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mají	mít	k5eAaImIp3nP	mít
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
kontinencí	kontinence	k1gFnSc7	kontinence
moči	moč	k1gFnSc2	moč
či	či	k8xC	či
stolice	stolice	k1gFnSc2	stolice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
především	především	k9	především
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
po	po	k7c6	po
urázech	uráz	k1gInPc6	uráz
<g/>
,	,	kIx,	,
během	během	k7c2	během
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
v	v	k7c6	v
případech	případ	k1gInPc6	případ
poruch	porucha	k1gFnPc2	porucha
režimu	režim	k1gInSc2	režim
močení	močení	k1gNnSc2	močení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
enuréza	enuréza	k1gFnSc1	enuréza
<g/>
,	,	kIx,	,
polyurie	polyurie	k1gFnSc1	polyurie
či	či	k8xC	či
hyperaktivní	hyperaktivní	k2eAgInSc1d1	hyperaktivní
močový	močový	k2eAgInSc1d1	močový
měchýř	měchýř	k1gInSc1	měchýř
a	a	k8xC	a
zejména	zejména	k9	zejména
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vysokém	vysoký	k2eAgInSc6d1	vysoký
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělí	k1gMnPc1	dospělí
častěji	často	k6eAd2	často
volí	volit	k5eAaImIp3nP	volit
spíše	spíše	k9	spíše
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
podpůrných	podpůrný	k2eAgFnPc2d1	podpůrná
(	(	kIx(	(
<g/>
prevenčních	prevenční	k2eAgFnPc2d1	prevenční
<g/>
)	)	kIx)	)
inkontinenčních	inkontinenční	k2eAgFnPc2d1	inkontinenční
pomůcek	pomůcka	k1gFnPc2	pomůcka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
již	již	k6eAd1	již
neslouží	sloužit	k5eNaImIp3nP	sloužit
jako	jako	k9	jako
pleny	plena	k1gFnPc1	plena
k	k	k7c3	k
zachytávání	zachytávání	k1gNnSc3	zachytávání
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
typicky	typicky	k6eAd1	typicky
například	například	k6eAd1	například
jen	jen	k9	jen
moči	moč	k1gFnSc2	moč
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
různé	různý	k2eAgFnPc1d1	různá
inkontinenční	inkontinenční	k2eAgFnPc1d1	inkontinenční
vložky	vložka	k1gFnPc1	vložka
<g/>
.	.	kIx.	.
</s>
<s>
Zdraví	zdraví	k1gNnSc1	zdraví
dospělí	dospělí	k1gMnPc1	dospělí
pleny	plena	k1gFnSc2	plena
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
není	být	k5eNaImIp3nS	být
kam	kam	k6eAd1	kam
ulevit	ulevit	k5eAaPmF	ulevit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
potápěče	potápěč	k1gMnPc4	potápěč
v	v	k7c6	v
suchých	suchý	k2eAgInPc6d1	suchý
oblecích	oblek	k1gInPc6	oblek
<g/>
,	,	kIx,	,
pracovníky	pracovník	k1gMnPc7	pracovník
v	v	k7c6	v
protichemických	protichemický	k2eAgInPc6d1	protichemický
oblecích	oblek	k1gInPc6	oblek
<g/>
,	,	kIx,	,
výškové	výškový	k2eAgFnPc4d1	výšková
pracovníky	pracovník	k1gMnPc7	pracovník
<g/>
,	,	kIx,	,
kosmonauty	kosmonaut	k1gMnPc7	kosmonaut
<g/>
,	,	kIx,	,
řidiče	řidič	k1gMnPc4	řidič
<g/>
,	,	kIx,	,
či	či	k8xC	či
jiné	jiný	k2eAgFnPc4d1	jiná
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
zkrátka	zkrátka	k6eAd1	zkrátka
problém	problém	k1gInSc4	problém
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
k	k	k7c3	k
WC	WC	kA	WC
a	a	k8xC	a
ulevit	ulevit	k5eAaPmF	ulevit
si	se	k3xPyFc3	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
společensky	společensky	k6eAd1	společensky
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nosí	nosit	k5eAaImIp3nP	nosit
pleny	plena	k1gFnPc4	plena
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
příjemné	příjemný	k2eAgNnSc1d1	příjemné
(	(	kIx(	(
<g/>
Diaper	Diaper	k1gInSc1	Diaper
lovers	lovers	k1gInSc1	lovers
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
ageplay	ageplaa	k1gFnSc2	ageplaa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
se	se	k3xPyFc4	se
každopádně	každopádně	k6eAd1	každopádně
s	s	k7c7	s
používáním	používání	k1gNnSc7	používání
plen	plena	k1gFnPc2	plena
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
obezřetnost	obezřetnost	k1gFnSc1	obezřetnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
nejste	být	k5eNaImIp2nP	být
plně	plně	k6eAd1	plně
inkontinentní	inkontinentní	k2eAgFnPc1d1	inkontinentní
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
abyste	aby	kYmCp2nP	aby
nosili	nosit	k5eAaImAgMnP	nosit
trvale	trvale	k6eAd1	trvale
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
i	i	k9	i
několikaměsíční	několikaměsíční	k2eAgNnSc1d1	několikaměsíční
používání	používání	k1gNnSc1	používání
plen	plena	k1gFnPc2	plena
zdravým	zdravý	k1gMnSc7	zdravý
člověkem	člověk	k1gMnSc7	člověk
24	[number]	k4	24
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
v	v	k7c4	v
týdny	týden	k1gInPc4	týden
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
natolik	natolik	k6eAd1	natolik
značnému	značný	k2eAgNnSc3d1	značné
ochabnutí	ochabnutí	k1gNnSc3	ochabnutí
svěračů	svěrač	k1gInPc2	svěrač
<g/>
,	,	kIx,	,
že	že	k8xS	že
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
inkontinenci	inkontinence	k1gFnSc3	inkontinence
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
úplné	úplný	k2eAgNnSc4d1	úplné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
látkové	látkový	k2eAgFnSc2d1	látková
pleny	plena	k1gFnSc2	plena
absorbovat	absorbovat	k5eAaBmF	absorbovat
kapaliny	kapalina	k1gFnPc4	kapalina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
použitím	použití	k1gNnSc7	použití
speciálních	speciální	k2eAgFnPc2d1	speciální
vložek	vložka	k1gFnPc2	vložka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
užitečné	užitečný	k2eAgNnSc1d1	užitečné
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
své	svůj	k3xOyFgFnPc1	svůj
plenky	plenka	k1gFnPc1	plenka
silně	silně	k6eAd1	silně
promočí	promočit	k5eAaPmIp3nP	promočit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgFnPc4d1	možná
plenky	plenka	k1gFnPc4	plenka
často	často	k6eAd1	často
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Tlustá	tlustý	k2eAgFnSc1d1	tlustá
plenka	plenka	k1gFnSc1	plenka
mezi	mezi	k7c7	mezi
nohama	noha	k1gFnPc7	noha
může	moct	k5eAaImIp3nS	moct
snížit	snížit	k5eAaPmF	snížit
pohyblivost	pohyblivost	k1gFnSc4	pohyblivost
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
hygienu	hygiena	k1gFnSc4	hygiena
dítěte	dítě	k1gNnSc2	dítě
i	i	k8xC	i
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výměna	výměna	k1gFnSc1	výměna
plen	plena	k1gFnPc2	plena
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
absorpci	absorpce	k1gFnSc4	absorpce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tom	ten	k3xDgNnSc6	ten
kolik	kolik	k4yQc4	kolik
mohou	moct	k5eAaImIp3nP	moct
pojmout	pojmout	k5eAaPmF	pojmout
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
typu	typ	k1gInSc6	typ
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
různit	různit	k5eAaImF	různit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
i	i	k9	i
množství	množství	k1gNnSc1	množství
vymočené	vymočený	k2eAgFnSc2d1	vymočená
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
plena	plena	k1gFnSc1	plena
vydrží	vydržet	k5eAaPmIp3nS	vydržet
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
5	[number]	k4	5
–	–	k?	–
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
měnit	měnit	k5eAaImF	měnit
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
prevence	prevence	k1gFnSc1	prevence
podráždění	podráždění	k1gNnSc2	podráždění
kůže	kůže	k1gFnSc2	kůže
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
opruzení	opruzení	k1gNnSc1	opruzení
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
plenka	plenka	k1gFnSc1	plenka
vyměněna	vyměněn	k2eAgFnSc1d1	vyměněna
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
po	po	k7c6	po
ušpinění	ušpinění	k1gNnSc6	ušpinění
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
od	od	k7c2	od
výkalů	výkal	k1gInPc2	výkal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výměny	výměna	k1gFnSc2	výměna
po	po	k7c6	po
vyčištění	vyčištění	k1gNnSc6	vyčištění
a	a	k8xC	a
usušení	usušení	k1gNnSc6	usušení
zadnice	zadnice	k1gFnSc2	zadnice
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
dětský	dětský	k2eAgInSc4d1	dětský
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
ochranný	ochranný	k2eAgInSc4d1	ochranný
krém	krém	k1gInSc4	krém
nebo	nebo	k8xC	nebo
dětský	dětský	k2eAgInSc4d1	dětský
zásyp	zásyp	k1gInSc4	zásyp
na	na	k7c4	na
redukci	redukce	k1gFnSc4	redukce
podráždění	podráždění	k1gNnSc2	podráždění
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
metoda	metoda	k1gFnSc1	metoda
na	na	k7c4	na
prevenci	prevence	k1gFnSc4	prevence
a	a	k8xC	a
léčbu	léčba	k1gFnSc4	léčba
plenkové	plenkový	k2eAgFnSc2d1	plenková
vyrážky	vyrážka	k1gFnSc2	vyrážka
je	být	k5eAaImIp3nS	být
vystavit	vystavit	k5eAaPmF	vystavit
zadnici	zadnice	k1gFnSc4	zadnice
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
slunci	slunce	k1gNnSc6	slunce
co	co	k9	co
nejčastěji	často	k6eAd3	často
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
též	též	k9	též
speciální	speciální	k2eAgInPc4d1	speciální
krémy	krém	k1gInPc4	krém
obsahující	obsahující	k2eAgFnSc2d1	obsahující
složky	složka	k1gFnSc2	složka
jako	jako	k8xC	jako
oxid	oxid	k1gInSc1	oxid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
použít	použít	k5eAaPmF	použít
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
plenkových	plenkový	k2eAgFnPc2d1	plenková
vyrážek	vyrážka	k1gFnPc2	vyrážka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
odložením	odložení	k1gNnSc7	odložení
plenky	plenka	k1gFnSc2	plenka
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
do	do	k7c2	do
plenkové	plenkový	k2eAgFnSc2d1	plenková
nádoby	nádoba	k1gFnSc2	nádoba
na	na	k7c4	na
praní	praní	k1gNnSc4	praní
nebo	nebo	k8xC	nebo
odpad	odpad	k1gInSc4	odpad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
oddělit	oddělit	k5eAaPmF	oddělit
co	co	k9	co
nejvíc	nejvíc	k6eAd1	nejvíc
výkalové	výkalový	k2eAgFnSc2d1	výkalový
hmoty	hmota	k1gFnSc2	hmota
do	do	k7c2	do
toalety	toaleta	k1gFnSc2	toaleta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
kontaminaci	kontaminace	k1gFnSc4	kontaminace
odpadních	odpadní	k2eAgFnPc2d1	odpadní
skládek	skládka	k1gFnPc2	skládka
a	a	k8xC	a
spodních	spodní	k2eAgFnPc2d1	spodní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
plen	plena	k1gFnPc2	plena
úplně	úplně	k6eAd1	úplně
obejdou	obejít	k5eAaPmIp3nP	obejít
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
nebo	nebo	k8xC	nebo
nutnosti	nutnost	k1gFnSc2	nutnost
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
nebo	nebo	k8xC	nebo
jiní	jiný	k2eAgMnPc1d1	jiný
pečovatelé	pečovatel	k1gMnPc1	pečovatel
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
naučit	naučit	k5eAaPmF	naučit
vnímat	vnímat	k5eAaImF	vnímat
zvyky	zvyk	k1gInPc1	zvyk
a	a	k8xC	a
signály	signál	k1gInPc1	signál
vylučování	vylučování	k1gNnSc2	vylučování
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
evidentní	evidentní	k2eAgMnSc1d1	evidentní
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
vylučovat	vylučovat	k5eAaImF	vylučovat
<g/>
,	,	kIx,	,
přenesou	přenést	k5eAaPmIp3nP	přenést
dítě	dítě	k1gNnSc4	dítě
na	na	k7c4	na
potřebné	potřebný	k2eAgNnSc4d1	potřebné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
často	často	k6eAd1	často
nazývá	nazývat	k5eAaImIp3nS	nazývat
dětský	dětský	k2eAgInSc4d1	dětský
nočníkový	nočníkový	k2eAgInSc4d1	nočníkový
trénink	trénink	k1gInSc4	trénink
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
trénují	trénovat	k5eAaImIp3nP	trénovat
rozpoznávání	rozpoznávání	k1gNnSc4	rozpoznávání
signálů	signál	k1gInPc2	signál
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bezplínková	bezplínkový	k2eAgFnSc1d1	bezplínková
komunikační	komunikační	k2eAgFnSc1d1	komunikační
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Porovnání	porovnání	k1gNnSc1	porovnání
energetické	energetický	k2eAgFnSc2d1	energetická
náročnosti	náročnost	k1gFnSc2	náročnost
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
a	a	k8xC	a
látkových	látkový	k2eAgFnPc2d1	látková
plen	plena	k1gFnPc2	plena
===	===	k?	===
</s>
</p>
<p>
<s>
Britská	britský	k2eAgFnSc1d1	britská
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
uveřejnila	uveřejnit	k5eAaPmAgFnS	uveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
porovnávající	porovnávající	k2eAgFnSc4d1	porovnávající
uhlíkovou	uhlíkový	k2eAgFnSc4d1	uhlíková
stopu	stopa	k1gFnSc4	stopa
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
používání	používání	k1gNnSc2	používání
látkových	látkový	k2eAgFnPc2d1	látková
a	a	k8xC	a
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
plen	plena	k1gFnPc2	plena
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
praní	praní	k1gNnSc2	praní
látkových	látkový	k2eAgFnPc2d1	látková
plenek	plenka	k1gFnPc2	plenka
při	při	k7c6	při
60	[number]	k4	60
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
"	"	kIx"	"
<g/>
přírodním	přírodní	k2eAgNnSc6d1	přírodní
<g/>
"	"	kIx"	"
sušení	sušení	k1gNnSc6	sušení
na	na	k7c6	na
šňůře	šňůra	k1gFnSc6	šňůra
jsou	být	k5eAaImIp3nP	být
látkové	látkový	k2eAgFnPc1d1	látková
pleny	plena	k1gFnPc1	plena
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
<g/>
x	x	k?	x
energeticky	energeticky	k6eAd1	energeticky
úspornější	úsporný	k2eAgFnSc1d2	úspornější
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
praní	praní	k1gNnSc2	praní
na	na	k7c4	na
90	[number]	k4	90
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
sušení	sušení	k1gNnSc4	sušení
v	v	k7c6	v
elektrické	elektrický	k2eAgFnSc6d1	elektrická
sušárně	sušárna	k1gFnSc6	sušárna
je	být	k5eAaImIp3nS	být
výsledek	výsledek	k1gInSc1	výsledek
opačný	opačný	k2eAgInSc1d1	opačný
–	–	k?	–
látkové	látkový	k2eAgFnSc2d1	látková
pleny	plena	k1gFnSc2	plena
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
<g/>
x	x	k?	x
energeticky	energeticky	k6eAd1	energeticky
náročnější	náročný	k2eAgNnSc1d2	náročnější
než	než	k8xS	než
jednorázové	jednorázový	k2eAgNnSc1d1	jednorázové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Alternativy	alternativa	k1gFnSc2	alternativa
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
současné	současný	k2eAgInPc4d1	současný
trendy	trend	k1gInPc4	trend
patří	patřit	k5eAaImIp3nP	patřit
hybridní	hybridní	k2eAgMnSc1d1	hybridní
mnohorázový	mnohorázový	k2eAgMnSc1d1	mnohorázový
<g/>
/	/	kIx~	/
<g/>
jednorázový	jednorázový	k2eAgInSc1d1	jednorázový
systém	systém	k1gInSc1	systém
s	s	k7c7	s
vnější	vnější	k2eAgFnSc7d1	vnější
plastickou	plastický	k2eAgFnSc7d1	plastická
částí	část	k1gFnSc7	část
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
víckrát	víckrát	k6eAd1	víckrát
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
absorbující	absorbující	k2eAgFnSc7d1	absorbující
částí	část	k1gFnSc7	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyhodí	vyhodit	k5eAaPmIp3nS	vyhodit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kompletně	kompletně	k6eAd1	kompletně
biologicky	biologicky	k6eAd1	biologicky
rozložitelná	rozložitelný	k2eAgFnSc1d1	rozložitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgFnSc2d1	speciální
plenky	plenka	k1gFnSc2	plenka
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
pomočení	pomočení	k1gNnSc1	pomočení
prvního	první	k4xOgMnSc2	první
amerického	americký	k2eAgMnSc2d1	americký
astronauta	astronaut	k1gMnSc2	astronaut
po	po	k7c6	po
mnohahodinových	mnohahodinový	k2eAgInPc6d1	mnohahodinový
odkladech	odklad	k1gInPc6	odklad
startu	start	k1gInSc2	start
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plenky	plenka	k1gFnPc1	plenka
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaPmNgFnP	využívat
i	i	k9	i
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
cestující	cestující	k1gMnPc1	cestující
v	v	k7c6	v
přecpaných	přecpaný	k2eAgInPc6d1	přecpaný
vlacích	vlak	k1gInPc6	vlak
takto	takto	k6eAd1	takto
řeší	řešit	k5eAaImIp3nS	řešit
nedostupnost	nedostupnost	k1gFnSc4	nedostupnost
toalet	toaleta	k1gFnPc2	toaleta
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInSc1d1	související
článek	článek	k1gInSc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
ABDL	ABDL	kA	ABDL
</s>
</p>
<p>
<s>
Močová	močový	k2eAgFnSc1d1	močová
inkontinence	inkontinence	k1gFnSc1	inkontinence
</s>
</p>
<p>
<s>
Defekace	defekace	k1gFnSc1	defekace
</s>
</p>
<p>
<s>
Urinace	urinace	k1gFnSc1	urinace
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
