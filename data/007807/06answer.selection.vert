<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
dostal	dostat	k5eAaPmAgMnS	dostat
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
nejdříve	dříve	k6eAd3	dříve
jako	jako	k8xC	jako
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
od	od	k7c2	od
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
operety	opereta	k1gFnSc2	opereta
(	(	kIx(	(
<g/>
od	od	k7c2	od
sezony	sezona	k1gFnSc2	sezona
1925	[number]	k4	1925
<g/>
/	/	kIx~	/
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
tamního	tamní	k2eAgNnSc2d1	tamní
Národního	národní	k2eAgNnSc2d1	národní
(	(	kIx(	(
<g/>
Zemského	zemský	k2eAgNnSc2d1	zemské
<g/>
)	)	kIx)	)
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
