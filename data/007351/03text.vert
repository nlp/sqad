<s>
Nářečí	nářečí	k1gNnSc1	nářečí
neboli	neboli	k8xC	neboli
dialekt	dialekt	k1gInSc1	dialekt
(	(	kIx(	(
<g/>
adj.	adj.	k?	adj.
nářeční	nářeční	k2eAgFnSc1d1	nářeční
<g/>
,	,	kIx,	,
zř.	zř.	k?	zř.
nářečový	nářečový	k2eAgInSc1d1	nářečový
a	a	k8xC	a
dialektní	dialektní	k2eAgInSc1d1	dialektní
<g/>
,	,	kIx,	,
dialektový	dialektový	k2eAgInSc1d1	dialektový
<g/>
,	,	kIx,	,
zast.	zast.	k?	zast.
dialektický	dialektický	k2eAgInSc1d1	dialektický
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nespisovný	spisovný	k2eNgInSc1d1	nespisovný
jazykový	jazykový	k2eAgInSc1d1	jazykový
útvar	útvar	k1gInSc1	útvar
užívaný	užívaný	k2eAgInSc1d1	užívaný
pouze	pouze	k6eAd1	pouze
mluvčími	mluvčí	k1gMnPc7	mluvčí
z	z	k7c2	z
určité	určitý	k2eAgFnSc2d1	určitá
geografické	geografický	k2eAgFnSc2d1	geografická
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
studiem	studio	k1gNnSc7	studio
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
dialektologie	dialektologie	k1gFnSc1	dialektologie
<g/>
.	.	kIx.	.
</s>
<s>
Nářečí	nářečí	k1gNnSc1	nářečí
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vznikala	vznikat	k5eAaImAgNnP	vznikat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
izolace	izolace	k1gFnSc2	izolace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
nich	on	k3xPp3gMnPc2	on
žijících	žijící	k2eAgMnPc2d1	žijící
<g/>
.	.	kIx.	.
</s>
<s>
Nářečí	nářečí	k1gNnSc1	nářečí
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
především	především	k9	především
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
<g/>
,	,	kIx,	,
neformální	formální	k2eNgFnSc4d1	neformální
ústní	ústní	k2eAgFnSc4d1	ústní
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Řidčeji	řídce	k6eAd2	řídce
můžeme	moct	k5eAaImIp1nP	moct
nářečí	nářečí	k1gNnSc4	nářečí
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
umělecké	umělecký	k2eAgFnSc3d1	umělecká
literatuře	literatura	k1gFnSc3	literatura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přesvědčivější	přesvědčivý	k2eAgFnPc4d2	přesvědčivější
charakteristiky	charakteristika	k1gFnPc4	charakteristika
postav	postava	k1gFnPc2	postava
i	i	k8xC	i
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
hojné	hojný	k2eAgInPc1d1	hojný
jsou	být	k5eAaImIp3nP	být
nářeční	nářeční	k2eAgInPc1d1	nářeční
prvky	prvek	k1gInPc1	prvek
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
,	,	kIx,	,
Tyla	Tyl	k1gMnSc2	Tyl
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Jiráska	Jirásek	k1gMnSc4	Jirásek
<g/>
,	,	kIx,	,
Staška	Stašek	k1gMnSc4	Stašek
<g/>
,	,	kIx,	,
Herbena	Herben	k2eAgMnSc4d1	Herben
<g/>
,	,	kIx,	,
Novákové	Novákové	k2eAgMnSc4d1	Novákové
<g/>
,	,	kIx,	,
Aloise	Alois	k1gMnSc4	Alois
a	a	k8xC	a
Viléma	Vilém	k1gMnSc4	Vilém
Mrštíkových	Mrštíkův	k2eAgFnPc2d1	Mrštíkova
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
užití	užití	k1gNnSc1	užití
nářečí	nářečí	k1gNnSc2	nářečí
je	být	k5eAaImIp3nS	být
však	však	k9	však
přizpůsobeno	přizpůsoben	k2eAgNnSc1d1	přizpůsobeno
potřebám	potřeba	k1gFnPc3	potřeba
umělecké	umělecký	k2eAgFnSc2d1	umělecká
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
třeba	třeba	k6eAd1	třeba
jej	on	k3xPp3gNnSc4	on
od	od	k7c2	od
nářečí	nářečí	k1gNnSc2	nářečí
skutečného	skutečný	k2eAgInSc2d1	skutečný
odlišovat	odlišovat	k5eAaImF	odlišovat
(	(	kIx(	(
<g/>
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
nářečí	nářečí	k1gNnSc6	nářečí
stylizovaném	stylizovaný	k2eAgNnSc6d1	stylizované
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nářeční	nářeční	k2eAgInSc4d1	nářeční
prvek	prvek	k1gInSc4	prvek
použitý	použitý	k2eAgInSc4d1	použitý
ve	v	k7c6	v
spisovném	spisovný	k2eAgInSc6d1	spisovný
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
dialektismus	dialektismus	k1gInSc1	dialektismus
(	(	kIx(	(
<g/>
dialektizmus	dialektizmus	k1gInSc1	dialektizmus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
nejodlišnější	odlišný	k2eAgNnSc1d3	nejodlišnější
nářečí	nářečí	k1gNnSc1	nářečí
na	na	k7c6	na
Klatovsku	Klatovsko	k1gNnSc6	Klatovsko
a	a	k8xC	a
Sušicku	Sušicko	k1gNnSc6	Sušicko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
používá	používat	k5eAaImIp3nS	používat
mnoho	mnoho	k4c1	mnoho
počeštělých	počeštělý	k2eAgNnPc2d1	počeštělé
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
svá	svůj	k3xOyFgNnPc4	svůj
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
