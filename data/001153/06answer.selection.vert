<s>
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
byl	být	k5eAaImAgMnS	být
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1955	[number]	k4	1955
a	a	k8xC	a
1975	[number]	k4	1975
probíhal	probíhat	k5eAaImAgInS	probíhat
na	na	k7c6	na
území	území	k1gNnSc6	území
Vietnamu	Vietnam	k1gInSc2	Vietnam
a	a	k8xC	a
v	v	k7c6	v
příhraničí	příhraničí	k1gNnSc6	příhraničí
sousedících	sousedící	k2eAgFnPc2d1	sousedící
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Kambodža	Kambodža	k1gFnSc1	Kambodža
a	a	k8xC	a
Laos	Laos	k1gInSc1	Laos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
