<s>
Filip	Filip	k1gMnSc1	Filip
I.	I.	kA	I.
vévoda	vévoda	k1gMnSc1	vévoda
orléanský	orléanský	k2eAgMnSc1d1	orléanský
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k8xS	jako
Monsieur	Monsieur	k1gMnSc1	Monsieur
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1640	[number]	k4	1640
Saint-Germain-en-Laye	Saint-Germainn-Lay	k1gInSc2	Saint-Germain-en-Lay
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1701	[number]	k4	1701
zámek	zámek	k1gInSc1	zámek
Saint-Cloud	Saint-Cloud	k1gInSc4	Saint-Cloud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
