<s>
Filip	Filip	k1gMnSc1	Filip
I.	I.	kA	I.
vévoda	vévoda	k1gMnSc1	vévoda
orléanský	orléanský	k2eAgMnSc1d1	orléanský
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k8xC	jako
Monsieur	Monsieur	k1gMnSc1	Monsieur
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1640	[number]	k4	1640
Saint-Germain-en-Laye	Saint-Germainn-Lay	k1gInSc2	Saint-Germain-en-Lay
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1701	[number]	k4	1701
zámek	zámek	k1gInSc1	zámek
Saint-Cloud	Saint-Cloud	k1gInSc4	Saint-Cloud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
zakladatelem	zakladatel	k1gMnSc7	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
Bourbon-Orléans	Bourbon-Orléansa	k1gFnPc2	Bourbon-Orléansa
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Bourbonů	bourbon	k1gInPc2	bourbon
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
manželky	manželka	k1gFnPc4	manželka
Anny	Anna	k1gFnSc2	Anna
Rakouské	rakouský	k2eAgFnSc6d1	rakouská
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
španělských	španělský	k2eAgInPc2d1	španělský
Habsburků	Habsburk	k1gInPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
byl	být	k5eAaImAgMnS	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
mladší	mladý	k2eAgFnSc2d2	mladší
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
měl	mít	k5eAaImAgMnS	mít
jako	jako	k8xS	jako
dítě	dítě	k1gNnSc4	dítě
titul	titul	k1gInSc1	titul
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Anjou	Anjý	k2eAgFnSc7d1	Anjý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
a	a	k8xC	a
korunovaci	korunovace	k1gFnSc3	korunovace
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zván	zván	k2eAgMnSc1d1	zván
Grand	grand	k1gMnSc1	grand
Dauphin	dauphin	k1gMnSc1	dauphin
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
držel	držet	k5eAaImAgMnS	držet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1643	[number]	k4	1643
až	až	k9	až
1661	[number]	k4	1661
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
znám	znát	k5eAaImIp1nS	znát
též	též	k9	též
jako	jako	k9	jako
Malý	Malý	k1gMnSc1	Malý
Monsieur	Monsieur	k1gMnSc1	Monsieur
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odlišil	odlišit	k5eAaPmAgInS	odlišit
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
Gastona	Gaston	k1gMnSc2	Gaston
<g/>
,	,	kIx,	,
zvaného	zvaný	k2eAgInSc2d1	zvaný
Velký	velký	k2eAgMnSc1d1	velký
Monsieur	Monsieur	k1gMnSc1	Monsieur
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Gastonově	Gastonův	k2eAgFnSc6d1	Gastonova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1660	[number]	k4	1660
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgInS	získat
jeho	jeho	k3xOp3gNnSc4	jeho
údělné	údělný	k2eAgNnSc1d1	údělné
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
vévodství	vévodství	k1gNnSc1	vévodství
orléanské	orléanský	k2eAgNnSc1d1	orléanský
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
<g/>
,	,	kIx,	,
originální	originální	k2eAgFnSc4d1	originální
a	a	k8xC	a
svébytnou	svébytný	k2eAgFnSc4d1	svébytná
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jako	jako	k9	jako
malé	malý	k2eAgNnSc4d1	malé
dítě	dítě	k1gNnSc4	dítě
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
potrpěl	potrpět	k5eAaImAgMnS	potrpět
na	na	k7c4	na
vkusné	vkusný	k2eAgFnPc4d1	vkusná
a	a	k8xC	a
elegantní	elegantní	k2eAgNnSc1d1	elegantní
oblékání	oblékání	k1gNnSc1	oblékání
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
se	se	k3xPyFc4	se
krášlil	krášlit	k5eAaImAgMnS	krášlit
<g/>
,	,	kIx,	,
líčil	líčit	k5eAaImAgMnS	líčit
a	a	k8xC	a
pudroval	pudrovat	k5eAaImAgMnS	pudrovat
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
až	až	k6eAd1	až
nápadně	nápadně	k6eAd1	nápadně
jemný	jemný	k2eAgInSc1d1	jemný
a	a	k8xC	a
zženštilý	zženštilý	k2eAgMnSc1d1	zženštilý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Povahově	povahově	k6eAd1	povahově
i	i	k9	i
svou	svůj	k3xOyFgFnSc7	svůj
fyziognomií	fyziognomie	k1gFnSc7	fyziognomie
byl	být	k5eAaImAgInS	být
pravým	pravý	k2eAgInSc7d1	pravý
opakem	opak	k1gInSc7	opak
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starší	k1gMnSc2	starší
bratra	bratr	k1gMnSc4	bratr
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něčem	něco	k3yInSc6	něco
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
tito	tento	k3xDgMnPc1	tento
bratři	bratr	k1gMnPc1	bratr
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
blízcí	blízký	k2eAgMnPc1d1	blízký
–	–	k?	–
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
velmi	velmi	k6eAd1	velmi
bystří	bystrý	k2eAgMnPc1d1	bystrý
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
milovali	milovat	k5eAaImAgMnP	milovat
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1658	[number]	k4	1658
Ludvík	Ludvík	k1gMnSc1	Ludvík
těžce	těžce	k6eAd1	těžce
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
Filip	Filip	k1gMnSc1	Filip
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
kardinála	kardinál	k1gMnSc2	kardinál
Mazarina	Mazarin	k1gMnSc2	Mazarin
intenzivně	intenzivně	k6eAd1	intenzivně
vzdělávat	vzdělávat	k5eAaImF	vzdělávat
a	a	k8xC	a
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
převzetí	převzetí	k1gNnSc4	převzetí
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
osobou	osoba	k1gFnSc7	osoba
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
jeho	jeho	k3xOp3gFnSc4	jeho
přízeň	přízeň	k1gFnSc4	přízeň
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
stínu	stín	k1gInSc6	stín
a	a	k8xC	a
v	v	k7c6	v
méně	málo	k6eAd2	málo
zajímavém	zajímavý	k2eAgInSc6d1	zajímavý
postavení	postavení	k1gNnSc2	postavení
druhorozeného	druhorozený	k2eAgNnSc2d1	druhorozené
<g/>
.	.	kIx.	.
</s>
<s>
Mnohým	mnohý	k2eAgMnPc3d1	mnohý
lidem	člověk	k1gMnPc3	člověk
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
se	se	k3xPyFc4	se
ale	ale	k9	ale
ulevilo	ulevit	k5eAaPmAgNnS	ulevit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jen	jen	k9	jen
málokdo	málokdo	k3yInSc1	málokdo
si	se	k3xPyFc3	se
dovedl	dovést	k5eAaPmAgInS	dovést
Filipa	Filip	k1gMnSc4	Filip
představit	představit	k5eAaPmF	představit
jako	jako	k9	jako
krále	král	k1gMnSc4	král
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
znám	znát	k5eAaImIp1nS	znát
svou	svůj	k3xOyFgFnSc7	svůj
nevázaností	nevázanost	k1gFnSc7	nevázanost
<g/>
,	,	kIx,	,
prostopášností	prostopášnost	k1gFnSc7	prostopášnost
a	a	k8xC	a
také	také	k9	také
dával	dávat	k5eAaImAgInS	dávat
zcela	zcela	k6eAd1	zcela
nepokrytě	pokrytě	k6eNd1	pokrytě
najevo	najevo	k6eAd1	najevo
svou	svůj	k3xOyFgFnSc4	svůj
homosexualitu	homosexualita	k1gFnSc4	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
také	také	k9	také
vedl	vést	k5eAaImAgMnS	vést
velmi	velmi	k6eAd1	velmi
nákladný	nákladný	k2eAgInSc4d1	nákladný
život	život	k1gInSc4	život
–	–	k?	–
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
dvořané	dvořan	k1gMnPc1	dvořan
se	se	k3xPyFc4	se
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
zaujetím	zaujetí	k1gNnSc7	zaujetí
věnoval	věnovat	k5eAaPmAgMnS	věnovat
hazardním	hazardní	k2eAgFnPc3d1	hazardní
karetním	karetní	k2eAgFnPc3d1	karetní
hrám	hra	k1gFnPc3	hra
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
Orléanský	Orléanský	k2eAgMnSc1d1	Orléanský
byl	být	k5eAaImAgInS	být
velkým	velký	k2eAgMnSc7d1	velký
milovníkem	milovník	k1gMnSc7	milovník
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
mecenášem	mecenáš	k1gMnSc7	mecenáš
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
malířů	malíř	k1gMnPc2	malíř
<g/>
,	,	kIx,	,
sochařů	sochař	k1gMnPc2	sochař
<g/>
,	,	kIx,	,
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Filip	Filip	k1gMnSc1	Filip
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
mecenášem	mecenáš	k1gMnSc7	mecenáš
Moliè	Moliè	k1gFnSc2	Moliè
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
divadelní	divadelní	k2eAgFnSc2d1	divadelní
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
přímluvě	přímluva	k1gFnSc3	přímluva
se	se	k3xPyFc4	se
Moliè	Moliè	k1gMnSc1	Moliè
mohl	moct	k5eAaImAgMnS	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pak	pak	k6eAd1	pak
náležitě	náležitě	k6eAd1	náležitě
odměnil	odměnit	k5eAaPmAgInS	odměnit
jeho	jeho	k3xOp3gInSc4	jeho
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
talent	talent	k1gInSc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
rovněž	rovněž	k9	rovněž
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
sbírky	sbírka	k1gFnPc4	sbírka
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
drahokamů	drahokam	k1gInPc2	drahokam
a	a	k8xC	a
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
proslavil	proslavit	k5eAaPmAgInS	proslavit
řadou	řada	k1gFnSc7	řada
homosexuálních	homosexuální	k2eAgMnPc2d1	homosexuální
skandálů	skandál	k1gInPc2	skandál
<g/>
,	,	kIx,	,
projevoval	projevovat	k5eAaImAgInS	projevovat
také	také	k9	také
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
zbožnost	zbožnost	k1gFnSc4	zbožnost
a	a	k8xC	a
nechyběl	chybět	k5eNaImAgMnS	chybět
na	na	k7c6	na
žádném	žádný	k3yNgInSc6	žádný
kázání	kázání	k1gNnSc6	kázání
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
sbíral	sbírat	k5eAaImAgMnS	sbírat
růžence	růženec	k1gInPc4	růženec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
zámku	zámek	k1gInSc6	zámek
Saint-Cloud	Saint-Cloud	k1gMnSc1	Saint-Cloud
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
nádheře	nádhera	k1gFnSc6	nádhera
a	a	k8xC	a
přepychu	přepych	k1gInSc6	přepych
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
tohoto	tento	k3xDgInSc2	tento
jeho	jeho	k3xOp3gInSc2	jeho
okázalého	okázalý	k2eAgInSc2d1	okázalý
soukromého	soukromý	k2eAgInSc2d1	soukromý
dvora	dvůr	k1gInSc2	dvůr
spolykal	spolykat	k5eAaPmAgMnS	spolykat
nemalé	malý	k2eNgInPc4d1	nemalý
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
tak	tak	k9	tak
neustále	neustále	k6eAd1	neustále
žádal	žádat	k5eAaImAgMnS	žádat
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
apanáže	apanáž	k1gFnSc2	apanáž
a	a	k8xC	a
přidělení	přidělení	k1gNnSc4	přidělení
dalších	další	k2eAgNnPc2d1	další
lén	léno	k1gNnPc2	léno
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
by	by	kYmCp3nS	by
získal	získat	k5eAaPmAgInS	získat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
nákladný	nákladný	k2eAgInSc4d1	nákladný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
však	však	k9	však
už	už	k6eAd1	už
Ludvík	Ludvík	k1gMnSc1	Ludvík
další	další	k2eAgFnSc4d1	další
jeho	jeho	k3xOp3gFnSc3	jeho
žádosti	žádost	k1gFnSc3	žádost
rezolutně	rezolutně	k6eAd1	rezolutně
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
jemnou	jemný	k2eAgFnSc4d1	jemná
povahu	povaha	k1gFnSc4	povaha
a	a	k8xC	a
slabší	slabý	k2eAgFnSc4d2	slabší
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
konstituci	konstituce	k1gFnSc4	konstituce
se	se	k3xPyFc4	se
Filip	Filip	k1gMnSc1	Filip
projevil	projevit	k5eAaPmAgMnS	projevit
jako	jako	k9	jako
velký	velký	k2eAgInSc4d1	velký
vojenský	vojenský	k2eAgInSc4d1	vojenský
talent	talent	k1gInSc4	talent
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
současníky	současník	k1gMnPc4	současník
udivil	udivit	k5eAaPmAgMnS	udivit
svou	svůj	k3xOyFgFnSc7	svůj
odvahou	odvaha	k1gFnSc7	odvaha
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1672	[number]	k4	1672
<g/>
–	–	k?	–
<g/>
1677	[number]	k4	1677
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Holandskem	Holandsko	k1gNnSc7	Holandsko
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
několika	několik	k4yIc2	několik
vítězství	vítězství	k1gNnPc2	vítězství
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
u	u	k7c2	u
Peene	Peen	k1gMnSc5	Peen
a	a	k8xC	a
Nordpeene	Nordpeen	k1gMnSc5	Nordpeen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1677	[number]	k4	1677
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
porazil	porazit	k5eAaPmAgMnS	porazit
Viléma	Vilém	k1gMnSc4	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
prince	princ	k1gMnSc2	princ
Oranžského	oranžský	k2eAgMnSc2d1	oranžský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
byl	být	k5eAaImAgMnS	být
Filip	Filip	k1gMnSc1	Filip
lidem	lid	k1gInSc7	lid
bouřlivě	bouřlivě	k6eAd1	bouřlivě
přivítán	přivítán	k2eAgInSc4d1	přivítán
a	a	k8xC	a
oslavován	oslavován	k2eAgInSc4d1	oslavován
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
ale	ale	k8xC	ale
natolik	natolik	k6eAd1	natolik
žárlil	žárlit	k5eAaImAgMnS	žárlit
na	na	k7c4	na
úspěchy	úspěch	k1gInPc4	úspěch
svého	svůj	k1gMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
velení	velení	k1gNnSc2	velení
armády	armáda	k1gFnSc2	armáda
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čin	čin	k1gInSc1	čin
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
pochopitelný	pochopitelný	k2eAgMnSc1d1	pochopitelný
–	–	k?	–
Ludvík	Ludvík	k1gMnSc1	Ludvík
měl	mít	k5eAaImAgMnS	mít
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
živé	živý	k2eAgFnSc6d1	živá
paměti	paměť	k1gFnSc6	paměť
povstání	povstání	k1gNnSc2	povstání
Frondy	fronda	k1gFnSc2	fronda
a	a	k8xC	a
zradu	zrada	k1gFnSc4	zrada
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
Gastona	Gaston	k1gMnSc2	Gaston
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svými	svůj	k3xOyFgFnPc7	svůj
intrikami	intrika	k1gFnPc7	intrika
ztrpčoval	ztrpčovat	k5eAaImAgInS	ztrpčovat
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
život	život	k1gInSc4	život
Ludvíku	Ludvík	k1gMnSc3	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k8xC	i
jemu	on	k3xPp3gNnSc3	on
samotnému	samotný	k2eAgNnSc3d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
nedopustit	dopustit	k5eNaPmF	dopustit
něco	něco	k3yInSc4	něco
podobného	podobný	k2eAgNnSc2d1	podobné
a	a	k8xC	a
držel	držet	k5eAaImAgInS	držet
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Filipa	Filip	k1gMnSc4	Filip
záměrně	záměrně	k6eAd1	záměrně
co	co	k9	co
nejdále	daleko	k6eAd3	daleko
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
bratrovi	bratr	k1gMnSc3	bratr
se	se	k3xPyFc4	se
podvolil	podvolit	k5eAaPmAgMnS	podvolit
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
věnovat	věnovat	k5eAaImF	věnovat
světu	svět	k1gInSc3	svět
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
především	především	k6eAd1	především
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
odborníkem	odborník	k1gMnSc7	odborník
na	na	k7c4	na
dvorskou	dvorský	k2eAgFnSc4d1	dvorská
etiketu	etiketa	k1gFnSc4	etiketa
a	a	k8xC	a
francouzský	francouzský	k2eAgInSc4d1	francouzský
dvorský	dvorský	k2eAgInSc4d1	dvorský
ceremoniál	ceremoniál	k1gInSc4	ceremoniál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Ludvíka	Ludvík	k1gMnSc2	Ludvík
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
Filip	Filip	k1gMnSc1	Filip
dvakrát	dvakrát	k6eAd1	dvakrát
oženit	oženit	k5eAaPmF	oženit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
za	za	k7c2	za
trvání	trvání	k1gNnSc2	trvání
těchto	tento	k3xDgNnPc2	tento
manželství	manželství	k1gNnPc2	manželství
měl	mít	k5eAaImAgMnS	mít
však	však	k9	však
řadu	řada	k1gFnSc4	řada
milenců	milenec	k1gMnPc2	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
velkou	velký	k2eAgFnSc7d1	velká
a	a	k8xC	a
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
láskou	láska	k1gFnSc7	láska
byl	být	k5eAaImAgMnS	být
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
de	de	k?	de
Lorraine-Armagnac	Lorraine-Armagnac	k1gInSc1	Lorraine-Armagnac
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
největším	veliký	k2eAgMnSc7d3	veliký
nepřítelem	nepřítel	k1gMnSc7	nepřítel
Filipových	Filipových	k2eAgFnPc2d1	Filipových
manželek	manželka	k1gFnPc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Nenávist	nenávist	k1gFnSc1	nenávist
byla	být	k5eAaImAgFnS	být
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
rytíř	rytíř	k1gMnSc1	rytíř
de	de	k?	de
Lorraine	Lorrain	k1gInSc5	Lorrain
byl	být	k5eAaImAgInS	být
fyzicky	fyzicky	k6eAd1	fyzicky
krásný	krásný	k2eAgInSc1d1	krásný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krutý	krutý	k2eAgInSc1d1	krutý
a	a	k8xC	a
bezzásadový	bezzásadový	k2eAgMnSc1d1	bezzásadový
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
dvorskými	dvorský	k2eAgFnPc7d1	dvorská
intrikami	intrika	k1gFnPc7	intrika
a	a	k8xC	a
Filipa	Filip	k1gMnSc4	Filip
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
jen	jen	k9	jen
využíval	využívat	k5eAaImAgInS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
Filipa	Filip	k1gMnSc2	Filip
Orléanského	Orléanský	k2eAgMnSc2d1	Orléanský
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1661	[number]	k4	1661
jeho	jeho	k3xOp3gFnSc2	jeho
sestřenice	sestřenice	k1gFnSc2	sestřenice
Henrietta	Henrietta	k1gFnSc1	Henrietta
Anna	Anna	k1gFnSc1	Anna
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Henrietty	Henrietta	k1gFnSc2	Henrietta
Marie	Maria	k1gFnSc2	Maria
Bourbonské	bourbonský	k2eAgFnSc2d1	Bourbonská
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
nešťastné	šťastný	k2eNgNnSc1d1	nešťastné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
narodily	narodit	k5eAaPmAgFnP	narodit
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
trpěla	trpět	k5eAaImAgFnS	trpět
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
vztahu	vztah	k1gInSc3	vztah
svého	svůj	k3xOyFgMnSc4	svůj
manžela	manžel	k1gMnSc4	manžel
k	k	k7c3	k
rytíři	rytíř	k1gMnSc3	rytíř
de	de	k?	de
Lorraine	Lorrain	k1gMnSc5	Lorrain
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rytíře	rytíř	k1gMnSc4	rytíř
od	od	k7c2	od
dvora	dvůr	k1gInSc2	dvůr
vypověděl	vypovědět	k5eAaPmAgInS	vypovědět
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hladkému	hladký	k2eAgInSc3d1	hladký
průběhu	průběh	k1gInSc3	průběh
celé	celý	k2eAgFnSc2d1	celá
akce	akce	k1gFnSc2	akce
přispělo	přispět	k5eAaPmAgNnS	přispět
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc1	několik
dvorských	dvorský	k2eAgMnPc2d1	dvorský
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
rytíře	rytíř	k1gMnSc2	rytíř
de	de	k?	de
Lorraine	Lorrain	k1gMnSc5	Lorrain
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
v	v	k7c6	v
podezření	podezření	k1gNnSc6	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
svést	svést	k5eAaPmF	svést
králova	králův	k2eAgMnSc4d1	králův
syna	syn	k1gMnSc4	syn
Dauphina	dauphin	k1gMnSc4	dauphin
Ludvíka	Ludvík	k1gMnSc4	Ludvík
k	k	k7c3	k
homosexuálním	homosexuální	k2eAgFnPc3d1	homosexuální
milostným	milostný	k2eAgFnPc3d1	milostná
hrátkám	hrátky	k1gFnPc3	hrátky
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
velmi	velmi	k6eAd1	velmi
rozzuřilo	rozzuřit	k5eAaPmAgNnS	rozzuřit
<g/>
.	.	kIx.	.
</s>
<s>
Rytíř	Rytíř	k1gMnSc1	Rytíř
de	de	k?	de
Lorraine	Lorrain	k1gInSc5	Lorrain
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1670	[number]	k4	1670
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
ostrovní	ostrovní	k2eAgFnSc6d1	ostrovní
pevnosti	pevnost	k1gFnSc6	pevnost
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
If	If	k1gMnSc1	If
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Henrietta	Henrietta	k1gFnSc1	Henrietta
Anna	Anna	k1gFnSc1	Anna
tak	tak	k6eAd1	tak
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
nad	nad	k7c7	nad
svým	svůj	k3xOyFgMnSc7	svůj
sokem	sok	k1gMnSc7	sok
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
nadlouho	nadlouho	k6eAd1	nadlouho
–	–	k?	–
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zemřela	zemřít	k5eAaPmAgFnS	zemřít
za	za	k7c2	za
záhadných	záhadný	k2eAgFnPc2d1	záhadná
okolností	okolnost	k1gFnPc2	okolnost
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c6	o
pomstě	pomsta	k1gFnSc6	pomsta
rytíře	rytíř	k1gMnSc2	rytíř
de	de	k?	de
Lorraine	Lorrain	k1gMnSc5	Lorrain
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
údajně	údajně	k6eAd1	údajně
vévodkyni	vévodkyně	k1gFnSc4	vévodkyně
nechal	nechat	k5eAaPmAgMnS	nechat
otrávit	otrávit	k5eAaPmF	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepotvrdila	potvrdit	k5eNaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
pitva	pitva	k1gFnSc1	pitva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgNnSc3	tento
podezření	podezření	k1gNnSc3	podezření
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
<g/>
,	,	kIx,	,
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
zánět	zánět	k1gInSc1	zánět
pobřišnice	pobřišnice	k1gFnSc2	pobřišnice
způsobený	způsobený	k2eAgInSc1d1	způsobený
spontánní	spontánní	k2eAgFnSc7d1	spontánní
perforací	perforace	k1gFnSc7	perforace
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
Filipovou	Filipův	k2eAgFnSc7d1	Filipova
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Alžběta	Alžběta	k1gFnSc1	Alžběta
Šarlota	Šarlota	k1gFnSc1	Šarlota
Falcká	falcký	k2eAgFnSc1d1	Falcká
(	(	kIx(	(
<g/>
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Liselotte	Liselott	k1gMnSc5	Liselott
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
falckého	falcký	k2eAgMnSc2d1	falcký
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
Ludvíka	Ludvík	k1gMnSc2	Ludvík
(	(	kIx(	(
<g/>
syna	syn	k1gMnSc2	syn
českého	český	k2eAgMnSc2d1	český
"	"	kIx"	"
<g/>
zimního	zimní	k2eAgMnSc2d1	zimní
<g/>
"	"	kIx"	"
krále	král	k1gMnSc2	král
Fridricha	Fridrich	k1gMnSc2	Fridrich
Falckého	falcký	k2eAgMnSc2d1	falcký
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
ženy	žena	k1gFnPc4	žena
Šarloty	Šarlota	k1gFnSc2	Šarlota
Hesensko-Kasselské	hesenskoasselský	k2eAgFnSc2d1	hesensko-kasselský
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
Filipovo	Filipův	k2eAgNnSc1d1	Filipovo
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
šťastnější	šťastný	k2eAgNnSc1d2	šťastnější
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
manželi	manžel	k1gMnPc7	manžel
sice	sice	k8xC	sice
nepanovala	panovat	k5eNaImAgFnS	panovat
skutečná	skutečný	k2eAgFnSc1d1	skutečná
láska	láska	k1gFnSc1	láska
a	a	k8xC	a
vášeň	vášeň	k1gFnSc1	vášeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
projevovali	projevovat	k5eAaImAgMnP	projevovat
si	se	k3xPyFc3	se
alespoň	alespoň	k9	alespoň
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
úctu	úcta	k1gFnSc4	úcta
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
daných	daný	k2eAgFnPc2d1	daná
možností	možnost	k1gFnPc2	možnost
harmonické	harmonický	k2eAgFnPc1d1	harmonická
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
byli	být	k5eAaImAgMnP	být
především	především	k9	především
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
opět	opět	k6eAd1	opět
vzešly	vzejít	k5eAaPmAgFnP	vzejít
tři	tři	k4xCgFnPc1	tři
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
..	..	k?	..
Filip	Filip	k1gMnSc1	Filip
Orléanský	Orléanský	k2eAgMnSc1d1	Orléanský
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
politicky	politicky	k6eAd1	politicky
věrný	věrný	k2eAgMnSc1d1	věrný
svému	svůj	k3xOyFgNnSc3	svůj
bratrovi	bratr	k1gMnSc3	bratr
Ludvíku	Ludvík	k1gMnSc6	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
loajalita	loajalita	k1gFnSc1	loajalita
k	k	k7c3	k
trůnu	trůn	k1gInSc3	trůn
byla	být	k5eAaImAgFnS	být
příkladná	příkladný	k2eAgFnSc1d1	příkladná
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
proti	proti	k7c3	proti
bratrovi	bratr	k1gMnSc3	bratr
neintrikoval	intrikovat	k5eNaImAgMnS	intrikovat
a	a	k8xC	a
stál	stát	k5eAaImAgMnS	stát
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
věrně	věrně	k6eAd1	věrně
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
boku	bok	k1gInSc6	bok
jak	jak	k8xC	jak
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
což	což	k9	což
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
oceňoval	oceňovat	k5eAaImAgMnS	oceňovat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
Filipovi	Filip	k1gMnSc3	Filip
vděčný	vděčný	k2eAgMnSc1d1	vděčný
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
měli	mít	k5eAaImAgMnP	mít
k	k	k7c3	k
sobě	se	k3xPyFc3	se
už	už	k6eAd1	už
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
všem	všecek	k3xTgInPc3	všecek
Filipovým	Filipův	k2eAgInPc3d1	Filipův
skandálům	skandál	k1gInPc3	skandál
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gFnPc3	jeho
povahovým	povahový	k2eAgFnPc3d1	povahová
chybám	chyba	k1gFnPc3	chyba
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc2	svůj
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
velmi	velmi	k6eAd1	velmi
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
,	,	kIx,	,
respektoval	respektovat	k5eAaImAgMnS	respektovat
a	a	k8xC	a
choval	chovat	k5eAaImAgMnS	chovat
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
vždy	vždy	k6eAd1	vždy
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
a	a	k8xC	a
shovívavostí	shovívavost	k1gFnSc7	shovívavost
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
Filipova	Filipův	k2eAgInSc2d1	Filipův
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
ale	ale	k9	ale
nesla	nést	k5eAaImAgFnS	nést
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
ostrého	ostrý	k2eAgInSc2d1	ostrý
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
bratry	bratr	k1gMnPc7	bratr
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
příčinou	příčina	k1gFnSc7	příčina
byl	být	k5eAaImAgInS	být
sňatek	sňatek	k1gInSc1	sňatek
Filipova	Filipův	k2eAgMnSc2d1	Filipův
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
Filipův	Filipův	k2eAgMnSc1d1	Filipův
syn	syn	k1gMnSc1	syn
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
svou	svůj	k3xOyFgFnSc4	svůj
sestřenici	sestřenice	k1gFnSc4	sestřenice
<g/>
,	,	kIx,	,
Ludvíkovu	Ludvíkův	k2eAgFnSc4d1	Ludvíkova
nemanželskou	manželský	k2eNgFnSc4d1	nemanželská
dceru	dcera	k1gFnSc4	dcera
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
milenkou	milenka	k1gFnSc7	milenka
Madame	madame	k1gFnSc2	madame
de	de	k?	de
Montespan	Montespan	k1gInSc1	Montespan
<g/>
,	,	kIx,	,
Františku	Františka	k1gFnSc4	Františka
Marii	Maria	k1gFnSc4	Maria
Bourbonskou	bourbonský	k2eAgFnSc4d1	Bourbonská
(	(	kIx(	(
<g/>
známou	známý	k2eAgFnSc4d1	známá
též	též	k9	též
jako	jako	k8xC	jako
Mademoiselle	Mademoiselle	k1gFnSc1	Mademoiselle
de	de	k?	de
Blois	Blois	k1gInSc1	Blois
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yQnSc7	což
Filip	Filip	k1gMnSc1	Filip
Orléanský	Orléanský	k2eAgMnSc1d1	Orléanský
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svatbě	svatba	k1gFnSc3	svatba
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Františky	Františka	k1gFnSc2	Františka
Marie	Maria	k1gFnSc2	Maria
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1692	[number]	k4	1692
stejně	stejně	k6eAd1	stejně
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Filip	Filip	k1gMnSc1	Filip
Orléanský	Orléanský	k2eAgMnSc1d1	Orléanský
těžce	těžce	k6eAd1	těžce
nesl	nést	k5eAaImAgMnS	nést
a	a	k8xC	a
často	často	k6eAd1	často
tento	tento	k3xDgInSc4	tento
jeho	jeho	k3xOp3gInSc4	jeho
synovi	syn	k1gMnSc3	syn
vnucený	vnucený	k2eAgInSc4d1	vnucený
sňatek	sňatek	k1gInSc4	sňatek
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
vyčítal	vyčítat	k5eAaImAgMnS	vyčítat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
celé	celý	k2eAgInPc1d1	celý
roky	rok	k1gInPc1	rok
trvající	trvající	k2eAgFnSc2d1	trvající
bratrské	bratrský	k2eAgFnSc2d1	bratrská
hádky	hádka	k1gFnSc2	hádka
podlomily	podlomit	k5eAaPmAgFnP	podlomit
Filipovo	Filipův	k2eAgNnSc4d1	Filipovo
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
sporů	spor	k1gInPc2	spor
dostal	dostat	k5eAaPmAgMnS	dostat
Filip	Filip	k1gMnSc1	Filip
záchvat	záchvat	k1gInSc4	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1701	[number]	k4	1701
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
zámku	zámek	k1gInSc6	zámek
Saint	Saint	k1gMnSc1	Saint
<g/>
–	–	k?	–
<g/>
Cloud	Cloud	k1gMnSc1	Cloud
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
bratrově	bratrův	k2eAgFnSc6d1	bratrova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
svědectví	svědectví	k1gNnSc2	svědectví
těžce	těžce	k6eAd1	těžce
otřesen	otřesen	k2eAgMnSc1d1	otřesen
a	a	k8xC	a
hořce	hořko	k6eAd1	hořko
plakal	plakat	k5eAaImAgMnS	plakat
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
Filipově	Filipův	k2eAgFnSc6d1	Filipova
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
smutný	smutný	k2eAgInSc1d1	smutný
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
vinen	vinen	k2eAgMnSc1d1	vinen
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgMnSc6	svůj
bratrovi	bratr	k1gMnSc6	bratr
ztratil	ztratit	k5eAaPmAgMnS	ztratit
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
svého	svůj	k3xOyFgMnSc2	svůj
posledního	poslední	k2eAgMnSc2d1	poslední
opravdového	opravdový	k2eAgMnSc2d1	opravdový
přítele	přítel	k1gMnSc2	přítel
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
zlatého	zlatý	k2eAgInSc2d1	zlatý
věku	věk	k1gInSc2	věk
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
byl	být	k5eAaImAgMnS	být
nositelem	nositel	k1gMnSc7	nositel
následujících	následující	k2eAgInPc2d1	následující
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
oslovení	oslovení	k1gNnSc2	oslovení
<g/>
:	:	kIx,	:
Monsieur	Monsieur	k1gMnSc1	Monsieur
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Anjou	Anjá	k1gFnSc4	Anjá
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1668	[number]	k4	1668
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
orléanský	orléanský	k2eAgMnSc1d1	orléanský
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
získal	získat	k5eAaPmAgMnS	získat
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
Gastona	Gaston	k1gMnSc2	Gaston
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1660	[number]	k4	1660
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Chartres	Chartresa	k1gFnPc2	Chartresa
<g/>
,	,	kIx,	,
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
z	z	k7c2	z
Nemours	Nemoursa	k1gFnPc2	Nemoursa
<g/>
,	,	kIx,	,
z	z	k7c2	z
Montpensier	Montpensira	k1gFnPc2	Montpensira
<g/>
,	,	kIx,	,
z	z	k7c2	z
Châtellerault	Châtelleraulta	k1gFnPc2	Châtelleraulta
<g/>
,	,	kIx,	,
z	z	k7c2	z
Saint-Fargea	Saint-Farge	k1gInSc2	Saint-Farge
<g/>
,	,	kIx,	,
z	z	k7c2	z
Beaupréa	Beaupréum	k1gNnSc2	Beaupréum
<g/>
,	,	kIx,	,
Pair	pair	k1gMnSc1	pair
de	de	k?	de
France	Franc	k1gMnSc4	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
Dauphin	dauphin	k1gMnSc1	dauphin
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Auvergne	Auvergn	k1gMnSc5	Auvergn
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
z	z	k7c2	z
Dombes	Dombesa	k1gFnPc2	Dombesa
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
z	z	k7c2	z
Joinville	Joinville	k1gNnSc2	Joinville
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Dourdan	Dourdana	k1gFnPc2	Dourdana
a	a	k8xC	a
Romorantin	Romorantin	k1gMnSc1	Romorantin
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Mortain	Mortaina	k1gFnPc2	Mortaina
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Bar-sur-Seine	Barur-Sein	k1gInSc5	Bar-sur-Sein
<g/>
,	,	kIx,	,
vikomt	vikomt	k1gMnSc1	vikomt
z	z	k7c2	z
Auge	Aug	k1gFnSc2	Aug
a	a	k8xC	a
Domfront	Domfront	k1gMnSc1	Domfront
<g/>
,	,	kIx,	,
markýz	markýz	k1gMnSc1	markýz
z	z	k7c2	z
Coucy	Couca	k1gFnSc2	Couca
a	a	k8xC	a
Folembray	Folembraa	k1gFnSc2	Folembraa
<g/>
,	,	kIx,	,
markýz	markýz	k1gMnSc1	markýz
z	z	k7c2	z
Méziè	Méziè	k1gFnSc2	Méziè
<g/>
,	,	kIx,	,
baron	baron	k1gMnSc1	baron
z	z	k7c2	z
Beaujolais	Beaujolais	k1gFnSc2	Beaujolais
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Montargis	Montargis	k1gFnSc2	Montargis
a	a	k8xC	a
rytíř	rytíř	k1gMnSc1	rytíř
z	z	k7c2	z
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Ordre	ordre	k1gInSc1	ordre
du	du	k?	du
Saint-Esprit	Saint-Esprit	k1gInSc1	Saint-Esprit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
Henriettou	Henriettý	k2eAgFnSc7d1	Henriettý
Annou	Anna	k1gFnSc7	Anna
Stuartovnou	Stuartovna	k1gFnSc7	Stuartovna
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
Marie	Marie	k1gFnSc1	Marie
Louisa	Louisa	k?	Louisa
Orléanská	Orléanský	k2eAgFnSc1d1	Orléanská
(	(	kIx(	(
<g/>
1662	[number]	k4	1662
<g/>
–	–	k?	–
<g/>
1689	[number]	k4	1689
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
za	za	k7c4	za
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Španělského	španělský	k2eAgInSc2d1	španělský
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
Karel	Karel	k1gMnSc1	Karel
1664	[number]	k4	1664
<g/>
–	–	k?	–
<g/>
1666	[number]	k4	1666
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
Marie	Maria	k1gFnSc2	Maria
Orléanská	Orléanský	k2eAgFnSc1d1	Orléanská
(	(	kIx(	(
<g/>
1669	[number]	k4	1669
<g/>
–	–	k?	–
<g/>
1728	[number]	k4	1728
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
za	za	k7c4	za
Viktora	Viktor	k1gMnSc4	Viktor
Amadea	Amadeus	k1gMnSc4	Amadeus
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
Piemont-Sardinského	Piemont-Sardinský	k2eAgMnSc2d1	Piemont-Sardinský
a	a	k8xC	a
Sicilského	sicilský	k2eAgNnSc2d1	sicilské
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
Alžbětou	Alžběta	k1gFnSc7	Alžběta
Šarlotou	Šarlota	k1gFnSc7	Šarlota
Falckou	falcký	k2eAgFnSc7d1	Falcká
měl	mít	k5eAaImAgMnS	mít
rovněž	rovněž	k9	rovněž
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
Alexander	Alexandra	k1gFnPc2	Alexandra
Ludvík	Ludvík	k1gMnSc1	Ludvík
(	(	kIx(	(
<g/>
1673	[number]	k4	1673
<g/>
–	–	k?	–
<g/>
1676	[number]	k4	1676
<g/>
)	)	kIx)	)
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
orléanský	orléanský	k2eAgMnSc1d1	orléanský
(	(	kIx(	(
<g/>
1674	[number]	k4	1674
<g/>
–	–	k?	–
<g/>
1723	[number]	k4	1723
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokračovatel	pokračovatel	k1gMnSc1	pokračovatel
dynastie	dynastie	k1gFnSc2	dynastie
Bourbon-Orléans	Bourbon-Orléans	k1gInSc1	Bourbon-Orléans
<g/>
,	,	kIx,	,
regent	regent	k1gMnSc1	regent
nezletilého	zletilý	k2eNgMnSc2d1	nezletilý
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Františka	Františka	k1gFnSc1	Františka
Marie	Maria	k1gFnSc2	Maria
Bourbonská	bourbonský	k2eAgFnSc1d1	Bourbonská
<g/>
,	,	kIx,	,
nemanželská	manželský	k2eNgFnSc1d1	nemanželská
dcera	dcera	k1gFnSc1	dcera
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
madame	madame	k1gFnSc7	madame
de	de	k?	de
Montespan	Montespana	k1gFnPc2	Montespana
Alžběta	Alžběta	k1gFnSc1	Alžběta
Charlotta	Charlotta	k1gFnSc1	Charlotta
Orléanská	Orléanský	k2eAgFnSc1d1	Orléanská
(	(	kIx(	(
<g/>
1676	[number]	k4	1676
<g/>
–	–	k?	–
<g/>
1744	[number]	k4	1744
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
za	za	k7c4	za
Leopolda	Leopold	k1gMnSc4	Leopold
Josefa	Josef	k1gMnSc4	Josef
<g/>
,	,	kIx,	,
vévodu	vévoda	k1gMnSc4	vévoda
de	de	k?	de
Bar	bar	k1gInSc4	bar
a	a	k8xC	a
vévodu	vévoda	k1gMnSc4	vévoda
lotrinského	lotrinský	k2eAgMnSc4d1	lotrinský
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
matkou	matka	k1gFnSc7	matka
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
Štěpána	Štěpán	k1gMnSc2	Štěpán
Lotrinského	lotrinský	k2eAgMnSc2d1	lotrinský
<g/>
.	.	kIx.	.
</s>
<s>
Francois	Francois	k1gMnSc1	Francois
Bluche	Bluch	k1gFnSc2	Bluch
<g/>
:	:	kIx,	:
Za	za	k7c2	za
časů	čas	k1gInPc2	čas
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Filip	Filip	k1gMnSc1	Filip
I.	I.	kA	I.
Orléanský	Orléanský	k2eAgMnSc1d1	Orléanský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Filip	Filip	k1gMnSc1	Filip
I.	I.	kA	I.
Orléanský	Orléanský	k2eAgInSc1d1	Orléanský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
http://www.thepeerage.com/p10140.htm#i101396	[url]	k4	http://www.thepeerage.com/p10140.htm#i101396
Genealogický	genealogický	k2eAgInSc4d1	genealogický
strom	strom	k1gInSc4	strom
v	v	k7c6	v
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Encyclopédie	Encyclopédie	k1gFnSc1	Encyclopédie
de	de	k?	de
Diderot	Diderot	k1gMnSc1	Diderot
</s>
