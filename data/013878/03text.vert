<s>
Dolní	dolní	k2eAgFnSc1d1
Domaslavice	Domaslavice	k1gFnSc1
(	(	kIx(
<g/>
zámek	zámek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
Domaslavice	Domaslavice	k1gFnPc1
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1804	#num#	k4
Zánik	zánik	k1gInSc1
</s>
<s>
1958	#num#	k4
Stavebník	stavebník	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Ohm	ohm	k1gInSc4
Janušovský	Janušovský	k2eAgInSc4d1
z	z	k7c2
Vyšehradu	Vyšehrad	k1gInSc2
Další	další	k2eAgMnPc1d1
majitelé	majitel	k1gMnPc1
</s>
<s>
Janušovští	Janušovský	k1gMnPc1
z	z	k7c2
Vyšehradu	Vyšehrad	k1gInSc2
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
Domaslavice	Domaslavice	k1gFnPc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
32,13	32,13	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
57,71	57,71	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
Domaslavice	Domaslavice	k1gFnSc1
(	(	kIx(
<g/>
zámek	zámek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
Domaslavice	Domaslavice	k1gFnSc1
(	(	kIx(
<g/>
zámek	zámek	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zámek	zámek	k1gInSc1
Dolní	dolní	k2eAgFnSc2d1
Domaslavice	Domaslavice	k1gFnSc2
stával	stávat	k5eAaImAgInS
v	v	k7c6
obci	obec	k1gFnSc6
Dolní	dolní	k2eAgFnSc1d1
Domaslavice	Domaslavice	k1gFnSc1
na	na	k7c6
břehu	břeh	k1gInSc6
říčky	říčka	k1gFnSc2
Lučiny	lučina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
dně	dno	k1gNnSc6
Žermanické	žermanický	k2eAgFnSc2d1
přehrady	přehrada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1305	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
soupisu	soupis	k1gInSc6
vratislavského	vratislavský	k2eAgNnSc2d1
biskupství	biskupství	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
už	už	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
existence	existence	k1gFnSc2
byla	být	k5eAaImAgFnS
alodním	alodní	k2eAgNnSc7d1
panstvím	panství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1454	#num#	k4
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Rajko	rajka	k1gFnSc5
z	z	k7c2
Domaslavic	Domaslavice	k1gFnPc2
a	a	k8xC
roku	rok	k1gInSc2
1485	#num#	k4
Jan	Jan	k1gMnSc1
Myloch	Myloch	k1gMnSc1
z	z	k7c2
Domaslavic	Domaslavice	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
o	o	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
z	z	k7c2
Domaslavic	Domaslavice	k1gFnPc2
jde	jít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
obec	obec	k1gFnSc4
vlastnili	vlastnit	k5eAaImAgMnP
Tlukové	Tluek	k1gMnPc1
z	z	k7c2
Tošanovic	Tošanovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1600	#num#	k4
ji	on	k3xPp3gFnSc4
vlastní	vlastní	k2eAgFnSc1d1
Anna	Anna	k1gFnSc1
Marklovská	Marklovský	k2eAgFnSc1d1
z	z	k7c2
Žebráče	Žebráč	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
níž	jenž	k3xRgFnSc2
ji	on	k3xPp3gFnSc4
roku	rok	k1gInSc2
1603	#num#	k4
kupuje	kupovat	k5eAaImIp3nS
Adam	Adam	k1gMnSc1
Scipion	Scipion	k1gInSc1
z	z	k7c2
Kretčína	Kretčín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
zde	zde	k6eAd1
vystřídala	vystřídat	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
majitelů	majitel	k1gMnPc2
–	–	k?
např.	např.	kA
páni	pan	k1gMnPc1
z	z	k7c2
Třánkovic	Třánkovice	k1gFnPc2
<g/>
,	,	kIx,
Skrbenští	Skrbenský	k2eAgMnPc1d1
z	z	k7c2
Hříště	hříště	k1gNnSc2
<g/>
,	,	kIx,
Harasovští	Harasovský	k2eAgMnPc1d1
z	z	k7c2
Harasova	Harasův	k2eAgInSc2d1
–	–	k?
až	až	k9
se	se	k3xPyFc4
roku	rok	k1gInSc2
1756	#num#	k4
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
vlastnictví	vlastnictví	k1gNnSc2
Janušovských	Janušovská	k1gFnPc2
z	z	k7c2
Vyšehradu	Vyšehrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1804	#num#	k4
nechal	nechat	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Ohm	ohm	k1gInSc4
Janušovský	Janušovský	k2eAgInSc4d1
z	z	k7c2
Vyšehradu	Vyšehrad	k1gInSc2
v	v	k7c6
obci	obec	k1gFnSc6
postavit	postavit	k5eAaPmF
zámek	zámek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1836	#num#	k4
jej	on	k3xPp3gMnSc4
vlastnil	vlastnit	k5eAaImAgMnS
kupec	kupec	k1gMnSc1
František	František	k1gMnSc1
Knězek	Knězek	k1gInSc4
z	z	k7c2
Nového	Nového	k2eAgInSc2d1
Jičína	Jičín	k1gInSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Rudolf	Rudolf	k1gMnSc1
Moselr	Moselr	k1gMnSc1
<g/>
,	,	kIx,
Felix	Felix	k1gMnSc1
Roter	Roter	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1876	#num#	k4
Jakub	Jakub	k1gMnSc1
Kulban	Kulban	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1883	#num#	k4
Pavel	Pavel	k1gMnSc1
Goch	Goch	k1gMnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1884	#num#	k4
Artur	Artur	k1gMnSc1
a	a	k8xC
Emilie	Emilie	k1gFnSc1
Gochovi	Gochovi	k1gRnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1903	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozparcelování	rozparcelování	k1gNnSc3
statku	statek	k1gInSc2
a	a	k8xC
zámek	zámek	k1gInSc4
odkoupil	odkoupit	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
Slanina	Slanina	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
v	v	k7c6
hospodářských	hospodářský	k2eAgFnPc6d1
budovách	budova	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
zřídil	zřídit	k5eAaPmAgMnS
garáž	garáž	k1gFnSc4
pro	pro	k7c4
autobusy	autobus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
zámek	zámek	k1gInSc1
vlastnili	vlastnit	k5eAaImAgMnP
ČSAD	ČSAD	kA
<g/>
,	,	kIx,
kterým	který	k3yRgMnSc7,k3yIgMnSc7,k3yQgMnSc7
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
zamítnut	zamítnout	k5eAaPmNgInS
příspěvek	příspěvek	k1gInSc1
na	na	k7c4
údržbu	údržba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
výstavby	výstavba	k1gFnSc2
Žermanické	žermanický	k2eAgFnSc2d1
přehrady	přehrada	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
byl	být	k5eAaImAgInS
zbořen	zbořit	k5eAaPmNgInS
a	a	k8xC
místo	místo	k1gNnSc1
zatopeno	zatopen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zámek	zámek	k1gInSc1
na	na	k7c4
hrady	hrad	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Zámky	zámek	k1gInPc1
na	na	k7c6
Lučině	lučina	k1gFnSc6
</s>
