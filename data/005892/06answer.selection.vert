<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS
rádiové	rádiový	k2eAgNnSc1d1
a	a	k8xC
infračervené	infračervený	k2eAgNnSc1d1
záření	záření	k1gNnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
viditelné	viditelný	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
o	o	k7c6
velmi	velmi	k6eAd1
dlouhé	dlouhý	k2eAgFnSc3d1
vlnové	vlnový	k2eAgFnSc3d1
délce	délka	k1gFnSc3
<g/>
,	,	kIx,
tj.	tj.	kA
červené	červený	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
<g/>
.	.	kIx.
</s>