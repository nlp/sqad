<s>
Antonie	Antonie	k1gFnSc1	Antonie
Ludmila	Ludmila	k1gFnSc1	Ludmila
baronka	baronka	k1gFnSc1	baronka
von	von	k1gInSc1	von
Procházka	procházka	k1gFnSc1	procházka
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Gundling	Gundling	k1gInSc1	Gundling
<g/>
(	(	kIx(	(
<g/>
ová	ová	k?	ová
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Tonia	Tonia	k1gFnSc1	Tonia
Baronin	Baronin	k1gInSc4	Baronin
von	von	k1gInSc1	von
Procházka	procházka	k1gFnSc1	procházka
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Antonie	Antonie	k1gFnSc2	Antonie
Ludmila	Ludmila	k1gFnSc1	Ludmila
Monika	Monika	k1gFnSc1	Monika
svobodná	svobodný	k2eAgFnSc1d1	svobodná
paní	paní	k1gFnSc1	paní
von	von	k1gInSc1	von
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
;	;	kIx,	;
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášter	klášter	k1gInSc1	klášter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
dobrodinka	dobrodinka	k1gFnSc1	dobrodinka
a	a	k8xC	a
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Klubu	klub	k1gInSc2	klub
německých	německý	k2eAgFnPc2d1	německá
umělkyň	umělkyně	k1gFnPc2	umělkyně
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
Klub	klub	k1gInSc1	klub
deutscher	deutschra	k1gFnPc2	deutschra
Künstlerinnen	Künstlerinnen	k1gInSc4	Künstlerinnen
in	in	k?	in
Prag	Prag	k1gInSc1	Prag
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
