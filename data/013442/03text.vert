<p>
<s>
Antonie	Antonie	k1gFnSc1	Antonie
Ludmila	Ludmila	k1gFnSc1	Ludmila
baronka	baronka	k1gFnSc1	baronka
von	von	k1gInSc1	von
Procházka	procházka	k1gFnSc1	procházka
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Gundling	Gundling	k1gInSc1	Gundling
<g/>
(	(	kIx(	(
<g/>
ová	ová	k?	ová
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Tonia	Tonia	k1gFnSc1	Tonia
Baronin	Baronin	k1gInSc4	Baronin
von	von	k1gInSc1	von
Procházka	procházka	k1gFnSc1	procházka
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Antonie	Antonie	k1gFnSc2	Antonie
Ludmila	Ludmila	k1gFnSc1	Ludmila
Monika	Monika	k1gFnSc1	Monika
svobodná	svobodný	k2eAgFnSc1d1	svobodná
paní	paní	k1gFnSc1	paní
von	von	k1gInSc1	von
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
;	;	kIx,	;
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášter	klášter	k1gInSc1	klášter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
dobrodinka	dobrodinka	k1gFnSc1	dobrodinka
a	a	k8xC	a
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Klubu	klub	k1gInSc2	klub
německých	německý	k2eAgFnPc2d1	německá
umělkyň	umělkyně	k1gFnPc2	umělkyně
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
Klub	klub	k1gInSc1	klub
deutscher	deutschra	k1gFnPc2	deutschra
Künstlerinnen	Künstlerinnen	k1gInSc4	Künstlerinnen
in	in	k?	in
Prag	Prag	k1gInSc1	Prag
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k8xC	jako
Antonie	Antonie	k1gFnSc1	Antonie
Ludmila	Ludmila	k1gFnSc1	Ludmila
Monika	Monika	k1gFnSc1	Monika
Gundlingová	Gundlingový	k2eAgFnSc1d1	Gundlingový
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
Eduarda	Eduard	k1gMnSc2	Eduard
Gundlinga	Gundling	k1gMnSc2	Gundling
a	a	k8xC	a
Pauliny	Paulin	k2eAgFnSc2d1	Paulina
Gundlingové	Gundlingový	k2eAgFnSc2d1	Gundlingový
rozené	rozený	k2eAgFnSc2d1	rozená
Stupkové	Stupková	k1gFnSc2	Stupková
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
francké	francký	k2eAgFnSc6d1	Francká
rodině	rodina	k1gFnSc6	rodina
Gundlingových	Gundlingový	k2eAgMnPc2d1	Gundlingový
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
pocházeli	pocházet	k5eAaImAgMnP	pocházet
např.	např.	kA	např.
slavní	slavný	k2eAgMnPc1d1	slavný
bratři	bratr	k1gMnPc1	bratr
Jacob	Jacoba	k1gFnPc2	Jacoba
Paul	Paula	k1gFnPc2	Paula
von	von	k1gInSc1	von
Gundling	Gundling	k1gInSc1	Gundling
a	a	k8xC	a
Nikolaus	Nikolaus	k1gInSc1	Nikolaus
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Gundling	Gundling	k1gInSc1	Gundling
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
strýc	strýc	k1gMnSc1	strýc
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Gundling	Gundling	k1gInSc1	Gundling
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
klášterní	klášterní	k2eAgFnSc2d1	klášterní
školy	škola	k1gFnSc2	škola
řádu	řád	k1gInSc2	řád
voršilek	voršilka	k1gFnPc2	voršilka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1867	[number]	k4	1867
až	až	k9	až
1873	[number]	k4	1873
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
následující	následující	k2eAgInPc4d1	následující
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
kurzy	kurz	k1gInPc1	kurz
malování	malování	k1gNnSc2	malování
a	a	k8xC	a
kreslení	kreslení	k1gNnSc2	kreslení
pro	pro	k7c4	pro
dámy	dáma	k1gFnPc4	dáma
na	na	k7c6	na
c.	c.	k?	c.
k.	k.	k?	k.
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc3d1	uměleckoprůmyslová
škole	škola	k1gFnSc3	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
podnikla	podniknout	k5eAaPmAgFnS	podniknout
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Švýcar	Švýcar	k1gMnSc1	Švýcar
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Španěl	Španěl	k1gMnSc1	Španěl
a	a	k8xC	a
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jí	on	k3xPp3gFnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
získat	získat	k5eAaPmF	získat
si	se	k3xPyFc3	se
velký	velký	k2eAgInSc4d1	velký
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
rozhled	rozhled	k1gInSc4	rozhled
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1893	[number]	k4	1893
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
ministerského	ministerský	k2eAgMnSc4d1	ministerský
radu	rada	k1gMnSc4	rada
Rudolfa	Rudolf	k1gMnSc2	Rudolf
sv.	sv.	kA	sv.
p.	p.	k?	p.
von	von	k1gInSc1	von
Procházka	procházka	k1gFnSc1	procházka
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1900	[number]	k4	1900
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
Romana	Roman	k1gMnSc4	Roman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pražském	pražský	k2eAgNnSc6d1	Pražské
povstání	povstání	k1gNnSc6	povstání
proti	proti	k7c3	proti
německým	německý	k2eAgMnPc3d1	německý
okupantům	okupant	k1gMnPc3	okupant
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
Antonie	Antonie	k1gFnSc1	Antonie
baronka	baronka	k1gFnSc1	baronka
Procházková	Procházková	k1gFnSc1	Procházková
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
pro	pro	k7c4	pro
Němce	Němec	k1gMnSc4	Němec
zřízeném	zřízený	k2eAgNnSc6d1	zřízené
v	v	k7c6	v
Břevnovském	břevnovský	k2eAgInSc6d1	břevnovský
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
následně	následně	k6eAd1	následně
16	[number]	k4	16
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Působení	působení	k1gNnSc2	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
členka	členka	k1gFnSc1	členka
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
společnosti	společnost	k1gFnSc2	společnost
červeného	červený	k2eAgInSc2d1	červený
kříže	kříž	k1gInSc2	kříž
byla	být	k5eAaImAgFnS	být
Antonie	Antonie	k1gFnSc1	Antonie
baronka	baronka	k1gFnSc1	baronka
Procházková	Procházková	k1gFnSc1	Procházková
nejprve	nejprve	k6eAd1	nejprve
zapisovatelkou	zapisovatelka	k1gFnSc7	zapisovatelka
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
Ženského	ženský	k2eAgInSc2d1	ženský
pomocného	pomocný	k2eAgInSc2d1	pomocný
spolku	spolek	k1gInSc2	spolek
Červeného	Červeného	k2eAgInSc2d1	Červeného
kříže	kříž	k1gInSc2	kříž
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
Frauenhilfsverein	Frauenhilfsverein	k2eAgInSc1d1	Frauenhilfsverein
vom	vom	k?	vom
Roten	Roten	k1gInSc1	Roten
Kreuze	Kreuze	k1gFnSc1	Kreuze
im	im	k?	im
Königreich	Königreich	k1gInSc1	Königreich
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
angažovala	angažovat	k5eAaBmAgFnS	angažovat
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
pomoci	pomoct	k5eAaPmF	pomoct
lidem	člověk	k1gMnPc3	člověk
v	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
činná	činný	k2eAgFnSc1d1	činná
také	také	k9	také
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
podpory	podpora	k1gFnSc2	podpora
<g/>
,	,	kIx,	,
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
učitelka	učitelka	k1gFnSc1	učitelka
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
Klubu	klub	k1gInSc2	klub
německých	německý	k2eAgFnPc2d1	německá
umělkyň	umělkyně	k1gFnPc2	umělkyně
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
sociálně	sociálně	k6eAd1	sociálně
slabé	slabý	k2eAgNnSc4d1	slabé
a	a	k8xC	a
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vojenské	vojenský	k2eAgFnSc2d1	vojenská
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
péče	péče	k1gFnSc2	péče
v	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válce	válka	k1gFnSc6	válka
obdržela	obdržet	k5eAaPmAgFnS	obdržet
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1916	[number]	k4	1916
Čestný	čestný	k2eAgInSc1d1	čestný
odznak	odznak	k1gInSc1	odznak
Rakouského	rakouský	k2eAgInSc2d1	rakouský
Červeného	Červeného	k2eAgInSc2d1	Červeného
kříže	kříž	k1gInSc2	kříž
s	s	k7c7	s
válečnou	válečný	k2eAgFnSc7d1	válečná
dekorací	dekorace	k1gFnSc7	dekorace
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
jí	jíst	k5eAaImIp3nS	jíst
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
Válečný	válečný	k2eAgInSc4d1	válečný
kříž	kříž	k1gInSc4	kříž
za	za	k7c2	za
občanské	občanský	k2eAgFnSc2d1	občanská
zásluhy	zásluha	k1gFnSc2	zásluha
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Uznání	uznání	k1gNnSc1	uznání
zvláště	zvláště	k6eAd1	zvláště
záslužné	záslužný	k2eAgNnSc1d1	záslužné
působení	působení	k1gNnSc1	působení
v	v	k7c6	v
Rakouského	rakouský	k2eAgInSc2d1	rakouský
Červeného	Červeného	k2eAgInSc2d1	Červeného
kříže	kříž	k1gInSc2	kříž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tonia	Tonia	k1gFnSc1	Tonia
von	von	k1gInSc1	von
Procházka	Procházka	k1gMnSc1	Procházka
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
von	von	k1gInSc1	von
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
:	:	kIx,	:
Meine	Mein	k1gInSc5	Mein
32	[number]	k4	32
Ahnen	Ahnen	k2eAgMnSc1d1	Ahnen
und	und	k?	und
ihre	ihrat	k5eAaPmIp3nS	ihrat
Sippenkreise	Sippenkreise	k1gFnSc1	Sippenkreise
<g/>
.	.	kIx.	.
</s>
<s>
Verlag	Verlag	k1gMnSc1	Verlag
Degener	Degener	k1gMnSc1	Degener
<g/>
.	.	kIx.	.
</s>
<s>
Leipzig	Leipzig	k1gInSc1	Leipzig
1928	[number]	k4	1928
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lukas	Lukas	k1gInSc1	Lukas
C.	C.	kA	C.
Gundling	Gundling	k1gInSc1	Gundling
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc1	Die
Prager	Prager	k1gMnSc1	Prager
Gundlinge	Gundlinge	k1gInSc1	Gundlinge
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Genealogische	Genealogisch	k1gMnSc2	Genealogisch
Blätter	Blätter	k1gMnSc1	Blätter
der	drát	k5eAaImRp2nS	drát
Familie	Familie	k1gFnPc1	Familie
Gundling	Gundling	k1gInSc1	Gundling
und	und	k?	und
anverwandte	anverwandte	k5eAaPmIp2nP	anverwandte
Familien	Familien	k1gInSc4	Familien
Nr	Nr	k1gFnSc2	Nr
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
Schwäbisch	Schwäbisch	k1gMnSc1	Schwäbisch
Gmünd	Gmünd	k1gMnSc1	Gmünd
<g/>
/	/	kIx~	/
<g/>
Erfurt	Erfurt	k1gInSc1	Erfurt
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
S.	S.	kA	S.
2	[number]	k4	2
<g/>
ff	ff	kA	ff
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lukas	Lukas	k1gInSc1	Lukas
C.	C.	kA	C.
Gundling	Gundling	k1gInSc1	Gundling
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc1	Die
Wege	Weg	k1gFnSc2	Weg
der	drát	k5eAaImRp2nS	drát
Gundlinge	Gundlinge	k1gNnSc4	Gundlinge
nach	nach	k1gInSc1	nach
Osten	osten	k1gInSc1	osten
<g/>
:	:	kIx,	:
Wie	Wie	k1gFnSc1	Wie
die	die	k?	die
Gundlinge	Gundlinge	k1gInSc1	Gundlinge
von	von	k1gInSc1	von
Württemberg	Württemberg	k1gInSc1	Württemberg
nach	nach	k1gInSc1	nach
Danzig	Danzig	k1gInSc4	Danzig
<g/>
,	,	kIx,	,
Krakau	Krakaa	k1gFnSc4	Krakaa
<g/>
,	,	kIx,	,
Prag	Prag	k1gMnSc1	Prag
und	und	k?	und
Wien	Wien	k1gNnSc4	Wien
kamen	kamna	k1gNnPc2	kamna
<g/>
,	,	kIx,	,
nebst	nebst	k1gMnSc1	nebst
der	drát	k5eAaImRp2nS	drát
Verbindung	Verbindung	k1gInSc1	Verbindung
der	drát	k5eAaImRp2nS	drát
Gundlinge	Gundlinge	k1gInSc1	Gundlinge
zum	zum	k?	zum
Genealogen	Genealogen	k1gInSc1	Genealogen
Roman	Roman	k1gMnSc1	Roman
von	von	k1gInSc1	von
Procházka	procházka	k1gFnSc1	procházka
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Südwestdeutsche	Südwestdeutsche	k1gFnSc1	Südwestdeutsche
Blätter	Blätter	k1gMnSc1	Blätter
für	für	k?	für
Familien-	Familien-	k1gMnSc1	Familien-
und	und	k?	und
Wappenkunde	Wappenkund	k1gInSc5	Wappenkund
(	(	kIx(	(
<g/>
SWDB	SWDB	kA	SWDB
<g/>
)	)	kIx)	)
Band	banda	k1gFnPc2	banda
34	[number]	k4	34
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
S.	S.	kA	S.
96	[number]	k4	96
<g/>
f.	f.	k?	f.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
