<p>
<s>
Ruka	ruka	k1gFnSc1	ruka
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
manus	manus	k1gInSc1	manus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
trupu	trup	k1gInSc2	trup
nejdistálnější	distální	k2eAgInSc4d3	distální
(	(	kIx(	(
<g/>
nejvzdálenější	vzdálený	k2eAgInSc4d3	nejvzdálenější
<g/>
)	)	kIx)	)
oddíl	oddíl	k1gInSc4	oddíl
horní	horní	k2eAgFnSc2d1	horní
končetiny	končetina	k1gFnSc2	končetina
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
interakci	interakce	k1gFnSc4	interakce
jedince	jedinec	k1gMnSc2	jedinec
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
a	a	k8xC	a
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
předměty	předmět	k1gInPc7	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
projevem	projev	k1gInSc7	projev
ruky	ruka	k1gFnSc2	ruka
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
úchopu	úchop	k1gInSc2	úchop
umožněná	umožněný	k2eAgFnSc1d1	umožněná
díky	díky	k7c3	díky
palci	palec	k1gInSc3	palec
protistojnému	protistojný	k2eAgInSc3d1	protistojný
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgInPc3d1	ostatní
prstům	prst	k1gInPc3	prst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruka	ruka	k1gFnSc1	ruka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
zápěstí	zápěstí	k1gNnSc2	zápěstí
a	a	k8xC	a
pěti	pět	k4xCc2	pět
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohyby	pohyb	k1gInPc4	pohyb
pronace	pronace	k1gFnSc2	pronace
a	a	k8xC	a
supinace	supinace	k1gFnSc2	supinace
(	(	kIx(	(
<g/>
otočení	otočení	k1gNnSc4	otočení
dlaně	dlaň	k1gFnPc1	dlaň
(	(	kIx(	(
<g/>
palmy	palma	k1gFnPc1	palma
<g/>
)	)	kIx)	)
ventrálním	ventrální	k2eAgMnPc3d1	ventrální
(	(	kIx(	(
<g/>
dopředu	dopředu	k6eAd1	dopředu
<g/>
)	)	kIx)	)
a	a	k8xC	a
dorsálním	dorsální	k2eAgMnSc6d1	dorsální
(	(	kIx(	(
<g/>
dozadu	dozadu	k6eAd1	dozadu
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základním	základní	k2eAgNnSc6d1	základní
anatomickém	anatomický	k2eAgNnSc6d1	anatomické
postavení	postavení	k1gNnSc6	postavení
směřují	směřovat	k5eAaImIp3nP	směřovat
dlaně	dlaň	k1gFnPc4	dlaň
ventrálně	ventrálně	k6eAd1	ventrálně
(	(	kIx(	(
<g/>
dopředu	dopředu	k6eAd1	dopředu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
používá	používat	k5eAaImIp3nS	používat
častěji	často	k6eAd2	často
pravou	pravý	k2eAgFnSc4d1	pravá
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
těmto	tento	k3xDgMnPc3	tento
lidem	člověk	k1gMnPc3	člověk
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
praváci	pravák	k1gMnPc1	pravák
<g/>
.	.	kIx.	.
</s>
<s>
Málo	málo	k4c1	málo
lidí	člověk	k1gMnPc2	člověk
používá	používat	k5eAaImIp3nS	používat
častěji	často	k6eAd2	často
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
těmto	tento	k3xDgMnPc3	tento
lidem	člověk	k1gMnPc3	člověk
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
leváci	levák	k1gMnPc1	levák
<g/>
.	.	kIx.	.
</s>
<s>
Přednostní	přednostní	k2eAgNnSc1d1	přednostní
užívání	užívání	k1gNnSc1	užívání
levé	levý	k2eAgFnSc2d1	levá
nebo	nebo	k8xC	nebo
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
dimenzí	dimenze	k1gFnPc2	dimenze
laterality	lateralita	k1gFnSc2	lateralita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Leváctví	leváctví	k1gNnSc1	leváctví
</s>
</p>
<p>
<s>
Šestiprstost	Šestiprstost	k1gFnSc1	Šestiprstost
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ruka	ruka	k1gFnSc1	ruka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Ruka	ruka	k1gFnSc1	ruka
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ruka	ruka	k1gFnSc1	ruka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
