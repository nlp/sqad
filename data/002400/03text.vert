<s>
Lupus	lupus	k1gInSc1	lupus
vulgaris	vulgaris	k1gFnSc2	vulgaris
je	být	k5eAaImIp3nS	být
chronické	chronický	k2eAgNnSc1d1	chronické
tuberkulózní	tuberkulózní	k2eAgNnSc1d1	tuberkulózní
kožní	kožní	k2eAgNnSc1d1	kožní
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
loupajícími	loupající	k2eAgInPc7d1	loupající
strupy	strup	k1gInPc7	strup
a	a	k8xC	a
zaschlými	zaschlý	k2eAgInPc7d1	zaschlý
kusy	kus	k1gInPc7	kus
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
po	po	k7c6	po
nichž	jenž	k3xRgMnPc6	jenž
zbývají	zbývat	k5eAaImIp3nP	zbývat
jen	jen	k9	jen
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
hojící	hojící	k2eAgFnPc1d1	hojící
jizvy	jizva	k1gFnPc1	jizva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
objev	objev	k1gInSc4	objev
fototerapie	fototerapie	k1gFnSc2	fototerapie
účinné	účinný	k2eAgFnPc1d1	účinná
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnSc4	onemocnění
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
dánský	dánský	k2eAgMnSc1d1	dánský
lékař	lékař	k1gMnSc1	lékař
Niels	Nielsa	k1gFnPc2	Nielsa
Ryberg	Ryberg	k1gMnSc1	Ryberg
Finsen	Finsen	k2eAgMnSc1d1	Finsen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
</s>
