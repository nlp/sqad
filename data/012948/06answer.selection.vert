<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
má	mít	k5eAaImIp3nS	mít
oceánské	oceánský	k2eAgNnSc1d1	oceánské
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkými	krátké	k1gNnPc7	krátké
<g/>
,	,	kIx,	,
chladnými	chladný	k2eAgNnPc7d1	chladné
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgInPc1d1	vlhký
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
mírné	mírný	k2eAgFnSc2d1	mírná
zimy	zima	k1gFnSc2	zima
<g/>
:	:	kIx,	:
průměr	průměr	k1gInSc4	průměr
srážek	srážka	k1gFnPc2	srážka
3000	[number]	k4	3000
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
srážky	srážka	k1gFnPc1	srážka
rychle	rychle	k6eAd1	rychle
klesájí	klesájet	k5eAaImIp3nP	klesájet
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
.	.	kIx.	.
</s>
