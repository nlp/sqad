<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
konzumace	konzumace	k1gFnSc1	konzumace
piva	pivo	k1gNnSc2	pivo
má	mít	k5eAaImIp3nS	mít
příznivé	příznivý	k2eAgInPc4d1	příznivý
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
dobrou	dobrý	k2eAgFnSc4d1	dobrá
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc4	snížení
rizika	riziko	k1gNnSc2	riziko
srdečních	srdeční	k2eAgFnPc2d1	srdeční
příhod	příhoda	k1gFnPc2	příhoda
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
vysokému	vysoký	k2eAgInSc3d1	vysoký
krevnímu	krevní	k2eAgInSc3d1	krevní
tlaku	tlak	k1gInSc3	tlak
<g/>
.	.	kIx.	.
</s>
