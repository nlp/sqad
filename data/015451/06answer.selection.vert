<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
látku	látka	k1gFnSc4
používaná	používaný	k2eAgFnSc1d1
zejména	zejména	k9
při	při	k7c6
výrobě	výroba	k1gFnSc6
polyesterů	polyester	k1gInPc2
a	a	k8xC
své	svůj	k3xOyFgNnSc4
uplatnění	uplatnění	k1gNnSc4
má	mít	k5eAaImIp3nS
i	i	k9
v	v	k7c6
polyuretanových	polyuretanový	k2eAgFnPc6d1
pěnách	pěna	k1gFnPc6
<g/>
,	,	kIx,
zde	zde	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
změkčovadlo	změkčovadlo	k1gNnSc1
<g/>
.	.	kIx.
</s>