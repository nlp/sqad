<s>
Pentan-	Pentan-	k?
<g/>
1,5	1,5	k4
<g/>
-diol	-diol	k1gInSc1
</s>
<s>
pentan-	pentan-	k?
<g/>
1,5	1,5	k4
<g/>
-diol	-diol	k1gInSc1
</s>
<s>
Struktura	struktura	k1gFnSc1
pentandiolu	pentandiol	k1gInSc2
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
pentan-	pentan-	k?
<g/>
1,5	1,5	k4
<g/>
-diol	-diol	k1gInSc1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
1,5	1,5	k4
<g/>
-pentandiol	-pentandiol	k1gInSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
C5H12O2	C5H12O2	k4
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
bezbarvá	bezbarvý	k2eAgFnSc1d1
olejovitá	olejovitý	k2eAgFnSc1d1
kapalina	kapalina	k1gFnSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
111-29-5	111-29-5	k4
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
104,15	104,15	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
−	−	k?
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
255	#num#	k4
K	k	k7c3
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
242	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
515	#num#	k4
K	k	k7c3
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
0,994	0,994	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
ve	v	k7c6
vodě	voda	k1gFnSc6
</s>
<s>
mísitelný	mísitelný	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
NFPA	NFPA	kA
704	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pentan-	Pentan-	k?
<g/>
1,5	1,5	k4
<g/>
-diol	-diola	k1gFnPc2
je	být	k5eAaImIp3nS
organická	organický	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
s	s	k7c7
molekulou	molekula	k1gFnSc7
HOCH	hoch	k1gMnSc1
<g/>
2	#num#	k4
<g/>
-CH	-CH	k?
<g/>
2	#num#	k4
<g/>
-CH	-CH	k?
<g/>
2	#num#	k4
<g/>
-CH	-CH	k?
<g/>
2	#num#	k4
<g/>
-CH	-CH	k?
<g/>
2	#num#	k4
<g/>
OH	OH	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
mastná	mastný	k2eAgFnSc1d1
a	a	k8xC
viskózní	viskózní	k2eAgFnSc1d1
kapalina	kapalina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
látku	látka	k1gFnSc4
používaná	používaný	k2eAgFnSc1d1
zejména	zejména	k9
při	při	k7c6
výrobě	výroba	k1gFnSc6
polyesterů	polyester	k1gInPc2
a	a	k8xC
své	svůj	k3xOyFgNnSc4
uplatnění	uplatnění	k1gNnSc4
má	mít	k5eAaImIp3nS
i	i	k9
v	v	k7c6
polyuretanových	polyuretanový	k2eAgFnPc6d1
pěnách	pěna	k1gFnPc6
<g/>
,	,	kIx,
zde	zde	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
změkčovadlo	změkčovadlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
hydrogenací	hydrogenace	k1gFnSc7
kyseliny	kyselina	k1gFnSc2
glutarové	glutarová	k1gFnSc2
(	(	kIx(
<g/>
HO	on	k3xPp3gMnSc4
<g/>
2	#num#	k4
<g/>
C	C	kA
<g/>
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
CO	co	k8xS
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
hydrogenolýza	hydrogenolýza	k1gFnSc1
tetrahydrofurfurylalkoholu	tetrahydrofurfurylalkohol	k1gInSc2
((	((	k?
<g/>
oxolan-	oxolan-	k?
<g/>
2	#num#	k4
<g/>
-yl	-yl	k?
<g/>
)	)	kIx)
<g/>
methanolu	methanol	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Alkoholy	alkohol	k1gInPc1
<g/>
:	:	kIx,
Alkanoly	Alkanol	k1gInPc1
(	(	kIx(
<g/>
alkoholy	alkohol	k1gInPc1
odvozené	odvozený	k2eAgInPc1d1
od	od	k7c2
alkanů	alkan	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
nižší	nízký	k2eAgInPc1d2
alkanoly	alkanol	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
methanol	methanol	k1gInSc1
CH3OH	CH3OH	k1gFnSc2
•	•	k?
ethanol	ethanol	k1gInSc1
C2H5OH	C2H5OH	k1gMnSc1
•	•	k?
propanol	propanol	k1gInSc1
(	(	kIx(
<g/>
propan-	propan-	k?
<g/>
1	#num#	k4
<g/>
-ol	-ol	k?
C	C	kA
<g/>
3	#num#	k4
<g/>
H	H	kA
<g/>
7	#num#	k4
<g/>
OH	OH	kA
<g/>
,	,	kIx,
</s>
<s>
isopropylalkohol	isopropylalkohol	k1gInSc1
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
CHOH	CHOH	kA
<g/>
)	)	kIx)
</s>
<s>
butanol	butanol	k1gInSc1
(	(	kIx(
<g/>
butan-	butan-	k?
<g/>
2	#num#	k4
<g/>
-ol	-ol	k?
<g/>
,	,	kIx,
isobutanol	isobutanol	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
-methylpropan-	-methylpropan-	k?
<g/>
1	#num#	k4
<g/>
-ol	-ol	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g/>
-methylpropan-	-methylpropan-	k?
<g/>
2	#num#	k4
<g/>
-ol	-ol	k?
C	C	kA
<g/>
4	#num#	k4
<g/>
H	H	kA
<g/>
9	#num#	k4
<g/>
OH	OH	kA
<g/>
)	)	kIx)
•	•	k?
pentanol	pentanol	k1gInSc1
<g/>
,	,	kIx,
isopentylalkohol	isopentylalkohol	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
-methylbutan-	-methylbutan-	k?
<g/>
1	#num#	k4
<g/>
-ol	-ol	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
-methylbutan-	-methylbutan-	k?
<g/>
2	#num#	k4
<g/>
-ol	-ol	k?
<g/>
,	,	kIx,
pentan-	pentan-	k?
<g/>
2	#num#	k4
<g/>
-ol	-ol	k?
<g/>
,	,	kIx,
pentan-	pentan-	k?
<g/>
3	#num#	k4
<g/>
-ol	-ol	k?
<g/>
,	,	kIx,
C5H11OH	C5H11OH	k1gMnSc1
•	•	k?
hexanol	hexanol	k1gInSc1
C6H13OH	C6H13OH	k1gFnSc1
•	•	k?
heptanol	heptanol	k1gInSc4
C7H15OH	C7H15OH	k1gFnSc2
</s>
<s>
mastné	mastný	k2eAgFnPc1d1
alkanoly	alkanola	k1gFnPc1
</s>
<s>
oktanol	oktanol	k1gInSc1
C8H17OH	C8H17OH	k1gFnSc2
(	(	kIx(
<g/>
oktan-	oktan-	k?
<g/>
1	#num#	k4
<g/>
-ol	-ol	k?
<g/>
,	,	kIx,
oktan-	oktan-	k?
<g/>
2	#num#	k4
<g/>
-ol	-ol	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
-ethylhexanol	-ethylhexanola	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
nonanol	nonanol	k1gInSc1
C9H19OH	C9H19OH	k1gMnSc1
•	•	k?
dekanol	dekanol	k1gInSc1
C10H21OH	C10H21OH	k1gMnSc1
•	•	k?
undekanol	undekanol	k1gInSc1
C11H23OH	C11H23OH	k1gMnSc1
•	•	k?
dodekanol	dodekanol	k1gInSc1
C12H25OH	C12H25OH	k1gMnSc1
•	•	k?
tridekanol	tridekanol	k1gInSc1
C13H27OH	C13H27OH	k1gMnSc1
•	•	k?
tetradekanol	tetradekanol	k1gInSc1
C14H29OH	C14H29OH	k1gMnSc1
•	•	k?
pentadekanol	pentadekanol	k1gInSc1
C15H31OH	C15H31OH	k1gMnSc1
•	•	k?
hexadekanol	hexadekanol	k1gInSc1
(	(	kIx(
<g/>
cetylalkohol	cetylalkohol	k1gInSc1
<g/>
)	)	kIx)
C16H33OH	C16H33OH	k1gFnSc1
</s>
<s>
fenylalkanoly	fenylalkanola	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
benzylalkohol	benzylalkohol	k1gInSc1
C6H5CH2OH	C6H5CH2OH	k1gFnSc2
•	•	k?
fenethylalkohol	fenethylalkohol	k1gInSc1
C6H5CH2CH2OH	C6H5CH2CH2OH	k1gFnPc2
</s>
<s>
alkandioly	alkandiola	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
ethandiol	ethandiol	k1gInSc1
(	(	kIx(
<g/>
ethylenglykol	ethylenglykol	k1gInSc1
<g/>
)	)	kIx)
C	C	kA
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
•	•	k?
propan-	propan-	k?
<g/>
1,2	1,2	k4
<g/>
-diol	-diol	k1gInSc1
(	(	kIx(
<g/>
propylenglykol	propylenglykol	k1gInSc1
<g/>
)	)	kIx)
C	C	kA
<g/>
3	#num#	k4
<g/>
H	H	kA
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
•	•	k?
propan-	propan-	k?
<g/>
1,3	1,3	k4
<g/>
-diol	-diol	k1gInSc1
C	C	kA
<g/>
3	#num#	k4
<g/>
H	H	kA
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
•	•	k?
pentan-	pentan-	k?
<g/>
1,5	1,5	k4
<g/>
-diol	-diola	k1gFnPc2
HOCH	hoch	k1gMnSc1
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
OH	OH	kA
</s>
<s>
alkantrioly	alkantriola	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
propantriol	propantriol	k1gInSc1
(	(	kIx(
<g/>
glycerol	glycerol	k1gInSc1
<g/>
)	)	kIx)
C	C	kA
<g/>
3	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
3	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
