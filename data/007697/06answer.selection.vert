<s>
Stavbu	stavba	k1gFnSc4	stavba
financovala	financovat	k5eAaBmAgFnS	financovat
Nationale	Nationale	k1gFnSc7	Nationale
Nederlanden	Nederlandna	k1gFnPc2	Nederlandna
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ING.	ing.	kA	ing.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
Tančící	tančící	k2eAgInSc1d1	tančící
dům	dům	k1gInSc1	dům
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Zlatým	zlatý	k2eAgMnSc7d1	zlatý
Andělem	Anděl	k1gMnSc7	Anděl
vložen	vložen	k2eAgInSc4d1	vložen
jako	jako	k8xS	jako
základní	základní	k2eAgNnSc4d1	základní
portfolio	portfolio	k1gNnSc4	portfolio
do	do	k7c2	do
investičního	investiční	k2eAgInSc2d1	investiční
fondu	fond	k1gInSc2	fond
ING	ing	kA	ing
Property	Propert	k1gInPc1	Propert
Fund	fund	k1gInSc1	fund
Central	Central	k1gMnSc5	Central
Europe	Europ	k1gMnSc5	Europ
<g/>
.	.	kIx.	.
</s>
