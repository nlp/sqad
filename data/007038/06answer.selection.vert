<s>
Teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
krystalická	krystalický	k2eAgFnSc1d1	krystalická
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
přechází	přecházet	k5eAaImIp3nS	přecházet
ze	z	k7c2	z
skupenství	skupenství	k1gNnSc2	skupenství
pevného	pevný	k2eAgNnSc2d1	pevné
do	do	k7c2	do
skupenství	skupenství	k1gNnSc2	skupenství
kapalného	kapalný	k2eAgNnSc2d1	kapalné
<g/>
.	.	kIx.	.
</s>
