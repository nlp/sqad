<s>
Teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
krystalická	krystalický	k2eAgFnSc1d1	krystalická
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
přechází	přecházet	k5eAaImIp3nS	přecházet
ze	z	k7c2	z
skupenství	skupenství	k1gNnSc2	skupenství
pevného	pevný	k2eAgNnSc2d1	pevné
do	do	k7c2	do
skupenství	skupenství	k1gNnSc2	skupenství
kapalného	kapalný	k2eAgNnSc2d1	kapalné
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
amorfních	amorfní	k2eAgFnPc2d1	amorfní
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
parafín	parafín	k1gInSc1	parafín
<g/>
)	)	kIx)	)
nelze	lze	k6eNd1	lze
tuto	tento	k3xDgFnSc4	tento
hranici	hranice	k1gFnSc4	hranice
určit	určit	k5eAaPmF	určit
přesně	přesně	k6eAd1	přesně
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
<g/>
:	:	kIx,	:
tt	tt	k?	tt
Základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
kelvin	kelvin	k1gInSc1	kelvin
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
K	k	k7c3	k
Další	další	k2eAgMnSc1d1	další
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
:	:	kIx,	:
viz	vidět	k5eAaImRp2nS	vidět
teplota	teplota	k1gFnSc1	teplota
Tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
teploty	teplota	k1gFnPc1	teplota
tání	tání	k1gNnSc2	tání
vybraných	vybraný	k2eAgFnPc2d1	vybraná
látek	látka	k1gFnPc2	látka
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
a	a	k8xC	a
v	v	k7c6	v
kelvinech	kelvin	k1gInPc6	kelvin
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgFnPc1	některý
sloučeniny	sloučenina	k1gFnPc1	sloučenina
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
kapalina	kapalina	k1gFnSc1	kapalina
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
pevnou	pevný	k2eAgFnSc4d1	pevná
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krystalické	krystalický	k2eAgFnSc2d1	krystalická
látky	látka	k1gFnSc2	látka
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
velikost	velikost	k1gFnSc1	velikost
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
čisté	čistý	k2eAgFnSc2d1	čistá
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
tuhnutí	tuhnutí	k1gNnPc2	tuhnutí
označuje	označovat	k5eAaImIp3nS	označovat
běžným	běžný	k2eAgInSc7d1	běžný
termínem	termín	k1gInSc7	termín
bod	bod	k1gInSc1	bod
tuhnutí	tuhnutí	k1gNnSc1	tuhnutí
či	či	k8xC	či
bod	bod	k1gInSc1	bod
mrazu	mráz	k1gInSc2	mráz
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
voda	voda	k1gFnSc1	voda
mrzne	mrznout	k5eAaImIp3nS	mrznout
či	či	k8xC	či
zamrzává	zamrzávat	k5eAaImIp3nS	zamrzávat
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
činí	činit	k5eAaImIp3nS	činit
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
atmosférických	atmosférický	k2eAgFnPc2d1	atmosférická
podmínek	podmínka	k1gFnPc2	podmínka
přibližně	přibližně	k6eAd1	přibližně
nula	nula	k1gFnSc1	nula
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tání	tání	k1gNnSc1	tání
Tuhnutí	tuhnutí	k1gNnPc2	tuhnutí
Teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
</s>
