<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
1948	[number]	k4	1948
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vojenský	vojenský	k2eAgMnSc1d1	vojenský
pilot	pilot	k1gMnSc1	pilot
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
československý	československý	k2eAgMnSc1d1	československý
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
<g/>
,	,	kIx,	,
Hrdina	Hrdina	k1gMnSc1	Hrdina
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Hrdina	Hrdina	k1gMnSc1	Hrdina
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
87	[number]	k4	87
<g/>
.	.	kIx.	.
kosmonautem	kosmonaut	k1gMnSc7	kosmonaut
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc6	první
z	z	k7c2	z
jiné	jiná	k1gFnSc2	jiná
země	zem	k1gFnSc2	zem
než	než	k8xS	než
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
nebo	nebo	k8xC	nebo
USA	USA	kA	USA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
úspěchem	úspěch	k1gInSc7	úspěch
československé	československý	k2eAgFnSc2d1	Československá
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
hlásí	hlásit	k5eAaImIp3nS	hlásit
jako	jako	k8xS	jako
k	k	k7c3	k
"	"	kIx"	"
<g/>
prvnímu	první	k4xOgNnSc3	první
evropskému	evropský	k2eAgMnSc3d1	evropský
kosmonautovi	kosmonaut	k1gMnSc3	kosmonaut
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kosmu	kosmos	k1gInSc2	kosmos
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
letěl	letět	k5eAaImAgMnS	letět
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Interkosmos	Interkosmosa	k1gFnPc2	Interkosmosa
<g/>
,	,	kIx,	,
když	když	k8xS	když
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
členským	členský	k2eAgMnPc3d1	členský
státům	stát	k1gInPc3	stát
programu	program	k1gInSc2	program
návštěvu	návštěva	k1gFnSc4	návštěva
jejich	jejich	k3xOp3gMnSc2	jejich
kosmonauta	kosmonaut	k1gMnSc2	kosmonaut
na	na	k7c6	na
sovětské	sovětský	k2eAgFnSc6d1	sovětská
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
stanici	stanice	k1gFnSc6	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
letu	let	k1gInSc3	let
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
Saljut	Saljut	k1gInSc1	Saljut
6	[number]	k4	6
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
společně	společně	k6eAd1	společně
se	s	k7c7	s
sovětským	sovětský	k2eAgMnSc7d1	sovětský
kosmonautem	kosmonaut	k1gMnSc7	kosmonaut
Alexejem	Alexej	k1gMnSc7	Alexej
Gubarevem	Gubarev	k1gInSc7	Gubarev
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1978	[number]	k4	1978
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
28	[number]	k4	28
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
prožil	prožít	k5eAaPmAgMnS	prožít
7	[number]	k4	7
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
17	[number]	k4	17
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
sloužil	sloužit	k5eAaImAgMnS	sloužit
ve	v	k7c6	v
vojenském	vojenský	k2eAgNnSc6d1	vojenské
letectvu	letectvo	k1gNnSc6	letectvo
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
jako	jako	k8xC	jako
ředitel	ředitel	k1gMnSc1	ředitel
Vojenského	vojenský	k2eAgNnSc2d1	vojenské
muzea	muzeum	k1gNnSc2	muzeum
letectví	letectví	k1gNnSc2	letectví
a	a	k8xC	a
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
v	v	k7c6	v
Praze-Kbelích	Praze-Kbel	k1gFnPc6	Praze-Kbel
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
obchodním	obchodní	k2eAgMnSc7d1	obchodní
zástupcem	zástupce	k1gMnSc7	zástupce
ČZ	ČZ	kA	ČZ
Strakonice	Strakonice	k1gFnPc1	Strakonice
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
obchodním	obchodní	k2eAgMnSc7d1	obchodní
radou	rada	k1gMnSc7	rada
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
až	až	k9	až
2013	[number]	k4	2013
poslancem	poslanec	k1gMnSc7	poslanec
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
za	za	k7c4	za
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
až	až	k9	až
2018	[number]	k4	2018
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc1	dětství
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
Jozef	Jozef	k1gMnSc1	Jozef
Remek	Remek	k6eAd1	Remek
byl	být	k5eAaImAgMnS	být
Slovák	Slovák	k1gMnSc1	Slovák
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
pilot	pilot	k1gMnSc1	pilot
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
generálporučíka	generálporučík	k1gMnSc4	generálporučík
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
velitel	velitel	k1gMnSc1	velitel
letectva	letectvo	k1gNnSc2	letectvo
Československé	československý	k2eAgFnSc2d1	Československá
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
Češka	Češka	k1gFnSc1	Češka
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
dětství	dětství	k1gNnSc4	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vychodil	vychodit	k5eAaPmAgMnS	vychodit
základní	základní	k2eAgFnSc4d1	základní
devítiletou	devítiletý	k2eAgFnSc4d1	devítiletá
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
babičce	babička	k1gFnSc3	babička
často	často	k6eAd1	často
vracel	vracet	k5eAaImAgInS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
také	také	k9	také
do	do	k7c2	do
Klubu	klub	k1gInSc2	klub
mladých	mladý	k2eAgMnPc2d1	mladý
astronomů	astronom	k1gMnPc2	astronom
při	při	k7c6	při
hvězdárně	hvězdárna	k1gFnSc6	hvězdárna
na	na	k7c6	na
Kraví	kraví	k2eAgFnSc6d1	kraví
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
povolání	povolání	k1gNnSc1	povolání
otce	otec	k1gMnSc2	otec
rodinu	rodina	k1gFnSc4	rodina
přivedlo	přivést	k5eAaPmAgNnS	přivést
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
bydliště	bydliště	k1gNnSc2	bydliště
<g/>
,	,	kIx,	,
Čáslavi	Čáslav	k1gFnSc2	Čáslav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
letecká	letecký	k2eAgFnSc1d1	letecká
základna	základna	k1gFnSc1	základna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvanáctiletce	dvanáctiletka	k1gFnSc6	dvanáctiletka
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
studium	studium	k1gNnSc4	studium
zakončil	zakončit	k5eAaPmAgMnS	zakončit
maturitou	maturita	k1gFnSc7	maturita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
<g/>
Má	mít	k5eAaImIp3nS	mít
sestry	sestra	k1gFnPc4	sestra
Jitku	Jitka	k1gFnSc4	Jitka
a	a	k8xC	a
Danu	Dana	k1gFnSc4	Dana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podruhé	podruhé	k6eAd1	podruhé
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
z	z	k7c2	z
každého	každý	k3xTgMnSc2	každý
manželství	manželství	k1gNnPc2	manželství
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
plynule	plynule	k6eAd1	plynule
rusky	rusky	k6eAd1	rusky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vojenský	vojenský	k2eAgMnSc1d1	vojenský
pilot	pilot	k1gMnSc1	pilot
==	==	k?	==
</s>
</p>
<p>
<s>
Remek	Remek	k6eAd1	Remek
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
Vyšším	vysoký	k2eAgNnSc6d2	vyšší
leteckém	letecký	k2eAgNnSc6d1	letecké
učilišti	učiliště	k1gNnSc6	učiliště
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
létal	létat	k5eAaImAgInS	létat
na	na	k7c6	na
cvičných	cvičný	k2eAgInPc6d1	cvičný
proudových	proudový	k2eAgInPc6d1	proudový
letounech	letoun	k1gInPc6	letoun
L-29	L-29	k1gMnSc1	L-29
Delfín	Delfín	k1gMnSc1	Delfín
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
školy	škola	k1gFnSc2	škola
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
leteckému	letecký	k2eAgInSc3d1	letecký
útvaru	útvar	k1gInSc3	útvar
do	do	k7c2	do
Žatce	Žatec	k1gInSc2	Žatec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
byl	být	k5eAaImAgMnS	být
přeložen	přeložit	k5eAaPmNgMnS	přeložit
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Zvolenskému	zvolenský	k2eAgInSc3d1	zvolenský
leteckému	letecký	k2eAgInSc3d1	letecký
stíhacímu	stíhací	k2eAgInSc3d1	stíhací
pluku	pluk	k1gInSc3	pluk
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
vyzbrojenému	vyzbrojený	k2eAgNnSc3d1	vyzbrojené
MIGy	mig	k1gInPc4	mig
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
vysoce	vysoce	k6eAd1	vysoce
postavený	postavený	k2eAgMnSc1d1	postavený
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
však	však	k9	však
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
protekci	protekce	k1gFnSc4	protekce
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
;	;	kIx,	;
otci	otec	k1gMnSc3	otec
měl	mít	k5eAaImAgInS	mít
za	za	k7c2	za
zlé	zlá	k1gFnSc2	zlá
rozchod	rozchod	k1gInSc4	rozchod
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
narážky	narážka	k1gFnPc4	narážka
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
mu	on	k3xPp3gMnSc3	on
vadily	vadit	k5eAaImAgFnP	vadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
zahájil	zahájit	k5eAaPmAgInS	zahájit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
Vojenské	vojenský	k2eAgFnSc6d1	vojenská
letecké	letecký	k2eAgFnSc6d1	letecká
akademii	akademie	k1gFnSc6	akademie
J.	J.	kA	J.
A.	A.	kA	A.
Gagarina	Gagarina	k1gMnSc1	Gagarina
v	v	k7c6	v
Moninu	Monin	k1gInSc6	Monin
u	u	k7c2	u
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
dostal	dostat	k5eAaPmAgMnS	dostat
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
Za	za	k7c4	za
službu	služba	k1gFnSc4	služba
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moninu	Monin	k1gInSc6	Monin
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
výbornou	výborná	k1gFnSc4	výborná
<g/>
,	,	kIx,	,
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
vysvědčení	vysvědčení	k1gNnSc6	vysvědčení
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
čtyřku	čtyřka	k1gFnSc4	čtyřka
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
známkování	známkování	k1gNnSc6	známkování
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
české	český	k2eAgFnSc3d1	Česká
dvojce	dvojka	k1gFnSc3	dvojka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
akademie	akademie	k1gFnSc2	akademie
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
vrátil	vrátit	k5eAaPmAgInS	vrátit
nakrátko	nakrátko	k6eAd1	nakrátko
k	k	k7c3	k
mateřskému	mateřský	k2eAgInSc3d1	mateřský
útvaru	útvar	k1gInSc3	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
zástupcem	zástupce	k1gMnSc7	zástupce
velitele	velitel	k1gMnSc2	velitel
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přišel	přijít	k5eAaPmAgInS	přijít
neočekávaný	očekávaný	k2eNgInSc1d1	neočekávaný
obrat	obrat	k1gInSc1	obrat
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
–	–	k?	–
výběr	výběr	k1gInSc4	výběr
kandidátů	kandidát	k1gMnPc2	kandidát
pro	pro	k7c4	pro
kosmický	kosmický	k2eAgInSc4d1	kosmický
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
zemím	zem	k1gFnPc3	zem
sdruženým	sdružený	k2eAgFnPc3d1	sdružená
v	v	k7c6	v
programu	program	k1gInSc6	program
výzkumu	výzkum	k1gInSc2	výzkum
vesmíru	vesmír	k1gInSc2	vesmír
Interkosmos	Interkosmos	k1gMnSc1	Interkosmos
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
pilotovaných	pilotovaný	k2eAgNnPc6d1	pilotované
kosmických	kosmický	k2eAgNnPc6d1	kosmické
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
probíhal	probíhat	k5eAaImAgInS	probíhat
výběr	výběr	k1gInSc1	výběr
od	od	k7c2	od
června	červen	k1gInSc2	červen
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pilotů	pilot	k1gMnPc2	pilot
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
každoročních	každoroční	k2eAgFnPc2d1	každoroční
prohlídek	prohlídka	k1gFnPc2	prohlídka
vybráno	vybrat	k5eAaPmNgNnS	vybrat
24	[number]	k4	24
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Remka	Remek	k1gInSc2	Remek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalších	další	k2eAgInPc6d1	další
testech	test	k1gInPc6	test
měl	mít	k5eAaImAgInS	mít
výborné	výborný	k2eAgInPc4d1	výborný
výsledky	výsledek	k1gInPc4	výsledek
–	–	k?	–
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
pod	pod	k7c7	pod
časovým	časový	k2eAgInSc7d1	časový
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
při	při	k7c6	při
zátěži	zátěž	k1gFnSc6	zátěž
<g/>
,	,	kIx,	,
při	při	k7c6	při
testech	test	k1gInPc6	test
orgánu	orgán	k1gInSc2	orgán
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
,	,	kIx,	,
trpěl	trpět	k5eAaImAgMnS	trpět
však	však	k9	však
rýmou	rýma	k1gFnSc7	rýma
a	a	k8xC	a
nadváhou	nadváha	k1gFnSc7	nadváha
–	–	k?	–
musel	muset	k5eAaImAgInS	muset
shodit	shodit	k5eAaPmF	shodit
12	[number]	k4	12
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
kandidátů	kandidát	k1gMnPc2	kandidát
snížil	snížit	k5eAaPmAgInS	snížit
na	na	k7c4	na
8	[number]	k4	8
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
–	–	k?	–
Ladislava	Ladislav	k1gMnSc4	Ladislav
Klímu	Klíma	k1gMnSc4	Klíma
<g/>
,	,	kIx,	,
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Pelčáka	Pelčák	k1gMnSc2	Pelčák
<g/>
,	,	kIx,	,
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Remka	Remek	k1gMnSc2	Remek
a	a	k8xC	a
Michala	Michal	k1gMnSc2	Michal
Vondrouška	Vondroušek	k1gMnSc2	Vondroušek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
kole	kolo	k1gNnSc6	kolo
testů	test	k1gInPc2	test
už	už	k6eAd1	už
ve	v	k7c6	v
Středisku	středisko	k1gNnSc6	středisko
přípravy	příprava	k1gFnSc2	příprava
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
J.	J.	kA	J.
A.	A.	kA	A.
Gagarina	Gagarina	k1gMnSc1	Gagarina
v	v	k7c6	v
Hvězdném	hvězdný	k2eAgNnSc6d1	Hvězdné
městečku	městečko	k1gNnSc6	městečko
zůstali	zůstat	k5eAaPmAgMnP	zůstat
Remek	Remek	k1gMnSc1	Remek
a	a	k8xC	a
Pelčák	Pelčák	k1gMnSc1	Pelčák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1976	[number]	k4	1976
byl	být	k5eAaImAgMnS	být
Remek	Remek	k1gMnSc1	Remek
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
kapitána	kapitán	k1gMnSc4	kapitán
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1976	[number]	k4	1976
se	se	k3xPyFc4	se
v	v	k7c6	v
Hvězdném	hvězdný	k2eAgNnSc6d1	Hvězdné
městečku	městečko	k1gNnSc6	městečko
s	s	k7c7	s
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Pelčákem	Pelčák	k1gMnSc7	Pelčák
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
kandidáty	kandidát	k1gMnPc7	kandidát
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
NDR	NDR	kA	NDR
začal	začít	k5eAaPmAgInS	začít
připravovat	připravovat	k5eAaImF	připravovat
ke	k	k7c3	k
kosmickému	kosmický	k2eAgInSc3d1	kosmický
letu	let	k1gInSc3	let
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1977	[number]	k4	1977
byly	být	k5eAaImAgFnP	být
sestaveny	sestavit	k5eAaPmNgFnP	sestavit
posádky	posádka	k1gFnPc1	posádka
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInSc4d1	budoucí
československo-sovětský	československoovětský	k2eAgInSc4d1	československo-sovětský
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
posádku	posádka	k1gFnSc4	posádka
tvořili	tvořit	k5eAaImAgMnP	tvořit
sovětský	sovětský	k2eAgMnSc1d1	sovětský
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
plukovník	plukovník	k1gMnSc1	plukovník
Alexej	Alexej	k1gMnSc1	Alexej
Gubarev	Gubarev	k1gFnSc1	Gubarev
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
již	již	k6eAd1	již
měl	mít	k5eAaImAgMnS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
let	let	k1gInSc4	let
Sojuzem	Sojuz	k1gInSc7	Sojuz
17	[number]	k4	17
a	a	k8xC	a
měsíční	měsíční	k2eAgInSc4d1	měsíční
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
Saljut	Saljut	k1gInSc1	Saljut
4	[number]	k4	4
<g/>
,	,	kIx,	,
a	a	k8xC	a
kapitán	kapitán	k1gMnSc1	kapitán
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
kosmonaut-výzkumník	kosmonautýzkumník	k1gMnSc1	kosmonaut-výzkumník
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Rukavišnikov	Rukavišnikov	k1gInSc1	Rukavišnikov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
dva	dva	k4xCgInPc4	dva
lety	let	k1gInPc4	let
(	(	kIx(	(
<g/>
Sojuz	Sojuz	k1gInSc1	Sojuz
10	[number]	k4	10
a	a	k8xC	a
Sojuz	Sojuz	k1gInSc1	Sojuz
16	[number]	k4	16
<g/>
)	)	kIx)	)
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
8	[number]	k4	8
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
Oldřich	Oldřich	k1gMnSc1	Oldřich
Pelčák	Pelčák	k1gMnSc1	Pelčák
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
Gubarev	Gubarev	k1gFnSc4	Gubarev
s	s	k7c7	s
Remkem	Remek	k1gMnSc7	Remek
odstartovali	odstartovat	k5eAaPmAgMnP	odstartovat
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Bajkonur	Bajkonura	k1gFnPc2	Bajkonura
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1978	[number]	k4	1978
v	v	k7c4	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
28	[number]	k4	28
UTC	UTC	kA	UTC
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
posádka	posádka	k1gFnSc1	posádka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Interkosmos	Interkosmos	k1gMnSc1	Interkosmos
i	i	k9	i
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
kosmických	kosmický	k2eAgInPc2d1	kosmický
letů	let	k1gInPc2	let
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stanicí	stanice	k1gFnSc7	stanice
Saljut	Saljut	k1gInSc4	Saljut
6	[number]	k4	6
se	se	k3xPyFc4	se
Sojuz	Sojuz	k1gInSc1	Sojuz
28	[number]	k4	28
spojil	spojit	k5eAaPmAgInS	spojit
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
UTC	UTC	kA	UTC
<g/>
,	,	kIx,	,
po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
se	se	k3xPyFc4	se
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
přivítali	přivítat	k5eAaPmAgMnP	přivítat
se	s	k7c7	s
členy	člen	k1gMnPc7	člen
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
posádky	posádka	k1gFnSc2	posádka
stanice	stanice	k1gFnSc2	stanice
Jurijem	Jurij	k1gMnSc7	Jurij
Romaněnkem	Romaněnek	k1gMnSc7	Romaněnek
a	a	k8xC	a
Georgijem	Georgij	k1gMnSc7	Georgij
Grečkem	Greček	k1gMnSc7	Greček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgInSc4d1	pracovní
den	den	k1gInSc4	den
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
trval	trvat	k5eAaImAgMnS	trvat
od	od	k7c2	od
6	[number]	k4	6
do	do	k7c2	do
21	[number]	k4	21
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
i	i	k9	i
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
cvičení	cvičení	k1gNnPc2	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
výcviku	výcvik	k1gInSc2	výcvik
Gubareva	Gubarevo	k1gNnSc2	Gubarevo
a	a	k8xC	a
Remka	Remko	k1gNnSc2	Remko
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
i	i	k8xC	i
ovládání	ovládání	k1gNnSc1	ovládání
zubní	zubní	k2eAgFnSc2d1	zubní
vrtačky	vrtačka	k1gFnSc2	vrtačka
<g/>
,	,	kIx,	,
Romaněnko	Romaněnka	k1gFnSc5	Romaněnka
a	a	k8xC	a
Grečko	Grečko	k1gNnSc1	Grečko
na	na	k7c6	na
Saljutu	Saljut	k1gInSc6	Saljut
6	[number]	k4	6
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
začali	začít	k5eAaPmAgMnP	začít
stěžovat	stěžovat	k5eAaImF	stěžovat
na	na	k7c4	na
bolesti	bolest	k1gFnPc4	bolest
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příletu	přílet	k1gInSc6	přílet
Gubarev	Gubarva	k1gFnPc2	Gubarva
s	s	k7c7	s
Remkem	Remko	k1gNnSc7	Remko
sdělili	sdělit	k5eAaPmAgMnP	sdělit
kolegům	kolega	k1gMnPc3	kolega
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
zuby	zub	k1gInPc1	zub
ošetří	ošetřit	k5eAaPmIp3nP	ošetřit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ošetřování	ošetřování	k1gNnSc1	ošetřování
zubů	zub	k1gInPc2	zub
začali	začít	k5eAaPmAgMnP	začít
učit	učit	k5eAaImF	učit
před	před	k7c7	před
dvěma	dva	k4xCgInPc7	dva
týdny	týden	k1gInPc7	týden
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
odmítnuti	odmítnout	k5eAaPmNgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
stížnosti	stížnost	k1gFnPc1	stížnost
na	na	k7c4	na
zuby	zub	k1gInPc4	zub
řídící	řídící	k2eAgNnSc4d1	řídící
středisko	středisko	k1gNnSc4	středisko
už	už	k6eAd1	už
neslyšelo	slyšet	k5eNaImAgNnS	slyšet
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
několik	několik	k4yIc4	několik
televizních	televizní	k2eAgFnPc2d1	televizní
reportáží	reportáž	k1gFnPc2	reportáž
<g/>
,	,	kIx,	,
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
stálé	stálý	k2eAgFnSc3d1	stálá
posádce	posádka	k1gFnSc3	posádka
a	a	k8xC	a
věnovali	věnovat	k5eAaPmAgMnP	věnovat
se	se	k3xPyFc4	se
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
vědeckému	vědecký	k2eAgInSc3d1	vědecký
programu	program	k1gInSc3	program
<g/>
.	.	kIx.	.
</s>
<s>
Experimenty	experiment	k1gInPc4	experiment
připravené	připravený	k2eAgInPc4d1	připravený
československými	československý	k2eAgMnPc7d1	československý
vědci	vědec	k1gMnSc6	vědec
byly	být	k5eAaImAgFnP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Chlorella-	Chlorella-	k?	Chlorella-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
zkoumající	zkoumající	k2eAgNnSc1d1	zkoumající
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
řas	řasa	k1gFnPc2	řasa
Chlorella	Chlorello	k1gNnSc2	Chlorello
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tepelná	tepelný	k2eAgFnSc1d1	tepelná
výměna-	výměna-	k?	výměna-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
srovnávající	srovnávající	k2eAgFnSc4d1	srovnávající
naměřenou	naměřený	k2eAgFnSc4d1	naměřená
teplotu	teplota	k1gFnSc4	teplota
těla	tělo	k1gNnSc2	tělo
s	s	k7c7	s
pocity	pocit	k1gInPc7	pocit
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxymetr	Oxymetr	k1gInSc1	Oxymetr
<g/>
,	,	kIx,	,
měření	měření	k1gNnSc1	měření
okysličení	okysličení	k1gNnSc2	okysličení
tkání	tkáň	k1gFnPc2	tkáň
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
prací	práce	k1gFnPc2	práce
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
zjistili	zjistit	k5eAaPmAgMnP	zjistit
poruchu	porucha	k1gFnSc4	porucha
napájení	napájení	k1gNnSc2	napájení
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
sice	sice	k8xC	sice
improvizovaně	improvizovaně	k6eAd1	improvizovaně
nahradili	nahradit	k5eAaPmAgMnP	nahradit
sadou	sada	k1gFnSc7	sada
monočlánků	monočlánek	k1gInPc2	monočlánek
<g/>
,	,	kIx,	,
měření	měření	k1gNnSc2	měření
však	však	k9	však
už	už	k6eAd1	už
neproběhlo	proběhnout	k5eNaPmAgNnS	proběhnout
kompletně	kompletně	k6eAd1	kompletně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Supos	Supos	k1gInSc1	Supos
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc2d1	spočívající
ve	v	k7c4	v
zjišťování	zjišťování	k1gNnSc4	zjišťování
psychologického	psychologický	k2eAgInSc2d1	psychologický
stavu	stav	k1gInSc2	stav
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
před	před	k7c7	před
<g/>
,	,	kIx,	,
během	běh	k1gInSc7	běh
a	a	k8xC	a
po	po	k7c6	po
letu	let	k1gInSc6	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Extinkce	extinkce	k1gFnSc1	extinkce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Remek	Remek	k1gMnSc1	Remek
sledoval	sledovat	k5eAaImAgMnS	sledovat
změny	změna	k1gFnPc4	změna
jasnosti	jasnost	k1gFnSc2	jasnost
hvězd	hvězda	k1gFnPc2	hvězda
při	při	k7c6	při
západu	západ	k1gInSc6	západ
za	za	k7c4	za
obzor	obzor	k1gInSc4	obzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Morava-splav	Moravaplat	k5eAaPmDgInS	Morava-splat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
využíval	využívat	k5eAaPmAgInS	využívat
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
pec	pec	k1gFnSc4	pec
Splav-	Splav-	k1gFnSc2	Splav-
<g/>
1	[number]	k4	1
k	k	k7c3	k
roztavení	roztavení	k1gNnSc3	roztavení
a	a	k8xC	a
pomalému	pomalý	k2eAgMnSc3d1	pomalý
(	(	kIx(	(
<g/>
45	[number]	k4	45
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
chladnutí	chladnutí	k1gNnSc2	chladnutí
vzorků	vzorek	k1gInPc2	vzorek
chloridu	chlorid	k1gInSc2	chlorid
měďného	měďný	k2eAgInSc2d1	měďný
a	a	k8xC	a
olovnatého	olovnatý	k2eAgInSc2d1	olovnatý
a	a	k8xC	a
chloridu	chlorid	k1gInSc2	chlorid
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Remek	Remek	k1gInSc1	Remek
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
většině	většina	k1gFnSc3	většina
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
letu	let	k1gInSc2	let
trpěli	trpět	k5eAaImAgMnP	trpět
nevolností	nevolnost	k1gFnSc7	nevolnost
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
nezvykem	nezvyk	k1gInSc7	nezvyk
organizmu	organizmus	k1gInSc2	organizmus
na	na	k7c4	na
stav	stav	k1gInSc4	stav
beztíže	beztíže	k1gFnSc2	beztíže
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečný	výjimečný	k2eAgMnSc1d1	výjimečný
ovšem	ovšem	k9	ovšem
byl	být	k5eAaImAgInS	být
otevřeným	otevřený	k2eAgInSc7d1	otevřený
popisem	popis	k1gInSc7	popis
svých	svůj	k3xOyFgFnPc2	svůj
obtíží	obtíž	k1gFnPc2	obtíž
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
sovětští	sovětský	k2eAgMnPc1d1	sovětský
kolegové	kolega	k1gMnPc1	kolega
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
obávali	obávat	k5eAaImAgMnP	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přiznáním	přiznání	k1gNnSc7	přiznání
potíží	potíž	k1gFnPc2	potíž
přijdou	přijít	k5eAaPmIp3nP	přijít
o	o	k7c4	o
šanci	šance	k1gFnSc4	šance
znova	znova	k6eAd1	znova
letět	letět	k5eAaImF	letět
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
den	den	k1gInSc1	den
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
ve	v	k7c6	v
14	[number]	k4	14
a	a	k8xC	a
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
tradiční	tradiční	k2eAgFnSc1d1	tradiční
tisková	tiskový	k2eAgFnSc1d1	tisková
konference	konference	k1gFnSc1	konference
s	s	k7c7	s
kosmonauty	kosmonaut	k1gMnPc7	kosmonaut
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
vysílaná	vysílaný	k2eAgFnSc1d1	vysílaná
československou	československý	k2eAgFnSc7d1	Československá
televizí	televize	k1gFnSc7	televize
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
přenosu	přenos	k1gInSc6	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Remka	Remek	k1gMnSc4	Remek
nedopadla	dopadnout	k5eNaPmAgFnS	dopadnout
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
trpěl	trpět	k5eAaImAgMnS	trpět
nevolností	nevolnost	k1gFnSc7	nevolnost
a	a	k8xC	a
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
roce	rok	k1gInSc6	rok
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
prostředí	prostředí	k1gNnSc6	prostředí
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
rozhovor	rozhovor	k1gInSc4	rozhovor
o	o	k7c6	o
letu	let	k1gInSc6	let
a	a	k8xC	a
prováděných	prováděný	k2eAgInPc6d1	prováděný
experimentech	experiment	k1gInPc6	experiment
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
obtížný	obtížný	k2eAgInSc1d1	obtížný
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
hledal	hledat	k5eAaImAgInS	hledat
správná	správný	k2eAgNnPc4d1	správné
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
programu	program	k1gInSc2	program
letu	let	k1gInSc2	let
se	s	k7c7	s
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Gubarev	Gubarev	k1gFnSc1	Gubarev
s	s	k7c7	s
Remkem	Remko	k1gNnSc7	Remko
rozloučili	rozloučit	k5eAaPmAgMnP	rozloučit
s	s	k7c7	s
hostiteli	hostitel	k1gMnPc7	hostitel
<g/>
,	,	kIx,	,
v	v	k7c6	v
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
UTC	UTC	kA	UTC
oddělili	oddělit	k5eAaPmAgMnP	oddělit
se	s	k7c7	s
Sojuzem	Sojuz	k1gInSc7	Sojuz
28	[number]	k4	28
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
v	v	k7c6	v
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
UTC	UTC	kA	UTC
přistáli	přistát	k5eAaImAgMnP	přistát
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
<g/>
,	,	kIx,	,
asi	asi	k9	asi
135	[number]	k4	135
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Arkalyku	Arkalyk	k1gInSc2	Arkalyk
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
Remkova	Remkův	k2eAgInSc2d1	Remkův
letu	let	k1gInSc2	let
činila	činit	k5eAaImAgFnS	činit
190	[number]	k4	190
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
18	[number]	k4	18
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
<g/>
Důvody	důvod	k1gInPc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byla	být	k5eAaImAgFnS	být
vybrána	vybrán	k2eAgFnSc1d1	vybrána
posádka	posádka	k1gFnSc1	posádka
Gubarev-Remek	Gubarev-Remka	k1gFnPc2	Gubarev-Remka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Rukavišnikov	Rukavišnikov	k1gInSc1	Rukavišnikov
a	a	k8xC	a
Oldřich	Oldřich	k1gMnSc1	Oldřich
Pelčák	Pelčák	k1gMnSc1	Pelčák
zůstali	zůstat	k5eAaPmAgMnP	zůstat
jako	jako	k9	jako
náhradníci	náhradník	k1gMnPc1	náhradník
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
předmětem	předmět	k1gInSc7	předmět
diskusí	diskuse	k1gFnPc2	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Tomáše	Tomáš	k1gMnSc2	Tomáš
Přibyla	Přibyl	k1gMnSc2	Přibyl
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ležely	ležet	k5eAaImAgFnP	ležet
v	v	k7c6	v
soupeření	soupeření	k1gNnSc6	soupeření
mezi	mezi	k7c7	mezi
zainteresovanými	zainteresovaný	k2eAgFnPc7d1	zainteresovaná
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
institucemi	instituce	k1gFnPc7	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
letu	let	k1gInSc2	let
Sojuzu	Sojuz	k1gInSc2	Sojuz
25	[number]	k4	25
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1977	[number]	k4	1977
byly	být	k5eAaImAgFnP	být
posádky	posádka	k1gFnPc1	posádka
sestavovány	sestavovat	k5eAaImNgFnP	sestavovat
vždy	vždy	k6eAd1	vždy
s	s	k7c7	s
nejméně	málo	k6eAd3	málo
jedním	jeden	k4xCgMnSc7	jeden
kosmonautem	kosmonaut	k1gMnSc7	kosmonaut
se	se	k3xPyFc4	se
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
posádek	posádka	k1gFnPc2	posádka
to	ten	k3xDgNnSc1	ten
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
sovětský	sovětský	k2eAgMnSc1d1	sovětský
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
zkušených	zkušený	k2eAgMnPc2d1	zkušený
vojenských	vojenský	k2eAgMnPc2d1	vojenský
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
z	z	k7c2	z
oddílu	oddíl	k1gInSc2	oddíl
CPK	CPK	kA	CPK
přineslo	přinést	k5eAaPmAgNnS	přinést
nutnost	nutnost	k1gFnSc4	nutnost
zařadit	zařadit	k5eAaPmF	zařadit
na	na	k7c4	na
velitelskou	velitelský	k2eAgFnSc4d1	velitelská
pozici	pozice	k1gFnSc4	pozice
také	také	k6eAd1	také
kosmonauty	kosmonaut	k1gMnPc7	kosmonaut
–	–	k?	–
inženýry	inženýr	k1gMnPc4	inženýr
z	z	k7c2	z
Eněrgije	Eněrgije	k1gFnSc2	Eněrgije
<g/>
.	.	kIx.	.
</s>
<s>
Prolomení	prolomení	k1gNnPc1	prolomení
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
léta	léto	k1gNnPc1	léto
platícího	platící	k2eAgNnSc2d1	platící
pravidla	pravidlo	k1gNnSc2	pravidlo
"	"	kIx"	"
<g/>
velitel	velitel	k1gMnSc1	velitel
je	být	k5eAaImIp3nS	být
voják	voják	k1gMnSc1	voják
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
pochopitelně	pochopitelně	k6eAd1	pochopitelně
zástupci	zástupce	k1gMnPc1	zástupce
CPK	CPK	kA	CPK
<g/>
,	,	kIx,	,
a	a	k8xC	a
vojáci	voják	k1gMnPc1	voják
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
bránili	bránit	k5eAaImAgMnP	bránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
měla	mít	k5eAaImAgFnS	mít
Gubarevova	Gubarevův	k2eAgFnSc1d1	Gubarevův
posádka	posádka	k1gFnSc1	posádka
výhodu	výhod	k1gInSc2	výhod
proti	proti	k7c3	proti
Rukavišnikovi	Rukavišnik	k1gMnSc3	Rukavišnik
s	s	k7c7	s
Pelčákem	Pelčák	k1gMnSc7	Pelčák
prostě	prostě	k9	prostě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rukavišnikov	Rukavišnikov	k1gInSc4	Rukavišnikov
nebyl	být	k5eNaImAgMnS	být
voják	voják	k1gMnSc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
u	u	k7c2	u
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
zkoušek	zkouška	k1gFnPc2	zkouška
(	(	kIx(	(
<g/>
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
byla	být	k5eAaImAgFnS	být
dvojice	dvojice	k1gFnSc1	dvojice
Gubarev	Gubarev	k1gFnSc1	Gubarev
<g/>
–	–	k?	–
<g/>
Remek	Remek	k6eAd1	Remek
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
jako	jako	k9	jako
o	o	k7c4	o
něco	něco	k3yInSc4	něco
lepší	dobrý	k2eAgNnSc1d2	lepší
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
zákulisní	zákulisní	k2eAgFnSc1d1	zákulisní
rivalita	rivalita	k1gFnSc1	rivalita
<g/>
,	,	kIx,	,
když	když	k8xS	když
obě	dva	k4xCgFnPc1	dva
soupeřící	soupeřící	k2eAgFnPc1d1	soupeřící
skupiny	skupina	k1gFnPc1	skupina
nařkly	nařknout	k5eAaPmAgFnP	nařknout
protistranu	protistrana	k1gFnSc4	protistrana
<g/>
,	,	kIx,	,
že	že	k8xS	že
napovídala	napovídat	k5eAaBmAgFnS	napovídat
"	"	kIx"	"
<g/>
své	své	k1gNnSc1	své
<g/>
"	"	kIx"	"
posádce	posádka	k1gFnSc3	posádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
rok	rok	k1gInSc1	rok
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
Hlavní	hlavní	k2eAgFnSc6d1	hlavní
politické	politický	k2eAgFnSc6d1	politická
správě	správa	k1gFnSc6	správa
ČSLA	ČSLA	kA	ČSLA
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
pracovníkem	pracovník	k1gMnSc7	pracovník
ve	v	k7c6	v
Výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
ústavu	ústav	k1gInSc6	ústav
letectva	letectvo	k1gNnSc2	letectvo
v	v	k7c6	v
Praze-Kbelích	Praze-Kbel	k1gInPc6	Praze-Kbel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
výcviku	výcvik	k1gInSc6	výcvik
i	i	k8xC	i
samotném	samotný	k2eAgInSc6d1	samotný
letu	let	k1gInSc6	let
napsal	napsat	k5eAaPmAgMnS	napsat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Karla	Karel	k1gMnSc2	Karel
Richtera	Richter	k1gMnSc2	Richter
knihu	kniha	k1gFnSc4	kniha
Pod	pod	k7c4	pod
námi	my	k3xPp1nPc7	my
planeta	planeta	k1gFnSc1	planeta
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
knihou	kniha	k1gFnSc7	kniha
vydanou	vydaný	k2eAgFnSc7d1	vydaná
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
Splněné	splněný	k2eAgFnPc4d1	splněná
naděje	naděje	k1gFnPc4	naděje
<g/>
.	.	kIx.	.
<g/>
Coby	Coby	k?	Coby
známá	známý	k2eAgFnSc1d1	známá
osobnost	osobnost	k1gFnSc1	osobnost
působil	působit	k5eAaImAgInS	působit
ve	v	k7c6	v
veřejných	veřejný	k2eAgFnPc6d1	veřejná
organizacích	organizace	k1gFnPc6	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Světové	světový	k2eAgFnSc2d1	světová
rady	rada	k1gFnSc2	rada
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ÚV	ÚV	kA	ÚV
Svazu	svaz	k1gInSc2	svaz
československo-sovětského	československoovětský	k2eAgNnSc2d1	československo-sovětský
přátelství	přátelství	k1gNnSc2	přátelství
(	(	kIx(	(
<g/>
SČSP	SČSP	kA	SČSP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Společnosti	společnost	k1gFnSc2	společnost
přátel	přítel	k1gMnPc2	přítel
národů	národ	k1gInPc2	národ
východu	východ	k1gInSc2	východ
(	(	kIx(	(
<g/>
transformovaného	transformovaný	k2eAgInSc2d1	transformovaný
SČSP	SČSP	kA	SČSP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
zůstal	zůstat	k5eAaPmAgMnS	zůstat
i	i	k9	i
po	po	k7c6	po
přejmenování	přejmenování	k1gNnSc6	přejmenování
na	na	k7c4	na
Česko-ruskou	českouský	k2eAgFnSc4d1	česko-ruská
společnost	společnost	k1gFnSc4	společnost
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Vojenské	vojenský	k2eAgFnSc6d1	vojenská
akademii	akademie	k1gFnSc6	akademie
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
ji	on	k3xPp3gFnSc4	on
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
zástupce	zástupce	k1gMnSc2	zástupce
velitele	velitel	k1gMnSc2	velitel
2	[number]	k4	2
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
PVOS	PVOS	kA	PVOS
(	(	kIx(	(
<g/>
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
musel	muset	k5eAaImAgMnS	muset
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
členství	členství	k1gNnSc2	členství
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
byl	být	k5eAaImAgInS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Vojenského	vojenský	k2eAgNnSc2d1	vojenské
muzea	muzeum	k1gNnSc2	muzeum
letectví	letectví	k1gNnSc2	letectví
a	a	k8xC	a
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
v	v	k7c6	v
Praze-Kbelích	Praze-Kbel	k1gFnPc6	Praze-Kbel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1995	[number]	k4	1995
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
zálohy	záloha	k1gFnSc2	záloha
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
plukovníka	plukovník	k1gMnSc4	plukovník
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
obchodní	obchodní	k2eAgMnSc1d1	obchodní
zástupce	zástupce	k1gMnSc1	zástupce
ČZ	ČZ	kA	ČZ
Strakonice	Strakonice	k1gFnPc1	Strakonice
v	v	k7c6	v
Ruské	ruský	k2eAgFnSc6d1	ruská
federaci	federace	k1gFnSc6	federace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
využíval	využívat	k5eAaImAgInS	využívat
své	svůj	k3xOyFgInPc4	svůj
staré	starý	k2eAgInPc4d1	starý
kontakty	kontakt	k1gInPc4	kontakt
a	a	k8xC	a
prestiž	prestiž	k1gFnSc4	prestiž
kosmonauta	kosmonaut	k1gMnSc2	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
společného	společný	k2eAgInSc2d1	společný
podniku	podnik	k1gInSc2	podnik
CZ-Turbo-GAZ	CZ-Turbo-GAZ	k1gFnSc2	CZ-Turbo-GAZ
v	v	k7c6	v
Nižném	nižný	k2eAgInSc6d1	nižný
Novgorodu	Novgorod	k1gInSc6	Novgorod
a	a	k8xC	a
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
jako	jako	k8xC	jako
ředitel	ředitel	k1gMnSc1	ředitel
zastoupení	zastoupení	k1gNnSc2	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
až	až	k9	až
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
obchodním	obchodní	k2eAgMnSc7d1	obchodní
radou	rada	k1gMnSc7	rada
a	a	k8xC	a
vedoucím	vedoucí	k1gMnSc7	vedoucí
obchodně	obchodně	k6eAd1	obchodně
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
úseku	úsek	k1gInSc2	úsek
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
ČR	ČR	kA	ČR
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Evropský	evropský	k2eAgMnSc1d1	evropský
poslanec	poslanec	k1gMnSc1	poslanec
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
KSČM	KSČM	kA	KSČM
jako	jako	k8xS	jako
nestraník	nestraník	k1gMnSc1	nestraník
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
komunistickými	komunistický	k2eAgMnPc7d1	komunistický
poslanci	poslanec	k1gMnPc7	poslanec
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
frakce	frakce	k1gFnSc2	frakce
Evropské	evropský	k2eAgFnSc2d1	Evropská
spojené	spojený	k2eAgFnSc2d1	spojená
levice	levice	k1gFnSc2	levice
a	a	k8xC	a
Severské	severský	k2eAgFnPc1d1	severská
zelené	zelený	k2eAgFnPc1d1	zelená
levice	levice	k1gFnPc1	levice
(	(	kIx(	(
<g/>
EUL	EUL	kA	EUL
<g/>
/	/	kIx~	/
<g/>
NGL	NGL	kA	NGL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
parlamentního	parlamentní	k2eAgInSc2d1	parlamentní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
energetiku	energetika	k1gFnSc4	energetika
<g/>
,	,	kIx,	,
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
delegace	delegace	k1gFnSc1	delegace
EU-Rusko	EU-Rusko	k1gNnSc1	EU-Rusko
a	a	k8xC	a
náhradníkem	náhradník	k1gMnSc7	náhradník
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejaktivnějším	aktivní	k2eAgMnPc3d3	nejaktivnější
českým	český	k2eAgMnPc3d1	český
poslancům	poslanec	k1gMnPc3	poslanec
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
letectví	letectví	k1gNnSc2	letectví
a	a	k8xC	a
kosmonautice	kosmonautika	k1gFnSc6	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
oceňován	oceňovat	k5eAaImNgMnS	oceňovat
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
přehled	přehled	k1gInSc4	přehled
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
navigačního	navigační	k2eAgInSc2d1	navigační
systému	systém	k1gInSc2	systém
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
znalost	znalost	k1gFnSc4	znalost
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
2009	[number]	k4	2009
svůj	svůj	k3xOyFgInSc4	svůj
mandát	mandát	k1gInSc4	mandát
obhájil	obhájit	k5eAaPmAgMnS	obhájit
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
rozpočtového	rozpočtový	k2eAgInSc2d1	rozpočtový
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
náhradníkem	náhradník	k1gMnSc7	náhradník
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
energetiku	energetika	k1gFnSc4	energetika
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
parlamentní	parlamentní	k2eAgFnSc6d1	parlamentní
delegaci	delegace	k1gFnSc6	delegace
EU-Rusko	EU-Rusko	k1gNnSc1	EU-Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kanceláře	kancelář	k1gFnPc4	kancelář
měl	mít	k5eAaImAgMnS	mít
mj.	mj.	kA	mj.
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
a	a	k8xC	a
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2012	[number]	k4	2012
zvažovalo	zvažovat	k5eAaImAgNnS	zvažovat
vedení	vedení	k1gNnSc1	vedení
KSČM	KSČM	kA	KSČM
jeho	jeho	k3xOp3gFnSc4	jeho
kandidaturu	kandidatura	k1gFnSc4	kandidatura
v	v	k7c6	v
první	první	k4xOgFnSc6	první
přímé	přímý	k2eAgFnSc6d1	přímá
volbě	volba	k1gFnSc6	volba
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Remek	Remek	k6eAd1	Remek
kandidovat	kandidovat	k5eAaImF	kandidovat
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
ÚV	ÚV	kA	ÚV
KSČM	KSČM	kA	KSČM
poté	poté	k6eAd1	poté
nevybral	vybrat	k5eNaPmAgMnS	vybrat
jiného	jiný	k2eAgMnSc4d1	jiný
kandidáta	kandidát	k1gMnSc4	kandidát
"	"	kIx"	"
<g/>
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
netříštily	tříštit	k5eNaImAgFnP	tříštit
síly	síla	k1gFnPc1	síla
levice	levice	k1gFnSc2	levice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
do	do	k7c2	do
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
služeb	služba	k1gFnPc2	služba
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
na	na	k7c4	na
mandát	mandát	k1gInSc4	mandát
poslance	poslanec	k1gMnSc2	poslanec
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
nový	nový	k2eAgMnSc1d1	nový
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
vnesl	vnést	k5eAaPmAgMnS	vnést
Remkovo	Remkův	k2eAgNnSc4d1	Remkův
jméno	jméno	k1gNnSc4	jméno
do	do	k7c2	do
diskuze	diskuze	k1gFnSc2	diskuze
o	o	k7c6	o
novém	nový	k2eAgInSc6d1	nový
velvyslanci	velvyslanec	k1gMnSc3	velvyslanec
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
vláda	vláda	k1gFnSc1	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
nástup	nástup	k1gInSc4	nástup
Remka	Remko	k1gNnSc2	Remko
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
schválila	schválit	k5eAaPmAgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
podepsal	podepsat	k5eAaPmAgMnS	podepsat
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
Remkovu	Remkův	k2eAgFnSc4d1	Remkova
pověřovací	pověřovací	k2eAgFnSc4d1	pověřovací
listinu	listina	k1gFnSc4	listina
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
jeho	jeho	k3xOp3gFnSc4	jeho
pověřovací	pověřovací	k2eAgFnSc4d1	pověřovací
listinu	listina	k1gFnSc4	listina
převzal	převzít	k5eAaPmAgMnS	převzít
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
televizních	televizní	k2eAgFnPc2d1	televizní
kamer	kamera	k1gFnPc2	kamera
prezident	prezident	k1gMnSc1	prezident
Putin	putin	k2eAgInSc1d1	putin
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
velvyslaneckou	velvyslanecký	k2eAgFnSc4d1	velvyslanecká
misi	mise	k1gFnSc4	mise
ukončil	ukončit	k5eAaPmAgMnS	ukončit
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
letu	let	k1gInSc6	let
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgMnS	obdržet
sovětský	sovětský	k2eAgInSc4d1	sovětský
titul	titul	k1gInSc4	titul
hrdina	hrdina	k1gMnSc1	hrdina
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
s	s	k7c7	s
Leninovým	Leninův	k2eAgInSc7d1	Leninův
řádem	řád	k1gInSc7	řád
<g/>
,	,	kIx,	,
československý	československý	k2eAgInSc4d1	československý
čestný	čestný	k2eAgInSc4d1	čestný
titul	titul	k1gInSc4	titul
letec-kosmonaut	letecosmonaut	k1gInSc1	letec-kosmonaut
ČSSR	ČSSR	kA	ČSSR
a	a	k8xC	a
titul	titul	k1gInSc4	titul
hrdina	hrdina	k1gMnSc1	hrdina
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
s	s	k7c7	s
Řádem	řád	k1gInSc7	řád
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
medailí	medaile	k1gFnSc7	medaile
"	"	kIx"	"
<g/>
Za	za	k7c4	za
službu	služba	k1gFnSc4	služba
vlasti	vlast	k1gFnSc2	vlast
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1978	[number]	k4	1978
objevil	objevit	k5eAaPmAgMnS	objevit
Antonín	Antonín	k1gMnSc1	Antonín
Mrkos	Mrkos	k1gMnSc1	Mrkos
pozorováním	pozorování	k1gNnSc7	pozorování
z	z	k7c2	z
Hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
na	na	k7c4	na
Kleti	klet	k2eAgMnPc1d1	klet
planetku	planetka	k1gFnSc4	planetka
obíhající	obíhající	k2eAgFnSc7d1	obíhající
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
po	po	k7c6	po
Vladimíru	Vladimír	k1gMnSc6	Vladimír
Remkovi	Remek	k1gMnSc6	Remek
–	–	k?	–
Remek	Remek	k1gInSc1	Remek
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
2552	[number]	k4	2552
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Háje	háj	k1gInSc2	háj
byl	být	k5eAaImAgMnS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
s	s	k7c7	s
Alexejem	Alexej	k1gMnSc7	Alexej
Gubarevem	Gubarev	k1gInSc7	Gubarev
zvěčněn	zvěčnit	k5eAaPmNgInS	zvěčnit
v	v	k7c4	v
sousoší	sousoší	k1gNnSc4	sousoší
Kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Jana	Jan	k1gMnSc2	Jan
Bartoše	Bartoš	k1gMnSc2	Bartoš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
letu	let	k1gInSc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vyznamenal	vyznamenat	k5eAaPmAgInS	vyznamenat
Svaz	svaz	k1gInSc1	svaz
letců	letec	k1gMnPc2	letec
ČR	ČR	kA	ČR
vyznamenáním	vyznamenání	k1gNnPc3	vyznamenání
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
význačnými	význačný	k2eAgMnPc7d1	význačný
letci	letec	k1gMnPc7	letec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
letu	let	k1gInSc2	let
prvního	první	k4xOgMnSc2	první
kosmonauta	kosmonaut	k1gMnSc2	kosmonaut
Jurije	Jurije	k1gFnSc2	Jurije
Gagarina	Gagarin	k1gMnSc2	Gagarin
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
ruský	ruský	k2eAgMnSc1d1	ruský
prezident	prezident	k1gMnSc1	prezident
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Medveděv	Medveděv	k1gMnSc1	Medveděv
udělil	udělit	k5eAaPmAgMnS	udělit
medaili	medaile	k1gFnSc4	medaile
"	"	kIx"	"
<g/>
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c6	o
dobývání	dobývání	k1gNnSc6	dobývání
kosmu	kosmos	k1gInSc2	kosmos
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
federace	federace	k1gFnSc2	federace
obdržel	obdržet	k5eAaPmAgMnS	obdržet
řád	řád	k1gInSc4	řád
Jurije	Jurije	k1gMnSc1	Jurije
Gagarina	Gagarina	k1gMnSc1	Gagarina
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
Cenu	cena	k1gFnSc4	cena
Antonína	Antonín	k1gMnSc2	Antonín
Vítka	Vítek	k1gMnSc2	Vítek
za	za	k7c4	za
popularizaci	popularizace	k1gFnSc4	popularizace
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
od	od	k7c2	od
Astronautické	astronautický	k2eAgFnSc2d1	astronautická
sekce	sekce	k1gFnSc2	sekce
České	český	k2eAgFnSc2d1	Česká
astronomické	astronomický	k2eAgFnSc2d1	astronomická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgMnSc1	první
laureát	laureát	k1gMnSc1	laureát
této	tento	k3xDgFnSc2	tento
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
REMEK	REMEK	kA	REMEK
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
námi	my	k3xPp1nPc7	my
planeta	planeta	k1gFnSc1	planeta
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Lit.	Lit.	k?	Lit.
zprac	zprac	k1gInSc1	zprac
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Richter	Richter	k1gMnSc1	Richter
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
Vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
249	[number]	k4	249
s.	s.	k?	s.
Roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozš	rozš	k5eAaPmIp2nS	rozš
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
281	[number]	k4	281
s.	s.	k?	s.
<g/>
)	)	kIx)	)
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GUBAREV	GUBAREV	kA	GUBAREV
<g/>
,	,	kIx,	,
Alexej	Alexej	k1gMnSc1	Alexej
<g/>
;	;	kIx,	;
REMEK	REMEK	kA	REMEK
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Splněné	splněný	k2eAgFnPc1d1	splněná
naděje	naděje	k1gFnPc1	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Libor	Libor	k1gMnSc1	Libor
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
;	;	kIx,	;
Lit.	Lit.	k1gFnSc1	Lit.
zprac	zprac	k1gFnSc1	zprac
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
Bobylev	Bobylev	k1gFnSc1	Bobylev
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
201	[number]	k4	201
s.	s.	k?	s.
</s>
</p>
<p>
<s>
REMEK	REMEK	kA	REMEK
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
RICHTER	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Kozmické	Kozmický	k2eAgInPc1d1	Kozmický
návraty	návrat	k1gInPc1	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Peter	Petra	k1gFnPc2	Petra
Čačko	Čačko	k1gNnSc4	Čačko
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Smena	Smena	k1gFnSc1	Smena
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
201	[number]	k4	201
s.	s.	k?	s.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PACNER	PACNER	kA	PACNER
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
REBROV	REBROV	kA	REBROV
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
<g/>
;	;	kIx,	;
DUFEK	Dufek	k1gMnSc1	Dufek
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
dnů	den	k1gInPc2	den
kosmických	kosmický	k2eAgInPc2d1	kosmický
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
195	[number]	k4	195
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PACNER	PACNER	kA	PACNER
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbové	Kolumbus	k1gMnPc1	Kolumbus
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Souboj	souboj	k1gInSc1	souboj
o	o	k7c4	o
stanice	stanice	k1gFnPc4	stanice
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přeprac	přeprac	k1gFnSc1	přeprac
<g/>
.	.	kIx.	.
a	a	k8xC	a
dopl	dopl	k1gInSc1	dopl
<g/>
..	..	k?	..
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
428	[number]	k4	428
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
749	[number]	k4	749
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Osmdesátý	osmdesátý	k4xOgInSc1	osmdesátý
sedmý	sedmý	k4xOgMnSc1	sedmý
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
prvního	první	k4xOgMnSc2	první
československého	československý	k2eAgMnSc2d1	československý
kosmonauta	kosmonaut	k1gMnSc2	kosmonaut
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Remka	Remek	k1gMnSc2	Remek
<g/>
,	,	kIx,	,
s.	s.	k?	s.
194	[number]	k4	194
<g/>
–	–	k?	–
<g/>
223	[number]	k4	223
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
III	III	kA	III
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
Q	Q	kA	Q
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
587	[number]	k4	587
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
33	[number]	k4	33
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VYKOUPIL	vykoupit	k5eAaPmAgMnS	vykoupit
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
.	.	kIx.	.
</s>
<s>
Ecce	Ecce	k6eAd1	Ecce
homo	homo	k6eAd1	homo
:	:	kIx,	:
z	z	k7c2	z
rozhlasových	rozhlasový	k2eAgInPc2d1	rozhlasový
fejetonů	fejeton	k1gInPc2	fejeton
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Julius	Julius	k1gMnSc1	Julius
Zirkus	Zirkus	k1gMnSc1	Zirkus
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
312	[number]	k4	312
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
903377	[number]	k4	903377
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vladimír	Vladimíra	k1gFnPc2	Vladimíra
Remek	Remek	k6eAd1	Remek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Remka	Remek	k1gMnSc2	Remek
</s>
</p>
<p>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
:	:	kIx,	:
Informační	informační	k2eAgFnSc1d1	informační
kancelář	kancelář	k1gFnSc1	kancelář
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Remek	Remek	k1gMnSc1	Remek
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
kontaktní	kontaktní	k2eAgInPc1d1	kontaktní
údaje	údaj	k1gInPc1	údaj
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
na	na	k7c6	na
webu	web	k1gInSc6	web
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
činnost	činnost	k1gFnSc1	činnost
<g/>
:	:	kIx,	:
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
,	,	kIx,	,
zařazení	zařazení	k1gNnSc1	zařazení
do	do	k7c2	do
výborů	výbor	k1gInPc2	výbor
atd.	atd.	kA	atd.
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOLUB	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
MEK	mek	k0	mek
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-9-11	[number]	k4	2011-9-11
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Remek	Remky	k1gFnPc2	Remky
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOLUB	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
;	;	kIx,	;
LÁLA	Lála	k1gMnSc1	Lála
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
MEK	mek	k0	mek
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2003-3-2	[number]	k4	2003-3-2
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Sojuz	Sojuz	k1gInSc1	Sojuz
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PACNER	PACNER	kA	PACNER
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
třiceti	třicet	k4xCc7	třicet
lety	let	k1gInPc7	let
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
první	první	k4xOgMnSc1	první
Čechoslovák	Čechoslovák	k1gMnSc1	Čechoslovák
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
technet	technet	k1gInSc1	technet
<g/>
.	.	kIx.	.
<g/>
idnes	idnes	k1gInSc1	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2008-2-29	[number]	k4	2008-2-29
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOBOTKA	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Výročí	výročí	k1gNnSc4	výročí
letu	let	k1gInSc2	let
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Remka	Remek	k1gMnSc2	Remek
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2008-3-1	[number]	k4	2008-3-1
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
Remkova	Remkův	k2eAgInSc2d1	Remkův
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACHÁČEK	Macháček	k1gMnSc1	Macháček
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
35	[number]	k4	35
lety	let	k1gInPc7	let
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
evropský	evropský	k2eAgMnSc1d1	evropský
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2013-3-2	[number]	k4	2013-3-2
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Milanem	Milan	k1gMnSc7	Milan
Halouskem	Halousek	k1gMnSc7	Halousek
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠVANDRLÍK	ŠVANDRLÍK	kA	ŠVANDRLÍK
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k6eAd1	Remek
první	první	k4xOgMnSc1	první
československý	československý	k2eAgMnSc1d1	československý
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
:	:	kIx,	:
Hamelika	Hamelika	k1gFnSc1	Hamelika
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
životopis	životopis	k1gInSc1	životopis
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HERTL	HERTL	kA	HERTL
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Půjdu	jít	k5eAaImIp1nS	jít
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
mne	já	k3xPp1nSc4	já
strana	strana	k1gFnSc1	strana
pošle	poslat	k5eAaPmIp3nS	poslat
<g/>
.	.	kIx.	.
</s>
<s>
ČRo	ČRo	k?	ČRo
Plus	plus	k1gInSc1	plus
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2013-2-28	[number]	k4	2013-2-28
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
unikátních	unikátní	k2eAgFnPc2d1	unikátní
archivních	archivní	k2eAgFnPc2d1	archivní
nahrávek	nahrávka	k1gFnPc2	nahrávka
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Remka	Remek	k1gMnSc4	Remek
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
letu	let	k1gInSc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
