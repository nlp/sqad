<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Remek	Remek	k1gMnSc1	Remek
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
1948	[number]	k4	1948
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vojenský	vojenský	k2eAgMnSc1d1	vojenský
pilot	pilot	k1gMnSc1	pilot
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
československý	československý	k2eAgMnSc1d1	československý
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
<g/>
,	,	kIx,	,
Hrdina	Hrdina	k1gMnSc1	Hrdina
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Hrdina	Hrdina	k1gMnSc1	Hrdina
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
87	[number]	k4	87
<g/>
.	.	kIx.	.
kosmonautem	kosmonaut	k1gMnSc7	kosmonaut
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc6	první
z	z	k7c2	z
jiné	jiná	k1gFnSc2	jiná
země	zem	k1gFnSc2	zem
než	než	k8xS	než
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
nebo	nebo	k8xC	nebo
USA	USA	kA	USA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
úspěchem	úspěch	k1gInSc7	úspěch
československé	československý	k2eAgFnSc2d1	Československá
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
hlásí	hlásit	k5eAaImIp3nS	hlásit
jako	jako	k8xS	jako
k	k	k7c3	k
"	"	kIx"	"
<g/>
prvnímu	první	k4xOgNnSc3	první
evropskému	evropský	k2eAgMnSc3d1	evropský
kosmonautovi	kosmonaut	k1gMnSc3	kosmonaut
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
