<s>
Fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
chloroplastech	chloroplast	k1gInPc6	chloroplast
zelených	zelený	k2eAgFnPc2d1	zelená
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
mnohých	mnohý	k2eAgInPc2d1	mnohý
dalších	další	k2eAgInPc2d1	další
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
organizmů	organizmus	k1gInPc2	organizmus
(	(	kIx(	(
<g/>
různé	různý	k2eAgFnPc1d1	různá
řasy	řasa	k1gFnPc1	řasa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
sinic	sinice	k1gFnPc2	sinice
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
