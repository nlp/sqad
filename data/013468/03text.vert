<p>
<s>
Plodinová	plodinový	k2eAgFnSc1d1	Plodinová
burza	burza	k1gFnSc1	burza
je	být	k5eAaImIp3nS	být
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
budova	budova	k1gFnSc1	budova
(	(	kIx(	(
<g/>
po	po	k7c6	po
funkcionalistické	funkcionalistický	k2eAgFnSc6d1	funkcionalistická
přestavbě	přestavba	k1gFnSc6	přestavba
<g/>
)	)	kIx)	)
spravovaná	spravovaný	k2eAgFnSc1d1	spravovaná
Českou	český	k2eAgFnSc7d1	Česká
národní	národní	k2eAgFnSc7d1	národní
bankou	banka	k1gFnSc7	banka
(	(	kIx(	(
<g/>
ČNB	ČNB	kA	ČNB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
do	do	k7c2	do
státního	státní	k2eAgInSc2d1	státní
seznamu	seznam	k1gInSc2	seznam
nemovitých	movitý	k2eNgFnPc2d1	nemovitá
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
<g/>
:	:	kIx,	:
Senovážné	Senovážný	k2eAgNnSc1d1	Senovážné
náměstí	náměstí	k1gNnSc1	náměstí
866	[number]	k4	866
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
110	[number]	k4	110
00	[number]	k4	00
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
–	–	k?	–
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
budovy	budova	k1gFnSc2	budova
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Ohmanna	Ohmann	k1gMnSc2	Ohmann
<g/>
,	,	kIx,	,
profesora	profesor	k1gMnSc2	profesor
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
objekt	objekt	k1gInSc1	objekt
Pražské	pražský	k2eAgFnSc2d1	Pražská
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
(	(	kIx(	(
<g/>
PPB	PPB	kA	PPB
<g/>
)	)	kIx)	)
postaven	postaven	k2eAgInSc1d1	postaven
v	v	k7c6	v
neorenesančním	neorenesanční	k2eAgInSc6d1	neorenesanční
stylu	styl	k1gInSc6	styl
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1893	[number]	k4	1893
až	až	k6eAd1	až
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hypšmana	Hypšman	k1gMnSc2	Hypšman
<g/>
,	,	kIx,	,
absolventa	absolvent	k1gMnSc2	absolvent
Akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
až	až	k9	až
1929	[number]	k4	1929
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
přístavba	přístavba	k1gFnSc1	přístavba
původní	původní	k2eAgFnSc2d1	původní
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
hlavní	hlavní	k2eAgFnSc2d1	hlavní
budovy	budova	k1gFnSc2	budova
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
neorenesanční	neorenesanční	k2eAgNnSc1d1	neorenesanční
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
funkcionalistickém	funkcionalistický	k2eAgInSc6d1	funkcionalistický
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
Únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
Plodinová	plodinový	k2eAgFnSc1d1	Plodinová
burza	burza	k1gFnSc1	burza
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
přešly	přejít	k5eAaPmAgInP	přejít
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
sousedící	sousedící	k2eAgFnSc2d1	sousedící
Státní	státní	k2eAgFnSc2d1	státní
banky	banka	k1gFnSc2	banka
československé	československý	k2eAgFnSc2d1	Československá
<g/>
)	)	kIx)	)
i	i	k9	i
provozy	provoz	k1gInPc4	provoz
dalších	další	k2eAgFnPc2d1	další
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
adaptována	adaptovat	k5eAaBmNgFnS	adaptovat
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
redakce	redakce	k1gFnSc2	redakce
a	a	k8xC	a
útvary	útvar	k1gInPc4	útvar
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
<g/>
Budovu	budova	k1gFnSc4	budova
v	v	k7c6	v
havarijním	havarijní	k2eAgInSc6d1	havarijní
stavu	stav	k1gInSc6	stav
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Kružíka	Kružík	k1gMnSc2	Kružík
zachovala	zachovat	k5eAaPmAgFnS	zachovat
prvky	prvek	k1gInPc4	prvek
art	art	k?	art
deco	deco	k6eAd1	deco
navržené	navržený	k2eAgFnSc2d1	navržená
Bohumilem	Bohumil	k1gMnSc7	Bohumil
Hypšmanem	Hypšman	k1gMnSc7	Hypšman
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
byl	být	k5eAaImAgMnS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kružík	Kružík	k1gMnSc1	Kružík
limitován	limitovat	k5eAaBmNgInS	limitovat
požadavky	požadavek	k1gInPc4	požadavek
památkářů	památkář	k1gMnPc2	památkář
a	a	k8xC	a
proto	proto	k8xC	proto
pro	pro	k7c4	pro
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
zvolil	zvolit	k5eAaPmAgMnS	zvolit
mobilní	mobilní	k2eAgFnSc4d1	mobilní
vestavbu	vestavba	k1gFnSc4	vestavba
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
samonosné	samonosný	k2eAgFnSc6d1	samonosná
moderní	moderní	k2eAgFnSc6d1	moderní
konstrukci	konstrukce	k1gFnSc6	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
upevněná	upevněný	k2eAgFnSc1d1	upevněná
ve	v	k7c6	v
zdech	zeď	k1gFnPc6	zeď
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konstrukce	konstrukce	k1gFnSc1	konstrukce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kdykoliv	kdykoliv	k6eAd1	kdykoliv
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
poškodil	poškodit	k5eAaPmAgInS	poškodit
původní	původní	k2eAgInSc1d1	původní
funkcionalistický	funkcionalistický	k2eAgInSc1d1	funkcionalistický
styl	styl	k1gInSc1	styl
celé	celý	k2eAgFnSc2d1	celá
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Zdařilým	zdařilý	k2eAgInSc7d1	zdařilý
prvkem	prvek	k1gInSc7	prvek
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
je	být	k5eAaImIp3nS	být
osvětlení	osvětlení	k1gNnSc1	osvětlení
budovy	budova	k1gFnSc2	budova
(	(	kIx(	(
<g/>
také	také	k9	také
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Kružíka	Kružík	k1gMnSc2	Kružík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
původní	původní	k2eAgInSc4d1	původní
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
úvod	úvod	k1gInSc1	úvod
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Obchodování	obchodování	k1gNnSc1	obchodování
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
PPB	PPB	kA	PPB
===	===	k?	===
</s>
</p>
<p>
<s>
Účelem	účel	k1gInSc7	účel
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
je	být	k5eAaImIp3nS	být
nákup	nákup	k1gInSc4	nákup
a	a	k8xC	a
prodej	prodej	k1gInSc4	prodej
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
komodit	komodita	k1gFnPc2	komodita
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
obchodovalo	obchodovat	k5eAaImAgNnS	obchodovat
obilím	obilí	k1gNnSc7	obilí
a	a	k8xC	a
zemědělskými	zemědělský	k2eAgFnPc7d1	zemědělská
plodinami	plodina	k1gFnPc7	plodina
na	na	k7c6	na
týdenních	týdenní	k2eAgInPc6d1	týdenní
trzích	trh	k1gInPc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
nejdříve	dříve	k6eAd3	dříve
na	na	k7c6	na
Koňském	koňský	k2eAgInSc6d1	koňský
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
na	na	k7c6	na
Senovážném	Senovážný	k2eAgNnSc6d1	Senovážné
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
donutil	donutit	k5eAaPmAgMnS	donutit
obchodníky	obchodník	k1gMnPc4	obchodník
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc1	zřízení
jednoho	jeden	k4xCgNnSc2	jeden
organizovaného	organizovaný	k2eAgNnSc2d1	organizované
střediska	středisko	k1gNnSc2	středisko
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
zemědělskými	zemědělský	k2eAgInPc7d1	zemědělský
produkty	produkt	k1gInPc7	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
PPB	PPB	kA	PPB
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
komitét	komitét	k1gInSc1	komitét
pro	pro	k7c4	pro
zřízení	zřízení	k1gNnSc4	zřízení
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
návrh	návrh	k1gInSc4	návrh
stanov	stanova	k1gFnPc2	stanova
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
schváleny	schválen	k2eAgInPc1d1	schválen
a	a	k8xC	a
poté	poté	k6eAd1	poté
mohl	moct	k5eAaImAgInS	moct
komitét	komitét	k1gInSc1	komitét
založit	založit	k5eAaPmF	založit
plodinovou	plodinový	k2eAgFnSc4d1	Plodinová
burzu	burza	k1gFnSc4	burza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
Senovážném	Senovážný	k2eAgNnSc6d1	Senovážné
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
PPB	PPB	kA	PPB
zahájila	zahájit	k5eAaPmAgFnS	zahájit
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostně	slavnostně	k6eAd1	slavnostně
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1894	[number]	k4	1894
a	a	k8xC	a
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
burzovní	burzovní	k2eAgFnSc6d1	burzovní
budově	budova	k1gFnSc6	budova
zahájen	zahájit	k5eAaPmNgInS	zahájit
první	první	k4xOgInSc1	první
burzovní	burzovní	k2eAgInSc1d1	burzovní
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
také	také	k9	také
vyšel	vyjít	k5eAaPmAgInS	vyjít
poprvé	poprvé	k6eAd1	poprvé
kurzovní	kurzovní	k2eAgInSc1d1	kurzovní
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pak	pak	k6eAd1	pak
nadále	nadále	k6eAd1	nadále
zaznamenával	zaznamenávat	k5eAaImAgMnS	zaznamenávat
ceny	cena	k1gFnPc4	cena
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
PPB	PPB	kA	PPB
za	za	k7c4	za
první	první	k4xOgInPc4	první
světové	světový	k2eAgInPc4d1	světový
války	válek	k1gInPc4	válek
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
téměř	téměř	k6eAd1	téměř
přerušila	přerušit	k5eAaPmAgFnS	přerušit
provoz	provoz	k1gInSc4	provoz
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většinu	většina	k1gFnSc4	většina
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
obhospodařoval	obhospodařovat	k5eAaImAgInS	obhospodařovat
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
došlo	dojít	k5eAaPmAgNnS	dojít
opět	opět	k6eAd1	opět
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
Pražské	pražský	k2eAgFnSc6d1	Pražská
plodinové	plodinový	k2eAgFnSc6d1	Plodinová
burze	burza	k1gFnSc6	burza
založena	založen	k2eAgFnSc1d1	založena
dřevařská	dřevařský	k2eAgFnSc1d1	dřevařská
burza	burza	k1gFnSc1	burza
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
lnářská	lnářský	k2eAgFnSc1d1	Lnářská
burza	burza	k1gFnSc1	burza
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
burza	burza	k1gFnSc1	burza
pro	pro	k7c4	pro
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
burza	burza	k1gFnSc1	burza
pro	pro	k7c4	pro
vejce	vejce	k1gNnPc4	vejce
a	a	k8xC	a
burza	burza	k1gFnSc1	burza
pro	pro	k7c4	pro
kávu	káva	k1gFnSc4	káva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
PPB	PPB	kA	PPB
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
pokles	pokles	k1gInSc1	pokles
činnosti	činnost	k1gFnSc2	činnost
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
nastal	nastat	k5eAaPmAgInS	nastat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
zřízen	zřízen	k2eAgInSc1d1	zřízen
obilný	obilný	k2eAgInSc1d1	obilný
monopol	monopol	k1gInSc1	monopol
<g/>
,	,	kIx,	,
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
plodinová	plodinový	k2eAgFnSc1d1	Plodinová
burza	burza	k1gFnSc1	burza
významu	význam	k1gInSc2	význam
jako	jako	k8xS	jako
místo	místo	k1gNnSc4	místo
cenotvorby	cenotvorba	k1gFnSc2	cenotvorba
obilí	obilí	k1gNnSc2	obilí
a	a	k8xC	a
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Buza	Buza	k1gFnSc1	Buza
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
života	život	k1gInSc2	život
země	zem	k1gFnSc2	zem
organizační	organizační	k2eAgFnSc2d1	organizační
činností	činnost	k1gFnSc7	činnost
<g/>
:	:	kIx,	:
experti	expert	k1gMnPc1	expert
burzy	burza	k1gFnSc2	burza
působili	působit	k5eAaImAgMnP	působit
jako	jako	k9	jako
znalci	znalec	k1gMnPc1	znalec
<g/>
,	,	kIx,	,
rozhodovali	rozhodovat	k5eAaImAgMnP	rozhodovat
soudní	soudní	k2eAgFnPc4d1	soudní
spory	spora	k1gFnPc4	spora
a	a	k8xC	a
koncipovaly	koncipovat	k5eAaBmAgFnP	koncipovat
osnovy	osnova	k1gFnPc1	osnova
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
vládních	vládní	k2eAgNnPc2d1	vládní
nařízení	nařízení	k1gNnPc2	nařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
PPB	PPB	kA	PPB
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
===	===	k?	===
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
omezila	omezit	k5eAaPmAgFnS	omezit
opět	opět	k6eAd1	opět
činnost	činnost	k1gFnSc4	činnost
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
pevným	pevný	k2eAgInSc7d1	pevný
systémem	systém	k1gInSc7	systém
řízeného	řízený	k2eAgNnSc2d1	řízené
válečného	válečný	k2eAgNnSc2d1	válečné
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Coby	Coby	k?	Coby
kapitalistická	kapitalistický	k2eAgFnSc1d1	kapitalistická
instituce	instituce	k1gFnSc1	instituce
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
plodinová	plodinový	k2eAgFnSc1d1	Plodinová
burza	burza	k1gFnSc1	burza
na	na	k7c6	na
významu	význam	k1gInSc6	význam
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vytrácel	vytrácet	k5eAaImAgMnS	vytrácet
účel	účel	k1gInSc4	účel
její	její	k3xOp3gFnSc2	její
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
číslo	číslo	k1gNnSc1	číslo
32	[number]	k4	32
ze	z	k7c2	z
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
byly	být	k5eAaImAgInP	být
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1952	[number]	k4	1952
všechny	všechen	k3xTgFnPc1	všechen
dosavadní	dosavadní	k2eAgFnPc1d1	dosavadní
plodinové	plodinový	k2eAgFnPc1d1	Plodinová
burzy	burza	k1gFnPc1	burza
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
(	(	kIx(	(
<g/>
Pražskou	pražský	k2eAgFnSc4d1	Pražská
plodinovou	plodinový	k2eAgFnSc4d1	Plodinová
burzu	burza	k1gFnSc4	burza
nevyjímaje	nevyjímaje	k7c4	nevyjímaje
<g/>
)	)	kIx)	)
definitivně	definitivně	k6eAd1	definitivně
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podrobná	podrobný	k2eAgFnSc1d1	podrobná
historie	historie	k1gFnSc1	historie
budovy	budova	k1gFnSc2	budova
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc4	první
kroky	krok	k1gInPc4	krok
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
"	"	kIx"	"
<g/>
Družstvo	družstvo	k1gNnSc1	družstvo
ku	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
a	a	k8xC	a
udržování	udržování	k1gNnSc4	udržování
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
<g/>
"	"	kIx"	"
pozemek	pozemek	k1gInSc1	pozemek
na	na	k7c6	na
Senovážném	Senovážný	k2eAgNnSc6d1	Senovážné
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
pozemku	pozemek	k1gInSc6	pozemek
měl	mít	k5eAaImAgInS	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
objekt	objekt	k1gInSc4	objekt
budoucí	budoucí	k2eAgFnSc2d1	budoucí
komoditní	komoditní	k2eAgFnSc2d1	komoditní
burzy	burza	k1gFnSc2	burza
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
této	tento	k3xDgFnSc2	tento
instituce	instituce	k1gFnSc2	instituce
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Senovážném	Senovážný	k2eAgNnSc6d1	Senovážné
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
zakoupen	zakoupen	k2eAgInSc4d1	zakoupen
čtyřkřídlý	čtyřkřídlý	k2eAgInSc4d1	čtyřkřídlý
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Požadavkům	požadavek	k1gInPc3	požadavek
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
nevyhovoval	vyhovovat	k5eNaImAgMnS	vyhovovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
koupi	koupě	k1gFnSc6	koupě
se	se	k3xPyFc4	se
plánovalo	plánovat	k5eAaImAgNnS	plánovat
jeho	jeho	k3xOp3gNnSc1	jeho
zbourání	zbourání	k1gNnSc1	zbourání
a	a	k8xC	a
výstavba	výstavba	k1gFnSc1	výstavba
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Projekt	projekt	k1gInSc1	projekt
budovy	budova	k1gFnSc2	budova
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
na	na	k7c4	na
projekt	projekt	k1gInSc4	projekt
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
rakouský	rakouský	k2eAgMnSc1d1	rakouský
architekt	architekt	k1gMnSc1	architekt
Bedřich	Bedřich	k1gMnSc1	Bedřich
Ohmann	Ohmann	k1gMnSc1	Ohmann
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
Ohmannův	Ohmannův	k2eAgInSc1d1	Ohmannův
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
inklinující	inklinující	k2eAgMnSc1d1	inklinující
k	k	k7c3	k
secesnímu	secesní	k2eAgInSc3d1	secesní
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
výrazně	výrazně	k6eAd1	výrazně
upraven	upravit	k5eAaPmNgInS	upravit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
stylu	styl	k1gInSc2	styl
tradiční	tradiční	k2eAgFnSc2d1	tradiční
novorenesance	novorenesance	k1gFnSc2	novorenesance
do	do	k7c2	do
realizační	realizační	k2eAgFnSc2d1	realizační
dokumentace	dokumentace	k1gFnSc2	dokumentace
realizátory	realizátor	k1gMnPc7	realizátor
stavby	stavba	k1gFnSc2	stavba
-	-	kIx~	-
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Jechenthalem	Jechenthal	k1gMnSc7	Jechenthal
a	a	k8xC	a
Františkem	František	k1gMnSc7	František
Hněvkovským	Hněvkovský	k2eAgMnSc7d1	Hněvkovský
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stavebních	stavební	k2eAgFnPc2d1	stavební
prací	práce	k1gFnPc2	práce
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
zadavatel	zadavatel	k1gMnSc1	zadavatel
o	o	k7c6	o
částečné	částečný	k2eAgFnSc6d1	částečná
změně	změna	k1gFnSc6	změna
původního	původní	k2eAgInSc2d1	původní
návrhu	návrh	k1gInSc2	návrh
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
dvojicí	dvojice	k1gFnSc7	dvojice
architektů	architekt	k1gMnPc2	architekt
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
byli	být	k5eAaImAgMnP	být
Josef	Josef	k1gMnSc1	Josef
Zítek	Zítek	k1gMnSc1	Zítek
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Schulz	Schulz	k1gMnSc1	Schulz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stavba	stavba	k1gFnSc1	stavba
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
zahájení	zahájení	k1gNnSc2	zahájení
provozu	provoz	k1gInSc2	provoz
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
stavby	stavba	k1gFnSc2	stavba
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1893	[number]	k4	1893
až	až	k9	až
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřnímu	vnitřní	k2eAgNnSc3d1	vnitřní
členění	členění	k1gNnSc3	členění
budovy	budova	k1gFnSc2	budova
dominovalo	dominovat	k5eAaImAgNnS	dominovat
mohutné	mohutný	k2eAgNnSc1d1	mohutné
schodiště	schodiště	k1gNnSc1	schodiště
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
a	a	k8xC	a
malým	malý	k2eAgInSc7d1	malý
burzovním	burzovní	k2eAgInSc7d1	burzovní
sálem	sál	k1gInSc7	sál
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
prostory	prostora	k1gFnPc1	prostora
v	v	k7c6	v
sobě	se	k3xPyFc3	se
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
také	také	k9	také
kavárnu	kavárna	k1gFnSc4	kavárna
a	a	k8xC	a
vinárnu	vinárna	k1gFnSc4	vinárna
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
palác	palác	k1gInSc1	palác
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plodinová	plodinový	k2eAgFnSc1d1	Plodinová
burza	burza	k1gFnSc1	burza
se	se	k3xPyFc4	se
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
===	===	k?	===
</s>
</p>
<p>
<s>
Stále	stále	k6eAd1	stále
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
potřeby	potřeba	k1gFnPc1	potřeba
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
v	v	k7c6	v
době	doba	k1gFnSc6	doba
I.	I.	kA	I.
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
řešeny	řešit	k5eAaImNgInP	řešit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
vyvlastněním	vyvlastnění	k1gNnSc7	vyvlastnění
sousedního	sousední	k2eAgInSc2d1	sousední
pozemku	pozemek	k1gInSc2	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
majitel	majitel	k1gMnSc1	majitel
-	-	kIx~	-
německé	německý	k2eAgNnSc1d1	německé
kasino	kasino	k1gNnSc1	kasino
-	-	kIx~	-
se	se	k3xPyFc4	se
ale	ale	k9	ale
o	o	k7c4	o
pozemek	pozemek	k1gInSc4	pozemek
pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
soudilo	soudit	k5eAaImAgNnS	soudit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
stavební	stavební	k2eAgFnPc1d1	stavební
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
pozemku	pozemek	k1gInSc6	pozemek
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
1928	[number]	k4	1928
až	až	k6eAd1	až
1929	[number]	k4	1929
probíhaly	probíhat	k5eAaImAgFnP	probíhat
nejen	nejen	k6eAd1	nejen
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
pozemku	pozemek	k1gInSc6	pozemek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
úpravy	úprava	k1gFnPc1	úprava
původní	původní	k2eAgFnSc2d1	původní
staré	starý	k2eAgFnSc2d1	stará
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stávající	stávající	k2eAgFnSc6d1	stávající
budově	budova	k1gFnSc6	budova
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
letech	let	k1gInPc6	let
provedena	proveden	k2eAgFnSc1d1	provedena
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
přístavba	přístavba	k1gFnSc1	přístavba
podle	podle	k7c2	podle
architektonického	architektonický	k2eAgInSc2d1	architektonický
návrhu	návrh	k1gInSc2	návrh
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hypšmana	Hypšman	k1gMnSc2	Hypšman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Budova	budova	k1gFnSc1	budova
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
až	až	k9	až
1959	[number]	k4	1959
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
činnost	činnost	k1gFnSc1	činnost
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
jako	jako	k8xC	jako
instituce	instituce	k1gFnSc1	instituce
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Živnobanka	Živnobanka	k1gFnSc1	Živnobanka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
předána	předat	k5eAaPmNgFnS	předat
<g/>
,	,	kIx,	,
přestavěla	přestavět	k5eAaPmAgFnS	přestavět
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
postory	postor	k1gInPc4	postor
na	na	k7c4	na
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1950	[number]	k4	1950
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Státní	státní	k2eAgFnSc1d1	státní
banka	banka	k1gFnSc1	banka
československá	československý	k2eAgFnSc1d1	Československá
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
tzv.	tzv.	kA	tzv.
úřadovně	úřadovna	k1gFnSc6	úřadovna
č.	č.	k?	č.
1	[number]	k4	1
přičleněna	přičlenit	k5eAaPmNgFnS	přičlenit
rovněž	rovněž	k9	rovněž
budova	budova	k1gFnSc1	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
č.	č.	k?	č.
866	[number]	k4	866
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
sídlilo	sídlit	k5eAaImAgNnS	sídlit
oddělení	oddělení	k1gNnSc4	oddělení
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
finančním	finanční	k2eAgNnSc7d1	finanční
zabezpečením	zabezpečení	k1gNnSc7	zabezpečení
monopolních	monopolní	k2eAgFnPc2d1	monopolní
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
Státní	státní	k2eAgFnSc2d1	státní
banky	banka	k1gFnSc2	banka
československé	československý	k2eAgFnSc2d1	Československá
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
bývalé	bývalý	k2eAgFnSc2d1	bývalá
burzy	burza	k1gFnSc2	burza
i	i	k8xC	i
místnost	místnost	k1gFnSc1	místnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
nacházela	nacházet	k5eAaImAgFnS	nacházet
malá	malý	k2eAgFnSc1d1	malá
kavárna	kavárna	k1gFnSc1	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
závodní	závodní	k2eAgFnSc4d1	závodní
(	(	kIx(	(
<g/>
dietní	dietní	k2eAgFnSc4d1	dietní
<g/>
)	)	kIx)	)
jídelnu	jídelna	k1gFnSc4	jídelna
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
Státní	státní	k2eAgFnSc2d1	státní
banky	banka	k1gFnSc2	banka
československé	československý	k2eAgFnPc1d1	Československá
sloužily	sloužit	k5eAaImAgFnP	sloužit
prostory	prostora	k1gFnPc1	prostora
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vinárny	vinárna	k1gFnSc2	vinárna
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
sálu	sál	k1gInSc2	sál
Plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
padesátých	padesátý	k4xOgInPc2	padesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
obsazena	obsadit	k5eAaPmNgNnP	obsadit
pracovníky	pracovník	k1gMnPc4	pracovník
platebního	platební	k2eAgInSc2d1	platební
a	a	k8xC	a
zúčtovacího	zúčtovací	k2eAgInSc2d1	zúčtovací
styku	styk	k1gInSc2	styk
<g/>
.	.	kIx.	.
<g/>
Objekt	objekt	k1gInSc1	objekt
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
sloužil	sloužit	k5eAaImAgInS	sloužit
Státní	státní	k2eAgFnSc3d1	státní
bance	banka	k1gFnSc3	banka
československé	československý	k2eAgFnSc3d1	Československá
sice	sice	k8xC	sice
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
výlučně	výlučně	k6eAd1	výlučně
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
dislokována	dislokován	k2eAgFnSc1d1	dislokována
řada	řada	k1gFnSc1	řada
cizích	cizí	k2eAgMnPc2d1	cizí
(	(	kIx(	(
<g/>
s	s	k7c7	s
bankou	banka	k1gFnSc7	banka
nikterak	nikterak	k6eAd1	nikterak
nesouvisejících	související	k2eNgFnPc2d1	nesouvisející
<g/>
)	)	kIx)	)
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc6	století
budova	budova	k1gFnSc1	budova
hostila	hostit	k5eAaImAgFnS	hostit
telefonní	telefonní	k2eAgFnSc4d1	telefonní
ústřednu	ústředna	k1gFnSc4	ústředna
národního	národní	k2eAgInSc2d1	národní
podniku	podnik	k1gInSc2	podnik
Masna	masna	k1gFnSc1	masna
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
<g/>
)	)	kIx)	)
dokonce	dokonce	k9	dokonce
i	i	k9	i
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vlastní	vlastní	k2eAgInSc4d1	vlastní
provoz	provoz	k1gInSc4	provoz
Státní	státní	k2eAgFnSc2d1	státní
banky	banka	k1gFnSc2	banka
československé	československý	k2eAgFnPc1d1	Československá
nakonec	nakonec	k6eAd1	nakonec
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
jen	jen	k9	jen
prostory	prostora	k1gFnPc1	prostora
dietní	dietní	k2eAgFnSc2d1	dietní
jídelny	jídelna	k1gFnSc2	jídelna
<g/>
,	,	kIx,	,
sklad	sklad	k1gInSc4	sklad
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
dalšího	další	k2eAgNnSc2d1	další
příslušenství	příslušenství	k1gNnSc2	příslušenství
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
místností	místnost	k1gFnPc2	místnost
využívaných	využívaný	k2eAgFnPc2d1	využívaná
Městskou	městský	k2eAgFnSc7d1	městská
pobočkou	pobočka	k1gFnSc7	pobočka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
agenda	agenda	k1gFnSc1	agenda
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
Na	na	k7c6	na
Příkopě	příkop	k1gInSc6	příkop
28	[number]	k4	28
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
budovy	budova	k1gFnSc2	budova
užíval	užívat	k5eAaImAgInS	užívat
Potravinoprojekt	Potravinoprojekt	k1gInSc1	Potravinoprojekt
<g/>
,	,	kIx,	,
státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
projektování	projektování	k1gNnSc4	projektování
podniků	podnik	k1gInPc2	podnik
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Slovan	Slovany	k1gInPc2	Slovany
Státní	státní	k2eAgFnSc1d1	státní
banka	banka	k1gFnSc1	banka
československá	československý	k2eAgFnSc1d1	Československá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
ČT	ČT	kA	ČT
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
budově	budova	k1gFnSc6	budova
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
studia	studio	k1gNnSc2	studio
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
interiéry	interiér	k1gInPc4	interiér
budovy	budova	k1gFnSc2	budova
stavebně	stavebně	k6eAd1	stavebně
upravila	upravit	k5eAaPmAgFnS	upravit
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
tak	tak	k9	tak
aby	aby	kYmCp3nP	aby
vyhovovaly	vyhovovat	k5eAaImAgInP	vyhovovat
jejím	její	k3xOp3gFnPc3	její
potřebám	potřeba	k1gFnPc3	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1960	[number]	k4	1960
se	se	k3xPyFc4	se
největším	veliký	k2eAgMnSc7d3	veliký
uživatelem	uživatel	k1gMnSc7	uživatel
budovy	budova	k1gFnSc2	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
stala	stát	k5eAaPmAgFnS	stát
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
měla	mít	k5eAaImAgFnS	mít
vysílací	vysílací	k2eAgInPc4d1	vysílací
prostory	prostor	k1gInPc4	prostor
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
i	i	k8xC	i
své	svůj	k3xOyFgNnSc4	svůj
ústředí	ústředí	k1gNnSc4	ústředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
ČNB	ČNB	kA	ČNB
kupuje	kupovat	k5eAaImIp3nS	kupovat
budovu	budova	k1gFnSc4	budova
plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
===	===	k?	===
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
Plodinové	plodinový	k2eAgFnSc2d1	Plodinová
burzy	burza	k1gFnSc2	burza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
trpěla	trpět	k5eAaImAgFnS	trpět
častými	častý	k2eAgFnPc7d1	častá
změnami	změna	k1gFnPc7	změna
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
chátrala	chátrat	k5eAaImAgFnS	chátrat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
havarijním	havarijní	k2eAgInSc6d1	havarijní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
budovy	budova	k1gFnSc2	budova
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
v	v	k7c6	v
gesci	gesce	k1gFnSc6	gesce
ČNB	ČNB	kA	ČNB
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zde	zde	k6eAd1	zde
(	(	kIx(	(
<g/>
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
)	)	kIx)	)
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
kongresové	kongresový	k2eAgNnSc4d1	Kongresové
centrum	centrum	k1gNnSc4	centrum
ČNB	ČNB	kA	ČNB
<g/>
,	,	kIx,	,
specializovanou	specializovaný	k2eAgFnSc4d1	specializovaná
odbornou	odborný	k2eAgFnSc4d1	odborná
knihovnu	knihovna	k1gFnSc4	knihovna
ČNB	ČNB	kA	ČNB
a	a	k8xC	a
bankovní	bankovní	k2eAgInSc1d1	bankovní
klub	klub	k1gInSc1	klub
ČNB	ČNB	kA	ČNB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kongresové	kongresový	k2eAgNnSc1d1	Kongresové
centrum	centrum	k1gNnSc1	centrum
ČNB	ČNB	kA	ČNB
===	===	k?	===
</s>
</p>
<p>
<s>
Kongresové	kongresový	k2eAgNnSc1d1	Kongresové
centrum	centrum	k1gNnSc1	centrum
ČNB	ČNB	kA	ČNB
disponuje	disponovat	k5eAaBmIp3nS	disponovat
dvěma	dva	k4xCgInPc7	dva
sály	sál	k1gInPc7	sál
(	(	kIx(	(
<g/>
velkým	velký	k2eAgInSc7d1	velký
a	a	k8xC	a
malým	malý	k2eAgInSc7d1	malý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společným	společný	k2eAgInSc7d1	společný
foyerem	foyer	k1gInSc7	foyer
a	a	k8xC	a
potřebným	potřebný	k2eAgNnSc7d1	potřebné
zázemím	zázemí	k1gNnSc7	zázemí
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
440	[number]	k4	440
m2	m2	k4	m2
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
300	[number]	k4	300
až	až	k9	až
360	[number]	k4	360
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc1d1	malý
sál	sál	k1gInSc1	sál
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
120	[number]	k4	120
m2	m2	k4	m2
pojme	pojmout	k5eAaPmIp3nS	pojmout
50	[number]	k4	50
až	až	k9	až
100	[number]	k4	100
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Prostory	prostor	k1gInPc1	prostor
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
mobiliářem	mobiliář	k1gInSc7	mobiliář
a	a	k8xC	a
základní	základní	k2eAgFnSc7d1	základní
konferenční	konferenční	k2eAgFnSc7d1	konferenční
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Sály	Sála	k1gFnPc1	Sála
jsou	být	k5eAaImIp3nP	být
ozvučeny	ozvučen	k2eAgFnPc1d1	ozvučena
<g/>
,	,	kIx,	,
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
datová	datový	k2eAgFnSc1d1	datová
projekce	projekce	k1gFnSc1	projekce
a	a	k8xC	a
tlumočnické	tlumočnický	k2eAgFnPc1d1	tlumočnická
kabiny	kabina	k1gFnPc1	kabina
s	s	k7c7	s
bezdrátovými	bezdrátový	k2eAgNnPc7d1	bezdrátové
sluchátky	sluchátko	k1gNnPc7	sluchátko
(	(	kIx(	(
<g/>
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tlumočení	tlumočení	k1gNnSc4	tlumočení
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgInPc1d1	jiný
externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
a	a	k8xC	a
možné	možný	k2eAgInPc1d1	možný
zdroje	zdroj	k1gInPc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pražská	pražský	k2eAgFnSc1d1	Pražská
plodinová	plodinový	k2eAgFnSc1d1	Plodinová
burza	burza	k1gFnSc1	burza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
