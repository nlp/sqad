<s>
K	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
budovy	budova	k1gFnSc2	budova
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
v	v	k7c6	v
gesci	gesce	k1gFnSc6	gesce
ČNB	ČNB	kA	ČNB
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
(	(	kIx(	(
<g/>
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
)	)	kIx)	)
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
kongresové	kongresový	k2eAgNnSc4d1	Kongresové
centrum	centrum	k1gNnSc4	centrum
ČNB	ČNB	kA	ČNB
<g/>
,	,	kIx,	,
specializovanou	specializovaný	k2eAgFnSc4d1	specializovaná
odbornou	odborný	k2eAgFnSc4d1	odborná
knihovnu	knihovna	k1gFnSc4	knihovna
ČNB	ČNB	kA	ČNB
a	a	k8xC	a
bankovní	bankovní	k2eAgInSc1d1	bankovní
klub	klub	k1gInSc1	klub
ČNB	ČNB	kA	ČNB
<g/>
.	.	kIx.	.
</s>
