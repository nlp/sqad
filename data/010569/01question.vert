<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
gruzínský	gruzínský	k2eAgMnSc1d1	gruzínský
revolucionář	revolucionář	k1gMnSc1	revolucionář
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922	[number]	k4	1922
až	až	k9	až
1952	[number]	k4	1952
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejkrutějších	krutý	k2eAgMnPc2d3	nejkrutější
diktátorů	diktátor	k1gMnPc2	diktátor
historie	historie	k1gFnSc2	historie
<g/>
?	?	kIx.	?
</s>
