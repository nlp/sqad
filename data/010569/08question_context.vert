<s>
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Džugašvili	Džugašvili	k1gMnSc1	Džugašvili
(	(	kIx(	(
<g/>
gruzínsky	gruzínsky	k6eAd1	gruzínsky
<g/>
:	:	kIx,	:
ი	ი	k?	ი
ბ	ბ	k?	ბ
ძ	ძ	k?	ძ
ჯ	ჯ	k?	ჯ
[	[	kIx(	[
<g/>
Ioseb	Iosba	k1gFnPc2	Iosba
Besarionis	Besarionis	k1gFnSc2	Besarionis
dze	dze	k?	dze
Džugašvili	Džugašvili	k1gFnSc2	Džugašvili
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
И	И	k?	И
В	В	k?	В
Д	Д	k?	Д
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1878	[number]	k4	1878
Gori	Gori	k1gNnSc2	Gori
<g/>
,	,	kIx,	,
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Stalin	Stalin	k1gMnSc1	Stalin
(	(	kIx(	(
<g/>
С	С	k?	С
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
gruzínský	gruzínský	k2eAgMnSc1d1	gruzínský
revolucionář	revolucionář	k1gMnSc1	revolucionář
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
teoretik	teoretik	k1gMnSc1	teoretik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
byl	být	k5eAaImAgMnS	být
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>

