<p>
<s>
Rodinné	rodinný	k2eAgFnPc1d1	rodinná
konstelace	konstelace	k1gFnPc1	konstelace
jsou	být	k5eAaImIp3nP	být
metodou	metoda	k1gFnSc7	metoda
rozvoje	rozvoj	k1gInSc2	rozvoj
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
sebepoznávání	sebepoznávání	k1gNnSc2	sebepoznávání
a	a	k8xC	a
psychoterapie	psychoterapie	k1gFnSc2	psychoterapie
<g/>
.	.	kIx.	.
</s>
<s>
Umožňují	umožňovat	k5eAaImIp3nP	umožňovat
názorně	názorně	k6eAd1	názorně
ukázat	ukázat	k5eAaPmF	ukázat
a	a	k8xC	a
pocítit	pocítit	k5eAaPmF	pocítit
souvislosti	souvislost	k1gFnPc4	souvislost
a	a	k8xC	a
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
členy	člen	k1gMnPc7	člen
širších	široký	k2eAgInPc2d2	širší
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
tvůrcem	tvůrce	k1gMnSc7	tvůrce
je	být	k5eAaImIp3nS	být
Bert	Berta	k1gFnPc2	Berta
Hellinger	Hellinger	k1gMnSc1	Hellinger
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
terapii	terapie	k1gFnSc6	terapie
a	a	k8xC	a
poradenství	poradenství	k1gNnSc6	poradenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účel	účel	k1gInSc4	účel
a	a	k8xC	a
původ	původ	k1gInSc4	původ
metody	metoda	k1gFnSc2	metoda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
rodinných	rodinný	k2eAgInPc6d1	rodinný
osudech	osud	k1gInPc6	osud
se	se	k3xPyFc4	se
některé	některý	k3yIgInPc1	některý
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
vzorce	vzorec	k1gInSc2	vzorec
chování	chování	k1gNnSc2	chování
<g/>
)	)	kIx)	)
opakují	opakovat	k5eAaImIp3nP	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
podobnosti	podobnost	k1gFnPc4	podobnost
v	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
nápadné	nápadný	k2eAgFnSc2d1	nápadná
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnPc1	jejich
aktéři	aktér	k1gMnPc1	aktér
ptát	ptát	k5eAaImF	ptát
po	po	k7c6	po
příčině	příčina	k1gFnSc6	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Rodinné	rodinný	k2eAgFnPc1d1	rodinná
konstelace	konstelace	k1gFnPc1	konstelace
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
potřebu	potřeba	k1gFnSc4	potřeba
a	a	k8xC	a
nabízejí	nabízet	k5eAaImIp3nP	nabízet
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
nevědomou	vědomý	k2eNgFnSc4d1	nevědomá
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
dynamiku	dynamika	k1gFnSc4	dynamika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodinné	rodinný	k2eAgFnPc1d1	rodinná
konstelace	konstelace	k1gFnPc1	konstelace
zavedl	zavést	k5eAaPmAgInS	zavést
Bert	Berta	k1gFnPc2	Berta
Hellinger	Hellinger	k1gInSc1	Hellinger
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
a	a	k8xC	a
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hellingerův	Hellingerův	k2eAgInSc1d1	Hellingerův
fenomenologický	fenomenologický	k2eAgInSc1d1	fenomenologický
přístup	přístup	k1gInSc1	přístup
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
psychodrama	psychodrama	k1gNnSc4	psychodrama
<g/>
,	,	kIx,	,
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
rakouským	rakouský	k2eAgMnSc7d1	rakouský
lékařem	lékař	k1gMnSc7	lékař
Jakobem	Jakob	k1gMnSc7	Jakob
Levy	Levy	k?	Levy
Moreno	Morena	k1gFnSc5	Morena
<g/>
,	,	kIx,	,
rodinné	rodinný	k2eAgMnPc4d1	rodinný
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
a	a	k8xC	a
sochání	sochání	k1gNnSc2	sochání
Virginie	Virginie	k1gFnSc2	Virginie
Satirové	Satirová	k1gFnSc2	Satirová
a	a	k8xC	a
hypnoterapii	hypnoterapie	k1gFnSc4	hypnoterapie
Miltona	Milton	k1gMnSc2	Milton
Ericksona	Erickson	k1gMnSc2	Erickson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technika	technikum	k1gNnPc1	technikum
konstelací	konstelace	k1gFnPc2	konstelace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rodinné	rodinný	k2eAgFnSc2d1	rodinná
konstelace	konstelace	k1gFnSc2	konstelace
formou	forma	k1gFnSc7	forma
semináře	seminář	k1gInPc4	seminář
===	===	k?	===
</s>
</p>
<p>
<s>
Technika	technika	k1gFnSc1	technika
konstelací	konstelace	k1gFnPc2	konstelace
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
sestavení	sestavení	k1gNnSc6	sestavení
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
"	"	kIx"	"
<g/>
modelu	model	k1gInSc2	model
<g/>
"	"	kIx"	"
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
vztahu	vztah	k1gInSc2	vztah
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
systému	systém	k1gInSc2	systém
ze	z	k7c2	z
"	"	kIx"	"
<g/>
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
"	"	kIx"	"
vybraných	vybraný	k2eAgMnPc2d1	vybraný
z	z	k7c2	z
ostatních	ostatní	k2eAgMnPc2d1	ostatní
účastníků	účastník	k1gMnPc2	účastník
semináře	seminář	k1gInSc2	seminář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klient	klient	k1gMnSc1	klient
terapeutovi	terapeut	k1gMnSc3	terapeut
popíše	popsat	k5eAaPmIp3nS	popsat
podstatu	podstata	k1gFnSc4	podstata
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
konstelace	konstelace	k1gFnPc4	konstelace
přivedl	přivést	k5eAaPmAgMnS	přivést
<g/>
.	.	kIx.	.
</s>
<s>
Terapeut	terapeut	k1gMnSc1	terapeut
klientovi	klient	k1gMnSc3	klient
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
příběhu	příběh	k1gInSc2	příběh
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
konstelaci	konstelace	k1gFnSc6	konstelace
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
klient	klient	k1gMnSc1	klient
si	se	k3xPyFc3	se
mezi	mezi	k7c7	mezi
účastníky	účastník	k1gMnPc7	účastník
semináře	seminář	k1gInSc2	seminář
vybere	vybrat	k5eAaPmIp3nS	vybrat
takové	takový	k3xDgNnSc1	takový
zástupce	zástupce	k1gMnSc1	zástupce
skutečných	skutečný	k2eAgFnPc2d1	skutečná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mu	on	k3xPp3gMnSc3	on
dotyčné	dotyčný	k2eAgFnSc6d1	dotyčná
pocitově	pocitově	k6eAd1	pocitově
nejvíc	hodně	k6eAd3	hodně
připomínají	připomínat	k5eAaImIp3nP	připomínat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
osobních	osobní	k2eAgInPc6d1	osobní
příbězích	příběh	k1gInPc6	příběh
jsou	být	k5eAaImIp3nP	být
důležití	důležitý	k2eAgMnPc1d1	důležitý
nejen	nejen	k6eAd1	nejen
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
,	,	kIx,	,
prarodiče	prarodič	k1gMnPc4	prarodič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
vzdálenější	vzdálený	k2eAgMnSc1d2	vzdálenější
příbuzní	příbuzný	k1gMnPc1	příbuzný
nebo	nebo	k8xC	nebo
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
nějak	nějak	k6eAd1	nějak
spjatí	spjatý	k2eAgMnPc1d1	spjatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
mívají	mívat	k5eAaImIp3nP	mívat
zvlášť	zvlášť	k6eAd1	zvlášť
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
rodinní	rodinní	k2eAgMnPc1d1	rodinní
příslušníci	příslušník	k1gMnPc1	příslušník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
vědomí	vědomí	k1gNnSc2	vědomí
rodiny	rodina	k1gFnSc2	rodina
vytěsněni	vytěsnit	k5eAaPmNgMnP	vytěsnit
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
zapomnělo	zapomenout	k5eAaPmAgNnS	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
potracené	potracený	k2eAgNnSc1d1	potracené
nebo	nebo	k8xC	nebo
brzy	brzy	k6eAd1	brzy
zemřelé	zemřelý	k2eAgFnPc1d1	zemřelá
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
nežádoucí	žádoucí	k2eNgMnPc1d1	nežádoucí
členové	člen	k1gMnPc1	člen
systému	systém	k1gInSc2	systém
či	či	k8xC	či
příbuzní	příbuzný	k1gMnPc1	příbuzný
s	s	k7c7	s
tragickým	tragický	k2eAgInSc7d1	tragický
osudem	osud	k1gInSc7	osud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klient	klient	k1gMnSc1	klient
si	se	k3xPyFc3	se
rovněž	rovněž	k9	rovněž
zvolí	zvolit	k5eAaPmIp3nS	zvolit
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
zastupovat	zastupovat	k5eAaImF	zastupovat
jeho	jeho	k3xOp3gFnSc4	jeho
samého	samý	k3xTgInSc2	samý
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vybrané	vybraný	k2eAgFnSc3d1	vybraná
zástupce	zástupka	k1gFnSc3	zástupka
klient	klient	k1gMnSc1	klient
rozestaví	rozestavit	k5eAaPmIp3nS	rozestavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
rozestavili	rozestavit	k5eAaPmAgMnP	rozestavit
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
<g/>
.	.	kIx.	.
</s>
<s>
Výchozí	výchozí	k2eAgNnSc1d1	výchozí
rozestavení	rozestavení	k1gNnSc1	rozestavení
tedy	tedy	k9	tedy
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gInSc3	jeho
vnitřnímu	vnitřní	k2eAgInSc3d1	vnitřní
obrazu	obraz	k1gInSc3	obraz
dotyčného	dotyčný	k2eAgInSc2d1	dotyčný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
to	ten	k3xDgNnSc1	ten
musí	muset	k5eAaImIp3nS	muset
dělat	dělat	k5eAaImF	dělat
soustředěně	soustředěně	k6eAd1	soustředěně
a	a	k8xC	a
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
formální	formální	k2eAgNnSc4d1	formální
očekávání	očekávání	k1gNnSc4	očekávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zástupci	zástupce	k1gMnPc1	zástupce
zaujmou	zaujmout	k5eAaPmIp3nP	zaujmout
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
začínají	začínat	k5eAaImIp3nP	začínat
projevovat	projevovat	k5eAaImF	projevovat
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
pocitové	pocitový	k2eAgFnPc4d1	pocitová
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
pocity	pocit	k1gInPc1	pocit
v	v	k7c6	v
konstelaci	konstelace	k1gFnSc6	konstelace
jsou	být	k5eAaImIp3nP	být
překvapivě	překvapivě	k6eAd1	překvapivě
autentické	autentický	k2eAgFnPc1d1	autentická
a	a	k8xC	a
silné	silný	k2eAgFnPc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Konstelace	konstelace	k1gFnPc1	konstelace
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
spatřit	spatřit	k5eAaPmF	spatřit
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
podstatě	podstata	k1gFnSc6	podstata
a	a	k8xC	a
často	často	k6eAd1	často
vynesou	vynést	k5eAaPmIp3nP	vynést
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
skrytou	skrytý	k2eAgFnSc4d1	skrytá
dynamiku	dynamika	k1gFnSc4	dynamika
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozorováním	pozorování	k1gNnSc7	pozorování
reakcí	reakce	k1gFnSc7	reakce
a	a	k8xC	a
dotazováním	dotazování	k1gNnSc7	dotazování
zástupců	zástupce	k1gMnPc2	zástupce
dostáváme	dostávat	k5eAaImIp1nP	dostávat
řadu	řada	k1gFnSc4	řada
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
systému	systém	k1gInSc6	systém
a	a	k8xC	a
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
systému	systém	k1gInSc2	systém
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vychází	vycházet	k5eAaImIp3nS	vycházet
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
v	v	k7c6	v
rozestavené	rozestavený	k2eAgFnSc6d1	rozestavená
konstelaci	konstelace	k1gFnSc6	konstelace
nechybí	chybit	k5eNaPmIp3nS	chybit
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
v	v	k7c6	v
systému	systém	k1gInSc6	systém
byl	být	k5eAaImAgInS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
<g/>
,	,	kIx,	,
či	či	k8xC	či
zda	zda	k8xS	zda
v	v	k7c6	v
systému	systém	k1gInSc6	systém
nepřevzal	převzít	k5eNaPmAgMnS	převzít
někdo	někdo	k3yInSc1	někdo
roli	role	k1gFnSc4	role
jiného	jiný	k2eAgMnSc4d1	jiný
člena	člen	k1gMnSc4	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Přeskupováním	přeskupování	k1gNnSc7	přeskupování
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
hledáním	hledání	k1gNnSc7	hledání
"	"	kIx"	"
<g/>
správného	správný	k2eAgNnSc2d1	správné
<g/>
"	"	kIx"	"
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
členy	člen	k1gInPc4	člen
a	a	k8xC	a
doplněním	doplnění	k1gNnSc7	doplnění
systému	systém	k1gInSc2	systém
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
vyloučené	vyloučený	k2eAgInPc4d1	vyloučený
anebo	anebo	k8xC	anebo
zapomenuté	zapomenutý	k2eAgMnPc4d1	zapomenutý
členy	člen	k1gMnPc4	člen
rodiny	rodina	k1gFnSc2	rodina
začíná	začínat	k5eAaImIp3nS	začínat
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
hojivý	hojivý	k2eAgInSc1d1	hojivý
účinek	účinek	k1gInSc1	účinek
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
nejen	nejen	k6eAd1	nejen
klient	klient	k1gMnSc1	klient
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
později	pozdě	k6eAd2	pozdě
celý	celý	k2eAgInSc1d1	celý
jeho	jeho	k3xOp3gInSc1	jeho
rodinný	rodinný	k2eAgInSc1d1	rodinný
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
účastníky	účastník	k1gMnPc4	účastník
semináře	seminář	k1gInSc2	seminář
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
"	"	kIx"	"
<g/>
práce	práce	k1gFnSc1	práce
<g/>
"	"	kIx"	"
v	v	k7c6	v
rolích	role	k1gFnPc6	role
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sama	sám	k3xTgFnSc1	sám
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
semináři	seminář	k1gInSc6	seminář
neobyčejně	obyčejně	k6eNd1	obyčejně
intenzívní	intenzívnět	k5eAaImIp3nS	intenzívnět
a	a	k8xC	a
přináší	přinášet	k5eAaImIp3nS	přinášet
vždy	vždy	k6eAd1	vždy
hluboké	hluboký	k2eAgInPc4d1	hluboký
prožitky	prožitek	k1gInPc4	prožitek
a	a	k8xC	a
překvapivé	překvapivý	k2eAgInPc4d1	překvapivý
náhledy	náhled	k1gInPc4	náhled
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konstelace	konstelace	k1gFnPc1	konstelace
v	v	k7c6	v
individuálním	individuální	k2eAgNnSc6d1	individuální
poradenství	poradenství	k1gNnSc6	poradenství
===	===	k?	===
</s>
</p>
<p>
<s>
Konstelace	konstelace	k1gFnSc1	konstelace
lze	lze	k6eAd1	lze
dělat	dělat	k5eAaImF	dělat
také	také	k9	také
jako	jako	k9	jako
individuální	individuální	k2eAgNnPc1d1	individuální
poradenství	poradenství	k1gNnPc1	poradenství
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
značkami	značka	k1gFnPc7	značka
místo	místo	k7c2	místo
osobních	osobní	k2eAgMnPc2d1	osobní
zástupců	zástupce	k1gMnPc2	zástupce
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
členy	člen	k1gMnPc4	člen
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
značky	značka	k1gFnSc2	značka
rozestaví	rozestavit	k5eAaPmIp3nS	rozestavit
na	na	k7c4	na
podlahu	podlaha	k1gFnSc4	podlaha
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
tam	tam	k6eAd1	tam
stáli	stát	k5eAaImAgMnP	stát
členové	člen	k1gMnPc1	člen
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
stoupnout	stoupnout	k5eAaPmF	stoupnout
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vcítil	vcítit	k5eAaPmAgInS	vcítit
do	do	k7c2	do
příslušných	příslušný	k2eAgFnPc2d1	příslušná
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formální	formální	k2eAgNnSc1d1	formální
dělení	dělení	k1gNnSc1	dělení
konstelací	konstelace	k1gFnPc2	konstelace
==	==	k?	==
</s>
</p>
<p>
<s>
Obecné	obecný	k2eAgInPc1d1	obecný
principy	princip	k1gInPc1	princip
formulované	formulovaný	k2eAgInPc1d1	formulovaný
Bertem	Bert	k1gInSc7	Bert
Hellingerem	Hellinger	k1gInSc7	Hellinger
a	a	k8xC	a
používané	používaný	k2eAgInPc4d1	používaný
při	při	k7c6	při
konstelacích	konstelace	k1gFnPc6	konstelace
lze	lze	k6eAd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
na	na	k7c4	na
rodinné	rodinný	k2eAgInPc4d1	rodinný
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgInPc4d1	pracovní
či	či	k8xC	či
školní	školní	k2eAgInPc4d1	školní
kolektivy	kolektiv	k1gInPc4	kolektiv
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
organizační	organizační	k2eAgInPc4d1	organizační
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgNnSc2d3	veliký
rozšíření	rozšíření	k1gNnSc2	rozšíření
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
původní	původní	k2eAgFnPc4d1	původní
aplikace	aplikace	k1gFnPc4	aplikace
metody	metoda	k1gFnSc2	metoda
na	na	k7c4	na
rodinné	rodinný	k2eAgInPc4d1	rodinný
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rodinné	rodinný	k2eAgFnSc2d1	rodinná
konstelace	konstelace	k1gFnSc2	konstelace
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zacílení	zacílení	k1gNnSc2	zacílení
lze	lze	k6eAd1	lze
Rodinné	rodinný	k2eAgFnSc2d1	rodinná
konstelace	konstelace	k1gFnSc2	konstelace
dále	daleko	k6eAd2	daleko
formálně	formálně	k6eAd1	formálně
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
část	část	k1gFnSc4	část
rodinného	rodinný	k2eAgInSc2d1	rodinný
systému	systém	k1gInSc2	systém
řeší	řešit	k5eAaImIp3nS	řešit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Konstelace	konstelace	k1gFnSc2	konstelace
původního	původní	k2eAgInSc2d1	původní
rodinného	rodinný	k2eAgInSc2d1	rodinný
systému	systém	k1gInSc2	systém
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
rodinných	rodinný	k2eAgFnPc2d1	rodinná
konstelací	konstelace	k1gFnPc2	konstelace
původního	původní	k2eAgInSc2d1	původní
rodinného	rodinný	k2eAgInSc2d1	rodinný
systému	systém	k1gInSc2	systém
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pochopení	pochopení	k1gNnSc4	pochopení
vztahů	vztah	k1gInPc2	vztah
členů	člen	k1gMnPc2	člen
původní	původní	k2eAgFnSc2d1	původní
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
,	,	kIx,	,
rodiče	rodič	k1gMnPc1	rodič
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
,	,	kIx,	,
prarodiče	prarodič	k1gMnPc1	prarodič
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
sourozenci	sourozenec	k1gMnPc1	sourozenec
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
starší	starý	k2eAgFnPc1d2	starší
generace	generace	k1gFnPc1	generace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hellingera	Hellingero	k1gNnSc2	Hellingero
je	být	k5eAaImIp3nS	být
naše	náš	k3xOp1gFnSc1	náš
životní	životní	k2eAgFnSc1d1	životní
cesta	cesta	k1gFnSc1	cesta
silně	silně	k6eAd1	silně
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
událostmi	událost	k1gFnPc7	událost
a	a	k8xC	a
vzorci	vzorec	k1gInPc7	vzorec
z	z	k7c2	z
rodinné	rodinný	k2eAgFnSc2d1	rodinná
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nevědomě	vědomě	k6eNd1	vědomě
přenášejí	přenášet	k5eAaImIp3nP	přenášet
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Konstelace	konstelace	k1gFnPc4	konstelace
partnerského	partnerský	k2eAgInSc2d1	partnerský
systému	systém	k1gInSc2	systém
====	====	k?	====
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
vztahovým	vztahový	k2eAgInSc7d1	vztahový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
lze	lze	k6eAd1	lze
lépe	dobře	k6eAd2	dobře
porozumět	porozumět	k5eAaPmF	porozumět
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
techniky	technika	k1gFnSc2	technika
Rodinných	rodinný	k2eAgFnPc2d1	rodinná
konstelací	konstelace	k1gFnPc2	konstelace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
partnerského	partnerský	k2eAgInSc2d1	partnerský
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
patří	patřit	k5eAaImIp3nS	patřit
nejen	nejen	k6eAd1	nejen
partner	partner	k1gMnSc1	partner
nebo	nebo	k8xC	nebo
partnerka	partnerka	k1gFnSc1	partnerka
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
dřívější	dřívější	k2eAgMnPc1d1	dřívější
"	"	kIx"	"
<g/>
důležití	důležitý	k2eAgMnPc1d1	důležitý
<g/>
"	"	kIx"	"
partneři	partner	k1gMnPc1	partner
a	a	k8xC	a
případně	případně	k6eAd1	případně
osoby	osoba	k1gFnPc1	osoba
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Systemické	Systemický	k2eAgFnSc2d1	Systemická
konstelace	konstelace	k1gFnSc2	konstelace
===	===	k?	===
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
Systemické	Systemický	k2eAgFnSc2d1	Systemická
konstelace	konstelace	k1gFnSc2	konstelace
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
obecnější	obecní	k2eAgFnPc4d2	obecní
aplikace	aplikace	k1gFnPc4	aplikace
konstelačních	konstelační	k2eAgInPc2d1	konstelační
postupů	postup	k1gInPc2	postup
na	na	k7c4	na
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
systém	systém	k1gInSc4	systém
tvořený	tvořený	k2eAgInSc4d1	tvořený
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
ovlivňujícími	ovlivňující	k2eAgInPc7d1	ovlivňující
členy	člen	k1gInPc7	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
a	a	k8xC	a
organizační	organizační	k2eAgFnSc1d1	organizační
konstelace	konstelace	k1gFnSc1	konstelace
===	===	k?	===
</s>
</p>
<p>
<s>
Konstelace	konstelace	k1gFnSc1	konstelace
lze	lze	k6eAd1	lze
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
použít	použít	k5eAaPmF	použít
i	i	k9	i
na	na	k7c4	na
odkrytí	odkrytí	k1gNnSc4	odkrytí
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
dynamiky	dynamika	k1gFnSc2	dynamika
pracovních	pracovní	k2eAgInPc2d1	pracovní
kolektivů	kolektiv	k1gInPc2	kolektiv
<g/>
,	,	kIx,	,
organizačních	organizační	k2eAgInPc2d1	organizační
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
podnikové	podnikový	k2eAgFnPc1d1	podniková
konstelace	konstelace	k1gFnPc1	konstelace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
různých	různý	k2eAgInPc2d1	různý
dalších	další	k2eAgInPc2d1	další
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Typologie	typologie	k1gFnSc1	typologie
konstelací	konstelace	k1gFnPc2	konstelace
===	===	k?	===
</s>
</p>
<p>
<s>
Konstelace	konstelace	k1gFnSc1	konstelace
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
pronikají	pronikat	k5eAaImIp3nP	pronikat
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
oblastí	oblast	k1gFnPc2	oblast
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
utřídění	utřídění	k1gNnSc4	utřídění
můžeme	moct	k5eAaImIp1nP	moct
systemické	systemický	k2eAgFnPc4d1	systemická
konstelace	konstelace	k1gFnPc4	konstelace
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rodinné	rodinný	k2eAgFnPc1d1	rodinná
konstelace	konstelace	k1gFnPc1	konstelace
</s>
</p>
<p>
<s>
Partnerské	partnerský	k2eAgFnPc1d1	partnerská
konstelace	konstelace	k1gFnPc1	konstelace
</s>
</p>
<p>
<s>
Experimentální	experimentální	k2eAgFnPc1d1	experimentální
formy	forma	k1gFnPc1	forma
(	(	kIx(	(
<g/>
šamanské	šamanský	k2eAgFnPc1d1	šamanská
konstelace	konstelace	k1gFnPc1	konstelace
<g/>
,	,	kIx,	,
konstelace	konstelace	k1gFnPc1	konstelace
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
voice	voice	k1gMnSc1	voice
dialogue	dialogu	k1gFnSc2	dialogu
–	–	k?	–
konstelace	konstelace	k1gFnSc2	konstelace
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
konstelace	konstelace	k1gFnSc1	konstelace
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
symptomů	symptom	k1gInPc2	symptom
<g/>
,	,	kIx,	,
konstelace	konstelace	k1gFnSc1	konstelace
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podnikové	podnikový	k2eAgFnPc1d1	podniková
a	a	k8xC	a
organizační	organizační	k2eAgFnSc1d1	organizační
konstelace	konstelace	k1gFnSc1	konstelace
</s>
</p>
<p>
<s>
Politické	politický	k2eAgFnPc1d1	politická
konstelace	konstelace	k1gFnPc1	konstelace
</s>
</p>
<p>
<s>
Konstelace	konstelace	k1gFnSc1	konstelace
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
Provádění	provádění	k1gNnSc1	provádění
rodinných	rodinný	k2eAgFnPc2d1	rodinná
či	či	k8xC	či
systemických	systemický	k2eAgFnPc2d1	systemická
konstelací	konstelace	k1gFnPc2	konstelace
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
licencováno	licencován	k2eAgNnSc1d1	licencováno
některými	některý	k3yIgFnPc7	některý
psychoterapeutickými	psychoterapeutický	k2eAgFnPc7d1	psychoterapeutická
společnostmi	společnost	k1gFnPc7	společnost
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
ale	ale	k9	ale
o	o	k7c4	o
metodu	metoda	k1gFnSc4	metoda
spornou	sporný	k2eAgFnSc4d1	sporná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
četné	četný	k2eAgMnPc4d1	četný
kritiky	kritik	k1gMnPc4	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
metoda	metoda	k1gFnSc1	metoda
fenomenologická	fenomenologický	k2eAgFnSc1d1	fenomenologická
<g/>
,	,	kIx,	,
nekauzální	kauzální	k2eNgFnSc1d1	nekauzální
<g/>
,	,	kIx,	,
subjektivistická	subjektivistický	k2eAgFnSc1d1	subjektivistická
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
účinnost	účinnost	k1gFnSc1	účinnost
nelze	lze	k6eNd1	lze
standardními	standardní	k2eAgFnPc7d1	standardní
metodami	metoda	k1gFnPc7	metoda
ověřovat	ověřovat	k5eAaImF	ověřovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hodnocení	hodnocení	k1gNnSc2	hodnocení
Českého	český	k2eAgInSc2d1	český
klubu	klub	k1gInSc2	klub
skeptiků	skeptik	k1gMnPc2	skeptik
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
pseudovědeckých	pseudovědecký	k2eAgFnPc2d1	pseudovědecká
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
léčbu	léčba	k1gFnSc4	léčba
zprostředkovávat	zprostředkovávat	k5eAaImF	zprostředkovávat
jakási	jakýsi	k3yIgFnSc1	jakýsi
"	"	kIx"	"
<g/>
energie	energie	k1gFnSc1	energie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
provozují	provozovat	k5eAaImIp3nP	provozovat
nekvalifikované	kvalifikovaný	k2eNgFnPc1d1	nekvalifikovaná
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nabízejí	nabízet	k5eAaImIp3nP	nabízet
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
odborní	odbornět	k5eAaImIp3nP	odbornět
lékaři	lékař	k1gMnPc1	lékař
a	a	k8xC	a
psychoterapeuti	psychoterapeut	k1gMnPc1	psychoterapeut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Wilfried	Wilfried	k1gMnSc1	Wilfried
Nelles	Nelles	k1gMnSc1	Nelles
<g/>
:	:	kIx,	:
Amorův	Amorův	k2eAgInSc1d1	Amorův
šíp	šíp	k1gInSc1	šíp
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
touha	touha	k1gFnSc1	touha
a	a	k8xC	a
růst	růst	k1gInSc1	růst
v	v	k7c6	v
partnerském	partnerský	k2eAgInSc6d1	partnerský
vztahu	vztah	k1gInSc6	vztah
<g/>
,	,	kIx,	,
Agamé	Agamý	k2eAgInPc1d1	Agamý
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
<g/>
:	:	kIx,	:
In	In	k1gFnSc1	In
guten	guten	k1gInSc1	guten
wie	wie	k?	wie
in	in	k?	in
schlechten	schlechten	k2eAgMnSc1d1	schlechten
Zeiten	Zeiten	k2eAgMnSc1d1	Zeiten
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-904016-0-0	[number]	k4	978-80-904016-0-0
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rodinné	rodinný	k2eAgFnSc2d1	rodinná
konstelace	konstelace	k1gFnSc2	konstelace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
