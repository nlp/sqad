<s>
Strunatci	strunatec	k1gMnPc1	strunatec
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
druhoústé	druhoústý	k2eAgNnSc4d1	druhoústý
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
skupinu	skupina	k1gFnSc4	skupina
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
právě	právě	k9	právě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
typu	typ	k1gInSc2	typ
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
