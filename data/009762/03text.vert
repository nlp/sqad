<p>
<s>
Strunatci	strunatec	k1gMnPc1	strunatec
(	(	kIx(	(
<g/>
Chordata	Chordat	k2eAgFnSc1d1	Chordat
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
kmen	kmen	k1gInSc4	kmen
druhoústých	druhoústý	k2eAgMnPc2d1	druhoústý
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
obratlovce	obratlovec	k1gMnPc4	obratlovec
a	a	k8xC	a
několik	několik	k4yIc4	několik
blízce	blízce	k6eAd1	blízce
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
skupin	skupina	k1gFnPc2	skupina
bezobratlých	bezobratlý	k2eAgFnPc2d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dvoustranně	dvoustranně	k6eAd1	dvoustranně
souměrní	souměrný	k2eAgMnPc1d1	souměrný
<g/>
,	,	kIx,	,
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
období	období	k1gNnSc1	období
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
mají	mít	k5eAaImIp3nP	mít
strunu	struna	k1gFnSc4	struna
hřbetní	hřbetní	k2eAgFnSc4d1	hřbetní
<g/>
,	,	kIx,	,
nervovou	nervový	k2eAgFnSc4d1	nervová
trubici	trubice	k1gFnSc4	trubice
<g/>
,	,	kIx,	,
žaberní	žaberní	k2eAgFnPc4d1	žaberní
štěrbiny	štěrbina	k1gFnPc4	štěrbina
<g/>
,	,	kIx,	,
endostyl	endostyl	k1gInSc4	endostyl
a	a	k8xC	a
svalnatou	svalnatý	k2eAgFnSc4d1	svalnatá
ocasní	ocasní	k2eAgFnSc4d1	ocasní
část	část	k1gFnSc4	část
až	až	k9	až
za	za	k7c7	za
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nervová	nervový	k2eAgFnSc1d1	nervová
trubice	trubice	k1gFnSc1	trubice
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
hřbetní	hřbetní	k2eAgFnSc6d1	hřbetní
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
struna	struna	k1gFnSc1	struna
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
probíhá	probíhat	k5eAaImIp3nS	probíhat
centrálně	centrálně	k6eAd1	centrálně
a	a	k8xC	a
srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
na	na	k7c6	na
břišní	břišní	k2eAgFnSc6d1	břišní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strunatci	strunatec	k1gMnPc1	strunatec
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
kambria	kambrium	k1gNnSc2	kambrium
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
530	[number]	k4	530
miliony	milion	k4xCgInPc7	milion
lety	let	k1gInPc7	let
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
známe	znát	k5eAaImIp1nP	znát
asi	asi	k9	asi
45	[number]	k4	45
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
strunatců	strunatec	k1gMnPc2	strunatec
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
podkmeny	podkmen	k1gInPc4	podkmen
<g/>
:	:	kIx,	:
pláštěnce	pláštěnec	k1gMnPc4	pláštěnec
<g/>
,	,	kIx,	,
bezlebečné	bezlebeční	k1gMnPc4	bezlebeční
a	a	k8xC	a
obratlovce	obratlovec	k1gMnPc4	obratlovec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
blíže	blízce	k6eAd2	blízce
obratlovcům	obratlovec	k1gMnPc3	obratlovec
mají	mít	k5eAaImIp3nP	mít
pláštěnci	pláštěnec	k1gMnSc3	pláštěnec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
larválním	larvální	k2eAgNnSc6d1	larvální
stádiu	stádium	k1gNnSc6	stádium
hřbetní	hřbetní	k2eAgFnSc4d1	hřbetní
strunu	struna	k1gFnSc4	struna
a	a	k8xC	a
nervový	nervový	k2eAgInSc4d1	nervový
provazec	provazec	k1gInSc4	provazec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
bezlebeční	bezlebeční	k1gMnPc1	bezlebeční
mají	mít	k5eAaImIp3nP	mít
jak	jak	k6eAd1	jak
hřbetní	hřbetní	k2eAgFnSc4d1	hřbetní
strunu	struna	k1gFnSc4	struna
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
nervový	nervový	k2eAgInSc4d1	nervový
provazec	provazec	k1gInSc4	provazec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
páteře	páteř	k1gFnSc2	páteř
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
sliznatek	sliznatka	k1gFnPc2	sliznatka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nervový	nervový	k2eAgInSc4d1	nervový
provazec	provazec	k1gInSc4	provazec
obemknut	obemknout	k5eAaPmNgInS	obemknout
chrupavčitými	chrupavčitý	k2eAgInPc7d1	chrupavčitý
nebo	nebo	k8xC	nebo
kostěnými	kostěný	k2eAgInPc7d1	kostěný
obratli	obratel	k1gInPc7	obratel
a	a	k8xC	a
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
struna	struna	k1gFnSc1	struna
je	být	k5eAaImIp3nS	být
zásadně	zásadně	k6eAd1	zásadně
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
strunatce	strunatec	k1gMnPc4	strunatec
patří	patřit	k5eAaImIp3nS	patřit
vedle	vedle	k7c2	vedle
samotného	samotný	k2eAgMnSc2d1	samotný
člověka	člověk	k1gMnSc2	člověk
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
obecně	obecně	k6eAd1	obecně
známých	známý	k2eAgNnPc2d1	známé
a	a	k8xC	a
hospodářsky	hospodářsky	k6eAd1	hospodářsky
významných	významný	k2eAgMnPc2d1	významný
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
všichni	všechen	k3xTgMnPc1	všechen
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Strunatci	strunatec	k1gMnPc1	strunatec
jsou	být	k5eAaImIp3nP	být
mnohobuněční	mnohobuněční	k2eAgMnPc1d1	mnohobuněční
živočichové	živočich	k1gMnPc1	živočich
s	s	k7c7	s
dokonale	dokonale	k6eAd1	dokonale
vyvinutým	vyvinutý	k2eAgNnSc7d1	vyvinuté
tělem	tělo	k1gNnSc7	tělo
(	(	kIx(	(
<g/>
členěným	členěný	k2eAgInSc7d1	členěný
na	na	k7c4	na
orgány	orgán	k1gInPc4	orgán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
dvoustranně	dvoustranně	k6eAd1	dvoustranně
souměrné	souměrný	k2eAgNnSc1d1	souměrné
(	(	kIx(	(
<g/>
bilaterálně	bilaterálně	k6eAd1	bilaterálně
symetrické	symetrický	k2eAgFnPc1d1	symetrická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
třemi	tři	k4xCgInPc7	tři
zárodečnými	zárodečný	k2eAgInPc7d1	zárodečný
listy	list	k1gInPc7	list
(	(	kIx(	(
<g/>
ektoderm	ektoderm	k1gInSc1	ektoderm
<g/>
,	,	kIx,	,
mezoderm	mezoderm	k1gInSc1	mezoderm
<g/>
,	,	kIx,	,
entoderm	entoderm	k1gInSc1	entoderm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různorodá	různorodý	k2eAgFnSc1d1	různorodá
a	a	k8xC	a
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
centimetru	centimetr	k1gInSc2	centimetr
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
a	a	k8xC	a
hlaváčovité	hlaváčovitý	k2eAgFnPc1d1	hlaváčovitý
ryby	ryba	k1gFnPc1	ryba
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
desítky	desítka	k1gFnPc4	desítka
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
plejtvák	plejtvák	k1gMnSc1	plejtvák
obrovský	obrovský	k2eAgMnSc1d1	obrovský
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
živočichy	živočich	k1gMnPc7	živočich
mají	mít	k5eAaImIp3nP	mít
strunatci	strunatec	k1gMnPc1	strunatec
některé	některý	k3yIgInPc4	některý
společné	společný	k2eAgInPc4d1	společný
rysy	rys	k1gInPc4	rys
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
embryonálním	embryonální	k2eAgNnSc6d1	embryonální
stadiu	stadion	k1gNnSc6	stadion
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
trávicí	trávicí	k2eAgFnSc2d1	trávicí
soustavy	soustava	k1gFnSc2	soustava
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
entodermu	entoderm	k1gInSc2	entoderm
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
začátek	začátek	k1gInSc4	začátek
a	a	k8xC	a
konec	konec	k1gInSc4	konec
z	z	k7c2	z
ektodermu	ektoderm	k1gInSc2	ektoderm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Anatomie	anatomie	k1gFnSc2	anatomie
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
definující	definující	k2eAgMnSc1d1	definující
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
uvádí	uvádět	k5eAaImIp3nS	uvádět
čtyři	čtyři	k4xCgFnPc4	čtyři
nebo	nebo	k8xC	nebo
pět	pět	k4xCc4	pět
anatomických	anatomický	k2eAgInPc2d1	anatomický
znaků	znak	k1gInPc2	znak
přítomných	přítomný	k1gMnPc2	přítomný
vždy	vždy	k6eAd1	vždy
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
embryonálním	embryonální	k2eAgNnSc6d1	embryonální
stadiu	stadion	k1gNnSc6	stadion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Struna	struna	k1gFnSc1	struna
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
(	(	kIx(	(
<g/>
chorda	chorda	k1gFnSc1	chorda
dorsalis	dorsalis	k1gFnSc2	dorsalis
<g/>
,	,	kIx,	,
též	též	k9	též
notochord	notochord	k6eAd1	notochord
<g/>
)	)	kIx)	)
–	–	k?	–
opora	opora	k1gFnSc1	opora
těla	tělo	k1gNnSc2	tělo
tvořená	tvořený	k2eAgFnSc1d1	tvořená
silně	silně	k6eAd1	silně
vakuolizovanými	vakuolizovaný	k2eAgFnPc7d1	vakuolizovaný
buňkami	buňka	k1gFnPc7	buňka
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
entodernu	entoderna	k1gFnSc4	entoderna
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
trávicí	trávicí	k2eAgFnSc7d1	trávicí
soustavou	soustava	k1gFnSc7	soustava
a	a	k8xC	a
pod	pod	k7c7	pod
nervovou	nervový	k2eAgFnSc7d1	nervová
trubicí	trubice	k1gFnSc7	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
podélnou	podélný	k2eAgFnSc4d1	podélná
osu	osa	k1gFnSc4	osa
těla	tělo	k1gNnSc2	tělo
směřující	směřující	k2eAgFnSc1d1	směřující
od	od	k7c2	od
hlavy	hlava	k1gFnSc2	hlava
až	až	k9	až
k	k	k7c3	k
ocasu	ocas	k1gInSc3	ocas
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pokročilejších	pokročilý	k2eAgMnPc2d2	pokročilejší
strunatců	strunatec	k1gMnPc2	strunatec
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
zaniká	zanikat	k5eAaImIp3nS	zanikat
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
funkci	funkce	k1gFnSc4	funkce
přejímá	přejímat	k5eAaImIp3nS	přejímat
chrupavka	chrupavka	k1gFnSc1	chrupavka
či	či	k8xC	či
kost	kost	k1gFnSc1	kost
(	(	kIx(	(
<g/>
páteř	páteř	k1gFnSc1	páteř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
trubice	trubice	k1gFnSc1	trubice
(	(	kIx(	(
<g/>
neurocoel	neurocoel	k1gInSc1	neurocoel
<g/>
)	)	kIx)	)
–	–	k?	–
trubicovitá	trubicovitý	k2eAgFnSc1d1	trubicovitá
nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
na	na	k7c6	na
hřbetní	hřbetní	k2eAgFnSc6d1	hřbetní
straně	strana	k1gFnSc6	strana
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
ektodermálního	ektodermální	k2eAgInSc2d1	ektodermální
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgMnPc2d1	mnohý
strunatců	strunatec	k1gMnPc2	strunatec
se	se	k3xPyFc4	se
v	v	k7c6	v
hlavové	hlavový	k2eAgFnSc6d1	hlavová
části	část	k1gFnSc6	část
z	z	k7c2	z
nervové	nervový	k2eAgFnSc2d1	nervová
trubice	trubice	k1gFnSc2	trubice
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
je	být	k5eAaImIp3nS	být
mícha	mícha	k1gFnSc1	mícha
<g/>
;	;	kIx,	;
centrální	centrální	k2eAgInSc1d1	centrální
nervový	nervový	k2eAgInSc1d1	nervový
systém	systém	k1gInSc1	systém
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
poměrně	poměrně	k6eAd1	poměrně
složité	složitý	k2eAgNnSc1d1	složité
smyslové	smyslový	k2eAgNnSc1d1	smyslové
vnímání	vnímání	k1gNnSc1	vnímání
či	či	k8xC	či
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žaberní	žaberní	k2eAgFnPc1d1	žaberní
štěrbiny	štěrbina	k1gFnPc1	štěrbina
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
váčky	váček	k1gInPc1	váček
–	–	k?	–
v	v	k7c6	v
hltanu	hltan	k1gInSc6	hltan
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
během	během	k7c2	během
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Procházejí	procházet	k5eAaImIp3nP	procházet
skrze	skrze	k?	skrze
stěnu	stěna	k1gFnSc4	stěna
trávicí	trávicí	k2eAgFnSc2d1	trávicí
trubice	trubice	k1gFnSc2	trubice
a	a	k8xC	a
směřují	směřovat	k5eAaImIp3nP	směřovat
ven	ven	k6eAd1	ven
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nižších	nízký	k2eAgMnPc2d2	nižší
strunatců	strunatec	k1gMnPc2	strunatec
slouží	sloužit	k5eAaImIp3nS	sloužit
mimo	mimo	k7c4	mimo
výměny	výměna	k1gFnPc4	výměna
plynů	plyn	k1gInPc2	plyn
i	i	k8xC	i
k	k	k7c3	k
filtrování	filtrování	k1gNnSc3	filtrování
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
redukovány	redukován	k2eAgFnPc1d1	redukována
a	a	k8xC	a
zřejmé	zřejmý	k2eAgFnPc1d1	zřejmá
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
jen	jen	k9	jen
v	v	k7c6	v
embryonální	embryonální	k2eAgFnSc6d1	embryonální
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
ocas	ocas	k1gInSc1	ocas
–	–	k?	–
nezasahuje	zasahovat	k5eNaImIp3nS	zasahovat
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
trávicí	trávicí	k2eAgFnSc1d1	trávicí
trubice	trubice	k1gFnSc2	trubice
(	(	kIx(	(
<g/>
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
celý	celý	k2eAgMnSc1d1	celý
až	až	k6eAd1	až
za	za	k7c7	za
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
postanální	postanální	k2eAgNnSc1d1	postanální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyztužen	vyztužit	k5eAaPmNgInS	vyztužit
chordou	chorda	k1gFnSc7	chorda
či	či	k8xC	či
páteří	páteř	k1gFnSc7	páteř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Endostyl	Endostyl	k1gInSc1	Endostyl
(	(	kIx(	(
<g/>
též	též	k9	též
podhltanová	podhltanový	k2eAgFnSc1d1	podhltanový
žláza	žláza	k1gFnSc1	žláza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
štítná	štítný	k2eAgFnSc1d1	štítná
žláza	žláza	k1gFnSc1	žláza
–	–	k?	–
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
se	se	k3xPyFc4	se
jako	jako	k9	jako
rýha	rýha	k1gFnSc1	rýha
v	v	k7c6	v
břišní	břišní	k2eAgFnSc6d1	břišní
stěně	stěna	k1gFnSc6	stěna
hltanu	hltan	k1gInSc2	hltan
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
produkuje	produkovat	k5eAaImIp3nS	produkovat
hlen	hlen	k1gInSc1	hlen
umožňující	umožňující	k2eAgInSc1d1	umožňující
lepší	dobrý	k2eAgInSc1d2	lepší
posun	posun	k1gInSc1	posun
potravy	potrava	k1gFnSc2	potrava
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
představuje	představovat	k5eAaImIp3nS	představovat
úložiště	úložiště	k1gNnSc1	úložiště
jódu	jód	k1gInSc2	jód
<g/>
.	.	kIx.	.
<g/>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
další	další	k2eAgInPc4d1	další
rysy	rys	k1gInPc4	rys
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obvykle	obvykle	k6eAd1	obvykle
platí	platit	k5eAaImIp3nP	platit
pro	pro	k7c4	pro
strunatce	strunatec	k1gMnPc4	strunatec
<g/>
.	.	kIx.	.
</s>
<s>
Vakovité	vakovitý	k2eAgNnSc1d1	vakovité
srdce	srdce	k1gNnSc1	srdce
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
stahu	stah	k1gInSc2	stah
schopná	schopný	k2eAgFnSc1d1	schopná
céva	céva	k1gFnSc1	céva
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
břišní	břišní	k2eAgFnSc6d1	břišní
straně	strana	k1gFnSc6	strana
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
pumpuje	pumpovat	k5eAaImIp3nS	pumpovat
krev	krev	k1gFnSc4	krev
nejprve	nejprve	k6eAd1	nejprve
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hlavě	hlava	k1gFnSc3	hlava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
strunatců	strunatec	k1gMnPc2	strunatec
je	být	k5eAaImIp3nS	být
cévní	cévní	k2eAgFnSc1d1	cévní
soustava	soustava	k1gFnSc1	soustava
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
žábrami	žábry	k1gFnPc7	žábry
<g/>
:	:	kIx,	:
odkysličená	odkysličený	k2eAgFnSc1d1	odkysličená
krev	krev	k1gFnSc1	krev
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
či	či	k8xC	či
do	do	k7c2	do
kontraktilních	kontraktilní	k2eAgFnPc2d1	kontraktilní
cév	céva	k1gFnPc2	céva
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
pumpována	pumpovat	k5eAaImNgFnS	pumpovat
do	do	k7c2	do
žaber	žábry	k1gFnPc2	žábry
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
odvádí	odvádět	k5eAaImIp3nS	odvádět
aortou	aorta	k1gFnSc7	aorta
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
venózní	venózní	k2eAgNnSc1d1	venózní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
plic	plíce	k1gFnPc2	plíce
u	u	k7c2	u
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
i	i	k9	i
toto	tento	k3xDgNnSc1	tento
změnilo	změnit	k5eAaPmAgNnS	změnit
a	a	k8xC	a
např.	např.	kA	např.
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
přiváděna	přiváděn	k2eAgFnSc1d1	přiváděna
i	i	k9	i
krev	krev	k1gFnSc1	krev
okysličená	okysličený	k2eAgFnSc1d1	okysličená
<g/>
.	.	kIx.	.
</s>
<s>
Trávicí	trávicí	k2eAgFnSc1d1	trávicí
trubice	trubice	k1gFnSc1	trubice
je	být	k5eAaImIp3nS	být
trubicovitá	trubicovitý	k2eAgFnSc1d1	trubicovitá
a	a	k8xC	a
zakládá	zakládat	k5eAaImIp3nS	zakládat
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Světločivné	světločivný	k2eAgFnPc1d1	světločivná
buňky	buňka	k1gFnPc1	buňka
oka	oko	k1gNnSc2	oko
(	(	kIx(	(
<g/>
sítnice	sítnice	k1gFnSc2	sítnice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
z	z	k7c2	z
ektodermu	ektoderm	k1gInSc2	ektoderm
centrální	centrální	k2eAgFnSc2d1	centrální
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
z	z	k7c2	z
epidermis	epidermis	k1gFnSc2	epidermis
<g/>
.	.	kIx.	.
<g/>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
příbuzných	příbuzný	k1gMnPc2	příbuzný
strunatců	strunatec	k1gMnPc2	strunatec
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
ze	z	k7c2	z
znaků	znak	k1gInPc2	znak
uvedených	uvedený	k2eAgInPc2d1	uvedený
výše	vysoce	k6eAd2	vysoce
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
obměnách	obměna	k1gFnPc6	obměna
nalézány	nalézat	k5eAaImNgFnP	nalézat
i	i	k9	i
např.	např.	kA	např.
u	u	k7c2	u
polostrunatců	polostrunatec	k1gMnPc2	polostrunatec
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
například	například	k6eAd1	například
něco	něco	k3yInSc4	něco
podobného	podobný	k2eAgNnSc2d1	podobné
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
struna	struna	k1gFnSc1	struna
hřbetní	hřbetní	k2eAgFnSc2d1	hřbetní
–	–	k?	–
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
říká	říkat	k5eAaImIp3nS	říkat
stomochord	stomochord	k1gInSc1	stomochord
<g/>
,	,	kIx,	,
vypadá	vypadat	k5eAaImIp3nS	vypadat
poměrně	poměrně	k6eAd1	poměrně
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřejmě	zřejmě	k6eAd1	zřejmě
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
konvergentní	konvergentní	k2eAgFnSc2d1	konvergentní
evoluce	evoluce	k1gFnSc2	evoluce
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgInPc1d1	jiný
zdroje	zdroj	k1gInPc1	zdroj
ho	on	k3xPp3gInSc2	on
však	však	k9	však
naopak	naopak	k6eAd1	naopak
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
homologii	homologie	k1gFnSc4	homologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
situace	situace	k1gFnSc1	situace
s	s	k7c7	s
neurochordem	neurochord	k1gInSc7	neurochord
žaludovců	žaludovec	k1gMnPc2	žaludovec
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
"	"	kIx"	"
<g/>
žaberní	žaberní	k2eAgFnPc4d1	žaberní
štěrbiny	štěrbina	k1gFnPc4	štěrbina
<g/>
"	"	kIx"	"
polostrunatců	polostrunatec	k1gMnPc2	polostrunatec
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
homologické	homologický	k2eAgFnSc2d1	homologická
(	(	kIx(	(
<g/>
vývojově	vývojově	k6eAd1	vývojově
spjaté	spjatý	k2eAgNnSc1d1	spjaté
<g/>
)	)	kIx)	)
s	s	k7c7	s
pravými	pravý	k2eAgFnPc7d1	pravá
žaberními	žaberní	k2eAgFnPc7d1	žaberní
štěrbinami	štěrbina	k1gFnPc7	štěrbina
strunatců	strunatec	k1gMnPc2	strunatec
<g/>
.	.	kIx.	.
</s>
<s>
Endostyl	Endostyl	k1gInSc1	Endostyl
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
orgán	orgán	k1gInSc1	orgán
strunatců	strunatec	k1gMnPc2	strunatec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
u	u	k7c2	u
polostrunatců	polostrunatec	k1gMnPc2	polostrunatec
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Embryonální	embryonální	k2eAgInSc4d1	embryonální
vývoj	vývoj	k1gInSc4	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
Strunatci	strunatec	k1gMnPc1	strunatec
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
druhoústé	druhoústý	k2eAgNnSc4d1	druhoústý
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
skupinu	skupina	k1gFnSc4	skupina
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
právě	právě	k9	právě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
typu	typ	k1gInSc2	typ
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
druhoústé	druhoústý	k2eAgNnSc4d1	druhoústý
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
embryologických	embryologický	k2eAgFnPc2d1	embryologická
učebnic	učebnice	k1gFnPc2	učebnice
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ústa	ústa	k1gNnPc1	ústa
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
řitní	řitní	k2eAgInSc1d1	řitní
otvor	otvor	k1gInSc1	otvor
<g/>
,	,	kIx,	,
a	a	k8xC	a
nevznikají	vznikat	k5eNaImIp3nP	vznikat
z	z	k7c2	z
blastoporu	blastopor	k1gInSc2	blastopor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
protilehlém	protilehlý	k2eAgNnSc6d1	protilehlé
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
většina	většina	k1gFnSc1	většina
druhoústých	druhoústý	k2eAgMnPc2d1	druhoústý
tvoří	tvořit	k5eAaImIp3nS	tvořit
tělní	tělní	k2eAgFnSc4d1	tělní
dutinu	dutina	k1gFnSc4	dutina
pomocí	pomocí	k7c2	pomocí
váčků	váček	k1gInPc2	váček
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
vychlipují	vychlipovat	k5eAaImIp3nP	vychlipovat
z	z	k7c2	z
střeva	střevo	k1gNnSc2	střevo
(	(	kIx(	(
<g/>
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
enterocoel	enterocoel	k1gInSc1	enterocoel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhoústí	druhoústý	k2eAgMnPc1d1	druhoústý
mají	mít	k5eAaImIp3nP	mít
radiální	radiální	k2eAgInSc4d1	radiální
typ	typ	k1gInSc4	typ
rýhování	rýhování	k1gNnSc2	rýhování
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
výjimek	výjimek	k1gInSc4	výjimek
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
pravidel	pravidlo	k1gNnPc2	pravidlo
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
strunatců	strunatec	k1gMnPc2	strunatec
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
enterocoel	enterocoel	k1gInSc1	enterocoel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
tělní	tělní	k2eAgFnSc1d1	tělní
dutina	dutina	k1gFnSc1	dutina
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
jako	jako	k9	jako
schizocoel	schizocoel	k1gInSc1	schizocoel
(	(	kIx(	(
<g/>
enterocoelie	enterocoelie	k1gFnSc1	enterocoelie
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
část	část	k1gFnSc4	část
těla	tělo	k1gNnSc2	tělo
kopinatců	kopinatec	k1gMnPc2	kopinatec
a	a	k8xC	a
mihule	mihule	k1gFnSc2	mihule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezoderm	mezoderm	k1gInSc1	mezoderm
tedy	tedy	k9	tedy
často	často	k6eAd1	často
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
odštěpených	odštěpený	k2eAgInPc2d1	odštěpený
bloků	blok	k1gInPc2	blok
mezodermu	mezoderm	k1gInSc2	mezoderm
či	či	k8xC	či
ze	z	k7c2	z
zakladatelských	zakladatelský	k2eAgFnPc2d1	zakladatelská
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vcestovaly	vcestovat	k5eAaPmAgFnP	vcestovat
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
budoucího	budoucí	k2eAgInSc2d1	budoucí
mezodermu	mezoderm	k1gInSc2	mezoderm
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mnozí	mnohý	k2eAgMnPc1d1	mnohý
strunatci	strunatec	k1gMnPc1	strunatec
nevytváří	vytvářet	k5eNaImIp3nP	vytvářet
dříve	dříve	k6eAd2	dříve
ústa	ústa	k1gNnPc4	ústa
ani	ani	k8xC	ani
řitní	řitní	k2eAgInSc4d1	řitní
otvor	otvor	k1gInSc4	otvor
–	–	k?	–
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
blastoporu	blastopor	k1gInSc6	blastopor
vzniká	vznikat	k5eAaImIp3nS	vznikat
žloutková	žloutkový	k2eAgFnSc1d1	žloutková
stopka	stopka	k1gFnSc1	stopka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
a	a	k8xC	a
řitní	řitní	k2eAgInSc1d1	řitní
otvor	otvor	k1gInSc1	otvor
se	se	k3xPyFc4	se
prolamuje	prolamovat	k5eAaImIp3nS	prolamovat
nezávisle	závisle	k6eNd1	závisle
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
strunatce	strunatec	k1gMnPc4	strunatec
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
zakládání	zakládání	k1gNnSc1	zakládání
některých	některý	k3yIgFnPc2	některý
orgánových	orgánový	k2eAgFnPc2d1	orgánová
soustav	soustava	k1gFnPc2	soustava
v	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
fázích	fáze	k1gFnPc6	fáze
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dorzální	dorzální	k2eAgFnSc2d1	dorzální
(	(	kIx(	(
<g/>
zádové	zádový	k2eAgFnSc2d1	zádová
<g/>
)	)	kIx)	)
stěny	stěna	k1gFnPc1	stěna
střeva	střevo	k1gNnSc2	střevo
se	se	k3xPyFc4	se
odškrcuje	odškrcovat	k5eAaImIp3nS	odškrcovat
pás	pás	k1gInSc1	pás
mezodermu	mezoderm	k1gInSc2	mezoderm
(	(	kIx(	(
<g/>
chordamezoderm	chordamezoderm	k1gInSc1	chordamezoderm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
struna	struna	k1gFnSc1	struna
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
části	část	k1gFnSc2	část
mezodermu	mezoderm	k1gInSc2	mezoderm
(	(	kIx(	(
<g/>
stěny	stěna	k1gFnSc2	stěna
coelomu	coelom	k1gInSc2	coelom
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
somity	somit	k1gInPc7	somit
<g/>
,	,	kIx,	,
jakési	jakýsi	k3yIgInPc4	jakýsi
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
segmentované	segmentovaný	k2eAgNnSc1d1	segmentované
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ektodermu	ektoderm	k1gInSc2	ektoderm
se	se	k3xPyFc4	se
zase	zase	k9	zase
vchlipuje	vchlipovat	k5eAaImIp3nS	vchlipovat
neuroektodermální	uroektodermální	k2eNgInSc1d1	neuroektodermální
pás	pás	k1gInSc1	pás
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
nervovou	nervový	k2eAgFnSc4d1	nervová
trubici	trubice	k1gFnSc4	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Žaberní	žaberní	k2eAgFnPc1d1	žaberní
štěrbiny	štěrbina	k1gFnPc1	štěrbina
vznikají	vznikat	k5eAaImIp3nP	vznikat
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
entodermu	entoderm	k1gInSc2	entoderm
vychlípením	vychlípení	k1gNnSc7	vychlípení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
vzniku	vznik	k1gInSc6	vznik
podílí	podílet	k5eAaImIp3nS	podílet
i	i	k8xC	i
vchlipující	vchlipující	k2eAgMnSc1d1	vchlipující
se	se	k3xPyFc4	se
ektoderm	ektoderm	k1gInSc1	ektoderm
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
perforaci	perforace	k1gFnSc3	perforace
tělní	tělní	k2eAgFnSc2d1	tělní
stěny	stěna	k1gFnSc2	stěna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
kosterní	kosterní	k2eAgFnSc2d1	kosterní
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
strunatce	strunatec	k1gMnPc4	strunatec
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
kostra	kostra	k1gFnSc1	kostra
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
mezodermu	mezoderm	k1gInSc6	mezoderm
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
v	v	k7c6	v
ektodermu	ektoderm	k1gInSc6	ektoderm
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mnohých	mnohý	k2eAgMnPc2d1	mnohý
jiných	jiný	k2eAgMnPc2d1	jiný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
tělo	tělo	k1gNnSc4	tělo
strunatců	strunatec	k1gMnPc2	strunatec
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
charakteristicky	charakteristicky	k6eAd1	charakteristicky
vzájemně	vzájemně	k6eAd1	vzájemně
uspořádaných	uspořádaný	k2eAgInPc2d1	uspořádaný
orgánů	orgán	k1gInPc2	orgán
<g/>
:	:	kIx,	:
toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
právě	právě	k9	právě
z	z	k7c2	z
průběhu	průběh	k1gInSc2	průběh
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Evoluce	evoluce	k1gFnSc2	evoluce
==	==	k?	==
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
přístupy	přístup	k1gInPc1	přístup
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
příbuznost	příbuznost	k1gFnSc4	příbuznost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
živočišných	živočišný	k2eAgInPc2d1	živočišný
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
nejbližší	blízký	k2eAgMnPc4d3	nejbližší
příbuzné	příbuzný	k1gMnPc4	příbuzný
strunatců	strunatec	k1gMnPc2	strunatec
polostrunatce	polostrunatec	k1gMnSc4	polostrunatec
(	(	kIx(	(
<g/>
Hemichordata	Hemichordat	k1gMnSc4	Hemichordat
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostnokožce	ostnokožec	k1gMnSc2	ostnokožec
(	(	kIx(	(
<g/>
Echinodermata	Echinoderma	k1gNnPc4	Echinoderma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
kmeny	kmen	k1gInPc1	kmen
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
společně	společně	k6eAd1	společně
s	s	k7c7	s
rodem	rod	k1gInSc7	rod
Xenoturbella	Xenoturbello	k1gNnSc2	Xenoturbello
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
společně	společně	k6eAd1	společně
sesterskou	sesterský	k2eAgFnSc4d1	sesterská
skupinu	skupina	k1gFnSc4	skupina
ke	k	k7c3	k
strunatcům	strunatec	k1gMnPc3	strunatec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sesterská	sesterský	k2eAgFnSc1d1	sesterská
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
Ambulacraria	Ambulacrarium	k1gNnSc2	Ambulacrarium
<g/>
.	.	kIx.	.
<g/>
Stáří	stáří	k1gNnSc1	stáří
kmene	kmen	k1gInSc2	kmen
strunatci	strunatec	k1gMnPc7	strunatec
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
stovek	stovka	k1gFnPc2	stovka
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
poměrně	poměrně	k6eAd1	poměrně
těžko	těžko	k6eAd1	těžko
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
to	ten	k3xDgNnSc1	ten
alespoň	alespoň	k9	alespoň
přibližně	přibližně	k6eAd1	přibližně
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
molekulární	molekulární	k2eAgFnPc1d1	molekulární
hodiny	hodina	k1gFnPc1	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
studie	studie	k1gFnSc2	studie
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
molekulárních	molekulární	k2eAgFnPc6d1	molekulární
hodinách	hodina	k1gFnPc6	hodina
se	se	k3xPyFc4	se
linie	linie	k1gFnSc1	linie
strunatců	strunatec	k1gMnPc2	strunatec
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Ambulacraria	Ambulacrarium	k1gNnSc2	Ambulacrarium
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
896	[number]	k4	896
miliony	milion	k4xCgInPc7	milion
lety	let	k1gInPc7	let
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
před	před	k7c7	před
832	[number]	k4	832
<g/>
–	–	k?	–
<g/>
1022	[number]	k4	1022
miliony	milion	k4xCgInPc4	milion
lety	let	k1gInPc4	let
s	s	k7c7	s
důvěryhodností	důvěryhodnost	k1gFnSc7	důvěryhodnost
95	[number]	k4	95
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
starší	starý	k2eAgFnSc1d2	starší
studie	studie	k1gFnSc1	studie
udává	udávat	k5eAaImIp3nS	udávat
výrazně	výrazně	k6eAd1	výrazně
jiná	jiný	k2eAgNnPc4d1	jiné
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
klade	klást	k5eAaImIp3nS	klást
tento	tento	k3xDgInSc4	tento
mezník	mezník	k1gInSc4	mezník
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
600	[number]	k4	600
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
více	hodně	k6eAd2	hodně
sedělo	sedět	k5eAaImAgNnS	sedět
na	na	k7c4	na
paleontologické	paleontologický	k2eAgInPc4d1	paleontologický
nálezy	nález	k1gInPc4	nález
<g/>
.	.	kIx.	.
</s>
<s>
Dawkins	Dawkins	k6eAd1	Dawkins
zase	zase	k9	zase
odhadl	odhadnout	k5eAaPmAgMnS	odhadnout
stáří	stáří	k1gNnSc4	stáří
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
tří	tři	k4xCgInPc2	tři
současných	současný	k2eAgInPc2d1	současný
podkmenů	podkmen	k1gInPc2	podkmen
na	na	k7c4	na
565	[number]	k4	565
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Evoluční	evoluční	k2eAgFnPc1d1	evoluční
hypotézy	hypotéza	k1gFnPc1	hypotéza
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
moderních	moderní	k2eAgNnPc2d1	moderní
dat	datum	k1gNnPc2	datum
o	o	k7c6	o
příbuznosti	příbuznost	k1gFnSc6	příbuznost
pláštěnců	pláštěnec	k1gMnPc2	pláštěnec
<g/>
,	,	kIx,	,
kopinatců	kopinatec	k1gMnPc2	kopinatec
a	a	k8xC	a
obratlovců	obratlovec	k1gMnPc2	obratlovec
si	se	k3xPyFc3	se
zřejmě	zřejmě	k6eAd1	zřejmě
můžeme	moct	k5eAaImIp1nP	moct
udělat	udělat	k5eAaPmF	udělat
obrázek	obrázek	k1gInSc4	obrázek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vypadal	vypadat	k5eAaImAgInS	vypadat
primitivní	primitivní	k2eAgMnSc1d1	primitivní
(	(	kIx(	(
<g/>
rozuměj	rozumět	k5eAaImRp2nS	rozumět
"	"	kIx"	"
<g/>
evolučně	evolučně	k6eAd1	evolučně
původní	původní	k2eAgInSc1d1	původní
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
strunatec	strunatec	k1gMnSc1	strunatec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
společný	společný	k2eAgInSc1d1	společný
předek	předek	k1gInSc1	předek
strunatců	strunatec	k1gMnPc2	strunatec
měl	mít	k5eAaImAgInS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
zřetelnou	zřetelný	k2eAgFnSc4d1	zřetelná
tělní	tělní	k2eAgFnSc4d1	tělní
segmentaci	segmentace	k1gFnSc4	segmentace
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
ploutevní	ploutevní	k2eAgInSc4d1	ploutevní
lem	lem	k1gInSc4	lem
<g/>
,	,	kIx,	,
chordu	chorda	k1gFnSc4	chorda
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
také	také	k9	také
přímý	přímý	k2eAgInSc4d1	přímý
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
ohledech	ohled	k1gInPc6	ohled
zřejmě	zřejmě	k6eAd1	zřejmě
druhotně	druhotně	k6eAd1	druhotně
odvozená	odvozený	k2eAgFnSc1d1	odvozená
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
např.	např.	kA	např.
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nepřímý	přímý	k2eNgInSc1d1	nepřímý
vývoj	vývoj	k1gInSc1	vývoj
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
larvu	larva	k1gFnSc4	larva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc4	několik
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
osvětlit	osvětlit	k5eAaPmF	osvětlit
vznik	vznik	k1gInSc4	vznik
strunatců	strunatec	k1gMnPc2	strunatec
z	z	k7c2	z
druhoústých	druhoústý	k2eAgInPc2d1	druhoústý
předků	předek	k1gInPc2	předek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Aurikuláriová	Aurikuláriový	k2eAgFnSc1d1	Aurikuláriový
(	(	kIx(	(
<g/>
též	též	k9	též
dipleurová	dipleurový	k2eAgFnSc1d1	dipleurový
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
předkem	předek	k1gInSc7	předek
strunatců	strunatec	k1gMnPc2	strunatec
byla	být	k5eAaImAgFnS	být
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
obrvená	obrvený	k2eAgFnSc1d1	obrvená
larva	larva	k1gFnSc1	larva
živočicha	živočich	k1gMnSc2	živočich
podobného	podobný	k2eAgInSc2d1	podobný
křídložábrým	křídložábrý	k2eAgInSc7d1	křídložábrý
(	(	kIx(	(
<g/>
Pterobranchia	Pterobranchium	k1gNnSc2	Pterobranchium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řady	řada	k1gFnSc2	řada
brv	brva	k1gFnPc2	brva
na	na	k7c6	na
zádové	zádový	k2eAgFnSc6d1	zádová
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
nervová	nervový	k2eAgFnSc1d1	nervová
trubice	trubice	k1gFnSc1	trubice
<g/>
,	,	kIx,	,
od	od	k7c2	od
dospělého	dospělý	k2eAgNnSc2d1	dospělé
křídložábrého	křídložábrý	k2eAgNnSc2d1	křídložábrý
byly	být	k5eAaImAgInP	být
"	"	kIx"	"
<g/>
okoukány	okoukán	k2eAgInPc1d1	okoukán
<g/>
"	"	kIx"	"
zase	zase	k9	zase
žaberní	žaberní	k2eAgFnPc1d1	žaberní
štěrbiny	štěrbina	k1gFnPc1	štěrbina
a	a	k8xC	a
chorda	chorda	k1gFnSc1	chorda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
procesu	proces	k1gInSc3	proces
neotenie	neotenie	k1gFnSc2	neotenie
se	se	k3xPyFc4	se
z	z	k7c2	z
larev	larva	k1gFnPc2	larva
těchto	tento	k3xDgMnPc2	tento
živočichů	živočich	k1gMnPc2	živočich
podobných	podobný	k2eAgMnPc2d1	podobný
křídložábrým	křídložábrý	k2eAgFnPc3d1	křídložábrý
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
pohlavně	pohlavně	k6eAd1	pohlavně
dospělý	dospělý	k1gMnSc1	dospělý
strunatec	strunatec	k1gMnSc1	strunatec
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
má	mít	k5eAaImIp3nS	mít
i	i	k8xC	i
trhliny	trhlina	k1gFnPc4	trhlina
<g/>
:	:	kIx,	:
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
dospělým	dospělý	k2eAgMnPc3d1	dospělý
strunatcům	strunatec	k1gMnPc3	strunatec
podobnější	podobný	k2eAgMnSc1d2	podobnější
dospělec	dospělec	k1gMnSc1	dospělec
polostrunatců	polostrunatec	k1gMnPc2	polostrunatec
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
larva	larva	k1gFnSc1	larva
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
hypotézy	hypotéza	k1gFnSc2	hypotéza
strunatci	strunatec	k1gMnPc1	strunatec
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
u	u	k7c2	u
strunatců	strunatec	k1gMnPc2	strunatec
tak	tak	k9	tak
výrazně	výrazně	k6eAd1	výrazně
změnilo	změnit	k5eAaPmAgNnS	změnit
dorzoventrální	dorzoventrální	k2eAgNnSc1d1	dorzoventrální
(	(	kIx(	(
<g/>
hřbetobřišní	hřbetobřišní	k2eAgNnSc4d1	hřbetobřišní
<g/>
)	)	kIx)	)
uspořádání	uspořádání	k1gNnSc4	uspořádání
těla	tělo	k1gNnSc2	tělo
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Hypotéza	hypotéza	k1gFnSc1	hypotéza
inverzní	inverzní	k2eAgFnSc1d1	inverzní
(	(	kIx(	(
<g/>
dorzoventrální	dorzoventrální	k2eAgFnSc1d1	dorzoventrální
inverze	inverze	k1gFnSc1	inverze
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
předek	předek	k1gInSc1	předek
strunatců	strunatec	k1gMnPc2	strunatec
měl	mít	k5eAaImAgInS	mít
nejprve	nejprve	k6eAd1	nejprve
podobné	podobný	k2eAgNnSc4d1	podobné
uspořádání	uspořádání	k1gNnSc4	uspořádání
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
prvoústí	prvoústět	k5eAaPmIp3nS	prvoústět
–	–	k?	–
nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
a	a	k8xC	a
svalovina	svalovina	k1gFnSc1	svalovina
na	na	k7c6	na
břišní	břišní	k2eAgFnSc6d1	břišní
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
ventrálně	ventrálně	k6eAd1	ventrálně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc1	srdce
na	na	k7c6	na
zádové	zádový	k2eAgFnSc6d1	zádová
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
dorzálně	dorzálně	k6eAd1	dorzálně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
předek	předek	k1gInSc1	předek
zřejmě	zřejmě	k6eAd1	zřejmě
již	již	k6eAd1	již
měl	mít	k5eAaImAgMnS	mít
nervovou	nervový	k2eAgFnSc4d1	nervová
trubici	trubice	k1gFnSc4	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Mutací	mutace	k1gFnSc7	mutace
tohoto	tento	k3xDgInSc2	tento
tělního	tělní	k2eAgInSc2d1	tělní
plánu	plán	k1gInSc2	plán
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
radikální	radikální	k2eAgFnSc3d1	radikální
změně	změna	k1gFnSc3	změna
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
se	se	k3xPyFc4	se
invertovalo	invertovat	k5eAaBmAgNnS	invertovat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
<g/>
"	"	kIx"	"
dorzoventrálně	dorzoventrálně	k6eAd1	dorzoventrálně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
inverzi	inverze	k1gFnSc3	inverze
nedošlo	dojít	k5eNaPmAgNnS	dojít
u	u	k7c2	u
polostrunatců	polostrunatec	k1gMnPc2	polostrunatec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
exkluzivní	exkluzivní	k2eAgInSc1d1	exkluzivní
znak	znak	k1gInSc1	znak
strunatců	strunatec	k1gMnPc2	strunatec
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
pro	pro	k7c4	pro
inverzní	inverzní	k2eAgFnSc4d1	inverzní
hypotézu	hypotéza	k1gFnSc4	hypotéza
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
exprese	exprese	k1gFnSc1	exprese
genu	gen	k1gInSc2	gen
bmp	bmp	k?	bmp
–	–	k?	–
u	u	k7c2	u
prvoústých	prvoústý	k2eAgInPc2d1	prvoústý
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
,	,	kIx,	,
u	u	k7c2	u
druhoústých	druhoústý	k2eAgFnPc2d1	druhoústý
na	na	k7c6	na
břišní	břišní	k2eAgFnSc6d1	břišní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hypotéza	hypotéza	k1gFnSc1	hypotéza
"	"	kIx"	"
<g/>
hemichordate	hemichordat	k1gInSc5	hemichordat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
polostrunatce	polostrunatec	k1gMnPc4	polostrunatec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
strunatci	strunatec	k1gMnPc1	strunatec
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
z	z	k7c2	z
živočicha	živočich	k1gMnSc2	živočich
podobného	podobný	k2eAgNnSc2d1	podobné
polostrunatcům	polostrunatec	k1gMnPc3	polostrunatec
(	(	kIx(	(
<g/>
již	již	k9	již
s	s	k7c7	s
nervovou	nervový	k2eAgFnSc7d1	nervová
trubicí	trubice	k1gFnSc7	trubice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
během	během	k7c2	během
evoluce	evoluce	k1gFnSc2	evoluce
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zdůraznění	zdůraznění	k1gNnSc3	zdůraznění
a	a	k8xC	a
zveličení	zveličení	k1gNnSc4	zveličení
znaků	znak	k1gInPc2	znak
typických	typický	k2eAgInPc2d1	typický
pro	pro	k7c4	pro
polostrunatce	polostrunatec	k1gMnPc4	polostrunatec
<g/>
.	.	kIx.	.
</s>
<s>
Přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
mohla	moct	k5eAaImAgFnS	moct
přestavba	přestavba	k1gFnSc1	přestavba
coelomových	coelomový	k2eAgInPc2d1	coelomový
váčků	váček	k1gInPc2	váček
a	a	k8xC	a
somitů	somit	k1gInPc2	somit
na	na	k7c6	na
hlavové	hlavový	k2eAgFnSc6d1	hlavová
i	i	k8xC	i
zádové	zádový	k2eAgFnSc6d1	zádová
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hypotéza	hypotéza	k1gFnSc1	hypotéza
bilaterálního	bilaterální	k2eAgMnSc2d1	bilaterální
předka	předek	k1gMnSc2	předek
<g/>
:	:	kIx,	:
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
evoluci	evoluce	k1gFnSc6	evoluce
strunatců	strunatec	k1gMnPc2	strunatec
k	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
významnému	významný	k2eAgNnSc3d1	významné
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Pohnutkou	pohnutka	k1gFnSc7	pohnutka
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
názoru	názor	k1gInSc3	názor
bylo	být	k5eAaImAgNnS	být
zjištění	zjištění	k1gNnSc1	zjištění
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
genom	genom	k1gInSc1	genom
strunatců	strunatec	k1gMnPc2	strunatec
v	v	k7c6	v
zásadních	zásadní	k2eAgInPc6d1	zásadní
genech	gen	k1gInPc6	gen
vlastně	vlastně	k9	vlastně
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
genomy	genom	k1gInPc7	genom
velmi	velmi	k6eAd1	velmi
odlišných	odlišný	k2eAgMnPc2d1	odlišný
prvoústých	prvoústý	k2eAgMnPc2d1	prvoústý
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
octomilka	octomilka	k1gFnSc1	octomilka
(	(	kIx(	(
<g/>
Drosophila	Drosophila	k1gFnSc1	Drosophila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
podobnosti	podobnost	k1gFnSc2	podobnost
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
Hox	Hox	k1gFnPc1	Hox
geny	gen	k1gInPc1	gen
určující	určující	k2eAgInSc1d1	určující
vývoj	vývoj	k1gInSc4	vývoj
hlavy	hlava	k1gFnPc4	hlava
a	a	k8xC	a
hrudi	hruď	k1gFnPc4	hruď
<g/>
,	,	kIx,	,
pax	pax	k?	pax
<g/>
6	[number]	k4	6
a	a	k8xC	a
six	six	k?	six
geny	gen	k1gInPc1	gen
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
či	či	k8xC	či
např.	např.	kA	např.
různé	různý	k2eAgInPc1d1	různý
geny	gen	k1gInPc1	gen
určující	určující	k2eAgInSc1d1	určující
vývoj	vývoj	k1gInSc4	vývoj
nervové	nervový	k2eAgFnSc2d1	nervová
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
střeva	střevo	k1gNnSc2	střevo
a	a	k8xC	a
útrobního	útrobní	k2eAgInSc2d1	útrobní
mezodermu	mezoderm	k1gInSc2	mezoderm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fosilní	fosilní	k2eAgInSc1d1	fosilní
záznam	záznam	k1gInSc1	záznam
===	===	k?	===
</s>
</p>
<p>
<s>
Fosilních	fosilní	k2eAgInPc2d1	fosilní
záznamů	záznam	k1gInPc2	záznam
prvních	první	k4xOgNnPc2	první
strunatců	strunatec	k1gMnPc2	strunatec
je	být	k5eAaImIp3nS	být
poskrovnu	poskrovnu	k6eAd1	poskrovnu
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
přiřadit	přiřadit	k5eAaPmF	přiřadit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
starou	starý	k2eAgFnSc4d1	stará
fosílii	fosílie	k1gFnSc4	fosílie
k	k	k7c3	k
určitému	určitý	k2eAgInSc3d1	určitý
kmeni	kmen	k1gInSc3	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
těla	tělo	k1gNnPc1	tělo
postrádají	postrádat	k5eAaImIp3nP	postrádat
kosti	kost	k1gFnPc1	kost
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
špatně	špatně	k6eAd1	špatně
fosilizují	fosilizovat	k5eAaImIp3nP	fosilizovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
údajným	údajný	k2eAgMnPc3d1	údajný
strunatcům	strunatec	k1gMnPc3	strunatec
patří	patřit	k5eAaImIp3nP	patřit
fosilní	fosilní	k2eAgMnPc1d1	fosilní
živočichové	živočich	k1gMnPc1	živočich
Yunnanozoon	Yunnanozoon	k1gNnSc1	Yunnanozoon
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgNnSc1d1	spodní
kambrium	kambrium	k1gNnSc1	kambrium
<g/>
,	,	kIx,	,
Čcheng-ťiang	Čcheng-ťiang	k1gInSc1	Čcheng-ťiang
<g/>
,	,	kIx,	,
provincie	provincie	k1gFnSc1	provincie
Jün-nan	Jünan	k1gMnSc1	Jün-nan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cathaymyrus	Cathaymyrus	k1gInSc1	Cathaymyrus
(	(	kIx(	(
<g/>
totožné	totožný	k2eAgNnSc1d1	totožné
naleziště	naleziště	k1gNnSc1	naleziště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Haikouella	Haikouella	k1gFnSc1	Haikouella
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgNnSc1d1	spodní
kambrium	kambrium	k1gNnSc1	kambrium
<g/>
,	,	kIx,	,
Kchun-ming	Kchuning	k1gInSc1	Kchun-ming
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pikaia	Pikaia	k1gFnSc1	Pikaia
(	(	kIx(	(
<g/>
střední	střední	k2eAgNnSc1d1	střední
kambrium	kambrium	k1gNnSc1	kambrium
<g/>
,	,	kIx,	,
burgesské	burgesské	k2eAgFnSc1d1	burgesské
břidlice	břidlice	k1gFnSc1	břidlice
<g/>
,	,	kIx,	,
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Systematika	systematika	k1gFnSc1	systematika
==	==	k?	==
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Chordata	Chordata	k1gFnSc1	Chordata
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
strunatci	strunatec	k1gMnPc1	strunatec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přisuzován	přisuzován	k2eAgInSc1d1	přisuzován
Williamu	William	k1gInSc3	William
Batesonovi	Bateson	k1gMnSc6	Bateson
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
se	se	k3xPyFc4	se
evidentně	evidentně	k6eAd1	evidentně
používalo	používat	k5eAaImAgNnS	používat
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
strunatců	strunatec	k1gMnPc2	strunatec
procházela	procházet	k5eAaImAgFnS	procházet
složitým	složitý	k2eAgInSc7d1	složitý
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
již	již	k6eAd1	již
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mimo	mimo	k7c4	mimo
obratlovce	obratlovec	k1gMnPc4	obratlovec
se	se	k3xPyFc4	se
podobnou	podobný	k2eAgFnSc7d1	podobná
tělní	tělní	k2eAgFnSc7d1	tělní
stavbou	stavba	k1gFnSc7	stavba
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
také	také	k9	také
pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
a	a	k8xC	a
bezlebeční	bezlebeční	k1gMnPc1	bezlebeční
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
uznávají	uznávat	k5eAaImIp3nP	uznávat
tyto	tento	k3xDgFnPc1	tento
tři	tři	k4xCgFnPc1	tři
skupiny	skupina	k1gFnPc1	skupina
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
podkmeny	podkmen	k1gInPc1	podkmen
<g/>
)	)	kIx)	)
strunatců	strunatec	k1gMnPc2	strunatec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podkmen	podkmen	k1gInSc4	podkmen
pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
(	(	kIx(	(
<g/>
Tunicata	Tunicata	k1gFnSc1	Tunicata
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podkmen	podkmen	k1gInSc1	podkmen
bezlebeční	bezlebeční	k2eAgInSc1d1	bezlebeční
(	(	kIx(	(
<g/>
Cephalochordata	Cephalochordata	k1gFnSc1	Cephalochordata
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podkmen	podkmen	k1gInSc4	podkmen
obratlovci	obratlovec	k1gMnPc1	obratlovec
(	(	kIx(	(
<g/>
Craniata	Craniata	k1gFnSc1	Craniata
<g/>
)	)	kIx)	)
<g/>
Taxon	taxon	k1gInSc1	taxon
strunatci	strunatec	k1gMnSc6	strunatec
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
monofyletický	monofyletický	k2eAgInSc1d1	monofyletický
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
s	s	k7c7	s
důkazy	důkaz	k1gInPc7	důkaz
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
byly	být	k5eAaImAgFnP	být
poměrně	poměrně	k6eAd1	poměrně
značné	značný	k2eAgInPc1d1	značný
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
tří	tři	k4xCgInPc2	tři
hlavních	hlavní	k2eAgInPc2d1	hlavní
podkmenů	podkmen	k1gInPc2	podkmen
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
jisté	jistý	k2eAgFnPc1d1	jistá
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
starší	starý	k2eAgInPc1d2	starší
<g/>
)	)	kIx)	)
zdroje	zdroj	k1gInPc1	zdroj
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
sesterskou	sesterský	k2eAgFnSc4d1	sesterská
skupinu	skupina	k1gFnSc4	skupina
obratlovců	obratlovec	k1gMnPc2	obratlovec
bezlebečné	bezlebeční	k1gMnPc4	bezlebeční
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
nedávné	dávný	k2eNgFnPc1d1	nedávná
studie	studie	k1gFnPc1	studie
však	však	k9	však
přichází	přicházet	k5eAaImIp3nS	přicházet
se	s	k7c7	s
zcela	zcela	k6eAd1	zcela
novým	nový	k2eAgInSc7d1	nový
konceptem	koncept	k1gInSc7	koncept
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
považuje	považovat	k5eAaImIp3nS	považovat
pláštěnce	pláštěnec	k1gMnPc4	pláštěnec
za	za	k7c4	za
příbuznější	příbuzný	k2eAgFnSc1d2	příbuznější
obratlovcům	obratlovec	k1gMnPc3	obratlovec
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
kopinatci	kopinatec	k1gMnPc1	kopinatec
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
studie	studie	k1gFnPc1	studie
pracují	pracovat	k5eAaImIp3nP	pracovat
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
rozsáhlejšími	rozsáhlý	k2eAgNnPc7d2	rozsáhlejší
daty	datum	k1gNnPc7	datum
získaných	získaný	k2eAgInPc2d1	získaný
čtením	čtení	k1gNnSc7	čtení
protein	protein	k1gInSc1	protein
<g/>
–	–	k?	–
<g/>
kódujících	kódující	k2eAgInPc2d1	kódující
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
novější	nový	k2eAgFnSc1d2	novější
teorie	teorie	k1gFnSc1	teorie
se	se	k3xPyFc4	se
tak	tak	k9	tak
postupně	postupně	k6eAd1	postupně
dostává	dostávat	k5eAaImIp3nS	dostávat
i	i	k9	i
do	do	k7c2	do
popularizačních	popularizační	k2eAgFnPc2d1	popularizační
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
Zrzavý	zrzavý	k2eAgInSc1d1	zrzavý
2006	[number]	k4	2006
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
hotovou	hotový	k2eAgFnSc4d1	hotová
věc	věc	k1gFnSc4	věc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodně	rozhodně	k6eAd1	rozhodně
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
kritické	kritický	k2eAgInPc1d1	kritický
hlasy	hlas	k1gInPc1	hlas
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zobrazena	zobrazit	k5eAaPmNgFnS	zobrazit
na	na	k7c6	na
níže	nízce	k6eAd2	nízce
uvedeném	uvedený	k2eAgInSc6d1	uvedený
fylogenetickém	fylogenetický	k2eAgInSc6d1	fylogenetický
stromu	strom	k1gInSc6	strom
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Snaha	snaha	k1gFnSc1	snaha
porozumět	porozumět	k5eAaPmF	porozumět
vývoji	vývoj	k1gInSc3	vývoj
strunatců	strunatec	k1gMnPc2	strunatec
pomocí	pomoc	k1gFnPc2	pomoc
morfologických	morfologický	k2eAgFnPc2d1	morfologická
informací	informace	k1gFnPc2	informace
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
především	především	k9	především
na	na	k7c6	na
základě	základ	k1gInSc6	základ
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
anatomie	anatomie	k1gFnSc2	anatomie
pláštěnců	pláštěnec	k1gMnPc2	pláštěnec
<g/>
,	,	kIx,	,
kopinatců	kopinatec	k1gMnPc2	kopinatec
a	a	k8xC	a
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
a	a	k8xC	a
nedává	dávat	k5eNaImIp3nS	dávat
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
obratlovcům	obratlovec	k1gMnPc3	obratlovec
evolučně	evolučně	k6eAd1	evolučně
blíže	blízce	k6eAd2	blízce
pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
nebo	nebo	k8xC	nebo
kopinatci	kopinatec	k1gMnPc1	kopinatec
<g/>
.	.	kIx.	.
</s>
<s>
Obratlovce	obratlovec	k1gMnPc4	obratlovec
a	a	k8xC	a
kopinatce	kopinatec	k1gMnPc4	kopinatec
spojuje	spojovat	k5eAaImIp3nS	spojovat
chorda	chorda	k1gFnSc1	chorda
dorsalis	dorsalis	k1gFnSc2	dorsalis
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgFnSc1d1	podobná
stavba	stavba	k1gFnSc1	stavba
neurální	neurální	k2eAgFnSc2d1	neurální
trubice	trubice	k1gFnSc2	trubice
a	a	k8xC	a
smyslových	smyslový	k2eAgInPc2d1	smyslový
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
třeba	třeba	k6eAd1	třeba
jistý	jistý	k2eAgInSc4d1	jistý
ploutevní	ploutevní	k2eAgInSc4d1	ploutevní
lem	lem	k1gInSc4	lem
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
a	a	k8xC	a
ocasu	ocas	k1gInSc6	ocas
či	či	k8xC	či
rozdělení	rozdělení	k1gNnSc6	rozdělení
těla	tělo	k1gNnSc2	tělo
do	do	k7c2	do
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
segmentů	segment	k1gInPc2	segment
<g/>
,	,	kIx,	,
metamer	metamra	k1gFnPc2	metamra
nebo	nebo	k8xC	nebo
somitů	somit	k1gInPc2	somit
<g/>
.	.	kIx.	.
</s>
<s>
Obratlovci	obratlovec	k1gMnPc1	obratlovec
a	a	k8xC	a
pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
také	také	k9	také
mnoho	mnoho	k4c4	mnoho
společných	společný	k2eAgInPc2d1	společný
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
je	on	k3xPp3gNnSc4	on
nový	nový	k2eAgInSc1d1	nový
typ	typ	k1gInSc1	typ
buněčných	buněčný	k2eAgInPc2d1	buněčný
spojů	spoj	k1gInPc2	spoj
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
těsné	těsný	k2eAgInPc1d1	těsný
spoje	spoj	k1gInPc1	spoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgFnSc1d1	podobná
stavba	stavba	k1gFnSc1	stavba
chordy	chorda	k1gFnSc2	chorda
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
některých	některý	k3yIgMnPc2	některý
myoepitelů	myoepitel	k1gMnPc2	myoepitel
<g/>
,	,	kIx,	,
neuromastové	uromastový	k2eNgFnPc1d1	uromastový
smyslové	smyslový	k2eAgFnPc1d1	smyslová
buňky	buňka	k1gFnPc1	buňka
(	(	kIx(	(
<g/>
základy	základ	k1gInPc1	základ
budoucí	budoucí	k2eAgFnSc2d1	budoucí
postranní	postranní	k2eAgFnSc2d1	postranní
čáry	čára	k1gFnSc2	čára
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ROČEK	Roček	k1gMnSc1	Roček
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
obratlovců	obratlovec	k1gMnPc2	obratlovec
:	:	kIx,	:
evoluce	evoluce	k1gFnSc1	evoluce
<g/>
,	,	kIx,	,
fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
512	[number]	k4	512
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
858	[number]	k4	858
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZRZAVÝ	zrzavý	k2eAgMnSc1d1	zrzavý
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
živočišné	živočišný	k2eAgFnSc2d1	živočišná
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Scientia	Scientia	k1gFnSc1	Scientia
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
255	[number]	k4	255
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86960	[number]	k4	86960
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HAJER	hajer	k1gMnSc1	hajer
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
a	a	k8xC	a
systém	systém	k1gInSc1	systém
strunatců	strunatec	k1gMnPc2	strunatec
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
JAP	JAP	kA	JAP
<g/>
,	,	kIx,	,
přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
strunatci	strunatec	k1gMnPc1	strunatec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Chordata	Chordat	k1gMnSc2	Chordat
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
strunatci	strunatec	k1gMnSc6	strunatec
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
"	"	kIx"	"
<g/>
Tree	Tree	k1gNnSc1	Tree
of	of	k?	of
Life	Lif	k1gInSc2	Lif
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Kurzy	kurz	k1gInPc1	kurz
morfologie	morfologie	k1gFnSc2	morfologie
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c4	na
rocek	rocek	k1gInSc4	rocek
<g/>
.	.	kIx.	.
<g/>
gli	gli	k?	gli
<g/>
.	.	kIx.	.
<g/>
cas	cas	k?	cas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
