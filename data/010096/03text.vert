<p>
<s>
Steve	Steve	k1gMnSc1	Steve
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Michael	Michael	k1gMnSc1	Michael
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
průkopníka	průkopník	k1gMnSc4	průkopník
minimalismu	minimalismus	k1gInSc2	minimalismus
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
netvoří	tvořit	k5eNaImIp3nP	tvořit
výlučně	výlučně	k6eAd1	výlučně
minimalistické	minimalistický	k2eAgFnPc1d1	minimalistická
skladby	skladba	k1gFnPc1	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
několik	několik	k4yIc4	několik
vlivných	vlivný	k2eAgInPc2d1	vlivný
kompozičních	kompoziční	k2eAgInPc2d1	kompoziční
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
např.	např.	kA	např.
fázování	fázování	k1gNnSc4	fázování
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgInPc1	který
dva	dva	k4xCgInPc1	dva
nástroje	nástroj	k1gInPc1	nástroj
hrají	hrát	k5eAaImIp3nP	hrát
tentýž	týž	k3xTgInSc4	týž
part	part	k1gInSc4	part
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
ve	v	k7c6	v
stálém	stálý	k2eAgNnSc6d1	stálé
tempu	tempo	k1gNnSc6	tempo
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
stále	stále	k6eAd1	stále
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
druhý	druhý	k4xOgInSc1	druhý
nástroj	nástroj	k1gInSc1	nástroj
předběhne	předběhnout	k5eAaPmIp3nS	předběhnout
první	první	k4xOgInSc1	první
nástroj	nástroj	k1gInSc1	nástroj
o	o	k7c4	o
celou	celý	k2eAgFnSc4d1	celá
fázi	fáze	k1gFnSc4	fáze
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ho	on	k3xPp3gMnSc4	on
vlastně	vlastně	k9	vlastně
opět	opět	k6eAd1	opět
jakoby	jakoby	k8xS	jakoby
"	"	kIx"	"
<g/>
dožene	dohnat	k5eAaPmIp3nS	dohnat
<g/>
"	"	kIx"	"
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
fázi	fáze	k1gFnSc4	fáze
napřed	napřed	k6eAd1	napřed
<g/>
)	)	kIx)	)
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
magnetofonových	magnetofonový	k2eAgInPc2d1	magnetofonový
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
deníku	deník	k1gInSc2	deník
The	The	k1gMnSc1	The
Guardian	Guardian	k1gMnSc1	Guardian
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
žijících	žijící	k2eAgMnPc2d1	žijící
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
údajně	údajně	k6eAd1	údajně
"	"	kIx"	"
<g/>
změnili	změnit	k5eAaPmAgMnP	změnit
běh	běh	k1gInSc4	běh
hudebních	hudební	k2eAgFnPc2d1	hudební
dějin	dějiny	k1gFnPc2	dějiny
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
oceněn	oceněn	k2eAgInSc1d1	oceněn
newyorským	newyorský	k2eAgNnSc7d1	newyorské
kulturním	kulturní	k2eAgNnSc7d1	kulturní
zařízením	zařízení	k1gNnSc7	zařízení
The	The	k1gFnSc2	The
Kitchen	Kitchna	k1gFnPc2	Kitchna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Díla	dílo	k1gNnPc1	dílo
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
It	It	k?	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gonna	Gonen	k2eAgFnSc1d1	Gonna
Rain	Rain	k1gInSc1	Rain
-	-	kIx~	-
páska	páska	k1gFnSc1	páska
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Come	Come	k1gFnSc1	Come
Out	Out	k1gFnSc1	Out
-	-	kIx~	-
páska	páska	k1gFnSc1	páska
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Piano	piano	k1gNnSc1	piano
Phase	Phase	k1gFnSc2	Phase
-	-	kIx~	-
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgFnPc4	dva
marimby	marimba	k1gFnPc4	marimba
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Violin	violina	k1gFnPc2	violina
Phase	Phase	k1gFnSc2	Phase
-	-	kIx~	-
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
housle	housle	k1gFnPc4	housle
na	na	k7c6	na
pásce	páska	k1gFnSc6	páska
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Music	Musice	k1gFnPc2	Musice
for	forum	k1gNnPc2	forum
18	[number]	k4	18
Musicians	Musicians	k1gInSc1	Musicians
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Music	Music	k1gMnSc1	Music
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Large	Large	k1gFnSc1	Large
Ensemble	ensemble	k1gInSc1	ensemble
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Octet	Octet	k1gInSc1	Octet
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tehillim	Tehillim	k1gInSc1	Tehillim
pro	pro	k7c4	pro
vokály	vokál	k1gInPc4	vokál
a	a	k8xC	a
soubor	soubor	k1gInSc1	soubor
-	-	kIx~	-
zhudebnění	zhudebnění	k1gNnSc1	zhudebnění
žalmů	žalm	k1gInPc2	žalm
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Different	Different	k1gInSc1	Different
Trains	Trainsa	k1gFnPc2	Trainsa
pro	pro	k7c4	pro
smyčcové	smyčcový	k2eAgNnSc4d1	smyčcové
kvarteto	kvarteto	k1gNnSc4	kvarteto
a	a	k8xC	a
magnetofonovou	magnetofonový	k2eAgFnSc4d1	magnetofonová
pásku	páska	k1gFnSc4	páska
-	-	kIx~	-
hudební	hudební	k2eAgFnSc1d1	hudební
skladba	skladba	k1gFnSc1	skladba
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
holokaustu	holokaust	k1gInSc2	holokaust
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
současnou	současný	k2eAgFnSc4d1	současná
klasickou	klasický	k2eAgFnSc4d1	klasická
hudební	hudební	k2eAgFnSc4d1	hudební
skladbu	skladba	k1gFnSc4	skladba
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Double	double	k2eAgInSc1d1	double
Sextet	sextet	k1gInSc1	sextet
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pulitzerova	Pulitzerův	k2eAgFnSc1d1	Pulitzerova
cena	cena	k1gFnSc1	cena
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Steve	Steve	k1gMnSc1	Steve
Reich	Reich	k?	Reich
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Steve	Steve	k1gMnSc1	Steve
Reich	Reich	k?	Reich
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
Steve	Steve	k1gMnSc1	Steve
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Steve	Steve	k1gMnSc1	Steve
Reich	Reich	k?	Reich
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
