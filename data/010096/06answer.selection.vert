<s>
Steve	Steve	k1gMnSc1	Steve
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Michael	Michael	k1gMnSc1	Michael
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
