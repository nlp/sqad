<p>
<s>
Dynamika	dynamika	k1gFnSc1	dynamika
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
příčinami	příčina	k1gFnPc7	příčina
pohybu	pohyb	k1gInSc3	pohyb
hmotných	hmotný	k2eAgInPc2d1	hmotný
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
soustav	soustava	k1gFnPc2	soustava
těles	těleso	k1gNnPc2	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
veličinami	veličina	k1gFnPc7	veličina
spojenými	spojený	k2eAgInPc7d1	spojený
s	s	k7c7	s
dynamikou	dynamika	k1gFnSc7	dynamika
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
hybnost	hybnost	k1gFnSc1	hybnost
a	a	k8xC	a
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
cílů	cíl	k1gInPc2	cíl
dynamiky	dynamika	k1gFnSc2	dynamika
je	být	k5eAaImIp3nS	být
určit	určit	k5eAaPmF	určit
pohyb	pohyb	k1gInSc1	pohyb
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
tělesa	těleso	k1gNnSc2	těleso
nebo	nebo	k8xC	nebo
těles	těleso	k1gNnPc2	těleso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známe	znát	k5eAaImIp1nP	znát
<g/>
-li	i	k?	-li
síly	síla	k1gFnPc1	síla
na	na	k7c4	na
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
(	(	kIx(	(
<g/>
těleso	těleso	k1gNnSc1	těleso
nebo	nebo	k8xC	nebo
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
působící	působící	k2eAgFnPc1d1	působící
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
určení	určení	k1gNnSc4	určení
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc2	rychlost
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
(	(	kIx(	(
<g/>
tělesa	těleso	k1gNnSc2	těleso
nebo	nebo	k8xC	nebo
těles	těleso	k1gNnPc2	těleso
<g/>
)	)	kIx)	)
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Dobrým	dobrý	k2eAgInSc7d1	dobrý
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
pohyb	pohyb	k1gInSc4	pohyb
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
známe	znát	k5eAaImIp1nP	znát
dynamický	dynamický	k2eAgInSc4d1	dynamický
zákon	zákon	k1gInSc4	zákon
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
planety	planeta	k1gFnPc1	planeta
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
co	co	k3yInSc1	co
nás	my	k3xPp1nPc4	my
opravdu	opravdu	k6eAd1	opravdu
zajímá	zajímat	k5eAaImIp3nS	zajímat
je	on	k3xPp3gInPc4	on
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
planety	planeta	k1gFnPc1	planeta
v	v	k7c6	v
budoucím	budoucí	k2eAgInSc6d1	budoucí
čase	čas	k1gInSc6	čas
nalézat	nalézat	k5eAaImF	nalézat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
integrací	integrace	k1gFnPc2	integrace
Newtonových	Newtonových	k2eAgFnPc2d1	Newtonových
rovnic	rovnice	k1gFnPc2	rovnice
(	(	kIx(	(
<g/>
pohybové	pohybový	k2eAgFnPc1d1	pohybová
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
,	,	kIx,	,
rovnice	rovnice	k1gFnPc1	rovnice
dynamiky	dynamika	k1gFnSc2	dynamika
v	v	k7c6	v
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
)	)	kIx)	)
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nerelativistické	relativistický	k2eNgFnSc2d1	nerelativistická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mechanice	mechanika	k1gFnSc6	mechanika
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc1	všechen
síly	síla	k1gFnPc1	síla
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c4	na
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
(	(	kIx(	(
<g/>
těleso	těleso	k1gNnSc1	těleso
nebo	nebo	k8xC	nebo
tělesa	těleso	k1gNnPc1	těleso
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
experimentálně	experimentálně	k6eAd1	experimentálně
objevené	objevený	k2eAgInPc1d1	objevený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Skládání	skládání	k1gNnSc1	skládání
sil	síla	k1gFnPc2	síla
</s>
</p>
<p>
<s>
Rovnováha	rovnováha	k1gFnSc1	rovnováha
sil	síla	k1gFnPc2	síla
</s>
</p>
<p>
<s>
Rozklad	rozklad	k1gInSc1	rozklad
sil	síla	k1gFnPc2	síla
</s>
</p>
<p>
<s>
Setrvačnost	setrvačnost	k1gFnSc1	setrvačnost
</s>
</p>
<p>
<s>
Hybnost	hybnost	k1gFnSc1	hybnost
</s>
</p>
<p>
<s>
Impuls	impuls	k1gInSc1	impuls
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
<s>
Dostředivá	dostředivý	k2eAgFnSc1d1	dostředivá
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Setrvačná	setrvačný	k2eAgFnSc1d1	setrvačná
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Inerciální	inerciální	k2eAgFnSc1d1	inerciální
vztažná	vztažný	k2eAgFnSc1d1	vztažná
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
Neinerciální	inerciální	k2eNgFnSc1d1	neinerciální
vztažná	vztažný	k2eAgFnSc1d1	vztažná
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
Coriolisova	Coriolisův	k2eAgFnSc1d1	Coriolisova
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Tlaková	tlakový	k2eAgFnSc1d1	tlaková
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Tlak	tlak	k1gInSc1	tlak
</s>
</p>
<p>
<s>
==	==	k?	==
Pohybové	pohybový	k2eAgInPc1d1	pohybový
zákony	zákon	k1gInPc1	zákon
==	==	k?	==
</s>
</p>
<p>
<s>
Newtonovy	Newtonův	k2eAgInPc1d1	Newtonův
pohybové	pohybový	k2eAgInPc1d1	pohybový
zákony	zákon	k1gInPc1	zákon
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
pohybový	pohybový	k2eAgInSc1d1	pohybový
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
pohybový	pohybový	k2eAgInSc1d1	pohybový
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
pohybový	pohybový	k2eAgInSc1d1	pohybový
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
akce	akce	k1gFnSc2	akce
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Síly	síla	k1gFnPc4	síla
působící	působící	k2eAgFnPc4d1	působící
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
pohybů	pohyb	k1gInPc2	pohyb
==	==	k?	==
</s>
</p>
<p>
<s>
Rovnoměrný	rovnoměrný	k2eAgInSc1d1	rovnoměrný
přímočarý	přímočarý	k2eAgInSc1d1	přímočarý
pohyb	pohyb	k1gInSc1	pohyb
</s>
</p>
<p>
<s>
Rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
zrychlený	zrychlený	k2eAgInSc4d1	zrychlený
přímočarý	přímočarý	k2eAgInSc4d1	přímočarý
pohyb	pohyb	k1gInSc4	pohyb
</s>
</p>
<p>
<s>
Nerovnoměrný	rovnoměrný	k2eNgInSc1d1	nerovnoměrný
přímočarý	přímočarý	k2eAgInSc1d1	přímočarý
pohyb	pohyb	k1gInSc1	pohyb
</s>
</p>
<p>
<s>
Rovnoměrný	rovnoměrný	k2eAgInSc1d1	rovnoměrný
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
kružnici	kružnice	k1gFnSc6	kružnice
</s>
</p>
<p>
<s>
Rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
zrychlený	zrychlený	k2eAgInSc4d1	zrychlený
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
kružnici	kružnice	k1gFnSc6	kružnice
</s>
</p>
<p>
<s>
Nerovnoměrný	rovnoměrný	k2eNgInSc1d1	nerovnoměrný
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
kružnici	kružnice	k1gFnSc6	kružnice
</s>
</p>
<p>
<s>
==	==	k?	==
Veličiny	veličina	k1gFnSc2	veličina
==	==	k?	==
</s>
</p>
<p>
<s>
Hybnost	hybnost	k1gFnSc1	hybnost
</s>
</p>
<p>
<s>
Dostředivá	dostředivý	k2eAgFnSc1d1	dostředivá
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Setrvačná	setrvačný	k2eAgFnSc1d1	setrvačná
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Coriolisova	Coriolisův	k2eAgFnSc1d1	Coriolisova
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Mechanika	mechanika	k1gFnSc1	mechanika
</s>
</p>
<p>
<s>
Kinematika	kinematika	k1gFnSc1	kinematika
</s>
</p>
<p>
<s>
Statika	statika	k1gFnSc1	statika
</s>
</p>
<p>
<s>
Teoretická	teoretický	k2eAgFnSc1d1	teoretická
mechanika	mechanika	k1gFnSc1	mechanika
</s>
</p>
