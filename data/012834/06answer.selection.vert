<s>
Dynamika	dynamika	k1gFnSc1	dynamika
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
příčinami	příčina	k1gFnPc7	příčina
pohybu	pohyb	k1gInSc3	pohyb
hmotných	hmotný	k2eAgInPc2d1	hmotný
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
soustav	soustava	k1gFnPc2	soustava
těles	těleso	k1gNnPc2	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
