<s>
Hangul	Hangul	k1gInSc1	Hangul
<g/>
,	,	kIx,	,
korejsky	korejsky	k6eAd1	korejsky
<g/>
:	:	kIx,	:
한	한	k?	한
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
přepisováno	přepisován	k2eAgNnSc1d1	přepisováno
jinak	jinak	k6eAd1	jinak
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
hangŭ	hangŭ	k?	hangŭ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
hangeul	hangeout	k5eAaPmAgMnS	hangeout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
hangyl	hangyl	k1gInSc1	hangyl
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
je	být	k5eAaImIp3nS	být
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
čosongul	čosongul	k1gInSc1	čosongul
-	-	kIx~	-
조	조	k?	조
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
korejské	korejský	k2eAgNnSc4d1	korejské
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
