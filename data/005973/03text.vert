<s>
Hangul	Hangul	k1gInSc1	Hangul
<g/>
,	,	kIx,	,
korejsky	korejsky	k6eAd1	korejsky
<g/>
:	:	kIx,	:
한	한	k?	한
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
přepisováno	přepisován	k2eAgNnSc1d1	přepisováno
jinak	jinak	k6eAd1	jinak
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
hangŭ	hangŭ	k?	hangŭ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
hangeul	hangeout	k5eAaPmAgMnS	hangeout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
hangyl	hangyl	k1gInSc1	hangyl
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
je	být	k5eAaImIp3nS	být
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
čosongul	čosongul	k1gInSc1	čosongul
-	-	kIx~	-
조	조	k?	조
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
korejské	korejský	k2eAgNnSc4d1	korejské
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
v	v	k7c4	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
naučit	naučit	k5eAaPmF	naučit
za	za	k7c4	za
jediné	jediný	k2eAgNnSc4d1	jediné
dopoledne	dopoledne	k1gNnSc4	dopoledne
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
mála	málo	k4c2	málo
skutečně	skutečně	k6eAd1	skutečně
užívaných	užívaný	k2eAgNnPc2d1	užívané
písem	písmo	k1gNnPc2	písmo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nevznikly	vzniknout	k5eNaPmAgInP	vzniknout
odvozením	odvození	k1gNnPc3	odvození
(	(	kIx(	(
<g/>
nápodobou	nápodoba	k1gFnSc7	nápodoba
a	a	k8xC	a
úpravou	úprava	k1gFnSc7	úprava
<g/>
)	)	kIx)	)
z	z	k7c2	z
jejich	jejich	k3xOp3gMnPc2	jejich
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
po	po	k7c4	po
současná	současný	k2eAgNnPc4d1	současné
písma	písmo	k1gNnPc4	písmo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
latinka	latinka	k1gFnSc1	latinka
<g/>
,	,	kIx,	,
arabské	arabský	k2eAgNnSc1d1	arabské
písmo	písmo	k1gNnSc1	písmo
nebo	nebo	k8xC	nebo
dévanágarí	dévanágarí	k1gNnSc1	dévanágarí
nebo	nebo	k8xC	nebo
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
korejských	korejský	k2eAgInPc6d1	korejský
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
téměř	téměř	k6eAd1	téměř
shodně	shodně	k6eAd1	shodně
<g/>
,	,	kIx,	,
jen	jen	k9	jen
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
odchylkami	odchylka	k1gFnPc7	odchylka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
abeceda	abeceda	k1gFnSc1	abeceda
jamo	jamo	k6eAd1	jamo
a	a	k8xC	a
metoda	metoda	k1gFnSc1	metoda
sestavování	sestavování	k1gNnSc2	sestavování
slabičných	slabičný	k2eAgInPc2d1	slabičný
znaků	znak	k1gInPc2	znak
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vytvořením	vytvoření	k1gNnSc7	vytvoření
hangulu	hangul	k1gInSc2	hangul
korejština	korejština	k1gFnSc1	korejština
neměla	mít	k5eNaImAgFnS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělaní	vzdělaný	k2eAgMnPc1d1	vzdělaný
Korejci	Korejec	k1gMnPc1	Korejec
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
jazyce	jazyk	k1gInSc6	jazyk
vyjadřovali	vyjadřovat	k5eAaImAgMnP	vyjadřovat
pomocí	pomocí	k7c2	pomocí
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
jejich	jejich	k3xOp3gNnSc7	jejich
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
studiem	studio	k1gNnSc7	studio
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
pokusy	pokus	k1gInPc1	pokus
vymyslet	vymyslet	k5eAaPmF	vymyslet
vhodnější	vhodný	k2eAgNnSc4d2	vhodnější
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
všechna	všechen	k3xTgFnSc1	všechen
tato	tento	k3xDgNnPc4	tento
písma	písmo	k1gNnPc4	písmo
však	však	k9	však
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
je	on	k3xPp3gInPc4	on
číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
jimi	on	k3xPp3gMnPc7	on
pouze	pouze	k6eAd1	pouze
vzdělaní	vzdělaný	k2eAgMnPc1d1	vzdělaný
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
si	se	k3xPyFc3	se
král	král	k1gMnSc1	král
Sedžong	Sedžong	k1gMnSc1	Sedžong
Veliký	veliký	k2eAgMnSc1d1	veliký
začal	začít	k5eAaPmAgMnS	začít
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
poddaní	poddaný	k1gMnPc1	poddaný
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
neumějí	umět	k5eNaImIp3nP	umět
číst	číst	k5eAaImF	číst
ani	ani	k8xC	ani
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
pocit	pocit	k1gInSc4	pocit
křivdy	křivda	k1gFnSc2	křivda
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
nemohli	moct	k5eNaImAgMnP	moct
předkládat	předkládat	k5eAaImF	předkládat
své	svůj	k3xOyFgFnPc4	svůj
stížnosti	stížnost	k1gFnPc4	stížnost
úřadům	úřad	k1gInPc3	úřad
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
ústně	ústně	k6eAd1	ústně
<g/>
.	.	kIx.	.
</s>
<s>
Krále	Král	k1gMnSc2	Král
Sedžonga	Sedžong	k1gMnSc2	Sedžong
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgMnSc6	jenž
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
vždy	vždy	k6eAd1	vždy
ochotně	ochotně	k6eAd1	ochotně
naslouchá	naslouchat	k5eAaImIp3nS	naslouchat
prostým	prostý	k2eAgInSc7d1	prostý
lidem	lid	k1gInSc7	lid
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
znepokojoval	znepokojovat	k5eAaImAgInS	znepokojovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zasadil	zasadit	k5eAaPmAgInS	zasadit
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
mluvené	mluvený	k2eAgFnSc3d1	mluvená
korejštině	korejština	k1gFnSc3	korejština
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
snadné	snadný	k2eAgNnSc1d1	snadné
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
naučit	naučit	k5eAaPmF	naučit
a	a	k8xC	a
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Dokončení	dokončení	k1gNnSc1	dokončení
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1446	[number]	k4	1446
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
popisující	popisující	k2eAgFnSc1d1	popisující
nové	nový	k2eAgNnSc4d1	nové
písmo	písmo	k1gNnSc4	písmo
byl	být	k5eAaImAgInS	být
nazván	nazván	k2eAgInSc1d1	nazván
hunmindžongum	hunmindžongum	k1gInSc1	hunmindžongum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
systém	systém	k1gInSc1	systém
správných	správný	k2eAgFnPc2d1	správná
hlásek	hláska	k1gFnPc2	hláska
pro	pro	k7c4	pro
vzdělání	vzdělání	k1gNnSc4	vzdělání
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
svého	svůj	k3xOyFgNnSc2	svůj
prohlášení	prohlášení	k1gNnSc2	prohlášení
král	král	k1gMnSc1	král
Sedžong	Sedžong	k1gMnSc1	Sedžong
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
Vzdělanci	vzdělanec	k1gMnPc1	vzdělanec
však	však	k9	však
dlouho	dlouho	k6eAd1	dlouho
nechtěli	chtít	k5eNaImAgMnP	chtít
hangul	hangul	k1gInSc4	hangul
přijmout	přijmout	k5eAaPmF	přijmout
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
tak	tak	k9	tak
velice	velice	k6eAd1	velice
snadné	snadný	k2eAgNnSc1d1	snadné
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
naučit	naučit	k5eAaPmF	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Posměšně	posměšně	k6eAd1	posměšně
mu	on	k3xPp3gMnSc3	on
říkali	říkat	k5eAaImAgMnP	říkat
amgul	amgul	k1gInSc4	amgul
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ženská	ženský	k2eAgFnSc1d1	ženská
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Opovrhovali	opovrhovat	k5eAaImAgMnP	opovrhovat
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
naučit	naučit	k5eAaPmF	naučit
i	i	k9	i
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
jinak	jinak	k6eAd1	jinak
číst	číst	k5eAaImF	číst
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
neučily	učít	k5eNaPmAgFnP	učít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
tak	tak	k6eAd1	tak
uplynula	uplynout	k5eAaPmAgFnS	uplynout
celá	celý	k2eAgNnPc4d1	celé
čtyři	čtyři	k4xCgNnPc4	čtyři
staletí	staletí	k1gNnPc4	staletí
<g/>
,	,	kIx,	,
než	než	k8xS	než
korejská	korejský	k2eAgFnSc1d1	Korejská
vláda	vláda	k1gFnSc1	vláda
veřejně	veřejně	k6eAd1	veřejně
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hangul	hangul	k1gInSc1	hangul
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
úředních	úřední	k2eAgInPc6d1	úřední
dokladech	doklad	k1gInPc6	doklad
<g/>
.	.	kIx.	.
</s>
<s>
Korejská	korejský	k2eAgNnPc1d1	korejské
slova	slovo	k1gNnPc1	slovo
jsou	být	k5eAaImIp3nP	být
sestavena	sestavit	k5eAaPmNgNnP	sestavit
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
slabik	slabika	k1gFnPc2	slabika
a	a	k8xC	a
po	po	k7c6	po
slabikách	slabika	k1gFnPc6	slabika
se	se	k3xPyFc4	se
také	také	k9	také
korejština	korejština	k1gFnSc1	korejština
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
do	do	k7c2	do
pomyslných	pomyslný	k2eAgInPc2d1	pomyslný
přibližně	přibližně	k6eAd1	přibližně
stejných	stejný	k2eAgInPc2d1	stejný
čtverečků	čtvereček	k1gInPc2	čtvereček
obdobně	obdobně	k6eAd1	obdobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
korejské	korejský	k2eAgFnPc1d1	Korejská
slabiky	slabika	k1gFnPc1	slabika
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
nebo	nebo	k8xC	nebo
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
-	-	kIx~	-
písmen	písmeno	k1gNnPc2	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
jamo	jamo	k1gNnSc1	jamo
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
:	:	kIx,	:
písmeno	písmeno	k1gNnSc1	písmeno
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vlevo	vlevo	k6eAd1	vlevo
nahoru	nahoru	k6eAd1	nahoru
prostřední	prostřední	k2eAgFnSc1d1	prostřední
část	část	k1gFnSc1	část
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Má	můj	k3xOp1gFnSc1	můj
<g/>
-li	i	k?	-li
prostřední	prostřední	k2eAgFnSc1d1	prostřední
samohláska	samohláska	k1gFnSc1	samohláska
tvar	tvar	k1gInSc1	tvar
svislé	svislý	k2eAgFnSc2d1	svislá
čáry	čára	k1gFnSc2	čára
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
samohlásky	samohláska	k1gFnPc1	samohláska
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
vodorovných	vodorovný	k2eAgFnPc2d1	vodorovná
čar	čára	k1gFnPc2	čára
se	se	k3xPyFc4	se
píšou	psát	k5eAaImIp3nP	psát
pod	pod	k7c4	pod
první	první	k4xOgFnSc4	první
hlásku	hláska	k1gFnSc4	hláska
<g/>
.	.	kIx.	.
</s>
<s>
Hlásková	hláskový	k2eAgNnPc1d1	hláskové
písmena	písmeno	k1gNnPc1	písmeno
jamo	jamo	k6eAd1	jamo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
pro	pro	k7c4	pro
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
zdvojena	zdvojen	k2eAgFnSc1d1	zdvojena
a	a	k8xC	a
písmena	písmeno	k1gNnPc1	písmeno
složených	složený	k2eAgFnPc2d1	složená
samohlásek	samohláska	k1gFnPc2	samohláska
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stlačit	stlačit	k5eAaPmF	stlačit
k	k	k7c3	k
sobě	se	k3xPyFc3	se
a	a	k8xC	a
napsat	napsat	k5eAaBmF	napsat
tak	tak	k9	tak
jednu	jeden	k4xCgFnSc4	jeden
vedle	vedle	k7c2	vedle
druhé	druhý	k4xOgFnSc2	druhý
<g/>
.	.	kIx.	.
obvykle	obvykle	k6eAd1	obvykle
také	také	k9	také
z	z	k7c2	z
písmena	písmeno	k1gNnSc2	písmeno
koncové	koncový	k2eAgFnSc2d1	koncová
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slabiky	slabika	k1gFnSc2	slabika
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
dole	dole	k6eAd1	dole
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hangulu	hangulum	k1gNnSc6	hangulum
možné	možný	k2eAgNnSc1d1	možné
zapsat	zapsat	k5eAaPmF	zapsat
tisíce	tisíc	k4xCgInPc4	tisíc
různých	různý	k2eAgFnPc2d1	různá
slabik	slabika	k1gFnPc2	slabika
a	a	k8xC	a
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
se	se	k3xPyFc4	se
písmena	písmeno	k1gNnSc2	písmeno
jamo	jamo	k6eAd1	jamo
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
čtou	číst	k5eAaImIp3nP	číst
zleva	zleva	k6eAd1	zleva
nahoře	nahoře	k6eAd1	nahoře
směrem	směr	k1gInSc7	směr
doprava	doprava	k6eAd1	doprava
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Korejština	korejština	k1gFnSc1	korejština
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
21	[number]	k4	21
samohlásek	samohláska	k1gFnPc2	samohláska
-	-	kIx~	-
10	[number]	k4	10
základních	základní	k2eAgFnPc2d1	základní
samohlásek	samohláska	k1gFnPc2	samohláska
a	a	k8xC	a
11	[number]	k4	11
jejich	jejich	k3xOp3gFnPc2	jejich
kombinací	kombinace	k1gFnPc2	kombinace
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
níže	nízce	k6eAd2	nízce
dle	dle	k7c2	dle
česká	český	k2eAgFnSc1d1	Česká
vědecká	vědecký	k2eAgFnSc1d1	vědecká
transkripce	transkripce	k1gFnSc1	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
Korejština	korejština	k1gFnSc1	korejština
má	mít	k5eAaImIp3nS	mít
14	[number]	k4	14
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
souhlásky	souhláska	k1gFnSc2	souhláska
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
předchozí	předchozí	k2eAgFnSc6d1	předchozí
samohlásce	samohláska	k1gFnSc6	samohláska
nebo	nebo	k8xC	nebo
znělé	znělý	k2eAgFnSc6d1	znělá
<g/>
/	/	kIx~	/
<g/>
počáteční	počáteční	k2eAgFnSc6d1	počáteční
souhlásce	souhláska	k1gFnSc6	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
a	a	k8xC	a
transliterace	transliterace	k1gFnSc1	transliterace
doznává	doznávat	k5eAaImIp3nS	doznávat
jistých	jistý	k2eAgFnPc2d1	jistá
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
kombinaci	kombinace	k1gFnSc6	kombinace
ukončovací	ukončovací	k2eAgFnSc3d1	ukončovací
souhlásce	souhláska	k1gFnSc3	souhláska
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
počáteční	počáteční	k2eAgFnSc3d1	počáteční
souhlásce	souhláska	k1gFnSc3	souhláska
znaku	znak	k1gInSc2	znak
dalšího	další	k1gNnSc2	další
Souhlásky	souhláska	k1gFnSc2	souhláska
a	a	k8xC	a
samohlásky	samohláska	k1gFnSc2	samohláska
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
po	po	k7c6	po
slabikách	slabika	k1gFnPc6	slabika
do	do	k7c2	do
čtverce	čtverec	k1gInSc2	čtverec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
slabika	slabika	k1gFnSc1	slabika
začíná	začínat	k5eAaImIp3nS	začínat
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
,	,	kIx,	,
vkládá	vkládat	k5eAaImIp3nS	vkládat
se	se	k3xPyFc4	se
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
formální	formální	k2eAgInSc1d1	formální
znak	znak	k1gInSc1	znak
ᄋ	ᄋ	k?	ᄋ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nevyslovuje	vyslovovat	k5eNaImIp3nS	vyslovovat
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlivky	vysvětlivka	k1gFnPc4	vysvětlivka
k	k	k7c3	k
příkladům	příklad	k1gInPc3	příklad
<g/>
:	:	kIx,	:
C	C	kA	C
-	-	kIx~	-
souhláska	souhláska	k1gFnSc1	souhláska
(	(	kIx(	(
<g/>
konsonant	konsonant	k1gInSc1	konsonant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
V	V	kA	V
-	-	kIx~	-
samohláska	samohláska	k1gFnSc1	samohláska
(	(	kIx(	(
<g/>
vokál	vokál	k1gInSc1	vokál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Exonymum	Exonymum	k1gInSc1	Exonymum
Jazykové	jazykový	k2eAgFnSc2d1	jazyková
skupiny	skupina	k1gFnSc2	skupina
Pravopis	pravopis	k1gInSc1	pravopis
SAMPA	SAMPA	kA	SAMPA
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gInPc2	jazyk
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
skupin	skupina	k1gFnPc2	skupina
<g/>
)	)	kIx)	)
Výslovnost	výslovnost	k1gFnSc1	výslovnost
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hangul	Hangula	k1gFnPc2	Hangula
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
