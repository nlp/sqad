<s>
Teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
sada	sada	k1gFnSc1	sada
dvou	dva	k4xCgFnPc2	dva
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
teorií	teorie	k1gFnPc2	teorie
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
:	:	kIx,	:
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
STR	str	kA	str
<g/>
)	)	kIx)	)
a	a	k8xC	a
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
OTR	OTR	kA	OTR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
teorie	teorie	k1gFnPc1	teorie
si	se	k3xPyFc3	se
za	za	k7c2	za
cíl	cíl	k1gInSc1	cíl
daly	dát	k5eAaPmAgInP	dát
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
vlnění	vlnění	k1gNnSc1	vlnění
se	se	k3xPyFc4	se
nechová	chovat	k5eNaImIp3nS	chovat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Newtonovými	Newtonův	k2eAgInPc7d1	Newtonův
pohybovými	pohybový	k2eAgInPc7d1	pohybový
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektromagnetické	elektromagnetický	k2eAgFnPc1d1	elektromagnetická
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
konstantní	konstantní	k2eAgFnSc7d1	konstantní
rychlostí	rychlost	k1gFnSc7	rychlost
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
obou	dva	k4xCgFnPc2	dva
teorií	teorie	k1gFnPc2	teorie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgMnPc1	dva
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
relativně	relativně	k6eAd1	relativně
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nP	zjistit
mezi	mezi	k7c7	mezi
danými	daný	k2eAgInPc7d1	daný
dvěma	dva	k4xCgInPc7	dva
událostmi	událost	k1gFnPc7	událost
různé	různý	k2eAgFnPc1d1	různá
časové	časový	k2eAgFnPc1d1	časová
i	i	k8xC	i
prostorové	prostorový	k2eAgInPc1d1	prostorový
intervaly	interval	k1gInPc1	interval
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
na	na	k7c4	na
oba	dva	k4xCgInPc4	dva
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
stejné	stejný	k2eAgNnSc1d1	stejné
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
nejzákladnější	základní	k2eAgFnSc7d3	nejzákladnější
(	(	kIx(	(
<g/>
a	a	k8xC	a
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
<g/>
)	)	kIx)	)
teorie	teorie	k1gFnSc1	teorie
moderní	moderní	k2eAgFnSc2d1	moderní
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
umožnila	umožnit	k5eAaPmAgFnS	umožnit
pochopit	pochopit	k5eAaPmF	pochopit
význam	význam	k1gInSc4	význam
některých	některý	k3yIgFnPc2	některý
přírodních	přírodní	k2eAgFnPc2d1	přírodní
zákonitostí	zákonitost	k1gFnPc2	zákonitost
při	při	k7c6	při
rychlostech	rychlost	k1gFnPc6	rychlost
srovnatelných	srovnatelný	k2eAgInPc2d1	srovnatelný
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
principech	princip	k1gInPc6	princip
použitých	použitý	k2eAgInPc2d1	použitý
při	při	k7c6	při
formulaci	formulace	k1gFnSc6	formulace
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
pak	pak	k6eAd1	pak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
celá	celý	k2eAgFnSc1d1	celá
relativistická	relativistický	k2eAgFnSc1d1	relativistická
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
fyzika	fyzik	k1gMnSc4	fyzik
studující	studující	k1gFnSc4	studující
především	především	k9	především
jevy	jev	k1gInPc4	jev
probíhající	probíhající	k2eAgInPc4d1	probíhající
při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
relativistická	relativistický	k2eAgFnSc1d1	relativistická
mechanika	mechanika	k1gFnSc1	mechanika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
mechanickými	mechanický	k2eAgInPc7d1	mechanický
jevy	jev	k1gInPc7	jev
při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nejdříve	dříve	k6eAd3	dříve
tzv.	tzv.	kA	tzv.
speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
platnost	platnost	k1gFnSc1	platnost
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
inerciální	inerciální	k2eAgFnPc4d1	inerciální
vztažné	vztažný	k2eAgFnPc4d1	vztažná
soustavy	soustava	k1gFnPc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
také	také	k9	také
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
popis	popis	k1gInSc4	popis
relativistických	relativistický	k2eAgInPc2d1	relativistický
jevů	jev	k1gInPc2	jev
také	také	k9	také
v	v	k7c6	v
soustavách	soustava	k1gFnPc6	soustava
neinerciálních	inerciální	k2eNgFnPc6d1	neinerciální
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
umožnila	umožnit	k5eAaPmAgFnS	umožnit
pochopení	pochopení	k1gNnSc4	pochopení
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
časem	čas	k1gInSc7	čas
a	a	k8xC	a
hmotou	hmota	k1gFnSc7	hmota
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
formulaci	formulace	k1gFnSc3	formulace
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
gravitační	gravitační	k2eAgFnSc2d1	gravitační
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
anglicky	anglicky	k6eAd1	anglicky
mluvícího	mluvící	k2eAgInSc2d1	mluvící
světa	svět	k1gInSc2	svět
ji	on	k3xPp3gFnSc4	on
šířil	šířit	k5eAaImAgMnS	šířit
Arthur	Arthur	k1gMnSc1	Arthur
Eddington	Eddington	k1gInSc4	Eddington
<g/>
.	.	kIx.	.
</s>
<s>
Sledování	sledování	k1gNnSc1	sledování
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
pohybu	pohyb	k1gInSc2	pohyb
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
vztahováno	vztahovat	k5eAaImNgNnS	vztahovat
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
zvolené	zvolený	k2eAgFnSc3d1	zvolená
vztažné	vztažný	k2eAgFnSc3d1	vztažná
soustavě	soustava	k1gFnSc3	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
určit	určit	k5eAaPmF	určit
vztažnou	vztažný	k2eAgFnSc4d1	vztažná
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
nebyli	být	k5eNaImAgMnP	být
bychom	by	kYmCp1nP	by
schopni	schopen	k2eAgMnPc1d1	schopen
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
pohybujeme	pohybovat	k5eAaImIp1nP	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
nelze	lze	k6eNd1	lze
bez	bez	k7c2	bez
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
existovala	existovat	k5eAaImAgFnS	existovat
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
univerzální	univerzální	k2eAgFnSc2d1	univerzální
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
celý	celý	k2eAgInSc4d1	celý
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vztahovat	vztahovat	k5eAaImF	vztahovat
veškerý	veškerý	k3xTgInSc4	veškerý
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
soustava	soustava	k1gFnSc1	soustava
byla	být	k5eAaImAgFnS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
éter	éter	k1gInSc1	éter
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
vůči	vůči	k7c3	vůči
prostoru	prostor	k1gInSc3	prostor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Experimenty	experiment	k1gInPc1	experiment
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
pokoušely	pokoušet	k5eAaImAgInP	pokoušet
určit	určit	k5eAaPmF	určit
relativní	relativní	k2eAgFnSc4d1	relativní
rychlost	rychlost	k1gFnSc4	rychlost
pohybu	pohyb	k1gInSc2	pohyb
vůči	vůči	k7c3	vůči
éteru	éter	k1gInSc3	éter
<g/>
,	,	kIx,	,
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
důsledku	důsledek	k1gInSc6	důsledek
k	k	k7c3	k
zavrhnutí	zavrhnutí	k1gNnSc3	zavrhnutí
existence	existence	k1gFnSc2	existence
univerzální	univerzální	k2eAgFnSc2d1	univerzální
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
k	k	k7c3	k
představě	představa	k1gFnSc3	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
děje	děj	k1gInSc2	děj
lze	lze	k6eAd1	lze
zvolit	zvolit	k5eAaPmF	zvolit
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
vztažnou	vztažný	k2eAgFnSc4d1	vztažná
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
popisující	popisující	k2eAgInPc1d1	popisující
dané	daný	k2eAgInPc1d1	daný
děje	děj	k1gInPc1	děj
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
stejný	stejný	k2eAgInSc4d1	stejný
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
splňuje	splňovat	k5eAaImIp3nS	splňovat
princip	princip	k1gInSc1	princip
korespondence	korespondence	k1gFnSc2	korespondence
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
aproximací	aproximace	k1gFnSc7	aproximace
relativistických	relativistický	k2eAgMnPc2d1	relativistický
vztahů	vztah	k1gInPc2	vztah
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
rychlosti	rychlost	k1gFnPc4	rychlost
získáme	získat	k5eAaPmIp1nP	získat
klasické	klasický	k2eAgInPc1d1	klasický
vztahy	vztah	k1gInPc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
např.	např.	kA	např.
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
rychlostí	rychlost	k1gFnPc2	rychlost
apod.	apod.	kA	apod.
Také	také	k9	také
z	z	k7c2	z
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
slabá	slabý	k2eAgNnPc4d1	slabé
pole	pole	k1gNnPc4	pole
získat	získat	k5eAaPmF	získat
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
gravitační	gravitační	k2eAgInSc1d1	gravitační
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
"	"	kIx"	"
<g/>
O	o	k7c6	o
elektrodynamice	elektrodynamika	k1gFnSc6	elektrodynamika
pohybujících	pohybující	k2eAgNnPc2d1	pohybující
se	se	k3xPyFc4	se
těles	těleso	k1gNnPc2	těleso
<g/>
"	"	kIx"	"
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Einstein	Einstein	k1gMnSc1	Einstein
speciální	speciální	k2eAgFnSc4d1	speciální
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
relativita	relativita	k1gFnSc1	relativita
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
pouze	pouze	k6eAd1	pouze
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
soustavách	soustava	k1gFnPc6	soustava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
přímočaře	přímočaro	k6eAd1	přímočaro
<g/>
.	.	kIx.	.
</s>
<s>
Takoví	takový	k3xDgMnPc1	takový
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
nemohou	moct	k5eNaImIp3nP	moct
pomocí	pomocí	k7c2	pomocí
žádného	žádný	k3yNgInSc2	žádný
experimentu	experiment	k1gInSc2	experiment
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
"	"	kIx"	"
<g/>
absolutním	absolutní	k2eAgInSc6d1	absolutní
klidu	klid	k1gInSc6	klid
<g/>
"	"	kIx"	"
a	a	k8xC	a
který	který	k3yRgMnSc1	který
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
tomuto	tento	k3xDgInSc3	tento
klidu	klid	k1gInSc3	klid
"	"	kIx"	"
<g/>
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
postuluje	postulovat	k5eAaImIp3nS	postulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
takové	takový	k3xDgMnPc4	takový
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
jakou	jaký	k3yIgFnSc7	jaký
rychlostí	rychlost	k1gFnSc7	rychlost
se	se	k3xPyFc4	se
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
silných	silný	k2eAgFnPc2d1	silná
stránek	stránka	k1gFnPc2	stránka
speciální	speciální	k2eAgFnSc2d1	speciální
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
z	z	k7c2	z
pouhých	pouhý	k2eAgInPc2d1	pouhý
dvou	dva	k4xCgInPc2	dva
předpokladů	předpoklad	k1gInPc2	předpoklad
<g/>
:	:	kIx,	:
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
je	být	k5eAaImIp3nS	být
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
fyziky	fyzika	k1gFnSc2	fyzika
jsou	být	k5eAaImIp3nP	být
shodné	shodný	k2eAgFnPc1d1	shodná
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
v	v	k7c6	v
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
soustavách	soustava	k1gFnPc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
byla	být	k5eAaImAgFnS	být
Einsteinem	Einstein	k1gMnSc7	Einstein
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
předtím	předtím	k6eAd1	předtím
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
čtyř	čtyři	k4xCgFnPc2	čtyři
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
,	,	kIx,	,
konaných	konaný	k2eAgInPc2d1	konaný
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
před	před	k7c7	před
Pruskou	pruský	k2eAgFnSc7d1	pruská
akademií	akademie	k1gFnSc7	akademie
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
Einsteinem	Einstein	k1gMnSc7	Einstein
již	již	k6eAd1	již
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
a	a	k8xC	a
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
některé	některý	k3yIgFnPc4	některý
rovnice	rovnice	k1gFnPc4	rovnice
německý	německý	k2eAgMnSc1d1	německý
matematik	matematik	k1gMnSc1	matematik
David	David	k1gMnSc1	David
Hilbert	Hilbert	k1gMnSc1	Hilbert
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
opakovaným	opakovaný	k2eAgNnPc3d1	opakované
obviněním	obvinění	k1gNnPc3	obvinění
Einsteina	Einstein	k1gMnSc2	Einstein
z	z	k7c2	z
plagiátorství	plagiátorství	k1gNnSc2	plagiátorství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
nepochybné	pochybný	k2eNgNnSc1d1	nepochybné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
rovnicích	rovnice	k1gFnPc6	rovnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
předmětem	předmět	k1gInSc7	předmět
sporu	spor	k1gInSc2	spor
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
původcem	původce	k1gMnSc7	původce
byl	být	k5eAaImAgMnS	být
Einstein	Einstein	k1gMnSc1	Einstein
a	a	k8xC	a
Hilbertovi	Hilbertův	k2eAgMnPc1d1	Hilbertův
ji	on	k3xPp3gFnSc4	on
objasnil	objasnit	k5eAaPmAgMnS	objasnit
při	při	k7c6	při
společných	společný	k2eAgInPc6d1	společný
hovorech	hovor	k1gInPc6	hovor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
spolu	spolu	k6eAd1	spolu
vedli	vést	k5eAaImAgMnP	vést
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
pak	pak	k6eAd1	pak
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1915	[number]	k4	1915
víceméně	víceméně	k9	víceméně
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
povědomím	povědomí	k1gNnSc7	povědomí
o	o	k7c6	o
práci	práce	k1gFnSc6	práce
kolegy	kolega	k1gMnSc2	kolega
<g/>
,	,	kIx,	,
odvodili	odvodit	k5eAaPmAgMnP	odvodit
rovnice	rovnice	k1gFnPc4	rovnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vyjádřením	vyjádření	k1gNnSc7	vyjádření
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
představuje	představovat	k5eAaImIp3nS	představovat
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
Newtonův	Newtonův	k2eAgInSc4d1	Newtonův
gravitační	gravitační	k2eAgInSc4d1	gravitační
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
gravitace	gravitace	k1gFnSc2	gravitace
používá	používat	k5eAaImIp3nS	používat
matematické	matematický	k2eAgFnSc2d1	matematická
konstrukce	konstrukce	k1gFnSc2	konstrukce
diferenciální	diferenciální	k2eAgFnSc2d1	diferenciální
geometrie	geometrie	k1gFnSc2	geometrie
a	a	k8xC	a
tenzorů	tenzor	k1gInPc2	tenzor
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
ekvivalentní	ekvivalentní	k2eAgInPc4d1	ekvivalentní
všechny	všechen	k3xTgMnPc4	všechen
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
,	,	kIx,	,
nejenom	nejenom	k6eAd1	nejenom
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
přímočaře	přímočaro	k6eAd1	přímočaro
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
"	"	kIx"	"
<g/>
obecná	obecná	k1gFnSc1	obecná
<g/>
"	"	kIx"	"
oproti	oproti	k7c3	oproti
"	"	kIx"	"
<g/>
speciální	speciální	k2eAgMnSc1d1	speciální
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
platí	platit	k5eAaImIp3nS	platit
stejné	stejný	k2eAgInPc4d1	stejný
zákony	zákon	k1gInPc4	zákon
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	s	k7c7	s
zrychlením	zrychlení	k1gNnSc7	zrychlení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
už	už	k6eAd1	už
gravitace	gravitace	k1gFnSc2	gravitace
není	být	k5eNaImIp3nS	být
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
v	v	k7c6	v
Newtonově	Newtonův	k2eAgInSc6d1	Newtonův
gravitačním	gravitační	k2eAgInSc6d1	gravitační
zákonu	zákon	k1gInSc6	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
zakřivení	zakřivení	k1gNnSc2	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
relativita	relativita	k1gFnSc1	relativita
je	být	k5eAaImIp3nS	být
geometrická	geometrický	k2eAgFnSc1d1	geometrická
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přítomnost	přítomnost	k1gFnSc1	přítomnost
hmoty	hmota	k1gFnSc2	hmota
či	či	k8xC	či
energie	energie	k1gFnSc2	energie
"	"	kIx"	"
<g/>
zakřivuje	zakřivovat	k5eAaImIp3nS	zakřivovat
<g/>
"	"	kIx"	"
časoprostor	časoprostor	k1gInSc1	časoprostor
a	a	k8xC	a
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
zakřivení	zakřivení	k1gNnSc1	zakřivení
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
dráhu	dráha	k1gFnSc4	dráha
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
dráhu	dráha	k1gFnSc4	dráha
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
princip	princip	k1gInSc1	princip
relativity	relativita	k1gFnSc2	relativita
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Výklad	výklad	k1gInSc4	výklad
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
Experimentální	experimentální	k2eAgFnSc1d1	experimentální
elektronická	elektronický	k2eAgFnSc1d1	elektronická
učebnice	učebnice	k1gFnSc1	učebnice
základů	základ	k1gInPc2	základ
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
</s>
