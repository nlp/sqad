<s>
Teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
sada	sada	k1gFnSc1	sada
dvou	dva	k4xCgFnPc2	dva
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
teorií	teorie	k1gFnPc2	teorie
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
:	:	kIx,	:
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
STR	str	kA	str
<g/>
)	)	kIx)	)
a	a	k8xC	a
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
OTR	OTR	kA	OTR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
