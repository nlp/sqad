<s>
Halucinace	halucinace	k1gFnSc1	halucinace
je	být	k5eAaImIp3nS	být
klamný	klamný	k2eAgInSc4d1	klamný
vjem	vjem	k1gInSc4	vjem
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
bez	bez	k7c2	bez
reálného	reálný	k2eAgInSc2d1	reálný
podnětu	podnět	k1gInSc2	podnět
v	v	k7c6	v
bdělém	bdělý	k2eAgInSc6d1	bdělý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Halucinující	halucinující	k2eAgMnSc1d1	halucinující
člověk	člověk	k1gMnSc1	člověk
ji	on	k3xPp3gFnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
pseudohalucinace	pseudohalucinace	k1gFnSc2	pseudohalucinace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
klamán	klamán	k2eAgMnSc1d1	klamán
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
člověka	člověk	k1gMnSc4	člověk
nelze	lze	k6eNd1	lze
ani	ani	k8xC	ani
po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
halucinace	halucinace	k1gFnSc2	halucinace
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
klam	klam	k1gInSc4	klam
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
z	z	k7c2	z
halucinace	halucinace	k1gFnSc2	halucinace
vzniknout	vzniknout	k5eAaPmF	vzniknout
blud	blud	k1gInSc4	blud
<g/>
.	.	kIx.	.
</s>
<s>
Pravé	pravý	k2eAgFnPc1d1	pravá
halucinace	halucinace	k1gFnPc1	halucinace
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
známkou	známka	k1gFnSc7	známka
poruchy	porucha	k1gFnSc2	porucha
duševního	duševní	k2eAgInSc2d1	duševní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
u	u	k7c2	u
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
,	,	kIx,	,
psychotických	psychotický	k2eAgFnPc2d1	psychotická
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
afektivních	afektivní	k2eAgFnPc2d1	afektivní
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
vyvolaných	vyvolaný	k2eAgFnPc2d1	vyvolaná
psychoaktivními	psychoaktivní	k2eAgFnPc7d1	psychoaktivní
látkami	látka	k1gFnPc7	látka
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
u	u	k7c2	u
množství	množství	k1gNnSc2	množství
onemocnění	onemocnění	k1gNnSc2	onemocnění
s	s	k7c7	s
organickým	organický	k2eAgInSc7d1	organický
podkladem	podklad	k1gInSc7	podklad
v	v	k7c6	v
centrálním	centrální	k2eAgInSc6d1	centrální
nervovém	nervový	k2eAgInSc6d1	nervový
systému	systém	k1gInSc6	systém
(	(	kIx(	(
<g/>
epilepsie	epilepsie	k1gFnSc1	epilepsie
<g/>
,	,	kIx,	,
nádory	nádor	k1gInPc1	nádor
<g/>
,	,	kIx,	,
cévní	cévní	k2eAgNnSc1d1	cévní
postižení	postižení	k1gNnSc1	postižení
<g/>
,	,	kIx,	,
neurodegenerativní	urodegenerativní	k2eNgFnPc1d1	neurodegenerativní
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
stavy	stav	k1gInPc1	stav
po	po	k7c6	po
úrazech	úraz	k1gInPc6	úraz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
smyslové	smyslový	k2eAgFnSc6d1	smyslová
deprivaci	deprivace	k1gFnSc6	deprivace
(	(	kIx(	(
<g/>
omezení	omezení	k1gNnSc1	omezení
smyslových	smyslový	k2eAgInPc2d1	smyslový
podnětů	podnět	k1gInPc2	podnět
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
však	však	k9	však
vyvolat	vyvolat	k5eAaPmF	vyvolat
i	i	k9	i
u	u	k7c2	u
jinak	jinak	k6eAd1	jinak
zdravého	zdravý	k2eAgMnSc2d1	zdravý
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
iluze	iluze	k1gFnSc2	iluze
se	se	k3xPyFc4	se
halucinace	halucinace	k1gFnSc1	halucinace
liší	lišit	k5eAaImIp3nS	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
bez	bez	k7c2	bez
podnětu	podnět	k1gInSc2	podnět
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
základem	základ	k1gInSc7	základ
iluze	iluze	k1gFnSc1	iluze
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
podráždění	podráždění	k1gNnSc1	podráždění
smyslového	smyslový	k2eAgInSc2d1	smyslový
receptoru	receptor	k1gInSc2	receptor
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
chybně	chybně	k6eAd1	chybně
interpretováno	interpretovat	k5eAaBmNgNnS	interpretovat
<g/>
.	.	kIx.	.
</s>
<s>
Blud	blud	k1gInSc1	blud
se	se	k3xPyFc4	se
v	v	k7c6	v
základním	základní	k2eAgNnSc6d1	základní
vymezení	vymezení	k1gNnSc6	vymezení
od	od	k7c2	od
halucinace	halucinace	k1gFnSc2	halucinace
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
halucinace	halucinace	k1gFnSc1	halucinace
je	být	k5eAaImIp3nS	být
poruchou	porucha	k1gFnSc7	porucha
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
blud	blud	k1gInSc1	blud
je	být	k5eAaImIp3nS	být
poruchou	porucha	k1gFnSc7	porucha
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
složitosti	složitost	k1gFnSc2	složitost
se	se	k3xPyFc4	se
halucinace	halucinace	k1gFnPc1	halucinace
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
elementární	elementární	k2eAgInPc4d1	elementární
-	-	kIx~	-
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
podněty	podnět	k1gInPc4	podnět
jako	jako	k8xS	jako
tóny	tón	k1gInPc4	tón
<g/>
,	,	kIx,	,
záblesky	záblesk	k1gInPc4	záblesk
<g/>
,	,	kIx,	,
komplexní	komplexní	k2eAgInSc4d1	komplexní
-	-	kIx~	-
celé	celý	k2eAgFnPc1d1	celá
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
srozumitelné	srozumitelný	k2eAgFnPc1d1	srozumitelná
věty	věta	k1gFnPc1	věta
<g/>
,	,	kIx,	,
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
-	-	kIx~	-
halucinace	halucinace	k1gFnPc1	halucinace
"	"	kIx"	"
<g/>
vnímané	vnímaný	k2eAgInPc1d1	vnímaný
<g/>
"	"	kIx"	"
více	hodně	k6eAd2	hodně
smysly	smysl	k1gInPc1	smysl
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
např.	např.	kA	např.
mluvící	mluvící	k2eAgFnSc1d1	mluvící
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
domnělé	domnělý	k2eAgFnSc2d1	domnělá
lokalizace	lokalizace	k1gFnSc2	lokalizace
smyslovým	smyslový	k2eAgInSc7d1	smyslový
orgánem	orgán	k1gInSc7	orgán
se	se	k3xPyFc4	se
halucinace	halucinace	k1gFnSc1	halucinace
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
<g/>
:	:	kIx,	:
zrakové	zrakový	k2eAgFnSc6d1	zraková
-	-	kIx~	-
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
komplexní	komplexní	k2eAgMnPc1d1	komplexní
(	(	kIx(	(
<g/>
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
celé	celý	k2eAgFnPc4d1	celá
scény	scéna	k1gFnPc4	scéna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
makropsie	makropsie	k1gFnSc1	makropsie
-	-	kIx~	-
objekty	objekt	k1gInPc1	objekt
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
být	být	k5eAaImF	být
větší	veliký	k2eAgInPc1d2	veliký
<g/>
,	,	kIx,	,
mikropsie	mikropsie	k1gFnPc1	mikropsie
-	-	kIx~	-
objekty	objekt	k1gInPc1	objekt
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
být	být	k5eAaImF	být
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
flashbacky	flashbacky	k6eAd1	flashbacky
-	-	kIx~	-
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
požili	požít	k5eAaPmAgMnP	požít
halucinogenní	halucinogenní	k2eAgFnSc4d1	halucinogenní
drogu	droga	k1gFnSc4	droga
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc4	návrat
zrakových	zrakový	k2eAgFnPc2d1	zraková
halucinací	halucinace	k1gFnPc2	halucinace
např.	např.	kA	např.
i	i	k8xC	i
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
po	po	k7c6	po
posledním	poslední	k2eAgNnSc6d1	poslední
požití	požití	k1gNnSc6	požití
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
na	na	k7c4	na
pár	pár	k4xCyI	pár
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
stav	stav	k1gInSc4	stav
prožitý	prožitý	k2eAgInSc4d1	prožitý
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
vyvolán	vyvolat	k5eAaPmNgInS	vyvolat
nějakým	nějaký	k3yIgInSc7	nějaký
podnětem	podnět	k1gInSc7	podnět
připomínající	připomínající	k2eAgNnSc1d1	připomínající
onu	onen	k3xDgFnSc4	onen
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
.	.	kIx.	.
sluchové	sluchový	k2eAgFnPc1d1	sluchová
-	-	kIx~	-
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
hlasů	hlas	k1gInPc2	hlas
jedné	jeden	k4xCgFnSc2	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
náležet	náležet	k5eAaImF	náležet
známým	známý	k2eAgInSc7d1	známý
i	i	k8xC	i
neznámým	známý	k2eNgInSc7d1	neznámý
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
Halucinace	halucinace	k1gFnSc1	halucinace
více	hodně	k6eAd2	hodně
hlasů	hlas	k1gInPc2	hlas
současně	současně	k6eAd1	současně
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
typickým	typický	k2eAgInPc3d1	typický
příznakům	příznak	k1gInPc3	příznak
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
.	.	kIx.	.
imperativní	imperativní	k2eAgInSc4d1	imperativní
-	-	kIx~	-
hlas	hlas	k1gInSc4	hlas
či	či	k8xC	či
hlasy	hlas	k1gInPc4	hlas
nemocnému	nemocný	k1gMnSc3	nemocný
něco	něco	k3yInSc4	něco
přikazují	přikazovat	k5eAaImIp3nP	přikazovat
<g/>
,	,	kIx,	,
teleologické	teleologický	k2eAgFnPc1d1	teleologická
-	-	kIx~	-
hlas	hlas	k1gInSc1	hlas
či	či	k8xC	či
hlasy	hlas	k1gInPc1	hlas
nemocnému	nemocný	k1gMnSc3	nemocný
radí	radit	k5eAaImIp3nP	radit
<g/>
,	,	kIx,	,
antagonistické	antagonistický	k2eAgInPc1d1	antagonistický
-	-	kIx~	-
protichůdné	protichůdný	k2eAgInPc1d1	protichůdný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
nemocného	nemocný	k1gMnSc2	nemocný
chválí	chválit	k5eAaImIp3nP	chválit
<g/>
,	,	kIx,	,
jiný	jiný	k2eAgInSc4d1	jiný
ho	on	k3xPp3gMnSc4	on
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
<g/>
.	.	kIx.	.
čichové	čichový	k2eAgNnSc4d1	čichové
a	a	k8xC	a
chuťové	chuťový	k2eAgNnSc4d1	chuťové
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
bludy	blud	k1gInPc7	blud
(	(	kIx(	(
<g/>
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
usiluje	usilovat	k5eAaImIp3nS	usilovat
člověku	člověk	k1gMnSc3	člověk
o	o	k7c4	o
život	život	k1gInSc4	život
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
čichovou	čichový	k2eAgFnSc7d1	čichová
halucinací	halucinace	k1gFnSc7	halucinace
dráždivého	dráždivý	k2eAgInSc2d1	dráždivý
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
tělové	tělový	k2eAgInPc4d1	tělový
<g/>
:	:	kIx,	:
hmatové	hmatový	k2eAgInPc4d1	hmatový
-	-	kIx~	-
pocity	pocit	k1gInPc1	pocit
kontaktu	kontakt	k1gInSc2	kontakt
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
svědění	svědění	k1gNnSc1	svědění
<g/>
,	,	kIx,	,
štípnutí	štípnutí	k1gNnSc1	štípnutí
vosy	vosa	k1gFnPc1	vosa
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
také	také	k9	také
<g />
.	.	kIx.	.
</s>
<s>
sexuální	sexuální	k2eAgInSc1d1	sexuální
obsah	obsah	k1gInSc1	obsah
<g/>
,	,	kIx,	,
pohybové	pohybový	k2eAgInPc1d1	pohybový
-	-	kIx~	-
pocity	pocit	k1gInPc1	pocit
neexistujícího	existující	k2eNgInSc2d1	neexistující
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
nemocný	nemocný	k1gMnSc1	nemocný
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
létá	létat	k5eAaImIp3nS	létat
<g/>
,	,	kIx,	,
vznáší	vznášet	k5eAaImIp3nS	vznášet
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
padá	padat	k5eAaImIp3nS	padat
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
verbálně	verbálně	k6eAd1	verbálně
motorické	motorický	k2eAgNnSc1d1	motorické
-	-	kIx~	-
nemocný	nemocný	k2eAgMnSc1d1	nemocný
je	být	k5eAaImIp3nS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
mluví	mluvit	k5eAaImIp3nS	mluvit
jeho	jeho	k3xOp3gNnPc7	jeho
ústy	ústa	k1gNnPc7	ústa
<g/>
,	,	kIx,	,
grafomotorické	grafomotorický	k2eAgFnPc1d1	grafomotorická
-	-	kIx~	-
nemocný	nemocný	k1gMnSc1	nemocný
je	být	k5eAaImIp3nS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
píše	psát	k5eAaImIp3nS	psát
jeho	jeho	k3xOp3gFnSc7	jeho
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
orgánové	orgán	k1gMnPc1	orgán
-	-	kIx~	-
vnímání	vnímání	k1gNnSc1	vnímání
vlastních	vlastní	k2eAgFnPc2d1	vlastní
útrob	útroba	k1gFnPc2	útroba
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
konkrétní	konkrétní	k2eAgFnSc7d1	konkrétní
představou	představa	k1gFnSc7	představa
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
změně	změna	k1gFnSc6	změna
vlastností	vlastnost	k1gFnPc2	vlastnost
či	či	k8xC	či
velikosti	velikost	k1gFnSc2	velikost
(	(	kIx(	(
<g/>
zkamenění	zkamenění	k1gNnSc1	zkamenění
<g/>
,	,	kIx,	,
odumření	odumření	k1gNnSc1	odumření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
negativní	negativní	k2eAgMnSc1d1	negativní
-	-	kIx~	-
nemocný	nemocný	k1gMnSc1	nemocný
popírá	popírat	k5eAaImIp3nS	popírat
určitou	určitý	k2eAgFnSc4d1	určitá
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
<g />
.	.	kIx.	.
</s>
<s>
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
mimo	mimo	k7c4	mimo
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
si	se	k3xPyFc3	se
ukládá	ukládat	k5eAaImIp3nS	ukládat
játra	játra	k1gNnPc4	játra
pod	pod	k7c4	pod
polštář	polštář	k1gInSc4	polštář
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
intrapsychické	intrapsychický	k2eAgNnSc4d1	intrapsychické
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
také	také	k9	také
nazvat	nazvat	k5eAaPmF	nazvat
bludnými	bludný	k2eAgFnPc7d1	bludná
produkcemi	produkce	k1gFnPc7	produkce
vyvolanými	vyvolaný	k2eAgFnPc7d1	vyvolaná
reakcí	reakce	k1gFnSc7	reakce
mozku	mozek	k1gInSc2	mozek
na	na	k7c4	na
halucinace	halucinace	k1gFnPc4	halucinace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
aktuální	aktuální	k2eAgInSc4d1	aktuální
děj	děj	k1gInSc4	děj
on-line	onin	k1gInSc5	on-lin
a	a	k8xC	a
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
napojen	napojen	k2eAgInSc1d1	napojen
na	na	k7c4	na
nějaké	nějaký	k3yIgInPc4	nějaký
dálkové	dálkový	k2eAgInPc4d1	dálkový
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
halucinacemi	halucinace	k1gFnPc7	halucinace
komentujícími	komentující	k2eAgFnPc7d1	komentující
i	i	k8xC	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
vidí	vidět	k5eAaImIp3nS	vidět
a	a	k8xC	a
dělá	dělat	k5eAaImIp3nS	dělat
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
pomocí	pomocí	k7c2	pomocí
jím	jíst	k5eAaImIp1nS	jíst
vyvolaných	vyvolaný	k2eAgFnPc2d1	vyvolaná
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
....	....	k?	....
<g/>
-	-	kIx~	-
nemocný	mocný	k2eNgMnSc1d1	nemocný
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc1	pocit
odnímání	odnímání	k1gNnSc2	odnímání
nebo	nebo	k8xC	nebo
vkládání	vkládání	k1gNnSc2	vkládání
myšlenek	myšlenka	k1gFnPc2	myšlenka
nebo	nebo	k8xC	nebo
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
vlastní	vlastní	k2eAgFnPc1d1	vlastní
myšlenky	myšlenka	k1gFnPc1	myšlenka
zveřejňovány	zveřejňován	k2eAgFnPc1d1	zveřejňována
<g/>
,	,	kIx,	,
ozvučovány	ozvučován	k2eAgFnPc1d1	ozvučován
nebo	nebo	k8xC	nebo
komentovány	komentován	k2eAgFnPc1d1	komentována
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
typický	typický	k2eAgInSc4d1	typický
příznak	příznak	k1gInSc4	příznak
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
.	.	kIx.	.
inadekvátní	inadekvátní	k2eAgMnSc1d1	inadekvátní
-	-	kIx~	-
nemocný	mocný	k2eNgMnSc1d1	nemocný
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnímá	vnímat	k5eAaImIp3nS	vnímat
okolní	okolní	k2eAgInSc4d1	okolní
svět	svět	k1gInSc4	svět
jinými	jiný	k2eAgInPc7d1	jiný
orgány	orgán	k1gInPc7	orgán
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
(	(	kIx(	(
<g/>
vidí	vidět	k5eAaImIp3nP	vidět
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
slyší	slyšet	k5eAaImIp3nS	slyšet
kolenem	koleno	k1gNnSc7	koleno
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Pro	pro	k7c4	pro
bližší	blízký	k2eAgNnSc4d2	bližší
vyjádření	vyjádření	k1gNnSc4	vyjádření
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
mít	mít	k5eAaImF	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
několik	několik	k4yIc4	několik
definic	definice	k1gFnPc2	definice
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
nám	my	k3xPp1nPc3	my
umožní	umožnit	k5eAaPmIp3nS	umožnit
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
jejich	jejich	k3xOp3gNnSc2	jejich
srovnávání	srovnávání	k1gNnSc2	srovnávání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
<g/>
,	,	kIx,	,
udělat	udělat	k5eAaPmF	udělat
si	se	k3xPyFc3	se
jasnější	jasný	k2eAgFnSc4d2	jasnější
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
halucinace	halucinace	k1gFnSc1	halucinace
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Halucinaci	halucinace	k1gFnSc4	halucinace
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
vjem	vjem	k1gInSc4	vjem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
na	na	k7c4	na
lidské	lidský	k2eAgInPc4d1	lidský
smysly	smysl	k1gInPc4	smysl
působil	působit	k5eAaImAgInS	působit
podnět	podnět	k1gInSc1	podnět
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
povaze	povaha	k1gFnSc3	povaha
těchto	tento	k3xDgInPc2	tento
vjemů	vjem	k1gInPc2	vjem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Sluchové	sluchový	k2eAgFnPc1d1	sluchová
či	či	k8xC	či
zrakové	zrakový	k2eAgFnPc1d1	zraková
halucinace	halucinace	k1gFnPc1	halucinace
způsobené	způsobený	k2eAgFnPc1d1	způsobená
požitím	požití	k1gNnSc7	požití
drogy	droga	k1gFnSc2	droga
nebo	nebo	k8xC	nebo
duševní	duševní	k2eAgFnSc7d1	duševní
chorobou	choroba	k1gFnSc7	choroba
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
vjemy	vjem	k1gInPc4	vjem
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ovšem	ovšem	k9	ovšem
vjemy	vjem	k1gInPc4	vjem
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Halucinace	halucinace	k1gFnPc1	halucinace
jsou	být	k5eAaImIp3nP	být
klamy	klam	k1gInPc4	klam
vnímaní	vnímaný	k2eAgMnPc1d1	vnímaný
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgMnPc2	jenž
pociťujeme	pociťovat	k5eAaImIp1nP	pociťovat
realitu	realita	k1gFnSc4	realita
<g/>
,	,	kIx,	,
i	i	k8xC	i
<g />
.	.	kIx.	.
</s>
<s>
když	když	k8xS	když
chybí	chybět	k5eAaImIp3nS	chybět
vnější	vnější	k2eAgFnSc1d1	vnější
podnětová	podnětový	k2eAgFnSc1d1	podnětový
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Při	při	k7c6	při
halucinacích	halucinace	k1gFnPc6	halucinace
jsou	být	k5eAaImIp3nP	být
vnímány	vnímat	k5eAaImNgInP	vnímat
určité	určitý	k2eAgInPc1d1	určitý
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
hlasy	hlas	k1gInPc1	hlas
<g/>
,	,	kIx,	,
děje	děj	k1gInPc1	děj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
percepčním	percepční	k2eAgNnSc6d1	percepční
poli	pole	k1gNnSc6	pole
subjektu	subjekt	k1gInSc2	subjekt
přítomny	přítomen	k2eAgMnPc4d1	přítomen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Halucinace	halucinace	k1gFnSc1	halucinace
je	být	k5eAaImIp3nS	být
klamný	klamný	k2eAgInSc4d1	klamný
vjem	vjem	k1gInSc4	vjem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
chorobně	chorobně	k6eAd1	chorobně
bez	bez	k7c2	bez
<g />
.	.	kIx.	.
</s>
<s>
příslušného	příslušný	k2eAgInSc2d1	příslušný
podnětu	podnět	k1gInSc2	podnět
a	a	k8xC	a
nemocný	nemocný	k1gMnSc1	nemocný
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
existenci	existence	k1gFnSc6	existence
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Halucinace	halucinace	k1gFnSc1	halucinace
<g/>
,	,	kIx,	,
vnímání	vnímání	k1gNnSc1	vnímání
předmětu	předmět	k1gInSc2	předmět
nebo	nebo	k8xC	nebo
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
osobou	osoba	k1gFnSc7	osoba
v	v	k7c6	v
bdělém	bdělý	k2eAgInSc6d1	bdělý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
iluze	iluze	k1gFnSc1	iluze
představa	představa	k1gFnSc1	představa
blud	blud	k1gInSc4	blud
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
halucinace	halucinace	k1gFnSc2	halucinace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
halucinace	halucinace	k1gFnSc2	halucinace
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
