<s>
Victoria	Victorium	k1gNnPc4
Beckham	Beckham	k1gInSc1
</s>
<s>
Victoria	Victorium	k1gNnSc2
Beckham	Beckham	k1gInSc4
Victoria	Victorium	k1gNnSc2
Beckham	Beckham	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Victoria	Victorium	k1gNnPc1
Caroline	Carolin	k1gInSc5
Adams	Adamsa	k1gFnPc2
Narození	narození	k1gNnPc4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1974	#num#	k4
(	(	kIx(
<g/>
46	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Goffs	Goffs	k1gInSc1
Oak	Oak	k1gFnSc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Žánry	žánr	k1gInPc7
</s>
<s>
Pop	pop	k1gInSc1
music	musice	k1gFnPc2
Povolání	povolání	k1gNnSc2
</s>
<s>
zpěvačka	zpěvačka	k1gFnSc1
<g/>
,	,	kIx,
textařka	textařka	k1gFnSc1
<g/>
,	,	kIx,
módní	módní	k2eAgFnSc1d1
návrhářka	návrhářka	k1gFnSc1
<g/>
,	,	kIx,
obchodnice	obchodnice	k1gFnSc1
<g/>
,	,	kIx,
modelka	modelka	k1gFnSc1
Aktivní	aktivní	k2eAgFnSc1d1
roky	rok	k1gInPc4
</s>
<s>
1994	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Spice	Spice	k1gFnSc2
Girls	girl	k1gFnPc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
důstojník	důstojník	k1gMnSc1
Řádu	řád	k1gInSc2
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
David	David	k1gMnSc1
Beckham	Beckham	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
1999	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1
BeckhamRomeo	BeckhamRomeo	k1gNnSc1
BeckhamCruz	BeckhamCruz	k1gInSc1
BeckhamHarper	BeckhamHarper	k1gMnSc1
Beckham	Beckham	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Victoria	Victoria	k1gFnSc1
Caroline	Caroline	k1gFnSc1
Beckham	Beckham	k1gInSc1
<g/>
,	,	kIx,
dívčím	dívčí	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Adams	Adamsa	k1gFnPc2
(	(	kIx(
<g/>
přechýleně	přechýleně	k6eAd1
Beckhamová	Beckhamový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Adamsová	Adamsový	k2eAgFnSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
*	*	kIx~
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1974	#num#	k4
<g/>
,	,	kIx,
Goffs	Goffs	k1gInSc1
Oak	Oak	k1gFnSc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
anglická	anglický	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
především	především	k9
pro	pro	k7c4
účinkování	účinkování	k1gNnSc4
v	v	k7c6
populární	populární	k2eAgFnSc6d1
hudební	hudební	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
Spice	Spiec	k1gInSc2
Girls	girl	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
měla	mít	k5eAaImAgFnS
přezdívku	přezdívka	k1gFnSc4
Posh	Posha	k1gFnPc2
Spice	Spice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
kapely	kapela	k1gFnSc2
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
věnovat	věnovat	k5eAaImF,k5eAaPmF
své	svůj	k3xOyFgFnSc3
kariéře	kariéra	k1gFnSc3
jako	jako	k8xS,k8xC
módní	módní	k2eAgFnSc1d1
návrhářka	návrhářka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
manželkou	manželka	k1gFnSc7
fotbalisty	fotbalista	k1gMnSc2
Davida	David	k1gMnSc2
Beckhama	Beckham	k1gMnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
má	mít	k5eAaImIp3nS
syny	syn	k1gMnPc7
Brooklyna	Brooklyn	k1gInSc2
(	(	kIx(
<g/>
podle	podle	k7c2
newyorské	newyorský	k2eAgFnSc2d1
čtvrti	čtvrt	k1gFnSc2
Brooklyn	Brooklyn	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Romea	Romeo	k1gMnSc4
<g/>
,	,	kIx,
Cruze	Cruze	k1gFnSc1
a	a	k8xC
dceru	dcera	k1gFnSc4
Harper	Harpra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.imdb.com/name/nm0065751/bio?ref_=nm_ov_bio_sm	https://www.imdb.com/name/nm0065751/bio?ref_=nm_ov_bio_sm	k6eAd1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Victoria	Victorium	k1gNnSc2
Beckhamová	Beckhamový	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
stránky	stránka	k1gFnPc4
obchodu	obchod	k1gInSc2
s	s	k7c7
módním	módní	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Victoria	Victorium	k1gNnPc1
Beckhamová	Beckhamový	k2eAgNnPc1d1
v	v	k7c6
Internet	Internet	k1gInSc1
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
29813	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
123314135	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7840	#num#	k4
3433	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
99041456	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
29734288	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
99041456	#num#	k4
</s>
