<s>
Live	Live	k1gFnSc1	Live
at	at	k?	at
Reading	Reading	k1gInSc1	Reading
je	být	k5eAaImIp3nS	být
CD	CD	kA	CD
<g/>
/	/	kIx~	/
<g/>
DVD	DVD	kA	DVD
grungeové	grungeová	k1gFnSc2	grungeová
skupiny	skupina	k1gFnSc2	skupina
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc4d1	vydané
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
koncert	koncert	k1gInSc4	koncert
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Reading	Reading	k1gInSc4	Reading
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
Kurtovy	Kurtův	k2eAgFnSc2d1	Kurtova
dcery	dcera	k1gFnSc2	dcera
Frances	Frances	k1gMnSc1	Frances
Bean	Bean	k1gMnSc1	Bean
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
na	na	k7c6	na
invalidním	invalidní	k2eAgInSc6d1	invalidní
vozíku	vozík	k1gInSc6	vozík
s	s	k7c7	s
parukou	paruka	k1gFnSc7	paruka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Zazněly	zaznět	k5eAaImAgFnP	zaznět
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
alb	album	k1gNnPc2	album
Bleach	Bleacha	k1gFnPc2	Bleacha
a	a	k8xC	a
Nevermind	Neverminda	k1gFnPc2	Neverminda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
připravovaných	připravovaný	k2eAgNnPc2d1	připravované
alb	album	k1gNnPc2	album
Incesticide	Incesticid	k1gMnSc5	Incesticid
a	a	k8xC	a
In	In	k1gMnSc5	In
Utero	Utera	k1gMnSc5	Utera
<g/>
,	,	kIx,	,
či	či	k8xC	či
raritní	raritní	k2eAgFnPc1d1	raritní
nahrávky	nahrávka	k1gFnPc1	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Zazněla	zaznít	k5eAaPmAgFnS	zaznít
i	i	k9	i
cover	cover	k1gInSc4	cover
verze	verze	k1gFnSc2	verze
písně	píseň	k1gFnSc2	píseň
The	The	k1gFnSc2	The
Money	Monea	k1gFnSc2	Monea
Will	Will	k1gMnSc1	Will
Roll	Roll	k1gMnSc1	Roll
Right	Right	k1gMnSc1	Right
In	In	k1gMnSc1	In
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Fang	Fang	k1gInSc1	Fang
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Breed	Breed	k1gInSc1	Breed
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
"	"	kIx"	"
<g/>
Drain	Drain	k1gMnSc1	Drain
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
"	"	kIx"	"
<g/>
Aneurysm	Aneurysm	k1gInSc1	Aneurysm
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
34	[number]	k4	34
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
School	School	k1gInSc4	School
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
"	"	kIx"	"
<g/>
Sliver	Sliver	k1gInSc1	Sliver
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
"	"	kIx"	"
<g/>
In	In	k1gFnSc1	In
Bloom	Bloom	k1gInSc1	Bloom
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
33	[number]	k4	33
"	"	kIx"	"
<g/>
Come	Com	k1gInSc2	Com
as	as	k1gNnSc2	as
You	You	k1gFnSc2	You
Are	ar	k1gInSc5	ar
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
34	[number]	k4	34
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Lithium	lithium	k1gNnSc1	lithium
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
"	"	kIx"	"
<g/>
About	About	k1gInSc1	About
a	a	k8xC	a
Girl	girl	k1gFnSc1	girl
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
"	"	kIx"	"
<g/>
Tourette	Tourett	k1gInSc5	Tourett
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
"	"	kIx"	"
-	-	kIx~	-
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
"	"	kIx"	"
<g/>
Polly	Polla	k1gFnSc2	Polla
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
48	[number]	k4	48
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Lounge	Loung	k1gMnSc4	Loung
Act	Act	k1gMnSc4	Act
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
"	"	kIx"	"
<g/>
Smells	Smellsa	k1gFnPc2	Smellsa
Like	Like	k1gInSc1	Like
Teen	Teen	k1gMnSc1	Teen
Spirit	Spirit	k1gInSc1	Spirit
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
44	[number]	k4	44
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Plain	Plain	k2eAgMnSc1d1	Plain
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
"	"	kIx"	"
<g/>
Negative	negativ	k1gInSc5	negativ
Creep	Creep	k1gInSc4	Creep
<g/>
"	"	kIx"	"
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
"	"	kIx"	"
<g/>
Been	Been	k1gInSc1	Been
a	a	k8xC	a
Son	son	k1gInSc1	son
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Apologies	Apologies	k1gMnSc1	Apologies
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
"	"	kIx"	"
<g/>
Blew	Blew	k1gFnSc1	Blew
<g/>
"	"	kIx"	"
-	-	kIx~	-
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
"	"	kIx"	"
<g/>
Dumb	Dumb	k1gInSc1	Dumb
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
<g/>
34	[number]	k4	34
"	"	kIx"	"
<g/>
Stay	Sta	k2eAgInPc1d1	Sta
Away	Away	k1gInPc1	Away
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
41	[number]	k4	41
"	"	kIx"	"
<g/>
Spank	Spank	k1gInSc1	Spank
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Buzz	Buzz	k1gInSc4	Buzz
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Money	Monea	k1gFnSc2	Monea
Will	Will	k1gMnSc1	Will
Roll	Roll	k1gMnSc1	Roll
Right	Right	k1gMnSc1	Right
In	In	k1gMnSc1	In
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Fang	Fang	k1gMnSc1	Fang
cover	cover	k1gMnSc1	cover
<g/>
)	)	kIx)	)
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
"	"	kIx"	"
<g/>
D-	D-	k1gFnSc1	D-
<g/>
7	[number]	k4	7
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Wipers	Wipers	k1gInSc1	Wipers
cover	cover	k1gInSc1	cover
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
43	[number]	k4	43
"	"	kIx"	"
<g/>
Territorial	Territorial	k1gInSc1	Territorial
Pissings	Pissings	k1gInSc1	Pissings
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Krist	Krista	k1gFnPc2	Krista
Novoselic	Novoselice	k1gFnPc2	Novoselice
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Dave	Dav	k1gInSc5	Dav
Grohl	Grohl	k1gMnPc4	Grohl
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Antony	anton	k1gInPc4	anton
Hodgkinson	Hodgkinson	k1gMnSc1	Hodgkinson
–	–	k?	–
tanečník	tanečník	k1gMnSc1	tanečník
</s>
