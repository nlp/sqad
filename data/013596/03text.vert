<s>
Karibské	karibský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
</s>
<s>
Karibské	karibský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
Vlajka	vlajka	k1gFnSc1
Karibského	karibský	k2eAgInSc2d1
společenstvíPoměr	společenstvíPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Mapa	mapa	k1gFnSc1
Karibiku	Karibik	k1gInSc2
s	s	k7c7
vyznačenými	vyznačený	k2eAgInPc7d1
státy	stát	k1gInPc7
CARICOM	CARICOM	kA
žlutě	žlutě	k6eAd1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Caribbean	Caribbean	k1gInSc1
Free	Fre	k1gFnSc2
Trade	Trad	k1gInSc5
Association	Association	k1gInSc1
Vznik	vznik	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1973	#num#	k4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Georgetown	Georgetown	k1gNnSc1
<g/>
,	,	kIx,
Guyana	Guyana	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
13,55	13,55	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
58	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
0,48	0,48	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Úřední	úřední	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
a	a	k8xC
španělština	španělština	k1gFnSc1
Členové	člen	k1gMnPc1
</s>
<s>
15	#num#	k4
členský	členský	k2eAgInSc1d1
stát	stát	k1gInSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.caricom.org	www.caricom.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Karibské	karibský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Caribbean	Caribbean	k2eAgFnSc1d1
Community	Communita	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezistátní	mezistátní	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
působící	působící	k2eAgFnPc4d1
v	v	k7c6
Karibiku	Karibik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdružuje	sdružovat	k5eAaImIp3nS
15	#num#	k4
členů	člen	k1gInPc2
<g/>
,	,	kIx,
5	#num#	k4
zámořských	zámořský	k2eAgNnPc2d1
území	území	k1gNnPc2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
má	mít	k5eAaImIp3nS
statut	statut	k1gInSc1
přidružených	přidružený	k2eAgInPc2d1
členů	člen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posláním	poslání	k1gNnSc7
a	a	k8xC
cílem	cíl	k1gInSc7
této	tento	k3xDgFnSc2
organizace	organizace	k1gFnSc2
je	být	k5eAaImIp3nS
posilovat	posilovat	k5eAaImF
vzájemné	vzájemný	k2eAgFnPc4d1
vazby	vazba	k1gFnPc4
mezi	mezi	k7c7
zúčastněnými	zúčastněný	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
různými	různý	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
podporovat	podporovat	k5eAaImF
ekonomický	ekonomický	k2eAgInSc4d1
i	i	k8xC
sociální	sociální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Karibské	karibský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Karibské	karibský	k2eAgNnSc4d1
společenství	společenství	k1gNnSc4
a	a	k8xC
společný	společný	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
)	)	kIx)
vzniklo	vzniknout	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1973	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přišla	přijít	k5eAaPmAgFnS
v	v	k7c4
platnost	platnost	k1gFnSc4
ustavující	ustavující	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
z	z	k7c2
Chaguaramas	Chaguaramasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakládající	zakládající	k2eAgInPc1d1
státy	stát	k1gInPc1
byly	být	k5eAaImAgInP
Trinidad	Trinidad	k1gInSc4
a	a	k8xC
Tobago	Tobago	k1gNnSc1
<g/>
,	,	kIx,
Barbados	Barbados	k1gInSc1
<g/>
,	,	kIx,
Jamajka	Jamajka	k1gFnSc1
a	a	k8xC
Guyana	Guyana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
byla	být	k5eAaImAgFnS
přijatá	přijatý	k2eAgFnSc1d1
pozměněná	pozměněný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karibské	karibský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
nejdříve	dříve	k6eAd3
navazovalo	navazovat	k5eAaImAgNnS
na	na	k7c4
Západoindickou	západoindický	k2eAgFnSc4d1
federaci	federace	k1gFnSc4
sdružující	sdružující	k2eAgInSc1d1
anglicky	anglicky	k6eAd1
mluvící	mluvící	k2eAgNnPc4d1
území	území	k1gNnPc4
v	v	k7c6
Karibiku	Karibik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
nizozemsky	nizozemsky	k6eAd1
mluvící	mluvící	k2eAgInSc1d1
Surinam	Surinam	k1gInSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
Haiti	Haiti	k1gNnPc2
(	(	kIx(
<g/>
kde	kde	k6eAd1
se	se	k3xPyFc4
mluví	mluvit	k5eAaImIp3nS
francouzsky	francouzsky	k6eAd1
a	a	k8xC
haitskou	haitský	k2eAgFnSc7d1
kreolštinou	kreolština	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
projekty	projekt	k1gInPc4
(	(	kIx(
<g/>
některé	některý	k3yIgNnSc1
již	již	k6eAd1
realizované	realizovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
teprve	teprve	k6eAd1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nP
např.	např.	kA
společný	společný	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
,	,	kIx,
pas	pas	k1gInSc4
<g/>
,	,	kIx,
víza	vízo	k1gNnPc4
<g/>
,	,	kIx,
univerzita	univerzita	k1gFnSc1
<g/>
;	;	kIx,
celní	celní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
,	,	kIx,
měnová	měnový	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zúčastněné	zúčastněný	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnSc1
</s>
<s>
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
</s>
<s>
Bahamy	Bahamy	k1gFnPc1
Bahamy	Bahamy	k1gFnPc1
</s>
<s>
Barbados	Barbados	k1gMnSc1
Barbados	Barbados	k1gMnSc1
</s>
<s>
Belize	Belize	k1gFnSc1
Belize	Belize	k1gFnSc2
</s>
<s>
Dominika	Dominik	k1gMnSc4
Dominika	Dominik	k1gMnSc4
</s>
<s>
Grenada	Grenada	k1gFnSc1
Grenada	Grenada	k1gFnSc1
</s>
<s>
Guyana	Guyana	k1gFnSc1
Guyana	Guyana	k1gFnSc1
</s>
<s>
Haiti	Haiti	k1gNnSc1
Haiti	Haiti	k1gNnSc2
</s>
<s>
Jamajka	Jamajka	k1gFnSc1
Jamajka	Jamajka	k1gFnSc1
</s>
<s>
Montserrat	Montserrat	k1gMnSc1
</s>
<s>
Surinam	Surinam	k6eAd1
Surinam	Surinam	k1gInSc1
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
Svatý	svatý	k1gMnSc1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnSc2
Svatý	svatý	k1gMnSc1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
</s>
<s>
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k6eAd1
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
</s>
<s>
Přidružená	přidružený	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Anguilla	Anguilla	k1gMnSc1
Anguilla	Anguilla	k1gMnSc1
</s>
<s>
Bermudy	Bermudy	k1gFnPc1
Bermudy	Bermudy	k1gFnPc1
</s>
<s>
Britské	britský	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
Britské	britský	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
</s>
<s>
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Turks	Turks	k1gInSc1
a	a	k8xC
Caicos	Caicos	k1gInSc1
Turks	Turksa	k1gFnPc2
a	a	k8xC
Caicos	Caicosa	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Latinskoamerická	latinskoamerický	k2eAgFnSc1d1
integrace	integrace	k1gFnSc1
</s>
<s>
Organizace	organizace	k1gFnSc1
východokaribských	východokaribský	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Sdružení	sdružení	k1gNnSc1
karibských	karibský	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Karibský	karibský	k2eAgInSc1d1
soudní	soudní	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Karibské	karibský	k2eAgFnSc2d1
společenství	společenství	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
"	"	kIx"
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
33586-1	33586-1	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2165	#num#	k4
0967	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79060698	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
123142959	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79060698	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
a	a	k8xC
Karibik	Karibik	k1gInSc1
</s>
