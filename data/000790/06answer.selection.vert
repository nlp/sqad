<s>
Uran	Uran	k1gInSc1	Uran
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
jen	jen	k9	jen
83	[number]	k4	83
%	%	kIx~	%
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
15	[number]	k4	15
%	%	kIx~	%
helia	helium	k1gNnPc4	helium
a	a	k8xC	a
stopová	stopový	k2eAgNnPc4d1	stopové
množství	množství	k1gNnPc4	množství
metanu	metan	k1gInSc2	metan
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
