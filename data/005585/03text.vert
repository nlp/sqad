<s>
Androctonus	Androctonus	k1gInSc1	Androctonus
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
štírů	štír	k1gMnPc2	štír
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Buthidae	Buthida	k1gFnSc2	Buthida
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
15	[number]	k4	15
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
pouštních	pouštní	k2eAgFnPc6d1	pouštní
oblastech	oblast	k1gFnPc6	oblast
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
až	až	k9	až
k	k	k7c3	k
Pákistánu	Pákistán	k1gInSc3	Pákistán
a	a	k8xC	a
Indii	Indie	k1gFnSc3	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
tyto	tento	k3xDgMnPc4	tento
štíry	štír	k1gMnPc4	štír
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
druhem	druh	k1gInSc7	druh
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
Androctonus	Androctonus	k1gMnSc1	Androctonus
amoreuxi	amoreuxe	k1gFnSc4	amoreuxe
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
až	až	k6eAd1	až
do	do	k7c2	do
10-14	[number]	k4	10-14
cm	cm	kA	cm
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nejmenším	malý	k2eAgInSc6d3	nejmenší
je	být	k5eAaImIp3nS	být
Androctonus	Androctonus	k1gInSc1	Androctonus
mauretanicus	mauretanicus	k1gInSc1	mauretanicus
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
asi	asi	k9	asi
8	[number]	k4	8
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
těchto	tento	k3xDgMnPc2	tento
štírů	štír	k1gMnPc2	štír
je	být	k5eAaImIp3nS	být
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nejnebezpečnějším	bezpečný	k2eNgInSc7d3	nejnebezpečnější
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
štír	štír	k1gMnSc1	štír
tlustorepý	tlustorepý	k2eAgMnSc1d1	tlustorepý
(	(	kIx(	(
<g/>
A.	A.	kA	A.
australis	australis	k1gFnSc2	australis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
A.	A.	kA	A.
amoreuxi	amoreuxe	k1gFnSc4	amoreuxe
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Androctonus	Androctonus	k1gMnSc1	Androctonus
bicolor	bicolor	k1gMnSc1	bicolor
<g/>
,	,	kIx,	,
Androctonus	Androctonus	k1gMnSc1	Androctonus
crassicauda	crassicauda	k1gMnSc1	crassicauda
a	a	k8xC	a
Androctonus	Androctonus	k1gMnSc1	Androctonus
mauritanicus	mauritanicus	k1gMnSc1	mauritanicus
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
osoby	osoba	k1gFnPc4	osoba
citlivější	citlivý	k2eAgFnPc4d2	citlivější
na	na	k7c4	na
živočišné	živočišný	k2eAgInPc4d1	živočišný
jedy	jed	k1gInPc4	jed
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
mají	mít	k5eAaImIp3nP	mít
až	až	k9	až
60	[number]	k4	60
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Nejjedovatější	jedovatý	k2eAgMnPc1d3	nejjedovatější
štíři	štír	k1gMnPc1	štír
rodu	rod	k1gInSc2	rod
nemají	mít	k5eNaImIp3nP	mít
slabá	slabý	k2eAgNnPc1d1	slabé
klepítka	klepítko	k1gNnPc1	klepítko
typická	typický	k2eAgNnPc1d1	typické
pro	pro	k7c4	pro
jedovaté	jedovatý	k2eAgInPc4d1	jedovatý
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Androctonus	Androctonus	k1gMnSc1	Androctonus
afghanus	afghanus	k1gMnSc1	afghanus
Lourenço	Lourenço	k1gMnSc1	Lourenço
&	&	k?	&
Qi	Qi	k1gMnSc1	Qi
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
*	*	kIx~	*
Androctonus	Androctonus	k1gInSc4	Androctonus
amoreuxi	amoreuxe	k1gFnSc4	amoreuxe
(	(	kIx(	(
<g/>
Audouin	Audouin	k1gInSc1	Audouin
<g/>
,1826	,1826	k4	,1826
<g/>
)	)	kIx)	)
Androctonus	Androctonus	k1gInSc1	Androctonus
australis	australis	k1gInSc1	australis
(	(	kIx(	(
<g/>
Linné	Linné	k1gNnSc1	Linné
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
Androctonus	Androctonus	k1gMnSc1	Androctonus
baluchicus	baluchicus	k1gMnSc1	baluchicus
(	(	kIx(	(
<g/>
Pocock	Pocock	k1gMnSc1	Pocock
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Androctonus	Androctonus	k1gMnSc1	Androctonus
bicolor	bicolor	k1gMnSc1	bicolor
Ehrenberg	Ehrenberg	k1gMnSc1	Ehrenberg
<g/>
,	,	kIx,	,
1828	[number]	k4	1828
Androctonus	Androctonus	k1gInSc1	Androctonus
crassicauda	crassicaudo	k1gNnSc2	crassicaudo
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Olivier	Olivier	k1gMnSc1	Olivier
<g/>
,	,	kIx,	,
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
Androctonus	Androctonus	k1gMnSc1	Androctonus
dekeyseri	dekeyser	k1gFnSc2	dekeyser
Lourenço	Lourenço	k1gMnSc1	Lourenço
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
*	*	kIx~	*
Androctonus	Androctonus	k1gMnSc1	Androctonus
finitimus	finitimus	k1gMnSc1	finitimus
(	(	kIx(	(
<g/>
Pocock	Pocock	k1gMnSc1	Pocock
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
Androctonus	Androctonus	k1gInSc4	Androctonus
gonneti	gonnet	k5eAaBmF	gonnet
Vachon	Vachon	k1gMnSc1	Vachon
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
*	*	kIx~	*
Androctonus	Androctonus	k1gInSc1	Androctonus
hoggarensis	hoggarensis	k1gFnSc2	hoggarensis
(	(	kIx(	(
<g/>
Pallary	Pallara	k1gFnSc2	Pallara
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Androctonus	Androctonus	k1gInSc1	Androctonus
liouvillei	liouville	k1gFnSc2	liouville
(	(	kIx(	(
<g/>
Pallary	Pallara	k1gFnSc2	Pallara
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Androctonus	Androctonus	k1gInSc4	Androctonus
maelfaiti	maelfait	k5eAaImF	maelfait
Lourenço	Lourenço	k6eAd1	Lourenço
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
*	*	kIx~	*
Androctonus	Androctonus	k1gMnSc1	Androctonus
mauritanicus	mauritanicus	k1gMnSc1	mauritanicus
(	(	kIx(	(
<g/>
Pocock	Pocock	k1gMnSc1	Pocock
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
Androctonus	Androctonus	k1gMnSc1	Androctonus
sergenti	sergent	k1gMnPc1	sergent
Vachon	Vachon	k1gMnSc1	Vachon
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
</s>
