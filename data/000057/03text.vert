<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
stupnice	stupnice	k1gFnSc1	stupnice
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
tónů	tón	k1gInPc2	tón
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
jedné	jeden	k4xCgFnSc2	jeden
oktávy	oktáva	k1gFnSc2	oktáva
<g/>
)	)	kIx)	)
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
podle	podle	k7c2	podle
určitých	určitý	k2eAgFnPc2d1	určitá
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
určují	určovat	k5eAaImIp3nP	určovat
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
stupni	stupeň	k1gInPc7	stupeň
(	(	kIx(	(
<g/>
intervaly	interval	k1gInPc7	interval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
stupnice	stupnice	k1gFnPc1	stupnice
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
zejména	zejména	k9	zejména
počtem	počet	k1gInSc7	počet
tónů	tón	k1gInPc2	tón
a	a	k8xC	a
vzdálenostmi	vzdálenost	k1gFnPc7	vzdálenost
(	(	kIx(	(
<g/>
intervaly	interval	k1gInPc7	interval
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
,	,	kIx,	,
např.	např.	kA	např.
umístěním	umístění	k1gNnSc7	umístění
půltónů	půltón	k1gInPc2	půltón
a	a	k8xC	a
celých	celý	k2eAgInPc2d1	celý
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnSc2d1	přesná
tónové	tónový	k2eAgFnSc2d1	tónová
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
ve	v	k7c6	v
stupnicích	stupnice	k1gFnPc6	stupnice
závisejí	záviset	k5eAaImIp3nP	záviset
také	také	k9	také
na	na	k7c6	na
použitém	použitý	k2eAgNnSc6d1	Použité
ladění	ladění	k1gNnSc6	ladění
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
tón	tón	k1gInSc1	tón
stupnice	stupnice	k1gFnSc2	stupnice
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tónika	tónika	k1gFnSc1	tónika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
název	název	k1gInSc1	název
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
pomocí	pomocí	k7c2	pomocí
písmen	písmeno	k1gNnPc2	písmeno
nebo	nebo	k8xC	nebo
pomocí	pomoc	k1gFnPc2	pomoc
solmizačních	solmizační	k2eAgFnPc2d1	solmizační
slabik	slabika	k1gFnPc2	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
tónů	tón	k1gInPc2	tón
ve	v	k7c6	v
stupnici	stupnice	k1gFnSc6	stupnice
lze	lze	k6eAd1	lze
nejznámější	známý	k2eAgFnSc1d3	nejznámější
stupnice	stupnice	k1gFnSc1	stupnice
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
pětitónové	pětitónový	k2eAgNnSc4d1	pětitónový
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pentatonické	pentatonický	k2eAgFnSc6d1	pentatonická
(	(	kIx(	(
<g/>
typické	typický	k2eAgFnSc6d1	typická
pro	pro	k7c4	pro
hudbu	hudba	k1gFnSc4	hudba
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šestitónové	šestitónový	k2eAgFnPc1d1	šestitónový
(	(	kIx(	(
<g/>
celotónové	celotónový	k2eAgFnPc1d1	celotónová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sedmitónové	sedmitónový	k2eAgInPc1d1	sedmitónový
(	(	kIx(	(
<g/>
diatonické	diatonický	k2eAgInPc1d1	diatonický
<g/>
,	,	kIx,	,
církevní	církevní	k2eAgInPc1d1	církevní
<g/>
,	,	kIx,	,
cikánské	cikánský	k2eAgInPc1d1	cikánský
<g/>
)	)	kIx)	)
dvanáctitónové	dvanáctitónový	k2eAgInPc1d1	dvanáctitónový
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
chromatická	chromatický	k2eAgFnSc1d1	chromatická
<g/>
,	,	kIx,	,
alterovaná	alterovaný	k2eAgFnSc1d1	alterovaná
<g/>
)	)	kIx)	)
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
evropská	evropský	k2eAgFnSc1d1	Evropská
hudba	hudba	k1gFnSc1	hudba
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
řeckých	řecký	k2eAgFnPc2d1	řecká
antických	antický	k2eAgFnPc2d1	antická
stupnic	stupnice	k1gFnPc2	stupnice
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
tedy	tedy	k9	tedy
hlavně	hlavně	k9	hlavně
se	s	k7c7	s
stupnicemi	stupnice	k1gFnPc7	stupnice
diatonickými	diatonický	k2eAgFnPc7d1	diatonická
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
durová	durový	k2eAgFnSc1d1	durová
(	(	kIx(	(
<g/>
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
varianty	varianta	k1gFnPc1	varianta
mollové	mollový	k2eAgFnPc1d1	mollová
(	(	kIx(	(
<g/>
měkké	měkký	k2eAgFnPc1d1	měkká
<g/>
)	)	kIx)	)
-	-	kIx~	-
aiolská	aiolský	k2eAgFnSc1d1	aiolská
<g/>
,	,	kIx,	,
harmonická	harmonický	k2eAgFnSc1d1	harmonická
<g/>
,	,	kIx,	,
melodická	melodický	k2eAgFnSc1d1	melodická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
stupnice	stupnice	k1gFnSc1	stupnice
umělé	umělý	k2eAgFnSc2d1	umělá
<g/>
,	,	kIx,	,
např.	např.	kA	např.
módy	móda	k1gFnPc4	móda
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
transponovatelností	transponovatelnost	k1gFnSc7	transponovatelnost
Oliviera	Olivier	k1gMnSc2	Olivier
Messiaena	Messiaen	k1gMnSc2	Messiaen
nebo	nebo	k8xC	nebo
jazzové	jazzový	k2eAgFnSc2d1	jazzová
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
i	i	k9	i
mnohé	mnohý	k2eAgFnPc1d1	mnohá
stupnice	stupnice	k1gFnPc1	stupnice
odvozené	odvozený	k2eAgFnPc1d1	odvozená
ze	z	k7c2	z
stupnic	stupnice	k1gFnPc2	stupnice
mimoevropského	mimoevropský	k2eAgInSc2d1	mimoevropský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Stupnice	stupnice	k1gFnSc1	stupnice
používané	používaný	k2eAgNnSc1d1	používané
v	v	k7c6	v
mimoevropské	mimoevropský	k2eAgFnSc6d1	mimoevropská
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
exotické	exotický	k2eAgNnSc4d1	exotické
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gInPc3	on
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
řazeny	řadit	k5eAaImNgFnP	řadit
stupnice	stupnice	k1gFnPc1	stupnice
pentatonické	pentatonický	k2eAgFnPc1d1	pentatonická
<g/>
,	,	kIx,	,
cikánské	cikánský	k2eAgFnPc1d1	cikánská
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
stupnice	stupnice	k1gFnSc1	stupnice
celotónová	celotónový	k2eAgFnSc1d1	celotónová
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
i	i	k9	i
stupnice	stupnice	k1gFnPc1	stupnice
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
podle	podle	k7c2	podle
zcela	zcela	k6eAd1	zcela
odlišných	odlišný	k2eAgNnPc2d1	odlišné
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
indonéské	indonéský	k2eAgFnSc2d1	Indonéská
stupnice	stupnice	k1gFnSc2	stupnice
sléndro	sléndro	k6eAd1	sléndro
a	a	k8xC	a
pélog	pélog	k1gInSc1	pélog
nebo	nebo	k8xC	nebo
stupnice	stupnice	k1gFnSc1	stupnice
indické	indický	k2eAgFnSc2d1	indická
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgFnSc2d1	pracující
s	s	k7c7	s
oktávou	oktáva	k1gFnSc7	oktáva
dělenou	dělený	k2eAgFnSc7d1	dělená
na	na	k7c4	na
22	[number]	k4	22
nestejných	stejný	k2eNgInPc2d1	nestejný
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diatonická	diatonický	k2eAgFnSc1d1	diatonická
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Stupně	stupeň	k1gInPc1	stupeň
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdáleny	vzdálit	k5eAaPmNgInP	vzdálit
o	o	k7c4	o
celé	celý	k2eAgInPc4d1	celý
tóny	tón	k1gInPc4	tón
i	i	k8xC	i
půltóny	půltón	k1gInPc4	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
stupnice	stupnice	k1gFnPc1	stupnice
-	-	kIx~	-
mody	modus	k1gInPc1	modus
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
středověké	středověký	k2eAgFnSc3d1	středověká
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
gregoriánském	gregoriánský	k2eAgInSc6d1	gregoriánský
chorálu	chorál	k1gInSc6	chorál
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
církevní	církevní	k2eAgFnPc1d1	církevní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sloužila	sloužit	k5eAaImAgFnS	sloužit
středověká	středověký	k2eAgFnSc1d1	středověká
hudba	hudba	k1gFnSc1	hudba
převážně	převážně	k6eAd1	převážně
církevním	církevní	k2eAgInPc3d1	církevní
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
jsou	být	k5eAaImIp3nP	být
řeckého	řecký	k2eAgMnSc4d1	řecký
původu	původa	k1gMnSc4	původa
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
řeckých	řecký	k2eAgInPc2d1	řecký
kmenů	kmen	k1gInPc2	kmen
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dórů	Dór	k1gMnPc2	Dór
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
původnímu	původní	k2eAgNnSc3d1	původní
antickému	antický	k2eAgNnSc3d1	antické
pojmenování	pojmenování	k1gNnSc3	pojmenování
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
mody	modus	k1gInPc1	modus
často	často	k6eAd1	často
liší	lišit	k5eAaImIp3nP	lišit
-	-	kIx~	-
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
zbytečně	zbytečně	k6eAd1	zbytečně
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
původní	původní	k2eAgInSc1d1	původní
antický	antický	k2eAgInSc1d1	antický
dórský	dórský	k2eAgInSc1d1	dórský
modus	modus	k1gInSc1	modus
dnes	dnes	k6eAd1	dnes
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
církevní	církevní	k2eAgMnSc1d1	církevní
frygický	frygický	k2eAgMnSc1d1	frygický
-	-	kIx~	-
podrobnosti	podrobnost	k1gFnPc1	podrobnost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
o	o	k7c6	o
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
modech	modus	k1gInPc6	modus
<g/>
.	.	kIx.	.
dórský	dórský	k2eAgInSc1d1	dórský
modus	modus	k1gInSc1	modus
-	-	kIx~	-
d	d	k?	d
e	e	k0	e
f	f	k?	f
g	g	kA	g
a	a	k8xC	a
h	h	k?	h
c	c	k0	c
d	d	k?	d
frygický	frygický	k2eAgInSc1d1	frygický
<g />
.	.	kIx.	.
</s>
<s>
modus	modus	k1gInSc1	modus
-	-	kIx~	-
e	e	k0	e
f	f	k?	f
g	g	kA	g
a	a	k8xC	a
h	h	k?	h
c	c	k0	c
d	d	k?	d
e	e	k0	e
lydický	lydický	k2eAgInSc1d1	lydický
modus	modus	k1gInSc1	modus
-	-	kIx~	-
f	f	k?	f
g	g	kA	g
a	a	k8xC	a
h	h	k?	h
c	c	k0	c
d	d	k?	d
e	e	k0	e
f	f	k?	f
mixolydický	mixolydický	k2eAgInSc1d1	mixolydický
modus	modus	k1gInSc1	modus
-	-	kIx~	-
g	g	kA	g
a	a	k8xC	a
h	h	k?	h
c	c	k0	c
d	d	k?	d
e	e	k0	e
f	f	k?	f
g	g	kA	g
Později	pozdě	k6eAd2	pozdě
přibyly	přibýt	k5eAaPmAgInP	přibýt
dnes	dnes	k6eAd1	dnes
nejrozšířenější	rozšířený	k2eAgInPc1d3	nejrozšířenější
<g/>
:	:	kIx,	:
aiolský	aiolský	k2eAgInSc1d1	aiolský
modus	modus	k1gInSc1	modus
-	-	kIx~	-
a	a	k8xC	a
h	h	k?	h
c	c	k0	c
d	d	k?	d
e	e	k0	e
f	f	k?	f
g	g	kA	g
a	a	k8xC	a
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
diatonickou	diatonický	k2eAgFnSc7d1	diatonická
mollovou	mollový	k2eAgFnSc7d1	mollová
stupnicí	stupnice	k1gFnSc7	stupnice
<g/>
)	)	kIx)	)
jónský	jónský	k2eAgInSc1d1	jónský
modus	modus	k1gInSc1	modus
-	-	kIx~	-
c	c	k0	c
d	d	k?	d
e	e	k0	e
f	f	k?	f
g	g	kA	g
a	a	k8xC	a
h	h	k?	h
c	c	k0	c
(	(	kIx(	(
<g/>
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
diatonickou	diatonický	k2eAgFnSc7d1	diatonická
durovou	durový	k2eAgFnSc7d1	durová
stupnicí	stupnice	k1gFnSc7	stupnice
<g/>
)	)	kIx)	)
Především	především	k9	především
z	z	k7c2	z
formálních	formální	k2eAgInPc2d1	formální
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c4	mezi
mody	modus	k1gInPc4	modus
včleněn	včlenit	k5eAaPmNgInS	včlenit
také	také	k9	také
modus	modus	k1gInSc1	modus
na	na	k7c6	na
sedmém	sedmý	k4xOgInSc6	sedmý
stupni	stupeň	k1gInSc6	stupeň
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
:	:	kIx,	:
lokrický	lokrický	k2eAgInSc1d1	lokrický
modus	modus	k1gInSc1	modus
-	-	kIx~	-
h	h	k?	h
c	c	k0	c
d	d	k?	d
e	e	k0	e
f	f	k?	f
g	g	kA	g
a	a	k8xC	a
h	h	k?	h
<g/>
,	,	kIx,	,
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
především	především	k6eAd1	především
alterovanou	alterovaný	k2eAgFnSc7d1	alterovaná
(	(	kIx(	(
<g/>
zmenšenou	zmenšený	k2eAgFnSc7d1	zmenšená
<g/>
)	)	kIx)	)
kvintou	kvinta	k1gFnSc7	kvinta
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
harmonie	harmonie	k1gFnSc2	harmonie
se	se	k3xPyFc4	se
od	od	k7c2	od
používání	používání	k1gNnSc2	používání
většiny	většina	k1gFnSc2	většina
církevních	církevní	k2eAgInPc2d1	církevní
modů	modus	k1gInPc2	modus
upustilo	upustit	k5eAaPmAgNnS	upustit
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
jónská	jónský	k2eAgFnSc1d1	jónská
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
durová	durový	k2eAgFnSc1d1	durová
-	-	kIx~	-
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
a	a	k8xC	a
aiolská	aiolský	k2eAgFnSc1d1	aiolská
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
měkká	měkký	k2eAgFnSc1d1	měkká
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
stupnice	stupnice	k1gFnPc4	stupnice
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
od	od	k7c2	od
libovolného	libovolný	k2eAgInSc2d1	libovolný
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
stupnice	stupnice	k1gFnSc1	stupnice
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
předznamenáním	předznamenání	k1gNnSc7	předznamenání
(	(	kIx(	(
<g/>
křížky	křížek	k1gInPc1	křížek
<g/>
,	,	kIx,	,
béčka	béčko	k1gNnPc1	béčko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Durové	durový	k2eAgFnPc1d1	durová
a	a	k8xC	a
mollové	mollový	k2eAgFnPc1d1	mollová
stupnice	stupnice	k1gFnPc1	stupnice
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
předznamenání	předznamenání	k1gNnSc2	předznamenání
uspořádat	uspořádat	k5eAaPmF	uspořádat
do	do	k7c2	do
kvintového	kvintový	k2eAgInSc2d1	kvintový
a	a	k8xC	a
kvartového	kvartový	k2eAgInSc2d1	kvartový
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
znějící	znějící	k2eAgFnPc1d1	znějící
stupnice	stupnice	k1gFnPc1	stupnice
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různá	různý	k2eAgNnPc4d1	různé
předznamenání	předznamenání	k1gNnPc4	předznamenání
-	-	kIx~	-
např.	např.	kA	např.
Cis	cis	k1gNnSc1	cis
dur	dur	k1gNnSc1	dur
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
♯	♯	k?	♯
<g/>
)	)	kIx)	)
a	a	k8xC	a
Des	des	k1gNnSc1	des
dur	dur	k1gNnSc2	dur
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
♭	♭	k?	♭
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvojici	dvojice	k1gFnSc4	dvojice
durové	durový	k2eAgFnSc2d1	durová
a	a	k8xC	a
mollové	mollový	k2eAgFnSc2d1	mollová
stupnice	stupnice	k1gFnSc2	stupnice
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
předznamenáním	předznamenání	k1gNnSc7	předznamenání
říkáme	říkat	k5eAaImIp1nP	říkat
souběžné	souběžný	k2eAgMnPc4d1	souběžný
(	(	kIx(	(
<g/>
paralelní	paralelní	k2eAgFnSc7d1	paralelní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
stupnici	stupnice	k1gFnSc3	stupnice
durové	durový	k2eAgNnSc4d1	durové
náleží	náležet	k5eAaImIp3nS	náležet
jedna	jeden	k4xCgFnSc1	jeden
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
stupnice	stupnice	k1gFnSc1	stupnice
aiolská	aiolský	k2eAgFnSc1d1	aiolská
(	(	kIx(	(
<g/>
přirozená	přirozený	k2eAgFnSc1d1	přirozená
<g/>
)	)	kIx)	)
mollová	mollový	k2eAgFnSc1d1	mollová
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
stupnice	stupnice	k1gFnPc1	stupnice
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
tónový	tónový	k2eAgInSc4d1	tónový
materiál	materiál	k1gInSc4	materiál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mollová	mollový	k2eAgFnSc1d1	mollová
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
stupni	stupeň	k1gInPc7	stupeň
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
,	,	kIx,	,
durová	durový	k2eAgFnSc1d1	durová
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
stupni	stupeň	k1gInPc7	stupeň
mollové	mollový	k2eAgFnSc2d1	mollová
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Durová	durový	k2eAgFnSc1d1	durová
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Intervaly	interval	k1gInPc1	interval
mezi	mezi	k7c7	mezi
sousedními	sousední	k2eAgInPc7d1	sousední
tóny	tón	k1gInPc7	tón
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
základního	základní	k2eAgInSc2d1	základní
tónu	tón	k1gInSc2	tón
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
půltónů	půltón	k1gInPc2	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Půltóny	půltón	k1gInPc1	půltón
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
mezi	mezi	k7c7	mezi
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
stupněm	stupeň	k1gInSc7	stupeň
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
stupnice	stupnice	k1gFnSc1	stupnice
postupuje	postupovat	k5eAaImIp3nS	postupovat
po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
tónech	tón	k1gInPc6	tón
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc4	název
durových	durový	k2eAgFnPc2d1	durová
stupnic	stupnice	k1gFnPc2	stupnice
označujeme	označovat	k5eAaImIp1nP	označovat
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
durová	durový	k2eAgFnSc1d1	durová
stupnice	stupnice	k1gFnSc1	stupnice
je	být	k5eAaImIp3nS	být
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
,	,	kIx,	,
h	h	k?	h
neboli	neboli	k8xC	neboli
C	C	kA	C
dur	dur	k1gNnSc4	dur
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
se	s	k7c7	s
stupnicí	stupnice	k1gFnSc7	stupnice
jónskou	jónský	k2eAgFnSc7d1	jónská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
stupnici	stupnice	k1gFnSc4	stupnice
lze	lze	k6eAd1	lze
zahrát	zahrát	k5eAaPmF	zahrát
na	na	k7c6	na
bílých	bílý	k2eAgFnPc6d1	bílá
klávesách	klávesa	k1gFnPc6	klávesa
na	na	k7c6	na
klavíru	klavír	k1gInSc6	klavír
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
notový	notový	k2eAgInSc1d1	notový
zápis	zápis	k1gInSc1	zápis
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgNnSc4	žádný
předznamenání	předznamenání	k1gNnSc4	předznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mollová	mollový	k2eAgFnSc1d1	mollová
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
mollová	mollový	k2eAgFnSc1d1	mollová
(	(	kIx(	(
<g/>
měkká	měkký	k2eAgFnSc1d1	měkká
<g/>
)	)	kIx)	)
stupnice	stupnice	k1gFnSc1	stupnice
a	a	k8xC	a
moll	moll	k1gNnSc1	moll
a	a	k8xC	a
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
aiolskou	aiolský	k2eAgFnSc7d1	aiolská
stupnicí	stupnice	k1gFnSc7	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stejné	stejný	k2eAgInPc4d1	stejný
tóny	tón	k1gInPc4	tón
jako	jako	k8xC	jako
základní	základní	k2eAgFnSc1d1	základní
durová	durový	k2eAgFnSc1d1	durová
stupnice	stupnice	k1gFnSc1	stupnice
C	C	kA	C
dur	dur	k1gNnSc7	dur
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gNnSc1	její
uspořádání	uspořádání	k1gNnSc1	uspořádání
je	být	k5eAaImIp3nS	být
jiné	jiný	k2eAgNnSc1d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Intervaly	interval	k1gInPc1	interval
mezi	mezi	k7c7	mezi
sousedními	sousední	k2eAgInPc7d1	sousední
tóny	tón	k1gInPc7	tón
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
půltónů	půltón	k1gInPc2	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Aiolská	aiolský	k2eAgFnSc1d1	aiolská
mollová	mollový	k2eAgFnSc1d1	mollová
stupnice	stupnice	k1gFnSc1	stupnice
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
půltóny	půltón	k1gInPc4	půltón
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
stupněm	stupeň	k1gInSc7	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc4	název
mollových	mollový	k2eAgFnPc2d1	mollová
stupnic	stupnice	k1gFnPc2	stupnice
označujeme	označovat	k5eAaImIp1nP	označovat
obvykle	obvykle	k6eAd1	obvykle
malými	malý	k2eAgInPc7d1	malý
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Harmonická	harmonický	k2eAgNnPc4d1	harmonické
moll	moll	k1gNnSc4	moll
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
základní	základní	k2eAgFnSc2d1	základní
(	(	kIx(	(
<g/>
aiolské	aiolský	k2eAgFnSc2d1	aiolská
<g/>
)	)	kIx)	)
mollové	mollový	k2eAgFnSc2d1	mollová
stupnice	stupnice	k1gFnSc2	stupnice
zvýšením	zvýšení	k1gNnSc7	zvýšení
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
předposledního	předposlední	k2eAgInSc2d1	předposlední
<g/>
)	)	kIx)	)
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Intervaly	interval	k1gInPc1	interval
mezi	mezi	k7c7	mezi
sousedními	sousední	k2eAgInPc7d1	sousední
tóny	tón	k1gInPc7	tón
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
půltónů	půltón	k1gInPc2	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
půltóny	půltón	k1gInPc1	půltón
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
stupněm	stupeň	k1gInSc7	stupeň
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
stupněm	stupeň	k1gInSc7	stupeň
je	být	k5eAaImIp3nS	být
interval	interval	k1gInSc1	interval
zvětšená	zvětšený	k2eAgFnSc1d1	zvětšená
sekunda	sekunda	k1gFnSc1	sekunda
(	(	kIx(	(
<g/>
tři	tři	k4xCgInPc4	tři
půltóny	půltón	k1gInPc4	půltón
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezi	mezi	k7c7	mezi
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
stupněm	stupeň	k1gInSc7	stupeň
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
jen	jen	k9	jen
půltón	půltón	k1gInSc1	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
pro	pro	k7c4	pro
a	a	k8xC	a
moll	moll	k1gNnPc2	moll
<g/>
:	:	kIx,	:
aiolská	aiolský	k2eAgFnSc1d1	aiolská
-	-	kIx~	-
a	a	k8xC	a
h	h	k?	h
c	c	k0	c
d	d	k?	d
e	e	k0	e
f	f	k?	f
g	g	kA	g
a	a	k8xC	a
harmonická	harmonický	k2eAgFnSc1d1	harmonická
-	-	kIx~	-
a	a	k8xC	a
h	h	k?	h
c	c	k0	c
d	d	k?	d
e	e	k0	e
f	f	k?	f
gis	gis	k1gNnPc4	gis
a	a	k8xC	a
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Melodická	melodický	k2eAgNnPc4d1	melodické
moll	moll	k1gNnSc4	moll
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
základní	základní	k2eAgFnSc2d1	základní
(	(	kIx(	(
<g/>
aiolské	aiolský	k2eAgFnSc2d1	aiolská
<g/>
)	)	kIx)	)
mollové	mollový	k2eAgFnSc2d1	mollová
stupnice	stupnice	k1gFnSc2	stupnice
zvýšením	zvýšení	k1gNnSc7	zvýšení
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Intervaly	interval	k1gInPc1	interval
mezi	mezi	k7c7	mezi
sousedními	sousední	k2eAgInPc7d1	sousední
tóny	tón	k1gInPc7	tón
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
půltónů	půltón	k1gInPc2	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Půltóny	půltón	k1gInPc1	půltón
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
mezi	mezi	k7c7	mezi
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
stupněm	stupeň	k1gInSc7	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
tónu	tón	k1gInSc2	tón
je	být	k5eAaImIp3nS	být
prováděno	provádět	k5eAaImNgNnS	provádět
jen	jen	k9	jen
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
vzestupném	vzestupný	k2eAgInSc6d1	vzestupný
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
sestupném	sestupný	k2eAgInSc6d1	sestupný
je	být	k5eAaImIp3nS	být
melodická	melodický	k2eAgFnSc1d1	melodická
stupnice	stupnice	k1gFnSc1	stupnice
shodná	shodný	k2eAgFnSc1d1	shodná
se	s	k7c7	s
základní	základní	k2eAgFnSc7d1	základní
aiolskou	aiolský	k2eAgFnSc7d1	aiolská
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
pro	pro	k7c4	pro
a	a	k8xC	a
moll	moll	k1gNnPc2	moll
<g/>
:	:	kIx,	:
aiolská	aiolský	k2eAgFnSc1d1	aiolská
-	-	kIx~	-
a	a	k8xC	a
h	h	k?	h
c	c	k0	c
d	d	k?	d
e	e	k0	e
f	f	k?	f
g	g	kA	g
a	a	k8xC	a
–	–	k?	–
a	a	k8xC	a
g	g	kA	g
f	f	k?	f
e	e	k0	e
d	d	k?	d
c	c	k0	c
h	h	k?	h
a	a	k8xC	a
melodická	melodický	k2eAgFnSc1d1	melodická
-	-	kIx~	-
a	a	k8xC	a
h	h	k?	h
c	c	k0	c
d	d	k?	d
e	e	k0	e
fis	fis	k1gNnPc1	fis
gis	gis	k1gNnSc1	gis
a	a	k8xC	a
–	–	k?	–
a	a	k8xC	a
g	g	kA	g
f	f	k?	f
e	e	k0	e
d	d	k?	d
c	c	k0	c
h	h	k?	h
a	a	k8xC	a
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
chromatická	chromatický	k2eAgFnSc1d1	chromatická
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
chromatické	chromatický	k2eAgFnSc6d1	chromatická
stupnici	stupnice	k1gFnSc6	stupnice
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
stupně	stupeň	k1gInPc4	stupeň
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdáleny	vzdálen	k2eAgInPc4d1	vzdálen
o	o	k7c4	o
půltóny	půltón	k1gInPc4	půltón
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
stupnice	stupnice	k1gFnSc1	stupnice
tedy	tedy	k9	tedy
má	mít	k5eAaImIp3nS	mít
dvanáct	dvanáct	k4xCc4	dvanáct
stupňů	stupeň	k1gInPc2	stupeň
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
notový	notový	k2eAgInSc1d1	notový
zápis	zápis	k1gInSc1	zápis
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
stupňové	stupňový	k2eAgNnSc4d1	stupňové
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
stupnic	stupnice	k1gFnPc2	stupnice
předešlých	předešlý	k2eAgFnPc2d1	předešlá
<g/>
:	:	kIx,	:
Sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
intervaly	interval	k1gInPc1	interval
mezi	mezi	k7c7	mezi
sousedními	sousední	k2eAgInPc7d1	sousední
tóny	tón	k1gInPc7	tón
jsou	být	k5eAaImIp3nP	být
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
půltónů	půltón	k1gInPc2	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Stupnice	stupnice	k1gFnSc1	stupnice
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
stupňů	stupeň	k1gInPc2	stupeň
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
půltónové	půltónový	k2eAgFnPc4d1	půltónová
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
mezi	mezi	k7c7	mezi
stupni	stupeň	k1gInPc7	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nazývá	nazývat	k5eAaImIp3nS	nazývat
anhemitonická	anhemitonický	k2eAgFnSc1d1	anhemitonický
bezpůltónová	bezpůltónový	k2eAgFnSc1d1	bezpůltónový
<g/>
)	)	kIx)	)
pentatonika	pentatonika	k1gFnSc1	pentatonika
<g/>
.	.	kIx.	.
</s>
<s>
Intervaly	interval	k1gInPc1	interval
mezi	mezi	k7c7	mezi
sousedními	sousední	k2eAgInPc7d1	sousední
tóny	tón	k1gInPc7	tón
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
klavíru	klavír	k1gInSc6	klavír
ji	on	k3xPp3gFnSc4	on
dávají	dávat	k5eAaImIp3nP	dávat
např.	např.	kA	např.
černé	černý	k2eAgInPc1d1	černý
klávesy	kláves	k1gInPc1	kláves
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
♯	♯	k?	♯
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
♯	♯	k?	♯
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
♯	♯	k?	♯
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
♯	♯	k?	♯
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
♯	♯	k?	♯
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pentatonická	pentatonický	k2eAgFnSc1d1	pentatonická
stupnice	stupnice	k1gFnSc1	stupnice
bývá	bývat	k5eAaImIp3nS	bývat
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejstarší	starý	k2eAgFnSc4d3	nejstarší
stupnici	stupnice	k1gFnSc4	stupnice
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
hudbě	hudba	k1gFnSc6	hudba
mnoha	mnoho	k4c2	mnoho
národů	národ	k1gInPc2	národ
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
stupnice	stupnice	k1gFnSc2	stupnice
vychází	vycházet	k5eAaImIp3nS	vycházet
dosud	dosud	k6eAd1	dosud
mnohá	mnohý	k2eAgFnSc1d1	mnohá
čínská	čínský	k2eAgFnSc1d1	čínská
nebo	nebo	k8xC	nebo
japonská	japonský	k2eAgFnSc1d1	japonská
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
najít	najít	k5eAaPmF	najít
i	i	k9	i
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
lidových	lidový	k2eAgFnPc6d1	lidová
písních	píseň	k1gFnPc6	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Celotónová	celotónový	k2eAgFnSc1d1	celotónová
stupnice	stupnice	k1gFnSc1	stupnice
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
celého	celý	k2eAgInSc2d1	celý
tónu	tón	k1gInSc2	tón
(	(	kIx(	(
<g/>
dvou	dva	k4xCgInPc2	dva
půltónů	půltón	k1gInPc2	půltón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Intervaly	interval	k1gInPc1	interval
mezi	mezi	k7c7	mezi
sousedními	sousední	k2eAgInPc7d1	sousední
tóny	tón	k1gInPc7	tón
od	od	k7c2	od
základního	základní	k2eAgInSc2d1	základní
tónu	tón	k1gInSc2	tón
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Interval	interval	k1gInSc1	interval
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
ladění	ladění	k1gNnPc2	ladění
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stupnice	stupnice	k1gFnSc2	stupnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://www.sbor.webzdarma.cz/slovnik%20hudby/S_slovnik.htm	[url]	k1gInPc3	http://www.sbor.webzdarma.cz/slovnik%20hudby/S_slovnik.htm
</s>
