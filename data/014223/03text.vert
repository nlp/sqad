<s>
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
</s>
<s>
Bc.	Bc.	k?
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
během	během	k7c2
sezóny	sezóna	k1gFnSc2
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Makula	Makula	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
(	(	kIx(
<g/>
24	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
168	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
56	#num#	k4
kg	kg	kA
Sportovní	sportovní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Sport	sport	k1gInSc1
</s>
<s>
biatlon	biatlon	k1gInSc1
Klub	klub	k1gInSc1
</s>
<s>
SKP	SKP	kA
Kornspitz	Kornspitz	k1gInSc1
Jablonec	Jablonec	k1gInSc1
Lyže	lyže	k1gFnSc2
</s>
<s>
Salomon	Salomon	k1gMnSc1
Světový	světový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
Debut	debut	k1gInSc1
</s>
<s>
2016	#num#	k4
Nejlepší	dobrý	k2eAgInSc4d3
umístění	umístění	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
výher	výhra	k1gFnPc2
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
individuální	individuální	k2eAgInPc1d1
<g/>
)	)	kIx)
Stupně	stupeň	k1gInPc1
vítězů	vítěz	k1gMnPc2
</s>
<s>
10	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
individuálních	individuální	k2eAgFnPc2d1
<g/>
)	)	kIx)
Medailový	medailový	k2eAgInSc4d1
zisk	zisk	k1gInSc4
Olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
</s>
<s>
0	#num#	k4
–	–	k?
0	#num#	k4
–	–	k?
0	#num#	k4
Mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
</s>
<s>
1	#num#	k4
–	–	k?
0	#num#	k4
–	–	k?
1	#num#	k4
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktualizovány	aktualizován	k2eAgInPc4d1
dne	den	k1gInSc2
20210216	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
2021	#num#	k4
Pokljuka	Pokljuk	k1gMnSc4
</s>
<s>
vytrvalostní	vytrvalostní	k2eAgInSc1d1
závod	závod	k1gInSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
2020	#num#	k4
Anterselva	Anterselva	k1gFnSc1
</s>
<s>
smíšená	smíšený	k2eAgFnSc1d1
štafeta	štafeta	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
2018	#num#	k4
Otepää	Otepää	k1gFnSc1
</s>
<s>
stíhací	stíhací	k2eAgInSc1d1
závod	závod	k1gInSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
2018	#num#	k4
Otepää	Otepää	k1gFnSc1
</s>
<s>
sprint	sprint	k1gInSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
2016	#num#	k4
Cheile	Cheila	k1gFnSc3
Grădiştei	Grădişte	k1gFnSc2
</s>
<s>
štafeta	štafeta	k1gFnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
2016	#num#	k4
Cheile	Cheila	k1gFnSc3
Grădiştei	Grădişte	k1gFnSc2
</s>
<s>
vytrvalostní	vytrvalostní	k2eAgInSc1d1
závod	závod	k1gInSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
2017	#num#	k4
Nové	Nové	k2eAgInSc4d1
Město	město	k1gNnSc1
</s>
<s>
sprint	sprint	k1gInSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
2017	#num#	k4
Nové	Nové	k2eAgInSc4d1
Město	město	k1gNnSc1
</s>
<s>
stíhací	stíhací	k2eAgInSc1d1
závod	závod	k1gInSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
2017	#num#	k4
Nové	Nové	k2eAgInSc4d1
Město	město	k1gNnSc1
</s>
<s>
12,5	12,5	k4
km	km	kA
</s>
<s>
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
biatlonistka	biatlonistka	k1gFnSc1
<g/>
,	,	kIx,
mistryně	mistryně	k1gFnSc1
světa	svět	k1gInSc2
z	z	k7c2
vytrvalostního	vytrvalostní	k2eAgInSc2d1
závodu	závod	k1gInSc2
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
2021	#num#	k4
v	v	k7c4
Pokljuce	Pokljuce	k1gFnPc4
a	a	k8xC
bronzová	bronzový	k2eAgFnSc1d1
medailistka	medailistka	k1gFnSc1
z	z	k7c2
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
2020	#num#	k4
ve	v	k7c6
smíšené	smíšený	k2eAgFnSc6d1
štafetě	štafeta	k1gFnSc6
v	v	k7c6
Anterselvě	Anterselva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
také	také	k9
juniorskou	juniorský	k2eAgFnSc7d1
světovou	světový	k2eAgFnSc7d1
šampionkou	šampionka	k1gFnSc7
ve	v	k7c6
stíhacím	stíhací	k2eAgInSc6d1
závodě	závod	k1gInSc6
a	a	k8xC
stříbrnou	stříbrný	k2eAgFnSc7d1
medailistkou	medailistka	k1gFnSc7
ve	v	k7c6
sprintu	sprint	k1gInSc6
z	z	k7c2
juniorského	juniorský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
2018	#num#	k4
v	v	k7c6
estonském	estonský	k2eAgMnSc6d1
Otepää	Otepää	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dvojnásobnou	dvojnásobný	k2eAgFnSc7d1
medailistkou	medailistka	k1gFnSc7
z	z	k7c2
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
biatlonu	biatlon	k1gInSc6
2016	#num#	k4
v	v	k7c6
rumunském	rumunský	k2eAgInSc6d1
Cheile	Cheila	k1gFnSc3
Grădiştei	Grădiştei	k1gNnSc1
v	v	k7c6
závodech	závod	k1gInPc6
dorostenců	dorostenec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
na	na	k7c6
Moravě	Morava	k1gFnSc6
zvítězila	zvítězit	k5eAaPmAgFnS
ve	v	k7c6
sprintu	sprint	k1gInSc6
na	na	k7c4
7,5	7,5	k4
km	km	kA
a	a	k8xC
ve	v	k7c6
stíhacím	stíhací	k2eAgInSc6d1
závodu	závod	k1gInSc6
na	na	k7c4
10	#num#	k4
km	km	kA
a	a	k8xC
skončila	skončit	k5eAaPmAgFnS
3	#num#	k4
<g/>
.	.	kIx.
ve	v	k7c6
vytrvalostním	vytrvalostní	k2eAgInSc6d1
závodu	závod	k1gInSc6
na	na	k7c4
12,5	12,5	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2019	#num#	k4
a	a	k8xC
2020	#num#	k4
vyhrála	vyhrát	k5eAaPmAgFnS
českou	český	k2eAgFnSc4d1
anketu	anketa	k1gFnSc4
Biatlonista	biatlonista	k1gMnSc1
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
Jablonci	Jablonec	k1gInSc6
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
a	a	k8xC
žije	žít	k5eAaImIp3nS
v	v	k7c6
Janově	Janov	k1gInSc6
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
začala	začít	k5eAaPmAgFnS
s	s	k7c7
biatlonem	biatlon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
matka	matka	k1gFnSc1
hrávala	hrávat	k5eAaImAgFnS
volejbal	volejbal	k1gInSc4
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
basketbal	basketbal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mládí	mládí	k1gNnSc6
vyhrávala	vyhrávat	k5eAaImAgFnS
závody	závod	k1gInPc4
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c6
lyžích	lyže	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS
obor	obor	k1gInSc4
Zoorehabilitace	Zoorehabilitace	k1gFnSc2
a	a	k8xC
asistenční	asistenční	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
se	s	k7c7
zvířaty	zvíře	k1gNnPc7
na	na	k7c6
Fakultě	fakulta	k1gFnSc6
agrobiologie	agrobiologie	k1gFnSc2
<g/>
,	,	kIx,
potravinových	potravinový	k2eAgInPc2d1
a	a	k8xC
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
České	český	k2eAgFnSc2d1
zemědělské	zemědělský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
obhájila	obhájit	k5eAaPmAgFnS
bakalářskou	bakalářský	k2eAgFnSc4d1
práci	práce	k1gFnSc4
na	na	k7c4
téma	téma	k1gNnSc4
Kineziotejping	Kineziotejping	k1gInSc1
jako	jako	k8xC,k8xS
metoda	metoda	k1gFnSc1
ve	v	k7c6
fyzioterapii	fyzioterapie	k1gFnSc6
koní	kůň	k1gMnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
získala	získat	k5eAaPmAgFnS
titul	titul	k1gInSc4
bakalář	bakalář	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
České	český	k2eAgFnSc6d1
zemědělské	zemědělský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
pokračuje	pokračovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
navazujícím	navazující	k2eAgNnSc6d1
magisterském	magisterský	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
dvou	dva	k4xCgInPc2
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
konkrétně	konkrétně	k6eAd1
Reprodukční	reprodukční	k2eAgFnSc1d1
biotechnologie	biotechnologie	k1gFnSc1
a	a	k8xC
šlechtění	šlechtění	k1gNnSc1
a	a	k8xC
Výživa	výživa	k1gFnSc1
zvířat	zvíře	k1gNnPc2
a	a	k8xC
dietetika	dietetika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
byla	být	k5eAaImAgFnS
také	také	k9
přijata	přijmout	k5eAaPmNgFnS
na	na	k7c4
šestiletý	šestiletý	k2eAgInSc4d1
obor	obor	k1gInSc4
na	na	k7c6
Fakultě	fakulta	k1gFnSc6
veterinárního	veterinární	k2eAgNnSc2d1
lékařství	lékařství	k1gNnSc2
Veterinární	veterinární	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
ale	ale	k9
studium	studium	k1gNnSc4
zatím	zatím	k6eAd1
pozastavila	pozastavit	k5eAaPmAgFnS
z	z	k7c2
důvodu	důvod	k1gInSc2
časové	časový	k2eAgFnSc2d1
náročnosti	náročnost	k1gFnSc2
aktivit	aktivita	k1gFnPc2
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
biatlonem	biatlon	k1gInSc7
a	a	k8xC
biatlonem	biatlon	k1gInSc7
samotným	samotný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
studiem	studio	k1gNnSc7
musí	muset	k5eAaImIp3nP
začít	začít	k5eAaPmF
do	do	k7c2
tří	tři	k4xCgNnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vychází	vycházet	k5eAaImIp3nS
na	na	k7c4
rok	rok	k1gInSc4
2022	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
plánuje	plánovat	k5eAaImIp3nS
po	po	k7c6
zimní	zimní	k2eAgFnSc6d1
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Pekingu	Peking	k1gInSc6
skončit	skončit	k5eAaPmF
s	s	k7c7
biatlonem	biatlon	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
členkou	členka	k1gFnSc7
oddílu	oddíl	k1gInSc2
SKP	SKP	kA
Kornspitz	Kornspitz	k1gInSc1
Jablonec	Jablonec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
</s>
<s>
Mezi	mezi	k7c7
dospělými	dospělí	k1gMnPc7
zvítězila	zvítězit	k5eAaPmAgFnS
ve	v	k7c6
sprintu	sprint	k1gInSc6
na	na	k7c4
7,5	7,5	k4
km	km	kA
v	v	k7c6
závodě	závod	k1gInSc6
IBU	IBU	kA
Cupu	cupat	k5eAaImIp1nS
v	v	k7c4
Beitostolenu	Beitostolen	k2eAgFnSc4d1
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byla	být	k5eAaImAgFnS
nominována	nominovat	k5eAaBmNgFnS
do	do	k7c2
sprintu	sprint	k1gInSc2
v	v	k7c6
závodu	závod	k1gInSc6
světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
na	na	k7c6
Moravě	Morava	k1gFnSc6
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Trenér	trenér	k1gMnSc1
reprezentace	reprezentace	k1gFnSc2
Ondřej	Ondřej	k1gMnSc1
Rybář	Rybář	k1gMnSc1
zdůraznil	zdůraznit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
„	„	k?
<g/>
příležitost	příležitost	k1gFnSc4
si	se	k3xPyFc3
vyzkoušet	vyzkoušet	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
velký	velký	k2eAgInSc1d1
biatlon	biatlon	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
na	na	k7c4
69	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Davidová	Davidová	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
představila	představit	k5eAaPmAgFnS
i	i	k9
v	v	k7c6
německém	německý	k2eAgInSc6d1
Ruhpoldingu	Ruhpolding	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
běžela	běžet	k5eAaImAgFnS
druhý	druhý	k4xOgInSc4
úsek	úsek	k1gInSc4
ženské	ženský	k2eAgFnSc2d1
štafety	štafeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sprintu	sprint	k1gInSc6
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
na	na	k7c4
28	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamenalo	znamenat	k5eAaImAgNnS
její	její	k3xOp3gInPc4
první	první	k4xOgInPc4
body	bod	k1gInPc4
ve	v	k7c6
světovém	světový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následném	následný	k2eAgInSc6d1
stíhacím	stíhací	k2eAgInSc6d1
závodě	závod	k1gInSc6
obsadila	obsadit	k5eAaPmAgFnS
48	#num#	k4
<g/>
.	.	kIx.
pozici	pozice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
</s>
<s>
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
během	během	k7c2
sezóny	sezóna	k1gFnSc2
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
Světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
v	v	k7c6
rakouském	rakouský	k2eAgInSc6d1
Hochfilzenu	Hochfilzen	k2eAgFnSc4d1
byla	být	k5eAaImAgFnS
na	na	k7c6
třetí	třetí	k4xOgFnSc6
pozici	pozice	k1gFnSc6
ženské	ženský	k2eAgFnSc2d1
štafety	štafeta	k1gFnSc2
4	#num#	k4
<g/>
×	×	k?
<g/>
6	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štafetu	štafeta	k1gFnSc4
převzala	převzít	k5eAaPmAgFnS
od	od	k7c2
Jessicy	Jessica	k1gFnSc2
Jislové	Jislová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
štafeta	štafeta	k1gFnSc1
ve	v	k7c6
složení	složení	k1gNnSc6
Eva	Eva	k1gFnSc1
Puskarčíková	Puskarčíková	k1gFnSc1
<g/>
,	,	kIx,
Jessica	Jessic	k2eAgFnSc1d1
Jislová	Jislová	k1gFnSc1
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
a	a	k8xC
Veronika	Veronika	k1gFnSc1
Vítková	Vítková	k1gFnSc1
obsadila	obsadit	k5eAaPmAgFnS
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
třetí	třetí	k4xOgFnSc6
části	část	k1gFnSc6
Světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
Annecy	Anneca	k1gFnSc2
jela	jet	k5eAaImAgFnS
v	v	k7c6
sobotním	sobotní	k2eAgInSc6d1
stíhacím	stíhací	k2eAgInSc6d1
závodu	závod	k1gInSc6
pod	pod	k7c7
startovacím	startovací	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
první	první	k4xOgFnSc6
střelbě	střelba	k1gFnSc6
v	v	k7c6
leže	leže	k6eAd1
musela	muset	k5eAaImAgFnS
na	na	k7c4
tři	tři	k4xCgNnPc4
trestná	trestný	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
byla	být	k5eAaImAgFnS
také	také	k9
se	s	k7c7
třemi	tři	k4xCgFnPc7
chybami	chyba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následná	následný	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
ve	v	k7c4
stoje	stoj	k1gInPc4
se	se	k3xPyFc4
neobešla	obešnout	k5eNaImAgFnS,k5eNaBmAgFnS,k5eNaPmAgFnS
bez	bez	k7c2
chyb	chyba	k1gFnPc2
a	a	k8xC
musela	muset	k5eAaImAgFnS
na	na	k7c4
dvě	dva	k4xCgNnPc4
trestná	trestný	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
poslední	poslední	k2eAgFnSc2d1
střelecké	střelecký	k2eAgFnSc2d1
položky	položka	k1gFnSc2
navštívila	navštívit	k5eAaPmAgFnS
dvakrát	dvakrát	k6eAd1
trestné	trestný	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stíhacím	stíhací	k2eAgInSc6d1
závodě	závod	k1gInSc6
dojela	dojet	k5eAaPmAgFnS
na	na	k7c4
58	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byla	být	k5eAaImAgFnS
pak	pak	k6eAd1
nominována	nominovat	k5eAaBmNgFnS
na	na	k7c4
Zimní	zimní	k2eAgFnPc4d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
2018	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
ve	v	k7c6
sprintu	sprint	k1gInSc6
dojela	dojet	k5eAaPmAgFnS
díky	díky	k7c3
jen	jen	k6eAd1
jedné	jeden	k4xCgFnSc6
chybě	chyba	k1gFnSc6
na	na	k7c4
střelnici	střelnice	k1gFnSc4
a	a	k8xC
solidnímu	solidní	k2eAgInSc3d1
běhu	běh	k1gInSc3
na	na	k7c4
15	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
na	na	k7c6
stupních	stupeň	k1gInPc6
vítězů	vítěz	k1gMnPc2
v	v	k7c6
italské	italský	k2eAgFnSc6d1
Anterselvě	Anterselva	k1gFnSc6
<g/>
,	,	kIx,
2019	#num#	k4
</s>
<s>
Sezona	sezona	k1gFnSc1
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
byla	být	k5eAaImAgFnS
pro	pro	k7c4
Markétu	Markéta	k1gFnSc4
Davidovou	Davidová	k1gFnSc4
zlomová	zlomový	k2eAgFnSc1d1
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
ve	v	k7c4
slovinské	slovinský	k2eAgFnPc4d1
Pokljuce	Pokljuce	k1gFnPc4
udělala	udělat	k5eAaPmAgFnS
jednu	jeden	k4xCgFnSc4
nepřesnost	nepřesnost	k1gFnSc4
na	na	k7c4
střelnici	střelnice	k1gFnSc4
ve	v	k7c6
vytrvalostním	vytrvalostní	k2eAgInSc6d1
závodě	závod	k1gInSc6
na	na	k7c4
15	#num#	k4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
díky	díky	k7c3
rychlému	rychlý	k2eAgInSc3d1
běhu	běh	k1gInSc3
skončila	skončit	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nestačila	stačit	k5eNaBmAgFnS
pouze	pouze	k6eAd1
na	na	k7c4
Ukrajinku	Ukrajinka	k1gFnSc4
Juliju	Juliju	k1gFnSc4
Džymovou	Džymová	k1gFnSc4
a	a	k8xC
Polku	Polka	k1gFnSc4
Moniku	Monika	k1gFnSc4
Hojniszovou	Hojniszová	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
měsíc	měsíc	k1gInSc4
později	pozdě	k6eAd2
v	v	k7c6
německém	německý	k2eAgInSc6d1
Oberhofu	Oberhof	k1gInSc6
získala	získat	k5eAaPmAgFnS
další	další	k2eAgFnSc4d1
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
ve	v	k7c6
světovém	světový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štafetu	štafeta	k1gFnSc4
rozjížděla	rozjíždět	k5eAaImAgFnS
Lucie	Lucie	k1gFnSc1
Charvátová	Charvátová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
kvůli	kvůli	k7c3
chybám	chyba	k1gFnPc3
na	na	k7c6
střelnici	střelnice	k1gFnSc6
dokončila	dokončit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
úsek	úsek	k1gInSc4
na	na	k7c4
14	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
ní	on	k3xPp3gFnSc7
jedoucí	jedoucí	k2eAgFnSc1d1
Veronika	Veronika	k1gFnSc1
Vítková	Vítková	k1gFnSc1
předávala	předávat	k5eAaImAgFnS
na	na	k7c4
12	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
Markétě	Markéta	k1gFnSc3
Davidové	Davidové	k2eAgFnSc3d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
předvedla	předvést	k5eAaPmAgFnS
dvě	dva	k4xCgFnPc4
čisté	čistý	k2eAgFnPc4d1
střelby	střelba	k1gFnPc4
a	a	k8xC
na	na	k7c6
třetím	třetí	k4xOgInSc6
úseku	úsek	k1gInSc6
byla	být	k5eAaImAgFnS
ze	z	k7c2
všech	všecek	k3xTgFnPc2
závodnic	závodnice	k1gFnPc2
nejrychlejší	rychlý	k2eAgFnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českou	český	k2eAgFnSc4d1
štafetu	štafeta	k1gFnSc4
tím	ten	k3xDgNnSc7
Davidová	Davidová	k1gFnSc1
dotáhla	dotáhnout	k5eAaPmAgFnS
do	do	k7c2
boje	boj	k1gInSc2
o	o	k7c4
medaile	medaile	k1gFnPc4
a	a	k8xC
Evě	Eva	k1gFnSc3
Puskarčíkové	Puskarčíková	k1gFnSc2
předala	předat	k5eAaPmAgFnS
na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
před	před	k7c4
Francouzku	Francouzka	k1gFnSc4
Anaï	Anaï	k1gFnPc2
Bescondovou	Bescondový	k2eAgFnSc4d1
a	a	k8xC
odolala	odolat	k5eAaPmAgFnS
Norce	norec	k1gInPc4
Tiril	Tirila	k1gFnPc2
Eckhoffové	Eckhoffový	k2eAgFnPc1d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
Puskarčíkové	Puskarčíková	k1gFnSc3
rychle	rychle	k6eAd1
přibližovala	přibližovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češky	Češka	k1gFnSc2
téměř	téměř	k6eAd1
po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
skončily	skončit	k5eAaPmAgInP
na	na	k7c6
bronzové	bronzový	k2eAgFnSc6d1
příčce	příčka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svůj	svůj	k3xOyFgInSc4
životní	životní	k2eAgInSc4d1
závod	závod	k1gInSc4
Davidová	Davidová	k1gFnSc1
zajela	zajet	k5eAaPmAgFnS
v	v	k7c6
italské	italský	k2eAgFnSc6d1
Anterselvě	Anterselva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
sprintu	sprint	k1gInSc2
na	na	k7c4
7,5	7,5	k4
kilometru	kilometr	k1gInSc2
dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2019	#num#	k4
vyrážela	vyrážet	k5eAaImAgFnS
pátá	pátý	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
rychlému	rychlý	k2eAgInSc3d1
běhu	běh	k1gInSc3
a	a	k8xC
přesné	přesný	k2eAgFnSc3d1
střelbě	střelba	k1gFnSc3
dokončila	dokončit	k5eAaPmAgFnS
závod	závod	k1gInSc4
s	s	k7c7
časem	čas	k1gInSc7
21	#num#	k4
<g/>
:	:	kIx,
<g/>
40,7	40,7	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závodě	závod	k1gInSc6
se	se	k3xPyFc4
k	k	k7c3
Davidové	Davidová	k1gFnSc3
nejvíce	nejvíce	k6eAd1,k6eAd3
přiblížila	přiblížit	k5eAaPmAgFnS
Finka	Finka	k1gFnSc1
Kaisa	Kaisa	k1gFnSc1
Mäkäräinenová	Mäkäräinenová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
na	na	k7c4
Davidovou	Davidová	k1gFnSc4
nestačila	stačit	k5eNaBmAgFnS
o	o	k7c4
1,7	1,7	k4
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
obsadila	obsadit	k5eAaPmAgFnS
Norka	Norka	k1gFnSc1
Olsbuová	Olsbuová	k1gFnSc1
Rø	Rø	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ztratila	ztratit	k5eAaPmAgFnS
na	na	k7c4
vítěznou	vítězný	k2eAgFnSc4d1
Davidovou	Davidová	k1gFnSc4
3,5	3,5	k4
vteřiny	vteřina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
podniku	podnik	k1gInSc6
pak	pak	k6eAd1
přidala	přidat	k5eAaPmAgFnS
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
závodě	závod	k1gInSc6
s	s	k7c7
hromadným	hromadný	k2eAgInSc7d1
startem	start	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nestačila	stačit	k5eNaBmAgFnS
jen	jen	k9
na	na	k7c4
Němku	Němka	k1gFnSc4
Lauru	Laura	k1gFnSc4
Dahlmeierovou	Dahlmeierová	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnSc4d1
individuální	individuální	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
získala	získat	k5eAaPmAgFnS
v	v	k7c6
kanadském	kanadský	k2eAgNnSc6d1
Canmore	Canmor	k1gMnSc5
<g/>
,	,	kIx,
kde	kde	k6eAd1
ve	v	k7c6
zkráceném	zkrácený	k2eAgInSc6d1
vytrvalostním	vytrvalostní	k2eAgInSc6d1
závodu	závod	k1gInSc6
jako	jako	k8xS,k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
mála	málo	k1gNnSc2
čistě	čistě	k6eAd1
střílela	střílet	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Předstihla	předstihnout	k5eAaPmAgFnS
ji	on	k3xPp3gFnSc4
jen	jen	k9
Norka	Norka	k1gFnSc1
Eckhoffová	Eckhoffový	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
udělala	udělat	k5eAaPmAgFnS
jednu	jeden	k4xCgFnSc4
chybu	chyba	k1gFnSc4
při	při	k7c6
střelbě	střelba	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
běžela	běžet	k5eAaImAgFnS
výrazně	výrazně	k6eAd1
rychleji	rychle	k6eAd2
než	než	k8xS
Davidová	Davidová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
druhým	druhý	k4xOgNnSc7
místem	místo	k1gNnSc7
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
s	s	k7c7
náskokem	náskok	k1gInSc7
do	do	k7c2
čela	čelo	k1gNnSc2
celkového	celkový	k2eAgNnSc2d1
hodnocení	hodnocení	k1gNnSc2
vytrvalostních	vytrvalostní	k2eAgInPc2d1
závodů	závod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závod	závod	k1gInSc1
s	s	k7c7
červeným	červený	k2eAgInSc7d1
dresem	dres	k1gInSc7
si	se	k3xPyFc3
vyzkoušela	vyzkoušet	k5eAaPmAgFnS
při	při	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
ve	v	k7c6
švédském	švédský	k2eAgInSc6d1
Östersundu	Östersund	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
malý	malý	k2eAgInSc4d1
křišťálový	křišťálový	k2eAgInSc4d1
glóbus	glóbus	k1gInSc4
nakonec	nakonec	k6eAd1
nezískala	získat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
Do	do	k7c2
sezony	sezona	k1gFnSc2
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
nastupovala	nastupovat	k5eAaImAgFnS
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
jako	jako	k8xC,k8xS
hlavní	hlavní	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
českého	český	k2eAgInSc2d1
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
medaile	medaile	k1gFnSc1
se	se	k3xPyFc4
dočkala	dočkat	k5eAaPmAgFnS
hned	hned	k6eAd1
v	v	k7c6
prvním	první	k4xOgInSc6
závodu	závod	k1gInSc6
sezony	sezona	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
obsadila	obsadit	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc1
místo	místo	k7c2
ve	v	k7c6
sprintu	sprint	k1gInSc6
v	v	k7c6
Östersundu	Östersund	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
získala	získat	k5eAaPmAgFnS
v	v	k7c4
Annecy	Anneca	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
skončila	skončit	k5eAaPmAgFnS
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
Östersundu	Östersund	k1gInSc6
třetí	třetí	k4xOgMnSc1
ve	v	k7c6
sprintu	sprint	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
první	první	k4xOgFnSc4
třech	tři	k4xCgInPc6
podnicích	podnik	k1gInPc6
světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
,	,	kIx,
takzvaném	takzvaný	k2eAgInSc6d1
trimestru	trimestr	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
pohybovala	pohybovat	k5eAaImAgFnS
vysoko	vysoko	k6eAd1
i	i	k9
v	v	k7c6
celkovém	celkový	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
vůbec	vůbec	k9
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
nepovedl	povést	k5eNaPmAgMnS
druhý	druhý	k4xOgInSc4
lednový	lednový	k2eAgInSc4d1
trimestr	trimestr	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
například	například	k6eAd1
v	v	k7c6
Ruhpoldingu	Ruhpolding	k1gInSc6
nedostala	dostat	k5eNaPmAgFnS
ani	ani	k8xC
do	do	k7c2
stíhacího	stíhací	k2eAgInSc2d1
závodu	závod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chuť	chuť	k1gFnSc1
si	se	k3xPyFc3
napravila	napravit	k5eAaPmAgFnS
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
v	v	k7c6
Antholzu	Antholz	k1gInSc6
2020	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
sehrála	sehrát	k5eAaPmAgFnS
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
ve	v	k7c6
smíšeném	smíšený	k2eAgInSc6d1
závodu	závod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
ve	v	k7c6
světovém	světový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
získala	získat	k5eAaPmAgFnS
v	v	k7c6
dalším	další	k2eAgInSc6d1
podniku	podnik	k1gInSc6
světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
po	po	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
doma	doma	k6eAd1
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
na	na	k7c6
Moravě	Morava	k1gFnSc6
obsadila	obsadit	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc1
místo	místo	k7c2
ve	v	k7c6
sprintu	sprint	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezonu	sezona	k1gFnSc4
dokončila	dokončit	k5eAaPmAgFnS
jako	jako	k9
čtrnáctá	čtrnáctý	k4xOgFnSc1
nejlepší	dobrý	k2eAgFnSc1d3
biatlonistka	biatlonistka	k1gFnSc1
sezony	sezona	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
bylo	být	k5eAaImAgNnS
zlepšení	zlepšení	k1gNnSc1
o	o	k7c4
sedm	sedm	k4xCc4
míst	místo	k1gNnPc2
oproti	oproti	k7c3
sezoně	sezona	k1gFnSc3
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
Olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
a	a	k8xC
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Akce	akce	k1gFnSc1
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
ZOH	ZOH	kA
Pchjongčchang	Pchjongčchang	k1gInSc4
<g/>
15.25.18.57	15.25.18.57	k4
<g/>
.12	.12	k4
<g/>
.8	.8	k4
<g/>
.	.	kIx.
<g/>
×	×	k?
<g/>
21	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
MS	MS	kA
Östersund	Östersund	k1gInSc4
<g/>
7.13.16.43	7.13.16.43	k4
<g/>
.15	.15	k4
<g/>
.6	.6	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
22	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
MS	MS	kA
Anterselva	Anterselva	k1gFnSc1
<g/>
37.25.20.8	37.25.20.8	k4
<g/>
.	.	kIx.
<g/>
4.3.14.23	4.3.14.23	k4
let	léto	k1gNnPc2
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
MS	MS	kA
Pokljuka	Pokljuk	k1gMnSc2
<g/>
44.32.13.1	44.32.13.1	k4
<g/>
.10	.10	k4
<g/>
.11	.11	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
24	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
2021	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
ZOH	ZOH	kA
Peking	Peking	k1gInSc4
<g/>
25	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
Výsledky	výsledek	k1gInPc1
z	z	k7c2
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
se	se	k3xPyFc4
započítávají	započítávat	k5eAaImIp3nP
do	do	k7c2
celkového	celkový	k2eAgNnSc2d1
hodnocení	hodnocení	k1gNnSc2
světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
,	,	kIx,
výsledky	výsledek	k1gInPc4
z	z	k7c2
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
se	se	k3xPyFc4
nezapočítávají	započítávat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Östersundnestartovala	Östersundnestartovala	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokljukanestartovala	Pokljukanestartovala	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
Město	město	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
69	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
70	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhofnestartovala	Oberhofnestartovala	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpolding	Ruhpolding	k1gInSc1
<g/>
28.48	28.48	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
75	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anterselvanestartovala	Anterselvanestartovala	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pchjongčchangnestartovala	Pchjongčchangnestartovala	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahtinestartovala	Kontiolahtinestartovala	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holmenkollennestartovala	Holmenkollennestartovala	k1gFnSc1
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
<g/>
72	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
nkl	nkl	k?
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
72	#num#	k4
%	%	kIx~
</s>
<s>
13	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
92	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Östersundnestartovala	Östersundnestartovala	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzen	k2eAgInSc4d1
<g/>
41.50	41.50	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annecy	Annec	k2eAgFnPc4d1
<g/>
35.58	35.58	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhof	Oberhof	k1gInSc1
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpolding	Ruhpolding	k1gInSc1
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anterselva	Anterselva	k1gFnSc1
<g/>
46.39	46.39	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahtinestartovala	Kontiolahtinestartovala	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holmenkollen	Holmenkollen	k1gInSc1
<g/>
79	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ťumeňnezúčastnila	Ťumeňnezúčastnila	k1gMnSc1
se	se	k3xPyFc4
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
<g/>
84	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
82	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
nkl	nkl	k?
<g/>
.	.	kIx.
</s>
<s>
nkl	nkl	k?
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
72	#num#	k4
%	%	kIx~
</s>
<s>
8	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
91	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokljuka	Pokljuk	k1gMnSc2
<g/>
32.49	32.49	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzen	k2eAgInSc4d1
<g/>
62	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
Město	město	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
67	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhof	Oberhof	k1gInSc1
<g/>
19.42	19.42	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpolding	Ruhpolding	k1gInSc1
<g/>
50	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anterselva	Anterselva	k1gFnSc1
<g/>
1.14	1.14	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s>
–	–	k?
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Canmore	Canmor	k1gInSc5
<g/>
–	–	k?
<g/>
2.7	2.7	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soldier	Soldier	k1gInSc1
Hollow	Hollow	k1gFnSc2
<g/>
32.12	32.12	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holmenkollen	Holmenkollen	k1gInSc1
<g/>
44.55	44.55	k4
<g/>
.14	.14	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3.7	3.7	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
79	#num#	k4
%	%	kIx~
</s>
<s>
428	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Östersund	Östersund	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
11.8	11.8	k4
<g/>
.11	.11	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzen	k2eAgInSc4d1
<g/>
35.24	35.24	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annecy	Annecum	k1gNnPc7
<g/>
3.28	3.28	k4
<g/>
.22	.22	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhof	Oberhof	k1gInSc1
<g/>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpolding	Ruhpolding	k1gInSc1
<g/>
71	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
–	–	k?
<g/>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokljuka	Pokljuk	k1gMnSc4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
39	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
Město	město	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahti	Kontiolahti	k1gNnSc1
<g/>
5.8	5.8	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holmenkollenzrušeno	Holmenkollenzrušen	k2eAgNnSc4d1
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
79	#num#	k4
%	%	kIx~
</s>
<s>
478	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahti	Kontiolahti	k1gNnSc1
<g/>
37	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
80	#num#	k4
%	%	kIx~
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahti	Kontiolahti	k1gNnPc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
6.22	6.22	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
70	#num#	k4
%	%	kIx~
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzen	k2eAgInSc4d1
<g/>
5.9	5.9	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
85	#num#	k4
%	%	kIx~
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzna	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
21.20	21.20	k4
<g/>
.4	.4	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
84	#num#	k4
%	%	kIx~
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhof	Oberhof	k1gInSc1
<g/>
7.12	7.12	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
83	#num#	k4
%	%	kIx~
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhof	Oberhof	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
85	#num#	k4
%	%	kIx~
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antholz-Anterselva	Antholz-Anterselva	k1gFnSc1
<g/>
–	–	k?
<g/>
5.41	5.41	k4
<g/>
.10	.10	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
81	#num#	k4
%	%	kIx~
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
Město	město	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
12.17	12.17	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
79	#num#	k4
%	%	kIx~
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
Město	město	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
11.6	11.6	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
88	#num#	k4
%	%	kIx~
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Östersund	Östersund	k1gInSc1
<g/>
70	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
63	#num#	k4
%	%	kIx~
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
83	#num#	k4
%	%	kIx~
</s>
<s>
649	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2021	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahti	Kontiolahti	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Östersund	Östersund	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzna	k1gFnPc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annecy	Annec	k2eAgFnPc4d1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhof	Oberhof	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpolding	Ruhpolding	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anterselva	Anterselva	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minsk	Minsk	k1gInSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otepää	Otepää	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holmenkollen-Oslo	Holmenkollen-Oslo	k1gFnSc1
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
</s>
<s>
Juniorská	juniorský	k2eAgNnPc1d1
mistrovství	mistrovství	k1gNnSc1
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
D	D	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
MSJ	MSJ	kA
Minsk	Minsk	k1gInSc4
<g/>
27.28.33.10	27.28.33.10	k4
<g/>
.18	.18	k4
let	léto	k1gNnPc2
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
MSJ	MSJ	kA
Cheile	Cheila	k1gFnSc6
Grădiştei	Grădiştei	k1gNnSc1
<g/>
4.10.3.2	4.10.3.2	k4
<g/>
.19	.19	k4
let	léto	k1gNnPc2
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
MSJ	MSJ	kA
Osrblie	Osrblie	k1gFnSc1
<g/>
43.14.18.11	43.14.18.11	k4
<g/>
.20	.20	k4
let	léto	k1gNnPc2
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
MSJ	MSJ	kA
Otepää	Otepää	k1gFnSc1
<g/>
2.1	2.1	k4
<g/>
.14	.14	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
21	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Medailová	medailový	k2eAgNnPc1d1
umístění	umístění	k1gNnPc1
v	v	k7c6
závodech	závod	k1gInPc6
Světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
</s>
<s>
Individuální	individuální	k2eAgFnSc1d1
</s>
<s>
Č.	Č.	kA
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Datum	datum	k1gNnSc1
</s>
<s>
Místo	místo	k7c2
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
1.201	1.201	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
20196	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
Pokljuka	Pokljuk	k1gMnSc2
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
vytrvalostní	vytrvalostní	k2eAgInSc1d1
závod	závod	k1gInSc1
</s>
<s>
2.24	2.24	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2019	#num#	k4
Anterselva	Anterselva	k1gFnSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
sprint	sprint	k1gInSc4
</s>
<s>
3.27	3.27	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2019	#num#	k4
Anterselva	Anterselva	k1gFnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
hromadný	hromadný	k2eAgInSc4d1
start	start	k1gInSc4
</s>
<s>
4.7	4.7	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
Canmore	Canmor	k1gInSc5
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
vytrvalostní	vytrvalostní	k2eAgInSc1d1
závod	závod	k1gInSc1
</s>
<s>
5.201	5.201	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
20201	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
Östersund	Östersund	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
sprint	sprint	k1gInSc4
</s>
<s>
6.20	6.20	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
Annecy	Anneca	k1gFnSc2
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
sprint	sprint	k1gInSc4
</s>
<s>
7.5	7.5	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
Nové	Nová	k1gFnSc6
Město	město	k1gNnSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
sprint	sprint	k1gInSc4
</s>
<s>
8.202	8.202	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
202116	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
Pokljuka	Pokljuk	k1gMnSc2
(	(	kIx(
<g/>
MS	MS	kA
<g/>
)	)	kIx)
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
vytrvalostní	vytrvalostní	k2eAgInSc1d1
závod	závod	k1gInSc1
</s>
<s>
–	–	k?
závod	závod	k1gInSc1
na	na	k7c4
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
</s>
<s>
Kolektivní	kolektivní	k2eAgNnSc1d1
</s>
<s>
Č.	Č.	kA
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Datum	datum	k1gNnSc1
</s>
<s>
Místo	místo	k7c2
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
1.201	1.201	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
201911	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2019	#num#	k4
Oberhof	Oberhof	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
štafeta	štafeta	k1gFnSc1
</s>
<s>
2.201	2.201	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
202013	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
Anterselva	Anterselva	k1gFnSc1
(	(	kIx(
<g/>
MS	MS	kA
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
smíšená	smíšený	k2eAgFnSc1d1
štafeta	štafeta	k1gFnSc1
</s>
<s>
–	–	k?
závod	závod	k1gInSc1
na	na	k7c4
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Tomáš	Tomáš	k1gMnSc1
Macek	Macek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dívka	dívka	k1gFnSc1
pro	pro	k7c4
biatlonovou	biatlonový	k2eAgFnSc4d1
budoucnost	budoucnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
rozlítaném	rozlítaný	k2eAgInSc6d1
životě	život	k1gInSc6
Markéty	Markéta	k1gFnSc2
Davidové	Davidová	k1gFnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-12-01	2016-12-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
třetí	třetí	k4xOgInSc1
na	na	k7c6
MS	MS	kA
dorostu	dorost	k1gInSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
biatlonu	biatlon	k1gInSc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MRZENA	mrzen	k2eAgFnSc1d1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královnou	královna	k1gFnSc7
biatlonového	biatlonový	k2eAgInSc2d1
roku	rok	k1gInSc2
je	být	k5eAaImIp3nS
Davidová	Davidová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládla	vládnout	k5eAaImAgFnS
i	i	k9
mezi	mezi	k7c4
fanoušky	fanoušek	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idnes	Idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-05-30	2019-05-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Davidová	Davidová	k1gFnSc1
je	být	k5eAaImIp3nS
podruhé	podruhé	k6eAd1
nejlepší	dobrý	k2eAgFnSc7d3
biatlonistkou	biatlonistka	k1gFnSc7
roku	rok	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualne	Aktualn	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-06-12	2020-06-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biatlon	biatlon	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Studium	studium	k1gNnSc1
mě	já	k3xPp1nSc2
velmi	velmi	k6eAd1
baví	bavit	k5eAaImIp3nS
<g/>
,	,	kIx,
i	i	k8xC
díky	díky	k7c3
tomu	ten	k3xDgMnSc3
ho	on	k3xPp3gNnSc4
zvládám	zvládat	k5eAaImIp1nS
skloubit	skloubit	k5eAaPmF
se	s	k7c7
sportem	sport	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČZU	ČZU	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-07	2019-03-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biatlonová	biatlonový	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
nechce	chtít	k5eNaImIp3nS
o	o	k7c6
pohybu	pohyb	k1gInSc6
ani	ani	k8xC
slyšet	slyšet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
jsem	být	k5eAaImIp1nS
nehnula	hnout	k5eNaPmAgFnS
brvou	brva	k1gFnSc7
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
Davidová	Davidová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-18	2019-04-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lidé	člověk	k1gMnPc1
na	na	k7c6
ČZU	ČZU	kA
-	-	kIx~
Bc.	Bc.	k1gMnSc2
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
<g/>
.	.	kIx.
is	is	k?
<g/>
.	.	kIx.
<g/>
czu	czu	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
<g/>
.	.	kIx.
www.facebook.com	www.facebook.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
<g/>
:	:	kIx,
Letní	letní	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
mě	já	k3xPp1nSc4
baví	bavit	k5eAaImIp3nS
<g/>
,	,	kIx,
chci	chtít	k5eAaImIp1nS
se	se	k3xPyFc4
stále	stále	k6eAd1
zlepšovat	zlepšovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Empresa	Empresa	k1gFnSc1
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
2019-09-24	2019-09-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
ŠANDORA	ŠANDORA	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
<g/>
:	:	kIx,
Jako	jako	k8xC,k8xS
ženská	ženský	k2eAgNnPc4d1
jednička	jednička	k1gFnSc1
se	se	k3xPyFc4
necítím	cítit	k5eNaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
VLTAVA	Vltava	k1gFnSc1
LABE	Labe	k1gNnSc2
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
2019-11-22	2019-11-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Davidová	Davidová	k1gFnSc1
šokovala	šokovat	k5eAaBmAgFnS
vítězstvím	vítězství	k1gNnSc7
v	v	k7c6
IBU	IBU	kA
Cupu	cup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodí	rodit	k5eAaImIp3nS
se	se	k3xPyFc4
nová	nový	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2016-11-28	2016-11-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
HERMANN	Hermann	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nominace	nominace	k1gFnSc1
do	do	k7c2
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgNnPc4
překvapivá	překvapivý	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
biatlonu	biatlon	k1gInSc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NOVÁČEK	Nováček	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezapomenutelný	zapomenutelný	k2eNgInSc1d1
debut	debut	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Davidová	Davidová	k1gFnSc1
si	se	k3xPyFc3
zkusila	zkusit	k5eAaPmAgFnS
"	"	kIx"
<g/>
velký	velký	k2eAgInSc1d1
biatlon	biatlon	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
teď	teď	k6eAd1
chce	chtít	k5eAaImIp3nS
fandit	fandit	k5eAaImF
u	u	k7c2
trati	trať	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2016-12-16	2016-12-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Štafeta	štafeta	k1gFnSc1
žen	žena	k1gFnPc2
potěšila	potěšit	k5eAaPmAgFnS
na	na	k7c4
závěr	závěr	k1gInSc4
II	II	kA
<g/>
.	.	kIx.
kola	kola	k1gFnSc1
SP	SP	kA
v	v	k7c4
Hochfilzenu	Hochfilzen	k2eAgFnSc4d1
šestou	šestý	k4xOgFnSc7
příčkou	příčka	k1gFnSc7
|	|	kIx~
Český	český	k2eAgInSc1d1
Biatlon	biatlon	k1gInSc1
<g/>
.	.	kIx.
www.biatlon.cz	www.biatlon.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dahlmeierová	Dahlmeierová	k1gFnSc1
si	se	k3xPyFc3
dojela	dojet	k5eAaPmAgFnS
pro	pro	k7c4
první	první	k4xOgInSc4
triumf	triumf	k1gInSc4
sezony	sezona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Puskarčíková	Puskarčíková	k1gFnSc1
pětadvacátá	pětadvacátý	k4xOgFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-12-16	2017-12-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BMW	BMW	kA
IBU	IBU	kA
World	World	k1gInSc1
Cup	cup	k1gInSc1
Biathlon	Biathlon	k1gInSc1
3	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
km	km	kA
Verfolgungswettkampf	Verfolgungswettkampf	k1gInSc4
Frauen	Frauna	k1gFnPc2
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
-	-	kIx~
Internationale	Internationale	k1gFnSc1
Biathlon	Biathlon	k1gInSc1
Union	union	k1gInSc1
-	-	kIx~
IBU	IBU	kA
<g/>
.	.	kIx.
de	de	k?
<g/>
.	.	kIx.
<g/>
biathlonworld	biathlonworld	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Senzace	senzace	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Davidová	Davidová	k1gFnSc1
končí	končit	k5eAaImIp3nS
třetí	třetí	k4xOgInSc4
<g/>
.	.	kIx.
www.sport.cz	www.sport.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-06	2018-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Neskutečná	skutečný	k2eNgFnSc1d1
stíhačka	stíhačka	k1gFnSc1
Davidové	Davidová	k1gFnSc2
s	s	k7c7
Puskarčíkovou	Puskarčíkův	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češky	Češka	k1gFnPc1
jsou	být	k5eAaImIp3nP
třetí	třetí	k4xOgInSc4
ve	v	k7c6
štafetě	štafeta	k1gFnSc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.sport.cz	www.sport.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-01-24	2019-01-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biatlonová	biatlonový	k2eAgFnSc1d1
fantazie	fantazie	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Bezchybná	bezchybný	k2eAgFnSc1d1
Davidová	Davidová	k1gFnSc1
vypálila	vypálit	k5eAaPmAgFnS
rybník	rybník	k1gInSc4
všem	všecek	k3xTgFnPc3
favoritkám	favoritka	k1gFnPc3
<g/>
.	.	kIx.
www.sport.cz	www.sport.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-01-13	2019-01-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
JÍROVEC	jírovec	k1gInSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Davidová	Davidová	k1gFnSc1
znovu	znovu	k6eAd1
skvostně	skvostně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češka	Češka	k1gFnSc1
nestačila	stačit	k5eNaBmAgFnS
v	v	k7c6
masáku	masák	k1gInSc6
jen	jen	k9
na	na	k7c4
Dahlmeierovou	Dahlmeierová	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2019-01-27	2019-01-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Davidová	Davidová	k1gFnSc1
dál	daleko	k6eAd2
ohromuje	ohromovat	k5eAaImIp3nS
<g/>
:	:	kIx,
A	a	k8xC
to	ten	k3xDgNnSc1
jsem	být	k5eAaImIp1nS
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
hlásila	hlásit	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
asi	asi	k9
umřu	umřít	k5eAaPmIp1nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-02-07	2019-02-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
na	na	k7c6
Instagramu	Instagram	k1gInSc6
</s>
<s>
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
na	na	k7c6
Facebooku	Facebook	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Mezinárodní	mezinárodní	k2eAgFnSc2d1
biatlonové	biatlonový	k2eAgFnSc2d1
unie	unie	k1gFnSc2
</s>
<s>
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistryně	mistryně	k1gFnSc1
světa	svět	k1gInSc2
v	v	k7c6
biatlonu	biatlon	k1gInSc6
–	–	k?
vytrvalostní	vytrvalostní	k2eAgInSc1d1
závod	závod	k1gInSc1
(	(	kIx(
<g/>
závod	závod	k1gInSc1
jednotlivkyň	jednotlivkyně	k1gFnPc2
<g/>
,	,	kIx,
15	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
<g/>
:	:	kIx,
Veněra	Veněra	k1gFnSc1
Černyšová	Černyšová	k1gFnSc1
•	•	k?
1985	#num#	k4
<g/>
:	:	kIx,
Kaja	Kaj	k2eAgFnSc1d1
Parveová	Parveová	k1gFnSc1
•	•	k?
1986	#num#	k4
<g/>
:	:	kIx,
Eva	Eva	k1gFnSc1
Korpela	Korpela	k1gFnSc1
•	•	k?
1987	#num#	k4
<g/>
:	:	kIx,
Sanna	Sanna	k1gFnSc1
Grönlidová	Grönlidový	k2eAgFnSc1d1
•	•	k?
1988	#num#	k4
<g/>
:	:	kIx,
Anne	Ann	k1gInSc2
Elvebakková	Elvebakková	k1gFnSc1
•	•	k?
1989	#num#	k4
<g/>
:	:	kIx,
Petra	Petra	k1gFnSc1
Schaaf	Schaaf	k1gInSc1
•	•	k?
1990	#num#	k4
<g/>
:	:	kIx,
Světlana	Světlana	k1gFnSc1
Davidovová	Davidovová	k1gFnSc1
•	•	k?
1991	#num#	k4
<g/>
:	:	kIx,
Petra	Petra	k1gFnSc1
Schaafová	Schaafový	k2eAgFnSc1d1
•	•	k?
1993	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Petra	Petra	k1gFnSc1
Schaafová	Schaafový	k2eAgFnSc1d1
•	•	k?
1995	#num#	k4
<g/>
:	:	kIx,
Corinne	Corinn	k1gInSc5
Niogretová	Niogretový	k2eAgFnSc1d1
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Emmanuelle	Emmanuella	k1gFnSc3
Claretová	Claretový	k2eAgFnSc1d1
•	•	k?
1997	#num#	k4
<g/>
:	:	kIx,
Magdalena	Magdalena	k1gFnSc1
Forsbergová	Forsbergový	k2eAgFnSc1d1
•	•	k?
1999	#num#	k4
<g/>
:	:	kIx,
Olena	Oleen	k2eAgFnSc1d1
Zubrilovová	Zubrilovová	k1gFnSc1
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Corinne	Corinn	k1gInSc5
Niogretová	Niogretový	k2eAgFnSc1d1
•	•	k?
2001	#num#	k4
<g/>
:	:	kIx,
Magdalena	Magdalena	k1gFnSc1
Forsbergová	Forsbergový	k2eAgFnSc1d1
•	•	k?
2003	#num#	k4
<g/>
:	:	kIx,
Kateřina	Kateřina	k1gFnSc1
Holubcová	Holubcová	k1gFnSc1
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
Olga	Olga	k1gFnSc1
Pylevová	Pylevová	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2005	#num#	k4
<g/>
:	:	kIx,
Andrea	Andrea	k1gFnSc1
Henkelová	Henkelová	k1gFnSc1
•	•	k?
2007	#num#	k4
<g/>
:	:	kIx,
Linda	Linda	k1gFnSc1
Grubbenová	Grubbenová	k1gFnSc1
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Jekatěrina	Jekatěrin	k2eAgFnSc1d1
Jurjevová	Jurjevová	k1gFnSc1
•	•	k?
2009	#num#	k4
<g/>
:	:	kIx,
Kati	kat	k1gMnPc1
Wilhelmová	Wilhelmová	k1gFnSc1
•	•	k?
2011	#num#	k4
<g/>
:	:	kIx,
Helena	Helena	k1gFnSc1
Ekholmová	Ekholmová	k1gFnSc1
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Tora	Tor	k1gMnSc2
Bergerová	Bergerová	k1gFnSc1
•	•	k?
2013	#num#	k4
<g/>
:	:	kIx,
Tora	Tor	k1gMnSc2
Bergerová	Bergerová	k1gFnSc1
•	•	k?
2015	#num#	k4
<g/>
:	:	kIx,
Jekatěrina	Jekatěrin	k2eAgFnSc1d1
Jurlovová	Jurlovová	k1gFnSc1
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
Marie	Marie	k1gFnSc1
Dorinová	Dorinová	k1gFnSc1
Habertová	Habertová	k1gFnSc1
•	•	k?
2017	#num#	k4
<g/>
:	:	kIx,
Laura	Laura	k1gFnSc1
Dahlmeierová	Dahlmeierová	k1gFnSc1
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Hanna	Hanna	k1gFnSc1
Öbergová	Öbergový	k2eAgFnSc1d1
•	•	k?
2020	#num#	k4
<g/>
:	:	kIx,
Dorothea	Dorothe	k2eAgFnSc1d1
Wiererová	Wiererová	k1gFnSc1
•	•	k?
2021	#num#	k4
<g/>
:	:	kIx,
Markéta	Markéta	k1gFnSc1
Davidová	Davidová	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
