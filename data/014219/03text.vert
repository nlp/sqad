<s>
Electro	Electro	k6eAd1
World	World	k1gInSc1
</s>
<s>
Electro	Electro	k6eAd1
World	World	k1gInSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
Logo	logo	k1gNnSc4
Základní	základní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
společnost	společnost	k1gFnSc1
s	s	k7c7
ručením	ručení	k1gNnSc7
omezeným	omezený	k2eAgNnSc7d1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
2002	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Chlumecká	chlumecký	k2eAgFnSc1d1
1531	#num#	k4
,	,	kIx,
Praha	Praha	k1gFnSc1
-	-	kIx~
Černý	černý	k2eAgInSc1d1
Most	most	k1gInSc1
Charakteristika	charakteristikon	k1gNnSc2
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
maloobchod	maloobchod	k1gInSc1
Produkty	produkt	k1gInPc1
</s>
<s>
spotřební	spotřební	k2eAgFnSc1d1
elektronika	elektronika	k1gFnSc1
Výsledek	výsledek	k1gInSc1
hospodaření	hospodaření	k1gNnSc2
</s>
<s>
▲	▲	k?
32	#num#	k4
784	#num#	k4
tis	tis	k1gInSc1
Kč	Kč	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zaměstnanci	zaměstnanec	k1gMnPc1
</s>
<s>
▲	▲	k?
770	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mateřská	mateřský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Nay	Nay	k?
a.s.	a.s.	k?
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInPc1d1
web	web	k1gInSc4
</s>
<s>
Electroworld	Electroworld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Electro	Electro	k6eAd1
World	World	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
největších	veliký	k2eAgInPc2d3
multikanálových	multikanálový	k2eAgInPc2d1
řetězců	řetězec	k1gInPc2
specializujících	specializující	k2eAgInPc2d1
se	se	k3xPyFc4
na	na	k7c4
prodej	prodej	k1gInSc4
spotřební	spotřební	k2eAgFnSc2d1
elektroniky	elektronika	k1gFnSc2
<g/>
,	,	kIx,
výpočetní	výpočetní	k2eAgFnSc2d1
a	a	k8xC
telekomunikační	telekomunikační	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
domácích	domácí	k2eAgInPc2d1
elektrospotřebičů	elektrospotřebič	k1gInPc2
<g/>
,	,	kIx,
veškerého	veškerý	k3xTgNnSc2
příslušenství	příslušenství	k1gNnSc2
a	a	k8xC
doplňků	doplněk	k1gInPc2
k	k	k7c3
nim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
včetně	včetně	k7c2
poskytování	poskytování	k1gNnSc2
souvisejících	související	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Firma	firma	k1gFnSc1
pod	pod	k7c7
značkou	značka	k1gFnSc7
Electro	Electro	k1gNnSc4
World	Worlda	k1gFnPc2
vstoupila	vstoupit	k5eAaPmAgFnS
na	na	k7c4
český	český	k2eAgInSc4d1
trh	trh	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
otevřela	otevřít	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
velkoformátovou	velkoformátový	k2eAgFnSc4d1
prodejnu	prodejna	k1gFnSc4
v	v	k7c6
pražském	pražský	k2eAgNnSc6d1
nákupním	nákupní	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
Metropole	metropol	k1gFnSc2
Zličín	Zličína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
provozuje	provozovat	k5eAaImIp3nS
19	#num#	k4
kamenných	kamenný	k2eAgFnPc2d1
velkoprodejen	velkoprodejna	k1gFnPc2
napříč	napříč	k7c7
celou	celý	k2eAgFnSc7d1
Českou	český	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
také	také	k9
úspěšně	úspěšně	k6eAd1
provozuje	provozovat	k5eAaImIp3nS
internetový	internetový	k2eAgInSc1d1
obchod	obchod	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
ještě	ještě	k9
širší	široký	k2eAgFnSc7d2
nabídkou	nabídka	k1gFnSc7
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
než	než	k8xS
lze	lze	k6eAd1
najít	najít	k5eAaPmF
v	v	k7c6
kamenných	kamenný	k2eAgFnPc6d1
prodejnách	prodejna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
hospodářského	hospodářský	k2eAgInSc2d1
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
převzetí	převzetí	k1gNnSc3
společnosti	společnost	k1gFnSc2
Electro	Electro	k1gNnSc1
World	World	k1gInSc1
novým	nový	k2eAgMnSc7d1
vlastníkem	vlastník	k1gMnSc7
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
NAY	NAY	kA
a.s.	a.s.	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
největší	veliký	k2eAgInSc1d3
obchodní	obchodní	k2eAgInSc1d1
řetězec	řetězec	k1gInSc1
se	s	k7c7
spotřební	spotřební	k2eAgFnSc7d1
elektronikou	elektronika	k1gFnSc7
a	a	k8xC
spotřebiči	spotřebič	k1gInPc7
pro	pro	k7c4
domácnost	domácnost	k1gFnSc4
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgNnSc7
spojením	spojení	k1gNnSc7
vznikl	vzniknout	k5eAaPmAgInS
významný	významný	k2eAgInSc1d1
regionální	regionální	k2eAgInSc1d1
subjekt	subjekt	k1gInSc1
s	s	k7c7
velkým	velký	k2eAgInSc7d1
potenciálem	potenciál	k1gInSc7
budoucího	budoucí	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vedení	vedení	k1gNnSc1
společnosti	společnost	k1gFnSc2
</s>
<s>
Generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
Ing.	ing.	kA
Martin	Martin	k1gMnSc1
Ohradzanský	Ohradzanský	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zastává	zastávat	k5eAaImIp3nS
pozici	pozice	k1gFnSc4
generálního	generální	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
také	také	k9
v	v	k7c6
mateřské	mateřský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
NAY	NAY	kA
a.s.	a.s.	k?
.	.	kIx.
</s>
<s desamb="1">
Jednateli	jednatel	k1gMnPc7
zodpovědnými	zodpovědný	k2eAgMnPc7d1
za	za	k7c2
řízení	řízení	k1gNnSc2
veškerých	veškerý	k3xTgFnPc2
aktivit	aktivita	k1gFnPc2
na	na	k7c6
českém	český	k2eAgInSc6d1
trhu	trh	k1gInSc6
jsou	být	k5eAaImIp3nP
Ing.	ing.	kA
Roman	Roman	k1gMnSc1
Kocourek	Kocourek	k1gMnSc1
a	a	k8xC
Michal	Michal	k1gMnSc1
Navrátil	Navrátil	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Věrnostní	věrnostní	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2015	#num#	k4
spustil	spustit	k5eAaPmAgInS
Electro	Electro	k1gNnSc4
World	Worlda	k1gFnPc2
vlastní	vlastnit	k5eAaImIp3nS
věrnostní	věrnostní	k2eAgInSc4d1
program	program	k1gInSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k6eAd1
jediným	jediný	k2eAgMnSc7d1
elektro-retailovým	elektro-retailův	k2eAgMnSc7d1
prodejcem	prodejce	k1gMnSc7
na	na	k7c6
českém	český	k2eAgInSc6d1
trhu	trh	k1gInSc6
s	s	k7c7
vlastní	vlastní	k2eAgFnSc7d1
věrnostní	věrnostní	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členství	členství	k1gNnSc1
ve	v	k7c6
věrnostním	věrnostní	k2eAgInSc6d1
klubu	klub	k1gInSc6
nabízí	nabízet	k5eAaImIp3nS
zákazníkům	zákazník	k1gMnPc3
řadu	řada	k1gFnSc4
benefitů	benefit	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
nabídka	nabídka	k1gFnSc1
se	se	k3xPyFc4
neustále	neustále	k6eAd1
rozšiřuje	rozšiřovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
hlavním	hlavní	k2eAgMnPc3d1
benefitům	benefita	k1gMnPc3
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
sbírání	sbírání	k1gNnSc1
bodů	bod	k1gInPc2
z	z	k7c2
každého	každý	k3xTgInSc2
nákupu	nákup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastřádané	nastřádaný	k2eAgInPc4d1
body	bod	k1gInPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
později	pozdě	k6eAd2
využít	využít	k5eAaPmF
při	při	k7c6
přepočtu	přepočet	k1gInSc6
na	na	k7c4
koruny	koruna	k1gFnPc4
jako	jako	k8xC,k8xS
slevu	sleva	k1gFnSc4
při	při	k7c6
některém	některý	k3yIgInSc6
z	z	k7c2
dalších	další	k2eAgInPc2d1
nákupů	nákup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
benefitem	benefit	k1gInSc7
je	být	k5eAaImIp3nS
okamžitá	okamžitý	k2eAgFnSc1d1
bezstarostná	bezstarostný	k2eAgFnSc1d1
výměna	výměna	k1gFnSc1
produktu	produkt	k1gInSc2
do	do	k7c2
1	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
bez	bez	k7c2
uplatnění	uplatnění	k1gNnSc2
zákonné	zákonný	k2eAgFnSc2d1
třicetidenní	třicetidenní	k2eAgFnSc2d1
lhůty	lhůta	k1gFnSc2
na	na	k7c4
vyřízení	vyřízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
věrnostního	věrnostní	k2eAgInSc2d1
klubu	klub	k1gInSc2
si	se	k3xPyFc3
nemusí	muset	k5eNaImIp3nP
dělat	dělat	k5eAaImF
starosti	starost	k1gFnPc4
ani	ani	k8xC
s	s	k7c7
archivací	archivace	k1gFnSc7
účtenek	účtenka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
benefitů	benefita	k1gMnPc2
věrnostního	věrnostní	k2eAgInSc2d1
klubu	klub	k1gInSc2
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
reklamace	reklamace	k1gFnSc1
velkých	velký	k2eAgInPc2d1
spotřebičů	spotřebič	k1gInPc2
z	z	k7c2
pohodlí	pohodlí	k1gNnSc2
domova	domov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Věrnostní	věrnostní	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
lze	lze	k6eAd1
zřídit	zřídit	k5eAaPmF
v	v	k7c6
kterékoliv	kterýkoliv	k3yIgFnSc6
z	z	k7c2
prodejen	prodejna	k1gFnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
sítě	síť	k1gFnSc2
Electro	Electro	k1gNnSc1
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
možné	možný	k2eAgNnSc1d1
vlastnit	vlastnit	k5eAaImF
virtuální	virtuální	k2eAgFnSc4d1
věrnostní	věrnostní	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
lze	lze	k6eAd1
kdykoliv	kdykoliv	k6eAd1
přeměnit	přeměnit	k5eAaPmF
na	na	k7c4
reálnou	reálný	k2eAgFnSc4d1
plastovou	plastový	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
formy	forma	k1gFnPc1
věrnostní	věrnostní	k2eAgFnPc1d1
karty	karta	k1gFnPc1
poskytují	poskytovat	k5eAaImIp3nP
stejné	stejný	k2eAgFnPc1d1
výhody	výhoda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
2019	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
počet	počet	k1gInSc1
členů	člen	k1gMnPc2
věrnostního	věrnostní	k2eAgInSc2d1
klubu	klub	k1gInSc2
hranice	hranice	k1gFnSc2
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reklamní	reklamní	k2eAgMnPc1d1
kampaně	kampaň	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
spustil	spustit	k5eAaPmAgInS
Electro	Electro	k1gNnSc4
World	Worldo	k1gNnPc2
novou	nový	k2eAgFnSc4d1
marketingovou	marketingový	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
slogan	slogan	k1gInSc1
„	„	k?
<g/>
Bez	bez	k7c2
starostí	starost	k1gFnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
komunikován	komunikovat	k5eAaImNgInS
ve	v	k7c6
všech	všecek	k3xTgInPc6
propagačních	propagační	k2eAgInPc6d1
kanálech	kanál	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
reklamních	reklamní	k2eAgFnPc2d1
kampaní	kampaň	k1gFnPc2
jsou	být	k5eAaImIp3nP
aktivně	aktivně	k6eAd1
zapojování	zapojování	k1gNnSc4
zaměstnanci	zaměstnanec	k1gMnSc3
prodejen	prodejna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
svými	svůj	k3xOyFgNnPc7
pravými	pravý	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
vystupují	vystupovat	k5eAaImIp3nP
v	v	k7c6
televizních	televizní	k2eAgInPc6d1
spotech	spot	k1gInPc6
i	i	k8xC
dalších	další	k2eAgInPc6d1
formátech	formát	k1gInPc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
tištěná	tištěný	k2eAgFnSc1d1
reklama	reklama	k1gFnSc1
<g/>
,	,	kIx,
letáky	leták	k1gInPc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Společnost	společnost	k1gFnSc1
Electro	Electro	k1gNnSc1
World	Worldo	k1gNnPc2
se	se	k3xPyFc4
dlouhodobě	dlouhodobě	k6eAd1
těší	těšit	k5eAaImIp3nS
oblibě	obliba	k1gFnSc3
zákaznické	zákaznický	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
potvrzují	potvrzovat	k5eAaImIp3nP
opakovaná	opakovaný	k2eAgNnPc4d1
ocenění	ocenění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ať	ať	k8xC,k8xS
už	už	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
několikrát	několikrát	k6eAd1
získaný	získaný	k2eAgInSc4d1
titul	titul	k1gInSc4
Obchodník	obchodník	k1gMnSc1
roku	rok	k1gInSc2
nebo	nebo	k8xC
o	o	k7c4
titul	titul	k1gInSc4
Superbrands	Superbrandsa	k1gFnPc2
<g/>
,	,	kIx,
kterým	který	k3yQgInPc3,k3yRgInPc3,k3yIgInPc3
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
Electro	Electro	k1gNnSc4
World	Worlda	k1gFnPc2
oceněna	oceněn	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titul	titul	k1gInSc1
Superbrands	Superbrands	k1gInSc4
je	být	k5eAaImIp3nS
uznáním	uznání	k1gNnSc7
vynikajícího	vynikající	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
značky	značka	k1gFnSc2
na	na	k7c6
lokálním	lokální	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Priority	priorita	k1gFnPc1
společnosti	společnost	k1gFnSc2
</s>
<s>
Electro	Electro	k6eAd1
World	World	k1gInSc1
průběžně	průběžně	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c6
rozvoji	rozvoj	k1gInSc6
Věrnostního	věrnostní	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
rozšiřování	rozšiřování	k1gNnSc4
a	a	k8xC
zkvalitňování	zkvalitňování	k1gNnSc4
služeb	služba	k1gFnPc2
poskytovaných	poskytovaný	k2eAgFnPc2d1
zákazníkům	zákazník	k1gMnPc3
a	a	k8xC
klade	klást	k5eAaImIp3nS
důraz	důraz	k1gInSc4
na	na	k7c6
tzv.	tzv.	kA
„	„	k?
<g/>
Omnichannel	Omnichannel	k1gMnSc1
<g/>
“	“	k?
strategii	strategie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neméně	málo	k6eNd2
důležitou	důležitý	k2eAgFnSc7d1
prioritou	priorita	k1gFnSc7
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
péče	péče	k1gFnSc1
o	o	k7c4
zaměstnance	zaměstnanec	k1gMnPc4
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
spokojenost	spokojenost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
odráží	odrážet	k5eAaImIp3nS
i	i	k9
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
výsledcích	výsledek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
důraz	důraz	k1gInSc1
je	být	k5eAaImIp3nS
kladen	klást	k5eAaImNgInS
také	také	k9
na	na	k7c4
možnost	možnost	k1gFnSc4
průběžného	průběžný	k2eAgInSc2d1
růstu	růst	k1gInSc2
zaměstnanců	zaměstnanec	k1gMnPc2
po	po	k7c6
odborné	odborný	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
a	a	k8xC
vytvoření	vytvoření	k1gNnSc4
co	co	k9
nejlepších	dobrý	k2eAgFnPc2d3
podmínek	podmínka	k1gFnPc2
pro	pro	k7c4
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Účetní	účetní	k2eAgFnSc1d1
závěrka	závěrka	k1gFnSc1
Electro	Electro	k1gNnSc1
World	World	k1gMnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
březnu	březen	k1gInSc6
2019	#num#	k4
↑	↑	k?
ELECTROWORLD	ELECTROWORLD	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodejny	prodejna	k1gFnSc2
Electro	Electro	k1gNnSc1
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ElectroWorld	ElectroWorld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ELECTROWORLD	ELECTROWORLD	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ElectroWorld	ElectroWorld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ElectroWorld	ElectroWorld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nay	Nay	k1gFnSc1
kupuje	kupovat	k5eAaImIp3nS
Electro	Electro	k1gNnSc4
World	Worlda	k1gFnPc2
v	v	k7c6
ČR	ČR	kA
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značku	značka	k1gFnSc4
zachová	zachovat	k5eAaPmIp3nS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-05-19	2014-05-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Veřejný	veřejný	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
a	a	k8xC
Sbírka	sbírka	k1gFnSc1
listin	listina	k1gFnPc2
-	-	kIx~
Ministerstvo	ministerstvo	k1gNnSc1
spravedlnosti	spravedlnost	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
or	or	k?
<g/>
.	.	kIx.
<g/>
justice	justice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KURZY	Kurz	k1gMnPc7
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Electro	Electro	k1gNnSc1
World	Worlda	k1gFnPc2
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
,	,	kIx,
Praha	Praha	k1gFnSc1
IČO	IČO	kA
26488361	#num#	k4
-	-	kIx~
Obchodní	obchodní	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
firem	firma	k1gFnPc2
|	|	kIx~
Kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
rejstrik-firem	rejstrik-firem	k6eAd1
<g/>
.	.	kIx.
<g/>
kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ELECTROWORLD	ELECTROWORLD	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodejny	prodejna	k1gFnSc2
Electro	Electro	k1gNnSc1
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ElectroWorld	ElectroWorld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
Volume	volum	k1gInSc5
7	#num#	k4
<g/>
.	.	kIx.
library	librara	k1gFnSc2
<g/>
.	.	kIx.
<g/>
superbrands	superbrands	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
Volume	volum	k1gInSc5
7	#num#	k4
<g/>
.	.	kIx.
library	librara	k1gFnSc2
<g/>
.	.	kIx.
<g/>
superbrands	superbrands	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Electro	Electro	k1gNnSc1
World	Worlda	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Electroworld	Electroworld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
společnosti	společnost	k1gFnSc2
</s>
