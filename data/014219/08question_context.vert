<s>
Electro	Electro	k6eAd1
World	World	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
největších	veliký	k2eAgInPc2d3
multikanálových	multikanálový	k2eAgInPc2d1
řetězců	řetězec	k1gInPc2
specializujících	specializující	k2eAgInPc2d1
se	se	k3xPyFc4
na	na	k7c4
prodej	prodej	k1gInSc4
spotřební	spotřební	k2eAgFnSc2d1
elektroniky	elektronika	k1gFnSc2
<g/>
,	,	kIx,
výpočetní	výpočetní	k2eAgFnSc2d1
a	a	k8xC
telekomunikační	telekomunikační	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
domácích	domácí	k2eAgInPc2d1
elektrospotřebičů	elektrospotřebič	k1gInPc2
<g/>
,	,	kIx,
veškerého	veškerý	k3xTgNnSc2
příslušenství	příslušenství	k1gNnSc2
a	a	k8xC
doplňků	doplněk	k1gInPc2
k	k	k7c3
nim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
včetně	včetně	k7c2
poskytování	poskytování	k1gNnSc2
souvisejících	související	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>