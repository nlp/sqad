<p>
<s>
Moldávie	Moldávie	k1gFnSc1	Moldávie
(	(	kIx(	(
<g/>
rumunsky	rumunsky	k6eAd1	rumunsky
Moldova	Moldův	k2eAgFnSc1d1	Moldova
<g/>
,	,	kIx,	,
starším	starý	k2eAgNnSc7d2	starší
českým	český	k2eAgNnSc7d1	české
označením	označení	k1gNnSc7	označení
též	též	k9	též
Multánsko	Multánsko	k1gNnSc1	Multánsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
historického	historický	k2eAgNnSc2d1	historické
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
mezi	mezi	k7c7	mezi
Karpaty	Karpaty	k1gInPc7	Karpaty
a	a	k8xC	a
řekou	řeka	k1gFnSc7	řeka
Dněstr	Dněstr	k1gInSc4	Dněstr
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1279	[number]	k4	1279
padlo	padnout	k5eAaPmAgNnS	padnout
území	území	k1gNnSc1	území
Moldávie	Moldávie	k1gFnSc2	Moldávie
do	do	k7c2	do
područí	područí	k1gNnSc2	područí
Mongolské	mongolský	k2eAgFnSc2d1	mongolská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Moldávie	Moldávie	k1gFnSc1	Moldávie
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1359	[number]	k4	1359
se	se	k3xPyFc4	se
moldavský	moldavský	k2eAgMnSc1d1	moldavský
kníže	kníže	k1gMnSc1	kníže
Bohdan	Bohdana	k1gFnPc2	Bohdana
I.	I.	kA	I.
uherského	uherský	k2eAgInSc2d1	uherský
vlivu	vliv	k1gInSc2	vliv
zbavil	zbavit	k5eAaPmAgInS	zbavit
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
nezávislým	závislý	k2eNgMnSc7d1	nezávislý
knížetem	kníže	k1gMnSc7	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
lenní	lenní	k2eAgFnSc2d1	lenní
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
polském	polský	k2eAgInSc6d1	polský
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
moldavským	moldavský	k2eAgMnSc7d1	moldavský
vládcem	vládce	k1gMnSc7	vládce
byl	být	k5eAaImAgMnS	být
kníže	kníže	k1gMnSc1	kníže
Stefan	Stefan	k1gMnSc1	Stefan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
1457	[number]	k4	1457
<g/>
–	–	k?	–
<g/>
1504	[number]	k4	1504
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
knížectví	knížectví	k1gNnSc1	knížectví
ohroženo	ohrožen	k2eAgNnSc1d1	ohroženo
agresí	agrese	k1gFnSc7	agrese
sousedního	sousední	k2eAgNnSc2d1	sousední
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
Stefan	Stefan	k1gMnSc1	Stefan
snažil	snažit	k5eAaImAgMnS	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
alianci	aliance	k1gFnSc4	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1489	[number]	k4	1489
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
Turky	Turek	k1gMnPc4	Turek
donucen	donucen	k2eAgInSc1d1	donucen
platit	platit	k5eAaImF	platit
tribut	tribut	k1gInSc4	tribut
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
uznal	uznat	k5eAaPmAgMnS	uznat
pak	pak	k6eAd1	pak
lenní	lenní	k2eAgFnSc4d1	lenní
závislost	závislost	k1gFnSc4	závislost
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
na	na	k7c6	na
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
získala	získat	k5eAaPmAgFnS	získat
Moldávie	Moldávie	k1gFnSc1	Moldávie
od	od	k7c2	od
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
samosprávu	samospráva	k1gFnSc4	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Moldávie	Moldávie	k1gFnSc2	Moldávie
západně	západně	k6eAd1	západně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Prut	prut	k1gInSc1	prut
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
Valašskem	Valašsko	k1gNnSc7	Valašsko
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Moldávie	Moldávie	k1gFnSc2	Moldávie
východně	východně	k6eAd1	východně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Prut	prut	k1gInSc1	prut
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
jako	jako	k8xC	jako
Besarábie	Besarábie	k1gFnSc2	Besarábie
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
znovu	znovu	k6eAd1	znovu
k	k	k7c3	k
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
větší	veliký	k2eAgFnSc6d2	veliký
části	část	k1gFnSc6	část
Besarábie	Besarábie	k1gFnSc1	Besarábie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
získala	získat	k5eAaPmAgFnS	získat
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
samostatnost	samostatnost	k1gFnSc4	samostatnost
jako	jako	k8xS	jako
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
Moldávie	Moldávie	k1gFnSc2	Moldávie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Besarábie	Besarábie	k1gFnSc1	Besarábie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Besarábie	Besarábie	k1gFnSc2	Besarábie
tvoří	tvořit	k5eAaImIp3nP	tvořit
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
historické	historický	k2eAgFnSc2d1	historická
Moldávie	Moldávie	k1gFnSc2	Moldávie
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Bukovina	Bukovina	k1gFnSc1	Bukovina
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
rumunskou	rumunský	k2eAgFnSc4d1	rumunská
a	a	k8xC	a
ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Moldávie	Moldávie	k1gFnSc2	Moldávie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Budžak	Budžak	k1gInSc1	Budžak
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Moldávie	Moldávie	k1gFnSc2	Moldávie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
