<s>
Státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
šest	šest	k4xCc1	šest
<g/>
:	:	kIx,	:
velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
malý	malý	k2eAgInSc1d1	malý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
státní	státní	k2eAgFnSc2d1	státní
barvy	barva	k1gFnSc2	barva
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
)	)	kIx)	)
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
vlajka	vlajka	k1gFnSc1	vlajka
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnPc1d1	Česká
republiky	republika	k1gFnPc1	republika
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
standarta	standarta	k1gFnSc1	standarta
<g/>
)	)	kIx)	)
státní	státní	k2eAgFnSc1d1	státní
pečeť	pečeť	k1gFnSc1	pečeť
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc1	můj
<g/>
?	?	kIx.	?
</s>
