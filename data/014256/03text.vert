<s>
Marjam	Marjam	k6eAd1
Mírzácháníová	Mírzácháníový	k2eAgFnSc1d1
</s>
<s>
Marjam	Marjam	k6eAd1
Mírzácháníová	Mírzácháníový	k2eAgFnSc1d1
Mírzácháníová	Mírzácháníová	k1gFnSc1
na	na	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
matematiků	matematik	k1gMnPc2
v	v	k7c6
Soulu	Soul	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
obdržela	obdržet	k5eAaPmAgFnS
Fieldsovu	Fieldsův	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
Narození	narození	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1977	#num#	k4
Teherán	Teherán	k1gInSc1
<g/>
,	,	kIx,
Írán	Írán	k1gInSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
40	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Stanford	Stanford	k1gInSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Sharif	Sharif	k1gMnSc1
University	universita	k1gFnSc2
of	of	k?
TechnologyHarvardova	TechnologyHarvardův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Pracoviště	pracoviště	k1gNnSc2
</s>
<s>
Princetonská	Princetonský	k2eAgFnSc1d1
univerzitaStanfordova	univerzitaStanfordův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Obor	obor	k1gInSc1
</s>
<s>
matematika	matematika	k1gFnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Blumenthal	Blumenthal	k1gMnSc1
Award	Award	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
Satter	Satter	k1gInSc1
Prize	Prize	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
Clay	Claa	k1gFnSc2
Research	Research	k1gMnSc1
Award	Award	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
Fieldsova	Fieldsův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Marjam	Marjam	k6eAd1
Mírzácháníová	Mírzácháníový	k2eAgFnSc1d1
(	(	kIx(
<g/>
nepřechýleně	přechýleně	k6eNd1
Mírzáchání	Mírzáchání	k1gNnSc1
<g/>
,	,	kIx,
psaná	psaný	k2eAgFnSc1d1
někdy	někdy	k6eAd1
jako	jako	k8xS,k8xC
Maryam	Maryam	k1gFnSc1
Mirzakhani	Mirzakhani	k1gFnSc1
<g/>
,	,	kIx,
persky	persky	k6eAd1
م	م	k?
م	م	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1977	#num#	k4
Teherán	Teherán	k1gInSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
íránská	íránský	k2eAgFnSc1d1
matematička	matematička	k1gFnSc1
a	a	k8xC
profesorka	profesorka	k1gFnSc1
matematiky	matematika	k1gFnSc2
na	na	k7c6
Stanfordově	Stanfordův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zabývala	zabývat	k5eAaImAgFnS
se	s	k7c7
především	především	k6eAd1
Teichmüllerovou	Teichmüllerův	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
<g/>
,	,	kIx,
hyperbolickou	hyperbolický	k2eAgFnSc7d1
geometrií	geometrie	k1gFnSc7
<g/>
,	,	kIx,
ergodickou	ergodický	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
a	a	k8xC
symplektickou	symplektický	k2eAgFnSc7d1
geometrií	geometrie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
první	první	k4xOgFnSc7
ženou	žena	k1gFnSc7
a	a	k8xC
první	první	k4xOgFnSc7
osobou	osoba	k1gFnSc7
íránského	íránský	k2eAgInSc2d1
původu	původ	k1gInSc2
oceněnou	oceněný	k2eAgFnSc4d1
Fieldsovou	Fieldsová	k1gFnSc4
medailí	medaile	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
považovaná	považovaný	k2eAgFnSc1d1
za	za	k7c4
nejprestižnější	prestižní	k2eAgNnSc4d3
matematické	matematický	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
a	a	k8xC
vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Marjam	Marjam	k6eAd1
Mírzácháníová	Mírzácháníová	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1977	#num#	k4
(	(	kIx(
<g/>
datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
<g/>
,	,	kIx,
anglická	anglický	k2eAgFnSc1d1
wikipedie	wikipedie	k1gFnSc1
-	-	kIx~
oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
,	,	kIx,
persky	persky	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
Teheránu	Teherán	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Íránu	Írán	k1gInSc6
v	v	k7c6
rodině	rodina	k1gFnSc6
elektroinženýra	elektroinženýr	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Navštěvovala	navštěvovat	k5eAaImAgFnS
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
Farzanegan	Farzanegana	k1gFnPc2
v	v	k7c6
Teheránu	Teherán	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Národní	národní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
výjimečných	výjimečný	k2eAgInPc2d1
talentů	talent	k1gInPc2
(	(	kIx(
<g/>
NODET	NODET	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgInSc6d1
ročníku	ročník	k1gInSc6
střední	střední	k2eAgFnSc2d1
školy	škola	k1gFnSc2
získala	získat	k5eAaPmAgFnS
na	na	k7c6
íránské	íránský	k2eAgFnSc6d1
národní	národní	k2eAgFnSc6d1
olympiádě	olympiáda	k1gFnSc6
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
za	za	k7c4
matematiku	matematika	k1gFnSc4
a	a	k8xC
bez	bez	k7c2
přijímacího	přijímací	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
na	na	k7c4
národní	národní	k2eAgFnSc4d1
vysokou	vysoký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
první	první	k4xOgFnSc7
íránskou	íránský	k2eAgFnSc7d1
ženou	žena	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
získala	získat	k5eAaPmAgFnS
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
matematické	matematický	k2eAgFnSc6d1
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
v	v	k7c6
Torontu	Toronto	k1gNnSc6
získala	získat	k5eAaPmAgFnS
dvě	dva	k4xCgFnPc4
zlaté	zlatý	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
matematické	matematický	k2eAgFnSc6d1
olympiádě	olympiáda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Mírzácháníová	Mírzácháníová	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gFnSc1
blízká	blízký	k2eAgFnSc1d1
přítelkyně	přítelkyně	k1gFnSc1
a	a	k8xC
kolegyně	kolegyně	k1gFnSc1
Roya	Roya	k?
Beheshti	Beheshti	k1gNnSc1
Zavarehová	Zavarehová	k1gFnSc1
byly	být	k5eAaImAgFnP
prvními	první	k4xOgFnPc7
ženami	žena	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
zúčastnily	zúčastnit	k5eAaPmAgFnP
íránské	íránský	k2eAgFnPc1d1
národní	národní	k2eAgFnPc1d1
matematické	matematický	k2eAgFnPc1d1
olympiády	olympiáda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
získaly	získat	k5eAaPmAgFnP
medaile	medaile	k1gFnPc1
a	a	k8xC
společně	společně	k6eAd1
pracovaly	pracovat	k5eAaImAgFnP
na	na	k7c6
knize	kniha	k1gFnSc6
Základy	základ	k1gInPc1
teorie	teorie	k1gFnSc2
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
náročné	náročný	k2eAgInPc1d1
problémy	problém	k1gInPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
maturitě	maturita	k1gFnSc6
a	a	k8xC
získání	získání	k1gNnSc6
bakalářského	bakalářský	k2eAgInSc2d1
titulu	titul	k1gInSc2
z	z	k7c2
matematiky	matematika	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
na	na	k7c4
Sharif	Sharif	k1gInSc4
University	universita	k1gFnSc2
of	of	k?
Technology	technolog	k1gMnPc7
(	(	kIx(
<g/>
Šarifově	Šarifův	k2eAgFnSc6d1
technologické	technologický	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
)	)	kIx)
v	v	k7c6
Teheránu	Teherán	k1gInSc6
<g/>
,	,	kIx,
Mírzácháníová	Mírzácháníová	k1gFnSc1
odešla	odejít	k5eAaPmAgFnS
do	do	k7c2
USA	USA	kA
studovat	studovat	k5eAaImF
na	na	k7c4
Harvardovu	Harvardův	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
zde	zde	k6eAd1
získala	získat	k5eAaPmAgFnS
titul	titul	k1gInSc4
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
obhajobou	obhajoba	k1gFnSc7
disertační	disertační	k2eAgFnSc2d1
práce	práce	k1gFnSc2
na	na	k7c4
téma	téma	k1gNnSc4
hyperbolických	hyperbolický	k2eAgInPc2d1
povrchů	povrch	k1gInPc2
(	(	kIx(
<g/>
potvrdila	potvrdit	k5eAaPmAgFnS
Wittenovu	Wittenův	k2eAgFnSc4d1
domněnku	domněnka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
hraje	hrát	k5eAaImIp3nS
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
teorii	teorie	k1gFnSc6
superstrun	superstruna	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
byla	být	k5eAaImAgFnS
oceněna	ocenit	k5eAaPmNgFnS
ve	v	k7c6
čtvrtém	čtvrtý	k4xOgInSc6
ročníku	ročník	k1gInSc6
Popular	Popular	k1gInSc1
Science	Scienec	k1gInPc1
„	„	k?
<g/>
Brilliant	Brilliant	k1gInSc1
10	#num#	k4
<g/>
“	“	k?
jako	jako	k8xC,k8xS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
10	#num#	k4
nejlepších	dobrý	k2eAgMnPc2d3
mladých	mladý	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
posunuli	posunout	k5eAaPmAgMnP
své	svůj	k3xOyFgInPc4
obory	obor	k1gInPc4
inovativním	inovativní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
<g/>
,	,	kIx,
Fieldsova	Fieldsův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Mírzácháníová	Mírzácháníová	k1gFnSc1
působila	působit	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
jako	jako	k8xS,k8xC
výzkumná	výzkumný	k2eAgFnSc1d1
pracovnice	pracovnice	k1gFnSc1
v	v	k7c6
Clayově	Clayův	k2eAgInSc6d1
matematickém	matematický	k2eAgInSc6d1
ústavu	ústav	k1gInSc6
(	(	kIx(
<g/>
Princetonská	Princetonský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
profesorka	profesorka	k1gFnSc1
na	na	k7c6
téže	tenže	k3xDgFnSc6,k3xTgFnSc6
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
profesorkou	profesorka	k1gFnSc7
na	na	k7c6
Stanfordově	Stanfordův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
první	první	k4xOgFnSc7
ženou	žena	k1gFnSc7
a	a	k8xC
první	první	k4xOgFnSc7
osobou	osoba	k1gFnSc7
íránského	íránský	k2eAgInSc2d1
původu	původ	k1gInSc2
oceněnou	oceněný	k2eAgFnSc4d1
Fieldsovou	Fieldsová	k1gFnSc4
medailí	medaile	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
Soulu	Soul	k1gInSc6
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
považovaná	považovaný	k2eAgFnSc1d1
za	za	k7c4
nejprestižnější	prestižní	k2eAgNnSc4d3
matematické	matematický	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
<g/>
,	,	kIx,
cenu	cena	k1gFnSc4
obdržela	obdržet	k5eAaPmAgFnS
za	za	k7c4
práci	práce	k1gFnSc4
o	o	k7c4
symetrii	symetrie	k1gFnSc4
zakřivených	zakřivený	k2eAgInPc2d1
povrchů	povrch	k1gInPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
se	se	k3xPyFc4
Marjam	Marjam	k1gInSc4
Mírzácháníová	Mírzácháníová	k1gFnSc1
vdala	vdát	k5eAaPmAgFnS
za	za	k7c2
Jana	Jan	k1gMnSc2
Vondráka	Vondrák	k1gMnSc2
<g/>
,	,	kIx,
českého	český	k2eAgMnSc2d1
teoretického	teoretický	k2eAgMnSc2d1
informatika	informatik	k1gMnSc2
a	a	k8xC
aplikovaného	aplikovaný	k2eAgMnSc2d1
matematika	matematik	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznámila	seznámit	k5eAaPmAgFnS
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
na	na	k7c6
Stanfordově	Stanfordův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
oba	dva	k4xCgMnPc1
pracovali	pracovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
narodila	narodit	k5eAaPmAgFnS
dcera	dcera	k1gFnSc1
Anahita	Anahita	k1gFnSc1
<g/>
,	,	kIx,
žili	žít	k5eAaImAgMnP
v	v	k7c6
Palo	Pala	k1gMnSc5
Altu	alt	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
jí	on	k3xPp3gFnSc3
byla	být	k5eAaImAgFnS
diagnostikována	diagnostikovat	k5eAaBmNgFnS
rakovina	rakovina	k1gFnSc1
prsu	prs	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
40	#num#	k4
let	léto	k1gNnPc2
podlehla	podlehnout	k5eAaPmAgFnS
ve	v	k7c6
Stanfordské	Stanfordský	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Nemyslím	myslet	k5eNaImIp1nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
každý	každý	k3xTgMnSc1
měl	mít	k5eAaImAgMnS
stát	stát	k5eAaPmF,k5eAaImF
matematikem	matematik	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
podle	podle	k7c2
mě	já	k3xPp1nSc2
nedá	dát	k5eNaPmIp3nS
řada	řada	k1gFnSc1
studentů	student	k1gMnPc2
matematice	matematika	k1gFnSc3
vůbec	vůbec	k9
šanci	šance	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
začátku	začátek	k1gInSc2
jsem	být	k5eAaImIp1nS
byla	být	k5eAaImAgFnS
v	v	k7c6
matematice	matematika	k1gFnSc6
špatná	špatná	k1gFnSc1
<g/>
,	,	kIx,
nechtěla	chtít	k5eNaImAgFnS
jsem	být	k5eAaImIp1nS
o	o	k7c6
ničem	nic	k3yNnSc6
přemýšlet	přemýšlet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejste	být	k5eNaImIp2nP
<g/>
-li	-li	k?
do	do	k7c2
matematiky	matematika	k1gFnSc2
nadšení	nadšení	k1gNnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
jevit	jevit	k5eAaImF
jako	jako	k9
chladný	chladný	k2eAgInSc4d1
a	a	k8xC
zbytečný	zbytečný	k2eAgInSc4d1
obor	obor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krása	krása	k1gFnSc1
matematiky	matematika	k1gFnSc2
se	se	k3xPyFc4
ukáže	ukázat	k5eAaPmIp3nS
pouze	pouze	k6eAd1
těm	ten	k3xDgMnPc3
trpělivějším	trpělivý	k2eAgMnPc3d2
<g/>
.	.	kIx.
<g/>
“	“	k?
Marjam	Marjam	k1gInSc1
Mírzácháníová	Mírzácháníová	k1gFnSc1
<g/>
,	,	kIx,
rozhovor	rozhovor	k1gInSc1
pro	pro	k7c4
The	The	k1gFnSc4
Guardian	Guardiana	k1gFnPc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkaz	odkaz	k1gInSc1
</s>
<s>
Na	na	k7c4
její	její	k3xOp3gFnSc4
počest	počest	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
střední	střední	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Farzanegan	Farzanegana	k1gFnPc2
pojmenovala	pojmenovat	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
amfiteátr	amfiteátr	k1gInSc4
a	a	k8xC
knihovnu	knihovna	k1gFnSc4
jejím	její	k3xOp3gNnSc7
jménem	jméno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
Sharif	Sharif	k1gInSc1
University	universita	k1gFnSc2
of	of	k?
Technology	technolog	k1gMnPc4
pojmenovala	pojmenovat	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
hlavní	hlavní	k2eAgFnSc4d1
knihovnu	knihovna	k1gFnSc4
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
matematiky	matematika	k1gFnSc2
jejím	její	k3xOp3gNnSc7
jménem	jméno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dům	dům	k1gInSc1
matematiky	matematika	k1gFnSc2
v	v	k7c6
Isfahánu	Isfahán	k1gInSc6
po	po	k7c6
ní	on	k3xPp3gFnSc6
pojmenoval	pojmenovat	k5eAaPmAgInS
konferenční	konferenční	k2eAgInSc1d1
sál	sál	k1gInSc1
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
založili	založit	k5eAaPmAgMnP
studenti	student	k1gMnPc1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
společnost	společnost	k1gFnSc1
„	„	k?
<g/>
Mirzakhani	Mirzakhan	k1gMnPc1
Society	societa	k1gFnSc2
<g/>
“	“	k?
pro	pro	k7c4
ženy	žena	k1gFnPc4
(	(	kIx(
<g/>
studentky	studentka	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
studenty	student	k1gMnPc7
matematiky	matematika	k1gFnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mirzakhaníová	Mirzakhaníová	k1gFnSc1
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
setkala	setkat	k5eAaPmAgFnS
v	v	k7c6
září	září	k1gNnSc6
2015	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
navštívila	navštívit	k5eAaPmAgFnS
Oxford	Oxford	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2019	#num#	k4
Nadace	nadace	k1gFnSc2
„	„	k?
<g/>
The	The	k1gMnSc1
Breakth	Breakth	k1gMnSc1
Prize	Prize	k1gFnSc1
Prize	Prize	k1gFnSc1
Foundation	Foundation	k1gInSc4
<g/>
“	“	k?
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
vytvořena	vytvořen	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Maryam	Maryam	k1gInSc4
Mirzakhaníové	Mirzakhaníová	k1gFnSc2
„	„	k?
<g/>
New	New	k1gFnSc2
Frontiers	Frontiersa	k1gFnPc2
Prize	Prize	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
uděluje	udělovat	k5eAaImIp3nS
vynikajícím	vynikající	k2eAgFnPc3d1
ženám	žena	k1gFnPc3
v	v	k7c6
oblasti	oblast	k1gFnSc6
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2020	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
Mezinárodní	mezinárodní	k2eAgInSc4d1
den	den	k1gInSc4
žen	žena	k1gFnPc2
v	v	k7c6
STEM	sto	k4xCgNnSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
Mirzakhaníová	Mirzakhaníový	k2eAgNnPc1d1
oceněna	oceněn	k2eAgNnPc1d1
UN	UN	kA
Women	Womna	k1gFnPc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xC,k8xS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
sedmi	sedm	k4xCc2
vědeckých	vědecký	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
formovaly	formovat	k5eAaImAgFnP
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
ji	on	k3xPp3gFnSc4
George	George	k1gFnSc4
Csicsery	Csicsera	k1gFnSc2
představil	představit	k5eAaPmAgInS
v	v	k7c6
dokumentárním	dokumentární	k2eAgInSc6d1
filmu	film	k1gInSc6
<g/>
:	:	kIx,
„	„	k?
<g/>
Tajemství	tajemství	k1gNnSc1
povrchu	povrch	k1gInSc2
<g/>
:	:	kIx,
Matematická	matematický	k2eAgFnSc1d1
vize	vize	k1gFnSc1
Maryam	Maryam	k1gInSc1
Mirzakhani	Mirzakhaň	k1gFnSc6
<g/>
“	“	k?
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Gold	Gold	k1gMnSc1
medal	medal	k1gMnSc1
International	International	k1gMnSc1
Mathematical	Mathematical	k1gMnSc1
Olympiad	Olympiad	k1gInSc1
(	(	kIx(
<g/>
Hong	Honga	k1gFnPc2
Kong	Kongo	k1gNnPc2
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gold	Gold	k1gMnSc1
medal	medal	k1gMnSc1
International	International	k1gMnSc1
Mathematical	Mathematical	k1gMnSc1
olympiad	olympiad	k6eAd1
(	(	kIx(
<g/>
Canada	Canada	k1gFnSc1
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
IPM	IPM	kA
Fellowship	Fellowship	k1gMnSc1
The	The	k1gMnSc1
Institute	institut	k1gInSc5
for	forum	k1gNnPc2
theoretical	theoreticat	k5eAaPmAgMnS
Physics	Physics	k1gInSc4
and	and	k?
Mathematics	Mathematics	k1gInSc1
<g/>
,	,	kIx,
Tehran	Tehran	k1gInSc1
<g/>
,	,	kIx,
Iran	Iran	k1gInSc1
<g/>
,	,	kIx,
1995-1999	1995-1999	k4
</s>
<s>
Harvard	Harvard	k1gInSc1
Junior	junior	k1gMnSc1
Fellowship	Fellowship	k1gMnSc1
Harvard	Harvard	k1gInSc4
University	universita	k1gFnSc2
<g/>
,	,	kIx,
2003	#num#	k4
</s>
<s>
Merit	meritum	k1gNnPc2
fellowship	fellowship	k1gInSc4
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
<g/>
,	,	kIx,
2003	#num#	k4
</s>
<s>
Clay	Claa	k1gFnPc1
Mathematics	Mathematicsa	k1gFnPc2
Institute	institut	k1gInSc5
Research	Research	k1gInSc1
Fellow	Fellow	k1gMnPc1
<g/>
,	,	kIx,
2004	#num#	k4
</s>
<s>
Satter	Satter	k1gMnSc1
Price	Price	k1gMnSc1
<g/>
,	,	kIx,
2013	#num#	k4
</s>
<s>
Blumenthal	Blumenthal	k1gMnSc1
Award	Award	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
Clay	Claa	k1gMnSc2
Research	Research	k1gMnSc1
Award	Award	k1gMnSc1
<g/>
,	,	kIx,
2014	#num#	k4
</s>
<s>
Fieldsova	Fieldsův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
<g/>
,	,	kIx,
2014	#num#	k4
</s>
<s>
Členka	členka	k1gFnSc1
Americké	americký	k2eAgFnSc2d1
filozofické	filozofický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
</s>
<s>
Čenka	Čenka	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
</s>
<s>
Členka	členka	k1gFnSc1
Americké	americký	k2eAgFnSc2d1
akademii	akademie	k1gFnSc4
umění	umění	k1gNnSc2
a	a	k8xC
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Pojmenován	pojmenován	k2eAgInSc1d1
„	„	k?
<g/>
Asteroid	asteroid	k1gInSc1
321357	#num#	k4
Mirzakhani	Mirzakhaň	k1gFnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Maryam	Maryam	k1gInSc1
Mirzakhani	Mirzakhaň	k1gFnSc3
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
MÍRZÁCHÁNÍ	MÍRZÁCHÁNÍ	kA
<g/>
,	,	kIx,
Marjam	Marjam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Weil-Petersson	Weil-Petersson	k1gMnSc1
volumes	volumes	k1gMnSc1
and	and	k?
intersection	intersection	k1gInSc1
theory	theora	k1gFnSc2
on	on	k3xPp3gMnSc1
the	the	k?
moduli	module	k1gFnSc3
space	space	k1gMnSc1
of	of	k?
curves	curves	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Mathematical	Mathematical	k1gMnPc3
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
2007-01-01	2007-01-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
0	#num#	k4
<g/>
894	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
347	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
526	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Report	report	k1gInSc1
of	of	k?
the	the	k?
President	president	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
Board	Board	k1gInSc1
of	of	k?
Trustees	Trustees	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanfordova	Stanfordův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2008-04-09	2008-04-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
KIDS	KIDS	kA
<g/>
,	,	kIx,
Baby	baby	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
JeduEdu	JeduEd	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-29	2019-04-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Matematička	matematička	k1gFnSc1
Marjam	Marjam	k1gInSc1
Mírzácháníová	Mírzácháníová	k1gFnSc1
svou	svůj	k3xOyFgFnSc4
práci	práce	k1gFnSc4
přirovnávala	přirovnávat	k5eAaImAgFnS
k	k	k7c3
psaní	psaní	k1gNnSc3
románů	román	k1gInPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-07-20	2017-07-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Wayback	Wayback	k1gInSc1
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-11-24	2005-11-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Íránská	íránský	k2eAgNnPc1d1
média	médium	k1gNnPc1
čelí	čelit	k5eAaImIp3nS
kritice	kritika	k1gFnSc3
<g/>
.	.	kIx.
‚	‚	k?
<g/>
Zahalila	zahalit	k5eAaPmAgFnS
<g/>
‘	‘	k?
úspěšnou	úspěšný	k2eAgFnSc4d1
matematičku	matematička	k1gFnSc4
|	|	kIx~
Svět	svět	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-08-18	2014-08-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Iranian	Iranian	k1gMnSc1
math	math	k1gMnSc1
genius	genius	k1gMnSc1
<g/>
,	,	kIx,
Fields	Fields	k1gInSc1
Medal	Medal	k1gMnSc1
winner	winner	k1gMnSc1
Mirzakhani	Mirzakhaň	k1gFnSc3
passes	passes	k1gInSc4
away	awaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehran	Tehran	k1gInSc1
Times	Times	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-07-15	2017-07-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dignitaries	Dignitaries	k1gMnSc1
grieve	grieev	k1gFnSc2
death	death	k1gMnSc1
of	of	k?
math	math	k1gInSc1
genius	genius	k1gMnSc1
Maryam	Maryam	k1gInSc1
Mirzakhani	Mirzakhan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehran	Tehran	k1gInSc1
Times	Times	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-07-15	2017-07-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gFnSc2
Mirzakhani	Mirzakhaň	k1gFnSc3
Society	societa	k1gFnSc2
-	-	kIx~
Oxford	Oxford	k1gInSc1
Mathematics	Mathematics	k1gInSc1
<g/>
'	'	kIx"
society	societa	k1gFnPc1
for	forum	k1gNnPc2
female	female	k6eAd1
undergraduates	undergraduatesa	k1gFnPc2
|	|	kIx~
Mathematical	Mathematical	k1gFnSc1
Institute	institut	k1gInSc5
<g/>
.	.	kIx.
www.maths.ox.ac.uk	www.maths.ox.ac.uk	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Breakthrough	Breakthrough	k1gMnSc1
Prize	Prize	k1gFnSc2
–	–	k?
Breakthrough	Breakthrough	k1gMnSc1
Prize	Prize	k1gFnSc2
Foundation	Foundation	k1gInSc1
Announces	Announces	k1gMnSc1
Maryam	Maryam	k1gInSc1
Mirzakhani	Mirzakhaň	k1gFnSc6
New	New	k1gFnPc1
Frontiers	Frontiersa	k1gFnPc2
Prize	Prize	k1gFnPc4
for	forum	k1gNnPc2
Women	Women	k2eAgInSc1d1
Mathematicians	Mathematicians	k1gInSc1
<g/>
.	.	kIx.
breakthroughprize	breakthroughprize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Devoted	Devoted	k1gInSc1
to	ten	k3xDgNnSc1
discovery	discovera	k1gFnPc1
<g/>
:	:	kIx,
seven	seven	k2eAgInSc1d1
women	women	k2eAgInSc1d1
scientists	scientists	k1gInSc1
who	who	k?
have	have	k1gInSc1
shaped	shaped	k1gMnSc1
our	our	k?
world	world	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UN	UN	kA
Women	Womna	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Secrets	Secrets	k1gInSc1
of	of	k?
the	the	k?
Surface	Surface	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Birkbeck	Birkbecka	k1gFnPc2
<g/>
,	,	kIx,
University	universita	k1gFnSc2
of	of	k?
London	London	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
Curriculum	Curriculum	k1gNnSc1
Vitae	Vita	k1gInSc2
Maryam	Maryam	k1gInSc1
Mirzakhani	Mirzakhan	k1gMnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Držitelé	držitel	k1gMnPc1
Fieldsovy	Fieldsův	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
</s>
<s>
Lars	Lars	k6eAd1
Ahlfors	Ahlfors	k1gInSc1
<g/>
,	,	kIx,
Jesse	Jesse	k1gFnSc1
Douglas	Douglas	k1gMnSc1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Laurent	Laurent	k1gMnSc1
Schwartz	Schwartz	k1gMnSc1
<g/>
,	,	kIx,
Atle	Atle	k1gFnSc1
Selberg	Selberg	k1gMnSc1
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kunihiko	Kunihika	k1gFnSc5
Kodaira	Kodair	k1gMnSc2
<g/>
,	,	kIx,
Jean-Pierre	Jean-Pierr	k1gMnSc5
Serre	Serr	k1gMnSc5
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Klaus	Klaus	k1gMnSc1
Roth	Roth	k1gMnSc1
<g/>
,	,	kIx,
René	René	k1gMnSc1
Thom	Thom	k1gMnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lars	Lars	k1gInSc1
Hörmander	Hörmander	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
John	John	k1gMnSc1
Milnor	Milnor	k1gMnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michael	Michael	k1gMnSc1
Atiyah	Atiyah	k1gMnSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
Cohen	Cohna	k1gFnPc2
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
Grothendieck	Grothendiecka	k1gFnPc2
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
Smale	Smale	k1gInSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alan	Alan	k1gMnSc1
Baker	Baker	k1gMnSc1
<g/>
,	,	kIx,
Heisuke	Heisuk	k1gMnPc4
Hironaka	Hironak	k1gMnSc4
<g/>
,	,	kIx,
Sergej	Sergej	k1gMnSc1
Petrovič	Petrovič	k1gMnSc1
Novikov	Novikov	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Griggs	Griggsa	k1gFnPc2
Thompson	Thompson	k1gMnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Enrico	Enrico	k1gMnSc1
Bombieri	Bombier	k1gMnPc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Mumford	Mumford	k1gMnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pierre	Pierr	k1gMnSc5
Deligne	Delign	k1gMnSc5
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Fefferman	Fefferman	k1gMnSc1
<g/>
,	,	kIx,
Grigorij	Grigorij	k1gMnSc1
Margulis	Margulis	k1gFnSc2
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Quillen	Quillen	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alain	Alain	k1gMnSc1
Connes	Connes	k1gMnSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
Thurston	Thurston	k1gInSc4
<g/>
,	,	kIx,
Čchiou	Čchiá	k1gFnSc4
Čcheng-tung	Čcheng-tunga	k1gFnPc2
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Simon	Simon	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Donaldson	Donaldson	k1gMnSc1
<g/>
,	,	kIx,
Gerd	Gerd	k1gMnSc1
Faltings	Faltings	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Freedman	Freedman	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladimir	Vladimir	k1gMnSc1
Drinfeld	Drinfeld	k1gMnSc1
<g/>
,	,	kIx,
Vaughan	Vaughan	k1gMnSc1
Jones	Jones	k1gMnSc1
<g/>
,	,	kIx,
Šigefumi	Šigefu	k1gFnPc7
Mori	Mori	k1gNnSc2
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
Witten	Witten	k2eAgMnSc1d1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jefim	Jefim	k1gInSc1
Zelmanov	Zelmanov	k1gInSc1
<g/>
,	,	kIx,
Pierre-Louis	Pierre-Louis	k1gInSc1
Lions	Lions	k1gInSc1
<g/>
,	,	kIx,
Jean	Jean	k1gMnSc1
Bourgain	Bourgain	k1gMnSc1
<g/>
,	,	kIx,
Jean-Christophe	Jean-Christophe	k1gFnSc1
Yoccoz	Yoccoz	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Richard	Richard	k1gMnSc1
Borcherds	Borcherds	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
Timothy	Timotha	k1gFnSc2
Gowers	Gowersa	k1gFnPc2
<g/>
,	,	kIx,
Maxim	Maxim	k1gMnSc1
Lvovič	Lvovič	k1gMnSc1
Koncevič	Koncevič	k1gMnSc1
<g/>
,	,	kIx,
Curtis	Curtis	k1gInSc1
T.	T.	kA
McMullen	McMullen	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Laurent	Laurent	k1gInSc1
Lafforgue	Lafforgue	k1gFnSc1
<g/>
,	,	kIx,
Vladimir	Vladimir	k1gMnSc1
Voevodskij	Voevodskij	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Andrej	Andrej	k1gMnSc1
Okounkov	Okounkov	k1gInSc1
<g/>
,	,	kIx,
Grigorij	Grigorij	k1gMnSc1
Perelman	Perelman	k1gMnSc1
<g/>
,	,	kIx,
Terence	Terence	k1gFnSc1
Tao	Tao	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Wendelin	Wendelin	k2eAgMnSc1d1
Werner	Werner	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Elon	Elon	k1gInSc1
Lindenstrauss	Lindenstrauss	k1gInSc1
<g/>
,	,	kIx,
Ngo	Ngo	k1gFnSc1
Bao	Bao	k1gFnSc2
Chau	Chaus	k1gInSc2
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
Smirnov	Smirnovo	k1gNnPc2
<g/>
,	,	kIx,
Cédric	Cédrice	k1gFnPc2
Villani	Villan	k1gMnPc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marjam	Marjam	k1gInSc1
Mírzácháníová	Mírzácháníová	k1gFnSc1
<g/>
,	,	kIx,
Artur	Artur	k1gMnSc1
Avila	Avila	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Hairer	Hairer	k1gMnSc1
<g/>
,	,	kIx,
Manjul	Manjul	k1gInSc1
Bhargava	Bhargava	k1gFnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Caucher	Cauchra	k1gFnPc2
Birkar	Birkar	k1gMnSc1
<g/>
,	,	kIx,
Alessio	Alessio	k6eAd1
Figalli	Figall	k1gMnPc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Scholze	Scholze	k1gFnSc2
<g/>
,	,	kIx,
Akshay	Akshaa	k1gFnSc2
Venkatesh	Venkatesha	k1gFnPc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1120321417	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2267	#num#	k4
5779	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2011116168	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
172545411	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2011116168	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Írán	Írán	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Matematika	matematika	k1gFnSc1
</s>
