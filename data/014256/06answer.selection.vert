<s>
Marjam	Marjam	k6eAd1
Mírzácháníová	Mírzácháníový	k2eAgFnSc1d1
(	(	kIx(
<g/>
nepřechýleně	přechýleně	k6eNd1
Mírzáchání	Mírzáchání	k1gNnSc1
<g/>
,	,	kIx,
psaná	psaný	k2eAgFnSc1d1
někdy	někdy	k6eAd1
jako	jako	k8xS,k8xC
Maryam	Maryam	k1gFnSc1
Mirzakhani	Mirzakhani	k1gFnSc1
<g/>
,	,	kIx,
persky	persky	k6eAd1
م	م	k?
م	م	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1977	#num#	k4
Teherán	Teherán	k1gInSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
íránská	íránský	k2eAgFnSc1d1
matematička	matematička	k1gFnSc1
a	a	k8xC
profesorka	profesorka	k1gFnSc1
matematiky	matematika	k1gFnSc2
na	na	k7c6
Stanfordově	Stanfordův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zabývala	zabývat	k5eAaImAgFnS
se	s	k7c7
především	především	k6eAd1
Teichmüllerovou	Teichmüllerův	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
<g/>
,	,	kIx,
hyperbolickou	hyperbolický	k2eAgFnSc7d1
geometrií	geometrie	k1gFnSc7
<g/>
,	,	kIx,
ergodickou	ergodický	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
a	a	k8xC
symplektickou	symplektický	k2eAgFnSc7d1
geometrií	geometrie	k1gFnSc7
<g/>
.	.	kIx.
</s>