<s>
Korepetitor	korepetitor	k1gMnSc1	korepetitor
také	také	k9	také
obvykle	obvykle	k6eAd1	obvykle
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
zkoušky	zkouška	k1gFnPc4	zkouška
sborových	sborový	k2eAgNnPc2d1	sborové
pěveckých	pěvecký	k2eAgNnPc2d1	pěvecké
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
nastudování	nastudování	k1gNnSc2	nastudování
operního	operní	k2eAgInSc2d1	operní
repertoáru	repertoár	k1gInSc2	repertoár
v	v	k7c6	v
divadlech	divadlo	k1gNnPc6	divadlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemusel	muset	k5eNaImAgInS	muset
být	být	k5eAaImF	být
angažován	angažován	k2eAgInSc1d1	angažován
nákladný	nákladný	k2eAgInSc1d1	nákladný
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
