<p>
<s>
Korepetitor	korepetitor	k1gMnSc1	korepetitor
je	být	k5eAaImIp3nS	být
hudebník	hudebník	k1gMnSc1	hudebník
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
úlohou	úloha	k1gFnSc7	úloha
je	být	k5eAaImIp3nS	být
doprovázet	doprovázet	k5eAaImF	doprovázet
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
hráče	hráč	k1gMnSc2	hráč
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
nebo	nebo	k8xC	nebo
zpěváky	zpěvák	k1gMnPc4	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Korepetitor	korepetitor	k1gMnSc1	korepetitor
také	také	k9	také
obvykle	obvykle	k6eAd1	obvykle
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
zkoušky	zkouška	k1gFnPc4	zkouška
sborových	sborový	k2eAgNnPc2d1	sborové
pěveckých	pěvecký	k2eAgNnPc2d1	pěvecké
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
nastudování	nastudování	k1gNnSc2	nastudování
operního	operní	k2eAgInSc2d1	operní
repertoáru	repertoár	k1gInSc2	repertoár
v	v	k7c6	v
divadlech	divadlo	k1gNnPc6	divadlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemusel	muset	k5eNaImAgInS	muset
být	být	k5eAaImF	být
angažován	angažován	k2eAgInSc1d1	angažován
nákladný	nákladný	k2eAgInSc1d1	nákladný
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Korepetitor	korepetitor	k1gMnSc1	korepetitor
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
využíván	využívat	k5eAaImNgInS	využívat
při	při	k7c6	při
nácviku	nácvik	k1gInSc6	nácvik
baletu	balet	k1gInSc2	balet
<g/>
,	,	kIx,	,
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
operety	opereta	k1gFnSc2	opereta
<g/>
,	,	kIx,	,
opery	opera	k1gFnSc2	opera
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
může	moct	k5eAaImIp3nS	moct
pružně	pružně	k6eAd1	pružně
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
požadavky	požadavek	k1gInPc4	požadavek
baletního	baletní	k2eAgNnSc2d1	baletní
<g/>
,	,	kIx,	,
tanečního	taneční	k2eAgNnSc2d1	taneční
či	či	k8xC	či
divadelního	divadelní	k2eAgMnSc2d1	divadelní
mistra	mistr	k1gMnSc2	mistr
a	a	k8xC	a
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
svou	svůj	k3xOyFgFnSc4	svůj
hru	hra	k1gFnSc4	hra
aktuálním	aktuální	k2eAgFnPc3d1	aktuální
potřebám	potřeba	k1gFnPc3	potřeba
nebo	nebo	k8xC	nebo
i	i	k9	i
jen	jen	k9	jen
může	moct	k5eAaImIp3nS	moct
vhodně	vhodně	k6eAd1	vhodně
doprovázet	doprovázet	k5eAaImF	doprovázet
třeba	třeba	k6eAd1	třeba
rozcvičky	rozcvička	k1gFnSc2	rozcvička
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
například	například	k6eAd1	například
při	při	k7c6	při
tréningu	tréning	k1gInSc6	tréning
tělocvičných	tělocvičný	k2eAgFnPc2d1	Tělocvičná
či	či	k8xC	či
gymnastických	gymnastický	k2eAgFnPc2d1	gymnastická
sestav	sestava	k1gFnPc2	sestava
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
sladění	sladění	k1gNnSc4	sladění
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Někteří	některý	k3yIgMnPc1	některý
korepetitoři	korepetitor	k1gMnPc1	korepetitor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
==	==	k?	==
</s>
</p>
<p>
<s>
Bohumír	Bohumír	k1gMnSc1	Bohumír
Váchal	Váchal	k1gMnSc1	Váchal
-	-	kIx~	-
korepetitor	korepetitor	k1gMnSc1	korepetitor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgMnSc1d1	osobní
korepetitor	korepetitor	k1gMnSc1	korepetitor
Evy	Eva	k1gFnSc2	Eva
Urbanové	Urbanová	k1gFnSc2	Urbanová
</s>
</p>
<p>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Charvát	Charvát	k1gMnSc1	Charvát
-	-	kIx~	-
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
korepetitor	korepetitor	k1gMnSc1	korepetitor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Klauda	Klauda	k1gMnSc1	Klauda
-	-	kIx~	-
korepetitor	korepetitor	k1gMnSc1	korepetitor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Zeman	Zeman	k1gMnSc1	Zeman
-	-	kIx~	-
korepetitor	korepetitor	k1gMnSc1	korepetitor
a	a	k8xC	a
klavírista	klavírista	k1gMnSc1	klavírista
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Iryna	Iryen	k2eAgFnSc1d1	Iryna
Roměnskaja	Roměnskaja	k1gFnSc1	Roměnskaja
-	-	kIx~	-
korepetitorka	korepetitorka	k1gFnSc1	korepetitorka
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Igor	Igor	k1gMnSc1	Igor
Rusinko	rusinka	k1gFnSc5	rusinka
-	-	kIx~	-
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
korepetitor	korepetitor	k1gMnSc1	korepetitor
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
