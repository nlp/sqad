<p>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
a	a	k8xC	a
demokratická	demokratický	k2eAgFnSc1d1	demokratická
unie	unie	k1gFnSc1	unie
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
(	(	kIx(	(
<g/>
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
<g/>
,	,	kIx,	,
též	též	k9	též
lidovci	lidovec	k1gMnPc1	lidovec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
křesťansko-demokratická	křesťanskoemokratický	k2eAgFnSc1d1	křesťansko-demokratická
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
sloučením	sloučení	k1gNnSc7	sloučení
několika	několik	k4yIc2	několik
katolických	katolický	k2eAgFnPc2d1	katolická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
nesla	nést	k5eAaImAgFnS	nést
jméno	jméno	k1gNnSc4	jméno
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
českých	český	k2eAgFnPc2d1	Česká
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
preference	preference	k1gFnSc1	preference
se	se	k3xPyFc4	se
v	v	k7c6	v
polistopadové	polistopadový	k2eAgFnSc6d1	polistopadová
historii	historie	k1gFnSc6	historie
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
pohybovaly	pohybovat	k5eAaImAgFnP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
%	%	kIx~	%
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
PSP	PSP	kA	PSP
ČR	ČR	kA	ČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
však	však	k9	však
strana	strana	k1gFnSc1	strana
těsně	těsně	k6eAd1	těsně
nepřekročila	překročit	k5eNaPmAgFnS	překročit
hranici	hranice	k1gFnSc4	hranice
5	[number]	k4	5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
nebyla	být	k5eNaImAgFnS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
až	až	k9	až
do	do	k7c2	do
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
jako	jako	k8xC	jako
první	první	k4xOgFnSc3	první
straně	strana	k1gFnSc3	strana
vypadlé	vypadlý	k2eAgFnSc2d1	vypadlá
ze	z	k7c2	z
sněmovny	sněmovna	k1gFnSc2	sněmovna
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
podařil	podařit	k5eAaPmAgInS	podařit
návrat	návrat	k1gInSc1	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
části	část	k1gFnSc2	část
nespokojených	spokojený	k2eNgMnPc2d1	nespokojený
členů	člen	k1gMnPc2	člen
do	do	k7c2	do
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
prošla	projít	k5eAaPmAgFnS	projít
strana	strana	k1gFnSc1	strana
generační	generační	k2eAgFnSc1d1	generační
obměnou	obměna	k1gFnSc7	obměna
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
posiluje	posilovat	k5eAaImIp3nS	posilovat
své	svůj	k3xOyFgNnSc4	svůj
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
stranou	stranou	k6eAd1	stranou
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
získaných	získaný	k2eAgInPc2d1	získaný
mandátů	mandát	k1gInPc2	mandát
v	v	k7c6	v
komunální	komunální	k2eAgFnSc6d1	komunální
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
disponuje	disponovat	k5eAaBmIp3nS	disponovat
poměrně	poměrně	k6eAd1	poměrně
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
širokou	široký	k2eAgFnSc7d1	široká
členskou	členský	k2eAgFnSc7d1	členská
základnou	základna	k1gFnSc7	základna
<g/>
,	,	kIx,	,
soustředěnou	soustředěný	k2eAgFnSc7d1	soustředěná
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
pozice	pozice	k1gFnSc1	pozice
obecně	obecně	k6eAd1	obecně
silnější	silný	k2eAgFnSc1d2	silnější
než	než	k8xS	než
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
politické	politický	k2eAgFnSc2d1	politická
scény	scéna	k1gFnSc2	scéna
i	i	k8xC	i
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
názorová	názorový	k2eAgFnSc1d1	názorová
pluralita	pluralita	k1gFnSc1	pluralita
jí	on	k3xPp3gFnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
široký	široký	k2eAgInSc4d1	široký
koaliční	koaliční	k2eAgInSc4d1	koaliční
potenciál	potenciál	k1gInSc4	potenciál
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
téměř	téměř	k6eAd1	téměř
nepřetržitou	přetržitý	k2eNgFnSc7d1	nepřetržitá
účastí	účast	k1gFnSc7	účast
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
a	a	k8xC	a
schopností	schopnost	k1gFnSc7	schopnost
prosadit	prosadit	k5eAaPmF	prosadit
vlastní	vlastní	k2eAgInSc4d1	vlastní
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
trvalým	trvalý	k2eAgNnSc7d1	trvalé
vnitrostranickým	vnitrostranický	k2eAgNnSc7d1	vnitrostranické
tříbením	tříbení	k1gNnSc7	tříbení
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
pravdomluvnost	pravdomluvnost	k1gFnSc1	pravdomluvnost
<g/>
,	,	kIx,	,
nedotknutelnost	nedotknutelnost	k1gFnSc1	nedotknutelnost
soukromého	soukromý	k2eAgInSc2d1	soukromý
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
pomoc	pomoc	k1gFnSc4	pomoc
potřebným	potřebný	k2eAgFnPc3d1	potřebná
<g/>
)	)	kIx)	)
a	a	k8xC	a
principu	princip	k1gInSc2	princip
subsidiarity	subsidiarita	k1gFnSc2	subsidiarita
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
témata	téma	k1gNnPc1	téma
jsou	být	k5eAaImIp3nP	být
podpora	podpora	k1gFnSc1	podpora
tradiční	tradiční	k2eAgFnSc2d1	tradiční
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
drobných	drobný	k2eAgInPc2d1	drobný
(	(	kIx(	(
<g/>
rodinných	rodinný	k2eAgInPc2d1	rodinný
<g/>
)	)	kIx)	)
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
uspokojivé	uspokojivý	k2eAgFnPc4d1	uspokojivá
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Existence	existence	k1gFnSc1	existence
strany	strana	k1gFnSc2	strana
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
dvakrát	dvakrát	k6eAd1	dvakrát
přerušena	přerušit	k5eAaPmNgFnS	přerušit
<g/>
:	:	kIx,	:
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
letech	let	k1gInPc6	let
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vůbec	vůbec	k9	vůbec
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
letech	let	k1gInPc6	let
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
existovala	existovat	k5eAaImAgFnS	existovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
obrozené	obrozený	k2eAgFnSc2d1	obrozená
<g/>
"	"	kIx"	"
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
začleněné	začleněný	k2eAgFnSc2d1	začleněná
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Obrozená	obrozený	k2eAgFnSc1d1	obrozená
strana	strana	k1gFnSc1	strana
<g/>
"	"	kIx"	"
trpěla	trpět	k5eAaImAgFnS	trpět
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
dichotomií	dichotomie	k1gFnSc7	dichotomie
<g/>
:	:	kIx,	:
měla	mít	k5eAaImAgFnS	mít
komunisty	komunista	k1gMnPc7	komunista
vnucené	vnucený	k2eAgNnSc4d1	vnucené
prokomunistické	prokomunistický	k2eAgNnSc4d1	prokomunistické
vedení	vedení	k1gNnSc4	vedení
a	a	k8xC	a
poslance	poslanec	k1gMnPc4	poslanec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
plně	plně	k6eAd1	plně
podporovali	podporovat	k5eAaImAgMnP	podporovat
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
a	a	k8xC	a
antikomunistickou	antikomunistický	k2eAgFnSc4d1	antikomunistická
členskou	členský	k2eAgFnSc4d1	členská
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
kterážto	kterážto	k?	kterážto
opakovaně	opakovaně	k6eAd1	opakovaně
využívala	využívat	k5eAaImAgFnS	využívat
stranický	stranický	k2eAgInSc4d1	stranický
aparát	aparát	k1gInSc4	aparát
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
akcí	akce	k1gFnPc2	akce
směřovaných	směřovaný	k2eAgInPc2d1	směřovaný
proti	proti	k7c3	proti
cílům	cíl	k1gInPc3	cíl
komunistické	komunistický	k2eAgFnSc2d1	komunistická
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
prokomunističtí	prokomunistický	k2eAgMnPc1d1	prokomunistický
funkcionáři	funkcionář	k1gMnPc1	funkcionář
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
marně	marně	k6eAd1	marně
<g/>
)	)	kIx)	)
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
několik	několik	k4yIc4	několik
ministran	ministrana	k1gFnPc2	ministrana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
hlásily	hlásit	k5eAaImAgFnP	hlásit
k	k	k7c3	k
dědictví	dědictví	k1gNnSc3	dědictví
ČSL	ČSL	kA	ČSL
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
řevnivost	řevnivost	k1gFnSc1	řevnivost
je	on	k3xPp3gInPc4	on
odsuzovala	odsuzovat	k5eAaImAgFnS	odsuzovat
k	k	k7c3	k
minimálnímu	minimální	k2eAgInSc3d1	minimální
významu	význam	k1gInSc3	význam
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnPc4	charakteristikon
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
KDU-ČSL	KDU-ČSL	k?	KDU-ČSL
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
ideologii	ideologie	k1gFnSc3	ideologie
sociálně	sociálně	k6eAd1	sociálně
tržního	tržní	k2eAgNnSc2d1	tržní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
skupina	skupina	k1gFnSc1	skupina
voličů	volič	k1gMnPc2	volič
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
rekrutovala	rekrutovat	k5eAaImAgFnS	rekrutovat
mezi	mezi	k7c7	mezi
katolickým	katolický	k2eAgNnSc7d1	katolické
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
venkova	venkov	k1gInSc2	venkov
a	a	k8xC	a
malých	malý	k2eAgNnPc2d1	malé
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
posunu	posun	k1gInSc3	posun
v	v	k7c6	v
orientaci	orientace	k1gFnSc6	orientace
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
začali	začít	k5eAaPmAgMnP	začít
volit	volit	k5eAaImF	volit
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
nekonfesně	konfesně	k6eNd1	konfesně
a	a	k8xC	a
nenábožensky	nábožensky	k6eNd1	nábožensky
orientovaní	orientovaný	k2eAgMnPc1d1	orientovaný
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
podporu	podpora	k1gFnSc4	podpora
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
politiky	politika	k1gFnSc2	politika
středního	střední	k2eAgInSc2d1	střední
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
baštu	bašta	k1gFnSc4	bašta
lze	lze	k6eAd1	lze
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
považovat	považovat	k5eAaImF	považovat
střední	střední	k2eAgFnSc4d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
vyloženě	vyloženě	k6eAd1	vyloženě
marginální	marginální	k2eAgInPc1d1	marginální
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gNnPc4	její
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
voličů	volič	k1gMnPc2	volič
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
disponuje	disponovat	k5eAaBmIp3nS	disponovat
na	na	k7c4	na
polistopadové	polistopadový	k2eAgInPc4d1	polistopadový
poměry	poměr	k1gInPc4	poměr
početně	početně	k6eAd1	početně
poměrně	poměrně	k6eAd1	poměrně
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
členskou	členský	k2eAgFnSc7d1	členská
základnou	základna	k1gFnSc7	základna
(	(	kIx(	(
<g/>
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
2005	[number]	k4	2005
čítala	čítat	k5eAaImAgFnS	čítat
42	[number]	k4	42
843	[number]	k4	843
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
pouhým	pouhý	k2eAgInSc7d1	pouhý
zlomkem	zlomek	k1gInSc7	zlomek
v	v	k7c6	v
porovnáním	porovnání	k1gNnSc7	porovnání
s	s	k7c7	s
časy	čas	k1gInPc7	čas
největší	veliký	k2eAgFnSc2d3	veliký
slávy	sláva	k1gFnSc2	sláva
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
měla	mít	k5eAaImAgFnS	mít
přes	přes	k7c4	přes
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
členů	člen	k1gInPc2	člen
a	a	k8xC	a
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
opět	opět	k6eAd1	opět
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
její	její	k3xOp3gInSc4	její
příznivci	příznivec	k1gMnPc1	příznivec
vyloučení	vyloučený	k2eAgMnPc1d1	vyloučený
či	či	k8xC	či
vystoupivší	vystoupivší	k2eAgMnPc1d1	vystoupivší
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
téměř	téměř	k6eAd1	téměř
97	[number]	k4	97
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
však	však	k9	však
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
strany	strana	k1gFnSc2	strana
stále	stále	k6eAd1	stále
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
především	především	k9	především
vymíráním	vymírání	k1gNnSc7	vymírání
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ne	ne	k9	ne
všeobecně	všeobecně	k6eAd1	všeobecně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
někteří	některý	k3yIgMnPc1	některý
mladší	mladý	k2eAgMnPc1d2	mladší
lidovečtí	lidovecký	k2eAgMnPc1d1	lidovecký
politikové	politik	k1gMnPc1	politik
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
velikost	velikost	k1gFnSc1	velikost
členské	členský	k2eAgFnSc2d1	členská
základny	základna	k1gFnSc2	základna
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
optický	optický	k2eAgInSc4d1	optický
klam	klam	k1gInSc4	klam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
přestárlá	přestárlý	k2eAgFnSc1d1	přestárlá
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
apolitického	apolitický	k2eAgInSc2d1	apolitický
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
členstva	členstvo	k1gNnSc2	členstvo
neúčastní	účastnit	k5eNaImIp3nS	účastnit
a	a	k8xC	a
nerozumí	rozumět	k5eNaImIp3nS	rozumět
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
se	se	k3xPyFc4	se
orientuje	orientovat	k5eAaBmIp3nS	orientovat
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
spolkový	spolkový	k2eAgInSc4d1	spolkový
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ale	ale	k8xC	ale
určitou	určitý	k2eAgFnSc7d1	určitá
věrností	věrnost	k1gFnSc7	věrnost
starým	starý	k1gMnSc7	starý
"	"	kIx"	"
<g/>
vyzkoušeným	vyzkoušený	k2eAgMnPc3d1	vyzkoušený
<g/>
"	"	kIx"	"
politikům	politik	k1gMnPc3	politik
a	a	k8xC	a
vůdcům	vůdce	k1gMnPc3	vůdce
konzervuje	konzervovat	k5eAaBmIp3nS	konzervovat
současný	současný	k2eAgInSc1d1	současný
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
je	být	k5eAaImIp3nS	být
však	však	k9	však
všeobecně	všeobecně	k6eAd1	všeobecně
vnímána	vnímán	k2eAgFnSc1d1	vnímána
potřeba	potřeba	k1gFnSc1	potřeba
mladých	mladý	k2eAgMnPc2d1	mladý
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nP	by
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
vstupovali	vstupovat	k5eAaImAgMnP	vstupovat
a	a	k8xC	a
podíleli	podílet	k5eAaImAgMnP	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
omlazení	omlazení	k1gNnSc3	omlazení
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
i	i	k9	i
k	k	k7c3	k
přijímaní	přijímaný	k2eAgMnPc1d1	přijímaný
moderních	moderní	k2eAgFnPc2d1	moderní
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Strana	strana	k1gFnSc1	strana
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
priority	priorita	k1gFnPc4	priorita
považuje	považovat	k5eAaImIp3nS	považovat
integraci	integrace	k1gFnSc4	integrace
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
drobných	drobný	k2eAgMnPc2d1	drobný
živnostníků	živnostník	k1gMnPc2	živnostník
<g/>
,	,	kIx,	,
prosazování	prosazování	k1gNnSc4	prosazování
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
dříve	dříve	k6eAd2	dříve
mělo	mít	k5eAaImAgNnS	mít
relativně	relativně	k6eAd1	relativně
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
moravské	moravský	k2eAgFnSc2d1	Moravská
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Karasem	Karas	k1gMnSc7	Karas
opakovaně	opakovaně	k6eAd1	opakovaně
předkládalo	předkládat	k5eAaImAgNnS	předkládat
zákony	zákon	k1gInPc4	zákon
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
proti	proti	k7c3	proti
interrupcím	interrupce	k1gFnPc3	interrupce
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
zákony	zákon	k1gInPc4	zákon
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
poslanci	poslanec	k1gMnPc7	poslanec
ostatních	ostatní	k2eAgFnPc2d1	ostatní
stran	strana	k1gFnPc2	strana
minimální	minimální	k2eAgFnPc4d1	minimální
<g/>
,	,	kIx,	,
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
iritovaly	iritovat	k5eAaImAgFnP	iritovat
i	i	k8xC	i
některé	některý	k3yIgMnPc4	některý
lidovce	lidovec	k1gMnPc4	lidovec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobné	podobný	k2eAgFnPc1d1	podobná
zbytečné	zbytečný	k2eAgFnPc1d1	zbytečná
a	a	k8xC	a
předem	předem	k6eAd1	předem
neúspěšné	úspěšný	k2eNgFnPc1d1	neúspěšná
snahy	snaha	k1gFnPc1	snaha
omezují	omezovat	k5eAaImIp3nP	omezovat
voličský	voličský	k2eAgInSc4d1	voličský
potenciál	potenciál	k1gInSc4	potenciál
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
postojům	postoj	k1gInPc3	postoj
jiných	jiný	k1gMnPc2	jiný
evropských	evropský	k2eAgFnPc2d1	Evropská
křesťansko-demokratických	křesťanskoemokratický	k2eAgFnPc2d1	křesťansko-demokratická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Strana	strana	k1gFnSc1	strana
také	také	k9	také
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
proti	proti	k7c3	proti
dekriminalizaci	dekriminalizace	k1gFnSc3	dekriminalizace
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
legalizaci	legalizace	k1gFnSc4	legalizace
eutanazie	eutanazie	k1gFnSc2	eutanazie
a	a	k8xC	a
registrovanému	registrovaný	k2eAgNnSc3d1	registrované
partnerství	partnerství	k1gNnSc3	partnerství
homosexuálů	homosexuál	k1gMnPc2	homosexuál
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
křídle	křídlo	k1gNnSc6	křídlo
lidovců	lidovec	k1gMnPc2	lidovec
(	(	kIx(	(
<g/>
konzervativnější	konzervativní	k2eAgMnSc1d2	konzervativnější
<g/>
,	,	kIx,	,
s	s	k7c7	s
těžištěm	těžiště	k1gNnSc7	těžiště
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
slabším	slabý	k2eAgNnSc6d2	slabší
křídle	křídlo	k1gNnSc6	křídlo
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
demokratů	demokrat	k1gMnPc2	demokrat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
modernější	moderní	k2eAgMnSc1d2	modernější
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
soustředěném	soustředěný	k2eAgInSc6d1	soustředěný
spíše	spíše	k9	spíše
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dvě	dva	k4xCgFnPc4	dva
mládežnické	mládežnický	k2eAgFnPc4d1	mládežnická
organizace	organizace	k1gFnPc4	organizace
Mladé	mladá	k1gFnSc2	mladá
křesťanské	křesťanský	k2eAgMnPc4d1	křesťanský
demokraty	demokrat	k1gMnPc4	demokrat
a	a	k8xC	a
Mladé	mladý	k2eAgMnPc4d1	mladý
lidovce	lidovec	k1gMnPc4	lidovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1919	[number]	k4	1919
sloučením	sloučení	k1gNnSc7	sloučení
vícero	vícero	k1gNnSc1	vícero
katolických	katolický	k2eAgFnPc2d1	katolická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
zásluhy	zásluha	k1gFnPc1	zásluha
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
strany	strana	k1gFnSc2	strana
měli	mít	k5eAaImAgMnP	mít
msgre	msgr	k1gInSc5	msgr
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
katolickou	katolický	k2eAgFnSc4d1	katolická
politickou	politický	k2eAgFnSc4d1	politická
scénu	scéna	k1gFnSc4	scéna
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
a	a	k8xC	a
msgre	msgr	k1gMnSc5	msgr
<g/>
.	.	kIx.	.
</s>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Stašek	Stašek	k1gMnSc1	Stašek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
a	a	k8xC	a
sjednotitelem	sjednotitel	k1gMnSc7	sjednotitel
katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
<g/>
Původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
strana	strana	k1gFnSc1	strana
charakter	charakter	k1gInSc1	charakter
volného	volný	k2eAgNnSc2d1	volné
sdružení	sdružení	k1gNnSc2	sdružení
dvou	dva	k4xCgNnPc2	dva
center	centrum	k1gNnPc2	centrum
<g/>
:	:	kIx,	:
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
moravského	moravský	k2eAgInSc2d1	moravský
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každé	každý	k3xTgNnSc4	každý
mělo	mít	k5eAaImAgNnS	mít
samostatné	samostatný	k2eAgNnSc1d1	samostatné
vedení	vedení	k1gNnSc1	vedení
a	a	k8xC	a
předsedu	předseda	k1gMnSc4	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgInSc1d1	centrální
spojovací	spojovací	k2eAgInSc1d1	spojovací
orgán	orgán	k1gInSc1	orgán
původně	původně	k6eAd1	původně
neexistoval	existovat	k5eNaImAgInS	existovat
<g/>
,	,	kIx,	,
společný	společný	k2eAgInSc1d1	společný
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
(	(	kIx(	(
<g/>
ČSL	ČSL	kA	ČSL
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
výrazně	výrazně	k6eAd1	výrazně
silnější	silný	k2eAgFnSc1d2	silnější
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
zastřešující	zastřešující	k2eAgFnSc4d1	zastřešující
celou	celý	k2eAgFnSc4d1	celá
stranu	strana	k1gFnSc4	strana
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
až	až	k9	až
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgFnPc4d1	malá
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
ukotvení	ukotvení	k1gNnSc4	ukotvení
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
a	a	k8xC	a
oponování	oponování	k1gNnSc2	oponování
protikatolickému	protikatolický	k2eAgInSc3d1	protikatolický
tlaku	tlak	k1gInSc3	tlak
socialistů	socialist	k1gMnPc2	socialist
a	a	k8xC	a
liberálů	liberál	k1gMnPc2	liberál
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
oddalovat	oddalovat	k5eAaImF	oddalovat
projednávání	projednávání	k1gNnSc4	projednávání
většiny	většina	k1gFnSc2	většina
rozhodujících	rozhodující	k2eAgInPc2d1	rozhodující
zákonů	zákon	k1gInPc2	zákon
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
až	až	k9	až
do	do	k7c2	do
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hnutí	hnutí	k1gNnSc1	hnutí
"	"	kIx"	"
<g/>
Pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
<g/>
"	"	kIx"	"
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
a	a	k8xC	a
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nepostradatelnou	postradatelný	k2eNgFnSc7d1	nepostradatelná
součástí	součást	k1gFnSc7	součást
vládních	vládní	k2eAgFnPc2d1	vládní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nakonec	nakonec	k6eAd1	nakonec
přispělo	přispět	k5eAaPmAgNnS	přispět
ke	k	k7c3	k
krachu	krach	k1gInSc3	krach
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
odluky	odluka	k1gFnSc2	odluka
církve	církev	k1gFnSc2	církev
od	od	k7c2	od
státu	stát	k1gInSc2	stát
a	a	k8xC	a
k	k	k7c3	k
dohodnutí	dohodnutí	k1gNnSc3	dohodnutí
oboustranně	oboustranně	k6eAd1	oboustranně
přijatelných	přijatelný	k2eAgInPc2d1	přijatelný
kompromisů	kompromis	k1gInPc2	kompromis
mezi	mezi	k7c7	mezi
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
státem	stát	k1gInSc7	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
počátků	počátek	k1gInPc2	počátek
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
určitým	určitý	k2eAgNnSc7d1	určité
napětím	napětí	k1gNnSc7	napětí
mezi	mezi	k7c7	mezi
slabším	slabý	k2eAgNnSc7d2	slabší
českým	český	k2eAgNnSc7d1	české
a	a	k8xC	a
silnějším	silný	k2eAgNnSc7d2	silnější
moravským	moravský	k2eAgNnSc7d1	Moravské
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
postupně	postupně	k6eAd1	postupně
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
z	z	k7c2	z
Československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
středovou	středový	k2eAgFnSc4d1	středová
klerikální	klerikální	k2eAgFnSc4d1	klerikální
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
nepostradatelnou	postradatelný	k2eNgFnSc4d1	nepostradatelná
při	při	k7c6	při
sestavování	sestavování	k1gNnSc6	sestavování
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Při	pře	k1gFnSc4	pře
stabilní	stabilní	k2eAgFnSc4d1	stabilní
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
%	%	kIx~	%
podpoře	podpora	k1gFnSc3	podpora
voličů	volič	k1gInPc2	volič
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
vlivnou	vlivný	k2eAgFnSc7d1	vlivná
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
neobešla	obešnout	k5eNaPmAgFnS	obešnout
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
vlád	vláda	k1gFnPc2	vláda
let	léto	k1gNnPc2	léto
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
trnem	trn	k1gInSc7	trn
v	v	k7c4	v
oku	oka	k1gFnSc4	oka
prezidentu	prezident	k1gMnSc3	prezident
Masarykovi	Masaryk	k1gMnSc3	Masaryk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
speciálně	speciálně	k6eAd1	speciálně
jejího	její	k3xOp3gMnSc4	její
předsedu	předseda	k1gMnSc4	předseda
osobně	osobně	k6eAd1	osobně
neměl	mít	k5eNaImAgMnS	mít
rád	rád	k2eAgMnSc1d1	rád
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
souviselo	souviset	k5eAaImAgNnS	souviset
s	s	k7c7	s
Masarykovým	Masarykův	k2eAgNnSc7d1	Masarykovo
tíhnutím	tíhnutí	k1gNnSc7	tíhnutí
k	k	k7c3	k
husitství	husitství	k1gNnSc3	husitství
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
náhledem	náhled	k1gInSc7	náhled
na	na	k7c4	na
římskokatolickou	římskokatolický	k2eAgFnSc4d1	Římskokatolická
církev	církev	k1gFnSc4	církev
jako	jako	k8xS	jako
na	na	k7c4	na
tmářskou	tmářský	k2eAgFnSc4d1	tmářská
organizaci	organizace	k1gFnSc4	organizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jejímu	její	k3xOp3gInSc3	její
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
vlivu	vliv	k1gInSc3	vliv
zabránit	zabránit	k5eAaPmF	zabránit
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
prezidenta	prezident	k1gMnSc2	prezident
Beneše	Beneš	k1gMnSc2	Beneš
se	se	k3xPyFc4	se
ČSL	ČSL	kA	ČSL
stala	stát	k5eAaPmAgFnS	stát
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
uskupení	uskupení	k1gNnSc2	uskupení
okolo	okolo	k7c2	okolo
Hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
strana	strana	k1gFnSc1	strana
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
svoji	svůj	k3xOyFgFnSc4	svůj
politiku	politika	k1gFnSc4	politika
na	na	k7c4	na
katolíky	katolík	k1gMnPc4	katolík
a	a	k8xC	a
otázky	otázka	k1gFnPc4	otázka
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
(	(	kIx(	(
<g/>
o	o	k7c4	o
hlasy	hlas	k1gInPc4	hlas
katolických	katolický	k2eAgMnPc2d1	katolický
rolníků	rolník	k1gMnPc2	rolník
a	a	k8xC	a
přízeň	přízeň	k1gFnSc4	přízeň
katolických	katolický	k2eAgMnPc2d1	katolický
hierarchů	hierarcha	k1gMnPc2	hierarcha
přitom	přitom	k6eAd1	přitom
vedla	vést	k5eAaImAgFnS	vést
permanentní	permanentní	k2eAgFnSc1d1	permanentní
a	a	k8xC	a
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
boj	boj	k1gInSc1	boj
s	s	k7c7	s
agrárníky	agrárník	k1gMnPc7	agrárník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
zahrnout	zahrnout	k5eAaPmF	zahrnout
všechny	všechen	k3xTgFnPc4	všechen
vrstvy	vrstva	k1gFnPc4	vrstva
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jí	jíst	k5eAaImIp3nS	jíst
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
komplikovalo	komplikovat	k5eAaBmAgNnS	komplikovat
život	život	k1gInSc4	život
v	v	k7c6	v
časech	čas	k1gInPc6	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
řešily	řešit	k5eAaImAgFnP	řešit
otázky	otázka	k1gFnPc1	otázka
významné	významný	k2eAgFnPc1d1	významná
pro	pro	k7c4	pro
některou	některý	k3yIgFnSc4	některý
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
drasticky	drasticky	k6eAd1	drasticky
pocítila	pocítit	k5eAaPmAgFnS	pocítit
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
postavily	postavit	k5eAaPmAgInP	postavit
zájmy	zájem	k1gInPc1	zájem
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
katolických	katolický	k2eAgMnPc2d1	katolický
rolníků	rolník	k1gMnPc2	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
širokému	široký	k2eAgInSc3d1	široký
koaličnímu	koaliční	k2eAgInSc3d1	koaliční
potenciálu	potenciál	k1gInSc3	potenciál
a	a	k8xC	a
nepostradatelnosti	nepostradatelnost	k1gFnSc3	nepostradatelnost
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
při	pře	k1gFnSc4	pře
prosazování	prosazování	k1gNnSc2	prosazování
svého	svůj	k3xOyFgInSc2	svůj
programu	program	k1gInSc2	program
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zdaleka	zdaleka	k6eAd1	zdaleka
neodpovídaly	odpovídat	k5eNaImAgInP	odpovídat
jejím	její	k3xOp3gInPc3	její
volebním	volební	k2eAgInPc3d1	volební
výsledkům	výsledek	k1gInPc3	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
spojila	spojit	k5eAaPmAgFnS	spojit
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
moravského	moravský	k2eAgNnSc2d1	Moravské
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Šrámka	Šrámek	k1gMnSc2	Šrámek
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
pravicovými	pravicový	k2eAgInPc7d1	pravicový
subjekty	subjekt	k1gInPc7	subjekt
ve	v	k7c4	v
Stranu	strana	k1gFnSc4	strana
národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
<g/>
:	:	kIx,	:
Šrámek	Šrámek	k1gMnSc1	Šrámek
sice	sice	k8xC	sice
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
Ústředním	ústřední	k2eAgInSc6d1	ústřední
výboru	výbor	k1gInSc6	výbor
ČSL	ČSL	kA	ČSL
a	a	k8xC	a
poslaneckých	poslanecký	k2eAgInPc6d1	poslanecký
i	i	k8xC	i
senátorských	senátorský	k2eAgInPc6d1	senátorský
klubech	klub	k1gInPc6	klub
odmítnutí	odmítnutí	k1gNnSc2	odmítnutí
projektu	projekt	k1gInSc2	projekt
Strany	strana	k1gFnSc2	strana
národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Stašek	Stašek	k1gMnSc1	Stašek
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
na	na	k7c6	na
účasti	účast	k1gFnSc6	účast
svého	svůj	k3xOyFgNnSc2	svůj
českého	český	k2eAgNnSc2d1	české
křídla	křídlo	k1gNnSc2	křídlo
ve	v	k7c6	v
SNJ	SNJ	kA	SNJ
a	a	k8xC	a
Šrámek	šrámek	k1gInSc4	šrámek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgInS	zabránit
rozštěpení	rozštěpení	k1gNnSc4	rozštěpení
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
musel	muset	k5eAaImAgMnS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
strana	strana	k1gFnSc1	strana
osobou	osoba	k1gFnSc7	osoba
Jana	Jan	k1gMnSc2	Jan
Šrámka	Šrámek	k1gMnSc2	Šrámek
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
v	v	k7c6	v
Londýnské	londýnský	k2eAgFnSc6d1	londýnská
exilové	exilový	k2eAgFnSc6d1	exilová
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
ČSL	ČSL	kA	ČSL
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
obnovených	obnovený	k2eAgFnPc2d1	obnovená
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byla	být	k5eAaImAgNnP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
jedinou	jediný	k2eAgFnSc7d1	jediná
českou	český	k2eAgFnSc7d1	Česká
nesocialistickou	socialistický	k2eNgFnSc7d1	nesocialistická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
její	její	k3xOp3gFnSc4	její
vedení	vedení	k1gNnSc1	vedení
doufalo	doufat	k5eAaImAgNnS	doufat
ve	v	k7c6	v
vítězství	vítězství	k1gNnSc6	vítězství
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
měla	mít	k5eAaImAgFnS	mít
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnSc2	jeho
mínění	mínění	k1gNnSc2	mínění
převzít	převzít	k5eAaPmF	převzít
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc4	všechen
voliče	volič	k1gInPc4	volič
agrárníků	agrárník	k1gMnPc2	agrárník
(	(	kIx(	(
<g/>
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
politiku	politika	k1gFnSc4	politika
<g/>
)	)	kIx)	)
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
volby	volba	k1gFnPc1	volba
skončily	skončit	k5eAaPmAgFnP	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
ČSL	ČSL	kA	ČSL
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
pouze	pouze	k6eAd1	pouze
20	[number]	k4	20
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
–	–	k?	–
proti	proti	k7c3	proti
40	[number]	k4	40
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
ČSL	ČSL	kA	ČSL
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členkou	členka	k1gFnSc7	členka
vlády	vláda	k1gFnSc2	vláda
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
stranami	strana	k1gFnPc7	strana
a	a	k8xC	a
tlumit	tlumit	k5eAaImF	tlumit
ty	ten	k3xDgInPc4	ten
nejradikálnější	radikální	k2eAgInPc4d3	nejradikálnější
výstřelky	výstřelek	k1gInPc4	výstřelek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
společně	společně	k6eAd1	společně
s	s	k7c7	s
nutností	nutnost	k1gFnSc7	nutnost
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
KSČ	KSČ	kA	KSČ
však	však	k9	však
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
silné	silný	k2eAgNnSc4d1	silné
pnutí	pnutí	k1gNnSc4	pnutí
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
starým	starý	k2eAgNnSc7d1	staré
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
"	"	kIx"	"
Šrámka	Šrámek	k1gMnSc4	Šrámek
a	a	k8xC	a
"	"	kIx"	"
<g/>
mladým	mladý	k2eAgNnSc7d1	mladé
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
a	a	k8xC	a
praktikovalo	praktikovat	k5eAaImAgNnS	praktikovat
ostře	ostro	k6eAd1	ostro
protikomunistický	protikomunistický	k2eAgInSc4d1	protikomunistický
kurs	kurs	k1gInSc4	kurs
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
nejradikálnějšími	radikální	k2eAgMnPc7d3	nejradikálnější
exponenty	exponent	k1gMnPc7	exponent
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vůdci	vůdce	k1gMnPc1	vůdce
byli	být	k5eAaImAgMnP	být
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Adolf	Adolf	k1gMnSc1	Adolf
Procházka	Procházka	k1gMnSc1	Procházka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Helena	Helena	k1gFnSc1	Helena
Koželuhová	Koželuhová	k1gFnSc1	Koželuhová
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Tigrid	Tigrid	k1gMnSc1	Tigrid
a	a	k8xC	a
Felix	Felix	k1gMnSc1	Felix
Uhl	Uhl	k1gMnSc1	Uhl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdrcující	zdrcující	k2eAgInSc1d1	zdrcující
výsledek	výsledek	k1gInSc1	výsledek
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
oslabil	oslabit	k5eAaPmAgInS	oslabit
pozici	pozice	k1gFnSc4	pozice
demokratických	demokratický	k2eAgFnPc2d1	demokratická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
lidovců	lidovec	k1gMnPc2	lidovec
zejména	zejména	k9	zejména
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
šla	jít	k5eAaImAgFnS	jít
ČSL	ČSL	kA	ČSL
od	od	k7c2	od
porážky	porážka	k1gFnSc2	porážka
k	k	k7c3	k
porážce	porážka	k1gFnSc3	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Neuspěla	uspět	k5eNaPmAgFnS	uspět
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
pozemkové	pozemkový	k2eAgFnSc3d1	pozemková
reformě	reforma	k1gFnSc3	reforma
<g/>
,	,	kIx,	,
milionářské	milionářský	k2eAgFnSc3d1	milionářská
dani	daň	k1gFnSc3	daň
<g/>
,	,	kIx,	,
jednotné	jednotný	k2eAgFnSc3d1	jednotná
škole	škola	k1gFnSc3	škola
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
zabránit	zabránit	k5eAaPmF	zabránit
ani	ani	k8xC	ani
zrušení	zrušení	k1gNnSc4	zrušení
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
odborových	odborový	k2eAgFnPc2d1	odborová
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
pohlceny	pohlcen	k2eAgInPc4d1	pohlcen
komunistickými	komunistický	k2eAgInPc7d1	komunistický
odbory	odbor	k1gInPc7	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Pozici	pozice	k1gFnSc4	pozice
strany	strana	k1gFnSc2	strana
navíc	navíc	k6eAd1	navíc
oslabila	oslabit	k5eAaPmAgFnS	oslabit
gradace	gradace	k1gFnSc1	gradace
konfliktu	konflikt	k1gInSc2	konflikt
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
problematické	problematický	k2eAgNnSc4d1	problematické
vyloučení	vyloučení	k1gNnSc4	vyloučení
Heleny	Helena	k1gFnSc2	Helena
Koželuhové	koželuh	k1gMnPc1	koželuh
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k9	již
komunisté	komunista	k1gMnPc1	komunista
protlačili	protlačit	k5eAaPmAgMnP	protlačit
svoje	svůj	k3xOyFgMnPc4	svůj
konfidenty	konfident	k1gMnPc4	konfident
na	na	k7c6	na
mnohé	mnohé	k1gNnSc4	mnohé
posty	posta	k1gFnSc2	posta
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
činnosti	činnost	k1gFnSc6	činnost
a	a	k8xC	a
plánech	plán	k1gInPc6	plán
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
i	i	k9	i
zajištěné	zajištěný	k2eAgInPc4d1	zajištěný
hlasy	hlas	k1gInPc4	hlas
některých	některý	k3yIgMnPc2	některý
jejích	její	k3xOp3gMnPc2	její
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
především	především	k9	především
Josefa	Josef	k1gMnSc4	Josef
Plojhara	Plojhar	k1gMnSc4	Plojhar
a	a	k8xC	a
Aloise	Alois	k1gMnSc4	Alois
Petra	Petr	k1gMnSc4	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
těžce	těžce	k6eAd1	těžce
svazoval	svazovat	k5eAaImAgInS	svazovat
též	též	k9	též
Šrámkův	Šrámkův	k2eAgInSc1d1	Šrámkův
špatný	špatný	k2eAgInSc1d1	špatný
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
jednáních	jednání	k1gNnPc6	jednání
zastupován	zastupován	k2eAgInSc4d1	zastupován
tajemníkem	tajemník	k1gMnSc7	tajemník
Hálou	Hála	k1gMnSc7	Hála
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
vybrán	vybrán	k2eAgInSc1d1	vybrán
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
absolutní	absolutní	k2eAgFnSc4d1	absolutní
věrnost	věrnost	k1gFnSc4	věrnost
Šrámkovi	Šrámek	k1gMnSc3	Šrámek
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnSc3	zkušenost
s	s	k7c7	s
vnitrostranickou	vnitrostranický	k2eAgFnSc7d1	vnitrostranická
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
schopnosti	schopnost	k1gFnPc4	schopnost
v	v	k7c6	v
mezistranické	mezistranický	k2eAgFnSc6d1	mezistranická
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
bohužel	bohužel	k9	bohužel
velice	velice	k6eAd1	velice
zřetelně	zřetelně	k6eAd1	zřetelně
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
přes	přes	k7c4	přes
výhrady	výhrada	k1gFnPc4	výhrada
svého	svůj	k3xOyFgMnSc2	svůj
předsedy	předseda	k1gMnSc2	předseda
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
demisi	demise	k1gFnSc3	demise
demokratických	demokratický	k2eAgMnPc2d1	demokratický
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
společný	společný	k2eAgInSc1d1	společný
postup	postup	k1gInSc1	postup
přišel	přijít	k5eAaPmAgInS	přijít
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Beneš	Beneš	k1gMnSc1	Beneš
naplnil	naplnit	k5eAaPmAgMnS	naplnit
nejhorší	zlý	k2eAgFnPc4d3	nejhorší
Šrámkovy	Šrámkův	k2eAgFnPc4d1	Šrámkova
obavy	obava	k1gFnPc4	obava
a	a	k8xC	a
podezření	podezření	k1gNnPc4	podezření
<g/>
,	,	kIx,	,
když	když	k8xS	když
porušil	porušit	k5eAaPmAgMnS	porušit
své	svůj	k3xOyFgInPc4	svůj
sliby	slib	k1gInPc4	slib
(	(	kIx(	(
<g/>
a	a	k8xC	a
po	po	k7c4	po
demisi	demise	k1gFnSc4	demise
dalších	další	k2eAgMnPc2d1	další
ministrů	ministr	k1gMnPc2	ministr
i	i	k8xC	i
ústavu	ústav	k1gInSc2	ústav
<g/>
)	)	kIx)	)
a	a	k8xC	a
demisi	demise	k1gFnSc4	demise
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
ČSL	ČSL	kA	ČSL
tehdy	tehdy	k6eAd1	tehdy
zahájila	zahájit	k5eAaPmAgFnS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
bitvu	bitva	k1gFnSc4	bitva
o	o	k7c4	o
sama	sám	k3xTgMnSc4	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
některé	některý	k3yIgInPc4	některý
prokomunistické	prokomunistický	k2eAgInPc4d1	prokomunistický
exponenty	exponent	k1gInPc4	exponent
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
Josefa	Josef	k1gMnSc4	Josef
Plojhara	Plojhar	k1gMnSc4	Plojhar
a	a	k8xC	a
Aloise	Alois	k1gMnSc4	Alois
Petra	Petr	k1gMnSc4	Petr
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
prokomunistické	prokomunistický	k2eAgNnSc1d1	prokomunistické
křídlo	křídlo	k1gNnSc1	křídlo
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
komunistických	komunistický	k2eAgFnPc2d1	komunistická
bojůvek	bojůvka	k1gFnPc2	bojůvka
násilím	násilí	k1gNnSc7	násilí
mnoho	mnoho	k4c1	mnoho
sekretariátů	sekretariát	k1gInPc2	sekretariát
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
stranu	strana	k1gFnSc4	strana
zcela	zcela	k6eAd1	zcela
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
Šrámkem	Šrámek	k1gMnSc7	Šrámek
a	a	k8xC	a
tajemníkem	tajemník	k1gMnSc7	tajemník
Hálou	Hála	k1gMnSc7	Hála
<g/>
,	,	kIx,	,
rezignovalo	rezignovat	k5eAaBmAgNnS	rezignovat
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
boj	boj	k1gInSc4	boj
a	a	k8xC	a
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
členů	člen	k1gMnPc2	člen
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
však	však	k9	však
jen	jen	k6eAd1	jen
některým	některý	k3yIgMnPc3	některý
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
představitelé	představitel	k1gMnPc1	představitel
strany	strana	k1gFnSc2	strana
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
a	a	k8xC	a
František	František	k1gMnSc1	František
Hála	Hála	k1gMnSc1	Hála
byli	být	k5eAaImAgMnP	být
dopadeni	dopadnout	k5eAaPmNgMnP	dopadnout
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
strávili	strávit	k5eAaPmAgMnP	strávit
v	v	k7c6	v
internaci	internace	k1gFnSc6	internace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkově	celkově	k6eAd1	celkově
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
11	[number]	k4	11
ze	z	k7c2	z
46	[number]	k4	46
poslanců	poslanec	k1gMnPc2	poslanec
ČSL	ČSL	kA	ČSL
(	(	kIx(	(
<g/>
a	a	k8xC	a
stovky	stovka	k1gFnPc1	stovka
dalších	další	k2eAgInPc2d1	další
členů	člen	k1gInPc2	člen
včetně	včetně	k7c2	včetně
například	například	k6eAd1	například
Heleny	Helena	k1gFnSc2	Helena
Koželuhové	koželuh	k1gMnPc1	koželuh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
21	[number]	k4	21
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
uvězněno	uvězněn	k2eAgNnSc1d1	uvězněno
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
4	[number]	k4	4
umučeni	umučit	k5eAaPmNgMnP	umučit
nebo	nebo	k8xC	nebo
popraveni	popravit	k5eAaPmNgMnP	popravit
–	–	k?	–
Stanislav	Stanislav	k1gMnSc1	Stanislav
Broj	broj	k1gFnSc4	broj
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
Sochorec	Sochorec	k1gMnSc1	Sochorec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Plesl	Plesl	k1gInSc1	Plesl
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
)	)	kIx)	)
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
odešlo	odejít	k5eAaPmAgNnS	odejít
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgMnPc1	čtyři
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
režimem	režim	k1gInSc7	režim
plně	plně	k6eAd1	plně
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
se	se	k3xPyFc4	se
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
represím	represe	k1gFnPc3	represe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
stranu	strana	k1gFnSc4	strana
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
prokomunistické	prokomunistický	k2eAgNnSc1d1	prokomunistické
křídlo	křídlo	k1gNnSc1	křídlo
vedené	vedený	k2eAgNnSc1d1	vedené
Aloisem	Alois	k1gMnSc7	Alois
Petrem	Petr	k1gMnSc7	Petr
a	a	k8xC	a
exkomunikovaným	exkomunikovaný	k2eAgMnSc7d1	exkomunikovaný
knězem	kněz	k1gMnSc7	kněz
Josefem	Josef	k1gMnSc7	Josef
Plojharem	Plojhar	k1gMnSc7	Plojhar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
sice	sice	k8xC	sice
bylo	být	k5eAaImAgNnS	být
vedení	vedení	k1gNnSc1	vedení
ČSL	ČSL	kA	ČSL
odvoláno	odvolán	k2eAgNnSc1d1	odvolán
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
se	se	k3xPyFc4	se
jevila	jevit	k5eAaImAgFnS	jevit
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
stranu	strana	k1gFnSc4	strana
od	od	k7c2	od
vlády	vláda	k1gFnSc2	vláda
komunistů	komunista	k1gMnPc2	komunista
oprostit	oprostit	k5eAaPmF	oprostit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
srpnová	srpnový	k2eAgFnSc1d1	srpnová
invaze	invaze	k1gFnSc1	invaze
zastavila	zastavit	k5eAaPmAgFnS	zastavit
a	a	k8xC	a
eradikovala	eradikovat	k5eAaImAgFnS	eradikovat
změny	změna	k1gFnPc4	změna
přinesené	přinesený	k2eAgNnSc1d1	přinesené
pražským	pražský	k2eAgNnSc7d1	Pražské
jarem	jaro	k1gNnSc7	jaro
a	a	k8xC	a
navrátila	navrátit	k5eAaPmAgFnS	navrátit
ČSL	ČSL	kA	ČSL
do	do	k7c2	do
vlivu	vliv	k1gInSc2	vliv
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
tak	tak	k8xS	tak
strana	strana	k1gFnSc1	strana
existovala	existovat	k5eAaImAgFnS	existovat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
strany	strana	k1gFnSc2	strana
–	–	k?	–
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
malými	malý	k2eAgFnPc7d1	malá
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
byla	být	k5eAaImAgFnS	být
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
socialistická	socialistický	k2eAgFnSc1d1	socialistická
–	–	k?	–
měla	mít	k5eAaImAgFnS	mít
vzbuzovat	vzbuzovat	k5eAaImF	vzbuzovat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
působí	působit	k5eAaImIp3nS	působit
více	hodně	k6eAd2	hodně
autonomních	autonomní	k2eAgFnPc2d1	autonomní
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
ČSL	ČSL	kA	ČSL
existovalo	existovat	k5eAaImAgNnS	existovat
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
voleno	volit	k5eAaImNgNnS	volit
podle	podle	k7c2	podle
přání	přání	k1gNnSc2	přání
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
také	také	k6eAd1	také
řídilo	řídit	k5eAaImAgNnS	řídit
svoji	svůj	k3xOyFgFnSc4	svůj
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozhodně	rozhodně	k6eAd1	rozhodně
neodpovídala	odpovídat	k5eNaImAgFnS	odpovídat
přání	přání	k1gNnSc3	přání
členské	členský	k2eAgFnSc2d1	členská
základny	základna	k1gFnSc2	základna
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
mnoho	mnoho	k4c1	mnoho
členů	člen	k1gInPc2	člen
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
<g/>
,	,	kIx,	,
vyjadřujíce	vyjadřovat	k5eAaImSgMnP	vyjadřovat
tak	tak	k8xS	tak
svým	svůj	k3xOyFgNnSc7	svůj
členstvím	členství	k1gNnSc7	členství
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
ČSL	ČSL	kA	ČSL
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
bráno	brán	k2eAgNnSc1d1	bráno
jako	jako	k8xS	jako
společenské	společenský	k2eAgNnSc1d1	společenské
mínus	mínus	k6eAd1	mínus
a	a	k8xC	a
vyjádření	vyjádření	k1gNnSc4	vyjádření
odporu	odpor	k1gInSc2	odpor
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
komunistickým	komunistický	k2eAgMnSc7d1	komunistický
státem	stát	k1gInSc7	stát
postihováno	postihován	k2eAgNnSc1d1	postihováno
–	–	k?	–
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
vysoce	vysoce	k6eAd1	vysoce
prokomunisticky	prokomunisticky	k6eAd1	prokomunisticky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ovšem	ovšem	k9	ovšem
představovali	představovat	k5eAaImAgMnP	představovat
výraznou	výrazný	k2eAgFnSc4d1	výrazná
menšinu	menšina	k1gFnSc4	menšina
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
fakticky	fakticky	k6eAd1	fakticky
protikomunistickým	protikomunistický	k2eAgNnSc7d1	protikomunistické
členstvem	členstvo	k1gNnSc7	členstvo
a	a	k8xC	a
prokomunistickým	prokomunistický	k2eAgNnSc7d1	prokomunistické
vedením	vedení	k1gNnSc7	vedení
často	často	k6eAd1	často
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
konfliktům	konflikt	k1gInPc3	konflikt
a	a	k8xC	a
řadoví	řadový	k2eAgMnPc1d1	řadový
členové	člen	k1gMnPc1	člen
často	často	k6eAd1	často
využívali	využívat	k5eAaPmAgMnP	využívat
stranický	stranický	k2eAgInSc4d1	stranický
aparát	aparát	k1gInSc4	aparát
k	k	k7c3	k
akcím	akce	k1gFnPc3	akce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
snahám	snaha	k1gFnPc3	snaha
prokomunistického	prokomunistický	k2eAgInSc2d1	prokomunistický
vedení	vedení	k1gNnSc6	vedení
to	ten	k3xDgNnSc1	ten
zakázat	zakázat	k5eAaPmF	zakázat
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
což	což	k3yQnSc4	což
se	se	k3xPyFc4	se
stávali	stávat	k5eAaImAgMnP	stávat
předmětem	předmět	k1gInSc7	předmět
represí	represe	k1gFnPc2	represe
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
ČSL	ČSL	kA	ČSL
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
a	a	k8xC	a
organizaci	organizace	k1gFnSc3	organizace
velkých	velký	k2eAgNnPc2d1	velké
katolických	katolický	k2eAgNnPc2d1	katolické
vystoupení	vystoupení	k1gNnPc2	vystoupení
proti	proti	k7c3	proti
komunismu	komunismus	k1gInSc3	komunismus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Národní	národní	k2eAgFnSc2d1	národní
poutě	pouť	k1gFnSc2	pouť
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
a	a	k8xC	a
petice	petice	k1gFnPc1	petice
Augustina	Augustin	k1gMnSc2	Augustin
Navrátila	Navrátil	k1gMnSc2	Navrátil
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
protikomunisticky	protikomunisticky	k6eAd1	protikomunisticky
smýšlející	smýšlející	k2eAgMnPc1d1	smýšlející
členové	člen	k1gMnPc1	člen
a	a	k8xC	a
funkcionáři	funkcionář	k1gMnPc1	funkcionář
profilovali	profilovat	k5eAaImAgMnP	profilovat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Obrodném	obrodný	k2eAgInSc6d1	obrodný
proudu	proud	k1gInSc6	proud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
postupně	postupně	k6eAd1	postupně
nabíral	nabírat	k5eAaImAgInS	nabírat
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
převzal	převzít	k5eAaPmAgInS	převzít
vedení	vedení	k1gNnSc4	vedení
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
strany	strana	k1gFnSc2	strana
vynesl	vynést	k5eAaPmAgMnS	vynést
Josefa	Josef	k1gMnSc4	Josef
Bartončíka	Bartončík	k1gMnSc4	Bartončík
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
stranu	strana	k1gFnSc4	strana
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
původní	původní	k2eAgFnSc3d1	původní
politice	politika	k1gFnSc3	politika
a	a	k8xC	a
ideologii	ideologie	k1gFnSc3	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
stranické	stranický	k2eAgFnSc2d1	stranická
členské	členský	k2eAgFnSc2d1	členská
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
hlásili	hlásit	k5eAaImAgMnP	hlásit
noví	nový	k2eAgMnPc1d1	nový
příznivci	příznivec	k1gMnPc1	příznivec
i	i	k8xC	i
staří	starý	k2eAgMnPc1d1	starý
členové	člen	k1gMnPc1	člen
vyloučení	vyloučení	k1gNnPc2	vyloučení
starým	staré	k1gNnSc7	staré
prokomunistickým	prokomunistický	k2eAgNnSc7d1	prokomunistické
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1989	[number]	k4	1989
měla	mít	k5eAaImAgFnS	mít
strana	strana	k1gFnSc1	strana
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
necelých	celý	k2eNgInPc2d1	necelý
90	[number]	k4	90
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
strana	strana	k1gFnSc1	strana
využívala	využívat	k5eAaImAgFnS	využívat
svůj	svůj	k3xOyFgInSc4	svůj
stávající	stávající	k2eAgInSc4d1	stávající
organizační	organizační	k2eAgInSc4d1	organizační
aparát	aparát	k1gInSc4	aparát
a	a	k8xC	a
těžila	těžit	k5eAaImAgFnS	těžit
z	z	k7c2	z
morálního	morální	k2eAgInSc2d1	morální
kreditu	kredit	k1gInSc2	kredit
protikomunistického	protikomunistický	k2eAgInSc2d1	protikomunistický
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgMnPc3	tento
faktorům	faktor	k1gMnPc3	faktor
měla	mít	k5eAaImAgFnS	mít
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Josefa	Josef	k1gMnSc2	Josef
Bartončíka	Bartončík	k1gMnSc2	Bartončík
značné	značný	k2eAgFnSc2d1	značná
politické	politický	k2eAgFnSc2d1	politická
ambice	ambice	k1gFnSc2	ambice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
současně	současně	k6eAd1	současně
vyvolávaly	vyvolávat	k5eAaImAgFnP	vyvolávat
antipatie	antipatie	k1gFnPc4	antipatie
vůči	vůči	k7c3	vůči
ČSL	ČSL	kA	ČSL
i	i	k8xC	i
vůči	vůči	k7c3	vůči
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozpor	rozpor	k1gInSc1	rozpor
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
projevil	projevit	k5eAaPmAgInS	projevit
u	u	k7c2	u
první	první	k4xOgFnSc2	první
návštěvy	návštěva	k1gFnSc2	návštěva
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
postkomunistickém	postkomunistický	k2eAgNnSc6d1	postkomunistické
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pojata	pojmout	k5eAaPmNgFnS	pojmout
monumentálním	monumentální	k2eAgInSc7d1	monumentální
triumfalistickým	triumfalistický	k2eAgInSc7d1	triumfalistický
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1990	[number]	k4	1990
straně	strana	k1gFnSc6	strana
přinesly	přinést	k5eAaPmAgInP	přinést
zklamání	zklamání	k1gNnSc4	zklamání
nejen	nejen	k6eAd1	nejen
kvůli	kvůli	k7c3	kvůli
relativně	relativně	k6eAd1	relativně
malému	malý	k2eAgInSc3d1	malý
volebnímu	volební	k2eAgInSc3d1	volební
zisku	zisk	k1gInSc3	zisk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
aféře	aféra	k1gFnSc3	aféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
<g/>
,	,	kIx,	,
když	když	k8xS	když
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
oznámil	oznámit	k5eAaPmAgMnS	oznámit
v	v	k7c6	v
předvolebním	předvolební	k2eAgNnSc6d1	předvolební
moratoriu	moratorium	k1gNnSc6	moratorium
<g/>
,	,	kIx,	,
že	že	k8xS	že
Josef	Josef	k1gMnSc1	Josef
Bartončík	Bartončík	k1gMnSc1	Bartončík
byl	být	k5eAaImAgMnS	být
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
StB	StB	k1gFnSc2	StB
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
později	pozdě	k6eAd2	pozdě
Ústřední	ústřední	k2eAgFnSc2d1	ústřední
volební	volební	k2eAgFnSc2d1	volební
komise	komise	k1gFnSc2	komise
posoudila	posoudit	k5eAaPmAgFnS	posoudit
jako	jako	k9	jako
hrubé	hrubý	k2eAgNnSc4d1	hrubé
porušení	porušení	k1gNnSc4	porušení
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagovala	reagovat	k5eAaBmAgFnS	reagovat
nejdříve	dříve	k6eAd3	dříve
nedůvěrou	nedůvěra	k1gFnSc7	nedůvěra
v	v	k7c4	v
obvinění	obvinění	k1gNnSc4	obvinění
a	a	k8xC	a
protesty	protest	k1gInPc4	protest
proti	proti	k7c3	proti
hrubému	hrubý	k2eAgNnSc3d1	hrubé
porušení	porušení	k1gNnSc3	porušení
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
ale	ale	k8xC	ale
reagovala	reagovat	k5eAaBmAgFnS	reagovat
volbou	volba	k1gFnSc7	volba
nového	nový	k2eAgNnSc2d1	nové
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Luxem	Lux	k1gMnSc7	Lux
a	a	k8xC	a
začlenila	začlenit	k5eAaPmAgFnS	začlenit
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
stanov	stanova	k1gFnPc2	stanova
lustrační	lustrační	k2eAgInPc1d1	lustrační
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
další	další	k2eAgInPc4d1	další
podobné	podobný	k2eAgInPc4d1	podobný
skandály	skandál	k1gInPc4	skandál
znemožnit	znemožnit	k5eAaPmF	znemožnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
též	též	k9	též
završil	završit	k5eAaPmAgInS	završit
příliv	příliv	k1gInSc1	příliv
nových	nový	k2eAgMnPc2d1	nový
členů	člen	k1gMnPc2	člen
do	do	k7c2	do
řad	řada	k1gFnPc2	řada
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
začal	začít	k5eAaPmAgInS	začít
doposud	doposud	k6eAd1	doposud
trvající	trvající	k2eAgInSc1d1	trvající
úbytek	úbytek	k1gInSc1	úbytek
členstva	členstvo	k1gNnSc2	členstvo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zatím	zatím	k6eAd1	zatím
nejvýraznějším	výrazný	k2eAgMnSc7d3	nejvýraznější
a	a	k8xC	a
nejschopnějším	schopný	k2eAgMnSc7d3	nejschopnější
předsedou	předseda	k1gMnSc7	předseda
lidovců	lidovec	k1gMnPc2	lidovec
v	v	k7c6	v
polistopadové	polistopadový	k2eAgFnSc6d1	polistopadová
éře	éra	k1gFnSc6	éra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
ČSL	ČSL	kA	ČSL
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
KDU	KDU	kA	KDU
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
používána	používat	k5eAaImNgFnS	používat
pro	pro	k7c4	pro
společnou	společný	k2eAgFnSc4d1	společná
koalici	koalice	k1gFnSc4	koalice
ČSL	ČSL	kA	ČSL
a	a	k8xC	a
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
udržovala	udržovat	k5eAaImAgFnS	udržovat
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
překračoval	překračovat	k5eAaImAgInS	překračovat
její	její	k3xOp3gFnPc4	její
volební	volební	k2eAgFnPc4d1	volební
preference	preference	k1gFnPc4	preference
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
členem	člen	k1gInSc7	člen
všech	všecek	k3xTgFnPc2	všecek
vlád	vláda	k1gFnPc2	vláda
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
po	po	k7c6	po
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
a	a	k8xC	a
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
stranu	strana	k1gFnSc4	strana
opustila	opustit	k5eAaPmAgFnS	opustit
frakce	frakce	k1gFnSc1	frakce
asi	asi	k9	asi
60	[number]	k4	60
členů	člen	k1gMnPc2	člen
kolem	kolem	k7c2	kolem
bývalého	bývalý	k2eAgMnSc2d1	bývalý
předsedy	předseda	k1gMnSc2	předseda
Bartončíka	Bartončík	k1gMnSc2	Bartončík
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
pravicovou	pravicový	k2eAgFnSc7d1	pravicová
linií	linie	k1gFnSc7	linie
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
Křesťanskosociální	křesťanskosociální	k2eAgFnSc4d1	křesťanskosociální
unii	unie	k1gFnSc4	unie
(	(	kIx(	(
<g/>
KSU	KSU	kA	KSU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
významněji	významně	k6eAd2	významně
na	na	k7c6	na
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
neprosadila	prosadit	k5eNaPmAgFnS	prosadit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
strana	strana	k1gFnSc1	strana
své	svůj	k3xOyFgNnSc4	svůj
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
ODS	ODS	kA	ODS
zrušila	zrušit	k5eAaPmAgFnS	zrušit
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
skandály	skandál	k1gInPc4	skandál
okolo	okolo	k7c2	okolo
jejího	její	k3xOp3gNnSc2	její
financování	financování	k1gNnSc2	financování
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Tošovského	Tošovského	k2eAgFnSc6d1	Tošovského
úřednické	úřednický	k2eAgFnSc6d1	úřednická
vládě	vláda	k1gFnSc6	vláda
následoval	následovat	k5eAaImAgInS	následovat
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
v	v	k7c6	v
době	doba	k1gFnSc6	doba
trvání	trvání	k1gNnSc2	trvání
tzv.	tzv.	kA	tzv.
Opoziční	opoziční	k2eAgFnSc2d1	opoziční
smlouvy	smlouva	k1gFnSc2	smlouva
mezi	mezi	k7c7	mezi
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
složkou	složka	k1gFnSc7	složka
tzv.	tzv.	kA	tzv.
Čtyřkoalice	Čtyřkoalika	k1gFnSc6	Čtyřkoalika
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc4d1	společný
projekt	projekt	k1gInSc4	projekt
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
,	,	kIx,	,
US	US	kA	US
<g/>
,	,	kIx,	,
ODA	ODA	kA	ODA
a	a	k8xC	a
DEU	DEU	kA	DEU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
této	tento	k3xDgFnSc2	tento
opoziční	opoziční	k2eAgFnSc2d1	opoziční
epizody	epizoda	k1gFnSc2	epizoda
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
těžká	těžký	k2eAgFnSc1d1	těžká
leukemie	leukemie	k1gFnSc1	leukemie
<g/>
.	.	kIx.	.
</s>
<s>
Nečekaný	čekaný	k2eNgInSc1d1	nečekaný
odchod	odchod	k1gInSc1	odchod
schopného	schopný	k2eAgMnSc2d1	schopný
a	a	k8xC	a
dominantního	dominantní	k2eAgMnSc2d1	dominantní
předsedy	předseda	k1gMnSc2	předseda
s	s	k7c7	s
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
autoritou	autorita	k1gFnSc7	autorita
stranou	strana	k1gFnSc7	strana
značně	značně	k6eAd1	značně
otřásl	otřást	k5eAaPmAgInS	otřást
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nebyla	být	k5eNaImAgFnS	být
připravena	připravit	k5eAaPmNgFnS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
"	"	kIx"	"
<g/>
kdo	kdo	k3yInSc1	kdo
Luxe	Lux	k1gMnSc4	Lux
nahradí	nahradit	k5eAaPmIp3nS	nahradit
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
zpočátku	zpočátku	k6eAd1	zpočátku
komplikována	komplikovat	k5eAaBmNgFnS	komplikovat
dodatečnou	dodatečný	k2eAgFnSc7d1	dodatečná
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
moci	moct	k5eAaImF	moct
vrátit	vrátit	k5eAaPmF	vrátit
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
bezpředmětnou	bezpředmětný	k2eAgFnSc7d1	bezpředmětná
<g/>
,	,	kIx,	,
když	když	k8xS	když
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1999	[number]	k4	1999
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KDU-ČSL	KDU-ČSL	k?	KDU-ČSL
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stala	stát	k5eAaPmAgFnS	stát
dominantní	dominantní	k2eAgFnSc7d1	dominantní
silou	síla	k1gFnSc7	síla
opozičního	opoziční	k2eAgInSc2d1	opoziční
projektu	projekt	k1gInSc2	projekt
Čtyřkoalice	Čtyřkoalice	k1gFnSc2	Čtyřkoalice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
částečně	částečně	k6eAd1	částečně
demontována	demontován	k2eAgFnSc1d1	demontována
(	(	kIx(	(
<g/>
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
už	už	k6eAd1	už
šla	jít	k5eAaImAgFnS	jít
fakticky	fakticky	k6eAd1	fakticky
jako	jako	k8xC	jako
dvoukoalice	dvoukoalice	k1gFnSc1	dvoukoalice
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
a	a	k8xC	a
US-DEU	US-DEU	k1gFnSc2	US-DEU
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jedinou	jediný	k2eAgFnSc4d1	jediná
stanou	stanout	k5eAaPmIp3nP	stanout
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
jednoznačně	jednoznačně	k6eAd1	jednoznačně
vydělala	vydělat	k5eAaPmAgFnS	vydělat
<g/>
,	,	kIx,	,
když	když	k8xS	když
lidovečtí	lidovecký	k2eAgMnPc1d1	lidovecký
voliči	volič	k1gMnPc1	volič
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
pomocí	pomocí	k7c2	pomocí
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
zásadně	zásadně	k6eAd1	zásadně
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
do	do	k7c2	do
kandidátek	kandidátka	k1gFnPc2	kandidátka
v	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
US-DEU	US-DEU	k1gFnSc2	US-DEU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2002	[number]	k4	2002
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
stala	stát	k5eAaPmAgFnS	stát
koaličním	koaliční	k2eAgMnSc7d1	koaliční
partnerem	partner	k1gMnSc7	partner
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
US-DEU	US-DEU	k1gFnSc7	US-DEU
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
postupně	postupně	k6eAd1	postupně
sestavila	sestavit	k5eAaPmAgFnS	sestavit
vlády	vláda	k1gFnPc4	vláda
s	s	k7c7	s
premiéry	premiér	k1gMnPc7	premiér
Špidlou	Špidla	k1gMnSc7	Špidla
<g/>
,	,	kIx,	,
Grossem	Gross	k1gMnSc7	Gross
a	a	k8xC	a
Paroubkem	Paroubek	k1gMnSc7	Paroubek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
druhou	druhý	k4xOgFnSc7	druhý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
vlád	vláda	k1gFnPc2	vláda
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
demise	demise	k1gFnSc1	demise
lidoveckých	lidovecký	k2eAgMnPc2d1	lidovecký
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
nevyjasněnými	vyjasněný	k2eNgInPc7d1	nevyjasněný
finančními	finanční	k2eAgInPc7d1	finanční
poměry	poměr	k1gInPc7	poměr
rodiny	rodina	k1gFnSc2	rodina
Grossových	Grossových	k2eAgInPc1d1	Grossových
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubek	k1gMnSc2	Paroubek
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ostrým	ostrý	k2eAgInPc3d1	ostrý
konfliktům	konflikt	k1gInPc3	konflikt
mezi	mezi	k7c7	mezi
KDU-ČSL	KDU-ČSL	k1gFnSc7	KDU-ČSL
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
socialisté	socialist	k1gMnPc1	socialist
prohlasovali	prohlasovat	k5eAaImAgMnP	prohlasovat
v	v	k7c6	v
Parlamentu	parlament	k1gInSc6	parlament
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
svých	svůj	k3xOyFgMnPc2	svůj
koaličních	koaliční	k2eAgMnPc2d1	koaliční
partnerů	partner	k1gMnPc2	partner
několik	několik	k4yIc4	několik
klíčových	klíčový	k2eAgInPc2d1	klíčový
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
novel	novela	k1gFnPc2	novela
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
novely	novela	k1gFnPc4	novela
zákoníku	zákoník	k1gInSc2	zákoník
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
církvích	církev	k1gFnPc6	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
vyjednávala	vyjednávat	k5eAaImAgFnS	vyjednávat
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
vlády	vláda	k1gFnSc2	vláda
se	s	k7c7	s
zelenými	zelený	k2eAgMnPc7d1	zelený
a	a	k8xC	a
občanskými	občanský	k2eAgMnPc7d1	občanský
demokraty	demokrat	k1gMnPc7	demokrat
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
ale	ale	k9	ale
nezískala	získat	k5eNaPmAgFnS	získat
důvěru	důvěra	k1gFnSc4	důvěra
Sněmovny	sněmovna	k1gFnSc2	sněmovna
(	(	kIx(	(
<g/>
vládní	vládní	k2eAgFnSc2d1	vládní
strany	strana	k1gFnSc2	strana
měly	mít	k5eAaImAgFnP	mít
100	[number]	k4	100
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
opozice	opozice	k1gFnSc1	opozice
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
KSČM	KSČM	kA	KSČM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
zahájil	zahájit	k5eAaPmAgMnS	zahájit
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
jednání	jednání	k1gNnSc4	jednání
o	o	k7c4	o
vládní	vládní	k2eAgFnSc4d1	vládní
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nP	by
tolerovali	tolerovat	k5eAaImAgMnP	tolerovat
komunisté	komunista	k1gMnPc1	komunista
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jej	on	k3xPp3gMnSc4	on
stálo	stát	k5eAaImAgNnS	stát
předsednické	předsednický	k2eAgNnSc1d1	předsednické
křeslo	křeslo	k1gNnSc1	křeslo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
strana	strana	k1gFnSc1	strana
takovouto	takovýto	k3xDgFnSc4	takovýto
možnost	možnost	k1gFnSc4	možnost
striktně	striktně	k6eAd1	striktně
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kalousek	Kalousek	k1gMnSc1	Kalousek
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
předsedy	předseda	k1gMnSc2	předseda
KDU-ČSL	KDU-ČSL	k1gMnSc2	KDU-ČSL
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
zvolen	zvolen	k2eAgMnSc1d1	zvolen
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
starosta	starosta	k1gMnSc1	starosta
Vsetína	Vsetín	k1gInSc2	Vsetín
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
Jiří	Jiří	k1gMnSc1	Jiří
Čunek	Čunek	k1gMnSc1	Čunek
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
koaliční	koaliční	k2eAgFnSc4d1	koaliční
vládu	vláda	k1gFnSc4	vláda
s	s	k7c7	s
ODS	ODS	kA	ODS
a	a	k8xC	a
Stranou	strana	k1gFnSc7	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
důvěru	důvěra	k1gFnSc4	důvěra
díky	díky	k7c3	díky
poslancům	poslanec	k1gMnPc3	poslanec
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
Pohankovi	Pohanka	k1gMnSc3	Pohanka
a	a	k8xC	a
Melčákovi	Melčák	k1gMnSc3	Melčák
<g/>
.	.	kIx.	.
</s>
<s>
Ministry	ministr	k1gMnPc4	ministr
druhé	druhý	k4xOgFnSc2	druhý
Topolánkovy	Topolánkův	k2eAgFnSc2d1	Topolánkova
vlády	vláda	k1gFnSc2	vláda
za	za	k7c4	za
KDU-ČSL	KDU-ČSL	k1gMnPc4	KDU-ČSL
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Jiří	Jiří	k1gMnSc1	Jiří
Čunek	Čunek	k1gMnSc1	Čunek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jehlička	jehlička	k1gFnSc1	jehlička
(	(	kIx(	(
<g/>
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
působení	působení	k1gNnSc6	působení
Heleny	Helena	k1gFnSc2	Helena
Třeštíkové	Třeštíková	k1gFnSc2	Třeštíková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Parkanová	Parkanová	k1gFnSc1	Parkanová
a	a	k8xC	a
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Jiřího	Jiří	k1gMnSc2	Jiří
Čunka	Čunka	k1gMnSc1	Čunka
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
a	a	k8xC	a
dalším	další	k2eAgMnSc7d1	další
ministrem	ministr	k1gMnSc7	ministr
KDU-ČSL	KDU-ČSL	k1gMnSc7	KDU-ČSL
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koaliční	koaliční	k2eAgNnSc1d1	koaliční
vládnutí	vládnutí	k1gNnSc1	vládnutí
s	s	k7c7	s
občanskými	občanský	k2eAgMnPc7d1	občanský
demokraty	demokrat	k1gMnPc7	demokrat
a	a	k8xC	a
zelenými	zelený	k2eAgMnPc7d1	zelený
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
reformy	reforma	k1gFnPc1	reforma
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
ODS	ODS	kA	ODS
vedl	vést	k5eAaImAgInS	vést
vládu	vláda	k1gFnSc4	vláda
k	k	k7c3	k
opatřením	opatření	k1gNnPc3	opatření
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
srovnávala	srovnávat	k5eAaImAgFnS	srovnávat
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
programem	program	k1gInSc7	program
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
klíčových	klíčový	k2eAgInPc2d1	klíčový
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
podpora	podpora	k1gFnSc1	podpora
rodin	rodina	k1gFnPc2	rodina
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
dětmi	dítě	k1gFnPc7	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
poslanců	poslanec	k1gMnPc2	poslanec
ODS	ODS	kA	ODS
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Vlastimilem	Vlastimil	k1gMnSc7	Vlastimil
Tlustým	tlustý	k2eAgMnSc7d1	tlustý
se	se	k3xPyFc4	se
postavilo	postavit	k5eAaPmAgNnS	postavit
proti	proti	k7c3	proti
vládnímu	vládní	k2eAgInSc3d1	vládní
návrhu	návrh	k1gInSc3	návrh
církevních	církevní	k2eAgFnPc2d1	církevní
restitucí	restituce	k1gFnPc2	restituce
a	a	k8xC	a
strana	strana	k1gFnSc1	strana
měla	mít	k5eAaImAgFnS	mít
zásadní	zásadní	k2eAgFnPc4d1	zásadní
potíže	potíž	k1gFnPc4	potíž
i	i	k9	i
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
body	bod	k1gInPc7	bod
Julínkovy	Julínkův	k2eAgFnSc2d1	Julínkova
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
reformy	reforma	k1gFnSc2	reforma
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
privatizace	privatizace	k1gFnSc2	privatizace
nemocnic	nemocnice	k1gFnPc2	nemocnice
<g/>
,	,	kIx,	,
úpravy	úprava	k1gFnPc4	úprava
interrupcí	interrupce	k1gFnPc2	interrupce
a	a	k8xC	a
poplatků	poplatek	k1gInPc2	poplatek
u	u	k7c2	u
lékaře	lékař	k1gMnSc2	lékař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
stranu	strana	k1gFnSc4	strana
sužovaly	sužovat	k5eAaImAgInP	sužovat
skandály	skandál	k1gInPc1	skandál
tvořící	tvořící	k2eAgInPc1d1	tvořící
se	se	k3xPyFc4	se
především	především	k6eAd1	především
okolo	okolo	k7c2	okolo
rodinných	rodinný	k2eAgFnPc2d1	rodinná
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
afér	aféra	k1gFnPc2	aféra
předsedy	předseda	k1gMnSc2	předseda
Čunka	Čunek	k1gMnSc2	Čunek
<g/>
.	.	kIx.	.
</s>
<s>
Problematická	problematický	k2eAgFnSc1d1	problematická
účast	účast	k1gFnSc1	účast
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
a	a	k8xC	a
skandály	skandál	k1gInPc1	skandál
čelních	čelní	k2eAgMnPc2d1	čelní
politiků	politik	k1gMnPc2	politik
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
odrazily	odrazit	k5eAaPmAgInP	odrazit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
krajských	krajský	k2eAgNnPc2d1	krajské
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
si	se	k3xPyFc3	se
oproti	oproti	k7c3	oproti
minulým	minulý	k2eAgNnPc3d1	Minulé
strana	strana	k1gFnSc1	strana
pohoršila	pohoršit	k5eAaPmAgFnS	pohoršit
<g/>
,	,	kIx,	,
a	a	k8xC	a
souběžných	souběžný	k2eAgFnPc6d1	souběžná
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neuspěl	uspět	k5eNaPmAgMnS	uspět
žádný	žádný	k3yNgMnSc1	žádný
její	její	k3xOp3gMnSc1	její
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
vlády	vláda	k1gFnSc2	vláda
2009	[number]	k4	2009
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Topolánkovy	Topolánkův	k2eAgFnSc2d1	Topolánkova
vlády	vláda	k1gFnSc2	vláda
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2009	[number]	k4	2009
KDU-ČSL	KDU-ČSL	k1gFnPc2	KDU-ČSL
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
staronovým	staronový	k2eAgMnSc7d1	staronový
předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
jmenovat	jmenovat	k5eAaImF	jmenovat
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
do	do	k7c2	do
"	"	kIx"	"
<g/>
přechodné	přechodný	k2eAgFnSc2d1	přechodná
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
<g/>
"	"	kIx"	"
premiéra	premiér	k1gMnSc2	premiér
Fischera	Fischer	k1gMnSc2	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
opustila	opustit	k5eAaPmAgFnS	opustit
skupina	skupina	k1gFnSc1	skupina
vlivných	vlivný	k2eAgMnPc2d1	vlivný
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Husák	Husák	k1gMnSc1	Husák
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šustr	Šustr	k1gMnSc1	Šustr
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Parkanová	Parkanová	k1gFnSc1	Parkanová
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Severa	Severa	k1gMnSc1	Severa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
poté	poté	k6eAd1	poté
spojili	spojit	k5eAaPmAgMnP	spojit
svůj	svůj	k3xOyFgInSc4	svůj
osud	osud	k1gInSc4	osud
s	s	k7c7	s
nově	nově	k6eAd1	nově
vznikající	vznikající	k2eAgFnSc7d1	vznikající
stranou	strana	k1gFnSc7	strana
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgMnS	následovat
je	být	k5eAaImIp3nS	být
však	však	k9	však
pouze	pouze	k6eAd1	pouze
omezený	omezený	k2eAgInSc1d1	omezený
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
následoval	následovat	k5eAaImAgInS	následovat
návrat	návrat	k1gInSc1	návrat
některých	některý	k3yIgMnPc2	některý
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
předtím	předtím	k6eAd1	předtím
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
či	či	k8xC	či
měli	mít	k5eAaImAgMnP	mít
pozastavené	pozastavený	k2eAgNnSc4d1	pozastavené
členství	členství	k1gNnSc4	členství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Cyrila	Cyril	k1gMnSc2	Cyril
Svobody	svoboda	k1gFnSc2	svoboda
strana	strana	k1gFnSc1	strana
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
návrat	návrat	k1gInSc4	návrat
ke	k	k7c3	k
konzervativním	konzervativní	k2eAgFnPc3d1	konzervativní
křesťanským	křesťanský	k2eAgFnPc3d1	křesťanská
hodnotám	hodnota	k1gFnPc3	hodnota
a	a	k8xC	a
ústup	ústup	k1gInSc4	ústup
od	od	k7c2	od
pravicové	pravicový	k2eAgFnSc2d1	pravicová
politiky	politika	k1gFnSc2	politika
prosazované	prosazovaný	k2eAgFnSc2d1	prosazovaná
právě	právě	k6eAd1	právě
poslancem	poslanec	k1gMnSc7	poslanec
Kalouskem	Kalousek	k1gMnSc7	Kalousek
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
předpokládanými	předpokládaný	k2eAgFnPc7d1	předpokládaná
předčasnými	předčasný	k2eAgFnPc7d1	předčasná
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
neuskutečnily	uskutečnit	k5eNaPmAgFnP	uskutečnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zrušil	zrušit	k5eAaPmAgMnS	zrušit
Ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
KDU-ČSL	KDU-ČSL	k1gMnSc4	KDU-ČSL
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
EDS	EDS	kA	EDS
Jany	Jana	k1gFnSc2	Jana
Hybáškové	Hybášková	k1gFnSc2	Hybášková
a	a	k8xC	a
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
a	a	k8xC	a
registrované	registrovaný	k2eAgMnPc4d1	registrovaný
příznivce	příznivec	k1gMnPc4	příznivec
EDS	EDS	kA	EDS
místa	místo	k1gNnPc4	místo
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
kandidátkách	kandidátka	k1gFnPc6	kandidátka
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
především	především	k9	především
hledání	hledání	k1gNnSc4	hledání
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
samotnou	samotný	k2eAgFnSc4d1	samotná
Janu	Jana	k1gFnSc4	Jana
Hybáškovou	Hybášková	k1gFnSc4	Hybášková
bylo	být	k5eAaImAgNnS	být
problematické	problematický	k2eAgNnSc1d1	problematické
a	a	k8xC	a
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
ostré	ostrý	k2eAgInPc4d1	ostrý
spory	spor	k1gInPc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
příznivci	příznivec	k1gMnPc1	příznivec
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
KDU-ČSL	KDU-ČSL	k1gFnPc2	KDU-ČSL
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
se	s	k7c7	s
spojenectvím	spojenectví	k1gNnSc7	spojenectví
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
názory	názor	k1gInPc4	názor
Hybáškové	Hybáškový	k2eAgInPc4d1	Hybáškový
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
interrupcí	interrupce	k1gFnPc2	interrupce
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
klíčových	klíčový	k2eAgFnPc6d1	klíčová
otázkách	otázka	k1gFnPc6	otázka
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
argumentovalo	argumentovat	k5eAaImAgNnS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Hybášková	Hybášková	k1gFnSc1	Hybášková
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
kandidáty	kandidát	k1gMnPc7	kandidát
své	svůj	k3xOyFgFnSc3	svůj
strany	strana	k1gFnSc2	strana
zavázala	zavázat	k5eAaPmAgFnS	zavázat
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
ochrany	ochrana	k1gFnSc2	ochrana
života	život	k1gInSc2	život
hlasovat	hlasovat	k5eAaImF	hlasovat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
postojem	postoj	k1gInSc7	postoj
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
se	se	k3xPyFc4	se
po	po	k7c6	po
odchodů	odchod	k1gInPc2	odchod
3	[number]	k4	3
senátorů	senátor	k1gMnPc2	senátor
(	(	kIx(	(
<g/>
Ludmila	Ludmila	k1gFnSc1	Ludmila
Müllerová	Müllerová	k1gFnSc1	Müllerová
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jehlička	jehlička	k1gFnSc1	jehlička
<g/>
)	)	kIx)	)
dočasně	dočasně	k6eAd1	dočasně
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
politický	politický	k2eAgInSc1d1	politický
klub	klub	k1gInSc1	klub
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
obnoven	obnovit	k5eAaPmNgInS	obnovit
po	po	k7c6	po
doplnění	doplnění	k1gNnSc6	doplnění
nových	nový	k2eAgMnPc2d1	nový
lidoveckých	lidovecký	k2eAgMnPc2d1	lidovecký
senátorů	senátor	k1gMnPc2	senátor
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
strávené	strávený	k2eAgNnSc1d1	strávené
mimo	mimo	k7c4	mimo
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
dopadly	dopadnout	k5eAaPmAgFnP	dopadnout
pro	pro	k7c4	pro
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
nezískala	získat	k5eNaPmAgFnS	získat
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
mandát	mandát	k1gInSc4	mandát
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
samostatné	samostatný	k2eAgFnSc2d1	samostatná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ztratila	ztratit	k5eAaPmAgNnP	ztratit
zastoupení	zastoupení	k1gNnPc1	zastoupení
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
komoře	komora	k1gFnSc6	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rezignuje	rezignovat	k5eAaBmIp3nS	rezignovat
na	na	k7c4	na
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
<g/>
Následně	následně	k6eAd1	následně
stranu	strana	k1gFnSc4	strana
dočasně	dočasně	k6eAd1	dočasně
vedla	vést	k5eAaImAgFnS	vést
Michaela	Michaela	k1gFnSc1	Michaela
Šojdrová	Šojdrová	k1gFnSc1	Šojdrová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
potýkat	potýkat	k5eAaImF	potýkat
zejména	zejména	k9	zejména
s	s	k7c7	s
výpadkem	výpadek	k1gInSc7	výpadek
financí	finance	k1gFnPc2	finance
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
relativně	relativně	k6eAd1	relativně
úspěšných	úspěšný	k2eAgFnPc6d1	úspěšná
podzimních	podzimní	k2eAgFnPc6d1	podzimní
volbách	volba	k1gFnPc6	volba
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
straně	strana	k1gFnSc3	strana
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
obnovit	obnovit	k5eAaPmF	obnovit
senátní	senátní	k2eAgInSc4d1	senátní
klub	klub	k1gInSc4	klub
a	a	k8xC	a
zbrzdit	zbrzdit	k5eAaPmF	zbrzdit
pokles	pokles	k1gInSc4	pokles
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
komunální	komunální	k2eAgFnSc6d1	komunální
politice	politika	k1gFnSc6	politika
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
sjezd	sjezd	k1gInSc1	sjezd
ve	v	k7c6	v
Žďáru	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Pavel	Pavel	k1gMnSc1	Pavel
Bělobrádek	Bělobrádka	k1gFnPc2	Bělobrádka
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kompletní	kompletní	k2eAgFnSc3d1	kompletní
výměně	výměna	k1gFnSc3	výměna
řídících	řídící	k2eAgInPc2d1	řídící
orgánů	orgán	k1gInPc2	orgán
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
pod	pod	k7c4	pod
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
formovat	formovat	k5eAaImF	formovat
svůj	svůj	k3xOyFgInSc4	svůj
nový	nový	k2eAgInSc4d1	nový
politický	politický	k2eAgInSc4d1	politický
kurz	kurz	k1gInSc4	kurz
a	a	k8xC	a
posunula	posunout	k5eAaPmAgFnS	posunout
se	se	k3xPyFc4	se
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
pravicovým	pravicový	k2eAgFnPc3d1	pravicová
pozicím	pozice	k1gFnPc3	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
uspokojivého	uspokojivý	k2eAgInSc2d1	uspokojivý
výsledku	výsledek	k1gInSc2	výsledek
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
a	a	k8xC	a
souběžně	souběžně	k6eAd1	souběžně
konaných	konaný	k2eAgFnPc6d1	konaná
senátních	senátní	k2eAgFnPc6d1	senátní
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ziskem	zisk	k1gInSc7	zisk
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
volebních	volební	k2eAgFnPc2d1	volební
koalic	koalice	k1gFnPc2	koalice
<g/>
)	)	kIx)	)
obsadila	obsadit	k5eAaPmAgFnS	obsadit
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
místo	místo	k1gNnSc1	místo
a	a	k8xC	a
obhájila	obhájit	k5eAaPmAgFnS	obhájit
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
počet	počet	k1gInSc4	počet
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
byli	být	k5eAaImAgMnP	být
za	za	k7c4	za
lidovce	lidovec	k1gMnPc4	lidovec
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
Jiří	Jiří	k1gMnSc1	Jiří
Čunek	Čunek	k1gMnSc1	Čunek
a	a	k8xC	a
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Brož	Brož	k1gMnSc1	Brož
<g/>
,	,	kIx,	,
uspěl	uspět	k5eAaPmAgMnS	uspět
také	také	k9	také
společný	společný	k2eAgMnSc1d1	společný
kandidát	kandidát	k1gMnSc1	kandidát
Libor	Libor	k1gMnSc1	Libor
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
kraje	kraj	k1gInSc2	kraj
zasedli	zasednout	k5eAaPmAgMnP	zasednout
zástupci	zástupce	k1gMnPc1	zástupce
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Koalice	koalice	k1gFnSc1	koalice
pro	pro	k7c4	pro
Pardubický	pardubický	k2eAgInSc4d1	pardubický
kraj	kraj	k1gInSc4	kraj
také	také	k9	také
v	v	k7c6	v
Pardubickém	pardubický	k2eAgInSc6d1	pardubický
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
stranický	stranický	k2eAgInSc1d1	stranický
sjezd	sjezd	k1gInSc1	sjezd
<g/>
,	,	kIx,	,
během	během	k7c2	během
nějž	jenž	k3xRgInSc2	jenž
byly	být	k5eAaImAgFnP	být
přepracovány	přepracován	k2eAgFnPc1d1	přepracována
stranické	stranický	k2eAgFnPc1d1	stranická
stanovy	stanova	k1gFnPc1	stanova
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
statut	statut	k1gInSc4	statut
příznivce	příznivec	k1gMnSc2	příznivec
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
regiony	region	k1gInPc1	region
budou	být	k5eAaImBp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
v	v	k7c6	v
celostátních	celostátní	k2eAgInPc6d1	celostátní
orgánech	orgán	k1gInPc6	orgán
také	také	k9	také
podle	podle	k7c2	podle
volebních	volební	k2eAgInPc2d1	volební
výsledků	výsledek	k1gInPc2	výsledek
a	a	k8xC	a
strana	strana	k1gFnSc1	strana
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
více	hodně	k6eAd2	hodně
otevřít	otevřít	k5eAaPmF	otevřít
nečlenům	nečlen	k1gMnPc3	nečlen
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
patrný	patrný	k2eAgInSc4d1	patrný
její	její	k3xOp3gInSc4	její
nekonfesijní	konfesijní	k2eNgInSc4d1	konfesijní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
cílovou	cílový	k2eAgFnSc4d1	cílová
voličskou	voličský	k2eAgFnSc4d1	voličská
skupinu	skupina	k1gFnSc4	skupina
byly	být	k5eAaImAgFnP	být
označeny	označen	k2eAgFnPc1d1	označena
rodiny	rodina	k1gFnPc1	rodina
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
neziskové	ziskový	k2eNgFnPc1d1	nezisková
organizace	organizace	k1gFnPc1	organizace
a	a	k8xC	a
starší	starý	k2eAgFnPc1d2	starší
generace	generace	k1gFnPc1	generace
<g/>
.	.	kIx.	.
</s>
<s>
Těsnou	těsný	k2eAgFnSc7d1	těsná
většinou	většina	k1gFnSc7	většina
nebyla	být	k5eNaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
změna	změna	k1gFnSc1	změna
slova	slovo	k1gNnSc2	slovo
Československá	československý	k2eAgFnSc1d1	Československá
v	v	k7c6	v
názvu	název	k1gInSc6	název
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
Česká	český	k2eAgNnPc4d1	české
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
poslaneckých	poslanecký	k2eAgFnPc2d1	Poslanecká
lavic	lavice	k1gFnPc2	lavice
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
sjezd	sjezd	k1gInSc1	sjezd
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
zejména	zejména	k9	zejména
přípravou	příprava	k1gFnSc7	příprava
na	na	k7c4	na
nadcházející	nadcházející	k2eAgFnPc4d1	nadcházející
sněmovní	sněmovní	k2eAgFnPc4d1	sněmovní
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
volby	volba	k1gFnPc4	volba
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
výše	vysoce	k6eAd2	vysoce
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
největším	veliký	k2eAgMnSc7d3	veliký
sponzorem	sponzor	k1gMnSc7	sponzor
byl	být	k5eAaImAgMnS	být
developer	developer	k1gMnSc1	developer
Luděk	Luděk	k1gMnSc1	Luděk
Sekyra	Sekyra	k1gMnSc1	Sekyra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sněmovních	sněmovní	k2eAgFnPc6d1	sněmovní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
strana	strana	k1gFnSc1	strana
s	s	k7c7	s
rezervou	rezerva	k1gFnSc7	rezerva
překonala	překonat	k5eAaPmAgFnS	překonat
pětiprocentní	pětiprocentní	k2eAgFnSc4d1	pětiprocentní
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc7	první
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšných	úspěšný	k2eAgFnPc6d1	úspěšná
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
vládní	vládní	k2eAgFnSc1d1	vládní
koalice	koalice	k1gFnSc1	koalice
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
hnutím	hnutí	k1gNnSc7	hnutí
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jí	on	k3xPp3gFnSc7	on
připadla	připadnout	k5eAaPmAgFnS	připadnout
tři	tři	k4xCgNnPc4	tři
ministerská	ministerský	k2eAgNnPc4d1	ministerské
křesla	křeslo	k1gNnPc4	křeslo
(	(	kIx(	(
<g/>
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropské	evropský	k2eAgFnPc1d1	Evropská
volby	volba	k1gFnPc1	volba
znamenaly	znamenat	k5eAaImAgFnP	znamenat
pro	pro	k7c4	pro
stranu	strana	k1gFnSc4	strana
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
získat	získat	k5eAaPmF	získat
tři	tři	k4xCgInPc4	tři
mandáty	mandát	k1gInPc4	mandát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
stranu	strana	k1gFnSc4	strana
historický	historický	k2eAgInSc4d1	historický
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
díky	díky	k7c3	díky
celkově	celkově	k6eAd1	celkově
nízké	nízký	k2eAgFnSc3d1	nízká
účasti	účast	k1gFnSc3	účast
a	a	k8xC	a
disciplinovanosti	disciplinovanost	k1gFnSc3	disciplinovanost
vlastních	vlastní	k2eAgMnPc2d1	vlastní
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
zvolenými	zvolený	k2eAgMnPc7d1	zvolený
europoslanci	europoslanec	k1gMnPc7	europoslanec
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Pavel	Pavel	k1gMnSc1	Pavel
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
Šojdrová	Šojdrová	k1gFnSc1	Šojdrová
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zdechovský	Zdechovský	k2eAgMnSc1d1	Zdechovský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nově	nově	k6eAd1	nově
zvolené	zvolený	k2eAgMnPc4d1	zvolený
europoslance	europoslanec	k1gMnPc4	europoslanec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
nezasedli	zasednout	k5eNaPmAgMnP	zasednout
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Svoboda	Svoboda	k1gMnSc1	Svoboda
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
vzápětí	vzápětí	k6eAd1	vzápětí
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
právního	právní	k2eAgInSc2d1	právní
výboru	výbor	k1gInSc2	výbor
EP	EP	kA	EP
(	(	kIx(	(
<g/>
JURI	JURI	kA	JURI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejvýše	nejvýše	k6eAd1	nejvýše
postavených	postavený	k2eAgInPc6d1	postavený
Čechem	Čech	k1gMnSc7	Čech
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
volební	volební	k2eAgInSc1d1	volební
sjezd	sjezd	k1gInSc1	sjezd
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
obhájil	obhájit	k5eAaPmAgMnS	obhájit
Pavel	Pavel	k1gMnSc1	Pavel
Bělobrádek	Bělobrádek	k1gInSc4	Bělobrádek
<g/>
,	,	kIx,	,
pozici	pozice	k1gFnSc4	pozice
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedy	místopředseda	k1gMnSc2	místopředseda
pak	pak	k6eAd1	pak
Marian	Marian	k1gMnSc1	Marian
Jurečka	Jurečka	k1gMnSc1	Jurečka
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
řadových	řadový	k2eAgMnPc2d1	řadový
místopředsedů	místopředseda	k1gMnPc2	místopředseda
získali	získat	k5eAaPmAgMnP	získat
Ondřej	Ondřej	k1gMnSc1	Ondřej
Benešík	Benešík	k1gMnSc1	Benešík
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Roithová	Roithová	k1gFnSc1	Roithová
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Bartošek	Bartošek	k1gMnSc1	Bartošek
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Mihola	Mihola	k1gFnSc1	Mihola
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
už	už	k6eAd1	už
naopak	naopak	k6eAd1	naopak
nekandidovali	kandidovat	k5eNaImAgMnP	kandidovat
dosavadní	dosavadní	k2eAgMnPc1d1	dosavadní
místopředsedové	místopředseda	k1gMnPc1	místopředseda
Roman	Roman	k1gMnSc1	Roman
Línek	línek	k1gMnSc1	línek
a	a	k8xC	a
Klára	Klára	k1gFnSc1	Klára
Liptáková	Liptáková	k1gFnSc1	Liptáková
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgInSc1d1	další
sjezd	sjezd	k1gInSc1	sjezd
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017	[number]	k4	2017
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
již	již	k6eAd1	již
po	po	k7c6	po
čtvrté	čtvrtá	k1gFnSc6	čtvrtá
stal	stát	k5eAaPmAgMnS	stát
Pavel	Pavel	k1gMnSc1	Pavel
Bělobrádek	Bělobrádka	k1gFnPc2	Bělobrádka
<g/>
,	,	kIx,	,
když	když	k8xS	když
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
náskokem	náskok	k1gInSc7	náskok
porazil	porazit	k5eAaPmAgMnS	porazit
svého	svůj	k3xOyFgMnSc4	svůj
vyzyvatele	vyzyvatele	k?	vyzyvatele
Jiřího	Jiří	k1gMnSc2	Jiří
Čunka	Čunek	k1gMnSc2	Čunek
<g/>
.	.	kIx.	.
</s>
<s>
Post	post	k1gInSc1	post
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedy	místopředseda	k1gMnSc2	místopředseda
obhájil	obhájit	k5eAaPmAgMnS	obhájit
Marian	Marian	k1gMnSc1	Marian
Jurečka	Jurečka	k1gMnSc1	Jurečka
a	a	k8xC	a
pozice	pozice	k1gFnSc1	pozice
řadových	řadový	k2eAgMnPc2d1	řadový
místopředsedů	místopředseda	k1gMnPc2	místopředseda
pak	pak	k6eAd1	pak
Ondřej	Ondřej	k1gMnSc1	Ondřej
Benešík	Benešík	k1gMnSc1	Benešík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Bartošek	Bartošek	k1gMnSc1	Bartošek
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Mihola	Mihola	k1gFnSc1	Mihola
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
místopředsedou	místopředseda	k1gMnSc7	místopředseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Daniel	Daniel	k1gMnSc1	Daniel
Herman	Herman	k1gMnSc1	Herman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koalice	koalice	k1gFnSc2	koalice
Lidovci	lidovec	k1gMnPc1	lidovec
a	a	k8xC	a
Starostové	Starosta	k1gMnPc1	Starosta
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
Starostové	Starostová	k1gFnSc2	Starostová
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
zrušit	zrušit	k5eAaPmF	zrušit
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
spolupráci	spolupráce	k1gFnSc6	spolupráce
Starostů	Starosta	k1gMnPc2	Starosta
s	s	k7c7	s
KDU-ČSL	KDU-ČSL	k1gFnSc7	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
koalice	koalice	k1gFnSc1	koalice
s	s	k7c7	s
názvem	název	k1gInSc7	název
Lidovci	lidovec	k1gMnPc1	lidovec
a	a	k8xC	a
Starostové	Starosta	k1gMnPc1	Starosta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
koalici	koalice	k1gFnSc4	koalice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
by	by	kYmCp3nP	by
byla	být	k5eAaImAgNnP	být
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
povinná	povinný	k2eAgFnSc1d1	povinná
desetiprocentní	desetiprocentní	k2eAgFnSc1d1	desetiprocentní
hranice	hranice	k1gFnSc1	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nepříznivým	příznivý	k2eNgInPc3d1	nepříznivý
průzkumům	průzkum	k1gInPc3	průzkum
(	(	kIx(	(
<g/>
koalice	koalice	k1gFnSc1	koalice
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
většiny	většina	k1gFnSc2	většina
průzkumů	průzkum	k1gInPc2	průzkum
nutnou	nutný	k2eAgFnSc4d1	nutná
desetiprocentní	desetiprocentní	k2eAgFnSc4d1	desetiprocentní
hranici	hranice	k1gFnSc4	hranice
nepřesáhla	přesáhnout	k5eNaPmAgFnS	přesáhnout
<g/>
)	)	kIx)	)
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
lidovci	lidovec	k1gMnPc1	lidovec
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
Starostům	Starosta	k1gMnPc3	Starosta
místo	místo	k7c2	místo
koalice	koalice	k1gFnSc2	koalice
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
lidovecké	lidovecký	k2eAgFnPc4d1	lidovecká
kandidátky	kandidátka	k1gFnPc4	kandidátka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
Starostové	Starosta	k1gMnPc1	Starosta
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
kandidovat	kandidovat	k5eAaImF	kandidovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
se	se	k3xPyFc4	se
tak	tak	k9	tak
tato	tento	k3xDgFnSc1	tento
koalice	koalice	k1gFnSc1	koalice
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volby	volba	k1gFnSc2	volba
2017	[number]	k4	2017
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
si	se	k3xPyFc3	se
vedení	vedený	k2eAgMnPc1d1	vedený
KDU-ČSL	KDU-ČSL	k1gMnPc1	KDU-ČSL
od	od	k7c2	od
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nakonec	nakonec	k6eAd1	nakonec
lidovci	lidovec	k1gMnPc1	lidovec
kandidovali	kandidovat	k5eAaImAgMnP	kandidovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
slibovalo	slibovat	k5eAaImAgNnS	slibovat
výsledek	výsledek	k1gInSc4	výsledek
mezi	mezi	k7c7	mezi
6-8	[number]	k4	6-8
procenty	procento	k1gNnPc7	procento
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
získala	získat	k5eAaPmAgFnS	získat
5,80	[number]	k4	5,80
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
konferenci	konference	k1gFnSc6	konference
27	[number]	k4	27
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
obhájilo	obhájit	k5eAaPmAgNnS	obhájit
vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
svá	svůj	k3xOyFgNnPc4	svůj
křesla	křeslo	k1gNnPc4	křeslo
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sjezd	sjezd	k1gInSc1	sjezd
2019	[number]	k4	2019
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2019	[number]	k4	2019
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Marek	Marek	k1gMnSc1	Marek
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
místopředsedkyní	místopředsedkyně	k1gFnPc2	místopředsedkyně
senátorka	senátorka	k1gFnSc1	senátorka
Šárka	Šárka	k1gFnSc1	Šárka
Jelínková	Jelínková	k1gFnSc1	Jelínková
<g/>
,	,	kIx,	,
řadovými	řadový	k2eAgFnPc7d1	řadová
místopředsedy	místopředseda	k1gMnPc7	místopředseda
poslanec	poslanec	k1gMnSc1	poslanec
Jan	Jan	k1gMnSc1	Jan
Bartošek	Bartošek	k1gMnSc1	Bartošek
a	a	k8xC	a
komunální	komunální	k2eAgMnPc1d1	komunální
politici	politik	k1gMnPc1	politik
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Niemiec	Niemiec	k1gMnSc1	Niemiec
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
Matek	matka	k1gFnPc2	matka
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Hladík	Hladík	k1gMnSc1	Hladík
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
rovněž	rovněž	k9	rovněž
odsouhlasil	odsouhlasit	k5eAaPmAgInS	odsouhlasit
oficiální	oficiální	k2eAgFnSc4d1	oficiální
změnu	změna	k1gFnSc4	změna
názvu	název	k1gInSc2	název
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
bude	být	k5eAaImBp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
předseda	předseda	k1gMnSc1	předseda
–	–	k?	–
Marek	Marek	k1gMnSc1	Marek
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
města	město	k1gNnSc2	město
Heřmanův	Heřmanův	k2eAgInSc4d1	Heřmanův
Městec	Městec	k1gInSc4	Městec
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
–	–	k?	–
Šárka	Šárka	k1gFnSc1	Šárka
Jelínková	Jelínková	k1gFnSc1	Jelínková
<g/>
,	,	kIx,	,
senátorka	senátorka	k1gFnSc1	senátorka
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
,	,	kIx,	,
zastupitelka	zastupitelka	k1gFnSc1	zastupitelka
Zlínského	zlínský	k2eAgInSc2d1	zlínský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místostarostka	místostarostka	k1gFnSc1	místostarostka
města	město	k1gNnSc2	město
Bystřice	Bystřice	k1gFnSc2	Bystřice
pod	pod	k7c7	pod
Hostýnem	Hostýn	k1gInSc7	Hostýn
</s>
</p>
<p>
<s>
místopředseda	místopředseda	k1gMnSc1	místopředseda
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Bartošek	Bartošek	k1gMnSc1	Bartošek
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
města	město	k1gNnSc2	město
Dačice	Dačice	k1gFnPc5	Dačice
</s>
</p>
<p>
<s>
místopředseda	místopředseda	k1gMnSc1	místopředseda
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
Hladík	Hladík	k1gMnSc1	Hladík
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
náměstek	náměstka	k1gFnPc2	náměstka
primátorky	primátorka	k1gFnSc2	primátorka
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
radní	radní	k1gMnSc1	radní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-sever	Brnoever	k1gMnSc1	Brno-sever
</s>
</p>
<p>
<s>
místopředseda	místopředseda	k1gMnSc1	místopředseda
–	–	k?	–
Štěpán	Štěpán	k1gMnSc1	Štěpán
Matek	matka	k1gFnPc2	matka
<g/>
,	,	kIx,	,
náměstek	náměstek	k1gMnSc1	náměstek
primátora	primátor	k1gMnSc2	primátor
města	město	k1gNnSc2	město
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
</s>
</p>
<p>
<s>
místopředseda	místopředseda	k1gMnSc1	místopředseda
–	–	k?	–
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Niemiec	Niemiec	k1gMnSc1	Niemiec
<g/>
,	,	kIx,	,
náměstek	náměstek	k1gMnSc1	náměstek
primátora	primátor	k1gMnSc2	primátor
města	město	k1gNnSc2	město
Havířov	Havířov	k1gInSc1	Havířov
</s>
</p>
<p>
<s>
předseda	předseda	k1gMnSc1	předseda
senátorského	senátorský	k2eAgInSc2d1	senátorský
klubu	klub	k1gInSc2	klub
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
Šilar	Šilar	k1gMnSc1	Šilar
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
generální	generální	k2eAgMnSc1d1	generální
sekretář	sekretář	k1gMnSc1	sekretář
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Hořava	Hořava	k1gMnSc1	Hořava
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
předsedů	předseda	k1gMnPc2	předseda
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
(	(	kIx(	(
<g/>
pověřen	pověřit	k5eAaPmNgMnS	pověřit
řízením	řízení	k1gNnSc7	řízení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
(	(	kIx(	(
<g/>
pověřen	pověřit	k5eAaPmNgMnS	pověřit
řízením	řízení	k1gNnSc7	řízení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Čunek	Čunek	k1gMnSc1	Čunek
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
<g/>
:	:	kIx,	:
Michaela	Michaela	k1gFnSc1	Michaela
Šojdrová	Šojdrový	k2eAgFnSc1d1	Šojdrová
(	(	kIx(	(
<g/>
pověřena	pověřit	k5eAaPmNgFnS	pověřit
řízením	řízení	k1gNnSc7	řízení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Bělobrádek	Bělobrádka	k1gFnPc2	Bělobrádka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Marek	Marek	k1gMnSc1	Marek
Výborný	Výborný	k1gMnSc1	Výborný
</s>
</p>
<p>
<s>
==	==	k?	==
Volební	volební	k2eAgInPc1d1	volební
výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
(	(	kIx(	(
<g/>
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
volby	volba	k1gFnPc1	volba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Sněmovní	sněmovní	k2eAgFnPc1d1	sněmovní
volby	volba	k1gFnPc1	volba
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
krajů	kraj	k1gInPc2	kraj
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
obcí	obec	k1gFnPc2	obec
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
====	====	k?	====
</s>
</p>
<p>
<s>
1	[number]	k4	1
V	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
s	s	k7c7	s
US-DEU	US-DEU	k1gFnSc7	US-DEU
</s>
</p>
<p>
<s>
2	[number]	k4	2
Čtyřkoalici	Čtyřkoalice	k1gFnSc6	Čtyřkoalice
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
<g/>
,	,	kIx,	,
Unie	unie	k1gFnSc1	unie
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
unie	unie	k1gFnSc1	unie
a	a	k8xC	a
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
aliance	aliance	k1gFnSc1	aliance
</s>
</p>
<p>
<s>
3	[number]	k4	3
4	[number]	k4	4
mandáty	mandát	k1gInPc7	mandát
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
se	s	k7c7	s
Stranou	strana	k1gFnSc7	strana
zelených	zelený	k2eAgMnPc2d1	zelený
</s>
</p>
<p>
<s>
4	[number]	k4	4
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
4	[number]	k4	4
mandáty	mandát	k1gInPc7	mandát
v	v	k7c6	v
koalicích	koalice	k1gFnPc6	koalice
</s>
</p>
<p>
<s>
5	[number]	k4	5
2	[number]	k4	2
mandáty	mandát	k1gInPc7	mandát
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
1	[number]	k4	1
mandát	mandát	k1gInSc1	mandát
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
</s>
</p>
<p>
<s>
==	==	k?	==
Poslanci	poslanec	k1gMnPc1	poslanec
a	a	k8xC	a
senátoři	senátor	k1gMnPc1	senátor
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
ČSL	ČSL	kA	ČSL
<g/>
/	/	kIx~	/
<g/>
KDU-ČSL	KDU-ČSL	k1gMnSc2	KDU-ČSL
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
a	a	k8xC	a
československých	československý	k2eAgInPc6d1	československý
zákonodárných	zákonodárný	k2eAgInPc6d1	zákonodárný
sborech	sbor	k1gInPc6	sbor
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Bařina	Bařina	k1gMnSc1	Bařina
(	(	kIx(	(
<g/>
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Bedřich	Bedřich	k1gMnSc1	Bedřich
Bezděk	Bezděk	k1gMnSc1	Bezděk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Čapka	Čapka	k1gMnSc1	Čapka
<g/>
,	,	kIx,	,
Ervín	Ervín	k1gMnSc1	Ervín
Červinka	Červinka	k1gMnSc1	Červinka
(	(	kIx(	(
<g/>
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
František	František	k1gMnSc1	František
Šabata	Šabata	k1gMnSc1	Šabata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Čuřík	Čuřík	k1gMnSc1	Čuřík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Dolanský	Dolanský	k1gMnSc1	Dolanský
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Mořic	Mořic	k1gMnSc1	Mořic
Hruban	Hruban	k1gMnSc1	Hruban
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kadlčák	Kadlčák	k1gMnSc1	Kadlčák
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kordač	Kordač	k1gMnSc1	Kordač
(	(	kIx(	(
<g/>
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
František	František	k1gMnSc1	František
Nosek	Nosek	k1gMnSc1	Nosek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Mašek	Mašek	k1gMnSc1	Mašek
(	(	kIx(	(
<g/>
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Alois	Alois	k1gMnSc1	Alois
Kaderka	Kaderka	k1gFnSc1	Kaderka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Mazanec	mazanec	k1gInSc1	mazanec
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Václav	Václav	k1gMnSc1	Václav
Myslivec	Myslivec	k1gMnSc1	Myslivec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rýpar	Rýpar	k1gMnSc1	Rýpar
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Cyril	Cyril	k1gMnSc1	Cyril
Stojan	stojan	k1gInSc1	stojan
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šamalík	Šamalík	k1gMnSc1	Šamalík
<g/>
,	,	kIx,	,
Vincenc	Vincenc	k1gMnSc1	Vincenc
Ševčík	Ševčík	k1gMnSc1	Ševčík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šolle	Šolle	k1gFnSc2	Šolle
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Valoušek	Valoušek	k1gMnSc1	Valoušek
<g/>
,	,	kIx,	,
Metod	Metod	k1gMnSc1	Metod
Zavoral	Zavoral	k1gMnSc1	Zavoral
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Zbořil	Zbořil	k1gMnSc1	Zbořil
(	(	kIx(	(
<g/>
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
František	František	k1gMnSc1	František
Mlčoch	Mlčoch	k1gMnSc1	Mlčoch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Adámek	Adámek	k1gMnSc1	Adámek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Adamovský	Adamovský	k1gMnSc1	Adamovský
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Bezděk	Bezděk	k1gMnSc1	Bezděk
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Čuřík	Čuřík	k1gMnSc1	Čuřík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Dolanský	Dolanský	k1gMnSc1	Dolanský
<g/>
,	,	kIx,	,
Mořic	Mořic	k1gMnSc1	Mořic
Hruban	Hruban	k1gMnSc1	Hruban
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Janalík	Janalík	k1gMnSc1	Janalík
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Kaderka	Kaderka	k1gFnSc1	Kaderka
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
zbaven	zbavit	k5eAaPmNgMnS	zbavit
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
<g />
.	.	kIx.	.
</s>
<s>
jej	on	k3xPp3gNnSc4	on
František	František	k1gMnSc1	František
Světlík	Světlík	k1gMnSc1	Světlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kopřiva	Kopřiva	k1gMnSc1	Kopřiva
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Košek	Košek	k1gMnSc1	Košek
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Mazanec	mazanec	k1gInSc1	mazanec
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Myslivec	Myslivec	k1gMnSc1	Myslivec
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Nosek	Nosek	k1gMnSc1	Nosek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Roudnický	roudnický	k2eAgMnSc1d1	roudnický
<g/>
,	,	kIx,	,
Augusta	Augusta	k1gMnSc1	Augusta
Rozsypalová	Rozsypalová	k1gFnSc1	Rozsypalová
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rýpar	Rýpar	k1gMnSc1	Rýpar
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šamalík	Šamalík	k1gMnSc1	Šamalík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
Milo	milo	k6eAd1	milo
Záruba	záruba	k1gFnSc1	záruba
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Jan	Jan	k1gMnSc1	Jan
Pěnkava	pěnkava	k1gFnSc1	pěnkava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kadlčák	Kadlčák	k1gMnSc1	Kadlčák
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Vincenc	Vincenc	k1gMnSc1	Vincenc
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Krupka	Krupka	k1gMnSc1	Krupka
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Procházka	Procházka	k1gMnSc1	Procházka
st.	st.	kA	st.
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Reyl	Reyl	k1gMnSc1	Reyl
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g />
.	.	kIx.	.
</s>
<s>
Cyril	Cyril	k1gMnSc1	Cyril
Stojan	stojan	k1gInSc1	stojan
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Koukal	Koukal	k1gMnSc1	Koukal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Šabata	Šabata	k1gMnSc1	Šabata
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Šachl	Šachl	k1gMnSc1	Šachl
<g/>
,	,	kIx,	,
Vincenc	Vincenc	k1gMnSc1	Vincenc
Ševčík	Ševčík	k1gMnSc1	Ševčík
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Alois	Alois	k1gMnSc1	Alois
Horák	Horák	k1gMnSc1	Horák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Valoušek	Valoušek	k1gMnSc1	Valoušek
<g/>
,	,	kIx,	,
Metod	Metod	k1gMnSc1	Metod
Zavoral	Zavoral	k1gMnSc1	Zavoral
<g/>
;	;	kIx,	;
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
z	z	k7c2	z
ČSS	ČSS	kA	ČSS
k	k	k7c3	k
ČSL	ČSL	kA	ČSL
Josef	Josef	k1gMnSc1	Josef
Holý	Holý	k1gMnSc1	Holý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1929	[number]	k4	1929
(	(	kIx(	(
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Adámek	Adámek	k1gMnSc1	Adámek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Bayer	Bayer	k1gMnSc1	Bayer
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Bezděk	Bezděk	k1gMnSc1	Bezděk
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Čančara	Čančara	k1gFnSc1	Čančara
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Čuřík	Čuřík	k1gMnSc1	Čuřík
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Dolanský	Dolanský	k1gMnSc1	Dolanský
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Dostálek	Dostálek	k1gMnSc1	Dostálek
<g/>
,	,	kIx,	,
Kašpar	Kašpar	k1gMnSc1	Kašpar
Hintermüller	Hintermüller	k1gMnSc1	Hintermüller
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Janalík	Janalík	k1gMnSc1	Janalík
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
Janovský	Janovský	k1gMnSc1	Janovský
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Kaňourek	kaňourek	k1gInSc1	kaňourek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Košek	Košek	k1gMnSc1	Košek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Matoušek	Matoušek	k1gMnSc1	Matoušek
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Mičura	Mičura	k1gFnSc1	Mičura
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Myslivec	Myslivec	k1gMnSc1	Myslivec
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Nosek	Nosek	k1gMnSc1	Nosek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Roudnický	roudnický	k2eAgMnSc1d1	roudnický
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rýpar	Rýpar	k1gMnSc1	Rýpar
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Stašek	Stašek	k1gMnSc1	Stašek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Světlík	Světlík	k1gMnSc1	Světlík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šamalík	Šamalík	k1gMnSc1	Šamalík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vičánek	Vičánek	k1gMnSc1	Vičánek
<g/>
,	,	kIx,	,
Augustin	Augustin	k1gMnSc1	Augustin
Vološin	Vološina	k1gFnPc2	Vološina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1929	[number]	k4	1929
(	(	kIx(	(
<g/>
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Antonín	Antonín	k1gMnSc1	Antonín
Adamovský	Adamovský	k1gMnSc1	Adamovský
<g/>
,	,	kIx,	,
Mořic	Mořic	k1gMnSc1	Mořic
Hruban	Hruban	k1gMnSc1	Hruban
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Kavan	Kavan	k1gMnSc1	Kavan
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kopřiva	Kopřiva	k1gMnSc1	Kopřiva
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Koukal	Koukal	k1gMnSc1	Koukal
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Krupka	Krupka	k1gMnSc1	Krupka
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Mazanec	mazanec	k1gInSc1	mazanec
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Artur	Artur	k1gMnSc1	Artur
Pavelka	Pavelka	k1gMnSc1	Pavelka
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Procházka	Procházka	k1gMnSc1	Procházka
st.	st.	kA	st.
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Reyl	Reyl	k1gMnSc1	Reyl
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Šabata	Šabata	k1gMnSc1	Šabata
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Šachl	Šachl	k1gMnSc1	Šachl
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Valoušek	Valoušek	k1gMnSc1	Valoušek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
(	(	kIx(	(
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Adámek	Adámek	k1gMnSc1	Adámek
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Bezděk	Bezděk	k1gMnSc1	Bezděk
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Dolanský	Dolanský	k1gMnSc1	Dolanský
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Dostálek	Dostálek	k1gMnSc1	Dostálek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Janalík	Janalík	k1gMnSc1	Janalík
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Kaňourek	kaňourek	k1gInSc1	kaňourek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Knotek	Knotek	k1gMnSc1	Knotek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Košek	Košek	k1gMnSc1	Košek
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Mičura	Mičur	k1gMnSc2	Mičur
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Myslivec	Myslivec	k1gMnSc1	Myslivec
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Augustin	Augustin	k1gMnSc1	Augustin
Čížek	Čížek	k1gMnSc1	Čížek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Nosek	Nosek	k1gMnSc1	Nosek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Roudnický	roudnický	k2eAgMnSc1d1	roudnický
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rýpar	Rýpar	k1gMnSc1	Rýpar
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Stašek	Stašek	k1gMnSc1	Stašek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Světlík	Světlík	k1gMnSc1	Světlík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šamalík	Šamalík	k1gMnSc1	Šamalík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vičánek	Vičánek	k1gMnSc1	Vičánek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
(	(	kIx(	(
<g/>
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Mořic	Mořic	k1gMnSc1	Mořic
Hruban	Hruban	k1gMnSc1	Hruban
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Kavan	Kavan	k1gMnSc1	Kavan
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kopřiva	Kopřiva	k1gMnSc1	Kopřiva
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Artur	Artur	k1gMnSc1	Artur
Pavelka	Pavelka	k1gMnSc1	Pavelka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Reyl	Reyl	k1gMnSc1	Reyl
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Šabata	Šabata	k1gMnSc1	Šabata
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Šachl	Šachl	k1gMnSc1	Šachl
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Valoušek	Valoušek	k1gMnSc1	Valoušek
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Leopold	Leopold	k1gMnSc1	Leopold
Koubek	Koubek	k1gMnSc1	Koubek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pavol	Pavol	k1gInSc4	Pavol
Žiška	Žišek	k1gInSc2	Žišek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Bayer	Bayer	k1gMnSc1	Bayer
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Bezděk	Bezděk	k1gMnSc1	Bezděk
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Brukner	Brukner	k1gMnSc1	Brukner
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Dostálek	Dostálek	k1gMnSc1	Dostálek
<g/>
,	,	kIx,	,
Kašpar	Kašpar	k1gMnSc1	Kašpar
Hintermüller	Hintermüller	k1gMnSc1	Hintermüller
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Hula	Hula	k1gMnSc1	Hula
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Janalík	Janalík	k1gMnSc1	Janalík
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Knotek	Knotek	k1gMnSc1	Knotek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Košek	Košek	k1gMnSc1	Košek
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Mičura	Mičura	k1gFnSc1	Mičura
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Otáhal	Otáhal	k1gMnSc1	Otáhal
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Řičář	Řičář	k1gMnSc1	Řičář
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Stašek	Stašek	k1gMnSc1	Stašek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Světlík	Světlík	k1gMnSc1	Světlík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šamalík	Šamalík	k1gMnSc1	Šamalík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vičánek	Vičánek	k1gMnSc1	Vičánek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Dytrych	Dytrych	k1gMnSc1	Dytrych
<g/>
,	,	kIx,	,
Mořic	Mořic	k1gMnSc1	Mořic
Hruban	Hruban	k1gMnSc1	Hruban
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
Janovský	Janovský	k1gMnSc1	Janovský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Pechanec	Pechanec	k1gMnSc1	Pechanec
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Roudnický	roudnický	k2eAgMnSc1d1	roudnický
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rýpar	Rýpar	k1gMnSc1	Rýpar
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Bedřich	Bedřich	k1gMnSc1	Bedřich
Brož	Brož	k1gMnSc1	Brož
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Sobota	sobota	k1gFnSc1	sobota
<g/>
,	,	kIx,	,
Pavol	Pavol	k1gInSc1	Pavol
Žiška	Žišek	k1gInSc2	Žišek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
:	:	kIx,	:
Štěpán	Štěpán	k1gMnSc1	Štěpán
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Brukner	Brukner	k1gMnSc1	Brukner
<g/>
,	,	kIx,	,
Bohumír	Bohumír	k1gMnSc1	Bohumír
Bunža	Bunža	k1gMnSc1	Bunža
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Červenka	Červenka	k1gMnSc1	Červenka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Desenský	Desenský	k2eAgMnSc1d1	Desenský
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Ducháček	Ducháček	k1gMnSc1	Ducháček
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Fránek	Fránek	k1gMnSc1	Fránek
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Fusek	Fusek	k1gMnSc1	Fusek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Hála	Hála	k1gMnSc1	Hála
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Holcman	Holcman	k1gMnSc1	Holcman
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hořínek	Hořínek	k1gMnSc1	Hořínek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Cyril	Cyril	k1gMnSc1	Cyril
Charvát	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Chomutovský	chomutovský	k2eAgMnSc1d1	chomutovský
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Chytil	Chytil	k1gMnSc1	Chytil
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kaďůrek	Kaďůrek	k1gInSc1	Kaďůrek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kellner	Kellner	k1gMnSc1	Kellner
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Klimek	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Konvalina	Konvalina	k1gMnSc1	Konvalina
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Kostelka	Kostelka	k1gMnSc1	Kostelka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Limpouch	Limpouch	k1gMnSc1	Limpouch
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Moravec	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Nermuť	rmoutit	k5eNaImRp2nS	rmoutit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
Pavlík	Pavlík	k1gMnSc1	Pavlík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Peňás	Peňás	k1gInSc1	Peňás
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Plojhar	Plojhar	k1gMnSc1	Plojhar
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Polomský	Polomský	k2eAgMnSc1d1	Polomský
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Procházka	Procházka	k1gMnSc1	Procházka
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Rolek	Rolek	k1gMnSc1	Rolek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Řehulka	Řehulka	k1gFnSc1	Řehulka
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Skuhrovcová	Skuhrovcový	k2eAgFnSc1d1	Skuhrovcová
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Soukup	Soukup	k1gMnSc1	Soukup
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Sova	Sova	k1gMnSc1	Sova
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Synek	Synek	k1gMnSc1	Synek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šmehlík	Šmehlík	k1gMnSc1	Šmehlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Štambachr	Štambachr	k1gMnSc1	Štambachr
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Trojanová	Trojanová	k1gFnSc1	Trojanová
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Vrána	Vrána	k1gMnSc1	Vrána
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vičánek	Vičánek	k1gMnSc1	Vičánek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Víšek	Víšek	k1gMnSc1	Víšek
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Vojanec	Vojanec	k1gMnSc1	Vojanec
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Vošahlík	Vošahlík	k1gMnSc1	Vošahlík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
:	:	kIx,	:
Štěpán	Štěpán	k1gMnSc1	Štěpán
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Broj	broj	k1gFnSc1	broj
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Brukner	Brukner	k1gMnSc1	Brukner
<g/>
,	,	kIx,	,
Bohumír	Bohumír	k1gMnSc1	Bohumír
Bunža	Bunža	k1gMnSc1	Bunža
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Desenský	Desenský	k2eAgMnSc1d1	Desenský
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Ducháček	Ducháček	k1gMnSc1	Ducháček
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Fránek	Fránek	k1gMnSc1	Fránek
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Fusek	Fusek	k1gMnSc1	Fusek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Hála	Hála	k1gMnSc1	Hála
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hořínek	Hořínek	k1gMnSc1	Hořínek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
Charvát	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Bohdan	Bohdan	k1gMnSc1	Bohdan
Chudoba	Chudoba	k1gMnSc1	Chudoba
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Chytil	Chytil	k1gMnSc1	Chytil
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kaďůrek	Kaďůrek	k1gInSc1	Kaďůrek
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Klimek	Klimek	k1gMnSc1	Klimek
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Klimek	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Konvalina	Konvalina	k1gMnSc1	Konvalina
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
Koželuhová	Koželuhová	k1gFnSc1	Koželuhová
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
zbavena	zbavit	k5eAaPmNgFnS	zbavit
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ji	on	k3xPp3gFnSc4	on
Josef	Josef	k1gMnSc1	Josef
Herl	Herl	k1gMnSc1	Herl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Matýsek	Matýsek	k1gMnSc1	Matýsek
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Nermuť	rmoutit	k5eNaImRp2nS	rmoutit
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Norbert	Norbert	k1gMnSc1	Norbert
Pexa	Pexa	k1gMnSc1	Pexa
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Plesl	Plesl	k1gInSc1	Plesl
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Plojhar	Plojhar	k1gMnSc1	Plojhar
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Polomský	Polomský	k2eAgMnSc1d1	Polomský
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Procházka	Procházka	k1gMnSc1	Procházka
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Rozehnal	Rozehnal	k1gMnSc1	Rozehnal
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Řehulka	Řehulka	k1gFnSc1	Řehulka
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
Sochorec	Sochorec	k1gMnSc1	Sochorec
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Václav	Václav	k1gMnSc1	Václav
Batěk	Batěk	k1gMnSc1	Batěk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Soukup	Soukup	k1gMnSc1	Soukup
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Sova	Sova	k1gMnSc1	Sova
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Synek	Synek	k1gMnSc1	Synek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Jaromír	Jaromír	k1gMnSc1	Jaromír
Berák	Berák	k1gMnSc1	Berák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Štambachr	Štambachr	k1gMnSc1	Štambachr
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Tomášek	Tomášek	k1gMnSc1	Tomášek
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Trojanová	Trojanová	k1gFnSc1	Trojanová
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Tylínek	Tylínek	k1gMnSc1	Tylínek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vičánek	Vičánek	k1gMnSc1	Vičánek
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Vojanec	Vojanec	k1gMnSc1	Vojanec
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Vošahlík	Vošahlík	k1gMnSc1	Vošahlík
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
František	František	k1gMnSc1	František
Zvěřina	Zvěřina	k1gMnSc1	Zvěřina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Batěk	Batěk	k1gMnSc1	Batěk
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Václav	Václav	k1gMnSc1	Václav
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Berák	Berák	k1gMnSc1	Berák
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Gemrot	Gemrot	k1gMnSc1	Gemrot
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Chytil	Chytil	k1gMnSc1	Chytil
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgInS	nahradit
jej	on	k3xPp3gMnSc4	on
Jarolím	Jarole	k1gFnPc3	Jarole
Leichman	Leichman	k1gMnSc1	Leichman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kaďůrek	Kaďůrek	k1gInSc1	Kaďůrek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Myška	Myška	k1gMnSc1	Myška
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Niederle	Niederle	k1gFnSc2	Niederle
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
zbaven	zbavit	k5eAaPmNgMnS	zbavit
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
neobsazeno	obsadit	k5eNaPmNgNnS	obsadit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
František	František	k1gMnSc1	František
Urban	Urban	k1gMnSc1	Urban
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Norbert	Norbert	k1gMnSc1	Norbert
<g />
.	.	kIx.	.
</s>
<s>
Pexa	Pexa	k1gMnSc1	Pexa
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Plesl	Plesl	k1gInSc1	Plesl
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Antonín	Antonín	k1gMnSc1	Antonín
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Plojhar	Plojhar	k1gMnSc1	Plojhar
<g/>
,	,	kIx,	,
Dionysius	Dionysius	k1gMnSc1	Dionysius
Polanský	Polanský	k1gMnSc1	Polanský
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Polišenský	Polišenský	k2eAgMnSc1d1	Polišenský
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Engelbert	Engelbert	k1gMnSc1	Engelbert
Toman	Toman	k1gMnSc1	Toman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Prokeš	Prokeš	k1gMnSc1	Prokeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Sedlák	Sedlák	k1gMnSc1	Sedlák
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Sláva	Sláva	k1gMnSc1	Sláva
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Sova	Sova	k1gMnSc1	Sova
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Vodáček	Vodáček	k1gMnSc1	Vodáček
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
<g/>
,	,	kIx,	,
nahradila	nahradit	k5eAaPmAgFnS	nahradit
jej	on	k3xPp3gInSc4	on
Marie	Marie	k1gFnSc1	Marie
Šachová	šachový	k2eAgFnSc1d1	šachová
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
<g/>
,	,	kIx,	,
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
Anděla	Anděla	k1gFnSc1	Anděla
Sukupová	Sukupová	k1gFnSc1	Sukupová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Vojanec	Vojanec	k1gMnSc1	Vojanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
:	:	kIx,	:
Jaromír	Jaromír	k1gMnSc1	Jaromír
Berák	Berák	k1gMnSc1	Berák
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Gemrot	Gemrot	k1gMnSc1	Gemrot
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hronovský	hronovský	k2eAgMnSc1d1	hronovský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Illa	Illa	k1gMnSc1	Illa
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Kotáb	Kotáb	k1gMnSc1	Kotáb
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kouba	Kouba	k1gMnSc1	Kouba
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Leška	Leška	k1gMnSc1	Leška
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Myška	Myška	k1gMnSc1	Myška
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Plojhar	Plojhar	k1gMnSc1	Plojhar
<g/>
,	,	kIx,	,
Dionysius	Dionysius	k1gMnSc1	Dionysius
Polanský	Polanský	k1gMnSc1	Polanský
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rovný	rovný	k2eAgMnSc1d1	rovný
<g/>
,	,	kIx,	,
Anděla	Anděla	k1gFnSc1	Anděla
Sukupová	Sukupová	k1gFnSc1	Sukupová
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Škoda	Škoda	k1gMnSc1	Škoda
<g/>
,	,	kIx,	,
Engelbert	Engelbert	k1gMnSc1	Engelbert
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Vorel	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Závěta	Závěta	k1gMnSc1	Závěta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Zedník	Zedník	k1gMnSc1	Zedník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
:	:	kIx,	:
Jaromír	Jaromír	k1gMnSc1	Jaromír
Berák	Berák	k1gMnSc1	Berák
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Coufal	Coufal	k1gMnSc1	Coufal
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Gemrot	Gemrot	k1gMnSc1	Gemrot
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Holáň	Holáň	k1gFnSc1	Holáň
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hronovský	hronovský	k2eAgMnSc1d1	hronovský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Illa	Illa	k1gMnSc1	Illa
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kouba	Kouba	k1gMnSc1	Kouba
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Plojhar	Plojhar	k1gMnSc1	Plojhar
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
Sýkora	Sýkora	k1gMnSc1	Sýkora
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Škoda	Škoda	k1gMnSc1	Škoda
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Volák	Volák	k1gMnSc1	Volák
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Závěta	Závěta	k1gMnSc1	Závěta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Zedník	Zedník	k1gMnSc1	Zedník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
:	:	kIx,	:
Jaromír	Jaromír	k1gMnSc1	Jaromír
Berák	Berák	k1gMnSc1	Berák
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Václav	Václav	k1gMnSc1	Václav
Červený	Červený	k1gMnSc1	Červený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Coufal	Coufal	k1gMnSc1	Coufal
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Gemrot	Gemrot	k1gMnSc1	Gemrot
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Holáň	Holáň	k1gFnSc1	Holáň
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hronovský	hronovský	k2eAgMnSc1d1	hronovský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Illa	Illa	k1gMnSc1	Illa
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Koníček	Koníček	k1gMnSc1	Koníček
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kouba	Kouba	k1gMnSc1	Kouba
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Míková	Míková	k1gFnSc1	Míková
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Pacner	Pacner	k1gMnSc1	Pacner
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Plojhar	Plojhar	k1gMnSc1	Plojhar
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Rejhon	Rejhon	k1gMnSc1	Rejhon
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
Spěváková	Spěváková	k1gFnSc1	Spěváková
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Škoda	Škoda	k1gMnSc1	Škoda
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Závěta	Závěta	k1gMnSc1	Závěta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Zedník	Zedník	k1gMnSc1	Zedník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
(	(	kIx(	(
<g/>
FS	FS	kA	FS
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Stanislav	Stanislav	k1gMnSc1	Stanislav
Brada	Brada	k1gMnSc1	Brada
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Coufal	Coufal	k1gMnSc1	Coufal
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Červený	Červený	k1gMnSc1	Červený
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Dutková	Dutková	k1gFnSc1	Dutková
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Gemrot	Gemrot	k1gMnSc1	Gemrot
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Holáň	Holáň	k1gFnSc1	Holáň
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hronovský	hronovský	k2eAgMnSc1d1	hronovský
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Chytil	Chytil	k1gMnSc1	Chytil
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g />
.	.	kIx.	.
</s>
<s>
Illa	Illa	k1gMnSc1	Illa
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Kabelka	Kabelka	k1gMnSc1	Kabelka
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Koníček	Koníček	k1gMnSc1	Koníček
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kouba	Kouba	k1gMnSc1	Kouba
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Míková	Míková	k1gFnSc1	Míková
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Pacner	Pacner	k1gMnSc1	Pacner
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Pauly	Paula	k1gFnSc2	Paula
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g />
.	.	kIx.	.
</s>
<s>
Plojhar	Plojhar	k1gMnSc1	Plojhar
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Rejhon	Rejhon	k1gMnSc1	Rejhon
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
Spěváková	Spěváková	k1gFnSc1	Spěváková
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Srb	Srb	k1gMnSc1	Srb
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Škoda	Škoda	k1gMnSc1	Škoda
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Turek	Turek	k1gMnSc1	Turek
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vybral	vybrat	k5eAaPmAgMnS	vybrat
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Závěta	Závěta	k1gMnSc1	Závěta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Zedník	Zedník	k1gMnSc1	Zedník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1971	[number]	k4	1971
byli	být	k5eAaImAgMnP	být
novými	nový	k2eAgMnPc7d1	nový
členy	člen	k1gMnPc7	člen
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hudeček	Hudeček	k1gMnSc1	Hudeček
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
Petera	Petera	k1gMnSc1	Petera
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Šebera	Šebera	k1gFnSc1	Šebera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
(	(	kIx(	(
<g/>
ČNR	ČNR	kA	ČNR
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bosák	Bosák	k1gMnSc1	Bosák
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Brada	Brada	k1gMnSc1	Brada
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Coufal	Coufal	k1gMnSc1	Coufal
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Dutková	Dutková	k1gFnSc1	Dutková
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hudeček	Hudeček	k1gMnSc1	Hudeček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hübner	Hübner	k1gMnSc1	Hübner
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Chytil	Chytil	k1gMnSc1	Chytil
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1970	[number]	k4	1970
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Kabelka	Kabelka	k1gMnSc1	Kabelka
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
Mička	Mička	k1gMnSc1	Mička
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Pařízek	Pařízek	k1gMnSc1	Pařízek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Pauly	Paula	k1gFnSc2	Paula
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Rubín	rubín	k1gInSc1	rubín
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Sluka	Sluka	k1gMnSc1	Sluka
<g/>
,	,	kIx,	,
Liběna	Liběna	k1gFnSc1	Liběna
Snížková	Snížkový	k2eAgFnSc1d1	Snížková
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1969	[number]	k4	1969
zbavena	zbaven	k2eAgFnSc1d1	zbavena
mandátu	mandát	k1gInSc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Srb	Srb	k1gMnSc1	Srb
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Škrobánek	Škrobánek	k1gMnSc1	Škrobánek
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Touška	Toušek	k1gMnSc2	Toušek
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Turek	Turek	k1gMnSc1	Turek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Válek	Válek	k1gMnSc1	Válek
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vybral	vybrat	k5eAaPmAgMnS	vybrat
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Vyhnálek	Vyhnálek	k1gMnSc1	Vyhnálek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1969	[number]	k4	1969
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
doplňovací	doplňovací	k2eAgFnSc6d1	doplňovací
volbě	volba	k1gFnSc6	volba
62	[number]	k4	62
nových	nový	k2eAgMnPc2d1	nový
poslanců	poslanec	k1gMnPc2	poslanec
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Chytráček	chytráček	k1gMnSc1	chytráček
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Šebera	Šebero	k1gNnSc2	Šebero
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1970	[number]	k4	1970
pak	pak	k6eAd1	pak
Rostislav	Rostislav	k1gMnSc1	Rostislav
Petera	Petera	k1gMnSc1	Petera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
(	(	kIx(	(
<g/>
FS	FS	kA	FS
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Frynta	Frynta	k1gMnSc1	Frynta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Gratcl	Gratcl	k1gMnSc1	Gratcl
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hudeček	Hudeček	k1gMnSc1	Hudeček
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kopřiva	Kopřiva	k1gMnSc1	Kopřiva
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Krauskopf	Krauskopf	k1gMnSc1	Krauskopf
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
Petera	Petera	k1gMnSc1	Petera
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Plojhar	Plojhar	k1gMnSc1	Plojhar
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Rejhon	Rejhon	k1gMnSc1	Rejhon
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Srb	Srb	k1gMnSc1	Srb
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Tichý	Tichý	k1gMnSc1	Tichý
<g/>
,	,	kIx,	,
Terezie	Terezie	k1gFnSc1	Terezie
Vlčková	Vlčková	k1gFnSc1	Vlčková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
(	(	kIx(	(
<g/>
ČNR	ČNR	kA	ČNR
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Miroslav	Miroslav	k1gMnSc1	Miroslav
Drahota	drahota	k1gFnSc1	drahota
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Chytráček	chytráček	k1gMnSc1	chytráček
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Král	Král	k1gMnSc1	Král
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kudela	Kudela	k1gFnSc1	Kudela
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
Kuklínek	Kuklínek	k1gMnSc1	Kuklínek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Pařízek	Pařízek	k1gMnSc1	Pařízek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Řepka	řepka	k1gFnSc1	řepka
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Sluka	Sluka	k1gMnSc1	Sluka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Šebera	Šebero	k1gNnSc2	Šebero
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Vašků	Vašek	k1gMnPc2	Vašek
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Žalman	Žalman	k1gMnSc1	Žalman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
(	(	kIx(	(
<g/>
FS	FS	kA	FS
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Andrš	Andrš	k1gMnSc1	Andrš
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Fejfuša	Fejfuša	k1gMnSc1	Fejfuša
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Formanová	Formanová	k1gFnSc1	Formanová
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hudeček	Hudeček	k1gMnSc1	Hudeček
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kopřiva	Kopřiva	k1gMnSc1	Kopřiva
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Krauskopf	Krauskopf	k1gMnSc1	Krauskopf
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
Petera	Petera	k1gMnSc1	Petera
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Plojhar	Plojhar	k1gMnSc1	Plojhar
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Protiva	protiva	k1gFnSc1	protiva
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rábel	Rábel	k1gMnSc1	Rábel
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Sodoma	Sodoma	k1gFnSc1	Sodoma
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Srb	Srb	k1gMnSc1	Srb
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Tichý	Tichý	k1gMnSc1	Tichý
<g/>
,	,	kIx,	,
Terezie	Terezie	k1gFnSc1	Terezie
Vlčková	Vlčková	k1gFnSc1	Vlčková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
(	(	kIx(	(
<g/>
ČNR	ČNR	kA	ČNR
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Chytráček	chytráček	k1gMnSc1	chytráček
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
Kuklínek	Kuklínek	k1gMnSc1	Kuklínek
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kuna	Kuna	k1gMnSc1	Kuna
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Meier	Meier	k1gMnSc1	Meier
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Měřička	měřička	k1gFnSc1	měřička
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Pařízek	Pařízek	k1gMnSc1	Pařízek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Řepka	řepka	k1gFnSc1	řepka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Vápeník	Vápeník	k1gMnSc1	Vápeník
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Vašků	Vašek	k1gMnPc2	Vašek
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gNnSc4	on
Jan	Jan	k1gMnSc1	Jan
Břicháček	Břicháček	k?	Břicháček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Vávrů	Vávrů	k1gMnSc1	Vávrů
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Žalman	Žalman	k1gMnSc1	Žalman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
FS	FS	kA	FS
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Andrš	Andrš	k1gMnSc1	Andrš
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Formanová	Formanová	k1gFnSc1	Formanová
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hudeček	Hudeček	k1gMnSc1	Hudeček
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kopřiva	Kopřiva	k1gMnSc1	Kopřiva
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Krauskopf	Krauskopf	k1gMnSc1	Krauskopf
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Laštovička	Laštovička	k1gMnSc1	Laštovička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Plojhar	Plojhar	k1gMnSc1	Plojhar
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
+	+	kIx~	+
<g/>
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Josef	Josef	k1gMnSc1	Josef
Bartončík	Bartončík	k1gMnSc1	Bartončík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Protiva	protiva	k1gFnSc1	protiva
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rábel	Rábel	k1gMnSc1	Rábel
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Servus	Servus	k?	Servus
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Sodoma	Sodoma	k1gFnSc1	Sodoma
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Srb	Srb	k1gMnSc1	Srb
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Tichý	Tichý	k1gMnSc1	Tichý
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Toman	Toman	k1gMnSc1	Toman
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Stanislav	Stanislav	k1gMnSc1	Stanislav
Toms	Toms	k1gInSc1	Toms
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
ČNR	ČNR	kA	ČNR
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Anděl	Anděl	k1gMnSc1	Anděl
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Chytráček	chytráček	k1gMnSc1	chytráček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kolařík	Kolařík	k1gMnSc1	Kolařík
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kozák	Kozák	k1gMnSc1	Kozák
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Krystyník	Krystyník	k1gMnSc1	Krystyník
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kuna	Kuna	k1gMnSc1	Kuna
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Meier	Meier	k1gMnSc1	Meier
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Měřička	měřička	k1gFnSc1	měřička
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Vávrů	Vávrů	k1gMnSc1	Vávrů
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Vrbka	Vrbka	k1gMnSc1	Vrbka
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Žalman	Žalman	k1gMnSc1	Žalman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
FS	FS	kA	FS
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Andrš	Andrš	k1gMnSc1	Andrš
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Bartončík	Bartončík	k1gMnSc1	Bartončík
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
Bartošková	Bartošková	k1gFnSc1	Bartošková
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Haičman	Haičman	k1gMnSc1	Haičman
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Milan	Milan	k1gMnSc1	Milan
Andrýsek	Andrýsek	k1gMnSc1	Andrýsek
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Hanák	Hanák	k1gMnSc1	Hanák
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Havlíček	Havlíček	k1gMnSc1	Havlíček
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
odvolán	odvolán	k2eAgInSc1d1	odvolán
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Josef	Josef	k1gMnSc1	Josef
Macek	Macek	k1gMnSc1	Macek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hlavačka	hlavačka	k1gFnSc1	hlavačka
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Krauskopf	Krauskopf	k1gMnSc1	Krauskopf
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Laštovička	Laštovička	k1gMnSc1	Laštovička
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Marešová	Marešová	k1gFnSc1	Marešová
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
odvolána	odvolat	k5eAaPmNgFnS	odvolat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ji	on	k3xPp3gFnSc4	on
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Cuhra	Cuhra	k1gMnSc1	Cuhra
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rábel	Rábel	k1gMnSc1	Rábel
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Servus	Servus	k?	Servus
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šimek	Šimek	k1gMnSc1	Šimek
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
odvolán	odvolán	k2eAgInSc1d1	odvolán
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Miroslav	Miroslav	k1gMnSc1	Miroslav
Téra	Téra	k1gMnSc1	Téra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Tichý	Tichý	k1gMnSc1	Tichý
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
odvolán	odvolán	k2eAgInSc1d1	odvolán
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Jiří	Jiří	k1gMnSc1	Jiří
Medřický	Medřický	k2eAgMnSc1d1	Medřický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Toms	Toms	k1gInSc1	Toms
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Jan	Jan	k1gMnSc1	Jan
Vácha	Vácha	k1gMnSc1	Vácha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Veselá	Veselá	k1gFnSc1	Veselá
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
odvolána	odvolat	k5eAaPmNgFnS	odvolat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ji	on	k3xPp3gFnSc4	on
Miroslav	Miroslav	k1gMnSc1	Miroslav
Lajkep	Lajkep	k1gMnSc1	Lajkep
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Žalman	Žalman	k1gMnSc1	Žalman
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Miroslav	Miroslav	k1gMnSc1	Miroslav
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
ČNR	ČNR	kA	ČNR
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Anděl	Anděl	k1gMnSc1	Anděl
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Bekárek	Bekárek	k1gMnSc1	Bekárek
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
/	/	kIx~	/
<g/>
odvolán	odvolán	k2eAgMnSc1d1	odvolán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kaňa	Kaňa	k1gMnSc1	Kaňa
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Karbaš	Karbaš	k1gMnSc1	Karbaš
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kolařík	Kolařík	k1gMnSc1	Kolařík
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kozák	Kozák	k1gMnSc1	Kozák
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Krystyník	Krystyník	k1gMnSc1	Krystyník
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kuna	Kuna	k1gMnSc1	Kuna
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Meier	Meira	k1gFnPc2	Meira
<g/>
,	,	kIx,	,
Květoslav	Květoslava	k1gFnPc2	Květoslava
Pazderník	pazderník	k1gMnSc1	pazderník
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
/	/	kIx~	/
<g/>
odvolán	odvolán	k2eAgMnSc1d1	odvolán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Samek	Samek	k1gMnSc1	Samek
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Sobotová	Sobotová	k1gFnSc1	Sobotová
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Škrobánek	Škrobánek	k1gMnSc1	Škrobánek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Vrbka	Vrbka	k1gMnSc1	Vrbka
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Žalman	Žalman	k1gMnSc1	Žalman
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
byli	být	k5eAaImAgMnP	být
novými	nový	k2eAgMnPc7d1	nový
poslanci	poslanec	k1gMnPc7	poslanec
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
Ivana	Ivan	k1gMnSc2	Ivan
Janů	Janů	k2eAgMnPc2d1	Janů
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Pavela	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Rubín	rubín	k1gInSc1	rubín
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šedivý	Šedivý	k1gMnSc1	Šedivý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
FS	FS	kA	FS
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Bartončík	Bartončík	k1gMnSc1	Bartončík
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Benda	Benda	k1gMnSc1	Benda
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Miroslav	Miroslav	k1gMnSc1	Miroslav
Téra	Téra	k1gMnSc1	Téra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Cuhra	Cuhra	k1gMnSc1	Cuhra
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Fričar	Fričar	k1gMnSc1	Fričar
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Hanák	Hanák	k1gMnSc1	Hanák
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Kaplanová	Kaplanová	k1gFnSc1	Kaplanová
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Michálek	Michálek	k1gMnSc1	Michálek
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Motyčka	Motyčka	k1gMnSc1	Motyčka
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Sacher	Sachra	k1gFnPc2	Sachra
<g/>
,	,	kIx,	,
Irena	Irena	k1gFnSc1	Irena
Šimečková	Šimečková	k1gFnSc1	Šimečková
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Šlápota	šlápota	k1gFnSc1	šlápota
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Tomsa	Toms	k1gMnSc2	Toms
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Tyl	Tyl	k1gMnSc1	Tyl
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Václav	Václav	k1gMnSc1	Václav
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Kaplanová	Kaplanová	k1gFnSc1	Kaplanová
<g/>
,	,	kIx,	,
Irena	Irena	k1gFnSc1	Irena
Šimečková	Šimečková	k1gFnSc1	Šimečková
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
,	,	kIx,	,
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
pak	pak	k6eAd1	pak
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
FS	FS	kA	FS
nevstoupil	vstoupit	k5eNaPmAgMnS	vstoupit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Téra	Téra	k1gMnSc1	Téra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
ČNR	ČNR	kA	ČNR
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bělehrádek	Bělehrádka	k1gFnPc2	Bělehrádka
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Cibuzar	Cibuzar	k1gMnSc1	Cibuzar
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Dušek	Dušek	k1gMnSc1	Dušek
(	(	kIx(	(
<g/>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
rolnická	rolnický	k2eAgFnSc1d1	rolnická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Chutný	chutný	k2eAgMnSc1d1	chutný
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
Janů	Janů	k1gMnSc1	Janů
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Karas	Karas	k1gMnSc1	Karas
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kubiš	Kubiš	k1gMnSc1	Kubiš
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Moskal	Moskal	k?	Moskal
(	(	kIx(	(
<g/>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
rolnická	rolnický	k2eAgFnSc1d1	rolnická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Pavela	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Procházka	Procházka	k1gMnSc1	Procházka
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Smělík	Smělík	k1gMnSc1	Smělík
(	(	kIx(	(
<g/>
MOH	MOH	k?	MOH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Strašek	strašek	k1gMnSc1	strašek
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
-	-	kIx~	-
1991	[number]	k4	1991
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Josef	Josef	k1gMnSc1	Josef
Meier	Meier	k1gMnSc1	Meier
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šedivý	Šedivý	k1gMnSc1	Šedivý
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Tollner	Tollner	k1gMnSc1	Tollner
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Wajsar	Wajsar	k1gMnSc1	Wajsar
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Marek	Marek	k1gMnSc1	Marek
Benda	Benda	k1gMnSc1	Benda
(	(	kIx(	(
<g/>
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c2	za
OF	OF	kA	OF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
někteří	některý	k3yIgMnPc1	některý
poslanci	poslanec	k1gMnPc1	poslanec
za	za	k7c4	za
KDS	KDS	kA	KDS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
FS	FS	kA	FS
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Borák	borák	k1gMnSc1	borák
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Cuhra	Cuhra	k1gMnSc1	Cuhra
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Čupr	čupr	k2eAgMnSc1d1	čupr
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Foltýn	Foltýn	k1gMnSc1	Foltýn
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
Holáň	Holáň	k1gFnSc1	Holáň
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Jajtner	Jajtner	k1gMnSc1	Jajtner
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Koutský	Koutský	k1gMnSc1	Koutský
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Krist	Krista	k1gFnPc2	Krista
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Pavel	Pavel	k1gMnSc1	Pavel
Šmíd	Šmíd	k1gMnSc1	Šmíd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Orel	Orel	k1gMnSc1	Orel
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Smělík	Smělík	k1gMnSc1	Smělík
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Šustr	Šustr	k1gMnSc1	Šustr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
ČNR	ČNR	kA	ČNR
<g/>
/	/	kIx~	/
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bělehrádek	Bělehrádka	k1gFnPc2	Bělehrádka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Ivana	Ivan	k1gMnSc2	Ivan
Janů	Janů	k1gMnSc2	Janů
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ji	on	k3xPp3gFnSc4	on
Jan	Jan	k1gMnSc1	Jan
Decker	Decker	k1gMnSc1	Decker
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Krupík	Krupík	k1gMnSc1	Krupík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kubiš	Kubiš	k1gMnSc1	Kubiš
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Mach	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Motyčka	Motyčka	k1gMnSc1	Motyčka
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Pavela	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Severa	Severa	k1gMnSc1	Severa
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Šafařík	Šafařík	k1gMnSc1	Šafařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Uřičář	Uřičář	k1gMnSc1	Uřičář
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
Jan	Jan	k1gMnSc1	Jan
Vraný	vraný	k2eAgMnSc1d1	vraný
(	(	kIx(	(
<g/>
zvolen	zvolen	k2eAgMnSc1d1	zvolen
za	za	k7c2	za
LSU	LSU	kA	LSU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
pak	pak	k6eAd1	pak
Jiří	Jiří	k1gMnSc1	Jiří
Haringer	Haringer	k1gMnSc1	Haringer
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Koronthály	Koronthála	k1gFnSc2	Koronthála
<g/>
,	,	kIx,	,
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Tollner	Tollner	k1gMnSc1	Tollner
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
(	(	kIx(	(
<g/>
členové	člen	k1gMnPc1	člen
KDS	KDS	kA	KDS
zvolení	zvolený	k2eAgMnPc1d1	zvolený
ODS-KDS	ODS-KDS	k1gMnPc1	ODS-KDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kozák	Kozák	k1gMnSc1	Kozák
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Vačkář	Vačkář	k1gMnSc1	Vačkář
(	(	kIx(	(
<g/>
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
za	za	k7c4	za
LSU	LSU	kA	LSU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Hrdý	Hrdý	k1gMnSc1	Hrdý
(	(	kIx(	(
<g/>
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c2	za
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1996-1998	[number]	k4	1996-1998
(	(	kIx(	(
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Libor	Libor	k1gMnSc1	Libor
Ambrozek	Ambrozka	k1gFnPc2	Ambrozka
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bělehrádek	Bělehrádek	k1gInSc1	Bělehrádek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Borák	borák	k1gMnSc1	borák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Grůza	Grůza	k1gFnSc1	Grůza
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
Holáň	Holáň	k1gFnSc1	Holáň
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
Jurková	Jurková	k1gFnSc1	Jurková
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ji	on	k3xPp3gFnSc4	on
Vladimír	Vladimír	k1gMnSc1	Vladimír
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
<g/>
,	,	kIx,	,
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Mach	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Orel	Orel	k1gMnSc1	Orel
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Severa	Severa	k1gMnSc1	Severa
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Šafařík	Šafařík	k1gMnSc1	Šafařík
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
Šojdrová	Šojdrová	k1gFnSc1	Šojdrová
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Talíř	Talíř	k1gMnSc1	Talíř
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Tollner	Tollner	k1gMnSc1	Tollner
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	s	k7c7	s
členem	člen	k1gInSc7	člen
klubu	klub	k1gInSc2	klub
stal	stát	k5eAaPmAgMnS	stát
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
Fójcik	Fójcik	k1gMnSc1	Fójcik
(	(	kIx(	(
<g/>
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c2	za
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1996-1998	[number]	k4	1996-1998
(	(	kIx(	(
<g/>
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Brýdl	Brýdl	k1gMnSc1	Brýdl
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Čada	Čada	k1gMnSc1	Čada
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Dočekal	Dočekal	k1gMnSc1	Dočekal
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Petřík	Petřík	k1gMnSc1	Petřík
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc1	Pithart
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Stodůlka	stodůlka	k1gFnSc1	stodůlka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šenkýř	Šenkýř	k1gMnSc1	Šenkýř
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Škrabiš	Škrabiš	k1gMnSc1	Škrabiš
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Špaček	Špaček	k1gMnSc1	Špaček
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Pavel	Pavel	k1gMnSc1	Pavel
Heřman	Heřman	k1gMnSc1	Heřman
(	(	kIx(	(
<g/>
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c2	za
DEU	DEU	kA	DEU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1998-2002	[number]	k4	1998-2002
(	(	kIx(	(
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Libor	Libor	k1gMnSc1	Libor
Ambrozek	Ambrozka	k1gFnPc2	Ambrozka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Grůza	Grůza	k1gFnSc1	Grůza
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
Holáň	Holáň	k1gFnSc1	Holáň
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kvapil	Kvapil	k1gMnSc1	Kvapil
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Jiří	Jiří	k1gMnSc1	Jiří
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
Müllerová	Müllerová	k1gFnSc1	Müllerová
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Parkanová	Parkanová	k1gFnSc1	Parkanová
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Severa	Severa	k1gMnSc1	Severa
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Šafařík	Šafařík	k1gMnSc1	Šafařík
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
Šojdrová	Šojdrová	k1gFnSc1	Šojdrová
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šustr	Šustr	k1gMnSc1	Šustr
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Talíř	Talíř	k1gMnSc1	Talíř
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Tollner	Tollner	k1gMnSc1	Tollner
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Pavel	Pavel	k1gMnSc1	Pavel
Plánička	plánička	k1gFnSc1	plánička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1998-2000	[number]	k4	1998-2000
(	(	kIx(	(
<g/>
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bělehrádek	Bělehrádek	k1gInSc1	Bělehrádek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Brýdl	Brýdl	k1gMnSc1	Brýdl
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Čada	Čada	k1gMnSc1	Čada
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Dočekal	Dočekal	k1gMnSc1	Dočekal
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Heřman	Heřman	k1gMnSc1	Heřman
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kaňa	Kaňa	k1gMnSc1	Kaňa
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kroupa	Kroupa	k1gMnSc1	Kroupa
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Petřík	Petřík	k1gMnSc1	Petřík
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc1	Pithart
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Roithová	Roithová	k1gFnSc1	Roithová
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Stodůlka	stodůlka	k1gFnSc1	stodůlka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šenkýř	Šenkýř	k1gMnSc1	Šenkýř
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Škrabiš	Škrabiš	k1gMnSc1	Škrabiš
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Špaček	Špaček	k1gMnSc1	Špaček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šula	Šula	k1gMnSc1	Šula
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2000-2002	[number]	k4	2000-2002
(	(	kIx(	(
<g/>
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bělehrádek	Bělehrádek	k1gInSc1	Bělehrádek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Brýdl	Brýdl	k1gMnSc1	Brýdl
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Čada	Čada	k1gMnSc1	Čada
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Dočekal	Dočekal	k1gMnSc1	Dočekal
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Janata	Janata	k1gMnSc1	Janata
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kaňa	Kaňa	k1gMnSc1	Kaňa
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kroupa	Kroupa	k1gMnSc1	Kroupa
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Petřík	Petřík	k1gMnSc1	Petřík
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc1	Pithart
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Roithová	Roithová	k1gFnSc1	Roithová
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Stodůlka	stodůlka	k1gFnSc1	stodůlka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šenkýř	Šenkýř	k1gMnSc1	Šenkýř
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Šimonovský	Šimonovský	k2eAgMnSc1d1	Šimonovský
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Škrabiš	Škrabiš	k1gMnSc1	Škrabiš
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Špaček	Špaček	k1gMnSc1	Špaček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šula	Šula	k1gMnSc1	Šula
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2002-2006	[number]	k4	2002-2006
(	(	kIx(	(
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Libor	Libor	k1gMnSc1	Libor
Ambrozek	Ambrozka	k1gFnPc2	Ambrozka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Grůza	Grůza	k1gFnSc1	Grůza
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
Holáň	Holáň	k1gFnSc1	Holáň
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Hovorka	Hovorka	k1gMnSc1	Hovorka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kvapil	Kvapil	k1gMnSc1	Kvapil
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Parkanová	Parkanová	k1gFnSc1	Parkanová
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Říha	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Škopík	Škopík	k1gMnSc1	Škopík
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
Šojdrová	Šojdrová	k1gFnSc1	Šojdrová
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šustr	Šustr	k1gMnSc1	Šustr
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Talíř	Talíř	k1gMnSc1	Talíř
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vícha	Vích	k1gMnSc2	Vích
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Vykydal	vykydat	k5eAaPmAgMnS	vykydat
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Pavel	Pavel	k1gMnSc1	Pavel
Severa	Severa	k1gMnSc1	Severa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
společné	společný	k2eAgFnSc6d1	společná
kandidátní	kandidátní	k2eAgFnSc6d1	kandidátní
listině	listina	k1gFnSc6	listina
Koalice	koalice	k1gFnSc1	koalice
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
a	a	k8xC	a
US-DEU	US-DEU	k1gMnPc1	US-DEU
byli	být	k5eAaImAgMnP	být
coby	coby	k?	coby
nominanti	nominant	k1gMnPc1	nominant
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
ještě	ještě	k9	ještě
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Karásek	Karásek	k1gMnSc1	Karásek
a	a	k8xC	a
Táňa	Táňa	k1gFnSc1	Táňa
Fischerová	Fischerová	k1gFnSc1	Fischerová
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ale	ale	k8xC	ale
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
US-DEU	US-DEU	k1gFnSc2	US-DEU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2002-2004	[number]	k4	2002-2004
(	(	kIx(	(
<g/>
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bělehrádek	Bělehrádek	k1gInSc1	Bělehrádek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Brýdl	Brýdl	k1gMnSc1	Brýdl
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Čada	Čada	k1gMnSc1	Čada
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Janata	Janata	k1gMnSc1	Janata
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kaňa	Kaňa	k1gMnSc1	Kaňa
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kroupa	Kroupa	k1gMnSc1	Kroupa
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc1	Pithart
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Roithová	Roithová	k1gFnSc1	Roithová
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Stodůlka	stodůlka	k1gFnSc1	stodůlka
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Šimonovský	Šimonovský	k2eAgMnSc1d1	Šimonovský
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Škrabiš	Škrabiš	k1gMnSc1	Škrabiš
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šula	Šula	k1gMnSc1	Šula
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Karel	Karel	k1gMnSc1	Karel
Barták	Barták	k1gMnSc1	Barták
(	(	kIx(	(
<g/>
nez	nez	k?	nez
<g/>
.	.	kIx.	.
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Josef	Josef	k1gMnSc1	Josef
Kalbáč	Kalbáč	k1gMnSc1	Kalbáč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2004-2006	[number]	k4	2004-2006
(	(	kIx(	(
<g/>
Senát	senát	k1gInSc1	senát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Barták	Barták	k1gMnSc1	Barták
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Brýdl	Brýdl	k1gMnSc1	Brýdl
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Janata	Janata	k1gMnSc1	Janata
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jehlička	jehlička	k1gFnSc1	jehlička
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kalbáč	Kalbáč	k1gMnSc1	Kalbáč
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
Müllerová	Müllerová	k1gFnSc1	Müllerová
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc1	Pithart
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
Slavotínek	Slavotínek	k1gMnSc1	Slavotínek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Stodůlka	stodůlka	k1gFnSc1	stodůlka
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Šimonovský	Šimonovský	k2eAgMnSc1d1	Šimonovský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
volbami	volba	k1gFnPc7	volba
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Milan	Milan	k1gMnSc1	Milan
Špaček	Špaček	k1gMnSc1	Špaček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
politický	politický	k2eAgInSc1d1	politický
katolicismus	katolicismus	k1gInSc1	katolicismus
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7325-155-0	[number]	k4	978-80-7325-155-0
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šindar	Šindar	k1gMnSc1	Šindar
<g/>
:	:	kIx,	:
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
pod	pod	k7c7	pod
cizím	cizí	k2eAgNnSc7d1	cizí
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Kostelní	kostelní	k2eAgFnSc1d1	kostelní
Vydří	vydří	k2eAgFnSc1d1	vydří
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7195-101-8	[number]	k4	978-80-7195-101-8
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
převážně	převážně	k6eAd1	převážně
dalším	další	k2eAgInSc7d1	další
osudem	osud	k1gInSc7	osud
předsedy	předseda	k1gMnSc2	předseda
ČSL	ČSL	kA	ČSL
Jana	Jan	k1gMnSc2	Jan
Šrámka	Šrámek	k1gMnSc2	Šrámek
po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
převratu	převrat	k1gInSc6	převrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
přináší	přinášet	k5eAaImIp3nP	přinášet
cenné	cenný	k2eAgFnPc1d1	cenná
informace	informace	k1gFnPc1	informace
o	o	k7c4	o
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
ČSL	ČSL	kA	ČSL
během	během	k7c2	během
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
Šrámkova	Šrámkův	k2eAgMnSc4d1	Šrámkův
nejbližšího	blízký	k2eAgMnSc4d3	nejbližší
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
a	a	k8xC	a
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
spoluvězně	spoluvězeň	k1gMnSc2	spoluvězeň
<g/>
,	,	kIx,	,
Františka	František	k1gMnSc2	František
Hály	Hála	k1gMnSc2	Hála
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Renner	Renner	k1gMnSc1	Renner
<g/>
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
PRIUS	prius	k1gNnSc1	prius
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Balík	Balík	k1gMnSc1	Balík
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
:	:	kIx,	:
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7325-130-7	[number]	k4	978-80-7325-130-7
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
osudem	osud	k1gInSc7	osud
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
četné	četný	k2eAgFnPc4d1	četná
exkurze	exkurze	k1gFnPc4	exkurze
do	do	k7c2	do
dění	dění	k1gNnSc2	dění
<g/>
,	,	kIx,	,
strategie	strategie	k1gFnSc2	strategie
a	a	k8xC	a
personálního	personální	k2eAgNnSc2d1	personální
složení	složení	k1gNnSc2	složení
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Předsedové	předseda	k1gMnPc1	předseda
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Stašek	Stašek	k1gMnSc1	Stašek
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
</s>
</p>
<p>
<s>
Augustin	Augustin	k1gMnSc1	Augustin
Navrátil	Navrátil	k1gMnSc1	Navrátil
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
</s>
</p>
<p>
<s>
Čtyřkoalice	Čtyřkoalice	k1gFnSc1	Čtyřkoalice
</s>
</p>
<p>
<s>
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
a	a	k8xC	a
demokratická	demokratický	k2eAgFnSc1d1	demokratická
unie	unie	k1gFnSc1	unie
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gFnSc1	téma
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
a	a	k8xC	a
demokratická	demokratický	k2eAgFnSc1d1	demokratická
unie	unie	k1gFnSc1	unie
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
strany	strana	k1gFnSc2	strana
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
ČSL	ČSL	kA	ČSL
v	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Mrklas	Mrklas	k1gMnSc1	Mrklas
<g/>
:	:	kIx,	:
Exilová	exilový	k2eAgFnSc1d1	exilová
demokratická	demokratický	k2eAgFnSc1d1	demokratická
pravice	pravice	k1gFnSc1	pravice
</s>
</p>
<p>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
demokraté	demokrat	k1gMnPc1	demokrat
</s>
</p>
<p>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
strana	strana	k1gFnSc1	strana
–	–	k?	–
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
