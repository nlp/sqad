<s>
Kuřička	kuřička	k1gFnSc1	kuřička
hadcová	hadcový	k2eAgFnSc1d1	hadcová
<g/>
,	,	kIx,	,
též	též	k9	též
kuřička	kuřička	k1gFnSc1	kuřička
Smejkalova	Smejkalův	k2eAgFnSc1d1	Smejkalova
(	(	kIx(	(
<g/>
Minuartia	Minuartia	k1gFnSc1	Minuartia
smejkalii	smejkalie	k1gFnSc4	smejkalie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hvozdíkovité	hvozdíkovitý	k2eAgFnSc2d1	hvozdíkovitý
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
endemitem	endemit	k1gInSc7	endemit
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
části	část	k1gFnSc2	část
Českého	český	k2eAgInSc2d1	český
masivu	masiv	k1gInSc2	masiv
<g/>
.	.	kIx.	.
</s>
