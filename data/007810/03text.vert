<s>
Kuřička	kuřička	k1gFnSc1	kuřička
hadcová	hadcový	k2eAgFnSc1d1	hadcová
<g/>
,	,	kIx,	,
též	též	k9	též
kuřička	kuřička	k1gFnSc1	kuřička
Smejkalova	Smejkalův	k2eAgFnSc1d1	Smejkalova
(	(	kIx(	(
<g/>
Minuartia	Minuartia	k1gFnSc1	Minuartia
smejkalii	smejkalie	k1gFnSc4	smejkalie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hvozdíkovité	hvozdíkovitý	k2eAgFnSc2d1	hvozdíkovitý
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
endemitem	endemit	k1gInSc7	endemit
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
části	část	k1gFnSc2	část
Českého	český	k2eAgInSc2d1	český
masivu	masiv	k1gInSc2	masiv
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
tedy	tedy	k9	tedy
jinde	jinde	k6eAd1	jinde
neroste	růst	k5eNaImIp3nS	růst
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ohrožení	ohrožení	k1gNnSc2	ohrožení
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
řazen	řadit	k5eAaImNgMnS	řadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Minuartia	Minuartia	k1gFnSc1	Minuartia
verna	vern	k1gInSc2	vern
auct	aucta	k1gFnPc2	aucta
<g/>
.	.	kIx.	.
p.	p.	k?	p.
<g/>
p.	p.	k?	p.
Minuartia	Minuartius	k1gMnSc2	Minuartius
verna	vern	k1gMnSc2	vern
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
collina	collina	k1gFnSc1	collina
auct	aucta	k1gFnPc2	aucta
<g/>
.	.	kIx.	.
</s>
<s>
Kuřička	kuřička	k1gFnSc1	kuřička
hadcová	hadcový	k2eAgFnSc1d1	hadcová
je	být	k5eAaImIp3nS	být
drobná	drobný	k2eAgFnSc1d1	drobná
rostlinka	rostlinka	k1gFnSc1	rostlinka
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
květy	květ	k1gInPc7	květ
<g/>
,	,	kIx,	,
kvetoucí	kvetoucí	k2eAgNnSc1d1	kvetoucí
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
obligátní	obligátní	k2eAgInSc4d1	obligátní
serpentinofyt	serpentinofyt	k1gInSc4	serpentinofyt
<g/>
,	,	kIx,	,
výskytem	výskyt	k1gInSc7	výskyt
je	být	k5eAaImIp3nS	být
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
podklad	podklad	k1gInSc4	podklad
tvořený	tvořený	k2eAgInSc4d1	tvořený
horninou	hornina	k1gFnSc7	hornina
hadcem	hadec	k1gInSc7	hadec
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
serpentinit	serpentinit	k5eAaPmF	serpentinit
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
polostínu	polostín	k1gInSc6	polostín
na	na	k7c6	na
skalních	skalní	k2eAgFnPc6d1	skalní
plošinách	plošina	k1gFnPc6	plošina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
štěrbinách	štěrbina	k1gFnPc6	štěrbina
a	a	k8xC	a
na	na	k7c6	na
mělkých	mělký	k2eAgFnPc6d1	mělká
hadcových	hadcový	k2eAgFnPc6d1	hadcová
půdách	půda	k1gFnPc6	půda
v	v	k7c6	v
řídkých	řídký	k2eAgInPc6d1	řídký
borových	borový	k2eAgInPc6d1	borový
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
semeny	semeno	k1gNnPc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgInPc1d1	znám
tři	tři	k4xCgFnPc1	tři
lokality	lokalita	k1gFnPc1	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kuřička	kuřička	k1gFnSc1	kuřička
hadcová	hadcový	k2eAgFnSc1d1	hadcová
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Borku	borek	k1gInSc6	borek
u	u	k7c2	u
Chotěboře	Chotěboř	k1gFnSc2	Chotěboř
vyhynula	vyhynout	k5eAaPmAgFnS	vyhynout
už	už	k6eAd1	už
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
u	u	k7c2	u
Kamberka	Kamberka	k1gFnSc1	Kamberka
na	na	k7c6	na
Hadcích	hadec	k1gInPc6	hadec
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Hrnčíře	Hrnčíř	k1gMnSc2	Hrnčíř
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgNnP	zachovat
pouze	pouze	k6eAd1	pouze
málo	málo	k6eAd1	málo
početná	početný	k2eAgFnSc1d1	početná
populace	populace	k1gFnSc1	populace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
přežití	přežití	k1gNnSc2	přežití
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
nejperspektivnější	perspektivní	k2eAgFnSc1d3	nejperspektivnější
oblast	oblast	k1gFnSc1	oblast
Dolnokralovických	Dolnokralovický	k2eAgInPc2d1	Dolnokralovický
hadců	hadec	k1gInPc2	hadec
<g/>
,	,	kIx,	,
svahy	svah	k1gInPc1	svah
okolo	okolo	k7c2	okolo
přehrady	přehrada	k1gFnSc2	přehrada
Želivka	Želivka	k1gFnSc1	Želivka
a	a	k8xC	a
lokality	lokalita	k1gFnPc1	lokalita
u	u	k7c2	u
obcí	obec	k1gFnPc2	obec
Sedlice	Sedlice	k1gFnPc4	Sedlice
a	a	k8xC	a
Bernartice	Bernartice	k1gFnPc4	Bernartice
<g/>
.	.	kIx.	.
</s>
<s>
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
-	-	kIx~	-
Evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnPc1d1	významná
lokality	lokalita	k1gFnPc1	lokalita
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
-	-	kIx~	-
kuřička	kuřička	k1gFnSc1	kuřička
Smejkalova	Smejkalův	k2eAgFnSc1d1	Smejkalova
</s>
