<p>
<s>
Bellmanův	Bellmanův	k2eAgInSc1d1	Bellmanův
<g/>
–	–	k?	–
<g/>
Fordův	Fordův	k2eAgInSc1d1	Fordův
algoritmus	algoritmus	k1gInSc1	algoritmus
počítá	počítat	k5eAaImIp3nS	počítat
nejkratší	krátký	k2eAgFnSc4d3	nejkratší
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
ohodnoceném	ohodnocený	k2eAgInSc6d1	ohodnocený
grafu	graf	k1gInSc6	graf
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
uzlu	uzel	k1gInSc2	uzel
do	do	k7c2	do
uzlu	uzel	k1gInSc2	uzel
dalšího	další	k2eAgInSc2d1	další
(	(	kIx(	(
<g/>
do	do	k7c2	do
ostatních	ostatní	k2eAgInPc2d1	ostatní
uzlů	uzel	k1gInPc2	uzel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
některé	některý	k3yIgFnPc4	některý
hrany	hrana	k1gFnPc4	hrana
ohodnoceny	ohodnocen	k2eAgFnPc4d1	ohodnocena
i	i	k8xC	i
záporně	záporně	k6eAd1	záporně
<g/>
.	.	kIx.	.
</s>
<s>
Dijkstrův	Dijkstrův	k2eAgInSc1d1	Dijkstrův
algoritmus	algoritmus	k1gInSc1	algoritmus
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
řeší	řešit	k5eAaImIp3nS	řešit
sice	sice	k8xC	sice
v	v	k7c6	v
kratším	krátký	k2eAgInSc6d2	kratší
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
nezáporné	záporný	k2eNgFnPc4d1	nezáporná
ohodnocené	ohodnocený	k2eAgFnPc4d1	ohodnocená
hrany	hrana	k1gFnPc4	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Bellmanův	Bellmanův	k2eAgInSc1d1	Bellmanův
<g/>
–	–	k?	–
<g/>
Fordův	Fordův	k2eAgInSc1d1	Fordův
algoritmus	algoritmus	k1gInSc1	algoritmus
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
grafy	graf	k1gInPc4	graf
se	s	k7c7	s
záporně	záporně	k6eAd1	záporně
ohodnocenými	ohodnocený	k2eAgFnPc7d1	ohodnocená
hranami	hrana	k1gFnPc7	hrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Algoritmus	algoritmus	k1gInSc1	algoritmus
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
ve	v	k7c6	v
směrovacím	směrovací	k2eAgInSc6d1	směrovací
protokolu	protokol	k1gInSc6	protokol
RIP	RIP	kA	RIP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
algoritmu	algoritmus	k1gInSc2	algoritmus
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
grafů	graf	k1gInPc2	graf
se	s	k7c7	s
záporně	záporně	k6eAd1	záporně
ohodnocenými	ohodnocený	k2eAgFnPc7d1	ohodnocená
hranami	hrana	k1gFnPc7	hrana
není	být	k5eNaImIp3nS	být
Dijkstrův	Dijkstrův	k2eAgInSc4d1	Dijkstrův
algoritmus	algoritmus	k1gInSc4	algoritmus
obecně	obecně	k6eAd1	obecně
vždy	vždy	k6eAd1	vždy
použitelný	použitelný	k2eAgInSc1d1	použitelný
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nasazujeme	nasazovat	k5eAaImIp1nP	nasazovat
Bellmanův	Bellmanův	k2eAgInSc4d1	Bellmanův
<g/>
–	–	k?	–
<g/>
Fordův	Fordův	k2eAgInSc1d1	Fordův
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Dijkstrův	Dijkstrův	k2eAgInSc1d1	Dijkstrův
algoritmus	algoritmus	k1gInSc1	algoritmus
využívá	využívat	k5eAaPmIp3nS	využívat
metodu	metoda	k1gFnSc4	metoda
relaxace	relaxace	k1gFnSc2	relaxace
hran	hrana	k1gFnPc2	hrana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
aktuálně	aktuálně	k6eAd1	aktuálně
nastavenou	nastavený	k2eAgFnSc4d1	nastavená
hodnotu	hodnota	k1gFnSc4	hodnota
nejkratší	krátký	k2eAgFnSc2d3	nejkratší
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
uzlu	uzel	k1gInSc2	uzel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnota	hodnota	k1gFnSc1	hodnota
v	v	k7c6	v
uzlu	uzel	k1gInSc6	uzel
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
hodnota	hodnota	k1gFnSc1	hodnota
z	z	k7c2	z
nynějšího	nynější	k2eAgInSc2d1	nynější
uzlu	uzel	k1gInSc2	uzel
plus	plus	k1gNnSc1	plus
ohodnocení	ohodnocení	k1gNnSc1	ohodnocení
hrany	hrana	k1gFnSc2	hrana
z	z	k7c2	z
nynějšího	nynější	k2eAgInSc2d1	nynější
uzlu	uzel	k1gInSc2	uzel
do	do	k7c2	do
uzlu	uzel	k1gInSc2	uzel
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yQgInSc6	který
bychom	by	kYmCp1nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
změnit	změnit	k5eAaPmF	změnit
jeho	jeho	k3xOp3gFnSc4	jeho
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
tuto	tento	k3xDgFnSc4	tento
hodnotu	hodnota	k1gFnSc4	hodnota
změníme	změnit	k5eAaPmIp1nP	změnit
(	(	kIx(	(
<g/>
snížíme	snížit	k5eAaPmIp1nP	snížit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
oproti	oproti	k7c3	oproti
Dijkstrovu	Dijkstrův	k2eAgInSc3d1	Dijkstrův
algoritmu	algoritmus	k1gInSc3	algoritmus
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
způsobu	způsob	k1gInSc6	způsob
průchodu	průchod	k1gInSc2	průchod
grafu	graf	k1gInSc2	graf
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Dijkstrův	Dijkstrův	k2eAgInSc1d1	Dijkstrův
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
projdeme	projít	k5eAaPmIp1nP	projít
všechny	všechen	k3xTgMnPc4	všechen
následníky	následník	k1gMnPc4	následník
jednoho	jeden	k4xCgInSc2	jeden
uzlu	uzel	k1gInSc2	uzel
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
tento	tento	k3xDgInSc1	tento
uzel	uzel	k1gInSc1	uzel
"	"	kIx"	"
<g/>
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
<g/>
"	"	kIx"	"
a	a	k8xC	a
poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
už	už	k6eAd1	už
neupravuje	upravovat	k5eNaImIp3nS	upravovat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
v	v	k7c6	v
Bellmanově	Bellmanův	k2eAgInSc6d1	Bellmanův
<g/>
–	–	k?	–
<g/>
Fordově	Fordův	k2eAgNnSc6d1	Fordovo
algoritmu	algoritmus	k1gInSc6	algoritmus
neděje	dít	k5eNaImIp3nS	dít
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
tyto	tento	k3xDgInPc4	tento
uzly	uzel	k1gInPc4	uzel
neuzavírá	uzavírat	k5eNaImIp3nS	uzavírat
takto	takto	k6eAd1	takto
ihned	ihned	k6eAd1	ihned
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prochází	procházet	k5eAaImIp3nS	procházet
několikrát	několikrát	k6eAd1	několikrát
všechny	všechen	k3xTgInPc4	všechen
uzly	uzel	k1gInPc4	uzel
a	a	k8xC	a
upravuje	upravovat	k5eAaImIp3nS	upravovat
postupně	postupně	k6eAd1	postupně
hodnoty	hodnota	k1gFnPc4	hodnota
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
nejkratších	krátký	k2eAgFnPc2d3	nejkratší
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Implementace	implementace	k1gFnSc2	implementace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Obecná	obecný	k2eAgFnSc1d1	obecná
implementace	implementace	k1gFnSc1	implementace
v	v	k7c6	v
pseudokódu	pseudokód	k1gInSc6	pseudokód
===	===	k?	===
</s>
</p>
<p>
<s>
bellman-ford	bellmanord	k1gInSc1	bellman-ford
<g/>
(	(	kIx(	(
<g/>
vrcholy	vrchol	k1gInPc1	vrchol
<g/>
,	,	kIx,	,
hrany	hrana	k1gFnPc1	hrana
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
//	//	k?	//
krok	krok	k1gInSc1	krok
1	[number]	k4	1
<g/>
:	:	kIx,	:
inicializace	inicializace	k1gFnSc1	inicializace
grafu	graf	k1gInSc2	graf
</s>
</p>
<p>
<s>
for	forum	k1gNnPc2	forum
each	eacha	k1gFnPc2	eacha
v	v	k7c4	v
in	in	k?	in
vrcholy	vrchol	k1gInPc4	vrchol
</s>
</p>
<p>
<s>
if	if	k?	if
v	v	k7c4	v
<g/>
=	=	kIx~	=
<g/>
zdroj	zdroj	k1gInSc4	zdroj
then	then	k1gInSc1	then
v.	v.	k?	v.
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
:	:	kIx,	:
<g/>
=	=	kIx~	=
0	[number]	k4	0
</s>
</p>
<p>
<s>
else	else	k1gFnSc1	else
v.	v.	k?	v.
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
:	:	kIx,	:
<g/>
=	=	kIx~	=
nekonečno	nekonečno	k1gNnSc1	nekonečno
</s>
</p>
<p>
<s>
v.	v.	k?	v.
<g/>
předchůdce	předchůdce	k1gMnSc1	předchůdce
:	:	kIx,	:
<g/>
=	=	kIx~	=
null	null	k1gInSc1	null
</s>
</p>
<p>
<s>
//	//	k?	//
krok	krok	k1gInSc1	krok
2	[number]	k4	2
<g/>
:	:	kIx,	:
opakovaně	opakovaně	k6eAd1	opakovaně
relaxovat	relaxovat	k5eAaBmF	relaxovat
hrany	hrana	k1gFnPc4	hrana
</s>
</p>
<p>
<s>
for	forum	k1gNnPc2	forum
i	i	k9	i
from	fro	k1gNnSc7	fro
1	[number]	k4	1
to	ten	k3xDgNnSc1	ten
size	size	k1gNnSc1	size
<g/>
(	(	kIx(	(
<g/>
vrcholy	vrchol	k1gInPc4	vrchol
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
</s>
</p>
<p>
<s>
for	forum	k1gNnPc2	forum
each	each	k1gInSc1	each
h	h	k?	h
in	in	k?	in
hrany	hrana	k1gFnSc2	hrana
//	//	k?	//
h	h	k?	h
je	být	k5eAaImIp3nS	být
hrana	hrana	k1gFnSc1	hrana
z	z	k7c2	z
u	u	k7c2	u
do	do	k7c2	do
v	v	k7c6	v
</s>
</p>
<p>
<s>
u	u	k7c2	u
:	:	kIx,	:
<g/>
=	=	kIx~	=
h.	h.	k?	h.
<g/>
počátek	počátek	k1gInSc4	počátek
</s>
</p>
<p>
<s>
v	v	k7c6	v
:	:	kIx,	:
<g/>
=	=	kIx~	=
h.	h.	k?	h.
<g/>
konec	konec	k1gInSc1	konec
</s>
</p>
<p>
<s>
if	if	k?	if
u.	u.	k?	u.
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
+	+	kIx~	+
h.	h.	k?	h.
<g/>
délka	délka	k1gFnSc1	délka
<	<	kIx(	<
v.	v.	k?	v.
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
</s>
</p>
<p>
<s>
v.	v.	k?	v.
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
:	:	kIx,	:
<g/>
=	=	kIx~	=
u.	u.	k?	u.
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
+	+	kIx~	+
h.	h.	k?	h.
<g/>
délka	délka	k1gFnSc1	délka
</s>
</p>
<p>
<s>
v.	v.	k?	v.
<g/>
předchůdce	předchůdce	k1gMnSc1	předchůdce
:	:	kIx,	:
<g/>
=	=	kIx~	=
u	u	k7c2	u
</s>
</p>
<p>
<s>
//	//	k?	//
krok	krok	k1gInSc1	krok
3	[number]	k4	3
<g/>
:	:	kIx,	:
kontrola	kontrola	k1gFnSc1	kontrola
záporných	záporný	k2eAgInPc2d1	záporný
cyklů	cyklus	k1gInPc2	cyklus
</s>
</p>
<p>
<s>
for	forum	k1gNnPc2	forum
each	each	k1gInSc1	each
h	h	k?	h
in	in	k?	in
hrany	hrana	k1gFnPc4	hrana
</s>
</p>
<p>
<s>
u	u	k7c2	u
:	:	kIx,	:
<g/>
=	=	kIx~	=
h.	h.	k?	h.
<g/>
počátek	počátek	k1gInSc4	počátek
</s>
</p>
<p>
<s>
v	v	k7c6	v
:	:	kIx,	:
<g/>
=	=	kIx~	=
h.	h.	k?	h.
<g/>
konec	konec	k1gInSc1	konec
</s>
</p>
<p>
<s>
if	if	k?	if
u.	u.	k?	u.
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
+	+	kIx~	+
h.	h.	k?	h.
<g/>
délka	délka	k1gFnSc1	délka
<	<	kIx(	<
v.	v.	k?	v.
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
</s>
</p>
<p>
<s>
error	error	k1gInSc1	error
"	"	kIx"	"
<g/>
Graf	graf	k1gInSc1	graf
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
záporný	záporný	k2eAgInSc1d1	záporný
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
cyklus	cyklus	k1gInSc1	cyklus
inicializuje	inicializovat	k5eAaImIp3nS	inicializovat
graf	graf	k1gInSc4	graf
–	–	k?	–
nastaví	nastavět	k5eAaBmIp3nS	nastavět
všem	všecek	k3xTgInPc3	všecek
vrcholům	vrchol	k1gInPc3	vrchol
kromě	kromě	k7c2	kromě
zdroje	zdroj	k1gInSc2	zdroj
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
zdroji	zdroj	k1gInSc6	zdroj
nulovou	nulový	k2eAgFnSc4d1	nulová
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
cyklus	cyklus	k1gInSc1	cyklus
upravuje	upravovat	k5eAaImIp3nS	upravovat
relaxací	relaxace	k1gFnSc7	relaxace
hodnoty	hodnota	k1gFnSc2	hodnota
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
nejkratších	krátký	k2eAgFnPc2d3	nejkratší
cest	cesta	k1gFnPc2	cesta
mezi	mezi	k7c7	mezi
zdrojem	zdroj	k1gInSc7	zdroj
a	a	k8xC	a
ostatními	ostatní	k2eAgInPc7d1	ostatní
uzly	uzel	k1gInPc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
třetí	třetí	k4xOgInSc1	třetí
cyklus	cyklus	k1gInSc1	cyklus
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
některá	některý	k3yIgFnSc1	některý
už	už	k6eAd1	už
určená	určený	k2eAgFnSc1d1	určená
hodnota	hodnota	k1gFnSc1	hodnota
nejkratší	krátký	k2eAgFnSc2d3	nejkratší
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
uzly	uzel	k1gInPc7	uzel
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgNnP	moct
být	být	k5eAaImF	být
ještě	ještě	k9	ještě
zkrácena	zkrácen	k2eAgFnSc1d1	zkrácena
(	(	kIx(	(
<g/>
snížena	snížen	k2eAgFnSc1d1	snížena
hodnota	hodnota	k1gFnSc1	hodnota
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
cesty	cesta	k1gFnPc1	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
graf	graf	k1gInSc1	graf
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
záporně	záporně	k6eAd1	záporně
ohodnocený	ohodnocený	k2eAgInSc1d1	ohodnocený
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
postrádá	postrádat	k5eAaImIp3nS	postrádat
úkol	úkol	k1gInSc4	úkol
nalezení	nalezení	k1gNnSc2	nalezení
minimální	minimální	k2eAgFnSc2d1	minimální
cesty	cesta	k1gFnSc2	cesta
smysl	smysl	k1gInSc1	smysl
(	(	kIx(	(
<g/>
opakovaným	opakovaný	k2eAgInSc7d1	opakovaný
průchodem	průchod	k1gInSc7	průchod
cyklem	cyklus	k1gInSc7	cyklus
získáme	získat	k5eAaPmIp1nP	získat
neomezeně	omezeně	k6eNd1	omezeně
krátkou	krátký	k2eAgFnSc4d1	krátká
cestu	cesta	k1gFnSc4	cesta
<g/>
)	)	kIx)	)
a	a	k8xC	a
hodnoty	hodnota	k1gFnPc1	hodnota
vykonstruované	vykonstruovaný	k2eAgFnPc1d1	vykonstruovaná
algoritmem	algoritmus	k1gInSc7	algoritmus
nemůžeme	moct	k5eNaImIp1nP	moct
brát	brát	k5eAaImF	brát
za	za	k7c4	za
správné	správný	k2eAgNnSc4d1	správné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Implementace	implementace	k1gFnSc1	implementace
–	–	k?	–
Perl	perl	k1gInSc1	perl
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Implementace	implementace	k1gFnSc1	implementace
–	–	k?	–
C	C	kA	C
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Složitost	složitost	k1gFnSc4	složitost
==	==	k?	==
</s>
</p>
<p>
<s>
Složitost	složitost	k1gFnSc1	složitost
algoritmu	algoritmus	k1gInSc2	algoritmus
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
E	E	kA	E
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
vrcholů	vrchol	k1gInPc2	vrchol
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
počet	počet	k1gInSc1	počet
hran	hrana	k1gFnPc2	hrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Základní	základní	k2eAgInPc1d1	základní
grafové	grafový	k2eAgInPc1d1	grafový
algoritmy	algoritmus	k1gInPc1	algoritmus
<g/>
(	(	kIx(	(
<g/>
texty	text	k1gInPc1	text
v	v	k7c6	v
pdf	pdf	k?	pdf
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Interaktivní	interaktivní	k2eAgFnPc1d1	interaktivní
demonstrace	demonstrace	k1gFnPc1	demonstrace
</s>
</p>
