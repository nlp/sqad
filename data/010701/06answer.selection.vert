<s>
Bellmanův	Bellmanův	k2eAgInSc1d1	Bellmanův
<g/>
–	–	k?	–
<g/>
Fordův	Fordův	k2eAgInSc1d1	Fordův
algoritmus	algoritmus	k1gInSc1	algoritmus
počítá	počítat	k5eAaImIp3nS	počítat
nejkratší	krátký	k2eAgFnSc4d3	nejkratší
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
ohodnoceném	ohodnocený	k2eAgInSc6d1	ohodnocený
grafu	graf	k1gInSc6	graf
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
uzlu	uzel	k1gInSc2	uzel
do	do	k7c2	do
uzlu	uzel	k1gInSc2	uzel
dalšího	další	k2eAgInSc2d1	další
(	(	kIx(	(
<g/>
do	do	k7c2	do
ostatních	ostatní	k2eAgInPc2d1	ostatní
uzlů	uzel	k1gInPc2	uzel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
některé	některý	k3yIgFnPc4	některý
hrany	hrana	k1gFnPc4	hrana
ohodnoceny	ohodnocen	k2eAgFnPc4d1	ohodnocena
i	i	k8xC	i
záporně	záporně	k6eAd1	záporně
<g/>
.	.	kIx.	.
</s>
