<s>
Willis	Willis	k1gFnSc1	Willis
Eugene	Eugen	k1gInSc5	Eugen
Lamb	Lamb	k1gMnSc1	Lamb
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Tuscon	Tuscon	k1gMnSc1	Tuscon
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Polykarpem	Polykarp	k1gMnSc7	Polykarp
Kuschem	Kusch	k1gMnSc7	Kusch
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Lamb	Lamb	k1gInSc1	Lamb
za	za	k7c4	za
objevy	objev	k1gInPc4	objev
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
jemné	jemný	k2eAgFnPc1d1	jemná
struktury	struktura	k1gFnPc1	struktura
vodíkového	vodíkový	k2eAgNnSc2d1	vodíkové
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Willis	Willis	k1gFnSc2	Willis
Lamb	Lamba	k1gFnPc2	Lamba
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Willis	Willis	k1gFnSc2	Willis
Eugene	Eugen	k1gInSc5	Eugen
Lamb	Lamb	k1gInSc4	Lamb
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
