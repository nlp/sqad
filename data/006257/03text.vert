<s>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
NM	NM	kA	NM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
česká	český	k2eAgFnSc1d1	Česká
muzejní	muzejní	k2eAgFnSc1d1	muzejní
instituce	instituce	k1gFnSc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
vědních	vědní	k2eAgInPc2d1	vědní
a	a	k8xC	a
sbírkových	sbírkový	k2eAgInPc2d1	sbírkový
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
sbírky	sbírka	k1gFnPc4	sbírka
spravuje	spravovat	k5eAaImIp3nS	spravovat
a	a	k8xC	a
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
i	i	k9	i
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
je	být	k5eAaImIp3nS	být
také	také	k9	také
označení	označení	k1gNnSc4	označení
monumentální	monumentální	k2eAgNnSc4d1	monumentální
novorenesanční	novorenesanční	k2eAgFnPc4d1	novorenesanční
historické	historický	k2eAgFnPc4d1	historická
hlavní	hlavní	k2eAgFnPc4d1	hlavní
budovy	budova	k1gFnPc4	budova
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Vlastenecké	vlastenecký	k2eAgNnSc1d1	vlastenecké
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
či	či	k8xC	či
Vaterländisches	Vaterländisches	k1gMnSc1	Vaterländisches
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
provoláním	provolání	k1gNnSc7	provolání
An	An	k1gFnSc2	An
die	die	k?	die
Vaterländichen	Vaterländichen	k1gInSc1	Vaterländichen
Freunde	Freund	k1gInSc5	Freund
der	drát	k5eAaImRp2nS	drát
Wissenschaften	Wissenschaften	k2eAgInSc4d1	Wissenschaften
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
stáli	stát	k5eAaImAgMnP	stát
zástupci	zástupce	k1gMnPc1	zástupce
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
české	český	k2eAgFnSc2d1	Česká
akademické	akademický	k2eAgFnSc2d1	akademická
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
především	především	k9	především
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
faktickým	faktický	k2eAgMnSc7d1	faktický
zakladatelem	zakladatel	k1gMnSc7	zakladatel
byl	být	k5eAaImAgMnS	být
hrabě	hrabě	k1gMnSc1	hrabě
Kašpar	Kašpar	k1gMnSc1	Kašpar
Maria	Maria	k1gFnSc1	Maria
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
<g/>
,	,	kIx,	,
paleontolog	paleontolog	k1gMnSc1	paleontolog
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
muzeu	muzeum	k1gNnSc6	muzeum
věnoval	věnovat	k5eAaPmAgInS	věnovat
své	svůj	k3xOyFgFnPc4	svůj
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
sbírek	sbírka	k1gFnPc2	sbírka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
Šternberský	šternberský	k2eAgInSc1d1	šternberský
palác	palác	k1gInSc1	palác
na	na	k7c6	na
pražských	pražský	k2eAgInPc6d1	pražský
Hradčanech	Hradčany	k1gInPc6	Hradčany
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1821	[number]	k4	1821
až	až	k9	až
1847	[number]	k4	1847
první	první	k4xOgInSc4	první
sbírky	sbírka	k1gFnSc2	sbírka
muzea	muzeum	k1gNnSc2	muzeum
uložené	uložený	k2eAgInPc1d1	uložený
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
osobnosti	osobnost	k1gFnPc4	osobnost
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
muzea	muzeum	k1gNnSc2	muzeum
pak	pak	k8xC	pak
patřili	patřit	k5eAaImAgMnP	patřit
například	například	k6eAd1	například
také	také	k9	také
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
či	či	k8xC	či
hrabě	hrabě	k1gMnSc1	hrabě
Jan	Jan	k1gMnSc1	Jan
Dětmar	Dětmar	k1gMnSc1	Dětmar
z	z	k7c2	z
Nostic	Nostice	k1gFnPc2	Nostice
<g/>
,	,	kIx,	,
J.	J.	kA	J.
T.	T.	kA	T.
Lindacker	Lindacker	k1gMnSc1	Lindacker
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Zippe	Zippe	k1gMnSc1	Zippe
(	(	kIx(	(
<g/>
zakladatel	zakladatel	k1gMnSc1	zakladatel
české	český	k2eAgFnSc2d1	Česká
mineralogie	mineralogie	k1gFnSc2	mineralogie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
botanici	botanik	k1gMnPc1	botanik
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
Haenke	Haenke	k1gInSc1	Haenke
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Presl	Presl	k1gInSc1	Presl
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Jana	Jan	k1gMnSc2	Jan
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Presla	Presla	k1gMnSc2	Presla
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
,	,	kIx,	,
či	či	k8xC	či
francouzský	francouzský	k2eAgMnSc1d1	francouzský
paleontolog	paleontolog	k1gMnSc1	paleontolog
Joachim	Joachim	k1gMnSc1	Joachim
Barrande	Barrand	k1gInSc5	Barrand
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc4	muzeum
proslavil	proslavit	k5eAaPmAgInS	proslavit
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
Bernard	Bernard	k1gMnSc1	Bernard
Bolzano	Bolzana	k1gFnSc5	Bolzana
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zakladateli	zakladatel	k1gMnPc7	zakladatel
a	a	k8xC	a
pokračovateli	pokračovatel	k1gMnPc7	pokračovatel
muzea	muzeum	k1gNnSc2	muzeum
byli	být	k5eAaImAgMnP	být
také	také	k9	také
členové	člen	k1gMnPc1	člen
významného	významný	k2eAgInSc2d1	významný
českého	český	k2eAgInSc2d1	český
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Kolowratů	Kolowrat	k1gInPc2	Kolowrat
<g/>
.	.	kIx.	.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
Vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
odkázal	odkázat	k5eAaPmAgInS	odkázat
mu	on	k3xPp3gMnSc3	on
dokonce	dokonce	k9	dokonce
knihovnu	knihovna	k1gFnSc4	knihovna
čítající	čítající	k2eAgFnSc4d1	čítající
35	[number]	k4	35
000	[number]	k4	000
svazků	svazek	k1gInPc2	svazek
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
sbírku	sbírka	k1gFnSc4	sbírka
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hrabě	hrabě	k1gMnSc1	hrabě
Jan	Jan	k1gMnSc1	Jan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Karel	Karel	k1gMnSc1	Karel
stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
a	a	k8xC	a
odkázal	odkázat	k5eAaPmAgInS	odkázat
část	část	k1gFnSc4	část
svých	svůj	k3xOyFgFnPc2	svůj
sbírek	sbírka	k1gFnPc2	sbírka
a	a	k8xC	a
březnickou	březnický	k2eAgFnSc4d1	Březnická
knihovnu	knihovna	k1gFnSc4	knihovna
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
vzácnými	vzácný	k2eAgInPc7d1	vzácný
prvotisky	prvotisk	k1gInPc7	prvotisk
a	a	k8xC	a
iluminovanými	iluminovaný	k2eAgInPc7d1	iluminovaný
rukopisy	rukopis	k1gInPc7	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
muzeum	muzeum	k1gNnSc4	muzeum
podporoval	podporovat	k5eAaImAgMnS	podporovat
hrabě	hrabě	k1gMnSc1	hrabě
Josef	Josef	k1gMnSc1	Josef
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
osobnosti	osobnost	k1gFnPc4	osobnost
patřili	patřit	k5eAaImAgMnP	patřit
také	také	k6eAd1	také
cestovatelé	cestovatel	k1gMnPc1	cestovatel
Emil	Emil	k1gMnSc1	Emil
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
Vojta	Vojta	k1gMnSc1	Vojta
Náprstek	náprstek	k1gInSc1	náprstek
<g/>
,	,	kIx,	,
slavista	slavista	k1gMnSc1	slavista
Pavel	Pavel	k1gMnSc1	Pavel
Josef	Josef	k1gMnSc1	Josef
Šafařík	Šafařík	k1gMnSc1	Šafařík
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
nesla	nést	k5eAaImAgFnS	nést
instituce	instituce	k1gFnSc1	instituce
název	název	k1gInSc4	název
České	český	k2eAgNnSc1d1	české
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1854	[number]	k4	1854
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
pak	pak	k6eAd1	pak
Museum	museum	k1gNnSc1	museum
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1919-1922	[number]	k4	1919-1922
Zemské	zemský	k2eAgNnSc4d1	zemské
museum	museum	k1gNnSc4	museum
<g/>
"	"	kIx"	"
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
do	do	k7c2	do
1939	[number]	k4	1939
Národní	národní	k2eAgNnSc1d1	národní
museum	museum	k1gNnSc4	museum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1940	[number]	k4	1940
-	-	kIx~	-
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Landesmuseum	Landesmuseum	k1gInSc4	Landesmuseum
in	in	k?	in
Prag	Prag	k1gInSc1	Prag
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
postavením	postavení	k1gNnSc7	postavení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
budovy	budova	k1gFnSc2	budova
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
historická	historický	k2eAgFnSc1d1	historická
či	či	k8xC	či
hlavní	hlavní	k2eAgFnSc1d1	hlavní
<g/>
,	,	kIx,	,
sídlilo	sídlit	k5eAaImAgNnS	sídlit
muzeum	muzeum	k1gNnSc1	muzeum
provizorně	provizorně	k6eAd1	provizorně
ve	v	k7c6	v
Šternberském	šternberský	k2eAgInSc6d1	šternberský
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
–	–	k?	–
<g/>
1846	[number]	k4	1846
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Nostickém	nostický	k2eAgInSc6d1	nostický
paláci	palác	k1gInSc6	palác
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Na	na	k7c6	na
příkopě	příkop	k1gInSc6	příkop
858	[number]	k4	858
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Živnostenské	živnostenský	k2eAgFnSc2d1	Živnostenská
banky	banka	k1gFnSc2	banka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
činností	činnost	k1gFnSc7	činnost
souvisely	souviset	k5eAaImAgFnP	souviset
také	také	k6eAd1	také
další	další	k2eAgFnPc1d1	další
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
především	především	k9	především
Matice	matice	k1gFnSc1	matice
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
publikací	publikace	k1gFnPc2	publikace
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
Archeologický	archeologický	k2eAgInSc1d1	archeologický
sbor	sbor	k1gInSc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
dominantu	dominanta	k1gFnSc4	dominanta
celého	celý	k2eAgNnSc2d1	celé
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Schulze	Schulz	k1gMnSc2	Schulz
s	s	k7c7	s
centrálním	centrální	k2eAgInSc7d1	centrální
prostorem	prostor	k1gInSc7	prostor
panteonu	panteon	k1gInSc2	panteon
<g/>
.	.	kIx.	.
</s>
<s>
Vyniká	vynikat	k5eAaImIp3nS	vynikat
výzdobou	výzdoba	k1gFnSc7	výzdoba
od	od	k7c2	od
umělců	umělec	k1gMnPc2	umělec
generace	generace	k1gFnSc2	generace
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
především	především	k9	především
sochařů	sochař	k1gMnPc2	sochař
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Schnircha	Schnirch	k1gMnSc2	Schnirch
<g/>
,	,	kIx,	,
Antonína	Antonín	k1gMnSc2	Antonín
Wagnera	Wagner	k1gMnSc2	Wagner
a	a	k8xC	a
Antonína	Antonín	k1gMnSc2	Antonín
Poppa	Popp	k1gMnSc2	Popp
a	a	k8xC	a
malířů	malíř	k1gMnPc2	malíř
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Hynaise	Hynaise	k1gFnSc2	Hynaise
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
Brožíka	Brožík	k1gMnSc2	Brožík
či	či	k8xC	či
Julia	Julius	k1gMnSc2	Julius
Mařáka	Mařák	k1gMnSc2	Mařák
<g/>
,	,	kIx,	,
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
v	v	k7c6	v
letech	let	k1gInPc6	let
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
vyšel	vyjít	k5eAaPmAgInS	vyjít
vítězně	vítězně	k6eAd1	vítězně
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
27	[number]	k4	27
účastníků	účastník	k1gMnPc2	účastník
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc4	důraz
především	především	k9	především
na	na	k7c4	na
monumentalitu	monumentalita	k1gFnSc4	monumentalita
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
funkčnosti	funkčnost	k1gFnSc2	funkčnost
(	(	kIx(	(
<g/>
idea	idea	k1gFnSc1	idea
Muzeum	muzeum	k1gNnSc1	muzeum
jako	jako	k8xC	jako
chrám	chrám	k1gInSc1	chrám
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
budovy	budova	k1gFnSc2	budova
je	být	k5eAaImIp3nS	být
dvaasedmdesát	dvaasedmdesát	k4xCc1	dvaasedmdesát
jmen	jméno	k1gNnPc2	jméno
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
vepsaných	vepsaný	k2eAgInPc2d1	vepsaný
zlatým	zlatý	k2eAgNnSc7d1	Zlaté
písmem	písmo	k1gNnSc7	písmo
do	do	k7c2	do
fasády	fasáda	k1gFnSc2	fasáda
pod	pod	k7c7	pod
okny	okno	k1gNnPc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
při	při	k7c6	při
Pražském	pražský	k2eAgNnSc6d1	Pražské
povstání	povstání	k1gNnSc6	povstání
poškodila	poškodit	k5eAaPmAgFnS	poškodit
vestibul	vestibul	k1gInSc4	vestibul
muzea	muzeum	k1gNnSc2	muzeum
nevybuchlá	vybuchlý	k2eNgFnSc1d1	nevybuchlá
bomba	bomba	k1gFnSc1	bomba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
přední	přední	k2eAgFnSc1d1	přední
fasáda	fasáda	k1gFnSc1	fasáda
budovy	budova	k1gFnSc2	budova
poškozena	poškodit	k5eAaPmNgFnS	poškodit
střelami	střela	k1gFnPc7	střela
armád	armáda	k1gFnPc2	armáda
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Kapek	Kapek	k1gMnSc1	Kapek
při	při	k7c6	při
besedě	beseda	k1gFnSc6	beseda
se	s	k7c7	s
studenty	student	k1gMnPc7	student
obratně	obratně	k6eAd1	obratně
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
poškození	poškození	k1gNnSc4	poškození
budovy	budova	k1gFnSc2	budova
<g/>
;	;	kIx,	;
spřátelená	spřátelený	k2eAgNnPc4d1	spřátelené
vojska	vojsko	k1gNnPc4	vojsko
zahájila	zahájit	k5eAaPmAgFnS	zahájit
varovnou	varovný	k2eAgFnSc4d1	varovná
střelbu	střelba	k1gFnSc4	střelba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
omezený	omezený	k2eAgInSc1d1	omezený
náměr	náměr	k1gInSc1	náměr
zbraní	zbraň	k1gFnPc2	zbraň
umístěných	umístěný	k2eAgFnPc2d1	umístěná
na	na	k7c6	na
těžké	těžký	k2eAgFnSc6d1	těžká
technice	technika	k1gFnSc6	technika
plně	plně	k6eAd1	plně
neumožnil	umožnit	k5eNaPmAgInS	umožnit
střílet	střílet	k5eAaImF	střílet
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Poněkud	poněkud	k6eAd1	poněkud
pozapomenuté	pozapomenutý	k2eAgNnSc1d1	pozapomenuté
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
používané	používaný	k2eAgNnSc1d1	používané
pojmenování	pojmenování	k1gNnSc1	pojmenování
"	"	kIx"	"
<g/>
El	Ela	k1gFnPc2	Ela
Grečkovy	Grečkův	k2eAgFnSc2d1	Grečkův
fresky	freska	k1gFnSc2	freska
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
jména	jméno	k1gNnSc2	jméno
velkého	velký	k2eAgMnSc2d1	velký
malíře	malíř	k1gMnSc2	malíř
El	Ela	k1gFnPc2	Ela
Greca	Grecus	k1gMnSc2	Grecus
a	a	k8xC	a
sovětského	sovětský	k2eAgMnSc2d1	sovětský
maršála	maršál	k1gMnSc2	maršál
Andreje	Andrej	k1gMnSc2	Andrej
Grečka	Greček	k1gMnSc2	Greček
<g/>
,	,	kIx,	,
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Statika	statika	k1gFnSc1	statika
budovy	budova	k1gFnSc2	budova
byla	být	k5eAaImAgFnS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
během	během	k7c2	během
výstavby	výstavba	k1gFnSc2	výstavba
trasy	trasa	k1gFnSc2	trasa
A	a	k8xC	a
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
vestibulu	vestibul	k1gInSc2	vestibul
stanice	stanice	k1gFnSc2	stanice
Muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poničení	poničení	k1gNnSc3	poničení
západní	západní	k2eAgFnSc2d1	západní
věže	věž	k1gFnSc2	věž
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
stěn	stěna	k1gFnPc2	stěna
<g/>
,	,	kIx,	,
vestibul	vestibul	k1gInSc1	vestibul
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
lépe	dobře	k6eAd2	dobře
vyztužen	vyztužen	k2eAgMnSc1d1	vyztužen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
i	i	k9	i
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
projektů	projekt	k1gInPc2	projekt
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
celkově	celkově	k6eAd1	celkově
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
kvůli	kvůli	k7c3	kvůli
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
trvat	trvat	k5eAaImF	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
celkové	celkový	k2eAgFnSc2d1	celková
opravy	oprava	k1gFnSc2	oprava
exteriéru	exteriér	k1gInSc2	exteriér
a	a	k8xC	a
interiéru	interiér	k1gInSc2	interiér
budovy	budova	k1gFnSc2	budova
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
také	také	k9	také
zastřešena	zastřešen	k2eAgNnPc1d1	zastřešeno
nádvoří	nádvoří	k1gNnPc1	nádvoří
<g/>
,	,	kIx,	,
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
kupole	kupole	k1gFnSc1	kupole
a	a	k8xC	a
postaven	postaven	k2eAgInSc1d1	postaven
propojovací	propojovací	k2eAgInSc1d1	propojovací
tunel	tunel	k1gInSc1	tunel
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
expozice	expozice	k1gFnPc1	expozice
jsou	být	k5eAaImIp3nP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
na	na	k7c6	na
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
větší	veliký	k2eAgFnSc6d2	veliký
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
jsou	být	k5eAaImIp3nP	být
předpokládány	předpokládat	k5eAaImNgInP	předpokládat
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
4,5	[number]	k4	4,5
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
znovuoteřena	znovuoteřit	k5eAaPmNgFnS	znovuoteřit
ke	k	k7c3	k
200	[number]	k4	200
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
založení	založení	k1gNnSc2	založení
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Přírodovědné	přírodovědný	k2eAgNnSc1d1	Přírodovědné
muzeum	muzeum	k1gNnSc1	muzeum
včetně	včetně	k7c2	včetně
sbírek	sbírka	k1gFnPc2	sbírka
se	se	k3xPyFc4	se
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
do	do	k7c2	do
areálu	areál	k1gInSc2	areál
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
zimoviště	zimoviště	k1gNnSc2	zimoviště
Čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
státních	státní	k2eAgInPc2d1	státní
cirkusů	cirkus	k1gInPc2	cirkus
v	v	k7c6	v
Horních	horní	k2eAgFnPc6d1	horní
Počernicích	Počernice	k1gFnPc6	Počernice
<g/>
.	.	kIx.	.
</s>
<s>
Sbírky	sbírka	k1gFnPc1	sbírka
Historického	historický	k2eAgNnSc2d1	historické
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
knihovny	knihovna	k1gFnSc2	knihovna
a	a	k8xC	a
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
sbírek	sbírka	k1gFnPc2	sbírka
Archivu	archiv	k1gInSc2	archiv
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
se	se	k3xPyFc4	se
přestěhovaly	přestěhovat	k5eAaPmAgInP	přestěhovat
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
objektů	objekt	k1gInPc2	objekt
někdejších	někdejší	k2eAgFnPc2d1	někdejší
Hamburských	hamburský	k2eAgFnPc2d1	hamburská
a	a	k8xC	a
Magdeburských	magdeburský	k2eAgFnPc2d1	Magdeburská
kasáren	kasárny	k1gFnPc2	kasárny
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
vedení	vedení	k1gNnSc2	vedení
muzea	muzeum	k1gNnSc2	muzeum
pořádá	pořádat	k5eAaImIp3nS	pořádat
časové	časový	k2eAgFnPc4d1	časová
výstavy	výstava	k1gFnPc4	výstava
<g/>
,	,	kIx,	,
semináře	seminář	k1gInPc4	seminář
a	a	k8xC	a
komentované	komentovaný	k2eAgFnPc4d1	komentovaná
prohlídky	prohlídka	k1gFnPc4	prohlídka
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
z	z	k7c2	z
muzea	muzeum	k1gNnSc2	muzeum
odnesen	odnesen	k2eAgInSc4d1	odnesen
preparát	preparát	k1gInSc4	preparát
mozku	mozek	k1gInSc2	mozek
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
požár	požár	k1gInSc1	požár
ohlášen	ohlášet	k5eAaImNgInS	ohlášet
v	v	k7c6	v
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
32	[number]	k4	32
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
hořela	hořet	k5eAaImAgFnS	hořet
podkrovní	podkrovní	k2eAgFnSc1d1	podkrovní
část	část	k1gFnSc1	část
pravého	pravý	k2eAgNnSc2d1	pravé
křídla	křídlo	k1gNnSc2	křídlo
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
blíže	blízce	k6eAd2	blízce
Čelakovského	Čelakovský	k2eAgInSc2d1	Čelakovský
sadům	sad	k1gInPc3	sad
<g/>
)	)	kIx)	)
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
asi	asi	k9	asi
200	[number]	k4	200
m2	m2	k4	m2
střechy	střecha	k1gFnPc1	střecha
<g/>
.	.	kIx.	.
zasahovalo	zasahovat	k5eAaImAgNnS	zasahovat
na	na	k7c4	na
150	[number]	k4	150
hasičů	hasič	k1gMnPc2	hasič
z	z	k7c2	z
12	[number]	k4	12
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
a	a	k8xC	a
8	[number]	k4	8
dobrovolných	dobrovolný	k2eAgFnPc2d1	dobrovolná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
muzea	muzeum	k1gNnSc2	muzeum
ocenil	ocenit	k5eAaPmAgMnS	ocenit
profesionální	profesionální	k2eAgMnSc1d1	profesionální
a	a	k8xC	a
rychlý	rychlý	k2eAgInSc1d1	rychlý
zásah	zásah	k1gInSc1	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
nebyly	být	k5eNaImAgInP	být
žádné	žádný	k3yNgInPc1	žádný
exponáty	exponát	k1gInPc1	exponát
<g/>
,	,	kIx,	,
mluvčí	mluvčí	k1gFnSc1	mluvčí
hasičů	hasič	k1gMnPc2	hasič
předběžně	předběžně	k6eAd1	předběžně
vyčíslila	vyčíslit	k5eAaPmAgFnS	vyčíslit
škodu	škoda	k1gFnSc4	škoda
na	na	k7c4	na
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
muzea	muzeum	k1gNnSc2	muzeum
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
předběžné	předběžný	k2eAgFnSc2d1	předběžná
spekulace	spekulace	k1gFnSc2	spekulace
požár	požár	k1gInSc1	požár
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
stavební	stavební	k2eAgFnSc7d1	stavební
činností	činnost	k1gFnSc7	činnost
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
s	s	k7c7	s
elektroinstalací	elektroinstalace	k1gFnSc7	elektroinstalace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
plně	plně	k6eAd1	plně
nese	nést	k5eAaImIp3nS	nést
konsorcium	konsorcium	k1gNnSc1	konsorcium
stavebních	stavební	k2eAgFnPc2d1	stavební
firem	firma	k1gFnPc2	firma
M-P-I	M-P-I	k1gFnPc2	M-P-I
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
firem	firma	k1gFnPc2	firma
Metrostav	metrostav	k1gInSc1	metrostav
<g/>
,	,	kIx,	,
Průmstav	Průmstav	k1gFnPc2	Průmstav
a	a	k8xC	a
Imos	Imosa	k1gFnPc2	Imosa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
provádějí	provádět	k5eAaImIp3nP	provádět
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
Metrostavu	metrostav	k1gInSc2	metrostav
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
souvislost	souvislost	k1gFnSc1	souvislost
požáru	požár	k1gInSc2	požár
s	s	k7c7	s
probíhající	probíhající	k2eAgFnSc7d1	probíhající
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
nelze	lze	k6eNd1	lze
potvrdit	potvrdit	k5eAaPmF	potvrdit
ani	ani	k8xC	ani
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ČTK	ČTK	kA	ČTK
se	se	k3xPyFc4	se
za	za	k7c4	za
nejpravděpodobnější	pravděpodobný	k2eAgFnSc4d3	nejpravděpodobnější
příčinu	příčina	k1gFnSc4	příčina
zatím	zatím	k6eAd1	zatím
považuje	považovat	k5eAaImIp3nS	považovat
závada	závada	k1gFnSc1	závada
na	na	k7c6	na
elektroinstalaci	elektroinstalace	k1gFnSc6	elektroinstalace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
předběžného	předběžný	k2eAgNnSc2d1	předběžné
ohledání	ohledání	k1gNnSc2	ohledání
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
přehřát	přehřát	k5eAaPmF	přehřát
ventilátor	ventilátor	k1gInSc4	ventilátor
nebo	nebo	k8xC	nebo
přístroj	přístroj	k1gInSc4	přístroj
na	na	k7c6	na
likvidaci	likvidace	k1gFnSc6	likvidace
azbestu	azbest	k1gInSc2	azbest
<g/>
.	.	kIx.	.
</s>
<s>
Hořet	hořet	k5eAaImF	hořet
zřejmě	zřejmě	k6eAd1	zřejmě
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
místnosti	místnost	k1gFnSc6	místnost
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
oheň	oheň	k1gInSc1	oheň
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgFnPc4d1	velká
místnosti	místnost	k1gFnPc4	místnost
a	a	k8xC	a
následně	následně	k6eAd1	následně
prohořela	prohořet	k5eAaPmAgFnS	prohořet
střecha	střecha	k1gFnSc1	střecha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
stejně	stejně	k6eAd1	stejně
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
sejmuta	sejmout	k5eAaPmNgFnS	sejmout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
hašení	hašení	k1gNnSc2	hašení
požáru	požár	k1gInSc2	požár
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
přerušen	přerušen	k2eAgInSc4d1	přerušen
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
Severojižní	severojižní	k2eAgFnSc2d1	severojižní
magistrály	magistrála	k1gFnSc2	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
muzea	muzeum	k1gNnSc2	muzeum
převedena	převeden	k2eAgFnSc1d1	převedena
protější	protější	k2eAgFnSc1d1	protější
budova	budova	k1gFnSc1	budova
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
působilo	působit	k5eAaImAgNnS	působit
Rádio	rádio	k1gNnSc4	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
aktu	akt	k1gInSc2	akt
předání	předání	k1gNnSc2	předání
budovy	budova	k1gFnSc2	budova
muzeu	muzeum	k1gNnSc3	muzeum
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
někdejší	někdejší	k2eAgMnSc1d1	někdejší
redaktor	redaktor	k1gMnSc1	redaktor
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
redakce	redakce	k1gFnSc2	redakce
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
kultury	kultura	k1gFnSc2	kultura
ČR	ČR	kA	ČR
Martin	Martin	k1gMnSc1	Martin
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
objektu	objekt	k1gInSc2	objekt
byly	být	k5eAaImAgFnP	být
přesunuty	přesunut	k2eAgFnPc1d1	přesunuta
kanceláře	kancelář	k1gFnPc1	kancelář
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
Historického	historický	k2eAgNnSc2d1	historické
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
a	a	k8xC	a
část	část	k1gFnSc4	část
Knihovny	knihovna	k1gFnSc2	knihovna
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
užíváno	užíván	k2eAgNnSc1d1	užíváno
označení	označení	k1gNnSc1	označení
Nová	Nová	k1gFnSc1	Nová
budova	budova	k1gFnSc1	budova
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
krátkodobých	krátkodobý	k2eAgFnPc2d1	krátkodobá
výstav	výstava	k1gFnPc2	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Hostí	hostit	k5eAaImIp3nS	hostit
také	také	k9	také
přírodovědeckou	přírodovědecký	k2eAgFnSc4d1	Přírodovědecká
stálou	stálý	k2eAgFnSc4d1	stálá
expozici	expozice	k1gFnSc4	expozice
Archa	archa	k1gFnSc1	archa
Noemova	Noemův	k2eAgFnSc1d1	Noemova
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
sbírek	sbírka	k1gFnPc2	sbírka
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
budově	budova	k1gFnSc6	budova
pod	pod	k7c4	pod
Národní	národní	k2eAgNnSc4d1	národní
muzeum	muzeum	k1gNnSc4	muzeum
spadají	spadat	k5eAaPmIp3nP	spadat
také	také	k9	také
tyto	tento	k3xDgFnPc1	tento
expozice	expozice	k1gFnPc1	expozice
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
bývalé	bývalý	k2eAgNnSc1d1	bývalé
sídlo	sídlo	k1gNnSc1	sídlo
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
Vinohradská	vinohradský	k2eAgFnSc1d1	Vinohradská
1	[number]	k4	1
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Náprstkovo	Náprstkův	k2eAgNnSc1d1	Náprstkovo
muzeum	muzeum	k1gNnSc1	muzeum
asijských	asijský	k2eAgFnPc2d1	asijská
<g/>
,	,	kIx,	,
afrických	africký	k2eAgFnPc2d1	africká
a	a	k8xC	a
amerických	americký	k2eAgFnPc2d1	americká
kultur	kultura	k1gFnPc2	kultura
(	(	kIx(	(
<g/>
Betlémské	betlémský	k2eAgNnSc1d1	Betlémské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
České	český	k2eAgNnSc1d1	české
muzeum	muzeum	k1gNnSc1	muzeum
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
Karmelitská	karmelitský	k2eAgFnSc1d1	Karmelitská
ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Muzeum	muzeum	k1gNnSc1	muzeum
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
(	(	kIx(	(
<g/>
Novotného	Novotného	k2eAgFnSc1d1	Novotného
lávka	lávka	k1gFnSc1	lávka
1	[number]	k4	1
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Muzeum	muzeum	k1gNnSc1	muzeum
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
(	(	kIx(	(
<g/>
Ke	k	k7c3	k
Karlovu	Karlův	k2eAgInSc3d1	Karlův
20	[number]	k4	20
<g/>
)	)	kIx)	)
Památník	památník	k1gInSc1	památník
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Ježka	Ježek	k1gMnSc2	Ježek
(	(	kIx(	(
<g/>
Kaprova	kaprův	k2eAgFnSc1d1	Kaprova
ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Památník	památník	k1gInSc1	památník
<g />
.	.	kIx.	.
</s>
<s>
Antonína	Antonín	k1gMnSc4	Antonín
Dvořáka	Dvořák	k1gMnSc4	Dvořák
(	(	kIx(	(
<g/>
Nelahozeves	Nelahozeves	k1gMnSc1	Nelahozeves
<g/>
)	)	kIx)	)
Památník	památník	k1gInSc1	památník
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
(	(	kIx(	(
<g/>
Jabkenice	Jabkenice	k1gFnSc1	Jabkenice
<g/>
)	)	kIx)	)
Památník	památník	k1gInSc1	památník
Josefa	Josef	k1gMnSc2	Josef
Suka	Suk	k1gMnSc2	Suk
(	(	kIx(	(
<g/>
Křečovice	Křečovice	k1gFnSc1	Křečovice
<g/>
)	)	kIx)	)
Lapidárium	lapidárium	k1gNnSc1	lapidárium
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
Výstaviště	výstaviště	k1gNnSc1	výstaviště
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
7	[number]	k4	7
-	-	kIx~	-
Holešovice	Holešovice	k1gFnPc1	Holešovice
Národopisné	národopisný	k2eAgNnSc1d1	Národopisné
muzeum	muzeum	k1gNnSc1	muzeum
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
Letohrádek	letohrádek	k1gInSc1	letohrádek
Kinských	Kinská	k1gFnPc2	Kinská
<g/>
)	)	kIx)	)
Praha	Praha	k1gFnSc1	Praha
5	[number]	k4	5
Národní	národní	k2eAgInSc1d1	národní
památník	památník	k1gInSc1	památník
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
(	(	kIx(	(
<g/>
U	u	k7c2	u
Památníku	památník	k1gInSc2	památník
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Památník	památník	k1gInSc1	památník
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Ladislava	Ladislav	k1gMnSc2	Ladislav
Riegra	Riegr	k1gMnSc2	Riegr
(	(	kIx(	(
<g/>
Palackého	Palacký	k1gMnSc2	Palacký
ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Muzeum	muzeum	k1gNnSc1	muzeum
české	český	k2eAgFnSc2d1	Česká
loutky	loutka	k1gFnSc2	loutka
a	a	k8xC	a
cirkusu	cirkus	k1gInSc2	cirkus
(	(	kIx(	(
<g/>
Prachatice	Prachatice	k1gFnPc1	Prachatice
<g/>
)	)	kIx)	)
Zámek	zámek	k1gInSc1	zámek
Vrchotovy	Vrchotův	k2eAgFnSc2d1	Vrchotův
Janovice	Janovice	k1gFnPc4	Janovice
Klavír	klavír	k1gInSc1	klavír
<g/>
?	?	kIx.	?
</s>
<s>
Klavír	klavír	k1gInSc1	klavír
<g/>
!	!	kIx.	!
</s>
<s>
Klavír	klavír	k1gInSc1	klavír
<g/>
!	!	kIx.	!
</s>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
i	i	k8xC	i
krása	krása	k1gFnSc1	krása
klávesových	klávesový	k2eAgInPc2d1	klávesový
nástrojů	nástroj	k1gInPc2	nástroj
v	v	k7c6	v
proměnách	proměna	k1gFnPc6	proměna
staletí	staletí	k1gNnPc2	staletí
(	(	kIx(	(
<g/>
Státní	státní	k2eAgInSc1d1	státní
zámek	zámek	k1gInSc1	zámek
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
)	)	kIx)	)
Památník	památník	k1gInSc1	památník
Jiřího	Jiří	k1gMnSc2	Jiří
Melantricha	Melantrich	k1gMnSc2	Melantrich
z	z	k7c2	z
Aventina	Aventin	k1gMnSc2	Aventin
(	(	kIx(	(
<g/>
Rožďalovice	Rožďalovice	k1gFnSc1	Rožďalovice
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Knihovna	knihovna	k1gFnSc1	knihovna
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
je	být	k5eAaImIp3nS	být
také	také	k9	také
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
část	část	k1gFnSc1	část
knihovny	knihovna	k1gFnSc2	knihovna
a	a	k8xC	a
oddělení	oddělení	k1gNnSc2	oddělení
rukopisů	rukopis	k1gInPc2	rukopis
a	a	k8xC	a
starých	starý	k2eAgInPc2d1	starý
tisků	tisk	k1gInPc2	tisk
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
budově	budova	k1gFnSc6	budova
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
unikátní	unikátní	k2eAgInSc1d1	unikátní
soubor	soubor	k1gInSc1	soubor
historických	historický	k2eAgFnPc2d1	historická
i	i	k8xC	i
současných	současný	k2eAgFnPc2d1	současná
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
Místodržitelském	místodržitelský	k2eAgInSc6d1	místodržitelský
letohrádku	letohrádek	k1gInSc6	letohrádek
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Stromovce	Stromovka	k1gFnSc6	Stromovka
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
NM	NM	kA	NM
také	také	k9	také
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
provoz	provoz	k1gInSc4	provoz
Muzea	muzeum	k1gNnSc2	muzeum
knihy	kniha	k1gFnSc2	kniha
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
ve	v	k7c6	v
Žďáru	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Archiv	archiv	k1gInSc1	archiv
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
Národnímu	národní	k2eAgNnSc3d1	národní
muzeu	muzeum	k1gNnSc3	muzeum
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnSc4	jeho
pracoviště	pracoviště	k1gNnSc4	pracoviště
a	a	k8xC	a
studovna	studovna	k1gFnSc1	studovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
7	[number]	k4	7
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Zátorách	Zátora	k1gFnPc6	Zátora
<g/>
.	.	kIx.	.
</s>
<s>
ČERNÝ	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Matouš	Matouš	k1gMnSc1	Matouš
<g/>
.	.	kIx.	.
</s>
<s>
Museum	museum	k1gNnSc1	museum
království	království	k1gNnSc2	království
Českého	český	k2eAgNnSc2d1	české
:	:	kIx,	:
Stručná	stručný	k2eAgFnSc1d1	stručná
zpráva	zpráva	k1gFnSc1	zpráva
historická	historický	k2eAgFnSc1d1	historická
i	i	k8xC	i
statistická	statistický	k2eAgFnSc1d1	statistická
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
vl	vl	k?	vl
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
<g/>
,	,	kIx,	,
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
HANUŠ	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
museum	museum	k1gNnSc1	museum
a	a	k8xC	a
naše	náš	k3xOp1gNnSc4	náš
obrození	obrození	k1gNnSc4	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgNnSc4d1	kulturní
a	a	k8xC	a
národní	národní	k2eAgNnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
šlechty	šlechta	k1gFnSc2	šlechta
české	český	k2eAgFnSc2d1	Česká
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
prvé	prvý	k4xOgFnSc6	prvý
půli	půle	k1gFnSc6	půle
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
a	a	k8xC	a
počátky	počátek	k1gInPc4	počátek
musea	museum	k1gNnSc2	museum
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Národní	národní	k2eAgNnSc1d1	národní
museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
364	[number]	k4	364
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
HANUŠ	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
museum	museum	k1gNnSc1	museum
a	a	k8xC	a
naše	náš	k3xOp1gNnSc4	náš
obrození	obrození	k1gNnSc4	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Založení	založení	k1gNnSc1	založení
vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
musea	museum	k1gNnSc2	museum
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
vývoj	vývoj	k1gInSc4	vývoj
do	do	k7c2	do
konce	konec	k1gInSc2	konec
doby	doba	k1gFnSc2	doba
Šternberkovy	Šternberkův	k2eAgFnSc2d1	Šternberkova
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
<g/>
-	-	kIx~	-
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Národní	národní	k2eAgNnSc1d1	národní
museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
505	[number]	k4	505
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
Průvodce	průvodce	k1gMnSc1	průvodce
sbírkami	sbírka	k1gFnPc7	sbírka
Musea	museum	k1gNnPc1	museum
království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Společnost	společnost	k1gFnSc1	společnost
Musea	museum	k1gNnSc2	museum
království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Sklenář	Sklenář	k1gMnSc1	Sklenář
<g/>
:	:	kIx,	:
Obraz	obraz	k1gInSc1	obraz
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Paseka	paseka	k1gFnSc1	paseka
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Národní	národní	k2eAgFnSc2d1	národní
muzeum	muzeum	k1gNnSc4	muzeum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
instituce	instituce	k1gFnSc2	instituce
</s>
