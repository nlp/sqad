<s>
Katakana	Katakana	k1gFnSc1	Katakana
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
fonetický	fonetický	k2eAgInSc4d1	fonetický
přepis	přepis	k1gInSc4	přepis
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
slov	slovo	k1gNnPc2	slovo
převzatých	převzatý	k2eAgMnPc2d1	převzatý
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
