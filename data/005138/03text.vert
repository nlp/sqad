<s>
Sedna	Sedna	k1gFnSc1	Sedna
je	být	k5eAaImIp3nS	být
inuitská	inuitský	k2eAgFnSc1d1	inuitská
bohyně	bohyně	k1gFnSc1	bohyně
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
všech	všecek	k3xTgMnPc2	všecek
živých	živý	k2eAgMnPc2d1	živý
tvorů	tvor	k1gMnPc2	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
má	mít	k5eAaImIp3nS	mít
ruce	ruka	k1gFnPc4	ruka
s	s	k7c7	s
osekanými	osekaný	k2eAgInPc7d1	osekaný
prsty	prst	k1gInPc7	prst
připomínající	připomínající	k2eAgFnSc2d1	připomínající
tulení	tulení	k2eAgFnSc2d1	tulení
ploutve	ploutev	k1gFnSc2	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tuleni	tuleň	k1gMnPc7	tuleň
prý	prý	k9	prý
také	také	k9	také
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
hlubinách	hlubina	k1gFnPc6	hlubina
<g/>
.	.	kIx.	.
</s>
<s>
Uctívána	uctíván	k2eAgFnSc1d1	uctívána
je	být	k5eAaImIp3nS	být
především	především	k9	především
rybáři	rybář	k1gMnPc1	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Sedně	Sedna	k1gFnSc6	Sedna
</s>
