<s>
Americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
Karlík	Karlík	k1gMnSc1	Karlík
a	a	k8xC	a
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
čokoládu	čokoláda	k1gFnSc4	čokoláda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Charlie	Charlie	k1gMnSc1	Charlie
and	and	k?	and
the	the	k?	the
Chocolate	Chocolat	k1gInSc5	Chocolat
Factory	Factor	k1gMnPc7	Factor
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc4d1	natočený
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
britského	britský	k2eAgMnSc2d1	britský
autora	autor	k1gMnSc2	autor
Roalda	Roald	k1gMnSc2	Roald
Dahla	Dahl	k1gMnSc2	Dahl
režíroval	režírovat	k5eAaImAgMnS	režírovat
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc4	Burton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Johnny	Johnen	k2eAgInPc4d1	Johnen
Depp	Depp	k1gInSc4	Depp
jako	jako	k9	jako
Willy	Willa	k1gFnSc2	Willa
Wonka	Wonek	k1gInSc2	Wonek
<g/>
,	,	kIx,	,
Freddie	Freddie	k1gFnSc2	Freddie
Highmore	Highmor	k1gInSc5	Highmor
jako	jako	k8xC	jako
Karlík	Karlík	k1gMnSc1	Karlík
Bucket	Bucket	k1gMnSc1	Bucket
a	a	k8xC	a
AnnaSophia	AnnaSophia	k1gFnSc1	AnnaSophia
Robb	Robba	k1gFnPc2	Robba
jako	jako	k8xC	jako
Fialka	fialka	k1gFnSc1	fialka
Garderóbová	Garderóbová	k1gFnSc1	Garderóbová
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
Danny	Danna	k1gMnSc2	Danna
Elfman	Elfman	k1gMnSc1	Elfman
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
adaptací	adaptace	k1gFnSc7	adaptace
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
Pan	Pan	k1gMnSc1	Pan
Wonka	Wonka	k1gMnSc1	Wonka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
čokoládovna	čokoládovna	k1gFnSc1	čokoládovna
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Willy	Willa	k1gMnSc2	Willa
Wonka	Wonek	k1gMnSc2	Wonek
&	&	k?	&
the	the	k?	the
Chocolate	Chocolat	k1gInSc5	Chocolat
Factory	Factor	k1gMnPc4	Factor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
Karlík	Karlík	k1gMnSc1	Karlík
Bucket	Bucket	k1gInSc4	Bucket
pocházející	pocházející	k2eAgInSc4d1	pocházející
z	z	k7c2	z
velice	velice	k6eAd1	velice
chudé	chudý	k2eAgFnSc2d1	chudá
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
doslechne	doslechnout	k5eAaPmIp3nS	doslechnout
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
o	o	k7c6	o
zlatých	zlatý	k2eAgInPc6d1	zlatý
kupónech	kupón	k1gInPc6	kupón
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Willy	Will	k1gInPc4	Will
Wonka	Wonko	k1gNnSc2	Wonko
umístil	umístit	k5eAaPmAgMnS	umístit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
čokolád	čokoláda	k1gFnPc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
pak	pak	k6eAd1	pak
nechal	nechat	k5eAaPmAgMnS	nechat
rozvézt	rozvézt	k5eAaPmF	rozvézt
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
kupóny	kupón	k1gInPc1	kupón
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
najdou	najít	k5eAaPmIp3nP	najít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kupón	kupón	k1gInSc4	kupón
nalezený	nalezený	k2eAgInSc4d1	nalezený
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
padělek	padělek	k1gInSc1	padělek
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
Karlík	Karlík	k1gMnSc1	Karlík
Bucket	Bucket	k1gMnSc1	Bucket
dostává	dostávat	k5eAaImIp3nS	dostávat
čokoládu	čokoláda	k1gFnSc4	čokoláda
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
chudá	chudý	k2eAgFnSc1d1	chudá
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
narozeniny	narozeniny	k1gFnPc4	narozeniny
a	a	k8xC	a
dostal	dostat	k5eAaPmAgInS	dostat
Wonkovu	Wonkův	k2eAgFnSc4d1	Wonkova
čokoládu	čokoláda	k1gFnSc4	čokoláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kupón	kupón	k1gInSc4	kupón
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nebyl	být	k5eNaImAgMnS	být
a	a	k8xC	a
Karlík	Karlík	k1gMnSc1	Karlík
byl	být	k5eAaImAgMnS	být
zklamaný	zklamaný	k2eAgMnSc1d1	zklamaný
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
mu	on	k3xPp3gMnSc3	on
dědeček	dědeček	k1gMnSc1	dědeček
dá	dát	k5eAaPmIp3nS	dát
penízek	penízek	k1gInSc4	penízek
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc1	jeho
tajná	tajný	k2eAgFnSc1d1	tajná
rezerva	rezerva	k1gFnSc1	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Pošle	pošle	k6eAd1	pošle
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
obchodu	obchod	k1gInSc2	obchod
koupit	koupit	k5eAaPmF	koupit
Wonkovu	Wonkův	k2eAgFnSc4d1	Wonkova
čokoládu	čokoláda	k1gFnSc4	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
Karlík	Karlík	k1gMnSc1	Karlík
vrátí	vrátit	k5eAaPmIp3nP	vrátit
a	a	k8xC	a
společně	společně	k6eAd1	společně
ji	on	k3xPp3gFnSc4	on
rozbalí	rozbalit	k5eAaPmIp3nP	rozbalit
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc4d1	zlatý
kupón	kupón	k1gInSc4	kupón
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zase	zase	k9	zase
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
malý	malý	k2eAgMnSc1d1	malý
Karlík	Karlík	k1gMnSc1	Karlík
opět	opět	k6eAd1	opět
prožívá	prožívat	k5eAaImIp3nS	prožívat
zklamání	zklamání	k1gNnSc4	zklamání
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
objeví	objevit	k5eAaPmIp3nS	objevit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
desetidolarovou	desetidolarový	k2eAgFnSc4d1	desetidolarová
bankovku	bankovka	k1gFnSc4	bankovka
a	a	k8xC	a
koupí	koupit	k5eAaPmIp3nS	koupit
si	se	k3xPyFc3	se
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
čokoládu	čokoláda	k1gFnSc4	čokoláda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
najde	najít	k5eAaPmIp3nS	najít
poslední	poslední	k2eAgFnSc1d1	poslední
z	z	k7c2	z
pěti	pět	k4xCc2	pět
zlatých	zlatý	k2eAgInPc2d1	zlatý
kupónů	kupón	k1gInPc2	kupón
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
najdou	najít	k5eAaPmIp3nP	najít
<g/>
,	,	kIx,	,
exkurzi	exkurze	k1gFnSc4	exkurze
do	do	k7c2	do
gigantické	gigantický	k2eAgFnSc2d1	gigantická
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
čokoládu	čokoláda	k1gFnSc4	čokoláda
podivína	podivín	k1gMnSc2	podivín
Willyho	Willy	k1gMnSc2	Willy
Wonky	Wonka	k1gMnSc2	Wonka
(	(	kIx(	(
<g/>
Johnny	Johnna	k1gMnSc2	Johnna
Depp	Depp	k1gMnSc1	Depp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prohlídky	prohlídka	k1gFnSc2	prohlídka
se	se	k3xPyFc4	se
čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
nenažraný	nažraný	k2eNgInSc1d1	nenažraný
Augustus	Augustus	k1gInSc1	Augustus
Kdoule	kdoule	k1gFnSc1	kdoule
<g/>
,	,	kIx,	,
namyšlená	namyšlený	k2eAgFnSc1d1	namyšlená
Fialka	fialka	k1gFnSc1	fialka
Garderóbová	Garderóbová	k1gFnSc1	Garderóbová
<g/>
,	,	kIx,	,
přechytralý	přechytralý	k2eAgInSc1d1	přechytralý
Mikey	Mike	k1gMnPc7	Mike
Televykuk	Televykuko	k1gNnPc2	Televykuko
a	a	k8xC	a
rozmazlená	rozmazlený	k2eAgNnPc4d1	rozmazlené
Veruka	Veruk	k1gMnSc2	Veruk
Saltini	Saltin	k2eAgMnPc1d1	Saltin
<g/>
,	,	kIx,	,
stanou	stanout	k5eAaPmIp3nP	stanout
oběťmi	oběť	k1gFnPc7	oběť
své	svůj	k3xOyFgFnSc2	svůj
chamtivosti	chamtivost	k1gFnSc2	chamtivost
a	a	k8xC	a
nafoukanosti	nafoukanost	k1gFnSc2	nafoukanost
a	a	k8xC	a
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jen	jen	k9	jen
skromný	skromný	k2eAgMnSc1d1	skromný
Karlík	Karlík	k1gMnSc1	Karlík
Bucket	Bucket	k1gMnSc1	Bucket
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
Willymu	Willym	k1gInSc3	Willym
sympatický	sympatický	k2eAgMnSc1d1	sympatický
už	už	k6eAd1	už
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Willy	Will	k1gMnPc4	Will
Karlikovi	Karlikův	k2eAgMnPc1d1	Karlikův
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
Willy	Will	k1gInPc1	Will
pak	pak	k6eAd1	pak
Karlíka	Karlík	k1gMnSc4	Karlík
zklame	zklamat	k5eAaPmIp3nS	zklamat
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nevyřešené	vyřešený	k2eNgFnSc3d1	nevyřešená
minulosti	minulost	k1gFnSc3	minulost
a	a	k8xC	a
Karlík	Karlík	k1gMnSc1	Karlík
jeho	jeho	k3xOp3gFnSc4	jeho
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dědicem	dědic	k1gMnSc7	dědic
jeho	jeho	k3xOp3gFnSc2	jeho
továrny	továrna	k1gFnSc2	továrna
<g/>
,	,	kIx,	,
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Willyho	Willyze	k6eAd1	Willyze
to	ten	k3xDgNnSc1	ten
překvapí	překvapit	k5eAaPmIp3nS	překvapit
a	a	k8xC	a
zdrtí	zdrtit	k5eAaPmIp3nS	zdrtit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nedá	dát	k5eNaPmIp3nS	dát
a	a	k8xC	a
schválně	schválně	k6eAd1	schválně
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
s	s	k7c7	s
Karlíkem	Karlík	k1gMnSc7	Karlík
setkat	setkat	k5eAaPmF	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
usmíří	usmířit	k5eAaPmIp3nS	usmířit
a	a	k8xC	a
Karlík	Karlík	k1gMnSc1	Karlík
pomůže	pomoct	k5eAaPmIp3nS	pomoct
Willymu	Willym	k1gInSc2	Willym
srovnat	srovnat	k5eAaPmF	srovnat
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
zubařem	zubař	k1gMnSc7	zubař
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Willyho	Willy	k1gMnSc4	Willy
v	v	k7c4	v
dětství	dětství	k1gNnSc4	dětství
uzurpoval	uzurpovat	k5eAaBmAgInS	uzurpovat
a	a	k8xC	a
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
Willy	Willa	k1gFnPc4	Willa
od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
nepromluvil	promluvit	k5eNaPmAgMnS	promluvit
<g/>
.	.	kIx.	.
</s>
<s>
Karlík	Karlík	k1gMnSc1	Karlík
pak	pak	k6eAd1	pak
začne	začít	k5eAaPmIp3nS	začít
Willymu	Willym	k1gInSc2	Willym
pomáhat	pomáhat	k5eAaImF	pomáhat
a	a	k8xC	a
Willy	Willa	k1gFnPc1	Willa
se	se	k3xPyFc4	se
spřátelí	spřátelit	k5eAaPmIp3nP	spřátelit
i	i	k9	i
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
