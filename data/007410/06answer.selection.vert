<s>
Americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
Karlík	Karlík	k1gMnSc1	Karlík
a	a	k8xC	a
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
čokoládu	čokoláda	k1gFnSc4	čokoláda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Charlie	Charlie	k1gMnSc1	Charlie
and	and	k?	and
the	the	k?	the
Chocolate	Chocolat	k1gInSc5	Chocolat
Factory	Factor	k1gMnPc7	Factor
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc4d1	natočený
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
britského	britský	k2eAgMnSc2d1	britský
autora	autor	k1gMnSc2	autor
Roalda	Roald	k1gMnSc2	Roald
Dahla	Dahl	k1gMnSc2	Dahl
režíroval	režírovat	k5eAaImAgMnS	režírovat
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc4	Burton
<g/>
.	.	kIx.	.
</s>
