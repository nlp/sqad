<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1741	[number]	k4	1741
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
do	do	k7c2	do
Prešpurku	Prešpurku	k?	Prešpurku
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistila	zjistit	k5eAaPmAgFnS	zjistit
postoj	postoj	k1gInSc4	postoj
Uher	Uhry	k1gFnPc2	Uhry
k	k	k7c3	k
pragmatické	pragmatický	k2eAgFnSc3d1	pragmatická
sankci	sankce	k1gFnSc3	sankce
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
dědičné	dědičný	k2eAgNnSc4d1	dědičné
právo	právo	k1gNnSc4	právo
pro	pro	k7c4	pro
ženské	ženský	k2eAgMnPc4d1	ženský
následovníky	následovník	k1gMnPc4	následovník
trůnu	trůn	k1gInSc2	trůn
v	v	k7c4	v
Habsburské	habsburský	k2eAgFnPc4d1	habsburská
monarchie	monarchie	k1gFnPc4	monarchie
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Habsburků	Habsburk	k1gMnPc2	Habsburk
včetně	včetně	k7c2	včetně
Uher	Uhry	k1gFnPc2	Uhry
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
podpoří	podpořit	k5eAaPmIp3nS	podpořit
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
</s>
