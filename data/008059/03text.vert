<s>
Robert	Robert	k1gMnSc1	Robert
Pelikán	Pelikán	k1gMnSc1	Pelikán
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1979	[number]	k4	1979
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
advokát	advokát	k1gMnSc1	advokát
a	a	k8xC	a
od	od	k7c2	od
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
za	za	k7c2	za
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
až	až	k9	až
2015	[number]	k4	2015
prvním	první	k4xOgMnSc7	první
náměstkem	náměstek	k1gMnSc7	náměstek
ministryně	ministryně	k1gFnSc2	ministryně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
právnické	právnický	k2eAgFnSc2d1	právnická
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
profesorka	profesorka	k1gFnSc1	profesorka
a	a	k8xC	a
soudkyně	soudkyně	k1gFnSc1	soudkyně
Irena	Irena	k1gFnSc1	Irena
Pelikánová	Pelikánová	k1gFnSc1	Pelikánová
(	(	kIx(	(
<g/>
profesorka	profesorka	k1gFnSc1	profesorka
srovnávacího	srovnávací	k2eAgNnSc2d1	srovnávací
práva	právo	k1gNnSc2	právo
obchodního	obchodní	k2eAgNnSc2d1	obchodní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Dragutin	Dragutin	k1gMnSc1	Dragutin
Pelikán	Pelikán	k1gMnSc1	Pelikán
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
právními	právní	k2eAgFnPc7d1	právní
dějinami	dějiny	k1gFnPc7	dějiny
(	(	kIx(	(
<g/>
docent	docent	k1gMnSc1	docent
vědeckého	vědecký	k2eAgInSc2d1	vědecký
komunismu	komunismus	k1gInSc2	komunismus
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Tomáš	Tomáš	k1gMnSc1	Tomáš
je	být	k5eAaImIp3nS	být
advokátem	advokát	k1gMnSc7	advokát
<g/>
,	,	kIx,	,
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
bratr	bratr	k1gMnSc1	bratr
Petr	Petr	k1gMnSc1	Petr
Pelikán	Pelikán	k1gMnSc1	Pelikán
je	být	k5eAaImIp3nS	být
arabistou	arabista	k1gMnSc7	arabista
a	a	k8xC	a
honorárním	honorární	k2eAgMnSc7d1	honorární
konzulem	konzul	k1gMnSc7	konzul
Súdánské	súdánský	k2eAgFnSc2d1	súdánská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Pelikán	Pelikán	k1gMnSc1	Pelikán
chtěl	chtít	k5eAaImAgMnS	chtít
původně	původně	k6eAd1	původně
studovat	studovat	k5eAaImF	studovat
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ale	ale	k8xC	ale
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2003	[number]	k4	2003
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
jako	jako	k9	jako
externí	externí	k2eAgMnSc1d1	externí
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
katedry	katedra	k1gFnSc2	katedra
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc2	doktor
práv	práv	k2eAgMnSc1d1	práv
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
Odborně	odborně	k6eAd1	odborně
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
především	především	k6eAd1	především
civilnímu	civilní	k2eAgInSc3d1	civilní
a	a	k8xC	a
obchodnímu	obchodní	k2eAgNnSc3d1	obchodní
právu	právo	k1gNnSc3	právo
<g/>
.	.	kIx.	.
</s>
<s>
Pracovně	pracovně	k6eAd1	pracovně
začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k8xC	jako
advokátní	advokátní	k2eAgMnSc1d1	advokátní
koncipient	koncipient	k1gMnSc1	koncipient
v	v	k7c6	v
kanceláři	kancelář	k1gFnSc6	kancelář
Císař	Císař	k1gMnSc1	Císař
<g/>
,	,	kIx,	,
Češka	Češka	k1gFnSc1	Češka
<g/>
,	,	kIx,	,
Smutný	smutný	k2eAgInSc1d1	smutný
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
až	až	k9	až
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
složil	složit	k5eAaPmAgMnS	složit
advokátní	advokátní	k2eAgFnSc4d1	advokátní
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
advokátem	advokát	k1gMnSc7	advokát
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
advokátní	advokátní	k2eAgFnSc3d1	advokátní
kanceláři	kancelář	k1gFnSc3	kancelář
Linklaters	Linklatersa	k1gFnPc2	Linklatersa
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
až	až	k9	až
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
advokátní	advokátní	k2eAgFnSc4d1	advokátní
kancelář	kancelář	k1gFnSc4	kancelář
Vrána	vrána	k1gFnSc1	vrána
&	&	k?	&
Pelikán	pelikán	k1gInSc1	pelikán
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
advokát	advokát	k1gMnSc1	advokát
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
například	například	k6eAd1	například
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
jejímuž	jejíž	k3xOyRp3gMnSc3	jejíž
synovi	syn	k1gMnSc3	syn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
amputoval	amputovat	k5eAaBmAgMnS	amputovat
eskalátor	eskalátor	k1gInSc4	eskalátor
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
metru	metr	k1gInSc6	metr
tři	tři	k4xCgInPc4	tři
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
Hájil	hájit	k5eAaImAgMnS	hájit
také	také	k9	také
město	město	k1gNnSc1	město
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
firem	firma	k1gFnPc2	firma
o	o	k7c6	o
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
sportovně-kulturního	sportovněulturní	k2eAgNnSc2d1	sportovně-kulturní
centra	centrum	k1gNnSc2	centrum
KV	KV	kA	KV
Arena	Aren	k1gInSc2	Aren
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
výkon	výkon	k1gInSc1	výkon
advokacie	advokacie	k1gFnSc2	advokacie
pozastavil	pozastavit	k5eAaPmAgInS	pozastavit
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Pelikán	Pelikán	k1gMnSc1	Pelikán
je	být	k5eAaImIp3nS	být
rozvedený	rozvedený	k2eAgMnSc1d1	rozvedený
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
jej	on	k3xPp3gMnSc4	on
ministr	ministr	k1gMnSc1	ministr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
Martin	Martin	k1gMnSc1	Martin
Kuba	Kuba	k1gMnSc1	Kuba
nominoval	nominovat	k5eAaBmAgMnS	nominovat
na	na	k7c4	na
člena	člen	k1gMnSc4	člen
rady	rada	k1gFnSc2	rada
Českého	český	k2eAgInSc2d1	český
telekomunikačního	telekomunikační	k2eAgInSc2d1	telekomunikační
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc4	jeho
nominaci	nominace	k1gFnSc4	nominace
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
spekulací	spekulace	k1gFnPc2	spekulace
za	za	k7c7	za
tím	ten	k3xDgNnSc7	ten
stál	stát	k5eAaImAgInS	stát
Pelikánův	Pelikánův	k2eAgInSc4d1	Pelikánův
blog	blog	k1gInSc4	blog
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
politiku	politika	k1gFnSc4	politika
vlády	vláda	k1gFnSc2	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
ekonomických	ekonomický	k2eAgMnPc2d1	ekonomický
poradců	poradce	k1gMnPc2	poradce
hnutí	hnutí	k1gNnPc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
přivedl	přivést	k5eAaPmAgMnS	přivést
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
a	a	k8xC	a
udělal	udělat	k5eAaPmAgInS	udělat
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
ředitele	ředitel	k1gMnSc2	ředitel
právní	právní	k2eAgFnSc2d1	právní
sekce	sekce	k1gFnSc2	sekce
<g/>
.	.	kIx.	.
</s>
<s>
Post	post	k1gInSc1	post
zastával	zastávat	k5eAaImAgInS	zastávat
do	do	k7c2	do
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
totiž	totiž	k9	totiž
nastala	nastat	k5eAaPmAgFnS	nastat
roztržka	roztržka	k1gFnSc1	roztržka
mezi	mezi	k7c7	mezi
ministryní	ministryně	k1gFnSc7	ministryně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Helenou	Helena	k1gFnSc7	Helena
Válkovou	Válková	k1gFnSc7	Válková
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
první	první	k4xOgMnSc1	první
náměstkyní	náměstkyně	k1gFnSc7	náměstkyně
Hanou	Hana	k1gFnSc7	Hana
Marvanovou	Marvanová	k1gFnSc7	Marvanová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
Marvanové	Marvanové	k2eAgInSc3d1	Marvanové
z	z	k7c2	z
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgMnS	být
Robert	Robert	k1gMnSc1	Robert
Pelikán	Pelikán	k1gMnSc1	Pelikán
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
jmenován	jmenován	k2eAgInSc4d1	jmenován
1	[number]	k4	1
<g/>
.	.	kIx.	.
náměstkem	náměstek	k1gMnSc7	náměstek
ministryně	ministryně	k1gFnSc2	ministryně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
pro	pro	k7c4	pro
úsek	úsek	k1gInSc4	úsek
justiční	justiční	k2eAgInSc1d1	justiční
a	a	k8xC	a
legislativní	legislativní	k2eAgInSc1d1	legislativní
<g/>
.	.	kIx.	.
</s>
<s>
Prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
např.	např.	kA	např.
zásadní	zásadní	k2eAgFnSc4d1	zásadní
novelu	novela	k1gFnSc4	novela
nového	nový	k2eAgInSc2d1	nový
občanského	občanský	k2eAgInSc2d1	občanský
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
státním	státní	k2eAgMnSc7d1	státní
zástupcem	zástupce	k1gMnSc7	zástupce
Pavlem	Pavel	k1gMnSc7	Pavel
Zemanem	Zeman	k1gMnSc7	Zeman
<g/>
,	,	kIx,	,
spor	spor	k1gInSc4	spor
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgMnS	týkat
návrhu	návrh	k1gInSc3	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státním	státní	k2eAgNnSc6d1	státní
zastupitelství	zastupitelství	k1gNnSc6	zastupitelství
<g/>
.	.	kIx.	.
</s>
<s>
Pelikán	Pelikán	k1gMnSc1	Pelikán
totiž	totiž	k9	totiž
zastával	zastávat	k5eAaImAgMnS	zastávat
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
kontrola	kontrola	k1gFnSc1	kontrola
nad	nad	k7c7	nad
státními	státní	k2eAgMnPc7d1	státní
zástupci	zástupce	k1gMnPc7	zástupce
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
exekutivy	exekutiva	k1gFnSc2	exekutiva
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ze	z	k7c2	z
státních	státní	k2eAgMnPc2d1	státní
zástupců	zástupce	k1gMnPc2	zástupce
stane	stanout	k5eAaPmIp3nS	stanout
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
moc	moc	k6eAd1	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
podala	podat	k5eAaPmAgFnS	podat
ministryně	ministryně	k1gFnSc1	ministryně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Helena	Helena	k1gFnSc1	Helena
Válková	Válková	k1gFnSc1	Válková
svou	svůj	k3xOyFgFnSc4	svůj
demisi	demise	k1gFnSc4	demise
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc3	březen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
následně	následně	k6eAd1	následně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kandidátem	kandidát	k1gMnSc7	kandidát
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
na	na	k7c4	na
post	post	k1gInSc4	post
nového	nový	k2eAgMnSc2d1	nový
ministra	ministr	k1gMnSc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
bude	být	k5eAaImBp3nS	být
právě	právě	k6eAd1	právě
Robert	Robert	k1gMnSc1	Robert
Pelikán	Pelikán	k1gMnSc1	Pelikán
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
jej	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
médií	médium	k1gNnPc2	médium
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
ministr	ministr	k1gMnSc1	ministr
jezdí	jezdit	k5eAaImIp3nS	jezdit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
místo	místo	k6eAd1	místo
autem	auto	k1gNnSc7	auto
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2017	[number]	k4	2017
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
týdeník	týdeník	k1gInSc4	týdeník
Respekt	respekt	k1gInSc1	respekt
komentoval	komentovat	k5eAaBmAgInS	komentovat
žádost	žádost	k1gFnSc4	žádost
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
o	o	k7c6	o
zbavení	zbavení	k1gNnSc6	zbavení
imunity	imunita	k1gFnSc2	imunita
poslanců	poslanec	k1gMnPc2	poslanec
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
a	a	k8xC	a
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Faltýnka	Faltýnka	k1gFnSc1	Faltýnka
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
říct	říct	k5eAaPmF	říct
mohu	moct	k5eAaImIp1nS	moct
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
každému	každý	k3xTgNnSc3	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
alespoň	alespoň	k9	alespoň
zdálky	zdálky	k6eAd1	zdálky
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vyšetřují	vyšetřovat	k5eAaImIp3nP	vyšetřovat
takové	takový	k3xDgFnPc1	takový
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
zcela	zcela	k6eAd1	zcela
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediným	jediný	k2eAgInSc7d1	jediný
možným	možný	k2eAgInSc7d1	možný
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
podanou	podaný	k2eAgFnSc4d1	podaná
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
těchto	tento	k3xDgFnPc2	tento
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
má	mít	k5eAaImIp3nS	mít
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
nepochybně	pochybně	k6eNd1	pochybně
pravdu	pravda	k1gFnSc4	pravda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	s	k7c7	s
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
rozhovoru	rozhovor	k1gInSc2	rozhovor
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
přesto	přesto	k6eAd1	přesto
vyšel	vyjít	k5eAaPmAgInS	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
Odvolání	odvolání	k1gNnSc1	odvolání
Pelikána	Pelikán	k1gMnSc2	Pelikán
z	z	k7c2	z
postu	post	k1gInSc2	post
ministra	ministr	k1gMnSc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
ČR	ČR	kA	ČR
pak	pak	k6eAd1	pak
požadovali	požadovat	k5eAaImAgMnP	požadovat
poslanci	poslanec	k1gMnPc1	poslanec
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
hnutí	hnutí	k1gNnSc1	hnutí
STAN	stan	k1gInSc1	stan
<g/>
.	.	kIx.	.
</s>
