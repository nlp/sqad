<s>
Robert	Robert	k1gMnSc1	Robert
Pelikán	Pelikán	k1gMnSc1	Pelikán
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1979	[number]	k4	1979
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
advokát	advokát	k1gMnSc1	advokát
a	a	k8xC	a
od	od	k7c2	od
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
za	za	k7c2	za
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
až	až	k9	až
2015	[number]	k4	2015
prvním	první	k4xOgMnSc7	první
náměstkem	náměstek	k1gMnSc7	náměstek
ministryně	ministryně	k1gFnSc2	ministryně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
