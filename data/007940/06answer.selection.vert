<s>
Hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgFnSc2d1	veveří
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západě	západ	k1gInSc6	západ
brněnské	brněnský	k2eAgFnSc2d1	brněnská
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
12	[number]	k4	12
kilometrů	kilometr	k1gInPc2	kilometr
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
ostrohu	ostroh	k1gInSc6	ostroh
nad	nad	k7c7	nad
Brněnskou	brněnský	k2eAgFnSc7d1	brněnská
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napájí	napájet	k5eAaImIp3nS	napájet
řeka	řeka	k1gFnSc1	řeka
Svratka	Svratka	k1gFnSc1	Svratka
<g/>
.	.	kIx.	.
</s>
