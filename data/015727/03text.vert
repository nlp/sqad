<s>
Okresní	okresní	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Karviné	Karviná	k1gFnSc6
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Karviné	Karviná	k1gFnSc6
Budova	budova	k1gFnSc1
Okresního	okresní	k2eAgInSc2d1
soudu	soud	k1gInSc2
v	v	k7c4
KarvinéOdvolací	KarvinéOdvolací	k2eAgInSc4d1
soud	soud	k1gInSc4
</s>
<s>
Krajský	krajský	k2eAgInSc4d1
soud	soud	k1gInSc4
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
Předseda	předseda	k1gMnSc1
soudu	soud	k1gInSc3
</s>
<s>
JUDr.	JUDr.	kA
Pavlína	Pavlína	k1gFnSc1
Jurášková	Jurášková	k1gFnSc1
Místopředseda	místopředseda	k1gMnSc1
soudu	soud	k1gInSc2
</s>
<s>
JUDr.	JUDr.	kA
Jan	Jan	k1gMnSc1
ChowaniecJUDr	ChowaniecJUDr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lívia	Lívia	k1gFnSc1
LaššákováMgr	LaššákováMgra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Martina	Martina	k1gFnSc1
Szvitková	Szvitková	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
park	park	k1gInSc1
Bedřicha	Bedřich	k1gMnSc2
Smetany	Smetana	k1gMnSc2
176	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
733	#num#	k4
31	#num#	k4
Karviná-Fryštát	Karviná-Fryštát	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
7	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Mapka	mapka	k1gFnSc1
soudního	soudní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
</s>
<s>
Mapka	mapka	k1gFnSc1
soudního	soudní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Karviné	Karviná	k1gFnSc6
je	být	k5eAaImIp3nS
okresní	okresní	k2eAgInSc1d1
soud	soud	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Karviné	Karviná	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
zřízenou	zřízený	k2eAgFnSc4d1
pobočku	pobočka	k1gFnSc4
v	v	k7c6
Havířově	Havířov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
odvolacím	odvolací	k2eAgInSc7d1
soudem	soud	k1gInSc7
je	být	k5eAaImIp3nS
Krajský	krajský	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhoduje	rozhodovat	k5eAaImIp3nS
jako	jako	k9
soud	soud	k1gInSc1
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
ve	v	k7c6
všech	všecek	k3xTgFnPc6
trestních	trestní	k2eAgFnPc6d1
a	a	k8xC
civilních	civilní	k2eAgFnPc6d1
věcech	věc	k1gFnPc6
<g/>
,	,	kIx,
ledaže	ledaže	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
specializovanou	specializovaný	k2eAgFnSc4d1
agendu	agenda	k1gFnSc4
(	(	kIx(
<g/>
nejzávažnější	závažný	k2eAgInPc1d3
trestné	trestný	k2eAgInPc1d1
činy	čin	k1gInPc1
<g/>
,	,	kIx,
insolvenční	insolvenční	k2eAgNnPc1d1
řízení	řízení	k1gNnPc1
<g/>
,	,	kIx,
spory	spor	k1gInPc1
ve	v	k7c6
věcech	věc	k1gFnPc6
obchodních	obchodní	k2eAgFnPc2d1
korporací	korporace	k1gFnPc2
<g/>
,	,	kIx,
hospodářské	hospodářský	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
duševního	duševní	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
svěřena	svěřit	k5eAaPmNgFnS
krajskému	krajský	k2eAgInSc3d1
soudu	soud	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Soud	soud	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
Okresním	okresní	k2eAgNnSc7d1
státním	státní	k2eAgNnSc7d1
zastupitelstvím	zastupitelství	k1gNnSc7
v	v	k7c6
Karviné	Karviná	k1gFnSc6
v	v	k7c6
nové	nový	k2eAgFnSc6d1
budově	budova	k1gFnSc6
v	v	k7c6
Karviné	Karviná	k1gFnSc6
v	v	k7c6
parku	park	k1gInSc6
Bedřicha	Bedřich	k1gMnSc2
Smetany	Smetana	k1gMnSc2
<g/>
,	,	kIx,
pobočka	pobočka	k1gFnSc1
soudu	soud	k1gInSc2
také	také	k9
spolu	spolu	k6eAd1
s	s	k7c7
pobočkou	pobočka	k1gFnSc7
tohoto	tento	k3xDgNnSc2
okresního	okresní	k2eAgNnSc2d1
státního	státní	k2eAgNnSc2d1
zastupitelství	zastupitelství	k1gNnSc2
v	v	k7c6
nové	nový	k2eAgFnSc6d1
budově	budova	k1gFnSc6
na	na	k7c6
Dlouhé	Dlouhé	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
v	v	k7c6
Havířově	Havířov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedením	vedení	k1gNnSc7
pobočky	pobočka	k1gFnSc2
v	v	k7c6
Havířově	Havířov	k1gInSc6
je	být	k5eAaImIp3nS
pověřena	pověřen	k2eAgFnSc1d1
místopředsedkyně	místopředsedkyně	k1gFnSc1
Mgr.	Mgr.	kA
Ivana	Ivana	k1gFnSc1
Váňová	Váňová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Soudní	soudní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
</s>
<s>
Mapka	mapka	k1gFnSc1
soudního	soudní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
</s>
<s>
Budova	budova	k1gFnSc1
pobočky	pobočka	k1gFnSc2
v	v	k7c6
Havířově	Havířov	k1gInSc6
</s>
<s>
Do	do	k7c2
obvodu	obvod	k1gInSc2
Okresního	okresní	k2eAgInSc2d1
soudu	soud	k1gInSc2
v	v	k7c6
Karviné	Karviná	k1gFnSc6
patří	patřit	k5eAaImIp3nS
území	území	k1gNnSc1
obcí	obec	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Bohumín	Bohumín	k1gInSc1
•	•	k?
</s>
<s>
Český	český	k2eAgInSc1d1
Těšín	Těšín	k1gInSc1
•	•	k?
</s>
<s>
Dětmarovice	Dětmarovice	k1gFnSc1
•	•	k?
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
Lutyně	Lutyně	k1gFnSc1
•	•	k?
</s>
<s>
Chotěbuz	Chotěbuz	k1gFnSc1
•	•	k?
</s>
<s>
Karviná	Karviná	k1gFnSc1
•	•	k?
</s>
<s>
Petrovice	Petrovice	k1gFnSc1
u	u	k7c2
Karviné	Karviná	k1gFnSc2
•	•	k?
</s>
<s>
Rychvald	Rychvald	k1gInSc1
•	•	k?
</s>
<s>
Stonava	Stonava	k1gFnSc1
</s>
<s>
Do	do	k7c2
obvodu	obvod	k1gInSc2
pobočky	pobočka	k1gFnSc2
v	v	k7c6
Havířově	Havířov	k1gInSc6
patří	patřit	k5eAaImIp3nS
území	území	k1gNnSc1
obcí	obec	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Albrechtice	Albrechtice	k1gFnPc1
•	•	k?
</s>
<s>
Doubrava	Doubrava	k1gFnSc1
•	•	k?
</s>
<s>
Havířov	Havířov	k1gInSc1
•	•	k?
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Suchá	Suchá	k1gFnSc1
•	•	k?
</s>
<s>
Orlová	Orlová	k1gFnSc1
•	•	k?
</s>
<s>
Petřvald	Petřvald	k1gInSc1
•	•	k?
</s>
<s>
Těrlicko	Těrlicko	k6eAd1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Okresnímu	okresní	k2eAgInSc3d1
soudu	soud	k1gInSc3
v	v	k7c6
Karviné	Karviná	k1gFnSc6
předcházel	předcházet	k5eAaImAgInS
Okresní	okresní	k2eAgInSc1d1
soud	soud	k1gInSc1
ve	v	k7c6
Fryštátě	Fryštáta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
normalizace	normalizace	k1gFnSc2
proběhly	proběhnout	k5eAaPmAgFnP
na	na	k7c4
soudě	soudě	k6eAd1
politické	politický	k2eAgInPc4d1
procesy	proces	k1gInPc4
s	s	k7c7
aktivisty	aktivista	k1gMnPc7
protestantských	protestantský	k2eAgFnPc2d1
církví	církev	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
Rudolfem	Rudolf	k1gMnSc7
Bubikem	Bubik	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Předsedové	předseda	k1gMnPc1
soudu	soud	k1gInSc2
</s>
<s>
Ludmila	Ludmila	k1gFnSc1
Pekařová	Pekařová	k1gFnSc1
</s>
<s>
Roman	Roman	k1gMnSc1
Raab	Raab	k1gMnSc1
(	(	kIx(
<g/>
zastupující	zastupující	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Chowaniec	Chowaniec	k1gMnSc1
</s>
<s>
Pavlína	Pavlína	k1gFnSc1
Jurášková	Jurášková	k1gFnSc1
</s>
<s>
Významní	významný	k2eAgMnPc1d1
soudci	soudce	k1gMnPc1
</s>
<s>
JUDr.	JUDr.	kA
Vlastimil	Vlastimil	k1gMnSc1
Ševčík	Ševčík	k1gMnSc1
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
OS	osa	k1gFnPc2
Karviná	Karviná	k1gFnSc1
působil	působit	k5eAaImAgMnS
do	do	k7c2
března	březen	k1gInSc2
1954	#num#	k4
<g/>
;	;	kIx,
pozdější	pozdní	k2eAgMnSc1d2
soudce	soudce	k1gMnSc1
Ústavního	ústavní	k2eAgInSc2d1
soudu	soud	k1gInSc2
</s>
<s>
JUDr.	JUDr.	kA
Petr	Petr	k1gMnSc1
Gemmel	Gemmel	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1964	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
OS	osa	k1gFnPc2
Karviná	Karviná	k1gFnSc1
působil	působit	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1989	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
;	;	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
soudce	soudce	k1gMnSc1
Nejvyššího	vysoký	k2eAgInSc2d3
soudu	soud	k1gInSc2
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Okresní	okresní	k2eAgInSc4d1
soud	soud	k1gInSc4
v	v	k7c6
Karviné	Karviná	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Soudy	soud	k1gInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Nejvyšší	vysoký	k2eAgInSc1d3
soud	soud	k1gInSc1
</s>
<s>
Brno	Brno	k1gNnSc1
Nejvyšší	vysoký	k2eAgFnSc2d3
správní	správní	k2eAgFnSc2d1
soud	soud	k1gInSc1
</s>
<s>
Brno	Brno	k1gNnSc1
Vrchní	vrchní	k1gFnSc2
soud	soud	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc4
Krajský	krajský	k2eAgInSc4d1
soud	soud	k1gInSc4
</s>
<s>
MS	MS	kA
Praha	Praha	k1gFnSc1
•	•	k?
KS	ks	kA
Praha	Praha	k1gFnSc1
•	•	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
•	•	k?
Brno	Brno	k1gNnSc1
•	•	k?
Ostrava	Ostrava	k1gFnSc1
Okresní	okresní	k2eAgFnSc1d1
soud	soud	k1gInSc4
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
•	•	k?
Praha	Praha	k1gFnSc1
2	#num#	k4
•	•	k?
Praha	Praha	k1gFnSc1
3	#num#	k4
•	•	k?
Praha	Praha	k1gFnSc1
4	#num#	k4
•	•	k?
Praha	Praha	k1gFnSc1
5	#num#	k4
•	•	k?
Praha	Praha	k1gFnSc1
6	#num#	k4
•	•	k?
Praha	Praha	k1gFnSc1
7	#num#	k4
•	•	k?
Praha	Praha	k1gFnSc1
8	#num#	k4
•	•	k?
Praha	Praha	k1gFnSc1
9	#num#	k4
•	•	k?
Praha	Praha	k1gFnSc1
10	#num#	k4
•	•	k?
Benešov	Benešov	k1gInSc1
•	•	k?
Beroun	Beroun	k1gInSc1
•	•	k?
Kladno	Kladno	k1gNnSc4
•	•	k?
Kolín	Kolín	k1gInSc1
•	•	k?
Kutná	kutný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
•	•	k?
Mělník	Mělník	k1gInSc1
•	•	k?
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
Nymburk	Nymburk	k1gInSc1
•	•	k?
Praha-východ	Praha-východ	k1gInSc1
•	•	k?
Praha-západ	Praha-západ	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Příbram	Příbram	k1gFnSc1
•	•	k?
Rakovník	Rakovník	k1gInSc1
•	•	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
•	•	k?
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
•	•	k?
Pelhřimov	Pelhřimov	k1gInSc1
•	•	k?
Písek	Písek	k1gInSc1
•	•	k?
Prachatice	Prachatice	k1gFnPc4
•	•	k?
Strakonice	Strakonice	k1gFnPc4
•	•	k?
Tábor	Tábor	k1gInSc1
•	•	k?
Domažlice	Domažlice	k1gFnPc4
•	•	k?
Cheb	Cheb	k1gInSc1
•	•	k?
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
•	•	k?
Klatovy	Klatovy	k1gInPc4
•	•	k?
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Plzeň-jih	Plzeň-jih	k1gMnSc1
•	•	k?
Plzeň-sever	Plzeň-sever	k1gMnSc1
•	•	k?
Rokycany	Rokycany	k1gInPc1
•	•	k?
Sokolov	Sokolov	k1gInSc1
•	•	k?
Tachov	Tachov	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
•	•	k?
Děčín	Děčín	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Chomutov	Chomutov	k1gInSc1
•	•	k?
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
•	•	k?
Liberec	Liberec	k1gInSc4
•	•	k?
Litoměřice	Litoměřice	k1gInPc4
•	•	k?
Louny	Louny	k1gInPc1
•	•	k?
Most	most	k1gInSc1
•	•	k?
Teplice	teplice	k1gFnSc2
•	•	k?
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
•	•	k?
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
•	•	k?
Chrudim	Chrudim	k1gFnSc1
•	•	k?
Jičín	Jičín	k1gInSc1
•	•	k?
Náchod	Náchod	k1gInSc1
•	•	k?
Pardubice	Pardubice	k1gInPc1
•	•	k?
Rychnov	Rychnov	k1gInSc1
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
•	•	k?
Semily	Semily	k1gInPc1
•	•	k?
Svitavy	Svitava	k1gFnSc2
•	•	k?
Trutnov	Trutnov	k1gInSc1
•	•	k?
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
•	•	k?
Blansko	Blansko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
MS	MS	kA
Brno	Brno	k1gNnSc1
•	•	k?
Brno-venkov	Brno-venkov	k1gInSc1
•	•	k?
Břeclav	Břeclav	k1gFnSc1
•	•	k?
Hodonín	Hodonín	k1gInSc1
•	•	k?
Jihlava	Jihlava	k1gFnSc1
•	•	k?
Kroměříž	Kroměříž	k1gFnSc1
•	•	k?
Prostějov	Prostějov	k1gInSc1
•	•	k?
Třebíč	Třebíč	k1gFnSc1
•	•	k?
Uherské	uherský	k2eAgNnSc4d1
Hradiště	Hradiště	k1gNnSc4
•	•	k?
Vyškov	Vyškov	k1gInSc1
•	•	k?
Zlín	Zlín	k1gInSc1
•	•	k?
Znojmo	Znojmo	k1gNnSc4
•	•	k?
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
•	•	k?
Bruntál	Bruntál	k1gInSc4
•	•	k?
Frýdek-Místek	Frýdek-Místek	k1gInSc1
•	•	k?
Jeseník	Jeseník	k1gInSc1
•	•	k?
Karviná	Karviná	k1gFnSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Opava	Opava	k1gFnSc1
•	•	k?
Ostrava	Ostrava	k1gFnSc1
•	•	k?
Přerov	Přerov	k1gInSc1
•	•	k?
Šumperk	Šumperk	k1gInSc1
•	•	k?
Vsetín	Vsetín	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Právo	právo	k1gNnSc1
|	|	kIx~
Slezsko	Slezsko	k1gNnSc1
</s>
