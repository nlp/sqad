<p>
<s>
Faleristika	Faleristika	k1gFnSc1	Faleristika
je	být	k5eAaImIp3nS	být
pomocná	pomocný	k2eAgFnSc1d1	pomocná
věda	věda	k1gFnSc1	věda
historická	historický	k2eAgFnSc1d1	historická
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
popisem	popis	k1gInSc7	popis
a	a	k8xC	a
historií	historie	k1gFnSc7	historie
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Pojednává	pojednávat	k5eAaImIp3nS	pojednávat
tedy	tedy	k9	tedy
o	o	k7c6	o
viditelně	viditelně	k6eAd1	viditelně
nošených	nošený	k2eAgNnPc6d1	nošené
vyznamenáních	vyznamenání	k1gNnPc6	vyznamenání
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
o	o	k7c6	o
řádech	řád	k1gInPc6	řád
<g/>
,	,	kIx,	,
medailích	medaile	k1gFnPc6	medaile
a	a	k8xC	a
odznacích	odznak	k1gInPc6	odznak
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
uděleny	udělit	k5eAaPmNgInP	udělit
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
úzký	úzký	k2eAgInSc4d1	úzký
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
heraldice	heraldika	k1gFnSc3	heraldika
jako	jako	k8xS	jako
nauce	nauka	k1gFnSc3	nauka
o	o	k7c6	o
znacích	znak	k1gInPc6	znak
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
disciplíny	disciplína	k1gFnPc1	disciplína
mají	mít	k5eAaImIp3nP	mít
společné	společný	k2eAgInPc4d1	společný
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
době	doba	k1gFnSc6	doba
křižáckých	křižácký	k2eAgFnPc2d1	křižácká
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
bojující	bojující	k2eAgMnPc1d1	bojující
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
,	,	kIx,	,
skrytí	skrytí	k1gNnSc1	skrytí
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
,	,	kIx,	,
zdobili	zdobit	k5eAaImAgMnP	zdobit
symboly	symbol	k1gInPc4	symbol
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
snazšímu	snadný	k2eAgNnSc3d2	snazší
rozlišení	rozlišení	k1gNnSc3	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
symboly	symbol	k1gInPc1	symbol
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
odznaky	odznak	k1gInPc1	odznak
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
prvních	první	k4xOgInPc2	první
rytířských	rytířský	k2eAgInPc2d1	rytířský
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Faleristika	Faleristika	k1gFnSc1	Faleristika
převzala	převzít	k5eAaPmAgFnS	převzít
heraldickou	heraldický	k2eAgFnSc4d1	heraldická
terminologii	terminologie	k1gFnSc4	terminologie
při	při	k7c6	při
popisování	popisování	k1gNnSc6	popisování
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
například	například	k6eAd1	například
zásady	zásada	k1gFnPc4	zásada
popisu	popis	k1gInSc2	popis
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
nositele	nositel	k1gMnSc2	nositel
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
–	–	k?	–
čili	čili	k8xC	čili
pravá	pravý	k2eAgFnSc1d1	pravá
a	a	k8xC	a
levá	levý	k2eAgFnSc1d1	levá
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
udávají	udávat	k5eAaImIp3nP	udávat
obráceně	obráceně	k6eAd1	obráceně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kresbách	kresba	k1gFnPc6	kresba
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
heraldický	heraldický	k2eAgInSc1d1	heraldický
způsob	způsob	k1gInSc1	způsob
šrafování	šrafování	k1gNnSc2	šrafování
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Heraldická	heraldický	k2eAgFnSc1d1	heraldická
zásada	zásada	k1gFnSc1	zásada
zřetelnosti	zřetelnost	k1gFnSc2	zřetelnost
a	a	k8xC	a
jednoznačné	jednoznačný	k2eAgFnSc6d1	jednoznačná
rozlišitelnosti	rozlišitelnost	k1gFnSc6	rozlišitelnost
znaků	znak	k1gInPc2	znak
byla	být	k5eAaImAgFnS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
i	i	k9	i
u	u	k7c2	u
starých	starý	k2eAgInPc2d1	starý
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
u	u	k7c2	u
novějších	nový	k2eAgFnPc2d2	novější
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Záležitostí	záležitost	k1gFnSc7	záležitost
praktické	praktický	k2eAgFnSc2d1	praktická
heraldiky	heraldika	k1gFnSc2	heraldika
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
dodržováním	dodržování	k1gNnSc7	dodržování
jejích	její	k3xOp3gFnPc2	její
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
popisem	popis	k1gInSc7	popis
znaků	znak	k1gInPc2	znak
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
zabývali	zabývat	k5eAaImAgMnP	zabývat
příslušní	příslušný	k2eAgMnPc1d1	příslušný
královští	královský	k2eAgMnPc1d1	královský
úředníci	úředník	k1gMnPc1	úředník
–	–	k?	–
heroldi	herold	k1gMnPc1	herold
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
faleristika	faleristika	k1gFnSc1	faleristika
zavedl	zavést	k5eAaPmAgInS	zavést
jako	jako	k9	jako
první	první	k4xOgInSc1	první
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
československý	československý	k2eAgMnSc1d1	československý
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
sběratel	sběratel	k1gMnSc1	sběratel
řádů	řád	k1gInPc2	řád
Oldřich	Oldřich	k1gMnSc1	Oldřich
Pilc	Pilc	k1gFnSc1	Pilc
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
ze	z	k7c2	z
starořeckého	starořecký	k2eAgNnSc2d1	starořecké
slova	slovo	k1gNnSc2	slovo
phalera	phaler	k1gMnSc2	phaler
(	(	kIx(	(
<g/>
falera	faler	k1gMnSc2	faler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označující	označující	k2eAgFnPc1d1	označující
kovové	kovový	k2eAgFnPc4d1	kovová
ozdoby	ozdoba	k1gFnPc4	ozdoba
na	na	k7c6	na
přilbách	přilba	k1gFnPc6	přilba
válečníků	válečník	k1gMnPc2	válečník
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
převzatého	převzatý	k2eAgInSc2d1	převzatý
Římany	Říman	k1gMnPc7	Říman
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
phalerae	phalera	k1gFnSc2	phalera
(	(	kIx(	(
<g/>
faleré	falerý	k2eAgNnSc1d1	falerý
<g/>
)	)	kIx)	)
jako	jako	k9	jako
označení	označení	k1gNnSc1	označení
vojenských	vojenský	k2eAgNnPc2d1	vojenské
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
kruhové	kruhový	k2eAgFnPc1d1	kruhová
medaile	medaile	k1gFnPc1	medaile
(	(	kIx(	(
<g/>
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
,	,	kIx,	,
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
a	a	k8xC	a
zlaté	zlatý	k2eAgFnSc2d1	zlatá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
boha	bůh	k1gMnSc2	bůh
či	či	k8xC	či
bohyně	bohyně	k1gFnPc4	bohyně
a	a	k8xC	a
nošené	nošený	k2eAgInPc4d1	nošený
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
těchto	tento	k3xDgMnPc2	tento
válečníků	válečník	k1gMnPc2	válečník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
řádů	řád	k1gInPc2	řád
-	-	kIx~	-
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
dobách	doba	k1gFnPc6	doba
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
rádi	rád	k2eAgMnPc1d1	rád
zdobili	zdobit	k5eAaImAgMnP	zdobit
viditelnými	viditelný	k2eAgInPc7d1	viditelný
znaky	znak	k1gInPc7	znak
nejen	nejen	k6eAd1	nejen
úspěchu	úspěch	k1gInSc2	úspěch
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
znaky	znak	k1gInPc1	znak
zásluh	zásluha	k1gFnPc2	zásluha
a	a	k8xC	a
vděčností	vděčnost	k1gFnPc2	vděčnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příslušnými	příslušný	k2eAgInPc7d1	příslušný
symboly	symbol	k1gInPc7	symbol
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
kroku	krok	k1gInSc6	krok
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
stupněm	stupeň	k1gInSc7	stupeň
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
řazením	řazení	k1gNnSc7	řazení
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
lovec	lovec	k1gMnSc1	lovec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
zdobil	zdobit	k5eAaImAgMnS	zdobit
zvířecí	zvířecí	k2eAgFnSc7d1	zvířecí
trofejí	trofej	k1gFnSc7	trofej
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
důstojník	důstojník	k1gMnSc1	důstojník
hrdě	hrdě	k6eAd1	hrdě
nosil	nosit	k5eAaImAgMnS	nosit
věnec	věnec	k1gInSc4	věnec
za	za	k7c4	za
statečnost	statečnost	k1gFnSc4	statečnost
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
<g/>
,	,	kIx,	,
středověký	středověký	k2eAgMnSc1d1	středověký
rytíř	rytíř	k1gMnSc1	rytíř
jel	jet	k5eAaImAgMnS	jet
do	do	k7c2	do
boje	boj	k1gInSc2	boj
s	s	k7c7	s
šátkem	šátek	k1gInSc7	šátek
milované	milovaný	k2eAgFnSc2d1	milovaná
dámy	dáma	k1gFnSc2	dáma
a	a	k8xC	a
prostý	prostý	k2eAgMnSc1d1	prostý
voják	voják	k1gMnSc1	voják
si	se	k3xPyFc3	se
přinesl	přinést	k5eAaPmAgMnS	přinést
domů	domů	k6eAd1	domů
z	z	k7c2	z
tureckých	turecký	k2eAgFnPc2d1	turecká
válek	válka	k1gFnPc2	válka
"	"	kIx"	"
<g/>
pamětní	pamětní	k2eAgInSc1d1	pamětní
peníz	peníz	k1gInSc1	peníz
<g/>
"	"	kIx"	"
s	s	k7c7	s
podobou	podoba	k1gFnSc7	podoba
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gMnSc1	člen
starého	starý	k2eAgInSc2d1	starý
rytířského	rytířský	k2eAgInSc2d1	rytířský
řádu	řád	k1gInSc2	řád
i	i	k8xC	i
účastník	účastník	k1gMnSc1	účastník
křížového	křížový	k2eAgNnSc2d1	křížové
tažení	tažení	k1gNnSc2	tažení
si	se	k3xPyFc3	se
připjali	připnout	k5eAaPmAgMnP	připnout
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
plášť	plášť	k1gInSc4	plášť
kříž	kříž	k1gInSc1	kříž
na	na	k7c4	na
znamení	znamení	k1gNnSc4	znamení
k	k	k7c3	k
příslušnosti	příslušnost	k1gFnSc3	příslušnost
k	k	k7c3	k
vybrané	vybraný	k2eAgFnSc3d1	vybraná
skupině	skupina	k1gFnSc3	skupina
<g/>
,	,	kIx,	,
panovník	panovník	k1gMnSc1	panovník
si	se	k3xPyFc3	se
zavázal	zavázat	k5eAaPmAgMnS	zavázat
k	k	k7c3	k
věrnosti	věrnost	k1gFnSc2	věrnost
své	svůj	k3xOyFgMnPc4	svůj
dvořany	dvořan	k1gMnPc4	dvořan
společným	společný	k2eAgInSc7d1	společný
odznakem	odznak	k1gInSc7	odznak
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
vývojem	vývoj	k1gInSc7	vývoj
času	čas	k1gInSc2	čas
a	a	k8xC	a
praktickou	praktický	k2eAgFnSc7d1	praktická
potřebou	potřeba	k1gFnSc7	potřeba
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
odlišit	odlišit	k5eAaPmF	odlišit
členství	členství	k1gNnSc4	členství
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
řádu	řád	k1gInSc6	řád
či	či	k8xC	či
spolku	spolek	k1gInSc6	spolek
od	od	k7c2	od
prostého	prostý	k2eAgNnSc2d1	prosté
zviditelnění	zviditelnění	k1gNnSc2	zviditelnění
projevu	projev	k1gInSc3	projev
díku	dík	k1gInSc2	dík
za	za	k7c4	za
věrnost	věrnost	k1gFnSc4	věrnost
<g/>
,	,	kIx,	,
statečnost	statečnost	k1gFnSc4	statečnost
či	či	k8xC	či
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
zásluhu	zásluha	k1gFnSc4	zásluha
<g/>
.	.	kIx.	.
</s>
<s>
Viditelné	viditelný	k2eAgInPc4d1	viditelný
symboly	symbol	k1gInPc4	symbol
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
psychologickou	psychologický	k2eAgFnSc7d1	psychologická
součástí	součást	k1gFnSc7	součást
života	život	k1gInSc2	život
a	a	k8xC	a
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
lidstvo	lidstvo	k1gNnSc4	lidstvo
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
kroku	krok	k1gInSc6	krok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Faléry	Falér	k1gInPc4	Falér
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
phalerae	phalerae	k6eAd1	phalerae
<g/>
)	)	kIx)	)
nebyly	být	k5eNaImAgFnP	být
jediným	jediný	k2eAgNnSc7d1	jediné
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Známe	znát	k5eAaImIp1nP	znát
také	také	k9	také
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
věnců	věnec	k1gInPc2	věnec
nošených	nošený	k2eAgInPc2d1	nošený
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
–	–	k?	–
latinsky	latinsky	k6eAd1	latinsky
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
říkalo	říkat	k5eAaImAgNnS	říkat
coronae	corona	k1gFnSc2	corona
(	(	kIx(	(
<g/>
koroné	koroná	k1gFnSc2	koroná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
vavřínové	vavřínový	k2eAgFnPc1d1	vavřínová
<g/>
,	,	kIx,	,
dubové	dubový	k2eAgFnPc1d1	dubová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zlaté	zlatý	k2eAgFnPc4d1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Takzvané	takzvaný	k2eAgInPc1d1	takzvaný
armillae	armilla	k1gInPc1	armilla
(	(	kIx(	(
<g/>
armilé	armilý	k2eAgFnSc3d1	armilý
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
náramky	náramek	k1gInPc1	náramek
<g/>
,	,	kIx,	,
torques	torques	k1gInSc1	torques
pak	pak	k8xC	pak
řetězy	řetěz	k1gInPc1	řetěz
(	(	kIx(	(
<g/>
keltského	keltský	k2eAgInSc2d1	keltský
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nošené	nošený	k2eAgNnSc1d1	nošené
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vyznamenání	vyznamenání	k1gNnPc1	vyznamenání
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
zachovala	zachovat	k5eAaPmAgFnS	zachovat
především	především	k9	především
na	na	k7c6	na
stélách	stéla	k1gFnPc6	stéla
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mramorových	mramorový	k2eAgInPc6d1	mramorový
náhrobcích	náhrobek	k1gInPc6	náhrobek
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
a	a	k8xC	a
často	často	k6eAd1	často
reprodukovaná	reprodukovaný	k2eAgFnSc1d1	reprodukovaná
je	být	k5eAaImIp3nS	být
stéla	stéla	k1gFnSc1	stéla
Marka	marka	k1gFnSc1	marka
Caelia	Caelia	k1gFnSc1	Caelia
uchovaná	uchovaný	k2eAgFnSc1d1	uchovaná
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Bonnu	Bonn	k1gInSc6	Bonn
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
legáta	legát	k1gMnSc4	legát
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
velitele	velitel	k1gMnSc2	velitel
či	či	k8xC	či
podvelitele	podvelitel	k1gMnSc2	podvelitel
legie	legie	k1gFnSc2	legie
<g/>
)	)	kIx)	)
Marka	Marek	k1gMnSc2	Marek
Caelia	Caelius	k1gMnSc2	Caelius
s	s	k7c7	s
věncem	věnec	k1gInSc7	věnec
<g/>
,	,	kIx,	,
třemi	tři	k4xCgInPc7	tři
řetězy	řetěz	k1gInPc7	řetěz
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgInPc7	dva
náramky	náramek	k1gInPc7	náramek
a	a	k8xC	a
pěti	pět	k4xCc7	pět
falérami	faléra	k1gFnPc7	faléra
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
padl	padnout	k5eAaPmAgInS	padnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
9	[number]	k4	9
po	po	k7c4	po
Kristu	Krista	k1gFnSc4	Krista
v	v	k7c6	v
proslulé	proslulý	k2eAgFnSc6d1	proslulá
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Teutoburském	Teutoburský	k2eAgInSc6d1	Teutoburský
lese	les	k1gInSc6	les
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Publia	Publius	k1gMnSc2	Publius
Quintilia	Quintilius	k1gMnSc2	Quintilius
Vara	Varus	k1gMnSc2	Varus
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Římané	Říman	k1gMnPc1	Říman
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
největších	veliký	k2eAgFnPc2d3	veliký
porážek	porážka	k1gFnPc2	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
snad	snad	k9	snad
ještě	ještě	k6eAd1	ještě
hůře	zle	k6eAd2	zle
než	než	k8xS	než
ztrátu	ztráta	k1gFnSc4	ztráta
padlých	padlý	k1gMnPc2	padlý
pociťovali	pociťovat	k5eAaImAgMnP	pociťovat
pohanu	pohana	k1gFnSc4	pohana
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jim	on	k3xPp3gMnPc3	on
Germáni	Germán	k1gMnPc1	Germán
připravili	připravit	k5eAaPmAgMnP	připravit
ukořistěním	ukořistění	k1gNnSc7	ukořistění
výsostných	výsostný	k2eAgMnPc2d1	výsostný
znaků	znak	k1gInPc2	znak
legií	legie	k1gFnPc2	legie
s	s	k7c7	s
orly	orel	k1gMnPc7	orel
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
císař	císař	k1gMnSc1	císař
Augustus	Augustus	k1gMnSc1	Augustus
zdrceně	zdrceně	k6eAd1	zdrceně
vykřikoval	vykřikovat	k5eAaImAgMnS	vykřikovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vare	var	k1gInSc5	var
<g/>
,	,	kIx,	,
Vare	var	k1gInSc5	var
<g/>
,	,	kIx,	,
vrať	vrátit	k5eAaPmRp2nS	vrátit
mi	já	k3xPp1nSc3	já
mé	můj	k3xOp1gMnPc4	můj
orly	orel	k1gMnPc4	orel
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
faléry	falér	k1gInPc1	falér
byly	být	k5eAaImAgInP	být
ovšem	ovšem	k9	ovšem
také	také	k9	také
nalezeny	nalézt	k5eAaBmNgInP	nalézt
při	při	k7c6	při
archeologických	archeologický	k2eAgFnPc6d1	archeologická
vykopávkách	vykopávka	k1gFnPc6	vykopávka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
uchovávány	uchovávat	k5eAaImNgInP	uchovávat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
rytířských	rytířský	k2eAgInPc2d1	rytířský
řádů	řád	k1gInPc2	řád
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prvních	první	k4xOgFnPc2	první
křižáckých	křižácký	k2eAgFnPc2d1	křižácká
výprav	výprava	k1gFnPc2	výprava
přinesl	přinést	k5eAaPmAgInS	přinést
některé	některý	k3yIgInPc4	některý
nové	nový	k2eAgInPc4d1	nový
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
a	a	k8xC	a
rozsah	rozsah	k1gInSc1	rozsah
řádů	řád	k1gInPc2	řád
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
organizací	organizace	k1gFnSc7	organizace
duchovní	duchovní	k2eAgFnSc7d1	duchovní
i	i	k8xC	i
světskou	světský	k2eAgFnSc4d1	světská
<g/>
,	,	kIx,	,
druhem	druh	k1gMnSc7	druh
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
souhrnem	souhrnem	k6eAd1	souhrnem
předpisů	předpis	k1gInPc2	předpis
a	a	k8xC	a
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Středověké	středověký	k2eAgInPc1d1	středověký
rytířské	rytířský	k2eAgInPc1d1	rytířský
řády	řád	k1gInPc1	řád
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
navazovaly	navazovat	k5eAaImAgInP	navazovat
na	na	k7c4	na
starší	starší	k1gMnPc4	starší
mnišské	mnišský	k2eAgInPc1d1	mnišský
řády	řád	k1gInPc1	řád
a	a	k8xC	a
řehole	řehole	k1gFnSc1	řehole
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
řídily	řídit	k5eAaImAgInP	řídit
přesně	přesně	k6eAd1	přesně
vypracovanými	vypracovaný	k2eAgFnPc7d1	vypracovaná
stanovami	stanova	k1gFnPc7	stanova
<g/>
,	,	kIx,	,
statuty	statut	k1gInPc7	statut
–	–	k?	–
čili	čili	k8xC	čili
řádem	řád	k1gInSc7	řád
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
pevnou	pevný	k2eAgFnSc4d1	pevná
<g/>
,	,	kIx,	,
přísně	přísně	k6eAd1	přísně
vedenou	vedený	k2eAgFnSc7d1	vedená
organizací	organizace	k1gFnSc7	organizace
s	s	k7c7	s
určenými	určený	k2eAgInPc7d1	určený
úkoly	úkol	k1gInPc7	úkol
a	a	k8xC	a
cíli	cíl	k1gInPc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
příslušníci	příslušník	k1gMnPc1	příslušník
se	se	k3xPyFc4	se
oblečením	oblečení	k1gNnSc7	oblečení
a	a	k8xC	a
symboly	symbol	k1gInPc4	symbol
lišili	lišit	k5eAaImAgMnP	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Vnějším	vnější	k2eAgNnSc7d1	vnější
označením	označení	k1gNnSc7	označení
řádu	řád	k1gInSc2	řád
byl	být	k5eAaImAgInS	být
většinou	většinou	k6eAd1	většinou
kříž	kříž	k1gInSc1	kříž
nošený	nošený	k2eAgInSc1d1	nošený
na	na	k7c6	na
oděvu	oděv	k1gInSc6	oděv
<g/>
,	,	kIx,	,
praporci	praporec	k1gInSc6	praporec
a	a	k8xC	a
štítu	štít	k1gInSc6	štít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdobnou	obdobný	k2eAgFnSc4d1	obdobná
symboliku	symbolika	k1gFnSc4	symbolika
převzaly	převzít	k5eAaPmAgInP	převzít
tzv.	tzv.	kA	tzv.
světské	světský	k2eAgInPc1d1	světský
řády	řád	k1gInPc1	řád
<g/>
,	,	kIx,	,
zakládané	zakládaný	k2eAgNnSc1d1	zakládané
suverénními	suverénní	k2eAgMnPc7d1	suverénní
panovníky	panovník	k1gMnPc7	panovník
a	a	k8xC	a
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
úzký	úzký	k2eAgInSc4d1	úzký
okruh	okruh	k1gInSc4	okruh
vysoké	vysoký	k2eAgFnSc2d1	vysoká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Vnějším	vnější	k2eAgInSc7d1	vnější
symbolem	symbol	k1gInSc7	symbol
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
světskému	světský	k2eAgInSc3d1	světský
řádu	řád	k1gInSc3	řád
byl	být	k5eAaImAgInS	být
jednotný	jednotný	k2eAgInSc1d1	jednotný
řádový	řádový	k2eAgInSc1d1	řádový
odznak	odznak	k1gInSc1	odznak
nošený	nošený	k2eAgInSc1d1	nošený
původně	původně	k6eAd1	původně
na	na	k7c6	na
řetězu	řetěz	k1gInSc6	řetěz
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
na	na	k7c6	na
stuze	stuha	k1gFnSc6	stuha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
růstem	růst	k1gInSc7	růst
moci	moc	k1gFnSc2	moc
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgMnS	být
panovník	panovník	k1gMnSc1	panovník
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
šlechtě	šlechta	k1gFnSc6	šlechta
<g/>
,	,	kIx,	,
světské	světský	k2eAgInPc4d1	světský
řády	řád	k1gInPc4	řád
ztrácely	ztrácet	k5eAaImAgFnP	ztrácet
svůj	svůj	k3xOyFgInSc4	svůj
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
pojem	pojem	k1gInSc1	pojem
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přesouval	přesouvat	k5eAaImAgInS	přesouvat
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
hmotný	hmotný	k2eAgInSc4d1	hmotný
symbol	symbol	k1gInSc4	symbol
–	–	k?	–
řádový	řádový	k2eAgInSc1d1	řádový
odznak	odznak	k1gInSc1	odznak
–	–	k?	–
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
viditelným	viditelný	k2eAgNnSc7d1	viditelné
vyjádřením	vyjádření	k1gNnSc7	vyjádření
pocty	pocta	k1gFnSc2	pocta
za	za	k7c4	za
vykonané	vykonaný	k2eAgFnPc4d1	vykonaná
služby	služba	k1gFnPc4	služba
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dekorovaní	dekorovaný	k2eAgMnPc1d1	dekorovaný
si	se	k3xPyFc3	se
nebyli	být	k5eNaImAgMnP	být
již	již	k6eAd1	již
navzájem	navzájem	k6eAd1	navzájem
rovni	roven	k2eAgMnPc1d1	roven
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
odměňováni	odměňovat	k5eAaImNgMnP	odměňovat
úměrně	úměrně	k6eAd1	úměrně
svým	svůj	k3xOyFgFnPc3	svůj
zásluhám	zásluha	k1gFnPc3	zásluha
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dostávali	dostávat	k5eAaImAgMnP	dostávat
odstupňované	odstupňovaný	k2eAgInPc4d1	odstupňovaný
odznaky	odznak	k1gInPc4	odznak
<g/>
.	.	kIx.	.
</s>
<s>
Řády	řád	k1gInPc1	řád
se	se	k3xPyFc4	se
skládaly	skládat	k5eAaImAgInP	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
odlišených	odlišený	k2eAgFnPc2d1	odlišená
velikostí	velikost	k1gFnPc2	velikost
a	a	k8xC	a
kvalitou	kvalita	k1gFnSc7	kvalita
jednotného	jednotný	k2eAgInSc2d1	jednotný
řádového	řádový	k2eAgInSc2d1	řádový
odznaku	odznak	k1gInSc2	odznak
a	a	k8xC	a
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
jeho	on	k3xPp3gNnSc2	on
nošení	nošení	k1gNnSc2	nošení
a	a	k8xC	a
zavěšení	zavěšení	k1gNnSc2	zavěšení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tak	tak	k6eAd1	tak
vznikaly	vznikat	k5eAaImAgInP	vznikat
již	již	k6eAd1	již
vysloveně	vysloveně	k6eAd1	vysloveně
záslužné	záslužný	k2eAgInPc4d1	záslužný
řády	řád	k1gInPc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
největší	veliký	k2eAgInSc1d3	veliký
rozvoj	rozvoj	k1gInSc1	rozvoj
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
dynastie	dynastie	k1gFnPc1	dynastie
a	a	k8xC	a
státy	stát	k1gInPc1	stát
předháněly	předhánět	k5eAaImAgInP	předhánět
v	v	k7c6	v
zakládání	zakládání	k1gNnSc6	zakládání
nových	nový	k2eAgInPc2d1	nový
záslužných	záslužný	k2eAgInPc2d1	záslužný
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vznikaly	vznikat	k5eAaImAgFnP	vznikat
další	další	k2eAgFnPc4d1	další
dekorace	dekorace	k1gFnPc4	dekorace
–	–	k?	–
medaile	medaile	k1gFnPc4	medaile
<g/>
,	,	kIx,	,
kříže	kříž	k1gInPc4	kříž
a	a	k8xC	a
čestné	čestný	k2eAgInPc4d1	čestný
odznaky	odznak	k1gInPc4	odznak
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
enormně	enormně	k6eAd1	enormně
narostl	narůst	k5eAaPmAgInS	narůst
především	především	k9	především
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
období	období	k1gNnSc6	období
obou	dva	k4xCgFnPc2	dva
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
Sovětského	sovětský	k2eAgInSc2d1	sovětský
Svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
pozdějších	pozdní	k2eAgInPc2d2	pozdější
tzv.	tzv.	kA	tzv.
socialistických	socialistický	k2eAgInPc2d1	socialistický
států	stát	k1gInPc2	stát
způsobil	způsobit	k5eAaPmAgInS	způsobit
zánik	zánik	k1gInSc1	zánik
mnoha	mnoho	k4c2	mnoho
klasických	klasický	k2eAgInPc2d1	klasický
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
dekorací	dekorace	k1gFnPc2	dekorace
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
vznik	vznik	k1gInSc4	vznik
nových	nový	k2eAgNnPc2d1	nové
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
odlišujících	odlišující	k2eAgNnPc2d1	odlišující
se	se	k3xPyFc4	se
od	od	k7c2	od
předchozích	předchozí	k2eAgNnPc2d1	předchozí
vědomě	vědomě	k6eAd1	vědomě
jak	jak	k8xC	jak
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
obsahem	obsah	k1gInSc7	obsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Názvosloví	názvosloví	k1gNnSc2	názvosloví
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
všech	všecek	k3xTgFnPc2	všecek
dekorací	dekorace	k1gFnPc2	dekorace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nepatří	patřit	k5eNaImIp3nP	patřit
mezi	mezi	k7c7	mezi
řády	řád	k1gInPc7	řád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
odměnou	odměna	k1gFnSc7	odměna
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
–	–	k?	–
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
různé	různý	k2eAgInPc4d1	různý
válečné	válečný	k2eAgInPc4d1	válečný
<g/>
,	,	kIx,	,
záslužné	záslužný	k2eAgInPc4d1	záslužný
a	a	k8xC	a
pamětní	pamětní	k2eAgInPc4d1	pamětní
kříže	kříž	k1gInPc4	kříž
a	a	k8xC	a
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
dekorace	dekorace	k1gFnSc2	dekorace
či	či	k8xC	či
odznak	odznak	k1gInSc1	odznak
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
jako	jako	k8xS	jako
označení	označení	k1gNnSc6	označení
vlastního	vlastní	k2eAgInSc2d1	vlastní
předmětu	předmět	k1gInSc2	předmět
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
–	–	k?	–
řádová	řádový	k2eAgFnSc1d1	řádová
dekorace	dekorace	k1gFnSc1	dekorace
<g/>
,	,	kIx,	,
náprsní	náprsní	k2eAgFnSc1d1	náprsní
dekorace	dekorace	k1gFnSc1	dekorace
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
řád	řád	k1gInSc1	řád
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
třídy	třída	k1gFnPc4	třída
či	či	k8xC	či
stupně	stupeň	k1gInPc4	stupeň
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
i	i	k9	i
odstupňovaně	odstupňovaně	k6eAd1	odstupňovaně
udělováno	udělován	k2eAgNnSc1d1	udělováno
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
to	ten	k3xDgNnSc1	ten
však	však	k9	však
neplatí	platit	k5eNaImIp3nS	platit
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
původní	původní	k2eAgFnSc7d1	původní
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
staré	starý	k2eAgInPc1d1	starý
či	či	k8xC	či
velké	velký	k2eAgInPc1d1	velký
<g/>
)	)	kIx)	)
řády	řád	k1gInPc1	řád
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
a	a	k8xC	a
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
o	o	k7c6	o
více	hodně	k6eAd2	hodně
stupních	stupeň	k1gInPc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
proto	proto	k8xC	proto
platí	platit	k5eAaImIp3nS	platit
že	že	k8xS	že
řád	řád	k1gInSc1	řád
je	být	k5eAaImIp3nS	být
hierarchicky	hierarchicky	k6eAd1	hierarchicky
vyšší	vysoký	k2eAgMnSc1d2	vyšší
a	a	k8xC	a
cennější	cenný	k2eAgMnSc1d2	cennější
(	(	kIx(	(
<g/>
i	i	k8xC	i
provedením	provedení	k1gNnSc7	provedení
<g/>
)	)	kIx)	)
než	než	k8xS	než
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
úředním	úřední	k2eAgInSc7d1	úřední
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
(	(	kIx(	(
<g/>
založená	založený	k2eAgNnPc1d1	založené
úředně	úředně	k6eAd1	úředně
formou	forma	k1gFnSc7	forma
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
řády	řád	k1gInPc4	řád
a	a	k8xC	a
dekorace	dekorace	k1gFnPc4	dekorace
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnPc1	vyznamenání
spolková	spolkový	k2eAgNnPc1d1	Spolkové
(	(	kIx(	(
<g/>
založená	založený	k2eAgFnSc1d1	založená
spolky	spolek	k1gInPc7	spolek
či	či	k8xC	či
organizacemi	organizace	k1gFnPc7	organizace
veřejnoprávními	veřejnoprávní	k2eAgFnPc7d1	veřejnoprávní
nebo	nebo	k8xC	nebo
i	i	k9	i
soukromými	soukromý	k2eAgFnPc7d1	soukromá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnPc1	vyznamenání
mají	mít	k5eAaImIp3nP	mít
jistou	jistý	k2eAgFnSc4d1	jistá
strukturu	struktura	k1gFnSc4	struktura
danou	daný	k2eAgFnSc4d1	daná
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
přesnou	přesný	k2eAgFnSc4d1	přesná
podobu	podoba	k1gFnSc4	podoba
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
jeho	jeho	k3xOp3gNnSc2	jeho
nošení	nošení	k1gNnSc2	nošení
<g/>
,	,	kIx,	,
zásady	zásada	k1gFnPc4	zásada
propůjčování	propůjčování	k1gNnSc2	propůjčování
a	a	k8xC	a
udělování	udělování	k1gNnSc2	udělování
atd.	atd.	kA	atd.
Spolková	spolkový	k2eAgNnPc4d1	Spolkové
vyznamenání	vyznamenání	k1gNnPc4	vyznamenání
nelze	lze	k6eNd1	lze
nosit	nosit	k5eAaImF	nosit
na	na	k7c6	na
vojenském	vojenský	k2eAgInSc6d1	vojenský
a	a	k8xC	a
policejním	policejní	k2eAgInSc6d1	policejní
stejnokroji	stejnokroj	k1gInSc6	stejnokroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
jejich	jejich	k3xOp3gNnSc2	jejich
nošení	nošení	k1gNnSc2	nošení
výjimečně	výjimečně	k6eAd1	výjimečně
v	v	k7c6	v
odůvodněných	odůvodněný	k2eAgInPc6d1	odůvodněný
případech	případ	k1gInPc6	případ
povoloval	povolovat	k5eAaImAgMnS	povolovat
ministr	ministr	k1gMnSc1	ministr
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Junácký	junácký	k2eAgInSc1d1	junácký
kříž	kříž	k1gInSc1	kříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spolkovém	spolkový	k2eAgNnSc6d1	Spolkové
oblečení	oblečení	k1gNnSc6	oblečení
–	–	k?	–
skautském	skautský	k2eAgNnSc6d1	skautské
kroji	kroj	k1gInSc6	kroj
–	–	k?	–
se	se	k3xPyFc4	se
státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
musí	muset	k5eAaImIp3nS	muset
přirozeně	přirozeně	k6eAd1	přirozeně
nosit	nosit	k5eAaImF	nosit
před	před	k7c7	před
vyznamenáními	vyznamenání	k1gNnPc7	vyznamenání
skautskými	skautský	k2eAgNnPc7d1	skautské
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řády	řád	k1gInPc4	řád
===	===	k?	===
</s>
</p>
<p>
<s>
Řády	řád	k1gInPc1	řád
jsou	být	k5eAaImIp3nP	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
druhem	druh	k1gInSc7	druh
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vnitřně	vnitřně	k6eAd1	vnitřně
členěny	členěn	k2eAgFnPc1d1	členěna
a	a	k8xC	a
za	za	k7c2	za
odstupňované	odstupňovaný	k2eAgFnSc2d1	odstupňovaná
zásluhy	zásluha	k1gFnSc2	zásluha
jsou	být	k5eAaImIp3nP	být
propůjčovány	propůjčován	k2eAgInPc4d1	propůjčován
řádové	řádový	k2eAgInPc4d1	řádový
odznaky	odznak	k1gInPc4	odznak
od	od	k7c2	od
nižších	nízký	k2eAgFnPc2d2	nižší
tříd	třída	k1gFnPc2	třída
až	až	k9	až
k	k	k7c3	k
nejvyšší	vysoký	k2eAgFnSc3d3	nejvyšší
I.	I.	kA	I.
třídě	třída	k1gFnSc6	třída
<g/>
.	.	kIx.	.
</s>
<s>
Řády	řád	k1gInPc1	řád
bývají	bývat	k5eAaImIp3nP	bývat
rozčleněny	rozčlenit	k5eAaPmNgInP	rozčlenit
na	na	k7c4	na
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
má	mít	k5eAaImIp3nS	mít
řád	řád	k1gInSc1	řád
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
propůjčení	propůjčení	k1gNnSc6	propůjčení
řádu	řád	k1gInSc2	řád
zpravidla	zpravidla	k6eAd1	zpravidla
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
tzv.	tzv.	kA	tzv.
Řádová	řádový	k2eAgFnSc1d1	řádová
kapitula	kapitula	k1gFnSc1	kapitula
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
nejvýznačnějšími	význačný	k2eAgMnPc7d3	nejvýznačnější
nositeli	nositel	k1gMnPc7	nositel
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgInS	být
zaručen	zaručit	k5eAaPmNgInS	zaručit
objektivní	objektivní	k2eAgInSc1d1	objektivní
výběr	výběr	k1gInSc1	výběr
nových	nový	k2eAgMnPc2d1	nový
vyznamenaných	vyznamenaný	k2eAgMnPc2d1	vyznamenaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
častější	častý	k2eAgNnSc1d2	častější
<g/>
,	,	kIx,	,
že	že	k8xS	že
řády	řád	k1gInPc4	řád
propůjčuje	propůjčovat	k5eAaImIp3nS	propůjčovat
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
základě	základ	k1gInSc6	základ
návrhu	návrh	k1gInSc2	návrh
a	a	k8xC	a
doporučení	doporučení	k1gNnSc4	doporučení
komisí	komise	k1gFnPc2	komise
či	či	k8xC	či
výboru	výbor	k1gInSc2	výbor
ústavních	ústavní	k2eAgInPc2d1	ústavní
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Junáku	junák	k1gMnSc6	junák
působí	působit	k5eAaImIp3nS	působit
obdobně	obdobně	k6eAd1	obdobně
Sbor	sbor	k1gInSc1	sbor
nositelů	nositel	k1gMnPc2	nositel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řády	řád	k1gInPc1	řád
jsou	být	k5eAaImIp3nP	být
propůjčovány	propůjčován	k2eAgInPc1d1	propůjčován
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
nositele	nositel	k1gMnSc2	nositel
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
orgánu	orgán	k1gInSc2	orgán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zvýšení	zvýšení	k1gNnSc4	zvýšení
kvality	kvalita	k1gFnSc2	kvalita
propůjčovaných	propůjčovaný	k2eAgInPc2d1	propůjčovaný
exemplářů	exemplář	k1gInPc2	exemplář
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
nákladů	náklad	k1gInPc2	náklad
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
tradice	tradice	k1gFnSc2	tradice
předávání	předávání	k1gNnSc2	předávání
těchto	tento	k3xDgInPc2	tento
symbolů	symbol	k1gInPc2	symbol
jako	jako	k8xC	jako
osobních	osobní	k2eAgFnPc2d1	osobní
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dekorace	dekorace	k1gFnSc2	dekorace
===	===	k?	===
</s>
</p>
<p>
<s>
Dekorace	dekorace	k1gFnPc1	dekorace
jsou	být	k5eAaImIp3nP	být
kříže	kříž	k1gInPc4	kříž
<g/>
,	,	kIx,	,
medaile	medaile	k1gFnPc4	medaile
a	a	k8xC	a
odznaky	odznak	k1gInPc4	odznak
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
též	též	k9	též
odstupňovány	odstupňován	k2eAgFnPc1d1	odstupňována
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
platí	platit	k5eAaImIp3nS	platit
např.	např.	kA	např.
že	že	k8xS	že
kříž	kříž	k1gInSc1	kříž
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
kříž	kříž	k1gInSc1	kříž
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
atd.	atd.	kA	atd.
Dekorace	dekorace	k1gFnPc1	dekorace
se	se	k3xPyFc4	se
udílejí	udílet	k5eAaImIp3nP	udílet
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
Že	že	k8xS	že
po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
nositele	nositel	k1gMnSc2	nositel
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
pozůstalým	pozůstalý	k1gMnPc3	pozůstalý
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
udělených	udělený	k2eAgInPc2d1	udělený
exemplářů	exemplář	k1gInPc2	exemplář
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
neomezuje	omezovat	k5eNaImIp3nS	omezovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kříže	kříž	k1gInPc1	kříž
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
válečné	válečný	k2eAgInPc1d1	válečný
<g/>
,	,	kIx,	,
za	za	k7c4	za
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
,	,	kIx,	,
záslužné	záslužný	k2eAgFnSc2d1	záslužná
a	a	k8xC	a
pamětní	pamětní	k2eAgFnSc2d1	pamětní
atd.	atd.	kA	atd.
Nosí	nosit	k5eAaImIp3nP	nosit
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
stuze	stuha	k1gFnSc6	stuha
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
kruhového	kruhový	k2eAgMnSc4d1	kruhový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
oválného	oválný	k2eAgInSc2d1	oválný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dvoustranné	dvoustranný	k2eAgInPc1d1	dvoustranný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jednostranné	jednostranný	k2eAgNnSc1d1	jednostranné
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
s	s	k7c7	s
medailemi	medaile	k1gFnPc7	medaile
upomínkovými	upomínkový	k2eAgFnPc7d1	upomínková
a	a	k8xC	a
plaketami	plaketa	k1gFnPc7	plaketa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
ouško	ouško	k1gNnSc4	ouško
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
osobně	osobně	k6eAd1	osobně
nositelné	nositelný	k2eAgFnPc1d1	nositelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odznaky	odznak	k1gInPc1	odznak
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
dekorace	dekorace	k1gFnPc4	dekorace
nejrůznějšího	různý	k2eAgInSc2d3	nejrůznější
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
zavěšeny	zavěsit	k5eAaPmNgInP	zavěsit
na	na	k7c6	na
stuze	stuha	k1gFnSc6	stuha
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
např.	např.	kA	např.
čestné	čestný	k2eAgFnSc2d1	čestná
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnSc2d1	pamětní
<g/>
,	,	kIx,	,
jubilejní	jubilejní	k2eAgFnSc2d1	jubilejní
<g/>
,	,	kIx,	,
služební	služební	k2eAgFnSc2d1	služební
<g/>
,	,	kIx,	,
výkonnostní	výkonnostní	k2eAgFnSc2d1	výkonnostní
<g/>
,	,	kIx,	,
za	za	k7c4	za
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
,	,	kIx,	,
záslužné	záslužný	k2eAgNnSc4d1	záslužné
<g/>
,	,	kIx,	,
válečné	válečný	k2eAgNnSc4d1	válečné
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
s	s	k7c7	s
odznaky	odznak	k1gInPc7	odznak
příležitostnými	příležitostný	k2eAgInPc7d1	příležitostný
(	(	kIx(	(
<g/>
sportovní	sportovní	k2eAgFnSc2d1	sportovní
<g/>
,	,	kIx,	,
spolkové	spolkový	k2eAgFnSc2d1	spolková
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnSc2d1	pamětní
<g/>
,	,	kIx,	,
podnikové	podnikový	k2eAgFnSc2d1	podniková
<g/>
,	,	kIx,	,
upomínkové	upomínkový	k2eAgFnSc2d1	upomínková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nP	nosit
většinou	většinou	k6eAd1	většinou
jako	jako	k9	jako
klopové	klop	k1gMnPc1	klop
a	a	k8xC	a
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
příslušnost	příslušnost	k1gFnSc4	příslušnost
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
sympatie	sympatie	k1gFnPc4	sympatie
<g/>
)	)	kIx)	)
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
organizaci	organizace	k1gFnSc3	organizace
či	či	k8xC	či
instituci	instituce	k1gFnSc3	instituce
projevenou	projevený	k2eAgFnSc4d1	projevená
zakoupením	zakoupení	k1gNnSc7	zakoupení
odznaku	odznak	k1gInSc2	odznak
<g/>
.	.	kIx.	.
</s>
<s>
Odznak	odznak	k1gInSc1	odznak
jako	jako	k8xS	jako
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
udělen	udělen	k2eAgMnSc1d1	udělen
za	za	k7c4	za
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc4	účast
formou	forma	k1gFnSc7	forma
dekorování	dekorování	k1gNnSc2	dekorování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
odznaku	odznak	k1gInSc2	odznak
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
klopového	klopový	k2eAgInSc2d1	klopový
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
miniatury	miniatura	k1gFnPc4	miniatura
ostatních	ostatní	k2eAgNnPc2d1	ostatní
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
(	(	kIx(	(
<g/>
křížů	kříž	k1gInPc2	kříž
<g/>
,	,	kIx,	,
medailí	medaile	k1gFnPc2	medaile
i	i	k8xC	i
řádů	řád	k1gInPc2	řád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Popis	popis	k1gInSc1	popis
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
===	===	k?	===
</s>
</p>
<p>
<s>
Faleristické	Faleristický	k2eAgNnSc4d1	Faleristický
názvosloví	názvosloví	k1gNnSc4	názvosloví
používáme	používat	k5eAaImIp1nP	používat
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
exemplářů	exemplář	k1gInPc2	exemplář
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řádový	řádový	k2eAgInSc1d1	řádový
odznak	odznak	k1gInSc1	odznak
–	–	k?	–
základní	základní	k2eAgInSc1d1	základní
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
řádových	řádový	k2eAgFnPc6d1	řádová
třídách	třída	k1gFnPc6	třída
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
příslušný	příslušný	k2eAgInSc4d1	příslušný
řád	řád	k1gInSc4	řád
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řádový	řádový	k2eAgInSc1d1	řádový
řetěz	řetěz	k1gInSc1	řetěz
–	–	k?	–
kolana	kolana	k1gFnSc1	kolana
–	–	k?	–
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
hlavy	hlava	k1gFnPc4	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
článků	článek	k1gInPc2	článek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
zavěšen	zavěsit	k5eAaPmNgInS	zavěsit
řádový	řádový	k2eAgInSc1d1	řádový
odznak	odznak	k1gInSc1	odznak
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
klenot	klenot	k1gInSc1	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
zavěšuje	zavěšovat	k5eAaImIp3nS	zavěšovat
na	na	k7c4	na
velkostuhu	velkostuha	k1gFnSc4	velkostuha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Medailon	medailon	k1gInSc1	medailon
–	–	k?	–
samostatný	samostatný	k2eAgInSc1d1	samostatný
nebo	nebo	k8xC	nebo
středová	středový	k2eAgFnSc1d1	středová
součást	součást	k1gFnSc1	součást
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
hvězdice	hvězdice	k1gFnPc1	hvězdice
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
terče	terč	k1gFnSc2	terč
a	a	k8xC	a
mezikruží	mezikruží	k1gNnSc2	mezikruží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terči	terč	k1gFnSc6	terč
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
řádů	řád	k1gInPc2	řád
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
řádový	řádový	k2eAgInSc1d1	řádový
odznak	odznak	k1gInSc1	odznak
<g/>
,	,	kIx,	,
v	v	k7c6	v
mezikruží	mezikruží	k1gNnSc6	mezikruží
řádové	řádový	k2eAgNnSc4d1	řádové
heslo	heslo	k1gNnSc4	heslo
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
latinské	latinský	k2eAgFnPc1d1	Latinská
<g/>
.	.	kIx.	.
</s>
<s>
Kruhové	kruhový	k2eAgNnSc4d1	kruhové
umístění	umístění	k1gNnSc4	umístění
textu	text	k1gInSc2	text
v	v	k7c6	v
mezikruží	mezikruží	k1gNnSc6	mezikruží
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
medaili	medaile	k1gFnSc4	medaile
<g/>
)	)	kIx)	)
nazýváme	nazývat	k5eAaImIp1nP	nazývat
opisem	opis	k1gInSc7	opis
<g/>
,	,	kIx,	,
vodorovně	vodorovně	k6eAd1	vodorovně
psané	psaný	k2eAgInPc4d1	psaný
texty	text	k1gInPc4	text
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
nápisy	nápis	k1gInPc4	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Medailon	medailon	k1gInSc1	medailon
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
doplněn	doplnit	k5eAaPmNgInS	doplnit
okružím	okruží	k1gNnSc7	okruží
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
kovový	kovový	k2eAgInSc1d1	kovový
prstenec	prstenec	k1gInSc1	prstenec
z	z	k7c2	z
krátkých	krátký	k2eAgInPc2d1	krátký
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
nelze	lze	k6eNd1	lze
nazvat	nazvat	k5eAaBmF	nazvat
věncem	věnec	k1gInSc7	věnec
tvořeným	tvořený	k2eAgInSc7d1	tvořený
ze	z	k7c2	z
stylizovaných	stylizovaný	k2eAgInPc2d1	stylizovaný
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
květů	květ	k1gInPc2	květ
či	či	k8xC	či
ratolesti	ratolest	k1gFnPc4	ratolest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kříže	kříž	k1gInPc1	kříž
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
ramena	rameno	k1gNnPc1	rameno
a	a	k8xC	a
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
je	on	k3xPp3gFnPc4	on
typově	typově	k6eAd1	typově
(	(	kIx(	(
<g/>
např.	např.	kA	např.
latinský	latinský	k2eAgMnSc1d1	latinský
<g/>
,	,	kIx,	,
řecký	řecký	k2eAgMnSc1d1	řecký
<g/>
,	,	kIx,	,
liliový	liliový	k2eAgMnSc1d1	liliový
<g/>
,	,	kIx,	,
ondřejský	ondřejský	k2eAgMnSc1d1	ondřejský
<g/>
,	,	kIx,	,
maltézský	maltézský	k2eAgMnSc1d1	maltézský
<g/>
,	,	kIx,	,
tlapatý	tlapatý	k2eAgMnSc1d1	tlapatý
<g/>
,	,	kIx,	,
hákový	hákový	k2eAgMnSc1d1	hákový
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Ramena	rameno	k1gNnPc1	rameno
křížů	kříž	k1gInPc2	kříž
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
hroty	hrot	k1gInPc4	hrot
(	(	kIx(	(
<g/>
ostré	ostrý	k2eAgInPc1d1	ostrý
<g/>
,	,	kIx,	,
zakulacené	zakulacený	k2eAgInPc1d1	zakulacený
<g/>
,	,	kIx,	,
s	s	k7c7	s
kuličkou	kulička	k1gFnSc7	kulička
<g/>
,	,	kIx,	,
rozeklané	rozeklaný	k2eAgNnSc1d1	rozeklané
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Mezi	mezi	k7c7	mezi
rameny	rameno	k1gNnPc7	rameno
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
paprsky	paprsek	k1gInPc4	paprsek
lilie	lilie	k1gFnSc2	lilie
<g/>
,	,	kIx,	,
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
orli	orel	k1gMnPc1	orel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
střed	střed	k1gInSc4	střed
kříže	kříž	k1gInSc2	kříž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
položen	položen	k2eAgInSc4d1	položen
medailon	medailon	k1gInSc4	medailon
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podložen	podložit	k5eAaPmNgMnS	podložit
věncem	věnec	k1gInSc7	věnec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
–	–	k?	–
zásadně	zásadně	k6eAd1	zásadně
se	se	k3xPyFc4	se
nenosí	nosit	k5eNaImIp3nP	nosit
na	na	k7c6	na
stuze	stuha	k1gFnSc6	stuha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
příchytek	příchytka	k1gFnPc2	příchytka
<g/>
,	,	kIx,	,
jehlicí	jehlice	k1gFnSc7	jehlice
či	či	k8xC	či
šroubovou	šroubový	k2eAgFnSc7d1	šroubová
maticí	matice	k1gFnSc7	matice
upevněna	upevněn	k2eAgFnSc1d1	upevněna
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
nositele	nositel	k1gMnSc2	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
vyšších	vysoký	k2eAgFnPc2d2	vyšší
tříd	třída	k1gFnPc2	třída
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
počet	počet	k1gInSc1	počet
cípů	cíp	k1gInPc2	cíp
<g/>
,	,	kIx,	,
za	za	k7c4	za
hrot	hrot	k1gInSc4	hrot
považujeme	považovat	k5eAaImIp1nP	považovat
jen	jen	k9	jen
vlastní	vlastní	k2eAgFnSc4d1	vlastní
špičku	špička	k1gFnSc4	špička
cípu	cíp	k1gInSc2	cíp
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
cíp	cíp	k1gInSc4	cíp
hvězdy	hvězda	k1gFnSc2	hvězda
tvořen	tvořen	k2eAgInSc4d1	tvořen
svazkem	svazek	k1gInSc7	svazek
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
jí	on	k3xPp3gFnSc7	on
paprskovou	paprskový	k2eAgFnSc7d1	paprsková
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Paprsky	paprsek	k1gInPc1	paprsek
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
ploché	plochý	k2eAgInPc4d1	plochý
<g/>
,	,	kIx,	,
perlené	perlený	k2eAgInPc4d1	perlený
<g/>
,	,	kIx,	,
briliantizující	briliantizující	k2eAgInPc4d1	briliantizující
<g/>
,	,	kIx,	,
plamenné	plamenný	k2eAgInPc4d1	plamenný
<g/>
,	,	kIx,	,
rozeklané	rozeklaný	k2eAgInPc4d1	rozeklaný
<g/>
,	,	kIx,	,
šípové	šípový	k2eAgInPc4d1	šípový
apod.	apod.	kA	apod.
Paprsky	paprsek	k1gInPc1	paprsek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
cípy	cíp	k1gInPc4	cíp
nazýváme	nazývat	k5eAaImIp1nP	nazývat
mezipaprsky	mezipaprsky	k6eAd1	mezipaprsky
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
křížové	křížový	k2eAgFnSc2d1	křížová
hvězdy	hvězda	k1gFnSc2	hvězda
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
základ	základ	k1gInSc4	základ
řádový	řádový	k2eAgInSc1d1	řádový
odznak	odznak	k1gInSc1	odznak
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kříže	kříž	k1gInSc2	kříž
doplněný	doplněný	k2eAgInSc4d1	doplněný
paprsky	paprsek	k1gInPc4	paprsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hvězdice	hvězdice	k1gFnSc1	hvězdice
–	–	k?	–
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
na	na	k7c6	na
stuze	stuha	k1gFnSc6	stuha
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
několikadílné	několikadílný	k2eAgFnPc1d1	několikadílná
nebo	nebo	k8xC	nebo
několikacípé	několikacípý	k2eAgInPc1d1	několikacípý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pětidílná	pětidílný	k2eAgFnSc1d1	pětidílná
<g/>
,	,	kIx,	,
osmicípá	osmicípý	k2eAgFnSc1d1	osmicípá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díly	díl	k1gInPc1	díl
jsou	být	k5eAaImIp3nP	být
dvouhroté	dvouhrotý	k2eAgInPc1d1	dvouhrotý
nebo	nebo	k8xC	nebo
tříhroté	tříhrotý	k2eAgInPc1d1	tříhrotý
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
na	na	k7c6	na
středovém	středový	k2eAgInSc6d1	středový
hrotu	hrot	k1gInSc6	hrot
nedotýkají	dotýkat	k5eNaImIp3nP	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Cípy	cíp	k1gInPc1	cíp
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgInSc4	jeden
hrot	hrot	k1gInSc4	hrot
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
popisu	popis	k1gInSc6	popis
povrchu	povrch	k1gInSc2	povrch
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
vyrobena	vyroben	k2eAgNnPc1d1	vyrobeno
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
a	a	k8xC	a
smaltovaná	smaltovaný	k2eAgFnSc1d1	smaltovaná
či	či	k8xC	či
barvená	barvený	k2eAgFnSc1d1	barvená
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
následujících	následující	k2eAgFnPc2d1	následující
charakteristik	charakteristika	k1gFnPc2	charakteristika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lemování	lemování	k1gNnSc1	lemování
–	–	k?	–
plošné	plošný	k2eAgFnSc2d1	plošná
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
</s>
</p>
<p>
<s>
Rámování	rámování	k1gNnSc1	rámování
–	–	k?	–
plošné	plošný	k2eAgFnSc2d1	plošná
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
vnější	vnější	k2eAgFnSc1d1	vnější
<g/>
,	,	kIx,	,
souvislá	souvislý	k2eAgFnSc1d1	souvislá
kovová	kovový	k2eAgFnSc1d1	kovová
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fasetování	fasetování	k1gNnSc1	fasetování
–	–	k?	–
převýšené	převýšený	k2eAgNnSc4d1	převýšené
rámování	rámování	k1gNnSc4	rámování
kovem	kov	k1gInSc7	kov
<g/>
,	,	kIx,	,
vnější	vnější	k2eAgFnSc7d1	vnější
</s>
</p>
<p>
<s>
Vroubení	vroubení	k1gNnSc1	vroubení
–	–	k?	–
převýšené	převýšený	k2eAgFnSc2d1	převýšená
vroubkované	vroubkovaný	k2eAgFnSc2d1	vroubkovaná
rámování	rámování	k1gNnSc2	rámování
kovem	kov	k1gInSc7	kov
(	(	kIx(	(
<g/>
popisuje	popisovat	k5eAaImIp3nS	popisovat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
zevnitř	zevnitř	k6eAd1	zevnitř
k	k	k7c3	k
okraji	okraj	k1gInSc3	okraj
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Stuhy	stuha	k1gFnSc2	stuha
===	===	k?	===
</s>
</p>
<p>
<s>
Stuhy	stuha	k1gFnPc1	stuha
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
každého	každý	k3xTgNnSc2	každý
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gMnSc4	on
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
a	a	k8xC	a
často	často	k6eAd1	často
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
<g/>
.	.	kIx.	.
</s>
<s>
Nasmějí	nasmát	k5eAaBmIp3nP	nasmát
být	být	k5eAaImF	být
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
státním	státní	k2eAgInSc6d1	státní
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
teritoriálním	teritoriální	k2eAgInSc6d1	teritoriální
okruhu	okruh	k1gInSc6	okruh
zaměnitelné	zaměnitelný	k2eAgNnSc1d1	zaměnitelné
<g/>
.	.	kIx.	.
</s>
<s>
Posloupnost	posloupnost	k1gFnSc1	posloupnost
barev	barva	k1gFnPc2	barva
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
heraldicky	heraldicky	k6eAd1	heraldicky
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
číselným	číselný	k2eAgInSc7d1	číselný
poměrem	poměr	k1gInSc7	poměr
v	v	k7c6	v
milimetrech	milimetr	k1gInPc6	milimetr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
červeno-bílo-modro-bílo-červená	červenoíloodroílo-červený	k2eAgFnSc1d1	červeno-bílo-modro-bílo-červený
stuha	stuha	k1gFnSc1	stuha
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
Řád	řád	k1gInSc1	řád
TGM	TGM	kA	TGM
IV	IV	kA	IV
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
)	)	kIx)	)
Pruhy	pruh	k1gInPc1	pruh
užší	úzký	k2eAgFnSc2d2	užší
než	než	k8xS	než
1	[number]	k4	1
mm	mm	kA	mm
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
průsvitky	průsvitka	k1gFnPc4	průsvitka
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
za	za	k7c4	za
lem	lem	k1gInSc4	lem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkostuha	Velkostuha	k1gFnSc1	Velkostuha
(	(	kIx(	(
<g/>
šerpa	šerpa	k1gFnSc1	šerpa
<g/>
)	)	kIx)	)
–	–	k?	–
průměrně	průměrně	k6eAd1	průměrně
10	[number]	k4	10
cm	cm	kA	cm
široká	široký	k2eAgFnSc1d1	široká
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
šikmo	šikmo	k6eAd1	šikmo
přes	přes	k7c4	přes
rameno	rameno	k1gNnSc4	rameno
k	k	k7c3	k
protilehlému	protilehlý	k2eAgInSc3d1	protilehlý
boku	bok	k1gInSc3	bok
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
sešitá	sešitý	k2eAgFnSc1d1	sešitá
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
připevněn	připevněn	k2eAgInSc4d1	připevněn
klenot	klenot	k1gInSc4	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Sešití	sešití	k1gNnSc1	sešití
překrývá	překrývat	k5eAaImIp3nS	překrývat
mašle	mašle	k1gFnSc1	mašle
nebo	nebo	k8xC	nebo
roseta	roseta	k1gFnSc1	roseta
<g/>
.	.	kIx.	.
</s>
<s>
Určena	určen	k2eAgFnSc1d1	určena
pro	pro	k7c4	pro
I.	I.	kA	I.
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náhrdelní	náhrdelní	k2eAgFnSc1d1	náhrdelní
stuha	stuha	k1gFnSc1	stuha
–	–	k?	–
široká	široký	k2eAgFnSc1d1	široká
kolem	kolem	k6eAd1	kolem
5	[number]	k4	5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
je	být	k5eAaImIp3nS	být
zavěšen	zavěsit	k5eAaPmNgInS	zavěsit
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
svislé	svislý	k2eAgFnSc2d1	svislá
spony	spona	k1gFnSc2	spona
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
III	III	kA	III
<g/>
.	.	kIx.	.
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
i	i	k9	i
pro	pro	k7c4	pro
I.	I.	kA	I.
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náprsní	náprsní	k2eAgFnSc1d1	náprsní
stuha	stuha	k1gFnSc1	stuha
–	–	k?	–
široká	široký	k2eAgFnSc1d1	široká
kolem	kolem	k6eAd1	kolem
3	[number]	k4	3
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
průvlečnou	průvlečný	k2eAgFnSc4d1	průvlečná
<g/>
,	,	kIx,	,
trojúhelníkovou	trojúhelníkový	k2eAgFnSc4d1	trojúhelníková
<g/>
,	,	kIx,	,
pentagon	pentagon	k1gInSc4	pentagon
<g/>
,	,	kIx,	,
obloučkovou	obloučkový	k2eAgFnSc4d1	obloučková
<g/>
,	,	kIx,	,
ráhnovou	ráhnový	k2eAgFnSc4d1	ráhnová
<g/>
,	,	kIx,	,
rámečkovou	rámečkový	k2eAgFnSc4d1	rámečková
<g/>
,	,	kIx,	,
se	s	k7c7	s
zavěšenou	zavěšený	k2eAgFnSc7d1	zavěšená
sponou	spona	k1gFnSc7	spona
<g/>
,	,	kIx,	,
motýlovou	motýlový	k2eAgFnSc7d1	Motýlová
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stužka	stužka	k1gFnSc1	stužka
–	–	k?	–
náhradní	náhradní	k2eAgFnSc4d1	náhradní
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
vysoká	vysoký	k2eAgFnSc1d1	vysoká
1	[number]	k4	1
cm	cm	kA	cm
<g/>
,	,	kIx,	,
široká	široký	k2eAgFnSc1d1	široká
jako	jako	k8xS	jako
náprsní	náprsní	k2eAgFnSc1d1	náprsní
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
na	na	k7c6	na
stejnokroji	stejnokroj	k1gInSc6	stejnokroj
nad	nad	k7c7	nad
levou	levý	k2eAgFnSc7d1	levá
kapsou	kapsa	k1gFnSc7	kapsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
typem	typ	k1gInSc7	typ
podle	podle	k7c2	podle
umístění	umístění	k1gNnSc2	umístění
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgInSc1d1	britský
podvazkový	podvazkový	k2eAgInSc1d1	podvazkový
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
nošený	nošený	k2eAgInSc1d1	nošený
na	na	k7c6	na
stehně	stehno	k1gNnSc6	stehno
(	(	kIx(	(
<g/>
podvazku	podvazek	k1gInSc2	podvazek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Stuhy	stuha	k1gFnPc4	stuha
náprsní	náprsní	k2eAgFnPc4d1	náprsní
a	a	k8xC	a
stužky	stužka	k1gFnPc1	stužka
se	se	k3xPyFc4	se
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
kovovými	kovový	k2eAgInPc7d1	kovový
štítky	štítek	k1gInPc7	štítek
se	se	k3xPyFc4	se
jmény	jméno	k1gNnPc7	jméno
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
tažení	tažení	k1gNnSc1	tažení
<g/>
,	,	kIx,	,
čísly	číslo	k1gNnPc7	číslo
<g/>
,	,	kIx,	,
ratolestmi	ratolest	k1gFnPc7	ratolest
<g/>
,	,	kIx,	,
hvězdičkami	hvězdička	k1gFnPc7	hvězdička
<g/>
.	.	kIx.	.
</s>
<s>
Stuhy	stuha	k1gFnPc1	stuha
se	se	k3xPyFc4	se
k	k	k7c3	k
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
vyznamenání	vyznamenání	k1gNnSc3	vyznamenání
připevňují	připevňovat	k5eAaImIp3nP	připevňovat
oušky	ouško	k1gNnPc7	ouško
<g/>
,	,	kIx,	,
kroužky	kroužek	k1gInPc7	kroužek
a	a	k8xC	a
sponami	spona	k1gFnPc7	spona
<g/>
.	.	kIx.	.
</s>
<s>
Ouško	ouško	k1gNnSc1	ouško
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kolmé	kolmý	k2eAgFnPc4d1	kolmá
<g/>
,	,	kIx,	,
kulové	kulový	k2eAgFnPc4d1	kulová
<g/>
,	,	kIx,	,
obdélníkové	obdélníkový	k2eAgFnPc4d1	obdélníková
atd.	atd.	kA	atd.
Řádové	řádový	k2eAgInPc1d1	řádový
odznaky	odznak	k1gInPc1	odznak
i	i	k8xC	i
dekorace	dekorace	k1gFnPc1	dekorace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
převýšeny	převýšen	k2eAgInPc1d1	převýšen
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
meči	meč	k1gInPc7	meč
a	a	k8xC	a
ratolestmi	ratolest	k1gFnPc7	ratolest
<g/>
.	.	kIx.	.
</s>
<s>
Stuhy	stuha	k1gFnPc1	stuha
se	se	k3xPyFc4	se
k	k	k7c3	k
oblečení	oblečení	k1gNnSc3	oblečení
přichycují	přichycovat	k5eAaImIp3nP	přichycovat
jehlicemi	jehlice	k1gFnPc7	jehlice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c6	na
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
<g/>
,	,	kIx,	,
svislé	svislý	k2eAgFnSc6d1	svislá
<g/>
,	,	kIx,	,
pevné	pevný	k2eAgFnSc3d1	pevná
<g/>
,	,	kIx,	,
sklopné	sklopný	k2eAgFnSc3d1	sklopná
<g/>
,	,	kIx,	,
pérové	pérový	k2eAgFnSc3d1	pérová
<g/>
,	,	kIx,	,
zapichovací	zapichovací	k2eAgFnSc3d1	zapichovací
<g/>
,	,	kIx,	,
připínací	připínací	k2eAgFnSc3d1	připínací
a	a	k8xC	a
zasouvací	zasouvací	k2eAgFnSc3d1	zasouvací
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ještě	ještě	k9	ještě
se	s	k7c7	s
zajištěním	zajištění	k1gNnSc7	zajištění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Miniatury	miniatura	k1gFnSc2	miniatura
===	===	k?	===
</s>
</p>
<p>
<s>
Miniatury	miniatura	k1gFnPc1	miniatura
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
na	na	k7c6	na
večerním	večerní	k2eAgMnSc6d1	večerní
či	či	k8xC	či
občanském	občanský	k2eAgInSc6d1	občanský
oděvu	oděv	k1gInSc6	oděv
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
rosety	roseta	k1gFnPc1	roseta
v	v	k7c6	v
klopové	klopový	k2eAgFnSc6d1	klopový
dírce	dírka	k1gFnSc6	dírka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
též	též	k9	též
mohou	moct	k5eAaImIp3nP	moct
protáhnout	protáhnout	k5eAaPmF	protáhnout
zúžené	zúžený	k2eAgFnPc4d1	zúžená
stuhy	stuha	k1gFnPc4	stuha
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
8	[number]	k4	8
mm	mm	kA	mm
široké	široký	k2eAgNnSc4d1	široké
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
LOBKOWICZ	Lobkowicz	k1gMnSc1	Lobkowicz
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
238	[number]	k4	238
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901579	[number]	k4	901579
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MĚŘIČKA	měřička	k1gFnSc1	měřička
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
Buch	buch	k1gInSc1	buch
der	drát	k5eAaImRp2nS	drát
Orden	Orden	k1gInSc1	Orden
und	und	k?	und
Auszeichnungen	Auszeichnungen	k1gInSc1	Auszeichnungen
<g/>
.	.	kIx.	.
</s>
<s>
Hanau	Hana	k1gMnSc3	Hana
/	/	kIx~	/
<g/>
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Verlag	Verlag	k1gMnSc1	Verlag
Werner	Werner	k1gMnSc1	Werner
Dausien	Dausina	k1gFnPc2	Dausina
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
252	[number]	k4	252
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7684	[number]	k4	7684
<g/>
-	-	kIx~	-
<g/>
1680	[number]	k4	1680
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
</s>
</p>
