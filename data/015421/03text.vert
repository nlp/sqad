<s>
Sesto	Sesta	k1gMnSc5
Calende	Calend	k1gMnSc5
</s>
<s>
Sesto	Sesta	k1gMnSc5
Calende	Calend	k1gMnSc5
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnPc4
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
38	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
8	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
198	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
region	region	k1gInSc1
</s>
<s>
Lombardie	Lombardie	k1gFnSc1
provincie	provincie	k1gFnSc2
</s>
<s>
Varese	Varese	k1gFnSc1
</s>
<s>
Sesto	Sesta	k1gMnSc5
Calende	Calend	k1gMnSc5
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
23,89	23,89	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
10	#num#	k4
973	#num#	k4
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
459,3	459,3	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.comune.sesto-calende.va.it	www.comune.sesto-calende.va.it	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
0331	#num#	k4
PSČ	PSČ	kA
</s>
<s>
21018	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
VA	va	k0wR
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sesto	Sesto	k1gNnSc1
Calende	Calende	k1gNnSc1
je	být	k5eAaImIp3nS
italská	italský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
v	v	k7c6
provincii	provincie	k1gFnSc6
Varese	Varese	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Lombardie	Lombardie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec	obec	k1gFnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
břehu	břeh	k1gInSc6
jezera	jezero	k1gNnSc2
Lago	Laga	k1gMnSc5
Maggiore	Maggior	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2011	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
10	#num#	k4
973	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1
obce	obec	k1gFnPc1
</s>
<s>
Angera	Angera	k1gFnSc1
<g/>
,	,	kIx,
Cadrezzate	Cadrezzat	k1gMnSc5
<g/>
,	,	kIx,
Castelletto	Castellett	k2eAgNnSc1d1
sopra	sopra	k6eAd1
Ticino	Ticin	k2eAgNnSc1d1
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Comabbio	Comabbio	k1gNnSc1
<g/>
,	,	kIx,
Dormelletto	Dormelletto	k1gNnSc1
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Golasecca	Golasecca	k1gFnSc1
<g/>
,	,	kIx,
Mercallo	Mercallo	k1gNnSc1
<g/>
,	,	kIx,
Osmate	Osmat	k1gMnSc5
<g/>
,	,	kIx,
Taino	Taino	k6eAd1
<g/>
,	,	kIx,
Vergiate	Vergiat	k1gInSc5
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Lombardie	Lombardie	k1gFnSc1
•	•	k?
Obce	obec	k1gFnSc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Varese	Varese	k1gFnSc2
</s>
<s>
Agra	Agra	k1gFnSc1
•	•	k?
Albizzate	Albizzat	k1gInSc5
•	•	k?
Angera	Anger	k1gMnSc2
•	•	k?
Arcisate	Arcisat	k1gInSc5
•	•	k?
Arsago	Arsago	k1gMnSc1
Seprio	Seprio	k1gMnSc1
•	•	k?
Azzate	Azzat	k1gInSc5
•	•	k?
Azzio	Azzio	k1gMnSc1
•	•	k?
Barasso	Barassa	k1gFnSc5
•	•	k?
Bardello	Bardello	k1gNnSc1
•	•	k?
Bedero	Bedero	k1gNnSc1
Valcuvia	Valcuvius	k1gMnSc2
•	•	k?
Besano	Besana	k1gFnSc5
•	•	k?
Besnate	Besnat	k1gInSc5
•	•	k?
Besozzo	Besozza	k1gFnSc5
•	•	k?
Biandronno	Biandronno	k1gNnSc4
•	•	k?
Bisuschio	Bisuschio	k1gMnSc1
•	•	k?
Bodio	Bodio	k1gMnSc1
Lomnago	Lomnago	k1gMnSc1
•	•	k?
Brebbia	Brebbia	k1gFnSc1
•	•	k?
Bregano	Bregana	k1gFnSc5
•	•	k?
Brenta	Brent	k1gInSc2
•	•	k?
Brezzo	Brezza	k1gFnSc5
di	di	k?
Bedero	Bedero	k1gNnSc4
•	•	k?
Brinzio	Brinzio	k1gMnSc1
•	•	k?
Brissago-Valtravaglia	Brissago-Valtravaglia	k1gFnSc1
•	•	k?
Brunello	Brunello	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Brusimpiano	Brusimpiana	k1gFnSc5
•	•	k?
Buguggiate	Buguggiat	k1gInSc5
•	•	k?
Busto	busta	k1gFnSc5
Arsizio	Arsizio	k1gMnSc1
•	•	k?
Cadegliano-Viconago	Cadegliano-Viconago	k1gMnSc1
•	•	k?
Cadrezzate	Cadrezzat	k1gInSc5
•	•	k?
Cairate	Cairat	k1gInSc5
•	•	k?
Cantello	Cantello	k1gNnSc4
•	•	k?
Caravate	Caravat	k1gInSc5
•	•	k?
Cardano	Cardana	k1gFnSc5
al	ala	k1gFnPc2
Campo	Campa	k1gFnSc5
•	•	k?
Carnago	Carnago	k6eAd1
•	•	k?
Caronno	Caronno	k1gNnSc1
Pertusella	Pertusella	k1gMnSc1
•	•	k?
Caronno	Caronno	k6eAd1
Varesino	Varesin	k2eAgNnSc4d1
•	•	k?
Casale	Casala	k1gFnSc3
Litta	Litta	k1gMnSc1
•	•	k?
Casalzuigno	Casalzuigno	k6eAd1
•	•	k?
Casciago	Casciago	k1gMnSc1
•	•	k?
Casorate	Casorat	k1gInSc5
Sempione	Sempion	k1gInSc5
•	•	k?
Cassano	Cassana	k1gFnSc5
Magnago	Magnaga	k1gFnSc5
•	•	k?
Cassano	Cassana	k1gFnSc5
Valcuvia	Valcuvium	k1gNnPc1
•	•	k?
Castellanza	Castellanz	k1gMnSc2
•	•	k?
Castello	Castello	k1gNnSc1
Cabiaglio	Cabiaglio	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Castelseprio	Castelseprio	k1gMnSc1
•	•	k?
Castelveccana	Castelveccana	k1gFnSc1
•	•	k?
Castiglione	Castiglion	k1gInSc5
Olona	Oloen	k2eAgMnSc4d1
•	•	k?
Castronno	Castronno	k6eAd1
•	•	k?
Cavaria	Cavarium	k1gNnSc2
con	con	k?
Premezzo	Premezza	k1gFnSc5
•	•	k?
Cazzago	Cazzago	k1gMnSc1
Brabbia	Brabbium	k1gNnSc2
•	•	k?
Cislago	Cislago	k1gMnSc1
•	•	k?
Cittiglio	Cittiglio	k1gMnSc1
•	•	k?
Clivio	Clivio	k1gMnSc1
•	•	k?
Cocquio-Trevisago	Cocquio-Trevisago	k1gMnSc1
•	•	k?
Comabbio	Comabbio	k1gMnSc1
•	•	k?
Comerio	Comerio	k1gMnSc1
•	•	k?
Cremenaga	Cremenaga	k1gFnSc1
•	•	k?
Crosio	Crosio	k6eAd1
della	delnout	k5eAaPmAgFnS,k5eAaImAgFnS,k5eAaBmAgFnS
Valle	Valle	k1gFnSc1
•	•	k?
Cuasso	Cuassa	k1gFnSc5
al	ala	k1gFnPc2
Monte	Mont	k1gInSc5
•	•	k?
Cugliate-Fabiasco	Cugliate-Fabiasco	k1gMnSc1
•	•	k?
Cunardo	Cunardo	k1gNnSc4
•	•	k?
Curiglia	Curiglium	k1gNnSc2
con	con	k?
Monteviasco	Monteviasco	k1gMnSc1
•	•	k?
Cuveglio	Cuveglio	k1gMnSc1
•	•	k?
Cuvio	Cuvio	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Daverio	Daverio	k1gMnSc1
•	•	k?
Dumenza	Dumenza	k1gFnSc1
•	•	k?
Duno	duna	k1gFnSc5
•	•	k?
Fagnano	Fagnana	k1gFnSc5
Olona	Oloen	k2eAgMnSc4d1
•	•	k?
Ferno	Ferno	k6eAd1
•	•	k?
Ferrera	Ferrera	k1gFnSc1
di	di	k?
Varese	Varese	k1gFnSc2
•	•	k?
Gallarate	Gallarat	k1gInSc5
•	•	k?
Galliate	Galliat	k1gMnSc5
Lombardo	Lombardo	k1gMnSc5
•	•	k?
Gavirate	Gavirat	k1gInSc5
•	•	k?
Gazzada	Gazzada	k1gFnSc1
Schianno	Schianno	k6eAd1
•	•	k?
Gemonio	Gemonio	k1gMnSc1
•	•	k?
Gerenzano	Gerenzana	k1gFnSc5
•	•	k?
Germignaga	Germignag	k1gMnSc2
•	•	k?
Golasecca	Golaseccus	k1gMnSc2
•	•	k?
Gorla	Gorl	k1gMnSc2
Maggiore	Maggior	k1gInSc5
•	•	k?
Gorla	Gorl	k1gMnSc2
Minore	Minor	k1gInSc5
•	•	k?
Gornate-Olona	Gornate-Olona	k1gFnSc1
•	•	k?
Grantola	Grantola	k1gFnSc1
•	•	k?
Inarzo	Inarza	k1gFnSc5
•	•	k?
Induno	Induna	k1gFnSc5
Olona	Oloen	k2eAgFnSc1d1
•	•	k?
Ispra	Ispra	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Jerago	Jerago	k1gMnSc1
con	con	k?
Orago	Orago	k1gMnSc1
•	•	k?
Lavena	Lavena	k1gFnSc1
Ponte	Pont	k1gInSc5
Tresa	tresa	k1gFnSc1
•	•	k?
Laveno-Mombello	Laveno-Mombello	k1gNnSc1
•	•	k?
Leggiuno	Leggiuna	k1gFnSc5
•	•	k?
Lonate	Lonat	k1gInSc5
Ceppino	Ceppina	k1gFnSc5
•	•	k?
Lonate	Lonat	k1gInSc5
Pozzolo	Pozzola	k1gFnSc5
•	•	k?
Lozza	Lozza	k1gFnSc1
•	•	k?
Luino	Luino	k6eAd1
•	•	k?
Luvinate	Luvinat	k1gInSc5
•	•	k?
Maccagno	Maccagen	k2eAgNnSc1d1
con	con	k?
Pino	Pino	k1gNnSc1
e	e	k0
Veddasca	Veddascum	k1gNnPc1
•	•	k?
Malgesso	Malgessa	k1gFnSc5
•	•	k?
Malnate	Malnat	k1gInSc5
•	•	k?
Marchirolo	Marchirola	k1gFnSc5
•	•	k?
Marnate	Marnat	k1gInSc5
•	•	k?
Marzio	Marzio	k1gMnSc1
•	•	k?
Masciago	Masciago	k1gMnSc1
Primo	primo	k1gNnSc1
•	•	k?
Mercallo	Mercallo	k1gNnSc1
•	•	k?
Mesenzana	Mesenzan	k1gMnSc2
•	•	k?
Montegrino	Montegrino	k1gNnSc1
Valtravaglia	Valtravaglius	k1gMnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Monvalle	Monvalle	k1gFnSc1
•	•	k?
Morazzone	Morazzon	k1gInSc5
•	•	k?
Mornago	Mornago	k1gNnSc4
•	•	k?
Oggiona	Oggiona	k1gFnSc1
con	con	k?
Santo	Santo	k1gNnSc1
Stefano	Stefana	k1gFnSc5
•	•	k?
Olgiate	Olgiat	k1gInSc5
Olona	Olono	k1gNnSc2
•	•	k?
Origgio	Origgio	k1gMnSc1
•	•	k?
Orino	Orino	k6eAd1
•	•	k?
Osmate	Osmat	k1gInSc5
•	•	k?
Porto	porto	k1gNnSc1
Ceresio	Ceresio	k1gMnSc1
•	•	k?
Porto	porto	k1gNnSc1
Valtravaglia	Valtravaglium	k1gNnSc2
•	•	k?
Rancio	Rancio	k1gMnSc1
Valcuvia	Valcuvium	k1gNnSc2
•	•	k?
Ranco	Ranco	k1gMnSc1
•	•	k?
Saltrio	Saltrio	k1gMnSc1
•	•	k?
Samarate	Samarat	k1gInSc5
•	•	k?
Sangiano	Sangiana	k1gFnSc5
•	•	k?
Saronno	Saronen	k2eAgNnSc1d1
•	•	k?
Sesto	Sesta	k1gMnSc5
Calende	Calend	k1gMnSc5
•	•	k?
Solbiate	Solbiat	k1gMnSc5
Arno	Arna	k1gMnSc5
•	•	k?
Solbiate	Solbiat	k1gInSc5
Olona	Oloen	k2eAgFnSc1d1
•	•	k?
Somma	Somma	k1gFnSc1
Lombardo	Lombardo	k1gMnSc1
•	•	k?
Sumirago	Sumirago	k1gMnSc1
•	•	k?
Taino	Taino	k6eAd1
•	•	k?
Ternate	Ternat	k1gInSc5
•	•	k?
Tradate	Tradat	k1gInSc5
•	•	k?
Travedona-Monate	Travedona-Monat	k1gInSc5
•	•	k?
Tronzano	Tronzana	k1gFnSc5
Lago	Laga	k1gFnSc5
Maggiore	Maggior	k1gInSc5
•	•	k?
Uboldo	Uboldo	k1gNnSc4
•	•	k?
Valganna	Valgann	k1gInSc2
•	•	k?
Varano	Varana	k1gFnSc5
Borghi	Borghi	k1gNnSc7
•	•	k?
Varese	Varese	k1gFnSc2
•	•	k?
Vedano	Vedana	k1gFnSc5
Olona	Olona	k1gFnSc1
•	•	k?
Venegono	Venegona	k1gFnSc5
Inferiore	Inferior	k1gInSc5
•	•	k?
Venegono	Venegona	k1gFnSc5
Superiore	superior	k1gMnSc5
•	•	k?
Vergiate	Vergiat	k1gInSc5
•	•	k?
Viggiù	Viggiù	k1gMnSc7
•	•	k?
Vizzola	Vizzola	k1gFnSc1
Ticino	Ticin	k2eAgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
671700	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4386780-7	4386780-7	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
158260001	#num#	k4
</s>
