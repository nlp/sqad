<p>
<s>
Acyloinová	Acyloinový	k2eAgFnSc1d1	Acyloinový
kondenzace	kondenzace	k1gFnSc1	kondenzace
je	být	k5eAaImIp3nS	být
reduktivní	reduktivní	k2eAgFnSc1d1	reduktivní
kondenzace	kondenzace	k1gFnSc1	kondenzace
dvou	dva	k4xCgInPc2	dva
esterů	ester	k1gInPc2	ester
kovovým	kovový	k2eAgInSc7d1	kovový
sodíkem	sodík	k1gInSc7	sodík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
α	α	k?	α
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
též	též	k9	též
acyloin	acyloin	k2eAgMnSc1d1	acyloin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reakce	reakce	k1gFnSc1	reakce
nejlépe	dobře	k6eAd3	dobře
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
,	,	kIx,	,
když	když	k8xS	když
R	R	kA	R
je	být	k5eAaImIp3nS	být
alifatický	alifatický	k2eAgInSc4d1	alifatický
a	a	k8xC	a
inertní	inertní	k2eAgInSc4d1	inertní
zbytek	zbytek	k1gInSc4	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
aprotické	aprotický	k2eAgNnSc1d1	aprotický
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
varu	var	k1gInSc2	var
(	(	kIx(	(
<g/>
benzen	benzen	k1gInSc1	benzen
<g/>
,	,	kIx,	,
toluen	toluen	k1gInSc1	toluen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
protických	protický	k2eAgNnPc2d1	protický
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
Bouveaultově-Blancově	Bouveaultově-Blancův	k2eAgFnSc3d1	Bouveaultově-Blancův
redukci	redukce	k1gFnSc3	redukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
acyloinové	acyloinový	k2eAgFnSc6d1	acyloinový
kondenzaci	kondenzace	k1gFnSc6	kondenzace
diesterů	diester	k1gMnPc2	diester
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
kruhu	kruh	k1gInSc2	kruh
a	a	k8xC	a
sterických	sterický	k2eAgFnPc6d1	sterický
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
upřednostňována	upřednostňován	k2eAgFnSc1d1	upřednostňována
intramolekulární	intramolekulární	k2eAgFnSc1d1	intramolekulární
cyklizace	cyklizace	k1gFnSc1	cyklizace
před	před	k7c7	před
mezimolekulovou	mezimolekulův	k2eAgFnSc7d1	mezimolekulův
polymerizací	polymerizace	k1gFnSc7	polymerizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Acyloin	Acyloin	k1gMnSc1	Acyloin
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Acyloin	Acyloin	k2eAgInSc4d1	Acyloin
condensation	condensation	k1gInSc4	condensation
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
