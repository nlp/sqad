<s>
Smrk	smrk	k1gInSc1
pichlavý	pichlavý	k2eAgInSc1d1
</s>
<s>
Smrk	smrk	k1gInSc1
pichlavý	pichlavý	k2eAgInSc4d1
Smrk	smrk	k1gInSc4
pichlavý	pichlavý	k2eAgInSc1d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
nahosemenné	nahosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Pinophyta	Pinophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
jehličnany	jehličnan	k1gInPc1
(	(	kIx(
<g/>
Pinopsida	Pinopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
borovicotvaré	borovicotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Pinales	Pinales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
borovicovité	borovicovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Pinaceae	Pinacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
smrk	smrk	k1gInSc1
(	(	kIx(
<g/>
Picea	Picea	k1gFnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Picea	Picea	k1gMnSc1
pungensEngelm	pungensEngelm	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1879	#num#	k4
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Smrk	smrk	k1gInSc1
pichlavý	pichlavý	k2eAgInSc1d1
(	(	kIx(
<g/>
Picea	Picea	k1gFnSc1
pungens	pungens	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jehličnatý	jehličnatý	k2eAgInSc1d1
strom	strom	k1gInSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
borovicovité	borovicovitý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehličí	jehličí	k1gNnSc1
je	být	k5eAaImIp3nS
delší	dlouhý	k2eAgFnPc4d2
<g/>
,	,	kIx,
tuhé	tuhý	k2eAgFnPc4d1
a	a	k8xC
silně	silně	k6eAd1
pichlavé	pichlavý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
často	často	k6eAd1
vysazován	vysazovat	k5eAaImNgInS
v	v	k7c6
parcích	park	k1gInPc6
a	a	k8xC
zahradách	zahrada	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
používán	používán	k2eAgInSc4d1
jako	jako	k8xS,k8xC
vánoční	vánoční	k2eAgInSc4d1
stromeček	stromeček	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Byl	být	k5eAaImAgInS
objeven	objevit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1862	#num#	k4
Charlesem	Charles	k1gMnSc7
Christopherem	Christopher	k1gMnSc7
Parrym	Parrym	k1gInSc4
u	u	k7c2
Pikes	Pikesa	k1gFnPc2
Peak	Peako	k1gNnPc2
v	v	k7c6
USA	USA	kA
a	a	k8xC
hned	hned	k6eAd1
dalšího	další	k2eAgInSc2d1
roku	rok	k1gInSc2
se	se	k3xPyFc4
semena	semeno	k1gNnSc2
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
smíchaná	smíchaný	k2eAgFnSc1d1
s	s	k7c7
P.	P.	kA
engelmannii	engelmannie	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
zavdalo	zavdat	k5eAaPmAgNnS
příčinu	příčina	k1gFnSc4
k	k	k7c3
záměnám	záměna	k1gFnPc3
obou	dva	k4xCgFnPc2
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
mají	mít	k5eAaImIp3nP
sivé	sivý	k2eAgNnSc1d1
jehličí	jehličí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpoznáme	rozpoznat	k5eAaPmIp1nP
je	on	k3xPp3gFnPc4
však	však	k9
snadno	snadno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
P.	P.	kA
pungens	pungens	k6eAd1
má	mít	k5eAaImIp3nS
silné	silný	k2eAgNnSc1d1
<g/>
,	,	kIx,
žlutohnědé	žlutohnědý	k2eAgFnPc1d1
a	a	k8xC
lysé	lysý	k2eAgFnPc1d1
větvičky	větvička	k1gFnPc1
a	a	k8xC
jehlice	jehlice	k1gFnPc1
pichlavé	pichlavý	k2eAgFnPc1d1
na	na	k7c4
všechny	všechen	k3xTgFnPc4
strany	strana	k1gFnPc4
odstávající	odstávající	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silné	silný	k2eAgInPc1d1
a	a	k8xC
nepryskyřičnaté	pryskyřičnatý	k2eNgInPc1d1
pupeny	pupen	k1gInPc1
jsou	být	k5eAaImIp3nP
obaleny	obalen	k2eAgFnPc1d1
šupinami	šupina	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
bývají	bývat	k5eAaImIp3nP
na	na	k7c6
špičkách	špička	k1gFnPc6
zpět	zpět	k6eAd1
zahnuté	zahnutý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
P.	P.	kA
engelmannii	engelmannie	k1gFnSc4
má	mít	k5eAaImIp3nS
větvičky	větvička	k1gFnPc1
světlé	světlý	k2eAgFnPc1d1
a	a	k8xC
žlaznatě	žlaznatě	k6eAd1
chlupaté	chlupatý	k2eAgInPc1d1
a	a	k8xC
pupeny	pupen	k1gInPc1
pryskyřičnaté	pryskyřičnatý	k2eAgInPc1d1
s	s	k7c7
přitisklými	přitisklý	k2eAgFnPc7d1
šupinami	šupina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehličí	jehličí	k1gNnSc6
má	mít	k5eAaImIp3nS
měkčí	měkký	k2eAgInSc1d2
<g/>
,	,	kIx,
ne	ne	k9
tak	tak	k6eAd1
pichlavé	pichlavý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
a	a	k8xC
znaky	znak	k1gInPc1
stromu	strom	k1gInSc2
</s>
<s>
Vždy	vždy	k6eAd1
zelený	zelený	k2eAgInSc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
statný	statný	k2eAgInSc4d1
jehličnatý	jehličnatý	k2eAgInSc4d1
strom	strom	k1gInSc4
<g/>
,	,	kIx,
dorůstající	dorůstající	k2eAgInSc4d1
30	#num#	k4
m	m	kA
<g/>
,	,	kIx,
vzácně	vzácně	k6eAd1
až	až	k9
50	#num#	k4
m.	m.	k?
Koruna	koruna	k1gFnSc1
je	být	k5eAaImIp3nS
dosti	dosti	k6eAd1
hustě	hustě	k6eAd1
zavětvena	zavětvit	k5eAaPmNgFnS
vodorovnými	vodorovný	k2eAgFnPc7d1
<g/>
,	,	kIx,
kuželovitými	kuželovitý	k2eAgFnPc7d1
větvemi	větev	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Kmen	kmen	k1gInSc1
dosahuje	dosahovat	k5eAaImIp3nS
70	#num#	k4
<g/>
–	–	k?
<g/>
120	#num#	k4
cm	cm	kA
v	v	k7c6
průměru	průměr	k1gInSc6
<g/>
,	,	kIx,
pokrytý	pokrytý	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
šedohnědou	šedohnědý	k2eAgFnSc7d1
<g/>
,	,	kIx,
silnou	silný	k2eAgFnSc7d1
a	a	k8xC
brázditou	brázditý	k2eAgFnSc7d1
borkou	borka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druh	druh	k1gInSc1
se	se	k3xPyFc4
dožívá	dožívat	k5eAaImIp3nS
400	#num#	k4
<g/>
–	–	k?
<g/>
600	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Jehlice	jehlice	k1gFnPc1
jsou	být	k5eAaImIp3nP
tuhé	tuhý	k2eAgInPc1d1
<g/>
,	,	kIx,
18	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
mm	mm	kA
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
ostře	ostro	k6eAd1
přišpičatělé	přišpičatělý	k2eAgFnPc1d1
a	a	k8xC
bodavé	bodavý	k2eAgFnPc1d1
<g/>
,	,	kIx,
čtyřhranné	čtyřhranný	k2eAgFnPc1d1
<g/>
,	,	kIx,
zakřivené	zakřivený	k2eAgFnPc1d1
a	a	k8xC
odstávající	odstávající	k2eAgFnPc1d1
na	na	k7c4
všechny	všechen	k3xTgFnPc4
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barvu	barva	k1gFnSc4
mají	mít	k5eAaImIp3nP
matně	matně	k6eAd1
zelenou	zelený	k2eAgFnSc4d1
<g/>
,	,	kIx,
sivozelenou	sivozelený	k2eAgFnSc4d1
až	až	k8xS
stříbřitě	stříbřitě	k6eAd1
bělavou	bělavý	k2eAgFnSc4d1
<g/>
,	,	kIx,
na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
mají	mít	k5eAaImIp3nP
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
řad	řada	k1gFnPc2
průduchů	průduch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Šišky	šiška	k1gFnPc1
jsou	být	k5eAaImIp3nP
měkké	měkký	k2eAgFnPc1d1
<g/>
,	,	kIx,
válcovitě	válcovitě	k6eAd1
podlouhlé	podlouhlý	k2eAgFnPc1d1
<g/>
,	,	kIx,
v	v	k7c6
mládí	mládí	k1gNnSc6
zelené	zelený	k2eAgFnPc1d1
<g/>
,	,	kIx,
zralé	zralý	k2eAgFnPc1d1
světle	světle	k6eAd1
hnědé	hnědý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Smrk	smrk	k1gInSc1
pichlavý	pichlavý	k2eAgInSc1d1
pochází	pocházet	k5eAaImIp3nS
původně	původně	k6eAd1
z	z	k7c2
jihozápadu	jihozápad	k1gInSc2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Colorada	Colorado	k1gNnSc2
se	se	k3xPyFc4
rozšířil	rozšířit	k5eAaPmAgInS
do	do	k7c2
Nového	Nového	k2eAgNnSc2d1
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
Utahu	Utah	k1gInSc2
a	a	k8xC
Wyomingu	Wyoming	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
roste	růst	k5eAaImIp3nS
ve	v	k7c6
výškách	výška	k1gFnPc6
2000	#num#	k4
<g/>
–	–	k?
<g/>
3300	#num#	k4
m	m	kA
<g/>
,	,	kIx,
roztroušeně	roztroušeně	k6eAd1
na	na	k7c6
březích	břeh	k1gInPc6
horských	horský	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
a	a	k8xC
také	také	k9
na	na	k7c6
bažinatých	bažinatý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
existují	existovat	k5eAaImIp3nP
ještě	ještě	k6eAd1
četné	četný	k2eAgFnPc1d1
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
však	však	k9
čisté	čistý	k2eAgInPc4d1
porosty	porost	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžeme	moct	k5eAaImIp1nP
ho	on	k3xPp3gMnSc4
vidět	vidět	k5eAaImF
i	i	k9
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
od	od	k7c2
Archangelsku	Archangelsk	k1gInSc2
až	až	k9
po	po	k7c4
Krym	Krym	k1gInSc4
a	a	k8xC
Kavkaz	Kavkaz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
nezmrzá	zmrzat	k5eNaImIp3nS
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
raší	rašit	k5eAaImIp3nS
pozdě	pozdě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daří	dařit	k5eAaImIp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
jak	jak	k6eAd1
v	v	k7c6
půdách	půda	k1gFnPc6
černozemních	černozemní	k2eAgFnPc6d1
ve	v	k7c6
stepním	stepní	k2eAgInSc6d1
pásu	pás	k1gInSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
na	na	k7c6
rašelinových	rašelinový	k2eAgFnPc6d1
a	a	k8xC
zamokřených	zamokřený	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Habitus	habitus	k1gInSc1
vzrostlého	vzrostlý	k2eAgMnSc2d1
jedince	jedinec	k1gMnSc2
</s>
<s>
Pěstování	pěstování	k1gNnSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejoblíbenějších	oblíbený	k2eAgInPc2d3
sadovnicky	sadovnicky	k6eAd1
využívaných	využívaný	k2eAgInPc2d1
smrků	smrk	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
v	v	k7c6
parcích	park	k1gInPc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k8xC
malých	malý	k2eAgFnPc6d1
zahrádkách	zahrádka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěstuje	pěstovat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
mnoha	mnoho	k4c6
kultivarech	kultivar	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavně	hlavně	k6eAd1
odrůdy	odrůda	k1gFnPc4
se	s	k7c7
sivým	sivý	k2eAgNnSc7d1
nebo	nebo	k8xC
modrosivým	modrosivý	k2eAgNnSc7d1
jehličím	jehličí	k1gNnSc7
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
dekorativní	dekorativní	k2eAgFnPc1d1
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
mladém	mladý	k2eAgInSc6d1
věku	věk	k1gInSc6
<g/>
,	,	kIx,
tj.	tj.	kA
do	do	k7c2
20	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roste	růst	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
dobře	dobře	k6eAd1
i	i	k9
v	v	k7c6
znečištěném	znečištěný	k2eAgNnSc6d1
ovzduší	ovzduší	k1gNnSc6
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
a	a	k8xC
blízko	blízko	k7c2
továren	továrna	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jiné	jiný	k2eAgInPc1d1
jehličnany	jehličnan	k1gInPc1
trpí	trpět	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
pomalému	pomalý	k2eAgInSc3d1
růstu	růst	k1gInSc3
se	se	k3xPyFc4
hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
do	do	k7c2
malých	malý	k2eAgFnPc2d1
zahrádek	zahrádka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
často	často	k6eAd1
vysazován	vysazovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
náhradní	náhradní	k2eAgFnSc1d1
dřevina	dřevina	k1gFnSc1
v	v	k7c6
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgInP
lesy	les	k1gInPc1
poškozeny	poškozen	k2eAgInPc1d1
exhalacemi	exhalace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
porosty	porost	k1gInPc4
jsou	být	k5eAaImIp3nP
napadány	napadán	k2eAgInPc1d1
patogenní	patogenní	k2eAgFnSc7d1
houbou	houba	k1gFnSc7
sypavkou	sypavka	k1gFnSc7
smrku	smrk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
nevýhodou	nevýhoda	k1gFnSc7
těchto	tento	k3xDgInPc2
porostů	porost	k1gInPc2
je	být	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
spadané	spadaný	k2eAgNnSc1d1
smolnaté	smolnatý	k2eAgNnSc1d1
jehličí	jehličí	k1gNnSc1
se	se	k3xPyFc4
obtížně	obtížně	k6eAd1
rozkládá	rozkládat	k5eAaImIp3nS
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
degradaci	degradace	k1gFnSc3
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
dřevo	dřevo	k1gNnSc1
smrku	smrk	k1gInSc2
pichlavého	pichlavý	k2eAgInSc2d1
nedosahuje	dosahovat	k5eNaImIp3nS
kvality	kvalita	k1gFnPc4
dřeva	dřevo	k1gNnSc2
smrku	smrk	k1gInSc2
ztepilého	ztepilý	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Smrk	smrk	k1gInSc1
pichlavý	pichlavý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
pro	pro	k7c4
zbarvení	zbarvení	k1gNnSc4
jehlic	jehlice	k1gFnPc2
vybraných	vybraný	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
na	na	k7c6
mladých	mladý	k2eAgInPc6d1
výhonech	výhon	k1gInPc6
také	také	k9
v	v	k7c6
ČR	ČR	kA
nazýván	nazýván	k2eAgInSc1d1
„	„	k?
<g/>
smrk	smrk	k1gInSc1
stříbrný	stříbrný	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stříbrné	stříbrná	k1gFnSc6
zabarvení	zabarvení	k1gNnSc4
jehlic	jehlice	k1gFnPc2
ale	ale	k8xC
není	být	k5eNaImIp3nS
jen	jen	k9
výsadou	výsada	k1gFnSc7
pouze	pouze	k6eAd1
P.	P.	kA
pungens	pungensa	k1gFnPc2
<g/>
,	,	kIx,
„	„	k?
<g/>
stříbrné	stříbrný	k2eAgFnSc2d1
<g/>
“	“	k?
jsou	být	k5eAaImIp3nP
také	také	k9
některé	některý	k3yIgInPc1
kultivary	kultivar	k1gInPc1
Picea	Piceum	k1gNnSc2
engelmannii	engelmannie	k1gFnSc4
<g/>
,	,	kIx,
Picea	Piceus	k1gMnSc2
glauca	glaucus	k1gMnSc2
<g/>
,	,	kIx,
Picea	Piceus	k1gMnSc2
mariana	marian	k1gMnSc2
i	i	k8xC
dalších	další	k2eAgMnPc2d1
smrků	smrk	k1gInPc2
a	a	k8xC
také	také	k9
douglasky	douglaska	k1gFnPc1
Pseudotsuga	Pseudotsug	k1gMnSc2
menziesii	menziesie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Je	být	k5eAaImIp3nS
státním	státní	k2eAgInSc7d1
stromem	strom	k1gInSc7
Utahu	Utah	k1gInSc2
a	a	k8xC
Colorada	Colorado	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PILÁT	Pilát	k1gMnSc1
<g/>
,	,	kIx,
Albert	Albert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehličnaté	jehličnatý	k2eAgInPc1d1
stromy	strom	k1gInPc1
a	a	k8xC
keře	keř	k1gInPc1
našich	náš	k3xOp1gFnPc2
zahrad	zahrada	k1gFnPc2
a	a	k8xC
parků	park	k1gInPc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
.	.	kIx.
508	#num#	k4
s.	s.	k?
</s>
<s>
KREMER	KREMER	kA
<g/>
,	,	kIx,
Bruno	Bruno	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stromy	strom	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
k.s	k.s	k?
<g/>
,	,	kIx,
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
nakladatelstvím	nakladatelství	k1gNnSc7
Ikar	Ikara	k1gFnPc2
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
287	#num#	k4
s.	s.	k?
</s>
<s>
MUSIL	Musil	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
;	;	kIx,
HAMERNÍK	hamerník	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehličnaté	jehličnatý	k2eAgFnPc4d1
dřeviny	dřevina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1567	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
73	#num#	k4
<g/>
-	-	kIx~
<g/>
76	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
smrk	smrk	k1gInSc1
pichlavý	pichlavý	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
smrk	smrk	k1gInSc1
pichlavý	pichlavý	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Picea	Pice	k1gInSc2
pungens	pungens	k6eAd1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rod	rod	k1gInSc1
smrk	smrk	k1gInSc1
(	(	kIx(
<g/>
Picea	Picea	k1gFnSc1
<g/>
)	)	kIx)
Evropa	Evropa	k1gFnSc1
</s>
<s>
smrk	smrk	k1gInSc1
ztepilý	ztepilý	k2eAgInSc1d1
•	•	k?
smrk	smrk	k1gInSc1
omorika	omorika	k1gFnSc1
Asie	Asie	k1gFnSc1
</s>
<s>
smrk	smrk	k1gInSc4
ajanský	ajanský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Glehnův	Glehnův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
dvoubarvý	dvoubarvý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
chupejský	chupejský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Koyamův	Koyamův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
korejský	korejský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
liťiangský	liťiangský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
lesklý	lesklý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Maximovičův	Maximovičův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Meyerův	Meyerův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
nachový	nachový	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Schrenkův	Schrenkův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
sibiřský	sibiřský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
štětinatý	štětinatý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
východní	východní	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc1
Wilsonův	Wilsonův	k2eAgInSc1d1
•	•	k?
Picea	Picea	k1gFnSc1
brachytyla	brachytýt	k5eAaPmAgFnS
•	•	k?
Picea	Pice	k2eAgFnSc1d1
crassifolia	crassifolia	k1gFnSc1
•	•	k?
Picea	Picea	k1gFnSc1
farreri	farrer	k1gFnSc2
•	•	k?
Picea	Pice	k2eAgFnSc1d1
morrisonicola	morrisonicola	k1gFnSc1
•	•	k?
Picea	Pice	k2eAgFnSc1d1
spinulosa	spinulosa	k1gFnSc1
•	•	k?
Picea	Picea	k1gFnSc1
smithiana	smithiana	k1gFnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
smrk	smrk	k1gInSc4
Brewerův	Brewerův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Engelmannův	Engelmannův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
černý	černý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
červený	červený	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
pichlavý	pichlavý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
sivý	sivý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
sitka	sitek	k1gMnSc2
•	•	k?
Picea	Piceus	k1gMnSc2
chihuahuana	chihuahuan	k1gMnSc2
•	•	k?
Picea	Picea	k1gMnSc1
martinezii	martinezie	k1gFnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
|	|	kIx~
Stromy	strom	k1gInPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4435789-8	4435789-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2006003883	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2006003883	#num#	k4
</s>
