<s>
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
</s>
<s>
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
I.	I.	kA
<g/>
)	)	kIx)
</s>
<s>
z	z	k7c2
Boží	boží	k2eAgFnSc2d1
milosti	milost	k1gFnSc2
zvolený	zvolený	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
národa	národ	k1gInSc2
německého	německý	k2eAgInSc2d1
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
rozmnožitel	rozmnožitel	k1gMnSc1
<g/>
,	,	kIx,
dědičný	dědičný	k2eAgMnSc1d1
císař	císař	k1gMnSc1
rakouský	rakouský	k2eAgMnSc1d1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
a	a	k8xC
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
etc	etc	k?
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
na	na	k7c6
obraze	obraz	k1gInSc6
Friedricha	Friedrich	k1gMnSc2
von	von	k1gInSc1
Amerling	Amerling	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1832	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1792	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1806	#num#	k4
<g/>
(	(	kIx(
<g/>
zvolený	zvolený	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
)	)	kIx)
<g/>
11	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1804	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1835	#num#	k4
<g/>
(	(	kIx(
<g/>
dědičný	dědičný	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1792	#num#	k4
(	(	kIx(
<g/>
českým	český	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
Úplné	úplný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
</s>
<s>
Tituly	titul	k1gInPc1
</s>
<s>
apoštolský	apoštolský	k2eAgMnSc1d1
král	král	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
milánský	milánský	k2eAgMnSc1d1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
lucemburský	lucemburský	k2eAgMnSc1d1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1768	#num#	k4
</s>
<s>
Florencie	Florencie	k1gFnSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1835	#num#	k4
(	(	kIx(
<g/>
67	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
Císařská	císařský	k2eAgFnSc1d1
hrobka	hrobka	k1gFnSc1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
I.	I.	kA
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Ludovika	Ludovika	k1gFnSc1
Modenská	modenský	k2eAgFnSc1d1
</s>
<s>
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karolína	Karolína	k1gFnSc1
Augusta	August	k1gMnSc2
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Marie	Marie	k1gFnSc1
LuisaFerdinandMarie	LuisaFerdinandMarie	k1gFnSc1
LeopoldaMarie	LeopoldaMarie	k1gFnSc1
KlementinaKarolína	KlementinaKarolína	k1gFnSc1
FerdinandaFrantišek	FerdinandaFrantišek	k1gInSc4
KarelMarie	KarelMarie	k1gFnSc2
Anna	Anna	k1gFnSc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Ludovika	Ludovikum	k1gNnSc2
Španělská	španělský	k2eAgFnSc1d1
</s>
<s>
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1768	#num#	k4
Florencie	Florencie	k1gFnSc2
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1835	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1792	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
král	král	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
a	a	k8xC
český	český	k2eAgMnSc1d1
a	a	k8xC
markrabě	markrabě	k1gMnSc1
moravský	moravský	k2eAgMnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1804	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
císař	císař	k1gMnSc1
rakouský	rakouský	k2eAgMnSc1d1
a	a	k8xC
jako	jako	k9
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
letech	let	k1gInPc6
1792	#num#	k4
<g/>
–	–	k?
<g/>
1806	#num#	k4
poslední	poslední	k2eAgFnSc4d1
císař	císař	k1gMnSc1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
národa	národ	k1gInSc2
německého	německý	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celá	celý	k2eAgFnSc1d1
jeho	jeho	k3xOp3gFnSc1
vláda	vláda	k1gFnSc1
byla	být	k5eAaImAgFnS
poznamenána	poznamenat	k5eAaPmNgFnS
dvěma	dva	k4xCgFnPc7
muži	muž	k1gMnSc6
–	–	k?
Klemensem	Klemens	k1gMnSc7
Václavem	Václav	k1gMnSc7
Lotharem	Lothar	k1gMnSc7
Metternichem	Metternich	k1gMnSc7
a	a	k8xC
Napoleonem	Napoleon	k1gMnSc7
Bonapartem	bonapart	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1768	#num#	k4
jako	jako	k8xC,k8xS
prvorozený	prvorozený	k2eAgMnSc1d1
syn	syn	k1gMnSc1
toskánského	toskánský	k2eAgMnSc2d1
velkovévody	velkovévoda	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Marie	Maria	k1gFnSc2
Ludoviky	Ludovika	k1gFnSc2
Španělské	španělský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdo	nikdo	k3yNnSc1
tenkrát	tenkrát	k6eAd1
nemohl	moct	k5eNaImAgMnS
tušit	tušit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
František	František	k1gMnSc1
jednou	jednou	k6eAd1
stane	stanout	k5eAaPmIp3nS
posledním	poslední	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgMnPc4d1
a	a	k8xC
zároveň	zároveň	k6eAd1
prvním	první	k4xOgMnSc7
císařem	císař	k1gMnSc7
rakouským	rakouský	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mládí	mládí	k1gNnSc1
prožil	prožít	k5eAaPmAgMnS
na	na	k7c6
dvoře	dvůr	k1gInSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
spolu	spolu	k6eAd1
s	s	k7c7
mnoha	mnoho	k4c7
svými	svůj	k3xOyFgMnPc7
sourozenci	sourozenec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1780	#num#	k4
zemřela	zemřít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
babička	babička	k1gFnSc1
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
a	a	k8xC
uherská	uherský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
jejím	její	k3xOp3gMnSc7
následníkem	následník	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
pokrokový	pokrokový	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
však	však	k9
zůstal	zůstat	k5eAaPmAgMnS
i	i	k9
po	po	k7c6
dvou	dva	k4xCgNnPc6
manželstvích	manželství	k1gNnPc6
bezdětný	bezdětný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svého	svůj	k1gMnSc4
synovce	synovec	k1gMnSc4
Františka	František	k1gMnSc4
si	se	k3xPyFc3
oblíbil	oblíbit	k5eAaPmAgMnS
a	a	k8xC
začal	začít	k5eAaPmAgMnS
s	s	k7c7
ním	on	k3xPp3gInSc7
počítat	počítat	k5eAaImF
jako	jako	k8xS,k8xC
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
následníkem	následník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Našel	najít	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
také	také	k6eAd1
vhodnou	vhodný	k2eAgFnSc4d1
manželku	manželka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1788	#num#	k4
se	se	k3xPyFc4
jí	jíst	k5eAaImIp3nS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
stala	stát	k5eAaPmAgFnS
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1767	#num#	k4
<g/>
–	–	k?
<g/>
1790	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Druhé	druhý	k4xOgNnSc1
manželství	manželství	k1gNnSc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
však	však	k9
zemřela	zemřít	k5eAaPmAgFnS
již	již	k6eAd1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
svatbě	svatba	k1gFnSc6
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
roku	rok	k1gInSc2
1790	#num#	k4
<g/>
,	,	kIx,
den	den	k1gInSc4
po	po	k7c6
porodu	porod	k1gInSc6
prvorozené	prvorozený	k2eAgFnSc2d1
dcery	dcera	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
nato	nato	k6eAd1
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
také	také	k9
císař	císař	k1gMnSc1
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
potěchou	potěcha	k1gFnSc7
jehož	jehož	k3xOyRp3gInPc2
posledních	poslední	k2eAgInPc2d1
dní	den	k1gInPc2
Alžběta	Alžběta	k1gFnSc1
byla	být	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
strýcově	strýcův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
řídil	řídit	k5eAaImAgMnS
František	František	k1gMnSc1
státní	státní	k2eAgFnSc2d1
záležitosti	záležitost	k1gFnSc2
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
z	z	k7c2
Florencie	Florencie	k1gFnSc2
přijel	přijet	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
bratrovi	bratr	k1gMnSc6
právoplatným	právoplatný	k2eAgMnSc7d1
následníkem	následník	k1gMnSc7
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1791	#num#	k4
se	se	k3xPyFc4
František	František	k1gMnSc1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
oženil	oženit	k5eAaPmAgMnS
podruhé	podruhé	k6eAd1
(	(	kIx(
<g/>
sňatek	sňatek	k1gInSc1
byl	být	k5eAaImAgInS
nejprve	nejprve	k6eAd1
uzavřen	uzavřít	k5eAaPmNgInS
per	pero	k1gNnPc2
procurationem	procuration	k1gInSc7
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1790	#num#	k4
v	v	k7c6
Neapoli	Neapol	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
Marií	Maria	k1gFnSc7
Terezou	Tereza	k1gFnSc7
Neapolsko-Sicilskou	neapolsko-sicilský	k2eAgFnSc7d1
(	(	kIx(
<g/>
1772	#num#	k4
<g/>
–	–	k?
<g/>
1807	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Manželé	manžel	k1gMnPc1
byli	být	k5eAaImAgMnP
bratranec	bratranec	k1gMnSc1
a	a	k8xC
sestřenice	sestřenice	k1gFnSc1
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc4
rodiče	rodič	k1gMnPc1
byli	být	k5eAaImAgMnP
vlastními	vlastní	k2eAgMnPc7d1
sourozenci	sourozenec	k1gMnPc7
–	–	k?
Mariina	Mariin	k2eAgFnSc1d1
matka	matka	k1gFnSc1
Marie	Marie	k1gFnSc1
Karolína	Karolína	k1gFnSc1
byla	být	k5eAaImAgFnS
Leopoldovou	Leopoldův	k2eAgFnSc7d1
sestrou	sestra	k1gFnSc7
a	a	k8xC
nevěstin	nevěstin	k2eAgMnSc1d1
otec	otec	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgInSc1d1
byl	být	k5eAaImAgMnS
bratrem	bratr	k1gMnSc7
Františkovy	Františkův	k2eAgFnSc2d1
matky	matka	k1gFnSc2
Marie	Maria	k1gFnSc2
Ludoviky	Ludovika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
blízké	blízký	k2eAgNnSc1d1
příbuzenství	příbuzenství	k1gNnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
většina	většina	k1gFnSc1
z	z	k7c2
Františkových	Františkův	k2eAgFnPc2d1
a	a	k8xC
Mariiných	Mariin	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
potýkat	potýkat	k5eAaImF
s	s	k7c7
genetickou	genetický	k2eAgFnSc7d1
degenerací	degenerace	k1gFnSc7
a	a	k8xC
jen	jen	k9
část	část	k1gFnSc4
z	z	k7c2
nich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
schopna	schopen	k2eAgFnSc1d1
samostatného	samostatný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Císař	Císař	k1gMnSc1
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
I.	I.	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1792	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
na	na	k7c4
trůn	trůn	k1gInSc4
nastoupil	nastoupit	k5eAaPmAgMnS
František	František	k1gMnSc1
jako	jako	k8xC,k8xS
císař	císař	k1gMnSc1
František	František	k1gMnSc1
II	II	kA
<g/>
..	..	k?
Byl	být	k5eAaImAgInS
rodinný	rodinný	k2eAgInSc4d1
typ	typ	k1gInSc4
a	a	k8xC
připomínal	připomínat	k5eAaImAgMnS
vzhledem	vzhled	k1gInSc7
a	a	k8xC
oblečením	oblečení	k1gNnSc7
spíše	spíše	k9
obyčejného	obyčejný	k2eAgMnSc2d1
měšťana	měšťan	k1gMnSc2
než	než	k8xS
císaře	císař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydával	vydávat	k5eAaImAgMnS,k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
procházky	procházka	k1gFnPc4
městem	město	k1gNnSc7
a	a	k8xC
rád	rád	k6eAd1
odpovídal	odpovídat	k5eAaImAgMnS
lidem	člověk	k1gMnPc3
na	na	k7c4
pozdrav	pozdrav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1792	#num#	k4
byl	být	k5eAaImAgInS
korunován	korunovat	k5eAaBmNgInS
na	na	k7c4
uherského	uherský	k2eAgMnSc4d1
krále	král	k1gMnSc4
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1792	#num#	k4
na	na	k7c4
krále	král	k1gMnSc4
českého	český	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
korunovaci	korunovace	k1gFnSc6
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
pozdějším	pozdní	k2eAgMnSc7d2
„	„	k?
<g/>
nejmocnějším	mocný	k2eAgMnSc7d3
mužem	muž	k1gMnSc7
<g/>
“	“	k?
Rakouska	Rakousko	k1gNnSc2
Klementem	Klement	k1gMnSc7
Václavem	Václav	k1gMnSc7
Lotharem	Lothar	k1gMnSc7
Metternichem	Metternich	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
vynikající	vynikající	k2eAgMnSc1d1
diplomat	diplomat	k1gMnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gFnSc7
pravou	pravý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Konflikt	konflikt	k1gInSc1
s	s	k7c7
Napoleonem	Napoleon	k1gMnSc7
Bonapartem	bonapart	k1gInSc7
</s>
<s>
Podobizna	podobizna	k1gFnSc1
Františka	Františka	k1gFnSc1
I.	I.	kA
od	od	k7c2
uměleckého	umělecký	k2eAgMnSc2d1
rytce	rytec	k1gMnSc2
Josefa	Josef	k1gMnSc2
AxmannaSetkání	AxmannaSetkání	k1gNnSc2
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
a	a	k8xC
Napoleona	Napoleon	k1gMnSc2
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
SlavkovaNad	SlavkovaNad	k1gInSc1
Evropou	Evropa	k1gFnSc7
se	se	k3xPyFc4
však	však	k9
začala	začít	k5eAaPmAgFnS
stahovat	stahovat	k5eAaImF
mračna	mračna	k1gFnSc1
<g/>
;	;	kIx,
tím	ten	k3xDgNnSc7
mračnem	mračno	k1gNnSc7
nebyl	být	k5eNaImAgInS
nikdo	nikdo	k3yNnSc1
jiný	jiný	k2eAgMnSc1d1
než	než	k8xS
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzské	francouzský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
pod	pod	k7c7
Napoleonovým	Napoleonův	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
několikrát	několikrát	k6eAd1
proniklo	proniknout	k5eAaPmAgNnS
na	na	k7c4
rakouská	rakouský	k2eAgNnPc4d1
území	území	k1gNnSc4
a	a	k8xC
vyhnalo	vyhnat	k5eAaPmAgNnS
tak	tak	k6eAd1
císařskou	císařský	k2eAgFnSc4d1
rodinu	rodina	k1gFnSc4
do	do	k7c2
Uher	Uhry	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
Rakušané	Rakušan	k1gMnPc1
nebyli	být	k5eNaImAgMnP
ani	ani	k8xC
tak	tak	k6eAd1
dobře	dobře	k6eAd1
vycvičení	vycvičený	k2eAgMnPc1d1
ani	ani	k8xC
vyzbrojení	vyzbrojený	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
Francouzi	Francouz	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1804	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
přijetí	přijetí	k1gNnSc4
císařského	císařský	k2eAgInSc2d1
titulu	titul	k1gInSc2
Napoleonem	Napoleon	k1gMnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
František	František	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
císařem	císař	k1gMnSc7
rakouským	rakouský	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
uzavřel	uzavřít	k5eAaPmAgMnS
dohodu	dohoda	k1gFnSc4
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
čele	čelo	k1gNnSc6
stál	stát	k5eAaImAgMnS
car	car	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
a	a	k8xC
znovu	znovu	k6eAd1
se	se	k3xPyFc4
utkal	utkat	k5eAaPmAgMnS
s	s	k7c7
Napoleonem	napoleon	k1gInSc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
„	„	k?
<g/>
Tří	tři	k4xCgFnPc2
císařů	císař	k1gMnPc2
<g/>
“	“	k?
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jej	on	k3xPp3gInSc4
ale	ale	k9
Napoleon	Napoleon	k1gMnSc1
rozdrtil	rozdrtit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
politický	politický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
zejména	zejména	k9
vznik	vznik	k1gInSc4
Rýnského	rýnský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
František	František	k1gMnSc1
vzdal	vzdát	k5eAaPmAgMnS
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1806	#num#	k4
koruny	koruna	k1gFnPc4
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgNnSc1d1
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
římskou	římský	k2eAgFnSc4d1
císařskou	císařský	k2eAgFnSc4d1
hodnost	hodnost	k1gFnSc4
za	za	k7c4
zrušenou	zrušený	k2eAgFnSc4d1
a	a	k8xC
zbavil	zbavit	k5eAaPmAgInS
zbylé	zbylý	k2eAgInPc4d1
říšské	říšský	k2eAgInPc4d1
stavy	stav	k1gInPc4
povinností	povinnost	k1gFnPc2
vůči	vůči	k7c3
sobě	se	k3xPyFc3
jako	jako	k8xC,k8xS
římskému	římský	k2eAgMnSc3d1
panovníkovi	panovník	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k9
formálně	formálně	k6eAd1
omezila	omezit	k5eAaPmAgFnS
pouze	pouze	k6eAd1
na	na	k7c4
rodové	rodový	k2eAgFnPc4d1
državy	država	k1gFnPc4
alpské	alpský	k2eAgFnPc4d1
<g/>
,	,	kIx,
českou	český	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
a	a	k8xC
uherskou	uherský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
a	a	k8xC
František	František	k1gMnSc1
tak	tak	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
jako	jako	k9
František	František	k1gMnSc1
I.	I.	kA
pouze	pouze	k6eAd1
rakouským	rakouský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Marie	Marie	k1gFnSc1
Ludovika	Ludovika	k1gFnSc1
</s>
<s>
Většina	většina	k1gFnSc1
Františkových	Františkův	k2eAgMnPc2d1
příbuzných	příbuzný	k1gMnPc2
byla	být	k5eAaImAgFnS
Napoleonem	napoleon	k1gInSc7
vyhnána	vyhnán	k2eAgFnSc1d1
ze	z	k7c2
svých	svůj	k3xOyFgNnPc2
království	království	k1gNnPc2
a	a	k8xC
vévodství	vévodství	k1gNnSc2
a	a	k8xC
hledala	hledat	k5eAaImAgFnS
útočiště	útočiště	k1gNnSc4
u	u	k7c2
svého	svůj	k3xOyFgMnSc4
příbuzného	příbuzný	k1gMnSc4
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
i	i	k9
rodina	rodina	k1gFnSc1
budoucí	budoucí	k2eAgFnSc2d1
císařovny	císařovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1808	#num#	k4
<g/>
,	,	kIx,
půl	půl	k1xP
roku	rok	k1gInSc2
po	po	k7c6
smrti	smrt	k1gFnSc6
Františkovy	Františkův	k2eAgFnSc2d1
druhé	druhý	k4xOgFnSc2
manželky	manželka	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
konala	konat	k5eAaImAgFnS
svatba	svatba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volba	volba	k1gFnSc1
padla	padnout	k5eAaImAgFnS,k5eAaPmAgFnS
opět	opět	k6eAd1
na	na	k7c4
sestřenici	sestřenice	k1gFnSc4
<g/>
,	,	kIx,
princeznu	princezna	k1gFnSc4
Marii	Maria	k1gFnSc4
Ludoviku	Ludovik	k1gInSc2
Beatrix	Beatrix	k1gInSc1
z	z	k7c2
Modeny	Modena	k1gFnSc2
(	(	kIx(
<g/>
1787	#num#	k4
<g/>
–	–	k?
<g/>
1816	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Marie	Marie	k1gFnSc1
Ludovika	Ludovika	k1gFnSc1
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
vzdělaná	vzdělaný	k2eAgFnSc1d1
žena	žena	k1gFnSc1
<g/>
,	,	kIx,
trpěla	trpět	k5eAaImAgFnS
však	však	k9
tuberkulózou	tuberkulóza	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
jí	on	k3xPp3gFnSc3
znemožňovala	znemožňovat	k5eAaImAgFnS
mít	mít	k5eAaImF
děti	dítě	k1gFnPc4
a	a	k8xC
zapojit	zapojit	k5eAaPmF
se	se	k3xPyFc4
plnohodnotně	plnohodnotně	k6eAd1
do	do	k7c2
života	život	k1gInSc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
o	o	k7c4
to	ten	k3xDgNnSc4
až	až	k9
do	do	k7c2
smrti	smrt	k1gFnSc2
snažila	snažit	k5eAaImAgFnS
a	a	k8xC
pokud	pokud	k8xS
mohla	moct	k5eAaImAgFnS
<g/>
,	,	kIx,
všude	všude	k6eAd1
svého	svůj	k1gMnSc2
manžela	manžel	k1gMnSc2
doprovázela	doprovázet	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohlížela	dohlížet	k5eAaImAgFnS
na	na	k7c4
výchovu	výchova	k1gFnSc4
všech	všecek	k3xTgFnPc2
svých	svůj	k3xOyFgFnPc2
vyvdaných	vyvdaný	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
a	a	k8xC
zvláště	zvláště	k6eAd1
se	se	k3xPyFc4
věnovala	věnovat	k5eAaPmAgFnS,k5eAaImAgFnS
následníku	následník	k1gMnSc3
trůnu	trůn	k1gInSc2
Ferdinandovi	Ferdinand	k1gMnSc3
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
stav	stav	k1gInSc4
se	se	k3xPyFc4
díky	díky	k7c3
ní	on	k3xPp3gFnSc3
velmi	velmi	k6eAd1
zlepšil	zlepšit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
neochvějnou	neochvějný	k2eAgFnSc7d1
nepřítelkyní	nepřítelkyně	k1gFnSc7
Napoleona	Napoleon	k1gMnSc2
a	a	k8xC
postavila	postavit	k5eAaPmAgFnS
se	se	k3xPyFc4
proti	proti	k7c3
plánu	plán	k1gInSc3
provdat	provdat	k5eAaPmF
Františkovu	Františkův	k2eAgFnSc4d1
dceru	dcera	k1gFnSc4
Marii	Maria	k1gFnSc4
Luisu	Luisa	k1gFnSc4
za	za	k7c2
císaře	císař	k1gMnSc2
Francouzů	Francouz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
když	když	k8xS
svého	svůj	k3xOyFgMnSc2
manžela	manžel	k1gMnSc2
dokázala	dokázat	k5eAaPmAgFnS
velmi	velmi	k6eAd1
ovlivnit	ovlivnit	k5eAaPmF
<g/>
,	,	kIx,
přesto	přesto	k8xC
byla	být	k5eAaImAgNnP
sňatková	sňatkový	k2eAgNnPc1d1
jednání	jednání	k1gNnPc1
mezi	mezi	k7c7
Napoleonem	napoleon	k1gInSc7
a	a	k8xC
Metternichem	Metternich	k1gInSc7
úspěšně	úspěšně	k6eAd1
dovedena	dovést	k5eAaPmNgFnS
do	do	k7c2
konce	konec	k1gInSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1810	#num#	k4
se	se	k3xPyFc4
Marie	Marie	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
stala	stát	k5eAaPmAgFnS
francouzskou	francouzský	k2eAgFnSc7d1
císařovnou	císařovna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metternich	Metternich	k1gInSc1
uskutečnil	uskutečnit	k5eAaPmAgInS
ještě	ještě	k9
jeden	jeden	k4xCgInSc1
sňatkový	sňatkový	k2eAgInSc1d1
plán	plán	k1gInSc1
s	s	k7c7
nešťastným	šťastný	k2eNgInSc7d1
koncem	konec	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
manželství	manželství	k1gNnSc1
další	další	k2eAgFnSc2d1
Františkovy	Františkův	k2eAgFnSc2d1
dcery	dcera	k1gFnSc2
Leopoldiny	Leopoldina	k1gFnSc2
s	s	k7c7
brazilským	brazilský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Pedrem	Pedr	k1gMnSc7
I.	I.	kA
</s>
<s>
Napoleonův	Napoleonův	k2eAgInSc1d1
konec	konec	k1gInSc1
a	a	k8xC
císařova	císařův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
</s>
<s>
Kníže	kníže	k1gMnSc1
Schwarzenberg	Schwarzenberg	k1gMnSc1
oznamuje	oznamovat	k5eAaImIp3nS
vládcům	vládce	k1gMnPc3
Rakouska	Rakousko	k1gNnSc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Pruska	Prusko	k1gNnSc2
vítězství	vítězství	k1gNnSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Lipska	Lipsko	k1gNnSc2
</s>
<s>
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
po	po	k7c6
císařské	císařský	k2eAgFnSc6d1
korunovaci	korunovace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
Po	po	k7c6
Bitvě	bitva	k1gFnSc6
národů	národ	k1gInPc2
u	u	k7c2
Lipska	Lipsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1813	#num#	k4
byl	být	k5eAaImAgMnS
Napoleon	Napoleon	k1gMnSc1
poražen	porazit	k5eAaPmNgMnS
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
žena	žena	k1gFnSc1
Marie	Marie	k1gFnSc1
Luisa	Luisa	k1gFnSc1
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
i	i	k9
se	s	k7c7
synem	syn	k1gMnSc7
<g/>
,	,	kIx,
římským	římský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Napoleonem	Napoleon	k1gMnSc7
Františkem	František	k1gMnSc7
Bonapartem	bonapart	k1gInSc7
(	(	kIx(
<g/>
pozdější	pozdní	k2eAgMnSc1d2
vévoda	vévoda	k1gMnSc1
František	František	k1gMnSc1
Zákupský	Zákupský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Vídeňském	vídeňský	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1814	#num#	k4
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1815	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
byl	být	k5eAaImAgInS
Metternichovým	Metternichův	k2eAgInSc7d1
triumfem	triumf	k1gInSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
Evropa	Evropa	k1gFnSc1
vrátila	vrátit	k5eAaPmAgFnS
do	do	k7c2
starých	starý	k2eAgFnPc2d1
kolejí	kolej	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Luisa	Luisa	k1gFnSc1
byla	být	k5eAaImAgFnS
poslána	poslat	k5eAaPmNgFnS
do	do	k7c2
Parmy	Parma	k1gFnSc2
jako	jako	k8xS,k8xC
vévodkyně	vévodkyně	k1gFnSc2
a	a	k8xC
malý	malý	k2eAgInSc1d1
Bonaparte	bonapart	k1gInSc5
zůstal	zůstat	k5eAaPmAgMnS
s	s	k7c7
dědečkem	dědeček	k1gMnSc7
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1816	#num#	k4
se	se	k3xPyFc4
František	František	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
naposledy	naposledy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
nastávající	nastávající	k1gFnSc4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
dcera	dcera	k1gFnSc1
bavorského	bavorský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Maxmiliána	Maxmilián	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
princezna	princezna	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Augusta	August	k1gMnSc2
Bavorská	bavorský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
–	–	k?
<g/>
1873	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karolína	Karolína	k1gFnSc1
byla	být	k5eAaImAgFnS
už	už	k6eAd1
jednou	jeden	k4xCgFnSc7
<g/>
,	,	kIx,
díky	díky	k7c3
Napoleonovi	Napoleon	k1gMnSc3
<g/>
,	,	kIx,
provdaná	provdaný	k2eAgFnSc1d1
za	za	k7c4
Viléma	Vilém	k1gMnSc4
I.	I.	kA
Württemberského	Württemberský	k2eAgInSc2d1
(	(	kIx(
<g/>
1781	#num#	k4
<g/>
–	–	k?
<g/>
1864	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
manželství	manželství	k1gNnSc1
však	však	k9
bylo	být	k5eAaImAgNnS
bezdětné	bezdětný	k2eAgNnSc1d1
<g/>
;	;	kIx,
z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
bylo	být	k5eAaImAgNnS
po	po	k7c6
Napoleonově	Napoleonův	k2eAgInSc6d1
pádu	pád	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1815	#num#	k4
rozvedeno	rozvést	k5eAaPmNgNnS
(	(	kIx(
<g/>
hlavním	hlavní	k2eAgInSc7d1
impulsem	impuls	k1gInSc7
však	však	k9
bylo	být	k5eAaImAgNnS
setkání	setkání	k1gNnSc1
Viléma	Vilém	k1gMnSc2
se	s	k7c7
sestrou	sestra	k1gFnSc7
ruského	ruský	k2eAgMnSc2d1
cara	car	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
I.	I.	kA
Kateřinou	Kateřina	k1gFnSc7
Pavlovnou	Pavlovna	k1gFnSc7
<g/>
,	,	kIx,
do	do	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
Vilém	Vilém	k1gMnSc1
hluboce	hluboko	k6eAd1
zamiloval	zamilovat	k5eAaPmAgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
s	s	k7c7
Františkem	františek	k1gInSc7
bylo	být	k5eAaImAgNnS
šťastné	šťastný	k2eAgNnSc1d1
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
ani	ani	k8xC
v	v	k7c6
něm	on	k3xPp3gInSc6
Karolína	Karolína	k1gFnSc1
neporodila	porodit	k5eNaPmAgFnS
žádné	žádný	k3yNgFnPc4
děti	dítě	k1gFnPc4
<g/>
;	;	kIx,
láskyplně	láskyplně	k6eAd1
se	se	k3xPyFc4
starala	starat	k5eAaImAgFnS
o	o	k7c4
všechny	všechen	k3xTgFnPc4
své	svůj	k3xOyFgFnPc4
děti	dítě	k1gFnPc4
nevlastní	vlastnit	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
Karolínou	Karolína	k1gFnSc7
podporovaný	podporovaný	k2eAgInSc1d1
sňatek	sňatek	k1gInSc1
Františkova	Františkův	k2eAgMnSc2d1
syna	syn	k1gMnSc2
Františka	František	k1gMnSc2
Karla	Karel	k1gMnSc2
s	s	k7c7
její	její	k3xOp3gFnSc7
polorodou	polorodý	k2eAgFnSc7d1
sestrou	sestra	k1gFnSc7
Žofií	Žofie	k1gFnPc2
Bavorskou	bavorský	k2eAgFnSc4d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
si	se	k3xPyFc3
dobře	dobře	k6eAd1
rozuměla	rozumět	k5eAaImAgFnS
<g/>
,	,	kIx,
a	a	k8xC
svou	svůj	k3xOyFgFnSc4
náklonnost	náklonnost	k1gFnSc4
později	pozdě	k6eAd2
přenesla	přenést	k5eAaPmAgFnS
i	i	k9
na	na	k7c4
Žofiina	Žofiin	k2eAgMnSc4d1
syna	syn	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
manželku	manželka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
šest	šest	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
se	se	k3xPyFc4
pak	pak	k6eAd1
císař	císař	k1gMnSc1
dočkal	dočkat	k5eAaPmAgMnS
prvního	první	k4xOgMnSc4
vnuka	vnuk	k1gMnSc4
Františka	František	k1gMnSc4
<g/>
,	,	kIx,
pozdějšího	pozdní	k2eAgMnSc4d2
císaře	císař	k1gMnSc4
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc4
I.	I.	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
o	o	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
přišel	přijít	k5eAaPmAgInS
s	s	k7c7
dědečkem	dědeček	k1gMnSc7
rozloučit	rozloučit	k5eAaPmF
k	k	k7c3
úmrtnímu	úmrtní	k2eAgNnSc3d1
loži	lože	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
trůn	trůn	k1gInSc4
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yQgMnSc3,k3yRgMnSc3
kladl	klást	k5eAaImAgInS
na	na	k7c6
srdce	srdce	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
poslouchal	poslouchat	k5eAaImAgMnS
Metternicha	Metternich	k1gMnSc4
a	a	k8xC
nedělal	dělat	k5eNaImAgMnS
nic	nic	k3yNnSc1
bez	bez	k7c2
porady	porada	k1gFnSc2
s	s	k7c7
ním	on	k3xPp3gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
manželka	manželka	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Augusta	August	k1gMnSc2
přesídlila	přesídlit	k5eAaPmAgFnS
do	do	k7c2
Salcburku	Salcburk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Držte	držet	k5eAaImRp2nP
se	se	k3xPyFc4
starého	starý	k2eAgInSc2d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
dobré	dobrý	k2eAgNnSc1d1
a	a	k8xC
naši	náš	k3xOp1gMnPc1
předkové	předek	k1gMnPc1
se	se	k3xPyFc4
při	při	k7c6
tom	ten	k3xDgNnSc6
dobře	dobře	k6eAd1
měli	mít	k5eAaImAgMnP
<g/>
;	;	kIx,
proč	proč	k6eAd1
ne	ne	k9
my	my	k3xPp1nPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
panují	panovat	k5eAaImIp3nP
nové	nový	k2eAgFnPc1d1
ideje	idea	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
nemohu	moct	k5eNaImIp1nS
schvalovat	schvalovat	k5eAaImF
a	a	k8xC
nikdy	nikdy	k6eAd1
jich	on	k3xPp3gMnPc2
neschválím	schválit	k5eNaPmIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdalujte	vzdalovat	k5eAaImRp2nP
se	se	k3xPyFc4
jich	on	k3xPp3gInPc2
a	a	k8xC
přidržujte	přidržovat	k5eAaImRp2nP
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInSc2
opaku	opak	k1gInSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
já	já	k3xPp1nSc1
nepotřebuji	potřebovat	k5eNaImIp1nS
učence	učenec	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
dobré	dobrý	k2eAgMnPc4d1
občany	občan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vaším	váš	k3xOp2gNnSc7
povoláním	povolání	k1gNnSc7
je	být	k5eAaImIp3nS
vzdělávat	vzdělávat	k5eAaImF
mládež	mládež	k1gFnSc4
k	k	k7c3
tomu	ten	k3xDgInSc3
účelu	účel	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
mně	já	k3xPp1nSc3
slouží	sloužit	k5eAaImIp3nS
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
učit	učit	k5eAaImF
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
nařizuji	nařizovat	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
to	ten	k3xDgNnSc1
neumí	umět	k5eNaImIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
přichází	přicházet	k5eAaImIp3nS
ke	k	k7c3
mně	já	k3xPp1nSc3
s	s	k7c7
novými	nový	k2eAgFnPc7d1
ideami	idea	k1gFnPc7
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
může	moct	k5eAaImIp3nS
jít	jít	k5eAaImF
<g/>
,	,	kIx,
nebo	nebo	k8xC
já	já	k3xPp1nSc1
jej	on	k3xPp3gMnSc4
odstraním	odstranit	k5eAaPmIp1nS
<g/>
.	.	kIx.
</s>
<s>
—	—	k?
<g/>
z	z	k7c2
proslovu	proslov	k1gInSc2
Františka	František	k1gMnSc2
I.	I.	kA
k	k	k7c3
profesorům	profesor	k1gMnPc3
lycea	lyceum	k1gNnSc2
v	v	k7c6
Lublani	Lublaň	k1gFnSc6
<g/>
,	,	kIx,
1821	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pomník	pomník	k1gInSc1
(	(	kIx(
<g/>
Krannerova	Krannerův	k2eAgFnSc1d1
kašna	kašna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Krannerova	Krannerův	k2eAgFnSc1d1
kašna	kašna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1850	#num#	k4
byl	být	k5eAaImAgInS
odhalen	odhalit	k5eAaPmNgInS
pomník	pomník	k1gInSc4
Františka	František	k1gMnSc2
I.	I.	kA
(	(	kIx(
<g/>
autor	autor	k1gMnSc1
Josef	Josef	k1gMnSc1
Kranner	Kranner	k1gMnSc1
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
kašna	kašna	k1gFnSc1
<g/>
,	,	kIx,
také	také	k9
Hold	hold	k1gInSc1
českých	český	k2eAgInPc2d1
stavů	stav	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
parku	park	k1gInSc6
na	na	k7c6
tehdy	tehdy	k6eAd1
novém	nový	k2eAgNnSc6d1
nábřeží	nábřeží	k1gNnSc6
na	na	k7c6
Starém	starý	k2eAgNnSc6d1
Městě	město	k1gNnSc6
Pražském	pražský	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Vztyčení	vztyčení	k1gNnSc1
pomníku	pomník	k1gInSc2
panovníkovi	panovník	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
symbolem	symbol	k1gInSc7
reakce	reakce	k1gFnSc2
a	a	k8xC
potlačování	potlačování	k1gNnSc2
všech	všecek	k3xTgFnPc2
liberálních	liberální	k2eAgFnPc2d1
a	a	k8xC
národních	národní	k2eAgFnPc2d1
snah	snaha	k1gFnPc2
<g/>
,	,	kIx,
vzbudilo	vzbudit	k5eAaPmAgNnS
v	v	k7c6
českých	český	k2eAgInPc6d1
vlasteneckých	vlastenecký	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
nechuť	nechuť	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byla	být	k5eAaImAgFnS
sama	sám	k3xTgFnSc1
jezdecká	jezdecký	k2eAgFnSc1d1
socha	socha	k1gFnSc1
Františka	Františka	k1gFnSc1
I.	I.	kA
roku	rok	k1gInSc2
1919	#num#	k4
sňata	sňat	k2eAgFnSc1d1
a	a	k8xC
vrácena	vrácen	k2eAgFnSc1d1
v	v	k7c6
kopii	kopie	k1gFnSc6
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
s	s	k7c7
rodinou	rodina	k1gFnSc7
</s>
<s>
Z	z	k7c2
manželství	manželství	k1gNnSc2
s	s	k7c7
Alžbětou	Alžběta	k1gFnSc7
Vilemínou	Vilemína	k1gFnSc7
Württemberskou	Württemberský	k2eAgFnSc7d1
<g/>
:	:	kIx,
</s>
<s>
Luisa	Luisa	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1790	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1791	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arcivévodkyně	arcivévodkyně	k1gFnSc1
</s>
<s>
Z	z	k7c2
manželství	manželství	k1gNnSc2
s	s	k7c7
Marií	Maria	k1gFnSc7
Terezou	Tereza	k1gFnSc7
Neapolsko-Sicilskou	neapolsko-sicilský	k2eAgFnSc7d1
<g/>
:	:	kIx,
</s>
<s>
Marie	Marie	k1gFnSc1
Luisa	Luisa	k1gFnSc1
(	(	kIx(
<g/>
1791	#num#	k4
<g/>
–	–	k?
<g/>
1847	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
císařovna	císařovna	k1gFnSc1
francouzská	francouzský	k2eAgFnSc1d1
<g/>
,	,	kIx,
vévodkyně	vévodkyně	k1gFnSc1
z	z	k7c2
Parmy	Parma	k1gFnSc2
<g/>
,	,	kIx,
Piacenzy	Piacenza	k1gFnSc2
a	a	k8xC
Guastally	Guastalla	k1gFnSc2
</s>
<s>
∞	∞	k?
1810	#num#	k4
císař	císař	k1gMnSc1
Napoleon	Napoleon	k1gMnSc1
I.	I.	kA
</s>
<s>
∞	∞	k?
1821	#num#	k4
hrabě	hrabě	k1gMnSc1
Adam	Adam	k1gMnSc1
Albert	Albert	k1gMnSc1
Neipperg	Neipperg	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
hraběte	hrabě	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
Jana	Jan	k1gMnSc2
Neipperga	Neipperg	k1gMnSc2
a	a	k8xC
Vilemíny	Vilemína	k1gFnSc2
Hetzfeld-Wilfenburg	Hetzfeld-Wilfenburg	k1gMnSc1
</s>
<s>
∞	∞	k?
1834	#num#	k4
hrabě	hrabě	k1gMnSc1
Karel	Karel	k1gMnSc1
Bombelles	Bombelles	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
hraběte	hrabě	k1gMnSc2
Marka	Marek	k1gMnSc2
Maria	Maria	k1gFnSc1
Bombellesa	Bombellesa	k1gFnSc1
a	a	k8xC
Angeliky	Angelika	k1gFnPc1
z	z	k7c2
Mackau	Mackaus	k1gInSc2
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgMnSc1d1
(	(	kIx(
<g/>
1793	#num#	k4
<g/>
–	–	k?
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1831	#num#	k4
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
ze	z	k7c2
Sardinie	Sardinie	k1gFnSc2
a	a	k8xC
Piemontu	Piemont	k1gInSc2
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
krále	král	k1gMnSc2
Viktora	Viktor	k1gMnSc2
Emanuela	Emanuel	k1gMnSc2
I.	I.	kA
ze	z	k7c2
Sardinie	Sardinie	k1gFnSc2
a	a	k8xC
Piemontu	Piemont	k1gInSc2
a	a	k8xC
arcivévodkyně	arcivévodkyně	k1gFnSc2
Marie	Maria	k1gFnSc2
Terezy	Tereza	k1gFnSc2
Rakouské-Este	Rakouské-Est	k1gMnSc5
</s>
<s>
Karolína	Karolína	k1gFnSc1
Leopoldina	Leopoldina	k1gFnSc1
(	(	kIx(
<g/>
1794	#num#	k4
<g/>
–	–	k?
<g/>
1795	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arcivévodkyně	arcivévodkyně	k1gFnSc1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Luisa	Luisa	k1gFnSc1
(	(	kIx(
<g/>
1795	#num#	k4
<g/>
–	–	k?
<g/>
1799	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arcivévodkyně	arcivévodkyně	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Leopoldina	Leopoldina	k1gFnSc1
(	(	kIx(
<g/>
1797	#num#	k4
<g/>
–	–	k?
<g/>
1826	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
císařovna	císařovna	k1gFnSc1
brazilská	brazilský	k2eAgFnSc1d1
<g/>
,	,	kIx,
∞	∞	k?
1817	#num#	k4
císař	císař	k1gMnSc1
Pedro	Pedra	k1gFnSc5
I.	I.	kA
Brazilský	brazilský	k2eAgMnSc1d1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
krále	král	k1gMnSc2
Jana	Jan	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portugalského	portugalský	k2eAgNnSc2d1
a	a	k8xC
Šarloty	Šarlota	k1gFnPc1
Španělské	španělský	k2eAgFnPc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Klementina	Klementina	k1gFnSc1
(	(	kIx(
<g/>
1798	#num#	k4
<g/>
–	–	k?
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1818	#num#	k4
Leopold	Leopold	k1gMnSc1
Salernský	Salernský	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
krále	král	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
I.	I.	kA
Neapolsko-Sicilského	neapolsko-sicilský	k2eAgNnSc2d1
a	a	k8xC
Marie	Marie	k1gFnSc1
Karolíny	Karolína	k1gFnSc2
Habsbursko-Lotrinské	habsbursko-lotrinský	k2eAgFnSc2d1
</s>
<s>
Josef	Josef	k1gMnSc1
František	František	k1gMnSc1
(	(	kIx(
<g/>
1799	#num#	k4
<g/>
–	–	k?
<g/>
1807	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arcivévoda	arcivévoda	k1gMnSc1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Ferdinanda	Ferdinand	k1gMnSc2
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1832	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1819	#num#	k4
saský	saský	k2eAgMnSc1d1
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
August	August	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1802	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arcivévoda	arcivévoda	k1gMnSc1
∞	∞	k?
1824	#num#	k4
princezna	princezna	k1gFnSc1
Žofie	Žofie	k1gFnSc1
Frederika	Frederika	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1805	#num#	k4
<g/>
–	–	k?
<g/>
1872	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
krále	král	k1gMnSc4
Maximiliána	Maximilián	k1gMnSc4
I.	I.	kA
Bavorského	bavorský	k2eAgMnSc4d1
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
∞	∞	k?
1854	#num#	k4
Alžběta	Alžběta	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sissi	Sisse	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
(	(	kIx(
<g/>
1832	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mexický	mexický	k2eAgMnSc1d1
císař	císař	k1gMnSc1
∞	∞	k?
1857	#num#	k4
Charlotta	Charlotta	k1gFnSc1
Belgická	belgický	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
(	(	kIx(
<g/>
1833	#num#	k4
<g/>
–	–	k?
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Viktor	Viktor	k1gMnSc1
(	(	kIx(
<g/>
1842	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
(	(	kIx(
<g/>
1804	#num#	k4
<g/>
–	–	k?
<g/>
1858	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arcivévodkyně	arcivévodkyně	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Nepomuk	Nepomuk	k1gMnSc1
(	(	kIx(
<g/>
1805	#num#	k4
<g/>
–	–	k?
<g/>
1809	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arcivévoda	arcivévoda	k1gMnSc1
</s>
<s>
Amálie	Amálie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
<g/>
/	/	kIx~
<g/>
†	†	k?
1807	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arcivévodkyně	arcivévodkyně	k1gFnSc1
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Karel	Karel	k1gMnSc1
V.	V.	kA
Lotrinský	lotrinský	k2eAgInSc4d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Marie	Marie	k1gFnSc1
Josefa	Josefa	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
Štěpán	Štěpán	k1gMnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
I.	I.	kA
Orléanský	Orléanský	k2eAgInSc5d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Charlotta	Charlotta	k1gFnSc1
Orléanská	Orléanský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
Falcká	falcký	k2eAgFnSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Leopold	Leopold	k1gMnSc1
I.	I.	kA
</s>
<s>
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Magdalena	Magdalena	k1gFnSc1
Falcko-Neuburská	Falcko-Neuburský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Brunšvicko-Wolfenbüttelský	Brunšvicko-Wolfenbüttelský	k2eAgMnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Kristýna	Kristýna	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Öttingenská	Öttingenský	k2eAgFnSc1d1
</s>
<s>
'	'	kIx"
<g/>
František	františek	k1gInSc4
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
<g/>
'	'	kIx"
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Francouzský	francouzský	k2eAgMnSc1d1
<g/>
,	,	kIx,
velký	velký	k2eAgMnSc1d1
dauphin	dauphin	k1gMnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
V.	V.	kA
Španělský	španělský	k2eAgMnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
</s>
<s>
Eduard	Eduard	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parmský	parmský	k2eAgInSc1d1
</s>
<s>
Isabela	Isabela	k1gFnSc1
Farnese	Farnese	k1gFnSc2
</s>
<s>
Dorotea	Dorote	k2eAgFnSc1d1
Žofie	Žofie	k1gFnSc1
Falcko-Neuburská	Falcko-Neuburský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Ludovika	Ludovikum	k1gNnSc2
Španělská	španělský	k2eAgFnSc1d1
</s>
<s>
August	August	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silný	silný	k2eAgInSc1d1
</s>
<s>
August	August	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polský	polský	k2eAgInSc1d1
</s>
<s>
Kristýna	Kristýna	k1gFnSc1
Eberhardýna	Eberhardýna	k1gFnSc1
Hohenzollernská	hohenzollernský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Amálie	Amálie	k1gFnSc2
Saská	saský	k2eAgFnSc1d1
</s>
<s>
Josef	Josef	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Josefa	Josefa	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
</s>
<s>
Amálie	Amálie	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Brunšvicko-Lüneburská	Brunšvicko-Lüneburský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.bartleby.com	www.bartleby.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.britannica.com/EBchecked/topic/216736/Francis-II	http://www.britannica.com/EBchecked/topic/216736/Francis-II	k4
<g/>
↑	↑	k?
Dějiny	dějiny	k1gFnPc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
II	II	kA
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85192	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Františkovsko-metternichovský	Františkovsko-metternichovský	k2eAgInSc1d1
absolutismus	absolutismus	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
72	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA
<g/>
,	,	kIx,
Ivana	Ivana	k1gFnSc1
<g/>
;	;	kIx,
RAK	rak	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
VLNAS	VLNAS	kA
<g/>
,	,	kIx,
Vít	Vít	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stínu	stín	k1gInSc6
tvých	tvůj	k3xOp2gNnPc2
křídel	křídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburkové	Habsburk	k1gMnPc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grafoprint-Neubert	Grafoprint-Neubert	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
289	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85785	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA
<g/>
,	,	kIx,
Brigitte	Brigitte	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburkové	Habsburk	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životopisná	životopisný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Brána	brána	k1gFnSc1
;	;	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85946	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
UHLÍŘ	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	františek	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
I.	I.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
RYANTOVÁ	RYANTOVÁ	kA
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
VOREL	Vorel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
králové	král	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
940	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
509	#num#	k4
<g/>
-	-	kIx~
<g/>
517	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
UHLÍŘ	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesta	cesta	k1gFnSc1
toskánského	toskánský	k2eAgMnSc2d1
prince	princ	k1gMnSc2
za	za	k7c7
císařskou	císařský	k2eAgFnSc7d1
korunou	koruna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
160	#num#	k4
<g/>
-	-	kIx~
<g/>
178	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
6097	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
hlav	hlava	k1gFnPc2
českého	český	k2eAgInSc2d1
státu	stát	k1gInSc2
</s>
<s>
Seznam	seznam	k1gInSc1
panovníků	panovník	k1gMnPc2
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
František	Františka	k1gFnPc2
I.	I.	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Císař	Císař	k1gMnSc1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
František	František	k1gMnSc1
II	II	kA
<g/>
.1792	.1792	k4
<g/>
–	–	k?
<g/>
1806	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
—	—	k?
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
—	—	k?
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
František	František	k1gMnSc1
I	i	k9
<g/>
.1804	.1804	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
František	František	k1gMnSc1
I	i	k9
<g/>
.1792	.1792	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
František	František	k1gMnSc1
I	i	k9
<g/>
.1792	.1792	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Chorvatský	chorvatský	k2eAgMnSc1d1
a	a	k8xC
slavonský	slavonský	k2eAgMnSc1d1
král	král	k1gMnSc1
František	František	k1gMnSc1
I	i	k9
<g/>
.1792	.1792	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Haličský	haličský	k2eAgMnSc1d1
a	a	k8xC
vladimiřský	vladimiřský	k2eAgMnSc1d1
král	král	k1gMnSc1
1792	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
—	—	k?
</s>
<s>
Lombardsko-benátský	lombardsko-benátský	k2eAgMnSc1d1
král	král	k1gMnSc1
1814	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
—	—	k?
</s>
<s>
Dalmatský	dalmatský	k2eAgMnSc1d1
král	král	k1gMnSc1
1815	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1
arcivévoda	arcivévoda	k1gMnSc1
1792	#num#	k4
<g/>
–	–	k?
<g/>
1806	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Lucemburský	lucemburský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
1792	#num#	k4
<g/>
–	–	k?
<g/>
1795	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
—	—	k?
(	(	kIx(
<g/>
Pod	pod	k7c7
francouzskou	francouzský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
jako	jako	k8xS,k8xC
Département	Département	k1gInSc1
des	des	k1gNnSc2
Forê	Forê	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Milánský	milánský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
1792	#num#	k4
<g/>
–	–	k?
<g/>
1797	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
—	—	k?
republika	republika	k1gFnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Flanderský	flanderský	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
1792	#num#	k4
<g/>
–	–	k?
<g/>
1793	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
—	—	k?
Pod	pod	k7c7
francouzskou	francouzský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Hlava	hlava	k1gFnSc1
dynastie	dynastie	k1gFnSc2
1792	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Čeští	český	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Mytičtí	mytický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
<g/>
(	(	kIx(
<g/>
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgMnPc1
mytičtí	mytický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
•	•	k?
Krok	krok	k1gInSc1
•	•	k?
Libuše	Libuše	k1gFnSc1
Mytičtí	mytický	k2eAgMnPc1d1
Přemyslovci	Přemyslovec	k1gMnPc1
</s>
<s>
Přemysl	Přemysl	k1gMnSc1
Oráč	oráč	k1gMnSc1
•	•	k?
Nezamysl	Nezamysl	k1gMnSc1
•	•	k?
Mnata	Mnata	k1gFnSc1
•	•	k?
Vojen	vojna	k1gFnPc2
•	•	k?
Vnislav	Vnislav	k1gMnSc1
•	•	k?
Křesomysl	Křesomysl	k1gMnSc1
•	•	k?
Neklan	Neklan	k1gMnSc1
•	•	k?
Hostivít	Hostivít	k1gMnSc1
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
–	–	k?
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
872	#num#	k4
<g/>
–	–	k?
<g/>
889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spytihněv	Spytihněv	k1gFnPc4
I.	I.	kA
(	(	kIx(
<g/>
894	#num#	k4
<g/>
–	–	k?
<g/>
915	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
915	#num#	k4
<g/>
–	–	k?
<g/>
921	#num#	k4
<g/>
)	)	kIx)
•	•	k?
svatý	svatý	k1gMnSc1
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
921	#num#	k4
<g/>
–	–	k?
<g/>
935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
935	#num#	k4
<g/>
–	–	k?
<g/>
972	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
972	#num#	k4
<g/>
–	–	k?
<g/>
999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
999	#num#	k4
<g/>
–	–	k?
<g/>
1002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
Chrabrý¹	Chrabrý¹	k1gMnSc1
(	(	kIx(
<g/>
1003	#num#	k4
<g/>
–	–	k?
<g/>
1004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladivoj	Vladivoj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
•	•	k?
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1004	#num#	k4
<g/>
–	–	k?
<g/>
1012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oldřich	Oldřich	k1gMnSc1
(	(	kIx(
<g/>
1012	#num#	k4
<g/>
–	–	k?
<g/>
1033	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1033	#num#	k4
<g/>
–	–	k?
<g/>
1034	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Břetislav	Břetislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1034	#num#	k4
<g/>
–	–	k?
<g/>
1055	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spytihněv	Spytihněv	k1gFnPc4
II	II	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1055	#num#	k4
<g/>
–	–	k?
<g/>
1061	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1061	#num#	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konrád	Konrád	k1gMnSc1
I.	I.	kA
Brněnský	brněnský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
–	–	k?
<g/>
1100	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
(	(	kIx(
<g/>
1100	#num#	k4
<g/>
–	–	k?
<g/>
1107	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Svatopluk	Svatopluk	k1gMnSc1
Olomoucký	olomoucký	k2eAgMnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1109	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1109	#num#	k4
<g/>
–	–	k?
<g/>
1117	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1117	#num#	k4
<g/>
–	–	k?
<g/>
1020	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
I.	I.	kA
(	(	kIx(
<g/>
1120	#num#	k4
<g/>
–	–	k?
<g/>
1025	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1125	#num#	k4
<g/>
–	–	k?
<g/>
1140	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1140	#num#	k4
<g/>
–	–	k?
<g/>
1172	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1172	#num#	k4
<g/>
–	–	k?
<g/>
1173	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soběslav	Soběslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1173	#num#	k4
<g/>
–	–	k?
<g/>
1178	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1178	#num#	k4
<g/>
–	–	k?
<g/>
1189	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ota	Ota	k1gMnSc1
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1191	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1191	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1192	#num#	k4
<g/>
–	–	k?
<g/>
1193	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Břetislav	Břetislav	k1gMnSc1
(	(	kIx(
<g/>
1193	#num#	k4
<g/>
–	–	k?
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Vladislav	Vladislav	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1198	#num#	k4
<g/>
–	–	k?
<g/>
1230	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1230	#num#	k4
<g/>
–	–	k?
<g/>
1253	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1253	#num#	k4
<g/>
–	–	k?
<g/>
1278	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1278	#num#	k4
<g/>
–	–	k?
<g/>
1305	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1305	#num#	k4
<g/>
–	–	k?
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
–	–	k?
<g/>
1307	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1307	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
)	)	kIx)
Lucemburkové	Lucemburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
1346	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1346	#num#	k4
<g/>
–	–	k?
<g/>
1378	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1378	#num#	k4
<g/>
–	–	k?
<g/>
1419	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zikmund	Zikmund	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1419	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1437	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1438	#num#	k4
<g/>
–	–	k?
<g/>
1439	#num#	k4
<g/>
)	)	kIx)
•	•	k?
interregnum	interregnum	k1gNnSc4
(	(	kIx(
<g/>
1439	#num#	k4
<g/>
–	–	k?
<g/>
1453	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
(	(	kIx(
<g/>
1453	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1457	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Korvín²	Korvín²	k1gMnSc1
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1490	#num#	k4
<g/>
)	)	kIx)
Jagellonci	Jagellonec	k1gInSc6
<g/>
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1516	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1516	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1564	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1564	#num#	k4
<g/>
–	–	k?
<g/>
1576	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1576	#num#	k4
<g/>
–	–	k?
<g/>
1611	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1611	#num#	k4
<g/>
–	–	k?
<g/>
1619	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1637	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fridrich	Fridrich	k1gMnSc1
Falcký³	Falcký³	k1gMnSc1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1620	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1637	#num#	k4
<g/>
–	–	k?
<g/>
1657	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgInSc1d1
•	•	k?
Leopold	Leopolda	k1gFnPc2
I.	I.	kA
(	(	kIx(
<g/>
1657	#num#	k4
<g/>
–	–	k?
<g/>
1705	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Josef	Josef	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1705	#num#	k4
<g/>
–	–	k?
<g/>
1711	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1711	#num#	k4
<g/>
–	–	k?
<g/>
1740	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
1740	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský³	Bavorský³	k1gFnSc1
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1743	#num#	k4
<g/>
)	)	kIx)
Habsburko-Lotrinkové	Habsburko-Lotrinkový	k2eAgFnSc2d1
<g/>
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1790	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1790	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgMnSc1d1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
¹	¹	k?
Piastovec	Piastovec	k1gInSc1
<g/>
,	,	kIx,
²	²	k?
vládce	vládce	k1gMnSc1
vedlejších	vedlejší	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
<g/>
,	,	kIx,
³	³	k?
vzdorokrál	vzdorokrál	k1gMnSc1
</s>
<s>
Rakouští	rakouský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Markrabata	markrabě	k1gNnPc4
Malý	malý	k2eAgInSc4d1
znak	znak	k1gInSc4
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babenberkové	Babenberkové	k2eAgInSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Jindřich	Jindřich	k1gMnSc1
I.	I.	kA
•	•	k?
Adalbert	Adalbert	k1gMnSc1
•	•	k?
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
do	do	k7c2
1156	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vévodové	vévoda	k1gMnPc1
Babenberkové	Babenberková	k1gFnSc2
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1156	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leopold	Leopold	k1gMnSc1
V.	V.	kA
•	•	k?
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
•	•	k?
Leopold	Leopold	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přemyslovci	Přemyslovec	k1gMnSc6
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zähringové	Zähringový	k2eAgInPc1d1
</s>
<s>
Heřman	Heřman	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bádenský	bádenský	k2eAgMnSc1d1
•	•	k?
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Bádenský	bádenský	k2eAgInSc1d1
Habsburkové	Habsburk	k1gMnPc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
I.	I.	kA
•	•	k?
Albrecht	Albrecht	k1gMnSc1
I.	I.	kA
•	•	k?
Rudolf	Rudolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Ota	Ota	k1gMnSc1
•	•	k?
Rudolf	Rudolf	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Albrecht	Albrecht	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Vilém	Vilém	k1gMnSc1
•	•	k?
Albrecht	Albrecht	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Albrecht	Albrecht	k1gMnSc1
V.	V.	kA
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Arnošt	Arnošt	k1gMnSc1
Železný	Železný	k1gMnSc1
•	•	k?
Fridrich	Fridrich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
do	do	k7c2
1453	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Arcivévodové	arcivévoda	k1gMnPc1
Habsburkové	Habsburk	k1gMnPc1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1453	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Rudolf	Rudolf	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Matyáš	Matyáš	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
Habsbursko-lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
do	do	k7c2
1804	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Císařové	Císařové	k2eAgFnSc1d1
Habsbursko-lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
od	od	k7c2
1804	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
rakouští	rakouský	k2eAgMnPc1d1
princové	princ	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
titulární	titulární	k2eAgMnPc1d1
arcivévodové	arcivévoda	k1gMnPc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
uvedeni	uvést	k5eAaPmNgMnP
v	v	k7c6
jiné	jiný	k2eAgFnSc6d1
šabloně	šablona	k1gFnSc6
</s>
<s>
Rakouští	rakouský	k2eAgMnPc1d1
arcivévodové	arcivévoda	k1gMnPc1
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
Znak	znak	k1gInSc4
rakouské	rakouský	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
I.	I.	kA
<g/>
*	*	kIx~
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
V.	V.	kA
<g/>
*	*	kIx~
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
<g/>
*	*	kIx~
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyrolský	tyrolský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štýrský	štýrský	k2eAgInSc4d1
6	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
V.	V.	kA
•	•	k?
Karel	Karel	k1gMnSc1
7	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Jan	Jan	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
Vilém	Vilém	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Tyrolský	tyrolský	k2eAgMnSc1d1
•	•	k?
Zikmund	Zikmund	k1gMnSc1
František	František	k1gMnSc1
Tyrolský	tyrolský	k2eAgMnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
Josef	Josef	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
9	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Štěpán	Štěpán	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
11	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
František	František	k1gMnSc1
<g/>
**	**	k?
12	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Těšínský	Těšínský	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Leopold	Leopold	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Josef	Josef	k1gMnSc1
Antonín	Antonín	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Antonín	Antonín	k1gMnSc1
Viktor	Viktor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jan	Jan	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Rainer	Rainer	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Ludvík	Ludvík	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modenský	modenský	k2eAgInSc1d1
<g/>
***	***	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
***	***	k?
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
***	***	k?
•	•	k?
Karel	Karel	k1gMnSc1
Ambrož	Ambrož	k1gMnSc1
<g/>
***	***	k?
13	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
•	•	k?
František	františek	k1gInSc1
Karel	Karel	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Fridrich	Fridrich	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Vilém	Vilém	k1gMnSc1
Těšínský	Těšínský	k1gMnSc1
•	•	k?
Štěpán	Štěpán	k1gMnSc1
Viktor	Viktor	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
•	•	k?
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Rainer	Rainer	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
V.	V.	kA
Modenský	modenský	k2eAgInSc1d1
<g/>
***	***	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Viktor	Viktor	k1gMnSc1
<g/>
***	***	k?
14	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
Mexický	mexický	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Viktor	Viktor	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jan	Jan	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Bedřich	Bedřich	k1gMnSc1
Rakousko-Těšínský	rakousko-těšínský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
•	•	k?
Evžen	Evžen	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
August	August	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Filip	Filip	k1gMnSc1
15	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
***	***	k?
•	•	k?
Ota	Ota	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Petr	Petr	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Leopold	Leopold	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
•	•	k?
Leo	Leo	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Vilém	Vilém	k1gMnSc1
František	František	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
František	František	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Luitpold	Luitpold	k1gMnSc1
16	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Evžen	Evžen	k1gMnSc1
•	•	k?
Gottfried	Gottfried	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Jiří	Jiří	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Rainer	Rainer	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Antonín	Antonín	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Pius	Pius	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Karel	Karel	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Hubert	Hubert	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Theodor	Theodor	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Klement	Klement	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
17	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Habsburg	Habsburg	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
***	***	k?
•	•	k?
Felix	Felix	k1gMnSc1
Rakouský	rakouský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
*	*	kIx~
také	také	k9
španělský	španělský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
(	(	kIx(
<g/>
infant	infant	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
**	**	k?
také	také	k9
toskánský	toskánský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
<g/>
,	,	kIx,
***	***	k?
také	také	k9
modenský	modenský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
<g/>
,	,	kIx,
kurzívou	kurzíva	k1gFnSc7
jsou	být	k5eAaImIp3nP
rakouští	rakouský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700557	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118534955	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
6630	#num#	k4
0031	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82111992	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500353963	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
36912131	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82111992	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Maďarsko	Maďarsko	k1gNnSc1
</s>
