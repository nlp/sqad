<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
texty	text	k1gInPc4	text
kapely	kapela	k1gFnSc2	kapela
Iron	iron	k1gInSc1	iron
Maiden	Maidno	k1gNnPc2	Maidno
často	často	k6eAd1	často
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
nesou	nést	k5eAaImIp3nP	nést
znaky	znak	k1gInPc7	znak
bájesloví	bájesloví	k1gNnSc2	bájesloví
<g/>
,	,	kIx,	,
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
poezie	poezie	k1gFnSc2	poezie
<g/>
;	;	kIx,	;
k	k	k7c3	k
takovým	takový	k3xDgFnPc3	takový
písním	píseň	k1gFnPc3	píseň
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Rime	Rim	k1gMnSc2	Rim
of	of	k?	of
the	the	k?	the
Ancient	Ancient	k1gMnSc1	Ancient
Mariner	Mariner	k1gMnSc1	Mariner
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
starém	staré	k1gNnSc6	staré
námořníkovi	námořníkův	k2eAgMnPc1d1	námořníkův
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
básni	báseň	k1gFnSc6	báseň
Samuela	Samuel	k1gMnSc2	Samuel
Taylora	Taylor	k1gMnSc2	Taylor
Coleridge	Coleridg	k1gMnSc2	Coleridg
<g/>
.	.	kIx.	.
</s>
