<s>
doc.	doc.	kA	doc.
Juraj	Juraj	k1gInSc1	Juraj
Jakubisko	Jakubisko	k1gNnSc1	Jakubisko
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1938	[number]	k4	1938
Kojšov	Kojšov	k1gInSc1	Kojšov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
i	i	k8xC	i
kameraman	kameraman	k1gMnSc1	kameraman
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
filmy	film	k1gInPc1	film
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
velmi	velmi	k6eAd1	velmi
osobitou	osobitý	k2eAgFnSc7d1	osobitá
poetikou	poetika	k1gFnSc7	poetika
<g/>
,	,	kIx,	,
bizarností	bizarnost	k1gFnSc7	bizarnost
i	i	k8xC	i
podivuhodnou	podivuhodný	k2eAgFnSc7d1	podivuhodná
hravostí	hravost	k1gFnSc7	hravost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
Jakubiskových	Jakubiskový	k2eAgInPc6d1	Jakubiskový
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
iracionální	iracionální	k2eAgNnPc1d1	iracionální
<g/>
,	,	kIx,	,
tajemné	tajemný	k2eAgNnSc1d1	tajemné
a	a	k8xC	a
senzační	senzační	k2eAgMnSc1d1	senzační
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
přirozené	přirozený	k2eAgNnSc4d1	přirozené
jako	jako	k9	jako
život	život	k1gInSc4	život
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
z	z	k7c2	z
nás	my	k3xPp1nPc4	my
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
schopní	schopný	k2eAgMnPc1d1	schopný
Jakubiskova	Jakubiskův	k2eAgNnSc2d1	Jakubiskovo
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
umožňujícího	umožňující	k2eAgMnSc2d1	umožňující
vidět	vidět	k5eAaImF	vidět
ono	onen	k3xDgNnSc4	onen
tajemné	tajemný	k2eAgNnSc4d1	tajemné
<g/>
,	,	kIx,	,
neočekávané	očekávaný	k2eNgNnSc4d1	neočekávané
a	a	k8xC	a
fantastické	fantastický	k2eAgNnSc4d1	fantastické
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
prostém	prostý	k2eAgNnSc6d1	prosté
denním	denní	k2eAgNnSc6d1	denní
živote	život	k1gInSc5	život
<g/>
"	"	kIx"	"
-	-	kIx~	-
Federico	Federico	k6eAd1	Federico
Fellini	Fellin	k2eAgMnPc1d1	Fellin
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
typickému	typický	k2eAgInSc3d1	typický
rukopisu	rukopis	k1gInSc3	rukopis
<g/>
,	,	kIx,	,
plnému	plný	k2eAgMnSc3d1	plný
alegorií	alegorie	k1gFnPc2	alegorie
<g/>
,	,	kIx,	,
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
vizionářské	vizionářský	k2eAgFnSc2d1	vizionářská
obraznosti	obraznost	k1gFnSc2	obraznost
se	se	k3xPyFc4	se
o	o	k7c6	o
Juraji	Juraj	k1gFnSc6	Juraj
Jakubiskovi	Jakubisek	k1gMnSc3	Jakubisek
píše	psát	k5eAaImIp3nS	psát
jako	jako	k9	jako
o	o	k7c6	o
umělci	umělec	k1gMnSc6	umělec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
magickým	magický	k2eAgInSc7d1	magický
realismem	realismus	k1gInSc7	realismus
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
filmu	film	k1gInSc6	film
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Gabriel	Gabriel	k1gMnSc1	Gabriel
García	García	k1gMnSc1	García
Márquez	Márquez	k1gMnSc1	Márquez
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Juraj	Juraj	k1gInSc1	Juraj
Jakubisko	Jakubisko	k1gNnSc1	Jakubisko
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pozornost	pozornost	k1gFnSc4	pozornost
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
experimentálním	experimentální	k2eAgInPc3d1	experimentální
filmům	film	k1gInPc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
je	být	k5eAaImIp3nS	být
slovenská	slovenský	k2eAgFnSc1d1	slovenská
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
filmová	filmový	k2eAgFnSc1d1	filmová
producentka	producentka	k1gFnSc1	producentka
Deana	Deano	k1gNnSc2	Deano
Horváthová-Jakubisková	Horváthová-Jakubiskový	k2eAgFnSc1d1	Horváthová-Jakubisková
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgInS	debutovat
dlouhometrážním	dlouhometrážní	k2eAgInSc7d1	dlouhometrážní
hraným	hraný	k2eAgInSc7d1	hraný
filmem	film	k1gInSc7	film
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
léta	léto	k1gNnSc2	léto
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
Kristove	Kristov	k1gInSc5	Kristov
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc4	film
Zběhové	zběh	k1gMnPc1	zběh
a	a	k8xC	a
poutníci	poutník	k1gMnPc1	poutník
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
Zbehovia	Zbehovia	k1gFnSc1	Zbehovia
a	a	k8xC	a
pútnici	pútnik	k1gMnPc1	pútnik
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ptáčkové	Ptáčková	k1gFnPc1	Ptáčková
<g/>
,	,	kIx,	,
siroty	sirota	k1gFnPc1	sirota
a	a	k8xC	a
blázni	blázen	k1gMnPc1	blázen
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
Vtáčikovia	Vtáčikovia	k1gFnSc1	Vtáčikovia
<g/>
,	,	kIx,	,
siroty	sirota	k1gFnPc1	sirota
a	a	k8xC	a
blázni	blázen	k1gMnPc1	blázen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
a	a	k8xC	a
tragikomedie	tragikomedie	k1gFnSc1	tragikomedie
Na	na	k7c4	na
shledanou	shledaná	k1gFnSc4	shledaná
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
<g/>
,	,	kIx,	,
přátelé	přítel	k1gMnPc1	přítel
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
Dovidenia	Dovidenium	k1gNnPc4	Dovidenium
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
<g/>
,	,	kIx,	,
priatelia	priatelia	k1gFnSc1	priatelia
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
dokončen	dokončit	k5eAaPmNgInS	dokončit
byl	být	k5eAaImAgInS	být
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
normalizační	normalizační	k2eAgFnPc1d1	normalizační
cenzurou	cenzura	k1gFnSc7	cenzura
zakázány	zakázán	k2eAgFnPc1d1	zakázána
a	a	k8xC	a
Juraj	Juraj	k1gInSc1	Juraj
Jakubisko	Jakubisko	k1gNnSc4	Jakubisko
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
věnovat	věnovat	k5eAaImF	věnovat
pouze	pouze	k6eAd1	pouze
dokumentární	dokumentární	k2eAgFnSc3d1	dokumentární
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
natočil	natočit	k5eAaBmAgInS	natočit
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
Postav	postav	k1gInSc1	postav
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
zasaď	zasadit	k5eAaPmRp2nS	zasadit
strom	strom	k1gInSc1	strom
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
Postav	postav	k1gInSc1	postav
dom	dom	k?	dom
<g/>
,	,	kIx,	,
zasaď	zasadit	k5eAaPmRp2nS	zasadit
strom	strom	k1gInSc1	strom
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
třídílný	třídílný	k2eAgInSc1d1	třídílný
slovenský	slovenský	k2eAgInSc1d1	slovenský
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Nevera	Nevero	k1gNnSc2	Nevero
po	po	k7c6	po
slovensky	slovensky	k6eAd1	slovensky
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
historická	historický	k2eAgFnSc1d1	historická
sága	sága	k1gFnSc1	sága
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
slovenským	slovenský	k2eAgInSc7d1	slovenský
názvem	název	k1gInSc7	název
Tisícročná	Tisícročný	k2eAgFnSc1d1	Tisícročná
včela	včela	k1gFnSc1	včela
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
množství	množství	k1gNnSc4	množství
filmových	filmový	k2eAgNnPc2d1	filmové
ocenění	ocenění	k1gNnPc2	ocenění
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
Perinbaba	Perinbaba	k1gFnSc1	Perinbaba
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
se	s	k7c7	s
slovenským	slovenský	k2eAgInSc7d1	slovenský
názvem	název	k1gInSc7	název
Sedím	sedit	k5eAaImIp1nS	sedit
na	na	k7c6	na
konári	konár	k1gFnSc6	konár
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
dobre	dobr	k1gInSc5	dobr
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lepšie	Lepšie	k1gFnSc1	Lepšie
byť	byť	k8xS	byť
bohatý	bohatý	k2eAgInSc1d1	bohatý
a	a	k8xC	a
zdravý	zdravý	k2eAgInSc1d1	zdravý
ako	ako	k?	ako
chudobný	chudobný	k2eAgInSc1d1	chudobný
a	a	k8xC	a
chorý	chorý	k2eAgInSc1d1	chorý
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Lepší	lepšit	k5eAaImIp3nS	lepšit
být	být	k5eAaImF	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
a	a	k8xC	a
zdravý	zdravý	k2eAgMnSc1d1	zdravý
než	než	k8xS	než
chudý	chudý	k2eAgMnSc1d1	chudý
a	a	k8xC	a
nemocný	mocný	k2eNgMnSc1d1	nemocný
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
milostného	milostný	k2eAgInSc2d1	milostný
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
tvorbě	tvorba	k1gFnSc3	tvorba
ze	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
natočil	natočit	k5eAaBmAgMnS	natočit
filmy	film	k1gInPc4	film
Nejasná	jasný	k2eNgFnSc1d1	nejasná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
a	a	k8xC	a
Post	post	k1gInSc1	post
coitum	coitum	k1gNnSc1	coitum
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
filmové	filmový	k2eAgFnSc2d1	filmová
akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
také	také	k9	také
docentem	docent	k1gMnSc7	docent
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
FAMU	FAMU	kA	FAMU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc4d1	poslední
film	film	k1gInSc4	film
režiséra	režisér	k1gMnSc2	režisér
Bathory	Bathora	k1gFnSc2	Bathora
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
neúspěšnějším	úspěšný	k2eNgInSc7d2	neúspěšnější
filmem	film	k1gInSc7	film
desetiletí	desetiletí	k1gNnSc2	desetiletí
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
nejúspěšnejším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Posbíral	posbírat	k5eAaPmAgMnS	posbírat
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc3	ocenění
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
kupř	kupř	kA	kupř
<g/>
:	:	kIx,	:
CENA	cena	k1gFnSc1	cena
IGRIC	IGRIC	kA	IGRIC
-	-	kIx~	-
Slovensko	Slovensko	k1gNnSc1	Slovensko
-	-	kIx~	-
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ČESKÝ	český	k2eAgMnSc1d1	český
LEV	Lev	k1gMnSc1	Lev
-	-	kIx~	-
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
SLNKO	SLNKO	kA	SLNKO
V	v	k7c6	v
SIETI	SIETI	kA	SIETI
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Monaco	Monaco	k1gMnSc1	Monaco
International	International	k1gFnSc2	International
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
WorldFest-Houston	WorldFest-Houston	k1gInSc4	WorldFest-Houston
International	International	k1gFnSc2	International
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
-	-	kIx~	-
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Institutu	institut	k1gInSc6	institut
klinické	klinický	k2eAgFnSc2d1	klinická
a	a	k8xC	a
experimentální	experimentální	k2eAgFnSc2d1	experimentální
medicíny	medicína	k1gFnSc2	medicína
(	(	kIx(	(
<g/>
IKEM	IKEM	kA	IKEM
<g/>
)	)	kIx)	)
transplantaci	transplantace	k1gFnSc4	transplantace
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesáti	sedmdesát	k4xCc6	sedmdesát
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zřejmě	zřejmě	k6eAd1	zřejmě
nejstarším	starý	k2eAgMnSc7d3	nejstarší
pacientem	pacient	k1gMnSc7	pacient
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
tento	tento	k3xDgInSc1	tento
zákrok	zákrok	k1gInSc1	zákrok
proveden	provést	k5eAaPmNgInS	provést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
autobiografie	autobiografie	k1gFnSc2	autobiografie
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Živé	živý	k2eAgNnSc4d1	živé
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
přípravách	příprava	k1gFnPc6	příprava
volného	volný	k2eAgInSc2d1	volný
pokračování	pokračování	k1gNnPc4	pokračování
pohádky	pohádka	k1gFnSc2	pohádka
Perinbaba	Perinbaba	k1gFnSc1	Perinbaba
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
uveden	uvést	k5eAaPmNgMnS	uvést
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
v	v	k7c6	v
roce	rok	k1gInSc6	rok
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
Bathory	Bathora	k1gFnSc2	Bathora
2004	[number]	k4	2004
Post	posta	k1gFnPc2	posta
Coitum	Coitum	k1gNnSc1	Coitum
1997	[number]	k4	1997
Nejasná	jasný	k2eNgFnSc1d1	nejasná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
1992	[number]	k4	1992
Lepší	lepšit	k5eAaImIp3nS	lepšit
být	být	k5eAaImF	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
a	a	k8xC	a
zdravý	zdravý	k2eAgMnSc1d1	zdravý
než	než	k8xS	než
<g />
.	.	kIx.	.
</s>
<s>
chudý	chudý	k2eAgMnSc1d1	chudý
a	a	k8xC	a
nemocný	nemocný	k1gMnSc1	nemocný
1990	[number]	k4	1990
Téměř	téměř	k6eAd1	téměř
růžový	růžový	k2eAgInSc1d1	růžový
příběh	příběh	k1gInSc1	příběh
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
Sedím	sedit	k5eAaImIp1nS	sedit
na	na	k7c4	na
konáry	konáry	k?	konáry
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
dobře	dobře	k6eAd1	dobře
1987	[number]	k4	1987
Teta	Teta	k1gFnSc1	Teta
(	(	kIx(	(
<g/>
mini	mini	k2eAgInPc1d1	mini
<g/>
)	)	kIx)	)
TV	TV	kA	TV
seriál	seriál	k1gInSc4	seriál
1987	[number]	k4	1987
Pehatý	Pehatý	k2eAgInSc4d1	Pehatý
Max	max	kA	max
a	a	k8xC	a
strašidla	strašidlo	k1gNnSc2	strašidlo
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
Freckled	Freckled	k1gMnSc1	Freckled
Max	Max	k1gMnSc1	Max
and	and	k?	and
the	the	k?	the
Spooks	Spooks	k1gInSc1	Spooks
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
Perinbaba	Perinbaba	k1gFnSc1	Perinbaba
1983	[number]	k4	1983
Tisícročná	Tisícročný	k2eAgNnPc4d1	Tisícročný
<g />
.	.	kIx.	.
</s>
<s>
včela	včela	k1gFnSc1	včela
1981	[number]	k4	1981
Nevera	Nevero	k1gNnSc2	Nevero
po	po	k7c6	po
slovensky	slovensky	k6eAd1	slovensky
I.	I.	kA	I.
<g/>
-II	-II	k?	-II
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
Postav	postav	k1gInSc1	postav
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
zasaď	zasadit	k5eAaPmRp2nS	zasadit
strom	strom	k1gInSc4	strom
1978	[number]	k4	1978
Tri	Tri	k1gFnSc1	Tri
vrecia	vrecium	k1gNnSc2	vrecium
cementu	cement	k1gInSc2	cement
a	a	k8xC	a
živý	živý	k2eAgInSc4d1	živý
kohút	kohút	k1gInSc4	kohút
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Tři	tři	k4xCgInPc1	tři
pytle	pytel	k1gInPc1	pytel
cementu	cement	k1gInSc2	cement
a	a	k8xC	a
živý	živý	k2eAgMnSc1d1	živý
kohout	kohout	k1gMnSc1	kohout
<g/>
)	)	kIx)	)
1977	[number]	k4	1977
Bubeník	Bubeník	k1gMnSc1	Bubeník
Červeného	Červeného	k2eAgInSc2d1	Červeného
kříže	kříž	k1gInSc2	kříž
1975	[number]	k4	1975
Slovensko	Slovensko	k1gNnSc1	Slovensko
-	-	kIx~	-
krajina	krajina	k1gFnSc1	krajina
pod	pod	k7c7	pod
Tatrami	Tatra	k1gFnPc7	Tatra
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
1972	[number]	k4	1972
Stavba	stavba	k1gFnSc1	stavba
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
The	The	k1gFnPc2	The
Construction	Construction	k1gInSc4	Construction
of	of	k?	of
the	the	k?	the
Century	Centura	k1gFnSc2	Centura
<g/>
)	)	kIx)	)
1970	[number]	k4	1970
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Na	na	k7c4	na
shledanou	shledaná	k1gFnSc4	shledaná
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
<g/>
,	,	kIx,	,
přátelé	přítel	k1gMnPc1	přítel
<g/>
!	!	kIx.	!
</s>
<s>
1969	[number]	k4	1969
Ptáčkové	Ptáčkové	k2eAgInPc1d1	Ptáčkové
<g/>
,	,	kIx,	,
siroty	sirota	k1gFnPc1	sirota
a	a	k8xC	a
blázni	blázen	k1gMnPc1	blázen
1968	[number]	k4	1968
Zběhové	zběh	k1gMnPc1	zběh
a	a	k8xC	a
poutníci	poutník	k1gMnPc1	poutník
1967	[number]	k4	1967
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
léta	léto	k1gNnSc2	léto
1965	[number]	k4	1965
Čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
1965	[number]	k4	1965
Déšť	déšť	k1gInSc1	déšť
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
<g/>
Rain	Rain	k1gInSc1	Rain
<g/>
)	)	kIx)	)
1963	[number]	k4	1963
Mlčení	mlčení	k1gNnSc2	mlčení
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
<g/>
Silence	silenka	k1gFnSc3	silenka
<g/>
)	)	kIx)	)
1962	[number]	k4	1962
První	první	k4xOgFnSc1	první
třída	třída	k1gFnSc1	třída
1961	[number]	k4	1961
Strieborný	Strieborný	k2eAgInSc4d1	Strieborný
vietor	vietor	k1gInSc4	vietor
1960	[number]	k4	1960
Každý	každý	k3xTgInSc1	každý
den	den	k1gInSc4	den
má	mít	k5eAaImIp3nS	mít
<g />
.	.	kIx.	.
</s>
<s>
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
1960	[number]	k4	1960
Poslední	poslední	k2eAgInSc4d1	poslední
nálet	nálet	k1gInSc4	nálet
2012	[number]	k4	2012
Gijón	Gijón	k1gInSc4	Gijón
International	International	k1gFnSc2	International
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
celoživotní	celoživotní	k2eAgInSc4d1	celoživotní
přínos	přínos	k1gInSc4	přínos
světovému	světový	k2eAgMnSc3d1	světový
filmu	film	k1gInSc2	film
2010	[number]	k4	2010
Monaco	Monaco	k1gNnSc4	Monaco
Charity	charita	k1gFnSc2	charita
Film	film	k1gInSc4	film
FestBathory	FestBathora	k1gFnSc2	FestBathora
•	•	k?	•
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
umělecký	umělecký	k2eAgInSc4d1	umělecký
počin	počin	k1gInSc4	počin
Slnko	Slnko	k1gNnSc1	Slnko
v	v	k7c6	v
sieti	sieť	k1gFnSc6	sieť
•	•	k?	•
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
filmový	filmový	k2eAgInSc4d1	filmový
počin	počin	k1gInSc4	počin
2009	[number]	k4	2009
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
2008	[number]	k4	2008
<g/>
Bathory	Bathora	k1gFnSc2	Bathora
•	•	k?	•
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
výtvarný	výtvarný	k2eAgInSc1d1	výtvarný
počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
•	•	k?	•
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
výtvarník	výtvarník	k1gMnSc1	výtvarník
a	a	k8xC	a
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
koncepce	koncepce	k1gFnSc1	koncepce
roku	rok	k1gInSc2	rok
Cena	cena	k1gFnSc1	cena
IgricBathory	IgricBathora	k1gFnSc2	IgricBathora
•	•	k?	•
za	za	k7c4	za
výtvarný	výtvarný	k2eAgInSc4d1	výtvarný
design	design	k1gInSc4	design
filmu	film	k1gInSc2	film
Elza	Elz	k1gInSc2	Elz
Morante	Morant	k1gMnSc5	Morant
•	•	k?	•
Cena	cena	k1gFnSc1	cena
kinematografie	kinematografie	k1gFnSc1	kinematografie
2008	[number]	k4	2008
XLIII	XLIII	kA	XLIII
<g/>
.	.	kIx.	.
</s>
<s>
MFF	MFF	kA	MFF
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
•	•	k?	•
Křišťálový	křišťálový	k2eAgInSc4d1	křišťálový
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
umělecký	umělecký	k2eAgInSc4d1	umělecký
přínos	přínos	k1gInSc4	přínos
světové	světový	k2eAgFnSc6d1	světová
kinematografii	kinematografie	k1gFnSc3	kinematografie
2003	[number]	k4	2003
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
2002	[number]	k4	2002
•	•	k?	•
Dlouholetý	dlouholetý	k2eAgInSc1d1	dlouholetý
umělecký	umělecký	k2eAgInSc1d1	umělecký
přínos	přínos	k1gInSc1	přínos
českému	český	k2eAgInSc3d1	český
filmu	film	k1gInSc3	film
(	(	kIx(	(
<g/>
udělený	udělený	k2eAgInSc1d1	udělený
Českou	český	k2eAgFnSc7d1	Česká
filmovou	filmový	k2eAgFnSc7d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc7d1	televizní
akademií	akademie	k1gFnSc7	akademie
<g/>
)	)	kIx)	)
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Republiky	republika	k1gFnSc2	republika
•	•	k?	•
Pribinův	Pribinův	k2eAgInSc1d1	Pribinův
kříž	kříž	k1gInSc1	kříž
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
2002	[number]	k4	2002
X.	X.	kA	X.
MFF	MFF	kA	MFF
Art	Art	k1gFnSc1	Art
Film	film	k1gInSc1	film
Trenčianské	trenčianský	k2eAgFnSc2d1	Trenčianská
<g />
.	.	kIx.	.
</s>
<s>
Teplice	teplice	k1gFnSc1	teplice
•	•	k?	•
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kamera	kamera	k1gFnSc1	kamera
za	za	k7c4	za
celoživotní	celoživotní	k2eAgInSc4d1	celoživotní
přínos	přínos	k1gInSc4	přínos
slovenské	slovenský	k2eAgFnPc1d1	slovenská
kinematografii	kinematografie	k1gFnSc3	kinematografie
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zahajovacího	zahajovací	k2eAgInSc2d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
akademie	akademie	k1gFnSc1	akademie
umění	umění	k1gNnSc2	umění
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
celoživotní	celoživotní	k2eAgFnSc4d1	celoživotní
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
činnost	činnost	k1gFnSc4	činnost
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gMnSc1	lev
2000	[number]	k4	2000
•	•	k?	•
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
filmový	filmový	k2eAgInSc4d1	filmový
plakát	plakát	k1gInSc4	plakát
k	k	k7c3	k
filmu	film	k1gInSc3	film
Kytice	kytice	k1gFnSc2	kytice
-	-	kIx~	-
Juraj	Juraj	k1gInSc1	Juraj
Jakubisko	Jakubisko	k1gNnSc1	Jakubisko
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
při	při	k7c6	při
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
<g />
.	.	kIx.	.
</s>
<s>
nominací	nominace	k1gFnSc7	nominace
ceny	cena	k1gFnSc2	cena
Český	český	k2eAgInSc4d1	český
lev	lev	k1gInSc4	lev
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
Přehlídka	přehlídka	k1gFnSc1	přehlídka
filmů	film	k1gInPc2	film
Juraje	Juraj	k1gInSc2	Juraj
Jakubiska	Jakubisko	k1gNnSc2	Jakubisko
v	v	k7c6	v
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
•	•	k?	•
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
pečeť	pečeť	k1gFnSc1	pečeť
za	za	k7c4	za
významný	významný	k2eAgInSc4d1	významný
přínos	přínos	k1gInSc4	přínos
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
udělená	udělený	k2eAgFnSc1d1	udělená
Jugoslávskou	jugoslávský	k2eAgFnSc7d1	jugoslávská
filmotékou	filmotéka	k1gFnSc7	filmotéka
Anketa	anketa	k1gFnSc1	anketa
slovenských	slovenský	k2eAgMnPc2d1	slovenský
filmových	filmový	k2eAgMnPc2d1	filmový
novinářů	novinář	k1gMnPc2	novinář
a	a	k8xC	a
kritiků	kritik	k1gMnPc2	kritik
•	•	k?	•
První	první	k4xOgFnSc4	první
místo	místo	k7c2	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
slovenský	slovenský	k2eAgMnSc1d1	slovenský
režisér	režisér	k1gMnSc1	režisér
1999	[number]	k4	1999
Cran	Cran	k1gInSc1	Cran
Gavier	Gavier	k1gInSc4	Gavier
<g/>
́	́	k?	́
<g/>
99	[number]	k4	99
<g/>
Sedím	sedit	k5eAaImIp1nS	sedit
na	na	k7c4	na
konári	konáre	k1gFnSc4	konáre
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
dobre	dobr	k1gInSc5	dobr
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
1998	[number]	k4	1998
Taos	Taos	k1gInSc1	Taos
Talking	Talking	k1gInSc4	Talking
Picture	Pictur	k1gMnSc5	Pictur
Festival	festival	k1gInSc1	festival
•	•	k?	•
Cena	cena	k1gFnSc1	cena
Maverick	Mavericko	k1gNnPc2	Mavericko
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
Taos	Taosa	k1gFnPc2	Taosa
Talking	Talking	k1gInSc1	Talking
Pictures	Pictures	k1gMnSc1	Pictures
Film	film	k1gInSc1	film
FestivalNejasná	FestivalNejasný	k2eAgFnSc1d1	FestivalNejasný
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
vizuální	vizuální	k2eAgInSc4d1	vizuální
přínos	přínos	k1gInSc4	přínos
do	do	k7c2	do
kinematografie	kinematografie	k1gFnSc2	kinematografie
XXI	XXI	kA	XXI
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
Denver	Denver	k1gInSc1	Denver
•	•	k?	•
Uznání	uznání	k1gNnSc1	uznání
za	za	k7c4	za
vynikající	vynikající	k2eAgInSc4d1	vynikající
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
filmovém	filmový	k2eAgNnSc6d1	filmové
umění	umění	k1gNnSc6	umění
Cena	cena	k1gFnSc1	cena
Matěje	Matěj	k1gMnSc2	Matěj
Hrebendy	Hrebenda	k1gFnSc2	Hrebenda
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
česko-slovenských	českolovenský	k2eAgInPc2d1	česko-slovenský
vztahů	vztah	k1gInPc2	vztah
Montreal	Montreal	k1gInSc1	Montreal
World	World	k1gMnSc1	World
Film	film	k1gInSc1	film
FestivalNejasná	FestivalNejasný	k2eAgFnSc1d1	FestivalNejasný
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
umělecký	umělecký	k2eAgInSc4d1	umělecký
přínos	přínos	k1gInSc4	přínos
a	a	k8xC	a
kinematografii	kinematografie	k1gFnSc4	kinematografie
roku	rok	k1gInSc2	rok
Český	český	k2eAgInSc4d1	český
literární	literární	k2eAgInSc4d1	literární
fond	fond	k1gInSc4	fond
•	•	k?	•
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
režisér	režisér	k1gMnSc1	režisér
roku	rok	k1gInSc2	rok
98	[number]	k4	98
San	San	k1gMnSc7	San
Diego	Diego	k6eAd1	Diego
Film	film	k1gInSc4	film
FestivalNejasná	FestivalNejasný	k2eAgFnSc1d1	FestivalNejasný
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
literárního	literární	k2eAgNnSc2d1	literární
fonduNejasná	fonduNejasný	k2eAgFnSc1d1	fonduNejasný
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
1997	[number]	k4	1997
PescaraNejasná	PescaraNejasný	k2eAgFnSc1d1	PescaraNejasný
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
delfín	delfín	k1gMnSc1	delfín
1993	[number]	k4	1993
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
de	de	k?	de
Cinema	Cinem	k1gMnSc2	Cinem
de	de	k?	de
Troia	Troius	k1gMnSc2	Troius
Setubal	Setubal	k1gMnSc2	Setubal
<g/>
,	,	kIx,	,
Costa	Cost	k1gMnSc2	Cost
AzilLepší	AzilLepší	k1gNnSc2	AzilLepší
je	být	k5eAaImIp3nS	být
být	být	k5eAaImF	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
a	a	k8xC	a
zdravý	zdravý	k2eAgMnSc1d1	zdravý
než	než	k8xS	než
chudý	chudý	k2eAgMnSc1d1	chudý
a	a	k8xC	a
nemocný	nemocný	k2eAgMnSc1d1	nemocný
•	•	k?	•
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
cena	cena	k1gFnSc1	cena
Zlatý	zlatý	k1gInSc1	zlatý
delfín	delfín	k1gMnSc1	delfín
1991	[number]	k4	1991
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc4	Angeles
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
-	-	kIx~	-
AFI	AFI	kA	AFI
Fest	fest	k6eAd1	fest
•	•	k?	•
Tribute	tribut	k1gInSc5	tribut
award	award	k1gMnSc1	award
1990	[number]	k4	1990
IFF	IFF	kA	IFF
MoscowSedím	MoscowSedím	k1gFnSc2	MoscowSedím
na	na	k7c4	na
konári	konáre	k1gFnSc4	konáre
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
dobre	dobr	k1gInSc5	dobr
•	•	k?	•
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
cena	cena	k1gFnSc1	cena
Festival	festival	k1gInSc1	festival
československého	československý	k2eAgNnSc2d1	Československé
filmuSedím	filmuSedit	k5eAaPmIp1nS	filmuSedit
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
konári	konár	k1gInSc6	konár
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
dobre	dobr	k1gInSc5	dobr
•	•	k?	•
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
IFF	IFF	kA	IFF
StasbourgSedím	StasbourgSedím	k1gMnSc2	StasbourgSedím
na	na	k7c6	na
konári	konár	k1gFnSc6	konár
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
dobre	dobr	k1gInSc5	dobr
•	•	k?	•
Cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
a	a	k8xC	a
Cena	cena	k1gFnSc1	cena
Alsace	Alsace	k1gFnSc2	Alsace
Media	medium	k1gNnSc2	medium
1989	[number]	k4	1989
Benátský	benátský	k2eAgInSc1d1	benátský
filmový	filmový	k2eAgInSc1d1	filmový
festivalSedím	festivalSedit	k5eAaPmIp1nS	festivalSedit
na	na	k7c4	na
konári	konáre	k1gFnSc4	konáre
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
dobre	dobr	k1gInSc5	dobr
•	•	k?	•
Čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
a	a	k8xC	a
plaketa	plaketa	k1gFnSc1	plaketa
RAI	RAI	kA	RAI
II	II	kA	II
1988	[number]	k4	1988
Titul	titul	k1gInSc4	titul
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
1987	[number]	k4	1987
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
RimovskiPerinbaba	RimovskiPerinbaba	k1gFnSc1	RimovskiPerinbaba
•	•	k?	•
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
cena	cena	k1gFnSc1	cena
Camerio	Camerio	k1gMnSc1	Camerio
I.	I.	kA	I.
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
Buenos	Buenosa	k1gFnPc2	Buenosa
AiresPerinbaba	AiresPerinbaba	k1gFnSc1	AiresPerinbaba
•	•	k?	•
První	první	k4xOgFnSc1	první
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
1986	[number]	k4	1986
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládežPerinbaba	mládežPerinbaba	k1gFnSc1	mládežPerinbaba
•	•	k?	•
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
Medaile	medaile	k1gFnSc2	medaile
slovenského	slovenský	k2eAgMnSc2d1	slovenský
filmuPerinbaba	filmuPerinbab	k1gMnSc2	filmuPerinbab
XXIV	XXIV	kA	XXIV
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
filmu	film	k1gInSc2	film
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
lázněPerinbaba	lázněPerinbaba	k1gFnSc1	lázněPerinbaba
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
stránku	stránka	k1gFnSc4	stránka
filmu	film	k1gInSc2	film
Filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
v	v	k7c6	v
BělehraduPerinbaba	BělehraduPerinbaba	k1gFnSc1	BělehraduPerinbaba
•	•	k?	•
Cena	cena	k1gFnSc1	cena
diváků	divák	k1gMnPc2	divák
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
BenátkyPerinbaba	BenátkyPerinbaba	k1gFnSc1	BenátkyPerinbaba
•	•	k?	•
Čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
RAI	RAI	kA	RAI
II	II	kA	II
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
mladého	mladý	k2eAgMnSc2d1	mladý
diváka	divák	k1gMnSc2	divák
LyonPerinbaba	LyonPerinbab	k1gMnSc2	LyonPerinbab
•	•	k?	•
Cena	cena	k1gFnSc1	cena
mladého	mladý	k2eAgMnSc2d1	mladý
diváka	divák	k1gMnSc2	divák
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
I.	I.	kA	I.
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
přehlídka	přehlídka	k1gFnSc1	přehlídka
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
Mar	Mar	k1gFnSc2	Mar
del	del	k?	del
PlataPerinbaba	PlataPerinbaba	k1gFnSc1	PlataPerinbaba
•	•	k?	•
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
uznání	uznání	k1gNnSc4	uznání
1985	[number]	k4	1985
XLII	XLII	kA	XLII
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
BenátkyPerinbaba	BenátkyPerinbaba	k1gFnSc1	BenátkyPerinbaba
•	•	k?	•
Cena	cena	k1gFnSc1	cena
Gondola	gondola	k1gFnSc1	gondola
<g/>
,	,	kIx,	,
Zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
uznání	uznání	k1gNnSc1	uznání
poroty	porota	k1gFnSc2	porota
Katolického	katolický	k2eAgNnSc2d1	katolické
filmového	filmový	k2eAgNnSc2d1	filmové
střediska	středisko	k1gNnSc2	středisko
1984	[number]	k4	1984
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
MFF	MFF	kA	MFF
SevillaTisícročná	SevillaTisícročný	k2eAgFnSc1d1	SevillaTisícročný
včela	včela	k1gFnSc1	včela
•	•	k?	•
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
cena	cena	k1gFnSc1	cena
Konfederace	konfederace	k1gFnSc2	konfederace
španělských	španělský	k2eAgInPc2d1	španělský
filmových	filmový	k2eAgInPc2d1	filmový
klubů	klub	k1gInPc2	klub
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
festivalu	festival	k1gInSc2	festival
XLVI	XLVI	kA	XLVI
<g/>
.	.	kIx.	.
</s>
<s>
MFF	MFF	kA	MFF
Benátky	Benátky	k1gFnPc1	Benátky
•	•	k?	•
Čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
a	a	k8xC	a
plaketa	plaketa	k1gFnSc1	plaketa
RAI	RAI	kA	RAI
II	II	kA	II
<g/>
.	.	kIx.	.
za	za	k7c4	za
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
filmovou	filmový	k2eAgFnSc4d1	filmová
tvorbu	tvorba	k1gFnSc4	tvorba
1982	[number]	k4	1982
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
slovenské	slovenský	k2eAgFnPc1d1	slovenská
veselohryNevera	veselohryNevera	k1gFnSc1	veselohryNevera
po	po	k7c6	po
slovensky	slovensky	k6eAd1	slovensky
•	•	k?	•
Cena	cena	k1gFnSc1	cena
smíchu	smích	k1gInSc2	smích
1979	[number]	k4	1979
MFF	MFF	kA	MFF
technických	technický	k2eAgInPc2d1	technický
filmů	film	k1gInPc2	film
Budapešť	Budapešť	k1gFnSc4	Budapešť
Bubeník	Bubeník	k1gMnSc1	Bubeník
červeného	červený	k2eAgInSc2d1	červený
kríža	kríž	k1gInSc2	kríž
•	•	k?	•
1	[number]	k4	1
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
1978	[number]	k4	1978
Filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
v	v	k7c4	v
Oberhausenu	Oberhausen	k2eAgFnSc4d1	Oberhausen
Bubeník	Bubeník	k1gMnSc1	Bubeník
červeného	červený	k2eAgInSc2d1	červený
kríža	kríž	k1gInSc2	kríž
•	•	k?	•
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
MFF	MFF	kA	MFF
Varne	Varn	k1gInSc5	Varn
Bubeník	Bubeník	k1gMnSc1	Bubeník
červeného	červený	k2eAgInSc2d1	červený
kríža	kríž	k1gInSc2	kríž
•	•	k?	•
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
medaile	medaile	k1gFnSc1	medaile
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
Celostátní	celostátní	k2eAgFnSc1d1	celostátní
přehlídka	přehlídka	k1gFnSc1	přehlídka
o	o	k7c6	o
umění	umění	k1gNnSc6	umění
ARS	ARS	kA	ARS
FILM	film	k1gInSc1	film
Bubeník	Bubeník	k1gMnSc1	Bubeník
červeného	červený	k2eAgInSc2d1	červený
kríža	kríž	k1gInSc2	kríž
•	•	k?	•
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
cena	cena	k1gFnSc1	cena
1973	[number]	k4	1973
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
MFF	MFF	kA	MFF
technického	technický	k2eAgInSc2d1	technický
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
Stavba	stavba	k1gFnSc1	stavba
století	století	k1gNnSc2	století
•	•	k?	•
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
cena	cena	k1gFnSc1	cena
1969	[number]	k4	1969
Přehlídka	přehlídka	k1gFnSc1	přehlídka
Československých	československý	k2eAgInPc2d1	československý
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
Sorrento	Sorrento	k1gNnSc1	Sorrento
•	•	k?	•
ZLATÁ	zlatá	k1gFnSc1	zlatá
SIRÉNA	Siréna	k1gFnSc1	Siréna
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
filmů	film	k1gInPc2	film
Kristove	Kristov	k1gInSc5	Kristov
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
Zbehovia	Zbehovium	k1gNnPc4	Zbehovium
a	a	k8xC	a
pútnici	pútnice	k1gFnSc4	pútnice
a	a	k8xC	a
Vtáčkovia	Vtáčkovium	k1gNnPc4	Vtáčkovium
<g/>
,	,	kIx,	,
siroty	sirota	k1gFnPc4	sirota
a	a	k8xC	a
blázni	blázen	k1gMnPc1	blázen
Trilobit	trilobit	k1gMnSc1	trilobit
1968	[number]	k4	1968
<g/>
Zbehovia	Zbehovium	k1gNnPc1	Zbehovium
a	a	k8xC	a
pútnici	pútnik	k1gMnPc1	pútnik
•	•	k?	•
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
•	•	k?	•
Nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
režie	režie	k1gFnSc2	režie
•	•	k?	•
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
IGRIC	IGRIC	kA	IGRIC
1968	[number]	k4	1968
<g/>
Zbehovia	Zbehovium	k1gNnSc2	Zbehovium
a	a	k8xC	a
pútnici	pútnice	k1gFnSc4	pútnice
•	•	k?	•
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
•	•	k?	•
Nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
režie	režie	k1gFnSc2	režie
•	•	k?	•
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
1968	[number]	k4	1968
XXIX	XXIX	kA	XXIX
<g/>
.	.	kIx.	.
</s>
<s>
MFF	MFF	kA	MFF
BenátkyZbehovia	BenátkyZbehovius	k1gMnSc4	BenátkyZbehovius
a	a	k8xC	a
pútnici	pútnice	k1gFnSc4	pútnice
•	•	k?	•
Medaile	medaile	k1gFnSc2	medaile
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
tvůrce	tvůrce	k1gMnPc4	tvůrce
(	(	kIx(	(
<g/>
Malý	malý	k2eAgMnSc1d1	malý
lev	lev	k1gMnSc1	lev
<g/>
)	)	kIx)	)
IGRIC	IGRIC	kA	IGRIC
<g/>
́	́	k?	́
<g/>
67	[number]	k4	67
•	•	k?	•
režie	režie	k1gFnSc1	režie
filmů	film	k1gInPc2	film
Kristove	Kristov	k1gInSc5	Kristov
roky	rok	k1gInPc4	rok
a	a	k8xC	a
Zbehovia	Zbehovium	k1gNnPc4	Zbehovium
a	a	k8xC	a
pútnici	pútnice	k1gFnSc4	pútnice
Trilobit	trilobit	k1gMnSc1	trilobit
1967	[number]	k4	1967
•	•	k?	•
výroční	výroční	k2eAgFnSc1d1	výroční
cena	cena	k1gFnSc1	cena
FITESu	FITESus	k1gInSc2	FITESus
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
filmů	film	k1gInPc2	film
Kristove	Kristov	k1gInSc5	Kristov
roky	rok	k1gInPc4	rok
a	a	k8xC	a
Zbehovia	Zbehovium	k1gNnPc4	Zbehovium
a	a	k8xC	a
pútnici	pútnice	k1gFnSc4	pútnice
Cena	cena	k1gFnSc1	cena
československé	československý	k2eAgFnSc2d1	Československá
filmové	filmový	k2eAgFnSc2d1	filmová
kritiky	kritika	k1gFnSc2	kritika
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g />
.	.	kIx.	.
</s>
<s>
1967	[number]	k4	1967
<g/>
Kristove	Kristov	k1gInSc5	Kristov
roky	rok	k1gInPc4	rok
•	•	k?	•
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
československý	československý	k2eAgInSc4d1	československý
film	film	k1gInSc4	film
roku	rok	k1gInSc2	rok
Filmový	filmový	k2eAgInSc4d1	filmový
festival	festival	k1gInSc4	festival
pracujících	pracující	k1gFnPc2	pracující
<g/>
́	́	k?	́
<g/>
67	[number]	k4	67
<g/>
,	,	kIx,	,
BruntálKristove	BruntálKristov	k1gInSc5	BruntálKristov
roky	rok	k1gInPc4	rok
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
•	•	k?	•
Cena	cena	k1gFnSc1	cena
města	město	k1gNnSc2	město
Bruntál	Bruntál	k1gInSc4	Bruntál
za	za	k7c4	za
nejprogresivnější	progresivní	k2eAgInSc4d3	nejprogresivnější
výkon	výkon	k1gInSc4	výkon
mladého	mladý	k2eAgMnSc2d1	mladý
umělce	umělec	k1gMnSc2	umělec
Cena	cena	k1gFnSc1	cena
FINÁLE	finále	k1gNnSc1	finále
PlzeňZbehovia	PlzeňZbehovius	k1gMnSc2	PlzeňZbehovius
a	a	k8xC	a
pútnici	pútnice	k1gFnSc4	pútnice
•	•	k?	•
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
kameru	kamera	k1gFnSc4	kamera
V.	V.	kA	V.
Celostátní	celostátní	k2eAgFnSc1d1	celostátní
přehlídka	přehlídka	k1gFnSc1	přehlídka
o	o	k7c6	o
umění	umění	k1gNnSc6	umění
ARS	ARS	kA	ARS
FILM	film	k1gInSc1	film
Dážď	Dážď	k1gFnSc1	Dážď
•	•	k?	•
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
Vysťahovalec	Vysťahovalec	k1gMnSc1	Vysťahovalec
•	•	k?	•
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
1967	[number]	k4	1967
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
týden	týden	k1gInSc1	týden
Mannheim	Mannheim	k1gInSc1	Mannheim
Kristove	Kristov	k1gInSc5	Kristov
roky	rok	k1gInPc4	rok
•	•	k?	•
Cena	cena	k1gFnSc1	cena
FIPRESCI	FIPRESCI	kA	FIPRESCI
•	•	k?	•
Cena	cena	k1gFnSc1	cena
Josefa	Josef	k1gMnSc2	Josef
Sternberga	Sternberg	k1gMnSc2	Sternberg
za	za	k7c4	za
nejoriginálnější	originální	k2eAgInSc4d3	nejoriginálnější
a	a	k8xC	a
nejpřekvapivější	překvapivý	k2eAgInSc4d3	nejpřekvapivější
film	film	k1gInSc4	film
festivalu	festival	k1gInSc2	festival
•	•	k?	•
Cena	cena	k1gFnSc1	cena
Jury	jury	k1gFnSc1	jury
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
lidové	lidový	k2eAgFnSc2d1	lidová
školy	škola	k1gFnSc2	škola
Cena	cena	k1gFnSc1	cena
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
Filmového	filmový	k2eAgInSc2d1	filmový
klubu	klub	k1gInSc2	klub
Kristove	Kristov	k1gInSc5	Kristov
roky	rok	k1gInPc4	rok
•	•	k?	•
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
Vysťahovalec	Vysťahovalec	k1gMnSc1	Vysťahovalec
•	•	k?	•
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
Múza	Múza	k1gFnSc1	Múza
lyriky	lyrika	k1gFnSc2	lyrika
Kristove	Kristov	k1gInSc5	Kristov
roky	rok	k1gInPc4	rok
•	•	k?	•
Cena	cena	k1gFnSc1	cena
pražských	pražský	k2eAgMnPc2d1	pražský
diváků	divák	k1gMnPc2	divák
TERPSICHORÉ	TERPSICHORÉ	kA	TERPSICHORÉ
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
pracujících	pracující	k1gMnPc2	pracující
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
Kristove	Kristov	k1gInSc5	Kristov
roky	rok	k1gInPc4	rok
•	•	k?	•
Cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
</s>
<s>
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
týdnu	týden	k1gInSc6	týden
v	v	k7c6	v
Mannheimu	Mannheim	k1gInSc6	Mannheim
Čakanie	Čakanie	k1gFnSc2	Čakanie
na	na	k7c6	na
Godota	Godota	k1gFnSc1	Godota
•	•	k?	•
Cena	cena	k1gFnSc1	cena
Simony	Simona	k1gFnSc2	Simona
Dubreuilh	Dubreuilha	k1gFnPc2	Dubreuilha
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Dny	den	k1gInPc1	den
krátkého	krátký	k2eAgInSc2d1	krátký
filmu	film	k1gInSc2	film
v	v	k7c4	v
Oberhausenu	Oberhausen	k2eAgFnSc4d1	Oberhausen
Čakanie	Čakanie	k1gFnPc4	Čakanie
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
•	•	k?	•
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátký	krátký	k2eAgInSc4d1	krátký
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
•	•	k?	•
II	II	kA	II
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
poroty	porota	k1gFnSc2	porota
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
lidových	lidový	k2eAgFnPc2d1	lidová
škol	škola	k1gFnPc2	škola
NSR	NSR	kA	NSR
X.	X.	kA	X.
MFF	MFF	kA	MFF
Grand	grand	k1gMnSc1	grand
Premio	Premio	k1gMnSc1	Premio
Bergamo	Bergamo	k1gNnSc1	Bergamo
Dážď	Dážď	k1gMnSc1	Dážď
•	•	k?	•
Zlatá	zlatá	k1gFnSc1	zlatá
medaile	medaile	k1gFnSc2	medaile
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
autorského	autorský	k2eAgInSc2d1	autorský
filmu	film	k1gInSc2	film
1964	[number]	k4	1964
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
festivalu	festival	k1gInSc2	festival
experimentálních	experimentální	k2eAgInPc2d1	experimentální
filmů	film	k1gInPc2	film
Knokke	Knokk	k1gInSc2	Knokk
le	le	k?	le
Zoute	Zout	k1gInSc5	Zout
Mlčanie	Mlčanie	k1gFnSc2	Mlčanie
•	•	k?	•
<g />
.	.	kIx.	.
</s>
<s>
Čestné	čestný	k2eAgNnSc1d1	čestné
uznání	uznání	k1gNnSc1	uznání
bruselské	bruselský	k2eAgFnSc2d1	bruselská
školy	škola	k1gFnSc2	škola
INSAC	INSAC	kA	INSAC
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národné	národný	k2eAgFnSc2d1	národná
divadlo	divadlo	k1gNnSc4	divadlo
•	•	k?	•
Svätopluk	Svätopluk	k1gInSc1	Svätopluk
2008	[number]	k4	2008
•	•	k?	•
Krútňava	Krútňava	k1gFnSc1	Krútňava
1999	[number]	k4	1999
Laterna	laterna	k1gFnSc1	laterna
magika	magika	k1gFnSc1	magika
•	•	k?	•
Casanova	Casanův	k2eAgMnSc2d1	Casanův
-	-	kIx~	-
Premiéra	premiér	k1gMnSc2	premiér
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Derniéra	derniéra	k1gFnSc1	derniéra
<g/>
:	:	kIx,	:
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
2010	[number]	k4	2010
6	[number]	k4	6
výstav	výstava	k1gFnPc2	výstava
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
2009	[number]	k4	2009
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
MIRO	Mira	k1gFnSc5	Mira
Galéria	Galérium	k1gNnSc2	Galérium
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc4	Slovensko
Prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
2005	[number]	k4	2005
Berlin	berlina	k1gFnPc2	berlina
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
2004	[number]	k4	2004
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
