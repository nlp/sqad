<p>
<s>
Saaremaa	Saaremaa	k1gFnSc1	Saaremaa
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Ösel	Ösel	k1gInSc1	Ösel
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
Ösel	Ösel	k1gInSc1	Ösel
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Osilia	Osilia	k1gFnSc1	Osilia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
estonských	estonský	k2eAgInPc2d1	estonský
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Saaremaa	Saaremaa	k6eAd1	Saaremaa
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Západoestonského	Západoestonský	k2eAgNnSc2d1	Západoestonský
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Hiiumaa	Hiiuma	k1gInSc2	Hiiuma
a	a	k8xC	a
západně	západně	k6eAd1	západně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Muhu	Muhus	k1gInSc2	Muhus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc1d1	zeměpisná
souřadnice	souřadnice	k1gFnPc1	souřadnice
středu	střed	k1gInSc2	střed
ostrova	ostrov	k1gInSc2	ostrov
jsou	být	k5eAaImIp3nP	být
58	[number]	k4	58
<g/>
°	°	k?	°
30	[number]	k4	30
<g/>
'	'	kIx"	'
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
22	[number]	k4	22
<g/>
°	°	k?	°
30	[number]	k4	30
<g/>
'	'	kIx"	'
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
2714	[number]	k4	2714
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
činí	činit	k5eAaImIp3nS	činit
největším	veliký	k2eAgInSc7d3	veliký
estonským	estonský	k2eAgInSc7d1	estonský
ostrovem	ostrov	k1gInSc7	ostrov
a	a	k8xC	a
po	po	k7c6	po
Sjæ	Sjæ	k1gFnSc6	Sjæ
<g/>
,	,	kIx,	,
Gotlandu	Gotlando	k1gNnSc6	Gotlando
a	a	k8xC	a
Fynu	Fyna	k1gFnSc4	Fyna
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
ostrova	ostrov	k1gInSc2	ostrov
má	mít	k5eAaImIp3nS	mít
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
54	[number]	k4	54
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Jméno	jméno	k1gNnSc1	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
jméno	jméno	k1gNnSc4	jméno
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
islandské	islandský	k2eAgFnPc1d1	islandská
ságy	sága	k1gFnPc1	sága
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
Saaremaa	Saaremaa	k1gFnSc1	Saaremaa
objevuje	objevovat	k5eAaImIp3nS	objevovat
jako	jako	k9	jako
Eysysla	Eysysla	k1gFnSc1	Eysysla
<g/>
,	,	kIx,	,
složenina	složenina	k1gFnSc1	složenina
z	z	k7c2	z
ey	ey	k?	ey
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ostrov	ostrov	k1gInSc4	ostrov
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
sysla	sysel	k1gMnSc2	sysel
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
území	území	k1gNnSc1	území
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
okruh	okruh	k1gInSc4	okruh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
starého	starý	k2eAgNnSc2d1	staré
označení	označení	k1gNnSc2	označení
zřejmě	zřejmě	k6eAd1	zřejmě
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jak	jak	k6eAd1	jak
německé	německý	k2eAgNnSc1d1	německé
a	a	k8xC	a
švédské	švédský	k2eAgNnSc1d1	švédské
jméno	jméno	k1gNnSc1	jméno
ostrova	ostrov	k1gInSc2	ostrov
Ösel	Ösela	k1gFnPc2	Ösela
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
latinské	latinský	k2eAgFnPc1d1	Latinská
Osilia	Osilia	k1gFnSc1	Osilia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Estonské	estonský	k2eAgNnSc1d1	Estonské
jméno	jméno	k1gNnSc1	jméno
Saaremaa	Saarema	k1gInSc2	Saarema
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
islandskému	islandský	k2eAgMnSc3d1	islandský
Eysysla	Eysysla	k1gMnSc3	Eysysla
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
utvořeno	utvořen	k2eAgNnSc1d1	utvořeno
ze	z	k7c2	z
složek	složka	k1gFnPc2	složka
saar	saara	k1gFnPc2	saara
(	(	kIx(	(
<g/>
genitiv	genitiv	k1gInSc1	genitiv
saare	saar	k1gInSc5	saar
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ostrov	ostrov	k1gInSc4	ostrov
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
maa	maa	k?	maa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
země	zem	k1gFnPc1	zem
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
území	území	k1gNnSc2	území
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
estonské	estonský	k2eAgNnSc1d1	Estonské
osídlení	osídlení	k1gNnSc1	osídlení
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
germánské	germánský	k2eAgNnSc1d1	germánské
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eysysla	Eysysla	k1gFnSc1	Eysysla
je	být	k5eAaImIp3nS	být
překladem	překlad	k1gInSc7	překlad
původnějšího	původní	k2eAgNnSc2d2	původnější
Saaremaa	Saaremaum	k1gNnSc2	Saaremaum
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
ve	v	k7c6	v
staroestonštině	staroestonština	k1gFnSc6	staroestonština
Saarenmaa	Saarenma	k1gInSc2	Saarenma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
teorie	teorie	k1gFnPc1	teorie
ztotožňují	ztotožňovat	k5eAaImIp3nP	ztotožňovat
ostrov	ostrov	k1gInSc4	ostrov
Saaremaa	Saarema	k1gInSc2	Saarema
s	s	k7c7	s
ostrovem	ostrov	k1gInSc7	ostrov
Thúlé	Thúlý	k2eAgFnSc2d1	Thúlý
<g/>
,	,	kIx,	,
popsaným	popsaný	k2eAgMnSc7d1	popsaný
starověkým	starověký	k2eAgMnSc7d1	starověký
mořeplavcem	mořeplavec	k1gMnSc7	mořeplavec
Pýtheem	Pýtheus	k1gMnSc7	Pýtheus
z	z	k7c2	z
Massilie	Massilie	k1gFnSc2	Massilie
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Thúlé	Thúlý	k2eAgNnSc1d1	Thúlý
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k9	jako
odvozené	odvozený	k2eAgInPc1d1	odvozený
z	z	k7c2	z
estonského	estonský	k2eAgNnSc2d1	Estonské
tuli	tuli	k1gNnSc2	tuli
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
oheň	oheň	k1gInSc1	oheň
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
staroestonsky	staroestonsky	k6eAd1	staroestonsky
tule	tula	k1gFnSc3	tula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
složkou	složka	k1gFnSc7	složka
pojmenování	pojmenování	k1gNnSc2	pojmenování
meteoritického	meteoritický	k2eAgInSc2d1	meteoritický
kráteru	kráter	k1gInSc2	kráter
v	v	k7c6	v
Kaali	Kaale	k1gFnSc6	Kaale
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Okolní	okolní	k2eAgInPc4d1	okolní
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
pobřeží	pobřeží	k1gNnSc4	pobřeží
===	===	k?	===
</s>
</p>
<p>
<s>
Saaremaa	Saaremaa	k1gFnSc1	Saaremaa
je	být	k5eAaImIp3nS	být
oddělena	oddělit	k5eAaPmNgFnS	oddělit
průlivem	průliv	k1gInSc7	průliv
Väike	Väik	k1gInSc2	Väik
väin	väin	k1gInSc4	väin
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Muhu	Muhus	k1gInSc2	Muhus
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
průlivem	průliv	k1gInSc7	průliv
Soela	Soelo	k1gNnSc2	Soelo
väin	väina	k1gFnPc2	väina
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Hiuumaa	Hiuuma	k1gInSc2	Hiuuma
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
břehy	břeh	k1gInPc4	břeh
omývá	omývat	k5eAaImIp3nS	omývat
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
mělké	mělký	k2eAgNnSc4d1	mělké
Průlivové	Průlivový	k2eAgNnSc4d1	Průlivový
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
a	a	k8xC	a
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
vody	voda	k1gFnSc2	voda
Rižského	rižský	k2eAgInSc2d1	rižský
zálivu	záliv	k1gInSc2	záliv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
a	a	k8xC	a
severozápadu	severozápad	k1gInSc2	severozápad
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
je	být	k5eAaImIp3nS	být
Saaremaa	Saaremaa	k1gFnSc1	Saaremaa
obklopena	obklopit	k5eAaPmNgFnS	obklopit
desítkami	desítka	k1gFnPc7	desítka
dalších	další	k2eAgInPc2d1	další
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
největší	veliký	k2eAgFnSc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Abruka	Abruk	k1gMnSc4	Abruk
a	a	k8xC	a
Vilsandi	Vilsand	k1gMnPc1	Vilsand
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odhlédneme	odhlédnout	k5eAaPmIp1nP	odhlédnout
<g/>
-li	i	k?	-li
od	od	k7c2	od
velmi	velmi	k6eAd1	velmi
členitého	členitý	k2eAgNnSc2d1	členité
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ostrov	ostrov	k1gInSc4	ostrov
Saaremaa	Saarema	k1gInSc2	Saarema
tvar	tvar	k1gInSc1	tvar
oválu	ovál	k1gInSc2	ovál
natočeného	natočený	k2eAgInSc2d1	natočený
přibližně	přibližně	k6eAd1	přibližně
východozápadním	východozápadní	k2eAgInSc7d1	východozápadní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
mohutný	mohutný	k2eAgMnSc1d1	mohutný
Sõ	Sõ	k1gMnSc1	Sõ
poloostrov	poloostrov	k1gInSc1	poloostrov
(	(	kIx(	(
<g/>
Sõ	Sõ	k1gMnSc1	Sõ
poolsaar	poolsaar	k1gMnSc1	poolsaar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrch	povrch	k1gInSc1	povrch
===	===	k?	===
</s>
</p>
<p>
<s>
Saaremaa	Saaremaa	k1gFnSc1	Saaremaa
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
plochý	plochý	k2eAgInSc1d1	plochý
a	a	k8xC	a
horopisně	horopisně	k6eAd1	horopisně
chudý	chudý	k2eAgInSc4d1	chudý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ma	Ma	k?	Ma
západě	západ	k1gInSc6	západ
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
Západoostrovní	Západoostrovní	k2eAgFnSc1d1	Západoostrovní
vysočina	vysočina	k1gFnSc1	vysočina
(	(	kIx(	(
<g/>
Lääne-Saaremaa	Lääne-Saaremaa	k1gFnSc1	Lääne-Saaremaa
kõ	kõ	k?	kõ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnSc2d1	dosahující
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
54	[number]	k4	54
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
Sõ	Sõ	k1gMnSc6	Sõ
poloostrově	poloostrov	k1gInSc6	poloostrov
Sõ	Sõ	k1gFnSc1	Sõ
vysočina	vysočina	k1gFnSc1	vysočina
(	(	kIx(	(
<g/>
Sõ	Sõ	k1gFnSc1	Sõ
kõ	kõ	k?	kõ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mohutné	mohutný	k2eAgInPc1d1	mohutný
pobřežní	pobřežní	k2eAgInPc1d1	pobřežní
vápencové	vápencový	k2eAgInPc1d1	vápencový
útesy	útes	k1gInPc1	útes
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
povrch	povrch	k1gInSc1	povrch
ostrova	ostrov	k1gInSc2	ostrov
nese	nést	k5eAaImIp3nS	nést
stopy	stopa	k1gFnPc1	stopa
působení	působení	k1gNnSc2	působení
někdejšího	někdejší	k2eAgInSc2d1	někdejší
pevninského	pevninský	k2eAgInSc2d1	pevninský
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plochý	plochý	k2eAgInSc4d1	plochý
povrch	povrch	k1gInSc4	povrch
ostrova	ostrov	k1gInSc2	ostrov
zdobí	zdobit	k5eAaImIp3nP	zdobit
též	též	k9	též
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
řady	řada	k1gFnSc2	řada
kruhových	kruhový	k2eAgNnPc2d1	kruhové
hradišť	hradiště	k1gNnPc2	hradiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
největší	veliký	k2eAgFnSc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Valjala	Valjala	k1gMnSc4	Valjala
<g/>
,	,	kIx,	,
Kaarma	Kaarm	k1gMnSc4	Kaarm
a	a	k8xC	a
Kahutsi	Kahutse	k1gFnSc4	Kahutse
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnSc4d1	dosahující
průměru	průměr	k1gInSc2	průměr
kruhu	kruh	k1gInSc2	kruh
110	[number]	k4	110
až	až	k9	až
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
výšky	výška	k1gFnSc2	výška
zachovaných	zachovaný	k2eAgInPc2d1	zachovaný
valů	val	k1gInPc2	val
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jezera	jezero	k1gNnSc2	jezero
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
jezer	jezero	k1gNnPc2	jezero
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
buď	buď	k8xC	buď
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
oddělením	oddělení	k1gNnSc7	oddělení
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tektonického	tektonický	k2eAgInSc2d1	tektonický
zdvihu	zdvih	k1gInSc2	zdvih
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
na	na	k7c6	na
Tagamõ	Tagamõ	k1gFnSc6	Tagamõ
poloostrově	poloostrov	k1gInSc6	poloostrov
(	(	kIx(	(
<g/>
Tagamõ	Tagamõ	k1gMnSc1	Tagamõ
poolsaar	poolsaar	k1gMnSc1	poolsaar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významné	významný	k2eAgNnSc1d1	významné
množství	množství	k1gNnSc1	množství
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
rovněž	rovněž	k9	rovněž
u	u	k7c2	u
jihovýchodního	jihovýchodní	k2eAgNnSc2d1	jihovýchodní
a	a	k8xC	a
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
jezer	jezero	k1gNnPc2	jezero
se	se	k3xPyFc4	se
často	často	k6eAd1	často
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
jejich	jejich	k3xOp3gNnPc6	jejich
označeních	označení	k1gNnPc6	označení
jako	jako	k8xC	jako
záliv	záliv	k1gInSc1	záliv
(	(	kIx(	(
<g/>
laht	laht	k1gInSc1	laht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zátoka	zátoka	k1gFnSc1	zátoka
(	(	kIx(	(
<g/>
abajas	abajas	k1gInSc1	abajas
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
meri	meri	k1gNnSc1	meri
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
jezerem	jezero	k1gNnSc7	jezero
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
dvojjezero	dvojjezero	k1gNnSc1	dvojjezero
Mullutu-Suurlaht	Mullutu-Suurlahta	k1gFnPc2	Mullutu-Suurlahta
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
14,4	[number]	k4	14,4
km2	km2	k4	km2
nedaleko	nedaleko	k7c2	nedaleko
Kuressaare	Kuressaar	k1gInSc5	Kuressaar
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jezer	jezero	k1gNnPc2	jezero
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
má	mít	k5eAaImIp3nS	mít
lehce	lehko	k6eAd1	lehko
slanou	slaný	k2eAgFnSc4d1	slaná
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
některá	některý	k3yIgFnSc1	některý
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
vysychají	vysychat	k5eAaImIp3nP	vysychat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejkrásnější	krásný	k2eAgNnSc4d3	nejkrásnější
jezero	jezero	k1gNnSc4	jezero
ostrova	ostrov	k1gInSc2	ostrov
bývá	bývat	k5eAaImIp3nS	bývat
označováno	označovat	k5eAaImNgNnS	označovat
členité	členitý	k2eAgNnSc1d1	členité
Medvědí	medvědí	k2eAgNnSc1d1	medvědí
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Karujärv	Karujärv	k1gInSc1	Karujärv
<g/>
)	)	kIx)	)
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
3,3	[number]	k4	3,3
km2	km2	k4	km2
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
lesích	les	k1gInPc6	les
Západaoostrovní	Západaoostrovní	k2eAgFnSc2d1	Západaoostrovní
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příroda	příroda	k1gFnSc1	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnSc2	geologie
===	===	k?	===
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
vápenci	vápenec	k1gInPc7	vápenec
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
také	také	k9	také
ledovcovými	ledovcový	k2eAgFnPc7d1	ledovcová
a	a	k8xC	a
mořskými	mořský	k2eAgFnPc7d1	mořská
usazeninami	usazenina	k1gFnPc7	usazenina
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
nížinný	nížinný	k2eAgInSc1d1	nížinný
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
bažinatý	bažinatý	k2eAgMnSc1d1	bažinatý
<g/>
.	.	kIx.	.
</s>
<s>
Půdy	půda	k1gFnPc1	půda
jsou	být	k5eAaImIp3nP	být
chudé	chudý	k2eAgMnPc4d1	chudý
<g/>
,	,	kIx,	,
štěrkovité	štěrkovitý	k2eAgFnPc1d1	štěrkovitá
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
produktech	produkt	k1gInPc6	produkt
zvětrávaných	zvětrávaný	k2eAgInPc2d1	zvětrávaný
vápenců	vápenec	k1gInPc2	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
jsou	být	k5eAaImIp3nP	být
naleziště	naleziště	k1gNnSc4	naleziště
dolomitů	dolomit	k1gInPc2	dolomit
(	(	kIx(	(
<g/>
Kaarma	Kaarma	k1gFnSc1	Kaarma
<g/>
)	)	kIx)	)
a	a	k8xC	a
vápenců	vápenec	k1gInPc2	vápenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
bohaté	bohatý	k2eAgNnSc1d1	bohaté
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
80	[number]	k4	80
%	%	kIx~	%
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
120	[number]	k4	120
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
chráněných	chráněný	k2eAgFnPc2d1	chráněná
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
%	%	kIx~	%
ostrova	ostrov	k1gInSc2	ostrov
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgFnSc2d1	tvořená
hlavně	hlavně	k9	hlavně
borovicí	borovice	k1gFnSc7	borovice
lesní	lesní	k2eAgFnSc2d1	lesní
a	a	k8xC	a
jalovcem	jalovec	k1gInSc7	jalovec
obecným	obecný	k2eAgInSc7d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bylin	bylina	k1gFnPc2	bylina
je	být	k5eAaImIp3nS	být
nejvzácnější	vzácný	k2eAgMnSc1d3	nejvzácnější
endemitický	endemitický	k2eAgMnSc1d1	endemitický
rhinanthus	rhinanthus	k1gMnSc1	rhinanthus
osiliensis	osiliensis	k1gFnSc2	osiliensis
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
Saaremaa	Saaremaa	k1gFnSc1	Saaremaa
robirohu	robiroh	k1gInSc2	robiroh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roste	růst	k5eAaImIp3nS	růst
tu	tu	k6eAd1	tu
také	také	k9	také
35	[number]	k4	35
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
36	[number]	k4	36
estonských	estonský	k2eAgInPc2d1	estonský
druhů	druh	k1gInPc2	druh
orchidejí	orchidea	k1gFnPc2	orchidea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
bohatá	bohatý	k2eAgFnSc1d1	bohatá
je	být	k5eAaImIp3nS	být
i	i	k9	i
zvířena	zvířena	k1gFnSc1	zvířena
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
kvůli	kvůli	k7c3	kvůli
oddělenosti	oddělenost	k1gFnSc3	oddělenost
souše	souš	k1gFnSc2	souš
zaostává	zaostávat	k5eAaImIp3nS	zaostávat
za	za	k7c4	za
pevninskou	pevninský	k2eAgFnSc4d1	pevninská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
tuleňů	tuleň	k1gMnPc2	tuleň
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nejtypičtější	typický	k2eAgMnSc1d3	nejtypičtější
je	být	k5eAaImIp3nS	být
tuleň	tuleň	k1gMnSc1	tuleň
kuželozubý	kuželozubý	k2eAgMnSc1d1	kuželozubý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
mořského	mořský	k2eAgNnSc2d1	mořské
ptactva	ptactvo	k1gNnSc2	ptactvo
<g/>
,	,	kIx,	,
ostrovem	ostrov	k1gInSc7	ostrov
rovněž	rovněž	k9	rovněž
procházejí	procházet	k5eAaImIp3nP	procházet
linie	linie	k1gFnPc1	linie
ptačího	ptačí	k2eAgInSc2d1	ptačí
tahu	tah	k1gInSc2	tah
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
možno	možno	k6eAd1	možno
setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
jinde	jinde	k6eAd1	jinde
vzácnými	vzácný	k2eAgInPc7d1	vzácný
druhy	druh	k1gInPc7	druh
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
berneška	berneška	k1gFnSc1	berneška
tmavá	tmavý	k2eAgFnSc1d1	tmavá
nebo	nebo	k8xC	nebo
kajka	kajka	k1gFnSc1	kajka
mořská	mořský	k2eAgFnSc1d1	mořská
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
hojní	hojnit	k5eAaImIp3nS	hojnit
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
a	a	k8xC	a
rys	rys	k1gMnSc1	rys
ostrovid	ostrovid	k1gMnSc1	ostrovid
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
kvůli	kvůli	k7c3	kvůli
omezenosti	omezenost	k1gFnSc3	omezenost
ostrova	ostrov	k1gInSc2	ostrov
vzácnější	vzácný	k2eAgFnSc1d2	vzácnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Západoostrovní	Západoostrovní	k2eAgFnSc2d1	Západoostrovní
vysočiny	vysočina	k1gFnSc2	vysočina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Viidumäe	Viidumäe	k1gNnSc2	Viidumäe
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Kuressaare	Kuressaar	k1gMnSc5	Kuressaar
v	v	k7c6	v
Kaali	Kaal	k1gMnSc6	Kaal
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
chráněný	chráněný	k2eAgInSc1d1	chráněný
přírodní	přírodní	k2eAgInSc1d1	přírodní
výtvor	výtvor	k1gInSc1	výtvor
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
kráterů	kráter	k1gInPc2	kráter
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
meteoritu	meteorit	k1gInSc2	meteorit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osídlení	osídlení	k1gNnSc2	osídlení
==	==	k?	==
</s>
</p>
<p>
<s>
Saaremaa	Saaremaa	k6eAd1	Saaremaa
správně	správně	k6eAd1	správně
náleží	náležet	k5eAaImIp3nP	náležet
do	do	k7c2	do
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
většinu	většina	k1gFnSc4	většina
rozlohy	rozloha	k1gFnSc2	rozloha
též	též	k9	též
sama	sám	k3xTgMnSc4	sám
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
33	[number]	k4	33
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
necelá	celý	k2eNgFnSc1d1	necelá
polovina	polovina	k1gFnSc1	polovina
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kuressaare	Kuressaar	k1gMnSc5	Kuressaar
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Saaremaa	Saaremaa	k1gFnSc1	Saaremaa
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
hustou	hustý	k2eAgFnSc4d1	hustá
síť	síť	k1gFnSc4	síť
silnic	silnice	k1gFnPc2	silnice
(	(	kIx(	(
<g/>
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
3	[number]	k4	3
100	[number]	k4	100
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jen	jen	k9	jen
menšina	menšina	k1gFnSc1	menšina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
asfaltový	asfaltový	k2eAgInSc4d1	asfaltový
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
čtyřkilometrovou	čtyřkilometrový	k2eAgFnSc7d1	čtyřkilometrová
silniční	silniční	k2eAgFnSc7d1	silniční
hrází	hráz	k1gFnSc7	hráz
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
ostrovem	ostrov	k1gInSc7	ostrov
Muhu	Muhus	k1gInSc2	Muhus
<g/>
,	,	kIx,	,
a	a	k8xC	a
přes	přes	k7c4	přes
něj	on	k3xPp3gMnSc4	on
trajektem	trajekt	k1gInSc7	trajekt
mezi	mezi	k7c7	mezi
Kuivastu	Kuivast	k1gInSc6	Kuivast
a	a	k8xC	a
Virtsu	Virts	k1gInSc6	Virts
i	i	k8xC	i
s	s	k7c7	s
estonskou	estonský	k2eAgFnSc7d1	Estonská
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
ostrově	ostrov	k1gInSc6	ostrov
Saaremaa	Saarema	k1gInSc2	Saarema
jsou	být	k5eAaImIp3nP	být
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
přístavy	přístav	k1gInPc7	přístav
Roomassaare	Roomassaar	k1gMnSc5	Roomassaar
(	(	kIx(	(
<g/>
Kuressaarský	Kuressaarský	k2eAgInSc1d1	Kuressaarský
přístav	přístav	k1gInSc1	přístav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nasva	Nasva	k1gFnSc1	Nasva
a	a	k8xC	a
Mõ	Mõ	k1gFnSc1	Mõ
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
a	a	k8xC	a
Veere	Veer	k1gInSc5	Veer
a	a	k8xC	a
Triigi	Triigi	k1gNnPc1	Triigi
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
čtyři	čtyři	k4xCgMnPc1	čtyři
slouží	sloužit	k5eAaImIp3nP	sloužit
též	též	k6eAd1	též
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
provozu	provoz	k1gInSc3	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
letiště	letiště	k1gNnSc1	letiště
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Roomassaare	Roomassaar	k1gMnSc5	Roomassaar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
ostrova	ostrov	k1gInSc2	ostrov
Saaremaa	Saaremaum	k1gNnSc2	Saaremaum
začala	začít	k5eAaPmAgFnS	začít
podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
již	již	k9	již
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
osmi	osm	k4xCc7	osm
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
po	po	k7c6	po
ústupu	ústup	k1gInSc6	ústup
pevninského	pevninský	k2eAgInSc2d1	pevninský
ledovce	ledovec	k1gInSc2	ledovec
oblast	oblast	k1gFnSc1	oblast
stala	stát	k5eAaPmAgFnS	stát
obyvatelnou	obyvatelný	k2eAgFnSc7d1	obyvatelná
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
poloze	poloha	k1gFnSc3	poloha
zároveň	zároveň	k6eAd1	zároveň
dobře	dobře	k6eAd1	dobře
dostupné	dostupný	k2eAgFnPc1d1	dostupná
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
chráněné	chráněný	k2eAgFnPc4d1	chráněná
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
díky	díky	k7c3	díky
příznivému	příznivý	k2eAgNnSc3d1	příznivé
přímořskému	přímořský	k2eAgNnSc3d1	přímořské
klimatu	klima	k1gNnSc3	klima
pak	pak	k6eAd1	pak
Saaremaa	Saaremaa	k1gFnSc1	Saaremaa
po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
nejhustěji	husto	k6eAd3	husto
osídleným	osídlený	k2eAgNnPc3d1	osídlené
baltským	baltský	k2eAgNnPc3d1	Baltské
územím	území	k1gNnPc3	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
ostrova	ostrov	k1gInSc2	ostrov
tvořily	tvořit	k5eAaImAgInP	tvořit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
baltofinské	baltofinský	k2eAgInPc1d1	baltofinský
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc7	jejichž
přímými	přímý	k2eAgInPc7d1	přímý
pokrevními	pokrevní	k2eAgInPc7d1	pokrevní
i	i	k8xC	i
kulturními	kulturní	k2eAgMnPc7d1	kulturní
potomky	potomek	k1gMnPc7	potomek
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dnešní	dnešní	k2eAgMnPc1d1	dnešní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jim	on	k3xPp3gInPc3	on
vládla	vládnout	k5eAaImAgFnS	vládnout
vlastní	vlastní	k2eAgFnSc1d1	vlastní
rodová	rodový	k2eAgFnSc1d1	rodová
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1206-1227	[number]	k4	1206-1227
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
těžkých	těžký	k2eAgFnPc6d1	těžká
a	a	k8xC	a
krvavých	krvavý	k2eAgFnPc6d1	krvavá
válkách	válka	k1gFnPc6	válka
ostrov	ostrov	k1gInSc4	ostrov
dobyt	dobyt	k2eAgInSc4d1	dobyt
řádem	řád	k1gInSc7	řád
mečových	mečový	k2eAgMnPc2d1	mečový
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
řádovým	řádový	k2eAgNnSc7d1	řádové
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
územím	území	k1gNnSc7	území
biskupství	biskupství	k1gNnSc2	biskupství
Ösel-Wiek	Ösel-Wiek	k1gInSc1	Ösel-Wiek
<g/>
.	.	kIx.	.
</s>
<s>
Ostrované	ostrovan	k1gMnPc1	ostrovan
se	se	k3xPyFc4	se
podřizovali	podřizovat	k5eAaImAgMnP	podřizovat
cizí	cizí	k2eAgFnSc3d1	cizí
vládě	vláda	k1gFnSc3	vláda
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
nechutí	nechuť	k1gFnSc7	nechuť
<g/>
,	,	kIx,	,
následující	následující	k2eAgNnSc4d1	následující
století	století	k1gNnSc4	století
přineslo	přinést	k5eAaPmAgNnS	přinést
několik	několik	k4yIc1	několik
velkých	velká	k1gFnPc2	velká
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1343	[number]	k4	1343
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Povstání	povstání	k1gNnSc2	povstání
svatojiřské	svatojiřský	k2eAgFnSc2d1	Svatojiřská
noci	noc	k1gFnSc2	noc
podařilo	podařit	k5eAaPmAgNnS	podařit
Estoncům	Estonec	k1gMnPc3	Estonec
osvobodit	osvobodit	k5eAaPmF	osvobodit
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
druhé	druhý	k4xOgFnSc6	druhý
křižácké	křižácký	k2eAgFnSc6d1	křižácká
výpravě	výprava	k1gFnSc6	výprava
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
podřídit	podřídit	k5eAaPmF	podřídit
jej	on	k3xPp3gInSc4	on
opět	opět	k6eAd1	opět
nadvládě	nadvláda	k1gFnSc3	nadvláda
biskupa	biskup	k1gInSc2	biskup
a	a	k8xC	a
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1558	[number]	k4	1558
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
dobyvačné	dobyvačný	k2eAgNnSc1d1	dobyvačné
tažení	tažení	k1gNnSc1	tažení
do	do	k7c2	do
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
využívají	využívat	k5eAaPmIp3nP	využívat
i	i	k9	i
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
své	svůj	k3xOyFgFnPc4	svůj
državy	država	k1gFnPc4	država
<g/>
.	.	kIx.	.
</s>
<s>
Biskup	biskup	k1gMnSc1	biskup
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
zániku	zánik	k1gInSc2	zánik
řádu	řád	k1gInSc2	řád
jediným	jediný	k2eAgMnSc7d1	jediný
lenním	lenní	k2eAgMnSc7d1	lenní
pánem	pán	k1gMnSc7	pán
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1559	[number]	k4	1559
prodává	prodávat	k5eAaImIp3nS	prodávat
Dánsku	Dánsko	k1gNnSc3	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sedmileté	sedmiletý	k2eAgFnSc6d1	sedmiletá
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1565	[number]	k4	1565
<g/>
–	–	k?	–
<g/>
1570	[number]	k4	1570
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
o	o	k7c6	o
získání	získání	k1gNnSc6	získání
ostrova	ostrov	k1gInSc2	ostrov
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
i	i	k9	i
Švédové	Švéd	k1gMnPc1	Švéd
a	a	k8xC	a
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Dánsku	Dánsko	k1gNnSc3	Dánsko
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
podaří	podařit	k5eAaPmIp3nS	podařit
své	svůj	k3xOyFgNnSc4	svůj
panství	panství	k1gNnSc4	panství
obhájit	obhájit	k5eAaPmF	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
dánsko-švédská	dánsko-švédský	k2eAgFnSc1d1	dánsko-švédský
válka	válka	k1gFnSc1	válka
roku	rok	k1gInSc2	rok
1645	[number]	k4	1645
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
ostrova	ostrov	k1gInSc2	ostrov
ke	k	k7c3	k
Švédskému	švédský	k2eAgNnSc3d1	švédské
království	království	k1gNnSc3	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
severní	severní	k2eAgFnSc6d1	severní
válce	válka	k1gFnSc6	válka
získává	získávat	k5eAaImIp3nS	získávat
roku	rok	k1gInSc2	rok
1710	[number]	k4	1710
ostrov	ostrov	k1gInSc4	ostrov
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
a	a	k8xC	a
udrží	udržet	k5eAaPmIp3nS	udržet
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
rok	rok	k1gInSc4	rok
dobude	dobýt	k5eAaPmIp3nS	dobýt
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
pak	pak	k6eAd1	pak
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
ostrovem	ostrov	k1gInSc7	ostrov
získává	získávat	k5eAaImIp3nS	získávat
nově	nově	k6eAd1	nově
vznikající	vznikající	k2eAgFnSc1d1	vznikající
Estonská	estonský	k2eAgFnSc1d1	Estonská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
zahájil	zahájit	k5eAaPmAgInS	zahájit
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
okupaci	okupace	k1gFnSc4	okupace
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Saaremaa	Saaremaa	k1gFnSc1	Saaremaa
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
však	však	k9	však
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
válečných	válečný	k2eAgFnPc2d1	válečná
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
až	až	k8xS	až
listopadu	listopad	k1gInSc6	listopad
1944	[number]	k4	1944
obsadila	obsadit	k5eAaPmAgFnS	obsadit
ostrov	ostrov	k1gInSc4	ostrov
opět	opět	k6eAd1	opět
sovětská	sovětský	k2eAgNnPc1d1	sovětské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
okupovala	okupovat	k5eAaBmAgFnS	okupovat
až	až	k8xS	až
do	do	k7c2	do
obnovení	obnovení	k1gNnSc2	obnovení
estonské	estonský	k2eAgFnSc2d1	Estonská
samostatnosti	samostatnost	k1gFnSc2	samostatnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Saaremaa	Saarema	k1gInSc2	Saarema
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Ostrov	ostrov	k1gInSc1	ostrov
Saaremaa	Saarema	k1gInSc2	Saarema
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
<g/>
)	)	kIx)	)
Saaremské	Saaremský	k2eAgNnSc1d1	Saaremský
ptactvo	ptactvo	k1gNnSc1	ptactvo
</s>
</p>
<p>
<s>
Expedition	Expedition	k1gInSc1	Expedition
in	in	k?	in
Estonia	Estonium	k1gNnSc2	Estonium
1987	[number]	k4	1987
–	–	k?	–
Expedice	expedice	k1gFnSc1	expedice
Balt	Balt	k1gInSc1	Balt
1987	[number]	k4	1987
do	do	k7c2	do
Estonska	Estonsko	k1gNnSc2	Estonsko
/	/	kIx~	/
<g/>
CZ	CZ	kA	CZ
</s>
</p>
