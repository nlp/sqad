<s>
Saaremaa	Saaremaa	k1gFnSc1	Saaremaa
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Ösel	Ösel	k1gInSc1	Ösel
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
Ösel	Ösel	k1gInSc1	Ösel
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Osilia	Osilia	k1gFnSc1	Osilia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
estonských	estonský	k2eAgInPc2d1	estonský
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Saaremaa	Saaremaa	k6eAd1	Saaremaa
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Západoestonského	Západoestonský	k2eAgNnSc2d1	Západoestonský
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Hiiumaa	Hiiuma	k1gInSc2	Hiiuma
a	a	k8xC	a
západně	západně	k6eAd1	západně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Muhu	Muhus	k1gInSc2	Muhus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc1d1	zeměpisná
souřadnice	souřadnice	k1gFnPc1	souřadnice
středu	střed	k1gInSc2	střed
ostrova	ostrov	k1gInSc2	ostrov
jsou	být	k5eAaImIp3nP	být
58	[number]	k4	58
<g/>
°	°	k?	°
30	[number]	k4	30
<g/>
'	'	kIx"	'
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
22	[number]	k4	22
<g/>
°	°	k?	°
30	[number]	k4	30
<g/>
'	'	kIx"	'
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
2714	[number]	k4	2714
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
činí	činit	k5eAaImIp3nS	činit
největším	veliký	k2eAgInSc7d3	veliký
estonským	estonský	k2eAgInSc7d1	estonský
ostrovem	ostrov	k1gInSc7	ostrov
a	a	k8xC	a
po	po	k7c6	po
Sjæ	Sjæ	k1gFnSc6	Sjæ
<g/>
,	,	kIx,	,
Gotlandu	Gotlando	k1gNnSc6	Gotlando
a	a	k8xC	a
Fynu	Fyna	k1gFnSc4	Fyna
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>

