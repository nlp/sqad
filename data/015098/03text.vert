<s>
Daugava	Daugava	k1gFnSc1
</s>
<s>
Daugava	Daugava	k1gFnSc1
řeka	řeka	k1gFnSc1
v	v	k7c6
severním	severní	k2eAgInSc6d1
BěloruskuZákladní	BěloruskuZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
1020	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
87	#num#	k4
900	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
700	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Pramen	pramen	k1gInSc1
</s>
<s>
Valdajská	Valdajský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
56	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
19,95	19,95	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
32	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
30,08	30,08	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
221	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
Rižský	rižský	k2eAgInSc1d1
záliv	záliv	k1gInSc1
57	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
40	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
24	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
0	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
,	,	kIx,
Bělorusko	Bělorusko	k1gNnSc1
Bělorusko	Bělorusko	k1gNnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Vitebská	Vitebský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
Tverská	Tverský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Smolenská	Smolenský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Baltské	baltský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Povodí	povodí	k1gNnSc1
Daugavy	Daugava	k1gFnSc2
(	(	kIx(
<g/>
Bělorusko	Bělorusko	k1gNnSc1
48,14	48,14	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
34,38	34,38	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
16,11	16,11	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
Litva	Litva	k1gFnSc1
1,38	1,38	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
Estonsko	Estonsko	k1gNnSc1
~	~	kIx~
<g/>
0,01	0,01	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Daugava	Daugava	k1gFnSc1
<g/>
,	,	kIx,
také	také	k9
zvaná	zvaný	k2eAgFnSc1d1
Západní	západní	k2eAgFnSc1d1
Dvina	Dvina	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
lotyšsky	lotyšsky	k6eAd1
Daugava	Daugava	k1gFnSc1
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
З	З	k?
Д	Д	k?
<g/>
,	,	kIx,
bělorusky	bělorusky	k6eAd1
Д	Д	k?
<g/>
,	,	kIx,
německy	německy	k6eAd1
Düna	Dün	k2eAgFnSc1d1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Dźwina	Dźwin	k2eAgFnSc1d1
<g/>
,	,	kIx,
livonsky	livonsky	k6eAd1
Vē	Vē	k2eAgFnSc1d1
<g/>
,	,	kIx,
estonsky	estonsky	k6eAd1
Väina	Väien	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
pramenící	pramenící	k2eAgFnSc1d1
ve	v	k7c6
Valdajské	Valdajský	k2eAgFnSc6d1
vrchovině	vrchovina	k1gFnSc6
v	v	k7c6
Rusku	Rusko	k1gNnSc6
(	(	kIx(
<g/>
Tverská	Tverský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Smolenská	Smolenský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protéká	protékat	k5eAaImIp3nS
Běloruskem	Bělorusko	k1gNnSc7
(	(	kIx(
<g/>
Vitebská	Vitebský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
ústí	ústit	k5eAaImIp3nS
do	do	k7c2
Rižského	rižský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
v	v	k7c6
Lotyšsku	Lotyšsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
1020	#num#	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
352	#num#	k4
km	km	kA
v	v	k7c6
Lotyšsku	Lotyšsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
87	#num#	k4
900	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Daugava	Daugava	k1gFnSc1
v	v	k7c6
Rize	Riga	k1gFnSc6
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS
na	na	k7c6
Valdajské	Valdajský	k2eAgFnSc6d1
vysočině	vysočina	k1gFnSc6
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
221	#num#	k4
m	m	kA
západně	západně	k6eAd1
od	od	k7c2
pramenů	pramen	k1gInPc2
Volhy	Volha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristické	charakteristický	k2eAgInPc1d1
pro	pro	k7c4
reliéf	reliéf	k1gInSc4
povodí	povodí	k1gNnSc2
řeky	řeka	k1gFnSc2
je	být	k5eAaImIp3nS
střídání	střídání	k1gNnSc1
přibližně	přibližně	k6eAd1
stejně	stejně	k6eAd1
velkých	velký	k2eAgFnPc2d1
vysočin	vysočina	k1gFnPc2
(	(	kIx(
<g/>
Vitebská	Vitebský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Haradocká	Haradocký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Latgalská	Latgalský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Vidzemská	Vidzemský	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
širokých	široký	k2eAgFnPc2d1
nížin	nížina	k1gFnPc2
(	(	kIx(
<g/>
Polacká	Polacký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Východolotyšská	Východolotyšský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Středolotyšská	Středolotyšský	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celý	k2eAgNnSc1d1
povodí	povodí	k1gNnPc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nP
v	v	k7c6
oblasti	oblast	k1gFnSc6
nadbytečných	nadbytečný	k2eAgFnPc2d1
srážek	srážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeka	řeka	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
jako	jako	k9
mělký	mělký	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
protéká	protékat	k5eAaImIp3nS
jezera	jezero	k1gNnSc2
Dvinec	Dvinec	k1gInSc1
a	a	k8xC
Ochvat-Žadanje	Ochvat-Žadanje	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
rozšiřuje	rozšiřovat	k5eAaImIp3nS
až	až	k9
na	na	k7c4
15	#num#	k4
m	m	kA
a	a	k8xC
teče	téct	k5eAaImIp3nS
v	v	k7c6
hlubokém	hluboký	k2eAgNnSc6d1
říčním	říční	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
mezi	mezi	k7c7
strmými	strmý	k2eAgInPc7d1
břehy	břeh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
charakter	charakter	k1gInSc4
si	se	k3xPyFc3
dolina	dolina	k1gFnSc1
zachovává	zachovávat	k5eAaImIp3nS
téměř	téměř	k6eAd1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
toku	tok	k1gInSc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
150	#num#	k4
km	km	kA
od	od	k7c2
pramene	pramen	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c6
krátkém	krátký	k2eAgInSc6d1
úseku	úsek	k1gInSc6
málo	málo	k6eAd1
výrazná	výrazný	k2eAgFnSc1d1
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
protéká	protékat	k5eAaImIp3nS
jezery	jezero	k1gNnPc7
Luka	luka	k1gNnPc1
a	a	k8xC
Kalakuckým	Kalakucký	k2eAgNnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
v	v	k7c6
přímořské	přímořský	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
teče	téct	k5eAaImIp3nS
mezi	mezi	k7c7
nízkými	nízký	k2eAgInPc7d1
břehy	břeh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
korytě	koryto	k1gNnSc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
četné	četný	k2eAgFnPc1d1
peřeje	peřej	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
souvisejí	souviset	k5eAaImIp3nP
se	s	k7c7
shluky	shluk	k1gInPc7
balvanů	balvan	k1gInPc2
a	a	k8xC
s	s	k7c7
dolomity	dolomit	k1gInPc7
vystupujícími	vystupující	k2eAgInPc7d1
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
se	se	k3xPyFc4
řeka	řeka	k1gFnSc1
rozvětvuje	rozvětvovat	k5eAaImIp3nS
na	na	k7c4
ramena	rameno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
ústí	ústí	k1gNnSc2
představuje	představovat	k5eAaImIp3nS
erozivní	erozivní	k2eAgFnSc4d1
deltu	delta	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
přibližně	přibližně	k6eAd1
35	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
ústí	ústí	k1gNnSc6
do	do	k7c2
Rižského	rižský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
řeka	řeka	k1gFnSc1
pod	pod	k7c7
vodou	voda	k1gFnSc7
práh	práh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přítoky	přítok	k1gInPc1
</s>
<s>
zleva	zleva	k6eAd1
–	–	k?
Meža	Meža	k1gMnSc1
<g/>
,	,	kIx,
Kasplja	Kasplja	k1gMnSc1
<g/>
,	,	kIx,
Lučesa	Lučesa	k1gFnSc1
<g/>
,	,	kIx,
Ulla	Ulla	k1gFnSc1
<g/>
,	,	kIx,
Diena	Diena	k1gFnSc1
</s>
<s>
zprava	zprava	k6eAd1
–	–	k?
Toropa	Toropa	k1gFnSc1
<g/>
,	,	kIx,
Drissa	Drissa	k1gFnSc1
<g/>
,	,	kIx,
Saryanka	Saryanka	k1gFnSc1
<g/>
,	,	kIx,
Dubna	duben	k1gInSc2
<g/>
,	,	kIx,
Ajviekste	Ajviekst	k1gMnSc5
<g/>
,	,	kIx,
Ogre	Ogrus	k1gMnSc5
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
vody	voda	k1gFnSc2
je	být	k5eAaImIp3nS
smíšený	smíšený	k2eAgInSc1d1
s	s	k7c7
převahou	převaha	k1gFnSc7
sněhového	sněhový	k2eAgNnSc2d1
a	a	k8xC
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
podzemního	podzemní	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnPc4d3
vodnosti	vodnost	k1gFnPc4
dosahuje	dosahovat	k5eAaImIp3nS
na	na	k7c6
jaře	jaro	k1gNnSc6
od	od	k7c2
konce	konec	k1gInSc2
března	březen	k1gInSc2
do	do	k7c2
začátku	začátek	k1gInSc2
června	červen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
voda	voda	k1gFnSc1
opadá	opadat	k5eAaBmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
díky	díky	k7c3
dešťům	dešť	k1gInPc3
může	moct	k5eAaImIp3nS
docházet	docházet	k5eAaImF
k	k	k7c3
občasným	občasný	k2eAgFnPc3d1
povodním	povodeň	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
s	s	k7c7
příchodem	příchod	k1gInSc7
podzimu	podzim	k1gInSc2
častější	častý	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zimě	zima	k1gFnSc6
je	být	k5eAaImIp3nS
vody	voda	k1gFnPc4
v	v	k7c6
řece	řeka	k1gFnSc6
méně	málo	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
oteplení	oteplení	k1gNnSc2
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
povodním	povodeň	k1gFnPc3
i	i	k9
v	v	k7c6
tomto	tento	k3xDgNnSc6
ročním	roční	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
průtok	průtok	k1gInSc1
vody	voda	k1gFnSc2
v	v	k7c6
ústí	ústí	k1gNnSc6
činí	činit	k5eAaImIp3nS
700	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Ledová	ledový	k2eAgFnSc1d1
pokrývka	pokrývka	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
řece	řeka	k1gFnSc6
od	od	k7c2
prosince	prosinec	k1gInSc2
do	do	k7c2
března	březen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Osídlení	osídlení	k1gNnSc1
</s>
<s>
Na	na	k7c6
řece	řeka	k1gFnSc6
byly	být	k5eAaImAgInP
vybudovány	vybudovat	k5eAaPmNgInP
Pljavinská	Pljavinský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Ķ	Ķ	k2eAgFnSc1d1
a	a	k8xC
Rižská	rižský	k2eAgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodní	vodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
je	být	k5eAaImIp3nS
možná	možná	k9
na	na	k7c6
oddělených	oddělený	k2eAgInPc6d1
úsecích	úsek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeka	řeka	k1gFnSc1
je	být	k5eAaImIp3nS
spojena	spojit	k5eAaPmNgFnS
Berezinským	berezinský	k2eAgInSc7d1
kanálem	kanál	k1gInSc7
s	s	k7c7
řekou	řeka	k1gFnSc7
Berezinou	Berezina	k1gFnSc7
a	a	k8xC
s	s	k7c7
Dněprem	Dněpr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgInPc1d2
města	město	k1gNnPc4
<g/>
,	,	kIx,
jimiž	jenž	k3xRgInPc7
protéká	protékat	k5eAaImIp3nS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
Veliž	Veliž	k1gFnSc1
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vitebsk	Vitebsk	k1gInSc1
<g/>
,	,	kIx,
Polock	Polock	k1gInSc1
(	(	kIx(
<g/>
Bělorusko	Bělorusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Daugavpils	Daugavpils	k1gInSc1
<g/>
,	,	kIx,
Jē	Jē	k1gInSc1
a	a	k8xC
Riga	Riga	k1gFnSc1
(	(	kIx(
<g/>
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Riga	Riga	k1gFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
ústí	ústí	k1gNnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
námořním	námořní	k2eAgInSc7d1
přístavem	přístav	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Mezinárodní	mezinárodní	k2eAgNnSc1d1
povodí	povodí	k1gNnSc4
řek	řeka	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
Daugava	Daugava	k1gFnSc1
(	(	kIx(
<g/>
Estonsko	Estonsko	k1gNnSc1
není	být	k5eNaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
З	З	k?
Д	Д	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Daugava	Daugava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
291083	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4013207-9	4013207-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
90005158	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
3148933613054302462	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
90005158	#num#	k4
</s>
