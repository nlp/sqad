<s>
Daugava	Daugava	k1gFnSc1
<g/>
,	,	kIx,
také	také	k9
zvaná	zvaný	k2eAgFnSc1d1
Západní	západní	k2eAgFnSc1d1
Dvina	Dvina	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
lotyšsky	lotyšsky	k6eAd1
Daugava	Daugava	k1gFnSc1
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
З	З	k?
Д	Д	k?
<g/>
,	,	kIx,
bělorusky	bělorusky	k6eAd1
Д	Д	k?
<g/>
,	,	kIx,
německy	německy	k6eAd1
Düna	Dün	k2eAgFnSc1d1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Dźwina	Dźwin	k2eAgFnSc1d1
<g/>
,	,	kIx,
livonsky	livonsky	k6eAd1
Vē	Vē	k2eAgFnSc1d1
<g/>
,	,	kIx,
estonsky	estonsky	k6eAd1
Väina	Väien	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
pramenící	pramenící	k2eAgFnSc1d1
ve	v	k7c6
Valdajské	Valdajský	k2eAgFnSc6d1
vrchovině	vrchovina	k1gFnSc6
v	v	k7c6
Rusku	Rusko	k1gNnSc6
(	(	kIx(
<g/>
Tverská	Tverský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Smolenská	Smolenský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>