<s>
Seznam	seznam	k1gInSc1	seznam
deseti	deset	k4xCc2	deset
obvodů	obvod	k1gInPc2	obvod
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
36	[number]	k4	36
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nyní	nyní	k6eAd1	nyní
tvoří	tvořit	k5eAaImIp3nP	tvořit
území	území	k1gNnSc4	území
obvodu	obvod	k1gInSc2	obvod
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
má	mít	k5eAaImIp3nS	mít
57	[number]	k4	57
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spravovány	spravovat	k5eAaImNgFnP	spravovat
voleným	volený	k2eAgNnSc7d1	volené
zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
radou	rada	k1gMnSc7	rada
<g/>
,	,	kIx,	,
starostou	starosta	k1gMnSc7	starosta
a	a	k8xC	a
úřadem	úřad	k1gInSc7	úřad
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
