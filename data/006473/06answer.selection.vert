<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
inflace	inflace	k1gFnSc2	inflace
k	k	k7c3	k
deflaci	deflace	k1gFnSc3	deflace
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
poměrně	poměrně	k6eAd1	poměrně
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
jejímu	její	k3xOp3gInSc3	její
vzniku	vznik	k1gInSc3	vznik
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
obvykle	obvykle	k6eAd1	obvykle
snaží	snažit	k5eAaImIp3nS	snažit
preventivně	preventivně	k6eAd1	preventivně
bojovat	bojovat	k5eAaImF	bojovat
<g/>
;	;	kIx,	;
většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
krátkých	krátký	k2eAgNnPc6d1	krátké
časových	časový	k2eAgNnPc6d1	časové
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
