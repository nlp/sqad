<s>
Někteří	některý	k3yIgMnPc1
pozdější	pozdní	k2eAgMnPc1d2
členové	člen	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
vůdci	vůdce	k1gMnPc1
nacistů	nacista	k1gMnPc2
byli	být	k5eAaImAgMnP
členy	člen	k1gInPc4
Freikorps	Freikorpsa	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Ernsta	Ernst	k1gMnSc2
Röhma	Röhm	k1gMnSc2
<g/>
,	,	kIx,
hlavy	hlava	k1gFnSc2
SA	SA	kA
<g/>
,	,	kIx,
či	či	k8xC
Rudolfa	Rudolf	k1gMnSc2
Hösse	Höss	k1gMnSc2
<g/>
,	,	kIx,
velitele	velitel	k1gMnSc2
vyhlazovacího	vyhlazovací	k2eAgInSc2d1
tábora	tábor	k1gInSc2
Auschwitz	Auschwitza	k1gFnPc2
-	-	kIx~
Birkenau	Birkenaa	k1gFnSc4
u	u	k7c2
polské	polský	k2eAgFnSc2d1
Osvětimi	Osvětim	k1gFnSc2
<g/>
.	.	kIx.
</s>