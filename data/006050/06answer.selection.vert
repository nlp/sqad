<s>
Kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
sůl	sůl	k1gFnSc1	sůl
normálně	normálně	k6eAd1	normálně
prodávaná	prodávaný	k2eAgFnSc1d1	prodávaná
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
obchodech	obchod	k1gInPc6	obchod
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
bývá	bývat	k5eAaImIp3nS	bývat
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
jodizovaná	jodizovaný	k2eAgFnSc1d1	jodizovaná
–	–	k?	–
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
přidáno	přidán	k2eAgNnSc1d1	Přidáno
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
jódu	jód	k1gInSc2	jód
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jodidu	jodid	k1gInSc2	jodid
draselného	draselný	k2eAgInSc2d1	draselný
nebo	nebo	k8xC	nebo
jodičnanu	jodičnan	k1gInSc2	jodičnan
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
