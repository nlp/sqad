<s>
Zápisky	zápiska	k1gFnPc1	zápiska
o	o	k7c6	o
56	[number]	k4	56
vybraných	vybraný	k2eAgInPc6d1	vybraný
případech	případ	k1gInPc6	případ
pořídil	pořídit	k5eAaPmAgMnS	pořídit
jeho	jeho	k3xOp3gNnSc4	jeho
největší	veliký	k2eAgMnSc1d3	veliký
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
doktor	doktor	k1gMnSc1	doktor
John	John	k1gMnSc1	John
Watson	Watson	k1gMnSc1	Watson
<g/>
,	,	kIx,	,
2	[number]	k4	2
případy	případ	k1gInPc7	případ
popsal	popsat	k5eAaPmAgMnS	popsat
detektiv	detektiv	k1gMnSc1	detektiv
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
psány	psán	k2eAgInPc1d1	psán
ve	v	k7c4	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
