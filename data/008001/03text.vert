<s>
Sherlock	Sherlock	k1gInSc1	Sherlock
Holmes	Holmesa	k1gFnPc2	Holmesa
je	být	k5eAaImIp3nS	být
literární	literární	k2eAgFnSc1d1	literární
postava	postava	k1gFnSc1	postava
soukromého	soukromý	k2eAgMnSc2d1	soukromý
detektiva	detektiv	k1gMnSc2	detektiv
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
románů	román	k1gInPc2	román
sira	sir	k1gMnSc2	sir
Arthura	Arthur	k1gMnSc2	Arthur
Conana	Conan	k1gMnSc2	Conan
Doyla	Doylo	k1gNnSc2	Doylo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
případy	případ	k1gInPc1	případ
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
dílo	dílo	k1gNnSc1	dílo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
4	[number]	k4	4
romány	román	k1gInPc4	román
a	a	k8xC	a
56	[number]	k4	56
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
uveřejňovány	uveřejňovat	k5eAaImNgFnP	uveřejňovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1887	[number]	k4	1887
až	až	k9	až
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
z	z	k7c2	z
románů	román	k1gInPc2	román
později	pozdě	k6eAd2	pozdě
ožily	ožít	k5eAaPmAgFnP	ožít
ještě	ještě	k9	ještě
mnohokrát	mnohokrát	k6eAd1	mnohokrát
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
jiných	jiný	k2eAgMnPc2d1	jiný
autorů	autor	k1gMnPc2	autor
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
,	,	kIx,	,
filmovém	filmový	k2eAgNnSc6d1	filmové
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
,	,	kIx,	,
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
i	i	k8xC	i
na	na	k7c6	na
počítačových	počítačový	k2eAgInPc6d1	počítačový
monitorech	monitor	k1gInPc6	monitor
(	(	kIx(	(
<g/>
české	český	k2eAgFnSc2d1	Česká
holmesovské	holmesovský	k2eAgFnSc2d1	holmesovská
pastiše	pastiš	k1gFnSc2	pastiš
napsal	napsat	k5eAaBmAgMnS	napsat
například	například	k6eAd1	například
Rudolf	Rudolf	k1gMnSc1	Rudolf
Čechura	Čechura	k1gMnSc1	Čechura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sherlock	Sherlock	k1gMnSc1	Sherlock
Holmes	Holmes	k1gMnSc1	Holmes
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
mnoho	mnoho	k4c1	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
holmesologů	holmesolog	k1gMnPc2	holmesolog
<g/>
,	,	kIx,	,
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sherlock	Sherlock	k1gMnSc1	Sherlock
Holmes	Holmes	k1gMnSc1	Holmes
bydlel	bydlet	k5eAaImAgMnS	bydlet
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Baker	Baker	k1gMnSc1	Baker
Street	Street	k1gMnSc1	Street
221	[number]	k4	221
<g/>
b	b	k?	b
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
podnájmu	podnájem	k1gInSc6	podnájem
u	u	k7c2	u
paní	paní	k1gFnSc2	paní
Hudsonové	Hudsonová	k1gFnSc2	Hudsonová
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
muzeum	muzeum	k1gNnSc1	muzeum
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
<g/>
.	.	kIx.	.
</s>
<s>
Zápisky	zápiska	k1gFnPc1	zápiska
o	o	k7c6	o
56	[number]	k4	56
vybraných	vybraný	k2eAgInPc6d1	vybraný
případech	případ	k1gInPc6	případ
pořídil	pořídit	k5eAaPmAgMnS	pořídit
jeho	jeho	k3xOp3gNnSc4	jeho
největší	veliký	k2eAgMnSc1d3	veliký
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
doktor	doktor	k1gMnSc1	doktor
John	John	k1gMnSc1	John
Watson	Watson	k1gMnSc1	Watson
<g/>
,	,	kIx,	,
2	[number]	k4	2
případy	případ	k1gInPc7	případ
popsal	popsat	k5eAaPmAgMnS	popsat
detektiv	detektiv	k1gMnSc1	detektiv
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
psány	psán	k2eAgInPc1d1	psán
ve	v	k7c4	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
jen	jen	k9	jen
o	o	k7c4	o
nepatrný	patrný	k2eNgInSc4d1	patrný
zlomek	zlomek	k1gInSc4	zlomek
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
Holmesových	Holmesový	k2eAgInPc2d1	Holmesový
případů	případ	k1gInPc2	případ
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
bratra	bratr	k1gMnSc4	bratr
Mycrofta	Mycroft	k1gMnSc4	Mycroft
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
7	[number]	k4	7
let	léto	k1gNnPc2	léto
starší	starý	k2eAgInSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Holmes	Holmes	k1gMnSc1	Holmes
nebyl	být	k5eNaImAgMnS	být
nikdy	nikdy	k6eAd1	nikdy
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
znám	znát	k5eAaImIp1nS	znát
svým	svůj	k3xOyFgInSc7	svůj
rezervovaným	rezervovaný	k2eAgInSc7d1	rezervovaný
postojem	postoj	k1gInSc7	postoj
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
–	–	k?	–
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Irenou	Irena	k1gFnSc7	Irena
Adlerovou	Adlerová	k1gFnSc7	Adlerová
z	z	k7c2	z
příběhu	příběh	k1gInSc2	příběh
Skandál	skandál	k1gInSc1	skandál
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
největší	veliký	k2eAgFnSc7d3	veliký
zálibou	záliba	k1gFnSc7	záliba
bylo	být	k5eAaImAgNnS	být
kouření	kouření	k1gNnSc1	kouření
dýmky	dýmka	k1gFnSc2	dýmka
a	a	k8xC	a
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
atributy	atribut	k1gInPc7	atribut
byly	být	k5eAaImAgFnP	být
lupa	lupa	k1gFnSc1	lupa
<g/>
,	,	kIx,	,
kostkovaná	kostkovaný	k2eAgFnSc1d1	kostkovaná
čepice	čepice	k1gFnSc1	čepice
a	a	k8xC	a
chemické	chemický	k2eAgInPc1d1	chemický
pokusy	pokus	k1gInPc1	pokus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Poslední	poslední	k2eAgInSc4d1	poslední
případ	případ	k1gInSc4	případ
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
největším	veliký	k2eAgMnSc7d3	veliký
rivalem	rival	k1gMnSc7	rival
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
byl	být	k5eAaImAgMnS	být
profesor	profesor	k1gMnSc1	profesor
Moriarty	Moriarta	k1gFnSc2	Moriarta
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
příběhu	příběh	k1gInSc6	příběh
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
unavený	unavený	k2eAgInSc1d1	unavený
A.	A.	kA	A.
Conan	Conan	k1gInSc1	Conan
Doyle	Doyle	k1gNnSc2	Doyle
původně	původně	k6eAd1	původně
nechal	nechat	k5eAaPmAgInS	nechat
zemřít	zemřít	k5eAaPmF	zemřít
utonutím	utonutí	k1gNnSc7	utonutí
ve	v	k7c6	v
vodopádu	vodopád	k1gInSc6	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
však	však	k9	však
pod	pod	k7c7	pod
velkým	velký	k2eAgInSc7d1	velký
tlakem	tlak	k1gInSc7	tlak
veřejnosti	veřejnost	k1gFnSc2	veřejnost
napsal	napsat	k5eAaPmAgInS	napsat
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sherlock	Sherlock	k1gMnSc1	Sherlock
Holmes	Holmes	k1gMnSc1	Holmes
umí	umět	k5eAaImIp3nS	umět
převézt	převézt	k5eAaPmF	převézt
i	i	k9	i
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
povídku	povídka	k1gFnSc4	povídka
Prázdný	prázdný	k2eAgInSc4d1	prázdný
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
Holmes	Holmes	k1gMnSc1	Holmes
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Šedesát	šedesát	k4xCc4	šedesát
případů	případ	k1gInPc2	případ
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
kánonu	kánon	k1gInSc2	kánon
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
A.	A.	kA	A.
Conan	Conan	k1gInSc4	Conan
Doyle	Doyle	k1gNnSc2	Doyle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgFnPc1d1	uvedena
zkratky	zkratka	k1gFnPc1	zkratka
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
anglických	anglický	k2eAgInPc2d1	anglický
názvů	název	k1gInPc2	název
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
holmesologům	holmesolog	k1gMnPc3	holmesolog
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
snadné	snadný	k2eAgFnSc3d1	snadná
a	a	k8xC	a
rychlé	rychlý	k2eAgFnSc3d1	rychlá
identifikaci	identifikace	k1gFnSc3	identifikace
<g/>
.	.	kIx.	.
</s>
<s>
Případy	případ	k1gInPc1	případ
jsou	být	k5eAaImIp3nP	být
seřazeny	seřadit	k5eAaPmNgInP	seřadit
podle	podle	k7c2	podle
data	datum	k1gNnSc2	datum
první	první	k4xOgFnSc2	první
publikace	publikace	k1gFnSc2	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
v	v	k7c6	v
šarlatové	šarlatový	k2eAgFnSc6d1	šarlatová
(	(	kIx(	(
<g/>
A	a	k8xC	a
Study	stud	k1gInPc1	stud
in	in	k?	in
Scarlet	Scarlet	k1gInSc1	Scarlet
<g/>
,	,	kIx,	,
STUD	stud	k1gInSc1	stud
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
Podpis	podpis	k1gInSc1	podpis
čtyř	čtyři	k4xCgMnPc2	čtyři
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Sign	signum	k1gNnPc2	signum
of	of	k?	of
Four	Four	k1gMnSc1	Four
<g/>
,	,	kIx,	,
SIGN	signum	k1gNnPc2	signum
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
Skandál	skandál	k1gInSc1	skandál
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
A	a	k9	a
Scandal	Scandal	k1gFnSc1	Scandal
in	in	k?	in
Bohemia	bohemia	k1gFnSc1	bohemia
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
SCAN	SCAN	kA	SCAN
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Spolek	spolek	k1gInSc1	spolek
ryšavců	ryšavec	k1gMnPc2	ryšavec
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Red-Headed	Red-Headed	k1gInSc4	Red-Headed
League	Leagu	k1gMnSc2	Leagu
<g/>
,	,	kIx,	,
REDH	REDH	kA	REDH
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Případ	případ	k1gInSc1	případ
totožnosti	totožnost	k1gFnSc2	totožnost
(	(	kIx(	(
<g/>
A	a	k9	a
Case	Case	k1gFnSc1	Case
of	of	k?	of
Identity	identita	k1gFnSc2	identita
<g/>
,	,	kIx,	,
IDEN	IDEN	kA	IDEN
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Záhada	záhada	k1gFnSc1	záhada
Boscombského	Boscombský	k2eAgNnSc2d1	Boscombský
údolí	údolí	k1gNnSc2	údolí
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Boscombe	Boscomb	k1gMnSc5	Boscomb
Valley	Valley	k1gInPc7	Valley
<g />
.	.	kIx.	.
</s>
<s>
Mystery	Myster	k1gInPc1	Myster
<g/>
,	,	kIx,	,
BOSC	BOSC	kA	BOSC
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Pět	pět	k4xCc1	pět
pomerančových	pomerančový	k2eAgNnPc2d1	pomerančové
jadérek	jadérko	k1gNnPc2	jadérko
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Five	Fiv	k1gFnSc2	Fiv
Orange	Orange	k1gFnPc2	Orange
Pips	Pips	k1gInSc1	Pips
<g/>
,	,	kIx,	,
FIVE	FIVE	kA	FIVE
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Ohyzdný	ohyzdný	k2eAgMnSc1d1	ohyzdný
žebrák	žebrák	k1gMnSc1	žebrák
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
with	with	k1gMnSc1	with
the	the	k?	the
Twisted	Twisted	k1gInSc1	Twisted
Lip	lípa	k1gFnPc2	lípa
<g/>
,	,	kIx,	,
TWIS	TWIS	kA	TWIS
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Modrá	modrat	k5eAaImIp3nS	modrat
karbunkule	karbunkul	k1gInSc5	karbunkul
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
<g />
.	.	kIx.	.
</s>
<s>
of	of	k?	of
the	the	k?	the
Blue	Blu	k1gMnSc2	Blu
Carbuncle	Carbuncl	k1gMnSc2	Carbuncl
<g/>
,	,	kIx,	,
BLUE	BLUE	kA	BLUE
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Strakatý	strakatý	k2eAgInSc4d1	strakatý
pás	pás	k1gInSc4	pás
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Speckled	Speckled	k1gInSc1	Speckled
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
SPEC	spéct	k5eAaPmRp2nSwK	spéct
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Inženýrův	inženýrův	k2eAgInSc4d1	inženýrův
palec	palec	k1gInSc4	palec
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Engineer	Engineer	k1gInSc1	Engineer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Thumb	Thumb	k1gInSc1	Thumb
<g/>
,	,	kIx,	,
ENGR	ENGR	kA	ENGR
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Urozený	urozený	k2eAgMnSc1d1	urozený
ženich	ženich	k1gMnSc1	ženich
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Noble	Noble	k1gNnSc2	Noble
Bachelor	Bachelora	k1gFnPc2	Bachelora
<g/>
,	,	kIx,	,
NOBL	NOBL	kA	NOBL
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Berylová	berylový	k2eAgFnSc1d1	berylový
korunka	korunka	k1gFnSc1	korunka
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Beryl	beryl	k1gInSc1	beryl
Coronet	Coronet	k1gInSc1	Coronet
<g/>
,	,	kIx,	,
BERY	BERY	kA	BERY
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
měděných	měděný	k2eAgInPc2d1	měděný
buků	buk	k1gInPc2	buk
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Copper	Copper	k1gMnSc1	Copper
Beeches	Beeches	k1gMnSc1	Beeches
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
COPP	COPP	kA	COPP
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
lysáček	lysáček	k1gMnSc1	lysáček
(	(	kIx(	(
<g/>
Silver	Silver	k1gMnSc1	Silver
Blaze	blaze	k6eAd1	blaze
<g/>
,	,	kIx,	,
SILV	Silva	k1gFnPc2	Silva
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Lepenková	lepenkový	k2eAgFnSc1d1	lepenková
krabice	krabice	k1gFnSc1	krabice
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Cardboard	Cardboard	k1gInSc1	Cardboard
Box	box	k1gInSc1	box
<g/>
,	,	kIx,	,
CARD	CARD	kA	CARD
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Žlutá	žlutat	k5eAaImIp3nS	žlutat
tvář	tvář	k1gFnSc4	tvář
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Yellow	Yellow	k1gMnSc1	Yellow
Face	Fac	k1gFnSc2	Fac
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
YELL	YELL	kA	YELL
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Makléřův	makléřův	k2eAgMnSc1d1	makléřův
úředník	úředník	k1gMnSc1	úředník
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Stockbroker	Stockbroker	k1gInSc1	Stockbroker
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Clerk	Clerk	k1gInSc1	Clerk
<g/>
,	,	kIx,	,
STOC	STOC	kA	STOC
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Gloria	Gloria	k1gFnSc1	Gloria
Scottová	Scottová	k1gFnSc1	Scottová
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Gloria	Gloria	k1gFnSc1	Gloria
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
GLOR	GLOR	kA	GLOR
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Musgraveský	Musgraveský	k2eAgInSc1d1	Musgraveský
rituál	rituál	k1gInSc1	rituál
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Musgrave	Musgrav	k1gInSc5	Musgrav
Ritual	Ritual	k1gMnSc1	Ritual
<g/>
,	,	kIx,	,
MUSG	MUSG	kA	MUSG
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Reigateské	Reigateský	k2eAgNnSc1d1	Reigateský
panstvo	panstvo	k1gNnSc1	panstvo
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Reigate	Reigat	k1gMnSc5	Reigat
Squire	Squir	k1gMnSc5	Squir
<g/>
,	,	kIx,	,
REIG	REIG	kA	REIG
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Mrzák	mrzák	k1gMnSc1	mrzák
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Crooked	Crooked	k1gMnSc1	Crooked
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
CROO	CROO	kA	CROO
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Domácí	domácí	k2eAgMnSc1d1	domácí
pacient	pacient	k1gMnSc1	pacient
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Resident	resident	k1gMnSc1	resident
Patient	Patient	k1gMnSc1	Patient
<g/>
,	,	kIx,	,
RESI	RESI	kA	RESI
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Řecký	řecký	k2eAgMnSc1d1	řecký
tlumočník	tlumočník	k1gMnSc1	tlumočník
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Greek	Greek	k1gInSc1	Greek
Interpreter	interpreter	k1gInSc1	interpreter
<g/>
,	,	kIx,	,
GREE	GREE	kA	GREE
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Námořní	námořní	k2eAgFnSc1d1	námořní
smlouva	smlouva	k1gFnSc1	smlouva
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Naval	navalit	k5eAaPmRp2nS	navalit
Treaty	Treata	k1gFnSc2	Treata
<g/>
,	,	kIx,	,
NAVA	NAVA	k?	NAVA
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgInSc1d1	poslední
případ	případ	k1gInSc1	případ
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Problem	Probl	k1gMnSc7	Probl
<g/>
,	,	kIx,	,
FINA	Fina	k1gFnSc1	Fina
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Pes	pes	k1gMnSc1	pes
baskervillský	baskervillský	k2eAgMnSc1d1	baskervillský
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Hound	Hound	k1gMnSc1	Hound
of	of	k?	of
the	the	k?	the
Baskervilles	Baskervilles	k1gInSc1	Baskervilles
<g/>
,	,	kIx,	,
HOUN	HOUN	kA	HOUN
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
–	–	k?	–
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
Prázdný	prázdný	k2eAgInSc4d1	prázdný
dům	dům	k1gInSc4	dům
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
<g />
.	.	kIx.	.
</s>
<s>
of	of	k?	of
the	the	k?	the
Empty	Empta	k1gFnSc2	Empta
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
EMPT	EMPT	kA	EMPT
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
Stavitel	stavitel	k1gMnSc1	stavitel
z	z	k7c2	z
Norwoodu	Norwood	k1gInSc2	Norwood
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Norwood	Norwood	k1gInSc1	Norwood
Builder	Builder	k1gInSc1	Builder
<g/>
,	,	kIx,	,
NORW	NORW	kA	NORW
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
Tančící	tančící	k2eAgFnSc2d1	tančící
figurky	figurka	k1gFnSc2	figurka
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Dancing	dancing	k1gInSc4	dancing
Men	Men	k1gMnSc2	Men
<g/>
,	,	kIx,	,
DANC	DANC	kA	DANC
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
Osamělá	osamělý	k2eAgFnSc1d1	osamělá
<g />
.	.	kIx.	.
</s>
<s>
cyklistka	cyklistka	k1gFnSc1	cyklistka
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Solitary	Solitara	k1gFnSc2	Solitara
Cyclist	Cyclist	k1gMnSc1	Cyclist
<g/>
,	,	kIx,	,
SOLI	sůl	k1gFnSc6	sůl
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
Škola	škola	k1gFnSc1	škola
v	v	k7c4	v
Priory	prior	k1gMnPc4	prior
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Priory	prior	k1gMnPc7	prior
School	School	k1gInSc4	School
<g/>
,	,	kIx,	,
PRIO	PRIO	kA	PRIO
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Černý	Černý	k1gMnSc1	Černý
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
Black	Black	k1gMnSc1	Black
Peter	Peter	k1gMnSc1	Peter
<g/>
,	,	kIx,	,
BLAC	BLAC	kA	BLAC
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Charles	Charles	k1gMnSc1	Charles
Augustus	Augustus	k1gMnSc1	Augustus
Milverton	Milverton	k1gInSc1	Milverton
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Augustus	Augustus	k1gMnSc1	Augustus
Milverton	Milverton	k1gInSc1	Milverton
<g/>
,	,	kIx,	,
CHAS	chasa	k1gFnPc2	chasa
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Šest	šest	k4xCc1	šest
Napoleonů	Napoleon	k1gMnPc2	Napoleon
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Six	Six	k1gFnSc3	Six
Napoleons	Napoleons	k1gInSc4	Napoleons
<g/>
,	,	kIx,	,
SIXN	SIXN	kA	SIXN
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
studenti	student	k1gMnPc1	student
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Three	Three	k1gNnSc2	Three
Students	Studentsa	k1gFnPc2	Studentsa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
STU	sto	k4xCgNnSc3	sto
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Zlatý	zlatý	k2eAgInSc4d1	zlatý
skřipec	skřipec	k1gInSc4	skřipec
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Golden	Goldna	k1gFnPc2	Goldna
Pince-Nez	Pince-Nez	k1gMnSc1	Pince-Nez
<g/>
,	,	kIx,	,
GOLD	GOLD	kA	GOLD
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Zmizelý	zmizelý	k2eAgMnSc1d1	zmizelý
hráč	hráč	k1gMnSc1	hráč
ragby	ragby	k1gNnSc2	ragby
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Missing	Missing	k1gInSc1	Missing
Three-Quarter	Three-Quartra	k1gFnPc2	Three-Quartra
<g/>
,	,	kIx,	,
MISS	miss	k1gFnPc2	miss
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Opatské	opatský	k2eAgNnSc1d1	opatské
sídlo	sídlo	k1gNnSc1	sídlo
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Abbey	Abbea	k1gMnSc2	Abbea
Grange	Grang	k1gMnSc2	Grang
<g/>
,	,	kIx,	,
ABBE	ABBE	kA	ABBE
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Druhá	druhý	k4xOgFnSc1	druhý
skvrna	skvrna	k1gFnSc1	skvrna
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Second	Second	k1gMnSc1	Second
Stain	Stain	k1gMnSc1	Stain
<g/>
,	,	kIx,	,
SECO	SECO	kA	SECO
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Vila	vít	k5eAaImAgFnS	vít
Vistárie	vistárie	k1gFnSc1	vistárie
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
Wisteria	Wisterium	k1gNnSc2	Wisterium
Lodge	Lodge	k1gFnPc2	Lodge
<g/>
,	,	kIx,	,
WIST	WIST	kA	WIST
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Bruce-Partingtonovy	Bruce-Partingtonův	k2eAgInPc1d1	Bruce-Partingtonův
dokumenty	dokument	k1gInPc1	dokument
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Bruce-Partington	Bruce-Partington	k1gInSc1	Bruce-Partington
Plans	Plans	k1gInSc1	Plans
<g/>
,	,	kIx,	,
BRUC	BRUC	kA	BRUC
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
Ďáblovo	ďáblův	k2eAgNnSc1d1	ďáblovo
kopyto	kopyto	k1gNnSc1	kopyto
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Devil	Devil	k1gInSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Foot	Foot	k1gInSc1	Foot
<g/>
,	,	kIx,	,
DEVI	DEVI	kA	DEVI
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
Rudý	rudý	k2eAgInSc4d1	rudý
kruh	kruh	k1gInSc4	kruh
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Red	Red	k1gMnSc1	Red
Circle	Circle	k1gFnSc2	Circle
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
REDC	REDC	kA	REDC
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
Nezvěstná	zvěstný	k2eNgFnSc1d1	nezvěstná
šlechtična	šlechtična	k1gFnSc1	šlechtična
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Disappearance	Disappearance	k1gFnSc2	Disappearance
of	of	k?	of
Lady	Lada	k1gFnSc2	Lada
Frances	Francesa	k1gFnPc2	Francesa
Carfax	Carfax	k1gInSc1	Carfax
<g/>
,	,	kIx,	,
LADY	lady	k1gFnSc1	lady
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
Umírající	umírající	k1gMnSc1	umírající
detektiv	detektiv	k1gMnSc1	detektiv
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Dying	Dying	k1gInSc1	Dying
Detective	Detectiv	k1gInSc5	Detectiv
<g/>
,	,	kIx,	,
DYIN	DYIN	kA	DYIN
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
Údolí	údolí	k1gNnSc6	údolí
strachu	strach	k1gInSc2	strach
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Valley	Vallea	k1gFnSc2	Vallea
<g />
.	.	kIx.	.
</s>
<s>
of	of	k?	of
Fear	Fear	k1gInSc1	Fear
<g/>
,	,	kIx,	,
VALL	VALL	kA	VALL
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
–	–	k?	–
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgFnSc1d1	poslední
poklona	poklona	k1gFnSc1	poklona
(	(	kIx(	(
<g/>
His	his	k1gNnSc1	his
Last	Lasta	k1gFnPc2	Lasta
Bow	Bow	k1gFnPc2	Bow
<g/>
,	,	kIx,	,
LAST	LAST	kA	LAST
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
Mazarinův	Mazarinův	k2eAgInSc4d1	Mazarinův
drahokam	drahokam	k1gInSc4	drahokam
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Mazarin	Mazarin	k1gInSc1	Mazarin
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
MAZA	MAZA	kA	MAZA
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Záhada	záhada	k1gFnSc1	záhada
na	na	k7c6	na
Thorském	Thorský	k2eAgInSc6d1	Thorský
mostě	most	k1gInSc6	most
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Problem	Probl	k1gInSc7	Probl
of	of	k?	of
Thor	Thor	k1gInSc1	Thor
Bridge	Bridge	k1gInSc1	Bridge
<g/>
,	,	kIx,	,
THOR	THOR	kA	THOR
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Šplhající	šplhající	k2eAgMnSc1d1	šplhající
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Creeping	Creeping	k1gInSc1	Creeping
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
CREE	CREE	kA	CREE
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
Upír	upír	k1gMnSc1	upír
v	v	k7c6	v
Sussexu	Sussex	k1gInSc6	Sussex
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Sussex	Sussex	k1gInSc1	Sussex
Vampire	Vampir	k1gMnSc5	Vampir
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
SUSS	SUSS	kA	SUSS
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
Tři	tři	k4xCgFnPc1	tři
Garridebové	Garridebová	k1gFnPc1	Garridebová
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Three	Three	k1gNnSc2	Three
Garridebs	Garridebsa	k1gFnPc2	Garridebsa
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
GAR	GAR	kA	GAR
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
Vznešený	vznešený	k2eAgMnSc1d1	vznešený
klient	klient	k1gMnSc1	klient
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Illustrious	Illustrious	k1gMnSc1	Illustrious
Client	Client	k1gMnSc1	Client
<g/>
,	,	kIx,	,
ILLU	ILLU	kA	ILLU
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
tří	tři	k4xCgInPc2	tři
štítů	štít	k1gInPc2	štít
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Three	Three	k1gNnSc2	Three
Gables	Gablesa	k1gFnPc2	Gablesa
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
GAB	GAB	kA	GAB
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Voják	voják	k1gMnSc1	voják
bílý	bílý	k1gMnSc1	bílý
jako	jako	k8xC	jako
stěna	stěna	k1gFnSc1	stěna
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Blanched	Blanched	k1gInSc1	Blanched
Soldier	Soldira	k1gFnPc2	Soldira
<g/>
,	,	kIx,	,
BLAN	blána	k1gFnPc2	blána
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Lví	lví	k2eAgFnSc1d1	lví
hříva	hříva	k1gFnSc1	hříva
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Lion	Lion	k1gMnSc1	Lion
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Mane	manout	k5eAaImIp3nS	manout
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
LION	Lion	k1gMnSc1	Lion
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Barvíř	barvíř	k1gMnSc1	barvíř
na	na	k7c4	na
penzi	penze	k1gFnSc4	penze
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Retired	Retired	k1gMnSc1	Retired
Colourman	Colourman	k1gMnSc1	Colourman
<g/>
,	,	kIx,	,
RETI	RETI	kA	RETI
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Podnájemnice	podnájemnice	k1gFnSc2	podnájemnice
v	v	k7c6	v
závoji	závoj	k1gInSc6	závoj
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
the	the	k?	the
Veiled	Veiled	k1gMnSc1	Veiled
Lodger	Lodger	k1gMnSc1	Lodger
<g/>
,	,	kIx,	,
VEIL	VEIL	kA	VEIL
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
Na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Shoscombe	Shoscomb	k1gInSc5	Shoscomb
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
Shoscombe	Shoscomb	k1gInSc5	Shoscomb
Old	Olda	k1gFnPc2	Olda
Place	plac	k1gInSc6	plac
<g/>
,	,	kIx,	,
SHOS	SHOS	kA	SHOS
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
Všechny	všechen	k3xTgFnPc1	všechen
ilustrace	ilustrace	k1gFnPc1	ilustrace
Doyleových	Doyleův	k2eAgInPc2d1	Doyleův
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
listu	list	k1gInSc6	list
The	The	k1gFnSc1	The
Strand	strand	k1gInSc1	strand
Magazine	Magazin	k1gInSc5	Magazin
podepsané	podepsaný	k2eAgFnPc4d1	podepsaná
monogramem	monogram	k1gInSc7	monogram
SP	SP	kA	SP
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
skotský	skotský	k2eAgMnSc1d1	skotský
kreslíř	kreslíř	k1gMnSc1	kreslíř
Sidney	Sidnea	k1gFnSc2	Sidnea
Paget	Paget	k1gMnSc1	Paget
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejslavnějšího	slavný	k2eAgMnSc4d3	nejslavnější
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
ilustrátorů	ilustrátor	k1gMnPc2	ilustrátor
společnosti	společnost	k1gFnSc2	společnost
Canon	Canon	kA	Canon
<g/>
.	.	kIx.	.
</s>
<s>
Paget	Paget	k1gMnSc1	Paget
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
(	(	kIx(	(
<g/>
104	[number]	k4	104
kreseb	kresba	k1gFnPc2	kresba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
(	(	kIx(	(
<g/>
97	[number]	k4	97
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Psa	pes	k1gMnSc4	pes
Baskervilského	baskervilský	k2eAgInSc2d1	baskervilský
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
a	a	k8xC	a
Návrat	návrat	k1gInSc1	návrat
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
(	(	kIx(	(
<g/>
95	[number]	k4	95
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
Strand	strand	k1gInSc4	strand
Magazine	Magazin	k1gInSc5	Magazin
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
Pinocoteca	Pinocotecus	k1gMnSc2	Pinocotecus
Holmesiana	Holmesian	k1gMnSc2	Holmesian
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
umělecký	umělecký	k2eAgInSc4d1	umělecký
editor	editor	k1gInSc4	editor
listu	list	k1gInSc2	list
Strand	strand	k1gInSc1	strand
Magazine	Magazin	k1gInSc5	Magazin
chtěl	chtít	k5eAaImAgMnS	chtít
původně	původně	k6eAd1	původně
o	o	k7c4	o
ilustrace	ilustrace	k1gFnPc4	ilustrace
požádat	požádat	k5eAaPmF	požádat
Pagetova	Pagetův	k2eAgMnSc4d1	Pagetův
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
Waltera	Walter	k1gMnSc4	Walter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
provize	provize	k1gFnSc1	provize
byla	být	k5eAaImAgFnS	být
údajně	údajně	k6eAd1	údajně
omylem	omylem	k6eAd1	omylem
zaslána	zaslat	k5eAaPmNgFnS	zaslat
Sidneymu	Sidneym	k1gInSc3	Sidneym
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
Waltera	Walter	k1gMnSc4	Walter
jako	jako	k9	jako
model	model	k1gInSc4	model
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
postavy	postava	k1gFnSc2	postava
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Sidneyho	Sidney	k1gMnSc2	Sidney
Pageta	Paget	k1gMnSc2	Paget
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1908	[number]	k4	1908
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Walter	Walter	k1gMnSc1	Walter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
jeden	jeden	k4xCgInSc1	jeden
příběh	příběh	k1gInSc1	příběh
ve	v	k7c4	v
Strand	strand	k1gInSc4	strand
Magazinu	Magazin	k1gInSc2	Magazin
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
umírajícího	umírající	k2eAgMnSc2d1	umírající
detektiva	detektiv	k1gMnSc2	detektiv
<g/>
.	.	kIx.	.
</s>
