<s>
Po	po	k7c6
náhlém	náhlý	k2eAgNnSc6d1
úmrtí	úmrtí	k1gNnSc6
Thomase	Thomas	k1gMnSc2
Clifforda	Clifford	k1gMnSc2
v	v	k7c6
říjnu	říjen	k1gInSc6
1673	#num#	k4
byl	být	k5eAaImAgInS
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
prvním	první	k4xOgInSc7
lordem	lord	k1gMnSc7
pokladu	poklad	k1gInSc2
(	(	kIx(
<g/>
Lord	lord	k1gMnSc1
High	High	k1gMnSc1
Treasurer	Treasurer	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1673	#num#	k4
<g/>
–	–	k?
<g/>
1679	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
postupným	postupný	k2eAgInSc7d1
rozkladem	rozklad	k1gInSc7
mocenského	mocenský	k2eAgNnSc2d1
uskupení	uskupení	k1gNnSc2
cabal	cabal	k1gMnSc1
byl	být	k5eAaImAgMnS
zhruba	zhruba	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1675	#num#	k4
de	de	k?
facto	facto	k1gNnSc1
prvním	první	k4xOgMnSc7
ministrem	ministr	k1gMnSc7
<g/>
.	.	kIx.
</s>