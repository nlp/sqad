<s>
Thomas	Thomas	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Leedsu	Leeds	k1gInSc2
</s>
<s>
Thomas	Thomas	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Leedsu	Leeds	k1gInSc2
Narození	narození	k1gNnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1632	#num#	k4
<g/>
York	York	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1712	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
80	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1712	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
80	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Easton	Easton	k1gInSc1
Neston	Neston	k1gInSc1
Povolání	povolání	k1gNnSc5
</s>
<s>
politik	politik	k1gMnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Podvazkový	podvazkový	k2eAgInSc1d1
řád	řád	k1gInSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Bridget	Bridget	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
Duchess	Duchessa	k1gFnPc2
of	of	k?
Leeds	Leedsa	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
1651	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Peregrine	Peregrin	k1gInSc5
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
LeedsuLady	LeedsuLada	k1gFnSc2
Catherine	Catherin	k1gInSc5
OsborneLady	OsborneLada	k1gFnSc2
Bridget	Bridget	k1gMnSc1
OsborneEdward	OsborneEdward	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
Viscount	Viscount	k1gInSc1
LatimerLady	LatimerLada	k1gFnSc2
Anne	Ann	k1gMnSc2
OsborneLady	OsborneLada	k1gFnSc2
Sophia	Sophius	k1gMnSc2
OsborneMartha	OsborneMarth	k1gMnSc2
Osborne	Osborn	k1gInSc5
Rodiče	rodič	k1gMnPc1
</s>
<s>
Sir	sir	k1gMnSc1
Edward	Edward	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
st	st	kA
Baronet	baroneta	k1gFnPc2
a	a	k8xC
Anne	Anne	k1gFnSc4
Walmesley	Walmeslea	k1gFnSc2
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Peregrine	Peregrin	k1gInSc5
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
3	#num#	k4
<g/>
rd	rd	k?
Duke	Duke	k1gInSc1
of	of	k?
Leeds	Leeds	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Lady	lady	k1gFnSc1
Mary	Mary	k1gFnSc1
Osborne	Osborn	k1gInSc5
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
vnoučata	vnouče	k1gNnPc1
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
High	High	k1gMnSc1
Sheriff	Sheriff	k1gMnSc1
of	of	k?
Yorkshire	Yorkshir	k1gInSc5
(	(	kIx(
<g/>
od	od	k7c2
1662	#num#	k4
<g/>
)	)	kIx)
<g/>
Lord	lord	k1gMnSc1
Lieutenant	Lieutenant	k1gMnSc1
of	of	k?
the	the	k?
West	West	k2eAgInSc4d1
Riding	Riding	k1gInSc4
of	of	k?
Yorkshire	Yorkshir	k1gInSc5
(	(	kIx(
<g/>
1674	#num#	k4
<g/>
–	–	k?
<g/>
1679	#num#	k4
<g/>
)	)	kIx)
<g/>
Lord	lord	k1gMnSc1
Lieutenant	Lieutenant	k1gMnSc1
of	of	k?
the	the	k?
West	West	k2eAgInSc4d1
Riding	Riding	k1gInSc4
of	of	k?
Yorkshire	Yorkshir	k1gInSc5
(	(	kIx(
<g/>
1689	#num#	k4
<g/>
–	–	k?
<g/>
1699	#num#	k4
<g/>
)	)	kIx)
<g/>
Member	Member	k1gInSc1
of	of	k?
the	the	k?
1661-79	1661-79	k4
Parliamentposlanec	Parliamentposlanec	k1gInSc1
anglického	anglický	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Thomas	Thomas	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Leedsu	Leeds	k1gInSc2
(	(	kIx(
<g/>
Thomas	Thomas	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
st	st	kA
Duke	Duke	k1gFnPc2
of	of	k?
Leeds	Leedsa	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
st	st	kA
Marquess	Marquessa	k1gFnPc2
of	of	k?
Carmarthen	Carmarthna	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
st	st	kA
Earl	earl	k1gMnSc1
of	of	k?
Danby	Danba	k1gFnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
st	st	kA
Viscount	Viscounta	k1gFnPc2
Osborne	Osborn	k1gInSc5
and	and	k?
Latimer	Latimero	k1gNnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
st	st	kA
Baron	baron	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
2	#num#	k4
<g/>
nd	nd	k?
Baronet	baronet	k1gMnSc1
Osborne	Osborn	k1gInSc5
of	of	k?
Kiveton	Kiveton	k1gInSc4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1632	#num#	k4
<g/>
,	,	kIx,
Kiveton	Kiveton	k1gInSc1
Park	park	k1gInSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1712	#num#	k4
<g/>
,	,	kIx,
Easton	Easton	k1gInSc1
Neston	Neston	k1gInSc4
House	house	k1gNnSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
anglický	anglický	k2eAgMnSc1d1
státník	státník	k1gMnSc1
<g/>
,	,	kIx,
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
osobností	osobnost	k1gFnPc2
anglických	anglický	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastával	zastávat	k5eAaImAgMnS
řadu	řada	k1gFnSc4
státních	státní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1673	#num#	k4
<g/>
–	–	k?
<g/>
1679	#num#	k4
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
ministrem	ministr	k1gMnSc7
<g/>
,	,	kIx,
uplatnil	uplatnit	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
jako	jako	k9
diplomat	diplomat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byl	být	k5eAaImAgMnS
vězněn	věznit	k5eAaImNgMnS
za	za	k7c4
velezradu	velezrada	k1gFnSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
aktivně	aktivně	k6eAd1
podpořil	podpořit	k5eAaPmAgMnS
detronizaci	detronizace	k1gFnSc4
Stuartovců	Stuartovec	k1gMnPc2
a	a	k8xC
nástup	nástup	k1gInSc1
Vilém	Viléma	k1gFnPc2
Oranžského	oranžský	k2eAgMnSc2d1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgInS
vysokého	vysoký	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
ve	v	k7c6
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
postavení	postavení	k1gNnSc2
příslušníka	příslušník	k1gMnSc4
venkovské	venkovský	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
se	se	k3xPyFc4
přes	přes	k7c4
všechny	všechen	k3xTgInPc4
tituly	titul	k1gInPc4
šlechtické	šlechtický	k2eAgFnSc2d1
hierarchie	hierarchie	k1gFnSc2
(	(	kIx(
<g/>
baron	baron	k1gMnSc1
1673	#num#	k4
<g/>
,	,	kIx,
vikomt	vikomt	k1gMnSc1
1673	#num#	k4
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
1674	#num#	k4
<g/>
,	,	kIx,
markýz	markýz	k1gMnSc1
1689	#num#	k4
<g/>
)	)	kIx)
vypracoval	vypracovat	k5eAaPmAgInS
až	až	k9
k	k	k7c3
titulu	titul	k1gInSc3
vévody	vévoda	k1gMnSc2
(	(	kIx(
<g/>
1694	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
jeho	jeho	k3xOp3gNnSc4
potomstvo	potomstvo	k1gNnSc4
užívalo	užívat	k5eAaImAgNnS
až	až	k9
do	do	k7c2
vymření	vymření	k1gNnSc2
rodu	rod	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
drobné	drobný	k2eAgFnSc2d1
statkářské	statkářský	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
obchodnické	obchodnický	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
synem	syn	k1gMnSc7
poslance	poslanec	k1gMnSc2
Sira	sir	k1gMnSc2
Edwarda	Edward	k1gMnSc2
Osborna	Osborna	k1gFnSc1
(	(	kIx(
<g/>
1596	#num#	k4
<g/>
–	–	k?
<g/>
1647	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
v	v	k7c6
Yorku	York	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1647	#num#	k4
po	po	k7c6
otci	otec	k1gMnSc6
zdědil	zdědit	k5eAaPmAgInS
titul	titul	k1gInSc1
baroneta	baronet	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1649	#num#	k4
<g/>
–	–	k?
<g/>
1650	#num#	k4
absolvoval	absolvovat	k5eAaPmAgInS
kavalírskou	kavalírský	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
po	po	k7c6
Francii	Francie	k1gFnSc6
a	a	k8xC
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
veřejného	veřejný	k2eAgInSc2d1
života	život	k1gInSc2
vstoupil	vstoupit	k5eAaPmAgMnS
po	po	k7c6
restauraci	restaurace	k1gFnSc6
monarchie	monarchie	k1gFnSc2
pod	pod	k7c7
patronátem	patronát	k1gInSc7
2	#num#	k4
<g/>
.	.	kIx.
vévody	vévoda	k1gMnPc4
z	z	k7c2
Buckinghamu	Buckingham	k1gInSc2
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gInPc1
statky	statek	k1gInPc1
v	v	k7c6
hrabství	hrabství	k1gNnSc6
York	York	k1gInSc4
sousedily	sousedit	k5eAaImAgInP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1660	#num#	k4
zastával	zastávat	k5eAaImAgInS
funkce	funkce	k1gFnSc2
ve	v	k7c6
správě	správa	k1gFnSc6
hrabství	hrabství	k1gNnSc2
York	York	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1665	#num#	k4
<g/>
–	–	k?
<g/>
1673	#num#	k4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
Dolní	dolní	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1668	#num#	k4
<g/>
–	–	k?
<g/>
1673	#num#	k4
byl	být	k5eAaImAgMnS
prezidentem	prezident	k1gMnSc7
úřadu	úřad	k1gInSc2
námořního	námořní	k2eAgInSc2d1
pokladu	poklad	k1gInSc2
(	(	kIx(
<g/>
Navy	Navy	k?
Treasurer	Treasurer	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1672	#num#	k4
<g/>
–	–	k?
<g/>
1674	#num#	k4
členem	člen	k1gMnSc7
výboru	výbor	k1gInSc2
Tajné	tajný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
pro	pro	k7c4
obchod	obchod	k1gInSc4
a	a	k8xC
kolonie	kolonie	k1gFnPc4
<g/>
,	,	kIx,
působil	působit	k5eAaImAgMnS
také	také	k9
na	na	k7c6
admiralitě	admiralita	k1gFnSc6
(	(	kIx(
<g/>
1673	#num#	k4
<g/>
–	–	k?
<g/>
1679	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
náhlém	náhlý	k2eAgNnSc6d1
úmrtí	úmrtí	k1gNnSc6
Thomase	Thomas	k1gMnSc2
Clifforda	Clifford	k1gMnSc2
v	v	k7c6
říjnu	říjen	k1gInSc6
1673	#num#	k4
byl	být	k5eAaImAgInS
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
prvním	první	k4xOgInSc7
lordem	lord	k1gMnSc7
pokladu	poklad	k1gInSc2
(	(	kIx(
<g/>
Lord	lord	k1gMnSc1
High	High	k1gMnSc1
Treasurer	Treasurer	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1673	#num#	k4
<g/>
–	–	k?
<g/>
1679	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
postupným	postupný	k2eAgInSc7d1
rozkladem	rozklad	k1gInSc7
mocenského	mocenský	k2eAgNnSc2d1
uskupení	uskupení	k1gNnSc2
cabal	cabal	k1gMnSc1
byl	být	k5eAaImAgMnS
zhruba	zhruba	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1675	#num#	k4
de	de	k?
facto	facto	k1gNnSc1
prvním	první	k4xOgMnSc7
ministrem	ministr	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
lordem	lord	k1gMnSc7
<g/>
–	–	k?
<g/>
místodržitelem	místodržitel	k1gMnSc7
v	v	k7c6
hrabství	hrabství	k1gNnSc6
York	York	k1gInSc1
(	(	kIx(
<g/>
1674	#num#	k4
<g/>
–	–	k?
<g/>
1679	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
členem	člen	k1gMnSc7
výboru	výbor	k1gInSc2
Tajné	tajný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
pro	pro	k7c4
obchod	obchod	k1gInSc4
a	a	k8xC
kolonie	kolonie	k1gFnSc2
(	(	kIx(
<g/>
1675	#num#	k4
<g/>
–	–	k?
<g/>
1679	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1673	#num#	k4
byl	být	k5eAaImAgMnS
jako	jako	k9
baron	baron	k1gMnSc1
Osborne	Osborn	k1gInSc5
povolán	povolat	k5eAaPmNgMnS
do	do	k7c2
Sněmovny	sněmovna	k1gFnSc2
lordů	lord	k1gMnPc2
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
vikomta	vikomt	k1gMnSc2
Latimera	Latimer	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
měl	mít	k5eAaImAgMnS
největší	veliký	k2eAgInSc4d3
vliv	vliv	k1gInSc4
na	na	k7c4
domácí	domácí	k2eAgFnSc4d1
a	a	k8xC
zahraniční	zahraniční	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
postavení	postavení	k1gNnSc1
vysoce	vysoce	k6eAd1
převyšovalo	převyšovat	k5eAaImAgNnS
pozice	pozice	k1gFnPc4
všech	všecek	k3xTgMnPc2
ostatních	ostatní	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	své	k1gNnSc4
postavení	postavení	k1gNnSc2
stvrdil	stvrdit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1678	#num#	k4
sňatkem	sňatek	k1gInSc7
nejstarší	starý	k2eAgFnPc4d3
dcery	dcera	k1gFnPc4
s	s	k7c7
nemanželským	manželský	k2eNgMnSc7d1
synem	syn	k1gMnSc7
Karla	Karel	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
hrabětem	hrabě	k1gMnSc7
z	z	k7c2
Plymouthu	Plymouth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1677	#num#	k4
získal	získat	k5eAaPmAgInS
Podvazkový	podvazkový	k2eAgInSc1d1
řád	řád	k1gInSc1
<g/>
,	,	kIx,
vynikl	vyniknout	k5eAaPmAgInS
v	v	k7c6
diplomatických	diplomatický	k2eAgNnPc6d1
jednáních	jednání	k1gNnPc6
s	s	k7c7
Francií	Francie	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
podezření	podezření	k1gNnSc4
z	z	k7c2
náklonnosti	náklonnost	k1gFnSc2
k	k	k7c3
Francouzům	Francouz	k1gMnPc3
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1679	#num#	k4
<g/>
–	–	k?
<g/>
1683	#num#	k4
vězněn	věznit	k5eAaImNgMnS
za	za	k7c4
velezradu	velezrada	k1gFnSc4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
když	když	k8xS
byl	být	k5eAaImAgMnS
konzervativec	konzervativec	k1gMnSc1
a	a	k8xC
stoupenec	stoupenec	k1gMnSc1
legitimity	legitimita	k1gFnSc2
stuartovského	stuartovský	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
,	,	kIx,
odmítl	odmítnout	k5eAaPmAgMnS
prokatolický	prokatolický	k2eAgInSc4d1
styl	styl	k1gInSc4
vlády	vláda	k1gFnSc2
Jakuba	Jakub	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
nakonec	nakonec	k6eAd1
aktivně	aktivně	k6eAd1
podpořil	podpořit	k5eAaPmAgMnS
slavnou	slavný	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1688	#num#	k4
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
signatářů	signatář	k1gMnPc2
dopisu	dopis	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vyzval	vyzvat	k5eAaPmAgMnS
Viléma	Vilém	k1gMnSc4
III	III	kA
<g/>
.	.	kIx.
k	k	k7c3
převzetí	převzetí	k1gNnSc3
vlády	vláda	k1gFnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1689	#num#	k4
byl	být	k5eAaImAgMnS
povýšen	povýšit	k5eAaPmNgMnS
na	na	k7c6
markýze	markýza	k1gFnSc6
z	z	k7c2
Carmarthenu	Carmarthen	k1gInSc2
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1689	#num#	k4
<g/>
–	–	k?
<g/>
1699	#num#	k4
byl	být	k5eAaImAgMnS
prezidentem	prezident	k1gMnSc7
Tajné	tajný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
se	se	k3xPyFc4
také	také	k9
uplatnil	uplatnit	k5eAaPmAgMnS
ve	v	k7c6
správě	správa	k1gFnSc6
hrabství	hrabství	k1gNnSc2
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Vilém	Vilém	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
bojoval	bojovat	k5eAaImAgMnS
v	v	k7c6
Irsku	Irsko	k1gNnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
předním	přední	k2eAgMnSc7d1
poradcem	poradce	k1gMnSc7
královny	královna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
vlastními	vlastní	k2eAgFnPc7d1
intrikami	intrika	k1gFnPc7
ale	ale	k8xC
škodil	škodit	k5eAaImAgMnS
svému	svůj	k3xOyFgMnSc3
postavení	postavení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1694	#num#	k4
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
vévody	vévoda	k1gMnSc2
z	z	k7c2
Leedsu	Leeds	k1gInSc2
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
nato	nato	k6eAd1
byl	být	k5eAaImAgInS
neoprávněně	oprávněně	k6eNd1
obviněn	obvinit	k5eAaPmNgInS
ze	z	k7c2
styků	styk	k1gInPc2
s	s	k7c7
jakobity	jakobita	k1gMnPc7
<g/>
,	,	kIx,
do	do	k7c2
veřejného	veřejný	k2eAgInSc2d1
života	život	k1gInSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
až	až	k9
po	po	k7c6
nástupu	nástup	k1gInSc6
královny	královna	k1gFnSc2
Anny	Anna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1711	#num#	k4
<g/>
–	–	k?
<g/>
1712	#num#	k4
byl	být	k5eAaImAgInS
nejvyšším	vysoký	k2eAgMnSc7d3
sudím	sudí	k1gMnSc7
v	v	k7c6
severních	severní	k2eAgInPc6d1
hrabstvích	hrabství	k1gNnPc6
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
dostal	dostat	k5eAaPmAgMnS
zpětně	zpětně	k6eAd1
vyplacenou	vyplacený	k2eAgFnSc4d1
rentu	renta	k1gFnSc4
z	z	k7c2
úřadů	úřad	k1gInPc2
zadržovanou	zadržovaný	k2eAgFnSc4d1
od	od	k7c2
roku	rok	k1gInSc2
1694	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1653	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Bridget	Bridget	k1gInSc4
Bertie	Bertie	k1gFnPc4
(	(	kIx(
<g/>
1629	#num#	k4
<g/>
–	–	k?
<g/>
1703	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
2	#num#	k4
<g/>
.	.	kIx.
hraběte	hrabě	k1gMnSc4
z	z	k7c2
Lindsey	Lindsea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
spolu	spolu	k6eAd1
sedm	sedm	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Edward	Edward	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
vikomt	vikomt	k1gMnSc1
Latimer	Latimer	k1gMnSc1
(	(	kIx(
<g/>
1655	#num#	k4
<g/>
–	–	k?
<g/>
1689	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zastával	zastávat	k5eAaImAgMnS
funkce	funkce	k1gFnPc4
u	u	k7c2
dvora	dvůr	k1gInSc2
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1677	#num#	k4
<g/>
–	–	k?
<g/>
1681	#num#	k4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
Dolní	dolní	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
však	však	k9
předčasně	předčasně	k6eAd1
a	a	k8xC
bez	bez	k7c2
potomstva	potomstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dědicem	dědic	k1gMnSc7
titulů	titul	k1gInPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mladší	mladý	k2eAgMnSc1d2
syn	syn	k1gMnSc1
Peregrine	Peregrin	k1gInSc5
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Leedsu	Leeds	k1gInSc2
(	(	kIx(
<g/>
1658	#num#	k4
<g/>
–	–	k?
<g/>
1729	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
admirálem	admirál	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dcera	dcera	k1gFnSc1
Bridget	Bridgeta	k1gFnPc2
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c4
nelegitimního	legitimní	k2eNgMnSc4d1
syna	syn	k1gMnSc4
Karla	Karel	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Charlese	Charles	k1gMnSc2
Fitzcharlese	Fitzcharlese	k1gFnSc1
<g/>
,	,	kIx,
hraběte	hrabě	k1gMnSc4
z	z	k7c2
Plymouthu	Plymouth	k1gInSc2
(	(	kIx(
<g/>
1657	#num#	k4
<g/>
–	–	k?
<g/>
1680	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Thomasův	Thomasův	k2eAgMnSc1d1
mladší	mladý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Charles	Charles	k1gMnSc1
Osborne	Osborn	k1gInSc5
(	(	kIx(
<g/>
1633	#num#	k4
<g/>
–	–	k?
<g/>
1719	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
dlouholetým	dlouholetý	k2eAgMnSc7d1
členem	člen	k1gMnSc7
Dolní	dolní	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
(	(	kIx(
<g/>
1677	#num#	k4
<g/>
–	–	k?
<g/>
1679	#num#	k4
<g/>
,	,	kIx,
1690	#num#	k4
<g/>
–	–	k?
<g/>
1701	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zastával	zastávat	k5eAaImAgMnS
funkce	funkce	k1gFnPc4
ve	v	k7c6
správě	správa	k1gFnSc6
hrabství	hrabství	k1gNnSc2
York	York	k1gInSc1
a	a	k8xC
nižší	nízký	k2eAgInPc1d2
státní	státní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1690	#num#	k4
<g/>
–	–	k?
<g/>
1701	#num#	k4
byl	být	k5eAaImAgInS
guvernérem	guvernér	k1gMnSc7
v	v	k7c6
Hullu	Hull	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KOVÁŘ	Kovář	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
:	:	kIx,
Stuartovská	stuartovský	k2eAgFnSc1d1
Anglie	Anglie	k1gFnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
ISBN	ISBN	kA
80-7277-059-4	80-7277-059-4	k4
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
15	#num#	k4
<g/>
.	.	kIx.
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1900	#num#	k4
(	(	kIx(
<g/>
reprint	reprint	k1gInSc1
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
785	#num#	k4
ISBN	ISBN	kA
80-7203-196-1	80-7203-196-1	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Thomas	Thomas	k1gMnSc1
Osborne	Osborn	k1gInSc5
na	na	k7c6
webu	web	k1gInSc6
britského	britský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
</s>
<s>
Thomas	Thomas	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Leedsu	Leeds	k1gInSc2
</s>
<s>
Thomas	Thomas	k1gMnSc1
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Leedsu	Leeds	k1gInSc2
na	na	k7c4
Encyclopedia	Encyclopedium	k1gNnPc4
Britannica	Britannic	k2eAgNnPc4d1
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
vévodů	vévoda	k1gMnPc2
z	z	k7c2
Leedsu	Leeds	k1gInSc2
Archivováno	archivovat	k5eAaBmNgNnS
11	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Thomas	Thomas	k1gMnSc1
Osborne	Osborn	k1gInSc5
byl	být	k5eAaImAgInS
za	za	k7c4
dobu	doba	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
dlouhého	dlouhý	k2eAgNnSc2d1
působení	působení	k1gNnSc2
znám	znát	k5eAaImIp1nS
celkem	celkem	k6eAd1
pod	pod	k7c7
sedmi	sedm	k4xCc7
jmény	jméno	k1gNnPc7
–	–	k?
Thomas	Thomas	k1gMnSc1
Osborne	Osborn	k1gInSc5
(	(	kIx(
<g/>
1631	#num#	k4
<g/>
-	-	kIx~
<g/>
1647	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sir	sir	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Osborne	Osborn	k1gInSc5
(	(	kIx(
<g/>
1647	#num#	k4
<g/>
-	-	kIx~
<g/>
1673	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
baron	baron	k1gMnSc1
Osborne	Osborn	k1gInSc5
(	(	kIx(
<g/>
1673	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
vikomt	vikomt	k1gMnSc1
Osborne	Osborn	k1gInSc5
z	z	k7c2
Latimeru	Latimer	k1gInSc2
(	(	kIx(
<g/>
1673	#num#	k4
<g/>
-	-	kIx~
<g/>
1674	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Danby	Danba	k1gFnSc2
(	(	kIx(
<g/>
1674	#num#	k4
<g/>
-	-	kIx~
<g/>
1689	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
markýz	markýz	k1gMnSc1
z	z	k7c2
Carmarthenu	Carmarthen	k1gInSc2
(	(	kIx(
<g/>
1689	#num#	k4
<g/>
-	-	kIx~
<g/>
1694	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Leedsu	Leeds	k1gInSc2
(	(	kIx(
<g/>
1694	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-	-	kIx~
<g/>
1712	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
složitém	složitý	k2eAgInSc6d1
systému	systém	k1gInSc6
šlechtické	šlechtický	k2eAgFnSc2d1
hierarchie	hierarchie	k1gFnSc2
britských	britský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
rekordmanem	rekordman	k1gMnSc7
<g/>
↑	↑	k?
Osborne	Osborn	k1gInSc5
jednal	jednat	k5eAaImAgMnS
s	s	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
jejích	její	k3xOp3gFnPc6
půjčkách	půjčka	k1gFnPc6
Anglii	Anglie	k1gFnSc4
s	s	k7c7
vědomím	vědomí	k1gNnSc7
krále	král	k1gMnSc2
Karla	Karel	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
však	však	k9
v	v	k7c6
zájmu	zájem	k1gInSc6
vlastního	vlastní	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
účast	účast	k1gFnSc1
na	na	k7c6
těchto	tento	k3xDgFnPc6
transakcích	transakce	k1gFnPc6
popřel	popřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
se	se	k3xPyFc4
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
ministra	ministr	k1gMnSc4
postavil	postavit	k5eAaPmAgMnS
a	a	k8xC
zabránil	zabránit	k5eAaPmAgMnS
parlamentu	parlament	k1gInSc3
odsoudit	odsoudit	k5eAaPmF
Osborna	Osborno	k1gNnPc4
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1147127301	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
6314	#num#	k4
8724	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
84149319	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
56961074	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
84149319	#num#	k4
</s>
<s>
1	#num#	k4
2	#num#	k4
Kindred	Kindred	k1gMnSc1
Britain	Britain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Darryl	Darryl	k1gInSc1
Roger	Roger	k1gInSc1
Lundy	Lunda	k1gMnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc2
Peerage	Peerag	k1gMnSc2
<g/>
.	.	kIx.
</s>
