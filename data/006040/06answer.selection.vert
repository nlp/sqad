<s>
První	první	k4xOgFnPc4	první
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
kvasinky	kvasinka	k1gFnPc4	kvasinka
poprvé	poprvé	k6eAd1	poprvé
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
Antoni	Antoň	k1gFnSc3	Antoň
van	van	k1gInSc4	van
Leeuwenhoek	Leeuwenhoek	k1gMnSc1	Leeuwenhoek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dopisech	dopis	k1gInPc6	dopis
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
výsledky	výsledek	k1gInPc1	výsledek
pozorování	pozorování	k1gNnPc2	pozorování
malých	malý	k2eAgFnPc2d1	malá
kuliček	kulička	k1gFnPc2	kulička
v	v	k7c6	v
pivě	pivo	k1gNnSc6	pivo
pomocí	pomocí	k7c2	pomocí
primitivního	primitivní	k2eAgInSc2d1	primitivní
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
