<s>
V	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
schématu	schéma	k1gNnSc6	schéma
moderní	moderní	k2eAgFnSc2d1	moderní
demokracie	demokracie	k1gFnSc2	demokracie
je	být	k5eAaImIp3nS	být
soudcovská	soudcovský	k2eAgFnSc1d1	soudcovská
moc	moc	k1gFnSc1	moc
oddělena	oddělen	k2eAgFnSc1d1	oddělena
od	od	k7c2	od
státu	stát	k1gInSc2	stát
a	a	k8xC	a
chráněna	chráněn	k2eAgFnSc1d1	chráněna
před	před	k7c4	před
jeho	jeho	k3xOp3gInPc4	jeho
zásahy	zásah	k1gInPc4	zásah
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
soudce	soudce	k1gMnSc1	soudce
nelze	lze	k6eNd1	lze
přímo	přímo	k6eAd1	přímo
odvolat	odvolat	k5eAaPmF	odvolat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
jen	jen	k9	jen
zákony	zákon	k1gInPc7	zákon
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
nařízeními	nařízení	k1gNnPc7	nařízení
a	a	k8xC	a
vyhláškami	vyhláška	k1gFnPc7	vyhláška
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přijímá	přijímat	k5eAaImIp3nS	přijímat
moc	moc	k6eAd1	moc
výkonná	výkonný	k2eAgFnSc1d1	výkonná
<g/>
.	.	kIx.	.
</s>
