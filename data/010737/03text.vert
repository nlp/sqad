<p>
<s>
Dělba	dělba	k1gFnSc1	dělba
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
separation	separation	k1gInSc1	separation
of	of	k?	of
powers	powers	k1gInSc1	powers
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
séparation	séparation	k1gInSc1	séparation
de	de	k?	de
pouvoirs	pouvoirs	k1gInSc1	pouvoirs
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Gewaltenteilung	Gewaltenteilung	k1gInSc1	Gewaltenteilung
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
princip	princip	k1gInSc1	princip
politického	politický	k2eAgNnSc2d1	politické
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
trojí	trojí	k4xRgInSc1	trojí
typ	typ	k1gInSc1	typ
moci	moc	k1gFnSc2	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
(	(	kIx(	(
<g/>
legislativa	legislativa	k1gFnSc1	legislativa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
(	(	kIx(	(
<g/>
exekutiva	exekutiva	k1gFnSc1	exekutiva
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
soudní	soudní	k2eAgFnSc4d1	soudní
moc	moc	k1gFnSc4	moc
(	(	kIx(	(
<g/>
judikativa	judikativum	k1gNnSc2	judikativum
či	či	k8xC	či
justice	justice	k1gFnSc2	justice
<g/>
)	)	kIx)	)
<g/>
a	a	k8xC	a
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
být	být	k5eAaImF	být
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
spíš	spíš	k9	spíš
o	o	k7c4	o
oddělení	oddělení	k1gNnSc4	oddělení
než	než	k8xS	než
rozdělení	rozdělení	k1gNnSc4	rozdělení
těchto	tento	k3xDgFnPc2	tento
složek	složka	k1gFnPc2	složka
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgFnPc1d1	uvedená
státní	státní	k2eAgFnPc1d1	státní
moci	moc	k1gFnPc1	moc
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
oddělení	oddělení	k1gNnSc2	oddělení
navíc	navíc	k6eAd1	navíc
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
navzájem	navzájem	k6eAd1	navzájem
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
–	–	k?	–
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
princip	princip	k1gInSc1	princip
brzd	brzda	k1gFnPc2	brzda
a	a	k8xC	a
protivah	protiváha	k1gFnPc2	protiváha
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
či	či	k8xC	či
"	"	kIx"	"
<g/>
rovnovah	rovnováha	k1gFnPc2	rovnováha
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
anglického	anglický	k2eAgMnSc2d1	anglický
checks	checks	k6eAd1	checks
and	and	k?	and
balances	balances	k1gInSc1	balances
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zamezit	zamezit	k5eAaPmF	zamezit
tendencím	tendence	k1gFnPc3	tendence
ke	k	k7c3	k
koncentraci	koncentrace	k1gFnSc3	koncentrace
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
bránit	bránit	k5eAaImF	bránit
libovůli	libovůle	k1gFnSc3	libovůle
a	a	k8xC	a
případnému	případný	k2eAgNnSc3d1	případné
zneužití	zneužití	k1gNnSc3	zneužití
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
chránit	chránit	k5eAaImF	chránit
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zásady	zásada	k1gFnPc1	zásada
dělby	dělba	k1gFnSc2	dělba
moci	moc	k1gFnSc2	moc
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
zásady	zásada	k1gFnPc4	zásada
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
dělba	dělba	k1gFnSc1	dělba
moci	moc	k1gFnSc2	moc
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
oddělenost	oddělenost	k1gFnSc1	oddělenost
mocí	moc	k1gFnPc2	moc
</s>
</p>
<p>
<s>
neslučitelnost	neslučitelnost	k1gFnSc1	neslučitelnost
mocí	moc	k1gFnPc2	moc
</s>
</p>
<p>
<s>
nezávislost	nezávislost	k1gFnSc4	nezávislost
mocí	moc	k1gFnSc7	moc
</s>
</p>
<p>
<s>
samostatnost	samostatnost	k1gFnSc4	samostatnost
mocí	moc	k1gFnSc7	moc
</s>
</p>
<p>
<s>
rovnováha	rovnováha	k1gFnSc1	rovnováha
mocí	moc	k1gFnPc2	moc
</s>
</p>
<p>
<s>
nezodpovědnost	nezodpovědnost	k1gFnSc1	nezodpovědnost
mocí	moc	k1gFnPc2	moc
(	(	kIx(	(
<g/>
jedné	jeden	k4xCgFnSc2	jeden
vůči	vůči	k7c3	vůči
druhé	druhý	k4xOgFnSc6	druhý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
rozdělení	rozdělení	k1gNnSc1	rozdělení
mocí	moc	k1gFnPc2	moc
bylo	být	k5eAaImAgNnS	být
běžné	běžný	k2eAgNnSc1d1	běžné
ve	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
republikách	republika	k1gFnPc6	republika
a	a	k8xC	a
městských	městský	k2eAgInPc6d1	městský
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
funkci	funkce	k1gFnSc4	funkce
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
sněmu	sněm	k1gInSc2	sněm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úřadů	úřad	k1gInPc2	úřad
(	(	kIx(	(
<g/>
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
a	a	k8xC	a
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
republice	republika	k1gFnSc6	republika
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
moc	moc	k6eAd1	moc
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
senát	senát	k1gInSc4	senát
<g/>
,	,	kIx,	,
zákonodárná	zákonodárný	k2eAgNnPc4d1	zákonodárné
shromáždění	shromáždění	k1gNnPc4	shromáždění
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
výkonné	výkonný	k2eAgInPc1d1	výkonný
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
řídil	řídit	k5eAaImAgInS	řídit
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
vydávat	vydávat	k5eAaImF	vydávat
příkazy	příkaz	k1gInPc4	příkaz
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárná	zákonodárný	k2eAgNnPc1d1	zákonodárné
shromáždění	shromáždění	k1gNnSc1	shromáždění
všech	všecek	k3xTgMnPc2	všecek
občanů	občan	k1gMnPc2	občan
vydávala	vydávat	k5eAaPmAgFnS	vydávat
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
volila	volit	k5eAaImAgFnS	volit
vysoké	vysoký	k2eAgFnPc4d1	vysoká
úředníky	úředník	k1gMnPc7	úředník
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
svěřena	svěřit	k5eAaPmNgFnS	svěřit
moc	moc	k6eAd1	moc
výkonná	výkonný	k2eAgFnSc1d1	výkonná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Princip	princip	k1gInSc1	princip
dělby	dělba	k1gFnSc2	dělba
moci	moc	k1gFnSc2	moc
nabyl	nabýt	k5eAaPmAgInS	nabýt
silně	silně	k6eAd1	silně
na	na	k7c6	na
významu	význam	k1gInSc6	význam
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
absolutismu	absolutismus	k1gInSc2	absolutismus
<g/>
.	.	kIx.	.
</s>
<s>
Panovník	panovník	k1gMnSc1	panovník
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zbavoval	zbavovat	k5eAaImAgInS	zbavovat
kontroly	kontrola	k1gFnSc2	kontrola
sněmem	sněm	k1gInSc7	sněm
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc4	výběr
daní	danit	k5eAaImIp3nP	danit
prováděli	provádět	k5eAaImAgMnP	provádět
jeho	jeho	k3xOp3gMnPc1	jeho
úředníci	úředník	k1gMnPc1	úředník
a	a	k8xC	a
panovník	panovník	k1gMnSc1	panovník
také	také	k9	také
disponoval	disponovat	k5eAaBmAgMnS	disponovat
stálou	stálý	k2eAgFnSc7d1	stálá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
nově	nově	k6eAd1	nově
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
moc	moc	k6eAd1	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
dělit	dělit	k5eAaImF	dělit
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Locke	Lock	k1gMnSc2	Lock
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ovšem	ovšem	k9	ovšem
ještě	ještě	k9	ještě
se	s	k7c7	s
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
justicí	justice	k1gFnSc7	justice
nepočítal	počítat	k5eNaImAgMnS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
princip	princip	k1gInSc1	princip
dělby	dělba	k1gFnSc2	dělba
moci	moc	k1gFnSc2	moc
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
až	až	k9	až
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filosof	filosof	k1gMnSc1	filosof
Montesquieu	Montesquieus	k1gInSc2	Montesquieus
jako	jako	k8xC	jako
rozdělení	rozdělení	k1gNnSc2	rozdělení
na	na	k7c4	na
moc	moc	k1gFnSc4	moc
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
<g/>
,	,	kIx,	,
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zajistila	zajistit	k5eAaPmAgFnS	zajistit
soudcovská	soudcovský	k2eAgFnSc1d1	soudcovská
nezávislost	nezávislost	k1gFnSc1	nezávislost
a	a	k8xC	a
výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
zákony	zákon	k1gInPc7	zákon
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
si	se	k3xPyFc3	se
nemůže	moct	k5eNaImIp3nS	moct
dávat	dávat	k5eAaImF	dávat
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
rozpracování	rozpracování	k1gNnSc1	rozpracování
tohoto	tento	k3xDgInSc2	tento
principu	princip	k1gInSc2	princip
pak	pak	k6eAd1	pak
provedl	provést	k5eAaPmAgMnS	provést
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
v	v	k7c6	v
Listech	list	k1gInPc6	list
federalistů	federalista	k1gMnPc2	federalista
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Deklarace	deklarace	k1gFnSc1	deklarace
práv	právo	k1gNnPc2	právo
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
občana	občan	k1gMnSc2	občan
pak	pak	k6eAd1	pak
výslovně	výslovně	k6eAd1	výslovně
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
není	být	k5eNaImIp3nS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
záruka	záruka	k1gFnSc1	záruka
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
vytyčena	vytyčen	k2eAgFnSc1d1	vytyčena
dělba	dělba	k1gFnSc1	dělba
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
vůbec	vůbec	k9	vůbec
ústavu	ústava	k1gFnSc4	ústava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělba	dělba	k1gFnSc1	dělba
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
demokracii	demokracie	k1gFnSc6	demokracie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
schématu	schéma	k1gNnSc6	schéma
moderní	moderní	k2eAgFnSc2d1	moderní
demokracie	demokracie	k1gFnSc2	demokracie
je	být	k5eAaImIp3nS	být
soudcovská	soudcovský	k2eAgFnSc1d1	soudcovská
moc	moc	k1gFnSc1	moc
oddělena	oddělen	k2eAgFnSc1d1	oddělena
od	od	k7c2	od
státu	stát	k1gInSc2	stát
a	a	k8xC	a
chráněna	chráněn	k2eAgFnSc1d1	chráněna
před	před	k7c4	před
jeho	jeho	k3xOp3gInPc4	jeho
zásahy	zásah	k1gInPc4	zásah
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
soudce	soudce	k1gMnSc1	soudce
nelze	lze	k6eNd1	lze
přímo	přímo	k6eAd1	přímo
odvolat	odvolat	k5eAaPmF	odvolat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
jen	jen	k9	jen
zákony	zákon	k1gInPc7	zákon
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
nařízeními	nařízení	k1gNnPc7	nařízení
a	a	k8xC	a
vyhláškami	vyhláška	k1gFnPc7	vyhláška
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přijímá	přijímat	k5eAaImIp3nS	přijímat
moc	moc	k6eAd1	moc
výkonná	výkonný	k2eAgFnSc1d1	výkonná
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
nejmenuje	jmenovat	k5eNaImIp3nS	jmenovat
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
také	také	k9	také
princip	princip	k1gInSc1	princip
neslučitelnosti	neslučitelnost	k1gFnSc2	neslučitelnost
některých	některý	k3yIgFnPc2	některý
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zároveň	zároveň	k6eAd1	zároveň
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
soudcem	soudce	k1gMnSc7	soudce
atd.	atd.	kA	atd.
Princip	princip	k1gInSc1	princip
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
"	"	kIx"	"
<g/>
brzd	brzda	k1gFnPc2	brzda
a	a	k8xC	a
rovnovah	rovnováha	k1gFnPc2	rovnováha
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
projevuje	projevovat	k5eAaImIp3nS	projevovat
např.	např.	kA	např.
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
kontrolována	kontrolovat	k5eAaImNgFnS	kontrolovat
a	a	k8xC	a
omezována	omezovat	k5eAaImNgFnS	omezovat
parlamentem	parlament	k1gInSc7	parlament
nejen	nejen	k6eAd1	nejen
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
možností	možnost	k1gFnSc7	možnost
vyslovení	vyslovení	k1gNnPc2	vyslovení
nedůvěry	nedůvěra	k1gFnSc2	nedůvěra
(	(	kIx(	(
<g/>
t.j.	t.j.	k?	t.j.
de	de	k?	de
facto	fact	k2eAgNnSc4d1	facto
odvolání	odvolání	k1gNnSc4	odvolání
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
schvalováním	schvalování	k1gNnSc7	schvalování
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
interpelacemi	interpelace	k1gFnPc7	interpelace
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
vláda	vláda	k1gFnSc1	vláda
administrativně	administrativně	k6eAd1	administrativně
řídí	řídit	k5eAaImIp3nS	řídit
a	a	k8xC	a
financuje	financovat	k5eAaBmIp3nS	financovat
soudnictví	soudnictví	k1gNnSc1	soudnictví
<g/>
,	,	kIx,	,
ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
může	moct	k5eAaImIp3nS	moct
zrušit	zrušit	k5eAaPmF	zrušit
zákony	zákon	k1gInPc7	zákon
apod.	apod.	kA	apod.
Zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákony	zákon	k1gInPc1	zákon
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
obecné	obecný	k2eAgNnSc1d1	obecné
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
míra	míra	k1gFnSc1	míra
jejich	jejich	k3xOp3gFnSc2	jejich
obecnosti	obecnost	k1gFnSc2	obecnost
a	a	k8xC	a
přípustnosti	přípustnost	k1gFnSc2	přípustnost
výjimek	výjimka	k1gFnPc2	výjimka
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
sporná	sporný	k2eAgFnSc1d1	sporná
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
soudní	soudní	k2eAgFnSc4d1	soudní
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ideální	ideální	k2eAgNnSc1d1	ideální
oddělení	oddělení	k1gNnSc1	oddělení
mocí	moc	k1gFnPc2	moc
však	však	k9	však
není	být	k5eNaImIp3nS	být
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
úplné	úplný	k2eAgNnSc1d1	úplné
<g/>
:	:	kIx,	:
v	v	k7c6	v
parlamentní	parlamentní	k2eAgFnSc6d1	parlamentní
formě	forma	k1gFnSc6	forma
vlády	vláda	k1gFnSc2	vláda
musí	muset	k5eAaImIp3nS	muset
exekutiva	exekutiva	k1gFnSc1	exekutiva
získat	získat	k5eAaPmF	získat
důvěru	důvěra	k1gFnSc4	důvěra
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
existuje	existovat	k5eAaImIp3nS	existovat
silné	silný	k2eAgNnSc4d1	silné
propojení	propojení	k1gNnSc4	propojení
mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
většinou	většina	k1gFnSc7	většina
a	a	k8xC	a
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
"	"	kIx"	"
<g/>
bloku	blok	k1gInSc2	blok
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
jen	jen	k9	jen
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
opozice	opozice	k1gFnSc1	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
především	především	k6eAd1	především
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
nejdůslednější	důsledný	k2eAgFnSc1d3	nejdůslednější
oddělení	oddělení	k1gNnSc4	oddělení
státních	státní	k2eAgFnPc2d1	státní
mocí	moc	k1gFnPc2	moc
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
exekutivu	exekutiva	k1gFnSc4	exekutiva
řídí	řídit	k5eAaImIp3nS	řídit
přímo	přímo	k6eAd1	přímo
volený	volený	k2eAgMnSc1d1	volený
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Kongresu	kongres	k1gInSc6	kongres
méně	málo	k6eAd2	málo
závislá	závislý	k2eAgFnSc1d1	závislá
a	a	k8xC	a
soudci	soudce	k1gMnPc7	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
jsou	být	k5eAaImIp3nP	být
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
doživotně	doživotně	k6eAd1	doživotně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
oddělenosti	oddělenost	k1gFnSc2	oddělenost
státních	státní	k2eAgFnPc2d1	státní
mocí	moc	k1gFnPc2	moc
specifický	specifický	k2eAgMnSc1d1	specifický
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejenže	nejenže	k6eAd1	nejenže
jsou	být	k5eAaImIp3nP	být
odděleny	oddělen	k2eAgFnPc4d1	oddělena
moci	moc	k1gFnPc4	moc
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
<g/>
,	,	kIx,	,
výkonná	výkonný	k2eAgFnSc1d1	výkonná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc1d1	soudní
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
2	[number]	k4	2
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
Ústavy	ústava	k1gFnSc2	ústava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozděleny	rozdělen	k2eAgInPc1d1	rozdělen
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
"	"	kIx"	"
<g/>
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
–	–	k?	–
Senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
–	–	k?	–
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
–	–	k?	–
obecné	obecný	k2eAgInPc4d1	obecný
soudy	soud	k1gInPc4	soud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
možnost	možnost	k1gFnSc1	možnost
koncentrace	koncentrace	k1gFnSc1	koncentrace
a	a	k8xC	a
zneužití	zneužití	k1gNnSc1	zneužití
moci	moc	k1gFnSc2	moc
i	i	k9	i
jen	jen	k9	jen
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
navíc	navíc	k6eAd1	navíc
roste	růst	k5eAaImIp3nS	růst
význam	význam	k1gInSc4	význam
dalších	další	k2eAgFnPc2d1	další
"	"	kIx"	"
<g/>
mocí	moc	k1gFnPc2	moc
<g/>
"	"	kIx"	"
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
veřejných	veřejný	k2eAgNnPc2d1	veřejné
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
také	také	k9	také
neměly	mít	k5eNaImAgFnP	mít
podléhat	podléhat	k5eAaImF	podléhat
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
parlamentním	parlamentní	k2eAgFnPc3d1	parlamentní
stranám	strana	k1gFnPc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zákonné	zákonný	k2eAgNnSc4d1	zákonné
postavení	postavení	k1gNnSc4	postavení
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
součástí	součást	k1gFnSc7	součást
snah	snaha	k1gFnPc2	snaha
o	o	k7c4	o
dělbu	dělba	k1gFnSc4	dělba
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Významnými	významný	k2eAgFnPc7d1	významná
nezávislými	závislý	k2eNgFnPc7d1	nezávislá
složkami	složka	k1gFnPc7	složka
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
svěřena	svěřen	k2eAgFnSc1d1	svěřena
správa	správa	k1gFnSc1	správa
měny	měna	k1gFnSc2	měna
a	a	k8xC	a
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
cenovou	cenový	k2eAgFnSc4d1	cenová
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
,	,	kIx,	,
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
kontrolu	kontrola	k1gFnSc4	kontrola
hospodaření	hospodaření	k1gNnSc2	hospodaření
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
plnění	plnění	k1gNnSc1	plnění
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
moci	moc	k1gFnSc2	moc
==	==	k?	==
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
této	tento	k3xDgFnSc2	tento
tradiční	tradiční	k2eAgFnSc2d1	tradiční
trojice	trojice	k1gFnSc2	trojice
bývají	bývat	k5eAaImIp3nP	bývat
zmiňovány	zmiňován	k2eAgInPc1d1	zmiňován
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
moci	moc	k1gFnSc2	moc
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
masmédia	masmédium	k1gNnPc1	masmédium
(	(	kIx(	(
<g/>
hromadné	hromadný	k2eAgInPc4d1	hromadný
sdělovací	sdělovací	k2eAgInPc4d1	sdělovací
a	a	k8xC	a
komunikační	komunikační	k2eAgInPc4d1	komunikační
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
například	například	k6eAd1	například
tisk	tisk	k1gInSc1	tisk
<g/>
,	,	kIx,	,
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
Internet	Internet	k1gInSc1	Internet
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
silné	silný	k2eAgFnPc1d1	silná
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
korporace	korporace	k1gFnPc1	korporace
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
církev	církev	k1gFnSc4	církev
či	či	k8xC	či
jiné	jiný	k2eAgFnPc4d1	jiná
ideologické	ideologický	k2eAgFnPc4d1	ideologická
a	a	k8xC	a
společenské	společenský	k2eAgFnPc4d1	společenská
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
občanská	občanský	k2eAgFnSc1d1	občanská
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
nekomerční	komerční	k2eNgInSc1d1	nekomerční
sdružení	sdružení	k1gNnSc2	sdružení
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
nespojená	spojený	k2eNgFnSc1d1	nespojená
se	s	k7c7	s
státní	státní	k2eAgFnSc7d1	státní
mocí	moc	k1gFnSc7	moc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Dělba	dělba	k1gFnSc1	dělba
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
sekce	sekce	k1gFnSc2	sekce
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
přednesených	přednesený	k2eAgInPc2d1	přednesený
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
vědecké	vědecký	k2eAgFnSc6d1	vědecká
konferenci	konference	k1gFnSc6	konference
Olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
právnické	právnický	k2eAgFnSc2d1	právnická
dny	dna	k1gFnSc2	dna
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Jiří	Jiří	k1gMnSc1	Jiří
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Iuridicum	Iuridicum	k1gInSc1	Iuridicum
Olomoucense	Olomoucense	k1gFnSc2	Olomoucense
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Právnickou	právnický	k2eAgFnSc7d1	právnická
fakultou	fakulta	k1gFnSc7	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
285	[number]	k4	285
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87382	[number]	k4	87382
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Demokracie	demokracie	k1gFnSc1	demokracie
</s>
</p>
<p>
<s>
Právní	právní	k2eAgInSc1d1	právní
stát	stát	k1gInSc1	stát
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dělba	dělba	k1gFnSc1	dělba
moci	moct	k5eAaImF	moct
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Saunders	Saunders	k1gInSc1	Saunders
<g/>
:	:	kIx,	:
Separation	Separation	k1gInSc1	Separation
of	of	k?	of
Powers	Powers	k1gInSc1	Powers
and	and	k?	and
the	the	k?	the
Judicial	Judicial	k1gMnSc1	Judicial
Branch	Branch	k1gMnSc1	Branch
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Madison	Madison	k1gInSc1	Madison
–	–	k?	–
Hamilton	Hamilton	k1gInSc1	Hamilton
on	on	k3xPp3gMnSc1	on
Checks	Checksa	k1gFnPc2	Checksa
and	and	k?	and
Balances	Balances	k1gMnSc1	Balances
Federalist	Federalist	k1gMnSc1	Federalist
Paper	Paper	k1gMnSc1	Paper
51	[number]	k4	51
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
M.	M.	kA	M.
J.	J.	kA	J.
C.	C.	kA	C.
Vile	vila	k1gFnSc6	vila
<g/>
,	,	kIx,	,
Constitutionalism	Constitutionalism	k1gMnSc1	Constitutionalism
and	and	k?	and
the	the	k?	the
Separation	Separation	k1gInSc1	Separation
of	of	k?	of
Powers	Powers	k1gInSc1	Powers
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Polybius	Polybius	k1gInSc1	Polybius
and	and	k?	and
the	the	k?	the
Founding	Founding	k1gInSc1	Founding
Fathers	Fathers	k1gInSc1	Fathers
<g/>
:	:	kIx,	:
the	the	k?	the
separation	separation	k1gInSc1	separation
of	of	k?	of
powers	powers	k6eAd1	powers
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Understanding	Understanding	k1gInSc1	Understanding
the	the	k?	the
separation	separation	k1gInSc1	separation
of	of	k?	of
powers	powers	k1gInSc1	powers
in	in	k?	in
the	the	k?	the
UK	UK	kA	UK
system	syst	k1gInSc7	syst
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Arbitrary	Arbitrara	k1gFnSc2	Arbitrara
Government	Government	k1gMnSc1	Government
Described	Described	k1gMnSc1	Described
and	and	k?	and
the	the	k?	the
Government	Government	k1gInSc1	Government
of	of	k?	of
the	the	k?	the
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
Vindicated	Vindicated	k1gMnSc1	Vindicated
from	from	k1gMnSc1	from
that	that	k2eAgInSc4d1	that
Aspersion	Aspersion	k1gInSc4	Aspersion
(	(	kIx(	(
<g/>
1644	[number]	k4	1644
<g/>
)	)	kIx)	)
</s>
</p>
