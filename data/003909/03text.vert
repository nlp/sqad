<s>
Seychelská	seychelský	k2eAgFnSc1d1	Seychelská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Seychely	Seychely	k1gFnPc1	Seychely
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
sejšely	sejšet	k5eAaPmAgFnP	sejšet
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
souostroví	souostroví	k1gNnSc6	souostroví
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
východně	východně	k6eAd1	východně
od	od	k7c2	od
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
severně	severně	k6eAd1	severně
od	od	k7c2	od
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
.	.	kIx.	.
</s>
<s>
Seychelská	seychelský	k2eAgFnSc1d1	Seychelská
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
na	na	k7c6	na
115	[number]	k4	115
malých	malý	k2eAgInPc6d1	malý
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
rozesetých	rozesetý	k2eAgNnPc6d1	rozeseté
v	v	k7c6	v
několika	několik	k4yIc6	několik
skupinách	skupina	k1gFnPc6	skupina
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
zhruba	zhruba	k6eAd1	zhruba
1100	[number]	k4	1100
krát	krát	k6eAd1	krát
800	[number]	k4	800
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
dvojího	dvojí	k4xRgInSc2	dvojí
druhu	druh	k1gInSc2	druh
–	–	k?	–
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
Seychelské	seychelský	k2eAgInPc4d1	seychelský
ostrovy	ostrov	k1gInPc4	ostrov
nebo	nebo	k8xC	nebo
též	též	k9	též
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Mahé	Mahá	k1gFnPc1	Mahá
<g/>
,	,	kIx,	,
Praslin	Praslin	k1gInSc1	Praslin
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Digue	Digue	k1gFnPc2	Digue
<g/>
,	,	kIx,	,
Silhouette	Silhouett	k1gInSc5	Silhouett
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
masivními	masivní	k2eAgInPc7d1	masivní
žulovými	žulový	k2eAgNnPc7d1	žulové
skalisky	skalisko	k1gNnPc7	skalisko
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatek	ostatek	k1gInSc1	ostatek
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
Vnější	vnější	k2eAgInPc1d1	vnější
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
představují	představovat	k5eAaImIp3nP	představovat
ploché	plochý	k2eAgInPc4d1	plochý
korálové	korálový	k2eAgInPc4d1	korálový
atoly	atol	k1gInPc4	atol
(	(	kIx(	(
<g/>
Amiranty	Amiranty	k1gFnPc4	Amiranty
<g/>
,	,	kIx,	,
Coetivy	Coetiva	k1gFnPc4	Coetiva
<g/>
,	,	kIx,	,
Providence	providence	k1gFnPc4	providence
<g/>
,	,	kIx,	,
Farquhar	Farquhar	k1gInSc4	Farquhar
a	a	k8xC	a
Aldabry	Aldabr	k1gInPc4	Aldabr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obydleno	obydlen	k2eAgNnSc1d1	obydleno
je	být	k5eAaImIp3nS	být
33	[number]	k4	33
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
na	na	k7c6	na
největším	veliký	k2eAgInSc6d3	veliký
ostrově	ostrov	k1gInSc6	ostrov
Mahé	Mahá	k1gFnSc2	Mahá
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Vnějších	vnější	k2eAgInPc6d1	vnější
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
představují	představovat	k5eAaImIp3nP	představovat
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
plochy	plocha	k1gFnSc2	plocha
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postrádají	postrádat	k5eAaImIp3nP	postrádat
vodní	vodní	k2eAgInPc1d1	vodní
zdroje	zdroj	k1gInPc1	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
%	%	kIx~	%
seychelského	seychelský	k2eAgNnSc2d1	Seychelské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Seychely	Seychely	k1gFnPc1	Seychely
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nP	těšit
poměrně	poměrně	k6eAd1	poměrně
stálému	stálý	k2eAgNnSc3d1	stálé
tropickému	tropický	k2eAgNnSc3d1	tropické
podnebí	podnebí	k1gNnSc3	podnebí
s	s	k7c7	s
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
denními	denní	k2eAgFnPc7d1	denní
teplotami	teplota	k1gFnPc7	teplota
mezi	mezi	k7c4	mezi
24	[number]	k4	24
a	a	k8xC	a
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
hojnému	hojný	k2eAgNnSc3d1	hojné
množství	množství	k1gNnSc3	množství
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
průměrné	průměrný	k2eAgInPc1d1	průměrný
roční	roční	k2eAgInPc1d1	roční
úhrny	úhrn	k1gInPc1	úhrn
činí	činit	k5eAaImIp3nP	činit
kolem	kolem	k7c2	kolem
3	[number]	k4	3
000	[number]	k4	000
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgMnPc4d1	častý
zejména	zejména	k9	zejména
během	během	k7c2	během
zimní	zimní	k2eAgFnSc2d1	zimní
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
převažuje	převažovat	k5eAaImIp3nS	převažovat
sucho	sucho	k1gNnSc1	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
blízkosti	blízkost	k1gFnSc3	blízkost
rovníku	rovník	k1gInSc2	rovník
Seychely	Seychely	k1gFnPc1	Seychely
leží	ležet	k5eAaImIp3nP	ležet
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
obvyklého	obvyklý	k2eAgInSc2d1	obvyklý
výskytu	výskyt	k1gInSc2	výskyt
tropických	tropický	k2eAgFnPc2d1	tropická
bouří	bouř	k1gFnPc2	bouř
<g/>
,	,	kIx,	,
bouře	bouře	k1gFnSc1	bouře
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
března	březen	k1gInSc2	březen
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
šedivé	šedivý	k2eAgInPc1d1	šedivý
a	a	k8xC	a
větrné	větrný	k2eAgInPc1d1	větrný
dny	den	k1gInPc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Nejtepleji	teple	k6eAd3	teple
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
až	až	k8xS	až
dubnu	duben	k1gInSc6	duben
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
nejchladnějším	chladný	k2eAgNnSc7d3	nejchladnější
obdobím	období	k1gNnSc7	období
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
léto	léto	k1gNnSc1	léto
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
z	z	k7c2	z
části	část	k1gFnSc2	část
pokryty	pokryt	k2eAgMnPc4d1	pokryt
hustým	hustý	k2eAgInSc7d1	hustý
tropickým	tropický	k2eAgInSc7d1	tropický
lesem	les	k1gInSc7	les
a	a	k8xC	a
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
odlehlost	odlehlost	k1gFnSc4	odlehlost
od	od	k7c2	od
pevniny	pevnina	k1gFnSc2	pevnina
jsou	být	k5eAaImIp3nP	být
domovem	domov	k1gInSc7	domov
mnoha	mnoho	k4c2	mnoho
endemických	endemický	k2eAgMnPc2d1	endemický
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
vypracovaná	vypracovaný	k2eAgFnSc1d1	vypracovaná
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
udává	udávat	k5eAaImIp3nS	udávat
jako	jako	k9	jako
endemické	endemický	k2eAgNnSc1d1	endemické
"	"	kIx"	"
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
<g/>
"	"	kIx"	"
75	[number]	k4	75
druhů	druh	k1gInPc2	druh
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
15	[number]	k4	15
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
3	[number]	k4	3
druhy	druh	k1gInPc1	druh
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
30	[number]	k4	30
druhů	druh	k1gInPc2	druh
obojživelníků	obojživelník	k1gMnPc2	obojživelník
a	a	k8xC	a
plazů	plaz	k1gMnPc2	plaz
a	a	k8xC	a
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
druhů	druh	k1gMnPc2	druh
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
vyniká	vynikat	k5eAaImIp3nS	vynikat
zejména	zejména	k9	zejména
atol	atol	k1gInSc1	atol
Aldabra	Aldabr	k1gInSc2	Aldabr
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
žije	žít	k5eAaImIp3nS	žít
největší	veliký	k2eAgFnSc1d3	veliký
světová	světový	k2eAgFnSc1d1	světová
populace	populace	k1gFnSc1	populace
obřích	obří	k2eAgFnPc2d1	obří
želv	želva	k1gFnPc2	želva
(	(	kIx(	(
<g/>
150	[number]	k4	150
až	až	k9	až
180	[number]	k4	180
tisíc	tisíc	k4xCgInSc4	tisíc
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
světové	světový	k2eAgNnSc4d1	světové
přírodní	přírodní	k2eAgNnSc4d1	přírodní
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
unikátním	unikátní	k2eAgInSc7d1	unikátní
symbolem	symbol	k1gInSc7	symbol
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
palma	palma	k1gFnSc1	palma
Lodoicea	Lodoicea	k1gFnSc1	Lodoicea
seychelská	seychelský	k2eAgFnSc1d1	Seychelská
(	(	kIx(	(
<g/>
Lodoicea	Lodoice	k2eAgFnSc1d1	Lodoicea
maldivica	maldivica	k1gFnSc1	maldivica
<g/>
)	)	kIx)	)
s	s	k7c7	s
obřími	obří	k2eAgInPc7d1	obří
plody	plod	k1gInPc7	plod
<g/>
,	,	kIx,	,
vážícími	vážící	k2eAgInPc7d1	vážící
kolem	kolem	k7c2	kolem
20	[number]	k4	20
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
háj	háj	k1gInSc1	háj
4	[number]	k4	4
000	[number]	k4	000
těchto	tento	k3xDgInPc2	tento
stromů	strom	k1gInPc2	strom
ve	v	k7c6	v
Vallée	Vallée	k1gFnSc6	Vallée
de	de	k?	de
Mai	Mai	k1gFnSc2	Mai
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Praslin	Praslina	k1gFnPc2	Praslina
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Seychelská	seychelský	k2eAgFnSc1d1	Seychelská
vláda	vláda	k1gFnSc1	vláda
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
velké	velký	k2eAgNnSc4d1	velké
úsilí	úsilí	k1gNnSc4	úsilí
na	na	k7c4	na
uchování	uchování	k1gNnSc4	uchování
místní	místní	k2eAgFnSc2d1	místní
přírody	příroda	k1gFnSc2	příroda
<g/>
;	;	kIx,	;
42	[number]	k4	42
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
a	a	k8xC	a
přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
ostrovy	ostrov	k1gInPc1	ostrov
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgInPc1d1	znám
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc2	středověk
arabským	arabský	k2eAgMnPc3d1	arabský
kupcům	kupec	k1gMnPc3	kupec
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
jako	jako	k8xS	jako
první	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
přistáli	přistát	k5eAaImAgMnP	přistát
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
zůstávaly	zůstávat	k5eAaImAgInP	zůstávat
dlouho	dlouho	k6eAd1	dlouho
bez	bez	k7c2	bez
trvalého	trvalý	k2eAgNnSc2d1	trvalé
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
Francie	Francie	k1gFnSc1	Francie
zřídila	zřídit	k5eAaPmAgFnS	zřídit
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
,	,	kIx,	,
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
Jeana	Jean	k1gMnSc2	Jean
Moreaua	Moreauus	k1gMnSc2	Moreauus
de	de	k?	de
Séchelles	Séchelles	k1gMnSc1	Séchelles
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
strategické	strategický	k2eAgFnSc3d1	strategická
poloze	poloha	k1gFnSc3	poloha
při	při	k7c6	při
námořní	námořní	k2eAgFnSc6d1	námořní
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
začali	začít	k5eAaPmAgMnP	začít
o	o	k7c4	o
ostrovy	ostrov	k1gInPc4	ostrov
usilovat	usilovat	k5eAaImF	usilovat
Britové	Brit	k1gMnPc1	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
Seychel	Seychely	k1gFnPc2	Seychely
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
za	za	k7c2	za
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
do	do	k7c2	do
jejich	jejich	k3xOp3gNnSc2	jejich
držení	držení	k1gNnSc2	držení
ostrovy	ostrov	k1gInPc1	ostrov
přešly	přejít	k5eAaPmAgInP	přejít
Pařížským	pařížský	k2eAgInSc7d1	pařížský
mírem	mír	k1gInSc7	mír
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
zde	zde	k6eAd1	zde
po	po	k7c6	po
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
zrušili	zrušit	k5eAaPmAgMnP	zrušit
otroctví	otroctví	k1gNnSc4	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
tohoto	tento	k3xDgInSc2	tento
aktu	akt	k1gInSc2	akt
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Victorie	Victorie	k1gFnSc2	Victorie
postavena	postaven	k2eAgFnSc1d1	postavena
socha	socha	k1gFnSc1	socha
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
symbolicky	symbolicky	k6eAd1	symbolicky
přetrhává	přetrhávat	k5eAaImIp3nS	přetrhávat
své	svůj	k3xOyFgInPc4	svůj
řetězy	řetěz	k1gInPc4	řetěz
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
byly	být	k5eAaImAgInP	být
dlouho	dlouho	k6eAd1	dlouho
spravovány	spravovat	k5eAaImNgInP	spravovat
z	z	k7c2	z
Mauricia	Mauricium	k1gNnSc2	Mauricium
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
vyčleněny	vyčlenit	k5eAaPmNgFnP	vyčlenit
jako	jako	k8xC	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
získaly	získat	k5eAaPmAgFnP	získat
Seychely	Seychely	k1gFnPc1	Seychely
nezávislost	nezávislost	k1gFnSc1	nezávislost
jako	jako	k8xS	jako
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Britského	britský	k2eAgNnSc2d1	Britské
společenství	společenství	k1gNnSc2	společenství
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
většinu	většina	k1gFnSc4	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
necelých	celý	k2eNgInPc2d1	necelý
87	[number]	k4	87
%	%	kIx~	%
<g/>
,	,	kIx,	,
představují	představovat	k5eAaImIp3nP	představovat
míšenci	míšenec	k1gMnPc1	míšenec
<g/>
,	,	kIx,	,
potomci	potomek	k1gMnPc1	potomek
francouzských	francouzský	k2eAgMnPc2d1	francouzský
kolonistů	kolonista	k1gMnPc2	kolonista
a	a	k8xC	a
afrických	africký	k2eAgMnPc2d1	africký
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
hovořící	hovořící	k2eAgInSc1d1	hovořící
seychelskou	seychelský	k2eAgFnSc7d1	Seychelská
kreolštinou	kreolština	k1gFnSc7	kreolština
(	(	kIx(	(
<g/>
Seselwa	Seselwa	k1gFnSc1	Seselwa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odvozenou	odvozený	k2eAgFnSc4d1	odvozená
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nS	tvořit
především	především	k9	především
menšiny	menšina	k1gFnPc4	menšina
Indů	Ind	k1gMnPc2	Ind
<g/>
,	,	kIx,	,
Malgašů	Malgaš	k1gMnPc2	Malgaš
a	a	k8xC	a
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
.	.	kIx.	.
</s>
<s>
Gramotnost	gramotnost	k1gFnSc1	gramotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
92	[number]	k4	92
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
mladších	mladý	k2eAgInPc2d2	mladší
ročníků	ročník	k1gInPc2	ročník
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
úplná	úplný	k2eAgFnSc1d1	úplná
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
Seychelanů	Seychelan	k1gMnPc2	Seychelan
žije	žít	k5eAaImIp3nS	žít
na	na	k7c4	na
trojici	trojice	k1gFnSc4	trojice
ostrovů	ostrov	k1gInPc2	ostrov
Mahé	Mahé	k1gNnPc2	Mahé
<g/>
,	,	kIx,	,
Praslin	Praslina	k1gFnPc2	Praslina
a	a	k8xC	a
La	la	k1gNnSc2	la
Digue	Digu	k1gInSc2	Digu
<g/>
.	.	kIx.	.
</s>
<s>
Dědictvím	dědictví	k1gNnSc7	dědictví
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
éry	éra	k1gFnSc2	éra
je	být	k5eAaImIp3nS	být
příslušnost	příslušnost	k1gFnSc4	příslušnost
většiny	většina	k1gFnSc2	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
Římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgNnSc1d1	Britské
dědictví	dědictví	k1gNnSc1	dědictví
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
do	do	k7c2	do
náboženské	náboženský	k2eAgFnSc2d1	náboženská
příslušnosti	příslušnost	k1gFnSc2	příslušnost
menšiny	menšina	k1gFnSc2	menšina
obyvatel	obyvatel	k1gMnSc1	obyvatel
k	k	k7c3	k
anglikánům	anglikán	k1gMnPc3	anglikán
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
skupiny	skupina	k1gFnPc1	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
presbyteriánům	presbyterián	k1gMnPc3	presbyterián
<g/>
,	,	kIx,	,
metodistům	metodista	k1gMnPc3	metodista
<g/>
,	,	kIx,	,
baptistům	baptista	k1gMnPc3	baptista
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
evangelickým	evangelický	k2eAgFnPc3d1	evangelická
církvím	církev	k1gFnPc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Indové	Ind	k1gMnPc1	Ind
jsou	být	k5eAaImIp3nP	být
zčásti	zčásti	k6eAd1	zčásti
hinduisté	hinduista	k1gMnPc1	hinduista
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
jsou	být	k5eAaImIp3nP	být
zčásti	zčásti	k6eAd1	zčásti
konfuciánci	konfuciánek	k1gMnPc1	konfuciánek
nebo	nebo	k8xC	nebo
taoisté	taoista	k1gMnPc1	taoista
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
buddhisté	buddhista	k1gMnPc1	buddhista
<g/>
.	.	kIx.	.
</s>
<s>
Seychely	Seychely	k1gFnPc1	Seychely
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
blahobytnou	blahobytný	k2eAgFnSc7d1	blahobytná
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
zdroj	zdroj	k1gInSc1	zdroj
příjmů	příjem	k1gInPc2	příjem
představuje	představovat	k5eAaImIp3nS	představovat
luxusní	luxusní	k2eAgInSc1d1	luxusní
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
stráví	strávit	k5eAaPmIp3nP	strávit
na	na	k7c6	na
Seychelách	Seychely	k1gFnPc6	Seychely
dovolenou	dovolená	k1gFnSc4	dovolená
zhruba	zhruba	k6eAd1	zhruba
150	[number]	k4	150
tisíc	tisíc	k4xCgInPc2	tisíc
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
významným	významný	k2eAgNnSc7d1	významné
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
lov	lov	k1gInSc4	lov
a	a	k8xC	a
zpracování	zpracování	k1gNnSc4	zpracování
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
tuňáků	tuňák	k1gMnPc2	tuňák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Seychelách	Seychely	k1gFnPc6	Seychely
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
tuňákových	tuňákův	k2eAgFnPc2d1	tuňákův
konzerváren	konzervárna	k1gFnPc2	konzervárna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělsky	zemědělsky	k6eAd1	zemědělsky
země	země	k1gFnSc1	země
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
soběstačná	soběstačný	k2eAgFnSc1d1	soběstačná
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
potravin	potravina	k1gFnPc2	potravina
se	se	k3xPyFc4	se
dováží	dovážet	k5eAaImIp3nP	dovážet
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
důležitými	důležitý	k2eAgFnPc7d1	důležitá
vývozními	vývozní	k2eAgFnPc7d1	vývozní
komoditami	komodita	k1gFnPc7	komodita
jsou	být	k5eAaImIp3nP	být
kokosové	kokosový	k2eAgInPc1d1	kokosový
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
palmová	palmový	k2eAgNnPc1d1	palmové
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
kopra	kopra	k1gFnSc1	kopra
<g/>
,	,	kIx,	,
skořice	skořice	k1gFnSc1	skořice
a	a	k8xC	a
vanilka	vanilka	k1gFnSc1	vanilka
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobými	dlouhodobý	k2eAgInPc7d1	dlouhodobý
problémy	problém	k1gInPc7	problém
jsou	být	k5eAaImIp3nP	být
nadhodnocený	nadhodnocený	k2eAgInSc4d1	nadhodnocený
kurs	kurs	k1gInSc4	kurs
místní	místní	k2eAgFnSc2d1	místní
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
dovozu	dovoz	k1gInSc6	dovoz
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
také	také	k9	také
velký	velký	k2eAgInSc4d1	velký
schodek	schodek	k1gInSc4	schodek
veřejných	veřejný	k2eAgFnPc2d1	veřejná
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
