<s>
Seychelská	seychelský	k2eAgFnSc1d1	Seychelská
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
na	na	k7c6	na
115	[number]	k4	115
malých	malý	k2eAgInPc6d1	malý
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
rozesetých	rozesetý	k2eAgNnPc6d1	rozeseté
v	v	k7c6	v
několika	několik	k4yIc6	několik
skupinách	skupina	k1gFnPc6	skupina
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
zhruba	zhruba	k6eAd1	zhruba
1100	[number]	k4	1100
krát	krát	k6eAd1	krát
800	[number]	k4	800
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
.	.	kIx.	.
</s>
