<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kutlvašr	Kutlvašr	k1gMnSc1	Kutlvašr
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Michalovice	Michalovice	k1gFnSc1	Michalovice
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
legionář	legionář	k1gMnSc1	legionář
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
velel	velet	k5eAaImAgMnS	velet
Pražskému	pražský	k2eAgNnSc3d1	Pražské
povstání	povstání	k1gNnSc3	povstání
a	a	k8xC	a
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
obětí	oběť	k1gFnSc7	oběť
politické	politický	k2eAgFnSc2d1	politická
perzekuce	perzekuce	k1gFnSc2	perzekuce
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kutlvašr	Kutlvašr	k1gMnSc1	Kutlvašr
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1895	[number]	k4	1895
v	v	k7c6	v
Michalovicích	Michalovice	k1gFnPc6	Michalovice
u	u	k7c2	u
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
jako	jako	k8xC	jako
šesté	šestý	k4xOgNnSc1	šestý
dítě	dítě	k1gNnSc1	dítě
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
sedláka	sedlák	k1gMnSc2	sedlák
Josefa	Josef	k1gMnSc2	Josef
Kutlvašra	Kutlvašr	k1gMnSc2	Kutlvašr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Německém	německý	k2eAgInSc6d1	německý
Brodě	Brod	k1gInSc6	Brod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
obchodní	obchodní	k2eAgFnSc4d1	obchodní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
byl	být	k5eAaImAgMnS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Jenč	Jenč	k1gInSc1	Jenč
v	v	k7c6	v
Humpolci	Humpolec	k1gInSc6	Humpolec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
jako	jako	k8xS	jako
úředník	úředník	k1gMnSc1	úředník
firmy	firma	k1gFnSc2	firma
Vielwart	Vielwart	k1gInSc1	Vielwart
a	a	k8xC	a
Dědina	dědina	k1gFnSc1	dědina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyvážela	vyvážet	k5eAaImAgFnS	vyvážet
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
stroje	stroj	k1gInPc4	stroj
do	do	k7c2	do
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1914	[number]	k4	1914
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
družiny	družina	k1gFnSc2	družina
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgFnSc2d1	zakládající
jednotky	jednotka	k1gFnSc2	jednotka
budoucích	budoucí	k2eAgFnPc2d1	budoucí
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
rozvědčík	rozvědčík	k1gMnSc1	rozvědčík
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
řady	řada	k1gFnPc4	řada
významných	významný	k2eAgFnPc2d1	významná
bitev	bitva	k1gFnPc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hrdinství	hrdinství	k1gNnSc4	hrdinství
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
byl	být	k5eAaImAgInS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Zborova	Zborov	k1gInSc2	Zborov
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgInS	být
raněn	ranit	k5eAaPmNgMnS	ranit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velitelem	velitel	k1gMnSc7	velitel
praporu	prapor	k1gInSc2	prapor
a	a	k8xC	a
pomocníkem	pomocník	k1gMnSc7	pomocník
velitele	velitel	k1gMnSc2	velitel
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
střeleckého	střelecký	k2eAgInSc2d1	střelecký
pluku	pluk	k1gInSc2	pluk
plukovníka	plukovník	k1gMnSc4	plukovník
Švece	Švec	k1gMnSc4	Švec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
také	také	k9	také
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
dobytí	dobytí	k1gNnSc4	dobytí
Kazaně	Kazaň	k1gFnSc2	Kazaň
6	[number]	k4	6
<g/>
.	.	kIx.	.
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1918	[number]	k4	1918
.	.	kIx.	.
<g/>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prozatímním	prozatímní	k2eAgMnSc7d1	prozatímní
velitelem	velitel	k1gMnSc7	velitel
pluku	pluk	k1gInSc2	pluk
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1919	[number]	k4	1919
byl	být	k5eAaImAgMnS	být
ministrem	ministr	k1gMnSc7	ministr
vojenství	vojenství	k1gNnSc2	vojenství
Milanem	Milan	k1gMnSc7	Milan
Rastislavem	Rastislav	k1gMnSc7	Rastislav
Štefánikem	Štefánik	k1gMnSc7	Štefánik
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
podplukovníka	podplukovník	k1gMnSc4	podplukovník
a	a	k8xC	a
následně	následně	k6eAd1	následně
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
i	i	k8xC	i
definitivním	definitivní	k2eAgMnSc7d1	definitivní
velitelem	velitel	k1gMnSc7	velitel
1	[number]	k4	1
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Jelizavetou	Jelizavetý	k2eAgFnSc7d1	Jelizavetý
Jakovlevovou	Jakovlevová	k1gFnSc7	Jakovlevová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
a	a	k8xC	a
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
plukovníka	plukovník	k1gMnSc4	plukovník
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
velitel	velitel	k1gMnSc1	velitel
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
1	[number]	k4	1
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
brigádního	brigádní	k2eAgMnSc4d1	brigádní
generála	generál	k1gMnSc4	generál
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
33	[number]	k4	33
let	léto	k1gNnPc2	léto
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgMnPc2d3	nejmladší
generálů	generál	k1gMnPc2	generál
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
zastával	zastávat	k5eAaImAgMnS	zastávat
postupně	postupně	k6eAd1	postupně
řadu	řada	k1gFnSc4	řada
velitelských	velitelský	k2eAgFnPc2d1	velitelská
a	a	k8xC	a
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934	[number]	k4	1934
až	až	k9	až
1939	[number]	k4	1939
velel	velet	k5eAaImAgInS	velet
4	[number]	k4	4
<g/>
.	.	kIx.	.
pěší	pěší	k2eAgFnSc3d1	pěší
divizi	divize	k1gFnSc3	divize
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mobilizace	mobilizace	k1gFnSc1	mobilizace
v	v	k7c6	v
září	září	k1gNnSc6	září
1938	[number]	k4	1938
převzal	převzít	k5eAaPmAgInS	převzít
velení	velení	k1gNnSc4	velení
Hraniční	hraniční	k2eAgFnSc2d1	hraniční
oblasti	oblast	k1gFnSc2	oblast
35	[number]	k4	35
se	se	k3xPyFc4	se
stanovištěm	stanoviště	k1gNnSc7	stanoviště
velitelství	velitelství	k1gNnPc2	velitelství
ve	v	k7c6	v
Vamberku	Vamberk	k1gInSc6	Vamberk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
okupace	okupace	k1gFnSc2	okupace
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
odbojové	odbojový	k2eAgFnSc2d1	odbojová
organizace	organizace	k1gFnSc2	organizace
Obrana	obrana	k1gFnSc1	obrana
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Pražského	pražský	k2eAgNnSc2d1	Pražské
povstání	povstání	k1gNnSc2	povstání
byl	být	k5eAaImAgInS	být
generálem	generál	k1gMnSc7	generál
Slunečkem	slunečko	k1gNnSc7	slunečko
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Alexem	Alex	k1gMnSc7	Alex
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
povstaleckých	povstalecký	k2eAgFnPc2d1	povstalecká
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
posléze	posléze	k6eAd1	posléze
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
odpoledne	odpoledne	k6eAd1	odpoledne
dojednal	dojednat	k5eAaPmAgMnS	dojednat
podmínky	podmínka	k1gFnPc4	podmínka
provedení	provedení	k1gNnSc2	provedení
kapitulace	kapitulace	k1gFnSc2	kapitulace
německých	německý	k2eAgFnPc2d1	německá
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
jejich	jejich	k3xOp3gInSc4	jejich
volný	volný	k2eAgInSc4d1	volný
průjezd	průjezd	k1gInSc4	průjezd
městem	město	k1gNnSc7	město
do	do	k7c2	do
zajetí	zajetí	k1gNnSc6	zajetí
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
silami	síla	k1gFnPc7	síla
západních	západní	k2eAgMnPc2d1	západní
Spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
zřejmě	zřejmě	k6eAd1	zřejmě
stalo	stát	k5eAaPmAgNnS	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
jeho	jeho	k3xOp3gInSc2	jeho
pronásledování	pronásledování	k1gNnSc1	pronásledování
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
<g/>
,	,	kIx,	,
prováděného	prováděný	k2eAgInSc2d1	prováděný
na	na	k7c6	na
přání	přání	k1gNnSc6	přání
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1945	[number]	k4	1945
začal	začít	k5eAaPmAgMnS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
nátlak	nátlak	k1gInSc4	nátlak
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
penzionování	penzionování	k1gNnSc4	penzionování
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xC	jako
vojenský	vojenský	k2eAgMnSc1d1	vojenský
velitel	velitel	k1gMnSc1	velitel
Prahy	Praha	k1gFnSc2	Praha
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zatímním	zatímní	k2eAgMnSc7d1	zatímní
velitelem	velitel	k1gMnSc7	velitel
V.	V.	kA	V.
sboru	sbor	k1gInSc2	sbor
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
po	po	k7c6	po
stížnostech	stížnost	k1gFnPc6	stížnost
sovětského	sovětský	k2eAgMnSc2d1	sovětský
vyslance	vyslanec	k1gMnSc2	vyslanec
Zorina	Zorin	k1gMnSc2	Zorin
<g/>
,	,	kIx,	,
odeslán	odeslán	k2eAgInSc4d1	odeslán
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
dalším	další	k2eAgInSc6d1	další
osudu	osud	k1gInSc6	osud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
služby	služba	k1gFnSc2	služba
se	s	k7c7	s
<g/>
,	,	kIx,	,
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
prezidenta	prezident	k1gMnSc2	prezident
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
až	až	k6eAd1	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1946	[number]	k4	1946
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
III	III	kA	III
<g/>
.	.	kIx.	.
sboru	sbor	k1gInSc2	sbor
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
povýšení	povýšení	k1gNnSc6	povýšení
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
divizní	divizní	k2eAgMnSc1d1	divizní
generál	generál	k1gMnSc1	generál
posléze	posléze	k6eAd1	posléze
sloužil	sloužit	k5eAaImAgMnS	sloužit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
jako	jako	k8xS	jako
zástupce	zástupce	k1gMnSc1	zástupce
velitele	velitel	k1gMnSc2	velitel
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
oblasti	oblast	k1gFnSc2	oblast
3	[number]	k4	3
opět	opět	k6eAd1	opět
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
převratu	převrat	k1gInSc6	převrat
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
výslužby	výslužba	k1gFnSc2	výslužba
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1948	[number]	k4	1948
zatčen	zatčen	k2eAgMnSc1d1	zatčen
a	a	k8xC	a
coby	coby	k?	coby
představitel	představitel	k1gMnSc1	představitel
údajné	údajný	k2eAgFnPc4d1	údajná
odbojové	odbojový	k2eAgFnPc4d1	odbojová
skupiny	skupina	k1gFnPc4	skupina
"	"	kIx"	"
<g/>
Pravda	pravda	k9	pravda
vítězí	vítězit	k5eAaImIp3nS	vítězit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
zinscenována	zinscenovat	k5eAaPmNgFnS	zinscenovat
provokací	provokace	k1gFnSc7	provokace
5	[number]	k4	5
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vykonstruovaném	vykonstruovaný	k2eAgInSc6d1	vykonstruovaný
procesu	proces	k1gInSc6	proces
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
Státním	státní	k2eAgInSc7d1	státní
soudem	soud	k1gInSc7	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
za	za	k7c4	za
velezradu	velezrada	k1gFnSc4	velezrada
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
a	a	k8xC	a
současně	současně	k6eAd1	současně
degradaci	degradace	k1gFnSc4	degradace
na	na	k7c4	na
vojína	vojín	k1gMnSc4	vojín
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
<g/>
.	.	kIx.	.
</s>
<s>
Vězněn	vězněn	k2eAgInSc1d1	vězněn
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
Mírově	mírově	k6eAd1	mírově
a	a	k8xC	a
v	v	k7c6	v
Leopoldově	Leopoldův	k2eAgNnSc6d1	Leopoldovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
po	po	k7c6	po
amnestii	amnestie	k1gFnSc6	amnestie
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
Antonína	Antonín	k1gMnSc4	Antonín
Novotného	Novotný	k1gMnSc4	Novotný
<g/>
,	,	kIx,	,
vyhlášené	vyhlášený	k2eAgInPc1d1	vyhlášený
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
přijetí	přijetí	k1gNnSc2	přijetí
"	"	kIx"	"
<g/>
socialistické	socialistický	k2eAgFnSc2d1	socialistická
ústavy	ústava	k1gFnSc2	ústava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g/>
přesněji	přesně	k6eAd2	přesně
"	"	kIx"	"
<g/>
dovršení	dovršení	k1gNnSc1	dovršení
socialismu	socialismus	k1gInSc2	socialismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
změně	změna	k1gFnSc3	změna
názvu	název	k1gInSc2	název
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
ČSR	ČSR	kA	ČSR
na	na	k7c6	na
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
výkonu	výkon	k1gInSc2	výkon
trestu	trest	k1gInSc2	trest
propuštěn	propustit	k5eAaPmNgMnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
špatném	špatný	k2eAgInSc6d1	špatný
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
bydlel	bydlet	k5eAaImAgMnS	bydlet
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
Praze-Vršovicích	Praze-Vršovice	k1gFnPc6	Praze-Vršovice
<g/>
,	,	kIx,	,
Rybalkova	Rybalkův	k2eAgNnSc2d1	Rybalkův
69	[number]	k4	69
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
vyměřen	vyměřen	k2eAgInSc4d1	vyměřen
starobní	starobní	k2eAgInSc4d1	starobní
důchod	důchod	k1gInSc4	důchod
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
pouze	pouze	k6eAd1	pouze
230	[number]	k4	230
Kčs	Kčs	kA	Kčs
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
,	,	kIx,	,
živil	živit	k5eAaImAgMnS	živit
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
hlídač	hlídač	k1gMnSc1	hlídač
v	v	k7c6	v
Jízdárně	jízdárna	k1gFnSc6	jízdárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
poté	poté	k6eAd1	poté
co	co	k3yRnSc1	co
odtamtud	odtamtud	k6eAd1	odtamtud
musel	muset	k5eAaImAgMnS	muset
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
noční	noční	k2eAgMnSc1d1	noční
vrátný	vrátný	k1gMnSc1	vrátný
v	v	k7c6	v
Nuselském	nuselský	k2eAgInSc6d1	nuselský
pivovaru	pivovar	k1gInSc6	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
náhle	náhle	k6eAd1	náhle
při	při	k7c6	při
zdravotní	zdravotní	k2eAgFnSc6d1	zdravotní
prohlídce	prohlídka	k1gFnSc6	prohlídka
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1961	[number]	k4	1961
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Motole	Motol	k1gInSc6	Motol
v	v	k7c6	v
ordinaci	ordinace	k1gFnSc6	ordinace
lékaře	lékař	k1gMnSc2	lékař
MUDr.	MUDr.	kA	MUDr.
Endta	Endt	k1gMnSc2	Endt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
otec	otec	k1gMnSc1	otec
bojoval	bojovat	k5eAaImAgMnS	bojovat
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
boku	bok	k1gInSc6	bok
v	v	k7c6	v
ruských	ruský	k2eAgFnPc6d1	ruská
legiích	legie	k1gFnPc6	legie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
zrušil	zrušit	k5eAaPmAgInS	zrušit
Městský	městský	k2eAgInSc1d1	městský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
všechna	všechen	k3xTgNnPc4	všechen
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1948	[number]	k4	1948
a	a	k8xC	a
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
rehabilitován	rehabilitován	k2eAgMnSc1d1	rehabilitován
však	však	k9	však
byl	být	k5eAaImAgMnS	být
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
memoriam	memoriam	k6eAd1	memoriam
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
armádního	armádní	k2eAgMnSc4d1	armádní
generála	generál	k1gMnSc4	generál
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
udělen	udělit	k5eAaPmNgInS	udělit
Řád	řád	k1gInSc1	řád
Milana	Milan	k1gMnSc2	Milan
Rastislava	Rastislav	k1gMnSc2	Rastislav
Štefánika	Štefánik	k1gMnSc2	Štefánik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
mu	on	k3xPp3gMnSc3	on
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
Řád	řád	k1gInSc1	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
vojenské	vojenský	k2eAgFnSc2d1	vojenská
skupiny	skupina	k1gFnSc2	skupina
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
za	za	k7c4	za
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
obranu	obrana	k1gFnSc4	obrana
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětní	pamětní	k2eAgNnPc1d1	pamětní
místa	místo	k1gNnPc1	místo
==	==	k?	==
</s>
</p>
<p>
<s>
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
na	na	k7c6	na
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Michalovicích	Michalovice	k1gFnPc6	Michalovice
</s>
</p>
<p>
<s>
Busta	busta	k1gFnSc1	busta
a	a	k8xC	a
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
Městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
-	-	kIx~	-
Nuselská	nuselský	k2eAgFnSc1d1	Nuselská
radnice	radnice	k1gFnSc1	radnice
</s>
</p>
<p>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
generála	generál	k1gMnSc2	generál
Kutlvašra	Kutlvašr	k1gMnSc2	Kutlvašr
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
-	-	kIx~	-
Nuslích	Nusle	k1gFnPc6	Nusle
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Osudy	osud	k1gInPc1	osud
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
–	–	k?	–
Divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
1945	[number]	k4	1945
–	–	k?	–
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
,	,	kIx,	,
61	[number]	k4	61
<g/>
,	,	kIx,	,
192	[number]	k4	192
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-200-1502-0	[number]	k4	978-80-200-1502-0
</s>
</p>
<p>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
Preclík	preclík	k1gInSc1	preclík
<g/>
:	:	kIx,	:
T.G.	T.G.	k1gMnSc1	T.G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
legionáři	legionář	k1gMnPc1	legionář
<g/>
,	,	kIx,	,
in	in	k?	in
ČAS	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
demokratického	demokratický	k2eAgNnSc2d1	demokratické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
97	[number]	k4	97
<g/>
,	,	kIx,	,
s.	s.	k?	s.
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
ročník	ročník	k1gInSc1	ročník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
leden-březen	ledenřezen	k2eAgMnSc1d1	leden-březen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
1648	[number]	k4	1648
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Kutlvašr	Kutlvašr	k1gMnSc1	Kutlvašr
</s>
</p>
<p>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
In	In	k1gFnSc1	In
memoriam	memoriam	k1gInSc1	memoriam
Karel	Karel	k1gMnSc1	Karel
Kutlvašr	Kutlvašr	k1gMnSc1	Kutlvašr
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
na	na	k7c4	na
valka	valko	k1gNnPc4	valko
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Říkali	říkat	k5eAaImAgMnP	říkat
mu	on	k3xPp3gMnSc3	on
bráška	bráška	k1gMnSc1	bráška
"	"	kIx"	"
<g/>
Kultivátor	kultivátor	k1gMnSc1	kultivátor
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kutlvašr	Kutlvašr	k1gMnSc1	Kutlvašr
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Spolku	spolek	k1gInSc2	spolek
pro	pro	k7c4	pro
vojenská	vojenský	k2eAgNnPc4d1	vojenské
pietní	pietní	k2eAgNnPc4d1	pietní
místa	místo	k1gNnPc4	místo
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kutlvašr	Kutlvašr	k1gMnSc1	Kutlvašr
na	na	k7c6	na
bojovniciprotitotalite	bojovniciprotitotalit	k1gInSc5	bojovniciprotitotalit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
