<s>
Karel	Karel	k1gMnSc1	Karel
Kutlvašr	Kutlvašr	k1gMnSc1	Kutlvašr
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Michalovice	Michalovice	k1gFnSc1	Michalovice
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
legionář	legionář	k1gMnSc1	legionář
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
velel	velet	k5eAaImAgMnS	velet
Pražskému	pražský	k2eAgNnSc3d1	Pražské
povstání	povstání	k1gNnSc3	povstání
a	a	k8xC	a
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
obětí	oběť	k1gFnSc7	oběť
politické	politický	k2eAgFnSc2d1	politická
perzekuce	perzekuce	k1gFnSc2	perzekuce
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
