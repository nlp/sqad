<s>
Žamberk	Žamberk	k1gInSc1	Žamberk
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Senftenberg	Senftenberg	k1gInSc1	Senftenberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
v	v	k7c6	v
Pardubickém	pardubický	k2eAgInSc6d1	pardubický
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Orlických	orlický	k2eAgMnPc2d1	orlický
hor.	hor.	k?	hor.
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
rozprostřeno	rozprostřen	k2eAgNnSc1d1	rozprostřeno
jak	jak	k8xC	jak
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Divoké	divoký	k2eAgFnSc2d1	divoká
Orlice	Orlice	k1gFnSc2	Orlice
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
okolních	okolní	k2eAgInPc6d1	okolní
kopcích	kopec	k1gInPc6	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Žamberk	Žamberk	k1gInSc1	Žamberk
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
16,91	[number]	k4	16,91
km2	km2	k4	km2
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
přes	přes	k7c4	přes
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
aglomeraci	aglomerace	k1gFnSc6	aglomerace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
město	město	k1gNnSc1	město
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
společně	společně	k6eAd1	společně
s	s	k7c7	s
přilehlými	přilehlý	k2eAgFnPc7d1	přilehlá
obcemi	obec	k1gFnPc7	obec
(	(	kIx(	(
<g/>
Dlouhoňovice	Dlouhoňovice	k1gFnPc1	Dlouhoňovice
<g/>
,	,	kIx,	,
Helvíkovice	Helvíkovice	k1gFnPc1	Helvíkovice
<g/>
,	,	kIx,	,
Lukavice	Lukavice	k1gFnPc1	Lukavice
a	a	k8xC	a
Líšnice	Líšnice	k1gFnPc1	Líšnice
<g/>
)	)	kIx)	)
však	však	k9	však
žije	žít	k5eAaImIp3nS	žít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
100	[number]	k4	100
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
administrativním	administrativní	k2eAgInSc7d1	administrativní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
spádovým	spádový	k2eAgNnSc7d1	spádové
centrem	centrum	k1gNnSc7	centrum
žamberského	žamberský	k2eAgInSc2d1	žamberský
mikroregionu	mikroregion	k1gInSc2	mikroregion
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
čítá	čítat	k5eAaImIp3nS	čítat
téměř	téměř	k6eAd1	téměř
29	[number]	k4	29
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
městskou	městský	k2eAgFnSc4d1	městská
památkovou	památkový	k2eAgFnSc4d1	památková
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
turistickým	turistický	k2eAgNnSc7d1	turistické
východiskem	východisko	k1gNnSc7	východisko
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
Orlických	orlický	k2eAgFnPc2d1	Orlická
hor	hora	k1gFnPc2	hora
i	i	k8xC	i
jejich	jejich	k3xOp3gNnSc2	jejich
podhůří	podhůří	k1gNnSc2	podhůří
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
veřejné	veřejný	k2eAgNnSc1d1	veřejné
vnitrostátní	vnitrostátní	k2eAgNnSc1d1	vnitrostátní
letiště	letiště	k1gNnSc1	letiště
(	(	kIx(	(
<g/>
sportovní	sportovní	k2eAgNnPc1d1	sportovní
letiště	letiště	k1gNnPc1	letiště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
Žamberka	Žamberka	k1gFnSc1	Žamberka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sportovně	sportovně	k6eAd1	sportovně
rekrační	rekrační	k2eAgInSc1d1	rekrační
areál	areál	k1gInSc1	areál
Pod	pod	k7c7	pod
Černým	černý	k2eAgInSc7d1	černý
lesem	les	k1gInSc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
doložená	doložený	k2eAgFnSc1d1	doložená
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Žamberku	Žamberk	k1gInSc6	Žamberk
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
listině	listina	k1gFnSc6	listina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1332	[number]	k4	1332
zbraslavský	zbraslavský	k2eAgMnSc1d1	zbraslavský
opat	opat	k1gMnSc1	opat
prodával	prodávat	k5eAaImAgMnS	prodávat
lanškrounskou	lanškrounský	k2eAgFnSc4d1	Lanškrounská
rychtu	rychta	k1gFnSc4	rychta
jakémusi	jakýsi	k3yIgMnSc3	jakýsi
Tyczkovi	Tyczek	k1gMnSc3	Tyczek
(	(	kIx(	(
<g/>
Tyčkovi	Tyček	k1gMnSc3	Tyček
<g/>
)	)	kIx)	)
ze	z	k7c2	z
Žamberka	Žamberka	k1gFnSc1	Žamberka
<g/>
.	.	kIx.	.
</s>
<s>
Lanšperský	Lanšperský	k2eAgInSc1d1	Lanšperský
hrad	hrad	k1gInSc1	hrad
i	i	k8xC	i
celé	celý	k2eAgNnSc1d1	celé
lanškrounské	lanškrounský	k2eAgNnSc1d1	lanškrounské
panství	panství	k1gNnSc1	panství
byly	být	k5eAaImAgFnP	být
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
králů	král	k1gMnPc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
a	a	k8xC	a
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Čechách	Čechy	k1gFnPc6	Čechy
založeny	založit	k5eAaPmNgInP	založit
nové	nový	k2eAgInPc1d1	nový
hrady	hrad	k1gInPc1	hrad
a	a	k8xC	a
osady	osada	k1gFnSc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
dobách	doba	k1gFnPc6	doba
byly	být	k5eAaImAgFnP	být
založeny	založen	k2eAgInPc4d1	založen
hrady	hrad	k1gInPc4	hrad
Lanšperk	Lanšperk	k1gInSc1	Lanšperk
<g/>
,	,	kIx,	,
Žampach	Žampach	k1gInSc1	Žampach
<g/>
,	,	kIx,	,
Litice	Litice	k1gFnSc1	Litice
<g/>
,	,	kIx,	,
Potštejn	Potštejn	k1gInSc1	Potštejn
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Kolonizátoři	kolonizátor	k1gMnPc1	kolonizátor
založili	založit	k5eAaPmAgMnP	založit
města	město	k1gNnPc4	město
a	a	k8xC	a
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dnes	dnes	k6eAd1	dnes
známe	znát	k5eAaImIp1nP	znát
jako	jako	k8xS	jako
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
Rokytnice	Rokytnice	k1gFnSc1	Rokytnice
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
Vamberk	Vamberk	k1gInSc1	Vamberk
<g/>
,	,	kIx,	,
Slatina	slatina	k1gFnSc1	slatina
nad	nad	k7c7	nad
Zdobnicí	Zdobnice	k1gFnSc7	Zdobnice
<g/>
,	,	kIx,	,
Pěčín	Pěčín	k1gInSc1	Pěčín
<g/>
,	,	kIx,	,
Kunvald	Kunvald	k1gInSc1	Kunvald
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
,	,	kIx,	,
Lanškroun	Lanškroun	k1gInSc1	Lanškroun
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Letohrad	letohrad	k1gInSc1	letohrad
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc4d1	přesný
rok	rok	k1gInSc4	rok
založení	založení	k1gNnSc2	založení
těchto	tento	k3xDgInPc2	tento
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
však	však	k9	však
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
dobách	doba	k1gFnPc6	doba
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
německými	německý	k2eAgMnPc7d1	německý
kolonisty	kolonista	k1gMnPc7	kolonista
i	i	k8xC	i
Žamberk	Žamberk	k1gInSc4	Žamberk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
německé	německý	k2eAgNnSc1d1	německé
město	město	k1gNnSc1	město
Žamberk	Žamberk	k1gInSc1	Žamberk
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
rolnická	rolnický	k2eAgFnSc1d1	rolnická
osada	osada	k1gFnSc1	osada
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
německé	německý	k2eAgNnSc1d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
přineslo	přinést	k5eAaPmAgNnS	přinést
do	do	k7c2	do
nově	nově	k6eAd1	nově
založeného	založený	k2eAgNnSc2d1	založené
města	město	k1gNnSc2	město
řemesla	řemeslo	k1gNnSc2	řemeslo
a	a	k8xC	a
také	také	k9	také
německé	německý	k2eAgNnSc1d1	německé
městské	městský	k2eAgNnSc1d1	Městské
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
byl	být	k5eAaImAgMnS	být
rychtář	rychtář	k1gMnSc1	rychtář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
listinách	listina	k1gFnPc6	listina
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
psalo	psát	k5eAaImAgNnS	psát
různě	různě	k6eAd1	různě
<g/>
:	:	kIx,	:
Senftinberg	Senftinberg	k1gMnSc1	Senftinberg
<g/>
,	,	kIx,	,
Semftenberg	Semftenberg	k1gMnSc1	Semftenberg
<g/>
,	,	kIx,	,
Zenftenberg	Zenftenberg	k1gMnSc1	Zenftenberg
<g/>
,	,	kIx,	,
Senftenberg	Senftenberg	k1gMnSc1	Senftenberg
nebo	nebo	k8xC	nebo
Zenftenberg	Zenftenberg	k1gMnSc1	Zenftenberg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
vlivem	vlivem	k7c2	vlivem
okolního	okolní	k2eAgNnSc2d1	okolní
slovanského	slovanský	k2eAgNnSc2d1	slovanské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
počeštilo	počeštit	k5eAaPmAgNnS	počeštit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
dobách	doba	k1gFnPc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
listinách	listina	k1gFnPc6	listina
objevuje	objevovat	k5eAaImIp3nS	objevovat
počeštěné	počeštěný	k2eAgNnSc4d1	počeštěné
jméno	jméno	k1gNnSc4	jméno
Žamberk	Žamberk	k1gInSc4	Žamberk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rozdělení	rozdělení	k1gNnSc6	rozdělení
města	město	k1gNnSc2	město
Žamberk	Žamberk	k1gInSc4	Žamberk
na	na	k7c4	na
litickou	litický	k2eAgFnSc4d1	Litická
a	a	k8xC	a
žampašskou	žampašský	k2eAgFnSc4d1	žampašská
polovinu	polovina	k1gFnSc4	polovina
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
již	již	k6eAd1	již
listina	listina	k1gFnSc1	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yQgFnSc6	který
psal	psát	k5eAaImAgMnS	psát
profesor	profesor	k1gMnSc1	profesor
August	August	k1gMnSc1	August
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
c.	c.	k?	c.
k.	k.	k?	k.
dvorním	dvorní	k2eAgInPc3d1	dvorní
archivu	archiv	k1gInSc2	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
litické	litický	k2eAgFnSc6d1	Litická
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
několik	několik	k4yIc1	několik
majitelů	majitel	k1gMnPc2	majitel
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Lichtenburka	Lichtenburek	k1gMnSc2	Lichtenburek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1356	[number]	k4	1356
přes	přes	k7c4	přes
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
(	(	kIx(	(
<g/>
1450	[number]	k4	1450
<g/>
-	-	kIx~	-
<g/>
1471	[number]	k4	1471
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
z	z	k7c2	z
Bubna	Bubno	k1gNnSc2	Bubno
(	(	kIx(	(
<g/>
1575	[number]	k4	1575
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Majiteli	majitel	k1gMnSc3	majitel
žampašské	žampašský	k2eAgFnSc2d1	žampašská
poloviny	polovina	k1gFnSc2	polovina
byli	být	k5eAaImAgMnP	být
Čeněk	Čeněk	k1gMnSc1	Čeněk
Žampach	Žampach	k1gMnSc1	Žampach
z	z	k7c2	z
Potštejna	Potštejn	k1gInSc2	Potštejn
1367	[number]	k4	1367
<g/>
,	,	kIx,	,
Burian	Burian	k1gMnSc1	Burian
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
1513	[number]	k4	1513
a	a	k8xC	a
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
z	z	k7c2	z
Bubna	Bubn	k1gInSc2	Bubn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roku	rok	k1gInSc2	rok
1575	[number]	k4	1575
obě	dva	k4xCgFnPc4	dva
poloviny	polovina	k1gFnSc2	polovina
Žamberka	Žamberka	k1gFnSc1	Žamberka
spojil	spojit	k5eAaPmAgMnS	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Albertova	Albertův	k2eAgFnSc1d1	Albertova
vila	vila	k1gFnSc1	vila
-	-	kIx~	-
Areál	areál	k1gInSc1	areál
léčebny	léčebna	k1gFnSc2	léčebna
Albertinum	Albertinum	k1gInSc4	Albertinum
Českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
barvírna	barvírna	k1gFnSc1	barvírna
a	a	k8xC	a
kovárna	kovárna	k1gFnSc1	kovárna
Divišovo	Divišův	k2eAgNnSc1d1	Divišovo
divadlo	divadlo	k1gNnSc1	divadlo
Kaple	kaple	k1gFnSc2	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
-	-	kIx~	-
ulice	ulice	k1gFnSc2	ulice
U	u	k7c2	u
kapličky	kaplička	k1gFnSc2	kaplička
Kaple	kaple	k1gFnSc2	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Rozálie	Rozálie	k1gFnSc2	Rozálie
-	-	kIx~	-
na	na	k7c6	na
Rozálce	Rozálka	k1gFnSc6	Rozálka
<g/>
/	/	kIx~	/
Kapelském	Kapelský	k2eAgInSc6d1	Kapelský
vrchu	vrch	k1gInSc6	vrch
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
zvaném	zvaný	k2eAgInSc6d1	zvaný
Wachtberg	Wachtberg	k1gInSc4	Wachtberg
Karlovice	Karlovice	k1gFnPc4	Karlovice
(	(	kIx(	(
<g/>
475	[number]	k4	475
m	m	kA	m
<g/>
)	)	kIx)	)
-	-	kIx~	-
kopec	kopec	k1gInSc4	kopec
který	který	k3yQgInSc4	který
využil	využít	k5eAaPmAgInS	využít
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
při	při	k7c6	při
<g />
.	.	kIx.	.
</s>
<s>
obléhání	obléhání	k1gNnSc1	obléhání
hradu	hrad	k1gInSc2	hrad
Žampach	Žampach	k1gInSc1	Žampach
Kašna	kašna	k1gFnSc1	kašna
se	s	k7c7	s
sousoším	sousoší	k1gNnSc7	sousoší
Kentaura	kentaur	k1gMnSc2	kentaur
a	a	k8xC	a
Nymfy	nymfa	k1gFnPc1	nymfa
od	od	k7c2	od
Františka	František	k1gMnSc2	František
Rouse	Rous	k1gMnSc2	Rous
-	-	kIx~	-
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
náměstí	náměstí	k1gNnSc1	náměstí
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
-	-	kIx~	-
ulice	ulice	k1gFnSc2	ulice
Kostelní	kostelní	k2eAgNnPc4d1	kostelní
Orlická	orlický	k2eAgNnPc4d1	Orlické
kasárna	kasárna	k1gNnPc4	kasárna
Pomník	pomník	k1gInSc1	pomník
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Havleny	Havlena	k1gFnSc2	Havlena
-	-	kIx~	-
Vojáčkovy	Vojáčkův	k2eAgFnSc2d1	Vojáčkova
sady	sada	k1gFnSc2	sada
Pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
-	-	kIx~	-
Vojáčkovy	Vojáčkův	k2eAgFnSc2d1	Vojáčkova
sady	sada	k1gFnSc2	sada
Pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
-	-	kIx~	-
náměstí	náměstí	k1gNnSc1	náměstí
Generála	generál	k1gMnSc2	generál
Knopa	Knopa	k1gFnSc1	Knopa
Radnice	radnice	k1gFnSc1	radnice
se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
starou	starý	k2eAgFnSc7d1	stará
hasičskou	hasičský	k2eAgFnSc7d1	hasičská
zbrojnicí	zbrojnice	k1gFnSc7	zbrojnice
-	-	kIx~	-
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
náměstí	náměstí	k1gNnSc1	náměstí
Rozhledna	rozhledna	k1gFnSc1	rozhledna
Rozálka	Rozálka	k1gFnSc1	Rozálka
(	(	kIx(	(
<g/>
Podorlická	podorlický	k2eAgFnSc1d1	Podorlická
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
468	[number]	k4	468
m	m	kA	m
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kapelském	Kapelský	k2eAgInSc6d1	Kapelský
vrchu	vrch	k1gInSc6	vrch
Špitál	špitál	k1gInSc1	špitál
svaté	svatá	k1gFnSc2	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
-	-	kIx~	-
ulice	ulice	k1gFnSc2	ulice
Československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
Zámek	zámek	k1gInSc1	zámek
Žamberk	Žamberk	k1gInSc1	Žamberk
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
-	-	kIx~	-
ulice	ulice	k1gFnSc1	ulice
Československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
Prvním	první	k4xOgInSc7	první
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
podnikem	podnik	k1gInSc7	podnik
v	v	k7c6	v
Žamberku	Žamberk	k1gInSc6	Žamberk
byla	být	k5eAaImAgFnS	být
továrna	továrna	k1gFnSc1	továrna
na	na	k7c6	na
sukna	sukno	k1gNnSc2	sukno
Vonwiller	Vonwiller	k1gInSc1	Vonwiller
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
postavil	postavit	k5eAaPmAgMnS	postavit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
otevřel	otevřít	k5eAaPmAgMnS	otevřít
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Vonwiller	Vonwiller	k1gMnSc1	Vonwiller
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
veřejné	veřejný	k2eAgFnSc2d1	veřejná
soutěže	soutěž	k1gFnSc2	soutěž
vypsané	vypsaný	k2eAgFnSc2d1	vypsaná
baronem	baron	k1gMnSc7	baron
Johnem	John	k1gMnSc7	John
Parishem	Parish	k1gInSc7	Parish
<g/>
.	.	kIx.	.
</s>
<s>
Továrna	továrna	k1gFnSc1	továrna
byla	být	k5eAaImAgFnS	být
dlouho	dlouho	k6eAd1	dlouho
jediným	jediný	k2eAgInSc7d1	jediný
textilním	textilní	k2eAgInSc7d1	textilní
podnikem	podnik	k1gInSc7	podnik
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
nepočítaje	nepočítaje	k7c4	nepočítaje
menší	malý	k2eAgFnPc4d2	menší
provozovny	provozovna	k1gFnPc4	provozovna
a	a	k8xC	a
domácí	domácí	k2eAgMnPc4d1	domácí
tkalce	tkadlec	k1gMnPc4	tkadlec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dodávali	dodávat	k5eAaImAgMnP	dodávat
své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
faktorům	faktor	k1gMnPc3	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
textilní	textilní	k2eAgFnSc4d1	textilní
továrnu	továrna	k1gFnSc4	továrna
Jindřich	Jindřich	k1gMnSc1	Jindřich
Žid	Žid	k1gMnSc1	Žid
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
otevřel	otevřít	k5eAaPmAgMnS	otevřít
hronovský	hronovský	k2eAgMnSc1d1	hronovský
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
textilem	textil	k1gInSc7	textil
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Žid	Žid	k1gMnSc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Agro	Agro	k1gNnSc1	Agro
Žamberk	Žamberk	k1gInSc1	Žamberk
Intergal	Intergal	k1gMnSc1	Intergal
Vrchovina-zrušeno	Vrchovinarušen	k2eAgNnSc1d1	Vrchovina-zrušen
Bühler	Bühler	k1gInSc4	Bühler
CZ	CZ	kA	CZ
ZEZ	ZEZ	kA	ZEZ
SILKO	SILKO	kA	SILKO
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Týniště	týniště	k1gNnSc2	týniště
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
-	-	kIx~	-
Letohrad	letohrad	k1gInSc1	letohrad
(	(	kIx(	(
<g/>
nádraží	nádraží	k1gNnSc1	nádraží
Žamberk	Žamberk	k1gInSc1	Žamberk
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
sousední	sousední	k2eAgFnSc2d1	sousední
obce	obec	k1gFnSc2	obec
Dlouhoňovice	Dlouhoňovice	k1gFnSc2	Dlouhoňovice
<g/>
)	)	kIx)	)
Silnice	silnice	k1gFnSc1	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
MŠ	MŠ	kA	MŠ
Sluníčko	sluníčko	k1gNnSc1	sluníčko
Žamberk	Žamberk	k1gInSc1	Žamberk
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
Generála	generál	k1gMnSc2	generál
Knopa	Knop	k1gMnSc2	Knop
433	[number]	k4	433
MŠ	MŠ	kA	MŠ
Čtyřlístek	čtyřlístek	k1gInSc1	čtyřlístek
Žamberk	Žamberk	k1gInSc1	Žamberk
<g/>
,	,	kIx,	,
Tylova	Tylův	k2eAgFnSc1d1	Tylova
1244	[number]	k4	1244
ZŠ	ZŠ	kA	ZŠ
Žamberk	Žamberk	k1gInSc1	Žamberk
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Nádražní	nádražní	k2eAgMnSc1d1	nádražní
743	[number]	k4	743
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
ZŠ	ZŠ	kA	ZŠ
Žamberk	Žamberk	k1gInSc1	Žamberk
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
581	[number]	k4	581
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Speciální	speciální	k2eAgFnSc1d1	speciální
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Žamberk	Žamberk	k1gInSc1	Žamberk
<g/>
,	,	kIx,	,
Nádražní	nádražní	k2eAgMnSc1d1	nádražní
468	[number]	k4	468
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
Petra	Petr	k1gMnSc2	Petr
Ebena	Eben	k1gMnSc2	Eben
<g/>
,	,	kIx,	,
Žamberk	Žamberk	k1gInSc1	Žamberk
<g/>
,	,	kIx,	,
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
nám.	nám.	k?	nám.
145	[number]	k4	145
[	[	kIx(	[
<g />
.	.	kIx.	.
</s>
<s>
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Žamberk	Žamberk	k1gInSc1	Žamberk
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Nádražní	nádražní	k2eAgFnPc1d1	nádražní
48	[number]	k4	48
Odborné	odborný	k2eAgFnPc1d1	odborná
učiliště	učiliště	k1gNnPc1	učiliště
a	a	k8xC	a
Praktická	praktický	k2eAgFnSc1d1	praktická
škola	škola	k1gFnSc1	škola
Žamberk	Žamberk	k1gInSc1	Žamberk
<g/>
,	,	kIx,	,
Tyršova	Tyršův	k2eAgFnSc1d1	Tyršova
214	[number]	k4	214
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
Žamberk	Žamberk	k1gInSc1	Žamberk
<g/>
,	,	kIx,	,
Zámek	zámek	k1gInSc1	zámek
1	[number]	k4	1
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
-	-	kIx~	-
Československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
472	[number]	k4	472
Rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Prokopa	Prokop	k1gMnSc2	Prokop
Diviše	Diviš	k1gMnSc2	Diviš
-	-	kIx~	-
Helvíkovice	Helvíkovice	k1gFnSc1	Helvíkovice
<g />
.	.	kIx.	.
</s>
<s>
326	[number]	k4	326
Divišovo	Divišův	k2eAgNnSc4d1	Divišovo
divadlo	divadlo	k1gNnSc4	divadlo
Ochotnické	ochotnický	k2eAgNnSc1d1	ochotnické
divadlo	divadlo	k1gNnSc1	divadlo
Žamberk	Žamberk	k1gInSc1	Žamberk
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Poliklinika	poliklinika	k1gFnSc1	poliklinika
Albertinum	Albertinum	k1gInSc4	Albertinum
Žamberk	Žamberk	k1gInSc1	Žamberk
Prokop	Prokop	k1gMnSc1	Prokop
Diviš	Diviš	k1gMnSc1	Diviš
(	(	kIx(	(
<g/>
1698	[number]	k4	1698
<g/>
-	-	kIx~	-
<g/>
1765	[number]	k4	1765
<g/>
)	)	kIx)	)
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
a	a	k8xC	a
konstruktér	konstruktér	k1gMnSc1	konstruktér
bleskosvodu	bleskosvod	k1gInSc2	bleskosvod
Josef	Josef	k1gMnSc1	Josef
Jan	Jan	k1gMnSc1	Jan
Šarapatka	šarapatka	k1gFnSc1	šarapatka
(	(	kIx(	(
<g/>
1731	[number]	k4	1731
<g/>
-	-	kIx~	-
<g/>
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
-	-	kIx~	-
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Karel	Karel	k1gMnSc1	Karel
František	František	k1gMnSc1	František
Rafael	Rafael	k1gMnSc1	Rafael
(	(	kIx(	(
<g/>
1795	[number]	k4	1795
<g/>
-	-	kIx~	-
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
kontrabasista	kontrabasista	k1gMnSc1	kontrabasista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Mazura	Mazura	k1gMnSc1	Mazura
(	(	kIx(	(
<g/>
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
-	-	kIx~	-
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
1850	[number]	k4	1850
zastával	zastávat	k5eAaImAgInS	zastávat
úřad	úřad	k1gInSc1	úřad
představeného	představený	k2eAgNnSc2d1	představené
města	město	k1gNnSc2	město
Theodor	Theodora	k1gFnPc2	Theodora
Brorsen	Brorsna	k1gFnPc2	Brorsna
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
<g/>
-	-	kIx~	-
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
-	-	kIx~	-
dánský	dánský	k2eAgMnSc1d1	dánský
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
botanik	botanik	k1gMnSc1	botanik
Woldemar	Woldemar	k1gMnSc1	Woldemar
Richard	Richard	k1gMnSc1	Richard
Mazura	Mazura	k1gMnSc1	Mazura
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
-	-	kIx~	-
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgMnSc1d1	okresní
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
amatérský	amatérský	k2eAgMnSc1d1	amatérský
fotograf	fotograf	k1gMnSc1	fotograf
Eduard	Eduard	k1gMnSc1	Eduard
Albert	Albert	k1gMnSc1	Albert
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
-	-	kIx~	-
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
chirurg	chirurg	k1gMnSc1	chirurg
na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
"	"	kIx"	"
<g/>
Pamětí	paměť	k1gFnPc2	paměť
žamberských	žamberský	k2eAgFnPc2d1	žamberská
<g/>
"	"	kIx"	"
Karel	Karel	k1gMnSc1	Karel
<g />
.	.	kIx.	.
</s>
<s>
Chotovský	Chotovský	k2eAgMnSc1d1	Chotovský
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
-	-	kIx~	-
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
"	"	kIx"	"
<g/>
Pamětí	paměť	k1gFnPc2	paměť
žamberských	žamberský	k2eAgFnPc2d1	žamberská
<g/>
"	"	kIx"	"
Václav	Václav	k1gMnSc1	Václav
F.	F.	kA	F.
Kumpošt	Kumpošt	k1gMnSc1	Kumpošt
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
-	-	kIx~	-
zakladatel	zakladatel	k1gMnSc1	zakladatel
časopisu	časopis	k1gInSc2	časopis
Vesmír	vesmír	k1gInSc1	vesmír
August	August	k1gMnSc1	August
Seydler	Seydler	k1gMnSc1	Seydler
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
-	-	kIx~	-
fyzik	fyzik	k1gMnSc1	fyzik
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
František	František	k1gMnSc1	František
Albert	Albert	k1gMnSc1	Albert
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
-	-	kIx~	-
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Tereza	Tereza	k1gFnSc1	Tereza
Svatová	Svatová	k1gFnSc1	Svatová
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Jan	Jan	k1gMnSc1	Jan
Vilímek	Vilímek	k1gMnSc1	Vilímek
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
malíř	malíř	k1gMnSc1	malíř
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
portrétů	portrét	k1gInPc2	portrét
celebrit	celebrita	k1gFnPc2	celebrita
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Kateřina	Kateřina	k1gFnSc1	Kateřina
Thomová	Thomová	k1gFnSc1	Thomová
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
význačná	význačný	k2eAgFnSc1d1	význačná
představitelka	představitelka	k1gFnSc1	představitelka
žambereckého	žamberecký	k2eAgNnSc2d1	žamberecký
ochotnického	ochotnický	k2eAgNnSc2d1	ochotnické
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
žambereckého	žamberecký	k2eAgNnSc2d1	žamberecký
muzea	muzeum	k1gNnSc2	muzeum
Jan	Jan	k1gMnSc1	Jan
Hejčl	Hejčl	k1gMnSc1	Hejčl
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
-	-	kIx~	-
děkan	děkan	k1gMnSc1	děkan
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
badatel	badatel	k1gMnSc1	badatel
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
František	František	k1gMnSc1	František
Rous	Rous	k1gMnSc1	Rous
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
-	-	kIx~	-
řezbář	řezbář	k1gMnSc1	řezbář
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Charfreitág	Charfreitág	k1gMnSc1	Charfreitág
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
restauratér	restauratér	k1gMnSc1	restauratér
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
Bedřich	Bedřich	k1gMnSc1	Bedřich
Havlena	Havlena	k1gFnSc1	Havlena
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
italský	italský	k2eAgMnSc1d1	italský
legionář	legionář	k1gMnSc1	legionář
popravený	popravený	k2eAgInSc4d1	popravený
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
rakouském	rakouský	k2eAgNnSc6d1	rakouské
zajetí	zajetí	k1gNnSc6	zajetí
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hajn	Hajn	k1gMnSc1	Hajn
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
-	-	kIx~	-
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
Katedry	katedra	k1gFnSc2	katedra
přesné	přesný	k2eAgFnSc2d1	přesná
mechaniky	mechanika	k1gFnSc2	mechanika
na	na	k7c4	na
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Josef	Josef	k1gMnSc1	Josef
Knop	Knop	k?	Knop
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
-	-	kIx~	-
generál	generál	k1gMnSc1	generál
<g />
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Marek	Marek	k1gMnSc1	Marek
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
-	-	kIx~	-
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
entomolog	entomolog	k1gMnSc1	entomolog
Jiří	Jiří	k1gMnSc1	Jiří
Faltus	Faltus	k1gMnSc1	Faltus
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
-	-	kIx~	-
umělecký	umělecký	k2eAgMnSc1d1	umělecký
knihař	knihař	k1gMnSc1	knihař
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
"	"	kIx"	"
<g/>
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
<g/>
"	"	kIx"	"
Pavel	Pavel	k1gMnSc1	Pavel
Kohn-Kubín	Kohn-Kubín	k1gMnSc1	Kohn-Kubín
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
příslušník	příslušník	k1gMnSc1	příslušník
311	[number]	k4	311
<g/>
.	.	kIx.	.
československé	československý	k2eAgFnSc2d1	Československá
bombardovací	bombardovací	k2eAgFnSc2d1	bombardovací
perutě	peruť	k1gFnSc2	peruť
RAF	raf	k0	raf
Jan	Jan	k1gMnSc1	Jan
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
student	student	k1gMnSc1	student
a	a	k8xC	a
studentský	studentský	k2eAgMnSc1d1	studentský
funkcionář	funkcionář	k1gMnSc1	funkcionář
popravený	popravený	k2eAgMnSc1d1	popravený
nacisty	nacista	k1gMnSc2	nacista
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
Eduard	Eduard	k1gMnSc1	Eduard
Landa	Landa	k1gMnSc1	Landa
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
malíř	malíř	k1gMnSc1	malíř
Petr	Petr	k1gMnSc1	Petr
<g />
.	.	kIx.	.
</s>
<s>
Eben	eben	k1gInSc1	eben
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
skladatel	skladatel	k1gMnSc1	skladatel
soudobé	soudobý	k2eAgFnSc2d1	soudobá
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
Jiří	Jiří	k1gMnSc1	Jiří
Šašek	Šašek	k1gMnSc1	Šašek
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Milan	Milan	k1gMnSc1	Milan
Maryška	Maryška	k1gMnSc1	Maryška
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
-	-	kIx~	-
filmový	filmový	k2eAgMnSc1d1	filmový
dokumentarista	dokumentarista	k1gMnSc1	dokumentarista
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
Fresagrandinaria	Fresagrandinarium	k1gNnSc2	Fresagrandinarium
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
Miharu	Mihar	k1gInSc2	Mihar
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc4	Japonsko
Nowa	Nowum	k1gNnSc2	Nowum
Sól	sólo	k1gNnPc2	sólo
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Püttlingen	Püttlingen	k1gInSc1	Püttlingen
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Rice	Rice	k1gFnPc2	Rice
Lake	Lake	k1gFnPc2	Lake
<g/>
,	,	kIx,	,
Wisconsin	Wisconsina	k1gFnPc2	Wisconsina
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Saint-Michel-sur-Orge	Saint-Michelur-Orge	k1gNnSc1	Saint-Michel-sur-Orge
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Senftenberg	Senftenberg	k1gInSc1	Senftenberg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Senftenberg	Senftenberg	k1gInSc1	Senftenberg
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Veszprém	Veszprý	k2eAgNnSc6d1	Veszprý
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
</s>
