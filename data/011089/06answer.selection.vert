<s>
Třincem	Třinec	k1gInSc7	Třinec
protéká	protékat	k5eAaImIp3nS	protékat
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
řeka	řeka	k1gFnSc1	řeka
Olše	olše	k1gFnSc1	olše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
výtoku	výtok	k1gInSc2	výtok
z	z	k7c2	z
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
