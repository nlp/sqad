<p>
<s>
Trojský	trojský	k2eAgMnSc1d1	trojský
kůň	kůň	k1gMnSc1	kůň
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
předposlední	předposlední	k2eAgInSc4d1	předposlední
akt	akt	k1gInSc4	akt
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgFnSc2d1	trvající
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Skupinka	skupinka	k1gFnSc1	skupinka
obléhajících	obléhající	k2eAgMnPc2d1	obléhající
Řeků	Řek	k1gMnPc2	Řek
(	(	kIx(	(
<g/>
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
Homéra	Homér	k1gMnSc2	Homér
nazývají	nazývat	k5eAaImIp3nP	nazývat
Achájové	Acháj	k1gMnPc1	Acháj
<g/>
,	,	kIx,	,
Danaové	Danaus	k1gMnPc1	Danaus
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Odysseovým	Odysseův	k2eAgNnSc7d1	Odysseovo
se	se	k3xPyFc4	se
ukryla	ukrýt	k5eAaPmAgFnS	ukrýt
do	do	k7c2	do
velkého	velký	k2eAgMnSc2d1	velký
dřevěného	dřevěný	k2eAgMnSc2d1	dřevěný
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
odplul	odplout	k5eAaPmAgInS	odplout
do	do	k7c2	do
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Trójané	Trójan	k1gMnPc1	Trójan
vtáhli	vtáhnout	k5eAaPmAgMnP	vtáhnout
koně	kůň	k1gMnSc4	kůň
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Odysseovi	Odysseův	k2eAgMnPc1d1	Odysseův
lidé	člověk	k1gMnPc1	člověk
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
bránu	brána	k1gFnSc4	brána
a	a	k8xC	a
učinili	učinit	k5eAaPmAgMnP	učinit
tak	tak	k6eAd1	tak
poslední	poslední	k2eAgInSc4d1	poslední
krok	krok	k1gInSc4	krok
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
hrdého	hrdý	k2eAgNnSc2d1	hrdé
města	město	k1gNnSc2	město
Trója	Trója	k1gFnSc1	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
speciálně	speciálně	k6eAd1	speciálně
zkonstruovaný	zkonstruovaný	k2eAgInSc4d1	zkonstruovaný
a	a	k8xC	a
použitý	použitý	k2eAgInSc4d1	použitý
válečný	válečný	k2eAgInSc4d1	válečný
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Řekům	Řek	k1gMnPc3	Řek
posloužil	posloužit	k5eAaPmAgInS	posloužit
jako	jako	k8xS	jako
válečná	válečný	k2eAgFnSc1d1	válečná
lest	lest	k1gFnSc1	lest
a	a	k8xC	a
překvapení	překvapení	k1gNnSc1	překvapení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
stručně	stručně	k6eAd1	stručně
vylíčen	vylíčit	k5eAaPmNgInS	vylíčit
v	v	k7c6	v
Homérově	Homérův	k2eAgFnSc6d1	Homérova
Odysseji	Odyssea	k1gFnSc6	Odyssea
(	(	kIx(	(
<g/>
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
493	[number]	k4	493
<g/>
-	-	kIx~	-
<g/>
515	[number]	k4	515
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rozvinut	rozvinout	k5eAaPmNgInS	rozvinout
ve	v	k7c6	v
Vergiliově	Vergiliův	k2eAgFnSc6d1	Vergiliova
Aeneidě	Aeneida	k1gFnSc6	Aeneida
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
49	[number]	k4	49
<g/>
nn	nn	k?	nn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčina	příčina	k1gFnSc1	příčina
(	(	kIx(	(
<g/>
záminka	záminka	k1gFnSc1	záminka
<g/>
)	)	kIx)	)
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
trojský	trojský	k2eAgMnSc1d1	trojský
princ	princ	k1gMnSc1	princ
Paris	Paris	k1gMnSc1	Paris
unesl	unést	k5eAaPmAgMnS	unést
krásnou	krásný	k2eAgFnSc4d1	krásná
Helenu	Helena	k1gFnSc4	Helena
<g/>
,	,	kIx,	,
manželku	manželka	k1gFnSc4	manželka
spartského	spartský	k2eAgMnSc2d1	spartský
krále	král	k1gMnSc2	král
Meneláa	Meneláus	k1gMnSc2	Meneláus
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
její	její	k3xOp3gFnSc2	její
i	i	k8xC	i
královské	královský	k2eAgFnSc2d1	královská
pokladnice	pokladnice	k1gFnSc2	pokladnice
<g/>
,	,	kIx,	,
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
Řekové	Řek	k1gMnPc1	Řek
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
mykénského	mykénský	k2eAgMnSc2d1	mykénský
krále	král	k1gMnSc2	král
Agamemnona	Agamemnon	k1gMnSc2	Agamemnon
obrovské	obrovský	k2eAgNnSc4d1	obrovské
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Čítalo	čítat	k5eAaImAgNnS	čítat
1184	[number]	k4	1184
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
sto	sto	k4xCgNnSc1	sto
tisíc	tisíc	k4xCgInSc1	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celých	celý	k2eAgNnPc2d1	celé
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
Řekové	Řek	k1gMnPc1	Řek
(	(	kIx(	(
<g/>
Achájci	Achájce	k1gMnPc1	Achájce
<g/>
)	)	kIx)	)
obléhali	obléhat	k5eAaImAgMnP	obléhat
Tróju	Trója	k1gFnSc4	Trója
<g/>
,	,	kIx,	,
drancovali	drancovat	k5eAaImAgMnP	drancovat
její	její	k3xOp3gNnSc4	její
široké	široký	k2eAgNnSc4d1	široké
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Trója	Trója	k1gFnSc1	Trója
statečně	statečně	k6eAd1	statečně
odolávala	odolávat	k5eAaImAgFnS	odolávat
obléhání	obléhání	k1gNnSc4	obléhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Desátý	desátý	k4xOgInSc1	desátý
rok	rok	k1gInSc1	rok
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
desátém	desátý	k4xOgInSc6	desátý
roce	rok	k1gInSc6	rok
již	již	k6eAd1	již
válečníci	válečník	k1gMnPc1	válečník
byli	být	k5eAaImAgMnP	být
unavení	unavení	k1gNnSc4	unavení
a	a	k8xC	a
hovořilo	hovořit	k5eAaImAgNnS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojení	spokojený	k2eNgMnPc1d1	nespokojený
bojovníci	bojovník	k1gMnPc1	bojovník
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
boj	boj	k1gInSc1	boj
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgMnPc7d1	hlavní
jeho	jeho	k3xOp3gFnSc1	jeho
aktéry	aktér	k1gMnPc4	aktér
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
postavili	postavit	k5eAaPmAgMnP	postavit
Meneláos	Meneláos	k1gMnSc1	Meneláos
a	a	k8xC	a
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Jenom	jenom	k9	jenom
zásahem	zásah	k1gInSc7	zásah
bohů	bůh	k1gMnPc2	bůh
nebyl	být	k5eNaImAgMnS	být
Paris	Paris	k1gMnSc1	Paris
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
v	v	k7c6	v
lítých	lítý	k2eAgInPc6d1	lítý
soubojích	souboj	k1gInPc6	souboj
padl	padnout	k5eAaImAgInS	padnout
hrdina	hrdina	k1gMnSc1	hrdina
Patroklos	Patroklosa	k1gFnPc2	Patroklosa
rukou	ruka	k1gFnPc2	ruka
Hektora	Hektor	k1gMnSc2	Hektor
<g/>
,	,	kIx,	,
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
syna	syn	k1gMnSc4	syn
trojského	trojský	k2eAgMnSc4d1	trojský
krále	král	k1gMnSc4	král
Priama	Priamos	k1gMnSc4	Priamos
<g/>
.	.	kIx.	.
</s>
<s>
Patrokla	Patroknout	k5eAaPmAgFnS	Patroknout
pomstil	pomstít	k5eAaPmAgMnS	pomstít
</s>
</p>
<p>
<s>
Achilleus	Achilleus	k1gMnSc1	Achilleus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
zabit	zabít	k5eAaPmNgInS	zabít
podivným	podivný	k2eAgInSc7d1	podivný
zásahem	zásah	k1gInSc7	zásah
Paridovým	Paridův	k2eAgInSc7d1	Paridův
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
války	válka	k1gFnSc2	válka
stále	stále	k6eAd1	stále
nebyl	být	k5eNaImAgMnS	být
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdy	tehdy	k6eAd1	tehdy
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Odysseus	Odysseus	k1gInSc1	Odysseus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
Tróju	Trója	k1gFnSc4	Trója
dobýt	dobýt	k5eAaPmF	dobýt
lstí	lest	k1gFnSc7	lest
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
postavit	postavit	k5eAaPmF	postavit
velkého	velký	k2eAgMnSc4d1	velký
dřevěného	dřevěný	k2eAgMnSc4d1	dřevěný
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
ukryjí	ukrýt	k5eAaPmIp3nP	ukrýt
vybraní	vybraný	k2eAgMnPc1d1	vybraný
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgMnSc4	tento
koně	kůň	k1gMnSc4	kůň
potom	potom	k6eAd1	potom
dostat	dostat	k5eAaPmF	dostat
za	za	k7c4	za
nedobytné	dobytný	k2eNgFnPc4d1	nedobytná
hradby	hradba	k1gFnPc4	hradba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
autora	autor	k1gMnSc4	autor
nápadu	nápad	k1gInSc2	nápad
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgMnSc1d1	považován
také	také	k9	také
věštec	věštec	k1gMnSc1	věštec
Kalchás	Kalchás	k1gInSc1	Kalchás
či	či	k8xC	či
věštec	věštec	k1gMnSc1	věštec
Prylis	Prylis	k1gFnSc2	Prylis
(	(	kIx(	(
<g/>
jemu	on	k3xPp3gMnSc3	on
tu	ten	k3xDgFnSc4	ten
myšlenku	myšlenka	k1gFnSc4	myšlenka
vnukla	vnuknout	k5eAaPmAgFnS	vnuknout
sama	sám	k3xTgFnSc1	sám
Athéna	Athéna	k1gFnSc1	Athéna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
Odysseus	Odysseus	k1gMnSc1	Odysseus
si	se	k3xPyFc3	se
všechny	všechen	k3xTgFnPc4	všechen
zásluhy	zásluha	k1gFnPc4	zásluha
přivlastnil	přivlastnit	k5eAaPmAgMnS	přivlastnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
koně	kůň	k1gMnSc2	kůň
==	==	k?	==
</s>
</p>
<p>
<s>
Stavby	stavba	k1gFnPc1	stavba
dutého	dutý	k2eAgMnSc2d1	dutý
dřevěného	dřevěný	k2eAgMnSc2d1	dřevěný
koně	kůň	k1gMnSc2	kůň
ze	z	k7c2	z
smrkových	smrkový	k2eAgNnPc2d1	smrkové
prken	prkno	k1gNnPc2	prkno
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
zkušený	zkušený	k2eAgMnSc1d1	zkušený
tesař	tesař	k1gMnSc1	tesař
Epeios	Epeios	k1gMnSc1	Epeios
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
boku	bok	k1gInSc6	bok
byly	být	k5eAaImAgFnP	být
skryté	skrytý	k2eAgFnPc1d1	skrytá
dveře	dveře	k1gFnPc1	dveře
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
boku	bok	k1gInSc6	bok
nápis	nápis	k1gInSc1	nápis
s	s	k7c7	s
velikými	veliký	k2eAgNnPc7d1	veliké
rytými	rytý	k2eAgNnPc7d1	ryté
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
na	na	k7c4	na
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
návrat	návrat	k1gInSc4	návrat
domů	domů	k6eAd1	domů
věnují	věnovat	k5eAaPmIp3nP	věnovat
toto	tento	k3xDgNnSc1	tento
vděční	vděčný	k2eAgMnPc1d1	vděčný
Řekové	Řek	k1gMnPc1	Řek
bohyni	bohyně	k1gFnSc4	bohyně
"	"	kIx"	"
(	(	kIx(	(
<g/>
Graves	Graves	k1gInSc1	Graves
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
břicha	břicho	k1gNnSc2	břicho
se	se	k3xPyFc4	se
ukryla	ukrýt	k5eAaPmAgFnS	ukrýt
skupina	skupina	k1gFnSc1	skupina
odvážných	odvážný	k2eAgMnPc2d1	odvážný
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jsou	být	k5eAaImIp3nP	být
uváděny	uváděn	k2eAgInPc4d1	uváděn
různé	různý	k2eAgInPc4d1	různý
počty	počet	k1gInPc4	počet
mužů	muž	k1gMnPc2	muž
počínaje	počínaje	k7c7	počínaje
číslem	číslo	k1gNnSc7	číslo
23	[number]	k4	23
nebo	nebo	k8xC	nebo
30	[number]	k4	30
až	až	k9	až
50	[number]	k4	50
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
nepravděpodobných	pravděpodobný	k2eNgMnPc2d1	nepravděpodobný
tři	tři	k4xCgNnPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
jsou	být	k5eAaImIp3nP	být
jmenování	jmenování	k1gNnSc1	jmenování
Meneláos	Meneláosa	k1gFnPc2	Meneláosa
<g/>
,	,	kIx,	,
Odysseus	Odysseus	k1gMnSc1	Odysseus
<g/>
,	,	kIx,	,
Diomédes	Diomédes	k1gMnSc1	Diomédes
<g/>
,	,	kIx,	,
Sthenelos	Sthenelos	k1gMnSc1	Sthenelos
<g/>
,	,	kIx,	,
Akamás	Akamás	k1gInSc1	Akamás
<g/>
,	,	kIx,	,
Thoás	Thoás	k1gInSc1	Thoás
<g/>
,	,	kIx,	,
Echión	Echión	k1gInSc1	Echión
a	a	k8xC	a
Neoptolemos	Neoptolemos	k1gInSc1	Neoptolemos
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
tam	tam	k6eAd1	tam
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
Epeios	Epeios	k1gInSc1	Epeios
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
za	za	k7c7	za
sebou	se	k3xPyFc7	se
žebřík	žebřík	k1gInSc4	žebřík
a	a	k8xC	a
protože	protože	k8xS	protože
uměl	umět	k5eAaImAgMnS	umět
ovládat	ovládat	k5eAaImF	ovládat
padací	padací	k2eAgFnPc4d1	padací
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
usadil	usadit	k5eAaPmAgMnS	usadit
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
zámku	zámek	k1gInSc2	zámek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Graves	Graves	k1gInSc1	Graves
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Řecká	řecký	k2eAgFnSc1d1	řecká
lest	lest	k1gFnSc1	lest
==	==	k?	==
</s>
</p>
<p>
<s>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
řecká	řecký	k2eAgNnPc1d1	řecké
vojska	vojsko	k1gNnPc1	vojsko
lstivě	lstivě	k6eAd1	lstivě
spálila	spálit	k5eAaPmAgNnP	spálit
tábor	tábor	k1gInSc4	tábor
a	a	k8xC	a
odplula	odplout	k5eAaPmAgFnS	odplout
z	z	k7c2	z
dohledu	dohled	k1gInSc2	dohled
z	z	k7c2	z
trojského	trojský	k2eAgNnSc2d1	Trojské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jediný	jediný	k2eAgMnSc1d1	jediný
Řek	Řek	k1gMnSc1	Řek
Sinón	Sinón	k1gMnSc1	Sinón
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
připravenu	připraven	k2eAgFnSc4d1	připravena
historku	historka	k1gFnSc4	historka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dezertér	dezertér	k1gMnSc1	dezertér
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozednění	rozednění	k1gNnSc6	rozednění
Trójané	Trójan	k1gMnPc1	Trójan
viděli	vidět	k5eAaImAgMnP	vidět
kouřící	kouřící	k2eAgInPc4d1	kouřící
zbytky	zbytek	k1gInPc4	zbytek
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
prázdné	prázdný	k2eAgNnSc1d1	prázdné
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
velikého	veliký	k2eAgMnSc4d1	veliký
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
objevili	objevit	k5eAaPmAgMnP	objevit
Sinóna	Sinón	k1gMnSc4	Sinón
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
jim	on	k3xPp3gMnPc3	on
tvrdil	tvrdit	k5eAaImAgInS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kůň	kůň	k1gMnSc1	kůň
je	být	k5eAaImIp3nS	být
darem	dar	k1gInSc7	dar
bohyni	bohyně	k1gFnSc6	bohyně
Athéně	Athéna	k1gFnSc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Trójané	Trójan	k1gMnPc1	Trójan
se	se	k3xPyFc4	se
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
chtěli	chtít	k5eAaImAgMnP	chtít
okamžitě	okamžitě	k6eAd1	okamžitě
zatáhnout	zatáhnout	k5eAaPmF	zatáhnout
za	za	k7c2	za
městské	městský	k2eAgFnSc2d1	městská
hradby	hradba	k1gFnSc2	hradba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
silné	silný	k2eAgInPc1d1	silný
hlasy	hlas	k1gInPc1	hlas
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
Kassandra	Kassandra	k1gFnSc1	Kassandra
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
koni	kůň	k1gMnSc6	kůň
jsou	být	k5eAaImIp3nP	být
schovaní	schovaný	k2eAgMnPc1d1	schovaný
</s>
</p>
<p>
<s>
ozbrojenci	ozbrojenec	k1gMnPc1	ozbrojenec
–	–	k?	–
zase	zase	k9	zase
jí	jíst	k5eAaImIp3nS	jíst
však	však	k9	však
nikdo	nikdo	k3yNnSc1	nikdo
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc4	týž
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
i	i	k9	i
další	další	k2eAgMnSc1d1	další
věštec	věštec	k1gMnSc1	věštec
Láokoón	Láokoón	k1gMnSc1	Láokoón
a	a	k8xC	a
vrhl	vrhnout	k5eAaImAgMnS	vrhnout
na	na	k7c4	na
koně	kůň	k1gMnSc4	kůň
oštěp	oštěp	k1gInSc4	oštěp
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
zbraně	zbraň	k1gFnPc1	zbraň
vevnitř	vevnitř	k6eAd1	vevnitř
zachrastily	zachrastit	k5eAaPmAgFnP	zachrastit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
prý	prý	k9	prý
řekl	říct	k5eAaPmAgMnS	říct
"	"	kIx"	"
<g/>
Ať	ať	k9	ať
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
cokoliv	cokoliv	k3yInSc1	cokoliv
<g/>
,	,	kIx,	,
bojím	bát	k5eAaImIp1nS	bát
se	se	k3xPyFc4	se
Danaů	Dana	k1gMnPc2	Dana
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
přinášejí	přinášet	k5eAaImIp3nP	přinášet
dary	dar	k1gInPc4	dar
<g/>
"	"	kIx"	"
–	–	k?	–
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
rčení	rčení	k1gNnSc4	rčení
Danajský	danajský	k2eAgInSc1d1	danajský
dar	dar	k1gInSc1	dar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řek	Řek	k1gMnSc1	Řek
Sinón	Sinón	k1gMnSc1	Sinón
před	před	k7c7	před
Trójany	Trójan	k1gMnPc7	Trójan
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
secvičené	secvičený	k2eAgFnSc3d1	secvičená
řeči	řeč	k1gFnSc3	řeč
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
připravil	připravit	k5eAaPmAgInS	připravit
lstivý	lstivý	k2eAgInSc1d1	lstivý
Odysseus	Odysseus	k1gInSc1	Odysseus
<g/>
.	.	kIx.	.
</s>
<s>
Láokoón	Láokoón	k1gMnSc1	Láokoón
stále	stále	k6eAd1	stále
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bůh	bůh	k1gMnSc1	bůh
Apollón	Apollón	k1gMnSc1	Apollón
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
dva	dva	k4xCgInPc4	dva
syny	syn	k1gMnPc7	syn
dva	dva	k4xCgInPc4	dva
obrovské	obrovský	k2eAgFnPc4d1	obrovská
hady	had	k1gMnPc7	had
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
umlčeli	umlčet	k5eAaPmAgMnP	umlčet
<g/>
.	.	kIx.	.
</s>
<s>
Trójané	Trójan	k1gMnPc1	Trójan
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc4	všechen
vzali	vzít	k5eAaPmAgMnP	vzít
jako	jako	k8xS	jako
potvrzení	potvrzení	k1gNnSc4	potvrzení
Sinonových	Sinonův	k2eAgFnPc2d1	Sinonův
řečí	řeč	k1gFnPc2	řeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pak	pak	k6eAd1	pak
už	už	k6eAd1	už
jim	on	k3xPp3gMnPc3	on
nestálo	stát	k5eNaImAgNnS	stát
nic	nic	k3yNnSc1	nic
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
–	–	k?	–
koně	kůň	k1gMnSc4	kůň
dotáhli	dotáhnout	k5eAaPmAgMnP	dotáhnout
za	za	k7c4	za
městské	městský	k2eAgFnPc4d1	městská
hradby	hradba	k1gFnPc4	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Museli	muset	k5eAaImAgMnP	muset
je	on	k3xPp3gInPc4	on
pobořit	pobořit	k5eAaPmF	pobořit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
městskými	městský	k2eAgFnPc7d1	městská
branami	brána	k1gFnPc7	brána
by	by	kYmCp3nS	by
kůň	kůň	k1gMnSc1	kůň
neprošel	projít	k5eNaPmAgInS	projít
<g/>
.	.	kIx.	.
</s>
<s>
Trójané	Trójan	k1gMnPc1	Trójan
se	se	k3xPyFc4	se
radovali	radovat	k5eAaImAgMnP	radovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
město	město	k1gNnSc1	město
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
zachováno	zachovat	k5eAaPmNgNnS	zachovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
vystavěn	vystavět	k5eAaPmNgInS	vystavět
z	z	k7c2	z
trupů	trup	k1gInPc2	trup
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
italského	italský	k2eAgMnSc2d1	italský
námořního	námořní	k2eAgMnSc2d1	námořní
archeologa	archeolog	k1gMnSc2	archeolog
Francesca	Francescus	k1gMnSc2	Francescus
Tiboni	Tiboň	k1gFnSc3	Tiboň
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
Trojským	trojský	k2eAgMnSc7d1	trojský
koněm	kůň	k1gMnSc7	kůň
<g/>
"	"	kIx"	"
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
obchodní	obchodní	k2eAgFnSc1d1	obchodní
loď	loď	k1gFnSc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Tiboniho	Tiboni	k1gMnSc2	Tiboni
se	se	k3xPyFc4	se
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
kůň	kůň	k1gMnSc1	kůň
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
hroch	hroch	k1gMnSc1	hroch
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
oba	dva	k4xCgInPc4	dva
pojmy	pojem	k1gInPc4	pojem
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
slovo	slovo	k1gNnSc1	slovo
hippos	hipposa	k1gFnPc2	hipposa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgMnS	používat
přeneseně	přeneseně	k6eAd1	přeneseně
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
obchodní	obchodní	k2eAgFnSc2d1	obchodní
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
na	na	k7c6	na
přídi	příď	k1gFnSc6	příď
zdobila	zdobit	k5eAaImAgFnS	zdobit
galionová	galionový	k2eAgFnSc1d1	galionový
figura	figura	k1gFnSc1	figura
připomínající	připomínající	k2eAgFnSc4d1	připomínající
koňskou	koňský	k2eAgFnSc4d1	koňská
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Řecký	řecký	k2eAgMnSc1d1	řecký
geograf	geograf	k1gMnSc1	geograf
Pausanias	Pausanias	k1gMnSc1	Pausanias
už	už	k6eAd1	už
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
století	století	k1gNnSc6	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nevěrohodné	věrohodný	k2eNgNnSc1d1	nevěrohodné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Řeky	Řek	k1gMnPc4	Řek
napadlo	napadnout	k5eAaPmAgNnS	napadnout
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
dar	dar	k1gInSc4	dar
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
socha	socha	k1gFnSc1	socha
koně	kůň	k1gMnSc2	kůň
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
jen	jen	k9	jen
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
Tiboni	Tibon	k1gMnPc1	Tibon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
athénské	athénský	k2eAgFnSc2d1	Athénská
archeoložky	archeoložka	k1gFnSc2	archeoložka
Eleny	Elena	k1gFnSc2	Elena
Stylianouové	Stylianouová	k1gFnSc2	Stylianouová
však	však	k9	však
tak	tak	k6eAd1	tak
nebo	nebo	k8xC	nebo
tak	tak	k6eAd1	tak
už	už	k6eAd1	už
dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
oddělit	oddělit	k5eAaPmF	oddělit
fakta	faktum	k1gNnPc4	faktum
od	od	k7c2	od
fikce	fikce	k1gFnSc2	fikce
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
ne	ne	k9	ne
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
řeč	řeč	k1gFnSc1	řeč
o	o	k7c6	o
legendě	legenda	k1gFnSc6	legenda
založené	založený	k2eAgFnPc1d1	založená
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
3000	[number]	k4	3000
let	léto	k1gNnPc2	léto
starém	starý	k2eAgNnSc6d1	staré
eposu	epos	k1gInSc6	epos
–	–	k?	–
jehož	jehož	k3xOyRp3gFnSc4	jehož
autentičnost	autentičnost	k1gFnSc4	autentičnost
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
rovněž	rovněž	k9	rovněž
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zkáza	zkáza	k1gFnSc1	zkáza
je	být	k5eAaImIp3nS	být
završena	završen	k2eAgFnSc1d1	završena
==	==	k?	==
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
lidé	člověk	k1gMnPc1	člověk
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
usnuli	usnout	k5eAaPmAgMnP	usnout
<g/>
,	,	kIx,	,
bojovníci	bojovník	k1gMnPc1	bojovník
opustili	opustit	k5eAaPmAgMnP	opustit
břicho	břicho	k1gNnSc4	břicho
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Echión	Echión	k1gMnSc1	Echión
vyskočil	vyskočit	k5eAaPmAgMnS	vyskočit
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
upadl	upadnout	k5eAaPmAgMnS	upadnout
a	a	k8xC	a
zlomil	zlomit	k5eAaPmAgMnS	zlomit
si	se	k3xPyFc3	se
vaz	vaz	k1gInSc4	vaz
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
vylezli	vylézt	k5eAaPmAgMnP	vylézt
po	po	k7c6	po
provazovém	provazový	k2eAgInSc6d1	provazový
žebříku	žebřík	k1gInSc6	žebřík
<g/>
,	,	kIx,	,
běželi	běžet	k5eAaImAgMnP	běžet
otevřít	otevřít	k5eAaPmF	otevřít
brány	brána	k1gFnPc4	brána
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
pobíjeli	pobíjet	k5eAaImAgMnP	pobíjet
stráže	stráž	k1gFnSc2	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Sinón	Sinón	k1gMnSc1	Sinón
dal	dát	k5eAaPmAgMnS	dát
ohňové	ohňový	k2eAgNnSc4d1	ohňové
znamení	znamení	k1gNnSc4	znamení
vojsku	vojsko	k1gNnSc3	vojsko
a	a	k8xC	a
loďstvu	loďstvo	k1gNnSc3	loďstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
k	k	k7c3	k
Tróji	Trója	k1gFnSc3	Trója
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Té	ten	k3xDgFnSc3	ten
noci	noc	k1gFnSc3	noc
byli	být	k5eAaImAgMnP	být
krutě	krutě	k6eAd1	krutě
a	a	k8xC	a
nemilosrdně	milosrdně	k6eNd1	milosrdně
vražděni	vražděn	k2eAgMnPc1d1	vražděn
obyvatelé	obyvatel	k1gMnPc1	obyvatel
a	a	k8xC	a
zejména	zejména	k9	zejména
členové	člen	k1gMnPc1	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
z	z	k7c2	z
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
nezachránil	zachránit	k5eNaPmAgMnS	zachránit
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
byly	být	k5eAaImAgFnP	být
vzaty	vzít	k5eAaPmNgFnP	vzít
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Trója	Trója	k1gFnSc1	Trója
lehla	lehnout	k5eAaPmAgFnS	lehnout
popelem	popel	k1gInSc7	popel
<g/>
.	.	kIx.	.
</s>
<s>
Trojská	trojský	k2eAgFnSc1d1	Trojská
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přenesený	přenesený	k2eAgInSc1d1	přenesený
význam	význam	k1gInSc1	význam
úsloví	úsloví	k1gNnSc2	úsloví
Trojský	trojský	k2eAgMnSc1d1	trojský
kůň	kůň	k1gMnSc1	kůň
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Trojský	trojský	k2eAgMnSc1d1	trojský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
metafory	metafora	k1gFnSc2	metafora
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
propašování	propašování	k1gNnSc6	propašování
něčeho	něco	k3yInSc2	něco
škodlivého	škodlivý	k2eAgNnSc2d1	škodlivé
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
jevilo	jevit	k5eAaImAgNnS	jevit
jako	jako	k9	jako
užitečné	užitečný	k2eAgNnSc1d1	užitečné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejčastější	častý	k2eAgNnSc4d3	nejčastější
užití	užití	k1gNnSc4	užití
slova	slovo	k1gNnSc2	slovo
Trojský	trojský	k2eAgMnSc1d1	trojský
Kůň	kůň	k1gMnSc1	kůň
představuje	představovat	k5eAaImIp3nS	představovat
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
označování	označování	k1gNnSc3	označování
za	za	k7c4	za
Trojského	trojský	k2eAgMnSc4d1	trojský
koně	kůň	k1gMnSc4	kůň
i	i	k8xC	i
příliv	příliv	k1gInSc1	příliv
uprchlíků	uprchlík	k1gMnPc2	uprchlík
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Trojský	trojský	k2eAgMnSc1d1	trojský
kůň	kůň	k1gMnSc1	kůň
v	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
tvorbě	tvorba	k1gFnSc6	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
-Trojský	-Trojský	k2eAgMnSc1d1	-Trojský
kůň	kůň	k1gMnSc1	kůň
–	–	k?	–
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
Bezinky	bezinka	k1gFnSc2	bezinka
</s>
</p>
<p>
<s>
Filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
-Kolik	-Kolik	k1gMnSc1	-Kolik
váží	vážit	k5eAaImIp3nS	vážit
Trojský	trojský	k2eAgMnSc1d1	trojský
kůň	kůň	k1gMnSc1	kůň
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
-Dobrodružství	-Dobrodružství	k1gNnSc1	-Dobrodružství
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
<g/>
:	:	kIx,	:
Trojský	trojský	k2eAgMnSc1d1	trojský
kůň	kůň	k1gMnSc1	kůň
–	–	k?	–
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
</s>
</p>
<p>
<s>
Graves	Graves	k1gMnSc1	Graves
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
Řecké	řecký	k2eAgInPc4d1	řecký
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7309-153-4	[number]	k4	80-7309-153-4
</s>
</p>
<p>
<s>
Houtzager	Houtzager	k1gInSc1	Houtzager
<g/>
,	,	kIx,	,
Guus	Guus	k1gInSc1	Guus
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7234-287-8	[number]	k4	80-7234-287-8
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Löwe	Löw	k1gInSc2	Löw
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnSc2	antika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trojský	trojský	k2eAgMnSc1d1	trojský
kůň	kůň	k1gMnSc1	kůň
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
