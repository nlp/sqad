<p>
<s>
Lingvistika	lingvistika	k1gFnSc1	lingvistika
dělí	dělit	k5eAaImIp3nS	dělit
slova	slovo	k1gNnPc4	slovo
do	do	k7c2	do
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vymezení	vymezení	k1gNnSc6	vymezení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
hrají	hrát	k5eAaImIp3nP	hrát
úlohu	úloha	k1gFnSc4	úloha
různé	různý	k2eAgInPc1d1	různý
faktory	faktor	k1gInPc1	faktor
<g/>
:	:	kIx,	:
jakým	jaký	k3yRgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
do	do	k7c2	do
věty	věta	k1gFnSc2	věta
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc1	jaký
tvary	tvar	k1gInPc1	tvar
tvoří	tvořit	k5eAaImIp3nP	tvořit
a	a	k8xC	a
jaké	jaký	k3yRgFnPc1	jaký
mluvnické	mluvnický	k2eAgFnPc1d1	mluvnická
kategorie	kategorie	k1gFnPc1	kategorie
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaImF	vysledovat
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Tradiční	tradiční	k2eAgInPc1d1	tradiční
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
následujících	následující	k2eAgInPc2d1	následující
10	[number]	k4	10
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
(	(	kIx(	(
<g/>
Substantiva	substantivum	k1gNnSc2	substantivum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
(	(	kIx(	(
<g/>
Adjektiva	adjektivum	k1gNnSc2	adjektivum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zájmena	zájmeno	k1gNnPc1	zájmeno
(	(	kIx(	(
<g/>
Pronomina	pronomen	k1gNnSc2	pronomen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
číslovky	číslovka	k1gFnPc1	číslovka
(	(	kIx(	(
<g/>
Numeralia	Numeralia	k1gFnSc1	Numeralia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
slovesa	sloveso	k1gNnSc2	sloveso
(	(	kIx(	(
<g/>
Verba	verbum	k1gNnSc2	verbum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
příslovce	příslovce	k1gNnSc1	příslovce
(	(	kIx(	(
<g/>
Adverbia	adverbium	k1gNnPc4	adverbium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
předložky	předložka	k1gFnPc1	předložka
(	(	kIx(	(
<g/>
Prepozice	prepozice	k1gFnPc1	prepozice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
spojky	spojka	k1gFnPc1	spojka
(	(	kIx(	(
<g/>
Konjunkce	konjunkce	k1gFnPc1	konjunkce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
částice	částice	k1gFnSc1	částice
(	(	kIx(	(
<g/>
Partikule	partikule	k1gFnSc1	partikule
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
citoslovce	citoslovce	k1gNnSc1	citoslovce
(	(	kIx(	(
<g/>
Interjekce	interjekce	k1gFnPc1	interjekce
<g/>
)	)	kIx)	)
<g/>
Slova	slovo	k1gNnPc1	slovo
prvních	první	k4xOgNnPc6	první
pěti	pět	k4xCc2	pět
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
ohebná	ohebný	k2eAgNnPc1d1	ohebné
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
skloňovat	skloňovat	k5eAaImF	skloňovat
nebo	nebo	k8xC	nebo
časovat	časovat	k5eAaBmF	časovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
podle	podle	k7c2	podle
vzorů	vzor	k1gInPc2	vzor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
číslovky	číslovka	k1gFnPc1	číslovka
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
přejatá	přejatý	k2eAgNnPc1d1	přejaté
substantiva	substantivum	k1gNnPc1	substantivum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
šapitó	šapitó	k1gNnSc1	šapitó
<g/>
,	,	kIx,	,
pyré	pyré	k1gNnSc1	pyré
<g/>
,	,	kIx,	,
filé	filé	k1gNnSc1	filé
<g/>
)	)	kIx)	)
a	a	k8xC	a
adjektiva	adjektivum	k1gNnSc2	adjektivum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lila	lila	k2eAgInPc1d1	lila
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nesklonná	sklonný	k2eNgFnSc1d1	nesklonná
<g/>
.	.	kIx.	.
</s>
<s>
Neohebné	ohebný	k2eNgInPc1d1	neohebný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
všechny	všechen	k3xTgInPc1	všechen
infinitivy	infinitiv	k1gInPc1	infinitiv
českých	český	k2eAgNnPc2d1	české
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
zbývajících	zbývající	k2eAgMnPc2d1	zbývající
pěti	pět	k4xCc2	pět
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
neohebná	ohebný	k2eNgNnPc1d1	neohebné
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
stupňování	stupňování	k1gNnSc3	stupňování
některých	některý	k3yIgNnPc2	některý
příslovcí	příslovce	k1gNnPc2	příslovce
a	a	k8xC	a
osobní	osobní	k2eAgInPc4d1	osobní
tvary	tvar	k1gInPc4	tvar
spojek	spojka	k1gFnPc2	spojka
abych	aby	kYmCp1nS	aby
<g/>
,	,	kIx,	,
abys	aby	kYmCp2nS	aby
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Ta	ten	k3xDgNnPc1	ten
slova	slovo	k1gNnPc1	slovo
prvních	první	k4xOgInPc2	první
čtyř	čtyři	k4xCgInPc2	čtyři
slovních	slovní	k2eAgMnPc2d1	slovní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
gramatické	gramatický	k2eAgFnPc1d1	gramatická
kategorie	kategorie	k1gFnPc1	kategorie
pádu	pád	k1gInSc2	pád
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
jmenného	jmenný	k2eAgInSc2d1	jmenný
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
a	a	k8xC	a
sousloví	sousloví	k1gNnPc1	sousloví
vzniklá	vzniklý	k2eAgNnPc1d1	vzniklé
jejich	jejich	k3xOp3gFnPc7	jejich
rozvinutím	rozvinutí	k1gNnSc7	rozvinutí
se	se	k3xPyFc4	se
souhrnně	souhrnně	k6eAd1	souhrnně
nazývají	nazývat	k5eAaImIp3nP	nazývat
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
číslovek	číslovka	k1gFnPc2	číslovka
jsou	být	k5eAaImIp3nP	být
jmény	jméno	k1gNnPc7	jméno
číslovky	číslovka	k1gFnSc2	číslovka
základní	základní	k2eAgFnSc2d1	základní
<g/>
,	,	kIx,	,
řadové	řadový	k2eAgFnSc2d1	řadová
a	a	k8xC	a
druhové	druhový	k2eAgFnSc2d1	druhová
<g/>
.	.	kIx.	.
ale	ale	k8xC	ale
číslovky	číslovka	k1gFnPc1	číslovka
násobné	násobný	k2eAgFnPc1d1	násobná
mají	mít	k5eAaImIp3nP	mít
charakter	charakter	k1gInSc4	charakter
příslovce	příslovce	k1gNnSc2	příslovce
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
jmenná	jmenný	k2eAgNnPc1d1	jmenné
sousloví	sousloví	k1gNnPc1	sousloví
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
obecná	obecný	k2eAgNnPc4d1	obecné
jména	jméno	k1gNnPc4	jméno
neboli	neboli	k8xC	neboli
apelativa	apelativum	k1gNnPc4	apelativum
a	a	k8xC	a
vlastní	vlastní	k2eAgNnPc4d1	vlastní
jména	jméno	k1gNnPc4	jméno
neboli	neboli	k8xC	neboli
propria	proprium	k1gNnPc4	proprium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
kritéria	kritérion	k1gNnPc1	kritérion
pro	pro	k7c4	pro
řazení	řazení	k1gNnSc4	řazení
slov	slovo	k1gNnPc2	slovo
k	k	k7c3	k
tradičním	tradiční	k2eAgMnPc3d1	tradiční
slovním	slovní	k2eAgMnPc3d1	slovní
druhům	druh	k1gMnPc3	druh
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
povahy	povaha	k1gFnPc1	povaha
<g/>
;	;	kIx,	;
mnohdy	mnohdy	k6eAd1	mnohdy
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
definice	definice	k1gFnPc1	definice
mlhavé	mlhavý	k2eAgFnPc1d1	mlhavá
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
kategorie	kategorie	k1gFnPc1	kategorie
se	se	k3xPyFc4	se
při	při	k7c6	při
pohledech	pohled	k1gInPc6	pohled
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
hledisek	hledisko	k1gNnPc2	hledisko
mohou	moct	k5eAaImIp3nP	moct
překrývat	překrývat	k5eAaImF	překrývat
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zařazení	zařazení	k1gNnSc4	zařazení
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
slova	slovo	k1gNnSc2	slovo
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
přídavnými	přídavný	k2eAgNnPc7d1	přídavné
jmény	jméno	k1gNnPc7	jméno
a	a	k8xC	a
příslovci	příslovce	k1gNnPc7	příslovce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
morfologický	morfologický	k2eAgInSc4d1	morfologický
<g/>
:	:	kIx,	:
adjektiva	adjektivum	k1gNnPc1	adjektivum
jsou	být	k5eAaImIp3nP	být
ohebná	ohebný	k2eAgNnPc1d1	ohebné
<g/>
,	,	kIx,	,
příslovce	příslovce	k1gNnPc4	příslovce
nikoli	nikoli	k9	nikoli
<g/>
,	,	kIx,	,
i	i	k8xC	i
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
<g/>
:	:	kIx,	:
funkcí	funkce	k1gFnPc2	funkce
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
<g/>
)	)	kIx)	)
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
příslovce	příslovce	k1gNnPc1	příslovce
obvykle	obvykle	k6eAd1	obvykle
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
slovesa	sloveso	k1gNnPc4	sloveso
nebo	nebo	k8xC	nebo
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Kritéria	kritérion	k1gNnPc1	kritérion
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
morfologická	morfologický	k2eAgFnSc1d1	morfologická
(	(	kIx(	(
<g/>
které	který	k3yRgInPc1	který
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
ohebné	ohebný	k2eAgInPc1d1	ohebný
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
pád	pád	k1gInSc1	pád
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
čas	čas	k1gInSc4	čas
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Morfologie	morfologie	k1gFnSc2	morfologie
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgInPc2d1	různý
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
velmi	velmi	k6eAd1	velmi
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
morfologická	morfologický	k2eAgNnPc1d1	morfologické
kritéria	kritérion	k1gNnPc1	kritérion
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
univerzální	univerzální	k2eAgInPc1d1	univerzální
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
vymezeny	vymezen	k2eAgFnPc1d1	vymezena
převážně	převážně	k6eAd1	převážně
sémanticky	sémanticky	k6eAd1	sémanticky
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
významu	význam	k1gInSc2	význam
<g/>
:	:	kIx,	:
třeba	třeba	k6eAd1	třeba
číslovky	číslovka	k1gFnPc1	číslovka
podle	podle	k7c2	podle
tradičního	tradiční	k2eAgNnSc2d1	tradiční
pojetí	pojetí	k1gNnSc2	pojetí
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
jsou	být	k5eAaImIp3nP	být
všechna	všechen	k3xTgNnPc4	všechen
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
udávají	udávat	k5eAaImIp3nP	udávat
počet	počet	k1gInSc4	počet
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
teoreticky	teoreticky	k6eAd1	teoreticky
tvořit	tvořit	k5eAaImF	tvořit
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
přirozená	přirozený	k2eAgNnPc4d1	přirozené
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
tedy	tedy	k9	tedy
nejen	nejen	k6eAd1	nejen
číslovky	číslovka	k1gFnPc4	číslovka
základní	základní	k2eAgFnPc4d1	základní
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc4	jeden
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
řadové	řadový	k2eAgNnSc1d1	řadové
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
syntaktického	syntaktický	k2eAgNnSc2d1	syntaktické
hlediska	hledisko	k1gNnSc2	hledisko
přídavnými	přídavný	k2eAgNnPc7d1	přídavné
jmény	jméno	k1gNnPc7	jméno
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
příslovci	příslovce	k1gNnSc6	příslovce
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
potřetí	potřetí	k4xO	potřetí
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
násobné	násobný	k2eAgNnSc1d1	násobné
(	(	kIx(	(
<g/>
syntakticky	syntakticky	k6eAd1	syntakticky
opět	opět	k6eAd1	opět
příslovce	příslovce	k1gNnSc1	příslovce
<g/>
:	:	kIx,	:
jedenkrát	jedenkrát	k6eAd1	jedenkrát
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
faktorem	faktor	k1gInSc7	faktor
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
každý	každý	k3xTgInSc4	každý
druh	druh	k1gInSc4	druh
definujeme	definovat	k5eAaBmIp1nP	definovat
odlišně	odlišně	k6eAd1	odlišně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc1	systém
udržuje	udržovat	k5eAaImIp3nS	udržovat
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
v	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
přitom	přitom	k6eAd1	přitom
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tradice	tradice	k1gFnSc1	tradice
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
základní	základní	k2eAgFnSc2d1	základní
i	i	k8xC	i
řadové	řadový	k2eAgFnSc2d1	řadová
číslovky	číslovka	k1gFnSc2	číslovka
tradičně	tradičně	k6eAd1	tradičně
řadí	řadit	k5eAaImIp3nS	řadit
pod	pod	k7c4	pod
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
(	(	kIx(	(
<g/>
číslovky	číslovka	k1gFnPc1	číslovka
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
příslovce	příslovce	k1gNnSc2	příslovce
nelze	lze	k6eNd1	lze
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jedním	jeden	k4xCgNnSc7	jeden
slovem	slovo	k1gNnSc7	slovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
formální	formální	k2eAgFnSc2d1	formální
lingvistické	lingvistický	k2eAgFnSc2d1	lingvistická
analýzy	analýza	k1gFnSc2	analýza
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
rozlišit	rozlišit	k5eAaPmF	rozlišit
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
(	(	kIx(	(
<g/>
typy	typ	k1gInPc4	typ
<g/>
)	)	kIx)	)
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
povahu	povaha	k1gFnSc4	povaha
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
jsou	být	k5eAaImIp3nP	být
vymezeny	vymezen	k2eAgFnPc1d1	vymezena
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
tradiční	tradiční	k2eAgInPc1d1	tradiční
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
–	–	k?	–
učí	učit	k5eAaImIp3nP	učit
se	se	k3xPyFc4	se
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
výše	vysoce	k6eAd2	vysoce
</s>
</p>
<p>
<s>
morfologické	morfologický	k2eAgInPc1d1	morfologický
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
–	–	k?	–
vymezeny	vymezen	k2eAgInPc1d1	vymezen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mluvnických	mluvnický	k2eAgFnPc2d1	mluvnická
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
ohýbají	ohýbat	k5eAaImIp3nP	ohýbat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
by	by	kYmCp3nS	by
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
jazyce	jazyk	k1gInSc6	jazyk
jiné	jiný	k2eAgNnSc1d1	jiné
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
češtinu	čeština	k1gFnSc4	čeština
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
:	:	kIx,	:
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
pád	pád	k1gInSc1	pád
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
kategorii	kategorie	k1gFnSc4	kategorie
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
převážně	převážně	k6eAd1	převážně
lexikálně	lexikálně	k6eAd1	lexikálně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
i	i	k9	i
některá	některý	k3yIgNnPc1	některý
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc4	nic
<g/>
,	,	kIx,	,
a	a	k8xC	a
číslovky	číslovka	k1gFnPc1	číslovka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pět	pět	k4xCc4	pět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
<g/>
:	:	kIx,	:
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
pád	pád	k1gInSc1	pád
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
stupeň	stupeň	k1gInSc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
i	i	k9	i
některá	některý	k3yIgNnPc1	některý
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jaký	jaký	k3yRgInSc4	jaký
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc4d1	základní
číslovku	číslovka	k1gFnSc4	číslovka
jeden	jeden	k4xCgMnSc1	jeden
/	/	kIx~	/
jedna	jeden	k4xCgFnSc1	jeden
/	/	kIx~	/
jedno	jeden	k4xCgNnSc1	jeden
a	a	k8xC	a
řadové	řadový	k2eAgFnPc1d1	řadová
číslovky	číslovka	k1gFnPc1	číslovka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
osobní	osobní	k2eAgNnPc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
:	:	kIx,	:
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
(	(	kIx(	(
<g/>
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zájmena	zájmeno	k1gNnPc1	zájmeno
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
bezrodá	bezrodý	k2eAgNnPc1d1	bezrodý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
pád	pád	k1gInSc1	pád
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
výraz	výraz	k1gInSc4	výraz
"	"	kIx"	"
<g/>
ohýbání	ohýbání	k1gNnSc2	ohýbání
<g/>
"	"	kIx"	"
není	být	k5eNaImIp3nS	být
moc	moc	k6eAd1	moc
výstižný	výstižný	k2eAgInSc1d1	výstižný
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
koncovek	koncovka	k1gFnPc2	koncovka
podobný	podobný	k2eAgInSc4d1	podobný
podstatným	podstatný	k2eAgInSc7d1	podstatný
a	a	k8xC	a
přídavným	přídavný	k2eAgNnPc3d1	přídavné
jménům	jméno	k1gNnPc3	jméno
zde	zde	k6eAd1	zde
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
přivlastňovací	přivlastňovací	k2eAgNnPc1d1	přivlastňovací
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
:	:	kIx,	:
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
a	a	k8xC	a
číslo	číslo	k1gNnSc1	číslo
vlastníka	vlastník	k1gMnSc2	vlastník
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
a	a	k8xC	a
číslo	číslo	k1gNnSc1	číslo
vlastněného	vlastněný	k2eAgInSc2d1	vlastněný
<g/>
,	,	kIx,	,
pád	pád	k1gInSc1	pád
</s>
</p>
<p>
<s>
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
:	:	kIx,	:
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
slovesný	slovesný	k2eAgInSc1d1	slovesný
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
(	(	kIx(	(
<g/>
u	u	k7c2	u
příčestí	příčestí	k1gNnSc2	příčestí
<g/>
)	)	kIx)	)
též	též	k9	též
jmenný	jmenný	k2eAgInSc1d1	jmenný
rod	rod	k1gInSc1	rod
</s>
</p>
<p>
<s>
stupňovaná	stupňovaný	k2eAgNnPc1d1	stupňované
příslovce	příslovce	k1gNnPc1	příslovce
<g/>
:	:	kIx,	:
např.	např.	kA	např.
hodně	hodně	k6eAd1	hodně
<g/>
,	,	kIx,	,
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
chytře	chytro	k6eAd1	chytro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
už	už	k6eAd1	už
třeba	třeba	k6eAd1	třeba
jak	jak	k6eAd1	jak
nebo	nebo	k8xC	nebo
zítra	zítra	k6eAd1	zítra
</s>
</p>
<p>
<s>
neohebná	ohebný	k2eNgNnPc1d1	neohebné
slova	slovo	k1gNnPc1	slovo
</s>
</p>
<p>
<s>
syntaktické	syntaktický	k2eAgInPc1d1	syntaktický
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
–	–	k?	–
vymezeny	vymezen	k2eAgInPc1d1	vymezen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
funkce	funkce	k1gFnSc2	funkce
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
:	:	kIx,	:
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
sloveso	sloveso	k1gNnSc4	sloveso
zpravidla	zpravidla	k6eAd1	zpravidla
jako	jako	k8xS	jako
podmět	podmět	k1gInSc4	podmět
nebo	nebo	k8xC	nebo
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
slova	slovo	k1gNnPc1	slovo
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
doplňku	doplněk	k1gInSc2	doplněk
nebo	nebo	k8xC	nebo
jmenné	jmenný	k2eAgFnSc2d1	jmenná
části	část	k1gFnSc2	část
přísudku	přísudek	k1gInSc2	přísudek
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
neshodného	shodný	k2eNgInSc2d1	neshodný
přívlastku	přívlastek	k1gInSc2	přívlastek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgNnPc2d1	tradiční
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
zájmena	zájmeno	k1gNnPc1	zájmeno
osobní	osobní	k2eAgNnPc1d1	osobní
(	(	kIx(	(
<g/>
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některá	některý	k3yIgFnSc1	některý
neurčitá	určitý	k2eNgFnSc1d1	neurčitá
a	a	k8xC	a
záporná	záporný	k2eAgFnSc1d1	záporná
(	(	kIx(	(
<g/>
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukazovací	ukazovací	k2eAgNnPc1d1	ukazovací
zájmena	zájmeno	k1gNnPc1	zájmeno
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
kontextech	kontext	k1gInPc6	kontext
(	(	kIx(	(
<g/>
např.	např.	kA	např.
to	ten	k3xDgNnSc4	ten
bych	by	kYmCp1nS	by
neřekl	říct	k5eNaPmAgMnS	říct
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
základní	základní	k2eAgFnPc1d1	základní
číslovky	číslovka	k1gFnPc1	číslovka
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
pádech	pád	k1gInPc6	pád
(	(	kIx(	(
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
pět	pět	k4xCc4	pět
<g/>
,	,	kIx,	,
deset	deset	k4xCc4	deset
<g/>
,	,	kIx,	,
sto	sto	k4xCgNnSc4	sto
<g/>
,	,	kIx,	,
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
,	,	kIx,	,
milión	milión	k4xCgInSc4	milión
<g/>
,	,	kIx,	,
osmina	osmina	k1gFnSc1	osmina
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
<g/>
:	:	kIx,	:
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
morfologicky	morfologicky	k6eAd1	morfologicky
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c6	v
rodě	rod	k1gInSc6	rod
<g/>
,	,	kIx,	,
čísle	číslo	k1gNnSc6	číslo
a	a	k8xC	a
pádu	pád	k1gInSc6	pád
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
slova	slovo	k1gNnPc4	slovo
ve	v	k7c6	v
jmenné	jmenný	k2eAgFnSc6d1	jmenná
části	část	k1gFnSc6	část
přísudku	přísudek	k1gInSc2	přísudek
nebo	nebo	k8xC	nebo
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
doplňku	doplněk	k1gInSc2	doplněk
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
morfologicky	morfologicky	k6eAd1	morfologicky
shodují	shodovat	k5eAaImIp3nP	shodovat
s	s	k7c7	s
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
v	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
klauzi	klauze	k1gFnSc6	klauze
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgNnPc2d1	tradiční
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
zájmena	zájmeno	k1gNnPc1	zájmeno
přivlastňovací	přivlastňovací	k2eAgNnPc1d1	přivlastňovací
(	(	kIx(	(
<g/>
můj	můj	k3xOp1gMnSc1	můj
<g/>
,	,	kIx,	,
tvůj	tvůj	k1gMnSc1	tvůj
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukazovací	ukazovací	k2eAgNnPc1d1	ukazovací
zájmena	zájmeno	k1gNnPc1	zájmeno
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
kontextech	kontext	k1gInPc6	kontext
(	(	kIx(	(
<g/>
např.	např.	kA	např.
to	ten	k3xDgNnSc4	ten
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některá	některý	k3yIgFnSc1	některý
neurčitá	určitý	k2eNgFnSc1d1	neurčitá
a	a	k8xC	a
záporná	záporný	k2eAgFnSc1d1	záporná
(	(	kIx(	(
<g/>
některý	některý	k3yIgMnSc1	některý
<g/>
,	,	kIx,	,
nějaký	nějaký	k3yIgMnSc1	nějaký
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgMnSc1	žádný
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgFnPc4d1	zbývající
základní	základní	k2eAgFnPc4d1	základní
a	a	k8xC	a
adjektivní	adjektivní	k2eAgFnPc4d1	adjektivní
řadové	řadový	k2eAgFnPc4d1	řadová
číslovky	číslovka	k1gFnPc4	číslovka
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc4	jeden
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
<g/>
,	,	kIx,	,
pěti	pět	k4xCc2	pět
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
několikátý	několikátý	k4xOyIgMnSc1	několikátý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příčestí	příčestí	k1gNnPc1	příčestí
trpná	trpný	k2eAgNnPc1d1	trpné
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
kontextech	kontext	k1gInPc6	kontext
(	(	kIx(	(
<g/>
udělán	udělán	k2eAgInSc1d1	udělán
<g/>
,	,	kIx,	,
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
:	:	kIx,	:
mají	mít	k5eAaImIp3nP	mít
funkci	funkce	k1gFnSc4	funkce
přísudku	přísudek	k1gInSc2	přísudek
klauze	klauze	k1gFnSc2	klauze
řídící	řídící	k2eAgFnSc2d1	řídící
nebo	nebo	k8xC	nebo
závislé	závislý	k2eAgFnSc2d1	závislá
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
složených	složený	k2eAgInPc6d1	složený
slovesných	slovesný	k2eAgInPc6d1	slovesný
tvarech	tvar	k1gInPc6	tvar
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
bych	by	kYmCp1nS	by
býval	bývat	k5eAaImAgMnS	bývat
mohl	moct	k5eAaImAgMnS	moct
chtít	chtít	k5eAaImF	chtít
udělat	udělat	k5eAaPmF	udělat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
příslovce	příslovce	k1gNnSc1	příslovce
<g/>
:	:	kIx,	:
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
sloveso	sloveso	k1gNnSc4	sloveso
nebo	nebo	k8xC	nebo
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgNnPc2d1	tradiční
příslovcí	příslovce	k1gNnPc2	příslovce
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
číslovky	číslovka	k1gFnPc1	číslovka
řadové	řadový	k2eAgFnPc1d1	řadová
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
číslovky	číslovka	k1gFnSc2	číslovka
násobné	násobný	k2eAgFnSc2d1	násobná
(	(	kIx(	(
<g/>
jedenkrát	jedenkrát	k6eAd1	jedenkrát
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
)	)	kIx)	)
a	a	k8xC	a
přechodníky	přechodník	k1gInPc1	přechodník
(	(	kIx(	(
<g/>
dělaje	dělat	k5eAaImSgInS	dělat
<g/>
,	,	kIx,	,
udělav	udělat	k5eAaPmDgInS	udělat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
spojky	spojka	k1gFnPc1	spojka
souřadící	souřadící	k2eAgFnPc1d1	souřadící
<g/>
:	:	kIx,	:
spojují	spojovat	k5eAaImIp3nP	spojovat
dva	dva	k4xCgInPc4	dva
rovnocenné	rovnocenný	k2eAgInPc4d1	rovnocenný
větné	větný	k2eAgInPc4d1	větný
členy	člen	k1gInPc4	člen
nebo	nebo	k8xC	nebo
klauze	klauze	k1gFnPc4	klauze
<g/>
,	,	kIx,	,
např.	např.	kA	např.
a	a	k8xC	a
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
spojky	spojka	k1gFnPc1	spojka
podřadící	podřadící	k2eAgFnPc1d1	podřadící
<g/>
:	:	kIx,	:
připojují	připojovat	k5eAaImIp3nP	připojovat
závislou	závislý	k2eAgFnSc4d1	závislá
klauzi	klauze	k1gFnSc4	klauze
k	k	k7c3	k
řídící	řídící	k2eAgFnSc3d1	řídící
<g/>
,	,	kIx,	,
např.	např.	kA	např.
že	že	k8xS	že
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
vztažná	vztažný	k2eAgNnPc4d1	vztažné
zájmena	zájmeno	k1gNnPc4	zájmeno
(	(	kIx(	(
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
<g/>
,	,	kIx,	,
čí	čí	k3xOyQgInSc1	čí
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
číslovky	číslovka	k1gFnPc4	číslovka
(	(	kIx(	(
<g/>
kolik	kolik	k9	kolik
<g/>
,	,	kIx,	,
kolikátý	kolikátý	k4xOyIgInSc4	kolikátý
<g/>
,	,	kIx,	,
kolikrát	kolikrát	k6eAd1	kolikrát
<g/>
)	)	kIx)	)
a	a	k8xC	a
příslovce	příslovce	k1gNnSc4	příslovce
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
<g/>
)	)	kIx)	)
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
spojují	spojovat	k5eAaImIp3nP	spojovat
funkci	funkce	k1gFnSc4	funkce
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
nebo	nebo	k8xC	nebo
příslovce	příslovce	k1gNnSc2	příslovce
<g/>
)	)	kIx)	)
a	a	k8xC	a
podřadící	podřadící	k2eAgFnPc4d1	podřadící
spojky	spojka	k1gFnPc4	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Tázací	tázací	k2eAgNnPc1d1	tázací
zájmena	zájmeno	k1gNnPc1	zájmeno
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
číslovky	číslovka	k1gFnSc2	číslovka
nebo	nebo	k8xC	nebo
příslovce	příslovce	k1gNnSc2	příslovce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
shodují	shodovat	k5eAaImIp3nP	shodovat
se	s	k7c7	s
vztažnými	vztažný	k2eAgInPc7d1	vztažný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
hindštině	hindština	k1gFnSc6	hindština
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
nepatřila	patřit	k5eNaImAgFnS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
do	do	k7c2	do
kategorií	kategorie	k1gFnPc2	kategorie
syntaktických	syntaktický	k2eAgNnPc2d1	syntaktické
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
příslovcí	příslovce	k1gNnPc2	příslovce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
předložky	předložka	k1gFnPc1	předložka
<g/>
:	:	kIx,	:
řídí	řídit	k5eAaImIp3nS	řídit
syntaktické	syntaktický	k2eAgNnSc4d1	syntaktické
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
diktují	diktovat	k5eAaImIp3nP	diktovat
jeho	on	k3xPp3gInSc4	on
pád	pád	k1gInSc4	pád
<g/>
)	)	kIx)	)
a	a	k8xC	a
upřesňují	upřesňovat	k5eAaImIp3nP	upřesňovat
jeho	jeho	k3xOp3gFnSc4	jeho
roli	role	k1gFnSc4	role
jako	jako	k8xS	jako
rozvití	rozvití	k1gNnSc4	rozvití
slovesa	sloveso	k1gNnSc2	sloveso
(	(	kIx(	(
<g/>
zajímat	zajímat	k5eAaImF	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
něco	něco	k3yInSc4	něco
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
něco	něco	k3yInSc4	něco
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
silný	silný	k2eAgInSc4d1	silný
v	v	k7c6	v
počtech	počet	k1gInPc6	počet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
sémantické	sémantický	k2eAgInPc1d1	sémantický
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
–	–	k?	–
vymezeny	vymezen	k2eAgInPc1d1	vymezen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
:	:	kIx,	:
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
substanci	substance	k1gFnSc4	substance
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
české	český	k2eAgFnPc1d1	Česká
tradiční	tradiční	k2eAgFnSc4d1	tradiční
přivlastňovací	přivlastňovací	k2eAgNnSc4d1	přivlastňovací
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
otcův	otcův	k2eAgMnSc1d1	otcův
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
tvar	tvar	k1gInSc4	tvar
sémantického	sémantický	k2eAgNnSc2d1	sémantické
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
<g/>
:	:	kIx,	:
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
české	český	k2eAgFnSc6d1	Česká
tradiční	tradiční	k2eAgFnSc6d1	tradiční
příslovce	příslovka	k1gFnSc6	příslovka
chytře	chytro	k6eAd1	chytro
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
tvar	tvar	k1gInSc4	tvar
sémantického	sémantický	k2eAgNnSc2d1	sémantické
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
chytrý	chytrý	k2eAgInSc4d1	chytrý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
příslovce	příslovce	k1gNnSc1	příslovce
<g/>
:	:	kIx,	:
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
okolnost	okolnost	k1gFnSc4	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
české	český	k2eAgFnPc1d1	Česká
tradiční	tradiční	k2eAgFnSc4d1	tradiční
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
zítřejší	zítřejší	k2eAgFnSc2d1	zítřejší
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
tvar	tvar	k1gInSc4	tvar
sémantického	sémantický	k2eAgNnSc2d1	sémantické
příslovce	příslovce	k1gNnSc2	příslovce
zítra	zítra	k6eAd1	zítra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
číslovky	číslovka	k1gFnPc1	číslovka
<g/>
:	:	kIx,	:
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
počet	počet	k1gInSc4	počet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
:	:	kIx,	:
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
událost	událost	k1gFnSc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgNnPc2d1	tradiční
sloves	sloveso	k1gNnPc2	sloveso
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
můžeme	moct	k5eAaImIp1nP	moct
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
odvozená	odvozený	k2eAgNnPc4d1	odvozené
podstatná	podstatný	k2eAgNnPc4d1	podstatné
jména	jméno	k1gNnPc4	jméno
(	(	kIx(	(
<g/>
dělání	dělání	k1gNnPc2	dělání
<g/>
)	)	kIx)	)
a	a	k8xC	a
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
(	(	kIx(	(
<g/>
dělající	dělající	k2eAgFnSc1d1	dělající
<g/>
,	,	kIx,	,
udělavší	udělavší	k2eAgFnSc1d1	udělavší
<g/>
,	,	kIx,	,
udělaný	udělaný	k2eAgInSc1d1	udělaný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pražská	pražský	k2eAgFnSc1d1	Pražská
lingvistická	lingvistický	k2eAgFnSc1d1	lingvistická
škola	škola	k1gFnSc1	škola
===	===	k?	===
</s>
</p>
<p>
<s>
Konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
klasifikace	klasifikace	k1gFnSc1	klasifikace
opět	opět	k6eAd1	opět
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
jazyku	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
lingvistické	lingvistický	k2eAgFnSc6d1	lingvistická
teorii	teorie	k1gFnSc6	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
ve	v	k7c6	v
Funkčním	funkční	k2eAgInSc6d1	funkční
generativním	generativní	k2eAgInSc6d1	generativní
popisu	popis	k1gInSc6	popis
jazyka	jazyk	k1gInSc2	jazyk
vycházejícím	vycházející	k2eAgInSc6d1	vycházející
z	z	k7c2	z
tradic	tradice	k1gFnPc2	tradice
Pražské	pražský	k2eAgFnSc2d1	Pražská
lingvistické	lingvistický	k2eAgFnSc2d1	lingvistická
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
dělení	dělení	k1gNnSc1	dělení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
ověřeno	ověřit	k5eAaPmNgNnS	ověřit
na	na	k7c6	na
materiálu	materiál	k1gInSc6	materiál
Pražského	pražský	k2eAgInSc2d1	pražský
závislostního	závislostní	k2eAgInSc2d1	závislostní
korpusu	korpus	k1gInSc2	korpus
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
uvedené	uvedený	k2eAgFnPc1d1	uvedená
definice	definice	k1gFnPc1	definice
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
anotačních	anotační	k2eAgInPc2d1	anotační
pokynů	pokyn	k1gInPc2	pokyn
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
korpus	korpus	k1gInSc4	korpus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tradiční	tradiční	k2eAgInPc1d1	tradiční
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
<g/>
;	;	kIx,	;
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
tradičně	tradičně	k6eAd1	tradičně
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
deset	deset	k4xCc1	deset
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
bez	bez	k7c2	bez
částic	částice	k1gFnPc2	částice
devět	devět	k4xCc4	devět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zařazení	zařazení	k1gNnSc4	zařazení
slova	slovo	k1gNnSc2	slovo
k	k	k7c3	k
příslušnému	příslušný	k2eAgInSc3d1	příslušný
slovnímu	slovní	k2eAgInSc3d1	slovní
druhu	druh	k1gInSc3	druh
je	být	k5eAaImIp3nS	být
určováno	určován	k2eAgNnSc1d1	určováno
morfologickými	morfologický	k2eAgFnPc7d1	morfologická
<g/>
,	,	kIx,	,
syntaktickými	syntaktický	k2eAgFnPc7d1	syntaktická
a	a	k8xC	a
sémantickými	sémantický	k2eAgFnPc7d1	sémantická
charakteristikami	charakteristika	k1gFnPc7	charakteristika
m-lematu	memat	k1gInSc2	m-lemat
</s>
</p>
<p>
<s>
syntaktické	syntaktický	k2eAgInPc1d1	syntaktický
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
je	on	k3xPp3gMnPc4	on
pouze	pouze	k6eAd1	pouze
pomocný	pomocný	k2eAgInSc1d1	pomocný
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
postihuje	postihovat	k5eAaImIp3nS	postihovat
funkci	funkce	k1gFnSc4	funkce
slova	slovo	k1gNnSc2	slovo
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
syntaktická	syntaktický	k2eAgNnPc1d1	syntaktické
substantiva	substantivum	k1gNnPc1	substantivum
(	(	kIx(	(
<g/>
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
sloveso	sloveso	k1gNnSc4	sloveso
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
disponují	disponovat	k5eAaBmIp3nP	disponovat
morfologickými	morfologický	k2eAgFnPc7d1	morfologická
kategoriemi	kategorie	k1gFnPc7	kategorie
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
pádu	pád	k1gInSc2	pád
<g/>
;	;	kIx,	;
zpravidla	zpravidla	k6eAd1	zpravidla
slova	slovo	k1gNnSc2	slovo
s	s	k7c7	s
větněčlenskou	větněčlenský	k2eAgFnSc7d1	větněčlenský
platností	platnost	k1gFnSc7	platnost
podmětu	podmět	k1gInSc2	podmět
nebo	nebo	k8xC	nebo
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
slova	slovo	k1gNnPc1	slovo
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
doplňku	doplněk	k1gInSc2	doplněk
nebo	nebo	k8xC	nebo
jmenné	jmenný	k2eAgFnSc2d1	jmenná
části	část	k1gFnSc2	část
přísudku	přísudek	k1gInSc2	přísudek
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
morfologických	morfologický	k2eAgFnPc6d1	morfologická
kategoriích	kategorie	k1gFnPc6	kategorie
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
substantivu	substantivum	k1gNnSc6	substantivum
<g/>
,	,	kIx,	,
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
neshodného	shodný	k2eNgInSc2d1	neshodný
přívlastku	přívlastek	k1gInSc2	přívlastek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
syntaktická	syntaktický	k2eAgNnPc1d1	syntaktické
adjektiva	adjektivum	k1gNnPc1	adjektivum
(	(	kIx(	(
<g/>
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
substantivum	substantivum	k1gNnSc1	substantivum
a	a	k8xC	a
v	v	k7c6	v
morfologických	morfologický	k2eAgFnPc6d1	morfologická
kategoriích	kategorie	k1gFnPc6	kategorie
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
pádu	pád	k1gInSc2	pád
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
s	s	k7c7	s
řídícím	řídící	k2eAgNnSc7d1	řídící
substantivem	substantivum	k1gNnSc7	substantivum
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
jako	jako	k9	jako
shodný	shodný	k2eAgInSc4d1	shodný
přívlastek	přívlastek	k1gInSc4	přívlastek
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
slova	slovo	k1gNnPc4	slovo
ve	v	k7c6	v
jmenné	jmenný	k2eAgFnSc6d1	jmenná
části	část	k1gFnSc6	část
přísudku	přísudek	k1gInSc2	přísudek
nebo	nebo	k8xC	nebo
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
doplňku	doplněk	k1gInSc2	doplněk
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
morfologické	morfologický	k2eAgFnPc1d1	morfologická
kategorie	kategorie	k1gFnPc1	kategorie
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
pád	pád	k1gInSc1	pád
<g/>
)	)	kIx)	)
shodují	shodovat	k5eAaImIp3nP	shodovat
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
substantivem	substantivum	k1gNnSc7	substantivum
v	v	k7c6	v
klauzi	klauze	k1gFnSc6	klauze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
syntaktická	syntaktický	k2eAgNnPc1d1	syntaktické
adverbia	adverbium	k1gNnPc1	adverbium
(	(	kIx(	(
<g/>
slova	slovo	k1gNnPc1	slovo
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
přísudku	přísudek	k1gInSc2	přísudek
klauze	klauze	k1gFnSc2	klauze
řídící	řídící	k2eAgFnSc2d1	řídící
nebo	nebo	k8xC	nebo
závislé	závislý	k2eAgFnSc2d1	závislá
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
syntaktická	syntaktický	k2eAgNnPc1d1	syntaktické
slovesa	sloveso	k1gNnPc1	sloveso
(	(	kIx(	(
<g/>
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
událost	událost	k1gFnSc4	událost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sémantické	sémantický	k2eAgInPc4d1	sémantický
slovní	slovní	k2eAgInPc4d1	slovní
druhy	druh	k1gInPc4	druh
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgFnPc1	čtyři
kategorie	kategorie	k1gFnPc1	kategorie
uzlů	uzel	k1gInPc2	uzel
komplexního	komplexní	k2eAgInSc2d1	komplexní
typu	typ	k1gInSc2	typ
podle	podle	k7c2	podle
tektogramatické	tektogramatický	k2eAgFnSc2d1	tektogramatická
roviny	rovina	k1gFnSc2	rovina
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
t-lematu	temat	k1gInSc2	t-lemat
a	a	k8xC	a
funktoru	funktor	k1gInSc2	funktor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sémantická	sémantický	k2eAgNnPc1d1	sémantické
substantiva	substantivum	k1gNnPc1	substantivum
(	(	kIx(	(
<g/>
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
substanci	substance	k1gFnSc4	substance
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sémantická	sémantický	k2eAgNnPc1d1	sémantické
adjektiva	adjektivum	k1gNnPc1	adjektivum
(	(	kIx(	(
<g/>
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sémantická	sémantický	k2eAgNnPc1d1	sémantické
adverbia	adverbium	k1gNnPc1	adverbium
(	(	kIx(	(
<g/>
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
okolnost	okolnost	k1gFnSc4	okolnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sémantická	sémantický	k2eAgNnPc1d1	sémantické
slovesa	sloveso	k1gNnPc1	sloveso
(	(	kIx(	(
<g/>
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
událost	událost	k1gFnSc4	událost
<g/>
)	)	kIx)	)
<g/>
U	u	k7c2	u
autosémantických	autosémantický	k2eAgInPc2d1	autosémantický
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
sémantický	sémantický	k2eAgInSc1d1	sémantický
slovní	slovní	k2eAgInSc1d1	slovní
druh	druh	k1gInSc1	druh
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
tradičním	tradiční	k2eAgInSc7d1	tradiční
slovním	slovní	k2eAgInSc7d1	slovní
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozdílům	rozdíl	k1gInPc3	rozdíl
zařazení	zařazení	k1gNnSc2	zařazení
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
posesivní	posesivní	k2eAgNnSc1d1	posesivní
adjektivum	adjektivum	k1gNnSc1	adjektivum
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
sémantická	sémantický	k2eAgNnPc4d1	sémantické
substantiva	substantivum	k1gNnPc4	substantivum
</s>
</p>
<p>
<s>
deadjektivní	deadjektivní	k2eAgNnSc1d1	deadjektivní
adverbium	adverbium	k1gNnSc1	adverbium
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
sémantická	sémantický	k2eAgNnPc4d1	sémantické
adjektiva	adjektivum	k1gNnPc4	adjektivum
</s>
</p>
<p>
<s>
adverbium	adverbium	k1gNnSc1	adverbium
s	s	k7c7	s
číselným	číselný	k2eAgInSc7d1	číselný
významem	význam	k1gInSc7	význam
patří	patřit	k5eAaImIp3nS	patřit
tektogramaticky	tektogramaticky	k6eAd1	tektogramaticky
mezi	mezi	k7c4	mezi
adjektivní	adjektivní	k2eAgFnPc4d1	adjektivní
číslovky	číslovka	k1gFnSc2	číslovka
základníZájmena	základníZájmen	k2eAgFnSc1d1	základníZájmen
a	a	k8xC	a
číslovky	číslovka	k1gFnPc1	číslovka
podle	podle	k7c2	podle
syntaktické	syntaktický	k2eAgFnSc2d1	syntaktická
funkce	funkce	k1gFnSc2	funkce
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
sémantická	sémantický	k2eAgNnPc4d1	sémantické
substantiva	substantivum	k1gNnPc4	substantivum
nebo	nebo	k8xC	nebo
sémantická	sémantický	k2eAgNnPc4d1	sémantické
adjektiva	adjektivum	k1gNnPc4	adjektivum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Částice	částice	k1gFnSc1	částice
a	a	k8xC	a
citoslovce	citoslovce	k1gNnSc1	citoslovce
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
atomické	atomický	k2eAgInPc4d1	atomický
uzly	uzel	k1gInPc4	uzel
a	a	k8xC	a
k	k	k7c3	k
sémantickým	sémantický	k2eAgInPc3d1	sémantický
slovním	slovní	k2eAgInPc3d1	slovní
druhům	druh	k1gInPc3	druh
se	se	k3xPyFc4	se
nepřiřazují	přiřazovat	k5eNaImIp3nP	přiřazovat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
uzly	uzel	k1gInPc1	uzel
souřadicích	souřadicí	k2eAgFnPc2d1	souřadicí
spojek	spojka	k1gFnPc2	spojka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
typ	typ	k1gInSc4	typ
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
uzlů	uzel	k1gInPc2	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Předložky	předložka	k1gFnPc1	předložka
a	a	k8xC	a
podřadicí	podřadicí	k2eAgFnPc1d1	podřadicí
spojky	spojka	k1gFnPc1	spojka
se	se	k3xPyFc4	se
tektogramaticky	tektogramaticky	k6eAd1	tektogramaticky
vůbec	vůbec	k9	vůbec
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
uzly	uzel	k1gInPc4	uzel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
slovní	slovní	k2eAgInSc1d1	slovní
druh	druh	k1gInSc1	druh
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
