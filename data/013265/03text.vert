<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Sirotek	Sirotek	k1gMnSc1	Sirotek
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kameraman	kameraman	k1gMnSc1	kameraman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
řady	řada	k1gFnSc2	řada
významných	významný	k2eAgMnPc2d1	významný
českých	český	k2eAgMnPc2d1	český
režisérů	režisér	k1gMnPc2	režisér
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Jaromila	Jaromil	k1gMnSc2	Jaromil
Jireše	Jireše	k1gMnSc2	Jireše
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
Vorlíčka	Vorlíček	k1gMnSc2	Vorlíček
nebo	nebo	k8xC	nebo
Věry	Věra	k1gFnSc2	Věra
Plívové-Šimkové	Plívové-Šimkový	k2eAgFnSc2d1	Plívové-Šimkový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
hlavní	hlavní	k2eAgFnSc1d1	hlavní
kamera	kamera	k1gFnSc1	kamera
===	===	k?	===
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
Král	Král	k1gMnSc1	Král
sokolů	sokol	k1gMnPc2	sokol
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
Nebát	bát	k5eNaImF	bát
se	se	k3xPyFc4	se
a	a	k8xC	a
nakrást	nakrást	k5eAaBmF	nakrást
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Chlípník	chlípník	k1gMnSc1	chlípník
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Řád	řád	k1gInSc1	řád
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Zasněžená	zasněžený	k2eAgFnSc1d1	zasněžená
romance	romance	k1gFnSc1	romance
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
Jestřábí	jestřábí	k2eAgFnSc1d1	jestřábí
moudrost	moudrost	k1gFnSc1	moudrost
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
Muka	muka	k1gFnSc1	muka
obraznosti	obraznost	k1gFnSc2	obraznost
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
Dotyky	dotyk	k1gInPc5	dotyk
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
Pan	Pan	k1gMnSc1	Pan
Tau	tau	k1gNnSc2	tau
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
Když	když	k8xS	když
v	v	k7c6	v
ráji	ráj	k1gInSc6	ráj
pršelo	pršet	k5eAaImAgNnS	pršet
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
Chobotnice	chobotnice	k1gFnSc1	chobotnice
z	z	k7c2	z
II	II	kA	II
<g/>
.	.	kIx.	.
patra	patro	k1gNnPc4	patro
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
Velká	velký	k2eAgFnSc1d1	velká
filmová	filmový	k2eAgFnSc1d1	filmová
loupež	loupež	k1gFnSc1	loupež
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
Veselé	Veselé	k2eAgFnPc4d1	Veselé
vánoce	vánoce	k1gFnPc4	vánoce
přejí	přát	k5eAaImIp3nP	přát
chobotnice	chobotnice	k1gFnPc1	chobotnice
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Poločas	poločas	k1gInSc1	poločas
štěstí	štěstí	k1gNnSc2	štěstí
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Rumburak	Rumburak	k1gInSc1	Rumburak
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
Hořký	hořký	k2eAgInSc4d1	hořký
podzim	podzim	k1gInSc4	podzim
s	s	k7c7	s
vůní	vůně	k1gFnSc7	vůně
manga	mango	k1gNnSc2	mango
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
Létající	létající	k2eAgInSc1d1	létající
Čestmír	Čestmír	k1gMnSc1	Čestmír
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Jak	jak	k8xS	jak
svět	svět	k1gInSc1	svět
přichází	přicházet	k5eAaImIp3nS	přicházet
o	o	k7c4	o
básníky	básník	k1gMnPc4	básník
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Neúplné	úplný	k2eNgInPc1d1	neúplný
zatmění	zatmění	k1gNnSc2	zatmění
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
a	a	k8xC	a
lyžníci	lyžník	k1gMnPc1	lyžník
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Opera	opera	k1gFnSc1	opera
ve	v	k7c6	v
vinici	vinice	k1gFnSc6	vinice
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Zralé	zralý	k2eAgFnPc4d1	zralá
víno	víno	k1gNnSc4	víno
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
Blázni	blázen	k1gMnPc1	blázen
<g/>
,	,	kIx,	,
vodníci	vodník	k1gMnPc1	vodník
a	a	k8xC	a
podvodníci	podvodník	k1gMnPc1	podvodník
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
Mladý	mladý	k1gMnSc1	mladý
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
Hop	hop	k0	hop
–	–	k?	–
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
lidoop	lidoop	k1gMnSc1	lidoop
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
Rozmarýny	rozmarýna	k1gFnSc2	rozmarýna
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
Boty	bota	k1gFnPc4	bota
plné	plný	k2eAgFnSc2d1	plná
vody	voda	k1gFnSc2	voda
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
Páni	pan	k1gMnPc1	pan
kluci	kluk	k1gMnPc1	kluk
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
Romance	romance	k1gFnSc1	romance
za	za	k7c4	za
korunu	koruna	k1gFnSc4	koruna
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
Kvočny	kvočna	k1gFnPc1	kvočna
a	a	k8xC	a
Král	Král	k1gMnSc1	Král
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
Noc	noc	k1gFnSc1	noc
oranžových	oranžový	k2eAgInPc2d1	oranžový
ohňů	oheň	k1gInPc2	oheň
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
O	o	k7c6	o
Sněhurce	Sněhurka	k1gFnSc6	Sněhurka
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
Lišáci	lišák	k1gMnPc1	lišák
<g/>
,	,	kIx,	,
Myšáci	myšák	k1gMnPc1	myšák
a	a	k8xC	a
Šibeničák	Šibeničák	k1gInSc1	Šibeničák
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
krve	krev	k1gFnSc2	krev
</s>
</p>
<p>
<s>
===	===	k?	===
druhá	druhý	k4xOgFnSc1	druhý
kamera	kamera	k1gFnSc1	kamera
===	===	k?	===
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
Zabil	zabít	k5eAaPmAgMnS	zabít
jsem	být	k5eAaImIp1nS	být
Einsteina	Einstein	k1gMnSc2	Einstein
<g/>
,	,	kIx,	,
pánové	pán	k1gMnPc1	pán
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Objížďka	objížďka	k1gFnSc1	objížďka
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
Sedm	sedm	k4xCc1	sedm
havranů	havran	k1gMnPc2	havran
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
Kočár	kočár	k1gInSc1	kočár
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
Sedm	sedm	k4xCc1	sedm
zabitých	zabitý	k2eAgFnPc2d1	zabitá
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
Svatba	svatba	k1gFnSc1	svatba
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
Lov	lov	k1gInSc1	lov
na	na	k7c4	na
mamuta	mamut	k1gMnSc4	mamut
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
Místo	místo	k1gNnSc1	místo
v	v	k7c6	v
houfu	houf	k1gInSc6	houf
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
Deváté	devátý	k4xOgInPc1	devátý
jméno	jméno	k1gNnSc4	jméno
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
Spanilá	spanilý	k2eAgFnSc1d1	spanilá
jízda	jízda	k1gFnSc1	jízda
</s>
</p>
<p>
<s>
===	===	k?	===
asistent	asistent	k1gMnSc1	asistent
kamery	kamera	k1gFnSc2	kamera
===	===	k?	===
</s>
</p>
<p>
<s>
1962	[number]	k4	1962
Pevnost	pevnost	k1gFnSc1	pevnost
na	na	k7c6	na
Rýně	Rýn	k1gInSc6	Rýn
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
Kde	kde	k6eAd1	kde
řeky	řeka	k1gFnPc1	řeka
mají	mít	k5eAaImIp3nP	mít
slunce	slunce	k1gNnSc1	slunce
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
Trápení	trápení	k1gNnSc1	trápení
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
Lidé	člověk	k1gMnPc1	člověk
jako	jako	k8xC	jako
ty	ty	k3xPp2nSc5	ty
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
Konec	konec	k1gInSc1	konec
cesty	cesta	k1gFnSc2	cesta
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Franta	Franta	k1gMnSc1	Franta
naučil	naučit	k5eAaPmAgMnS	naučit
bát	bát	k5eAaImF	bát
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
O	o	k7c6	o
medvědu	medvěd	k1gMnSc6	medvěd
Ondřejovi	Ondřej	k1gMnSc6	Ondřej
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
filmové	filmový	k2eAgNnSc1d1	filmové
nebe	nebe	k1gNnSc1	nebe
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Sirotek	Sirotek	k1gMnSc1	Sirotek
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Sirotek	Sirotek	k1gMnSc1	Sirotek
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
