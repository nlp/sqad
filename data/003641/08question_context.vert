<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začínaly	začínat	k5eAaImAgInP	začínat
objevovat	objevovat	k5eAaImF	objevovat
první	první	k4xOgInSc1	první
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
databáze	databáze	k1gFnSc2	databáze
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
filozofie	filozofie	k1gFnSc1	filozofie
byla	být	k5eAaImAgFnS	být
přebírána	přebírat	k5eAaImNgFnS	přebírat
z	z	k7c2	z
objektově	objektově	k6eAd1	objektově
orientovaných	orientovaný	k2eAgInPc2d1	orientovaný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
databáze	databáze	k1gFnPc1	databáze
měly	mít	k5eAaImAgFnP	mít
podle	podle	k7c2	podle
předpokladů	předpoklad	k1gInPc2	předpoklad
vytlačit	vytlačit	k5eAaPmF	vytlačit
relační	relační	k2eAgInPc4d1	relační
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
