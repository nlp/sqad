<s>
KOH-I-NOOR	KOH-I-NOOR	k?	KOH-I-NOOR
HARDTMUTH	HARDTMUTH	kA	HARDTMUTH
a.s.	a.s.	k?	a.s.
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
jihočeských	jihočeský	k2eAgInPc6d1	jihočeský
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
výrobce	výrobce	k1gMnSc1	výrobce
psacích	psací	k2eAgFnPc2d1	psací
a	a	k8xC	a
výtvarných	výtvarný	k2eAgFnPc2d1	výtvarná
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
Josefem	Josef	k1gMnSc7	Josef
Hardtmuthem	Hardtmuth	k1gInSc7	Hardtmuth
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
firmy	firma	k1gFnSc2	firma
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slavného	slavný	k2eAgInSc2d1	slavný
diamantu	diamant	k1gInSc2	diamant
<g/>
.	.	kIx.	.
</s>
