<s>
Oxford	Oxford	k1gInSc1	Oxford
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
a	a	k8xC	a
nemetropolitní	metropolitní	k2eNgInSc1d1	nemetropolitní
distrikt	distrikt	k1gInSc1	distrikt
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Oxfordshire	Oxfordshir	k1gInSc5	Oxfordshir
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
149	[number]	k4	149
800	[number]	k4	800
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
používajících	používající	k2eAgMnPc2d1	používající
angličtinu	angličtina	k1gFnSc4	angličtina
-	-	kIx~	-
University	universita	k1gFnSc2	universita
of	of	k?	of
Oxford	Oxford	k1gInSc1	Oxford
<g/>
.	.	kIx.	.
</s>
