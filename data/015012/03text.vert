<s>
Solón	Solón	k1gMnSc1
</s>
<s>
Solón	Solón	k1gMnSc1
Narození	narození	k1gNnSc2
</s>
<s>
638	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
559	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
79	#num#	k4
<g/>
–	–	k?
<g/>
80	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Athény	Athéna	k1gFnPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
zákonodárce	zákonodárce	k1gMnSc1
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
a	a	k8xC
elegist	elegist	k1gMnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
Řekové	Řek	k1gMnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
původní	původní	k2eAgInPc4d1
texty	text	k1gInPc4
na	na	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
citáty	citát	k1gInPc1
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Solón	Solón	k1gMnSc1
(	(	kIx(
<g/>
starořecky	starořecky	k6eAd1
Σ	Σ	k?
<g/>
;	;	kIx,
asi	asi	k9
638	#num#	k4
–	–	k?
559	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
slavný	slavný	k2eAgMnSc1d1
antický	antický	k2eAgMnSc1d1
řecký	řecký	k2eAgMnSc1d1
básník	básník	k1gMnSc1
a	a	k8xC
zákonodárce	zákonodárce	k1gMnSc1
v	v	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
řazen	řadit	k5eAaImNgInS
k	k	k7c3
sedmi	sedm	k4xCc3
největším	veliký	k2eAgMnPc3d3
mudrcům	mudrc	k1gMnPc3
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Solón	Solón	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Athénách	Athéna	k1gFnPc6
v	v	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
mnoha	mnoho	k4c2
zchudlých	zchudlý	k2eAgFnPc2d1
aristokratických	aristokratický	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
jako	jako	k9
syn	syn	k1gMnSc1
Exikestidův	Exikestidův	k2eAgMnSc1d1
a	a	k8xC
potomek	potomek	k1gMnSc1
krále	král	k1gMnSc2
Medonta	Medont	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	své	k1gNnSc1
životní	životní	k2eAgFnSc2d1
zkušenosti	zkušenost	k1gFnSc2
načerpal	načerpat	k5eAaPmAgInS
při	při	k7c6
cestách	cesta	k1gFnPc6
na	na	k7c4
Kypr	Kypr	k1gInSc4
a	a	k8xC
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
Megařané	Megařan	k1gMnPc1
zmocnili	zmocnit	k5eAaPmAgMnP
Salamíny	Salamína	k1gFnPc4
a	a	k8xC
ohrožovali	ohrožovat	k5eAaImAgMnP
tak	tak	k6eAd1
athénský	athénský	k2eAgInSc4d1
přístav	přístav	k1gInSc4
<g/>
,	,	kIx,
přiměl	přimět	k5eAaPmAgMnS
Solón	Solón	k1gMnSc1
Athéňany	Athéňan	k1gMnPc4
k	k	k7c3
válečné	válečný	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
proti	proti	k7c3
Megaře	Megara	k1gFnSc3
a	a	k8xC
v	v	k7c6
jejich	jejich	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
populárním	populární	k2eAgInSc7d1
jak	jak	k9
pro	pro	k7c4
sílící	sílící	k2eAgInSc4d1
lid	lid	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
pro	pro	k7c4
aristokraty	aristokrat	k1gMnPc4
a	a	k8xC
roku	rok	k1gInSc2
594	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
archontem	archón	k1gMnSc7
s	s	k7c7
plnou	plný	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
pro	pro	k7c4
vypracování	vypracování	k1gNnSc4
nové	nový	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
a	a	k8xC
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
demokratických	demokratický	k2eAgFnPc2d1
reforem	reforma	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
vyvedly	vyvést	k5eAaPmAgFnP
Athény	Athéna	k1gFnPc4
z	z	k7c2
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
Solón	Solón	k1gMnSc1
vystavil	vystavit	k5eAaPmAgMnS
nové	nový	k2eAgInPc4d1
zákony	zákon	k1gInPc4
na	na	k7c6
mramorových	mramorový	k2eAgFnPc6d1
deskách	deska	k1gFnPc6
a	a	k8xC
vzdal	vzdát	k5eAaPmAgMnS
se	se	k3xPyFc4
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reforma	reforma	k1gFnSc1
však	však	k9
nebyla	být	k5eNaImAgFnS
hned	hned	k6eAd1
příznivě	příznivě	k6eAd1
přijata	přijmout	k5eAaPmNgFnS
a	a	k8xC
fungování	fungování	k1gNnSc1
nové	nový	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
zaručilo	zaručit	k5eAaPmAgNnS
po	po	k7c6
nastalých	nastalý	k2eAgInPc6d1
nepokojích	nepokoj	k1gInPc6
až	až	k8xS
panování	panování	k1gNnSc6
tyrana	tyran	k1gMnSc2
Peisistrata	Peisistrat	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Solón	Solón	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
vysokém	vysoký	k2eAgInSc6d1
věku	věk	k1gInSc6
patrně	patrně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
559	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
smrti	smrt	k1gFnSc6
mu	on	k3xPp3gMnSc3
vděční	vděčný	k2eAgMnPc1d1
občané	občan	k1gMnPc1
postavili	postavit	k5eAaPmAgMnP
pomník	pomník	k1gInSc4
a	a	k8xC
nazvali	nazvat	k5eAaBmAgMnP,k5eAaPmAgMnP
jej	on	k3xPp3gInSc4
„	„	k?
<g/>
svatým	svatý	k2eAgMnSc7d1
zákonodárcem	zákonodárce	k1gMnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdivovali	obdivovat	k5eAaImAgMnP
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
kromě	kromě	k7c2
jiných	jiný	k1gMnPc2
i	i	k9
Platón	platón	k1gInSc4
a	a	k8xC
Démosthenés	Démosthenés	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plútarchos	Plútarchos	k1gInSc1
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
životopis	životopis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Básník	básník	k1gMnSc1
a	a	k8xC
mudrc	mudrc	k1gMnSc1
</s>
<s>
Solónův	Solónův	k2eAgInSc1d1
literární	literární	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
obsahoval	obsahovat	k5eAaImAgInS
podle	podle	k7c2
starověké	starověký	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
5	#num#	k4
000	#num#	k4
veršů	verš	k1gInPc2
různého	různý	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
básnická	básnický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
,	,	kIx,
nepochybně	pochybně	k6eNd1
ovlivněná	ovlivněný	k2eAgFnSc1d1
tvorbou	tvorba	k1gFnSc7
básníka	básník	k1gMnSc2
Tyrtaia	Tyrtaius	k1gMnSc2
<g/>
,	,	kIx,
úzce	úzko	k6eAd1
souvisela	souviset	k5eAaImAgFnS
s	s	k7c7
činností	činnost	k1gFnSc7
politickou	politický	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Básněmi	báseň	k1gFnPc7
se	se	k3xPyFc4
obracel	obracet	k5eAaImAgMnS
ke	k	k7c3
spoluobčanům	spoluobčan	k1gMnPc3
a	a	k8xC
ospravedlňoval	ospravedlňovat	k5eAaImAgInS
jimi	on	k3xPp3gMnPc7
své	svůj	k3xOyFgNnSc4
jednání	jednání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyl	být	k5eNaImAgMnS
velkým	velký	k2eAgMnSc7d1
básníkem	básník	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
verších	verš	k1gInPc6
se	se	k3xPyFc4
odráží	odrážet	k5eAaImIp3nS
jasné	jasný	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
a	a	k8xC
upřímnost	upřímnost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
tvorba	tvorba	k1gFnSc1
však	však	k9
nedosáhla	dosáhnout	k5eNaPmAgFnS
květnatého	květnatý	k2eAgNnSc2d1
vyjadřování	vyjadřování	k1gNnSc2
soudobých	soudobý	k2eAgMnPc2d1
básníků	básník	k1gMnPc2
Alkaia	Alkaium	k1gNnSc2
z	z	k7c2
Lesbu	Lesbos	k1gInSc2
a	a	k8xC
Sapfó	Sapfó	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
programové	programový	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
psal	psát	k5eAaImAgInS
i	i	k9
verše	verš	k1gInPc4
s	s	k7c7
úvahami	úvaha	k1gFnPc7
o	o	k7c6
smyslu	smysl	k1gInSc6
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dochováno	dochovat	k5eAaPmNgNnS
asi	asi	k9
200	#num#	k4
veršů	verš	k1gInPc2
z	z	k7c2
elegií	elegie	k1gFnPc2
a	a	k8xC
asi	asi	k9
70	#num#	k4
veršů	verš	k1gInPc2
jambických	jambický	k2eAgFnPc2d1
a	a	k8xC
trochejských	trochejský	k2eAgFnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zajisté	zajisté	k9
dobro	dobro	k1gNnSc1
i	i	k8xC
zlo	zlo	k1gNnSc1
nám	my	k3xPp1nPc3
smrtelným	smrtelný	k2eAgFnPc3d1
přináší	přinášet	k5eAaImIp3nS
Osud	osud	k1gInSc1
<g/>
;	;	kIx,
<g/>
tomu	ten	k3xDgNnSc3
pak	pak	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
dá	dát	k5eAaPmIp3nS
bůh	bůh	k1gMnSc1
<g/>
,	,	kIx,
neujde	ujít	k5eNaPmIp3nS
smrtelný	smrtelný	k2eAgMnSc1d1
tvor	tvor	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Nebezpečné	bezpečný	k2eNgNnSc1d1
je	být	k5eAaImIp3nS
všechno	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
konáme	konat	k5eAaImIp1nP
<g/>
,	,	kIx,
nikdo	nikdo	k3yNnSc1
však	však	k9
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
vydaří	vydařit	k5eAaPmIp3nS
věc	věc	k1gFnSc4
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
právě	právě	k9
se	se	k3xPyFc4
<g />
.	.	kIx.
</s>
<s hack="1">
dal	dát	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
Někdo	někdo	k3yInSc1
chce	chtít	k5eAaImIp3nS
jednati	jednat	k5eAaImF
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
při	při	k7c6
vší	všecek	k3xTgFnSc6
snaze	snaha	k1gFnSc6
ho	on	k3xPp3gMnSc4
stihnezkáza	stihnezkáza	k1gFnSc1
a	a	k8xC
naprostý	naprostý	k2eAgInSc1d1
zmar	zmar	k1gInSc1
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
předvídal	předvídat	k5eAaImAgInS
dřív	dříve	k6eAd2
<g/>
;	;	kIx,
<g/>
jiný	jiný	k2eAgMnSc1d1
<g/>
,	,	kIx,
ač	ač	k8xS
jedná	jednat	k5eAaImIp3nS
špatně	špatně	k6eAd1
<g/>
,	,	kIx,
zas	zas	k6eAd1
vyvázne	vyváznout	k5eAaPmIp3nS
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
bludu	blud	k1gInSc2
<g/>
,	,	kIx,
<g/>
neboť	neboť	k8xC
ve	v	k7c6
věcech	věc	k1gFnPc6
všech	všecek	k3xTgMnPc2
zdaru	zdar	k1gInSc2
mu	on	k3xPp3gMnSc3
dopřává	dopřávat	k5eAaImIp3nS
bůh	bůh	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Mezí	mez	k1gFnPc2
nemá	mít	k5eNaImIp3nS
však	však	k9
píle	píle	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
lidé	člověk	k1gMnPc1
bohatství	bohatství	k1gNnSc2
kupí	kupit	k5eAaImIp3nP
<g/>
,	,	kIx,
<g/>
neboť	neboť	k8xC
nyní	nyní	k6eAd1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
z	z	k7c2
nás	my	k3xPp1nPc2
pro	pro	k7c4
život	život	k1gInSc4
nejvíce	nejvíce	k6eAd1,k6eAd3
má	mít	k5eAaImIp3nS
<g/>
,	,	kIx,
<g/>
dychtí	dychtit	k5eAaImIp3nS
mít	mít	k5eAaImF
dvakrát	dvakrát	k6eAd1
tolik	tolik	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
mohl	moct	k5eAaImAgInS
by	by	kYmCp3nS
nasytit	nasytit	k5eAaPmF
všechny	všechen	k3xTgMnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Od	od	k7c2
bohů	bůh	k1gMnPc2
nesmrtných	smrtný	k2eNgMnPc2d1
zisk	zisk	k1gInSc4
obdržel	obdržet	k5eAaPmAgMnS
smrtelný	smrtelný	k2eAgMnSc1d1
tvor	tvor	k1gMnSc1
<g/>
:	:	kIx,
<g/>
z	z	k7c2
něho	on	k3xPp3gNnSc2
však	však	k9
pochází	pocházet	k5eAaImIp3nS
Áté	Áté	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
A	a	k8xC
tu	tu	k6eAd1
když	když	k8xS
pošle	poslat	k5eAaPmIp3nS
Zeus	Zeus	k1gInSc4
trestat	trestat	k5eAaImF
<g/>
,	,	kIx,
<g/>
stíhá	stíhat	k5eAaImIp3nS
jednoho	jeden	k4xCgMnSc4
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
zítra	zítra	k6eAd1
pak	pak	k6eAd1
jiného	jiné	k1gNnSc2
zas	zas	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
—	—	k?
<g/>
Solón	Solón	k1gMnSc1
<g/>
,	,	kIx,
Elegie	elegie	k1gFnSc1
<g/>
,	,	kIx,
úryvek	úryvek	k1gInSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Solón	Solón	k1gMnSc1
byl	být	k5eAaImAgMnS
ve	v	k7c6
starověku	starověk	k1gInSc6
standardně	standardně	k6eAd1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
uváděn	uvádět	k5eAaImNgInS
jako	jako	k8xS,k8xC
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
sedmi	sedm	k4xCc2
mudrců	mudrc	k1gMnPc2
archaického	archaický	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
ho	on	k3xPp3gNnSc4
zařadil	zařadit	k5eAaPmAgMnS
již	již	k9
Platón	Platón	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
připisována	připisován	k2eAgFnSc1d1
řada	řada	k1gFnSc1
mravních	mravní	k2eAgNnPc2d1
naučení	naučení	k1gNnPc2
<g/>
,	,	kIx,
životních	životní	k2eAgFnPc2d1
rad	rada	k1gFnPc2
a	a	k8xC
moudrých	moudrý	k2eAgInPc2d1
výroků	výrok	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Ničeho	nic	k3yNnSc2
příliš	příliš	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bohatství	bohatství	k1gNnSc4
rodí	rodit	k5eAaImIp3nP
přesycenost	přesycenost	k1gFnSc1
a	a	k8xC
přesycenost	přesycenost	k1gFnSc1
zpupnost	zpupnost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyhýbej	vyhýbat	k5eAaImRp2nS
se	se	k3xPyFc4
rozkoši	rozkoš	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
plodí	plodit	k5eAaImIp3nS
zármutek	zármutek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Naučíš	naučit	k5eAaPmIp2nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
ovládat	ovládat	k5eAaImF
<g/>
,	,	kIx,
budeš	být	k5eAaImBp2nS
umět	umět	k5eAaImF
vládnout	vládnout	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nedávej	dávat	k5eNaImRp2nS
rady	rada	k1gFnSc2
nejpříjemnější	příjemný	k2eAgInSc1d3
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
nejlepší	dobrý	k2eAgMnSc1d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nezískávej	získávat	k5eNaImRp2nS
si	se	k3xPyFc3
přátele	přítel	k1gMnPc4
rychle	rychle	k6eAd1
a	a	k8xC
ty	ten	k3xDgInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsi	být	k5eAaImIp2nS
už	už	k6eAd1
získal	získat	k5eAaPmAgMnS
<g/>
,	,	kIx,
rychle	rychle	k6eAd1
nezavrhuj	zavrhovat	k5eNaImRp2nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyžaduješ	vyžadovat	k5eAaImIp2nS
<g/>
-li	-li	k?
od	od	k7c2
druhých	druhý	k4xOgInPc2
odpovědnost	odpovědnost	k1gFnSc4
<g/>
,	,	kIx,
ujmi	ujmout	k5eAaPmRp2nS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
i	i	k9
ty	ty	k3xPp2nSc5
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Neobcuj	obcovat	k5eNaImRp2nS
se	se	k3xPyFc4
špatnými	špatná	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pečuj	pečovat	k5eAaImRp2nS
o	o	k7c4
důležité	důležitý	k2eAgFnPc4d1
věci	věc	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Učiň	učinit	k5eAaPmRp2nS,k5eAaImRp2nS
si	se	k3xPyFc3
vůdcem	vůdce	k1gMnSc7
rozum	rozum	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cos	cos	k3yInSc1
nespatřil	spatřit	k5eNaPmAgMnS
<g/>
,	,	kIx,
o	o	k7c6
tom	ten	k3xDgNnSc6
nepovídej	povídat	k5eNaImRp2nS
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
jsi	být	k5eAaImIp2nS
spatřil	spatřit	k5eAaPmAgMnS
<g/>
,	,	kIx,
o	o	k7c6
tom	ten	k3xDgNnSc6
mlč	mlčet	k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zákony	zákon	k1gInPc1
jsou	být	k5eAaImIp3nP
podobny	podoben	k2eAgInPc1d1
pavučinám	pavučina	k1gFnPc3
<g/>
,	,	kIx,
neboť	neboť	k8xC
i	i	k9
ty	ty	k3xPp2nSc1
<g/>
,	,	kIx,
padne	padnout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
-li	-li	k?
do	do	k7c2
nich	on	k3xPp3gMnPc2
něco	něco	k3yInSc4
lehkého	lehký	k2eAgNnSc2d1
a	a	k8xC
slabého	slabý	k2eAgNnSc2d1
<g/>
,	,	kIx,
vydrží	vydržet	k5eAaPmIp3nS
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
však	však	k9
do	do	k7c2
nich	on	k3xPp3gMnPc2
padne	padnout	k5eAaImIp3nS,k5eAaPmIp3nS
něco	něco	k3yInSc1
většího	veliký	k2eAgNnSc2d2
<g/>
,	,	kIx,
protrhne	protrhnout	k5eAaPmIp3nS
je	on	k3xPp3gNnSc4
to	ten	k3xDgNnSc4
a	a	k8xC
unikne	uniknout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celý	celý	k2eAgInSc1d1
lidský	lidský	k2eAgInSc1d1
život	život	k1gInSc1
je	být	k5eAaImIp3nS
jen	jen	k9
snůškou	snůška	k1gFnSc7
náhod	náhoda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
člověka	člověk	k1gMnSc4
je	být	k5eAaImIp3nS
lepší	dobrý	k2eAgNnSc1d2
zemřít	zemřít	k5eAaPmF
nežli	nežli	k8xS
žít	žít	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Solónovy	Solónův	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Solónova	Solónův	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
Solónovým	Solónův	k2eAgInSc7d1
činem	čin	k1gInSc7
ve	v	k7c6
svěřeném	svěřený	k2eAgInSc6d1
úřadě	úřad	k1gInSc6
byla	být	k5eAaImAgFnS
všeobecná	všeobecný	k2eAgFnSc1d1
amnestie	amnestie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
týkala	týkat	k5eAaImAgFnS
kromě	kromě	k7c2
vrahů	vrah	k1gMnPc2
<g/>
,	,	kIx,
vlastizrádců	vlastizrádce	k1gMnPc2
a	a	k8xC
jiných	jiný	k2eAgMnPc2d1
delikventů	delikvent	k1gMnPc2
také	také	k6eAd1
vyhnanců	vyhnanec	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgFnPc4
patřili	patřit	k5eAaImAgMnP
i	i	k9
Alkméonovci	Alkméonovec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
poslal	poslat	k5eAaPmAgMnS
Solón	Solón	k1gMnSc1
vojsko	vojsko	k1gNnSc4
proti	proti	k7c3
městu	město	k1gNnSc3
Kirze	Kirze	k1gFnSc2
do	do	k7c2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
vyhlásila	vyhlásit	k5eAaPmAgFnS
delfská	delfský	k2eAgFnSc1d1
amfyktiónie	amfyktiónie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
si	se	k3xPyFc3
zjednal	zjednat	k5eAaPmAgMnS
klid	klid	k1gInSc4
na	na	k7c4
práci	práce	k1gFnSc4
při	při	k7c6
ústavních	ústavní	k2eAgFnPc6d1
reformách	reforma	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
činem	čin	k1gInSc7
bylo	být	k5eAaImAgNnS
zrušení	zrušení	k1gNnSc1
všech	všecek	k3xTgInPc2
dluhů	dluh	k1gInPc2
bez	bez	k7c2
náhrady	náhrada	k1gFnSc2
a	a	k8xC
vykoupení	vykoupení	k1gNnPc1
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
upadli	upadnout	k5eAaPmAgMnP
pro	pro	k7c4
dluhy	dluh	k1gInPc4
do	do	k7c2
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
zrušil	zrušit	k5eAaPmAgInS
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
za	za	k7c4
dluhy	dluh	k1gInPc4
ručilo	ručit	k5eAaImAgNnS
tělem	tělo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Zrušil	zrušit	k5eAaPmAgInS
i	i	k9
poplatky	poplatek	k1gInPc4
z	z	k7c2
pozemků	pozemek	k1gInPc2
a	a	k8xC
další	další	k2eAgFnSc4d1
nevolnickou	nevolnický	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
<g/>
,	,	kIx,
dle	dle	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
rolník	rolník	k1gMnSc1
stal	stát	k5eAaPmAgMnS
otrokem	otrok	k1gMnSc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
nemohl	moct	k5eNaImAgMnS
odevzdávat	odevzdávat	k5eAaImF
část	část	k1gFnSc4
ročního	roční	k2eAgInSc2d1
výnosu	výnos	k1gInSc2
obilí	obilí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Podpořil	podpořit	k5eAaPmAgInS
rozvoj	rozvoj	k1gInSc4
obchodu	obchod	k1gInSc2
a	a	k8xC
pěstování	pěstování	k1gNnSc4
plodin	plodina	k1gFnPc2
na	na	k7c4
úkor	úkor	k1gInSc4
obilnářství	obilnářství	k1gNnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zakázal	zakázat	k5eAaPmAgInS
vývoz	vývoz	k1gInSc1
obilí	obilí	k1gNnSc2
a	a	k8xC
povolil	povolit	k5eAaPmAgInS
vývoz	vývoz	k1gInSc1
olivového	olivový	k2eAgInSc2d1
oleje	olej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
krokem	krok	k1gInSc7
také	také	k6eAd1
zajistil	zajistit	k5eAaPmAgMnS
výživu	výživa	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zavedl	zavést	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
systém	systém	k1gInSc1
měr	míra	k1gFnPc2
a	a	k8xC
vah	váha	k1gFnPc2
a	a	k8xC
dal	dát	k5eAaPmAgInS
razit	razit	k5eAaImF
stříbrné	stříbrný	k2eAgFnPc4d1
mince	mince	k1gFnPc4
(	(	kIx(
<g/>
nejisté	jistý	k2eNgFnPc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řemeslníkům	řemeslník	k1gMnPc3
bez	bez	k7c2
půdy	půda	k1gFnSc2
udělil	udělit	k5eAaPmAgInS
občanská	občanský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
podnikání	podnikání	k1gNnSc2
podpořil	podpořit	k5eAaPmAgInS
zákonem	zákon	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vyvazoval	vyvazovat	k5eAaImAgInS
syny	syn	k1gMnPc4
z	z	k7c2
povinnosti	povinnost	k1gFnSc2
živit	živit	k5eAaImF
staré	starý	k2eAgMnPc4d1
rodiče	rodič	k1gMnPc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	on	k3xPp3gNnPc4
nedali	dát	k5eNaPmAgMnP
vyučit	vyučit	k5eAaPmF
řemeslu	řemeslo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Solón	Solón	k1gMnSc1
zrušil	zrušit	k5eAaPmAgInS
rodová	rodový	k2eAgNnPc4d1
privilegia	privilegium	k1gNnPc4
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Athén	Athéna	k1gFnPc2
rozdělil	rozdělit	k5eAaPmAgInS
do	do	k7c2
čtyř	čtyři	k4xCgFnPc2
tříd	třída	k1gFnPc2
podle	podle	k7c2
majetku	majetek	k1gInSc2
(	(	kIx(
<g/>
timokracie	timokracie	k1gFnSc1
<g/>
)	)	kIx)
na	na	k7c6
</s>
<s>
nejvyšší	vysoký	k2eAgFnSc4d3
třídu	třída	k1gFnSc4
–	–	k?
vlastníky	vlastník	k1gMnPc7
(	(	kIx(
<g/>
pentakosiomedimnoi	pentakosiomedimno	k1gMnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
patřili	patřit	k5eAaImAgMnP
lidé	člověk	k1gMnPc1
dosahující	dosahující	k2eAgInSc1d1
výnosu	výnos	k1gInSc2
500	#num#	k4
medimnů	medimn	k1gInPc2
(	(	kIx(
<g/>
tj.	tj.	kA
asi	asi	k9
25	#num#	k4
tisíc	tisíc	k4xCgInPc2
litrů	litr	k1gInPc2
<g/>
)	)	kIx)
plodin	plodina	k1gFnPc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnPc1
příslušníci	příslušník	k1gMnPc1
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
stát	stát	k5eAaImF,k5eAaPmF
nejvyššími	vysoký	k2eAgMnPc7d3
úředníky	úředník	k1gMnPc7
<g/>
,	,	kIx,
</s>
<s>
druhou	druhý	k4xOgFnSc4
třídu	třída	k1gFnSc4
–	–	k?
jezdce	jezdec	k1gInSc2
(	(	kIx(
<g/>
hippeis	hippeis	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výnos	výnos	k1gInSc1
300	#num#	k4
medimnů	medimn	k1gMnPc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnPc3
příslušníkům	příslušník	k1gMnPc3
byly	být	k5eAaImAgFnP
dostupné	dostupný	k2eAgInPc1d1
všechny	všechen	k3xTgInPc1
vysoké	vysoký	k2eAgInPc1d1
úřady	úřad	k1gInPc1
kromě	kromě	k7c2
úřadu	úřad	k1gInSc2
pokladníka	pokladník	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
třetí	třetí	k4xOgFnSc4
třídu	třída	k1gFnSc4
–	–	k?
pěšáky	pěšák	k1gMnPc7
<g/>
,	,	kIx,
těžkooděnce	těžkooděnec	k1gMnPc4
(	(	kIx(
<g/>
zeugítai	zeugítai	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výnos	výnos	k1gInSc1
200	#num#	k4
medimnů	medimn	k1gMnPc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
pocházeli	pocházet	k5eAaImAgMnP
nižší	nízký	k2eAgMnPc1d2
úředníci	úředník	k1gMnPc1
<g/>
,	,	kIx,
</s>
<s>
čtvrtou	čtvrtý	k4xOgFnSc4
třídu	třída	k1gFnSc4
–	–	k?
dělníky	dělník	k1gMnPc7
<g/>
,	,	kIx,
nádeníky	nádeník	k1gMnPc7
(	(	kIx(
<g/>
thétes	thétesa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bez	bez	k7c2
omezení	omezení	k1gNnSc2
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
každý	každý	k3xTgMnSc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
mohl	moct	k5eAaImAgInS
stát	stát	k5eAaPmF,k5eAaImF
členem	člen	k1gMnSc7
búlé	búlá	k1gFnSc2
<g/>
,	,	kIx,
sněmu	sněm	k1gInSc2
či	či	k8xC
„	„	k?
<g/>
Rady	rada	k1gFnSc2
pěti	pět	k4xCc3
set	set	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
kontrolovala	kontrolovat	k5eAaImAgFnS
činnost	činnost	k1gFnSc4
úřadů	úřad	k1gInPc2
a	a	k8xC
schvalovala	schvalovat	k5eAaImAgFnS
zákony	zákon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
krok	krok	k1gInSc1
však	však	k9
vyvolal	vyvolat	k5eAaPmAgInS
politický	politický	k2eAgInSc1d1
třídní	třídní	k2eAgInSc1d1
boj	boj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
vydal	vydat	k5eAaPmAgMnS
Solón	Solón	k1gMnSc1
zákon	zákon	k1gInSc4
<g/>
,	,	kIx,
dle	dle	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
se	se	k3xPyFc4
kterýkoli	kterýkoli	k3yIgMnSc1
občan	občan	k1gMnSc1
mohl	moct	k5eAaImAgMnS
odvolat	odvolat	k5eAaPmF
proti	proti	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
jakéhokoli	jakýkoli	k3yIgInSc2
úřadu	úřad	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
k	k	k7c3
soudu	soud	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takový	takový	k3xDgInSc1
zákon	zákon	k1gInSc1
neměl	mít	k5eNaImAgInS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
precedens	precedens	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřídil	zřídit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
nejvyšší	vysoký	k2eAgInSc1d3
soud	soud	k1gInSc1
(	(	kIx(
<g/>
Hélaia	Hélaia	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
porotců	porotce	k1gMnPc2
volených	volený	k2eAgMnPc2d1
losem	los	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
stanovil	stanovit	k5eAaPmAgInS
složení	složení	k1gNnSc4
lidového	lidový	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
podle	podle	k7c2
starých	starý	k2eAgFnPc2d1
tradic	tradice	k1gFnPc2
<g/>
;	;	kIx,
mohli	moct	k5eAaImAgMnP
se	se	k3xPyFc4
ho	on	k3xPp3gNnSc4
zúčastnit	zúčastnit	k5eAaPmF
všichni	všechen	k3xTgMnPc1
občané	občan	k1gMnPc1
Athén	Athéna	k1gFnPc2
a	a	k8xC
sněm	sněm	k1gInSc1
měl	mít	k5eAaImAgInS
právo	právo	k1gNnSc4
rozhodovat	rozhodovat	k5eAaImF
o	o	k7c6
všech	všecek	k3xTgFnPc6
věcech	věc	k1gFnPc6
a	a	k8xC
volit	volit	k5eAaImF
všechny	všechen	k3xTgMnPc4
úředníky	úředník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Starověké	starověký	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
o	o	k7c6
datu	datum	k1gNnSc6
jeho	jeho	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
nejsou	být	k5eNaImIp3nP
jednotné	jednotný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Dle	dle	k7c2
profesora	profesor	k1gMnSc2
Olivy	Oliva	k1gMnSc2
zemřel	zemřít	k5eAaPmAgMnS
pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
560	#num#	k4
<g/>
/	/	kIx~
<g/>
559	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Údaj	údaj	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
559	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
přebíráme	přebírat	k5eAaImIp1nP
z	z	k7c2
Příručního	příruční	k2eAgInSc2d1
slovníku	slovník	k1gInSc2
naučného	naučný	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Příruční	příruční	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
nakladatelství	nakladatelství	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
933	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
279018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
178	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plútarchos	Plútarchosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životopisy	životopis	k1gInPc4
slavných	slavný	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
a	a	k8xC
Římanů	Říman	k1gMnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
svazek	svazek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Antonín	Antonín	k1gMnSc1
Hartmann	Hartmann	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
834	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1657086	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
159	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedm	sedm	k4xCc1
mudrců	mudrc	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc4
výroky	výrok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Herrmann	Herrmanna	k1gFnPc2
&	&	k?
synové	syn	k1gMnPc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
411	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87054	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
182	#num#	k4
(	(	kIx(
<g/>
poznámka	poznámka	k1gFnSc1
č.	č.	k?
5	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OLIVA	Oliva	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Solón	Solón	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
.	.	kIx.
245	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
133245	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
42	#num#	k4
<g/>
–	–	k?
<g/>
43	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BORECKÝ	borecký	k2eAgInSc1d1
<g/>
,	,	kIx,
Bořivoj	Bořivoj	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc4
spisovatelů	spisovatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecko	Řecko	k1gNnSc1
<g/>
:	:	kIx,
antická	antický	k2eAgFnSc1d1
<g/>
,	,	kIx,
byzantská	byzantský	k2eAgFnSc1d1
a	a	k8xC
novořecká	novořecký	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
668	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2111317	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
558	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BAHNÍK	bahník	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc1
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
.	.	kIx.
717	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
160204	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
576	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Úryvek	úryvek	k1gInSc4
v	v	k7c6
překladu	překlad	k1gInSc6
Ferdinanda	Ferdinand	k1gMnSc2
Stiebitze	Stiebitze	k1gFnSc2
uvádíme	uvádět	k5eAaImIp1nP
dle	dle	k7c2
→	→	k?
OLIVA	Oliva	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Solón	Solón	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
.	.	kIx.
245	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
133245	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
159	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedm	sedm	k4xCc1
mudrců	mudrc	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc4
výroky	výrok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Herrmann	Herrmanna	k1gFnPc2
&	&	k?
synové	syn	k1gMnPc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
411	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87054	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
40	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Platón	Platón	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prótagoras	Prótagorasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
František	František	k1gMnSc1
Novotný	Novotný	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
opr	opr	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Oikoymenh	Oikoymenh	k1gMnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
85	#num#	k4
s.	s.	k?
Oikúmené	Oikúmený	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85241	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
55.1	55.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedm	sedm	k4xCc1
mudrců	mudrc	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc4
výroky	výrok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Herrmann	Herrmanna	k1gFnPc2
&	&	k?
synové	syn	k1gMnPc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
411	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87054	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
321.1	321.1	k4
2	#num#	k4
SVOBODA	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlomky	zlomek	k1gInPc4
předsokratovských	předsokratovský	k2eAgMnPc2d1
myslitelů	myslitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
.	.	kIx.
175	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
764151	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
5.1	5.1	k4
2	#num#	k4
3	#num#	k4
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedm	sedm	k4xCc1
mudrců	mudrc	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc4
výroky	výrok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Herrmann	Herrmanna	k1gFnPc2
&	&	k?
synové	syn	k1gMnPc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
411	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87054	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
315.1	315.1	k4
2	#num#	k4
Diogenés	Diogenésa	k1gFnPc2
Laertios	Laertiosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životy	život	k1gInPc1
<g/>
,	,	kIx,
názory	názor	k1gInPc1
a	a	k8xC
výroky	výrok	k1gInPc1
proslulých	proslulý	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Antonín	Antonín	k1gMnSc1
Kolář	Kolář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelhřimov	Pelhřimov	k1gInSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
473	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901916	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
53	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
OLIVA	Oliva	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Solón	Solón	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
.	.	kIx.
245	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
133245	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
216	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hérodotos	Hérodotos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jaroslav	Jaroslav	k1gMnSc1
Šonka	Šonka	k1gMnSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
v	v	k7c4
nakl	nakl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academia	academia	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
548	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1192	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
33	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hérodotos	Hérodotos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jaroslav	Jaroslav	k1gMnSc1
Šonka	Šonka	k1gMnSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
v	v	k7c4
nakl	nakl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academia	academia	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
548	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1192	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pseudo-Aristotelés	Pseudo-Aristotelés	k1gInSc1
<g/>
,	,	kIx,
Athénská	athénský	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
VI	VI	kA
<g/>
.	.	kIx.
<g/>
-VII	-VII	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BORECKÝ	borecký	k2eAgMnSc1d1
<g/>
,	,	kIx,
Bořivoj	Bořivoj	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc4
spisovatelů	spisovatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecko	Řecko	k1gNnSc1
<g/>
:	:	kIx,
antická	antický	k2eAgFnSc1d1
<g/>
,	,	kIx,
byzantská	byzantský	k2eAgFnSc1d1
a	a	k8xC
novořecká	novořecký	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
668	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2111317	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
558	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BUTTIN	BUTTIN	kA
<g/>
,	,	kIx,
Anne-Marie	Anne-Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecko	Řecko	k1gNnSc1
<g/>
:	:	kIx,
776	#num#	k4
až	až	k9
338	#num#	k4
př.n.l.	př.n.l.	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NLN	NLN	kA
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
261	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
566	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Diogenés	Diogenés	k6eAd1
Laertios	Laertios	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životy	život	k1gInPc1
<g/>
,	,	kIx,
názory	názor	k1gInPc1
a	a	k8xC
výroky	výrok	k1gInPc1
proslulých	proslulý	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Antonín	Antonín	k1gMnSc1
Kolář	Kolář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelhřimov	Pelhřimov	k1gInSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
473	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901916	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
47	#num#	k4
<g/>
–	–	k?
<g/>
56	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HOŠEK	Hošek	k1gMnSc1
<g/>
,	,	kIx,
Radislav	Radislav	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnSc1d3
řecká	řecký	k2eAgFnSc1d1
lyrika	lyrika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Ferdinand	Ferdinand	k1gMnSc1
Stiebitz	Stiebitza	k1gFnPc2
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
v	v	k7c6
tomto	tento	k3xDgInSc6
souboru	soubor	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
447	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
161529	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LEVI	LEVI	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
starého	starý	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
:	:	kIx,
kulturní	kulturní	k2eAgInSc1d1
atlas	atlas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Magdaléna	Magdaléna	k1gFnSc1
Tůmová	Tůmová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
239	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7176	#num#	k4
<g/>
-	-	kIx~
<g/>
214	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
OLIVA	Oliva	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Solón	Solón	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
.	.	kIx.
245	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
133245	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
157	#num#	k4
<g/>
–	–	k?
<g/>
168	#num#	k4
je	být	k5eAaImIp3nS
český	český	k2eAgInSc4d1
překlad	překlad	k1gInSc4
zachovaných	zachovaný	k2eAgInPc2d1
zlomků	zlomek	k1gInPc2
Solónových	Solónův	k2eAgFnPc2d1
básní	báseň	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
169	#num#	k4
<g/>
–	–	k?
<g/>
184	#num#	k4
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
zlomků	zlomek	k1gInPc2
jeho	jeho	k3xOp3gInPc2
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
:	:	kIx,
illustrovaná	illustrovaný	k2eAgFnSc1d1
encyklopaedie	encyklopaedie	k1gFnSc1
obecných	obecný	k2eAgFnPc2d1
vědomostí	vědomost	k1gFnPc2
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1905	#num#	k4
<g/>
.	.	kIx.
1064	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
277218	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
652	#num#	k4
<g/>
–	–	k?
<g/>
654	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
online	onlin	k1gInSc5
</s>
<s>
Plútarchos	Plútarchos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životopisy	životopis	k1gInPc4
slavných	slavný	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
a	a	k8xC
Římanů	Říman	k1gMnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
svazek	svazek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Antonín	Antonín	k1gMnSc1
Hartmann	Hartmann	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
834	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1657086	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Solónův	Solónův	k2eAgInSc1d1
životopis	životopis	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
131	#num#	k4
<g/>
–	–	k?
<g/>
159	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
STIEBITZ	STIEBITZ	kA
<g/>
,	,	kIx,
Ferdinand	Ferdinand	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecká	řecký	k2eAgFnSc1d1
lyrika	lyrika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
krásné	krásný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
hudby	hudba	k1gFnSc2
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
.	.	kIx.
356	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
430970	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Překlad	překlad	k1gInSc1
Solónových	Solónův	k2eAgFnPc2d1
básní	báseň	k1gFnPc2
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
71	#num#	k4
<g/>
–	–	k?
<g/>
79	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedm	sedm	k4xCc1
mudrců	mudrc	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc4
výroky	výrok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Herrmann	Herrmanna	k1gFnPc2
&	&	k?
synové	syn	k1gMnPc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
411	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87054	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecký	řecký	k2eAgInSc1d1
zázrak	zázrak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Garamond	garamond	k1gInSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7407	#num#	k4
<g/>
-	-	kIx~
<g/>
336	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Solónova	Solónův	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
</s>
<s>
Starověké	starověký	k2eAgFnPc1d1
Athény	Athéna	k1gFnPc1
</s>
<s>
Drakón	Drakón	k1gMnSc1
</s>
<s>
Kleisthenés	Kleisthenés	k6eAd1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Solón	Solón	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Solón	Solón	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Autor	autor	k1gMnSc1
Solón	Solón	k1gMnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Solón	Solón	k1gMnSc1
</s>
<s>
O	o	k7c6
Solónovi	Solón	k1gMnSc6
na	na	k7c6
webu	web	k1gInSc6
„	„	k?
<g/>
fysis	fysis	k1gFnSc2
<g/>
"	"	kIx"
</s>
<s>
Dílo	dílo	k1gNnSc1
Lives_of_the_Eminent_Philosophers	Lives_of_the_Eminent_Philosophersa	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Book_I	Book_I	k1gFnSc1
<g/>
#	#	kIx~
<g/>
Solon	Solon	k1gNnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000701672	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118615394	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1161	#num#	k4
5631	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79110932	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500354818	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
14908273	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79110932	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Antika	antika	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
