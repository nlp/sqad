<s>
Zejména	zejména	k9	zejména
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
Euckenovi	Euckenův	k2eAgMnPc1d1	Euckenův
udělena	udělit	k5eAaPmNgFnS	udělit
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
za	za	k7c4	za
závažné	závažný	k2eAgNnSc4d1	závažné
hledání	hledání	k1gNnSc4	hledání
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
pronikavou	pronikavý	k2eAgFnSc4d1	pronikavá
sílu	síla	k1gFnSc4	síla
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
rozhled	rozhled	k1gInSc4	rozhled
<g/>
,	,	kIx,	,
za	za	k7c4	za
vřelost	vřelost	k1gFnSc4	vřelost
a	a	k8xC	a
mohutnost	mohutnost	k1gFnSc4	mohutnost
stylistického	stylistický	k2eAgNnSc2d1	stylistické
ztvárnění	ztvárnění	k1gNnSc2	ztvárnění
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
v	v	k7c6	v
četných	četný	k2eAgInPc6d1	četný
pracích	prak	k1gInPc6	prak
hájil	hájit	k5eAaImAgMnS	hájit
a	a	k8xC	a
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
idealistickou	idealistický	k2eAgFnSc4d1	idealistická
životní	životní	k2eAgFnSc4d1	životní
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
ze	z	k7c2	z
zdůvodnění	zdůvodnění	k1gNnSc2	zdůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
