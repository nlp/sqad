<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Christoph	Christoph	k1gMnSc1	Christoph
Eucken	Eucken	k2eAgMnSc1d1	Eucken
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
Aurich	Aurich	k1gInSc1	Aurich
<g/>
,	,	kIx,	,
Východní	východní	k2eAgNnSc1d1	východní
Frísko	Frísko	k1gNnSc1	Frísko
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Jena	Jena	k1gFnSc1	Jena
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
obnovu	obnova	k1gFnSc4	obnova
idealistické	idealistický	k2eAgFnSc2d1	idealistická
metafyziky	metafyzika	k1gFnSc2	metafyzika
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
filozofické	filozofický	k2eAgInPc1d1	filozofický
názory	názor	k1gInPc1	názor
==	==	k?	==
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Christoph	Christoph	k1gMnSc1	Christoph
Eucken	Eucken	k1gInSc4	Eucken
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
Göttingenu	Göttingen	k1gInSc6	Göttingen
klasickou	klasický	k2eAgFnSc4d1	klasická
filologii	filologie	k1gFnSc4	filologie
<g/>
,	,	kIx,	,
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
a	a	k8xC	a
hluboce	hluboko	k6eAd1	hluboko
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
Aristotelovými	Aristotelův	k2eAgInPc7d1	Aristotelův
postupy	postup	k1gInPc7	postup
a	a	k8xC	a
výrazovými	výrazový	k2eAgInPc7d1	výrazový
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1871	[number]	k4	1871
až	až	k9	až
1874	[number]	k4	1874
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
filozofie	filozofie	k1gFnSc2	filozofie
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
v	v	k7c6	v
Jeně	Jena	k1gFnSc6	Jena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
jako	jako	k8xS	jako
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
činitel	činitel	k1gMnSc1	činitel
hnutí	hnutí	k1gNnSc2	hnutí
kritizující	kritizující	k2eAgInSc4d1	kritizující
převládající	převládající	k2eAgInSc4d1	převládající
naturalisticko-pozitivistický	naturalistickoozitivistický	k2eAgInSc4d1	naturalisticko-pozitivistický
světový	světový	k2eAgInSc4d1	světový
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tradic	tradice	k1gFnPc2	tradice
německého	německý	k2eAgInSc2d1	německý
klasického	klasický	k2eAgInSc2d1	klasický
idealismu	idealismus	k1gInSc2	idealismus
vytvořit	vytvořit	k5eAaPmF	vytvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
koncepci	koncepce	k1gFnSc4	koncepce
metafyziky	metafyzika	k1gFnSc2	metafyzika
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgInSc1d1	spojující
filozofický	filozofický	k2eAgInSc4d1	filozofický
aktivismus	aktivismus	k1gInSc4	aktivismus
s	s	k7c7	s
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
náboženstvím	náboženství	k1gNnSc7	náboženství
a	a	k8xC	a
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
prvky	prvek	k1gInPc7	prvek
pozdější	pozdní	k2eAgFnSc2d2	pozdější
filozofie	filozofie	k1gFnSc2	filozofie
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Východiskem	východisko	k1gNnSc7	východisko
jeho	jeho	k3xOp3gFnPc2	jeho
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
idealistického	idealistický	k2eAgNnSc2d1	idealistické
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
tvořivých	tvořivý	k2eAgFnPc2d1	tvořivá
sil	síla	k1gFnPc2	síla
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pojetí	pojetí	k1gNnSc1	pojetí
duchovního	duchovní	k2eAgInSc2d1	duchovní
světa	svět	k1gInSc2	svět
jako	jako	k8xC	jako
autonomního	autonomní	k2eAgInSc2d1	autonomní
<g/>
,	,	kIx,	,
věčného	věčný	k2eAgInSc2d1	věčný
<g/>
,	,	kIx,	,
majícího	mající	k2eAgMnSc2d1	mající
absolutní	absolutní	k2eAgInSc4d1	absolutní
smysl	smysl	k1gInSc4	smysl
a	a	k8xC	a
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zastánce	zastánce	k1gMnSc1	zastánce
neoidealismu	neoidealismus	k1gInSc2	neoidealismus
požadoval	požadovat	k5eAaImAgMnS	požadovat
a	a	k8xC	a
zdůvodňoval	zdůvodňovat	k5eAaImAgMnS	zdůvodňovat
sjednocení	sjednocení	k1gNnSc4	sjednocení
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
života	život	k1gInSc2	život
do	do	k7c2	do
mravně-duchovního	mravněuchovní	k2eAgInSc2d1	mravně-duchovní
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
překonal	překonat	k5eAaPmAgInS	překonat
úpadek	úpadek	k1gInSc4	úpadek
moderní	moderní	k2eAgFnSc2d1	moderní
civilizace	civilizace	k1gFnSc2	civilizace
spočívající	spočívající	k2eAgFnSc1d1	spočívající
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kulturní	kulturní	k2eAgFnSc1d1	kulturní
práce	práce	k1gFnSc1	práce
stala	stát	k5eAaPmAgFnS	stát
neosobní	osobní	k2eNgFnSc1d1	neosobní
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
opětovně	opětovně	k6eAd1	opětovně
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
opravdový	opravdový	k2eAgInSc1d1	opravdový
duchovní	duchovní	k2eAgInSc1d1	duchovní
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
postup	postup	k1gInSc4	postup
Eucken	Eucken	k1gInSc1	Eucken
označoval	označovat	k5eAaImAgInS	označovat
jako	jako	k9	jako
noologickou	noologický	k2eAgFnSc4d1	noologický
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
duchovní	duchovní	k2eAgInSc1d1	duchovní
svět	svět	k1gInSc1	svět
nechápal	chápat	k5eNaImAgInS	chápat
jen	jen	k9	jen
psychologicky	psychologicky	k6eAd1	psychologicky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k9	jako
objektivní	objektivní	k2eAgFnSc4d1	objektivní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Smysl	smysl	k1gInSc4	smysl
bytí	bytí	k1gNnSc2	bytí
pak	pak	k6eAd1	pak
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
v	v	k7c6	v
aktivním	aktivní	k2eAgNnSc6d1	aktivní
naplňování	naplňování	k1gNnSc6	naplňování
duchovních	duchovní	k2eAgFnPc2d1	duchovní
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Euckenovy	Euckenův	k2eAgInPc1d1	Euckenův
spisy	spis	k1gInPc1	spis
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
obrovským	obrovský	k2eAgInSc7d1	obrovský
kazatelským	kazatelský	k2eAgInSc7d1	kazatelský
patosem	patos	k1gInSc7	patos
působícím	působící	k2eAgInSc7d1	působící
zejména	zejména	k6eAd1	zejména
proti	proti	k7c3	proti
odlištění	odlištění	k1gNnSc3	odlištění
lidské	lidský	k2eAgFnSc2d1	lidská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
Euckenovi	Euckenův	k2eAgMnPc1d1	Euckenův
udělena	udělit	k5eAaPmNgFnS	udělit
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
za	za	k7c4	za
závažné	závažný	k2eAgNnSc4d1	závažné
hledání	hledání	k1gNnSc4	hledání
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
pronikavou	pronikavý	k2eAgFnSc4d1	pronikavá
sílu	síla	k1gFnSc4	síla
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
rozhled	rozhled	k1gInSc4	rozhled
<g/>
,	,	kIx,	,
za	za	k7c4	za
vřelost	vřelost	k1gFnSc4	vřelost
a	a	k8xC	a
mohutnost	mohutnost	k1gFnSc4	mohutnost
stylistického	stylistický	k2eAgNnSc2d1	stylistické
ztvárnění	ztvárnění	k1gNnSc2	ztvárnění
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
v	v	k7c6	v
četných	četný	k2eAgInPc6d1	četný
pracích	prak	k1gInPc6	prak
hájil	hájit	k5eAaImAgMnS	hájit
a	a	k8xC	a
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
idealistickou	idealistický	k2eAgFnSc4d1	idealistická
životní	životní	k2eAgFnSc4d1	životní
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
ze	z	k7c2	z
zdůvodnění	zdůvodnění	k1gNnSc2	zdůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
krásnou	krásný	k2eAgFnSc4d1	krásná
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
jiné	jiný	k2eAgInPc4d1	jiný
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
svou	svůj	k3xOyFgFnSc7	svůj
formou	forma	k1gFnSc7	forma
a	a	k8xC	a
pojetím	pojetí	k1gNnSc7	pojetí
mají	mít	k5eAaImIp3nP	mít
literární	literární	k2eAgFnSc4d1	literární
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
příslušných	příslušný	k2eAgFnPc6d1	příslušná
stanovách	stanova	k1gFnPc6	stanova
k	k	k7c3	k
Nobelově	Nobelův	k2eAgFnSc3d1	Nobelova
ceně	cena	k1gFnSc3	cena
(	(	kIx(	(
<g/>
prvním	první	k4xOgMnSc6	první
takovýmto	takovýto	k3xDgMnSc7	takovýto
nositelem	nositel	k1gMnSc7	nositel
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
německý	německý	k2eAgMnSc1d1	německý
historik	historik	k1gMnSc1	historik
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsen	k2eAgMnSc1d1	Mommsen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spisy	spis	k1gInPc4	spis
==	==	k?	==
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Methode	Method	k1gMnSc5	Method
der	drát	k5eAaImRp2nS	drát
aristotelischen	aristotelischen	k1gInSc4	aristotelischen
Forschung	Forschung	k1gInSc1	Forschung
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
Metoda	metoda	k1gFnSc1	metoda
Aristotelových	Aristotelův	k2eAgNnPc2d1	Aristotelovo
bádání	bádání	k1gNnPc2	bádání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Grundbegriffe	Grundbegriff	k1gMnSc5	Grundbegriff
der	drát	k5eAaImRp2nS	drát
Gegenwart	Gegenwart	k1gInSc1	Gegenwart
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
Základní	základní	k2eAgInPc1d1	základní
pojmy	pojem	k1gInPc1	pojem
moderního	moderní	k2eAgNnSc2d1	moderní
filozofického	filozofický	k2eAgNnSc2d1	filozofické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Geschichte	Geschicht	k1gMnSc5	Geschicht
der	drát	k5eAaImRp2nS	drát
philosophischen	philosophischen	k2eAgMnSc1d1	philosophischen
Terminologie	terminologie	k1gFnPc4	terminologie
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
filozofického	filozofický	k2eAgNnSc2d1	filozofické
názvosloví	názvosloví	k1gNnSc2	názvosloví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Einheit	Einheit	k1gInSc1	Einheit
des	des	k1gNnSc1	des
Geistesleben	Geistesleben	k2eAgMnSc1d1	Geistesleben
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Jednota	jednota	k1gFnSc1	jednota
duchovního	duchovní	k2eAgInSc2d1	duchovní
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Lebensanschauungen	Lebensanschauungen	k1gInSc1	Lebensanschauungen
der	drát	k5eAaImRp2nS	drát
grosser	grosser	k1gMnSc1	grosser
Denker	Denker	k1gMnSc1	Denker
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Velcí	velký	k2eAgMnPc1d1	velký
myslitelé	myslitel	k1gMnPc1	myslitel
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc7	jejich
pohledy	pohled	k1gInPc7	pohled
na	na	k7c4	na
život	život	k1gInSc4	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Kampf	Kampf	k1gMnSc1	Kampf
um	um	k1gInSc4	um
einen	einen	k2eAgInSc4d1	einen
geistigen	geistigen	k1gInSc4	geistigen
Lebensinhalt	Lebensinhalt	k1gInSc1	Lebensinhalt
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
Zápas	zápas	k1gInSc1	zápas
za	za	k7c4	za
duchovní	duchovní	k2eAgInSc4d1	duchovní
obsah	obsah	k1gInSc4	obsah
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Wahrheitsgehalt	Wahrheitsgehalt	k1gInSc1	Wahrheitsgehalt
der	drát	k5eAaImRp2nS	drát
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Pravda	pravda	k1gFnSc1	pravda
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Grundlinien	Grundlinien	k2eAgMnSc1d1	Grundlinien
einer	einer	k1gMnSc1	einer
neuen	neuna	k1gFnPc2	neuna
Lebensanschauung	Lebensanschauung	k1gMnSc1	Lebensanschauung
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Podstata	podstata	k1gFnSc1	podstata
života	život	k1gInSc2	život
a	a	k8xC	a
životní	životní	k2eAgInPc4d1	životní
názory	názor	k1gInPc4	názor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Philosophie	Philosophie	k1gFnSc1	Philosophie
der	drát	k5eAaImRp2nS	drát
Geschichte	Geschicht	k1gInSc5	Geschicht
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Filozofie	filozofie	k1gFnSc1	filozofie
dějin	dějiny	k1gFnPc2	dějiny
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Geistige	Geistige	k6eAd1	Geistige
Stromungen	Stromungen	k1gInSc1	Stromungen
der	drát	k5eAaImRp2nS	drát
Gegenwart	Gegenwart	k1gInSc1	Gegenwart
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Hlavní	hlavní	k2eAgInPc1d1	hlavní
proudy	proud	k1gInPc1	proud
moderního	moderní	k2eAgNnSc2d1	moderní
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Sinn	Sinn	k1gInSc1	Sinn
und	und	k?	und
Wert	Wert	k1gInSc1	Wert
des	des	k1gNnSc1	des
Lebens	Lebens	k1gInSc1	Lebens
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Smysl	smysl	k1gInSc1	smysl
a	a	k8xC	a
hodnota	hodnota	k1gFnSc1	hodnota
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Können	Können	k2eAgInSc4d1	Können
wir	wir	k?	wir
noch	noch	k1gInSc4	noch
Christen	Christen	k2eAgInSc4d1	Christen
sein	sein	k1gInSc4	sein
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
Můžeme	moct	k5eAaImIp1nP	moct
ještě	ještě	k6eAd1	ještě
být	být	k5eAaImF	být
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Sozialismus	Sozialismus	k1gInSc1	Sozialismus
und	und	k?	und
seine	seinout	k5eAaPmIp3nS	seinout
Lebensgestaltung	Lebensgestaltung	k1gInSc1	Lebensgestaltung
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Socialismus	socialismus	k1gInSc1	socialismus
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
utváření	utváření	k1gNnSc1	utváření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
a	a	k8xC	a
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
Kalich	kalich	k1gInSc1	kalich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
</s>
</p>
<p>
<s>
Smysl	smysl	k1gInSc1	smysl
a	a	k8xC	a
hodnota	hodnota	k1gFnSc1	hodnota
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
a	a	k8xC	a
úvodem	úvod	k1gInSc7	úvod
opatřil	opatřit	k5eAaPmAgMnS	opatřit
Rudolf	Rudolf	k1gMnSc1	Rudolf
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rudolf	Rudolf	k1gMnSc1	Rudolf
Christoph	Christoph	k1gMnSc1	Christoph
Eucken	Eucken	k2eAgMnSc1d1	Eucken
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Rudolf	Rudolf	k1gMnSc1	Rudolf
Christoph	Christoph	k1gMnSc1	Christoph
Eucken	Eucken	k2eAgMnSc1d1	Eucken
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Rudolf	Rudolf	k1gMnSc1	Rudolf
Christoph	Christoph	k1gMnSc1	Christoph
Eucken	Eucken	k1gInSc4	Eucken
</s>
</p>
<p>
<s>
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
</s>
</p>
<p>
<s>
http://nobelprize.org/nobel_prizes/literature/laureates/1908/eucken-bio.html	[url]	k1gMnSc1	http://nobelprize.org/nobel_prizes/literature/laureates/1908/eucken-bio.html
</s>
</p>
<p>
<s>
http://www.dhm.de/lemo/html/biografien/EuckenRudolf/	[url]	k?	http://www.dhm.de/lemo/html/biografien/EuckenRudolf/
–	–	k?	–
německy	německy	k6eAd1	německy
</s>
</p>
<p>
<s>
https://web.archive.org/web/20130603233813/http://www.kirjasto.sci.fi/eucken.htm	[url]	k1gInSc1	https://web.archive.org/web/20130603233813/http://www.kirjasto.sci.fi/eucken.htm
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
