<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Mrázek	Mrázek	k1gMnSc1	Mrázek
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1903	[number]	k4	1903
–	–	k?	–
???	???	k?	???
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
poúnorový	poúnorový	k2eAgMnSc1d1	poúnorový
poslanec	poslanec	k1gMnSc1	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
dělník	dělník	k1gMnSc1	dělník
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
Místního	místní	k2eAgInSc2d1	místní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
KSČ	KSČ	kA	KSČ
ve	v	k7c6	v
volebním	volební	k2eAgInSc6d1	volební
kraji	kraj	k1gInSc6	kraj
Havlíčkův	Havlíčkův	k2eAgInSc4d1	Havlíčkův
Brod	Brod	k1gInSc4	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
nabyl	nabýt	k5eAaPmAgInS	nabýt
až	až	k9	až
dodatečně	dodatečně	k6eAd1	dodatečně
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1953	[number]	k4	1953
jako	jako	k8xS	jako
náhradník	náhradník	k1gMnSc1	náhradník
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
poslanec	poslanec	k1gMnSc1	poslanec
Václav	Václav	k1gMnSc1	Václav
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
zasedal	zasedat	k5eAaImAgInS	zasedat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Mrázek	Mrázek	k1gMnSc1	Mrázek
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
</s>
</p>
