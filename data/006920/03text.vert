<s>
Dominik	Dominik	k1gMnSc1	Dominik
Zbrožek	Zbrožka	k1gFnPc2	Zbrožka
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Dominik	Dominik	k1gMnSc1	Dominik
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1832	[number]	k4	1832
Sambir	Sambira	k1gFnPc2	Sambira
<g/>
,	,	kIx,	,
Rakouské	rakouský	k2eAgNnSc1d1	rakouské
císařství	císařství	k1gNnSc1	císařství
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Sambir	Sambir	k1gInSc1	Sambir
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Lvivská	Lvivský	k2eAgFnSc1d1	Lvivský
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
polský	polský	k2eAgMnSc1d1	polský
geodet	geodet	k1gMnSc1	geodet
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
profesor	profesor	k1gMnSc1	profesor
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
vedoucí	vedoucí	k1gMnSc1	vedoucí
katedry	katedra	k1gFnSc2	katedra
geodezie	geodezie	k1gFnSc2	geodezie
a	a	k8xC	a
sférické	sférický	k2eAgFnSc2d1	sférická
astronomie	astronomie	k1gFnSc2	astronomie
(	(	kIx(	(
<g/>
zal	zal	k?	zal
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
Technické	technický	k2eAgFnSc2d1	technická
akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
TA	ta	k0	ta
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Technische	Technische	k1gFnSc1	Technische
Akademie	akademie	k1gFnSc2	akademie
im	im	k?	im
Lemberg	Lemberg	k1gInSc1	Lemberg
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
–	–	k?	–
k.	k.	k?	k.
u.	u.	k?	u.
k.	k.	k?	k.
Technische	Technische	k1gNnSc7	Technische
Hochschule	Hochschule	k1gFnSc2	Hochschule
in	in	k?	in
Lemberg	Lemberg	k1gMnSc1	Lemberg
<g/>
,	,	kIx,	,
c.	c.	k?	c.
a	a	k8xC	a
k.	k.	k?	k.
Vysoká	vysoká	k1gFnSc1	vysoká
škola	škola	k1gFnSc1	škola
technická	technický	k2eAgFnSc1d1	technická
(	(	kIx(	(
<g/>
VŠT	VŠT	kA	VŠT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
2000	[number]	k4	2000
–	–	k?	–
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc1d1	národní
univerzita	univerzita	k1gFnSc1	univerzita
Lvivská	Lvivský	k2eAgFnSc1d1	Lvivský
polytechnika	polytechnika	k1gFnSc1	polytechnika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česko-polský	českoolský	k2eAgMnSc1d1	česko-polský
šlechtic	šlechtic	k1gMnSc1	šlechtic
erbu	erb	k1gInSc2	erb
Poraj	Poraj	k1gInSc1	Poraj
(	(	kIx(	(
<g/>
Páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Růže	růž	k1gFnSc2	růž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Samboře	Sambora	k1gFnSc6	Sambora
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
letech	let	k1gInPc6	let
1851	[number]	k4	1851
<g/>
–	–	k?	–
<g/>
1853	[number]	k4	1853
na	na	k7c6	na
TA	ten	k3xDgNnPc4	ten
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
praktickou	praktický	k2eAgFnSc4d1	praktická
geometrii	geometrie	k1gFnSc4	geometrie
přednášel	přednášet	k5eAaImAgMnS	přednášet
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Netvořic	Netvořice	k1gFnPc2	Netvořice
profesor	profesor	k1gMnSc1	profesor
<g />
.	.	kIx.	.
</s>
<s>
Ignác	Ignác	k1gMnSc1	Ignác
Lemoch	Lemoch	k1gMnSc1	Lemoch
(	(	kIx(	(
<g/>
*	*	kIx~	*
1802	[number]	k4	1802
<g/>
,	,	kIx,	,
†	†	k?	†
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
VŠT	VŠT	kA	VŠT
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1858	[number]	k4	1858
<g/>
–	–	k?	–
<g/>
1860	[number]	k4	1860
na	na	k7c6	na
Stavovském	stavovský	k2eAgInSc6d1	stavovský
polytechnickém	polytechnický	k2eAgInSc6d1	polytechnický
ústavu	ústav	k1gInSc6	ústav
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
žákem	žák	k1gMnSc7	žák
profesorů	profesor	k1gMnPc2	profesor
Karla	Karel	k1gMnSc2	Karel
Kořistky	Kořistka	k1gFnSc2	Kořistka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1825	[number]	k4	1825
<g/>
,	,	kIx,	,
†	†	k?	†
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
Jelínka	Jelínek	k1gMnSc4	Jelínek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1822	[number]	k4	1822
<g/>
,	,	kIx,	,
†	†	k?	†
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
spolužákem	spolužák	k1gMnSc7	spolužák
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
–	–	k?	–
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
budoucí	budoucí	k2eAgMnSc1d1	budoucí
generálmajor	generálmajor	k1gMnSc1	generálmajor
c.	c.	k?	c.
a	a	k8xC	a
k.	k.	k?	k.
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
armády	armáda	k1gFnSc2	armáda
Robert	Robert	k1gMnSc1	Robert
Daublebský	Daublebský	k2eAgMnSc1d1	Daublebský
ze	z	k7c2	z
Sternecku	Sterneck	k1gInSc2	Sterneck
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
nivelačních	nivelační	k2eAgFnPc2d1	nivelační
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
profesora	profesor	k1gMnSc2	profesor
Karla	Karel	k1gMnSc2	Karel
Kořistky	Kořistka	k1gFnSc2	Kořistka
a	a	k8xC	a
astronomických	astronomický	k2eAgNnPc2d1	astronomické
a	a	k8xC	a
meteorologických	meteorologický	k2eAgNnPc2d1	meteorologické
pozorování	pozorování	k1gNnPc2	pozorování
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
profesora	profesor	k1gMnSc2	profesor
Karla	Karel	k1gMnSc2	Karel
Jelínka	Jelínek	k1gMnSc2	Jelínek
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1864	[number]	k4	1864
pracoval	pracovat	k5eAaImAgMnS	pracovat
soukromým	soukromý	k2eAgMnSc7d1	soukromý
asistentem	asistent	k1gMnSc7	asistent
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
matematiky	matematika	k1gFnSc2	matematika
Johanna	Johann	k1gMnSc4	Johann
Liebleina	Lieblein	k1gMnSc4	Lieblein
(	(	kIx(	(
<g/>
*	*	kIx~	*
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
†	†	k?	†
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1867	[number]	k4	1867
do	do	k7c2	do
září	září	k1gNnSc2	září
1871	[number]	k4	1871
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
a	a	k8xC	a
soukromý	soukromý	k2eAgMnSc1d1	soukromý
docent	docent	k1gMnSc1	docent
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
technice	technika	k1gFnSc6	technika
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Františka	František	k1gMnSc2	František
Müllera	Müller	k1gMnSc2	Müller
(	(	kIx(	(
<g/>
*	*	kIx~	*
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
†	†	k?	†
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
habilitaci	habilitace	k1gFnSc6	habilitace
jako	jako	k8xC	jako
soukromý	soukromý	k2eAgMnSc1d1	soukromý
docent	docent	k1gMnSc1	docent
požádal	požádat	k5eAaPmAgMnS	požádat
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1871	[number]	k4	1871
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
práce	práce	k1gFnSc2	práce
"	"	kIx"	"
<g/>
Geodetické	geodetický	k2eAgNnSc1d1	geodetické
měření	měření	k1gNnSc1	měření
výšek	výška	k1gFnPc2	výška
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sepsal	sepsat	k5eAaPmAgMnS	sepsat
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
habilitační	habilitační	k2eAgFnSc2d1	habilitační
komise	komise	k1gFnSc2	komise
ve	v	k7c4	v
složení	složení	k1gNnSc4	složení
profesorů	profesor	k1gMnPc2	profesor
Františka	František	k1gMnSc4	František
Müllera	Müller	k1gMnSc4	Müller
<g/>
,	,	kIx,	,
Viléma	Vilém	k1gMnSc4	Vilém
Bukovského	Bukovský	k1gMnSc4	Bukovský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1831	[number]	k4	1831
<g/>
,	,	kIx,	,
†	†	k?	†
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Gabriela	Gabriela	k1gFnSc1	Gabriela
Blažka	Blažka	k1gFnSc1	Blažka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
†	†	k?	†
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
soukromým	soukromý	k2eAgMnSc7d1	soukromý
docentem	docent	k1gMnSc7	docent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgMnS	být
povolán	povolat	k5eAaPmNgMnS	povolat
do	do	k7c2	do
Lvova	Lvov	k1gInSc2	Lvov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1871	[number]	k4	1871
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
vedoucím	vedoucí	k1gMnSc7	vedoucí
katedry	katedra	k1gFnSc2	katedra
geodézie	geodézie	k1gFnSc2	geodézie
a	a	k8xC	a
sférické	sférický	k2eAgFnSc2d1	sférická
astronomie	astronomie	k1gFnSc2	astronomie
TA	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
astronomickou	astronomický	k2eAgFnSc4d1	astronomická
observatoř	observatoř	k1gFnSc4	observatoř
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
a	a	k8xC	a
meteorologickou	meteorologický	k2eAgFnSc4d1	meteorologická
stanici	stanice	k1gFnSc4	stanice
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
usnesení	usnesení	k1gNnSc2	usnesení
Kolegia	kolegium	k1gNnSc2	kolegium
profesorů	profesor	k1gMnPc2	profesor
plnil	plnit	k5eAaImAgMnS	plnit
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1874	[number]	k4	1874
také	také	k6eAd1	také
funkci	funkce	k1gFnSc4	funkce
prvního	první	k4xOgMnSc2	první
knihovníka	knihovník	k1gMnSc2	knihovník
TA	ta	k0	ta
<g/>
,	,	kIx,	,
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
knihovny	knihovna	k1gFnSc2	knihovna
a	a	k8xC	a
otevření	otevření	k1gNnSc2	otevření
studovny	studovna	k1gFnSc2	studovna
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
tvůrcem	tvůrce	k1gMnSc7	tvůrce
první	první	k4xOgFnSc2	první
nivelační	nivelační	k2eAgFnSc2d1	nivelační
sítě	síť	k1gFnSc2	síť
Lvova	Lvov	k1gInSc2	Lvov
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
městské	městský	k2eAgFnSc2d1	městská
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
.	.	kIx.	.
</s>
<s>
Určení	určení	k1gNnSc1	určení
výšky	výška	k1gFnSc2	výška
základního	základní	k2eAgInSc2d1	základní
bodu	bod	k1gInSc2	bod
této	tento	k3xDgFnSc2	tento
sítě	síť	k1gFnSc2	síť
barometrickou	barometrický	k2eAgFnSc4d1	barometrická
nivelaci	nivelace	k1gFnSc4	nivelace
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
Komise	komise	k1gFnSc2	komise
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
polní	polní	k2eAgMnSc1d1	polní
podmaršálek	podmaršálek	k1gMnSc1	podmaršálek
August	August	k1gMnSc1	August
von	von	k1gInSc4	von
Fligely	Fligela	k1gFnSc2	Fligela
(	(	kIx(	(
<g/>
*	*	kIx~	*
1810	[number]	k4	1810
<g/>
,	,	kIx,	,
†	†	k?	†
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
a	a	k8xC	a
plukovník	plukovník	k1gMnSc1	plukovník
Johann	Johann	k1gMnSc1	Johann
Ganahl	Ganahl	k1gMnSc1	Ganahl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
†	†	k?	†
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
bývalí	bývalý	k2eAgMnPc1d1	bývalý
ředitele	ředitel	k1gMnSc2	ředitel
Vojenského	vojenský	k2eAgInSc2d1	vojenský
zeměpisného	zeměpisný	k2eAgInSc2d1	zeměpisný
ústavu	ústav	k1gInSc2	ústav
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
astronomie	astronomie	k1gFnSc2	astronomie
Jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Krakově	krakův	k2eAgFnSc6d1	Krakova
Michał	Michał	k1gFnSc6	Michał
Franciszek	Franciszka	k1gFnPc2	Franciszka
Karliński	Karlińsk	k1gFnSc2	Karlińsk
(	(	kIx(	(
<g/>
*	*	kIx~	*
1830	[number]	k4	1830
<g/>
,	,	kIx,	,
†	†	k?	†
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
geodezie	geodezie	k1gFnSc2	geodezie
VŠT	VŠT	kA	VŠT
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
Josef	Josef	k1gMnSc1	Josef
Philipp	Philipp	k1gMnSc1	Philipp
Herr	Herr	k1gMnSc1	Herr
(	(	kIx(	(
<g/>
*	*	kIx~	*
1819	[number]	k4	1819
<g/>
,	,	kIx,	,
†	†	k?	†
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
památku	památka	k1gFnSc4	památka
o	o	k7c6	o
stabilizaci	stabilizace	k1gFnSc6	stabilizace
tohoto	tento	k3xDgInSc2	tento
bodu	bod	k1gInSc2	bod
byla	být	k5eAaImAgFnS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
budově	budova	k1gFnSc6	budova
Národní	národní	k2eAgFnSc2d1	národní
univerzity	univerzita	k1gFnSc2	univerzita
Lvovská	lvovský	k2eAgFnSc1d1	Lvovská
polytechnika	polytechnika	k1gFnSc1	polytechnika
úsilím	úsilí	k1gNnSc7	úsilí
současné	současný	k2eAgFnSc2d1	současná
katedry	katedra	k1gFnSc2	katedra
geodezie	geodezie	k1gFnSc2	geodezie
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
textem	text	k1gInSc7	text
(	(	kIx(	(
<g/>
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
"	"	kIx"	"
<g/>
В	В	k?	В
п	п	k?	п
п	п	k?	п
н	н	k?	н
м	м	k?	м
м	м	k?	м
Л	Л	k?	Л
<g/>
.	.	kIx.	.
З	З	k?	З
у	у	k?	у
1880	[number]	k4	1880
р	р	k?	р
в	в	k?	в
а	а	k?	а
<g/>
,	,	kIx,	,
а	а	k?	а
К	К	k?	К
А	А	k?	А
<g/>
,	,	kIx,	,
п	п	k?	п
з	з	k?	з
к	к	k?	к
г	г	k?	г
<g/>
,	,	kIx,	,
р	р	k?	р
Л	Л	k?	Л
П	П	k?	П
п	п	k?	п
Д	Д	k?	Д
З	З	k?	З
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
děkanem	děkan	k1gMnSc7	děkan
fakulty	fakulta	k1gFnSc2	fakulta
silničního	silniční	k2eAgInSc2d1	silniční
a	a	k8xC	a
vodního	vodní	k2eAgNnSc2d1	vodní
inženýrství	inženýrství	k1gNnSc2	inženýrství
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
/	/	kIx~	/
<g/>
79	[number]	k4	79
a	a	k8xC	a
1879	[number]	k4	1879
<g/>
/	/	kIx~	/
<g/>
80	[number]	k4	80
<g/>
)	)	kIx)	)
a	a	k8xC	a
fakulty	fakulta	k1gFnSc2	fakulta
strojní	strojní	k2eAgFnSc2d1	strojní
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
/	/	kIx~	/
<g/>
85	[number]	k4	85
<g/>
,	,	kIx,	,
1885	[number]	k4	1885
<g/>
/	/	kIx~	/
<g/>
86	[number]	k4	86
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
<g/>
/	/	kIx~	/
<g/>
87	[number]	k4	87
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
<g/>
/	/	kIx~	/
<g/>
88	[number]	k4	88
<g/>
)	)	kIx)	)
a	a	k8xC	a
rektorem	rektor	k1gMnSc7	rektor
VŠT	VŠT	kA	VŠT
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
/	/	kIx~	/
<g/>
89	[number]	k4	89
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
a	a	k8xC	a
polském	polský	k2eAgInSc6d1	polský
jazyce	jazyk	k1gInSc6	jazyk
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
učebnic	učebnice	k1gFnPc2	učebnice
a	a	k8xC	a
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
pojednání	pojednání	k1gNnPc2	pojednání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
byli	být	k5eAaImAgMnP	být
publikovaní	publikovaný	k2eAgMnPc1d1	publikovaný
knižně	knižně	k6eAd1	knižně
nebo	nebo	k8xC	nebo
v	v	k7c6	v
"	"	kIx"	"
<g/>
Pamiętniku	Pamiętnik	k1gMnSc3	Pamiętnik
Akademii	akademie	k1gFnSc4	akademie
umiejętności	umiejętnośce	k1gFnSc3	umiejętnośce
w	w	k?	w
Krakowie	Krakowie	k1gFnSc1	Krakowie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Wydzial	Wydzial	k1gInSc1	Wydzial
matematyczno-przyrodniczy	matematycznorzyrodnicza	k1gFnSc2	matematyczno-przyrodnicza
<g/>
)	)	kIx)	)
a	a	k8xC	a
časopisech	časopis	k1gInPc6	časopis
"	"	kIx"	"
<g/>
Przegląd	Przegląd	k1gMnSc1	Przegląd
techniczny	techniczna	k1gFnSc2	techniczna
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Czasopiśmo	Czasopiśma	k1gFnSc5	Czasopiśma
techniczne	technicznout	k5eAaPmIp3nS	technicznout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Krakovské	krakovský	k2eAgFnSc2d1	Krakovská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
Technické	technický	k2eAgFnSc2d1	technická
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
a	a	k8xC	a
Polytechnické	polytechnický	k2eAgFnPc1d1	polytechnická
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
společnosti	společnost	k1gFnSc2	společnost
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
i	i	k9	i
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
Lvova	Lvov	k1gInSc2	Lvov
a	a	k8xC	a
poslancem	poslanec	k1gMnSc7	poslanec
Sejmu	sejmout	k5eAaPmIp1nS	sejmout
Království	království	k1gNnSc1	království
Haliče	Halič	k1gFnSc2	Halič
a	a	k8xC	a
Lodomerie	Lodomerie	k1gFnSc2	Lodomerie
za	za	k7c4	za
město	město	k1gNnSc4	město
Sambir	Sambira	k1gFnPc2	Sambira
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Samboře	Sambora	k1gFnSc6	Sambora
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
r.	r.	kA	r.
1989	[number]	k4	1989
existoval	existovat	k5eAaImAgInS	existovat
na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
městském	městský	k2eAgInSc6d1	městský
hřbitově	hřbitov	k1gInSc6	hřbitov
jeho	jeho	k3xOp3gInSc4	jeho
hrob	hrob	k1gInSc4	hrob
s	s	k7c7	s
nadpisem	nadpis	k1gInSc7	nadpis
<g/>
:	:	kIx,	:
Dominik	Dominik	k1gMnSc1	Dominik
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
/	/	kIx~	/
Profesor	profesor	k1gMnSc1	profesor
Politechniki	Politechnik	k1gFnSc2	Politechnik
Lwowskiej	Lwowskiej	k1gInSc1	Lwowskiej
/	/	kIx~	/
Poseł	Poseł	k1gFnSc1	Poseł
do	do	k7c2	do
Sejmu	Sejm	k1gInSc2	Sejm
/	/	kIx~	/
Obywatel	Obywatel	k1gMnSc1	Obywatel
Sambora	Sambor	k1gMnSc2	Sambor
/	/	kIx~	/
1832	[number]	k4	1832
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
VII	VII	kA	VII
<g/>
.1889	.1889	k4	.1889
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
Dzieślewski	Dzieślewsk	k1gFnSc2	Dzieślewsk
<g/>
,	,	kIx,	,
také	také	k9	také
Zdzisławski	Zdzisławsk	k1gFnSc2	Zdzisławsk
(	(	kIx(	(
<g/>
*	*	kIx~	*
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
†	†	k?	†
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
VŠT	VŠT	kA	VŠT
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
polski	polski	k6eAd1	polski
profesor	profesor	k1gMnSc1	profesor
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
<g/>
,	,	kIx,	,
asistent	asistent	k1gMnSc1	asistent
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
geodezie	geodezie	k1gFnSc2	geodezie
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Placyd	Placyd	k1gInSc1	Placyd
Zdzisław	Zdzisław	k1gFnSc2	Zdzisław
Dziwiński	Dziwińsk	k1gFnSc2	Dziwińsk
(	(	kIx(	(
<g/>
*	*	kIx~	*
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
<g/>
†	†	k?	†
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
VŠT	VŠT	kA	VŠT
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
asistent	asistent	k1gMnSc1	asistent
katedry	katedra	k1gFnSc2	katedra
geodezie	geodezie	k1gFnSc2	geodezie
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maurycy	Maurycy	k1gInPc4	Maurycy
Pius	Pius	k1gMnSc1	Pius
Rudzki	Rudzk	k1gFnSc2	Rudzk
(	(	kIx(	(
<g/>
*	*	kIx~	*
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
†	†	k?	†
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
geofyzik	geofyzik	k1gMnSc1	geofyzik
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
Seweryn	Seweryn	k1gMnSc1	Seweryn
Widt	Widt	k1gMnSc1	Widt
(	(	kIx(	(
<g/>
*	*	kIx~	*
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
†	†	k?	†
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
VŠT	VŠT	kA	VŠT
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
<g/>
,	,	kIx,	,
asistent	asistent	k1gMnSc1	asistent
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
vedoucí	vedoucí	k1gMnSc1	vedoucí
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
katedry	katedra	k1gFnSc2	katedra
geodezie	geodezie	k1gFnSc2	geodezie
<g/>
.	.	kIx.	.
<g/>
Synovec	synovec	k1gMnSc1	synovec
profesora	profesor	k1gMnSc2	profesor
D.	D.	kA	D.
Zbrožka	Zbrožka	k1gFnSc1	Zbrožka
<g/>
.	.	kIx.	.
</s>
<s>
August	August	k1gMnSc1	August
Wiktor	Wiktor	k1gMnSc1	Wiktor
Witkowski	Witkowske	k1gFnSc4	Witkowske
(	(	kIx(	(
<g/>
*	*	kIx~	*
1854	[number]	k4	1854
<g/>
,	,	kIx,	,
†	†	k?	†
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
Jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
Jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lvovské	lvovský	k2eAgFnPc1d1	Lvovská
polytechniky	polytechnika	k1gFnPc1	polytechnika
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asistent	asistent	k1gMnSc1	asistent
katedry	katedra	k1gFnSc2	katedra
geodezie	geodezie	k1gFnSc2	geodezie
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
/	/	kIx~	/
<g/>
79	[number]	k4	79
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
O	o	k7c6	o
libeli	libel	k1gInSc6	libel
i	i	k8xC	i
osi	osi	k?	osi
kolimacyjnej	kolimacyjnat	k5eAaPmRp2nS	kolimacyjnat
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Czasopismo	Czasopisma	k1gFnSc5	Czasopisma
techniczne	technicznout	k5eAaPmIp3nS	technicznout
<g/>
.	.	kIx.	.
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
nr	nr	k?	nr
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
69	[number]	k4	69
<g/>
–	–	k?	–
<g/>
72	[number]	k4	72
<g/>
;	;	kIx,	;
nr	nr	k?	nr
7	[number]	k4	7
<g/>
,	,	kIx,	,
s.	s.	k?	s.
81	[number]	k4	81
<g/>
–	–	k?	–
<g/>
82	[number]	k4	82
;	;	kIx,	;
nr	nr	k?	nr
8	[number]	k4	8
<g/>
,	,	kIx,	,
s.	s.	k?	s.
104	[number]	k4	104
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
O	o	k7c6	o
niwelacyi	niwelacy	k1gFnSc6	niwelacy
ścisłej	ścisłej	k1gInSc1	ścisłej
:	:	kIx,	:
[	[	kIx(	[
<g/>
wykład	wykład	k6eAd1	wykład
na	na	k7c6	na
zgromadzeniu	zgromadzenium	k1gNnSc6	zgromadzenium
tyg	tyg	k?	tyg
<g/>
.	.	kIx.	.
</s>
<s>
T-wa	Ta	k1gMnSc1	T-wa
Politechnicznego	Politechnicznego	k1gMnSc1	Politechnicznego
we	we	k?	we
Lwowie	Lwowie	k1gFnSc1	Lwowie
(	(	kIx(	(
<g/>
6	[number]	k4	6
marca	marca	k6eAd1	marca
1886	[number]	k4	1886
r.	r.	kA	r.
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Czasopismo	Czasopisma	k1gFnSc5	Czasopisma
techniczne	technicznout	k5eAaPmIp3nS	technicznout
<g/>
.	.	kIx.	.
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
nr	nr	k?	nr
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
72	[number]	k4	72
<g/>
–	–	k?	–
<g/>
76	[number]	k4	76
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
O	o	k7c6	o
niwelacyi	niwelacy	k1gFnSc6	niwelacy
ścisłej	ścisłej	k1gInSc4	ścisłej
miasta	miast	k1gMnSc2	miast
Lwowa	Lwowus	k1gMnSc2	Lwowus
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Czasopismo	Czasopisma	k1gFnSc5	Czasopisma
techniczne	technicznout	k5eAaPmIp3nS	technicznout
<g/>
.	.	kIx.	.
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
nr	nr	k?	nr
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
47	[number]	k4	47
<g/>
.	.	kIx.	.
</s>
<s>
Zbrožek	Zbrožka	k1gFnPc2	Zbrožka
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
O	o	k7c6	o
niwelacyi	niwelacy	k1gFnSc6	niwelacy
ścisłej	ścisłej	k1gInSc4	ścisłej
miasta	miast	k1gMnSc2	miast
Lwowa	Lwowus	k1gMnSc2	Lwowus
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Czasopismo	Czasopisma	k1gFnSc5	Czasopisma
techniczne	technicznout	k5eAaPmIp3nS	technicznout
<g/>
.	.	kIx.	.
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
nr	nr	k?	nr
10	[number]	k4	10
<g/>
,	,	kIx,	,
s.	s.	k?	s.
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
O	o	k7c6	o
planimetrach	planimetra	k1gFnPc6	planimetra
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Dżwignia	Dżwignium	k1gNnSc2	Dżwignium
<g/>
.	.	kIx.	.
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
nr	nr	k?	nr
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
;	;	kIx,	;
nr	nr	k?	nr
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
19	[number]	k4	19
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
<g/>
;	;	kIx,	;
nr	nr	k?	nr
6	[number]	k4	6
<g/>
,	,	kIx,	,
nr	nr	k?	nr
8	[number]	k4	8
<g/>
,	,	kIx,	,
nr	nr	k?	nr
9	[number]	k4	9
<g/>
,	,	kIx,	,
s.	s.	k?	s.
67	[number]	k4	67
<g/>
–	–	k?	–
<g/>
69	[number]	k4	69
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
O	o	k7c4	o
czasie	czasie	k1gFnPc4	czasie
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Czasopismo	Czasopisma	k1gFnSc5	Czasopisma
techniczne	technicznout	k5eAaPmIp3nS	technicznout
<g/>
.	.	kIx.	.
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
nr	nr	k?	nr
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
Opad	opad	k1gInSc1	opad
atmosferyczny	atmosferyczna	k1gFnSc2	atmosferyczna
na	na	k7c4	na
stacjach	stacjach	k1gInSc4	stacjach
deszczomiarowych	deszczomiarowych	k1gInSc1	deszczomiarowych
w	w	k?	w
r.	r.	kA	r.
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Pamiętnik	Pamiętnik	k1gMnSc1	Pamiętnik
Akademii	akademie	k1gFnSc4	akademie
umiejętności	umiejętnośce	k1gFnSc4	umiejętnośce
w	w	k?	w
Krakowie	Krakowie	k1gFnSc1	Krakowie
<g/>
.	.	kIx.	.
</s>
<s>
Wydzial	Wydzial	k1gInSc1	Wydzial
matematyczno-przyrodniczy	matematycznorzyrodnicza	k1gFnSc2	matematyczno-przyrodnicza
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
XXII	XXII	kA	XXII
<g/>
.	.	kIx.	.
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
219	[number]	k4	219
<g/>
–	–	k?	–
<g/>
233	[number]	k4	233
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
Sprawozdanie	Sprawozdanie	k1gFnSc1	Sprawozdanie
z	z	k7c2	z
lustracyi	lustracy	k1gFnSc2	lustracy
stacyj	stacyj	k1gMnSc1	stacyj
ombrometrycznych	ombrometrycznych	k1gMnSc1	ombrometrycznych
w	w	k?	w
Galicyi	Galicy	k1gFnSc2	Galicy
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Czasopismo	Czasopisma	k1gFnSc5	Czasopisma
techniczne	technicznout	k5eAaPmIp3nS	technicznout
<g/>
.	.	kIx.	.
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
nr	nr	k?	nr
7	[number]	k4	7
<g/>
,	,	kIx,	,
s.	s.	k?	s.
87	[number]	k4	87
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
Teoria	Teorium	k1gNnSc2	Teorium
najmniejszych	najmniejszych	k1gMnSc1	najmniejszych
kwadratów	kwadratów	k?	kwadratów
/	/	kIx~	/
Wyklady	Wyklad	k1gInPc4	Wyklad
spisane	spisanout	k5eAaPmIp3nS	spisanout
przez	przez	k1gMnSc1	przez
A.	A.	kA	A.
Witkowskiego	Witkowskiego	k1gMnSc1	Witkowskiego
<g/>
.	.	kIx.	.
</s>
<s>
Lwów	Lwów	k?	Lwów
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
Teoria	Teorium	k1gNnSc2	Teorium
planimetru	planimetr	k1gInSc2	planimetr
biegunowego	biegunowego	k6eAd1	biegunowego
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Pamiętnik	Pamiętnik	k1gMnSc1	Pamiętnik
Akademii	akademie	k1gFnSc4	akademie
umiejętności	umiejętnośce	k1gFnSc4	umiejętnośce
w	w	k?	w
Krakowie	Krakowie	k1gFnSc1	Krakowie
:	:	kIx,	:
Wydzial	Wydzial	k1gInSc1	Wydzial
matematyczno-przyrodniczy	matematycznorzyrodnicza	k1gFnSc2	matematyczno-przyrodnicza
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
II	II	kA	II
<g/>
.	.	kIx.	.
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
113	[number]	k4	113
<g/>
–	–	k?	–
<g/>
123	[number]	k4	123
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
Vorträge	Vorträge	k1gFnSc1	Vorträge
der	drát	k5eAaImRp2nS	drát
Geodäsie	Geodäsie	k1gFnSc2	Geodäsie
gehalten	gehalten	k2eAgInSc1d1	gehalten
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
k.k.	k.k.	k?	k.k.
technischen	technischen	k1gInSc4	technischen
Academie	academia	k1gFnSc2	academia
im	im	k?	im
Jahre	Jahr	k1gInSc5	Jahr
1871	[number]	k4	1871
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
zusammengestellt	zusammengestellt	k2eAgInSc1d1	zusammengestellt
von	von	k1gInSc1	von
P.	P.	kA	P.
Dziwiński	Dziwińsk	k1gInSc3	Dziwińsk
(	(	kIx(	(
<g/>
Autolitografie	Autolitografie	k1gFnSc1	Autolitografie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lemberg	Lemberg	k1gMnSc1	Lemberg
<g/>
,	,	kIx,	,
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
216	[number]	k4	216
S.	S.	kA	S.
[	[	kIx(	[
<g/>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Z	Z	kA	Z
Obserwatorium	Obserwatorium	k1gNnSc1	Obserwatorium
c.k.	c.k.	k?	c.k.
Szkoly	Szkola	k1gFnSc2	Szkola
politechnicznej	politechnicznat	k5eAaImRp2nS	politechnicznat
we	we	k?	we
Lwowie	Lwowie	k1gFnSc1	Lwowie
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Czasopismo	Czasopisma	k1gFnSc5	Czasopisma
techniczne	technicznout	k5eAaPmIp3nS	technicznout
<g/>
.	.	kIx.	.
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
nr	nr	k?	nr
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
:	:	kIx,	:
Zastosowanie	Zastosowanie	k1gFnSc1	Zastosowanie
wyznaczników	wyznaczników	k?	wyznaczników
w	w	k?	w
teorji	teort	k5eAaPmIp1nS	teort
najmniejszych	najmniejszych	k1gInSc4	najmniejszych
kwadratów	kwadratów	k?	kwadratów
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Pamiętnik	Pamiętnik	k1gMnSc1	Pamiętnik
Akademii	akademie	k1gFnSc4	akademie
umiejętności	umiejętnośce	k1gFnSc4	umiejętnośce
w	w	k?	w
Krakowie	Krakowie	k1gFnSc1	Krakowie
<g/>
.	.	kIx.	.
</s>
<s>
Wydzial	Wydzial	k1gInSc1	Wydzial
matematyczno-przyrodniczy	matematycznorzyrodnicza	k1gFnSc2	matematyczno-przyrodnicza
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
IX	IX	kA	IX
<g/>
.	.	kIx.	.
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
119	[number]	k4	119
<g/>
–	–	k?	–
<g/>
218	[number]	k4	218
<g/>
.	.	kIx.	.
</s>
<s>
Drbal	Drbal	k1gMnSc1	Drbal
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
:	:	kIx,	:
Čeští	český	k2eAgMnPc1d1	český
zeměměřiči	zeměměřič	k1gMnPc1	zeměměřič
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
geodezie	geodezie	k1gFnSc2	geodezie
a	a	k8xC	a
kartografie	kartografie	k1gFnSc1	kartografie
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
110	[number]	k4	110
<g/>
.	.	kIx.	.
</s>
<s>
Drbal	Drbal	k1gMnSc1	Drbal
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
:	:	kIx,	:
Zbrožek	Zbrožka	k1gFnPc2	Zbrožka
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Bucko	Bucko	k1gNnSc1	Bucko
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
I.	I.	kA	I.
<g/>
:	:	kIx,	:
Vidomi	Vido	k1gFnPc7	Vido
včeni	včen	k1gMnPc1	včen
Deržavnoho	Deržavno	k1gMnSc4	Deržavno
universytetu	universytet	k1gInSc2	universytet
Ľvivs	Ľvivs	k1gInSc1	Ľvivs
<g/>
'	'	kIx"	'
<g/>
ka	ka	k?	ka
politechnika	politechnika	k1gFnSc1	politechnika
<g/>
.	.	kIx.	.
1844	[number]	k4	1844
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Biografičnyj	Biografičnyj	k1gMnSc1	Biografičnyj
dovidnyk	dovidnyk	k1gMnSc1	dovidnyk
<g/>
.	.	kIx.	.
</s>
<s>
Ľviv	Ľviv	k1gMnSc1	Ľviv
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
61	[number]	k4	61
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
5-7707-5706-X	[number]	k4	5-7707-5706-X
Drbal	Drbal	k1gMnSc1	Drbal
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
:	:	kIx,	:
Zbrožek	Zbrožka	k1gFnPc2	Zbrožka
Dominik	Dominik	k1gMnSc1	Dominik
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Čechy	Čech	k1gMnPc4	Čech
v	v	k7c6	v
Halyčyni	Halyčyně	k1gFnSc6	Halyčyně
:	:	kIx,	:
Biohrafičnyj	Biohrafičnyj	k1gInSc1	Biohrafičnyj
dovidnyk	dovidnyk	k1gInSc1	dovidnyk
=	=	kIx~	=
Češi	Čech	k1gMnPc1	Čech
v	v	k7c6	v
Haliči	Halič	k1gFnSc6	Halič
:	:	kIx,	:
Biografická	biografický	k2eAgFnSc1d1	biografická
příručka	příručka	k1gFnSc1	příručka
/	/	kIx~	/
Zprac	Zprac	k1gInSc1	Zprac
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
Drbal	Drbal	k1gMnSc1	Drbal
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Kril	Kril	k1gInSc1	Kril
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Motornyj	Motornyj	k1gInSc1	Motornyj
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Motornyj	Motornyj	k1gInSc1	Motornyj
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Topinka	topinka	k1gFnSc1	topinka
<g/>
.	.	kIx.	.
</s>
<s>
Ľviv	Ľviv	k1gInSc1	Ľviv
:	:	kIx,	:
Centr	centr	k1gInSc1	centr
Jevropy	Jevropa	k1gFnSc2	Jevropa
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
966-7022-20-X	[number]	k4	966-7022-20-X
Drbal	Drbal	k1gMnSc1	Drbal
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
:	:	kIx,	:
Z	z	k7c2	z
geodetického	geodetický	k2eAgInSc2d1	geodetický
a	a	k8xC	a
kartografického	kartografický	k2eAgInSc2d1	kartografický
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
:	:	kIx,	:
Dominik	Dominik	k1gMnSc1	Dominik
Zbrožek	Zbrožka	k1gFnPc2	Zbrožka
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Geodetický	geodetický	k2eAgInSc1d1	geodetický
a	a	k8xC	a
kartografický	kartografický	k2eAgInSc1d1	kartografický
obzor	obzor	k1gInSc1	obzor
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
č.	č.	k?	č.
9	[number]	k4	9
<g/>
,	,	kIx,	,
s.	s.	k?	s.
228	[number]	k4	228
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0016-7096	[number]	k4	0016-7096
Drbal	Drbal	k1gMnSc1	Drbal
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
:	:	kIx,	:
Dominik	Dominik	k1gMnSc1	Dominik
Zbrožek	Zbrožka	k1gFnPc2	Zbrožka
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Geodezija	Geodezija	k1gMnSc1	Geodezija
<g/>
,	,	kIx,	,
kartografija	kartografija	k1gMnSc1	kartografija
i	i	k8xC	i
aerofotoznimanňa	aerofotoznimanňa	k1gMnSc1	aerofotoznimanňa
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
č.	č.	k?	č.
55	[number]	k4	55
<g/>
,	,	kIx,	,
s.	s.	k?	s.
136	[number]	k4	136
<g/>
–	–	k?	–
<g/>
142	[number]	k4	142
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0130-1039	[number]	k4	0130-1039
Drbal	drbat	k5eAaImAgInS	drbat
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
;	;	kIx,	;
Kocáb	Kocáb	k1gMnSc1	Kocáb
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
;	;	kIx,	;
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Dominik	Dominik	k1gMnSc1	Dominik
Zbrožek	Zbrožka	k1gFnPc2	Zbrožka
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
–	–	k?	–
peršyj	peršyj	k1gMnSc1	peršyj
zaviduvač	zaviduvač	k1gMnSc1	zaviduvač
kafedry	kafedr	k1gInPc7	kafedr
geodeziji	geodezít	k5eAaPmIp1nS	geodezít
Ľvivs	Ľvivs	k1gInSc1	Ľvivs
<g/>
'	'	kIx"	'
<g/>
koji	koji	k1gNnSc1	koji
politechniky	politechnika	k1gFnSc2	politechnika
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Visnyk	Visnyko	k1gNnPc2	Visnyko
geodeziji	geodezít	k5eAaPmIp1nS	geodezít
ta	ten	k3xDgNnPc4	ten
kartografiji	kartografít	k5eAaPmIp1nS	kartografít
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
č.	č.	k?	č.
2	[number]	k4	2
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
37	[number]	k4	37
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
<g/>
.	.	kIx.	.
</s>
<s>
Dziwiński	Dziwińsk	k1gMnPc1	Dziwińsk
<g/>
,	,	kIx,	,
Placyd	Placyd	k1gMnSc1	Placyd
<g/>
:	:	kIx,	:
Dominik	Dominik	k1gMnSc1	Dominik
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
,	,	kIx,	,
rektor	rektor	k1gMnSc1	rektor
Szkoły	Szkoła	k1gFnSc2	Szkoła
Politechnicznej	Politechnicznej	k1gFnSc2	Politechnicznej
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Czasopiśmo	Czasopiśma	k1gFnSc5	Czasopiśma
techniczne	technicznout	k5eAaPmIp3nS	technicznout
<g/>
.	.	kIx.	.
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
nr	nr	k?	nr
13	[number]	k4	13
<g/>
,	,	kIx,	,
s.	s.	k?	s.
97	[number]	k4	97
<g/>
–	–	k?	–
<g/>
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
<g/>
,	,	kIx,	,
Marcin	Marcin	k1gMnSc1	Marcin
<g/>
:	:	kIx,	:
Astronomia	Astronomia	k1gFnSc1	Astronomia
w	w	k?	w
Polsce	Polska	k1gFnSc3	Polska
(	(	kIx(	(
<g/>
zarys	zarys	k1gInSc1	zarys
historyczny	historyczna	k1gFnSc2	historyczna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kraków	Kraków	k?	Kraków
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Kafedra	Kafedra	k1gFnSc1	Kafedra
geodeziji	geodezít	k5eAaPmIp1nS	geodezít
130	[number]	k4	130
rokiv	rokiva	k1gFnPc2	rokiva
:	:	kIx,	:
Istoryko-bibliografičnyj	Istorykoibliografičnyj	k1gInSc1	Istoryko-bibliografičnyj
narys	narys	k1gInSc1	narys
/	/	kIx~	/
V.I.	V.I.	k1gFnSc1	V.I.
Vaščenko	Vaščenka	k1gFnSc5	Vaščenka
<g/>
,	,	kIx,	,
O.J.	O.J.	k1gMnSc1	O.J.
Drbal	Drbal	k1gMnSc1	Drbal
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
M.	M.	kA	M.
Kolgunov	Kolgunov	k1gInSc1	Kolgunov
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
R.	R.	kA	R.
Tartačyns	Tartačynsa	k1gFnPc2	Tartačynsa
<g/>
'	'	kIx"	'
<g/>
ka	ka	k?	ka
<g/>
,	,	kIx,	,
O.I.	O.I.	k1gMnSc1	O.I.
Tereščuk	Tereščuk	k1gMnSc1	Tereščuk
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
S.	S.	kA	S.
Trevoho	Trevo	k1gMnSc2	Trevo
;	;	kIx,	;
Za	za	k7c2	za
red	red	k?	red
<g/>
.	.	kIx.	.
</s>
<s>
O.I.	O.I.	k?	O.I.
Moroza	Moroza	k1gFnSc1	Moroza
<g/>
.	.	kIx.	.
</s>
<s>
Ľviv	Ľviv	k1gInSc1	Ľviv
:	:	kIx,	:
Ľvivs	Ľvivs	k1gInSc1	Ľvivs
<g/>
'	'	kIx"	'
<g/>
ka	ka	k?	ka
politechnika	politechnika	k1gFnSc1	politechnika
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
5	[number]	k4	5
<g/>
,	,	kIx,	,
114	[number]	k4	114
<g/>
–	–	k?	–
<g/>
116	[number]	k4	116
<g/>
.	.	kIx.	.
</s>
<s>
Kafedra	Kafedra	k1gFnSc1	Kafedra
geodeziji	geodezít	k5eAaPmIp1nS	geodezít
135	[number]	k4	135
rokiv	rokiva	k1gFnPc2	rokiva
:	:	kIx,	:
Istoryko-bibliografičnyj	Istorykoibliografičnyj	k1gInSc1	Istoryko-bibliografičnyj
narys	narys	k1gInSc1	narys
/	/	kIx~	/
V.	V.	kA	V.
Vaščenko	Vaščenka	k1gFnSc5	Vaščenka
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Drbal	Drbal	k1gMnSc1	Drbal
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Kolgunov	Kolgunov	k1gInSc1	Kolgunov
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Moroz	Moroz	k1gInSc1	Moroz
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Tarnavs	Tarnavs	k1gInSc1	Tarnavs
<g/>
'	'	kIx"	'
<g/>
kyj	kyj	k1gInSc1	kyj
;	;	kIx,	;
Editor	editor	k1gInSc1	editor
O.	O.	kA	O.
Moroz	Moroz	k1gInSc1	Moroz
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
dopl	dopl	k1gMnSc1	dopl
<g/>
.	.	kIx.	.
</s>
<s>
Ľviv	Ľviv	k1gInSc1	Ľviv
:	:	kIx,	:
Ľvivs	Ľvivs	k1gInSc1	Ľvivs
<g/>
'	'	kIx"	'
<g/>
ka	ka	k?	ka
politechnika	politechnika	k1gFnSc1	politechnika
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
14	[number]	k4	14
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
,	,	kIx,	,
127	[number]	k4	127
<g/>
–	–	k?	–
<g/>
128	[number]	k4	128
<g/>
.	.	kIx.	.
</s>
<s>
Krawczyk	Krawczyk	k1gInSc1	Krawczyk
<g/>
,	,	kIx,	,
Jerzy	Jerz	k1gInPc1	Jerz
<g/>
:	:	kIx,	:
Dominik	Dominik	k1gMnSc1	Dominik
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
w	w	k?	w
setną	setną	k?	setną
rocznice	rocznice	k1gFnSc2	rocznice
śmierci	śmierce	k1gMnSc3	śmierce
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Przegląd	Przegląd	k1gMnSc1	Przegląd
geodezyjny	geodezyjna	k1gMnSc2	geodezyjna
<g/>
.	.	kIx.	.
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
nr	nr	k?	nr
11	[number]	k4	11
<g/>
,	,	kIx,	,
s.	s.	k?	s.
III	III	kA	III
<g/>
–	–	k?	–
<g/>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0033-2127	[number]	k4	0033-2127
Krawczyk	Krawczyka	k1gFnPc2	Krawczyka
<g/>
,	,	kIx,	,
Jerzy	Jerza	k1gFnSc2	Jerza
<g/>
:	:	kIx,	:
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
Dominik	Dominik	k1gMnSc1	Dominik
herbu	herbat	k5eAaPmIp1nS	herbat
Poraj	Poraj	k1gFnSc4	Poraj
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Słownik	Słownik	k1gMnSc1	Słownik
biograficzny	biograficzna	k1gFnSc2	biograficzna
techników	techników	k?	techników
polskich	polskich	k1gMnSc1	polskich
<g/>
.	.	kIx.	.
</s>
<s>
Tо	Tо	k?	Tо
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Warszawa	Warszawa	k1gMnSc1	Warszawa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
179	[number]	k4	179
<g/>
–	–	k?	–
<g/>
180	[number]	k4	180
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
83-85001-32-8	[number]	k4	83-85001-32-8
Krawczyk	Krawczyka	k1gFnPc2	Krawczyka
<g/>
,	,	kIx,	,
Jerzy	Jerz	k1gInPc1	Jerz
<g/>
:	:	kIx,	:
Lwowska	Lwowska	k1gFnSc1	Lwowska
szkoła	szkoła	k1gFnSc1	szkoła
geodezyjna	geodezyjna	k1gFnSc1	geodezyjna
<g/>
.	.	kIx.	.
</s>
<s>
Warszawa	Warszawa	k1gMnSc1	Warszawa
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
23	[number]	k4	23
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Kucharzewski	Kucharzewsk	k1gMnPc1	Kucharzewsk
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
<g/>
:	:	kIx,	:
Piśmiennictwo	Piśmiennictwo	k1gMnSc1	Piśmiennictwo
techniczne	technicznout	k5eAaPmIp3nS	technicznout
polskie	polskie	k1gFnPc4	polskie
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Warszawa	Warszawa	k1gMnSc1	Warszawa
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
253	[number]	k4	253
<g/>
–	–	k?	–
<g/>
254	[number]	k4	254
<g/>
.	.	kIx.	.
</s>
<s>
Kucharzewski	Kucharzewsk	k1gMnPc1	Kucharzewsk
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
<g/>
:	:	kIx,	:
Szkoła	Szkoła	k1gFnSc1	Szkoła
Politechniczna	Politechniczna	k1gFnSc1	Politechniczna
Lwowska	Lwowska	k1gFnSc1	Lwowska
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Przegląd	Przegląd	k1gMnSc1	Przegląd
techniczny	techniczna	k1gFnSc2	techniczna
<g/>
.	.	kIx.	.
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
LIV	LIV	kA	LIV
<g/>
,	,	kIx,	,
nr	nr	k?	nr
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nacionaľnyj	Nacionaľnyj	k1gMnSc1	Nacionaľnyj
universytet	universytet	k1gMnSc1	universytet
"	"	kIx"	"
<g/>
Ľvivs	Ľvivs	k1gInSc1	Ľvivs
<g/>
'	'	kIx"	'
<g/>
ka	ka	k?	ka
politechnika	politechnika	k1gFnSc1	politechnika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ľviv	Ľviv	k1gInSc1	Ľviv
:	:	kIx,	:
Ľvivs	Ľvivs	k1gInSc1	Ľvivs
<g/>
'	'	kIx"	'
<g/>
ka	ka	k?	ka
politechnika	politechnika	k1gFnSc1	politechnika
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
7	[number]	k4	7
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
,	,	kIx,	,
133	[number]	k4	133
<g/>
,	,	kIx,	,
150	[number]	k4	150
<g/>
,	,	kIx,	,
151	[number]	k4	151
<g/>
.	.	kIx.	.
</s>
<s>
Popławski	Popławski	k1gNnSc1	Popławski
<g/>
,	,	kIx,	,
Zbysław	Zbysław	k1gFnSc1	Zbysław
<g/>
:	:	kIx,	:
Dzieje	Dziej	k1gInSc2	Dziej
Politechniki	Politechnik	k1gFnSc2	Politechnik
Lwowskiej	Lwowskiej	k1gInSc1	Lwowskiej
1844	[number]	k4	1844
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Wrocław-Warszawa-Kraków	Wrocław-Warszawa-Kraków	k?	Wrocław-Warszawa-Kraków
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
54	[number]	k4	54
<g/>
,	,	kIx,	,
60	[number]	k4	60
<g/>
,	,	kIx,	,
82	[number]	k4	82
<g/>
,	,	kIx,	,
83	[number]	k4	83
<g/>
,	,	kIx,	,
302	[number]	k4	302
<g/>
,	,	kIx,	,
304	[number]	k4	304
<g/>
,	,	kIx,	,
308	[number]	k4	308
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
83-04-03932-X	[number]	k4	83-04-03932-X
Popławski	Popławsk	k1gFnSc2	Popławsk
<g/>
,	,	kIx,	,
Zbysław	Zbysław	k1gFnSc1	Zbysław
<g/>
:	:	kIx,	:
Wykaz	Wykaz	k1gInSc1	Wykaz
pracowników	pracowników	k?	pracowników
naukowych	naukowych	k1gInSc1	naukowych
Politechniki	Politechniki	k1gNnSc1	Politechniki
Lwowskiej	Lwowskiej	k1gInSc1	Lwowskiej
w	w	k?	w
latach	latach	k1gInSc1	latach
1844	[number]	k4	1844
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Kraków	Kraków	k?	Kraków
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
61	[number]	k4	61
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Seria	Seria	k1gFnSc1	Seria
historyczno-techniczna	historycznoechniczna	k1gFnSc1	historyczno-techniczna
;	;	kIx,	;
z.	z.	k?	z.
2	[number]	k4	2
;	;	kIx,	;
monogr	monogr	k1gInSc1	monogr
<g/>
.	.	kIx.	.
;	;	kIx,	;
175	[number]	k4	175
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajączkowski	Zajączkowski	k1gNnSc1	Zajączkowski
<g/>
,	,	kIx,	,
Władysław	Władysław	k1gFnSc1	Władysław
<g/>
:	:	kIx,	:
C.	C.	kA	C.
k.	k.	k?	k.
Szkoła	Szkoł	k2eAgFnSc1d1	Szkoła
Politechniczna	Politechniczna	k1gFnSc1	Politechniczna
we	we	k?	we
Lwowie	Lwowie	k1gFnSc1	Lwowie
<g/>
.	.	kIx.	.
</s>
<s>
Rys	rys	k1gInSc4	rys
historyczny	historyczna	k1gFnSc2	historyczna
jej	on	k3xPp3gNnSc2	on
założenia	założenium	k1gNnSc2	założenium
i	i	k9	i
rozwoju	rozwoju	k5eAaPmIp1nS	rozwoju
<g/>
,	,	kIx,	,
tudzież	tudzież	k?	tudzież
stan	stan	k1gInSc4	stan
jej	on	k3xPp3gInSc4	on
obecny	obecny	k?	obecny
<g/>
.	.	kIx.	.
</s>
<s>
Lwów	Lwów	k?	Lwów
:	:	kIx,	:
nakł	nakł	k?	nakł
<g/>
.	.	kIx.	.
</s>
<s>
Szkoły	Szkoł	k2eAgFnPc4d1	Szkoła
Politechnicznej	Politechnicznej	k1gFnPc4	Politechnicznej
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
170	[number]	k4	170
s.	s.	k?	s.
Dominik	Dominik	k1gMnSc1	Dominik
Zbrożek	Zbrożek	k1gMnSc1	Zbrożek
<g/>
.	.	kIx.	.
</s>
<s>
Wspomnienie	Wspomnienie	k1gFnSc1	Wspomnienie
pośmiertne	pośmiertnout	k5eAaPmIp3nS	pośmiertnout
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Przegląd	Przegląd	k1gMnSc1	Przegląd
techniczny	techniczna	k1gFnSc2	techniczna
<g/>
.	.	kIx.	.
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
XXVI	XXVI	kA	XXVI
<g/>
,	,	kIx,	,
s.	s.	k?	s.
212	[number]	k4	212
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
П	П	k?	П
п	п	k?	п
н	н	k?	н
м	м	k?	м
м	м	k?	м
Л	Л	k?	Л
(	(	kIx(	(
<g/>
Bod	bod	k1gInSc4	bod
první	první	k4xOgFnSc2	první
nivelační	nivelační	k2eAgFnSc2d1	nivelační
sítě	síť	k1gFnSc2	síť
města	město	k1gNnSc2	město
Lvova	Lvův	k2eAgNnSc2d1	Lvův
</s>
