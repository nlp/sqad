<s>
Kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Dominik	Dominik	k1gMnSc1	Dominik
Zbrožek	Zbrožka	k1gFnPc2	Zbrožka
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
geodet	geodet	k1gMnSc1	geodet
a	a	k8xC	a
první	první	k4xOgMnSc1	první
vedoucí	vedoucí	k1gMnSc1	vedoucí
katedry	katedra	k1gFnSc2	katedra
geodezie	geodezie	k1gFnSc2	geodezie
a	a	k8xC	a
sférické	sférický	k2eAgFnSc2d1	sférická
astronomie	astronomie	k1gFnSc2	astronomie
Technické	technický	k2eAgFnSc2d1	technická
akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
TA	ta	k0	ta
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
<g/>
?	?	kIx.	?
</s>
