<s>
Syndrom	syndrom	k1gInSc1	syndrom
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
určitou	určitý	k2eAgFnSc4d1	určitá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
označení	označení	k1gNnSc1	označení
dosud	dosud	k6eAd1	dosud
neobjasněné	objasněný	k2eNgFnPc4d1	neobjasněná
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Downův	Downův	k2eAgInSc1d1	Downův
syndrom	syndrom	k1gInSc1	syndrom
symptom	symptom	k1gInSc4	symptom
syndrom	syndrom	k1gInSc4	syndrom
karpálního	karpální	k2eAgInSc2d1	karpální
tunelu	tunel	k1gInSc2	tunel
syndrom	syndrom	k1gInSc1	syndrom
chronické	chronický	k2eAgFnSc2d1	chronická
únavy	únava	k1gFnSc2	únava
syndrom	syndrom	k1gInSc1	syndrom
náhlého	náhlý	k2eAgNnSc2d1	náhlé
úmrtí	úmrtí	k1gNnSc2	úmrtí
dítěte	dítě	k1gNnSc2	dítě
syndrom	syndrom	k1gInSc1	syndrom
z	z	k7c2	z
ozáření	ozáření	k1gNnSc2	ozáření
syndrom	syndrom	k1gInSc1	syndrom
získané	získaný	k2eAgFnSc2d1	získaná
imunitní	imunitní	k2eAgFnSc2d1	imunitní
nedostatečnosti	nedostatečnost	k1gFnSc2	nedostatečnost
postabortivní	postabortivní	k2eAgInSc4d1	postabortivní
syndrom	syndrom	k1gInSc4	syndrom
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
syndrom	syndrom	k1gInSc1	syndrom
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
syndrom	syndrom	k1gInSc1	syndrom
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
