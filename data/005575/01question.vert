<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
označuje	označovat	k5eAaImIp3nS	označovat
soubor	soubor	k1gInSc1	soubor
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
určitou	určitý	k2eAgFnSc4d1	určitá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
?	?	kIx.	?
</s>
