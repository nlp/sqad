<s>
Merkur	Merkur	k1gMnSc1
je	být	k5eAaImIp3nS
Slunci	slunce	k1gNnSc3
nejbližší	blízký	k2eAgFnSc1d3
a	a	k8xC
současně	současně	k6eAd1
i	i	k9
nejmenší	malý	k2eAgFnSc7d3
planetou	planeta	k1gFnSc7
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
40	[number]	k4
%	%	kIx~
větší	veliký	k2eAgFnSc2d2
velikosti	velikost	k1gFnSc2
než	než	k8xS
pozemský	pozemský	k2eAgInSc4d1
Měsíc	měsíc	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
menší	malý	k2eAgInSc4d2
než	než	k8xS
Jupiterův	Jupiterův	k2eAgInSc4d1
měsíc	měsíc	k1gInSc4
Ganymed	Ganymed	k1gMnSc1
a	a	k8xC
Saturnův	Saturnův	k2eAgInSc1d1
Titan	titan	k1gInSc1
<g/>
.	.	kIx.
</s>