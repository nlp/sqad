<s>
Stoletá	stoletý	k2eAgFnSc1d1	stoletá
válka	válka	k1gFnSc1	válka
byl	být	k5eAaImAgInS	být
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1337	[number]	k4	1337
<g/>
-	-	kIx~	-
<g/>
1453	[number]	k4	1453
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
byl	být	k5eAaImAgInS	být
spor	spor	k1gInSc1	spor
o	o	k7c4	o
nadvládu	nadvláda	k1gFnSc4	nadvláda
ve	v	k7c6	v
Flandrech	Flandry	k1gInPc6	Flandry
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
záminka	záminka	k1gFnSc1	záminka
posloužilo	posloužit	k5eAaPmAgNnS	posloužit
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Eduarda	Eduard	k1gMnSc2	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Angličané	Angličan	k1gMnPc1	Angličan
ztratili	ztratit	k5eAaPmAgMnP	ztratit
všechna	všechen	k3xTgNnPc4	všechen
francouzská	francouzský	k2eAgNnPc4d1	francouzské
území	území	k1gNnPc4	území
(	(	kIx(	(
<g/>
která	který	k3yIgNnPc4	který
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Normandské	normandský	k2eAgNnSc4d1	normandské
vévodství	vévodství	k1gNnSc4	vévodství
<g/>
)	)	kIx)	)
kromě	kromě	k7c2	kromě
Calais	Calais	k1gNnSc2	Calais
a	a	k8xC	a
Normanských	normanský	k2eAgInPc2d1	normanský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
válku	válka	k1gFnSc4	válka
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc2	označení
stoletá	stoletý	k2eAgFnSc1d1	stoletá
<g/>
,	,	kIx,	,
trvala	trvat	k5eAaImAgFnS	trvat
celých	celý	k2eAgNnPc2d1	celé
116	[number]	k4	116
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
však	však	k9	však
o	o	k7c4	o
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
sled	sled	k1gInSc4	sled
bojových	bojový	k2eAgFnPc2d1	bojová
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
střídavá	střídavý	k2eAgNnPc4d1	střídavé
období	období	k1gNnPc4	období
prudkých	prudký	k2eAgInPc2d1	prudký
bojů	boj	k1gInPc2	boj
následovaná	následovaný	k2eAgFnSc1d1	následovaná
roky	rok	k1gInPc4	rok
příměří	příměří	k1gNnSc2	příměří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
lze	lze	k6eAd1	lze
rozeznat	rozeznat	k5eAaPmF	rozeznat
3	[number]	k4	3
hlavní	hlavní	k2eAgFnPc4d1	hlavní
fáze	fáze	k1gFnPc4	fáze
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
byla	být	k5eAaImAgFnS	být
dvě	dva	k4xCgNnPc4	dva
suverénní	suverénní	k2eAgNnPc4d1	suverénní
království	království	k1gNnPc4	království
a	a	k8xC	a
angličtí	anglický	k2eAgMnPc1d1	anglický
králové	král	k1gMnPc1	král
spravovali	spravovat	k5eAaImAgMnP	spravovat
jako	jako	k8xS	jako
léno	léno	k1gNnSc4	léno
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
francouzského	francouzský	k2eAgNnSc2d1	francouzské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
se	se	k3xPyFc4	se
přičinil	přičinit	k5eAaPmAgMnS	přičinit
roku	rok	k1gInSc2	rok
1066	[number]	k4	1066
normanský	normanský	k2eAgMnSc1d1	normanský
vévoda	vévoda	k1gMnSc1	vévoda
Vilém	Vilém	k1gMnSc1	Vilém
I.	I.	kA	I.
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
porazil	porazit	k5eAaPmAgMnS	porazit
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Harolda	Harold	k1gMnSc2	Harold
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Godwinsona	Godwinsona	k1gFnSc1	Godwinsona
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hastingsu	Hastings	k1gInSc2	Hastings
<g/>
.	.	kIx.	.
</s>
<s>
Ponechal	ponechat	k5eAaPmAgMnS	ponechat
si	se	k3xPyFc3	se
i	i	k8xC	i
své	svůj	k3xOyFgFnSc2	svůj
državy	država	k1gFnSc2	država
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Angličané	Angličan	k1gMnPc1	Angličan
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
o	o	k7c4	o
tato	tento	k3xDgNnPc4	tento
území	území	k1gNnPc4	území
sváděli	svádět	k5eAaImAgMnP	svádět
boje	boj	k1gInPc4	boj
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
se	se	k3xPyFc4	se
střídavým	střídavý	k2eAgInSc7d1	střídavý
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Stoletá	stoletý	k2eAgFnSc1d1	stoletá
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
konečným	konečný	k2eAgNnSc7d1	konečné
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
těchto	tento	k3xDgInPc2	tento
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
příčinou	příčina	k1gFnSc7	příčina
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
i	i	k9	i
spor	spor	k1gInSc1	spor
obou	dva	k4xCgNnPc2	dva
království	království	k1gNnSc2	království
o	o	k7c4	o
nadvládu	nadvláda	k1gFnSc4	nadvláda
nad	nad	k7c7	nad
důležitým	důležitý	k2eAgNnSc7d1	důležité
obchodním	obchodní	k2eAgNnSc7d1	obchodní
centrem	centrum	k1gNnSc7	centrum
<g/>
,	,	kIx,	,
Flandrami	Flandra	k1gFnPc7	Flandra
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
sporech	spor	k1gInPc6	spor
převahu	převaha	k1gFnSc4	převaha
Francouzi	Francouz	k1gMnSc3	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1336	[number]	k4	1336
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
rok	rok	k1gInSc4	rok
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
flanderský	flanderský	k2eAgMnSc1d1	flanderský
kníže	kníže	k1gMnSc1	kníže
Ludvík	Ludvík	k1gMnSc1	Ludvík
I.	I.	kA	I.
pozatýkat	pozatýkat	k5eAaPmF	pozatýkat
všechny	všechen	k3xTgMnPc4	všechen
anglické	anglický	k2eAgMnPc4d1	anglický
obyvatele	obyvatel	k1gMnPc4	obyvatel
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
odvetné	odvetný	k2eAgNnSc1d1	odvetné
opatření	opatření	k1gNnSc1	opatření
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Angličanů	Angličan	k1gMnPc2	Angličan
<g/>
:	:	kIx,	:
zákaz	zákaz	k1gInSc1	zákaz
vývozu	vývoz	k1gInSc2	vývoz
anglické	anglický	k2eAgFnSc2d1	anglická
vlny	vlna	k1gFnSc2	vlna
do	do	k7c2	do
Flander	Flandry	k1gInPc2	Flandry
a	a	k8xC	a
dovozu	dovoz	k1gInSc2	dovoz
flanderských	flanderský	k2eAgInPc2d1	flanderský
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
především	především	k9	především
sukna	sukno	k1gNnSc2	sukno
<g/>
)	)	kIx)	)
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Flandrech	Flandry	k1gInPc6	Flandry
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
lidové	lidový	k2eAgNnSc1d1	lidové
povstání	povstání	k1gNnSc1	povstání
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jakoba	Jakob	k1gMnSc2	Jakob
van	van	k1gInSc1	van
Artelvede	Artelved	k1gMnSc5	Artelved
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
spojencem	spojenec	k1gMnSc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
napadl	napadnout	k5eAaPmAgMnS	napadnout
anglické	anglický	k2eAgFnPc4d1	anglická
državy	država	k1gFnPc4	država
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Francii	Francie	k1gFnSc3	Francie
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Stoletou	stoletý	k2eAgFnSc4d1	stoletá
válku	válka	k1gFnSc4	válka
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
etapy	etapa	k1gFnPc4	etapa
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc1	první
etapa	etapa	k1gFnSc1	etapa
(	(	kIx(	(
<g/>
1337	[number]	k4	1337
-	-	kIx~	-
1360	[number]	k4	1360
<g/>
)	)	kIx)	)
Druhá	druhý	k4xOgFnSc1	druhý
etapa	etapa	k1gFnSc1	etapa
(	(	kIx(	(
<g/>
1360	[number]	k4	1360
-	-	kIx~	-
1415	[number]	k4	1415
<g/>
)	)	kIx)	)
Třetí	třetí	k4xOgFnSc1	třetí
etapa	etapa	k1gFnSc1	etapa
(	(	kIx(	(
<g/>
1415	[number]	k4	1415
-	-	kIx~	-
1453	[number]	k4	1453
<g/>
)	)	kIx)	)
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
měli	mít	k5eAaImAgMnP	mít
Angličané	Angličan	k1gMnPc1	Angličan
především	především	k6eAd1	především
zájem	zájem	k1gInSc4	zájem
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
upevnit	upevnit	k5eAaPmF	upevnit
své	svůj	k3xOyFgNnSc4	svůj
obchodní	obchodní	k2eAgNnSc4d1	obchodní
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
Flandrech	Flandry	k1gInPc6	Flandry
<g/>
.	.	kIx.	.
</s>
<s>
Spojencem	spojenec	k1gMnSc7	spojenec
Francie	Francie	k1gFnSc2	Francie
bylo	být	k5eAaImAgNnS	být
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
snášelo	snášet	k5eAaImAgNnS	snášet
svoji	svůj	k3xOyFgFnSc4	svůj
lenní	lenní	k2eAgFnSc4d1	lenní
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Spojencem	spojenec	k1gMnSc7	spojenec
Anglie	Anglie	k1gFnSc2	Anglie
byly	být	k5eAaImAgInP	být
samotné	samotný	k2eAgInPc1d1	samotný
Flandry	Flandry	k1gInPc1	Flandry
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Sluys	Sluysa	k1gFnPc2	Sluysa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1340	[number]	k4	1340
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
Angličanům	Angličan	k1gMnPc3	Angličan
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
průlivem	průliv	k1gInSc7	průliv
La	la	k1gNnSc2	la
Manche	Manche	k1gNnSc2	Manche
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
Angličanům	Angličan	k1gMnPc3	Angličan
dařilo	dařit	k5eAaImAgNnS	dařit
v	v	k7c6	v
námořních	námořní	k2eAgInPc6d1	námořní
střetech	střet	k1gInPc6	střet
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozemních	pozemní	k2eAgInPc6d1	pozemní
tolik	tolik	k6eAd1	tolik
štěstí	štěstit	k5eAaImIp3nP	štěstit
neměli	mít	k5eNaImAgMnP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Ztratili	ztratit	k5eAaPmAgMnP	ztratit
dokonce	dokonce	k9	dokonce
i	i	k9	i
svého	svůj	k3xOyFgMnSc4	svůj
velkého	velký	k2eAgMnSc4d1	velký
spojence	spojenec	k1gMnSc4	spojenec
Jakoba	Jakob	k1gMnSc4	Jakob
van	van	k1gInSc1	van
Artevelde	Arteveld	k1gInSc5	Arteveld
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1345	[number]	k4	1345
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dosadit	dosadit	k5eAaPmF	dosadit
na	na	k7c4	na
flanderský	flanderský	k2eAgInSc4d1	flanderský
trůn	trůn	k1gInSc4	trůn
prince	princ	k1gMnSc2	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
však	však	k9	však
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
anglická	anglický	k2eAgFnSc1d1	anglická
armáda	armáda	k1gFnSc1	armáda
vedená	vedený	k2eAgFnSc1d1	vedená
Eduardem	Eduard	k1gMnSc7	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
ve	v	k7c6	v
významné	významný	k2eAgFnSc6d1	významná
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
nad	nad	k7c7	nad
mnohonásobnou	mnohonásobný	k2eAgFnSc7d1	mnohonásobná
přesilou	přesila	k1gFnSc7	přesila
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
boji	boj	k1gInSc6	boj
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
osvědčili	osvědčit	k5eAaPmAgMnP	osvědčit
velšští	velšský	k2eAgMnPc1d1	velšský
lukostřelci	lukostřelec	k1gMnPc1	lukostřelec
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
upevnil	upevnit	k5eAaPmAgInS	upevnit
jejich	jejich	k3xOp3gFnSc4	jejich
morálku	morálka	k1gFnSc4	morálka
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přikázal	přikázat	k5eAaPmAgMnS	přikázat
svým	svůj	k3xOyFgMnPc3	svůj
rytířům	rytíř	k1gMnPc3	rytíř
sesednout	sesednout	k5eAaPmF	sesednout
z	z	k7c2	z
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
bojovat	bojovat	k5eAaImF	bojovat
bok	bok	k1gInSc4	bok
po	po	k7c6	po
boku	bok	k1gInSc6	bok
s	s	k7c7	s
lukostřelci	lukostřelec	k1gMnPc7	lukostřelec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
padl	padnout	k5eAaPmAgMnS	padnout
též	též	k9	též
významný	významný	k2eAgMnSc1d1	významný
spojenec	spojenec	k1gMnSc1	spojenec
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
slepý	slepý	k2eAgMnSc1d1	slepý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc4	jeho
chrabrosti	chrabrost	k1gFnPc4	chrabrost
vzdal	vzdát	k5eAaPmAgMnS	vzdát
později	pozdě	k6eAd2	pozdě
hold	hold	k1gInSc4	hold
anglický	anglický	k2eAgMnSc1d1	anglický
princ	princ	k1gMnSc1	princ
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Eduarda	Eduard	k1gMnSc2	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Přijal	přijmout	k5eAaPmAgMnS	přijmout
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
znaku	znak	k1gInSc2	znak
tři	tři	k4xCgNnPc1	tři
pera	pero	k1gNnPc1	pero
z	z	k7c2	z
chocholu	chochol	k1gInSc2	chochol
jeho	jeho	k3xOp3gFnSc2	jeho
přilby	přilba	k1gFnSc2	přilba
a	a	k8xC	a
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
přijal	přijmout	k5eAaPmAgInS	přijmout
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
heslo	heslo	k1gNnSc4	heslo
Ich	Ich	k1gFnSc1	Ich
diene	dien	k1gInSc5	dien
(	(	kIx(	(
<g/>
sloužím	sloužit	k5eAaImIp1nS	sloužit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
bitvách	bitva	k1gFnPc6	bitva
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
taktice	taktika	k1gFnSc6	taktika
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgNnPc1d1	francouzské
vojska	vojsko	k1gNnPc1	vojsko
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgNnP	skládat
především	především	k6eAd1	především
ze	z	k7c2	z
šlechtické	šlechtický	k2eAgFnSc2d1	šlechtická
jízdy	jízda	k1gFnSc2	jízda
a	a	k8xC	a
z	z	k7c2	z
oddílů	oddíl	k1gInPc2	oddíl
měšťanů	měšťan	k1gMnPc2	měšťan
a	a	k8xC	a
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	Šlechta	k1gMnSc1	Šlechta
nižším	nízký	k2eAgInSc7d2	nižší
stavem	stav	k1gInSc7	stav
pohrdala	pohrdat	k5eAaImAgFnS	pohrdat
a	a	k8xC	a
bránila	bránit	k5eAaImAgFnS	bránit
jim	on	k3xPp3gMnPc3	on
zapojit	zapojit	k5eAaPmF	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
neshody	neshoda	k1gFnPc1	neshoda
a	a	k8xC	a
dokonalejší	dokonalý	k2eAgFnSc1d2	dokonalejší
taktika	taktika	k1gFnSc1	taktika
Angličanů	Angličan	k1gMnPc2	Angličan
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
sérii	série	k1gFnSc3	série
proher	prohra	k1gFnPc2	prohra
a	a	k8xC	a
tragédií	tragédie	k1gFnPc2	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
disponoval	disponovat	k5eAaBmAgInS	disponovat
nejen	nejen	k6eAd1	nejen
výbornou	výborný	k2eAgFnSc7d1	výborná
jízdou	jízda	k1gFnSc7	jízda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
výbornými	výborný	k2eAgMnPc7d1	výborný
lukostřelci	lukostřelec	k1gMnPc7	lukostřelec
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
ozbrojenými	ozbrojený	k2eAgInPc7d1	ozbrojený
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
luky	luk	k1gInPc7	luk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
byli	být	k5eAaImAgMnP	být
lukostřelci	lukostřelec	k1gMnPc1	lukostřelec
schopni	schopen	k2eAgMnPc1d1	schopen
vyřadit	vyřadit	k5eAaPmF	vyřadit
z	z	k7c2	z
boje	boj	k1gInSc2	boj
francouzské	francouzský	k2eAgMnPc4d1	francouzský
jezdce	jezdec	k1gMnPc4	jezdec
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnPc1	jejich
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
kopí	kopí	k1gNnPc1	kopí
a	a	k8xC	a
těžké	těžký	k2eAgInPc1d1	těžký
meče	meč	k1gInPc1	meč
dostaly	dostat	k5eAaPmAgInP	dostat
vůbec	vůbec	k9	vůbec
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Lukostřelci	lukostřelec	k1gMnPc1	lukostřelec
se	se	k3xPyFc4	se
ukázali	ukázat	k5eAaPmAgMnP	ukázat
jako	jako	k9	jako
nepostradatelná	postradatelný	k2eNgFnSc1d1	nepostradatelná
součást	součást	k1gFnSc1	součást
anglické	anglický	k2eAgFnSc2d1	anglická
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1347	[number]	k4	1347
po	po	k7c6	po
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
obléhání	obléhání	k1gNnSc6	obléhání
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Angličané	Angličan	k1gMnPc1	Angličan
přístavní	přístavní	k2eAgFnSc4d1	přístavní
město	město	k1gNnSc4	město
Calais	Calais	k1gNnSc2	Calais
a	a	k8xC	a
drželi	držet	k5eAaImAgMnP	držet
ho	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
dvě	dva	k4xCgFnPc1	dva
stě	sto	k4xCgFnPc1	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1347	[number]	k4	1347
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
osmiletý	osmiletý	k2eAgInSc1d1	osmiletý
mír	mír	k1gInSc1	mír
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Anglií	Anglie	k1gFnSc7	Anglie
trvající	trvající	k2eAgFnSc7d1	trvající
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Filipa	Filip	k1gMnSc2	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1350	[number]	k4	1350
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
přezdívaný	přezdívaný	k2eAgInSc1d1	přezdívaný
Dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Poitiers	Poitiersa	k1gFnPc2	Poitiersa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1356	[number]	k4	1356
byli	být	k5eAaImAgMnP	být
Francouzi	Francouz	k1gMnPc1	Francouz
poraženi	poražen	k2eAgMnPc1d1	poražen
princem	princ	k1gMnSc7	princ
Eduardem	Eduard	k1gMnSc7	Eduard
a	a	k8xC	a
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
svými	svůj	k3xOyFgMnPc7	svůj
šlechtici	šlechtic	k1gMnPc7	šlechtic
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zajat	zajat	k2eAgInSc1d1	zajat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následných	následný	k2eAgInPc6d1	následný
zmatcích	zmatek	k1gInPc6	zmatek
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
chopil	chopit	k5eAaPmAgMnS	chopit
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Jana	Jan	k1gMnSc2	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
panstva	panstvo	k1gNnSc2	panstvo
a	a	k8xC	a
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
potlačil	potlačit	k5eAaPmAgInS	potlačit
protiválečnou	protiválečný	k2eAgFnSc4d1	protiválečná
vzpouru	vzpoura	k1gFnSc4	vzpoura
pařížských	pařížský	k2eAgMnPc2d1	pařížský
měšťanů	měšťan	k1gMnPc2	měšťan
(	(	kIx(	(
<g/>
vedenou	vedený	k2eAgFnSc4d1	vedená
Étiennem	Étienn	k1gMnSc7	Étienn
Marcelem	Marcel	k1gMnSc7	Marcel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k1gMnSc2	svůj
vítězství	vítězství	k1gNnSc1	vítězství
Karel	Karel	k1gMnSc1	Karel
nijak	nijak	k6eAd1	nijak
nezneužil	zneužít	k5eNaPmAgMnS	zneužít
<g/>
,	,	kIx,	,
jen	jen	k9	jen
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zavést	zavést	k5eAaPmF	zavést
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
pořádek	pořádek	k1gInSc4	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Mírem	mír	k1gInSc7	mír
v	v	k7c4	v
Brétigny	Brétign	k1gMnPc4	Brétign
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1360	[number]	k4	1360
skončila	skončit	k5eAaPmAgFnS	skončit
první	první	k4xOgFnSc1	první
etapa	etapa	k1gFnSc1	etapa
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koruny	koruna	k1gFnSc2	koruna
a	a	k8xC	a
Flandry	Flandry	k1gInPc1	Flandry
setrvaly	setrvat	k5eAaPmAgInP	setrvat
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Angličanům	Angličan	k1gMnPc3	Angličan
však	však	k9	však
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
Calais	Calais	k1gNnSc2	Calais
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
většina	většina	k1gFnSc1	většina
francouzského	francouzský	k2eAgNnSc2d1	francouzské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
měl	mít	k5eAaImAgMnS	mít
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
zaplatit	zaplatit	k5eAaPmF	zaplatit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
výkupné	výkupné	k1gNnSc4	výkupné
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
tří	tři	k4xCgInPc2	tři
milionů	milion	k4xCgInPc2	milion
dukátů	dukát	k1gInPc2	dukát
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nezaplatil	zaplatit	k5eNaPmAgMnS	zaplatit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
zajatec	zajatec	k1gMnSc1	zajatec
odveden	odveden	k2eAgMnSc1d1	odveden
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1364	[number]	k4	1364
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
udělil	udělit	k5eAaPmAgInS	udělit
svému	svůj	k3xOyFgInSc3	svůj
druhému	druhý	k4xOgMnSc3	druhý
synovi	syn	k1gMnSc3	syn
Filipu	Filip	k1gMnSc3	Filip
Smělému	Smělý	k1gMnSc3	Smělý
Burgundské	burgundský	k2eAgNnSc1d1	burgundské
vévodství	vévodství	k1gNnSc1	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
Mírem	mír	k1gInSc7	mír
z	z	k7c2	z
Brétigny	Brétigna	k1gFnSc2	Brétigna
začalo	začít	k5eAaPmAgNnS	začít
druhé	druhý	k4xOgNnSc1	druhý
období	období	k1gNnSc1	období
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1364	[number]	k4	1364
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
a	a	k8xC	a
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	let	k1gInSc4	let
Francouzi	Francouz	k1gMnPc1	Francouz
obnovili	obnovit	k5eAaPmAgMnP	obnovit
boje	boj	k1gInPc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
však	však	k9	však
už	už	k6eAd1	už
změnili	změnit	k5eAaPmAgMnP	změnit
svou	svůj	k3xOyFgFnSc4	svůj
taktiku	taktika	k1gFnSc4	taktika
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
schopného	schopný	k2eAgMnSc2d1	schopný
vojáka	voják	k1gMnSc2	voják
Bertranda	Bertrando	k1gNnSc2	Bertrando
z	z	k7c2	z
Guesclinu	Guesclin	k1gInSc2	Guesclin
vedli	vést	k5eAaImAgMnP	vést
malou	malý	k2eAgFnSc4d1	malá
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
bránili	bránit	k5eAaImAgMnP	bránit
města	město	k1gNnSc2	město
a	a	k8xC	a
velkým	velký	k2eAgFnPc3d1	velká
bitvám	bitva	k1gFnPc3	bitva
se	se	k3xPyFc4	se
opatrně	opatrně	k6eAd1	opatrně
vyhýbali	vyhýbat	k5eAaImAgMnP	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
vyplatil	vyplatit	k5eAaPmAgMnS	vyplatit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
moci	moc	k1gFnSc6	moc
už	už	k9	už
jen	jen	k9	jen
Bayonne	Bayonn	k1gInSc5	Bayonn
<g/>
,	,	kIx,	,
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
<g/>
,	,	kIx,	,
Brest	Brest	k1gInSc1	Brest
<g/>
,	,	kIx,	,
Cherbourg	Cherbourg	k1gInSc1	Cherbourg
a	a	k8xC	a
Calais	Calais	k1gNnSc1	Calais
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Karla	Karla	k1gFnSc1	Karla
V.	V.	kA	V.
Francie	Francie	k1gFnSc1	Francie
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
zbavil	zbavit	k5eAaPmAgMnS	zbavit
zemi	zem	k1gFnSc4	zem
loupeživých	loupeživý	k2eAgNnPc2d1	loupeživé
band	bando	k1gNnPc2	bando
bývalých	bývalý	k2eAgMnPc2d1	bývalý
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
zbourat	zbourat	k5eAaPmF	zbourat
mnoho	mnoho	k4c4	mnoho
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nestaly	stát	k5eNaPmAgFnP	stát
anglickou	anglický	k2eAgFnSc7d1	anglická
oporou	opora	k1gFnSc7	opora
<g/>
.	.	kIx.	.
</s>
<s>
Nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
parlament	parlament	k1gInSc1	parlament
zasedal	zasedat	k5eAaImAgInS	zasedat
stále	stále	k6eAd1	stále
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jak	jak	k8xS	jak
dosud	dosud	k6eAd1	dosud
příležitostně	příležitostně	k6eAd1	příležitostně
<g/>
,	,	kIx,	,
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
vydávat	vydávat	k5eAaImF	vydávat
zákony	zákon	k1gInPc4	zákon
vyhradil	vyhradit	k5eAaPmAgMnS	vyhradit
jen	jen	k6eAd1	jen
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
zvelebil	zvelebit	k5eAaPmAgMnS	zvelebit
Louvre	Louvre	k1gInSc4	Louvre
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
několik	několik	k4yIc4	několik
zámků	zámek	k1gInPc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1377	[number]	k4	1377
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
vládu	vláda	k1gFnSc4	vláda
svému	svůj	k3xOyFgMnSc3	svůj
vnukovi	vnuk	k1gMnSc3	vnuk
<g/>
,	,	kIx,	,
Richardu	Richard	k1gMnSc3	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
synovi	syn	k1gMnSc3	syn
prince	princ	k1gMnSc2	princ
Eduarda	Eduard	k1gMnSc2	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
strýcové	strýc	k1gMnPc1	strýc
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Lancasteru	Lancaster	k1gInSc2	Lancaster
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
z	z	k7c2	z
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
z	z	k7c2	z
Woodstocku	Woodstock	k1gInSc2	Woodstock
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
<g/>
,	,	kIx,	,
uznali	uznat	k5eAaPmAgMnP	uznat
mladého	mladý	k2eAgMnSc4d1	mladý
krále	král	k1gMnSc4	král
a	a	k8xC	a
svolili	svolit	k5eAaPmAgMnP	svolit
k	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
panování	panování	k1gNnSc2	panování
Richarda	Richard	k1gMnSc2	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
pokojné	pokojný	k2eAgNnSc1d1	pokojné
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1381	[number]	k4	1381
musel	muset	k5eAaImAgMnS	muset
potlačit	potlačit	k5eAaPmF	potlačit
lidové	lidový	k2eAgNnSc4d1	lidové
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Jana	Jan	k1gMnSc2	Jan
Wicliffa	Wicliff	k1gMnSc2	Wicliff
kladlo	klást	k5eAaImAgNnS	klást
za	za	k7c2	za
cíl	cíl	k1gInSc4	cíl
rovnost	rovnost	k1gFnSc4	rovnost
všech	všecek	k3xTgInPc2	všecek
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Wata	Wat	k1gInSc2	Wat
Tylera	Tylero	k1gNnSc2	Tylero
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Toweru	Towera	k1gFnSc4	Towera
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
a	a	k8xC	a
Wat	Wat	k1gMnSc1	Wat
Tyler	Tyler	k1gMnSc1	Tyler
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
povstání	povstání	k1gNnSc2	povstání
Wata	Wat	k1gInSc2	Wat
Tylera	Tyler	k1gMnSc2	Tyler
se	se	k3xPyFc4	se
Richard	Richard	k1gMnSc1	Richard
obrátil	obrátit	k5eAaPmAgMnS	obrátit
proti	proti	k7c3	proti
parlamentu	parlament	k1gInSc3	parlament
a	a	k8xC	a
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
strýcům	strýc	k1gMnPc3	strýc
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
neustále	neustále	k6eAd1	neustále
omezovali	omezovat	k5eAaImAgMnP	omezovat
jeho	jeho	k3xOp3gFnSc4	jeho
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1389	[number]	k4	1389
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
chopil	chopit	k5eAaPmAgMnS	chopit
plně	plně	k6eAd1	plně
moci	moct	k5eAaImF	moct
a	a	k8xC	a
o	o	k7c4	o
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1397	[number]	k4	1397
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
některé	některý	k3yIgMnPc4	některý
své	svůj	k3xOyFgMnPc4	svůj
protivníky	protivník	k1gMnPc4	protivník
zajmout	zajmout	k5eAaPmF	zajmout
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
i	i	k9	i
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Omezil	omezit	k5eAaPmAgMnS	omezit
moc	moc	k1gFnSc4	moc
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vládnout	vládnout	k5eAaImF	vládnout
absolutisticky	absolutisticky	k6eAd1	absolutisticky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1399	[number]	k4	1399
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Richardův	Richardův	k2eAgMnSc1d1	Richardův
strýc	strýc	k1gMnSc1	strýc
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
jeho	on	k3xPp3gInSc2	on
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
využil	využít	k5eAaPmAgInS	využít
královu	králův	k2eAgFnSc4d1	králova
nepřítomnost	nepřítomnost	k1gFnSc4	nepřítomnost
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
(	(	kIx(	(
<g/>
Richard	Richard	k1gMnSc1	Richard
tehdy	tehdy	k6eAd1	tehdy
podnikl	podniknout	k5eAaPmAgMnS	podniknout
válečnou	válečný	k2eAgFnSc4d1	válečná
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Spojil	spojit	k5eAaPmAgMnS	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
nespokojenými	spokojený	k2eNgInPc7d1	nespokojený
stavy	stav	k1gInPc7	stav
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
Richard	Richard	k1gMnSc1	Richard
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zajat	zajmout	k5eAaPmNgMnS	zajmout
a	a	k8xC	a
zbaven	zbavit	k5eAaPmNgMnS	zbavit
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
potom	potom	k6eAd1	potom
přijal	přijmout	k5eAaPmAgInS	přijmout
za	za	k7c4	za
krále	král	k1gMnSc4	král
syna	syn	k1gMnSc4	syn
Jana	Jan	k1gMnSc4	Jan
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
jako	jako	k8xS	jako
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
tak	tak	k6eAd1	tak
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
dosedla	dosednout	k5eAaPmAgFnS	dosednout
dynastie	dynastie	k1gFnSc1	dynastie
Lancasterů	Lancaster	k1gMnPc2	Lancaster
<g/>
.	.	kIx.	.
</s>
<s>
Sesazeného	sesazený	k2eAgMnSc4d1	sesazený
krále	král	k1gMnSc4	král
Richarda	Richard	k1gMnSc2	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
popravili	popravit	k5eAaPmAgMnP	popravit
(	(	kIx(	(
<g/>
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
mohla	moct	k5eAaImAgFnS	moct
vypadat	vypadat	k5eAaImF	vypadat
jako	jako	k9	jako
přirozená	přirozený	k2eAgFnSc1d1	přirozená
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1400	[number]	k4	1400
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Pontefract	Pontefract	k1gInSc1	Pontefract
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1380	[number]	k4	1380
vládl	vládnout	k5eAaImAgMnS	vládnout
syn	syn	k1gMnSc1	syn
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
pro	pro	k7c4	pro
zemi	zem	k1gFnSc4	zem
tehdy	tehdy	k6eAd1	tehdy
nastaly	nastat	k5eAaPmAgInP	nastat
zlé	zlý	k2eAgInPc1d1	zlý
časy	čas	k1gInPc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
mladého	mladý	k2eAgMnSc4d1	mladý
panovníka	panovník	k1gMnSc4	panovník
nejprve	nejprve	k6eAd1	nejprve
vládli	vládnout	k5eAaImAgMnP	vládnout
jeho	jeho	k3xOp3gMnPc1	jeho
strýcové	strýc	k1gMnPc1	strýc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
několik	několik	k4yIc4	několik
povstání	povstání	k1gNnPc2	povstání
<g/>
;	;	kIx,	;
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Flandrech	Flandry	k1gInPc6	Flandry
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc4	povstání
byla	být	k5eAaImAgFnS	být
potlačena	potlačen	k2eAgFnSc1d1	potlačena
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1384	[number]	k4	1384
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
Flandrech	Flandry	k1gInPc6	Flandry
ujal	ujmout	k5eAaPmAgMnS	ujmout
burgundský	burgundský	k2eAgMnSc1d1	burgundský
vévoda	vévoda	k1gMnSc1	vévoda
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
Flander	Flandry	k1gInPc2	Flandry
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
Burgundsko	Burgundsko	k1gNnSc1	Burgundsko
Francii	Francie	k1gFnSc6	Francie
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
odcizovat	odcizovat	k5eAaImF	odcizovat
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
stále	stále	k6eAd1	stále
nezávislejší	závislý	k2eNgFnSc4d2	nezávislejší
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1388	[number]	k4	1388
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
strýců	strýc	k1gMnPc2	strýc
a	a	k8xC	a
vybral	vybrat	k5eAaPmAgMnS	vybrat
si	se	k3xPyFc3	se
rádce	rádce	k1gMnSc1	rádce
z	z	k7c2	z
příslušníků	příslušník	k1gMnPc2	příslušník
nižší	nízký	k2eAgFnSc2d2	nižší
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1392	[number]	k4	1392
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
objevil	objevit	k5eAaPmAgMnS	objevit
první	první	k4xOgInSc4	první
záchvat	záchvat	k1gInSc4	záchvat
duševní	duševní	k2eAgFnSc2d1	duševní
choroby	choroba	k1gFnSc2	choroba
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
chopili	chopit	k5eAaPmAgMnP	chopit
jeho	jeho	k3xOp3gMnPc1	jeho
strýcové	strýc	k1gMnPc1	strýc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1404	[number]	k4	1404
burgundský	burgundský	k2eAgMnSc1d1	burgundský
vévoda	vévoda	k1gMnSc1	vévoda
Filip	Filip	k1gMnSc1	Filip
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
Nebojácný	bojácný	k2eNgMnSc1d1	nebojácný
si	se	k3xPyFc3	se
chtěl	chtít	k5eAaImAgMnS	chtít
zachovat	zachovat	k5eAaPmF	zachovat
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
zabránit	zabránit	k5eAaPmF	zabránit
králův	králův	k2eAgMnSc1d1	králův
bratr	bratr	k1gMnSc1	bratr
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
jej	on	k3xPp3gMnSc4	on
Jan	Jan	k1gMnSc1	Jan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1407	[number]	k4	1407
zavraždit	zavraždit	k5eAaPmF	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
burgundskou	burgundský	k2eAgFnSc7d1	burgundská
a	a	k8xC	a
orleánskou	orleánský	k2eAgFnSc7d1	Orleánská
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nepokojů	nepokoj	k1gInPc2	nepokoj
ve	v	k7c4	v
Francii	Francie	k1gFnSc4	Francie
využil	využít	k5eAaPmAgMnS	využít
nový	nový	k2eAgMnSc1d1	nový
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1415	[number]	k4	1415
se	se	k3xPyFc4	se
vylodil	vylodit	k5eAaPmAgMnS	vylodit
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
začalo	začít	k5eAaPmAgNnS	začít
třetí	třetí	k4xOgNnSc1	třetí
a	a	k8xC	a
poslední	poslední	k2eAgNnSc4d1	poslední
období	období	k1gNnSc4	období
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
nad	nad	k7c7	nad
šestinásobnou	šestinásobný	k2eAgFnSc7d1	šestinásobná
přesilou	přesila	k1gFnSc7	přesila
Francouzů	Francouz	k1gMnPc2	Francouz
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Azincourtu	Azincourt	k1gInSc2	Azincourt
podobnou	podobný	k2eAgFnSc7d1	podobná
taktikou	taktika	k1gFnSc7	taktika
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
princ	princ	k1gMnSc1	princ
Eduard	Eduard	k1gMnSc1	Eduard
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
a	a	k8xC	a
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Poitiers	Poitiersa	k1gFnPc2	Poitiersa
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
celou	celý	k2eAgFnSc4d1	celá
Normandii	Normandie	k1gFnSc4	Normandie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
královnou	královna	k1gFnSc7	královna
Isabelou	Isabela	k1gFnSc7	Isabela
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
burgundským	burgundský	k2eAgMnSc7d1	burgundský
vévodou	vévoda	k1gMnSc7	vévoda
Filipem	Filip	k1gMnSc7	Filip
Dobrým	dobrý	k2eAgMnSc7d1	dobrý
smlouvu	smlouva	k1gFnSc4	smlouva
z	z	k7c2	z
Troyes	Troyesa	k1gFnPc2	Troyesa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
měl	mít	k5eAaImAgMnS	mít
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
dostat	dostat	k5eAaPmF	dostat
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
dceru	dcera	k1gFnSc4	dcera
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Kateřinu	Kateřina	k1gFnSc4	Kateřina
a	a	k8xC	a
po	po	k7c6	po
tchánově	tchánův	k2eAgFnSc6d1	tchánova
smrti	smrt	k1gFnSc6	smrt
zasednout	zasednout	k5eAaPmF	zasednout
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Karel	Karel	k1gMnSc1	Karel
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
z	z	k7c2	z
následnictví	následnictví	k1gNnSc2	následnictví
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
Angličané	Angličan	k1gMnPc1	Angličan
našli	najít	k5eAaPmAgMnP	najít
v	v	k7c6	v
burgundském	burgundské	k1gNnSc6	burgundské
vévodovi	vévodův	k2eAgMnPc1d1	vévodův
mocného	mocný	k2eAgMnSc4d1	mocný
spojence	spojenec	k1gMnSc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
se	se	k3xPyFc4	se
však	však	k9	však
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koruny	koruna	k1gFnSc2	koruna
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1422	[number]	k4	1422
<g/>
,	,	kIx,	,
jen	jen	k9	jen
sedm	sedm	k4xCc4	sedm
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
Karlem	Karel	k1gMnSc7	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nečekaně	nečekaně	k6eAd1	nečekaně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	se	k3xPyFc3	se
devítiměsíčního	devítiměsíční	k2eAgMnSc4d1	devítiměsíční
syna	syn	k1gMnSc4	syn
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
nemluvně	nemluvně	k1gNnSc1	nemluvně
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c4	za
krále	král	k1gMnPc4	král
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
části	část	k1gFnSc6	část
Francie	Francie	k1gFnSc2	Francie
severně	severně	k6eAd1	severně
od	od	k7c2	od
Loiry	Loira	k1gFnSc2	Loira
a	a	k8xC	a
v	v	k7c6	v
Guyenne	Guyenn	k1gMnSc5	Guyenn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
za	za	k7c4	za
něho	on	k3xPp3gMnSc4	on
vládl	vládnout	k5eAaImAgMnS	vládnout
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Bedfordu	Bedford	k1gInSc2	Bedford
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
oblastech	oblast	k1gFnPc6	oblast
Francie	Francie	k1gFnSc2	Francie
vládl	vládnout	k5eAaImAgInS	vládnout
z	z	k7c2	z
města	město	k1gNnSc2	město
Bourges	Bourges	k1gMnSc1	Bourges
syn	syn	k1gMnSc1	syn
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
devatenáctiletý	devatenáctiletý	k2eAgMnSc1d1	devatenáctiletý
lenivý	lenivý	k2eAgMnSc1d1	lenivý
mladík	mladík	k1gMnSc1	mladík
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gNnSc3	jeho
sídelnímu	sídelní	k2eAgNnSc3d1	sídelní
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
Angličané	Angličan	k1gMnPc1	Angličan
posměšně	posměšně	k6eAd1	posměšně
říkali	říkat	k5eAaImAgMnP	říkat
Králík	Králík	k1gMnSc1	Králík
z	z	k7c2	z
Bourges	Bourgesa	k1gFnPc2	Bourgesa
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Bedfordu	Bedford	k1gInSc2	Bedford
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc2	jenž
vojska	vojsko	k1gNnSc2	vojsko
postoupila	postoupit	k5eAaPmAgFnS	postoupit
před	před	k7c4	před
Loiru	Loira	k1gFnSc4	Loira
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1428	[number]	k4	1428
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
Orléans	Orléans	k1gInSc4	Orléans
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
poslední	poslední	k2eAgFnSc1d1	poslední
pozice	pozice	k1gFnSc1	pozice
francouzských	francouzský	k2eAgFnPc2d1	francouzská
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
strategicky	strategicky	k6eAd1	strategicky
obrovsky	obrovsky	k6eAd1	obrovsky
důležité	důležitý	k2eAgNnSc1d1	důležité
město	město	k1gNnSc1	město
-	-	kIx~	-
prolomením	prolomení	k1gNnSc7	prolomení
francouzsko-anglické	francouzskonglický	k2eAgFnSc2d1	francouzsko-anglická
hranice	hranice	k1gFnSc2	hranice
na	na	k7c6	na
Loiře	Loira	k1gFnSc6	Loira
by	by	kYmCp3nS	by
už	už	k9	už
Angličanům	Angličan	k1gMnPc3	Angličan
nic	nic	k6eAd1	nic
nebránilo	bránit	k5eNaImAgNnS	bránit
dobýt	dobýt	k5eAaPmF	dobýt
zbytek	zbytek	k1gInSc4	zbytek
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
známého	známý	k1gMnSc2	známý
Dunoise	Dunoise	k1gFnSc2	Dunoise
<g/>
,	,	kIx,	,
přezdívaného	přezdívaný	k2eAgInSc2d1	přezdívaný
Bastard	bastard	k1gMnSc1	bastard
Orleánský	orleánský	k2eAgMnSc1d1	orleánský
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Francouzi	Francouz	k1gMnPc1	Francouz
bránili	bránit	k5eAaImAgMnP	bránit
až	až	k9	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
posil	posila	k1gFnPc2	posila
<g/>
.	.	kIx.	.
</s>
<s>
Přivedlo	přivést	k5eAaPmAgNnS	přivést
je	on	k3xPp3gNnSc4	on
prosté	prostý	k2eAgNnSc4d1	prosté
venkovské	venkovský	k2eAgNnSc4d1	venkovské
děvče	děvče	k1gNnSc4	děvče
Johanka	Johanka	k1gFnSc1	Johanka
a	a	k8xC	a
Orléans	Orléans	k1gInSc1	Orléans
ji	on	k3xPp3gFnSc4	on
přijal	přijmout	k5eAaPmAgMnS	přijmout
s	s	k7c7	s
obrovským	obrovský	k2eAgNnSc7d1	obrovské
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1429	[number]	k4	1429
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
Orléans	Orléans	k1gInSc4	Orléans
osvobodit	osvobodit	k5eAaPmF	osvobodit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
získala	získat	k5eAaPmAgFnS	získat
svoje	svůj	k3xOyFgNnSc4	svůj
bojové	bojový	k2eAgNnSc4d1	bojové
jméno	jméno	k1gNnSc4	jméno
Panna	Panna	k1gFnSc1	Panna
Orleánská	orleánský	k2eAgFnSc1d1	Orleánská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Remeši	Remeš	k1gFnSc6	Remeš
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Panna	Panna	k1gFnSc1	Panna
předtím	předtím	k6eAd1	předtím
dobyla	dobýt	k5eAaPmAgFnS	dobýt
<g/>
,	,	kIx,	,
korunován	korunován	k2eAgMnSc1d1	korunován
princ	princ	k1gMnSc1	princ
Karel	Karel	k1gMnSc1	Karel
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
Angličany	Angličan	k1gMnPc4	Angličan
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
nazýván	nazýván	k2eAgMnSc1d1	nazýván
Králík	Králík	k1gMnSc1	Králík
z	z	k7c2	z
Bourges	Bourgesa	k1gFnPc2	Bourgesa
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
minimálně	minimálně	k6eAd1	minimálně
Vzdorokrál	vzdorokrál	k1gMnSc1	vzdorokrál
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupením	vystoupení	k1gNnSc7	vystoupení
Jany	Jana	k1gFnSc2	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
ve	v	k7c6	v
stoleté	stoletý	k2eAgFnSc6d1	stoletá
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
uplatnily	uplatnit	k5eAaPmAgFnP	uplatnit
lidové	lidový	k2eAgFnPc1d1	lidová
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
Angličanům	Angličan	k1gMnPc3	Angličan
nabyla	nabýt	k5eAaPmAgFnS	nabýt
národního	národní	k2eAgInSc2d1	národní
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
museli	muset	k5eAaImAgMnP	muset
náhle	náhle	k6eAd1	náhle
čelit	čelit	k5eAaImF	čelit
potížím	potíž	k1gFnPc3	potíž
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
burgundským	burgundský	k2eAgMnSc7d1	burgundský
vévodou	vévoda	k1gMnSc7	vévoda
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
vojska	vojsko	k1gNnPc1	vojsko
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
strádala	strádat	k5eAaImAgFnS	strádat
nedostatkem	nedostatek	k1gInSc7	nedostatek
zásob	zásoba	k1gFnPc2	zásoba
a	a	k8xC	a
posil	posila	k1gFnPc2	posila
<g/>
.	.	kIx.	.
</s>
<s>
Počaly	počnout	k5eAaPmAgFnP	počnout
neshody	neshoda	k1gFnPc1	neshoda
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgMnPc7	dva
nejmocnějšími	mocný	k2eAgMnPc7d3	nejmocnější
panovníky	panovník	k1gMnPc7	panovník
lancasterské	lancasterský	k2eAgFnSc2d1	lancasterský
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
vévodou	vévoda	k1gMnSc7	vévoda
z	z	k7c2	z
Bedfordu	Bedford	k1gInSc2	Bedford
a	a	k8xC	a
vévodou	vévoda	k1gMnSc7	vévoda
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
<g/>
.	.	kIx.	.
</s>
<s>
Osvobození	osvobození	k1gNnSc1	osvobození
města	město	k1gNnSc2	město
Orléans	Orléansa	k1gFnPc2	Orléansa
a	a	k8xC	a
porážka	porážka	k1gFnSc1	porážka
Angličanů	Angličan	k1gMnPc2	Angličan
u	u	k7c2	u
měst	město	k1gNnPc2	město
Pataye	Patay	k1gInSc2	Patay
a	a	k8xC	a
Jargeau	Jargeaum	k1gNnSc6	Jargeaum
znamenaly	znamenat	k5eAaImAgFnP	znamenat
obrat	obrat	k5eAaPmF	obrat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
šlechta	šlechta	k1gFnSc1	šlechta
však	však	k9	však
záviděla	závidět	k5eAaImAgFnS	závidět
Janě	Jana	k1gFnSc6	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
její	její	k3xOp3gNnSc4	její
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
těžce	těžce	k6eAd1	těžce
nesla	nést	k5eAaImAgFnS	nést
<g/>
,	,	kIx,	,
že	že	k8xS	že
spásu	spása	k1gFnSc4	spása
Francii	Francie	k1gFnSc4	Francie
přineslo	přinést	k5eAaPmAgNnS	přinést
obyčejné	obyčejný	k2eAgNnSc1d1	obyčejné
děvče	děvče	k1gNnSc1	děvče
<g/>
.	.	kIx.	.
</s>
<s>
Jana	Jana	k1gFnSc1	Jana
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
vyštvána	vyštvat	k5eAaPmNgFnS	vyštvat
z	z	k7c2	z
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
osvobozování	osvobozování	k1gNnSc6	osvobozování
Compiégne	Compiégn	k1gInSc5	Compiégn
byla	být	k5eAaImAgFnS	být
zajata	zajmout	k5eAaPmNgFnS	zajmout
Burgunďany	Burgunďan	k1gMnPc7	Burgunďan
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1431	[number]	k4	1431
v	v	k7c6	v
Rouenu	Rouen	k1gInSc6	Rouen
upálena	upálit	k5eAaPmNgFnS	upálit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1435	[number]	k4	1435
bylo	být	k5eAaImAgNnS	být
svoláno	svolán	k2eAgNnSc1d1	svoláno
zasedání	zasedání	k1gNnSc1	zasedání
do	do	k7c2	do
města	město	k1gNnSc2	město
Arras	Arrasa	k1gFnPc2	Arrasa
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
dostavili	dostavit	k5eAaPmAgMnP	dostavit
vyslanci	vyslanec	k1gMnPc1	vyslanec
skoro	skoro	k6eAd1	skoro
všech	všecek	k3xTgInPc2	všecek
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Nepřekonatelné	překonatelný	k2eNgInPc1d1	nepřekonatelný
rozpory	rozpor	k1gInPc1	rozpor
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
však	však	k9	však
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
dohodě	dohoda	k1gFnSc3	dohoda
zabránily	zabránit	k5eAaPmAgInP	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
proto	proto	k8xC	proto
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
však	však	k9	však
ztratili	ztratit	k5eAaPmAgMnP	ztratit
svého	svůj	k3xOyFgMnSc4	svůj
burgundského	burgundský	k2eAgMnSc4d1	burgundský
spojence	spojenec	k1gMnSc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Příklon	příklon	k1gInSc4	příklon
k	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
Burgundsko	Burgundsko	k1gNnSc4	Burgundsko
zisk	zisk	k1gInSc1	zisk
mnoha	mnoho	k4c2	mnoho
území	území	k1gNnPc2	území
a	a	k8xC	a
lenní	lenní	k2eAgFnSc4d1	lenní
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
francouzském	francouzský	k2eAgMnSc6d1	francouzský
králi	král	k1gMnSc6	král
až	až	k9	až
do	do	k7c2	do
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
už	už	k9	už
Angličany	Angličan	k1gMnPc4	Angličan
stíhala	stíhat	k5eAaImAgFnS	stíhat
jedna	jeden	k4xCgFnSc1	jeden
porážka	porážka	k1gFnSc1	porážka
za	za	k7c7	za
druhou	druhý	k4xOgFnSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1436	[number]	k4	1436
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Francouzi	Francouz	k1gMnPc1	Francouz
Paříž	Paříž	k1gFnSc1	Paříž
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1449	[number]	k4	1449
Normandii	Normandie	k1gFnSc4	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1453	[number]	k4	1453
prohráli	prohrát	k5eAaPmAgMnP	prohrát
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
Castellione	Castellion	k1gInSc5	Castellion
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
válečné	válečný	k2eAgFnPc4d1	válečná
akce	akce	k1gFnPc4	akce
přerušit	přerušit	k5eAaPmF	přerušit
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
moci	moc	k1gFnSc6	moc
jedině	jedině	k6eAd1	jedině
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
Calais	Calais	k1gNnSc2	Calais
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
a	a	k8xC	a
Normanské	normanský	k2eAgInPc1d1	normanský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
opět	opět	k6eAd1	opět
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
příměří	příměří	k1gNnPc2	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
však	však	k9	však
byla	být	k5eAaImAgFnS	být
konfliktem	konflikt	k1gInSc7	konflikt
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
upadla	upadnout	k5eAaPmAgFnS	upadnout
do	do	k7c2	do
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
politika	politika	k1gFnSc1	politika
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XI	XI	kA	XI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
tučným	tučný	k2eAgNnSc7d1	tučné
výkupným	výkupné	k1gNnSc7	výkupné
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Angličanů	Angličan	k1gMnPc2	Angličan
prodlužování	prodlužování	k1gNnSc2	prodlužování
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
nakonec	nakonec	k6eAd1	nakonec
učinilo	učinit	k5eAaImAgNnS	učinit
své	svůj	k3xOyFgMnPc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
konflikt	konflikt	k1gInSc1	konflikt
středověké	středověký	k2eAgFnSc2d1	středověká
Evropy	Evropa	k1gFnSc2	Evropa
skončil	skončit	k5eAaPmAgInS	skončit
nenápadně	nápadně	k6eNd1	nápadně
a	a	k8xC	a
bez	bez	k7c2	bez
uzavření	uzavření	k1gNnSc2	uzavření
konečné	konečný	k2eAgFnSc2d1	konečná
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Calais	Calais	k1gNnSc7	Calais
získali	získat	k5eAaPmAgMnP	získat
Francouzi	Francouz	k1gMnPc1	Francouz
za	za	k7c2	za
zcela	zcela	k6eAd1	zcela
jiných	jiný	k2eAgFnPc2d1	jiná
okolností	okolnost	k1gFnPc2	okolnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1558	[number]	k4	1558
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc4d1	jediné
území	území	k1gNnSc4	území
z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
se	se	k3xPyFc4	se
Angličanům	Angličan	k1gMnPc3	Angličan
na	na	k7c6	na
Francouzech	Francouzy	k1gInPc6	Francouzy
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
podržet	podržet	k5eAaPmF	podržet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Normanské	normanský	k2eAgInPc1d1	normanský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
etapa	etapa	k1gFnSc1	etapa
(	(	kIx(	(
<g/>
1337	[number]	k4	1337
<g/>
-	-	kIx~	-
<g/>
1360	[number]	k4	1360
<g/>
)	)	kIx)	)
1337	[number]	k4	1337
-	-	kIx~	-
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
vznesl	vznést	k5eAaPmAgMnS	vznést
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
začátek	začátek	k1gInSc1	začátek
otevřeného	otevřený	k2eAgInSc2d1	otevřený
konfliktu	konflikt	k1gInSc2	konflikt
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
1338	[number]	k4	1338
-	-	kIx~	-
francouzské	francouzský	k2eAgNnSc1d1	francouzské
loďstvo	loďstvo	k1gNnSc1	loďstvo
napadlo	napadnout	k5eAaPmAgNnS	napadnout
a	a	k8xC	a
vypálilo	vypálit	k5eAaPmAgNnS	vypálit
anglický	anglický	k2eAgInSc1d1	anglický
přístav	přístav	k1gInSc1	přístav
Portsmouth	Portsmouth	k1gInSc1	Portsmouth
1340	[number]	k4	1340
-	-	kIx~	-
Angličané	Angličan	k1gMnPc1	Angličan
porazili	porazit	k5eAaPmAgMnP	porazit
francouzské	francouzský	k2eAgNnSc4d1	francouzské
loďstvo	loďstvo	k1gNnSc4	loďstvo
u	u	k7c2	u
Sluis	Sluis	k1gFnSc2	Sluis
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
nadvládu	nadvláda	k1gFnSc4	nadvláda
nad	nad	k7c7	nad
kanálem	kanál	k1gInSc7	kanál
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
1345	[number]	k4	1345
-	-	kIx~	-
v	v	k7c6	v
Gentu	Gento	k1gNnSc6	Gento
byl	být	k5eAaImAgMnS	být
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
významný	významný	k2eAgMnSc1d1	významný
spojenec	spojenec	k1gMnSc1	spojenec
Anglie	Anglie	k1gFnSc2	Anglie
Jakob	Jakob	k1gMnSc1	Jakob
van	van	k1gInSc1	van
Artevelde	Arteveld	k1gInSc5	Arteveld
1346	[number]	k4	1346
-	-	kIx~	-
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
porazila	porazit	k5eAaPmAgFnS	porazit
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
padl	padnout	k5eAaPmAgMnS	padnout
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
začátek	začátek	k1gInSc4	začátek
obléhání	obléhání	k1gNnSc4	obléhání
přístavu	přístav	k1gInSc2	přístav
Calais	Calais	k1gNnSc4	Calais
1347	[number]	k4	1347
-	-	kIx~	-
po	po	k7c6	po
počátečních	počáteční	k2eAgInPc6d1	počáteční
bojích	boj	k1gInPc6	boj
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
finančně	finančně	k6eAd1	finančně
vyčerpaly	vyčerpat	k5eAaPmAgFnP	vyčerpat
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
osmileté	osmiletý	k2eAgNnSc4d1	osmileté
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
po	po	k7c6	po
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
obléhání	obléhání	k1gNnSc6	obléhání
se	se	k3xPyFc4	se
francouzský	francouzský	k2eAgInSc1d1	francouzský
přístav	přístav	k1gInSc1	přístav
Calais	Calais	k1gNnSc2	Calais
vzdal	vzdát	k5eAaPmAgInS	vzdát
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
Eduardovi	Eduard	k1gMnSc3	Eduard
III	III	kA	III
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1350	[number]	k4	1350
-	-	kIx~	-
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Filipa	Filip	k1gMnSc2	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
usedl	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
Jan	Jana	k1gFnPc2	Jana
II	II	kA	II
<g/>
.	.	kIx.	.
1355	[number]	k4	1355
-	-	kIx~	-
konec	konec	k1gInSc1	konec
příměří	příměří	k1gNnSc2	příměří
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
uzavřeného	uzavřený	k2eAgNnSc2d1	uzavřené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1347	[number]	k4	1347
1356	[number]	k4	1356
-	-	kIx~	-
princ	princ	k1gMnSc1	princ
Eduard	Eduard	k1gMnSc1	Eduard
zajal	zajmout	k5eAaPmAgMnS	zajmout
u	u	k7c2	u
Poitiers	Poitiersa	k1gFnPc2	Poitiersa
Jana	Jan	k1gMnSc2	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
1358	[number]	k4	1358
-	-	kIx~	-
povstání	povstání	k1gNnSc1	povstání
měšťanů	měšťan	k1gMnPc2	měšťan
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
selské	selské	k1gNnSc4	selské
povstání	povstání	k1gNnSc2	povstání
II	II	kA	II
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
(	(	kIx(	(
<g/>
1360	[number]	k4	1360
<g/>
-	-	kIx~	-
<g/>
1415	[number]	k4	1415
<g/>
)	)	kIx)	)
1360	[number]	k4	1360
-	-	kIx~	-
Francie	Francie	k1gFnSc1	Francie
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
v	v	k7c4	v
Brétigny	Brétigna	k1gFnPc4	Brétigna
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dovoloval	dovolovat	k5eAaImAgInS	dovolovat
Angličanům	Angličan	k1gMnPc3	Angličan
ponechat	ponechat	k5eAaPmF	ponechat
si	se	k3xPyFc3	se
všechna	všechen	k3xTgNnPc4	všechen
získaná	získaný	k2eAgNnPc4d1	získané
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
katastrofy	katastrofa	k1gFnSc2	katastrofa
1364	[number]	k4	1364
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
anglickém	anglický	k2eAgNnSc6d1	anglické
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
znovu	znovu	k6eAd1	znovu
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
válku	válka	k1gFnSc4	válka
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
vyhnal	vyhnat	k5eAaPmAgInS	vyhnat
Angličany	Angličan	k1gMnPc4	Angličan
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
oblastí	oblast	k1gFnPc2	oblast
Francie	Francie	k1gFnSc2	Francie
1377	[number]	k4	1377
-	-	kIx~	-
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
zemřel	zemřít	k5eAaPmAgMnS	zemřít
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
zanechal	zanechat	k5eAaPmAgInS	zanechat
svému	svůj	k3xOyFgMnSc3	svůj
vnukovi	vnuk	k1gMnSc3	vnuk
Richardovi	Richard	k1gMnSc3	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
1380	[number]	k4	1380
-	-	kIx~	-
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
moci	moc	k1gFnSc6	moc
zůstaly	zůstat	k5eAaPmAgInP	zůstat
už	už	k6eAd1	už
jen	jen	k9	jen
Bayonne	Bayonn	k1gInSc5	Bayonn
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Calais	Calais	k1gNnPc1	Calais
<g/>
,	,	kIx,	,
Cherbourg	Cherbourg	k1gInSc1	Cherbourg
<g/>
,	,	kIx,	,
Brest	Brest	k1gInSc1	Brest
a	a	k8xC	a
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
<g/>
,	,	kIx,	,
vlády	vláda	k1gFnPc1	vláda
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
Karla	Karel	k1gMnSc4	Karel
V.	V.	kA	V.
ujal	ujmout	k5eAaPmAgMnS	ujmout
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
1381	[number]	k4	1381
-	-	kIx~	-
lidové	lidový	k2eAgNnSc1d1	lidové
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
Watem	Wat	k1gMnSc7	Wat
Tylerem	Tyler	k1gMnSc7	Tyler
1388	[number]	k4	1388
-	-	kIx~	-
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
strýců	strýc	k1gMnPc2	strýc
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
vybral	vybrat	k5eAaPmAgMnS	vybrat
si	se	k3xPyFc3	se
poradce	poradce	k1gMnSc1	poradce
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
nižší	nízký	k2eAgFnSc2d2	nižší
šlechty	šlechta	k1gFnSc2	šlechta
1392	[number]	k4	1392
-	-	kIx~	-
u	u	k7c2	u
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
propukla	propuknout	k5eAaPmAgFnS	propuknout
duševní	duševní	k2eAgFnSc1d1	duševní
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
strýcové	strýc	k1gMnPc1	strýc
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
chopili	chopit	k5eAaPmAgMnP	chopit
vlády	vláda	k1gFnSc2	vláda
1413	[number]	k4	1413
-	-	kIx~	-
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
korunován	korunovat	k5eAaBmNgMnS	korunovat
králem	král	k1gMnSc7	král
Anglie	Anglie	k1gFnSc2	Anglie
III	III	kA	III
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
(	(	kIx(	(
<g/>
1415	[number]	k4	1415
<g/>
-	-	kIx~	-
<g/>
1453	[number]	k4	1453
<g/>
)	)	kIx)	)
1415	[number]	k4	1415
-	-	kIx~	-
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
<g />
.	.	kIx.	.
</s>
<s>
obnovil	obnovit	k5eAaPmAgInS	obnovit
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
vylodil	vylodit	k5eAaPmAgMnS	vylodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
1420	[number]	k4	1420
-	-	kIx~	-
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
v	v	k7c4	v
Troyes	Troyes	k1gInSc4	Troyes
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
královnou	královna	k1gFnSc7	královna
Izabelou	Izabela	k1gFnSc7	Izabela
a	a	k8xC	a
burgundským	burgundský	k2eAgMnSc7d1	burgundský
vévodou	vévoda	k1gMnSc7	vévoda
Filipem	Filip	k1gMnSc7	Filip
Dobrým	dobrý	k2eAgMnSc7d1	dobrý
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uznávala	uznávat	k5eAaImAgFnS	uznávat
Jindřicha	Jindřich	k1gMnSc4	Jindřich
V.	V.	kA	V.
za	za	k7c4	za
dědice	dědic	k1gMnPc4	dědic
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koruny	koruna	k1gFnSc2	koruna
1422	[number]	k4	1422
-	-	kIx~	-
smrt	smrt	k1gFnSc4	smrt
Jindřicha	Jindřich	k1gMnSc2	Jindřich
V.	V.	kA	V.
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
<g />
.	.	kIx.	.
</s>
<s>
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jindřichova	Jindřichův	k2eAgMnSc2d1	Jindřichův
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Bedfordu	Bedford	k1gInSc2	Bedford
1429	[number]	k4	1429
-	-	kIx~	-
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
Orléans	Orléans	k1gInSc4	Orléans
a	a	k8xC	a
porazila	porazit	k5eAaPmAgFnS	porazit
Angličany	Angličan	k1gMnPc4	Angličan
u	u	k7c2	u
města	město	k1gNnSc2	město
Pataye	Patay	k1gFnSc2	Patay
1431	[number]	k4	1431
-	-	kIx~	-
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
čarodějnici	čarodějnice	k1gFnSc4	čarodějnice
a	a	k8xC	a
upálena	upálit	k5eAaPmNgFnS	upálit
v	v	k7c6	v
Rouenu	Rouen	k1gInSc6	Rouen
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
začala	začít	k5eAaPmAgFnS	začít
ztrácet	ztrácet	k5eAaImF	ztrácet
svoje	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
1449	[number]	k4	1449
-	-	kIx~	-
Normandie	Normandie	k1gFnSc1	Normandie
znovu	znovu	k6eAd1	znovu
dobyta	dobyt	k2eAgFnSc1d1	dobyta
Francouzi	Francouz	k1gMnSc3	Francouz
1453	[number]	k4	1453
-	-	kIx~	-
konec	konec	k1gInSc1	konec
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Angličané	Angličan	k1gMnPc1	Angličan
ztratili	ztratit	k5eAaPmAgMnP	ztratit
všechna	všechen	k3xTgNnPc4	všechen
území	území	k1gNnSc4	území
kromě	kromě	k7c2	kromě
přístavu	přístav	k1gInSc2	přístav
Calais	Calais	k1gNnSc2	Calais
ALLMAND	ALLMAND	kA	ALLMAND
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
T.	T.	kA	T.
The	The	k1gMnSc1	The
hundred	hundred	k1gMnSc1	hundred
years	years	k6eAd1	years
war	war	k?	war
:	:	kIx,	:
England	England	k1gInSc1	England
and	and	k?	and
France	Franc	k1gMnSc2	Franc
at	at	k?	at
war	war	k?	war
<g/>
,	,	kIx,	,
c.	c.	k?	c.
1300	[number]	k4	1300
<g/>
-	-	kIx~	-
<g/>
c.	c.	k?	c.
1450	[number]	k4	1450
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
207	[number]	k4	207
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
521264995	[number]	k4	521264995
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
FROISSART	FROISSART	kA	FROISSART
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
229	[number]	k4	229
s.	s.	k?	s.
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
:	:	kIx,	:
(	(	kIx(	(
<g/>
1356	[number]	k4	1356
<g/>
-	-	kIx~	-
<g/>
1450	[number]	k4	1450
<g/>
)	)	kIx)	)
:	:	kIx,	:
rytířské	rytířský	k2eAgFnPc1d1	rytířská
bitvy	bitva	k1gFnPc1	bitva
a	a	k8xC	a
osudy	osud	k1gInPc1	osud
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
346	[number]	k4	346
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1499	[number]	k4	1499
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Rytířská	rytířský	k2eAgFnSc1d1	rytířská
krev	krev	k1gFnSc1	krev
:	:	kIx,	:
(	(	kIx(	(
<g/>
1208	[number]	k4	1208
<g/>
-	-	kIx~	-
<g/>
1346	[number]	k4	1346
<g/>
)	)	kIx)	)
:	:	kIx,	:
rytířské	rytířský	k2eAgFnPc1d1	rytířská
bitvy	bitva	k1gFnPc1	bitva
a	a	k8xC	a
osudy	osud	k1gInPc1	osud
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
338	[number]	k4	338
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1401	[number]	k4	1401
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
NICOLLE	NICOLLE	kA	NICOLLE
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Kreščak	Kreščak	k6eAd1	Kreščak
1346	[number]	k4	1346
<g/>
.	.	kIx.	.
</s>
<s>
Triumf	triumf	k1gInSc1	triumf
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
luku	luk	k1gInSc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SEWARD	SEWARD	kA	SEWARD
<g/>
,	,	kIx,	,
Desmond	Desmond	k1gInSc1	Desmond
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Hundred	Hundred	k1gInSc1	Hundred
Years	Years	k1gInSc1	Years
War	War	k1gFnSc1	War
<g/>
:	:	kIx,	:
<g/>
The	The	k1gFnSc1	The
English	Englisha	k1gFnPc2	Englisha
in	in	k?	in
France	Franc	k1gMnSc2	Franc
1337	[number]	k4	1337
<g/>
-	-	kIx~	-
<g/>
1453	[number]	k4	1453
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
:	:	kIx,	:
Penguin	Penguin	k2eAgInSc1d1	Penguin
Books	Books	k1gInSc1	Books
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
304	[number]	k4	304
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
140283617	[number]	k4	140283617
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SUMPTION	SUMPTION	kA	SUMPTION
<g/>
,	,	kIx,	,
Jonathan	Jonathan	k1gMnSc1	Jonathan
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Hundred	Hundred	k1gInSc1	Hundred
Years	Yearsa	k1gFnPc2	Yearsa
War	War	k1gMnPc2	War
I.	I.	kA	I.
Trial	trial	k1gInSc4	trial
by	by	kYmCp3nS	by
Battle	Battle	k1gFnSc1	Battle
<g/>
.	.	kIx.	.
</s>
<s>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
672	[number]	k4	672
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8122	[number]	k4	8122
<g/>
-	-	kIx~	-
<g/>
1655	[number]	k4	1655
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SUMPTION	SUMPTION	kA	SUMPTION
<g/>
,	,	kIx,	,
Jonathan	Jonathan	k1gMnSc1	Jonathan
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Hundred	Hundred	k1gInSc1	Hundred
Years	Yearsa	k1gFnPc2	Yearsa
War	War	k1gMnPc2	War
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Trial	trial	k1gInSc1	trial
by	by	kYmCp3nS	by
Fire	Fire	k1gInSc4	Fire
<g/>
.	.	kIx.	.
</s>
<s>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
696	[number]	k4	696
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8122	[number]	k4	8122
<g/>
-	-	kIx~	-
<g/>
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SUMPTION	SUMPTION	kA	SUMPTION
<g/>
,	,	kIx,	,
Jonathan	Jonathan	k1gMnSc1	Jonathan
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Hundred	Hundred	k1gInSc1	Hundred
Years	Yearsa	k1gFnPc2	Yearsa
War	War	k1gMnPc2	War
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Divided	Divided	k1gMnSc1	Divided
Houses	Houses	k1gMnSc1	Houses
<g/>
.	.	kIx.	.
</s>
<s>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
1024	[number]	k4	1024
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8122	[number]	k4	8122
<g/>
-	-	kIx~	-
<g/>
2177	[number]	k4	2177
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
URBAN	Urban	k1gMnSc1	Urban
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Kresčak	Kresčak	k1gInSc1	Kresčak
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1346	[number]	k4	1346
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
86	[number]	k4	86
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
324	[number]	k4	324
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Anglie	Anglie	k1gFnSc2	Anglie
Anglie	Anglie	k1gFnSc2	Anglie
Dějiny	dějiny	k1gFnPc1	dějiny
Francie	Francie	k1gFnSc2	Francie
Francie	Francie	k1gFnSc2	Francie
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stoletá	stoletý	k2eAgFnSc1d1	stoletá
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
