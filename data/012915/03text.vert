<p>
<s>
Hygiena	hygiena	k1gFnSc1	hygiena
je	být	k5eAaImIp3nS	být
dodržování	dodržování	k1gNnSc4	dodržování
zásad	zásada	k1gFnPc2	zásada
pro	pro	k7c4	pro
uchování	uchování	k1gNnSc4	uchování
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
prevence	prevence	k1gFnSc2	prevence
infekčních	infekční	k2eAgNnPc2d1	infekční
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
terminologii	terminologie	k1gFnSc6	terminologie
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
obor	obor	k1gInSc4	obor
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
všemi	všecek	k3xTgInPc7	všecek
faktory	faktor	k1gInPc7	faktor
ovlivňujícími	ovlivňující	k2eAgInPc7d1	ovlivňující
tělesné	tělesný	k2eAgFnSc2d1	tělesná
zdraví	zdraví	k1gNnSc3	zdraví
i	i	k8xC	i
duševní	duševní	k2eAgFnSc4d1	duševní
pohodu	pohoda	k1gFnSc4	pohoda
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
současné	současný	k2eAgNnSc1d1	současné
pojetí	pojetí	k1gNnSc1	pojetí
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
udržování	udržování	k1gNnSc4	udržování
čistoty	čistota	k1gFnSc2	čistota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
osobního	osobní	k2eAgInSc2d1	osobní
se	se	k3xPyFc4	se
hygiena	hygiena	k1gFnSc1	hygiena
zabývá	zabývat	k5eAaImIp3nS	zabývat
kvalitou	kvalita	k1gFnSc7	kvalita
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
stravování	stravování	k1gNnSc6	stravování
<g/>
,	,	kIx,	,
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
námahou	námaha	k1gFnSc7	námaha
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
spánkem	spánek	k1gInSc7	spánek
<g/>
,	,	kIx,	,
čistotou	čistota	k1gFnSc7	čistota
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
užíváním	užívání	k1gNnSc7	užívání
tabáku	tabák	k1gInSc2	tabák
<g/>
,	,	kIx,	,
narkotik	narkotik	k1gMnSc1	narkotik
atd.	atd.	kA	atd.
a	a	k8xC	a
duševním	duševní	k2eAgNnSc7d1	duševní
zdravím	zdraví	k1gNnSc7	zdraví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
veřejného	veřejný	k2eAgNnSc2d1	veřejné
se	se	k3xPyFc4	se
okruh	okruh	k1gInSc1	okruh
zájmů	zájem	k1gInPc2	zájem
oboru	obor	k1gInSc2	obor
týká	týkat	k5eAaImIp3nS	týkat
klimatických	klimatický	k2eAgFnPc2d1	klimatická
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
charakteru	charakter	k1gInSc2	charakter
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc2	uspořádání
obydlí	obydlí	k1gNnSc2	obydlí
<g/>
,	,	kIx,	,
topení	topení	k1gNnSc1	topení
<g/>
,	,	kIx,	,
větrání	větrání	k1gNnSc1	větrání
<g/>
,	,	kIx,	,
odstraňování	odstraňování	k1gNnSc1	odstraňování
odpadů	odpad	k1gInPc2	odpad
<g/>
,	,	kIx,	,
lékařských	lékařský	k2eAgFnPc2d1	lékařská
znalostí	znalost	k1gFnPc2	znalost
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
a	a	k8xC	a
prevenci	prevence	k1gFnSc6	prevence
chorob	choroba	k1gFnPc2	choroba
až	až	k9	až
po	po	k7c4	po
pohřbívání	pohřbívání	k1gNnSc4	pohřbívání
zemřelých	zemřelý	k1gMnPc2	zemřelý
(	(	kIx(	(
<g/>
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
dalšího	další	k2eAgInSc2d1	další
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
epidemiologie	epidemiologie	k1gFnSc1	epidemiologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
vnější	vnější	k2eAgInPc4d1	vnější
znaky	znak	k1gInPc4	znak
dobré	dobrý	k2eAgFnSc2d1	dobrá
hygieny	hygiena	k1gFnSc2	hygiena
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
pokládána	pokládán	k2eAgFnSc1d1	pokládána
absence	absence	k1gFnSc1	absence
viditelné	viditelný	k2eAgFnSc2d1	viditelná
špíny	špína	k1gFnSc2	špína
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
skvrn	skvrna	k1gFnPc2	skvrna
na	na	k7c6	na
šatech	šat	k1gInPc6	šat
<g/>
)	)	kIx)	)
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
rozvoje	rozvoj	k1gInSc2	rozvoj
mikrobiální	mikrobiální	k2eAgFnSc1d1	mikrobiální
teorie	teorie	k1gFnSc1	teorie
nemocí	nemoc	k1gFnPc2	nemoc
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
hygiena	hygiena	k1gFnSc1	hygiena
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
činnost	činnost	k1gFnSc4	činnost
nebo	nebo	k8xC	nebo
opatření	opatření	k1gNnSc4	opatření
vedoucí	vedoucí	k1gFnSc1	vedoucí
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
nebo	nebo	k8xC	nebo
částečnému	částečný	k2eAgNnSc3d1	částečné
omezení	omezení	k1gNnSc3	omezení
škodlivého	škodlivý	k2eAgNnSc2d1	škodlivé
působení	působení	k1gNnSc2	působení
mikrobů	mikrob	k1gInPc2	mikrob
(	(	kIx(	(
<g/>
baktérií	baktérie	k1gFnPc2	baktérie
<g/>
,	,	kIx,	,
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
virů	vir	k1gInPc2	vir
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
hygiena	hygiena	k1gFnSc1	hygiena
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
kráse	krása	k1gFnSc6	krása
<g/>
,	,	kIx,	,
pohodlí	pohodlí	k1gNnSc4	pohodlí
i	i	k8xC	i
sociálnímu	sociální	k2eAgInSc3d1	sociální
styku	styk	k1gInSc3	styk
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
podporuje	podporovat	k5eAaImIp3nS	podporovat
prevenci	prevence	k1gFnSc4	prevence
a	a	k8xC	a
izolování	izolování	k1gNnSc4	izolování
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tedy	tedy	k9	tedy
jste	být	k5eAaImIp2nP	být
<g/>
-li	i	k?	-li
zdravý	zdravý	k2eAgInSc1d1	zdravý
<g/>
,	,	kIx,	,
dobrá	dobrý	k2eAgFnSc1d1	dobrá
hygiena	hygiena	k1gFnSc1	hygiena
vám	vy	k3xPp2nPc3	vy
pomůže	pomoct	k5eAaPmIp3nS	pomoct
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
jste	být	k5eAaImIp2nP	být
<g/>
-li	i	k?	-li
nemocen	nemocen	k2eAgInSc1d1	nemocen
<g/>
,	,	kIx,	,
dobrá	dobrý	k2eAgFnSc1d1	dobrá
hygiena	hygiena	k1gFnSc1	hygiena
může	moct	k5eAaImIp3nS	moct
redukovat	redukovat	k5eAaBmF	redukovat
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
nákazy	nákaza	k1gFnSc2	nákaza
od	od	k7c2	od
vás	vy	k3xPp2nPc2	vy
na	na	k7c4	na
druhé	druhý	k4xOgNnSc1	druhý
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
<g />
.	.	kIx.	.
</s>
<s>
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přehnaná	přehnaný	k2eAgFnSc1d1	přehnaná
důslednost	důslednost	k1gFnSc1	důslednost
v	v	k7c6	v
hygienických	hygienický	k2eAgInPc6d1	hygienický
návycích	návyk	k1gInPc6	návyk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
jistých	jistý	k2eAgInPc6d1	jistý
ohledech	ohled	k1gInPc6	ohled
pro	pro	k7c4	pro
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
–	–	k?	–
kvůli	kvůli	k7c3	kvůli
redukci	redukce	k1gFnSc3	redukce
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
mikrobiálního	mikrobiální	k2eAgNnSc2d1	mikrobiální
prostředí	prostředí	k1gNnSc2	prostředí
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
přirozené	přirozený	k2eAgFnSc2d1	přirozená
odolnosti	odolnost	k1gFnSc2	odolnost
a	a	k8xC	a
obranyschopnosti	obranyschopnost	k1gFnSc2	obranyschopnost
organismu	organismus	k1gInSc2	organismus
a	a	k8xC	a
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
např.	např.	kA	např.
k	k	k7c3	k
vyššímu	vysoký	k2eAgInSc3d2	vyšší
výskytu	výskyt	k1gInSc2	výskyt
alergií	alergie	k1gFnPc2	alergie
<g/>
.	.	kIx.	.
</s>
<s>
Mytí	mytí	k1gNnSc1	mytí
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgInSc4d3	nejčastější
příklad	příklad	k1gInSc4	příklad
hygienického	hygienický	k2eAgNnSc2d1	hygienické
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
mýdlem	mýdlo	k1gNnSc7	mýdlo
nebo	nebo	k8xC	nebo
saponátem	saponát	k1gInSc7	saponát
pomáhajícím	pomáhající	k2eAgInSc7d1	pomáhající
odstranit	odstranit	k5eAaPmF	odstranit
mastnotu	mastnota	k1gFnSc4	mastnota
a	a	k8xC	a
narušit	narušit	k5eAaPmF	narušit
nečistotu	nečistota	k1gFnSc4	nečistota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
umyta	umyt	k2eAgFnSc1d1	umyta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hygienické	hygienický	k2eAgFnSc2d1	hygienická
zásady	zásada	k1gFnSc2	zásada
–	–	k?	–
časté	častý	k2eAgNnSc1d1	časté
mytí	mytí	k1gNnSc1	mytí
rukou	ruka	k1gFnPc2	ruka
nebo	nebo	k8xC	nebo
použití	použití	k1gNnSc1	použití
převařené	převařený	k2eAgFnPc1d1	převařená
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
sterilizované	sterilizovaný	k2eAgFnSc2d1	sterilizovaná
<g/>
)	)	kIx)	)
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
–	–	k?	–
měly	mít	k5eAaImAgFnP	mít
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
redukci	redukce	k1gFnSc4	redukce
šíření	šíření	k1gNnSc2	šíření
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hygiena	hygiena	k1gFnSc1	hygiena
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
==	==	k?	==
</s>
</p>
<p>
<s>
Hygiena	hygiena	k1gFnSc1	hygiena
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
podle	podle	k7c2	podle
bohyně	bohyně	k1gFnSc2	bohyně
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
Hygieia	Hygieia	k1gFnSc1	Hygieia
<g/>
,	,	kIx,	,
uctívaná	uctívaný	k2eAgFnSc1d1	uctívaná
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Asklepiem	Asklepium	k1gNnSc7	Asklepium
v	v	k7c6	v
Epidauru	Epidaur	k1gInSc6	Epidaur
na	na	k7c6	na
Peloponésu	Peloponés	k1gInSc6	Peloponés
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znázorňována	znázorňovat	k5eAaImNgFnS	znázorňovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
sličné	sličný	k2eAgFnSc2d1	sličná
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
symbolem	symbol	k1gInSc7	symbol
je	být	k5eAaImIp3nS	být
had	had	k1gMnSc1	had
pijící	pijící	k2eAgFnSc4d1	pijící
krev	krev	k1gFnSc4	krev
z	z	k7c2	z
misky	miska	k1gFnSc2	miska
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
bohyně	bohyně	k1gFnSc1	bohyně
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hygiena	hygiena	k1gFnSc1	hygiena
obecná	obecná	k1gFnSc1	obecná
==	==	k?	==
</s>
</p>
<p>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
zákonitosti	zákonitost	k1gFnPc4	zákonitost
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hygiena	hygiena	k1gFnSc1	hygiena
komunální	komunální	k2eAgFnSc1d1	komunální
==	==	k?	==
</s>
</p>
<p>
<s>
Sleduje	sledovat	k5eAaImIp3nS	sledovat
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hygiena	hygiena	k1gFnSc1	hygiena
práce	práce	k1gFnSc2	práce
==	==	k?	==
</s>
</p>
<p>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
vliv	vliv	k1gInSc4	vliv
pracovních	pracovní	k2eAgFnPc2d1	pracovní
podmínek	podmínka	k1gFnPc2	podmínka
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hygiena	hygiena	k1gFnSc1	hygiena
výživy	výživa	k1gFnSc2	výživa
==	==	k?	==
</s>
</p>
<p>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
působení	působení	k1gNnSc4	působení
výživy	výživa	k1gFnSc2	výživa
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
racionalizaci	racionalizace	k1gFnSc4	racionalizace
výživy	výživa	k1gFnSc2	výživa
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
nezávadnost	nezávadnost	k1gFnSc1	nezávadnost
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hygiena	hygiena	k1gFnSc1	hygiena
duševní	duševní	k2eAgFnSc1d1	duševní
(	(	kIx(	(
<g/>
mentální	mentální	k2eAgFnSc1d1	mentální
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
systém	systém	k1gInSc1	systém
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
návodů	návod	k1gInPc2	návod
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
<g/>
,	,	kIx,	,
prohloubení	prohloubení	k1gNnSc3	prohloubení
nebo	nebo	k8xC	nebo
obnovení	obnovení	k1gNnSc4	obnovení
duševního	duševní	k2eAgNnSc2d1	duševní
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hygiena	hygiena	k1gFnSc1	hygiena
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
disciplín	disciplína	k1gFnPc2	disciplína
(	(	kIx(	(
<g/>
hygiena	hygiena	k1gFnSc1	hygiena
osobní	osobní	k2eAgFnSc1d1	osobní
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc1d1	sociální
<g/>
,	,	kIx,	,
vojenská	vojenský	k2eAgFnSc1d1	vojenská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hygienická	hygienický	k2eAgFnSc1d1	hygienická
stanice	stanice	k1gFnSc1	stanice
==	==	k?	==
</s>
</p>
<p>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
vlivem	vlivem	k7c2	vlivem
životních	životní	k2eAgFnPc2d1	životní
a	a	k8xC	a
pracovních	pracovní	k2eAgFnPc2d1	pracovní
podmínek	podmínka	k1gFnPc2	podmínka
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
lidských	lidský	k2eAgNnPc2d1	lidské
společenství	společenství	k1gNnPc2	společenství
<g/>
,	,	kIx,	,
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
účinky	účinek	k1gInPc4	účinek
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
složek	složka	k1gFnPc2	složka
životního	životní	k2eAgNnSc2d1	životní
i	i	k8xC	i
pracovního	pracovní	k2eAgNnSc2d1	pracovní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Vypracovává	vypracovávat	k5eAaImIp3nS	vypracovávat
vědecky	vědecky	k6eAd1	vědecky
podložené	podložený	k2eAgFnPc4d1	podložená
zásady	zásada	k1gFnPc4	zásada
pro	pro	k7c4	pro
život	život	k1gInSc4	život
a	a	k8xC	a
práci	práce	k1gFnSc4	práce
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
omezují	omezovat	k5eAaImIp3nP	omezovat
rizika	riziko	k1gNnPc1	riziko
poškození	poškození	k1gNnSc3	poškození
zdraví	zdravit	k5eAaImIp3nP	zdravit
faktory	faktor	k1gInPc1	faktor
zevního	zevní	k2eAgNnSc2d1	zevní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
šíření	šíření	k1gNnSc4	šíření
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hygiena	hygiena	k1gFnSc1	hygiena
veterinární	veterinární	k2eAgFnSc1d1	veterinární
==	==	k?	==
</s>
</p>
<p>
<s>
Veterinární	veterinární	k2eAgFnSc1d1	veterinární
hygiena	hygiena	k1gFnSc1	hygiena
je	být	k5eAaImIp3nS	být
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
především	především	k9	především
na	na	k7c4	na
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
a	a	k8xC	a
hygienickou	hygienický	k2eAgFnSc4d1	hygienická
nezávadnost	nezávadnost	k1gFnSc4	nezávadnost
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
surovin	surovina	k1gFnPc2	surovina
živočišného	živočišný	k2eAgInSc2d1	živočišný
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
doplňující	doplňující	k2eAgFnSc7d1	doplňující
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgInSc2	tento
oboru	obor	k1gInSc2	obor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
též	též	k9	též
veterinární	veterinární	k2eAgFnSc1d1	veterinární
ekologie	ekologie	k1gFnSc1	ekologie
a	a	k8xC	a
infekční	infekční	k2eAgFnPc1d1	infekční
choroby	choroba	k1gFnPc1	choroba
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
epizootologie	epizootologie	k1gFnPc4	epizootologie
vztahující	vztahující	k2eAgFnPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
potravinám	potravina	k1gFnPc3	potravina
a	a	k8xC	a
surovinám	surovina	k1gFnPc3	surovina
živočišného	živočišný	k2eAgInSc2d1	živočišný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Bencko	Bencko	k1gNnSc1	Bencko
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
:	:	kIx,	:
Primary	Primara	k1gFnSc2	Primara
prevention	prevention	k1gInSc1	prevention
and	and	k?	and
quality	qualita	k1gFnSc2	qualita
of	of	k?	of
life	lif	k1gFnSc2	lif
<g/>
,	,	kIx,	,
In	In	k1gFnSc2	In
<g/>
:	:	kIx,	:
Der	drát	k5eAaImRp2nS	drát
Mensch	Mensch	k1gInSc1	Mensch
als	als	k?	als
Subject	Subject	k1gInSc1	Subject
und	und	k?	und
Object	Object	k1gInSc1	Object
der	drát	k5eAaImRp2nS	drát
Medizin	Medizin	k1gInSc4	Medizin
<g/>
.	.	kIx.	.
</s>
<s>
Veranstaltung	Veranstaltung	k1gInSc1	Veranstaltung
des	des	k1gNnSc2	des
Rektors	Rektorsa	k1gFnPc2	Rektorsa
der	drát	k5eAaImRp2nS	drát
Universität	Universitätum	k1gNnPc2	Universitätum
Heidelberg	Heidelberg	k1gInSc1	Heidelberg
aus	aus	k?	aus
Anlass	Anlass	k1gInSc1	Anlass
des	des	k1gNnSc1	des
650	[number]	k4	650
<g/>
-jährigen	ährigen	k1gInSc1	-jährigen
Bestehens	Bestehens	k1gInSc4	Bestehens
der	drát	k5eAaImRp2nS	drát
KarlsUniversität	KarlsUniversität	k1gMnSc1	KarlsUniversität
Prag-Heidelberg	Prag-Heidelberg	k1gMnSc1	Prag-Heidelberg
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
-	-	kIx~	-
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Mai	Mai	k?	Mai
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Ticháček	ticháček	k1gMnSc1	ticháček
<g/>
,	,	kIx,	,
B.	B.	kA	B.
<g/>
:	:	kIx,	:
Základy	základ	k1gInPc1	základ
epidemiologie	epidemiologie	k1gFnSc2	epidemiologie
<g/>
.	.	kIx.	.
</s>
<s>
Galen	Galen	k1gInSc1	Galen
<g/>
,	,	kIx,	,
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
s.	s.	k?	s.
230	[number]	k4	230
</s>
</p>
<p>
<s>
Slonim	Slonim	k?	Slonim
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
,	,	kIx,	,
Švandová	Švandová	k1gFnSc1	Švandová
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
,	,	kIx,	,
Strnad	Strnad	k1gMnSc1	Strnad
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Č.	Č.	kA	Č.
<g/>
,	,	kIx,	,
<g/>
:	:	kIx,	:
A	a	k8xC	a
history	histor	k1gInPc4	histor
of	of	k?	of
poliomyelitis	poliomyelitis	k1gFnSc1	poliomyelitis
in	in	k?	in
the	the	k?	the
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
<g/>
.	.	kIx.	.
</s>
<s>
Centr	centr	k1gInSc1	centr
<g/>
.	.	kIx.	.
eur	euro	k1gNnPc2	euro
J.	J.	kA	J.
publ	publ	k1gInSc1	publ
<g/>
.	.	kIx.	.
</s>
<s>
Hlth	Hlth	k1gInSc1	Hlth
5	[number]	k4	5
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
53	[number]	k4	53
-	-	kIx~	-
56	[number]	k4	56
</s>
</p>
<p>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Bencko	Bencko	k1gNnSc1	Bencko
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
,	,	kIx,	,
Cikrt	Cikrt	k1gInSc1	Cikrt
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
B.	B.	kA	B.
<g/>
:	:	kIx,	:
Primární	primární	k2eAgFnSc1d1	primární
prevence	prevence	k1gFnSc1	prevence
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
zdraví	zdraví	k1gNnSc2	zdraví
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
rizika	riziko	k1gNnPc1	riziko
a	a	k8xC	a
možná	možný	k2eAgNnPc1d1	možné
východiska	východisko	k1gNnPc1	východisko
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Prakt	Prakt	k1gInSc1	Prakt
<g/>
.	.	kIx.	.
</s>
<s>
Lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
76	[number]	k4	76
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
s.	s.	k?	s.
480	[number]	k4	480
-	-	kIx~	-
484	[number]	k4	484
</s>
</p>
<p>
<s>
Bencko	Bencko	k1gNnSc1	Bencko
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
:	:	kIx,	:
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
prostředí	prostředí	k1gNnSc1	prostředí
a	a	k8xC	a
paragrafy	paragraf	k1gInPc1	paragraf
<g/>
,	,	kIx,	,
Prakt	Prakt	k1gInSc1	Prakt
<g/>
.	.	kIx.	.
</s>
<s>
Lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
74	[number]	k4	74
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
s.	s.	k?	s.
492	[number]	k4	492
-	-	kIx~	-
494	[number]	k4	494
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
umyvadlo	umyvadlo	k1gNnSc1	umyvadlo
</s>
</p>
<p>
<s>
pračka	pračka	k1gFnSc1	pračka
</s>
</p>
<p>
<s>
mýdlo	mýdlo	k1gNnSc1	mýdlo
</s>
</p>
<p>
<s>
sanitace	sanitace	k1gFnSc1	sanitace
</s>
</p>
<p>
<s>
toaleta	toaleta	k1gFnSc1	toaleta
</s>
</p>
<p>
<s>
duševní	duševní	k2eAgFnSc1d1	duševní
hygiena	hygiena	k1gFnSc1	hygiena
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hygiena	hygiena	k1gFnSc1	hygiena
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hygiena	hygiena	k1gFnSc1	hygiena
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Hygiena	hygiena	k1gFnSc1	hygiena
-	-	kIx~	-
časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
zdraví	zdraví	k1gNnSc2	zdraví
</s>
</p>
