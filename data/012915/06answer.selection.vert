<s>
Mytí	mytí	k1gNnSc1	mytí
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgInSc4d3	nejčastější
příklad	příklad	k1gInSc4	příklad
hygienického	hygienický	k2eAgNnSc2d1	hygienické
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
mýdlem	mýdlo	k1gNnSc7	mýdlo
nebo	nebo	k8xC	nebo
saponátem	saponát	k1gInSc7	saponát
pomáhajícím	pomáhající	k2eAgInSc7d1	pomáhající
odstranit	odstranit	k5eAaPmF	odstranit
mastnotu	mastnota	k1gFnSc4	mastnota
a	a	k8xC	a
narušit	narušit	k5eAaPmF	narušit
nečistotu	nečistota	k1gFnSc4	nečistota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
umyta	umyt	k2eAgFnSc1d1	umyta
<g/>
.	.	kIx.	.
</s>
