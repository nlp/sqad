<s>
Gordon	Gordon	k1gMnSc1	Gordon
Matthew	Matthew	k1gMnSc1	Matthew
Thomas	Thomas	k1gMnSc1	Thomas
Sumner	Sumner	k1gMnSc1	Sumner
<g/>
,	,	kIx,	,
CBE	CBE	kA	CBE
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
spíš	spíš	k9	spíš
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
Sting	Stinga	k1gFnPc2	Stinga
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgMnSc1d1	anglický
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
občasný	občasný	k2eAgMnSc1d1	občasný
herec	herec	k1gMnSc1	herec
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
upon	upon	k1gInSc1	upon
Tyne	Tyne	k1gFnSc4	Tyne
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
,	,	kIx,	,
skladatelem	skladatel	k1gMnSc7	skladatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
baskytaristou	baskytarista	k1gMnSc7	baskytarista
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
až	až	k9	až
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
The	The	k1gFnSc2	The
Police	police	k1gFnSc2	police
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
také	také	k9	také
postavu	postava	k1gFnSc4	postava
Feyda-Rauthy	Feyda-Rautha	k1gMnSc2	Feyda-Rautha
Harkonnena	Harkonnen	k1gMnSc2	Harkonnen
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Duna	duna	k1gFnSc1	duna
Davida	David	k1gMnSc2	David
Lynche	Lynch	k1gMnSc2	Lynch
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Broken	Broken	k1gInSc1	Broken
music	musice	k1gFnPc2	musice
vydal	vydat	k5eAaPmAgInS	vydat
Sting	Sting	k1gInSc1	Sting
vlastní	vlastní	k2eAgFnSc4d1	vlastní
autobiografii	autobiografie	k1gFnSc4	autobiografie
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
r.	r.	kA	r.
2004	[number]	k4	2004
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Dream	Dream	k1gInSc1	Dream
of	of	k?	of
the	the	k?	the
Blue	Blue	k1gInSc1	Blue
Turtles	Turtles	k1gInSc1	Turtles
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
...	...	k?	...
<g/>
Nothing	Nothing	k1gInSc1	Nothing
Like	Lik	k1gFnSc2	Lik
the	the	k?	the
Sun	Sun	kA	Sun
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
The	The	k1gFnPc2	The
Soul	Soul	k1gInSc1	Soul
Cages	Cages	k1gMnSc1	Cages
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Ten	ten	k3xDgInSc1	ten
Summoner	Summoner	k1gInSc1	Summoner
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tales	Talesa	k1gFnPc2	Talesa
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Mercury	Mercura	k1gFnSc2	Mercura
Falling	Falling	k1gInSc1	Falling
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Brand	Brand	k1gInSc1	Brand
<g />
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Day	Day	k1gFnSc1	Day
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Sacred	Sacred	k1gInSc1	Sacred
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Songs	Songs	k1gInSc1	Songs
from	from	k1gMnSc1	from
the	the	k?	the
Labyrinth	Labyrinth	k1gMnSc1	Labyrinth
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
If	If	k1gMnSc1	If
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Winter	Winter	k1gMnSc1	Winter
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Night	Nighta	k1gFnPc2	Nighta
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Symphonicities	Symphonicitiesa	k1gFnPc2	Symphonicitiesa
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Last	Last	k1gMnSc1	Last
Ship	Ship	k1gMnSc1	Ship
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
57	[number]	k4	57
<g/>
th	th	k?	th
&	&	k?	&
9	[number]	k4	9
<g/>
th	th	k?	th
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Bring	Bring	k1gInSc1	Bring
on	on	k3xPp3gInSc1	on
the	the	k?	the
Night	Night	k1gInSc1	Night
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Acoustic	Acoustice	k1gFnPc2	Acoustice
Live	Live	k1gNnPc2	Live
in	in	k?	in
Newcastle	Newcastle	k1gFnSc2	Newcastle
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
...	...	k?	...
<g/>
All	All	k1gFnSc1	All
This	This	k1gInSc1	This
Time	Time	k1gFnSc1	Time
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Journey	Journea	k1gFnSc2	Journea
and	and	k?	and
the	the	k?	the
Labyrinth	Labyrinth	k1gInSc1	Labyrinth
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Berlin	berlina	k1gFnPc2	berlina
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Fields	Fieldsa	k1gFnPc2	Fieldsa
of	of	k?	of
Gold	Gold	k1gMnSc1	Gold
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Sting	Sting	k1gMnSc1	Sting
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Best	Best	k1gMnSc1	Best
of	of	k?	of
Sting	Sting	k1gMnSc1	Sting
&	&	k?	&
The	The	k1gMnSc1	The
Police	police	k1gFnSc2	police
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
At	At	k1gMnSc1	At
the	the	k?	the
Movies	Movies	k1gMnSc1	Movies
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Brand	Brand	k1gMnSc1	Brand
New	New	k1gMnSc1	New
Day	Day	k1gMnSc1	Day
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Remixes	Remixes	k1gMnSc1	Remixes
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Songs	Songs	k1gInSc1	Songs
of	of	k?	of
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
25	[number]	k4	25
Years	Yearsa	k1gFnPc2	Yearsa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Best	Best	k1gInSc1	Best
of	of	k?	of
25	[number]	k4	25
Years	Years	k1gInSc1	Years
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Nada	Nad	k1gInSc2	Nad
como	como	k1gMnSc1	como
el	ela	k1gFnPc2	ela
sol	sol	k1gInSc1	sol
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Demolition	Demolition	k1gInSc1	Demolition
Man	mana	k1gFnPc2	mana
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Five	Fiv	k1gInSc2	Fiv
Live	Live	k1gInSc1	Live
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
at	at	k?	at
TFI	tfi	k0	tfi
Friday	Fridaa	k1gMnSc2	Fridaa
EP	EP	kA	EP
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Still	Still	k1gMnSc1	Still
Be	Be	k1gMnSc1	Be
Love	lov	k1gInSc5	lov
in	in	k?	in
the	the	k?	the
World	World	k1gInSc1	World
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
"	"	kIx"	"
<g/>
It	It	k1gFnSc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Probably	Probably	k1gMnSc7	Probably
Me	Me	k1gMnSc7	Me
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Ericem	Erice	k1gMnSc7	Erice
Claptonem	Clapton	k1gInSc7	Clapton
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
#	#	kIx~	#
<g/>
30	[number]	k4	30
UK	UK	kA	UK
1993	[number]	k4	1993
"	"	kIx"	"
<g/>
If	If	k1gFnSc1	If
I	i	k8xC	i
Ever	Ever	k1gInSc1	Ever
Lose	los	k1gInSc6	los
My	my	k3xPp1nPc1	my
Faith	Faith	k1gMnSc1	Faith
in	in	k?	in
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
14	[number]	k4	14
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
17	[number]	k4	17
US	US	kA	US
1993	[number]	k4	1993
"	"	kIx"	"
<g/>
Seven	Seven	k2eAgInSc1d1	Seven
Days	Days	k1gInSc1	Days
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
25	[number]	k4	25
UK	UK	kA	UK
1993	[number]	k4	1993
"	"	kIx"	"
<g/>
Fields	Fields	k1gInSc1	Fields
of	of	k?	of
Gold	Gold	k1gInSc1	Gold
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
16	[number]	k4	16
<g />
.	.	kIx.	.
</s>
<s>
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
23	[number]	k4	23
US	US	kA	US
Singl	singl	k1gInSc1	singl
nezařazený	zařazený	k2eNgInSc1d1	nezařazený
na	na	k7c4	na
desku	deska	k1gFnSc4	deska
<g/>
;	;	kIx,	;
soundtrack	soundtrack	k1gInSc4	soundtrack
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
jménem	jméno	k1gNnSc7	jméno
1993	[number]	k4	1993
"	"	kIx"	"
<g/>
Demolition	Demolition	k1gInSc1	Demolition
Man	mana	k1gFnPc2	mana
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
21	[number]	k4	21
UK	UK	kA	UK
Soundtrack	soundtrack	k1gInSc1	soundtrack
z	z	k7c2	z
filmu	film	k1gInSc2	film
Tři	tři	k4xCgMnPc1	tři
mušketýři	mušketýr	k1gMnPc1	mušketýr
1994	[number]	k4	1994
"	"	kIx"	"
<g/>
All	All	k1gFnPc2	All
for	forum	k1gNnPc2	forum
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Bryanem	Bryan	k1gMnSc7	Bryan
Adamsem	Adams	k1gMnSc7	Adams
a	a	k8xC	a
Rodem	rod	k1gInSc7	rod
Stewartem	Stewart	k1gMnSc7	Stewart
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
#	#	kIx~	#
<g/>
2	[number]	k4	2
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
1	[number]	k4	1
US	US	kA	US
Ten	ten	k3xDgInSc4	ten
Summoner	Summoner	k1gInSc4	Summoner
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tales	Tales	k1gInSc1	Tales
1994	[number]	k4	1994
"	"	kIx"	"
<g/>
Nothing	Nothing	k1gInSc1	Nothing
'	'	kIx"	'
<g/>
Bout	Bout	k1gMnSc1	Bout
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
32	[number]	k4	32
UK	UK	kA	UK
Fields	Fieldsa	k1gFnPc2	Fieldsa
of	of	k?	of
Gold	Gold	k1gMnSc1	Gold
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Sting	Sting	k1gMnSc1	Sting
1984-1994	[number]	k4	1984-1994
1994	[number]	k4	1994
"	"	kIx"	"
<g/>
When	When	k1gNnSc1	When
We	We	k1gFnSc3	We
Dance	Danka	k1gFnSc3	Danka
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
9	[number]	k4	9
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
38	[number]	k4	38
US	US	kA	US
1995	[number]	k4	1995
"	"	kIx"	"
<g/>
This	Thisa	k1gFnPc2	Thisa
Cowboy	Cowboa	k1gFnSc2	Cowboa
Song	song	k1gInSc1	song
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
15	[number]	k4	15
UK	UK	kA	UK
Soundtrack	soundtrack	k1gInSc1	soundtrack
z	z	k7c2	z
filmu	film	k1gInSc2	film
Ace	Ace	k1gFnSc2	Ace
Ventura	Ventura	kA	Ventura
<g/>
:	:	kIx,	:
Zvířecí	zvířecí	k2eAgMnSc1d1	zvířecí
detektiv	detektiv	k1gMnSc1	detektiv
1996	[number]	k4	1996
"	"	kIx"	"
<g/>
Spirits	Spirits	k1gInSc1	Spirits
in	in	k?	in
the	the	k?	the
Material	Material	k1gMnSc1	Material
World	World	k1gMnSc1	World
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Pato	pata	k1gFnSc5	pata
Banton	Banton	k1gInSc4	Banton
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Sting	Sting	k1gMnSc1	Sting
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
36	[number]	k4	36
UK	UK	kA	UK
Mercury	Mercur	k1gInPc4	Mercur
Falling	Falling	k1gInSc1	Falling
1996	[number]	k4	1996
"	"	kIx"	"
<g/>
Let	let	k1gInSc1	let
Your	Youra	k1gFnPc2	Youra
Soul	Soul	k1gInSc1	Soul
Be	Be	k1gMnSc1	Be
Your	Your	k1gMnSc1	Your
Pilot	pilot	k1gMnSc1	pilot
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
15	[number]	k4	15
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
86	[number]	k4	86
US	US	kA	US
1996	[number]	k4	1996
"	"	kIx"	"
<g/>
You	You	k1gMnSc1	You
Still	Still	k1gMnSc1	Still
Touch	Touch	k1gMnSc1	Touch
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
27	[number]	k4	27
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
60	[number]	k4	60
US	US	kA	US
1996	[number]	k4	1996
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
I	i	k9	i
Was	Was	k1gMnSc1	Was
Brought	Brought	k1gMnSc1	Brought
to	ten	k3xDgNnSc4	ten
My	my	k3xPp1nPc1	my
Senses	Senses	k1gInSc4	Senses
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
31	[number]	k4	31
UK	UK	kA	UK
1996	[number]	k4	1996
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
So	So	kA	So
Happy	Happ	k1gMnPc7	Happ
I	i	k8xC	i
Can	Can	k1gFnPc7	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Stop	stop	k1gInSc1	stop
Crying	Crying	k1gInSc1	Crying
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
94	[number]	k4	94
US	US	kA	US
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Best	Best	k1gMnSc1	Best
of	of	k?	of
Sting	Sting	k1gMnSc1	Sting
&	&	k?	&
The	The	k1gMnSc1	The
Police	police	k1gFnSc2	police
1997	[number]	k4	1997
"	"	kIx"	"
<g/>
Roxanne	Roxann	k1gInSc5	Roxann
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
97	[number]	k4	97
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
remix	remix	k1gInSc1	remix
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
s	s	k7c7	s
The	The	k1gFnSc7	The
Police	police	k1gFnSc2	police
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
17	[number]	k4	17
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
59	[number]	k4	59
US	US	kA	US
Brand	Branda	k1gFnPc2	Branda
New	New	k1gFnSc1	New
Day	Day	k1gFnSc1	Day
1999	[number]	k4	1999
"	"	kIx"	"
<g/>
Brand	Brand	k1gMnSc1	Brand
New	New	k1gMnSc1	New
Day	Day	k1gMnSc1	Day
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
13	[number]	k4	13
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
100	[number]	k4	100
US	US	kA	US
2000	[number]	k4	2000
"	"	kIx"	"
<g/>
Desert	desert	k1gInSc1	desert
Rose	Ros	k1gMnSc2	Ros
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Cheb	Cheb	k1gInSc1	Cheb
Mami	mami	k1gFnSc2	mami
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
15	[number]	k4	15
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
17	[number]	k4	17
US	US	kA	US
2000	[number]	k4	2000
"	"	kIx"	"
<g/>
After	After	k1gInSc1	After
the	the	k?	the
Rain	Rain	k1gInSc1	Rain
Has	hasit	k5eAaImRp2nS	hasit
Fallen	Fallen	k1gInSc1	Fallen
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
31	[number]	k4	31
UK	UK	kA	UK
Slicker	Slicker	k1gMnSc1	Slicker
Than	Than	k1gMnSc1	Than
Your	Your	k1gMnSc1	Your
Average	Average	k1gFnSc1	Average
(	(	kIx(	(
<g/>
Craig	Craig	k1gMnSc1	Craig
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
"	"	kIx"	"
<g/>
Rise	Ris	k1gInSc2	Ris
&	&	k?	&
Fall	Fall	k1gMnSc1	Fall
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Craig	Craig	k1gMnSc1	Craig
David	David	k1gMnSc1	David
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Sting	Sting	k1gMnSc1	Sting
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
2	[number]	k4	2
UK	UK	kA	UK
Sacred	Sacred	k1gMnSc1	Sacred
Love	lov	k1gInSc5	lov
2003	[number]	k4	2003
"	"	kIx"	"
<g/>
Send	Send	k1gMnSc1	Send
Your	Your	k1gMnSc1	Your
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
30	[number]	k4	30
UK	UK	kA	UK
2003	[number]	k4	2003
"	"	kIx"	"
<g/>
Whenever	Whenever	k1gMnSc1	Whenever
I	i	k8xC	i
Say	Say	k1gMnSc1	Say
Your	Your	k1gMnSc1	Your
Name	Name	k1gFnSc1	Name
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Mary	Mary	k1gFnSc1	Mary
J.	J.	kA	J.
Blidge	Blidge	k1gFnSc1	Blidge
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
60	[number]	k4	60
UK	UK	kA	UK
2004	[number]	k4	2004
"	"	kIx"	"
<g/>
Stolen	stolen	k2eAgMnSc1d1	stolen
Car	car	k1gMnSc1	car
(	(	kIx(	(
<g/>
Take	Take	k1gFnSc1	Take
Me	Me	k1gMnSc1	Me
Dancing	dancing	k1gInSc1	dancing
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
60	[number]	k4	60
UK	UK	kA	UK
Soundtrack	soundtrack	k1gInSc1	soundtrack
z	z	k7c2	z
filmu	film	k1gInSc2	film
Racing	Racing	k1gInSc4	Racing
Stripes	Stripes	k1gInSc1	Stripes
2005	[number]	k4	2005
"	"	kIx"	"
<g/>
Taking	Taking	k1gInSc1	Taking
the	the	k?	the
Inside	Insid	k1gInSc5	Insid
Rail	Rail	k1gInSc4	Rail
<g/>
"	"	kIx"	"
#	#	kIx~	#
<g/>
?	?	kIx.	?
</s>
<s>
US	US	kA	US
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
?	?	kIx.	?
</s>
<s>
UK	UK	kA	UK
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sting	Stinga	k1gFnPc2	Stinga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
internetové	internetový	k2eAgFnSc2d1	internetová
stránky	stránka	k1gFnSc2	stránka
</s>
