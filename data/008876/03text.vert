<p>
<s>
Onyx	onyx	k1gInSc1	onyx
je	být	k5eAaImIp3nS	být
neprůhledný	průhledný	k2eNgInSc1d1	neprůhledný
poloprůsvitný	poloprůsvitný	k2eAgInSc1d1	poloprůsvitný
černobílý	černobílý	k2eAgInSc1d1	černobílý
minerál	minerál	k1gInSc1	minerál
používaný	používaný	k2eAgInSc1d1	používaný
ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
<g/>
,	,	kIx,	,
v	v	k7c6	v
galanterii	galanterie	k1gFnSc6	galanterie
a	a	k8xC	a
v	v	k7c6	v
glyptice	glyptika	k1gFnSc6	glyptika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
varieta	varieta	k1gFnSc1	varieta
chalcedonu	chalcedon	k1gInSc2	chalcedon
<g/>
,	,	kIx,	,
dalšími	další	k2eAgFnPc7d1	další
jsou	být	k5eAaImIp3nP	být
bělavý	bělavý	k2eAgInSc4d1	bělavý
achát	achát	k1gInSc4	achát
nebo	nebo	k8xC	nebo
červený	červený	k2eAgInSc4d1	červený
karneol	karneol	k1gInSc4	karneol
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
vrstevnatosti	vrstevnatost	k1gFnSc3	vrstevnatost
onyx	onyx	k1gInSc4	onyx
dostal	dostat	k5eAaPmAgInS	dostat
řecký	řecký	k2eAgInSc1d1	řecký
název	název	k1gInSc1	název
onyx	onyx	k1gInSc1	onyx
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
nehet	nehet	k1gInSc4	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
střídavě	střídavě	k6eAd1	střídavě
černé	černý	k2eAgInPc1d1	černý
a	a	k8xC	a
bílé	bílý	k2eAgInPc1d1	bílý
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
pásky	pásek	k1gInPc1	pásek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
využity	využít	k5eAaPmNgInP	využít
podle	podle	k7c2	podle
natočení	natočení	k1gNnSc2	natočení
valounku	valounek	k1gInSc2	valounek
a	a	k8xC	a
vybroušení	vybroušení	k1gNnSc2	vybroušení
<g/>
,	,	kIx,	,
kameje	kamea	k1gFnSc2	kamea
mívají	mívat	k5eAaImIp3nP	mívat
černé	černý	k2eAgNnSc1d1	černé
pozadí	pozadí	k1gNnSc4	pozadí
a	a	k8xC	a
bílý	bílý	k2eAgInSc4d1	bílý
reliéf	reliéf	k1gInSc4	reliéf
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
nádobky	nádobka	k1gFnPc4	nádobka
jsou	být	k5eAaImIp3nP	být
pruhované	pruhovaný	k2eAgFnPc4d1	pruhovaná
či	či	k8xC	či
žíhané	žíhaný	k2eAgFnPc4d1	žíhaná
v	v	k7c6	v
kroužcích	kroužek	k1gInPc6	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
kámen	kámen	k1gInSc1	kámen
trojbarevný	trojbarevný	k2eAgInSc1d1	trojbarevný
–	–	k?	–
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
červenohnědou	červenohnědý	k2eAgFnSc7d1	červenohnědá
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
sardonyx	sardonyx	k1gInSc1	sardonyx
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barevnost	barevnost	k1gFnSc1	barevnost
a	a	k8xC	a
struktura	struktura	k1gFnSc1	struktura
onyxu	onyx	k1gInSc2	onyx
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
například	například	k6eAd1	například
u	u	k7c2	u
achátů	achát	k1gInPc2	achát
<g/>
)	)	kIx)	)
dá	dát	k5eAaPmIp3nS	dát
upravovat	upravovat	k5eAaImF	upravovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
onyxů	onyx	k1gInPc2	onyx
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
namáčením	namáčení	k1gNnSc7	namáčení
do	do	k7c2	do
cukerného	cukerný	k2eAgInSc2d1	cukerný
roztoku	roztok	k1gInSc2	roztok
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
částečky	částečka	k1gFnPc4	částečka
cukru	cukr	k1gInSc2	cukr
zuhelnatí	zuhelnatit	k5eAaPmIp3nP	zuhelnatit
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1	přírodní
kameny	kámen	k1gInPc1	kámen
nemají	mít	k5eNaImIp3nP	mít
tak	tak	k6eAd1	tak
jednolitou	jednolitý	k2eAgFnSc4d1	jednolitá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
nařezat	nařezat	k5eAaPmF	nařezat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
najde	najít	k5eAaPmIp3nS	najít
kus	kus	k1gInSc1	kus
bez	bez	k7c2	bez
vady	vada	k1gFnSc2	vada
<g/>
.	.	kIx.	.
<g/>
Název	název	k1gInSc1	název
zlatý	zlatý	k2eAgInSc1d1	zlatý
onyx	onyx	k1gInSc1	onyx
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
často	často	k6eAd1	často
jen	jen	k9	jen
onyx	onyx	k1gInSc1	onyx
i	i	k9	i
jinak	jinak	k6eAd1	jinak
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
vrstevnaté	vrstevnatý	k2eAgFnPc4d1	vrstevnatá
odrůdy	odrůda	k1gFnPc4	odrůda
aragonitu	aragonit	k1gInSc2	aragonit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
chemického	chemický	k2eAgNnSc2d1	chemické
hlediska	hledisko	k1gNnSc2	hledisko
jiný	jiný	k2eAgInSc4d1	jiný
minerál	minerál	k1gInSc4	minerál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
glyptika	glyptika	k1gFnSc1	glyptika
<g/>
:	:	kIx,	:
kameje	kameje	k1gFnSc1	kameje
<g/>
,	,	kIx,	,
intaglie	intaglie	k1gFnSc1	intaglie
<g/>
,	,	kIx,	,
pečetidla	pečetidlo	k1gNnPc1	pečetidlo
</s>
</p>
<p>
<s>
šperkařství	šperkařství	k1gNnSc1	šperkařství
<g/>
:	:	kIx,	:
kameny	kámen	k1gInPc4	kámen
do	do	k7c2	do
náušnic	náušnice	k1gFnPc2	náušnice
<g/>
,	,	kIx,	,
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
náhrdelníků	náhrdelník	k1gInPc2	náhrdelník
</s>
</p>
<p>
<s>
nádobí	nádobí	k1gNnSc1	nádobí
<g/>
:	:	kIx,	:
drobné	drobný	k2eAgFnPc1d1	drobná
číšky	číška	k1gFnPc1	číška
<g/>
,	,	kIx,	,
misky	miska	k1gFnPc1	miska
<g/>
,	,	kIx,	,
vázičky	vázička	k1gFnPc1	vázička
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ozdobný	ozdobný	k2eAgInSc1d1	ozdobný
kámen	kámen	k1gInSc1	kámen
</s>
</p>
<p>
<s>
desky	deska	k1gFnPc1	deska
stolků	stolek	k1gInPc2	stolek
</s>
</p>
<p>
<s>
obklady	obklad	k1gInPc1	obklad
stěn	stěna	k1gFnPc2	stěna
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
SRN	SRN	kA	SRN
–	–	k?	–
Iberg	Iberg	k1gInSc1	Iberg
<g/>
,	,	kIx,	,
Leisberg	Leisberg	k1gInSc1	Leisberg
<g/>
,	,	kIx,	,
Saalhausen	Saalhausen	k1gInSc1	Saalhausen
<g/>
,	,	kIx,	,
Plieskendorf	Plieskendorf	k1gInSc1	Plieskendorf
</s>
</p>
<p>
<s>
dále	daleko	k6eAd2	daleko
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
–	–	k?	–
Levice	levice	k1gFnSc1	levice
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Proslulá	proslulý	k2eAgFnSc1d1	proslulá
"	"	kIx"	"
<g/>
onyxová	onyxový	k2eAgFnSc1d1	onyxová
příčka	příčka	k1gFnSc1	příčka
<g/>
"	"	kIx"	"
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
Tugendhat	Tugendhat	k1gFnPc2	Tugendhat
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zhotovena	zhotoven	k2eAgFnSc1d1	zhotovena
z	z	k7c2	z
aragonitových	aragonitový	k2eAgFnPc2d1	aragonitová
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
<g/>
Jméno	jméno	k1gNnSc1	jméno
Onix	Onix	k1gInSc1	Onix
nese	nést	k5eAaImIp3nS	nést
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
Pokémonů	Pokémon	k1gInPc2	Pokémon
<g/>
.	.	kIx.	.
</s>
<s>
Onyx	onyx	k1gInSc1	onyx
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
i	i	k9	i
díl	díl	k1gInSc1	díl
knižní	knižní	k2eAgFnSc2d1	knižní
série	série	k1gFnSc2	série
HALO	halo	k1gNnSc1	halo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
<p>
<s>
Achát	achát	k1gInSc1	achát
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Onyx	onyx	k1gInSc1	onyx
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
onyx	onyx	k1gInSc1	onyx
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Onyx	onyx	k1gInSc1	onyx
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Onyx	onyx	k1gInSc1	onyx
v	v	k7c6	v
atlasu	atlas	k1gInSc6	atlas
minerálů	minerál	k1gInPc2	minerál
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
