<p>
<s>
Belize	Belize	k6eAd1	Belize
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
stát	stát	k1gInSc1	stát
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Guatemalou	Guatemala	k1gFnSc7	Guatemala
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgFnSc7d1	formální
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
součástí	součást	k1gFnPc2	součást
mayské	mayský	k2eAgFnSc2d1	mayská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
vzkvétala	vzkvétat	k5eAaImAgFnS	vzkvétat
zde	zde	k6eAd1	zde
mayská	mayský	k2eAgFnSc1d1	mayská
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
mezidobí	mezidobí	k1gNnSc6	mezidobí
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1520	[number]	k4	1520
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Španělé	Španěl	k1gMnPc1	Španěl
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Guatemaly	Guatemala	k1gFnSc2	Guatemala
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
usadili	usadit	k5eAaPmAgMnP	usadit
britští	britský	k2eAgMnPc1d1	britský
dřevorubci	dřevorubec	k1gMnPc1	dřevorubec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
začali	začít	k5eAaPmAgMnP	začít
těžit	těžit	k5eAaImF	těžit
dřevo	dřevo	k1gNnSc4	dřevo
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
lodí	loď	k1gFnPc2	loď
Královského	královský	k2eAgNnSc2d1	královské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
území	území	k1gNnSc1	území
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
přejmenovali	přejmenovat	k5eAaPmAgMnP	přejmenovat
na	na	k7c4	na
Britský	britský	k2eAgInSc4d1	britský
Honduras	Honduras	k1gInSc4	Honduras
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Belize	Belize	k1gFnSc2	Belize
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
získala	získat	k5eAaPmAgFnS	získat
kolonie	kolonie	k1gFnSc1	kolonie
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c6	na
Belize	Beliz	k1gInSc6	Beliz
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1981	[number]	k4	1981
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
britské	britský	k2eAgFnPc1d1	britská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Belize	Belize	k1gFnPc1	Belize
chránily	chránit	k5eAaImAgFnP	chránit
před	před	k7c7	před
útoky	útok	k1gInPc7	útok
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Guatemaly	Guatemala	k1gFnSc2	Guatemala
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nový	nový	k2eAgInSc4d1	nový
stát	stát	k1gInSc4	stát
uznala	uznat	k5eAaPmAgFnS	uznat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
považovala	považovat	k5eAaImAgFnS	považovat
Belize	Belize	k1gFnSc1	Belize
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Belize	Belize	k1gFnSc1	Belize
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Britského	britský	k2eAgMnSc4d1	britský
panovníka	panovník	k1gMnSc4	panovník
v	v	k7c6	v
Belize	Beliza	k1gFnSc6	Beliza
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
získání	získání	k1gNnSc2	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
zastávali	zastávat	k5eAaImAgMnP	zastávat
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc1	dva
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1981	[number]	k4	1981
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1993	[number]	k4	1993
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
Elmira	Elmira	k1gFnSc1	Elmira
Minita	Minita	k1gFnSc1	Minita
Gordonová	Gordonová	k1gFnSc1	Gordonová
<g/>
,	,	kIx,	,
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Sir	sir	k1gMnSc1	sir
Colville	Colville	k1gFnSc2	Colville
Norbert	Norbert	k1gMnSc1	Norbert
Young	Young	k1gMnSc1	Young
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
současným	současný	k2eAgMnSc7d1	současný
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Belize	Belize	k6eAd1	Belize
je	být	k5eAaImIp3nS	být
zapojeno	zapojit	k5eAaPmNgNnS	zapojit
do	do	k7c2	do
několika	několik	k4yIc2	několik
regionálních	regionální	k2eAgFnPc2d1	regionální
integračních	integrační	k2eAgFnPc2d1	integrační
organizací	organizace	k1gFnPc2	organizace
např.	např.	kA	např.
karibského	karibský	k2eAgNnSc2d1	Karibské
CARICOMU	CARICOMU	kA	CARICOMU
a	a	k8xC	a
středoamerického	středoamerický	k2eAgNnSc2d1	středoamerické
SICA	SICA	kA	SICA
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
dalších	další	k2eAgFnPc2d1	další
mezistátních	mezistátní	k2eAgFnPc2d1	mezistátní
amerických	americký	k2eAgFnPc2d1	americká
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Sdružení	sdružení	k1gNnSc1	sdružení
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Latinskoamerický	latinskoamerický	k2eAgInSc1d1	latinskoamerický
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Společenství	společenství	k1gNnSc1	společenství
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
a	a	k8xC	a
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Petrocaribe	Petrocarib	k1gMnSc5	Petrocarib
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
se	se	k3xPyFc4	se
zvedají	zvedat	k5eAaImIp3nP	zvedat
Maya	Maya	k?	Maya
Mountains	Mountains	k1gInSc4	Mountains
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Východní	východní	k2eAgNnSc4d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Honduraského	honduraský	k2eAgInSc2d1	honduraský
zálivu	záliv	k1gInSc2	záliv
je	být	k5eAaImIp3nS	být
lemováno	lemovat	k5eAaImNgNnS	lemovat
skupinou	skupina	k1gFnSc7	skupina
130	[number]	k4	130
písečných	písečný	k2eAgInPc2d1	písečný
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
a	a	k8xC	a
korálových	korálový	k2eAgInPc2d1	korálový
útesů	útes	k1gInPc2	útes
<g/>
,	,	kIx,	,
známých	známý	k1gMnPc2	známý
pod	pod	k7c7	pod
společným	společný	k2eAgInSc7d1	společný
názvem	název	k1gInSc7	název
Belizský	belizský	k2eAgInSc4d1	belizský
bariérový	bariérový	k2eAgInSc4d1	bariérový
útes	útes	k1gInSc4	útes
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc4	součást
Mezoamerického	Mezoamerický	k2eAgInSc2d1	Mezoamerický
korálového	korálový	k2eAgInSc2d1	korálový
útesu	útes	k1gInSc2	útes
<g/>
,	,	kIx,	,
největšího	veliký	k2eAgInSc2d3	veliký
korálového	korálový	k2eAgInSc2d1	korálový
útesu	útes	k1gInSc2	útes
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
do	do	k7c2	do
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Belize	Belize	k1gFnSc1	Belize
a	a	k8xC	a
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
Hondo	honda	k1gFnSc5	honda
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc1d1	tropické
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
severovýchodních	severovýchodní	k2eAgInPc2d1	severovýchodní
pasátů	pasát	k1gInPc2	pasát
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
2500	[number]	k4	2500
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
sušší	suchý	k2eAgNnSc1d2	sušší
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
srážkový	srážkový	k2eAgInSc1d1	srážkový
úhrn	úhrn	k1gInSc1	úhrn
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
4000	[number]	k4	4000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
23	[number]	k4	23
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
28	[number]	k4	28
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Dřevařský	dřevařský	k2eAgInSc1d1	dřevařský
průmysl	průmysl	k1gInSc1	průmysl
způsobil	způsobit	k5eAaPmAgInS	způsobit
vykácení	vykácení	k1gNnSc4	vykácení
obrovských	obrovský	k2eAgFnPc2d1	obrovská
ploch	plocha	k1gFnPc2	plocha
tropického	tropický	k2eAgInSc2d1	tropický
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Belize	Beliza	k1gFnSc6	Beliza
vyhlášeny	vyhlášen	k2eAgInPc4d1	vyhlášen
národní	národní	k2eAgInPc4d1	národní
parky	park	k1gInPc4	park
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
a	a	k8xC	a
přijaty	přijat	k2eAgInPc4d1	přijat
projekty	projekt	k1gInPc4	projekt
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
18,53	[number]	k4	18,53
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
pevnina	pevnina	k1gFnSc1	pevnina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
výsostných	výsostný	k2eAgFnPc2d1	výsostná
vod	voda	k1gFnPc2	voda
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
určitým	určitý	k2eAgInSc7d1	určitý
stupněm	stupeň	k1gInSc7	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Belize	Belize	k6eAd1	Belize
je	být	k5eAaImIp3nS	být
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
příjem	příjem	k1gInSc1	příjem
státu	stát	k1gInSc2	stát
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
turismu	turismus	k1gInSc6	turismus
a	a	k8xC	a
těžbě	těžba	k1gFnSc6	těžba
přírodních	přírodní	k2eAgFnPc2d1	přírodní
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
hrubý	hrubý	k2eAgInSc4d1	hrubý
domácí	domácí	k2eAgInSc4d1	domácí
produkt	produkt	k1gInSc4	produkt
přepočtený	přepočtený	k2eAgInSc4d1	přepočtený
na	na	k7c4	na
1	[number]	k4	1
obyvatele	obyvatel	k1gMnSc4	obyvatel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
paritu	parita	k1gFnSc4	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
hodnoty	hodnota	k1gFnSc2	hodnota
6	[number]	k4	6
566	[number]	k4	566
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc1d1	malá
farmy	farma	k1gFnPc1	farma
produkují	produkovat	k5eAaImIp3nP	produkovat
pomeranče	pomeranč	k1gInPc4	pomeranč
<g/>
,	,	kIx,	,
grepy	grep	k1gInPc4	grep
<g/>
,	,	kIx,	,
fazole	fazole	k1gFnPc4	fazole
<g/>
,	,	kIx,	,
banány	banán	k1gInPc4	banán
<g/>
,	,	kIx,	,
kukuřici	kukuřice	k1gFnSc4	kukuřice
a	a	k8xC	a
kokosové	kokosový	k2eAgInPc4d1	kokosový
ořechy	ořech	k1gInPc4	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
chov	chov	k1gInSc4	chov
dobytka	dobytek	k1gInSc2	dobytek
a	a	k8xC	a
rybolov	rybolov	k1gInSc1	rybolov
(	(	kIx(	(
<g/>
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
krevety	kreveta	k1gFnPc1	kreveta
<g/>
,	,	kIx,	,
humři	humr	k1gMnPc1	humr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
exportní	exportní	k2eAgFnSc7d1	exportní
surovinou	surovina	k1gFnSc7	surovina
je	být	k5eAaImIp3nS	být
surová	surový	k2eAgFnSc1d1	surová
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
banány	banán	k1gInPc1	banán
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
tropické	tropický	k2eAgNnSc4d1	tropické
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
džusy	džus	k1gInPc4	džus
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
mořské	mořský	k2eAgInPc1d1	mořský
plody	plod	k1gInPc1	plod
<g/>
.	.	kIx.	.
<g/>
Turisticky	turisticky	k6eAd1	turisticky
přitažlivé	přitažlivý	k2eAgInPc1d1	přitažlivý
jsou	být	k5eAaImIp3nP	být
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
korálové	korálový	k2eAgInPc1d1	korálový
útesy	útes	k1gInPc1	útes
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
turismu	turismus	k1gInSc2	turismus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
představovaly	představovat	k5eAaImAgInP	představovat
téměř	téměř	k6eAd1	téměř
19	[number]	k4	19
%	%	kIx~	%
celkového	celkový	k2eAgNnSc2d1	celkové
státního	státní	k2eAgNnSc2d1	státní
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Belize	Belize	k1gFnSc1	Belize
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
lidnatou	lidnatý	k2eAgFnSc7d1	lidnatá
zemí	zem	k1gFnSc7	zem
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
jsou	být	k5eAaImIp3nP	být
potomci	potomek	k1gMnPc1	potomek
afrických	africký	k2eAgInPc2d1	africký
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
Mayů	May	k1gMnPc2	May
<g/>
,	,	kIx,	,
evropských	evropský	k2eAgMnPc2d1	evropský
osadníků	osadník	k1gMnPc2	osadník
nebo	nebo	k8xC	nebo
míšenců	míšenec	k1gMnPc2	míšenec
těchto	tento	k3xDgFnPc2	tento
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
populace	populace	k1gFnSc2	populace
ovšem	ovšem	k9	ovšem
mluví	mluvit	k5eAaImIp3nS	mluvit
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
belizská	belizský	k2eAgFnSc1d1	Belizská
kreolština	kreolština	k1gFnSc1	kreolština
a	a	k8xC	a
původní	původní	k2eAgInPc1d1	původní
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
garífuna	garífuna	k1gFnSc1	garífuna
<g/>
,	,	kIx,	,
mayské	mayský	k2eAgInPc1d1	mayský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
mesticové	mestic	k1gMnPc1	mestic
tvořili	tvořit	k5eAaImAgMnP	tvořit
50	[number]	k4	50
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
Kriolové	Kriolový	k2eAgFnSc2d1	Kriolový
21	[number]	k4	21
%	%	kIx~	%
<g/>
,	,	kIx,	,
Mayové	Mayové	k2eAgFnSc1d1	Mayové
10	[number]	k4	10
%	%	kIx~	%
<g/>
,	,	kIx,	,
indiáni	indián	k1gMnPc1	indián
kmene	kmen	k1gInSc2	kmen
Garífuna	Garífuna	k1gFnSc1	Garífuna
6,4	[number]	k4	6,4
%	%	kIx~	%
a	a	k8xC	a
mennonité	mennonitý	k2eAgNnSc1d1	mennonité
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
okolo	okolo	k7c2	okolo
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Náboženská	náboženský	k2eAgFnSc1d1	náboženská
svoboda	svoboda	k1gFnSc1	svoboda
je	být	k5eAaImIp3nS	být
zaručena	zaručit	k5eAaPmNgFnS	zaručit
ústavou	ústava	k1gFnSc7	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
belizští	belizský	k2eAgMnPc1d1	belizský
atleti	atlet	k1gMnPc1	atlet
účastní	účastnit	k5eAaImIp3nP	účastnit
regionálních	regionální	k2eAgInPc2d1	regionální
Středoamerických	středoamerický	k2eAgInPc2d1	středoamerický
a	a	k8xC	a
karibských	karibský	k2eAgFnPc2d1	karibská
her	hra	k1gFnPc2	hra
a	a	k8xC	a
her	hra	k1gFnPc2	hra
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Středoamerického	středoamerický	k2eAgInSc2d1	středoamerický
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Belize	Belize	k6eAd1	Belize
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
6	[number]	k4	6
distriktů	distrikt	k1gInPc2	distrikt
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
distrikty	distrikt	k1gInPc1	distrikt
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
31	[number]	k4	31
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Belize	Belize	k1gFnSc2	Belize
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Belize	Belize	k1gFnSc2	Belize
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Belize	Belize	k1gFnSc1	Belize
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
turistické	turistický	k2eAgFnPc1d1	turistická
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
zemi	zem	k1gFnSc6	zem
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Detailní	detailní	k2eAgFnPc4d1	detailní
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
životě	život	k1gInSc6	život
v	v	k7c6	v
Belize	Beliza	k1gFnSc6	Beliza
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
</s>
</p>
<p>
<s>
Belize	Belize	k1gFnSc1	Belize
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Western	western	k1gInSc1	western
Hemispehere	Hemispeher	k1gInSc5	Hemispeher
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Belize	Belize	k1gFnSc1	Belize
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-05-02	[number]	k4	2011-05-02
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Belize	Belize	k1gFnSc1	Belize
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ALFORD	ALFORD	kA	ALFORD
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
E	E	kA	E
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Belize	Belize	k1gFnSc1	Belize
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
