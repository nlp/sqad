<s>
Transkripční	transkripční	k2eAgInSc1d1	transkripční
faktor	faktor	k1gInSc1	faktor
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
užívaný	užívaný	k2eAgInSc1d1	užívaný
pro	pro	k7c4	pro
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
protein	protein	k1gInSc4	protein
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
spouštět	spouštět	k5eAaImF	spouštět
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
regulovat	regulovat	k5eAaImF	regulovat
transkripci	transkripce	k1gFnSc4	transkripce
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
