<s>
Transkripční	transkripční	k2eAgInSc1d1	transkripční
faktor	faktor	k1gInSc1	faktor
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
užívaný	užívaný	k2eAgInSc1d1	užívaný
pro	pro	k7c4	pro
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
protein	protein	k1gInSc4	protein
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
spouštět	spouštět	k5eAaImF	spouštět
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
regulovat	regulovat	k5eAaImF	regulovat
transkripci	transkripce	k1gFnSc4	transkripce
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
proto	proto	k8xC	proto
jak	jak	k6eAd1	jak
specializované	specializovaný	k2eAgInPc1d1	specializovaný
proteiny	protein	k1gInPc1	protein
spouštějící	spouštějící	k2eAgFnSc4d1	spouštějící
transkripci	transkripce	k1gFnSc4	transkripce
u	u	k7c2	u
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
obecné	obecný	k2eAgInPc1d1	obecný
faktory	faktor	k1gInPc1	faktor
nutné	nutný	k2eAgInPc1d1	nutný
pro	pro	k7c4	pro
správný	správný	k2eAgInSc4d1	správný
průběh	průběh	k1gInSc4	průběh
procesu	proces	k1gInSc2	proces
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obecným	obecný	k2eAgInPc3d1	obecný
transkripčním	transkripční	k2eAgInPc3d1	transkripční
faktorům	faktor	k1gInPc3	faktor
při	při	k7c6	při
transkripci	transkripce	k1gFnSc6	transkripce
pomocí	pomocí	k7c2	pomocí
RNA	RNA	kA	RNA
polymerázy	polymeráza	k1gFnSc2	polymeráza
II	II	kA	II
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
proteiny	protein	k1gInPc1	protein
TFIIA	TFIIA	kA	TFIIA
<g/>
,	,	kIx,	,
TFIIB	TFIIB	kA	TFIIB
<g/>
,	,	kIx,	,
TFIIE	TFIIE	kA	TFIIE
<g/>
,	,	kIx,	,
TFIIF	TFIIF	kA	TFIIF
a	a	k8xC	a
TFIIH	TFIIH	kA	TFIIH
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
účastní	účastnit	k5eAaImIp3nS	účastnit
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
preiniciačního	preiniciační	k2eAgInSc2d1	preiniciační
komplexu	komplex	k1gInSc2	komplex
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
interagují	interagovat	k5eAaImIp3nP	interagovat
s	s	k7c7	s
RNA	RNA	kA	RNA
polymerázou	polymeráza	k1gFnSc7	polymeráza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
proteinem	protein	k1gInSc7	protein
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
faktorů	faktor	k1gInPc2	faktor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
TBP	TBP	kA	TBP
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
protein	protein	k1gInSc4	protein
vážící	vážící	k2eAgInSc4d1	vážící
se	se	k3xPyFc4	se
na	na	k7c4	na
TATA	tata	k0	tata
box	box	k1gInSc1	box
sekvenci	sekvence	k1gFnSc4	sekvence
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
přítomnou	přítomný	k2eAgFnSc4d1	přítomná
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
každého	každý	k3xTgInSc2	každý
přepisovaného	přepisovaný	k2eAgInSc2d1	přepisovaný
genu	gen	k1gInSc2	gen
(	(	kIx(	(
<g/>
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
promotoru	promotor	k1gInSc2	promotor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
