<p>
<s>
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
ve	v	k7c6	v
dnech	den	k1gInPc6	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
až	až	k9	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1948	[number]	k4	1948
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
pořádat	pořádat	k5eAaImF	pořádat
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
kvůli	kvůli	k7c3	kvůli
probíhající	probíhající	k2eAgFnSc3d1	probíhající
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgInPc4d1	světový
válce	válec	k1gInPc4	válec
neuskutečnily	uskutečnit	k5eNaPmAgFnP	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
nemohli	moct	k5eNaImAgMnP	moct
účastnit	účastnit	k5eAaImF	účastnit
sportovci	sportovec	k1gMnPc1	sportovec
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
označil	označit	k5eAaPmAgInS	označit
hry	hra	k1gFnPc4	hra
za	za	k7c7	za
"	"	kIx"	"
<g/>
nástroj	nástroj	k1gInSc1	nástroj
imperialismu	imperialismus	k1gInSc2	imperialismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
nevyslal	vyslat	k5eNaPmAgInS	vyslat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
4099	[number]	k4	4099
sportovců	sportovec	k1gMnPc2	sportovec
z	z	k7c2	z
59	[number]	k4	59
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
reprezentovalo	reprezentovat	k5eAaImAgNnS	reprezentovat
69	[number]	k4	69
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
14	[number]	k4	14
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
hodnocení	hodnocení	k1gNnSc6	hodnocení
se	se	k3xPyFc4	se
československá	československý	k2eAgFnSc1d1	Československá
výprava	výprava	k1gFnSc1	výprava
s	s	k7c7	s
jedenácti	jedenáct	k4xCc7	jedenáct
medailemi	medaile	k1gFnPc7	medaile
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počet	počet	k1gInSc1	počet
medailí	medaile	k1gFnPc2	medaile
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Kandidátská	kandidátský	k2eAgNnPc1d1	kandidátské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
uspořádání	uspořádání	k1gNnSc6	uspořádání
14	[number]	k4	14
<g/>
.	.	kIx.	.
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
ucházelo	ucházet	k5eAaImAgNnS	ucházet
5	[number]	k4	5
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
<g/>
:	:	kIx,	:
Baltimore	Baltimore	k1gInSc1	Baltimore
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lausanne	Lausanne	k1gNnSc1	Lausanne
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Minneapolis	Minneapolis	k1gFnSc1	Minneapolis
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Československo	Československo	k1gNnSc1	Československo
na	na	k7c4	na
LOH	LOH	kA	LOH
1948	[number]	k4	1948
==	==	k?	==
</s>
</p>
