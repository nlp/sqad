<s>
Telgárt	Telgárt	k1gInSc1	Telgárt
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brezno	Brezna	k1gFnSc5	Brezna
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
Margecany	Margecana	k1gFnSc2	Margecana
-	-	kIx~	-
Červená	červenat	k5eAaImIp3nS	červenat
Skala	Skala	k1gMnSc1	Skala
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1326	[number]	k4	1326
či	či	k8xC	či
1549	[number]	k4	1549
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
(	(	kIx(	(
<g/>
SNP	SNP	kA	SNP
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
Telgártu	Telgárt	k1gInSc2	Telgárt
vedly	vést	k5eAaImAgInP	vést
těžké	těžký	k2eAgInPc1d1	těžký
boje	boj	k1gInPc1	boj
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
Telgárt	Telgárta	k1gFnPc2	Telgárta
vypálila	vypálit	k5eAaPmAgFnS	vypálit
německá	německý	k2eAgFnSc1d1	německá
vojska	vojsko	k1gNnPc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Zničeno	zničen	k2eAgNnSc1d1	zničeno
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
260	[number]	k4	260
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
obyvatelé	obyvatel	k1gMnPc1	obyvatel
obce	obec	k1gFnSc2	obec
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
až	až	k9	až
1990	[number]	k4	1990
nesla	nést	k5eAaImAgFnS	nést
obec	obec	k1gFnSc1	obec
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
českého	český	k2eAgMnSc2d1	český
komunistického	komunistický	k2eAgMnSc2d1	komunistický
politika	politik	k1gMnSc2	politik
a	a	k8xC	a
účastníka	účastník	k1gMnSc2	účastník
SNP	SNP	kA	SNP
Jana	Jan	k1gMnSc2	Jan
Švermy	Šverma	k1gFnSc2	Šverma
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
název	název	k1gInSc1	název
Švermovo	Švermův	k2eAgNnSc5d1	Švermovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zde	zde	k6eAd1	zde
působili	působit	k5eAaImAgMnP	působit
<g/>
:	:	kIx,	:
Ondrej	Ondrej	k1gMnSc1	Ondrej
Lunter	Lunter	k1gMnSc1	Lunter
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Demagog	demagog	k1gMnSc1	demagog
<g/>
.	.	kIx.	.
<g/>
SK	Sk	kA	Sk
a	a	k8xC	a
Demagog	demagog	k1gMnSc1	demagog
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
Anastázia	Anastázia	k1gFnSc1	Anastázia
Miertušová	Miertušová	k1gFnSc1	Miertušová
<g/>
,	,	kIx,	,
slovenská	slovenský	k2eAgFnSc1d1	slovenská
výtvarnice	výtvarnice	k1gFnSc1	výtvarnice
Ivan	Ivan	k1gMnSc1	Ivan
Andrijovič	Andrijovič	k1gMnSc1	Andrijovič
Stavrovský	Stavrovský	k2eAgMnSc1d1	Stavrovský
<g/>
,	,	kIx,	,
řeckokatolický	řeckokatolický	k2eAgMnSc1d1	řeckokatolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
osvětový	osvětový	k2eAgMnSc1d1	osvětový
pracovník	pracovník	k1gMnSc1	pracovník
Ján	Ján	k1gMnSc1	Ján
Ambróz	Ambróza	k1gFnPc2	Ambróza
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
Michal	Michal	k1gMnSc1	Michal
Spišiak	Spišiak	k1gMnSc1	Spišiak
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Černák	Černák	k1gMnSc1	Černák
<g/>
,	,	kIx,	,
mafián	mafián	k1gMnSc1	mafián
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Telgárt	Telgárta	k1gFnPc2	Telgárta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
