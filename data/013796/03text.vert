<s>
Antoni	Antoni	k1gNnSc1
Gaudí	Gaudí	k1gNnSc1
</s>
<s>
Antoni	Anton	k1gMnPc1
Gaudí	Gaudí	k1gNnSc2
Antoni	Anton	k1gMnPc1
Gaudí	Gaudí	k1gNnSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1878	#num#	k4
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1852	#num#	k4
Riudoms	Riudomsa	k1gFnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1926	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
73	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Barcelona	Barcelona	k1gFnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
střet	střet	k1gInSc1
s	s	k7c7
tramvají	tramvaj	k1gFnSc7
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Chrám	chrám	k1gInSc1
Sagrada	Sagrada	k1gFnSc1
Família	Famílius	k1gMnSc2
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Escuela	Escuela	k1gFnSc1
Técnica	Técnica	k1gMnSc1
Superior	superior	k1gMnSc1
de	de	k?
Arquitectura	Arquitectura	k1gFnSc1
de	de	k?
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
1873	#num#	k4
<g/>
–	–	k?
<g/>
1876	#num#	k4
<g/>
)	)	kIx)
<g/>
Escola	Escola	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Llotja	Llotja	k1gFnSc1
Povolání	povolání	k1gNnSc1
</s>
<s>
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
designér	designér	k1gMnSc1
a	a	k8xC
kreslič	kreslič	k1gMnSc1
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Casa	Cas	k2eAgFnSc1d1
Milà	Milà	k1gFnSc1
FamíliaBellesguardCasa	FamíliaBellesguardCasa	k1gFnSc1
El	Ela	k1gFnPc2
CaprichoPark	CaprichoPark	k1gInSc1
Güell	Güell	k1gMnSc1
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Podpis	podpis	k1gInSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
citáty	citát	k1gInPc1
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Antoni	Anton	k1gMnPc1
Gaudí	Gaudí	k1gNnPc2
i	i	k8xC
Cornet	Corneta	k1gFnPc2
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1852	#num#	k4
Reus	Reusa	k1gFnPc2
u	u	k7c2
Tarragony	Tarragona	k1gFnSc2
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1926	#num#	k4
Barcelona	Barcelona	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
katalánský	katalánský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
katalánského	katalánský	k2eAgInSc2d1
modernismu	modernismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valná	valný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
Gaudího	Gaudí	k2eAgInSc2d1
staveb	stavba	k1gFnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Katalánska	Katalánsko	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
nejznámější	známý	k2eAgNnPc1d3
díla	dílo	k1gNnPc1
patří	patřit	k5eAaImIp3nP
chrám	chrám	k1gInSc4
Sagrada	Sagrada	k1gFnSc1
Familia	Familia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tvorba	tvorba	k1gFnSc1
</s>
<s>
Gaudího	Gaudí	k1gMnSc4
návrhy	návrh	k1gInPc4
jsou	být	k5eAaImIp3nP
charakteristické	charakteristický	k2eAgInPc1d1
jedinečnou	jedinečný	k2eAgFnSc7d1
osobitostí	osobitost	k1gFnSc7
a	a	k8xC
důrazem	důraz	k1gInSc7
na	na	k7c4
detail	detail	k1gInSc4
<g/>
:	:	kIx,
velice	velice	k6eAd1
často	často	k6eAd1
do	do	k7c2
svých	svůj	k3xOyFgInPc2
plánů	plán	k1gInPc2
zahrnoval	zahrnovat	k5eAaImAgMnS
propracované	propracovaný	k2eAgFnSc2d1
mozaiky	mozaika	k1gFnSc2
<g/>
,	,	kIx,
vitráže	vitráž	k1gFnSc2
<g/>
,	,	kIx,
keramiku	keramika	k1gFnSc4
nebo	nebo	k8xC
tesařské	tesařský	k2eAgInPc4d1
a	a	k8xC
kovářské	kovářský	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
zavedl	zavést	k5eAaPmAgInS
i	i	k9
nové	nový	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
zpracování	zpracování	k1gNnSc2
těchto	tento	k3xDgInPc2
materiálů	materiál	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
metodu	metoda	k1gFnSc4
trencadís	trencadísa	k1gFnPc2
<g/>
,	,	kIx,
tvorbu	tvorba	k1gFnSc4
barevných	barevný	k2eAgFnPc2d1
mozaik	mozaika	k1gFnPc2
z	z	k7c2
keramických	keramický	k2eAgInPc2d1
střepů	střep	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Antoni	Anton	k1gMnPc1
Gaudí	Gaudí	k1gNnSc2
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
katalánského	katalánský	k2eAgNnSc2d1
modernistického	modernistický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
dosahovalo	dosahovat	k5eAaImAgNnS
největšího	veliký	k2eAgInSc2d3
vlivu	vliv	k1gInSc2
na	na	k7c6
přelomu	přelom	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
mnohém	mnohé	k1gNnSc6
však	však	k9
klasické	klasický	k2eAgFnPc4d1
modernistické	modernistický	k2eAgFnPc4d1
tendence	tendence	k1gFnPc4
přesahoval	přesahovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
styl	styl	k1gInSc1
byl	být	k5eAaImAgInS
inspirován	inspirovat	k5eAaBmNgInS
přírodními	přírodní	k2eAgInPc7d1
tvary	tvar	k1gInPc7
a	a	k8xC
orientálními	orientální	k2eAgFnPc7d1
technikami	technika	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
složité	složitý	k2eAgInPc4d1
návrhy	návrh	k1gInPc4
málokdy	málokdy	k6eAd1
kreslil	kreslit	k5eAaImAgInS
na	na	k7c4
papír	papír	k1gInSc4
<g/>
,	,	kIx,
mnohem	mnohem	k6eAd1
raději	rád	k6eAd2
tvořil	tvořit	k5eAaImAgInS
propracované	propracovaný	k2eAgInPc4d1
trojrozměrné	trojrozměrný	k2eAgInPc4d1
modely	model	k1gInPc4
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc2
částí	část	k1gFnPc2
a	a	k8xC
složité	složitý	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
tvorbu	tvorba	k1gFnSc4
jsou	být	k5eAaImIp3nP
typické	typický	k2eAgInPc1d1
parabolické	parabolický	k2eAgInPc1d1
oblouky	oblouk	k1gInPc1
a	a	k8xC
dokonalá	dokonalý	k2eAgNnPc1d1
konstrukční	konstrukční	k2eAgNnPc1d1
řešení	řešení	k1gNnPc1
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
originální	originální	k2eAgFnSc7d1
uměleckou	umělecký	k2eAgFnSc7d1
imaginací	imaginace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gaudí	Gaudí	k1gNnPc2
vynikal	vynikat	k5eAaImAgMnS
rovněž	rovněž	k9
jako	jako	k9
designer	designer	k1gInSc4
interiérů	interiér	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
svých	svůj	k3xOyFgFnPc2
budov	budova	k1gFnPc2
zdobil	zdobit	k5eAaImAgMnS
osobně	osobně	k6eAd1
<g/>
,	,	kIx,
od	od	k7c2
nábytku	nábytek	k1gInSc2
až	až	k9
po	po	k7c4
nejmenší	malý	k2eAgInPc4d3
detaily	detail	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
architektonická	architektonický	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
prošla	projít	k5eAaPmAgFnS
zhruba	zhruba	k6eAd1
třemi	tři	k4xCgInPc7
vývojovými	vývojový	k2eAgInPc7d1
stupni	stupeň	k1gInPc7
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
orientální	orientální	k2eAgInSc1d1
styl	styl	k1gInSc1
–	–	k?
inspirovaný	inspirovaný	k2eAgInSc1d1
maurskými	maurský	k2eAgFnPc7d1
památkami	památka	k1gFnPc7
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
a	a	k8xC
uměním	umění	k1gNnSc7
Středního	střední	k2eAgInSc2d1
a	a	k8xC
Dálného	dálný	k2eAgInSc2d1
východu	východ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
výzdobu	výzdoba	k1gFnSc4
používal	používat	k5eAaImAgMnS
keramické	keramický	k2eAgFnPc4d1
dlaždice	dlaždice	k1gFnPc4
s	s	k7c7
květy	květ	k1gInPc7
a	a	k8xC
listy	list	k1gInPc7
<g/>
,	,	kIx,
sloupy	sloup	k1gInPc1
z	z	k7c2
cihel	cihla	k1gFnPc2
<g/>
,	,	kIx,
maurské	maurský	k2eAgInPc1d1
oblouky	oblouk	k1gInPc1
a	a	k8xC
věže	věž	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
sem	sem	k6eAd1
např.	např.	kA
Casa	Casa	k1gFnSc1
Vicens	Vicens	k1gInSc1
nebo	nebo	k8xC
Casa	Casa	k1gFnSc1
El	Ela	k1gFnPc2
Capricho	Capricha	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
gotické	gotický	k2eAgNnSc4d1
období	období	k1gNnSc4
–	–	k?
vycházel	vycházet	k5eAaImAgMnS
ze	z	k7c2
středověkého	středověký	k2eAgNnSc2d1
gotického	gotický	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
osobitým	osobitý	k2eAgNnSc7d1
konstrukčním	konstrukční	k2eAgNnSc7d1
řešením	řešení	k1gNnSc7
založeným	založený	k2eAgNnSc7d1
na	na	k7c4
znalosti	znalost	k1gFnPc4
fraktální	fraktální	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkci	funkce	k1gFnSc3
opěrných	opěrný	k2eAgInPc2d1
oblouků	oblouk	k1gInPc2
převzaly	převzít	k5eAaPmAgFnP
rozvětvené	rozvětvený	k2eAgInPc4d1
nakloněné	nakloněný	k2eAgInPc4d1
pilíře	pilíř	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
reprezentováno	reprezentovat	k5eAaImNgNnS
hlavně	hlavně	k9
díly	díl	k1gInPc4
mimo	mimo	k7c4
Barcelonu	Barcelona	k1gFnSc4
<g/>
,	,	kIx,
např.	např.	kA
nerealizovaný	realizovaný	k2eNgInSc1d1
projekt	projekt	k1gInSc1
katolické	katolický	k2eAgFnSc2d1
mise	mise	k1gFnSc2
v	v	k7c6
Tangeru	Tanger	k1gInSc6
<g/>
,	,	kIx,
budovy	budova	k1gFnPc4
v	v	k7c4
Astorze	Astorze	k1gFnPc4
a	a	k8xC
Leónu	Leóna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
naturalisticko-secesní	naturalisticko-secesní	k2eAgInSc4d1
styl	styl	k1gInSc4
–	–	k?
zdokonalil	zdokonalit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
osobní	osobní	k2eAgInSc4d1
styl	styl	k1gInSc4
inspirovaný	inspirovaný	k2eAgInSc1d1
organickými	organický	k2eAgInPc7d1
tvary	tvar	k1gInPc7
přírody	příroda	k1gFnSc2
a	a	k8xC
nápaditou	nápaditý	k2eAgFnSc7d1
ornamentální	ornamentální	k2eAgFnSc7d1
výzdobou	výzdoba	k1gFnSc7
<g/>
,	,	kIx,
strukturálním	strukturální	k2eAgNnSc7d1
bohatstvím	bohatství	k1gNnSc7
tvarů	tvar	k1gInPc2
a	a	k8xC
objemů	objem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zahájeno	zahájit	k5eAaPmNgNnS
pracemi	práce	k1gFnPc7
na	na	k7c6
Guellově	Guellův	k2eAgInSc6d1
parku	park	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Mnoho	mnoho	k6eAd1
Gaudího	Gaudí	k1gMnSc2
projektů	projekt	k1gInPc2
zůstalo	zůstat	k5eAaPmAgNnS
nerealizovaných	realizovaný	k2eNgMnPc2d1
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
zakázky	zakázka	k1gFnPc1
byly	být	k5eAaImAgFnP
realizovány	realizovat	k5eAaBmNgFnP
jen	jen	k6eAd1
částečně	částečně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Gaudího	Gaudí	k1gMnSc4
tvorba	tvorba	k1gFnSc1
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
těší	těšit	k5eAaImIp3nS
oblibě	obliba	k1gFnSc3
široké	široký	k2eAgFnSc3d1
veřejnosti	veřejnost	k1gFnSc3
a	a	k8xC
obdivu	obdiv	k1gInSc3
současných	současný	k2eAgMnPc2d1
architektů	architekt	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrám	chrám	k1gInSc1
Sagrada	Sagrada	k1gFnSc1
Família	Família	k1gFnSc1
<g/>
,	,	kIx,
zatím	zatím	k6eAd1
stále	stále	k6eAd1
ve	v	k7c6
výstavbě	výstavba	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejnavštěvovanější	navštěvovaný	k2eAgFnSc7d3
architektonickou	architektonický	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
celého	celý	k2eAgNnSc2d1
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1984	#num#	k4
a	a	k8xC
2005	#num#	k4
bylo	být	k5eAaImAgNnS
sedm	sedm	k4xCc1
Gaudího	Gaudí	k2eAgInSc2d1
staveb	stavba	k1gFnPc2
připsaných	připsaný	k2eAgInPc2d1
na	na	k7c4
seznam	seznam	k1gInSc4
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Gaudí	Gaudit	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1852	#num#	k4
v	v	k7c4
Reus	Reus	k1gInSc4
jako	jako	k8xC,k8xS
páté	pátý	k4xOgNnSc4
dítě	dítě	k1gNnSc4
Francescovi	Francesec	k1gMnSc3
Gaudímu	Gaudí	k1gMnSc3
i	i	k8xC
Serra	Serr	k1gMnSc2
(	(	kIx(
<g/>
povoláním	povolání	k1gNnSc7
kovotepec	kovotepec	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gFnSc3
manželce	manželka	k1gFnSc3
Antonii	Antonie	k1gFnSc4
Cornetové	Cornetový	k2eAgFnPc1d1
i	i	k8xC
Bertran	Bertran	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dětství	dětství	k1gNnSc6
byl	být	k5eAaImAgInS
často	často	k6eAd1
nemocný	nemocný	k2eAgMnSc1d1,k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
plicní	plicní	k2eAgFnSc6d1
infekci	infekce	k1gFnSc6
se	se	k3xPyFc4
u	u	k7c2
něho	on	k3xPp3gNnSc2
vyvinula	vyvinout	k5eAaPmAgFnS
revmatoidní	revmatoidní	k2eAgFnSc1d1
artritida	artritida	k1gFnSc1
<g/>
,	,	kIx,
následkem	následkem	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
občas	občas	k6eAd1
ani	ani	k8xC
nemohl	moct	k5eNaImAgMnS
chodit	chodit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Školní	školní	k2eAgFnSc4d1
docházku	docházka	k1gFnSc4
zahájil	zahájit	k5eAaPmAgMnS
v	v	k7c6
obecné	obecný	k2eAgFnSc6d1
škole	škola	k1gFnSc6
Rafaela	Rafael	k1gMnSc2
Palaua	Palauus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
pak	pak	k6eAd1
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
malé	malý	k2eAgFnSc2d1
podkrovní	podkrovní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
založené	založený	k2eAgFnSc2d1
Franciscem	Francisce	k1gMnSc7
Berenguerem	Berenguer	k1gMnSc7
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1863	#num#	k4
pak	pak	k6eAd1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1868	#num#	k4
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
církevní	církevní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
Colegio	Colegio	k6eAd1
de	de	k?
los	los	k1gInSc1
Padres	Padres	k1gMnSc1
Escolapios	Escolapios	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
škole	škola	k1gFnSc6
byl	být	k5eAaImAgInS
průměrným	průměrný	k2eAgMnSc7d1
žákem	žák	k1gMnSc7
<g/>
,	,	kIx,
jediný	jediný	k2eAgInSc1d1
předmět	předmět	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
vynikal	vynikat	k5eAaImAgMnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
geometrie	geometrie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
mládí	mládí	k1gNnSc2
byl	být	k5eAaImAgInS
zaujat	zaujmout	k5eAaPmNgInS
přírodou	příroda	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
tvorbě	tvorba	k1gFnSc6
v	v	k7c6
podobě	podoba	k1gFnSc6
balkónů	balkón	k1gInPc2
připomínajících	připomínající	k2eAgInPc2d1
chaluhy	chaluha	k1gFnPc4
(	(	kIx(
<g/>
Casa	Casa	k1gMnSc1
Mila	Mila	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sloupů	sloup	k1gInPc2
připomínajících	připomínající	k2eAgInPc2d1
stromy	strom	k1gInPc4
(	(	kIx(
<g/>
Güellův	Güellův	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
ap.	ap.	kA
</s>
<s>
Roku	rok	k1gInSc2
1869	#num#	k4
odjel	odjet	k5eAaPmAgInS
s	s	k7c7
bratrem	bratr	k1gMnSc7
do	do	k7c2
Barcelony	Barcelona	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
začal	začít	k5eAaPmAgInS
studovat	studovat	k5eAaImF
na	na	k7c6
přírodovědecké	přírodovědecký	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1874	#num#	k4
Gaudí	Gaudí	k1gNnSc2
úspěšně	úspěšně	k6eAd1
složil	složit	k5eAaPmAgMnS
všechny	všechen	k3xTgFnPc4
předepsané	předepsaný	k2eAgFnPc4d1
zkoušky	zkouška	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
mohl	moct	k5eAaImAgInS
tak	tak	k6eAd1
začít	začít	k5eAaPmF
se	s	k7c7
studiem	studio	k1gNnSc7
architektury	architektura	k1gFnSc2
na	na	k7c6
Escola	Escola	k1gFnSc1
Provincial	Provincial	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Arquitektura	Arquitektura	k1gFnSc1
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1878	#num#	k4
pak	pak	k6eAd1
získal	získat	k5eAaPmAgMnS
diplom	diplom	k1gInSc4
architekta	architekt	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1878	#num#	k4
navrhl	navrhnout	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
dům	dům	k1gInSc4
(	(	kIx(
<g/>
Casa	Cas	k1gInSc2
Vicens	Vicens	k1gInSc1
pro	pro	k7c4
majitele	majitel	k1gMnSc4
cihelny	cihelna	k1gFnSc2
Manuela	Manuela	k1gFnSc1
Vicens	Vicens	k1gInSc1
y	y	k?
Montaner	Montaner	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesnaží	snažit	k5eNaImIp3nS
se	se	k3xPyFc4
napodobit	napodobit	k5eAaPmF
žádný	žádný	k3yNgInSc4
jiný	jiný	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
naprosto	naprosto	k6eAd1
originální	originální	k2eAgFnSc1d1
<g/>
,	,	kIx,
kombinuje	kombinovat	k5eAaImIp3nS
hrubý	hrubý	k2eAgInSc1d1
kámen	kámen	k1gInSc1
s	s	k7c7
cihlami	cihla	k1gFnPc7
a	a	k8xC
keramickými	keramický	k2eAgFnPc7d1
dlaždicemi	dlaždice	k1gFnPc7
<g/>
,	,	kIx,
inspiruje	inspirovat	k5eAaBmIp3nS
se	se	k3xPyFc4
maurským	maurský	k2eAgInSc7d1
stylem	styl	k1gInSc7
a	a	k8xC
zároveň	zároveň	k6eAd1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
vlastní	vlastní	k2eAgInPc4d1
ornamenty	ornament	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčovým	klíčový	k2eAgNnSc7d1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
jeho	jeho	k3xOp3gNnSc1
setkání	setkání	k1gNnSc1
s	s	k7c7
člověkem	člověk	k1gMnSc7
jménem	jméno	k1gNnSc7
Eusebi	Eusebi	k1gNnSc4
Güell	Güell	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc7
hlavním	hlavní	k2eAgMnSc7d1
mecenášem	mecenáš	k1gMnSc7
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
díky	díky	k7c3
němu	on	k3xPp3gMnSc3
vznikla	vzniknout	k5eAaPmAgFnS
mnohá	mnohý	k2eAgFnSc1d1
jeho	jeho	k3xOp3gMnPc3
skvostná	skvostný	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1883	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
vrcholný	vrcholný	k2eAgInSc4d1
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
nedostavěnou	dostavěný	k2eNgFnSc4d1
katedrálu	katedrála	k1gFnSc4
Sagrada	Sagrada	k1gFnSc1
Família	Família	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
její	její	k3xOp3gFnSc2
stavby	stavba	k1gFnSc2
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
i	i	k8xC
jinými	jiný	k2eAgNnPc7d1
díly	dílo	k1gNnPc7
<g/>
,	,	kIx,
například	například	k6eAd1
Palácem	palác	k1gInSc7
rodiny	rodina	k1gFnSc2
Güellů	Güell	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
poprvé	poprvé	k6eAd1
navrhoval	navrhovat	k5eAaImAgInS
i	i	k9
interiér	interiér	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
Güellovým	Güellův	k2eAgInSc7d1
parkem	park	k1gInSc7
<g/>
,	,	kIx,
dodnes	dodnes	k6eAd1
jedním	jeden	k4xCgInSc7
z	z	k7c2
klenotů	klenot	k1gInPc2
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1914	#num#	k4
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
se	se	k3xPyFc4
plně	plně	k6eAd1
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
stavbě	stavba	k1gFnSc3
katedrály	katedrála	k1gFnSc2
Sagrada	Sagrada	k1gFnSc1
Familia	Familia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc7
posledním	poslední	k2eAgNnSc7d1
přáním	přání	k1gNnSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
čistě	čistě	k6eAd1
z	z	k7c2
darovaných	darovaný	k2eAgInPc2d1
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
staví	stavit	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
dodnes	dodnes	k6eAd1
—	—	k?
předpokládaný	předpokládaný	k2eAgInSc4d1
rok	rok	k1gInSc4
dostavby	dostavba	k1gFnSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
rok	rok	k1gInSc4
2026	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Žil	žít	k5eAaImAgMnS
samotářsky	samotářsky	k6eAd1
<g/>
,	,	kIx,
značně	značně	k6eAd1
asketicky	asketicky	k6eAd1
a	a	k8xC
v	v	k7c6
naprosté	naprostý	k2eAgFnSc6d1
oddanosti	oddanost	k1gFnSc6
své	svůj	k3xOyFgFnSc6
umělecké	umělecký	k2eAgFnSc6d1
práci	práce	k1gFnSc6
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
se	se	k3xPyFc4
neoženil	oženit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1926	#num#	k4
utrpěl	utrpět	k5eAaPmAgInS
těžká	těžký	k2eAgNnPc4d1
zranění	zranění	k1gNnPc4
při	při	k7c6
srážce	srážka	k1gFnSc6
s	s	k7c7
tramvají	tramvaj	k1gFnSc7
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
prý	prý	k9
zahleděl	zahledět	k5eAaPmAgMnS
na	na	k7c4
Sagradu	Sagrada	k1gFnSc4
Familiu	Familius	k1gMnSc6
–	–	k?
zemřel	zemřít	k5eAaPmAgMnS
o	o	k7c4
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
svému	svůj	k3xOyFgInSc3
chudému	chudý	k2eAgInSc3d1
oděvu	oděv	k1gInSc3
byl	být	k5eAaImAgInS
považován	považován	k2eAgInSc1d1
za	za	k7c4
žebráka	žebrák	k1gMnSc4
<g/>
,	,	kIx,
identifikoval	identifikovat	k5eAaBmAgMnS
ho	on	k3xPp3gNnSc4
až	až	k6eAd1
kaplan	kaplan	k1gMnSc1
chrámu	chrám	k1gInSc2
Svaté	svatý	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
Mosén	Mosén	k1gInSc1
Gil	Gil	k1gMnSc1
Parés	Parés	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochován	pochovat	k5eAaPmNgInS
je	být	k5eAaImIp3nS
v	v	k7c6
kryptě	krypta	k1gFnSc6
chrámu	chrám	k1gInSc2
Sagrada	Sagrada	k1gFnSc1
Familia	Familia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgMnS
vegetarián	vegetarián	k1gMnSc1
<g/>
,	,	kIx,
ne	ne	k9
ale	ale	k9
z	z	k7c2
etických	etický	k2eAgFnPc2d1
<g/>
,	,	kIx,
jako	jako	k9
spíše	spíše	k9
ze	z	k7c2
zdravotních	zdravotní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
trpěl	trpět	k5eAaImAgMnS
totiž	totiž	k9
silným	silný	k2eAgInSc7d1
revmatismem	revmatismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
v	v	k7c6
katolické	katolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
probíhá	probíhat	k5eAaImIp3nS
proces	proces	k1gInSc4
Gaudího	Gaudí	k1gMnSc2
beatifikace	beatifikace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavby	stavba	k1gFnPc1
</s>
<s>
Dílo	dílo	k1gNnSc1
Antoniho	Antoni	k1gMnSc2
GaudíhoSvětové	GaudíhoSvětový	k2eAgNnSc1d1
dědictví	dědictví	k1gNnSc1
UNESCO	UNESCO	kA
Smluvní	smluvní	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Typ	typ	k1gInSc1
</s>
<s>
kulturní	kulturní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
Kritérium	kritérium	k1gNnSc1
</s>
<s>
i	i	k9
<g/>
,	,	kIx,
ii	ii	k?
<g/>
,	,	kIx,
iv	iv	k?
Odkaz	odkaz	k1gInSc1
</s>
<s>
320	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zařazení	zařazení	k1gNnSc1
do	do	k7c2
seznamu	seznam	k1gInSc2
Zařazení	zařazení	k1gNnSc1
</s>
<s>
1984	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc1
<g/>
)	)	kIx)
Změny	změna	k1gFnPc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Následující	následující	k2eAgInSc1d1
přehled	přehled	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
nejdůležitější	důležitý	k2eAgFnPc4d3
stavby	stavba	k1gFnPc4
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgInPc6,k3yRgInPc6,k3yIgInPc6
se	se	k3xPyFc4
Guadí	Guadí	k1gNnSc2
podílel	podílet	k5eAaImAgMnS
<g/>
;	;	kIx,
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
kompletní	kompletní	k2eAgInSc4d1
seznam	seznam	k1gInSc4
architektovy	architektův	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedm	sedm	k4xCc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
staveb	stavba	k1gFnPc2
figuruje	figurovat	k5eAaImIp3nS
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s>
Casa	Casa	k1gFnSc1
Vicens	Vicensa	k1gFnPc2
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Casa	Casa	k1gFnSc1
El	Ela	k1gFnPc2
Capricho	Capricha	k1gFnSc5
<g/>
,	,	kIx,
Comillas	Comillas	k1gMnSc1
u	u	k7c2
Santanderu	Santander	k1gInSc2
</s>
<s>
Colegio	Colegia	k1gFnSc5
Teresiano	Teresiana	k1gFnSc5
</s>
<s>
Casa	Casa	k1gMnSc1
Calvet	Calvet	k1gMnSc1
</s>
<s>
Krypta	krypta	k1gFnSc1
v	v	k7c6
Colò	Colò	k1gFnSc6
Güell	Güella	k1gFnPc2
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bellesguard	Bellesguard	k1gMnSc1
</s>
<s>
Güellův	Güellův	k2eAgInSc1d1
park	park	k1gInSc1
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Casa	Cas	k2eAgFnSc1d1
Batlló	Batlló	k1gFnSc1
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Casa	Cas	k2eAgFnSc1d1
Milà	Milà	k1gFnSc1
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sagrada	Sagrada	k1gFnSc1
Família	Família	k1gFnSc1
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Palác	palác	k1gInSc1
Güell	Güell	k1gInSc1
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Brána	brána	k1gFnSc1
usedlosti	usedlost	k1gFnSc2
Finca	Finca	k1gMnSc1
Miralles	Miralles	k1gMnSc1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
(	(	kIx(
<g/>
Palma	palma	k1gFnSc1
de	de	k?
Mallorca	Mallorca	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
spolupráce	spolupráce	k1gFnSc2
při	při	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
</s>
<s>
Biskupský	biskupský	k2eAgInSc1d1
palác	palác	k1gInSc1
v	v	k7c4
Astorze	Astorze	k1gFnPc4
</s>
<s>
Casa	Casa	k1gFnSc1
de	de	k?
los	los	k1gInSc1
Botines	Botines	k1gInSc1
</s>
<s>
Güellovy	Güellův	k2eAgInPc4d1
vinné	vinný	k2eAgInPc4d1
sklepy	sklep	k1gInPc4
</s>
<s>
Sagrada	Sagrada	k1gFnSc1
Familia	Familia	k1gFnSc1
</s>
<s>
Palau	Palau	k6eAd1
Güell	Güell	k1gInSc1
</s>
<s>
Casa	Casa	k1gFnSc1
Vicens	Vicensa	k1gFnPc2
</s>
<s>
El	Ela	k1gFnPc2
Capricho	Capricha	k1gFnSc5
</s>
<s>
Güellův	Güellův	k2eAgInSc1d1
park	park	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Budovy	budova	k1gFnPc4
chráněné	chráněný	k2eAgFnPc4d1
UNESCEM	Unesco	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
;	;	kIx,
MYSLIVEČKOVÁ	Myslivečková	k1gFnSc1
<g/>
,	,	kIx,
Olga	Olga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přemožitelé	přemožitel	k1gMnPc1
času	čas	k1gInSc2
sv.	sv.	kA
13	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
novinářů	novinář	k1gMnPc2
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Antoni	Anton	k1gMnPc1
Gaudi	Gaud	k1gMnPc1
<g/>
,	,	kIx,
s.	s.	k?
104	#num#	k4
<g/>
-	-	kIx~
<g/>
107	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
La	la	k1gNnSc2
Sagrada	Sagrada	k1gFnSc1
Familia	Familia	k1gFnSc1
de	de	k?
Barcelona	Barcelona	k1gFnSc1
ultima	ultimo	k1gNnSc2
los	los	k1gMnSc1
preparativos	preparativos	k1gMnSc1
para	para	k1gFnSc1
su	su	k?
apertura	apertura	k1gFnSc1
al	ala	k1gFnPc2
culto	culto	k1gNnSc1
<g/>
.	.	kIx.
lavozdigital	lavozdigital	k1gMnSc1
<g/>
.	.	kIx.
<g/>
es	es	k1gNnSc1
<g/>
.	.	kIx.
www.lavozdigital.es	www.lavozdigital.es	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Blázen	blázen	k1gMnSc1
nebo	nebo	k8xC
génius	génius	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Antoni	Antoň	k1gFnSc6
Gaudí	Gaudí	k1gNnSc4
<g/>
,	,	kIx,
mistr	mistr	k1gMnSc1
katalánského	katalánský	k2eAgInSc2d1
modernismu	modernismus	k1gInSc2
<g/>
,	,	kIx,
by	by	kYmCp3nS
oslavil	oslavit	k5eAaPmAgInS
115	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.radiovaticana.cz/clanek.php4?id=13569	http://www.radiovaticana.cz/clanek.php4?id=13569	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
VYKOUPIL	vykoupit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecce	Ecc	k1gInSc2
homo	homo	k6eAd1
:	:	kIx,
z	z	k7c2
rozhlasových	rozhlasový	k2eAgInPc2d1
fejetonů	fejeton	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Julius	Julius	k1gMnSc1
Zirkus	Zirkus	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
312	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
903377	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZERBST	ZERBST	kA
<g/>
,	,	kIx,
Rainer	Rainer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antoni	Antoň	k1gFnSc6
Gaudí	Gaudí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Miroslav	Miroslav	k1gMnSc1
Veselý	Veselý	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Benedikt	benedikt	k1gInSc1
Taschen	Taschen	k1gInSc1
/	/	kIx~
Vydavateľstvo	Vydavateľstvo	k1gNnSc1
Slovart	Slovarta	k1gFnPc2
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
8228	#num#	k4
<g/>
-	-	kIx~
<g/>
9715	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Antoni	Anton	k1gMnPc1
Gaudí	Gaudí	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Antoni	Anton	k1gMnPc1
Gaudí	Gaudí	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Antoni	Anton	k1gMnPc1
Gaudí	Gaudí	k1gNnPc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Antoni	Anton	k1gMnPc1
Gaudí	Gaudí	k1gNnSc2
(	(	kIx(
<g/>
ve	v	k7c6
všech	všecek	k3xTgInPc6
úředních	úřední	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
EU	EU	kA
<g/>
)	)	kIx)
</s>
<s>
Antoni	Anton	k1gMnPc1
Gaudí	Gaudí	k1gNnPc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
Lexis	Lexis	k1gFnSc2
Education	Education	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Výročí	výročí	k1gNnSc1
týdne	týden	k1gInSc2
(	(	kIx(
<g/>
ČRo	ČRo	k1gFnSc1
3	#num#	k4
–	–	k?
Vltava	Vltava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Antonio	Antonio	k1gMnSc1
Gaudí	Gaudí	k1gNnSc4
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
(	(	kIx(
<g/>
audio	audio	k2eAgFnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Antoni	Anton	k1gMnPc1
Gaudi	Gaud	k1gMnPc1
<g/>
,	,	kIx,
architekt	architekt	k1gMnSc1
</s>
<s>
A.	A.	kA
<g/>
Gaudi-dílo-video	Gaudi-dílo-video	k6eAd1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6450	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119009692	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2095	#num#	k4
5718	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79079077	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500014514	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
9855586	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79079077	#num#	k4
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
