<p>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
(	(	kIx(	(
<g/>
CNS	CNS	kA	CNS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgFnSc1d1	ústřední
část	část	k1gFnSc1	část
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
periferními	periferní	k2eAgInPc7d1	periferní
nervy	nerv	k1gInPc7	nerv
hraje	hrát	k5eAaImIp3nS	hrát
ústřední	ústřední	k2eAgFnSc3d1	ústřední
roli	role	k1gFnSc3	role
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
jejich	jejich	k3xOp3gNnSc2	jejich
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
míchy	mícha	k1gFnSc2	mícha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochranné	ochranný	k2eAgInPc1d1	ochranný
obaly	obal	k1gInPc1	obal
==	==	k?	==
</s>
</p>
<p>
<s>
Mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
třemi	tři	k4xCgFnPc7	tři
plenami	plena	k1gFnPc7	plena
–	–	k?	–
blánami	blána	k1gFnPc7	blána
čili	čili	k8xC	čili
meningy	meninga	k1gFnSc2	meninga
<g/>
.	.	kIx.	.
</s>
<s>
Mícha	mícha	k1gFnSc1	mícha
je	být	k5eAaImIp3nS	být
obalena	obalit	k5eAaPmNgFnS	obalit
pouze	pouze	k6eAd1	pouze
plenou	plena	k1gFnSc7	plena
míšní	míšní	k2eAgFnSc7d1	míšní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
plena	plena	k1gFnSc1	plena
(	(	kIx(	(
<g/>
dura	dur	k2eAgFnSc1d1	dura
mater	mater	k1gFnSc1	mater
<g/>
)	)	kIx)	)
–	–	k?	–
Vnější	vnější	k2eAgInSc4d1	vnější
obal	obal	k1gInSc4	obal
CNS	CNS	kA	CNS
<g/>
,	,	kIx,	,
v	v	k7c6	v
lebce	lebka	k1gFnSc6	lebka
nasedá	nasedat	k5eAaImIp3nS	nasedat
ke	k	k7c3	k
kosti	kost	k1gFnSc3	kost
a	a	k8xC	a
v	v	k7c6	v
páteři	páteř	k1gFnSc6	páteř
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
durální	durální	k2eAgInSc4d1	durální
vak	vak	k1gInSc4	vak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pavučnice	pavučnice	k1gFnSc1	pavučnice
(	(	kIx(	(
<g/>
Arachnoidea	Arachnoidea	k1gFnSc1	Arachnoidea
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Omozečnice	omozečnice	k1gFnSc1	omozečnice
<g/>
,	,	kIx,	,
měkká	měkký	k2eAgFnSc1d1	měkká
plena	plena	k1gFnSc1	plena
mozková	mozkový	k2eAgFnSc1d1	mozková
(	(	kIx(	(
<g/>
pia	pia	k?	pia
mater	mater	k1gFnSc1	mater
<g/>
)	)	kIx)	)
-	-	kIx~	-
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
pavoučnicí	pavoučnice	k1gFnSc7	pavoučnice
je	být	k5eAaImIp3nS	být
mozkomíšní	mozkomíšní	k2eAgNnSc1d1	mozkomíšní
mokZánět	mokZánět	k5eAaImF	mokZánět
mozkových	mozkový	k2eAgInPc2d1	mozkový
obalů	obal	k1gInPc2	obal
(	(	kIx(	(
<g/>
meningoencefalitida	meningoencefalitida	k1gFnSc1	meningoencefalitida
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
např.	např.	kA	např.
infikovaným	infikovaný	k2eAgNnSc7d1	infikované
klíštětem	klíště	k1gNnSc7	klíště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mícha	mícha	k1gFnSc1	mícha
==	==	k?	==
</s>
</p>
<p>
<s>
Mícha	mícha	k1gFnSc1	mícha
(	(	kIx(	(
<g/>
mícha	mícha	k1gFnSc1	mícha
páteřní	páteřní	k2eAgFnSc1d1	páteřní
<g/>
,	,	kIx,	,
medulla	medulla	k1gFnSc1	medulla
spinalis	spinalis	k1gFnSc1	spinalis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nervová	nervový	k2eAgFnSc1d1	nervová
trubice	trubice	k1gFnSc1	trubice
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
páteřním	páteřní	k2eAgInSc6d1	páteřní
kanálku	kanálek	k1gInSc6	kanálek
<g/>
,	,	kIx,	,
předává	předávat	k5eAaImIp3nS	předávat
informace	informace	k1gFnPc4	informace
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
informace	informace	k1gFnPc4	informace
orgánům	orgán	k1gMnPc3	orgán
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
<g/>
cm	cm	kA	cm
a	a	k8xC	a
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
31	[number]	k4	31
párů	pár	k1gInPc2	pár
míšních	míšní	k2eAgInPc2d1	míšní
nervů	nerv	k1gInPc2	nerv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Krční	krční	k2eAgFnSc1d1	krční
</s>
</p>
<p>
<s>
Hrudní	hrudní	k2eAgFnSc1d1	hrudní
</s>
</p>
<p>
<s>
Bederní	bederní	k2eAgFnSc1d1	bederní
</s>
</p>
<p>
<s>
Křížové	Křížové	k2eAgFnSc1d1	Křížové
</s>
</p>
<p>
<s>
KostrčníKaždý	KostrčníKaždý	k2eAgInSc1d1	KostrčníKaždý
nerv	nerv	k1gInSc1	nerv
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vlákna	vlákna	k1gFnSc1	vlákna
smyslová	smyslový	k2eAgFnSc1d1	smyslová
<g/>
,	,	kIx,	,
motorická	motorický	k2eAgFnSc1d1	motorická
a	a	k8xC	a
některá	některý	k3yIgFnSc1	některý
vegetativní	vegetativní	k2eAgFnSc1d1	vegetativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
průřezu	průřez	k1gInSc6	průřez
míchou	mícha	k1gFnSc7	mícha
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
bílou	bílý	k2eAgFnSc4d1	bílá
hmotu	hmota	k1gFnSc4	hmota
míšní	míšeň	k1gFnPc2	míšeň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
axony	axon	k1gInPc7	axon
neuronů	neuron	k1gInPc2	neuron
<g/>
,	,	kIx,	,
a	a	k8xC	a
šedou	šedý	k2eAgFnSc4d1	šedá
hmotu	hmota	k1gFnSc4	hmota
míšní	míšeň	k1gFnPc2	míšeň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
těly	tělo	k1gNnPc7	tělo
neuronů	neuron	k1gInPc2	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c6	na
míše	mícha	k1gFnSc6	mícha
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
přední	přední	k2eAgMnPc1d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgMnPc1d1	zadní
kořeny	kořen	k1gInPc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
(	(	kIx(	(
<g/>
dorzální	dorzální	k2eAgFnSc1d1	dorzální
<g/>
)	)	kIx)	)
kterými	který	k3yRgInPc7	který
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
vlákna	vlákna	k1gFnSc1	vlákna
smyslových	smyslový	k2eAgInPc2d1	smyslový
neuronů	neuron	k1gInPc2	neuron
přivádějících	přivádějící	k2eAgInPc2d1	přivádějící
informaci	informace	k1gFnSc4	informace
(	(	kIx(	(
<g/>
vzruchy	vzruch	k1gInPc4	vzruch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
interneuronu	interneuron	k1gInSc2	interneuron
je	být	k5eAaImIp3nS	být
informace	informace	k1gFnSc1	informace
vyhodnocena	vyhodnotit	k5eAaPmNgFnS	vyhodnotit
a	a	k8xC	a
předána	předat	k5eAaPmNgFnS	předat
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
motorické	motorický	k2eAgInPc4d1	motorický
neurony	neuron	k1gInPc4	neuron
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
předními	přední	k2eAgFnPc7d1	přední
(	(	kIx(	(
<g/>
ventrálními	ventrální	k2eAgInPc7d1	ventrální
<g/>
)	)	kIx)	)
kořeny	kořen	k1gInPc7	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
zpracování	zpracování	k1gNnSc1	zpracování
podnětu	podnět	k1gInSc2	podnět
provedeno	provést	k5eAaPmNgNnS	provést
bez	bez	k7c2	bez
zásahu	zásah	k1gInSc2	zásah
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
děj	děj	k1gInSc1	děj
reflex	reflex	k1gInSc4	reflex
(	(	kIx(	(
<g/>
také	také	k9	také
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podnět	podnět	k1gInSc1	podnět
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
po	po	k7c6	po
reflexním	reflexní	k2eAgInSc6d1	reflexní
oblouku	oblouk	k1gInSc6	oblouk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
vlákna	vlákna	k1gFnSc1	vlákna
předních	přední	k2eAgInPc2d1	přední
a	a	k8xC	a
zadních	zadní	k2eAgInPc2d1	zadní
kořenů	kořen	k1gInPc2	kořen
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c4	v
míšní	míšní	k2eAgInSc4d1	míšní
nerv	nerv	k1gInSc4	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
míšních	míšní	k2eAgInPc2d1	míšní
nervů	nerv	k1gInPc2	nerv
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
poruchám	porucha	k1gFnPc3	porucha
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
buď	buď	k8xC	buď
částečné	částečný	k2eAgNnSc4d1	částečné
ochrnuti	ochrnout	k5eAaPmNgMnP	ochrnout
(	(	kIx(	(
<g/>
pareza	pareza	k1gFnSc1	pareza
-	-	kIx~	-
spastická	spastický	k2eAgFnSc1d1	spastická
nebo	nebo	k8xC	nebo
chabá	chabý	k2eAgFnSc1d1	chabá
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
k	k	k7c3	k
ochrnutí	ochrnutí	k1gNnSc3	ochrnutí
úplnému	úplný	k2eAgInSc3d1	úplný
(	(	kIx(	(
<g/>
plegie	plegie	k1gFnSc2	plegie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mozek	mozek	k1gInSc4	mozek
==	==	k?	==
</s>
</p>
<p>
<s>
Mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
encephalon	encephalon	k1gInSc1	encephalon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
zadní	zadní	k2eAgFnPc1d1	zadní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnPc1d1	střední
(	(	kIx(	(
<g/>
mesencephalon	mesencephalon	k1gInSc1	mesencephalon
<g/>
)	)	kIx)	)
a	a	k8xC	a
přední	přední	k2eAgMnSc1d1	přední
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
plní	plnit	k5eAaImIp3nS	plnit
některé	některý	k3yIgFnPc4	některý
základní	základní	k2eAgFnPc4d1	základní
funkce	funkce	k1gFnPc4	funkce
jako	jako	k8xC	jako
mícha	mícha	k1gFnSc1	mícha
=	=	kIx~	=
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
vstupní	vstupní	k2eAgInSc4d1	vstupní
signál	signál	k1gInSc4	signál
ze	z	k7c2	z
smyslových	smyslový	k2eAgInPc2d1	smyslový
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
výstupní	výstupní	k2eAgInPc4d1	výstupní
impulsy	impuls	k1gInPc4	impuls
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vysílá	vysílat	k5eAaImIp3nS	vysílat
k	k	k7c3	k
výkonným	výkonný	k2eAgInPc3d1	výkonný
orgánům	orgán	k1gInPc3	orgán
soustředěným	soustředěný	k2eAgInPc3d1	soustředěný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hlavy	hlava	k1gFnSc2	hlava
zejména	zejména	k9	zejména
dutiny	dutina	k1gFnPc1	dutina
ústní	ústní	k2eAgFnPc1d1	ústní
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
reflexy	reflex	k1gInPc4	reflex
=	=	kIx~	=
svaly	sval	k1gInPc4	sval
obličeje	obličej	k1gInSc2	obličej
a	a	k8xC	a
slinné	slinný	k2eAgFnSc2d1	slinná
žlázy	žláza	k1gFnSc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
významnější	významný	k2eAgFnSc7d2	významnější
funkcí	funkce	k1gFnSc7	funkce
mozku	mozek	k1gInSc2	mozek
je	být	k5eAaImIp3nS	být
integrace	integrace	k1gFnSc1	integrace
a	a	k8xC	a
koordinace	koordinace	k1gFnSc1	koordinace
ostatních	ostatní	k2eAgInPc2d1	ostatní
orgánů	orgán	k1gInPc2	orgán
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anatomicky	anatomicky	k6eAd1	anatomicky
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Přední	přední	k2eAgInSc1d1	přední
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
prosencephalon	prosencephalon	k1gInSc1	prosencephalon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
mezimozek	mezimozek	k1gInSc1	mezimozek
(	(	kIx(	(
<g/>
diencephalon	diencephalon	k1gInSc1	diencephalon
<g/>
)	)	kIx)	)
a	a	k8xC	a
koncový	koncový	k2eAgInSc4d1	koncový
mozek	mozek	k1gInSc4	mozek
(	(	kIx(	(
<g/>
telencephalon	telencephalon	k1gInSc4	telencephalon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střední	střední	k2eAgInSc1d1	střední
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
mesencephalon	mesencephalon	k1gInSc1	mesencephalon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
nedělí	dělit	k5eNaImIp3nS	dělit
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Zadní	zadní	k2eAgInSc1d1	zadní
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
rhombencephalon	rhombencephalon	k1gInSc1	rhombencephalon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
kaudálnější	kaudální	k2eAgFnPc1d2	kaudální
prodloužené	prodloužený	k2eAgFnPc1d1	prodloužená
míchy	mícha	k1gFnPc1	mícha
(	(	kIx(	(
<g/>
medulla	medulla	k1gFnSc1	medulla
oblongata	oblongata	k1gFnSc1	oblongata
<g/>
)	)	kIx)	)
a	a	k8xC	a
rostrálnějšího	rostrální	k2eAgInSc2d2	rostrální
metencephalonu	metencephalon	k1gInSc2	metencephalon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Varolův	Varolův	k2eAgInSc4d1	Varolův
most	most	k1gInSc4	most
(	(	kIx(	(
<g/>
pons	pons	k6eAd1	pons
<g/>
)	)	kIx)	)
a	a	k8xC	a
mozeček	mozeček	k1gInSc1	mozeček
(	(	kIx(	(
<g/>
cerebellum	cerebellum	k1gInSc1	cerebellum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Varolův	Varolův	k2eAgInSc1d1	Varolův
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
mícha	mícha	k1gFnSc1	mícha
a	a	k8xC	a
střední	střední	k2eAgInSc1d1	střední
mozek	mozek	k1gInSc1	mozek
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nS	tvořit
mozkový	mozkový	k2eAgInSc1d1	mozkový
kmen	kmen	k1gInSc1	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SINĚLNIKOV	SINĚLNIKOV	kA	SINĚLNIKOV
<g/>
,	,	kIx,	,
R.	R.	kA	R.
D.	D.	kA	D.
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
anatomie	anatomie	k1gFnSc2	anatomie
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zdravotnické	zdravotnický	k2eAgNnSc1d1	zdravotnické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
400	[number]	k4	400
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
centrální	centrální	k2eAgFnSc1d1	centrální
nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
CNS	CNS	kA	CNS
-	-	kIx~	-
Anamneza	Anamnez	k1gMnSc2	Anamnez
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
