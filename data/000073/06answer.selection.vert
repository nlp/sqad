<s>
Velára	velára	k1gFnSc1	velára
<g/>
,	,	kIx,	,
též	též	k9	též
zadopatrová	zadopatrový	k2eAgFnSc1d1	zadopatrový
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
řeči	řeč	k1gFnSc6	řeč
tvořen	tvořit	k5eAaImNgMnS	tvořit
přiblížením	přiblížení	k1gNnSc7	přiblížení
nebo	nebo	k8xC	nebo
kontaktem	kontakt	k1gInSc7	kontakt
hřbetu	hřbet	k1gInSc2	hřbet
jazyka	jazyk	k1gInSc2	jazyk
s	s	k7c7	s
měkkým	měkký	k2eAgInSc7d1	měkký
(	(	kIx(	(
<g/>
zadním	zadní	k2eAgMnSc7d1	zadní
<g/>
)	)	kIx)	)
patrem	patro	k1gNnSc7	patro
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
palatum	palatum	k1gNnSc1	palatum
velum	velum	k1gNnSc1	velum
=	=	kIx~	=
měkké	měkký	k2eAgNnSc1d1	měkké
patro	patro	k1gNnSc1	patro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
