<s>
Laos	Laos	k1gInSc1	Laos
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Laoská	laoský	k2eAgFnSc1d1	laoská
lidově	lidově	k6eAd1	lidově
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Zadní	zadní	k2eAgFnSc2d1	zadní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
