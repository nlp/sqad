<s>
Latina	latina	k1gFnSc1	latina
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
nominativ	nominativ	k1gInSc1	nominativ
pád	pád	k1gInSc1	pád
genitiv	genitiv	k1gInSc1	genitiv
pád	pád	k1gInSc1	pád
dativ	dativ	k1gInSc1	dativ
pád	pád	k1gInSc1	pád
akuzativ	akuzativ	k1gInSc1	akuzativ
pád	pád	k1gInSc1	pád
vokativ	vokativ	k1gInSc1	vokativ
pád	pád	k1gInSc1	pád
ablativ	ablativ	k1gInSc1	ablativ
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Latinské	latinský	k2eAgNnSc1d1	latinské
skloňování	skloňování	k1gNnSc1	skloňování
<g/>
.	.	kIx.	.
</s>
