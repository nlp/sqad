<s>
Heyrieux	Heyrieux	k1gInSc1
</s>
<s>
Heyrieux	Heyrieux	k1gInSc1
radnice	radnice	k1gFnSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
37	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
5	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
259-381	259-381	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
region	region	k1gInSc1
</s>
<s>
Auvergne-Rhône-Alpes	Auvergne-Rhône-Alpes	k1gInSc1
departement	departement	k1gInSc1
</s>
<s>
Isè	Isè	k1gMnSc5
arrondissement	arrondissement	k1gMnSc1
</s>
<s>
Vienne	Viennout	k5eAaPmIp3nS,k5eAaImIp3nS
kanton	kanton	k1gInSc1
</s>
<s>
Heyrieux	Heyrieux	k1gInSc1
</s>
<s>
Heyrieux	Heyrieux	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
13,95	13,95	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
4	#num#	k4
758	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
341,1	341,1	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.mairie-heyrieux.fr	www.mairie-heyrieux.fr	k1gMnSc1
PSČ	PSČ	kA
</s>
<s>
38540	#num#	k4
INSEE	INSEE	kA
</s>
<s>
38189	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Heyrieux	Heyrieux	k1gInSc1
je	být	k5eAaImIp3nS
francouzská	francouzský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
v	v	k7c6
departementu	departement	k1gInSc6
Isè	Isè	k1gInSc1
v	v	k7c6
regionu	region	k1gInSc6
Auvergne-Rhône-Alpes	Auvergne-Rhône-Alpes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
4	#num#	k4
758	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
centrem	centrum	k1gNnSc7
kantonu	kanton	k1gInSc2
Heyrieux	Heyrieux	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1
obce	obec	k1gFnPc1
<g/>
:	:	kIx,
Grenay	Grenaa	k1gFnPc1
<g/>
,	,	kIx,
Saint-Quentin	Saint-Quentin	k1gMnSc1
Fallavier	Fallavier	k1gMnSc1
<g/>
,	,	kIx,
Diémoz	Diémoz	k1gMnSc1
<g/>
,	,	kIx,
Valencin	Valencin	k1gMnSc1
a	a	k8xC
Saint-Pierre-de-Chandieu	Saint-Pierre-de-Chandiea	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
departementu	departement	k1gInSc6
Isè	Isè	k1gMnSc5
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INSEE	INSEE	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4363004-2	4363004-2	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
235201838	#num#	k4
</s>
