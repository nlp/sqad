<s>
Princ	princ	k1gMnSc1	princ
je	být	k5eAaImIp3nS	být
titul	titul	k1gInSc4	titul
označující	označující	k2eAgFnSc2d1	označující
mužské	mužský	k2eAgFnSc2d1	mužská
potomky	potomek	k1gMnPc4	potomek
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
velkovévody	velkovévoda	k1gMnSc2	velkovévoda
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
<g/>
,	,	kIx,	,
knížete	kníže	k1gMnSc2	kníže
a	a	k8xC	a
lantkraběte	lantkrabě	k1gMnSc2	lantkrabě
<g/>
.	.	kIx.	.
</s>
