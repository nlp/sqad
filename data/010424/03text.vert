<p>
<s>
Princ	princ	k1gMnSc1	princ
je	být	k5eAaImIp3nS	být
titul	titul	k1gInSc4	titul
označující	označující	k2eAgFnSc2d1	označující
mužské	mužský	k2eAgFnSc2d1	mužská
potomky	potomek	k1gMnPc4	potomek
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
velkovévody	velkovévoda	k1gMnSc2	velkovévoda
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
<g/>
,	,	kIx,	,
knížete	kníže	k1gMnSc2	kníže
a	a	k8xC	a
lantkraběte	lantkrabě	k1gMnSc2	lantkrabě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
princ	princ	k1gMnSc1	princ
<g/>
"	"	kIx"	"
vztahovat	vztahovat	k5eAaImF	vztahovat
i	i	k9	i
na	na	k7c4	na
syny	syn	k1gMnPc4	syn
hraběte	hrabě	k1gMnSc2	hrabě
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
rod	rod	k1gInSc4	rod
samostatně	samostatně	k6eAd1	samostatně
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ženu	žena	k1gFnSc4	žena
stejného	stejný	k2eAgInSc2d1	stejný
stupně	stupeň	k1gInSc2	stupeň
jako	jako	k8xC	jako
princ	princ	k1gMnSc1	princ
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
titulu	titul	k1gInSc2	titul
princezna	princezna	k1gFnSc1	princezna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
princ	princ	k1gMnSc1	princ
<g/>
/	/	kIx~	/
<g/>
princezna	princezna	k1gFnSc1	princezna
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
potomky	potomek	k1gMnPc4	potomek
panovnického	panovnický	k2eAgInSc2d1	panovnický
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
princem	princ	k1gMnSc7	princ
či	či	k8xC	či
princeznou	princezna	k1gFnSc7	princezna
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
děti	dítě	k1gFnPc1	dítě
panovníka	panovník	k1gMnSc2	panovník
nebo	nebo	k8xC	nebo
panovnice	panovnice	k1gFnSc2	panovnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
jeho	jeho	k3xOp3gNnSc1	jeho
<g/>
/	/	kIx~	/
<g/>
její	její	k3xOp3gMnPc1	její
bratři	bratr	k1gMnPc1	bratr
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
pojetí	pojetí	k1gNnSc6	pojetí
pak	pak	k6eAd1	pak
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
<g/>
/	/	kIx~	/
<g/>
její	její	k3xOp3gFnPc4	její
sestry	sestra	k1gFnPc4	sestra
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
početnosti	početnost	k1gFnSc2	početnost
potomků	potomek	k1gMnPc2	potomek
panovnických	panovnický	k2eAgInPc2d1	panovnický
rodů	rod	k1gInPc2	rod
bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
zvýraznit	zvýraznit	k5eAaPmF	zvýraznit
postavení	postavení	k1gNnSc4	postavení
následníka	následník	k1gMnSc2	následník
úřadujícího	úřadující	k2eAgMnSc2d1	úřadující
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
označení	označení	k1gNnSc1	označení
korunní	korunní	k2eAgMnSc1d1	korunní
princ	princ	k1gMnSc1	princ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
princ	princ	k1gMnSc1	princ
<g/>
/	/	kIx~	/
<g/>
princezna	princezna	k1gFnSc1	princezna
<g/>
"	"	kIx"	"
mnohdy	mnohdy	k6eAd1	mnohdy
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
mladého	mladý	k2eAgNnSc2d1	mladé
a	a	k8xC	a
morálního	morální	k2eAgNnSc2d1	morální
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Neformální	formální	k2eNgInSc1d1	neformální
titul	titul	k1gInSc1	titul
==	==	k?	==
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
princ	princ	k1gMnSc1	princ
<g/>
/	/	kIx~	/
<g/>
princezna	princezna	k1gFnSc1	princezna
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
nebo	nebo	k8xC	nebo
doposud	doposud	k6eAd1	doposud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
používán	používán	k2eAgInSc1d1	používán
jen	jen	k9	jen
jako	jako	k9	jako
neformální	formální	k2eNgNnSc1d1	neformální
označení	označení	k1gNnSc1	označení
členů	člen	k1gInPc2	člen
panovnických	panovnický	k2eAgFnPc2d1	panovnická
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
tímto	tento	k3xDgNnSc7	tento
označením	označení	k1gNnSc7	označení
rozuměli	rozumět	k5eAaImAgMnP	rozumět
členové	člen	k1gMnPc1	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
však	však	k9	však
měli	mít	k5eAaImAgMnP	mít
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
<g/>
,	,	kIx,	,
specifické	specifický	k2eAgInPc4d1	specifický
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
nikoliv	nikoliv	k9	nikoliv
princ	princ	k1gMnSc1	princ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
dauphin	dauphin	k1gMnSc1	dauphin
<g/>
"	"	kIx"	"
–	–	k?	–
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
le	le	k?	le
duc	duc	k0	duc
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Anjou	Anja	k1gMnSc7	Anja
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Anjou	Anja	k1gMnSc7	Anja
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
le	le	k?	le
comte	comte	k5eAaPmIp2nP	comte
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Artois	Artois	k1gFnSc1	Artois
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Artois	Artois	k1gFnSc2	Artois
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
princům	princ	k1gMnPc3	princ
a	a	k8xC	a
princeznám	princezna	k1gFnPc3	princezna
královské	královský	k2eAgFnSc2d1	královská
krve	krev	k1gFnSc2	krev
náleží	náležet	k5eAaImIp3nS	náležet
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
infant	infant	k1gMnSc1	infant
<g/>
/	/	kIx~	/
<g/>
infantka	infantka	k1gFnSc1	infantka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
má	mít	k5eAaImIp3nS	mít
titul	titul	k1gInSc1	titul
kníže	kníže	k1gMnSc1	kníže
<g/>
/	/	kIx~	/
<g/>
kněžna	kněžna	k1gFnSc1	kněžna
z	z	k7c2	z
Asturie	Asturie	k1gFnSc2	Asturie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Habsburské	habsburský	k2eAgNnSc1d1	habsburské
soustátí	soustátí	k1gNnSc1	soustátí
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
habsburském	habsburský	k2eAgNnSc6d1	habsburské
soustátí	soustátí	k1gNnSc6	soustátí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
byly	být	k5eAaImAgFnP	být
i	i	k8xC	i
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
náležel	náležet	k5eAaImAgInS	náležet
synům	syn	k1gMnPc3	syn
římského	římský	k2eAgInSc2d1	římský
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
rakouského	rakouský	k2eAgMnSc4d1	rakouský
císaře	císař	k1gMnSc4	císař
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
Erzherzog	Erzherzog	k1gInSc1	Erzherzog
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
překládán	překládán	k2eAgInSc4d1	překládán
buď	buď	k8xC	buď
jako	jako	k8xC	jako
arcikníže	arcikníže	k1gMnSc1	arcikníže
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
(	(	kIx(	(
<g/>
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
císařství	císařství	k1gNnSc2	císařství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
princ	princ	k1gMnSc1	princ
<g/>
/	/	kIx~	/
<g/>
princezna	princezna	k1gFnSc1	princezna
<g/>
"	"	kIx"	"
náležel	náležet	k5eAaImAgInS	náležet
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
potomkům	potomek	k1gMnPc3	potomek
mediatizovaných	mediatizovaný	k2eAgInPc2d1	mediatizovaný
knížecích	knížecí	k2eAgInPc2d1	knížecí
rodů	rod	k1gInPc2	rod
a	a	k8xC	a
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
mediatizace	mediatizace	k1gFnSc1	mediatizace
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
(	(	kIx(	(
<g/>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
udělené	udělený	k2eAgInPc1d1	udělený
knížecí	knížecí	k2eAgInPc1d1	knížecí
tituly	titul	k1gInPc1	titul
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
spojovány	spojovat	k5eAaImNgInP	spojovat
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
potomci	potomek	k1gMnPc1	potomek
měli	mít	k5eAaImAgMnP	mít
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
hrabě	hrabě	k1gMnSc1	hrabě
<g/>
/	/	kIx~	/
<g/>
hraběnka	hraběnka	k1gFnSc1	hraběnka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
princ	princ	k1gMnSc1	princ
<g/>
/	/	kIx~	/
<g/>
princezna	princezna	k1gFnSc1	princezna
<g/>
"	"	kIx"	"
tak	tak	k6eAd1	tak
náležel	náležet	k5eAaImAgInS	náležet
potomkům	potomek	k1gMnPc3	potomek
a	a	k8xC	a
členům	člen	k1gMnPc3	člen
vybraných	vybraný	k2eAgFnPc2d1	vybraná
knížecích	knížecí	k2eAgFnPc2d1	knížecí
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
hierarchii	hierarchie	k1gFnSc6	hierarchie
zastával	zastávat	k5eAaImAgMnS	zastávat
postavení	postavení	k1gNnSc4	postavení
výše	výše	k1gFnSc2	výše
než	než	k8xS	než
hrabě	hrabě	k1gMnSc1	hrabě
a	a	k8xC	a
níž	nízce	k6eAd2	nízce
než	než	k8xS	než
kníže	kníže	k1gMnSc1	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
==	==	k?	==
</s>
</p>
<p>
<s>
Britský	britský	k2eAgInSc1d1	britský
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
středoevropského	středoevropský	k2eAgNnSc2d1	středoevropské
značně	značně	k6eAd1	značně
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
princ	princ	k1gMnSc1	princ
<g/>
/	/	kIx~	/
<g/>
princezna	princezna	k1gFnSc1	princezna
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
kníže	kníže	k1gMnSc1	kníže
<g/>
/	/	kIx~	/
<g/>
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pojí	pojit	k5eAaImIp3nS	pojit
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
formálně	formálně	k6eAd1	formálně
<g/>
)	)	kIx)	)
s	s	k7c7	s
jistým	jistý	k2eAgNnSc7d1	jisté
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
následníkům	následník	k1gMnPc3	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Fakticky	fakticky	k6eAd1	fakticky
vzato	vzít	k5eAaPmNgNnS	vzít
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
titul	titul	k1gInSc1	titul
princ	princ	k1gMnSc1	princ
Charles	Charles	k1gMnSc1	Charles
nutno	nutno	k6eAd1	nutno
překládat	překládat	k5eAaImF	překládat
jako	jako	k9	jako
kníže	kníže	k1gMnSc1	kníže
Charles	Charles	k1gMnSc1	Charles
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Karel	Karel	k1gMnSc1	Karel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dcera	dcera	k1gFnSc1	dcera
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
královská	královský	k2eAgFnSc1d1	královská
princezna	princezna	k1gFnSc1	princezna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Princess	Princessa	k1gFnPc2	Princessa
Royal	Royal	k1gMnSc1	Royal
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
udělován	udělovat	k5eAaImNgInS	udělovat
nejstarším	starý	k2eAgInSc7d3	nejstarší
dcerám	dcera	k1gFnPc3	dcera
britského	britský	k2eAgMnSc2d1	britský
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Korunní	korunní	k2eAgMnPc1d1	korunní
princové	princ	k1gMnPc1	princ
a	a	k8xC	a
princezny	princezna	k1gFnPc1	princezna
==	==	k?	==
</s>
</p>
<p>
<s>
Korunní	korunní	k2eAgMnSc1d1	korunní
princ	princ	k1gMnSc1	princ
či	či	k8xC	či
princezna	princezna	k1gFnSc1	princezna
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
neformální	formální	k2eNgInSc4d1	neformální
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
označujícího	označující	k2eAgMnSc4d1	označující
následníka	následník	k1gMnSc4	následník
či	či	k8xC	či
následnici	následnice	k1gFnSc4	následnice
trůnu	trůn	k1gInSc2	trůn
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
oficiální	oficiální	k2eAgInSc4d1	oficiální
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Korunním	korunní	k2eAgMnSc7d1	korunní
princem	princ	k1gMnSc7	princ
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Rudolf	Rudolf	k1gMnSc1	Rudolf
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
současný	současný	k2eAgMnSc1d1	současný
britský	britský	k2eAgMnSc1d1	britský
princ	princ	k1gMnSc1	princ
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jeho	jeho	k3xOp3gInSc1	jeho
anglický	anglický	k2eAgInSc1d1	anglický
titul	titul	k1gInSc1	titul
Prince	princ	k1gMnSc2	princ
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
kníže	kníže	k1gMnSc1	kníže
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
Prince	princa	k1gFnSc3	princa
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pohádková	pohádkový	k2eAgFnSc1d1	pohádková
postava	postava	k1gFnSc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Princové	princ	k1gMnPc1	princ
a	a	k8xC	a
princezny	princezna	k1gFnPc1	princezna
jsou	být	k5eAaImIp3nP	být
oblíbenými	oblíbený	k2eAgFnPc7d1	oblíbená
postavami	postava	k1gFnPc7	postava
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
lidové	lidový	k2eAgFnSc2d1	lidová
slovesnosti	slovesnost	k1gFnSc2	slovesnost
<g/>
,	,	kIx,	,
také	také	k9	také
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dost	dost	k6eAd1	dost
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
zejména	zejména	k9	zejména
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
nebo	nebo	k8xC	nebo
v	v	k7c6	v
pověstech	pověst	k1gFnPc6	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
tyto	tento	k3xDgFnPc1	tento
postavy	postava	k1gFnPc1	postava
dostaly	dostat	k5eAaPmAgFnP	dostat
i	i	k9	i
do	do	k7c2	do
moderních	moderní	k2eAgNnPc2d1	moderní
hraných	hraný	k2eAgNnPc2d1	hrané
literárně-dramatických	literárněramatický	k2eAgNnPc2d1	literárně-dramatické
a	a	k8xC	a
audiovizuálních	audiovizuální	k2eAgNnPc2d1	audiovizuální
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
do	do	k7c2	do
filmových	filmový	k2eAgFnPc2d1	filmová
nebo	nebo	k8xC	nebo
televizních	televizní	k2eAgFnPc2d1	televizní
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
princ	princ	k1gMnSc1	princ
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
