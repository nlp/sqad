<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
získávání	získávání	k1gNnSc1	získávání
živin	živina	k1gFnPc2	živina
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
a	a	k8xC	a
králík	králík	k1gMnSc1	králík
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
co	co	k9	co
nejlépe	dobře	k6eAd3	dobře
využil	využít	k5eAaPmAgMnS	využít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
požírá	požírat	k5eAaImIp3nS	požírat
měkké	měkký	k2eAgInPc4d1	měkký
bobky	bobek	k1gInPc4	bobek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
bílkoviny	bílkovina	k1gFnPc4	bílkovina
i	i	k8xC	i
vitamíny	vitamín	k1gInPc4	vitamín
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
.	.	kIx.
</s>
