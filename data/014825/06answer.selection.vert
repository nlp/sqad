<s desamb="1">
Od	od	k7c2
počátku	počátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
tak	tak	k9
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
i	i	k9
s	s	k7c7
inkluzivnější	inkluzivný	k2eAgFnSc7d2
podobou	podoba	k1gFnSc7
této	tento	k3xDgFnSc2
zkratky	zkratka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
tuto	tento	k3xDgFnSc4
rozmanitost	rozmanitost	k1gFnSc4
uvnitř	uvnitř	k7c2
komunity	komunita	k1gFnSc2
zohledňuje	zohledňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
tedy	tedy	k8xC
LGBTQIA	LGBTQIA	kA
zahrnující	zahrnující	k2eAgMnSc1d1
i	i	k8xC
queer	queer	k2gFnPc4
<g/>
,	,	kIx,
intersex	intersex	k2gFnPc4
a	a	k8xC
asexuální	asexuální	k2eAgFnPc4
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>