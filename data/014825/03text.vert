<s>
LGBT	LGBT	kA
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
symbol	symbol	k1gInSc1
LGBT	LGBT	kA
komunity	komunita	k1gFnSc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
</s>
<s>
LGBT	LGBT	kA
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
častěji	často	k6eAd2
také	také	k9
GLBT	GLBT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
označující	označující	k2eAgFnSc2d1
lesby	lesba	k1gFnSc2
<g/>
,	,	kIx,
gaye	gay	k1gMnPc4
<g/>
,	,	kIx,
bisexuály	bisexuál	k1gMnPc4
a	a	k8xC
transgender	transgender	k1gInSc4
osoby	osoba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
Mnohdy	mnohdy	k6eAd1
je	být	k5eAaImIp3nS
však	však	k9
užívána	užívat	k5eAaImNgFnS
k	k	k7c3
souhrnnému	souhrnný	k2eAgNnSc3d1
označení	označení	k1gNnSc3
celého	celý	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
sexuálních	sexuální	k2eAgFnPc2d1
a	a	k8xC
genderových	genderův	k2eAgFnPc2d1
identit	identita	k1gFnPc2
<g/>
,	,	kIx,
objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
proto	proto	k8xC
i	i	k9
varianta	varianta	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
LGBTQIA	LGBTQIA	kA
rovněž	rovněž	k6eAd1
zastřešující	zastřešující	k2eAgInSc4d1
queer	queer	k1gInSc4
<g/>
,	,	kIx,
intersex	intersex	k1gInSc4
a	a	k8xC
asexuální	asexuální	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
případně	případně	k6eAd1
jen	jen	k9
LGBTQ	LGBTQ	kA
nebo	nebo	k8xC
LGBT	LGBT	kA
<g/>
+	+	kIx~
ve	v	k7c6
smyslu	smysl	k1gInSc6
„	„	k?
<g/>
LGBT	LGBT	kA
a	a	k8xC
další	další	k2eAgFnSc2d1
identity	identita	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Užívá	užívat	k5eAaImIp3nS
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
k	k	k7c3
souhrnnému	souhrnný	k2eAgNnSc3d1
označení	označení	k1gNnSc3
komunit	komunita	k1gFnPc2
či	či	k8xC
hnutí	hnutí	k1gNnSc2
těchto	tento	k3xDgFnPc2
osob	osoba	k1gFnPc2
a	a	k8xC
termínů	termín	k1gInPc2
s	s	k7c7
nimi	on	k3xPp3gInPc7
souvisejících	související	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
pojmu	pojem	k1gInSc2
</s>
<s>
Období	období	k1gNnSc1
na	na	k7c6
přelomu	přelom	k1gInSc6
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
po	po	k7c6
Stonewallských	Stonewallský	k2eAgInPc6d1
nepokojích	nepokoj	k1gInPc6
v	v	k7c4
newyorské	newyorský	k2eAgInPc4d1
Greenwich	Greenwich	k1gInSc4
Village	Villag	k1gFnSc2
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
počátek	počátek	k1gInSc4
moderního	moderní	k2eAgNnSc2d1
homosexuálního	homosexuální	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vymezení	vymezení	k1gNnSc1
této	tento	k3xDgFnSc2
menšiny	menšina	k1gFnSc2
či	či	k8xC
komunity	komunita	k1gFnSc2
jako	jako	k8xS,k8xC
„	„	k?
<g/>
homosexuální	homosexuální	k2eAgInSc1d1
<g/>
“	“	k?
však	však	k9
bývá	bývat	k5eAaImIp3nS
vnímáno	vnímán	k2eAgNnSc4d1
jako	jako	k8xS,k8xC
zjednodušující	zjednodušující	k2eAgNnSc4d1
a	a	k8xC
zpochybňováno	zpochybňován	k2eAgNnSc4d1
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
marginalizuje	marginalizovat	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
další	další	k2eAgFnPc4d1
sexuální	sexuální	k2eAgFnPc4d1
menšiny	menšina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
sdílejí	sdílet	k5eAaImIp3nP
podobné	podobný	k2eAgFnPc4d1
charakteristiky	charakteristika	k1gFnPc4
či	či	k8xC
osudy	osud	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Např.	např.	kA
výbor	výbor	k1gInSc1
pro	pro	k7c4
gay	gay	k1gMnSc1
a	a	k8xC
lesbické	lesbický	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
Americké	americký	k2eAgFnSc2d1
psychologické	psychologický	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
doporučil	doporučit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
autorům	autor	k1gMnPc3
odborných	odborný	k2eAgInPc2d1
textů	text	k1gInPc2
přistupovat	přistupovat	k5eAaImF
k	k	k7c3
termínu	termín	k1gInSc3
homosexualita	homosexualita	k1gFnSc1
a	a	k8xC
z	z	k7c2
něj	on	k3xPp3gInSc2
odvozených	odvozený	k2eAgInPc2d1
výrazů	výraz	k1gInPc2
jako	jako	k8xC,k8xS
k	k	k7c3
zastaralým	zastaralý	k2eAgFnPc3d1
a	a	k8xC
neadekvátním	adekvátní	k2eNgNnSc6d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
postupnou	postupný	k2eAgFnSc7d1
emancipací	emancipace	k1gFnSc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
používat	používat	k5eAaImF
sousloví	sousloví	k1gNnSc1
„	„	k?
<g/>
gay	gay	k1gMnSc1
a	a	k8xC
lesbický	lesbický	k2eAgInSc1d1
<g/>
“	“	k?
a	a	k8xC
od	od	k7c2
něj	on	k3xPp3gInSc2
odvozená	odvozený	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
GL	GL	kA
(	(	kIx(
<g/>
též	též	k9
G	G	kA
<g/>
/	/	kIx~
<g/>
L	L	kA
nebo	nebo	k8xC
G	G	kA
<g/>
&	&	k?
<g/>
L	L	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
pozdních	pozdní	k2eAgInPc6d1
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
doplněná	doplněná	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
B	B	kA
pro	pro	k7c4
bisexuální	bisexuální	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
oživením	oživení	k1gNnSc7
veřejného	veřejný	k2eAgNnSc2d1
angažmá	angažmá	k1gNnSc2
lidí	člověk	k1gMnPc2
s	s	k7c7
transgender	transgendra	k1gFnPc2
identitou	identita	k1gFnSc7
(	(	kIx(
<g/>
trans	trans	k1gInSc1
lidí	člověk	k1gMnPc2
<g/>
)	)	kIx)
na	na	k7c6
přelomu	přelom	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
zkratka	zkratka	k1gFnSc1
rozšířena	rozšířit	k5eAaPmNgFnS
ještě	ještě	k9
o	o	k7c4
písmeno	písmeno	k1gNnSc4
T.	T.	kA
Přibližně	přibližně	k6eAd1
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
navíc	navíc	k6eAd1
písmeno	písmeno	k1gNnSc1
L	L	kA
začalo	začít	k5eAaPmAgNnS
více	hodně	k6eAd2
objevovat	objevovat	k5eAaImF
na	na	k7c6
prvním	první	k4xOgNnSc6
místě	místo	k1gNnSc6
zkratky	zkratka	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
větším	veliký	k2eAgNnSc7d2
prosazováním	prosazování	k1gNnSc7
lesbické	lesbický	k2eAgFnSc2d1
části	část	k1gFnSc2
hnutí	hnutí	k1gNnSc2
a	a	k8xC
feministického	feministický	k2eAgInSc2d1
proudu	proud	k1gInSc2
v	v	k7c6
ní	on	k3xPp3gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Vznikla	vzniknout	k5eAaPmAgFnS
tak	tak	k6eAd1
obecně	obecně	k6eAd1
užívaná	užívaný	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
LGBT	LGBT	kA
<g/>
.	.	kIx.
</s>
<s>
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
mnohdy	mnohdy	k6eAd1
užívána	užívat	k5eAaImNgFnS
k	k	k7c3
pojmenování	pojmenování	k1gNnSc3
celého	celý	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
neheterosexuálních	heterosexuální	k2eNgFnPc2d1
a	a	k8xC
necisgenderových	cisgenderův	k2eNgFnPc2d1
identit	identita	k1gFnPc2
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
výlučně	výlučně	k6eAd1
leseb	lesba	k1gFnPc2
<g/>
,	,	kIx,
gayů	gay	k1gMnPc2
<g/>
,	,	kIx,
bisexuálních	bisexuální	k2eAgInPc2d1
a	a	k8xC
trans	trans	k1gInSc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
počátku	počátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
tak	tak	k9
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
i	i	k9
s	s	k7c7
inkluzivnější	inkluzivný	k2eAgFnSc7d2
podobou	podoba	k1gFnSc7
této	tento	k3xDgFnSc2
zkratky	zkratka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
tuto	tento	k3xDgFnSc4
rozmanitost	rozmanitost	k1gFnSc4
uvnitř	uvnitř	k7c2
komunity	komunita	k1gFnSc2
zohledňuje	zohledňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
tedy	tedy	k8xC
LGBTQIA	LGBTQIA	kA
zahrnující	zahrnující	k2eAgMnSc1d1
i	i	k8xC
queer	queer	k2gFnPc4
<g/>
,	,	kIx,
intersex	intersex	k2gFnPc4
a	a	k8xC
asexuální	asexuální	k2eAgFnPc4
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
případně	případně	k6eAd1
jen	jen	k9
LGBTQ	LGBTQ	kA
nebo	nebo	k8xC
LGBT	LGBT	kA
<g/>
+	+	kIx~
ve	v	k7c6
smyslu	smysl	k1gInSc6
„	„	k?
<g/>
LGBT	LGBT	kA
a	a	k8xC
další	další	k2eAgFnSc2d1
identity	identita	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
zkratkového	zkratkový	k2eAgNnSc2d1
pojmenování	pojmenování	k1gNnSc2
lze	lze	k6eAd1
využít	využít	k5eAaPmF
i	i	k9
samostatně	samostatně	k6eAd1
stojícího	stojící	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
queer	quera	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
jednoduše	jednoduše	k6eAd1
označuje	označovat	k5eAaImIp3nS
všechny	všechen	k3xTgMnPc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
nejsou	být	k5eNaImIp3nP
heterosexuální	heterosexuální	k2eAgFnPc1d1
nebo	nebo	k8xC
cis	cis	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Písmeno	písmeno	k1gNnSc1
Q	Q	kA
též	též	k6eAd1
někdy	někdy	k6eAd1
v	v	k7c6
angličtině	angličtina	k1gFnSc6
představuje	představovat	k5eAaImIp3nS
„	„	k?
<g/>
questioning	questioning	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k9
nejisté	jistý	k2eNgFnPc1d1
<g/>
,	,	kIx,
nevyhraněné	vyhraněný	k2eNgFnPc1d1
osoby	osoba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
svou	svůj	k3xOyFgFnSc4
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
nebo	nebo	k8xC
genderovou	genderový	k2eAgFnSc4d1
identitu	identita	k1gFnSc4
teprve	teprve	k6eAd1
objevují	objevovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevují	objevovat	k5eAaImIp3nP
se	se	k3xPyFc4
i	i	k9
varianty	varianta	k1gFnPc1
zahrnující	zahrnující	k2eAgFnPc1d1
písmena	písmeno	k1gNnPc4
P	P	kA
pro	pro	k7c4
„	„	k?
<g/>
pansexuály	pansexuála	k1gFnPc4
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
polyamoriky	polyamorika	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
SA	SA	kA
pro	pro	k7c4
„	„	k?
<g/>
straight	straight	k1gMnSc1
allies	allies	k1gMnSc1
<g/>
“	“	k?
či	či	k8xC
F	F	kA
pro	pro	k7c4
„	„	k?
<g/>
fetish	fetish	k1gInSc4
<g/>
“	“	k?
nebo	nebo	k8xC
K	K	kA
pro	pro	k7c4
„	„	k?
<g/>
kinks	kinks	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
třeba	třeba	k9
komunitu	komunita	k1gFnSc4
lidí	člověk	k1gMnPc2
vyznávajících	vyznávající	k2eAgInPc2d1
BDSM	BDSM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
zkratky	zkratka	k1gFnPc1
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
uplatňovány	uplatňován	k2eAgFnPc1d1
i	i	k8xC
zpětně	zpětně	k6eAd1
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
LGBT	LGBT	kA
hnutí	hnutí	k1gNnSc1
nepoužívalo	používat	k5eNaImAgNnS
nebo	nebo	k8xC
samo	sám	k3xTgNnSc1
ještě	ještě	k9
ani	ani	k8xC
neexistovalo	existovat	k5eNaImAgNnS
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
někdy	někdy	k6eAd1
bývá	bývat	k5eAaImIp3nS
předmětem	předmět	k1gInSc7
kritiky	kritika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
LGBT	LGBT	kA
práva	právo	k1gNnPc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
v	v	k7c6
amerických	americký	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
v	v	k7c6
Asii	Asie	k1gFnSc6
a	a	k8xC
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
a	a	k8xC
Oceánii	Oceánie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Postupem	postup	k1gInSc7
času	čas	k1gInSc2
se	se	k3xPyFc4
jednotlivé	jednotlivý	k2eAgFnPc1d1
komunity	komunita	k1gFnPc1
začaly	začít	k5eAaPmAgFnP
snažit	snažit	k5eAaImF
prosadit	prosadit	k5eAaPmF
svá	svůj	k3xOyFgNnPc4
rovná	rovný	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
každoročně	každoročně	k6eAd1
koná	konat	k5eAaImIp3nS
festival	festival	k1gInSc1
Prague	Praguus	k1gMnSc5
Pride	Prid	k1gMnSc5
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
příslušníci	příslušník	k1gMnPc1
LGBT	LGBT	kA
komunity	komunita	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gMnPc2
podporovatelé	podporovatel	k1gMnPc1
setkávají	setkávat	k5eAaImIp3nP
a	a	k8xC
diskutují	diskutovat	k5eAaImIp3nP
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
o	o	k7c6
stavu	stav	k1gInSc6
práv	právo	k1gNnPc2
a	a	k8xC
situaci	situace	k1gFnSc3
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
pridu	prid	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
se	s	k7c7
průvody	průvod	k1gInPc7
hrdosti	hrdost	k1gFnSc2
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
akce	akce	k1gFnPc1
konají	konat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
dalších	další	k2eAgNnPc6d1
českých	český	k2eAgNnPc6d1
městech	město	k1gNnPc6
(	(	kIx(
<g/>
více	hodně	k6eAd2
zde	zde	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
politickém	politický	k2eAgNnSc6d1
spektru	spektrum	k1gNnSc6
se	se	k3xPyFc4
podporou	podpora	k1gFnSc7
LGBT	LGBT	kA
práv	právo	k1gNnPc2
vyznačují	vyznačovat	k5eAaImIp3nP
zejména	zejména	k9
liberální	liberální	k2eAgMnPc1d1
a	a	k8xC
progresivní	progresivní	k2eAgFnPc1d1
levicové	levicový	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
směrem	směr	k1gInSc7
doprava	doprava	k6eAd1
blíže	blíž	k1gFnSc2
konzervatismu	konzervatismus	k1gInSc2
podpora	podpora	k1gFnSc1
klesá	klesat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Transsexuálové	transsexuál	k1gMnPc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
transgender	transgender	k1gInSc4
osoby	osoba	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
zdrojích	zdroj	k1gInPc6
jsou	být	k5eAaImIp3nP
vedle	vedle	k7c2
nich	on	k3xPp3gMnPc2
jmenováni	jmenovat	k5eAaBmNgMnP,k5eAaImNgMnP
zvlášť	zvlášť	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Programové	programový	k2eAgInPc1d1
prohlášení	prohlášení	k1gNnSc4
Výboru	výbor	k1gInSc2
pro	pro	k7c4
sexuální	sexuální	k2eAgFnPc4d1
menšiny	menšina	k1gFnPc4
<g/>
,	,	kIx,
Rada	rada	k1gFnSc1
vlády	vláda	k1gFnSc2
pro	pro	k7c4
lidská	lidský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
20091	#num#	k4
2	#num#	k4
3	#num#	k4
GOLD	GOLD	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
ABCs	ABCsa	k1gFnPc2
of	of	k?
L.G.B.T.Q.I.A.	L.G.B.T.Q.I.A.	k1gFnPc2
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
GRINBERG	GRINBERG	kA
<g/>
,	,	kIx,
Emanuella	Emanuello	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
What	What	k1gInSc1
the	the	k?
'	'	kIx"
<g/>
Q	Q	kA
<g/>
'	'	kIx"
in	in	k?
LGBTQ	LGBTQ	kA
stands	stands	k1gInSc1
for	forum	k1gNnPc2
<g/>
,	,	kIx,
and	and	k?
other	other	k1gInSc1
identity	identita	k1gFnSc2
terms	termsa	k1gFnPc2
explained	explained	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Analýza	analýza	k1gFnSc1
situace	situace	k1gFnSc2
lesbické	lesbický	k2eAgFnSc2d1
<g/>
,	,	kIx,
gay	gay	k1gMnSc1
<g/>
,	,	kIx,
bisexuální	bisexuální	k2eAgMnSc1d1
a	a	k8xC
transgender	transgender	k1gInSc1
menšiny	menšina	k1gFnSc2
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
Pracovní	pracovní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
pro	pro	k7c4
otázky	otázka	k1gFnPc4
menšin	menšina	k1gFnPc2
ministryně	ministryně	k1gFnSc1
pro	pro	k7c4
lidská	lidský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
a	a	k8xC
národnostní	národnostní	k2eAgFnPc4d1
menšiny	menšina	k1gFnPc4
Džamily	Džamily	k1gFnSc1
Stehlíkové	Stehlíkové	k2eAgFnSc1d1
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
7	#num#	k4
a	a	k8xC
násl	násl	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
American	American	k1gInSc1
Psychological	Psychological	k1gMnSc1
Association	Association	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avoiding	Avoiding	k1gInSc1
Heterosexual	Heterosexual	k1gMnSc1
Bias	Biasa	k1gFnPc2
in	in	k?
Language	language	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
Psychologist	Psychologist	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říjen	říjen	k1gInSc1
1991	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
46	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
973	#num#	k4
<g/>
-	-	kIx~
<g/>
974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Michaela	Michaela	k1gFnSc1
Appeltová	Appeltová	k1gFnSc1
<g/>
:	:	kIx,
Povinnosti	povinnost	k1gFnPc1
máme	mít	k5eAaImIp1nP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
načase	načase	k6eAd1
získat	získat	k5eAaPmF
práva	právo	k1gNnPc4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Gita	Gita	k1gFnSc1
<g/>
,	,	kIx,
genderová	genderová	k1gFnSc1
tisková	tiskový	k2eAgFnSc1d1
a	a	k8xC
informační	informační	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
František	František	k1gMnSc1
Kůst	Kůst	k1gMnSc1
<g/>
:	:	kIx,
Queer	Queer	k1gMnSc1
<g/>
,	,	kIx,
queer	queer	k1gMnSc1
theory	theora	k1gFnSc2
<g/>
,	,	kIx,
queer	queer	k1gInSc4
cinema	cinemum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revue	revue	k1gFnSc1
pro	pro	k7c4
média	médium	k1gNnPc4
č.	č.	k?
9	#num#	k4
<g/>
,	,	kIx,
září	září	k1gNnSc2
2004	#num#	k4
<g/>
↑	↑	k?
PADILLA	PADILLA	kA
<g/>
,	,	kIx,
Yolanda	Yolando	k1gNnSc2
C.	C.	kA
Gay	gay	k1gMnSc1
and	and	k?
Lesbian	Lesbian	k1gInSc1
Rights	Rights	k1gInSc1
Organizing	Organizing	k1gInSc1
<g/>
:	:	kIx,
Community-based	Community-based	k1gMnSc1
Strategies	Strategies	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Harrington	Harrington	k1gInSc1
Park	park	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
235	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781560232759	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SWIGONSKI	SWIGONSKI	kA
<g/>
,	,	kIx,
Mary	Mary	k1gFnSc1
E.	E.	kA
<g/>
,	,	kIx,
Robin	robin	k2eAgMnSc1d1
S.	S.	kA
Mama	mama	k1gFnSc1
<g/>
,	,	kIx,
Kelly	Kella	k1gFnPc1
Ward	Ward	k1gMnSc1
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
Shepard	Shepard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
From	From	k1gInSc1
Hate	Hat	k1gFnSc2
Crimes	Crimesa	k1gFnPc2
to	ten	k3xDgNnSc1
Human	Human	k1gInSc4
Rights	Rights	k1gInSc4
<g/>
:	:	kIx,
A	a	k9
Tribute	tribut	k1gInSc5
to	ten	k3xDgNnSc4
Matthew	Matthew	k1gMnSc1
Shepard	Shepard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Haworth	Haworth	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
56023	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vyberte	vybrat	k5eAaPmRp2nP
si	se	k3xPyFc3
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
o	o	k7c4
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
uvažujete	uvažovat	k5eAaImIp2nP
<g/>
,	,	kIx,
a	a	k8xC
zjistíte	zjistit	k5eAaPmIp2nP
detaily	detail	k1gInPc1
jejího	její	k3xOp3gNnSc2
hodnocení	hodnocení	k1gNnSc2
a	a	k8xC
její	její	k3xOp3gMnPc4
férové	férový	k2eAgMnPc4d1
kandidáty	kandidát	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsme	být	k5eAaImIp1nP
fér	fér	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koalice	koalice	k1gFnSc1
za	za	k7c4
manželství	manželství	k1gNnPc4
<g/>
,	,	kIx,
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc4
LGBT	LGBT	kA
organizací	organizace	k1gFnSc7
</s>
<s>
LGBT	LGBT	kA
slogany	slogan	k1gInPc1
</s>
<s>
LGBT	LGBT	kA
práva	právo	k1gNnPc4
podle	podle	k7c2
zemí	zem	k1gFnPc2
a	a	k8xC
teritorií	teritorium	k1gNnPc2
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
LGBT	LGBT	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
LGBT	LGBT	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Olga	Olga	k1gFnSc1
Pechová	Pechová	k1gFnSc1
<g/>
:	:	kIx,
LGBT	LGBT	kA
mládež	mládež	k1gFnSc1
a	a	k8xC
diskriminace	diskriminace	k1gFnSc1
<g/>
,	,	kIx,
In	In	k1gFnSc1
Radka	Radka	k1gFnSc1
Neumannová	Neumannová	k1gFnSc1
a	a	k8xC
Martina	Martina	k1gFnSc1
Kykalová	Kykalová	k1gFnSc1
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Antidiskriminační	Antidiskriminační	k2eAgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
a	a	k8xC
veřejná	veřejný	k2eAgFnSc1d1
správa	správa	k1gFnSc1
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Multikulturní	multikulturní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
50	#num#	k4
<g/>
–	–	k?
<g/>
53	#num#	k4
</s>
<s>
Analýza	analýza	k1gFnSc1
situace	situace	k1gFnSc2
lesbické	lesbický	k2eAgFnSc2d1
<g/>
,	,	kIx,
gay	gay	k1gMnSc1
<g/>
,	,	kIx,
bisexuální	bisexuální	k2eAgMnSc1d1
a	a	k8xC
transgender	transgender	k1gInSc1
menšiny	menšina	k1gFnSc2
v	v	k7c6
ČR	ČR	kA
vydaná	vydaný	k2eAgFnSc1d1
Úřadem	úřad	k1gInSc7
vlády	vláda	k1gFnSc2
v	v	k7c6
říjnu	říjen	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sexualita	sexualita	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7705503-2	7705503-2	k4
</s>
