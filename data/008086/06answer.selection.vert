<s>
První	první	k4xOgFnSc1	první
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Amundsenova	Amundsenův	k2eAgFnSc1d1	Amundsenova
expedice	expedice	k1gFnSc1	expedice
vedená	vedený	k2eAgFnSc1d1	vedená
norským	norský	k2eAgMnSc7d1	norský
polárníkem	polárník	k1gMnSc7	polárník
Roaldem	Roald	k1gMnSc7	Roald
Amundsenem	Amundsen	k1gMnSc7	Amundsen
<g/>
.	.	kIx.	.
</s>
