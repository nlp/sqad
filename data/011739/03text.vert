<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gInSc1	Pinus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
stálezelených	stálezelený	k2eAgInPc2d1	stálezelený
jehličnatých	jehličnatý	k2eAgInPc2d1	jehličnatý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
borovicovité	borovicovitý	k2eAgFnSc2d1	borovicovitý
(	(	kIx(	(
<g/>
Pinaceae	Pinacea	k1gFnSc2	Pinacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zhruba	zhruba	k6eAd1	zhruba
110	[number]	k4	110
druhy	druh	k1gInPc4	druh
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
rod	rod	k1gInSc4	rod
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
zástupci	zástupce	k1gMnPc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
borovicovitých	borovicovitý	k2eAgFnPc2d1	borovicovitý
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
Pinus	Pinus	k1gMnSc1	Pinus
merkusii	merkusie	k1gFnSc4	merkusie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
velikýː	velikýː	k?	velikýː
rostou	růst	k5eAaImIp3nP	růst
od	od	k7c2	od
oblasti	oblast	k1gFnSc2	oblast
severských	severský	k2eAgInPc2d1	severský
lesů	les	k1gInPc2	les
přes	přes	k7c4	přes
mírný	mírný	k2eAgInSc4d1	mírný
pás	pás	k1gInSc4	pás
a	a	k8xC	a
subtropy	subtropy	k1gInPc4	subtropy
až	až	k9	až
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
tropů	trop	k1gMnPc2	trop
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
až	až	k9	až
k	k	k7c3	k
horní	horní	k2eAgFnSc3d1	horní
hranici	hranice	k1gFnSc3	hranice
lesa	les	k1gInSc2	les
či	či	k8xC	či
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
maxima	maxima	k1gFnSc1	maxima
druhového	druhový	k2eAgNnSc2d1	druhové
bohatství	bohatství	k1gNnSc2	bohatství
ovšem	ovšem	k9	ovšem
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc7d1	další
bohatou	bohatý	k2eAgFnSc7d1	bohatá
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
východní	východní	k2eAgFnSc1d1	východní
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Indočína	Indočína	k1gFnSc1	Indočína
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
Himálaje	Himálaj	k1gFnSc2	Himálaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
původních	původní	k2eAgInPc2d1	původní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
dvoujehličných	dvoujehličný	k2eAgFnPc2d1	dvoujehličný
a	a	k8xC	a
3	[number]	k4	3
pětijehličné	pětijehličný	k2eAgInPc1d1	pětijehličný
<g/>
.	.	kIx.	.
</s>
<s>
Vesměs	vesměs	k6eAd1	vesměs
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
silně	silně	k6eAd1	silně
světlomilné	světlomilný	k2eAgMnPc4d1	světlomilný
<g/>
,	,	kIx,	,
konkurenčně	konkurenčně	k6eAd1	konkurenčně
slabé	slabý	k2eAgInPc4d1	slabý
stromy	strom	k1gInPc4	strom
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
malými	malý	k2eAgInPc7d1	malý
nároky	nárok	k1gInPc7	nárok
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
dobře	dobře	k6eAd1	dobře
snášet	snášet	k5eAaImF	snášet
nejrůznější	různý	k2eAgNnSc4d3	nejrůznější
stanoviště	stanoviště	k1gNnSc4	stanoviště
<g/>
;	;	kIx,	;
mnohé	mnohé	k1gNnSc1	mnohé
plní	plnit	k5eAaImIp3nS	plnit
roli	role	k1gFnSc4	role
pionýrských	pionýrský	k2eAgFnPc2d1	Pionýrská
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
severoamerické	severoamerický	k2eAgInPc1d1	severoamerický
i	i	k8xC	i
středomořské	středomořský	k2eAgInPc1d1	středomořský
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
adaptovány	adaptován	k2eAgInPc1d1	adaptován
na	na	k7c4	na
lesní	lesní	k2eAgInPc4d1	lesní
požáry	požár	k1gInPc4	požár
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
již	již	k6eAd1	již
fosilní	fosilní	k2eAgInPc1d1	fosilní
nálezy	nález	k1gInPc1	nález
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
borovice	borovice	k1gFnPc4	borovice
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
nejstarší	starý	k2eAgInPc1d3	nejstarší
žijící	žijící	k2eAgInPc1d1	žijící
vyšší	vysoký	k2eAgInPc1d2	vyšší
organismy	organismus	k1gInPc1	organismus
na	na	k7c6	na
Zemiː	Zemiː	k1gFnSc6	Zemiː
některé	některý	k3yIgInPc1	některý
exempláře	exemplář	k1gInPc1	exemplář
borovice	borovice	k1gFnSc2	borovice
dlouhověké	dlouhověký	k2eAgInPc1d1	dlouhověký
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gInSc1	Pinus
longaeva	longaevo	k1gNnSc2	longaevo
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
staré	starý	k2eAgMnPc4d1	starý
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
800	[number]	k4	800
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
taxonomicky	taxonomicky	k6eAd1	taxonomicky
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
podrodyː	podrodyː	k?	podrodyː
Pinus	Pinus	k1gInSc4	Pinus
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
borovice	borovice	k1gFnPc1	borovice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
5	[number]	k4	5
nebo	nebo	k8xC	nebo
až	až	k9	až
8	[number]	k4	8
jehlicemi	jehlice	k1gFnPc7	jehlice
ve	v	k7c6	v
svazečku	svazeček	k1gInSc6	svazeček
s	s	k7c7	s
vytrvalou	vytrvalý	k2eAgFnSc7d1	vytrvalá
pochvou	pochva	k1gFnSc7	pochva
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgInPc7	dva
cévními	cévní	k2eAgInPc7d1	cévní
svazky	svazek	k1gInPc7	svazek
<g/>
,	,	kIx,	,
průduchy	průduch	k1gInPc1	průduch
rozloženými	rozložený	k2eAgMnPc7d1	rozložený
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
stranách	strana	k1gFnPc6	strana
jehlice	jehlice	k1gFnSc1	jehlice
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
hřbetních	hřbetní	k2eAgFnPc6d1	hřbetní
šupinách	šupina	k1gFnPc6	šupina
šišek	šiška	k1gFnPc2	šiška
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
"	"	kIx"	"
<g/>
pupkem	pupek	k1gInSc7	pupek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
umbo	umbo	k6eAd1	umbo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Strobus	Strobus	k1gMnSc1	Strobus
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
měkké	měkký	k2eAgFnPc4d1	měkká
<g/>
"	"	kIx"	"
borovice	borovice	k1gFnPc4	borovice
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
3	[number]	k4	3
nebo	nebo	k8xC	nebo
5	[number]	k4	5
jehlicemi	jehlice	k1gFnPc7	jehlice
ve	v	k7c6	v
svazečku	svazeček	k1gInSc6	svazeček
s	s	k7c7	s
opadavou	opadavý	k2eAgFnSc7d1	opadavá
pochvou	pochva	k1gFnSc7	pochva
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
cévním	cévní	k2eAgInSc7d1	cévní
svazkem	svazek	k1gInSc7	svazek
<g/>
,	,	kIx,	,
průduchy	průduch	k1gInPc1	průduch
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
jehlice	jehlice	k1gFnSc2	jehlice
a	a	k8xC	a
pupkem	pupek	k1gInSc7	pupek
(	(	kIx(	(
<g/>
umbo	umbo	k6eAd1	umbo
<g/>
)	)	kIx)	)
umístěným	umístěný	k2eAgMnSc7d1	umístěný
na	na	k7c6	na
konci	konec	k1gInSc6	konec
semenné	semenný	k2eAgFnSc2d1	semenná
šupiny	šupina	k1gFnSc2	šupina
šišky	šiška	k1gFnSc2	šiška
<g/>
.	.	kIx.	.
</s>
<s>
Evolučně	evolučně	k6eAd1	evolučně
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
spodní	spodní	k2eAgFnSc2d1	spodní
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
jehličnaté	jehličnatý	k2eAgFnPc4d1	jehličnatá
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
<g/>
,	,	kIx,	,
měkké	měkký	k2eAgInPc1d1	měkký
až	až	k9	až
středně	středně	k6eAd1	středně
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
opracovatelné	opracovatelný	k2eAgNnSc4d1	opracovatelné
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
využitelné	využitelný	k2eAgNnSc4d1	využitelné
ke	k	k7c3	k
stavebním	stavební	k2eAgInPc3d1	stavební
účelům	účel	k1gInPc3	účel
i	i	k9	i
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
nábytku	nábytek	k1gInSc2	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
též	též	k9	též
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
množství	množství	k1gNnSc4	množství
aromatických	aromatický	k2eAgFnPc2d1	aromatická
terpenických	terpenický	k2eAgFnPc2d1	terpenický
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
aromaterapii	aromaterapie	k1gFnSc6	aromaterapie
a	a	k8xC	a
kosmetice	kosmetika	k1gFnSc6	kosmetika
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
léčivých	léčivý	k2eAgFnPc6d1	léčivá
mastech	mast	k1gFnPc6	mast
při	při	k7c6	při
revmatismu	revmatismus	k1gInSc6	revmatismus
nebo	nebo	k8xC	nebo
artritidě	artritida	k1gFnSc6	artritida
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
také	také	k9	také
zdrojem	zdroj	k1gInSc7	zdroj
přírodního	přírodní	k2eAgInSc2d1	přírodní
terpentýnu	terpentýn	k1gInSc2	terpentýn
<g/>
.	.	kIx.	.
</s>
<s>
Jehličí	jehličí	k1gNnSc1	jehličí
a	a	k8xC	a
kůra	kůra	k1gFnSc1	kůra
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k4c4	mnoho
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
či	či	k8xC	či
stále	stále	k6eAd1	stále
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
využívány	využívat	k5eAaImNgInP	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jedlá	jedlý	k2eAgNnPc1d1	jedlé
semena	semeno	k1gNnPc1	semeno
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
známá	známá	k1gFnSc1	známá
jako	jako	k8xC	jako
piniové	piniový	k2eAgInPc1d1	piniový
či	či	k8xC	či
"	"	kIx"	"
<g/>
cedrové	cedrový	k2eAgInPc4d1	cedrový
<g/>
"	"	kIx"	"
oříšky	oříšek	k1gInPc4	oříšek
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
mají	mít	k5eAaImIp3nP	mít
borovice	borovice	k1gFnPc1	borovice
v	v	k7c6	v
zahradní	zahradní	k2eAgFnSc6d1	zahradní
architektuře	architektura	k1gFnSc6	architektura
jako	jako	k8xC	jako
okrasné	okrasný	k2eAgInPc1d1	okrasný
stromy	strom	k1gInPc1	strom
a	a	k8xC	a
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jako	jako	k9	jako
vánoční	vánoční	k2eAgInPc4d1	vánoční
stromky	stromek	k1gInPc4	stromek
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
druhy	druh	k1gInPc1	druh
borovic	borovice	k1gFnPc2	borovice
hrály	hrát	k5eAaImAgInP	hrát
značnou	značný	k2eAgFnSc4d1	značná
roli	role	k1gFnSc4	role
v	v	k7c6	v
kulturním	kulturní	k2eAgMnSc6d1	kulturní
<g/>
,	,	kIx,	,
folklórním	folklórní	k2eAgMnSc6d1	folklórní
a	a	k8xC	a
spirituálním	spirituální	k2eAgInSc6d1	spirituální
životě	život	k1gInSc6	život
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
nesčetných	sčetný	k2eNgInPc6d1	nesčetný
literárních	literární	k2eAgInPc6d1	literární
a	a	k8xC	a
výtvarných	výtvarný	k2eAgInPc6d1	výtvarný
dílech	díl	k1gInPc6	díl
a	a	k8xC	a
v	v	k7c6	v
heraldice	heraldika	k1gFnSc6	heraldika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
borovice	borovice	k1gFnSc2	borovice
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
původní	původní	k2eAgNnSc1d1	původní
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
borový	borový	k2eAgInSc4d1	borový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
asi	asi	k9	asi
i	i	k9	i
obecně	obecně	k6eAd1	obecně
jehličnatý	jehličnatý	k2eAgInSc1d1	jehličnatý
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
tedy	tedy	k9	tedy
strom	strom	k1gInSc1	strom
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
v	v	k7c6	v
boru	bor	k1gInSc6	bor
(	(	kIx(	(
<g/>
podobnou	podobný	k2eAgFnSc4d1	podobná
etymologii	etymologie	k1gFnSc4	etymologie
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
borůvka	borůvka	k1gFnSc1	borůvka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgInSc1d1	slovní
základ	základ	k1gInSc1	základ
je	být	k5eAaImIp3nS	být
všeslovanský	všeslovanský	k2eAgInSc1d1	všeslovanský
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
<g/>
,	,	kIx,	,
paralely	paralela	k1gFnPc4	paralela
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
v	v	k7c6	v
starogermánských	starogermánský	k2eAgInPc6d1	starogermánský
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
staroanglicky	staroanglicky	k6eAd1	staroanglicky
bearu	beara	k1gFnSc4	beara
–	–	k?	–
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
staroislandsky	staroislandsky	k6eAd1	staroislandsky
bǫ	bǫ	k?	bǫ
–	–	k?	–
strom	strom	k1gInSc1	strom
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
prapůvod	prapůvod	k1gInSc1	prapůvod
zřejmě	zřejmě	k6eAd1	zřejmě
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
indoevropskému	indoevropský	k2eAgMnSc3d1	indoevropský
*	*	kIx~	*
<g/>
bhar-	bhar-	k?	bhar-
"	"	kIx"	"
<g/>
hrot	hrot	k1gInSc1	hrot
<g/>
,	,	kIx,	,
štětina	štětina	k1gFnSc1	štětina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přeneseně	přeneseně	k6eAd1	přeneseně
tedy	tedy	k9	tedy
jehličnatý	jehličnatý	k2eAgInSc4d1	jehličnatý
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Odborným	odborný	k2eAgInSc7d1	odborný
názvem	název	k1gInSc7	název
je	být	k5eAaImIp3nS	být
latinské	latinský	k2eAgNnSc4d1	latinské
slovo	slovo	k1gNnSc4	slovo
pinus	pinus	k1gInSc1	pinus
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
Římané	Říman	k1gMnPc1	Říman
označovali	označovat	k5eAaImAgMnP	označovat
právě	právě	k9	právě
borovice	borovice	k1gFnSc2	borovice
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
základ	základ	k1gInSc4	základ
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
k	k	k7c3	k
praindoevroskému	praindoevroské	k1gNnSc3	praindoevroské
*	*	kIx~	*
<g/>
pī	pī	k?	pī
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
"	"	kIx"	"
<g/>
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
šťávou	šťáva	k1gFnSc7	šťáva
<g/>
"	"	kIx"	"
–	–	k?	–
pryskyřicí	pryskyřice	k1gFnSc7	pryskyřice
–	–	k?	–
souvisí	souviset	k5eAaImIp3nS	souviset
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
též	též	k9	též
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
někdy	někdy	k6eAd1	někdy
užívaný	užívaný	k2eAgInSc1d1	užívaný
název	název	k1gInSc1	název
sosna	sosna	k1gFnSc1	sosna
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
předpokládaný	předpokládaný	k2eAgInSc4d1	předpokládaný
kořen	kořen	k1gInSc4	kořen
*	*	kIx~	*
<g/>
sop-	sop-	k?	sop-
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
např.	např.	kA	např.
také	také	k6eAd1	také
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
francouzském	francouzský	k2eAgInSc6d1	francouzský
názvu	název	k1gInSc6	název
jedle	jedle	k1gFnSc2	jedle
(	(	kIx(	(
<g/>
sapin	sapina	k1gFnPc2	sapina
<g/>
)	)	kIx)	)
či	či	k8xC	či
latinském	latinský	k2eAgInSc6d1	latinský
výrazu	výraz	k1gInSc6	výraz
pro	pro	k7c4	pro
smrk	smrk	k1gInSc4	smrk
(	(	kIx(	(
<g/>
sappinus	sappinus	k1gInSc4	sappinus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Habitus	habitus	k1gInSc1	habitus
a	a	k8xC	a
růst	růst	k1gInSc1	růst
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
borovic	borovice	k1gFnPc2	borovice
je	být	k5eAaImIp3nS	být
stromovitého	stromovitý	k2eAgInSc2d1	stromovitý
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnPc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
do	do	k7c2	do
výšek	výška	k1gFnPc2	výška
přes	přes	k7c4	přes
70	[number]	k4	70
m	m	kA	m
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
Lambertova	Lambertův	k2eAgFnSc1d1	Lambertova
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
)	)	kIx)	)
a	a	k8xC	a
průměru	průměr	k1gInSc6	průměr
kmene	kmen	k1gInSc2	kmen
přes	přes	k7c4	přes
2	[number]	k4	2
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
naopak	naopak	k6eAd1	naopak
habitus	habitus	k1gInSc1	habitus
vysloveně	vysloveně	k6eAd1	vysloveně
keřovitý	keřovitý	k2eAgMnSc1d1	keřovitý
<g/>
,	,	kIx,	,
např.	např.	kA	např.
borovice	borovice	k1gFnSc1	borovice
kleč	kleč	k1gFnSc1	kleč
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
zakrslá	zakrslý	k2eAgFnSc1d1	zakrslá
nebo	nebo	k8xC	nebo
borovice	borovice	k1gFnSc1	borovice
vrcholová	vrcholový	k2eAgFnSc1d1	vrcholová
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
válcovitá	válcovitý	k2eAgNnPc1d1	válcovité
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
kulovitá	kulovitý	k2eAgNnPc1d1	kulovité
<g/>
,	,	kIx,	,
oválná	oválný	k2eAgNnPc1d1	oválné
nebo	nebo	k8xC	nebo
rozprostřená	rozprostřený	k2eAgNnPc1d1	rozprostřené
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
šupinovitá	šupinovitý	k2eAgFnSc1d1	šupinovitá
nebo	nebo	k8xC	nebo
hladká	hladký	k2eAgFnSc1d1	hladká
(	(	kIx(	(
<g/>
u	u	k7c2	u
podrodu	podrod	k1gInSc2	podrod
Strobus	Strobus	k1gInSc1	Strobus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
silnou	silný	k2eAgFnSc4d1	silná
rozbrázděnou	rozbrázděný	k2eAgFnSc4d1	rozbrázděná
borku	borka	k1gFnSc4	borka
<g/>
;	;	kIx,	;
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
Bungeova	Bungeův	k2eAgFnSc1d1	Bungeův
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
kůra	kůra	k1gFnSc1	kůra
i	i	k9	i
v	v	k7c6	v
dospělém	dospělý	k2eAgInSc6d1	dospělý
věku	věk	k1gInSc6	věk
odlupuje	odlupovat	k5eAaImIp3nS	odlupovat
v	v	k7c6	v
plátech	plát	k1gInPc6	plát
jako	jako	k8xS	jako
u	u	k7c2	u
tisu	tis	k1gInSc2	tis
červeného	červený	k2eAgInSc2d1	červený
nebo	nebo	k8xC	nebo
platanu	platan	k1gInSc2	platan
<g/>
.	.	kIx.	.
</s>
<s>
Větvení	větvení	k1gNnSc1	větvení
u	u	k7c2	u
stromových	stromový	k2eAgInPc2d1	stromový
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
monopodiální	monopodiální	k2eAgNnSc1d1	monopodiální
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
průběžným	průběžný	k2eAgInSc7d1	průběžný
kmenem	kmen	k1gInSc7	kmen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovný	rovný	k2eAgInSc4d1	rovný
a	a	k8xC	a
přímý	přímý	k2eAgInSc4d1	přímý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nejrůzněji	různě	k6eAd3	různě
pokroucený	pokroucený	k2eAgMnSc1d1	pokroucený
<g/>
;	;	kIx,	;
větve	větev	k1gFnPc1	větev
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
v	v	k7c6	v
nepravidelných	pravidelný	k2eNgInPc6d1	nepravidelný
pseudopřeslenech	pseudopřeslen	k1gInPc6	pseudopřeslen
<g/>
.	.	kIx.	.
</s>
<s>
Kořenový	kořenový	k2eAgInSc1d1	kořenový
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
kůlovitý	kůlovitý	k2eAgInSc1d1	kůlovitý
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
mělce	mělce	k6eAd1	mělce
kořenící	kořenící	k2eAgFnSc1d1	kořenící
borovice	borovice	k1gFnSc1	borovice
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
hlavním	hlavní	k2eAgInSc7d1	hlavní
kořenem	kořen	k1gInSc7	kořen
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
postranními	postranní	k2eAgInPc7d1	postranní
kořeny	kořen	k1gInPc7	kořen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
sahat	sahat	k5eAaImF	sahat
do	do	k7c2	do
značných	značný	k2eAgFnPc2d1	značná
hloubek	hloubka	k1gFnPc2	hloubka
(	(	kIx(	(
<g/>
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
na	na	k7c6	na
nepříznivých	příznivý	k2eNgInPc6d1	nepříznivý
substrátech	substrát	k1gInPc6	substrát
<g/>
,	,	kIx,	,
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
a	a	k8xC	a
podobných	podobný	k2eAgInPc6d1	podobný
stanovištích	stanoviště	k1gNnPc6	stanoviště
ale	ale	k8xC	ale
až	až	k9	až
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nepříznivých	příznivý	k2eNgNnPc6d1	nepříznivé
stanovištích	stanoviště	k1gNnPc6	stanoviště
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nP	vyvíjet
pouze	pouze	k6eAd1	pouze
podpovrchové	podpovrchový	k2eAgInPc1d1	podpovrchový
postranní	postranní	k2eAgInPc1d1	postranní
kořeny	kořen	k1gInPc1	kořen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
např.	např.	kA	např.
u	u	k7c2	u
borovice	borovice	k1gFnSc2	borovice
těžké	těžký	k2eAgNnSc1d1	těžké
zasahovat	zasahovat	k5eAaImF	zasahovat
až	až	k9	až
46	[number]	k4	46
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Borovice	borovice	k1gFnSc1	borovice
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
dokáží	dokázat	k5eAaPmIp3nP	dokázat
opatřit	opatřit	k5eAaPmF	opatřit
vodu	voda	k1gFnSc4	voda
i	i	k9	i
ze	z	k7c2	z
značně	značně	k6eAd1	značně
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
dřeviny	dřevina	k1gFnPc4	dřevina
nedostupná	dostupný	k2eNgFnSc1d1	nedostupná
<g/>
.	.	kIx.	.
<g/>
Borovice	borovice	k1gFnSc1	borovice
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
dlouho	dlouho	k6eAd1	dlouho
žijící	žijící	k2eAgFnPc4d1	žijící
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
exempláře	exemplář	k1gInPc1	exemplář
borovice	borovice	k1gFnSc2	borovice
dlouhověké	dlouhověký	k2eAgFnSc2d1	dlouhověká
ve	v	k7c6	v
White	Whit	k1gInSc5	Whit
Mountains	Mountains	k1gInSc4	Mountains
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
mají	mít	k5eAaImIp3nP	mít
prokazatelný	prokazatelný	k2eAgInSc4d1	prokazatelný
věk	věk	k1gInSc4	věk
přes	přes	k7c4	přes
4800	[number]	k4	4800
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
činí	činit	k5eAaImIp3nS	činit
nejstarší	starý	k2eAgMnPc4d3	nejstarší
žijící	žijící	k2eAgMnPc4d1	žijící
jedince	jedinec	k1gMnPc4	jedinec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
věk	věk	k1gInSc1	věk
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
topolový	topolový	k2eAgInSc4d1	topolový
porost	porost	k1gInSc4	porost
Pando	panda	k1gFnSc5	panda
v	v	k7c6	v
Utahu	Utah	k1gInSc2	Utah
nebo	nebo	k8xC	nebo
skandinávský	skandinávský	k2eAgInSc4d1	skandinávský
smrk	smrk	k1gInSc4	smrk
Old	Olda	k1gFnPc2	Olda
Tjikko	Tjikko	k1gNnSc1	Tjikko
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obnovující	obnovující	k2eAgMnSc1d1	obnovující
se	s	k7c7	s
klony	klon	k1gInPc7	klon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věk	věk	k1gInSc1	věk
přes	přes	k7c4	přes
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
ze	z	k7c2	z
sekce	sekce	k1gFnSc2	sekce
Balfourianae	Balfouriana	k1gFnSc2	Balfouriana
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
borovice	borovice	k1gFnSc2	borovice
bělokmenné	bělokmenný	k2eAgFnSc2d1	bělokmenný
<g/>
,	,	kIx,	,
ohebné	ohebný	k2eAgFnSc2d1	ohebná
nebo	nebo	k8xC	nebo
u	u	k7c2	u
evropské	evropský	k2eAgFnSc2d1	Evropská
borovice	borovice	k1gFnSc2	borovice
Heldreichovy	Heldreichův	k2eAgFnSc2d1	Heldreichova
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgMnPc2d3	nejstarší
evropských	evropský	k2eAgMnPc2d1	evropský
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
borovic	borovice	k1gFnPc2	borovice
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
věku	věk	k1gInSc2	věk
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jehličí	jehličí	k1gNnSc1	jehličí
a	a	k8xC	a
pupeny	pupen	k1gInPc1	pupen
===	===	k?	===
</s>
</p>
<p>
<s>
Olistění	olistěný	k2eAgMnPc1d1	olistěný
borovic	borovice	k1gFnPc2	borovice
nabývá	nabývat	k5eAaImIp3nS	nabývat
čtyř	čtyři	k4xCgFnPc2	čtyři
různých	různý	k2eAgFnPc2d1	různá
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Semenáčky	semenáček	k1gInPc1	semenáček
klíčí	klíčit	k5eAaImIp3nP	klíčit
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
děložními	děložní	k2eAgInPc7d1	děložní
lístky	lístek	k1gInPc7	lístek
<g/>
;	;	kIx,	;
mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
mají	mít	k5eAaImIp3nP	mít
primární	primární	k2eAgFnSc1d1	primární
<g/>
,	,	kIx,	,
jednotlivě	jednotlivě	k6eAd1	jednotlivě
stojící	stojící	k2eAgFnPc4d1	stojící
jehlice	jehlice	k1gFnPc4	jehlice
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
ještě	ještě	k9	ještě
nikoli	nikoli	k9	nikoli
ve	v	k7c6	v
svazečcích	svazeček	k1gInPc6	svazeček
<g/>
)	)	kIx)	)
umístěné	umístěný	k2eAgFnPc1d1	umístěná
spirálovitě	spirálovitě	k6eAd1	spirálovitě
okolo	okolo	k7c2	okolo
letorostu	letorost	k1gInSc2	letorost
<g/>
.	.	kIx.	.
</s>
<s>
Katafyly	Katafyl	k1gInPc1	Katafyl
(	(	kIx(	(
<g/>
též	též	k9	též
primární	primární	k2eAgFnPc1d1	primární
šupiny	šupina	k1gFnPc1	šupina
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgInPc1d1	drobný
<g/>
,	,	kIx,	,
nefotosyntetizující	fotosyntetizující	k2eNgInPc1d1	fotosyntetizující
kopinaté	kopinatý	k2eAgInPc1d1	kopinatý
útvary	útvar	k1gInPc1	útvar
patrně	patrně	k6eAd1	patrně
listenového	listenový	k2eAgInSc2d1	listenový
původu	původ	k1gInSc2	původ
vyrůstající	vyrůstající	k2eAgFnSc1d1	vyrůstající
na	na	k7c6	na
letorostech	letorost	k1gInPc6	letorost
pod	pod	k7c4	pod
brachyblasty	brachyblast	k1gInPc4	brachyblast
s	s	k7c7	s
jehlicemi	jehlice	k1gFnPc7	jehlice
<g/>
;	;	kIx,	;
u	u	k7c2	u
tzv.	tzv.	kA	tzv.
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
borovic	borovice	k1gFnPc2	borovice
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
vytrvalé	vytrvalý	k2eAgNnSc1d1	vytrvalé
<g/>
,	,	kIx,	,
u	u	k7c2	u
měkkých	měkký	k2eAgFnPc2d1	měkká
borovic	borovice	k1gFnPc2	borovice
naopak	naopak	k6eAd1	naopak
záhy	záhy	k6eAd1	záhy
opadávají	opadávat	k5eAaImIp3nP	opadávat
a	a	k8xC	a
na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
po	po	k7c6	po
nich	on	k3xPp3gNnPc6	on
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
patrné	patrný	k2eAgFnPc4d1	patrná
drobné	drobný	k2eAgFnPc4d1	drobná
jizvy	jizva	k1gFnPc4	jizva
–	–	k?	–
polštářky	polštářek	k1gInPc4	polštářek
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
dospělé	dospělý	k2eAgFnPc1d1	dospělá
jehlice	jehlice	k1gFnPc1	jehlice
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
krátkých	krátký	k2eAgInPc6d1	krátký
výhoncích	výhonek	k1gInPc6	výhonek
(	(	kIx(	(
<g/>
brachyblastech	brachyblast	k1gInPc6	brachyblast
<g/>
)	)	kIx)	)
z	z	k7c2	z
blanité	blanitý	k2eAgFnSc2d1	blanitá
pochvy	pochva	k1gFnSc2	pochva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
opadavá	opadavý	k2eAgFnSc1d1	opadavá
<g/>
,	,	kIx,	,
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
nebo	nebo	k8xC	nebo
růžicovitě	růžicovitě	k6eAd1	růžicovitě
se	se	k3xPyFc4	se
kroutící	kroutící	k2eAgNnPc1d1	kroutící
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
svazečcích	svazeček	k1gInPc6	svazeček
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
po	po	k7c6	po
1	[number]	k4	1
nebo	nebo	k8xC	nebo
po	po	k7c6	po
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Nabývají	nabývat	k5eAaImIp3nP	nabývat
různé	různý	k2eAgFnPc4d1	různá
délky	délka	k1gFnPc4	délka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
cm	cm	kA	cm
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
Banksova	Banksův	k2eAgFnSc1d1	Banksova
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
cm	cm	kA	cm
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
Engelmannova	Engelmannov	k1gInSc2	Engelmannov
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc2	borovice
bahenní	bahenní	k2eAgFnSc2d1	bahenní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
tuhé	tuhý	k2eAgFnPc1d1	tuhá
a	a	k8xC	a
ostře	ostro	k6eAd1	ostro
pichlavé	pichlavý	k2eAgNnSc1d1	pichlavé
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
hedvábně	hedvábně	k6eAd1	hedvábně
měkké	měkký	k2eAgNnSc1d1	měkké
<g/>
,	,	kIx,	,
s	s	k7c7	s
průduchy	průduch	k1gInPc7	průduch
umístěnými	umístěný	k2eAgInPc7d1	umístěný
buď	buď	k8xC	buď
ve	v	k7c6	v
světlejším	světlý	k2eAgInSc6d2	světlejší
proužku	proužek	k1gInSc6	proužek
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
jehlice	jehlice	k1gFnSc2	jehlice
(	(	kIx(	(
<g/>
podrod	podrod	k1gInSc1	podrod
Strobus	Strobus	k1gInSc1	Strobus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
stranách	strana	k1gFnPc6	strana
jehlice	jehlice	k1gFnSc2	jehlice
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
bývají	bývat	k5eAaImIp3nP	bývat
chomáčovitě	chomáčovitě	k6eAd1	chomáčovitě
nahloučeny	nahloučit	k5eAaPmNgInP	nahloučit
na	na	k7c6	na
letorostech	letorost	k1gInPc6	letorost
<g/>
,	,	kIx,	,
svazečky	svazeček	k1gInPc1	svazeček
jehlic	jehlice	k1gFnPc2	jehlice
borovice	borovice	k1gFnSc1	borovice
tuhé	tuhý	k2eAgFnPc1d1	tuhá
však	však	k9	však
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
i	i	k9	i
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
kmeni	kmen	k1gInSc6	kmen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
jehlice	jehlice	k1gFnPc1	jehlice
převislé	převislý	k2eAgFnPc1d1	převislá
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
nápadně	nápadně	k6eAd1	nápadně
např.	např.	kA	např.
u	u	k7c2	u
borovice	borovice	k1gFnSc2	borovice
Lumholtzovy	Lumholtzův	k2eAgFnSc2d1	Lumholtzův
nebo	nebo	k8xC	nebo
kanárské	kanárský	k2eAgFnSc2d1	Kanárská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stromě	strom	k1gInSc6	strom
vytrvávají	vytrvávat	k5eAaImIp3nP	vytrvávat
nejčastěji	často	k6eAd3	často
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
i	i	k9	i
jehlice	jehlice	k1gFnPc4	jehlice
přetrvávající	přetrvávající	k2eAgFnPc4d1	přetrvávající
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	let	k1gInSc1	let
<g/>
.	.	kIx.	.
<g/>
Pupeny	pupen	k1gInPc1	pupen
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
<g/>
,	,	kIx,	,
špičaté	špičatý	k2eAgFnPc1d1	špičatá
<g/>
,	,	kIx,	,
pryskyřičné	pryskyřičný	k2eAgFnPc1d1	pryskyřičná
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
<g/>
,	,	kIx,	,
kryté	krytý	k2eAgFnPc4d1	krytá
šupinami	šupina	k1gFnPc7	šupina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
několika	několik	k4yIc2	několik
málo	málo	k4c1	málo
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc2	borovice
tuhá	tuhý	k2eAgFnSc1d1	tuhá
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
u	u	k7c2	u
rodu	rod	k1gInSc2	rod
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
zmlazovací	zmlazovací	k2eAgFnSc1d1	zmlazovací
schopnost	schopnost	k1gFnSc1	schopnost
<g/>
,	,	kIx,	,
po	po	k7c6	po
poškození	poškození	k1gNnSc6	poškození
nebo	nebo	k8xC	nebo
řezu	řez	k1gInSc6	řez
tedy	tedy	k8xC	tedy
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
regenerovat	regenerovat	k5eAaBmF	regenerovat
<g/>
;	;	kIx,	;
chybějící	chybějící	k2eAgInSc4d1	chybějící
vzrůstný	vzrůstný	k2eAgInSc4d1	vzrůstný
vrchol	vrchol	k1gInSc4	vrchol
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
ohnutými	ohnutý	k2eAgFnPc7d1	ohnutá
bočními	boční	k2eAgFnPc7d1	boční
větvemi	větev	k1gFnPc7	větev
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bajonety	bajonet	k1gInPc1	bajonet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhony	výhon	k1gInPc1	výhon
z	z	k7c2	z
pupenů	pupen	k1gInPc2	pupen
přirůstají	přirůstat	k5eAaImIp3nP	přirůstat
různými	různý	k2eAgFnPc7d1	různá
druhově	druhově	k6eAd1	druhově
specifickými	specifický	k2eAgFnPc7d1	specifická
způsobyː	způsobyː	k?	způsobyː
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
jejich	jejich	k3xOp3gInSc4	jejich
růst	růst	k1gInSc4	růst
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uninodální	uninodální	k2eAgMnSc1d1	uninodální
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jeden	jeden	k4xCgInSc4	jeden
článek	článek	k1gInSc4	článek
(	(	kIx(	(
<g/>
internodium	internodium	k1gNnSc4	internodium
<g/>
)	)	kIx)	)
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
vegetační	vegetační	k2eAgFnSc4d1	vegetační
sezónu	sezóna	k1gFnSc4	sezóna
(	(	kIx(	(
<g/>
ze	z	k7c2	z
známějších	známý	k2eAgInPc2d2	známější
druhů	druh	k1gInPc2	druh
např.	např.	kA	např.
borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
nebo	nebo	k8xC	nebo
černá	černý	k2eAgFnSc1d1	černá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
multinodální	multinodálnit	k5eAaPmIp3nS	multinodálnit
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
více	hodně	k6eAd2	hodně
internodií	internodium	k1gNnPc2	internodium
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
vegetační	vegetační	k2eAgFnSc6d1	vegetační
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
;	;	kIx,	;
druhý	druhý	k4xOgMnSc1	druhý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
několika	několik	k4yIc2	několik
různých	různý	k2eAgInPc2d1	různý
morfologických	morfologický	k2eAgInPc2d1	morfologický
typů	typ	k1gInPc2	typ
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
borovice	borovice	k1gFnSc2	borovice
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
patogenů	patogen	k1gInPc2	patogen
nebo	nebo	k8xC	nebo
různých	různý	k2eAgFnPc2d1	různá
mutací	mutace	k1gFnPc2	mutace
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
vznikat	vznikat	k5eAaImF	vznikat
různé	různý	k2eAgInPc4d1	různý
novotvary	novotvar	k1gInPc4	novotvar
–	–	k?	–
čarověníky	čarověník	k1gInPc4	čarověník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Generativní	generativní	k2eAgInPc1d1	generativní
orgány	orgán	k1gInPc1	orgán
===	===	k?	===
</s>
</p>
<p>
<s>
Samčí	samčí	k2eAgFnPc1d1	samčí
<g/>
,	,	kIx,	,
prašníkové	prašníkový	k2eAgFnPc1d1	prašníkový
šištice	šištice	k1gFnPc1	šištice
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
žluté	žlutý	k2eAgFnPc1d1	žlutá
nebo	nebo	k8xC	nebo
růžové	růžový	k2eAgFnPc1d1	růžová
<g/>
,	,	kIx,	,
nahloučené	nahloučený	k2eAgFnPc1d1	nahloučená
na	na	k7c6	na
letorostech	letorost	k1gInPc6	letorost
v	v	k7c6	v
paždí	paždí	k1gNnSc6	paždí
podpůrných	podpůrný	k2eAgFnPc2d1	podpůrná
šupin	šupina	k1gFnPc2	šupina
brachyblastů	brachyblast	k1gInPc2	brachyblast
<g/>
;	;	kIx,	;
tyčinky	tyčinka	k1gFnPc1	tyčinka
mají	mít	k5eAaImIp3nP	mít
plochou	plochý	k2eAgFnSc4d1	plochá
nitku	nitka	k1gFnSc4	nitka
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
prašná	prašný	k2eAgNnPc4d1	prašné
pouzdra	pouzdro	k1gNnPc4	pouzdro
<g/>
.	.	kIx.	.
</s>
<s>
Pylová	pylový	k2eAgNnPc4d1	pylové
zrna	zrno	k1gNnPc4	zrno
mají	mít	k5eAaImIp3nP	mít
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
váčky	váček	k1gInPc1	váček
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rozšiřována	rozšiřovat	k5eAaImNgFnS	rozšiřovat
větrem	vítr	k1gInSc7	vítr
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Samičí	samičí	k2eAgFnPc1d1	samičí
šištice	šištice	k1gFnPc1	šištice
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
po	po	k7c6	po
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
ojediněle	ojediněle	k6eAd1	ojediněle
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
počtu	počet	k1gInSc6	počet
<g/>
,	,	kIx,	,
na	na	k7c6	na
krátkých	krátký	k2eAgFnPc6d1	krátká
stopkách	stopka	k1gFnPc6	stopka
hned	hned	k6eAd1	hned
pod	pod	k7c7	pod
vrcholovým	vrcholový	k2eAgInSc7d1	vrcholový
pupenem	pupen	k1gInSc7	pupen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opylení	opylení	k1gNnSc6	opylení
upadá	upadat	k5eAaImIp3nS	upadat
pylové	pylový	k2eAgNnSc4d1	pylové
zrno	zrno	k1gNnSc4	zrno
často	často	k6eAd1	často
do	do	k7c2	do
roční	roční	k2eAgFnSc2d1	roční
dormance	dormanka	k1gFnSc3	dormanka
a	a	k8xC	a
samičí	samičí	k2eAgFnSc1d1	samičí
šištice	šištice	k1gFnSc1	šištice
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
konelety	koneleta	k1gFnSc2	koneleta
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgInPc1d1	drobný
útvary	útvar	k1gInPc1	útvar
velikosti	velikost	k1gFnSc3	velikost
zhruba	zhruba	k6eAd1	zhruba
lískového	lískový	k2eAgInSc2d1	lískový
oříšku	oříšek	k1gInSc2	oříšek
na	na	k7c4	na
přímé	přímý	k2eAgNnSc4d1	přímé
nebo	nebo	k8xC	nebo
nazpět	nazpět	k6eAd1	nazpět
ohnuté	ohnutý	k2eAgFnSc6d1	ohnutá
stopce	stopka	k1gFnSc6	stopka
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
druhý	druhý	k4xOgInSc4	druhý
rok	rok	k1gInSc4	rok
pylové	pylový	k2eAgNnSc1d1	pylové
zrnko	zrnko	k1gNnSc1	zrnko
vyklíčí	vyklíčit	k5eAaPmIp3nS	vyklíčit
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
oplození	oplození	k1gNnSc1	oplození
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
konelety	koneleta	k1gFnSc2	koneleta
dalších	další	k2eAgFnPc2d1	další
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
měsíců	měsíc	k1gInPc2	měsíc
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
v	v	k7c4	v
dřevnaté	dřevnatý	k2eAgFnPc4d1	dřevnatá
šišky	šiška	k1gFnPc4	šiška
se	s	k7c7	s
semeny	semeno	k1gNnPc7	semeno
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
dozrání	dozrání	k1gNnSc6	dozrání
většinou	většinou	k6eAd1	většinou
hygroskopicky	hygroskopicky	k6eAd1	hygroskopicky
otevírají	otevírat	k5eAaImIp3nP	otevírat
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
šišky	šiška	k1gFnSc2	šiška
borovice	borovice	k1gFnSc2	borovice
limby	limba	k1gFnSc2	limba
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
opadávají	opadávat	k5eAaImIp3nP	opadávat
neotevřené	otevřený	k2eNgNnSc1d1	neotevřené
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
šišky	šiška	k1gFnSc2	šiška
tzv.	tzv.	kA	tzv.
serotinní	serotinný	k2eAgMnPc1d1	serotinný
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
na	na	k7c6	na
stromě	strom	k1gInSc6	strom
neotevřené	otevřený	k2eNgNnSc1d1	neotevřené
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
a	a	k8xC	a
otevírají	otevírat	k5eAaImIp3nP	otevírat
se	se	k3xPyFc4	se
při	při	k7c6	při
požárech	požár	k1gInPc6	požár
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
leží	ležet	k5eAaImIp3nP	ležet
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
na	na	k7c6	na
semenných	semenný	k2eAgFnPc6d1	semenná
šupinách	šupina	k1gFnPc6	šupina
<g/>
;	;	kIx,	;
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
mají	mít	k5eAaImIp3nP	mít
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
pevně	pevně	k6eAd1	pevně
přirostlé	přirostlý	k2eAgNnSc1d1	přirostlé
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
volně	volně	k6eAd1	volně
objímavé	objímavý	k2eAgNnSc1d1	objímavé
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ale	ale	k8xC	ale
také	také	k9	také
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
pouze	pouze	k6eAd1	pouze
rudimentárně	rudimentárně	k6eAd1	rudimentárně
nebo	nebo	k8xC	nebo
úplně	úplně	k6eAd1	úplně
chybět	chybět	k5eAaImF	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Okřídlená	okřídlený	k2eAgNnPc1d1	okřídlené
semena	semeno	k1gNnPc1	semeno
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
využívají	využívat	k5eAaPmIp3nP	využívat
vítr	vítr	k1gInSc4	vítr
<g/>
,	,	kIx,	,
těžká	těžký	k2eAgNnPc1d1	těžké
a	a	k8xC	a
velká	velký	k2eAgNnPc1d1	velké
bezkřídlá	bezkřídlý	k2eAgNnPc1d1	bezkřídlý
semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
rozšiřována	rozšiřován	k2eAgNnPc1d1	rozšiřováno
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
je	on	k3xPp3gMnPc4	on
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
a	a	k8xC	a
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Klíčí	klíčit	k5eAaImIp3nS	klíčit
většinou	většina	k1gFnSc7	většina
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
však	však	k9	však
přeléhají	přeléhat	k5eAaBmIp3nP	přeléhat
<g/>
.	.	kIx.	.
<g/>
Velikost	velikost	k1gFnSc1	velikost
šišek	šiška	k1gFnPc2	šiška
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
:	:	kIx,	:
od	od	k7c2	od
několika	několik	k4yIc2	několik
cm	cm	kA	cm
až	až	k9	až
po	po	k7c4	po
60	[number]	k4	60
cm	cm	kA	cm
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
Lambertova	Lambertův	k2eAgFnSc1d1	Lambertova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
šišky	šiška	k1gFnSc2	šiška
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
u	u	k7c2	u
borovice	borovice	k1gFnSc2	borovice
Coulterovy	Coulterův	k2eAgFnPc4d1	Coulterův
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
kilogramy	kilogram	k1gInPc7	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
druhů	druh	k1gInPc2	druh
nabývají	nabývat	k5eAaImIp3nP	nabývat
různého	různý	k2eAgMnSc4d1	různý
tvaruː	tvaruː	k?	tvaruː
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
téměř	téměř	k6eAd1	téměř
kulovité	kulovitý	k2eAgFnPc1d1	kulovitá
<g/>
,	,	kIx,	,
vejcovité	vejcovitý	k2eAgFnPc1d1	vejcovitá
<g/>
,	,	kIx,	,
rohlíčkovité	rohlíčkovitý	k2eAgFnPc1d1	rohlíčkovitý
nebo	nebo	k8xC	nebo
dlouze	dlouho	k6eAd1	dlouho
válcovité	válcovitý	k2eAgNnSc1d1	válcovité
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
popisu	popis	k1gInSc3	popis
šišek	šiška	k1gFnPc2	šiška
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
několik	několik	k4yIc4	několik
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
nabývat	nabývat	k5eAaImF	nabývat
různých	různý	k2eAgFnPc2d1	různá
morfologických	morfologický	k2eAgFnPc2d1	morfologická
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
koncové	koncový	k2eAgFnSc6d1	koncová
straně	strana	k1gFnSc6	strana
semenných	semenný	k2eAgFnPc2d1	semenná
šupin	šupina	k1gFnPc2	šupina
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
štítek	štítek	k1gInSc1	štítek
(	(	kIx(	(
<g/>
apofýza	apofýza	k1gFnSc1	apofýza
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
část	část	k1gFnSc4	část
tvořící	tvořící	k2eAgFnSc4d1	tvořící
povrch	povrch	k7c2wR	povrch
zavřené	zavřený	k2eAgFnSc2d1	zavřená
šišky	šiška	k1gFnSc2	šiška
<g/>
;	;	kIx,	;
zhruba	zhruba	k6eAd1	zhruba
uprostřed	uprostřed	k7c2	uprostřed
něho	on	k3xPp3gMnSc2	on
(	(	kIx(	(
<g/>
dorzálně	dorzálně	k6eAd1	dorzálně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
okraji	okraj	k1gInSc6	okraj
(	(	kIx(	(
<g/>
terminálně	terminálně	k6eAd1	terminálně
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
tzv.	tzv.	kA	tzv.
pupek	pupek	k1gInSc1	pupek
(	(	kIx(	(
<g/>
umbo	umbo	k1gNnSc1	umbo
<g/>
,	,	kIx,	,
přírůstek	přírůstek	k1gInSc1	přírůstek
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
opatřen	opatřit	k5eAaPmNgMnS	opatřit
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
osinou	osina	k1gFnSc7	osina
nebo	nebo	k8xC	nebo
pevným	pevný	k2eAgInSc7d1	pevný
a	a	k8xC	a
ostrým	ostrý	k2eAgInSc7d1	ostrý
trnem	trn	k1gInSc7	trn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
ploidie	ploidie	k1gFnSc1	ploidie
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
n	n	k0	n
=	=	kIx~	=
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
a	a	k8xC	a
ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
zástupci	zástupce	k1gMnPc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
borovicovitých	borovicovitý	k2eAgMnPc2d1	borovicovitý
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
Pinus	Pinus	k1gMnSc1	Pinus
merkusii	merkusie	k1gFnSc4	merkusie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
velikýː	velikýː	k?	velikýː
rostou	růst	k5eAaImIp3nP	růst
od	od	k7c2	od
oblasti	oblast	k1gFnSc2	oblast
severských	severský	k2eAgInPc2d1	severský
lesů	les	k1gInPc2	les
přes	přes	k7c4	přes
mírný	mírný	k2eAgInSc4d1	mírný
pás	pás	k1gInSc4	pás
a	a	k8xC	a
subtropy	subtropy	k1gInPc4	subtropy
až	až	k9	až
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
tropů	trop	k1gMnPc2	trop
<g/>
,	,	kIx,	,
od	od	k7c2	od
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
až	až	k9	až
do	do	k7c2	do
výšek	výška	k1gFnPc2	výška
4000	[number]	k4	4000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
tvoří	tvořit	k5eAaImIp3nP	tvořit
dominantu	dominanta	k1gFnSc4	dominanta
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
až	až	k9	až
k	k	k7c3	k
horní	horní	k2eAgFnSc3d1	horní
hranici	hranice	k1gFnSc3	hranice
lesa	les	k1gInSc2	les
nebo	nebo	k8xC	nebo
i	i	k9	i
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Maximum	maximum	k1gNnSc1	maximum
druhového	druhový	k2eAgNnSc2d1	druhové
bohatství	bohatství	k1gNnSc2	bohatství
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rostou	růst	k5eAaImIp3nP	růst
přibližně	přibližně	k6eAd1	přibližně
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
všech	všecek	k3xTgMnPc2	všecek
taxonů	taxon	k1gInPc2	taxon
<g/>
;	;	kIx,	;
další	další	k2eAgFnSc7d1	další
bohatou	bohatý	k2eAgFnSc7d1	bohatá
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
východní	východní	k2eAgFnSc1d1	východní
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Indočína	Indočína	k1gFnSc1	Indočína
a	a	k8xC	a
Himálaj	Himálaj	k1gFnSc1	Himálaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
autochtonních	autochtonní	k2eAgInPc2d1	autochtonní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
dvoujehličných	dvoujehličný	k2eAgFnPc2d1	dvoujehličný
a	a	k8xC	a
3	[number]	k4	3
pětijehličné	pětijehličný	k2eAgInPc1d1	pětijehličný
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
původu	původ	k1gInSc2	původ
taxonu	taxon	k1gInSc2	taxon
je	být	k5eAaImIp3nS	být
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Starý	starý	k2eAgInSc4d1	starý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
dřeviny	dřevina	k1gFnPc1	dřevina
jsou	být	k5eAaImIp3nP	být
borovice	borovice	k1gFnPc4	borovice
pěstovány	pěstován	k2eAgFnPc4d1	pěstována
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
v	v	k7c6	v
plantážních	plantážní	k2eAgFnPc6d1	plantážní
monokulturách	monokultura	k1gFnPc6	monokultura
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
mimo	mimo	k7c4	mimo
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
areál	areál	k1gInSc4	areál
<g/>
,	,	kIx,	,
též	též	k9	též
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
včetně	včetně	k7c2	včetně
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgInP	být
introdukovány	introdukovat	k5eAaBmNgInP	introdukovat
(	(	kIx(	(
<g/>
masivně	masivně	k6eAd1	masivně
např.	např.	kA	např.
borovice	borovice	k1gFnSc1	borovice
paprsčitá	paprsčitý	k2eAgFnSc1d1	paprsčitá
–	–	k?	–
Pinus	Pinus	k1gInSc1	Pinus
radiata	radiata	k1gFnSc1	radiata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
snášejí	snášet	k5eAaImIp3nP	snášet
znečištěné	znečištěný	k2eAgNnSc1d1	znečištěné
ovzduší	ovzduší	k1gNnSc1	ovzduší
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
exhalace	exhalace	k1gFnPc1	exhalace
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
dřevinám	dřevina	k1gFnPc3	dřevina
celosvětově	celosvětově	k6eAd1	celosvětově
často	často	k6eAd1	často
vysazovaným	vysazovaný	k2eAgInSc7d1	vysazovaný
v	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vesměs	vesměs	k6eAd1	vesměs
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
silně	silně	k6eAd1	silně
světlomilné	světlomilný	k2eAgNnSc4d1	světlomilné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
konkurenčně	konkurenčně	k6eAd1	konkurenčně
slabé	slabý	k2eAgFnPc4d1	slabá
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
,	,	kIx,	,
s	s	k7c7	s
dosti	dosti	k6eAd1	dosti
malými	malý	k2eAgInPc7d1	malý
nároky	nárok	k1gInPc7	nárok
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
dobře	dobře	k6eAd1	dobře
snášet	snášet	k5eAaImF	snášet
půdy	půda	k1gFnPc4	půda
chudé	chudý	k2eAgFnPc4d1	chudá
<g/>
,	,	kIx,	,
písčité	písčitý	k2eAgFnPc4d1	písčitá
a	a	k8xC	a
kamenité	kamenitý	k2eAgFnPc4d1	kamenitá
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
exponovaná	exponovaný	k2eAgNnPc4d1	exponované
suchá	suchý	k2eAgNnPc4d1	suché
a	a	k8xC	a
skalnatá	skalnatý	k2eAgNnPc4d1	skalnaté
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
však	však	k9	však
přizpůsobí	přizpůsobit	k5eAaPmIp3nS	přizpůsobit
i	i	k8xC	i
vlhčím	vlhčit	k5eAaImIp1nS	vlhčit
až	až	k6eAd1	až
bažinatým	bažinatý	k2eAgFnPc3d1	bažinatá
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Preferují	preferovat	k5eAaImIp3nP	preferovat
obvykle	obvykle	k6eAd1	obvykle
kyselejší	kyselý	k2eAgFnPc1d2	kyselejší
půdy	půda	k1gFnPc1	půda
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
generalisty	generalista	k1gMnPc4	generalista
schopnými	schopný	k2eAgMnPc7d1	schopný
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
se	se	k3xPyFc4	se
danému	daný	k2eAgInSc3d1	daný
substrátu	substrát	k1gInSc3	substrát
<g/>
;	;	kIx,	;
jen	jen	k9	jen
málo	málo	k4c4	málo
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
vápnomilných	vápnomilný	k2eAgFnPc2d1	vápnomilná
(	(	kIx(	(
<g/>
obligátně	obligátně	k6eAd1	obligátně
borovice	borovice	k1gFnSc1	borovice
Heldreichova	Heldreichův	k2eAgFnSc1d1	Heldreichova
<g/>
,	,	kIx,	,
na	na	k7c6	na
vápenci	vápenec	k1gInSc6	vápenec
dobře	dobře	k6eAd1	dobře
roste	růst	k5eAaImIp3nS	růst
též	též	k9	též
jinak	jinak	k6eAd1	jinak
všestranná	všestranný	k2eAgFnSc1d1	všestranná
borovice	borovice	k1gFnSc1	borovice
černá	černý	k2eAgFnSc1d1	černá
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
schopných	schopný	k2eAgMnPc2d1	schopný
růst	růst	k5eAaImF	růst
na	na	k7c6	na
trvale	trvale	k6eAd1	trvale
zamokřených	zamokřený	k2eAgInPc6d1	zamokřený
substrátech	substrát	k1gInPc6	substrát
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnPc1	borovice
blatka	blatka	k1gFnSc1	blatka
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
kříženci	kříženec	k1gMnPc1	kříženec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
pionýrské	pionýrský	k2eAgFnPc4d1	Pionýrská
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zahajují	zahajovat	k5eAaImIp3nP	zahajovat
sukcesi	sukcese	k1gFnSc4	sukcese
dřevin	dřevina	k1gFnPc2	dřevina
na	na	k7c6	na
různá	různý	k2eAgNnPc1d1	různé
druhotně	druhotně	k6eAd1	druhotně
narušená	narušený	k2eAgNnPc1d1	narušené
stanoviště	stanoviště	k1gNnPc1	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nenáročnosti	nenáročnost	k1gFnSc3	nenáročnost
na	na	k7c4	na
vláhu	vláha	k1gFnSc4	vláha
a	a	k8xC	a
živiny	živina	k1gFnPc4	živina
plní	plnit	k5eAaImIp3nS	plnit
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
zalesňování	zalesňování	k1gNnSc6	zalesňování
neplodných	plodný	k2eNgFnPc2d1	neplodná
půd	půda	k1gFnPc2	půda
<g/>
,	,	kIx,	,
v	v	k7c6	v
půdoochranných	půdoochranný	k2eAgFnPc6d1	půdoochranná
výsadbách	výsadba	k1gFnPc6	výsadba
a	a	k8xC	a
rekultivacích	rekultivace	k1gFnPc6	rekultivace
<g/>
.	.	kIx.	.
<g/>
Druhem	druh	k1gInSc7	druh
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
ekologickou	ekologický	k2eAgFnSc7d1	ekologická
amplitudou	amplituda	k1gFnSc7	amplituda
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
areálem	areál	k1gInSc7	areál
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
coby	coby	k?	coby
pionýrská	pionýrský	k2eAgFnSc1d1	Pionýrská
dřevina	dřevina	k1gFnSc1	dřevina
schopná	schopný	k2eAgFnSc1d1	schopná
osídlit	osídlit	k5eAaPmF	osídlit
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
biotopů	biotop	k1gInPc2	biotop
od	od	k7c2	od
skalních	skalní	k2eAgInPc2d1	skalní
výchozů	výchoz	k1gInPc2	výchoz
přes	přes	k7c4	přes
písčiny	písčina	k1gFnPc4	písčina
<g/>
,	,	kIx,	,
rašeliniště	rašeliniště	k1gNnSc4	rašeliniště
<g/>
,	,	kIx,	,
slatiniště	slatiniště	k1gNnSc4	slatiniště
a	a	k8xC	a
vřesoviště	vřesoviště	k1gNnSc4	vřesoviště
až	až	k9	až
po	po	k7c4	po
hadcové	hadcový	k2eAgNnSc4d1	hadcové
podloží	podloží	k1gNnSc4	podloží
<g/>
;	;	kIx,	;
tvoří	tvořit	k5eAaImIp3nS	tvořit
též	též	k9	též
důležitou	důležitý	k2eAgFnSc4d1	důležitá
součást	součást	k1gFnSc4	součást
skandinávské	skandinávský	k2eAgFnSc2d1	skandinávská
a	a	k8xC	a
západosibiřské	západosibiřský	k2eAgFnSc2d1	Západosibiřská
tajgy	tajga	k1gFnSc2	tajga
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
borovicemi	borovice	k1gFnPc7	borovice
rostoucími	rostoucí	k2eAgFnPc7d1	rostoucí
v	v	k7c6	v
severské	severský	k2eAgFnSc3d1	severská
tajze	tajga	k1gFnSc3	tajga
jsou	být	k5eAaImIp3nP	být
borovice	borovice	k1gFnPc1	borovice
sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
a	a	k8xC	a
zakrslá	zakrslý	k2eAgFnSc1d1	zakrslá
(	(	kIx(	(
<g/>
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
borovice	borovice	k1gFnSc1	borovice
Banksova	Banksův	k2eAgFnSc1d1	Banksova
a	a	k8xC	a
pokroucená	pokroucený	k2eAgFnSc1d1	pokroucená
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Borovice	borovice	k1gFnSc1	borovice
kleč	kleč	k1gFnSc1	kleč
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
druhy	druh	k1gInPc1	druh
tvoří	tvořit	k5eAaImIp3nP	tvořit
porosty	porost	k1gInPc7	porost
nad	nad	k7c7	nad
horní	horní	k2eAgFnSc7d1	horní
hranicí	hranice	k1gFnSc7	hranice
lesa	les	k1gInSc2	les
v	v	k7c6	v
evropských	evropský	k2eAgNnPc6d1	Evropské
pohořích	pohoří	k1gNnPc6	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
jsou	být	k5eAaImIp3nP	být
borovice	borovice	k1gFnSc1	borovice
jako	jako	k8xS	jako
pinie	pinie	k1gFnSc1	pinie
<g/>
,	,	kIx,	,
přímořská	přímořský	k2eAgFnSc1d1	přímořská
nebo	nebo	k8xC	nebo
halepská	halepský	k2eAgFnSc1d1	halepská
součástí	součást	k1gFnSc7	součást
tvrdolisté	tvrdolistý	k2eAgFnSc2d1	tvrdolistý
vegetace	vegetace	k1gFnSc2	vegetace
přizpůsobené	přizpůsobený	k2eAgFnSc2d1	přizpůsobená
suchým	suchý	k2eAgNnPc3d1	suché
a	a	k8xC	a
horkým	horký	k2eAgNnPc3d1	horké
létům	léto	k1gNnPc3	léto
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
borovice	borovice	k1gFnSc2	borovice
v	v	k7c6	v
suchomilné	suchomilný	k2eAgFnSc6d1	suchomilná
vegetaci	vegetace	k1gFnSc6	vegetace
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
severoamerického	severoamerický	k2eAgInSc2d1	severoamerický
západu	západ	k1gInSc2	západ
porůstají	porůstat	k5eAaImIp3nP	porůstat
horské	horský	k2eAgInPc1d1	horský
hřebeny	hřeben	k1gInPc1	hřeben
a	a	k8xC	a
svahy	svah	k1gInPc1	svah
na	na	k7c6	na
méně	málo	k6eAd2	málo
úrodných	úrodný	k2eAgFnPc6d1	úrodná
půdách	půda	k1gFnPc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
lesy	les	k1gInPc1	les
s	s	k7c7	s
uplatněním	uplatnění	k1gNnSc7	uplatnění
borovic	borovice	k1gFnPc2	borovice
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
a	a	k8xC	a
středojih	středojih	k1gInSc1	středojih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
rostou	růst	k5eAaImIp3nP	růst
ve	v	k7c6	v
smíšených	smíšený	k2eAgInPc6d1	smíšený
lesích	les	k1gInPc6	les
severovýchodu	severovýchod	k1gInSc2	severovýchod
a	a	k8xC	a
Appalačských	Appalačský	k2eAgInPc2d1	Appalačský
hor.	hor.	k?	hor.
V	v	k7c6	v
zapojených	zapojený	k2eAgInPc6d1	zapojený
smíšených	smíšený	k2eAgInPc6d1	smíšený
klimaxových	klimaxový	k2eAgInPc6d1	klimaxový
lesích	les	k1gInPc6	les
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
málo	málo	k1gNnSc4	málo
druhů	druh	k1gInPc2	druh
schopných	schopný	k2eAgMnPc2d1	schopný
snášet	snášet	k5eAaImF	snášet
alespoň	alespoň	k9	alespoň
částečné	částečný	k2eAgNnSc4d1	částečné
zastínění	zastínění	k1gNnSc4	zastínění
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
borovice	borovice	k1gFnSc1	borovice
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
<g/>
;	;	kIx,	;
ostatní	ostatní	k2eAgNnSc1d1	ostatní
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
konkurenci	konkurence	k1gFnSc3	konkurence
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
pouze	pouze	k6eAd1	pouze
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
potlačena	potlačit	k5eAaPmNgFnS	potlačit
nepříznivými	příznivý	k2eNgInPc7d1	nepříznivý
životními	životní	k2eAgInPc7d1	životní
faktory	faktor	k1gInPc7	faktor
(	(	kIx(	(
<g/>
sucho	sucho	k1gNnSc1	sucho
<g/>
,	,	kIx,	,
minimum	minimum	k1gNnSc1	minimum
živin	živina	k1gFnPc2	živina
<g/>
)	)	kIx)	)
a	a	k8xC	a
nebo	nebo	k8xC	nebo
pravidelně	pravidelně	k6eAd1	pravidelně
likvidována	likvidován	k2eAgFnSc1d1	likvidována
požáry	požár	k1gInPc7	požár
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklými	obvyklý	k2eAgFnPc7d1	obvyklá
doprovodnými	doprovodný	k2eAgFnPc7d1	doprovodná
rostlinami	rostlina	k1gFnPc7	rostlina
borovic	borovice	k1gFnPc2	borovice
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
náročné	náročný	k2eAgInPc1d1	náročný
světlomilné	světlomilný	k2eAgInPc1d1	světlomilný
duby	dub	k1gInPc1	dub
<g/>
,	,	kIx,	,
břízy	bříza	k1gFnPc1	bříza
<g/>
,	,	kIx,	,
jeřáby	jeřáb	k1gInPc1	jeřáb
<g/>
,	,	kIx,	,
jalovce	jalovec	k1gInPc1	jalovec
<g/>
,	,	kIx,	,
modříny	modřín	k1gInPc1	modřín
<g/>
,	,	kIx,	,
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
vřesovcovitých	vřesovcovitý	k2eAgMnPc2d1	vřesovcovitý
(	(	kIx(	(
<g/>
vřes	vřes	k1gInSc4	vřes
<g/>
,	,	kIx,	,
borůvky	borůvka	k1gFnPc4	borůvka
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
chladnějších	chladný	k2eAgFnPc6d2	chladnější
severských	severský	k2eAgFnPc6d1	severská
oblastech	oblast	k1gFnPc6	oblast
též	tenž	k3xDgFnSc2	tenž
keřovité	keřovitý	k2eAgFnSc2d1	keřovitá
vrby	vrba	k1gFnSc2	vrba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ohrožení	ohrožení	k1gNnSc1	ohrožení
<g/>
,	,	kIx,	,
invazivita	invazivita	k1gFnSc1	invazivita
===	===	k?	===
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
borovic	borovice	k1gFnPc2	borovice
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
stabilní	stabilní	k2eAgNnSc4d1	stabilní
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
známek	známka	k1gFnPc2	známka
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
řadí	řadit	k5eAaImIp3nS	řadit
organizace	organizace	k1gFnPc4	organizace
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
borovici	borovice	k1gFnSc4	borovice
Torreyovu	Torreyův	k2eAgFnSc4d1	Torreyův
<g/>
,	,	kIx,	,
borovici	borovice	k1gFnSc4	borovice
šupinatou	šupinatý	k2eAgFnSc4d1	šupinatá
<g/>
,	,	kIx,	,
a	a	k8xC	a
druh	druh	k1gMnSc1	druh
Pinus	Pinus	k1gMnSc1	Pinus
cernua	cernua	k1gMnSc1	cernua
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
druhů	druh	k1gInPc2	druh
hospodářsky	hospodářsky	k6eAd1	hospodářsky
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
původního	původní	k2eAgInSc2d1	původní
výskytu	výskyt	k1gInSc2	výskyt
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
uvádí	uvádět	k5eAaImIp3nS	uvádět
invazní	invazní	k2eAgInSc4d1	invazní
potenciál	potenciál	k1gInSc4	potenciál
narušující	narušující	k2eAgInPc4d1	narušující
původní	původní	k2eAgInPc4d1	původní
ekosystémy	ekosystém	k1gInPc4	ekosystém
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
borovice	borovice	k1gFnSc1	borovice
Elliottova	Elliottův	k2eAgFnSc1d1	Elliottova
<g/>
,	,	kIx,	,
paprsčitá	paprsčitý	k2eAgFnSc1d1	paprsčitá
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
halepská	halepský	k2eAgFnSc1d1	halepská
nebo	nebo	k8xC	nebo
přímořská	přímořský	k2eAgFnSc1d1	přímořská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
též	též	k9	též
evropská	evropský	k2eAgFnSc1d1	Evropská
borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Světlomilné	světlomilný	k2eAgFnPc1d1	světlomilná
a	a	k8xC	a
pionýrské	pionýrský	k2eAgFnPc1d1	Pionýrská
borovice	borovice	k1gFnPc1	borovice
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
hrozbou	hrozba	k1gFnSc7	hrozba
především	především	k6eAd1	především
pro	pro	k7c4	pro
původní	původní	k2eAgNnPc4d1	původní
travinná	travinný	k2eAgNnPc4d1	travinný
a	a	k8xC	a
křovinná	křovinný	k2eAgNnPc4d1	křovinné
společenstva	společenstvo	k1gNnPc4	společenstvo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
vzácný	vzácný	k2eAgInSc4d1	vzácný
jihoafrický	jihoafrický	k2eAgInSc4d1	jihoafrický
fynbos	fynbos	k1gInSc4	fynbos
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
postupně	postupně	k6eAd1	postupně
zarůstají	zarůstat	k5eAaImIp3nP	zarůstat
a	a	k8xC	a
vytlačují	vytlačovat	k5eAaImIp3nP	vytlačovat
tak	tak	k6eAd1	tak
místní	místní	k2eAgInPc1d1	místní
druhy	druh	k1gInPc1	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
narušují	narušovat	k5eAaImIp3nP	narušovat
též	též	k9	též
lesní	lesní	k2eAgInPc4d1	lesní
porosty	porost	k1gInPc4	porost
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
narušovány	narušován	k2eAgInPc1d1	narušován
klimatickou	klimatický	k2eAgFnSc7d1	klimatická
změnou	změna	k1gFnSc7	změna
<g/>
,	,	kIx,	,
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
disturbancemi	disturbance	k1gFnPc7	disturbance
nebo	nebo	k8xC	nebo
požáry	požár	k1gInPc7	požár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
invazní	invazní	k2eAgInSc4d1	invazní
druh	druh	k1gInSc4	druh
považována	považován	k2eAgFnSc1d1	považována
borovice	borovice	k1gFnSc1	borovice
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
poškozující	poškozující	k2eAgInPc1d1	poškozující
vzácné	vzácný	k2eAgInPc1d1	vzácný
reliktní	reliktní	k2eAgInPc1d1	reliktní
bory	bor	k1gInPc1	bor
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ekosystémové	Ekosystémový	k2eAgInPc1d1	Ekosystémový
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
dřevin	dřevina	k1gFnPc2	dřevina
formují	formovat	k5eAaImIp3nP	formovat
borovice	borovice	k1gFnSc2	borovice
mykorhizní	mykorhizní	k2eAgInPc1d1	mykorhizní
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
lesními	lesní	k2eAgFnPc7d1	lesní
houbami	houba	k1gFnPc7	houba
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
ektomykorhizu	ektomykorhiz	k1gInSc6	ektomykorhiz
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
houby	houby	k6eAd1	houby
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kolem	kolem	k7c2	kolem
kořene	kořen	k1gInSc2	kořen
tzv.	tzv.	kA	tzv.
hyfový	hyfový	k2eAgInSc4d1	hyfový
plášť	plášť	k1gInSc4	plášť
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
savá	savý	k2eAgFnSc1d1	savá
plocha	plocha	k1gFnSc1	plocha
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
zásobení	zásobení	k1gNnSc2	zásobení
rostliny	rostlina	k1gFnSc2	rostlina
živinami	živina	k1gFnPc7	živina
<g/>
.	.	kIx.	.
</s>
<s>
Častými	častý	k2eAgInPc7d1	častý
symbionty	symbiont	k1gInPc7	symbiont
jsou	být	k5eAaImIp3nP	být
muchomůrky	muchomůrky	k?	muchomůrky
<g/>
,	,	kIx,	,
lišky	liška	k1gFnPc1	liška
<g/>
,	,	kIx,	,
ryzce	ryzec	k1gInPc1	ryzec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ryzec	ryzec	k1gMnSc1	ryzec
pravý	pravý	k2eAgMnSc1d1	pravý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgInPc1d1	mnohý
klouzky	klouzek	k1gInPc1	klouzek
<g/>
,	,	kIx,	,
holubinky	holubinka	k1gFnPc1	holubinka
či	či	k8xC	či
hřiby	hřib	k1gInPc1	hřib
(	(	kIx(	(
<g/>
hřib	hřib	k1gInSc1	hřib
borový	borový	k2eAgInSc1d1	borový
<g/>
,	,	kIx,	,
hřib	hřib	k1gInSc1	hřib
smrkový	smrkový	k2eAgInSc1d1	smrkový
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnSc2	semeno
borovic	borovice	k1gFnPc2	borovice
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
potravu	potrava	k1gFnSc4	potrava
mnoha	mnoho	k4c3	mnoho
živočichům	živočich	k1gMnPc3	živočich
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
křivky	křivka	k1gFnPc1	křivka
<g/>
,	,	kIx,	,
datlovití	datlovitý	k2eAgMnPc1d1	datlovitý
nebo	nebo	k8xC	nebo
ořešník	ořešník	k1gMnSc1	ořešník
kropenatý	kropenatý	k2eAgMnSc1d1	kropenatý
<g/>
,	,	kIx,	,
specializující	specializující	k2eAgMnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
šišky	šiška	k1gMnPc4	šiška
borovice	borovice	k1gFnSc2	borovice
limby	limba	k1gFnSc2	limba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlodavci	hlodavec	k1gMnPc1	hlodavec
(	(	kIx(	(
<g/>
veverky	veverka	k1gFnPc1	veverka
<g/>
,	,	kIx,	,
plši	plch	k1gMnPc1	plch
<g/>
,	,	kIx,	,
různí	různý	k2eAgMnPc1d1	různý
myšovití	myšovití	k1gMnPc1	myšovití
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doplňkově	doplňkově	k6eAd1	doplňkově
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
šelmy	šelma	k1gFnPc1	šelma
(	(	kIx(	(
<g/>
kuna	kuna	k1gFnSc1	kuna
<g/>
,	,	kIx,	,
norek	norek	k1gMnSc1	norek
<g/>
,	,	kIx,	,
sobol	sobol	k1gMnSc1	sobol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
semeny	semeno	k1gNnPc7	semeno
borovice	borovice	k1gFnSc2	borovice
sibiřské	sibiřský	k2eAgFnSc6d1	sibiřská
se	se	k3xPyFc4	se
v	v	k7c6	v
tajze	tajga	k1gFnSc6	tajga
živí	živit	k5eAaImIp3nS	živit
až	až	k9	až
30	[number]	k4	30
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
dál	daleko	k6eAd2	daleko
přímo	přímo	k6eAd1	přímo
závisí	záviset	k5eAaImIp3nP	záviset
i	i	k9	i
predátoři	predátor	k1gMnPc1	predátor
v	v	k7c6	v
složitých	složitý	k2eAgFnPc6d1	složitá
ekosystémových	ekosystémův	k2eAgFnPc6d1	ekosystémův
vazbách	vazba	k1gFnPc6	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgNnSc1d1	mladé
borové	borový	k2eAgNnSc1d1	borové
jehličí	jehličí	k1gNnSc1	jehličí
je	být	k5eAaImIp3nS	být
požíráno	požírat	k5eAaImNgNnS	požírat
spárkatou	spárkatý	k2eAgFnSc7d1	spárkatá
zvěří	zvěř	k1gFnSc7	zvěř
(	(	kIx(	(
<g/>
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
např.	např.	kA	např.
losy	los	k1gInPc4	los
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ptáky	pták	k1gMnPc4	pták
(	(	kIx(	(
<g/>
tetřev	tetřev	k1gMnSc1	tetřev
hlušec	hlušec	k1gMnSc1	hlušec
<g/>
)	)	kIx)	)
a	a	k8xC	a
housenkami	housenka	k1gFnPc7	housenka
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
působit	působit	k5eAaImF	působit
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Mšice	mšice	k1gFnSc1	mšice
medovnice	medovnice	k1gFnSc2	medovnice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
medovnice	medovnice	k1gFnSc1	medovnice
borová	borový	k2eAgFnSc1d1	Borová
(	(	kIx(	(
<g/>
Cinara	Cinara	k1gFnSc1	Cinara
pini	pin	k1gFnSc2	pin
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
medovnice	medovnice	k1gFnSc1	medovnice
lesklá	lesklý	k2eAgFnSc1d1	lesklá
(	(	kIx(	(
<g/>
Cinara	Cinara	k1gFnSc1	Cinara
nuda	nuda	k1gFnSc1	nuda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
produkují	produkovat	k5eAaImIp3nP	produkovat
napichováním	napichování	k1gNnSc7	napichování
a	a	k8xC	a
sáním	sání	k1gNnSc7	sání
asimilačního	asimilační	k2eAgInSc2d1	asimilační
proudu	proud	k1gInSc2	proud
ze	z	k7c2	z
sítkovic	sítkovice	k1gFnPc2	sítkovice
sladkou	sladký	k2eAgFnSc4d1	sladká
lepkavou	lepkavý	k2eAgFnSc4d1	lepkavá
šťávu	šťáva	k1gFnSc4	šťáva
–	–	k?	–
medovici	medovice	k1gFnSc4	medovice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
sbírána	sbírat	k5eAaImNgFnS	sbírat
a	a	k8xC	a
využívána	využívat	k5eAaImNgFnS	využívat
včelami	včela	k1gFnPc7	včela
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
lesního	lesní	k2eAgInSc2d1	lesní
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Choroby	choroba	k1gFnPc1	choroba
a	a	k8xC	a
škůdci	škůdce	k1gMnPc1	škůdce
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
obvyklé	obvyklý	k2eAgMnPc4d1	obvyklý
hmyzí	hmyzí	k2eAgMnPc4d1	hmyzí
škůdce	škůdce	k1gMnPc4	škůdce
borovic	borovice	k1gFnPc2	borovice
patří	patřit	k5eAaImIp3nS	patřit
bekyně	bekyně	k1gFnSc1	bekyně
mniška	mniška	k1gFnSc1	mniška
<g/>
,	,	kIx,	,
bourovec	bourovec	k1gMnSc1	bourovec
borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
lýkohubi	lýkohub	k1gMnPc1	lýkohub
<g/>
,	,	kIx,	,
kůrovci	kůrovec	k1gMnPc1	kůrovec
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Dendroctonus	Dendroctonus	k1gInSc1	Dendroctonus
<g/>
)	)	kIx)	)
či	či	k8xC	či
obaleči	obaleč	k1gMnPc1	obaleč
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
napadány	napadat	k5eAaBmNgInP	napadat
také	také	k9	také
rostlinnými	rostlinný	k2eAgMnPc7d1	rostlinný
parazity	parazit	k1gMnPc7	parazit
a	a	k8xC	a
poloparazity	poloparazita	k1gFnPc1	poloparazita
jako	jako	k8xS	jako
jmelí	jmelí	k1gNnSc1	jmelí
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
druhy	druh	k1gInPc1	druh
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
santálovitých	santálovitý	k2eAgMnPc2d1	santálovitý
<g/>
.	.	kIx.	.
</s>
<s>
Dřeviny	dřevina	k1gFnPc4	dřevina
oslabené	oslabený	k2eAgFnPc4d1	oslabená
škůdci	škůdce	k1gMnPc7	škůdce
nebo	nebo	k8xC	nebo
různými	různý	k2eAgInPc7d1	různý
abiotickými	abiotický	k2eAgInPc7d1	abiotický
stresy	stres	k1gInPc7	stres
(	(	kIx(	(
<g/>
např.	např.	kA	např.
suchem	sucho	k1gNnSc7	sucho
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
náchylné	náchylný	k2eAgInPc1d1	náchylný
na	na	k7c6	na
poškození	poškození	k1gNnSc2	poškození
houbovými	houbový	k2eAgInPc7d1	houbový
patogeny	patogen	k1gInPc7	patogen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
šíření	šíření	k1gNnSc1	šíření
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umocněno	umocnit	k5eAaPmNgNnS	umocnit
v	v	k7c6	v
monokulturních	monokulturní	k2eAgFnPc6d1	monokulturní
hospodářských	hospodářský	k2eAgFnPc6d1	hospodářská
výsadbáchː	výsadbáchː	k?	výsadbáchː
Sphaeropsis	Sphaeropsis	k1gFnPc6	Sphaeropsis
sapinea	sapinea	k6eAd1	sapinea
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
chřadnutí	chřadnutí	k1gNnSc1	chřadnutí
a	a	k8xC	a
prosychání	prosychání	k1gNnSc1	prosychání
borovic	borovice	k1gFnPc2	borovice
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
borovice	borovice	k1gFnPc1	borovice
černé	černý	k2eAgFnPc1d1	černá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
dalších	další	k1gNnPc2	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mycosphaerella	Mycosphaerella	k1gMnSc1	Mycosphaerella
pini	pin	k1gFnSc2	pin
<g/>
,	,	kIx,	,
Lophodermium	Lophodermium	k1gNnSc1	Lophodermium
seditiosum	seditiosum	k1gNnSc1	seditiosum
a	a	k8xC	a
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
druhy	druh	k1gInPc1	druh
pak	pak	k6eAd1	pak
sypavku	sypavka	k1gFnSc4	sypavka
–	–	k?	–
závažné	závažný	k2eAgNnSc1d1	závažné
onemocnění	onemocnění	k1gNnSc1	onemocnění
napadající	napadající	k2eAgFnSc2d1	napadající
jehlice	jehlice	k1gFnSc2	jehlice
<g/>
.	.	kIx.	.
</s>
<s>
Škodí	škodit	k5eAaImIp3nP	škodit
též	též	k9	též
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
rzí	rez	k1gFnPc2	rez
a	a	k8xC	a
cenangií	cenangie	k1gFnPc2	cenangie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvážnějším	vážní	k2eAgInPc3d3	nejvážnější
patogenům	patogen	k1gInPc3	patogen
patří	patřit	k5eAaImIp3nS	patřit
dvoubytná	dvoubytný	k2eAgFnSc1d1	dvoubytná
rez	rez	k1gFnSc1	rez
vejmutovková	vejmutovkový	k2eAgFnSc1d1	vejmutovková
(	(	kIx(	(
<g/>
Cronartium	Cronartium	k1gNnSc1	Cronartium
ribicola	ribicola	k1gFnSc1	ribicola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
druhým	druhý	k4xOgNnSc7	druhý
hostitelem	hostitel	k1gMnSc7	hostitel
je	být	k5eAaImIp3nS	být
rybíz	rybíz	k1gInSc1	rybíz
<g/>
.	.	kIx.	.
</s>
<s>
Napadá	napadat	k5eAaImIp3nS	napadat
především	především	k9	především
borovice	borovice	k1gFnSc1	borovice
z	z	k7c2	z
podrodu	podrod	k1gInSc2	podrod
Strobus	Strobus	k1gInSc1	Strobus
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
úhyn	úhyn	k1gInSc4	úhyn
i	i	k9	i
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zcela	zcela	k6eAd1	zcela
vitálních	vitální	k2eAgMnPc2d1	vitální
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
;	;	kIx,	;
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	s	k7c7	s
zduřeninami	zduřenina	k1gFnPc7	zduřenina
na	na	k7c6	na
kmeni	kmen	k1gInSc6	kmen
<g/>
,	,	kIx,	,
výtokem	výtok	k1gInSc7	výtok
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
drobnými	drobný	k2eAgFnPc7d1	drobná
oranžovými	oranžový	k2eAgFnPc7d1	oranžová
plodnicemi	plodnice	k1gFnPc7	plodnice
<g/>
.	.	kIx.	.
</s>
<s>
Rez	rez	k1gFnSc1	rez
borová	borový	k2eAgFnSc1d1	Borová
(	(	kIx(	(
<g/>
Cronartium	Cronartium	k1gNnSc1	Cronartium
asclepiadeum	asclepiadeum	k1gNnSc1	asclepiadeum
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
naopak	naopak	k6eAd1	naopak
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
dvoujehličné	dvoujehličný	k2eAgFnPc4d1	dvoujehličný
borovice	borovice	k1gFnPc4	borovice
<g/>
.	.	kIx.	.
</s>
<s>
Sosnokrut	Sosnokrut	k1gInSc1	Sosnokrut
(	(	kIx(	(
<g/>
Melampsora	Melampsora	k1gFnSc1	Melampsora
pinitorqua	pinitorqua	k1gFnSc1	pinitorqua
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
dvoubytná	dvoubytný	k2eAgFnSc1d1	dvoubytná
rez	rez	k1gFnSc1	rez
způsobující	způsobující	k2eAgFnSc1d1	způsobující
typické	typický	k2eAgNnSc4d1	typické
zkroucení	zkroucení	k1gNnSc4	zkroucení
výhonů	výhon	k1gInPc2	výhon
především	především	k6eAd1	především
u	u	k7c2	u
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgMnSc1d1	lesní
<g/>
,	,	kIx,	,
přípletka	přípletka	k1gFnSc1	přípletka
černá	černý	k2eAgFnSc1d1	černá
(	(	kIx(	(
<g/>
Herpotrychia	Herpotrychia	k1gFnSc1	Herpotrychia
nigra	nigr	k1gMnSc2	nigr
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
napadá	napadat	k5eAaPmIp3nS	napadat
borovice	borovice	k1gFnSc1	borovice
ve	v	k7c6	v
vysokohorském	vysokohorský	k2eAgNnSc6d1	vysokohorské
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dlouho	dlouho	k6eAd1	dlouho
leží	ležet	k5eAaImIp3nS	ležet
sníh	sníh	k1gInSc1	sníh
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
tedy	tedy	k9	tedy
kleč	kleč	k1gFnSc4	kleč
nebo	nebo	k8xC	nebo
borovici	borovice	k1gFnSc4	borovice
limbu	limb	k1gInSc2	limb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dřevokazných	dřevokazný	k2eAgFnPc2d1	dřevokazná
hub	houba	k1gFnPc2	houba
na	na	k7c6	na
borovicích	borovice	k1gFnPc6	borovice
rostou	růst	k5eAaImIp3nP	růst
ohňovce	ohňovec	k1gInPc1	ohňovec
<g/>
,	,	kIx,	,
ďubkatec	ďubkatec	k1gMnSc1	ďubkatec
borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
kotrč	kotrč	k1gInSc1	kotrč
kadeřavý	kadeřavý	k2eAgInSc1d1	kadeřavý
<g/>
,	,	kIx,	,
václavky	václavka	k1gFnPc4	václavka
nebo	nebo	k8xC	nebo
hnědák	hnědák	k1gMnSc1	hnědák
Schweinitzův	Schweinitzův	k2eAgMnSc1d1	Schweinitzův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Adaptace	adaptace	k1gFnSc1	adaptace
na	na	k7c4	na
oheň	oheň	k1gInSc4	oheň
===	===	k?	===
</s>
</p>
<p>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
i	i	k8xC	i
středomořské	středomořský	k2eAgFnSc2d1	středomořská
borovice	borovice	k1gFnSc2	borovice
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
adaptovány	adaptován	k2eAgInPc1d1	adaptován
na	na	k7c4	na
lesní	lesní	k2eAgInPc4d1	lesní
požáry	požár	k1gInPc4	požár
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
coby	coby	k?	coby
světlomilné	světlomilný	k2eAgFnPc4d1	světlomilná
dřeviny	dřevina	k1gFnPc4	dřevina
dokáží	dokázat	k5eAaPmIp3nP	dokázat
udržovat	udržovat	k5eAaImF	udržovat
v	v	k7c6	v
porostu	porost	k1gInSc6	porost
<g/>
;	;	kIx,	;
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
bez	bez	k7c2	bez
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
požárů	požár	k1gInPc2	požár
ze	z	k7c2	z
stanoviště	stanoviště	k1gNnSc2	stanoviště
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
v	v	k7c6	v
zastínění	zastínění	k1gNnSc6	zastínění
těžko	těžko	k6eAd1	těžko
zmlazují	zmlazovat	k5eAaImIp3nP	zmlazovat
<g/>
;	;	kIx,	;
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
řadit	řadit	k5eAaImF	řadit
mezi	mezi	k7c7	mezi
pyrofyty	pyrofyt	k1gInPc7	pyrofyt
<g/>
.	.	kIx.	.
</s>
<s>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
nálezy	nález	k1gInPc1	nález
přitom	přitom	k6eAd1	přitom
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
schopnost	schopnost	k1gFnSc1	schopnost
nakládat	nakládat	k5eAaImF	nakládat
s	s	k7c7	s
ohněm	oheň	k1gInSc7	oheň
provází	provázet	k5eAaImIp3nS	provázet
borovice	borovice	k1gFnSc1	borovice
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
jejich	jejich	k3xOp3gFnSc2	jejich
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
adaptacím	adaptace	k1gFnPc3	adaptace
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
tlustá	tlustý	k2eAgFnSc1d1	tlustá
kůra	kůra	k1gFnSc1	kůra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
menším	malý	k2eAgInPc3d2	menší
požárům	požár	k1gInPc3	požár
odolá	odolat	k5eAaPmIp3nS	odolat
a	a	k8xC	a
strom	strom	k1gInSc1	strom
dokáže	dokázat	k5eAaPmIp3nS	dokázat
přežít	přežít	k5eAaPmF	přežít
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
serotinní	serotinný	k2eAgMnPc1d1	serotinný
šišky	šiška	k1gFnSc2	šiška
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
otevírají	otevírat	k5eAaImIp3nP	otevírat
a	a	k8xC	a
vysemeňují	vysemeňovat	k5eAaImIp3nP	vysemeňovat
až	až	k9	až
při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
indukovaných	indukovaný	k2eAgFnPc6d1	indukovaná
právě	právě	k6eAd1	právě
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
semenáčkům	semenáček	k1gInPc3	semenáček
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
prostředí	prostředí	k1gNnSc1	prostředí
vyčištěné	vyčištěný	k2eAgNnSc1d1	vyčištěné
od	od	k7c2	od
konkurenčních	konkurenční	k2eAgFnPc2d1	konkurenční
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
Banksova	Banksův	k2eAgFnSc1d1	Banksova
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
pokroucená	pokroucený	k2eAgFnSc1d1	pokroucená
<g/>
,	,	kIx,	,
Pinus	Pinus	k1gInSc1	Pinus
muricata	muricata	k1gFnSc1	muricata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
spící	spící	k2eAgNnPc4d1	spící
očka	očko	k1gNnPc4	očko
skrytá	skrytý	k2eAgFnSc1d1	skrytá
pod	pod	k7c7	pod
kůrou	kůra	k1gFnSc7	kůra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
ohoření	ohoření	k1gNnSc6	ohoření
probudí	probudit	k5eAaPmIp3nS	probudit
a	a	k8xC	a
strom	strom	k1gInSc1	strom
znovu	znovu	k6eAd1	znovu
obrazí	obrazit	k5eAaImIp3nS	obrazit
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gInSc1	Pinus
echinata	echinata	k1gFnSc1	echinata
<g/>
,	,	kIx,	,
Pinus	Pinus	k1gInSc1	Pinus
brutia	brutia	k1gFnSc1	brutia
<g/>
,	,	kIx,	,
Pinus	Pinus	k1gInSc1	Pinus
canariensis	canariensis	k1gFnSc2	canariensis
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
pozoruhodnou	pozoruhodný	k2eAgFnSc7d1	pozoruhodná
adaptací	adaptace	k1gFnSc7	adaptace
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
trávové	trávový	k2eAgNnSc1d1	trávové
stadium	stadium	k1gNnSc1	stadium
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
terminální	terminální	k2eAgInSc1d1	terminální
růstový	růstový	k2eAgInSc1d1	růstový
pupen	pupen	k1gInSc1	pupen
mladého	mladý	k2eAgInSc2d1	mladý
stromku	stromek	k1gInSc2	stromek
je	být	k5eAaImIp3nS	být
ukryt	ukrýt	k5eAaPmNgInS	ukrýt
pod	pod	k7c7	pod
hustým	hustý	k2eAgInSc7d1	hustý
chomáčem	chomáč	k1gInSc7	chomáč
přízemně	přízemně	k6eAd1	přízemně
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
jehlic	jehlice	k1gFnPc2	jehlice
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rostlina	rostlina	k1gFnSc1	rostlina
spíše	spíše	k9	spíše
než	než	k8xS	než
strom	strom	k1gInSc1	strom
připomíná	připomínat	k5eAaImIp3nS	připomínat
velký	velký	k2eAgInSc1d1	velký
trs	trs	k1gInSc1	trs
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
požáru	požár	k1gInSc6	požár
trs	trs	k1gInSc1	trs
jehlic	jehlice	k1gFnPc2	jehlice
sice	sice	k8xC	sice
ohoří	ohořet	k5eAaPmIp3nS	ohořet
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
hlavní	hlavní	k2eAgInSc1d1	hlavní
pupen	pupen	k1gInSc1	pupen
je	být	k5eAaImIp3nS	být
ochráněn	ochránit	k5eAaPmNgInS	ochránit
<g/>
;	;	kIx,	;
ohořelé	ohořelý	k2eAgFnPc4d1	ohořelá
jehlice	jehlice	k1gFnPc4	jehlice
jsou	být	k5eAaImIp3nP	být
poté	poté	k6eAd1	poté
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
novými	nový	k2eAgFnPc7d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
trávové	trávový	k2eAgNnSc1d1	trávové
stadium	stadium	k1gNnSc1	stadium
trvá	trvat	k5eAaImIp3nS	trvat
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
jedinců	jedinec	k1gMnPc2	jedinec
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
ojediněle	ojediněle	k6eAd1	ojediněle
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
strom	strom	k1gInSc1	strom
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
neroste	růst	k5eNaImIp3nS	růst
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
buduje	budovat	k5eAaImIp3nS	budovat
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
a	a	k8xC	a
důkladnou	důkladný	k2eAgFnSc4d1	důkladná
síť	síť	k1gFnSc4	síť
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
živiny	živina	k1gFnPc4	živina
a	a	k8xC	a
vyčkává	vyčkávat	k5eAaImIp3nS	vyčkávat
na	na	k7c4	na
dobré	dobrý	k2eAgFnPc4d1	dobrá
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c4	na
dostatek	dostatek	k1gInSc4	dostatek
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
kupříkladu	kupříkladu	k6eAd1	kupříkladu
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
a	a	k8xC	a
likvidaci	likvidace	k1gFnSc6	likvidace
konkurence	konkurence	k1gFnSc1	konkurence
ohněm	oheň	k1gInSc7	oheň
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
teprve	teprve	k6eAd1	teprve
potom	potom	k6eAd1	potom
zahájí	zahájit	k5eAaPmIp3nS	zahájit
rychlý	rychlý	k2eAgInSc1d1	rychlý
růst	růst	k1gInSc1	růst
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
si	se	k3xPyFc3	se
silnou	silný	k2eAgFnSc4d1	silná
<g/>
,	,	kIx,	,
ohnivzdornou	ohnivzdorný	k2eAgFnSc4d1	ohnivzdorná
borku	borka	k1gFnSc4	borka
<g/>
.	.	kIx.	.
</s>
<s>
Trávová	trávový	k2eAgFnSc1d1	trávová
etapa	etapa	k1gFnSc1	etapa
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
u	u	k7c2	u
borovice	borovice	k1gFnSc2	borovice
Elliottovy	Elliottův	k2eAgFnSc2d1	Elliottova
variety	varieta	k1gFnSc2	varieta
densa	dens	k1gMnSc2	dens
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc2	borovice
bahenní	bahenní	k2eAgFnSc2d1	bahenní
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc2	borovice
Montezumovy	Montezumův	k2eAgFnSc2d1	Montezumova
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc2	borovice
pavejmutky	pavejmutka	k1gFnSc2	pavejmutka
a	a	k8xC	a
druhů	druh	k1gInPc2	druh
Pinus	Pinus	k1gInSc1	Pinus
engelmannii	engelmannie	k1gFnSc3	engelmannie
<g/>
,	,	kIx,	,
Pinus	Pinus	k1gInSc1	Pinus
merkusii	merkusie	k1gFnSc4	merkusie
a	a	k8xC	a
Pinus	Pinus	k1gInSc4	Pinus
tropicalis	tropicalis	k1gFnSc2	tropicalis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
a	a	k8xC	a
evoluce	evoluce	k1gFnSc2	evoluce
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc4	rod
Pinus	Pinus	k1gMnSc1	Pinus
platně	platně	k6eAd1	platně
popsal	popsat	k5eAaPmAgMnS	popsat
Carl	Carl	k1gMnSc1	Carl
Linné	Linná	k1gFnSc2	Linná
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Species	species	k1gFnSc1	species
plantarum	plantarum	k1gInSc4	plantarum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1753	[number]	k4	1753
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
něho	on	k3xPp3gInSc2	on
zahrnul	zahrnout	k5eAaPmAgMnS	zahrnout
i	i	k9	i
všechny	všechen	k3xTgFnPc4	všechen
tehdy	tehdy	k6eAd1	tehdy
známé	známý	k2eAgFnPc4d1	známá
jedle	jedle	k1gFnPc4	jedle
<g/>
,	,	kIx,	,
modříny	modřín	k1gInPc4	modřín
a	a	k8xC	a
smrky	smrk	k1gInPc4	smrk
<g/>
;	;	kIx,	;
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
jinými	jiný	k2eAgMnPc7d1	jiný
autory	autor	k1gMnPc7	autor
vyděleny	vydělit	k5eAaPmNgFnP	vydělit
do	do	k7c2	do
samostatných	samostatný	k2eAgInPc2d1	samostatný
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Systematická	systematický	k2eAgFnSc1d1	systematická
klasifikace	klasifikace	k1gFnSc1	klasifikace
rodu	rod	k1gInSc2	rod
Pinus	Pinus	k1gInSc1	Pinus
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
čase	čas	k1gInSc6	čas
proměňovalaː	proměňovalaː	k?	proměňovalaː
byl	být	k5eAaImAgInS	být
dělen	dělit	k5eAaImNgInS	dělit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
rodů	rod	k1gInPc2	rod
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
byly	být	k5eAaImAgInP	být
navrhovány	navrhovat	k5eAaImNgInP	navrhovat
různé	různý	k2eAgInPc1d1	různý
další	další	k2eAgInPc1d1	další
podrody	podrod	k1gInPc1	podrod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ducampopinus	Ducampopinus	k1gMnSc1	Ducampopinus
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
zhruba	zhruba	k6eAd1	zhruba
sekci	sekce	k1gFnSc4	sekce
Parrya	Parryum	k1gNnSc2	Parryum
v	v	k7c6	v
podrodu	podrod	k1gInSc6	podrod
Strobus	Strobus	k1gInSc1	Strobus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
molekulárními	molekulární	k2eAgFnPc7d1	molekulární
genetickými	genetický	k2eAgFnPc7d1	genetická
studiemi	studie	k1gFnPc7	studie
fylogeneze	fylogeneze	k1gFnSc2	fylogeneze
borovic	borovice	k1gFnPc2	borovice
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Měnil	měnit	k5eAaImAgMnS	měnit
se	se	k3xPyFc4	se
také	také	k9	také
počet	počet	k1gInSc1	počet
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sekcí	sekce	k1gFnPc2	sekce
a	a	k8xC	a
podsekcí	podsekce	k1gFnPc2	podsekce
a	a	k8xC	a
zařazení	zařazení	k1gNnSc2	zařazení
některých	některý	k3yIgInPc2	některý
problematických	problematický	k2eAgInPc2d1	problematický
druhů	druh	k1gInPc2	druh
<g/>
;	;	kIx,	;
fylogenetické	fylogenetický	k2eAgFnPc4d1	fylogenetická
studie	studie	k1gFnPc4	studie
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
shodují	shodovat	k5eAaImIp3nP	shodovat
na	na	k7c6	na
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
monofyletičnosti	monofyletičnost	k1gFnPc4	monofyletičnost
podrodů	podrod	k1gInPc2	podrod
a	a	k8xC	a
sekcí	sekce	k1gFnPc2	sekce
uvedených	uvedený	k2eAgFnPc2d1	uvedená
v	v	k7c6	v
přehledu	přehled	k1gInSc6	přehled
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
<g/>
Borovice	borovice	k1gFnSc1	borovice
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
vývojově	vývojově	k6eAd1	vývojově
staré	starý	k2eAgInPc4d1	starý
taxony	taxon	k1gInPc4	taxon
<g/>
,	,	kIx,	,
fosilní	fosilní	k2eAgInPc1d1	fosilní
záznamy	záznam	k1gInPc1	záznam
udávají	udávat	k5eAaImIp3nP	udávat
existenci	existence	k1gFnSc4	existence
rodu	rod	k1gInSc2	rod
již	již	k9	již
před	před	k7c7	před
130	[number]	k4	130
<g/>
–	–	k?	–
<g/>
140	[number]	k4	140
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
křídy	křída	k1gFnSc2	křída
bylo	být	k5eAaImAgNnS	být
už	už	k6eAd1	už
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
i	i	k9	i
rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
oba	dva	k4xCgInPc4	dva
podrody	podrod	k1gInPc4	podrod
Pinus	Pinus	k1gMnSc1	Pinus
a	a	k8xC	a
Strobus	Strobus	k1gMnSc1	Strobus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
časově	časově	k6eAd1	časově
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
dalším	další	k2eAgInPc3d1	další
generickým	generický	k2eAgInPc3d1	generický
párům	pár	k1gInPc3	pár
čeledi	čeleď	k1gFnSc2	čeleď
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Abies	Abies	k1gInSc1	Abies
a	a	k8xC	a
Keteleeria	Keteleerium	k1gNnPc1	Keteleerium
nebo	nebo	k8xC	nebo
Larix	Larix	k1gInSc1	Larix
a	a	k8xC	a
Pseudotsuga	Pseudotsuga	k1gFnSc1	Pseudotsuga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývojově	vývojově	k6eAd1	vývojově
původnější	původní	k2eAgInPc1d2	původnější
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
podrodů	podrod	k1gInPc2	podrod
je	být	k5eAaImIp3nS	být
Strobus	Strobus	k1gInSc1	Strobus
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc7	jeho
bazální	bazální	k2eAgFnSc7d1	bazální
větví	větev	k1gFnSc7	větev
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
nejprimitivnějšími	primitivní	k2eAgFnPc7d3	nejprimitivnější
recentními	recentní	k2eAgFnPc7d1	recentní
borovicemi	borovice	k1gFnPc7	borovice
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
sekce	sekce	k1gFnSc1	sekce
Parrya	Parrya	k1gFnSc1	Parrya
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
řadu	řada	k1gFnSc4	řada
druhů	druh	k1gInPc2	druh
s	s	k7c7	s
unikátními	unikátní	k2eAgFnPc7d1	unikátní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jediná	jediný	k2eAgFnSc1d1	jediná
jehlice	jehlice	k1gFnSc1	jehlice
na	na	k7c6	na
brachyblastu	brachyblast	k1gInSc6	brachyblast
u	u	k7c2	u
Pinus	Pinus	k1gMnSc1	Pinus
monophylla	monophyll	k1gMnSc2	monophyll
<g/>
,	,	kIx,	,
dvoudomost	dvoudomost	k1gFnSc1	dvoudomost
u	u	k7c2	u
Pinus	Pinus	k1gInSc4	Pinus
johannis	johannis	k1gFnSc4	johannis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
podrody	podrod	k1gInPc1	podrod
jsou	být	k5eAaImIp3nP	být
monofyletické	monofyletický	k2eAgInPc1d1	monofyletický
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
jasně	jasně	k6eAd1	jasně
morfologicky	morfologicky	k6eAd1	morfologicky
rozlišitelné	rozlišitelný	k2eAgFnSc3d1	rozlišitelná
a	a	k8xC	a
geneticky	geneticky	k6eAd1	geneticky
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
některé	některý	k3yIgInPc1	některý
jiné	jiný	k2eAgInPc1d1	jiný
rody	rod	k1gInPc1	rod
v	v	k7c6	v
čeledi	čeleď	k1gFnSc6	čeleď
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
brát	brát	k5eAaImF	brát
je	být	k5eAaImIp3nS	být
i	i	k9	i
jako	jako	k9	jako
dva	dva	k4xCgInPc1	dva
samostatné	samostatný	k2eAgInPc1d1	samostatný
rody	rod	k1gInPc1	rod
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jasné	jasný	k2eAgFnSc3d1	jasná
monofyletičnosti	monofyletičnost	k1gFnSc3	monofyletičnost
celého	celý	k2eAgInSc2d1	celý
rodu	rod	k1gInSc2	rod
Pinus	Pinus	k1gInSc1	Pinus
(	(	kIx(	(
<g/>
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
a	a	k8xC	a
nomenklatorickým	nomenklatorický	k2eAgFnPc3d1	nomenklatorická
obtížím	obtíž	k1gFnPc3	obtíž
případného	případný	k2eAgNnSc2d1	případné
přejmenovávání	přejmenovávání	k1gNnSc2	přejmenovávání
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
pojímány	pojímat	k5eAaImNgFnP	pojímat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
podrodů	podrod	k1gInPc2	podrod
<g/>
.	.	kIx.	.
</s>
<s>
Sesterským	sesterský	k2eAgInSc7d1	sesterský
taxonem	taxon	k1gInSc7	taxon
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
smrk	smrk	k1gInSc1	smrk
(	(	kIx(	(
<g/>
Picea	Picea	k1gFnSc1	Picea
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Cathaya	Cathay	k2eAgFnSc1d1	Cathay
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
přesný	přesný	k2eAgInSc4d1	přesný
popis	popis	k1gInSc4	popis
jejich	jejich	k3xOp3gFnSc2	jejich
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
příbuznosti	příbuznost	k1gFnSc2	příbuznost
však	však	k9	však
dosud	dosud	k6eAd1	dosud
chybí	chybit	k5eAaPmIp3nS	chybit
silné	silný	k2eAgInPc4d1	silný
fylogenetické	fylogenetický	k2eAgInPc4d1	fylogenetický
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Systematický	systematický	k2eAgInSc4d1	systematický
přehled	přehled	k1gInSc4	přehled
recentních	recentní	k2eAgInPc2d1	recentní
druhů	druh	k1gInPc2	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
podrody	podrod	k1gInPc4	podrod
<g/>
:	:	kIx,	:
Pinus	Pinus	k1gMnSc1	Pinus
a	a	k8xC	a
Strobus	Strobus	k1gMnSc1	Strobus
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišováno	rozlišován	k2eAgNnSc1d1	rozlišováno
je	být	k5eAaImIp3nS	být
110	[number]	k4	110
<g/>
–	–	k?	–
<g/>
114	[number]	k4	114
recentních	recentní	k2eAgInPc2d1	recentní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
popisovaných	popisovaný	k2eAgInPc2d1	popisovaný
taxonů	taxon	k1gInPc2	taxon
je	být	k5eAaImIp3nS	být
však	však	k9	však
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mnohé	mnohý	k2eAgFnPc1d1	mnohá
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
zdrojích	zdroj	k1gInPc6	zdroj
pojímány	pojímat	k5eAaImNgInP	pojímat
jako	jako	k9	jako
samostatné	samostatný	k2eAgInPc1d1	samostatný
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
jen	jen	k6eAd1	jen
jako	jako	k8xC	jako
poddruhy	poddruh	k1gInPc4	poddruh
nebo	nebo	k8xC	nebo
varianty	varianta	k1gFnPc4	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Borovice	borovice	k1gFnSc1	borovice
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
stále	stále	k6eAd1	stále
probíhá	probíhat	k5eAaImIp3nS	probíhat
speciace	speciace	k1gFnSc1	speciace
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
nových	nový	k2eAgInPc2d1	nový
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
průběžně	průběžně	k6eAd1	průběžně
popisovány	popisován	k2eAgInPc1d1	popisován
(	(	kIx(	(
<g/>
např.	např.	kA	např.
laoská	laoský	k2eAgFnSc1d1	laoská
borovice	borovice	k1gFnSc1	borovice
větromilná	větromilný	k2eAgFnSc1d1	větromilný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
definitivně	definitivně	k6eAd1	definitivně
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
spontánním	spontánní	k2eAgFnPc3d1	spontánní
hybridizacím	hybridizace	k1gFnPc3	hybridizace
<g/>
.	.	kIx.	.
</s>
<s>
Lektotypem	Lektotyp	k1gInSc7	Lektotyp
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
sylvestris	sylvestris	k1gFnSc2	sylvestris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Následující	následující	k2eAgInSc1d1	následující
seznam	seznam	k1gInSc1	seznam
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
převzat	převzat	k2eAgInSc1d1	převzat
ze	z	k7c2	z
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
Gymnosperm	Gymnosperm	k1gInSc4	Gymnosperm
Database	Databasa	k1gFnSc3	Databasa
a	a	k8xC	a
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
článku	článek	k1gInSc2	článek
revidován	revidovat	k5eAaImNgInS	revidovat
podle	podle	k7c2	podle
dalších	další	k2eAgNnPc2d1	další
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
názvy	název	k1gInPc1	název
jsou	být	k5eAaImIp3nP	být
uváděné	uváděný	k2eAgInPc1d1	uváděný
podle	podle	k7c2	podle
stránek	stránka	k1gFnPc2	stránka
Biolib	Bioliba	k1gFnPc2	Bioliba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Botany	Botan	k1gInPc1	Botan
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
dostupné	dostupný	k2eAgFnSc2d1	dostupná
odborné	odborný	k2eAgFnSc2d1	odborná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podrod	podrod	k1gInSc1	podrod
Pinus	Pinus	k1gInSc1	Pinus
===	===	k?	===
</s>
</p>
<p>
<s>
Též	též	k9	též
Diploxylon	Diploxylon	k1gInSc4	Diploxylon
či	či	k8xC	či
"	"	kIx"	"
<g/>
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
borovice	borovice	k1gFnPc1	borovice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
cca	cca	kA	cca
70	[number]	k4	70
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
5	[number]	k4	5
nebo	nebo	k8xC	nebo
až	až	k6eAd1	až
8	[number]	k4	8
jehlic	jehlice	k1gFnPc2	jehlice
ve	v	k7c6	v
svazečku	svazeček	k1gInSc6	svazeček
s	s	k7c7	s
vytrvalou	vytrvalý	k2eAgFnSc7d1	vytrvalá
pochvou	pochva	k1gFnSc7	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jehlicích	jehlice	k1gFnPc6	jehlice
dva	dva	k4xCgInPc1	dva
cévní	cévní	k2eAgInPc1d1	cévní
svazky	svazek	k1gInPc1	svazek
<g/>
,	,	kIx,	,
průduchy	průduch	k1gInPc1	průduch
rozloženy	rozložit	k5eAaPmNgInP	rozložit
víceméně	víceméně	k9	víceméně
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
stranách	strana	k1gFnPc6	strana
jehlice	jehlice	k1gFnSc2	jehlice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřbetních	hřbetní	k2eAgFnPc6d1	hřbetní
šupinách	šupina	k1gFnPc6	šupina
šišek	šiška	k1gFnPc2	šiška
je	být	k5eAaImIp3nS	být
výrazný	výrazný	k2eAgInSc1d1	výrazný
"	"	kIx"	"
<g/>
pupek	pupek	k1gInSc1	pupek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
umbo	umbo	k6eAd1	umbo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sekce	sekce	k1gFnSc1	sekce
Pinus	Pinus	k1gInSc1	Pinus
–	–	k?	–
Eurasie	Eurasie	k1gFnSc2	Eurasie
a	a	k8xC	a
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
P.	P.	kA	P.
resinosa	resinosa	k1gFnSc1	resinosa
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
P.	P.	kA	P.
tropicalis	tropicalis	k1gInSc1	tropicalis
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Pinus	Pinus	k1gInSc1	Pinus
–	–	k?	–
dvou-	dvou-	k?	dvou-
nebo	nebo	k8xC	nebo
tříjehličné	tříjehličný	k2eAgFnSc2d1	tříjehličný
borovice	borovice	k1gFnSc2	borovice
Starého	Starého	k2eAgInSc2d1	Starého
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křídlo	křídlo	k1gNnSc4	křídlo
semene	semeno	k1gNnSc2	semeno
volně	volně	k6eAd1	volně
připevněné	připevněný	k2eAgFnPc1d1	připevněná
<g/>
,	,	kIx,	,
opadavé	opadavý	k2eAgFnPc1d1	opadavá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
densata	densat	k1gMnSc2	densat
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
densiflora	densiflor	k1gMnSc2	densiflor
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
hustokvětá	hustokvětý	k2eAgFnSc1d1	hustokvětá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
hwangshanensis	hwangshanensis	k1gFnSc2	hwangshanensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
hwangšanská	hwangšanský	k2eAgFnSc1d1	hwangšanský
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
kesiya	kesiya	k1gMnSc1	kesiya
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
luchuensis	luchuensis	k1gFnSc2	luchuensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
rjúkjúská	rjúkjúský	k2eAgFnSc1d1	rjúkjúský
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
luchenská	luchenský	k2eAgFnSc1d1	luchenský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
massoniana	massonian	k1gMnSc2	massonian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Massonova	Massonův	k2eAgFnSc1d1	Massonův
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
latteri	latter	k1gFnSc2	latter
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
merkusii	merkusie	k1gFnSc3	merkusie
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
mugo	mugo	k1gMnSc1	mugo
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
kleč	kleč	k1gFnSc1	kleč
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
nigra	nigr	k1gMnSc2	nigr
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
černá	černý	k2eAgFnSc1d1	černá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
resinosa	resinosa	k1gFnSc1	resinosa
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
smolná	smolný	k2eAgFnSc1d1	smolná
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
sylvestris	sylvestris	k1gFnSc2	sylvestris
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgFnSc2d1	lesní
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
tabuliformis	tabuliformis	k1gFnSc2	tabuliformis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
deskovitá	deskovitý	k2eAgFnSc1d1	deskovitá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
taiwanensis	taiwanensis	k1gFnSc2	taiwanensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
tchajwanská	tchajwanský	k2eAgFnSc1d1	tchajwanská
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
thunbergii	thunbergie	k1gFnSc4	thunbergie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Thunbergova	Thunbergův	k2eAgFnSc1d1	Thunbergův
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
tropicalis	tropicalis	k1gFnSc2	tropicalis
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
uncinata	uncinat	k1gMnSc2	uncinat
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
zobanitá	zobanitý	k2eAgFnSc1d1	zobanitý
<g/>
,	,	kIx,	,
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
poddruhy	poddruh	k1gInPc7	poddruh
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
pyrenejská	pyrenejský	k2eAgFnSc1d1	pyrenejská
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
blatka	blatka	k1gFnSc1	blatka
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
všechny	všechen	k3xTgFnPc1	všechen
též	též	k9	též
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
poddruhy	poddruh	k1gInPc4	poddruh
borovice	borovice	k1gFnSc2	borovice
kleče	kleč	k1gFnSc2	kleč
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
yunnanensis	yunnanensis	k1gFnSc2	yunnanensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
jünnanská	jünnanský	k2eAgFnSc1d1	jünnanský
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Pinaster	Pinastra	k1gFnPc2	Pinastra
–	–	k?	–
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
borovice	borovice	k1gFnSc2	borovice
dlouholisté	dlouholistý	k2eAgNnSc4d1	dlouholistý
středomořské	středomořský	k2eAgNnSc4d1	středomořské
rozšíření	rozšíření	k1gNnSc4	rozšíření
<g/>
;	;	kIx,	;
šišky	šiška	k1gFnPc4	šiška
vždy	vždy	k6eAd1	vždy
bez	bez	k7c2	bez
osin	osina	k1gFnPc2	osina
nebo	nebo	k8xC	nebo
trnů	trn	k1gInPc2	trn
<g/>
,	,	kIx,	,
křídla	křídlo	k1gNnSc2	křídlo
semene	semeno	k1gNnSc2	semeno
pevně	pevně	k6eAd1	pevně
či	či	k8xC	či
volně	volně	k6eAd1	volně
připojená	připojený	k2eAgFnSc1d1	připojená
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
brutia	brutium	k1gNnSc2	brutium
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
kalabrijská	kalabrijský	k2eAgFnSc1d1	kalabrijská
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
borovice	borovice	k1gFnSc1	borovice
anatolská	anatolský	k2eAgFnSc1d1	Anatolská
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
canariensis	canariensis	k1gFnSc2	canariensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
kanárská	kanárský	k2eAgFnSc1d1	Kanárská
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
halepensis	halepensis	k1gFnSc2	halepensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
alepská	alepský	k2eAgFnSc1d1	alepský
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
heldreichii	heldreichie	k1gFnSc4	heldreichie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Heldreichova	Heldreichův	k2eAgFnSc1d1	Heldreichova
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
borovice	borovice	k1gFnSc1	borovice
bělokorá	bělokorý	k2eAgFnSc1d1	bělokorá
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
pinaster	pinaster	k1gMnSc1	pinaster
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
přímořská	přímořský	k2eAgFnSc1d1	přímořská
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
borovice	borovice	k1gFnSc1	borovice
hvězdovitá	hvězdovitý	k2eAgFnSc1d1	hvězdovitá
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
pinea	pinea	k1gMnSc1	pinea
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
pinie	pinie	k1gFnSc2	pinie
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
roxburghii	roxburghie	k1gFnSc4	roxburghie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
dlouholistá	dlouholistý	k2eAgFnSc1d1	dlouholistá
</s>
</p>
<p>
<s>
Sekce	sekce	k1gFnSc1	sekce
Trifoliae	Trifolia	k1gFnSc2	Trifolia
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Attenuatae	Attenuatae	k1gFnPc2	Attenuatae
–	–	k?	–
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
borovice	borovice	k1gFnSc2	borovice
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
adaptované	adaptovaný	k2eAgFnPc1d1	adaptovaná
na	na	k7c4	na
lesní	lesní	k2eAgInPc4d1	lesní
požáry	požár	k1gInPc4	požár
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
tuto	tento	k3xDgFnSc4	tento
sekci	sekce	k1gFnSc4	sekce
neuznávají	uznávat	k5eNaImIp3nP	uznávat
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nP	řadit
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
bazální	bazální	k2eAgFnSc4d1	bazální
větev	větev	k1gFnSc4	větev
následující	následující	k2eAgFnSc2d1	následující
sekce	sekce	k1gFnSc2	sekce
Australes	Australes	k1gMnSc1	Australes
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
attenuata	attenuata	k1gFnSc1	attenuata
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
zúžená	zúžený	k2eAgFnSc1d1	zúžená
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
hrbolkatá	hrbolkatý	k2eAgFnSc1d1	hrbolkatá
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
muricata	muricata	k1gFnSc1	muricata
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
ostnitá	ostnitý	k2eAgFnSc1d1	ostnitá
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnSc1	borovice
biskupská	biskupský	k2eAgFnSc1d1	biskupská
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
radiata	radiata	k1gFnSc1	radiata
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
paprsčitá	paprsčitý	k2eAgFnSc1d1	paprsčitá
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
borovice	borovice	k1gFnSc1	borovice
monterejská	monterejský	k2eAgFnSc1d1	monterejský
<g/>
,	,	kIx,	,
3	[number]	k4	3
jehlice	jehlice	k1gFnPc4	jehlice
<g/>
,	,	kIx,	,
var.	var.	k?	var.
binata	binata	k1gFnSc1	binata
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
glabra	glabra	k1gMnSc1	glabra
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Australes	Australes	k1gInSc1	Australes
–	–	k?	–
jehlice	jehlice	k1gFnSc1	jehlice
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
až	až	k6eAd1	až
po	po	k7c6	po
pěti	pět	k4xCc6	pět
<g/>
;	;	kIx,	;
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Karibik	Karibik	k1gInSc1	Karibik
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
caribaea	caribaea	k1gMnSc1	caribaea
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
karibská	karibský	k2eAgFnSc1d1	karibská
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
cubensis	cubensis	k1gFnSc2	cubensis
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
echinata	echinat	k1gMnSc2	echinat
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
ježatá	ježatý	k2eAgFnSc1d1	ježatá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
elliottii	elliottie	k1gFnSc4	elliottie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Elliottova	Elliottův	k2eAgFnSc1d1	Elliottova
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
greggii	greggie	k1gFnSc4	greggie
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
herrerae	herreraat	k5eAaPmIp3nS	herreraat
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
jaliscana	jaliscan	k1gMnSc2	jaliscan
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
chaliská	chaliskat	k5eAaPmIp3nS	chaliskat
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
lawsonii	lawsonie	k1gFnSc4	lawsonie
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
leiophylla	leiophylla	k1gMnSc1	leiophylla
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
lumholtzii	lumholtzie	k1gFnSc4	lumholtzie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Lumholtzova	Lumholtzův	k2eAgFnSc1d1	Lumholtzův
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
occidentalis	occidentalis	k1gFnSc2	occidentalis
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
oocarpa	oocarp	k1gMnSc2	oocarp
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
palustris	palustris	k1gFnSc2	palustris
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
bahenní	bahenní	k2eAgFnSc2d1	bahenní
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
patula	patulum	k1gNnSc2	patulum
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
rozložená	rozložený	k2eAgFnSc1d1	rozložená
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
pringlei	pringle	k1gFnSc2	pringle
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
praetermissa	praetermiss	k1gMnSc2	praetermiss
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
přehlížená	přehlížený	k2eAgFnSc1d1	přehlížená
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
pungens	pungens	k1gInSc1	pungens
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
pichlavá	pichlavý	k2eAgFnSc1d1	pichlavá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
rigida	rigid	k1gMnSc2	rigid
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
tuhá	tuhý	k2eAgNnPc4d1	tuhé
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
serotina	serotina	k1gFnSc1	serotina
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
pozdní	pozdní	k2eAgFnSc2d1	pozdní
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
taeda	taeda	k1gMnSc1	taeda
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
kadidlová	kadidlový	k2eAgFnSc1d1	Kadidlová
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
tecunumanii	tecunumanie	k1gFnSc4	tecunumanie
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
teocote	teocot	k1gMnSc5	teocot
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Contortae	Contorta	k1gFnSc2	Contorta
–	–	k?	–
jehlice	jehlice	k1gFnSc2	jehlice
po	po	k7c4	po
2	[number]	k4	2
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
banksiana	banksian	k1gMnSc2	banksian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Banksova	Banksův	k2eAgFnSc1d1	Banksova
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
contorta	contort	k1gMnSc2	contort
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
pokroucená	pokroucený	k2eAgFnSc1d1	pokroucená
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
clausa	claus	k1gMnSc2	claus
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
virginiana	virginian	k1gMnSc2	virginian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
virginská	virginský	k2eAgFnSc1d1	virginská
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Ponderosae	Ponderosa	k1gFnSc2	Ponderosa
–	–	k?	–
jehlice	jehlice	k1gFnSc2	jehlice
po	po	k7c4	po
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
5	[number]	k4	5
nebo	nebo	k8xC	nebo
8	[number]	k4	8
<g/>
,	,	kIx,	,
opadavé	opadavý	k2eAgNnSc1d1	opadavé
křídlo	křídlo	k1gNnSc1	křídlo
semene	semeno	k1gNnSc2	semeno
<g/>
;	;	kIx,	;
Střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
západ	západ	k1gInSc1	západ
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jihozápad	jihozápad	k1gInSc1	jihozápad
Kanady	Kanada	k1gFnSc2	Kanada
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
apulcensis	apulcensis	k1gFnSc2	apulcensis
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
arizonica	arizonica	k1gMnSc1	arizonica
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
arizonská	arizonský	k2eAgFnSc1d1	Arizonská
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
cooperi	cooper	k1gFnSc2	cooper
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
coulteri	coulter	k1gFnSc2	coulter
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Coulterova	Coulterův	k2eAgFnSc1d1	Coulterův
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
devoniana	devonian	k1gMnSc2	devonian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
mičoakánská	mičoakánský	k2eAgFnSc1d1	mičoakánský
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
douglasiana	douglasian	k1gMnSc2	douglasian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Douglasova	Douglasův	k2eAgFnSc1d1	Douglasova
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
durangensis	durangensis	k1gFnSc2	durangensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
durangská	durangskat	k5eAaPmIp3nS	durangskat
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
engelmannii	engelmannie	k1gFnSc4	engelmannie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Engelmannova	Engelmannov	k1gInSc2	Engelmannov
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
hartwegii	hartwegie	k1gFnSc4	hartwegie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Hartwegova	Hartwegův	k2eAgFnSc1d1	Hartwegův
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
jeffreyi	jeffrey	k1gFnSc2	jeffrey
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Jeffreyova	Jeffreyovo	k1gNnSc2	Jeffreyovo
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
maximinoi	maximino	k1gFnSc2	maximino
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
montezumae	montezuma	k1gFnSc2	montezuma
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Montezumova	Montezumův	k2eAgFnSc1d1	Montezumova
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
ponderosa	ponderosa	k1gFnSc1	ponderosa
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
těžká	těžký	k2eAgFnSc1d1	těžká
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
pseudostrobus	pseudostrobus	k1gMnSc1	pseudostrobus
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
pavejmutka	pavejmutka	k1gFnSc1	pavejmutka
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
sabiniana	sabinian	k1gMnSc2	sabinian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Sabineova	Sabineův	k2eAgFnSc1d1	Sabineův
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
torreyana	torreyan	k1gMnSc2	torreyan
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Torreyova	Torreyovo	k1gNnSc2	Torreyovo
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
washoensis	washoensis	k1gFnSc2	washoensis
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
yecorensis	yecorensis	k1gFnSc2	yecorensis
</s>
</p>
<p>
<s>
===	===	k?	===
Podrod	podrod	k1gInSc1	podrod
Strobus	Strobus	k1gInSc1	Strobus
===	===	k?	===
</s>
</p>
<p>
<s>
Též	též	k9	též
Haploxylon	Haploxylon	k1gInSc4	Haploxylon
či	či	k8xC	či
"	"	kIx"	"
<g/>
měkké	měkký	k2eAgFnPc4d1	měkká
<g/>
"	"	kIx"	"
borovice	borovice	k1gFnPc4	borovice
<g/>
,	,	kIx,	,
cca	cca	kA	cca
40	[number]	k4	40
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
3	[number]	k4	3
nebo	nebo	k8xC	nebo
5	[number]	k4	5
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
1	[number]	k4	1
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
)	)	kIx)	)
jehlic	jehlice	k1gFnPc2	jehlice
ve	v	k7c6	v
svazečku	svazeček	k1gInSc6	svazeček
s	s	k7c7	s
opadavou	opadavý	k2eAgFnSc7d1	opadavá
pochvou	pochva	k1gFnSc7	pochva
<g/>
,	,	kIx,	,
jehlice	jehlice	k1gFnSc1	jehlice
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
cévním	cévní	k2eAgInSc7d1	cévní
svazkem	svazek	k1gInSc7	svazek
<g/>
,	,	kIx,	,
průduchy	průduch	k1gInPc1	průduch
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
jehlice	jehlice	k1gFnSc2	jehlice
<g/>
.	.	kIx.	.
</s>
<s>
Pupek	pupek	k1gInSc1	pupek
(	(	kIx(	(
<g/>
umbo	umbo	k6eAd1	umbo
<g/>
)	)	kIx)	)
umístěn	umístit	k5eAaPmNgInS	umístit
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
semenné	semenný	k2eAgFnSc2d1	semenná
šupiny	šupina	k1gFnSc2	šupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sekce	sekce	k1gFnSc1	sekce
Quinquefoliae	Quinquefolia	k1gFnSc2	Quinquefolia
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Krempfianae	Krempfiana	k1gInSc2	Krempfiana
–	–	k?	–
2	[number]	k4	2
jehlice	jehlice	k1gFnSc2	jehlice
<g/>
,	,	kIx,	,
šišky	šiška	k1gFnSc2	šiška
bez	bez	k7c2	bez
trnů	trn	k1gInPc2	trn
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
krempfii	krempfie	k1gFnSc4	krempfie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Krempfova	Krempfův	k2eAgFnSc1d1	Krempfův
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Gerardianae	Gerardiana	k1gInSc2	Gerardiana
–	–	k?	–
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
jehlic	jehlice	k1gFnPc2	jehlice
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Himálaj	Himálaj	k1gFnSc1	Himálaj
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
bungeana	bungean	k1gMnSc2	bungean
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Bungeova	Bungeův	k2eAgFnSc1d1	Bungeův
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
gerardiana	gerardian	k1gMnSc2	gerardian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Gerardova	Gerardův	k2eAgFnSc1d1	Gerardova
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
squamata	squama	k1gNnPc1	squama
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
šupinatá	šupinatý	k2eAgFnSc1d1	šupinatá
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Strobus	Strobus	k1gInSc1	Strobus
–	–	k?	–
5	[number]	k4	5
jehlic	jehlice	k1gFnPc2	jehlice
<g/>
;	;	kIx,	;
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc1	Asie
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
albicaulis	albicaulis	k1gFnSc2	albicaulis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
bělokmenná	bělokmenný	k2eAgFnSc1d1	bělokmenný
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
amamiana	amamian	k1gMnSc2	amamian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
jakušimská	jakušimský	k2eAgFnSc1d1	jakušimský
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
anemophila	anemophil	k1gMnSc2	anemophil
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
větromilná	větromilný	k2eAgFnSc1d1	větromilný
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
armandii	armandie	k1gFnSc4	armandie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Armandova	Armandův	k2eAgFnSc1d1	Armandova
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
ayacahuite	ayacahuit	k1gInSc5	ayacahuit
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
mexická	mexický	k2eAgFnSc1d1	mexická
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
bhutanica	bhutanica	k1gMnSc1	bhutanica
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
cembra	cembra	k1gMnSc1	cembra
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
limba	limba	k1gFnSc1	limba
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
chiapensis	chiapensis	k1gFnSc2	chiapensis
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
dabeshanensis	dabeshanensis	k1gFnSc2	dabeshanensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
dabiešanská	dabiešanský	k2eAgFnSc1d1	dabiešanský
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
dalatensis	dalatensis	k1gFnSc2	dalatensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
dalatská	dalatský	k2eAgFnSc1d1	dalatský
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
fenzeliana	fenzelian	k1gMnSc2	fenzelian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Fenzelova	Fenzelův	k2eAgFnSc1d1	Fenzelův
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
flexilis	flexilis	k1gFnSc2	flexilis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
ohebná	ohebný	k2eAgFnSc1d1	ohebná
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
koraiensis	koraiensis	k1gFnSc2	koraiensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
korejská	korejský	k2eAgFnSc1d1	Korejská
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
kwangtungensis	kwangtungensis	k1gFnSc2	kwangtungensis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
kwangtungská	kwangtungskat	k5eAaPmIp3nS	kwangtungskat
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
lambertiana	lambertian	k1gMnSc2	lambertian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Lambertova	Lambertův	k2eAgFnSc1d1	Lambertova
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
monticola	monticola	k1gFnSc1	monticola
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
pohorská	pohorský	k2eAgFnSc1d1	Pohorská
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
morrisonicola	morrisonicola	k1gFnSc1	morrisonicola
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
morrisonská	morrisonský	k2eAgFnSc1d1	morrisonský
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
parviflora	parviflor	k1gMnSc2	parviflor
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
drobnokvětá	drobnokvětý	k2eAgFnSc1d1	drobnokvětá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
peuce	peuce	k1gFnSc2	peuce
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
rumelská	rumelský	k2eAgFnSc1d1	rumelská
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
pumila	pumil	k1gMnSc2	pumil
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
zakrslá	zakrslý	k2eAgFnSc1d1	zakrslá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
sibirica	sibirica	k1gMnSc1	sibirica
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
strobiformis	strobiformis	k1gFnSc2	strobiformis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
vejmutovkovitá	vejmutovkovitý	k2eAgFnSc1d1	vejmutovkovitý
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
strobus	strobus	k1gMnSc1	strobus
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
wallichiana	wallichian	k1gMnSc2	wallichian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
himalájská	himalájský	k2eAgFnSc1d1	himalájská
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
wangii	wangie	k1gFnSc4	wangie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
WangovaSekce	WangovaSekce	k1gFnSc1	WangovaSekce
Parrya	Parrya	k1gFnSc1	Parrya
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Nelsoniae	Nelsonia	k1gFnSc2	Nelsonia
–	–	k?	–
jehlice	jehlice	k1gFnSc1	jehlice
po	po	k7c6	po
třech	tři	k4xCgMnPc6	tři
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
bez	bez	k7c2	bez
křídel	křídlo	k1gNnPc2	křídlo
<g/>
;	;	kIx,	;
severovýchodní	severovýchodní	k2eAgNnSc1d1	severovýchodní
Mexiko	Mexiko	k1gNnSc1	Mexiko
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
nelsonii	nelsonie	k1gFnSc4	nelsonie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Nelsonova	Nelsonův	k2eAgFnSc1d1	Nelsonova
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Rzedowskianae	Rzedowskianae	k1gFnPc2	Rzedowskianae
–	–	k?	–
Mexiko	Mexiko	k1gNnSc4	Mexiko
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
maximartinezii	maximartinezie	k1gFnSc4	maximartinezie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Martínezova	Martínezův	k2eAgFnSc1d1	Martínezův
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
pinceana	pincean	k1gMnSc2	pincean
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Pinceova	Pinceův	k2eAgFnSc1d1	Pinceův
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc4	Pinus
rzedowskii	rzedowskie	k1gFnSc4	rzedowskie
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Rzedowského	Rzedowského	k2eAgFnSc2d1	Rzedowského
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Cembroides	Cembroidesa	k1gFnPc2	Cembroidesa
–	–	k?	–
jehlice	jehlice	k1gFnSc2	jehlice
po	po	k7c6	po
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
po	po	k7c4	po
6	[number]	k4	6
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc4	semeno
obvykle	obvykle	k6eAd1	obvykle
bez	bez	k7c2	bez
křídel	křídlo	k1gNnPc2	křídlo
<g/>
;	;	kIx,	;
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
jihozápad	jihozápad	k1gInSc1	jihozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
cembroides	cembroides	k1gMnSc1	cembroides
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
limbovitá	limbovitý	k2eAgFnSc1d1	limbovitý
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
culminicola	culminicola	k1gFnSc1	culminicola
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
vrcholová	vrcholový	k2eAgFnSc1d1	vrcholová
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
discolor	discolor	k1gMnSc1	discolor
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
edulis	edulis	k1gFnSc2	edulis
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
jedlá	jedlý	k2eAgFnSc1d1	jedlá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
johannis	johannis	k1gFnSc2	johannis
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
monophylla	monophylla	k1gMnSc1	monophylla
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
jednolistá	jednolistý	k2eAgFnSc1d1	jednolistá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
orizabensis	orizabensis	k1gFnSc2	orizabensis
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
quadrifolia	quadrifolium	k1gNnSc2	quadrifolium
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
remota	remot	k1gMnSc2	remot
</s>
</p>
<p>
<s>
Podsekce	Podsekce	k1gFnSc1	Podsekce
Balfourianae	Balfouriana	k1gFnSc2	Balfouriana
–	–	k?	–
jehlice	jehlice	k1gFnSc2	jehlice
po	po	k7c4	po
5	[number]	k4	5
<g/>
,	,	kIx,	,
jihozápad	jihozápad	k1gInSc1	jihozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
aristata	aristat	k1gMnSc2	aristat
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
osinatá	osinatý	k2eAgFnSc1d1	osinatá
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
balfouriana	balfourian	k1gMnSc2	balfourian
–	–	k?	–
borovice	borovice	k1gFnSc2	borovice
Balfourova	Balfourův	k2eAgFnSc1d1	Balfourova
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
longaeva	longaevo	k1gNnSc2	longaevo
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
dlouhověká	dlouhověký	k2eAgFnSc1d1	dlouhověká
</s>
</p>
<p>
<s>
===	===	k?	===
Významnější	významný	k2eAgMnPc1d2	významnější
kříženci	kříženec	k1gMnPc1	kříženec
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgInPc1d1	mnohý
druhy	druh	k1gInPc1	druh
borovic	borovice	k1gFnPc2	borovice
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
i	i	k8xC	i
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
navzájem	navzájem	k6eAd1	navzájem
snadno	snadno	k6eAd1	snadno
kříží	křížit	k5eAaImIp3nP	křížit
a	a	k8xC	a
na	na	k7c6	na
překryvu	překryvo	k1gNnSc6	překryvo
areálů	areál	k1gInPc2	areál
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
nepřehledné	přehledný	k2eNgInPc1d1	nepřehledný
hybridní	hybridní	k2eAgInPc1d1	hybridní
roje	roj	k1gInPc1	roj
obtížně	obtížně	k6eAd1	obtížně
určitelných	určitelný	k2eAgMnPc2d1	určitelný
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
agregát	agregát	k1gInSc1	agregát
druhů	druh	k1gInPc2	druh
borovice	borovice	k1gFnSc2	borovice
kleče	kleč	k1gFnSc2	kleč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
jsou	být	k5eAaImIp3nP	být
uvedeni	uveden	k2eAgMnPc1d1	uveden
významnější	významný	k2eAgMnPc1d2	významnější
kříženci	kříženec	k1gMnPc1	kříženec
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
kulturních	kulturní	k2eAgFnPc6d1	kulturní
výsadbách	výsadba	k1gFnPc6	výsadba
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
×	×	k?	×
schwerinii	schwerinie	k1gFnSc4	schwerinie
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
Schwerinova	Schwerinův	k2eAgFnSc1d1	Schwerinova
<g/>
;	;	kIx,	;
kulturní	kulturní	k2eAgMnSc1d1	kulturní
kříženec	kříženec	k1gMnSc1	kříženec
borovice	borovice	k1gFnSc2	borovice
himálajské	himálajský	k2eAgFnSc2d1	himálajská
a	a	k8xC	a
vejmutovky	vejmutovka	k1gFnSc2	vejmutovka
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
vysazovaný	vysazovaný	k2eAgInSc1d1	vysazovaný
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
×	×	k?	×
hakkodensis	hakkodensis	k1gFnSc1	hakkodensis
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
honšúská	honšúská	k1gFnSc1	honšúská
<g/>
;	;	kIx,	;
kříženec	kříženec	k1gMnSc1	kříženec
borovice	borovice	k1gFnSc2	borovice
drobnokvěté	drobnokvětý	k2eAgFnSc2d1	drobnokvětá
a	a	k8xC	a
zakrslé	zakrslý	k2eAgFnSc2d1	zakrslá
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgFnSc2d1	vytvářející
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
přirozené	přirozený	k2eAgFnSc2d1	přirozená
populace	populace	k1gFnSc2	populace
na	na	k7c6	na
japonském	japonský	k2eAgInSc6d1	japonský
ostrově	ostrov	k1gInSc6	ostrov
Honšú	Honšú	k1gFnSc2	Honšú
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
×	×	k?	×
reflexa	reflex	k2eAgFnSc1d1	reflex
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
ohnutá	ohnutý	k2eAgFnSc1d1	ohnutá
<g/>
;	;	kIx,	;
přirozený	přirozený	k2eAgMnSc1d1	přirozený
kříženec	kříženec	k1gMnSc1	kříženec
severoamerických	severoamerický	k2eAgFnPc2d1	severoamerická
borovic	borovice	k1gFnPc2	borovice
P.	P.	kA	P.
flexilis	flexilis	k1gFnSc2	flexilis
a	a	k8xC	a
P.	P.	kA	P.
strobiformis	strobiformis	k1gInSc1	strobiformis
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
×	×	k?	×
funebris	funebris	k1gFnSc1	funebris
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
mandžuská	mandžuský	k2eAgFnSc1d1	mandžuská
<g/>
;	;	kIx,	;
přírodní	přírodní	k2eAgMnPc1d1	přírodní
kříženec	kříženec	k1gMnSc1	kříženec
borovice	borovice	k1gFnSc2	borovice
hustokvěté	hustokvětý	k2eAgFnSc2d1	hustokvětá
a	a	k8xC	a
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgFnSc2d1	vytvářející
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
populace	populace	k1gFnSc2	populace
na	na	k7c6	na
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
×	×	k?	×
murraybanksiana	murraybanksiana	k1gFnSc1	murraybanksiana
–	–	k?	–
spontánní	spontánní	k2eAgFnPc4d1	spontánní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
i	i	k8xC	i
kulturní	kulturní	k2eAgMnSc1d1	kulturní
kříženec	kříženec	k1gMnSc1	kříženec
borovice	borovice	k1gFnSc2	borovice
Banksovy	Banksův	k2eAgFnSc2d1	Banksova
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
poddruhů	poddruh	k1gInPc2	poddruh
borovice	borovice	k1gFnSc2	borovice
pokroucené	pokroucený	k2eAgInPc1d1	pokroucený
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
×	×	k?	×
rhaetica	rhaetic	k2eAgFnSc1d1	rhaetic
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
<g/>
;	;	kIx,	;
komplex	komplex	k1gInSc1	komplex
několika	několik	k4yIc2	několik
menších	malý	k2eAgInPc2d2	menší
taxonů	taxon	k1gInPc2	taxon
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
hybridizací	hybridizace	k1gFnSc7	hybridizace
různých	různý	k2eAgInPc2d1	různý
poddruhů	poddruh	k1gInPc2	poddruh
borovice	borovice	k1gFnSc2	borovice
zobanité	zobanitý	k2eAgFnSc2d1	zobanitý
a	a	k8xC	a
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgFnSc2d1	lesní
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gInSc1	Pinus
×	×	k?	×
ascendens	ascendens	k1gInSc1	ascendens
–	–	k?	–
borovice	borovice	k1gFnSc1	borovice
vystoupavá	vystoupavý	k2eAgFnSc1d1	vystoupavá
<g/>
;	;	kIx,	;
komplex	komplex	k1gInSc1	komplex
několika	několik	k4yIc2	několik
menších	malý	k2eAgInPc2d2	menší
taxonů	taxon	k1gInPc2	taxon
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
hybridizací	hybridizace	k1gFnSc7	hybridizace
různých	různý	k2eAgInPc2d1	různý
poddruhů	poddruh	k1gInPc2	poddruh
borovice	borovice	k1gFnSc2	borovice
zobanité	zobanitý	k2eAgMnPc4d1	zobanitý
a	a	k8xC	a
borovice	borovice	k1gFnSc1	borovice
kleče	kleč	k1gFnSc2	kleč
<g/>
;	;	kIx,	;
varianta	varianta	k1gFnSc1	varianta
skalickyi	skalicky	k1gFnSc2	skalicky
(	(	kIx(	(
<g/>
kříženec	kříženec	k1gMnSc1	kříženec
kleče	kleč	k1gFnSc2	kleč
a	a	k8xC	a
blatky	blatka	k1gFnSc2	blatka
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
popisovaný	popisovaný	k2eAgMnSc1d1	popisovaný
jako	jako	k8xS	jako
Pinus	Pinus	k1gMnSc1	Pinus
×	×	k?	×
pseudopumilio	pseudopumilio	k1gMnSc1	pseudopumilio
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
rašelinná	rašelinný	k2eAgFnSc1d1	rašelinná
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ekologicky	ekologicky	k6eAd1	ekologicky
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
středoevropská	středoevropský	k2eAgNnPc4d1	středoevropské
rašeliniště	rašeliniště	k1gNnPc4	rašeliniště
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pryskyřice	pryskyřice	k1gFnPc4	pryskyřice
a	a	k8xC	a
silice	silice	k1gFnPc4	silice
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
aromatickou	aromatický	k2eAgFnSc4d1	aromatická
pryskyřici	pryskyřice	k1gFnSc4	pryskyřice
jsou	být	k5eAaImIp3nP	být
bohaté	bohatý	k2eAgInPc1d1	bohatý
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
borovic	borovice	k1gFnPc2	borovice
<g/>
,	,	kIx,	,
využívány	využívat	k5eAaImNgInP	využívat
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
druhy	druh	k1gInPc1	druh
"	"	kIx"	"
<g/>
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
borovic	borovice	k1gFnPc2	borovice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
podrodu	podrod	k1gInSc2	podrod
Pinus	Pinus	k1gInSc4	Pinus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
středomořská	středomořský	k2eAgFnSc1d1	středomořská
borovice	borovice	k1gFnSc1	borovice
přímořská	přímořský	k2eAgFnSc1d1	přímořská
<g/>
,	,	kIx,	,
kalábrijská	kalábrijský	k2eAgFnSc1d1	kalábrijská
nebo	nebo	k8xC	nebo
halepská	halepský	k2eAgFnSc1d1	halepská
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
borovice	borovice	k1gFnSc2	borovice
Elliotova	Elliotův	k2eAgInSc2d1	Elliotův
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
bahenní	bahenní	k2eAgFnPc1d1	bahenní
nebo	nebo	k8xC	nebo
asijské	asijský	k2eAgFnPc1d1	asijská
borovice	borovice	k1gFnPc1	borovice
dlouholistá	dlouholistý	k2eAgFnSc1d1	dlouholistá
či	či	k8xC	či
Massonova	Massonův	k2eAgFnSc1d1	Massonův
<g/>
;	;	kIx,	;
k	k	k7c3	k
hlavním	hlavní	k2eAgMnPc3d1	hlavní
světovým	světový	k2eAgMnPc3d1	světový
producentům	producent	k1gMnPc3	producent
patří	patřit	k5eAaImIp3nS	patřit
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
též	též	k9	též
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velkou	velký	k2eAgFnSc4d1	velká
produkci	produkce	k1gFnSc4	produkce
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
nebo	nebo	k8xC	nebo
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rostou	růst	k5eAaImIp3nP	růst
borovice	borovice	k1gFnPc4	borovice
pouze	pouze	k6eAd1	pouze
introdukovaně	introdukovaně	k6eAd1	introdukovaně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
tradiční	tradiční	k2eAgInPc4d1	tradiční
způsoby	způsob	k1gInPc4	způsob
těžby	těžba	k1gFnSc2	těžba
spočívající	spočívající	k2eAgInPc4d1	spočívající
v	v	k7c6	v
nařezávání	nařezávání	k1gNnSc6	nařezávání
a	a	k8xC	a
odkorňování	odkorňování	k1gNnSc6	odkorňování
živých	živý	k2eAgInPc2d1	živý
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
zachytávání	zachytávání	k1gNnSc2	zachytávání
prýštící	prýštící	k2eAgFnSc1d1	prýštící
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
smolaření	smolaření	k1gNnSc4	smolaření
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
zakázány	zakázat	k5eAaPmNgFnP	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
využívala	využívat	k5eAaImAgFnS	využívat
jednak	jednak	k8xC	jednak
mechanicky	mechanicky	k6eAd1	mechanicky
pro	pro	k7c4	pro
utěsňování	utěsňování	k1gNnSc4	utěsňování
a	a	k8xC	a
impregnaci	impregnace	k1gFnSc4	impregnace
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
jako	jako	k9	jako
léčivo	léčivo	k1gNnSc4	léčivo
nebo	nebo	k8xC	nebo
přírodní	přírodní	k2eAgNnSc4d1	přírodní
lepidlo	lepidlo	k1gNnSc4	lepidlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
zdrojem	zdroj	k1gInSc7	zdroj
přírodního	přírodní	k2eAgInSc2d1	přírodní
terpentýnu	terpentýn	k1gInSc2	terpentýn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
destilačním	destilační	k2eAgInSc7d1	destilační
zbytkem	zbytek	k1gInSc7	zbytek
(	(	kIx(	(
<g/>
kalafunou	kalafuna	k1gFnSc7	kalafuna
<g/>
)	)	kIx)	)
výchozí	výchozí	k2eAgFnSc7d1	výchozí
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
množství	množství	k1gNnSc4	množství
dalších	další	k2eAgInPc2d1	další
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
laky	lak	k1gInPc1	lak
<g/>
,	,	kIx,	,
rozpouštědla	rozpouštědlo	k1gNnPc1	rozpouštědlo
k	k	k7c3	k
ředění	ředění	k1gNnSc3	ředění
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
insekticidní	insekticidní	k2eAgInPc4d1	insekticidní
přípravky	přípravek	k1gInPc4	přípravek
<g/>
,	,	kIx,	,
gumy	guma	k1gFnPc4	guma
<g/>
,	,	kIx,	,
tiskové	tiskový	k2eAgFnPc4d1	tisková
barvy	barva	k1gFnPc4	barva
atd.	atd.	kA	atd.
Ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
borovice	borovice	k1gFnSc2	borovice
černé	černá	k1gFnSc2	černá
prosyceného	prosycený	k2eAgInSc2d1	prosycený
pryskyřicí	pryskyřice	k1gFnSc7	pryskyřice
se	se	k3xPyFc4	se
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
louče	louč	k1gFnPc1	louč
na	na	k7c4	na
svícení	svícení	k1gNnSc4	svícení
<g/>
.	.	kIx.	.
</s>
<s>
Pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
borovice	borovice	k1gFnSc2	borovice
halepské	halepský	k2eAgFnSc2d1	halepská
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
přidávána	přidáván	k2eAgNnPc1d1	přidáváno
do	do	k7c2	do
bílého	bílý	k2eAgNnSc2d1	bílé
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
nazývaného	nazývaný	k2eAgNnSc2d1	nazývané
retsina	retsin	k2eAgNnSc2d1	retsin
<g/>
.	.	kIx.	.
<g/>
Borová	borový	k2eAgFnSc1d1	Borová
silice	silice	k1gFnSc1	silice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
terpenických	terpenický	k2eAgFnPc2d1	terpenický
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
alfa	alfa	k1gNnSc1	alfa
a	a	k8xC	a
beta	beta	k1gNnSc1	beta
pinen	pinen	k1gInSc1	pinen
<g/>
,	,	kIx,	,
limonen	limonen	k1gInSc1	limonen
<g/>
,	,	kIx,	,
kamfen	kamfen	k1gInSc1	kamfen
<g/>
,	,	kIx,	,
felandren	felandrna	k1gFnPc2	felandrna
<g/>
,	,	kIx,	,
geraniol	geraniola	k1gFnPc2	geraniola
<g/>
,	,	kIx,	,
linalool	linaloola	k1gFnPc2	linaloola
<g/>
,	,	kIx,	,
citronellol	citronellola	k1gFnPc2	citronellola
<g/>
,	,	kIx,	,
borneol	borneola	k1gFnPc2	borneola
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Éterické	éterický	k2eAgInPc1d1	éterický
oleje	olej	k1gInPc1	olej
se	se	k3xPyFc4	se
destilují	destilovat	k5eAaImIp3nP	destilovat
především	především	k9	především
z	z	k7c2	z
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgFnSc2d1	lesní
a	a	k8xC	a
z	z	k7c2	z
kosodřeviny	kosodřevina	k1gFnSc2	kosodřevina
(	(	kIx(	(
<g/>
kleče	kleče	k6eAd1	kleče
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
parní	parní	k2eAgNnSc4d1	parní
destilací	destilace	k1gFnSc7	destilace
z	z	k7c2	z
čerstvých	čerstvý	k2eAgFnPc2d1	čerstvá
větviček	větvička	k1gFnPc2	větvička
<g/>
,	,	kIx,	,
jehlic	jehlice	k1gFnPc2	jehlice
a	a	k8xC	a
šišek	šiška	k1gFnPc2	šiška
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
zklidňující	zklidňující	k2eAgInPc4d1	zklidňující
<g/>
,	,	kIx,	,
analgetické	analgetický	k2eAgInPc4d1	analgetický
<g/>
,	,	kIx,	,
antiseptické	antiseptický	k2eAgInPc4d1	antiseptický
a	a	k8xC	a
antibakteriální	antibakteriální	k2eAgInPc4d1	antibakteriální
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
užívají	užívat	k5eAaImIp3nP	užívat
se	se	k3xPyFc4	se
v	v	k7c6	v
aromaterapii	aromaterapie	k1gFnSc6	aromaterapie
i	i	k8xC	i
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
různých	různý	k2eAgFnPc2d1	různá
antirevmatických	antirevmatický	k2eAgFnPc2d1	antirevmatická
<g/>
,	,	kIx,	,
hojivých	hojivý	k2eAgFnPc2d1	hojivá
a	a	k8xC	a
masážních	masážní	k2eAgFnPc2d1	masážní
mastí	mast	k1gFnPc2	mast
<g/>
,	,	kIx,	,
při	při	k7c6	při
bolavých	bolavý	k2eAgInPc6d1	bolavý
kloubech	kloub	k1gInPc6	kloub
<g/>
,	,	kIx,	,
artritidě	artritida	k1gFnSc6	artritida
nebo	nebo	k8xC	nebo
různých	různý	k2eAgFnPc6d1	různá
pohmožděninách	pohmožděnina	k1gFnPc6	pohmožděnina
a	a	k8xC	a
drobných	drobný	k2eAgNnPc6d1	drobné
poraněních	poranění	k1gNnPc6	poranění
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ke	k	k7c3	k
koupelím	koupel	k1gFnPc3	koupel
<g/>
,	,	kIx,	,
potírání	potírání	k1gNnSc3	potírání
a	a	k8xC	a
inhalacím	inhalace	k1gFnPc3	inhalace
při	při	k7c6	při
nachlazení	nachlazení	k1gNnSc6	nachlazení
a	a	k8xC	a
chorobách	choroba	k1gFnPc6	choroba
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
při	při	k7c6	při
péči	péče	k1gFnSc6	péče
o	o	k7c4	o
pleť	pleť	k1gFnSc4	pleť
a	a	k8xC	a
kožních	kožní	k2eAgFnPc6d1	kožní
nemocech	nemoc	k1gFnPc6	nemoc
včetně	včetně	k7c2	včetně
lupénky	lupénka	k1gFnSc2	lupénka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
příjemná	příjemný	k2eAgFnSc1d1	příjemná
vůně	vůně	k1gFnSc1	vůně
v	v	k7c6	v
aromaterapii	aromaterapie	k1gFnSc6	aromaterapie
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zklidňovat	zklidňovat	k5eAaImF	zklidňovat
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
ulevovat	ulevovat	k5eAaImF	ulevovat
při	při	k7c6	při
stresu	stres	k1gInSc6	stres
<g/>
,	,	kIx,	,
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
úzkost	úzkost	k1gFnSc4	úzkost
a	a	k8xC	a
tenzi	tenze	k1gFnSc4	tenze
a	a	k8xC	a
osvěžovat	osvěžovat	k5eAaImF	osvěžovat
mysl	mysl	k1gFnSc4	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
různých	různý	k2eAgInPc2d1	různý
parfémů	parfém	k1gInPc2	parfém
<g/>
,	,	kIx,	,
aromatických	aromatický	k2eAgNnPc2d1	aromatické
mýdel	mýdlo	k1gNnPc2	mýdlo
<g/>
,	,	kIx,	,
osvěžovačů	osvěžovač	k1gMnPc2	osvěžovač
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
jehličí	jehličí	k1gNnSc1	jehličí
a	a	k8xC	a
kůra	kůra	k1gFnSc1	kůra
===	===	k?	===
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnPc1	borovice
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
dřevo	dřevo	k1gNnSc4	dřevo
s	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
spektrem	spektrum	k1gNnSc7	spektrum
využití	využití	k1gNnSc2	využití
<g/>
;	;	kIx,	;
vedle	vedle	k7c2	vedle
smrku	smrk	k1gInSc2	smrk
jsou	být	k5eAaImIp3nP	být
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
jehličnatým	jehličnatý	k2eAgInSc7d1	jehličnatý
stromem	strom	k1gInSc7	strom
v	v	k7c6	v
dřevařském	dřevařský	k2eAgInSc6d1	dřevařský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
druhovému	druhový	k2eAgNnSc3d1	druhové
bohatství	bohatství	k1gNnSc3	bohatství
a	a	k8xC	a
proměnlivosti	proměnlivost	k1gFnSc2	proměnlivost
borovic	borovice	k1gFnPc2	borovice
existují	existovat	k5eAaImIp3nP	existovat
značné	značný	k2eAgInPc1d1	značný
rozdíly	rozdíl	k1gInPc1	rozdíl
i	i	k9	i
v	v	k7c6	v
charakteristice	charakteristika	k1gFnSc6	charakteristika
jejich	jejich	k3xOp3gNnSc2	jejich
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
vzato	vzít	k5eAaPmNgNnS	vzít
je	být	k5eAaImIp3nS	být
měkčí	měkký	k2eAgNnSc1d2	měkčí
až	až	k9	až
středně	středně	k6eAd1	středně
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
opracovatelné	opracovatelný	k2eAgNnSc1d1	opracovatelné
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
silně	silně	k6eAd1	silně
pryskyřičné	pryskyřičný	k2eAgFnPc1d1	pryskyřičná
<g/>
,	,	kIx,	,
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
kresbou	kresba	k1gFnSc7	kresba
letokruhů	letokruh	k1gInPc2	letokruh
<g/>
;	;	kIx,	;
bělové	bělový	k2eAgNnSc1d1	Bělové
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
krémové	krémový	k2eAgFnPc4d1	krémová
až	až	k8xS	až
okrové	okrový	k2eAgFnPc4d1	okrová
<g/>
,	,	kIx,	,
jádrové	jádrový	k2eAgFnPc4d1	jádrová
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
oranžové	oranžový	k2eAgFnSc2d1	oranžová
<g/>
,	,	kIx,	,
načervenalé	načervenalý	k2eAgFnSc2d1	načervenalá
a	a	k8xC	a
hnědé	hnědý	k2eAgFnSc2d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
hoří	hořet	k5eAaImIp3nS	hořet
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
borovic	borovice	k1gFnPc2	borovice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gInSc1	Pinus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
a	a	k8xC	a
hutnější	hutný	k2eAgInSc1d2	hutnější
–	–	k?	–
z	z	k7c2	z
často	často	k6eAd1	často
průmyslově	průmyslově	k6eAd1	průmyslově
využívaných	využívaný	k2eAgInPc2d1	využívaný
druhů	druh	k1gInPc2	druh
více	hodně	k6eAd2	hodně
např.	např.	kA	např.
u	u	k7c2	u
borovice	borovice	k1gFnSc2	borovice
bahenní	bahenní	k2eAgFnSc2d1	bahenní
(	(	kIx(	(
<g/>
650	[number]	k4	650
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kadidlové	kadidlový	k2eAgNnSc1d1	kadidlový
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
u	u	k7c2	u
borovice	borovice	k1gFnSc2	borovice
těžké	těžký	k2eAgFnSc2d1	těžká
(	(	kIx(	(
<g/>
450	[number]	k4	450
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jeffreyovy	Jeffreyov	k1gInPc1	Jeffreyov
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
550	[number]	k4	550
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
či	či	k8xC	či
černé	černý	k2eAgFnSc2d1	černá
–	–	k?	–
<g/>
,	,	kIx,	,
s	s	k7c7	s
hrubší	hrubý	k2eAgFnSc7d2	hrubší
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
výraznějšími	výrazný	k2eAgInPc7d2	výraznější
letokruhy	letokruh	k1gInPc7	letokruh
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
"	"	kIx"	"
<g/>
měkkých	měkký	k2eAgFnPc2d1	měkká
borovic	borovice	k1gFnPc2	borovice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Strobus	Strobus	k1gInSc1	Strobus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dřevo	dřevo	k1gNnSc1	dřevo
měkké	měkký	k2eAgNnSc1d1	měkké
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
odolné	odolný	k2eAgFnPc1d1	odolná
<g/>
,	,	kIx,	,
s	s	k7c7	s
jemnou	jemný	k2eAgFnSc7d1	jemná
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
méně	málo	k6eAd2	málo
výraznými	výrazný	k2eAgInPc7d1	výrazný
letokruhy	letokruh	k1gInPc7	letokruh
<g/>
;	;	kIx,	;
nejvyužívanějšími	využívaný	k2eAgFnPc7d3	nejvyužívanější
měkkými	měkký	k2eAgFnPc7d1	měkká
borovicemi	borovice	k1gFnPc7	borovice
jsou	být	k5eAaImIp3nP	být
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
(	(	kIx(	(
<g/>
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
400	[number]	k4	400
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
Lambertova	Lambertův	k2eAgFnSc1d1	Lambertova
a	a	k8xC	a
borovice	borovice	k1gFnSc1	borovice
pohorská	pohorský	k2eAgFnSc1d1	Pohorská
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
na	na	k7c4	na
okna	okno	k1gNnPc4	okno
a	a	k8xC	a
dveře	dveře	k1gFnPc4	dveře
včetně	včetně	k7c2	včetně
rámů	rám	k1gInPc2	rám
<g/>
,	,	kIx,	,
na	na	k7c4	na
trámoví	trámoví	k1gNnSc4	trámoví
<g/>
,	,	kIx,	,
podvaly	podval	k1gInPc4	podval
a	a	k8xC	a
"	"	kIx"	"
<g/>
polštáře	polštář	k1gInPc1	polštář
<g/>
"	"	kIx"	"
pod	pod	k7c4	pod
podlahy	podlaha	k1gFnPc4	podlaha
<g/>
,	,	kIx,	,
též	též	k9	též
jako	jako	k9	jako
stavební	stavební	k2eAgNnSc4d1	stavební
dřevo	dřevo	k1gNnSc4	dřevo
či	či	k8xC	či
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
buničiny	buničina	k1gFnSc2	buničina
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
smrku	smrk	k1gInSc3	smrk
se	se	k3xPyFc4	se
hůře	zle	k6eAd2	zle
natírá	natírat	k5eAaImIp3nS	natírat
<g/>
,	,	kIx,	,
lakuje	lakovat	k5eAaImIp3nS	lakovat
<g/>
,	,	kIx,	,
lepí	lepit	k5eAaImIp3nS	lepit
a	a	k8xC	a
moří	mořit	k5eAaImIp3nS	mořit
<g/>
,	,	kIx,	,
při	při	k7c6	při
nevhodném	vhodný	k2eNgNnSc6d1	nevhodné
zpracování	zpracování	k1gNnSc6	zpracování
časem	časem	k6eAd1	časem
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
<g/>
Ze	z	k7c2	z
stavebního	stavební	k2eAgNnSc2d1	stavební
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
dřevo	dřevo	k1gNnSc4	dřevo
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
odolné	odolný	k2eAgFnSc2d1	odolná
proti	proti	k7c3	proti
vlivu	vliv	k1gInSc3	vliv
vlhkosti	vlhkost	k1gFnSc2	vlhkost
–	–	k?	–
osvědčuje	osvědčovat	k5eAaImIp3nS	osvědčovat
se	se	k3xPyFc4	se
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
se	s	k7c7	s
střídajícím	střídající	k2eAgMnSc7d1	střídající
se	se	k3xPyFc4	se
suchem	sucho	k1gNnSc7	sucho
a	a	k8xC	a
vlhkem	vlhko	k1gNnSc7	vlhko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
křehkost	křehkost	k1gFnSc1	křehkost
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
pružnost	pružnost	k1gFnSc1	pružnost
<g/>
,	,	kIx,	,
i	i	k8xC	i
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
používat	používat	k5eAaImF	používat
na	na	k7c4	na
konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
prvky	prvek	k1gInPc4	prvek
namáhané	namáhaný	k2eAgFnSc2d1	namáhaná
ohybem	ohyb	k1gInSc7	ohyb
<g/>
.	.	kIx.	.
</s>
<s>
Vadou	vada	k1gFnSc7	vada
dřeva	dřevo	k1gNnSc2	dřevo
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
po	po	k7c6	po
čase	čas	k1gInSc6	čas
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
suky	suk	k1gInPc1	suk
<g/>
.	.	kIx.	.
<g/>
Kůra	kůra	k1gFnSc1	kůra
borovic	borovice	k1gFnPc2	borovice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
antioxidační	antioxidační	k2eAgFnPc4d1	antioxidační
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
flavonoidy	flavonoid	k1gInPc4	flavonoid
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc4	tříslovina
a	a	k8xC	a
množství	množství	k1gNnSc4	množství
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
konzumována	konzumovat	k5eAaBmNgFnS	konzumovat
např.	např.	kA	např.
americkými	americký	k2eAgMnPc7d1	americký
indiány	indián	k1gMnPc7	indián
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
staletí	staletí	k1gNnPc4	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
prostředek	prostředek	k1gInSc4	prostředek
proti	proti	k7c3	proti
kurdějím	kurděje	k1gFnPc3	kurděje
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaPmNgFnS	využívat
ruskými	ruský	k2eAgInPc7d1	ruský
kozáky	kozák	k1gInPc7	kozák
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
rovněž	rovněž	k9	rovněž
potravní	potravní	k2eAgInPc4d1	potravní
doplňky	doplněk	k1gInPc4	doplněk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Pycnogenol	Pycnogenol	k1gInSc1	Pycnogenol
<g/>
.	.	kIx.	.
</s>
<s>
Jehličí	jehličí	k1gNnSc1	jehličí
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
na	na	k7c4	na
podestýlky	podestýlka	k1gFnPc4	podestýlka
<g/>
,	,	kIx,	,
mulče	mulč	k1gInPc4	mulč
nebo	nebo	k8xC	nebo
k	k	k7c3	k
balení	balení	k1gNnSc3	balení
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
z	z	k7c2	z
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
<g/>
,	,	kIx,	,
pevných	pevný	k2eAgFnPc2d1	pevná
a	a	k8xC	a
ohebných	ohebný	k2eAgFnPc2d1	ohebná
jehlic	jehlice	k1gFnPc2	jehlice
borovice	borovice	k1gFnSc2	borovice
karibské	karibský	k2eAgFnSc2d1	karibská
se	se	k3xPyFc4	se
v	v	k7c6	v
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
pletou	plést	k5eAaImIp3nP	plést
ozdobné	ozdobný	k2eAgInPc1d1	ozdobný
rohože	rohož	k1gFnPc4	rohož
a	a	k8xC	a
koše	koš	k1gInPc4	koš
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
borového	borový	k2eAgNnSc2d1	borové
jehličí	jehličí	k1gNnSc2	jehličí
a	a	k8xC	a
čerstvých	čerstvý	k2eAgFnPc2d1	čerstvá
větviček	větvička	k1gFnPc2	větvička
či	či	k8xC	či
pupenů	pupen	k1gInPc2	pupen
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
a	a	k8xC	a
antioxidantů	antioxidant	k1gInPc2	antioxidant
zabraňujících	zabraňující	k2eAgInPc2d1	zabraňující
oxidačnímu	oxidační	k2eAgInSc3d1	oxidační
stresu	stres	k1gInSc2	stres
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
napomáhajících	napomáhající	k2eAgInPc2d1	napomáhající
celkovému	celkový	k2eAgNnSc3d1	celkové
pročištění	pročištění	k1gNnSc3	pročištění
a	a	k8xC	a
regeneraci	regenerace	k1gFnSc3	regenerace
organismu	organismus	k1gInSc2	organismus
a	a	k8xC	a
posílení	posílení	k1gNnSc4	posílení
imunity	imunita	k1gFnSc2	imunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Semena	semeno	k1gNnSc2	semeno
===	===	k?	===
</s>
</p>
<p>
<s>
Semena	semeno	k1gNnSc2	semeno
borovic	borovice	k1gFnPc2	borovice
jsou	být	k5eAaImIp3nP	být
jedlá	jedlý	k2eAgNnPc1d1	jedlé
a	a	k8xC	a
u	u	k7c2	u
druhů	druh	k1gInPc2	druh
s	s	k7c7	s
velkými	velký	k2eAgNnPc7d1	velké
semeny	semeno	k1gNnPc7	semeno
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
sloužila	sloužit	k5eAaImAgFnS	sloužit
lidem	člověk	k1gMnPc3	člověk
jako	jako	k8xS	jako
vydatný	vydatný	k2eAgInSc4d1	vydatný
zdroj	zdroj	k1gInSc4	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mají	mít	k5eAaImIp3nP	mít
značnou	značný	k2eAgFnSc4d1	značná
výživovou	výživový	k2eAgFnSc4d1	výživová
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
s	s	k7c7	s
mnoha	mnoho	k4c2	mnoho
druhy	druh	k1gInPc7	druh
ořechů	ořech	k1gInPc2	ořech
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
pekany	pekan	k1gInPc1	pekan
<g/>
,	,	kIx,	,
kešu	kešu	k6eAd1	kešu
či	či	k8xC	či
vlašské	vlašský	k2eAgInPc1d1	vlašský
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
arašídy	arašíd	k1gInPc1	arašíd
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
množství	množství	k1gNnSc4	množství
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
)	)	kIx)	)
a	a	k8xC	a
minerály	minerál	k1gInPc4	minerál
jako	jako	k8xS	jako
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
mangan	mangan	k1gInSc1	mangan
a	a	k8xC	a
zinek	zinek	k1gInSc1	zinek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
gastronomii	gastronomie	k1gFnSc6	gastronomie
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
využívat	využívat	k5eAaPmF	využívat
mnoha	mnoho	k4c2	mnoho
způsobyː	způsobyː	k?	způsobyː
syrové	syrový	k2eAgNnSc4d1	syrové
nebo	nebo	k8xC	nebo
pražené	pražený	k2eAgNnSc4d1	pražené
<g/>
,	,	kIx,	,
do	do	k7c2	do
salátů	salát	k1gInPc2	salát
a	a	k8xC	a
dezertů	dezert	k1gInPc2	dezert
blízkovýchodní	blízkovýchodní	k2eAgFnSc2d1	blízkovýchodní
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
,	,	kIx,	,
na	na	k7c4	na
zdobení	zdobení	k1gNnSc4	zdobení
pečiva	pečivo	k1gNnSc2	pečivo
nebo	nebo	k8xC	nebo
mleté	mletý	k2eAgInPc4d1	mletý
do	do	k7c2	do
kaše	kaše	k1gFnSc2	kaše
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1	neodmyslitelná
součástí	součást	k1gFnSc7	součást
italského	italský	k2eAgInSc2d1	italský
pesta	pest	k1gInSc2	pest
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
se	se	k3xPyFc4	se
minimálně	minimálně	k6eAd1	minimálně
už	už	k6eAd1	už
od	od	k7c2	od
antických	antický	k2eAgFnPc2d1	antická
dob	doba	k1gFnPc2	doba
využívala	využívat	k5eAaPmAgFnS	využívat
semena	semeno	k1gNnSc2	semeno
borovice	borovice	k1gFnSc2	borovice
pinie	pinie	k1gFnSc2	pinie
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
piniové	piniový	k2eAgInPc1d1	piniový
oříšky	oříšek	k1gInPc1	oříšek
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInPc1	jejich
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
i	i	k9	i
ve	v	k7c6	v
vykopávkách	vykopávka	k1gFnPc6	vykopávka
Pompejí	Pompeje	k1gFnPc2	Pompeje
a	a	k8xC	a
dle	dle	k7c2	dle
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
těšily	těšit	k5eAaImAgFnP	těšit
velké	velká	k1gFnPc1	velká
oblibě	obliba	k1gFnSc3	obliba
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
jako	jako	k8xC	jako
afrodiziakum	afrodiziakum	k1gNnSc1	afrodiziakum
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
významnými	významný	k2eAgInPc7d1	významný
eurasijskými	eurasijský	k2eAgInPc7d1	eurasijský
druhy	druh	k1gInPc7	druh
"	"	kIx"	"
<g/>
jedlých	jedlý	k2eAgFnPc2d1	jedlá
<g/>
"	"	kIx"	"
borovic	borovice	k1gFnPc2	borovice
jsou	být	k5eAaImIp3nP	být
borovice	borovice	k1gFnPc1	borovice
limba	limba	k1gFnSc1	limba
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
zakrslá	zakrslý	k2eAgFnSc1d1	zakrslá
a	a	k8xC	a
borovice	borovice	k1gFnSc1	borovice
sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejíchž	jejíž	k3xOyRp3gNnPc2	jejíž
semen	semeno	k1gNnPc2	semeno
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
"	"	kIx"	"
<g/>
cedrové	cedrový	k2eAgInPc1d1	cedrový
ořechy	ořech	k1gInPc1	ořech
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
к	к	k?	к
о	о	k?	о
<g/>
,	,	kIx,	,
kedrovyje	kedrovýt	k5eAaPmIp3nS	kedrovýt
orjechi	orjechi	k6eAd1	orjechi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
též	též	k9	též
lisuje	lisovat	k5eAaImIp3nS	lisovat
jedlý	jedlý	k2eAgInSc4d1	jedlý
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
borovice	borovice	k1gFnSc1	borovice
korejská	korejský	k2eAgFnSc1d1	Korejská
nebo	nebo	k8xC	nebo
borovice	borovice	k1gFnSc1	borovice
Gerardova	Gerardův	k2eAgFnSc1d1	Gerardova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
má	mít	k5eAaImIp3nS	mít
jedlá	jedlý	k2eAgNnPc4d1	jedlé
semena	semeno	k1gNnPc4	semeno
např.	např.	kA	např.
borovice	borovice	k1gFnSc1	borovice
těžká	těžký	k2eAgFnSc1d1	těžká
či	či	k8xC	či
borovice	borovice	k1gFnSc1	borovice
Coulterova	Coulterův	k2eAgFnSc1d1	Coulterův
<g/>
;	;	kIx,	;
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
úlohu	úloha	k1gFnSc4	úloha
však	však	k9	však
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
skupina	skupina	k1gFnSc1	skupina
zhruba	zhruba	k6eAd1	zhruba
13	[number]	k4	13
druhů	druh	k1gInPc2	druh
borovic	borovice	k1gFnPc2	borovice
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
lokálně	lokálně	k6eAd1	lokálně
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
piñ	piñ	k?	piñ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejdůležitějším	důležitý	k2eAgMnPc3d3	nejdůležitější
patří	patřit	k5eAaImIp3nS	patřit
borovice	borovice	k1gFnPc4	borovice
jedlá	jedlý	k2eAgFnSc1d1	jedlá
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
limbovitá	limbovitý	k2eAgFnSc1d1	limbovitý
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
jednolistá	jednolistý	k2eAgFnSc1d1	jednolistá
<g/>
,	,	kIx,	,
Pinus	Pinus	k1gInSc1	Pinus
johannis	johannis	k1gInSc1	johannis
<g/>
,	,	kIx,	,
Pinus	Pinus	k1gInSc1	Pinus
remota	remota	k1gFnSc1	remota
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
;	;	kIx,	;
odnepaměti	odnepaměti	k6eAd1	odnepaměti
byly	být	k5eAaImAgInP	být
využívány	využívat	k5eAaImNgInP	využívat
místními	místní	k2eAgInPc7d1	místní
indiánskými	indiánský	k2eAgInPc7d1	indiánský
kmeny	kmen	k1gInPc7	kmen
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
<g/>
Semena	semeno	k1gNnSc2	semeno
některých	některý	k3yIgFnPc2	některý
variant	varianta	k1gFnPc2	varianta
borovice	borovice	k1gFnSc2	borovice
Armandovy	Armandův	k2eAgFnSc2d1	Armandova
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
přimíchána	přimíchán	k2eAgNnPc4d1	přimícháno
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
nekvalitních	kvalitní	k2eNgFnPc6d1	nekvalitní
prodávaných	prodávaný	k2eAgFnPc6d1	prodávaná
směsích	směs	k1gFnPc6	směs
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
však	však	k9	však
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
osob	osoba	k1gFnPc2	osoba
způsobit	způsobit	k5eAaPmF	způsobit
ztrátu	ztráta	k1gFnSc4	ztráta
či	či	k8xC	či
změnu	změna	k1gFnSc4	změna
vnímání	vnímání	k1gNnSc2	vnímání
chuti	chuť	k1gFnSc2	chuť
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Pine	pin	k1gInSc5	pin
Nut	Nut	k1gMnSc1	Nut
Syndrome	syndrom	k1gInSc5	syndrom
<g/>
,	,	kIx,	,
syndrom	syndrom	k1gInSc1	syndrom
piniového	piniový	k2eAgInSc2d1	piniový
oříšku	oříšek	k1gInSc2	oříšek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
chuť	chuť	k1gFnSc4	chuť
veškerých	veškerý	k3xTgFnPc2	veškerý
konzumovaných	konzumovaný	k2eAgFnPc2d1	konzumovaná
potravin	potravina	k1gFnPc2	potravina
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
až	až	k8xS	až
týdnů	týden	k1gInPc2	týden
přebita	přebit	k2eAgFnSc1d1	přebita
podivnou	podivný	k2eAgFnSc7d1	podivná
kovovou	kovový	k2eAgFnSc7d1	kovová
pachutí	pachuť	k1gFnSc7	pachuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Okrasné	okrasný	k2eAgNnSc1d1	okrasné
zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
patří	patřit	k5eAaImIp3nS	patřit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
nenáročnost	nenáročnost	k1gFnSc4	nenáročnost
<g/>
,	,	kIx,	,
vytrvávající	vytrvávající	k2eAgNnSc4d1	vytrvávající
jehličí	jehličí	k1gNnSc4	jehličí
a	a	k8xC	a
atraktivní	atraktivní	k2eAgInSc4d1	atraktivní
habitus	habitus	k1gInSc4	habitus
k	k	k7c3	k
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
okrasným	okrasný	k2eAgFnPc3d1	okrasná
dřevinám	dřevina	k1gFnPc3	dřevina
a	a	k8xC	a
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
bylo	být	k5eAaImAgNnS	být
vyšlechtěno	vyšlechtěn	k2eAgNnSc1d1	vyšlechtěno
množství	množství	k1gNnSc1	množství
kultivarů	kultivar	k1gInPc2	kultivar
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1	uspořádání
koruny	koruna	k1gFnSc2	koruna
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
měkčí	měkký	k2eAgInSc1d2	měkčí
<g/>
,	,	kIx,	,
vzdušnější	vzdušný	k2eAgInSc1d2	vzdušnější
a	a	k8xC	a
živější	živý	k2eAgInSc1d2	živější
než	než	k8xS	než
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
,	,	kIx,	,
např.	např.	kA	např.
smrku	smrk	k1gInSc2	smrk
či	či	k8xC	či
jedle	jedle	k1gFnSc2	jedle
<g/>
,	,	kIx,	,
esteticky	esteticky	k6eAd1	esteticky
zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
šišky	šiška	k1gFnPc1	šiška
<g/>
,	,	kIx,	,
pokroucené	pokroucený	k2eAgFnPc1d1	pokroucená
větve	větev	k1gFnPc1	větev
nebo	nebo	k8xC	nebo
rezavě	rezavě	k6eAd1	rezavě
oranžový	oranžový	k2eAgInSc1d1	oranžový
kmen	kmen	k1gInSc1	kmen
u	u	k7c2	u
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
v	v	k7c6	v
zahradních	zahradní	k2eAgFnPc6d1	zahradní
i	i	k8xC	i
krajinářských	krajinářský	k2eAgFnPc6d1	krajinářská
úpravách	úprava	k1gFnPc6	úprava
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
solitéry	solitéra	k1gFnPc1	solitéra
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sadovnické	sadovnický	k2eAgNnSc4d1	sadovnické
použití	použití	k1gNnSc4	použití
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
obvykle	obvykle	k6eAd1	obvykle
semeny	semeno	k1gNnPc7	semeno
<g/>
,	,	kIx,	,
vzácnější	vzácný	k2eAgInPc4d2	vzácnější
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
kultivary	kultivar	k1gInPc4	kultivar
lze	lze	k6eAd1	lze
také	také	k9	také
roubovat	roubovat	k5eAaImF	roubovat
na	na	k7c4	na
podnož	podnož	k1gInSc4	podnož
z	z	k7c2	z
odolného	odolný	k2eAgInSc2d1	odolný
příbuzného	příbuzný	k2eAgInSc2d1	příbuzný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výsadbách	výsadba	k1gFnPc6	výsadba
borovice	borovice	k1gFnSc2	borovice
dobře	dobře	k6eAd1	dobře
harmonují	harmonovat	k5eAaImIp3nP	harmonovat
např.	např.	kA	např.
s	s	k7c7	s
břízami	bříza	k1gFnPc7	bříza
<g/>
,	,	kIx,	,
duby	dub	k1gInPc7	dub
<g/>
,	,	kIx,	,
jalovci	jalovec	k1gInPc7	jalovec
<g/>
,	,	kIx,	,
různými	různý	k2eAgInPc7d1	různý
vřesy	vřes	k1gInPc7	vřes
<g/>
,	,	kIx,	,
akátem	akát	k1gInSc7	akát
<g/>
,	,	kIx,	,
hlohem	hloh	k1gInSc7	hloh
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
listnáči	listnáč	k1gInPc7	listnáč
s	s	k7c7	s
malebnými	malebný	k2eAgFnPc7d1	malebná
korunami	koruna	k1gFnPc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Zakrslé	zakrslý	k2eAgInPc1d1	zakrslý
a	a	k8xC	a
poléhavé	poléhavý	k2eAgInPc1d1	poléhavý
keřovité	keřovitý	k2eAgInPc1d1	keřovitý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nP	hodit
pro	pro	k7c4	pro
výsadbu	výsadba	k1gFnSc4	výsadba
do	do	k7c2	do
nádob	nádoba	k1gFnPc2	nádoba
<g/>
,	,	kIx,	,
na	na	k7c4	na
balkóny	balkón	k1gInPc4	balkón
<g/>
,	,	kIx,	,
terasy	teras	k1gInPc4	teras
nebo	nebo	k8xC	nebo
skalky	skalka	k1gFnPc4	skalka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
obsahu	obsah	k1gInSc3	obsah
aromatických	aromatický	k2eAgFnPc2d1	aromatická
silic	silice	k1gFnPc2	silice
jsou	být	k5eAaImIp3nP	být
borovice	borovice	k1gFnPc1	borovice
často	často	k6eAd1	často
vysazovány	vysazovat	k5eAaImNgFnP	vysazovat
v	v	k7c6	v
lázeňských	lázeňský	k2eAgInPc6d1	lázeňský
městech	město	k1gNnPc6	město
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
parcích	park	k1gInPc6	park
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
středoevropských	středoevropský	k2eAgFnPc6d1	středoevropská
podmínkách	podmínka	k1gFnPc6	podmínka
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejčastěji	často	k6eAd3	často
pěstovaným	pěstovaný	k2eAgInPc3d1	pěstovaný
stromům	strom	k1gInPc3	strom
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc2	borovice
černá	černý	k2eAgFnSc1d1	černá
nebo	nebo	k8xC	nebo
americká	americký	k2eAgFnSc1d1	americká
borovice	borovice	k1gFnSc1	borovice
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
potom	potom	k6eAd1	potom
např.	např.	kA	např.
borovice	borovice	k1gFnSc1	borovice
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
,	,	kIx,	,
Jeffreyova	Jeffreyův	k2eAgFnSc1d1	Jeffreyův
<g/>
,	,	kIx,	,
himálajská	himálajský	k2eAgFnSc1d1	himálajská
nebo	nebo	k8xC	nebo
limba	limba	k1gFnSc1	limba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
zahradách	zahrada	k1gFnPc6	zahrada
se	se	k3xPyFc4	se
uplatní	uplatnit	k5eAaPmIp3nP	uplatnit
málo	málo	k6eAd1	málo
vzrůstné	vzrůstný	k2eAgInPc1d1	vzrůstný
až	až	k8xS	až
keřovité	keřovitý	k2eAgInPc1d1	keřovitý
druhy	druh	k1gInPc1	druh
jako	jako	k8xC	jako
blatka	blatka	k1gFnSc1	blatka
<g/>
,	,	kIx,	,
kleč	kleč	k1gFnSc1	kleč
<g/>
,	,	kIx,	,
kultivary	kultivar	k1gInPc1	kultivar
borovice	borovice	k1gFnSc2	borovice
osinaté	osinatý	k2eAgInPc1d1	osinatý
s	s	k7c7	s
typickými	typický	k2eAgFnPc7d1	typická
kapičkami	kapička	k1gFnPc7	kapička
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
na	na	k7c6	na
jehlicích	jehlice	k1gFnPc6	jehlice
a	a	k8xC	a
osinatými	osinatý	k2eAgFnPc7d1	osinatá
šiškami	šiška	k1gFnPc7	šiška
nebo	nebo	k8xC	nebo
pomalu	pomalu	k6eAd1	pomalu
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
borovice	borovice	k1gFnSc1	borovice
pokroucená	pokroucený	k2eAgFnSc1d1	pokroucená
či	či	k8xC	či
drobnokvětá	drobnokvětý	k2eAgFnSc1d1	drobnokvětá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Středomoří	středomoří	k1gNnSc6	středomoří
je	být	k5eAaImIp3nS	být
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
borovice	borovice	k1gFnSc1	borovice
pinie	pinie	k1gFnSc2	pinie
poskytující	poskytující	k2eAgFnSc2d1	poskytující
žádaný	žádaný	k2eAgInSc4d1	žádaný
stín	stín	k1gInSc4	stín
svou	svůj	k3xOyFgFnSc7	svůj
široce	široko	k6eAd1	široko
deštníkovitou	deštníkovitý	k2eAgFnSc7d1	deštníkovitá
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
<g/>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
druhy	druh	k1gInPc1	druh
borovic	borovice	k1gFnPc2	borovice
jsou	být	k5eAaImIp3nP	být
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
jako	jako	k8xC	jako
vánoční	vánoční	k2eAgInPc1d1	vánoční
stromky	stromek	k1gInPc1	stromek
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
především	především	k9	především
borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
i	i	k9	i
borovice	borovice	k1gFnSc2	borovice
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
či	či	k8xC	či
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
<g/>
;	;	kIx,	;
v	v	k7c6	v
konkurenci	konkurence	k1gFnSc6	konkurence
jedliček	jedlička	k1gFnPc2	jedlička
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
stále	stále	k6eAd1	stále
žádanější	žádaný	k2eAgFnPc1d2	žádanější
jedle	jedle	k1gFnPc1	jedle
kavkazské	kavkazský	k2eAgFnSc2d1	kavkazská
<g/>
)	)	kIx)	)
však	však	k9	však
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
10	[number]	k4	10
procent	procento	k1gNnPc2	procento
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
rychlý	rychlý	k2eAgInSc1d1	rychlý
růst	růst	k1gInSc1	růst
a	a	k8xC	a
snadné	snadný	k2eAgNnSc1d1	snadné
pěstování	pěstování	k1gNnSc1	pěstování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgMnPc2d1	americký
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vánoční	vánoční	k2eAgInPc4d1	vánoční
účely	účel	k1gInPc4	účel
široce	široko	k6eAd1	široko
pěstována	pěstován	k2eAgFnSc1d1	pěstována
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
introdukovaná	introdukovaný	k2eAgFnSc1d1	introdukovaná
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
asijské	asijský	k2eAgFnPc1d1	asijská
borovice	borovice	k1gFnPc1	borovice
drobnokvětá	drobnokvětý	k2eAgFnSc1d1	drobnokvětá
<g/>
,	,	kIx,	,
zakrslá	zakrslý	k2eAgFnSc1d1	zakrslá
<g/>
,	,	kIx,	,
hustokvětá	hustokvětý	k2eAgFnSc1d1	hustokvětá
a	a	k8xC	a
Thunbergova	Thunbergův	k2eAgFnSc1d1	Thunbergův
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
borovice	borovice	k1gFnSc1	borovice
ohebná	ohebný	k2eAgFnSc1d1	ohebná
nebo	nebo	k8xC	nebo
i	i	k9	i
evropské	evropský	k2eAgFnSc2d1	Evropská
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgFnSc2d1	lesní
a	a	k8xC	a
kleč	kleč	k1gFnSc4	kleč
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
využívány	využívat	k5eAaPmNgInP	využívat
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
bonsají	bonsaj	k1gFnPc2	bonsaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kulturní	kulturní	k2eAgInSc1d1	kulturní
a	a	k8xC	a
spirituální	spirituální	k2eAgInSc1d1	spirituální
význam	význam	k1gInSc1	význam
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
jiné	jiný	k2eAgInPc1d1	jiný
stálezelené	stálezelený	k2eAgInPc1d1	stálezelený
jehličnany	jehličnan	k1gInPc1	jehličnan
platily	platit	k5eAaImAgInP	platit
i	i	k9	i
borovice	borovice	k1gFnSc2	borovice
v	v	k7c6	v
mírných	mírný	k2eAgFnPc6d1	mírná
a	a	k8xC	a
chladných	chladný	k2eAgFnPc6d1	chladná
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
naděje	naděje	k1gFnSc2	naděje
<g/>
,	,	kIx,	,
věčného	věčný	k2eAgInSc2d1	věčný
života	život	k1gInSc2	život
a	a	k8xC	a
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
přírody	příroda	k1gFnSc2	příroda
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
byly	být	k5eAaImAgFnP	být
zdobeny	zdoben	k2eAgFnPc1d1	zdobena
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgFnP	být
páleny	pálit	k5eAaImNgFnP	pálit
jejich	jejich	k3xOp3gFnPc1	jejich
smolnaté	smolnatý	k2eAgFnPc1d1	smolnatý
větévky	větévka	k1gFnPc1	větévka
a	a	k8xC	a
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
obydlí	obydlí	k1gNnPc2	obydlí
proti	proti	k7c3	proti
zlým	zlý	k2eAgFnPc3d1	zlá
silám	síla	k1gFnPc3	síla
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
Slezsku	Slezsko	k1gNnSc6	Slezsko
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antické	antický	k2eAgFnSc6d1	antická
mytologii	mytologie	k1gFnSc6	mytologie
byly	být	k5eAaImAgFnP	být
borovice	borovice	k1gFnPc1	borovice
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
sídla	sídlo	k1gNnPc4	sídlo
dryád	dryáda	k1gFnPc2	dryáda
<g/>
,	,	kIx,	,
nymf	nymfa	k1gFnPc2	nymfa
a	a	k8xC	a
zasvěcovány	zasvěcován	k2eAgFnPc1d1	zasvěcována
božstvům	božstvo	k1gNnPc3	božstvo
jako	jako	k9	jako
Pan	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
nebo	nebo	k8xC	nebo
samotný	samotný	k2eAgInSc1d1	samotný
Zeus	Zeus	k1gInSc1	Zeus
<g/>
.	.	kIx.	.
</s>
<s>
Šišky	šiška	k1gFnSc2	šiška
borovice	borovice	k1gFnPc1	borovice
pinie	pinie	k1gFnSc2	pinie
byly	být	k5eAaImAgFnP	být
významnou	významný	k2eAgFnSc4d1	významná
a	a	k8xC	a
ceněnou	ceněný	k2eAgFnSc7d1	ceněná
obětinou	obětina	k1gFnSc7	obětina
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
symbolem	symbol	k1gInSc7	symbol
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nymfa	nymfa	k1gFnSc1	nymfa
Pitys	Pitys	k1gInSc1	Pitys
(	(	kIx(	(
<g/>
srovnejte	srovnat	k5eAaPmRp2nP	srovnat
etymologii	etymologie	k1gFnSc4	etymologie
slova	slovo	k1gNnSc2	slovo
Pinus	Pinus	k1gInSc1	Pinus
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
řecké	řecký	k2eAgFnSc2d1	řecká
pověsti	pověst	k1gFnSc2	pověst
proměněna	proměnit	k5eAaPmNgFnS	proměnit
žárlivým	žárlivý	k2eAgMnSc7d1	žárlivý
milencem	milenec	k1gMnSc7	milenec
Boreásem	Boreás	k1gMnSc7	Boreás
v	v	k7c6	v
borovici	borovice	k1gFnSc6	borovice
a	a	k8xC	a
kapky	kapka	k1gFnSc2	kapka
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
roní	ronit	k5eAaImIp3nS	ronit
poraněný	poraněný	k2eAgInSc4d1	poraněný
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
její	její	k3xOp3gFnPc4	její
slzy	slza	k1gFnPc4	slza
<g/>
.	.	kIx.	.
<g/>
Borovice	borovice	k1gFnPc4	borovice
bývají	bývat	k5eAaImIp3nP	bývat
ústřední	ústřední	k2eAgFnSc7d1	ústřední
dřevinou	dřevina	k1gFnSc7	dřevina
ve	v	k7c6	v
filozoficky	filozoficky	k6eAd1	filozoficky
promyšlené	promyšlený	k2eAgFnSc6d1	promyšlená
kompozici	kompozice	k1gFnSc6	kompozice
japonských	japonský	k2eAgFnPc2d1	japonská
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obecně	obecně	k6eAd1	obecně
vzato	vzít	k5eAaPmNgNnS	vzít
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
dlouhověkost	dlouhověkost	k1gFnSc1	dlouhověkost
a	a	k8xC	a
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Tmavý	tmavý	k2eAgInSc1d1	tmavý
habitus	habitus	k1gInSc1	habitus
borovice	borovice	k1gFnSc2	borovice
Thunbergovy	Thunbergův	k2eAgFnSc2d1	Thunbergův
v	v	k7c6	v
nich	on	k3xPp3gNnPc6	on
představuje	představovat	k5eAaImIp3nS	představovat
mužské	mužský	k2eAgNnSc1d1	mužské
<g/>
,	,	kIx,	,
pozitivní	pozitivní	k2eAgFnPc1d1	pozitivní
síly	síla	k1gFnPc1	síla
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
načervenalý	načervenalý	k2eAgInSc1d1	načervenalý
kmen	kmen	k1gInSc1	kmen
borovice	borovice	k1gFnSc2	borovice
hustokvěté	hustokvětý	k2eAgFnSc2d1	hustokvětá
síly	síla	k1gFnSc2	síla
ženské	ženský	k2eAgFnPc4d1	ženská
<g/>
,	,	kIx,	,
negativní	negativní	k2eAgFnPc4d1	negativní
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
předchozími	předchozí	k2eAgInPc7d1	předchozí
v	v	k7c6	v
nedílné	dílný	k2eNgFnSc6d1	nedílná
jednotě	jednota	k1gFnSc6	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bambusem	bambus	k1gInSc7	bambus
a	a	k8xC	a
slivoní	slivoň	k1gFnSc7	slivoň
tvoří	tvořit	k5eAaImIp3nS	tvořit
borovice	borovice	k1gFnSc1	borovice
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
a	a	k8xC	a
japonské	japonský	k2eAgFnSc6d1	japonská
kultuře	kultura	k1gFnSc6	kultura
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
motiv	motiv	k1gInSc4	motiv
"	"	kIx"	"
<g/>
tří	tři	k4xCgMnPc2	tři
zimních	zimní	k2eAgMnPc2d1	zimní
přátel	přítel	k1gMnPc2	přítel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zobrazovaný	zobrazovaný	k2eAgInSc4d1	zobrazovaný
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
<g/>
,	,	kIx,	,
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
a	a	k8xC	a
užitém	užitý	k2eAgNnSc6d1	užité
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ve	v	k7c6	v
výzdobách	výzdoba	k1gFnPc6	výzdoba
vztahujících	vztahující	k2eAgFnPc6d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
příchodu	příchod	k1gInSc3	příchod
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
<g/>
Významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
hrály	hrát	k5eAaImAgInP	hrát
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
jedlé	jedlý	k2eAgFnSc2d1	jedlá
borovice	borovice	k1gFnSc2	borovice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
limby	limb	k1gInPc1	limb
<g/>
,	,	kIx,	,
piňony	piňon	k1gInPc1	piňon
<g/>
)	)	kIx)	)
coby	coby	k?	coby
zdroj	zdroj	k1gInSc4	zdroj
základní	základní	k2eAgFnSc2d1	základní
potravy	potrava	k1gFnSc2	potrava
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
domorodých	domorodý	k2eAgFnPc6d1	domorodá
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
Apačů	Apač	k1gMnPc2	Apač
nebo	nebo	k8xC	nebo
Navajů	Navaj	k1gMnPc2	Navaj
pálení	pálení	k1gNnSc2	pálení
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
borovice	borovice	k1gFnSc2	borovice
jedlé	jedlý	k2eAgFnSc2d1	jedlá
provázelo	provázet	k5eAaImAgNnS	provázet
iniciační	iniciační	k2eAgInPc4d1	iniciační
rituály	rituál	k1gInPc4	rituál
mladých	mladý	k2eAgMnPc2d1	mladý
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Kolébky	kolébka	k1gFnPc1	kolébka
se	s	k7c7	s
zemřelými	zemřelý	k2eAgFnPc7d1	zemřelá
dětmi	dítě	k1gFnPc7	dítě
byly	být	k5eAaImAgFnP	být
umísťovány	umísťovat	k5eAaImNgFnP	umísťovat
do	do	k7c2	do
koruny	koruna	k1gFnSc2	koruna
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
borovice	borovice	k1gFnSc2	borovice
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
na	na	k7c4	na
živé	živé	k1gNnSc4	živé
větve	větev	k1gFnSc2	větev
byly	být	k5eAaImAgFnP	být
věšeny	věšen	k2eAgFnPc1d1	věšen
použité	použitý	k2eAgFnPc1d1	použitá
kolébky	kolébka	k1gFnPc1	kolébka
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jim	on	k3xPp3gMnPc3	on
úspěšně	úspěšně	k6eAd1	úspěšně
odrostly	odrůst	k5eAaPmAgInP	odrůst
<g/>
.	.	kIx.	.
</s>
<s>
Borovice	borovice	k1gFnSc1	borovice
Pinus	Pinus	k1gInSc1	Pinus
teocote	teocot	k1gInSc5	teocot
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
Aztéků	Azték	k1gMnPc2	Azték
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
strom	strom	k1gInSc4	strom
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
pálena	pálen	k2eAgFnSc1d1	pálena
v	v	k7c6	v
chrámech	chrám	k1gInPc6	chrám
jako	jako	k8xS	jako
obětina	obětina	k1gFnSc1	obětina
<g/>
,	,	kIx,	,
Mayové	Mayové	k2eAgInPc4d1	Mayové
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
středoamerické	středoamerický	k2eAgInPc4d1	středoamerický
indiánské	indiánský	k2eAgInPc4d1	indiánský
kmeny	kmen	k1gInPc4	kmen
považovali	považovat	k5eAaImAgMnP	považovat
borovice	borovice	k1gFnSc1	borovice
za	za	k7c4	za
oduševnělé	oduševnělý	k2eAgFnPc4d1	oduševnělá
bytosti	bytost	k1gFnPc4	bytost
a	a	k8xC	a
vyvarovali	vyvarovat	k5eAaPmAgMnP	vyvarovat
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc3	jejich
poškození	poškození	k1gNnSc3	poškození
či	či	k8xC	či
zabití	zabití	k1gNnSc4	zabití
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gNnSc1	jejich
chvojí	chvojí	k1gNnSc1	chvojí
a	a	k8xC	a
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
dodnes	dodnes	k6eAd1	dodnes
hrají	hrát	k5eAaImIp3nP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
pohansko-křesťanských	pohanskořesťanský	k2eAgInPc6d1	pohansko-křesťanský
rituálech	rituál	k1gInPc6	rituál
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Sibiře	Sibiř	k1gFnSc2	Sibiř
přisuzovali	přisuzovat	k5eAaImAgMnP	přisuzovat
velkou	velký	k2eAgFnSc4d1	velká
duchovní	duchovní	k2eAgFnSc4d1	duchovní
sílu	síla	k1gFnSc4	síla
borovici	borovice	k1gFnSc3	borovice
sibiřské	sibiřský	k2eAgFnSc3d1	sibiřská
<g/>
,	,	kIx,	,
vědomi	vědom	k2eAgMnPc1d1	vědom
si	se	k3xPyFc3	se
její	její	k3xOp3gFnSc2	její
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
důležitosti	důležitost	k1gFnSc2	důležitost
pro	pro	k7c4	pro
potravní	potravní	k2eAgInPc4d1	potravní
ekosystém	ekosystém	k1gInSc4	ekosystém
tajgy	tajga	k1gFnSc2	tajga
<g/>
;	;	kIx,	;
ústřední	ústřední	k2eAgFnSc4d1	ústřední
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
tento	tento	k3xDgInSc4	tento
strom	strom	k1gInSc4	strom
též	též	k9	též
coby	coby	k?	coby
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zvonící	zvonící	k2eAgInSc1d1	zvonící
cedr	cedr	k1gInSc1	cedr
<g/>
"	"	kIx"	"
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
směru	směr	k1gInSc6	směr
ruského	ruský	k2eAgNnSc2d1	ruské
rodnověří	rodnověří	k1gNnSc2	rodnověří
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
porosty	porost	k1gInPc1	porost
a	a	k8xC	a
solitérní	solitérní	k2eAgInPc1d1	solitérní
stromy	strom	k1gInPc1	strom
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgInPc1d1	lesní
se	se	k3xPyFc4	se
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
těšily	těšit	k5eAaImAgFnP	těšit
značné	značný	k2eAgFnPc4d1	značná
úctě	úcta	k1gFnSc6	úcta
šamanů	šaman	k1gMnPc2	šaman
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
mongolských	mongolský	k2eAgNnPc2d1	mongolské
a	a	k8xC	a
burjatských	burjatský	k2eAgNnPc2d1	burjatský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Obliba	obliba	k1gFnSc1	obliba
borovic	borovice	k1gFnPc2	borovice
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
kultuře	kultura	k1gFnSc6	kultura
se	se	k3xPyFc4	se
odrazila	odrazit	k5eAaPmAgFnS	odrazit
také	také	k9	také
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nesčetném	sčetný	k2eNgNnSc6d1	nesčetné
množství	množství	k1gNnSc6	množství
literárních	literární	k2eAgInPc2d1	literární
a	a	k8xC	a
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
i	i	k9	i
lidové	lidový	k2eAgFnPc1d1	lidová
slovesnosti	slovesnost	k1gFnPc1	slovesnost
<g/>
.	.	kIx.	.
</s>
<s>
Borovice	borovice	k1gFnSc1	borovice
byly	být	k5eAaImAgInP	být
častým	častý	k2eAgInSc7d1	častý
námětem	námět	k1gInSc7	námět
staré	starý	k2eAgFnSc2d1	stará
čínské	čínský	k2eAgFnSc2d1	čínská
poezie	poezie	k1gFnSc2	poezie
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
jako	jako	k9	jako
Li	li	k9	li
Po	Po	kA	Po
<g/>
,	,	kIx,	,
Chan	Chan	k1gMnSc1	Chan
Šan	Šan	k1gMnSc1	Šan
<g/>
,	,	kIx,	,
Š-te	Še	k1gFnSc1	Š-te
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
evropské	evropský	k2eAgFnPc1d1	Evropská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
poezii	poezie	k1gFnSc6	poezie
se	se	k3xPyFc4	se
borovice	borovice	k1gFnSc1	borovice
objevuje	objevovat	k5eAaImIp3nS	objevovat
např.	např.	kA	např.
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
Antonína	Antonín	k1gMnSc2	Antonín
Sovy	Sova	k1gMnSc2	Sova
<g/>
,	,	kIx,	,
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Holana	Holan	k1gMnSc2	Holan
nebo	nebo	k8xC	nebo
Josefa	Josef	k1gMnSc2	Josef
Václava	Václav	k1gMnSc2	Václav
Sládka	Sládek	k1gMnSc2	Sládek
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc4	život
jedné	jeden	k4xCgFnSc2	jeden
lesní	lesní	k2eAgFnSc2d1	lesní
borovice	borovice	k1gFnSc2	borovice
je	být	k5eAaImIp3nS	být
tématem	téma	k1gNnSc7	téma
přírodní	přírodní	k2eAgFnSc2d1	přírodní
prózy	próza	k1gFnSc2	próza
Jana	Jan	k1gMnSc2	Jan
Vrby	Vrba	k1gMnSc2	Vrba
Borovice	borovice	k1gFnSc1	borovice
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
domýšlivé	domýšlivý	k2eAgFnSc2d1	domýšlivá
borovice	borovice	k1gFnSc2	borovice
proměněné	proměněný	k2eAgFnSc2d1	proměněná
na	na	k7c4	na
vánoční	vánoční	k2eAgInSc4d1	vánoční
stromek	stromek	k1gInSc4	stromek
pak	pak	k6eAd1	pak
námětem	námět	k1gInSc7	námět
pohádky	pohádka	k1gFnSc2	pohádka
Hanse	Hans	k1gMnSc2	Hans
Christiana	Christian	k1gMnSc4	Christian
Andersena	Andersen	k1gMnSc4	Andersen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
motiv	motiv	k1gInSc4	motiv
zobrazovaný	zobrazovaný	k2eAgInSc4d1	zobrazovaný
řadou	řada	k1gFnSc7	řada
malířů	malíř	k1gMnPc2	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
orientální	orientální	k2eAgFnSc2d1	orientální
provenience	provenience	k1gFnSc2	provenience
jsou	být	k5eAaImIp3nP	být
borovice	borovice	k1gFnPc1	borovice
často	často	k6eAd1	často
přítomny	přítomen	k2eAgInPc1d1	přítomen
například	například	k6eAd1	například
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
japonského	japonský	k2eAgMnSc2d1	japonský
malíře	malíř	k1gMnSc2	malíř
Hasegawy	Hasegawa	k1gMnSc2	Hasegawa
Tō	Tō	k1gMnSc2	Tō
či	či	k8xC	či
dřevorytce	dřevorytec	k1gMnSc2	dřevorytec
Hokusaie	Hokusaie	k1gFnSc2	Hokusaie
<g/>
,	,	kIx,	,
z	z	k7c2	z
evropských	evropský	k2eAgMnPc2d1	evropský
u	u	k7c2	u
Paula	Paul	k1gMnSc2	Paul
Cézanna	Cézann	k1gMnSc2	Cézann
nebo	nebo	k8xC	nebo
ruského	ruský	k2eAgMnSc2d1	ruský
krajináře	krajinář	k1gMnSc2	krajinář
Ivana	Ivan	k1gMnSc2	Ivan
Šiškina	Šiškin	k1gMnSc2	Šiškin
(	(	kIx(	(
<g/>
Ráno	ráno	k6eAd1	ráno
v	v	k7c6	v
borovém	borový	k2eAgInSc6d1	borový
lese	les	k1gInSc6	les
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgInPc2d1	další
obrazů	obraz	k1gInPc2	obraz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
heraldická	heraldický	k2eAgFnSc1d1	heraldická
figura	figura	k1gFnSc1	figura
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
praporech	prapor	k1gInPc6	prapor
a	a	k8xC	a
erbech	erb	k1gInPc6	erb
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
v	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
stylizovaně	stylizovaně	k6eAd1	stylizovaně
nebo	nebo	k8xC	nebo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
šišek	šiška	k1gFnPc2	šiška
<g/>
.	.	kIx.	.
</s>
<s>
Borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
je	být	k5eAaImIp3nS	být
národním	národní	k2eAgInSc7d1	národní
stromem	strom	k1gInSc7	strom
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Maine	Main	k1gInSc5	Main
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
pokroucená	pokroucený	k2eAgFnSc1d1	pokroucená
symbolem	symbol	k1gInSc7	symbol
kanadské	kanadský	k2eAgFnSc2d1	kanadská
provincie	provincie	k1gFnSc2	provincie
Alberta	Albert	k1gMnSc2	Albert
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc2	borovice
karibská	karibský	k2eAgFnSc1d1	karibská
varianta	varianta	k1gFnSc1	varianta
bahamensis	bahamensis	k1gFnSc2	bahamensis
pak	pak	k6eAd1	pak
národním	národní	k2eAgInSc7d1	národní
stromem	strom	k1gInSc7	strom
státu	stát	k1gInSc2	stát
Turks	Turksa	k1gFnPc2	Turksa
a	a	k8xC	a
Caicos	Caicosa	k1gFnPc2	Caicosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Borovice	borovice	k1gFnSc1	borovice
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
Nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
druhem	druh	k1gInSc7	druh
borovice	borovice	k1gFnSc2	borovice
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
lesních	lesní	k2eAgInPc6d1	lesní
porostech	porost	k1gInPc6	porost
činí	činit	k5eAaImIp3nS	činit
zhruba	zhruba	k6eAd1	zhruba
17	[number]	k4	17
%	%	kIx~	%
<g/>
,	,	kIx,	,
přirozené	přirozený	k2eAgNnSc1d1	přirozené
rozšíření	rozšíření	k1gNnSc1	rozšíření
by	by	kYmCp3nS	by
však	však	k9	však
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
jen	jen	k9	jen
asi	asi	k9	asi
3,5	[number]	k4	3,5
<g/>
–	–	k?	–
<g/>
5,5	[number]	k4	5,5
%	%	kIx~	%
a	a	k8xC	a
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
by	by	kYmCp3nS	by
prakticky	prakticky	k6eAd1	prakticky
výhradně	výhradně	k6eAd1	výhradně
ostrůvky	ostrůvek	k1gInPc1	ostrůvek
různých	různý	k2eAgFnPc2d1	různá
extrémních	extrémní	k2eAgFnPc2d1	extrémní
stanovišť	stanoviště	k1gNnPc2	stanoviště
(	(	kIx(	(
<g/>
písky	písek	k1gInPc1	písek
<g/>
,	,	kIx,	,
skály	skála	k1gFnPc1	skála
<g/>
,	,	kIx,	,
rašeliny	rašelina	k1gFnPc1	rašelina
<g/>
,	,	kIx,	,
pískovcová	pískovcový	k2eAgNnPc1d1	pískovcové
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
říční	říční	k2eAgInPc1d1	říční
zářezy	zářez	k1gInPc1	zářez
<g/>
,	,	kIx,	,
hadce	hadec	k1gInPc1	hadec
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
konkurenčně	konkurenčně	k6eAd1	konkurenčně
slabá	slabý	k2eAgFnSc1d1	slabá
borovice	borovice	k1gFnSc1	borovice
dokázala	dokázat	k5eAaPmAgFnS	dokázat
vytrvat	vytrvat	k5eAaPmF	vytrvat
od	od	k7c2	od
maxima	maximum	k1gNnSc2	maximum
svého	svůj	k3xOyFgNnSc2	svůj
rozšíření	rozšíření	k1gNnSc2	rozšíření
v	v	k7c6	v
období	období	k1gNnSc6	období
preboreálu	preboreál	k1gInSc2	preboreál
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
ústupu	ústup	k1gInSc6	ústup
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
tvořila	tvořit	k5eAaImAgFnS	tvořit
většinu	většina	k1gFnSc4	většina
středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
vegetace	vegetace	k1gFnSc2	vegetace
světlá	světlý	k2eAgFnSc1d1	světlá
březovo-borová	březovoorový	k2eAgFnSc1d1	březovo-borový
tajga	tajga	k1gFnSc1	tajga
<g/>
.	.	kIx.	.
</s>
<s>
Masivní	masivní	k2eAgFnPc1d1	masivní
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
výsadby	výsadba	k1gFnPc1	výsadba
borových	borový	k2eAgFnPc2d1	Borová
monokultur	monokultura	k1gFnPc2	monokultura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
započaly	započnout	k5eAaPmAgFnP	započnout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
borovici	borovice	k1gFnSc4	borovice
i	i	k9	i
mimo	mimo	k7c4	mimo
její	její	k3xOp3gInSc4	její
přirozený	přirozený	k2eAgInSc4d1	přirozený
areál	areál	k1gInSc4	areál
<g/>
,	,	kIx,	,
vlivem	vlivem	k7c2	vlivem
nevhodných	vhodný	k2eNgFnPc2d1	nevhodná
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
často	často	k6eAd1	často
nevhodné	vhodný	k2eNgFnSc2d1	nevhodná
semenné	semenný	k2eAgFnSc2d1	semenná
provenience	provenience	k1gFnSc2	provenience
byly	být	k5eAaImAgInP	být
však	však	k9	však
stromy	strom	k1gInPc1	strom
značně	značně	k6eAd1	značně
ohrožené	ohrožený	k2eAgInPc1d1	ohrožený
škůdci	škůdce	k1gMnPc7	škůdce
a	a	k8xC	a
chorobami	choroba	k1gFnPc7	choroba
(	(	kIx(	(
<g/>
kalamity	kalamita	k1gFnPc1	kalamita
bekyně	bekyně	k1gFnSc2	bekyně
mnišky	mniška	k1gFnSc2	mniška
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
vln	vlna	k1gFnPc2	vlna
sypavky	sypavka	k1gFnSc2	sypavka
<g/>
)	)	kIx)	)
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
posléze	posléze	k6eAd1	posléze
vystřídány	vystřídán	k2eAgFnPc1d1	vystřídána
monokulturami	monokultura	k1gFnPc7	monokultura
smrku	smrk	k1gInSc2	smrk
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
úspěchů	úspěch	k1gInPc2	úspěch
byly	být	k5eAaImAgFnP	být
zkoušeny	zkoušen	k2eAgFnPc1d1	zkoušena
náhrady	náhrada	k1gFnPc1	náhrada
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
introdukovaných	introdukovaný	k2eAgFnPc2d1	introdukovaná
borovic	borovice	k1gFnPc2	borovice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
banksovky	banksovka	k1gFnSc2	banksovka
nebo	nebo	k8xC	nebo
vejmutovky	vejmutovka	k1gFnSc2	vejmutovka
<g/>
;	;	kIx,	;
odolná	odolný	k2eAgFnSc1d1	odolná
borovice	borovice	k1gFnSc1	borovice
pokroucená	pokroucený	k2eAgFnSc1d1	pokroucená
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
využívána	využívat	k5eAaPmNgNnP	využívat
k	k	k7c3	k
rekultivacím	rekultivace	k1gFnPc3	rekultivace
zdevastovaných	zdevastovaný	k2eAgFnPc2d1	zdevastovaná
emisních	emisní	k2eAgFnPc2d1	emisní
holin	holina	k1gFnPc2	holina
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc4	několik
taxonů	taxon	k1gInPc2	taxon
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
<g />
.	.	kIx.	.
</s>
<s>
borovice	borovice	k1gFnSc1	borovice
kleče	kleč	k1gFnSc2	kleč
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
mugo	mugo	k1gMnSc1	mugo
agg	agg	k?	agg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
samotná	samotný	k2eAgFnSc1d1	samotná
borovice	borovice	k1gFnSc1	borovice
kleč	kleč	k1gFnSc4	kleč
na	na	k7c6	na
vysokohorských	vysokohorský	k2eAgFnPc6d1	vysokohorská
holích	hole	k1gFnPc6	hole
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
,	,	kIx,	,
Jizerských	jizerský	k2eAgFnPc2d1	Jizerská
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
Šumavy	Šumava	k1gFnSc2	Šumava
(	(	kIx(	(
<g/>
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
pohořích	pohoří	k1gNnPc6	pohoří
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
není	být	k5eNaImIp3nS	být
původní	původní	k2eAgInSc1d1	původní
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
lesnické	lesnický	k2eAgFnPc1d1	lesnická
výsadby	výsadba	k1gFnPc1	výsadba
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Jeseníků	Jeseník	k1gInPc2	Jeseník
působí	působit	k5eAaImIp3nS	působit
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
ochraně	ochrana	k1gFnSc6	ochrana
zdejší	zdejší	k2eAgFnSc2d1	zdejší
vzácné	vzácný	k2eAgFnSc2d1	vzácná
květeny	květena	k1gFnSc2	květena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
blatka	blatka	k1gFnSc1	blatka
na	na	k7c6	na
rašeliništích	rašeliniště	k1gNnPc6	rašeliniště
v	v	k7c6	v
jihozápadních	jihozápadní	k2eAgFnPc6d1	jihozápadní
a	a	k8xC	a
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Žďárských	Žďárských	k2eAgInPc6d1	Žďárských
vrších	vrch	k1gInPc6	vrch
a	a	k8xC	a
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
některé	některý	k3yIgInPc1	některý
hybridní	hybridní	k2eAgInPc1d1	hybridní
taxony	taxon	k1gInPc1	taxon
(	(	kIx(	(
<g/>
např.	např.	kA	např.
borovice	borovice	k1gFnSc1	borovice
rašelinná	rašelinný	k2eAgFnSc1d1	rašelinná
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc1	výše
<g/>
)	)	kIx)	)
na	na	k7c6	na
šumavských	šumavský	k2eAgNnPc6d1	Šumavské
rašeliništích	rašeliniště	k1gNnPc6	rašeliniště
<g/>
.	.	kIx.	.
</s>
<s>
Zdomácnělým	zdomácnělý	k2eAgInSc7d1	zdomácnělý
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
jihoevropská	jihoevropský	k2eAgFnSc1d1	jihoevropská
borovice	borovice	k1gFnSc1	borovice
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
využívaná	využívaný	k2eAgFnSc1d1	využívaná
v	v	k7c6	v
sadovnictví	sadovnictví	k1gNnSc6	sadovnictví
i	i	k8xC	i
v	v	k7c6	v
lesnictví	lesnictví	k1gNnSc6	lesnictví
<g/>
,	,	kIx,	,
a	a	k8xC	a
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
borovice	borovice	k1gFnSc1	borovice
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
limitem	limit	k1gInSc7	limit
pro	pro	k7c4	pro
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
pěstování	pěstování	k1gNnSc4	pěstování
dalších	další	k2eAgInPc2d1	další
taxonů	taxon	k1gInPc2	taxon
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
většina	většina	k1gFnSc1	většina
středomořských	středomořský	k2eAgInPc2d1	středomořský
či	či	k8xC	či
středoamerických	středoamerický	k2eAgInPc2d1	středoamerický
druhů	druh	k1gInPc2	druh
nevydrží	vydržet	k5eNaPmIp3nP	vydržet
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
již	již	k6eAd1	již
uvedené	uvedený	k2eAgInPc4d1	uvedený
druhy	druh	k1gInPc4	druh
se	se	k3xPyFc4	se
v	v	k7c6	v
okrasných	okrasný	k2eAgFnPc6d1	okrasná
výsadbách	výsadba	k1gFnPc6	výsadba
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
borovicí	borovice	k1gFnSc7	borovice
těžkou	těžký	k2eAgFnSc7d1	těžká
či	či	k8xC	či
Jeffreyovou	Jeffreyový	k2eAgFnSc7d1	Jeffreyový
<g/>
,	,	kIx,	,
s	s	k7c7	s
borovicí	borovice	k1gFnSc7	borovice
osinatou	osinatý	k2eAgFnSc7d1	osinatá
<g/>
,	,	kIx,	,
borovicí	borovice	k1gFnSc7	borovice
Heldreichovou	Heldreichová	k1gFnSc7	Heldreichová
nebo	nebo	k8xC	nebo
borovicí	borovice	k1gFnSc7	borovice
limbou	limba	k1gFnSc7	limba
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc1d1	další
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
převážně	převážně	k6eAd1	převážně
pouze	pouze	k6eAd1	pouze
sbírkový	sbírkový	k2eAgInSc1d1	sbírkový
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
sbírky	sbírka	k1gFnPc1	sbírka
botanických	botanický	k2eAgInPc2d1	botanický
druhů	druh	k1gInPc2	druh
borovic	borovice	k1gFnPc2	borovice
i	i	k8xC	i
jejich	jejich	k3xOp3gInPc2	jejich
kultivarů	kultivar	k1gInPc2	kultivar
shromáždilo	shromáždit	k5eAaPmAgNnS	shromáždit
například	například	k6eAd1	například
plzeňské	plzeňský	k2eAgNnSc1d1	plzeňské
arboretum	arboretum	k1gNnSc1	arboretum
Sofronka	Sofronka	k1gFnSc1	Sofronka
<g/>
,	,	kIx,	,
Dendrologická	dendrologický	k2eAgFnSc1d1	Dendrologická
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Průhonicích	Průhonice	k1gFnPc6	Průhonice
<g/>
,	,	kIx,	,
arboretum	arboretum	k1gNnSc1	arboretum
České	český	k2eAgFnSc2d1	Česká
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Kostelci	Kostelec	k1gInSc6	Kostelec
nad	nad	k7c7	nad
Černými	černý	k2eAgInPc7d1	černý
lesy	les	k1gInPc7	les
nebo	nebo	k8xC	nebo
arboreta	arboretum	k1gNnSc2	arboretum
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Mendelovy	Mendelův	k2eAgFnSc2d1	Mendelova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
exemplářů	exemplář	k1gInPc2	exemplář
borovic	borovice	k1gFnPc2	borovice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
chráněno	chránit	k5eAaImNgNnS	chránit
jako	jako	k8xS	jako
památné	památný	k2eAgInPc1d1	památný
stromy	strom	k1gInPc1	strom
<g/>
;	;	kIx,	;
v	v	k7c6	v
převážné	převážný	k2eAgFnSc6d1	převážná
většině	většina	k1gFnSc6	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
borovice	borovice	k1gFnPc4	borovice
lesní	lesní	k2eAgFnPc4d1	lesní
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
borovice	borovice	k1gFnPc1	borovice
černé	černý	k2eAgFnPc1d1	černá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
narazit	narazit	k5eAaPmF	narazit
i	i	k9	i
na	na	k7c4	na
památné	památný	k2eAgFnPc4d1	památná
vejmutovky	vejmutovka	k1gFnPc4	vejmutovka
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnPc4	borovice
Jeffreyovy	Jeffreyův	k2eAgFnPc4d1	Jeffreyův
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgFnPc4d1	těžká
<g/>
,	,	kIx,	,
Schwerinovy	Schwerinův	k2eAgFnPc4d1	Schwerinova
nebo	nebo	k8xC	nebo
rumelské	rumelský	k2eAgFnPc4d1	rumelská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BUSINSKÝ	BUSINSKÝ	kA	BUSINSKÝ
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
;	;	kIx,	;
VELEBIL	Velebil	k1gMnSc1	Velebil
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Borovice	borovice	k1gFnSc1	borovice
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Průhonice	Průhonice	k1gFnPc1	Průhonice
<g/>
:	:	kIx,	:
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
ústav	ústav	k1gInSc1	ústav
Silva	Silva	k1gFnSc1	Silva
Taroucy	Tarouca	k1gMnSc2	Tarouca
pro	pro	k7c4	pro
krajinu	krajina	k1gFnSc4	krajina
a	a	k8xC	a
okrasné	okrasný	k2eAgNnSc4d1	okrasné
zahradnictví	zahradnictví	k1gNnSc4	zahradnictví
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85116	[number]	k4	85116
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BUSINSKÝ	BUSINSKÝ	kA	BUSINSKÝ
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Komentovaný	komentovaný	k2eAgInSc4d1	komentovaný
světový	světový	k2eAgInSc4d1	světový
klíč	klíč	k1gInSc4	klíč
rodu	rod	k1gInSc2	rod
Pinus	Pinus	k1gInSc1	Pinus
L.	L.	kA	L.
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
VÚKOZ	VÚKOZ	kA	VÚKOZ
Průhonice	Průhonice	k1gFnPc1	Průhonice
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BUSINSKÝ	BUSINSKÝ	kA	BUSINSKÝ
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Genus	Genus	k1gInSc1	Genus
Pinus	Pinus	k1gInSc1	Pinus
L.	L.	kA	L.
<g/>
,	,	kIx,	,
Pinesː	Pinesː	k1gFnSc1	Pinesː
Contribution	Contribution	k1gInSc1	Contribution
to	ten	k3xDgNnSc4	ten
Knowledge	Knowledge	k1gNnSc4	Knowledge
<g/>
.	.	kIx.	.
</s>
<s>
Průhonice	Průhonice	k1gFnPc1	Průhonice
<g/>
:	:	kIx,	:
Acta	Act	k2eAgFnSc1d1	Acta
Pruhoniciana	Pruhoniciana	k1gFnSc1	Pruhoniciana
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
120	[number]	k4	120
s.	s.	k?	s.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MIROV	MIROV	kA	MIROV
<g/>
,	,	kIx,	,
Nicholas	Nicholas	k1gMnSc1	Nicholas
T.	T.	kA	T.
<g/>
;	;	kIx,	;
HASBROUCK	HASBROUCK	kA	HASBROUCK
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
story	story	k1gFnSc1	story
of	of	k?	of
pines	pines	k1gInSc1	pines
<g/>
.	.	kIx.	.
</s>
<s>
Bloomington	Bloomington	k1gInSc1	Bloomington
<g/>
:	:	kIx,	:
Indiana	Indiana	k1gFnSc1	Indiana
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
35462	[number]	k4	35462
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MIROV	MIROV	kA	MIROV
<g/>
,	,	kIx,	,
Nicholas	Nicholas	k1gMnSc1	Nicholas
T.	T.	kA	T.
The	The	k1gMnSc1	The
genus	genus	k1gMnSc1	genus
Pinus	Pinus	k1gMnSc1	Pinus
<g/>
..	..	k?	..
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Ronald	Ronald	k1gMnSc1	Ronald
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FARJON	FARJON	kA	FARJON
<g/>
,	,	kIx,	,
Aljos	Aljos	k1gInSc1	Aljos
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Handbook	handbook	k1gInSc1	handbook
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Conifers	Conifersa	k1gFnPc2	Conifersa
<g/>
..	..	k?	..
Boston	Boston	k1gInSc1	Boston
<g/>
:	:	kIx,	:
Brill	Brill	k1gInSc1	Brill
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
17718	[number]	k4	17718
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Karanténní	karanténní	k2eAgFnSc1d1	karanténní
sypavka	sypavka	k1gFnSc1	sypavka
borovic	borovice	k1gFnPc2	borovice
</s>
</p>
<p>
<s>
Chřadnutí	chřadnutí	k1gNnSc1	chřadnutí
a	a	k8xC	a
prosychání	prosychání	k1gNnSc1	prosychání
borovic	borovice	k1gFnPc2	borovice
</s>
</p>
<p>
<s>
Bor	bor	k1gInSc1	bor
(	(	kIx(	(
<g/>
les	les	k1gInSc1	les
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
(	(	kIx(	(
<g/>
symbol	symbol	k1gInSc1	symbol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
borovice	borovice	k1gFnSc2	borovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
borovice	borovice	k1gFnSc2	borovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
borovice	borovice	k1gFnSc2	borovice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Pinetum	Pinetum	k1gNnSc1	Pinetum
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
–	–	k?	–
internetový	internetový	k2eAgInSc4d1	internetový
portál	portál	k1gInSc4	portál
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
borovicím	borovice	k1gFnPc3	borovice
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arboretum	arboretum	k1gNnSc4	arboretum
Sofronka	Sofronka	k1gFnSc1	Sofronka
–	–	k?	–
české	český	k2eAgNnSc4d1	české
arboretum	arboretum	k1gNnSc4	arboretum
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
na	na	k7c4	na
borovice	borovice	k1gFnPc4	borovice
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Conifersociety	Conifersocieta	k1gFnSc2	Conifersocieta
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
