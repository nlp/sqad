<s>
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
</s>
<s>
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libraZemě	libraZemě	k6eAd1
</s>
<s>
Gibraltar	Gibraltar	k1gInSc4
Gibraltar	Gibraltar	k1gInSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
GIP	GIP	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
2,5	2,5	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2015	#num#	k4
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
£	£	k?
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
pence	pence	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
5	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
50	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
£	£	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
2	#num#	k4
Bankovky	bankovka	k1gFnSc2
</s>
<s>
£	£	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
10	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
20	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
50	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
100	#num#	k4
</s>
<s>
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
je	být	k5eAaImIp3nS
zákonné	zákonný	k2eAgNnSc4d1
platidlo	platidlo	k1gNnSc4
Gibraltaru	Gibraltar	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
status	status	k1gInSc4
Britské	britský	k2eAgNnSc1d1
zámořské	zámořský	k2eAgNnSc1d1
teritorium	teritorium	k1gNnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
British	British	k1gInSc1
overseas	overseas	k1gInSc1
territory	territor	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
součástí	součást	k1gFnSc7
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
patří	patřit	k5eAaImIp3nS
pod	pod	k7c4
jeho	jeho	k3xOp3gFnSc4
suverenitu	suverenita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc4
gibraltarské	gibraltarský	k2eAgFnSc2d1
libry	libra	k1gFnSc2
je	být	k5eAaImIp3nS
GIP	GIP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
je	být	k5eAaImIp3nS
pevně	pevně	k6eAd1
navázána	navázán	k2eAgFnSc1d1
na	na	k7c4
měnu	měna	k1gFnSc4
svého	svůj	k3xOyFgInSc2
nadřazeného	nadřazený	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
celku-	celku-	k?
libru	libra	k1gFnSc4
šterlinků	šterlink	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kurs	kurs	k1gInSc1
je	být	k5eAaImIp3nS
stanoven	stanovit	k5eAaPmNgInS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Gibraltaru	Gibraltar	k1gInSc6
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
platit	platit	k5eAaImF
jak	jak	k6eAd1
britskou	britský	k2eAgFnSc4d1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
gibraltarskou	gibraltarský	k2eAgFnSc7d1
librou	libra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
toto	tento	k3xDgNnSc1
ovšem	ovšem	k9
neplatí	platit	k5eNaImIp3nS
–	–	k?
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
gibraltarskou	gibraltarský	k2eAgFnSc4d1
libru	libra	k1gFnSc4
použít	použít	k5eAaPmF
nelze	lze	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Gibraltar	Gibraltar	k1gInSc1
tiskne	tisknout	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnPc4d1
bankovky	bankovka	k1gFnPc4
už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1927	#num#	k4
<g/>
,	,	kIx,
mince	mince	k1gFnSc1
razí	razit	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1553	#num#	k4
<g/>
-	-	kIx~
<g/>
8133	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
web	web	k1gInSc1
www.gibraltar-stamps.com	www.gibraltar-stamps.com	k1gInSc1
-	-	kIx~
vyobrazení	vyobrazení	k1gNnSc1
gibraltarských	gibraltarský	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
web	web	k1gInSc1
www.gibraltar-stamps.com	www.gibraltar-stamps.com	k1gInSc1
-	-	kIx~
vyobrazení	vyobrazení	k1gNnSc1
gibraltarských	gibraltarský	k2eAgFnPc2d1
bankovek	bankovka	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnSc2
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
