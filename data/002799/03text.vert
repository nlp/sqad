<s>
Jiří	Jiří	k1gMnSc1	Jiří
Korn	Korn	k1gMnSc1	Korn
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
muzikálový	muzikálový	k2eAgMnSc1d1	muzikálový
a	a	k8xC	a
popový	popový	k2eAgMnSc1d1	popový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Rebels	Rebelsa	k1gFnPc2	Rebelsa
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
hit	hit	k1gInSc1	hit
Šípková	šípkový	k2eAgFnSc1d1	šípková
Růženka	Růženka	k1gFnSc1	Růženka
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skupiny	skupina	k1gFnSc2	skupina
Olympic	Olympice	k1gInPc2	Olympice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
interpretem	interpret	k1gMnSc7	interpret
např.	např.	kA	např.
písně	píseň	k1gFnPc4	píseň
Únos	únos	k1gInSc1	únos
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
angažmá	angažmá	k1gNnSc2	angažmá
v	v	k7c6	v
Olympicu	Olympicus	k1gInSc6	Olympicus
zpěvák	zpěvák	k1gMnSc1	zpěvák
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svoji	svůj	k3xOyFgFnSc4	svůj
sólovou	sólový	k2eAgFnSc4d1	sólová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
své	svůj	k3xOyFgFnSc2	svůj
písně	píseň	k1gFnSc2	píseň
spojil	spojit	k5eAaPmAgMnS	spojit
i	i	k9	i
s	s	k7c7	s
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
televizním	televizní	k2eAgInSc7d1	televizní
cyklem	cyklus	k1gInSc7	cyklus
Možná	možná	k9	možná
přijde	přijít	k5eAaPmIp3nS	přijít
i	i	k9	i
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
proměně	proměna	k1gFnSc3	proměna
Kornovy	Kornovy	k?	Kornovy
kariéry	kariéra	k1gFnPc1	kariéra
v	v	k7c4	v
taneční	taneční	k1gFnPc4	taneční
a	a	k8xC	a
kritiky	kritik	k1gMnPc4	kritik
i	i	k9	i
více	hodně	k6eAd2	hodně
oceňovanou	oceňovaný	k2eAgFnSc4d1	oceňovaná
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
vokálního	vokální	k2eAgNnSc2d1	vokální
kvarteta	kvarteto	k1gNnSc2	kvarteto
4	[number]	k4	4
<g/>
TET	Teta	k1gFnPc2	Teta
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Honza	Honza	k1gMnSc1	Honza
málem	málem	k6eAd1	málem
králem	král	k1gMnSc7	král
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
spojil	spojit	k5eAaPmAgMnS	spojit
kariéru	kariéra	k1gFnSc4	kariéra
s	s	k7c7	s
muzikály	muzikál	k1gInPc7	muzikál
(	(	kIx(	(
<g/>
Bídníci	bídník	k1gMnPc1	bídník
<g/>
,	,	kIx,	,
Dracula	Draculum	k1gNnPc1	Draculum
<g/>
,	,	kIx,	,
Monte	Mont	k1gMnSc5	Mont
Cristo	Crista	k1gMnSc5	Crista
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
i	i	k9	i
v	v	k7c6	v
ledové	ledový	k2eAgFnSc6d1	ledová
revui	revue	k1gFnSc6	revue
Mrazík	mrazík	k1gInSc1	mrazík
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
kulečník	kulečník	k1gInSc4	kulečník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
celostátního	celostátní	k2eAgMnSc2d1	celostátní
přeborníka	přeborník	k1gMnSc2	přeborník
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
šokuje	šokovat	k5eAaBmIp3nS	šokovat
společnost	společnost	k1gFnSc4	společnost
výstředním	výstřední	k2eAgInSc7d1	výstřední
výběrem	výběr	k1gInSc7	výběr
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
bývalá	bývalý	k2eAgFnSc1d1	bývalá
manželka	manželka	k1gFnSc1	manželka
Kateřina	Kateřina	k1gFnSc1	Kateřina
Kornová	kornový	k2eAgFnSc1d1	Kornová
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
moderátorka	moderátorka	k1gFnSc1	moderátorka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vlastní	vlastní	k2eAgInSc4d1	vlastní
kadeřnický	kadeřnický	k2eAgInSc4d1	kadeřnický
salón	salón	k1gInSc4	salón
<g/>
..	..	k?	..
Robinson	Robinson	k1gMnSc1	Robinson
Yvetta	Yvetta	k1gFnSc1	Yvetta
Rána	Rána	k1gFnSc1	Rána
jsou	být	k5eAaImIp3nP	být
zlá	zlý	k2eAgFnSc1d1	zlá
Tam	tam	k6eAd1	tam
u	u	k7c2	u
dvou	dva	k4xCgFnPc2	dva
cest	cesta	k1gFnPc2	cesta
Ne	ne	k9	ne
maestro	maestro	k1gMnSc1	maestro
Klaudie	Klaudie	k1gFnSc2	Klaudie
Zpívat	zpívat	k5eAaImF	zpívat
jako	jako	k9	jako
déšť	déšť	k1gInSc1	déšť
Žal	žal	k1gInSc1	žal
se	se	k3xPyFc4	se
odkládá	odkládat	k5eAaImIp3nS	odkládat
Hotel	hotel	k1gInSc1	hotel
Ritz	Ritz	k1gInSc1	Ritz
Každá	každý	k3xTgFnSc1	každý
trampota	trampota	k1gFnSc1	trampota
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
mez	mez	k1gFnSc4	mez
Windsurfing	windsurfing	k1gInSc1	windsurfing
Karel	Karel	k1gMnSc1	Karel
nese	nést	k5eAaImIp3nS	nést
asi	asi	k9	asi
<g />
.	.	kIx.	.
</s>
<s>
čaj	čaj	k1gInSc1	čaj
Miss	miss	k1gFnSc1	miss
Moskva	Moskva	k1gFnSc1	Moskva
Hlava	hlava	k1gFnSc1	hlava
mazaná	mazaný	k2eAgFnSc1d1	mazaná
Té	ten	k3xDgFnSc6	ten
co	co	k3yRnSc1	co
snídá	snídat	k5eAaImIp3nS	snídat
Ještě	ještě	k6eAd1	ještě
tě	ty	k3xPp2nSc4	ty
mám	mít	k5eAaImIp1nS	mít
plnou	plný	k2eAgFnSc4d1	plná
náruč	náruč	k1gFnSc4	náruč
Mandarín	mandarín	k1gMnSc1	mandarín
Slunce	slunce	k1gNnSc2	slunce
Jakoby	jakoby	k8xS	jakoby
nic	nic	k6eAd1	nic
"	"	kIx"	"
<g/>
To	to	k9	to
pan	pan	k1gMnSc1	pan
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
půjdu	jít	k5eAaImIp1nS	jít
tam	tam	k6eAd1	tam
a	a	k8xC	a
ty	ten	k3xDgFnPc4	ten
tam	tam	k6eAd1	tam
<g/>
"	"	kIx"	"
festival	festival	k1gInSc1	festival
"	"	kIx"	"
<g/>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
"	"	kIx"	"
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
)	)	kIx)	)
Cavan	Cavan	k1gMnSc1	Cavan
Internationale	Internationale	k1gFnSc2	Internationale
Song	song	k1gInSc1	song
Contest	Contest	k1gInSc1	Contest
-	-	kIx~	-
Irsko	Irsko	k1gNnSc1	Irsko
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
<g/>
,	,	kIx,	,
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
píseň	píseň	k1gFnSc1	píseň
a	a	k8xC	a
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
interpret	interpret	k1gMnSc1	interpret
-	-	kIx~	-
skladba	skladba	k1gFnSc1	skladba
You	You	k1gFnSc2	You
are	ar	k1gInSc5	ar
my	my	k3xPp1nPc1	my
Voo-doo	Voooo	k1gMnSc1	Voo-doo
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
Thálie	Thálie	k1gFnSc1	Thálie
2004	[number]	k4	2004
Wellness	Wellness	k1gInSc1	Wellness
celebrity	celebrita	k1gFnSc2	celebrita
award	award	k6eAd1	award
2006	[number]	k4	2006
Osobnost	osobnost	k1gFnSc1	osobnost
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
1976	[number]	k4	1976
Honza	Honza	k1gMnSc1	Honza
málem	málem	k6eAd1	málem
králem	král	k1gMnSc7	král
-	-	kIx~	-
Honza	Honza	k1gMnSc1	Honza
1978	[number]	k4	1978
<g />
.	.	kIx.	.
</s>
<s>
Sólo	sólo	k1gNnSc1	sólo
pro	pro	k7c4	pro
starou	starý	k2eAgFnSc4d1	stará
dámu	dáma	k1gFnSc4	dáma
-	-	kIx~	-
zpěvák	zpěvák	k1gMnSc1	zpěvák
1980	[number]	k4	1980
Trhák	trhák	k1gInSc1	trhák
1981	[number]	k4	1981
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
bratří	bratr	k1gMnPc2	bratr
Mánesů	Mánes	k1gMnPc2	Mánes
1983	[number]	k4	1983
Anděl	Anděla	k1gFnPc2	Anděla
s	s	k7c7	s
ďáblem	ďábel	k1gMnSc7	ďábel
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
1988	[number]	k4	1988
Anděl	Anděla	k1gFnPc2	Anděla
svádí	svádět	k5eAaImIp3nS	svádět
ďábla	ďábel	k1gMnSc2	ďábel
1990	[number]	k4	1990
Téměř	téměř	k6eAd1	téměř
růžový	růžový	k2eAgInSc1d1	růžový
příběh	příběh	k1gInSc1	příběh
-	-	kIx~	-
strýc	strýc	k1gMnSc1	strýc
Leonard	Leonard	k1gInSc4	Leonard
1999	[number]	k4	1999
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
Báječná	báječný	k2eAgFnSc1d1	báječná
show	show	k1gFnSc1	show
2002	[number]	k4	2002
Brak	braka	k1gFnPc2	braka
-	-	kIx~	-
vrah	vrah	k1gMnSc1	vrah
2005	[number]	k4	2005
Trampoty	trampota	k1gFnSc2	trampota
vodníka	vodník	k1gMnSc4	vodník
Jakoubka	Jakoubek	k1gMnSc4	Jakoubek
<g />
.	.	kIx.	.
</s>
<s>
2006	[number]	k4	2006
Kde	kde	k6eAd1	kde
lampy	lampa	k1gFnPc1	lampa
bloudí	bloudit	k5eAaImIp3nP	bloudit
2006	[number]	k4	2006
Letiště	letiště	k1gNnSc2	letiště
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Kvaska	Kvasko	k1gNnSc2	Kvasko
-	-	kIx~	-
Michal	Michal	k1gMnSc1	Michal
2007	[number]	k4	2007
Nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
peklo	peklo	k1gNnSc1	peklo
<g/>
,	,	kIx,	,
zem	zem	k1gFnSc1	zem
(	(	kIx(	(
<g/>
slovenský	slovenský	k2eAgInSc1d1	slovenský
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Bathory	Bathora	k1gFnSc2	Bathora
2009	[number]	k4	2009
2	[number]	k4	2
<g/>
Bobule	bobule	k1gFnSc1	bobule
2011	[number]	k4	2011
Hranaři	hranař	k1gMnPc1	hranař
2011	[number]	k4	2011
Expozitura	expozitura	k1gFnSc1	expozitura
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g />
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
králové	král	k1gMnPc1	král
<g/>
)	)	kIx)	)
-	-	kIx~	-
nájemný	nájemný	k2eAgMnSc1d1	nájemný
vrah	vrah	k1gMnSc1	vrah
Frantík	Frantík	k1gMnSc1	Frantík
1972	[number]	k4	1972
Jiří	Jiří	k1gMnSc1	Jiří
Korn	Korn	k1gMnSc1	Korn
LP	LP	kA	LP
01	[number]	k4	01
-	-	kIx~	-
Supraphon	supraphon	k1gInSc4	supraphon
1974	[number]	k4	1974
Jiří	Jiří	k1gMnSc1	Jiří
Korn	Korn	k1gMnSc1	Korn
-	-	kIx~	-
Amiga	Amiga	k1gFnSc1	Amiga
1978	[number]	k4	1978
Jiří	Jiří	k1gMnSc1	Jiří
Korn	Korn	k1gMnSc1	Korn
LP	LP	kA	LP
02	[number]	k4	02
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
1978	[number]	k4	1978
Radost	radost	k6eAd1	radost
až	až	k9	až
do	do	k7c2	do
rána	ráno	k1gNnSc2	ráno
<g/>
/	/	kIx~	/
<g/>
Balón	balón	k1gInSc1	balón
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
43	[number]	k4	43
2229	[number]	k4	2229
<g/>
,	,	kIx,	,
SP	SP	kA	SP
1979	[number]	k4	1979
Zpívat	zpívat	k5eAaImF	zpívat
jako	jako	k8xC	jako
déšť	déšť	k1gInSc1	déšť
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
1980	[number]	k4	1980
<g />
.	.	kIx.	.
</s>
<s>
Singing	Singing	k1gInSc1	Singing
And	Anda	k1gFnPc2	Anda
Dancing	dancing	k1gInSc4	dancing
1980	[number]	k4	1980
Gentleman	gentleman	k1gMnSc1	gentleman
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
1982	[number]	k4	1982
Hej	hej	k6eAd1	hej
<g/>
...	...	k?	...
<g/>
(	(	kIx(	(
<g/>
poslouchej	poslouchat	k5eAaImRp2nS	poslouchat
<g/>
)	)	kIx)	)
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
1984	[number]	k4	1984
24	[number]	k4	24
Stop	stopa	k1gFnPc2	stopa
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
1984	[number]	k4	1984
Magic	Magice	k1gInPc2	Magice
jet	jet	k5eAaImF	jet
(	(	kIx(	(
<g/>
pozměněná	pozměněný	k2eAgFnSc1d1	pozměněná
anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
alba	album	k1gNnSc2	album
24	[number]	k4	24
Stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
-	-	kIx~	-
Supraphon	supraphon	k1gInSc4	supraphon
1986	[number]	k4	1986
Trénink	trénink	k1gInSc4	trénink
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
1989	[number]	k4	1989
Switch	Switcha	k1gFnPc2	Switcha
Off	Off	k1gFnPc2	Off
Before	Befor	k1gInSc5	Befor
Leaving	Leaving	k1gInSc4	Leaving
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
LP	LP	kA	LP
Před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
vypni	vypnout	k5eAaPmRp2nS	vypnout
proud	proud	k1gInSc1	proud
<g/>
)	)	kIx)	)
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
1989	[number]	k4	1989
Před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
vypni	vypnout	k5eAaPmRp2nS	vypnout
proud	proud	k1gInSc1	proud
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
1992	[number]	k4	1992
O5	O5	k1gFnPc2	O5
(	(	kIx(	(
<g/>
Opět	opět	k6eAd1	opět
<g/>
)	)	kIx)	)
-	-	kIx~	-
Monitor	monitor	k1gInSc4	monitor
1993	[number]	k4	1993
Pastel	pastel	k1gInSc1	pastel
Songs	Songs	k1gInSc4	Songs
-	-	kIx~	-
Frydrych	Frydrych	k1gMnSc1	Frydrych
Music	Music	k1gMnSc1	Music
1995	[number]	k4	1995
Duny	duna	k1gFnSc2	duna
-	-	kIx~	-
Happy	Happa	k1gFnSc2	Happa
Music	Music	k1gMnSc1	Music
Production	Production	k1gInSc4	Production
1996	[number]	k4	1996
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
šoubyznys	šoubyznys	k1gInSc1	šoubyznys
-	-	kIx~	-
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Korn	Korn	k1gMnSc1	Korn
2001	[number]	k4	2001
<g />
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
hity-	hity-	k?	hity-
Sony	Sony	kA	Sony
Music	Music	k1gMnSc1	Music
Bonton	bonton	k1gInSc4	bonton
2002	[number]	k4	2002
Tep	tep	k1gInSc1	tep
-	-	kIx~	-
Sony	Sony	kA	Sony
Music	Music	k1gMnSc1	Music
Bonton	bonton	k1gInSc4	bonton
2005	[number]	k4	2005
Robinson	Robinson	k1gMnSc1	Robinson
-	-	kIx~	-
Areca	Areca	k1gMnSc1	Areca
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
edice	edice	k1gFnPc1	edice
Portréty	portrét	k1gInPc1	portrét
českých	český	k2eAgFnPc2d1	Česká
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Těch	ten	k3xDgInPc2	ten
pár	pár	k4xCyI	pár
dnů	den	k1gInPc2	den
-	-	kIx~	-
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Korn	Korn	k1gMnSc1	Korn
-	-	kIx~	-
(	(	kIx(	(
<g/>
CD	CD	kA	CD
aj	aj	kA	aj
DVD	DVD	kA	DVD
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kolekce	kolekce	k1gFnSc1	kolekce
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
2009	[number]	k4	2009
Muzikál	muzikál	k1gInSc4	muzikál
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
-	-	kIx~	-
EMI	EMI	kA	EMI
Czech	Czech	k1gInSc4	Czech
Republic	Republice	k1gFnPc2	Republice
1967	[number]	k4	1967
Šípková	šípkový	k2eAgFnSc1d1	šípková
Růženka	Růženka	k1gFnSc1	Růženka
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
1996	[number]	k4	1996
Rebels	Rebelsa	k1gFnPc2	Rebelsa
Komplet	komplet	k6eAd1	komplet
-	-	kIx~	-
Bonton	bonton	k1gInSc1	bonton
1999	[number]	k4	1999
Rebels	Rebelsa	k1gFnPc2	Rebelsa
-	-	kIx~	-
Black	Black	k1gInSc4	Black
Point	pointa	k1gFnPc2	pointa
2003	[number]	k4	2003
Rebels	Rebelsa	k1gFnPc2	Rebelsa
Live	Live	k1gFnPc2	Live
-	-	kIx~	-
Black	Black	k1gInSc4	Black
Point	pointa	k1gFnPc2	pointa
1973	[number]	k4	1973
Olympic	Olympice	k1gFnPc2	Olympice
4	[number]	k4	4
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
2004	[number]	k4	2004
4TET	[number]	k4	4TET
1	[number]	k4	1
<g/>
st	st	kA	st
-	-	kIx~	-
Areca	Arec	k2eAgNnPc1d1	Arec
Multimedia	multimedium	k1gNnPc1	multimedium
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Akord	akord	k1gInSc1	akord
Shop	shop	k1gInSc1	shop
2005	[number]	k4	2005
4TET	[number]	k4	4TET
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
nd	nd	k?	nd
-	-	kIx~	-
Areca	Arec	k2eAgNnPc1d1	Arec
Multimedia	multimedium	k1gNnPc1	multimedium
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Akord	akord	k1gInSc1	akord
Shop	shop	k1gInSc1	shop
2008	[number]	k4	2008
4TET	[number]	k4	4TET
3	[number]	k4	3
<g/>
rd	rd	k?	rd
-	-	kIx~	-
Akord	akord	k1gInSc1	akord
Shop	shop	k1gInSc4	shop
Bídníci	bídník	k1gMnPc1	bídník
<g/>
....	....	k?	....
<g/>
Thenardiér	Thenardiér	k1gMnSc1	Thenardiér
Dracula	Dracul	k1gMnSc2	Dracul
<g/>
....	....	k?	....
<g/>
šašek	šašek	k1gMnSc1	šašek
<g/>
/	/	kIx~	/
<g/>
sluha	sluha	k1gMnSc1	sluha
<g/>
/	/	kIx~	/
<g/>
profesor	profesor	k1gMnSc1	profesor
Golem	Golem	k1gMnSc1	Golem
...	...	k?	...
<g/>
Mordechaj	Mordechaj	k1gMnSc1	Mordechaj
Hans	Hans	k1gMnSc1	Hans
Andersen	Andersen	k1gMnSc1	Andersen
(	(	kIx(	(
<g/>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
...	...	k?	...
<g/>
Hogo	Hogo	k1gMnSc1	Hogo
Fogo	Fogo	k1gMnSc1	Fogo
Mise	mise	k1gFnSc1	mise
...	...	k?	...
<g/>
Gort	Gort	k1gInSc1	Gort
Miss	miss	k1gFnPc2	miss
Saigon	Saigon	k1gInSc1	Saigon
<g/>
....	....	k?	....
<g/>
Engeneer	Engeneer	k1gInSc1	Engeneer
Monte	Mont	k1gMnSc5	Mont
Cristo	Crista	k1gMnSc5	Crista
<g/>
....	....	k?	....
<g/>
Danglars	Danglarsa	k1gFnPc2	Danglarsa
Mrazík	mrazík	k1gInSc1	mrazík
(	(	kIx(	(
<g/>
muzikál	muzikál	k1gInSc1	muzikál
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
)	)	kIx)	)
...	...	k?	...
<g/>
Baba	baba	k1gFnSc1	baba
Jaga	Jaga	k1gFnSc1	Jaga
Quasimodo	Quasimodo	k1gMnSc1	Quasimodo
<g/>
...	...	k?	...
Claudius	Claudius	k1gMnSc1	Claudius
Frollo	Frollo	k1gNnSc1	Frollo
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
(	(	kIx(	(
<g/>
muzikál	muzikál	k1gInSc1	muzikál
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
...	...	k?	...
role	role	k1gFnSc1	role
-	-	kIx~	-
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
Touha	touha	k1gFnSc1	touha
.....	.....	k?	.....
<g/>
soudce	soudce	k1gMnSc1	soudce
Carmen	Carmen	k2eAgMnSc1d1	Carmen
....	....	k?	....
<g/>
starosta	starosta	k1gMnSc1	starosta
Jesus	Jesus	k1gMnSc1	Jesus
Christ	Christ	k1gMnSc1	Christ
Superstar	superstar	k1gFnSc1	superstar
(	(	kIx(	(
<g/>
Hudební	hudební	k2eAgNnSc1d1	hudební
Divadlo	divadlo	k1gNnSc1	divadlo
Karlín	Karlín	k1gInSc1	Karlín
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
...	...	k?	...
Herodes	Herodes	k1gMnSc1	Herodes
Popelka	Popelka	k1gMnSc1	Popelka
(	(	kIx(	(
<g/>
muzikál	muzikál	k1gInSc1	muzikál
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
)	)	kIx)	)
Aida	Aida	k1gFnSc1	Aida
(	(	kIx(	(
<g/>
Hudební	hudební	k2eAgNnSc1d1	hudební
divadlo	divadlo	k1gNnSc1	divadlo
Karlín	Karlín	k1gInSc1	Karlín
<g/>
)	)	kIx)	)
...	...	k?	...
Zoser	Zoser	k1gInSc1	Zoser
Mauglí	Mauglí	k1gNnSc1	Mauglí
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
<g />
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
<g/>
)	)	kIx)	)
...	...	k?	...
Panter	panter	k1gInSc1	panter
Baghíra	Baghír	k1gMnSc2	Baghír
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
,	,	kIx,	,
větší	veliký	k2eAgNnSc1d2	veliký
než	než	k8xS	než
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
Hudební	hudební	k2eAgNnSc1d1	hudební
divadlo	divadlo	k1gNnSc1	divadlo
Karlín	Karlín	k1gInSc1	Karlín
<g/>
)	)	kIx)	)
...	...	k?	...
Šéf	šéf	k1gMnSc1	šéf
tiskárny	tiskárna	k1gFnSc2	tiskárna
The	The	k1gMnSc1	The
Addams	Addamsa	k1gFnPc2	Addamsa
Family	Famila	k1gFnSc2	Famila
(	(	kIx(	(
<g/>
Hudební	hudební	k2eAgNnSc1d1	hudební
divadlo	divadlo	k1gNnSc1	divadlo
Karlín	Karlín	k1gInSc1	Karlín
<g/>
)	)	kIx)	)
...	...	k?	...
Gomez	Gomez	k1gInSc1	Gomez
Addams	Addamsa	k1gFnPc2	Addamsa
Služky	služka	k1gFnSc2	služka
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Rubín	rubín	k1gInSc1	rubín
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
...	...	k?	...
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Milostivá	milostivý	k2eAgFnSc1d1	milostivá
Paní	paní	k1gFnSc1	paní
Rudá	rudý	k2eAgFnSc1d1	rudá
Magie	magie	k1gFnSc1	magie
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
