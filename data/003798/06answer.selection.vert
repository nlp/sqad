<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Linux	Linux	kA	Linux
používá	používat	k5eAaImIp3nS	používat
unixové	unixový	k2eAgNnSc1d1	unixové
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
myšlenek	myšlenka	k1gFnPc2	myšlenka
Unixu	Unix	k1gInSc2	Unix
a	a	k8xC	a
respektuje	respektovat	k5eAaImIp3nS	respektovat
příslušné	příslušný	k2eAgInPc4d1	příslušný
standardy	standard	k1gInPc4	standard
POSIX	POSIX	kA	POSIX
a	a	k8xC	a
Single	singl	k1gInSc5	singl
UNIX	UNIX	kA	UNIX
Specification	Specification	k1gInSc1	Specification
<g/>
.	.	kIx.	.
</s>
