<p>
<s>
Rudi	ruď	k1gFnPc1	ruď
Dolezal	Dolezal	k1gMnPc2	Dolezal
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1958	[number]	k4	1958
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Hannesem	Hannes	k1gMnSc7	Hannes
Rossacherem	Rossacher	k1gMnSc7	Rossacher
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
tří	tři	k4xCgFnPc2	tři
cen	cena	k1gFnPc2	cena
Romy	Rom	k1gMnPc4	Rom
za	za	k7c4	za
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
Weltberühmt	Weltberühmt	k2eAgInSc4d1	Weltberühmt
in	in	k?	in
Österreich	Österreich	k1gInSc4	Österreich
–	–	k?	–
50	[number]	k4	50
Jahre	Jahr	k1gInSc5	Jahr
Austropop	Austropop	k1gInSc1	Austropop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rudi	ruď	k1gFnSc2	ruď
Dolezal	Dolezal	k1gFnSc2	Dolezal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Rudi	ruď	k1gFnPc1	ruď
Dolezal	Dolezal	k1gFnSc2	Dolezal
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
