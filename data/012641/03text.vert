<p>
<s>
Alex	Alex	k1gMnSc1	Alex
Dowis	Dowis	k1gFnSc2	Dowis
je	být	k5eAaImIp3nS	být
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
scénograf	scénograf	k1gMnSc1	scénograf
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
i	i	k8xC	i
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
divadelních	divadelní	k2eAgFnPc2d1	divadelní
a	a	k8xC	a
filmových	filmový	k2eAgFnPc2d1	filmová
produkcí	produkce	k1gFnPc2	produkce
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Alex	Alex	k1gMnSc1	Alex
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
malbě	malba	k1gFnSc3	malba
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gFnSc2	jeho
rané	raný	k2eAgFnSc2d1	raná
tvorby	tvorba	k1gFnSc2	tvorba
tvoří	tvořit	k5eAaImIp3nP	tvořit
velké	velký	k2eAgFnPc1d1	velká
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
a	a	k8xC	a
venkovních	venkovní	k2eAgInPc2d1	venkovní
designů	design	k1gInPc2	design
<g/>
,	,	kIx,	,
vycházejících	vycházející	k2eAgFnPc2d1	vycházející
zejména	zejména	k9	zejména
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
umění	umění	k1gNnSc2	umění
graffity	graffita	k1gFnSc2	graffita
a	a	k8xC	a
street	streeta	k1gFnPc2	streeta
art	art	k?	art
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
se	se	k3xPyFc4	se
Alex	Alex	k1gMnSc1	Alex
Dowis	Dowis	k1gInSc4	Dowis
zabýval	zabývat	k5eAaImAgMnS	zabývat
provozem	provoz	k1gInSc7	provoz
hudebního	hudební	k2eAgInSc2d1	hudební
klubu	klub	k1gInSc2	klub
a	a	k8xC	a
produkcí	produkce	k1gFnSc7	produkce
desítek	desítka	k1gFnPc2	desítka
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
zakladatel	zakladatel	k1gMnSc1	zakladatel
produkční	produkční	k2eAgFnSc2d1	produkční
agentury	agentura	k1gFnSc2	agentura
DEPO	depo	k1gNnSc1	depo
CREW	CREW	kA	CREW
a	a	k8xC	a
festivalu	festival	k1gInSc2	festival
HIP	hip	k0	hip
HOP	hop	k0	hop
KEMP	kemp	k1gInSc4	kemp
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
začal	začít	k5eAaPmAgInS	začít
zároveň	zároveň	k6eAd1	zároveň
aktivně	aktivně	k6eAd1	aktivně
působit	působit	k5eAaImF	působit
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
divadle	divadlo	k1gNnSc6	divadlo
Archa	archa	k1gFnSc1	archa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yQgMnPc3	který
posléze	posléze	k6eAd1	posléze
představil	představit	k5eAaPmAgInS	představit
četná	četný	k2eAgNnPc4d1	četné
autorská	autorský	k2eAgNnPc4d1	autorské
představení	představení	k1gNnPc4	představení
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
Alex	Alex	k1gMnSc1	Alex
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k6eAd1	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
televizní	televizní	k2eAgFnSc2d1	televizní
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
Slovensko	Slovensko	k1gNnSc4	Slovensko
má	mít	k5eAaImIp3nS	mít
talent	talent	k1gInSc1	talent
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
Čech	Čech	k1gMnSc1	Čech
představil	představit	k5eAaPmAgMnS	představit
světu	svět	k1gInSc3	svět
Light	Light	k1gMnSc1	Light
Art	Art	k1gMnSc1	Art
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
rozšířit	rozšířit	k5eAaPmF	rozšířit
svůj	svůj	k3xOyFgInSc4	svůj
ateliér	ateliér	k1gInSc4	ateliér
Dowis	Dowis	k1gFnSc2	Dowis
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
o	o	k7c4	o
další	další	k2eAgMnPc4d1	další
výtvarníky	výtvarník	k1gMnPc4	výtvarník
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
Light	Light	k2eAgMnSc1d1	Light
Art	Art	k1gMnSc1	Art
Show	show	k1gFnSc2	show
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
TaFantastika	TaFantastika	k1gFnSc1	TaFantastika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
se	se	k3xPyFc4	se
Alex	Alex	k1gMnSc1	Alex
věnuje	věnovat	k5eAaPmIp3nS	věnovat
filmovým	filmový	k2eAgFnPc3d1	filmová
dekoracím	dekorace	k1gFnPc3	dekorace
<g/>
,	,	kIx,	,
scénografii	scénografie	k1gFnSc3	scénografie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyhledávaný	vyhledávaný	k2eAgInSc1d1	vyhledávaný
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
reklamních	reklamní	k2eAgFnPc6d1	reklamní
kampaních	kampaň	k1gFnPc6	kampaň
(	(	kIx(	(
<g/>
na	na	k7c4	na
příklad	příklad	k1gInSc4	příklad
Babiččina	babiččin	k2eAgFnSc1d1	babiččina
volba	volba	k1gFnSc1	volba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
další	další	k2eAgFnPc4d1	další
aktivity	aktivita	k1gFnPc4	aktivita
patří	patřit	k5eAaImIp3nS	patřit
živé	živý	k2eAgFnSc2d1	živá
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
exhibice	exhibice	k1gFnSc2	exhibice
Sand	Sand	k1gMnSc1	Sand
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
Speed	Speed	k1gMnSc1	Speed
Painting	Painting	k1gInSc1	Painting
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
workshopy	workshop	k1gInPc4	workshop
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
eventech	event	k1gInPc6	event
<g/>
,	,	kIx,	,
výstavách	výstava	k1gFnPc6	výstava
a	a	k8xC	a
společenských	společenský	k2eAgFnPc6d1	společenská
akcích	akce	k1gFnPc6	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
zahajovací	zahajovací	k2eAgInSc1d1	zahajovací
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
pro	pro	k7c4	pro
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
odehrávalo	odehrávat	k5eAaImAgNnS	odehrávat
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kompletní	kompletní	k2eAgFnSc4d1	kompletní
produkci	produkce	k1gFnSc4	produkce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
návrhu	návrh	k1gInSc2	návrh
vzhledu	vzhled	k1gInSc2	vzhled
maskotů	maskot	k1gInPc2	maskot
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
,	,	kIx,	,
kterými	který	k3yRgMnPc7	který
byli	být	k5eAaImAgMnP	být
Bob	Bob	k1gMnSc1	Bob
&	&	k?	&
Bobek	Bobek	k1gMnSc1	Bobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Alex	Alex	k1gMnSc1	Alex
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vzhled	vzhled	k1gInSc4	vzhled
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
Poštu	pošta	k1gFnSc4	pošta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
byl	být	k5eAaImAgMnS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
do	do	k7c2	do
Španělského	španělský	k2eAgInSc2d1	španělský
televizního	televizní	k2eAgInSc2d1	televizní
pořadu	pořad	k1gInSc2	pořad
El	Ela	k1gFnPc2	Ela
hormiguero	hormiguero	k1gNnSc1	hormiguero
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Antonio	Antonio	k1gMnSc1	Antonio
Banderas	Banderas	k1gMnSc1	Banderas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
premiéry	premiéra	k1gFnSc2	premiéra
nového	nový	k2eAgInSc2d1	nový
filmu	film	k1gInSc2	film
režiséra	režisér	k1gMnSc2	režisér
Pedro	Pedro	k1gNnSc1	Pedro
Almodóvar	Almodóvar	k1gInSc1	Almodóvar
Alex	Alex	k1gMnSc1	Alex
maloval	malovat	k5eAaImAgMnS	malovat
motivy	motiv	k1gInPc4	motiv
z	z	k7c2	z
filmu	film	k1gInSc2	film
v	v	k7c6	v
živém	živý	k2eAgNnSc6d1	živé
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgMnS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
také	také	k6eAd1	také
do	do	k7c2	do
talentové	talentový	k2eAgFnSc2d1	talentová
soutěže	soutěž	k1gFnSc2	soutěž
America	America	k1gFnSc1	America
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
got	got	k?	got
Talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
malování	malování	k1gNnPc2	malování
světlem	světlo	k1gNnSc7	světlo
tedy	tedy	k8xC	tedy
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prvních	první	k4xOgFnPc2	první
Auditions	Auditionsa	k1gFnPc2	Auditionsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letos	letos	k6eAd1	letos
Alex	Alex	k1gMnSc1	Alex
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
týmem	tým	k1gInSc7	tým
připravuje	připravovat	k5eAaImIp3nS	připravovat
celovečerní	celovečerní	k2eAgFnSc7d1	celovečerní
výtvarnou	výtvarný	k2eAgFnSc7d1	výtvarná
show	show	k1gFnSc7	show
Art	Art	k1gFnSc2	Art
of	of	k?	of
Evolution	Evolution	k1gInSc1	Evolution
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
