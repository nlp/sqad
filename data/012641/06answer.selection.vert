<s>
Podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gFnSc2	jeho
rané	raný	k2eAgFnSc2d1	raná
tvorby	tvorba	k1gFnSc2	tvorba
tvoří	tvořit	k5eAaImIp3nP	tvořit
velké	velký	k2eAgFnPc1d1	velká
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
a	a	k8xC	a
venkovních	venkovní	k2eAgInPc2d1	venkovní
designů	design	k1gInPc2	design
<g/>
,	,	kIx,	,
vycházejících	vycházející	k2eAgFnPc2d1	vycházející
zejména	zejména	k9	zejména
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
umění	umění	k1gNnSc2	umění
graffity	graffita	k1gFnSc2	graffita
a	a	k8xC	a
street	streeta	k1gFnPc2	streeta
art	art	k?	art
<g/>
.	.	kIx.	.
</s>
