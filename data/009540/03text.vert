<p>
<s>
Michigan-Huron	Michigan-Huron	k1gInSc1	Michigan-Huron
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Lake	Lake	k1gInSc1	Lake
Michigan-Huron	Michigan-Huron	k1gInSc1	Michigan-Huron
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
provincie	provincie	k1gFnSc2	provincie
Ontario	Ontario	k1gNnSc4	Ontario
<g/>
)	)	kIx)	)
a	a	k8xC	a
USA	USA	kA	USA
(	(	kIx(	(
<g/>
státy	stát	k1gInPc1	stát
Wisconsin	Wisconsina	k1gFnPc2	Wisconsina
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc1	Illinois
<g/>
,	,	kIx,	,
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
bývá	bývat	k5eAaImIp3nS	bývat
uváděno	uvádět	k5eAaImNgNnS	uvádět
jako	jako	k9	jako
dvě	dva	k4xCgNnPc1	dva
jezera	jezero	k1gNnPc1	jezero
(	(	kIx(	(
<g/>
Michiganské	michiganský	k2eAgFnPc1d1	Michiganská
a	a	k8xC	a
Huronské	Huronský	k2eAgFnPc1d1	Huronský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
spojená	spojený	k2eAgNnPc1d1	spojené
průlivem	průliv	k1gInSc7	průliv
Mackinac	Mackinac	k1gInSc1	Mackinac
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hydrologického	hydrologický	k2eAgNnSc2d1	hydrologické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jezero	jezero	k1gNnSc4	jezero
jedno	jeden	k4xCgNnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
spojená	spojený	k2eAgNnPc4d1	spojené
průlivem	průliv	k1gInSc7	průliv
širokým	široký	k2eAgInSc7d1	široký
8	[number]	k4	8
km	km	kA	km
a	a	k8xC	a
hlubokým	hluboký	k2eAgFnPc3d1	hluboká
40	[number]	k4	40
m	m	kA	m
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
proudí	proudit	k5eAaPmIp3nS	proudit
voda	voda	k1gFnSc1	voda
někdy	někdy	k6eAd1	někdy
jedním	jeden	k4xCgInSc7	jeden
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
zase	zase	k9	zase
druhým	druhý	k4xOgInSc7	druhý
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
117	[number]	k4	117
702	[number]	k4	702
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
710	[number]	k4	710
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnSc2d1	maximální
hloubky	hloubka	k1gFnSc2	hloubka
282	[number]	k4	282
m.	m.	k?	m.
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
176	[number]	k4	176
m.	m.	k?	m.
Podle	podle	k7c2	podle
rozlohy	rozloha	k1gFnSc2	rozloha
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
sladkovodní	sladkovodní	k2eAgNnSc4d1	sladkovodní
jezero	jezero	k1gNnSc4	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
objemu	objem	k1gInSc2	objem
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
však	však	k9	však
až	až	k9	až
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
po	po	k7c6	po
Bajkalu	Bajkal	k1gInSc6	Bajkal
<g/>
,	,	kIx,	,
Tanganice	Tanganika	k1gFnSc6	Tanganika
a	a	k8xC	a
Hořejším	hořejší	k2eAgNnSc6d1	hořejší
jezeru	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Michiganské	michiganský	k2eAgNnSc1d1	Michiganské
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
Huronské	Huronský	k2eAgNnSc1d1	Huronské
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
průliv	průliv	k1gInSc1	průliv
Mackinac	Mackinac	k1gInSc1	Mackinac
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Michigan-Huron	Michigan-Huron	k1gInSc1	Michigan-Huron
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Michigan	Michigan	k1gInSc1	Michigan
and	and	k?	and
Huron	Huron	k1gInSc1	Huron
<g/>
:	:	kIx,	:
One	One	k1gFnSc1	One
Lake	Lak	k1gFnSc2	Lak
or	or	k?	or
Two	Two	k1gMnSc1	Two
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Lake	Lake	k1gFnSc1	Lake
Iroquois	Iroquois	k1gFnSc2	Iroquois
</s>
</p>
