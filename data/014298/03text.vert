<s>
Kanton	Kanton	k1gInSc1
Angers-Trélazé	Angers-Trélazá	k1gFnSc2
</s>
<s>
Kanton	Kanton	k1gInSc1
Angers-Trélazé	Angers-Trélazé	k1gNnSc1
Kanton	Kanton	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
arrondissementu	arrondissement	k1gInSc2
Angers	Angers	k1gInSc4
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
Region	region	k1gInSc1
</s>
<s>
Pays	Pays	k1gInSc1
de	de	k?
la	la	k1gNnSc1
Loire	Loir	k1gInSc5
Departement	departement	k1gInSc1
</s>
<s>
Maine-et-Loire	Maine-et-Loir	k1gMnSc5
Arrondissement	Arrondissement	k1gMnSc1
</s>
<s>
Angers	Angers	k6eAd1
Počet	počet	k1gInSc1
obcí	obec	k1gFnPc2
</s>
<s>
5	#num#	k4
Sídlo	sídlo	k1gNnSc4
správy	správa	k1gFnSc2
</s>
<s>
Angers	Angers	k6eAd1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
30	#num#	k4
564	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kanton	Kanton	k1gInSc1
Angers-Trélazé	Angers-Trélazá	k1gFnSc2
(	(	kIx(
<g/>
fr.	fr.	k?
Canton	Canton	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Angers-Trélazé	Angers-Trélazá	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzský	francouzský	k2eAgInSc4d1
kanton	kanton	k1gInSc4
v	v	k7c6
departementu	departement	k1gInSc6
Maine-et-Loire	Maine-et-Loir	k1gInSc5
v	v	k7c6
regionu	region	k1gInSc2
Pays	Paysa	k1gFnPc2
de	de	k?
la	la	k1gNnSc4
Loire	Loir	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gInSc4
pět	pět	k4xCc4
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
kantonu	kanton	k1gInSc2
</s>
<s>
Andard	Andard	k1gMnSc1
</s>
<s>
Angers	Angers	k1gInSc1
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Brain-sur-l	Brain-sur-l	k1gInSc1
<g/>
'	'	kIx"
<g/>
Authion	Authion	k1gInSc1
</s>
<s>
Sarrigné	Sarrigné	k2eAgFnSc1d1
</s>
<s>
Trélazé	Trélazé	k6eAd1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kantony	Kanton	k1gInPc1
v	v	k7c6
departementu	departement	k1gInSc6
Maine-et-Loire	Maine-et-Loir	k1gInSc5
</s>
<s>
Allonnes	Allonnes	k1gInSc1
•	•	k?
Angers-Centre	Angers-Centr	k1gInSc5
•	•	k?
Angers-Est	Angers-Est	k1gMnSc1
•	•	k?
Angers-Nord	Angers-Nord	k1gMnSc1
•	•	k?
Angers-Nord-Est	Angers-Nord-Est	k1gMnSc1
•	•	k?
Angers-Nord-Ouest	Angers-Nord-Ouest	k1gMnSc1
•	•	k?
Angers-Ouest	Angers-Ouest	k1gMnSc1
•	•	k?
Angers-Sud	Angers-Sud	k1gMnSc1
•	•	k?
Angers-Trélazé	Angers-Trélazá	k1gFnSc2
•	•	k?
Baugé	Baugá	k1gFnSc2
•	•	k?
Beaufort-en-Vallée	Beaufort-en-Vallée	k1gInSc1
•	•	k?
Beaupréau	Beaupréaus	k1gInSc2
•	•	k?
Candé	Candý	k2eAgNnSc1d1
•	•	k?
Chalonnes-sur-Loire	Chalonnes-sur-Loir	k1gInSc5
•	•	k?
Champtoceaux	Champtoceaux	k1gInSc1
•	•	k?
Châteauneuf-sur-Sarthe	Châteauneuf-sur-Sarth	k1gFnSc2
•	•	k?
Chemillé	Chemillý	k2eAgFnSc2d1
•	•	k?
Cholet-	Cholet-	k1gFnSc2
<g/>
1	#num#	k4
•	•	k?
Cholet-	Cholet-	k1gFnPc2
<g/>
2	#num#	k4
•	•	k?
Cholet-	Cholet-	k1gFnPc2
<g/>
3	#num#	k4
•	•	k?
Doué-la-Fontaine	Doué-la-Fontain	k1gInSc5
•	•	k?
Durtal	Durtal	k1gMnSc1
•	•	k?
Gennes	Gennes	k1gMnSc1
•	•	k?
Le	Le	k1gMnSc1
Lion-d	Lion-d	k1gMnSc1
<g/>
'	'	kIx"
<g/>
Angers	Angers	k1gInSc1
•	•	k?
Longué-Jumelles	Longué-Jumelles	k1gInSc1
•	•	k?
Le	Le	k1gFnSc1
Louroux-Béconnais	Louroux-Béconnais	k1gFnSc2
•	•	k?
Montfaucon-Montigné	Montfaucon-Montigná	k1gFnSc2
•	•	k?
Montreuil-Bellay	Montreuil-Bellaa	k1gFnSc2
•	•	k?
Montrevault	Montrevault	k1gMnSc1
•	•	k?
Noyant	Noyant	k1gMnSc1
•	•	k?
Les	les	k1gInSc1
Ponts-de-Cé	Ponts-de-Cá	k1gFnSc2
•	•	k?
Pouancé	Pouancá	k1gFnSc2
•	•	k?
Saint-Florent-le-Vieil	Saint-Florent-le-Vieil	k1gMnSc1
•	•	k?
Saint-Georges-sur-Loire	Saint-Georges-sur-Loir	k1gInSc5
•	•	k?
Saumur-Nord	Saumur-Nord	k1gMnSc1
•	•	k?
Saumur-Sud	Saumur-Sudo	k1gNnPc2
•	•	k?
Segré	Segrý	k2eAgFnSc2d1
•	•	k?
Seiches-sur-le-Loir	Seiches-sur-le-Loir	k1gMnSc1
•	•	k?
Thouarcé	Thouarcá	k1gFnSc2
•	•	k?
Tiercé	Tiercá	k1gFnSc2
•	•	k?
Vihiers	Vihiers	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
