<p>
<s>
Slepýš	slepýš	k1gMnSc1	slepýš
křehký	křehký	k2eAgMnSc1d1	křehký
(	(	kIx(	(
<g/>
Anguis	Anguis	k1gInSc1	Anguis
fragilis	fragilis	k1gInSc1	fragilis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
představitel	představitel	k1gMnSc1	představitel
beznohých	beznohý	k2eAgFnPc2d1	beznohá
ještěrek	ještěrka	k1gFnPc2	ještěrka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
stavy	stav	k1gInPc4	stav
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgInP	být
rozeznávány	rozeznáván	k2eAgInPc1d1	rozeznáván
dva	dva	k4xCgInPc1	dva
poddruhy	poddruh	k1gInPc1	poddruh
slepýše	slepýš	k1gMnSc2	slepýš
křehkého	křehký	k2eAgMnSc2d1	křehký
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Anguis	Anguis	k1gInSc1	Anguis
fragilis	fragilis	k1gFnSc2	fragilis
fragilis	fragilis	k1gFnSc2	fragilis
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
Anguis	Anguis	k1gInSc1	Anguis
fragilis	fragilis	k1gFnSc2	fragilis
colchicus	colchicus	k1gMnSc1	colchicus
Nordmann	Nordmann	k1gMnSc1	Nordmann
<g/>
,	,	kIx,	,
1840	[number]	k4	1840
<g/>
;	;	kIx,	;
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
a	a	k8xC	a
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
a	a	k8xC	a
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
je	být	k5eAaImIp3nS	být
i	i	k9	i
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
velice	velice	k6eAd1	velice
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
poddruh	poddruh	k1gInSc1	poddruh
Anguis	Anguis	k1gFnSc2	Anguis
fragilis	fragilis	k1gFnSc2	fragilis
colchicus	colchicus	k1gInSc1	colchicus
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
Anguis	Anguis	k1gFnPc2	Anguis
colchica	colchica	k1gMnSc1	colchica
(	(	kIx(	(
<g/>
Nordmann	Nordmann	k1gMnSc1	Nordmann
<g/>
,	,	kIx,	,
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
–	–	k?	–
slepýš	slepýš	k1gMnSc1	slepýš
východní	východní	k2eAgMnSc1d1	východní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
od	od	k7c2	od
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
až	až	k9	až
po	po	k7c4	po
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
Ural	Ural	k1gInSc4	Ural
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
některých	některý	k3yIgInPc6	některý
středomořských	středomořský	k2eAgInPc6d1	středomořský
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
chybí	chybět	k5eAaImIp3nS	chybět
též	též	k9	též
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
Laponsku	Laponsko	k1gNnSc6	Laponsko
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
severovýchodním	severovýchodní	k2eAgNnSc6d1	severovýchodní
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slepýš	slepýš	k1gMnSc1	slepýš
křehký	křehký	k2eAgMnSc1d1	křehký
žije	žít	k5eAaImIp3nS	žít
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
až	až	k9	až
po	po	k7c4	po
víc	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
populace	populace	k1gFnSc1	populace
však	však	k9	však
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
400	[number]	k4	400
do	do	k7c2	do
700	[number]	k4	700
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
obvykle	obvykle	k6eAd1	obvykle
délky	délka	k1gFnPc1	délka
30	[number]	k4	30
cm	cm	kA	cm
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
do	do	k7c2	do
45	[number]	k4	45
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
malou	malý	k2eAgFnSc4d1	malá
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tupě	tupě	k6eAd1	tupě
zakončená	zakončený	k2eAgFnSc1d1	zakončená
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
trupu	trup	k1gInSc2	trup
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
neoddělená	oddělený	k2eNgFnSc1d1	neoddělená
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
hřbetu	hřbet	k1gInSc2	hřbet
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
dva	dva	k4xCgInPc1	dva
pruhy	pruh	k1gInPc1	pruh
větších	veliký	k2eAgFnPc2d2	veliký
šupin	šupina	k1gFnPc2	šupina
<g/>
.	.	kIx.	.
</s>
<s>
Zbarven	zbarven	k2eAgMnSc1d1	zbarven
je	být	k5eAaImIp3nS	být
nenápadně	nápadně	k6eNd1	nápadně
hnědě	hnědě	k6eAd1	hnědě
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
třemi	tři	k4xCgInPc7	tři
tmavými	tmavý	k2eAgInPc7d1	tmavý
pruhy	pruh	k1gInPc7	pruh
a	a	k8xC	a
modročerným	modročerný	k2eAgInSc7d1	modročerný
břichem	břich	k1gInSc7	břich
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
mívají	mívat	k5eAaImIp3nP	mívat
břicho	břicho	k1gNnSc4	břicho
spíše	spíše	k9	spíše
břidlicově	břidlicově	k6eAd1	břidlicově
šedé	šedý	k2eAgInPc1d1	šedý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	s	k7c7	s
žlutavou	žlutavý	k2eAgFnSc7d1	žlutavá
kresbou	kresba	k1gFnSc7	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Slepýš	slepýš	k1gMnSc1	slepýš
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
svým	svůj	k3xOyFgNnSc7	svůj
zbarvením	zbarvení	k1gNnSc7	zbarvení
i	i	k8xC	i
kresbou	kresba	k1gFnSc7	kresba
značně	značně	k6eAd1	značně
variabilní	variabilní	k2eAgMnPc1d1	variabilní
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
nádech	nádech	k1gInSc4	nádech
spíše	spíše	k9	spíše
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
naopak	naopak	k6eAd1	naopak
přecházejí	přecházet	k5eAaImIp3nP	přecházet
až	až	k9	až
do	do	k7c2	do
červenavého	červenavý	k2eAgInSc2d1	červenavý
odstínu	odstín	k1gInSc2	odstín
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
blankytně	blankytně	k6eAd1	blankytně
modré	modrý	k2eAgFnPc1d1	modrá
skvrny	skvrna	k1gFnPc1	skvrna
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	s	k7c7	s
plazením	plazení	k1gNnSc7	plazení
jako	jako	k9	jako
hadi	had	k1gMnPc1	had
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
mrštný	mrštný	k2eAgInSc1d1	mrštný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stanoviště	stanoviště	k1gNnSc2	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
lesích	les	k1gInPc6	les
na	na	k7c6	na
pasekách	paseka	k1gFnPc6	paseka
<g/>
,	,	kIx,	,
v	v	k7c6	v
křovinatých	křovinatý	k2eAgFnPc6d1	křovinatá
stráních	stráň	k1gFnPc6	stráň
i	i	k8xC	i
na	na	k7c6	na
loukách	louka	k1gFnPc6	louka
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
až	až	k9	až
do	do	k7c2	do
horských	horský	k2eAgFnPc2d1	horská
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Slepýši	slepýš	k1gMnPc1	slepýš
žijí	žít	k5eAaImIp3nP	žít
skrytě	skrytě	k6eAd1	skrytě
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
pod	pod	k7c7	pod
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
v	v	k7c6	v
pařezech	pařez	k1gInPc6	pařez
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
listím	listí	k1gNnSc7	listí
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
vylézají	vylézat	k5eAaImIp3nP	vylézat
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
po	po	k7c6	po
dešti	dešť	k1gInSc6	dešť
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
pátrají	pátrat	k5eAaImIp3nP	pátrat
po	po	k7c6	po
dešťovkách	dešťovka	k1gFnPc6	dešťovka
a	a	k8xC	a
slimácích	slimák	k1gMnPc6	slimák
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
březí	březí	k2eAgFnPc4d1	březí
samičky	samička	k1gFnPc4	samička
lze	lze	k6eAd1	lze
zastihnout	zastihnout	k5eAaPmF	zastihnout
při	při	k7c6	při
slunění	slunění	k1gNnSc6	slunění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etologie	etologie	k1gFnSc2	etologie
==	==	k?	==
</s>
</p>
<p>
<s>
Slepýš	slepýš	k1gMnSc1	slepýš
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
hibernuje	hibernovat	k5eAaBmIp3nS	hibernovat
a	a	k8xC	a
vylézá	vylézat	k5eAaImIp3nS	vylézat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
až	až	k6eAd1	až
počátkem	počátkem	k7c2	počátkem
května	květen	k1gInSc2	květen
(	(	kIx(	(
<g/>
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
podnebí	podnebí	k1gNnSc6	podnebí
a	a	k8xC	a
počasí	počasí	k1gNnSc6	počasí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Slepýš	slepýš	k1gMnSc1	slepýš
křehký	křehký	k2eAgMnSc1d1	křehký
je	být	k5eAaImIp3nS	být
věrný	věrný	k2eAgMnSc1d1	věrný
svému	svůj	k3xOyFgNnSc3	svůj
stanovišti	stanoviště	k1gNnSc3	stanoviště
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ho	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
na	na	k7c6	na
témže	týž	k3xTgNnSc6	týž
místě	místo	k1gNnSc6	místo
potkávat	potkávat	k5eAaImF	potkávat
i	i	k9	i
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
však	však	k9	však
žije	žít	k5eAaImIp3nS	žít
skrytě	skrytě	k6eAd1	skrytě
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
i	i	k9	i
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
pomalému	pomalý	k2eAgInSc3d1	pomalý
pohybu	pohyb	k1gInSc3	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
vlhčí	vlhký	k2eAgNnPc4d2	vlhčí
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
se	se	k3xPyFc4	se
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
často	často	k6eAd1	často
do	do	k7c2	do
kompostů	kompost	k1gInPc2	kompost
na	na	k7c6	na
zahrádkách	zahrádka	k1gFnPc6	zahrádka
<g/>
,	,	kIx,	,
přebývá	přebývat	k5eAaImIp3nS	přebývat
ve	v	k7c6	v
vyhnilých	vyhnilý	k2eAgInPc6d1	vyhnilý
pařezech	pařez	k1gInPc6	pařez
nebo	nebo	k8xC	nebo
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
,	,	kIx,	,
skalních	skalní	k2eAgFnPc6d1	skalní
škvírách	škvíra	k1gFnPc6	škvíra
<g/>
,	,	kIx,	,
kamenitých	kamenitý	k2eAgFnPc6d1	kamenitá
sutích	suť	k1gFnPc6	suť
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
odpadních	odpadní	k2eAgFnPc6d1	odpadní
skládkách	skládka	k1gFnPc6	skládka
<g/>
.	.	kIx.	.
</s>
<s>
Zimu	zima	k1gFnSc4	zima
přečká	přečkat	k5eAaPmIp3nS	přečkat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
svých	svůj	k3xOyFgInPc2	svůj
druhů	druh	k1gInPc2	druh
případně	případně	k6eAd1	případně
jiných	jiný	k2eAgMnPc2d1	jiný
zástupců	zástupce	k1gMnPc2	zástupce
plazů	plaz	k1gMnPc2	plaz
nebo	nebo	k8xC	nebo
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
hibernuje	hibernovat	k5eAaPmIp3nS	hibernovat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ukládá	ukládat	k5eAaImIp3nS	ukládat
se	se	k3xPyFc4	se
k	k	k7c3	k
zimnímu	zimní	k2eAgInSc3d1	zimní
spánku	spánek	k1gInSc3	spánek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slepýši	slepýš	k1gMnPc1	slepýš
loví	lovit	k5eAaImIp3nP	lovit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgMnPc1d1	aktivní
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
časně	časně	k6eAd1	časně
zrána	zrána	k6eAd1	zrána
a	a	k8xC	a
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
nebo	nebo	k8xC	nebo
po	po	k7c6	po
teplých	teplý	k2eAgInPc6d1	teplý
deštích	dešť	k1gInPc6	dešť
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hledají	hledat	k5eAaImIp3nP	hledat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Narazit	narazit	k5eAaPmF	narazit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
ale	ale	k8xC	ale
i	i	k9	i
přes	přes	k7c4	přes
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
ale	ale	k9	ale
přes	přes	k7c4	přes
den	den	k1gInSc4	den
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
mechu	mech	k1gInSc6	mech
a	a	k8xC	a
pod	pod	k7c7	pod
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Potravou	potrava	k1gFnSc7	potrava
slepýšů	slepýš	k1gMnPc2	slepýš
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
žížaly	žížala	k1gFnSc2	žížala
a	a	k8xC	a
slimáci	slimák	k1gMnPc1	slimák
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
různí	různý	k2eAgMnPc1d1	různý
členovci	členovec	k1gMnPc1	členovec
<g/>
,	,	kIx,	,
larvy	larva	k1gFnPc1	larva
a	a	k8xC	a
červi	červ	k1gMnPc1	červ
přiměřené	přiměřený	k2eAgFnSc2d1	přiměřená
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
slepýš	slepýš	k1gMnSc1	slepýš
může	moct	k5eAaImIp3nS	moct
odlomit	odlomit	k5eAaPmF	odlomit
část	část	k1gFnSc4	část
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sebou	se	k3xPyFc7	se
ještě	ještě	k9	ještě
nějakou	nějaký	k3yIgFnSc4	nějaký
chvíli	chvíle	k1gFnSc4	chvíle
mrská	mrskat	k5eAaImIp3nS	mrskat
<g/>
.	.	kIx.	.
</s>
<s>
Slepýší	Slepýší	k2eAgInSc1d1	Slepýší
ocas	ocas	k1gInSc1	ocas
po	po	k7c6	po
čase	čas	k1gInSc6	čas
částečně	částečně	k6eAd1	částečně
regeneruje	regenerovat	k5eAaBmIp3nS	regenerovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Páření	páření	k1gNnSc1	páření
probíhá	probíhat	k5eAaImIp3nS	probíhat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
ze	z	k7c2	z
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kladení	kladení	k1gNnSc3	kladení
vajec	vejce	k1gNnPc2	vejce
(	(	kIx(	(
<g/>
rození	rození	k1gNnSc1	rození
mláďat	mládě	k1gNnPc2	mládě
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
populací	populace	k1gFnPc2	populace
dojít	dojít	k5eAaPmF	dojít
až	až	k9	až
počátkem	počátkem	k7c2	počátkem
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
tzv.	tzv.	kA	tzv.
hemipenis	hemipenis	k1gInSc4	hemipenis
a	a	k8xC	a
při	při	k7c6	při
kopulaci	kopulace	k1gFnSc6	kopulace
si	se	k3xPyFc3	se
samičku	samička	k1gFnSc4	samička
přidržují	přidržovat	k5eAaImIp3nP	přidržovat
kousáním	kousání	k1gNnSc7	kousání
do	do	k7c2	do
šíje	šíj	k1gFnSc2	šíj
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
až	až	k9	až
15	[number]	k4	15
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Slepýš	slepýš	k1gMnSc1	slepýš
křehký	křehký	k2eAgMnSc1d1	křehký
je	být	k5eAaImIp3nS	být
vejcoživorodý	vejcoživorodý	k2eAgInSc1d1	vejcoživorodý
<g/>
,	,	kIx,	,
vaječný	vaječný	k2eAgInSc1d1	vaječný
vývoj	vývoj	k1gInSc1	vývoj
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
samice	samice	k1gFnSc2	samice
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
klubou	klubat	k5eAaImIp3nP	klubat
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
vejce	vejce	k1gNnSc1	vejce
kloakou	kloaka	k1gFnSc7	kloaka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Slepýš	slepýš	k1gMnSc1	slepýš
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
neškodná	škodný	k2eNgFnSc1d1	neškodná
ještěrka	ještěrka	k1gFnSc1	ještěrka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
neznalými	znalý	k2eNgInPc7d1	neznalý
zaměňována	zaměňován	k2eAgFnSc1d1	zaměňována
s	s	k7c7	s
hadem	had	k1gMnSc7	had
a	a	k8xC	a
preventivně	preventivně	k6eAd1	preventivně
likvidována	likvidován	k2eAgFnSc1d1	likvidována
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
nesmyslné	smyslný	k2eNgNnSc1d1	nesmyslné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nezákonné	zákonný	k2eNgInPc1d1	nezákonný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
chráněn	chráněn	k2eAgMnSc1d1	chráněn
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
svých	svůj	k3xOyFgNnPc6	svůj
vývojových	vývojový	k2eAgNnPc6d1	vývojové
stádiích	stádium	k1gNnPc6	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Chráněna	chráněn	k2eAgFnSc1d1	chráněna
jsou	být	k5eAaImIp3nP	být
jím	on	k3xPp3gMnSc7	on
užívaná	užívaný	k2eAgNnPc4d1	užívané
přirozená	přirozený	k2eAgNnPc4d1	přirozené
i	i	k8xC	i
umělá	umělý	k2eAgNnPc4d1	umělé
sídla	sídlo	k1gNnPc4	sídlo
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
biotop	biotop	k1gInSc4	biotop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MORAVEC	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Plazi	plaz	k1gMnPc1	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
531	[number]	k4	531
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2416	[number]	k4	2416
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Anguis	Anguis	k1gFnSc2	Anguis
fragilis	fragilis	k1gFnSc2	fragilis
–	–	k?	–
slepýš	slepýš	k1gMnSc1	slepýš
křehký	křehký	k2eAgMnSc1d1	křehký
<g/>
,	,	kIx,	,
s.	s.	k?	s.
237	[number]	k4	237
<g/>
–	–	k?	–
<g/>
261	[number]	k4	261
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světem	svět	k1gInSc7	svět
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
<g/>
,	,	kIx,	,
obojživelníci	obojživelník	k1gMnPc1	obojživelník
a	a	k8xC	a
plazi	plaz	k1gMnPc1	plaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
slepýš	slepýš	k1gMnSc1	slepýš
křehký	křehký	k2eAgMnSc1d1	křehký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slepýš	slepýš	k1gMnSc1	slepýš
křehký	křehký	k2eAgMnSc1d1	křehký
<g/>
,	,	kIx,	,
stručný	stručný	k2eAgInSc1d1	stručný
článek	článek	k1gInSc1	článek
</s>
</p>
<p>
<s>
Slepýš	slepýš	k1gMnSc1	slepýš
křehký	křehký	k2eAgMnSc1d1	křehký
</s>
</p>
