<s>
Zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Vipera	Vipera	k1gFnSc1	Vipera
berus	berus	k1gMnSc1	berus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
had	had	k1gMnSc1	had
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
zmijovitých	zmijovitý	k2eAgMnPc2d1	zmijovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejrozšířenějšího	rozšířený	k2eAgMnSc4d3	nejrozšířenější
hada	had	k1gMnSc4	had
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
nejseverněji	severně	k6eAd3	severně
žijícím	žijící	k2eAgMnSc7d1	žijící
hadem	had	k1gMnSc7	had
vyznačujícím	vyznačující	k2eAgMnSc7d1	vyznačující
se	se	k3xPyFc4	se
extrémní	extrémní	k2eAgFnSc7d1	extrémní
odolností	odolnost	k1gFnSc7	odolnost
vůči	vůči	k7c3	vůči
chladnému	chladný	k2eAgNnSc3d1	chladné
počasí	počasí	k1gNnSc3	počasí
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
jedovatým	jedovatý	k2eAgMnSc7d1	jedovatý
hadem	had	k1gMnSc7	had
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c4	mezi
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
účinný	účinný	k2eAgInSc4d1	účinný
na	na	k7c4	na
malé	malý	k2eAgMnPc4d1	malý
hlodavce	hlodavec	k1gMnPc4	hlodavec
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
má	mít	k5eAaImIp3nS	mít
ho	on	k3xPp3gMnSc4	on
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
její	její	k3xOp3gNnSc4	její
uštknutí	uštknutí	k1gNnSc4	uštknutí
život	život	k1gInSc1	život
člověka	člověk	k1gMnSc2	člověk
neohrozí	ohrozit	k5eNaPmIp3nS	ohrozit
–	–	k?	–
riziko	riziko	k1gNnSc1	riziko
představuje	představovat	k5eAaImIp3nS	představovat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
alergické	alergický	k2eAgMnPc4d1	alergický
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc1d1	malá
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
staré	starý	k2eAgMnPc4d1	starý
a	a	k8xC	a
nemocné	nemocný	k2eAgMnPc4d1	nemocný
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
jedu	jed	k1gInSc2	jed
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
miligramů	miligram	k1gInPc2	miligram
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
ho	on	k3xPp3gMnSc4	on
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
okolo	okolo	k7c2	okolo
10	[number]	k4	10
miligramů	miligram	k1gInPc2	miligram
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
při	při	k7c6	při
jednom	jeden	k4xCgNnSc6	jeden
uštknutí	uštknutí	k1gNnSc6	uštknutí
neuvolní	uvolnit	k5eNaPmIp3nS	uvolnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
jedu	jed	k1gInSc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečnější	bezpečný	k2eNgInSc1d2	nebezpečnější
je	být	k5eAaImIp3nS	být
poddruh	poddruh	k1gInSc1	poddruh
zmije	zmije	k1gFnSc2	zmije
obecná	obecná	k1gFnSc1	obecná
bosenská	bosenský	k2eAgFnSc1d1	bosenská
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
atypické	atypický	k2eAgNnSc4d1	atypické
složení	složení	k1gNnSc4	složení
jedu	jed	k1gInSc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
různých	různý	k2eAgNnPc2d1	různé
pojetí	pojetí	k1gNnSc2	pojetí
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
nabývat	nabývat	k5eAaImF	nabývat
celé	celý	k2eAgFnPc1d1	celá
řady	řada	k1gFnPc1	řada
barevných	barevný	k2eAgFnPc2d1	barevná
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
černá	černý	k2eAgFnSc1d1	černá
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
dokonce	dokonce	k9	dokonce
postrádat	postrádat	k5eAaImF	postrádat
jinak	jinak	k6eAd1	jinak
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
klikatou	klikatý	k2eAgFnSc4d1	klikatá
tmavou	tmavý	k2eAgFnSc4d1	tmavá
čáru	čára	k1gFnSc4	čára
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
řadu	řada	k1gFnSc4	řada
národních	národní	k2eAgFnPc2d1	národní
pojmenování	pojmenování	k1gNnPc2	pojmenování
zmije	zmije	k1gFnSc2	zmije
obecné	obecná	k1gFnSc2	obecná
či	či	k8xC	či
celého	celý	k2eAgInSc2d1	celý
jejího	její	k3xOp3gInSc2	její
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vretenica	vretenica	k6eAd1	vretenica
obyčajná	obyčajný	k2eAgFnSc1d1	obyčajná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
druhu	druh	k1gInSc2	druh
zmije	zmije	k1gFnSc2	zmije
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zmiji	zmije	k1gFnSc4	zmije
obecnou	obecná	k1gFnSc4	obecná
(	(	kIx(	(
<g/>
Vipera	Vipera	k1gFnSc1	Vipera
berus	berus	k1gInSc1	berus
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
berus	berus	k1gMnSc1	berus
<g/>
)	)	kIx)	)
a	a	k8xC	a
zmiji	zmije	k1gFnSc4	zmije
obecnou	obecný	k2eAgFnSc4d1	obecná
bosenskou	bosenský	k2eAgFnSc4d1	bosenská
(	(	kIx(	(
<g/>
Vipera	Viper	k1gMnSc2	Viper
berus	berus	k1gInSc1	berus
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
bosniensis	bosniensis	k1gFnSc2	bosniensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
starší	starý	k2eAgFnSc1d2	starší
literatura	literatura	k1gFnSc1	literatura
uvádí	uvádět	k5eAaImIp3nS	uvádět
ještě	ještě	k6eAd1	ještě
jako	jako	k9	jako
třetí	třetí	k4xOgInSc1	třetí
poddruh	poddruh	k1gInSc1	poddruh
zmiji	zmije	k1gFnSc4	zmije
obecnou	obecný	k2eAgFnSc7d1	obecná
sachalinskou	sachalinský	k2eAgFnSc7d1	sachalinská
(	(	kIx(	(
<g/>
Vipera	Viper	k1gMnSc2	Viper
berus	berus	k1gInSc1	berus
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
sachalinensis	sachalinensis	k1gFnSc1	sachalinensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
aktuálně	aktuálně	k6eAd1	aktuálně
brána	brát	k5eAaImNgFnS	brát
již	již	k6eAd1	již
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vipera	Vipera	k1gFnSc1	Vipera
berus	berus	k1gInSc1	berus
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
bosniensis	bosniensis	k1gFnSc1	bosniensis
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nominotypického	nominotypický	k2eAgInSc2d1	nominotypický
poddruhu	poddruh	k1gInSc2	poddruh
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
neurotoxické	urotoxický	k2eNgFnSc2d1	urotoxický
složky	složka	k1gFnSc2	složka
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
jedu	jed	k1gInSc6	jed
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
uštknutí	uštknutí	k1gNnSc1	uštknutí
nebezpečnější	bezpečný	k2eNgNnSc1d2	nebezpečnější
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poddruh	poddruh	k1gInSc1	poddruh
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
dělil	dělit	k5eAaImAgInS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
formy	forma	k1gFnPc4	forma
<g/>
:	:	kIx,	:
horskou	horský	k2eAgFnSc4d1	horská
a	a	k8xC	a
nížinnou	nížinný	k2eAgFnSc4d1	nížinná
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
samci	samec	k1gMnPc1	samec
horského	horský	k2eAgInSc2d1	horský
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
zbarvení	zbarvení	k1gNnSc4	zbarvení
spíše	spíše	k9	spíše
do	do	k7c2	do
šeda	šedo	k1gNnSc2	šedo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samci	samec	k1gMnPc1	samec
nížinného	nížinný	k2eAgInSc2d1	nížinný
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
hnědí	hnědý	k2eAgMnPc1d1	hnědý
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejrozšířenějším	rozšířený	k2eAgMnSc7d3	nejrozšířenější
hadem	had	k1gMnSc7	had
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
areál	areál	k1gInSc1	areál
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
Anglie	Anglie	k1gFnSc2	Anglie
až	až	k9	až
po	po	k7c4	po
Sachalin	Sachalin	k1gInSc4	Sachalin
<g/>
,	,	kIx,	,
od	od	k7c2	od
Středomoří	středomoří	k1gNnSc2	středomoří
až	až	k9	až
za	za	k7c4	za
severní	severní	k2eAgInSc4d1	severní
polární	polární	k2eAgInSc4d1	polární
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejjedovatějším	jedovatý	k2eAgMnSc7d3	nejjedovatější
hadem	had	k1gMnSc7	had
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytujícím	vyskytující	k2eAgInPc3d1	vyskytující
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přísně	přísně	k6eAd1	přísně
chráněna	chránit	k5eAaImNgFnS	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dostatek	dostatek	k1gInSc4	dostatek
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
a	a	k8xC	a
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
stanovištěm	stanoviště	k1gNnSc7	stanoviště
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
lesostepní	lesostepní	k2eAgFnPc1d1	lesostepní
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
prosluněné	prosluněný	k2eAgFnPc1d1	prosluněná
horské	horský	k2eAgFnPc1d1	horská
stráně	stráň	k1gFnPc1	stráň
<g/>
,	,	kIx,	,
rašeliniště	rašeliniště	k1gNnPc1	rašeliniště
nebo	nebo	k8xC	nebo
mokřady	mokřad	k1gInPc1	mokřad
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nočního	noční	k2eAgMnSc4d1	noční
tvora	tvor	k1gMnSc4	tvor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
den	den	k1gInSc4	den
ukryt	ukryt	k2eAgInSc4d1	ukryt
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vyhřívá	vyhřívat	k5eAaImIp3nS	vyhřívat
na	na	k7c6	na
sluníčku	sluníčko	k1gNnSc6	sluníčko
a	a	k8xC	a
potravu	potrava	k1gFnSc4	potrava
loví	lovit	k5eAaImIp3nP	lovit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
horské	horský	k2eAgFnPc1d1	horská
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
loví	lovit	k5eAaImIp3nP	lovit
převážně	převážně	k6eAd1	převážně
za	za	k7c2	za
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
bývá	bývat	k5eAaImIp3nS	bývat
příliš	příliš	k6eAd1	příliš
zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
zmije	zmije	k1gFnSc2	zmije
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
<g/>
,	,	kIx,	,
typickým	typický	k2eAgInSc7d1	typický
areálem	areál	k1gInSc7	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
výše	vysoce	k6eAd2	vysoce
položené	položený	k2eAgFnPc1d1	položená
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
nad	nad	k7c4	nad
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
horských	horský	k2eAgFnPc2d1	horská
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgMnSc1d1	malý
had	had	k1gMnSc1	had
<g/>
,	,	kIx,	,
dospělé	dospělý	k2eAgFnPc1d1	dospělá
samice	samice	k1gFnPc1	samice
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
délky	délka	k1gFnPc4	délka
až	až	k9	až
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
cm	cm	kA	cm
<g/>
,	,	kIx,	,
samci	samec	k1gMnSc3	samec
však	však	k9	však
většinou	většinou	k6eAd1	většinou
do	do	k7c2	do
60	[number]	k4	60
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
jedinci	jedinec	k1gMnPc1	jedinec
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
samice	samice	k1gFnPc1	samice
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
měřit	měřit	k5eAaImF	měřit
až	až	k9	až
okolo	okolo	k7c2	okolo
1	[number]	k4	1
m	m	kA	m
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vzácné	vzácný	k2eAgInPc4d1	vzácný
případy	případ	k1gInPc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
200-300	[number]	k4	200-300
g.	g.	k?	g.
Dožít	dožít	k5eAaPmF	dožít
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
20	[number]	k4	20
až	až	k9	až
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejobvyklejší	obvyklý	k2eAgNnSc1d3	nejobvyklejší
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
šedé	šedý	k2eAgNnSc1d1	šedé
až	až	k6eAd1	až
modrošedé	modrošedý	k2eAgNnSc1d1	modrošedé
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
tmavou	tmavý	k2eAgFnSc7d1	tmavá
klikatou	klikatý	k2eAgFnSc7d1	klikatá
čárou	čára	k1gFnSc7	čára
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
víceméně	víceméně	k9	víceméně
vzácných	vzácný	k2eAgFnPc2d1	vzácná
barevných	barevný	k2eAgFnPc2d1	barevná
variant	varianta	k1gFnPc2	varianta
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc4	některý
vůbec	vůbec	k9	vůbec
žádnou	žádný	k3yNgFnSc4	žádný
čáru	čára	k1gFnSc4	čára
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
černou	černá	k1gFnSc7	černá
(	(	kIx(	(
<g/>
morpha	morpha	k1gMnSc1	morpha
prester	prester	k1gMnSc1	prester
<g/>
)	)	kIx)	)
a	a	k8xC	a
červenou	červený	k2eAgFnSc7d1	červená
formou	forma	k1gFnSc7	forma
(	(	kIx(	(
<g/>
morpha	morpha	k1gMnSc1	morpha
chersea	chersea	k1gMnSc1	chersea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
českých	český	k2eAgMnPc2d1	český
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
štěrbinovitá	štěrbinovitý	k2eAgFnSc1d1	štěrbinovitá
zornička	zornička	k1gFnSc1	zornička
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc1	všechen
naše	náš	k3xOp1gFnPc1	náš
užovky	užovka	k1gFnPc1	užovka
mají	mít	k5eAaImIp3nP	mít
zorničky	zornička	k1gFnPc4	zornička
kulaté	kulatý	k2eAgFnPc4d1	kulatá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šupiny	šupina	k1gFnPc1	šupina
zmijí	zmije	k1gFnPc2	zmije
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
kýlnaté	kýlnatý	k2eAgFnPc4d1	kýlnatá
ve	v	k7c6	v
21	[number]	k4	21
řadách	řada	k1gFnPc6	řada
kolem	kolem	k7c2	kolem
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Zmijí	zmijí	k2eAgInSc1d1	zmijí
jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
složením	složení	k1gNnSc7	složení
i	i	k8xC	i
účinností	účinnost	k1gFnSc7	účinnost
podobný	podobný	k2eAgInSc4d1	podobný
chřestýšímu	chřestýší	k2eAgInSc3d1	chřestýší
(	(	kIx(	(
<g/>
zmijovití	zmijovitý	k2eAgMnPc1d1	zmijovitý
a	a	k8xC	a
chřestýšovití	chřestýšovitý	k2eAgMnPc1d1	chřestýšovitý
jsou	být	k5eAaImIp3nP	být
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
podčeledi	podčeleď	k1gFnPc1	podčeleď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
peptidů	peptid	k1gInPc2	peptid
<g/>
,	,	kIx,	,
polypeptidů	polypeptid	k1gInPc2	polypeptid
<g/>
,	,	kIx,	,
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
proteinových	proteinový	k2eAgInPc2d1	proteinový
toxinů	toxin	k1gInPc2	toxin
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc1	účinek
jedu	jed	k1gInSc2	jed
spočívají	spočívat	k5eAaImIp3nP	spočívat
především	především	k9	především
v	v	k7c4	v
narušení	narušení	k1gNnSc4	narušení
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
integrity	integrita	k1gFnSc2	integrita
cévních	cévní	k2eAgFnPc2d1	cévní
stěn	stěna	k1gFnPc2	stěna
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
vyplavováním	vyplavování	k1gNnSc7	vyplavování
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
krevních	krevní	k2eAgInPc2d1	krevní
elementů	element	k1gInPc2	element
mimo	mimo	k7c4	mimo
krevní	krevní	k2eAgNnSc4d1	krevní
řečiště	řečiště	k1gNnSc4	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
Smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
jedu	jet	k5eAaImIp1nS	jet
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Jedový	jedový	k2eAgInSc1d1	jedový
aparát	aparát	k1gInSc1	aparát
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
obvykle	obvykle	k6eAd1	obvykle
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
až	až	k9	až
14	[number]	k4	14
mg	mg	kA	mg
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
39	[number]	k4	39
mg	mg	kA	mg
jedu	jed	k1gInSc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nevypouští	vypouštět	k5eNaImIp3nP	vypouštět
při	při	k7c6	při
jednom	jeden	k4xCgNnSc6	jeden
kousnutí	kousnutí	k1gNnSc6	kousnutí
celou	celý	k2eAgFnSc4d1	celá
zásobu	zásoba	k1gFnSc4	zásoba
svého	svůj	k3xOyFgInSc2	svůj
jedu	jed	k1gInSc2	jed
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
pro	pro	k7c4	pro
velké	velký	k2eAgMnPc4d1	velký
živočichy	živočich	k1gMnPc4	živočich
jako	jako	k8xS	jako
je	on	k3xPp3gFnPc4	on
člověk	člověk	k1gMnSc1	člověk
nepředstavuje	představovat	k5eNaImIp3nS	představovat
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
toxicity	toxicita	k1gFnSc2	toxicita
závažnou	závažný	k2eAgFnSc4d1	závažná
hrozbu	hrozba	k1gFnSc4	hrozba
a	a	k8xC	a
k	k	k7c3	k
úmrtí	úmrtí	k1gNnSc3	úmrtí
lidí	člověk	k1gMnPc2	člověk
dochází	docházet	k5eAaImIp3nS	docházet
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
vzácných	vzácný	k2eAgInPc6d1	vzácný
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Pozor	pozor	k1gInSc1	pozor
je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
dávat	dávat	k5eAaImF	dávat
u	u	k7c2	u
starých	starý	k2eAgMnPc2d1	starý
nemocných	mocný	k2eNgMnPc2d1	nemocný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
zejména	zejména	k9	zejména
na	na	k7c4	na
možnou	možný	k2eAgFnSc4d1	možná
alergickou	alergický	k2eAgFnSc4d1	alergická
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
potravou	potrava	k1gFnSc7	potrava
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
drobní	drobný	k2eAgMnPc1d1	drobný
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
obojživelníci	obojživelník	k1gMnPc1	obojživelník
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc1	ještěrka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
ptačí	ptačí	k2eAgNnPc1d1	ptačí
vejce	vejce	k1gNnPc1	vejce
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
netopýři	netopýr	k1gMnPc1	netopýr
a	a	k8xC	a
malí	malý	k2eAgMnPc1d1	malý
zajíčci	zajíček	k1gMnPc1	zajíček
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
obratlovců	obratlovec	k1gMnPc2	obratlovec
požírá	požírat	k5eAaImIp3nS	požírat
i	i	k9	i
některé	některý	k3yIgMnPc4	některý
bezobratlé	bezobratlí	k1gMnPc4	bezobratlí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
žížaly	žížala	k1gFnPc1	žížala
a	a	k8xC	a
slimáci	slimák	k1gMnPc1	slimák
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumů	výzkum	k1gInPc2	výzkum
provedených	provedený	k2eAgInPc2d1	provedený
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejčastější	častý	k2eAgFnSc4d3	nejčastější
kořist	kořist	k1gFnSc4	kořist
tohoto	tento	k3xDgMnSc2	tento
hada	had	k1gMnSc2	had
norník	norník	k1gMnSc1	norník
rudý	rudý	k1gMnSc1	rudý
(	(	kIx(	(
<g/>
cca	cca	kA	cca
22	[number]	k4	22
až	až	k9	až
28	[number]	k4	28
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následován	následován	k2eAgMnSc1d1	následován
hraboši	hraboš	k1gMnPc1	hraboš
<g/>
,	,	kIx,	,
především	především	k9	především
hrabošem	hraboš	k1gMnSc7	hraboš
polním	polní	k2eAgMnSc7d1	polní
a	a	k8xC	a
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
rejsků	rejsek	k1gInPc2	rejsek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
plazů	plaz	k1gMnPc2	plaz
preferuje	preferovat	k5eAaImIp3nS	preferovat
zmije	zmije	k1gFnSc1	zmije
výrazně	výrazně	k6eAd1	výrazně
ještěrku	ještěrka	k1gFnSc4	ještěrka
živorodou	živorodý	k2eAgFnSc4d1	živorodá
a	a	k8xC	a
skokany	skokan	k1gMnPc7	skokan
rodu	rod	k1gInSc2	rod
Rana	Rana	k1gFnSc1	Rana
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
staří	starý	k2eAgMnPc1d1	starý
cca	cca	kA	cca
1	[number]	k4	1
rok	rok	k1gInSc1	rok
loví	lovit	k5eAaImIp3nP	lovit
nejčastěji	často	k6eAd3	často
ještěrky	ještěrka	k1gFnPc4	ještěrka
(	(	kIx(	(
<g/>
asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
je	být	k5eAaImIp3nS	být
plachý	plachý	k2eAgMnSc1d1	plachý
had	had	k1gMnSc1	had
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
před	před	k7c7	před
člověkem	člověk	k1gMnSc7	člověk
prchá	prchat	k5eAaImIp3nS	prchat
<g/>
.	.	kIx.	.
</s>
<s>
Zaskočena	zaskočen	k2eAgFnSc1d1	zaskočena
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
do	do	k7c2	do
spirály	spirála	k1gFnSc2	spirála
<g/>
,	,	kIx,	,
esovitě	esovitě	k6eAd1	esovitě
stahuje	stahovat	k5eAaImIp3nS	stahovat
přední	přední	k2eAgFnSc4d1	přední
část	část	k1gFnSc4	část
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
hrozí	hrozit	k5eAaImIp3nS	hrozit
útočníkovi	útočník	k1gMnSc3	útočník
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
provádí	provádět	k5eAaImIp3nS	provádět
výpady	výpad	k1gInPc7	výpad
hlavou	hlava	k1gFnSc7	hlava
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
zavřenou	zavřený	k2eAgFnSc7d1	zavřená
tlamou	tlama	k1gFnSc7	tlama
a	a	k8xC	a
bez	bez	k7c2	bez
skutečného	skutečný	k2eAgInSc2d1	skutečný
úmyslu	úmysl	k1gInSc2	úmysl
uštknout	uštknout	k5eAaPmF	uštknout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uštkne	uštknout	k5eAaPmIp3nS	uštknout
však	však	k9	však
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
šlápneme	šlápnout	k5eAaPmIp1nP	šlápnout
<g/>
,	,	kIx,	,
leckdy	leckdy	k6eAd1	leckdy
též	též	k9	též
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
pokoušíme	pokoušet	k5eAaImIp1nP	pokoušet
vzít	vzít	k5eAaPmF	vzít
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgNnSc1d1	zimní
období	období	k1gNnSc1	období
tráví	trávit	k5eAaImIp3nS	trávit
zmije	zmije	k1gFnSc1	zmije
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
hibernace	hibernace	k1gFnSc2	hibernace
30	[number]	k4	30
cm	cm	kA	cm
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
skalních	skalní	k2eAgFnPc2d1	skalní
puklin	puklina	k1gFnPc2	puklina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ukryta	ukryt	k2eAgFnSc1d1	ukryta
ještě	ještě	k9	ještě
hlouběji	hluboko	k6eAd2	hluboko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
vylézají	vylézat	k5eAaImIp3nP	vylézat
(	(	kIx(	(
<g/>
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podmínkách	podmínka	k1gFnPc6	podmínka
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
)	)	kIx)	)
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
soubojů	souboj	k1gInPc2	souboj
mezi	mezi	k7c7	mezi
samci	samec	k1gMnPc7	samec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
hadí	hadí	k2eAgInPc4d1	hadí
tance	tanec	k1gInPc4	tanec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
různém	různý	k2eAgNnSc6d1	různé
strkání	strkání	k1gNnSc6	strkání
a	a	k8xC	a
přetlačování	přetlačování	k1gNnSc6	přetlačování
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
jedových	jedový	k2eAgInPc2d1	jedový
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xS	jako
typický	typický	k2eAgMnSc1d1	typický
vejcoživorodý	vejcoživorodý	k2eAgMnSc1d1	vejcoživorodý
živočich	živočich	k1gMnSc1	živočich
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ale	ale	k8xC	ale
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
případě	případ	k1gInSc6	případ
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přechodný	přechodný	k2eAgInSc4d1	přechodný
stav	stav	k1gInSc4	stav
mezi	mezi	k7c7	mezi
vejcoživorodostí	vejcoživorodost	k1gFnSc7	vejcoživorodost
a	a	k8xC	a
živorodostí	živorodost	k1gFnSc7	živorodost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
vyživovány	vyživován	k2eAgInPc1d1	vyživován
primitivní	primitivní	k2eAgFnSc7d1	primitivní
placentou	placenta	k1gFnSc7	placenta
typu	typ	k1gInSc2	typ
chorio-allantois	choriollantois	k1gFnSc2	chorio-allantois
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
adaptace	adaptace	k1gFnSc1	adaptace
hraje	hrát	k5eAaImIp3nS	hrát
nesmírně	smírně	k6eNd1	smírně
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
mimořádné	mimořádný	k2eAgFnSc6d1	mimořádná
odolnosti	odolnost	k1gFnSc6	odolnost
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
vůči	vůči	k7c3	vůči
chladnému	chladný	k2eAgNnSc3d1	chladné
klimatu	klima	k1gNnSc3	klima
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
samice	samice	k1gFnSc1	samice
porodí	porodit	k5eAaPmIp3nS	porodit
obvykle	obvykle	k6eAd1	obvykle
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
snese	snést	k5eAaPmIp3nS	snést
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
počet	počet	k1gInSc1	počet
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
hned	hned	k6eAd1	hned
klubou	klubat	k5eAaImIp3nP	klubat
(	(	kIx(	(
<g/>
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
děje	dít	k5eAaImIp3nS	dít
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
cm	cm	kA	cm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
na	na	k7c6	na
zimní	zimní	k2eAgFnSc6d1	zimní
hibernaci	hibernace	k1gFnSc6	hibernace
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zmije	zmije	k1gFnSc2	zmije
obecná	obecná	k1gFnSc1	obecná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
zmiji	zmije	k1gFnSc6	zmije
obecné	obecný	k2eAgFnSc6d1	obecná
na	na	k7c6	na
serveru	server	k1gInSc6	server
Teraporadny	Teraporadna	k1gFnSc2	Teraporadna
Zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
Video	video	k1gNnSc4	video
zmije	zmije	k1gFnSc2	zmije
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
přes	přes	k7c4	přes
1	[number]	k4	1
m.	m.	k?	m.
Výška	výška	k1gFnSc1	výška
zídky	zídka	k1gFnSc2	zídka
80	[number]	k4	80
cm	cm	kA	cm
Zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
na	na	k7c6	na
nature	natur	k1gMnSc5	natur
<g/>
.	.	kIx.	.
<g/>
unas	unasit	k5eAaPmRp2nS	unasit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
