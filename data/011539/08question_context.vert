<s>
Věstonická	věstonický	k2eAgFnSc1d1	Věstonická
venuše	venuše	k1gFnSc1	venuše
je	být	k5eAaImIp3nS	být
keramická	keramický	k2eAgFnSc1d1	keramická
soška	soška	k1gFnSc1	soška
nahé	nahý	k2eAgFnSc2d1	nahá
ženy	žena	k1gFnSc2	žena
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
pálené	pálený	k2eAgFnSc2d1	pálená
hlíny	hlína	k1gFnSc2	hlína
pocházející	pocházející	k2eAgFnSc6d1	pocházející
z	z	k7c2	z
mladého	mladý	k2eAgInSc2d1	mladý
paleolitu	paleolit	k1gInSc2	paleolit
a	a	k8xC	a
datovaná	datovaný	k2eAgFnSc1d1	datovaná
do	do	k7c2	do
období	období	k1gNnSc2	období
29	[number]	k4	29
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
figurka	figurka	k1gFnSc1	figurka
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgMnPc7d1	další
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
známá	známý	k2eAgFnSc1d1	známá
keramická	keramický	k2eAgFnSc1d1	keramická
soška	soška	k1gFnSc1	soška
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>

