<p>
<s>
Věstonická	věstonický	k2eAgFnSc1d1	Věstonická
venuše	venuše	k1gFnSc1	venuše
je	být	k5eAaImIp3nS	být
keramická	keramický	k2eAgFnSc1d1	keramická
soška	soška	k1gFnSc1	soška
nahé	nahý	k2eAgFnSc2d1	nahá
ženy	žena	k1gFnSc2	žena
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
pálené	pálený	k2eAgFnSc2d1	pálená
hlíny	hlína	k1gFnSc2	hlína
pocházející	pocházející	k2eAgFnSc6d1	pocházející
z	z	k7c2	z
mladého	mladý	k2eAgInSc2d1	mladý
paleolitu	paleolit	k1gInSc2	paleolit
a	a	k8xC	a
datovaná	datovaný	k2eAgFnSc1d1	datovaná
do	do	k7c2	do
období	období	k1gNnSc2	období
29	[number]	k4	29
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
figurka	figurka	k1gFnSc1	figurka
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgMnPc7d1	další
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
známá	známý	k2eAgFnSc1d1	známá
keramická	keramický	k2eAgFnSc1d1	keramická
soška	soška	k1gFnSc1	soška
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
objev	objev	k1gInSc1	objev
vyvrátil	vyvrátit	k5eAaPmAgInS	vyvrátit
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
keramika	keramika	k1gFnSc1	keramika
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
paleolitu	paleolit	k1gInSc2	paleolit
ještě	ještě	k6eAd1	ještě
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nalezení	nalezení	k1gNnPc4	nalezení
sošky	soška	k1gFnSc2	soška
==	==	k?	==
</s>
</p>
<p>
<s>
Soška	soška	k1gFnSc1	soška
byla	být	k5eAaImAgFnS	být
nalezena	nalezen	k2eAgFnSc1d1	nalezena
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1925	[number]	k4	1925
v	v	k7c6	v
popelišti	popeliště	k1gNnSc6	popeliště
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
pravěkého	pravěký	k2eAgNnSc2d1	pravěké
naleziště	naleziště	k1gNnSc2	naleziště
mezi	mezi	k7c7	mezi
Dolními	dolní	k2eAgFnPc7d1	dolní
Věstonicemi	Věstonice	k1gFnPc7	Věstonice
a	a	k8xC	a
Pavlovem	Pavlov	k1gInSc7	Pavlov
<g/>
.	.	kIx.	.
</s>
<s>
Paleolitický	paleolitický	k2eAgInSc4d1	paleolitický
nález	nález	k1gInSc4	nález
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
tým	tým	k1gInSc1	tým
archeologa	archeolog	k1gMnSc2	archeolog
Karla	Karel	k1gMnSc2	Karel
Absolona	Absolon	k1gMnSc2	Absolon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zde	zde	k6eAd1	zde
prováděl	provádět	k5eAaImAgMnS	provádět
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
letech	let	k1gInPc6	let
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Absolon	Absolon	k1gMnSc1	Absolon
u	u	k7c2	u
samotného	samotný	k2eAgInSc2d1	samotný
nálezu	nález	k1gInSc2	nález
nebyl	být	k5eNaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
za	za	k7c4	za
objevitele	objevitel	k1gMnSc4	objevitel
označován	označován	k2eAgInSc1d1	označován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
nálezu	nález	k1gInSc2	nález
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Sošku	soška	k1gFnSc4	soška
nalezli	naleznout	k5eAaPmAgMnP	naleznout
dělník	dělník	k1gMnSc1	dělník
Josef	Josef	k1gMnSc1	Josef
Seidl	Seidl	k1gMnSc1	Seidl
a	a	k8xC	a
technický	technický	k2eAgMnSc1d1	technický
vedoucí	vedoucí	k1gMnSc1	vedoucí
výzkumů	výzkum	k1gInPc2	výzkum
Emanuel	Emanuel	k1gMnSc1	Emanuel
Dania	Danius	k1gMnSc2	Danius
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zbytcích	zbytek	k1gInPc6	zbytek
pravěkého	pravěký	k2eAgNnSc2d1	pravěké
ohniště	ohniště	k1gNnSc2	ohniště
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
10	[number]	k4	10
m	m	kA	m
ležela	ležet	k5eAaImAgFnS	ležet
soška	soška	k1gFnSc1	soška
společně	společně	k6eAd1	společně
s	s	k7c7	s
kamennými	kamenný	k2eAgInPc7d1	kamenný
nástroji	nástroj	k1gInPc7	nástroj
a	a	k8xC	a
zvířecími	zvířecí	k2eAgFnPc7d1	zvířecí
kostmi	kost	k1gFnPc7	kost
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
rozlomena	rozlomit	k5eAaPmNgFnS	rozlomit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
kusy	kus	k1gInPc4	kus
ležící	ležící	k2eAgInPc4d1	ležící
asi	asi	k9	asi
10	[number]	k4	10
cm	cm	kA	cm
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
nepředpokládalo	předpokládat	k5eNaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
očištění	očištění	k1gNnSc6	očištění
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
celek	celek	k1gInSc1	celek
podobá	podobat	k5eAaImIp3nS	podobat
ženské	ženský	k2eAgFnSc3d1	ženská
postavě	postava	k1gFnSc3	postava
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
venuše	venuše	k1gFnSc2	venuše
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
i	i	k8xC	i
další	další	k2eAgFnPc1d1	další
keramické	keramický	k2eAgFnPc1d1	keramická
plastiky	plastika	k1gFnPc1	plastika
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podoba	podoba	k1gFnSc1	podoba
sošky	soška	k1gFnSc2	soška
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
11,5	[number]	k4	11,5
cm	cm	kA	cm
vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
v	v	k7c6	v
bocích	bok	k1gInPc6	bok
4,3	[number]	k4	4,3
cm	cm	kA	cm
široká	široký	k2eAgFnSc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Použitý	použitý	k2eAgInSc1d1	použitý
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
směsí	směs	k1gFnSc7	směs
hlíny	hlína	k1gFnSc2	hlína
a	a	k8xC	a
vápence	vápenec	k1gInSc2	vápenec
<g/>
,	,	kIx,	,
obdobné	obdobný	k2eAgFnPc1d1	obdobná
drobné	drobný	k2eAgFnPc1d1	drobná
plastiky	plastika	k1gFnPc1	plastika
byly	být	k5eAaImAgFnP	být
přitom	přitom	k6eAd1	přitom
většinou	většinou	k6eAd1	většinou
zhotovovány	zhotovovat	k5eAaImNgInP	zhotovovat
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
nebo	nebo	k8xC	nebo
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
stylizace	stylizace	k1gFnSc1	stylizace
obličeje	obličej	k1gInSc2	obličej
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
oči	oko	k1gNnPc4	oko
jsou	být	k5eAaImIp3nP	být
naznačeny	naznačit	k5eAaPmNgInP	naznačit
krátkými	krátký	k2eAgFnPc7d1	krátká
šikmými	šikmý	k2eAgFnPc7d1	šikmá
rýhami	rýha	k1gFnPc7	rýha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
mohutnost	mohutnost	k1gFnSc1	mohutnost
povislého	povislý	k2eAgNnSc2d1	povislé
poprsí	poprsí	k1gNnSc2	poprsí
(	(	kIx(	(
<g/>
levé	levý	k2eAgNnSc1d1	levé
ňadro	ňadro	k1gNnSc1	ňadro
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgNnSc1d2	veliký
<g/>
)	)	kIx)	)
a	a	k8xC	a
boků	bok	k1gInPc2	bok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
rýhy	rýha	k1gFnPc1	rýha
znázorňující	znázorňující	k2eAgInPc4d1	znázorňující
zřejmě	zřejmě	k6eAd1	zřejmě
tukové	tukový	k2eAgInPc4d1	tukový
zářezy	zářez	k1gInPc4	zářez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zkoumání	zkoumání	k1gNnPc4	zkoumání
sošky	soška	k1gFnSc2	soška
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
soška	soška	k1gFnSc1	soška
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
pomocí	pomocí	k7c2	pomocí
tomografu	tomograf	k1gInSc2	tomograf
ve	v	k7c6	v
Fakultní	fakultní	k2eAgFnSc6d1	fakultní
nemocnici	nemocnice	k1gFnSc6	nemocnice
u	u	k7c2	u
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
badatelé	badatel	k1gMnPc1	badatel
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
zjistit	zjistit	k5eAaPmF	zjistit
složení	složení	k1gNnSc4	složení
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
výsledky	výsledek	k1gInPc1	výsledek
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
plastika	plastika	k1gFnSc1	plastika
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jemné	jemný	k2eAgFnSc2d1	jemná
hlíny	hlína	k1gFnSc2	hlína
smíchané	smíchaný	k2eAgFnSc2d1	smíchaná
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
i	i	k9	i
malá	malý	k2eAgNnPc4d1	malé
bílá	bílý	k2eAgNnPc4d1	bílé
zrníčka	zrníčko	k1gNnPc4	zrníčko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vysrážený	vysrážený	k2eAgInSc4d1	vysrážený
vápenec	vápenec	k1gInSc4	vápenec
nebo	nebo	k8xC	nebo
úlomky	úlomek	k1gInPc4	úlomek
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
předchozích	předchozí	k2eAgMnPc2d1	předchozí
výzkumů	výzkum	k1gInPc2	výzkum
tak	tak	k8xC	tak
jednoznačnou	jednoznačný	k2eAgFnSc4d1	jednoznačná
odpověď	odpověď	k1gFnSc4	odpověď
nedal	dát	k5eNaPmAgMnS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
objeven	objevit	k5eAaPmNgInS	objevit
další	další	k2eAgInSc1d1	další
detail	detail	k1gInSc1	detail
<g/>
:	:	kIx,	:
na	na	k7c6	na
hýždích	hýždě	k1gFnPc6	hýždě
sošky	soška	k1gFnSc2	soška
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
otisk	otisk	k1gInSc1	otisk
prstu	prst	k1gInSc2	prst
asi	asi	k9	asi
desetiletého	desetiletý	k2eAgNnSc2d1	desetileté
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedmnáctého	sedmnáctý	k4xOgInSc2	sedmnáctý
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
Věstonickou	věstonický	k2eAgFnSc4d1	Věstonická
venuši	venuše	k1gFnSc4	venuše
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
mikrotomografem	mikrotomograf	k1gInSc7	mikrotomograf
pracovníci	pracovník	k1gMnPc1	pracovník
Moravského	moravský	k2eAgNnSc2d1	Moravské
zemského	zemský	k2eAgNnSc2d1	zemské
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
technologického	technologický	k2eAgNnSc2d1	Technologické
centra	centrum	k1gNnSc2	centrum
firmy	firma	k1gFnSc2	firma
FEI	FEI	kA	FEI
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
necelých	celý	k2eNgFnPc2d1	necelá
15	[number]	k4	15
hodin	hodina	k1gFnPc2	hodina
skenována	skenovat	k5eAaImNgFnS	skenovat
a	a	k8xC	a
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
vrypy	vryp	k1gInPc1	vryp
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
trojrozměrný	trojrozměrný	k2eAgInSc1d1	trojrozměrný
model	model	k1gInSc1	model
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
několika	několik	k4yIc2	několik
mikrometrů	mikrometr	k1gInPc2	mikrometr
a	a	k8xC	a
pořízeno	pořídit	k5eAaPmNgNnS	pořídit
asi	asi	k9	asi
80	[number]	k4	80
GB	GB	kA	GB
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sošce	soška	k1gFnSc6	soška
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
několik	několik	k4yIc1	několik
neznámých	známý	k2eNgFnPc2d1	neznámá
příměsí	příměs	k1gFnPc2	příměs
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
nohou	noha	k1gFnSc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Keramická	keramický	k2eAgFnSc1d1	keramická
hmota	hmota	k1gFnSc1	hmota
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
homogenní	homogenní	k2eAgNnSc1d1	homogenní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
==	==	k?	==
</s>
</p>
<p>
<s>
Soška	soška	k1gFnSc1	soška
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
Moravském	moravský	k2eAgNnSc6d1	Moravské
zemském	zemský	k2eAgNnSc6d1	zemské
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
ústavu	ústav	k1gInSc2	ústav
Anthropos	Anthroposa	k1gFnPc2	Anthroposa
<g/>
.	.	kIx.	.
</s>
<s>
Vystavována	vystavován	k2eAgFnSc1d1	vystavována
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
ojediněle	ojediněle	k6eAd1	ojediněle
a	a	k8xC	a
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
expozici	expozice	k1gFnSc6	expozice
je	být	k5eAaImIp3nS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
kopií	kopie	k1gFnSc7	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
hodnotu	hodnota	k1gFnSc4	hodnota
stanovili	stanovit	k5eAaPmAgMnP	stanovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
američtí	americký	k2eAgMnPc1d1	americký
starožitníci	starožitník	k1gMnPc1	starožitník
na	na	k7c4	na
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
Věstonická	věstonický	k2eAgFnSc1d1	Věstonická
venuše	venuše	k1gFnSc1	venuše
s	s	k7c7	s
částí	část	k1gFnSc7	část
sbírek	sbírka	k1gFnPc2	sbírka
brněnského	brněnský	k2eAgNnSc2d1	brněnské
muzea	muzeum	k1gNnSc2	muzeum
uschována	uschován	k2eAgFnSc1d1	uschována
v	v	k7c6	v
mikulovském	mikulovský	k2eAgInSc6d1	mikulovský
zámku	zámek	k1gInSc6	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Protektorátní	protektorátní	k2eAgInPc1d1	protektorátní
úřady	úřad	k1gInPc1	úřad
měly	mít	k5eAaImAgFnP	mít
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
poškození	poškození	k1gNnSc2	poškození
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
muzejníci	muzejník	k1gMnPc1	muzejník
nejcennější	cenný	k2eAgFnSc4d3	nejcennější
část	část	k1gFnSc4	část
sbírky	sbírka	k1gFnSc2	sbírka
i	i	k8xC	i
s	s	k7c7	s
venuší	venuše	k1gFnSc7	venuše
převezli	převézt	k5eAaPmAgMnP	převézt
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
fotografování	fotografování	k1gNnSc2	fotografování
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
Mikulově	Mikulov	k1gInSc6	Mikulov
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
během	během	k7c2	během
osvobozovacích	osvobozovací	k2eAgInPc2d1	osvobozovací
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
když	když	k8xS	když
zámek	zámek	k1gInSc1	zámek
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
vyhořel	vyhořet	k5eAaPmAgMnS	vyhořet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
archeologické	archeologický	k2eAgNnSc1d1	Archeologické
naleziště	naleziště	k1gNnSc1	naleziště
Dolní	dolní	k2eAgFnPc1d1	dolní
Věstonice	Věstonice	k1gFnPc1	Věstonice
a	a	k8xC	a
soubor	soubor	k1gInSc1	soubor
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
nálezů	nález	k1gInPc2	nález
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Věstonické	věstonický	k2eAgFnSc2d1	Věstonická
venuše	venuše	k1gFnSc2	venuše
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
nálezy	nález	k1gInPc1	nález
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
lokalitě	lokalita	k1gFnSc6	lokalita
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
sošky	soška	k1gFnPc1	soška
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
pravost	pravost	k1gFnSc1	pravost
je	být	k5eAaImIp3nS	být
však	však	k9	však
zpochybňována	zpochybňován	k2eAgFnSc1d1	zpochybňována
<g/>
.	.	kIx.	.
<g/>
Venuše	Venuše	k1gFnSc1	Venuše
z	z	k7c2	z
Dolních	dolní	k2eAgFnPc2d1	dolní
Věstonic	Věstonice	k1gFnPc2	Věstonice
není	být	k5eNaImIp3nS	být
ojedinělou	ojedinělý	k2eAgFnSc7d1	ojedinělá
ukázkou	ukázka	k1gFnSc7	ukázka
umění	umění	k1gNnSc2	umění
pravěkých	pravěký	k2eAgInPc2d1	pravěký
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
plastiky	plastika	k1gFnPc1	plastika
z	z	k7c2	z
pálené	pálený	k2eAgFnSc2d1	pálená
hlíny	hlína	k1gFnSc2	hlína
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
také	také	k9	také
ze	z	k7c2	z
sousedního	sousední	k2eAgNnSc2d1	sousední
Pavlova	Pavlův	k2eAgNnSc2d1	Pavlovo
a	a	k8xC	a
z	z	k7c2	z
Předmostí	předmostí	k1gNnSc2	předmostí
u	u	k7c2	u
Přerova	Přerov	k1gInSc2	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Proslulá	proslulý	k2eAgFnSc1d1	proslulá
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
Landecká	Landecký	k2eAgFnSc1d1	Landecká
venuše	venuše	k1gFnSc1	venuše
(	(	kIx(	(
<g/>
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
také	také	k9	také
petřkovická	petřkovický	k2eAgFnSc1d1	Petřkovická
<g/>
)	)	kIx)	)
nalezená	nalezený	k2eAgFnSc1d1	nalezená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
na	na	k7c6	na
ostravském	ostravský	k2eAgInSc6d1	ostravský
vrchu	vrch	k1gInSc6	vrch
Landek	Landek	k1gInSc1	Landek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
4,6	[number]	k4	4,6
cm	cm	kA	cm
vysoká	vysoký	k2eAgFnSc1d1	vysoká
soška	soška	k1gFnSc1	soška
z	z	k7c2	z
krevele	krevel	k1gInSc2	krevel
je	být	k5eAaImIp3nS	být
torzem	torzo	k1gNnSc7	torzo
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
výjimečnost	výjimečnost	k1gFnSc1	výjimečnost
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
netypicky	typicky	k6eNd1	typicky
štíhlých	štíhlý	k2eAgFnPc6d1	štíhlá
proporcích	proporce	k1gFnPc6	proporce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
milovníkům	milovník	k1gMnPc3	milovník
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
dokonce	dokonce	k9	dokonce
připomínají	připomínat	k5eAaImIp3nP	připomínat
kubistickou	kubistický	k2eAgFnSc4d1	kubistická
sochu	socha	k1gFnSc4	socha
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdokonalejších	dokonalý	k2eAgFnPc2d3	nejdokonalejší
sošek	soška	k1gFnPc2	soška
paleolitu	paleolit	k1gInSc2	paleolit
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
Věstonické	věstonický	k2eAgFnSc2d1	Věstonická
venuše	venuše	k1gFnSc2	venuše
považována	považován	k2eAgFnSc1d1	považována
i	i	k8xC	i
Willendorfská	Willendorfský	k2eAgFnSc1d1	Willendorfská
venuše	venuše	k1gFnSc1	venuše
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
Lespuque	Lespuque	k1gNnSc6	Lespuque
byla	být	k5eAaImAgFnS	být
zase	zase	k9	zase
nalezena	naleznout	k5eAaPmNgFnS	naleznout
venuše	venuše	k1gFnSc1	venuše
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
klu	kel	k1gInSc2	kel
mamuta	mamut	k1gMnSc2	mamut
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgFnPc1d1	obdobná
figurky	figurka	k1gFnPc1	figurka
starší	starý	k2eAgFnSc2d2	starší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
spatřily	spatřit	k5eAaPmAgFnP	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
i	i	k8xC	i
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrazy	odraz	k1gInPc1	odraz
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
Věstonické	věstonický	k2eAgFnSc2d1	Věstonická
venuše	venuše	k1gFnSc2	venuše
popsal	popsat	k5eAaPmAgMnS	popsat
Eduard	Eduard	k1gMnSc1	Eduard
Štorch	Štorch	k1gMnSc1	Štorch
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
románu	román	k1gInSc6	román
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
Lovci	lovec	k1gMnPc1	lovec
mamutů	mamut	k1gMnPc2	mamut
</s>
</p>
<p>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
pochází	pocházet	k5eAaImIp3nS	pocházet
obraz	obraz	k1gInSc1	obraz
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Buriana	Burian	k1gMnSc2	Burian
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
výrobu	výroba	k1gFnSc4	výroba
Věstonické	věstonický	k2eAgFnSc2d1	Věstonická
venuše	venuše	k1gFnSc2	venuše
pravěkým	pravěký	k2eAgMnSc7d1	pravěký
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
62	[number]	k4	62
<g/>
×	×	k?	×
<g/>
43	[number]	k4	43
cm	cm	kA	cm
je	být	k5eAaImIp3nS	být
malován	malovat	k5eAaImNgInS	malovat
olejem	olej	k1gInSc7	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
použit	použít	k5eAaPmNgMnS	použít
i	i	k9	i
jako	jako	k9	jako
ilustrace	ilustrace	k1gFnSc1	ilustrace
knihy	kniha	k1gFnSc2	kniha
Lovci	lovec	k1gMnPc1	lovec
mamutů	mamut	k1gMnPc2	mamut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
(	(	kIx(	(
<g/>
figurka	figurka	k1gFnSc1	figurka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Petřkovická	petřkovický	k2eAgFnSc1d1	Petřkovická
venuše	venuše	k1gFnSc1	venuše
</s>
</p>
<p>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
z	z	k7c2	z
Hohle	Hohle	k1gFnSc2	Hohle
Fels	Felsa	k1gFnPc2	Felsa
</s>
</p>
<p>
<s>
Willendorfská	Willendorfský	k2eAgFnSc1d1	Willendorfská
venuše	venuše	k1gFnSc1	venuše
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Věstonická	věstonický	k2eAgFnSc1d1	Věstonická
venuše	venuše	k1gFnSc1	venuše
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Věstonická	věstonický	k2eAgFnSc1d1	Věstonická
venuše	venuše	k1gFnSc1	venuše
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
