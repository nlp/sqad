<p>
<s>
La	la	k1gNnSc1	la
Victoria	Victorium	k1gNnSc2	Victorium
de	de	k?	de
Acentejo	Acentejo	k1gMnSc1	Acentejo
je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
31	[number]	k4	31
obcí	obec	k1gFnPc2	obec
na	na	k7c6	na
španělském	španělský	k2eAgInSc6d1	španělský
ostrově	ostrov	k1gInSc6	ostrov
Tenerife	Tenerif	k1gMnSc5	Tenerif
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
9	[number]	k4	9
042	[number]	k4	042
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
celé	celý	k2eAgFnSc2d1	celá
obce	obec	k1gFnSc2	obec
činí	činit	k5eAaImIp3nS	činit
18,36	[number]	k4	18,36
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
obcemi	obec	k1gFnPc7	obec
Santa	Sant	k1gMnSc2	Sant
Úrsula	Úrsul	k1gMnSc2	Úrsul
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Matanza	Matanz	k1gMnSc2	Matanz
de	de	k?	de
Acentejo	Acenteja	k1gMnSc5	Acenteja
<g/>
,	,	kIx,	,
Tacoronte	Tacoront	k1gMnSc5	Tacoront
a	a	k8xC	a
El	Ela	k1gFnPc2	Ela
Sauzal	Sauzal	k1gMnPc1	Sauzal
tvoří	tvořit	k5eAaImIp3nP	tvořit
Comarcu	Comarca	k1gMnSc4	Comarca
de	de	k?	de
Acentejo	Acentejo	k1gMnSc1	Acentejo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
