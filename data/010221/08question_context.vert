<s>
La	la	k1gNnSc1	la
Victoria	Victorium	k1gNnSc2	Victorium
de	de	k?	de
Acentejo	Acentejo	k1gMnSc1	Acentejo
je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
31	[number]	k4	31
obcí	obec	k1gFnPc2	obec
na	na	k7c6	na
španělském	španělský	k2eAgInSc6d1	španělský
ostrově	ostrov	k1gInSc6	ostrov
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
