<s>
Tracheotomie	tracheotomie	k1gFnSc1
je	být	k5eAaImIp3nS
chirurgický	chirurgický	k2eAgInSc1d1
zákrok	zákrok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
otevření	otevření	k1gNnSc3
přístupu	přístup	k1gInSc2
(	(	kIx(
<g/>
vytvoření	vytvoření	k1gNnSc1
otvoru	otvor	k1gInSc2
<g/>
)	)	kIx)
do	do	k7c2
průdušnice	průdušnice	k1gFnSc2
a	a	k8xC
k	k	k7c3
zajištění	zajištění	k1gNnSc3
dýchání	dýchání	k1gNnSc2
<g/>
.	.	kIx.
</s>