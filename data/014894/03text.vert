<s>
Tracheotomie	tracheotomie	k1gFnSc1
</s>
<s>
Tracheotomie	tracheotomie	k1gFnSc1
</s>
<s>
Tracheotomie	tracheotomie	k1gFnSc1
je	být	k5eAaImIp3nS
chirurgický	chirurgický	k2eAgInSc1d1
zákrok	zákrok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
otevření	otevření	k1gNnSc3
přístupu	přístup	k1gInSc2
(	(	kIx(
<g/>
vytvoření	vytvoření	k1gNnSc1
otvoru	otvor	k1gInSc2
<g/>
)	)	kIx)
do	do	k7c2
průdušnice	průdušnice	k1gFnSc2
a	a	k8xC
k	k	k7c3
zajištění	zajištění	k1gNnSc3
dýchání	dýchání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tracheotomie	tracheotomie	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
řeckého	řecký	k2eAgInSc2d1
slovního	slovní	k2eAgInSc2d1
základu	základ	k1gInSc2
tom-	tom-	k?
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
znamená	znamenat	k5eAaImIp3nS
řezat	řezat	k5eAaImF
<g/>
,	,	kIx,
vztahuje	vztahovat	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
akutnímu	akutní	k2eAgNnSc3d1
proříznutí	proříznutí	k1gNnSc3
průdušnice	průdušnice	k1gFnSc2
(	(	kIx(
<g/>
trachea	trachea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
zákrok	zákrok	k1gInSc1
je	být	k5eAaImIp3nS
prováděn	provádět	k5eAaImNgInS
záchranáři	záchranář	k1gMnSc3
<g/>
,	,	kIx,
lékaři	lékař	k1gMnSc3
a	a	k8xC
chirurgy	chirurg	k1gMnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
otvoru	otvor	k1gInSc2
v	v	k7c6
průdušnici	průdušnice	k1gFnSc6
zasune	zasunout	k5eAaPmIp3nS
tracheostomická	tracheostomický	k2eAgFnSc1d1
kanyla	kanyla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
první	první	k4xOgFnSc2
pomoci	pomoc	k1gFnSc2
lze	lze	k6eAd1
tracheotomii	tracheotomie	k1gFnSc4
nahradit	nahradit	k5eAaPmF
vbodnutím	vbodnutí	k1gNnSc7
jedné	jeden	k4xCgFnSc2
či	či	k8xC
více	hodně	k6eAd2
širokých	široký	k2eAgFnPc2d1
jehel	jehla	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
buď	buď	k8xC
v	v	k7c6
místě	místo	k1gNnSc6
koniotomie	koniotomie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
nebo	nebo	k8xC
horní	horní	k2eAgFnPc4d1
tracheotomie	tracheotomie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Tracheotomie	tracheotomie	k1gFnSc1
u	u	k7c2
malého	malý	k2eAgNnSc2d1
dítěte	dítě	k1gNnSc2
</s>
<s>
Tracheotomie	tracheotomie	k1gFnSc1
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
provádí	provádět	k5eAaImIp3nS
na	na	k7c6
dvou	dva	k4xCgNnPc6
základních	základní	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
:	:	kIx,
</s>
<s>
Horní	horní	k2eAgFnSc1d1
tracheotomie	tracheotomie	k1gFnSc1
–	–	k?
průnik	průnik	k1gInSc1
těsně	těsně	k6eAd1
pod	pod	k7c7
prstencovou	prstencový	k2eAgFnSc7d1
chrupavkou	chrupavka	k1gFnSc7
hrtanu	hrtan	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
cestě	cesta	k1gFnSc6
řezu	řez	k1gInSc2
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
vazivo	vazivo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
tracheotomie	tracheotomie	k1gFnSc1
–	–	k?
průnik	průnik	k1gInSc1
řezem	řez	k1gInSc7
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
čáře	čára	k1gFnSc6
těsně	těsně	k6eAd1
nad	nad	k7c7
horním	horní	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
hrudníku	hrudník	k1gInSc2
(	(	kIx(
<g/>
nad	nad	k7c4
incisura	incisur	k1gMnSc4
jugularis	jugularis	k1gFnSc2
sterni	sternit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
tomto	tento	k3xDgInSc6
místě	místo	k1gNnSc6
už	už	k9
je	být	k5eAaImIp3nS
trachea	trachea	k1gFnSc1
hlouběji	hluboko	k6eAd2
a	a	k8xC
v	v	k7c6
cestě	cesta	k1gFnSc6
zákroku	zákrok	k1gInSc2
leží	ležet	k5eAaImIp3nP
žilní	žilní	k2eAgFnPc1d1
spojky	spojka	k1gFnPc1
a	a	k8xC
pleteně	pleteň	k1gFnPc1
</s>
<s>
Tracheotomie	tracheotomie	k1gFnSc1
a	a	k8xC
tracheostomie	tracheostomie	k1gFnSc1
</s>
<s>
Někdo	někdo	k3yInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
zaměnit	zaměnit	k5eAaPmF
pojem	pojem	k1gInSc4
tracheotomie	tracheotomie	k1gFnSc2
s	s	k7c7
tracheostomií	tracheostomie	k1gFnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
tracheotomie	tracheotomie	k1gFnSc1
je	být	k5eAaImIp3nS
chirurgický	chirurgický	k2eAgInSc4d1
zákrok	zákrok	k1gInSc4
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
tracheostomie	tracheostomie	k1gFnSc1
je	být	k5eAaImIp3nS
stav	stav	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
průdušnice	průdušnice	k1gFnSc1
uměle	uměle	k6eAd1
vyústěna	vyústit	k5eAaPmNgFnS
na	na	k7c4
povrch	povrch	k1gInSc4
těla	tělo	k1gNnSc2
po	po	k7c4
vykonání	vykonání	k1gNnSc4
tracheotomie	tracheotomie	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
po	po	k7c6
vytvoření	vytvoření	k1gNnSc6
otvoru	otvor	k1gInSc2
v	v	k7c6
průdušnici	průdušnice	k1gFnSc6
a	a	k8xC
zasunutí	zasunutí	k1gNnSc6
tracheostomické	tracheostomický	k2eAgFnSc2d1
kanyly	kanyla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
tracheostomie	tracheostomie	k1gFnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
zajištění	zajištění	k1gNnSc1
průchodnosti	průchodnost	k1gFnSc2
dýchacích	dýchací	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
pro	pro	k7c4
umožnění	umožnění	k1gNnSc4
ventilace	ventilace	k1gFnSc2
(	(	kIx(
<g/>
spontánní	spontánní	k2eAgNnSc1d1
nebo	nebo	k8xC
s	s	k7c7
pomocí	pomoc	k1gFnSc7
přístroje	přístroj	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
být	být	k5eAaImF
trvalá	trvalý	k2eAgFnSc1d1
nebo	nebo	k8xC
dočasná	dočasný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Tracheostomii	tracheostomie	k1gFnSc4
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
pacienti	pacient	k1gMnPc1
dočasně	dočasně	k6eAd1
nebo	nebo	k8xC
dlouhodobě	dlouhodobě	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
dlouhodobém	dlouhodobý	k2eAgInSc6d1
případě	případ	k1gInSc6
se	se	k3xPyFc4
pacient	pacient	k1gMnSc1
musí	muset	k5eAaImIp3nS
potýkat	potýkat	k5eAaImF
s	s	k7c7
překážkami	překážka	k1gFnPc7
v	v	k7c6
komunikaci	komunikace	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
jde	jít	k5eAaImIp3nS
však	však	k9
řešit	řešit	k5eAaImF
mluvicí	mluvicí	k2eAgFnSc7d1
kanylou	kanyla	k1gFnSc7
bez	bez	k7c2
balónku	balónek	k1gInSc2
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
se	s	k7c7
speciálním	speciální	k2eAgInSc7d1
ventilem	ventil	k1gInSc7
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
řešení	řešení	k1gNnSc1
umožní	umožnit	k5eAaPmIp3nS
proudění	proudění	k1gNnSc1
vzduchu	vzduch	k1gInSc2
kolem	kolem	k7c2
hlasivek	hlasivka	k1gFnPc2
a	a	k8xC
následnou	následný	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
pacienty	pacient	k1gMnPc4
<g/>
,	,	kIx,
kterým	který	k3yQgNnSc7,k3yRgNnSc7,k3yIgNnSc7
to	ten	k3xDgNnSc1
činí	činit	k5eAaImIp3nS
potíže	potíž	k1gFnPc4
a	a	k8xC
je	být	k5eAaImIp3nS
jim	on	k3xPp3gMnPc3
to	ten	k3xDgNnSc1
velmi	velmi	k6eAd1
nepříjemné	příjemný	k2eNgNnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
další	další	k2eAgNnSc1d1
řešení	řešení	k1gNnSc1
<g/>
:	:	kIx,
elektrolarynx	elektrolarynx	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrolarynx	Elektrolarynx	k1gInSc1
je	být	k5eAaImIp3nS
elektrický	elektrický	k2eAgInSc4d1
generátor	generátor	k1gInSc4
zvuku	zvuk	k1gInSc2
o	o	k7c6
velikosti	velikost	k1gFnSc6
ruční	ruční	k2eAgFnSc2d1
svítilny	svítilna	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
po	po	k7c6
zapnutí	zapnutí	k1gNnSc6
a	a	k8xC
přiložení	přiložení	k1gNnSc6
na	na	k7c6
měkké	měkký	k2eAgFnSc6d1
části	část	k1gFnSc6
krku	krk	k1gInSc2
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
slyšitelné	slyšitelný	k2eAgFnPc4d1
vibrace	vibrace	k1gFnPc4
<g/>
,	,	kIx,
pohybem	pohyb	k1gInSc7
úst	ústa	k1gNnPc2
a	a	k8xC
jazyka	jazyk	k1gInSc2
uživatelé	uživatel	k1gMnPc1
vytváří	vytvářit	k5eAaPmIp3nP,k5eAaImIp3nP
jednotlivá	jednotlivý	k2eAgNnPc4d1
slova	slovo	k1gNnPc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
verbální	verbální	k2eAgMnPc1d1
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístroj	přístroj	k1gInSc4
těmto	tento	k3xDgMnPc3
pacientům	pacient	k1gMnPc3
nahrazuje	nahrazovat	k5eAaImIp3nS
hlasivky	hlasivka	k1gFnPc4
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
jim	on	k3xPp3gMnPc3
mluvit	mluvit	k5eAaImF
<g/>
,	,	kIx,
dalo	dát	k5eAaPmAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
funguje	fungovat	k5eAaImIp3nS
na	na	k7c6
stejném	stejný	k2eAgInSc6d1
principu	princip	k1gInSc6
jako	jako	k8xC,k8xS
hlasivky	hlasivka	k1gFnPc4
až	až	k9
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
tomu	ten	k3xDgNnSc3
nepotřebuje	potřebovat	k5eNaImIp3nS
proudění	proudění	k1gNnSc1
vzduchu	vzduch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
tracheotomie	tracheotomie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Tracheotomy	Tracheotom	k1gInPc1
Info	Info	k1gNnSc1
(	(	kIx(
<g/>
Komunita	komunita	k1gFnSc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
podstoupili	podstoupit	k5eAaPmAgMnP
tracheotomii	tracheotomie	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Aaron	Aaron	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
tracheostomy	tracheostom	k1gInPc7
page	pag	k1gFnSc2
(	(	kIx(
<g/>
Péče	péče	k1gFnSc1
o	o	k7c6
tracheostomii	tracheostomie	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Jak	jak	k6eAd1
provést	provést	k5eAaPmF
akutní	akutní	k2eAgFnPc4d1
tracheotomi	tracheoto	k1gFnPc7
(	(	kIx(
<g/>
Pro	pro	k7c4
informační	informační	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
RT	RT	kA
Corner	Corner	k1gMnSc1
(	(	kIx(
<g/>
Vzdělávací	vzdělávací	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
pro	pro	k7c4
lékaře	lékař	k1gMnPc4
a	a	k8xC
sestry	sestra	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
Obrázky	obrázek	k1gInPc4
a	a	k8xC
videozáznamy	videozáznam	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
