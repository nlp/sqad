<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
chlorozlatitá	chlorozlatitý	k2eAgFnSc1d1	chlorozlatitý
je	být	k5eAaImIp3nS	být
anorganická	anorganický	k2eAgFnSc1d1	anorganická
sloučenina	sloučenina	k1gFnSc1	sloučenina
s	s	k7c7	s
chemickým	chemický	k2eAgInSc7d1	chemický
vzorcem	vzorec	k1gInSc7	vzorec
HAuCl	HAuClum	k1gNnPc2	HAuClum
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
chlorozlatitá	chlorozlatitý	k2eAgFnSc1d1	chlorozlatitý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštením	rozpouštení	k1gNnSc7	rozpouštení
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
lučavce	lučavka	k1gFnSc6	lučavka
královské	královský	k2eAgFnSc6d1	královská
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
odpařením	odpaření	k1gNnSc7	odpaření
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Zahříváním	zahřívání	k1gNnSc7	zahřívání
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
chlorid	chlorid	k1gInSc4	chlorid
zlatitý	zlatitý	k2eAgInSc4d1	zlatitý
a	a	k8xC	a
chlorovodík	chlorovodík	k1gInSc4	chlorovodík
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
reverzibilní	reverzibilní	k2eAgFnSc1d1	reverzibilní
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Au	au	k0	au
<g/>
2	[number]	k4	2
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
6	[number]	k4	6
+	+	kIx~	+
2	[number]	k4	2
HCl	HCl	k1gFnPc2	HCl
2	[number]	k4	2
HAuCl	HAuCla	k1gFnPc2	HAuCla
<g/>
4	[number]	k4	4
<g/>
Kyselina	kyselina	k1gFnSc1	kyselina
chlorozlatitá	chlorozlatitat	k5eAaPmIp3nS	chlorozlatitat
je	být	k5eAaImIp3nS	být
prekurzorem	prekurzor	k1gMnSc7	prekurzor
komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc1	reakce
==	==	k?	==
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
chlorozlatitá	chlorozlatitý	k2eAgFnSc1d1	chlorozlatitý
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
redukována	redukovat	k5eAaBmNgFnS	redukovat
kovy	kov	k1gInPc7	kov
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tato	tento	k3xDgFnSc1	tento
kyselina	kyselina	k1gFnSc1	kyselina
je	být	k5eAaImIp3nS	být
redukována	redukovat	k5eAaBmNgFnS	redukovat
dimethylsulfidem	dimethylsulfid	k1gInSc7	dimethylsulfid
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
komplexní	komplexní	k2eAgFnSc2d1	komplexní
sloučeniny	sloučenina	k1gFnSc2	sloučenina
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
SAuCl	SAuCla	k1gFnPc2	SAuCla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
dalších	další	k2eAgInPc2d1	další
komplexů	komplex	k1gInPc2	komplex
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
chlorozlatitá	chlorozlatitý	k2eAgFnSc1d1	chlorozlatitý
je	být	k5eAaImIp3nS	být
elektrolytem	elektrolyt	k1gInSc7	elektrolyt
Wohlwillova	Wohlwillův	k2eAgInSc2d1	Wohlwillův
procesu	proces	k1gInSc2	proces
k	k	k7c3	k
refinaci	refinace	k1gFnSc3	refinace
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
chlorid	chlorid	k1gInSc1	chlorid
zlatitý	zlatitý	k2eAgInSc1d1	zlatitý
</s>
</p>
<p>
<s>
lučavka	lučavka	k1gFnSc1	lučavka
královská	královský	k2eAgFnSc1d1	královská
</s>
</p>
<p>
<s>
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Chloroauric	Chloroauric	k1gMnSc1	Chloroauric
acid	acid	k1gMnSc1	acid
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kyselina	kyselina	k1gFnSc1	kyselina
chlorozlatitá	chlorozlatitat	k5eAaPmIp3nS	chlorozlatitat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
