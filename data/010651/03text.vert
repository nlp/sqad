<p>
<s>
Snědek	snědek	k1gInSc1	snědek
pyrenejský	pyrenejský	k2eAgInSc1d1	pyrenejský
(	(	kIx(	(
<g/>
Ornithogalum	Ornithogalum	k1gInSc1	Ornithogalum
pyrenaicum	pyrenaicum	k1gInSc1	pyrenaicum
L.	L.	kA	L.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jednoděložné	jednoděložný	k2eAgFnSc2d1	jednoděložná
rostliny	rostlina	k1gFnSc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
chřestovité	chřestovitý	k2eAgFnSc2d1	chřestovitý
(	(	kIx(	(
<g/>
Asparagaceae	Asparagacea	k1gFnSc2	Asparagacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgInPc6d1	dřívější
taxonomických	taxonomický	k2eAgInPc6d1	taxonomický
systémech	systém	k1gInPc6	systém
byl	být	k5eAaImAgInS	být
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
hyacintovité	hyacintovitý	k2eAgFnSc2d1	hyacintovitý
(	(	kIx(	(
<g/>
Hyacinthaceae	Hyacinthacea	k1gFnSc2	Hyacinthacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
liliovité	liliovitý	k2eAgFnSc2d1	liliovitá
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
(	(	kIx(	(
<g/>
Liliaceae	Liliaceae	k1gNnSc1	Liliaceae
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
asi	asi	k9	asi
30-90	[number]	k4	30-90
cm	cm	kA	cm
vysokou	vysoký	k2eAgFnSc4d1	vysoká
vytrvalou	vytrvalý	k2eAgFnSc4d1	vytrvalá
rostlinu	rostlina	k1gFnSc4	rostlina
s	s	k7c7	s
cibulí	cibule	k1gFnSc7	cibule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
do	do	k7c2	do
2	[number]	k4	2
cm	cm	kA	cm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
v	v	k7c6	v
přízemní	přízemní	k2eAgFnSc6d1	přízemní
růžici	růžice	k1gFnSc6	růžice
<g/>
,	,	kIx,	,
víceméně	víceméně	k9	víceméně
sivozelené	sivozelený	k2eAgInPc1d1	sivozelený
<g/>
,	,	kIx,	,
přisedlé	přisedlý	k2eAgInPc1d1	přisedlý
<g/>
.	.	kIx.	.
</s>
<s>
Čepele	čepel	k1gInPc1	čepel
jsou	být	k5eAaImIp3nP	být
čárkovité	čárkovitý	k2eAgInPc1d1	čárkovitý
<g/>
,	,	kIx,	,
žlábkovité	žlábkovitý	k2eAgInPc1d1	žlábkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
květenstvích	květenství	k1gNnPc6	květenství
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
je	být	k5eAaImIp3nS	být
podlouhle	podlouhle	k6eAd1	podlouhle
válcovitý	válcovitý	k2eAgInSc1d1	válcovitý
hrozen	hrozen	k1gInSc1	hrozen
<g/>
,	,	kIx,	,
listeny	listen	k1gInPc1	listen
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
květních	květní	k2eAgFnPc2d1	květní
stopek	stopka	k1gFnPc2	stopka
jsou	být	k5eAaImIp3nP	být
dole	dole	k6eAd1	dole
nejsou	být	k5eNaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
v	v	k7c4	v
postranní	postranní	k2eAgInPc4d1	postranní
bezžilné	bezžilný	k2eAgInPc4d1	bezžilný
laloky	lalok	k1gInPc4	lalok
a	a	k8xC	a
do	do	k7c2	do
špičky	špička	k1gFnSc2	špička
se	se	k3xPyFc4	se
zužují	zužovat	k5eAaImIp3nP	zužovat
pozvolna	pozvolna	k6eAd1	pozvolna
<g/>
.	.	kIx.	.
</s>
<s>
Okvětních	okvětní	k2eAgInPc2d1	okvětní
lístků	lístek	k1gInPc2	lístek
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
volné	volný	k2eAgNnSc4d1	volné
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
zelenavě	zelenavě	k6eAd1	zelenavě
až	až	k9	až
vodově	vodově	k6eAd1	vodově
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
nejsou	být	k5eNaImIp3nP	být
zvlněné	zvlněný	k2eAgFnPc1d1	zvlněná
<g/>
,	,	kIx,	,
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
mají	mít	k5eAaImIp3nP	mít
nevýrazně	výrazně	k6eNd1	výrazně
ohraničený	ohraničený	k2eAgInSc4d1	ohraničený
zelený	zelený	k2eAgInSc4d1	zelený
proužek	proužek	k1gInSc4	proužek
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
,	,	kIx,	,
nitky	nitka	k1gFnPc1	nitka
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
zoubků	zoubek	k1gInPc2	zoubek
<g/>
.	.	kIx.	.
</s>
<s>
Čnělky	čnělka	k1gFnPc1	čnělka
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
3-4	[number]	k4	3-4
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Gyneceum	Gyneceum	k1gNnSc1	Gyneceum
je	být	k5eAaImIp3nS	být
srostlé	srostlý	k2eAgNnSc1d1	srostlé
ze	z	k7c2	z
3	[number]	k4	3
plodolistů	plodolist	k1gInPc2	plodolist
<g/>
,	,	kIx,	,
plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
tobolka	tobolka	k1gFnSc1	tobolka
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
rozlišovány	rozlišován	k2eAgInPc4d1	rozlišován
2	[number]	k4	2
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
považované	považovaný	k2eAgInPc1d1	považovaný
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
snědek	snědek	k1gInSc1	snědek
pyrenejský	pyrenejský	k2eAgInSc1d1	pyrenejský
pravý	pravý	k2eAgInSc1d1	pravý
(	(	kIx(	(
<g/>
Ornithogalum	Ornithogalum	k1gInSc1	Ornithogalum
pyrenaicum	pyrenaicum	k1gNnSc1	pyrenaicum
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
pyrenaicum	pyrenaicum	k1gInSc1	pyrenaicum
<g/>
)	)	kIx)	)
a	a	k8xC	a
snědek	snědek	k1gInSc1	snědek
pyrenejský	pyrenejský	k2eAgInSc1d1	pyrenejský
kulatoplodý	kulatoplodý	k2eAgInSc1d1	kulatoplodý
(	(	kIx(	(
<g/>
Ornithogalum	Ornithogalum	k1gInSc1	Ornithogalum
pyrenaicum	pyrenaicum	k1gNnSc1	pyrenaicum
supbp	supbp	k1gInSc1	supbp
<g/>
.	.	kIx.	.
sphaerocarpum	sphaerocarpum	k1gInSc1	sphaerocarpum
(	(	kIx(	(
<g/>
Kerner	Kerner	k1gInSc1	Kerner
<g/>
)	)	kIx)	)
Hegi	Hegi	k1gNnSc1	Hegi
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Ornithogalum	Ornithogalum	k1gInSc1	Ornithogalum
sphaerocarpum	sphaerocarpum	k1gNnSc1	sphaerocarpum
Kerner	Kerner	k1gInSc1	Kerner
<g/>
,	,	kIx,	,
Loncomelos	Loncomelos	k1gMnSc1	Loncomelos
sphaerocarpus	sphaerocarpus	k1gMnSc1	sphaerocarpus
(	(	kIx(	(
<g/>
Kerner	Kerner	k1gMnSc1	Kerner
<g/>
)	)	kIx)	)
Hrouda	hrouda	k1gFnSc1	hrouda
<g/>
,	,	kIx,	,
Loncomelos	Loncomelos	k1gMnSc1	Loncomelos
pyrenaicus	pyrenaicus	k1gMnSc1	pyrenaicus
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
spaehaerocarpus	spaehaerocarpus	k1gMnSc1	spaehaerocarpus
(	(	kIx(	(
<g/>
Kerner	Kerner	k1gMnSc1	Kerner
<g/>
)	)	kIx)	)
Holub	Holub	k1gMnSc1	Holub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Dostál	Dostál	k1gMnSc1	Dostál
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
druh	druh	k1gInSc1	druh
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Loncomelos	Loncomelos	k1gMnSc1	Loncomelos
<g/>
,	,	kIx,	,
Loncomelos	Loncomelos	k1gMnSc1	Loncomelos
sphaerocarpus	sphaerocarpus	k1gMnSc1	sphaerocarpus
nazývá	nazývat	k5eAaImIp3nS	nazývat
snědovka	snědovka	k1gFnSc1	snědovka
kulatoplodá	kulatoplodý	k2eAgFnSc1d1	kulatoplodá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Snědek	snědek	k1gInSc1	snědek
pyrenejský	pyrenejský	k2eAgInSc1d1	pyrenejský
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
až	až	k6eAd1	až
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
s	s	k7c7	s
přesahem	přesah	k1gInSc7	přesah
do	do	k7c2	do
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
roste	růst	k5eAaImIp3nS	růst
jen	jen	k9	jen
snědek	snědek	k1gInSc4	snědek
jehlancovitý	jehlancovitý	k2eAgInSc4d1	jehlancovitý
kulatoplodý	kulatoplodý	k2eAgInSc4d1	kulatoplodý
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Hostýnských	hostýnský	k2eAgInPc2d1	hostýnský
vrchů	vrch	k1gInPc2	vrch
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Bílých	bílý	k2eAgInPc2d1	bílý
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
často	často	k6eAd1	často
na	na	k7c6	na
sušších	suchý	k2eAgFnPc6d2	sušší
slatinných	slatinný	k2eAgFnPc6d1	slatinná
loukách	louka	k1gFnPc6	louka
<g/>
,	,	kIx,	,
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
stráních	stráň	k1gFnPc6	stráň
a	a	k8xC	a
úhorech	úhor	k1gInPc6	úhor
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kriticky	kriticky	k6eAd1	kriticky
ohroženou	ohrožený	k2eAgFnSc4d1	ohrožená
rostlinu	rostlina	k1gFnSc4	rostlina
flóry	flóra	k1gFnSc2	flóra
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
kategorie	kategorie	k1gFnSc2	kategorie
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Mladé	mladý	k2eAgInPc1d1	mladý
výhonky	výhonek	k1gInPc1	výhonek
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
zelenina	zelenina	k1gFnSc1	zelenina
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
chřest	chřest	k1gInSc4	chřest
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
Bath	Bath	k1gMnSc1	Bath
Asparagus	Asparagus	k1gMnSc1	Asparagus
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Prussian	Prussian	k1gMnSc1	Prussian
Asparagus	Asparagus	k1gMnSc1	Asparagus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
snědek	snědek	k1gInSc1	snědek
pyrenejský	pyrenejský	k2eAgMnSc1d1	pyrenejský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
snědek	snědek	k1gInSc1	snědek
pyrenejský	pyrenejský	k2eAgMnSc1d1	pyrenejský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
