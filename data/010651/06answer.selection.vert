<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
květenstvích	květenství	k1gNnPc6	květenství
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
je	být	k5eAaImIp3nS	být
podlouhle	podlouhle	k6eAd1	podlouhle
válcovitý	válcovitý	k2eAgInSc1d1	válcovitý
hrozen	hrozen	k1gInSc1	hrozen
<g/>
,	,	kIx,	,
listeny	listen	k1gInPc1	listen
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
květních	květní	k2eAgFnPc2d1	květní
stopek	stopka	k1gFnPc2	stopka
jsou	být	k5eAaImIp3nP	být
dole	dole	k6eAd1	dole
nejsou	být	k5eNaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
v	v	k7c4	v
postranní	postranní	k2eAgInPc4d1	postranní
bezžilné	bezžilný	k2eAgInPc4d1	bezžilný
laloky	lalok	k1gInPc4	lalok
a	a	k8xC	a
do	do	k7c2	do
špičky	špička	k1gFnSc2	špička
se	se	k3xPyFc4	se
zužují	zužovat	k5eAaImIp3nP	zužovat
pozvolna	pozvolna	k6eAd1	pozvolna
<g/>
.	.	kIx.	.
</s>
