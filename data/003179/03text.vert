<s>
Původem	původ	k1gInSc7	původ
řecké	řecký	k2eAgNnSc1d1	řecké
slovo	slovo	k1gNnSc1	slovo
ortogonální	ortogonální	k2eAgMnSc1d1	ortogonální
znamená	znamenat	k5eAaImIp3nS	znamenat
pravoúhlý	pravoúhlý	k2eAgInSc1d1	pravoúhlý
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
ο	ο	k?	ο
pravý	pravý	k2eAgInSc1d1	pravý
a	a	k8xC	a
γ	γ	k?	γ
úhel	úhel	k1gInSc1	úhel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přeneseně	přeneseně	k6eAd1	přeneseně
<g/>
,	,	kIx,	,
v	v	k7c6	v
technice	technika	k1gFnSc6	technika
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nezávislý	závislý	k2eNgInSc1d1	nezávislý
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
neovlivňující	ovlivňující	k2eNgFnSc1d1	neovlivňující
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
termín	termín	k1gInSc1	termín
užíván	užívat	k5eAaImNgInS	užívat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
elementární	elementární	k2eAgFnSc2d1	elementární
geometrie	geometrie	k1gFnSc2	geometrie
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
přímek	přímka	k1gFnPc2	přímka
protínajících	protínající	k2eAgFnPc2d1	protínající
se	se	k3xPyFc4	se
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
úhlu	úhel	k1gInSc6	úhel
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
řečeno	říct	k5eAaPmNgNnS	říct
pokud	pokud	k8xS	pokud
všechny	všechen	k3xTgInPc1	všechen
čtyři	čtyři	k4xCgInPc1	čtyři
úhly	úhel	k1gInPc1	úhel
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
protínající	protínající	k2eAgFnPc1d1	protínající
se	se	k3xPyFc4	se
přímky	přímka	k1gFnPc1	přímka
vymezují	vymezovat	k5eAaImIp3nP	vymezovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravému	pravý	k2eAgInSc3d1	pravý
úhlu	úhel	k1gInSc3	úhel
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
velikost	velikost	k1gFnSc1	velikost
90	[number]	k4	90
<g/>
°	°	k?	°
nebo	nebo	k8xC	nebo
π	π	k?	π
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
radiánu	radián	k1gInSc2	radián
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
pravoúhlý	pravoúhlý	k2eAgInSc1d1	pravoúhlý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
je	být	k5eAaImIp3nS	být
ortogonalita	ortogonalita	k1gFnSc1	ortogonalita
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
kolmost	kolmost	k1gFnSc1	kolmost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
lineární	lineární	k2eAgFnSc2d1	lineární
algebry	algebra	k1gFnSc2	algebra
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zobecnění	zobecnění	k1gNnSc3	zobecnění
pojmu	pojem	k1gInSc2	pojem
ortogonality	ortogonalita	k1gFnSc2	ortogonalita
na	na	k7c4	na
obecné	obecný	k2eAgInPc4d1	obecný
vektorové	vektorový	k2eAgInPc4d1	vektorový
prostory	prostor	k1gInPc4	prostor
se	s	k7c7	s
skalárním	skalární	k2eAgInSc7d1	skalární
součinem	součin	k1gInSc7	součin
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
unitární	unitární	k2eAgInPc1d1	unitární
prostory	prostor	k1gInPc1	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vektory	vektor	k1gInPc1	vektor
jsou	být	k5eAaImIp3nP	být
nazývány	nazýván	k2eAgMnPc4d1	nazýván
ortogonálními	ortogonální	k2eAgFnPc7d1	ortogonální
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
jejich	jejich	k3xOp3gInSc4	jejich
skalární	skalární	k2eAgInSc4d1	skalární
součin	součin	k1gInSc4	součin
nulový	nulový	k2eAgInSc4d1	nulový
<g/>
.	.	kIx.	.
</s>
<s>
Význačnou	význačný	k2eAgFnSc4d1	význačná
úlohu	úloha	k1gFnSc4	úloha
hrají	hrát	k5eAaImIp3nP	hrát
ortogonální	ortogonální	k2eAgFnPc1d1	ortogonální
báze	báze	k1gFnPc1	báze
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
nekonečnědimenzionálních	konečnědimenzionální	k2eNgInPc2d1	konečnědimenzionální
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
úplnosti	úplnost	k1gFnSc2	úplnost
báze	báze	k1gFnSc2	báze
netriviální	triviálnět	k5eNaImIp3nS	triviálnět
a	a	k8xC	a
ortogonalita	ortogonalita	k1gFnSc1	ortogonalita
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
jeho	jeho	k3xOp3gFnSc4	jeho
definici	definice	k1gFnSc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
systémy	systém	k1gInPc4	systém
ortogonálních	ortogonální	k2eAgFnPc2d1	ortogonální
funkcí	funkce	k1gFnPc2	funkce
umožňující	umožňující	k2eAgInPc1d1	umožňující
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
funkci	funkce	k1gFnSc4	funkce
z	z	k7c2	z
daného	daný	k2eAgInSc2d1	daný
prostoru	prostor	k1gInSc2	prostor
funkcí	funkce	k1gFnPc2	funkce
jako	jako	k8xC	jako
součet	součet	k1gInSc1	součet
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
řady	řada	k1gFnSc2	řada
vektorů	vektor	k1gInPc2	vektor
báze	báze	k1gFnSc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
navíc	navíc	k6eAd1	navíc
vektory	vektor	k1gInPc1	vektor
jednotkovou	jednotkový	k2eAgFnSc4d1	jednotková
normu	norma	k1gFnSc4	norma
(	(	kIx(	(
<g/>
velikost	velikost	k1gFnSc4	velikost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ortonormalitu	ortonormalita	k1gFnSc4	ortonormalita
(	(	kIx(	(
<g/>
ortonormální	ortonormální	k2eAgInSc1d1	ortonormální
vektor	vektor	k1gInSc1	vektor
<g/>
,	,	kIx,	,
ortonormální	ortonormální	k2eAgFnSc1d1	ortonormální
báze	báze	k1gFnSc1	báze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
teorii	teorie	k1gFnSc6	teorie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
stavům	stav	k1gInPc3	stav
systému	systém	k1gInSc2	systém
přiřazeny	přiřazen	k2eAgInPc4d1	přiřazen
vektory	vektor	k1gInPc4	vektor
z	z	k7c2	z
Hilbertova	Hilbertův	k2eAgInSc2d1	Hilbertův
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
ortogonální	ortogonální	k2eAgInPc1d1	ortogonální
vektory	vektor	k1gInPc1	vektor
takovým	takový	k3xDgInPc3	takový
stavům	stav	k1gInPc3	stav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
nalezení	nalezení	k1gNnSc2	nalezení
jednoho	jeden	k4xCgMnSc4	jeden
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
je	být	k5eAaImIp3nS	být
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
pak	pak	k6eAd1	pak
stavy	stav	k1gInPc4	stav
odpovídající	odpovídající	k2eAgInPc4d1	odpovídající
klasickým	klasický	k2eAgInSc7d1	klasický
stavům	stav	k1gInPc3	stav
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
stavy	stav	k1gInPc4	stav
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určené	určený	k2eAgInPc4d1	určený
hodnotami	hodnota	k1gFnPc7	hodnota
měřitelných	měřitelný	k2eAgFnPc2d1	měřitelná
veličin	veličina	k1gFnPc2	veličina
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
ortogonální	ortogonální	k2eAgFnSc4d1	ortogonální
bázi	báze	k1gFnSc4	báze
Hilbertova	Hilbertův	k2eAgInSc2d1	Hilbertův
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
funkcí	funkce	k1gFnPc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k6eAd1	langle
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gNnSc2	rangle
}	}	kIx)	}
ortogonální	ortogonální	k2eAgInSc1d1	ortogonální
s	s	k7c7	s
váhou	váha	k1gFnSc7	váha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
≥	≥	k?	≥
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
dvojici	dvojice	k1gFnSc4	dvojice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
i	i	k8xC	i
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
pro	pro	k7c4	pro
:	:	kIx,	:
i	i	k8xC	i
≠	≠	k?	≠
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
w	w	k?	w
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
pro	pro	k7c4	pro
}}	}}	k?	}}
<g/>
i	i	k8xC	i
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
k	k	k7c3	k
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
f	f	k?	f
nazýváme	nazývat	k5eAaImIp1nP	nazývat
normovanou	normovaný	k2eAgFnSc7d1	normovaná
s	s	k7c7	s
váhou	váha	k1gFnSc7	váha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
w	w	k?	w
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
:	:	kIx,	:
Systém	systém	k1gInSc1	systém
funkcí	funkce	k1gFnPc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
<g/>
}}	}}	k?	}}
ortogonální	ortogonální	k2eAgInSc1d1	ortogonální
s	s	k7c7	s
váhou	váha	k1gFnSc7	váha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
každá	každý	k3xTgFnSc1	každý
funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
normovaná	normovaný	k2eAgFnSc1d1	normovaná
s	s	k7c7	s
váhou	váha	k1gFnSc7	váha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
ortonormální	ortonormální	k2eAgMnSc1d1	ortonormální
(	(	kIx(	(
<g/>
ortonormovaný	ortonormovaný	k2eAgMnSc1d1	ortonormovaný
<g/>
)	)	kIx)	)
s	s	k7c7	s
váhou	váha	k1gFnSc7	váha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc6	L_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Systémy	systém	k1gInPc1	systém
ortogonálních	ortogonální	k2eAgFnPc2d1	ortogonální
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc6	L_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
našly	najít	k5eAaPmAgFnP	najít
praktické	praktický	k2eAgNnSc4d1	praktické
uplatnění	uplatnění	k1gNnSc4	uplatnění
především	především	k6eAd1	především
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
,	,	kIx,	,
g	g	kA	g
∈	∈	k?	∈
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
,	,	kIx,	,
<g/>
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
L_	L_	k1gFnSc1	L_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
<g/>
}	}	kIx)	}
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
ortogonální	ortogonální	k2eAgFnSc4d1	ortogonální
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc6	L_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
na	na	k7c6	na
intervalu	interval	k1gInSc6	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k6eAd1	langle
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gFnSc1	rangle
}	}	kIx)	}
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
f	f	k?	f
,	,	kIx,	,
g	g	kA	g
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
,	,	kIx,	,
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
skalární	skalární	k2eAgInSc4d1	skalární
součin	součin	k1gInSc4	součin
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
vztahu	vztah	k1gInSc6	vztah
vyjadřujeme	vyjadřovat	k5eAaImIp1nP	vyjadřovat
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
Funkci	funkce	k1gFnSc4	funkce
f	f	k?	f
nazýváme	nazývat	k5eAaImIp1nP	nazývat
normovanou	normovaný	k2eAgFnSc4d1	normovaná
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc6	L_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
její	její	k3xOp3gFnSc6	její
norma	norma	k1gFnSc1	norma
rovna	roven	k2eAgFnSc1d1	rovna
jedné	jeden	k4xCgFnSc2	jeden
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
f	f	k?	f
∥	∥	k?	∥
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
:	:	kIx,	:
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
konečný	konečný	k2eAgInSc4d1	konečný
nebo	nebo	k8xC	nebo
spočetný	spočetný	k2eAgInSc4d1	spočetný
systém	systém	k1gInSc4	systém
funkcí	funkce	k1gFnPc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
L_	L_	k1gFnSc1	L_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
ortogonální	ortogonální	k2eAgMnSc1d1	ortogonální
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc6	L_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
dvojici	dvojice	k1gFnSc4	dvojice
funkcí	funkce	k1gFnPc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
pro	pro	k7c4	pro
:	:	kIx,	:
i	i	k8xC	i
≠	≠	k?	≠
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
pro	pro	k7c4	pro
}}	}}	k?	}}
<g/>
i	i	k8xC	i
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
k	k	k7c3	k
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
navíc	navíc	k6eAd1	navíc
každá	každý	k3xTgFnSc1	každý
funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
normovaná	normovaný	k2eAgNnPc1d1	normované
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
funkcí	funkce	k1gFnPc2	funkce
je	být	k5eAaImIp3nS	být
ortonormovaný	ortonormovaný	k2eAgInSc1d1	ortonormovaný
(	(	kIx(	(
<g/>
ortonormální	ortonormální	k2eAgInSc1d1	ortonormální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
δ	δ	k?	δ
:	:	kIx,	:
i	i	k8xC	i
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
ik	ik	k?	ik
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
δ	δ	k?	δ
:	:	kIx,	:
i	i	k8xC	i
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
ik	ik	k?	ik
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
Kroneckerovo	Kroneckerův	k2eAgNnSc4d1	Kroneckerův
delta	delta	k1gNnSc4	delta
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
ortogonální	ortogonální	k2eAgInSc1d1	ortogonální
systém	systém	k1gInSc1	systém
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
funkce	funkce	k1gFnPc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
platí	platit	k5eAaImIp3nP	platit
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
ortonormální	ortonormální	k2eAgInSc4d1	ortonormální
systém	systém	k1gInSc4	systém
zavedením	zavedení	k1gNnSc7	zavedení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Ortogonální	ortogonální	k2eAgFnSc1d1	ortogonální
instrukční	instrukční	k2eAgFnSc1d1	instrukční
sada	sada	k1gFnSc1	sada
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
sada	sada	k1gFnSc1	sada
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
procesoru	procesor	k1gInSc2	procesor
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
nejsou	být	k5eNaImIp3nP	být
přítomny	přítomen	k2eAgFnPc4d1	přítomna
duplicitní	duplicitní	k2eAgFnPc4d1	duplicitní
strojové	strojový	k2eAgFnPc4d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
operaci	operace	k1gFnSc4	operace
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
jediná	jediný	k2eAgFnSc1d1	jediná
strojová	strojový	k2eAgFnSc1d1	strojová
instrukce	instrukce	k1gFnSc1	instrukce
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
sada	sada	k1gFnSc1	sada
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
strojové	strojový	k2eAgFnPc1d1	strojová
instrukce	instrukce	k1gFnPc1	instrukce
mohly	moct	k5eAaImAgFnP	moct
použít	použít	k5eAaPmF	použít
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
registr	registr	k1gInSc4	registr
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
adresním	adresní	k2eAgInSc6d1	adresní
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
Terminologie	terminologie	k1gFnSc1	terminologie
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
instrukce	instrukce	k1gFnSc1	instrukce
je	být	k5eAaImIp3nS	být
vektor	vektor	k1gInSc4	vektor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc7	jehož
dalšími	další	k2eAgFnPc7d1	další
složkami	složka	k1gFnPc7	složka
jsou	být	k5eAaImIp3nP	být
operandy	operand	k1gInPc4	operand
a	a	k8xC	a
adresní	adresní	k2eAgInSc4d1	adresní
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Ortogonalitu	Ortogonalita	k1gFnSc4	Ortogonalita
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
názvu	název	k1gInSc6	název
technologie	technologie	k1gFnSc2	technologie
se	s	k7c7	s
zkratkou	zkratka	k1gFnSc7	zkratka
OFDM	OFDM	kA	OFDM
(	(	kIx(	(
<g/>
Orthogonal	Orthogonal	k1gMnSc1	Orthogonal
Frequency	Frequenca	k1gFnSc2	Frequenca
Division	Division	k1gInSc1	Division
Multiplexing	Multiplexing	k1gInSc1	Multiplexing
-	-	kIx~	-
ortogonální	ortogonální	k2eAgInSc1d1	ortogonální
multiplex	multiplex	k1gInSc1	multiplex
s	s	k7c7	s
frekvenčním	frekvenční	k2eAgNnSc7d1	frekvenční
dělením	dělení	k1gNnSc7	dělení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgFnSc4d1	využívající
širokopásmovou	širokopásmový	k2eAgFnSc4d1	širokopásmová
modulaci	modulace	k1gFnSc4	modulace
po	po	k7c4	po
vícero	vícero	k1gNnSc4	vícero
frekvenčních	frekvenční	k2eAgInPc6d1	frekvenční
kanálech	kanál	k1gInPc6	kanál
<g/>
,	,	kIx,	,
komunikace	komunikace	k1gFnSc1	komunikace
na	na	k7c6	na
žádném	žádný	k3yNgInSc6	žádný
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
neomezuje	omezovat	k5eNaImIp3nS	omezovat
ty	ten	k3xDgMnPc4	ten
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Gramova-Schmidtova	Gramova-Schmidtův	k2eAgFnSc1d1	Gramova-Schmidtův
ortogonalizace	ortogonalizace	k1gFnSc1	ortogonalizace
Ortogonální	ortogonální	k2eAgFnSc1d1	ortogonální
polynomy	polynom	k1gInPc1	polynom
Ortonormalita	Ortonormalita	k1gFnSc1	Ortonormalita
A	a	k9	a
<g/>
*	*	kIx~	*
<g/>
Pathfinder	Pathfinder	k1gMnSc1	Pathfinder
</s>
