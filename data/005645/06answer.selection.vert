<s>
Hudební	hudební	k2eAgInSc1d1	hudební
magazín	magazín	k1gInSc1	magazín
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
žebříčku	žebříček	k1gInSc6	žebříček
hudebního	hudební	k2eAgMnSc2d1	hudební
redaktora	redaktor	k1gMnSc2	redaktor
Davida	David	k1gMnSc2	David
Frickeho	Fricke	k1gMnSc2	Fricke
označil	označit	k5eAaPmAgMnS	označit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
za	za	k7c4	za
12	[number]	k4	12
<g/>
.	.	kIx.	.
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
kytaristu	kytarista	k1gMnSc4	kytarista
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
žebříčku	žebříček	k1gInSc6	žebříček
času	čas	k1gInSc2	čas
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
73	[number]	k4	73
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
