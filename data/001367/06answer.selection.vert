<s>
Titanic	Titanic	k1gInSc1	Titanic
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
americký	americký	k2eAgInSc1d1	americký
velkofilm	velkofilm	k1gInSc1	velkofilm
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc2	ocenění
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
11	[number]	k4	11
Oscarů	Oscar	k1gMnPc2	Oscar
včetně	včetně	k7c2	včetně
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
