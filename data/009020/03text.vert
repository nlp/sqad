<p>
<s>
Aurelia	Aurelia	k1gFnSc1	Aurelia
Cotta	Cotta	k1gFnSc1	Cotta
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
120	[number]	k4	120
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
54	[number]	k4	54
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
matkou	matka	k1gFnSc7	matka
římského	římský	k2eAgMnSc2d1	římský
diktátora	diktátor	k1gMnSc2	diktátor
Julia	Julius	k1gMnSc2	Julius
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Aurelia	Aurelia	k1gFnSc1	Aurelia
Cotta	Cotta	k1gFnSc1	Cotta
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
jako	jako	k9	jako
dcera	dcera	k1gFnSc1	dcera
Rutilie	Rutilie	k1gFnSc1	Rutilie
a	a	k8xC	a
Lucia	Lucia	k1gFnSc1	Lucia
Aurelia	Aurelium	k1gNnSc2	Aurelium
Cotty	Cotta	k1gFnSc2	Cotta
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gNnSc4	jeho
bratr	bratr	k1gMnSc1	bratr
Marka	Marek	k1gMnSc2	Marek
Aurelia	Aurelium	k1gNnSc2	Aurelium
Cotty	Cotta	k1gMnSc2	Cotta
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
119	[number]	k4	119
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
římským	římský	k2eAgMnSc7d1	římský
konzulem	konzul	k1gMnSc7	konzul
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
dědeček	dědeček	k1gMnSc1	dědeček
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
jím	jíst	k5eAaImIp1nS	jíst
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
144	[number]	k4	144
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
významnou	významný	k2eAgFnSc4d1	významná
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Rutilie	Rutilie	k1gFnSc1	Rutilie
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
Rutilia	Rutilium	k1gNnSc2	Rutilium
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
konzulární	konzulární	k2eAgNnSc4d1	konzulární
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
bratrů	bratr	k1gMnPc2	bratr
byli	být	k5eAaImAgMnP	být
také	také	k9	také
konzulyː	konzulyː	k?	konzulyː
Gaius	Gaius	k1gMnSc1	Gaius
Aurelius	Aurelius	k1gMnSc1	Aurelius
Cotta	Cotta	k1gMnSc1	Cotta
<g/>
,	,	kIx,	,
Marcus	Marcus	k1gMnSc1	Marcus
Aurelius	Aurelius	k1gMnSc1	Aurelius
Cotta	Cotta	k1gMnSc1	Cotta
a	a	k8xC	a
Lucius	Lucius	k1gMnSc1	Lucius
Aurelius	Aurelius	k1gMnSc1	Aurelius
Cotta	Cotta	k1gMnSc1	Cotta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aurelia	Aurelia	k1gFnSc1	Aurelia
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
praetora	praetor	k1gMnSc2	praetor
Gaia	Gaius	k1gMnSc2	Gaius
Julia	Julius	k1gMnSc2	Julius
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
tři	tři	k4xCgFnPc4	tři
dětiː	dětiː	k?	dětiː
</s>
</p>
<p>
<s>
Julia	Julius	k1gMnSc2	Julius
Major	major	k1gMnSc1	major
(	(	kIx(	(
<g/>
102	[number]	k4	102
–	–	k?	–
68	[number]	k4	68
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Pinaria	Pinarium	k1gNnSc2	Pinarium
a	a	k8xC	a
babička	babička	k1gFnSc1	babička
guvernéra	guvernér	k1gMnSc2	guvernér
Lucia	Lucius	k1gMnSc2	Lucius
Pinaria	Pinarium	k1gNnSc2	Pinarium
</s>
</p>
<p>
<s>
Julia	Julius	k1gMnSc2	Julius
Minor	minor	k2eAgMnSc1d1	minor
(	(	kIx(	(
<g/>
101	[number]	k4	101
–	–	k?	–
51	[number]	k4	51
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Marka	marka	k1gFnSc1	marka
Atia	Atia	k1gFnSc1	Atia
a	a	k8xC	a
babička	babička	k1gFnSc1	babička
císaře	císař	k1gMnSc2	císař
Augusta	August	k1gMnSc2	August
</s>
</p>
<p>
<s>
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
(	(	kIx(	(
<g/>
100	[number]	k4	100
–	–	k?	–
44	[number]	k4	44
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Charakter	charakter	k1gInSc1	charakter
==	==	k?	==
</s>
</p>
<p>
<s>
Historik	historik	k1gMnSc1	historik
Tacitus	Tacitus	k1gMnSc1	Tacitus
ji	on	k3xPp3gFnSc4	on
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
ideální	ideální	k2eAgFnSc4d1	ideální
římskou	římský	k2eAgFnSc4d1	římská
matrónu	matróna	k1gFnSc4	matróna
<g/>
.	.	kIx.	.
</s>
<s>
Plútarchos	Plútarchos	k1gInSc1	Plútarchos
ji	on	k3xPp3gFnSc4	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
přísnou	přísný	k2eAgFnSc4d1	přísná
a	a	k8xC	a
váženou	vážený	k2eAgFnSc4d1	Vážená
<g/>
"	"	kIx"	"
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Inteligentní	inteligentní	k2eAgFnSc1d1	inteligentní
<g/>
,	,	kIx,	,
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
krásou	krása	k1gFnSc7	krása
proslulá	proslulý	k2eAgFnSc1d1	proslulá
Aurelia	Aurelia	k1gFnSc1	Aurelia
si	se	k3xPyFc3	se
udržela	udržet	k5eAaPmAgFnS	udržet
vysoké	vysoký	k2eAgNnSc4d1	vysoké
postavení	postavení	k1gNnSc4	postavení
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aurelia	Aurelia	k1gFnSc1	Aurelia
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
rodina	rodina	k1gFnSc1	rodina
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
synovu	synův	k2eAgFnSc4d1	synova
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
bezpečí	bezpečí	k1gNnSc4	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnSc1d2	starší
Gaius	Gaius	k1gMnSc1	Gaius
Julius	Julius	k1gMnSc1	Julius
byl	být	k5eAaImAgMnS	být
často	často	k6eAd1	často
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
výchova	výchova	k1gFnSc1	výchova
syna	syn	k1gMnSc2	syn
ležela	ležet	k5eAaImAgFnS	ležet
na	na	k7c6	na
Aureliných	Aurelin	k2eAgNnPc6d1	Aurelin
ramenou	rameno	k1gNnPc6	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Juliu	Julius	k1gMnSc3	Julius
Caesarovi	Caesarův	k2eAgMnPc1d1	Caesarův
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nutil	nutit	k5eAaImAgMnS	nutit
ho	on	k3xPp3gNnSc4	on
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
římský	římský	k2eAgMnSc1d1	římský
diktátor	diktátor	k1gMnSc1	diktátor
Sulla	Sulla	k1gMnSc1	Sulla
k	k	k7c3	k
rozvodu	rozvod	k1gInSc3	rozvod
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Cornelií	Cornelie	k1gFnSc7	Cornelie
Cinnou	Cinna	k1gFnSc7	Cinna
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Caesar	Caesar	k1gMnSc1	Caesar
to	ten	k3xDgNnSc4	ten
zarputile	zarputile	k6eAd1	zarputile
odmítal	odmítat	k5eAaImAgMnS	odmítat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vystavil	vystavit	k5eAaPmAgMnS	vystavit
velkému	velký	k2eAgNnSc3d1	velké
riziku	riziko	k1gNnSc3	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Aurelia	Aurelia	k1gFnSc1	Aurelia
syna	syn	k1gMnSc2	syn
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Gaiem	Gai	k1gMnSc7	Gai
Cottou	Cotta	k1gMnSc7	Cotta
bránila	bránit	k5eAaImAgFnS	bránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Cornelia	Cornelia	k1gFnSc1	Cornelia
zemřela	zemřít	k5eAaPmAgFnS	zemřít
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
<g/>
,	,	kIx,	,
povýšila	povýšit	k5eAaPmAgFnS	povýšit
Aurelia	Aurelia	k1gFnSc1	Aurelia
svou	svůj	k3xOyFgFnSc4	svůj
vnučku	vnučka	k1gFnSc4	vnučka
Julii	Julie	k1gFnSc4	Julie
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
řídila	řídit	k5eAaImAgFnS	řídit
synovu	synův	k2eAgFnSc4d1	synova
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Pompeiou	Pompeiý	k2eAgFnSc7d1	Pompeiý
Sullou	Sulla	k1gFnSc7	Sulla
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
slavnosti	slavnost	k1gFnSc2	slavnost
Bona	bona	k1gFnSc1	bona
Dea	Dea	k1gFnSc1	Dea
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Caesarově	Caesarův	k2eAgInSc6d1	Caesarův
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
objevila	objevit	k5eAaPmAgFnS	objevit
Aureliina	Aureliin	k2eAgFnSc1d1	Aureliin
služebná	služebný	k2eAgFnSc1d1	služebná
Publia	Publia	k1gFnSc1	Publia
Clodia	Clodium	k1gNnSc2	Clodium
<g/>
,	,	kIx,	,
převlečeného	převlečený	k2eAgInSc2d1	převlečený
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zahájil	zahájit	k5eAaPmAgMnS	zahájit
nebo	nebo	k8xC	nebo
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
druhou	druhý	k4xOgFnSc7	druhý
snachou	snacha	k1gFnSc7	snacha
Pompeií	Pompeie	k1gFnPc2	Pompeie
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
sám	sám	k3xTgMnSc1	sám
Caesar	Caesar	k1gMnSc1	Caesar
připouštěl	připouštět	k5eAaImAgMnS	připouštět
její	její	k3xOp3gFnSc4	její
nevinnost	nevinnost	k1gFnSc4	nevinnost
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
mimo	mimo	k7c4	mimo
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Aurelia	Aurelius	k1gMnSc2	Aurelius
Cotta	Cott	k1gMnSc2	Cott
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
