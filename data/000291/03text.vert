<s>
Děti	dítě	k1gFnPc1	dítě
z	z	k7c2	z
Bullerbynu	Bullerbyn	k1gInSc2	Bullerbyn
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
Barnen	Barnen	k1gInSc1	Barnen
i	i	k8xC	i
Bullerbyn	Bullerbyn	k1gInSc1	Bullerbyn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1947	[number]	k4	1947
až	až	k8xS	až
1966	[number]	k4	1966
napsala	napsat	k5eAaBmAgFnS	napsat
švédská	švédský	k2eAgFnSc1d1	švédská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Astrid	Astrida	k1gFnPc2	Astrida
Lindgrenová	Lindgrenová	k1gFnSc1	Lindgrenová
<g/>
.	.	kIx.	.
</s>
<s>
Předobrazem	předobraz	k1gInSc7	předobraz
Bullerbynu	Bullerbyn	k1gInSc2	Bullerbyn
byla	být	k5eAaImAgFnS	být
osada	osada	k1gFnSc1	osada
Sevedstorp	Sevedstorp	k1gInSc4	Sevedstorp
nedaleko	nedaleko	k7c2	nedaleko
městečka	městečko	k1gNnSc2	městečko
Vimmerby	Vimmerba	k1gFnSc2	Vimmerba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Astrid	Astrid	k1gInSc1	Astrid
Lindgrenová	Lindgrenový	k2eAgFnSc1d1	Lindgrenová
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
narodila	narodit	k5eAaPmAgFnS	narodit
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
blízko	blízko	k6eAd1	blízko
sebe	sebe	k3xPyFc4	sebe
ležící	ležící	k2eAgInPc1d1	ležící
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
nastěhovali	nastěhovat	k5eAaPmAgMnP	nastěhovat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
narození	narození	k1gNnSc6	narození
a	a	k8xC	a
kde	kde	k6eAd1	kde
potom	potom	k6eAd1	potom
strávila	strávit	k5eAaPmAgFnS	strávit
dětství	dětství	k1gNnSc4	dětství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
šesti	šest	k4xCc2	šest
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
osady	osada	k1gFnSc2	osada
Bullerbyn	Bullerbyn	k1gInSc1	Bullerbyn
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
"	"	kIx"	"
<g/>
Hlučná	hlučný	k2eAgFnSc1d1	hlučná
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
pouhých	pouhý	k2eAgInPc2d1	pouhý
tří	tři	k4xCgInPc2	tři
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
obývá	obývat	k5eAaImIp3nS	obývat
jedna	jeden	k4xCgFnSc1	jeden
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěčkou	vypravěčka	k1gFnSc7	vypravěčka
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
dívenka	dívenka	k1gFnSc1	dívenka
Lisa	Lisa	k1gFnSc1	Lisa
(	(	kIx(	(
<g/>
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
knihy	kniha	k1gFnSc2	kniha
oslaví	oslavit	k5eAaPmIp3nS	oslavit
sedmé	sedmý	k4xOgFnPc4	sedmý
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
starší	starší	k1gMnPc4	starší
bratry	bratr	k1gMnPc4	bratr
jménem	jméno	k1gNnSc7	jméno
Lasse	Lasse	k1gFnSc2	Lasse
(	(	kIx(	(
<g/>
9	[number]	k4	9
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bosse	boss	k1gMnSc2	boss
(	(	kIx(	(
<g/>
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
bydlí	bydlet	k5eAaImIp3nS	bydlet
na	na	k7c6	na
statku	statek	k1gInSc6	statek
jménem	jméno	k1gNnSc7	jméno
Mellangå	Mellangå	k1gFnSc2	Mellangå
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bydlí	bydlet	k5eAaImIp3nS	bydlet
na	na	k7c4	na
Norrgå	Norrgå	k1gFnSc4	Norrgå
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc4	dva
dcerky	dcerka	k1gFnPc4	dcerka
Annu	Anna	k1gFnSc4	Anna
(	(	kIx(	(
<g/>
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
Brittu	Britta	k1gFnSc4	Britta
(	(	kIx(	(
<g/>
9	[number]	k4	9
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
domě	dům	k1gInSc6	dům
mají	mít	k5eAaImIp3nP	mít
syna	syn	k1gMnSc2	syn
Olleho	Olle	k1gMnSc2	Olle
(	(	kIx(	(
<g/>
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
malinkou	malinká	k1gFnSc4	malinká
Kerstin	Kerstina	k1gFnPc2	Kerstina
(	(	kIx(	(
<g/>
ti	ten	k3xDgMnPc1	ten
bydlí	bydlet	k5eAaImIp3nP	bydlet
na	na	k7c6	na
Sörgå	Sörgå	k1gFnSc6	Sörgå
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
spolu	spolu	k6eAd1	spolu
zažívají	zažívat	k5eAaImIp3nP	zažívat
různá	různý	k2eAgNnPc1d1	různé
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
a	a	k8xC	a
společně	společně	k6eAd1	společně
chodí	chodit	k5eAaImIp3nP	chodit
do	do	k7c2	do
jednotřídky	jednotřídka	k1gFnSc2	jednotřídka
ve	v	k7c6	v
Storbynu	Storbyn	k1gInSc6	Storbyn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knih	kniha	k1gFnPc2	kniha
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
několik	několik	k4yIc1	několik
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Činoherák	Činoherák	k1gMnSc1	Činoherák
Ústí	ústí	k1gNnSc2	ústí
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Filip	Filip	k1gMnSc1	Filip
Nuckolls	Nuckollsa	k1gFnPc2	Nuckollsa
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
</s>
