<s>
Děti	dítě	k1gFnPc1	dítě
z	z	k7c2	z
Bullerbynu	Bullerbyn	k1gInSc2	Bullerbyn
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
Barnen	Barnen	k1gInSc1	Barnen
i	i	k8xC	i
Bullerbyn	Bullerbyn	k1gInSc1	Bullerbyn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1947	[number]	k4	1947
až	až	k8xS	až
1966	[number]	k4	1966
napsala	napsat	k5eAaPmAgFnS	napsat
švédská	švédský	k2eAgFnSc1d1	švédská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Astrid	Astrida	k1gFnPc2	Astrida
Lindgrenová	Lindgrenová	k1gFnSc1	Lindgrenová
<g/>
.	.	kIx.	.
</s>
