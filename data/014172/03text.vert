<s>
Skleněná	skleněný	k2eAgNnPc1d1
textilní	textilní	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
</s>
<s>
Skleněná	skleněný	k2eAgNnPc1d1
textilní	textilní	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
jsou	být	k5eAaImIp3nP
textilní	textilní	k2eAgInSc4d1
materiál	materiál	k1gInSc4
získaný	získaný	k2eAgInSc4d1
z	z	k7c2
taveniny	tavenina	k1gFnSc2
nízkoalkalického	nízkoalkalický	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Nejstarší	starý	k2eAgInPc1d3
patenty	patent	k1gInPc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
skleněných	skleněný	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
za	za	k7c4
začátek	začátek	k1gInSc4
průmyslové	průmyslový	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
textilních	textilní	k2eAgNnPc2d1
skleněných	skleněný	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
rok	rok	k1gInSc1
1930	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Druh	druh	k1gInSc1
skla	sklo	k1gNnSc2
</s>
<s>
Pevnostv	Pevnostv	k1gInSc1
tahu	tah	k1gInSc2
</s>
<s>
GPa	GPa	k?
</s>
<s>
E-modul	E-modul	k1gInSc1
</s>
<s>
GPa	GPa	k?
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
electric	electric	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1,7	1,7	k4
–	–	k?
3,5	3,5	k4
</s>
<s>
69	#num#	k4
–	–	k?
72	#num#	k4
</s>
<s>
S	s	k7c7
(	(	kIx(
<g/>
strength	strength	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2,0	2,0	k4
–	–	k?
4,5	4,5	k4
</s>
<s>
85	#num#	k4
</s>
<s>
C	C	kA
(	(	kIx(
<g/>
corrosion	corrosion	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1,7	1,7	k4
–	–	k?
2,8	2,8	k4
</s>
<s>
70	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sklo	sklo	k1gNnSc1
je	být	k5eAaImIp3nS
odolné	odolný	k2eAgNnSc1d1
proti	proti	k7c3
ohni	oheň	k1gInSc3
a	a	k8xC
mnoha	mnoho	k4c3
chemikáliím	chemikálie	k1gFnPc3
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
v	v	k7c6
tahu	tah	k1gInSc6
a	a	k8xC
nízký	nízký	k2eAgInSc1d1
modul	modul	k1gInSc1
pružnosti	pružnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlhkost	vlhkost	k1gFnSc1
však	však	k9
pevnost	pevnost	k1gFnSc4
vláken	vlákno	k1gNnPc2
snižuje	snižovat	k5eAaImIp3nS
a	a	k8xC
odolnost	odolnost	k1gFnSc4
proti	proti	k7c3
trvalému	trvalý	k2eAgNnSc3d1
namáhání	namáhání	k1gNnSc3
a	a	k8xC
pevnost	pevnost	k1gFnSc1
v	v	k7c6
oděru	oděr	k1gInSc6
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
nízká	nízký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
asi	asi	k9
2500	#num#	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
bod	bod	k1gInSc1
tání	tání	k1gNnSc2
až	až	k9
přes	přes	k7c4
1000	#num#	k4
<g/>
°	°	k?
C	C	kA
<g/>
,	,	kIx,
dlouhodobě	dlouhodobě	k6eAd1
snáší	snášet	k5eAaImIp3nS
sklo	sklo	k1gNnSc1
teploty	teplota	k1gFnSc2
až	až	k9
450	#num#	k4
<g/>
°	°	k?
C.	C.	kA
</s>
<s>
V	v	k7c6
tabulce	tabulka	k1gFnSc6
je	být	k5eAaImIp3nS
příklad	příklad	k1gInSc4
tří	tři	k4xCgInPc2
druhů	druh	k1gInPc2
skla	sklo	k1gNnSc2
nejčastěji	často	k6eAd3
používaných	používaný	k2eAgFnPc2d1
k	k	k7c3
výrobě	výroba	k1gFnSc3
textilního	textilní	k2eAgNnSc2d1
vlákna	vlákno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
obsahují	obsahovat	k5eAaImIp3nP
nejméně	málo	k6eAd3
50	#num#	k4
%	%	kIx~
oxidu	oxid	k1gInSc2
křemičitého	křemičitý	k2eAgInSc2d1
(	(	kIx(
<g/>
SiO	SiO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obsahem	obsah	k1gInSc7
ostatních	ostatní	k2eAgInPc2d1
chemických	chemický	k2eAgInPc2d1
prvků	prvek	k1gInPc2
se	se	k3xPyFc4
jednotlivé	jednotlivý	k2eAgInPc1d1
druhy	druh	k1gInPc1
liší	lišit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Například	například	k6eAd1
E-sklo	E-sklo	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
55	#num#	k4
%	%	kIx~
SiO	SiO	k1gFnPc2
<g/>
2	#num#	k4
<g/>
,	,	kIx,
18	#num#	k4
%	%	kIx~
CaO	CaO	k1gMnSc1
<g/>
,	,	kIx,
8	#num#	k4
%	%	kIx~
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
4,6	4,6	k4
%	%	kIx~
MgO	MgO	k1gFnPc4
a	a	k8xC
jiné	jiný	k2eAgInPc4d1
prvky	prvek	k1gInPc4
s	s	k7c7
podíly	podíl	k1gInPc7
pod	pod	k7c4
5	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlákna	vlákno	k1gNnPc1
z	z	k7c2
E-skla	E-skla	k1gFnSc2
jsou	být	k5eAaImIp3nP
vhodná	vhodný	k2eAgNnPc1d1
jako	jako	k8xC,k8xS
elektroizolační	elektroizolační	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S-sklo	S-sklo	k1gMnSc1
snáší	snášet	k5eAaImIp3nS
teploty	teplota	k1gFnPc4
přes	přes	k7c4
1000	#num#	k4
<g/>
°	°	k?
C	C	kA
a	a	k8xC
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
pružné	pružný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
C-sklo	C-sklo	k1gMnSc1
je	být	k5eAaImIp3nS
mimořádně	mimořádně	k6eAd1
odolné	odolný	k2eAgNnSc1d1
proti	proti	k7c3
chemikáliím	chemikálie	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Skleněná	skleněný	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
se	se	k3xPyFc4
vyrábějí	vyrábět	k5eAaImIp3nP
ze	z	k7c2
skelné	skelný	k2eAgFnSc2d1
taveniny	tavenina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Metody	metoda	k1gFnPc1
zvlákňování	zvlákňování	k1gNnSc2
</s>
<s>
Zvlákňování	zvlákňování	k1gNnSc1
přes	přes	k7c4
trysky	trysk	k1gInPc4
<g/>
:	:	kIx,
Skelná	skelný	k2eAgFnSc1d1
tavenina	tavenina	k1gFnSc1
při	při	k7c6
odtahování	odtahování	k1gNnSc6
z	z	k7c2
trysky	tryska	k1gFnSc2
rychlostí	rychlost	k1gFnSc7
30-60	30-60	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
sek	sek	k1gInSc1
<g/>
.	.	kIx.
tuhne	tuhnout	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc1d1
filamenty	filament	k1gInPc1
s	s	k7c7
jemností	jemnost	k1gFnSc7
4-13	4-13	k4
µ	µ	k1gFnPc2
se	se	k3xPyFc4
spojují	spojovat	k5eAaImIp3nP
do	do	k7c2
jednoho	jeden	k4xCgInSc2
svazku	svazek	k1gInSc2
<g/>
,	,	kIx,
šlichtují	šlichtovat	k5eAaImIp3nP
a	a	k8xC
navíjí	navíjet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
90	#num#	k4
<g/>
%	%	kIx~
skleněných	skleněný	k2eAgInPc2d1
filamentů	filament	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
staplová	staplový	k2eAgNnPc4d1
vlákna	vlákno	k1gNnPc4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
dvoufázová	dvoufázový	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
stupni	stupeň	k1gInSc6
se	se	k3xPyFc4
zhotovují	zhotovovat	k5eAaImIp3nP
z	z	k7c2
taveniny	tavenina	k1gFnSc2
tzv.	tzv.	kA
pelety	peleta	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
případně	případně	k6eAd1
skladují	skladovat	k5eAaImIp3nP
a	a	k8xC
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
fázi	fáze	k1gFnSc6
roztaví	roztavit	k5eAaPmIp3nP
a	a	k8xC
zvlákňují	zvlákňovat	k5eAaImIp3nP
tažením	tažení	k1gNnSc7
přes	přes	k7c4
trysku	tryska	k1gFnSc4
s	s	k7c7
pomocí	pomoc	k1gFnSc7
sítového	sítový	k2eAgInSc2d1
bubnu	buben	k1gInSc2
rychlostí	rychlost	k1gFnPc2
do	do	k7c2
60	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
sek	sek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Foukání	foukání	k1gNnSc1
přes	přes	k7c4
trysku	tryska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtah	odtah	k1gInSc1
z	z	k7c2
trysky	tryska	k1gFnSc2
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
stlačeným	stlačený	k2eAgInSc7d1
vzduchem	vzduch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlostí	rychlost	k1gFnPc2
150-200	150-200	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
sek	sek	k1gInSc1
<g/>
.	.	kIx.
se	se	k3xPyFc4
vytahují	vytahovat	k5eAaImIp3nP
niti	nit	k1gFnPc1
rozdílných	rozdílný	k2eAgFnPc2d1
délek	délka	k1gFnPc2
a	a	k8xC
ukládají	ukládat	k5eAaImIp3nP
na	na	k7c4
sítový	sítový	k2eAgInSc4d1
buben	buben	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
se	se	k3xPyFc4
vlákenný	vlákenný	k2eAgInSc1d1
materiál	materiál	k1gInSc1
odtahuje	odtahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
prochází	procházet	k5eAaImIp3nS
olejovou	olejový	k2eAgFnSc7d1
mlhovinou	mlhovina	k1gFnSc7
a	a	k8xC
navíjí	navíjet	k5eAaImIp3nS
na	na	k7c4
cívku	cívka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Tažení	tažení	k1gNnSc1
tyčemi	tyč	k1gFnPc7
<g/>
:	:	kIx,
100-200	100-200	k4
skleněných	skleněný	k2eAgFnPc2d1
tyčí	tyč	k1gFnPc2
150-190	150-190	k4
cm	cm	kA
dlouhých	dlouhý	k2eAgInPc2d1
a	a	k8xC
o	o	k7c6
průměru	průměr	k1gInSc6
4-5	4-5	k4
mm	mm	kA
se	se	k3xPyFc4
taví	tavit	k5eAaImIp3nS
na	na	k7c6
dolním	dolní	k2eAgInSc6d1
konci	konec	k1gInSc6
při	při	k7c6
konstantním	konstantní	k2eAgInSc6d1
posunu	posun	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odletující	odletující	k2eAgFnPc1d1
kapky	kapka	k1gFnPc1
táhnou	táhnout	k5eAaImIp3nP
vlákna	vlákno	k1gNnPc1
a	a	k8xC
padají	padat	k5eAaImIp3nP
na	na	k7c4
buben	buben	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
se	se	k3xPyFc4
rychlostí	rychlost	k1gFnSc7
40-50	40-50	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
sek	sek	k1gInSc1
<g/>
.	.	kIx.
nitě	nit	k1gFnPc1
navíjí	navíjet	k5eAaImIp3nP
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
se	se	k3xPyFc4
kapky	kapka	k1gFnPc1
odhazují	odhazovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Modifikované	modifikovaný	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
tyčemi	tyč	k1gFnPc7
<g/>
:	:	kIx,
Nitě	nit	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
leží	ležet	k5eAaImIp3nP
na	na	k7c6
odtahovacím	odtahovací	k2eAgInSc6d1
bubnu	buben	k1gInSc6
vedle	vedle	k7c2
sebe	se	k3xPyFc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
zvedají	zvedat	k5eAaImIp3nP
pomocí	pomocí	k7c2
proudu	proud	k1gInSc2
vzduchu	vzduch	k1gInSc2
a	a	k8xC
vedou	vést	k5eAaImIp3nP
do	do	k7c2
sběrného	sběrný	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
pak	pak	k6eAd1
lámou	lámat	k5eAaImIp3nP
na	na	k7c4
různé	různý	k2eAgFnPc4d1
délky	délka	k1gFnPc4
<g/>
,	,	kIx,
odtahují	odtahovat	k5eAaImIp3nP
a	a	k8xC
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
pramen	pramen	k1gInSc4
vláken	vlákno	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
navíjí	navíjet	k5eAaImIp3nS
jako	jako	k9
přást	přást	k1gInSc1
na	na	k7c4
cívku	cívka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vlákno	vlákno	k1gNnSc1
se	se	k3xPyFc4
dodává	dodávat	k5eAaImIp3nS
jako	jako	k9
hladký	hladký	k2eAgInSc4d1
nebo	nebo	k8xC
tvarovaný	tvarovaný	k2eAgInSc4d1
filament	filament	k1gInSc4
v	v	k7c6
tloušťce	tloušťka	k1gFnSc6
400-4000	400-4000	k4
tex	tex	k?
(	(	kIx(
<g/>
s	s	k7c7
průměrem	průměr	k1gInSc7
jednotlivých	jednotlivý	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
od	od	k7c2
6	#num#	k4
μ	μ	k1gFnPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
jako	jako	k9
skaná	skaný	k2eAgFnSc1d1
příze	příze	k1gFnSc1
400-2000	400-2000	k4
tex	tex	k?
nebo	nebo	k8xC
jako	jako	k9
stříž	stříž	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Staplové	Staplový	k2eAgFnPc1d1
příze	příz	k1gFnPc1
se	se	k3xPyFc4
dopřádají	dopřádat	k5eAaImIp3nP
na	na	k7c6
odstředivých	odstředivý	k2eAgInPc6d1
nebo	nebo	k8xC
frikčních	frikční	k2eAgInPc6d1
strojích	stroj	k1gInPc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Prstencové	prstencový	k2eAgInPc1d1
stroje	stroj	k1gInPc1
jsou	být	k5eAaImIp3nP
svým	svůj	k3xOyFgInSc7
systémem	systém	k1gInSc7
udělování	udělování	k1gNnSc2
zákrutu	zákrut	k1gInSc2
pro	pro	k7c4
skleněná	skleněný	k2eAgNnPc4d1
vlákna	vlákno	k1gNnSc2
nezpůsobilé	způsobilý	k2eNgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžně	běžně	k6eAd1
vyráběné	vyráběný	k2eAgFnSc2d1
příze	příz	k1gFnSc2
mají	mít	k5eAaImIp3nP
jemnost	jemnost	k1gFnSc4
125-2000	125-2000	k4
tex	tex	k?
a	a	k8xC
požívají	požívat	k5eAaImIp3nP
se	se	k3xPyFc4
většinou	většinou	k6eAd1
pro	pro	k7c4
podkladové	podkladový	k2eAgFnPc4d1
tkaniny	tkanina	k1gFnPc4
na	na	k7c4
tapety	tapeta	k1gFnPc4
a	a	k8xC
dekorační	dekorační	k2eAgFnPc4d1
látky	látka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Filamenty	Filament	k1gInPc1
se	se	k3xPyFc4
zpracovávají	zpracovávat	k5eAaImIp3nP
např.	např.	kA
na	na	k7c4
tkaniny	tkanina	k1gFnPc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
základních	základní	k2eAgFnPc6d1
vazbách	vazba	k1gFnPc6
s	s	k7c7
váhou	váha	k1gFnSc7
600-1300	600-1300	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
m²	m²	k?
<g/>
,	,	kIx,
tloušťkou	tloušťka	k1gFnSc7
0,8	0,8	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
pevností	pevnost	k1gFnSc7
v	v	k7c6
tahu	tah	k1gInSc6
<g/>
:	:	kIx,
osnova	osnova	k1gFnSc1
5000	#num#	k4
N	N	kA
<g/>
/	/	kIx~
<g/>
5	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
útek	útek	k1gInSc1
2500	#num#	k4
N	N	kA
<g/>
/	/	kIx~
<g/>
5	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
bod	bod	k1gInSc4
tání	tání	k1gNnSc2
až	až	k9
1200	#num#	k4
<g/>
°	°	k?
C.	C.	kA
Ve	v	k7c6
tkaninách	tkanina	k1gFnPc6
se	se	k3xPyFc4
též	též	k9
kombinuje	kombinovat	k5eAaImIp3nS
osnova	osnova	k1gFnSc1
nebo	nebo	k8xC
útek	útek	k1gInSc1
s	s	k7c7
přízemi	příz	k1gFnPc7
z	z	k7c2
aramidových	aramidový	k2eAgFnPc2d1
nebo	nebo	k8xC
uhlíkových	uhlíkový	k2eAgFnPc2d1
vláken	vlákna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
tkanin	tkanina	k1gFnPc2
se	se	k3xPyFc4
šijí	šít	k5eAaImIp3nP
ochranné	ochranný	k2eAgInPc1d1
oděvy	oděv	k1gInPc1
<g/>
,	,	kIx,
především	především	k9
pro	pro	k7c4
extrémně	extrémně	k6eAd1
horké	horký	k2eAgInPc1d1
provozy	provoz	k1gInPc1
(	(	kIx(
<g/>
hutníci	hutník	k1gMnPc1
<g/>
,	,	kIx,
svářeči	svářeč	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tkaniny	tkanina	k1gFnPc1
nebo	nebo	k8xC
paralelně	paralelně	k6eAd1
ložené	ložený	k2eAgInPc4d1
filamenty	filament	k1gInPc4
(	(	kIx(
<g/>
jednosměrné	jednosměrný	k2eAgInPc1d1
svazky	svazek	k1gInPc1
<g/>
,	,	kIx,
angl.	angl.	k?
<g/>
:	:	kIx,
UD-Rovings	UD-Rovings	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
vkládají	vkládat	k5eAaImIp3nP
jako	jako	k9
armatury	armatura	k1gFnPc1
do	do	k7c2
kompozit	kompozitum	k1gNnPc2
a	a	k8xC
stavebních	stavební	k2eAgFnPc2d1
hmot	hmota	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ze	z	k7c2
skleněných	skleněný	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
se	se	k3xPyFc4
také	také	k9
zhotovují	zhotovovat	k5eAaImIp3nP
izolace	izolace	k1gFnPc4
proti	proti	k7c3
žáru	žár	k1gInSc3
nebo	nebo	k8xC
chemickým	chemický	k2eAgInPc3d1
vlivům	vliv	k1gInPc3
ve	v	k7c6
formě	forma	k1gFnSc6
rohoží	rohož	k1gFnPc2
(	(	kIx(
<g/>
soudržnost	soudržnost	k1gFnSc1
je	být	k5eAaImIp3nS
zajištěna	zajištěn	k2eAgFnSc1d1
lisováním	lisování	k1gNnSc7
nebo	nebo	k8xC
prošíváním	prošívání	k1gNnSc7
vrstvy	vrstva	k1gFnSc2
vláken	vlákno	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
hadic	hadice	k1gFnPc2
<g/>
,	,	kIx,
stuh	stuha	k1gFnPc2
a	a	k8xC
šňůr	šňůra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
zhotoveno	zhotoven	k2eAgNnSc1d1
přes	přes	k7c4
milion	milion	k4xCgInSc4
tun	tuna	k1gFnPc2
kompozit	kompozitum	k1gNnPc2
(	(	kIx(
<g/>
tyto	tento	k3xDgInPc1
plasty	plast	k1gInPc1
obsahují	obsahovat	k5eAaImIp3nP
cca	cca	kA
50	#num#	k4
%	%	kIx~
váhového	váhový	k2eAgNnSc2d1
množství	množství	k1gNnSc2
skleněných	skleněný	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
na	na	k7c4
nejrůznější	různý	k2eAgInPc4d3
účely	účel	k1gInPc4
<g/>
:	:	kIx,
od	od	k7c2
nárazníků	nárazník	k1gInPc2
na	na	k7c4
auta	auto	k1gNnPc4
<g/>
,	,	kIx,
přes	přes	k7c4
čluny	člun	k1gInPc4
<g/>
,	,	kIx,
vrtule	vrtule	k1gFnPc4
větrných	větrný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
<g/>
,	,	kIx,
až	až	k9
k	k	k7c3
mostům	most	k1gInPc3
pro	pro	k7c4
chodce	chodec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Celosvětová	celosvětový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
skleněných	skleněný	k2eAgNnPc2d1
textilních	textilní	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
dosáhla	dosáhnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
asi	asi	k9
4,7	4,7	k4
miliony	milion	k4xCgInPc1
tun	tuna	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgInSc2
cca	cca	kA
80	#num#	k4
%	%	kIx~
jako	jako	k8xS,k8xC
roving	roving	k1gInSc4
a	a	k8xC
20	#num#	k4
%	%	kIx~
příze	příze	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Skleněná	skleněný	k2eAgFnSc1d1
vlákna	vlákna	k1gFnSc1
s	s	k7c7
optickými	optický	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
</s>
<s>
Skleněná	skleněný	k2eAgFnSc1d1
vlákna	vlákna	k1gFnSc1
s	s	k7c7
optickými	optický	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
od	od	k7c2
textilních	textilní	k2eAgNnPc2d1
skleněných	skleněný	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
chemickým	chemický	k2eAgNnSc7d1
složením	složení	k1gNnSc7
a	a	k8xC
technologií	technologie	k1gFnSc7
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Např.	např.	kA
pro	pro	k7c4
vlákna	vlákno	k1gNnPc4
na	na	k7c4
přenášení	přenášení	k1gNnSc4
dat	datum	k1gNnPc2
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
z	z	k7c2
čistého	čistý	k2eAgInSc2d1
(	(	kIx(
<g/>
nebo	nebo	k8xC
dopovaného	dopovaný	k2eAgMnSc2d1
<g/>
)	)	kIx)
SiO	SiO	k1gFnSc1
<g/>
2	#num#	k4
nánosem	nános	k1gInSc7
par	para	k1gFnPc2
(	(	kIx(
<g/>
CVD	CVD	kA
<g/>
)	)	kIx)
preforma	preform	k1gMnSc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
vzniká	vznikat	k5eAaImIp3nS
tažením	tažení	k1gNnSc7
(	(	kIx(
<g/>
při	při	k7c6
2000	#num#	k4
<g/>
°	°	k?
C	C	kA
<g/>
)	)	kIx)
filament	filament	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vlákna	vlákna	k1gFnSc1
s	s	k7c7
optickými	optický	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
také	také	k9
k	k	k7c3
přenosu	přenos	k1gInSc3
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
infračervených	infračervený	k2eAgInPc2d1
a	a	k8xC
laserových	laserový	k2eAgInPc2d1
paprsků	paprsek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
skleněných	skleněný	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
</s>
<s>
Skleněné	skleněný	k2eAgFnPc1d1
tyče	tyč	k1gFnPc1
jako	jako	k8xS,k8xC
surovina	surovina	k1gFnSc1
a	a	k8xC
pokusně	pokusně	k6eAd1
vyrobená	vyrobený	k2eAgFnSc1d1
staplová	staplová	k1gFnSc1
vlákna	vlákno	k1gNnSc2
(	(	kIx(
<g/>
NDR	NDR	kA
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svazek	svazek	k1gInSc1
skleněných	skleněný	k2eAgInPc2d1
filamentů	filament	k1gInPc2
(	(	kIx(
<g/>
UD-Roving	UD-Roving	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tkanina	tkanina	k1gFnSc1
v	v	k7c6
plátnové	plátnový	k2eAgFnSc6d1
vazbě	vazba	k1gFnSc6
(	(	kIx(
<g/>
1100	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
m²	m²	k?
<g/>
)	)	kIx)
</s>
<s>
Pletenec	pletenec	k1gInSc1
ze	z	k7c2
skleněné	skleněný	k2eAgFnSc2d1
příze	příz	k1gFnSc2
</s>
<s>
Vrstvený	vrstvený	k2eAgInSc4d1
skleněný	skleněný	k2eAgInSc4d1
pancíř	pancíř	k1gInSc4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Kießling	Kießling	k1gInSc1
<g/>
/	/	kIx~
<g/>
Matthes	Matthes	k1gInSc1
<g/>
:	:	kIx,
Textil-	Textil-	k1gFnSc1
Fachwörterbuch	Fachwörterbucha	k1gFnPc2
<g/>
,	,	kIx,
Berlin	berlina	k1gFnPc2
1993	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
7949	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
546	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
204	#num#	k4
<g/>
↑	↑	k?
Skleněná	skleněný	k2eAgFnSc1d1
vlákna	vlákna	k1gFnSc1
a	a	k8xC
výrobky	výrobek	k1gInPc1
z	z	k7c2
nich	on	k3xPp3gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technor	Technora	k1gFnPc2
<g/>
,	,	kIx,
2005-2015	2005-2015	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Introduction	Introduction	k1gInSc1
of	of	k?
Glass	Glass	k1gInSc1
Fiber	Fiber	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Textile	textil	k1gInSc5
Learner	Learner	k1gMnSc1
<g/>
,	,	kIx,
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Glasfasern	Glasfasern	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
conrad	conrad	k1gInSc1
<g/>
,	,	kIx,
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chokri	Chokri	k1gNnSc1
Cherif	Cherif	k1gMnSc1
<g/>
:	:	kIx,
Textile	textil	k1gInSc5
Werkstoffe	Werkstoff	k1gInSc5
für	für	k?
den	den	k1gInSc4
Leichtbau	Leichtbaus	k1gInSc2
<g/>
,	,	kIx,
Springer-Verlag	Springer-Verlag	k1gMnSc1
Berlin	berlina	k1gFnPc2
Heidelberg	Heidelberg	k1gInSc4
2011	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
642	#num#	k4
<g/>
-	-	kIx~
<g/>
17991	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
↑	↑	k?
Vlákna	vlákno	k1gNnSc2
pro	pro	k7c4
kompozity	kompozitum	k1gNnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kořínek	Kořínek	k1gMnSc1
<g/>
,	,	kIx,
2016-11-05	2016-11-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fiberglas	Fiberglas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adameg	Adamega	k1gFnPc2
<g/>
,	,	kIx,
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Technologies	Technologies	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vetrotex	Vetrotex	k1gInSc1
<g/>
,	,	kIx,
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Global	globat	k5eAaImAgMnS
glass-fibre	glass-fibr	k1gInSc5
production	production	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
JEC	JEC	kA
<g/>
,	,	kIx,
2011-02-21	2011-02-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Glasfaser	Glasfaser	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lumitos	Lumitosa	k1gFnPc2
<g/>
,	,	kIx,
1997-2017	1997-2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Schenek	Schenka	k1gFnPc2
<g/>
:	:	kIx,
Lexikon	lexikon	k1gInSc1
Garne	Garn	k1gInSc5
und	und	k?
Zwirne	Zwirn	k1gInSc5
<g/>
,	,	kIx,
Deutscher	Deutschra	k1gFnPc2
Fachverlag	Fachverlaga	k1gFnPc2
2005	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3-87150-810-1	3-87150-810-1	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Aramid	Aramid	k1gInSc1
</s>
<s>
Optické	optický	k2eAgNnSc1d1
vlákno	vlákno	k1gNnSc1
</s>
<s>
Polyakrylonitrilová	Polyakrylonitrilový	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
skleněná	skleněný	k2eAgNnPc1d1
textilní	textilní	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
