<s>
Skleněná	skleněný	k2eAgNnPc1d1
textilní	textilní	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
jsou	být	k5eAaImIp3nP
textilní	textilní	k2eAgInSc4d1
materiál	materiál	k1gInSc4
získaný	získaný	k2eAgInSc4d1
z	z	k7c2
taveniny	tavenina	k1gFnSc2
nízkoalkalického	nízkoalkalický	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Nejstarší	starý	k2eAgInPc1d3
patenty	patent	k1gInPc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
skleněných	skleněný	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
za	za	k7c4
začátek	začátek	k1gInSc4
průmyslové	průmyslový	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
textilních	textilní	k2eAgNnPc2d1
skleněných	skleněný	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
rok	rok	k1gInSc1
1930	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>