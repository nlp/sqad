<s>
Hydrologie	hydrologie	k1gFnSc1	hydrologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
:	:	kIx,	:
Yδ	Yδ	k1gMnSc1	Yδ
<g/>
,	,	kIx,	,
Yδ	Yδ	k1gMnSc1	Yδ
<g/>
+	+	kIx~	+
<g/>
Λ	Λ	k?	Λ
<g/>
,	,	kIx,	,
Hydrologia	Hydrologia	k1gFnSc1	Hydrologia
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
studium	studium	k1gNnSc4	studium
vody	voda	k1gFnSc2	voda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
pohybem	pohyb	k1gInSc7	pohyb
a	a	k8xC	a
rozšířením	rozšíření	k1gNnSc7	rozšíření
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Studuje	studovat	k5eAaImIp3nS	studovat
také	také	k9	také
hydrologické	hydrologický	k2eAgInPc4d1	hydrologický
cykly	cyklus	k1gInPc4	cyklus
a	a	k8xC	a
vodní	vodní	k2eAgInPc4d1	vodní
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Odborník	odborník	k1gMnSc1	odborník
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
hydrologií	hydrologie	k1gFnSc7	hydrologie
je	být	k5eAaImIp3nS	být
hydrolog	hydrolog	k1gMnSc1	hydrolog
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
geografických	geografický	k2eAgInPc6d1	geografický
či	či	k8xC	či
jiných	jiný	k2eAgInPc6d1	jiný
přírodovědných	přírodovědný	k2eAgInPc6d1	přírodovědný
oborech	obor	k1gInPc6	obor
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vodohospodářství	vodohospodářství	k1gNnSc6	vodohospodářství
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Součástmi	součást	k1gFnPc7	součást
hydrologie	hydrologie	k1gFnPc4	hydrologie
jsou	být	k5eAaImIp3nP	být
hydrografie	hydrografie	k1gFnPc1	hydrografie
<g/>
,	,	kIx,	,
hydrometeorologie	hydrometeorologie	k1gFnPc1	hydrometeorologie
a	a	k8xC	a
hydroklimatologie	hydroklimatologie	k1gFnPc1	hydroklimatologie
<g/>
,	,	kIx,	,
hydrologie	hydrologie	k1gFnPc1	hydrologie
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
hydrogeologie	hydrogeologie	k1gFnSc1	hydrogeologie
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
kvality	kvalita	k1gFnSc2	kvalita
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Oceánografie	oceánografie	k1gFnPc1	oceánografie
a	a	k8xC	a
meteorologie	meteorologie	k1gFnPc1	meteorologie
nejsou	být	k5eNaImIp3nP	být
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgInPc2d1	důležitý
aspektů	aspekt	k1gInPc2	aspekt
<g/>
.	.	kIx.	.
</s>
<s>
Hydrologický	hydrologický	k2eAgInSc1d1	hydrologický
výzkum	výzkum	k1gInSc1	výzkum
není	být	k5eNaImIp3nS	být
užitečný	užitečný	k2eAgInSc1d1	užitečný
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
poznání	poznání	k1gNnSc4	poznání
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
pochopení	pochopení	k1gNnSc4	pochopení
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
jejích	její	k3xOp3gFnPc2	její
zásad	zásada	k1gFnPc2	zásada
a	a	k8xC	a
plánování	plánování	k1gNnSc2	plánování
<g/>
.	.	kIx.	.
</s>
<s>
Hydrologické	hydrologický	k2eAgFnSc3d1	hydrologická
znalosti	znalost	k1gFnSc3	znalost
a	a	k8xC	a
dovednosti	dovednost	k1gFnSc3	dovednost
lidstvo	lidstvo	k1gNnSc1	lidstvo
získává	získávat	k5eAaImIp3nS	získávat
již	již	k6eAd1	již
po	po	k7c6	po
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
už	už	k6eAd1	už
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
4000	[number]	k4	4000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
přehrazen	přehrazen	k2eAgInSc1d1	přehrazen
Nil	Nil	k1gInSc1	Nil
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
produkce	produkce	k1gFnSc1	produkce
okolní	okolní	k2eAgFnSc2d1	okolní
vyprahlé	vyprahlý	k2eAgFnSc2d1	vyprahlá
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnPc1	město
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
byla	být	k5eAaImAgFnS	být
před	před	k7c7	před
povodněmi	povodeň	k1gFnPc7	povodeň
chráněna	chránit	k5eAaImNgFnS	chránit
vysokými	vysoký	k2eAgInPc7d1	vysoký
hliněnými	hliněný	k2eAgInPc7d1	hliněný
valy	val	k1gInPc7	val
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
rozvinuli	rozvinout	k5eAaPmAgMnP	rozvinout
stavbu	stavba	k1gFnSc4	stavba
akvaduktů	akvadukt	k1gInPc2	akvadukt
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Číňané	Číňan	k1gMnPc1	Číňan
budovali	budovat	k5eAaImAgMnP	budovat
zavlažovací	zavlažovací	k2eAgNnPc4d1	zavlažovací
a	a	k8xC	a
protipovodňová	protipovodňový	k2eAgNnPc4d1	protipovodňové
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Marcus	Marcus	k1gMnSc1	Marcus
Vitruvius	Vitruvius	k1gMnSc1	Vitruvius
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
stol	stol	k1gInSc1	stol
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
teorii	teorie	k1gFnSc4	teorie
hydrologického	hydrologický	k2eAgInSc2d1	hydrologický
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yQgInSc6	který
jsou	být	k5eAaImIp3nP	být
srážky	srážka	k1gFnPc1	srážka
dopadající	dopadající	k2eAgFnPc1d1	dopadající
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
pohlcovány	pohlcován	k2eAgMnPc4d1	pohlcován
zemským	zemský	k2eAgInSc7d1	zemský
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
vědečtějšího	vědecký	k2eAgInSc2d2	vědečtější
přístupu	přístup	k1gInSc2	přístup
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
a	a	k8xC	a
Bernard	Bernard	k1gMnSc1	Bernard
Palissy	Palissa	k1gFnSc2	Palissa
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
přesného	přesný	k2eAgInSc2d1	přesný
výkladu	výklad	k1gInSc2	výklad
hydrologického	hydrologický	k2eAgInSc2d1	hydrologický
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Hydrologické	hydrologický	k2eAgFnPc1d1	hydrologická
proměnné	proměnná	k1gFnPc1	proměnná
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
vyčísleny	vyčíslen	k2eAgFnPc1d1	vyčíslena
v	v	k7c4	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pionýry	pionýr	k1gInPc1	pionýr
moderní	moderní	k2eAgFnSc2d1	moderní
hydrologie	hydrologie	k1gFnSc2	hydrologie
byli	být	k5eAaImAgMnP	být
především	především	k6eAd1	především
Pierre	Pierr	k1gInSc5	Pierr
Perrault	Perrault	k1gInSc1	Perrault
<g/>
,	,	kIx,	,
Edme	Edmus	k1gMnSc5	Edmus
Mariotte	Mariott	k1gMnSc5	Mariott
a	a	k8xC	a
Edmund	Edmund	k1gMnSc1	Edmund
Halley	Halley	k1gMnSc1	Halley
<g/>
.	.	kIx.	.
</s>
<s>
Měřením	měření	k1gNnSc7	měření
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
odtoku	odtok	k1gInSc2	odtok
a	a	k8xC	a
povodí	povodí	k1gNnSc2	povodí
Perrault	Perraulta	k1gFnPc2	Perraulta
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
byly	být	k5eAaImAgFnP	být
dostačující	dostačující	k2eAgInPc1d1	dostačující
pro	pro	k7c4	pro
průtok	průtok	k1gInSc4	průtok
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Seině	Seina	k1gFnSc6	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Marriotte	Marriotte	k5eAaPmIp2nP	Marriotte
zkombinoval	zkombinovat	k5eAaPmAgMnS	zkombinovat
měření	měření	k1gNnSc1	měření
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
průřezu	průřez	k1gInSc2	průřez
říčního	říční	k2eAgNnSc2d1	říční
koryta	koryto	k1gNnSc2	koryto
Seiny	Seina	k1gFnSc2	Seina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
výpočtu	výpočet	k1gInSc2	výpočet
jejího	její	k3xOp3gInSc2	její
průtoku	průtok	k1gInSc2	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Halley	Halley	k1gMnSc1	Halley
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypařování	vypařování	k1gNnSc1	vypařování
ze	z	k7c2	z
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
bylo	být	k5eAaImAgNnS	být
dostatečné	dostatečný	k2eAgInPc4d1	dostatečný
pro	pro	k7c4	pro
říční	říční	k2eAgInSc4d1	říční
odtok	odtok	k1gInSc4	odtok
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pokroky	pokrok	k1gInPc1	pokrok
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
Bernoulliho	Bernoulli	k1gMnSc2	Bernoulli
piezometr	piezometr	k1gInSc4	piezometr
<g/>
,	,	kIx,	,
Bernoulliho	Bernoulli	k1gMnSc4	Bernoulli
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
,	,	kIx,	,
Pitotovu	Pitotův	k2eAgFnSc4d1	Pitotova
trubici	trubice	k1gFnSc4	trubice
a	a	k8xC	a
Chézyho	Chézy	k1gMnSc4	Chézy
rovnici	rovnice	k1gFnSc6	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
v	v	k7c6	v
podpovrchové	podpovrchový	k2eAgFnSc6d1	podpovrchová
hydrologii	hydrologie	k1gFnSc6	hydrologie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
Darcyho	Darcy	k1gMnSc2	Darcy
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
,	,	kIx,	,
Dupuit-Thiemovu	Dupuit-Thiemův	k2eAgFnSc4d1	Dupuit-Thiemův
rovnici	rovnice	k1gFnSc4	rovnice
proudění	proudění	k1gNnSc2	proudění
vody	voda	k1gFnSc2	voda
ke	k	k7c3	k
studni	studna	k1gFnSc3	studna
a	a	k8xC	a
Hagen-Poiseuillovu	Hagen-Poiseuillův	k2eAgFnSc4d1	Hagen-Poiseuillův
rovnici	rovnice	k1gFnSc4	rovnice
kapilárního	kapilární	k2eAgInSc2d1	kapilární
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
prováděn	provádět	k5eAaImNgInS	provádět
státní	státní	k2eAgInSc1d1	státní
hydrologický	hydrologický	k2eAgInSc1d1	hydrologický
průzkum	průzkum	k1gInSc1	průzkum
<g/>
,	,	kIx,	,
počaly	počnout	k5eAaPmAgFnP	počnout
racionální	racionální	k2eAgFnPc4d1	racionální
analýzy	analýza	k1gFnPc4	analýza
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
dřívější	dřívější	k2eAgInSc4d1	dřívější
empirismus	empirismus	k1gInSc4	empirismus
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
přínosem	přínos	k1gInSc7	přínos
byl	být	k5eAaImAgInS	být
zejména	zejména	k9	zejména
jednotkový	jednotkový	k2eAgInSc1d1	jednotkový
hydrogram	hydrogram	k1gInSc1	hydrogram
Leroye	Leroy	k1gMnSc2	Leroy
Shermana	Sherman	k1gMnSc2	Sherman
<g/>
,	,	kIx,	,
infiltrační	infiltrační	k2eAgFnSc1d1	infiltrační
teorie	teorie	k1gFnSc1	teorie
Roberta	Robert	k1gMnSc4	Robert
E.	E.	kA	E.
Hortona	Horton	k1gMnSc4	Horton
a	a	k8xC	a
C.	C.	kA	C.
V.	V.	kA	V.
Theisova	Theisův	k2eAgFnSc1d1	Theisův
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgNnSc1d1	popisující
neustálené	ustálený	k2eNgNnSc1d1	neustálené
proudění	proudění	k1gNnSc1	proudění
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
