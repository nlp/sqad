<s>
Hydrologie	hydrologie	k1gFnSc1	hydrologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
:	:	kIx,	:
Yδ	Yδ	k1gMnSc1	Yδ
<g/>
,	,	kIx,	,
Yδ	Yδ	k1gMnSc1	Yδ
<g/>
+	+	kIx~	+
<g/>
Λ	Λ	k?	Λ
<g/>
,	,	kIx,	,
Hydrologia	Hydrologia	k1gFnSc1	Hydrologia
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
studium	studium	k1gNnSc4	studium
vody	voda	k1gFnSc2	voda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
pohybem	pohyb	k1gInSc7	pohyb
a	a	k8xC	a
rozšířením	rozšíření	k1gNnSc7	rozšíření
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
