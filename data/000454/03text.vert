<s>
Neapol	Neapol	k1gFnSc1	Neapol
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Napoli	Napole	k1gFnSc4	Napole
<g/>
,	,	kIx,	,
neapolsky	neapolsky	k6eAd1	neapolsky
Nà	Nà	k1gFnSc1	Nà
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
provincie	provincie	k1gFnSc2	provincie
a	a	k8xC	a
regionu	region	k1gInSc2	region
Kampánie	Kampánie	k1gFnSc2	Kampánie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc4	třetí
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Itálie	Itálie	k1gFnSc2	Itálie
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
předměstími	předměstí	k1gNnPc7	předměstí
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
konurbaci	konurbace	k1gFnSc4	konurbace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	s	k7c7	s
3	[number]	k4	3
121	[number]	k4	121
397	[number]	k4	397
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
založeny	založen	k2eAgInPc4d1	založen
na	na	k7c4	na
informaci	informace	k1gFnSc4	informace
Istat	Istat	k1gInSc1	Istat
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
po	po	k7c6	po
aglomeraci	aglomerace	k1gFnSc6	aglomerace
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
místě	místo	k1gNnSc6	místo
nejlidnatějších	lidnatý	k2eAgFnPc2d3	nejlidnatější
metropolitních	metropolitní	k2eAgFnPc2d1	metropolitní
oblastí	oblast	k1gFnPc2	oblast
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
4	[number]	k4	4
400	[number]	k4	400
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
2	[number]	k4	2
228,79	[number]	k4	228,79
km2	km2	k4	km2
(	(	kIx(	(
<g/>
údaje	údaj	k1gInSc2	údaj
od	od	k7c2	od
Svimez	Svimeza	k1gFnPc2	Svimeza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
budiž	budiž	k9	budiž
zmíněno	zmíněn	k2eAgNnSc1d1	zmíněno
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
celkové	celkový	k2eAgFnSc2d1	celková
regionální	regionální	k2eAgFnSc2d1	regionální
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
1⁄	1⁄	k?	1⁄
provinční	provinční	k2eAgNnSc1d1	provinční
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Svůdné	svůdný	k2eAgNnSc1d1	svůdné
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
Vittoria	Vittorium	k1gNnSc2	Vittorium
de	de	k?	de
Sicy	Sicy	k?	Sicy
ve	v	k7c6	v
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
"	"	kIx"	"
<g/>
Neapolský	neapolský	k2eAgInSc1d1	neapolský
záliv	záliv	k1gInSc1	záliv
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
baia	baius	k1gMnSc2	baius
di	di	k?	di
Napoli	Napole	k1gFnSc4	Napole
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnohými	mnohý	k2eAgMnPc7d1	mnohý
lidmi	člověk	k1gMnPc7	člověk
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgNnPc2d3	nejkrásnější
měst	město	k1gNnPc2	město
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
v	v	k7c6	v
Neapolském	neapolský	k2eAgInSc6d1	neapolský
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
Vesuvem	Vesuv	k1gInSc7	Vesuv
a	a	k8xC	a
vulkanickým	vulkanický	k2eAgNnSc7d1	vulkanické
územím	území	k1gNnSc7	území
Campi	Camp	k1gFnSc2	Camp
Flegrei	Flegre	k1gFnSc2	Flegre
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
s	s	k7c7	s
obzvlášť	obzvlášť	k6eAd1	obzvlášť
velkým	velký	k2eAgInSc7d1	velký
historickým	historický	k2eAgInSc7d1	historický
a	a	k8xC	a
architektonickým	architektonický	k2eAgInSc7d1	architektonický
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
domácího	domácí	k2eAgInSc2d1	domácí
i	i	k8xC	i
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
bylo	být	k5eAaImAgNnS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Neapolské	neapolský	k2eAgNnSc1d1	Neapolské
panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
zdejší	zdejší	k2eAgInPc1d1	zdejší
typické	typický	k2eAgInPc1d1	typický
produkty	produkt	k1gInPc1	produkt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
symboly	symbol	k1gInPc4	symbol
Itálie	Itálie	k1gFnSc2	Itálie
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
kolektivním	kolektivní	k2eAgNnSc6d1	kolektivní
povědomí	povědomí	k1gNnSc6	povědomí
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
starořečtině	starořečtina	k1gFnSc6	starořečtina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ν	ν	k?	ν
π	π	k?	π
<g/>
,	,	kIx,	,
nea	nea	k?	nea
polis	polis	k1gInSc1	polis
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
nové	nový	k2eAgNnSc1d1	nové
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
(	(	kIx(	(
<g/>
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
<g/>
)	)	kIx)	)
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Napolo	napolo	k6eAd1	napolo
(	(	kIx(	(
<g/>
esperanto	esperanto	k1gNnSc1	esperanto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nà	Nà	k1gFnSc1	Nà
(	(	kIx(	(
<g/>
katalánsky	katalánsky	k6eAd1	katalánsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nápoly	Nápol	k1gInPc1	Nápol
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ナ	ナ	k?	ナ
Napori	Napor	k1gFnSc2	Napor
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Napulj	Napulj	k1gFnSc1	Napulj
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Н	Н	k?	Н
<g/>
,	,	kIx,	,
Napulj	Napulj	k1gFnSc1	Napulj
(	(	kIx(	(
<g/>
srbsky	srbsky	k6eAd1	srbsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Н	Н	k?	Н
<g/>
,	,	kIx,	,
Neapal	Neapal	k1gFnSc1	Neapal
(	(	kIx(	(
<g/>
bělorusky	bělorusky	k6eAd1	bělorusky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neapel	Neapel	k1gMnSc1	Neapel
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neapelj	Neapelj	k1gFnSc1	Neapelj
(	(	kIx(	(
<g/>
slovinsky	slovinsky	k6eAd1	slovinsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neapol	Neapol	k1gFnSc1	Neapol
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Н	Н	k?	Н
<g/>
,	,	kIx,	,
Neapol	Neapol	k1gFnSc1	Neapol
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neapole	Neapol	k1gFnSc2	Neapol
(	(	kIx(	(
<g/>
lotyšsky	lotyšsky	k6eAd1	lotyšsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ν	Ν	k?	Ν
<g/>
,	,	kIx,	,
Neápolē	Neápolē	k1gFnSc1	Neápolē
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ნ	ნ	k?	ნ
<g/>
,	,	kIx,	,
Neapoli	Neapol	k1gFnSc6	Neapol
(	(	kIx(	(
<g/>
gruzínsky	gruzínsky	k6eAd1	gruzínsky
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Neapolis	Neapolis	k1gFnPc4	Neapolis
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
litevsky	litevsky	k6eAd1	litevsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Naapoli	Naapole	k1gFnSc4	Naapole
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
那	那	k?	那
Nabulesi	Nabulese	k1gFnSc4	Nabulese
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ن	ن	k?	ن
/	/	kIx~	/
ن	ن	k?	ن
Nā	Nā	k1gFnSc1	Nā
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Napels	Napels	k1gInSc1	Napels
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ن	ن	k?	ن
Nā	Nā	k1gMnSc1	Nā
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
persky	persky	k6eAd1	persky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Naples	Naples	k1gMnSc1	Naples
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Napli	Naple	k1gFnSc4	Naple
(	(	kIx(	(
<g/>
maltsky	maltsky	k6eAd1	maltsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nápoles	Nápoles	k1gMnSc1	Nápoles
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nápolés	Nápolés	k1gInSc1	Nápolés
(	(	kIx(	(
<g/>
tagalog	tagalog	k1gInSc1	tagalog
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Napolí	Napolí	k1gNnSc1	Napolí
(	(	kIx(	(
<g/>
islandsky	islandsky	k6eAd1	islandsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
נ	נ	k?	נ
Nā	Nā	k1gFnSc1	Nā
(	(	kIx(	(
<g/>
jidiš	jidiš	k1gNnSc1	jidiš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
น	น	k?	น
<g/>
ี	ี	k?	ี
Nā	Nā	k1gFnSc2	Nā
(	(	kIx(	(
<g/>
thajsky	thajsky	k6eAd1	thajsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
나	나	k?	나
Napolli	Napolle	k1gFnSc4	Napolle
(	(	kIx(	(
<g/>
korejsky	korejsky	k6eAd1	korejsky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
založeno	založit	k5eAaPmNgNnS	založit
obyvateli	obyvatel	k1gMnPc7	obyvatel
řecké	řecký	k2eAgFnSc2d1	řecká
osady	osada	k1gFnSc2	osada
Cuma	Cum	k1gInSc2	Cum
asi	asi	k9	asi
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nazývané	nazývaný	k2eAgFnSc6d1	nazývaná
zemí	zem	k1gFnSc7	zem
decumani	decuman	k1gMnPc1	decuman
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stávalo	stávat	k5eAaImAgNnS	stávat
již	již	k9	již
dříve	dříve	k6eAd2	dříve
existující	existující	k2eAgNnSc1d1	existující
město	město	k1gNnSc1	město
Partenope	Partenop	k1gMnSc5	Partenop
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
Palepolis	Palepolis	k1gFnPc4	Palepolis
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
staré	starý	k2eAgNnSc4d1	staré
město	město	k1gNnSc4	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
dnešní	dnešní	k2eAgFnSc6d1	dnešní
hoře	hora	k1gFnSc6	hora
Monte	Mont	k1gInSc5	Mont
Echia	Echius	k1gMnSc4	Echius
<g/>
;	;	kIx,	;
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
byla	být	k5eAaImAgFnS	být
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Néa-pólis	Néaólis	k1gFnSc1	Néa-pólis
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nové	nový	k2eAgNnSc4d1	nové
město	město	k1gNnSc4	město
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
326	[number]	k4	326
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sannitských	sannitský	k2eAgFnPc2d1	sannitský
válek	válka	k1gFnPc2	válka
Římané	Říman	k1gMnPc1	Říman
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Neapol	Neapol	k1gFnSc4	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
hostila	hostit	k5eAaImAgFnS	hostit
Neapol	Neapol	k1gFnSc1	Neapol
mnoho	mnoho	k6eAd1	mnoho
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
<g/>
,	,	kIx,	,
již	jenž	k3xRgMnPc1	jenž
tam	tam	k6eAd1	tam
trávili	trávit	k5eAaImAgMnP	trávit
své	svůj	k3xOyFgFnPc4	svůj
volné	volný	k2eAgFnPc4d1	volná
chvíle	chvíle	k1gFnPc4	chvíle
mezi	mezi	k7c7	mezi
vládnutím	vládnutí	k1gNnSc7	vládnutí
<g/>
,	,	kIx,	,
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
bývá	bývat	k5eAaImIp3nS	bývat
zejména	zejména	k9	zejména
velmi	velmi	k6eAd1	velmi
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
neapolský	neapolský	k2eAgInSc1d1	neapolský
pobyt	pobyt	k1gInSc1	pobyt
císaře	císař	k1gMnSc2	císař
Felixe	Felix	k1gMnSc2	Felix
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
476	[number]	k4	476
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
poslední	poslední	k2eAgMnSc1d1	poslední
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
Romulus	Romulus	k1gMnSc1	Romulus
Augustus	Augustus	k1gMnSc1	Augustus
<g/>
)	)	kIx)	)
sesazen	sesadit	k5eAaPmNgMnS	sesadit
gótským	gótský	k2eAgMnSc7d1	gótský
králem	král	k1gMnSc7	král
Odoakrem	Odoakr	k1gMnSc7	Odoakr
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
v	v	k7c4	v
Castel	Castel	k1gInSc4	Castel
dell	dell	k1gMnSc1	dell
<g/>
'	'	kIx"	'
<g/>
Ovo	Ovo	k1gMnSc1	Ovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Neapol	Neapol	k1gFnSc1	Neapol
dobyta	dobýt	k5eAaPmNgFnS	dobýt
Byzantinci	Byzantinec	k1gMnPc7	Byzantinec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Fabricia	Fabricius	k1gMnSc2	Fabricius
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
nezávislým	závislý	k2eNgInSc7d1	nezávislý
vévodstvím	vévodství	k1gNnPc3	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1137	[number]	k4	1137
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Neapol	Neapol	k1gFnSc4	Neapol
Normané	Norman	k1gMnPc1	Norman
<g/>
,	,	kIx,	,
vévodství	vévodství	k1gNnSc1	vévodství
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nastoleno	nastolen	k2eAgNnSc1d1	nastoleno
Království	království	k1gNnSc1	království
sicilské	sicilský	k2eAgNnSc1d1	sicilské
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Palermem	Palermo	k1gNnSc7	Palermo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
město	město	k1gNnSc1	město
připadlo	připadnout	k5eAaPmAgNnS	připadnout
německému	německý	k2eAgInSc3d1	německý
císařskému	císařský	k2eAgInSc3d1	císařský
rodu	rod	k1gInSc3	rod
Štaufů	Štauf	k1gMnPc2	Štauf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1266	[number]	k4	1266
papež	papež	k1gMnSc1	papež
postoupil	postoupit	k5eAaPmAgMnS	postoupit
sicilské	sicilský	k2eAgNnSc4d1	sicilské
království	království	k1gNnSc4	království
Anjouovcům	Anjouovec	k1gMnPc3	Anjouovec
(	(	kIx(	(
<g/>
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
větvi	větev	k1gFnSc6	větev
francouzského	francouzský	k2eAgInSc2d1	francouzský
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
Kapetovců	Kapetovec	k1gInPc2	Kapetovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přenesli	přenést	k5eAaPmAgMnP	přenést
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
z	z	k7c2	z
Palerma	Palermo	k1gNnSc2	Palermo
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přesun	přesun	k1gInSc1	přesun
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
přílišné	přílišný	k2eAgNnSc4d1	přílišné
množství	množství	k1gNnSc4	množství
daní	daň	k1gFnPc2	daň
způsobily	způsobit	k5eAaPmAgFnP	způsobit
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
sicilského	sicilský	k2eAgNnSc2d1	sicilské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1282	[number]	k4	1282
povstalo	povstat	k5eAaPmAgNnS	povstat
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
povstání	povstání	k1gNnSc3	povstání
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xS	jako
Sicilské	sicilský	k2eAgFnSc2d1	sicilská
nešpory	nešpora	k1gFnSc2	nešpora
(	(	kIx(	(
<g/>
vespri	vespri	k6eAd1	vespri
siciliani	siciliaň	k1gFnSc3	siciliaň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1284	[number]	k4	1284
aragonský	aragonský	k2eAgMnSc1d1	aragonský
král	král	k1gMnSc1	král
Petr	Petr	k1gMnSc1	Petr
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
spojenectví	spojenectví	k1gNnSc4	spojenectví
se	s	k7c7	s
Sicilany	Sicilan	k1gMnPc7	Sicilan
<g/>
,	,	kIx,	,
připojil	připojit	k5eAaPmAgInS	připojit
ostrov	ostrov	k1gInSc1	ostrov
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
království	království	k1gNnSc3	království
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1442	[number]	k4	1442
Aragonci	Aragonek	k1gMnPc1	Aragonek
dobyli	dobýt	k5eAaPmAgMnP	dobýt
také	také	k9	také
Království	království	k1gNnSc4	království
napolské	napolský	k2eAgNnSc4d1	napolský
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1501	[number]	k4	1501
bylo	být	k5eAaImAgNnS	být
Neapolské	neapolský	k2eAgNnSc1d1	Neapolské
království	království	k1gNnSc1	království
dobyto	dobyt	k2eAgNnSc1d1	dobyto
Španěly	Španěl	k1gMnPc7	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
válek	válka	k1gFnPc2	válka
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
španělské	španělský	k2eAgNnSc1d1	španělské
Rakousko	Rakousko	k1gNnSc1	Rakousko
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
Neapol	Neapol	k1gFnSc1	Neapol
(	(	kIx(	(
<g/>
1707	[number]	k4	1707
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlastnilo	vlastnit	k5eAaImAgNnS	vlastnit
ji	on	k3xPp3gFnSc4	on
až	až	k9	až
do	do	k7c2	do
r.	r.	kA	r.
1734	[number]	k4	1734
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
království	království	k1gNnSc1	království
stalo	stát	k5eAaPmAgNnS	stát
opět	opět	k6eAd1	opět
nezávislým	závislý	k2eNgInSc7d1	nezávislý
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
rodu	rod	k1gInSc2	rod
Bourbonů	bourbon	k1gInPc2	bourbon
se	se	k3xPyFc4	se
Neapol	Neapol	k1gFnSc1	Neapol
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
evropských	evropský	k2eAgFnPc2d1	Evropská
metropolí	metropol	k1gFnPc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Neapol	Neapol	k1gFnSc1	Neapol
dobyta	dobyt	k2eAgFnSc1d1	dobyta
oddíly	oddíl	k1gInPc4	oddíl
Francouzů	Francouz	k1gMnPc2	Francouz
vedenými	vedený	k2eAgFnPc7d1	vedená
generálem	generál	k1gMnSc7	generál
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
císařem	císař	k1gMnSc7	císař
<g/>
)	)	kIx)	)
Napoleonem	napoleon	k1gInSc7	napoleon
Bonapartem	bonapart	k1gInSc7	bonapart
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
později	pozdě	k6eAd2	pozdě
svěřil	svěřit	k5eAaPmAgMnS	svěřit
království	království	k1gNnSc4	království
(	(	kIx(	(
<g/>
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
)	)	kIx)	)
svému	svůj	k1gMnSc3	svůj
bratrovi	bratr	k1gMnSc3	bratr
Josefovi	Josef	k1gMnSc3	Josef
a	a	k8xC	a
poté	poté	k6eAd1	poté
svému	svůj	k3xOyFgMnSc3	svůj
švagrovi	švagr	k1gMnSc3	švagr
maršálu	maršál	k1gMnSc3	maršál
Muratovi	Murat	k1gMnSc3	Murat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
<g/>
,	,	kIx,	,
po	po	k7c6	po
Muratově	Muratův	k2eAgFnSc6d1	Muratova
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
Neapolské	neapolský	k2eAgFnSc6d1	neapolská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Neapol	Neapol	k1gFnSc1	Neapol
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Bourbonů	bourbon	k1gInPc2	bourbon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
bylo	být	k5eAaImAgNnS	být
Království	království	k1gNnSc1	království
obojí	oboj	k1gFnPc2	oboj
Sicílie	Sicílie	k1gFnSc2	Sicílie
dobyto	dobýt	k5eAaPmNgNnS	dobýt
Tisícovkou	tisícovka	k1gFnSc7	tisícovka
Giuseppa	Giusepp	k1gMnSc2	Giusepp
Garibaldiho	Garibaldi	k1gMnSc2	Garibaldi
a	a	k8xC	a
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
Italskému	italský	k2eAgNnSc3d1	italské
království	království	k1gNnSc3	království
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vlivem	vlivem	k7c2	vlivem
špatných	špatný	k2eAgFnPc2d1	špatná
hygienických	hygienický	k2eAgFnPc2d1	hygienická
podmínek	podmínka	k1gFnPc2	podmínka
epidemie	epidemie	k1gFnSc2	epidemie
cholery	cholera	k1gFnSc2	cholera
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
30	[number]	k4	30
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Neapol	Neapol	k1gFnSc4	Neapol
smutně	smutně	k6eAd1	smutně
proslula	proslout	k5eAaPmAgFnS	proslout
tzv.	tzv.	kA	tzv.
odpadkovou	odpadkový	k2eAgFnSc7d1	odpadková
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vedení	vedení	k1gNnSc1	vedení
města	město	k1gNnSc2	město
ani	ani	k8xC	ani
vyšší	vysoký	k2eAgInPc1d2	vyšší
státní	státní	k2eAgInPc1d1	státní
úřady	úřad	k1gInPc1	úřad
nebyly	být	k5eNaImAgInP	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
schopny	schopen	k2eAgInPc1d1	schopen
pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
kapacit	kapacita	k1gFnPc2	kapacita
skládek	skládka	k1gFnPc2	skládka
a	a	k8xC	a
spaloven	spalovna	k1gFnPc2	spalovna
zajistit	zajistit	k5eAaPmF	zajistit
odvoz	odvoz	k1gInSc4	odvoz
odpadků	odpadek	k1gInPc2	odpadek
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
hromadily	hromadit	k5eAaImAgInP	hromadit
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Neapol	Neapol	k1gFnSc1	Neapol
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
turistů	turist	k1gMnPc2	turist
jako	jako	k8xS	jako
výchozí	výchozí	k2eAgInSc4d1	výchozí
bod	bod	k1gInSc4	bod
pro	pro	k7c4	pro
návštěvu	návštěva	k1gFnSc4	návštěva
pamětihodností	pamětihodnost	k1gFnPc2	pamětihodnost
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Pompeje	Pompeje	k1gInPc1	Pompeje
<g/>
,	,	kIx,	,
Královská	královský	k2eAgFnSc1d1	královská
rezidence	rezidence	k1gFnSc1	rezidence
Caserta	Caserta	k1gFnSc1	Caserta
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
Capri	Capri	k1gNnSc2	Capri
<g/>
,	,	kIx,	,
Ischia	Ischium	k1gNnSc2	Ischium
či	či	k8xC	či
pobřeží	pobřeží	k1gNnSc2	pobřeží
Amalfitana	Amalfitan	k1gMnSc2	Amalfitan
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
město	město	k1gNnSc1	město
samotné	samotný	k2eAgNnSc1d1	samotné
je	být	k5eAaImIp3nS	být
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
umělecké	umělecký	k2eAgNnSc4d1	umělecké
a	a	k8xC	a
architektonické	architektonický	k2eAgNnSc4d1	architektonické
dědictví	dědictví	k1gNnSc4	dědictví
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
vyhledávané	vyhledávaný	k2eAgFnPc1d1	vyhledávaná
i	i	k9	i
díky	díky	k7c3	díky
každoročnímu	každoroční	k2eAgNnSc3d1	každoroční
květnu	květno	k1gNnSc3	květno
pamětihodností	pamětihodnost	k1gFnPc2	pamětihodnost
(	(	kIx(	(
<g/>
Maggio	Maggio	k6eAd1	Maggio
dei	dei	k?	dei
Monumenti	Monument	k1gMnPc1	Monument
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
turistickým	turistický	k2eAgFnPc3d1	turistická
a	a	k8xC	a
kulturním	kulturní	k2eAgFnPc3d1	kulturní
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Neapol	Neapol	k1gFnSc1	Neapol
je	být	k5eAaImIp3nS	být
proslulá	proslulý	k2eAgFnSc1d1	proslulá
svými	svůj	k3xOyFgInPc7	svůj
hrady	hrad	k1gInPc7	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
tu	tu	k6eAd1	tu
Castel	Castel	k1gMnSc1	Castel
dell	dell	k1gMnSc1	dell
<g/>
'	'	kIx"	'
<g/>
Ovo	Ovo	k1gMnSc1	Ovo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hrad	hrad	k1gInSc1	hrad
vejce	vejce	k1gNnSc1	vejce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
slavného	slavný	k2eAgNnSc2d1	slavné
panoramatu	panorama	k1gNnSc2	panorama
zálivu	záliv	k1gInSc2	záliv
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
Kapujský	Kapujský	k2eAgInSc1d1	Kapujský
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
vystavěný	vystavěný	k2eAgInSc1d1	vystavěný
Vilémem	Vilém	k1gMnSc7	Vilém
I.	I.	kA	I.
Zlým	zlý	k1gMnSc7	zlý
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
konci	konec	k1gInSc6	konec
bývalé	bývalý	k2eAgFnSc2d1	bývalá
hlavní	hlavní	k2eAgFnSc2d1	hlavní
starověké	starověký	k2eAgFnSc2d1	starověká
východo-západní	východoápadní	k2eAgFnSc2d1	východo-západní
komunikace	komunikace	k1gFnSc2	komunikace
(	(	kIx(	(
<g/>
decumanus	decumanus	k1gMnSc1	decumanus
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Via	via	k7c4	via
Decumana	Decuman	k1gMnSc4	Decuman
<g/>
)	)	kIx)	)
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
ulice	ulice	k1gFnSc1	ulice
Via	via	k7c4	via
dei	dei	k?	dei
Tribunali	Tribunali	k1gFnSc4	Tribunali
<g/>
;	;	kIx,	;
také	také	k9	také
Castel	Castel	k1gInSc1	Castel
Nuovo	Nuovo	k1gNnSc1	Nuovo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Nový	nový	k2eAgInSc1d1	nový
hrad	hrad	k1gInSc1	hrad
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
vévodí	vévodit	k5eAaImIp3nS	vévodit
Radničnímu	radniční	k2eAgNnSc3d1	radniční
náměstí	náměstí	k1gNnSc3	náměstí
(	(	kIx(	(
<g/>
Piazza	Piazza	k1gFnSc1	Piazza
del	del	k?	del
Municipio	Municipio	k1gNnSc1	Municipio
<g/>
)	)	kIx)	)
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
Castel	Castel	k1gMnSc1	Castel
Sant	Sant	k1gMnSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Elmo	Elmo	k1gMnSc1	Elmo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hrad	hrad	k1gInSc1	hrad
sv.	sv.	kA	sv.
Eliáše	Eliáš	k1gMnSc2	Eliáš
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tyčící	tyčící	k2eAgMnSc1d1	tyčící
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
kopce	kopec	k1gInSc2	kopec
Vomero	Vomero	k1gNnSc1	Vomero
společně	společně	k6eAd1	společně
s	s	k7c7	s
kartouzou	kartouza	k1gFnSc7	kartouza
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Castel	Castel	k1gMnSc1	Castel
dell	dell	k1gMnSc1	dell
<g/>
'	'	kIx"	'
<g/>
Ovo	Ovo	k1gFnSc1	Ovo
-	-	kIx~	-
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
středověké	středověký	k2eAgFnSc2d1	středověká
pověsti	pověst	k1gFnSc2	pověst
tvrdící	tvrdící	k2eAgFnSc2d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
antický	antický	k2eAgMnSc1d1	antický
básník	básník	k1gMnSc1	básník
Vergilius	Vergilius	k1gMnSc1	Vergilius
byl	být	k5eAaImAgMnS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
velkým	velký	k2eAgMnSc7d1	velký
kouzelníkem	kouzelník	k1gMnSc7	kouzelník
a	a	k8xC	a
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
čarovné	čarovný	k2eAgNnSc4d1	čarovné
vejce	vejce	k1gNnSc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
prý	prý	k9	prý
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
do	do	k7c2	do
základů	základ	k1gInPc2	základ
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
rozbito	rozbít	k5eAaPmNgNnS	rozbít
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc4	hrad
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zřítil	zřítit	k5eAaPmAgInS	zřítit
a	a	k8xC	a
městu	město	k1gNnSc3	město
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
přineslo	přinést	k5eAaPmAgNnS	přinést
zkázu	zkáza	k1gFnSc4	zkáza
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
nejstaršího	starý	k2eAgNnSc2d3	nejstarší
osídlení	osídlení	k1gNnSc2	osídlení
zde	zde	k6eAd1	zde
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
osadníci	osadník	k1gMnPc1	osadník
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Rhodu	Rhodos	k1gInSc2	Rhodos
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
své	svůj	k3xOyFgFnSc6	svůj
obchodní	obchodní	k2eAgFnSc6d1	obchodní
osadě	osada	k1gFnSc6	osada
dali	dát	k5eAaPmAgMnP	dát
jméno	jméno	k1gNnSc4	jméno
Parthenopé	Parthenopý	k2eAgFnSc2d1	Parthenopý
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
tam	tam	k6eAd1	tam
římský	římský	k2eAgMnSc1d1	římský
konzul	konzul	k1gMnSc1	konzul
L.	L.	kA	L.
L.	L.	kA	L.
Lucullus	Lucullus	k1gMnSc1	Lucullus
zbudoval	zbudovat	k5eAaPmAgMnS	zbudovat
výstavnou	výstavný	k2eAgFnSc4d1	výstavná
villu	villa	k1gFnSc4	villa
(	(	kIx(	(
<g/>
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
místo	místo	k1gNnSc1	místo
zváno	zván	k2eAgNnSc1d1	zváno
Castellum	Castellum	k1gNnSc1	Castellum
Lucullanum	Lucullanum	k1gInSc1	Lucullanum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
císařem	císař	k1gMnSc7	císař
Valentinianem	Valentinian	k1gMnSc7	Valentinian
III	III	kA	III
<g/>
.	.	kIx.	.
opevněnou	opevněný	k2eAgFnSc4d1	opevněná
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
476	[number]	k4	476
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgInS	být
vykázán	vykázat	k5eAaPmNgInS	vykázat
na	na	k7c4	na
dožití	dožití	k1gNnSc4	dožití
sesazený	sesazený	k2eAgMnSc1d1	sesazený
poslední	poslední	k2eAgMnSc1d1	poslední
císař	císař	k1gMnSc1	císař
římský	římský	k2eAgMnSc1d1	římský
<g/>
,	,	kIx,	,
Romulus	Romulus	k1gMnSc1	Romulus
Augustus	Augustus	k1gMnSc1	Augustus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
tu	tu	k6eAd1	tu
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
různých	různý	k2eAgFnPc6d1	různá
peripetiích	peripetie	k1gFnPc6	peripetie
byl	být	k5eAaImAgMnS	být
ve	v	k7c4	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Normany	Norman	k1gMnPc4	Norman
postaven	postaven	k2eAgInSc4d1	postaven
středověký	středověký	k2eAgInSc4d1	středověký
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
aragonskými	aragonský	k2eAgMnPc7d1	aragonský
panovníky	panovník	k1gMnPc7	panovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
konají	konat	k5eAaImIp3nP	konat
výstavy	výstava	k1gFnSc2	výstava
a	a	k8xC	a
sympozia	sympozion	k1gNnSc2	sympozion
<g/>
;	;	kIx,	;
vstup	vstup	k1gInSc1	vstup
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
je	být	k5eAaImIp3nS	být
volný	volný	k2eAgInSc1d1	volný
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
působivá	působivý	k2eAgFnSc1d1	působivá
je	být	k5eAaImIp3nS	být
rybářská	rybářský	k2eAgFnSc1d1	rybářská
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
na	na	k7c6	na
základech	základ	k1gInPc6	základ
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Kapujský	Kapujský	k2eAgInSc1d1	Kapujský
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
Castel	Castel	k1gInSc1	Castel
Capuano	Capuana	k1gFnSc5	Capuana
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1153	[number]	k4	1153
Vilémem	Vilém	k1gMnSc7	Vilém
I.	I.	kA	I.
Sicilským	sicilský	k2eAgNnSc7d1	sicilské
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
zamýšlen	zamýšlet	k5eAaImNgInS	zamýšlet
jako	jako	k8xS	jako
královské	královský	k2eAgNnSc1d1	královské
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
,	,	kIx,	,
zažil	zažít	k5eAaPmAgMnS	zažít
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
z	z	k7c2	z
dvorního	dvorní	k2eAgNnSc2d1	dvorní
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
Sicilské	sicilský	k2eAgNnSc1d1	sicilské
království	království	k1gNnSc1	království
mělo	mít	k5eAaImAgNnS	mít
totiž	totiž	k9	totiž
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
v	v	k7c6	v
Palermu	Palermo	k1gNnSc6	Palermo
<g/>
.	.	kIx.	.
</s>
<s>
Římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
král	král	k1gMnSc1	král
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štaufský	Štaufský	k2eAgMnSc1d1	Štaufský
se	se	k3xPyFc4	se
často	často	k6eAd1	často
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
zdržoval	zdržovat	k5eAaImAgMnS	zdržovat
a	a	k8xC	a
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
strategicky	strategicky	k6eAd1	strategicky
umístěnému	umístěný	k2eAgInSc3d1	umístěný
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
přístupové	přístupový	k2eAgFnSc6d1	přístupová
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaImAgMnS	věnovat
svou	svůj	k3xOyFgFnSc4	svůj
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
vládců	vládce	k1gMnPc2	vládce
z	z	k7c2	z
aragonské	aragonský	k2eAgFnSc2d1	Aragonská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Kapujská	Kapujský	k2eAgFnSc1d1	Kapujský
brána	brána	k1gFnSc1	brána
situována	situovat	k5eAaBmNgFnS	situovat
přímo	přímo	k6eAd1	přímo
před	před	k7c4	před
Kapujský	Kapujský	k2eAgInSc4d1	Kapujský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
dnešní	dnešní	k2eAgNnPc1d1	dnešní
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgNnPc1d1	renesanční
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
nedaleko	nedaleko	k7c2	nedaleko
sochy	socha	k1gFnSc2	socha
krále	král	k1gMnSc2	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Aragonského	aragonský	k2eAgMnSc2d1	aragonský
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc4	dílo
mistra	mistr	k1gMnSc2	mistr
Giuliana	Giulian	k1gMnSc2	Giulian
da	da	k?	da
Maiano	Maiana	k1gFnSc5	Maiana
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
španělského	španělský	k2eAgMnSc4d1	španělský
místokrále	místokrál	k1gMnSc4	místokrál
Dona	Don	k1gMnSc4	Don
Pedra	Pedr	k1gMnSc4	Pedr
z	z	k7c2	z
Toleda	Toledo	k1gNnSc2	Toledo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
s	s	k7c7	s
hradem	hrad	k1gInSc7	hrad
spojeny	spojen	k2eAgFnPc1d1	spojena
královské	královský	k2eAgFnPc1d1	královská
soudní	soudní	k2eAgFnPc1d1	soudní
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
500	[number]	k4	500
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
Kapujského	Kapujský	k2eAgInSc2d1	Kapujský
hradu	hrad	k1gInSc2	hrad
stalo	stát	k5eAaPmAgNnS	stát
synonymem	synonymum	k1gNnSc7	synonymum
soudního	soudní	k2eAgInSc2d1	soudní
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
před	před	k7c7	před
několika	několik	k4yIc7	několik
lety	léto	k1gNnPc7	léto
započalo	započnout	k5eAaPmAgNnS	započnout
stěhování	stěhování	k1gNnSc1	stěhování
Úřadu	úřad	k1gInSc2	úřad
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
do	do	k7c2	do
moderních	moderní	k2eAgInPc2d1	moderní
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
Řídícího	řídící	k2eAgNnSc2d1	řídící
centra	centrum	k1gNnSc2	centrum
(	(	kIx(	(
<g/>
Centro	Centro	k1gNnSc4	Centro
direzionale	direzionale	k6eAd1	direzionale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obchodně-kancelářského	obchodněancelářský	k2eAgInSc2d1	obchodně-kancelářský
okrsku	okrsek	k1gInSc2	okrsek
<g/>
,	,	kIx,	,
zbudovaného	zbudovaný	k2eAgNnSc2d1	zbudované
východně	východně	k6eAd1	východně
od	od	k7c2	od
starého	starý	k2eAgNnSc2d1	staré
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Maschio	Maschio	k6eAd1	Maschio
Angioino	Angioino	k1gNnSc4	Angioino
(	(	kIx(	(
<g/>
správněji	správně	k6eAd2	správně
Castel	Castel	k1gInSc1	Castel
Nuovo	Nuovo	k1gNnSc1	Nuovo
<g/>
)	)	kIx)	)
-	-	kIx~	-
zbudován	zbudován	k2eAgInSc1d1	zbudován
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1279	[number]	k4	1279
<g/>
-	-	kIx~	-
<g/>
1282	[number]	k4	1282
Karlem	Karel	k1gMnSc7	Karel
I.	I.	kA	I.
z	z	k7c2	z
Anjou	Anja	k1gMnSc7	Anja
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
určil	určit	k5eAaPmAgMnS	určit
za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
palác	palác	k1gInSc4	palác
své	svůj	k3xOyFgFnSc2	svůj
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
dob	doba	k1gFnPc2	doba
jeho	jeho	k3xOp3gMnSc2	jeho
vnuka	vnuk	k1gMnSc2	vnuk
Roberta	Robert	k1gMnSc2	Robert
Moudrého	moudrý	k2eAgMnSc2d1	moudrý
tam	tam	k6eAd1	tam
pobývali	pobývat	k5eAaImAgMnP	pobývat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
i	i	k9	i
básníci	básník	k1gMnPc1	básník
Francesco	Francesco	k1gMnSc1	Francesco
Petrarca	Petrarca	k1gMnSc1	Petrarca
a	a	k8xC	a
Giovanni	Giovann	k1gMnPc1	Giovann
Boccaccio	Boccaccio	k6eAd1	Boccaccio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
aragonském	aragonský	k2eAgInSc6d1	aragonský
vpádu	vpád	k1gInSc6	vpád
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
zcela	zcela	k6eAd1	zcela
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
a	a	k8xC	a
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
stavby	stavba	k1gFnSc2	stavba
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
.	.	kIx.	.
</s>
<s>
Mramorový	mramorový	k2eAgInSc1d1	mramorový
vítězný	vítězný	k2eAgInSc1d1	vítězný
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
středovou	středový	k2eAgFnSc7d1	středová
a	a	k8xC	a
rohovou	rohový	k2eAgFnSc7d1	rohová
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Francescem	Francesce	k1gMnSc7	Francesce
Lauranou	Lauraný	k2eAgFnSc4d1	Lauraný
jako	jako	k9	jako
připomínka	připomínka	k1gFnSc1	připomínka
proslulého	proslulý	k2eAgInSc2d1	proslulý
vstupu	vstup	k1gInSc2	vstup
Alfonse	Alfons	k1gMnSc2	Alfons
V.	V.	kA	V.
(	(	kIx(	(
<g/>
I.	I.	kA	I.
<g/>
)	)	kIx)	)
Aragonského	aragonský	k2eAgMnSc2d1	aragonský
do	do	k7c2	do
města	město	k1gNnSc2	město
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1443	[number]	k4	1443
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
příkop	příkop	k1gInSc1	příkop
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
vysušený	vysušený	k2eAgMnSc1d1	vysušený
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
legendy	legenda	k1gFnSc2	legenda
prý	prý	k9	prý
hostil	hostit	k5eAaImAgInS	hostit
krokodýla	krokodýl	k1gMnSc4	krokodýl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žral	žrát	k5eAaImAgMnS	žrát
nešťastníky	nešťastník	k1gMnPc4	nešťastník
zavřené	zavřený	k2eAgNnSc4d1	zavřené
v	v	k7c6	v
kobkách	kobka	k1gFnPc6	kobka
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Krokodýlí	krokodýlí	k2eAgInSc1d1	krokodýlí
příkop	příkop	k1gInSc1	příkop
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Monumentální	monumentální	k2eAgInSc1d1	monumentální
"	"	kIx"	"
<g/>
Sál	sál	k1gInSc1	sál
baronů	baron	k1gMnPc2	baron
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
konají	konat	k5eAaImIp3nP	konat
schůze	schůze	k1gFnPc1	schůze
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
býval	bývat	k5eAaImAgMnS	bývat
také	také	k9	také
ústřední	ústřední	k2eAgFnSc7d1	ústřední
místností	místnost	k1gFnSc7	místnost
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
takto	takto	k6eAd1	takto
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1487	[number]	k4	1487
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
baroni	baron	k1gMnPc1	baron
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
spikli	spiknout	k5eAaPmAgMnP	spiknout
proti	proti	k7c3	proti
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
Aragonskému	aragonský	k2eAgMnSc3d1	aragonský
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
hlavním	hlavní	k2eAgInSc6d1	hlavní
sále	sál	k1gInSc6	sál
Pietro	Pietro	k1gNnSc1	Pietro
da	da	k?	da
Morrone	Morron	k1gMnSc5	Morron
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
papež	papež	k1gMnSc1	papež
Celestin	Celestin	k1gMnSc1	Celestin
V.	V.	kA	V.
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1294	[number]	k4	1294
"	"	kIx"	"
<g/>
ze	z	k7c2	z
zbabělosti	zbabělost	k1gFnSc2	zbabělost
způsobil	způsobit	k5eAaPmAgMnS	způsobit
veliký	veliký	k2eAgInSc4d1	veliký
odpor	odpor	k1gInSc4	odpor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Dante	Dante	k1gMnSc1	Dante
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
otevřel	otevřít	k5eAaPmAgMnS	otevřít
cestu	cesta	k1gFnSc4	cesta
pro	pro	k7c4	pro
příjezd	příjezd	k1gInSc4	příjezd
Bonifáce	Bonifác	k1gMnSc2	Bonifác
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
po	po	k7c6	po
konkláve	konkláve	k1gNnSc6	konkláve
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
místnosti	místnost	k1gFnSc6	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
sv.	sv.	kA	sv.
Eliáše	Eliáš	k1gMnSc2	Eliáš
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
zvaný	zvaný	k2eAgInSc1d1	zvaný
Belforte	Belfort	k1gInSc5	Belfort
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
kopce	kopec	k1gInSc2	kopec
Vomero	Vomero	k1gNnSc1	Vomero
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
okolo	okolo	k7c2	okolo
r.	r.	kA	r.
1275	[number]	k4	1275
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dal	dát	k5eAaPmAgMnS	dát
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
stavbě	stavba	k1gFnSc3	stavba
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Anjou	Anja	k1gFnSc7	Anja
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
přestavěn	přestavěn	k2eAgMnSc1d1	přestavěn
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1538	[number]	k4	1538
<g/>
-	-	kIx~	-
<g/>
1546	[number]	k4	1546
místokrálem	místokrál	k1gMnSc7	místokrál
Pedrem	Pedr	k1gMnSc7	Pedr
z	z	k7c2	z
Toleda	Toledo	k1gNnSc2	Toledo
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ale	ale	k9	ale
zachoval	zachovat	k5eAaPmAgMnS	zachovat
jeho	on	k3xPp3gInSc4	on
hvězdicový	hvězdicový	k2eAgInSc4d1	hvězdicový
půdorys	půdorys	k1gInSc4	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc4	hrad
byl	být	k5eAaImAgMnS	být
dějištěm	dějiště	k1gNnSc7	dějiště
posledního	poslední	k2eAgInSc2d1	poslední
zoufalého	zoufalý	k2eAgInSc2d1	zoufalý
odporu	odpor	k1gInSc2	odpor
zastánců	zastánce	k1gMnPc2	zastánce
Parthenopské	Parthenopský	k2eAgFnSc2d1	Parthenopský
republiky	republika	k1gFnSc2	republika
proti	proti	k7c3	proti
Bourbonům	bourbon	k1gInPc3	bourbon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
prostornosti	prostornost	k1gFnSc3	prostornost
<g/>
,	,	kIx,	,
impozantnosti	impozantnost	k1gFnSc3	impozantnost
a	a	k8xC	a
nádhernému	nádherný	k2eAgInSc3d1	nádherný
výhledu	výhled	k1gInSc3	výhled
na	na	k7c4	na
panoráma	panoráma	k1gNnSc4	panoráma
města	město	k1gNnSc2	město
dějištěm	dějiště	k1gNnSc7	dějiště
různých	různý	k2eAgFnPc2d1	různá
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
opěrným	opěrný	k2eAgInSc7d1	opěrný
bodem	bod	k1gInSc7	bod
neapolské	neapolský	k2eAgFnSc2d1	neapolská
moci	moc	k1gFnSc2	moc
od	od	k7c2	od
r.	r.	kA	r.
1600	[number]	k4	1600
do	do	k7c2	do
r.	r.	kA	r.
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
místokrále	místokrál	k1gMnSc2	místokrál
Fernanda	Fernando	k1gNnSc2	Fernando
Ruíze	Ruíze	k1gFnSc2	Ruíze
de	de	k?	de
Castra	Castr	k1gMnSc2	Castr
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
architekta	architekt	k1gMnSc2	architekt
Domenica	Domenicus	k1gMnSc2	Domenicus
Fontany	Fontan	k1gInPc1	Fontan
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
přebudován	přebudovat	k5eAaPmNgMnS	přebudovat
dalšími	další	k2eAgMnPc7d1	další
vládci	vládce	k1gMnPc7	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Sály	Sála	k1gFnPc1	Sála
jsou	být	k5eAaImIp3nP	být
přepychově	přepychově	k6eAd1	přepychově
zařízené	zařízený	k2eAgFnPc1d1	zařízená
a	a	k8xC	a
zdobené	zdobený	k2eAgFnPc1d1	zdobená
freskami	freska	k1gFnPc7	freska
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
stylech	styl	k1gInPc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
nádherné	nádherný	k2eAgNnSc1d1	nádherné
je	být	k5eAaImIp3nS	být
mramorové	mramorový	k2eAgNnSc1d1	mramorové
"	"	kIx"	"
<g/>
Schodiště	schodiště	k1gNnSc1	schodiště
cti	čest	k1gFnSc2	čest
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Exotická	exotický	k2eAgFnSc1d1	exotická
zahrada	zahrada	k1gFnSc1	zahrada
byla	být	k5eAaImAgFnS	být
vysazena	vysazen	k2eAgFnSc1d1	vysazena
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
obohaceno	obohatit	k5eAaPmNgNnS	obohatit
o	o	k7c4	o
veliké	veliký	k2eAgFnPc4d1	veliká
sochy	socha	k1gFnPc4	socha
vládců	vládce	k1gMnPc2	vládce
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
:	:	kIx,	:
Roger	Roger	k1gInSc1	Roger
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Sicilský	sicilský	k2eAgMnSc1d1	sicilský
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štaufský	Štaufský	k2eAgMnSc1d1	Štaufský
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
z	z	k7c2	z
Anjou	Anja	k1gMnSc7	Anja
<g/>
,	,	kIx,	,
Alfons	Alfons	k1gMnSc1	Alfons
V.	V.	kA	V.
Aragonský	aragonský	k2eAgMnSc1d1	aragonský
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
<g/>
,	,	kIx,	,
Joachim	Joachim	k1gMnSc1	Joachim
Murat	Murat	k1gInSc1	Murat
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Emanuel	Emanuel	k1gMnSc1	Emanuel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Savojský	savojský	k2eAgInSc1d1	savojský
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
rezidence	rezidence	k1gFnSc1	rezidence
Capodimonte	Capodimont	k1gInSc5	Capodimont
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
existujícím	existující	k2eAgInSc6d1	existující
loveckém	lovecký	k2eAgInSc6d1	lovecký
revíru	revír	k1gInSc6	revír
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
pahorku	pahorek	k1gInSc6	pahorek
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pobývali	pobývat	k5eAaImAgMnP	pobývat
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Joachim	Joachim	k1gInSc1	Joachim
Murat	Murat	k2eAgInSc1d1	Murat
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
zasvěcena	zasvětit	k5eAaPmNgFnS	zasvětit
Národnímu	národní	k2eAgNnSc3d1	národní
muzeu	muzeum	k1gNnSc3	muzeum
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
zamýšleno	zamýšlen	k2eAgNnSc1d1	zamýšleno
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
díla	dílo	k1gNnPc4	dílo
od	od	k7c2	od
Michelangela	Michelangel	k1gMnSc2	Michelangel
<g/>
,	,	kIx,	,
Rafaella	Rafaell	k1gMnSc2	Rafaell
<g/>
,	,	kIx,	,
Botticelliho	Botticelli	k1gMnSc2	Botticelli
<g/>
,	,	kIx,	,
Caravaggia	Caravaggius	k1gMnSc2	Caravaggius
a	a	k8xC	a
Tiziana	Tizian	k1gMnSc2	Tizian
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
významná	významný	k2eAgFnSc1d1	významná
sbírka	sbírka	k1gFnSc1	sbírka
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dokola	dokola	k6eAd1	dokola
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zelené	zelený	k2eAgFnPc4d1	zelená
plíce	plíce	k1gFnPc4	plíce
města	město	k1gNnSc2	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
cílem	cíl	k1gInSc7	cíl
místních	místní	k2eAgFnPc2d1	místní
rodin	rodina	k1gFnPc2	rodina
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
archeologické	archeologický	k2eAgNnSc1d1	Archeologické
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Bourbonské	bourbonský	k2eAgNnSc4d1	Bourbonské
královské	královský	k2eAgNnSc4d1	královské
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nejdříve	dříve	k6eAd3	dříve
zamýšleno	zamýšlet	k5eAaImNgNnS	zamýšlet
na	na	k7c6	na
pahorku	pahorek	k1gInSc6	pahorek
Capodimonte	Capodimont	k1gInSc5	Capodimont
jako	jako	k8xS	jako
výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
pro	pro	k7c4	pro
sbírku	sbírka	k1gFnSc4	sbírka
tzv.	tzv.	kA	tzv.
farnesiánských	farnesiánský	k2eAgInPc2d1	farnesiánský
mramorů	mramor	k1gInPc2	mramor
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
římských	římský	k2eAgFnPc2d1	římská
soch	socha	k1gFnPc2	socha
-	-	kIx~	-
kopií	kopie	k1gFnPc2	kopie
řeckých	řecký	k2eAgMnPc2d1	řecký
bronzových	bronzový	k2eAgMnPc2d1	bronzový
originálů	originál	k1gMnPc2	originál
-	-	kIx~	-
sesbíraných	sesbíraný	k2eAgMnPc2d1	sesbíraný
během	během	k7c2	během
věků	věk	k1gInPc2	věk
šlechtickým	šlechtický	k2eAgInSc7d1	šlechtický
rodem	rod	k1gInSc7	rod
Farnese	Farnese	k1gFnSc2	Farnese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
zdědil	zdědit	k5eAaPmAgInS	zdědit
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
matce	matka	k1gFnSc6	matka
Alžbětě	Alžběta	k1gFnSc6	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
objevu	objev	k1gInSc6	objev
měst	město	k1gNnPc2	město
zasypaných	zasypaný	k2eAgFnPc2d1	zasypaná
výbuchem	výbuch	k1gInSc7	výbuch
Vesuvu	Vesuv	k1gInSc2	Vesuv
<g/>
,	,	kIx,	,
Pompejí	Pompeje	k1gFnPc2	Pompeje
a	a	k8xC	a
Herculanea	Herculane	k1gInSc2	Herculane
<g/>
,	,	kIx,	,
přimělo	přimět	k5eAaPmAgNnS	přimět
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
zachráněných	zachráněný	k2eAgInPc2d1	zachráněný
nálezů	nález	k1gInPc2	nález
a	a	k8xC	a
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
výbuchu	výbuch	k1gInSc2	výbuch
panovníka	panovník	k1gMnSc2	panovník
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
tzv.	tzv.	kA	tzv.
Muzea	muzeum	k1gNnSc2	muzeum
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
v	v	k7c4	v
Largo	largo	k1gNnSc4	largo
delle	dell	k1gMnSc5	dell
Pigne	Pign	k1gMnSc5	Pign
<g/>
,	,	kIx,	,
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
sloužící	sloužící	k2eAgNnSc1d1	sloužící
jako	jako	k8xS	jako
kasárny	kasárna	k1gNnPc7	kasárna
kavalerie	kavalerie	k1gFnSc2	kavalerie
a	a	k8xC	a
poté	poté	k6eAd1	poté
dočasně	dočasně	k6eAd1	dočasně
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc4	sídlo
Neapolské	neapolský	k2eAgFnSc2d1	neapolská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Adaptační	adaptační	k2eAgFnPc1d1	adaptační
práce	práce	k1gFnPc1	práce
se	se	k3xPyFc4	se
protáhly	protáhnout	k5eAaPmAgFnP	protáhnout
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
až	až	k9	až
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
muzeum	muzeum	k1gNnSc4	muzeum
poprvé	poprvé	k6eAd1	poprvé
spatřil	spatřit	k5eAaPmAgMnS	spatřit
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
kráse	krása	k1gFnSc6	krása
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tři	tři	k4xCgFnPc4	tři
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
kolekce	kolekce	k1gFnPc4	kolekce
předmětů	předmět	k1gInPc2	předmět
<g/>
:	:	kIx,	:
krom	krom	k7c2	krom
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
farnesiánských	farnesiánský	k2eAgInPc2d1	farnesiánský
mramorů	mramor	k1gInPc2	mramor
a	a	k8xC	a
artefaktů	artefakt	k1gInPc2	artefakt
vykopaných	vykopaný	k2eAgInPc2d1	vykopaný
z	z	k7c2	z
Pompejí	Pompeje	k1gFnPc2	Pompeje
a	a	k8xC	a
Herkulanea	Herkulaneum	k1gNnSc2	Herkulaneum
i	i	k9	i
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
egyptských	egyptský	k2eAgFnPc2d1	egyptská
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zachovalých	zachovalý	k2eAgFnPc2d1	zachovalá
mumií	mumie	k1gFnPc2	mumie
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vůbec	vůbec	k9	vůbec
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
archeologické	archeologický	k2eAgNnSc4d1	Archeologické
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Teatro	Teatro	k1gNnSc1	Teatro
San	San	k1gFnSc2	San
Carlo	Carlo	k1gNnSc1	Carlo
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc2	Karel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slavnostně	slavnostně	k6eAd1	slavnostně
otevřené	otevřený	k2eAgInPc4d1	otevřený
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1737	[number]	k4	1737
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejstarší	starý	k2eAgFnSc1d3	nejstarší
činnou	činný	k2eAgFnSc7d1	činná
operní	operní	k2eAgFnSc7d1	operní
scénou	scéna	k1gFnSc7	scéna
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
umělecké	umělecký	k2eAgMnPc4d1	umělecký
šéfy	šéf	k1gMnPc4	šéf
divadla	divadlo	k1gNnSc2	divadlo
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
skladatelé	skladatel	k1gMnPc1	skladatel
Gioacchino	Gioacchino	k1gNnSc4	Gioacchino
Rossini	Rossin	k2eAgMnPc1d1	Rossin
a	a	k8xC	a
Gaetano	Gaetana	k1gFnSc5	Gaetana
Donizetti	Donizett	k1gMnPc1	Donizett
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
rozměry	rozměr	k1gInPc7	rozměr
a	a	k8xC	a
celkovým	celkový	k2eAgNnSc7d1	celkové
pojetím	pojetí	k1gNnSc7	pojetí
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prototypem	prototyp	k1gInSc7	prototyp
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
operních	operní	k2eAgFnPc2d1	operní
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
požáru	požár	k1gInSc2	požár
restaurováno	restaurován	k2eAgNnSc1d1	restaurováno
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
přestavěno	přestavět	k5eAaPmNgNnS	přestavět
v	v	k7c6	v
klasicistním	klasicistní	k2eAgInSc6d1	klasicistní
stylu	styl	k1gInSc6	styl
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
průčelí	průčelí	k1gNnSc1	průčelí
a	a	k8xC	a
dvorana	dvorana	k1gFnSc1	dvorana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
kolem	kolem	k7c2	kolem
tohoto	tento	k3xDgNnSc2	tento
divadla	divadlo	k1gNnSc2	divadlo
se	se	k3xPyFc4	se
utvořila	utvořit	k5eAaPmAgFnS	utvořit
celoevropsky	celoevropsky	k6eAd1	celoevropsky
proslulá	proslulý	k2eAgFnSc1d1	proslulá
neapolská	neapolský	k2eAgFnSc1d1	neapolská
operní	operní	k2eAgFnSc1d1	operní
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
členové	člen	k1gMnPc1	člen
zformovali	zformovat	k5eAaPmAgMnP	zformovat
žánr	žánr	k1gInSc4	žánr
opery	opera	k1gFnSc2	opera
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Díváme	dívat	k5eAaImIp1nP	dívat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
na	na	k7c4	na
město	město	k1gNnSc4	město
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
si	se	k3xPyFc3	se
určitě	určitě	k6eAd1	určitě
všimnete	všimnout	k5eAaPmIp2nP	všimnout
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
kopulí	kopule	k1gFnPc2	kopule
a	a	k8xC	a
křížů	kříž	k1gInPc2	kříž
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
svědčí	svědčit	k5eAaImIp3nP	svědčit
o	o	k7c6	o
množství	množství	k1gNnSc6	množství
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Neapol	Neapol	k1gFnSc1	Neapol
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Bývala	bývat	k5eAaImAgFnS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
la	la	k1gNnSc1	la
città	città	k?	città
dalle	dalle	k1gFnSc2	dalle
500	[number]	k4	500
cupole	cupole	k1gFnSc1	cupole
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
pěti	pět	k4xCc2	pět
set	sto	k4xCgNnPc2	sto
věží	věž	k1gFnPc2	věž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
počet	počet	k1gInSc4	počet
sakrálních	sakrální	k2eAgFnPc2d1	sakrální
staveb	stavba	k1gFnPc2	stavba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
hlavní	hlavní	k2eAgFnSc7d1	hlavní
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
klášter	klášter	k1gInSc1	klášter
sv.	sv.	kA	sv.
Kláry	Klára	k1gFnSc2	Klára
<g/>
,	,	kIx,	,
v	v	k7c6	v
samém	samý	k3xTgInSc6	samý
srdci	srdce	k1gNnSc6	srdce
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
postaveného	postavený	k2eAgNnSc2d1	postavené
v	v	k7c6	v
letech	let	k1gInPc6	let
1310	[number]	k4	1310
až	až	k9	až
1340	[number]	k4	1340
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
Roberta	Robert	k1gMnSc2	Robert
z	z	k7c2	z
Anjou	Anja	k1gFnSc7	Anja
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
původních	původní	k2eAgInPc6d1	původní
gotických	gotický	k2eAgInPc6d1	gotický
základech	základ	k1gInPc6	základ
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
provedena	proveden	k2eAgFnSc1d1	provedena
barokní	barokní	k2eAgFnSc1d1	barokní
přestavba	přestavba	k1gFnSc1	přestavba
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
zničená	zničený	k2eAgFnSc1d1	zničená
při	při	k7c6	při
masivních	masivní	k2eAgInPc6d1	masivní
náletech	nálet	k1gInPc6	nálet
Spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
restaurována	restaurován	k2eAgFnSc1d1	restaurována
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
původní	původní	k2eAgFnSc6d1	původní
gotické	gotický	k2eAgFnSc6d1	gotická
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
hluboce	hluboko	k6eAd1	hluboko
zapůsobí	zapůsobit	k5eAaPmIp3nS	zapůsobit
svou	svůj	k3xOyFgFnSc7	svůj
prostorností	prostornost	k1gFnSc7	prostornost
a	a	k8xC	a
prostotou	prostota	k1gFnSc7	prostota
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hrob	hrob	k1gInSc1	hrob
krále	král	k1gMnSc2	král
Roberta	Robert	k1gMnSc2	Robert
za	za	k7c7	za
hlavním	hlavní	k2eAgInSc7d1	hlavní
oltářem	oltář	k1gInSc7	oltář
a	a	k8xC	a
mezi	mezi	k7c7	mezi
pohřbenými	pohřbený	k2eAgFnPc7d1	pohřbená
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
hroby	hrob	k1gInPc1	hrob
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
Kristýny	Kristýna	k1gFnSc2	Kristýna
Savojské	savojský	k2eAgFnSc2d1	Savojská
a	a	k8xC	a
národního	národní	k2eAgMnSc2d1	národní
hrdiny	hrdina	k1gMnSc2	hrdina
Salva	salva	k1gFnSc1	salva
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Acquisto	Acquista	k1gMnSc5	Acquista
<g/>
.	.	kIx.	.
</s>
<s>
Značné	značný	k2eAgFnPc4d1	značná
umělecké	umělecký	k2eAgFnPc4d1	umělecká
hodnoty	hodnota	k1gFnPc4	hodnota
je	být	k5eAaImIp3nS	být
majolikový	majolikový	k2eAgInSc1d1	majolikový
ambit	ambit	k1gInSc1	ambit
Klarisek	klariska	k1gFnPc2	klariska
<g/>
,	,	kIx,	,
zahrada	zahrada	k1gFnSc1	zahrada
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
arkádou	arkáda	k1gFnSc7	arkáda
nově	nově	k6eAd1	nově
vyzdobená	vyzdobený	k2eAgFnSc1d1	vyzdobená
barevnými	barevný	k2eAgFnPc7d1	barevná
majolikovými	majolikový	k2eAgFnPc7d1	majoliková
dlaždicemi	dlaždice	k1gFnPc7	dlaždice
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Nový	nový	k2eAgInSc4d1	nový
kostel	kostel	k1gInSc4	kostel
Páně	páně	k2eAgNnSc2d1	páně
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
nazýván	nazýván	k2eAgInSc1d1	nazýván
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
odlišen	odlišit	k5eAaPmNgInS	odlišit
od	od	k7c2	od
Starého	Starého	k2eAgInSc2d1	Starého
kostela	kostel	k1gInSc2	kostel
Páně	páně	k2eAgFnSc2d1	páně
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
sv.	sv.	kA	sv.
Kláry	Klára	k1gFnPc1	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostně	slavnostně	k6eAd1	slavnostně
vysvěcen	vysvěcen	k2eAgInSc1d1	vysvěcen
roku	rok	k1gInSc3	rok
1597	[number]	k4	1597
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
požadována	požadovat	k5eAaImNgFnS	požadovat
jezuity	jezuita	k1gMnPc7	jezuita
a	a	k8xC	a
postavena	postavit	k5eAaPmNgNnP	postavit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nacházel	nacházet	k5eAaImAgInS	nacházet
palác	palác	k1gInSc1	palác
Sanseverino	Sanseverino	k1gNnSc1	Sanseverino
knížete	kníže	k1gMnSc2	kníže
ze	z	k7c2	z
Salerna	Salerna	k1gFnSc1	Salerna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bohatě	bohatě	k6eAd1	bohatě
zdobený	zdobený	k2eAgInSc1d1	zdobený
interiér	interiér	k1gInSc1	interiér
se	s	k7c7	s
zlatým	zlatý	k2eAgNnSc7d1	Zlaté
štukováním	štukování	k1gNnSc7	štukování
<g/>
,	,	kIx,	,
sochy	socha	k1gFnSc2	socha
a	a	k8xC	a
freska	fresko	k1gNnSc2	fresko
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
mistra	mistr	k1gMnSc2	mistr
Belisaria	Belisarium	k1gNnSc2	Belisarium
Corenzia	Corenzius	k1gMnSc2	Corenzius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vzdána	vzdán	k2eAgFnSc1d1	vzdána
pocta	pocta	k1gFnSc1	pocta
mnohým	mnohý	k2eAgMnPc3d1	mnohý
světcům	světec	k1gMnPc3	světec
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
např.	např.	kA	např.
sv.	sv.	kA	sv.
Ignác	Ignác	k1gMnSc1	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Moscati	Moscati	k1gMnSc1	Moscati
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
patřilo	patřit	k5eAaImAgNnS	patřit
původnímu	původní	k2eAgInSc3d1	původní
paláci	palác	k1gInSc3	palác
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyvedeno	vyvést	k5eAaPmNgNnS	vyvést
technikou	technika	k1gFnSc7	technika
piperno	piperna	k1gFnSc5	piperna
<g/>
,	,	kIx,	,
s	s	k7c7	s
kvádry	kvádr	k1gInPc7	kvádr
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
diamantu	diamant	k1gInSc2	diamant
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
předešel	předejít	k5eAaPmAgInS	předejít
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc1d1	stejný
ferrarský	ferrarský	k2eAgInSc1d1	ferrarský
Diamantový	diamantový	k2eAgInSc1d1	diamantový
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
Neapolský	neapolský	k2eAgInSc1d1	neapolský
dóm	dóm	k1gInSc1	dóm
je	být	k5eAaImIp3nS	být
stavba	stavba	k1gFnSc1	stavba
největší	veliký	k2eAgFnSc2d3	veliký
důležitosti	důležitost	k1gFnSc2	důležitost
na	na	k7c6	na
historické	historický	k2eAgFnSc6d1	historická
mapě	mapa	k1gFnSc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
dříve	dříve	k6eAd2	dříve
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stával	stávat	k5eAaImAgInS	stávat
chrám	chrám	k1gInSc1	chrám
boha	bůh	k1gMnSc2	bůh
Apolóna	Apolón	k1gMnSc2	Apolón
<g/>
,	,	kIx,	,
a	a	k8xC	a
první	první	k4xOgFnSc1	první
katedrála	katedrála	k1gFnSc1	katedrála
byla	být	k5eAaImAgFnS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
za	za	k7c4	za
Konstantina	Konstantin	k1gMnSc4	Konstantin
ve	v	k7c6	v
IV	IV	kA	IV
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Samotný	samotný	k2eAgInSc1d1	samotný
dóm	dóm	k1gInSc1	dóm
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Anjouovců	Anjouovec	k1gInPc2	Anjouovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
přetvářen	přetvářet	k5eAaImNgInS	přetvářet
během	běh	k1gInSc7	běh
staletí	staletí	k1gNnPc2	staletí
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
souborem	soubor	k1gInSc7	soubor
mnoha	mnoho	k4c2	mnoho
rozličných	rozličný	k2eAgInPc2d1	rozličný
slohů	sloh	k1gInPc2	sloh
<g/>
:	:	kIx,	:
novogotické	novogotický	k2eAgNnSc1d1	novogotické
průčelí	průčelí	k1gNnSc1	průčelí
zkonstruované	zkonstruovaný	k2eAgNnSc1d1	zkonstruované
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
portály	portál	k1gInPc1	portál
v	v	k7c6	v
květnaté	květnatý	k2eAgFnSc6d1	květnatá
gotice	gotika	k1gFnSc6	gotika
<g/>
,	,	kIx,	,
interiéry	interiér	k1gInPc1	interiér
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
barokní	barokní	k2eAgFnSc2d1	barokní
<g/>
:	:	kIx,	:
zejména	zejména	k9	zejména
Cappella	Cappella	k1gFnSc1	Cappella
del	del	k?	del
Tesoro	Tesora	k1gFnSc5	Tesora
(	(	kIx(	(
<g/>
kaple	kaple	k1gFnSc1	kaple
Pokladů	poklad	k1gInPc2	poklad
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
slohu	sloh	k1gInSc6	sloh
napoletánského	napoletánský	k2eAgNnSc2d1	napoletánský
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
Cappella	Cappella	k1gFnSc1	Cappella
(	(	kIx(	(
<g/>
Kaple	kaple	k1gFnSc1	kaple
<g/>
)	)	kIx)	)
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
sochu	socha	k1gFnSc4	socha
sv.	sv.	kA	sv.
Gennara	Gennara	k1gFnSc1	Gennara
a	a	k8xC	a
51	[number]	k4	51
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
soch	socha	k1gFnPc2	socha
"	"	kIx"	"
<g/>
spolupatronů	spolupatron	k1gInPc2	spolupatron
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
poklad	poklad	k1gInSc1	poklad
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
z	z	k7c2	z
rozličných	rozličný	k2eAgInPc2d1	rozličný
darů	dar	k1gInPc2	dar
od	od	k7c2	od
panovníků	panovník	k1gMnPc2	panovník
a	a	k8xC	a
bohatých	bohatý	k2eAgMnPc2d1	bohatý
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
dominuje	dominovat	k5eAaImIp3nS	dominovat
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
mitra	mitra	k1gFnSc1	mitra
od	od	k7c2	od
Mattea	Matteum	k1gNnSc2	Matteum
Tregliy	Treglia	k1gFnSc2	Treglia
posázená	posázený	k2eAgFnSc1d1	posázená
drahokamy	drahokam	k1gInPc7	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kapli	kaple	k1gFnSc6	kaple
je	být	k5eAaImIp3nS	být
také	také	k9	také
přechovávána	přechováván	k2eAgFnSc1d1	přechovávána
lebka	lebka	k1gFnSc1	lebka
světce	světec	k1gMnSc2	světec
a	a	k8xC	a
především	především	k9	především
nádobka	nádobka	k1gFnSc1	nádobka
obsahující	obsahující	k2eAgFnSc1d1	obsahující
jeho	jeho	k3xOp3gFnSc1	jeho
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgInSc1d3	nejznámější
"	"	kIx"	"
<g/>
zázračný	zázračný	k2eAgInSc1d1	zázračný
<g/>
"	"	kIx"	"
předmět	předmět	k1gInSc1	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Dominika	Dominik	k1gMnSc2	Dominik
je	být	k5eAaImIp3nS	být
též	též	k9	též
dílem	dílo	k1gNnSc7	dílo
vrstvení	vrstvení	k1gNnSc2	vrstvení
stylů	styl	k1gInPc2	styl
<g/>
:	:	kIx,	:
postaven	postavit	k5eAaPmNgMnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1283	[number]	k4	1283
<g/>
-	-	kIx~	-
<g/>
1324	[number]	k4	1324
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Anjou	Anjý	k2eAgFnSc4d1	Anjý
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
po	po	k7c6	po
několika	několik	k4yIc6	několik
rozvratech	rozvrat	k1gInPc6	rozvrat
a	a	k8xC	a
zmatcích	zmatek	k1gInPc6	zmatek
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
restaurován	restaurovat	k5eAaBmNgInS	restaurovat
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
vtisknout	vtisknout	k5eAaPmF	vtisknout
kostelu	kostel	k1gInSc3	kostel
jeho	on	k3xPp3gNnSc2	on
původní	původní	k2eAgFnSc4d1	původní
gotickou	gotický	k2eAgFnSc4d1	gotická
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Cappellone	Cappellon	k1gInSc5	Cappellon
del	del	k?	del
Crocifisso	Crocifissa	k1gFnSc5	Crocifissa
(	(	kIx(	(
<g/>
Kaple	kaple	k1gFnSc1	kaple
ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
schována	schovat	k5eAaPmNgFnS	schovat
právě	právě	k9	právě
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
<g/>
)	)	kIx)	)
stůl	stůl	k1gInSc4	stůl
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
krucifix	krucifix	k1gInSc4	krucifix
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
promluvil	promluvit	k5eAaPmAgMnS	promluvit
k	k	k7c3	k
Tomáši	Tomáš	k1gMnSc3	Tomáš
Akvinskému	Akvinský	k2eAgMnSc3d1	Akvinský
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
studentem	student	k1gMnSc7	student
teologie	teologie	k1gFnSc2	teologie
v	v	k7c6	v
přilehlém	přilehlý	k2eAgInSc6d1	přilehlý
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Universitě	universita	k1gFnSc6	universita
<g/>
.	.	kIx.	.
</s>
<s>
Sakristie	sakristie	k1gFnSc1	sakristie
je	být	k5eAaImIp3nS	být
vyzdobená	vyzdobený	k2eAgFnSc1d1	vyzdobená
freskami	freska	k1gFnPc7	freska
s	s	k7c7	s
motivem	motiv	k1gInSc7	motiv
vítězství	vítězství	k1gNnSc1	vítězství
řádu	řád	k1gInSc2	řád
Dominikánů	dominikán	k1gMnPc2	dominikán
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
přebýval	přebývat	k5eAaImAgMnS	přebývat
právě	právě	k6eAd1	právě
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
chrámu	chrám	k1gInSc6	chrám
<g/>
)	)	kIx)	)
od	od	k7c2	od
Francesca	Francesc	k1gInSc2	Francesc
Solimeny	Solimen	k1gInPc4	Solimen
a	a	k8xC	a
také	také	k9	také
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
pochováni	pochován	k2eAgMnPc1d1	pochován
panovníci	panovník	k1gMnPc1	panovník
a	a	k8xC	a
urození	urozený	k2eAgMnPc1d1	urozený
z	z	k7c2	z
aragonské	aragonský	k2eAgFnSc2d1	Aragonská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
z	z	k7c2	z
Pauly	Paula	k1gFnSc2	Paula
(	(	kIx(	(
<g/>
San	San	k1gMnSc1	San
Francesco	Francesco	k1gMnSc1	Francesco
di	di	k?	di
Paola	Paola	k1gMnSc1	Paola
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Piazza	Piazza	k1gFnSc1	Piazza
del	del	k?	del
Plebiscito	Plebiscit	k2eAgNnSc4d1	Plebiscito
naproti	naproti	k6eAd1	naproti
Královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
dokončen	dokončen	k2eAgInSc1d1	dokončen
1816	[number]	k4	1816
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
římskému	římský	k2eAgInSc3d1	římský
Pantheonu	Pantheon	k1gInSc3	Pantheon
<g/>
.	.	kIx.	.
</s>
<s>
Cappella	Cappella	k6eAd1	Cappella
Sansevero	Sansevero	k1gNnSc4	Sansevero
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
postavena	postavit	k5eAaPmNgNnP	postavit
Giovanem	Giovan	k1gMnSc7	Giovan
Franceskem	Francesk	k1gInSc7	Francesk
di	di	k?	di
Sangro	Sangra	k1gFnSc5	Sangra
vévodou	vévoda	k1gMnSc7	vévoda
z	z	k7c2	z
Torremaggiore	Torremaggior	k1gInSc5	Torremaggior
roku	rok	k1gInSc2	rok
1590	[number]	k4	1590
a	a	k8xC	a
následně	následně	k6eAd1	následně
určena	určit	k5eAaPmNgFnS	určit
jeho	jeho	k3xOp3gFnSc1	jeho
vnukem	vnuk	k1gMnSc7	vnuk
Alessandrem	Alessandr	k1gInSc7	Alessandr
k	k	k7c3	k
umístění	umístění	k1gNnSc3	umístění
hrobů	hrob	k1gInPc2	hrob
rodiny	rodina	k1gFnSc2	rodina
San	San	k1gMnSc5	San
Severo	Severa	k1gMnSc5	Severa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nepřeberným	přeberný	k2eNgNnSc7d1	nepřeberné
množstvím	množství	k1gNnSc7	množství
významných	významný	k2eAgFnPc2d1	významná
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1744	[number]	k4	1744
až	až	k8xS	až
cca	cca	kA	cca
1770	[number]	k4	1770
od	od	k7c2	od
Raimonda	Raimond	k1gMnSc2	Raimond
di	di	k?	di
Sangro	Sangro	k1gNnSc1	Sangro
<g/>
,	,	kIx,	,
VII	VII	kA	VII
<g/>
.	.	kIx.	.
knížetem	kníže	k1gNnSc7wR	kníže
z	z	k7c2	z
Sansevera	Sansevero	k1gNnSc2	Sansevero
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjímají	vyjímat	k5eAaImIp3nP	vyjímat
zejména	zejména	k9	zejména
La	la	k1gNnSc4	la
Pudicizia	Pudicizium	k1gNnSc2	Pudicizium
(	(	kIx(	(
<g/>
Cudnost	cudnost	k1gFnSc1	cudnost
<g/>
)	)	kIx)	)
od	od	k7c2	od
Antonia	Antonio	k1gMnSc2	Antonio
Corradiniho	Corradini	k1gMnSc2	Corradini
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
smyslnost	smyslnost	k1gFnSc4	smyslnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
známé	známý	k2eAgNnSc1d1	známé
dílo	dílo	k1gNnSc1	dílo
<g />
.	.	kIx.	.
</s>
<s>
Zahalený	zahalený	k2eAgMnSc1d1	zahalený
Kristus	Kristus	k1gMnSc1	Kristus
(	(	kIx(	(
<g/>
Cristo	Crista	k1gMnSc5	Crista
Velato	Velat	k2eAgNnSc1d1	Velat
<g/>
)	)	kIx)	)
od	od	k7c2	od
Giuseppa	Giusepp	k1gMnSc2	Giusepp
Sanmartina	Sanmartin	k1gMnSc2	Sanmartin
z	z	k7c2	z
r.	r.	kA	r.
1753	[number]	k4	1753
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
schopnost	schopnost	k1gFnSc1	schopnost
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
ztvárnění	ztvárnění	k1gNnSc2	ztvárnění
efektu	efekt	k1gInSc2	efekt
splývajícího	splývající	k2eAgNnSc2d1	splývající
mramorového	mramorový	k2eAgNnSc2d1	mramorové
roucha	roucho	k1gNnSc2	roucho
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
Krista	Kristus	k1gMnSc2	Kristus
<g/>
:	:	kIx,	:
dílo	dílo	k1gNnSc1	dílo
lákalo	lákat	k5eAaImAgNnS	lákat
Canovu	Canovo	k1gNnSc3	Canovo
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
neapolském	neapolský	k2eAgInSc6d1	neapolský
pobytu	pobyt	k1gInSc6	pobyt
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mocně	mocně	k6eAd1	mocně
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
je	být	k5eAaImIp3nS	být
koupit	koupit	k5eAaPmF	koupit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
marně	marně	k6eAd1	marně
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
několika	několik	k4yIc7	několik
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
neapolská	neapolský	k2eAgFnSc1d1	neapolská
novinářka	novinářka	k1gFnSc1	novinářka
Clara	Clara	k1gFnSc1	Clara
Miccinelli	Miccinelle	k1gFnSc4	Miccinelle
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
neapolském	neapolský	k2eAgInSc6d1	neapolský
notářském	notářský	k2eAgInSc6d1	notářský
archivu	archiv	k1gInSc6	archiv
vystopovala	vystopovat	k5eAaPmAgFnS	vystopovat
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
kníže	kníže	k1gNnSc1wR	kníže
di	di	k?	di
Sansevero	Sansevero	k1gNnSc1	Sansevero
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
se	s	k7c7	s
sochařem	sochař	k1gMnSc7	sochař
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
jasně	jasně	k6eAd1	jasně
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
plátěné	plátěný	k2eAgNnSc4d1	plátěné
roucho	roucho	k1gNnSc4	roucho
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
kníže	kníže	k1gMnSc1	kníže
procesem	proces	k1gInSc7	proces
mramorování	mramorování	k1gNnSc2	mramorování
tkaniny	tkanina	k1gFnSc2	tkanina
jím	on	k3xPp3gInSc7	on
vynalezeným	vynalezený	k2eAgInSc7d1	vynalezený
<g/>
.	.	kIx.	.
</s>
<s>
Sammartino	Sammartin	k2eAgNnSc1d1	Sammartino
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vytesal	vytesat	k5eAaPmAgMnS	vytesat
Krista	Kristus	k1gMnSc4	Kristus
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
dočistit	dočistit	k5eAaPmF	dočistit
roucho	roucho	k1gNnSc4	roucho
<g/>
,	,	kIx,	,
broušením	broušení	k1gNnSc7	broušení
a	a	k8xC	a
leštěním	leštění	k1gNnSc7	leštění
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
knihu	kniha	k1gFnSc4	kniha
<g/>
:	:	kIx,	:
Clara	Clara	k1gFnSc1	Clara
Miccinelli	Miccinelle	k1gFnSc4	Miccinelle
<g/>
:	:	kIx,	:
Il	Il	k1gFnSc4	Il
tesoro	tesora	k1gFnSc5	tesora
del	del	k?	del
Principe	princip	k1gInSc5	princip
di	di	k?	di
Sansevero	Sansevero	k1gNnSc1	Sansevero
-	-	kIx~	-
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
</s>
<s>
SEN	sen	k1gInSc1	sen
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c4	v
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Fotografové	fotograf	k1gMnPc1	fotograf
zachycovali	zachycovat	k5eAaImAgMnP	zachycovat
pohledy	pohled	k1gInPc4	pohled
na	na	k7c4	na
Sorrento	Sorrento	k1gNnSc4	Sorrento
<g/>
,	,	kIx,	,
Capri	Capri	k1gNnSc4	Capri
a	a	k8xC	a
archeologicky	archeologicky	k6eAd1	archeologicky
proslavené	proslavený	k2eAgFnPc1d1	proslavená
lokality	lokalita	k1gFnPc1	lokalita
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Pompeje	Pompeje	k1gFnPc1	Pompeje
nebo	nebo	k8xC	nebo
Agrigento	Agrigento	k1gNnSc1	Agrigento
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
výjevy	výjev	k1gInPc1	výjev
všedního	všední	k2eAgInSc2d1	všední
života	život	k1gInSc2	život
ve	v	k7c6	v
městě	město	k1gNnSc6	město
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
kontrasty	kontrast	k1gInPc7	kontrast
mezi	mezi	k7c7	mezi
chudobou	chudoba	k1gFnSc7	chudoba
a	a	k8xC	a
bohatstvím	bohatství	k1gNnSc7	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
působili	působit	k5eAaImAgMnP	působit
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xC	jako
například	například	k6eAd1	například
Calvert	Calvert	k1gMnSc1	Calvert
Richard	Richard	k1gMnSc1	Richard
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
Firmin-Eugè	Firmin-Eugè	k1gMnSc1	Firmin-Eugè
Le	Le	k1gFnSc2	Le
Dieu	Dieus	k1gInSc2	Dieus
<g/>
,	,	kIx,	,
Gustave	Gustav	k1gMnSc5	Gustav
Le	Le	k1gMnSc5	Le
Gray	Gra	k1gMnPc4	Gra
<g/>
,	,	kIx,	,
George	Georg	k1gMnPc4	Georg
Wilson	Wilson	k1gMnSc1	Wilson
Bridges	Bridges	k1gMnSc1	Bridges
<g/>
,	,	kIx,	,
Giorgio	Giorgio	k1gMnSc1	Giorgio
Sommer	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Giuseppe	Giusepp	k1gInSc5	Giusepp
Incorpora	Incorpor	k1gMnSc4	Incorpor
nebo	nebo	k8xC	nebo
Luigi	Luige	k1gFnSc4	Luige
Sacchi	Sacch	k1gFnSc2	Sacch
<g/>
.	.	kIx.	.
</s>
<s>
Atény	Atény	k1gFnPc1	Atény
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
</s>
