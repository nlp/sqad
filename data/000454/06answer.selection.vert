<s>
Neapol	Neapol	k1gFnSc1	Neapol
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Napoli	Napole	k1gFnSc4	Napole
<g/>
,	,	kIx,	,
neapolsky	neapolsky	k6eAd1	neapolsky
Nà	Nà	k1gFnSc1	Nà
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
provincie	provincie	k1gFnSc2	provincie
a	a	k8xC	a
regionu	region	k1gInSc2	region
Kampánie	Kampánie	k1gFnSc2	Kampánie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc4	třetí
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Itálie	Itálie	k1gFnSc2	Itálie
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
předměstími	předměstí	k1gNnPc7	předměstí
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
konurbaci	konurbace	k1gFnSc4	konurbace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	s	k7c7	s
3	[number]	k4	3
121	[number]	k4	121
397	[number]	k4	397
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
založeny	založen	k2eAgInPc4d1	založen
na	na	k7c4	na
informaci	informace	k1gFnSc4	informace
Istat	Istat	k1gInSc1	Istat
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
po	po	k7c6	po
aglomeraci	aglomerace	k1gFnSc6	aglomerace
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
místě	místo	k1gNnSc6	místo
nejlidnatějších	lidnatý	k2eAgFnPc2d3	nejlidnatější
metropolitních	metropolitní	k2eAgFnPc2d1	metropolitní
oblastí	oblast	k1gFnPc2	oblast
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
4	[number]	k4	4
400	[number]	k4	400
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
2	[number]	k4	2
228,79	[number]	k4	228,79
km2	km2	k4	km2
(	(	kIx(	(
<g/>
údaje	údaj	k1gInSc2	údaj
od	od	k7c2	od
Svimez	Svimeza	k1gFnPc2	Svimeza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
