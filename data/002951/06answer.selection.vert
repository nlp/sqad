<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
určitá	určitý	k2eAgFnSc1d1	určitá
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
,	,	kIx,	,
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Prolog	prolog	k1gInSc1	prolog
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pravidlu	pravidlo	k1gNnSc3	pravidlo
(	(	kIx(	(
<g/>
A	A	kA	A
:	:	kIx,	:
<g/>
-	-	kIx~	-
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
Bn	Bn	k1gMnSc1	Bn
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
