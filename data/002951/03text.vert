<s>
Ve	v	k7c6	v
výrokové	výrokový	k2eAgFnSc6d1	výroková
logice	logika	k1gFnSc6	logika
se	se	k3xPyFc4	se
jako	jako	k9	jako
Hornova	Hornův	k2eAgFnSc1d1	Hornova
klauzule	klauzule	k1gFnSc1	klauzule
označuje	označovat	k5eAaImIp3nS	označovat
speciální	speciální	k2eAgInSc4d1	speciální
druh	druh	k1gInSc4	druh
klauzule	klauzule	k1gFnSc1	klauzule
(	(	kIx(	(
<g/>
disjunkce	disjunkce	k1gFnSc1	disjunkce
literálů	literál	k1gInPc2	literál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejvýše	nejvýše	k6eAd1	nejvýše
jeden	jeden	k4xCgInSc4	jeden
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
literál	literál	k1gInSc4	literál
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
negované	negovaný	k2eAgFnPc1d1	negovaná
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
¬	¬	k?	¬
p	p	k?	p
∨	∨	k?	∨
¬	¬	k?	¬
q	q	k?	q
∨	∨	k?	∨
⋯	⋯	k?	⋯
∨	∨	k?	∨
¬	¬	k?	¬
t	t	k?	t
∨	∨	k?	∨
u	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
neg	neg	k?	neg
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
lor	lor	k?	lor
\	\	kIx~	\
<g/>
neg	neg	k?	neg
q	q	k?	q
<g/>
\	\	kIx~	\
<g/>
lor	lor	k?	lor
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
\	\	kIx~	\
<g/>
lor	lor	k?	lor
\	\	kIx~	\
<g/>
neg	neg	k?	neg
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
lor	lor	k?	lor
u	u	k7c2	u
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Hornovu	Hornův	k2eAgFnSc4d1	Hornova
klauzuli	klauzule	k1gFnSc4	klauzule
tak	tak	k9	tak
lze	lze	k6eAd1	lze
obecně	obecně	k6eAd1	obecně
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xS	jako
implikaci	implikace	k1gFnSc4	implikace
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
p	p	k?	p
∧	∧	k?	∧
q	q	k?	q
∧	∧	k?	∧
⋯	⋯	k?	⋯
∧	∧	k?	∧
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
⟹	⟹	k?	⟹
:	:	kIx,	:
u	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
land	land	k1gMnSc1	land
q	q	k?	q
<g/>
\	\	kIx~	\
<g/>
land	land	k1gMnSc1	land
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
\	\	kIx~	\
<g/>
land	land	k1gInSc1	land
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
implies	implies	k1gInSc1	implies
u	u	k7c2	u
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
Hornova	Hornův	k2eAgFnSc1d1	Hornova
formule	formule	k1gFnSc1	formule
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
formule	formule	k1gFnSc1	formule
v	v	k7c6	v
konjunktivní	konjunktivní	k2eAgFnSc6d1	konjunktivní
normální	normální	k2eAgFnSc6d1	normální
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
Hornových	Hornových	k2eAgFnPc2d1	Hornových
klauzulí	klauzule	k1gFnPc2	klauzule
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
duální	duální	k2eAgFnSc1d1	duální
Hornova	Hornův	k2eAgFnSc1d1	Hornova
klauzule	klauzule	k1gFnSc1	klauzule
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejvýše	vysoce	k6eAd3	vysoce
jeden	jeden	k4xCgInSc1	jeden
negativní	negativní	k2eAgInSc1d1	negativní
literál	literál	k1gInSc1	literál
(	(	kIx(	(
<g/>
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hornovy	Hornův	k2eAgFnPc1d1	Hornova
klauzule	klauzule	k1gFnPc1	klauzule
mají	mít	k5eAaImIp3nP	mít
stěžejní	stěžejní	k2eAgFnSc4d1	stěžejní
roli	role	k1gFnSc4	role
v	v	k7c6	v
logickém	logický	k2eAgNnSc6d1	logické
programování	programování	k1gNnSc6	programování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Prolog	prolog	k1gInSc1	prolog
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
pro	pro	k7c4	pro
konstruktivní	konstruktivní	k2eAgFnSc4d1	konstruktivní
logiku	logika	k1gFnSc4	logika
<g/>
.	.	kIx.	.
</s>
<s>
Hornova	Hornův	k2eAgFnSc1d1	Hornova
klauzule	klauzule	k1gFnSc1	klauzule
obsahující	obsahující	k2eAgFnSc1d1	obsahující
právě	právě	k9	právě
jeden	jeden	k4xCgInSc1	jeden
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
literál	literál	k1gInSc1	literál
a	a	k8xC	a
několik	několik	k4yIc1	několik
(	(	kIx(	(
<g/>
nejméně	málo	k6eAd3	málo
jeden	jeden	k4xCgMnSc1	jeden
<g/>
)	)	kIx)	)
negativních	negativní	k2eAgFnPc2d1	negativní
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
implikaci	implikace	k1gFnSc4	implikace
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
určitá	určitý	k2eAgFnSc1d1	určitá
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
,	,	kIx,	,
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Prolog	prolog	k1gInSc1	prolog
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pravidlu	pravidlo	k1gNnSc3	pravidlo
(	(	kIx(	(
<g/>
A	A	kA	A
:	:	kIx,	:
<g/>
-	-	kIx~	-
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
Bn	Bn	k1gMnSc1	Bn
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klauzule	klauzule	k1gFnSc1	klauzule
obsahující	obsahující	k2eAgFnSc1d1	obsahující
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
literál	literál	k1gInSc4	literál
a	a	k8xC	a
žádné	žádný	k3yNgInPc4	žádný
negativní	negativní	k2eAgInPc4d1	negativní
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
prostému	prostý	k2eAgNnSc3d1	prosté
tvrzení	tvrzení	k1gNnSc3	tvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
cílová	cílový	k2eAgFnSc1d1	cílová
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
,	,	kIx,	,
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Prolog	prolog	k1gInSc1	prolog
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
faktu	fakt	k1gInSc2	fakt
(	(	kIx(	(
<g/>
A	A	kA	A
:	:	kIx,	:
<g/>
-	-	kIx~	-
true	true	k1gInSc1	true
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
prostě	prostě	k9	prostě
A.	A.	kA	A.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klauzule	klauzule	k1gFnPc1	klauzule
neobsahující	obsahující	k2eNgFnPc1d1	neobsahující
žádný	žádný	k3yNgInSc4	žádný
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
literál	literál	k1gInSc4	literál
a	a	k8xC	a
obsahující	obsahující	k2eAgInSc4d1	obsahující
několik	několik	k4yIc1	několik
negativních	negativní	k2eAgMnPc2d1	negativní
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Prolog	prolog	k1gInSc1	prolog
dotazu	dotaz	k1gInSc2	dotaz
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
-	-	kIx~	-
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
Bn	Bn	k1gMnSc1	Bn
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
