<p>
<s>
Nine	Nine	k1gFnSc1	Nine
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Nine	Nin	k1gInSc2	Nin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americko-italský	americkotalský	k2eAgInSc1d1	americko-italský
filmový	filmový	k2eAgInSc1d1	filmový
muzikál	muzikál	k1gInSc1	muzikál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Režisérem	režisér	k1gMnSc7	režisér
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
Rob	roba	k1gFnPc2	roba
Marshall	Marshall	k1gMnSc1	Marshall
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Daniel	Daniel	k1gMnSc1	Daniel
Day-Lewis	Day-Lewis	k1gInSc4	Day-Lewis
<g/>
,	,	kIx,	,
Marion	Marion	k1gInSc4	Marion
Cotillard	Cotillarda	k1gFnPc2	Cotillarda
<g/>
,	,	kIx,	,
Penélope	Penélop	k1gInSc5	Penélop
Cruz	Cruz	k1gInSc1	Cruz
<g/>
,	,	kIx,	,
Nicole	Nicole	k1gFnSc1	Nicole
Kidman	Kidman	k1gMnSc1	Kidman
a	a	k8xC	a
Judi	Judi	k1gNnSc1	Judi
Dench	Dencha	k1gFnPc2	Dencha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
4	[number]	k4	4
Oscary	Oscar	k1gInPc4	Oscar
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
kostýmy	kostým	k1gInPc1	kostým
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
výprava	výprava	k1gFnSc1	výprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nominován	nominovat	k5eAaBmNgInS	nominovat
byl	být	k5eAaImAgInS	být
také	také	k9	také
na	na	k7c4	na
5	[number]	k4	5
Zlatých	zlatý	k2eAgInPc2d1	zlatý
globů	globus	k1gInPc2	globus
<g/>
,	,	kIx,	,
1	[number]	k4	1
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
a	a	k8xC	a
2	[number]	k4	2
ceny	cena	k1gFnSc2	cena
SAG	SAG	kA	SAG
Award	Award	k1gInSc1	Award
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
dalších	další	k2eAgInPc2d1	další
8	[number]	k4	8
ocenění	ocenění	k1gNnPc2	ocenění
a	a	k8xC	a
48	[number]	k4	48
nominací	nominace	k1gFnPc2	nominace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc1	reakce
==	==	k?	==
</s>
</p>
<p>
<s>
aktuální	aktuální	k2eAgInPc1d1	aktuální
k	k	k7c3	k
12	[number]	k4	12
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
2018	[number]	k4	2018
</s>
</p>
<p>
<s>
csfd	csfd	k1gInSc1	csfd
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
imdb	imdb	k1gInSc1	imdb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
fdb	fdb	k?	fdb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Nine	Nine	k6eAd1	Nine
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc3d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
</s>
</p>
<p>
<s>
Nine	Ninat	k5eAaPmIp3nS	Ninat
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nine	Nine	k6eAd1	Nine
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
