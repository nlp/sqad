<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
je	být	k5eAaImIp3nS	být
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
malba	malba	k1gFnSc1	malba
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
milánském	milánský	k2eAgInSc6d1	milánský
kostele	kostel	k1gInSc6	kostel
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
delle	delle	k1gFnSc1	delle
Grazie	Grazie	k1gFnSc1	Grazie
<g/>
.	.	kIx.	.
</s>
<s>
Da	Da	k?	Da
Vinci	Vinca	k1gMnPc7	Vinca
malbu	malba	k1gFnSc4	malba
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
patrony	patron	k1gInPc4	patron
Ludovica	Ludovicus	k1gMnSc4	Ludovicus
Sforza	Sforz	k1gMnSc4	Sforz
a	a	k8xC	a
Beatrici	Beatrice	k1gFnSc4	Beatrice
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
.	.	kIx.	.
</s>
<s>
Malba	malba	k1gFnSc1	malba
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
Ježíše	Ježíš	k1gMnPc4	Ježíš
a	a	k8xC	a
dvanáct	dvanáct	k4xCc1	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
večeři	večeře	k1gFnSc6	večeře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obraze	obraz	k1gInSc6	obraz
je	být	k5eAaImIp3nS	být
použita	použit	k2eAgFnSc1d1	použita
trojúhelníková	trojúhelníkový	k2eAgFnSc1d1	trojúhelníková
kompozice	kompozice	k1gFnSc1	kompozice
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
za	za	k7c7	za
Kristem	Kristus	k1gMnSc7	Kristus
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc1	uspořádání
skupinek	skupinka	k1gFnPc2	skupinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
techniky	technika	k1gFnSc2	technika
malby	malba	k1gFnSc2	malba
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
fresku	freska	k1gFnSc4	freska
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
často	často	k6eAd1	často
používána	používán	k2eAgFnSc1d1	používána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
díle	dílo	k1gNnSc6	dílo
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
čtyři	čtyři	k4xCgFnPc1	čtyři
skupinky	skupinka	k1gFnPc1	skupinka
po	po	k7c6	po
třech	tři	k4xCgMnPc6	tři
apoštolech	apoštol	k1gMnPc6	apoštol
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
nahnuti	nahnout	k5eAaPmNgMnP	nahnout
k	k	k7c3	k
sobě	se	k3xPyFc3	se
a	a	k8xC	a
debatují	debatovat	k5eAaImIp3nP	debatovat
o	o	k7c6	o
zprávě	zpráva	k1gFnSc6	zpráva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jim	on	k3xPp3gMnPc3	on
právě	právě	k9	právě
Kristus	Kristus	k1gMnSc1	Kristus
pověděl	povědět	k5eAaPmAgMnS	povědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zradil	zradit	k5eAaPmAgMnS	zradit
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
Jidáš	jidáš	k1gInSc1	jidáš
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
záklonu	záklon	k1gInSc6	záklon
a	a	k8xC	a
couvá	couvat	k5eAaImIp3nS	couvat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ježíš	Ježíš	k1gMnSc1	Ježíš
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
zradě	zrada	k1gFnSc6	zrada
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
Poslední	poslední	k2eAgFnSc2d1	poslední
večeře	večeře	k1gFnSc2	večeře
byl	být	k5eAaImAgInS	být
také	také	k9	také
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
dobrodružně-fantastickém	dobrodružněantastický	k2eAgInSc6d1	dobrodružně-fantastický
románu	román	k1gInSc6	román
Dana	Dana	k1gFnSc1	Dana
Browna	Browna	k1gFnSc1	Browna
Šifra	šifra	k1gFnSc1	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dílu	díl	k1gInSc6	díl
brán	brát	k5eAaImNgInS	brát
jako	jako	k8xS	jako
šifra	šifra	k1gFnSc1	šifra
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gFnSc1	vedoucí
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
stovky	stovka	k1gFnSc2	stovka
let	léto	k1gNnPc2	léto
hledaného	hledaný	k2eAgInSc2d1	hledaný
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
Svatého	svatý	k2eAgInSc2d1	svatý
Grálu	grál	k1gInSc2	grál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dílu	díl	k1gInSc6	díl
se	se	k3xPyFc4	se
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
chtěl	chtít	k5eAaImAgMnS	chtít
poukázat	poukázat	k5eAaPmF	poukázat
na	na	k7c4	na
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
Ježíšem	Ježíš	k1gMnSc7	Ježíš
Nazaretským	nazaretský	k2eAgInSc7d1	nazaretský
a	a	k8xC	a
Máří	Máří	k?	Máří
Magdalenou	Magdalena	k1gFnSc7	Magdalena
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
například	například	k6eAd1	například
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
vyobrazení	vyobrazení	k1gNnSc6	vyobrazení
Poslední	poslední	k2eAgFnSc1d1	poslední
Večeře	večeře	k1gFnSc1	večeře
se	se	k3xPyFc4	se
nikde	nikde	k6eAd1	nikde
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
kalich	kalich	k1gInSc1	kalich
(	(	kIx(	(
<g/>
nejčastější	častý	k2eAgFnSc1d3	nejčastější
podoba	podoba	k1gFnSc1	podoba
Svatého	svatý	k2eAgInSc2d1	svatý
Grálu	grál	k1gInSc2	grál
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
pramenech	pramen	k1gInPc6	pramen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejzajímavějším	zajímavý	k2eAgInSc7d3	nejzajímavější
faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
osoba	osoba	k1gFnSc1	osoba
po	po	k7c6	po
Ježíšově	Ježíšův	k2eAgFnSc6d1	Ježíšova
pravici	pravice	k1gFnSc6	pravice
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nejblíže	blízce	k6eAd3	blízce
Pána	pán	k1gMnSc4	pán
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
důkladném	důkladný	k2eAgNnSc6d1	důkladné
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
povšimnout	povšimnout	k5eAaPmF	povšimnout
náznaku	náznak	k1gInSc2	náznak
ňader	ňadro	k1gNnPc2	ňadro
<g/>
,	,	kIx,	,
ženských	ženský	k2eAgInPc2d1	ženský
rysů	rys	k1gInPc2	rys
ve	v	k7c6	v
tváři	tvář	k1gFnSc6	tvář
<g/>
,	,	kIx,	,
ženských	ženský	k2eAgInPc2d1	ženský
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
osoba	osoba	k1gFnSc1	osoba
po	po	k7c6	po
Ježíšově	Ježíšův	k2eAgFnSc6d1	Ježíšova
pravici	pravice	k1gFnSc6	pravice
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
identickém	identický	k2eAgNnSc6d1	identické
oblečení	oblečení	k1gNnSc6	oblečení
jako	jako	k8xC	jako
sám	sám	k3xTgMnSc1	sám
Ježíš	Ježíš	k1gMnSc1	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
tuto	tento	k3xDgFnSc4	tento
osobu	osoba	k1gFnSc4	osoba
vystřihli	vystřihnout	k5eAaPmAgMnP	vystřihnout
a	a	k8xC	a
položili	položit	k5eAaPmAgMnP	položit
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
Ježíšovu	Ježíšův	k2eAgFnSc4d1	Ježíšova
levici	levice	k1gFnSc4	levice
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
velmi	velmi	k6eAd1	velmi
záhadně	záhadně	k6eAd1	záhadně
<g/>
,	,	kIx,	,
možná	možná	k9	možná
až	až	k9	až
milenecky	milenecky	k6eAd1	milenecky
<g/>
,	,	kIx,	,
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
levém	levý	k2eAgNnSc6d1	levé
rameni	rameno	k1gNnSc6	rameno
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
dílu	díl	k1gInSc6	díl
od	od	k7c2	od
Dana	Dan	k1gMnSc2	Dan
Browna	Brown	k1gMnSc2	Brown
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
osoba	osoba	k1gFnSc1	osoba
po	po	k7c6	po
Ježíšově	Ježíšův	k2eAgFnSc6d1	Ježíšova
pravici	pravice	k1gFnSc6	pravice
nemusela	muset	k5eNaImAgFnS	muset
být	být	k5eAaImF	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Máří	Máří	k?	Máří
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
údajnou	údajný	k2eAgFnSc7d1	údajná
Máří	Máří	k?	Máří
a	a	k8xC	a
Ježíšem	Ježíš	k1gMnSc7	Ježíš
je	být	k5eAaImIp3nS	být
volný	volný	k2eAgInSc1d1	volný
prostor	prostor	k1gInSc1	prostor
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
V	v	k7c4	v
což	což	k3yQnSc4	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tvar	tvar	k1gInSc4	tvar
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
kalichu	kalich	k1gInSc2	kalich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
starý	starý	k2eAgInSc4d1	starý
znak	znak	k1gInSc4	znak
pro	pro	k7c4	pro
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
ženského	ženský	k2eAgNnSc2d1	ženské
lůna	lůno	k1gNnSc2	lůno
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Dílo	dílo	k1gNnSc1	dílo
Poslední	poslední	k2eAgFnPc1d1	poslední
večeře	večeře	k1gFnPc1	večeře
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
,	,	kIx,	,
Gallery	Galler	k1gInPc1	Galler
of	of	k?	of
Paintings	Paintings	k1gInSc1	Paintings
and	and	k?	and
Drawings	Drawings	k1gInSc1	Drawings
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
<g/>
Official	Official	k1gInSc1	Official
Milanese	Milanese	k1gFnSc2	Milanese
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Last	Last	k1gMnSc1	Last
Supper	Supper	k1gMnSc1	Supper
<g/>
"	"	kIx"	"
site	site	k1gFnSc1	site
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
-	-	kIx~	-
The	The	k1gMnSc1	The
Last	Last	k1gMnSc1	Last
Supper	Supper	k1gMnSc1	Supper
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
John	John	k1gMnSc1	John
the	the	k?	the
Apostle	Apostle	k1gMnSc1	Apostle
in	in	k?	in
Art	Art	k1gMnSc1	Art
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Is	Is	k1gMnSc1	Is
that	that	k1gMnSc1	that
John	John	k1gMnSc1	John
or	or	k?	or
Mary	Mary	k1gFnSc1	Mary
Magdalene	Magdalen	k1gInSc5	Magdalen
in	in	k?	in
the	the	k?	the
Last	Last	k1gMnSc1	Last
Supper	Supper	k1gMnSc1	Supper
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
24	[number]	k4	24
'	'	kIx"	'
<g/>
Last	Last	k2eAgInSc1d1	Last
Supper	Supper	k1gInSc1	Supper
<g/>
'	'	kIx"	'
Parodies	Parodies	k1gInSc1	Parodies
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Two	Two	k1gFnSc1	Two
Da	Da	k1gMnSc2	Da
Vinci	Vinca	k1gMnSc2	Vinca
Self-portraits	Selfortraits	k1gInSc4	Self-portraits
<g/>
?	?	kIx.	?
</s>
