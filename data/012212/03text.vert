<p>
<s>
V	v	k7c6	v
soubojích	souboj	k1gInPc6	souboj
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
ročníku	ročník	k1gInSc2	ročník
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1	Moravskoslezská
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
ligy	liga	k1gFnSc2	liga
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
se	se	k3xPyFc4	se
utkalo	utkat	k5eAaPmAgNnS	utkat
16	[number]	k4	16
týmů	tým	k1gInPc2	tým
každý	každý	k3xTgInSc1	každý
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
dvoukolovým	dvoukolový	k2eAgInSc7d1	dvoukolový
systémem	systém	k1gInSc7	systém
podzim	podzim	k1gInSc1	podzim
<g/>
–	–	k?	–
<g/>
jaro	jaro	k1gNnSc1	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ročník	ročník	k1gInSc1	ročník
začal	začít	k5eAaPmAgInS	začít
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
úvodními	úvodní	k2eAgInPc7d1	úvodní
čtyřmi	čtyři	k4xCgInPc7	čtyři
zápasy	zápas	k1gInPc7	zápas
1	[number]	k4	1
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
zbývajícími	zbývající	k2eAgInPc7d1	zbývající
třemi	tři	k4xCgInPc7	tři
zápasy	zápas	k1gInPc7	zápas
30	[number]	k4	30
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
podzimní	podzimní	k2eAgFnSc6d1	podzimní
části	část	k1gFnSc6	část
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
klub	klub	k1gInSc1	klub
FC	FC	kA	FC
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
sestupujícím	sestupující	k2eAgMnSc7d1	sestupující
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
anulovány	anulován	k2eAgInPc1d1	anulován
a	a	k8xC	a
soutěž	soutěž	k1gFnSc1	soutěž
byla	být	k5eAaImAgFnS	být
dohrána	dohrát	k5eAaPmNgFnS	dohrát
s	s	k7c7	s
15	[number]	k4	15
účastníky	účastník	k1gMnPc7	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
II	II	kA	II
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
postoupil	postoupit	k5eAaPmAgMnS	postoupit
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
Vítkovic	Vítkovice	k1gInPc2	Vítkovice
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
fakticky	fakticky	k6eAd1	fakticky
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
,	,	kIx,	,
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
také	také	k9	také
poslední	poslední	k2eAgInSc4d1	poslední
Slavičín	Slavičín	k1gInSc4	Slavičín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nové	Nové	k2eAgInPc1d1	Nové
týmy	tým	k1gInPc1	tým
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
==	==	k?	==
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
II	II	kA	II
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
sestoupila	sestoupit	k5eAaPmAgFnS	sestoupit
do	do	k7c2	do
MSFL	MSFL	kA	MSFL
mužstva	mužstvo	k1gNnSc2	mužstvo
FC	FC	kA	FC
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
a	a	k8xC	a
SFC	SFC	kA	SFC
Opava	Opava	k1gFnSc1	Opava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Divize	divize	k1gFnSc2	divize
D	D	kA	D
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
mužstvo	mužstvo	k1gNnSc1	mužstvo
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Slovácko	Slovácko	k1gNnSc1	Slovácko
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
Divize	divize	k1gFnSc2	divize
E	E	kA	E
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
vítězné	vítězný	k2eAgNnSc1d1	vítězné
mužstvo	mužstvo	k1gNnSc1	mužstvo
FC	FC	kA	FC
Tescoma	Tescoma	k1gNnSc4	Tescoma
Zlín	Zlín	k1gInSc1	Zlín
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
a	a	k8xC	a
SK	Sk	kA	Sk
Spartak	Spartak	k1gInSc1	Spartak
Hulín	Hulín	k1gInSc1	Hulín
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
==	==	k?	==
</s>
</p>
<p>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
ročníku	ročník	k1gInSc2	ročník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
útočník	útočník	k1gMnSc1	útočník
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Klesnil	Klesnil	k1gMnSc1	Klesnil
z	z	k7c2	z
Opavy	Opava	k1gFnSc2	Opava
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
soupeřům	soupeř	k1gMnPc3	soupeř
nastřílel	nastřílet	k5eAaPmAgMnS	nastřílet
14	[number]	k4	14
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účastníci	účastník	k1gMnPc1	účastník
ročníku	ročník	k1gInSc2	ročník
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
==	==	k?	==
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
sestoupila	sestoupit	k5eAaPmAgFnS	sestoupit
mužstva	mužstvo	k1gNnPc1	mužstvo
FC	FC	kA	FC
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
a	a	k8xC	a
SFC	SFC	kA	SFC
Opava	Opava	k1gFnSc1	Opava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
z	z	k7c2	z
Divize	divize	k1gFnSc2	divize
D	D	kA	D
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
dostalo	dostat	k5eAaPmAgNnS	dostat
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Slovácko	Slovácko	k1gNnSc1	Slovácko
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
skončilo	skončit	k5eAaPmAgNnS	skončit
druhé	druhý	k4xOgNnSc1	druhý
za	za	k7c2	za
Šardicemi	Šardice	k1gFnPc7	Šardice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
Divize	divize	k1gFnSc2	divize
E	E	kA	E
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
postoupil	postoupit	k5eAaPmAgMnS	postoupit
jak	jak	k6eAd1	jak
vítěz	vítěz	k1gMnSc1	vítěz
FC	FC	kA	FC
Tescoma	Tescoma	k1gFnSc1	Tescoma
Zlín	Zlín	k1gInSc1	Zlín
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
třetí	třetí	k4xOgFnPc4	třetí
SK	Sk	kA	Sk
Spartak	Spartak	k1gInSc1	Spartak
Hulín	Hulín	k1gInSc1	Hulín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konečná	konečný	k2eAgFnSc1d1	konečná
tabulka	tabulka	k1gFnSc1	tabulka
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Z	z	k7c2	z
=	=	kIx~	=
Odehrané	odehraný	k2eAgInPc1d1	odehraný
zápasy	zápas	k1gInPc1	zápas
<g/>
;	;	kIx,	;
V	v	k7c4	v
=	=	kIx~	=
Vítězství	vítězství	k1gNnSc4	vítězství
<g/>
;	;	kIx,	;
R	R	kA	R
=	=	kIx~	=
Remízy	remíza	k1gFnSc2	remíza
<g/>
;	;	kIx,	;
P	P	kA	P
=	=	kIx~	=
Prohry	prohra	k1gFnSc2	prohra
<g/>
;	;	kIx,	;
VG	VG	kA	VG
=	=	kIx~	=
Vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
;	;	kIx,	;
OG	OG	kA	OG
=	=	kIx~	=
Obdržené	obdržený	k2eAgInPc1d1	obdržený
góly	gól	k1gInPc1	gól
<g/>
;	;	kIx,	;
B	B	kA	B
=	=	kIx~	=
Body	bod	k1gInPc4	bod
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
=	=	kIx~	=
Mužstvo	mužstvo	k1gNnSc1	mužstvo
sestoupivší	sestoupivší	k2eAgNnSc1d1	sestoupivší
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
)	)	kIx)	)
=	=	kIx~	=
Mužstvo	mužstvo	k1gNnSc1	mužstvo
postoupivší	postoupivší	k2eAgNnSc1d1	postoupivší
z	z	k7c2	z
nižší	nízký	k2eAgFnSc2d2	nižší
soutěže	soutěž	k1gFnSc2	soutěž
(	(	kIx(	(
<g/>
nováček	nováček	k1gMnSc1	nováček
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
O	o	k7c6	o
pořadí	pořadí	k1gNnSc6	pořadí
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
bilance	bilance	k1gFnSc1	bilance
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
<g/>
:	:	kIx,	:
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
-	-	kIx~	-
Olomouc	Olomouc	k1gFnSc1	Olomouc
B	B	kA	B
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
B	B	kA	B
-	-	kIx~	-
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
</s>
</p>
<p>
<s>
O	o	k7c6	o
pořadí	pořadí	k1gNnSc6	pořadí
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
bilance	bilance	k1gFnSc1	bilance
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
<g/>
:	:	kIx,	:
Zlín	Zlín	k1gInSc1	Zlín
B	B	kA	B
-	-	kIx~	-
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
-	-	kIx~	-
Zlín	Zlín	k1gInSc1	Zlín
B	B	kA	B
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
</s>
</p>
<p>
<s>
O	o	k7c6	o
pořadí	pořadí	k1gNnSc6	pořadí
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
bilance	bilance	k1gFnSc1	bilance
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
<g/>
:	:	kIx,	:
Hulín	Hulín	k1gInSc1	Hulín
-	-	kIx~	-
Slovácko	Slovácko	k1gNnSc1	Slovácko
B	B	kA	B
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Slovácko	Slovácko	k1gNnSc1	Slovácko
B	B	kA	B
-	-	kIx~	-
Hulín	Hulín	k1gInSc1	Hulín
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
FC	FC	kA	FC
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
a	a	k8xC	a
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
prodal	prodat	k5eAaPmAgInS	prodat
divizní	divizní	k2eAgInSc1d1	divizní
práva	právo	k1gNnSc2	právo
Prostějovu	Prostějov	k1gInSc3	Prostějov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soupisky	soupiska	k1gFnPc4	soupiska
mužstev	mužstvo	k1gNnPc2	mužstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
SFC	SFC	kA	SFC
Opava	Opava	k1gFnSc1	Opava
===	===	k?	===
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Květon	Květon	k1gInSc1	Květon
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
–	–	k?	–
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Cigánek	cigánka	k1gFnPc2	cigánka
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Čáp	Čáp	k1gMnSc1	Čáp
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
René	René	k1gMnSc1	René
Formánek	Formánek	k1gMnSc1	Formánek
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Furik	Furik	k1gMnSc1	Furik
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Furik	Furik	k1gMnSc1	Furik
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Halaška	Halašek	k1gMnSc2	Halašek
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Chmelíček	chmelíček	k1gInSc1	chmelíček
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
Kiša	Kiša	k1gMnSc1	Kiša
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Klesnil	Klesnil	k1gMnSc1	Klesnil
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lubor	Lubor	k1gMnSc1	Lubor
Knapp	Knapp	k1gMnSc1	Knapp
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kolínek	Kolínek	k1gMnSc1	Kolínek
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Korčián	Korčián	k1gMnSc1	Korčián
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
Křeček	křeček	k1gMnSc1	křeček
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Neubert	Neubert	k1gMnSc1	Neubert
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Partyš	Partyš	k1gMnSc1	Partyš
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lumír	Lumír	k1gMnSc1	Lumír
Sedláček	Sedláček	k1gMnSc1	Sedláček
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Schaffartzik	Schaffartzik	k1gMnSc1	Schaffartzik
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Švrček	Švrček	k1gMnSc1	Švrček
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Vyskočil	Vyskočil	k1gMnSc1	Vyskočil
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Robin	robin	k2eAgInSc1d1	robin
Wirth	Wirth	k1gInSc1	Wirth
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Zapletal	Zapletal	k1gMnSc1	Zapletal
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dušan	Dušan	k1gMnSc1	Dušan
Žmolík	Žmolík	k1gMnSc1	Žmolík
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
–	–	k?	–
</s>
</p>
<p>
<s>
trenér	trenér	k1gMnSc1	trenér
Josef	Josef	k1gMnSc1	Josef
Mazura	Mazura	k1gMnSc1	Mazura
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Zabloudil	Zabloudil	k1gMnSc1	Zabloudil
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Čapka	Čapka	k1gMnSc1	Čapka
<g/>
:	:	kIx,	:
100	[number]	k4	100
let	léto	k1gNnPc2	léto
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
FC	FC	kA	FC
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
CERM	CERM	kA	CERM
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
MSFL	MSFL	kA	MSFL
má	mít	k5eAaImIp3nS	mít
letos	letos	k6eAd1	letos
velkého	velký	k2eAgMnSc2d1	velký
favorita	favorit	k1gMnSc2	favorit
a	a	k8xC	a
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
tři	tři	k4xCgInPc4	tři
až	až	k9	až
čtyři	čtyři	k4xCgInPc4	čtyři
očekávané	očekávaný	k2eAgInPc4d1	očekávaný
sekundanty	sekundant	k1gMnPc7	sekundant
</s>
</p>
<p>
<s>
MSFL	MSFL	kA	MSFL
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
ve	v	k7c6	v
výsledcích	výsledek	k1gInPc6	výsledek
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kol	kola	k1gFnPc2	kola
</s>
</p>
<p>
<s>
MSFL	MSFL	kA	MSFL
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
</s>
</p>
