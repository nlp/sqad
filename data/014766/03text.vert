<s>
SC	SC	kA
Braga	Braga	k1gFnSc1
</s>
<s>
Sporting	Sporting	k1gInSc1
BragaNázev	BragaNázev	k1gFnSc2
</s>
<s>
Sporting	Sporting	k1gInSc1
Club	club	k1gInSc1
de	de	k?
Braga	Braga	k1gFnSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Os	osa	k1gFnPc2
Arsenalistas	Arsenalistasa	k1gFnPc2
Země	zem	k1gFnSc2
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Braga	Braga	k1gFnSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
1921	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_scbraga	_scbraga	k1gFnSc1
<g/>
1011	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_scbraga	_scbraga	k1gFnSc1
<g/>
1011	#num#	k4
<g/>
a	a	k8xC
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
portugalská	portugalský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2019-20	2019-20	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Estádio	Estádio	k1gMnSc1
Municipal	Municipal	k1gMnSc1
de	de	k?
Braga	Braga	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
41	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
8	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
30	#num#	k4
154	#num#	k4
Vedení	vedení	k1gNnPc2
Předseda	předseda	k1gMnSc1
</s>
<s>
António	António	k1gMnSc1
Salvador	Salvador	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Carlos	Carlos	k1gMnSc1
Carvalhal	Carvalhal	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
</s>
<s>
SC	SC	kA
Braga	Braga	k1gFnSc1
</s>
<s>
Sporting	Sporting	k1gInSc1
Braga	Brag	k1gMnSc2
je	být	k5eAaImIp3nS
portugalský	portugalský	k2eAgInSc4d1
fotbalový	fotbalový	k2eAgInSc4d1
klub	klub	k1gInSc4
sídlící	sídlící	k2eAgInSc4d1
ve	v	k7c6
městě	město	k1gNnSc6
Braga	Braga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
založen	založit	k5eAaPmNgMnS
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
s	s	k7c7
názvem	název	k1gInSc7
Arsenal	Arsenal	k1gMnPc2
do	do	k7c2
Minho	Min	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejlepších	dobrý	k2eAgInPc2d3
týmů	tým	k1gInPc2
nejvyšší	vysoký	k2eAgFnSc2d3
portugalské	portugalský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
Primeira	Primeira	k1gFnSc1
Liga	liga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
tým	tým	k1gInSc1
účastnil	účastnit	k5eAaImAgInS
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skupině	skupina	k1gFnSc6
skončil	skončit	k5eAaPmAgInS
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
a	a	k8xC
pokračoval	pokračovat	k5eAaImAgMnS
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
probojoval	probojovat	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Primeira	Primeira	k1gFnSc1
Liga	liga	k1gFnSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Taça	Taça	k1gFnSc1
de	de	k?
Portugal	portugal	k1gInSc1
<g/>
:	:	kIx,
vítěz	vítěz	k1gMnSc1
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
,	,	kIx,
finalista	finalista	k1gMnSc1
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Taça	Taç	k2eAgFnSc1d1
da	da	k?
Liga	liga	k1gFnSc1
<g/>
:	:	kIx,
vítěz	vítěz	k1gMnSc1
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Supertaça	Supertaça	k6eAd1
Cândido	Cândida	k1gFnSc5
de	de	k?
Oliveira	Oliveira	k1gMnSc1
<g/>
:	:	kIx,
finalista	finalista	k1gMnSc1
1982	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
:	:	kIx,
finalista	finalista	k1gMnSc1
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
Intertoto	Intertota	k1gFnSc5
<g/>
:	:	kIx,
vítěz	vítěz	k1gMnSc1
2008	#num#	k4
</s>
<s>
Známí	známý	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Roland	Roland	k1gInSc1
Linz	Linza	k1gFnPc2
</s>
<s>
Stéphane	Stéphanout	k5eAaPmIp3nS
Demol	Demol	k1gInSc1
</s>
<s>
Artur	Artur	k1gMnSc1
</s>
<s>
Diego	Diego	k1gMnSc1
Costa	Costa	k1gMnSc1
</s>
<s>
Elpídio	Elpídio	k6eAd1
Silva	Silva	k1gFnSc1
</s>
<s>
Pena	Pena	k6eAd1
</s>
<s>
Kostadin	Kostadin	k2eAgInSc1d1
Kostadinov	Kostadinov	k1gInSc1
</s>
<s>
Pablo	Pablo	k1gNnSc1
Contreras	Contrerasa	k1gFnPc2
</s>
<s>
Miklós	Miklós	k6eAd1
Fehér	Fehér	k1gInSc1
</s>
<s>
Chiquinho	Chiquinha	k1gMnSc5
Conde	Cond	k1gMnSc5
</s>
<s>
Alberto	Alberta	k1gFnSc5
Rodríguez	Rodrígueza	k1gFnPc2
</s>
<s>
António	António	k1gMnSc1
Folha	Folha	k1gMnSc1
</s>
<s>
Eduardo	Eduardo	k1gNnSc1
</s>
<s>
Jaime	Jaimat	k5eAaPmIp3nS
Pacheco	Pacheco	k6eAd1
</s>
<s>
Joã	Joã	k1gMnSc1
Vieira	Vieira	k1gMnSc1
Pinto	pinta	k1gFnSc5
</s>
<s>
Jordã	Jordã	k6eAd1
</s>
<s>
José	José	k6eAd1
Nunes	Nunes	k1gInSc1
</s>
<s>
Nuno	Nuno	k1gMnSc1
Frechaut	Frechaut	k1gMnSc1
</s>
<s>
Ricardo	Ricardo	k1gNnSc1
Rocha	Roch	k1gMnSc2
</s>
<s>
Sílvio	Sílvio	k6eAd1
</s>
<s>
Tiago	Tiago	k6eAd1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
SC	SC	kA
Braga	Brag	k1gMnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Portugal	portugal	k1gInSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Cup	cup	k1gInSc1
Winners	Winners	k1gInSc1
<g/>
,	,	kIx,
RSSSF	RSSSF	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Portugal	portugal	k1gInSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
League	League	k1gInSc1
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc1
<g/>
,	,	kIx,
RSSSF	RSSSF	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
SC	SC	kA
Braga	Braga	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Primeira	Primeira	k1gFnSc1
Liga	liga	k1gFnSc1
–	–	k?
portugalská	portugalský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
ročníky	ročník	k1gInPc1
<g/>
)	)	kIx)
Kluby	klub	k1gInPc1
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
CF	CF	kA
Os	osa	k1gFnPc2
Belenenses	Belenensesa	k1gFnPc2
•	•	k?
Benfica	Benfic	k1gInSc2
Lisabon	Lisabon	k1gInSc1
•	•	k?
Boavista	Boavista	k1gMnSc1
FC	FC	kA
•	•	k?
SC	SC	kA
Braga	Braga	k1gFnSc1
•	•	k?
FC	FC	kA
Famalicã	Famalicã	k1gMnSc1
•	•	k?
Gil	Gil	k1gMnSc5
Vicente	Vicent	k1gMnSc5
FC	FC	kA
•	•	k?
CS	CS	kA
Marítimo	Marítima	k1gFnSc5
•	•	k?
Moreirense	Moreirense	k1gFnPc4
FC	FC	kA
•	•	k?
FC	FC	kA
Paços	Paços	k1gMnSc1
de	de	k?
Ferreira	Ferreira	k1gMnSc1
•	•	k?
Portimonense	Portimonense	k1gFnSc2
SC	SC	kA
•	•	k?
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
Rio	Rio	k1gMnSc1
Ave	ave	k1gNnSc2
FC	FC	kA
•	•	k?
CD	CD	kA
Santa	Santa	k1gFnSc1
Clara	Clara	k1gFnSc1
•	•	k?
Sporting	Sporting	k1gInSc1
CP	CP	kA
•	•	k?
CD	CD	kA
Tondela	Tondela	k1gFnSc1
•	•	k?
Vitória	Vitórium	k1gNnSc2
de	de	k?
Guimarã	Guimarã	k1gMnSc1
•	•	k?
SC	SC	kA
Farense	Farense	k1gFnSc2
•	•	k?
CD	CD	kA
Nacional	Nacional	k1gFnSc2
Sezóny	sezóna	k1gFnSc2
</s>
<s>
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
•	•	k?
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
•	•	k?
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
•	•	k?
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
1952	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
53	#num#	k4
•	•	k?
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
•	•	k?
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
•	•	k?
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
•	•	k?
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
<g/>
:	:	kIx,
Portugalský	portugalský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Portugalský	portugalský	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Portugalský	portugalský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Portugalsko	Portugalsko	k1gNnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
126060310	#num#	k4
</s>
