<p>
<s>
John	John	k1gMnSc1	John
Quincy	Quinca	k1gFnSc2	Quinca
Adams	Adams	k1gInSc1	Adams
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1767	[number]	k4	1767
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
šestý	šestý	k4xOgMnSc1	šestý
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
také	také	k9	také
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgInSc1	druhý
je	být	k5eAaImIp3nS	být
George	George	k1gFnPc2	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Briantree	Briantree	k1gFnSc6	Briantree
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
část	část	k1gFnSc1	část
Quincy	Quinca	k1gFnSc2	Quinca
<g/>
)	)	kIx)	)
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
druhého	druhý	k4xOgMnSc4	druhý
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
Johna	John	k1gMnSc4	John
Adamse	Adams	k1gMnSc4	Adams
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Leidenu	Leiden	k1gInSc2	Leiden
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
Harvardovu	Harvardův	k2eAgFnSc4d1	Harvardova
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
prováděl	provádět	k5eAaImAgInS	provádět
právnickou	právnický	k2eAgFnSc4d1	právnická
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
čtrnáctiletý	čtrnáctiletý	k2eAgMnSc1d1	čtrnáctiletý
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tajemníkem	tajemník	k1gMnSc7	tajemník
amerického	americký	k2eAgMnSc2d1	americký
emisara	emisar	k1gMnSc2	emisar
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
delegace	delegace	k1gFnSc2	delegace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
vyjednat	vyjednat	k5eAaPmF	vyjednat
mír	mír	k1gInSc4	mír
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1794	[number]	k4	1794
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
,	,	kIx,	,
1796	[number]	k4	1796
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
a	a	k8xC	a
1797	[number]	k4	1797
do	do	k7c2	do
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
byl	být	k5eAaImAgMnS	být
senátorem	senátor	k1gMnSc7	senátor
státu	stát	k1gInSc2	stát
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1803	[number]	k4	1803
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
senátorem	senátor	k1gMnSc7	senátor
celostátního	celostátní	k2eAgInSc2d1	celostátní
Senátu	senát	k1gInSc2	senát
za	za	k7c4	za
federalisty	federalista	k1gMnPc4	federalista
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
a	a	k8xC	a
přešel	přejít	k5eAaPmAgMnS	přejít
k	k	k7c3	k
republikánům	republikán	k1gMnPc3	republikán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1808	[number]	k4	1808
<g/>
–	–	k?	–
<g/>
1814	[number]	k4	1814
byl	být	k5eAaImAgInS	být
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1817	[number]	k4	1817
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
1817	[number]	k4	1817
<g/>
–	–	k?	–
<g/>
1825	[number]	k4	1825
byl	být	k5eAaImAgMnS	být
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
takový	takový	k3xDgInSc4	takový
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
Floridu	Florida	k1gFnSc4	Florida
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Monroeově	Monroeův	k2eAgFnSc6d1	Monroeova
doktríně	doktrína	k1gFnSc6	doktrína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odmítala	odmítat	k5eAaImAgFnS	odmítat
vliv	vliv	k1gInSc4	vliv
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezidentem	prezident	k1gMnSc7	prezident
==	==	k?	==
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
praxi	praxe	k1gFnSc3	praxe
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
John	John	k1gMnSc1	John
Quincy	Quinca	k1gFnSc2	Quinca
Adams	Adams	k1gInSc1	Adams
stal	stát	k5eAaPmAgMnS	stát
ideálním	ideální	k2eAgMnSc7d1	ideální
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
Andrewem	Andrew	k1gMnSc7	Andrew
Jacksonem	Jackson	k1gMnSc7	Jackson
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
byli	být	k5eAaImAgMnP	být
ze	z	k7c2	z
stejné	stejný	k2eAgFnSc2d1	stejná
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
a	a	k8xC	a
porazil	porazit	k5eAaPmAgMnS	porazit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
udělal	udělat	k5eAaPmAgInS	udělat
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
z	z	k7c2	z
Jacksona	Jackson	k1gMnSc2	Jackson
mocného	mocný	k2eAgMnSc2d1	mocný
a	a	k8xC	a
schopného	schopný	k2eAgMnSc2d1	schopný
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jeho	on	k3xPp3gNnSc2	on
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
mařil	mařit	k5eAaImAgInS	mařit
jeho	jeho	k3xOp3gInPc4	jeho
plány	plán	k1gInPc4	plán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
spor	spor	k1gInSc1	spor
vedl	vést	k5eAaImAgInS	vést
až	až	k9	až
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
stávajících	stávající	k2eAgMnPc2d1	stávající
republikánů	republikán	k1gMnPc2	republikán
na	na	k7c4	na
Národní	národní	k2eAgMnPc4d1	národní
republikány	republikán	k1gMnPc4	republikán
(	(	kIx(	(
<g/>
podporující	podporující	k2eAgFnPc4d1	podporující
Adamse	Adamse	k1gFnPc4	Adamse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
dnešní	dnešní	k2eAgMnPc1d1	dnešní
republikáni	republikán	k1gMnPc1	republikán
a	a	k8xC	a
Demokratické	demokratický	k2eAgMnPc4d1	demokratický
republikány	republikán	k1gMnPc4	republikán
(	(	kIx(	(
<g/>
podporující	podporující	k2eAgFnSc1d1	podporující
Jacksona	Jacksona	k1gFnSc1	Jacksona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
dnešní	dnešní	k2eAgMnPc1d1	dnešní
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Porážka	porážka	k1gFnSc1	porážka
==	==	k?	==
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
udály	udát	k5eAaPmAgFnP	udát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
však	však	k9	však
Jackson	Jackson	k1gInSc1	Jackson
Adamse	Adams	k1gMnSc2	Adams
porazil	porazit	k5eAaPmAgInS	porazit
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
vděčil	vděčit	k5eAaImAgMnS	vděčit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
bezohledné	bezohledný	k2eAgFnSc3d1	bezohledná
diskreditační	diskreditační	k2eAgFnSc3d1	diskreditační
kampani	kampaň	k1gFnSc3	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Adams	Adams	k6eAd1	Adams
však	však	k9	však
proti	proti	k7c3	proti
tradici	tradice	k1gFnSc3	tradice
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
nástupci	nástupce	k1gMnSc3	nástupce
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
také	také	k6eAd1	také
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
jet	jet	k5eAaImF	jet
do	do	k7c2	do
Kapitolu	Kapitol	k1gInSc2	Kapitol
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
voze	vůz	k1gInSc6	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gInSc7	člen
Kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jím	jíst	k5eAaImIp1nS	jíst
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
Protizednářskou	Protizednářský	k2eAgFnSc4d1	Protizednářský
stranu	strana	k1gFnSc4	strana
na	na	k7c4	na
guvernéra	guvernér	k1gMnSc4	guvernér
státu	stát	k1gInSc2	stát
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
na	na	k7c4	na
následky	následek	k1gInPc4	následek
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
ranila	ranit	k5eAaPmAgFnS	ranit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
dříve	dříve	k6eAd2	dříve
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
John	John	k1gMnSc1	John
Quincy	Quinca	k1gFnSc2	Quinca
Adams	Adamsa	k1gFnPc2	Adamsa
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
John	John	k1gMnSc1	John
Quincy	Quinca	k1gFnSc2	Quinca
Adams	Adams	k1gInSc1	Adams
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
John	John	k1gMnSc1	John
Quincy	Quinca	k1gFnSc2	Quinca
Adams	Adamsa	k1gFnPc2	Adamsa
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
–	–	k?	–
John	John	k1gMnSc1	John
Quincy	Quinca	k1gFnSc2	Quinca
Adams	Adams	k1gInSc1	Adams
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Encyclopæ	Encyclopæ	k1gMnSc1	Encyclopæ
Britannica	Britannica	k1gMnSc1	Britannica
–	–	k?	–
John	John	k1gMnSc1	John
Quincy	Quinca	k1gFnSc2	Quinca
Adams	Adams	k1gInSc1	Adams
</s>
</p>
