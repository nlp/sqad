<s>
Jaké	jaký	k3yIgNnSc1	jaký
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc4d1	souhrnné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
náboženské	náboženský	k2eAgInPc4d1	náboženský
a	a	k8xC	a
světové	světový	k2eAgInPc4d1	světový
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
racionalistickou	racionalistický	k2eAgFnSc7d1	racionalistická
kritikou	kritika	k1gFnSc7	kritika
křesťanství	křesťanství	k1gNnSc2	křesťanství
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
?	?	kIx.	?
</s>
