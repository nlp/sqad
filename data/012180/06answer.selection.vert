<s>
Deismus	deismus	k1gInSc1	deismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
deus	deus	k6eAd1	deus
–	–	k?	–
Bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc4d1	souhrnné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
náboženské	náboženský	k2eAgInPc4d1	náboženský
a	a	k8xC	a
světové	světový	k2eAgInPc4d1	světový
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
racionalistickou	racionalistický	k2eAgFnSc7d1	racionalistická
kritikou	kritika	k1gFnSc7	kritika
křesťanství	křesťanství	k1gNnSc2	křesťanství
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyústily	vyústit	k5eAaPmAgFnP	vyústit
do	do	k7c2	do
francouzského	francouzský	k2eAgMnSc2d1	francouzský
a	a	k8xC	a
německého	německý	k2eAgNnSc2d1	německé
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
