<s>
Arabština	arabština	k1gFnSc1	arabština
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ا	ا	k?	ا
ا	ا	k?	ا
<g/>
,	,	kIx,	,
al-Lughat	al-Lughat	k5eAaPmF	al-Lughat
Al-Arabíja	Al-Arabíja	k1gFnSc1	Al-Arabíja
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
semitský	semitský	k2eAgInSc4d1	semitský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
značné	značný	k2eAgInPc4d1	značný
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
spisovnou	spisovný	k2eAgFnSc7d1	spisovná
arabštinou	arabština	k1gFnSc7	arabština
a	a	k8xC	a
regionálními	regionální	k2eAgInPc7d1	regionální
hovorovými	hovorový	k2eAgInPc7d1	hovorový
jazyky	jazyk	k1gInPc7	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
egyptskou	egyptský	k2eAgFnSc7d1	egyptská
<g/>
,	,	kIx,	,
syrskou	syrský	k2eAgFnSc7d1	Syrská
<g/>
,	,	kIx,	,
iráckou	irácký	k2eAgFnSc7d1	irácká
<g/>
,	,	kIx,	,
marockou	marocký	k2eAgFnSc7d1	marocká
hovorovou	hovorový	k2eAgFnSc7d1	hovorová
arabštinou	arabština	k1gFnSc7	arabština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
arabština	arabština	k1gFnSc1	arabština
se	se	k3xPyFc4	se
jen	jen	k9	jen
málo	málo	k6eAd1	málo
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
jazyka	jazyk	k1gInSc2	jazyk
Koránu	korán	k1gInSc2	korán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aktivně	aktivně	k6eAd1	aktivně
ji	on	k3xPp3gFnSc4	on
ovládají	ovládat	k5eAaImIp3nP	ovládat
pouze	pouze	k6eAd1	pouze
vzdělanci	vzdělanec	k1gMnPc1	vzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
hovorových	hovorový	k2eAgInPc6d1	hovorový
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
podobné	podobný	k2eAgFnPc1d1	podobná
odstředivé	odstředivý	k2eAgFnPc1d1	odstředivá
tendence	tendence	k1gFnPc1	tendence
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc4	jaký
odpoutaly	odpoutat	k5eAaPmAgInP	odpoutat
románské	románský	k2eAgInPc1d1	románský
jazyky	jazyk	k1gInPc1	jazyk
od	od	k7c2	od
kdysi	kdysi	k6eAd1	kdysi
jednotné	jednotný	k2eAgFnSc2d1	jednotná
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
však	však	k9	však
rozdíly	rozdíl	k1gInPc1	rozdíl
jen	jen	k6eAd1	jen
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
projevují	projevovat	k5eAaImIp3nP	projevovat
v	v	k7c6	v
psané	psaný	k2eAgFnSc6d1	psaná
formě	forma	k1gFnSc6	forma
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přestože	přestože	k8xS	přestože
někteří	některý	k3yIgMnPc1	některý
spisovatelé	spisovatel	k1gMnPc1	spisovatel
píší	psát	k5eAaImIp3nP	psát
v	v	k7c6	v
regionálních	regionální	k2eAgFnPc6d1	regionální
variantách	varianta	k1gFnPc6	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
–	–	k?	–
arabské	arabský	k2eAgNnSc4d1	arabské
písmo	písmo	k1gNnSc4	písmo
nezachytí	zachytit	k5eNaPmIp3nS	zachytit
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
krátkých	krátký	k2eAgFnPc6d1	krátká
samohláskách	samohláska	k1gFnPc6	samohláska
<g/>
,	,	kIx,	,
pozměněné	pozměněný	k2eAgFnPc1d1	pozměněná
souhlásky	souhláska	k1gFnPc1	souhláska
si	se	k3xPyFc3	se
zase	zase	k9	zase
ponechávají	ponechávat	k5eAaImIp3nP	ponechávat
původní	původní	k2eAgInSc4d1	původní
zápis	zápis	k1gInSc4	zápis
(	(	kIx(	(
<g/>
např.	např.	kA	např.
znak	znak	k1gInSc1	znak
ج	ج	k?	ج
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
přepisovaný	přepisovaný	k2eAgInSc4d1	přepisovaný
a	a	k8xC	a
čtený	čtený	k2eAgInSc4d1	čtený
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
dž	dž	k?	dž
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
[	[	kIx(	[
<g/>
g	g	kA	g
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
pořád	pořád	k6eAd1	pořád
stejně	stejně	k6eAd1	stejně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
nápisy	nápis	k1gInPc1	nápis
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
arabština	arabština	k1gFnSc1	arabština
je	být	k5eAaImIp3nS	být
dochována	dochovat	k5eAaPmNgFnS	dochovat
ve	v	k7c6	v
staroarabské	staroarabský	k2eAgFnSc6d1	staroarabský
poezii	poezie	k1gFnSc6	poezie
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Koránu	korán	k1gInSc6	korán
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
gramatických	gramatický	k2eAgNnPc2d1	gramatické
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
úředním	úřední	k2eAgInSc7d1	úřední
a	a	k8xC	a
literárním	literární	k2eAgInSc7d1	literární
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
Araby	Arab	k1gMnPc7	Arab
dobytém	dobytý	k2eAgNnSc6d1	dobyté
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
používali	používat	k5eAaImAgMnP	používat
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
příslušníci	příslušník	k1gMnPc1	příslušník
jiných	jiný	k2eAgInPc2d1	jiný
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
od	od	k7c2	od
Arabů	Arab	k1gMnPc2	Arab
přejali	přejmout	k5eAaPmAgMnP	přejmout
islám	islám	k1gInSc4	islám
(	(	kIx(	(
<g/>
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
Peršané	Peršan	k1gMnPc1	Peršan
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
poezie	poezie	k1gFnSc2	poezie
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
klasická	klasický	k2eAgFnSc1d1	klasická
arabská	arabský	k2eAgFnSc1d1	arabská
literatura	literatura	k1gFnSc1	literatura
také	také	k9	také
řadu	řada	k1gFnSc4	řada
děl	dělo	k1gNnPc2	dělo
naučných	naučný	k2eAgFnPc6d1	naučná
(	(	kIx(	(
<g/>
historických	historický	k2eAgFnPc6d1	historická
<g/>
,	,	kIx,	,
geografických	geografický	k2eAgInPc2d1	geografický
<g/>
,	,	kIx,	,
lékařských	lékařský	k2eAgInPc2d1	lékařský
<g/>
,	,	kIx,	,
filozofických	filozofický	k2eAgInPc2d1	filozofický
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
arabským	arabský	k2eAgInPc3d1	arabský
překladům	překlad	k1gInPc3	překlad
přežilo	přežít	k5eAaPmAgNnS	přežít
evropský	evropský	k2eAgInSc4d1	evropský
středověk	středověk	k1gInSc4	středověk
nejedno	nejeden	k4xCyIgNnSc1	nejeden
dílo	dílo	k1gNnSc4	dílo
antických	antický	k2eAgMnPc2d1	antický
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
modernizuje	modernizovat	k5eAaBmIp3nS	modernizovat
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
<g/>
,	,	kIx,	,
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
se	se	k3xPyFc4	se
styl	styl	k1gInSc1	styl
i	i	k8xC	i
skladba	skladba	k1gFnSc1	skladba
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
moderní	moderní	k2eAgFnSc1d1	moderní
spisovná	spisovný	k2eAgFnSc1d1	spisovná
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
oproti	oproti	k7c3	oproti
arabštině	arabština	k1gFnSc3	arabština
klasické	klasický	k2eAgFnSc3d1	klasická
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgFnPc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
převzaty	převzít	k5eAaPmNgInP	převzít
z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
<g/>
:	:	kIx,	:
kafe	kafe	k?	kafe
<g/>
,	,	kIx,	,
žirafa	žirafa	k1gFnSc1	žirafa
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc1	cukr
<g/>
,	,	kIx,	,
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
sirup	sirup	k1gInSc1	sirup
<g/>
,	,	kIx,	,
algebra	algebra	k1gFnSc1	algebra
<g/>
,	,	kIx,	,
gazela	gazela	k1gFnSc1	gazela
<g/>
,	,	kIx,	,
sofa	sofa	k1gFnSc1	sofa
<g/>
,	,	kIx,	,
ghúl	ghúl	k1gMnSc1	ghúl
<g/>
,	,	kIx,	,
admirál	admirál	k1gMnSc1	admirál
<g/>
.	.	kIx.	.
</s>
<s>
Arabština	arabština	k1gFnSc1	arabština
je	být	k5eAaImIp3nS	být
flexivní	flexivní	k2eAgInSc4d1	flexivní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ohýbá	ohýbat	k5eAaImIp3nS	ohýbat
slova	slovo	k1gNnPc4	slovo
pomocí	pomocí	k7c2	pomocí
předpon	předpona	k1gFnPc2	předpona
<g/>
,	,	kIx,	,
přípon	přípona	k1gFnPc2	přípona
a	a	k8xC	a
zejména	zejména	k9	zejména
změnami	změna	k1gFnPc7	změna
uvnitř	uvnitř	k7c2	uvnitř
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
v	v	k7c6	v
indoevropských	indoevropský	k2eAgInPc6d1	indoevropský
jazycích	jazyk	k1gInPc6	jazyk
nepříliš	příliš	k6eNd1	příliš
obvyklými	obvyklý	k2eAgInPc7d1	obvyklý
<g/>
:	:	kIx,	:
kitábun	kitábun	k1gInSc1	kitábun
=	=	kIx~	=
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
kutubun	kutubun	k1gInSc1	kutubun
=	=	kIx~	=
knihy	kniha	k1gFnPc1	kniha
(	(	kIx(	(
<g/>
kmen	kmen	k1gInSc1	kmen
KTB	KTB	kA	KTB
<g/>
)	)	kIx)	)
rasala	rasal	k1gMnSc2	rasal
=	=	kIx~	=
poslal	poslat	k5eAaPmAgMnS	poslat
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ursila	ursila	k1gFnSc1	ursila
=	=	kIx~	=
byl	být	k5eAaImAgInS	být
poslán	poslán	k2eAgInSc1d1	poslán
(	(	kIx(	(
<g/>
kmen	kmen	k1gInSc1	kmen
RSL	RSL	kA	RSL
<g/>
)	)	kIx)	)
kabírun	kabírun	k1gMnSc1	kabírun
=	=	kIx~	=
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
akbar	akbar	k1gInSc1	akbar
=	=	kIx~	=
větší	veliký	k2eAgInSc1d2	veliký
(	(	kIx(	(
<g/>
kmen	kmen	k1gInSc1	kmen
KBR	KBR	kA	KBR
<g/>
)	)	kIx)	)
madínatun	madínatun	k1gMnSc1	madínatun
=	=	kIx~	=
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
mudunun	mudunun	k1gInSc1	mudunun
=	=	kIx~	=
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
kmen	kmen	k1gInSc1	kmen
MDN	MDN	kA	MDN
<g/>
)	)	kIx)	)
</s>
<s>
Časy	čas	k1gInPc4	čas
má	mít	k5eAaImIp3nS	mít
arabština	arabština	k1gFnSc1	arabština
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
(	(	kIx(	(
<g/>
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
a	a	k8xC	a
dokončený	dokončený	k2eAgInSc1d1	dokončený
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
pády	pád	k1gInPc4	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
a	a	k8xC	a
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
arabština	arabština	k1gFnSc1	arabština
je	být	k5eAaImIp3nS	být
vidový	vidový	k2eAgInSc4d1	vidový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
protikladu	protiklad	k1gInSc6	protiklad
ukončeného	ukončený	k2eAgInSc2d1	ukončený
a	a	k8xC	a
neukončeného	ukončený	k2eNgInSc2d1	neukončený
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Českému	český	k2eAgInSc3d1	český
minulému	minulý	k2eAgInSc3d1	minulý
času	čas	k1gInSc3	čas
přibližně	přibližně	k6eAd1	přibližně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
arabské	arabský	k2eAgNnSc4d1	arabské
perfektum	perfektum	k1gNnSc4	perfektum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
způsob	způsob	k1gInSc1	způsob
(	(	kIx(	(
<g/>
indikativ	indikativ	k1gInSc1	indikativ
–	–	k?	–
oznamovací	oznamovací	k2eAgInSc1d1	oznamovací
způsob	způsob	k1gInSc1	způsob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
perfektu	perfektum	k1gNnSc3	perfektum
je	být	k5eAaImIp3nS	být
imperfektum	imperfektum	k1gNnSc4	imperfektum
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
český	český	k2eAgInSc4d1	český
přítomný	přítomný	k2eAgInSc4d1	přítomný
a	a	k8xC	a
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Imperfektum	imperfektum	k1gNnSc1	imperfektum
tvoří	tvořit	k5eAaImIp3nS	tvořit
pět	pět	k4xCc4	pět
způsobů	způsob	k1gInPc2	způsob
(	(	kIx(	(
<g/>
indikativ	indikativ	k1gInSc1	indikativ
<g/>
,	,	kIx,	,
subjunktiv	subjunktiv	k1gInSc1	subjunktiv
<g/>
,	,	kIx,	,
apokopát	apokopát	k1gInSc1	apokopát
<g/>
,	,	kIx,	,
energikus	energikus	k1gInSc1	energikus
<g/>
,	,	kIx,	,
imperativ	imperativ	k1gInSc1	imperativ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
kataba	kataba	k1gMnSc1	kataba
=	=	kIx~	=
napsal	napsat	k5eAaBmAgInS	napsat
(	(	kIx(	(
<g/>
perfektní	perfektní	k2eAgInSc1d1	perfektní
tvar	tvar	k1gInSc1	tvar
<g/>
)	)	kIx)	)
jaktubu	jaktubat	k5eAaPmIp1nS	jaktubat
=	=	kIx~	=
píše	psát	k5eAaImIp3nS	psát
(	(	kIx(	(
<g/>
imperfektní	imperfektní	k2eAgInSc4d1	imperfektní
tvar	tvar	k1gInSc4	tvar
<g/>
)	)	kIx)	)
všimněte	všimnout	k5eAaPmRp2nP	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kořen	kořen	k1gInSc1	kořen
KTB	KTB	kA	KTB
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
nemění	měnit	k5eNaImIp3nS	měnit
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
uktub	uktubat	k5eAaPmRp2nS	uktubat
=	=	kIx~	=
napiš	napsat	k5eAaPmRp2nS	napsat
kátib	kátib	k1gInSc1	kátib
=	=	kIx~	=
píšící	píšící	k2eAgInSc1d1	píšící
kátib	kátib	k1gInSc1	kátib
=	=	kIx~	=
spisovatel	spisovatel	k1gMnSc1	spisovatel
kuttáb	kuttáb	k1gMnSc1	kuttáb
=	=	kIx~	=
spisovatelé	spisovatel	k1gMnPc1	spisovatel
kitáb	kitáb	k1gMnSc1	kitáb
=	=	kIx~	=
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
psaní	psaní	k1gNnSc1	psaní
Kořeny	kořen	k1gInPc4	kořen
slov	slovo	k1gNnPc2	slovo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
hodně	hodně	k6eAd1	hodně
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
ukázka	ukázka	k1gFnSc1	ukázka
(	(	kIx(	(
<g/>
kořen	kořen	k1gInSc1	kořen
SKN	SKN	kA	SKN
<g/>
:	:	kIx,	:
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
sakana	sakana	k1gFnSc1	sakana
=	=	kIx~	=
bydlel	bydlet	k5eAaImAgMnS	bydlet
(	(	kIx(	(
<g/>
perfektní	perfektní	k2eAgInSc4d1	perfektní
tvar	tvar	k1gInSc4	tvar
<g/>
)	)	kIx)	)
jaskunu	jaskuna	k1gFnSc4	jaskuna
=	=	kIx~	=
bydlí	bydlet	k5eAaImIp3nS	bydlet
(	(	kIx(	(
<g/>
imperfektní	imperfektní	k2eAgInSc1d1	imperfektní
tvar	tvar	k1gInSc1	tvar
<g/>
)	)	kIx)	)
sákin	sákin	k1gInSc1	sákin
=	=	kIx~	=
bydlící	bydlící	k2eAgMnPc1d1	bydlící
<g/>
,	,	kIx,	,
obyvatel	obyvatel	k1gMnSc1	obyvatel
sukkán	sukkán	k2eAgMnSc1d1	sukkán
=	=	kIx~	=
obyvatelé	obyvatel	k1gMnPc1	obyvatel
sakan	sakany	k1gInPc2	sakany
=	=	kIx~	=
bydliště	bydliště	k1gNnSc2	bydliště
<g/>
,	,	kIx,	,
bydlení	bydlení	k1gNnSc4	bydlení
Ukázka	ukázka	k1gFnSc1	ukázka
časování	časování	k1gNnSc2	časování
<g/>
:	:	kIx,	:
askun	askun	k1gInSc1	askun
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
bydlím	bydlet	k5eAaImIp1nS	bydlet
<g/>
)	)	kIx)	)
taskunu	taskuna	k1gFnSc4	taskuna
(	(	kIx(	(
<g/>
bydlíš	bydlet	k5eAaImIp2nS	bydlet
–	–	k?	–
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
taskunína	taskunína	k1gFnSc1	taskunína
(	(	kIx(	(
<g/>
bydlíš	bydlet	k5eAaImIp2nS	bydlet
–	–	k?	–
ženě	žena	k1gFnSc3	žena
<g/>
)	)	kIx)	)
jaskunu	jaskun	k1gInSc3	jaskun
(	(	kIx(	(
<g/>
bydlí	bydlet	k5eAaImIp3nS	bydlet
–	–	k?	–
on	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
taskunu	taskun	k1gInSc6	taskun
(	(	kIx(	(
<g/>
bydlí	bydlet	k5eAaImIp3nS	bydlet
–	–	k?	–
ona	onen	k3xDgFnSc1	onen
<g/>
)	)	kIx)	)
V	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dva	dva	k4xCgInPc1	dva
rody	rod	k1gInPc1	rod
–	–	k?	–
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
.	.	kIx.	.
</s>
<s>
Rodově	rodově	k6eAd1	rodově
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
podstatná	podstatný	k2eAgNnPc4d1	podstatné
i	i	k8xC	i
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
"	"	kIx"	"
<g/>
džamíl	džamíl	k1gMnSc1	džamíl
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
hezký	hezký	k2eAgMnSc1d1	hezký
<g/>
,	,	kIx,	,
když	když	k8xS	když
přidáme	přidat	k5eAaPmIp1nP	přidat
koncovku	koncovka	k1gFnSc4	koncovka
"	"	kIx"	"
<g/>
-a	-a	k?	-a
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
džamíla	džamíla	k6eAd1	džamíla
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
hezká	hezký	k2eAgFnSc1d1	hezká
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnSc2	číslo
má	mít	k5eAaImIp3nS	mít
arabština	arabština	k1gFnSc1	arabština
tři	tři	k4xCgFnPc4	tři
(	(	kIx(	(
<g/>
singulár	singulár	k1gInSc1	singulár
<g/>
,	,	kIx,	,
duál	duál	k1gInSc1	duál
<g/>
,	,	kIx,	,
plurál	plurál	k1gInSc1	plurál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Duál	duál	k1gInSc1	duál
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednoduše	jednoduše	k6eAd1	jednoduše
koncovkou	koncovka	k1gFnSc7	koncovka
"	"	kIx"	"
<g/>
-áni	-áni	k6eAd1	-áni
<g/>
"	"	kIx"	"
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rod	rod	k1gInSc4	rod
<g/>
:	:	kIx,	:
darsun	darsun	k1gInSc1	darsun
=	=	kIx~	=
lekce	lekce	k1gFnSc2	lekce
darsáni	darsán	k2eAgMnPc1d1	darsán
=	=	kIx~	=
dvě	dva	k4xCgFnPc1	dva
lekce	lekce	k1gFnPc1	lekce
Množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
–	–	k?	–
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
a	a	k8xC	a
vnější	vnější	k2eAgFnSc6d1	vnější
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
samohláskovými	samohláskový	k2eAgFnPc7d1	samohlásková
změnami	změna	k1gFnPc7	změna
uvnitř	uvnitř	k7c2	uvnitř
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
souhláskovými	souhláskový	k2eAgInPc7d1	souhláskový
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgMnSc1d1	vnější
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
koncovkami	koncovka	k1gFnPc7	koncovka
podle	podle	k7c2	podle
rodu	rod	k1gInSc2	rod
<g/>
:	:	kIx,	:
mudarrisun	mudarrisun	k1gMnSc1	mudarrisun
=	=	kIx~	=
učitel	učitel	k1gMnSc1	učitel
mudarrisún	mudarrisún	k1gMnSc1	mudarrisún
=	=	kIx~	=
učitelé	učitel	k1gMnPc1	učitel
tilmídha	tilmídha	k1gMnSc1	tilmídha
=	=	kIx~	=
žákyně	žákyně	k1gFnSc1	žákyně
tilmídhát	tilmídhát	k1gMnSc1	tilmídhát
=	=	kIx~	=
žákyně	žákyně	k1gFnSc1	žákyně
(	(	kIx(	(
<g/>
plurál	plurál	k1gInSc1	plurál
<g/>
)	)	kIx)	)
Hlavní	hlavní	k2eAgInSc1d1	hlavní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Arabské	arabský	k2eAgNnSc1d1	arabské
písmo	písmo	k1gNnSc1	písmo
Arabština	arabština	k1gFnSc1	arabština
používá	používat	k5eAaImIp3nS	používat
řadu	řada	k1gFnSc4	řada
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
žádném	žádný	k3yNgInSc6	žádný
jiném	jiný	k2eAgInSc6d1	jiný
evropském	evropský	k2eAgInSc6d1	evropský
jazyce	jazyk	k1gInSc6	jazyk
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
emfatické	emfatický	k2eAgFnSc2d1	emfatická
souhlásky	souhláska	k1gFnSc2	souhláska
t	t	k?	t
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
,	,	kIx,	,
s	s	k7c7	s
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
neemfatický	emfatický	k2eNgInSc4d1	emfatický
protějšek	protějšek	k1gInSc4	protějšek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
emfatickým	emfatický	k2eAgInSc7d1	emfatický
důrazem	důraz	k1gInSc7	důraz
–	–	k?	–
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
nich	on	k3xPp3gInPc6	on
lžícovitě	lžícovitě	k6eAd1	lžícovitě
prohnut	prohnut	k2eAgInSc1d1	prohnut
směrem	směr	k1gInSc7	směr
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
při	při	k7c6	při
ruském	ruský	k2eAgNnSc6d1	ruské
tvrdém	tvrdé	k1gNnSc6	tvrdé
"	"	kIx"	"
<g/>
l	l	kA	l
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
například	například	k6eAd1	například
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
ajn	ajn	k?	ajn
<g/>
"	"	kIx"	"
–	–	k?	–
ع	ع	k?	ع
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nejobtížnějším	obtížný	k2eAgInSc7d3	nejobtížnější
zvukem	zvuk	k1gInSc7	zvuk
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduše	jednoduše	k6eAd1	jednoduše
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
hrdelní	hrdelní	k2eAgNnSc1d1	hrdelní
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
–	–	k?	–
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
v	v	k7c6	v
hrdle	hrdla	k1gFnSc6	hrdla
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
silnou	silný	k2eAgFnSc4d1	silná
hrtanovou	hrtanový	k2eAgFnSc4d1	hrtanová
hlásku	hláska	k1gFnSc4	hláska
(	(	kIx(	(
<g/>
laryngálu	laryngála	k1gFnSc4	laryngála
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hlasnější	hlasný	k2eAgInSc1d2	hlasný
protějšek	protějšek	k1gInSc1	protějšek
<g/>
,	,	kIx,	,
obyčejně	obyčejně	k6eAd1	obyčejně
přepisovaný	přepisovaný	k2eAgInSc1d1	přepisovaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ghajn	ghajn	k1gInSc1	ghajn
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
někdy	někdy	k6eAd1	někdy
jako	jako	k9	jako
[	[	kIx(	[
<g/>
g	g	kA	g
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
jako	jako	k9	jako
[	[	kIx(	[
<g/>
gh	gh	k?	gh
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
arabštině	arabština	k1gFnSc6	arabština
ale	ale	k8xC	ale
jako	jako	k9	jako
francouzské	francouzský	k2eAgFnSc2d1	francouzská
[	[	kIx(	[
<g/>
r	r	kA	r
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
je	být	k5eAaImIp3nS	být
určitě	určitě	k6eAd1	určitě
také	také	k9	také
"	"	kIx"	"
<g/>
hamza	hamza	k1gFnSc1	hamza
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
hlasový	hlasový	k2eAgInSc1d1	hlasový
ráz	ráz	k1gInSc1	ráz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
jak	jak	k6eAd1	jak
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
či	či	k8xC	či
uprostřed	uprostřed	k7c2	uprostřed
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Arabské	arabský	k2eAgNnSc1d1	arabské
písmo	písmo	k1gNnSc1	písmo
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
nabatejské	nabatejský	k2eAgFnSc2d1	nabatejská
varianty	varianta	k1gFnSc2	varianta
kurzívy	kurzíva	k1gFnSc2	kurzíva
aramejského	aramejský	k2eAgNnSc2d1	aramejské
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Arabské	arabský	k2eAgNnSc1d1	arabské
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
kurzivní	kurzivní	k2eAgMnSc1d1	kurzivní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
tiskací	tiskací	k2eAgNnSc4d1	tiskací
či	či	k8xC	či
psací	psací	k2eAgNnSc4d1	psací
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
starých	starý	k2eAgFnPc6d1	stará
písmech	písmo	k1gNnPc6	písmo
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
ani	ani	k9	ani
malá	malý	k2eAgNnPc4d1	malé
a	a	k8xC	a
velká	velký	k2eAgNnPc4d1	velké
písmena	písmeno	k1gNnPc4	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
tedy	tedy	k9	tedy
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
starosemitského	starosemitský	k2eAgNnSc2d1	starosemitský
–	–	k?	–
fénického	fénický	k2eAgNnSc2d1	fénické
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
hebrejština	hebrejština	k1gFnSc1	hebrejština
či	či	k8xC	či
řecká	řecký	k2eAgFnSc1d1	řecká
alfabeta	alfabeta	k1gFnSc1	alfabeta
či	či	k8xC	či
latinka	latinka	k1gFnSc1	latinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
podobné	podobný	k2eAgNnSc1d1	podobné
evropským	evropský	k2eAgFnPc3d1	Evropská
písmům	písmo	k1gNnPc3	písmo
(	(	kIx(	(
<g/>
určité	určitý	k2eAgFnSc2d1	určitá
podobnosti	podobnost	k1gFnSc2	podobnost
se	se	k3xPyFc4	se
ale	ale	k9	ale
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
mezi	mezi	k7c7	mezi
arabským	arabský	k2eAgNnSc7d1	arabské
písmem	písmo	k1gNnSc7	písmo
a	a	k8xC	a
hebrejskou	hebrejský	k2eAgFnSc7d1	hebrejská
kurzívou	kurzíva	k1gFnSc7	kurzíva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
nepíší	psát	k5eNaImIp3nP	psát
znaky	znak	k1gInPc1	znak
pro	pro	k7c4	pro
krátké	krátký	k2eAgFnPc4d1	krátká
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
písmen	písmeno	k1gNnPc2	písmeno
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
dle	dle	k7c2	dle
jejich	jejich	k3xOp3gNnSc2	jejich
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
znak	znak	k1gInSc1	znak
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
podobu	podoba	k1gFnSc4	podoba
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
šest	šest	k4xCc1	šest
písmen	písmeno	k1gNnPc2	písmeno
(	(	kIx(	(
<g/>
ا	ا	k?	ا
<g/>
,	,	kIx,	,
د	د	k?	د
<g/>
,	,	kIx,	,
ذ	ذ	k?	ذ
<g/>
,	,	kIx,	,
ر	ر	k?	ر
<g/>
,	,	kIx,	,
ز	ز	k?	ز
<g/>
,	,	kIx,	,
و	و	k?	و
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
neváží	vážit	k5eNaImIp3nP	vážit
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
předchozímu	předchozí	k2eAgNnSc3d1	předchozí
písmenu	písmeno	k1gNnSc3	písmeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gNnSc4	on
připojit	připojit	k5eAaPmF	připojit
další	další	k2eAgNnSc4d1	další
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
koncovou	koncový	k2eAgFnSc4d1	koncová
a	a	k8xC	a
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
formu	forma	k1gFnSc4	forma
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rá	rá	k?	rá
(	(	kIx(	(
<g/>
ر	ر	k?	ر
<g/>
)	)	kIx)	)
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
noha	noha	k1gFnSc1	noha
ر	ر	k?	ر
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
zvláštností	zvláštnost	k1gFnSc7	zvláštnost
na	na	k7c6	na
písmu	písmo	k1gNnSc6	písmo
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
ligatury	ligatura	k1gFnSc2	ligatura
(	(	kIx(	(
<g/>
znaky	znak	k1gInPc1	znak
usnadňující	usnadňující	k2eAgInPc1d1	usnadňující
psaní	psaní	k1gNnSc3	psaní
určitých	určitý	k2eAgFnPc2d1	určitá
skladeb	skladba	k1gFnPc2	skladba
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
existuje	existovat	k5eAaImIp3nS	existovat
jediná	jediný	k2eAgFnSc1d1	jediná
povinná	povinný	k2eAgFnSc1d1	povinná
ligatura	ligatura	k1gFnSc1	ligatura
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
lám	lám	k?	lám
+	+	kIx~	+
alif	alif	k1gMnSc1	alif
(	(	kIx(	(
<g/>
lá	lá	k0	lá
<g/>
)	)	kIx)	)
–	–	k?	–
ل	ل	k?	ل
<g/>
)	)	kIx)	)
a	a	k8xC	a
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
psaní	psaní	k1gNnSc4	psaní
některých	některý	k3yIgFnPc2	některý
vazeb	vazba	k1gFnPc2	vazba
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
je	být	k5eAaImIp3nS	být
arabština	arabština	k1gFnSc1	arabština
úřední	úřední	k2eAgFnSc1d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
Lingvistika	lingvistika	k1gFnSc1	lingvistika
Arabské	arabský	k2eAgNnSc4d1	arabské
písmo	písmo	k1gNnSc4	písmo
Arabská	arabský	k2eAgFnSc1d1	arabská
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
Wiki	Wik	k1gFnSc2	Wik
<g/>
:	:	kIx,	:
Doporučení	doporučení	k1gNnSc1	doporučení
pro	pro	k7c4	pro
transkripci	transkripce	k1gFnSc4	transkripce
arabštiny	arabština	k1gFnSc2	arabština
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
arabština	arabština	k1gFnSc1	arabština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
arabština	arabština	k1gFnSc1	arabština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
