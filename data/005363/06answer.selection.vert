<s>
Arabské	arabský	k2eAgNnSc1d1	arabské
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
kurzivní	kurzivní	k2eAgMnSc1d1	kurzivní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
tiskací	tiskací	k2eAgNnSc4d1	tiskací
či	či	k8xC	či
psací	psací	k2eAgNnSc4d1	psací
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
starých	starý	k2eAgFnPc6d1	stará
písmech	písmo	k1gNnPc6	písmo
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
ani	ani	k9	ani
malá	malý	k2eAgNnPc4d1	malé
a	a	k8xC	a
velká	velký	k2eAgNnPc4d1	velké
písmena	písmeno	k1gNnPc4	písmeno
<g/>
.	.	kIx.	.
</s>
