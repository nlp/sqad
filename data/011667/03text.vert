<p>
<s>
Mlezivo	mlezivo	k1gNnSc1	mlezivo
(	(	kIx(	(
<g/>
též	též	k9	též
kolostrum	kolostrum	k1gNnSc1	kolostrum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
prvotní	prvotní	k2eAgNnSc4d1	prvotní
mléko	mléko	k1gNnSc4	mléko
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
v	v	k7c6	v
mléčné	mléčný	k2eAgFnSc6d1	mléčná
žláze	žláza	k1gFnSc6	žláza
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
produkováno	produkovat	k5eAaImNgNnS	produkovat
asi	asi	k9	asi
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
dní	den	k1gInPc2	den
po	po	k7c4	po
něm     on      k3xPgInSc6p3	něm     on      k3xPgInSc6p3	k4	něm     on      k3xPgInSc6p3
.	.	kIx.	.
</s>
<s>
Složením	složení	k1gNnSc7	složení
se	se	k3xPyFc4	se
kolostrum	kolostrum	k1gNnSc1	kolostrum
významně	významně	k6eAd1	významně
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
zralého	zralý	k2eAgNnSc2d1	zralé
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
mleziva	mlezivo	k1gNnSc2	mlezivo
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
z	z	k7c2	z
nezralého	zralý	k2eNgNnSc2d1	nezralé
mléka	mléko	k1gNnSc2	mléko
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
mléko	mléko	k1gNnSc1	mléko
zralé	zralý	k2eAgNnSc1d1	zralé
<g/>
.	.	kIx.	.
</s>
<s>
Kolostrum	kolostrum	k1gNnSc1	kolostrum
je	být	k5eAaImIp3nS	být
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
proteiny	protein	k1gInPc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porodu	porod	k1gInSc6	porod
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
u	u	k7c2	u
mláďat	mládě	k1gNnPc2	mládě
přirozeně	přirozeně	k6eAd1	přirozeně
pasivní	pasivní	k2eAgFnSc4d1	pasivní
imunitu	imunita	k1gFnSc4	imunita
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
příjem	příjem	k1gInSc1	příjem
hotových	hotový	k2eAgFnPc2d1	hotová
protilátek	protilátka	k1gFnPc2	protilátka
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
novorozená	novorozený	k2eAgNnPc1d1	novorozené
mláďata	mládě	k1gNnPc1	mládě
chrání	chránit	k5eAaImIp3nP	chránit
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
života	život	k1gInSc2	život
před	před	k7c7	před
infekcemi	infekce	k1gFnPc7	infekce
ze	z	k7c2	z
zevního	zevní	k2eAgNnSc2d1	zevní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
když	když	k8xS	když
ještě	ještě	k6eAd1	ještě
nejsou	být	k5eNaImIp3nP	být
schopna	schopen	k2eAgFnSc1d1	schopna
imunitní	imunitní	k2eAgFnSc1d1	imunitní
reakce	reakce	k1gFnSc1	reakce
a	a	k8xC	a
produkce	produkce	k1gFnSc1	produkce
vlastních	vlastní	k2eAgFnPc2d1	vlastní
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
Kolostrum	kolostrum	k1gNnSc1	kolostrum
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgInPc2d1	významný
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ovlivní	ovlivnit	k5eAaPmIp3nP	ovlivnit
imunitu	imunita	k1gFnSc4	imunita
novorozence	novorozenec	k1gMnSc2	novorozenec
celoživotně	celoživotně	k6eAd1	celoživotně
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
vitaminů	vitamin	k1gInPc2	vitamin
<g/>
,	,	kIx,	,
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
stopových	stopový	k2eAgInPc2d1	stopový
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
také	také	k9	také
významné	významný	k2eAgFnPc4d1	významná
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
růstové	růstový	k2eAgInPc4d1	růstový
faktory	faktor	k1gInPc4	faktor
a	a	k8xC	a
inhibitory	inhibitor	k1gInPc4	inhibitor
proteáz	proteáza	k1gFnPc2	proteáza
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgMnSc1d1	hrající
velmi	velmi	k6eAd1	velmi
výraznou	výrazný	k2eAgFnSc4d1	výrazná
roli	role	k1gFnSc4	role
v	v	k7c6	v
imunitním	imunitní	k2eAgInSc6d1	imunitní
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
protilátky	protilátka	k1gFnPc4	protilátka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
přímé	přímý	k2eAgInPc4d1	přímý
účinky	účinek	k1gInPc4	účinek
proti	proti	k7c3	proti
bakteriím	bakterie	k1gFnPc3	bakterie
<g/>
,	,	kIx,	,
virům	vir	k1gInPc3	vir
a	a	k8xC	a
plísním	plíseň	k1gFnPc3	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kolostru	kolostrum	k1gNnSc6	kolostrum
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
též	též	k9	též
laktoferrin	laktoferrin	k1gInSc1	laktoferrin
<g/>
,	,	kIx,	,
bioaktivní	bioaktivní	k2eAgInSc1d1	bioaktivní
glykol-protein	glykolrotein	k1gInSc1	glykol-protein
–	–	k?	–
u	u	k7c2	u
zdravých	zdravý	k2eAgMnPc2d1	zdravý
jedinců	jedinec	k1gMnPc2	jedinec
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
imunitu	imunita	k1gFnSc4	imunita
proti	proti	k7c3	proti
chřipkám	chřipka	k1gFnPc3	chřipka
<g/>
,	,	kIx,	,
nachlazením	nachlazení	k1gNnSc7	nachlazení
<g/>
,	,	kIx,	,
parazitům	parazit	k1gMnPc3	parazit
a	a	k8xC	a
infekčním	infekční	k2eAgFnPc3d1	infekční
bakteriím	bakterie	k1gFnPc3	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
při	při	k7c6	při
omezování	omezování	k1gNnSc6	omezování
růstu	růst	k1gInSc3	růst
metastáz	metastáza	k1gFnPc2	metastáza
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
redukovat	redukovat	k5eAaBmF	redukovat
plísně	plíseň	k1gFnPc4	plíseň
a	a	k8xC	a
omezuje	omezovat	k5eAaImIp3nS	omezovat
produkci	produkce	k1gFnSc4	produkce
volných	volný	k2eAgMnPc2d1	volný
radikálů	radikál	k1gMnPc2	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
příznivých	příznivý	k2eAgInPc2d1	příznivý
účinků	účinek	k1gInPc2	účinek
lactoferinu	lactoferin	k1gInSc2	lactoferin
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc4	jeho
schopnost	schopnost	k1gFnSc4	schopnost
redukovat	redukovat	k5eAaBmF	redukovat
záněty	zánět	k1gInPc4	zánět
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
regulace	regulace	k1gFnSc2	regulace
zánětlivých	zánětlivý	k2eAgInPc2d1	zánětlivý
cytokinů	cytokin	k1gInPc2	cytokin
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Interleukin-	Interleukin-	k1gFnSc1	Interleukin-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Interleukin-	Interleukin-	k1gFnSc1	Interleukin-
<g/>
6	[number]	k4	6
a	a	k8xC	a
faktor	faktor	k1gInSc1	faktor
nekrózy	nekróza	k1gFnSc2	nekróza
tumorů	tumor	k1gInPc2	tumor
(	(	kIx(	(
<g/>
TNF	TNF	kA	TNF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
jejich	jejich	k3xOp3gFnSc1	jejich
složka	složka	k1gFnSc1	složka
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
okolností	okolnost	k1gFnPc2	okolnost
potřebné	potřebný	k2eAgNnSc1d1	potřebné
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
může	moct	k5eAaImIp3nS	moct
škodit	škodit	k5eAaImF	škodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kolostru	kolostrum	k1gNnSc6	kolostrum
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitá	důležitý	k2eAgFnSc1d1	důležitá
skupina	skupina	k1gFnSc1	skupina
molekul	molekula	k1gFnPc2	molekula
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
PRP	PRP	kA	PRP
(	(	kIx(	(
<g/>
Proline-Rich-Polypeptides	Proline-Rich-Polypeptides	k1gMnSc1	Proline-Rich-Polypeptides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
výkonný	výkonný	k2eAgInSc1d1	výkonný
regulátor	regulátor	k1gInSc1	regulátor
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
odpovědi	odpověď	k1gFnSc6	odpověď
humorální	humorální	k2eAgFnSc2d1	humorální
složky	složka	k1gFnSc2	složka
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
stimulovaly	stimulovat	k5eAaImAgFnP	stimulovat
PRPs	PRPs	k1gInSc4	PRPs
nízkou	nízký	k2eAgFnSc7d1	nízká
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
utlumovaly	utlumovat	k5eAaImAgFnP	utlumovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
imunitní	imunitní	k2eAgFnSc4d1	imunitní
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
PRPs	PRPs	k6eAd1	PRPs
rovněž	rovněž	k9	rovněž
ukázaly	ukázat	k5eAaPmAgInP	ukázat
prospěšné	prospěšný	k2eAgInPc1d1	prospěšný
účinky	účinek	k1gInPc1	účinek
ve	v	k7c6	v
zrání	zrání	k1gNnSc6	zrání
thymocytů	thymocyt	k1gInPc2	thymocyt
a	a	k8xC	a
tvorbě	tvorba	k1gFnSc3	tvorba
T-supresorových	Tupresorův	k2eAgFnPc2d1	T-supresorův
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
nesporně	sporně	k6eNd1	sporně
přinese	přinést	k5eAaPmIp3nS	přinést
rovnováhu	rovnováha	k1gFnSc4	rovnováha
v	v	k7c6	v
tělových	tělový	k2eAgFnPc6d1	tělová
imunitních	imunitní	k2eAgFnPc6d1	imunitní
reakcích	reakce	k1gFnPc6	reakce
a	a	k8xC	a
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
cestu	cesta	k1gFnSc4	cesta
řešení	řešení	k1gNnSc2	řešení
autoimunních	autoimunní	k2eAgNnPc2d1	autoimunní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Růstové	růstový	k2eAgInPc1d1	růstový
faktory	faktor	k1gInPc1	faktor
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
hojení	hojení	k1gNnSc4	hojení
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
na	na	k7c4	na
vytváření	vytváření	k1gNnSc4	vytváření
svalové	svalový	k2eAgFnSc2d1	svalová
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
a	a	k8xC	a
na	na	k7c4	na
zvyšování	zvyšování	k1gNnSc4	zvyšování
výdrže	výdrž	k1gFnSc2	výdrž
a	a	k8xC	a
vitality	vitalita	k1gFnSc2	vitalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
látky	látka	k1gFnPc1	látka
obsažené	obsažený	k2eAgFnPc1d1	obsažená
v	v	k7c6	v
kolostru	kolostrum	k1gNnSc6	kolostrum
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
novorozenci	novorozenec	k1gMnPc1	novorozenec
přežít	přežít	k5eAaPmF	přežít
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ocitá	ocitat	k5eAaImIp3nS	ocitat
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RNDr.	RNDr.	kA	RNDr.
Joseph	Joseph	k1gMnSc1	Joseph
Smarda	Smarda	k1gMnSc1	Smarda
–	–	k?	–
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
imunoproteinového	imunoproteinový	k2eAgInSc2d1	imunoproteinový
doplňku	doplněk	k1gInSc2	doplněk
–	–	k?	–
Press	Press	k1gInSc1	Press
Realease	Realeasa	k1gFnSc3	Realeasa
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mlezivo	mlezivo	k1gNnSc4	mlezivo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Mlezivo	mlezivo	k1gNnSc4	mlezivo
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
