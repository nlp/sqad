<s>
Mlezivo	mlezivo	k1gNnSc1	mlezivo
(	(	kIx(	(
<g/>
též	též	k9	též
kolostrum	kolostrum	k1gNnSc1	kolostrum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
prvotní	prvotní	k2eAgNnSc4d1	prvotní
mléko	mléko	k1gNnSc4	mléko
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
v	v	k7c6	v
mléčné	mléčný	k2eAgFnSc6d1	mléčná
žláze	žláza	k1gFnSc6	žláza
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
produkováno	produkovat	k5eAaImNgNnS	produkovat
asi	asi	k9	asi
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
dní	den	k1gInPc2	den
po	po	k7c4	po
něm     on      k3xPgInSc6p3	něm     on      k3xPgInSc6p3	k4	něm     on      k3xPgInSc6p3
.	.	kIx.	.
</s>
