<s>
Husitský	husitský	k2eAgInSc1d1
bojový	bojový	k2eAgInSc1d1
vůz	vůz	k1gInSc1
</s>
<s>
Dobové	dobový	k2eAgNnSc1d1
vyobrazení	vyobrazení	k1gNnSc1
husitské	husitský	k2eAgFnSc2d1
vozové	vozový	k2eAgFnSc2d1
hradby	hradba	k1gFnSc2
</s>
<s>
Replika	replika	k1gFnSc1
husitského	husitský	k2eAgInSc2d1
vozu	vůz	k1gInSc2
</s>
<s>
Husitské	husitský	k2eAgInPc1d1
bojové	bojový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
byl	být	k5eAaImAgInS
středověký	středověký	k2eAgInSc1d1
bojový	bojový	k2eAgInSc1d1
prostředek	prostředek	k1gInSc1
používaný	používaný	k2eAgInSc1d1
husity	husita	k1gMnPc7
během	během	k7c2
husitských	husitský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívány	využíván	k2eAgInPc1d1
byly	být	k5eAaImAgInP
především	především	k9
pro	pro	k7c4
stavbu	stavba	k1gFnSc4
tzv.	tzv.	kA
vozových	vozový	k2eAgFnPc2d1
hradeb	hradba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
počátcích	počátek	k1gInPc6
byly	být	k5eAaImAgFnP
pravděpodobně	pravděpodobně	k6eAd1
obyčejnými	obyčejný	k2eAgInPc7d1
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
užívanými	užívaný	k2eAgInPc7d1
selskými	selský	k2eAgInPc7d1
vozy	vůz	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
postupem	postupem	k7c2
času	čas	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
základě	základ	k1gInSc6
zkušeností	zkušenost	k1gFnPc2
z	z	k7c2
bitev	bitva	k1gFnPc2
k	k	k7c3
rozlišení	rozlišení	k1gNnSc3
zásobovacích	zásobovací	k2eAgInPc2d1
a	a	k8xC
bojových	bojový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
účel	účel	k1gInSc4
byly	být	k5eAaImAgInP
využívány	využíván	k2eAgInPc1d1
větší	veliký	k2eAgInPc1d2
a	a	k8xC
pevnější	pevný	k2eAgInPc4d2
vozy	vůz	k1gInPc4
<g/>
,	,	kIx,
vybavené	vybavený	k2eAgInPc4d1
na	na	k7c6
stranách	strana	k1gFnPc6
deskou	deska	k1gFnSc7
ze	z	k7c2
silných	silný	k2eAgFnPc2d1
fošen	fošna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vybavení	vybavení	k1gNnSc3
bojových	bojový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
náleželo	náležet	k5eAaImAgNnS
silné	silný	k2eAgNnSc1d1
prkno	prkno	k1gNnSc1
zavěšené	zavěšený	k2eAgNnSc1d1
ze	z	k7c2
spodu	spod	k1gInSc2
podél	podél	k6eAd1
mezi	mezi	k7c7
koly	kolo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgMnSc7
byli	být	k5eAaImAgMnP
bojovníci	bojovník	k1gMnPc1
chráněni	chránit	k5eAaImNgMnP
proti	proti	k7c3
nepřátelské	přátelský	k2eNgFnSc3d1
palbě	palba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgInSc2
každý	každý	k3xTgMnSc1
vůz	vůz	k1gInSc4
vezl	vézt	k5eAaImAgInS
řetěz	řetěz	k1gInSc1
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
byla	být	k5eAaImAgFnS
spojována	spojovat	k5eAaImNgFnS
kola	kolo	k1gNnSc2
vozů	vůz	k1gInPc2
k	k	k7c3
sobě	se	k3xPyFc3
při	při	k7c6
srážení	srážení	k1gNnSc6
do	do	k7c2
vozové	vozový	k2eAgFnSc2d1
hradby	hradba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
voze	vůz	k1gInSc6
bylo	být	k5eAaImAgNnS
i	i	k9
nářadí	nářadí	k1gNnSc1
jako	jako	k8xC,k8xS
lopaty	lopata	k1gFnPc1
<g/>
,	,	kIx,
sekery	sekera	k1gFnPc1
<g/>
,	,	kIx,
rýče	rýč	k1gInPc1
a	a	k8xC
motyky	motyka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
sloužilo	sloužit	k5eAaImAgNnS
při	při	k7c6
úpravách	úprava	k1gFnPc6
cest	cesta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vozové	vozový	k2eAgFnPc1d1
hradby	hradba	k1gFnPc1
byly	být	k5eAaImAgFnP
velmi	velmi	k6eAd1
praktické	praktický	k2eAgFnPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
bojovníci	bojovník	k1gMnPc1
byli	být	k5eAaImAgMnP
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
jako	jako	k9
bojovníci	bojovník	k1gMnPc1
na	na	k7c6
koních	kůň	k1gMnPc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
byli	být	k5eAaImAgMnP
výše	vysoce	k6eAd2
než	než	k8xS
pěšáci	pěšák	k1gMnPc1
a	a	k8xC
lépe	dobře	k6eAd2
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
útočilo	útočit	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
Husitská	husitský	k2eAgNnPc1d1
polní	polní	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
měla	mít	k5eAaImAgNnP
v	v	k7c6
poli	pole	k1gNnSc6
k	k	k7c3
dispozici	dispozice	k1gFnSc3
obvykle	obvykle	k6eAd1
větší	veliký	k2eAgNnSc4d2
množství	množství	k1gNnSc4
vozů	vůz	k1gInPc2
(	(	kIx(
<g/>
ve	v	k7c6
velkých	velký	k2eAgFnPc6d1
bitvách	bitva	k1gFnPc6
až	až	k6eAd1
stovky	stovka	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
ve	v	k7c6
vozové	vozový	k2eAgFnSc6d1
hradbě	hradba	k1gFnSc6
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
libovolně	libovolně	k6eAd1
nakonfigurovat	nakonfigurovat	k5eAaPmF
podle	podle	k7c2
okamžitých	okamžitý	k2eAgFnPc2d1
potřeb	potřeba	k1gFnPc2
do	do	k7c2
různých	různý	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
i	i	k8xC
většího	veliký	k2eAgInSc2d2
počtu	počet	k1gInSc2
řad	řada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČORNEJ	ČORNEJ	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Husitské	husitský	k2eAgFnPc4d1
válečnictví	válečnictví	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
PŘENOSIL	přenosit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
k	k	k7c3
NATO	NATO	kA
:	:	kIx,
český	český	k2eAgInSc1d1
stát	stát	k1gInSc1
a	a	k8xC
střední	střední	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Evropský	evropský	k2eAgInSc1d1
literární	literární	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
53	#num#	k4
<g/>
-	-	kIx~
<g/>
67	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
DOLEJŠÍ	Dolejší	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
KŘÍŽEK	křížek	k1gInSc1
<g/>
,	,	kIx,
Leonid	Leonid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Husité	husita	k1gMnPc1
:	:	kIx,
vrchol	vrchol	k1gInSc1
válečného	válečný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
1419	#num#	k4
<g/>
-	-	kIx~
<g/>
1434	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Elka	Elka	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
361	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87057	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KUFFNER	KUFFNER	kA
<g/>
,	,	kIx,
Hanuš	Hanuš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Husitské	husitský	k2eAgFnPc4d1
vojny	vojna	k1gFnPc4
v	v	k7c6
obrazech	obraz	k1gInPc6
:	:	kIx,
Sbírka	sbírka	k1gFnSc1
59	#num#	k4
mapek	mapka	k1gFnPc2
a	a	k8xC
plánů	plán	k1gInPc2
k	k	k7c3
dějinám	dějiny	k1gFnPc3
válek	válka	k1gFnPc2
i	i	k8xC
válečnictví	válečnictví	k1gNnSc1
husitského	husitský	k2eAgMnSc2d1
se	se	k3xPyFc4
stručným	stručný	k2eAgInSc7d1
výkladem	výklad	k1gInSc7
zobrazených	zobrazený	k2eAgInPc2d1
dějů	děj	k1gInPc2
:	:	kIx,
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
mapkou	mapka	k1gFnSc7
zemí	zem	k1gFnPc2
koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
za	za	k7c2
válek	válka	k1gFnPc2
husitských	husitský	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královské	královský	k2eAgInPc1d1
Vinohrady	Vinohrady	k1gInPc1
<g/>
:	:	kIx,
vl	vl	k?
<g/>
.	.	kIx.
<g/>
n.	n.	k?
<g/>
,	,	kIx,
1908	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
ŠMAHEL	ŠMAHEL	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Husitská	husitský	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
válečných	válečný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
420	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7184	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
75	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Vozová	vozový	k2eAgFnSc1d1
hradba	hradba	k1gFnSc1
ve	v	k7c6
Vlastenském	vlastenský	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
historickém	historický	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Husitství	husitství	k1gNnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
