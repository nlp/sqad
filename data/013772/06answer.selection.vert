<s>
Vozové	vozový	k2eAgFnPc1d1
hradby	hradba	k1gFnPc1
byly	být	k5eAaImAgFnP
velmi	velmi	k6eAd1
praktické	praktický	k2eAgFnPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
bojovníci	bojovník	k1gMnPc1
byli	být	k5eAaImAgMnP
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
jako	jako	k9
bojovníci	bojovník	k1gMnPc1
na	na	k7c6
koních	kůň	k1gMnPc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
byli	být	k5eAaImAgMnP
výše	vysoce	k6eAd2
než	než	k8xS
pěšáci	pěšák	k1gMnPc1
a	a	k8xC
lépe	dobře	k6eAd2
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
útočilo	útočit	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>