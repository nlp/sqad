<s>
Husitské	husitský	k2eAgInPc1d1
bojové	bojový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
byl	být	k5eAaImAgInS
středověký	středověký	k2eAgInSc1d1
bojový	bojový	k2eAgInSc1d1
prostředek	prostředek	k1gInSc1
používaný	používaný	k2eAgInSc1d1
husity	husita	k1gMnPc7
během	během	k7c2
husitských	husitský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívány	využíván	k2eAgInPc1d1
byly	být	k5eAaImAgInP
především	především	k9
pro	pro	k7c4
stavbu	stavba	k1gFnSc4
tzv.	tzv.	kA
vozových	vozový	k2eAgFnPc2d1
hradeb	hradba	k1gFnPc2
<g/>
.	.	kIx.
</s>