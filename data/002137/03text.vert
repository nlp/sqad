<s>
Antverpy	Antverpy	k1gFnPc1	Antverpy
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
Antwerpen	Antwerpen	k2eAgInSc1d1	Antwerpen
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Anvers	Anvers	k1gInSc1	Anvers
<g/>
,	,	kIx,	,
vlámsky	vlámsky	k6eAd1	vlámsky
Antwerpn	Antwerpn	k1gInSc1	Antwerpn
<g/>
,	,	kIx,	,
valonsky	valonsky	k6eAd1	valonsky
Anverse	Anverse	k1gFnSc1	Anverse
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
přibližně	přibližně	k6eAd1	přibližně
513	[number]	k4	513
000	[number]	k4	000
(	(	kIx(	(
<g/>
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
lednu	leden	k1gInSc3	leden
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
obyvateli	obyvatel	k1gMnPc7	obyvatel
největší	veliký	k2eAgFnSc4d3	veliký
belgické	belgický	k2eAgNnSc4d1	Belgické
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
přístav	přístav	k1gInSc1	přístav
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Antverpy	Antverpy	k1gFnPc1	Antverpy
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Šeldy	Šelda	k1gFnSc2	Šelda
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
si	se	k3xPyFc3	se
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
vysloužilo	vysloužit	k5eAaPmAgNnS	vysloužit
dvě	dva	k4xCgFnPc4	dva
přezdívky	přezdívka	k1gFnPc4	přezdívka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rubensovo	Rubensův	k2eAgNnSc1d1	Rubensovo
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
tu	ten	k3xDgFnSc4	ten
žil	žít	k5eAaImAgMnS	žít
vlámský	vlámský	k2eAgMnSc1d1	vlámský
malíř	malíř	k1gMnSc1	malíř
Peter	Peter	k1gMnSc1	Peter
Paul	Paul	k1gMnSc1	Paul
Rubens	Rubens	k1gInSc1	Rubens
<g/>
,	,	kIx,	,
pojmenování	pojmenování	k1gNnSc1	pojmenování
"	"	kIx"	"
<g/>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
západu	západ	k1gInSc2	západ
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
město	město	k1gNnSc1	město
vysloužilo	vysloužit	k5eAaPmAgNnS	vysloužit
díky	díky	k7c3	díky
židům	žid	k1gMnPc3	žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
velkou	velký	k2eAgFnSc4d1	velká
komunitu	komunita	k1gFnSc4	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Antverpy	Antverpy	k1gFnPc1	Antverpy
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
vojenským	vojenský	k2eAgNnSc7d1	vojenské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
původní	původní	k2eAgFnPc1d1	původní
hradby	hradba	k1gFnPc1	hradba
byly	být	k5eAaImAgFnP	být
zbořeny	zbořen	k2eAgFnPc1d1	zbořena
a	a	k8xC	a
nahradily	nahradit	k5eAaPmAgFnP	nahradit
je	on	k3xPp3gNnSc4	on
bulváry	bulvár	k1gInPc1	bulvár
a	a	k8xC	a
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
několik	několik	k4yIc1	několik
menších	malý	k2eAgFnPc2d2	menší
tvrzí	tvrz	k1gFnPc2	tvrz
a	a	k8xC	a
pevností	pevnost	k1gFnPc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Antverpy	Antverpy	k1gFnPc1	Antverpy
se	se	k3xPyFc4	se
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
staly	stát	k5eAaPmAgFnP	stát
cílem	cíl	k1gInSc7	cíl
německých	německý	k2eAgFnPc2d1	německá
raket	raketa	k1gFnPc2	raketa
V-	V-	k1gMnPc2	V-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
město	město	k1gNnSc4	město
velmi	velmi	k6eAd1	velmi
poničily	poničit	k5eAaPmAgFnP	poničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zničit	zničit	k5eAaPmF	zničit
přístav	přístav	k1gInSc4	přístav
se	se	k3xPyFc4	se
Němcům	Němec	k1gMnPc3	Němec
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
město	město	k1gNnSc4	město
Antverpy	Antverpy	k1gFnPc1	Antverpy
hostilo	hostit	k5eAaImAgNnS	hostit
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
katedrála	katedrála	k1gFnSc1	katedrála
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
Šeldy	Šelda	k1gFnSc2	Šelda
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
dostavěná	dostavěný	k2eAgFnSc1d1	dostavěná
roku	rok	k1gInSc2	rok
1523	[number]	k4	1523
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
stavba	stavba	k1gFnSc1	stavba
trvala	trvat	k5eAaImAgFnS	trvat
269	[number]	k4	269
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
měří	měřit	k5eAaImIp3nS	měřit
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
117	[number]	k4	117
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
65	[number]	k4	65
m	m	kA	m
a	a	k8xC	a
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
7	[number]	k4	7
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
14	[number]	k4	14
kaplí	kaple	k1gFnPc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
dokončenou	dokončený	k2eAgFnSc4d1	dokončená
věž	věž	k1gFnSc4	věž
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
123	[number]	k4	123
m.	m.	k?	m.
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
klenba	klenba	k1gFnSc1	klenba
je	být	k5eAaImIp3nS	být
podepřena	podepřít	k5eAaPmNgFnS	podepřít
125	[number]	k4	125
nosnými	nosný	k2eAgInPc7d1	nosný
sloupy	sloup	k1gInPc7	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
okolo	okolo	k7c2	okolo
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
katedrály	katedrála	k1gFnSc2	katedrála
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
bývalé	bývalý	k2eAgNnSc1d1	bývalé
hlavní	hlavní	k2eAgNnSc1d1	hlavní
tržiště	tržiště	k1gNnSc1	tržiště
Grote	grot	k1gInSc5	grot
Markt	Markt	k1gInSc1	Markt
se	s	k7c7	s
67	[number]	k4	67
m	m	kA	m
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
renesanční	renesanční	k2eAgFnSc7d1	renesanční
radnicí	radnice	k1gFnSc7	radnice
a	a	k8xC	a
cechovní	cechovní	k2eAgInPc4d1	cechovní
domy	dům	k1gInPc4	dům
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
sochařskou	sochařský	k2eAgFnSc7d1	sochařská
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
katedrály	katedrála	k1gFnSc2	katedrála
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Groenplaats	Groenplaats	k1gInSc1	Groenplaats
(	(	kIx(	(
<g/>
Zelené	Zelené	k2eAgNnSc1d1	Zelené
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
dominuje	dominovat	k5eAaImIp3nS	dominovat
socha	socha	k1gFnSc1	socha
Rubense	Rubense	k1gFnSc2	Rubense
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
historickou	historický	k2eAgFnSc7d1	historická
zástavbou	zástavba	k1gFnSc7	zástavba
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
budova	budova	k1gFnSc1	budova
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Plantin-Moretus	Plantin-Moretus	k1gInSc1	Plantin-Moretus
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Dalšimi	Dalši	k1gFnPc7	Dalši
památkami	památka	k1gFnPc7	památka
chráněnými	chráněný	k2eAgFnPc7d1	chráněná
UNESCEM	Unesco	k1gNnSc7	Unesco
jsou	být	k5eAaImIp3nP	být
dům	dům	k1gInSc4	dům
Guiette	Guiett	k1gInSc5	Guiett
od	od	k7c2	od
architekta	architekt	k1gMnSc4	architekt
Le	Le	k1gMnSc2	Le
Corbusiera	Corbusier	k1gMnSc2	Corbusier
a	a	k8xC	a
zvonice	zvonice	k1gFnSc2	zvonice
(	(	kIx(	(
<g/>
věže	věž	k1gFnSc2	věž
<g/>
)	)	kIx)	)
zdejší	zdejší	k2eAgFnSc2d1	zdejší
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Antverpy	Antverpy	k1gFnPc1	Antverpy
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
především	především	k9	především
jako	jako	k9	jako
centrum	centrum	k1gNnSc1	centrum
diamantového	diamantový	k2eAgInSc2d1	diamantový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
nimi	on	k3xPp3gNnPc7	on
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
zdejší	zdejší	k2eAgFnSc4d1	zdejší
tradici	tradice	k1gFnSc4	tradice
jejich	jejich	k3xOp3gNnSc2	jejich
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
novogotická	novogotický	k2eAgFnSc1d1	novogotická
budova	budova	k1gFnSc1	budova
nejstarší	starý	k2eAgFnSc1d3	nejstarší
finanční	finanční	k2eAgFnPc4d1	finanční
burzy	burza	k1gFnPc4	burza
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
fungovat	fungovat	k5eAaImF	fungovat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1531	[number]	k4	1531
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
ale	ale	k8xC	ale
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
jsou	být	k5eAaImIp3nP	být
Antverpy	Antverpy	k1gFnPc1	Antverpy
uznávány	uznáván	k2eAgFnPc1d1	uznávána
jako	jako	k8xS	jako
město	město	k1gNnSc1	město
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
designu	design	k1gInSc2	design
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
absolventům	absolvent	k1gMnPc3	absolvent
Královské	královský	k2eAgFnSc2d1	královská
akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
zoo	zoo	k1gFnPc2	zoo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
uprostřed	uprostřed	k7c2	uprostřed
města	město	k1gNnSc2	město
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
přes	přes	k7c4	přes
4000	[number]	k4	4000
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Mylhúzy	Mylhúza	k1gFnPc1	Mylhúza
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Rostock	Rostock	k1gInSc1	Rostock
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Haifa	Haifa	k1gFnSc1	Haifa
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
JAR	jaro	k1gNnPc2	jaro
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Ludwigshafen	Ludwigshafen	k1gInSc1	Ludwigshafen
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Antverpy	Antverpy	k1gFnPc1	Antverpy
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Antverpy	Antverpy	k1gFnPc1	Antverpy
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Antverpy	Antverpy	k1gFnPc1	Antverpy
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Antverpy	Antverpy	k1gFnPc1	Antverpy
<g/>
,	,	kIx,	,
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
zastoupení	zastoupení	k1gNnSc1	zastoupení
belgických	belgický	k2eAgInPc2d1	belgický
Flander	Flandry	k1gInPc2	Flandry
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
