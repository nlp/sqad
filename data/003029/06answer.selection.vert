<s>
John	John	k1gMnSc1	John
von	von	k1gInSc1	von
Neumann	Neumann	k1gMnSc1	Neumann
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Neumann	Neumann	k1gMnSc1	Neumann
János	János	k1gMnSc1	János
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1903	[number]	k4	1903
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1957	[number]	k4	1957
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rakousko-uherský	rakouskoherský	k2eAgInSc1d1	rakousko-uherský
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
americký	americký	k2eAgMnSc1d1	americký
matematik	matematik	k1gMnSc1	matematik
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
značnou	značný	k2eAgFnSc4d1	značná
mírou	míra	k1gFnSc7	míra
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
oborům	obor	k1gInPc3	obor
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
funkcionální	funkcionální	k2eAgFnSc1d1	funkcionální
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc1	teorie
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
ekonomika	ekonomika	k1gFnSc1	ekonomika
<g/>
,	,	kIx,	,
informatika	informatika	k1gFnSc1	informatika
<g/>
,	,	kIx,	,
numerická	numerický	k2eAgFnSc1d1	numerická
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
hydrodynamika	hydrodynamika	k1gFnSc1	hydrodynamika
<g/>
,	,	kIx,	,
statistika	statistika	k1gFnSc1	statistika
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
matematických	matematický	k2eAgFnPc2d1	matematická
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
