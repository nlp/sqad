<s>
John	John	k1gMnSc1	John
von	von	k1gInSc1	von
Neumann	Neumann	k1gMnSc1	Neumann
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Neumann	Neumann	k1gMnSc1	Neumann
János	János	k1gMnSc1	János
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1903	[number]	k4	1903
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1957	[number]	k4	1957
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rakousko-uherský	rakouskoherský	k2eAgInSc1d1	rakousko-uherský
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
americký	americký	k2eAgMnSc1d1	americký
matematik	matematik	k1gMnSc1	matematik
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
značnou	značný	k2eAgFnSc4d1	značná
mírou	míra	k1gFnSc7	míra
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
oborům	obor	k1gInPc3	obor
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
funkcionální	funkcionální	k2eAgFnSc1d1	funkcionální
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc1	teorie
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
ekonomika	ekonomika	k1gFnSc1	ekonomika
<g/>
,	,	kIx,	,
informatika	informatika	k1gFnSc1	informatika
<g/>
,	,	kIx,	,
numerická	numerický	k2eAgFnSc1d1	numerická
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
hydrodynamika	hydrodynamika	k1gFnSc1	hydrodynamika
<g/>
,	,	kIx,	,
statistika	statistika	k1gFnSc1	statistika
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
matematických	matematický	k2eAgFnPc2d1	matematická
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
bohatého	bohatý	k2eAgMnSc2d1	bohatý
maďarského	maďarský	k2eAgMnSc2d1	maďarský
bankéře	bankéř	k1gMnSc2	bankéř
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
malička	maličko	k1gNnSc2	maličko
projevoval	projevovat	k5eAaImAgInS	projevovat
znaky	znak	k1gInPc4	znak
geniality	genialita	k1gFnSc2	genialita
-	-	kIx~	-
měl	mít	k5eAaImAgMnS	mít
jazykové	jazykový	k2eAgNnSc4d1	jazykové
nadání	nadání	k1gNnSc4	nadání
a	a	k8xC	a
neobyčejnou	obyčejný	k2eNgFnSc4d1	neobyčejná
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
žertovat	žertovat	k5eAaImF	žertovat
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
ve	v	k7c6	v
starořečtině	starořečtina	k1gFnSc6	starořečtina
a	a	k8xC	a
zpaměti	zpaměti	k6eAd1	zpaměti
uměl	umět	k5eAaImAgMnS	umět
dělit	dělit	k5eAaImF	dělit
osmimístnými	osmimístný	k2eAgNnPc7d1	osmimístné
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dvanácti	dvanáct	k4xCc2	dvanáct
let	léto	k1gNnPc2	léto
ho	on	k3xPp3gMnSc4	on
soukromě	soukromě	k6eAd1	soukromě
učil	učít	k5eAaPmAgMnS	učít
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
profesor	profesor	k1gMnSc1	profesor
matematiky	matematika	k1gFnSc2	matematika
z	z	k7c2	z
Budapešťské	budapešťský	k2eAgFnSc2d1	Budapešťská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
publikoval	publikovat	k5eAaBmAgMnS	publikovat
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgInS	zapsat
na	na	k7c4	na
Budapešťskou	budapešťský	k2eAgFnSc4d1	Budapešťská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
otce	otec	k1gMnSc2	otec
zvolil	zvolit	k5eAaPmAgMnS	zvolit
perspektivní	perspektivní	k2eAgInSc4d1	perspektivní
obor	obor	k1gInSc4	obor
chemické	chemický	k2eAgNnSc1d1	chemické
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
snadné	snadný	k2eAgNnSc1d1	snadné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nudil	nudit	k5eAaImAgMnS	nudit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
napsal	napsat	k5eAaPmAgMnS	napsat
doktorskou	doktorský	k2eAgFnSc4d1	doktorská
práci	práce	k1gFnSc4	práce
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvaadvaceti	dvaadvacet	k4xCc6	dvaadvacet
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
(	(	kIx(	(
<g/>
soukromý	soukromý	k2eAgMnSc1d1	soukromý
<g/>
)	)	kIx)	)
docent	docent	k1gMnSc1	docent
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
kvantovou	kvantový	k2eAgFnSc7d1	kvantová
teorií	teorie	k1gFnSc7	teorie
a	a	k8xC	a
teorií	teorie	k1gFnSc7	teorie
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
vědcem	vědec	k1gMnSc7	vědec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celosvětově	celosvětově	k6eAd1	celosvětově
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
jako	jako	k8xC	jako
spolutvůrce	spolutvůrce	k1gMnSc1	spolutvůrce
matematické	matematický	k2eAgFnSc2d1	matematická
teorie	teorie	k1gFnSc2	teorie
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
používána	používán	k2eAgFnSc1d1	používána
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
i	i	k8xC	i
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
-	-	kIx~	-
už	už	k6eAd1	už
jako	jako	k8xC	jako
světově	světově	k6eAd1	světově
proslulý	proslulý	k2eAgMnSc1d1	proslulý
vědec	vědec	k1gMnSc1	vědec
-	-	kIx~	-
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
vedoucím	vedoucí	k1gMnSc7	vedoucí
oddělení	oddělení	k1gNnSc2	oddělení
matematiky	matematika	k1gFnSc2	matematika
nového	nový	k2eAgInSc2d1	nový
Institut	institut	k1gInSc1	institut
for	forum	k1gNnPc2	forum
Advanced	Advanced	k1gInSc1	Advanced
Study	stud	k1gInPc4	stud
v	v	k7c6	v
Princetonu	Princeton	k1gInSc6	Princeton
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc4	jeho
objevy	objev	k1gInPc4	objev
jako	jako	k8xS	jako
průkopníka	průkopník	k1gMnSc2	průkopník
digitálních	digitální	k2eAgMnPc2d1	digitální
počítačů	počítač	k1gMnPc2	počítač
a	a	k8xC	a
operační	operační	k2eAgFnSc2d1	operační
teorie	teorie	k1gFnSc2	teorie
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
(	(	kIx(	(
<g/>
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Von	von	k1gInSc1	von
Neumannova	Neumannův	k2eAgFnSc1d1	Neumannova
algebra	algebra	k1gFnSc1	algebra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
teorie	teorie	k1gFnSc2	teorie
her	hra	k1gFnPc2	hra
a	a	k8xC	a
konceptu	koncept	k1gInSc2	koncept
buňkového	buňkový	k2eAgInSc2d1	buňkový
automatu	automat	k1gInSc2	automat
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Edwardem	Edward	k1gMnSc7	Edward
Tellerem	Teller	k1gMnSc7	Teller
a	a	k8xC	a
Stanislawem	Stanislaw	k1gMnSc7	Stanislaw
Ulamem	Ulam	k1gMnSc7	Ulam
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
jadernou	jaderný	k2eAgFnSc7d1	jaderná
fyzikou	fyzika	k1gFnSc7	fyzika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
základní	základní	k2eAgInPc4d1	základní
předpoklady	předpoklad	k1gInPc4	předpoklad
termonukleárních	termonukleární	k2eAgFnPc2d1	termonukleární
reakcí	reakce	k1gFnPc2	reakce
a	a	k8xC	a
vodíkové	vodíkový	k2eAgFnSc2d1	vodíková
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Von	von	k1gInSc1	von
Neumannova	Neumannův	k2eAgFnSc1d1	Neumannova
architektura	architektura	k1gFnSc1	architektura
digitálního	digitální	k2eAgInSc2d1	digitální
počítače	počítač	k1gInSc2	počítač
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
v	v	k7c6	v
článku	článek	k1gInSc6	článek
"	"	kIx"	"
<g/>
First	First	k1gFnSc1	First
Draft	draft	k1gInSc1	draft
of	of	k?	of
a	a	k8xC	a
Report	report	k1gInSc1	report
on	on	k3xPp3gMnSc1	on
the	the	k?	the
EDVAC	EDVAC	kA	EDVAC
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
moduly	modul	k1gInPc1	modul
jím	jíst	k5eAaImIp1nS	jíst
navrženého	navržený	k2eAgInSc2d1	navržený
počítače	počítač	k1gInSc2	počítač
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
procesor	procesor	k1gInSc1	procesor
<g/>
,	,	kIx,	,
řadič	řadič	k1gInSc1	řadič
<g/>
,	,	kIx,	,
operační	operační	k2eAgFnSc1d1	operační
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
vstupní	vstupní	k2eAgNnSc1d1	vstupní
a	a	k8xC	a
výstupní	výstupní	k2eAgNnSc1d1	výstupní
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koncepce	koncepce	k1gFnSc1	koncepce
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc4	základ
architektury	architektura	k1gFnSc2	architektura
současných	současný	k2eAgMnPc2d1	současný
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
dvojková	dvojkový	k2eAgFnSc1d1	dvojková
soustava	soustava	k1gFnSc1	soustava
programy	program	k1gInPc4	program
a	a	k8xC	a
data	datum	k1gNnPc4	datum
v	v	k7c6	v
operační	operační	k2eAgFnSc6d1	operační
paměti	paměť	k1gFnSc6	paměť
(	(	kIx(	(
<g/>
nenačítají	načítat	k5eNaBmIp3nP	načítat
se	se	k3xPyFc4	se
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
paměti	paměť	k1gFnSc2	paměť
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
výpočtu	výpočet	k1gInSc2	výpočet
<g/>
,	,	kIx,	,
jednotné	jednotný	k2eAgNnSc1d1	jednotné
kódování	kódování	k1gNnSc1	kódování
-	-	kIx~	-
k	k	k7c3	k
programům	program	k1gInPc3	program
lze	lze	k6eAd1	lze
přistupovat	přistupovat	k5eAaImF	přistupovat
jako	jako	k9	jako
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
<g/>
,	,	kIx,	,
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
univerzalitu	univerzalita	k1gFnSc4	univerzalita
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
bezproblémové	bezproblémový	k2eAgNnSc1d1	bezproblémové
zavedení	zavedení	k1gNnSc1	zavedení
cyklů	cyklus	k1gInPc2	cyklus
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
podmíněného	podmíněný	k2eAgNnSc2d1	podmíněné
větvení	větvení	k1gNnSc2	větvení
<g/>
)	)	kIx)	)
rychlost	rychlost	k1gFnSc1	rychlost
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
paměti	paměť	k1gFnSc2	paměť
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
jednotky	jednotka	k1gFnSc2	jednotka
přímé	přímý	k2eAgNnSc4d1	přímé
adresování	adresování	k1gNnSc4	adresování
(	(	kIx(	(
<g/>
přístup	přístup	k1gInSc1	přístup
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
okamžiku	okamžik	k1gInSc6	okamžik
přístupná	přístupný	k2eAgFnSc1d1	přístupná
kterákoliv	kterýkoliv	k3yIgFnSc1	kterýkoliv
buňka	buňka	k1gFnSc1	buňka
paměti	paměť	k1gFnSc2	paměť
aritmeticko-logická	aritmetickoogický	k2eAgFnSc1d1	aritmeticko-logická
jednotka	jednotka	k1gFnSc1	jednotka
-	-	kIx~	-
pouze	pouze	k6eAd1	pouze
obvody	obvod	k1gInPc1	obvod
pro	pro	k7c4	pro
sčítání	sčítání	k1gNnSc4	sčítání
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
operace	operace	k1gFnPc1	operace
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
sčítání	sčítání	k1gNnSc4	sčítání
<g/>
)	)	kIx)	)
1946	[number]	k4	1946
John	John	k1gMnSc1	John
von	von	k1gInSc1	von
Neumann	Neumann	k1gMnSc1	Neumann
<g />
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
funkčních	funkční	k2eAgFnPc2d1	funkční
jednotek	jednotka	k1gFnPc2	jednotka
-	-	kIx~	-
řídící	řídící	k2eAgFnSc1d1	řídící
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
aritmeticko-logická	aritmetickoogický	k2eAgFnSc1d1	aritmeticko-logická
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
vstupní	vstupní	k2eAgNnSc1d1	vstupní
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
výstupní	výstupní	k2eAgNnSc1d1	výstupní
zařízení	zařízení	k1gNnSc1	zařízení
Struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
zpracovávaných	zpracovávaný	k2eAgInPc6d1	zpracovávaný
problémech	problém	k1gInPc6	problém
<g/>
,	,	kIx,	,
na	na	k7c6	na
řešení	řešení	k1gNnSc6	řešení
problému	problém	k1gInSc2	problém
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
zvenčí	zvenčí	k6eAd1	zvenčí
zavést	zavést	k5eAaPmF	zavést
návod	návod	k1gInSc4	návod
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
(	(	kIx(	(
<g/>
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
uložit	uložit	k5eAaPmF	uložit
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
není	být	k5eNaImIp3nS	být
stroj	stroj	k1gInSc1	stroj
schopen	schopen	k2eAgInSc1d1	schopen
práce	práce	k1gFnSc2	práce
Programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
mezivýsledky	mezivýsledek	k1gInPc1	mezivýsledek
a	a	k8xC	a
konečné	konečný	k2eAgInPc1d1	konečný
výsledky	výsledek	k1gInPc1	výsledek
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
do	do	k7c2	do
téže	tenže	k3xDgFnSc2	tenže
paměti	paměť	k1gFnSc2	paměť
Paměť	paměť	k1gFnSc1	paměť
je	být	k5eAaImIp3nS	být
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
na	na	k7c4	na
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgFnPc4d1	velká
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
průběžně	průběžně	k6eAd1	průběžně
očíslované	očíslovaný	k2eAgFnPc1d1	očíslovaná
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
číslo	číslo	k1gNnSc4	číslo
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
adresu	adresa	k1gFnSc4	adresa
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přečíst	přečíst	k5eAaPmF	přečíst
<g />
.	.	kIx.	.
</s>
<s>
nebo	nebo	k8xC	nebo
změnit	změnit	k5eAaPmF	změnit
obsah	obsah	k1gInSc4	obsah
buňky	buňka	k1gFnSc2	buňka
Po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucí	jdoucí	k2eAgFnPc1d1	jdoucí
instrukce	instrukce	k1gFnPc1	instrukce
programu	program	k1gInSc2	program
se	se	k3xPyFc4	se
uloží	uložit	k5eAaPmIp3nS	uložit
do	do	k7c2	do
paměťových	paměťový	k2eAgFnPc2d1	paměťová
buněk	buňka	k1gFnPc2	buňka
jdoucích	jdoucí	k2eAgFnPc2d1	jdoucí
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
následující	následující	k2eAgFnSc3d1	následující
instrukci	instrukce	k1gFnSc3	instrukce
se	se	k3xPyFc4	se
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
z	z	k7c2	z
řídící	řídící	k2eAgFnSc2d1	řídící
jednotky	jednotka	k1gFnSc2	jednotka
zvýšením	zvýšení	k1gNnSc7	zvýšení
instrukční	instrukční	k2eAgFnSc2d1	instrukční
adresy	adresa	k1gFnSc2	adresa
o	o	k7c4	o
1	[number]	k4	1
Instrukcemi	instrukce	k1gFnPc7	instrukce
skoku	skok	k1gInSc2	skok
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
odklonit	odklonit	k5eAaPmF	odklonit
od	od	k7c2	od
zpracování	zpracování	k1gNnSc2	zpracování
instrukcí	instrukce	k1gFnPc2	instrukce
v	v	k7c6	v
uloženém	uložený	k2eAgNnSc6d1	uložené
pořadí	pořadí	k1gNnSc6	pořadí
Existují	existovat	k5eAaImIp3nP	existovat
alespoň	alespoň	k9	alespoň
-	-	kIx~	-
aritmetické	aritmetický	k2eAgFnPc4d1	aritmetická
instrukce	instrukce	k1gFnPc4	instrukce
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
sčítání	sčítání	k1gNnSc1	sčítání
<g/>
,	,	kIx,	,
násobení	násobení	k1gNnSc1	násobení
<g/>
,	,	kIx,	,
ukládání	ukládání	k1gNnSc1	ukládání
konstant	konstanta	k1gFnPc2	konstanta
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
logické	logický	k2eAgFnPc1d1	logická
instrukce	instrukce	k1gFnPc1	instrukce
(	(	kIx(	(
<g/>
porovnání	porovnání	k1gNnSc1	porovnání
<g/>
,	,	kIx,	,
not	nota	k1gFnPc2	nota
<g/>
,	,	kIx,	,
and	and	k?	and
<g/>
,	,	kIx,	,
or	or	k?	or
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
instrukce	instrukce	k1gFnPc4	instrukce
přenosu	přenos	k1gInSc2	přenos
(	(	kIx(	(
<g/>
z	z	k7c2	z
paměti	paměť	k1gFnSc2	paměť
do	do	k7c2	do
řídící	řídící	k2eAgFnSc2d1	řídící
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
vstup	vstup	k1gInSc1	vstup
<g/>
/	/	kIx~	/
<g/>
výstup	výstup	k1gInSc1	výstup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podmíněné	podmíněný	k2eAgInPc1d1	podmíněný
skoky	skok	k1gInPc1	skok
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnSc1d1	ostatní
(	(	kIx(	(
<g/>
posunutí	posunutí	k1gNnSc1	posunutí
<g/>
,	,	kIx,	,
přerušení	přerušení	k1gNnSc1	přerušení
<g/>
,	,	kIx,	,
čekání	čekání	k1gNnSc1	čekání
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Všechna	všechen	k3xTgNnPc1	všechen
data	datum	k1gNnPc1	datum
(	(	kIx(	(
<g/>
instrukce	instrukce	k1gFnSc2	instrukce
<g/>
,	,	kIx,	,
adresy	adresa	k1gFnSc2	adresa
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
binárně	binárně	k6eAd1	binárně
kódovaná	kódovaný	k2eAgNnPc1d1	kódované
<g/>
,	,	kIx,	,
správné	správný	k2eAgNnSc1d1	správné
dekódování	dekódování	k1gNnSc1	dekódování
zabezpečují	zabezpečovat	k5eAaImIp3nP	zabezpečovat
vhodné	vhodný	k2eAgInPc1d1	vhodný
logické	logický	k2eAgInPc1d1	logický
obvody	obvod	k1gInPc1	obvod
v	v	k7c6	v
řídící	řídící	k2eAgFnSc6d1	řídící
jednotce	jednotka	k1gFnSc6	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
John	John	k1gMnSc1	John
von	von	k1gInSc1	von
Neumann	Neumann	k1gMnSc1	Neumann
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc2	galerie
John	John	k1gMnSc1	John
von	von	k1gInSc1	von
Neumann	Neumann	k1gMnSc1	Neumann
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Zdroj	zdroj	k1gInSc1	zdroj
článku	článek	k1gInSc2	článek
(	(	kIx(	(
<g/>
převzato	převzít	k5eAaPmNgNnS	převzít
se	s	k7c7	s
svolením	svolení	k1gNnSc7	svolení
<g/>
)	)	kIx)	)
Životopis	životopis	k1gInSc1	životopis
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaImAgInS	dít
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
John	John	k1gMnSc1	John
von	von	k1gInSc1	von
Neumann	Neumann	k1gMnSc1	Neumann
</s>
