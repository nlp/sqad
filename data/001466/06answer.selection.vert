<s>
Philipp	Philipp	k1gMnSc1	Philipp
Melanchthon	Melanchthon	k1gMnSc1	Melanchthon
(	(	kIx(	(
<g/>
pořečtěná	pořečtěný	k2eAgFnSc1d1	pořečtěná
forma	forma	k1gFnSc1	forma
příjmení	příjmení	k1gNnSc2	příjmení
Schwarzerdt	Schwarzerdta	k1gFnPc2	Schwarzerdta
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1497	[number]	k4	1497
Bretten	Brettno	k1gNnPc2	Brettno
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1560	[number]	k4	1560
Wittenberg	Wittenberg	k1gMnSc1	Wittenberg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
humanistický	humanistický	k2eAgMnSc1d1	humanistický
učenec	učenec	k1gMnSc1	učenec
<g/>
,	,	kIx,	,
reformační	reformační	k2eAgMnSc1d1	reformační
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
poradce	poradce	k1gMnSc1	poradce
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
Martina	Martin	k1gMnSc2	Martin
Luthera	Luther	k1gMnSc2	Luther
<g/>
.	.	kIx.	.
</s>
