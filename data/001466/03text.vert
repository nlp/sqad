<s>
Philipp	Philipp	k1gMnSc1	Philipp
Melanchthon	Melanchthon	k1gMnSc1	Melanchthon
(	(	kIx(	(
<g/>
pořečtěná	pořečtěný	k2eAgFnSc1d1	pořečtěná
forma	forma	k1gFnSc1	forma
příjmení	příjmení	k1gNnSc2	příjmení
Schwarzerdt	Schwarzerdta	k1gFnPc2	Schwarzerdta
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1497	[number]	k4	1497
Bretten	Brettno	k1gNnPc2	Brettno
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1560	[number]	k4	1560
Wittenberg	Wittenberg	k1gMnSc1	Wittenberg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
humanistický	humanistický	k2eAgMnSc1d1	humanistický
učenec	učenec	k1gMnSc1	učenec
<g/>
,	,	kIx,	,
reformační	reformační	k2eAgMnSc1d1	reformační
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
poradce	poradce	k1gMnSc1	poradce
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
Martina	Martin	k1gMnSc2	Martin
Luthera	Luther	k1gMnSc2	Luther
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
teologické	teologický	k2eAgFnSc6d1	teologická
pozici	pozice	k1gFnSc6	pozice
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
hledat	hledat	k5eAaImF	hledat
konsenzus	konsenzus	k1gInSc4	konsenzus
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
reformačními	reformační	k2eAgInPc7d1	reformační
myšlenkovými	myšlenkový	k2eAgInPc7d1	myšlenkový
proudy	proud	k1gInPc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
reformu	reforma	k1gFnSc4	reforma
evangelického	evangelický	k2eAgInSc2d1	evangelický
církevního	církevní	k2eAgInSc2d1	církevní
systému	systém	k1gInSc2	systém
a	a	k8xC	a
německého	německý	k2eAgNnSc2d1	německé
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Melanchthon	Melanchthon	k1gInSc1	Melanchthon
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
praeceptor	praeceptor	k1gInSc1	praeceptor
Germaniae	Germaniae	k1gFnSc1	Germaniae
(	(	kIx(	(
<g/>
učitel	učitel	k1gMnSc1	učitel
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Brettenu	Bretten	k1gInSc6	Bretten
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Bádensko-Württembersko	Bádensko-Württembersko	k1gNnSc1	Bádensko-Württembersko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
v	v	k7c6	v
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
a	a	k8xC	a
Tübingen	Tübingen	k1gInSc4	Tübingen
převzal	převzít	k5eAaPmAgMnS	převzít
1518	[number]	k4	1518
profesuru	profesura	k1gFnSc4	profesura
klasických	klasický	k2eAgInPc2d1	klasický
jazyků	jazyk	k1gInPc2	jazyk
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1519	[number]	k4	1519
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
Lutherovy	Lutherův	k2eAgFnPc4d1	Lutherova
disputace	disputace	k1gFnPc4	disputace
s	s	k7c7	s
Eckem	Eckum	k1gNnSc7	Eckum
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
nechyběl	chybět	k5eNaImAgMnS	chybět
na	na	k7c6	na
říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
ve	v	k7c6	v
Špýru	Špýr	k1gInSc6	Špýr
a	a	k8xC	a
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Augsburgu	Augsburg	k1gInSc6	Augsburg
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
císaři	císař	k1gMnSc3	císař
Karlu	Karel	k1gMnSc3	Karel
V.	V.	kA	V.
předloženo	předložit	k5eAaPmNgNnS	předložit
jeho	jeho	k3xOp3gNnSc1	jeho
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
dílo	dílo	k1gNnSc1	dílo
Augsburské	augsburský	k2eAgNnSc1d1	Augsburské
vyznání	vyznání	k1gNnSc1	vyznání
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dodnes	dodnes	k6eAd1	dodnes
představuje	představovat	k5eAaImIp3nS	představovat
shrnutí	shrnutí	k1gNnSc4	shrnutí
víry	víra	k1gFnSc2	víra
lutherských	lutherský	k2eAgFnPc2d1	lutherská
evangelických	evangelický	k2eAgFnPc2d1	evangelická
církví	církev	k1gFnPc2	církev
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Lutherově	Lutherův	k2eAgFnSc6d1	Lutherova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vůdcem	vůdce	k1gMnSc7	vůdce
německých	německý	k2eAgMnPc2d1	německý
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Melanchthon	Melanchthon	k1gMnSc1	Melanchthon
zemřel	zemřít	k5eAaPmAgMnS	zemřít
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1560	[number]	k4	1560
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Melanchthonova	Melanchthonův	k2eAgInSc2d1	Melanchthonův
návrhu	návrh	k1gInSc2	návrh
byly	být	k5eAaImAgFnP	být
reformovány	reformován	k2eAgFnPc1d1	reformována
univerzity	univerzita	k1gFnPc1	univerzita
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
<g/>
,	,	kIx,	,
Tübingenu	Tübingen	k1gInSc6	Tübingen
<g/>
,	,	kIx,	,
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
<g/>
,	,	kIx,	,
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
Rostocku	Rostocko	k1gNnSc6	Rostocko
<g/>
,	,	kIx,	,
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
protestantské	protestantský	k2eAgFnPc1d1	protestantská
univerzity	univerzita	k1gFnPc1	univerzita
v	v	k7c6	v
Marburgu	Marburg	k1gInSc6	Marburg
<g/>
,	,	kIx,	,
Královci	Královec	k1gInSc6	Královec
a	a	k8xC	a
Jeně	Jen	k1gInSc6	Jen
<g/>
.	.	kIx.	.
</s>
<s>
Institutiones	Institutiones	k1gInSc1	Institutiones
graecae	graeca	k1gMnSc2	graeca
grammaticae	grammatica	k1gMnSc2	grammatica
(	(	kIx(	(
<g/>
1518	[number]	k4	1518
<g/>
)	)	kIx)	)
-	-	kIx~	-
řecká	řecký	k2eAgFnSc1d1	řecká
gramatika	gramatika	k1gFnSc1	gramatika
Compendiaria	Compendiarium	k1gNnSc2	Compendiarium
dialectica	dialectica	k6eAd1	dialectica
ratio	ratio	k6eAd1	ratio
(	(	kIx(	(
<g/>
1520	[number]	k4	1520
<g/>
)	)	kIx)	)
Základní	základní	k2eAgFnSc2d1	základní
pravdy	pravda	k1gFnSc2	pravda
teologie	teologie	k1gFnSc2	teologie
<g/>
/	/	kIx~	/
<g/>
Loci	Loci	k1gNnSc1	Loci
communes	communesa	k1gFnPc2	communesa
(	(	kIx(	(
<g/>
1521	[number]	k4	1521
<g/>
)	)	kIx)	)
-	-	kIx~	-
první	první	k4xOgFnSc1	první
evangelická	evangelický	k2eAgFnSc1d1	evangelická
dogmatika	dogmatika	k1gFnSc1	dogmatika
Výuka	výuka	k1gFnSc1	výuka
vizitátorů	vizitátor	k1gMnPc2	vizitátor
a	a	k8xC	a
farářů	farář	k1gMnPc2	farář
kurfiřtství	kurfiřtství	k1gNnSc2	kurfiřtství
Saského	saský	k2eAgInSc2d1	saský
(	(	kIx(	(
<g/>
1528	[number]	k4	1528
<g/>
)	)	kIx)	)
Augsburské	augsburský	k2eAgNnSc1d1	Augsburské
vyznání	vyznání	k1gNnSc1	vyznání
víry	víra	k1gFnSc2	víra
<g/>
/	/	kIx~	/
<g/>
Confessio	Confessio	k6eAd1	Confessio
Augustana	Augustan	k1gMnSc2	Augustan
(	(	kIx(	(
<g/>
1530	[number]	k4	1530
<g/>
)	)	kIx)	)
O	o	k7c6	o
moci	moc	k1gFnSc6	moc
a	a	k8xC	a
vrchnosti	vrchnost	k1gFnSc6	vrchnost
papeže	papež	k1gMnSc4	papež
<g/>
/	/	kIx~	/
<g/>
De	De	k?	De
potestate	potestat	k1gMnSc5	potestat
papae	papaus	k1gMnSc5	papaus
(	(	kIx(	(
<g/>
1537	[number]	k4	1537
<g/>
)	)	kIx)	)
</s>
