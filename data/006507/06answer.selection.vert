<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1685	[number]	k4	1685
Eisenach	Eisenach	k1gInSc1	Eisenach
–	–	k?	–
28.	[number]	k4	28.
července	červenec	k1gInSc2	červenec
1750	[number]	k4	1750
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
virtuos	virtuos	k1gMnSc1	virtuos
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
hudebních	hudební	k2eAgMnPc2d1	hudební
géniů	génius	k1gMnPc2	génius
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
završitele	završitel	k1gMnSc2	završitel
barokního	barokní	k2eAgInSc2d1	barokní
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
