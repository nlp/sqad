<s>
Koktejl	koktejl	k1gInSc1	koktejl
je	být	k5eAaImIp3nS	být
nápoj	nápoj	k1gInSc4	nápoj
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
smícháním	smíchání	k1gNnPc3	smíchání
několika	několik	k4yIc2	několik
různých	různý	k2eAgFnPc2d1	různá
nápojových	nápojový	k2eAgFnPc2d1	nápojová
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Mícháním	míchání	k1gNnSc7	míchání
koktejlů	koktejl	k1gInPc2	koktejl
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
barmani	barman	k1gMnPc1	barman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
často	často	k6eAd1	často
předvádí	předvádět	k5eAaImIp3nP	předvádět
hostům	host	k1gMnPc3	host
akrobacii	akrobacie	k1gFnSc4	akrobacie
pohybů	pohyb	k1gInPc2	pohyb
se	s	k7c7	s
sklenicemi	sklenice	k1gFnPc7	sklenice
<g/>
,	,	kIx,	,
lahvemi	lahev	k1gFnPc7	lahev
s	s	k7c7	s
míchanými	míchaný	k2eAgInPc7d1	míchaný
nápoji	nápoj	k1gInPc7	nápoj
<g/>
,	,	kIx,	,
lžícemi	lžíce	k1gFnPc7	lžíce
naplněnými	naplněný	k2eAgFnPc7d1	naplněná
ovocem	ovoce	k1gNnSc7	ovoce
nebo	nebo	k8xC	nebo
ledem	led	k1gInSc7	led
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
přísadami	přísada	k1gFnPc7	přísada
a	a	k8xC	a
surovinami	surovina	k1gFnPc7	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
míchání	míchání	k1gNnPc4	míchání
koktejlů	koktejl	k1gInPc2	koktejl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
šejkr	šejkr	k1gInSc1	šejkr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
kovová	kovový	k2eAgFnSc1d1	kovová
nádoba	nádoba	k1gFnSc1	nádoba
velikosti	velikost	k1gFnSc2	velikost
větší	veliký	k2eAgFnSc2d2	veliký
sklenice	sklenice	k1gFnSc2	sklenice
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
suroviny	surovina	k1gFnPc1	surovina
nadávkují	nadávkovat	k5eAaPmIp3nP	nadávkovat
<g/>
,	,	kIx,	,
šejkr	šejkr	k1gInSc1	šejkr
se	se	k3xPyFc4	se
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
sklenicí	sklenice	k1gFnSc7	sklenice
a	a	k8xC	a
nápoj	nápoj	k1gInSc1	nápoj
se	se	k3xPyFc4	se
míchá	míchat	k5eAaImIp3nS	míchat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
šejkru	šejkr	k1gInSc2	šejkr
se	se	k3xPyFc4	se
namíchaný	namíchaný	k2eAgInSc4d1	namíchaný
koktejl	koktejl	k1gInSc4	koktejl
nalije	nalít	k5eAaBmIp3nS	nalít
do	do	k7c2	do
sklenice	sklenice	k1gFnSc2	sklenice
<g/>
.	.	kIx.	.
</s>
<s>
Koktejly	koktejl	k1gInPc1	koktejl
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
alkohol	alkohol	k1gInSc1	alkohol
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc7	jejich
základní	základní	k2eAgFnSc7d1	základní
složkou	složka	k1gFnSc7	složka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k1gMnPc4	známý
a	a	k8xC	a
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
koktejly	koktejl	k1gInPc1	koktejl
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
Bloody	Blooda	k1gFnPc1	Blooda
Mary	Mary	k1gFnSc2	Mary
<g/>
,	,	kIx,	,
Cuba	Cubum	k1gNnSc2	Cubum
Libre	Libr	k1gInSc5	Libr
<g/>
,	,	kIx,	,
Daiquiri	Daiquir	k1gFnSc5	Daiquir
<g/>
,	,	kIx,	,
High	High	k1gMnSc1	High
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
Mojito	Mojit	k2eAgNnSc1d1	Mojito
<g/>
,	,	kIx,	,
Piñ	Piñ	k1gFnSc1	Piñ
Colada	Colada	k1gFnSc1	Colada
<g/>
,	,	kIx,	,
Sex	sex	k1gInSc1	sex
on	on	k3xPp3gInSc1	on
the	the	k?	the
beach	beach	k1gInSc1	beach
<g/>
,	,	kIx,	,
Tequila	tequila	k1gFnSc1	tequila
Sunrise	Sunrise	k1gFnSc1	Sunrise
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejoblíbenější	oblíbený	k2eAgInPc4d3	nejoblíbenější
nealkoholické	alkoholický	k2eNgInPc4d1	nealkoholický
koktejly	koktejl	k1gInPc4	koktejl
patří	patřit	k5eAaImIp3nP	patřit
nápoje	nápoj	k1gInPc4	nápoj
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
z	z	k7c2	z
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
bývají	bývat	k5eAaImIp3nP	bývat
dochuceny	dochutit	k5eAaPmNgInP	dochutit
dalšími	další	k2eAgFnPc7d1	další
pochutinami	pochutina	k1gFnPc7	pochutina
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vanilka	vanilka	k1gFnSc1	vanilka
<g/>
,	,	kIx,	,
mixované	mixovaný	k2eAgNnSc1d1	mixované
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
ovocné	ovocný	k2eAgInPc4d1	ovocný
výtažky	výtažek	k1gInPc4	výtažek
a	a	k8xC	a
tresti	tresť	k1gFnPc4	tresť
<g/>
,	,	kIx,	,
smetana	smetana	k1gFnSc1	smetana
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
koktejly	koktejl	k1gInPc1	koktejl
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vyrobit	vyrobit	k5eAaPmF	vyrobit
jednoduše	jednoduše	k6eAd1	jednoduše
i	i	k8xC	i
doma	doma	k6eAd1	doma
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
mixérech	mixér	k1gInPc6	mixér
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
obchodní	obchodní	k2eAgFnSc6d1	obchodní
síti	síť	k1gFnSc6	síť
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
zakoupit	zakoupit	k5eAaPmF	zakoupit
například	například	k6eAd1	například
v	v	k7c6	v
cukrárnách	cukrárna	k1gFnPc6	cukrárna
<g/>
,	,	kIx,	,
mléčných	mléčný	k2eAgInPc6d1	mléčný
barech	bar	k1gInPc6	bar
apod.	apod.	kA	apod.
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
koktejl	koktejl	k1gInSc4	koktejl
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
koktejl	koktejl	k1gInSc4	koktejl
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
1200	[number]	k4	1200
receptů	recept	k1gInPc2	recept
na	na	k7c4	na
koktejly	koktejl	k1gInPc4	koktejl
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
Smíchejto	Smíchejto	k1gNnSc1	Smíchejto
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Recepty	recept	k1gInPc1	recept
na	na	k7c4	na
koktejly	koktejl	k1gInPc4	koktejl
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
Alkoholium	Alkoholium	k1gNnSc1	Alkoholium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
