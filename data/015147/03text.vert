<s>
Herbert	Herbert	k1gMnSc1
Zimmermann	Zimmermann	k1gMnSc1
</s>
<s>
Herbert	Herbert	k1gMnSc1
Zimmermann	Zimmermann	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1954	#num#	k4
(	(	kIx(
<g/>
66	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Engers	Engers	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
fotbalista	fotbalista	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
</s>
<s>
1980	#num#	k4
</s>
<s>
SRN	SRN	kA
</s>
<s>
Herbert	Herbert	k1gMnSc1
Zimmermann	Zimmermann	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1954	#num#	k4
<g/>
,	,	kIx,
Engers	Engers	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
německý	německý	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
reprezentoval	reprezentovat	k5eAaImAgMnS
Západní	západní	k2eAgNnSc4d1
Německo	Německo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
kariéry	kariéra	k1gFnSc2
hrál	hrát	k5eAaImAgInS
na	na	k7c6
postu	post	k1gInSc6
útočníka	útočník	k1gMnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
obránce	obránce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Se	s	k7c7
západoněmeckou	západoněmecký	k2eAgFnSc7d1
reprezentací	reprezentace	k1gFnSc7
vyhrál	vyhrát	k5eAaPmAgMnS
mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
1980	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
bojů	boj	k1gInPc2
na	na	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
však	však	k9
nezasáhl	zasáhnout	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrál	hrát	k5eAaImAgMnS
ale	ale	k9
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
v	v	k7c6
Argentině	Argentina	k1gFnSc6
roku	rok	k1gInSc2
1978	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
národním	národní	k2eAgInSc6d1
týmu	tým	k1gInSc6
odehrál	odehrát	k5eAaPmAgInS
14	#num#	k4
utkání	utkání	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc2
vstřelil	vstřelit	k5eAaPmAgMnS
2	#num#	k4
branky	branka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třikrát	třikrát	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mistrem	mistr	k1gMnSc7
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
dvakrát	dvakrát	k6eAd1
s	s	k7c7
Bayernem	Bayern	k1gInSc7
Mnichov	Mnichov	k1gInSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jednou	jednou	k6eAd1
s	s	k7c7
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Köln	Kölna	k1gFnPc2
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Kolínem	Kolín	k1gInSc7
také	také	k6eAd1
získal	získat	k5eAaPmAgMnS
tři	tři	k4xCgInPc4
německé	německý	k2eAgInPc4d1
poháry	pohár	k1gInPc4
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.rsssf.com/tables/80e-full.html	http://www.rsssf.com/tables/80e-full.html	k1gInSc1
<g/>
↑	↑	k?
http://www.rsssf.com/tables/78full.html	http://www.rsssf.com/tables/78full.html	k1gInSc1
<g/>
↑	↑	k?
http://www.fussballdaten.de/spieler/zimmermannherbert/1984/	http://www.fussballdaten.de/spieler/zimmermannherbert/1984/	k4
<g/>
↑	↑	k?
http://www.national-football-teams.com/player/16898/Herbert_Zimmermann.html	http://www.national-football-teams.com/player/16898/Herbert_Zimmermann.html	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Medailisté	medailista	k1gMnPc1
-	-	kIx~
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1980	#num#	k4
NSR	NSR	kA
</s>
<s>
Harald	Harald	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
Hans-Peter	Hans-Peter	k1gMnSc1
Briegel	Briegel	k1gMnSc1
•	•	k?
Bernhard	Bernhard	k1gMnSc1
Cullmann	Cullmann	k1gMnSc1
•	•	k?
Karlheinz	Karlheinz	k1gMnSc1
Förster	Förster	k1gMnSc1
•	•	k?
Bernard	Bernard	k1gMnSc1
Dietz	Dietz	k1gMnSc1
•	•	k?
Bernd	Bernd	k1gMnSc1
Schuster	Schuster	k1gMnSc1
•	•	k?
Bernd	Bernd	k1gMnSc1
Förster	Förster	k1gMnSc1
•	•	k?
Karl-Heinz	Karl-Heinz	k1gMnSc1
Rummenigge	Rummenigg	k1gFnSc2
•	•	k?
Horst	Horst	k1gMnSc1
Hrubesch	Hrubesch	k1gMnSc1
•	•	k?
Hansi	Hans	k1gMnPc7
Müller	Müller	k1gMnSc1
•	•	k?
Klaus	Klaus	k1gMnSc1
Allofs	Allofs	k1gInSc1
•	•	k?
Caspar	Caspar	k1gInSc1
Memering	Memering	k1gInSc1
•	•	k?
Rainer	Rainer	k1gInSc1
Bonhof	Bonhof	k1gMnSc1
•	•	k?
Felix	Felix	k1gMnSc1
Magath	Magath	k1gMnSc1
•	•	k?
Uli	Uli	k1gMnSc1
Stielike	Stielik	k1gFnSc2
•	•	k?
Herbert	Herbert	k1gMnSc1
Zimmermann	Zimmermann	k1gMnSc1
•	•	k?
Karl	Karl	k1gMnSc1
Del	Del	k1gMnSc1
<g/>
'	'	kIx"
<g/>
Haye	Haye	k1gFnSc1
•	•	k?
Lothar	Lothar	k1gMnSc1
Matthäus	Matthäus	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Votava	Votava	k1gMnSc1
•	•	k?
Manfred	Manfred	k1gMnSc1
Kaltz	Kaltz	k1gMnSc1
•	•	k?
Walter	Walter	k1gMnSc1
Junghans	Junghansa	k1gFnPc2
•	•	k?
Eike	Eike	k1gNnSc4
Immel	Immlo	k1gNnPc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Jupp	Jupp	k1gMnSc1
Derwall	Derwall	k1gMnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
Theo	Thea	k1gFnSc5
Custers	Custers	k1gInSc4
•	•	k?
Eric	Eric	k1gInSc1
Gerets	Gerets	k1gInSc1
•	•	k?
Luc	Luc	k1gFnSc2
Millecamps	Millecampsa	k1gFnPc2
•	•	k?
Walter	Walter	k1gMnSc1
Meeuws	Meeuwsa	k1gFnPc2
•	•	k?
Michel	Michel	k1gMnSc1
Renquin	Renquin	k2eAgMnSc1d1
•	•	k?
Julien	Julien	k2eAgInSc1d1
Cools	Cools	k1gInSc1
•	•	k?
René	René	k1gMnSc1
Vandereycken	Vandereycken	k2eAgMnSc1d1
•	•	k?
Wilfried	Wilfried	k1gInSc1
Van	van	k1gInSc1
Moer	Moer	k1gInSc1
•	•	k?
François	François	k1gInSc1
Van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Elst	Elst	k1gInSc1
•	•	k?
Erwin	Erwin	k1gMnSc1
Vandenbergh	Vandenbergh	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Ceulemans	Ceulemans	k1gInSc1
•	•	k?
Jean-Marie	Jean-Marie	k1gFnSc2
Pfaff	Pfaff	k1gMnSc1
•	•	k?
Maurice	Maurika	k1gFnSc3
Martens	Martensa	k1gFnPc2
•	•	k?
Gerard	Gerard	k1gInSc1
Plessers	Plessers	k1gInSc1
•	•	k?
René	René	k1gMnSc1
Verheyen	Verheyen	k2eAgMnSc1d1
•	•	k?
Marc	Marc	k1gInSc1
Millecamps	Millecamps	k1gInSc1
•	•	k?
Raymond	Raymond	k1gInSc1
Mommens	Mommens	k1gInSc1
•	•	k?
Guy	Guy	k1gMnSc5
Dardenne	Dardenn	k1gMnSc5
•	•	k?
Willy	Willa	k1gFnPc1
Wellens	Wellensa	k1gFnPc2
•	•	k?
Michel	Michel	k1gMnSc1
Preud	Preud	k1gMnSc1
<g/>
'	'	kIx"
<g/>
homme	hommat	k5eAaPmIp3nS
•	•	k?
Jos	Jos	k1gFnSc1
Heyligen	Heyligen	k1gInSc1
•	•	k?
Ronny	Ronna	k1gFnSc2
Martens	Martensa	k1gFnPc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Guy	Guy	k1gMnSc1
Thys	Thysa	k1gFnPc2
Československo	Československo	k1gNnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Netolička	Netolička	k1gMnSc1
•	•	k?
Jozef	Jozef	k1gMnSc1
Barmoš	Barmoš	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Jurkemik	Jurkemik	k1gMnSc1
•	•	k?
Anton	Anton	k1gMnSc1
Ondruš	Ondruš	k1gMnSc1
•	•	k?
Koloman	Koloman	k1gMnSc1
Gögh	Gögh	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Štambachr	Štambachr	k1gMnSc1
•	•	k?
Ján	Ján	k1gMnSc1
Kozák	Kozák	k1gMnSc1
•	•	k?
Antonín	Antonín	k1gMnSc1
Panenka	panenka	k1gFnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Gajdůšek	Gajdůšek	k1gMnSc1
•	•	k?
Marián	Marián	k1gMnSc1
Masný	masný	k2eAgMnSc1d1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Nehoda	nehoda	k1gFnSc1
•	•	k?
Rostislav	Rostislav	k1gMnSc1
Vojáček	Vojáček	k1gMnSc1
•	•	k?
Verner	Verner	k1gMnSc1
Lička	lička	k1gFnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Fiala	Fiala	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Vízek	Vízek	k1gMnSc1
•	•	k?
Oldřich	Oldřich	k1gMnSc1
Rott	Rott	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Pollák	Pollák	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
Karol	Karol	k1gInSc1
Dobiaš	Dobiaš	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Němec	Němec	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Seman	Seman	k1gMnSc1
•	•	k?
Dušan	Dušan	k1gMnSc1
Keketi	Keket	k1gMnPc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Jozef	Jozef	k1gMnSc1
Vengloš	Vengloš	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
