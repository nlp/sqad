<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
závěru	závěr	k1gInSc6	závěr
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
ústředním	ústřední	k2eAgInSc7d1	ústřední
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
prevence	prevence	k1gFnSc1	prevence
vzniku	vznik	k1gInSc2	vznik
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
dalšího	další	k2eAgInSc2d1	další
podobného	podobný	k2eAgInSc2d1	podobný
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
