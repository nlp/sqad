<s>
Etymologie	etymologie	k1gFnSc1	etymologie
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
lingvistiky	lingvistika	k1gFnSc2	lingvistika
zkoumající	zkoumající	k2eAgInSc4d1	zkoumající
původ	původ	k1gInSc4	původ
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
sdruženy	sdružen	k2eAgFnPc4d1	sdružena
mj.	mj.	kA	mj.
v	v	k7c6	v
etymologických	etymologický	k2eAgInPc6d1	etymologický
slovnících	slovník	k1gInPc6	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
jazyce	jazyk	k1gInSc6	jazyk
existují	existovat	k5eAaImIp3nP	existovat
historické	historický	k2eAgFnPc1d1	historická
(	(	kIx(	(
<g/>
psané	psaný	k2eAgInPc4d1	psaný
<g/>
)	)	kIx)	)
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
využívány	využíván	k2eAgFnPc1d1	využívána
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byla	být	k5eAaImAgNnP	být
slova	slovo	k1gNnPc1	slovo
dříve	dříve	k6eAd2	dříve
používána	používán	k2eAgNnPc1d1	používáno
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
dotyčném	dotyčný	k2eAgInSc6d1	dotyčný
jazyce	jazyk	k1gInSc6	jazyk
objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Etymologové	etymolog	k1gMnPc1	etymolog
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
metodu	metoda	k1gFnSc4	metoda
komparativní	komparativní	k2eAgFnSc2d1	komparativní
lingvistiky	lingvistika	k1gFnSc2	lingvistika
k	k	k7c3	k
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
staré	starý	k2eAgFnPc1d1	stará
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
existovala	existovat	k5eAaImAgFnS	existovat
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
přímá	přímý	k2eAgFnSc1d1	přímá
informace	informace	k1gFnSc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
Analýzou	analýza	k1gFnSc7	analýza
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
jazyků	jazyk	k1gInPc2	jazyk
pomocí	pomocí	k7c2	pomocí
komparativní	komparativní	k2eAgFnSc2d1	komparativní
metody	metoda	k1gFnSc2	metoda
mohou	moct	k5eAaImIp3nP	moct
zjistit	zjistit	k5eAaPmF	zjistit
něco	něco	k3yInSc4	něco
o	o	k7c6	o
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gMnSc7	jejich
společným	společný	k2eAgMnSc7d1	společný
předchůdcem	předchůdce	k1gMnSc7	předchůdce
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
slovní	slovní	k2eAgFnSc3d1	slovní
zásobě	zásoba	k1gFnSc3	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
kořeny	kořen	k1gInPc1	kořen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vedou	vést	k5eAaImIp3nP	vést
mj.	mj.	kA	mj.
k	k	k7c3	k
původu	původ	k1gInSc3	původ
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
-	-	kIx~	-
Ačkoliv	ačkoliv	k8xS	ačkoliv
etymologický	etymologický	k2eAgInSc1d1	etymologický
výzkum	výzkum	k1gInSc1	výzkum
vzešel	vzejít	k5eAaPmAgInS	vzejít
z	z	k7c2	z
filologické	filologický	k2eAgFnSc2d1	filologická
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
především	především	k9	především
jazykové	jazykový	k2eAgFnPc1d1	jazyková
rodiny	rodina	k1gFnPc1	rodina
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
málo	málo	k1gNnSc1	málo
nebo	nebo	k8xC	nebo
žádná	žádný	k3yNgFnSc1	žádný
dokumentace	dokumentace	k1gFnSc1	dokumentace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
uralská	uralský	k2eAgFnSc1d1	Uralská
nebo	nebo	k8xC	nebo
austronéská	austronéský	k2eAgFnSc1d1	austronéský
<g/>
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
etymologickým	etymologický	k2eAgInSc7d1	etymologický
oborem	obor	k1gInSc7	obor
je	být	k5eAaImIp3nS	být
onomastika	onomastika	k1gFnSc1	onomastika
<g/>
;	;	kIx,	;
původ	původ	k1gInSc1	původ
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Etymologická	etymologický	k2eAgFnSc1d1	etymologická
příbuznost	příbuznost	k1gFnSc1	příbuznost
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
filiace	filiace	k1gFnSc1	filiace
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
etymologie	etymologie	k1gFnSc2	etymologie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
ἐ	ἐ	k?	ἐ
(	(	kIx(	(
<g/>
etymologia	etymologia	k1gFnSc1	etymologia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
ἔ	ἔ	k?	ἔ
(	(	kIx(	(
<g/>
etymon	etymon	k1gNnSc1	etymon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
smysl	smysl	k1gInSc1	smysl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kořen	kořen	k1gInSc1	kořen
slova	slovo	k1gNnSc2	slovo
<g/>
"	"	kIx"	"
a	a	k8xC	a
-λ	-λ	k?	-λ
(	(	kIx(	(
<g/>
-logia	ogia	k1gFnSc1	-logia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nauka	nauka	k1gFnSc1	nauka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
vědecké	vědecký	k2eAgFnSc2d1	vědecká
etymologie	etymologie	k1gFnSc2	etymologie
existovala	existovat	k5eAaImAgFnS	existovat
lidová	lidový	k2eAgFnSc1d1	lidová
etymologie	etymologie	k1gFnSc1	etymologie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lidé	člověk	k1gMnPc1	člověk
mnohdy	mnohdy	k6eAd1	mnohdy
pouze	pouze	k6eAd1	pouze
odhadovali	odhadovat	k5eAaImAgMnP	odhadovat
původ	původ	k1gInSc4	původ
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nesprávně	správně	k6eNd1	správně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobnosti	podobnost	k1gFnSc2	podobnost
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
peklo	peklo	k1gNnSc1	peklo
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
peče	péct	k5eAaImIp3nS	péct
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
staršího	starý	k2eAgInSc2d2	starší
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
smůlu	smůla	k1gFnSc4	smůla
či	či	k8xC	či
dehet	dehet	k1gInSc4	dehet
<g/>
;	;	kIx,	;
trpaslík	trpaslík	k1gMnSc1	trpaslík
od	od	k7c2	od
slovesa	sloveso	k1gNnSc2	sloveso
trpět	trpět	k5eAaImF	trpět
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
staroslověnského	staroslověnský	k2eAgInSc2d1	staroslověnský
"	"	kIx"	"
<g/>
trь	trь	k?	trь
<g/>
"	"	kIx"	"
-	-	kIx~	-
tj.	tj.	kA	tj.
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
měří	měřit	k5eAaImIp3nS	měřit
tři	tři	k4xCgFnPc4	tři
pěsti	pěst	k1gFnPc4	pěst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mluvčí	mluvčit	k5eAaImIp3nS	mluvčit
této	tento	k3xDgFnSc3	tento
domněnce	domněnka	k1gFnSc3	domněnka
podobu	podoba	k1gFnSc4	podoba
slova	slovo	k1gNnSc2	slovo
přizpůsobili	přizpůsobit	k5eAaPmAgMnP	přizpůsobit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
veždajší	veždajšit	k5eAaPmIp3nS	veždajšit
<g/>
"	"	kIx"	"
od	od	k7c2	od
vždy	vždy	k6eAd1	vždy
k	k	k7c3	k
"	"	kIx"	"
<g/>
vezdejší	vezdejší	k2eAgMnSc1d1	vezdejší
<g/>
"	"	kIx"	"
od	od	k7c2	od
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
protěžovat	protěžovat	k5eAaImF	protěžovat
<g/>
"	"	kIx"	"
domněle	domněle	k6eAd1	domněle
od	od	k7c2	od
těžký	těžký	k2eAgInSc4d1	těžký
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
správného	správný	k2eAgInSc2d1	správný
"	"	kIx"	"
<g/>
protežovat	protežovat	k5eAaImF	protežovat
<g/>
"	"	kIx"	"
-	-	kIx~	-
od	od	k7c2	od
protekce	protekce	k1gFnSc2	protekce
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příklady	příklad	k1gInPc7	příklad
lidové	lidový	k2eAgFnSc2d1	lidová
etymologie	etymologie	k1gFnSc2	etymologie
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
období	období	k1gNnSc6	období
humanismu	humanismus	k1gInSc2	humanismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
např	např	kA	např
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
Zikimunda	Zikimund	k1gMnSc2	Zikimund
Hrubého	Hrubý	k1gMnSc2	Hrubý
z	z	k7c2	z
Jelení	jelení	k2eAgFnSc2d1	jelení
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Blahoslava	Blahoslav	k1gMnSc2	Blahoslav
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Ámose	Ámos	k1gMnSc2	Ámos
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
Blahoslavem	Blahoslav	k1gMnSc7	Blahoslav
odvozován	odvozován	k2eAgInSc4d1	odvozován
od	od	k7c2	od
slov	slovo	k1gNnPc2	slovo
kost	kost	k1gFnSc1	kost
a	a	k8xC	a
popel	popel	k1gInSc1	popel
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přejímka	přejímka	k1gFnSc1	přejímka
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
castellum	castellum	k1gInSc4	castellum
-	-	kIx~	-
"	"	kIx"	"
<g/>
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
slovo	slovo	k1gNnSc4	slovo
svatba	svatba	k1gFnSc1	svatba
jako	jako	k9	jako
"	"	kIx"	"
<g/>
sva	sva	k?	sva
dva	dva	k4xCgInPc4	dva
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jsme	být	k5eAaImIp1nP	být
dva	dva	k4xCgInPc1	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
etymologie	etymologie	k1gFnSc1	etymologie
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
u	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
skutečný	skutečný	k2eAgInSc1d1	skutečný
původ	původ	k1gInSc1	původ
není	být	k5eNaImIp3nS	být
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
obecně	obecně	k6eAd1	obecně
známý	známý	k2eAgMnSc1d1	známý
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
protěžovat	protěžovat	k5eAaImF	protěžovat
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc4	místo
protežovat	protežovat	k5eAaImF	protežovat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
antidatovat	antidatovat	k5eAaImF	antidatovat
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc4	místo
antedatovat	antedatovat	k5eAaBmF	antedatovat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
hrozinka	hrozinka	k1gFnSc1	hrozinka
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
českých	český	k2eAgInPc2d1	český
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
rozinka	rozinka	k1gFnSc1	rozinka
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
rosine	rosinout	k5eAaPmIp3nS	rosinout
<g/>
,	,	kIx,	,
ikdyž	ikdyž	k6eAd1	ikdyž
natolik	natolik	k6eAd1	natolik
staré	starý	k2eAgFnPc1d1	stará
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
vžité	vžitý	k2eAgNnSc1d1	vžité
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
přijato	přijmout	k5eAaPmNgNnS	přijmout
za	za	k7c4	za
pravopisně	pravopisně	k6eAd1	pravopisně
správné	správný	k2eAgNnSc1d1	správné
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
iniciály	iniciála	k1gFnPc4	iniciála
<g/>
"	"	kIx"	"
místo	místo	k7c2	místo
nacionále	nacionále	k1gNnSc2	nacionále
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
u	u	k7c2	u
frází	fráze	k1gFnPc2	fráze
<g/>
.	.	kIx.	.
např.	např.	kA	např.
rčení	rčení	k1gNnSc4	rčení
panický	panický	k2eAgInSc1d1	panický
strach	strach	k1gInSc1	strach
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
řeckého	řecký	k2eAgMnSc2d1	řecký
bůžka	bůžek	k1gMnSc2	bůžek
Pana	Pan	k1gMnSc2	Pan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
strašil	strašit	k5eAaImAgMnS	strašit
pastýře	pastýř	k1gMnPc4	pastýř
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc7d1	Česká
lidovou	lidový	k2eAgFnSc7d1	lidová
etymologií	etymologie	k1gFnSc7	etymologie
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
přiřazováno	přiřazovat	k5eAaImNgNnS	přiřazovat
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
panic	panice	k1gFnPc2	panice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
jazyka	jazyk	k1gInSc2	jazyk
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
významu	význam	k1gInSc2	význam
stávajících	stávající	k2eAgNnPc2d1	stávající
slov	slovo	k1gNnPc2	slovo
i	i	k9	i
ke	k	k7c3	k
vzniku	vznik	k1gInSc2	vznik
jak	jak	k8xC	jak
nových	nový	k2eAgNnPc2d1	nové
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
tak	tak	k9	tak
nových	nový	k2eAgFnPc2d1	nová
skutečností	skutečnost	k1gFnPc2	skutečnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgNnPc1d1	nové
slova	slovo	k1gNnPc1	slovo
(	(	kIx(	(
<g/>
neologismy	neologismus	k1gInPc1	neologismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
jakkoliv	jakkoliv	k6eAd1	jakkoliv
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zažívají	zažívat	k5eAaImIp3nP	zažívat
(	(	kIx(	(
<g/>
zdomácňují	zdomácňovat	k5eAaImIp3nP	zdomácňovat
<g/>
,	,	kIx,	,
neutralizují	neutralizovat	k5eAaBmIp3nP	neutralizovat
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
zpravidla	zpravidla	k6eAd1	zpravidla
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
úspěšností	úspěšnost	k1gFnSc7	úspěšnost
<g/>
,	,	kIx,	,
u	u	k7c2	u
slangů	slang	k1gInPc2	slang
je	být	k5eAaImIp3nS	být
vývoj	vývoj	k1gInSc4	vývoj
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgInSc1d1	prvotní
původ	původ	k1gInSc1	původ
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
autentický	autentický	k2eAgMnSc1d1	autentický
<g/>
,	,	kIx,	,
nekonvenční	konvenční	k2eNgInSc1d1	nekonvenční
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
projevy	projev	k1gInPc1	projev
postupně	postupně	k6eAd1	postupně
mohou	moct	k5eAaImIp3nP	moct
nabýt	nabýt	k5eAaPmF	nabýt
významu	význam	k1gInSc3	význam
konvenčních	konvenční	k2eAgInPc2d1	konvenční
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
expresivita	expresivita	k1gFnSc1	expresivita
<g/>
,	,	kIx,	,
citoslovečnost	citoslovečnost	k1gFnSc1	citoslovečnost
(	(	kIx(	(
<g/>
interjekce	interjekce	k1gFnSc1	interjekce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žvatlavost	žvatlavost	k1gFnSc4	žvatlavost
–	–	k?	–
spontánní	spontánní	k2eAgInPc4d1	spontánní
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
náhodný	náhodný	k2eAgInSc1d1	náhodný
projev	projev	k1gInSc1	projev
mluvidel	mluvidla	k1gNnPc2	mluvidla
<g/>
,	,	kIx,	,
tvorba	tvorba	k1gFnSc1	tvorba
zvuků	zvuk	k1gInPc2	zvuk
vyjadřujících	vyjadřující	k2eAgInPc2d1	vyjadřující
emocionální	emocionální	k2eAgInSc4d1	emocionální
stav	stav	k1gInSc4	stav
(	(	kIx(	(
<g/>
specifickým	specifický	k2eAgInSc7d1	specifický
projevem	projev	k1gInSc7	projev
této	tento	k3xDgFnSc2	tento
snahy	snaha	k1gFnSc2	snaha
je	být	k5eAaImIp3nS	být
i	i	k9	i
zpěv	zpěv	k1gInSc1	zpěv
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
onomatopoie	onomatopoie	k1gFnSc1	onomatopoie
<g/>
,	,	kIx,	,
napodobování	napodobování	k1gNnSc1	napodobování
zvuků	zvuk	k1gInPc2	zvuk
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
fičet	fičet	k5eAaImF	fičet
<g/>
,	,	kIx,	,
bručet	bručet	k5eAaImF	bručet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
aspekt	aspekt	k1gInSc1	aspekt
zvukomalebnosti	zvukomalebnost	k1gFnSc2	zvukomalebnost
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
oblíbenost	oblíbenost	k1gFnSc4	oblíbenost
slov	slovo	k1gNnPc2	slovo
vzniklých	vzniklý	k2eAgNnPc2d1	vzniklé
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
traktor	traktor	k1gInSc1	traktor
<g/>
,	,	kIx,	,
trabant	trabant	k1gInSc1	trabant
<g/>
,	,	kIx,	,
mašinka	mašinka	k1gFnSc1	mašinka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
slov	slovo	k1gNnPc2	slovo
lze	lze	k6eAd1	lze
mluvit	mluvit	k5eAaImF	mluvit
i	i	k9	i
například	například	k6eAd1	například
o	o	k7c6	o
pohybomalebnosti	pohybomalebnost	k1gFnSc6	pohybomalebnost
(	(	kIx(	(
<g/>
sklápět	sklápět	k5eAaImF	sklápět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
jeho	jeho	k3xOp3gInSc4	jeho
původní	původní	k2eAgInSc4d1	původní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
změny	změna	k1gFnPc4	změna
expresivního	expresivní	k2eAgInSc2d1	expresivní
náboje	náboj	k1gInSc2	náboj
či	či	k8xC	či
stylové	stylový	k2eAgFnSc2d1	stylová
<g/>
,	,	kIx,	,
citové	citový	k2eAgFnSc2d1	citová
či	či	k8xC	či
postojové	postojový	k2eAgFnSc2d1	postojová
příznačnosti	příznačnost	k1gFnSc2	příznačnost
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
z	z	k7c2	z
hanlivého	hanlivý	k2eAgInSc2d1	hanlivý
nebo	nebo	k8xC	nebo
uznalého	uznalý	k2eAgInSc2d1	uznalý
na	na	k7c4	na
neutrální	neutrální	k2eAgInSc4d1	neutrální
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
ze	z	k7c2	z
slangového	slangový	k2eAgInSc2d1	slangový
na	na	k7c6	na
obecné	obecná	k1gFnSc6	obecná
nebo	nebo	k8xC	nebo
spisovné	spisovný	k2eAgNnSc1d1	spisovné
<g/>
)	)	kIx)	)
změna	změna	k1gFnSc1	změna
konotací	konotace	k1gFnPc2	konotace
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
asociací	asociace	k1gFnPc2	asociace
<g/>
)	)	kIx)	)
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
významem	význam	k1gInSc7	význam
slova	slovo	k1gNnSc2	slovo
rozšíření	rozšíření	k1gNnSc2	rozšíření
či	či	k8xC	či
zúžení	zúžení	k1gNnSc2	zúžení
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
přenesení	přenesení	k1gNnSc2	přenesení
jádra	jádro	k1gNnSc2	jádro
významu	význam	k1gInSc2	význam
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
prototyp	prototyp	k1gInSc4	prototyp
(	(	kIx(	(
<g/>
základní	základní	k2eAgInSc4d1	základní
typ	typ	k1gInSc4	typ
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
šíře	šíř	k1gFnSc2	šíř
významu	význam	k1gInSc2	význam
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
lampou	lampa	k1gFnSc7	lampa
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
rozumí	rozumět	k5eAaImIp3nS	rozumět
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
elektrická	elektrický	k2eAgFnSc1d1	elektrická
lampa	lampa	k1gFnSc1	lampa
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
olejová	olejový	k2eAgFnSc1d1	olejová
<g/>
,	,	kIx,	,
sídlištěm	sídliště	k1gNnSc7	sídliště
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
primárně	primárně	k6eAd1	primárně
soubor	soubor	k1gInSc4	soubor
panelových	panelový	k2eAgInPc2d1	panelový
výškových	výškový	k2eAgInPc2d1	výškový
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
chatrčí	chatrč	k1gFnPc2	chatrč
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
metafora	metafora	k1gFnSc1	metafora
–	–	k?	–
rozšíření	rozšíření	k1gNnSc1	rozšíření
nebo	nebo	k8xC	nebo
přenesení	přenesení	k1gNnSc1	přenesení
významu	význam	k1gInSc2	význam
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobnosti	podobnost	k1gFnSc2	podobnost
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
metafory	metafora	k1gFnPc1	metafora
například	například	k6eAd1	například
z	z	k7c2	z
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
hlava	hlava	k1gFnSc1	hlava
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
noha	noha	k1gFnSc1	noha
stolu	stol	k1gInSc2	stol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
metonymie	metonymie	k1gFnSc1	metonymie
–	–	k?	–
přenesení	přenesení	k1gNnSc1	přenesení
významu	význam	k1gInSc2	význam
podle	podle	k7c2	podle
souvislostí	souvislost	k1gFnPc2	souvislost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
z	z	k7c2	z
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
pojmu	pojem	k1gInSc2	pojem
profil	profil	k1gInSc1	profil
se	se	k3xPyFc4	se
význam	význam	k1gInSc1	význam
přenese	přenést	k5eAaPmIp3nS	přenést
na	na	k7c4	na
typ	typ	k1gInSc4	typ
kovodělných	kovodělný	k2eAgInPc2d1	kovodělný
polotovarů	polotovar	k1gInPc2	polotovar
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
názvu	název	k1gInSc2	název
činnosti	činnost	k1gFnSc2	činnost
(	(	kIx(	(
<g/>
obchod	obchod	k1gInSc1	obchod
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
přenese	přenést	k5eAaPmIp3nS	přenést
na	na	k7c4	na
místnost	místnost	k1gFnSc4	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
činnost	činnost	k1gFnSc1	činnost
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
(	(	kIx(	(
<g/>
prodejnu	prodejna	k1gFnSc4	prodejna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
specifickým	specifický	k2eAgInSc7d1	specifický
případem	případ	k1gInSc7	případ
metafory	metafora	k1gFnSc2	metafora
nebo	nebo	k8xC	nebo
metonymie	metonymie	k1gFnSc2	metonymie
je	být	k5eAaImIp3nS	být
apelativizace	apelativizace	k1gFnSc1	apelativizace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
slovo	slovo	k1gNnSc4	slovo
z	z	k7c2	z
vlastního	vlastní	k2eAgNnSc2d1	vlastní
jména	jméno	k1gNnSc2	jméno
získá	získat	k5eAaPmIp3nS	získat
obecný	obecný	k2eAgInSc1d1	obecný
význam	význam	k1gInSc1	význam
(	(	kIx(	(
<g/>
xerox	xerox	k1gInSc1	xerox
<g/>
,	,	kIx,	,
donkichot	donkichot	k1gMnSc1	donkichot
<g/>
,	,	kIx,	,
šovinista	šovinista	k1gMnSc1	šovinista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
apelativizace	apelativizace	k1gFnSc1	apelativizace
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
<g />
.	.	kIx.	.
</s>
<s>
také	také	k9	také
u	u	k7c2	u
frází	fráze	k1gFnPc2	fráze
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
číst	číst	k5eAaImF	číst
Tolstého	Tolstý	k2eAgNnSc2d1	Tolstý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
poslouchat	poslouchat	k5eAaImF	poslouchat
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
naplnění	naplnění	k1gNnSc1	naplnění
obsahu	obsah	k1gInSc2	obsah
–	–	k?	–
do	do	k7c2	do
šíře	šíř	k1gFnSc2	šíř
pojmu	pojem	k1gInSc2	pojem
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
změny	změna	k1gFnSc2	změna
významu	význam	k1gInSc2	význam
vejdou	vejít	k5eAaPmIp3nP	vejít
nové	nový	k2eAgInPc1d1	nový
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgFnPc6	jenž
původní	původní	k2eAgMnSc1d1	původní
uživatelé	uživatel	k1gMnPc1	uživatel
neměli	mít	k5eNaImAgMnP	mít
tušení	tušení	k1gNnSc4	tušení
<g/>
,	,	kIx,	,
a	a	k8xC	a
jádro	jádro	k1gNnSc1	jádro
významu	význam	k1gInSc2	význam
se	se	k3xPyFc4	se
přenese	přenést	k5eAaPmIp3nS	přenést
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slovo	slovo	k1gNnSc1	slovo
letadlo	letadlo	k1gNnSc4	letadlo
ve	v	k7c6	v
staročeštině	staročeština	k1gFnSc6	staročeština
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pták	pták	k1gMnSc1	pták
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
původních	původní	k2eAgNnPc2d1	původní
slov	slovo	k1gNnPc2	slovo
lze	lze	k6eAd1	lze
vytvářet	vytvářet	k5eAaImF	vytvářet
odvozeniny	odvozenina	k1gFnSc2	odvozenina
a	a	k8xC	a
složeniny	složenina	k1gFnSc2	složenina
<g/>
.	.	kIx.	.
tvořivé	tvořivý	k2eAgNnSc1d1	tvořivé
přizpůsobování	přizpůsobování	k1gNnSc1	přizpůsobování
stávajících	stávající	k2eAgNnPc2d1	stávající
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
komolení	komolení	k1gNnSc1	komolení
i	i	k8xC	i
celková	celkový	k2eAgFnSc1d1	celková
tendence	tendence	k1gFnSc1	tendence
vývoje	vývoj	k1gInSc2	vývoj
jazyka	jazyk	k1gInSc2	jazyk
disimilace	disimilace	k1gFnSc1	disimilace
–	–	k?	–
rozdělení	rozdělení	k1gNnSc2	rozdělení
původně	původně	k6eAd1	původně
jediného	jediný	k2eAgNnSc2d1	jediné
slova	slovo	k1gNnSc2	slovo
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
významy	význam	k1gInPc7	význam
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
slova	slovo	k1gNnSc2	slovo
legrace	legrace	k1gFnSc2	legrace
a	a	k8xC	a
rekreace	rekreace	k1gFnSc2	rekreace
z	z	k7c2	z
"	"	kIx"	"
<g/>
rekrací	rekrace	k1gFnPc2	rekrace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
původem	původ	k1gMnSc7	původ
latinského	latinský	k2eAgInSc2d1	latinský
<g />
.	.	kIx.	.
</s>
<s>
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
školní	školní	k2eAgFnSc4d1	školní
přestávku	přestávka	k1gFnSc4	přestávka
příponami	přípona	k1gFnPc7	přípona
<g/>
,	,	kIx,	,
předponami	předpona	k1gFnPc7	předpona
<g/>
,	,	kIx,	,
koncovkami	koncovka	k1gFnPc7	koncovka
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc7d1	jiná
flexí	flexe	k1gFnSc7	flexe
skládáním	skládání	k1gNnSc7	skládání
(	(	kIx(	(
<g/>
slovních	slovní	k2eAgInPc2d1	slovní
kořenů	kořen	k1gInPc2	kořen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
němčinu	němčina	k1gFnSc4	němčina
<g/>
)	)	kIx)	)
spojováním	spojování	k1gNnSc7	spojování
kořenových	kořenový	k2eAgFnPc2d1	kořenová
i	i	k8xC	i
nekořenových	kořenový	k2eNgFnPc2d1	kořenový
částí	část	k1gFnPc2	část
slov	slovo	k1gNnPc2	slovo
nesoucích	nesoucí	k2eAgMnPc2d1	nesoucí
významové	významový	k2eAgNnSc4d1	významové
asociace	asociace	k1gFnPc4	asociace
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc6	který
nabyly	nabýt	k5eAaPmAgFnP	nabýt
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
celku	celek	k1gInSc6	celek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
workoholismus	workoholismus	k1gInSc1	workoholismus
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
podobností	podobnost	k1gFnSc7	podobnost
s	s	k7c7	s
alkoholismem	alkoholismus	k1gInSc7	alkoholismus
<g/>
,	,	kIx,	,
standardní	standardní	k2eAgFnSc7d1	standardní
slovotvorbou	slovotvorba	k1gFnSc7	slovotvorba
by	by	kYmCp3nP	by
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
například	například	k6eAd1	například
workismus	workismus	k1gInSc1	workismus
<g/>
)	)	kIx)	)
–	–	k?	–
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
smysl	smysl	k1gInSc1	smysl
pro	pro	k7c4	pro
klasickou	klasický	k2eAgFnSc4d1	klasická
etymologii	etymologie	k1gFnSc4	etymologie
oslaben	oslabit	k5eAaPmNgInS	oslabit
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
makarónština	makarónština	k1gFnSc1	makarónština
<g/>
.	.	kIx.	.
analogie	analogie	k1gFnSc1	analogie
–	–	k?	–
přizpůsobování	přizpůsobování	k1gNnSc1	přizpůsobování
slova	slovo	k1gNnSc2	slovo
nebo	nebo	k8xC	nebo
tvaru	tvar	k1gInSc2	tvar
slovu	slout	k5eAaImIp1nS	slout
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
významem	význam	k1gInSc7	význam
(	(	kIx(	(
<g/>
rozinka	rozinka	k1gFnSc1	rozinka
–	–	k?	–
hrozinka	hrozinka	k1gFnSc1	hrozinka
podle	podle	k7c2	podle
slova	slovo	k1gNnSc2	slovo
hrozen	hrozen	k1gInSc1	hrozen
<g/>
)	)	kIx)	)
kontaminace	kontaminace	k1gFnSc1	kontaminace
–	–	k?	–
dvě	dva	k4xCgFnPc4	dva
slova	slovo	k1gNnPc1	slovo
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
významem	význam	k1gInSc7	význam
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
nebo	nebo	k8xC	nebo
splynou	splynout	k5eAaPmIp3nP	splynout
(	(	kIx(	(
<g/>
tip	tip	k1gInSc1	tip
a	a	k8xC	a
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
bujarý	bujarý	k2eAgInSc1d1	bujarý
z	z	k7c2	z
bujný	bujný	k2eAgInSc1d1	bujný
a	a	k8xC	a
jarý	jarý	k2eAgInSc1d1	jarý
<g/>
,	,	kIx,	,
rozřešení	rozřešení	k1gNnSc1	rozřešení
a	a	k8xC	a
rozhřešení	rozhřešení	k1gNnSc1	rozhřešení
<g/>
,	,	kIx,	,
krumpáč	krumpáč	k1gInSc1	krumpáč
z	z	k7c2	z
der	drát	k5eAaImRp2nS	drát
Krumm	Krumm	k1gInSc1	Krumm
a	a	k8xC	a
kopáč	kopáč	k1gInSc1	kopáč
<g/>
,	,	kIx,	,
hřbitov	hřbitov	k1gInSc1	hřbitov
z	z	k7c2	z
břitov	břitovo	k1gNnPc2	břitovo
a	a	k8xC	a
hrob	hrob	k1gInSc1	hrob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
tabu	tabu	k2eAgInPc1d1	tabu
–	–	k?	–
názvy	název	k1gInPc1	název
označující	označující	k2eAgInPc1d1	označující
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
nebo	nebo	k8xC	nebo
mocné	mocný	k2eAgInPc1d1	mocný
jevy	jev	k1gInPc1	jev
se	se	k3xPyFc4	se
měnily	měnit	k5eAaImAgInP	měnit
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyslovování	vyslovování	k1gNnSc1	vyslovování
pravé	pravý	k2eAgFnSc2d1	pravá
podoby	podoba	k1gFnSc2	podoba
bylo	být	k5eAaImAgNnS	být
tabuizováno	tabuizovat	k5eAaBmNgNnS	tabuizovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vznikaly	vznikat	k5eAaImAgFnP	vznikat
podoby	podoba	k1gFnPc1	podoba
zkomolené	zkomolený	k2eAgFnPc1d1	zkomolená
(	(	kIx(	(
<g/>
blecha	blecha	k1gFnSc1	blecha
<g/>
,	,	kIx,	,
herdek	herdek	k0	herdek
<g/>
,	,	kIx,	,
safra	safra	k?	safra
<g/>
)	)	kIx)	)
opisné	opisný	k2eAgFnPc1d1	opisná
(	(	kIx(	(
<g/>
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
zmije	zmije	k1gFnSc1	zmije
<g/>
,	,	kIx,	,
zubatá	zubatá	k1gFnSc1	zubatá
<g/>
)	)	kIx)	)
eufemistické	eufemistický	k2eAgNnSc1d1	eufemistické
(	(	kIx(	(
<g/>
boží	boží	k2eAgMnSc1d1	boží
posel	posel	k1gMnSc1	posel
pro	pro	k7c4	pro
blesk	blesk	k1gInSc4	blesk
<g/>
,	,	kIx,	,
hostec	hostec	k1gInSc4	hostec
pro	pro	k7c4	pro
revma	revma	k1gNnSc4	revma
<g/>
,	,	kIx,	,
zlom	zlom	k1gInSc1	zlom
vaz	vaz	k1gInSc1	vaz
<g/>
)	)	kIx)	)
Další	další	k2eAgFnSc1d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
přejímání	přejímání	k1gNnSc4	přejímání
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
postupné	postupný	k2eAgNnSc4d1	postupné
přizpůsobování	přizpůsobování	k1gNnSc4	přizpůsobování
se	se	k3xPyFc4	se
hostitelskému	hostitelský	k2eAgInSc3d1	hostitelský
jazyku	jazyk	k1gInSc3	jazyk
<g/>
.	.	kIx.	.
přizpůsobení	přizpůsobení	k1gNnSc1	přizpůsobení
(	(	kIx(	(
<g/>
asimilace	asimilace	k1gFnSc1	asimilace
<g/>
)	)	kIx)	)
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
výslovnosti	výslovnost	k1gFnSc2	výslovnost
a	a	k8xC	a
tvarosloví	tvarosloví	k1gNnSc4	tvarosloví
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
významově	významově	k6eAd1	významově
podobným	podobný	k2eAgNnPc3d1	podobné
hostitelským	hostitelský	k2eAgNnPc3d1	hostitelské
slovům	slovo	k1gNnPc3	slovo
(	(	kIx(	(
<g/>
sexuální	sexuální	k2eAgNnSc1d1	sexuální
harašení	harašení	k1gNnSc1	harašení
<g/>
)	)	kIx)	)
doslovné	doslovný	k2eAgNnSc1d1	doslovné
překládání	překládání	k1gNnSc1	překládání
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
-	-	kIx~	-
kalky	kalk	k1gInPc7	kalk
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
orto-grafie	ortorafie	k1gFnSc1	orto-grafie
=	=	kIx~	=
pravo-pis	pravois	k1gFnSc1	pravo-pis
<g/>
)	)	kIx)	)
</s>
