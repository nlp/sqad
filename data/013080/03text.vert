<p>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
zelená	zelená	k1gFnSc1	zelená
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
muchomůrka	muchomůrka	k?	muchomůrka
hlíznatá	hlíznatý	k2eAgFnSc1d1	hlíznatá
(	(	kIx(	(
<g/>
Amanita	Amanita	k1gFnSc1	Amanita
phalloides	phalloides	k1gMnSc1	phalloides
E.	E.	kA	E.
M.	M.	kA	M.
Fries	Fries	k1gMnSc1	Fries
<g/>
,	,	kIx,	,
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejjedovatější	jedovatý	k2eAgFnSc4d3	nejjedovatější
a	a	k8xC	a
nejnebezpečnější	bezpečný	k2eNgFnSc4d3	nejnebezpečnější
houbu	houba	k1gFnSc4	houba
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
–	–	k?	–
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zde	zde	k6eAd1	zde
nejvíce	hodně	k6eAd3	hodně
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
otrav	otrava	k1gFnPc2	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
prudce	prudko	k6eAd1	prudko
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
první	první	k4xOgInPc1	první
příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
objevují	objevovat	k5eAaImIp3nP	objevovat
až	až	k9	až
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
jed	jed	k1gInSc1	jed
vstřebán	vstřebán	k2eAgInSc1d1	vstřebán
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
těžce	těžce	k6eAd1	těžce
zasaženy	zasažen	k2eAgInPc1d1	zasažen
důležité	důležitý	k2eAgInPc1d1	důležitý
orgány	orgán	k1gInPc1	orgán
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
játra	játra	k1gNnPc4	játra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amatérští	amatérský	k2eAgMnPc1d1	amatérský
houbaři	houbař	k1gMnPc1	houbař
ji	on	k3xPp3gFnSc4	on
mohou	moct	k5eAaImIp3nP	moct
zaměnit	zaměnit	k5eAaPmF	zaměnit
se	s	k7c7	s
žampiony	žampion	k1gInPc7	žampion
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
druhy	druh	k1gInPc7	druh
jedlých	jedlý	k2eAgFnPc2d1	jedlá
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Synonyma	synonymum	k1gNnSc2	synonymum
==	==	k?	==
</s>
</p>
<p>
<s>
Agaricus	Agaricus	k1gMnSc1	Agaricus
phalloides	phalloides	k1gMnSc1	phalloides
Vaillant	Vaillant	k1gMnSc1	Vaillant
ex	ex	k6eAd1	ex
Fries	Fries	k1gMnSc1	Fries
<g/>
,	,	kIx,	,
1821	[number]	k4	1821
</s>
</p>
<p>
<s>
Amanita	Amanit	k2eAgFnSc1d1	Amanita
viridis	viridis	k1gFnSc1	viridis
Persoon	Persoona	k1gFnPc2	Persoona
<g/>
,	,	kIx,	,
1797	[number]	k4	1797
–	–	k?	–
muchomůrka	muchomůrka	k?	muchomůrka
zelená	zelenat	k5eAaImIp3nS	zelenat
</s>
</p>
<p>
<s>
Amanitina	Amanitina	k1gFnSc1	Amanitina
phalloides	phalloides	k1gMnSc1	phalloides
(	(	kIx(	(
<g/>
Vaillant	Vaillant	k1gMnSc1	Vaillant
ex	ex	k6eAd1	ex
Fries	Fries	k1gMnSc1	Fries
<g/>
,	,	kIx,	,
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
E.	E.	kA	E.
<g/>
-	-	kIx~	-
<g/>
J.	J.	kA	J.
Gilbert	Gilbert	k1gMnSc1	Gilbert
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
</s>
</p>
<p>
<s>
Fungus	Fungus	k1gMnSc1	Fungus
phalloides	phalloides	k1gMnSc1	phalloides
Vaillant	Vaillant	k1gMnSc1	Vaillant
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Amanita	Amanita	k1gFnSc1	Amanita
phalloides	phalloides	k1gInSc1	phalloides
var.	var.	k?	var.
alba	alba	k1gFnSc1	alba
(	(	kIx(	(
<g/>
Vittadini	Vittadin	k2eAgMnPc1d1	Vittadin
<g/>
)	)	kIx)	)
E.	E.	kA	E.
<g/>
-	-	kIx~	-
<g/>
J.	J.	kA	J.
Gilbert	Gilbert	k1gMnSc1	Gilbert
–	–	k?	–
muchomůrka	muchomůrka	k?	muchomůrka
bílá	bílý	k2eAgNnPc4d1	bílé
</s>
</p>
<p>
<s>
Amanita	Amanita	k1gFnSc1	Amanita
phalloides	phalloidesa	k1gFnPc2	phalloidesa
var.	var.	k?	var.
umbrina	umbrina	k1gMnSc1	umbrina
(	(	kIx(	(
<g/>
Ferry	Ferro	k1gNnPc7	Ferro
<g/>
)	)	kIx)	)
Maire	Mair	k1gMnSc5	Mair
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Makroskopický	makroskopický	k2eAgMnSc1d1	makroskopický
===	===	k?	===
</s>
</p>
<p>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
cm	cm	kA	cm
je	být	k5eAaImIp3nS	být
zprvu	zprvu	k6eAd1	zprvu
sklenutý	sklenutý	k2eAgInSc1d1	sklenutý
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
ploše	plocha	k1gFnSc3	plocha
rozložený	rozložený	k2eAgInSc4d1	rozložený
<g/>
,	,	kIx,	,
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
,	,	kIx,	,
lesklý	lesklý	k2eAgInSc4d1	lesklý
<g/>
,	,	kIx,	,
radiálně	radiálně	k6eAd1	radiálně
vláknitý	vláknitý	k2eAgInSc1d1	vláknitý
<g/>
,	,	kIx,	,
tence	tenko	k6eAd1	tenko
a	a	k8xC	a
měkce	měkko	k6eAd1	měkko
masitý	masitý	k2eAgInSc1d1	masitý
<g/>
.	.	kIx.	.
</s>
<s>
Zabarvení	zabarvení	k1gNnSc1	zabarvení
klobouku	klobouk	k1gInSc2	klobouk
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
<g/>
,	,	kIx,	,
od	od	k7c2	od
bíložlutého	bíložlutý	k2eAgInSc2d1	bíložlutý
přes	přes	k7c4	přes
žlutozelené	žlutozelený	k2eAgNnSc4d1	žlutozelené
až	až	k9	až
po	po	k7c6	po
zelenohnědé	zelenohnědý	k2eAgFnSc6d1	zelenohnědá
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
plachetky	plachetka	k1gFnSc2	plachetka
na	na	k7c6	na
klobouku	klobouk	k1gInSc6	klobouk
většinou	většinou	k6eAd1	většinou
nezůstávají	zůstávat	k5eNaImIp3nP	zůstávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třeň	třeň	k1gInSc1	třeň
je	být	k5eAaImIp3nS	být
dole	dole	k6eAd1	dole
kyjovitě	kyjovitě	k6eAd1	kyjovitě
ztlustlý	ztlustlý	k2eAgMnSc1d1	ztlustlý
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
cm	cm	kA	cm
vysoký	vysoký	k2eAgMnSc1d1	vysoký
a	a	k8xC	a
0,8	[number]	k4	0,8
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
cm	cm	kA	cm
široký	široký	k2eAgInSc4d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pevný	pevný	k2eAgInSc4d1	pevný
<g/>
,	,	kIx,	,
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
,	,	kIx,	,
až	až	k9	až
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
hub	houba	k1gFnPc2	houba
dutý	dutý	k2eAgInSc1d1	dutý
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
s	s	k7c7	s
málo	málo	k6eAd1	málo
zřetelnými	zřetelný	k2eAgFnPc7d1	zřetelná
zelenými	zelený	k2eAgFnPc7d1	zelená
šupinami	šupina	k1gFnPc7	šupina
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
vajíčka	vajíčko	k1gNnSc2	vajíčko
(	(	kIx(	(
<g/>
cípatě	cípatě	k6eAd1	cípatě
roztrhaná	roztrhaný	k2eAgFnSc1d1	roztrhaná
velmi	velmi	k6eAd1	velmi
tenká	tenký	k2eAgFnSc1d1	tenká
pochva	pochva	k1gFnSc1	pochva
někdy	někdy	k6eAd1	někdy
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
kalich	kalich	k1gInSc1	kalich
smrti	smrt	k1gFnSc3	smrt
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
má	mít	k5eAaImIp3nS	mít
bílý	bílý	k2eAgInSc1d1	bílý
visutý	visutý	k2eAgInSc1d1	visutý
prstenec	prstenec	k1gInSc1	prstenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lupeny	lupen	k1gInPc1	lupen
jsou	být	k5eAaImIp3nP	být
0,8	[number]	k4	0,8
<g/>
–	–	k?	–
<g/>
1,2	[number]	k4	1,2
cm	cm	kA	cm
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
bělavé	bělavý	k2eAgInPc1d1	bělavý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dužnina	dužnina	k1gFnSc1	dužnina
je	být	k5eAaImIp3nS	být
také	také	k9	také
bílá	bílý	k2eAgFnSc1d1	bílá
i	i	k9	i
když	když	k8xS	když
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
pokožkou	pokožka	k1gFnSc7	pokožka
klobouku	klobouk	k1gInSc2	klobouk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
žlutozelená	žlutozelený	k2eAgFnSc1d1	žlutozelená
až	až	k6eAd1	až
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
výraznou	výrazný	k2eAgFnSc4d1	výrazná
vůni	vůně	k1gFnSc4	vůně
ani	ani	k8xC	ani
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
až	až	k9	až
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
hub	houba	k1gFnPc2	houba
je	být	k5eAaImIp3nS	být
vůně	vůně	k1gFnSc1	vůně
nasládlá	nasládlý	k2eAgFnSc1d1	nasládlá
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
syrové	syrový	k2eAgFnPc4d1	syrová
brambory	brambora	k1gFnPc4	brambora
až	až	k8xS	až
med	med	k1gInSc4	med
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mikroskopický	mikroskopický	k2eAgMnSc1d1	mikroskopický
===	===	k?	===
</s>
</p>
<p>
<s>
Výtrusy	výtrus	k1gInPc1	výtrus
jsou	být	k5eAaImIp3nP	být
mírně	mírně	k6eAd1	mírně
elipsoidní	elipsoidní	k2eAgFnPc1d1	elipsoidní
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
×	×	k?	×
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
μ	μ	k?	μ
<g/>
,	,	kIx,	,
hladké	hladký	k2eAgNnSc1d1	hladké
s	s	k7c7	s
tukovou	tukový	k2eAgFnSc7d1	tuková
kapkou	kapka	k1gFnSc7	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Výtrusný	výtrusný	k2eAgInSc1d1	výtrusný
prach	prach	k1gInSc1	prach
má	mít	k5eAaImIp3nS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výskytu	výskyt	k1gInSc2	výskyt
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
celý	celý	k2eAgInSc4d1	celý
mírný	mírný	k2eAgInSc4d1	mírný
pás	pás	k1gInSc4	pás
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
sever	sever	k1gInSc4	sever
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Zavlečena	zavlečen	k2eAgFnSc1d1	zavlečena
však	však	k9	však
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
se	s	k7c7	s
sazenicemi	sazenice	k1gFnPc7	sazenice
stromků	stromek	k1gInPc2	stromek
<g/>
)	)	kIx)	)
i	i	k9	i
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
San	San	k1gFnSc2	San
Franciska	Francisko	k1gNnSc2	Francisko
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
,	,	kIx,	,
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Newarku	Newark	k1gInSc2	Newark
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
byl	být	k5eAaImAgInS	být
hlášen	hlásit	k5eAaImNgInS	hlásit
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Canberry	Canberra	k1gFnSc2	Canberra
a	a	k8xC	a
Melbourne	Melbourne	k1gNnSc2	Melbourne
a	a	k8xC	a
na	na	k7c6	na
několika	několik	k4yIc6	několik
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
zelená	zelená	k1gFnSc1	zelená
preferuje	preferovat	k5eAaImIp3nS	preferovat
zejména	zejména	k9	zejména
teplejší	teplý	k2eAgFnPc4d2	teplejší
oblasti	oblast	k1gFnPc4	oblast
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
roste	růst	k5eAaImIp3nS	růst
především	především	k9	především
v	v	k7c6	v
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Doprovází	doprovázet	k5eAaImIp3nP	doprovázet
především	především	k9	především
duby	dub	k1gInPc1	dub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
habry	habr	k1gInPc1	habr
a	a	k8xC	a
buky	buk	k1gInPc1	buk
<g/>
.	.	kIx.	.
</s>
<s>
Vzácněji	vzácně	k6eAd2	vzácně
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
v	v	k7c6	v
borových	borový	k2eAgInPc6d1	borový
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
zelená	zelená	k1gFnSc1	zelená
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
jedovatých	jedovatý	k2eAgInPc2d1	jedovatý
peptidických	peptidický	k2eAgInPc2d1	peptidický
alkaloidů	alkaloid	k1gInPc2	alkaloid
zejména	zejména	k9	zejména
falotoxinů	falotoxin	k1gInPc2	falotoxin
a	a	k8xC	a
amatotoxinů	amatotoxin	k1gInPc2	amatotoxin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jedovatost	jedovatost	k1gFnSc4	jedovatost
a	a	k8xC	a
otravy	otrava	k1gFnPc4	otrava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přítomné	přítomný	k2eAgInPc1d1	přítomný
jedy	jed	k1gInPc1	jed
===	===	k?	===
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
plodnice	plodnice	k1gFnSc1	plodnice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
smrtící	smrtící	k2eAgInSc4d1	smrtící
koktejl	koktejl	k1gInSc4	koktejl
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
jedů	jed	k1gInPc2	jed
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
falotoxiny	falotoxina	k1gFnPc4	falotoxina
a	a	k8xC	a
amatoxiny	amatoxina	k1gFnPc4	amatoxina
<g/>
.	.	kIx.	.
</s>
<s>
Sto	sto	k4xCgNnSc1	sto
gramů	gram	k1gInPc2	gram
syrové	syrový	k2eAgNnSc1d1	syrové
muchomůrky	muchomůrky	k?	muchomůrky
zelené	zelené	k1gNnSc1	zelené
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
10	[number]	k4	10
mg	mg	kA	mg
faloidinu	faloidin	k1gInSc2	faloidin
<g/>
,	,	kIx,	,
8	[number]	k4	8
mg	mg	kA	mg
α	α	k?	α
a	a	k8xC	a
5	[number]	k4	5
mg	mg	kA	mg
β	β	k?	β
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
toxikologického	toxikologický	k2eAgInSc2d1	toxikologický
jsou	být	k5eAaImIp3nP	být
významnější	významný	k2eAgFnPc1d2	významnější
amatoxiny	amatoxina	k1gFnPc1	amatoxina
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
falotoxiny	falotoxin	k1gInPc1	falotoxin
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
odbourávají	odbourávat	k5eAaImIp3nP	odbourávat
v	v	k7c6	v
trávicím	trávicí	k2eAgInSc6d1	trávicí
traktu	trakt	k1gInSc6	trakt
působením	působení	k1gNnSc7	působení
žaludečních	žaludeční	k2eAgNnPc2d1	žaludeční
šťav	šťavum	k1gNnPc2	šťavum
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nevstřebávají	vstřebávat	k5eNaImIp3nP	vstřebávat
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
smrtelné	smrtelný	k2eAgFnSc3d1	smrtelná
otravě	otrava	k1gFnSc3	otrava
zdravého	zdravý	k2eAgMnSc2d1	zdravý
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
kolem	kolem	k7c2	kolem
60	[number]	k4	60
kg	kg	kA	kg
stačí	stačit	k5eAaBmIp3nS	stačit
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
g	g	kA	g
houby	houba	k1gFnSc2	houba
(	(	kIx(	(
<g/>
váženo	vážit	k5eAaImNgNnS	vážit
v	v	k7c6	v
syrovém	syrový	k2eAgInSc6d1	syrový
stavu	stav	k1gInSc6	stav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jedna	jeden	k4xCgFnSc1	jeden
plodnice	plodnice	k1gFnSc1	plodnice
váží	vážit	k5eAaImIp3nS	vážit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
mezi	mezi	k7c7	mezi
30	[number]	k4	30
až	až	k8xS	až
40	[number]	k4	40
gramy	gram	k1gInPc7	gram
(	(	kIx(	(
<g/>
některá	některý	k3yIgFnSc1	některý
literatura	literatura	k1gFnSc1	literatura
ovšem	ovšem	k9	ovšem
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnPc4d2	menší
dávky	dávka	k1gFnPc4	dávka
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vycházíme	vycházet	k5eAaImIp1nP	vycházet
<g/>
-li	i	k?	-li
z	z	k7c2	z
hodnoty	hodnota	k1gFnSc2	hodnota
LD50	LD50	k1gFnSc2	LD50
=	=	kIx~	=
0,1	[number]	k4	0,1
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
amanitinů	amanitin	k1gMnPc2	amanitin
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
pro	pro	k7c4	pro
otravu	otrava	k1gFnSc4	otrava
s	s	k7c7	s
padesátiprocentní	padesátiprocentní	k2eAgFnSc7d1	padesátiprocentní
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
smrtelnou	smrtelný	k2eAgFnSc7d1	smrtelná
požití	požití	k1gNnSc4	požití
0,75	[number]	k4	0,75
g	g	kA	g
syrové	syrový	k2eAgFnSc2d1	syrová
houby	houba	k1gFnSc2	houba
na	na	k7c4	na
kilogram	kilogram	k1gInSc4	kilogram
živé	živý	k2eAgFnSc2d1	živá
váhy	váha	k1gFnSc2	váha
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
amanitin	amanitin	k1gInSc1	amanitin
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jedů	jed	k1gInPc2	jed
muchomůrky	muchomůrky	k?	muchomůrky
zelené	zelená	k1gFnPc4	zelená
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
<g/>
,	,	kIx,	,
že	že	k8xS	že
množství	množství	k1gNnSc1	množství
jedu	jed	k1gInSc2	jed
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
bychom	by	kYmCp1nP	by
nabrali	nabrat	k5eAaPmAgMnP	nabrat
na	na	k7c4	na
špičku	špička	k1gFnSc4	špička
nože	nůž	k1gInSc2	nůž
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
asi	asi	k9	asi
0,5	[number]	k4	0,5
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
zabilo	zabít	k5eAaPmAgNnS	zabít
100	[number]	k4	100
000	[number]	k4	000
myší	myš	k1gFnPc2	myš
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
by	by	kYmCp3nP	by
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
řadu	řada	k1gFnSc4	řada
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
18	[number]	k4	18
km	km	kA	km
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
níž	jenž	k3xRgFnSc2	jenž
bychom	by	kYmCp1nP	by
vykračovali	vykračovat	k5eAaImAgMnP	vykračovat
průměrně	průměrně	k6eAd1	průměrně
4,5	[number]	k4	4,5
hodiny	hodina	k1gFnSc2	hodina
vojenským	vojenský	k2eAgInSc7d1	vojenský
krokem	krok	k1gInSc7	krok
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
cituje	citovat	k5eAaBmIp3nS	citovat
také	také	k9	také
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hře	hra	k1gFnSc6	hra
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otrava	otrava	k1gMnSc1	otrava
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pozření	pozření	k1gNnSc6	pozření
muchomůrky	muchomůrky	k?	muchomůrky
zelené	zelená	k1gFnSc2	zelená
se	se	k3xPyFc4	se
amatoxiny	amatoxina	k1gFnPc1	amatoxina
rychle	rychle	k6eAd1	rychle
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
z	z	k7c2	z
trávícího	trávící	k2eAgInSc2d1	trávící
traktu	trakt	k1gInSc2	trakt
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
pronikají	pronikat	k5eAaImIp3nP	pronikat
do	do	k7c2	do
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgInPc1d1	počáteční
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
však	však	k9	však
projeví	projevit	k5eAaPmIp3nS	projevit
až	až	k9	až
po	po	k7c6	po
poškození	poškození	k1gNnSc6	poškození
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
jaterních	jaterní	k2eAgFnPc2d1	jaterní
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
hepatocytů	hepatocyt	k1gInPc2	hepatocyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
8	[number]	k4	8
až	až	k9	až
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
má	mít	k5eAaImIp3nS	mít
postižený	postižený	k1gMnSc1	postižený
celkové	celkový	k2eAgFnSc2d1	celková
potíže	potíž	k1gFnSc2	potíž
<g/>
;	;	kIx,	;
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
únavu	únava	k1gFnSc4	únava
<g/>
,	,	kIx,	,
žaludeční	žaludeční	k2eAgFnSc4d1	žaludeční
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
závratě	závrať	k1gFnPc4	závrať
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc4	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc4	pocit
chladu	chlad	k1gInSc2	chlad
až	až	k8xS	až
mrazení	mrazení	k1gNnSc2	mrazení
<g/>
.	.	kIx.	.
</s>
<s>
Nevolnost	nevolnost	k1gFnSc1	nevolnost
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stupňuje	stupňovat	k5eAaImIp3nS	stupňovat
<g/>
,	,	kIx,	,
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
bolesti	bolest	k1gFnPc1	bolest
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
silným	silný	k2eAgNnSc7d1	silné
dávením	dávení	k1gNnSc7	dávení
a	a	k8xC	a
vodovitými	vodovitý	k2eAgInPc7d1	vodovitý
průjmy	průjem	k1gInPc7	průjem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
dehydrataci	dehydratace	k1gFnSc3	dehydratace
organizmu	organizmus	k1gInSc2	organizmus
až	až	k8xS	až
oběhovému	oběhový	k2eAgNnSc3d1	oběhové
selhání	selhání	k1gNnSc3	selhání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
zejména	zejména	k9	zejména
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pacient	pacient	k1gMnSc1	pacient
tuto	tento	k3xDgFnSc4	tento
fázi	fáze	k1gFnSc4	fáze
přežije	přežít	k5eAaPmIp3nS	přežít
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
den	den	k1gInSc4	den
otravy	otrava	k1gFnSc2	otrava
<g/>
)	)	kIx)	)
ke	k	k7c3	k
zdánlivému	zdánlivý	k2eAgNnSc3d1	zdánlivé
zlepšení	zlepšení	k1gNnSc3	zlepšení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
skončí	skončit	k5eAaPmIp3nP	skončit
zvracení	zvracení	k1gNnSc3	zvracení
i	i	k8xC	i
průjmy	průjem	k1gInPc7	průjem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
selhání	selhání	k1gNnSc3	selhání
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
silné	silný	k2eAgFnSc2d1	silná
otravy	otrava	k1gFnSc2	otrava
přestanou	přestat	k5eAaPmIp3nP	přestat
pracovat	pracovat	k5eAaImF	pracovat
játra	játra	k1gNnPc4	játra
úplně	úplně	k6eAd1	úplně
a	a	k8xC	a
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
celková	celkový	k2eAgFnSc1d1	celková
apatie	apatie	k1gFnSc1	apatie
<g/>
,	,	kIx,	,
přecházející	přecházející	k2eAgFnSc1d1	přecházející
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
tachykardie	tachykardie	k1gFnSc1	tachykardie
<g/>
,	,	kIx,	,
pokles	pokles	k1gInSc1	pokles
tlaku	tlak	k1gInSc2	tlak
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc2	rozšíření
očních	oční	k2eAgFnPc2d1	oční
zornic	zornice	k1gFnPc2	zornice
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
obvykle	obvykle	k6eAd1	obvykle
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
12	[number]	k4	12
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
otravy	otrava	k1gFnSc2	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Otrava	otrava	k1gFnSc1	otrava
je	být	k5eAaImIp3nS	být
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
ve	v	k7c6	v
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Léčba	léčba	k1gFnSc1	léčba
===	===	k?	===
</s>
</p>
<p>
<s>
Úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
léčby	léčba	k1gFnSc2	léčba
a	a	k8xC	a
šance	šance	k1gFnSc1	šance
postiženého	postižený	k1gMnSc2	postižený
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
požité	požitý	k2eAgFnSc2d1	požitá
muchomůrky	muchomůrky	k?	muchomůrky
a	a	k8xC	a
včasnosti	včasnost	k1gFnSc2	včasnost
lékařského	lékařský	k2eAgInSc2d1	lékařský
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Nutný	nutný	k2eAgMnSc1d1	nutný
je	být	k5eAaImIp3nS	být
okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
převoz	převoz	k1gInSc1	převoz
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
a	a	k8xC	a
opakovaný	opakovaný	k2eAgInSc4d1	opakovaný
výplach	výplach	k1gInSc4	výplach
trávicí	trávicí	k2eAgFnSc2d1	trávicí
trubice	trubice	k1gFnSc2	trubice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
maximálně	maximálně	k6eAd1	maximálně
omezilo	omezit	k5eAaPmAgNnS	omezit
další	další	k2eAgNnSc4d1	další
vstřebávání	vstřebávání	k1gNnSc4	vstřebávání
<g/>
.	.	kIx.	.
</s>
<s>
Ideálním	ideální	k2eAgMnSc7d1	ideální
antidotem	antidot	k1gMnSc7	antidot
je	být	k5eAaImIp3nS	být
silibinin	silibinin	k2eAgMnSc1d1	silibinin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
blokuje	blokovat	k5eAaImIp3nS	blokovat
transport	transport	k1gInSc4	transport
amanitinu	amanitin	k1gInSc2	amanitin
do	do	k7c2	do
hepatocytu	hepatocyt	k1gInSc2	hepatocyt
a	a	k8xC	a
regeneruje	regenerovat	k5eAaBmIp3nS	regenerovat
RNA	RNA	kA	RNA
polymerázu	polymeráza	k1gFnSc4	polymeráza
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
léčbou	léčba	k1gFnSc7	léčba
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
začít	začít	k5eAaPmF	začít
co	co	k3yRnSc4	co
nejdříve	dříve	k6eAd3	dříve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
silibinin	silibinin	k2eAgInSc1d1	silibinin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
podat	podat	k5eAaPmF	podat
N-acetylcystein	Ncetylcystein	k1gInSc4	N-acetylcystein
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podávání	podávání	k1gNnSc2	podávání
vysokých	vysoký	k2eAgFnPc2d1	vysoká
dávek	dávka	k1gFnPc2	dávka
penicilinu	penicilin	k1gInSc2	penicilin
G	G	kA	G
se	se	k3xPyFc4	se
již	již	k6eAd1	již
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
brzkém	brzký	k2eAgNnSc6d1	brzké
zachycení	zachycení	k1gNnSc6	zachycení
otravy	otrava	k1gFnSc2	otrava
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
odstranění	odstranění	k1gNnSc4	odstranění
toxinů	toxin	k1gInPc2	toxin
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
včasném	včasný	k2eAgInSc6d1	včasný
zásahu	zásah	k1gInSc6	zásah
existuje	existovat	k5eAaImIp3nS	existovat
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
plné	plný	k2eAgNnSc4d1	plné
vyléčení	vyléčení	k1gNnSc4	vyléčení
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
-li	i	k?	-li
k	k	k7c3	k
plnému	plný	k2eAgNnSc3d1	plné
rozvinutí	rozvinutí	k1gNnSc3	rozvinutí
těžké	těžký	k2eAgFnSc2d1	těžká
otravy	otrava	k1gFnSc2	otrava
<g/>
,	,	kIx,	,
končí	končit	k5eAaImIp3nS	končit
pacient	pacient	k1gMnSc1	pacient
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
minimálně	minimálně	k6eAd1	minimálně
s	s	k7c7	s
těžkými	těžký	k2eAgInPc7d1	těžký
doživotními	doživotní	k2eAgInPc7d1	doživotní
následky	následek	k1gInPc7	následek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
lékař	lékař	k1gMnSc1	lékař
Pierre	Pierr	k1gInSc5	Pierr
Bastien	Bastien	k1gInSc4	Bastien
třikrát	třikrát	k6eAd1	třikrát
dobrovolně	dobrovolně	k6eAd1	dobrovolně
požil	požít	k5eAaPmAgInS	požít
muchomůrku	muchomůrku	k?	muchomůrku
zelenou	zelená	k1gFnSc4	zelená
v	v	k7c6	v
prokazatelně	prokazatelně	k6eAd1	prokazatelně
smrtelné	smrtelný	k2eAgFnSc3d1	smrtelná
dávce	dávka	k1gFnSc3	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Otravu	otrava	k1gFnSc4	otrava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
však	však	k9	však
díky	díky	k7c3	díky
léčbě	léčba	k1gFnSc3	léčba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sám	sám	k3xTgMnSc1	sám
sestavil	sestavit	k5eAaPmAgInS	sestavit
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
běžně	běžně	k6eAd1	běžně
a	a	k8xC	a
volně	volně	k6eAd1	volně
dostupných	dostupný	k2eAgInPc2d1	dostupný
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
pokaždé	pokaždé	k6eAd1	pokaždé
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejčastější	častý	k2eAgFnPc1d3	nejčastější
záměny	záměna	k1gFnPc1	záměna
==	==	k?	==
</s>
</p>
<p>
<s>
Dospělou	dospělý	k2eAgFnSc4d1	dospělá
muchomůrku	muchomůrku	k?	muchomůrku
zelenou	zelená	k1gFnSc4	zelená
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
snadno	snadno	k6eAd1	snadno
zaměnit	zaměnit	k5eAaPmF	zaměnit
se	s	k7c7	s
žampionem	žampion	k1gInSc7	žampion
<g/>
,	,	kIx,	,
holubinkou	holubinka	k1gFnSc7	holubinka
<g/>
,	,	kIx,	,
bedlou	bedla	k1gFnSc7	bedla
<g/>
,	,	kIx,	,
zelánkou	zelánka	k1gFnSc7	zelánka
či	či	k8xC	či
masákem-albínem	masákemlbín	k1gInSc7	masákem-albín
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
plodnice	plodnice	k1gFnPc1	plodnice
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
pýchavce	pýchavka	k1gFnSc3	pýchavka
či	či	k8xC	či
prášivce	prášivka	k1gFnSc3	prášivka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
žampionu	žampion	k1gInSc2	žampion
lze	lze	k6eAd1	lze
muchomůrku	muchomůrku	k?	muchomůrku
zelenou	zelená	k1gFnSc4	zelená
odlišit	odlišit	k5eAaPmF	odlišit
barvou	barva	k1gFnSc7	barva
lupenů	lupen	k1gInPc2	lupen
–	–	k?	–
u	u	k7c2	u
žampionu	žampion	k1gInSc2	žampion
jsou	být	k5eAaImIp3nP	být
lupeny	lupen	k1gInPc1	lupen
světle	světle	k6eAd1	světle
až	až	k6eAd1	až
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
,	,	kIx,	,
u	u	k7c2	u
muchomůrky	muchomůrky	k?	muchomůrky
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
odlišným	odlišný	k2eAgInSc7d1	odlišný
znakem	znak	k1gInSc7	znak
muchomůrky	muchomůrky	k?	muchomůrky
zelené	zelená	k1gFnSc2	zelená
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
kalich	kalich	k1gInSc1	kalich
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
u	u	k7c2	u
žampionu	žampion	k1gInSc2	žampion
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zelánky	zelánka	k1gFnSc2	zelánka
<g/>
,	,	kIx,	,
holubinek	holubinka	k1gFnPc2	holubinka
či	či	k8xC	či
masáka-albína	masákalbín	k1gInSc2	masáka-albín
se	se	k3xPyFc4	se
plodnice	plodnice	k1gFnSc1	plodnice
muchomůrky	muchomůrky	k?	muchomůrky
zelené	zelený	k2eAgInPc1d1	zelený
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
prsten	prsten	k1gInSc4	prsten
a	a	k8xC	a
pochvu	pochva	k1gFnSc4	pochva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
či	či	k8xC	či
návštěvníci	návštěvník	k1gMnPc1	návštěvník
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
splést	splést	k5eAaPmF	splést
muchomůrku	muchomůrku	k?	muchomůrku
zelenou	zelená	k1gFnSc4	zelená
s	s	k7c7	s
asijským	asijský	k2eAgInSc7d1	asijský
druhem	druh	k1gInSc7	druh
Volvariella	Volvariell	k1gMnSc2	Volvariell
volvacea	volvaceus	k1gMnSc2	volvaceus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
(	(	kIx(	(
<g/>
v	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
stádiích	stádium	k1gNnPc6	stádium
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dopustit	dopustit	k5eAaPmF	dopustit
záměny	záměna	k1gFnPc1	záměna
i	i	k8xC	i
zkušení	zkušený	k2eAgMnPc1d1	zkušený
odborníci	odborník	k1gMnPc1	odborník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
záměny	záměna	k1gFnPc1	záměna
jsou	být	k5eAaImIp3nP	být
údajně	údajně	k6eAd1	údajně
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
(	(	kIx(	(
<g/>
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
otrav	otrava	k1gFnPc2	otrava
houbami	houba	k1gFnPc7	houba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
jihoevropských	jihoevropský	k2eAgFnPc6d1	jihoevropská
zemích	zem	k1gFnPc6	zem
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
vajíček	vajíčko	k1gNnPc2	vajíčko
muchomůrky	muchomůrky	k?	muchomůrky
zelené	zelená	k1gFnSc2	zelená
s	s	k7c7	s
vajíčky	vajíčko	k1gNnPc7	vajíčko
jedlé	jedlý	k2eAgFnPc1d1	jedlá
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
ceněné	ceněný	k2eAgNnSc4d1	ceněné
muchomůrky	muchomůrky	k?	muchomůrky
císařské	císařský	k2eAgInPc1d1	císařský
(	(	kIx(	(
<g/>
Amanita	Amanita	k1gFnSc1	Amanita
caesarea	caesarea	k1gMnSc1	caesarea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
v	v	k7c6	v
Polabí	Polabí	k1gNnSc6	Polabí
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
přísně	přísně	k6eAd1	přísně
chráněna	chránit	k5eAaImNgFnS	chránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Otravy	otrava	k1gMnPc4	otrava
slavných	slavný	k2eAgFnPc2d1	slavná
==	==	k?	==
</s>
</p>
<p>
<s>
Otrava	otrava	k1gMnSc1	otrava
muchomůrkou	muchomůrkou	k?	muchomůrkou
zelenou	zelená	k1gFnSc4	zelená
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
několika	několik	k4yIc2	několik
historických	historický	k2eAgFnPc2d1	historická
osobností	osobnost	k1gFnPc2	osobnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jídlem	jídlo	k1gNnSc7	jídlo
připraveným	připravený	k2eAgFnPc3d1	připravená
z	z	k7c2	z
muchomůrky	muchomůrky	k?	muchomůrky
zelené	zelená	k1gFnSc2	zelená
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
záměrně	záměrně	k6eAd1	záměrně
otráven	otráven	k2eAgMnSc1d1	otráven
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Claudius	Claudius	k1gMnSc1	Claudius
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
odborníků	odborník	k1gMnPc2	odborník
však	však	k9	však
popsané	popsaný	k2eAgInPc1d1	popsaný
příznaky	příznak	k1gInPc1	příznak
otravě	otrava	k1gFnSc3	otrava
muchomůrkou	muchomůrkou	k?	muchomůrkou
příliš	příliš	k6eAd1	příliš
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
následky	následek	k1gInPc4	následek
otravy	otrava	k1gFnSc2	otrava
muchomůrkou	muchomůrkou	k?	muchomůrkou
zelenou	zelená	k1gFnSc4	zelená
údajně	údajně	k6eAd1	údajně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
však	však	k9	však
nemoc	nemoc	k1gFnSc1	nemoc
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
smrti	smrt	k1gFnSc3	smrt
trvala	trvat	k5eAaImAgFnS	trvat
téměř	téměř	k6eAd1	téměř
pět	pět	k4xCc4	pět
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
možnost	možnost	k1gFnSc1	možnost
otravy	otrava	k1gFnSc2	otrava
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
snědl	sníst	k5eAaPmAgMnS	sníst
jídlo	jídlo	k1gNnSc4	jídlo
připravené	připravený	k2eAgFnSc2d1	připravená
z	z	k7c2	z
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Popsané	popsaný	k2eAgInPc1d1	popsaný
symptomy	symptom	k1gInPc1	symptom
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
otravě	otrava	k1gFnSc3	otrava
muchomůrkou	muchomůrkou	k?	muchomůrkou
zelenou	zelená	k1gFnSc4	zelená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otrava	otrava	k1gMnSc1	otrava
muchomůrkou	muchomůrkou	k?	muchomůrkou
zelenou	zelená	k1gFnSc4	zelená
postihla	postihnout	k5eAaPmAgFnS	postihnout
celou	celý	k2eAgFnSc4d1	celá
rodinu	rodina	k1gFnSc4	rodina
českého	český	k2eAgMnSc2d1	český
katolického	katolický	k2eAgMnSc2d1	katolický
básníka	básník	k1gMnSc2	básník
Jana	Jan	k1gMnSc2	Jan
Zahradníčka	Zahradníček	k1gMnSc2	Zahradníček
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
jeho	jeho	k3xOp3gFnPc1	jeho
dcery	dcera	k1gFnPc1	dcera
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
zemřely	zemřít	k5eAaPmAgFnP	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
poznámka	poznámka	k1gFnSc1	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Amanitina	Amanitin	k1gMnSc2	Amanitin
E.	E.	kA	E.
<g/>
-	-	kIx~	-
<g/>
J.	J.	kA	J.
Gilbert	Gilbert	k1gMnSc1	Gilbert
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
(	(	kIx(	(
<g/>
muchomůrečka	muchomůrečka	k1gFnSc1	muchomůrečka
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
dnešními	dnešní	k2eAgInPc7d1	dnešní
mykology	mykolog	k1gMnPc4	mykolog
uznáván	uznáván	k2eAgInSc4d1	uznáván
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
se	se	k3xPyFc4	se
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Amanita	Amanit	k2eAgFnSc1d1	Amanita
Persoon	Persoon	k1gInSc1	Persoon
<g/>
,	,	kIx,	,
1797	[number]	k4	1797
(	(	kIx(	(
<g/>
muchomůrka	muchomůrka	k?	muchomůrka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
Muchomůrka	Muchomůrka	k?	Muchomůrka
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
muchomůrkovitých	muchomůrkovitý	k2eAgFnPc2d1	muchomůrkovitý
–	–	k?	–
Amanitaceae	Amanitaceae	k1gFnPc2	Amanitaceae
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
molekulárních	molekulární	k2eAgInPc2d1	molekulární
výzkumů	výzkum	k1gInPc2	výzkum
někteří	některý	k3yIgMnPc1	některý
současní	současný	k2eAgMnPc1d1	současný
taxonomové	taxonom	k1gMnPc1	taxonom
řadí	řadit	k5eAaImIp3nP	řadit
celý	celý	k2eAgInSc4d1	celý
rod	rod	k1gInSc4	rod
Amanita	Amanita	k1gFnSc1	Amanita
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
Plutaceae	Plutaceae	k1gNnSc2	Plutaceae
Kotlaba	Kotlaba	k1gMnSc1	Kotlaba
&	&	k?	&
Pouzar	Pouzar	k1gMnSc1	Pouzar
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
(	(	kIx(	(
<g/>
štítovkovité	štítovkovitý	k2eAgFnSc2d1	štítovkovitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
čeledí	čeleď	k1gFnSc7	čeleď
Amanitaceae	Amanitaceae	k1gNnSc2	Amanitaceae
R.	R.	kA	R.
Heim	Heim	k1gMnSc1	Heim
ex	ex	k6eAd1	ex
Pouzar	Pouzar	k1gMnSc1	Pouzar
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
(	(	kIx(	(
<g/>
muchomůrkovité	muchomůrkovitý	k2eAgFnSc2d1	muchomůrkovitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kotlaba	Kotlaba	k1gMnSc1	Kotlaba
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pouzar	Pouzar	k1gMnSc1	Pouzar
<g/>
:	:	kIx,	:
Přehled	přehled	k1gInSc1	přehled
československých	československý	k2eAgFnPc2d1	Československá
hub	houba	k1gFnPc2	houba
(	(	kIx(	(
<g/>
s.	s.	k?	s.
238	[number]	k4	238
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Erhart	Erharta	k1gFnPc2	Erharta
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Erhartová	Erhartová	k1gFnSc1	Erhartová
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Příhoda	Příhoda	k1gMnSc1	Příhoda
<g/>
:	:	kIx,	:
Houby	houby	k6eAd1	houby
ve	v	k7c6	v
fotografii	fotografia	k1gFnSc6	fotografia
(	(	kIx(	(
<g/>
s.	s.	k?	s.
98	[number]	k4	98
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kubička	Kubička	k1gMnSc1	Kubička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Erhart	Erhart	k1gInSc1	Erhart
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Erhartová	Erhartová	k1gFnSc1	Erhartová
<g/>
:	:	kIx,	:
Jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
houby	houba	k1gFnPc1	houba
(	(	kIx(	(
<g/>
s.	s.	k?	s.
58	[number]	k4	58
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avicenum	Avicenum	k1gNnSc1	Avicenum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aurel	Aurel	k1gMnSc1	Aurel
Dermek	Dermek	k1gMnSc1	Dermek
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
našich	naši	k1gMnPc2	naši
húb	húb	k?	húb
(	(	kIx(	(
<g/>
s.	s.	k?	s.
240	[number]	k4	240
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Smotlacha	smotlacha	k1gMnSc1	smotlacha
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
tržních	tržní	k2eAgFnPc2d1	tržní
a	a	k8xC	a
jedovatých	jedovatý	k2eAgFnPc2d1	jedovatá
hub	houba	k1gFnPc2	houba
(	(	kIx(	(
<g/>
Ss	Ss	k1gFnSc1	Ss
<g/>
.	.	kIx.	.
196	[number]	k4	196
<g/>
–	–	k?	–
<g/>
197	[number]	k4	197
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Jiří	Jiří	k1gMnSc1	Jiří
Malý	Malý	k1gMnSc1	Malý
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
uprav	upravit	k5eAaPmRp2nS	upravit
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
bílá	bílý	k2eAgFnSc1d1	bílá
</s>
</p>
<p>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
jízlivá	jízlivý	k2eAgFnSc1d1	jízlivá
</s>
</p>
<p>
<s>
Otrava	otrava	k1gFnSc1	otrava
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Bastien	Bastien	k1gInSc4	Bastien
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
muchomůrka	muchomůrka	k?	muchomůrka
zelená	zelenat	k5eAaImIp3nS	zelenat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
zelená	zelenat	k5eAaImIp3nS	zelenat
na	na	k7c4	na
biolibu	bioliba	k1gFnSc4	bioliba
</s>
</p>
<p>
<s>
Internetový	internetový	k2eAgInSc4d1	internetový
atlas	atlas	k1gInSc4	atlas
hub	houba	k1gFnPc2	houba
</s>
</p>
<p>
<s>
Otrava	otrava	k1gMnSc1	otrava
muchomůrkou	muchomůrkou	k?	muchomůrkou
zelenou	zelená	k1gFnSc4	zelená
na	na	k7c4	na
www.biotox.cz	www.biotox.cz	k1gInSc4	www.biotox.cz
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
doporučený	doporučený	k2eAgInSc1d1	doporučený
postup	postup	k1gInSc1	postup
diagnostiky	diagnostika	k1gFnSc2	diagnostika
a	a	k8xC	a
léčby	léčba	k1gFnSc2	léčba
intoxikace	intoxikace	k1gFnSc2	intoxikace
houbou	houba	k1gFnSc7	houba
"	"	kIx"	"
<g/>
Amanita	Amanita	k1gFnSc1	Amanita
phalloides	phalloides	k1gMnSc1	phalloides
<g/>
"	"	kIx"	"
–	–	k?	–
neplatný	platný	k2eNgInSc4d1	neplatný
odkaz	odkaz	k1gInSc4	odkaz
!	!	kIx.	!
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
jedovatým	jedovatý	k2eAgFnPc3d1	jedovatá
houbám	houba	k1gFnPc3	houba
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
toxinech	toxin	k1gInPc6	toxin
a	a	k8xC	a
otravách	otrava	k1gFnPc6	otrava
–	–	k?	–
neplatný	platný	k2eNgInSc1d1	neplatný
odkaz	odkaz	k1gInSc1	odkaz
!	!	kIx.	!
</s>
</p>
