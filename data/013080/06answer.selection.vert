<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
zelená	zelená	k1gFnSc1	zelená
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
muchomůrka	muchomůrka	k?	muchomůrka
hlíznatá	hlíznatý	k2eAgFnSc1d1	hlíznatá
(	(	kIx(	(
<g/>
Amanita	Amanita	k1gFnSc1	Amanita
phalloides	phalloides	k1gMnSc1	phalloides
E.	E.	kA	E.
M.	M.	kA	M.
Fries	Fries	k1gMnSc1	Fries
<g/>
,	,	kIx,	,
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejjedovatější	jedovatý	k2eAgFnSc4d3	nejjedovatější
a	a	k8xC	a
nejnebezpečnější	bezpečný	k2eNgFnSc4d3	nejnebezpečnější
houbu	houba	k1gFnSc4	houba
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
–	–	k?	–
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zde	zde	k6eAd1	zde
nejvíce	hodně	k6eAd3	hodně
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
otrav	otrava	k1gFnPc2	otrava
<g/>
.	.	kIx.	.
</s>
