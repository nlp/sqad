<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
vydal	vydat	k5eAaPmAgMnS	vydat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
sbírku	sbírka	k1gFnSc4	sbírka
Město	město	k1gNnSc4	město
v	v	k7c6	v
slzách	slza	k1gFnPc6	slza
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1923	[number]	k4	1923
mu	on	k3xPp3gNnSc3	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
sbírka	sbírka	k1gFnSc1	sbírka
Samá	samý	k3xTgFnSc1	samý
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
předmluvy	předmluva	k1gFnPc1	předmluva
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
sbírkám	sbírka	k1gFnPc3	sbírka
byly	být	k5eAaImAgInP	být
psány	psát	k5eAaImNgInP	psát
jménem	jméno	k1gNnSc7	jméno
Devětsilu	Devětsil	k1gInSc2	Devětsil
a	a	k8xC	a
naznačovaly	naznačovat	k5eAaImAgInP	naznačovat
posun	posun	k1gInSc4	posun
myšlení	myšlení	k1gNnSc2	myšlení
uvnitř	uvnitř	k7c2	uvnitř
tzv.	tzv.	kA	tzv.
socialistické	socialistický	k2eAgFnSc2d1	socialistická
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
proletářské	proletářský	k2eAgFnSc2d1	proletářská
poezie	poezie	k1gFnSc2	poezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
