<s>
Vladimir	Vladimir	k1gInSc1
Komarov	Komarov	k1gInSc1
</s>
<s>
Vladimir	Vladimir	k1gInSc1
Michajlovič	Michajlovič	k1gInSc1
Komarov	Komarov	k1gInSc1
Vladimir	Vladimir	k1gInSc1
Komarov	Komarov	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
<g/>
Sovětský	sovětský	k2eAgInSc1d1
kosmonaut	kosmonaut	k1gMnSc1
Státní	státní	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
</s>
<s>
SSSR	SSSR	kA
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1927	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Vjazniki	Vjazniki	k1gNnSc1
<g/>
,	,	kIx,
Vladimirská	Vladimirský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
SSSR	SSSR	kA
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1967	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
40	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
Jiné	jiná	k1gFnSc2
zaměstnání	zaměstnání	k1gNnSc2
</s>
<s>
Inženýr	inženýr	k1gMnSc1
Hodnost	hodnost	k1gFnSc1
</s>
<s>
Plukovník	plukovník	k1gMnSc1
Čas	čas	k1gInSc1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
</s>
<s>
2	#num#	k4
<g/>
d	d	k?
0	#num#	k4
<g/>
3	#num#	k4
<g/>
h	h	k?
0	#num#	k4
<g/>
4	#num#	k4
<g/>
m	m	kA
Kosmonaut	kosmonaut	k1gMnSc1
od	od	k7c2
</s>
<s>
1960	#num#	k4
Mise	mise	k1gFnSc1
</s>
<s>
Voschod	Voschod	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
Sojuz	Sojuz	k1gInSc1
1	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vladimir	Vladimir	k1gInSc1
Michajlovič	Michajlovič	k1gInSc1
Komarov	Komarov	k1gInSc4
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
В	В	k?
М	М	k?
К	К	k?
<g/>
;	;	kIx,
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1927	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1967	#num#	k4
<g/>
,	,	kIx,
Orenburská	orenburský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
v	v	k7c6
Sojuzu	Sojuz	k1gInSc6
1	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
letec	letec	k1gMnSc1
<g/>
,	,	kIx,
důstojník	důstojník	k1gMnSc1
<g/>
,	,	kIx,
tragicky	tragicky	k6eAd1
zesnulý	zesnulý	k2eAgMnSc1d1
sovětský	sovětský	k2eAgMnSc1d1
kosmonaut	kosmonaut	k1gMnSc1
ruské	ruský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
první	první	k4xOgInSc4
oficiálně	oficiálně	k6eAd1
oznámenou	oznámený	k2eAgFnSc7d1
lidskou	lidský	k2eAgFnSc7d1
obětí	oběť	k1gFnSc7
letů	let	k1gInPc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
domovníkem	domovník	k1gMnSc7
a	a	k8xC
matka	matka	k1gFnSc1
kuchařkou	kuchařka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bydleli	bydlet	k5eAaImAgMnP
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
malém	malý	k2eAgInSc6d1
suterénním	suterénní	k2eAgInSc6d1
bytě	byt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
na	na	k7c6
základní	základní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
se	se	k3xPyFc4
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
učil	učit	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válku	válek	k1gInSc2
přežil	přežít	k5eAaPmAgMnS
na	na	k7c6
vesnici	vesnice	k1gFnSc6
u	u	k7c2
příbuzných	příbuzný	k1gMnPc2
<g/>
,	,	kIx,
naučil	naučit	k5eAaPmAgMnS
se	se	k3xPyFc4
pracovat	pracovat	k5eAaImF
i	i	k9
s	s	k7c7
koňmi	kůň	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
vystudoval	vystudovat	k5eAaPmAgInS
vojenské	vojenský	k2eAgNnSc4d1
letecké	letecký	k2eAgNnSc4d1
učiliště	učiliště	k1gNnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
batajském	batajský	k2eAgNnSc6d1
leteckém	letecký	k2eAgNnSc6d1
učilišti	učiliště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloužil	sloužit	k5eAaImAgMnS
u	u	k7c2
stíhacího	stíhací	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
prvních	první	k4xOgMnPc2
zvládl	zvládnout	k5eAaPmAgMnS
lety	léto	k1gNnPc7
s	s	k7c7
prvními	první	k4xOgMnPc7
reaktivními	reaktivní	k2eAgMnPc7d1
letadly	letadlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
KSSS	KSSS	kA
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
ukončil	ukončit	k5eAaPmAgInS
s	s	k7c7
titulem	titul	k1gInSc7
leteckého	letecký	k2eAgMnSc2d1
inženýra	inženýr	k1gMnSc2
leteckou	letecký	k2eAgFnSc4d1
inženýrskou	inženýrský	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
Žukovského	Žukovského	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželku	manželka	k1gFnSc4
a	a	k8xC
syna	syn	k1gMnSc4
Žeňu	Žeňa	k1gMnSc4
odstěhoval	odstěhovat	k5eAaPmAgMnS
k	k	k7c3
otci	otec	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosmickou	kosmický	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
zahájil	zahájit	k5eAaPmAgMnS
výcvikem	výcvik	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
byl	být	k5eAaImAgMnS
dvojník	dvojník	k1gMnSc1
Popoviče	Popovič	k1gMnSc2
při	při	k7c6
letu	let	k1gInSc6
Vostoku	Vostok	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lety	let	k1gInPc1
do	do	k7c2
vesmíru	vesmír	k1gInSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
byl	být	k5eAaImAgMnS
velitelem	velitel	k1gMnSc7
prvního	první	k4xOgInSc2
letu	let	k1gInSc2
vícečlenné	vícečlenný	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
na	na	k7c4
kosmické	kosmický	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
Voschod	Voschod	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
se	se	k3xPyFc4
letu	let	k1gInSc6
zúčastnili	zúčastnit	k5eAaPmAgMnP
Konstantin	Konstantin	k1gMnSc1
Feoktistov	Feoktistov	k1gInSc1
(	(	kIx(
<g/>
vědecký	vědecký	k2eAgMnSc1d1
pracovník	pracovník	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Boris	Boris	k1gMnSc1
Jegorov	Jegorov	k1gInSc1
(	(	kIx(
<g/>
lékař	lékař	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Startovali	startovat	k5eAaBmAgMnP
z	z	k7c2
kosmodromu	kosmodrom	k1gInSc2
Bajkonur	Bajkonura	k1gFnPc2
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1964	#num#	k4
a	a	k8xC
přistáli	přistát	k5eAaPmAgMnP,k5eAaImAgMnP
s	s	k7c7
pomocí	pomoc	k1gFnSc7
padáků	padák	k1gInPc2
v	v	k7c6
kabině	kabina	k1gFnSc6
o	o	k7c4
den	den	k1gInSc4
později	pozdě	k6eAd2
nedaleko	nedaleko	k7c2
kosmodromu	kosmodrom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
zahynul	zahynout	k5eAaPmAgMnS
plukovník	plukovník	k1gMnSc1
V.	V.	kA
M.	M.	kA
Komarov	Komarov	k1gInSc1
při	při	k7c6
zkušebním	zkušební	k2eAgInSc6d1
letu	let	k1gInSc6
nové	nový	k2eAgFnSc2d1
<g/>
,	,	kIx,
vícemístné	vícemístný	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Sojuz	Sojuz	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
nouzovém	nouzový	k2eAgNnSc6d1
přistání	přistání	k1gNnSc6
s	s	k7c7
téměř	téměř	k6eAd1
neovladatelnou	ovladatelný	k2eNgFnSc7d1
kosmickou	kosmický	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
<g/>
,	,	kIx,
s	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
měl	mít	k5eAaImAgMnS
již	již	k6eAd1
během	během	k7c2
letu	let	k1gInSc2
potíže	potíž	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
zamotal	zamotat	k5eAaPmAgInS
padák	padák	k1gInSc1
a	a	k8xC
náraz	náraz	k1gInSc1
v	v	k7c6
rychlosti	rychlost	k1gFnSc6
130	#num#	k4
<g/>
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
nepřežil	přežít	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Voschod	Voschod	k1gInSc1
1	#num#	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1964	#num#	k4
-	-	kIx~
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1964	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sojuz	Sojuz	k1gInSc1
1	#num#	k4
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1967	#num#	k4
-	-	kIx~
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Po	po	k7c6
svém	svůj	k3xOyFgInSc6
prvním	první	k4xOgInSc6
letu	let	k1gInSc6
byl	být	k5eAaImAgInS
společně	společně	k6eAd1
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
členy	člen	k1gMnPc7
posádky	posádka	k1gFnSc2
vyznamenán	vyznamenán	k2eAgMnSc1d1
Leninovým	Leninův	k2eAgInSc7d1
řádem	řád	k1gInSc7
<g/>
,	,	kIx,
medailí	medaile	k1gFnPc2
Zlatá	zlatý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
a	a	k8xC
byl	být	k5eAaImAgInS
mu	on	k3xPp3gMnSc3
udělen	udělit	k5eAaPmNgInS
titul	titul	k1gInSc1
hrdiny	hrdina	k1gMnSc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Získal	získat	k5eAaPmAgInS
posmrtně	posmrtně	k6eAd1
další	další	k2eAgFnSc4d1
Zlatou	zlatý	k2eAgFnSc4d1
hvězdu	hvězda	k1gFnSc4
a	a	k8xC
byl	být	k5eAaImAgInS
pochován	pochovat	k5eAaPmNgInS
slavnostně	slavnostně	k6eAd1
na	na	k7c6
Rudém	rudý	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
u	u	k7c2
Kremelské	kremelský	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něho	on	k3xPp3gInSc2
byly	být	k5eAaImAgFnP
pojmenovány	pojmenován	k2eAgFnPc1d1
obce	obec	k1gFnPc1
<g/>
,	,	kIx,
letecké	letecký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
a	a	k8xC
útvary	útvar	k1gInPc1
<g/>
,	,	kIx,
vědecko-výzkumná	vědecko-výzkumný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
SSSR	SSSR	kA
a	a	k8xC
kráter	kráter	k1gInSc1
Komarov	Komarovo	k1gNnPc2
na	na	k7c6
odvrácené	odvrácený	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7
jménem	jméno	k1gNnSc7
byla	být	k5eAaImAgFnS
pojmenována	pojmenován	k2eAgFnSc1d1
planetka	planetka	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
pásu	pás	k1gInSc2
1836	#num#	k4
Komarov	Komarovo	k1gNnPc2
<g/>
,	,	kIx,
objevená	objevený	k2eAgFnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1971	#num#	k4
Nikolajem	Nikolaj	k1gMnSc7
Černychem	Černych	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
;	;	kIx,
LÁLA	Lála	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kosmonauti-piloti	Kosmonauti-pilot	k1gMnPc1
SSSR	SSSR	kA
<g/>
,	,	kIx,
s.	s.	k?
353	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Sojuz	Sojuz	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
Magazín	magazín	k1gInSc1
Hospodářských	hospodářský	k2eAgFnPc2d1
novin	novina	k1gFnPc2
<g/>
,	,	kIx,
29.9	29.9	k4
<g/>
.2009	.2009	k4
-	-	kIx~
část	část	k1gFnSc1
dostupná	dostupný	k2eAgFnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Časopis	časopis	k1gInSc1
SCIENCE	SCIENCE	kA
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
22	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
,	,	kIx,
ISSN	ISSN	kA
1214-4754	1214-4754	k4
-	-	kIx~
dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
Archivováno	archivován	k2eAgNnSc1d1
17	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sto	sto	k4xCgNnSc4
hvězdných	hvězdný	k2eAgMnPc2d1
kapitánů	kapitán	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Práce	práce	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Vladimír	Vladimír	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
Komarov	Komarov	k1gInSc4
<g/>
,	,	kIx,
s.	s.	k?
122	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Crater	Crater	k1gInSc1
Komarov	Komarov	k1gInSc4
on	on	k3xPp3gMnSc1
Moon	Moon	k1gMnSc1
Gazetteer	Gazetteer	k1gMnSc1
of	of	k?
Planetary	Planetara	k1gFnSc2
Nomenclature	Nomenclatur	k1gMnSc5
<g/>
,	,	kIx,
IAU	IAU	kA
<g/>
,	,	kIx,
USGS	USGS	kA
<g/>
,	,	kIx,
NASA	NASA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
JPL	JPL	kA
Small-Body	Small-Boda	k1gFnSc2
Database	Databasa	k1gFnSc3
Browser	Browser	k1gInSc1
<g/>
:	:	kIx,
1836	#num#	k4
Komarov	Komarovo	k1gNnPc2
(	(	kIx(
<g/>
1971	#num#	k4
OT	ot	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2016-05-24	2016-05-24	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vladimir	Vladimira	k1gFnPc2
Komarov	Komarovo	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Na	na	k7c6
webu	web	k1gInSc6
Space	Space	k1gFnSc2
</s>
<s>
Na	na	k7c6
webu	web	k1gInSc6
Kosmo	Kosma	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Třídílný	třídílný	k2eAgInSc1d1
článek	článek	k1gInSc1
o	o	k7c6
letu	let	k1gInSc6
lodi	loď	k1gFnSc2
Sojuz	Sojuz	k1gInSc1
1	#num#	k4
na	na	k7c6
iDnes	iDnesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
http://technet.idnes.cz/smrt-vladimir-komarov-06a-/veda.aspx?c=A120423_173557_veda_mla	http://technet.idnes.cz/smrt-vladimir-komarov-06a-/veda.aspx?c=A120423_173557_veda_mla	k6eAd1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7052	#num#	k4
9648	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
114492	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
99734223	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
114492	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
