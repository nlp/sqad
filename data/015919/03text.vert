<s>
Bund	bund	k1gInSc1
</s>
<s>
Všeobecný	všeobecný	k2eAgInSc1d1
dělnický	dělnický	k2eAgInSc1d1
židovský	židovský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
v	v	k7c6
Litvě	Litva	k1gFnSc6
<g/>
,	,	kIx,
Polsku	Polsko	k1gNnSc6
a	a	k8xC
Rusku	Rusko	k1gNnSc6
א	א	k?
<g/>
ַ	ַ	k?
<g/>
ל	ל	k?
<g/>
ַ	ַ	k?
<g/>
נ	נ	k?
י	י	k?
<g/>
ִ	ִ	k?
<g/>
ד	ד	k?
א	א	k?
<g/>
ַ	ַ	k?
<g/>
ר	ר	k?
ב	ב	k?
<g/>
ּ	ּ	k?
<g/>
ו	ו	k?
א	א	k?
ל	ל	k?
פ	פ	k?
א	א	k?
ר	ר	k?
<g/>
ַ	ַ	k?
<g/>
נ	נ	k?
Zkratka	zkratka	k1gFnSc1
</s>
<s>
Bund	bund	k1gInSc4
Datum	datum	k1gInSc1
založení	založení	k1gNnSc1
</s>
<s>
1897	#num#	k4
Datum	datum	k1gInSc1
rozpuštění	rozpuštění	k1gNnSc2
</s>
<s>
1920	#num#	k4
Předseda	předseda	k1gMnSc1
</s>
<s>
Victor	Victor	k1gMnSc1
Alter	Alter	k1gMnSc1
Ideologie	ideologie	k1gFnSc1
</s>
<s>
sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
</s>
<s>
socialismus	socialismus	k1gInSc1
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
levice	levice	k1gFnSc1
Barvy	barva	k1gFnSc2
</s>
<s>
rudá	rudý	k2eAgFnSc5d1
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1
Bundu	bund	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
</s>
<s>
Bund	bund	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
jidiš	jidiš	k1gNnSc6
<g/>
:	:	kIx,
ב	ב	k?
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
„	„	k?
<g/>
Svaz	svaz	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
Všeobecný	všeobecný	k2eAgInSc1d1
dělnický	dělnický	k2eAgInSc1d1
židovský	židovský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
v	v	k7c6
Litvě	Litva	k1gFnSc6
<g/>
,	,	kIx,
Polsku	Polsko	k1gNnSc6
a	a	k8xC
Rusku	Rusko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
jidiš	jidiš	k1gNnSc6
<g/>
:	:	kIx,
א	א	k?
<g/>
ַ	ַ	k?
<g/>
ל	ל	k?
<g/>
ַ	ַ	k?
<g/>
נ	נ	k?
י	י	k?
<g/>
ִ	ִ	k?
<g/>
ד	ד	k?
א	א	k?
<g/>
ַ	ַ	k?
<g/>
ר	ר	k?
ב	ב	k?
<g/>
ּ	ּ	k?
<g/>
ו	ו	k?
א	א	k?
ל	ל	k?
פ	פ	k?
א	א	k?
ר	ר	k?
<g/>
ַ	ַ	k?
<g/>
נ	נ	k?
<g/>
,	,	kIx,
Algemajner	Algemajner	k1gMnSc1
jidišer	jidišer	k1gMnSc1
arbeter	arbeter	k1gMnSc1
bund	bunda	k1gFnPc2
in	in	k?
Lite	lit	k1gInSc5
<g/>
,	,	kIx,
Pojlin	Pojlin	k2eAgInSc1d1
un	un	k?
Rusland	Rusland	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
židovská	židovská	k1gFnSc1
sekulární	sekulární	k2eAgFnSc1d1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Strana	strana	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1897	#num#	k4
ve	v	k7c6
Vilniusu	Vilnius	k1gInSc6
v	v	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
Ruském	ruský	k2eAgNnSc6d1
impériu	impérium	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
sjednotit	sjednotit	k5eAaPmF
všechny	všechen	k3xTgMnPc4
židovské	židovský	k2eAgMnPc4d1
dělníky	dělník	k1gMnPc4
v	v	k7c6
carském	carský	k2eAgNnSc6d1
Rusku	Rusko	k1gNnSc6
do	do	k7c2
jednotné	jednotný	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formálně	formálně	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
součást	součást	k1gFnSc4
Sociálně	sociálně	k6eAd1
demokratické	demokratický	k2eAgFnSc2d1
dělnické	dělnický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
revolučního	revoluční	k2eAgNnSc2d1
období	období	k1gNnSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1904	#num#	k4
a	a	k8xC
1906	#num#	k4
měl	mít	k5eAaImAgInS
Bund	bund	k1gInSc1
na	na	k7c4
30	#num#	k4
tisíc	tisíc	k4xCgInPc2
platících	platící	k2eAgMnPc2d1
členů	člen	k1gInPc2
a	a	k8xC
desetitisíce	desetitisíce	k1gInPc1
dalších	další	k2eAgMnPc2d1
byli	být	k5eAaImAgMnP
aktivními	aktivní	k2eAgMnPc7d1
i	i	k8xC
pasivními	pasivní	k2eAgMnPc7d1
sympatizanty	sympatizant	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Členové	člen	k1gMnPc1
Bundu	bund	k1gInSc2
se	se	k3xPyFc4
označovali	označovat	k5eAaImAgMnP
jako	jako	k9
bundisté	bundista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
aktivní	aktivní	k2eAgFnSc1d1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1920	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc4
pozůstatky	pozůstatek	k1gInPc1
zůstaly	zůstat	k5eAaPmAgInP
aktivní	aktivní	k2eAgInPc1d1
jak	jak	k8xS,k8xC
v	v	k7c6
židovské	židovský	k2eAgFnSc6d1
diaspoře	diaspora	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Příbuzná	příbuzný	k2eAgNnPc1d1
hesla	heslo	k1gNnPc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Židů	Žid	k1gMnPc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
</s>
<s>
Bolševici	bolševik	k1gMnPc1
</s>
<s>
Birobidžan	Birobidžan	k1gMnSc1
</s>
<s>
Komzet	Komzet	k5eAaImF,k5eAaPmF
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
Po	Po	kA
<g/>
'	'	kIx"
<g/>
alej	alej	k1gFnSc1
Cijon	Cijon	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
KSSS	KSSS	kA
(	(	kIx(
<g/>
Jevsekcija	Jevsekcija	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
General	General	k1gMnSc1
Jewish	Jewish	k1gMnSc1
Labour	Laboura	k1gFnPc2
Bund	bund	k1gInSc4
in	in	k?
Lithuania	Lithuanium	k1gNnSc2
<g/>
,	,	kIx,
Poland	Poland	k1gInSc1
and	and	k?
Russia	Russia	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
TERNER	TERNER	kA
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pardubice	Pardubice	k1gInPc4
<g/>
:	:	kIx,
Kora	Kora	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
262	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901092	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
46	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SACHAR	SACHAR	kA
<g/>
,	,	kIx,
Howard	Howard	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
Dějiny	dějiny	k1gFnPc1
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Regia	Regia	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
767	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902484	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
84	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Leon	Leona	k1gFnPc2
<g/>
,	,	kIx,
A.	A.	kA
<g/>
,	,	kIx,
"	"	kIx"
<g/>
The	The	k1gFnSc1
Jewish	Jewish	k1gMnSc1
Question	Question	k1gInSc1
<g/>
"	"	kIx"
1970	#num#	k4
<g/>
,	,	kIx,
Pathfinder	Pathfinder	k1gInSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1	#num#	k4
-	-	kIx~
26	#num#	k4
<g/>
↑	↑	k?
Trotsky	Trotsky	k1gMnSc1
<g/>
,	,	kIx,
L.	L.	kA
<g/>
,	,	kIx,
"	"	kIx"
<g/>
The	The	k1gFnSc1
Russian	Russian	k1gMnSc1
Revolution	Revolution	k1gInSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
1959	#num#	k4
<g/>
,	,	kIx,
Doubleday	Doubledaa	k1gFnPc1
<g/>
,	,	kIx,
New	New	k1gFnPc1
York	York	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SCHULMAN	SCHULMAN	kA
<g/>
,	,	kIx,
Elias	Elias	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Jewish	Jewish	k1gMnSc1
Labor	Labor	k1gMnSc1
Bund	bunda	k1gFnPc2
in	in	k?
Russia	Russius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Jewish	Jewisha	k1gFnPc2
Quarterly	Quarterla	k1gMnSc2
Review	Review	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červenec	červenec	k1gInSc1
1974	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
65	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
65	#num#	k4
<g/>
-	-	kIx~
<g/>
69	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bund	bunda	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hebraistika	hebraistika	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20080314002	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
5005966-X	5005966-X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2315	#num#	k4
5150	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50073279	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
160151924	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50073279	#num#	k4
</s>
