<s>
Prokop	Prokop	k1gMnSc1	Prokop
Maxa	Maxa	k1gMnSc1	Maxa
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1883	[number]	k4	1883
Manětín	Manětín	k1gInSc1	Manětín
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1961	[number]	k4	1961
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
,	,	kIx,	,
<g/>
byl	být	k5eAaImAgInS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
realistické	realistický	k2eAgFnSc2d1	realistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
organizátor	organizátor	k1gMnSc1	organizátor
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Revolučního	revoluční	k2eAgNnSc2d1	revoluční
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
Československou	československý	k2eAgFnSc4d1	Československá
stranu	strana	k1gFnSc4	strana
pokrokovou	pokrokový	k2eAgFnSc4d1	pokroková
<g/>
.	.	kIx.	.
</s>
