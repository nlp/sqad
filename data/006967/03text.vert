<s>
Prokop	Prokop	k1gMnSc1	Prokop
Maxa	Maxa	k1gMnSc1	Maxa
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1883	[number]	k4	1883
Manětín	Manětín	k1gInSc1	Manětín
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1961	[number]	k4	1961
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
,	,	kIx,	,
<g/>
byl	být	k5eAaImAgInS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
realistické	realistický	k2eAgFnSc2d1	realistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
organizátor	organizátor	k1gMnSc1	organizátor
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Revolučního	revoluční	k2eAgNnSc2d1	revoluční
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
Československou	československý	k2eAgFnSc4d1	Československá
stranu	strana	k1gFnSc4	strana
pokrokovou	pokrokový	k2eAgFnSc4d1	pokroková
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
akademii	akademie	k1gFnSc6	akademie
a	a	k8xC	a
spojencem	spojenec	k1gMnSc7	spojenec
Tomáše	Tomáš	k1gMnSc2	Tomáš
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
realistické	realistický	k2eAgFnPc1d1	realistická
strany	strana	k1gFnPc1	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
její	její	k3xOp3gFnSc4	její
pražskou	pražský	k2eAgFnSc4d1	Pražská
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
odboji	odboj	k1gInSc6	odboj
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
organizoval	organizovat	k5eAaBmAgMnS	organizovat
československé	československý	k2eAgFnSc2d1	Československá
legie	legie	k1gFnSc2	legie
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgInS	podporovat
Masarykovu	Masarykův	k2eAgFnSc4d1	Masarykova
koncepci	koncepce	k1gFnSc4	koncepce
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
kongresu	kongres	k1gInSc2	kongres
socialistických	socialistický	k2eAgFnPc2d1	socialistická
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
osobně	osobně	k6eAd1	osobně
mluvit	mluvit	k5eAaImF	mluvit
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
domácí	domácí	k2eAgFnSc2d1	domácí
české	český	k2eAgFnSc2d1	Česká
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1917-1918	[number]	k4	1917-1918
byl	být	k5eAaImAgMnS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
československé	československý	k2eAgFnPc1d1	Československá
legie	legie	k1gFnPc1	legie
vojensky	vojensky	k6eAd1	vojensky
vystoupily	vystoupit	k5eAaPmAgFnP	vystoupit
proti	proti	k7c3	proti
bolševickému	bolševický	k2eAgInSc3d1	bolševický
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dočasně	dočasně	k6eAd1	dočasně
uvězněn	uvěznit	k5eAaPmNgInS	uvěznit
ruskými	ruský	k2eAgInPc7d1	ruský
komunistickými	komunistický	k2eAgInPc7d1	komunistický
úřady	úřad	k1gInPc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
Revolučním	revoluční	k2eAgNnSc6d1	revoluční
národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
za	za	k7c4	za
Československou	československý	k2eAgFnSc4d1	Československá
stranu	strana	k1gFnSc4	strana
pokrokovou	pokrokový	k2eAgFnSc4d1	pokroková
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
nabyl	nabýt	k5eAaPmAgInS	nabýt
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
profesí	profes	k1gFnSc7	profes
"	"	kIx"	"
<g/>
praporníkem	praporník	k1gMnSc7	praporník
<g/>
"	"	kIx"	"
československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
ČSR	ČSR	kA	ČSR
coby	coby	k?	coby
vyslanec	vyslanec	k1gMnSc1	vyslanec
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polsku	Polska	k1gFnSc4	Polska
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
zastával	zastávat	k5eAaImAgMnS	zastávat
vysoké	vysoký	k2eAgFnPc4d1	vysoká
funkce	funkce	k1gFnPc4	funkce
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Československa	Československo	k1gNnSc2	Československo
emigroval	emigrovat	k5eAaBmAgInS	emigrovat
coby	coby	k?	coby
československý	československý	k2eAgMnSc1d1	československý
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
působil	působit	k5eAaImAgMnS	působit
opět	opět	k6eAd1	opět
v	v	k7c6	v
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
odboji	odboj	k1gInSc6	odboj
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
Československé	československý	k2eAgFnSc2d1	Československá
<g/>
.	.	kIx.	.
</s>
<s>
Orientoval	orientovat	k5eAaBmAgMnS	orientovat
se	se	k3xPyFc4	se
na	na	k7c4	na
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
vzájemnost	vzájemnost	k1gFnSc4	vzájemnost
a	a	k8xC	a
sbližoval	sbližovat	k5eAaImAgMnS	sbližovat
se	se	k3xPyFc4	se
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Slovanského	slovanský	k2eAgInSc2d1	slovanský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
zůstal	zůstat	k5eAaPmAgMnS	zůstat
loajální	loajální	k2eAgMnSc1d1	loajální
vůči	vůči	k7c3	vůči
novému	nový	k2eAgInSc3d1	nový
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářel	vytvářet	k5eAaImAgInS	vytvářet
akční	akční	k2eAgInSc1d1	akční
výbor	výbor	k1gInSc1	výbor
Československé	československý	k2eAgFnSc2d1	Československá
obce	obec	k1gFnSc2	obec
legionářské	legionářský	k2eAgFnSc2d1	legionářská
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
začlenění	začlenění	k1gNnSc3	začlenění
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
do	do	k7c2	do
jednotné	jednotný	k2eAgFnSc2d1	jednotná
struktury	struktura	k1gFnSc2	struktura
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Svaz	svaz	k1gInSc1	svaz
bojovníků	bojovník	k1gMnPc2	bojovník
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
oficiálně	oficiálně	k6eAd1	oficiálně
Svaz	svaz	k1gInSc1	svaz
protifašistických	protifašistický	k2eAgMnPc2d1	protifašistický
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
ústředním	ústřední	k2eAgInSc6d1	ústřední
výboru	výbor	k1gInSc6	výbor
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
