<s>
Změnu	změna	k1gFnSc4	změna
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
udává	udávat	k5eAaImIp3nS	udávat
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc1	inflace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
jako	jako	k9	jako
poměr	poměr	k1gInSc1	poměr
vybraného	vybraný	k2eAgInSc2d1	vybraný
cenového	cenový	k2eAgInSc2d1	cenový
indexu	index	k1gInSc2	index
na	na	k7c6	na
konci	konec	k1gInSc6	konec
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
