<s>
Polarografie	polarografie	k1gFnSc1	polarografie
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
elektrochemické	elektrochemický	k2eAgFnPc4d1	elektrochemická
analytické	analytický	k2eAgFnPc4d1	analytická
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
její	její	k3xOp3gInSc4	její
objev	objev	k1gInSc4	objev
dostal	dostat	k5eAaPmAgMnS	dostat
Čech	Čech	k1gMnSc1	Čech
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgInSc1d1	Heyrovský
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
určování	určování	k1gNnSc3	určování
přítomnosti	přítomnost	k1gFnSc2	přítomnost
(	(	kIx(	(
<g/>
kvality	kvalita	k1gFnSc2	kvalita
<g/>
)	)	kIx)	)
a	a	k8xC	a
koncentrace	koncentrace	k1gFnPc1	koncentrace
(	(	kIx(	(
<g/>
kvantity	kvantita	k1gFnPc1	kvantita
<g/>
)	)	kIx)	)
redukovatelných	redukovatelný	k2eAgFnPc2d1	redukovatelná
nebo	nebo	k8xC	nebo
oxidovatelných	oxidovatelný	k2eAgFnPc2d1	oxidovatelný
neznámých	známý	k2eNgFnPc2d1	neznámá
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vyhodnocování	vyhodnocování	k1gNnSc6	vyhodnocování
závislosti	závislost	k1gFnSc2	závislost
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
na	na	k7c4	na
napětí	napětí	k1gNnSc4	napětí
na	na	k7c4	na
dvojici	dvojice	k1gFnSc4	dvojice
elektrod	elektroda	k1gFnPc2	elektroda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ponořené	ponořený	k2eAgFnPc1d1	ponořená
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
<g/>
.	.	kIx.	.
</s>
<s>
Závislosti	závislost	k1gFnPc1	závislost
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
poloha	poloha	k1gFnSc1	poloha
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
druhy	druh	k1gInPc4	druh
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velikosti	velikost	k1gFnSc2	velikost
nárůstu	nárůst	k1gInSc2	nárůst
proudu	proud	k1gInSc2	proud
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
koncentraci	koncentrace	k1gFnSc4	koncentrace
příslušné	příslušný	k2eAgFnSc2d1	příslušná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
pracovní	pracovní	k2eAgFnSc1d1	pracovní
elektroda	elektroda	k1gFnSc1	elektroda
slouží	sloužit	k5eAaImIp3nS	sloužit
rtuťová	rtuťový	k2eAgFnSc1d1	rtuťová
kapajicí	kapajicí	k2eAgFnSc1d1	kapajicí
elektroda	elektroda	k1gFnSc1	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
kapky	kapka	k1gFnSc2	kapka
rtuti	rtuť	k1gFnSc2	rtuť
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
elektrická	elektrický	k2eAgNnPc1d1	elektrické
dvojvrstva	dvojvrstvo	k1gNnPc1	dvojvrstvo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvyšování	zvyšování	k1gNnSc6	zvyšování
potenciálu	potenciál	k1gInSc2	potenciál
mezi	mezi	k7c7	mezi
anodou	anoda	k1gFnSc7	anoda
a	a	k8xC	a
katodou	katoda	k1gFnSc7	katoda
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vyloučení	vyloučení	k1gNnSc3	vyloučení
příslušných	příslušný	k2eAgInPc2d1	příslušný
iontů	ion	k1gInPc2	ion
na	na	k7c6	na
katodě	katoda	k1gFnSc6	katoda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
množstvím	množství	k1gNnSc7	množství
iontů	ion	k1gInPc2	ion
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
množství	množství	k1gNnSc1	množství
vylučujících	vylučující	k2eAgInPc2d1	vylučující
se	se	k3xPyFc4	se
iontů	ion	k1gInPc2	ion
rovná	rovnat	k5eAaImIp3nS	rovnat
počtu	počet	k1gInSc3	počet
přicházejících	přicházející	k2eAgInPc2d1	přicházející
iontů	ion	k1gInPc2	ion
z	z	k7c2	z
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
proud	proud	k1gInSc1	proud
přestane	přestat	k5eAaPmIp3nS	přestat
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
středu	střed	k1gInSc2	střed
nárůstu	nárůst	k1gInSc2	nárůst
polarizační	polarizační	k2eAgFnSc2d1	polarizační
křivky	křivka	k1gFnSc2	křivka
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
základní	základní	k2eAgInSc1d1	základní
(	(	kIx(	(
<g/>
indiferentní	indiferentní	k2eAgInSc1d1	indiferentní
<g/>
)	)	kIx)	)
elektrolyt	elektrolyt	k1gInSc1	elektrolyt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vodivost	vodivost	k1gFnSc1	vodivost
roztoku	roztok	k1gInSc2	roztok
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
povrchově	povrchově	k6eAd1	povrchově
aktivní	aktivní	k2eAgFnSc1d1	aktivní
látka	látka	k1gFnSc1	látka
-	-	kIx~	-
typicky	typicky	k6eAd1	typicky
želatina	želatina	k1gFnSc1	želatina
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
zabránit	zabránit	k5eAaPmF	zabránit
vzniku	vznik	k1gInSc3	vznik
maxima	maximum	k1gNnSc2	maximum
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
polarografické	polarografický	k2eAgFnSc2d1	polarografická
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
i	i	k9	i
probublávání	probublávání	k1gNnSc1	probublávání
vzorku	vzorek	k1gInSc2	vzorek
v	v	k7c6	v
polarografické	polarografický	k2eAgFnSc6d1	polarografická
nádobce	nádobka	k1gFnSc6	nádobka
inertním	inertní	k2eAgInSc7d1	inertní
plynem	plyn	k1gInSc7	plyn
kvůli	kvůli	k7c3	kvůli
odstranění	odstranění	k1gNnSc3	odstranění
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
na	na	k7c6	na
polarogramu	polarogram	k1gInSc6	polarogram
protáhlé	protáhlý	k2eAgFnSc2d1	protáhlá
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
znehodnocuje	znehodnocovat	k5eAaImIp3nS	znehodnocovat
záznam	záznam	k1gInSc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
polarografické	polarografický	k2eAgFnSc6d1	polarografická
metodě	metoda	k1gFnSc6	metoda
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
polarograf	polarograf	k1gInSc1	polarograf
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
tři	tři	k4xCgFnPc1	tři
základní	základní	k2eAgFnPc1d1	základní
části	část	k1gFnPc1	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
zdroj	zdroj	k1gInSc4	zdroj
<g/>
,	,	kIx,	,
zapisovač	zapisovač	k1gInSc4	zapisovač
a	a	k8xC	a
ovladač	ovladač	k1gInSc4	ovladač
rtuťové	rtuťový	k2eAgFnSc2d1	rtuťová
kapající	kapající	k2eAgFnSc2d1	kapající
elektrody	elektroda	k1gFnSc2	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Polarografie	polarografie	k1gFnSc2	polarografie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
&	&	k?	&
Jindřich	Jindřich	k1gMnSc1	Jindřich
Forejt	Forejt	k1gMnSc1	Forejt
<g/>
:	:	kIx,	:
Oscilografická	oscilografický	k2eAgFnSc1d1	oscilografický
polarografie	polarografie	k1gFnSc1	polarografie
-	-	kIx~	-
Polarografie	polarografie	k1gFnSc2	polarografie
střídavým	střídavý	k2eAgInSc7d1	střídavý
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc2	její
theorie	theorie	k1gFnSc2	theorie
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
<g/>
Vynález	vynález	k1gInSc1	vynález
polarografie	polarografie	k1gFnSc2	polarografie
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
Fotografie	fotografia	k1gFnPc4	fotografia
tří	tři	k4xCgInPc2	tři
typů	typ	k1gInPc2	typ
polarografů	polarograf	k1gInPc2	polarograf
z	z	k7c2	z
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Karolinu	Karolinum	k1gNnSc6	Karolinum
Pořad	pořad	k1gInSc4	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k6eAd1	plus
Všechny	všechen	k3xTgFnPc4	všechen
věci	věc	k1gFnPc4	věc
na	na	k7c6	na
světě	svět	k1gInSc6	svět
nejsou	být	k5eNaImIp3nP	být
krásné	krásný	k2eAgFnPc1d1	krásná
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
unikátní	unikátní	k2eAgFnPc4d1	unikátní
ukázky	ukázka	k1gFnPc4	ukázka
s	s	k7c7	s
hlasem	hlas	k1gInSc7	hlas
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
princip	princip	k1gInSc1	princip
polarografie	polarografie	k1gFnSc2	polarografie
</s>
