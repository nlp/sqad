<s>
Kongo	Kongo	k1gNnSc1	Kongo
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Congo	Congo	k6eAd1	Congo
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
Rio	Rio	k1gMnSc1	Rio
Congo	Congo	k1gMnSc1	Congo
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
až	až	k6eAd1	až
1997	[number]	k4	1997
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Zair	Zair	k1gInSc1	Zair
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvodnatější	vodnatý	k2eAgFnSc1d3	nejvodnatější
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nejvodnatější	vodnatý	k2eAgFnSc1d3	nejvodnatější
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
po	po	k7c6	po
Amazonce	Amazonka	k1gFnSc6	Amazonka
<g/>
.	.	kIx.	.
</s>
