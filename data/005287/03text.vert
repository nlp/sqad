<s>
Kongo	Kongo	k1gNnSc1	Kongo
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Congo	Congo	k6eAd1	Congo
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
Rio	Rio	k1gMnSc1	Rio
Congo	Congo	k1gMnSc1	Congo
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
až	až	k6eAd1	až
1997	[number]	k4	1997
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Zair	Zair	k1gInSc1	Zair
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvodnatější	vodnatý	k2eAgFnSc1d3	nejvodnatější
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nejvodnatější	vodnatý	k2eAgFnSc1d3	nejvodnatější
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
po	po	k7c6	po
Amazonce	Amazonka	k1gFnSc6	Amazonka
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
délky	délka	k1gFnSc2	délka
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
4	[number]	k4	4
667	[number]	k4	667
km	km	kA	km
druhá	druhý	k4xOgFnSc1	druhý
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
po	po	k7c6	po
Nilu	Nil	k1gInSc6	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
povodí	povodí	k1gNnSc1	povodí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
největší	veliký	k2eAgInSc1d3	veliký
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
říčním	říční	k2eAgNnSc7d1	říční
povodím	povodí	k1gNnSc7	povodí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
po	po	k7c6	po
Amazonce	Amazonka	k1gFnSc6	Amazonka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
3	[number]	k4	3
680	[number]	k4	680
000	[number]	k4	000
km2	km2	k4	km2
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Konžské	konžský	k2eAgFnSc6d1	Konžská
pánvi	pánev	k1gFnSc6	pánev
a	a	k8xC	a
okolních	okolní	k2eAgFnPc6d1	okolní
hornatinách	hornatina	k1gFnPc6	hornatina
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
jejích	její	k3xOp3gInPc2	její
přítoků	přítok	k1gInPc2	přítok
protéká	protékat	k5eAaImIp3nS	protékat
Konžským	konžský	k2eAgInSc7d1	konžský
deštným	deštný	k2eAgInSc7d1	deštný
pralesem	prales	k1gInSc7	prales
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
po	po	k7c6	po
Amazonském	amazonský	k2eAgInSc6d1	amazonský
pralese	prales	k1gInSc6	prales
<g/>
)	)	kIx)	)
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
tropický	tropický	k2eAgInSc1d1	tropický
deštný	deštný	k2eAgInSc1d1	deštný
prales	prales	k1gInSc1	prales
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
tvoří	tvořit	k5eAaImIp3nS	tvořit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Konžskou	konžský	k2eAgFnSc7d1	Konžská
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Konžskou	konžský	k2eAgFnSc7d1	Konžská
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
části	část	k1gFnSc6	část
před	před	k7c7	před
ústím	ústí	k1gNnSc7	ústí
rovněž	rovněž	k9	rovněž
severní	severní	k2eAgFnSc6d1	severní
hranici	hranice	k1gFnSc6	hranice
Angoly	Angola	k1gFnSc2	Angola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejích	její	k3xOp3gInPc6	její
březích	břeh	k1gInPc6	břeh
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
hlavní	hlavní	k2eAgNnPc1d1	hlavní
města	město	k1gNnPc1	město
Brazzaville	Brazzaville	k1gInSc1	Brazzaville
a	a	k8xC	a
Kinshasa	Kinshasa	k1gFnSc1	Kinshasa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stavby	stavba	k1gFnSc2	stavba
říčního	říční	k2eAgNnSc2d1	říční
údolí	údolí	k1gNnSc2	údolí
se	se	k3xPyFc4	se
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
(	(	kIx(	(
<g/>
2100	[number]	k4	2100
km	km	kA	km
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
pramenů	pramen	k1gInPc2	pramen
k	k	k7c3	k
Boyomským	Boyomský	k2eAgInPc3d1	Boyomský
vodopádům	vodopád	k1gInPc3	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úsek	úsek	k1gInSc1	úsek
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Lualaba	Lualaba	k1gFnSc1	Lualaba
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
tok	tok	k1gInSc1	tok
(	(	kIx(	(
<g/>
1700	[number]	k4	1700
km	km	kA	km
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
Boyomských	Boyomský	k2eAgInPc2d1	Boyomský
vodopádů	vodopád	k1gInPc2	vodopád
ke	k	k7c3	k
Kinshase	Kinshasa	k1gFnSc3	Kinshasa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
Konžskou	konžský	k2eAgFnSc7d1	Konžská
pánví	pánev	k1gFnSc7	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
klidný	klidný	k2eAgInSc4d1	klidný
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
neznatelný	znatelný	k2eNgInSc4d1	neznatelný
spád	spád	k1gInSc4	spád
(	(	kIx(	(
<g/>
0,07	[number]	k4	0,07
‰	‰	k?	‰
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgInPc1d1	říční
břehy	břeh	k1gInPc1	břeh
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
nízké	nízký	k2eAgFnPc1d1	nízká
a	a	k8xC	a
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
bažinaté	bažinatý	k2eAgFnPc1d1	bažinatá
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
řetězec	řetězec	k1gInSc4	řetězec
jezerům	jezero	k1gNnPc3	jezero
podobných	podobný	k2eAgFnPc2d1	podobná
rozšíření	rozšíření	k1gNnPc2	rozšíření
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
široká	široký	k2eAgNnPc1d1	široké
až	až	k8xS	až
15	[number]	k4	15
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
se	s	k7c7	s
zúženími	zúžení	k1gNnPc7	zúžení
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
2	[number]	k4	2
až	až	k9	až
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
pánve	pánev	k1gFnSc2	pánev
se	se	k3xPyFc4	se
zaplavovaná	zaplavovaný	k2eAgNnPc1d1	zaplavované
území	území	k1gNnPc1	území
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
pravých	pravý	k2eAgInPc2d1	pravý
přítoků	přítok	k1gInPc2	přítok
(	(	kIx(	(
<g/>
Ubangi	Ubangi	k1gNnSc1	Ubangi
<g/>
,	,	kIx,	,
Sanga	Sanga	k1gFnSc1	Sanga
<g/>
)	)	kIx)	)
spojují	spojovat	k5eAaImIp3nP	spojovat
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
periodicky	periodicky	k6eAd1	periodicky
zaplavovaných	zaplavovaný	k2eAgFnPc2d1	zaplavovaná
oblastí	oblast	k1gFnPc2	oblast
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
k	k	k7c3	k
západnímu	západní	k2eAgInSc3d1	západní
okraji	okraj	k1gInSc3	okraj
pánve	pánev	k1gFnSc2	pánev
její	její	k3xOp3gInSc1	její
charakter	charakter	k1gInSc1	charakter
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
semknuta	semknout	k5eAaPmNgFnS	semknout
mezi	mezi	k7c4	mezi
vysoké	vysoký	k2eAgMnPc4d1	vysoký
(	(	kIx(	(
<g/>
až	až	k9	až
100	[number]	k4	100
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
prudké	prudký	k2eAgInPc4d1	prudký
břehy	břeh	k1gInPc4	břeh
a	a	k8xC	a
zužuje	zužovat	k5eAaImIp3nS	zužovat
se	se	k3xPyFc4	se
místy	místy	k6eAd1	místy
až	až	k9	až
na	na	k7c4	na
1	[number]	k4	1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
zde	zde	k6eAd1	zde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
m	m	kA	m
a	a	k8xC	a
rychlost	rychlost	k1gFnSc1	rychlost
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zúžený	zúžený	k2eAgInSc1d1	zúžený
úsek	úsek	k1gInSc1	úsek
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Kanál	kanál	k1gInSc1	kanál
<g/>
)	)	kIx)	)
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
jezerovité	jezerovitý	k2eAgNnSc4d1	jezerovitý
rozšíření	rozšíření	k1gNnSc4	rozšíření
Malebo	Maleba	k1gFnSc5	Maleba
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
střední	střední	k2eAgInSc4d1	střední
tok	tok	k1gInSc4	tok
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
(	(	kIx(	(
<g/>
500	[number]	k4	500
km	km	kA	km
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
Kinshasy	Kinshasa	k1gFnSc2	Kinshasa
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
u	u	k7c2	u
města	město	k1gNnSc2	město
Banana	Banan	k1gMnSc2	Banan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
k	k	k7c3	k
oceánu	oceán	k1gInSc2	oceán
přes	přes	k7c4	přes
Jihoguinejskou	Jihoguinejský	k2eAgFnSc4d1	Jihoguinejský
vysočinu	vysočina	k1gFnSc4	vysočina
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
500	[number]	k4	500
m	m	kA	m
<g/>
)	)	kIx)	)
soutěsce	soutěska	k1gFnSc3	soutěska
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
koryta	koryto	k1gNnSc2	koryto
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
400	[number]	k4	400
až	až	k9	až
500	[number]	k4	500
m	m	kA	m
a	a	k8xC	a
místy	místy	k6eAd1	místy
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
na	na	k7c4	na
220	[number]	k4	220
až	až	k9	až
250	[number]	k4	250
m.	m.	k?	m.
Na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
mezi	mezi	k7c7	mezi
Kinshasou	Kinshasa	k1gFnSc7	Kinshasa
a	a	k8xC	a
Matadi	Matad	k1gMnPc1	Matad
řeka	řek	k1gMnSc2	řek
klesá	klesat	k5eAaImIp3nS	klesat
o	o	k7c4	o
270	[number]	k4	270
m	m	kA	m
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
peřejí	peřej	k1gFnPc2	peřej
a	a	k8xC	a
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dohromady	dohromady	k6eAd1	dohromady
nazývají	nazývat	k5eAaImIp3nP	nazývat
Livingstonovy	Livingstonův	k2eAgInPc4d1	Livingstonův
vodopády	vodopád	k1gInPc4	vodopád
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nejvodnější	vodný	k2eAgNnSc4d3	vodný
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Matadi	Matad	k1gMnPc1	Matad
řeka	řeka	k1gFnSc1	řeka
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
přímořské	přímořský	k2eAgFnSc2d1	přímořská
roviny	rovina	k1gFnSc2	rovina
a	a	k8xC	a
koryto	koryto	k1gNnSc1	koryto
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
na	na	k7c4	na
12	[number]	k4	12
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
zde	zde	k6eAd1	zde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
25	[number]	k4	25
až	až	k9	až
30	[number]	k4	30
m.	m.	k?	m.
Poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Boma	Bom	k1gInSc2	Bom
začíná	začínat	k5eAaImIp3nS	začínat
estuár	estuár	k1gInSc1	estuár
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
šířka	šířka	k1gFnSc1	šířka
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
až	až	k9	až
19	[number]	k4	19
km	km	kA	km
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
na	na	k7c4	na
3,5	[number]	k4	3,5
km	km	kA	km
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
na	na	k7c4	na
9,8	[number]	k4	9,8
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
estuáru	estuár	k1gInSc2	estuár
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
vznikající	vznikající	k2eAgFnSc7d1	vznikající
mladou	mladý	k2eAgFnSc7d1	mladá
deltou	delta	k1gFnSc7	delta
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužením	prodloužení	k1gNnSc7	prodloužení
estuáru	estuár	k1gInSc2	estuár
je	být	k5eAaImIp3nS	být
podvodní	podvodní	k2eAgInSc4d1	podvodní
proud	proud	k1gInSc4	proud
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
nejméně	málo	k6eAd3	málo
800	[number]	k4	800
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
(	(	kIx(	(
<g/>
Lualaba	Lualaba	k1gMnSc1	Lualaba
<g/>
)	)	kIx)	)
Lufira	Lufira	k1gMnSc1	Lufira
<g/>
,	,	kIx,	,
Luvua	Luvua	k1gMnSc1	Luvua
<g/>
,	,	kIx,	,
Lukuga	Lukuga	k1gFnSc1	Lukuga
(	(	kIx(	(
<g/>
zprava	zprava	k6eAd1	zprava
<g/>
)	)	kIx)	)
Střední	střední	k2eAgInSc1d1	střední
tok	tok	k1gInSc1	tok
Lomami	Loma	k1gFnPc7	Loma
<g/>
,	,	kIx,	,
Lulonga	Lulonga	k1gFnSc1	Lulonga
<g/>
,	,	kIx,	,
Ruki	Ruki	k1gNnSc1	Ruki
<g/>
,	,	kIx,	,
Kasai	Kasai	k1gNnSc1	Kasai
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
levý	levý	k2eAgInSc1d1	levý
přítok	přítok	k1gInSc1	přítok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
přítokem	přítok	k1gInSc7	přítok
Fimi	Fim	k1gFnSc2	Fim
a	a	k8xC	a
ústím	ústí	k1gNnSc7	ústí
nazývá	nazývat	k5eAaImIp3nS	nazývat
Kwa	Kwa	k1gFnSc1	Kwa
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Nsele	Nsele	k1gFnPc4	Nsele
(	(	kIx(	(
<g/>
zleva	zleva	k6eAd1	zleva
<g/>
)	)	kIx)	)
Lindi	Lind	k1gMnPc1	Lind
<g/>
,	,	kIx,	,
Tshopo	Tshopa	k1gFnSc5	Tshopa
<g/>
,	,	kIx,	,
Aruwimi	Aruwi	k1gFnPc7	Aruwi
<g/>
,	,	kIx,	,
Itimbiri	Itimbir	k1gMnPc7	Itimbir
<g/>
,	,	kIx,	,
Mongala	Mongala	k1gFnSc1	Mongala
(	(	kIx(	(
<g/>
se	s	k7c7	s
smutně	smutně	k6eAd1	smutně
proslulým	proslulý	k2eAgInSc7d1	proslulý
přítokem	přítok	k1gInSc7	přítok
Ebola	Ebolo	k1gNnSc2	Ebolo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ubangi	Ubangi	k1gNnSc1	Ubangi
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
přítok	přítok	k1gInSc1	přítok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Likouala	Likouala	k1gFnSc1	Likouala
<g/>
,	,	kIx,	,
Sangha	Sangha	k1gFnSc1	Sangha
(	(	kIx(	(
<g/>
zprava	zprava	k6eAd1	zprava
<g/>
)	)	kIx)	)
Dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
Inkisi	Inkis	k1gMnSc3	Inkis
<g/>
,	,	kIx,	,
Bombo	bomba	k1gFnSc5	bomba
K	k	k7c3	k
systému	systém	k1gInSc3	systém
řeky	řeka	k1gFnSc2	řeka
náleží	náležet	k5eAaImIp3nS	náležet
několik	několik	k4yIc1	několik
velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Tanganika	Tanganika	k1gFnSc1	Tanganika
a	a	k8xC	a
Kivu	Kiva	k1gFnSc4	Kiva
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Lukugy	Lukuga	k1gFnSc2	Lukuga
<g/>
,	,	kIx,	,
Bangweulu	Bangweul	k1gInSc2	Bangweul
a	a	k8xC	a
Mweru	Mwer	k1gInSc2	Mwer
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Luvuy	Luvua	k1gFnSc2	Luvua
<g/>
,	,	kIx,	,
Mai	Mai	k1gMnSc5	Mai
Ndombe	Ndomb	k1gMnSc5	Ndomb
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Kasai	Kasa	k1gFnSc2	Kasa
a	a	k8xC	a
Tumba	tumba	k1gFnSc1	tumba
spojena	spojit	k5eAaPmNgFnS	spojit
průtokem	průtok	k1gInSc7	průtok
Irebu	Ireb	k1gInSc2	Ireb
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
Kongem	Kongo	k1gNnSc7	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
průtok	průtok	k1gInSc4	průtok
řek	řeka	k1gFnPc2	řeka
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Konga	Kongo	k1gNnSc2	Kongo
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
převážně	převážně	k6eAd1	převážně
bohaté	bohatý	k2eAgFnPc1d1	bohatá
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
přítoků	přítok	k1gInPc2	přítok
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
maximum	maximum	k1gNnSc4	maximum
od	od	k7c2	od
září	září	k1gNnSc2	září
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Přítoky	přítok	k1gInPc1	přítok
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
včetně	včetně	k7c2	včetně
horního	horní	k2eAgInSc2d1	horní
toku	tok	k1gInSc2	tok
Lualaby	Lualaba	k1gMnSc2	Lualaba
mají	mít	k5eAaImIp3nP	mít
maximum	maximum	k1gNnSc4	maximum
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
a	a	k8xC	a
především	především	k9	především
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
je	být	k5eAaImIp3nS	být
sezónní	sezónní	k2eAgNnSc1d1	sezónní
kolísání	kolísání	k1gNnSc1	kolísání
menší	malý	k2eAgNnSc1d2	menší
díky	díky	k7c3	díky
vyváženosti	vyváženost	k1gFnSc3	vyváženost
severních	severní	k2eAgInPc2d1	severní
a	a	k8xC	a
jižních	jižní	k2eAgInPc2d1	jižní
přítoků	přítok	k1gInPc2	přítok
a	a	k8xC	a
tok	tok	k1gInSc1	tok
Konga	Kongo	k1gNnSc2	Kongo
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
přirozeně	přirozeně	k6eAd1	přirozeně
regulován	regulovat	k5eAaImNgInS	regulovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
vzestupům	vzestup	k1gInPc3	vzestup
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
poklesům	pokles	k1gInPc3	pokles
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
závisí	záviset	k5eAaImIp3nS	záviset
vzestup	vzestup	k1gInSc1	vzestup
hladiny	hladina	k1gFnSc2	hladina
na	na	k7c6	na
podzimním	podzimní	k2eAgNnSc6d1	podzimní
maximu	maximum	k1gNnSc6	maximum
Lualaby	Lualaba	k1gFnSc2	Lualaba
a	a	k8xC	a
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
květen	květen	k1gInSc4	květen
až	až	k8xS	až
červen	červen	k1gInSc4	červen
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
druhotný	druhotný	k2eAgInSc4d1	druhotný
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vzestup	vzestup	k1gInSc1	vzestup
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ale	ale	k9	ale
způsoben	způsobit	k5eAaPmNgInS	způsobit
severními	severní	k2eAgInPc7d1	severní
přítoky	přítok	k1gInPc7	přítok
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
nastává	nastávat	k5eAaImIp3nS	nastávat
hlavní	hlavní	k2eAgInSc4d1	hlavní
vzestup	vzestup	k1gInSc4	vzestup
také	také	k9	také
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
a	a	k8xC	a
méně	málo	k6eAd2	málo
významný	významný	k2eAgInSc1d1	významný
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
až	až	k8xS	až
května	květen	k1gInSc2	květen
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
podzimním	podzimní	k2eAgInSc7d1	podzimní
maximem	maxim	k1gInSc7	maxim
řeky	řeka	k1gFnSc2	řeka
Kasai	Kasa	k1gFnSc2	Kasa
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
za	za	k7c4	za
rok	rok	k1gInSc4	rok
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
činí	činit	k5eAaImIp3nS	činit
41	[number]	k4	41
800	[number]	k4	800
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
V	v	k7c6	v
nejvodnějším	vodný	k2eAgInSc6d3	vodný
měsíci	měsíc	k1gInSc6	měsíc
prosinci	prosinec	k1gInSc6	prosinec
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
60	[number]	k4	60
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
v	v	k7c6	v
nejméně	málo	k6eAd3	málo
vodném	vodný	k2eAgInSc6d1	vodný
červenci	červenec	k1gInSc6	červenec
29	[number]	k4	29
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Absolutní	absolutní	k2eAgFnSc2d1	absolutní
krajní	krajní	k2eAgFnSc2d1	krajní
hodnoty	hodnota	k1gFnSc2	hodnota
jsou	být	k5eAaImIp3nP	být
23	[number]	k4	23
000	[number]	k4	000
a	a	k8xC	a
75	[number]	k4	75
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Celkový	celkový	k2eAgInSc1d1	celkový
roční	roční	k2eAgInSc1d1	roční
odtok	odtok	k1gInSc1	odtok
je	být	k5eAaImIp3nS	být
1230	[number]	k4	1230
až	až	k9	až
1453	[number]	k4	1453
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Ohromná	ohromný	k2eAgFnSc1d1	ohromná
masa	masa	k1gFnSc1	masa
vody	voda	k1gFnSc2	voda
oslazuje	oslazovat	k5eAaImIp3nS	oslazovat
oceán	oceán	k1gInSc1	oceán
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
75	[number]	k4	75
km	km	kA	km
od	od	k7c2	od
břehu	břeh	k1gInSc2	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Úsekem	úsek	k1gInSc7	úsek
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
nese	nést	k5eAaImIp3nS	nést
řeka	řeka	k1gFnSc1	řeka
50	[number]	k4	50
Mt	Mt	k1gMnPc2	Mt
pevných	pevný	k2eAgFnPc2d1	pevná
částic	částice	k1gFnPc2	částice
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
zásob	zásoba	k1gFnPc2	zásoba
vodní	vodní	k2eAgFnSc2d1	vodní
energie	energie	k1gFnSc2	energie
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
Kongo	Kongo	k1gNnSc4	Kongo
první	první	k4xOgFnSc2	první
místo	místo	k7c2	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
však	však	k9	však
využívá	využívat	k5eAaPmIp3nS	využívat
pouze	pouze	k6eAd1	pouze
zanedbatelné	zanedbatelný	k2eAgNnSc1d1	zanedbatelné
množství	množství	k1gNnSc1	množství
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Marinel	Marinel	k1gMnSc1	Marinel
<g/>
,	,	kIx,	,
Delcomiun	Delcomiun	k1gMnSc1	Delcomiun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
nabízí	nabízet	k5eAaImIp3nS	nabízet
vynikající	vynikající	k2eAgFnSc4d1	vynikající
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
budování	budování	k1gNnSc4	budování
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
převýšení	převýšení	k1gNnSc2	převýšení
96	[number]	k4	96
m	m	kA	m
na	na	k7c4	na
15	[number]	k4	15
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohla	moct	k5eAaImAgFnS	moct
by	by	kYmCp3nS	by
zde	zde	k6eAd1	zde
vzniknout	vzniknout	k5eAaPmF	vzniknout
svou	svůj	k3xOyFgFnSc7	svůj
produkcí	produkce	k1gFnSc7	produkce
největší	veliký	k2eAgFnSc1d3	veliký
soustava	soustava	k1gFnSc1	soustava
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
zbudované	zbudovaný	k2eAgFnSc2d1	zbudovaná
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
Inga	Ing	k1gInSc2	Ing
I	i	k9	i
a	a	k8xC	a
Inga	Inga	k1gFnSc1	Inga
II	II	kA	II
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
Kinshase	Kinshasa	k1gFnSc6	Kinshasa
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárny	elektrárna	k1gFnPc1	elektrárna
byly	být	k5eAaImAgFnP	být
zbudovány	zbudovat	k5eAaPmNgFnP	zbudovat
také	také	k9	také
pro	pro	k7c4	pro
provincii	provincie	k1gFnSc4	provincie
Shaba	Shaba	k1gFnSc1	Shaba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
díky	díky	k7c3	díky
těžbě	těžba	k1gFnSc3	těžba
mědi	měď	k1gFnSc2	měď
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
základny	základna	k1gFnPc4	základna
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zbudováno	zbudovat	k5eAaPmNgNnS	zbudovat
1800	[number]	k4	1800
km	km	kA	km
elektrického	elektrický	k2eAgNnSc2d1	elektrické
vedení	vedení	k1gNnSc2	vedení
(	(	kIx(	(
<g/>
700	[number]	k4	700
000	[number]	k4	000
V	V	kA	V
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
ztrát	ztráta	k1gFnPc2	ztráta
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
nicméně	nicméně	k8xC	nicméně
neslouží	sloužit	k5eNaImIp3nP	sloužit
k	k	k7c3	k
napájení	napájení	k1gNnSc3	napájení
oblastí	oblast	k1gFnPc2	oblast
mezi	mezi	k7c7	mezi
Kinshasou	Kinshasa	k1gFnSc7	Kinshasa
a	a	k8xC	a
Shabou	Shaba	k1gFnSc7	Shaba
–	–	k?	–
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zvýšit	zvýšit	k5eAaPmF	zvýšit
kontrolu	kontrola	k1gFnSc4	kontrola
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
snažila	snažit	k5eAaImAgFnS	snažit
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
snižování	snižování	k1gNnSc3	snižování
produkce	produkce	k1gFnSc2	produkce
dolů	dol	k1gInPc2	dol
však	však	k9	však
hlavním	hlavní	k2eAgMnSc7d1	hlavní
spotřebitelem	spotřebitel	k1gMnSc7	spotřebitel
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Kinshasy	Kinshasa	k1gFnSc2	Kinshasa
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
tak	tak	k9	tak
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgNnSc1d1	dobré
veřejné	veřejný	k2eAgNnSc1d1	veřejné
osvětlení	osvětlení	k1gNnSc1	osvětlení
i	i	k9	i
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
nejodlehlejších	odlehlý	k2eAgFnPc6d3	nejodlehlejší
částech	část	k1gFnPc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
splavných	splavný	k2eAgFnPc2d1	splavná
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
řekách	řeka	k1gFnPc6	řeka
a	a	k8xC	a
jezerech	jezero	k1gNnPc6	jezero
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
splavných	splavný	k2eAgInPc2d1	splavný
úseků	úsek	k1gInPc2	úsek
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Konžské	konžský	k2eAgFnSc6d1	Konžská
pánvi	pánev	k1gFnSc6	pánev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
představují	představovat	k5eAaImIp3nP	představovat
jeden	jeden	k4xCgMnSc1	jeden
velký	velký	k2eAgInSc1d1	velký
rozvětvený	rozvětvený	k2eAgInSc1d1	rozvětvený
vodní	vodní	k2eAgInSc1d1	vodní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
od	od	k7c2	od
oceánu	oceán	k1gInSc2	oceán
oddělen	oddělit	k5eAaPmNgInS	oddělit
Livingstonovými	Livingstonový	k2eAgInPc7d1	Livingstonový
vodopády	vodopád	k1gInPc7	vodopád
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
hlavní	hlavní	k2eAgInPc1d1	hlavní
úseky	úsek	k1gInPc1	úsek
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
vodní	vodní	k2eAgFnSc4d1	vodní
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
samotné	samotný	k2eAgFnSc6d1	samotná
řece	řeka	k1gFnSc6	řeka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Bukama	Bukama	k?	Bukama
–	–	k?	–
Kongolo	Kongola	k1gFnSc5	Kongola
(	(	kIx(	(
<g/>
645	[number]	k4	645
km	km	kA	km
<g/>
)	)	kIx)	)
–	–	k?	–
na	na	k7c6	na
Lualabě	Lualaba	k1gFnSc6	Lualaba
Kindu	Kind	k1gInSc2	Kind
–	–	k?	–
Ubundu	Ubund	k1gInSc2	Ubund
(	(	kIx(	(
<g/>
300	[number]	k4	300
km	km	kA	km
<g/>
)	)	kIx)	)
–	–	k?	–
Lualabě	Lualaba	k1gFnSc3	Lualaba
Kisangani	Kisangaň	k1gFnSc3	Kisangaň
–	–	k?	–
Kinshasa	Kinshasa	k1gFnSc1	Kinshasa
(	(	kIx(	(
<g/>
1742	[number]	k4	1742
km	km	kA	km
<g/>
)	)	kIx)	)
Matadi	Matad	k1gMnPc1	Matad
–	–	k?	–
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
(	(	kIx(	(
<g/>
138	[number]	k4	138
km	km	kA	km
<g/>
)	)	kIx)	)
–	–	k?	–
dostupný	dostupný	k2eAgInSc4d1	dostupný
pro	pro	k7c4	pro
námořní	námořní	k2eAgFnPc4d1	námořní
lodě	loď	k1gFnPc4	loď
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
splavné	splavný	k2eAgInPc1d1	splavný
úseky	úsek	k1gInPc1	úsek
jsou	být	k5eAaImIp3nP	být
pospojovány	pospojován	k2eAgFnPc1d1	pospojován
železnicemi	železnice	k1gFnPc7	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
říční	říční	k2eAgInSc1d1	říční
a	a	k8xC	a
jezerní	jezerní	k2eAgInPc1d1	jezerní
přístavy	přístav	k1gInPc1	přístav
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
jsou	být	k5eAaImIp3nP	být
Kinshasa	Kinshasa	k1gFnSc1	Kinshasa
<g/>
,	,	kIx,	,
Brazaville	Brazavill	k1gMnSc4	Brazavill
<g/>
,	,	kIx,	,
Mbandaka	Mbandak	k1gMnSc4	Mbandak
<g/>
,	,	kIx,	,
Kisangani	Kisangaň	k1gFnSc6	Kisangaň
<g/>
,	,	kIx,	,
Ubundu	Ubund	k1gInSc6	Ubund
<g/>
,	,	kIx,	,
Kindu	Kind	k1gInSc6	Kind
<g/>
,	,	kIx,	,
Kongolo	Kongola	k1gFnSc5	Kongola
<g/>
,	,	kIx,	,
Kabalo	kabala	k1gFnSc5	kabala
<g/>
,	,	kIx,	,
Bukama	Bukama	k?	Bukama
<g/>
,	,	kIx,	,
Bangi	Bang	k1gMnSc5	Bang
(	(	kIx(	(
<g/>
Ubangi	Ubang	k1gMnSc6	Ubang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ilebo	Ileba	k1gFnSc5	Ileba
aneb	aneb	k?	aneb
Port	port	k1gInSc1	port
Franki	Franki	k1gNnSc1	Franki
(	(	kIx(	(
<g/>
Kasai	Kasai	k1gNnSc1	Kasai
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kalima	Kalima	k1gFnSc1	Kalima
<g/>
,	,	kIx,	,
Kigoma	Kigoma	k1gFnSc1	Kigoma
<g/>
,	,	kIx,	,
Bujumbura	Bujumbura	k1gFnSc1	Bujumbura
(	(	kIx(	(
<g/>
Tanganika	Tanganika	k1gFnSc1	Tanganika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bukavu	Bukava	k1gFnSc4	Bukava
(	(	kIx(	(
<g/>
Kivu	Kiva	k1gFnSc4	Kiva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
námořní	námořní	k2eAgInSc1d1	námořní
přístav	přístav	k1gInSc1	přístav
Matadi	Matad	k1gMnPc1	Matad
s	s	k7c7	s
vnějším	vnější	k2eAgInSc7d1	vnější
přístavem	přístav	k1gInSc7	přístav
Ango-Ango	Ango-Ango	k6eAd1	Ango-Ango
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Boma	Boma	k1gFnSc1	Boma
a	a	k8xC	a
Banana	Banana	k1gFnSc1	Banana
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
a	a	k8xC	a
jezera	jezero	k1gNnPc1	jezero
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
jsou	být	k5eAaImIp3nP	být
bohatá	bohatý	k2eAgNnPc1d1	bohaté
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
mají	mít	k5eAaImIp3nP	mít
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
využití	využití	k1gNnSc1	využití
(	(	kIx(	(
<g/>
okoun	okoun	k1gMnSc1	okoun
nilský	nilský	k2eAgMnSc1d1	nilský
<g/>
,	,	kIx,	,
tlamouni	tlamoun	k1gMnPc1	tlamoun
<g/>
,	,	kIx,	,
parmy	parma	k1gFnPc1	parma
<g/>
,	,	kIx,	,
tygří	tygří	k2eAgFnPc1d1	tygří
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
sladkovodní	sladkovodní	k2eAgMnPc1d1	sladkovodní
sledi	sleď	k1gMnPc1	sleď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
řeky	řeka	k1gFnSc2	řeka
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1482	[number]	k4	1482
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
1484	[number]	k4	1484
<g/>
)	)	kIx)	)
portugalským	portugalský	k2eAgMnSc7d1	portugalský
mořeplavcem	mořeplavec	k1gMnSc7	mořeplavec
Diogem	Diog	k1gMnSc7	Diog
Cã	Cã	k1gMnSc7	Cã
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
(	(	kIx(	(
<g/>
Lualaba	Lualaba	k1gMnSc1	Lualaba
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
Davidem	David	k1gMnSc7	David
Livingstonem	Livingston	k1gInSc7	Livingston
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
toku	tok	k1gInSc2	tok
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
Henry	henry	k1gInSc1	henry
Morton	Morton	k1gInSc4	Morton
Stanley	Stanlea	k1gFnSc2	Stanlea
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1876	[number]	k4	1876
až	až	k9	až
1877	[number]	k4	1877
</s>
