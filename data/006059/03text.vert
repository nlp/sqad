<s>
Teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
kapalina	kapalina	k1gFnSc1	kapalina
vře	vřít	k5eAaImIp3nS	vřít
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikálně	fyzikálně	k6eAd1	fyzikálně
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
definována	definovat	k5eAaBmNgFnS	definovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
vyrovná	vyrovnat	k5eAaBmIp3nS	vyrovnat
tlak	tlak	k1gInSc1	tlak
par	para	k1gFnPc2	para
kapaliny	kapalina	k1gFnSc2	kapalina
s	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
okolního	okolní	k2eAgInSc2d1	okolní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
atmosférickém	atmosférický	k2eAgInSc6d1	atmosférický
tlaku	tlak	k1gInSc6	tlak
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
obecněji	obecně	k6eAd2	obecně
na	na	k7c6	na
tlaku	tlak	k1gInSc6	tlak
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c4	na
kapalinu	kapalina	k1gFnSc4	kapalina
působí	působit	k5eAaImIp3nS	působit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tlaku	tlak	k1gInSc6	tlak
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
ve	v	k7c6	v
fázovém	fázový	k2eAgInSc6d1	fázový
diagramu	diagram	k1gInSc6	diagram
čáru	čára	k1gFnSc4	čára
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
"	"	kIx"	"
<g/>
bod	bod	k1gInSc1	bod
varu	var	k1gInSc2	var
<g/>
"	"	kIx"	"
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
trojného	trojný	k2eAgInSc2d1	trojný
bodu	bod	k1gInSc2	bod
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
bodem	bod	k1gInSc7	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hodnotou	hodnota	k1gFnSc7	hodnota
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
křivce	křivka	k1gFnSc6	křivka
za	za	k7c2	za
daného	daný	k2eAgInSc2d1	daný
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
veličiny	veličina	k1gFnSc2	veličina
<g/>
:	:	kIx,	:
tv	tv	k?	tv
Základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
kelvin	kelvin	k1gInSc1	kelvin
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
K	k	k7c3	k
Další	další	k2eAgMnSc1d1	další
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
:	:	kIx,	:
viz	vidět	k5eAaImRp2nS	vidět
teplota	teplota	k1gFnSc1	teplota
Tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
teploty	teplota	k1gFnSc2	teplota
varu	var	k1gInSc2	var
vybraných	vybraný	k2eAgFnPc2d1	vybraná
látek	látka	k1gFnPc2	látka
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
Var	Vary	k1gInPc2	Vary
</s>
