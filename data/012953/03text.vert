<p>
<s>
Nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
s	s	k7c7	s
článkem	článek	k1gInSc7	článek
William	William	k1gInSc1	William
Abraham	Abraham	k1gMnSc1	Abraham
Bell	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
také	také	k9	také
fotografem	fotograf	k1gMnSc7	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1910	[number]	k4	1910
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fotograf	fotograf	k1gMnSc1	fotograf
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgMnSc1d1	aktivní
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotograficky	fotograficky	k6eAd1	fotograficky
dokumentoval	dokumentovat	k5eAaBmAgMnS	dokumentovat
zranění	zranění	k1gNnSc4	zranění
a	a	k8xC	a
choroby	choroba	k1gFnSc2	choroba
vojáků	voják	k1gMnPc2	voják
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
pro	pro	k7c4	pro
armádní	armádní	k2eAgNnSc4d1	armádní
muzeum	muzeum	k1gNnSc4	muzeum
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
lékařské	lékařský	k2eAgFnSc6d1	lékařská
knize	kniha	k1gFnSc6	kniha
Medical	Medical	k1gFnSc2	Medical
and	and	k?	and
Surgical	Surgical	k1gFnSc2	Surgical
History	Histor	k1gInPc4	Histor
of	of	k?	of
the	the	k?	the
War	War	k1gFnSc2	War
of	of	k?	of
the	the	k?	the
Rebellion	Rebellion	k?	Rebellion
(	(	kIx(	(
<g/>
Historie	historie	k1gFnSc1	historie
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
chirurgie	chirurgie	k1gFnSc2	chirurgie
válečné	válečný	k2eAgFnSc2d1	válečná
vzpoury	vzpoura	k1gFnSc2	vzpoura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
západní	západní	k2eAgFnSc4d1	západní
krajinu	krajina	k1gFnSc4	krajina
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
účasti	účast	k1gFnSc6	účast
na	na	k7c6	na
Wheelerově	Wheelerův	k2eAgFnSc6d1	Wheelerova
expedici	expedice	k1gFnSc6	expedice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
zralém	zralý	k2eAgInSc6d1	zralý
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgInS	psát
odborné	odborný	k2eAgInPc4d1	odborný
články	článek	k1gInPc4	článek
o	o	k7c6	o
suchém	suchý	k2eAgInSc6d1	suchý
želatinovém	želatinový	k2eAgInSc6d1	želatinový
procesu	proces	k1gInSc6	proces
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
fotografických	fotografický	k2eAgInPc2d1	fotografický
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
na	na	k7c4	na
epidemii	epidemie	k1gFnSc4	epidemie
cholery	cholera	k1gFnSc2	cholera
odešel	odejít	k5eAaPmAgMnS	odejít
ke	k	k7c3	k
kvakerské	kvakerský	k2eAgFnSc3d1	kvakerská
rodině	rodina	k1gFnSc3	rodina
do	do	k7c2	do
Abingtonu	Abington	k1gInSc2	Abington
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
,	,	kIx,	,
ven	ven	k6eAd1	ven
z	z	k7c2	z
Philadelphie	Philadelphia	k1gFnSc2	Philadelphia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
Mexicko-americké	mexickomerický	k2eAgFnSc2d1	mexicko-americká
války	válka	k1gFnSc2	válka
Bell	bell	k1gInSc1	bell
odcestoval	odcestovat	k5eAaPmAgInS	odcestovat
do	do	k7c2	do
Louisiany	Louisiana	k1gFnSc2	Louisiana
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
amerického	americký	k2eAgMnSc2d1	americký
6	[number]	k4	6
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
Bell	bell	k1gInSc1	bell
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
daguerreotypického	daguerreotypický	k2eAgNnSc2d1	daguerreotypický
studia	studio	k1gNnSc2	studio
svého	svůj	k3xOyFgMnSc2	svůj
švagra	švagr	k1gMnSc2	švagr
Johna	John	k1gMnSc2	John
Keenana	Keenan	k1gMnSc2	Keenan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
otevřel	otevřít	k5eAaPmAgInS	otevřít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
fotografické	fotografický	k2eAgNnSc4d1	fotografické
studio	studio	k1gNnSc4	studio
na	na	k7c4	na
Chestnut	Chestnut	k2eAgInSc4d1	Chestnut
Street	Street	k1gInSc4	Street
a	a	k8xC	a
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
provozoval	provozovat	k5eAaImAgMnS	provozovat
nebo	nebo	k8xC	nebo
společně	společně	k6eAd1	společně
vedl	vést	k5eAaImAgInS	vést
studio	studio	k1gNnSc4	studio
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
Americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Bell	bell	k1gInSc1	bell
zapsal	zapsat	k5eAaPmAgInS	zapsat
do	do	k7c2	do
Prvního	první	k4xOgInSc2	první
pluku	pluk	k1gInSc2	pluk
pensylvánských	pensylvánský	k2eAgMnPc2d1	pensylvánský
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
bitvy	bitva	k1gFnPc1	bitva
u	u	k7c2	u
Antietamu	Antietam	k1gInSc2	Antietam
a	a	k8xC	a
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Gettysburgu	Gettysburg	k1gInSc2	Gettysburg
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
Bell	bell	k1gInSc1	bell
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
jako	jako	k9	jako
šéf	šéf	k1gMnSc1	šéf
fotografického	fotografický	k2eAgNnSc2d1	fotografické
oddělení	oddělení	k1gNnSc2	oddělení
do	do	k7c2	do
armádního	armádní	k2eAgNnSc2d1	armádní
muzea	muzeum	k1gNnSc2	muzeum
medicíny	medicína	k1gFnSc2	medicína
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
strávil	strávit	k5eAaPmAgInS	strávit
fotografickou	fotografický	k2eAgFnSc7d1	fotografická
dokumentací	dokumentace	k1gFnSc7	dokumentace
vojáků	voják	k1gMnPc2	voják
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
poraněními	poranění	k1gNnPc7	poranění
a	a	k8xC	a
amputacemi	amputace	k1gFnPc7	amputace
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Medical	Medical	k1gFnSc2	Medical
and	and	k?	and
Surgical	Surgical	k1gFnSc2	Surgical
History	Histor	k1gInPc4	Histor
of	of	k?	of
the	the	k?	the
War	War	k1gFnSc2	War
of	of	k?	of
the	the	k?	the
Rebellion	Rebellion	k?	Rebellion
(	(	kIx(	(
<g/>
Historie	historie	k1gFnSc1	historie
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
chirurgie	chirurgie	k1gFnSc2	chirurgie
válečné	válečný	k2eAgFnSc2d1	válečná
vzpoury	vzpoura	k1gFnSc2	vzpoura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Portrétoval	portrétovat	k5eAaImAgMnS	portrétovat
také	také	k9	také
hodnostáře	hodnostář	k1gMnPc4	hodnostář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
muzeum	muzeum	k1gNnSc4	muzeum
navštívili	navštívit	k5eAaPmAgMnP	navštívit
a	a	k8xC	a
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
také	také	k9	také
na	na	k7c6	na
bojištích	bojiště	k1gNnPc6	bojiště
Americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pořídil	pořídit	k5eAaPmAgMnS	pořídit
studio	studio	k1gNnSc4	studio
Jamese	Jamese	k1gFnSc2	Jamese
McCleese	McCleese	k1gFnSc2	McCleese
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
se	se	k3xPyFc4	se
Bell	bell	k1gInSc1	bell
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
průzkumné	průzkumný	k2eAgFnPc4d1	průzkumná
expedice	expedice	k1gFnPc4	expedice
George	Georg	k1gMnSc2	Georg
Wheelera	Wheeler	k1gMnSc2	Wheeler
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
mapování	mapování	k1gNnSc1	mapování
americké	americký	k2eAgFnSc2d1	americká
země	zem	k1gFnSc2	zem
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
100	[number]	k4	100
<g/>
.	.	kIx.	.
poledníku	poledník	k1gInSc2	poledník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
náhradník	náhradník	k1gMnSc1	náhradník
za	za	k7c2	za
fotografa	fotograf	k1gMnSc2	fotograf
Timothyho	Timothy	k1gMnSc2	Timothy
H.	H.	kA	H.
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Sullivana	Sullivan	k1gMnSc2	Sullivan
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výpravy	výprava	k1gFnSc2	výprava
pořídil	pořídit	k5eAaPmAgInS	pořídit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
velkoformátových	velkoformátový	k2eAgInPc2d1	velkoformátový
snímků	snímek	k1gInPc2	snímek
a	a	k8xC	a
stereofotografií	stereofotografie	k1gFnPc2	stereofotografie
krajin	krajina	k1gFnPc2	krajina
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
neprozkoumané	prozkoumaný	k2eNgFnSc6d1	neprozkoumaná
oblasti	oblast	k1gFnSc6	oblast
poblíž	poblíž	k7c2	poblíž
řeky	řeka	k1gFnSc2	řeka
Colorado	Colorado	k1gNnSc1	Colorado
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Utah	Utah	k1gInSc1	Utah
a	a	k8xC	a
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
expedice	expedice	k1gFnSc2	expedice
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
se	s	k7c7	s
suchým	suchý	k2eAgInSc7d1	suchý
želatinovým	želatinový	k2eAgInSc7d1	želatinový
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velkým	velký	k2eAgMnSc7d1	velký
odborníkem	odborník	k1gMnSc7	odborník
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
expedici	expedice	k1gFnSc6	expedice
se	se	k3xPyFc4	se
Bell	bell	k1gInSc1	bell
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
filadelfské	filadelfský	k2eAgFnSc6d1	Filadelfská
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
výstavě	výstava	k1gFnSc6	výstava
prodal	prodat	k5eAaPmAgInS	prodat
své	svůj	k3xOyFgNnSc4	svůj
studio	studio	k1gNnSc4	studio
na	na	k7c4	na
Chestnut	Chestnut	k2eAgInSc4d1	Chestnut
Street	Street	k1gInSc4	Street
svému	svůj	k3xOyFgMnSc3	svůj
zeti	zeť	k1gMnSc3	zeť
Williamu	William	k1gInSc2	William
H.	H.	kA	H.
Rauovi	Raua	k1gMnSc6	Raua
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
byl	být	k5eAaImAgInS	být
Bell	bell	k1gInSc1	bell
najat	najmout	k5eAaPmNgInS	najmout
Námořnictvem	námořnictvo	k1gNnSc7	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgNnPc2d1	americké
jako	jako	k8xC	jako
fotograf	fotograf	k1gMnSc1	fotograf
pro	pro	k7c4	pro
expedici	expedice	k1gFnSc4	expedice
zkoumající	zkoumající	k2eAgInSc4d1	zkoumající
přechod	přechod	k1gInSc4	přechod
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Patagonie	Patagonie	k1gFnSc2	Patagonie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
pozorování	pozorování	k1gNnSc1	pozorování
tranzitu	tranzit	k1gInSc2	tranzit
<g/>
,	,	kIx,	,
Bell	bell	k1gInSc1	bell
zachytil	zachytit	k5eAaPmAgInS	zachytit
sérii	série	k1gFnSc4	série
snímků	snímek	k1gInPc2	snímek
brazilské	brazilský	k2eAgFnSc2d1	brazilská
botanické	botanický	k2eAgFnSc2d1	botanická
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bell	bell	k1gInSc1	bell
trávil	trávit	k5eAaImAgInS	trávit
většinu	většina	k1gFnSc4	většina
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
letech	léto	k1gNnPc6	léto
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
psaní	psaní	k1gNnSc6	psaní
odborných	odborný	k2eAgInPc2d1	odborný
článků	článek	k1gInPc2	článek
pro	pro	k7c4	pro
odborné	odborný	k2eAgInPc4d1	odborný
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
například	například	k6eAd1	například
Photographic	Photographice	k1gInPc2	Photographice
Mosaics	Mosaics	k1gInSc4	Mosaics
nebo	nebo	k8xC	nebo
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Photographer	Photographra	k1gFnPc2	Photographra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
fotografovat	fotografovat	k5eAaImF	fotografovat
malby	malba	k1gFnPc4	malba
pro	pro	k7c4	pro
světovou	světový	k2eAgFnSc4d1	světová
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1910	[number]	k4	1910
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
nemoci	nemoc	k1gFnSc6	nemoc
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
na	na	k7c4	na
Boston	Boston	k1gInSc4	Boston
Avenue	avenue	k1gFnSc2	avenue
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
.	.	kIx.	.
<g/>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
zetěm	zeť	k1gMnSc7	zeť
Williamem	William	k1gInSc7	William
Rauem	Rau	k1gMnSc7	Rau
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
zapálenými	zapálený	k2eAgMnPc7d1	zapálený
fotografy	fotograf	k1gMnPc7	fotograf
také	také	k9	také
Bellův	Bellův	k2eAgMnSc1d1	Bellův
syn	syn	k1gMnSc1	syn
Sargent	Sargent	k1gMnSc1	Sargent
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Louisa	Louisa	k?	Louisa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Henry	Henry	k1gMnSc1	Henry
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
rytec	rytec	k1gMnSc1	rytec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
trvala	trvat	k5eAaImAgFnS	trvat
šest	šest	k4xCc4	šest
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
,	,	kIx,	,
Bell	bell	k1gInSc1	bell
pracoval	pracovat	k5eAaImAgInS	pracovat
od	od	k7c2	od
raných	raný	k2eAgInPc2d1	raný
počátků	počátek	k1gInPc2	počátek
fotografických	fotografický	k2eAgInPc2d1	fotografický
procesů	proces	k1gInPc2	proces
-	-	kIx~	-
daguerrotypie	daguerrotypie	k1gFnSc2	daguerrotypie
<g/>
,	,	kIx,	,
mokrého	mokrý	k2eAgInSc2d1	mokrý
kolodiového	kolodiový	k2eAgInSc2d1	kolodiový
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
albuminového	albuminový	k2eAgInSc2d1	albuminový
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
stereofotografie	stereofotografie	k1gFnSc2	stereofotografie
až	až	k8xS	až
do	do	k7c2	do
období	období	k1gNnSc2	období
počátků	počátek	k1gInPc2	počátek
klasického	klasický	k2eAgInSc2d1	klasický
fotografického	fotografický	k2eAgInSc2d1	fotografický
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
průkopníka	průkopník	k1gMnSc4	průkopník
suchého	suchý	k2eAgInSc2d1	suchý
želatinového	želatinový	k2eAgInSc2d1	želatinový
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
diapozitivního	diapozitivní	k2eAgInSc2d1	diapozitivní
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
s	s	k7c7	s
noční	noční	k2eAgFnSc7d1	noční
fotografií	fotografia	k1gFnSc7	fotografia
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
scénu	scéna	k1gFnSc4	scéna
osvětloval	osvětlovat	k5eAaImAgMnS	osvětlovat
zapálením	zapálení	k1gNnSc7	zapálení
hořčíkového	hořčíkový	k2eAgInSc2d1	hořčíkový
drátu	drát	k1gInSc2	drát
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
odborné	odborný	k2eAgInPc4d1	odborný
články	článek	k1gInPc4	článek
o	o	k7c6	o
technice	technika	k1gFnSc6	technika
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
želatinových	želatinový	k2eAgFnPc2d1	želatinová
emulzí	emulze	k1gFnPc2	emulze
využití	využití	k1gNnSc2	využití
pyrogallolu	pyrogallol	k1gInSc2	pyrogallol
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
zlata	zlato	k1gNnSc2	zlato
z	z	k7c2	z
odpadních	odpadní	k2eAgInPc2d1	odpadní
roztoků	roztok	k1gInPc2	roztok
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyvolávání	vyvolávání	k1gNnSc4	vyvolávání
isochromatických	isochromatický	k2eAgFnPc2d1	isochromatický
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
<g/>
Ne	ne	k9	ne
Wheelerově	Wheelerův	k2eAgFnSc6d1	Wheelerova
průzkumné	průzkumný	k2eAgFnSc6d1	průzkumná
expedici	expedice	k1gFnSc6	expedice
používal	používat	k5eAaImAgInS	používat
Bell	bell	k1gInSc1	bell
dva	dva	k4xCgInPc4	dva
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
-	-	kIx~	-
velkoformátový	velkoformátový	k2eAgInSc4d1	velkoformátový
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
desky	deska	k1gFnSc2	deska
28	[number]	k4	28
<g/>
x	x	k?	x
<g/>
20	[number]	k4	20
cm	cm	kA	cm
a	a	k8xC	a
kameru	kamera	k1gFnSc4	kamera
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
20	[number]	k4	20
x	x	k?	x
13	[number]	k4	13
cm	cm	kA	cm
pro	pro	k7c4	pro
stereofotografie	stereofotografie	k1gFnPc4	stereofotografie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
expedicích	expedice	k1gFnPc6	expedice
využíval	využívat	k5eAaImAgInS	využívat
jak	jak	k6eAd1	jak
mokrý	mokrý	k2eAgInSc1d1	mokrý
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
suchý	suchý	k2eAgInSc4d1	suchý
kolodiový	kolodiový	k2eAgInSc4d1	kolodiový
proces	proces	k1gInSc4	proces
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
fotografie	fotografia	k1gFnPc1	fotografia
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
tmavým	tmavý	k2eAgNnSc7d1	tmavé
popředím	popředí	k1gNnSc7	popředí
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
zvyšujících	zvyšující	k2eAgInPc2d1	zvyšující
se	se	k3xPyFc4	se
světlých	světlý	k2eAgInPc2d1	světlý
tónů	tón	k1gInPc2	tón
se	se	k3xPyFc4	se
vzrůstající	vzrůstající	k2eAgFnSc7d1	vzrůstající
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Krajinářské	krajinářský	k2eAgFnPc1d1	krajinářská
fotografie	fotografia	k1gFnPc1	fotografia
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
<g/>
,	,	kIx,	,
Marble	Marble	k1gMnSc1	Marble
Canyon	Canyon	k1gMnSc1	Canyon
<g/>
,	,	kIx,	,
Paria	Paria	k1gFnSc1	Paria
River	River	k1gMnSc1	River
<g/>
,	,	kIx,	,
Mount	Mount	k1gMnSc1	Mount
Nebo	nebo	k8xC	nebo
a	a	k8xC	a
stará	starat	k5eAaImIp3nS	starat
mormonská	mormonský	k2eAgNnPc4d1	mormonské
sídla	sídlo	k1gNnPc4	sídlo
v	v	k7c6	v
Moně	Mona	k1gFnSc6	Mona
v	v	k7c6	v
Utahu	Utah	k1gInSc6	Utah
<g/>
.	.	kIx.	.
<g/>
Bellovy	Bellův	k2eAgFnPc1d1	Bellova
práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
na	na	k7c6	na
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
a	a	k8xC	a
na	na	k7c6	na
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Louisville	Louisvilla	k1gFnSc6	Louisvilla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
a	a	k8xC	a
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
Centennial	Centennial	k1gInSc1	Centennial
Exposition	Exposition	k1gInSc1	Exposition
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
snímky	snímek	k1gInPc1	snímek
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
sbírek	sbírka	k1gFnPc2	sbírka
muzea	muzeum	k1gNnSc2	muzeum
Smithsonian	Smithsoniana	k1gFnPc2	Smithsoniana
American	Americany	k1gInPc2	Americany
Art	Art	k1gFnSc2	Art
Museum	museum	k1gNnSc1	museum
<g/>
,,	,,	k?	,,
National	National	k1gFnSc1	National
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Health	Health	k1gMnSc1	Health
and	and	k?	and
Medicine	Medicin	k1gMnSc5	Medicin
<g/>
,	,	kIx,	,
Oddělení	oddělení	k1gNnSc1	oddělení
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
fotografie	fotografia	k1gFnSc2	fotografia
Knihovny	knihovna	k1gFnSc2	knihovna
Kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
také	také	k9	také
fotografického	fotografický	k2eAgNnSc2d1	fotografické
muzea	muzeum	k1gNnSc2	muzeum
George	Georg	k1gMnSc2	Georg
Eastmana	Eastman	k1gMnSc2	Eastman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
William	William	k1gInSc1	William
Bell	bell	k1gInSc4	bell
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
