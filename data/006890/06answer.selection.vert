<s>
Ptakoještěři	ptakoještěr	k1gMnPc1	ptakoještěr
či	či	k8xC	či
Pterosauři	pterosaurus	k1gMnPc1	pterosaurus
(	(	kIx(	(
<g/>
Pterosauria-	Pterosauria-	k1gMnSc1	Pterosauria-
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
okřídlení	okřídlení	k1gNnSc3	okřídlení
ještěři	ještěr	k1gMnPc1	ještěr
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vyhynulí	vyhynulý	k2eAgMnPc1d1	vyhynulý
druhohorní	druhohorní	k2eAgMnPc1d1	druhohorní
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
první	první	k4xOgMnPc1	první
obratlovci	obratlovec	k1gMnPc1	obratlovec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
aktivního	aktivní	k2eAgInSc2d1	aktivní
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
