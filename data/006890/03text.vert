<s>
Ptakoještěři	ptakoještěr	k1gMnPc1	ptakoještěr
či	či	k8xC	či
Pterosauři	pterosaurus	k1gMnPc1	pterosaurus
(	(	kIx(	(
<g/>
Pterosauria-	Pterosauria-	k1gMnSc1	Pterosauria-
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
okřídlení	okřídlení	k1gNnSc3	okřídlení
ještěři	ještěr	k1gMnPc1	ještěr
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vyhynulí	vyhynulý	k2eAgMnPc1d1	vyhynulý
druhohorní	druhohorní	k2eAgMnPc1d1	druhohorní
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
první	první	k4xOgMnPc1	první
obratlovci	obratlovec	k1gMnPc1	obratlovec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
aktivního	aktivní	k2eAgInSc2d1	aktivní
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jim	on	k3xPp3gFnPc3	on
fylogeneticky	fylogeneticky	k6eAd1	fylogeneticky
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
od	od	k7c2	od
svrchního	svrchní	k2eAgInSc2d1	svrchní
triasu	trias	k1gInSc2	trias
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
220	[number]	k4	220
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
křídy	křída	k1gFnSc2	křída
před	před	k7c7	před
66	[number]	k4	66
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc2	jejich
křídla	křídlo	k1gNnSc2	křídlo
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
kožovitou	kožovitý	k2eAgFnSc7d1	kožovitá
blánou	blána	k1gFnSc7	blána
<g/>
,	,	kIx,	,
napjatou	napjatý	k2eAgFnSc7d1	napjatá
mezi	mezi	k7c7	mezi
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
prodlouženým	prodloužený	k2eAgInSc7d1	prodloužený
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
prstem	prst	k1gInSc7	prst
<g/>
.	.	kIx.	.
</s>
<s>
Ptakoještěři	ptakoještěr	k1gMnPc1	ptakoještěr
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
mylně	mylně	k6eAd1	mylně
zahrnováni	zahrnovat	k5eAaImNgMnP	zahrnovat
mezi	mezi	k7c7	mezi
dinosaury	dinosaurus	k1gMnPc7	dinosaurus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
krokodýly	krokodýl	k1gMnPc7	krokodýl
pouze	pouze	k6eAd1	pouze
jejich	jejich	k3xOp3gFnSc7	jejich
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
vývojovou	vývojový	k2eAgFnSc7d1	vývojová
skupinou	skupina	k1gFnSc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
bývají	bývat	k5eAaImIp3nP	bývat
laickou	laický	k2eAgFnSc7d1	laická
veřejností	veřejnost	k1gFnSc7	veřejnost
stále	stále	k6eAd1	stále
často	často	k6eAd1	často
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
dinosaury	dinosaurus	k1gMnPc4	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
objevený	objevený	k2eAgMnSc1d1	objevený
ptakoještěr	ptakoještěr	k1gMnSc1	ptakoještěr
byl	být	k5eAaImAgMnS	být
popsán	popsat	k5eAaPmNgMnS	popsat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnSc2	některý
nekompletní	kompletní	k2eNgFnSc2d1	nekompletní
kostry	kostra	k1gFnSc2	kostra
však	však	k9	však
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgFnPc1d1	známa
již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1757	[number]	k4	1757
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
dodnes	dodnes	k6eAd1	dodnes
objeveno	objevit	k5eAaPmNgNnS	objevit
přes	přes	k7c4	přes
120	[number]	k4	120
platných	platný	k2eAgInPc2d1	platný
rodů	rod	k1gInPc2	rod
těchto	tento	k3xDgMnPc2	tento
létajících	létající	k2eAgMnPc2d1	létající
pravěkých	pravěký	k2eAgMnPc2d1	pravěký
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pterosauři	pterosaurus	k1gMnPc1	pterosaurus
představovali	představovat	k5eAaImAgMnP	představovat
bezpochyby	bezpochyby	k6eAd1	bezpochyby
největší	veliký	k2eAgMnPc4d3	veliký
létající	létající	k2eAgMnPc4d1	létající
živočichy	živočich	k1gMnPc4	živočich
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gMnSc1	druh
Quetzalcoatlus	Quetzalcoatlus	k1gMnSc1	Quetzalcoatlus
northropi	northropi	k6eAd1	northropi
ze	z	k7c2	z
svrchní	svrchní	k2eAgFnSc2d1	svrchní
křídy	křída	k1gFnSc2	křída
Texasu	Texas	k1gInSc2	Texas
mohl	moct	k5eAaImAgMnS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rozpětí	rozpětí	k1gNnSc4	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
12-15	[number]	k4	12-15
(	(	kIx(	(
<g/>
v	v	k7c6	v
extrémním	extrémní	k2eAgInSc6d1	extrémní
případě	případ	k1gInSc6	případ
snad	snad	k9	snad
dokonce	dokonce	k9	dokonce
18	[number]	k4	18
<g/>
)	)	kIx)	)
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
středně	středně	k6eAd1	středně
velkému	velký	k2eAgNnSc3d1	velké
turistickému	turistický	k2eAgNnSc3d1	turistické
letadlu	letadlo	k1gNnSc3	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
činila	činit	k5eAaImAgFnS	činit
zřejmě	zřejmě	k6eAd1	zřejmě
jen	jen	k9	jen
kolem	kolem	k7c2	kolem
130-250	[number]	k4	130-250
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Podobných	podobný	k2eAgInPc2d1	podobný
rozměrů	rozměr	k1gInPc2	rozměr
dosahovali	dosahovat	k5eAaImAgMnP	dosahovat
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
jiní	jiný	k2eAgMnPc1d1	jiný
pterosauři	pterosaurus	k1gMnPc1	pterosaurus
(	(	kIx(	(
<g/>
Hatzegopteryx	Hatzegopteryx	k1gInSc1	Hatzegopteryx
<g/>
,	,	kIx,	,
Arambourgiania	Arambourgianium	k1gNnPc1	Arambourgianium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
pterosauři	pterosaurus	k1gMnPc1	pterosaurus
byli	být	k5eAaImAgMnP	být
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
drobní	drobný	k2eAgMnPc1d1	drobný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gNnSc2	jejich
rozpětí	rozpětí	k1gNnSc2	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
nepřesahovalo	přesahovat	k5eNaImAgNnS	přesahovat
25	[number]	k4	25
cm	cm	kA	cm
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
čínský	čínský	k2eAgInSc1d1	čínský
rod	rod	k1gInSc1	rod
Nemicolopterus	Nemicolopterus	k1gInSc1	Nemicolopterus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
viz	vidět	k5eAaImRp2nS	vidět
také	také	k6eAd1	také
"	"	kIx"	"
<g/>
Velikost	velikost	k1gFnSc1	velikost
ptakoještěrů	ptakoještěr	k1gMnPc2	ptakoještěr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
byli	být	k5eAaImAgMnP	být
i	i	k9	i
pterosauři	pterosaurus	k1gMnPc1	pterosaurus
téměř	téměř	k6eAd1	téměř
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
teplokrevní	teplokrevný	k2eAgMnPc1d1	teplokrevný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
druh	druh	k1gMnSc1	druh
Sordes	Sordes	k1gMnSc1	Sordes
pilosus	pilosus	k1gMnSc1	pilosus
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgNnP	být
objevena	objeven	k2eAgFnSc1d1	objevena
i	i	k8xC	i
jakási	jakýsi	k3yIgFnSc1	jakýsi
primitivní	primitivní	k2eAgFnSc1d1	primitivní
srst	srst	k1gFnSc1	srst
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
exempláře	exemplář	k1gInSc2	exemplář
nalezeného	nalezený	k2eAgInSc2d1	nalezený
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
byly	být	k5eAaImAgFnP	být
dokonce	dokonce	k9	dokonce
objeveny	objeven	k2eAgInPc4d1	objeven
náznaky	náznak	k1gInPc4	náznak
jakéhosi	jakýsi	k3yIgNnSc2	jakýsi
proto-peří	protoeří	k1gNnSc2	proto-peří
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncem	koncem	k7c2	koncem
křídy	křída	k1gFnSc2	křída
již	již	k6eAd1	již
byli	být	k5eAaImAgMnP	být
ptakoještěři	ptakoještěr	k1gMnPc1	ptakoještěr
vytlačováni	vytlačovat	k5eAaImNgMnP	vytlačovat
konkurenčními	konkurenční	k2eAgMnPc7d1	konkurenční
létajícími	létající	k2eAgMnPc7d1	létající
obratlovci	obratlovec	k1gMnPc7	obratlovec
-	-	kIx~	-
ptáky	pták	k1gMnPc7	pták
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
ptakoještěři	ptakoještěr	k1gMnPc1	ptakoještěr
dosahovali	dosahovat	k5eAaImAgMnP	dosahovat
rozpětí	rozpětí	k1gNnSc4	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
přes	přes	k7c4	přes
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
zřejmě	zřejmě	k6eAd1	zřejmě
vykonávat	vykonávat	k5eAaImF	vykonávat
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
poutě	pouť	k1gFnPc1	pouť
na	na	k7c4	na
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
tisíců	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
zřejmě	zřejmě	k6eAd1	zřejmě
dosahovali	dosahovat	k5eAaImAgMnP	dosahovat
rychlostí	rychlost	k1gFnSc7	rychlost
i	i	k8xC	i
přes	přes	k7c4	přes
120	[number]	k4	120
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
dostávali	dostávat	k5eAaImAgMnP	dostávat
rychlým	rychlý	k2eAgInSc7d1	rychlý
výskokem	výskok	k1gInSc7	výskok
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Objevy	objev	k1gInPc1	objev
některých	některý	k3yIgMnPc2	některý
druhohorních	druhohorní	k2eAgMnPc2d1	druhohorní
parazitů	parazit	k1gMnPc2	parazit
podobných	podobný	k2eAgFnPc2d1	podobná
blechám	blecha	k1gFnPc3	blecha
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jurský	jurský	k2eAgInSc1d1	jurský
a	a	k8xC	a
křídový	křídový	k2eAgInSc1d1	křídový
hmyz	hmyz	k1gInSc1	hmyz
se	se	k3xPyFc4	se
evolučně	evolučně	k6eAd1	evolučně
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
k	k	k7c3	k
sání	sání	k1gNnSc3	sání
krve	krev	k1gFnSc2	krev
teplokrevných	teplokrevný	k2eAgMnPc2d1	teplokrevný
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
i	i	k8xC	i
ptakoještěrů	ptakoještěr	k1gMnPc2	ptakoještěr
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
rody	rod	k1gInPc1	rod
Pseudopulex	Pseudopulex	k1gInSc1	Pseudopulex
<g/>
,	,	kIx,	,
Tarwinia	Tarwinium	k1gNnPc1	Tarwinium
a	a	k8xC	a
Saurophthirus	Saurophthirus	k1gInSc1	Saurophthirus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
objevy	objev	k1gInPc1	objev
ptakoještěrů	ptakoještěr	k1gMnPc2	ptakoještěr
pocházejí	pocházet	k5eAaImIp3nP	pocházet
již	již	k9	již
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgMnSc7	první
objeveným	objevený	k2eAgMnSc7d1	objevený
ptakoještěrem	ptakoještěr	k1gMnSc7	ptakoještěr
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Pester	Pester	k1gMnSc1	Pester
Exemplar	Exemplar	k1gMnSc1	Exemplar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
řazený	řazený	k2eAgInSc1d1	řazený
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Aurorazhdarcho	Aurorazhdarcha	k1gMnSc5	Aurorazhdarcha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
svrchnojurský	svrchnojurský	k2eAgMnSc1d1	svrchnojurský
pterosaur	pterosaur	k1gMnSc1	pterosaur
malých	malý	k2eAgInPc2d1	malý
rozměrů	rozměr	k1gInPc2	rozměr
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
bavorském	bavorský	k2eAgInSc6d1	bavorský
litografickém	litografický	k2eAgInSc6d1	litografický
vápenci	vápenec	k1gInSc6	vápenec
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1757	[number]	k4	1757
a	a	k8xC	a
1779	[number]	k4	1779
a	a	k8xC	a
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
sbírky	sbírka	k1gFnSc2	sbírka
arcivévodkyně	arcivévodkyně	k1gFnSc2	arcivévodkyně
Marie	Maria	k1gFnSc2	Maria
Anny	Anna	k1gFnSc2	Anna
Habsbursko-Lotrinské	habsburskootrinský	k2eAgFnSc2d1	habsbursko-lotrinská
(	(	kIx(	(
<g/>
dcery	dcera	k1gFnSc2	dcera
panovnice	panovnice	k1gFnSc2	panovnice
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
formálně	formálně	k6eAd1	formálně
popsaným	popsaný	k2eAgMnSc7d1	popsaný
pterosaurem	pterosaurus	k1gMnSc7	pterosaurus
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
Pterodactylus	Pterodactylus	k1gMnSc1	Pterodactylus
antiquus	antiquus	k1gMnSc1	antiquus
<g/>
,	,	kIx,	,
vědecky	vědecky	k6eAd1	vědecky
popsaný	popsaný	k2eAgInSc1d1	popsaný
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
Cosimou	Cosima	k1gFnSc7	Cosima
A.	A.	kA	A.
Collinim	Collinim	k1gMnSc1	Collinim
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
území	území	k1gNnSc2	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
známe	znát	k5eAaImIp1nP	znát
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jediný	jediný	k2eAgInSc4d1	jediný
druh	druh	k1gInSc4	druh
našeho	náš	k3xOp1gMnSc2	náš
ptakoještěra	ptakoještěr	k1gMnSc2	ptakoještěr
<g/>
,	,	kIx,	,
objeveného	objevený	k2eAgInSc2d1	objevený
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
u	u	k7c2	u
Chocně	Choceň	k1gFnSc2	Choceň
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
o	o	k7c4	o
mládě	mládě	k1gNnSc4	mládě
azdarchidního	azdarchidní	k2eAgMnSc2d1	azdarchidní
pterosaura	pterosaurus	k1gMnSc2	pterosaurus
<g/>
,	,	kIx,	,
žijícího	žijící	k2eAgMnSc2d1	žijící
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
92	[number]	k4	92
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
mládě	mládě	k1gNnSc4	mládě
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
asi	asi	k9	asi
1,5	[number]	k4	1,5
-	-	kIx~	-
1,6	[number]	k4	1,6
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
mohl	moct	k5eAaImAgMnS	moct
mít	mít	k5eAaImF	mít
tento	tento	k3xDgInSc4	tento
létající	létající	k2eAgInSc4d1	létající
plaz	plaz	k1gInSc4	plaz
rozpětí	rozpětí	k1gNnSc2	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
až	až	k9	až
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
nepatřil	patřit	k5eNaImAgInS	patřit
ale	ale	k9	ale
k	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
zástupcům	zástupce	k1gMnPc3	zástupce
své	svůj	k3xOyFgFnSc2	svůj
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Drobné	drobný	k2eAgInPc4d1	drobný
zuby	zub	k1gInPc4	zub
možných	možný	k2eAgMnPc2d1	možný
rybožravých	rybožravý	k2eAgMnPc2d1	rybožravý
ptakoještěrů	ptakoještěr	k1gMnPc2	ptakoještěr
byly	být	k5eAaImAgFnP	být
odkryty	odkrýt	k5eAaPmNgFnP	odkrýt
také	také	k9	také
v	v	k7c6	v
cenomanských	cenomanský	k2eAgFnPc6d1	cenomanská
a	a	k8xC	a
turonských	turonský	k2eAgFnPc6d1	turonský
vrstvách	vrstva	k1gFnPc6	vrstva
(	(	kIx(	(
<g/>
raná	raný	k2eAgFnSc1d1	raná
svrchní	svrchní	k2eAgFnSc1d1	svrchní
křída	křída	k1gFnSc1	křída
<g/>
)	)	kIx)	)
v	v	k7c6	v
lomu	lom	k1gInSc6	lom
v	v	k7c6	v
Úpohlavech	Úpohlav	k1gInPc6	Úpohlav
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
a	a	k8xC	a
mnohé	mnohé	k1gNnSc1	mnohé
další	další	k2eAgFnSc2d1	další
skupiny	skupina	k1gFnSc2	skupina
živočichů	živočich	k1gMnPc2	živočich
vyhynuli	vyhynout	k5eAaPmAgMnP	vyhynout
ptakoještěři	ptakoještěr	k1gMnPc1	ptakoještěr
na	na	k7c6	na
konci	konec	k1gInSc6	konec
křídové	křídový	k2eAgFnSc2d1	křídová
periody	perioda	k1gFnSc2	perioda
při	při	k7c6	při
velkém	velký	k2eAgNnSc6d1	velké
hromadném	hromadný	k2eAgNnSc6d1	hromadné
vymírání	vymírání	k1gNnSc6	vymírání
K-Pg	K-Pga	k1gFnPc2	K-Pga
(	(	kIx(	(
<g/>
před	před	k7c7	před
66	[number]	k4	66
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ptakoještěři	ptakoještěr	k1gMnPc1	ptakoještěr
vymírali	vymírat	k5eAaImAgMnP	vymírat
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
křídy	křída	k1gFnSc2	křída
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
vymírání	vymírání	k1gNnSc1	vymírání
souviselo	souviset	k5eAaImAgNnS	souviset
s	s	k7c7	s
ekologickou	ekologický	k2eAgFnSc7d1	ekologická
kompeticí	kompetice	k1gFnSc7	kompetice
s	s	k7c7	s
ptáky	pták	k1gMnPc7	pták
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
však	však	k9	však
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokles	pokles	k1gInSc1	pokles
druhové	druhový	k2eAgFnSc2d1	druhová
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
ptakoještěrů	ptakoještěr	k1gMnPc2	ptakoještěr
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
ptáků	pták	k1gMnPc2	pták
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nesouvisel	souviset	k5eNaImAgMnS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Biodiverzita	biodiverzita	k1gFnSc1	biodiverzita
pterosaurů	pterosaurus	k1gMnPc2	pterosaurus
na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
konci	konec	k1gInSc6	konec
křídy	křída	k1gFnSc2	křída
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
menší	malý	k2eAgFnSc1d2	menší
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
existovaly	existovat	k5eAaImAgInP	existovat
spíše	spíše	k9	spíše
jen	jen	k6eAd1	jen
obří	obří	k2eAgInPc1d1	obří
specializované	specializovaný	k2eAgInPc1d1	specializovaný
druhy	druh	k1gInPc1	druh
azdarchoidů	azdarchoid	k1gInPc2	azdarchoid
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
některé	některý	k3yIgInPc4	některý
objevy	objev	k1gInPc4	objev
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
stále	stále	k6eAd1	stále
existovaly	existovat	k5eAaImAgInP	existovat
i	i	k9	i
menší	malý	k2eAgInPc1d2	menší
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
