<s>
Athény	Athéna	k1gFnPc1	Athéna
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
přístav	přístav	k1gInSc4	přístav
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
linky	linka	k1gFnPc4	linka
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
části	část	k1gFnPc4	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dopravu	doprava	k1gFnSc4	doprava
například	například	k6eAd1	například
i	i	k9	i
k	k	k7c3	k
přístavu	přístav	k1gInSc3	přístav
Pireus	Pireus	k1gMnSc1	Pireus
<g/>
.	.	kIx.	.
</s>
