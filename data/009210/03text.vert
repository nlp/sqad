<p>
<s>
Patagonie	Patagonie	k1gFnSc1	Patagonie
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
území	území	k1gNnSc4	území
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
ji	on	k3xPp3gFnSc4	on
ohraničují	ohraničovat	k5eAaImIp3nP	ohraničovat
Andy	Anda	k1gFnSc2	Anda
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
řeka	řeka	k1gFnSc1	řeka
Río	Río	k1gFnSc2	Río
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Patagonie	Patagonie	k1gFnSc1	Patagonie
náleží	náležet	k5eAaImIp3nS	náležet
Argentině	Argentina	k1gFnSc3	Argentina
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Patagonie	Patagonie	k1gFnSc2	Patagonie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
souostroví	souostroví	k1gNnSc3	souostroví
Ohňová	ohňový	k2eAgFnSc1d1	ohňová
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnPc1	podnebí
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
a	a	k8xC	a
straně	strana	k1gFnSc6	strana
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
lidské	lidský	k2eAgNnSc4d1	lidské
osídlení	osídlení	k1gNnSc4	osídlení
převážně	převážně	k6eAd1	převážně
nehostinné	hostinný	k2eNgFnSc2d1	nehostinná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Západní	západní	k2eAgFnSc3d1	západní
(	(	kIx(	(
<g/>
chilská	chilský	k2eAgFnSc1d1	chilská
<g/>
)	)	kIx)	)
Patagonie	Patagonie	k1gFnSc1	Patagonie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
chladné	chladný	k2eAgNnSc1d1	chladné
a	a	k8xC	a
extrémně	extrémně	k6eAd1	extrémně
vlhké	vlhký	k2eAgNnSc4d1	vlhké
podnebí	podnebí	k1gNnSc4	podnebí
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
srážkovými	srážkový	k2eAgInPc7d1	srážkový
úhrny	úhrn	k1gInPc7	úhrn
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
studeného	studený	k2eAgInSc2d1	studený
Humboldtova	Humboldtův	k2eAgInSc2d1	Humboldtův
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Nenachází	nacházet	k5eNaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
žádné	žádný	k3yNgFnPc1	žádný
rozsáhlejší	rozsáhlý	k2eAgFnPc1d2	rozsáhlejší
nížiny	nížina	k1gFnPc1	nížina
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc1	hora
stoupají	stoupat	k5eAaImIp3nP	stoupat
strmě	strmě	k6eAd1	strmě
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
rozdrobené	rozdrobený	k2eAgNnSc1d1	rozdrobené
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ledovcové	ledovcový	k2eAgFnSc2d1	ledovcová
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
je	být	k5eAaImIp3nS	být
glaciální	glaciální	k2eAgInSc1d1	glaciální
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
ledovců	ledovec	k1gInPc2	ledovec
(	(	kIx(	(
<g/>
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
ledovce	ledovec	k1gInPc1	ledovec
Jihopatagonského	Jihopatagonský	k2eAgNnSc2d1	Jihopatagonský
ledovcového	ledovcový	k2eAgNnSc2d1	ledovcové
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
Severopatagonského	Severopatagonský	k2eAgNnSc2d1	Severopatagonský
ledovcového	ledovcový	k2eAgNnSc2d1	ledovcové
pole	pole	k1gNnSc2	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
místy	místy	k6eAd1	místy
až	až	k9	až
k	k	k7c3	k
mořské	mořský	k2eAgFnSc3d1	mořská
hladině	hladina	k1gFnSc3	hladina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
pleistocénu	pleistocén	k1gInSc2	pleistocén
byl	být	k5eAaImAgInS	být
rozsah	rozsah	k1gInSc1	rozsah
zalednění	zalednění	k1gNnSc1	zalednění
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgMnSc1d2	veliký
a	a	k8xC	a
ledovce	ledovec	k1gInPc1	ledovec
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
hluboká	hluboký	k2eAgNnPc1d1	hluboké
údolí	údolí	k1gNnPc1	údolí
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zaplavena	zaplaven	k2eAgNnPc1d1	zaplaveno
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k6eAd1	tak
hluboké	hluboký	k2eAgInPc1d1	hluboký
zálivy	záliv	k1gInPc1	záliv
zvané	zvaný	k2eAgInPc1d1	zvaný
fjordy	fjord	k1gInPc1	fjord
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
osad	osada	k1gFnPc2	osada
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Chaitén	Chaitén	k1gInSc4	Chaitén
<g/>
,	,	kIx,	,
Aysén	Aysén	k1gInSc4	Aysén
<g/>
,	,	kIx,	,
Coyhaique	Coyhaiqu	k1gInPc4	Coyhaiqu
a	a	k8xC	a
Punta	punto	k1gNnPc4	punto
Arenas	Arenasa	k1gFnPc2	Arenasa
<g/>
.	.	kIx.	.
</s>
<s>
Coyhaique	Coyhaique	k6eAd1	Coyhaique
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
částečně	částečně	k6eAd1	částečně
chráněném	chráněný	k2eAgNnSc6d1	chráněné
hřebeny	hřeben	k1gInPc7	hřeben
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
srážkové	srážkový	k2eAgInPc1d1	srážkový
úhrny	úhrn	k1gInPc1	úhrn
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
nižší	nízký	k2eAgFnPc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Punta	punto	k1gNnPc4	punto
Arenas	Arenas	k1gInSc1	Arenas
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Magalhã	Magalhã	k1gFnSc2	Magalhã
průlivu	průliv	k1gInSc2	průliv
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
chráněno	chránit	k5eAaImNgNnS	chránit
před	před	k7c7	před
srážkami	srážka	k1gFnPc7	srážka
hřebeny	hřeben	k1gInPc4	hřeben
And	Anda	k1gFnPc2	Anda
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východní	východní	k2eAgFnSc1d1	východní
(	(	kIx(	(
<g/>
argentinská	argentinský	k2eAgFnSc1d1	Argentinská
<g/>
)	)	kIx)	)
Patagonie	Patagonie	k1gFnSc1	Patagonie
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
suchým	suchý	k2eAgMnSc7d1	suchý
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
teplejším	teplý	k2eAgNnSc7d2	teplejší
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
blokovány	blokovat	k5eAaImNgFnP	blokovat
Andami	Anda	k1gFnPc7	Anda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
nachází	nacházet	k5eAaImIp3nS	nacházet
step	step	k1gFnSc1	step
či	či	k8xC	či
polopoušť	polopoušť	k1gFnSc1	polopoušť
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
protínají	protínat	k5eAaImIp3nP	protínat
řeky	řeka	k1gFnPc1	řeka
stékající	stékající	k2eAgFnPc1d1	stékající
z	z	k7c2	z
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
neúrodná	úrodný	k2eNgFnSc1d1	neúrodná
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
celé	celý	k2eAgFnSc2d1	celá
Patagonie	Patagonie	k1gFnSc2	Patagonie
je	být	k5eAaImIp3nS	být
Neuquén	Neuquén	k1gInSc1	Neuquén
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
severu	sever	k1gInSc6	sever
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
Patagonie	Patagonie	k1gFnSc2	Patagonie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
265	[number]	k4	265
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
města	město	k1gNnPc4	město
patří	patřit	k5eAaImIp3nS	patřit
Comodoro	Comodora	k1gFnSc5	Comodora
Rivadavia	Rivadavium	k1gNnSc2	Rivadavium
<g/>
,	,	kIx,	,
Río	Río	k1gMnSc1	Río
Gallegos	Gallegos	k1gMnSc1	Gallegos
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Carlos	Carlos	k1gMnSc1	Carlos
de	de	k?	de
Bariloche	Bariloch	k1gFnSc2	Bariloch
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
And	Anda	k1gFnPc2	Anda
a	a	k8xC	a
Ushuaia	Ushuaia	k1gFnSc1	Ushuaia
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
jihu	jih	k1gInSc6	jih
ostrova	ostrov	k1gInSc2	ostrov
Isla	Isla	k1gMnSc1	Isla
Grande	grand	k1gMnSc5	grand
de	de	k?	de
Tierra	Tierra	k1gMnSc1	Tierra
del	del	k?	del
Fuego	Fuego	k1gMnSc1	Fuego
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Argentinská	argentinský	k2eAgFnSc1d1	Argentinská
část	část	k1gFnSc1	část
Patagonie	Patagonie	k1gFnSc2	Patagonie
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
765	[number]	k4	765
720	[number]	k4	720
km2	km2	k4	km2
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
pěti	pět	k4xCc6	pět
provinciích	provincie	k1gFnPc6	provincie
(	(	kIx(	(
<g/>
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Neuquén	Neuquén	k1gInSc1	Neuquén
</s>
</p>
<p>
<s>
Río	Río	k?	Río
Negro	Negro	k1gNnSc1	Negro
</s>
</p>
<p>
<s>
Chubut	Chubut	k1gMnSc1	Chubut
</s>
</p>
<p>
<s>
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
</s>
</p>
<p>
<s>
Tierra	Tierra	k1gFnSc1	Tierra
del	del	k?	del
FuegoChilská	FuegoChilský	k2eAgFnSc1d1	FuegoChilský
část	část	k1gFnSc1	část
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
regionů	region	k1gInPc2	region
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
Ríos	Ríos	k1gInSc1	Ríos
s	s	k7c7	s
provinciemi	provincie	k1gFnPc7	provincie
Valdivia	Valdivia	k1gFnSc1	Valdivia
a	a	k8xC	a
Ranco	Ranco	k1gNnSc1	Ranco
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
Lagos	Lagos	k1gInSc1	Lagos
s	s	k7c7	s
provinciemi	provincie	k1gFnPc7	provincie
Chiloé	Chiloá	k1gFnSc2	Chiloá
<g/>
,	,	kIx,	,
Llanquihue	Llanquihue	k1gNnSc1	Llanquihue
<g/>
,	,	kIx,	,
Osorno	Osorno	k1gNnSc1	Osorno
a	a	k8xC	a
Palena	Palena	k1gFnSc1	Palena
</s>
</p>
<p>
<s>
Aysén	Aysén	k1gInSc1	Aysén
s	s	k7c7	s
provinciemi	provincie	k1gFnPc7	provincie
Aysén	Aysén	k1gInSc1	Aysén
<g/>
,	,	kIx,	,
Capitán	Capitán	k2eAgInSc1d1	Capitán
Prat	Prat	k1gInSc1	Prat
<g/>
,	,	kIx,	,
Coihaique	Coihaique	k1gFnSc1	Coihaique
a	a	k8xC	a
General	General	k1gFnSc1	General
Carrera	Carrera	k1gFnSc1	Carrera
</s>
</p>
<p>
<s>
Magallanes	Magallanes	k1gInSc1	Magallanes
a	a	k8xC	a
Chilská	chilský	k2eAgFnSc1d1	chilská
Antarktida	Antarktida	k1gFnSc1	Antarktida
s	s	k7c7	s
provinciemi	provincie	k1gFnPc7	provincie
Magallanes	Magallanes	k1gMnSc1	Magallanes
<g/>
,	,	kIx,	,
Tierra	Tierra	k1gMnSc1	Tierra
del	del	k?	del
Fuego	Fuego	k1gMnSc1	Fuego
<g/>
,	,	kIx,	,
Última	Última	k1gFnSc1	Última
Esperanza	Esperanza	k1gFnSc1	Esperanza
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Patagonská	patagonský	k2eAgFnSc1d1	patagonská
poušť	poušť	k1gFnSc1	poušť
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Patagonie	Patagonie	k1gFnSc2	Patagonie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Patagonie	Patagonie	k1gFnSc2	Patagonie
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Patagonie	Patagonie	k1gFnSc2	Patagonie
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
http://cestovani.idnes.cz/kde-lezi-prava-patagonie-08d-/igsvet.asp?c=A061008_184532_igsvet_tom	[url]	k1gInSc1	http://cestovani.idnes.cz/kde-lezi-prava-patagonie-08d-/igsvet.asp?c=A061008_184532_igsvet_tom
</s>
</p>
