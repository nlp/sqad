<s>
V	v	k7c6	v
nejvýznamnějším	významný	k2eAgNnSc6d3	nejvýznamnější
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
výzdoba	výzdoba	k1gFnSc1	výzdoba
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
českého	český	k2eAgNnSc2d1	české
vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
stálé	stálý	k2eAgInPc4d1	stálý
soubory	soubor	k1gInPc4	soubor
činohry	činohra	k1gFnSc2	činohra
<g/>
,	,	kIx,	,
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
<g/>
;	;	kIx,	;
ty	ten	k3xDgInPc1	ten
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
střídavě	střídavě	k6eAd1	střídavě
také	také	k9	také
v	v	k7c6	v
klasicistním	klasicistní	k2eAgNnSc6d1	klasicistní
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
premiéry	premiéra	k1gFnPc1	premiéra
Mozartových	Mozartových	k2eAgFnPc2d1	Mozartových
oper	opera	k1gFnPc2	opera
Don	dona	k1gFnPc2	dona
Giovanni	Giovann	k1gMnPc1	Giovann
a	a	k8xC	a
La	la	k1gNnSc1	la
clemenza	clemenz	k1gMnSc2	clemenz
di	di	k?	di
Tito	tento	k3xDgMnPc1	tento
<g/>
.	.	kIx.	.
</s>
