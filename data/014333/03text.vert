<s>
Čertova	čertův	k2eAgFnSc1d1
ruka	ruka	k1gFnSc1
(	(	kIx(
<g/>
hrad	hrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Čertova	čertův	k2eAgFnSc1d1
ruka	ruka	k1gFnSc1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Zánik	zánik	k1gInSc1
</s>
<s>
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Stavebník	stavebník	k1gMnSc1
</s>
<s>
neznámý	známý	k2eNgMnSc1d1
Další	další	k2eAgMnSc1d1
majitelé	majitel	k1gMnPc1
</s>
<s>
neznámý	známý	k2eNgMnSc1d1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Šourkova	Šourkův	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
,	,	kIx,
Hruboskalské	hruboskalský	k2eAgNnSc1d1
skalní	skalní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
,	,	kIx,
Sedmihorky	Sedmihorky	k?
<g/>
,	,	kIx,
Karlovice	Karlovice	k1gFnPc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
18,2	18,2	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
33,52	33,52	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Čertova	čertův	k2eAgFnSc1d1
ruka	ruka	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hrad	hrad	k1gInSc1
Čertova	čertův	k2eAgFnSc1d1
ruka	ruka	k1gFnSc1
stával	stávat	k5eAaImAgInS
v	v	k7c6
pískovcových	pískovcový	k2eAgFnPc6d1
skalách	skála	k1gFnPc6
Hruboskalského	hruboskalský	k2eAgNnSc2d1
skalního	skalní	k2eAgNnSc2d1
města	město	k1gNnSc2
nedaleko	nedaleko	k7c2
Sedmihorek	Sedmihorka	k1gFnPc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
okres	okres	k1gInSc1
Semily	Semily	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Místo	místo	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
hrad	hrad	k1gInSc1
stával	stávat	k5eAaImAgInS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
osídleno	osídlit	k5eAaPmNgNnS
již	již	k6eAd1
od	od	k7c2
eneolitu	eneolit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
hradě	hrad	k1gInSc6
neexistují	existovat	k5eNaImIp3nP
žádné	žádný	k3yNgInPc1
písemné	písemný	k2eAgInPc1d1
prameny	pramen	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
podle	podle	k7c2
archeologických	archeologický	k2eAgInPc2d1
nálezů	nález	k1gInPc2
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
snad	snad	k9
ve	v	k7c6
třináctém	třináctý	k4xOgNnSc6
století	století	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podoba	podoba	k1gFnSc1
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
malý	malý	k2eAgInSc4d1
dřevěný	dřevěný	k2eAgInSc4d1
hrad	hrad	k1gInSc4
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
skalního	skalní	k2eAgInSc2d1
útvaru	útvar	k1gInSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Šourkovy	Šourkův	k2eAgFnSc2d1
věže	věž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachovala	Zachoval	k1gMnSc4
se	se	k3xPyFc4
pouze	pouze	k6eAd1
cisterna	cisterna	k1gFnSc1
vysekaná	vysekaný	k2eAgFnSc1d1
do	do	k7c2
skály	skála	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
zbytky	zbytek	k1gInPc1
přístupové	přístupový	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
<g/>
,	,	kIx,
záseky	záseka	k1gFnPc4
po	po	k7c6
schodištích	schodiště	k1gNnPc6
a	a	k8xC
vysekané	vysekaný	k2eAgInPc4d1
otvory	otvor	k1gInPc4
pro	pro	k7c4
ukotvení	ukotvení	k1gNnSc4
trámů	trám	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dostupnost	dostupnost	k1gFnSc1
</s>
<s>
Přímo	přímo	k6eAd1
ke	k	k7c3
skalnímu	skalní	k2eAgInSc3d1
hradu	hrad	k1gInSc3
žádná	žádný	k3yNgFnSc1
značená	značený	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
nevede	vést	k5eNaImIp3nS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
vedou	vést	k5eAaImIp3nP
hned	hned	k6eAd1
dvě	dva	k4xCgFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejblíže	blízce	k6eAd3
prochází	procházet	k5eAaImIp3nS
modrá	modrý	k2eAgFnSc1d1
značka	značka	k1gFnSc1
od	od	k7c2
rozcestí	rozcestí	k1gNnSc2
pod	pod	k7c7
hradem	hrad	k1gInSc7
Kavčiny	kavčin	k2eAgFnSc2d1
a	a	k8xC
pokračující	pokračující	k2eAgFnSc2d1
do	do	k7c2
Sedmihorek	Sedmihorka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
něco	něco	k3yInSc4
vzdálenější	vzdálený	k2eAgMnSc1d2
je	být	k5eAaImIp3nS
žlutá	žlutý	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
kopírující	kopírující	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
od	od	k7c2
hradu	hrad	k1gInSc2
Valdštejn	Valdštejna	k1gFnPc2
a	a	k8xC
od	od	k7c2
Smíchousova	Smíchousův	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
pokračující	pokračující	k2eAgFnSc1d1
k	k	k7c3
zámku	zámek	k1gInSc3
Hrubá	hrubý	k2eAgFnSc1d1
Skála	skála	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souběžně	souběžně	k6eAd1
s	s	k7c7
modrou	modrý	k2eAgFnSc7d1
značkou	značka	k1gFnSc7
vede	vést	k5eAaImIp3nS
také	také	k6eAd1
NS	NS	kA
Hruboskalsko	Hruboskalsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Chlum-Kozlov	Chlum-Kozlov	k1gInSc1
</s>
<s>
Hrubá	hrubý	k2eAgFnSc1d1
Skála	skála	k1gFnSc1
(	(	kIx(
<g/>
zámek	zámek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kavčiny	kavčin	k2eAgFnPc1d1
(	(	kIx(
<g/>
hrad	hrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Radeč	Radeč	k1gInSc1
(	(	kIx(
<g/>
hrad	hrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Valdštejn	Valdštejn	k1gInSc1
(	(	kIx(
<g/>
hrad	hrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
DURDÍK	DURDÍK	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrovaná	ilustrovaný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
českých	český	k2eAgInPc2d1
hradů	hrad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
736	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
Čertova	čertův	k2eAgFnSc1d1
ruka	ruka	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
96	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
hradů	hrad	k1gInPc2
v	v	k7c6
Libereckém	liberecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
hrad	hrad	k1gInSc1
na	na	k7c4
hrady	hrad	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
