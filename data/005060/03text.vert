<s>
Autorun	Autorun	k1gNnSc1	Autorun
<g/>
.	.	kIx.	.
<g/>
inf	inf	k?	inf
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
konfiguračního	konfigurační	k2eAgInSc2d1	konfigurační
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využíván	využívat	k5eAaImNgInS	využívat
funkcemi	funkce	k1gFnPc7	funkce
AutoRun	AutoRuna	k1gFnPc2	AutoRuna
a	a	k8xC	a
AutoPlay	AutoPlaa	k1gFnSc2	AutoPlaa
(	(	kIx(	(
<g/>
automatické	automatický	k2eAgNnSc4d1	automatické
přehrávání	přehrávání	k1gNnSc4	přehrávání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
prostého	prostý	k2eAgInSc2d1	prostý
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
funkci	funkce	k1gFnSc4	funkce
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
soubor	soubor	k1gInSc1	soubor
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
kořenovém	kořenový	k2eAgInSc6d1	kořenový
adresáři	adresář	k1gInSc6	adresář
svazku	svazek	k1gInSc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
souborové	souborový	k2eAgInPc1d1	souborový
systémy	systém	k1gInPc1	systém
ve	v	k7c6	v
Windows	Windows	kA	Windows
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
malá	malý	k2eAgNnPc4d1	malé
a	a	k8xC	a
velká	velký	k2eAgNnPc4d1	velké
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
nezáleží	záležet	k5eNaImIp3nS	záležet
v	v	k7c6	v
názvu	název	k1gInSc6	název
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
AutoRun	AutoRuna	k1gFnPc2	AutoRuna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
poprvé	poprvé	k6eAd1	poprvé
použita	použít	k5eAaPmNgNnP	použít
v	v	k7c6	v
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
redukce	redukce	k1gFnSc1	redukce
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
technickou	technický	k2eAgFnSc4d1	technická
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
technicky	technicky	k6eAd1	technicky
méně	málo	k6eAd2	málo
zdatné	zdatný	k2eAgMnPc4d1	zdatný
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
AutoRun	AutoRun	k1gInSc1	AutoRun
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
(	(	kIx(	(
<g/>
po	po	k7c6	po
vložení	vložení	k1gNnSc6	vložení
média	médium	k1gNnSc2	médium
do	do	k7c2	do
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
)	)	kIx)	)
automatické	automatický	k2eAgNnSc1d1	automatické
spuštění	spuštění	k1gNnSc1	spuštění
počítačového	počítačový	k2eAgInSc2d1	počítačový
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
umístěného	umístěný	k2eAgInSc2d1	umístěný
na	na	k7c6	na
datovém	datový	k2eAgInSc6d1	datový
disku	disk	k1gInSc6	disk
CD-ROM	CD-ROM	k1gFnSc2	CD-ROM
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
instalační	instalační	k2eAgInSc1d1	instalační
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
Autorunu	Autorun	k1gInSc2	Autorun
lze	lze	k6eAd1	lze
nastavit	nastavit	k5eAaPmF	nastavit
ikonu	ikona	k1gFnSc4	ikona
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
u	u	k7c2	u
disku	disk	k1gInSc2	disk
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
v	v	k7c6	v
Průzkumníku	průzkumník	k1gMnSc3	průzkumník
Windows	Windows	kA	Windows
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
v	v	k7c4	v
Tento	tento	k3xDgInSc4	tento
počítač	počítač	k1gInSc4	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
možnosti	možnost	k1gFnPc1	možnost
nastavení	nastavení	k1gNnSc2	nastavení
byly	být	k5eAaImAgFnP	být
přidány	přidat	k5eAaPmNgFnP	přidat
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
verzích	verze	k1gFnPc6	verze
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
(	(	kIx(	(
<g/>
podpora	podpora	k1gFnSc1	podpora
automatického	automatický	k2eAgNnSc2d1	automatické
přehrávání	přehrávání	k1gNnSc2	přehrávání
AutoPlay	AutoPlaa	k1gFnSc2	AutoPlaa
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
Autorun	Autorun	k1gInSc1	Autorun
<g/>
.	.	kIx.	.
<g/>
inf	inf	k?	inf
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prostý	prostý	k2eAgInSc4d1	prostý
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
kořenovém	kořenový	k2eAgInSc6d1	kořenový
adresáři	adresář	k1gInSc6	adresář
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvojice	dvojice	k1gFnSc1	dvojice
klíč	klíč	k1gInSc1	klíč
<g/>
=	=	kIx~	=
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
klasické	klasický	k2eAgInPc1d1	klasický
konfigurační	konfigurační	k2eAgInPc1d1	konfigurační
INI	INI	kA	INI
soubory	soubor	k1gInPc1	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Upřesnění	upřesnění	k1gNnSc1	upřesnění
klíčů	klíč	k1gInPc2	klíč
<g/>
:	:	kIx,	:
Název	název	k1gInSc1	název
a	a	k8xC	a
umístění	umístění	k1gNnSc1	umístění
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zavolán	zavolat	k5eAaPmNgInS	zavolat
po	po	k7c6	po
vložení	vložení	k1gNnSc6	vložení
média	médium	k1gNnSc2	médium
(	(	kIx(	(
<g/>
AutoRun	AutoRun	k1gInSc1	AutoRun
úkol	úkol	k1gInSc1	úkol
<g/>
)	)	kIx)	)
Název	název	k1gInSc1	název
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ikonu	ikona	k1gFnSc4	ikona
(	(	kIx(	(
<g/>
namísto	namísto	k7c2	namísto
standardní	standardní	k2eAgFnSc2d1	standardní
ikony	ikona	k1gFnSc2	ikona
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
)	)	kIx)	)
Příkazy	příkaz	k1gInPc1	příkaz
pro	pro	k7c4	pro
kontextovou	kontextový	k2eAgFnSc4d1	kontextová
nabídku	nabídka	k1gFnSc4	nabídka
(	(	kIx(	(
<g/>
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
se	se	k3xPyFc4	se
po	po	k7c4	po
kliknutí	kliknutí	k1gNnSc4	kliknutí
pravým	pravý	k2eAgNnSc7d1	pravé
tlačítkem	tlačítko	k1gNnSc7	tlačítko
myši	myš	k1gFnSc2	myš
<g/>
)	)	kIx)	)
Výchozí	výchozí	k2eAgInSc1d1	výchozí
příkaz	příkaz	k1gInSc1	příkaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
spuštění	spuštění	k1gNnSc6	spuštění
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
dvojklik	dvojklik	k1gMnSc1	dvojklik
<g/>
)	)	kIx)	)
Změna	změna	k1gFnSc1	změna
automatického	automatický	k2eAgNnSc2d1	automatické
přehrávání	přehrávání	k1gNnSc2	přehrávání
Definice	definice	k1gFnSc2	definice
přítomnosti	přítomnost	k1gFnSc2	přítomnost
ovladačů	ovladač	k1gInPc2	ovladač
Pouhá	pouhý	k2eAgFnSc1d1	pouhá
existence	existence	k1gFnSc1	existence
souboru	soubor	k1gInSc2	soubor
Autorun	Autoruna	k1gFnPc2	Autoruna
<g/>
.	.	kIx.	.
<g/>
inf	inf	k?	inf
na	na	k7c6	na
médiu	médium	k1gNnSc6	médium
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
systém	systém	k1gInSc4	systém
Windows	Windows	kA	Windows
použije	použít	k5eAaPmIp3nS	použít
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
Zpracování	zpracování	k1gNnSc4	zpracování
INF	INF	kA	INF
souborů	soubor	k1gInPc2	soubor
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
verzi	verze	k1gFnSc4	verze
OS	osa	k1gFnPc2	osa
<g/>
,	,	kIx,	,
typu	typ	k1gInSc2	typ
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
některých	některý	k3yIgNnPc2	některý
nastavení	nastavení	k1gNnPc2	nastavení
registrů	registr	k1gInPc2	registr
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
registrech	registr	k1gInPc6	registr
povolena	povolen	k2eAgFnSc1d1	povolena
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
INF	INF	kA	INF
soubory	soubor	k1gInPc1	soubor
<g/>
:	:	kIx,	:
Verze	verze	k1gFnSc1	verze
před	před	k7c7	před
Windows	Windows	kA	Windows
XP	XP	kA	XP
Autorun	Autorun	k1gInSc1	Autorun
úkol	úkol	k1gInSc1	úkol
je	být	k5eAaImIp3nS	být
načten	načten	k2eAgInSc1d1	načten
<g/>
,	,	kIx,	,
analyzován	analyzován	k2eAgInSc1d1	analyzován
<g/>
,	,	kIx,	,
instrukce	instrukce	k1gFnPc4	instrukce
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
provedeny	provést	k5eAaPmNgInP	provést
<g/>
.	.	kIx.	.
</s>
<s>
Úkol	úkol	k1gInSc1	úkol
je	být	k5eAaImIp3nS	být
vykonán	vykonat	k5eAaPmNgInS	vykonat
bez	bez	k7c2	bez
zásahu	zásah	k1gInSc2	zásah
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
XP	XP	kA	XP
<g/>
,	,	kIx,	,
před	před	k7c7	před
aktualizací	aktualizace	k1gFnSc7	aktualizace
Service	Service	k1gFnSc1	Service
Pack	Pack	k1gMnSc1	Pack
2	[number]	k4	2
Zavedení	zavedení	k1gNnSc1	zavedení
automatického	automatický	k2eAgNnSc2d1	automatické
přehrávání	přehrávání	k1gNnSc2	přehrávání
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
typu	typ	k1gInSc2	typ
DRIVE_CDROM	DRIVE_CDROM	k1gFnSc2	DRIVE_CDROM
(	(	kIx(	(
<g/>
CD-ROM	CD-ROM	k1gFnSc1	CD-ROM
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
)	)	kIx)	)
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
Automatické	automatický	k2eAgNnSc4d1	automatické
přehrávání	přehrávání	k1gNnSc4	přehrávání
i	i	k8xC	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
autorun	autoruna	k1gFnPc2	autoruna
<g/>
.	.	kIx.	.
<g/>
inf	inf	k?	inf
nalezen	nalézt	k5eAaBmNgMnS	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Disk	disk	k1gInSc1	disk
typu	typ	k1gInSc2	typ
DRIVE_REMOVABLE	DRIVE_REMOVABLE	k1gMnPc2	DRIVE_REMOVABLE
nepoužívá	používat	k5eNaImIp3nS	používat
soubory	soubor	k1gInPc4	soubor
autorun	autoruna	k1gFnPc2	autoruna
<g/>
.	.	kIx.	.
<g/>
inf.	inf.	k?	inf.
Všechna	všechen	k3xTgNnPc1	všechen
vyměnitelná	vyměnitelný	k2eAgNnPc1d1	vyměnitelné
média	médium	k1gNnPc1	médium
jsou	být	k5eAaImIp3nP	být
zpracovávána	zpracovávat	k5eAaImNgFnS	zpracovávat
Automatickým	automatický	k2eAgNnSc7d1	automatické
přehráváním	přehrávání	k1gNnSc7	přehrávání
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
XP	XP	kA	XP
Service	Service	k1gFnSc1	Service
Pack	Pack	k1gInSc1	Pack
2	[number]	k4	2
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc1d2	vyšší
(	(	kIx(	(
<g/>
Windows	Windows	kA	Windows
Vista	vista	k2eAgInSc1d1	vista
včetně	včetně	k7c2	včetně
<g/>
)	)	kIx)	)
AutoRun	AutoRuna	k1gFnPc2	AutoRuna
úkol	úkol	k1gInSc1	úkol
bude	být	k5eAaImBp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dialogu	dialog	k1gInSc2	dialog
Přehrát	přehrát	k5eAaPmF	přehrát
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
Úkol	úkol	k1gInSc1	úkol
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
spárován	spárovat	k5eAaPmNgInS	spárovat
s	s	k7c7	s
klíčem	klíč	k1gInSc7	klíč
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
vynechán	vynechán	k2eAgInSc1d1	vynechán
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
Vista	vista	k2eAgInPc1d1	vista
Úkoly	úkol	k1gInPc1	úkol
Autorunu	Autorun	k1gInSc2	Autorun
jsou	být	k5eAaImIp3nP	být
prováděny	provádět	k5eAaImNgInP	provádět
automaticky	automaticky	k6eAd1	automaticky
na	na	k7c6	na
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
typu	typ	k1gInSc6	typ
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
úkoly	úkol	k1gInPc1	úkol
jsou	být	k5eAaImIp3nP	být
prováděny	provádět	k5eAaImNgInP	provádět
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Automatického	automatický	k2eAgNnSc2d1	automatické
přehrávání	přehrávání	k1gNnSc2	přehrávání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ve	v	k7c6	v
výchozím	výchozí	k2eAgNnSc6d1	výchozí
nastavení	nastavení	k1gNnSc6	nastavení
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
uživateli	uživatel	k1gMnSc3	uživatel
příslušné	příslušný	k2eAgNnSc4d1	příslušné
dialogové	dialogový	k2eAgNnSc4d1	dialogové
okno	okno	k1gNnSc4	okno
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
7	[number]	k4	7
Úkoly	úkol	k1gInPc1	úkol
AutoRunu	AutoRuna	k1gFnSc4	AutoRuna
lze	lze	k6eAd1	lze
zadat	zadat	k5eAaPmF	zadat
pouze	pouze	k6eAd1	pouze
médiím	médium	k1gNnPc3	médium
CD	CD	kA	CD
<g/>
/	/	kIx~	/
<g/>
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
jednotky	jednotka	k1gFnPc4	jednotka
jsou	být	k5eAaImIp3nP	být
povoleny	povolit	k5eAaPmNgInP	povolit
pouze	pouze	k6eAd1	pouze
klíče	klíč	k1gInPc1	klíč
label	labela	k1gFnPc2	labela
(	(	kIx(	(
<g/>
popis	popis	k1gInSc1	popis
<g/>
)	)	kIx)	)
a	a	k8xC	a
icon	icon	k1gInSc1	icon
(	(	kIx(	(
<g/>
ikona	ikona	k1gFnSc1	ikona
<g/>
)	)	kIx)	)
Tento	tento	k3xDgInSc4	tento
soubor	soubor	k1gInSc4	soubor
autorun	autorun	k1gInSc1	autorun
<g/>
.	.	kIx.	.
<g/>
inf	inf	k?	inf
určuje	určovat	k5eAaImIp3nS	určovat
soubor	soubor	k1gInSc1	soubor
setup	setup	k1gInSc1	setup
<g/>
.	.	kIx.	.
<g/>
exe	exe	k?	exe
jako	jako	k8xS	jako
spouštěč	spouštěč	k1gInSc1	spouštěč
aplikace	aplikace	k1gFnSc2	aplikace
(	(	kIx(	(
<g/>
při	při	k7c6	při
aktivaci	aktivace	k1gFnSc6	aktivace
AutoRunu	AutoRun	k1gInSc2	AutoRun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
ikona	ikona	k1gFnSc1	ikona
uložena	uložen	k2eAgFnSc1d1	uložena
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
setup	setup	k1gInSc1	setup
<g/>
.	.	kIx.	.
<g/>
exe	exe	k?	exe
bude	být	k5eAaImBp3nS	být
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
jednotku	jednotka	k1gFnSc4	jednotka
v	v	k7c6	v
Průzkumníku	průzkumník	k1gMnSc3	průzkumník
Windows	Windows	kA	Windows
<g/>
:	:	kIx,	:
Následující	následující	k2eAgFnPc1d1	následující
sekce	sekce	k1gFnPc1	sekce
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
platné	platný	k2eAgInPc4d1	platný
klíče	klíč	k1gInPc4	klíč
pro	pro	k7c4	pro
soubory	soubor	k1gInPc4	soubor
autorun	autoruna	k1gFnPc2	autoruna
<g/>
.	.	kIx.	.
<g/>
inf.	inf.	k?	inf.
Sekce	sekce	k1gFnSc1	sekce
Autorun	Autorun	k1gInSc1	Autorun
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
výchozí	výchozí	k2eAgInSc1d1	výchozí
AutoRun	AutoRun	k1gInSc1	AutoRun
příkazy	příkaz	k1gInPc1	příkaz
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
je	on	k3xPp3gMnPc4	on
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
platný	platný	k2eAgInSc1d1	platný
<g/>
.	.	kIx.	.
</s>
<s>
Povolené	povolený	k2eAgInPc1d1	povolený
klíče	klíč	k1gInPc1	klíč
<g/>
:	:	kIx,	:
action	action	k1gInSc1	action
<g/>
=	=	kIx~	=
<g/>
text	text	k1gInSc1	text
action	action	k1gInSc1	action
<g/>
=	=	kIx~	=
<g/>
@	@	kIx~	@
<g/>
[	[	kIx(	[
<g/>
filepath	filepath	k1gInSc1	filepath
<g/>
\	\	kIx~	\
<g/>
]	]	kIx)	]
<g/>
filename	filenam	k1gInSc5	filenam
<g/>
,	,	kIx,	,
<g/>
-resourceID	esourceID	k?	-resourceID
Windows	Windows	kA	Windows
XP	XP	kA	XP
SP2	SP2	k1gFnPc2	SP2
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc2d2	vyšší
<g/>
;	;	kIx,	;
jednotky	jednotka	k1gFnSc2	jednotka
typu	typ	k1gInSc2	typ
DRIVE_REMOVABLE	DRIVE_REMOVABLE	k1gFnSc2	DRIVE_REMOVABLE
and	and	k?	and
DRIVE_FIXED	DRIVE_FIXED	k1gMnSc1	DRIVE_FIXED
Určuje	určovat	k5eAaImIp3nS	určovat
text	text	k1gInSc4	text
použitý	použitý	k2eAgInSc4d1	použitý
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
Přehrát	přehrát	k5eAaPmF	přehrát
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
buď	buď	k8xC	buď
jako	jako	k8xS	jako
text	text	k1gInSc4	text
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
reference	reference	k1gFnPc4	reference
na	na	k7c4	na
zdroj	zdroj	k1gInSc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Ikona	ikona	k1gFnSc1	ikona
je	být	k5eAaImIp3nS	být
zobrazena	zobrazit	k5eAaPmNgFnS	zobrazit
vedle	vedle	k7c2	vedle
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
položka	položka	k1gFnSc1	položka
je	být	k5eAaImIp3nS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
ve	v	k7c6	v
výchozím	výchozí	k2eAgNnSc6d1	výchozí
nastavení	nastavení	k1gNnSc6	nastavení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
Přehrát	přehrát	k5eAaPmF	přehrát
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4	se
nezobrazuje	zobrazovat	k5eNaImIp3nS	zobrazovat
na	na	k7c6	na
jednotkách	jednotka	k1gFnPc6	jednotka
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
DRIVE_REMOVABLE	DRIVE_REMOVABLE	k1gFnSc1	DRIVE_REMOVABLE
dialogové	dialogový	k2eAgNnSc1d1	dialogové
okno	okno	k1gNnSc4	okno
Přehrát	přehrát	k5eAaPmF	přehrát
automaticky	automaticky	k6eAd1	automaticky
se	se	k3xPyFc4	se
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
další	další	k2eAgFnSc2d1	další
položky	položka	k1gFnSc2	položka
<g/>
,	,	kIx,	,
úkol	úkol	k1gInSc4	úkol
AutoRun	AutoRuna	k1gFnPc2	AutoRuna
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vynechán	vynechán	k2eAgInSc1d1	vynechán
<g/>
.	.	kIx.	.
</s>
<s>
DRIVE_FIXED	DRIVE_FIXED	k?	DRIVE_FIXED
Text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
a	a	k8xC	a
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
Přehrát	přehrát	k5eAaPmF	přehrát
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
typů	typ	k1gInPc2	typ
disků	disk	k1gInPc2	disk
je	být	k5eAaImIp3nS	být
klíč	klíč	k1gInSc1	klíč
ignorován	ignorován	k2eAgInSc1d1	ignorován
<g/>
.	.	kIx.	.
icon	icon	k1gInSc1	icon
<g/>
=	=	kIx~	=
<g/>
iconfilename	iconfilenam	k1gInSc5	iconfilenam
<g/>
[	[	kIx(	[
<g/>
,	,	kIx,	,
<g/>
index	index	k1gInSc1	index
<g/>
]	]	kIx)	]
Název	název	k1gInSc1	název
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ikonu	ikona	k1gFnSc4	ikona
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ikona	ikona	k1gFnSc1	ikona
nahradí	nahradit	k5eAaPmIp3nS	nahradit
standardní	standardní	k2eAgFnSc4d1	standardní
ikonu	ikona	k1gFnSc4	ikona
jednotky	jednotka	k1gFnSc2	jednotka
v	v	k7c6	v
průzkumníku	průzkumník	k1gMnSc3	průzkumník
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
label	label	k1gMnSc1	label
<g/>
=	=	kIx~	=
<g/>
text	text	k1gInSc1	text
Určuje	určovat	k5eAaImIp3nS	určovat
textový	textový	k2eAgInSc4d1	textový
popisek	popisek	k1gInSc4	popisek
reprezentující	reprezentující	k2eAgFnSc4d1	reprezentující
jednotku	jednotka	k1gFnSc4	jednotka
v	v	k7c6	v
Průzkumníku	průzkumník	k1gMnSc3	průzkumník
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
open	open	k1gMnSc1	open
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
exepath	exepath	k1gInSc1	exepath
<g/>
\	\	kIx~	\
<g/>
]	]	kIx)	]
<g/>
exefile	exefile	k6eAd1	exefile
[	[	kIx(	[
<g/>
param	param	k6eAd1	param
<g/>
1	[number]	k4	1
[	[	kIx(	[
<g/>
param	param	k6eAd1	param
<g/>
2	[number]	k4	2
...	...	k?	...
<g/>
]]	]]	k?	]]
Určuje	určovat	k5eAaImIp3nS	určovat
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
název	název	k1gInSc4	název
souboru	soubor	k1gInSc2	soubor
a	a	k8xC	a
volitelné	volitelný	k2eAgInPc4d1	volitelný
parametry	parametr	k1gInPc4	parametr
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
spouští	spouštět	k5eAaImIp3nP	spouštět
AutoRun	AutoRun	k1gInSc4	AutoRun
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
funkci	funkce	k1gFnSc6	funkce
CreateProcess	CreateProcessa	k1gFnPc2	CreateProcessa
vyvolanou	vyvolaný	k2eAgFnSc7d1	vyvolaná
AutoRunem	AutoRun	k1gInSc7	AutoRun
<g/>
.	.	kIx.	.
shellexecute	shellexecut	k1gInSc5	shellexecut
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
filepath	filepath	k1gInSc1	filepath
<g/>
\	\	kIx~	\
<g/>
]	]	kIx)	]
<g/>
filename	filenam	k1gInSc5	filenam
[	[	kIx(	[
<g/>
param	param	k1gInSc1	param
<g/>
1	[number]	k4	1
[	[	kIx(	[
<g/>
param	param	k6eAd1	param
<g/>
2	[number]	k4	2
...	...	k?	...
<g/>
]]	]]	k?	]]
Windows	Windows	kA	Windows
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
ME	ME	kA	ME
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc4d2	vyšší
Podobné	podobný	k2eAgInPc4d1	podobný
otevřít	otevřít	k5eAaPmF	otevřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomocí	pomocí	k7c2	pomocí
souboru	soubor	k1gInSc2	soubor
asociace	asociace	k1gFnSc2	asociace
pro	pro	k7c4	pro
běh	běh	k1gInSc4	běh
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
souboru	soubor	k1gInSc2	soubor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
proto	proto	k8xC	proto
spustitelný	spustitelný	k2eAgInSc4d1	spustitelný
nebo	nebo	k8xC	nebo
datový	datový	k2eAgInSc4d1	datový
soubor	soubor	k1gInSc4	soubor
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ShellExecuteEx	ShellExecuteEx	k1gInSc4	ShellExecuteEx
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
volána	volat	k5eAaImNgFnS	volat
AutoRunem	AutoRun	k1gInSc7	AutoRun
<g/>
.	.	kIx.	.
)	)	kIx)	)
UseAutoPlay	UseAutoPlaa	k1gFnPc1	UseAutoPlaa
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
Windows	Windows	kA	Windows
XP	XP	kA	XP
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc2d2	vyšší
<g/>
;	;	kIx,	;
jednotky	jednotka	k1gFnSc2	jednotka
typu	typ	k1gInSc2	typ
DRIVE_CDROM	DRIVE_CDROM	k1gFnSc1	DRIVE_CDROM
Použití	použití	k1gNnSc3	použití
spíše	spíše	k9	spíše
Automatického	automatický	k2eAgNnSc2d1	automatické
přehrávání	přehrávání	k1gNnSc2	přehrávání
<g/>
,	,	kIx,	,
než	než	k8xS	než
AutoRunu	AutoRuna	k1gFnSc4	AutoRuna
s	s	k7c7	s
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
OS	OS	kA	OS
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
Windows	Windows	kA	Windows
XP	XP	kA	XP
tento	tento	k3xDgInSc4	tento
klíč	klíč	k1gInSc4	klíč
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
<g/>
.	.	kIx.	.
shell	shell	k1gInSc1	shell
<g/>
\	\	kIx~	\
<g/>
verb	verbum	k1gNnPc2	verbum
<g/>
\	\	kIx~	\
<g/>
command	command	k1gInSc1	command
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
exepath	exepath	k1gInSc1	exepath
<g/>
\	\	kIx~	\
<g/>
]	]	kIx)	]
<g/>
exefile	exefile	k6eAd1	exefile
[	[	kIx(	[
<g/>
param	param	k6eAd1	param
<g/>
1	[number]	k4	1
[	[	kIx(	[
<g/>
param	param	k6eAd1	param
<g/>
2	[number]	k4	2
...	...	k?	...
<g/>
]]	]]	k?	]]
Přidává	přidávat	k5eAaImIp3nS	přidávat
vlastní	vlastní	k2eAgInSc4d1	vlastní
příkaz	příkaz	k1gInSc4	příkaz
k	k	k7c3	k
disku	disco	k1gNnSc3	disco
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
nabídce	nabídka	k1gFnSc6	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Verb	verbum	k1gNnPc2	verbum
je	být	k5eAaImIp3nS	být
řetězec	řetězec	k1gInSc1	řetězec
bez	bez	k7c2	bez
mezery	mezera	k1gFnSc2	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
shell	shell	k1gInSc1	shell
<g/>
\	\	kIx~	\
<g/>
verb	verbum	k1gNnPc2	verbum
<g/>
=	=	kIx~	=
<g/>
menu	menu	k1gNnSc7	menu
text	text	k1gInSc1	text
text	text	k1gInSc1	text
v	v	k7c6	v
menu	menu	k1gNnSc6	menu
<g/>
,	,	kIx,	,
Ampersand	Ampersand	k1gInSc4	Ampersand
(	(	kIx(	(
<g/>
&	&	k?	&
<g/>
)	)	kIx)	)
vybere	vybrat	k5eAaPmIp3nS	vybrat
klávesovou	klávesový	k2eAgFnSc4d1	klávesová
zkratku	zkratka	k1gFnSc4	zkratka
pro	pro	k7c4	pro
menu	menu	k1gNnSc4	menu
<g/>
.	.	kIx.	.
shell	shell	k1gInSc1	shell
<g/>
=	=	kIx~	=
<g/>
verb	verbum	k1gNnPc2	verbum
Odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
se	se	k3xPyFc4	se
na	na	k7c4	na
shell	shell	k1gInSc4	shell
<g/>
\	\	kIx~	\
<g/>
verb	verbum	k1gNnPc2	verbum
jako	jako	k8xC	jako
výchozí	výchozí	k2eAgInSc4d1	výchozí
příkaz	příkaz	k1gInSc4	příkaz
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
nabídce	nabídka	k1gFnSc6	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chybí	chybit	k5eAaPmIp3nS	chybit
výchozí	výchozí	k2eAgFnSc1d1	výchozí
položka	položka	k1gFnSc1	položka
menu	menu	k1gNnSc2	menu
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
Přehrát	přehrát	k5eAaPmF	přehrát
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Windows	Windows	kA	Windows
Vista	vista	k2eAgNnSc1d1	vista
a	a	k8xC	a
vyšší	vysoký	k2eAgInSc1d2	vyšší
Obsah	obsah	k1gInSc1	obsah
(	(	kIx(	(
<g/>
Content	Content	k1gInSc1	Content
<g/>
)	)	kIx)	)
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
autorům	autor	k1gMnPc3	autor
sdělit	sdělit	k5eAaPmF	sdělit
typ	typ	k1gInSc4	typ
obsahu	obsah	k1gInSc2	obsah
automatického	automatický	k2eAgNnSc2d1	automatické
přehrávání	přehrávání	k1gNnSc2	přehrávání
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
přímo	přímo	k6eAd1	přímo
nutné	nutný	k2eAgNnSc1d1	nutné
zkoumat	zkoumat	k5eAaImF	zkoumat
obsah	obsah	k1gInSc4	obsah
média	médium	k1gNnSc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Platné	platný	k2eAgInPc1d1	platný
klíče	klíč	k1gInPc1	klíč
<g/>
:	:	kIx,	:
MusicFiles	MusicFiles	k1gMnSc1	MusicFiles
<g/>
,	,	kIx,	,
PictureFiles	PictureFiles	k1gMnSc1	PictureFiles
<g/>
,	,	kIx,	,
VideoFiles	VideoFiles	k1gMnSc1	VideoFiles
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
klíč	klíč	k1gInSc1	klíč
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uveden	uveden	k2eAgInSc1d1	uveden
s	s	k7c7	s
pravdivými	pravdivý	k2eAgFnPc7d1	pravdivá
<g/>
/	/	kIx~	/
<g/>
nepravdivými	pravdivý	k2eNgFnPc7d1	nepravdivá
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
case	case	k6eAd1	case
sensitive	sensitiv	k1gMnSc5	sensitiv
<g/>
.	.	kIx.	.
</s>
<s>
Pravda	pravda	k1gFnSc1	pravda
–	–	k?	–
True	True	k1gFnSc1	True
(	(	kIx(	(
<g/>
true	true	k1gNnSc1	true
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
yes	yes	k?	yes
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
)	)	kIx)	)
zobrazí	zobrazit	k5eAaPmIp3nP	zobrazit
obslužné	obslužný	k2eAgInPc1d1	obslužný
manipulátory	manipulátor	k1gInPc1	manipulátor
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
typem	typ	k1gInSc7	typ
obsahu	obsah	k1gInSc2	obsah
Nepravda	nepravda	k1gFnSc1	nepravda
–	–	k?	–
False	False	k1gFnSc1	False
(	(	kIx(	(
<g/>
false	false	k1gFnSc1	false
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
no	no	k9	no
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
)	)	kIx)	)
nezobrazí	zobrazit	k5eNaPmIp3nP	zobrazit
obslužné	obslužný	k2eAgInPc1d1	obslužný
manipulátory	manipulátor	k1gInPc1	manipulátor
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
typem	typ	k1gInSc7	typ
obsahu	obsah	k1gInSc2	obsah
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Windows	Windows	kA	Windows
Vista	vista	k2eAgInPc1d1	vista
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc1d2	vyšší
Automatické	automatický	k2eAgInPc1d1	automatický
přehrávání	přehrávání	k1gNnSc4	přehrávání
obsahu	obsah	k1gInSc2	obsah
hledá	hledat	k5eAaImIp3nS	hledat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
uvedených	uvedený	k2eAgFnPc6d1	uvedená
složkách	složka	k1gFnPc6	složka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
podsložkách	podsložka	k1gFnPc6	podsložka
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
složky	složka	k1gFnSc2	složka
je	být	k5eAaImIp3nS	být
brán	brát	k5eAaImNgInS	brát
vždy	vždy	k6eAd1	vždy
jako	jako	k8xC	jako
absolutní	absolutní	k2eAgFnSc1d1	absolutní
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
z	z	k7c2	z
kořenového	kořenový	k2eAgInSc2d1	kořenový
adresáře	adresář	k1gInSc2	adresář
média	médium	k1gNnSc2	médium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Windows	Windows	kA	Windows
Vista	vista	k2eAgFnSc4d1	vista
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc4d2	vyšší
Automatické	automatický	k2eAgNnSc4d1	automatické
přehrávání	přehrávání	k1gNnSc4	přehrávání
nebude	být	k5eNaImBp3nS	být
skenovat	skenovat	k5eAaImF	skenovat
uvedené	uvedený	k2eAgFnPc4d1	uvedená
složky	složka	k1gFnPc4	složka
ani	ani	k8xC	ani
jejich	jejich	k3xOp3gFnPc4	jejich
podsložky	podsložka	k1gFnPc4	podsložka
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Windows	Windows	kA	Windows
XP	XP	kA	XP
a	a	k8xC	a
vyšší	vysoký	k2eAgNnSc1d2	vyšší
Označuje	označovat	k5eAaImIp3nS	označovat
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uloženy	uložit	k5eAaPmNgInP	uložit
soubory	soubor	k1gInPc1	soubor
ovladačů	ovladač	k1gInPc2	ovladač
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
braní	braní	k1gNnSc1	braní
zdlouhavému	zdlouhavý	k2eAgNnSc3d1	zdlouhavé
prohledávání	prohledávání	k1gNnSc3	prohledávání
celého	celý	k2eAgInSc2d1	celý
obsahu	obsah	k1gInSc2	obsah
media	medium	k1gNnSc2	medium
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
uvedení	uvedení	k1gNnSc2	uvedení
této	tento	k3xDgFnSc2	tento
sekce	sekce	k1gFnSc2	sekce
budou	být	k5eAaImBp3nP	být
ovladače	ovladač	k1gInPc1	ovladač
hledány	hledat	k5eAaImNgInP	hledat
v	v	k7c6	v
disketách	disketa	k1gFnPc6	disketa
(	(	kIx(	(
<g/>
jednotky	jednotka	k1gFnSc2	jednotka
A	A	kA	A
nebo	nebo	k8xC	nebo
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
CD	CD	kA	CD
<g/>
/	/	kIx~	/
<g/>
DVD	DVD	kA	DVD
médiích	médium	k1gNnPc6	médium
menších	malý	k2eAgFnPc2d2	menší
než	než	k8xS	než
1	[number]	k4	1
<g/>
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průběhu	průběh	k1gInSc6	průběh
instalace	instalace	k1gFnSc2	instalace
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
ovladačů	ovladač	k1gInPc2	ovladač
<g/>
.	.	kIx.	.
</s>
<s>
Platný	platný	k2eAgInSc1d1	platný
klíč	klíč	k1gInSc1	klíč
<g/>
:	:	kIx,	:
DriverPath	DriverPath	k1gInSc1	DriverPath
<g/>
=	=	kIx~	=
<g/>
directorypath	directorypath	k1gInSc1	directorypath
který	který	k3yRgInSc1	který
vypíše	vypsat	k5eAaPmIp3nS	vypsat
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
hledat	hledat	k5eAaImF	hledat
ovladače	ovladač	k1gInPc1	ovladač
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Autorun	Autoruna	k1gFnPc2	Autoruna
<g/>
.	.	kIx.	.
<g/>
inf	inf	k?	inf
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
