<s>
Autorun	Autorun	k1gNnSc1	Autorun
<g/>
.	.	kIx.	.
<g/>
inf	inf	k?	inf
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
konfiguračního	konfigurační	k2eAgInSc2d1	konfigurační
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využíván	využívat	k5eAaPmNgInS	využívat
funkcemi	funkce	k1gFnPc7	funkce
AutoRun	AutoRuna	k1gFnPc2	AutoRuna
a	a	k8xC	a
AutoPlay	AutoPlaa	k1gFnSc2	AutoPlaa
(	(	kIx(	(
<g/>
automatické	automatický	k2eAgNnSc4d1	automatické
přehrávání	přehrávání	k1gNnSc4	přehrávání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
