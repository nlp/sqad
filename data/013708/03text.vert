<s>
Národní	národní	k2eAgInSc1d1
institut	institut	k1gInSc1
standardů	standard	k1gInPc2
a	a	k8xC
technologie	technologie	k1gFnSc2
</s>
<s>
Národní	národní	k2eAgInSc1d1
institut	institut	k1gInSc1
standardů	standard	k1gInPc2
a	a	k8xC
technologie	technologie	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1901	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Gaithersburg	Gaithersburg	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
39	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
54,84	54,84	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
77	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
35,04	35,04	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Lídr	lídr	k1gMnSc1
</s>
<s>
Patrick	Patrick	k6eAd1
D.	D.	kA
Gallagher	Gallaghra	k1gFnPc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Mateřská	mateřský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
obchodu	obchod	k1gInSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgMnPc2d1
Zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
2	#num#	k4
900	#num#	k4
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.nist.gov	www.nist.gov	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInSc1d1
institut	institut	k1gInSc1
standardů	standard	k1gInPc2
a	a	k8xC
technologie	technologie	k1gFnSc2
(	(	kIx(
<g/>
National	National	k1gInSc1
Institute	institut	k1gInSc1
of	of	k?
Standards	Standards	k1gInPc2
and	and	k8xC
Technology	technolog	k1gInSc2
<g/>
,	,	kIx,
NIST	NIST	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
laboratoř	laboratoř	k1gFnSc1
měřicích	měřicí	k2eAgInPc2d1
standardů	standard	k1gInPc2
při	při	k7c6
ministerstvu	ministerstvo	k1gNnSc6
obchodu	obchod	k1gInSc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
instituce	instituce	k1gFnSc2
je	být	k5eAaImIp3nS
podpora	podpora	k1gFnSc1
inovací	inovace	k1gFnPc2
a	a	k8xC
průmyslové	průmyslový	k2eAgFnSc2d1
konkurenceschopnosti	konkurenceschopnost	k1gFnSc2
USA	USA	kA
zlepšováním	zlepšování	k1gNnSc7
vědeckých	vědecký	k2eAgNnPc2d1
měření	měření	k1gNnPc2
<g/>
,	,	kIx,
standardů	standard	k1gInPc2
a	a	k8xC
technologie	technologie	k1gFnSc2
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
ekonomickou	ekonomický	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
a	a	k8xC
zlepšování	zlepšování	k1gNnSc4
kvality	kvalita	k1gFnSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
NIST	NIST	kA
má	mít	k5eAaImIp3nS
kolem	kolem	k7c2
2900	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
(	(	kIx(
<g/>
vědci	vědec	k1gMnPc1
<g/>
,	,	kIx,
inženýři	inženýr	k1gMnPc1
<g/>
,	,	kIx,
technici	technik	k1gMnPc1
<g/>
,	,	kIx,
administrativa	administrativa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1800	#num#	k4
externích	externí	k2eAgMnPc2d1
spolupracovníků	spolupracovník	k1gMnPc2
a	a	k8xC
rozpočet	rozpočet	k1gInSc4
kolem	kolem	k7c2
850	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
NIST	NIST	kA
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
standardy	standard	k1gInPc4
ohledně	ohledně	k7c2
šifrování	šifrování	k1gNnSc2
a	a	k8xC
digitálních	digitální	k2eAgInPc2d1
podpisů	podpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
je	být	k5eAaImIp3nS
však	však	k9
závislý	závislý	k2eAgInSc1d1
na	na	k7c6
NSA	NSA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.pcworld.com/article/2454380/overreliance-on-the-nsa-led-to-weak-crypto-standard-nist-advisers-find.html	http://www.pcworld.com/article/2454380/overreliance-on-the-nsa-led-to-weak-crypto-standard-nist-advisers-find.html	k1gInSc1
-	-	kIx~
Overreliance	Overrelianec	k1gMnSc2
on	on	k3xPp3gMnSc1
the	the	k?
NSA	NSA	kA
led	led	k1gInSc1
to	ten	k3xDgNnSc4
weak	weak	k6eAd1
crypto	crypto	k1gNnSc1
standard	standard	k1gInSc1
<g/>
,	,	kIx,
NIST	NIST	kA
advisers	advisers	k1gInSc1
find	find	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Národní	národní	k2eAgInSc4d1
institut	institut	k1gInSc4
standardů	standard	k1gInPc2
a	a	k8xC
technologie	technologie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2007416940	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2158	#num#	k4
463X	463X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
88112126	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
131110795	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
88112126	#num#	k4
</s>
