<s>
Hokej	hokej	k1gInSc1	hokej
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
koncem	koncem	k7c2	koncem
19.	[number]	k4	19.
století	století	k1gNnSc2	století
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
pravidla	pravidlo	k1gNnPc1	pravidlo
byla	být	k5eAaImAgNnP	být
vytvořena	vytvořit	k5eAaPmNgNnP	vytvořit
v	v	k7c6	v
Montréalu	Montréal	k1gInSc6	Montréal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
částečně	částečně	k6eAd1	částečně
i	i	k9	i
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
