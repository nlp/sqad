<s>
Důsledkem	důsledek	k1gInSc7	důsledek
byly	být	k5eAaImAgFnP	být
akutní	akutní	k2eAgFnPc1d1	akutní
potíže	potíž	k1gFnPc1	potíž
s	s	k7c7	s
dýcháním	dýchání	k1gNnSc7	dýchání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vyřešit	vyřešit	k5eAaPmF	vyřešit
jedině	jedině	k6eAd1	jedině
pomocí	pomocí	k7c2	pomocí
tracheotomie	tracheotomie	k1gFnSc2	tracheotomie
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
schopnost	schopnost	k1gFnSc4	schopnost
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
