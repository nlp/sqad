<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedna	jeden	k4xCgFnSc1	jeden
žena	žena	k1gFnSc1	žena
má	mít	k5eAaImIp3nS	mít
zároveň	zároveň	k6eAd1	zároveň
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednoho	jeden	k4xCgMnSc2	jeden
manžela	manžel	k1gMnSc2	manžel
<g/>
?	?	kIx.	?
</s>
