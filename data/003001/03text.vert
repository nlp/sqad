<s>
Polyandrie	polyandrie	k1gFnSc1	polyandrie
čili	čili	k8xC	čili
mnohomužství	mnohomužství	k1gNnSc1	mnohomužství
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedna	jeden	k4xCgFnSc1	jeden
žena	žena	k1gFnSc1	žena
má	mít	k5eAaImIp3nS	mít
zároveň	zároveň	k6eAd1	zároveň
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednoho	jeden	k4xCgMnSc2	jeden
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Polyandrie	polyandrie	k1gFnSc1	polyandrie
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
než	než	k8xS	než
monogamie	monogamie	k1gFnSc1	monogamie
a	a	k8xC	a
polygamie	polygamie	k1gFnSc1	polygamie
<g/>
,	,	kIx,	,
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
ji	on	k3xPp3gFnSc4	on
necelé	celý	k2eNgNnSc4d1	necelé
jedno	jeden	k4xCgNnSc4	jeden
procento	procento	k1gNnSc4	procento
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nevedou	vést	k5eNaImIp3nP	vést
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
obvykle	obvykle	k6eAd1	obvykle
důvody	důvod	k1gInPc1	důvod
sexuální	sexuální	k2eAgInPc1d1	sexuální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
muž	muž	k1gMnSc1	muž
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
podporovat	podporovat	k5eAaImF	podporovat
manželku	manželka	k1gFnSc4	manželka
jen	jen	k9	jen
omezeně	omezeně	k6eAd1	omezeně
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
musí	muset	k5eAaImIp3nP	muset
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
s	s	k7c7	s
tvrdými	tvrdý	k2eAgFnPc7d1	tvrdá
podmínkami	podmínka	k1gFnPc7	podmínka
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
úrodné	úrodný	k2eAgFnSc2d1	úrodná
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
až	až	k6eAd1	až
tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
proto	proto	k8xC	proto
žení	ženit	k5eAaImIp3nP	ženit
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
vychovávají	vychovávat	k5eAaImIp3nP	vychovávat
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
začíná	začínat	k5eAaImIp3nS	začínat
takové	takový	k3xDgNnSc1	takový
manželství	manželství	k1gNnSc1	manželství
monogamně	monogamně	k6eAd1	monogamně
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
manželé	manžel	k1gMnPc1	manžel
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
až	až	k9	až
dodatečně	dodatečně	k6eAd1	dodatečně
<g/>
.	.	kIx.	.
</s>
<s>
Pěstují	pěstovat	k5eAaImIp3nP	pěstovat
pak	pak	k6eAd1	pak
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
rotační	rotační	k2eAgInSc4d1	rotační
způsob	způsob	k1gInSc4	způsob
sexuálních	sexuální	k2eAgInPc2d1	sexuální
styků	styk	k1gInPc2	styk
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
povoleny	povolen	k2eAgInPc1d1	povolen
rozvody	rozvod	k1gInPc1	rozvod
a	a	k8xC	a
manželství	manželství	k1gNnPc1	manželství
bývají	bývat	k5eAaImIp3nP	bývat
neodvolatelně	odvolatelně	k6eNd1	odvolatelně
celoživotní	celoživotní	k2eAgFnPc1d1	celoživotní
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
společná	společný	k2eAgFnSc1d1	společná
manželka	manželka	k1gFnSc1	manželka
neplodná	plodný	k2eNgFnSc1d1	neplodná
<g/>
,	,	kIx,	,
smí	smět	k5eAaImIp3nP	smět
si	se	k3xPyFc3	se
muži	muž	k1gMnPc1	muž
přibrat	přibrat	k5eAaPmF	přibrat
do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
další	další	k2eAgFnSc4d1	další
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
její	její	k3xOp3gFnSc4	její
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
legitimní	legitimní	k2eAgFnPc1d1	legitimní
a	a	k8xC	a
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
domácnosti	domácnost	k1gFnSc6	domácnost
nechybí	chybět	k5eNaImIp3nS	chybět
mužský	mužský	k2eAgInSc4d1	mužský
vzor	vzor	k1gInSc4	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Keňanka	Keňanka	k1gFnSc1	Keňanka
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
dva	dva	k4xCgMnPc4	dva
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
o	o	k7c4	o
ženu	žena	k1gFnSc4	žena
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
dělit	dělit	k5eAaImF	dělit
<g/>
,	,	kIx,	,
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
</s>
