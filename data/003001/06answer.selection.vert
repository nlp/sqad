<s>
Polyandrie	polyandrie	k1gFnSc1	polyandrie
čili	čili	k8xC	čili
mnohomužství	mnohomužství	k1gNnSc1	mnohomužství
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedna	jeden	k4xCgFnSc1	jeden
žena	žena	k1gFnSc1	žena
má	mít	k5eAaImIp3nS	mít
zároveň	zároveň	k6eAd1	zároveň
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednoho	jeden	k4xCgMnSc2	jeden
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
