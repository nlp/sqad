<p>
<s>
Bratr	bratr	k1gMnSc1	bratr
Jün	Jün	k1gMnSc1	Jün
(	(	kIx(	(
<g/>
též	též	k9	též
Bratr	bratr	k1gMnSc1	bratr
Yun	Yun	k1gMnSc1	Yun
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Jün	Jün	k1gMnSc1	Jün
ti-siung	tiiung	k1gMnSc1	ti-siung
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Yún	Yún	k1gFnSc2	Yún
dì	dì	k?	dì
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
云	云	k?	云
<g/>
,	,	kIx,	,
občanským	občanský	k2eAgNnSc7d1	občanské
jménem	jméno	k1gNnSc7	jméno
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Liou	Lious	k1gInSc2	Lious
Čen-jing	Čening	k1gInSc4	Čen-jing
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gMnSc7	pchin-jin
Liu	Liu	k1gFnPc2	Liu
Zhenying	Zhenying	k1gInSc1	Zhenying
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
刘	刘	k?	刘
<g/>
,	,	kIx,	,
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
vedoucích	vedoucí	k1gMnPc2	vedoucí
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
čínské	čínský	k2eAgFnSc2d1	čínská
podzemní	podzemní	k2eAgFnSc2d1	podzemní
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
propagátorem	propagátor	k1gMnSc7	propagátor
hnutí	hnutí	k1gNnSc2	hnutí
Zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
bratra	bratr	k1gMnSc2	bratr
Yuna	Yunus	k1gMnSc2	Yunus
je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Nebeský	nebeský	k2eAgMnSc1d1	nebeský
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Hattaway	Hattawaa	k1gFnSc2	Hattawaa
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
mládí	mládí	k1gNnSc6	mládí
<g/>
,	,	kIx,	,
evangelizaci	evangelizace	k1gFnSc6	evangelizace
a	a	k8xC	a
zázracích	zázrak	k1gInPc6	zázrak
bratra	bratr	k1gMnSc4	bratr
Yuna	Yunus	k1gMnSc4	Yunus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
autorem	autor	k1gMnSc7	autor
knihy	kniha	k1gFnSc2	kniha
Živá	živý	k2eAgFnSc1d1	živá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
rovněž	rovněž	k9	rovněž
napsal	napsat	k5eAaBmAgMnS	napsat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Hattawayem	Hattaway	k1gMnSc7	Hattaway
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
bratru	bratr	k1gMnSc6	bratr
Yunovi	Yuna	k1gMnSc6	Yuna
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
v	v	k7c6	v
Křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
magazínu	magazín	k1gInSc6	magazín
<g/>
.	.	kIx.	.
<g/>
Bratr	bratr	k1gMnSc1	bratr
Yun	Yun	k1gMnSc1	Yun
je	být	k5eAaImIp3nS	být
disident	disident	k1gMnSc1	disident
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
r.	r.	kA	r.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
a	a	k8xC	a
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Deling	Deling	k1gInSc1	Deling
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
čínských	čínský	k2eAgMnPc2d1	čínský
podzemních	podzemní	k2eAgMnPc2d1	podzemní
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
Lin	Lina	k1gFnPc2	Lina
Sien-kao	Sienao	k6eAd1	Sien-kao
<g/>
,	,	kIx,	,
Jünovu	Jünův	k2eAgFnSc4d1	Jünův
činnost	činnost	k1gFnSc4	činnost
zpochybnil	zpochybnit	k5eAaPmAgMnS	zpochybnit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
příliš	příliš	k6eAd1	příliš
nesouvisí	souviset	k5eNaImIp3nS	souviset
se	s	k7c7	s
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
aktivitou	aktivita	k1gFnSc7	aktivita
čínských	čínský	k2eAgMnPc2d1	čínský
podzemních	podzemní	k2eAgMnPc2d1	podzemní
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Jün	Jün	k?	Jün
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
obvinění	obvinění	k1gNnSc3	obvinění
naopak	naopak	k6eAd1	naopak
ohradil	ohradit	k5eAaPmAgMnS	ohradit
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
Lin	Lina	k1gFnPc2	Lina
Sien-kao	Sienao	k6eAd1	Sien-kao
nikdy	nikdy	k6eAd1	nikdy
nepotkal	potkat	k5eNaPmAgMnS	potkat
ani	ani	k8xC	ani
nečetl	číst	k5eNaImAgMnS	číst
jeho	jeho	k3xOp3gFnSc2	jeho
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Bůh	bůh	k1gMnSc1	bůh
dokáže	dokázat	k5eAaPmIp3nS	dokázat
změnit	změnit	k5eAaPmF	změnit
i	i	k9	i
srdce	srdce	k1gNnSc1	srdce
komunisty	komunista	k1gMnSc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Listopad	listopad	k1gInSc1	listopad
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
4345	[number]	k4	4345
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Kniha	kniha	k1gFnSc1	kniha
Nebeský	nebeský	k2eAgMnSc1d1	nebeský
muž	muž	k1gMnSc1	muž
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Audio	audio	k2eAgFnSc1d1	audio
přednáška	přednáška	k1gFnSc1	přednáška
z	z	k7c2	z
návštěvy	návštěva	k1gFnSc2	návštěva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
</s>
</p>
