<p>
<s>
Bratr	bratr	k1gMnSc1	bratr
Jün	Jün	k1gMnSc1	Jün
(	(	kIx(	(
<g/>
též	též	k9	též
Bratr	bratr	k1gMnSc1	bratr
Yun	Yun	k1gMnSc1	Yun
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Jün	Jün	k1gMnSc1	Jün
ti-siung	tiiung	k1gMnSc1	ti-siung
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Yún	Yún	k1gFnSc2	Yún
dì	dì	k?	dì
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
云	云	k?	云
<g/>
,	,	kIx,	,
občanským	občanský	k2eAgNnSc7d1	občanské
jménem	jméno	k1gNnSc7	jméno
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Liou	Lious	k1gInSc2	Lious
Čen-jing	Čening	k1gInSc4	Čen-jing
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gMnSc7	pchin-jin
Liu	Liu	k1gFnPc2	Liu
Zhenying	Zhenying	k1gInSc1	Zhenying
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
刘	刘	k?	刘
<g/>
,	,	kIx,	,
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
vedoucích	vedoucí	k1gMnPc2	vedoucí
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
čínské	čínský	k2eAgFnSc2d1	čínská
podzemní	podzemní	k2eAgFnSc2d1	podzemní
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
propagátorem	propagátor	k1gMnSc7	propagátor
hnutí	hnutí	k1gNnSc2	hnutí
Zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
bratra	bratr	k1gMnSc2	bratr
Yuna	Yunus	k1gMnSc2	Yunus
je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Nebeský	nebeský	k2eAgMnSc1d1	nebeský
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Hattaway	Hattawaa	k1gFnSc2	Hattawaa
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
mládí	mládí	k1gNnSc6	mládí
<g/>
,	,	kIx,	,
evangelizaci	evangelizace	k1gFnSc6	evangelizace
a	a	k8xC	a
zázracích	zázrak	k1gInPc6	zázrak
bratra	bratr	k1gMnSc4	bratr
Yuna	Yunus	k1gMnSc4	Yunus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
autorem	autor	k1gMnSc7	autor
knihy	kniha	k1gFnSc2	kniha
Živá	živý	k2eAgFnSc1d1	živá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
rovněž	rovněž	k9	rovněž
napsal	napsat	k5eAaPmAgMnS	napsat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Hattawayem	Hattaway	k1gMnSc7	Hattaway
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
bratru	bratr	k1gMnSc6	bratr
Yunovi	Yuna	k1gMnSc6	Yuna
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
v	v	k7c6	v
Křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
magazínu	magazín	k1gInSc6	magazín
<g/>
.	.	kIx.	.
<g/>
Bratr	bratr	k1gMnSc1	bratr
Yun	Yun	k1gMnSc1	Yun
je	být	k5eAaImIp3nS	být
disident	disident	k1gMnSc1	disident
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
r.	r.	kA	r.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
a	a	k8xC	a
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Deling	Deling	k1gInSc1	Deling
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
