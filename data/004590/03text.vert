<s>
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
(	(	kIx(	(
<g/>
K	K	kA	K
620	[number]	k4	620
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
opera	opera	k1gFnSc1	opera
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
hrána	hrát	k5eAaImNgFnS	hrát
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
předměstském	předměstský	k2eAgNnSc6d1	předměstské
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Vídeňce	Vídeňka	k1gFnSc6	Vídeňka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc4	libreto
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
napsal	napsat	k5eAaBmAgMnS	napsat
divadelník	divadelník	k1gMnSc1	divadelník
Emanuel	Emanuel	k1gMnSc1	Emanuel
Schikaneder	Schikaneder	k1gMnSc1	Schikaneder
<g/>
,	,	kIx,	,
libreto	libreto	k1gNnSc1	libreto
je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
německy	německy	k6eAd1	německy
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
jazyk	jazyk	k1gInSc4	jazyk
oper	opera	k1gFnPc2	opera
běžná	běžný	k2eAgFnSc1d1	běžná
italština	italština	k1gFnSc1	italština
a	a	k8xC	a
němčina	němčina	k1gFnSc1	němčina
byla	být	k5eAaImAgFnS	být
spíše	spíše	k9	spíše
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohádkový	pohádkový	k2eAgInSc1d1	pohádkový
děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
zednářské	zednářský	k2eAgFnSc3d1	zednářská
symbolice	symbolika	k1gFnSc3	symbolika
a	a	k8xC	a
filozofii	filozofie	k1gFnSc3	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejhranějších	hraný	k2eAgNnPc2d3	nejhranější
děl	dělo	k1gNnPc2	dělo
světového	světový	k2eAgInSc2d1	světový
operního	operní	k2eAgInSc2d1	operní
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
Sarastro	Sarastro	k1gNnSc1	Sarastro
(	(	kIx(	(
<g/>
bas	bas	k1gInSc1	bas
<g/>
)	)	kIx)	)
princ	princ	k1gMnSc1	princ
Tamino	Tamino	k1gNnSc4	Tamino
(	(	kIx(	(
<g/>
tenor	tenor	k1gInSc1	tenor
<g/>
)	)	kIx)	)
Královna	královna	k1gFnSc1	královna
noci	noc	k1gFnSc2	noc
(	(	kIx(	(
<g/>
koloraturní	koloraturní	k2eAgInSc1d1	koloraturní
soprán	soprán	k1gInSc1	soprán
<g/>
)	)	kIx)	)
dcera	dcera	k1gFnSc1	dcera
Královny	královna	k1gFnSc2	královna
noci	noc	k1gFnSc2	noc
Pamina	Pamina	k1gFnSc1	Pamina
(	(	kIx(	(
<g/>
soprán	soprán	k1gInSc1	soprán
<g/>
)	)	kIx)	)
ptáčník	ptáčník	k1gMnSc1	ptáčník
Papageno	Papagen	k2eAgNnSc4d1	Papageno
(	(	kIx(	(
<g/>
baryton	baryton	k1gInSc4	baryton
<g/>
)	)	kIx)	)
Papagena	Papagena	k1gFnSc1	Papagena
(	(	kIx(	(
<g/>
soprán	soprán	k1gInSc1	soprán
<g/>
)	)	kIx)	)
mouřenín	mouřenín	k1gMnSc1	mouřenín
Monostatos	Monostatos	k1gMnSc1	Monostatos
(	(	kIx(	(
<g/>
tenor	tenor	k1gInSc1	tenor
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
tři	tři	k4xCgFnPc1	tři
dívky	dívka	k1gFnPc1	dívka
z	z	k7c2	z
královniny	královnin	k2eAgFnSc2d1	Královnina
družiny	družina	k1gFnSc2	družina
(	(	kIx(	(
<g/>
2	[number]	k4	2
soprány	soprán	k1gInPc1	soprán
<g/>
,	,	kIx,	,
mezzosoprán	mezzosoprán	k1gInSc1	mezzosoprán
<g/>
)	)	kIx)	)
tři	tři	k4xCgMnPc1	tři
panoši	panoš	k1gMnPc1	panoš
z	z	k7c2	z
královniny	královnin	k2eAgFnSc2d1	Královnina
družiny	družina	k1gFnSc2	družina
<g/>
,	,	kIx,	,
průvodci	průvodce	k1gMnPc1	průvodce
Tamina	Tamina	k1gMnSc1	Tamina
a	a	k8xC	a
Papagena	Papagena	k1gFnSc1	Papagena
(	(	kIx(	(
<g/>
soprán	soprán	k1gInSc1	soprán
<g/>
,	,	kIx,	,
mezzosoprán	mezzosoprán	k1gInSc1	mezzosoprán
<g/>
,	,	kIx,	,
alt	alt	k1gInSc1	alt
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
...	...	k?	...
Princ	princ	k1gMnSc1	princ
Tamino	Tamino	k1gNnSc4	Tamino
prchá	prchat	k5eAaImIp3nS	prchat
před	před	k7c7	před
obrovským	obrovský	k2eAgMnSc7d1	obrovský
hadem	had	k1gMnSc7	had
<g/>
,	,	kIx,	,
unaven	unaven	k2eAgMnSc1d1	unaven
usne	usnout	k5eAaPmIp3nS	usnout
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
si	se	k3xPyFc3	se
nevšimne	všimnout	k5eNaPmIp3nS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
hada	had	k1gMnSc2	had
zabily	zabít	k5eAaPmAgFnP	zabít
tři	tři	k4xCgFnPc1	tři
dámy	dáma	k1gFnPc1	dáma
z	z	k7c2	z
družiny	družin	k2eAgFnSc2d1	družina
jisté	jistý	k2eAgFnSc2d1	jistá
královny	královna	k1gFnSc2	královna
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
princ	princ	k1gMnSc1	princ
probudí	probudit	k5eAaPmIp3nS	probudit
<g/>
,	,	kIx,	,
dámy	dáma	k1gFnPc1	dáma
odejdou	odejít	k5eAaPmIp3nP	odejít
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
před	před	k7c7	před
sebou	se	k3xPyFc7	se
spatří	spatřit	k5eAaPmIp3nS	spatřit
princ	princ	k1gMnSc1	princ
mužíka	mužík	k1gMnSc2	mužík
<g/>
,	,	kIx,	,
ptáčníka	ptáčník	k1gMnSc2	ptáčník
Papagena	Papagen	k1gMnSc2	Papagen
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vychloubá	vychloubat	k5eAaImIp3nS	vychloubat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zachránil	zachránit	k5eAaPmAgMnS	zachránit
prince	princ	k1gMnSc4	princ
před	před	k7c7	před
hadem	had	k1gMnSc7	had
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgFnP	vrátit
tři	tři	k4xCgFnPc1	tři
dámy	dáma	k1gFnPc1	dáma
a	a	k8xC	a
vykouzlily	vykouzlit	k5eAaPmAgFnP	vykouzlit
mu	on	k3xPp3gNnSc3	on
na	na	k7c6	na
ústech	ústa	k1gNnPc6	ústa
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
Královna	královna	k1gFnSc1	královna
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
daruje	darovat	k5eAaPmIp3nS	darovat
princi	princa	k1gFnSc3	princa
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
flétnu	flétna	k1gFnSc4	flétna
a	a	k8xC	a
Tamino	Tamino	k1gNnSc1	Tamino
jí	on	k3xPp3gFnSc3	on
slibuje	slibovat	k5eAaImIp3nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zachrání	zachránit	k5eAaPmIp3nP	zachránit
její	její	k3xOp3gFnSc4	její
dceru	dcera	k1gFnSc4	dcera
Paminu	Pamin	k2eAgFnSc4d1	Pamina
ze	z	k7c2	z
spárů	spár	k1gInPc2	spár
čaroděje	čaroděj	k1gMnSc2	čaroděj
Sarastra	Sarastr	k1gMnSc2	Sarastr
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázet	doprovázet	k5eAaImF	doprovázet
ho	on	k3xPp3gInSc4	on
má	mít	k5eAaImIp3nS	mít
Papageno	Papagen	k2eAgNnSc1d1	Papageno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nejdříve	dříve	k6eAd3	dříve
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
zámku	zámek	k1gInSc2	zámek
a	a	k8xC	a
dostává	dostávat	k5eAaImIp3nS	dostávat
zvonkohru	zvonkohra	k1gFnSc4	zvonkohra
<g/>
,	,	kIx,	,
po	po	k7c6	po
těch	ten	k3xDgInPc6	ten
darech	dar	k1gInPc6	dar
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
Papageno	Papagen	k2eAgNnSc1d1	Papageno
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
Tamino	Tamino	k1gNnSc4	Tamino
nevědí	vědět	k5eNaImIp3nP	vědět
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
vydat	vydat	k5eAaPmF	vydat
a	a	k8xC	a
tak	tak	k6eAd1	tak
jim	on	k3xPp3gMnPc3	on
tři	tři	k4xCgFnPc1	tři
dámy	dáma	k1gFnPc1	dáma
dají	dát	k5eAaPmIp3nP	dát
tři	tři	k4xCgMnPc4	tři
chlapce	chlapec	k1gMnPc4	chlapec
(	(	kIx(	(
<g/>
Tři	tři	k4xCgMnPc4	tři
génie	génius	k1gMnPc4	génius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gFnPc4	on
povedou	vést	k5eAaImIp3nP	vést
až	až	k6eAd1	až
do	do	k7c2	do
Sarastrovy	Sarastrův	k2eAgFnSc2d1	Sarastrův
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
hlídá	hlídat	k5eAaImIp3nS	hlídat
Paminu	Pamina	k1gFnSc4	Pamina
mouřenín	mouřenín	k1gMnSc1	mouřenín
Monostatos	Monostatos	k1gMnSc1	Monostatos
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
Pamina	Pamina	k1gFnSc1	Pamina
zalíbila	zalíbit	k5eAaPmAgFnS	zalíbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dlouho	dlouho	k6eAd1	dlouho
si	se	k3xPyFc3	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
choutky	choutka	k1gFnPc4	choutka
nedělá	dělat	k5eNaImIp3nS	dělat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
vystrašen	vystrašit	k5eAaPmNgInS	vystrašit
Papagenem	Papagen	k1gInSc7	Papagen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Sarastrovy	Sarastrův	k2eAgFnSc2d1	Sarastrův
říše	říš	k1gFnSc2	říš
dostal	dostat	k5eAaPmAgMnS	dostat
jaksi	jaksi	k6eAd1	jaksi
napřed	napřed	k6eAd1	napřed
<g/>
,	,	kIx,	,
potěší	potěšit	k5eAaPmIp3nS	potěšit
Paminu	Pamina	k1gFnSc4	Pamina
zprávou	zpráva	k1gFnSc7	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gNnSc4	její
osvobození	osvobození	k1gNnSc4	osvobození
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
a	a	k8xC	a
že	že	k8xS	že
princ	princ	k1gMnSc1	princ
Tamino	Tamino	k1gNnSc1	Tamino
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
vydal	vydat	k5eAaPmAgMnS	vydat
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
šíleně	šíleně	k6eAd1	šíleně
zamilován	zamilován	k2eAgMnSc1d1	zamilován
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
dovedli	dovést	k5eAaPmAgMnP	dovést
tři	tři	k4xCgMnPc1	tři
géniové	génius	k1gMnPc1	génius
prince	princ	k1gMnSc2	princ
Tamina	Tamin	k1gMnSc2	Tamin
až	až	k9	až
před	před	k7c4	před
palác	palác	k1gInSc4	palác
Sarastra	Sarastrum	k1gNnSc2	Sarastrum
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
hlídá	hlídat	k5eAaImIp3nS	hlídat
vchod	vchod	k1gInSc4	vchod
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
prozradí	prozradit	k5eAaPmIp3nP	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
obelstěn	obelstít	k5eAaPmNgMnS	obelstít
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sarastro	Sarastro	k1gNnSc1	Sarastro
není	být	k5eNaImIp3nS	být
zlý	zlý	k2eAgMnSc1d1	zlý
čaroděj	čaroděj	k1gMnSc1	čaroděj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čestný	čestný	k2eAgMnSc1d1	čestný
vládce	vládce	k1gMnSc1	vládce
a	a	k8xC	a
jako	jako	k8xC	jako
velekněz	velekněz	k1gMnSc1	velekněz
chrámu	chrám	k1gInSc2	chrám
moudrosti	moudrost	k1gFnSc2	moudrost
čelí	čelit	k5eAaImIp3nP	čelit
zlým	zlý	k2eAgInPc3d1	zlý
plánům	plán	k1gInPc3	plán
Královny	královna	k1gFnSc2	královna
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vydá	vydat	k5eAaPmIp3nS	vydat
hledat	hledat	k5eAaImF	hledat
Paminu	Pamina	k1gFnSc4	Pamina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
mezitím	mezitím	k6eAd1	mezitím
utekla	utéct	k5eAaPmAgFnS	utéct
s	s	k7c7	s
Papagenem	Papagen	k1gInSc7	Papagen
z	z	k7c2	z
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Tamino	Tamino	k1gNnSc4	Tamino
je	být	k5eAaImIp3nS	být
volá	volat	k5eAaImIp3nS	volat
pomocí	pomocí	k7c2	pomocí
Kouzelné	kouzelný	k2eAgFnSc2d1	kouzelná
flétny	flétna	k1gFnSc2	flétna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Pamina	Pamina	k1gFnSc1	Pamina
a	a	k8xC	a
Papageno	Papagen	k2eAgNnSc1d1	Papageno
jsou	být	k5eAaImIp3nP	být
dopadeni	dopadnout	k5eAaPmNgMnP	dopadnout
Monostatem	Monostato	k1gNnSc7	Monostato
a	a	k8xC	a
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Papageno	Papagen	k2eAgNnSc1d1	Papageno
ovšem	ovšem	k9	ovšem
zahraje	zahrát	k5eAaPmIp3nS	zahrát
na	na	k7c4	na
čarovnou	čarovný	k2eAgFnSc4d1	čarovná
zvonkohru	zvonkohra	k1gFnSc4	zvonkohra
a	a	k8xC	a
z	z	k7c2	z
otroků	otrok	k1gMnPc2	otrok
i	i	k9	i
Monostata	Monostat	k1gMnSc2	Monostat
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
veselí	veselý	k2eAgMnPc1d1	veselý
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
vzdalují	vzdalovat	k5eAaImIp3nP	vzdalovat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přichází	přicházet	k5eAaImIp3nS	přicházet
Sarastro	Sarastro	k1gNnSc1	Sarastro
<g/>
,	,	kIx,	,
princezna	princezna	k1gFnSc1	princezna
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
utéct	utéct	k5eAaPmF	utéct
kvůli	kvůli	k7c3	kvůli
matce	matka	k1gFnSc3	matka
<g/>
.	.	kIx.	.
</s>
<s>
Velekněz	velekněz	k1gMnSc1	velekněz
jí	jíst	k5eAaImIp3nS	jíst
ale	ale	k8xC	ale
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
pouze	pouze	k6eAd1	pouze
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
zlými	zlý	k2eAgFnPc7d1	zlá
mocnostmi	mocnost	k1gFnPc7	mocnost
Královny	královna	k1gFnSc2	královna
noci	noc	k1gFnSc2	noc
a	a	k8xC	a
očekává	očekávat	k5eAaImIp3nS	očekávat
setkání	setkání	k1gNnSc1	setkání
Paminy	Pamina	k1gMnSc2	Pamina
s	s	k7c7	s
Taminem	Tamin	k1gMnSc7	Tamin
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
Monostatos	Monostatos	k1gMnSc1	Monostatos
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zachází	zacházet	k5eAaImIp3nS	zacházet
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
hrubě	hrubě	k6eAd1	hrubě
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
nelíbí	líbit	k5eNaImIp3nP	líbit
Sarastrovi	Sarastrův	k2eAgMnPc1d1	Sarastrův
<g/>
,	,	kIx,	,
mouřenín	mouřenín	k1gMnSc1	mouřenín
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
potrestán	potrestat	k5eAaPmNgInS	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Pamina	Pamina	k1gFnSc1	Pamina
spatří	spatřit	k5eAaPmIp3nS	spatřit
Tamina	Tamin	k2eAgFnSc1d1	Tamin
<g/>
,	,	kIx,	,
pozná	poznat	k5eAaPmIp3nS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ten	ten	k3xDgInSc1	ten
pravý	pravý	k2eAgInSc1d1	pravý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
než	než	k8xS	než
vkročí	vkročit	k5eAaPmIp3nS	vkročit
do	do	k7c2	do
šťastného	šťastný	k2eAgInSc2d1	šťastný
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
složit	složit	k5eAaPmF	složit
několik	několik	k4yIc4	několik
očistných	očistný	k2eAgFnPc2d1	očistná
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Sarastro	Sarastro	k6eAd1	Sarastro
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
bohy	bůh	k1gMnPc4	bůh
o	o	k7c4	o
patronaci	patronace	k1gFnSc4	patronace
zkoušek	zkouška	k1gFnPc2	zkouška
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přivolán	přivolán	k2eAgMnSc1d1	přivolán
Tamino	Tamino	k1gNnSc4	Tamino
i	i	k9	i
Papageno	Papagen	k2eAgNnSc1d1	Papageno
<g/>
.	.	kIx.	.
</s>
<s>
Pamině	Pamina	k1gFnSc3	Pamina
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
nelíbí	líbit	k5eNaImIp3nS	líbit
a	a	k8xC	a
přemlouvá	přemlouvat	k5eAaImIp3nS	přemlouvat
Sarastra	Sarastra	k1gFnSc1	Sarastra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
princ	princ	k1gMnSc1	princ
zkoušek	zkouška	k1gFnPc2	zkouška
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Tamino	Tamino	k1gNnSc1	Tamino
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
řádném	řádný	k2eAgNnSc6d1	řádné
vykonání	vykonání	k1gNnSc6	vykonání
svého	svůj	k3xOyFgNnSc2	svůj
předsevzetí	předsevzetí	k1gNnSc2	předsevzetí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
zkouškou	zkouška	k1gFnSc7	zkouška
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
mlčenlivost	mlčenlivost	k1gFnSc4	mlčenlivost
<g/>
,	,	kIx,	,
Tamino	Tamin	k2eAgNnSc4d1	Tamino
a	a	k8xC	a
Papageno	Papagen	k2eAgNnSc4d1	Papageno
se	se	k3xPyFc4	se
octnou	octnout	k5eAaPmIp3nP	octnout
v	v	k7c6	v
tajemné	tajemný	k2eAgFnSc6d1	tajemná
komnatě	komnata	k1gFnSc6	komnata
<g/>
,	,	kIx,	,
odměnou	odměna	k1gFnSc7	odměna
má	mít	k5eAaImIp3nS	mít
dostat	dostat	k5eAaPmF	dostat
Papageno	Papagen	k2eAgNnSc1d1	Papageno
lásku	láska	k1gFnSc4	láska
dívky	dívka	k1gFnPc1	dívka
Papageny	Papagen	k1gInPc4	Papagen
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
ovšem	ovšem	k9	ovšem
nesmí	smět	k5eNaImIp3nP	smět
promluvit	promluvit	k5eAaPmF	promluvit
s	s	k7c7	s
dámami	dáma	k1gFnPc7	dáma
Královny	královna	k1gFnPc4	královna
noci	noc	k1gFnPc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
noci	noc	k1gFnSc2	noc
mezitím	mezitím	k6eAd1	mezitím
zabránila	zabránit	k5eAaPmAgFnS	zabránit
Monostatovi	Monostat	k1gMnSc3	Monostat
zmocnit	zmocnit	k5eAaPmF	zmocnit
se	se	k3xPyFc4	se
spící	spící	k2eAgFnSc2d1	spící
Paminy	Pamina	k1gFnSc2	Pamina
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
skryl	skrýt	k5eAaPmAgMnS	skrýt
a	a	k8xC	a
vyslechl	vyslechnout	k5eAaPmAgMnS	vyslechnout
jak	jak	k8xS	jak
Královna	královna	k1gFnSc1	královna
noci	noc	k1gFnSc2	noc
podává	podávat	k5eAaImIp3nS	podávat
dceři	dcera	k1gFnSc3	dcera
dýku	dýka	k1gFnSc4	dýka
a	a	k8xC	a
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabila	zabít	k5eAaPmAgFnS	zabít
Sarastra	Sarastra	k1gFnSc1	Sarastra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
princezna	princezna	k1gFnSc1	princezna
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
i	i	k9	i
dýku	dýka	k1gFnSc4	dýka
i	i	k8xC	i
Monostata	Monostat	k1gMnSc4	Monostat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
drzost	drzost	k1gFnSc4	drzost
opět	opět	k6eAd1	opět
potrestán	potrestat	k5eAaPmNgMnS	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušky	zkouška	k1gFnPc4	zkouška
snáší	snášet	k5eAaImIp3nS	snášet
hůře	zle	k6eAd2	zle
Papageno	Papagen	k2eAgNnSc1d1	Papageno
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
žízeň	žízeň	k1gFnSc1	žízeň
<g/>
,	,	kIx,	,
nějaká	nějaký	k3yIgFnSc1	nějaký
stařena	stařena	k1gFnSc1	stařena
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přistoupí	přistoupit	k5eAaPmIp3nP	přistoupit
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
dá	dát	k5eAaPmIp3nS	dát
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
ožení	oženit	k5eAaPmIp3nP	oženit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prý	prý	k9	prý
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
vyvolená-Papagena	vyvolená-Papagena	k1gFnSc1	vyvolená-Papagena
<g/>
!	!	kIx.	!
</s>
<s>
Ale	ale	k9	ale
ptáčník	ptáčník	k1gMnSc1	ptáčník
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
Pamina	Pamina	k1gFnSc1	Pamina
uslyší	uslyšet	k5eAaPmIp3nS	uslyšet
Kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
flétnu	flétna	k1gFnSc4	flétna
a	a	k8xC	a
přiběhne	přiběhnout	k5eAaPmIp3nS	přiběhnout
s	s	k7c7	s
radostí	radost	k1gFnSc7	radost
k	k	k7c3	k
Taminovi	Tamin	k1gMnSc3	Tamin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezná	neznat	k5eAaImIp3nS	neznat
podmínky	podmínka	k1gFnPc4	podmínka
zkoušky	zkouška	k1gFnSc2	zkouška
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
vylekána	vylekán	k2eAgFnSc1d1	vylekána
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
Tamino	Tamin	k2eAgNnSc1d1	Tamino
ani	ani	k8xC	ani
Papageno	Papagen	k2eAgNnSc1d1	Papageno
nepromluví	promluvit	k5eNaPmIp3nS	promluvit
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
oba	dva	k4xCgMnPc1	dva
složili	složit	k5eAaPmAgMnP	složit
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Pamina	Pamina	k1gMnSc1	Pamina
si	se	k3xPyFc3	se
chce	chtít	k5eAaImIp3nS	chtít
vzít	vzít	k5eAaPmF	vzít
život	život	k1gInSc4	život
dýkou	dýka	k1gFnSc7	dýka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zabrání	zabránit	k5eAaPmIp3nS	zabránit
jí	on	k3xPp3gFnSc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
Tři	tři	k4xCgMnPc1	tři
géniové	génius	k1gMnPc1	génius
<g/>
.	.	kIx.	.
</s>
<s>
Princezna	princezna	k1gFnSc1	princezna
pak	pak	k6eAd1	pak
jde	jít	k5eAaImIp3nS	jít
s	s	k7c7	s
Taminem	Tamin	k1gInSc7	Tamin
podstoupit	podstoupit	k5eAaPmF	podstoupit
poslední	poslední	k2eAgFnPc4d1	poslední
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Papageno	Papagen	k2eAgNnSc1d1	Papageno
je	být	k5eAaImIp3nS	být
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
a	a	k8xC	a
volá	volat	k5eAaImIp3nS	volat
svou	svůj	k3xOyFgFnSc4	svůj
Papagenu	Papagen	k1gInSc2	Papagen
<g/>
,	,	kIx,	,
nechce	chtít	k5eNaImIp3nS	chtít
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
chystá	chystat	k5eAaImIp3nS	chystat
oprátku	oprátka	k1gFnSc4	oprátka
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
Géniové	génius	k1gMnPc1	génius
a	a	k8xC	a
připomínají	připomínat	k5eAaImIp3nP	připomínat
mu	on	k3xPp3gMnSc3	on
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
moc	moc	k1gFnSc4	moc
zvonkohry	zvonkohra	k1gFnSc2	zvonkohra
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
krásná	krásný	k2eAgFnSc1d1	krásná
Papagena	Papagena	k1gFnSc1	Papagena
a	a	k8xC	a
těší	těšit	k5eAaImIp3nS	těšit
se	se	k3xPyFc4	se
s	s	k7c7	s
Papagenem	Papagen	k1gInSc7	Papagen
na	na	k7c4	na
spoustu	spousta	k1gFnSc4	spousta
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
noci	noc	k1gFnSc2	noc
s	s	k7c7	s
Monostatem	Monostato	k1gNnSc7	Monostato
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
na	na	k7c4	na
Chrám	chrám	k1gInSc4	chrám
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
oslněni	oslněn	k2eAgMnPc1d1	oslněn
světlem	světlo	k1gNnSc7	světlo
Sarastra	Sarastr	k1gMnSc2	Sarastr
<g/>
,	,	kIx,	,
Tamina	Tamin	k1gMnSc2	Tamin
a	a	k8xC	a
Paminy	Pamin	k2eAgFnSc2d1	Pamina
(	(	kIx(	(
<g/>
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
,	,	kIx,	,
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
<g/>
)	)	kIx)	)
a	a	k8xC	a
propadají	propadat	k5eAaPmIp3nP	propadat
se	se	k3xPyFc4	se
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
Sarastro	Sarastro	k1gNnSc4	Sarastro
zase	zase	k9	zase
uznáván	uznáván	k2eAgMnSc1d1	uznáván
jako	jako	k8xC	jako
dobrý	dobrý	k2eAgMnSc1d1	dobrý
a	a	k8xC	a
moudrý	moudrý	k2eAgMnSc1d1	moudrý
vládce	vládce	k1gMnSc1	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Tamino	Tamino	k1gNnSc1	Tamino
a	a	k8xC	a
Pamina	Pamina	k1gFnSc1	Pamina
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
jako	jako	k9	jako
Papageno	Papagen	k2eAgNnSc1d1	Papageno
a	a	k8xC	a
Papagena	Papagena	k1gFnSc1	Papagena
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
jim	on	k3xPp3gMnPc3	on
nemluví	mluvit	k5eNaImIp3nS	mluvit
do	do	k7c2	do
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
krásného	krásný	k2eAgInSc2d1	krásný
<g/>
,	,	kIx,	,
svobodného	svobodný	k2eAgInSc2d1	svobodný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Nástrojové	nástrojový	k2eAgNnSc1d1	nástrojové
obsazení	obsazení	k1gNnSc1	obsazení
opery	opera	k1gFnSc2	opera
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
2	[number]	k4	2
flétny	flétna	k1gFnSc2	flétna
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jedna	jeden	k4xCgFnSc1	jeden
alternující	alternující	k2eAgFnSc2d1	alternující
s	s	k7c7	s
pikolou	pikola	k1gFnSc7	pikola
<g/>
)	)	kIx)	)
2	[number]	k4	2
hoboje	hoboj	k1gInSc2	hoboj
2	[number]	k4	2
klarinety	klarinet	k1gInPc4	klarinet
nebo	nebo	k8xC	nebo
basetové	basetový	k2eAgInPc4d1	basetový
rohy	roh	k1gInPc4	roh
2	[number]	k4	2
fagoty	fagot	k1gInPc4	fagot
2	[number]	k4	2
lesní	lesní	k2eAgInPc1d1	lesní
rohy	roh	k1gInPc1	roh
2	[number]	k4	2
trubky	trubka	k1gFnSc2	trubka
3	[number]	k4	3
pozouny	pozouna	k1gFnSc2	pozouna
tympány	tympán	k1gInPc1	tympán
zvonkohra	zvonkohra	k1gFnSc1	zvonkohra
smyčcová	smyčcový	k2eAgFnSc1d1	smyčcová
sekce	sekce	k1gFnSc1	sekce
Dietrich	Dietrich	k1gMnSc1	Dietrich
Berke	Berke	k1gInSc1	Berke
<g/>
:	:	kIx,	:
Vorwort	Vorwort	k1gInSc1	Vorwort
zum	zum	k?	zum
Urtext	Urtext	k1gInSc1	Urtext
des	des	k1gNnSc1	des
Klavierauszugs	Klavierauszugs	k1gInSc1	Klavierauszugs
der	drát	k5eAaImRp2nS	drát
Neuen	Neuna	k1gFnPc2	Neuna
Mozart-Ausgabe	Mozart-Ausgab	k1gInSc5	Mozart-Ausgab
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc5	Die
Zauberflöte	Zauberflöt	k1gMnSc5	Zauberflöt
<g/>
,	,	kIx,	,
Bärenreiter	Bärenreiter	k1gMnSc1	Bärenreiter
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
Kassel	Kassel	k1gInSc1	Kassel
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Fritz	Fritz	k1gMnSc1	Fritz
Brukner	Brukner	k1gMnSc1	Brukner
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gMnSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc5	Die
Zauberflöte	Zauberflöt	k1gMnSc5	Zauberflöt
<g/>
.	.	kIx.	.
</s>
<s>
Unbekannte	Unbekannt	k1gMnSc5	Unbekannt
Handschriften	Handschriftno	k1gNnPc2	Handschriftno
und	und	k?	und
seltene	selten	k1gMnSc5	selten
Drucke	Druckus	k1gMnSc5	Druckus
aus	aus	k?	aus
der	drát	k5eAaImRp2nS	drát
Frühzeit	Frühzeit	k1gInSc4	Frühzeit
von	von	k1gInSc4	von
Mozarts	Mozartsa	k1gFnPc2	Mozartsa
Oper	opera	k1gFnPc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Verlag	Verlag	k1gMnSc1	Verlag
Gilhofer	Gilhofer	k1gMnSc1	Gilhofer
&	&	k?	&
Ranschburg	Ranschburg	k1gMnSc1	Ranschburg
<g/>
,	,	kIx,	,
Wien	Wien	k1gMnSc1	Wien
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Forschungsinstitut	Forschungsinstitut	k1gMnSc1	Forschungsinstitut
für	für	k?	für
Musiktheater	Musiktheater	k1gMnSc1	Musiktheater
der	drát	k5eAaImRp2nS	drát
Universität	Universität	k1gMnSc1	Universität
Bayreuth	Bayreuth	k1gMnSc1	Bayreuth
<g/>
:	:	kIx,	:
Mozarts	Mozarts	k1gInSc1	Mozarts
Opern	Opern	k1gInSc1	Opern
–	–	k?	–
Alles	Alles	k1gInSc1	Alles
von	von	k1gInSc1	von
'	'	kIx"	'
<g/>
Apollo	Apollo	k1gMnSc1	Apollo
und	und	k?	und
Hyacinth	Hyacinth	k1gMnSc1	Hyacinth
<g/>
'	'	kIx"	'
bis	bis	k?	bis
zur	zur	k?	zur
'	'	kIx"	'
<g/>
Zauberflöte	Zauberflöt	k1gInSc5	Zauberflöt
<g/>
'	'	kIx"	'
Attila	Attila	k1gMnSc1	Attila
Csampai	Csampa	k1gFnSc2	Csampa
<g/>
,	,	kIx,	,
Dietmar	Dietmar	k1gMnSc1	Dietmar
Holland	Holland	k1gInSc1	Holland
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gInSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc5	Die
Zauberflöte	Zauberflöt	k1gMnSc5	Zauberflöt
<g/>
.	.	kIx.	.
</s>
<s>
Rowohlt	Rowohlt	k1gMnSc1	Rowohlt
<g/>
,	,	kIx,	,
Reinbek	Reinbek	k1gMnSc1	Reinbek
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-499-17476-6	[number]	k4	3-499-17476-6
Helmut	Helmut	k2eAgInSc4d1	Helmut
Perl	perl	k1gInSc4	perl
<g/>
:	:	kIx,	:
Der	drát	k5eAaImRp2nS	drát
Fall	Fall	k1gInSc4	Fall
"	"	kIx"	"
<g/>
Zauberflöte	Zauberflöt	k1gMnSc5	Zauberflöt
<g/>
"	"	kIx"	"
WBG	WBG	kA	WBG
Darmstadt	Darmstadt	k1gInSc4	Darmstadt
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-254-00266-0	[number]	k4	3-254-00266-0
Der	drát	k5eAaImRp2nS	drát
Zauberfloete	Zauberfloe	k1gNnSc2	Zauberfloe
zweyter	zweyter	k1gMnSc1	zweyter
Theil	Theil	k1gMnSc1	Theil
unter	untra	k1gFnPc2	untra
dem	dem	k?	dem
Titel	titla	k1gFnPc2	titla
<g/>
:	:	kIx,	:
Das	Das	k1gFnSc1	Das
Labyrinth	Labyrintha	k1gFnPc2	Labyrintha
oder	odra	k1gFnPc2	odra
der	drát	k5eAaImRp2nS	drát
Kampf	Kampf	k1gInSc1	Kampf
mit	mit	k?	mit
den	den	k1gInSc4	den
Elementen	Elementen	k2eAgInSc4d1	Elementen
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
autora	autor	k1gMnSc2	autor
Peter	Petra	k1gFnPc2	Petra
von	von	k1gInSc1	von
Winter	Winter	k1gMnSc1	Winter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
Manuela	Manuela	k1gFnSc1	Manuela
Jahrmärker	Jahrmärker	k1gMnSc1	Jahrmärker
a	a	k8xC	a
Till	Till	k1gMnSc1	Till
Gerrit	Gerrita	k1gFnPc2	Gerrita
Waidelich	Waidelich	k1gInSc4	Waidelich
<g/>
,	,	kIx,	,
Tutzing	Tutzing	k1gInSc4	Tutzing
1992	[number]	k4	1992
David	David	k1gMnSc1	David
Buch	buch	k1gInSc1	buch
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Die	Die	k1gMnSc5	Die
Zauberflöte	Zauberflöt	k1gMnSc5	Zauberflöt
<g/>
,	,	kIx,	,
Masonic	Masonice	k1gFnPc2	Masonice
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
and	and	k?	and
Other	Other	k1gMnSc1	Other
Fairy	Faira	k1gFnSc2	Faira
Tales	Tales	k1gMnSc1	Tales
in	in	k?	in
Acta	Acta	k1gMnSc1	Acta
Musicologica	Musicologica	k1gMnSc1	Musicologica
76	[number]	k4	76
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Assmann	Assmann	k1gMnSc1	Assmann
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc5	Die
Zauberflöte	Zauberflöt	k1gMnSc5	Zauberflöt
<g/>
.	.	kIx.	.
</s>
<s>
Oper	opera	k1gFnPc2	opera
und	und	k?	und
Mysterium	mysterium	k1gNnSc1	mysterium
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Carl	Carl	k1gMnSc1	Carl
Hanser	Hanser	k1gMnSc1	Hanser
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
München	München	k2eAgMnSc1d1	München
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
446	[number]	k4	446
<g/>
-	-	kIx~	-
<g/>
20673	[number]	k4	20673
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
383	[number]	k4	383
s.	s.	k?	s.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Recenze	recenze	k1gFnSc1	recenze
"	"	kIx"	"
<g/>
Die	Die	k1gMnSc5	Die
Zauberflöte	Zauberflöt	k1gMnSc5	Zauberflöt
<g/>
"	"	kIx"	"
na	na	k7c4	na
Perlentaucher	Perlentauchra	k1gFnPc2	Perlentauchra
Heinrich	Heinrich	k1gMnSc1	Heinrich
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
<g/>
:	:	kIx,	:
Das	Das	k1gMnSc1	Das
Mysterium	mysterium	k1gNnSc4	mysterium
der	drát	k5eAaImRp2nS	drát
Zauberflöte	Zauberflöt	k1gMnSc5	Zauberflöt
-	-	kIx~	-
Die	Die	k1gMnSc6	Die
geheime	geheimat	k5eAaPmIp3nS	geheimat
freimaurerische	freimaurerische	k1gInSc1	freimaurerische
Symbolsprache	Symbolsprache	k1gNnSc2	Symbolsprache
Mozarts	Mozartsa	k1gFnPc2	Mozartsa
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
MdG-Verlag	MdG-Verlag	k1gMnSc1	MdG-Verlag
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Claudio	Claudio	k6eAd1	Claudio
Abbado	Abbada	k1gFnSc5	Abbada
<g/>
,	,	kIx,	,
Mahler	Mahler	k1gMnSc1	Mahler
Chamber	Chamber	k1gMnSc1	Chamber
Orchestra	orchestra	k1gFnSc1	orchestra
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
;	;	kIx,	;
Hanno	Hanen	k2eAgNnSc1d1	Hanno
Müller-Brachmann	Müller-Brachmann	k1gNnSc1	Müller-Brachmann
-	-	kIx~	-
Papageno	Papagen	k2eAgNnSc1d1	Papageno
<g/>
,	,	kIx,	,
Erika	Erika	k1gFnSc1	Erika
Miklósa	Miklósa	k1gFnSc1	Miklósa
-	-	kIx~	-
Königin	Königin	k1gInSc1	Königin
der	drát	k5eAaImRp2nS	drát
Nacht	Nacht	k1gMnSc1	Nacht
<g/>
,	,	kIx,	,
René	René	k1gMnSc1	René
Pape	Pap	k1gFnSc2	Pap
-	-	kIx~	-
Sarastro	Sarastro	k1gNnSc1	Sarastro
<g/>
,	,	kIx,	,
Dorothea	Dorothea	k1gFnSc1	Dorothea
Röschmann	Röschmann	k1gMnSc1	Röschmann
-	-	kIx~	-
Pamina	Pamina	k1gMnSc1	Pamina
<g/>
,	,	kIx,	,
Christoph	Christoph	k1gMnSc1	Christoph
Strehl	Strehl	k1gMnSc1	Strehl
-	-	kIx~	-
Tamino	Tamino	k1gNnSc1	Tamino
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Deutsche	Deutsche	k1gFnSc1	Deutsche
Grammophon	Grammophona	k1gFnPc2	Grammophona
<g/>
.	.	kIx.	.
</s>
<s>
Nikolaus	Nikolaus	k1gInSc1	Nikolaus
Harnoncourt	Harnoncourta	k1gFnPc2	Harnoncourta
<g/>
,	,	kIx,	,
Chor	Chora	k1gFnPc2	Chora
und	und	k?	und
Orchester	orchestra	k1gFnPc2	orchestra
des	des	k1gNnSc1	des
Opernhauses	Opernhauses	k1gMnSc1	Opernhauses
Zürich	Zürich	k1gMnSc1	Zürich
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
;	;	kIx,	;
Edita	Edita	k1gFnSc1	Edita
Gruberová	Gruberová	k1gFnSc1	Gruberová
-	-	kIx~	-
Königin	Königin	k1gInSc1	Königin
der	drát	k5eAaImRp2nS	drát
Nacht	Nacht	k1gInSc1	Nacht
<g/>
,	,	kIx,	,
Barbara	Barbara	k1gFnSc1	Barbara
Bonney	Bonnea	k1gFnSc2	Bonnea
-	-	kIx~	-
Pamina	Pamina	k1gFnSc1	Pamina
<g/>
,	,	kIx,	,
Matti	Matti	k1gNnSc1	Matti
Salminen	Salminna	k1gFnPc2	Salminna
<g/>
,	,	kIx,	,
Hans-Peter	Hans-Peter	k1gMnSc1	Hans-Peter
Blochwitz	Blochwitz	k1gMnSc1	Blochwitz
<g/>
,	,	kIx,	,
Anton	Anton	k1gMnSc1	Anton
Scharinger	Scharinger	k1gMnSc1	Scharinger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teldec	Teldec	k1gMnSc1	Teldec
<g/>
.	.	kIx.	.
</s>
<s>
Karl	Karl	k1gMnSc1	Karl
Böhm	Böhm	k1gMnSc1	Böhm
<g/>
,	,	kIx,	,
Berlínští	berlínský	k2eAgMnPc1d1	berlínský
filharmonikové	filharmonik	k1gMnPc1	filharmonik
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
;	;	kIx,	;
Fritz	Fritz	k1gInSc1	Fritz
Wunderlich	Wunderlich	k1gInSc1	Wunderlich
-	-	kIx~	-
Tamino	Tamino	k1gNnSc1	Tamino
<g/>
,	,	kIx,	,
Evelyn	Evelyn	k1gNnSc1	Evelyn
Lear	Leara	k1gFnPc2	Leara
-	-	kIx~	-
Pamina	Pamina	k1gFnSc1	Pamina
<g/>
,	,	kIx,	,
Roberta	Roberta	k1gFnSc1	Roberta
Peters	Peters	k1gInSc1	Peters
-	-	kIx~	-
Königin	Königin	k1gInSc1	Königin
der	drát	k5eAaImRp2nS	drát
Nacht	Nacht	k1gMnSc1	Nacht
<g/>
,	,	kIx,	,
Dietrich	Dietrich	k1gMnSc1	Dietrich
Fischer-Dieskau	Fischer-Dieskaus	k1gInSc2	Fischer-Dieskaus
-	-	kIx~	-
Papageno	Papagen	k2eAgNnSc1d1	Papageno
<g/>
,	,	kIx,	,
Franz	Franz	k1gInSc1	Franz
Crass	Crass	k1gInSc1	Crass
-	-	kIx~	-
Sarastro	Sarastro	k1gNnSc1	Sarastro
<g/>
,	,	kIx,	,
weiters	weiters	k1gInSc1	weiters
<g/>
:	:	kIx,	:
Friedrich	Friedrich	k1gMnSc1	Friedrich
Lenz	Lenz	k1gMnSc1	Lenz
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
King	King	k1gMnSc1	King
<g/>
,	,	kIx,	,
Martti	Martti	k1gNnSc1	Martti
Talvela	Talvela	k1gFnSc1	Talvela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Deutsche	Deutsche	k1gFnSc1	Deutsche
Grammophon	Grammophona	k1gFnPc2	Grammophona
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
Klemperer	Klemperra	k1gFnPc2	Klemperra
<g/>
,	,	kIx,	,
Philharmonia	Philharmonium	k1gNnSc2	Philharmonium
Orchestra	orchestra	k1gFnSc1	orchestra
&	&	k?	&
Chorus	chorus	k1gInSc1	chorus
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
;	;	kIx,	;
Gottlob	Gottloba	k1gFnPc2	Gottloba
Frick	Frick	k1gInSc1	Frick
<g/>
,	,	kIx,	,
Nicolai	Nicolai	k1gNnSc1	Nicolai
Gedda	Gedda	k1gMnSc1	Gedda
<g/>
,	,	kIx,	,
Waler	Waler	k1gMnSc1	Waler
Berry	Berra	k1gFnSc2	Berra
<g/>
,	,	kIx,	,
Elisabeth	Elisabeth	k1gMnSc1	Elisabeth
Schwarzkopf	Schwarzkopf	k1gMnSc1	Schwarzkopf
<g/>
,	,	kIx,	,
Christa	Christa	k1gMnSc1	Christa
Ludwig	Ludwig	k1gMnSc1	Ludwig
<g/>
,	,	kIx,	,
Lucia	Lucia	k1gFnSc1	Lucia
Popp	Popp	k1gInSc1	Popp
<g/>
,	,	kIx,	,
Gundula	Gundula	k1gFnSc1	Gundula
Janowitz	Janowitz	k1gInSc1	Janowitz
<g/>
,	,	kIx,	,
Marga	Marga	k1gFnSc1	Marga
Höffgen	Höffgen	k1gInSc1	Höffgen
<g/>
,	,	kIx,	,
Franz	Franz	k1gInSc1	Franz
Crass	Crass	k1gInSc1	Crass
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
EMI	EMI	kA	EMI
<g/>
.	.	kIx.	.
</s>
<s>
Vittorio	Vittorio	k1gNnSc1	Vittorio
Gui	Gui	k1gFnPc2	Gui
<g/>
,	,	kIx,	,
Orchester	orchestra	k1gFnPc2	orchestra
der	drát	k5eAaImRp2nS	drát
Wiener	Wiener	k1gMnSc1	Wiener
Staatsoper	Staatsoper	k1gMnSc1	Staatsoper
<g/>
,	,	kIx,	,
Internationalen	Internationalen	k2eAgInSc1d1	Internationalen
Stiftung	Stiftung	k1gInSc1	Stiftung
Mozarteum	Mozarteum	k1gNnSc1	Mozarteum
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
live	live	k1gFnSc1	live
<g/>
/	/	kIx~	/
<g/>
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
,	,	kIx,	,
Pamina	Pamina	k1gFnSc1	Pamina
Sena	sena	k1gFnSc1	sena
Jurinac	Jurinac	k1gFnSc1	Jurinac
<g/>
,	,	kIx,	,
Königin	Königin	k2eAgInSc1d1	Königin
der	drát	k5eAaImRp2nS	drát
Nacht	Nachtum	k1gNnPc2	Nachtum
Mimi	mimi	k1gNnSc7	mimi
Coertse	Coertse	k1gFnSc1	Coertse
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Oncina	Oncina	k1gMnSc1	Oncina
<g/>
,	,	kIx,	,
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Taddei	Tadde	k1gMnSc5	Tadde
<g/>
,	,	kIx,	,
Sarastro	Sarastro	k1gNnSc1	Sarastro
Boris	Boris	k1gMnSc1	Boris
Christoff	Christoff	k1gMnSc1	Christoff
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
:	:	kIx,	:
House	house	k1gNnSc1	house
of	of	k?	of
Opera	opera	k1gFnSc1	opera
ALD3013	ALD3013	k1gMnSc1	ALD3013
Karl	Karl	k1gMnSc1	Karl
Böhm	Böhm	k1gMnSc1	Böhm
<g/>
,	,	kIx,	,
Vídeňští	vídeňský	k2eAgMnPc1d1	vídeňský
filharmonikové	filharmonik	k1gMnPc1	filharmonik
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
;	;	kIx,	;
Léopold	Léopold	k1gInSc4	Léopold
Simoneau	Simoneaa	k1gFnSc4	Simoneaa
-	-	kIx~	-
Tamino	Tamina	k1gMnSc5	Tamina
<g/>
,	,	kIx,	,
Hilde	Hild	k1gMnSc5	Hild
Güden	Güden	k1gInSc1	Güden
-	-	kIx~	-
Pamina	Pamina	k1gFnSc1	Pamina
<g/>
,	,	kIx,	,
Wilma	Wilma	k1gFnSc1	Wilma
Lipp	Lipp	k1gMnSc1	Lipp
-	-	kIx~	-
Königin	Königin	k1gMnSc1	Königin
d.	d.	k?	d.
Nacht	Nacht	k1gMnSc1	Nacht
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
Berry	Berra	k1gFnSc2	Berra
-	-	kIx~	-
Papageno	Papagen	k2eAgNnSc4d1	Papageno
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Böhme	Böhm	k1gInSc5	Böhm
-	-	kIx~	-
Sarastro	Sarastro	k1gNnSc1	Sarastro
<g/>
,	,	kIx,	,
weiters	weiters	k1gInSc1	weiters
<g/>
:	:	kIx,	:
Emmy	Emma	k1gFnSc2	Emma
Loose	Loose	k1gFnSc2	Loose
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
Jaresch	Jaresch	k1gMnSc1	Jaresch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Decca	Decca	k6eAd1	Decca
<g/>
.	.	kIx.	.
</s>
<s>
Ferenc	Ferenc	k1gMnSc1	Ferenc
Fricsay	Fricsaa	k1gFnSc2	Fricsaa
<g/>
,	,	kIx,	,
RIAS-Symphonie-Orchester	RIAS-Symphonie-Orchester	k1gInSc4	RIAS-Symphonie-Orchester
Berlin	berlina	k1gFnPc2	berlina
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Berlin	berlina	k1gFnPc2	berlina
1954	[number]	k4	1954
Sarastro	Sarastro	k1gNnSc1	Sarastro
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Greindl	Greindl	k1gMnSc1	Greindl
<g/>
;	;	kIx,	;
Tamino	Tamino	k1gNnSc1	Tamino
<g/>
:	:	kIx,	:
Ernst	Ernst	k1gMnSc1	Ernst
Haefliger	Haefliger	k1gMnSc1	Haefliger
<g/>
;	;	kIx,	;
Königin	Königin	k1gMnSc1	Königin
der	drát	k5eAaImRp2nS	drát
Nacht	Nacht	k1gInSc1	Nacht
<g/>
:	:	kIx,	:
Rita	Rita	k1gFnSc1	Rita
Streich	Streich	k1gMnSc1	Streich
<g/>
;	;	kIx,	;
Pamina	Pamina	k1gMnSc1	Pamina
<g/>
:	:	kIx,	:
Maria	Maria	k1gFnSc1	Maria
Stader	Stader	k1gInSc1	Stader
<g/>
;	;	kIx,	;
Papageno	Papagen	k2eAgNnSc1d1	Papageno
<g/>
:	:	kIx,	:
Dietrich	Dietrich	k1gInSc1	Dietrich
Fischer-Dieskau	Fischer-Dieskaus	k1gInSc2	Fischer-Dieskaus
<g/>
;	;	kIx,	;
Papagena	Papagena	k1gFnSc1	Papagena
<g/>
:	:	kIx,	:
Lisa	Lisa	k1gFnSc1	Lisa
Otto	Otto	k1gMnSc1	Otto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Deutsche	Deutsche	k1gNnSc1	Deutsche
Grammophon	Grammophona	k1gFnPc2	Grammophona
DG	dg	kA	dg
<g/>
.	.	kIx.	.
</s>
<s>
Arturo	Artura	k1gFnSc5	Artura
Toscanini	Toscanin	k2eAgMnPc1d1	Toscanin
<g/>
,	,	kIx,	,
Vídeňští	vídeňský	k2eAgMnPc1d1	vídeňský
filharmonikové	filharmonik	k1gMnPc1	filharmonik
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Salzburg	Salzburg	k1gInSc1	Salzburg
1937	[number]	k4	1937
Sarastro	Sarastro	k1gNnSc1	Sarastro
<g/>
:	:	kIx,	:
Alexander	Alexandra	k1gFnPc2	Alexandra
Kipnis	Kipnis	k1gInSc1	Kipnis
<g/>
;	;	kIx,	;
Tamino	Tamino	k1gNnSc1	Tamino
<g/>
:	:	kIx,	:
Helge	Helge	k1gFnSc1	Helge
Rosvaenge	Rosvaenge	k1gFnSc1	Rosvaenge
<g/>
;	;	kIx,	;
Königin	Königin	k1gInSc1	Königin
der	drát	k5eAaImRp2nS	drát
Nacht	Nacht	k1gInSc1	Nacht
<g/>
:	:	kIx,	:
Julie	Julie	k1gFnSc1	Julie
Osváth	Osváth	k1gMnSc1	Osváth
<g/>
;	;	kIx,	;
Pamina	Pamina	k1gMnSc1	Pamina
<g/>
:	:	kIx,	:
Jarmila	Jarmila	k1gFnSc1	Jarmila
Novotná	Novotná	k1gFnSc1	Novotná
<g/>
;	;	kIx,	;
Papageno	Papagen	k2eAgNnSc1d1	Papageno
<g/>
:	:	kIx,	:
Willi	Wille	k1gFnSc4	Wille
Domgraf-Fassbaender	Domgraf-Fassbaendra	k1gFnPc2	Domgraf-Fassbaendra
<g/>
;	;	kIx,	;
Papagena	Papagena	k1gFnSc1	Papagena
<g/>
:	:	kIx,	:
Dora	Dora	k1gFnSc1	Dora
Komarek	Komarka	k1gFnPc2	Komarka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naxos	Naxos	k1gInSc1	Naxos
<g/>
.	.	kIx.	.
filmový	filmový	k2eAgInSc1d1	filmový
záznam	záznam	k1gInSc1	záznam
uvedení	uvedení	k1gNnSc2	uvedení
opery	opera	k1gFnSc2	opera
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Bregenzu	Bregenz	k1gInSc6	Bregenz
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Bodamského	bodamský	k2eAgNnSc2d1	Bodamské
jezera	jezero	k1gNnSc2	jezero
ze	z	k7c2	z
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
Anna	Anna	k1gFnSc1	Anna
Durlovski	Durlovsk	k1gFnSc2	Durlovsk
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
Schmutzhard	Schmutzhard	k1gMnSc1	Schmutzhard
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Reiter	Reiter	k1gMnSc1	Reiter
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
Reinhardt	Reinhardt	k1gMnSc1	Reinhardt
a	a	k8xC	a
Bernarda	Bernard	k1gMnSc2	Bernard
Bobro	Bobro	k1gNnSc4	Bobro
<g/>
.	.	kIx.	.
</s>
<s>
Spoluúčinkuje	spoluúčinkovat	k5eAaImIp3nS	spoluúčinkovat
Pražský	pražský	k2eAgInSc1d1	pražský
filharmonický	filharmonický	k2eAgInSc1d1	filharmonický
sbor	sbor	k1gInSc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
Vídeňských	vídeňský	k2eAgMnPc2d1	vídeňský
symfoniků	symfonik	k1gMnPc2	symfonik
řídí	řídit	k5eAaImIp3nS	řídit
Patrick	Patrick	k1gMnSc1	Patrick
Summer	Summer	k1gMnSc1	Summer
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Pountney	Pountnea	k1gFnSc2	Pountnea
<g/>
.	.	kIx.	.
</s>
