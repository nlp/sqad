<s>
Hodiny	hodina	k1gFnPc1	hodina
(	(	kIx(	(
<g/>
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
slova	slovo	k1gNnSc2	slovo
hodina	hodina	k1gFnSc1	hodina
<g/>
,	,	kIx,	,
z	z	k7c2	z
praslovanského	praslovanský	k2eAgNnSc2d1	praslovanské
godъ	godъ	k?	godъ
výročí	výročí	k1gNnSc2	výročí
<g/>
,	,	kIx,	,
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
přístroje	přístroj	k1gInPc4	přístroj
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Doplňkově	doplňkově	k6eAd1	doplňkově
může	moct	k5eAaImIp3nS	moct
ukazovat	ukazovat	k5eAaImF	ukazovat
i	i	k9	i
kalendářní	kalendářní	k2eAgInPc4d1	kalendářní
údaje	údaj	k1gInPc4	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
přenosné	přenosný	k2eAgFnPc1d1	přenosná
<g/>
,	,	kIx,	,
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
hodinky	hodinka	k1gFnPc1	hodinka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
přesné	přesný	k2eAgFnPc4d1	přesná
hodiny	hodina	k1gFnPc4	hodina
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
název	název	k1gInSc1	název
chronometr	chronometr	k1gInSc1	chronometr
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
indikuje	indikovat	k5eAaBmIp3nS	indikovat
pomocí	pomocí	k7c2	pomocí
ruček	ručka	k1gFnPc2	ručka
na	na	k7c6	na
ciferníku	ciferník	k1gInSc6	ciferník
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
u	u	k7c2	u
elektronických	elektronický	k2eAgFnPc2d1	elektronická
hodin	hodina	k1gFnPc2	hodina
číslicovým	číslicový	k2eAgInSc7d1	číslicový
displejem	displej	k1gInSc7	displej
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odměřování	odměřování	k1gNnSc4	odměřování
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
kratšího	krátký	k2eAgInSc2d2	kratší
časového	časový	k2eAgInSc2d1	časový
úseku	úsek	k1gInSc2	úsek
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nP	sloužit
stopky	stopka	k1gFnPc1	stopka
<g/>
.	.	kIx.	.
</s>
<s>
Ciferník	ciferník	k1gInSc1	ciferník
moderních	moderní	k2eAgFnPc2d1	moderní
ručkových	ručkový	k2eAgFnPc2d1	Ručková
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
dělen	dělit	k5eAaImNgInS	dělit
na	na	k7c4	na
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
časový	časový	k2eAgInSc1d1	časový
údaj	údaj	k1gInSc1	údaj
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
obvykle	obvykle	k6eAd1	obvykle
kratší	krátký	k2eAgFnSc1d2	kratší
hodinová	hodinový	k2eAgFnSc1d1	hodinová
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
souosá	souosý	k2eAgFnSc1d1	souosá
minutová	minutový	k2eAgFnSc1d1	minutová
ručka	ručka	k1gFnSc1	ručka
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
mají	mít	k5eAaImIp3nP	mít
ještě	ještě	k6eAd1	ještě
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
sekundovou	sekundový	k2eAgFnSc4d1	sekundová
ručku	ručka	k1gFnSc4	ručka
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
ciferníku	ciferník	k1gInSc2	ciferník
nebo	nebo	k8xC	nebo
rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgFnSc6d1	centrální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1	mechanická
i	i	k8xC	i
elektronické	elektronický	k2eAgFnPc1d1	elektronická
hodiny	hodina	k1gFnPc1	hodina
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
konstantní	konstantní	k2eAgFnPc1d1	konstantní
jednotky	jednotka	k1gFnPc1	jednotka
času	čas	k1gInSc2	čas
<g/>
:	:	kIx,	:
hodina	hodina	k1gFnSc1	hodina
má	mít	k5eAaImIp3nS	mít
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
minuta	minuta	k1gFnSc1	minuta
60	[number]	k4	60
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měření	měření	k1gNnSc2	měření
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
lze	lze	k6eAd1	lze
měřit	měřit	k5eAaImF	měřit
dvojím	dvojit	k5eAaImIp1nS	dvojit
podstatně	podstatně	k6eAd1	podstatně
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
přímým	přímý	k2eAgNnSc7d1	přímé
sledováním	sledování	k1gNnSc7	sledování
a	a	k8xC	a
indikací	indikace	k1gFnSc7	indikace
rovnoměrného	rovnoměrný	k2eAgInSc2d1	rovnoměrný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
:	:	kIx,	:
místní	místní	k2eAgInSc1d1	místní
čas	čas	k1gInSc1	čas
indikují	indikovat	k5eAaBmIp3nP	indikovat
sluneční	sluneční	k2eAgFnPc1d1	sluneční
hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc4	jejich
předchůdce	předchůdce	k1gMnPc4	předchůdce
gnómon	gnómon	k1gInSc1	gnómon
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
polohu	poloha	k1gFnSc4	poloha
Slunce	slunce	k1gNnSc2	slunce
vrženým	vržený	k2eAgInSc7d1	vržený
stínem	stín	k1gInSc7	stín
tyče	tyč	k1gFnSc2	tyč
Jiné	jiný	k2eAgFnSc2d1	jiná
konstrukce	konstrukce	k1gFnSc2	konstrukce
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odměřování	odměřování	k1gNnSc3	odměřování
časových	časový	k2eAgInPc2d1	časový
intervalů	interval	k1gInPc2	interval
<g/>
:	:	kIx,	:
Přesýpací	přesýpací	k2eAgFnPc1d1	přesýpací
hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
starší	starý	k2eAgFnPc1d2	starší
vodní	vodní	k2eAgFnPc1d1	vodní
hodiny	hodina	k1gFnPc1	hodina
(	(	kIx(	(
<g/>
klepsydra	klepsydra	k1gFnSc1	klepsydra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sleduje	sledovat	k5eAaImIp3nS	sledovat
pokles	pokles	k1gInSc1	pokles
hladiny	hladina	k1gFnSc2	hladina
kapaliny	kapalina	k1gFnSc2	kapalina
nebo	nebo	k8xC	nebo
písku	písek	k1gInSc2	písek
v	v	k7c6	v
průhledné	průhledný	k2eAgFnSc6d1	průhledná
nádrži	nádrž	k1gFnSc6	nádrž
<g/>
;	;	kIx,	;
svíčkové	svíčkový	k2eAgFnSc2d1	svíčková
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
čas	čas	k1gInSc1	čas
odečítá	odečítat	k5eAaImIp3nS	odečítat
podle	podle	k7c2	podle
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
odhořívání	odhořívání	k1gNnSc2	odhořívání
svíčky	svíčka	k1gFnSc2	svíčka
apod.	apod.	kA	apod.
I	i	k9	i
když	když	k8xS	když
údaj	údaj	k1gInSc4	údaj
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
nemůže	moct	k5eNaImIp3nS	moct
od	od	k7c2	od
místního	místní	k2eAgInSc2d1	místní
slunečního	sluneční	k2eAgInSc2d1	sluneční
času	čas	k1gInSc2	čas
nijak	nijak	k6eAd1	nijak
odchýlit	odchýlit	k5eAaPmF	odchýlit
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
odečítat	odečítat	k5eAaImF	odečítat
v	v	k7c6	v
nejlepším	dobrý	k2eAgInSc6d3	nejlepší
případě	případ	k1gInSc6	případ
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
moderní	moderní	k2eAgFnSc2d1	moderní
hodiny	hodina	k1gFnSc2	hodina
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
výjimek	výjimka	k1gFnPc2	výjimka
užívají	užívat	k5eAaImIp3nP	užívat
druhý	druhý	k4xOgInSc4	druhý
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
počítáním	počítání	k1gNnSc7	počítání
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
mechanických	mechanický	k2eAgFnPc2d1	mechanická
<g/>
,	,	kIx,	,
elektrických	elektrický	k2eAgFnPc2d1	elektrická
nebo	nebo	k8xC	nebo
atomárních	atomární	k2eAgFnPc2d1	atomární
oscilací	oscilace	k1gFnPc2	oscilace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pohonu	pohon	k1gInSc2	pohon
a	a	k8xC	a
konstrukce	konstrukce	k1gFnSc2	konstrukce
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
Mechanické	mechanický	k2eAgFnPc4d1	mechanická
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
stacionární	stacionární	k2eAgFnPc4d1	stacionární
(	(	kIx(	(
<g/>
nepřenosné	přenosný	k2eNgFnPc4d1	nepřenosná
<g/>
)	)	kIx)	)
kyvadlové	kyvadlový	k2eAgFnPc4d1	kyvadlová
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
věžní	věžní	k2eAgFnPc4d1	věžní
hodiny	hodina	k1gFnPc4	hodina
<g/>
;	;	kIx,	;
přenosné	přenosný	k2eAgFnPc4d1	přenosná
hodiny	hodina	k1gFnPc4	hodina
a	a	k8xC	a
Hodinky	hodinka	k1gFnPc4	hodinka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
řízené	řízený	k2eAgFnPc1d1	řízená
setrvačkou	setrvačka	k1gFnSc7	setrvačka
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
nepokojem	nepokoj	k1gInSc7	nepokoj
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
námořní	námořní	k2eAgInPc4d1	námořní
a	a	k8xC	a
letecké	letecký	k2eAgInPc4d1	letecký
chronometry	chronometr	k1gInPc4	chronometr
<g/>
;	;	kIx,	;
elektrické	elektrický	k2eAgFnPc4d1	elektrická
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
poháněné	poháněný	k2eAgMnPc4d1	poháněný
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
<g/>
;	;	kIx,	;
Elektronické	elektronický	k2eAgFnPc4d1	elektronická
hodiny	hodina	k1gFnPc4	hodina
a	a	k8xC	a
hodinky	hodinka	k1gFnPc4	hodinka
s	s	k7c7	s
elektronickým	elektronický	k2eAgInSc7d1	elektronický
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
křemenným	křemenný	k2eAgInSc7d1	křemenný
krystalem	krystal	k1gInSc7	krystal
<g/>
;	;	kIx,	;
Atomové	atomový	k2eAgFnPc4d1	atomová
hodiny	hodina	k1gFnPc4	hodina
jako	jako	k8xC	jako
nejpřesnější	přesný	k2eAgInPc4d3	nejpřesnější
normály	normál	k1gInPc4	normál
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
sice	sice	k8xC	sice
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
přesnosti	přesnost	k1gFnSc6	přesnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
nepřesnosti	nepřesnost	k1gFnSc2	nepřesnost
<g/>
"	"	kIx"	"
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
různé	různý	k2eAgFnPc4d1	různá
veličiny	veličina	k1gFnPc4	veličina
<g/>
:	:	kIx,	:
Chod	chod	k1gInSc1	chod
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
odchylku	odchylka	k1gFnSc4	odchylka
od	od	k7c2	od
normálu	normál	k1gInSc2	normál
<g/>
;	;	kIx,	;
hodiny	hodina	k1gFnPc1	hodina
se	se	k3xPyFc4	se
předcházejí	předcházet	k5eAaImIp3nP	předcházet
nebo	nebo	k8xC	nebo
zpožďují	zpožďovat	k5eAaImIp3nP	zpožďovat
například	například	k6eAd1	například
o	o	k7c4	o
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
nebo	nebo	k8xC	nebo
sekund	sekunda	k1gFnPc2	sekunda
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
chyba	chyba	k1gFnSc1	chyba
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
odchylkou	odchylka	k1gFnSc7	odchylka
skutečné	skutečný	k2eAgFnSc2d1	skutečná
frekvence	frekvence	k1gFnSc2	frekvence
oscilátoru	oscilátor	k1gInSc2	oscilátor
(	(	kIx(	(
<g/>
kyvadla	kyvadlo	k1gNnPc4	kyvadlo
<g/>
,	,	kIx,	,
setrvačky	setrvačka	k1gFnPc4	setrvačka
<g/>
)	)	kIx)	)
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
konstruktér	konstruktér	k1gMnSc1	konstruktér
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Variaci	variace	k1gFnSc4	variace
chodu	chod	k1gInSc2	chod
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
isochronie	isochronie	k1gFnSc2	isochronie
<g/>
,	,	kIx,	,
nepravidelnost	nepravidelnost	k1gFnSc1	nepravidelnost
chodu	chod	k1gInSc2	chod
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
této	tento	k3xDgFnSc2	tento
odchylky	odchylka	k1gFnSc2	odchylka
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
nestálost	nestálost	k1gFnSc1	nestálost
oscilátoru	oscilátor	k1gInSc2	oscilátor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
frekvence	frekvence	k1gFnSc1	frekvence
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
mění	měnit	k5eAaImIp3nS	měnit
-	-	kIx~	-
například	například	k6eAd1	například
s	s	k7c7	s
polohou	poloha	k1gFnSc7	poloha
<g/>
,	,	kIx,	,
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
chod	chod	k1gInSc1	chod
lze	lze	k6eAd1	lze
regulací	regulace	k1gFnSc7	regulace
upravit	upravit	k5eAaPmF	upravit
<g/>
,	,	kIx,	,
variace	variace	k1gFnSc1	variace
je	být	k5eAaImIp3nS	být
dána	dán	k2eAgFnSc1d1	dána
kvalitou	kvalita	k1gFnSc7	kvalita
a	a	k8xC	a
konstrukcí	konstrukce	k1gFnSc7	konstrukce
stroje	stroj	k1gInSc2	stroj
a	a	k8xC	a
regulovat	regulovat	k5eAaImF	regulovat
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
nedá	dát	k5eNaPmIp3nS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
charakteristikou	charakteristika	k1gFnSc7	charakteristika
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
mechanický	mechanický	k2eAgInSc1d1	mechanický
oscilátor	oscilátor	k1gInSc1	oscilátor
a	a	k8xC	a
převody	převod	k1gInPc1	převod
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
používají	používat	k5eAaImIp3nP	používat
i	i	k8xC	i
mechanický	mechanický	k2eAgInSc1d1	mechanický
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
závaží	závaží	k1gNnSc1	závaží
<g/>
,	,	kIx,	,
pružina	pružina	k1gFnSc1	pružina
<g/>
)	)	kIx)	)
a	a	k8xC	a
ručkovou	ručkový	k2eAgFnSc4d1	Ručková
indikaci	indikace	k1gFnSc4	indikace
na	na	k7c6	na
ciferníku	ciferník	k1gInSc6	ciferník
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgFnPc1	každý
mechanické	mechanický	k2eAgFnPc1d1	mechanická
hodiny	hodina	k1gFnPc1	hodina
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc1	tento
hlavní	hlavní	k2eAgFnPc1d1	hlavní
části	část	k1gFnPc1	část
<g/>
:	:	kIx,	:
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
čili	čili	k8xC	čili
pohon	pohon	k1gInSc1	pohon
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
závaží	závaží	k1gNnSc1	závaží
<g/>
,	,	kIx,	,
pružina	pružina	k1gFnSc1	pružina
s	s	k7c7	s
natahovacím	natahovací	k2eAgNnSc7d1	natahovací
zařízením	zařízení	k1gNnSc7	zařízení
nebo	nebo	k8xC	nebo
zdroj	zdroj	k1gInSc1	zdroj
proudu	proud	k1gInSc2	proud
<g/>
;	;	kIx,	;
soustavu	soustava	k1gFnSc4	soustava
ozubených	ozubený	k2eAgInPc2d1	ozubený
převodů	převod	k1gInPc2	převod
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
přenáší	přenášet	k5eAaImIp3nS	přenášet
energii	energie	k1gFnSc4	energie
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
k	k	k7c3	k
oscilátoru	oscilátor	k1gInSc3	oscilátor
<g/>
;	;	kIx,	;
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
oscilátor	oscilátor	k1gInSc4	oscilátor
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
posledním	poslední	k2eAgInSc7d1	poslední
(	(	kIx(	(
<g/>
nejrychlejším	rychlý	k2eAgMnSc7d3	nejrychlejší
<g/>
)	)	kIx)	)
kolem	kolo	k1gNnSc7	kolo
převodu	převod	k1gInSc2	převod
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
krokové	krokový	k2eAgNnSc1d1	krokové
nebo	nebo	k8xC	nebo
stoupací	stoupací	k2eAgNnSc1d1	stoupací
kolo	kolo	k1gNnSc1	kolo
<g/>
;	;	kIx,	;
oscilátor	oscilátor	k1gInSc1	oscilátor
či	či	k8xC	či
regulátor	regulátor	k1gInSc1	regulátor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
rychlost	rychlost	k1gFnSc4	rychlost
otáčení	otáčení	k1gNnSc2	otáčení
krokového	krokový	k2eAgNnSc2d1	krokové
(	(	kIx(	(
<g/>
stoupacího	stoupací	k2eAgNnSc2d1	stoupací
<g/>
)	)	kIx)	)
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
závisí	záviset	k5eAaImIp3nS	záviset
přesnost	přesnost	k1gFnSc4	přesnost
hodin	hodina	k1gFnPc2	hodina
<g/>
;	;	kIx,	;
Indikační	indikační	k2eAgNnSc1d1	indikační
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hodinové	hodinový	k2eAgInPc1d1	hodinový
převody	převod	k1gInPc1	převod
<g/>
,	,	kIx,	,
ručky	ručka	k1gFnPc1	ručka
a	a	k8xC	a
ciferník	ciferník	k1gInSc1	ciferník
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
bicí	bicí	k2eAgNnPc1d1	bicí
nebo	nebo	k8xC	nebo
hrací	hrací	k2eAgNnPc1d1	hrací
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
;	;	kIx,	;
indikace	indikace	k1gFnSc1	indikace
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
polohy	poloha	k1gFnPc1	poloha
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Ideálním	ideální	k2eAgInSc7d1	ideální
pohonem	pohon	k1gInSc7	pohon
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
závaží	závaží	k1gNnSc1	závaží
<g/>
,	,	kIx,	,
zavěšené	zavěšený	k2eAgNnSc1d1	zavěšené
buďto	buďto	k8xC	buďto
na	na	k7c6	na
laně	lano	k1gNnSc6	lano
či	či	k8xC	či
struně	struna	k1gFnSc6	struna
<g/>
,	,	kIx,	,
navinuté	navinutý	k2eAgFnSc6d1	navinutá
na	na	k7c6	na
bubnu	buben	k1gInSc6	buben
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
na	na	k7c6	na
řetízku	řetízek	k1gInSc6	řetízek
<g/>
,	,	kIx,	,
zachyceném	zachycený	k2eAgNnSc6d1	zachycené
na	na	k7c6	na
řetězovém	řetězový	k2eAgNnSc6d1	řetězové
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Závaží	závaží	k1gNnSc1	závaží
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
přesně	přesně	k6eAd1	přesně
konstantní	konstantní	k2eAgFnSc4d1	konstantní
hnací	hnací	k2eAgFnSc4d1	hnací
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
však	však	k9	však
použít	použít	k5eAaPmF	použít
u	u	k7c2	u
přenosných	přenosný	k2eAgFnPc2d1	přenosná
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
přenosných	přenosný	k2eAgFnPc2d1	přenosná
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
hodinek	hodinka	k1gFnPc2	hodinka
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
používá	používat	k5eAaImIp3nS	používat
plochá	plochý	k2eAgFnSc1d1	plochá
pružina	pružina	k1gFnSc1	pružina
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
péro	péro	k1gNnSc1	péro
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svinutá	svinutý	k2eAgFnSc1d1	svinutá
do	do	k7c2	do
spirály	spirála	k1gFnSc2	spirála
a	a	k8xC	a
u	u	k7c2	u
lepších	dobrý	k2eAgFnPc2d2	lepší
hodin	hodina	k1gFnPc2	hodina
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
pérovníku	pérovník	k1gInSc6	pérovník
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgFnSc1d1	hnací
síla	síla	k1gFnSc1	síla
pružiny	pružina	k1gFnSc2	pružina
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
rozvíjením	rozvíjení	k1gNnSc7	rozvíjení
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
působí	působit	k5eAaImIp3nS	působit
nerovnoměrnost	nerovnoměrnost	k1gFnSc1	nerovnoměrnost
chodu	chod	k1gInSc2	chod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
přesných	přesný	k2eAgFnPc2d1	přesná
přenosných	přenosný	k2eAgFnPc2d1	přenosná
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
chronometrů	chronometr	k1gInPc2	chronometr
ji	on	k3xPp3gFnSc4	on
kompenzovaly	kompenzovat	k5eAaBmAgInP	kompenzovat
složité	složitý	k2eAgInPc1d1	složitý
mechanismy	mechanismus	k1gInPc1	mechanismus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
šnek	šnek	k1gInSc1	šnek
s	s	k7c7	s
řetízkem	řetízek	k1gInSc7	řetízek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
s	s	k7c7	s
rozvíjením	rozvíjení	k1gNnSc7	rozvíjení
pružiny	pružina	k1gFnSc2	pružina
zároveň	zároveň	k6eAd1	zároveň
mění	měnit	k5eAaImIp3nS	měnit
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
síla	síla	k1gFnSc1	síla
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgNnSc1d1	hnací
kolo	kolo	k1gNnSc1	kolo
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ozubené	ozubený	k2eAgNnSc1d1	ozubené
kolo	kolo	k1gNnSc1	kolo
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
pérovníkem	pérovník	k1gInSc7	pérovník
<g/>
,	,	kIx,	,
bubnem	buben	k1gInSc7	buben
nebo	nebo	k8xC	nebo
řetězovým	řetězový	k2eAgNnSc7d1	řetězové
kolem	kolo	k1gNnSc7	kolo
<g/>
,	,	kIx,	,
přenáší	přenášet	k5eAaImIp3nS	přenášet
energii	energie	k1gFnSc4	energie
několika	několik	k4yIc2	několik
převody	převod	k1gInPc7	převod
dorychla	dorychnout	k5eAaPmAgFnS	dorychnout
až	až	k9	až
na	na	k7c4	na
krokové	krokový	k2eAgNnSc4d1	krokové
(	(	kIx(	(
<g/>
stoupací	stoupací	k2eAgNnSc4d1	stoupací
<g/>
)	)	kIx)	)
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
otáčení	otáčení	k1gNnSc1	otáčení
reguluje	regulovat	k5eAaImIp3nS	regulovat
krok	krok	k1gInSc4	krok
a	a	k8xC	a
oscilátor	oscilátor	k1gInSc4	oscilátor
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
mechanické	mechanický	k2eAgFnPc1d1	mechanická
hodiny	hodina	k1gFnPc1	hodina
mají	mít	k5eAaImIp3nP	mít
tři	tři	k4xCgInPc1	tři
až	až	k6eAd1	až
pět	pět	k4xCc1	pět
převodů	převod	k1gInPc2	převod
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
tvoří	tvořit	k5eAaImIp3nS	tvořit
typická	typický	k2eAgFnSc1d1	typická
hodinová	hodinový	k2eAgFnSc1d1	hodinová
kola	kola	k1gFnSc1	kola
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pastorek	pastorek	k1gInSc1	pastorek
a	a	k8xC	a
kolo	kolo	k1gNnSc1	kolo
na	na	k7c6	na
společném	společný	k2eAgInSc6d1	společný
hřídeli	hřídel	k1gInSc6	hřídel
<g/>
.	.	kIx.	.
</s>
<s>
Pastorek	pastorek	k1gInSc1	pastorek
mívá	mívat	k5eAaImIp3nS	mívat
6	[number]	k4	6
až	až	k9	až
10	[number]	k4	10
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
u	u	k7c2	u
levnějších	levný	k2eAgInPc2d2	levnější
strojů	stroj	k1gInPc2	stroj
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
cévkový	cévkový	k2eAgMnSc1d1	cévkový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
tvořený	tvořený	k2eAgInSc4d1	tvořený
věncem	věnec	k1gInSc7	věnec
kolíčků	kolíček	k1gInPc2	kolíček
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
ozubení	ozubení	k1gNnSc1	ozubení
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
jsou	být	k5eAaImIp3nP	být
cykloidní	cykloidní	k2eAgFnPc1d1	cykloidní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
povrchy	povrch	k1gInPc1	povrch
zubů	zub	k1gInPc2	zub
po	po	k7c6	po
sobě	se	k3xPyFc3	se
nesmýkaly	smýkat	k5eNaImAgFnP	smýkat
nýbrž	nýbrž	k8xC	nýbrž
odvalovaly	odvalovat	k5eAaImAgFnP	odvalovat
a	a	k8xC	a
snížily	snížit	k5eAaPmAgFnP	snížit
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ztráty	ztráta	k1gFnSc2	ztráta
třením	tření	k1gNnSc7	tření
<g/>
.	.	kIx.	.
</s>
<s>
Vlastním	vlastní	k2eAgNnSc7d1	vlastní
jádrem	jádro	k1gNnSc7	jádro
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
oscilátor	oscilátor	k1gInSc1	oscilátor
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
soustava	soustava	k1gFnSc1	soustava
setrvačných	setrvačný	k2eAgFnPc2d1	setrvačná
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
direkční	direkční	k2eAgFnPc1d1	direkční
síly	síla	k1gFnPc1	síla
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
frekvencí	frekvence	k1gFnSc7	frekvence
kmitů	kmit	k1gInPc2	kmit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
isochronii	isochronie	k1gFnSc6	isochronie
závisí	záviset	k5eAaImIp3nS	záviset
přesnost	přesnost	k1gFnSc1	přesnost
chodu	chod	k1gInSc2	chod
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
oscilátorem	oscilátor	k1gInSc7	oscilátor
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
lihýř	lihýř	k1gFnSc4	lihýř
<g/>
,	,	kIx,	,
rameno	rameno	k1gNnSc4	rameno
s	s	k7c7	s
dvěma	dva	k4xCgNnPc7	dva
závažími	závaží	k1gNnPc7	závaží
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
kolem	kolem	k7c2	kolem
svislé	svislý	k2eAgFnSc2d1	svislá
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Hřídel	hřídel	k1gFnSc1	hřídel
lihýře	lihýř	k1gInSc2	lihýř
je	být	k5eAaImIp3nS	být
zavěšen	zavěsit	k5eAaPmNgInS	zavěsit
na	na	k7c6	na
provazu	provaz	k1gInSc6	provaz
nebo	nebo	k8xC	nebo
řemeni	řemen	k1gInSc6	řemen
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
zkrucováním	zkrucování	k1gNnSc7	zkrucování
vzniká	vznikat	k5eAaImIp3nS	vznikat
direkční	direkční	k2eAgFnSc1d1	direkční
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Posunem	posun	k1gInSc7	posun
závažíček	závažíčko	k1gNnPc2	závažíčko
po	po	k7c6	po
rameni	rameno	k1gNnSc6	rameno
lze	lze	k6eAd1	lze
měnit	měnit	k5eAaImF	měnit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
frekvenci	frekvence	k1gFnSc4	frekvence
lihýře	lihýř	k1gInSc2	lihýř
<g/>
.	.	kIx.	.
</s>
<s>
Lihýř	Lihýř	k1gFnSc1	Lihýř
je	být	k5eAaImIp3nS	být
fyzikálně	fyzikálně	k6eAd1	fyzikálně
velmi	velmi	k6eAd1	velmi
neurčitý	určitý	k2eNgInSc1d1	neurčitý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
frekvence	frekvence	k1gFnSc1	frekvence
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
stavu	stav	k1gInSc6	stav
závěsu	závěs	k1gInSc2	závěs
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Chyba	chyba	k1gFnSc1	chyba
chodu	chod	k1gInSc2	chod
i	i	k8xC	i
variace	variace	k1gFnPc4	variace
lihýřových	lihýřův	k2eAgFnPc2d1	lihýřův
hodin	hodina	k1gFnPc2	hodina
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
minut	minuta	k1gFnPc2	minuta
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgInS	udržet
jako	jako	k9	jako
oscilátor	oscilátor	k1gInSc1	oscilátor
nepřenosných	přenosný	k2eNgFnPc2d1	nepřenosná
hodin	hodina	k1gFnPc2	hodina
až	až	k9	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
u	u	k7c2	u
přenosných	přenosný	k2eAgFnPc2d1	přenosná
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
hodinek	hodinka	k1gFnPc2	hodinka
ještě	ještě	k6eAd1	ještě
o	o	k7c6	o
století	století	k1gNnSc6	století
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
ideální	ideální	k2eAgInSc1d1	ideální
mechanický	mechanický	k2eAgInSc1d1	mechanický
oscilátor	oscilátor	k1gInSc1	oscilátor
je	být	k5eAaImIp3nS	být
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
direktivní	direktivní	k2eAgFnSc7d1	direktivní
silou	síla	k1gFnSc7	síla
je	být	k5eAaImIp3nS	být
dokonale	dokonale	k6eAd1	dokonale
stálá	stálý	k2eAgFnSc1d1	stálá
zemská	zemský	k2eAgFnSc1d1	zemská
gravitace	gravitace	k1gFnSc1	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc1	prostředek
měření	měření	k1gNnSc2	měření
času	čas	k1gInSc2	čas
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
poprvé	poprvé	k6eAd1	poprvé
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gFnPc1	Galile
<g/>
,	,	kIx,	,
konstrukce	konstrukce	k1gFnPc1	konstrukce
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
připisuje	připisovat	k5eAaImIp3nS	připisovat
už	už	k6eAd1	už
jeho	jeho	k3xOp3gMnSc3	jeho
synovi	syn	k1gMnSc3	syn
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
kyvadlové	kyvadlový	k2eAgFnSc2d1	kyvadlová
hodiny	hodina	k1gFnSc2	hodina
postavil	postavit	k5eAaPmAgInS	postavit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1655	[number]	k4	1655
podrobně	podrobně	k6eAd1	podrobně
popsal	popsat	k5eAaPmAgMnS	popsat
holandský	holandský	k2eAgMnSc1d1	holandský
fyzik	fyzik	k1gMnSc1	fyzik
Christiaan	Christiaana	k1gFnPc2	Christiaana
Huygens	Huygensa	k1gFnPc2	Huygensa
<g/>
.	.	kIx.	.
</s>
<s>
Matematické	matematický	k2eAgNnSc1d1	matematické
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
má	mít	k5eAaImIp3nS	mít
přesnou	přesný	k2eAgFnSc4d1	přesná
vlastní	vlastní	k2eAgFnSc4d1	vlastní
frekvenci	frekvence	k1gFnSc4	frekvence
kyvů	kyv	k1gInPc2	kyv
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ovšem	ovšem	k9	ovšem
mírně	mírně	k6eAd1	mírně
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
rozkyvu	rozkyv	k1gInSc6	rozkyv
(	(	kIx(	(
<g/>
amplitudě	amplituda	k1gFnSc6	amplituda
<g/>
)	)	kIx)	)
kyvadla	kyvadlo	k1gNnPc4	kyvadlo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
u	u	k7c2	u
přesnějších	přesný	k2eAgFnPc2d2	přesnější
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
tzv.	tzv.	kA	tzv.
pendlovek	pendlovky	k1gFnPc2	pendlovky
<g/>
)	)	kIx)	)
volí	volit	k5eAaImIp3nS	volit
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
amplituda	amplituda	k1gFnSc1	amplituda
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
do	do	k7c2	do
několika	několik	k4yIc2	několik
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
přesnost	přesnost	k1gFnSc4	přesnost
výroby	výroba	k1gFnSc2	výroba
kroku	krok	k1gInSc2	krok
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
i	i	k9	i
délka	délka	k1gFnSc1	délka
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
<g/>
,	,	kIx,	,
přesné	přesný	k2eAgFnPc1d1	přesná
kyvadlové	kyvadlový	k2eAgFnPc4d1	kyvadlová
hodiny	hodina	k1gFnPc4	hodina
proto	proto	k8xC	proto
mívaly	mívat	k5eAaImAgFnP	mívat
i	i	k8xC	i
teplotní	teplotní	k2eAgFnSc4d1	teplotní
kompenzaci	kompenzace	k1gFnSc4	kompenzace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
u	u	k7c2	u
přenosných	přenosný	k2eAgFnPc2d1	přenosná
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
u	u	k7c2	u
hodinek	hodinka	k1gFnPc2	hodinka
dlouho	dlouho	k6eAd1	dlouho
udržel	udržet	k5eAaPmAgInS	udržet
lihýř	lihýř	k1gInSc1	lihýř
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
teprve	teprve	k6eAd1	teprve
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
nahradila	nahradit	k5eAaPmAgFnS	nahradit
setrvačka	setrvačka	k1gFnSc1	setrvačka
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
"	"	kIx"	"
<g/>
nepokoj	nepokoj	k1gInSc4	nepokoj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
Unruh	Unruh	k1gInSc1	Unruh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Setrvačka	setrvačka	k1gFnSc1	setrvačka
je	být	k5eAaImIp3nS	být
kolo	kolo	k1gNnSc4	kolo
na	na	k7c6	na
hřídeli	hřídel	k1gInSc6	hřídel
a	a	k8xC	a
direkční	direkční	k2eAgFnSc4d1	direkční
sílu	síla	k1gFnSc4	síla
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
vlásek	vlásek	k1gInSc1	vlásek
<g/>
,	,	kIx,	,
jemná	jemný	k2eAgFnSc1d1	jemná
spirálová	spirálový	k2eAgFnSc1d1	spirálová
pružina	pružina	k1gFnSc1	pružina
<g/>
.	.	kIx.	.
</s>
<s>
Frekvenci	frekvence	k1gFnSc4	frekvence
setrvačky	setrvačka	k1gFnSc2	setrvačka
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
teplotní	teplotní	k2eAgFnSc4d1	teplotní
roztažnost	roztažnost	k1gFnSc4	roztažnost
věnce	věnec	k1gInSc2	věnec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
lepších	dobrý	k2eAgFnPc2d2	lepší
hodinek	hodinka	k1gFnPc2	hodinka
kompenzovala	kompenzovat	k5eAaBmAgFnS	kompenzovat
bimetalem	bimetal	k1gInSc7	bimetal
<g/>
,	,	kIx,	,
novější	nový	k2eAgFnPc4d2	novější
hodiny	hodina	k1gFnPc4	hodina
ji	on	k3xPp3gFnSc4	on
omezují	omezovat	k5eAaImIp3nP	omezovat
volbou	volba	k1gFnSc7	volba
vhodné	vhodný	k2eAgFnSc2d1	vhodná
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tuhost	tuhost	k1gFnSc1	tuhost
vlásku	vlásek	k1gInSc2	vlásek
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnPc4d2	lepší
hodinky	hodinka	k1gFnPc4	hodinka
proto	proto	k8xC	proto
mívají	mívat	k5eAaImIp3nP	mívat
vlásek	vlásek	k1gInSc4	vlásek
ze	z	k7c2	z
speciální	speciální	k2eAgFnSc2d1	speciální
slitiny	slitina	k1gFnSc2	slitina
<g/>
,	,	kIx,	,
invaru	invar	k1gInSc2	invar
<g/>
.	.	kIx.	.
</s>
<s>
Chod	chod	k1gInSc1	chod
hodin	hodina	k1gFnPc2	hodina
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
tření	tření	k1gNnSc1	tření
čepů	čep	k1gInPc2	čep
setrvačky	setrvačka	k1gFnSc2	setrvačka
v	v	k7c6	v
ložisku	ložisko	k1gNnSc6	ložisko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
umělých	umělý	k2eAgInPc2d1	umělý
rubínů	rubín	k1gInPc2	rubín
a	a	k8xC	a
s	s	k7c7	s
co	co	k9	co
nejtenčími	tenký	k2eAgInPc7d3	nejtenčí
čepy	čep	k1gInPc7	čep
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
kolem	kolem	k7c2	kolem
desetiny	desetina	k1gFnSc2	desetina
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
čepy	čep	k1gInPc1	čep
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
mechanicky	mechanicky	k6eAd1	mechanicky
choulostivé	choulostivý	k2eAgNnSc1d1	choulostivé
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ložiska	ložisko	k1gNnPc1	ložisko
moderních	moderní	k2eAgFnPc2d1	moderní
hodinek	hodinka	k1gFnPc2	hodinka
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
ještě	ještě	k6eAd1	ještě
odpružena	odpružen	k2eAgFnSc1d1	odpružena
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
konstrukčním	konstrukční	k2eAgInSc7d1	konstrukční
problémem	problém	k1gInSc7	problém
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
při	při	k7c6	při
každém	každý	k3xTgNnSc6	každý
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
každém	každý	k3xTgInSc6	každý
druhém	druhý	k4xOgNnSc6	druhý
<g/>
)	)	kIx)	)
kyvu	kyv	k1gInSc3	kyv
oscilátoru	oscilátor	k1gInSc2	oscilátor
propouští	propouštět	k5eAaImIp3nS	propouštět
jeden	jeden	k4xCgInSc4	jeden
zub	zub	k1gInSc4	zub
krokového	krokový	k2eAgNnSc2d1	krokové
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
dodává	dodávat	k5eAaImIp3nS	dodávat
oscilátoru	oscilátor	k1gInSc3	oscilátor
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgInSc1d1	ideální
krok	krok	k1gInSc1	krok
by	by	kYmCp3nS	by
přitom	přitom	k6eAd1	přitom
měl	mít	k5eAaImAgInS	mít
oscilátor	oscilátor	k1gInSc1	oscilátor
co	co	k9	co
nejméně	málo	k6eAd3	málo
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
<g/>
,	,	kIx,	,
u	u	k7c2	u
lepších	dobrý	k2eAgFnPc2d2	lepší
hodinek	hodinka	k1gFnPc2	hodinka
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
užívají	užívat	k5eAaImIp3nP	užívat
tzv.	tzv.	kA	tzv.
volné	volný	k2eAgInPc4d1	volný
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oscilátor	oscilátor	k1gInSc1	oscilátor
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
kyvu	kyv	k1gInSc2	kyv
kroku	krok	k1gInSc2	krok
vůbec	vůbec	k9	vůbec
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vývoje	vývoj	k1gInSc2	vývoj
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
desítky	desítka	k1gFnPc1	desítka
velmi	velmi	k6eAd1	velmi
důmyslných	důmyslný	k2eAgFnPc2d1	důmyslná
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
Kroky	krok	k1gInPc1	krok
vratné	vratný	k2eAgInPc1d1	vratný
<g/>
,	,	kIx,	,
výrobně	výrobně	k6eAd1	výrobně
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
oscilátor	oscilátor	k1gInSc1	oscilátor
v	v	k7c6	v
krajní	krajní	k2eAgFnSc6d1	krajní
poloze	poloha	k1gFnSc6	poloha
krokové	krokový	k2eAgNnSc4d1	krokové
kolo	kolo	k1gNnSc4	kolo
vracet	vracet	k5eAaImF	vracet
o	o	k7c4	o
kousek	kousek	k1gInSc4	kousek
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
silně	silně	k6eAd1	silně
snižuje	snižovat	k5eAaImIp3nS	snižovat
přesnost	přesnost	k1gFnSc4	přesnost
hodin	hodina	k1gFnPc2	hodina
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
vratné	vratný	k2eAgNnSc4d1	vratné
<g />
.	.	kIx.	.
</s>
<s>
kroky	krok	k1gInPc4	krok
patří	patřit	k5eAaImIp3nP	patřit
lihýřový	lihýřový	k2eAgMnSc1d1	lihýřový
(	(	kIx(	(
<g/>
lopatkový	lopatkový	k2eAgInSc1d1	lopatkový
<g/>
)	)	kIx)	)
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
hákový	hákový	k2eAgInSc1d1	hákový
krok	krok	k1gInSc1	krok
starších	starý	k2eAgFnPc2d2	starší
nástěnných	nástěnný	k2eAgFnPc2d1	nástěnná
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kukaček	kukačka	k1gFnPc2	kukačka
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
kolíčkový	kolíčkový	k2eAgInSc1d1	kolíčkový
krok	krok	k1gInSc1	krok
mechanických	mechanický	k2eAgInPc2d1	mechanický
budíků	budík	k1gInPc2	budík
aj.	aj.	kA	aj.
Kroky	krok	k1gInPc4	krok
klidové	klidový	k2eAgInPc4d1	klidový
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tuto	tento	k3xDgFnSc4	tento
vadu	vada	k1gFnSc4	vada
nemají	mít	k5eNaImIp3nP	mít
<g/>
;	;	kIx,	;
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
různé	různý	k2eAgFnPc1d1	různá
varianty	varianta	k1gFnPc1	varianta
kotvového	kotvový	k2eAgInSc2d1	kotvový
kroku	krok	k1gInSc2	krok
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
chronometrové	chronometrový	k2eAgInPc4d1	chronometrový
kroky	krok	k1gInPc4	krok
aj.	aj.	kA	aj.
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
různých	různý	k2eAgNnPc2d1	různé
časoměrných	časoměrný	k2eAgNnPc2d1	časoměrné
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
speciální	speciální	k2eAgInPc4d1	speciální
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
stopky	stopka	k1gFnPc1	stopka
a	a	k8xC	a
chronografy	chronograf	k1gInPc1	chronograf
na	na	k7c4	na
odměřování	odměřování	k1gNnSc4	odměřování
kratších	krátký	k2eAgInPc2d2	kratší
časových	časový	k2eAgInPc2d1	časový
intervalů	interval	k1gInPc2	interval
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnSc2d1	různá
registrační	registrační	k2eAgFnSc2d1	registrační
hodiny	hodina	k1gFnSc2	hodina
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
píchačky	píchačka	k1gFnSc2	píchačka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hlídačské	hlídačský	k2eAgFnPc1d1	hlídačský
nebo	nebo	k8xC	nebo
ponocenské	ponocenský	k2eAgFnPc1d1	ponocenský
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
tachografy	tachograf	k1gInPc1	tachograf
<g/>
,	,	kIx,	,
taxametry	taxametr	k1gInPc1	taxametr
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
slepecké	slepecký	k2eAgFnPc1d1	slepecká
hodiny	hodina	k1gFnPc1	hodina
s	s	k7c7	s
hmatovou	hmatový	k2eAgFnSc7d1	hmatová
indikací	indikace	k1gFnSc7	indikace
<g/>
,	,	kIx,	,
šachové	šachový	k2eAgFnSc2d1	šachová
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
