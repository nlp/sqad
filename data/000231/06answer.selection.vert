<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
přenosné	přenosný	k2eAgFnPc1d1	přenosná
<g/>
,	,	kIx,	,
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
hodinky	hodinka	k1gFnPc1	hodinka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
přesné	přesný	k2eAgFnPc4d1	přesná
hodiny	hodina	k1gFnPc4	hodina
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
název	název	k1gInSc1	název
chronometr	chronometr	k1gInSc1	chronometr
<g/>
.	.	kIx.	.
</s>
