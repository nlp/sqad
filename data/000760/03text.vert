<s>
Přeclav	Přeclat	k5eAaPmDgInS	Přeclat
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
(	(	kIx(	(
<g/>
též	též	k9	též
z	z	k7c2	z
Pogorely	Pogorela	k1gFnSc2	Pogorela
<g/>
,	,	kIx,	,
z	z	k7c2	z
Pogorella	Pogorello	k1gNnSc2	Pogorello
<g/>
,	,	kIx,	,
z	z	k7c2	z
Pohořelce	Pohořelec	k1gInSc2	Pohořelec
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Przeclaus	Przeclaus	k1gMnSc1	Przeclaus
de	de	k?	de
Pogarella	Pogarella	k1gMnSc1	Pogarella
nebo	nebo	k8xC	nebo
de	de	k?	de
Pogorella	Pogorella	k1gMnSc1	Pogorella
<g/>
,	,	kIx,	,
pol.	pol.	k?	pol.
Przecław	Przecław	k1gMnSc1	Przecław
z	z	k7c2	z
Pogorzeli	Pogorzeli	k1gFnSc2	Pogorzeli
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Preczlaw	Preczlaw	k1gFnSc1	Preczlaw
von	von	k1gInSc1	von
Pogarell	Pogarell	k1gInSc1	Pogarell
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1299	[number]	k4	1299
nebo	nebo	k8xC	nebo
1310	[number]	k4	1310
<g/>
,	,	kIx,	,
Pohořelá	pohořelý	k2eAgFnSc1d1	Pohořelá
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Pogorzela	Pogorzela	k1gMnPc5	Pogorzela
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Michalov	Michalov	k1gInSc1	Michalov
(	(	kIx(	(
<g/>
Michałów	Michałów	k1gFnSc1	Michałów
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lehnické	Lehnický	k2eAgNnSc1d1	Lehnické
knížectví	knížectví	k1gNnSc1	knížectví
-	-	kIx~	-
noc	noc	k1gFnSc1	noc
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1376	[number]	k4	1376
<g/>
,	,	kIx,	,
Otmuchov	Otmuchov	k1gInSc1	Otmuchov
<g/>
,	,	kIx,	,
Niské	Niská	k1gFnPc1	Niská
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
dvacátý	dvacátý	k4xOgMnSc1	dvacátý
vratislavský	vratislavský	k2eAgMnSc1d1	vratislavský
biskup	biskup	k1gMnSc1	biskup
(	(	kIx(	(
<g/>
1341	[number]	k4	1341
<g/>
-	-	kIx~	-
<g/>
1376	[number]	k4	1376
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
kancléř	kancléř	k1gMnSc1	kancléř
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
jeho	jeho	k3xOp3gInSc2	jeho
episkopátu	episkopát	k1gInSc2	episkopát
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
vratislavské	vratislavský	k2eAgFnSc2d1	Vratislavská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Přeclav	Přeclat	k5eAaPmDgInS	Přeclat
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc6d1	Pohořelá
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
po	po	k7c6	po
vsi	ves	k1gFnSc6	ves
Pohořelá	pohořelý	k2eAgFnSc1d1	Pohořelá
(	(	kIx(	(
<g/>
pol.	pol.	k?	pol.
Pogorzela	Pogorzela	k1gMnSc1	Pogorzela
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Pogarell	Pogarell	k1gInSc1	Pogarell
<g/>
)	)	kIx)	)
poblíž	poblíž	k7c2	poblíž
Břehu	břeh	k1gInSc2	břeh
(	(	kIx(	(
<g/>
pol.	pol.	k?	pol.
Brzeg	Brzeg	k1gInSc1	Brzeg
<g/>
)	)	kIx)	)
a	a	k8xC	a
nosil	nosit	k5eAaImAgInS	nosit
erb	erb	k1gInSc4	erb
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Grzymała	Grzymała	k1gMnSc1	Grzymała
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
známých	známý	k2eAgInPc2d1	známý
rodů	rod	k1gInPc2	rod
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zakladatelem	zakladatel	k1gMnSc7	zakladatel
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Kamenci	Kamenec	k1gInSc6	Kamenec
<g/>
.	.	kIx.	.
</s>
<s>
Přeclavovým	Přeclavový	k2eAgMnSc7d1	Přeclavový
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Bohuš	Bohuš	k1gMnSc1	Bohuš
z	z	k7c2	z
Michalova	Michalův	k2eAgMnSc2d1	Michalův
a	a	k8xC	a
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
(	(	kIx(	(
<g/>
zmiňován	zmiňovat	k5eAaImNgMnS	zmiňovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1284	[number]	k4	1284
<g/>
-	-	kIx~	-
<g/>
1309	[number]	k4	1309
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přeclav	Přeclat	k5eAaPmDgInS	Přeclat
měl	mít	k5eAaImAgInS	mít
tři	tři	k4xCgMnPc4	tři
starší	starý	k2eAgMnPc4d2	starší
bratry	bratr	k1gMnPc4	bratr
(	(	kIx(	(
<g/>
Jindřicha	Jindřich	k1gMnSc4	Jindřich
<g/>
,	,	kIx,	,
Bohuše	Bohuš	k1gMnSc4	Bohuš
a	a	k8xC	a
Mirzana	Mirzan	k1gMnSc4	Mirzan
<g/>
)	)	kIx)	)
a	a	k8xC	a
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
Gunthera	Gunther	k1gMnSc2	Gunther
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
biskupem	biskup	k1gMnSc7	biskup
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kanovníky	kanovník	k1gMnPc7	kanovník
vratislavské	vratislavský	k2eAgFnSc2d1	Vratislavská
kapituly	kapitula	k1gFnSc2	kapitula
stali	stát	k5eAaPmAgMnP	stát
také	také	k9	také
jeho	jeho	k3xOp3gMnPc1	jeho
synovci	synovec	k1gMnPc1	synovec
Čambor	Čambora	k1gFnPc2	Čambora
(	(	kIx(	(
<g/>
zm	zm	k?	zm
<g/>
.	.	kIx.	.
1343	[number]	k4	1343
<g/>
-	-	kIx~	-
<g/>
1383	[number]	k4	1383
<g/>
,	,	kIx,	,
též	též	k9	též
kanovník	kanovník	k1gMnSc1	kanovník
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jakub	Jakub	k1gMnSc1	Jakub
(	(	kIx(	(
<g/>
zm	zm	k?	zm
<g/>
.	.	kIx.	.
1352	[number]	k4	1352
<g/>
-	-	kIx~	-
<g/>
1362	[number]	k4	1362
<g/>
,	,	kIx,	,
též	též	k9	též
kanovník	kanovník	k1gMnSc1	kanovník
v	v	k7c6	v
Hlohově	hlohově	k6eAd1	hlohově
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
biskupských	biskupský	k2eAgFnPc6d1	biskupská
službách	služba	k1gFnPc6	služba
jako	jako	k8xS	jako
rytíř	rytíř	k1gMnSc1	rytíř
sloužil	sloužit	k5eAaImAgMnS	sloužit
další	další	k2eAgMnSc1d1	další
synovec	synovec	k1gMnSc1	synovec
Jaroch	Jaroch	k1gMnSc1	Jaroch
(	(	kIx(	(
<g/>
zm	zm	k?	zm
<g/>
.	.	kIx.	.
1349	[number]	k4	1349
<g/>
-	-	kIx~	-
<g/>
1383	[number]	k4	1383
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Přeclav	Přeclav	k1gMnSc1	Přeclav
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
zmiňován	zmiňovat	k5eAaImNgInS	zmiňovat
až	až	k6eAd1	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1329	[number]	k4	1329
jako	jako	k8xS	jako
kanovník	kanovník	k1gMnSc1	kanovník
katedrální	katedrální	k2eAgFnSc2d1	katedrální
kapituly	kapitula	k1gFnSc2	kapitula
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
29	[number]	k4	29
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
1336	[number]	k4	1336
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zdržoval	zdržovat	k5eAaImAgInS	zdržovat
i	i	k9	i
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1341	[number]	k4	1341
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gInSc4	on
vratislavská	vratislavský	k2eAgFnSc1d1	Vratislavská
kapitula	kapitula	k1gFnSc1	kapitula
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
biskupa	biskup	k1gMnSc2	biskup
Nankera	Nanker	k1gMnSc2	Nanker
zvolila	zvolit	k5eAaPmAgFnS	zvolit
novým	nový	k2eAgMnSc7d1	nový
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
neproběhla	proběhnout	k5eNaPmAgFnS	proběhnout
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Vratislav	Vratislav	k1gFnSc1	Vratislav
dosud	dosud	k6eAd1	dosud
podléhala	podléhat	k5eAaImAgFnS	podléhat
interdiktu	interdikt	k1gInSc3	interdikt
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
se	se	k3xPyFc4	se
konat	konat	k5eAaImF	konat
v	v	k7c6	v
Nise	Nisa	k1gFnSc6	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vratislavské	vratislavský	k2eAgFnSc2d1	Vratislavská
kapituly	kapitula	k1gFnSc2	kapitula
podporovala	podporovat	k5eAaImAgFnS	podporovat
Jana	Jana	k1gFnSc1	Jana
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
Přeclav	Přeclav	k1gMnPc7	Přeclav
byl	být	k5eAaImAgMnS	být
kandidátem	kandidát	k1gMnSc7	kandidát
Lucemburky	Lucemburk	k1gInPc1	Lucemburk
podporovaným	podporovaný	k2eAgNnSc7d1	podporované
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
však	však	k9	však
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
hnězdenský	hnězdenský	k2eAgMnSc1d1	hnězdenský
Janislav	Janislav	k1gMnSc1	Janislav
jakožto	jakožto	k8xS	jakožto
metropolita	metropolita	k1gMnSc1	metropolita
vratislavské	vratislavský	k2eAgFnSc2d1	Vratislavská
diecéze	diecéze	k1gFnSc2	diecéze
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
volbu	volba	k1gFnSc4	volba
potvrdit	potvrdit	k5eAaPmF	potvrdit
a	a	k8xC	a
udělit	udělit	k5eAaPmF	udělit
Přeclavovi	Přeclava	k1gMnSc3	Přeclava
biskupské	biskupský	k2eAgNnSc1d1	Biskupské
svěcení	svěcení	k1gNnSc1	svěcení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
vyhradil	vyhradit	k5eAaPmAgMnS	vyhradit
jmenování	jmenování	k1gNnSc4	jmenování
nového	nový	k2eAgInSc2d1	nový
biskupa	biskup	k1gInSc2	biskup
papež	papež	k1gMnSc1	papež
<g/>
.	.	kIx.	.
</s>
<s>
Přeclav	Přeclat	k5eAaPmDgInS	Přeclat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vydal	vydat	k5eAaPmAgInS	vydat
osobně	osobně	k6eAd1	osobně
k	k	k7c3	k
papeži	papež	k1gMnSc3	papež
Benediktu	Benedikt	k1gMnSc3	Benedikt
XII	XII	kA	XII
<g/>
.	.	kIx.	.
do	do	k7c2	do
Avignonu	Avignon	k1gInSc2	Avignon
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
získal	získat	k5eAaPmAgInS	získat
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1342	[number]	k4	1342
konfirmační	konfirmační	k2eAgFnSc4d1	konfirmační
bulu	bula	k1gFnSc4	bula
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
biskupské	biskupský	k2eAgNnSc1d1	Biskupské
svěcení	svěcení	k1gNnSc1	svěcení
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vrátil	vrátit	k5eAaPmAgMnS	vrátit
již	již	k6eAd1	již
jako	jako	k8xS	jako
právoplatný	právoplatný	k2eAgMnSc1d1	právoplatný
biskup	biskup	k1gMnSc1	biskup
do	do	k7c2	do
Vratislavi	Vratislav	k1gFnSc2	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Biskup	biskup	k1gMnSc1	biskup
Přeclav	Přeclav	k1gMnSc1	Přeclav
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
svázán	svázán	k2eAgMnSc1d1	svázán
s	s	k7c7	s
Lucemburky	Lucemburk	k1gInPc7	Lucemburk
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
s	s	k7c7	s
markrabětem	markrabě	k1gMnSc7	markrabě
Karlem	Karel	k1gMnSc7	Karel
<g/>
,	,	kIx,	,
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Přeclavova	Přeclavův	k2eAgInSc2d1	Přeclavův
nástupu	nástup	k1gInSc2	nástup
spravoval	spravovat	k5eAaImAgInS	spravovat
jménem	jméno	k1gNnSc7	jméno
otce	otec	k1gMnSc2	otec
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
kroky	krok	k1gInPc7	krok
nového	nový	k2eAgInSc2d1	nový
biskupa	biskup	k1gInSc2	biskup
bylo	být	k5eAaImAgNnS	být
rychlé	rychlý	k2eAgNnSc4d1	rychlé
vyřízení	vyřízení	k1gNnSc4	vyřízení
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
vedl	vést	k5eAaImAgMnS	vést
biskup	biskup	k1gMnSc1	biskup
Nanker	Nanker	k1gMnSc1	Nanker
<g/>
,	,	kIx,	,
a	a	k8xC	a
potvrzení	potvrzení	k1gNnSc1	potvrzení
králova	králův	k2eAgNnSc2d1	královo
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Zrušil	zrušit	k5eAaPmAgMnS	zrušit
Nankerem	Nanker	k1gInSc7	Nanker
uložený	uložený	k2eAgInSc1d1	uložený
interdikt	interdikt	k1gInSc1	interdikt
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
Vratislaví	Vratislav	k1gFnPc2	Vratislav
i	i	k8xC	i
exkomunikaci	exkomunikace	k1gFnSc4	exkomunikace
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
členů	člen	k1gMnPc2	člen
vratislavské	vratislavský	k2eAgFnSc2d1	Vratislavská
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
-	-	kIx~	-
ti	ten	k3xDgMnPc1	ten
však	však	k9	však
museli	muset	k5eAaImAgMnP	muset
biskupa	biskup	k1gMnSc4	biskup
přijít	přijít	k5eAaPmF	přijít
odprosit	odprosit	k5eAaPmF	odprosit
v	v	k7c6	v
průvodu	průvod	k1gInSc6	průvod
z	z	k7c2	z
radnice	radnice	k1gFnSc2	radnice
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
,	,	kIx,	,
bosí	bosý	k2eAgMnPc1d1	bosý
a	a	k8xC	a
v	v	k7c6	v
kajícnickém	kajícnický	k2eAgInSc6d1	kajícnický
oděvu	oděv	k1gInSc6	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
hradu	hrad	k1gInSc2	hrad
Milíče	Milíč	k1gInSc2	Milíč
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
obsazení	obsazení	k1gNnSc1	obsazení
bylo	být	k5eAaImAgNnS	být
pohnutkou	pohnutka	k1gFnSc7	pohnutka
k	k	k7c3	k
uvedenému	uvedený	k2eAgInSc3d1	uvedený
sporu	spor	k1gInSc3	spor
<g/>
,	,	kIx,	,
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
králem	král	k1gMnSc7	král
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
:	:	kIx,	:
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
zpět	zpět	k6eAd1	zpět
vratislavské	vratislavský	k2eAgFnSc3d1	Vratislavská
kapitule	kapitula	k1gFnSc3	kapitula
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gNnSc4	on
držela	držet	k5eAaImAgFnS	držet
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
král	král	k1gMnSc1	král
měl	mít	k5eAaImAgMnS	mít
mít	mít	k5eAaImF	mít
možnost	možnost	k1gFnSc4	možnost
jej	on	k3xPp3gNnSc2	on
vojensky	vojensky	k6eAd1	vojensky
využívat	využívat	k5eAaImF	využívat
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1342	[number]	k4	1342
vydal	vydat	k5eAaPmAgMnS	vydat
biskup	biskup	k1gMnSc1	biskup
Přeclav	Přeclav	k1gMnSc1	Přeclav
listinu	listina	k1gFnSc4	listina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
konsolidovala	konsolidovat	k5eAaBmAgFnS	konsolidovat
a	a	k8xC	a
potvrzovala	potvrzovat	k5eAaImAgFnS	potvrzovat
lenní	lenní	k2eAgInSc4d1	lenní
vztah	vztah	k1gInSc4	vztah
slezských	slezský	k2eAgMnPc2d1	slezský
knížat	kníže	k1gMnPc2wR	kníže
k	k	k7c3	k
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
uznáno	uznán	k2eAgNnSc4d1	uznáno
patronátní	patronátní	k2eAgNnSc4d1	patronátní
právo	právo	k1gNnSc4	právo
krále	král	k1gMnSc2	král
nad	nad	k7c7	nad
vratislavským	vratislavský	k2eAgNnSc7d1	vratislavské
biskupstvím	biskupství	k1gNnSc7	biskupství
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
byl	být	k5eAaImAgMnS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
pánem	pán	k1gMnSc7	pán
všech	všecek	k3xTgInPc2	všecek
majetků	majetek	k1gInPc2	majetek
biskupství	biskupství	k1gNnSc2	biskupství
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
využívat	využívat	k5eAaPmF	využívat
jeho	jeho	k3xOp3gInPc4	jeho
hrady	hrad	k1gInPc4	hrad
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
se	se	k3xPyFc4	se
zavázal	zavázat	k5eAaPmAgMnS	zavázat
církevně	církevně	k6eAd1	církevně
stíhat	stíhat	k5eAaImF	stíhat
všechny	všechen	k3xTgInPc4	všechen
(	(	kIx(	(
<g/>
rozuměj	rozumět	k5eAaImRp2nS	rozumět
především	především	k9	především
případná	případný	k2eAgNnPc1d1	případné
odbojná	odbojný	k2eAgNnPc1d1	odbojné
knížata	kníže	k1gNnPc1	kníže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
zpronevěřili	zpronevěřit	k5eAaPmAgMnP	zpronevěřit
slibu	slib	k1gInSc2	slib
věrnosti	věrnost	k1gFnPc1	věrnost
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Markrabě	markrabě	k1gMnSc1	markrabě
Karel	Karel	k1gMnSc1	Karel
jménem	jméno	k1gNnSc7	jméno
otce	otec	k1gMnSc2	otec
naopak	naopak	k6eAd1	naopak
přijal	přijmout	k5eAaPmAgMnS	přijmout
biskupství	biskupství	k1gNnSc4	biskupství
ve	v	k7c4	v
svou	svůj	k3xOyFgFnSc4	svůj
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
zavázal	zavázat	k5eAaPmAgMnS	zavázat
se	se	k3xPyFc4	se
chránit	chránit	k5eAaImF	chránit
jeho	jeho	k3xOp3gInSc4	jeho
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Přeclav	Přeclat	k5eAaPmDgInS	Přeclat
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
diplomatických	diplomatický	k2eAgInPc6d1	diplomatický
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
Kazimírem	Kazimír	k1gMnSc7	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
sjednávání	sjednávání	k1gNnSc6	sjednávání
Namyslovského	Namyslovský	k2eAgInSc2d1	Namyslovský
míru	mír	k1gInSc2	mír
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
a	a	k8xC	a
při	při	k7c6	při
organizaci	organizace	k1gFnSc6	organizace
setkání	setkání	k1gNnPc2	setkání
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
s	s	k7c7	s
Kazimírem	Kazimír	k1gMnSc7	Kazimír
Velikým	veliký	k2eAgMnSc7d1	veliký
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
roku	rok	k1gInSc2	rok
1351	[number]	k4	1351
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tyto	tento	k3xDgFnPc4	tento
zásluhy	zásluha	k1gFnPc4	zásluha
se	se	k3xPyFc4	se
Přeclav	Přeclav	k1gFnSc4	Přeclav
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
kancléřem	kancléř	k1gMnSc7	kancléř
Karla	Karel	k1gMnSc4	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
používal	používat	k5eAaImAgInS	používat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc4	výkon
skutečných	skutečný	k2eAgFnPc2d1	skutečná
povinností	povinnost	k1gFnPc2	povinnost
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
svázaných	svázaný	k2eAgFnPc2d1	svázaná
brzy	brzy	k6eAd1	brzy
přenechal	přenechat	k5eAaPmAgInS	přenechat
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Přeclavem	Přeclav	k1gInSc7	Přeclav
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
totiž	totiž	k9	totiž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ochlazení	ochlazení	k1gNnSc3	ochlazení
vztahů	vztah	k1gInPc2	vztah
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
císařův	císařův	k2eAgInSc4d1	císařův
plán	plán	k1gInSc4	plán
na	na	k7c4	na
vyčlenění	vyčlenění	k1gNnSc4	vyčlenění
vratislavského	vratislavský	k2eAgNnSc2d1	vratislavské
biskupství	biskupství	k1gNnSc2	biskupství
z	z	k7c2	z
hnězdenské	hnězdenský	k2eAgFnSc2d1	Hnězdenská
církevní	církevní	k2eAgFnSc2d1	církevní
provincie	provincie	k1gFnSc2	provincie
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
přičlenění	přičlenění	k1gNnSc2	přičlenění
k	k	k7c3	k
církevní	církevní	k2eAgFnSc3d1	církevní
provincii	provincie	k1gFnSc3	provincie
pražské	pražský	k2eAgFnSc2d1	Pražská
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
roku	rok	k1gInSc2	rok
1344	[number]	k4	1344
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
plánu	plán	k1gInSc3	plán
se	se	k3xPyFc4	se
stavěli	stavět	k5eAaImAgMnP	stavět
z	z	k7c2	z
pochopitelných	pochopitelný	k2eAgInPc2d1	pochopitelný
důvodů	důvod	k1gInPc2	důvod
hnězdenský	hnězdenský	k2eAgMnSc1d1	hnězdenský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
i	i	k8xC	i
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
důrazně	důrazně	k6eAd1	důrazně
i	i	k9	i
vratislavská	vratislavský	k2eAgFnSc1d1	Vratislavská
katedrální	katedrální	k2eAgFnSc1d1	katedrální
kapitula	kapitula	k1gFnSc1	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
biskup	biskup	k1gMnSc1	biskup
Přeclav	Přeclav	k1gMnSc1	Přeclav
po	po	k7c6	po
počáteční	počáteční	k2eAgFnSc6d1	počáteční
podpoře	podpora	k1gFnSc6	podpora
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
spíše	spíše	k9	spíše
vlažný	vlažný	k2eAgInSc4d1	vlažný
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Dvojí	dvojí	k4xRgFnSc1	dvojí
příslušnost	příslušnost	k1gFnSc1	příslušnost
biskupství	biskupství	k1gNnSc2	biskupství
pod	pod	k7c4	pod
patronát	patronát	k1gInSc4	patronát
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
a	a	k8xC	a
pod	pod	k7c4	pod
záštitu	záštita	k1gFnSc4	záštita
polského	polský	k2eAgMnSc2d1	polský
metropolity	metropolita	k1gMnSc2	metropolita
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
výhodou	výhoda	k1gFnSc7	výhoda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
manévrovací	manévrovací	k2eAgInSc4d1	manévrovací
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Karlův	Karlův	k2eAgMnSc1d1	Karlův
vychovatel	vychovatel	k1gMnSc1	vychovatel
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zasedl	zasednout	k5eAaPmAgInS	zasednout
Lucemburkům	Lucemburk	k1gInPc3	Lucemburk
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
nakloněný	nakloněný	k2eAgMnSc1d1	nakloněný
Inocenc	Inocenc	k1gMnSc1	Inocenc
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
záměr	záměr	k1gInSc1	záměr
na	na	k7c4	na
převod	převod	k1gInSc4	převod
(	(	kIx(	(
<g/>
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c6	o
rozdělení	rozdělení	k1gNnSc6	rozdělení
<g/>
)	)	kIx)	)
vratislavské	vratislavský	k2eAgFnSc2d1	Vratislavská
diecéze	diecéze	k1gFnSc2	diecéze
nerealizovatelným	realizovatelný	k2eNgInPc3d1	nerealizovatelný
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1360	[number]	k4	1360
musel	muset	k5eAaImAgInS	muset
definitivně	definitivně	k6eAd1	definitivně
zříci	zříct	k5eAaPmF	zříct
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1358	[number]	k4	1358
opět	opět	k6eAd1	opět
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
všechna	všechen	k3xTgNnPc4	všechen
stará	starý	k2eAgNnPc4d1	staré
privilegia	privilegium	k1gNnPc4	privilegium
biskupství	biskupství	k1gNnSc2	biskupství
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
knížecího	knížecí	k2eAgInSc2d1	knížecí
titulu	titul	k1gInSc2	titul
a	a	k8xC	a
panovnické	panovnický	k2eAgFnSc2d1	panovnická
patronace	patronace	k1gFnSc2	patronace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1367	[number]	k4	1367
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nový	nový	k2eAgInSc4d1	nový
spor	spor	k1gInSc4	spor
o	o	k7c4	o
privilegia	privilegium	k1gNnPc4	privilegium
mezi	mezi	k7c7	mezi
městskou	městský	k2eAgFnSc7d1	městská
radou	rada	k1gFnSc7	rada
a	a	k8xC	a
církví	církev	k1gFnSc7	církev
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
vyústil	vyústit	k5eAaPmAgInS	vyústit
opět	opět	k6eAd1	opět
v	v	k7c4	v
interdikt	interdikt	k1gInSc4	interdikt
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
rozhodně	rozhodně	k6eAd1	rozhodně
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
jeho	on	k3xPp3gInSc2	on
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
i	i	k8xC	i
politického	politický	k2eAgInSc2d1	politický
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Interdikt	interdikt	k1gInSc1	interdikt
zrušil	zrušit	k5eAaPmAgInS	zrušit
roku	rok	k1gInSc2	rok
1368	[number]	k4	1368
sám	sám	k3xTgInSc4	sám
papež	papež	k1gMnSc1	papež
Urban	Urban	k1gMnSc1	Urban
V.	V.	kA	V.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
císaře	císař	k1gMnSc4	císař
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
i	i	k9	i
k	k	k7c3	k
urovnání	urovnání	k1gNnSc3	urovnání
sporu	spor	k1gInSc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
soudní	soudní	k2eAgFnSc4d1	soudní
imunitu	imunita	k1gFnSc4	imunita
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
soudní	soudní	k2eAgFnSc4d1	soudní
pravomoc	pravomoc	k1gFnSc4	pravomoc
nad	nad	k7c7	nad
služebníky	služebník	k1gMnPc7	služebník
a	a	k8xC	a
poddanými	poddaný	k1gMnPc7	poddaný
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1370	[number]	k4	1370
navíc	navíc	k6eAd1	navíc
vydal	vydat	k5eAaPmAgInS	vydat
edikt	edikt	k1gInSc1	edikt
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
duchovním	duchovnět	k5eAaImIp1nS	duchovnět
zakazoval	zakazovat	k5eAaImAgMnS	zakazovat
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
pořizovat	pořizovat	k5eAaImF	pořizovat
nemovitosti	nemovitost	k1gFnPc4	nemovitost
nebo	nebo	k8xC	nebo
nakupovat	nakupovat	k5eAaBmF	nakupovat
domovní	domovní	k2eAgFnSc2d1	domovní
renty	renta	k1gFnSc2	renta
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
soustředěného	soustředěný	k2eAgNnSc2d1	soustředěné
úsilí	úsilí	k1gNnSc2	úsilí
Přeclava	Přeclava	k1gFnSc1	Přeclava
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
byla	být	k5eAaImAgFnS	být
konsolidace	konsolidace	k1gFnSc1	konsolidace
pozemkového	pozemkový	k2eAgInSc2d1	pozemkový
majetku	majetek	k1gInSc2	majetek
biskupství	biskupství	k1gNnSc2	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
centrum	centrum	k1gNnSc1	centrum
tvořilo	tvořit	k5eAaImAgNnS	tvořit
Niské	Niský	k2eAgNnSc1d1	Niský
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
proto	proto	k6eAd1	proto
věnoval	věnovat	k5eAaImAgMnS	věnovat
největší	veliký	k2eAgFnSc4d3	veliký
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
majetkových	majetkový	k2eAgFnPc2d1	majetková
transakcí	transakce	k1gFnPc2	transakce
odkupoval	odkupovat	k5eAaImAgInS	odkupovat
statky	statek	k1gInPc4	statek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nalézaly	nalézat	k5eAaImAgInP	nalézat
roztříštěně	roztříštěně	k6eAd1	roztříštěně
uvnitř	uvnitř	k7c2	uvnitř
biskupské	biskupský	k2eAgFnSc2d1	biskupská
země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
sousedství	sousedství	k1gNnSc6	sousedství
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
prodával	prodávat	k5eAaImAgMnS	prodávat
majetky	majetek	k1gInPc4	majetek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
s	s	k7c7	s
knížectvím	knížectví	k1gNnSc7	knížectví
územně	územně	k6eAd1	územně
nesouvisely	souviset	k5eNaImAgFnP	souviset
<g/>
;	;	kIx,	;
listiny	listina	k1gFnSc2	listina
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1346	[number]	k4	1346
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1348	[number]	k4	1348
výkup	výkup	k1gInSc4	výkup
zbývajících	zbývající	k2eAgInPc2d1	zbývající
cizích	cizí	k2eAgInPc2d1	cizí
knížecích	knížecí	k2eAgInPc2d1	knížecí
práv	právo	k1gNnPc2	právo
ke	k	k7c3	k
statkům	statek	k1gInPc3	statek
na	na	k7c6	na
biskupském	biskupský	k2eAgNnSc6d1	Biskupské
území	území	k1gNnSc6	území
výslovně	výslovně	k6eAd1	výslovně
umožňovaly	umožňovat	k5eAaImAgInP	umožňovat
a	a	k8xC	a
usnadňovaly	usnadňovat	k5eAaImAgInP	usnadňovat
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
prodaným	prodaný	k2eAgInSc7d1	prodaný
majetkem	majetek	k1gInSc7	majetek
bylo	být	k5eAaImAgNnS	být
panství	panství	k1gNnSc1	panství
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
hradu	hrad	k1gInSc2	hrad
Milíč	Milíč	k1gMnSc1	Milíč
<g/>
,	,	kIx,	,
náležející	náležející	k2eAgFnSc3d1	náležející
katedrální	katedrální	k2eAgFnSc3d1	katedrální
kapitule	kapitula	k1gFnSc3	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
statek	statek	k1gInSc4	statek
málo	málo	k6eAd1	málo
výnosný	výnosný	k2eAgInSc4d1	výnosný
a	a	k8xC	a
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
církevních	církevní	k2eAgFnPc2d1	církevní
držav	država	k1gFnPc2	država
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
strategická	strategický	k2eAgFnSc1d1	strategická
poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
polských	polský	k2eAgFnPc6d1	polská
hranicích	hranice	k1gFnPc6	hranice
byla	být	k5eAaImAgFnS	být
církvi	církev	k1gFnSc3	církev
spíše	spíše	k9	spíše
ke	k	k7c3	k
škodě	škoda	k1gFnSc3	škoda
než	než	k8xS	než
k	k	k7c3	k
užitku	užitek	k1gInSc3	užitek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1358	[number]	k4	1358
jej	on	k3xPp3gMnSc4	on
proto	proto	k8xC	proto
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
kapitula	kapitula	k1gFnSc1	kapitula
prodali	prodat	k5eAaPmAgMnP	prodat
olešnickému	olešnický	k2eAgNnSc3d1	Olešnické
knížeti	kníže	k1gNnSc3wR	kníže
Konrádovi	Konrád	k1gMnSc3	Konrád
I.	I.	kA	I.
Výtěžek	výtěžek	k1gInSc1	výtěžek
sloužil	sloužit	k5eAaImAgInS	sloužit
zejména	zejména	k9	zejména
k	k	k7c3	k
nákladnému	nákladný	k2eAgNnSc3d1	nákladné
vykoupení	vykoupení	k1gNnSc3	vykoupení
hradu	hrad	k1gInSc2	hrad
Frýdberka	Frýdberka	k1gFnSc1	Frýdberka
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
majitelé	majitel	k1gMnPc1	majitel
plenili	plenit	k5eAaImAgMnP	plenit
a	a	k8xC	a
ohrožovali	ohrožovat	k5eAaImAgMnP	ohrožovat
biskupské	biskupský	k2eAgFnPc4d1	biskupská
vesnice	vesnice	k1gFnPc4	vesnice
v	v	k7c6	v
Rychlebských	Rychlebský	k2eAgFnPc6d1	Rychlebská
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
získal	získat	k5eAaPmAgMnS	získat
biskup	biskup	k1gMnSc1	biskup
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
přímého	přímý	k2eAgNnSc2d1	přímé
držení	držení	k1gNnSc2	držení
hrady	hrad	k1gInPc1	hrad
Kaltenštejn	Kaltenštejn	k1gNnSc1	Kaltenštejn
(	(	kIx(	(
<g/>
1345	[number]	k4	1345
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jánský	jánský	k2eAgInSc1d1	jánský
Vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
1348	[number]	k4	1348
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1350	[number]	k4	1350
upevnil	upevnit	k5eAaPmAgInS	upevnit
také	také	k9	také
dosud	dosud	k6eAd1	dosud
spornou	sporný	k2eAgFnSc4d1	sporná
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
biskupství	biskupství	k1gNnSc2	biskupství
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
Wiązów	Wiązów	k1gFnSc2	Wiązów
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Wansen	Wansen	k1gInSc1	Wansen
<g/>
)	)	kIx)	)
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
,	,	kIx,	,
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
wansenskou	wansenský	k2eAgFnSc7d1	wansenský
zastávkou	zastávka	k1gFnSc7	zastávka
<g/>
,	,	kIx,	,
na	na	k7c4	na
půl	půl	k1xP	půl
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
Nisou	Nisa	k1gFnSc7	Nisa
a	a	k8xC	a
Vratislaví	Vratislav	k1gFnSc7	Vratislav
<g/>
,	,	kIx,	,
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
též	též	k9	též
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
a	a	k8xC	a
hradem	hrad	k1gInSc7	hrad
Pačkovem	Pačkov	k1gInSc7	Pačkov
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
cípu	cíp	k1gInSc6	cíp
Niska	Nisek	k1gInSc2	Nisek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dal	dát	k5eAaPmAgInS	dát
vystavět	vystavět	k5eAaPmF	vystavět
mohutné	mohutný	k2eAgMnPc4d1	mohutný
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
hmoty	hmota	k1gFnSc2	hmota
zachované	zachovaný	k2eAgNnSc4d1	zachované
opevnění	opevnění	k1gNnSc4	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Přeclav	Přeclat	k5eAaPmDgInS	Přeclat
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
nejen	nejen	k6eAd1	nejen
o	o	k7c4	o
rozlohu	rozloha	k1gFnSc4	rozloha
biskupského	biskupský	k2eAgNnSc2d1	Biskupské
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c4	o
jeho	jeho	k3xOp3gInSc4	jeho
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Vymohl	vymoct	k5eAaPmAgMnS	vymoct
si	se	k3xPyFc3	se
na	na	k7c6	na
Janu	Jan	k1gMnSc6	Jan
Lucemburském	lucemburský	k2eAgMnSc6d1	lucemburský
výslovné	výslovný	k2eAgNnSc4d1	výslovné
potvrzení	potvrzení	k1gNnSc4	potvrzení
zakládání	zakládání	k1gNnSc2	zakládání
vsí	ves	k1gFnPc2	ves
na	na	k7c6	na
hospodářskému	hospodářský	k2eAgInSc3d1	hospodářský
rozvoji	rozvoj	k1gInSc6	rozvoj
příznivějším	příznivý	k2eAgInSc6d2	příznivější
"	"	kIx"	"
<g/>
německém	německý	k2eAgInSc6d1	německý
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
emfyteutickém	emfyteutický	k2eAgNnSc6d1	emfyteutické
<g/>
)	)	kIx)	)
právu	právo	k1gNnSc6	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
možnost	možnost	k1gFnSc4	možnost
změny	změna	k1gFnSc2	změna
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
již	již	k6eAd1	již
existujících	existující	k2eAgFnPc6d1	existující
vsích	ves	k1gFnPc6	ves
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
právo	právo	k1gNnSc4	právo
zavedl	zavést	k5eAaPmAgInS	zavést
i	i	k9	i
do	do	k7c2	do
Otmuchova	Otmuchův	k2eAgInSc2d1	Otmuchův
(	(	kIx(	(
<g/>
1347	[number]	k4	1347
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
město	město	k1gNnSc4	město
s	s	k7c7	s
německým	německý	k2eAgNnSc7d1	německé
právem	právo	k1gNnSc7	právo
povýšil	povýšit	k5eAaPmAgInS	povýšit
Javorník	Javorník	k1gInSc1	Javorník
(	(	kIx(	(
<g/>
1373	[number]	k4	1373
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
i	i	k9	i
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
modernizoval	modernizovat	k5eAaBmAgMnS	modernizovat
opevnění	opevnění	k1gNnPc4	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
ohledech	ohled	k1gInPc6	ohled
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
přírůstkem	přírůstek	k1gInSc7	přírůstek
bylo	být	k5eAaImAgNnS	být
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1344	[number]	k4	1344
odkoupení	odkoupení	k1gNnSc4	odkoupení
města	město	k1gNnSc2	město
Grodkova	Grodkův	k2eAgNnSc2d1	Grodkův
(	(	kIx(	(
<g/>
pol.	pol.	k?	pol.
Grodków	Grodków	k1gMnSc1	Grodków
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Grottkau	Grottkaa	k1gFnSc4	Grottkaa
<g/>
)	)	kIx)	)
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
od	od	k7c2	od
trvale	trvale	k6eAd1	trvale
zadluženého	zadlužený	k2eAgMnSc2d1	zadlužený
břežského	břežský	k2eAgMnSc2d1	břežský
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Marnotratného	marnotratný	k2eAgInSc2d1	marnotratný
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
léno	léno	k1gNnSc4	léno
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
biskup	biskup	k1gInSc4	biskup
a	a	k8xC	a
kapitula	kapitula	k1gFnSc1	kapitula
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
drželi	držet	k5eAaImAgMnP	držet
nově	nově	k6eAd1	nově
nabyté	nabytý	k2eAgNnSc4d1	nabyté
území	území	k1gNnSc4	území
napůl	napůl	k6eAd1	napůl
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
složit	složit	k5eAaPmF	složit
lenní	lenní	k2eAgFnSc4d1	lenní
přísahu	přísaha	k1gFnSc4	přísaha
(	(	kIx(	(
<g/>
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
nisko-otmuchovské	niskotmuchovský	k2eAgNnSc4d1	nisko-otmuchovský
území	území	k1gNnSc4	území
lénu	léno	k1gNnSc3	léno
nepodléhalo	podléhat	k5eNaImAgNnS	podléhat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dědicové	dědic	k1gMnPc1	dědic
po	po	k7c6	po
břežském	břežský	k2eAgNnSc6d1	břežský
knížeti	kníže	k1gNnSc6wR	kníže
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
I.	I.	kA	I.
Lehnicko-břežský	Lehnickořežský	k2eAgMnSc1d1	Lehnicko-břežský
a	a	k8xC	a
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
darování	darování	k1gNnPc4	darování
neuznávali	uznávat	k5eNaImAgMnP	uznávat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1360	[number]	k4	1360
Grodkov	Grodkov	k1gInSc1	Grodkov
vojensky	vojensky	k6eAd1	vojensky
obsadili	obsadit	k5eAaPmAgMnP	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatečně	dostatečně	k6eNd1	dostatečně
energický	energický	k2eAgInSc1d1	energický
postup	postup	k1gInSc1	postup
biskupa	biskup	k1gInSc2	biskup
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
rozhořčení	rozhořčení	k1gNnSc4	rozhořčení
katedrální	katedrální	k2eAgFnSc2d1	katedrální
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
finančních	finanční	k2eAgFnPc2d1	finanční
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
Grodkov	Grodkov	k1gInSc4	Grodkov
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zde	zde	k6eAd1	zde
biskup	biskup	k1gMnSc1	biskup
podnítil	podnítit	k5eAaPmAgMnS	podnítit
modernizaci	modernizace	k1gFnSc4	modernizace
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
doplněním	doplnění	k1gNnSc7	doplnění
věží	věž	k1gFnPc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Získání	získání	k1gNnSc1	získání
Grodkova	Grodkův	k2eAgInSc2d1	Grodkův
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
i	i	k9	i
v	v	k7c6	v
titulatuře	titulatura	k1gFnSc6	titulatura
<g/>
:	:	kIx,	:
nadále	nadále	k6eAd1	nadále
byl	být	k5eAaImAgInS	být
vratislavský	vratislavský	k2eAgInSc1d1	vratislavský
biskup	biskup	k1gInSc1	biskup
vévodou	vévoda	k1gMnSc7	vévoda
(	(	kIx(	(
<g/>
Herzog	Herzog	k1gMnSc1	Herzog
<g/>
)	)	kIx)	)
grodkovským	grodkovský	k2eAgFnPc3d1	grodkovský
a	a	k8xC	a
knížetem	kníže	k1gNnSc7wR	kníže
(	(	kIx(	(
<g/>
Fürst	Fürst	k1gMnSc1	Fürst
<g/>
)	)	kIx)	)
niským	niský	k1gMnSc7	niský
či	či	k8xC	či
nisko-otmuchovským	niskotmuchovský	k2eAgMnSc7d1	nisko-otmuchovský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
staletích	staletí	k1gNnPc6	staletí
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
rozlišování	rozlišování	k1gNnSc1	rozlišování
vytratilo	vytratit	k5eAaPmAgNnS	vytratit
a	a	k8xC	a
léno	léno	k1gNnSc1	léno
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
útvar	útvar	k1gInSc4	útvar
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
knížectví	knížectví	k1gNnSc3	knížectví
nisko-grodkovské	niskorodkovský	k2eAgFnPc1d1	nisko-grodkovský
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
majetková	majetkový	k2eAgFnSc1d1	majetková
správa	správa	k1gFnSc1	správa
biskupa	biskup	k1gMnSc2	biskup
<g/>
,	,	kIx,	,
politické	politický	k2eAgNnSc4d1	politické
uklidnění	uklidnění	k1gNnSc4	uklidnění
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
i	i	k9	i
celkový	celkový	k2eAgInSc1d1	celkový
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
vzestup	vzestup	k1gInSc1	vzestup
se	se	k3xPyFc4	se
kladně	kladně	k6eAd1	kladně
odrazily	odrazit	k5eAaPmAgFnP	odrazit
zejména	zejména	k9	zejména
na	na	k7c4	na
zřizování	zřizování	k1gNnSc4	zřizování
řady	řada	k1gFnSc2	řada
nových	nový	k2eAgFnPc2d1	nová
farností	farnost	k1gFnPc2	farnost
a	a	k8xC	a
stavbě	stavba	k1gFnSc3	stavba
nových	nový	k2eAgInPc2d1	nový
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
diecézi	diecéze	k1gFnSc6	diecéze
310	[number]	k4	310
farností	farnost	k1gFnPc2	farnost
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1370	[number]	k4	1370
již	již	k9	již
1030	[number]	k4	1030
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Přeclavova	Přeclavův	k2eAgFnSc1d1	Přeclavův
doba	doba	k1gFnSc1	doba
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
poslední	poslední	k2eAgFnSc1d1	poslední
dobou	doba	k1gFnSc7	doba
velkých	velký	k2eAgFnPc2d1	velká
církevních	církevní	k2eAgFnPc2d1	církevní
fundací	fundace	k1gFnPc2	fundace
<g/>
.	.	kIx.	.
</s>
<s>
Lehnický	Lehnický	k2eAgMnSc1d1	Lehnický
kníže	kníže	k1gMnSc1	kníže
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1354	[number]	k4	1354
v	v	k7c6	v
Lehnici	Lehnice	k1gFnSc6	Lehnice
kolegiátní	kolegiátní	k2eAgFnSc4d1	kolegiátní
kapitulu	kapitula	k1gFnSc4	kapitula
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
břežský	břežský	k2eAgMnSc1d1	břežský
kníže	kníže	k1gMnSc1	kníže
Ludvíkv	Ludvíkv	k1gMnSc1	Ludvíkv
I.	I.	kA	I.
založil	založit	k5eAaPmAgMnS	založit
při	při	k7c6	při
kapli	kaple	k1gFnSc6	kaple
břežského	břežský	k2eAgInSc2d1	břežský
hradu	hrad	k1gInSc2	hrad
roku	rok	k1gInSc2	rok
1368	[number]	k4	1368
kolegiátní	kolegiátní	k2eAgFnSc4d1	kolegiátní
kapitulu	kapitula	k1gFnSc4	kapitula
svaté	svatý	k2eAgFnSc2d1	svatá
Hedviky	Hedvika	k1gFnSc2	Hedvika
a	a	k8xC	a
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
založili	založit	k5eAaPmAgMnP	založit
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
v	v	k7c6	v
Lehnici	Lehnice	k1gFnSc6	Lehnice
klášter	klášter	k1gInSc1	klášter
benediktinek	benediktinka	k1gFnPc2	benediktinka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
založil	založit	k5eAaPmAgMnS	založit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1351	[number]	k4	1351
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
Kazimírem	Kazimír	k1gMnSc7	Kazimír
Velikým	veliký	k2eAgMnSc7d1	veliký
kostel	kostel	k1gInSc1	kostel
zasvěcený	zasvěcený	k2eAgInSc1d1	zasvěcený
patronům	patron	k1gInPc3	patron
obou	dva	k4xCgFnPc6	dva
království	království	k1gNnPc4	království
<g/>
,	,	kIx,	,
svatému	svatý	k1gMnSc3	svatý
Stanislavovi	Stanislav	k1gMnSc3	Stanislav
a	a	k8xC	a
svatému	svatý	k1gMnSc3	svatý
Václavovi	Václav	k1gMnSc3	Václav
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
svaté	svatý	k2eAgFnSc3d1	svatá
Dorotě	Dorota	k1gFnSc3	Dorota
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
klášter	klášter	k1gInSc4	klášter
augustiniánů	augustinián	k1gMnPc2	augustinián
poustevníků	poustevník	k1gMnPc2	poustevník
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
přišly	přijít	k5eAaPmAgInP	přijít
nové	nový	k2eAgInPc1d1	nový
řeholní	řeholní	k2eAgInPc1d1	řeholní
řády	řád	k1gInPc1	řád
-	-	kIx~	-
karmelitáni	karmelitán	k1gMnPc1	karmelitán
<g/>
,	,	kIx,	,
kartuziáni	kartuzián	k1gMnPc1	kartuzián
a	a	k8xC	a
pavlíni	pavlín	k1gMnPc1	pavlín
<g/>
.	.	kIx.	.
</s>
<s>
Množil	množit	k5eAaImAgInS	množit
se	se	k3xPyFc4	se
také	také	k9	také
počet	počet	k1gInSc1	počet
špitálů	špitál	k1gInPc2	špitál
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
různých	různý	k2eAgFnPc2d1	různá
institucí	instituce	k1gFnPc2	instituce
<g/>
:	:	kIx,	:
jako	jako	k8xC	jako
vzorové	vzorový	k2eAgFnSc2d1	vzorová
založil	založit	k5eAaPmAgMnS	založit
Přeclav	Přeclav	k1gMnSc4	Přeclav
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
v	v	k7c6	v
biskupské	biskupský	k2eAgFnSc6d1	biskupská
Nise	Nisa	k1gFnSc6	Nisa
mužský	mužský	k2eAgInSc4d1	mužský
útulek	útulek	k1gInSc4	útulek
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
útulek	útulek	k1gInSc4	útulek
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
vrcholně	vrcholně	k6eAd1	vrcholně
gotických	gotický	k2eAgFnPc2d1	gotická
staveb	stavba	k1gFnPc2	stavba
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
biskupa	biskup	k1gInSc2	biskup
Přeclava	Přeclava	k1gFnSc1	Přeclava
<g/>
,	,	kIx,	,
nejosobnější	osobní	k2eAgFnSc4d3	nejosobnější
památku	památka	k1gFnSc4	památka
však	však	k9	však
zanechal	zanechat	k5eAaPmAgMnS	zanechat
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
východní	východní	k2eAgFnSc6d1	východní
pobočné	pobočný	k2eAgFnSc6d1	pobočná
kapli	kaple	k1gFnSc6	kaple
vratislavské	vratislavský	k2eAgFnSc2d1	Vratislavská
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
zasvěcené	zasvěcený	k2eAgFnSc3d1	zasvěcená
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
vyzdobená	vyzdobený	k2eAgFnSc1d1	vyzdobená
freskami	freska	k1gFnPc7	freska
a	a	k8xC	a
vitrážemi	vitráž	k1gFnPc7	vitráž
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1361	[number]	k4	1361
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
zřídil	zřídit	k5eAaPmAgMnS	zřídit
i	i	k9	i
dotaci	dotace	k1gFnSc4	dotace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
financovala	financovat	k5eAaBmAgFnS	financovat
denní	denní	k2eAgFnPc4d1	denní
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
dvanácti	dvanáct	k4xCc2	dvanáct
oltářníky	oltářník	k1gMnPc4	oltářník
<g/>
.	.	kIx.	.
</s>
<s>
Mentalita	mentalita	k1gFnSc1	mentalita
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
měnila	měnit	k5eAaImAgFnS	měnit
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
neuspokojivý	uspokojivý	k2eNgInSc1d1	neuspokojivý
stav	stav	k1gInSc1	stav
papežství	papežství	k1gNnPc2	papežství
a	a	k8xC	a
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
církevními	církevní	k2eAgFnPc7d1	církevní
frakcemi	frakce	k1gFnPc7	frakce
za	za	k7c2	za
Přeclavových	Přeclavový	k2eAgMnPc2d1	Přeclavový
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
způsobily	způsobit	k5eAaPmAgInP	způsobit
jistý	jistý	k2eAgInSc4d1	jistý
pokles	pokles	k1gInSc4	pokles
prestiže	prestiž	k1gFnSc2	prestiž
církve	církev	k1gFnSc2	církev
i	i	k9	i
přes	přes	k7c4	přes
její	její	k3xOp3gInSc4	její
materiální	materiální	k2eAgInSc4d1	materiální
rozkvět	rozkvět	k1gInSc4	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
přítomností	přítomnost	k1gFnSc7	přítomnost
sekt	sekta	k1gFnPc2	sekta
a	a	k8xC	a
lidových	lidový	k2eAgNnPc2d1	lidové
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
kvalifikovaných	kvalifikovaný	k2eAgFnPc2d1	kvalifikovaná
oficiální	oficiální	k2eAgFnSc7d1	oficiální
církví	církev	k1gFnSc7	církev
jako	jako	k8xC	jako
kacířství	kacířství	k1gNnSc2	kacířství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1349	[number]	k4	1349
například	například	k6eAd1	například
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
přišli	přijít	k5eAaPmAgMnP	přijít
flagelanti	flagelant	k1gMnPc1	flagelant
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
excesivní	excesivní	k2eAgInPc4d1	excesivní
projevy	projev	k1gInPc4	projev
zbožnosti	zbožnost	k1gFnSc2	zbožnost
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
jednomu	jeden	k4xCgNnSc3	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
protižidovských	protižidovský	k2eAgInPc2d1	protižidovský
pogromů	pogrom	k1gInPc2	pogrom
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Biskup	biskup	k1gInSc1	biskup
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
rázně	rázně	k6eAd1	rázně
zakročil	zakročit	k5eAaPmAgMnS	zakročit
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
jejich	jejich	k3xOp3gMnSc4	jejich
vůdce	vůdce	k1gMnSc4	vůdce
upálit	upálit	k5eAaPmF	upálit
<g/>
.	.	kIx.	.
</s>
<s>
Přeclav	Přeclat	k5eAaPmDgInS	Přeclat
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
zavedl	zavést	k5eAaPmAgMnS	zavést
v	v	k7c6	v
diecézi	diecéze	k1gFnSc6	diecéze
svěcení	svěcení	k1gNnSc2	svěcení
svátku	svátek	k1gInSc2	svátek
svaté	svatý	k2eAgFnSc2d1	svatá
Hedviky	Hedvika	k1gFnSc2	Hedvika
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
místní	místní	k2eAgFnPc4d1	místní
patronky	patronka	k1gFnPc4	patronka
<g/>
.	.	kIx.	.
</s>
<s>
Biskup	biskup	k1gMnSc1	biskup
Přeclav	Přeclav	k1gMnSc1	Přeclav
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
biskupském	biskupský	k2eAgInSc6d1	biskupský
hradě	hrad	k1gInSc6	hrad
Otmuchově	Otmuchův	k2eAgInSc6d1	Otmuchův
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1376	[number]	k4	1376
<g/>
.	.	kIx.	.
</s>
<s>
Pochován	pochován	k2eAgMnSc1d1	pochován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
mariánské	mariánský	k2eAgFnSc6d1	Mariánská
kapli	kaple	k1gFnSc6	kaple
vratislavské	vratislavský	k2eAgFnSc2d1	Vratislavská
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dal	dát	k5eAaPmAgMnS	dát
sám	sám	k3xTgMnSc1	sám
vystavět	vystavět	k5eAaPmF	vystavět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
dosud	dosud	k6eAd1	dosud
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
náhrobek	náhrobek	k1gInSc1	náhrobek
je	být	k5eAaImIp3nS	být
cennou	cenný	k2eAgFnSc7d1	cenná
památkou	památka	k1gFnSc7	památka
gotického	gotický	k2eAgNnSc2d1	gotické
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Přeclavovou	Přeclavový	k2eAgFnSc7d1	Přeclavový
smrtí	smrt	k1gFnSc7	smrt
skončilo	skončit	k5eAaPmAgNnS	skončit
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
vrcholných	vrcholný	k2eAgNnPc2d1	vrcholné
období	období	k1gNnPc2	období
dějin	dějiny	k1gFnPc2	dějiny
vratislavské	vratislavský	k2eAgFnSc2d1	Vratislavská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
z	z	k7c2	z
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
hlediska	hledisko	k1gNnSc2	hledisko
bylo	být	k5eAaImAgNnS	být
biskupství	biskupství	k1gNnSc4	biskupství
tak	tak	k6eAd1	tak
mocné	mocný	k2eAgFnPc1d1	mocná
jako	jako	k8xS	jako
nikdy	nikdy	k6eAd1	nikdy
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
rovněž	rovněž	k9	rovněž
rychlé	rychlý	k2eAgNnSc4d1	rychlé
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
farní	farní	k2eAgFnSc2d1	farní
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
zakládání	zakládání	k1gNnSc1	zakládání
nových	nový	k2eAgFnPc2d1	nová
církevních	církevní	k2eAgFnPc2d1	církevní
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
nalezení	nalezení	k1gNnSc4	nalezení
smírného	smírný	k2eAgInSc2d1	smírný
modu	modus	k1gInSc2	modus
vivendi	vivend	k1gMnPc1	vivend
se	s	k7c7	s
světskou	světský	k2eAgFnSc7d1	světská
mocí	moc	k1gFnSc7	moc
pozvedlo	pozvednout	k5eAaPmAgNnS	pozvednout
-	-	kIx~	-
ač	ač	k8xS	ač
jen	jen	k9	jen
dočasně	dočasně	k6eAd1	dočasně
-	-	kIx~	-
společenskou	společenský	k2eAgFnSc4d1	společenská
vážnost	vážnost	k1gFnSc4	vážnost
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
před	před	k7c7	před
Přeclavovým	Přeclavový	k2eAgInSc7d1	Přeclavový
nástupem	nástup	k1gInSc7	nástup
řadou	řada	k1gFnSc7	řada
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
i	i	k8xC	i
vnějších	vnější	k2eAgInPc2d1	vnější
sporů	spor	k1gInPc2	spor
oslabena	oslaben	k2eAgFnSc1d1	oslabena
<g/>
.	.	kIx.	.
</s>
