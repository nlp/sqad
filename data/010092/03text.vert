<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Bartoš	Bartoš	k1gMnSc1	Bartoš
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1980	[number]	k4	1980
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
aktivista	aktivista	k1gMnSc1	aktivista
<g/>
,	,	kIx,	,
softwarový	softwarový	k2eAgMnSc1d1	softwarový
vývojář	vývojář	k1gMnSc1	vývojář
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
České	český	k2eAgFnSc2d1	Česká
pirátské	pirátský	k2eAgFnSc2d1	pirátská
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
České	český	k2eAgFnSc2d1	Česká
pirátské	pirátský	k2eAgFnSc2d1	pirátská
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
do	do	k7c2	do
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
s	s	k7c7	s
přestávkou	přestávka	k1gFnSc7	přestávka
mezi	mezi	k7c7	mezi
červnem	červen	k1gInSc7	červen
a	a	k8xC	a
zářím	září	k1gNnSc7	září
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Bartoš	Bartoš	k1gMnSc1	Bartoš
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
Jablonci	Jablonec	k1gInSc6	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
maturoval	maturovat	k5eAaBmAgMnS	maturovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
magisterský	magisterský	k2eAgInSc1d1	magisterský
obor	obor	k1gInSc1	obor
informační	informační	k2eAgInSc1d1	informační
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
PhDr.	PhDr.	kA	PhDr.
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
informační	informační	k2eAgInSc1d1	informační
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
semestru	semestr	k1gInSc2	semestr
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
počítačových	počítačový	k2eAgFnPc2d1	počítačová
věd	věda	k1gFnPc2	věda
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
New	New	k1gFnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Doktorské	doktorský	k2eAgNnSc1d1	doktorské
studium	studium	k1gNnSc1	studium
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
obdržel	obdržet	k5eAaPmAgMnS	obdržet
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
<g/>
Pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
firmy	firma	k1gFnPc4	firma
jako	jako	k8xS	jako
specialista	specialista	k1gMnSc1	specialista
na	na	k7c4	na
informační	informační	k2eAgFnPc4d1	informační
technologie	technologie	k1gFnPc4	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
IT	IT	kA	IT
kariéru	kariéra	k1gFnSc4	kariéra
přerušil	přerušit	k5eAaPmAgMnS	přerušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
a	a	k8xC	a
jako	jako	k9	jako
ředitel	ředitel	k1gMnSc1	ředitel
marketingu	marketing	k1gInSc2	marketing
startoval	startovat	k5eAaBmAgInS	startovat
nový	nový	k2eAgInSc1d1	nový
český	český	k2eAgInSc1d1	český
pracovní	pracovní	k2eAgInSc1d1	pracovní
portál	portál	k1gInSc1	portál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
projektech	projekt	k1gInPc6	projekt
týkajících	týkající	k2eAgInPc6d1	týkající
se	se	k3xPyFc4	se
kvality	kvalita	k1gFnSc2	kvalita
dat	datum	k1gNnPc2	datum
ve	v	k7c6	v
firmách	firma	k1gFnPc6	firma
<g/>
,	,	kIx,	,
budování	budování	k1gNnSc6	budování
datových	datový	k2eAgInPc2d1	datový
skladů	sklad	k1gInPc2	sklad
a	a	k8xC	a
návrhů	návrh	k1gInPc2	návrh
manažerských	manažerský	k2eAgInPc2d1	manažerský
informačních	informační	k2eAgInPc2d1	informační
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
také	také	k9	také
přednáškové	přednáškový	k2eAgFnPc4d1	přednášková
činnosti	činnost	k1gFnPc4	činnost
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
autorského	autorský	k2eAgNnSc2d1	autorské
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
informační	informační	k2eAgFnSc2d1	informační
společnosti	společnost	k1gFnSc2	společnost
či	či	k8xC	či
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Angažuje	angažovat	k5eAaBmIp3nS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
aktivitách	aktivita	k1gFnPc6	aktivita
souvisejících	související	k2eAgFnPc2d1	související
s	s	k7c7	s
DIY	DIY	kA	DIY
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
diskžokej	diskžokej	k1gMnSc1	diskžokej
mixuje	mixovat	k5eAaImIp3nS	mixovat
psychedelický	psychedelický	k2eAgInSc4d1	psychedelický
trance	tranec	k1gInPc4	tranec
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
akordeon	akordeon	k1gInSc4	akordeon
a	a	k8xC	a
zpívá	zpívat	k5eAaImIp3nS	zpívat
v	v	k7c6	v
punkové	punkový	k2eAgFnSc3d1	punková
kapele	kapela	k1gFnSc3	kapela
Nohama	noha	k1gFnPc7	noha
napřed	napřed	k6eAd1	napřed
<g/>
.	.	kIx.	.
</s>
<s>
Bartoš	Bartoš	k1gMnSc1	Bartoš
také	také	k9	také
hrával	hrávat	k5eAaImAgMnS	hrávat
na	na	k7c4	na
varhany	varhany	k1gFnPc4	varhany
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Lydií	Lydie	k1gFnSc7	Lydie
Frankovou	Franková	k1gFnSc7	Franková
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
členkou	členka	k1gFnSc7	členka
Pirátské	pirátský	k2eAgFnSc2d1	pirátská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Názory	názor	k1gInPc1	názor
==	==	k?	==
</s>
</p>
<p>
<s>
Pirátskou	pirátský	k2eAgFnSc4d1	pirátská
stranu	strana	k1gFnSc4	strana
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
jedinou	jediný	k2eAgFnSc4d1	jediná
skutečně	skutečně	k6eAd1	skutečně
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
politickou	politický	k2eAgFnSc4d1	politická
platformu	platforma	k1gFnSc4	platforma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
reflektovat	reflektovat	k5eAaImF	reflektovat
realitu	realita	k1gFnSc4	realita
třetího	třetí	k4xOgNnSc2	třetí
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Technologie	technologie	k1gFnPc1	technologie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
máme	mít	k5eAaImIp1nP	mít
nyní	nyní	k6eAd1	nyní
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
nám	my	k3xPp1nPc3	my
mohou	moct	k5eAaImIp3nP	moct
úžasně	úžasně	k6eAd1	úžasně
pomáhat	pomáhat	k5eAaImF	pomáhat
a	a	k8xC	a
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
ulehčit	ulehčit	k5eAaPmF	ulehčit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
stát	stát	k5eAaPmF	stát
nástrojem	nástroj	k1gInSc7	nástroj
digitální	digitální	k2eAgFnSc2d1	digitální
totality	totalita	k1gFnSc2	totalita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
pacifistu	pacifista	k1gMnSc4	pacifista
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kritický	kritický	k2eAgInSc1d1	kritický
vůči	vůči	k7c3	vůči
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Podepsal	podepsat	k5eAaPmAgMnS	podepsat
petici	petice	k1gFnSc4	petice
proti	proti	k7c3	proti
konání	konání	k1gNnSc3	konání
Dnů	den	k1gInPc2	den
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
signatáři	signatář	k1gMnPc1	signatář
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
festival	festival	k1gInSc1	festival
pomáha	pomáha	k1gMnSc1	pomáha
Izraeli	Izrael	k1gMnSc3	Izrael
k	k	k7c3	k
legitimizaci	legitimizace	k1gFnSc3	legitimizace
anexe	anexe	k1gFnSc2	anexe
Východního	východní	k2eAgInSc2d1	východní
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Rezoluce	rezoluce	k1gFnSc2	rezoluce
RB	RB	kA	RB
OSN	OSN	kA	OSN
č.	č.	k?	č.
478	[number]	k4	478
porušením	porušení	k1gNnSc7	porušení
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
také	také	k9	také
anexi	anexe	k1gFnSc4	anexe
Krymu	Krym	k1gInSc2	Krym
Ruskou	ruský	k2eAgFnSc7d1	ruská
federací	federace	k1gFnSc7	federace
<g/>
.	.	kIx.	.
<g/>
Prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
zavedení	zavedení	k1gNnSc1	zavedení
jmenovitého	jmenovitý	k2eAgNnSc2d1	jmenovité
hlasování	hlasování	k1gNnSc2	hlasování
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
radách	rada	k1gFnPc6	rada
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
Bartoše	Bartoš	k1gMnSc2	Bartoš
"	"	kIx"	"
<g/>
Lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
vědět	vědět	k5eAaImF	vědět
i	i	k8xC	i
zpětně	zpětně	k6eAd1	zpětně
si	se	k3xPyFc3	se
jednoduše	jednoduše	k6eAd1	jednoduše
dohledat	dohledat	k5eAaPmF	dohledat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jejich	jejich	k3xOp3gMnPc1	jejich
zvolení	zvolený	k2eAgMnPc1d1	zvolený
zastupitelé	zastupitel	k1gMnPc1	zastupitel
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
působení	působení	k1gNnSc1	působení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
obhájil	obhájit	k5eAaPmAgMnS	obhájit
na	na	k7c6	na
Celostátním	celostátní	k2eAgInSc6d1	celostátní
fóru	fór	k1gInSc6	fór
Pirátů	pirát	k1gMnPc2	pirát
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
post	posta	k1gFnPc2	posta
předsedy	předseda	k1gMnSc2	předseda
České	český	k2eAgFnSc2d1	Česká
pirátské	pirátský	k2eAgFnSc2d1	pirátská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
když	když	k8xS	když
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
89	[number]	k4	89
ze	z	k7c2	z
116	[number]	k4	116
delegátů	delegát	k1gMnPc2	delegát
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
77	[number]	k4	77
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
lídr	lídr	k1gMnSc1	lídr
Pirátů	pirát	k1gMnPc2	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Piráti	pirát	k1gMnPc1	pirát
získali	získat	k5eAaPmAgMnP	získat
jen	jen	k6eAd1	jen
4,78	[number]	k4	4,78
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
do	do	k7c2	do
EP	EP	kA	EP
se	se	k3xPyFc4	se
nedostali	dostat	k5eNaPmAgMnP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Bartoš	Bartoš	k1gMnSc1	Bartoš
však	však	k9	však
získal	získat	k5eAaPmAgMnS	získat
12	[number]	k4	12
644	[number]	k4	644
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
13	[number]	k4	13
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
oznámil	oznámit	k5eAaPmAgMnS	oznámit
rezignaci	rezignace	k1gFnSc4	rezignace
na	na	k7c4	na
předsednickou	předsednický	k2eAgFnSc4d1	předsednická
funkci	funkce	k1gFnSc4	funkce
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
to	ten	k3xDgNnSc4	ten
jednak	jednak	k8xC	jednak
únavou	únava	k1gFnSc7	únava
po	po	k7c6	po
předvolební	předvolební	k2eAgFnSc6d1	předvolební
kampani	kampaň	k1gFnSc6	kampaň
a	a	k8xC	a
jednak	jednak	k8xC	jednak
názorovým	názorový	k2eAgNnSc7d1	názorové
rozdělením	rozdělení	k1gNnSc7	rozdělení
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
Celostátního	celostátní	k2eAgNnSc2d1	celostátní
fóra	fórum	k1gNnSc2	fórum
České	český	k2eAgFnSc2d1	Česká
pirátské	pirátský	k2eAgFnSc2d1	pirátská
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
byl	být	k5eAaImAgMnS	být
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
lídrem	lídr	k1gMnSc7	lídr
Pirátů	pirát	k1gMnPc2	pirát
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
13	[number]	k4	13
361	[number]	k4	361
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Celostátním	celostátní	k2eAgNnSc6d1	celostátní
fóru	fórum	k1gNnSc6	fórum
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
Pirátů	pirát	k1gMnPc2	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
jediný	jediný	k2eAgMnSc1d1	jediný
kandidát	kandidát	k1gMnSc1	kandidát
získal	získat	k5eAaPmAgMnS	získat
276	[number]	k4	276
hlasů	hlas	k1gInPc2	hlas
z	z	k7c2	z
293	[number]	k4	293
hlasujících	hlasující	k2eAgMnPc2d1	hlasující
členů	člen	k1gMnPc2	člen
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
94	[number]	k4	94
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ivan	Ivan	k1gMnSc1	Ivan
Bartoš	Bartoš	k1gMnSc1	Bartoš
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Ivan	Ivan	k1gMnSc1	Ivan
Bartoš	Bartoš	k1gMnSc1	Bartoš
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
http://www.pirati.cz/lide/ivan_bartos	[url]	k1gMnSc1	http://www.pirati.cz/lide/ivan_bartos
Ivan	Ivan	k1gMnSc1	Ivan
Bartoš	Bartoš	k1gMnSc1	Bartoš
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Pirátů	pirát	k1gMnPc2	pirát
</s>
</p>
<p>
<s>
Blog	Blog	k1gInSc1	Blog
na	na	k7c4	na
Aktualne	Aktualn	k1gInSc5	Aktualn
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Bartošna	Bartošn	k1gInSc2	Bartošn
Facebooku	Facebook	k1gInSc2	Facebook
</s>
</p>
<p>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Ivanem	Ivan	k1gMnSc7	Ivan
Bartošem	Bartoš	k1gMnSc7	Bartoš
pro	pro	k7c4	pro
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
z	z	k7c2	z
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
</s>
</p>
<p>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Ivanem	Ivan	k1gMnSc7	Ivan
Bartošem	Bartoš	k1gMnSc7	Bartoš
pro	pro	k7c4	pro
A	a	k9	a
<g/>
2	[number]	k4	2
<g/>
larm	larma	k1gFnPc2	larma
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Bartoš	Bartoš	k1gMnSc1	Bartoš
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
