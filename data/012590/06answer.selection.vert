<s>
Indigovník	indigovník	k1gInSc4	indigovník
pravý	pravý	k2eAgInSc4d1	pravý
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
pro	pro	k7c4	pro
modré	modrý	k2eAgNnSc4d1	modré
barvivo	barvivo	k1gNnSc4	barvivo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejkvalitnějších	kvalitní	k2eAgNnPc2d3	nejkvalitnější
<g/>
.	.	kIx.	.
</s>
