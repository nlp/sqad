<p>
<s>
Jeanette	Jeanette	k5eAaPmIp2nP	Jeanette
Thurberová	Thurberová	k1gFnSc1	Thurberová
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1850	[number]	k4	1850
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
ředitelka	ředitelka	k1gFnSc1	ředitelka
newyorské	newyorský	k2eAgFnSc2d1	newyorská
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgInSc7	první
z	z	k7c2	z
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
nutnosti	nutnost	k1gFnSc3	nutnost
pokračovat	pokračovat	k5eAaImF	pokračovat
též	též	k9	též
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
tradici	tradice	k1gFnSc6	tradice
starého	starý	k2eAgInSc2d1	starý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
evropské	evropský	k2eAgFnPc1d1	Evropská
hudby	hudba	k1gFnPc1	hudba
a	a	k8xC	a
nezbytnosti	nezbytnost	k1gFnPc1	nezbytnost
importu	import	k1gInSc2	import
této	tento	k3xDgFnSc2	tento
tradice	tradice	k1gFnSc2	tradice
i	i	k8xC	i
jejích	její	k3xOp3gFnPc2	její
nových	nový	k2eAgFnPc2d1	nová
odnoží	odnož	k1gFnPc2	odnož
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jeanette	Jeanette	k5eAaPmIp2nP	Jeanette
Thurberová	Thurberová	k1gFnSc1	Thurberová
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
hudební	hudební	k2eAgNnSc4d1	hudební
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
založila	založit	k5eAaPmAgFnS	založit
"	"	kIx"	"
<g/>
Americkou	americký	k2eAgFnSc4d1	americká
operní	operní	k2eAgFnSc4d1	operní
společnost	společnost	k1gFnSc4	společnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Národní	národní	k2eAgFnSc4d1	národní
hudební	hudební	k2eAgFnSc4d1	hudební
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
se	se	k3xPyFc4	se
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
místo	místo	k1gNnSc4	místo
ředitele	ředitel	k1gMnSc2	ředitel
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
českému	český	k2eAgMnSc3d1	český
skladateli	skladatel	k1gMnSc3	skladatel
Antonínu	Antonín	k1gMnSc3	Antonín
Dvořákovi	Dvořák	k1gMnSc3	Dvořák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
stal	stát	k5eAaPmAgMnS	stát
od	od	k7c2	od
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
zásadní	zásadní	k2eAgInSc1d1	zásadní
krok	krok	k1gInSc1	krok
k	k	k7c3	k
výchově	výchova	k1gFnSc3	výchova
vlastních	vlastní	k2eAgMnPc2d1	vlastní
skladatelů	skladatel	k1gMnPc2	skladatel
a	a	k8xC	a
založení	založení	k1gNnSc2	založení
tradice	tradice	k1gFnSc2	tradice
americké	americký	k2eAgFnSc2d1	americká
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
v	v	k7c6	v
Bronxville	Bronxvilla	k1gFnSc6	Bronxvilla
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jeanette	Jeanett	k1gInSc5	Jeanett
Thurber	Thurber	k1gInSc4	Thurber
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Money	Mone	k2eAgInPc1d1	Mone
<g/>
:	:	kIx,	:
New	New	k1gFnSc1	New
Music	Music	k1gMnSc1	Music
Funding	Funding	k1gInSc1	Funding
in	in	k?	in
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
an	an	k?	an
excerpt	excerpt	k1gInSc1	excerpt
concerning	concerning	k1gInSc1	concerning
Jeanette	Jeanett	k1gInSc5	Jeanett
M.	M.	kA	M.
Thurber	Thurber	k1gMnSc1	Thurber
</s>
</p>
