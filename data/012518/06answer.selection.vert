<s>
Alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
alkohol	alkohol	k1gInSc4	alkohol
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nápoje	nápoj	k1gInPc4	nápoj
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
přes	přes	k7c4	přes
0,75	[number]	k4	0,75
<g/>
%	%	kIx~	%
objemových	objemový	k2eAgNnPc2d1	objemové
procent	procento	k1gNnPc2	procento
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
