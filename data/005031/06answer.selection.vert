<s>
Naše	náš	k3xOp1gFnSc1	náš
Galaxie	galaxie	k1gFnSc1	galaxie
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Místní	místní	k2eAgFnSc2d1	místní
skupiny	skupina	k1gFnSc2	skupina
galaxií	galaxie	k1gFnPc2	galaxie
společně	společně	k6eAd1	společně
s	s	k7c7	s
galaxií	galaxie	k1gFnSc7	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
největší	veliký	k2eAgFnSc6d3	veliký
<g/>
;	;	kIx,	;
celkově	celkově	k6eAd1	celkově
naše	náš	k3xOp1gFnSc1	náš
místní	místní	k2eAgFnSc1d1	místní
skupina	skupina	k1gFnSc1	skupina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
30	[number]	k4	30
galaxií	galaxie	k1gFnPc2	galaxie
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
1	[number]	k4	1
megaparseku	megaparsek	k1gInSc2	megaparsek
<g/>
.	.	kIx.	.
</s>
