<p>
<s>
Samsu-Ditana	Samsu-Ditan	k1gMnSc4	Samsu-Ditan
byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
babylonským	babylonský	k2eAgMnSc7d1	babylonský
králem	král	k1gMnSc7	král
z	z	k7c2	z
amoritské	amoritský	k2eAgFnSc2d1	amoritský
dynastie	dynastie	k1gFnSc2	dynastie
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
babylonská	babylonský	k2eAgFnSc1d1	Babylonská
dynastie	dynastie	k1gFnSc1	dynastie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
letech	let	k1gInPc6	let
1625	[number]	k4	1625
<g/>
–	–	k?	–
<g/>
1595	[number]	k4	1595
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
době	doba	k1gFnSc3	doba
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
za	za	k7c4	za
Chammurapiho	Chammurapi	k1gMnSc4	Chammurapi
zabírala	zabírat	k5eAaImAgFnS	zabírat
babylonská	babylonský	k2eAgFnSc1d1	Babylonská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
asi	asi	k9	asi
o	o	k7c4	o
150	[number]	k4	150
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
v	v	k7c6	v
čase	čas	k1gInSc6	čas
Samsu-Ditany	Samsu-Ditan	k1gInPc1	Samsu-Ditan
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc4d2	menší
rozlohu	rozloha	k1gFnSc4	rozloha
<g/>
:	:	kIx,	:
stále	stále	k6eAd1	stále
však	však	k9	však
sahala	sahat	k5eAaImAgFnS	sahat
od	od	k7c2	od
Babylónu	babylón	k1gInSc2	babylón
a	a	k8xC	a
Eufratu	Eufrat	k1gInSc2	Eufrat
po	po	k7c6	po
Mari	Mar	k1gInSc6	Mar
a	a	k8xC	a
Terqu	Terqus	k1gInSc6	Terqus
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
rozkladu	rozklad	k1gInSc6	rozklad
<g/>
,	,	kIx,	,
úřady	úřad	k1gInPc1	úřad
se	se	k3xPyFc4	se
stávaly	stávat	k5eAaImAgFnP	stávat
dědičnými	dědičný	k2eAgFnPc7d1	dědičná
<g/>
,	,	kIx,	,
královské	královský	k2eAgFnSc2d1	královská
výsady	výsada	k1gFnSc2	výsada
byly	být	k5eAaImAgFnP	být
omezovány	omezován	k2eAgFnPc1d1	omezována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1594	[number]	k4	1594
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
říši	říš	k1gFnSc3	říš
napadli	napadnout	k5eAaPmAgMnP	napadnout
Chetité	Chetita	k1gMnPc1	Chetita
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
krále	král	k1gMnSc2	král
Muršiliše	Muršiliš	k1gMnSc2	Muršiliš
I.	I.	kA	I.
Samsu-Ditana	Samsu-Ditan	k1gMnSc2	Samsu-Ditan
byl	být	k5eAaImAgInS	být
sesazen	sesazen	k2eAgMnSc1d1	sesazen
<g/>
,	,	kIx,	,
Babylón	Babylón	k1gInSc1	Babylón
vypleněn	vyplenit	k5eAaPmNgInS	vyplenit
a	a	k8xC	a
ponechán	ponechat	k5eAaPmNgInS	ponechat
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
.	.	kIx.	.
</s>
<s>
Oslabená	oslabený	k2eAgFnSc1d1	oslabená
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stala	stát	k5eAaPmAgFnS	stát
snadnou	snadný	k2eAgFnSc7d1	snadná
kořistí	kořist	k1gFnSc7	kořist
pro	pro	k7c4	pro
Kassity	Kassit	k1gInPc4	Kassit
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
Babylón	Babylón	k1gInSc4	Babylón
obsadili	obsadit	k5eAaPmAgMnP	obsadit
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
novou	nový	k2eAgFnSc4d1	nová
vládnoucí	vládnoucí	k2eAgFnSc4d1	vládnoucí
dynastii	dynastie	k1gFnSc4	dynastie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Samsu-Ditana	Samsu-Ditan	k1gMnSc2	Samsu-Ditan
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
