<s>
Javory	javor	k1gInPc1	javor
v	v	k7c6	v
Arnoltově	Arnoltův	k2eAgInSc6d1	Arnoltův
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
památné	památný	k2eAgInPc1d1	památný
stromy	strom	k1gInPc1	strom
javory	javor	k1gInPc1	javor
kleny	klen	k2eAgInPc1d1	klen
(	(	kIx(	(
<g/>
Acer	Acer	k1gInSc1	Acer
pseudoplatanus	pseudoplatanus	k1gInSc1	pseudoplatanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
rostou	růst	k5eAaImIp3nP	růst
za	za	k7c7	za
Arnoltovem	Arnoltovo	k1gNnSc7	Arnoltovo
asi	asi	k9	asi
450	[number]	k4	450
m	m	kA	m
sv.	sv.	kA	sv.
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
CHKO	CHKO	kA	CHKO
Slavkovský	slavkovský	k2eAgInSc4d1	slavkovský
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
