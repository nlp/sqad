<p>
<s>
Javory	javor	k1gInPc1	javor
v	v	k7c6	v
Arnoltově	Arnoltův	k2eAgInSc6d1	Arnoltův
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
památné	památný	k2eAgInPc1d1	památný
stromy	strom	k1gInPc1	strom
javory	javor	k1gInPc1	javor
kleny	klen	k2eAgInPc1d1	klen
(	(	kIx(	(
<g/>
Acer	Acer	k1gInSc1	Acer
pseudoplatanus	pseudoplatanus	k1gInSc1	pseudoplatanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
rostou	růst	k5eAaImIp3nP	růst
za	za	k7c7	za
Arnoltovem	Arnoltovo	k1gNnSc7	Arnoltovo
asi	asi	k9	asi
450	[number]	k4	450
m	m	kA	m
sv.	sv.	kA	sv.
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
CHKO	CHKO	kA	CHKO
Slavkovský	slavkovský	k2eAgInSc4d1	slavkovský
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Koruny	koruna	k1gFnPc1	koruna
stromů	strom	k1gInPc2	strom
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
21	[number]	k4	21
m	m	kA	m
a	a	k8xC	a
18,5	[number]	k4	18,5
m	m	kA	m
<g/>
,	,	kIx,	,
obvody	obvod	k1gInPc1	obvod
kmenů	kmen	k1gInPc2	kmen
měří	měřit	k5eAaImIp3nP	měřit
292	[number]	k4	292
cm	cm	kA	cm
a	a	k8xC	a
420	[number]	k4	420
cm	cm	kA	cm
(	(	kIx(	(
<g/>
měření	měření	k1gNnSc4	měření
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
javory	javor	k1gInPc1	javor
jsou	být	k5eAaImIp3nP	být
chráněny	chránit	k5eAaImNgInP	chránit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
jako	jako	k8xS	jako
biologicky	biologicky	k6eAd1	biologicky
významné	významný	k2eAgInPc1d1	významný
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stromy	strom	k1gInPc7	strom
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
==	==	k?	==
</s>
</p>
<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Arnoltově	Arnoltův	k2eAgFnSc6d1	Arnoltova
</s>
</p>
<p>
<s>
Bambasův	Bambasův	k2eAgInSc1d1	Bambasův
dub	dub	k1gInSc1	dub
</s>
</p>
<p>
<s>
Kleny	klen	k1gInPc1	klen
v	v	k7c6	v
Kostelní	kostelní	k2eAgFnSc6d1	kostelní
Bříze	bříza	k1gFnSc6	bříza
</s>
</p>
<p>
<s>
Lípy	lípa	k1gFnPc1	lípa
u	u	k7c2	u
Vondrů	Vondra	k1gMnPc2	Vondra
</s>
</p>
<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
(	(	kIx(	(
<g/>
Kostelní	kostelní	k2eAgFnSc1d1	kostelní
Bříza	bříza	k1gFnSc1	bříza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Kostelní	kostelní	k2eAgFnSc6d1	kostelní
Bříze	bříza	k1gFnSc6	bříza
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Javory	javor	k1gInPc4	javor
v	v	k7c6	v
Arnoltově	Arnoltův	k2eAgMnSc6d1	Arnoltův
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
