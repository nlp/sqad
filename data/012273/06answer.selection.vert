<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Sudoměře	Sudoměř	k1gFnSc2	Sudoměř
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1420	[number]	k4	1420
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
mezi	mezi	k7c7	mezi
rybníky	rybník	k1gInPc7	rybník
Markovec	Markovec	k1gMnSc1	Markovec
a	a	k8xC	a
Škaredý	škaredý	k2eAgMnSc1d1	škaredý
<g/>
,	,	kIx,	,
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
usedlosti	usedlost	k1gFnSc2	usedlost
Sudoměř	Sudoměř	k1gFnSc1	Sudoměř
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgNnSc1d1	vzdálené
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
km	km	kA	km
od	od	k7c2	od
měst	město	k1gNnPc2	město
Strakonice	Strakonice	k1gFnPc1	Strakonice
a	a	k8xC	a
Písek	Písek	k1gInSc1	Písek
<g/>
.	.	kIx.	.
</s>
