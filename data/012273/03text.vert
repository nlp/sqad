<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Sudoměře	Sudoměř	k1gFnSc2	Sudoměř
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1420	[number]	k4	1420
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
mezi	mezi	k7c7	mezi
rybníky	rybník	k1gInPc7	rybník
Markovec	Markovec	k1gMnSc1	Markovec
a	a	k8xC	a
Škaredý	škaredý	k2eAgMnSc1d1	škaredý
<g/>
,	,	kIx,	,
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
usedlosti	usedlost	k1gFnSc2	usedlost
Sudoměř	Sudoměř	k1gFnSc1	Sudoměř
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgNnSc1d1	vzdálené
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
km	km	kA	km
od	od	k7c2	od
měst	město	k1gNnPc2	město
Strakonice	Strakonice	k1gFnPc1	Strakonice
a	a	k8xC	a
Písek	Písek	k1gInSc1	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Husitský	husitský	k2eAgInSc1d1	husitský
oddíl	oddíl	k1gInSc1	oddíl
<g/>
,	,	kIx,	,
přemisťující	přemisťující	k2eAgFnSc2d1	přemisťující
se	se	k3xPyFc4	se
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
vznikajícímu	vznikající	k2eAgNnSc3d1	vznikající
Hradišti	Hradiště	k1gNnSc3	Hradiště
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Tábor	Tábor	k1gInSc4	Tábor
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
porazil	porazit	k5eAaPmAgMnS	porazit
početnější	početní	k2eAgMnSc1d2	početnější
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
vyzbrojené	vyzbrojený	k2eAgNnSc1d1	vyzbrojené
vojsko	vojsko	k1gNnSc1	vojsko
strakonických	strakonický	k2eAgMnPc2d1	strakonický
johanitů	johanita	k1gMnPc2	johanita
a	a	k8xC	a
katolických	katolický	k2eAgMnPc2d1	katolický
šlechticů	šlechtic	k1gMnPc2	šlechtic
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
landfrýdu	landfrýd	k1gInSc2	landfrýd
včetně	včetně	k7c2	včetně
oddílu	oddíl	k1gInSc2	oddíl
rytířů	rytíř	k1gMnPc2	rytíř
přezdívaných	přezdívaný	k2eAgMnPc2d1	přezdívaný
"	"	kIx"	"
<g/>
Železní	železný	k2eAgMnPc1d1	železný
páni	pan	k1gMnPc1	pan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Střetnutí	střetnutí	k1gNnSc1	střetnutí
obou	dva	k4xCgMnPc2	dva
protivníků	protivník	k1gMnPc2	protivník
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgFnSc7	první
větší	veliký	k2eAgFnSc7d2	veliký
bitvou	bitva	k1gFnSc7	bitva
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
předznamenalo	předznamenat	k5eAaPmAgNnS	předznamenat
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vyvíjelo	vyvíjet	k5eAaImAgNnS	vyvíjet
husitské	husitský	k2eAgNnSc1d1	husitské
válečnictví	válečnictví	k1gNnSc1	válečnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předehra	předehra	k1gFnSc1	předehra
==	==	k?	==
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1419	[number]	k4	1419
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
na	na	k7c6	na
téměř	téměř	k6eAd1	téměř
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
boje	boj	k1gInPc4	boj
kališnické	kališnický	k2eAgFnSc2d1	kališnická
a	a	k8xC	a
katolické	katolický	k2eAgFnSc2d1	katolická
strany	strana	k1gFnSc2	strana
uvnitř	uvnitř	k6eAd1	uvnitř
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
české	český	k2eAgFnSc2d1	Česká
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
venkovských	venkovský	k2eAgInPc2d1	venkovský
radikálů	radikál	k1gInPc2	radikál
nespokojených	spokojený	k2eNgInPc2d1	nespokojený
s	s	k7c7	s
kolísavou	kolísavý	k2eAgFnSc7d1	kolísavá
politikou	politika	k1gFnSc7	politika
pražské	pražský	k2eAgFnSc2d1	Pražská
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Břeňkem	Břeněk	k1gInSc7	Břeněk
Švihovským	Švihovský	k2eAgInSc7d1	Švihovský
z	z	k7c2	z
Dolan	Dolany	k1gInPc2	Dolany
<g/>
,	,	kIx,	,
bratry	bratr	k1gMnPc7	bratr
Chvalem	Chval	k1gMnSc7	Chval
a	a	k8xC	a
Kunešem	Kuneš	k1gMnSc7	Kuneš
z	z	k7c2	z
Machovic	Machovice	k1gFnPc2	Machovice
<g/>
,	,	kIx,	,
Valkounem	Valkoun	k1gInSc7	Valkoun
z	z	k7c2	z
Adlaru	Adlar	k1gInSc2	Adlar
a	a	k8xC	a
Janem	Jan	k1gMnSc7	Jan
Žižkou	Žižka	k1gMnSc7	Žižka
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
tuto	tento	k3xDgFnSc4	tento
smlouvu	smlouva	k1gFnSc4	smlouva
akceptovat	akceptovat	k5eAaBmF	akceptovat
a	a	k8xC	a
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
působiště	působiště	k1gNnSc2	působiště
kněze	kněz	k1gMnSc2	kněz
Václava	Václav	k1gMnSc2	Václav
Korandy	Koranda	k1gFnSc2	Koranda
<g/>
,	,	kIx,	,
centra	centrum	k1gNnSc2	centrum
husitského	husitský	k2eAgNnSc2d1	husitské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
posléze	posléze	k6eAd1	posléze
vedli	vést	k5eAaImAgMnP	vést
poměrně	poměrně	k6eAd1	poměrně
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
oddíly	oddíl	k1gInPc1	oddíl
místních	místní	k2eAgMnPc2d1	místní
feudálů	feudál	k1gMnPc2	feudál
organizovaných	organizovaný	k2eAgMnPc2d1	organizovaný
do	do	k7c2	do
krajského	krajský	k2eAgInSc2d1	krajský
landfrýdu	landfrýd	k1gInSc2	landfrýd
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
podařilo	podařit	k5eAaPmAgNnS	podařit
porazit	porazit	k5eAaPmF	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Nekmíře	Nekmíř	k1gInSc2	Nekmíř
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bitvě	bitva	k1gFnSc6	bitva
husité	husita	k1gMnPc1	husita
uplatnili	uplatnit	k5eAaPmAgMnP	uplatnit
defenzivní	defenzivní	k2eAgFnSc4d1	defenzivní
taktiku	taktika	k1gFnSc4	taktika
opírající	opírající	k2eAgFnSc4d1	opírající
se	se	k3xPyFc4	se
o	o	k7c4	o
vozovou	vozový	k2eAgFnSc4d1	vozová
hradbu	hradba	k1gFnSc4	hradba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1420	[number]	k4	1420
muže	muž	k1gMnPc4	muž
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
landfrýdu	landfrýd	k1gInSc2	landfrýd
posílily	posílit	k5eAaPmAgInP	posílit
oddíly	oddíl	k1gInPc1	oddíl
Václava	Václav	k1gMnSc2	Václav
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
,	,	kIx,	,
vojenského	vojenský	k2eAgMnSc2d1	vojenský
náměstka	náměstek	k1gMnSc2	náměstek
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
královská	královský	k2eAgNnPc1d1	královské
vojska	vojsko	k1gNnPc1	vojsko
posléze	posléze	k6eAd1	posléze
Plzeň	Plzeň	k1gFnSc1	Plzeň
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
dalších	další	k2eAgInPc2d1	další
měsíců	měsíc	k1gInPc2	měsíc
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
krvavým	krvavý	k2eAgFnPc3d1	krvavá
srážkám	srážka	k1gFnPc3	srážka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nepřinesly	přinést	k5eNaPmAgFnP	přinést
výhodu	výhoda	k1gFnSc4	výhoda
ani	ani	k8xC	ani
jedné	jeden	k4xCgFnSc3	jeden
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
<g/>
Někdy	někdy	k6eAd1	někdy
před	před	k7c7	před
polovinou	polovina	k1gFnSc7	polovina
března	březen	k1gInSc2	březen
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
písemná	písemný	k2eAgFnSc1d1	písemná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
nové	nový	k2eAgFnSc2d1	nová
vzorné	vzorný	k2eAgFnSc2d1	vzorná
husitské	husitský	k2eAgFnSc2d1	husitská
obce	obec	k1gFnSc2	obec
poblíž	poblíž	k7c2	poblíž
Sezimova	Sezimův	k2eAgNnSc2d1	Sezimovo
Ústí	ústí	k1gNnSc2	ústí
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
s	s	k7c7	s
biblickým	biblický	k2eAgNnSc7d1	biblické
jménem	jméno	k1gNnSc7	jméno
Tábor	tábor	k1gMnSc1	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Kněz	kněz	k1gMnSc1	kněz
Hromádka	Hromádka	k1gMnSc1	Hromádka
v	v	k7c6	v
listu	list	k1gInSc6	list
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
žádal	žádat	k5eAaImAgMnS	žádat
Jana	Jan	k1gMnSc4	Jan
Žižku	Žižka	k1gMnSc4	Žižka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
vznikající	vznikající	k2eAgFnSc2d1	vznikající
komuny	komuna	k1gFnSc2	komuna
vyslal	vyslat	k5eAaPmAgMnS	vyslat
část	část	k1gFnSc4	část
svých	svůj	k3xOyFgMnPc2	svůj
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
svízelnou	svízelný	k2eAgFnSc4d1	svízelná
situaci	situace	k1gFnSc4	situace
uvnitř	uvnitř	k7c2	uvnitř
hradeb	hradba	k1gFnPc2	hradba
vypravil	vypravit	k5eAaPmAgInS	vypravit
pomocný	pomocný	k2eAgInSc1d1	pomocný
odřad	odřad	k1gInSc1	odřad
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
dalších	další	k2eAgInPc2d1	další
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
odchod	odchod	k1gInSc1	odchod
však	však	k9	však
znamenal	znamenat	k5eAaImAgInS	znamenat
výrazné	výrazný	k2eAgNnSc4d1	výrazné
oslabení	oslabení	k1gNnSc4	oslabení
pro	pro	k7c4	pro
husity	husita	k1gMnPc4	husita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
setrvali	setrvat	k5eAaPmAgMnP	setrvat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
března	březen	k1gInSc2	březen
již	již	k6eAd1	již
město	město	k1gNnSc1	město
svíralo	svírat	k5eAaImAgNnS	svírat
několik	několik	k4yIc4	několik
narychlo	narychlo	k6eAd1	narychlo
vybudovaných	vybudovaný	k2eAgFnPc2d1	vybudovaná
bašt	bašta	k1gFnPc2	bašta
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
příslušníci	příslušník	k1gMnPc1	příslušník
místní	místní	k2eAgFnSc2d1	místní
katolické	katolický	k2eAgFnSc2d1	katolická
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
opozice	opozice	k1gFnSc2	opozice
využili	využít	k5eAaPmAgMnP	využít
příznivé	příznivý	k2eAgFnPc4d1	příznivá
situace	situace	k1gFnPc4	situace
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
opevňovat	opevňovat	k5eAaImF	opevňovat
své	svůj	k3xOyFgInPc4	svůj
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc1d1	vnější
i	i	k8xC	i
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
tlak	tlak	k1gInSc1	tlak
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
obleženou	obležený	k2eAgFnSc4d1	obležená
posádku	posádka	k1gFnSc4	posádka
kritický	kritický	k2eAgMnSc1d1	kritický
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
předáci	předák	k1gMnPc1	předák
proto	proto	k8xC	proto
přistoupili	přistoupit	k5eAaPmAgMnP	přistoupit
na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
Pražanů	Pražan	k1gMnPc2	Pražan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jako	jako	k9	jako
prostředníci	prostředník	k1gMnPc1	prostředník
sjednali	sjednat	k5eAaPmAgMnP	sjednat
s	s	k7c7	s
královskou	královský	k2eAgFnSc7d1	královská
stranou	strana	k1gFnSc7	strana
podmínky	podmínka	k1gFnSc2	podmínka
jejich	jejich	k3xOp3gInSc2	jejich
svobodného	svobodný	k2eAgInSc2d1	svobodný
odchodu	odchod	k1gInSc2	odchod
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
Starých	Starých	k2eAgInPc2d1	Starých
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgInPc2d1	český
čítala	čítat	k5eAaImAgFnS	čítat
400	[number]	k4	400
pěších	pěší	k1gMnPc2	pěší
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
12	[number]	k4	12
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
9	[number]	k4	9
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
patrně	patrně	k6eAd1	patrně
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
husitům	husita	k1gMnPc3	husita
královská	královský	k2eAgFnSc1d1	královská
strana	strana	k1gFnSc1	strana
zaručila	zaručit	k5eAaPmAgFnS	zaručit
podmínky	podmínka	k1gFnPc4	podmínka
kapitulace	kapitulace	k1gFnSc2	kapitulace
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
dodržena	dodržet	k5eAaPmNgFnS	dodržet
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
trase	trasa	k1gFnSc6	trasa
jejich	jejich	k3xOp3gInSc2	jejich
pochodu	pochod	k1gInSc2	pochod
byl	být	k5eAaImAgMnS	být
informován	informován	k2eAgMnSc1d1	informován
mistr	mistr	k1gMnSc1	mistr
převorství	převorství	k1gNnSc2	převorství
strakonických	strakonický	k2eAgMnPc2d1	strakonický
johanitů	johanita	k1gMnPc2	johanita
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
a	a	k8xC	a
oddíly	oddíl	k1gInPc1	oddíl
železných	železný	k2eAgMnPc2d1	železný
pánů	pan	k1gMnPc2	pan
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kutnohorského	kutnohorský	k2eAgMnSc2d1	kutnohorský
mincmistra	mincmistr	k1gMnSc2	mincmistr
Mikeše	Mikeš	k1gMnSc2	Mikeš
Divůčeka	Divůčeek	k1gMnSc2	Divůčeek
z	z	k7c2	z
Jemniště	Jemniště	k1gNnSc2	Jemniště
a	a	k8xC	a
Petra	Petr	k1gMnSc4	Petr
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
operovaly	operovat	k5eAaImAgInP	operovat
v	v	k7c6	v
Píseckém	písecký	k2eAgInSc6d1	písecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Předáci	předák	k1gMnPc1	předák
husitů	husita	k1gMnPc2	husita
Valkoun	Valkoun	k1gInSc1	Valkoun
z	z	k7c2	z
Adlaru	Adlar	k1gInSc2	Adlar
a	a	k8xC	a
Břeněk	Břeněk	k1gInSc4	Břeněk
Švihovský	Švihovský	k2eAgInSc4d1	Švihovský
z	z	k7c2	z
Dolan	Dolany	k1gInPc2	Dolany
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
dobře	dobře	k6eAd1	dobře
vyznali	vyznat	k5eAaBmAgMnP	vyznat
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
vedli	vést	k5eAaImAgMnP	vést
výpravu	výprava	k1gFnSc4	výprava
přes	přes	k7c4	přes
Kasejovice	Kasejovice	k1gFnPc4	Kasejovice
<g/>
,	,	kIx,	,
Řepici	řepice	k1gFnSc4	řepice
a	a	k8xC	a
Štěkeň	Štěkeň	k1gFnSc4	Štěkeň
k	k	k7c3	k
Sudoměři	Sudoměř	k1gFnSc3	Sudoměř
<g/>
,	,	kIx,	,
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
pokračovat	pokračovat	k5eAaImF	pokračovat
k	k	k7c3	k
Táboru	Tábor	k1gInSc3	Tábor
přes	přes	k7c4	přes
Týn	Týn	k1gInSc4	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Obezřetně	obezřetně	k6eAd1	obezřetně
se	se	k3xPyFc4	se
vyhýbali	vyhýbat	k5eAaImAgMnP	vyhýbat
místům	místo	k1gNnPc3	místo
s	s	k7c7	s
nepřátelskými	přátelský	k2eNgFnPc7d1	nepřátelská
posádkami	posádka	k1gFnPc7	posádka
a	a	k8xC	a
využívali	využívat	k5eAaPmAgMnP	využívat
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohli	moct	k5eAaImAgMnP	moct
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
místní	místní	k2eAgFnSc2d1	místní
drobné	drobný	k2eAgFnSc2d1	drobná
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
však	však	k9	však
byli	být	k5eAaImAgMnP	být
sledováni	sledován	k2eAgMnPc1d1	sledován
příslušníky	příslušník	k1gMnPc7	příslušník
landfrýdu	landfrýd	k1gInSc3	landfrýd
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
s	s	k7c7	s
plzeňskými	plzeňský	k2eAgMnPc7d1	plzeňský
šlechtici	šlechtic	k1gMnPc7	šlechtic
spojil	spojit	k5eAaPmAgMnS	spojit
oddíl	oddíl	k1gInSc4	oddíl
johanitů	johanita	k1gMnPc2	johanita
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
řádového	řádový	k2eAgMnSc2d1	řádový
mistra	mistr	k1gMnSc2	mistr
od	od	k7c2	od
samostatného	samostatný	k2eAgInSc2d1	samostatný
útoku	útok	k1gInSc2	útok
odradila	odradit	k5eAaPmAgFnS	odradit
pouze	pouze	k6eAd1	pouze
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
táhnou	táhnout	k5eAaImIp3nP	táhnout
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
kutnohorský	kutnohorský	k2eAgMnSc1d1	kutnohorský
mincmistr	mincmistr	k1gMnSc1	mincmistr
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
nedlouho	nedlouho	k1gNnSc1	nedlouho
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
Písek	Písek	k1gInSc4	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Shromážděné	shromážděný	k2eAgNnSc1d1	shromážděné
vojsko	vojsko	k1gNnSc1	vojsko
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
na	na	k7c4	na
husity	husita	k1gMnPc4	husita
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
poradě	porada	k1gFnSc6	porada
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bitva	bitva	k1gFnSc1	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zprávy	zpráva	k1gFnPc1	zpráva
současníků	současník	k1gMnPc2	současník
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
událostí	událost	k1gFnPc2	událost
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1420	[number]	k4	1420
existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgMnPc1	čtyři
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
podrobné	podrobný	k2eAgInPc4d1	podrobný
písemné	písemný	k2eAgInPc4d1	písemný
prameny	pramen	k1gInPc4	pramen
různé	různý	k2eAgFnSc2d1	různá
výpovědní	výpovědní	k2eAgFnSc2d1	výpovědní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
je	být	k5eAaImIp3nS	být
svědectví	svědectví	k1gNnSc4	svědectví
Husitské	husitský	k2eAgFnSc2d1	husitská
kroniky	kronika	k1gFnSc2	kronika
literáta	literát	k1gMnSc2	literát
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
přímým	přímý	k2eAgMnSc7d1	přímý
účastníkem	účastník	k1gMnSc7	účastník
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
zprávy	zpráva	k1gFnPc1	zpráva
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
přijímány	přijímat	k5eAaImNgInP	přijímat
jako	jako	k9	jako
spolehlivé	spolehlivý	k2eAgInPc1d1	spolehlivý
a	a	k8xC	a
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
textu	text	k1gInSc2	text
mnozí	mnohý	k2eAgMnPc1d1	mnohý
historici	historik	k1gMnPc1	historik
zastávají	zastávat	k5eAaImIp3nP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
kolony	kolona	k1gFnSc2	kolona
husitů	husita	k1gMnPc2	husita
tehdy	tehdy	k6eAd1	tehdy
nebyl	být	k5eNaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Břeněk	Břeněk	k1gInSc1	Břeněk
Švihovský	Švihovský	k2eAgInSc1d1	Švihovský
z	z	k7c2	z
Dolan	Dolany	k1gInPc2	Dolany
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
byl	být	k5eAaImAgMnS	být
patrně	patrně	k6eAd1	patrně
hodnostně	hodnostně	k6eAd1	hodnostně
vedle	vedle	k7c2	vedle
něho	on	k3xPp3gMnSc2	on
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
takticko	takticko	k6eAd1	takticko
<g/>
–	–	k?	–
<g/>
operační	operační	k2eAgFnSc2d1	operační
záležitosti	záležitost	k1gFnSc2	záležitost
spjaté	spjatý	k2eAgFnSc2d1	spjatá
s	s	k7c7	s
bitvou	bitva	k1gFnSc7	bitva
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
vozová	vozový	k2eAgFnSc1d1	vozová
hradba	hradba	k1gFnSc1	hradba
<g/>
)	)	kIx)	)
a	a	k8xC	a
pochodem	pochod	k1gInSc7	pochod
kolony	kolona	k1gFnSc2	kolona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
písemným	písemný	k2eAgInSc7d1	písemný
pramenem	pramen	k1gInSc7	pramen
je	být	k5eAaImIp3nS	být
obsáhlejší	obsáhlý	k2eAgInSc1d2	obsáhlejší
popis	popis	k1gInSc1	popis
bitvy	bitva	k1gFnSc2	bitva
z	z	k7c2	z
rukopisu	rukopis	k1gInSc2	rukopis
Š	Š	kA	Š
Starých	Starých	k2eAgInPc2d1	Starých
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgInPc2d1	český
pocházejícího	pocházející	k2eAgNnSc2d1	pocházející
z	z	k7c2	z
období	období	k1gNnSc2	období
panování	panování	k1gNnSc2	panování
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
byl	být	k5eAaImAgMnS	být
patrně	patrně	k6eAd1	patrně
velkým	velký	k2eAgMnSc7d1	velký
Žižkovým	Žižkův	k2eAgMnSc7d1	Žižkův
ctitelem	ctitel	k1gMnSc7	ctitel
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
vítězství	vítězství	k1gNnSc1	vítězství
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
vybojováno	vybojován	k2eAgNnSc1d1	vybojováno
pouze	pouze	k6eAd1	pouze
Žižkovou	Žižkův	k2eAgFnSc7d1	Žižkova
zásluhou	zásluha	k1gFnSc7	zásluha
a	a	k8xC	a
Břeněk	Břeněk	k1gInSc1	Břeněk
Švihovský	Švihovský	k2eAgInSc1d1	Švihovský
z	z	k7c2	z
Dolan	Dolany	k1gInPc2	Dolany
byl	být	k5eAaImAgInS	být
pasován	pasovat	k5eAaBmNgInS	pasovat
do	do	k7c2	do
role	role	k1gFnSc2	role
jeho	jeho	k3xOp3gMnSc2	jeho
pomocníka	pomocník	k1gMnSc2	pomocník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pramen	pramen	k1gInSc1	pramen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
jako	jako	k9	jako
jediný	jediný	k2eAgInSc4d1	jediný
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
početní	početní	k2eAgInSc4d1	početní
stav	stav	k1gInSc4	stav
katolických	katolický	k2eAgMnPc2d1	katolický
spojenců	spojenec	k1gMnPc2	spojenec
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Kronika	kronika	k1gFnSc1	kronika
starého	starý	k1gMnSc2	starý
kolegiáta	kolegiát	k1gMnSc2	kolegiát
pražského	pražský	k2eAgMnSc2d1	pražský
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgInPc6	dva
tisících	tisíc	k4xCgInPc6	tisíc
jezdcích	jezdec	k1gInPc6	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
kroniky	kronika	k1gFnSc2	kronika
také	také	k9	také
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitvy	bitva	k1gFnSc2	bitva
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
významným	významný	k2eAgInSc7d1	významný
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
text	text	k1gInSc1	text
literární	literární	k2eAgFnSc2d1	literární
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
názvem	název	k1gInSc7	název
Historie	historie	k1gFnSc1	historie
česká	český	k2eAgFnSc1d1	Česká
od	od	k7c2	od
italského	italský	k2eAgMnSc2d1	italský
humanisty	humanista	k1gMnSc2	humanista
Enea	Enea	k1gMnSc1	Enea
Silvia	Silvia	k1gFnSc1	Silvia
Piccolominiho	Piccolomini	k1gMnSc2	Piccolomini
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
papeže	papež	k1gMnSc2	papež
Pia	Pius	k1gMnSc2	Pius
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
středověký	středověký	k2eAgMnSc1d1	středověký
historik	historik	k1gMnSc1	historik
neznal	znát	k5eNaImAgMnS	znát
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
vešlo	vejít	k5eAaPmAgNnS	vejít
jeho	jeho	k3xOp3gNnSc1	jeho
krátké	krátký	k2eAgNnSc1d1	krátké
podání	podání	k1gNnSc1	podání
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
pověstí	pověst	k1gFnPc2	pověst
a	a	k8xC	a
opírali	opírat	k5eAaImAgMnP	opírat
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
čeští	český	k2eAgMnPc1d1	český
odborníci	odborník	k1gMnPc1	odborník
působící	působící	k2eAgMnPc1d1	působící
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Václav	Václava	k1gFnPc2	Václava
Vladivoj	Vladivoj	k1gInSc1	Vladivoj
Tomek	Tomek	k1gMnSc1	Tomek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pravděpodobný	pravděpodobný	k2eAgInSc4d1	pravděpodobný
průběh	průběh	k1gInSc4	průběh
===	===	k?	===
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
husité	husita	k1gMnPc1	husita
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
o	o	k7c6	o
hrozícím	hrozící	k2eAgNnSc6d1	hrozící
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
,	,	kIx,	,
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
jim	on	k3xPp3gMnPc3	on
než	než	k8xS	než
vyhledat	vyhledat	k5eAaPmF	vyhledat
vhodné	vhodný	k2eAgNnSc4d1	vhodné
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Vozovou	vozový	k2eAgFnSc4d1	vozová
hradbu	hradba	k1gFnSc4	hradba
postavili	postavit	k5eAaPmAgMnP	postavit
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
usedlosti	usedlost	k1gFnSc2	usedlost
Sudoměř	Sudoměř	k1gFnSc4	Sudoměř
<g/>
,	,	kIx,	,
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
vyvýšenině	vyvýšenina	k1gFnSc6	vyvýšenina
za	za	k7c4	za
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
širokou	široký	k2eAgFnSc7d1	široká
šíjí	šíj	k1gFnSc7	šíj
(	(	kIx(	(
<g/>
hrází	hráz	k1gFnSc7	hráz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
rybníky	rybník	k1gInPc7	rybník
Markovec	Markovec	k1gMnSc1	Markovec
a	a	k8xC	a
Škaredý	škaredý	k2eAgMnSc1d1	škaredý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
stála	stát	k5eAaImAgFnS	stát
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
byl	být	k5eAaImAgInS	být
vypuštěný	vypuštěný	k2eAgMnSc1d1	vypuštěný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výběru	výběr	k1gInSc2	výběr
místa	místo	k1gNnSc2	místo
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
předáci	předák	k1gMnPc1	předák
husitů	husita	k1gMnPc2	husita
byli	být	k5eAaImAgMnP	být
s	s	k7c7	s
krajinou	krajina	k1gFnSc7	krajina
a	a	k8xC	a
terénem	terén	k1gInSc7	terén
kolem	kolem	k7c2	kolem
Sudoměře	Sudoměř	k1gFnSc2	Sudoměř
dobře	dobře	k6eAd1	dobře
obeznámeni	obeznámit	k5eAaPmNgMnP	obeznámit
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
stojící	stojící	k2eAgFnSc1d1	stojící
tvrz	tvrz	k1gFnSc1	tvrz
Řepice	řepice	k1gFnSc1	řepice
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
Chvala	Chval	k1gMnSc2	Chval
z	z	k7c2	z
Machovic	Machovice	k1gFnPc2	Machovice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
sem	sem	k6eAd1	sem
přivedli	přivést	k5eAaPmAgMnP	přivést
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
příslušníků	příslušník	k1gMnPc2	příslušník
vojska	vojsko	k1gNnSc2	vojsko
nebo	nebo	k8xC	nebo
místní	místní	k2eAgMnPc1d1	místní
sedláci	sedlák	k1gMnPc1	sedlák
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
výpravy	výprava	k1gFnSc2	výprava
znali	znát	k5eAaImAgMnP	znát
okolí	okolí	k1gNnSc4	okolí
z	z	k7c2	z
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
pobytu	pobyt	k1gInSc2	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
železní	železný	k2eAgMnPc1d1	železný
páni	pan	k1gMnPc1	pan
odvážili	odvážit	k5eAaPmAgMnP	odvážit
útoku	útok	k1gInSc2	útok
v	v	k7c6	v
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
nevýhodné	výhodný	k2eNgNnSc1d1	nevýhodné
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
patrně	patrně	k6eAd1	patrně
jejich	jejich	k3xOp3gFnSc1	jejich
výrazná	výrazný	k2eAgFnSc1d1	výrazná
početní	početní	k2eAgFnSc1d1	početní
převaha	převaha	k1gFnSc1	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kolika	kolik	k4yQc6	kolik
ozbrojencům	ozbrojenec	k1gMnPc3	ozbrojenec
vlastně	vlastně	k9	vlastně
husité	husita	k1gMnPc1	husita
čelili	čelit	k5eAaImAgMnP	čelit
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jich	on	k3xPp3gMnPc2	on
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
až	až	k9	až
5000	[number]	k4	5000
(	(	kIx(	(
<g/>
též	též	k9	též
Hanuš	Hanuš	k1gMnSc1	Hanuš
Kuffner	Kuffner	k1gMnSc1	Kuffner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
desetiletích	desetiletí	k1gNnPc6	desetiletí
přední	přední	k2eAgMnSc1d1	přední
historici	historik	k1gMnPc1	historik
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Pekař	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Macek	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
V.	V.	kA	V.
V.	V.	kA	V.
Tomek	Tomek	k1gMnSc1	Tomek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Durdík	Durdík	k1gMnSc1	Durdík
<g/>
)	)	kIx)	)
vycházeli	vycházet	k5eAaImAgMnP	vycházet
z	z	k7c2	z
Kroniky	kronika	k1gFnSc2	kronika
starého	starý	k2eAgMnSc2d1	starý
kolegiáta	kolegiát	k1gMnSc2	kolegiát
pražského	pražský	k2eAgMnSc2d1	pražský
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
počet	počet	k1gInSc4	počet
snížili	snížit	k5eAaPmAgMnP	snížit
na	na	k7c4	na
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Současní	současný	k2eAgMnPc1d1	současný
odborníci	odborník	k1gMnPc1	odborník
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
možnostem	možnost	k1gFnPc3	možnost
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
šlechticů	šlechtic	k1gMnPc2	šlechtic
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
700	[number]	k4	700
těžkých	těžký	k2eAgMnPc2d1	těžký
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Defenzivní	defenzivní	k2eAgNnSc4d1	defenzivní
postavení	postavení	k1gNnSc4	postavení
husitů	husita	k1gMnPc2	husita
však	však	k9	však
českým	český	k2eAgMnPc3d1	český
feudálům	feudál	k1gMnPc3	feudál
a	a	k8xC	a
johanitům	johanita	k1gMnPc3	johanita
nedovolilo	dovolit	k5eNaPmAgNnS	dovolit
uplatnit	uplatnit	k5eAaPmF	uplatnit
početní	početní	k2eAgFnSc4d1	početní
výhodu	výhoda	k1gFnSc4	výhoda
ani	ani	k8xC	ani
rozvinout	rozvinout	k5eAaPmF	rozvinout
standardní	standardní	k2eAgInSc4d1	standardní
útočný	útočný	k2eAgInSc4d1	útočný
jízdní	jízdní	k2eAgInSc4d1	jízdní
šik	šik	k1gInSc4	šik
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
první	první	k4xOgFnSc1	první
řada	řada	k1gFnSc1	řada
patrně	patrně	k6eAd1	patrně
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
vysoké	vysoký	k2eAgFnPc4d1	vysoká
ztráty	ztráta	k1gFnPc4	ztráta
po	po	k7c6	po
salvě	salva	k1gFnSc6	salva
ze	z	k7c2	z
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
nedostala	dostat	k5eNaPmAgFnS	dostat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
do	do	k7c2	do
těsné	těsný	k2eAgFnSc2d1	těsná
blízkosti	blízkost	k1gFnSc2	blízkost
vozové	vozový	k2eAgFnSc2d1	vozová
hradby	hradba	k1gFnSc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
druhý	druhý	k4xOgInSc4	druhý
sled	sled	k1gInSc4	sled
prorazil	prorazit	k5eAaPmAgMnS	prorazit
k	k	k7c3	k
vozům	vůz	k1gInPc3	vůz
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
boji	boj	k1gInSc3	boj
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
čelní	čelní	k2eAgFnSc6d1	čelní
frontě	fronta	k1gFnSc6	fronta
(	(	kIx(	(
<g/>
špici	špice	k1gFnSc6	špice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
katolických	katolický	k2eAgMnPc2d1	katolický
pánů	pan	k1gMnPc2	pan
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
útok	útok	k1gInSc4	útok
přes	přes	k7c4	přes
vypuštěné	vypuštěný	k2eAgNnSc4d1	vypuštěné
dno	dno	k1gNnSc4	dno
rybníka	rybník	k1gInSc2	rybník
Škaredý	škaredý	k2eAgMnSc1d1	škaredý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bahno	bahno	k1gNnSc1	bahno
znemožňovalo	znemožňovat	k5eAaImAgNnS	znemožňovat
chůzi	chůze	k1gFnSc4	chůze
koním	kůň	k1gMnPc3	kůň
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
sesedli	sesednout	k5eAaPmAgMnP	sesednout
ze	z	k7c2	z
sedel	sedlo	k1gNnPc2	sedlo
a	a	k8xC	a
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
těžké	těžký	k2eAgNnSc1d1	těžké
odění	odění	k1gNnSc1	odění
i	i	k8xC	i
kluzký	kluzký	k2eAgInSc1d1	kluzký
terén	terén	k1gInSc1	terén
ovšem	ovšem	k9	ovšem
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
výhodu	výhoda	k1gFnSc4	výhoda
pohyblivějším	pohyblivý	k2eAgMnPc3d2	pohyblivější
husitům	husita	k1gMnPc3	husita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
neváhali	váhat	k5eNaImAgMnP	váhat
s	s	k7c7	s
protiútokem	protiútok	k1gInSc7	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
všech	všecek	k3xTgInPc2	všecek
náznaků	náznak	k1gInPc2	náznak
boj	boj	k1gInSc1	boj
trval	trvat	k5eAaImAgInS	trvat
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zapadlo	zapadnout	k5eAaPmAgNnS	zapadnout
slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
na	na	k7c4	na
bojiště	bojiště	k1gNnSc4	bojiště
se	se	k3xPyFc4	se
snesla	snést	k5eAaPmAgFnS	snést
mlha	mlha	k1gFnSc1	mlha
<g/>
.	.	kIx.	.
</s>
<s>
Snížená	snížený	k2eAgFnSc1d1	snížená
viditelnost	viditelnost	k1gFnSc1	viditelnost
a	a	k8xC	a
vysoké	vysoký	k2eAgFnPc1d1	vysoká
ztráty	ztráta	k1gFnPc1	ztráta
patrně	patrně	k6eAd1	patrně
vnesly	vnést	k5eAaPmAgFnP	vnést
do	do	k7c2	do
řad	řada	k1gFnPc2	řada
útočníků	útočník	k1gMnPc2	útočník
zmatek	zmatek	k1gInSc4	zmatek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
úspěch	úspěch	k1gInSc1	úspěch
katolíků	katolík	k1gMnPc2	katolík
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
kutnohorský	kutnohorský	k2eAgMnSc1d1	kutnohorský
mincmistr	mincmistr	k1gMnSc1	mincmistr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
odváděl	odvádět	k5eAaImAgInS	odvádět
30	[number]	k4	30
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mu	on	k3xPp3gMnSc3	on
padli	padnout	k5eAaImAgMnP	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
při	při	k7c6	při
výpadu	výpad	k1gInSc6	výpad
z	z	k7c2	z
vozové	vozový	k2eAgFnSc2d1	vozová
hradby	hradba	k1gFnSc2	hradba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bilance	bilance	k1gFnSc1	bilance
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dosaženém	dosažený	k2eAgNnSc6d1	dosažené
vítězství	vítězství	k1gNnSc6	vítězství
se	se	k3xPyFc4	se
husité	husita	k1gMnPc1	husita
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
velení	velení	k1gNnSc1	velení
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
převzal	převzít	k5eAaPmAgMnS	převzít
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
přenocovat	přenocovat	k5eAaPmF	přenocovat
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
pohřbili	pohřbít	k5eAaPmAgMnP	pohřbít
své	svůj	k3xOyFgMnPc4	svůj
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
,	,	kIx,	,
raněné	raněný	k1gMnPc4	raněný
zanechali	zanechat	k5eAaPmAgMnP	zanechat
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
vsích	ves	k1gFnPc6	ves
a	a	k8xC	a
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
se	se	k3xPyFc4	se
k	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
Újezdu	Újezd	k1gInSc2	Újezd
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
setrvali	setrvat	k5eAaPmAgMnP	setrvat
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
o	o	k7c6	o
vítězství	vítězství	k1gNnSc6	vítězství
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
na	na	k7c6	na
Táboře	Tábor	k1gInSc6	Tábor
a	a	k8xC	a
vstříc	vstříc	k7c3	vstříc
jim	on	k3xPp3gFnPc3	on
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
menší	malý	k2eAgInSc4d2	menší
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
oddíl	oddíl	k1gInSc4	oddíl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
doprovodil	doprovodit	k5eAaPmAgMnS	doprovodit
až	až	k9	až
k	k	k7c3	k
Hradišti	Hradiště	k1gNnSc3	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Žižkovým	Žižkův	k2eAgInSc7d1	Žižkův
příchodem	příchod	k1gInSc7	příchod
na	na	k7c4	na
Tábor	Tábor	k1gInSc4	Tábor
byla	být	k5eAaImAgFnS	být
dokonána	dokonat	k5eAaPmNgFnS	dokonat
první	první	k4xOgFnSc1	první
etapa	etapa	k1gFnSc1	etapa
sjednocení	sjednocení	k1gNnSc2	sjednocení
husitských	husitský	k2eAgFnPc2d1	husitská
sil	síla	k1gFnPc2	síla
z	z	k7c2	z
jižních	jižní	k2eAgFnPc2d1	jižní
a	a	k8xC	a
západních	západní	k2eAgFnPc2d1	západní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
celkových	celkový	k2eAgFnPc6d1	celková
ztrátách	ztráta	k1gFnPc6	ztráta
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
prameny	pramen	k1gInPc1	pramen
nehovoří	hovořit	k5eNaImIp3nP	hovořit
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
však	však	k9	však
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
byly	být	k5eAaImAgFnP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Kronikáři	kronikář	k1gMnPc1	kronikář
zanechali	zanechat	k5eAaPmAgMnP	zanechat
pouze	pouze	k6eAd1	pouze
lakonickou	lakonický	k2eAgFnSc4d1	lakonická
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
kališníků	kališník	k1gMnPc2	kališník
padl	padnout	k5eAaImAgInS	padnout
Břeněk	Břeněk	k1gInSc1	Břeněk
Švihovský	Švihovský	k2eAgInSc1d1	Švihovský
z	z	k7c2	z
Dolan	Dolany	k1gInPc2	Dolany
a	a	k8xC	a
z	z	k7c2	z
královských	královský	k2eAgMnPc2d1	královský
byl	být	k5eAaImAgMnS	být
těžce	těžce	k6eAd1	těžce
raněn	raněn	k2eAgMnSc1d1	raněn
mistr	mistr	k1gMnSc1	mistr
johanitů	johanita	k1gMnPc2	johanita
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Svému	svůj	k3xOyFgNnSc3	svůj
zranění	zranění	k1gNnSc3	zranění
v	v	k7c6	v
příštích	příští	k2eAgInPc6d1	příští
měsících	měsíc	k1gInPc6	měsíc
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Prameny	pramen	k1gInPc1	pramen
===	===	k?	===
</s>
</p>
<p>
<s>
BŘEZOVÉ	březový	k2eAgNnSc4d1	Březové
<g/>
,	,	kIx,	,
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
z.	z.	k?	z.
Husitská	husitský	k2eAgFnSc1d1	husitská
kronika	kronika	k1gFnSc1	kronika
<g/>
;	;	kIx,	;
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c4	o
vítězství	vítězství	k1gNnSc4	vítězství
u	u	k7c2	u
Domažlic	Domažlice	k1gFnPc2	Domažlice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
427	[number]	k4	427
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PICCOLOMINI	PICCOLOMINI	kA	PICCOLOMINI
<g/>
,	,	kIx,	,
Enea	Enea	k1gMnSc1	Enea
Silvio	Silvio	k1gMnSc1	Silvio
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
:	:	kIx,	:
Dialog	dialog	k1gInSc1	dialog
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
160	[number]	k4	160
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7382	[number]	k4	7382
<g/>
-	-	kIx~	-
<g/>
136	[number]	k4	136
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prameny	pramen	k1gInPc1	pramen
dějin	dějiny	k1gFnPc2	dějiny
českých	český	k2eAgFnPc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Josef	Josef	k1gMnSc1	Josef
Emler	Emler	k1gMnSc1	Emler
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
V	v	k7c6	v
komisi	komise	k1gFnSc6	komise
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Grégr	Grégr	k1gMnSc1	Grégr
a	a	k8xC	a
Ferd	Ferd	k1gMnSc1	Ferd
<g/>
.	.	kIx.	.
</s>
<s>
Dattel	Dattel	k1gInSc1	Dattel
258	[number]	k4	258
s.	s.	k?	s.
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Kronika	kronika	k1gFnSc1	kronika
starého	starý	k2eAgMnSc2d1	starý
kolegiáta	kolegiát	k1gMnSc2	kolegiát
pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
starých	starý	k2eAgInPc2d1	starý
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgInPc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
580	[number]	k4	580
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
kronik	kronika	k1gFnPc2	kronika
doby	doba	k1gFnSc2	doba
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
491	[number]	k4	491
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Kronika	kronika	k1gFnSc1	kronika
velmi	velmi	k6eAd1	velmi
pěkná	pěkný	k2eAgFnSc1d1	pěkná
o	o	k7c4	o
Janu	Jan	k1gMnSc3	Jan
Žižkovi	Žižka	k1gMnSc3	Žižka
<g/>
;	;	kIx,	;
Kronika	kronika	k1gFnSc1	kronika
Bartoška	Bartošek	k1gMnSc2	Bartošek
z	z	k7c2	z
Drahonic	Drahonice	k1gFnPc2	Drahonice
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
V.	V.	kA	V.
1402	[number]	k4	1402
<g/>
-	-	kIx~	-
<g/>
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
790	[number]	k4	790
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
296	[number]	k4	296
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
BĚLINA	Bělina	k1gMnSc1	Bělina
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Slavné	slavný	k2eAgFnPc1d1	slavná
bitvy	bitva	k1gFnPc1	bitva
naší	náš	k3xOp1gFnSc2	náš
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Marsyas	Marsyas	k1gMnSc1	Marsyas
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
272	[number]	k4	272
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901606	[number]	k4	901606
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DURDÍK	DURDÍK	kA	DURDÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgNnSc1d1	husitské
vojenství	vojenství	k1gNnSc1	vojenství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
210	[number]	k4	210
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgNnSc1d1	husitské
revoluční	revoluční	k2eAgNnSc1d1	revoluční
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
203	[number]	k4	203
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Tábor	Tábor	k1gInSc1	Tábor
v	v	k7c6	v
husitském	husitský	k2eAgNnSc6d1	husitské
revolučním	revoluční	k2eAgNnSc6d1	revoluční
hnutí	hnutí	k1gNnSc6	hnutí
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
430	[number]	k4	430
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PALACKÝ	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
národu	národ	k1gInSc2	národ
Českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
B.	B.	kA	B.
Kočí	Kočí	k1gMnSc1	Kočí
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
1279	[number]	k4	1279
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PEKAŘ	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
1191	[number]	k4	1191
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
385	[number]	k4	385
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
264	[number]	k4	264
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
válečných	válečný	k2eAgNnPc2d1	válečné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
420	[number]	k4	420
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEK	Tomek	k1gMnSc1	Tomek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
Vladivoj	Vladivoj	k1gInSc1	Vladivoj
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
V	v	k7c6	v
ráji	ráj	k1gInSc6	ráj
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
228	[number]	k4	228
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900875	[number]	k4	900875
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
URBÁNEK	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
313	[number]	k4	313
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Sudoměře	Sudoměř	k1gFnSc2	Sudoměř
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Britské	britský	k2eAgInPc1d1	britský
listy	list	k1gInPc1	list
–	–	k?	–
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
informace	informace	k1gFnSc2	informace
</s>
</p>
