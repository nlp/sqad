<s>
Město	město	k1gNnSc1	město
Příbor	Příbor	k1gInSc1	Příbor
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Freiberg	Freiberg	k1gInSc1	Freiberg
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
8	[number]	k4	8
486	[number]	k4	486
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
8	[number]	k4	8
789	[number]	k4	789
<g/>
.	.	kIx.	.
</s>
<s>
Příbor	Příbor	k1gInSc1	Příbor
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgNnSc7d3	nejstarší
městem	město	k1gNnSc7	město
okresu	okres	k1gInSc2	okres
Nový	nový	k2eAgInSc4d1	nový
Jičín	Jičín	k1gInSc4	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1251	[number]	k4	1251
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
markraběte	markrabě	k1gMnSc2	markrabě
moravského	moravský	k2eAgMnSc2d1	moravský
Přemysla	Přemysl	k1gMnSc2	Přemysl
<g/>
,	,	kIx,	,
budoucího	budoucí	k2eAgMnSc2d1	budoucí
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc4	město
založil	založit	k5eAaPmAgMnS	založit
Frank	Frank	k1gMnSc1	Frank
z	z	k7c2	z
Hückeswagenu	Hückeswagen	k1gInSc2	Hückeswagen
<g/>
.	.	kIx.	.
</s>
<s>
Příbor	Příbor	k1gInSc1	Příbor
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
prvopočátku	prvopočátek	k1gInSc2	prvopočátek
významným	významný	k2eAgNnSc7d1	významné
správním	správní	k2eAgNnSc7d1	správní
<g/>
,	,	kIx,	,
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
celého	celý	k2eAgNnSc2d1	celé
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1292	[number]	k4	1292
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
jako	jako	k8xC	jako
městečko	městečko	k1gNnSc1	městečko
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1294	[number]	k4	1294
jako	jako	k8xS	jako
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
panství	panství	k1gNnSc6	panství
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Hückeswagenu	Hückeswagen	k1gInSc2	Hückeswagen
a	a	k8xC	a
Příbora	Příbor	k1gInSc2	Příbor
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1307	[number]	k4	1307
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
biskupství	biskupství	k1gNnSc2	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1389	[number]	k4	1389
olomoucký	olomoucký	k2eAgInSc4d1	olomoucký
biskup	biskup	k1gInSc4	biskup
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
z	z	k7c2	z
Rýzmberka	Rýzmberka	k1gFnSc1	Rýzmberka
udělil	udělit	k5eAaPmAgInS	udělit
městu	město	k1gNnSc3	město
právo	právo	k1gNnSc4	právo
opevnění	opevnění	k1gNnSc3	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zástavní	zástavní	k2eAgMnSc1d1	zástavní
držitelé	držitel	k1gMnPc1	držitel
panství	panství	k1gNnSc2	panství
Hukvald	Hukvaldy	k1gInPc2	Hukvaldy
ovládali	ovládat	k5eAaImAgMnP	ovládat
město	město	k1gNnSc4	město
bývalí	bývalý	k2eAgMnPc1d1	bývalý
husitští	husitský	k2eAgMnPc1d1	husitský
hejtmané	hejtmaný	k2eAgFnPc1d1	hejtmaný
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Sokol	Sokol	k1gMnSc1	Sokol
z	z	k7c2	z
Lamberka	Lamberka	k1gFnSc1	Lamberka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1435	[number]	k4	1435
-	-	kIx~	-
1438	[number]	k4	1438
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Čapek	Čapek	k1gMnSc1	Čapek
ze	z	k7c2	z
Sán	sát	k5eAaImNgMnS	sát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1438	[number]	k4	1438
-	-	kIx~	-
1452	[number]	k4	1452
a	a	k8xC	a
bratřický	bratřický	k2eAgMnSc1d1	bratřický
hejtman	hejtman	k1gMnSc1	hejtman
Jan	Jan	k1gMnSc1	Jan
Talafús	Talafús	k1gInSc4	Talafús
z	z	k7c2	z
Ostrova	ostrov	k1gInSc2	ostrov
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1452	[number]	k4	1452
-	-	kIx~	-
1465	[number]	k4	1465
<g/>
.	.	kIx.	.
</s>
<s>
Zástavní	zástavní	k2eAgMnSc1d1	zástavní
držitel	držitel	k1gMnSc1	držitel
Hukvald	Hukvaldy	k1gInPc2	Hukvaldy
Dobeš	Dobeš	k1gMnSc1	Dobeš
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
vymohl	vymoct	k5eAaPmAgMnS	vymoct
na	na	k7c6	na
králi	král	k1gMnSc6	král
Vladislavu	Vladislava	k1gFnSc4	Vladislava
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagelonském	jagelonský	k2eAgInSc6d1	jagelonský
udělení	udělení	k1gNnSc4	udělení
městu	město	k1gNnSc3	město
dvou	dva	k4xCgInPc2	dva
výročních	výroční	k2eAgInPc2d1	výroční
trhů	trh	k1gInPc2	trh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1493	[number]	k4	1493
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Marek	Marek	k1gMnSc1	Marek
Khuen	Khuen	k1gInSc4	Khuen
udělil	udělit	k5eAaPmAgMnS	udělit
městu	město	k1gNnSc3	město
čtyři	čtyři	k4xCgInPc1	čtyři
výroční	výroční	k2eAgInPc1d1	výroční
trhy	trh	k1gInPc1	trh
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
trh	trh	k1gInSc1	trh
týdenní	týdenní	k2eAgInSc1d1	týdenní
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1560	[number]	k4	1560
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgMnSc1d3	nejpočetnější
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
cechů	cech	k1gInPc2	cech
byl	být	k5eAaImAgInS	být
významný	významný	k2eAgInSc1d1	významný
cech	cech	k1gInSc1	cech
soukeníků	soukeník	k1gMnPc2	soukeník
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
svá	svůj	k3xOyFgNnPc4	svůj
sukna	sukno	k1gNnPc4	sukno
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvážel	vyvážet	k5eAaImAgMnS	vyvážet
do	do	k7c2	do
Krakova	Krakov	k1gInSc2	Krakov
<g/>
,	,	kIx,	,
Poznaně	Poznaň	k1gFnSc2	Poznaň
<g/>
,	,	kIx,	,
Lvova	Lvov	k1gInSc2	Lvov
<g/>
,	,	kIx,	,
Levoče	Levoča	k1gFnSc2	Levoča
a	a	k8xC	a
Prešova	Prešov	k1gInSc2	Prešov
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1615	[number]	k4	1615
postoupil	postoupit	k5eAaPmAgMnS	postoupit
městu	město	k1gNnSc3	město
kardinál	kardinál	k1gMnSc1	kardinál
František	František	k1gMnSc1	František
Ditrichštejn	Ditrichštejn	k1gMnSc1	Ditrichštejn
řadu	řad	k1gInSc2	řad
privilegií	privilegium	k1gNnPc2	privilegium
a	a	k8xC	a
práv	právo	k1gNnPc2	právo
jako	jako	k9	jako
městské	městský	k2eAgNnSc1d1	Městské
právo	právo	k1gNnSc1	právo
olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
<g/>
,	,	kIx,	,
osvobození	osvobození	k1gNnSc4	osvobození
od	od	k7c2	od
robot	robota	k1gFnPc2	robota
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc4	rozšíření
rybolovu	rybolov	k1gInSc2	rybolov
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Lubině	Lubin	k2eAgFnSc6d1	Lubina
a	a	k8xC	a
mnohé	mnohý	k2eAgFnSc6d1	mnohá
jiné	jiná	k1gFnSc6	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
někdy	někdy	k6eAd1	někdy
přespával	přespávat	k5eAaImAgMnS	přespávat
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
jeho	jeho	k3xOp3gInSc1	jeho
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1617	[number]	k4	1617
si	se	k3xPyFc3	se
Příbor	Příbor	k1gInSc4	Příbor
od	od	k7c2	od
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
biskupa	biskup	k1gInSc2	biskup
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
celé	celý	k2eAgNnSc4d1	celé
panství	panství	k1gNnSc4	panství
Hukvaldy	Hukvaldy	k1gInPc4	Hukvaldy
na	na	k7c4	na
6	[number]	k4	6
let	léto	k1gNnPc2	léto
za	za	k7c4	za
plat	plat	k1gInSc4	plat
9	[number]	k4	9
600	[number]	k4	600
zl	zl	k?	zl
<g/>
.	.	kIx.	.
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
město	město	k1gNnSc1	město
třikrát	třikrát	k6eAd1	třikrát
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1621	[number]	k4	1621
od	od	k7c2	od
Valachů	valach	k1gInPc2	valach
<g/>
,	,	kIx,	,
1626	[number]	k4	1626
od	od	k7c2	od
dánského	dánský	k2eAgMnSc2d1	dánský
generála	generál	k1gMnSc2	generál
Mansfelda	Mansfeldo	k1gNnSc2	Mansfeldo
a	a	k8xC	a
1643	[number]	k4	1643
od	od	k7c2	od
Švédů	Švéd	k1gMnPc2	Švéd
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zdejší	zdejší	k2eAgFnSc4d1	zdejší
školu	škola	k1gFnSc4	škola
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
Sarkander	Sarkander	k1gMnSc1	Sarkander
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1694	[number]	k4	1694
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
piaristická	piaristický	k2eAgFnSc1d1	piaristická
kolej	kolej	k1gFnSc1	kolej
a	a	k8xC	a
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
velký	velký	k2eAgInSc1d1	velký
rozmach	rozmach	k1gInSc1	rozmach
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Piaristé	piarista	k1gMnPc1	piarista
hráli	hrát	k5eAaImAgMnP	hrát
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
provozovali	provozovat	k5eAaImAgMnP	provozovat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
spravovali	spravovat	k5eAaImAgMnP	spravovat
knihovnu	knihovna	k1gFnSc4	knihovna
s	s	k7c7	s
30	[number]	k4	30
prvotisky	prvotisk	k1gInPc7	prvotisk
(	(	kIx(	(
<g/>
tisky	tisk	k1gInPc1	tisk
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1500	[number]	k4	1500
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Příboře	Příbor	k1gInSc6	Příbor
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nejstarší	starý	k2eAgNnSc4d3	nejstarší
české	český	k2eAgNnSc4d1	české
ochotnické	ochotnický	k2eAgNnSc4d1	ochotnické
divadlo	divadlo	k1gNnSc4	divadlo
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tu	tu	k6eAd1	tu
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
první	první	k4xOgInSc1	první
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
dělnický	dělnický	k2eAgInSc1d1	dělnický
spolek	spolek	k1gInSc1	spolek
Harmonie	harmonie	k1gFnSc2	harmonie
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1856	[number]	k4	1856
se	se	k3xPyFc4	se
v	v	k7c6	v
Příboře	Příbor	k1gInSc6	Příbor
narodil	narodit	k5eAaPmAgMnS	narodit
zakladatel	zakladatel	k1gMnSc1	zakladatel
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
-	-	kIx~	-
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
tu	tu	k6eAd1	tu
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
český	český	k2eAgInSc1d1	český
učitelský	učitelský	k2eAgInSc1d1	učitelský
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
-	-	kIx~	-
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
centrem	centrum	k1gNnSc7	centrum
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
severní	severní	k2eAgFnSc2d1	severní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přispělo	přispět	k5eAaPmAgNnS	přispět
i	i	k9	i
založení	založení	k1gNnSc4	založení
reálky	reálka	k1gFnSc2	reálka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
příborské	příborský	k2eAgInPc1d1	příborský
prapory	prapor	k1gInPc1	prapor
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
obsadily	obsadit	k5eAaPmAgFnP	obsadit
pro	pro	k7c4	pro
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
Československo	Československo	k1gNnSc4	Československo
města	město	k1gNnSc2	město
Nový	nový	k2eAgInSc4d1	nový
Jičín	Jičín	k1gInSc4	Jičín
<g/>
,	,	kIx,	,
Fulnek	Fulnek	k1gInSc4	Fulnek
<g/>
,	,	kIx,	,
Odry	odr	k1gInPc4	odr
<g/>
,	,	kIx,	,
Bílovec	Bílovec	k1gInSc4	Bílovec
a	a	k8xC	a
Opavu	Opava	k1gFnSc4	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
Příbor	Příbor	k1gInSc1	Příbor
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
války	válka	k1gFnSc2	válka
město	město	k1gNnSc4	město
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
ve	v	k7c6	v
městě	město	k1gNnSc6	město
existoval	existovat	k5eAaImAgInS	existovat
pobočný	pobočný	k2eAgInSc4d1	pobočný
závod	závod	k1gInSc4	závod
Tatra	Tatra	k1gFnSc1	Tatra
Příbor	Příbor	k1gInSc1	Příbor
<g/>
,	,	kIx,	,
vyrábějící	vyrábějící	k2eAgFnSc1d1	vyrábějící
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
osobní	osobní	k2eAgInPc1d1	osobní
automobily	automobil	k1gInPc1	automobil
Tatra	Tatra	k1gFnSc1	Tatra
603	[number]	k4	603
<g/>
,	,	kIx,	,
T-613	T-613	k1gFnSc1	T-613
a	a	k8xC	a
T-	T-	k1gFnSc1	T-
<g/>
700	[number]	k4	700
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
významných	významný	k2eAgMnPc2d1	významný
rodáků	rodák	k1gMnPc2	rodák
města	město	k1gNnSc2	město
možno	možno	k6eAd1	možno
jmenovat	jmenovat	k5eAaImF	jmenovat
zakladatele	zakladatel	k1gMnPc4	zakladatel
moravské	moravský	k2eAgFnSc2d1	Moravská
vlastivědy	vlastivěda	k1gFnSc2	vlastivěda
a	a	k8xC	a
topografie	topografie	k1gFnSc2	topografie
Řehoře	Řehoř	k1gMnSc2	Řehoř
Volného	volný	k2eAgMnSc2d1	volný
(	(	kIx(	(
<g/>
1793	[number]	k4	1793
-	-	kIx~	-
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozofa	filozof	k1gMnSc2	filozof
a	a	k8xC	a
národního	národní	k2eAgMnSc2d1	národní
buditele	buditel	k1gMnSc2	buditel
Bonifáce	Bonifác	k1gMnSc2	Bonifác
Buzka	Buzek	k1gMnSc2	Buzek
(	(	kIx(	(
<g/>
1788	[number]	k4	1788
-	-	kIx~	-
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historika	historik	k1gMnSc2	historik
Bertolda	Bertolda	k1gFnSc1	Bertolda
Bretholze	Bretholze	k1gFnSc1	Bretholze
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
-	-	kIx~	-
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
a	a	k8xC	a
geologa	geolog	k1gMnSc2	geolog
Mořice	Mořic	k1gMnSc2	Mořic
Remeše	Remeš	k1gMnSc2	Remeš
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
-	-	kIx~	-
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
pamětní	pamětní	k2eAgFnSc1d1	pamětní
síň	síň	k1gFnSc1	síň
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
v	v	k7c6	v
Příboře	Příbor	k1gInSc6	Příbor
-	-	kIx~	-
Lidická	lidický	k2eAgFnSc1d1	Lidická
50	[number]	k4	50
Související	související	k2eAgFnSc1d1	související
informace	informace	k1gFnSc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Příboře	Příbor	k1gInSc6	Příbor
<g/>
.	.	kIx.	.
</s>
<s>
Památkově	památkově	k6eAd1	památkově
chráněné	chráněný	k2eAgInPc4d1	chráněný
domy	dům	k1gInPc4	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
Křížovou	Křížův	k2eAgFnSc7d1	Křížova
cestou	cesta	k1gFnSc7	cesta
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
Radnice	radnice	k1gFnSc1	radnice
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
Piaristický	piaristický	k2eAgInSc4d1	piaristický
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Valentina	Valentin	k1gMnSc2	Valentin
Kostelík	kostelík	k1gInSc4	kostelík
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
Serafinského	Serafinský	k2eAgNnSc2d1	Serafinský
Sousoší	sousoší	k1gNnSc2	sousoší
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc2d1	svatý
Rocha	Roch	k1gMnSc2	Roch
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Floriána	Florián	k1gMnSc2	Florián
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
Sloup	sloup	k1gInSc1	sloup
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g />
.	.	kIx.	.
</s>
<s>
Assumpty	Assumpta	k1gFnPc4	Assumpta
Rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
v	v	k7c6	v
Zámečnické	zámečnický	k2eAgFnSc6d1	zámečnická
ulici	ulice	k1gFnSc6	ulice
Fontána	fontána	k1gFnSc1	fontána
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
Piaristická	piaristický	k2eAgFnSc1d1	piaristická
kolej	kolej	k1gFnSc1	kolej
Fara	fara	k1gFnSc1	fara
Secesní	secesní	k2eAgFnSc1d1	secesní
budova	budova	k1gFnSc1	budova
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
gymnázia	gymnázium	k1gNnSc2	gymnázium
Kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Sarkandra	Sarkandra	k1gFnSc1	Sarkandra
Kaplička	kaplička	k1gFnSc1	kaplička
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Sedlnické	Sedlnický	k2eAgFnSc2d1	Sedlnická
sněženky	sněženka	k1gFnSc2	sněženka
Bertold	Bertold	k1gMnSc1	Bertold
Bretholz	Bretholz	k1gMnSc1	Bretholz
–	–	k?	–
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
editor	editor	k1gMnSc1	editor
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
Bonifác	Bonifác	k1gMnSc1	Bonifác
Buzek	Buzek	k1gMnSc1	Buzek
–	–	k?	–
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
buditel	buditel	k1gMnSc1	buditel
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
–	–	k?	–
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
Jan	Jan	k1gMnSc1	Jan
Gillar	Gillar	k1gMnSc1	Gillar
–	–	k?	–
architekt	architekt	k1gMnSc1	architekt
Gregor	Gregor	k1gMnSc1	Gregor
Wolný	Wolný	k1gMnSc1	Wolný
–	–	k?	–
benediktin	benediktin	k1gMnSc1	benediktin
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
středoškolský	středoškolský	k2eAgMnSc1d1	středoškolský
učitel	učitel	k1gMnSc1	učitel
Příbor	Příbor	k1gInSc1	Příbor
(	(	kIx(	(
<g/>
katastrální	katastrální	k2eAgFnSc1d1	katastrální
území	území	k1gNnSc4	území
Příbor	Příbor	k1gInSc1	Příbor
a	a	k8xC	a
Klokočov	Klokočov	k1gInSc1	Klokočov
u	u	k7c2	u
Příbora	Příbor	k1gInSc2	Příbor
<g/>
)	)	kIx)	)
Hájov	Hájov	k1gInSc1	Hájov
Prchalov	Prchalov	k1gInSc1	Prchalov
</s>
