<s hack="1">
obyvatel	obyvatel	k1gMnSc1
<g/>
,	,	kIx,
největší	veliký	k2eAgNnSc1d3
město	město	k1gNnSc1
Izraele	Izrael	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
o	o	k7c6
rozloze	rozloha	k1gFnSc6
125,1	125,1	k4
km²	km²	k?
žije	žít	k5eAaImIp3nS
celkem	celkem	k6eAd1
901	#num#	k4
300	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
údaje	údaj	k1gInPc4
z	z	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
považují	považovat	k5eAaImIp3nP
Jeruzalém	Jeruzalém	k1gInSc4
za	za	k7c4
své	svůj	k3xOyFgMnPc4
<g />
.	.	kIx.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
י	י	k?
<g/>
ְ	ְ	k?
<g/>
ר	ר	k?
<g/>
ּ	ּ	k?
<g/>
ש	ש	k?
<g/>
ָ	ָ	k?
<g/>
ׁ	ׁ	k?
<g/>
ל	ל	k?
<g/>
ַ	ַ	k?
<g/>
י	י	k?
<g/>
ִ	ִ	k?
<g/>
ם	ם	k?
<g/>
,	,	kIx,
Jerušalajim	Jerušalajim	k1gInSc1
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
ا	ا	k?
<g/>
ُ	ُ	k?
<g/>
د	د	k?
<g/>
,	,	kIx,
al-Kuds	al-Kuds	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
do	do	k7c2
rozlohy	rozloha	k1gFnSc2
a	a	k8xC
počtu	počet	k1gInSc2
<g />
.	.	kIx.
</s>