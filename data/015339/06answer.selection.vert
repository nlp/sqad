<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
místa	místo	k1gNnSc2
tří	tři	k4xCgNnPc2
monoteistických	monoteistický	k2eAgNnPc2d1
náboženství	náboženství	k1gNnPc2
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
ve	v	k7c6
Starém	starý	k2eAgNnSc6d1
Městě	město	k1gNnSc6
na	na	k7c6
celkové	celkový	k2eAgFnSc6d1
rozloze	rozloha	k1gFnSc6
necelého	celý	k2eNgInSc2d1
čtverečního	čtvereční	k2eAgInSc2d1
kilometru	kilometr	k1gInSc2
a	a	k8xC
zahrnují	zahrnovat	k5eAaImIp3nP
Chrámovou	chrámový	k2eAgFnSc4d1
horu	hora	k1gFnSc4
<g/>
,	,	kIx,
Západní	západní	k2eAgFnSc4d1
zeď	zeď	k1gFnSc4
<g/>
,	,	kIx,
baziliku	bazilika	k1gFnSc4
Svatého	svatý	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
<g/>
,	,	kIx,
Skalní	skalní	k2eAgInSc4d1
dóm	dóm	k1gInSc4
a	a	k8xC
mešitu	mešita	k1gFnSc4
al-Aksá	al-Aksat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>