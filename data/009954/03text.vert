<p>
<s>
Blake	Blake	k6eAd1	Blake
Christina	Christina	k1gFnSc1	Christina
Lively	Livela	k1gFnSc2	Livela
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1987	[number]	k4	1987
Tarzana	Tarzan	k1gMnSc2	Tarzan
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
modelka	modelka	k1gFnSc1	modelka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
Sereny	Serena	k1gFnSc2	Serena
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Woodsenové	Woodsenová	k1gFnSc2	Woodsenová
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Super	super	k2eAgFnSc1d1	super
drbna	drbna	k1gFnSc1	drbna
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Sesterstvo	sesterstvo	k1gNnSc1	sesterstvo
putovních	putovní	k2eAgFnPc2d1	putovní
kalhot	kalhoty	k1gFnPc2	kalhoty
<g/>
,	,	kIx,	,
Soukromé	soukromý	k2eAgInPc4d1	soukromý
životy	život	k1gInPc4	život
Pippy	Pippa	k1gFnSc2	Pippa
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Divoši	divoch	k1gMnPc1	divoch
<g/>
,	,	kIx,	,
Green	Green	k2eAgMnSc1d1	Green
Lantern	Lantern	k1gMnSc1	Lantern
a	a	k8xC	a
Věčně	věčně	k6eAd1	věčně
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
herce	herec	k1gMnSc4	herec
Ryana	Ryan	k1gMnSc4	Ryan
Reyonldse	Reyonlds	k1gMnSc4	Reyonlds
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc1	dva
dcery	dcera	k1gFnPc1	dcera
James	Jamesa	k1gFnPc2	Jamesa
a	a	k8xC	a
Ines	Inesa	k1gFnPc2	Inesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc1d1	poslední
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
umělecky	umělecky	k6eAd1	umělecky
založené	založený	k2eAgFnSc2d1	založená
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS	být
herec	herec	k1gMnSc1	herec
Ernie	Ernie	k1gFnSc2	Ernie
Lively	Livela	k1gFnSc2	Livela
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Elaine	elain	k1gInSc5	elain
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
sourozenci	sourozenec	k1gMnPc1	sourozenec
jsou	být	k5eAaImIp3nP	být
Eric	Eric	k1gInSc4	Eric
<g/>
,	,	kIx,	,
Jason	Jason	k1gNnSc4	Jason
a	a	k8xC	a
sestry	sestra	k1gFnPc4	sestra
Robyn	Robyno	k1gNnPc2	Robyno
a	a	k8xC	a
Lori	Lori	k1gFnPc2	Lori
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
také	také	k9	také
filmové	filmový	k2eAgFnPc4d1	filmová
ambice	ambice	k1gFnPc4	ambice
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
téměř	téměř	k6eAd1	téměř
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
člen	člen	k1gMnSc1	člen
rodiny	rodina	k1gFnSc2	rodina
půjde	jít	k5eAaImIp3nS	jít
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
stopách	stopa	k1gFnPc6	stopa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
herectví	herectví	k1gNnSc2	herectví
nechtěla	chtít	k5eNaImAgFnS	chtít
věnovat	věnovat	k5eAaPmF	věnovat
<g/>
,	,	kIx,	,
plánovala	plánovat	k5eAaImAgFnS	plánovat
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Stanfordově	Stanfordův	k2eAgFnSc6d1	Stanfordova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
léta	léto	k1gNnSc2	léto
mezi	mezi	k7c7	mezi
druhým	druhý	k4xOgNnSc7	druhý
a	a	k8xC	a
s	s	k7c7	s
třetím	třetí	k4xOgInSc7	třetí
ročníkem	ročník	k1gInSc7	ročník
na	na	k7c4	na
Burbank	Burbank	k1gInSc4	Burbank
High	High	k1gInSc4	High
School	Schoola	k1gFnPc2	Schoola
navštívila	navštívit	k5eAaPmAgFnS	navštívit
pár	pár	k4xCyI	pár
konkurzů	konkurz	k1gInPc2	konkurz
a	a	k8xC	a
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
získala	získat	k5eAaPmAgFnS	získat
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Sesterstvo	sesterstvo	k1gNnSc1	sesterstvo
putovních	putovní	k2eAgFnPc2d1	putovní
kalhot	kalhoty	k1gFnPc2	kalhoty
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Svojí	svojit	k5eAaImIp3nS	svojit
hereckou	herecký	k2eAgFnSc4d1	herecká
kariéru	kariéra	k1gFnSc4	kariéra
zahájila	zahájit	k5eAaPmAgFnS	zahájit
v	v	k7c6	v
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Sandman	Sandman	k1gMnSc1	Sandman
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
nastala	nastat	k5eAaPmAgFnS	nastat
delší	dlouhý	k2eAgFnSc1d2	delší
pauza	pauza	k1gFnSc1	pauza
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
roli	role	k1gFnSc4	role
Bridget	Bridgeta	k1gFnPc2	Bridgeta
ve	v	k7c6	v
filmu	film	k1gInSc6	film
-	-	kIx~	-
Sesterstvo	sesterstvo	k1gNnSc1	sesterstvo
putovních	putovní	k2eAgFnPc2d1	putovní
kalhot	kalhoty	k1gFnPc2	kalhoty
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
roli	role	k1gFnSc4	role
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Teen	Teen	k1gNnSc4	Teen
Choice	Choice	k1gFnSc2	Choice
Award	Awarda	k1gFnPc2	Awarda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
hororu	horor	k1gInSc6	horor
Simon	Simon	k1gMnSc1	Simon
Says	Says	k1gInSc1	Says
a	a	k8xC	a
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Accepted	Accepted	k1gInSc1	Accepted
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
přijala	přijmout	k5eAaPmAgFnS	přijmout
roli	role	k1gFnSc4	role
Sereny	Serena	k1gFnSc2	Serena
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Woodsenové	Woodsenová	k1gFnSc2	Woodsenová
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
televize	televize	k1gFnSc2	televize
The	The	k1gFnSc1	The
CW	CW	kA	CW
Super	super	k2eAgFnSc1d1	super
drbna	drbna	k1gFnSc1	drbna
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Leighton	Leighton	k1gInSc1	Leighton
Meester	Meester	k1gInSc1	Meester
a	a	k8xC	a
Taylor	Taylor	k1gInSc1	Taylor
Momsen	Momsen	k2eAgInSc1d1	Momsen
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
znovu	znovu	k6eAd1	znovu
zahrála	zahrát	k5eAaPmAgFnS	zahrát
roli	role	k1gFnSc4	role
Bridget	Bridgeta	k1gFnPc2	Bridgeta
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
filmu	film	k1gInSc2	film
Sesterstvo	sesterstvo	k1gNnSc4	sesterstvo
putovních	putovní	k2eAgFnPc2d1	putovní
kalhot	kalhoty	k1gFnPc2	kalhoty
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
získal	získat	k5eAaPmAgInS	získat
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
ohlas	ohlas	k1gInSc4	ohlas
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gInSc4	jeho
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
hrála	hrát	k5eAaImAgFnS	hrát
menší	malý	k2eAgFnSc4d2	menší
roli	role	k1gFnSc4	role
Gabrielly	Gabriella	k1gFnSc2	Gabriella
DiMarco	DiMarco	k6eAd1	DiMarco
v	v	k7c6	v
romantické	romantický	k2eAgFnSc6d1	romantická
komedii	komedie	k1gFnSc6	komedie
s	s	k7c7	s
názvem	název	k1gInSc7	název
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
miluji	milovat	k5eAaImIp1nS	milovat
Tě	ty	k3xPp2nSc4	ty
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Soukromé	soukromý	k2eAgInPc4d1	soukromý
životy	život	k1gInPc4	život
Pippy	Pippa	k1gFnSc2	Pippa
Lee	Lea	k1gFnSc6	Lea
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
mladší	mladý	k2eAgFnSc4d2	mladší
verzi	verze	k1gFnSc4	verze
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
uznání	uznání	k1gNnSc4	uznání
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
začala	začít	k5eAaPmAgFnS	začít
natáčet	natáčet	k5eAaImF	natáčet
scény	scéna	k1gFnPc4	scéna
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Bena	Bena	k?	Bena
Afflecka	Afflecko	k1gNnSc2	Afflecko
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
Carol	Carol	k1gInSc4	Carol
Ferris	Ferris	k1gFnSc2	Ferris
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Green	Green	k2eAgInSc4d1	Green
Lantern	Lantern	k1gInSc4	Lantern
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Reynoldsem	Reynolds	k1gMnSc7	Reynolds
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
si	se	k3xPyFc3	se
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vzala	vzít	k5eAaPmAgFnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vydělal	vydělat	k5eAaPmAgInS	vydělat
přes	přes	k7c4	přes
212	[number]	k4	212
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
velké	velký	k2eAgNnSc4d1	velké
zklamání	zklamání	k1gNnSc4	zklamání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
kráse	krása	k1gFnSc3	krása
byla	být	k5eAaImAgFnS	být
Blake	Blake	k1gFnSc1	Blake
Lively	Livela	k1gFnSc2	Livela
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zvolena	zvolit	k5eAaPmNgFnS	zvolit
časopisem	časopis	k1gInSc7	časopis
AskMen	AskMen	k1gInSc1	AskMen
první	první	k4xOgFnSc2	první
z	z	k7c2	z
99	[number]	k4	99
nejkrásnějších	krásný	k2eAgFnPc2d3	nejkrásnější
žen	žena	k1gFnPc2	žena
světa	svět	k1gInSc2	svět
a	a	k8xC	a
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
také	také	k9	také
tváří	tvář	k1gFnSc7	tvář
kampaně	kampaň	k1gFnSc2	kampaň
Mademoiselle	Mademoiselle	k1gFnSc2	Mademoiselle
pro	pro	k7c4	pro
značku	značka	k1gFnSc4	značka
Chanel	Chanela	k1gFnPc2	Chanela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
přišla	přijít	k5eAaPmAgFnS	přijít
role	role	k1gFnSc1	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Divoši	divoch	k1gMnPc1	divoch
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Taylora	Taylor	k1gMnSc4	Taylor
Kitsche	Kitsch	k1gMnSc4	Kitsch
<g/>
,	,	kIx,	,
Aarona	Aaron	k1gMnSc4	Aaron
Johnsona	Johnson	k1gMnSc4	Johnson
<g/>
,	,	kIx,	,
<g/>
Salmy	Salmo	k1gNnPc7	Salmo
Hayek	Hayek	k1gInSc1	Hayek
a	a	k8xC	a
Johna	John	k1gMnSc4	John
Travolty	Travolt	k1gInPc1	Travolt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tváří	tvář	k1gFnSc7	tvář
značky	značka	k1gFnSc2	značka
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Oréal	Oréal	k1gMnSc1	Oréal
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
měsíc	měsíc	k1gInSc1	měsíc
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
Adaline	Adalin	k1gInSc5	Adalin
Bowmanové	Bowmanové	k2eAgNnPc6d1	Bowmanové
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Věčně	věčně	k6eAd1	věčně
mladá	mladý	k2eAgFnSc1d1	mladá
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Age	Age	k1gFnSc2	Age
of	of	k?	of
Adaline	Adalin	k1gInSc5	Adalin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
hororovém	hororový	k2eAgInSc6d1	hororový
snímku	snímek	k1gInSc6	snímek
Mělčiny	mělčina	k1gFnSc2	mělčina
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
byl	být	k5eAaImAgInS	být
kritiky	kritika	k1gFnSc2	kritika
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
,	,	kIx,	,
obzvlášť	obzvlášť	k6eAd1	obzvlášť
její	její	k3xOp3gInSc4	její
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
obsazení	obsazení	k1gNnSc3	obsazení
filmu	film	k1gInSc2	film
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
laskavost	laskavost	k1gFnSc1	laskavost
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zahraje	zahrát	k5eAaPmIp3nS	zahrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
ženskou	ženský	k2eAgFnSc4d1	ženská
postavu	postava	k1gFnSc4	postava
ve	v	k7c6	v
filmovém	filmový	k2eAgNnSc6d1	filmové
zpracování	zpracování	k1gNnSc6	zpracování
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
novely	novela	k1gFnSc2	novela
The	The	k1gMnSc1	The
Rhytm	Rhytm	k1gMnSc1	Rhytm
Section	Section	k1gInSc4	Section
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
premiéru	premiéra	k1gFnSc4	premiéra
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
začalo	začít	k5eAaPmAgNnS	začít
povídat	povídat	k5eAaImF	povídat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Blake	Blake	k1gFnSc1	Blake
chodí	chodit	k5eAaImIp3nS	chodit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kolegou	kolega	k1gMnSc7	kolega
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Super	super	k2eAgFnSc1d1	super
drbna	drbna	k1gFnSc1	drbna
Pennem	Penn	k1gInSc7	Penn
Badgleym	Badgleym	k1gInSc1	Badgleym
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
poprvé	poprvé	k6eAd1	poprvé
promluvili	promluvit	k5eAaPmAgMnP	promluvit
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
vztahu	vztah	k1gInSc6	vztah
<g/>
,	,	kIx,	,
dvojice	dvojice	k1gFnSc1	dvojice
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
rozešla	rozejít	k5eAaPmAgFnS	rozejít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Green	Green	k2eAgInSc4d1	Green
Lantern	Lantern	k1gInSc4	Lantern
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Reynolsem	Reynols	k1gMnSc7	Reynols
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vzala	vzít	k5eAaPmAgFnS	vzít
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
James	James	k1gMnSc1	James
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Inez	Ineza	k1gFnPc2	Ineza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Televize	televize	k1gFnSc1	televize
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Blake	Blak	k1gInSc2	Blak
Lively	Livela	k1gFnSc2	Livela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Blake	Blake	k1gInSc1	Blake
Lively	Livela	k1gFnSc2	Livela
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
