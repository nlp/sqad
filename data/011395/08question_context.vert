<s>
Mladí	mladý	k2eAgMnPc1d1
ptáci	pták	k1gMnPc1
jsou	být	k5eAaImIp3nP
snadno	snadno	k6eAd1
rozpoznatelní	rozpoznatelný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Tučňák	tučňák	k1gMnSc1
královský	královský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Eudyptes	Eudyptes	k1gMnSc1
schlegeli	schleget	k5eAaPmAgMnP
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velký	velký	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
čeledi	čeleď	k1gFnSc2
tučnákovitých	tučnákovitý	k2eAgFnPc2d1
obývající	obývající	k2eAgFnPc4d1
vody	voda	k1gFnPc4
obklopující	obklopující	k2eAgFnSc4d1
Antarktidu	Antarktida	k1gFnSc4
<g/>
.	.	kIx.
</s>