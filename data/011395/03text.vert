<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
(	(	kIx(	(
<g/>
Eudyptes	Eudyptes	k1gMnSc1	Eudyptes
schlegeli	schleget	k5eAaPmAgMnP	schleget
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
zástupce	zástupce	k1gMnSc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
tučnákovitých	tučnákovitý	k2eAgFnPc2d1	tučnákovitý
obývající	obývající	k2eAgFnPc4d1	obývající
vody	voda	k1gFnPc4	voda
obklopující	obklopující	k2eAgFnSc4d1	obklopující
Antarktidu	Antarktida	k1gFnSc4	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Množí	množit	k5eAaImIp3nP	množit
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Macquarie	Macquarie	k1gFnSc2	Macquarie
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
druhy	druh	k1gInPc1	druh
tučňáků	tučňák	k1gMnPc2	tučňák
tráví	trávit	k5eAaImIp3nP	trávit
i	i	k9	i
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
i	i	k8xC	i
vzhledem	vzhled	k1gInSc7	vzhled
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
podobá	podobat	k5eAaImIp3nS	podobat
tučnáku	tučnák	k1gInSc3	tučnák
žlutorohému	žlutorohý	k2eAgInSc3d1	žlutorohý
(	(	kIx(	(
<g/>
Eudyptes	Eudyptes	k1gMnSc1	Eudyptes
chrysolophus	chrysolophus	k1gMnSc1	chrysolophus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obličej	obličej	k1gInSc4	obličej
a	a	k8xC	a
bradu	brada	k1gFnSc4	brada
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
poddruhů	poddruh	k1gInPc2	poddruh
tučňáka	tučňák	k1gMnSc2	tučňák
žlutorohého	žlutorohý	k2eAgMnSc2d1	žlutorohý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
kříží	křížit	k5eAaImIp3nP	křížit
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
poměrně	poměrně	k6eAd1	poměrně
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
záležitost	záležitost	k1gFnSc1	záležitost
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
i	i	k9	i
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
žily	žít	k5eAaImAgFnP	žít
páry	pára	k1gFnPc1	pára
tvořené	tvořený	k2eAgFnPc1d1	tvořená
zástupci	zástupce	k1gMnSc3	zástupce
obou	dva	k4xCgFnPc2	dva
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
název	název	k1gInSc1	název
získal	získat	k5eAaPmAgInS	získat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
německého	německý	k2eAgMnSc2d1	německý
zoologa	zoolog	k1gMnSc2	zoolog
Hermanna	Hermann	k1gMnSc2	Hermann
Schlegela	Schlegel	k1gMnSc2	Schlegel
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
výšky	výška	k1gFnPc4	výška
kolem	kolem	k7c2	kolem
70	[number]	k4	70
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
zhruba	zhruba	k6eAd1	zhruba
6	[number]	k4	6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
českého	český	k2eAgInSc2d1	český
názvu	název	k1gInSc2	název
královský	královský	k2eAgMnSc1d1	královský
<g/>
,	,	kIx,	,
podobného	podobný	k2eAgInSc2d1	podobný
významu	význam	k1gInSc2	význam
císařský	císařský	k2eAgInSc1d1	císařský
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
zaměňován	zaměňován	k2eAgMnSc1d1	zaměňován
s	s	k7c7	s
tučňákem	tučňák	k1gMnSc7	tučňák
císařským	císařský	k2eAgMnSc7d1	císařský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
téměř	téměř	k6eAd1	téměř
dvakrát	dvakrát	k6eAd1	dvakrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
a	a	k8xC	a
několikanásobně	několikanásobně	k6eAd1	několikanásobně
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
krilem	kril	k1gInSc7	kril
<g/>
,	,	kIx,	,
rybami	ryba	k1gFnPc7	ryba
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
jiných	jiný	k2eAgMnPc2d1	jiný
vodních	vodní	k2eAgMnPc2d1	vodní
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
nebo	nebo	k8xC	nebo
na	na	k7c6	na
holých	holý	k2eAgInPc6d1	holý
svazích	svah	k1gInPc6	svah
pokrytých	pokrytý	k2eAgInPc2d1	pokrytý
nejrůznější	různý	k2eAgFnSc7d3	nejrůznější
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
si	se	k3xPyFc3	se
staví	stavit	k5eAaBmIp3nP	stavit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mělké	mělký	k2eAgFnSc2d1	mělká
díry	díra	k1gFnSc2	díra
v	v	k7c6	v
písku	písek	k1gInSc6	písek
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
pokládá	pokládat	k5eAaImIp3nS	pokládat
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
kameny	kámen	k1gInPc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
druhy	druh	k1gInPc4	druh
tučňáků	tučňák	k1gMnPc2	tučňák
je	být	k5eAaImIp3nS	být
i	i	k9	i
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
vysoce	vysoce	k6eAd1	vysoce
koloniální	koloniální	k2eAgInSc1d1	koloniální
a	a	k8xC	a
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
obrovských	obrovský	k2eAgFnPc6d1	obrovská
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
rozprostírat	rozprostírat	k5eAaImF	rozprostírat
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
větším	většit	k5eAaImIp1nS	většit
jak	jak	k6eAd1	jak
jeden	jeden	k4xCgInSc4	jeden
kilometr	kilometr	k1gInSc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plně	plně	k6eAd1	plně
rozprouděné	rozprouděný	k2eAgNnSc1d1	rozprouděný
je	být	k5eAaImIp3nS	být
až	až	k9	až
během	během	k7c2	během
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgNnPc1	dva
vejce	vejce	k1gNnPc1	vejce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
přežije	přežít	k5eAaPmIp3nS	přežít
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
nakladení	nakladení	k1gNnSc6	nakladení
vajec	vejce	k1gNnPc2	vejce
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
lovit	lovit	k5eAaImF	lovit
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
partner	partner	k1gMnSc1	partner
udržuje	udržovat	k5eAaImIp3nS	udržovat
vejce	vejce	k1gNnPc4	vejce
v	v	k7c6	v
teple	teplo	k1gNnSc6	teplo
<g/>
;	;	kIx,	;
drží	držet	k5eAaImIp3nP	držet
je	on	k3xPp3gFnPc4	on
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
ploše	plocha	k1gFnSc6	plocha
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
pod	pod	k7c7	pod
kožními	kožní	k2eAgInPc7d1	kožní
záhyby	záhyb	k1gInPc7	záhyb
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
chovu	chov	k1gInSc2	chov
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
tukových	tukový	k2eAgFnPc2d1	tuková
zásob	zásoba	k1gFnPc2	zásoba
svého	své	k1gNnSc2	své
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
samice	samice	k1gFnSc2	samice
si	se	k3xPyFc3	se
úlohy	úloha	k1gFnPc1	úloha
vymění	vyměnit	k5eAaPmIp3nP	vyměnit
<g/>
,	,	kIx,	,
samec	samec	k1gInSc1	samec
jde	jít	k5eAaImIp3nS	jít
hledat	hledat	k5eAaImF	hledat
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnPc1	vejce
se	se	k3xPyFc4	se
inkubují	inkubovat	k5eAaImIp3nP	inkubovat
zhruba	zhruba	k6eAd1	zhruba
35	[number]	k4	35
dní	den	k1gInPc2	den
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
života	život	k1gInSc2	život
začnou	začít	k5eAaPmIp3nP	začít
mláďata	mládě	k1gNnPc4	mládě
tvořit	tvořit	k5eAaImF	tvořit
velké	velká	k1gFnPc4	velká
skupinky	skupinka	k1gFnSc2	skupinka
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
choulí	choulet	k5eAaImIp3nP	choulet
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
více	hodně	k6eAd2	hodně
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
opeřené	opeřený	k2eAgNnSc1d1	opeřené
je	být	k5eAaImIp3nS	být
mládě	mládě	k1gNnSc1	mládě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
65	[number]	k4	65
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
prozatím	prozatím	k6eAd1	prozatím
není	být	k5eNaImIp3nS	být
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
se	se	k3xPyFc4	se
však	však	k9	však
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
jediném	jediný	k2eAgInSc6d1	jediný
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
ekologická	ekologický	k2eAgFnSc1d1	ekologická
nestabilita	nestabilita	k1gFnSc1	nestabilita
může	moct	k5eAaImIp3nS	moct
vyústit	vyústit	k5eAaPmF	vyústit
v	v	k7c4	v
závažný	závažný	k2eAgInSc4d1	závažný
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nechová	chovat	k5eNaImIp3nS	chovat
jej	on	k3xPp3gMnSc4	on
doposud	doposud	k6eAd1	doposud
žádná	žádný	k3yNgFnSc1	žádný
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Maquarie	Maquarie	k1gFnSc2	Maquarie
<g/>
.	.	kIx.	.
</s>
<s>
Spatřen	spatřit	k5eAaPmNgInS	spatřit
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
i	i	k9	i
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
nebo	nebo	k8xC	nebo
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Buďto	buďto	k8xC	buďto
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
tuláky	tulák	k1gMnPc4	tulák
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
menší	malý	k2eAgFnSc4d2	menší
skupinu	skupina	k1gFnSc4	skupina
spíše	spíše	k9	spíše
zmatených	zmatený	k2eAgMnPc2d1	zmatený
mladistvých	mladistvý	k1gMnPc2	mladistvý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
totiž	totiž	k9	totiž
tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
obvykle	obvykle	k6eAd1	obvykle
přečkává	přečkávat	k5eAaImIp3nS	přečkávat
zimu	zima	k1gFnSc4	zima
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
migruje	migrovat	k5eAaImIp3nS	migrovat
na	na	k7c4	na
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgInSc4d1	zmíněný
ostrov	ostrov	k1gInSc4	ostrov
Maquarie	Maquarie	k1gFnSc2	Maquarie
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
drobné	drobný	k2eAgFnPc1d1	drobná
kolonie	kolonie	k1gFnPc1	kolonie
na	na	k7c6	na
vzdálených	vzdálený	k2eAgInPc6d1	vzdálený
ostrovech	ostrov	k1gInPc6	ostrov
Campbellových	Campbellův	k2eAgInPc6d1	Campbellův
a	a	k8xC	a
Kerguelenech	Kerguelen	k1gInPc6	Kerguelen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
až	až	k9	až
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Georgii	Georgie	k1gFnSc6	Georgie
<g/>
.	.	kIx.	.
</s>
<s>
Dost	dost	k6eAd1	dost
často	často	k6eAd1	často
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
záměnu	záměna	k1gFnSc4	záměna
s	s	k7c7	s
tučňáky	tučňák	k1gMnPc7	tučňák
žlutorohými	žlutorohý	k2eAgMnPc7d1	žlutorohý
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
jedinců	jedinec	k1gMnPc2	jedinec
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
zatoulá	zatoulat	k5eAaPmIp3nS	zatoulat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ať	ať	k9	ať
už	už	k9	už
z	z	k7c2	z
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
prozatím	prozatím	k6eAd1	prozatím
o	o	k7c4	o
populaci	populace	k1gFnSc4	populace
významnou	významný	k2eAgFnSc4d1	významná
nebo	nebo	k8xC	nebo
trvale	trvale	k6eAd1	trvale
hnízdící	hnízdící	k2eAgFnSc1d1	hnízdící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
až	až	k9	až
75	[number]	k4	75
cm	cm	kA	cm
<g/>
,	,	kIx,	,
a	a	k8xC	a
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
8	[number]	k4	8
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
při	při	k7c6	při
pelichání	pelichání	k1gNnSc6	pelichání
nebo	nebo	k8xC	nebo
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
i	i	k8xC	i
týdnů	týden	k1gInPc2	týden
hladoví	hladovět	k5eAaImIp3nS	hladovět
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c2	za
takových	takový	k3xDgFnPc2	takový
okolností	okolnost	k1gFnPc2	okolnost
může	moct	k5eAaImIp3nS	moct
vážit	vážit	k5eAaImF	vážit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
až	až	k9	až
čtyři	čtyři	k4xCgInPc4	čtyři
kilogramy	kilogram	k1gInPc4	kilogram
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
má	mít	k5eAaImIp3nS	mít
šedé	šedý	k2eAgNnSc1d1	šedé
až	až	k8xS	až
černé	černý	k2eAgNnSc1d1	černé
peří	peří	k1gNnSc1	peří
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgFnSc6d1	vrchní
straně	strana	k1gFnSc6	strana
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
a	a	k8xC	a
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
už	už	k9	už
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
jen	jen	k9	jen
bílé	bílý	k2eAgNnSc1d1	bílé
peří	peří	k1gNnSc1	peří
(	(	kIx(	(
<g/>
snad	snad	k9	snad
jen	jen	k9	jen
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
strana	strana	k1gFnSc1	strana
křídel	křídlo	k1gNnPc2	křídlo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
místy	místy	k6eAd1	místy
ozdobena	ozdobit	k5eAaPmNgFnS	ozdobit
nepravidelným	pravidelný	k2eNgInSc7d1	nepravidelný
černým	černý	k2eAgInSc7d1	černý
flekem	flek	k1gInSc7	flek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
příbuzných	příbuzný	k1gMnPc2	příbuzný
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
bíle	bíle	k6eAd1	bíle
tváře	tvář	k1gFnPc4	tvář
a	a	k8xC	a
bradu	brada	k1gFnSc4	brada
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
je	být	k5eAaImIp3nS	být
výrazněji	výrazně	k6eAd2	výrazně
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
volně	volně	k6eAd1	volně
visících	visící	k2eAgNnPc2d1	visící
peříček	peříčko	k1gNnPc2	peříčko
(	(	kIx(	(
<g/>
chocholku	chocholka	k1gFnSc4	chocholka
<g/>
)	)	kIx)	)
zlatavé	zlatavý	k2eAgFnPc4d1	zlatavá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
až	až	k9	až
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
10	[number]	k4	10
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Disponuje	disponovat	k5eAaBmIp3nS	disponovat
silným	silný	k2eAgInSc7d1	silný
červeno-hnědým	červenonědý	k2eAgInSc7d1	červeno-hnědý
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
součástí	součást	k1gFnSc7	součást
k	k	k7c3	k
soužití	soužití	k1gNnSc3	soužití
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
kolonii	kolonie	k1gFnSc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Duhovky	duhovka	k1gFnPc1	duhovka
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
červené	červený	k2eAgFnPc1d1	červená
a	a	k8xC	a
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
růžové	růžový	k2eAgFnPc1d1	růžová
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podrážky	podrážka	k1gFnPc1	podrážka
černé	černý	k2eAgFnPc1d1	černá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
snadno	snadno	k6eAd1	snadno
rozpoznatelní	rozpoznatelný	k2eAgMnPc1d1	rozpoznatelný
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
má	mít	k5eAaImIp3nS	mít
jejich	jejich	k3xOp3gInSc1	jejich
tmavý	tmavý	k2eAgInSc1d1	tmavý
odstín	odstín	k1gInSc1	odstín
šatu	šat	k1gInSc2	šat
hnědý	hnědý	k2eAgInSc1d1	hnědý
nádech	nádech	k1gInSc1	nádech
<g/>
,	,	kIx,	,
a	a	k8xC	a
světlá	světlý	k2eAgNnPc4d1	světlé
pera	pero	k1gNnPc4	pero
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
našedlá	našedlý	k2eAgNnPc4d1	našedlé
nežli	nežli	k8xS	nežli
bílá	bílý	k2eAgNnPc4d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc4	zobák
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
prostě	prostě	k9	prostě
černý	černý	k2eAgInSc4d1	černý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyspělý	vyspělý	k2eAgMnSc1d1	vyspělý
jedinci	jedinec	k1gMnPc1	jedinec
s	s	k7c7	s
juvenilním	juvenilní	k2eAgInSc7d1	juvenilní
šatem	šat	k1gInSc7	šat
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
dospělým	dospělí	k1gMnPc3	dospělí
zdánlivě	zdánlivě	k6eAd1	zdánlivě
podobní	podobný	k2eAgMnPc1d1	podobný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zobák	zobák	k1gInSc1	zobák
a	a	k8xC	a
chocholky	chocholka	k1gFnPc1	chocholka
mají	mít	k5eAaImIp3nP	mít
stále	stále	k6eAd1	stále
nevýrazné	výrazný	k2eNgFnPc1d1	nevýrazná
<g/>
.	.	kIx.	.
</s>
<s>
Odlišně	odlišně	k6eAd1	odlišně
vypadají	vypadat	k5eAaPmIp3nP	vypadat
celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
chovu	chov	k1gInSc2	chov
<g/>
,	,	kIx,	,
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
než	než	k8xS	než
kolonii	kolonie	k1gFnSc4	kolonie
opustí	opustit	k5eAaPmIp3nP	opustit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
ostatní	ostatní	k2eAgMnSc1d1	ostatní
dospělý	dospělý	k1gMnSc1	dospělý
nebudou	být	k5eNaImBp3nP	být
chovat	chovat	k5eAaImF	chovat
agresivně	agresivně	k6eAd1	agresivně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
lovu	lov	k1gInSc2	lov
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
zdatný	zdatný	k2eAgMnSc1d1	zdatný
plavec	plavec	k1gMnSc1	plavec
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
živí	živit	k5eAaImIp3nS	živit
potravou	potrava	k1gFnSc7	potrava
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Primárně	primárně	k6eAd1	primárně
pak	pak	k6eAd1	pak
různými	různý	k2eAgMnPc7d1	různý
korýši	korýš	k1gMnPc7	korýš
a	a	k8xC	a
krilem	kril	k1gInSc7	kril
<g/>
,	,	kIx,	,
např.	např.	kA	např.
krunýřovkou	krunýřovka	k1gFnSc7	krunýřovka
krillovou	krillová	k1gFnSc7	krillová
(	(	kIx(	(
<g/>
Euphausia	Euphausia	k1gFnSc1	Euphausia
superba	superba	k1gFnSc1	superba
<g/>
)	)	kIx)	)
či	či	k8xC	či
druhem	druh	k1gInSc7	druh
Euphausia	Euphausium	k1gNnSc2	Euphausium
vallentini	vallentin	k2eAgMnPc1d1	vallentin
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
jídelníček	jídelníček	k1gInSc1	jídelníček
tvoří	tvořit	k5eAaImIp3nS	tvořit
ale	ale	k8xC	ale
i	i	k9	i
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
mořských	mořský	k2eAgFnPc2d1	mořská
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
Electronna	Electronen	k2eAgNnPc1d1	Electronen
carlesbergi	carlesbergi	k1gNnPc1	carlesbergi
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lampovníkovitých	lampovníkovitý	k2eAgMnPc2d1	lampovníkovitý
nebo	nebo	k8xC	nebo
Magnesudis	Magnesudis	k1gFnPc2	Magnesudis
prionosa	prionosa	k1gFnSc1	prionosa
<g/>
.	.	kIx.	.
</s>
<s>
Zřídka	zřídka	k6eAd1	zřídka
loví	lovit	k5eAaImIp3nP	lovit
taktéž	taktéž	k?	taktéž
chobotnice	chobotnice	k1gFnPc1	chobotnice
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
zástupce	zástupce	k1gMnSc1	zástupce
rodu	rod	k1gInSc2	rod
Moroteuthis	Moroteuthis	k1gFnSc2	Moroteuthis
<g/>
.	.	kIx.	.
<g/>
Tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
má	mít	k5eAaImIp3nS	mít
především	především	k9	především
vynikající	vynikající	k2eAgInSc1d1	vynikající
zrak	zrak	k1gInSc1	zrak
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yIgInSc7	který
vidí	vidět	k5eAaImIp3nS	vidět
i	i	k9	i
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
vodě	voda	k1gFnSc6	voda
prakticky	prakticky	k6eAd1	prakticky
za	za	k7c2	za
tmy	tma	k1gFnSc2	tma
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
rohovky	rohovka	k1gFnPc1	rohovka
jsou	být	k5eAaImIp3nP	být
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
zaostřuje	zaostřovat	k5eAaImIp3nS	zaostřovat
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
vnímá	vnímat	k5eAaImIp3nS	vnímat
pouze	pouze	k6eAd1	pouze
nuance	nuance	k1gFnSc1	nuance
takových	takový	k3xDgFnPc2	takový
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
převládají	převládat	k5eAaImIp3nP	převládat
<g/>
,	,	kIx,	,
především	především	k9	především
tedy	tedy	k9	tedy
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
modrou	modrý	k2eAgFnSc4d1	modrá
či	či	k8xC	či
fialovou	fialový	k2eAgFnSc4d1	fialová
<g/>
,	,	kIx,	,
a	a	k8xC	a
třeba	třeba	k6eAd1	třeba
červenou	červený	k2eAgFnSc7d1	červená
nerozeznává	rozeznávat	k5eNaImIp3nS	rozeznávat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
přizpůsobení	přizpůsobení	k1gNnSc3	přizpůsobení
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
orientuje	orientovat	k5eAaBmIp3nS	orientovat
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
od	od	k7c2	od
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
potopit	potopit	k5eAaPmF	potopit
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
větší	veliký	k2eAgFnSc2d2	veliký
jak	jak	k8xC	jak
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Predátoři	predátor	k1gMnPc1	predátor
a	a	k8xC	a
parazité	parazit	k1gMnPc1	parazit
===	===	k?	===
</s>
</p>
<p>
<s>
Dospělci	dospělec	k1gMnPc1	dospělec
nemají	mít	k5eNaImIp3nP	mít
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgNnSc4	žádný
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
ani	ani	k9	ani
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
invazivní	invazivní	k2eAgInPc1d1	invazivní
druhy	druh	k1gInPc1	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
vymýceni	vymýtit	k5eAaPmNgMnP	vymýtit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozoru	pozor	k1gInSc6	pozor
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
před	před	k7c7	před
některými	některý	k3yIgFnPc7	některý
druhy	druh	k1gInPc1	druh
kytovců	kytovec	k1gMnPc2	kytovec
nebo	nebo	k8xC	nebo
ploutvonožců	ploutvonožec	k1gMnPc2	ploutvonožec
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
ohrožovány	ohrožovat	k5eAaImNgInP	ohrožovat
mořskými	mořský	k2eAgMnPc7d1	mořský
dravými	dravý	k2eAgMnPc7d1	dravý
ptáky	pták	k1gMnPc7	pták
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
chaluhou	chaluha	k1gFnSc7	chaluha
subantarktickou	subantarktický	k2eAgFnSc7d1	subantarktický
(	(	kIx(	(
<g/>
Catharcts	Catharctsa	k1gFnPc2	Catharctsa
lonnbergi	lonnbergi	k6eAd1	lonnbergi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Tučňáci	tučňák	k1gMnPc1	tučňák
královští	královský	k2eAgMnPc1d1	královský
jsou	být	k5eAaImIp3nP	být
častými	častý	k2eAgMnPc7d1	častý
nositeli	nositel	k1gMnPc7	nositel
parazitů	parazit	k1gMnPc2	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgNnSc4d3	nejčastější
patří	patřit	k5eAaImIp3nS	patřit
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
vší	veš	k1gFnPc2	veš
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Austrogoniodes	Austrogoniodesa	k1gFnPc2	Austrogoniodesa
<g/>
,	,	kIx,	,
blechy	blecha	k1gFnSc2	blecha
(	(	kIx(	(
<g/>
Pagipsylla	Pagipsylla	k1gMnSc1	Pagipsylla
galliralli	galliralle	k1gFnSc4	galliralle
<g/>
)	)	kIx)	)
a	a	k8xC	a
klíšťata	klíště	k1gNnPc1	klíště
(	(	kIx(	(
<g/>
Ixodes	Ixodes	k1gInSc1	Ixodes
uriae	uria	k1gInSc2	uria
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
obzvláště	obzvláště	k6eAd1	obzvláště
na	na	k7c6	na
mladých	mladý	k2eAgMnPc6d1	mladý
ptácích	pták	k1gMnPc6	pták
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hnízd	hnízdo	k1gNnPc2	hnízdo
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnPc1	rozmnožování
a	a	k8xC	a
chov	chov	k1gInSc1	chov
mláďat	mládě	k1gNnPc2	mládě
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
ostrova	ostrov	k1gInSc2	ostrov
Maquarie	Maquarie	k1gFnSc2	Maquarie
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
objevuje	objevovat	k5eAaImIp3nS	objevovat
začátkem	začátkem	k7c2	začátkem
října	říjen	k1gInSc2	říjen
nebo	nebo	k8xC	nebo
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
sejde	sejít	k5eAaPmIp3nS	sejít
obrovská	obrovský	k2eAgFnSc1d1	obrovská
kolonie	kolonie	k1gFnSc1	kolonie
tučňáků	tučňák	k1gMnPc2	tučňák
(	(	kIx(	(
<g/>
o	o	k7c4	o
150	[number]	k4	150
000	[number]	k4	000
až	až	k9	až
500	[number]	k4	500
000	[number]	k4	000
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
však	však	k9	však
jen	jen	k6eAd1	jen
samců	samec	k1gMnPc2	samec
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
dorazí	dorazit	k5eAaPmIp3nP	dorazit
asi	asi	k9	asi
o	o	k7c4	o
10	[number]	k4	10
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvotní	prvotní	k2eAgFnSc7d1	prvotní
úlohou	úloha	k1gFnSc7	úloha
samců	samec	k1gMnPc2	samec
je	být	k5eAaImIp3nS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
předhánějí	předhánět	k5eAaImIp3nP	předhánět
v	v	k7c6	v
zabrání	zabrání	k1gNnSc6	zabrání
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
míst	místo	k1gNnPc2	místo
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
takové	takový	k3xDgNnSc4	takový
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
středu	střed	k1gInSc6	střed
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
hnízda	hnízdo	k1gNnPc1	hnízdo
nejbezpečnější	bezpečný	k2eAgNnPc1d3	nejbezpečnější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nacházet	nacházet	k5eAaImF	nacházet
až	až	k9	až
téměř	téměř	k6eAd1	téměř
dva	dva	k4xCgInPc4	dva
kilometry	kilometr	k1gInPc4	kilometr
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Obhájené	obhájený	k2eAgNnSc4d1	obhájené
hnízdo	hnízdo	k1gNnSc4	hnízdo
následně	následně	k6eAd1	následně
vystelou	vystlat	k5eAaPmIp3nP	vystlat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dle	dle	k7c2	dle
svých	svůj	k3xOyFgFnPc2	svůj
zkušeností	zkušenost	k1gFnPc2	zkušenost
a	a	k8xC	a
dostupného	dostupný	k2eAgInSc2d1	dostupný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vyseděnou	vyseděný	k2eAgFnSc4d1	vyseděný
prohlubeň	prohlubeň	k1gFnSc4	prohlubeň
tak	tak	k6eAd1	tak
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
různé	různý	k2eAgFnPc1d1	různá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
větvičky	větvička	k1gFnPc1	větvička
nebo	nebo	k8xC	nebo
třeba	třeba	k9	třeba
kameny	kámen	k1gInPc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Neustále	neustále	k6eAd1	neustále
hnízdo	hnízdo	k1gNnSc4	hnízdo
přerovnávají	přerovnávat	k5eAaImIp3nP	přerovnávat
a	a	k8xC	a
vylepšují	vylepšovat	k5eAaImIp3nP	vylepšovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
,	,	kIx,	,
než	než	k8xS	než
jej	on	k3xPp3gMnSc4	on
zhotoví	zhotovit	k5eAaPmIp3nP	zhotovit
do	do	k7c2	do
úplné	úplný	k2eAgFnSc2d1	úplná
spokojenosti	spokojenost	k1gFnSc2	spokojenost
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnou	skutečný	k2eAgFnSc4d1	skutečná
stopku	stopka	k1gFnSc4	stopka
zajistí	zajistit	k5eAaPmIp3nP	zajistit
až	až	k9	až
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
si	se	k3xPyFc3	se
všimnou	všimnout	k5eAaPmIp3nP	všimnout
ještě	ještě	k9	ještě
v	v	k7c6	v
dáli	dál	k1gFnSc6	dál
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
danou	daný	k2eAgFnSc4d1	daná
chvíli	chvíle	k1gFnSc4	chvíle
samci	samec	k1gMnSc3	samec
kolektivně	kolektivně	k6eAd1	kolektivně
zpozorní	zpozornět	k5eAaPmIp3nS	zpozornět
<g/>
,	,	kIx,	,
a	a	k8xC	a
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
samice	samice	k1gFnPc1	samice
kolébají	kolébat	k5eAaImIp3nP	kolébat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hnízdišti	hnízdiště	k1gNnSc3	hnízdiště
<g/>
,	,	kIx,	,
samci	samec	k1gMnSc3	samec
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
náruživě	náruživě	k6eAd1	náruživě
volají	volat	k5eAaImIp3nP	volat
<g/>
.	.	kIx.	.
</s>
<s>
Vzpřímeně	vzpřímeně	k6eAd1	vzpřímeně
stojí	stát	k5eAaImIp3nS	stát
u	u	k7c2	u
svého	svůj	k3xOyFgNnSc2	svůj
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
zvláštně	zvláštně	k6eAd1	zvláštně
kývají	kývat	k5eAaImIp3nP	kývat
nataženou	natažený	k2eAgFnSc7d1	natažená
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
třepetají	třepetat	k5eAaImIp3nP	třepetat
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
vidět	vidět	k5eAaImF	vidět
a	a	k8xC	a
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
signály	signál	k1gInPc1	signál
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
hlasitým	hlasitý	k2eAgNnSc7d1	hlasité
troubením	troubení	k1gNnSc7	troubení
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
samice	samice	k1gFnSc1	samice
vábí	vábit	k5eAaImIp3nS	vábit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
shledávají	shledávat	k5eAaImIp3nP	shledávat
konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
družky	družka	k1gFnPc1	družka
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
se	se	k3xPyFc4	se
pářili	pářit	k5eAaImAgMnP	pářit
loni	loni	k6eAd1	loni
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
přísně	přísně	k6eAd1	přísně
monogamní	monogamní	k2eAgNnSc1d1	monogamní
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
hlasům	hlas	k1gInPc3	hlas
bedlivě	bedlivě	k6eAd1	bedlivě
naslouchají	naslouchat	k5eAaImIp3nP	naslouchat
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
svého	svůj	k3xOyFgMnSc4	svůj
partnera	partner	k1gMnSc4	partner
rozpoznají	rozpoznat	k5eAaPmIp3nP	rozpoznat
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
jakousi	jakýsi	k3yIgFnSc7	jakýsi
navigací	navigace	k1gFnSc7	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
neutiší	utišit	k5eNaPmIp3nS	utišit
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
partnerka	partnerka	k1gFnSc1	partnerka
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
přímo	přímo	k6eAd1	přímo
naproti	naproti	k7c3	naproti
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
tak	tak	k6eAd1	tak
neustále	neustále	k6eAd1	neustále
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
své	svůj	k3xOyFgInPc4	svůj
osobní	osobní	k2eAgInPc4d1	osobní
údaje	údaj	k1gInPc4	údaj
–	–	k?	–
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
jakého	jaký	k3yRgInSc2	jaký
je	být	k5eAaImIp3nS	být
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgInS	připravit
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
takový	takový	k3xDgInSc4	takový
hluk	hluk	k1gInSc4	hluk
<g/>
.	.	kIx.	.
</s>
<s>
Námluvy	námluva	k1gFnPc1	námluva
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
celkem	celkem	k6eAd1	celkem
krátkodobé	krátkodobý	k2eAgNnSc1d1	krátkodobé
<g/>
.	.	kIx.	.
</s>
<s>
Nalezený	nalezený	k2eAgInSc1d1	nalezený
pár	pár	k1gInSc1	pár
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
uvítá	uvítat	k5eAaPmIp3nS	uvítat
<g/>
,	,	kIx,	,
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
se	se	k3xPyFc4	se
o	o	k7c6	o
původu	původ	k1gInSc6	původ
partnera	partner	k1gMnSc2	partner
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
se	se	k3xPyFc4	se
zanedlouho	zanedlouho	k6eAd1	zanedlouho
pářit	pářit	k5eAaImF	pářit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
říjen	říjen	k1gInSc1	říjen
snese	snést	k5eAaPmIp3nS	snést
samice	samice	k1gFnSc2	samice
asi	asi	k9	asi
dvě	dva	k4xCgNnPc4	dva
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Prvé	prvý	k4xOgNnSc1	prvý
snesené	snesený	k2eAgNnSc1d1	snesené
vejce	vejce	k1gNnSc1	vejce
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
asi	asi	k9	asi
100	[number]	k4	100
gramů	gram	k1gInPc2	gram
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
patrně	patrně	k6eAd1	patrně
menší	malý	k2eAgNnSc1d2	menší
než	než	k8xS	než
druhé	druhý	k4xOgNnSc1	druhý
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
až	až	k9	až
160	[number]	k4	160
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nevšední	všední	k2eNgInSc1d1	nevšední
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
nebývá	bývat	k5eNaImIp3nS	bývat
první	první	k4xOgNnSc1	první
vejce	vejce	k1gNnSc1	vejce
ani	ani	k8xC	ani
oplodněné	oplodněný	k2eAgNnSc1d1	oplodněné
<g/>
,	,	kIx,	,
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
upoutání	upoutání	k1gNnSc3	upoutání
predátorů	predátor	k1gMnPc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
jej	on	k3xPp3gMnSc4	on
kolikrát	kolikrát	k6eAd1	kolikrát
zcela	zcela	k6eAd1	zcela
vědomě	vědomě	k6eAd1	vědomě
vyhodí	vyhodit	k5eAaPmIp3nP	vyhodit
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Opuštěných	opuštěný	k2eAgNnPc2d1	opuštěné
vajec	vejce	k1gNnPc2	vejce
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
nespočet	nespočet	k1gInSc1	nespočet
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
draví	dravý	k2eAgMnPc1d1	dravý
ptáci	pták	k1gMnPc1	pták
této	tento	k3xDgFnSc2	tento
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
kořisti	kořist	k1gFnSc2	kořist
nepohrdnou	pohrdnout	k5eNaPmIp3nP	pohrdnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
snesené	snesený	k2eAgNnSc4d1	snesené
druhé	druhý	k4xOgNnSc4	druhý
vejce	vejce	k1gNnSc4	vejce
pak	pak	k6eAd1	pak
nemají	mít	k5eNaImIp3nP	mít
ani	ani	k8xC	ani
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
už	už	k6eAd1	už
ani	ani	k8xC	ani
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
inkubována	inkubovat	k5eAaImNgNnP	inkubovat
zhruba	zhruba	k6eAd1	zhruba
35	[number]	k4	35
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dny	den	k1gInPc1	den
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
hnízda	hnízdo	k1gNnSc2	hnízdo
přítomny	přítomen	k2eAgInPc4d1	přítomen
oba	dva	k4xCgInPc4	dva
z	z	k7c2	z
páru	pár	k1gInSc2	pár
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
ten	ten	k3xDgInSc1	ten
který	který	k3yRgInSc4	který
nesedí	sedit	k5eNaImIp3nS	sedit
svého	svůj	k3xOyFgMnSc4	svůj
partnera	partner	k1gMnSc4	partner
pouze	pouze	k6eAd1	pouze
střeží	střežit	k5eAaImIp3nS	střežit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
sedí	sedit	k5eAaImIp3nS	sedit
zpravidla	zpravidla	k6eAd1	zpravidla
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
současně	současně	k6eAd1	současně
po	po	k7c6	po
nakladení	nakladení	k1gNnSc6	nakladení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
pozici	pozice	k1gFnSc4	pozice
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
lovit	lovit	k5eAaImF	lovit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gInSc4	samec
zatím	zatím	k6eAd1	zatím
vejce	vejce	k1gNnSc1	vejce
udržuje	udržovat	k5eAaImIp3nS	udržovat
v	v	k7c6	v
teple	teplo	k1gNnSc6	teplo
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nP	držet
je	on	k3xPp3gFnPc4	on
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
ploše	plocha	k1gFnSc6	plocha
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
pod	pod	k7c7	pod
kožními	kožní	k2eAgInPc7d1	kožní
záhyby	záhyb	k1gInPc7	záhyb
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Nepřijímá	přijímat	k5eNaImIp3nS	přijímat
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
tukových	tukový	k2eAgFnPc2d1	tuková
zásob	zásoba	k1gFnPc2	zásoba
svého	své	k1gNnSc2	své
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
za	za	k7c4	za
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
samice	samice	k1gFnPc1	samice
vrátí	vrátit	k5eAaPmIp3nP	vrátit
a	a	k8xC	a
úlohy	úloha	k1gFnPc1	úloha
si	se	k3xPyFc3	se
vymění	vyměnit	k5eAaPmIp3nP	vyměnit
<g/>
,	,	kIx,	,
samec	samec	k1gInSc1	samec
jde	jít	k5eAaImIp3nS	jít
hledat	hledat	k5eAaImF	hledat
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vylíhnutí	vylíhnutí	k1gNnSc2	vylíhnutí
pak	pak	k6eAd1	pak
bývají	bývat	k5eAaImIp3nP	bývat
opět	opět	k6eAd1	opět
pospolu	pospolu	k6eAd1	pospolu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
i	i	k9	i
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
dnů	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
páru	pár	k1gInSc2	pár
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
loví	lovit	k5eAaImIp3nS	lovit
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
(	(	kIx(	(
<g/>
po	po	k7c6	po
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
dnech	den	k1gInPc6	den
<g/>
)	)	kIx)	)
potomky	potomek	k1gMnPc4	potomek
opouštějí	opouštět	k5eAaImIp3nP	opouštět
a	a	k8xC	a
ponechávají	ponechávat	k5eAaImIp3nP	ponechávat
je	on	k3xPp3gNnSc4	on
v	v	k7c6	v
takzvaných	takzvaný	k2eAgFnPc6d1	takzvaná
školkách	školka	k1gFnPc6	školka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
skupinka	skupinka	k1gFnSc1	skupinka
převážně	převážně	k6eAd1	převážně
stejně	stejně	k6eAd1	stejně
starých	starý	k2eAgNnPc2d1	staré
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
udržují	udržovat	k5eAaImIp3nP	udržovat
těsně	těsně	k6eAd1	těsně
pospolu	pospolu	k6eAd1	pospolu
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
získá	získat	k5eAaPmIp3nS	získat
více	hodně	k6eAd2	hodně
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
predátory	predátor	k1gMnPc7	predátor
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
před	před	k7c7	před
dotíravými	dotíravý	k2eAgMnPc7d1	dotíravý
dravými	dravý	k2eAgMnPc7d1	dravý
ptáky	pták	k1gMnPc7	pták
<g/>
.	.	kIx.	.
</s>
<s>
Krmeni	krmen	k2eAgMnPc1d1	krmen
jsou	být	k5eAaImIp3nP	být
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
až	až	k8xS	až
třetí	třetí	k4xOgInSc4	třetí
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
už	už	k6eAd1	už
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
volají	volat	k5eAaImIp3nP	volat
a	a	k8xC	a
vyčkávají	vyčkávat	k5eAaImIp3nP	vyčkávat
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
odezvu	odezva	k1gFnSc4	odezva
<g/>
.	.	kIx.	.
</s>
<s>
Rozpozná	rozpoznat	k5eAaPmIp3nS	rozpoznat
<g/>
-li	i	k?	-li
mládě	mládě	k1gNnSc1	mládě
jejich	jejich	k3xOp3gInPc7	jejich
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
hladové	hladový	k2eAgFnPc4d1	hladová
pak	pak	k6eAd1	pak
nedočkavostí	nedočkavost	k1gFnSc7	nedočkavost
cupitá	cupitat	k5eAaImIp3nS	cupitat
naproti	naproti	k7c3	naproti
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
juvenilního	juvenilní	k2eAgMnSc2d1	juvenilní
plně	plně	k6eAd1	plně
funkčního	funkční	k2eAgNnSc2d1	funkční
peří	peří	k1gNnSc2	peří
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
dostanou	dostat	k5eAaPmIp3nP	dostat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
65	[number]	k4	65
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolonii	kolonie	k1gFnSc4	kolonie
za	za	k7c4	za
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
opustí	opustit	k5eAaPmIp3nS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
moři	moře	k1gNnSc6	moře
pohlavně	pohlavně	k6eAd1	pohlavně
vyzrají	vyzrát	k5eAaPmIp3nP	vyzrát
za	za	k7c4	za
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
o	o	k7c4	o
vlastní	vlastní	k2eAgInSc4d1	vlastní
chov	chov	k1gInSc4	chov
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Pokusí	pokusit	k5eAaPmIp3nS	pokusit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zpravidla	zpravidla	k6eAd1	zpravidla
vždy	vždy	k6eAd1	vždy
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
asi	asi	k9	asi
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
neexistují	existovat	k5eNaImIp3nP	existovat
však	však	k9	však
spolehlivé	spolehlivý	k2eAgInPc1d1	spolehlivý
záznamy	záznam	k1gInPc1	záznam
<g/>
.	.	kIx.	.
<g/>
Dospěly	dochvít	k5eAaPmAgInP	dochvít
se	se	k3xPyFc4	se
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
vykrmují	vykrmovat	k5eAaImIp3nP	vykrmovat
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
přepeřit	přepeřit	k5eAaPmF	přepeřit
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
tak	tak	k9	tak
dalších	další	k2eAgInPc2d1	další
25	[number]	k4	25
dní	den	k1gInPc2	den
hladoví	hladovět	k5eAaImIp3nS	hladovět
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oceáně	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
<g/>
,	,	kIx,	,
do	do	k7c2	do
období	období	k1gNnSc2	období
dalšího	další	k2eAgNnSc2d1	další
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
IUCN	IUCN	kA	IUCN
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c7	mezi
druhy	druh	k1gInPc7	druh
téměř	téměř	k6eAd1	téměř
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
<g/>
;	;	kIx,	;
dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
loveny	lovit	k5eAaImNgFnP	lovit
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
olej	olej	k1gInSc4	olej
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
Tasmánská	tasmánský	k2eAgFnSc1d1	tasmánská
vláda	vláda	k1gFnSc1	vláda
vydala	vydat	k5eAaPmAgFnS	vydat
licenci	licence	k1gFnSc4	licence
povolující	povolující	k2eAgInSc4d1	povolující
lov	lov	k1gInSc4	lov
tučňáků	tučňák	k1gMnPc2	tučňák
královských	královský	k2eAgInPc2d1	královský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
určitého	určitý	k2eAgNnSc2d1	určité
množství	množství	k1gNnSc2	množství
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
maximálně	maximálně	k6eAd1	maximálně
150	[number]	k4	150
000	[number]	k4	000
tučňáků	tučňák	k1gMnPc2	tučňák
na	na	k7c4	na
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
této	tento	k3xDgFnSc2	tento
vyhlášky	vyhláška	k1gFnSc2	vyhláška
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
populace	populace	k1gFnSc1	populace
tučňáků	tučňák	k1gMnPc2	tučňák
královských	královský	k2eAgMnPc2d1	královský
obnovila	obnovit	k5eAaPmAgFnS	obnovit
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Macquarie	Macquarie	k1gFnSc1	Macquarie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
zhruba	zhruba	k6eAd1	zhruba
850	[number]	k4	850
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
populace	populace	k1gFnSc1	populace
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
a	a	k8xC	a
dle	dle	k7c2	dle
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
svazu	svaz	k1gInSc2	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
pravidelně	pravidelně	k6eAd1	pravidelně
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
ostrově	ostrov	k1gInSc6	ostrov
obdobný	obdobný	k2eAgInSc1d1	obdobný
počet	počet	k1gInSc1	počet
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
<g/>
Tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
není	být	k5eNaImIp3nS	být
ohrožen	ohrožen	k2eAgMnSc1d1	ohrožen
žádnou	žádný	k3yNgFnSc7	žádný
přímou	přímý	k2eAgFnSc7d1	přímá
hrozbou	hrozba	k1gFnSc7	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Invazivní	invazivní	k2eAgInPc1d1	invazivní
druhy	druh	k1gInPc1	druh
živočichů	živočich	k1gMnPc2	živočich
byli	být	k5eAaImAgMnP	být
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
zdejší	zdejší	k2eAgFnSc2d1	zdejší
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
flóry	flóra	k1gFnSc2	flóra
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
odstraněny	odstraněn	k2eAgMnPc4d1	odstraněn
<g/>
;	;	kIx,	;
divoké	divoký	k2eAgFnSc2d1	divoká
kočky	kočka	k1gFnSc2	kočka
(	(	kIx(	(
<g/>
Felis	Felis	k1gFnSc1	Felis
catus	catus	k1gMnSc1	catus
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
nepůvodní	původní	k2eNgMnPc1d1	nepůvodní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
jako	jako	k8xC	jako
krysa	krysa	k1gFnSc1	krysa
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Rattus	Rattus	k1gMnSc1	Rattus
rattus	rattus	k1gMnSc1	rattus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chřástal	chřástal	k1gMnSc1	chřástal
weka	weka	k1gMnSc1	weka
(	(	kIx(	(
<g/>
Galliralus	Galliralus	k1gMnSc1	Galliralus
australis	australis	k1gFnSc2	australis
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
či	či	k8xC	či
králík	králík	k1gMnSc1	králík
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Oryctolagus	Oryctolagus	k1gMnSc1	Oryctolagus
cuniculus	cuniculus	k1gMnSc1	cuniculus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
pesticidů	pesticid	k1gInPc2	pesticid
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
může	moct	k5eAaImIp3nS	moct
populaci	populace	k1gFnSc4	populace
těchto	tento	k3xDgFnPc2	tento
tučňáku	tučňák	k1gMnSc3	tučňák
ohrozit	ohrozit	k5eAaPmF	ohrozit
kolísává	kolísávat	k5eAaImIp3nS	kolísávat
potravní	potravní	k2eAgFnSc1d1	potravní
dostupnost	dostupnost	k1gFnSc1	dostupnost
vlivem	vliv	k1gInSc7	vliv
komerčních	komerční	k2eAgInPc2d1	komerční
rybolovů	rybolov	k1gInPc2	rybolov
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
a	a	k8xC	a
následných	následný	k2eAgFnPc2d1	následná
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řáde	řád	k1gInSc5	řád
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
hrozbu	hrozba	k1gFnSc4	hrozba
turistický	turistický	k2eAgInSc1d1	turistický
rozmach	rozmach	k1gInSc1	rozmach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
opakovaně	opakovaně	k6eAd1	opakovaně
narušoval	narušovat	k5eAaImAgInS	narušovat
přirozený	přirozený	k2eAgInSc1d1	přirozený
běh	běh	k1gInSc1	běh
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Royal	Royal	k1gMnSc1	Royal
Penguin	Penguin	k1gMnSc1	Penguin
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
WILLIAMS	WILLIAMS	kA	WILLIAMS
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
D.	D.	kA	D.
<g/>
;	;	kIx,	;
WILSON	WILSON	kA	WILSON
<g/>
,	,	kIx,	,
Rory	Rory	k1gInPc7	Rory
P.	P.	kA	P.
The	The	k1gMnPc2	The
penguins	penguinsa	k1gFnPc2	penguinsa
:	:	kIx,	:
Spheniscidae	Spheniscidae	k1gFnPc2	Spheniscidae
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Bird	Bird	k1gMnSc1	Bird
Families	Families	k1gMnSc1	Families
of	of	k?	of
the	the	k?	the
World	World	k1gMnSc1	World
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
19854667	[number]	k4	19854667
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
212	[number]	k4	212
-	-	kIx~	-
223	[number]	k4	223
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
