<p>
<s>
IBEX	IBEX	kA	IBEX
(	(	kIx(	(
<g/>
Interstellar	Interstellar	k1gMnSc1	Interstellar
Boundary	Boundara	k1gFnSc2	Boundara
Explorer	Explorer	k1gMnSc1	Explorer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
Explorer	Explorer	k1gInSc1	Explorer
91	[number]	k4	91
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
sonda	sonda	k1gFnSc1	sonda
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
a	a	k8xC	a
která	který	k3yIgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
a	a	k8xC	a
mezihvězdným	mezihvězdný	k2eAgInSc7d1	mezihvězdný
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úkoly	úkol	k1gInPc1	úkol
mise	mise	k1gFnSc2	mise
==	==	k?	==
</s>
</p>
<p>
<s>
Sonda	sonda	k1gFnSc1	sonda
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
starší	starší	k1gMnPc4	starší
mise	mise	k1gFnSc2	mise
Voyager	Voyager	k1gInSc4	Voyager
1	[number]	k4	1
a	a	k8xC	a
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
bude	být	k5eAaImBp3nS	být
předávat	předávat	k5eAaImF	předávat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
naší	náš	k3xOp1gFnSc2	náš
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hledat	hledat	k5eAaImF	hledat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
případného	případný	k2eAgInSc2d1	případný
ochranného	ochranný	k2eAgInSc2d1	ochranný
štítu	štít	k1gInSc2	štít
před	před	k7c7	před
nebezpečným	bezpečný	k2eNgNnSc7d1	nebezpečné
zářením	záření	k1gNnSc7	záření
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
protitlak	protitlak	k1gInSc4	protitlak
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
velmi	velmi	k6eAd1	velmi
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
obou	dva	k4xCgFnPc2	dva
misí	mise	k1gFnPc2	mise
Voyagerů	Voyager	k1gInPc2	Voyager
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
míří	mířit	k5eAaImIp3nS	mířit
mimo	mimo	k6eAd1	mimo
naší	náš	k3xOp1gFnSc7	náš
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
IBEX	IBEX	kA	IBEX
nevzdálí	vzdálit	k5eNaPmIp3nS	vzdálit
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
na	na	k7c4	na
tří-čtvrtinovou	tří-čtvrtinový	k2eAgFnSc4d1	tří-čtvrtinový
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mapování	mapování	k1gNnSc1	mapování
hranice	hranice	k1gFnSc2	hranice
heliosféry	heliosféra	k1gFnSc2	heliosféra
je	být	k5eAaImIp3nS	být
prováděno	provádět	k5eAaImNgNnS	provádět
pomocí	pomocí	k7c2	pomocí
takzvaných	takzvaný	k2eAgInPc2d1	takzvaný
energetických	energetický	k2eAgInPc2d1	energetický
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
výměně	výměna	k1gFnSc6	výměna
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
částicemi	částice	k1gFnPc7	částice
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
atomy	atom	k1gInPc1	atom
poté	poté	k6eAd1	poté
cestují	cestovat	k5eAaImIp3nP	cestovat
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
našemu	náš	k3xOp1gNnSc3	náš
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
cestě	cesta	k1gFnSc6	cesta
je	být	k5eAaImIp3nS	být
IBEX	IBEX	kA	IBEX
detekuje	detekovat	k5eAaImIp3nS	detekovat
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
mapu	mapa	k1gFnSc4	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
sondy	sonda	k1gFnSc2	sonda
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc3d1	základní
konstrukci	konstrukce	k1gFnSc3	konstrukce
sondy	sonda	k1gFnSc2	sonda
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
společnost	společnost	k1gFnSc1	společnost
Orbital	orbital	k1gInSc4	orbital
Science	Science	k1gFnSc2	Science
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
výrobcem	výrobce	k1gMnSc7	výrobce
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
Pegasus	Pegasus	k1gMnSc1	Pegasus
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
vědeckými	vědecký	k2eAgInPc7d1	vědecký
nástroji	nástroj	k1gInPc7	nástroj
je	být	k5eAaImIp3nS	být
dvojice	dvojice	k1gFnSc1	dvojice
detektorů	detektor	k1gInPc2	detektor
energetických	energetický	k2eAgInPc2d1	energetický
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
detektor	detektor	k1gInSc1	detektor
je	být	k5eAaImIp3nS	být
citlivý	citlivý	k2eAgInSc1d1	citlivý
na	na	k7c4	na
částice	částice	k1gFnPc4	částice
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
energií	energie	k1gFnSc7	energie
(	(	kIx(	(
<g/>
300	[number]	k4	300
eV	eV	k?	eV
až	až	k9	až
6	[number]	k4	6
keV	keV	k?	keV
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
na	na	k7c6	na
částice	částice	k1gFnPc1	částice
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
energií	energie	k1gFnSc7	energie
(	(	kIx(	(
<g/>
10	[number]	k4	10
eV	eV	k?	eV
až	až	k9	až
2	[number]	k4	2
keV	keV	k?	keV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
dokáží	dokázat	k5eAaPmIp3nP	dokázat
zaznamenávat	zaznamenávat	k5eAaImF	zaznamenávat
časy	čas	k1gInPc1	čas
<g/>
,	,	kIx,	,
směry	směr	k1gInPc1	směr
<g/>
,	,	kIx,	,
hmotnosti	hmotnost	k1gFnPc1	hmotnost
a	a	k8xC	a
energie	energie	k1gFnPc1	energie
detekovaných	detekovaný	k2eAgInPc2d1	detekovaný
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
detektorů	detektor	k1gInPc2	detektor
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
Lockheed	Lockheed	k1gInSc4	Lockheed
Martin	Martin	k1gMnSc1	Martin
Advanced	Advanced	k1gMnSc1	Advanced
Technology	technolog	k1gMnPc7	technolog
Center	centrum	k1gNnPc2	centrum
a	a	k8xC	a
Los	los	k1gMnSc1	los
Alamos	Alamos	k1gMnSc1	Alamos
National	National	k1gFnSc2	National
Laboratory	Laborator	k1gInPc4	Laborator
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Řízení	řízení	k1gNnSc1	řízení
mise	mise	k1gFnSc2	mise
==	==	k?	==
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
svěřeno	svěřen	k2eAgNnSc1d1	svěřeno
oddělení	oddělení	k1gNnSc1	oddělení
vědeckého	vědecký	k2eAgInSc2d1	vědecký
výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c4	v
Southwest	Southwest	k1gFnSc4	Southwest
Research	Researcha	k1gFnPc2	Researcha
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
San	San	k1gMnSc6	San
Antoniu	Antonio	k1gMnSc6	Antonio
(	(	kIx(	(
<g/>
USA	USA	kA	USA
-	-	kIx~	-
stát	stát	k1gInSc1	stát
Texas	Texas	k1gInSc1	Texas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
mise	mise	k1gFnSc2	mise
==	==	k?	==
</s>
</p>
<p>
<s>
Sondu	sonda	k1gFnSc4	sonda
vypustil	vypustit	k5eAaPmAgInS	vypustit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
raketou	raketa	k1gFnSc7	raketa
Pegasus	Pegasus	k1gMnSc1	Pegasus
XL	XL	kA	XL
/	/	kIx~	/
<g/>
Star	Star	kA	Star
27	[number]	k4	27
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
12	[number]	k4	12
km	km	kA	km
nad	nad	k7c7	nad
Zemí	zem	k1gFnSc7	zem
letoun	letoun	k1gMnSc1	letoun
Lockheed	Lockheed	k1gMnSc1	Lockheed
L-1011	L-1011	k1gMnSc1	L-1011
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
letu	let	k1gInSc3	let
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
z	z	k7c2	z
atolu	atol	k1gInSc2	atol
Kwajalein	Kwajaleina	k1gFnPc2	Kwajaleina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
startu	start	k1gInSc6	start
bylo	být	k5eAaImAgNnS	být
sondě	sonda	k1gFnSc6	sonda
agenturou	agentura	k1gFnSc7	agentura
COSPAR	COSPAR	kA	COSPAR
přiděleno	přidělen	k2eAgNnSc1d1	přiděleno
označení	označení	k1gNnSc1	označení
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
51	[number]	k4	51
<g/>
A.	A.	kA	A.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
byla	být	k5eAaImAgFnS	být
sonda	sonda	k1gFnSc1	sonda
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c4	na
silně	silně	k6eAd1	silně
eliptickou	eliptický	k2eAgFnSc4d1	eliptická
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
nízkým	nízký	k2eAgNnSc7d1	nízké
perigeem	perigeum	k1gNnSc7	perigeum
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
apogeu	apogeum	k1gNnSc6	apogeum
zažehnut	zažehnut	k2eAgInSc1d1	zažehnut
motor	motor	k1gInSc1	motor
na	na	k7c4	na
tuhá	tuhý	k2eAgNnPc4d1	tuhé
paliva	palivo	k1gNnPc4	palivo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
i	i	k9	i
perigeum	perigeum	k1gNnSc4	perigeum
a	a	k8xC	a
navedl	navést	k5eAaPmAgMnS	navést
sondu	sonda	k1gFnSc4	sonda
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
vysokou	vysoký	k2eAgFnSc4d1	vysoká
eliptickou	eliptický	k2eAgFnSc4d1	eliptická
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konečná	konečný	k2eAgFnSc1d1	konečná
dráha	dráha	k1gFnSc1	dráha
IBEXu	IBEXus	k1gInSc2	IBEXus
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
eliptická	eliptický	k2eAgFnSc1d1	eliptická
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Země	zem	k1gFnSc2	zem
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
excentricitou	excentricita	k1gFnSc7	excentricita
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
výška	výška	k1gFnSc1	výška
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
perigea	perigeum	k1gNnSc2	perigeum
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
6000	[number]	k4	6000
až	až	k9	až
11	[number]	k4	11
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
apogeum	apogeum	k1gNnSc4	apogeum
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
až	až	k9	až
300	[number]	k4	300
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
tři-čtvrtiny	tři-čtvrtin	k2eAgFnPc4d1	tři-čtvrtin
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
mezi	mezi	k7c7	mezi
Zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
Měsícem	měsíc	k1gInSc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
orbitální	orbitální	k2eAgNnPc1d1	orbitální
data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
odporu	odpor	k1gInSc2	odpor
atmosféry	atmosféra	k1gFnSc2	atmosféra
v	v	k7c6	v
perigeu	perigeum	k1gNnSc6	perigeum
<g/>
,	,	kIx,	,
a	a	k8xC	a
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
turbulencí	turbulence	k1gFnPc2	turbulence
z	z	k7c2	z
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
kosmických	kosmický	k2eAgNnPc2d1	kosmické
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
apogeum	apogeum	k1gNnSc1	apogeum
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
sondě	sonda	k1gFnSc3	sonda
opustit	opustit	k5eAaPmF	opustit
magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
Země	zem	k1gFnSc2	zem
i	i	k8xC	i
Van	vana	k1gFnPc2	vana
Allenovy	Allenův	k2eAgInPc1d1	Allenův
pásy	pás	k1gInPc1	pás
a	a	k8xC	a
provádět	provádět	k5eAaImF	provádět
měření	měření	k1gNnSc4	měření
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
rušivých	rušivý	k2eAgInPc2d1	rušivý
vlivů	vliv	k1gInPc2	vliv
od	od	k7c2	od
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
již	již	k6eAd1	již
neprobíhají	probíhat	k5eNaImIp3nP	probíhat
měření	měření	k1gNnPc1	měření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sonda	sonda	k1gFnSc1	sonda
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
jiným	jiný	k2eAgFnPc3d1	jiná
činnostem	činnost	k1gFnPc3	činnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
odesílání	odesílání	k1gNnSc1	odesílání
nasbíraných	nasbíraný	k2eAgNnPc2d1	nasbírané
dat	datum	k1gNnPc2	datum
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
měření	měření	k1gNnSc1	měření
==	==	k?	==
</s>
</p>
<p>
<s>
Prvotní	prvotní	k2eAgInPc1d1	prvotní
údaje	údaj	k1gInPc1	údaj
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
neočekávaný	očekávaný	k2eNgInSc1d1	neočekávaný
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
tenký	tenký	k2eAgInSc4d1	tenký
pruh	pruh	k1gInSc4	pruh
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
energetických	energetický	k2eAgInPc2d1	energetický
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
dva	dva	k4xCgMnPc1	dva
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jinde	jinde	k6eAd1	jinde
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pruh	pruh	k1gInSc1	pruh
se	se	k3xPyFc4	se
nenachází	nacházet	k5eNaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nazývané	nazývaný	k2eAgFnSc6d1	nazývaná
bow	bow	k?	bow
shock	shock	k6eAd1	shock
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
heliosféry	heliosféra	k1gFnSc2	heliosféra
a	a	k8xC	a
mezihvězdného	mezihvězdný	k2eAgNnSc2d1	mezihvězdné
média	médium	k1gNnSc2	médium
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
mezihvězdným	mezihvězdný	k2eAgInSc7d1	mezihvězdný
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zatím	zatím	k6eAd1	zatím
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
je	být	k5eAaImIp3nS	být
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hustota	hustota	k1gFnSc1	hustota
energetických	energetický	k2eAgInPc2d1	energetický
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
způsobena	způsoben	k2eAgFnSc1d1	způsobena
interakci	interakce	k1gFnSc3	interakce
mezi	mezi	k7c7	mezi
heliosférou	heliosféra	k1gFnSc7	heliosféra
a	a	k8xC	a
lokálním	lokální	k2eAgNnSc7d1	lokální
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Slunce	slunce	k1gNnSc2	slunce
deformuje	deformovat	k5eAaImIp3nS	deformovat
siločáry	siločára	k1gFnPc4	siločára
galaktického	galaktický	k2eAgNnSc2d1	Galaktické
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
stlačování	stlačování	k1gNnSc3	stlačování
heliosféry	heliosféra	k1gFnSc2	heliosféra
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
podstata	podstata	k1gFnSc1	podstata
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měření	měření	k1gNnSc1	měření
IBEXu	IBEXus	k1gInSc2	IBEXus
jsou	být	k5eAaImIp3nP	být
porovnávána	porovnáván	k2eAgNnPc1d1	porovnáváno
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
odeslanými	odeslaný	k2eAgInPc7d1	odeslaný
oběma	dva	k4xCgInPc7	dva
Voyagery	Voyager	k1gInPc7	Voyager
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
již	již	k6eAd1	již
dorazily	dorazit	k5eAaPmAgInP	dorazit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
vzniku	vznik	k1gInSc2	vznik
energetických	energetický	k2eAgInPc2d1	energetický
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
od	od	k7c2	od
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
u	u	k7c2	u
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Snímkování	snímkování	k1gNnSc1	snímkování
celé	celý	k2eAgFnSc2d1	celá
oblohy	obloha	k1gFnSc2	obloha
trvá	trvat	k5eAaImIp3nS	trvat
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
a	a	k8xC	a
rozdíly	rozdíl	k1gInPc4	rozdíl
ve	v	k7c6	v
snímaných	snímaný	k2eAgFnPc6d1	snímaná
oblastech	oblast	k1gFnPc6	oblast
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
sekvencemi	sekvence	k1gFnPc7	sekvence
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
podle	podle	k7c2	podle
sluneční	sluneční	k2eAgFnSc2d1	sluneční
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
hustoty	hustota	k1gFnSc2	hustota
mezihvězdného	mezihvězdný	k2eAgNnSc2d1	mezihvězdné
media	medium	k1gNnSc2	medium
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
magnetických	magnetický	k2eAgFnPc2d1	magnetická
polí	pole	k1gFnPc2	pole
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
IBEXu	IBEXus	k1gInSc2	IBEXus
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
pomoci	pomoct	k5eAaPmF	pomoct
k	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
pochopení	pochopení	k1gNnSc3	pochopení
těchto	tento	k3xDgInPc2	tento
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
získané	získaný	k2eAgFnPc1d1	získaná
informace	informace	k1gFnPc1	informace
pomohou	pomoct	k5eAaPmIp3nP	pomoct
lépe	dobře	k6eAd2	dobře
stanovit	stanovit	k5eAaPmF	stanovit
polohu	poloha	k1gFnSc4	poloha
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
v	v	k7c6	v
mléčné	mléčný	k2eAgFnSc6d1	mléčná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
mise	mise	k1gFnSc2	mise
je	být	k5eAaImIp3nS	být
165	[number]	k4	165
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
cca	cca	kA	cca
3,1	[number]	k4	3,1
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
IBEX	IBEX	kA	IBEX
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
IBEX	IBEX	kA	IBEX
na	na	k7c6	na
SPACE	SPACE	kA	SPACE
</s>
</p>
<p>
<s>
Novinový	novinový	k2eAgInSc1d1	novinový
článek	článek	k1gInSc1	článek
o	o	k7c6	o
sondě	sonda	k1gFnSc6	sonda
</s>
</p>
<p>
<s>
osel	osel	k1gMnSc1	osel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Tajemný	tajemný	k2eAgInSc1d1	tajemný
pás	pás	k1gInSc1	pás
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
je	být	k5eAaImIp3nS	být
kolébkou	kolébka	k1gFnSc7	kolébka
rychlých	rychlý	k2eAgInPc2d1	rychlý
atomů	atom	k1gInPc2	atom
</s>
</p>
