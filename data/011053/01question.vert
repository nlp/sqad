<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
sonda	sonda	k1gFnSc1	sonda
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
a	a	k8xC	a
která	který	k3yIgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
a	a	k8xC	a
mezihvězdným	mezihvězdný	k2eAgInSc7d1	mezihvězdný
prostorem	prostor	k1gInSc7	prostor
<g/>
?	?	kIx.	?
</s>
