<s>
IBEX	IBEX	kA	IBEX
(	(	kIx(	(
<g/>
Interstellar	Interstellar	k1gMnSc1	Interstellar
Boundary	Boundara	k1gFnSc2	Boundara
Explorer	Explorer	k1gMnSc1	Explorer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
Explorer	Explorer	k1gInSc1	Explorer
91	[number]	k4	91
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
sonda	sonda	k1gFnSc1	sonda
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
a	a	k8xC	a
která	který	k3yIgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
a	a	k8xC	a
mezihvězdným	mezihvězdný	k2eAgInSc7d1	mezihvězdný
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
