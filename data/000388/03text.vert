<s>
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
Wrath	Wrath	k1gMnSc1	Wrath
of	of	k?	of
the	the	k?	the
Lich	Lich	k?	Lich
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
WotLK	WotLK	k1gMnSc2	WotLK
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
2	[number]	k4	2
<g/>
.	.	kIx.	.
datadisk	datadisk	k1gInSc4	datadisk
k	k	k7c3	k
fantasy	fantas	k1gInPc1	fantas
MMORPG	MMORPG	kA	MMORPG
hře	hra	k1gFnSc3	hra
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
oznámen	oznámit	k5eAaPmNgInS	oznámit
na	na	k7c6	na
BlizzConu	BlizzCon	k1gInSc6	BlizzCon
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Vydán	vydán	k2eAgMnSc1d1	vydán
byl	být	k5eAaImAgMnS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
2,8	[number]	k4	2,8
milionu	milion	k4xCgInSc2	milion
kopií	kopie	k1gFnPc2	kopie
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejrychleji	rychle	k6eAd3	rychle
prodávaným	prodávaný	k2eAgInSc7d1	prodávaný
herním	herní	k2eAgInSc7d1	herní
titulem	titul	k1gInSc7	titul
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc4d1	maximální
úroveň	úroveň	k1gFnSc4	úroveň
(	(	kIx(	(
<g/>
level	level	k1gInSc4	level
<g/>
)	)	kIx)	)
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
na	na	k7c4	na
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
hratelné	hratelný	k2eAgNnSc1d1	hratelné
povolání	povolání	k1gNnSc1	povolání
-	-	kIx~	-
Death	Death	k1gMnSc1	Death
Knight	Knight	k1gMnSc1	Knight
(	(	kIx(	(
<g/>
rytíř	rytíř	k1gMnSc1	rytíř
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
Nový	nový	k2eAgInSc1d1	nový
kontinent	kontinent	k1gInSc1	kontinent
-	-	kIx~	-
Northrend	Northrend	k1gInSc1	Northrend
Nová	nový	k2eAgFnSc1d1	nová
profese	profese	k1gFnSc1	profese
-	-	kIx~	-
Inscription	Inscription	k1gInSc1	Inscription
Zvýšení	zvýšení	k1gNnSc1	zvýšení
skillu	skillat	k5eAaPmIp1nS	skillat
u	u	k7c2	u
profesí	profes	k1gFnPc2	profes
na	na	k7c4	na
450	[number]	k4	450
Nové	Nové	k2eAgInPc1d1	Nové
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
úkoly	úkol	k1gInPc1	úkol
<g/>
,	,	kIx,	,
monstra	monstrum	k1gNnPc1	monstrum
<g/>
,	,	kIx,	,
dungeony	dungeon	k1gInPc1	dungeon
<g/>
,	,	kIx,	,
kouzla	kouzlo	k1gNnPc1	kouzlo
či	či	k8xC	či
zbraně	zbraň	k1gFnPc1	zbraň
Dobývací	dobývací	k2eAgFnPc4d1	dobývací
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
zničitelné	zničitelný	k2eAgFnPc4d1	zničitelná
budovy	budova	k1gFnPc4	budova
v	v	k7c6	v
PvP	PvP	k1gFnSc6	PvP
ve	v	k7c6	v
world	world	k6eAd1	world
PvP	PvP	k1gFnSc6	PvP
teritoriu	teritorium	k1gNnSc6	teritorium
Lake	Lak	k1gFnSc2	Lak
Wintergrasp	Wintergrasp	k1gInSc4	Wintergrasp
a	a	k8xC	a
battlegroundech	battleground	k1gInPc6	battleground
The	The	k1gFnSc2	The
Isle	Isl	k1gFnSc2	Isl
of	of	k?	of
Conquest	Conquest	k1gInSc1	Conquest
a	a	k8xC	a
Strand	strand	k1gInSc1	strand
of	of	k?	of
the	the	k?	the
Ancients	Ancients	k1gInSc1	Ancients
<g/>
.	.	kIx.	.
</s>
<s>
Možnosti	možnost	k1gFnPc1	možnost
úprav	úprava	k1gFnPc2	úprava
vzhledu	vzhled	k1gInSc2	vzhled
postav	postava	k1gFnPc2	postava
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
změny	změna	k1gFnPc1	změna
účesů	účes	k1gInPc2	účes
apod.	apod.	kA	apod.
Neinstancované	Neinstancovaný	k2eAgFnPc4d1	Neinstancovaný
battlegroundy	battlegrounda	k1gFnPc4	battlegrounda
(	(	kIx(	(
<g/>
bojiště	bojiště	k1gNnSc4	bojiště
<g/>
)	)	kIx)	)
Vylepšený	vylepšený	k2eAgMnSc1d1	vylepšený
engine	enginout	k5eAaPmIp3nS	enginout
Nové	Nové	k2eAgFnSc1d1	Nové
frakce	frakce	k1gFnSc1	frakce
Systém	systém	k1gInSc1	systém
tzn.	tzn.	kA	tzn.
Achievements	Achievements	k1gInSc1	Achievements
(	(	kIx(	(
<g/>
hodnocení	hodnocení	k1gNnSc1	hodnocení
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
oba	dva	k4xCgInPc4	dva
datadisky	datadisek	k1gInPc4	datadisek
i	i	k8xC	i
původní	původní	k2eAgInSc4d1	původní
obsah	obsah	k1gInSc4	obsah
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Northrend	Northrend	k1gInSc1	Northrend
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
přibyl	přibýt	k5eAaPmAgInS	přibýt
nový	nový	k2eAgInSc1d1	nový
kontinent	kontinent	k1gInSc1	kontinent
Northrend	Northrend	k1gInSc1	Northrend
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
severně	severně	k6eAd1	severně
nad	nad	k7c7	nad
starými	starý	k2eAgInPc7d1	starý
kontinenty	kontinent	k1gInPc7	kontinent
Azerothu	Azeroth	k1gInSc2	Azeroth
<g/>
.	.	kIx.	.
</s>
<s>
Northrend	Northrend	k1gInSc1	Northrend
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
trochu	trochu	k6eAd1	trochu
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
Outland	Outland	k1gInSc4	Outland
<g/>
.	.	kIx.	.
</s>
<s>
Nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
sídlo	sídlo	k1gNnSc1	sídlo
bývalého	bývalý	k2eAgMnSc2d1	bývalý
paladina	paladin	k1gMnSc2	paladin
prince	princ	k1gMnSc4	princ
Arthase	Arthasa	k1gFnSc6	Arthasa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spojil	spojit	k5eAaPmAgMnS	spojit
svou	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
i	i	k8xC	i
tělo	tělo	k1gNnSc4	tělo
s	s	k7c7	s
králem	král	k1gMnSc7	král
lichů	lich	k1gMnPc2	lich
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
novým	nový	k2eAgMnSc7d1	nový
vládcem	vládce	k1gMnSc7	vládce
temných	temný	k2eAgFnPc2d1	temná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
lokací	lokace	k1gFnPc2	lokace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
levely	level	k1gInPc4	level
68	[number]	k4	68
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
kontinent	kontinent	k1gInSc4	kontinent
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zde	zde	k6eAd1	zde
nepřevládá	převládat	k5eNaImIp3nS	převládat
jen	jen	k9	jen
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
nezasněžené	zasněžený	k2eNgFnPc4d1	nezasněžená
lokace	lokace	k1gFnPc4	lokace
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Grizzly	grizzly	k1gMnPc1	grizzly
Hills	Hills	k1gInSc4	Hills
a	a	k8xC	a
Howling	Howling	k1gInSc4	Howling
Fjord	fjord	k1gInSc1	fjord
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
kotlina	kotlina	k1gFnSc1	kotlina
Scholazar	Scholazara	k1gFnPc2	Scholazara
Basin	Basin	k1gInSc4	Basin
s	s	k7c7	s
tropickým	tropický	k2eAgNnSc7d1	tropické
počasím	počasí	k1gNnSc7	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
levelu	level	k1gInSc2	level
55	[number]	k4	55
u	u	k7c2	u
jedné	jeden	k4xCgFnSc2	jeden
postavy	postava	k1gFnSc2	postava
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
postavu	postava	k1gFnSc4	postava
s	s	k7c7	s
hero	hero	k6eAd1	hero
povoláním	povolání	k1gNnSc7	povolání
-	-	kIx~	-
Death	Death	k1gMnSc1	Death
Knight	Knight	k1gMnSc1	Knight
(	(	kIx(	(
<g/>
rytíř	rytíř	k1gMnSc1	rytíř
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgInSc2	jeden
Death	Deatha	k1gFnPc2	Deatha
Knighta	Knight	k1gInSc2	Knight
na	na	k7c6	na
účtu	účet	k1gInSc6	účet
a	a	k8xC	a
realmu	realm	k1gInSc6	realm
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gMnSc1	Death
Knight	Knight	k1gMnSc1	Knight
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
zbraní	zbraň	k1gFnPc2	zbraň
(	(	kIx(	(
<g/>
obouruční	obouruční	k2eAgFnPc1d1	obouruční
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
jednoruční	jednoruční	k2eAgFnPc1d1	jednoruční
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
kopí	kopí	k1gNnPc1	kopí
<g/>
,	,	kIx,	,
meče	meč	k1gInPc1	meč
<g/>
,	,	kIx,	,
kladiva	kladivo	k1gNnPc1	kladivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gMnSc1	Death
Knight	Knight	k1gMnSc1	Knight
nemůže	moct	k5eNaImIp3nS	moct
používat	používat	k5eAaImF	používat
štít	štít	k1gInSc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
zbroje	zbroj	k1gFnSc2	zbroj
<g/>
,	,	kIx,	,
Death	Death	k1gMnSc1	Death
Knight	Knight	k1gMnSc1	Knight
může	moct	k5eAaImIp3nS	moct
nosit	nosit	k5eAaImF	nosit
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
oblečení	oblečení	k1gNnSc2	oblečení
a	a	k8xC	a
zbroje	zbroj	k1gFnSc2	zbroj
(	(	kIx(	(
<g/>
plátovou	plátový	k2eAgFnSc4d1	plátová
zbroj	zbroj	k1gFnSc4	zbroj
<g/>
,	,	kIx,	,
kovovou	kovový	k2eAgFnSc4d1	kovová
zbroj	zbroj	k1gFnSc4	zbroj
<g/>
,	,	kIx,	,
látkovou	látkový	k2eAgFnSc4d1	látková
zbroj	zbroj	k1gFnSc4	zbroj
a	a	k8xC	a
koženou	kožený	k2eAgFnSc7d1	kožená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýhodnější	výhodný	k2eAgFnSc1d3	nejvýhodnější
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
je	být	k5eAaImIp3nS	být
zbroj	zbroj	k1gFnSc4	zbroj
plátová	plátový	k2eAgFnSc1d1	plátová
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
odolnost	odolnost	k1gFnSc4	odolnost
a	a	k8xC	a
vydrží	vydržet	k5eAaPmIp3nS	vydržet
spoustu	spousta	k1gFnSc4	spousta
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
Death	Death	k1gMnSc1	Death
Knighta	Knighta	k1gMnSc1	Knighta
hráč	hráč	k1gMnSc1	hráč
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
55	[number]	k4	55
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
začátečnickou	začátečnický	k2eAgFnSc7d1	začátečnická
výbavou	výbava	k1gFnSc7	výbava
a	a	k8xC	a
plní	plnit	k5eAaImIp3nP	plnit
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
za	za	k7c4	za
novou	nový	k2eAgFnSc4d1	nová
postavu	postava	k1gFnSc4	postava
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
v	v	k7c4	v
Eastern	Eastern	k1gNnSc4	Eastern
Plaguelands	Plaguelandsa	k1gFnPc2	Plaguelandsa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
posledního	poslední	k2eAgInSc2d1	poslední
úkolu	úkol	k1gInSc2	úkol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
za	za	k7c7	za
svojí	svůj	k3xOyFgFnSc7	svůj
postavu	postav	k1gInSc6	postav
zabijete	zabít	k5eAaPmIp2nP	zabít
velkého	velký	k2eAgMnSc4d1	velký
Patchwerka	Patchwerka	k1gFnSc1	Patchwerka
<g/>
,	,	kIx,	,
máte	mít	k5eAaImIp2nP	mít
možnost	možnost	k1gFnSc4	možnost
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
zlepšování	zlepšování	k1gNnSc6	zlepšování
své	svůj	k3xOyFgFnSc2	svůj
postavy	postava	k1gFnSc2	postava
a	a	k8xC	a
zvyšování	zvyšování	k1gNnSc2	zvyšování
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Inskripce	inskripce	k1gFnSc1	inskripce
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hráči	hráč	k1gMnSc3	hráč
vylepšovat	vylepšovat	k5eAaImF	vylepšovat
nebo	nebo	k8xC	nebo
měnit	měnit	k5eAaImF	měnit
efekty	efekt	k1gInPc1	efekt
stávajících	stávající	k2eAgNnPc2d1	stávající
kouzel	kouzlo	k1gNnPc2	kouzlo
(	(	kIx(	(
<g/>
glyphy	glypha	k1gFnPc1	glypha
<g/>
,	,	kIx,	,
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
80	[number]	k4	80
může	moct	k5eAaImIp3nS	moct
každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
mít	mít	k5eAaImF	mít
3	[number]	k4	3
hlavní	hlavní	k2eAgInSc4d1	hlavní
a	a	k8xC	a
3	[number]	k4	3
"	"	kIx"	"
<g/>
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
<g/>
"	"	kIx"	"
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
talentové	talentový	k2eAgFnSc6d1	talentová
větvi	větev	k1gFnSc6	větev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
vytvářet	vytvářet	k5eAaImF	vytvářet
svitky	svitek	k1gInPc4	svitek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dočasně	dočasně	k6eAd1	dočasně
zlepší	zlepšit	k5eAaPmIp3nP	zlepšit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Wrath	Wrath	k1gMnSc1	Wrath
of	of	k?	of
The	The	k1gMnSc1	The
Lich	Lich	k?	Lich
King	King	k1gMnSc1	King
přinesl	přinést	k5eAaPmAgMnS	přinést
do	do	k7c2	do
světa	svět	k1gInSc2	svět
Azerothu	Azerotha	k1gFnSc4	Azerotha
nové	nový	k2eAgFnSc2d1	nová
výzvy	výzva	k1gFnSc2	výzva
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nových	nový	k2eAgFnPc2d1	nová
instancí	instance	k1gFnPc2	instance
a	a	k8xC	a
raidů	raid	k1gInPc2	raid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
předchozího	předchozí	k2eAgInSc2d1	předchozí
datadisku	datadisek	k1gInSc2	datadisek
The	The	k1gFnSc2	The
Burning	Burning	k1gInSc1	Burning
Crusade	Crusad	k1gInSc5	Crusad
jsou	být	k5eAaImIp3nP	být
raidy	raid	k1gInPc4	raid
pro	pro	k7c4	pro
10	[number]	k4	10
a	a	k8xC	a
25	[number]	k4	25
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Přináší	přinášet	k5eAaImIp3nS	přinášet
navíc	navíc	k6eAd1	navíc
možnosti	možnost	k1gFnPc4	možnost
hrát	hrát	k5eAaImF	hrát
je	on	k3xPp3gNnSc4	on
v	v	k7c6	v
takzvaném	takzvaný	k2eAgInSc6d1	takzvaný
Heroic	Heroic	k1gMnSc1	Heroic
módu	móda	k1gFnSc4	móda
(	(	kIx(	(
<g/>
hráči	hráč	k1gMnPc1	hráč
používají	používat	k5eAaImIp3nP	používat
zkratku	zkratka	k1gFnSc4	zkratka
HC	HC	kA	HC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
instance	instance	k1gFnPc1	instance
pro	pro	k7c4	pro
5	[number]	k4	5
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc1	všechen
mají	mít	k5eAaImIp3nP	mít
heroic	heroice	k1gInPc2	heroice
mód	mód	k1gInSc4	mód
pro	pro	k7c4	pro
level	level	k1gInSc4	level
80	[number]	k4	80
<g/>
)	)	kIx)	)
Caverns	Caverns	k1gInSc1	Caverns
of	of	k?	of
Time	Time	k1gInSc1	Time
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Culling	Culling	k1gInSc1	Culling
of	of	k?	of
Stratholme	Stratholme	k1gFnSc1	Stratholme
Crusaders	Crusaders	k1gInSc1	Crusaders
<g/>
'	'	kIx"	'
Colloseum	Colloseum	k1gInSc1	Colloseum
<g/>
:	:	kIx,	:
Trial	trial	k1gInSc1	trial
of	of	k?	of
the	the	k?	the
Champion	Champion	k1gInSc1	Champion
The	The	k1gMnSc1	The
Nexus	nexus	k1gInSc1	nexus
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Oculus	Oculus	k1gMnSc1	Oculus
Ulduar	Ulduar	k1gMnSc1	Ulduar
<g/>
:	:	kIx,	:
Halls	Halls	k1gInSc1	Halls
of	of	k?	of
Lightning	Lightning	k1gInSc1	Lightning
Utgarde	Utgard	k1gMnSc5	Utgard
Keep	Keep	k1gMnSc1	Keep
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Utgarde	Utgard	k1gMnSc5	Utgard
Pinnacle	Pinnacl	k1gMnSc5	Pinnacl
Ulduar	Ulduar	k1gInSc1	Ulduar
<g/>
:	:	kIx,	:
Halls	Halls	k1gInSc1	Halls
of	of	k?	of
Stone	ston	k1gInSc5	ston
Gundrak	Gundrak	k1gInSc1	Gundrak
The	The	k1gMnSc1	The
Violet	Violeta	k1gFnPc2	Violeta
Hold	hold	k1gInSc1	hold
Drak	drak	k1gMnSc1	drak
<g/>
'	'	kIx"	'
<g/>
Tharon	Tharon	k1gMnSc1	Tharon
Keep	Keep	k1gMnSc1	Keep
Ahn	Ahn	k1gMnSc1	Ahn
<g/>
'	'	kIx"	'
<g/>
kahet	kahet	k1gMnSc1	kahet
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Old	Olda	k1gFnPc2	Olda
Kingdom	Kingdom	k1gInSc1	Kingdom
Azjol-Nerub	Azjol-Nerub	k1gMnSc1	Azjol-Nerub
The	The	k1gMnSc1	The
Nexus	nexus	k1gInSc4	nexus
Utgarde	Utgard	k1gMnSc5	Utgard
Keep	Keep	k1gInSc1	Keep
Forge	Forg	k1gInPc1	Forg
of	of	k?	of
Souls	Souls	k1gInSc1	Souls
(	(	kIx(	(
<g/>
přístupné	přístupný	k2eAgNnSc1d1	přístupné
až	až	k9	až
v	v	k7c6	v
ICC	ICC	kA	ICC
-	-	kIx~	-
Icecrown	Icecrown	k1gInSc1	Icecrown
Citadel	citadela	k1gFnPc2	citadela
-	-	kIx~	-
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Pit	pit	k2eAgInSc1d1	pit
of	of	k?	of
Saron	Saron	k1gInSc1	Saron
(	(	kIx(	(
<g/>
též	též	k9	též
až	až	k6eAd1	až
s	s	k7c7	s
ICC	ICC	kA	ICC
<g/>
)	)	kIx)	)
Halls	Halls	k1gInSc1	Halls
of	of	k?	of
Reflection	Reflection	k1gInSc1	Reflection
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
předchozí	předchozí	k2eAgInPc4d1	předchozí
dva	dva	k4xCgInPc4	dva
<g/>
)	)	kIx)	)
Nové	Nové	k2eAgFnPc1d1	Nové
instance	instance	k1gFnPc1	instance
pro	pro	k7c4	pro
raidy	raid	k1gInPc4	raid
Icecrown	Icecrowna	k1gFnPc2	Icecrowna
Citadel	citadela	k1gFnPc2	citadela
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
volitelný	volitelný	k2eAgMnSc1d1	volitelný
heroic	heroic	k1gMnSc1	heroic
mód	móda	k1gFnPc2	móda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
dát	dát	k5eAaPmF	dát
vůdce	vůdce	k1gMnPc4	vůdce
raidu	raid	k1gInSc2	raid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zabil	zabít	k5eAaPmAgMnS	zabít
Lich	Lich	k?	Lich
Kinga	King	k1gMnSc4	King
na	na	k7c4	na
normální	normální	k2eAgFnSc4d1	normální
obtížnost	obtížnost	k1gFnSc4	obtížnost
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jít	jít	k5eAaImF	jít
10	[number]	k4	10
<g/>
n	n	k0	n
10	[number]	k4	10
<g/>
hc	hc	k?	hc
25	[number]	k4	25
<g/>
n	n	k0	n
25	[number]	k4	25
<g/>
hc	hc	k?	hc
Naxxramas	Naxxramas	k1gInSc1	Naxxramas
(	(	kIx(	(
<g/>
redesignovaný	redesignovaný	k2eAgInSc1d1	redesignovaný
raid	raid	k1gInSc1	raid
z	z	k7c2	z
původního	původní	k2eAgMnSc2d1	původní
World	World	k1gInSc4	World
Of	Of	k1gFnSc3	Of
Warcraft	Warcraft	k1gInSc1	Warcraft
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jít	jít	k5eAaImF	jít
10	[number]	k4	10
<g/>
n	n	k0	n
25	[number]	k4	25
<g/>
n	n	k0	n
Onyxia	Onyxius	k1gMnSc2	Onyxius
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lair	Laira	k1gFnPc2	Laira
(	(	kIx(	(
<g/>
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
redesignu	redesigno	k1gNnSc3	redesigno
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
vyvážení	vyvážení	k1gNnSc1	vyvážení
pro	pro	k7c4	pro
lvl	lvl	k?	lvl
80	[number]	k4	80
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
n	n	k0	n
25	[number]	k4	25
<g/>
n	n	k0	n
Crusaders	Crusaders	k1gInSc1	Crusaders
<g/>
'	'	kIx"	'
Colloseum	Colloseum	k1gInSc1	Colloseum
<g/>
:	:	kIx,	:
Trial	trial	k1gInSc1	trial
of	of	k?	of
the	the	k?	the
Crusader	Crusader	k1gInSc1	Crusader
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jít	jít	k5eAaImF	jít
10	[number]	k4	10
<g/>
n	n	k0	n
10	[number]	k4	10
<g/>
hc	hc	k?	hc
25	[number]	k4	25
<g/>
n	n	k0	n
25	[number]	k4	25
<g/>
hc	hc	k?	hc
The	The	k1gMnSc1	The
Eye	Eye	k1gMnSc1	Eye
of	of	k?	of
Eternity	eternit	k1gInPc4	eternit
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jít	jít	k5eAaImF	jít
10	[number]	k4	10
<g/>
n	n	k0	n
25	[number]	k4	25
<g/>
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Obsidian	Obsidian	k1gMnSc1	Obsidian
Sanctum	Sanctum	k1gNnSc4	Sanctum
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jít	jít	k5eAaImF	jít
10	[number]	k4	10
<g/>
n	n	k0	n
25	[number]	k4	25
<g/>
n	n	k0	n
OS	OS	kA	OS
má	mít	k5eAaImIp3nS	mít
specifickou	specifický	k2eAgFnSc4d1	specifická
obtížnost	obtížnost	k1gFnSc4	obtížnost
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
vůdce	vůdce	k1gMnSc1	vůdce
raidu	raid	k1gInSc2	raid
zvolí	zvolit	k5eAaPmIp3nS	zvolit
kolik	kolik	k9	kolik
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
bosse	boss	k1gMnSc2	boss
bojovat	bojovat	k5eAaImF	bojovat
draků	drak	k1gInPc2	drak
0D	[number]	k4	0D
1D	[number]	k4	1D
2D	[number]	k4	2D
3	[number]	k4	3
<g/>
D.	D.	kA	D.
Ulduar	Ulduara	k1gFnPc2	Ulduara
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jít	jít	k5eAaImF	jít
10	[number]	k4	10
<g/>
n	n	k0	n
25	[number]	k4	25
<g/>
n	n	k0	n
Vault	Vault	k2eAgInSc4d1	Vault
of	of	k?	of
Archavon	Archavon	k1gInSc4	Archavon
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jít	jít	k5eAaImF	jít
10	[number]	k4	10
<g/>
n	n	k0	n
25	[number]	k4	25
<g/>
n	n	k0	n
padají	padat	k5eAaImIp3nP	padat
převážně	převážně	k6eAd1	převážně
pvp	pvp	k?	pvp
věci	věc	k1gFnSc2	věc
The	The	k1gFnSc2	The
Ruby	rub	k1gInPc1	rub
Sanctum	Sanctum	k1gNnSc1	Sanctum
10	[number]	k4	10
<g/>
n	n	k0	n
25	[number]	k4	25
<g/>
n	n	k0	n
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
systém	systém	k1gInSc4	systém
úspěchů	úspěch	k1gInPc2	úspěch
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
achievement	achievement	k1gInSc4	achievement
získáte	získat	k5eAaPmIp2nP	získat
(	(	kIx(	(
<g/>
např	např	kA	např
zabijete	zabít	k5eAaPmIp2nP	zabít
nějakého	nějaký	k3yIgMnSc4	nějaký
bosse	boss	k1gMnSc4	boss
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ozve	ozvat	k5eAaPmIp3nS	ozvat
se	se	k3xPyFc4	se
speciální	speciální	k2eAgInSc1d1	speciální
zvuk	zvuk	k1gInSc1	zvuk
a	a	k8xC	a
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
a	a	k8xC	a
v	v	k7c4	v
chatu	chata	k1gFnSc4	chata
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
achievement	achievement	k1gInSc1	achievement
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
většinu	většina	k1gFnSc4	většina
achievementu	achievement	k1gInSc2	achievement
nejsou	být	k5eNaImIp3nP	být
udělovány	udělovat	k5eAaImNgFnP	udělovat
žádné	žádný	k3yNgFnPc1	žádný
odměny	odměna	k1gFnPc1	odměna
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
achievement	achievement	k1gInSc1	achievement
pointy	pointa	k1gFnSc2	pointa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
některé	některý	k3yIgInPc4	některý
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
např	např	kA	např
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
mounta	mount	k1gMnSc4	mount
<g/>
,	,	kIx,	,
peta	petus	k1gMnSc4	petus
apod.	apod.	kA	apod.
Jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
kategorii	kategorie	k1gFnSc6	kategorie
od	od	k7c2	od
general	generat	k5eAaPmAgInS	generat
<g/>
,	,	kIx,	,
PvE	PvE	k1gFnSc5	PvE
<g/>
,	,	kIx,	,
PvP	PvP	k1gFnSc5	PvP
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
reputace	reputace	k1gFnPc4	reputace
nebo	nebo	k8xC	nebo
questy	questa	k1gFnPc4	questa
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
Feats	Feats	k1gInSc4	Feats
of	of	k?	of
Strength	Strengtha	k1gFnPc2	Strengtha
(	(	kIx(	(
<g/>
FoS	fosa	k1gFnPc2	fosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speciální	speciální	k2eAgInPc4d1	speciální
achievementy	achievement	k1gInPc4	achievement
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
pouze	pouze	k6eAd1	pouze
jednou	jednou	k6eAd1	jednou
(	(	kIx(	(
<g/>
např.	např.	kA	např.
získání	získání	k1gNnSc1	získání
cenného	cenný	k2eAgInSc2d1	cenný
mounta	mount	k1gInSc2	mount
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
za	za	k7c4	za
složení	složení	k1gNnSc4	složení
legendárního	legendární	k2eAgInSc2d1	legendární
předmětu	předmět	k1gInSc2	předmět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zabití	zabití	k1gNnSc1	zabití
bosse	boss	k1gMnSc2	boss
v	v	k7c6	v
instancich	instanci	k1gFnPc6	instanci
které	který	k3yQgNnSc1	který
již	již	k9	již
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
datadisku	datadisek	k1gInSc6	datadisek
nebo	nebo	k8xC	nebo
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
patchu	patch	k1gInSc6	patch
zabít	zabít	k5eAaPmF	zabít
nelze	lze	k6eNd1	lze
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
achievementů	achievement	k1gInPc2	achievement
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
a	a	k8xC	a
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
okolo	okolo	k7c2	okolo
1000	[number]	k4	1000
achievementu	achievement	k1gInSc2	achievement
<g/>
.	.	kIx.	.
</s>
