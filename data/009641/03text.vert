<p>
<s>
Wubbo	Wubba	k1gMnSc5	Wubba
Johannes	Johannes	k1gMnSc1	Johannes
Ockels	Ockels	k1gInSc1	Ockels
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1946	[number]	k4	1946
Almelo	Almela	k1gFnSc5	Almela
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
astronaut	astronaut	k1gMnSc1	astronaut
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
jednoho	jeden	k4xCgInSc2	jeden
kosmického	kosmický	k2eAgInSc2d1	kosmický
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
mise	mise	k1gFnSc2	mise
STS-61-A	STS-61-A	k1gFnSc2	STS-61-A
raketoplánu	raketoplán	k1gInSc2	raketoplán
Challenger	Challenger	k1gInSc1	Challenger
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letu	let	k1gInSc6	let
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc3d1	Evropská
kosmické	kosmický	k2eAgFnSc3d1	kosmická
agentuře	agentura	k1gFnSc3	agentura
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
Technologické	technologický	k2eAgFnSc6d1	technologická
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Delftu	Delft	k1gInSc6	Delft
(	(	kIx(	(
<g/>
Delft	Delft	k1gInSc1	Delft
University	universita	k1gFnSc2	universita
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Wubbo	Wubba	k1gMnSc5	Wubba
Ockels	Ockels	k1gInSc1	Ockels
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Almelu	Almel	k1gInSc6	Almel
<g/>
,	,	kIx,	,
dětství	dětství	k1gNnSc1	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
Groningenu	Groningen	k1gInSc6	Groningen
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
dokončil	dokončit	k5eAaPmAgMnS	dokončit
studia	studio	k1gNnSc2	studio
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Groningenu	Groningen	k1gInSc6	Groningen
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
groningenském	groningenský	k2eAgInSc6d1	groningenský
Institutu	institut	k1gInSc6	institut
nukleárních	nukleární	k2eAgInPc2d1	nukleární
urychlovačů	urychlovač	k1gInPc2	urychlovač
(	(	kIx(	(
<g/>
Kernfysisch	Kernfysisch	k1gMnSc1	Kernfysisch
Versneller	Versneller	k1gMnSc1	Versneller
Instituut	Instituut	k1gMnSc1	Instituut
/	/	kIx~	/
Nuclear	Nuclear	k1gMnSc1	Nuclear
Physics	Physicsa	k1gFnPc2	Physicsa
Accelerator	Accelerator	k1gMnSc1	Accelerator
Institute	institut	k1gInSc5	institut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
obhájil	obhájit	k5eAaPmAgMnS	obhájit
disertaci	disertace	k1gFnSc4	disertace
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
Roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
náboru	nábor	k1gInSc2	nábor
astronautů	astronaut	k1gMnPc2	astronaut
Evropské	evropský	k2eAgFnSc2d1	Evropská
kosmické	kosmický	k2eAgFnSc2d1	kosmická
agentury	agentura	k1gFnSc2	agentura
(	(	kIx(	(
<g/>
ESA	eso	k1gNnSc2	eso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1977	[number]	k4	1977
postoupil	postoupit	k5eAaPmAgInS	postoupit
mezi	mezi	k7c4	mezi
pětici	pětice	k1gFnSc4	pětice
nizozemských	nizozemský	k2eAgMnPc2d1	nizozemský
finalistů	finalista	k1gMnPc2	finalista
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1978	[number]	k4	1978
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
mezi	mezi	k7c4	mezi
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
trojici	trojice	k1gFnSc4	trojice
prvních	první	k4xOgMnPc2	první
astronautů	astronaut	k1gMnPc2	astronaut
ESA	eso	k1gNnSc2	eso
(	(	kIx(	(
<g/>
Ockels	Ockels	k1gInSc1	Ockels
<g/>
,	,	kIx,	,
Ulf	Ulf	k1gMnSc1	Ulf
Merbold	Merbold	k1gMnSc1	Merbold
a	a	k8xC	a
Claude	Claud	k1gInSc5	Claud
Nicollier	Nicolliero	k1gNnPc2	Nicolliero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgMnPc1d1	budoucí
astronauti	astronaut	k1gMnPc1	astronaut
zahájili	zahájit	k5eAaPmAgMnP	zahájit
výcvik	výcvik	k1gInSc4	výcvik
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
společnou	společný	k2eAgFnSc4d1	společná
přípravu	příprava	k1gFnSc4	příprava
s	s	k7c7	s
astronauty	astronaut	k1gMnPc7	astronaut
9	[number]	k4	9
<g/>
.	.	kIx.	.
náboru	nábor	k1gInSc2	nábor
NASA	NASA	kA	NASA
v	v	k7c6	v
letech	let	k1gInPc6	let
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
Ockels	Ockels	k1gInSc1	Ockels
završil	završit	k5eAaPmAgInS	završit
získáním	získání	k1gNnSc7	získání
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
letového	letový	k2eAgMnSc2d1	letový
specialisty	specialista	k1gMnSc2	specialista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
letu	let	k1gInSc6	let
evropské	evropský	k2eAgFnSc2d1	Evropská
laboratoře	laboratoř	k1gFnSc2	laboratoř
Spacelab	Spacelaba	k1gFnPc2	Spacelaba
při	při	k7c6	při
misi	mise	k1gFnSc6	mise
STS-9	STS-9	k1gFnSc2	STS-9
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
–	–	k?	–
<g/>
prosinci	prosinec	k1gInSc6	prosinec
1983	[number]	k4	1983
byl	být	k5eAaImAgMnS	být
náhradníkem	náhradník	k1gMnSc7	náhradník
Ulfa	Ulf	k1gInSc2	Ulf
Merbolda	Merbolda	k1gFnSc1	Merbolda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1985	[number]	k4	1985
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
posádky	posádka	k1gFnSc2	posádka
letu	let	k1gInSc2	let
STS-	STS-	k1gFnSc7	STS-
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
A.	A.	kA	A.
Do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
vzlétl	vzlétnout	k5eAaPmAgMnS	vzlétnout
v	v	k7c6	v
raketoplánu	raketoplán	k1gInSc6	raketoplán
Challenger	Challengero	k1gNnPc2	Challengero
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
přistál	přistát	k5eAaPmAgInS	přistát
po	po	k7c6	po
7	[number]	k4	7
dnech	den	k1gInPc6	den
a	a	k8xC	a
44	[number]	k4	44
minutách	minuta	k1gFnPc6	minuta
letu	let	k1gInSc2	let
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
astronauty	astronaut	k1gMnPc7	astronaut
Reinhardem	Reinhard	k1gMnSc7	Reinhard
Furrerem	Furrer	k1gMnSc7	Furrer
a	a	k8xC	a
Ernstem	Ernst	k1gMnSc7	Ernst
Messerschmidem	Messerschmid	k1gMnSc7	Messerschmid
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
letu	let	k1gInSc2	let
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
experimenty	experiment	k1gInPc4	experiment
v	v	k7c6	v
laboratorním	laboratorní	k2eAgInSc6d1	laboratorní
modulu	modul	k1gInSc6	modul
Spacelab	Spacelaba	k1gFnPc2	Spacelaba
D-	D-	k1gFnSc4	D-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
letu	let	k1gInSc6	let
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
oddílu	oddíl	k1gInSc2	oddíl
astronautů	astronaut	k1gMnPc2	astronaut
ESA	eso	k1gNnSc2	eso
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
ve	v	k7c6	v
výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
a	a	k8xC	a
technologickém	technologický	k2eAgNnSc6d1	Technologické
středisku	středisko	k1gNnSc6	středisko
ESA	eso	k1gNnSc2	eso
v	v	k7c4	v
Noordwijku	Noordwijka	k1gFnSc4	Noordwijka
(	(	kIx(	(
<g/>
ESTEC	ESTEC	kA	ESTEC
<g/>
,	,	kIx,	,
European	European	k1gMnSc1	European
Space	Space	k1gFnSc2	Space
Research	Research	k1gMnSc1	Research
&	&	k?	&
Technology	technolog	k1gMnPc4	technolog
Center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
modulu	modul	k1gInSc2	modul
Columbus	Columbus	k1gMnSc1	Columbus
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vedl	vést	k5eAaImAgInS	vést
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
úřad	úřad	k1gInSc1	úřad
střediska	středisko	k1gNnSc2	středisko
ESA	eso	k1gNnSc2	eso
taméž	taméž	k1gFnSc1	taméž
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
začal	začít	k5eAaPmAgInS	začít
přednášet	přednášet	k5eAaImF	přednášet
na	na	k7c6	na
Technologické	technologický	k2eAgFnSc6d1	technologická
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Delftu	Delft	k1gInSc6	Delft
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
letectví	letectví	k1gNnSc2	letectví
a	a	k8xC	a
kosmonautky	kosmonautka	k1gFnSc2	kosmonautka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
ESA	eso	k1gNnSc2	eso
a	a	k8xC	a
soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
<g/>
Wubbo	Wubba	k1gFnSc5	Wubba
Ockels	Ockels	k1gInSc4	Ockels
byl	být	k5eAaImAgInS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
prodělal	prodělat	k5eAaPmAgMnS	prodělat
operaci	operace	k1gFnSc4	operace
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
odebrán	odebrán	k2eAgInSc4d1	odebrán
rakovinný	rakovinný	k2eAgInSc4d1	rakovinný
nádor	nádor	k1gInSc4	nádor
na	na	k7c6	na
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
,	,	kIx,	,
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
rakovina	rakovina	k1gFnSc1	rakovina
ledvin	ledvina	k1gFnPc2	ledvina
vrátila	vrátit	k5eAaPmAgFnS	vrátit
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
komplikace	komplikace	k1gFnPc4	komplikace
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojené	spojený	k2eAgFnPc4d1	spojená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wubbo	Wubba	k1gFnSc5	Wubba
Ockels	Ockels	k1gInSc4	Ockels
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Astronaut	astronaut	k1gMnSc1	astronaut
biography	biographa	k1gFnSc2	biographa
<g/>
.	.	kIx.	.
</s>
<s>
Wubbo	Wubba	k1gMnSc5	Wubba
Ockels	Ockels	k1gInSc1	Ockels
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ESA	eso	k1gNnPc1	eso
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
–	–	k?	–
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
biografie	biografie	k1gFnSc1	biografie
ESA	eso	k1gNnSc2	eso
</s>
</p>
