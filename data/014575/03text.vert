<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
lidové	lidový	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
中	中	k?
Zhō	Zhō	k1gFnSc1
rénmín	rénmín	k1gInSc1
gò	gò	k6eAd1
žen-min	žen-min	k2eAgMnSc1d1
kung-che-kuo	kung-che-kuo	k1gMnSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
HymnaI-jung-ťün	HymnaI-jung-ťün	k1gMnSc1
Ťin-sing-čchü	Ťin-sing-čchü	k1gMnSc1
(	(	kIx(
<g/>
Pochod	pochod	k1gInSc1
dobrovolníků	dobrovolník	k1gMnPc2
<g/>
)	)	kIx)
Geografie	geografie	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontrolované	kontrolovaný	k2eAgNnSc4d1
území	území	k1gNnPc4
tmavě	tmavě	k6eAd1
zeleně	zeleně	k6eAd1
<g/>
,	,	kIx,
nárokované	nárokovaný	k2eAgNnSc4d1
území	území	k1gNnSc4
světle	světle	k6eAd1
zeleně	zeleně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Peking	Peking	k1gInSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
9	#num#	k4
596	#num#	k4
960	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
2,82	2,82	k4
%	%	kIx~
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
(	(	kIx(
<g/>
8	#num#	k4
844,43	844,43	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+8	+8	k4
Poloha	poloha	k1gFnSc1
</s>
<s>
35	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
103	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
7	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
1	#num#	k4
412	#num#	k4
468	#num#	k4
771	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
137	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
ob.	ob.	k?
/	/	kIx~
km²	km²	k?
(	(	kIx(
<g/>
74	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
HDI	HDI	kA
</s>
<s>
▲	▲	k?
0,727	0,727	k4
(	(	kIx(
<g/>
střední	střední	k2eAgFnSc2d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
90	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
)	)	kIx)
Jazyk	jazyk	k1gInSc1
</s>
<s>
čínština	čínština	k1gFnSc1
(	(	kIx(
<g/>
úřední	úřední	k2eAgNnSc1d1
<g/>
)	)	kIx)
Náboženství	náboženství	k1gNnSc1
</s>
<s>
čínské	čínský	k2eAgNnSc1d1
lidové	lidový	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
<g/>
,	,	kIx,
buddhismus	buddhismus	k1gInSc1
<g/>
,	,	kIx,
taoismus	taoismus	k1gInSc1
<g/>
,	,	kIx,
konfucianismus	konfucianismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
Státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Státní	státní	k2eAgInSc1d1
zřízení	zřízení	k1gNnSc4
</s>
<s>
lidově	lidově	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1949	#num#	k4
(	(	kIx(
<g/>
vítězství	vítězství	k1gNnSc4
v	v	k7c6
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
nad	nad	k7c7
Čínskou	čínský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
)	)	kIx)
Prezident	prezident	k1gMnSc1
</s>
<s>
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
(	(	kIx(
<g/>
习	习	k?
;	;	kIx,
習	習	k?
<g/>
)	)	kIx)
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Li	li	k8xS
Kche-čchiang	Kche-čchiang	k1gInSc1
(	(	kIx(
<g/>
李	李	k?
;	;	kIx,
李	李	k?
Měna	měna	k1gFnSc1
</s>
<s>
čínský	čínský	k2eAgInSc1d1
jüan	jüan	k1gInSc1
<g/>
,	,	kIx,
ofic	ofic	k1gFnSc1
<g/>
.	.	kIx.
renminbi	renminbi	k1gNnSc1
(	(	kIx(
<g/>
CNY	CNY	kA
<g/>
)	)	kIx)
HDP	HDP	kA
<g/>
/	/	kIx~
<g/>
obyv	obyv	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
PPP	PPP	kA
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
990	#num#	k4
USD	USD	kA
(	(	kIx(
<g/>
72	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
ISO	ISO	kA
3166-1	3166-1	k4
</s>
<s>
156	#num#	k4
CHN	CHN	kA
CN	CN	kA
MPZ	MPZ	kA
</s>
<s>
PRC	prc	k0
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+86	+86	k4
Národní	národní	k2eAgInSc1d1
TLD	TLD	kA
</s>
<s>
<g/>
cn	cn	k?
<g/>
,	,	kIx,
.	.	kIx.
<g/>
中	中	k?
<g/>
,	,	kIx,
.	.	kIx.
<g/>
中	中	k?
</s>
<s>
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
v	v	k7c6
českém	český	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
Čung-chua	Čung-chua	k1gFnSc1
žen-min	žen-min	k1gInSc1
kung-che-kuo	kung-che-kuo	k6eAd1
<g/>
,	,	kIx,
pchin-jinem	pchin-jin	k1gInSc7
Zhō	Zhō	k1gFnSc1
rénmín	rénmín	k1gMnSc1
gò	gò	k?
<g/>
,	,	kIx,
znaky	znak	k1gInPc4
zjednodušené	zjednodušený	k2eAgFnSc2d1
中	中	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
ČLR	ČLR	kA
(	(	kIx(
<g/>
mezinárodně	mezinárodně	k6eAd1
CN	CN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
stát	stát	k1gInSc4
ležící	ležící	k2eAgInSc4d1
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
1,4	1,4	k4
miliardy	miliarda	k4xCgFnSc2
obyvatel	obyvatel	k1gMnPc2
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
nejlidnatější	lidnatý	k2eAgFnSc1d3
zemí	zem	k1gFnSc7
světa	svět	k1gInSc2
<g/>
,	,	kIx,
rozloha	rozloha	k1gFnSc1
9,6	9,6	k4
milionu	milion	k4xCgInSc2
km²	km²	k?
ji	on	k3xPp3gFnSc4
činí	činit	k5eAaImIp3nS
čtvrtým	čtvrtý	k4xOgInSc7
největším	veliký	k2eAgInSc7d3
státem	stát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
rychle	rychle	k6eAd1
rostoucí	rostoucí	k2eAgFnSc3d1
ekonomice	ekonomika	k1gFnSc3
a	a	k8xC
vojenské	vojenský	k2eAgFnSc3d1
síle	síla	k1gFnSc3
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
spojenému	spojený	k2eAgInSc3d1
vzrůstajícímu	vzrůstající	k2eAgInSc3d1
vlivu	vliv	k1gInSc3
ve	v	k7c6
světě	svět	k1gInSc6
se	se	k3xPyFc4
Čína	Čína	k1gFnSc1
stala	stát	k5eAaPmAgFnS
novou	nový	k2eAgFnSc7d1
supervelmocí	supervelmoc	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
je	být	k5eAaImIp3nS
ČLR	ČLR	kA
stálým	stálý	k2eAgInSc7d1
členem	člen	k1gInSc7
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
s	s	k7c7
právem	právo	k1gNnSc7
veta	veto	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
bylo	být	k5eAaImAgNnS
toto	tento	k3xDgNnSc1
postavení	postavení	k1gNnSc1
odepřeno	odepřít	k5eAaPmNgNnS
Čínské	čínský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Země	země	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
vedena	vést	k5eAaImNgFnS
Komunistickou	komunistický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
již	již	k6eAd1
dostala	dostat	k5eAaPmAgFnS
pod	pod	k7c4
svoji	svůj	k3xOyFgFnSc4
kontrolu	kontrola	k1gFnSc4
bývalé	bývalý	k2eAgFnSc2d1
cizí	cizí	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
Hongkong	Hongkong	k1gInSc1
a	a	k8xC
Macao	Macao	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznáší	vznášet	k5eAaImIp3nS
nárok	nárok	k1gInSc4
i	i	k9
na	na	k7c4
ostrov	ostrov	k1gInSc4
Tchaj-wan	Tchaj-wan	k1gInSc1
a	a	k8xC
několik	několik	k4yIc1
dalších	další	k2eAgNnPc2d1
menších	malý	k2eAgNnPc2d2
území	území	k1gNnPc2
<g/>
,	,	kIx,
tyto	tento	k3xDgFnPc1
oblasti	oblast	k1gFnPc1
jsou	být	k5eAaImIp3nP
však	však	k9
spravovány	spravovat	k5eAaImNgInP
vládou	vláda	k1gFnSc7
Čínské	čínský	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc4
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Prehistorie	prehistorie	k1gFnSc1
</s>
<s>
Původní	původní	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
Číny	Čína	k1gFnSc2
náleží	náležet	k5eAaImIp3nS
mezi	mezi	k7c4
nejstarší	starý	k2eAgNnSc4d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archeologické	archeologický	k2eAgInPc1d1
nálezy	nález	k1gInPc1
Sinanthropua	Sinanthropu	k1gInSc2
Pekinensis	Pekinensis	k1gFnSc2
dokazují	dokazovat	k5eAaImIp3nP
osídlení	osídlení	k1gNnSc4
již	již	k6eAd1
před	před	k7c4
půl	půl	k1xP
miliónem	milión	k4xCgInSc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlády	vláda	k1gFnPc1
dynastií	dynastie	k1gFnPc2
</s>
<s>
Velká	velký	k2eAgFnSc1d1
čínská	čínský	k2eAgFnSc1d1
zeď	zeď	k1gFnSc1
chránila	chránit	k5eAaImAgFnS
Čínu	Čína	k1gFnSc4
před	před	k7c7
nájezdy	nájezd	k1gInPc7
nomádů	nomád	k1gMnPc2
ze	z	k7c2
severu	sever	k1gInSc2
</s>
<s>
Kchaj-feng	Kchaj-feng	k1gInSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
dynastie	dynastie	k1gFnSc2
Sung	Sunga	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
obrazu	obraz	k1gInSc6
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
od	od	k7c2
Čang	Čanga	k1gFnPc2
Ce-tuana	Ce-tuan	k1gMnSc2
</s>
<s>
Pagoda	pagoda	k1gFnSc1
Ling-siao	Ling-siao	k6eAd1
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Císařský	císařský	k2eAgInSc1d1
palác	palác	k1gInSc1
v	v	k7c6
Zakázaném	zakázaný	k2eAgNnSc6d1
městě	město	k1gNnSc6
z	z	k7c2
období	období	k1gNnSc2
vlády	vláda	k1gFnSc2
dynastie	dynastie	k1gFnSc2
Ming	Ming	k1gMnSc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Sia	Sia	k1gFnSc2
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
první	první	k4xOgFnSc4
čínskou	čínský	k2eAgFnSc4d1
vládnoucí	vládnoucí	k2eAgFnSc4d1
dynastii	dynastie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Šang	Šanga	k1gFnPc2
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc1
historicky	historicky	k6eAd1
doložená	doložený	k2eAgFnSc1d1
čínská	čínský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládla	vládnout	k5eAaImAgFnS
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
až	až	k9
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
již	již	k6eAd1
používány	používat	k5eAaImNgInP
písmo	písmo	k1gNnSc4
a	a	k8xC
kalendář	kalendář	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Čou	Čou	k1gFnSc2
vládnoucí	vládnoucí	k2eAgFnSc2d1
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
až	až	k9
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
již	již	k6eAd1
ovládala	ovládat	k5eAaImAgFnS
a	a	k8xC
spravovala	spravovat	k5eAaImAgFnS
rozsáhlé	rozsáhlý	k2eAgNnSc4d1
území	území	k1gNnSc4
od	od	k7c2
řeky	řeka	k1gFnSc2
Jang-c	Jang-c	k1gInSc1
<g/>
’	’	k?
<g/>
-ťiang	-ťiang	k1gInSc1
až	až	k9
po	po	k7c4
Velkou	velký	k2eAgFnSc4d1
čínskou	čínský	k2eAgFnSc4d1
zeď	zeď	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Čouské	Čouské	k2eAgNnPc1d1
období	období	k1gNnPc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
Západní	západní	k2eAgFnPc4d1
a	a	k8xC
Východní	východní	k2eAgFnPc4d1
Čou	Čou	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
Východního	východní	k2eAgMnSc2d1
Čou	Čou	k1gMnSc2
se	se	k3xPyFc4
vládnoucí	vládnoucí	k2eAgMnSc1d1
král	král	k1gMnSc1
přesunul	přesunout	k5eAaPmAgMnS
na	na	k7c4
místo	místo	k1gNnSc4
moderního	moderní	k2eAgInSc2d1
Si-anu	Si-an	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
moc	moc	k6eAd1
čouského	čouský	k2eAgMnSc2d1
krále	král	k1gMnSc2
zeslábla	zeslábnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
8	#num#	k4
<g/>
.	.	kIx.
až	až	k9
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
následovalo	následovat	k5eAaImAgNnS
období	období	k1gNnSc1
Jar	Jara	k1gFnPc2
a	a	k8xC
podzimů	podzim	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
vládci	vládce	k1gMnPc1
údělných	údělný	k2eAgInPc2d1
států	stát	k1gInPc2
rozdělili	rozdělit	k5eAaPmAgMnP
zemi	zem	k1gFnSc4
mezi	mezi	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
období	období	k1gNnSc6
válčících	válčící	k2eAgInPc2d1
států	stát	k1gInPc2
utkalo	utkat	k5eAaPmAgNnS
několik	několik	k4yIc1
silných	silný	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
vládu	vláda	k1gFnSc4
nad	nad	k7c7
Čínou	Čína	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gInSc6
zvítězila	zvítězit	k5eAaPmAgFnS
dynastie	dynastie	k1gFnSc1
Čchin	Čchin	k1gInSc1
<g/>
,	,	kIx,
ovládající	ovládající	k2eAgInSc1d1
region	region	k1gInSc1
kolem	kolem	k7c2
původní	původní	k2eAgFnSc2d1
čouské	čouský	k2eAgFnSc2d1
metropole	metropol	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
dynastie	dynastie	k1gFnSc2
Čchin	Čchin	k1gInSc1
přinesla	přinést	k5eAaPmAgFnS
sjednocení	sjednocení	k1gNnSc4
území	území	k1gNnPc2
pod	pod	k7c4
silně	silně	k6eAd1
centralizovanou	centralizovaný	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
<g/>
,	,	kIx,
220	#num#	k4
let	léto	k1gNnPc2
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vystavěna	vystavěn	k2eAgFnSc1d1
Velká	velký	k2eAgFnSc1d1
čínská	čínský	k2eAgFnSc1d1
zeď	zeď	k1gFnSc1
<g/>
,	,	kIx,
postaven	postavit	k5eAaPmNgInS
zavodňovací	zavodňovací	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
proběhla	proběhnout	k5eAaPmAgFnS
reforma	reforma	k1gFnSc1
písma	písmo	k1gNnSc2
a	a	k8xC
byly	být	k5eAaImAgFnP
sjednoceny	sjednocen	k2eAgFnPc4d1
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ze	z	k7c2
zmatků	zmatek	k1gInPc2
následujících	následující	k2eAgInPc2d1
po	po	k7c6
pádu	pád	k1gInSc6
říše	říš	k1gFnSc2
Čchin	Čchina	k1gFnPc2
se	se	k3xPyFc4
zrodila	zrodit	k5eAaPmAgFnS
říše	říše	k1gFnSc1
Chan	Chana	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
Liou	Lioa	k1gFnSc4
Pang	Panga	k1gFnPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
vůdců	vůdce	k1gMnPc2
protičchingských	protičchingský	k2eAgMnPc2d1
povstalců	povstalec	k1gMnPc2
<g/>
,	,	kIx,
opětovně	opětovně	k6eAd1
sjednotil	sjednotit	k5eAaPmAgInS
Čínu	Čína	k1gFnSc4
a	a	k8xC
roku	rok	k1gInSc2
202	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
prohlásil	prohlásit	k5eAaPmAgMnS
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dynastie	dynastie	k1gFnSc1
Chan	Chana	k1gFnPc2
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejmocnějších	mocný	k2eAgFnPc2d3
dynastií	dynastie	k1gFnPc2
v	v	k7c6
celé	celý	k2eAgFnSc6d1
čínské	čínský	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
<g/>
,	,	kIx,
220	#num#	k4
až	až	k8xS
206	#num#	k4
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
obchodu	obchod	k1gInSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Technické	technický	k2eAgFnPc1d1
znalosti	znalost	k1gFnPc1
zahrnovaly	zahrnovat	k5eAaImAgFnP
používání	používání	k1gNnSc4
papíru	papír	k1gInSc2
a	a	k8xC
porcelánu	porcelán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dynastie	dynastie	k1gFnSc1
Chan	Chana	k1gFnPc2
navázala	navázat	k5eAaPmAgFnS
na	na	k7c4
předchozí	předchozí	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
expanzi	expanze	k1gFnSc4
dynastie	dynastie	k1gFnSc2
Čchin	Čchina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prvním	první	k4xOgInPc3
kontaktům	kontakt	k1gInPc3
se	se	k3xPyFc4
Západem	západ	k1gInSc7
<g/>
,	,	kIx,
s	s	k7c7
Římany	Říman	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
našeho	náš	k3xOp1gInSc2
letopočtu	letopočet	k1gInSc2
se	se	k3xPyFc4
z	z	k7c2
Indie	Indie	k1gFnSc2
do	do	k7c2
Číny	Čína	k1gFnSc2
rozšířil	rozšířit	k5eAaPmAgInS
buddhismus	buddhismus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
období	období	k1gNnSc6
tří	tři	k4xCgFnPc2
říší	říš	k1gFnPc2
(	(	kIx(
<g/>
220	#num#	k4
<g/>
-	-	kIx~
<g/>
265	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
Čína	Čína	k1gFnSc1
nejprve	nejprve	k6eAd1
rozpadla	rozpadnout	k5eAaPmAgFnS
na	na	k7c4
tři	tři	k4xCgInPc4
státy	stát	k1gInPc4
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
roztříštěnost	roztříštěnost	k1gFnSc1
ještě	ještě	k6eAd1
zvýšila	zvýšit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Tradičně	tradičně	k6eAd1
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc1
období	období	k1gNnSc1
vymezeno	vymezit	k5eAaPmNgNnS
rokem	rok	k1gInSc7
220	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
oficiálně	oficiálně	k6eAd1
vznikl	vzniknout	k5eAaPmAgInS
stát	stát	k1gInSc1
Wej	Wej	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
rokem	rok	k1gInSc7
280	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
říše	říše	k1gFnSc1
Ťin	Ťin	k1gFnSc2
dobyla	dobýt	k5eAaPmAgFnS
stát	stát	k5eAaImF,k5eAaPmF
Wu	Wu	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
dynastie	dynastie	k1gFnSc2
Suej	Suej	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
přelomu	přelom	k1gInSc6
6	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
opětovnému	opětovný	k2eAgNnSc3d1
sjednocení	sjednocení	k1gNnSc3
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
následující	následující	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
Tchang	Tchanga	k1gFnPc2
<g/>
,	,	kIx,
618	#num#	k4
až	až	k8xS
907	#num#	k4
<g/>
,	,	kIx,
nastal	nastat	k5eAaPmAgInS
"	"	kIx"
<g/>
zlatý	zlatý	k2eAgInSc1d1
věk	věk	k1gInSc1
<g/>
"	"	kIx"
v	v	k7c6
oblasti	oblast	k1gFnSc6
umění	umění	k1gNnSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Čína	Čína	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
stala	stát	k5eAaPmAgFnS
nejvýznamnějším	významný	k2eAgInSc7d3
státem	stát	k1gInSc7
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
vynalezen	vynalezen	k2eAgInSc1d1
knihtisk	knihtisk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politicky	politicky	k6eAd1
Čína	Čína	k1gFnSc1
navázala	navázat	k5eAaPmAgFnS
kontakty	kontakt	k1gInPc4
s	s	k7c7
Japonskem	Japonsko	k1gNnSc7
a	a	k8xC
zeměmi	zem	k1gFnPc7
jižní	jižní	k2eAgFnSc2d1
a	a	k8xC
západní	západní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
907	#num#	k4
až	až	k9
960	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
tzv.	tzv.	kA
období	období	k1gNnSc1
Pěti	pět	k4xCc2
dynastií	dynastie	k1gFnPc2
a	a	k8xC
deseti	deset	k4xCc2
říší	říš	k1gFnPc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dalším	další	k2eAgFnPc3d1
válkám	válka	k1gFnPc3
a	a	k8xC
fragmentaci	fragmentace	k1gFnSc3
ovládaných	ovládaný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
dynastie	dynastie	k1gFnSc2
Sung	Sunga	k1gFnPc2
(	(	kIx(
<g/>
960	#num#	k4
<g/>
-	-	kIx~
<g/>
1279	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
naopak	naopak	k6eAd1
obdobím	období	k1gNnSc7
velkých	velký	k2eAgInPc2d1
hospodářských	hospodářský	k2eAgInPc2d1
a	a	k8xC
kulturních	kulturní	k2eAgInPc2d1
úspěchů	úspěch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
manufaktur	manufaktura	k1gFnPc2
<g/>
,	,	kIx,
směny	směna	k1gFnSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
významnému	významný	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
peněžního	peněžní	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
<g/>
,	,	kIx,
věd	věda	k1gFnPc2
i	i	k8xC
farmakologie	farmakologie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vládu	vláda	k1gFnSc4
dynastie	dynastie	k1gFnSc2
Sung	Sung	k1gInSc1
ukončily	ukončit	k5eAaPmAgInP
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
čtvrtině	čtvrtina	k1gFnSc6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
nájezdy	nájezd	k1gInPc7
Mongolů	mongol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mongolové	Mongol	k1gMnPc1
ovládli	ovládnout	k5eAaPmAgMnP
celou	celý	k2eAgFnSc4d1
Čínu	Čína	k1gFnSc4
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
říše	říše	k1gFnSc1
sahala	sahat	k5eAaImAgFnS
přes	přes	k7c4
celou	celý	k2eAgFnSc4d1
Asii	Asie	k1gFnSc4
až	až	k9
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
z	z	k7c2
vládců	vládce	k1gMnPc2
mongolské	mongolský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
Jüan	jüan	k1gInSc1
byl	být	k5eAaImAgMnS
chán	chán	k1gMnSc1
Kublaj	Kublaj	k1gMnSc1
(	(	kIx(
<g/>
známé	známá	k1gFnSc6
jsou	být	k5eAaImIp3nP
jeho	jeho	k3xOp3gInPc4
kontakty	kontakt	k1gInPc4
s	s	k7c7
benátským	benátský	k2eAgMnSc7d1
cestovatelem	cestovatel	k1gMnSc7
Markem	Marek	k1gMnSc7
Polem	polem	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Povstání	povstání	k1gNnSc1
tchaj-pchingů	tchaj-pching	k1gInPc2
v	v	k7c6
letech	léto	k1gNnPc6
1850	#num#	k4
až	až	k9
1864	#num#	k4
si	se	k3xPyFc3
vyžádalo	vyžádat	k5eAaPmAgNnS
přes	přes	k7c4
20	#num#	k4
milionů	milion	k4xCgInPc2
obětí	oběť	k1gFnPc2
</s>
<s>
Krutovláda	krutovláda	k1gFnSc1
Mongolů	Mongol	k1gMnPc2
vyvolávala	vyvolávat	k5eAaImAgFnS
v	v	k7c6
Číně	Čína	k1gFnSc6
povstání	povstání	k1gNnSc2
a	a	k8xC
za	za	k7c2
dynastie	dynastie	k1gFnSc2
Ming	Ming	k1gInSc1
(	(	kIx(
<g/>
1368	#num#	k4
<g/>
-	-	kIx~
<g/>
1644	#num#	k4
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
Mongolové	Mongol	k1gMnPc1
vyhnáni	vyhnat	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
dynastie	dynastie	k1gFnSc2
Ming	Minga	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Peking	Peking	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
námořních	námořní	k2eAgInPc2d1
obchodů	obchod	k1gInPc2
a	a	k8xC
výprav	výprava	k1gFnPc2
<g/>
,	,	kIx,
stavbě	stavba	k1gFnSc3
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
zpracování	zpracování	k1gNnSc4
železa	železo	k1gNnSc2
nebo	nebo	k8xC
rozvoji	rozvoj	k1gInSc6
uměleckých	umělecký	k2eAgNnPc2d1
řemesel	řemeslo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začali	začít	k5eAaPmAgMnP
do	do	k7c2
Číny	Čína	k1gFnSc2
pronikat	pronikat	k5eAaImF
portugalští	portugalský	k2eAgMnPc1d1
obchodníci	obchodník	k1gMnPc1
a	a	k8xC
misionáři	misionář	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gInSc1
vliv	vliv	k1gInSc1
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
výrazně	výrazně	k6eAd1
omezen	omezit	k5eAaPmNgInS
na	na	k7c4
oblast	oblast	k1gFnSc4
města	město	k1gNnSc2
a	a	k8xC
přístavu	přístav	k1gInSc6
Macao	Macao	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1
dynastií	dynastie	k1gFnSc7
v	v	k7c6
Číně	Čína	k1gFnSc6
byla	být	k5eAaImAgFnS
mandžuská	mandžuský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
Čching	Čching	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládla	vládnout	k5eAaImAgFnS
v	v	k7c6
letech	léto	k1gNnPc6
1644	#num#	k4
až	až	k9
1911	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
císaři	císař	k1gMnPc1
této	tento	k3xDgFnSc2
dynastie	dynastie	k1gFnSc2
obnovili	obnovit	k5eAaPmAgMnP
moc	moc	k6eAd1
a	a	k8xC
hranice	hranice	k1gFnSc1
říše	říš	k1gFnSc2
jako	jako	k9
bývaly	bývat	k5eAaImAgFnP
za	za	k7c2
dynastie	dynastie	k1gFnSc2
Tchang	Tchang	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Čínská	čínský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
Mandžusko	Mandžusko	k1gNnSc4
<g/>
,	,	kIx,
Mongolsko	Mongolsko	k1gNnSc1
<g/>
,	,	kIx,
Tchaj-wan	Tchaj-wan	k1gInSc1
a	a	k8xC
Tibet	Tibet	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Císaři	Císař	k1gMnSc3
dynastie	dynastie	k1gFnSc2
Čhing	Čhing	k1gInSc4
podporovali	podporovat	k5eAaImAgMnP
vědu	věda	k1gFnSc4
a	a	k8xC
umění	umění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhé	Dlouhé	k2eAgInSc1d1
období	období	k1gNnSc4
míru	mír	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
stoupla	stoupnout	k5eAaPmAgFnS
životní	životní	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
císařství	císařství	k1gNnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zakončeno	zakončen	k2eAgNnSc1d1
tzv.	tzv.	kA
Opiovou	opiový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
-	-	kIx~
<g/>
1842	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
Angličany	Angličan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
porážce	porážka	k1gFnSc6
musela	muset	k5eAaImAgFnS
Čína	Čína	k1gFnSc1
postoupit	postoupit	k5eAaPmF
Spojenému	spojený	k2eAgNnSc3d1
království	království	k1gNnSc3
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
město	město	k1gNnSc1
Hongkong	Hongkong	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
britskou	britský	k2eAgFnSc7d1
korunní	korunní	k2eAgFnSc7d1
kolonií	kolonie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovaly	následovat	k5eAaImAgInP
další	další	k2eAgInPc1d1
dvě	dva	k4xCgFnPc1
opiové	opiový	k2eAgFnPc1d1
války	válka	k1gFnPc1
a	a	k8xC
pokles	pokles	k1gInSc1
životní	životní	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
Čína	Čína	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
vojenského	vojenský	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
s	s	k7c7
Japonskem	Japonsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
protimandžuských	protimandžuský	k2eAgNnPc6d1
povstáních	povstání	k1gNnPc6
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
dynastie	dynastie	k1gFnSc2
Čching	Čching	k1gInSc4
nucena	nucen	k2eAgFnSc1d1
abdikovat	abdikovat	k5eAaBmF
a	a	k8xC
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1911	#num#	k4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
Čínská	čínský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Čínská	čínský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Okupační	okupační	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
Japonského	japonský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
vstupují	vstupovat	k5eAaImIp3nP
v	v	k7c6
srpnu	srpen	k1gInSc6
1937	#num#	k4
do	do	k7c2
Pekingu	Peking	k1gInSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
po	po	k7c6
tzv.	tzv.	kA
Mukdenského	Mukdenský	k2eAgInSc2d1
Incidentu	incident	k1gInSc2
obsadila	obsadit	k5eAaPmAgFnS
japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
Mandžusko	Mandžusko	k1gNnSc1
a	a	k8xC
vytvořila	vytvořit	k5eAaPmAgFnS
zde	zde	k6eAd1
loutkový	loutkový	k2eAgInSc4d1
stát	stát	k1gInSc4
Mandžukuo	Mandžukuo	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
odrazovým	odrazový	k2eAgInSc7d1
můstkem	můstek	k1gInSc7
k	k	k7c3
japonské	japonský	k2eAgFnSc3d1
invazi	invaze	k1gFnSc3
do	do	k7c2
ostatní	ostatní	k2eAgFnSc2d1
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Incident	incident	k1gInSc1
na	na	k7c6
mostě	most	k1gInSc6
Marca	Marc	k2eAgFnSc1d1
Pola	pola	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
uměle	uměle	k6eAd1
vyvolán	vyvolat	k5eAaPmNgInS
Japonci	Japonec	k1gMnSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
záminkou	záminka	k1gFnSc7
k	k	k7c3
rozpoutání	rozpoutání	k1gNnSc3
8	#num#	k4
let	léto	k1gNnPc2
trvající	trvající	k2eAgMnSc1d1
druhé	druhý	k4xOgFnSc2
čínsko-japonské	čínsko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
součást	součást	k1gFnSc4
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
císařská	císařský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
při	při	k7c6
svém	svůj	k3xOyFgNnSc6
tažení	tažení	k1gNnSc6
do	do	k7c2
Číny	Čína	k1gFnSc2
dopouštěla	dopouštět	k5eAaImAgFnS
mnoha	mnoho	k4c2
válečných	válečný	k2eAgInPc2d1
zločinů	zločin	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
vešel	vejít	k5eAaPmAgInS
ve	v	k7c4
známost	známost	k1gFnSc4
Nankingský	nankingský	k2eAgInSc4d1
masakr	masakr	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
japonští	japonský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
zmasakrovali	zmasakrovat	k5eAaPmAgMnP
na	na	k7c4
300	#num#	k4
000	#num#	k4
civilistů	civilista	k1gMnPc2
a	a	k8xC
válečných	válečný	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Zvláštní	zvláštní	k2eAgFnSc1d1
japonská	japonský	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
731	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
Mandžusku	Mandžusko	k1gNnSc6
<g/>
,	,	kIx,
prováděla	provádět	k5eAaImAgFnS
pokusy	pokus	k1gInPc4
na	na	k7c6
lidech	člověk	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc7
na	na	k7c6
obyvatelstvu	obyvatelstvo	k1gNnSc6
testovali	testovat	k5eAaImAgMnP
biologické	biologický	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
antrax	antrax	k1gInSc1
nebo	nebo	k8xC
dýmějový	dýmějový	k2eAgInSc1d1
mor	mor	k1gInSc1
<g/>
,	,	kIx,
kterými	který	k3yRgInPc7,k3yIgInPc7,k3yQgInPc7
se	se	k3xPyFc4
nakazily	nakazit	k5eAaPmAgFnP
statisíce	statisíce	k1gInPc4
Číňanů	Číňan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Celkově	celkově	k6eAd1
si	se	k3xPyFc3
druhá	druhý	k4xOgFnSc1
čínsko-japonská	čínsko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
vyžádala	vyžádat	k5eAaPmAgFnS
přes	přes	k7c4
11	#num#	k4
milionů	milion	k4xCgInPc2
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
v	v	k7c6
Číně	Čína	k1gFnSc6
znovu	znovu	k6eAd1
propukla	propuknout	k5eAaPmAgFnS
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
mezi	mezi	k7c7
Komunistickou	komunistický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
Číny	Čína	k1gFnSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Kungčchantang	Kungčchantang	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
tehdy	tehdy	k6eAd1
ještě	ještě	k9
vládnoucími	vládnoucí	k2eAgMnPc7d1
nacionalisty	nacionalista	k1gMnPc7
(	(	kIx(
<g/>
Kuomintang	Kuomintang	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
roku	rok	k1gInSc2
1949	#num#	k4
vítězstvím	vítězství	k1gNnSc7
komunistů	komunista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Mao	Mao	k1gMnSc2
Ce-tunga	Ce-tung	k1gMnSc2
převzali	převzít	k5eAaPmAgMnP
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
celou	celý	k2eAgFnSc7d1
pevninskou	pevninský	k2eAgFnSc7d1
Čínou	Čína	k1gFnSc7
včetně	včetně	k7c2
ostrova	ostrov	k1gInSc2
Chaj-nan	Chaj-nany	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušníci	příslušník	k1gMnPc1
Kuomintangu	Kuomintang	k1gInSc2
byli	být	k5eAaImAgMnP
donuceni	donucen	k2eAgMnPc1d1
se	se	k3xPyFc4
stáhnout	stáhnout	k5eAaPmF
na	na	k7c4
ostrov	ostrov	k1gInSc4
Tchaj-wan	Tchaj-wan	k1gInSc1
a	a	k8xC
několik	několik	k4yIc1
menších	malý	k2eAgInPc2d2
přilehlých	přilehlý	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
území	území	k1gNnSc4
dnešní	dnešní	k2eAgFnSc2d1
Čínské	čínský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1949	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgMnS
Mao	Mao	k1gMnSc1
Ce-tung	Ce-tung	k1gMnSc1
na	na	k7c6
pekingském	pekingský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
Nebeského	nebeský	k2eAgInSc2d1
klidu	klid	k1gInSc2
slovy	slovo	k1gNnPc7
„	„	k?
<g/>
Číňané	Číňan	k1gMnPc1
povstali	povstat	k5eAaPmAgMnP
<g/>
“	“	k?
Čínskou	čínský	k2eAgFnSc4d1
lidovou	lidový	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
Mao	Mao	k1gFnSc1
Ce-tunga	Ce-tunga	k1gFnSc1
</s>
<s>
Mao	Mao	k?
Ce-tung	Ce-tung	k1gInSc1
vyhlašuje	vyhlašovat	k5eAaImIp3nS
ČLR	ČLR	kA
<g/>
,	,	kIx,
1949	#num#	k4
</s>
<s>
Období	období	k1gNnSc1
vlády	vláda	k1gFnSc2
během	během	k7c2
života	život	k1gInSc2
Mao	Mao	k1gMnSc2
Ce-tunga	Ce-tung	k1gMnSc2
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
dodnes	dodnes	k6eAd1
vzbuzuje	vzbuzovat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
kontroverzí	kontroverze	k1gFnPc2
<g/>
,	,	kIx,
hlavně	hlavně	k9
kvůli	kvůli	k7c3
nesmiřitelné	smiřitelný	k2eNgFnSc3d1
politice	politika	k1gFnSc3
vůči	vůči	k7c3
odpůrcům	odpůrce	k1gMnPc3
<g/>
,	,	kIx,
mnohým	mnohý	k2eAgMnPc3d1
omylům	omyl	k1gInPc3
a	a	k8xC
až	až	k9
zločinným	zločinný	k2eAgInPc3d1
důsledkům	důsledek	k1gInPc3
na	na	k7c6
obyvatelstvu	obyvatelstvo	k1gNnSc6
způsobených	způsobený	k2eAgFnPc2d1
mj.	mj.	kA
fanatickým	fanatický	k2eAgInSc7d1
vykládáním	vykládání	k1gNnSc7
komunistické	komunistický	k2eAgFnSc2d1
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
okamžitě	okamžitě	k6eAd1
uzákonila	uzákonit	k5eAaPmAgFnS
pozemkovou	pozemkový	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
<g/>
,	,	kIx,
znárodnila	znárodnit	k5eAaPmAgFnS
průmyslovou	průmyslový	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
a	a	k8xC
započala	započnout	k5eAaPmAgFnS
s	s	k7c7
vyhlašováním	vyhlašování	k1gNnSc7
kampaní	kampaň	k1gFnPc2
(	(	kIx(
<g/>
programy	program	k1gInPc1
gramotnosti	gramotnost	k1gFnSc2
<g/>
,	,	kIx,
očkování	očkování	k1gNnSc1
<g/>
,	,	kIx,
zvýšení	zvýšení	k1gNnSc1
kvality	kvalita	k1gFnSc2
zemědělství	zemědělství	k1gNnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
po	po	k7c6
vzniku	vznik	k1gInSc6
nového	nový	k2eAgInSc2d1
státu	stát	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
ČLR	ČLR	kA
také	také	k9
formálně	formálně	k6eAd1
anektovala	anektovat	k5eAaBmAgFnS
Východní	východní	k2eAgInSc4d1
Turkestán	Turkestán	k1gInSc4
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Sin-ťiang	Sin-ťianga	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
i	i	k9
Tibet	Tibet	k1gInSc1
(	(	kIx(
<g/>
oficiálně	oficiálně	k6eAd1
až	až	k6eAd1
roku	rok	k1gInSc2
1951	#num#	k4
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgFnSc1d1
Tibetská	tibetský	k2eAgFnSc1d1
autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
dokončila	dokončit	k5eAaPmAgFnS
opětovné	opětovný	k2eAgNnSc4d1
sjednocení	sjednocení	k1gNnSc4
území	území	k1gNnSc2
připojením	připojení	k1gNnSc7
všech	všecek	k3xTgInPc2
separatistických	separatistický	k2eAgInPc2d1
států	stát	k1gInPc2
vzniklých	vzniklý	k2eAgInPc2d1
po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
republiky	republika	k1gFnSc2
roku	rok	k1gInSc2
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunistická	komunistický	k2eAgFnSc1d1
Čína	Čína	k1gFnSc1
se	se	k3xPyFc4
také	také	k9
zapojila	zapojit	k5eAaPmAgNnP
do	do	k7c2
korejské	korejský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
straně	strana	k1gFnSc6
Severní	severní	k2eAgFnSc2d1
Koreje	Korea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
pod	pod	k7c7
heslem	heslo	k1gNnSc7
„	„	k?
<g/>
Ať	ať	k9
rozkvétá	rozkvétat	k5eAaImIp3nS
sto	sto	k4xCgNnSc4
květů	květ	k1gInPc2
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
soupeří	soupeřit	k5eAaImIp3nS
sto	sto	k4xCgNnSc1
škol	škola	k1gFnPc2
učení	učení	k1gNnSc2
<g/>
“	“	k?
kampaň	kampaň	k1gFnSc1
Sta	sto	k4xCgNnPc1
květů	květ	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
byli	být	k5eAaImAgMnP
intelektuálové	intelektuál	k1gMnPc1
vyzváni	vyzvat	k5eAaPmNgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
dali	dát	k5eAaPmAgMnP
kritikou	kritika	k1gFnSc7
podněty	podnět	k1gInPc4
pro	pro	k7c4
další	další	k2eAgNnSc4d1
zlepšení	zlepšení	k1gNnSc4
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
ale	ale	k9
kampaň	kampaň	k1gFnSc1
postupně	postupně	k6eAd1
zvrhla	zvrhnout	k5eAaPmAgFnS
v	v	k7c4
kritiku	kritika	k1gFnSc4
vládního	vládní	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
,	,	kIx,
následovala	následovat	k5eAaImAgFnS
kampaň	kampaň	k1gFnSc4
proti	proti	k7c3
pravičákům	pravičák	k1gMnPc3
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
odsoudila	odsoudit	k5eAaPmAgFnS
statisíce	statisíce	k1gInPc4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
intelektuálů	intelektuál	k1gMnPc2
<g/>
,	,	kIx,
k	k	k7c3
smrti	smrt	k1gFnSc3
nebo	nebo	k8xC
k	k	k7c3
převýchově	převýchova	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
pokusu	pokus	k1gInSc6
dohnat	dohnat	k5eAaPmF
a	a	k8xC
předehnat	předehnat	k5eAaPmF
Západ	západ	k1gInSc4
byl	být	k5eAaImAgInS
vyhlášen	vyhlášen	k2eAgInSc4d1
tzv.	tzv.	kA
Velký	velký	k2eAgInSc4d1
skok	skok	k1gInSc4
vpřed	vpřed	k6eAd1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
ale	ale	k9
skončil	skončit	k5eAaPmAgInS
katastrofou	katastrofa	k1gFnSc7
–	–	k?
na	na	k7c4
hladomor	hladomor	k1gInSc4
zemřelo	zemřít	k5eAaPmAgNnS
podle	podle	k7c2
odhadů	odhad	k1gInPc2
dokonce	dokonce	k9
více	hodně	k6eAd2
než	než	k8xS
30	#num#	k4
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
z	z	k7c2
čela	čelo	k1gNnSc2
KS	ks	kA
Číny	Čína	k1gFnSc2
odstoupil	odstoupit	k5eAaPmAgInS
Mao	Mao	k1gFnSc4
Ce-tung	Ce-tunga	k1gFnPc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
si	se	k3xPyFc3
i	i	k9
nadále	nadále	k6eAd1
ve	v	k7c6
straně	strana	k1gFnSc6
udržoval	udržovat	k5eAaImAgInS
velký	velký	k2eAgInSc1d1
vliv	vliv	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
Liou	Lioa	k1gMnSc4
Šao-čchi	Šao-čch	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
ideologické	ideologický	k2eAgFnSc2d1
roztržky	roztržka	k1gFnSc2
se	s	k7c7
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
byla	být	k5eAaImAgFnS
navíc	navíc	k6eAd1
ČLR	ČLR	kA
odkázána	odkázat	k5eAaPmNgFnS
pouze	pouze	k6eAd1
sama	sám	k3xTgFnSc1
na	na	k7c4
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1964	#num#	k4
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
jaderná	jaderný	k2eAgFnSc1d1
zkouška	zkouška	k1gFnSc1
a	a	k8xC
ČLR	ČLR	kA
se	se	k3xPyFc4
tak	tak	k6eAd1
zařadila	zařadit	k5eAaPmAgFnS
mezi	mezi	k7c4
jaderné	jaderný	k2eAgFnPc4d1
mocnosti	mocnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Kulturní	kulturní	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Kulturní	kulturní	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mao	Mao	k?
Ce-tung	Ce-tung	k1gInSc1
upevnil	upevnit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
moc	moc	k1gFnSc4
mezi	mezi	k7c7
rolníky	rolník	k1gMnPc7
přes	přes	k7c4
pozemkovou	pozemkový	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
a	a	k8xC
také	také	k9
díky	díky	k7c3
popravám	poprava	k1gFnPc3
mezi	mezi	k7c7
1	#num#	k4
a	a	k8xC
2	#num#	k4
miliony	milion	k4xCgInPc1
zemědělských	zemědělský	k2eAgMnPc2d1
hospodářů	hospodář	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Čínská	čínský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
se	se	k3xPyFc4
z	z	k7c2
550	#num#	k4
milionů	milion	k4xCgInPc2
téměř	téměř	k6eAd1
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
900	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
Mao	Mao	k1gMnPc4
Ce-tungův	Ce-tungův	k2eAgInSc1d1
rozsáhlý	rozsáhlý	k2eAgInSc1d1
projekt	projekt	k1gInSc1
ekonomické	ekonomický	k2eAgFnSc2d1
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Velký	velký	k2eAgInSc1d1
skok	skok	k1gInSc1
vpřed	vpřed	k6eAd1
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1958	#num#	k4
a	a	k8xC
1961	#num#	k4
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
za	za	k7c4
následek	následek	k1gInSc4
okolo	okolo	k7c2
45	#num#	k4
milionů	milion	k4xCgInPc2
úmrtí	úmrtí	k1gNnPc2
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
následkem	následkem	k7c2
hladomoru	hladomor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
Mao	Mao	k1gMnSc1
Ce-tung	Ce-tung	k1gMnSc1
inicioval	iniciovat	k5eAaBmAgMnS
Velkou	velký	k2eAgFnSc4d1
proletářskou	proletářský	k2eAgFnSc4d1
kulturní	kulturní	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
na	na	k7c6
myšlence	myšlenka	k1gFnSc6
nepřetržité	přetržitý	k2eNgFnSc2d1
revoluce	revoluce	k1gFnSc2
a	a	k8xC
masové	masový	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
měla	mít	k5eAaImAgFnS
za	za	k7c4
cíl	cíl	k1gInSc4
vymýtit	vymýtit	k5eAaPmF
kapitalismus	kapitalismus	k1gInSc4
uvnitř	uvnitř	k7c2
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
Maovým	Maový	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
pro	pro	k7c4
uskutečnění	uskutečnění	k1gNnSc4
revoluce	revoluce	k1gFnSc2
však	však	k9
patrně	patrně	k6eAd1
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
od	od	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
postupně	postupně	k6eAd1
ztrácel	ztrácet	k5eAaImAgMnS
vliv	vliv	k1gInSc4
ve	v	k7c6
vedení	vedení	k1gNnSc6
strany	strana	k1gFnSc2
a	a	k8xC
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgInS
zbavit	zbavit	k5eAaPmF
svých	svůj	k3xOyFgMnPc2
ideologických	ideologický	k2eAgMnPc2d1
odpůrců	odpůrce	k1gMnPc2
a	a	k8xC
osobních	osobní	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kampaň	kampaň	k1gFnSc1
byla	být	k5eAaImAgFnS
zaměřena	zaměřit	k5eAaPmNgFnS
proti	proti	k7c3
čtyřem	čtyři	k4xCgInPc3
přežitkům	přežitek	k1gInPc3
–	–	k?
starému	starý	k2eAgNnSc3d1
myšlení	myšlení	k1gNnSc3
<g/>
,	,	kIx,
kultuře	kultura	k1gFnSc3
<g/>
,	,	kIx,
obyčejům	obyčej	k1gInPc3
a	a	k8xC
návykům	návyk	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnací	hnací	k2eAgFnSc7d1
silou	síla	k1gFnSc7
Maovy	Maova	k1gFnSc2
revoluce	revoluce	k1gFnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
mládež	mládež	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vytvářela	vytvářet	k5eAaImAgFnS
tzv.	tzv.	kA
rudé	rudý	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgFnPc7d1
oběťmi	oběť	k1gFnPc7
kampaně	kampaň	k1gFnSc2
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
Liou	Lioum	k1gNnSc3
Šao-čchi	Šao-čchi	k1gNnSc2
a	a	k8xC
Teng	Teng	k1gMnSc1
Siao-pching	Siao-pching	k1gInSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
zahraniční	zahraniční	k2eAgMnPc1d1
diplomaté	diplomat	k1gMnPc1
a	a	k8xC
veškeré	veškerý	k3xTgFnSc2
autority	autorita	k1gFnSc2
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
obrovským	obrovský	k2eAgFnPc3d1
škodám	škoda	k1gFnPc3
na	na	k7c6
kulturním	kulturní	k2eAgNnSc6d1
dědictví	dědictví	k1gNnSc6
(	(	kIx(
<g/>
byly	být	k5eAaImAgFnP
páleny	pálen	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
<g/>
,	,	kIx,
vypleněna	vypleněn	k2eAgNnPc1d1
muzea	muzeum	k1gNnPc1
a	a	k8xC
ničeny	ničen	k2eAgFnPc1d1
historické	historický	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
<g/>
)	)	kIx)
a	a	k8xC
země	země	k1gFnSc1
se	se	k3xPyFc4
zmítala	zmítat	k5eAaImAgFnS
v	v	k7c6
chaosu	chaos	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kampaň	kampaň	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1969	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
její	její	k3xOp3gInPc1
výsledky	výsledek	k1gInPc1
přetrvaly	přetrvat	k5eAaPmAgInP
do	do	k7c2
Maovy	Maova	k1gFnSc2
smrti	smrt	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
a	a	k8xC
dále	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Mao	Mao	k?
Ce-tung	Ce-tung	k1gMnSc1
a	a	k8xC
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Richard	Richard	k1gMnSc1
Nixon	Nixon	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
během	během	k7c2
americko-čínského	americko-čínský	k2eAgNnSc2d1
sblížení	sblížení	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
obě	dva	k4xCgFnPc1
země	zem	k1gFnPc1
v	v	k7c6
konfliktu	konflikt	k1gInSc6
se	s	k7c7
SSSR	SSSR	kA
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
1984	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
31	#num#	k4
měsících	měsíc	k1gInPc6
intenzivního	intenzivní	k2eAgNnSc2d1
vyšetřování	vyšetřování	k1gNnSc2
<g/>
,	,	kIx,
ověřování	ověřování	k1gNnSc2
a	a	k8xC
přehodnocování	přehodnocování	k1gNnSc2
Ústředním	ústřední	k2eAgInSc7d1
výborem	výbor	k1gInSc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
počty	počet	k1gInPc4
vztahující	vztahující	k2eAgInPc4d1
se	se	k3xPyFc4
k	k	k7c3
Velké	velký	k2eAgFnSc3d1
kulturní	kulturní	k2eAgFnSc3d1
revoluci	revoluce	k1gFnSc3
tyto	tento	k3xDgMnPc4
<g/>
:	:	kIx,
4,2	4,2	k4
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
bylo	být	k5eAaImAgNnS
zatčeno	zatknout	k5eAaPmNgNnS
a	a	k8xC
vyšetřováno	vyšetřovat	k5eAaImNgNnS
<g/>
,	,	kIx,
1,7	1,7	k4
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
zemřelo	zemřít	k5eAaPmAgNnS
nepřirozenou	přirozený	k2eNgFnSc7d1
smrtí	smrt	k1gFnSc7
<g/>
,	,	kIx,
135	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
bylo	být	k5eAaImAgNnS
označeno	označit	k5eAaPmNgNnS
za	za	k7c4
kontrarevolucionáře	kontrarevolucionář	k1gMnPc4
a	a	k8xC
popraveno	popraven	k2eAgNnSc4d1
<g/>
,	,	kIx,
237	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
<g/>
,	,	kIx,
7,03	7,03	k4
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
bylo	být	k5eAaImAgNnS
zraněno	zranit	k5eAaPmNgNnS
nebo	nebo	k8xC
zmrzačeno	zmrzačit	k5eAaPmNgNnS
při	při	k7c6
ozbrojených	ozbrojený	k2eAgInPc6d1
útocích	útok	k1gInPc6
a	a	k8xC
71	#num#	k4
200	#num#	k4
rodin	rodina	k1gFnPc2
bylo	být	k5eAaImAgNnS
rozbito	rozbít	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statistiky	statistik	k1gMnPc4
sestavené	sestavený	k2eAgMnPc4d1
ze	z	k7c2
zdrojů	zdroj	k1gInPc2
na	na	k7c6
úrovni	úroveň	k1gFnSc6
krajů	kraj	k1gInPc2
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
průběhu	průběh	k1gInSc6
revoluce	revoluce	k1gFnSc2
zemřelo	zemřít	k5eAaPmAgNnS
7,73	7,73	k4
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
nepřirozenou	přirozený	k2eNgFnSc7d1
smrtí	smrt	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celkově	celkově	k6eAd1
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Mao	Mao	k1gMnSc1
Ce-tung	Ce-tung	k1gMnSc1
je	být	k5eAaImIp3nS
zodpovědný	zodpovědný	k2eAgMnSc1d1
za	za	k7c4
smrt	smrt	k1gFnSc4
40	#num#	k4
až	až	k9
70	#num#	k4
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
prostřednictvím	prostřednictvím	k7c2
hladu	hlad	k1gInSc2
<g/>
,	,	kIx,
nucených	nucený	k2eAgFnPc2d1
prací	práce	k1gFnPc2
a	a	k8xC
poprav	poprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byl	být	k5eAaImAgInS
prakticky	prakticky	k6eAd1
v	v	k7c6
každé	každý	k3xTgFnSc6
čínské	čínský	k2eAgFnSc6d1
vesnici	vesnice	k1gFnSc6
určen	určit	k5eAaPmNgMnS
pro	pro	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
popravu	poprava	k1gFnSc4
alespoň	alespoň	k9
jeden	jeden	k4xCgMnSc1
statkář	statkář	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
obvykle	obvykle	k6eAd1
několik	několik	k4yIc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
si	se	k3xPyFc3
vyžádalo	vyžádat	k5eAaPmAgNnS
odhadem	odhad	k1gInSc7
2	#num#	k4
až	až	k8xS
5	#num#	k4
milionů	milion	k4xCgInPc2
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Liberalizace	liberalizace	k1gFnSc1
ekonomiky	ekonomika	k1gFnSc2
</s>
<s>
Kulturní	kulturní	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
až	až	k9
smrtí	smrt	k1gFnSc7
Mao	Mao	k1gMnSc2
Ce-tunga	Ce-tung	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následném	následný	k2eAgInSc6d1
boji	boj	k1gInSc6
o	o	k7c4
moc	moc	k1gFnSc4
neuspěla	uspět	k5eNaPmAgFnS
tzv.	tzv.	kA
banda	banda	k1gFnSc1
čtyř	čtyři	k4xCgMnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
měla	mít	k5eAaImAgFnS
velký	velký	k2eAgInSc4d1
podíl	podíl	k1gInSc4
na	na	k7c4
kulturní	kulturní	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
obviněna	obvinit	k5eAaPmNgFnS
z	z	k7c2
uvedení	uvedení	k1gNnSc2
země	zem	k1gFnSc2
do	do	k7c2
chaosu	chaos	k1gInSc2
a	a	k8xC
všichni	všechen	k3xTgMnPc1
členové	člen	k1gMnPc1
odsouzeni	odsoudit	k5eAaPmNgMnP,k5eAaImNgMnP
k	k	k7c3
vysokým	vysoký	k2eAgInPc3d1
trestům	trest	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
čela	čelo	k1gNnSc2
státu	stát	k1gInSc2
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
proreformní	proreformní	k2eAgNnSc1d1
křídlo	křídlo	k1gNnSc1
KS	ks	kA
Číny	Čína	k1gFnSc2
reprezentované	reprezentovaný	k2eAgFnSc2d1
Teng	Teng	k1gMnSc1
Siao-pchingem	Siao-pching	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
postupně	postupně	k6eAd1
převzal	převzít	k5eAaPmAgInS
veškerou	veškerý	k3xTgFnSc4
moc	moc	k1gFnSc4
ve	v	k7c6
státě	stát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
oteplování	oteplování	k1gNnSc3
vztahů	vztah	k1gInPc2
se	s	k7c7
Západem	západ	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
následujících	následující	k2eAgInPc6d1
letech	let	k1gInPc6
proběhly	proběhnout	k5eAaPmAgFnP
mnohé	mnohý	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
<g/>
,	,	kIx,
díky	díky	k7c3
nímž	jenž	k3xRgMnSc7
se	se	k3xPyFc4
hlavně	hlavně	k9
v	v	k7c6
ekonomické	ekonomický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
země	zem	k1gFnSc2
postupně	postupně	k6eAd1
přiblížila	přiblížit	k5eAaPmAgFnS
více	hodně	k6eAd2
Západu	západ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřízeny	zřízen	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
například	například	k6eAd1
tzv.	tzv.	kA
„	„	k?
<g/>
Zvláštní	zvláštní	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
umožněno	umožnit	k5eAaPmNgNnS
podnikání	podnikání	k1gNnSc1
západním	západní	k2eAgMnSc7d1
investorům	investor	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
ale	ale	k9
naopak	naopak	k6eAd1
zavedena	zaveden	k2eAgFnSc1d1
politika	politika	k1gFnSc1
jednoho	jeden	k4xCgNnSc2
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
ve	v	k7c6
výsledku	výsledek	k1gInSc6
přinesla	přinést	k5eAaPmAgFnS
mnohé	mnohý	k2eAgInPc4d1
negativní	negativní	k2eAgInPc4d1
vlivy	vliv	k1gInPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
nepoměru	nepoměr	k1gInSc2
mezi	mezi	k7c7
pohlavími	pohlaví	k1gNnPc7
<g/>
,	,	kIx,
stárnutí	stárnutí	k1gNnSc1
populace	populace	k1gFnSc2
a	a	k8xC
úbytku	úbytek	k1gInSc3
pracovní	pracovní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Studentské	studentský	k2eAgInPc1d1
protesty	protest	k1gInPc1
na	na	k7c6
náměstí	náměstí	k1gNnSc6
Tchien-an-men	Tchien-an-men	k1gInSc1
</s>
<s>
Roku	rok	k1gInSc2
1989	#num#	k4
došlo	dojít	k5eAaPmAgNnS
po	po	k7c6
narůstající	narůstající	k2eAgFnSc6d1
nespokojenosti	nespokojenost	k1gFnSc6
k	k	k7c3
protestům	protest	k1gInPc3
studentů	student	k1gMnPc2
proti	proti	k7c3
vládě	vláda	k1gFnSc3
KS	ks	kA
Číny	Čína	k1gFnSc2
na	na	k7c6
náměstí	náměstí	k1gNnSc6
Nebeského	nebeský	k2eAgInSc2d1
klidu	klid	k1gInSc2
v	v	k7c6
metropoli	metropol	k1gFnSc6
Pekingu	Peking	k1gInSc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
však	však	k9
byly	být	k5eAaImAgFnP
tvrdě	tvrdě	k6eAd1
potlačeny	potlačit	k5eAaPmNgFnP
zásahem	zásah	k1gInSc7
armády	armáda	k1gFnSc2
a	a	k8xC
uvrhly	uvrhnout	k5eAaPmAgFnP
ČLR	ČLR	kA
do	do	k7c2
přechodné	přechodný	k2eAgFnSc2d1
izolace	izolace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
demokratizaci	demokratizace	k1gFnSc3
a	a	k8xC
zavedení	zavedení	k1gNnSc3
pluralitního	pluralitní	k2eAgInSc2d1
systému	systém	k1gInSc2
nedošlo	dojít	k5eNaPmAgNnS
<g/>
,	,	kIx,
komunistické	komunistický	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
země	zem	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
generačně	generačně	k6eAd1
obměnilo	obměnit	k5eAaPmAgNnS
a	a	k8xC
pokračovalo	pokračovat	k5eAaImAgNnS
nadále	nadále	k6eAd1
v	v	k7c6
hospodářských	hospodářský	k2eAgFnPc6d1
reformách	reforma	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
90	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
a	a	k8xC
nové	nový	k2eAgNnSc4d1
tisíciletí	tisíciletí	k1gNnSc4
</s>
<s>
Obří	obří	k2eAgInSc1d1
portrét	portrét	k1gInSc1
Mao	Mao	k1gMnSc2
Ce-tunga	Ce-tung	k1gMnSc2
na	na	k7c6
bráně	brána	k1gFnSc6
Nebeského	nebeský	k2eAgInSc2d1
klidu	klid	k1gInSc2
v	v	k7c6
Pekingu	Peking	k1gInSc6
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1997	#num#	k4
byl	být	k5eAaImAgMnS
k	k	k7c3
Čínské	čínský	k2eAgFnSc3d1
lidové	lidový	k2eAgFnSc3d1
republice	republika	k1gFnSc3
připojen	připojen	k2eAgInSc4d1
Hongkong	Hongkong	k1gInSc4
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
pod	pod	k7c7
správou	správa	k1gFnSc7
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
pod	pod	k7c7
podmínkou	podmínka	k1gFnSc7
„	„	k?
<g/>
jeden	jeden	k4xCgInSc1
stát	stát	k1gInSc1
<g/>
,	,	kIx,
dva	dva	k4xCgInPc1
systémy	systém	k1gInPc1
<g/>
“	“	k?
<g/>
;	;	kIx,
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1999	#num#	k4
bylo	být	k5eAaImAgNnS
pod	pod	k7c7
stejnými	stejný	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
připojeno	připojit	k5eAaPmNgNnS
Macao	Macao	k1gNnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
pod	pod	k7c7
správou	správa	k1gFnSc7
Portugalska	Portugalsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
výdajích	výdaj	k1gInPc6
na	na	k7c4
ozbrojené	ozbrojený	k2eAgFnPc4d1
síly	síla	k1gFnPc4
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
Čína	Čína	k1gFnSc1
na	na	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
hned	hned	k6eAd1
za	za	k7c4
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
výrazně	výrazně	k6eAd1
začala	začít	k5eAaPmAgFnS
modernizovat	modernizovat	k5eAaBmF
všechny	všechen	k3xTgFnPc4
složky	složka	k1gFnPc4
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
administrativy	administrativa	k1gFnSc2
Chu	Chu	k1gFnSc1
Ťin-tchaa	Ťin-tchaa	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vládla	vládnout	k5eAaImAgFnS
od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
začaly	začít	k5eAaPmAgFnP
být	být	k5eAaImF
řešeny	řešen	k2eAgInPc4d1
problémy	problém	k1gInPc4
zejména	zejména	k9
v	v	k7c6
sociální	sociální	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
koncept	koncept	k1gInSc4
s	s	k7c7
názvem	název	k1gInSc7
Harmonická	harmonický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
slibuje	slibovat	k5eAaImIp3nS
fúzi	fúze	k1gFnSc4
socialismu	socialismus	k1gInSc2
a	a	k8xC
demokracie	demokracie	k1gFnSc2
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
střední	střední	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
vládu	vláda	k1gFnSc4
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
zahrnuje	zahrnovat	k5eAaImIp3nS
prvky	prvek	k1gInPc4
nového	nový	k2eAgNnSc2d1
konfuciánství	konfuciánství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
administrativa	administrativa	k1gFnSc1
byla	být	k5eAaImAgFnS
vůbec	vůbec	k9
první	první	k4xOgFnSc7
v	v	k7c6
historii	historie	k1gFnSc6
ČLR	ČLR	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
začala	začít	k5eAaPmAgFnS
otevřeně	otevřeně	k6eAd1
podporovat	podporovat	k5eAaImF
náboženství	náboženství	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
buddhismus	buddhismus	k1gInSc1
a	a	k8xC
taoismus	taoismus	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vidí	vidět	k5eAaImIp3nS
jako	jako	k9
tradiční	tradiční	k2eAgFnSc4d1
součást	součást	k1gFnSc4
čínské	čínský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
zahraniční	zahraniční	k2eAgMnPc1d1
pozorovatelé	pozorovatel	k1gMnPc1
ovšem	ovšem	k9
označují	označovat	k5eAaImIp3nP
aktivitu	aktivita	k1gFnSc4
čínské	čínský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
za	za	k7c4
další	další	k2eAgInSc4d1
pokus	pokus	k1gInSc4
o	o	k7c4
namalování	namalování	k1gNnSc4
falešného	falešný	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
zemi	zem	k1gFnSc6
neustále	neustále	k6eAd1
probíhají	probíhat	k5eAaImIp3nP
masové	masový	k2eAgFnPc4d1
represe	represe	k1gFnPc4
<g/>
,	,	kIx,
cenzura	cenzura	k1gFnSc1
informací	informace	k1gFnPc2
a	a	k8xC
zabíjení	zabíjení	k1gNnSc2
jako	jako	k9
v	v	k7c6
předchozích	předchozí	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračují	pokračovat	k5eAaImIp3nP
také	také	k9
restrikce	restrikce	k1gFnPc1
proti	proti	k7c3
určitým	určitý	k2eAgFnPc3d1
náboženským	náboženský	k2eAgFnPc3d1
skupinám	skupina	k1gFnPc3
proti	proti	k7c3
nimž	jenž	k3xRgMnPc3
trvá	trvat	k5eAaImIp3nS
takzvaný	takzvaný	k2eAgInSc4d1
„	„	k?
<g/>
boj	boj	k1gInSc4
proti	proti	k7c3
zlým	zlý	k2eAgInPc3d1
kultům	kult	k1gInPc3
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
Strana	strana	k1gFnSc1
vyhlásila	vyhlásit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
,	,	kIx,
zasažena	zasažen	k2eAgNnPc1d1
jsou	být	k5eAaImIp3nP
veškerá	veškerý	k3xTgNnPc1
náboženství	náboženství	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
nepodřizují	podřizovat	k5eNaImIp3nP
Straně	strana	k1gFnSc3
<g/>
,	,	kIx,
tedy	tedy	k8xC
křesťané	křesťan	k1gMnPc1
loajální	loajální	k2eAgFnSc2d1
papeži	papež	k1gMnSc3
<g/>
,	,	kIx,
buddhisté	buddhista	k1gMnPc1
loajální	loajální	k2eAgFnSc2d1
dalajlámovi	dalajláma	k1gMnSc3
<g/>
,	,	kIx,
Fa-lun-kung	Fa-lun-kung	k1gInSc1
loajální	loajální	k2eAgInSc1d1
k	k	k7c3
zakladateli	zakladatel	k1gMnSc6
Li	li	k8xS
Chung-č	Chung-č	k1gInSc4
<g/>
'	'	kIx"
<g/>
ovi	ovi	k?
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
,	,	kIx,
zůstává	zůstávat	k5eAaImIp3nS
také	také	k9
sporná	sporný	k2eAgFnSc1d1
otázka	otázka	k1gFnSc1
Tibetu	Tibet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
byly	být	k5eAaImAgInP
v	v	k7c6
Tibetu	Tibet	k1gInSc6
krvavě	krvavě	k6eAd1
potlačeny	potlačen	k2eAgFnPc1d1
údajné	údajný	k2eAgFnPc1d1
protičínské	protičínský	k2eAgFnPc1d1
demonstrace	demonstrace	k1gFnPc1
<g/>
,	,	kIx,
různé	různý	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
poukazovaly	poukazovat	k5eAaImAgInP
na	na	k7c4
angažování	angažování	k1gNnSc4
čínské	čínský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
ve	v	k7c6
vyprovokování	vyprovokování	k1gNnSc6
a	a	k8xC
řízení	řízení	k1gNnSc6
nepokojů	nepokoj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Exilový	exilový	k2eAgMnSc1d1
tibetský	tibetský	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
vůdce	vůdce	k1gMnSc1
dalajláma	dalajláma	k1gMnSc1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vzpoura	vzpoura	k1gFnSc1
ve	v	k7c6
Lhase	Lhasa	k1gFnSc6
byla	být	k5eAaImAgFnS
zinscenována	zinscenovat	k5eAaPmNgFnS
čínskou	čínský	k2eAgFnSc7d1
policií	policie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Režimem	režim	k1gInSc7
propagovaná	propagovaný	k2eAgFnSc1d1
„	„	k?
<g/>
harmonická	harmonický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
“	“	k?
a	a	k8xC
vláda	vláda	k1gFnSc1
práva	právo	k1gNnSc2
je	být	k5eAaImIp3nS
vzhledem	vzhled	k1gInSc7
k	k	k7c3
skutečným	skutečný	k2eAgFnPc3d1
událostem	událost	k1gFnPc3
v	v	k7c6
Číně	Čína	k1gFnSc6
chápána	chápat	k5eAaImNgFnS
rovněž	rovněž	k6eAd1
jako	jako	k8xS,k8xC
pouhý	pouhý	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
manévr	manévr	k1gInSc4
na	na	k7c4
okrášlení	okrášlení	k1gNnSc4
obrazu	obraz	k1gInSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínský	čínský	k2eAgMnSc1d1
právník	právník	k1gMnSc1
Kao	Kao	k1gFnSc2
Č	Č	kA
<g/>
'	'	kIx"
<g/>
-šeng	-šeng	k1gMnSc1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
nezvěstný	zvěstný	k2eNgMnSc1d1
<g/>
,	,	kIx,
prodělal	prodělat	k5eAaPmAgMnS
mučení	mučení	k1gNnSc4
čínskou	čínský	k2eAgFnSc7d1
tajnou	tajný	k2eAgFnSc7d1
policií	policie	k1gFnSc7
<g/>
,	,	kIx,
o	o	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
do	do	k7c2
Sněmovny	sněmovna	k1gFnSc2
reprezentantů	reprezentant	k1gMnPc2
USA	USA	kA
svoji	svůj	k3xOyFgFnSc4
výpověď	výpověď	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Právníci	právník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
hájí	hájit	k5eAaImIp3nP
vládou	vláda	k1gFnSc7
pronásledované	pronásledovaný	k2eAgFnSc2d1
duchovní	duchovní	k2eAgFnSc2d1
z	z	k7c2
řad	řada	k1gFnPc2
hnutí	hnutí	k1gNnSc2
Fa-lun-kung	Fa-lun-kunga	k1gFnPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
sami	sám	k3xTgMnPc1
pronásledováni	pronásledován	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nepopiratelný	popiratelný	k2eNgInSc1d1
velký	velký	k2eAgInSc1d1
ekonomický	ekonomický	k2eAgInSc1d1
růst	růst	k1gInSc1
Číny	Čína	k1gFnSc2
za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
několik	několik	k4yIc4
desetiletí	desetiletí	k1gNnPc2
je	být	k5eAaImIp3nS
podle	podle	k7c2
znalců	znalec	k1gMnPc2
vykoupen	vykoupen	k2eAgMnSc1d1
devastací	devastace	k1gFnSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
pošlapáváním	pošlapávání	k1gNnSc7
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečná	skutečný	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
a	a	k8xC
vláda	vláda	k1gFnSc1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
ani	ani	k8xC
svoboda	svoboda	k1gFnSc1
projevu	projev	k1gInSc2
<g/>
,	,	kIx,
svoboda	svoboda	k1gFnSc1
víry	víra	k1gFnSc2
a	a	k8xC
šíření	šíření	k1gNnSc2
informací	informace	k1gFnPc2
není	být	k5eNaImIp3nS
v	v	k7c6
Číně	Čína	k1gFnSc6
fakticky	fakticky	k6eAd1
povolována	povolován	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
ani	ani	k8xC
umožněna	umožněn	k2eAgFnSc1d1
<g/>
,	,	kIx,
přesto	přesto	k8xC
že	že	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
čínských	čínský	k2eAgInPc6d1
zákonech	zákon	k1gInPc6
ustavena	ustavit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Geografie	geografie	k1gFnSc2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
poloha	poloha	k1gFnSc1
</s>
<s>
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
rozlohy	rozloha	k1gFnSc2
třetím	třetí	k4xOgInSc7
největším	veliký	k2eAgInSc7d3
státem	stát	k1gInSc7
světa	svět	k1gInSc2
(	(	kIx(
<g/>
po	po	k7c6
Rusku	Rusko	k1gNnSc6
a	a	k8xC
Kanadě	Kanada	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
povrch	povrch	k6eAd1wR
činí	činit	k5eAaImIp3nS
-	-	kIx~
při	pře	k1gFnSc3
započtení	započtení	k1gNnSc2
všech	všecek	k3xTgNnPc2
území	území	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
si	se	k3xPyFc3
ČLR	ČLR	kA
nárokuje	nárokovat	k5eAaImIp3nS
(	(	kIx(
<g/>
včetně	včetně	k7c2
Tchaj-wanu	Tchaj-wan	k1gInSc2
a	a	k8xC
několika	několik	k4yIc2
sporných	sporný	k2eAgNnPc2d1
příhraničních	příhraniční	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
)	)	kIx)
-	-	kIx~
9	#num#	k4
596	#num#	k4
960	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odečteme	odečíst	k5eAaPmIp1nP
<g/>
-li	-li	k?
Tchaj-wan	Tchaj-wan	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rozloha	rozloha	k1gFnSc1
9	#num#	k4
560	#num#	k4
772	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
započtení	započtení	k1gNnSc6
vnitřních	vnitřní	k2eAgFnPc2d1
vodních	vodní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
a	a	k8xC
sporných	sporný	k2eAgNnPc2d1
území	území	k1gNnPc2
pak	pak	k6eAd1
9	#num#	k4
780	#num#	k4
000	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
maximální	maximální	k2eAgFnSc1d1
uváděná	uváděný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozloha	rozloha	k1gFnSc1
Číny	Čína	k1gFnSc2
v	v	k7c6
km	km	kA
<g/>
2	#num#	k4
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
započítávané	započítávaný	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
</s>
<s>
rozloha	rozloha	k1gFnSc1
</s>
<s>
poznámky	poznámka	k1gFnPc1
</s>
<s>
veškerá	veškerý	k3xTgFnSc1
souš	souš	k1gFnSc1
+	+	kIx~
vnitrozemské	vnitrozemský	k2eAgFnSc2d1
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
</s>
<s>
9	#num#	k4
780	#num#	k4
000	#num#	k4
</s>
<s>
plocha	plocha	k1gFnSc1
všech	všecek	k3xTgNnPc2
jezer	jezero	k1gNnPc2
a	a	k8xC
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
</s>
<s>
183	#num#	k4
000	#num#	k4
</s>
<s>
veškerá	veškerý	k3xTgFnSc1
souš	souš	k1gFnSc1
pod	pod	k7c7
de	de	k?
jure	jurat	k5eAaPmIp3nS
vládou	vláda	k1gFnSc7
ČLR	ČLR	kA
–	–	k?
tedy	tedy	k9
včetně	včetně	k7c2
Taiwanu	Taiwan	k1gInSc2
i	i	k8xC
dalších	další	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgMnPc4
si	se	k3xPyFc3
ČLR	ČLR	kA
nárokuje	nárokovat	k5eAaImIp3nS
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
Jihočínském	jihočínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
indicko-čínských	indicko-čínský	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
atp.	atp.	kA
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
596	#num#	k4
960	#num#	k4
</s>
<s>
nejčastěji	často	k6eAd3
uváděná	uváděný	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
</s>
<s>
souš	souš	k1gFnSc1
bez	bez	k7c2
sporných	sporný	k2eAgNnPc2d1
území	území	k1gNnPc2
</s>
<s>
9	#num#	k4
502	#num#	k4
000	#num#	k4
</s>
<s>
minimální	minimální	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
se	se	k3xPyFc4
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
v	v	k7c6
literatuře	literatura	k1gFnSc6
</s>
<s>
veškerá	veškerý	k3xTgNnPc4
sporná	sporný	k2eAgNnPc4d1
území	území	k1gNnPc4
(	(	kIx(
<g/>
bez	bez	k7c2
severního	severní	k2eAgInSc2d1
Assámu	Assám	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
94	#num#	k4
000	#num#	k4
</s>
<s>
Taiwan	Taiwan	k1gMnSc1
</s>
<s>
36	#num#	k4
188	#num#	k4
</s>
<s>
Hongkong	Hongkong	k1gInSc1
</s>
<s>
1	#num#	k4
098	#num#	k4
</s>
<s>
Macao	Macao	k1gNnSc1
</s>
<s>
28	#num#	k4
</s>
<s>
sporné	sporný	k2eAgNnSc1d1
území	území	k1gNnSc1
severního	severní	k2eAgInSc2d1
Ladakhu	Ladakh	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
Čína	Čína	k1gFnSc1
okupuje	okupovat	k5eAaBmIp3nS
</s>
<s>
30	#num#	k4
000	#num#	k4
</s>
<s>
další	další	k2eAgNnPc1d1
sporná	sporný	k2eAgNnPc1d1
území	území	k1gNnPc1
na	na	k7c6
hranicích	hranice	k1gFnPc6
s	s	k7c7
Indií	Indie	k1gFnSc7
<g/>
,	,	kIx,
Barmou	Barma	k1gFnSc7
<g/>
,	,	kIx,
Kazachstánem	Kazachstán	k1gInSc7
i	i	k8xC
Ruskem	Rusko	k1gNnSc7
</s>
<s>
27	#num#	k4
000	#num#	k4
</s>
<s>
souš	souš	k1gFnSc1
+	+	kIx~
území	území	k1gNnSc1
pod	pod	k7c7
faktickou	faktický	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
Číny	Čína	k1gFnSc2
(	(	kIx(
<g/>
všechna	všechen	k3xTgNnPc4
nárok	nárok	k1gInSc1
<g/>
.	.	kIx.
území	území	k1gNnSc1
bez	bez	k7c2
Taiwanu	Taiwan	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
560	#num#	k4
772	#num#	k4
</s>
<s>
druhá	druhý	k4xOgFnSc1
nejčastěji	často	k6eAd3
uváděná	uváděný	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
Číny	Čína	k1gFnSc2
a	a	k8xC
USA	USA	kA
v	v	k7c6
km	km	kA
<g/>
2	#num#	k4
dle	dle	k7c2
CIA	CIA	kA
WORLD	WORLD	kA
FACTBOOK	FACTBOOK	kA
</s>
<s>
Čína	Čína	k1gFnSc1
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
USA	USA	kA
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
celková	celkový	k2eAgFnSc1d1
</s>
<s>
9	#num#	k4
596	#num#	k4
960	#num#	k4
</s>
<s>
9	#num#	k4
833	#num#	k4
517	#num#	k4
</s>
<s>
souš	souš	k1gFnSc1
</s>
<s>
9	#num#	k4
326	#num#	k4
410	#num#	k4
</s>
<s>
9	#num#	k4
147	#num#	k4
593	#num#	k4
</s>
<s>
vodstvo	vodstvo	k1gNnSc1
</s>
<s>
270	#num#	k4
550	#num#	k4
</s>
<s>
685	#num#	k4
924	#num#	k4
</s>
<s>
Pěstování	pěstování	k1gNnSc1
rýže	rýže	k1gFnSc2
v	v	k7c6
Kuang-si	Kuang-se	k1gFnSc6
</s>
<s>
Chua-šan	Chua-šan	k1gInSc1
je	být	k5eAaImIp3nS
posvátnou	posvátný	k2eAgFnSc7d1
horou	hora	k1gFnSc7
taoismu	taoismus	k1gInSc2
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
při	při	k7c6
západním	západní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
severu	sever	k1gInSc2
k	k	k7c3
jihu	jih	k1gInSc3
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
od	od	k7c2
řeky	řeka	k1gFnSc2
Chej-lung-ťiang	Chej-lung-ťiang	k1gInSc1
(	(	kIx(
<g/>
Amur	Amur	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c4
53	#num#	k4
<g/>
°	°	k?
s.	s.	k?
<g/>
š.	š.	k?
k	k	k7c3
nejjižnějšímu	jižní	k2eAgInSc3d3
cípu	cíp	k1gInSc3
ostrovů	ostrov	k1gInPc2
Nan-ša	Nan-š	k1gInSc2
(	(	kIx(
<g/>
Spratlyho	Spratlyha	k1gFnSc5
ostrovy	ostrov	k1gInPc1
<g/>
)	)	kIx)
na	na	k7c4
18	#num#	k4
<g/>
°	°	k?
s.	s.	k?
<g/>
š.	š.	k?
Spojnice	spojnice	k1gFnSc1
měří	měřit	k5eAaImIp3nS
kolem	kolem	k7c2
5	#num#	k4
500	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Pamíru	Pamír	k1gInSc2
na	na	k7c6
západě	západ	k1gInSc6
(	(	kIx(
<g/>
71	#num#	k4
<g/>
°	°	k?
v.d.	v.d.	k?
<g/>
)	)	kIx)
po	po	k7c4
soutok	soutok	k1gInSc4
řek	řeka	k1gFnPc2
Chej-lung-ťiang	Chej-lung-ťianga	k1gFnPc2
a	a	k8xC
Ussuri	Ussuri	k1gNnPc2
(	(	kIx(
<g/>
135	#num#	k4
<g/>
°	°	k?
v.d.	v.d.	k?
<g/>
)	)	kIx)
přesahuje	přesahovat	k5eAaImIp3nS
spojnice	spojnice	k1gFnSc1
5	#num#	k4
200	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časový	časový	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
čtyři	čtyři	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
celém	celý	k2eAgInSc6d1
státě	stát	k1gInSc6
se	se	k3xPyFc4
však	však	k9
používá	používat	k5eAaImIp3nS
jednotný	jednotný	k2eAgInSc1d1
čas	čas	k1gInSc1
(	(	kIx(
<g/>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradičním	tradiční	k2eAgNnSc7d1
evropským	evropský	k2eAgNnSc7d1
označením	označení	k1gNnSc7
pro	pro	k7c4
oblast	oblast	k1gFnSc4
Číny	Čína	k1gFnSc2
(	(	kIx(
<g/>
i	i	k9
Japonska	Japonsko	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
"	"	kIx"
<g/>
dálný	dálný	k2eAgInSc4d1
východ	východ	k1gInSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
ČLR	ČLR	kA
přímo	přímo	k6eAd1
sousedí	sousedit	k5eAaImIp3nS
se	s	k7c7
14	#num#	k4
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
suchozemskou	suchozemský	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
o	o	k7c6
délce	délka	k1gFnSc6
22	#num#	k4
800	#num#	k4
km	km	kA
a	a	k8xC
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
mnohem	mnohem	k6eAd1
větším	veliký	k2eAgNnSc7d2
Ruskem	Rusko	k1gNnSc7
<g/>
)	)	kIx)
nejvíce	nejvíce	k6eAd1,k6eAd3
sousedů	soused	k1gMnPc2
ze	z	k7c2
všech	všecek	k3xTgInPc2
států	stát	k1gInPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
směru	směr	k1gInSc6
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
jimi	on	k3xPp3gFnPc7
jsou	být	k5eAaImIp3nP
Indie	Indie	k1gFnPc1
(	(	kIx(
<g/>
délka	délka	k1gFnSc1
hranice	hranice	k1gFnSc1
3	#num#	k4
380	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pákistán	Pákistán	k1gInSc1
(	(	kIx(
<g/>
523	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Afghánistán	Afghánistán	k1gInSc1
(	(	kIx(
<g/>
76	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tádžikistán	Tádžikistán	k1gInSc1
(	(	kIx(
<g/>
414	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kyrgyzstán	Kyrgyzstán	k1gInSc1
(	(	kIx(
<g/>
858	#num#	k4
km	km	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kazachstán	Kazachstán	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
533	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
645	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mongolsko	Mongolsko	k1gNnSc1
(	(	kIx(
<g/>
4	#num#	k4
677	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
416	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vietnam	Vietnam	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
281	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Laos	Laos	k1gInSc1
(	(	kIx(
<g/>
423	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Myanmar	Myanmar	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
185	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bhútán	Bhútán	k1gInSc1
(	(	kIx(
<g/>
470	#num#	k4
km	km	kA
<g/>
)	)	kIx)
a	a	k8xC
Nepál	Nepál	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
236	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
moře	moře	k1gNnSc1
odděluje	oddělovat	k5eAaImIp3nS
Čínu	Čína	k1gFnSc4
od	od	k7c2
Jižní	jižní	k2eAgFnSc2d1
Koreje	Korea	k1gFnSc2
<g/>
,	,	kIx,
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
Čínské	čínský	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
na	na	k7c6
Tchaj-wanu	Tchaj-wan	k1gInSc6
<g/>
,	,	kIx,
Filipín	Filipíny	k1gFnPc2
<g/>
,	,	kIx,
Bruneje	Brunej	k1gInSc2
<g/>
,	,	kIx,
Malajsie	Malajsie	k1gFnSc2
a	a	k8xC
Indonésie	Indonésie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
ČLR	ČLR	kA
vede	vést	k5eAaImIp3nS
spor	spor	k1gInSc1
o	o	k7c4
pozemní	pozemní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
s	s	k7c7
Indií	Indie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Podobnou	podobný	k2eAgFnSc4d1
rozepři	rozepře	k1gFnSc4
má	mít	k5eAaImIp3nS
s	s	k7c7
Tádžikistánem	Tádžikistán	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
krátký	krátký	k2eAgInSc4d1
úsek	úsek	k1gInSc4
hranice	hranice	k1gFnSc2
s	s	k7c7
KLDR	KLDR	kA
není	být	k5eNaImIp3nS
určen	určit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohraniční	pohraniční	k2eAgInPc1d1
spory	spor	k1gInPc1
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
byly	být	k5eAaImAgFnP
urovnány	urovnán	k2eAgFnPc1d1
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jihočínská	jihočínský	k2eAgFnSc1d1
krasová	krasový	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc6
omývají	omývat	k5eAaImIp3nP
čínské	čínský	k2eAgNnSc1d1
území	území	k1gNnSc1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
směru	směr	k1gInSc6
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
Pochajský	Pochajský	k2eAgInSc1d1
záliv	záliv	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
čínské	čínský	k2eAgNnSc4d1
kontinentální	kontinentální	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
Žluté	žlutý	k2eAgNnSc4d1
<g/>
,	,	kIx,
Východočínské	východočínský	k2eAgNnSc4d1
a	a	k8xC
Jihočínské	jihočínský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
jsou	být	k5eAaImIp3nP
okrajovými	okrajový	k2eAgFnPc7d1
moři	moře	k1gNnSc6
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
teritoriálních	teritoriální	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
Číny	Čína	k1gFnSc2
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
rozloze	rozloha	k1gFnSc6
4,73	4,73	k4
milionu	milion	k4xCgInSc2
km²	km²	k?
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
na	na	k7c4
5	#num#	k4
400	#num#	k4
ostrovů	ostrov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnPc4d3
z	z	k7c2
nich	on	k3xPp3gInPc2
o	o	k7c6
rozloze	rozloha	k1gFnSc6
36	#num#	k4
000	#num#	k4
km²	km²	k?
je	být	k5eAaImIp3nS
Tchaj-wan	Tchaj-wan	k1gInSc1
(	(	kIx(
<g/>
ovládán	ovládán	k2eAgInSc1d1
je	být	k5eAaImIp3nS
však	však	k9
Čínskou	čínský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
následovaný	následovaný	k2eAgInSc1d1
ostrovem	ostrov	k1gInSc7
Chaj-nan	Chaj-nan	k1gMnSc1
(	(	kIx(
<g/>
34	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýchodnějšími	východní	k2eAgInPc7d3
čínskými	čínský	k2eAgInPc7d1
ostrovy	ostrov	k1gInPc7
je	být	k5eAaImIp3nS
souostroví	souostroví	k1gNnSc1
Tiao-jü	Tiao-jü	k1gFnSc2
nacházející	nacházející	k2eAgFnSc2d1
se	se	k3xPyFc4
severně	severně	k6eAd1
od	od	k7c2
Tchaj-wanu	Tchaj-wan	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nejvýchodněji	východně	k6eAd3
leží	ležet	k5eAaImIp3nS
ostrov	ostrov	k1gInSc4
Čch	Čch	k1gFnSc2
<g/>
'	'	kIx"
<g/>
-wej	-wej	k1gInSc1
<g/>
;	;	kIx,
ostrovy	ostrov	k1gInPc1
jsou	být	k5eAaImIp3nP
ovšem	ovšem	k9
pod	pod	k7c7
námořní	námořní	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
ostrůvků	ostrůvek	k1gInPc2
a	a	k8xC
skalních	skalní	k2eAgInPc2d1
útesů	útes	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
označovány	označovat	k5eAaImNgInP
jako	jako	k9
„	„	k?
<g/>
ostrovy	ostrov	k1gInPc4
Jihočínského	jihočínský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
patří	patřit	k5eAaImIp3nS
hlavně	hlavně	k6eAd1
do	do	k7c2
čtyř	čtyři	k4xCgNnPc2
větších	veliký	k2eAgNnPc2d2
souostroví	souostroví	k1gNnPc2
<g/>
:	:	kIx,
Tung-ša	Tung-ša	k1gFnSc1
(	(	kIx(
<g/>
Východní	východní	k2eAgInPc1d1
písky	písek	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Si-ša	Si-ša	k1gFnSc1
(	(	kIx(
<g/>
Západní	západní	k2eAgInPc1d1
písky	písek	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Čung-ša	Čung-ša	k1gFnSc1
(	(	kIx(
<g/>
Střední	střední	k2eAgInPc1d1
písky	písek	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
Nan-ša	Nan-ša	k1gFnSc1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgInPc1d1
písky	písek	k1gInPc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
o	o	k7c4
oblast	oblast	k1gFnSc4
se	se	k3xPyFc4
stále	stále	k6eAd1
vedou	vést	k5eAaImIp3nP
rozsáhlé	rozsáhlý	k2eAgInPc1d1
územní	územní	k2eAgInPc1d1
spory	spor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
ČLR	ČLR	kA
je	být	k5eAaImIp3nS
zaangažována	zaangažován	k2eAgFnSc1d1
ve	v	k7c6
spletitém	spletitý	k2eAgInSc6d1
sporu	spor	k1gInSc6
o	o	k7c4
ostrovy	ostrov	k1gInPc4
Nan-ša	Nan-šus	k1gMnSc2
(	(	kIx(
<g/>
Spratlyho	Spratly	k1gMnSc2
ostrovy	ostrov	k1gInPc4
<g/>
)	)	kIx)
s	s	k7c7
Malajsií	Malajsie	k1gFnSc7
<g/>
,	,	kIx,
Filipínami	Filipíny	k1gFnPc7
<g/>
,	,	kIx,
Čínskou	čínský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
,	,	kIx,
Vietnamem	Vietnam	k1gInSc7
a	a	k8xC
Brunejí	Brunej	k1gFnSc7
a	a	k8xC
ve	v	k7c6
sporu	spor	k1gInSc6
o	o	k7c6
souostroví	souostroví	k1gNnSc6
Tiao-jü-tao	Tiao-jü-tao	k1gMnSc1
(	(	kIx(
<g/>
Senkaku	Senkak	k1gInSc2
<g/>
)	)	kIx)
s	s	k7c7
Japonskem	Japonsko	k1gNnSc7
a	a	k8xC
s	s	k7c7
Čínskou	čínský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
vede	vést	k5eAaImIp3nS
spor	spor	k1gInSc1
o	o	k7c4
námořní	námořní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
s	s	k7c7
Vietnamem	Vietnam	k1gInSc7
v	v	k7c6
Tonkinském	tonkinský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrovy	ostrov	k1gInPc1
Si-ša	Si-ša	k1gFnSc1
(	(	kIx(
<g/>
Paracelské	Paracelský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
obsazené	obsazený	k2eAgInPc1d1
Čínskou	čínský	k2eAgFnSc7d1
lidovou	lidový	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
nárokovány	nárokován	k2eAgFnPc1d1
Vietnamem	Vietnam	k1gInSc7
a	a	k8xC
Čínskou	čínský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
na	na	k7c6
Tchaj-wanu	Tchaj-wan	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobná	podobný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
je	být	k5eAaImIp3nS
i	i	k9
okolo	okolo	k7c2
souostroví	souostroví	k1gNnSc2
Tung-ša	Tung-š	k1gInSc2
(	(	kIx(
<g/>
Prataské	Prataský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
Čung-ša	Čung-ša	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geologie	geologie	k1gFnSc1
a	a	k8xC
geomorfologie	geomorfologie	k1gFnSc1
</s>
<s>
Mount	Mount	k1gMnSc1
Everest	Everest	k1gInSc4
z	z	k7c2
čínské	čínský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
</s>
<s>
Téměř	téměř	k6eAd1
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
Číny	Čína	k1gFnSc2
leží	ležet	k5eAaImIp3nS
na	na	k7c6
Eurasijské	Eurasijský	k2eAgFnSc6d1
litosférické	litosférický	k2eAgFnSc6d1
desce	deska	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
srážka	srážka	k1gFnSc1
se	s	k7c7
sousední	sousední	k2eAgFnSc7d1
indoaustralskou	indoaustralský	k2eAgFnSc7d1
deskou	deska	k1gFnSc7
způsobila	způsobit	k5eAaPmAgFnS
alpinsko-himálajské	alpinsko-himálajský	k2eAgNnSc4d1
vrásnění	vrásnění	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
ovlivnilo	ovlivnit	k5eAaPmAgNnS
podobu	podoba	k1gFnSc4
zejména	zejména	k6eAd1
západu	západ	k1gInSc2
čínského	čínský	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
srážky	srážka	k1gFnSc2
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
tektonicky	tektonicky	k6eAd1
aktivní	aktivní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tibetská	tibetský	k2eAgFnSc1d1
náhorní	náhorní	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významný	významný	k2eAgInSc1d1
tektonický	tektonický	k2eAgInSc1d1
zlom	zlom	k1gInSc1
-	-	kIx~
Severovýchodní	severovýchodní	k2eAgInSc1d1
zlom	zlom	k1gInSc1
-	-	kIx~
probíhá	probíhat	k5eAaImIp3nS
i	i	k9
napříč	napříč	k7c7
Čínou	Čína	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
řeky	řeka	k1gFnSc2
Ussuri	Ussur	k1gFnSc2
až	až	k9
po	po	k7c4
Tři	tři	k4xCgInPc4
soutěsky	soutěsk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
na	na	k7c6
tomto	tento	k3xDgInSc6
zlomu	zlom	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
masivnímu	masivní	k2eAgNnSc3d1
zemětřesení	zemětřesení	k1gNnSc3
roku	rok	k1gInSc2
1976	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
si	se	k3xPyFc3
vyžádalo	vyžádat	k5eAaPmAgNnS
čtvrt	čtvrt	k1xP,k1gFnSc1,k1gFnSc4
milionu	milion	k4xCgInSc2
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Největším	veliký	k2eAgNnSc7d3
pohořím	pohoří	k1gNnSc7
Číny	Čína	k1gFnSc2
(	(	kIx(
<g/>
i	i	k8xC
světa	svět	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
Himálaj	Himálaj	k1gFnSc1
<g/>
,	,	kIx,
tvořící	tvořící	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
hráz	hráz	k1gFnSc1
Tibetské	tibetský	k2eAgFnSc2d1
náhorní	náhorní	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
přírodní	přírodní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
Číny	Čína	k1gFnSc2
s	s	k7c7
Indií	Indie	k1gFnSc7
<g/>
,	,	kIx,
Nepálem	Nepál	k1gInSc7
a	a	k8xC
Bhútánem	Bhútán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
i	i	k9
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
Ču-mu-lang-ma	Ču-mu-lang-ma	k1gNnSc1
feng	fenga	k1gFnPc2
<g/>
,	,	kIx,
tibetsky	tibetsky	k6eAd1
Qomolangma	Qomolangma	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
hraniční	hraniční	k2eAgFnSc7d1
horou	hora	k1gFnSc7
mezi	mezi	k7c7
Nepálem	Nepál	k1gInSc7
a	a	k8xC
Čínou	Čína	k1gFnSc7
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
její	její	k3xOp3gFnSc7
Tibetskou	tibetský	k2eAgFnSc7d1
autonomní	autonomní	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
obvykle	obvykle	k6eAd1
udávaná	udávaný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
je	být	k5eAaImIp3nS
8848	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
,	,	kIx,
ta	ten	k3xDgNnPc1
se	se	k3xPyFc4
v	v	k7c6
čase	čas	k1gInSc6
nicméně	nicméně	k8xC
stále	stále	k6eAd1
zvyšuje	zvyšovat	k5eAaImIp3nS
<g/>
,	,	kIx,
neboť	neboť	k8xC
tlak	tlak	k1gInSc1
indoaustralské	indoaustralský	k2eAgFnSc2d1
desky	deska	k1gFnSc2
na	na	k7c4
eurasijskou	eurasijský	k2eAgFnSc4d1
trvá	trvat	k5eAaImIp3nS
a	a	k8xC
celý	celý	k2eAgInSc1d1
Himálaj	Himálaj	k1gFnSc1
se	se	k3xPyFc4
nadále	nadále	k6eAd1
zvedá	zvedat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Everest	Everest	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
velkým	velký	k2eAgNnSc7d1
lákadlem	lákadlo	k1gNnSc7
extrémní	extrémní	k2eAgFnSc2d1
turistiky	turistika	k1gFnSc2
<g/>
,	,	kIx,
tu	tu	k6eAd1
se	se	k3xPyFc4
však	však	k9
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
její	její	k3xOp3gInPc4
negativní	negativní	k2eAgInPc4d1
dopady	dopad	k1gInPc4
na	na	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
rozhodla	rozhodnout	k5eAaPmAgFnS
regulovat	regulovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Sin-ťiangu	Sin-ťiang	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
naopak	naopak	k6eAd1
nachází	nacházet	k5eAaImIp3nS
Turfanská	Turfanský	k2eAgFnSc1d1
proláklina	proláklina	k1gFnSc1
<g/>
,	,	kIx,
nejníže	nízce	k6eAd3
položené	položený	k2eAgNnSc4d1
místo	místo	k1gNnSc4
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosahuje	dosahovat	k5eAaImIp3nS
hloubky	hloubek	k1gInPc4
až	až	k9
154	#num#	k4
metrů	metr	k1gInPc2
pod	pod	k7c7
hladinou	hladina	k1gFnSc7
moře	moře	k1gNnSc2
a	a	k8xC
její	její	k3xOp3gFnSc1
rozloha	rozloha	k1gFnSc1
činí	činit	k5eAaImIp3nS
něco	něco	k3yInSc4
okolo	okolo	k7c2
50	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
rovněž	rovněž	k9
nejsušším	suchý	k2eAgMnSc7d3
a	a	k8xC
nejteplejším	teplý	k2eAgNnSc7d3
místem	místo	k1gNnSc7
Číny	Čína	k1gFnSc2
(	(	kIx(
<g/>
co	co	k9
do	do	k7c2
extrémních	extrémní	k2eAgFnPc2d1
dosažených	dosažený	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kousek	kousek	k1gInSc1
za	za	k7c7
ní	on	k3xPp3gFnSc7
se	se	k3xPyFc4
přitom	přitom	k6eAd1
zdvihají	zdvihat	k5eAaImIp3nP
výběžky	výběžek	k1gInPc1
Tchien-šanu	Tchien-šan	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
ji	on	k3xPp3gFnSc4
oddělují	oddělovat	k5eAaImIp3nP
od	od	k7c2
Džungarské	Džungarský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
na	na	k7c6
severu	sever	k1gInSc6
a	a	k8xC
Tarimské	Tarimský	k2eAgFnPc1d1
pánve	pánev	k1gFnPc1
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Podnebí	podnebí	k1gNnSc1
</s>
<s>
Chaj-nan	Chaj-nan	k1gMnSc1
<g/>
,	,	kIx,
“	“	k?
<g/>
čínská	čínský	k2eAgFnSc1d1
Havaj	Havaj	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
V	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
bývají	bývat	k5eAaImIp3nP
studené	studený	k2eAgFnPc1d1
zimy	zima	k1gFnPc1
a	a	k8xC
horká	horký	k2eAgNnPc4d1
léta	léto	k1gNnPc4
s	s	k7c7
přiměřeným	přiměřený	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
srážek	srážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šanghaj	Šanghaj	k1gFnSc1
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
střední	střední	k2eAgFnSc2d1
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
mírné	mírný	k2eAgFnPc4d1
zimy	zima	k1gFnPc4
a	a	k8xC
vysoké	vysoký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
srážek	srážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
je	být	k5eAaImIp3nS
vlhké	vlhký	k2eAgNnSc4d1
subtropické	subtropický	k2eAgNnSc4d1
podnebí	podnebí	k1gNnSc4
<g/>
,	,	kIx,
na	na	k7c6
západě	západ	k1gInSc6
je	být	k5eAaImIp3nS
podnebí	podnebí	k1gNnSc1
velmi	velmi	k6eAd1
drsné	drsný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lhasa	Lhasa	k1gFnSc1
má	mít	k5eAaImIp3nS
velmi	velmi	k6eAd1
studené	studený	k2eAgFnSc2d1
zimy	zima	k1gFnSc2
a	a	k8xC
málo	málo	k4c4
srážek	srážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
Čína	Čína	k1gFnSc1
leží	ležet	k5eAaImIp3nS
jižněji	jižně	k6eAd2
(	(	kIx(
<g/>
blíže	blízce	k6eAd2
rovníku	rovník	k1gInSc3
<g/>
)	)	kIx)
než	než	k8xS
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
má	můj	k3xOp1gFnSc1
v	v	k7c6
průměru	průměr	k1gInSc6
nižší	nízký	k2eAgFnSc2d2
teploty	teplota	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
v	v	k7c6
nížinných	nížinný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
způsobeno	způsobit	k5eAaPmNgNnS
především	především	k9
dvěma	dva	k4xCgFnPc7
bariérami	bariéra	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zabraňují	zabraňovat	k5eAaImIp3nP
pronikání	pronikání	k1gNnSc4
teplého	teplý	k2eAgInSc2d1
vzduchu	vzduch	k1gInSc2
-	-	kIx~
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
jsou	být	k5eAaImIp3nP
takovou	takový	k3xDgFnSc7
bariérou	bariéra	k1gFnSc7
Himálaje	Himálaj	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tzv.	tzv.	kA
sibiřská	sibiřský	k2eAgFnSc1d1
anticyklóna	anticyklóna	k1gFnSc1
(	(	kIx(
<g/>
či	či	k8xC
též	též	k9
sibiřsko-mongolská	sibiřsko-mongolský	k2eAgFnSc1d1
tlaková	tlakový	k2eAgFnSc1d1
výše	výše	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
počasí	počasí	k1gNnSc6
v	v	k7c6
Číně	Čína	k1gFnSc6
ovlivňují	ovlivňovat	k5eAaImIp3nP
monzuny	monzun	k1gInPc1
(	(	kIx(
<g/>
větry	vítr	k1gInPc1
vznikající	vznikající	k2eAgInPc1d1
z	z	k7c2
rozdílu	rozdíl	k1gInSc2
teplot	teplota	k1gFnPc2
mezi	mezi	k7c7
pevninou	pevnina	k1gFnSc7
a	a	k8xC
oceánem	oceán	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
vzniká	vznikat	k5eAaImIp3nS
odrazem	odraz	k1gInSc7
slunečního	sluneční	k2eAgInSc2d1
svitu	svit	k1gInSc2
od	od	k7c2
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
zpět	zpět	k6eAd1
do	do	k7c2
atmosféry	atmosféra	k1gFnSc2
a	a	k8xC
naopak	naopak	k6eAd1
jeho	jeho	k3xOp3gNnSc7
pohlcováním	pohlcování	k1gNnSc7
vodní	vodní	k2eAgFnPc1d1
masou	masa	k1gFnSc7
oceánu	oceán	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monzunové	monzunový	k2eAgNnSc1d1
proudění	proudění	k1gNnSc1
přichází	přicházet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Číny	Čína	k1gFnSc2
povětšinou	povětšina	k1gFnSc7
z	z	k7c2
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
od	od	k7c2
Jihočínského	jihočínský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
postupem	postup	k1gInSc7
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
slábne	slábnout	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bariérou	bariéra	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
monzunové	monzunový	k2eAgNnSc1d1
proudění	proudění	k1gNnSc1
při	při	k7c6
jeho	jeho	k3xOp3gInSc6
postupu	postup	k1gInSc6
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
povětšinou	povětšinou	k6eAd1
zastaví	zastavit	k5eAaPmIp3nS
<g/>
,	,	kIx,
je	on	k3xPp3gInPc4
pohoří	pohořet	k5eAaPmIp3nS
Cheng-tuan	Cheng-tuan	k1gMnSc1
Šan	Šan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
monzunové	monzunový	k2eAgNnSc1d1
proudění	proudění	k1gNnSc1
nejvíce	nejvíce	k6eAd1,k6eAd3
uplatňuje	uplatňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
rozlišují	rozlišovat	k5eAaImIp3nP
-	-	kIx~
jako	jako	k8xS,k8xC
i	i	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
podobných	podobný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
-	-	kIx~
především	především	k6eAd1
dvě	dva	k4xCgNnPc4
roční	roční	k2eAgNnPc4d1
období	období	k1gNnPc4
<g/>
:	:	kIx,
sucha	sucho	k1gNnSc2
a	a	k8xC
dešťů	dešť	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
mají	mít	k5eAaImIp3nP
vliv	vliv	k1gInSc4
i	i	k8xC
zimní	zimní	k2eAgInPc1d1
monzuny	monzun	k1gInPc1
způsobené	způsobený	k2eAgInPc1d1
sibiřskou	sibiřský	k2eAgFnSc7d1
anticyklónou	anticyklóna	k1gFnSc7
<g/>
,	,	kIx,
posílenou	posílený	k2eAgFnSc4d1
chladným	chladný	k2eAgInSc7d1
proudem	proud	k1gInSc7
Oja-šijo	Oja-šijo	k6eAd1
v	v	k7c6
Japonském	japonský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monzuny	monzun	k1gInPc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
doprovázeny	doprovázet	k5eAaImNgInP
i	i	k9
tajfuny	tajfun	k1gInPc1
(	(	kIx(
<g/>
jak	jak	k8xS,k8xC
v	v	k7c6
Asii	Asie	k1gFnSc6
říkají	říkat	k5eAaImIp3nP
tropickým	tropický	k2eAgFnPc3d1
cyklónám	cyklóna	k1gFnPc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gInSc1
vliv	vliv	k1gInSc1
je	být	k5eAaImIp3nS
silný	silný	k2eAgInSc1d1
jen	jen	k9
na	na	k7c6
čínských	čínský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
či	či	k8xC
na	na	k7c6
jihovýchodním	jihovýchodní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
před	před	k7c7
vstupem	vstup	k1gInSc7
nad	nad	k7c4
čínskou	čínský	k2eAgFnSc4d1
pevninu	pevnina	k1gFnSc4
se	se	k3xPyFc4
tajfuny	tajfun	k1gInPc1
většinou	většinou	k6eAd1
rozbijí	rozbít	k5eAaPmIp3nP
či	či	k8xC
ztratí	ztratit	k5eAaPmIp3nP
sílu	síla	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Himálaj	Himálaj	k1gFnSc1
<g/>
,	,	kIx,
sibiřská	sibiřský	k2eAgFnSc1d1
anticyklóna	anticyklóna	k1gFnSc1
a	a	k8xC
monzunové	monzunový	k2eAgNnSc1d1
proudění	proudění	k1gNnSc1
z	z	k7c2
jihovýchodu	jihovýchod	k1gInSc2
Čínu	Čína	k1gFnSc4
roztínají	roztínat	k5eAaImIp3nP
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
i	i	k8xC
co	co	k9
do	do	k7c2
srážek	srážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klimatologové	klimatolog	k1gMnPc1
tak	tak	k9
rozlišují	rozlišovat	k5eAaImIp3nP
aridní	aridní	k2eAgFnSc4d1
(	(	kIx(
<g/>
suchou	suchý	k2eAgFnSc4d1
<g/>
)	)	kIx)
a	a	k8xC
humidní	humidní	k2eAgFnSc4d1
(	(	kIx(
<g/>
vlhkou	vlhký	k2eAgFnSc4d1
<g/>
)	)	kIx)
Čínu	Čína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Extrémně	extrémně	k6eAd1
málo	málo	k4c1
srážek	srážka	k1gFnPc2
je	být	k5eAaImIp3nS
ve	v	k7c6
srážkovém	srážkový	k2eAgInSc6d1
stínu	stín	k1gInSc6
Himálají	Himálaj	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
aridity	aridita	k1gFnSc2
<g/>
/	/	kIx~
<g/>
humidity	humidita	k1gFnSc2
ovlivnila	ovlivnit	k5eAaPmAgFnS
i	i	k9
kulturní	kulturní	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
aridních	aridní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
se	se	k3xPyFc4
rozvinuly	rozvinout	k5eAaPmAgFnP
pastevecké	pastevecký	k2eAgFnPc1d1
<g/>
,	,	kIx,
kočovné	kočovný	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
humidních	humidní	k2eAgFnPc6d1
zemědělské	zemědělský	k2eAgNnSc4d1
<g/>
,	,	kIx,
usedlé	usedlý	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
vlhkosti	vlhkost	k1gFnSc2
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
i	i	k9
hranicí	hranice	k1gFnSc7
mezi	mezi	k7c7
"	"	kIx"
<g/>
studenou	studený	k2eAgFnSc7d1
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
teplou	teplý	k2eAgFnSc7d1
<g/>
"	"	kIx"
Čínou	Čína	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradice	tradice	k1gFnSc1
za	za	k7c4
hraniční	hraniční	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
považuje	považovat	k5eAaImIp3nS
řeku	řeka	k1gFnSc4
Jang-c	Jang-c	k1gFnSc1
<g/>
’	’	k?
<g/>
-ťiang	-ťiang	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
dlouho	dlouho	k6eAd1
v	v	k7c6
Číně	Čína	k1gFnSc6
dokonce	dokonce	k9
platilo	platit	k5eAaImAgNnS
pravidlo	pravidlo	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
Jang-c	Jang-c	k1gFnSc1
<g/>
’	’	k?
<g/>
-ťiang	-ťiang	k1gInSc1
se	se	k3xPyFc4
staví	stavit	k5eAaBmIp3nS,k5eAaPmIp3nS,k5eAaImIp3nS
budovy	budova	k1gFnPc1
bez	bez	k7c2
vytápění	vytápění	k1gNnSc2
a	a	k8xC
na	na	k7c4
sever	sever	k1gInSc4
s	s	k7c7
vytápěním	vytápění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc4d3
průměrnou	průměrný	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
má	mít	k5eAaImIp3nS
ostrov	ostrov	k1gInSc1
Chaj-nan	Chaj-nan	k1gInSc1
oddělený	oddělený	k2eAgInSc1d1
od	od	k7c2
Lejčouského	Lejčouský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
Chajnanským	Chajnanský	k2eAgInSc7d1
průlivem	průliv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chaj-nan	Chaj-nan	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
nazýván	nazýván	k2eAgInSc1d1
"	"	kIx"
<g/>
čínská	čínský	k2eAgFnSc1d1
Havaj	Havaj	k1gFnSc1
<g/>
"	"	kIx"
a	a	k8xC
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
vyhledávanou	vyhledávaný	k2eAgFnSc7d1
turistickou	turistický	k2eAgFnSc7d1
destinací	destinace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
využitím	využití	k1gNnSc7
Köppen-Geigerovy	Köppen-Geigerův	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
lze	lze	k6eAd1
Čínu	Čína	k1gFnSc4
rozdělit	rozdělit	k5eAaPmF
zejména	zejména	k9
na	na	k7c4
čtyři	čtyři	k4xCgFnPc4
klimatické	klimatický	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
-	-	kIx~
jihovýchod	jihovýchod	k1gInSc1
má	mít	k5eAaImIp3nS
subtropické	subtropický	k2eAgNnSc4d1
klima	klima	k1gNnSc4
<g/>
,	,	kIx,
jihozápad	jihozápad	k1gInSc4
mírné	mírný	k2eAgNnSc1d1
monzunové	monzunový	k2eAgNnSc1d1
klima	klima	k1gNnSc1
<g/>
,	,	kIx,
severovýchod	severovýchod	k1gInSc1
mírné	mírný	k2eAgFnSc2d1
kontinentální	kontinentální	k2eAgFnSc2d1
a	a	k8xC
jihovýchod	jihovýchod	k1gInSc1
<g/>
,	,	kIx,
ovlivněn	ovlivněn	k2eAgInSc1d1
silně	silně	k6eAd1
Himálajemi	Himálaj	k1gFnPc7
<g/>
,	,	kIx,
pak	pak	k6eAd1
alpinské	alpinský	k2eAgNnSc4d1
chladné	chladný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
úzký	úzký	k2eAgInSc4d1
pás	pás	k1gInSc4
pobřeží	pobřeží	k1gNnSc2
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
zejména	zejména	k9
tamní	tamní	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
lze	lze	k6eAd1
označit	označit	k5eAaPmF
za	za	k7c4
tropické	tropický	k2eAgNnSc4d1
monzunové	monzunový	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Vodstvo	vodstvo	k1gNnSc1
</s>
<s>
Jang-c	Jang-c	k1gFnSc1
<g/>
’	’	k?
<g/>
-ťiang	-ťianga	k1gFnPc2
v	v	k7c4
Jün-nana	Jün-nan	k1gMnSc4
</s>
<s>
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
omývána	omývat	k5eAaImNgFnS
čtyřmi	čtyři	k4xCgInPc7
okrajovými	okrajový	k2eAgInPc7d1
moři	moře	k1gNnSc6
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInPc3d3
a	a	k8xC
strategicky	strategicky	k6eAd1
nejvýznamnějším	významný	k2eAgNnSc7d3
mořem	moře	k1gNnSc7
při	při	k7c6
čínském	čínský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
je	být	k5eAaImIp3nS
Jihočínské	jihočínský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Východočínském	východočínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
leží	ležet	k5eAaImIp3nS
sporný	sporný	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šelfové	šelfový	k2eAgNnSc1d1
Žluté	žlutý	k2eAgNnSc1d1
moře	moře	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
název	název	k1gInSc4
podle	podle	k7c2
sprašových	sprašový	k2eAgFnPc2d1
usazenin	usazenina	k1gFnPc2
ze	z	k7c2
Žluté	žlutý	k2eAgFnSc2d1
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gInSc6
okraji	okraj	k1gInSc6
leží	ležet	k5eAaImIp3nS
ještě	ještě	k9
Pochajské	Pochajský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsostné	výsostný	k2eAgFnPc4d1
vody	voda	k1gFnPc4
ČLR	ČLR	kA
(	(	kIx(
<g/>
o	o	k7c4
něž	jenž	k3xRgMnPc4
se	se	k3xPyFc4
nevede	vést	k5eNaImIp3nS
spor	spor	k1gInSc1
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
rozlohu	rozloha	k1gFnSc4
879	#num#	k4
666	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
(	(	kIx(
<g/>
tedy	tedy	k9
zhruba	zhruba	k6eAd1
pětkrát	pětkrát	k6eAd1
méně	málo	k6eAd2
než	než	k8xS
výsostné	výsostný	k2eAgFnPc1d1
vody	voda	k1gFnPc1
Japonska	Japonsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říční	říční	k2eAgInSc4d1
systém	systém	k1gInSc4
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
úmoří	úmoří	k1gNnSc4
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
,	,	kIx,
Indického	indický	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
,	,	kIx,
Severního	severní	k2eAgInSc2d1
ledového	ledový	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
a	a	k8xC
bezodtokou	bezodtoký	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
<g/>
.	.	kIx.
60	#num#	k4
procent	procento	k1gNnPc2
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
odvodňováno	odvodňovat	k5eAaImNgNnS
do	do	k7c2
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
(	(	kIx(
<g/>
východ	východ	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
30	#num#	k4
procent	procento	k1gNnPc2
je	být	k5eAaImIp3nS
bezodtoká	bezodtoký	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
západ	západ	k1gInSc1
Číny	Čína	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
10	#num#	k4
procent	procento	k1gNnPc2
čínských	čínský	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
úmoří	úmoří	k1gNnSc2
Indického	indický	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
(	(	kIx(
<g/>
jihozápad	jihozápad	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedno	jeden	k4xCgNnSc1
procento	procento	k1gNnSc1
pak	pak	k6eAd1
teče	téct	k5eAaImIp3nS
do	do	k7c2
Severního	severní	k2eAgInSc2d1
ledového	ledový	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
-	-	kIx~
malá	malý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Altaj	Altaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
ústí	ústit	k5eAaImIp3nP
čtyři	čtyři	k4xCgFnPc1
nejdůležitější	důležitý	k2eAgFnPc1d3
čínské	čínský	k2eAgFnPc1d1
řeky	řeka	k1gFnPc1
<g/>
:	:	kIx,
Jang-c	Jang-c	k1gInSc1
<g/>
’	’	k?
<g/>
-ťiang	-ťiang	k1gInSc1
(	(	kIx(
<g/>
nejdelší	dlouhý	k2eAgFnSc1d3
řeka	řeka	k1gFnSc1
Asie	Asie	k1gFnSc2
i	i	k8xC
Eurasie	Eurasie	k1gFnSc2
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnSc1
nejdelší	dlouhý	k2eAgFnSc1d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Žlutá	žlutý	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
(	(	kIx(
<g/>
Chuang-che	Chuang-che	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hraniční	hraniční	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
Amur	Amur	k1gInSc1
a	a	k8xC
Perlová	perlový	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohromady	dohromady	k6eAd1
tyto	tento	k3xDgFnPc1
čtyři	čtyři	k4xCgFnPc1
řeky	řeka	k1gFnPc1
odvodňují	odvodňovat	k5eAaImIp3nP
40	#num#	k4
procent	procento	k1gNnPc2
území	území	k1gNnSc2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeky	Řek	k1gMnPc4
Amur	Amur	k1gInSc4
<g/>
,	,	kIx,
Argun	Argun	k1gNnSc4
a	a	k8xC
Ussuri	Ussuri	k1gNnSc4
spolu	spolu	k6eAd1
vytvářejí	vytvářet	k5eAaImIp3nP
největší	veliký	k2eAgFnSc4d3
říční	říční	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
(	(	kIx(
<g/>
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
státy	stát	k1gInPc7
tvořenou	tvořený	k2eAgFnSc7d1
řekou	řeka	k1gFnSc7
<g/>
)	)	kIx)
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oddělují	oddělovat	k5eAaImIp3nP
území	území	k1gNnSc4
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
povodí	povodí	k1gNnSc6
Jang-c	Jang-c	k1gFnSc1
<g/>
’	’	k?
<g/>
-ťiang	-ťianga	k1gFnPc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
dvě	dva	k4xCgNnPc4
největší	veliký	k2eAgNnPc4d3
sladkovodní	sladkovodní	k2eAgNnPc4d1
jezera	jezero	k1gNnPc4
Číny	Čína	k1gFnSc2
-	-	kIx~
Tung-tching-chu	Tung-tching-ch	k1gInSc2
(	(	kIx(
<g/>
5000	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
a	a	k8xC
Pcho-jang-chu	Pcho-jang-cha	k1gFnSc4
(	(	kIx(
<g/>
2700	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc1
rozloha	rozloha	k1gFnSc1
ovšem	ovšem	k9
silně	silně	k6eAd1
kolísá	kolísat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgNnSc7d3
slanovodním	slanovodní	k2eAgNnSc7d1
jezerem	jezero	k1gNnSc7
je	být	k5eAaImIp3nS
Kukunor	Kukunor	k1gInSc1
v	v	k7c6
provincii	provincie	k1gFnSc6
Čching-chaj	Čching-chaj	k1gInSc4
<g/>
,	,	kIx,
často	často	k6eAd1
označované	označovaný	k2eAgFnPc1d1
za	za	k7c4
největší	veliký	k2eAgNnSc4d3
čínské	čínský	k2eAgNnSc4d1
jezero	jezero	k1gNnSc4
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jemu	on	k3xPp3gInSc3
však	však	k9
podle	podle	k7c2
čínských	čínský	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
hrozí	hrozit	k5eAaImIp3nS
vyschnutí	vyschnutí	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
území	území	k1gNnSc2
jí	jíst	k5eAaImIp3nS
nárokovaná	nárokovaný	k2eAgFnSc1d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Administrativní	administrativní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
má	mít	k5eAaImIp3nS
vícestupňový	vícestupňový	k2eAgInSc4d1
správní	správní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
úrovní	úroveň	k1gFnSc7
stojí	stát	k5eAaImIp3nS
provincie	provincie	k1gFnSc1
(	(	kIx(
<g/>
省	省	k?
<g/>
,	,	kIx,
shěng	shěng	k1gMnSc1
<g/>
,	,	kIx,
šeng	šeng	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
ČLR	ČLR	kA
jich	on	k3xPp3gMnPc2
kontroluje	kontrolovat	k5eAaImIp3nS
22	#num#	k4
<g/>
,	,	kIx,
ostrov	ostrov	k1gInSc1
Tchaj-wan	Tchaj-wan	k1gInSc1
je	být	k5eAaImIp3nS
vládou	vláda	k1gFnSc7
ČLR	ČLR	kA
sice	sice	k8xC
považován	považován	k2eAgInSc1d1
za	za	k7c4
další	další	k2eAgFnSc4d1
provincii	provincie	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
fakticky	fakticky	k6eAd1
nad	nad	k7c7
ním	on	k3xPp3gMnSc7
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k6eAd1
provincií	provincie	k1gFnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
ČLR	ČLR	kA
pět	pět	k4xCc4
autonomních	autonomní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
(	(	kIx(
<g/>
自	自	k?
<g/>
,	,	kIx,
zì	zì	k?
<g/>
,	,	kIx,
c	c	k0
<g/>
'	'	kIx"
<g/>
-č	-č	k?
<g/>
'	'	kIx"
<g/>
-čchü	-čchü	k?
<g/>
)	)	kIx)
zahrnujících	zahrnující	k2eAgNnPc2d1
území	území	k1gNnPc2
obydlené	obydlený	k2eAgFnPc1d1
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
příslušníky	příslušník	k1gMnPc7
národnostních	národnostní	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgInPc4
ústřední	ústřední	k2eAgInPc4d1
vládou	vláda	k1gFnSc7
přímo	přímo	k6eAd1
spravovaná	spravovaný	k2eAgNnPc1d1
města	město	k1gNnPc1
(	(	kIx(
<g/>
直	直	k?
<g/>
,	,	kIx,
zhíxiáshì	zhíxiáshì	k?
<g/>
,	,	kIx,
č	č	k0
<g/>
'	'	kIx"
<g/>
-sia-š	-sia-š	k1gInSc4
<g/>
'	'	kIx"
<g/>
)	)	kIx)
a	a	k8xC
dvě	dva	k4xCgFnPc1
zvláštní	zvláštní	k2eAgFnPc1d1
správní	správní	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
(	(	kIx(
<g/>
特	特	k?
<g/>
,	,	kIx,
tè	tè	k6eAd1
xíngzhè	xíngzhè	k?
<g/>
,	,	kIx,
tche-pie	tche-pie	k1gFnSc1
sing-čeng-čchü	sing-čeng-čchü	k?
<g/>
)	)	kIx)
těšící	těšící	k2eAgFnSc3d1
se	se	k3xPyFc4
velké	velký	k2eAgFnSc3d1
míře	míra	k1gFnSc3
autonomie	autonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
provincie	provincie	k1gFnPc1
a	a	k8xC
autonomní	autonomní	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
rozděleny	rozdělit	k5eAaPmNgFnP
na	na	k7c4
autonomní	autonomní	k2eAgFnPc4d1
prefektury	prefektura	k1gFnPc4
<g/>
,	,	kIx,
okresy	okres	k1gInPc4
<g/>
,	,	kIx,
autonomní	autonomní	k2eAgInPc4d1
okresy	okres	k1gInPc4
a	a	k8xC
města	město	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
ty	ten	k3xDgMnPc4
dále	daleko	k6eAd2
na	na	k7c4
města	město	k1gNnPc4
a	a	k8xC
obce	obec	k1gFnPc4
včetně	včetně	k7c2
měst	město	k1gNnPc2
a	a	k8xC
obcí	obec	k1gFnPc2
etnických	etnický	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
provincií	provincie	k1gFnPc2
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Český	český	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Čínský	čínský	k2eAgInSc1d1
název	název	k1gInSc1
<g/>
(	(	kIx(
<g/>
přepis	přepis	k1gInSc1
pchin-jin	pchin-jin	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Status	status	k1gInSc1
</s>
<s>
An-chuej安	An-chuej安	k?
(	(	kIx(
<g/>
Ā	Ā	k?
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc2
</s>
<s>
Če-ťiang浙	Če-ťiang浙	k?
(	(	kIx(
<g/>
Zhè	Zhè	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Čching-chaj青	Čching-chaj青	k?
(	(	kIx(
<g/>
Qī	Qī	k1gFnSc2
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc2
</s>
<s>
Čchung-čching重	Čchung-čching重	k?
(	(	kIx(
<g/>
Chóngqì	Chóngqì	k1gMnSc1
<g/>
)	)	kIx)
<g/>
přímo	přímo	k6eAd1
spravované	spravovaný	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Fu-ťien福	Fu-ťien福	k?
(	(	kIx(
<g/>
Fújià	Fújià	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Hongkong香	Hongkong香	k?
(	(	kIx(
<g/>
Xiā	Xiā	k1gMnSc1
<g/>
)	)	kIx)
<g/>
zvláštní	zvláštní	k2eAgFnSc1d1
správní	správní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
ZSO	ZSO	kA
<g/>
)	)	kIx)
</s>
<s>
Chaj-nan海	Chaj-nan海	k?
(	(	kIx(
<g/>
Hǎ	Hǎ	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc2
</s>
<s>
Che-nan河	Che-nan河	k?
(	(	kIx(
<g/>
Hénán	Hénán	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Che-pej河	Che-pej河	k?
(	(	kIx(
<g/>
Héběi	Hébě	k1gFnSc2
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc2
</s>
<s>
Chej-lung-ťiang黑	Chej-lung-ťiang黑	k?
(	(	kIx(
<g/>
Hē	Hē	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Chu-nan湖	Chu-nan湖	k?
(	(	kIx(
<g/>
Húnán	Húnán	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Chu-pej湖	Chu-pej湖	k?
(	(	kIx(
<g/>
Húběi	Húbě	k1gFnSc2
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc2
</s>
<s>
Jün-nan云	Jün-nan云	k?
(	(	kIx(
<g/>
Yúnnán	Yúnnán	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc2
</s>
<s>
Kan-su甘	Kan-su甘	k?
(	(	kIx(
<g/>
Gā	Gā	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Kuang-si广	Kuang-si广	k?
(	(	kIx(
<g/>
Guǎ	Guǎ	k1gFnSc1
<g/>
)	)	kIx)
<g/>
autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
AO	AO	kA
<g/>
)	)	kIx)
</s>
<s>
Kuang-tung广	Kuang-tung广	k?
(	(	kIx(
<g/>
Guǎ	Guǎ	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Kuej-čou贵	Kuej-čou贵	k?
(	(	kIx(
<g/>
Guì	Guì	k1gMnSc6
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc2
</s>
<s>
Liao-ning辽	Liao-ning辽	k?
(	(	kIx(
<g/>
Liáoníng	Liáoníng	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Macao澳	Macao澳	k?
(	(	kIx(
<g/>
À	À	k1gInSc1
<g/>
)	)	kIx)
<g/>
zvláštní	zvláštní	k2eAgFnSc1d1
správní	správní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
ZSO	ZSO	kA
<g/>
)	)	kIx)
</s>
<s>
Ning-sia宁	Ning-sia宁	k?
(	(	kIx(
<g/>
Níngxià	Níngxià	k1gFnSc1
<g/>
)	)	kIx)
<g/>
autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
AO	AO	kA
<g/>
)	)	kIx)
</s>
<s>
Peking北	Peking北	k?
(	(	kIx(
<g/>
Běijī	Běijī	k1gMnSc1
<g/>
)	)	kIx)
<g/>
přímo	přímo	k6eAd1
spravované	spravovaný	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
S	s	k7c7
<g/>
’	’	k?
<g/>
-čchuan四	-čchuan四	k?
(	(	kIx(
<g/>
Sì	Sì	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Sin-ťiang新	Sin-ťiang新	k?
(	(	kIx(
<g/>
Xī	Xī	k1gMnSc1
<g/>
)	)	kIx)
<g/>
autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
AO	AO	kA
<g/>
)	)	kIx)
</s>
<s>
Šan-si山	Šan-si山	k?
(	(	kIx(
<g/>
Shā	Shā	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Šan-tung山	Šan-tung山	k?
(	(	kIx(
<g/>
Shā	Shā	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Šanghaj上	Šanghaj上	k?
(	(	kIx(
<g/>
Shà	Shà	k1gFnSc2
<g/>
)	)	kIx)
<g/>
přímo	přímo	k6eAd1
spravované	spravovaný	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Šen-si陝	Šen-si陝	k?
(	(	kIx(
<g/>
Shǎ	Shǎ	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
[	[	kIx(
<g/>
p.	p.	k?
1	#num#	k4
<g/>
]	]	kIx)
<g/>
台	台	k?
(	(	kIx(
<g/>
Táiwā	Táiwā	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Tchien-ťin天	Tchien-ťin天	k?
(	(	kIx(
<g/>
Tiā	Tiā	k1gMnSc1
<g/>
)	)	kIx)
<g/>
přímo	přímo	k6eAd1
spravované	spravovaný	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Tibet西	Tibet西	k?
(	(	kIx(
<g/>
Xī	Xī	k1gMnSc1
<g/>
)	)	kIx)
<g/>
autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
AO	AO	kA
<g/>
)	)	kIx)
</s>
<s>
Ťi-lin吉	Ťi-lin吉	k?
(	(	kIx(
<g/>
Jílín	Jílín	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Ťiang-si江	Ťiang-si江	k?
(	(	kIx(
<g/>
Jiā	Jiā	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Ťiang-su江	Ťiang-su江	k?
(	(	kIx(
<g/>
Jiā	Jiā	k1gMnSc1
<g/>
)	)	kIx)
<g/>
provincie	provincie	k1gFnSc1
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
Mongolsko内	Mongolsko内	k1gFnSc1
(	(	kIx(
<g/>
Nè	Nè	k1gNnSc1
Měnggǔ	Měnggǔ	k1gFnSc1
<g/>
)	)	kIx)
<g/>
autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
AO	AO	kA
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
považuje	považovat	k5eAaImIp3nS
Tchaj-wan	Tchaj-wan	k1gInSc4
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
23	#num#	k4
<g/>
.	.	kIx.
provincii	provincie	k1gFnSc6
<g/>
,	,	kIx,
přestože	přestože	k8xS
jej	on	k3xPp3gMnSc4
fakticky	fakticky	k6eAd1
neovládá	ovládat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Města	město	k1gNnSc2
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
10	#num#	k4
miliony	milion	k4xCgInPc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Šanghaj	Šanghaj	k1gFnSc1
(	(	kIx(
<g/>
Pchu-tung	Pchu-tung	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
24,2	24,2	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Peking	Peking	k1gInSc1
<g/>
,	,	kIx,
21,7	21,7	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tchien-ťin	Tchien-ťin	k1gMnSc1
<g/>
,	,	kIx,
15,6	15,6	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kanton	Kanton	k1gInSc1
<g/>
,	,	kIx,
14,5	14,5	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Čcheng-tu	Čcheng-ta	k1gFnSc4
<g/>
,	,	kIx,
14,4	14,4	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Šen-čen	Šen-čen	k2eAgMnSc1d1
<g/>
,	,	kIx,
12,5	12,5	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Wu-chan	Wu-chan	k1gMnSc1
<g/>
,	,	kIx,
10,6	10,6	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Čínský	čínský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Chu	Chu	k1gMnSc1
Ťin-tchao	Ťin-tchao	k1gMnSc1
a	a	k8xC
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Bush	Bush	k1gMnSc1
na	na	k7c6
summitu	summit	k1gInSc6
států	stát	k1gInPc2
APEC	APEC	kA
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
</s>
<s>
Čínský	čínský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
na	na	k7c4
setkání	setkání	k1gNnPc4
představitelů	představitel	k1gMnPc2
států	stát	k1gInPc2
BRICS	BRICS	kA
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
je	být	k5eAaImIp3nS
totalitní	totalitní	k2eAgInSc4d1
režim	režim	k1gInSc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
do	do	k7c2
současnosti	současnost	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
jejím	její	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
vystřídalo	vystřídat	k5eAaPmAgNnS
pět	pět	k4xCc1
„	„	k?
<g/>
generací	generace	k1gFnPc2
vůdců	vůdce	k1gMnPc2
<g/>
“	“	k?
<g/>
:	:	kIx,
</s>
<s>
První	první	k4xOgFnSc1
válečná	válečný	k2eAgFnSc1d1
generace	generace	k1gFnSc1
</s>
<s>
Mao	Mao	k?
Ce-tung	Ce-tung	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
1945	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
1954	#num#	k4
<g/>
–	–	k?
<g/>
1959	#num#	k4
</s>
<s>
Čou	Čou	k?
En-laj	En-laj	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
1949	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
</s>
<s>
Ču	Ču	k?
Te	Te	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
stálého	stálý	k2eAgInSc2d1
výboru	výbor	k1gInSc2
Všečínského	všečínský	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
lidových	lidový	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
1959	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
(	(	kIx(
<g/>
hlava	hlava	k1gFnSc1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Liou	Lio	k2eAgFnSc4d1
Šao-čchi	Šao-čche	k1gFnSc4
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
1959	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
</s>
<s>
Druhá	druhý	k4xOgFnSc1
válečná	válečný	k2eAgFnSc1d1
generace	generace	k1gFnSc1
</s>
<s>
Teng	Teng	k1gInSc1
Siao-pching	Siao-pching	k1gInSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
vojenské	vojenský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
1981	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
,	,	kIx,
Maův	Maův	k1gMnSc1
nástupce	nástupce	k1gMnSc1
<g/>
,	,	kIx,
provedl	provést	k5eAaPmAgMnS
ekonomické	ekonomický	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
a	a	k8xC
otevřel	otevřít	k5eAaPmAgMnS
Čínu	Čína	k1gFnSc4
zahraničnímu	zahraniční	k2eAgInSc3d1
kapitálu	kapitál	k1gInSc3
</s>
<s>
Chua	Chua	k1gMnSc1
Kuo-feng	Kuo-feng	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
1976	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
1976	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
</s>
<s>
Chu	Chu	k?
Jao-pang	Jao-pang	k1gMnSc1
<g/>
,	,	kIx,
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
strany	strana	k1gFnSc2
1980	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
</s>
<s>
Třetí	třetí	k4xOgFnSc1
generace	generace	k1gFnSc1
(	(	kIx(
<g/>
vzešlá	vzešlý	k2eAgFnSc1d1
z	z	k7c2
ředitelů	ředitel	k1gMnPc2
státních	státní	k2eAgInPc2d1
podniků	podnik	k1gInPc2
vzdělaných	vzdělaný	k2eAgInPc2d1
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Čao	čao	k0
C	C	kA
<g/>
'	'	kIx"
<g/>
-jang	-jang	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
1983	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
<g/>
,	,	kIx,
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
strany	strana	k1gFnSc2
1987	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
</s>
<s>
Ťiang	Ťiang	k1gMnSc1
Ce-min	Ce-min	k2eAgMnSc1d1
<g/>
,	,	kIx,
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
strany	strana	k1gFnSc2
1989	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
1993	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
</s>
<s>
Li	li	k8xS
Pcheng	Pcheng	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
1988	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
</s>
<s>
Ču	Ču	k?
Žung-ťi	Žung-ťi	k1gNnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
1998	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
generace	generace	k1gFnSc1
<g/>
,	,	kIx,
technokratická	technokratický	k2eAgFnSc1d1
</s>
<s>
Chu	Chu	k?
Ťin-tchao	Ťin-tchao	k1gMnSc1
<g/>
,	,	kIx,
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
strany	strana	k1gFnSc2
2002	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
2003	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
</s>
<s>
Wen	Wen	k?
Ťia-pao	Ťia-pao	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
od	od	k7c2
2003	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
</s>
<s>
Pátá	pátý	k4xOgFnSc1
generace	generace	k1gFnSc1
</s>
<s>
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
<g/>
,	,	kIx,
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
strany	strana	k1gFnSc2
od	od	k7c2
2012	#num#	k4
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
od	od	k7c2
2013	#num#	k4
</s>
<s>
Li	li	k8xS
Kche-čchiang	Kche-čchiang	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
od	od	k7c2
2013	#num#	k4
</s>
<s>
Čtyři	čtyři	k4xCgInPc1
základní	základní	k2eAgInPc1d1
principy	princip	k1gInPc1
</s>
<s>
Základním	základní	k2eAgInSc7d1
pilířem	pilíř	k1gInSc7
politického	politický	k2eAgInSc2d1
systému	systém	k1gInSc2
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
jsou	být	k5eAaImIp3nP
čtyři	čtyři	k4xCgNnPc4
nezpochybnitelná	zpochybnitelný	k2eNgNnPc4d1
dogmata	dogma	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
jsou	být	k5eAaImIp3nP
od	od	k7c2
roku	rok	k1gInSc2
1982	#num#	k4
součástí	součást	k1gFnSc7
Ústavy	ústava	k1gFnSc2
ČLR	ČLR	kA
a	a	k8xC
vytvářejí	vytvářet	k5eAaImIp3nP
rámec	rámec	k1gInSc4
pro	pro	k7c4
čínský	čínský	k2eAgInSc4d1
právní	právní	k2eAgInSc4d1
řád	řád	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
Musíme	muset	k5eAaImIp1nP
zachovat	zachovat	k5eAaPmF
cestu	cesta	k1gFnSc4
socialismu	socialismus	k1gInSc2
(	(	kIx(
<g/>
必	必	k?
<g/>
)	)	kIx)
</s>
<s>
Musíme	muset	k5eAaImIp1nP
zachovat	zachovat	k5eAaPmF
demokratickou	demokratický	k2eAgFnSc4d1
diktaturu	diktatura	k1gFnSc4
lidu	lid	k1gInSc2
(	(	kIx(
<g/>
必	必	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
původně	původně	k6eAd1
diktaturu	diktatura	k1gFnSc4
proletariátu	proletariát	k1gInSc2
无	无	k?
<g/>
)	)	kIx)
</s>
<s>
Musíme	muset	k5eAaImIp1nP
zachovat	zachovat	k5eAaPmF
vedoucí	vedoucí	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
必	必	k?
<g/>
)	)	kIx)
</s>
<s>
Musíme	muset	k5eAaImIp1nP
zachovat	zachovat	k5eAaPmF
marxismus-leninismus	marxismus-leninismus	k1gInSc4
a	a	k8xC
Mao	Mao	k1gFnSc4
Ce-tungovo	Ce-tungův	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
(	(	kIx(
<g/>
必	必	k?
<g/>
、	、	k?
<g/>
毛	毛	k?
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
vůdčí	vůdčí	k2eAgFnSc4d1
ideu	idea	k1gFnSc4
strany	strana	k1gFnSc2
a	a	k8xC
státu	stát	k1gInSc2
</s>
<s>
Tyto	tento	k3xDgInPc1
principy	princip	k1gInPc1
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
projevech	projev	k1gInPc6
často	často	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
nejvyšším	vysoký	k2eAgNnSc6d3
vedení	vedení	k1gNnSc6
KS	ks	kA
Číny	Čína	k1gFnSc2
znovu	znovu	k6eAd1
panuje	panovat	k5eAaImIp3nS
přesvědčení	přesvědčení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
ideologie	ideologie	k1gFnSc1
je	být	k5eAaImIp3nS
důležitější	důležitý	k2eAgFnSc1d2
než	než	k8xS
ekonomická	ekonomický	k2eAgFnSc1d1
základna	základna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Zahraniční	zahraniční	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šanghajská	šanghajský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
spolupráci	spolupráce	k1gFnSc4
<g/>
:	:	kIx,
Členové	člen	k1gMnPc1
Pozorovatelé	pozorovatel	k1gMnPc1
Partnerský	partnerský	k2eAgInSc4d1
dialog	dialog	k1gInSc4
Zájemci	zájemce	k1gMnPc1
o	o	k7c6
pozorování	pozorování	k1gNnSc6
Sporná	sporný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
ČLR	ČLR	kA
má	mít	k5eAaImIp3nS
diplomatické	diplomatický	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
se	s	k7c7
175	#num#	k4
zeměmi	zem	k1gFnPc7
a	a	k8xC
udržuje	udržovat	k5eAaImIp3nS
velvyslanectví	velvyslanectví	k1gNnSc1
ve	v	k7c6
162	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
legitimita	legitimita	k1gFnSc1
je	být	k5eAaImIp3nS
zpochybňována	zpochybňovat	k5eAaImNgFnS
Čínskou	čínský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
a	a	k8xC
několika	několik	k4yIc7
dalšími	další	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
;	;	kIx,
jde	jít	k5eAaImIp3nS
tedy	tedy	k9
o	o	k7c4
největší	veliký	k2eAgInSc4d3
a	a	k8xC
nejlidnatější	lidnatý	k2eAgInSc4d3
stát	stát	k1gInSc4
s	s	k7c7
omezeným	omezený	k2eAgNnSc7d1
uznáním	uznání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
nahradila	nahradit	k5eAaPmAgFnS
ČLR	ČLR	kA
Čínskou	čínský	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
jako	jako	k8xS,k8xC
výhradního	výhradní	k2eAgMnSc4d1
zástupce	zástupce	k1gMnSc4
Číny	Čína	k1gFnSc2
v	v	k7c6
OSN	OSN	kA
a	a	k8xC
jako	jako	k8xC,k8xS
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
pěti	pět	k4xCc2
stálých	stálý	k2eAgMnPc2d1
členů	člen	k1gMnPc2
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
bývalý	bývalý	k2eAgMnSc1d1
člen	člen	k1gMnSc1
a	a	k8xC
lídr	lídr	k1gMnSc1
hnutí	hnutí	k1gNnSc2
nezúčastněných	zúčastněný	k2eNgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
stále	stále	k6eAd1
sebe	sebe	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
obhájce	obhájce	k1gMnSc4
rozvojových	rozvojový	k2eAgFnPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Spolu	spolu	k6eAd1
s	s	k7c7
Brazílií	Brazílie	k1gFnSc7
<g/>
,	,	kIx,
Ruskem	Rusko	k1gNnSc7
<g/>
,	,	kIx,
Indií	Indie	k1gFnSc7
a	a	k8xC
Jihoafrickou	jihoafrický	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
členem	člen	k1gInSc7
BRICS	BRICS	kA
–	–	k?
skupiny	skupina	k1gFnSc2
rozvíjejících	rozvíjející	k2eAgFnPc2d1
se	se	k3xPyFc4
hlavních	hlavní	k2eAgFnPc2d1
ekonomik	ekonomika	k1gFnPc2
a	a	k8xC
hostila	hostit	k5eAaImAgFnS
třetí	třetí	k4xOgInSc4
oficiální	oficiální	k2eAgInSc4d1
summit	summit	k1gInSc4
v	v	k7c4
San-ja	San-jum	k1gNnPc4
na	na	k7c4
Chaj-nanu	Chaj-nana	k1gFnSc4
v	v	k7c6
dubnu	duben	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Pod	pod	k7c7
výkladem	výklad	k1gInSc7
politiky	politika	k1gFnSc2
jedné	jeden	k4xCgFnSc2
Číny	Čína	k1gFnSc2
Peking	Peking	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
takovou	takový	k3xDgFnSc4
podmínku	podmínka	k1gFnSc4
k	k	k7c3
navázání	navázání	k1gNnSc3
diplomatických	diplomatický	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
,	,	kIx,
že	že	k8xS
druhá	druhý	k4xOgFnSc1
země	země	k1gFnSc1
uznává	uznávat	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
požadavek	požadavek	k1gInSc4
na	na	k7c4
Tchaj-wan	Tchaj-wan	k1gInSc4
a	a	k8xC
zpřetrhává	zpřetrhávat	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnPc4d1
diplomatické	diplomatický	k2eAgFnPc4d1
vazby	vazba	k1gFnPc4
s	s	k7c7
Čínskou	čínský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínští	čínský	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
protestovali	protestovat	k5eAaBmAgMnP
při	při	k7c6
mnoha	mnoho	k4c6
příležitostech	příležitost	k1gFnPc6
<g/>
,	,	kIx,
když	když	k8xS
zahraniční	zahraniční	k2eAgFnPc1d1
země	zem	k1gFnPc1
učinily	učinit	k5eAaPmAgFnP,k5eAaImAgFnP
diplomatické	diplomatický	k2eAgInPc4d1
pokusy	pokus	k1gInPc4
o	o	k7c6
sblížení	sblížení	k1gNnSc6
s	s	k7c7
Tchaj-wanem	Tchaj-wan	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
zejména	zejména	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
prodeje	prodej	k1gInSc2
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Mnoho	mnoho	k6eAd1
ze	z	k7c2
současné	současný	k2eAgFnSc2d1
čínské	čínský	k2eAgFnSc2d1
zahraniční	zahraniční	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
politice	politika	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
premiéra	premiéra	k1gFnSc1
Čou	Čou	k1gFnSc1
En-laje	En-laje	k1gFnSc1
-	-	kIx~
Panča	Panča	k1gFnSc1
šíla	šíl	k1gMnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
také	také	k6eAd1
poháněna	pohánět	k5eAaImNgFnS
pojmem	pojem	k1gInSc7
"	"	kIx"
<g/>
harmonie	harmonie	k1gFnSc2
bez	bez	k7c2
uniformity	uniformita	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
podporuje	podporovat	k5eAaImIp3nS
diplomatické	diplomatický	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
mezi	mezi	k7c4
státy	stát	k1gInPc4
navzdory	navzdory	k7c3
ideologickým	ideologický	k2eAgInPc3d1
rozdílům	rozdíl	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
politika	politika	k1gFnSc1
zřejmě	zřejmě	k6eAd1
vedla	vést	k5eAaImAgFnS
Čínu	Čína	k1gFnSc4
k	k	k7c3
podpoře	podpora	k1gFnSc3
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
západními	západní	k2eAgInPc7d1
<g />
.	.	kIx.
</s>
<s hack="1">
státy	stát	k1gInPc1
označované	označovaný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
darebácké	darebácký	k2eAgInPc1d1
a	a	k8xC
represivní	represivní	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
Zimbabwe	Zimbabwe	k1gNnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
a	a	k8xC
Írán	Írán	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Čína	Čína	k1gFnSc1
má	mít	k5eAaImIp3nS
úzké	úzký	k2eAgInPc4d1
hospodářské	hospodářský	k2eAgInPc4d1
a	a	k8xC
vojenské	vojenský	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
tyto	tento	k3xDgInPc4
dva	dva	k4xCgInPc4
státy	stát	k1gInPc4
často	často	k6eAd1
hlasují	hlasovat	k5eAaImIp3nP
v	v	k7c6
Radě	rada	k1gFnSc6
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
jednotně	jednotně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Územní	územní	k2eAgInPc1d1
spory	spor	k1gInPc1
</s>
<s>
Od	od	k7c2
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
po	po	k7c6
druhé	druhý	k4xOgFnSc6
čínské	čínský	k2eAgFnSc6d1
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
si	se	k3xPyFc3
ČLR	ČLR	kA
nárokuje	nárokovat	k5eAaImIp3nS
území	území	k1gNnSc4
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
Čínské	čínský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
oddělené	oddělený	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
entity	entita	k1gFnSc2
dnes	dnes	k6eAd1
známé	známý	k2eAgInPc1d1
jako	jako	k8xS,k8xC
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
svého	svůj	k3xOyFgNnSc2
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
Tchaj-wan	Tchaj-wan	k1gInSc4
pokládá	pokládat	k5eAaImIp3nS
za	za	k7c4
své	svůj	k3xOyFgNnSc4
území	území	k1gNnSc4
jako	jako	k8xS,k8xC
provincii	provincie	k1gFnSc4
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Ťin-men	Ťin-men	k1gInSc4
a	a	k8xC
Ma-cu	Ma-ca	k1gFnSc4
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc4
provincie	provincie	k1gFnSc2
Fu-ťien	Fu-ťien	k1gInSc1
a	a	k8xC
ostrovy	ostrov	k1gInPc1
v	v	k7c6
Jihočínském	jihočínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
Čínské	čínský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
provincie	provincie	k1gFnSc2
Chaj-nan	Chaj-nana	k1gFnPc2
a	a	k8xC
provincie	provincie	k1gFnSc2
Kuang-tung	Kuang-tunga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
tvrzení	tvrzení	k1gNnPc4
jsou	být	k5eAaImIp3nP
kontroverzní	kontroverzní	k2eAgInPc1d1
kvůli	kvůli	k7c3
komplikovaným	komplikovaný	k2eAgInPc3d1
vztahům	vztah	k1gInPc3
v	v	k7c6
okolí	okolí	k1gNnSc6
úžiny	úžina	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
ČLR	ČLR	kA
operuje	operovat	k5eAaImIp3nS
s	s	k7c7
politikou	politika	k1gFnSc7
jedné	jeden	k4xCgFnSc2
Číny	Čína	k1gFnSc2
jako	jako	k9
s	s	k7c7
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejdůležitějších	důležitý	k2eAgInPc2d3
diplomatických	diplomatický	k2eAgInPc2d1
principů	princip	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
Tchaj-wanu	Tchaj-wan	k1gInSc2
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
zapojena	zapojen	k2eAgFnSc1d1
do	do	k7c2
dalších	další	k2eAgInPc2d1
územních	územní	k2eAgInPc2d1
sporů	spor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
zapojená	zapojený	k2eAgFnSc1d1
do	do	k7c2
jednání	jednání	k1gNnSc2
vyřešit	vyřešit	k5eAaPmF
své	svůj	k3xOyFgFnPc4
sporné	sporný	k2eAgFnPc4d1
pozemní	pozemní	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
hranice	hranice	k1gFnSc2
s	s	k7c7
Indií	Indie	k1gFnSc7
a	a	k8xC
nedefinované	definovaný	k2eNgFnPc4d1
hranice	hranice	k1gFnPc4
s	s	k7c7
Bhútánem	Bhútán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
zapojena	zapojit	k5eAaPmNgFnS
do	do	k7c2
mnohostranných	mnohostranný	k2eAgInPc2d1
sporů	spor	k1gInPc2
o	o	k7c6
vlastnictví	vlastnictví	k1gNnSc6
několika	několik	k4yIc2
malých	malý	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
jihu	jih	k1gInSc6
Číny	Čína	k1gFnSc2
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
ostrovy	ostrov	k1gInPc1
Senkaku	Senkak	k1gInSc2
a	a	k8xC
mělčina	mělčina	k1gFnSc1
Scarborough	Scarborougha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úsilí	úsilí	k1gNnSc1
o	o	k7c4
globální	globální	k2eAgFnSc4d1
nadvládu	nadvláda	k1gFnSc4
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
oficiální	oficiální	k2eAgFnSc7d1
strategií	strategie	k1gFnSc7
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
stala	stát	k5eAaPmAgFnS
"	"	kIx"
<g/>
globální	globální	k2eAgFnSc1d1
mediální	mediální	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
se	se	k3xPyFc4
ČLR	ČLR	kA
pokouší	pokoušet	k5eAaImIp3nP
různými	různý	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
ovládnout	ovládnout	k5eAaPmF
mediální	mediální	k2eAgInSc4d1
obraz	obraz	k1gInSc4
Číny	Čína	k1gFnSc2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostřednictvím	prostřednictvím	k7c2
státních	státní	k2eAgFnPc2d1
novin	novina	k1gFnPc2
China	China	k1gFnSc1
Daily	Daila	k1gFnSc2
investuje	investovat	k5eAaBmIp3nS
obrovské	obrovský	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
do	do	k7c2
reklamy	reklama	k1gFnSc2
v	v	k7c6
prestižních	prestižní	k2eAgInPc6d1
západních	západní	k2eAgInPc6d1
médiích	médium	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
kupuje	kupovat	k5eAaImIp3nS
prostor	prostor	k1gInSc4
pro	pro	k7c4
přílohy	příloha	k1gFnPc4
s	s	k7c7
propagandistickým	propagandistický	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
"	"	kIx"
<g/>
China	China	k1gFnSc1
Watch	Watch	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
v	v	k7c6
USA	USA	kA
takto	takto	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
investovala	investovat	k5eAaBmAgFnS
20,8	20,8	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
britském	britský	k2eAgNnSc6d1
The	The	k1gMnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegraph	k1gMnSc1
si	se	k3xPyFc3
čínská	čínský	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
předplatila	předplatit	k5eAaPmAgFnS
vlastní	vlastní	k2eAgInPc4d1
články	článek	k1gInPc4
za	za	k7c4
750	#num#	k4
000	#num#	k4
£	£	k?
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
placené	placený	k2eAgFnSc2d1
internetové	internetový	k2eAgFnSc2d1
verze	verze	k1gFnSc2
britského	britský	k2eAgInSc2d1
deníku	deník	k1gInSc2
poskytovány	poskytovat	k5eAaImNgFnP
čtenářům	čtenář	k1gMnPc3
zdarma	zdarma	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
afrických	africký	k2eAgInPc6d1
státech	stát	k1gInPc6
Čína	Čína	k1gFnSc1
využila	využít	k5eAaPmAgFnS
přechodu	přechod	k1gInSc3
televizního	televizní	k2eAgNnSc2d1
vysílání	vysílání	k1gNnSc2
z	z	k7c2
analogového	analogový	k2eAgInSc2d1
na	na	k7c4
digitální	digitální	k2eAgInSc4d1
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
sítě	síť	k1gFnSc2
optických	optický	k2eAgInPc2d1
kabelů	kabel	k1gInPc2
a	a	k8xC
satelitů	satelit	k1gInPc2
a	a	k8xC
prostřednictvím	prostřednictvím	k7c2
své	svůj	k3xOyFgFnSc2
společnosti	společnost	k1gFnSc2
StarTimes	StarTimesa	k1gFnPc2
zároveň	zároveň	k6eAd1
nabídla	nabídnout	k5eAaPmAgFnS
levné	levný	k2eAgNnSc4d1
předplatné	předplatné	k1gNnSc4
na	na	k7c4
mix	mix	k1gInSc4
afrických	africký	k2eAgFnPc2d1
a	a	k8xC
čínských	čínský	k2eAgFnPc2d1
vysílacích	vysílací	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státní	státní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
Xinhua	Xinhuum	k1gNnSc2
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
přeplatit	přeplatit	k5eAaPmF
a	a	k8xC
získat	získat	k5eAaPmF
novináře	novinář	k1gMnPc4
ze	z	k7c2
západních	západní	k2eAgNnPc2d1
médií	médium	k1gNnPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
zahraniční	zahraniční	k2eAgInSc4d1
Twitterový	Twitterový	k2eAgInSc4d1
účet	účet	k1gInSc4
s	s	k7c7
11,7	11,7	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
uživatelů	uživatel	k1gMnPc2
(	(	kIx(
<g/>
přestože	přestože	k8xS
v	v	k7c6
ČLR	ČLR	kA
je	být	k5eAaImIp3nS
Twitter	Twitter	k1gInSc1
zakázán	zakázat	k5eAaPmNgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
svědectví	svědectví	k1gNnSc2
novinářů	novinář	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
čínských	čínský	k2eAgNnPc6d1
médiích	médium	k1gNnPc6
zaměstnáni	zaměstnat	k5eAaPmNgMnP
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
jejich	jejich	k3xOp3gFnSc1
práce	práce	k1gFnSc1
povahu	povaha	k1gFnSc4
žurnalismu	žurnalismus	k1gInSc2
s	s	k7c7
kreativním	kreativní	k2eAgInSc7d1
propagandistickým	propagandistický	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
<g/>
,	,	kIx,
často	často	k6eAd1
na	na	k7c4
hranici	hranice	k1gFnSc4
nízkoúrovňové	nízkoúrovňový	k2eAgFnSc2d1
špionáže	špionáž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Skupina	skupina	k1gFnSc1
Global	globat	k5eAaImAgInS
CAMG	CAMG	kA
Media	medium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
čínským	čínský	k2eAgMnSc7d1
podnikatelem	podnikatel	k1gMnSc7
Tommy	Tomma	k1gFnSc2
Jiangem	Jiang	k1gInSc7
a	a	k8xC
registrovaná	registrovaný	k2eAgNnPc1d1
v	v	k7c4
Melbourne	Melbourne	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
tří	tři	k4xCgFnPc2
podobných	podobný	k2eAgFnPc2d1
korporací	korporace	k1gFnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
z	z	k7c2
60	#num#	k4
%	%	kIx~
vlastněna	vlastněn	k2eAgFnSc1d1
státním	státní	k2eAgNnPc3d1
provozovatelem	provozovatel	k1gMnSc7
China	China	k1gFnSc1
Radio	radio	k1gNnSc1
International	International	k1gFnSc2
(	(	kIx(
<g/>
CRI	CRI	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strategií	strategie	k1gFnPc2
CRI	CRI	kA
je	být	k5eAaImIp3nS
"	"	kIx"
<g/>
půjčit	půjčit	k5eAaPmF
si	se	k3xPyFc3
loď	loď	k1gFnSc4
a	a	k8xC
vyplout	vyplout	k5eAaPmF
na	na	k7c4
moře	moře	k1gNnSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Globální	globální	k2eAgFnSc1d1
síť	síť	k1gFnSc1
CRI	CRI	kA
provozuje	provozovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
58	#num#	k4
radiostanic	radiostanice	k1gFnPc2
ve	v	k7c6
35	#num#	k4
státech	stát	k1gInPc6
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
11	#num#	k4
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
se	se	k3xPyFc4
strategie	strategie	k1gFnSc1
z	z	k7c2
"	"	kIx"
<g/>
půjčit	půjčit	k5eAaPmF
si	se	k3xPyFc3
loď	loď	k1gFnSc4
<g/>
"	"	kIx"
změnila	změnit	k5eAaPmAgFnS
na	na	k7c4
přímý	přímý	k2eAgInSc4d1
nákup	nákup	k1gInSc4
médií	médium	k1gNnPc2
prostřednictvím	prostřednictvím	k7c2
podnikatelů	podnikatel	k1gMnPc2
jako	jako	k9
je	být	k5eAaImIp3nS
majitel	majitel	k1gMnSc1
Alibaba	Alibaba	k1gMnSc1
Group	Group	k1gMnSc1
Jack	Jack	k1gMnSc1
Ma	Ma	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
vlastníkem	vlastník	k1gMnSc7
nezávislých	závislý	k2eNgFnPc2d1
hongkongských	hongkongský	k2eAgFnPc2d1
novin	novina	k1gFnPc2
South	Southa	k1gFnPc2
China	China	k1gFnSc1
Morning	Morning	k1gInSc1
Post	post	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miliardář	miliardář	k1gMnSc1
Huang	Huang	k1gMnSc1
Xiangmo	Xiangma	k1gFnSc5
založil	založit	k5eAaPmAgMnS
na	na	k7c4
University	universita	k1gFnPc4
of	of	k?
Technology	technolog	k1gMnPc4
Sydney	Sydney	k1gNnSc2
think	think	k6eAd1
tank	tank	k1gInSc4
Australia	Australia	k1gFnSc1
China	China	k1gFnSc1
Relations	Relations	k1gInSc4
Institute	institut	k1gInSc5
(	(	kIx(
<g/>
ACRI	ACRI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zaplatil	zaplatit	k5eAaPmAgMnS
a	a	k8xC
zorganizoval	zorganizovat	k5eAaPmAgMnS
cesty	cesta	k1gFnPc4
do	do	k7c2
Číny	Čína	k1gFnSc2
pro	pro	k7c4
28	#num#	k4
prestižních	prestižní	k2eAgMnPc2d1
australských	australský	k2eAgMnPc2d1
novinářů	novinář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
byli	být	k5eAaImAgMnP
hosty	host	k1gMnPc7
Všečínské	všečínský	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
novinářů	novinář	k1gMnPc2
kontrolované	kontrolovaný	k2eAgInPc1d1
Komunistickou	komunistický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
ČLR	ČLR	kA
a	a	k8xC
podali	podat	k5eAaPmAgMnP
velmi	velmi	k6eAd1
jednostranná	jednostranný	k2eAgNnPc4d1
svědectví	svědectví	k1gNnPc4
o	o	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobná	podobný	k2eAgFnSc1d1
nadace	nadace	k1gFnSc1
China-United	China-United	k1gInSc4
States	States	k1gInSc1
Exchange	Exchange	k1gNnSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
Cusef	Cusef	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
milionářem	milionář	k1gMnSc7
Tung	Tung	k1gMnSc1
Chee-hwa	Chee-hwa	k1gMnSc1
<g/>
,	,	kIx,
zařídila	zařídit	k5eAaPmAgFnS
cestu	cesta	k1gFnSc4
127	#num#	k4
amerických	americký	k2eAgMnPc2d1
novinářů	novinář	k1gMnPc2
ze	z	k7c2
40	#num#	k4
médií	médium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
novináře	novinář	k1gMnPc4
z	z	k7c2
chudších	chudý	k2eAgFnPc2d2
zemí	zem	k1gFnPc2
jsou	být	k5eAaImIp3nP
připravené	připravený	k2eAgFnPc1d1
dlouhodobé	dlouhodobý	k2eAgFnPc1d1
stáže	stáž	k1gFnPc1
a	a	k8xC
Čína	Čína	k1gFnSc1
tak	tak	k6eAd1
plánuje	plánovat	k5eAaImIp3nS
během	během	k7c2
5	#num#	k4
let	léto	k1gNnPc2
vyškolit	vyškolit	k5eAaPmF
novou	nový	k2eAgFnSc4d1
generaci	generace	k1gFnSc4
500	#num#	k4
novinářů	novinář	k1gMnPc2
z	z	k7c2
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
a	a	k8xC
1000	#num#	k4
z	z	k7c2
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
propagandistických	propagandistický	k2eAgMnPc6d1
videích	videus	k1gMnPc6
je	být	k5eAaImIp3nS
kritická	kritický	k2eAgFnSc1d1
žurnalistika	žurnalistika	k1gFnSc1
označována	označovat	k5eAaImNgFnS
za	za	k7c4
nezodpovědnou	zodpovědný	k2eNgFnSc4d1
a	a	k8xC
škodlivou	škodlivý	k2eAgFnSc4d1
pro	pro	k7c4
společnost	společnost	k1gFnSc4
a	a	k8xC
žurnalisté	žurnalist	k1gMnPc1
vyznávající	vyznávající	k2eAgFnSc2d1
"	"	kIx"
<g/>
západní	západní	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
žurnalistiky	žurnalistika	k1gFnSc2
<g/>
"	"	kIx"
označováni	označovat	k5eAaImNgMnP
za	za	k7c4
"	"	kIx"
<g/>
vymyté	vymytý	k2eAgInPc4d1
mozky	mozek	k1gInPc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostřednictvím	prostřednictví	k1gNnSc7
společností	společnost	k1gFnPc2
jako	jako	k8xS,k8xC
StarTimes	StarTimes	k1gInSc4
se	se	k3xPyFc4
mediální	mediální	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
Číny	Čína	k1gFnSc2
přesunuje	přesunovat	k5eAaImIp3nS
od	od	k7c2
obsahu	obsah	k1gInSc2
zpráv	zpráva	k1gFnPc2
ke	k	k7c3
kritické	kritický	k2eAgFnSc3d1
infrastruktuře	infrastruktura	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
souhrnné	souhrnný	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
amerických	americký	k2eAgMnPc2d1
sinologů	sinolog	k1gMnPc2
o	o	k7c6
působení	působení	k1gNnSc6
Číny	Čína	k1gFnSc2
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
publikoval	publikovat	k5eAaBmAgMnS
Hoover	Hoover	k1gMnSc1
Institution	Institution	k1gInSc4
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
veškerá	veškerý	k3xTgNnPc4
čínská	čínský	k2eAgNnPc4d1
nezávislá	závislý	k2eNgNnPc4d1
media	medium	k1gNnPc4
v	v	k7c6
USA	USA	kA
byla	být	k5eAaImAgFnS
Čínou	Čína	k1gFnSc7
kooptována	kooptován	k2eAgFnSc1d1
nebo	nebo	k8xC
zlikvidována	zlikvidován	k2eAgFnSc1d1
její	její	k3xOp3gFnSc7
agresivní	agresivní	k2eAgFnSc7d1
expanzí	expanze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
Čína	Čína	k1gFnSc1
drasticky	drasticky	k6eAd1
omezila	omezit	k5eAaPmAgFnS
působení	působení	k1gNnSc4
amerických	americký	k2eAgNnPc2d1
médií	médium	k1gNnPc2
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
území	území	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
ukončila	ukončit	k5eAaPmAgFnS
akreditace	akreditace	k1gFnSc1
novinářů	novinář	k1gMnPc2
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Wall	Wall	k1gMnSc1
Street	Street	k1gMnSc1
Journal	Journal	k1gMnSc1
a	a	k8xC
The	The	k1gMnPc2
Washington	Washington	k1gInSc1
Post	posta	k1gFnPc2
a	a	k8xC
požádala	požádat	k5eAaPmAgFnS
časopis	časopis	k1gInSc4
TIME	TIME	kA
a	a	k8xC
stanici	stanice	k1gFnSc3
Voice	Voika	k1gFnSc3
of	of	k?
America	Americum	k1gNnSc2
o	o	k7c4
detailní	detailní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
o	o	k7c6
jejích	její	k3xOp3gMnPc6
novinářích	novinář	k1gMnPc6
<g/>
,	,	kIx,
majetku	majetek	k1gInSc2
i	i	k8xC
operacích	operace	k1gFnPc6
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ředitel	ředitel	k1gMnSc1
německého	německý	k2eAgInSc2d1
Spolkového	spolkový	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
ústavy	ústava	k1gFnSc2
Thomas	Thomas	k1gMnSc1
Haldenwang	Haldenwang	k1gMnSc1
varoval	varovat	k5eAaImAgMnS
zákazníky	zákazník	k1gMnPc4
před	před	k7c4
poskytováním	poskytování	k1gNnSc7
osobních	osobní	k2eAgNnPc2d1
dat	datum	k1gNnPc2
čínským	čínský	k2eAgFnPc3d1
platebním	platební	k2eAgFnPc3d1
společnostem	společnost	k1gFnPc3
nebo	nebo	k8xC
jiným	jiný	k2eAgFnPc3d1
technologickým	technologický	k2eAgFnPc3d1
a	a	k8xC
internetovým	internetový	k2eAgFnPc3d1
firmám	firma	k1gFnPc3
<g/>
,	,	kIx,
protože	protože	k8xS
čínské	čínský	k2eAgInPc1d1
zákony	zákon	k1gInPc1
po	po	k7c6
těchto	tento	k3xDgFnPc6
firmách	firma	k1gFnPc6
vyžadují	vyžadovat	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
je	on	k3xPp3gMnPc4
poskytovaly	poskytovat	k5eAaImAgFnP
čínské	čínský	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjádřil	vyjádřit	k5eAaPmAgMnS
rovněž	rovněž	k9
obavu	obava	k1gFnSc4
z	z	k7c2
"	"	kIx"
<g/>
hybridní	hybridní	k2eAgFnSc2d1
hrozby	hrozba	k1gFnSc2
<g/>
"	"	kIx"
ze	z	k7c2
strany	strana	k1gFnSc2
Číny	Čína	k1gFnSc2
spočívající	spočívající	k2eAgFnSc2d1
v	v	k7c6
akvizicích	akvizice	k1gFnPc6
významných	významný	k2eAgFnPc2d1
německých	německý	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ředitel	ředitel	k1gMnSc1
amerických	americký	k2eAgFnPc2d1
tajných	tajný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
2020	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
Ratcliffe	Ratcliff	k1gInSc5
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc7d3
světovou	světový	k2eAgFnSc7d1
hrozbou	hrozba	k1gFnSc7
demokracie	demokracie	k1gFnSc2
a	a	k8xC
svobody	svoboda	k1gFnSc2
od	od	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
svými	svůj	k3xOyFgFnPc7
špionážními	špionážní	k2eAgFnPc7d1
aktivitami	aktivita	k1gFnPc7
připraví	připravit	k5eAaPmIp3nS
USA	USA	kA
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
o	o	k7c4
duševní	duševní	k2eAgNnSc4d1
vlastnictví	vlastnictví	k1gNnSc4
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
500	#num#	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
a	a	k8xC
s	s	k7c7
pomocí	pomoc	k1gFnSc7
masivní	masivní	k2eAgFnSc2d1
vlivové	vlivový	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
se	s	k7c7
prostřednictvím	prostřednictví	k1gNnSc7
odborů	odbor	k1gInPc2
velkých	velký	k2eAgFnPc2d1
amerických	americký	k2eAgFnPc2d1
firem	firma	k1gFnPc2
snaží	snažit	k5eAaImIp3nP
ovlivnit	ovlivnit	k5eAaPmF
členy	člen	k1gMnPc4
Kongresu	kongres	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
k	k	k7c3
ní	on	k3xPp3gFnSc3
zaujali	zaujmout	k5eAaPmAgMnP
mírnější	mírný	k2eAgInPc4d2
postoje	postoj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
černý	černý	k2eAgInSc4d1
seznam	seznam	k1gInSc4
čínských	čínský	k2eAgFnPc2d1
firem	firma	k1gFnPc2
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
armádou	armáda	k1gFnSc7
byly	být	k5eAaImAgFnP
nově	nově	k6eAd1
zařazeny	zařadit	k5eAaPmNgFnP
<g/>
:	:	kIx,
výrobce	výrobce	k1gMnSc2
čipů	čip	k1gInPc2
SMIC	SMIC	kA
<g/>
,	,	kIx,
národní	národní	k2eAgFnSc1d1
naftařská	naftařský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
CNOOC	CNOOC	kA
<g/>
,	,	kIx,
China	China	k1gFnSc1
Construction	Construction	k1gInSc1
Technology	technolog	k1gMnPc4
Co	co	k9
a	a	k8xC
China	China	k1gFnSc1
International	International	k1gFnSc1
Engineering	Engineering	k1gInSc4
Consulting	Consulting	k1gInSc1
Corp	Corp	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vydírání	vydírání	k1gNnSc1
čínských	čínský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
</s>
<s>
Aby	aby	kYmCp3nS
umlčela	umlčet	k5eAaPmAgFnS
kritiku	kritika	k1gFnSc4
politiky	politika	k1gFnSc2
Pekingu	Peking	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc6
přístupu	přístup	k1gInSc6
k	k	k7c3
otázce	otázka	k1gFnSc3
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
čínská	čínský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
vydírat	vydírat	k5eAaImF
Číňany	Číňan	k1gMnPc4
žijící	žijící	k2eAgMnPc4d1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
a	a	k8xC
přinutit	přinutit	k5eAaPmF
je	být	k5eAaImIp3nS
k	k	k7c3
návratu	návrat	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
program	program	k1gInSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
"	"	kIx"
<g/>
hon	hon	k1gInSc1
na	na	k7c4
lišku	liška	k1gFnSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
prosadil	prosadit	k5eAaPmAgMnS
čínský	čínský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týká	týkat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
Číňanů	Číňan	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
občany	občan	k1gMnPc7
USA	USA	kA
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
držiteli	držitel	k1gMnPc7
zelené	zelený	k2eAgFnSc2d1
karty	karta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínský	čínský	k2eAgInSc4d1
režim	režim	k1gInSc4
za	za	k7c7
tím	ten	k3xDgInSc7
účelem	účel	k1gInSc7
zadržuje	zadržovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gMnPc4
příbuzné	příbuzný	k1gMnPc4
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
vysílá	vysílat	k5eAaImIp3nS
své	svůj	k3xOyFgMnPc4
agenty	agent	k1gMnPc4
do	do	k7c2
USA	USA	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
je	on	k3xPp3gInPc4
vydírali	vydírat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ředitel	ředitel	k1gMnSc1
amerického	americký	k2eAgInSc2d1
Federálního	federální	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
pro	pro	k7c4
vyšetřování	vyšetřování	k1gNnSc4
(	(	kIx(
<g/>
FBI	FBI	kA
<g/>
)	)	kIx)
Christopher	Christophra	k1gFnPc2
Wray	Wraa	k1gFnSc2
uvedl	uvést	k5eAaPmAgMnS
případ	případ	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
čínský	čínský	k2eAgMnSc1d1
agent	agent	k1gMnSc1
navštívil	navštívit	k5eAaPmAgMnS
cílovou	cílový	k2eAgFnSc4d1
rodinu	rodina	k1gFnSc4
v	v	k7c6
USA	USA	kA
a	a	k8xC
řekl	říct	k5eAaPmAgMnS
jim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
vybrat	vybrat	k5eAaPmF
mezi	mezi	k7c7
návratem	návrat	k1gInSc7
do	do	k7c2
vlasti	vlast	k1gFnSc2
či	či	k8xC
sebevraždou	sebevražda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
polovina	polovina	k1gFnSc1
aktivních	aktivní	k2eAgFnPc2d1
operací	operace	k1gFnPc2
americké	americký	k2eAgFnSc2d1
kontrarozvědky	kontrarozvědka	k1gFnSc2
se	se	k3xPyFc4
nějakým	nějaký	k3yIgInSc7
způsobem	způsob	k1gInSc7
týká	týkat	k5eAaImIp3nS
Číny	Čína	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Šíření	šíření	k1gNnSc1
dezinformací	dezinformace	k1gFnPc2
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
prostřednictvím	prostřednictvím	k7c2
svých	svůj	k3xOyFgFnPc2
vlivových	vlivový	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
šíří	šíř	k1gFnPc2
účelové	účelový	k2eAgFnSc2d1
dezinformace	dezinformace	k1gFnSc2
s	s	k7c7
cílem	cíl	k1gInSc7
diskreditovat	diskreditovat	k5eAaBmF
politiky	politik	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
už	už	k6eAd1
dříve	dříve	k6eAd2
stali	stát	k5eAaPmAgMnP
terčem	terč	k1gInSc7
přímých	přímý	k2eAgFnPc2d1
výhrůžek	výhrůžka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
obdržela	obdržet	k5eAaPmAgFnS
opakovaně	opakovaně	k6eAd1
e-maily	e-mail	k1gInPc4
od	od	k7c2
švýcarských	švýcarský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
Michaela	Michael	k1gMnSc2
Winklera	Winkler	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
asistenta	asistent	k1gMnSc2
Roberta	Robert	k1gMnSc2
J.	J.	kA
Mojzese	Mojzese	k1gFnSc2
(	(	kIx(
<g/>
RefinSol	RefinSol	k1gInSc1
Advisory	Advisor	k1gInPc4
Services	Services	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
následně	následně	k6eAd1
požadovali	požadovat	k5eAaImAgMnP
informaci	informace	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
redakce	redakce	k1gFnSc1
jejich	jejich	k3xOp3gFnSc4
zprávu	zpráva	k1gFnSc4
obdržela	obdržet	k5eAaPmAgFnS
i	i	k9
jak	jak	k6eAd1
na	na	k7c4
ni	on	k3xPp3gFnSc4
reagovala	reagovat	k5eAaBmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
e-mailové	e-mailové	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
odvolávala	odvolávat	k5eAaImAgFnS
pouze	pouze	k6eAd1
na	na	k7c4
mezitím	mezitím	k6eAd1
smazané	smazaný	k2eAgInPc4d1
lživé	lživý	k2eAgInPc4d1
příspěvky	příspěvek	k1gInPc4
na	na	k7c6
Twitteru	Twitter	k1gInSc6
a	a	k8xC
Redditu	Reddita	k1gFnSc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
tvrzení	tvrzení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
Miloš	Miloš	k1gMnSc1
Vystrčil	Vystrčil	k1gMnSc1
obdržel	obdržet	k5eAaPmAgMnS
během	během	k7c2
návštěvy	návštěva	k1gFnSc2
Tchaj-wanu	Tchaj-wan	k1gInSc2
příslib	příslib	k1gInSc1
příspěvku	příspěvek	k1gInSc2
4	#num#	k4
miliony	milion	k4xCgInPc1
dolarů	dolar	k1gInPc2
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
budoucí	budoucí	k2eAgFnSc4d1
prezidentskou	prezidentský	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Tuto	tento	k3xDgFnSc4
dezinformaci	dezinformace	k1gFnSc4
vzápětí	vzápětí	k6eAd1
vyvrátilo	vyvrátit	k5eAaPmAgNnS
i	i	k9
Ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
Tchaj-wanu	Tchaj-wan	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Michael	Michael	k1gMnSc1
Winkler	Winkler	k1gMnSc1
není	být	k5eNaImIp3nS
jen	jen	k9
obchodník	obchodník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
čínskou	čínský	k2eAgFnSc7d1
manželkou	manželka	k1gFnSc7
je	být	k5eAaImIp3nS
vlastníkem	vlastník	k1gMnSc7
zpravodajského	zpravodajský	k2eAgInSc2d1
portálu	portál	k1gInSc2
„	„	k?
<g/>
Eurasia-Info	Eurasia-Info	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
spoluzakladatelem	spoluzakladatel	k1gMnSc7
sdružení	sdružení	k1gNnSc2
Eurasia-Forum	Eurasia-Forum	k1gNnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
sloužícího	sloužící	k1gMnSc4
mj.	mj.	kA
čínskému	čínský	k2eAgMnSc3d1
velvyslanectví	velvyslanectví	k1gNnSc6
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Číně	Čína	k1gFnSc6
cenzura	cenzura	k1gFnSc1
zablokovala	zablokovat	k5eAaPmAgFnS
zjištění	zjištění	k1gNnSc4
o	o	k7c6
dezinformační	dezinformační	k2eAgFnSc6d1
kampani	kampaň	k1gFnSc6
i	i	k9
přístup	přístup	k1gInSc4
k	k	k7c3
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
dezinformaci	dezinformace	k1gFnSc4
vyvrátilo	vyvrátit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
webu	web	k1gInSc6
iFeng	iFeng	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
provozuje	provozovat	k5eAaImIp3nS
televize	televize	k1gFnSc1
založená	založený	k2eAgFnSc1d1
bývalým	bývalý	k2eAgMnSc7d1
důstojníkem	důstojník	k1gMnSc7
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
osvobozenecké	osvobozenecký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
Liou	Lious	k1gInSc2
Čchang-lem	Čchang-lo	k1gNnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
šířena	šířen	k2eAgFnSc1d1
verze	verze	k1gFnSc1
obou	dva	k4xCgInPc2
švýcarských	švýcarský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dezinformace	dezinformace	k1gFnPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
agresivní	agresivní	k2eAgFnSc2d1
čínské	čínský	k2eAgFnSc2d1
zahraniční	zahraniční	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
uplatňované	uplatňovaný	k2eAgInPc1d1
zejména	zejména	k6eAd1
mluvčím	mluvčí	k1gMnSc7
Ministerstva	ministerstvo	k1gNnSc2
zahraničí	zahraničí	k1gNnSc2
ČLR	ČLR	kA
Zhao	Zhao	k6eAd1
Lijianem	Lijian	k1gInSc7
(	(	kIx(
<g/>
dezinformace	dezinformace	k1gFnSc1
na	na	k7c6
Twitteru	Twitter	k1gInSc6
o	o	k7c6
původu	původ	k1gInSc6
Covid-	Covid-	k1gFnSc2
<g/>
19	#num#	k4
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
označované	označovaný	k2eAgFnPc4d1
v	v	k7c6
diplomacii	diplomacie	k1gFnSc6
jako	jako	k9
"	"	kIx"
<g/>
Wolf	Wolf	k1gMnSc1
warrior	warrior	k1gMnSc1
diplomacy	diplomaca	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hackerské	Hackerský	k2eAgInPc1d1
útoky	útok	k1gInPc1
</s>
<s>
Státem	stát	k1gInSc7
podporovaná	podporovaný	k2eAgFnSc1d1
hackerská	hackerský	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
sídlící	sídlící	k2eAgFnSc1d1
v	v	k7c6
Číně	Čína	k1gFnSc6
a	a	k8xC
označovaná	označovaný	k2eAgNnPc4d1
Hafnium	hafnium	k1gNnSc4
provádí	provádět	k5eAaImIp3nP
útoky	útok	k1gInPc4
prostřednictvím	prostřednictvím	k7c2
pronajatých	pronajatý	k2eAgInPc2d1
virtuálních	virtuální	k2eAgInPc2d1
serverů	server	k1gInPc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
zcizení	zcizení	k1gNnSc4
informací	informace	k1gFnPc2
z	z	k7c2
různých	různý	k2eAgInPc2d1
sektorů	sektor	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
výzkumu	výzkum	k1gInSc2
infekčních	infekční	k2eAgFnPc2d1
nemocí	nemoc	k1gFnPc2
<g/>
,	,	kIx,
právních	právní	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
,	,	kIx,
vzdělávacích	vzdělávací	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
obranného	obranný	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
politických	politický	k2eAgInPc2d1
think	think	k6eAd1
tanků	tank	k1gInPc2
a	a	k8xC
neziskových	ziskový	k2eNgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
2021	#num#	k4
využila	využít	k5eAaPmAgFnS
neopravené	opravený	k2eNgFnPc4d1
chyby	chyba	k1gFnPc4
v	v	k7c6
softwaru	software	k1gInSc6
Microsoft	Microsoft	kA
Exchange	Exchange	k1gInSc1
Server	server	k1gInSc1
a	a	k8xC
napadla	napadnout	k5eAaPmAgFnS
desetitisíce	desetitisíce	k1gInPc4
organizací	organizace	k1gFnPc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Terčem	terč	k1gInSc7
útoku	útok	k1gInSc2
byly	být	k5eAaImAgFnP
rovněž	rovněž	k9
některé	některý	k3yIgInPc1
české	český	k2eAgInPc1d1
úřady	úřad	k1gInPc1
-	-	kIx~
Ministerstvo	ministerstvo	k1gNnSc1
práce	práce	k1gFnSc2
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
servery	server	k1gInPc1
České	český	k2eAgFnSc2d1
správy	správa	k1gFnSc2
sociálního	sociální	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
nebo	nebo	k8xC
pražský	pražský	k2eAgInSc1d1
Magistrát	magistrát	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
</s>
<s>
Letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Liao-ning	Liao-ning	k1gInSc4
třídy	třída	k1gFnSc2
Type	typ	k1gInSc5
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
ve	v	k7c6
službě	služba	k1gFnSc6
u	u	k7c2
hladinových	hladinový	k2eAgFnPc2d1
námořních	námořní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
</s>
<s>
S	s	k7c7
2,3	2,3	k4
milióny	milión	k4xCgInPc7
aktivními	aktivní	k2eAgInPc7d1
vojáky	voják	k1gMnPc4
je	být	k5eAaImIp3nS
čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
osvobozenecká	osvobozenecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
PLA	PLA	kA
<g/>
)	)	kIx)
největší	veliký	k2eAgFnSc7d3
stálou	stálý	k2eAgFnSc7d1
vojenskou	vojenský	k2eAgFnSc7d1
silou	síla	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řízena	řízen	k2eAgFnSc1d1
je	on	k3xPp3gMnPc4
ústřední	ústřední	k2eAgFnSc7d1
vojenskou	vojenský	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
(	(	kIx(
<g/>
CMC	CMC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
KLDR	KLDR	kA
má	mít	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
druhý	druhý	k4xOgInSc1
největší	veliký	k2eAgInSc1d3
počet	počet	k1gInSc1
rezervistů	rezervista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
pozemních	pozemní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
(	(	kIx(
<g/>
PLAGF	PLAGF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
námořnictva	námořnictvo	k1gNnSc2
(	(	kIx(
<g/>
PLAN	plan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
letectva	letectvo	k1gNnSc2
(	(	kIx(
<g/>
PLAAF	PLAAF	kA
<g/>
)	)	kIx)
a	a	k8xC
raketového	raketový	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
(	(	kIx(
<g/>
PLARF	PLARF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
čínské	čínský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
činil	činit	k5eAaImAgInS
čínský	čínský	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
rozpočet	rozpočet	k1gInSc1
na	na	k7c4
rok	rok	k1gInSc4
2017	#num#	k4
151,5	151,5	k4
miliardy	miliarda	k4xCgFnSc2
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
představuje	představovat	k5eAaImIp3nS
druhý	druhý	k4xOgMnSc1
největší	veliký	k2eAgInSc1d3
vojenský	vojenský	k2eAgInSc1d1
rozpočet	rozpočet	k1gInSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
poměr	poměr	k1gInSc1
vojenských	vojenský	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
–	–	k?
HDP	HDP	kA
s	s	k7c7
1,3	1,3	k4
<g/>
%	%	kIx~
HDP	HDP	kA
je	být	k5eAaImIp3nS
pod	pod	k7c7
světovým	světový	k2eAgInSc7d1
průměrem	průměr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohý	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
–	–	k?
včetně	včetně	k7c2
SIPRI	SIPRI	kA
a	a	k8xC
amerického	americký	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
ministra	ministr	k1gMnSc2
obrany	obrana	k1gFnSc2
však	však	k9
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Čína	Čína	k1gFnSc1
svou	svůj	k3xOyFgFnSc4
skutečnou	skutečný	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
vojenských	vojenský	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
nehlásí	hlásit	k5eNaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
mnohem	mnohem	k6eAd1
vyšší	vysoký	k2eAgInSc4d2
než	než	k8xS
oficiální	oficiální	k2eAgInSc4d1
rozpočet	rozpočet	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čína	Čína	k1gFnSc1
také	také	k9
disponuje	disponovat	k5eAaBmIp3nS
jadernými	jaderný	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
považovaná	považovaný	k2eAgFnSc1d1
za	za	k7c4
hlavní	hlavní	k2eAgFnSc4d1
oblastní	oblastní	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
a	a	k8xC
potenciální	potenciální	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
supervelmoc	supervelmoc	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
zprávy	zpráva	k1gFnSc2
amerického	americký	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
obrany	obrana	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
má	mít	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
ve	v	k7c6
výzbroji	výzbroj	k1gInSc6
mezi	mezi	k7c7
50	#num#	k4
až	až	k9
75	#num#	k4
kusy	kus	k1gInPc4
jaderných	jaderný	k2eAgFnPc2d1
mezikontinentálních	mezikontinentální	k2eAgFnPc2d1
balistických	balistický	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
řadou	řada	k1gFnSc7
balistických	balistický	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
krátkého	krátký	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
čtyřmi	čtyři	k4xCgMnPc7
stálými	stálý	k2eAgMnPc7d1
členy	člen	k1gMnPc7
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
má	mít	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
relativně	relativně	k6eAd1
omezené	omezený	k2eAgFnSc2d1
možnosti	možnost	k1gFnSc2
projekce	projekce	k1gFnSc2
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
špionáž	špionáž	k1gFnSc1
</s>
<s>
Čínský	čínský	k2eAgInSc1d1
zájem	zájem	k1gInSc1
se	se	k3xPyFc4
soustřeďuje	soustřeďovat	k5eAaImIp3nS
zejména	zejména	k9
na	na	k7c4
strategické	strategický	k2eAgInPc4d1
sektory	sektor	k1gInPc4
hospodářství	hospodářství	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
energetika	energetika	k1gFnSc1
<g/>
,	,	kIx,
telekomunikace	telekomunikace	k1gFnPc1
<g/>
,	,	kIx,
finance	finance	k1gFnPc1
<g/>
,	,	kIx,
logistika	logistika	k1gFnSc1
<g/>
,	,	kIx,
zdravotnictví	zdravotnictví	k1gNnSc1
a	a	k8xC
špičkové	špičkový	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1
špionáž	špionáž	k1gFnSc1
</s>
<s>
Čínský	čínský	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c6
celonárodní	celonárodní	k2eAgFnSc6d1
špionáži	špionáž	k1gFnSc6
<g/>
,	,	kIx,
přijatý	přijatý	k2eAgInSc1d1
Všelidovým	všelidový	k2eAgInSc7d1
kongresem	kongres	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
,	,	kIx,
nařizuje	nařizovat	k5eAaImIp3nS
všem	všecek	k3xTgMnPc3
čínským	čínský	k2eAgMnPc3d1
jednotlivcům	jednotlivec	k1gMnPc3
i	i	k8xC
firmám	firma	k1gFnPc3
v	v	k7c6
případě	případ	k1gInSc6
státního	státní	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
provádět	provádět	k5eAaImF
špionáž	špionáž	k1gFnSc4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Např.	např.	kA
Huawei	Huawe	k1gFnSc2
China	China	k1gFnSc1
zavedla	zavést	k5eAaPmAgFnS
formální	formální	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
bonusového	bonusový	k2eAgInSc2d1
programu	program	k1gInSc2
odměňujícího	odměňující	k2eAgMnSc4d1
zaměstnance	zaměstnanec	k1gMnSc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
ukradli	ukradnout	k5eAaPmAgMnP
konkurenci	konkurence	k1gFnSc3
tajné	tajný	k2eAgFnSc2d1
informace	informace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměstnancům	zaměstnanec	k1gMnPc3
bylo	být	k5eAaImAgNnS
nařízeno	nařízen	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
tajné	tajný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
získali	získat	k5eAaPmAgMnP
od	od	k7c2
cizích	cizí	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
ukládali	ukládat	k5eAaImAgMnP
na	na	k7c4
interní	interní	k2eAgFnSc4d1
firemní	firemní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
zvláště	zvláště	k6eAd1
citlivých	citlivý	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
poslali	poslat	k5eAaPmAgMnP
zašifrovaný	zašifrovaný	k2eAgInSc4d1
email	email	k1gInSc4
do	do	k7c2
speciální	speciální	k2eAgFnSc2d1
emailové	emailový	k2eAgFnSc2d1
schránky	schránka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrnice	směrnice	k1gFnSc1
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
žádný	žádný	k3yNgMnSc1
ze	z	k7c2
zaměstnanců	zaměstnanec	k1gMnPc2
nebude	být	k5eNaImBp3nS
potrestán	potrestat	k5eAaPmNgMnS
za	za	k7c4
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
bude	být	k5eAaImBp3nS
v	v	k7c6
jejich	jejich	k3xOp3gNnSc6
v	v	k7c6
rámci	rámec	k1gInSc6
provedena	provést	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skandálu	skandál	k1gInSc6
v	v	k7c6
USA	USA	kA
upozornilo	upozornit	k5eAaPmAgNnS
vedení	vedení	k1gNnSc1
pracovníky	pracovník	k1gMnPc7
Huawei	Huawe	k1gFnSc2
USA	USA	kA
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
podobné	podobný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
nejsou	být	k5eNaImIp3nP
podporovány	podporovat	k5eAaImNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
emailové	emailový	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
nicméně	nicméně	k8xC
podle	podle	k7c2
amerických	americký	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
regionech	region	k1gInPc6
světa	svět	k1gInSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
naprosto	naprosto	k6eAd1
běžná	běžný	k2eAgFnSc1d1
součást	součást	k1gFnSc1
pracovní	pracovní	k2eAgFnSc2d1
náplně	náplň	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
má	mít	k5eAaImIp3nS
93	#num#	k4
milionů	milion	k4xCgInPc2
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
řada	řada	k1gFnSc1
pracuje	pracovat	k5eAaImIp3nS
skrytě	skrytě	k6eAd1
v	v	k7c6
zahraničních	zahraniční	k2eAgFnPc6d1
firmách	firma	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
větší	veliký	k2eAgFnSc1d2
čínská	čínský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
kdekoli	kdekoli	k6eAd1
na	na	k7c6
světě	svět	k1gInSc6
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
stranickou	stranický	k2eAgFnSc4d1
buňku	buňka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
o	o	k7c4
politickou	politický	k2eAgFnSc4d1
agendu	agenda	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
zárukou	záruka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
společnost	společnost	k1gFnSc1
bude	být	k5eAaImBp3nS
plnit	plnit	k5eAaImF
stranické	stranický	k2eAgFnPc4d1
direktivy	direktiva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Čínu	Čína	k1gFnSc4
je	být	k5eAaImIp3nS
byznys	byznys	k1gInSc1
neoddělitelný	oddělitelný	k2eNgInSc1d1
od	od	k7c2
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
pozitivní	pozitivní	k2eAgInPc4d1
<g/>
"	"	kIx"
prostředky	prostředek	k1gInPc1
užívá	užívat	k5eAaImIp3nS
hrazení	hrazení	k1gNnSc4
účasti	účast	k1gFnSc2
na	na	k7c6
konferencích	konference	k1gFnPc6
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
finanční	finanční	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
firmám	firma	k1gFnPc3
v	v	k7c6
problémech	problém	k1gInPc6
<g/>
,	,	kIx,
nabídku	nabídka	k1gFnSc4
exekutivních	exekutivní	k2eAgFnPc2d1
pracovních	pracovní	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
firmách	firma	k1gFnPc6
nebo	nebo	k8xC
horentní	horentní	k2eAgFnPc4d1
peněžní	peněžní	k2eAgFnPc4d1
sumy	suma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinou	jiný	k2eAgFnSc7d1
formou	forma	k1gFnSc7
získávání	získávání	k1gNnSc2
spolupracovníků	spolupracovník	k1gMnPc2
pro	pro	k7c4
tajné	tajný	k2eAgFnPc4d1
služby	služba	k1gFnPc4
doma	doma	k6eAd1
i	i	k8xC
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
je	být	k5eAaImIp3nS
vysílání	vysílání	k1gNnSc1
"	"	kIx"
<g/>
volavek	volavka	k1gFnPc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
honeytraps	honeytraps	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
následné	následný	k2eAgNnSc1d1
vydírání	vydírání	k1gNnSc1
kompromitujícími	kompromitující	k2eAgInPc7d1
materiály	materiál	k1gInPc7
nebo	nebo	k8xC
"	"	kIx"
<g/>
kooptování	kooptování	k1gNnSc1
<g/>
"	"	kIx"
zahraničních	zahraniční	k2eAgMnPc2d1
průmyslových	průmyslový	k2eAgMnPc2d1
expertů	expert	k1gMnPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
bez	bez	k7c2
jejich	jejich	k3xOp3gNnSc2
vědomí	vědomí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Deník	deník	k1gInSc1
Frankfurter	Frankfurtra	k1gFnPc2
Allgemeine	Allgemein	k1gInSc5
Zeitung	Zeitung	k1gMnSc1
popsal	popsat	k5eAaPmAgMnS
konkrétní	konkrétní	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
německých	německý	k2eAgMnPc2d1
manažerů	manažer	k1gMnPc2
<g/>
,	,	kIx,
bankéřů	bankéř	k1gMnPc2
a	a	k8xC
právníků	právník	k1gMnPc2
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číňané	Číňan	k1gMnPc1
je	on	k3xPp3gMnPc4
odposlouchávají	odposlouchávat	k5eAaImIp3nP
v	v	k7c6
restauracích	restaurace	k1gFnPc6
<g/>
,	,	kIx,
přes	přes	k7c4
telefony	telefon	k1gInPc4
nebo	nebo	k8xC
stahují	stahovat	k5eAaImIp3nP
data	datum	k1gNnPc4
rovnou	rovnou	k6eAd1
z	z	k7c2
jejich	jejich	k3xOp3gNnPc2
zařízení	zařízení	k1gNnPc2
odcizených	odcizený	k2eAgNnPc2d1
z	z	k7c2
hotelového	hotelový	k2eAgInSc2d1
trezoru	trezor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přísná	přísný	k2eAgNnPc4d1
bezpečnostní	bezpečnostní	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
platí	platit	k5eAaImIp3nS
i	i	k9
pro	pro	k7c4
průmyslníky	průmyslník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
ve	v	k7c6
vládním	vládní	k2eAgInSc6d1
speciálu	speciál	k1gInSc6
doprovázeli	doprovázet	k5eAaImAgMnP
na	na	k7c6
cestách	cesta	k1gFnPc6
do	do	k7c2
Pekingu	Peking	k1gInSc2
kancléřku	kancléřka	k1gFnSc4
Merkelovou	Merkelová	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chytré	chytrý	k2eAgInPc1d1
telefony	telefon	k1gInPc1
nechávají	nechávat	k5eAaImIp3nP
doma	doma	k6eAd1
a	a	k8xC
i	i	k9
řadoví	řadový	k2eAgMnPc1d1
úředníci	úředník	k1gMnPc1
z	z	k7c2
ministerstev	ministerstvo	k1gNnPc2
hospodářství	hospodářství	k1gNnSc2
a	a	k8xC
zahraničí	zahraničí	k1gNnSc2
pro	pro	k7c4
tento	tento	k3xDgInSc4
účel	účel	k1gInSc4
používají	používat	k5eAaImIp3nP
ty	ten	k3xDgInPc1
nejlacinější	laciný	k2eAgInPc1d3
tlačítkové	tlačítkový	k2eAgInPc1d1
přístroje	přístroj	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
po	po	k7c6
návratu	návrat	k1gInSc6
vyhazují	vyhazovat	k5eAaImIp3nP
do	do	k7c2
elektrošrotu	elektrošrot	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
ČLR	ČLR	kA
je	být	k5eAaImIp3nS
praxe	praxe	k1gFnSc1
"	"	kIx"
<g/>
domácí	domácí	k2eAgInSc1d1
med	med	k1gInSc1
z	z	k7c2
cizích	cizí	k2eAgFnPc2d1
kytek	kytka	k1gFnPc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Picking	Picking	k1gInSc1
flowers	flowers	k1gInSc1
in	in	k?
foreign	foreigna	k1gFnPc2
lands	lands	k6eAd1
to	ten	k3xDgNnSc4
make	make	k1gNnSc4
honey	honea	k1gFnSc2
in	in	k?
China	China	k1gFnSc1
<g/>
)	)	kIx)
realizována	realizovat	k5eAaBmNgFnS
poskytováním	poskytování	k1gNnSc7
státních	státní	k2eAgNnPc2d1
stipendií	stipendium	k1gNnPc2
pro	pro	k7c4
dlouhodobé	dlouhodobý	k2eAgInPc4d1
pobyty	pobyt	k1gInPc4
studentů	student	k1gMnPc2
a	a	k8xC
vysokoškolských	vysokoškolský	k2eAgMnPc2d1
pedagogů	pedagog	k1gMnPc2
na	na	k7c6
prestižních	prestižní	k2eAgFnPc6d1
západních	západní	k2eAgFnPc6d1
univerzitách	univerzita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
často	často	k6eAd1
přímo	přímo	k6eAd1
čínská	čínský	k2eAgFnSc1d1
National	National	k1gFnSc1
University	universita	k1gFnSc2
of	of	k?
Defense	defense	k1gFnSc2
Technology	technolog	k1gMnPc4
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
jsou	být	k5eAaImIp3nP
dokonce	dokonce	k9
vysocí	vysoký	k2eAgMnPc1d1
důstojníci	důstojník	k1gMnPc1
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
pod	pod	k7c7
skrytou	skrytý	k2eAgFnSc7d1
identitou	identita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
životopise	životopis	k1gInSc6
uvádějí	uvádět	k5eAaImIp3nP
čínské	čínský	k2eAgFnPc1d1
výzkumné	výzkumný	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
neexistují	existovat	k5eNaImIp3nP
nebo	nebo	k8xC
instituce	instituce	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
zastírá	zastírat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
vojenské	vojenský	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
(	(	kIx(
<g/>
např.	např.	kA
Changsha	Changsha	k1gMnSc1
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc4
je	on	k3xPp3gInPc4
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
Vědecko-technologická	vědecko-technologický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
národní	národní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
Lidově	lidově	k6eAd1
osvobozenecké	osvobozenecký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
nebo	nebo	k8xC
Xi	Xi	k1gFnSc2
<g/>
’	’	k?
<g/>
an	an	k?
Institute	institut	k1gInSc5
of	of	k?
High	High	k1gInSc1
Technology	technolog	k1gMnPc4
je	on	k3xPp3gInPc4
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
Univerzita	univerzita	k1gFnSc1
vojenského	vojenský	k2eAgNnSc2d1
raketového	raketový	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgFnPc4d1
univerzity	univerzita	k1gFnPc4
si	se	k3xPyFc3
často	často	k6eAd1
ani	ani	k8xC
nejsou	být	k5eNaImIp3nP
vědomy	vědom	k2eAgFnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
čínští	čínský	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
armádní	armádní	k2eAgMnSc1d1
experti	expert	k1gMnPc1
a	a	k8xC
nechávají	nechávat	k5eAaImIp3nP
celou	celý	k2eAgFnSc4d1
zodpovědnost	zodpovědnost	k1gFnSc4
za	za	k7c4
bezpečnostní	bezpečnostní	k2eAgFnPc4d1
prověrky	prověrka	k1gFnPc4
na	na	k7c6
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Harvardově	Harvardův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Bostonu	Boston	k1gInSc6
<g/>
,	,	kIx,
během	během	k7c2
vyšetřování	vyšetřování	k1gNnSc2
vedoucího	vedoucí	k1gMnSc2
katedry	katedra	k1gFnSc2
chemie	chemie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
přijal	přijmout	k5eAaPmAgInS
nelegální	legální	k2eNgFnPc4d1
finance	finance	k1gFnPc4
z	z	k7c2
čínské	čínský	k2eAgFnPc1d1
Wuhan	Wuhan	k1gInSc4
University	universita	k1gFnSc2
of	of	k?
Technology	technolog	k1gMnPc7
<g/>
,	,	kIx,
vyslýchala	vyslýchat	k5eAaImAgFnS
FBI	FBI	kA
i	i	k8xC
jeho	jeho	k3xOp3gFnSc4
studentku	studentka	k1gFnSc4
Ye	Ye	k1gFnSc2
Yanqing	Yanqing	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
uvedla	uvést	k5eAaPmAgFnS
v	v	k7c6
žádosti	žádost	k1gFnSc6
o	o	k7c4
víza	vízo	k1gNnPc4
nepravdivé	pravdivý	k2eNgInPc4d1
údaje	údaj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ye	Ye	k1gFnSc1
Yanqing	Yanqing	k1gInSc1
byla	být	k5eAaImAgFnS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
důstojnicí	důstojnice	k1gFnSc7
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
dvěma	dva	k4xCgMnPc7
důstojníky	důstojník	k1gMnPc7
ČLA	ČLA	kA
byla	být	k5eAaImAgFnS
zapojena	zapojit	k5eAaPmNgFnS
do	do	k7c2
programu	program	k1gInSc2
rekrutování	rekrutování	k1gNnSc2
zahraničních	zahraniční	k2eAgMnPc2d1
odborníků	odborník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předávala	předávat	k5eAaImAgFnS
informace	informace	k1gFnPc4
o	o	k7c6
americké	americký	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
uprchnout	uprchnout	k5eAaPmF
zpět	zpět	k6eAd1
do	do	k7c2
Číny	Čína	k1gFnSc2
ještě	ještě	k6eAd1
před	před	k7c7
zatčením	zatčení	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
čínský	čínský	k2eAgInSc4d1
konzulát	konzulát	k1gInSc4
v	v	k7c6
San	San	k1gFnSc6
Franciscu	Franciscus	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
červnu	červen	k1gInSc6
2020	#num#	k4
uchýlila	uchýlit	k5eAaPmAgFnS
bioložka	bioložka	k1gFnSc1
Juan	Juan	k1gMnSc1
Tang	tango	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
pracovala	pracovat	k5eAaImAgFnS
na	na	k7c4
University	universita	k1gFnPc4
of	of	k?
California	Californium	k1gNnPc1
v	v	k7c6
Davisu	Davis	k1gInSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
byla	být	k5eAaImAgFnS
vyšetřována	vyšetřovat	k5eAaImNgFnS
FBI	FBI	kA
pro	pro	k7c4
falšování	falšování	k1gNnSc4
víza	vízo	k1gNnSc2
a	a	k8xC
zatajení	zatajení	k1gNnSc2
vazeb	vazba	k1gFnPc2
na	na	k7c4
čínskou	čínský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
veřejně	veřejně	k6eAd1
dostupných	dostupný	k2eAgInPc6d1
zdrojích	zdroj	k1gInPc6
přitom	přitom	k6eAd1
byly	být	k5eAaImAgFnP
nalezeny	naleznout	k5eAaPmNgFnP,k5eAaBmNgFnP
její	její	k3xOp3gFnPc1
fotografie	fotografia	k1gFnPc1
ve	v	k7c6
vojenské	vojenský	k2eAgFnSc6d1
uniformě	uniforma	k1gFnSc6
a	a	k8xC
pátrání	pátrání	k1gNnSc1
v	v	k7c6
jejím	její	k3xOp3gNnSc6
bydlišti	bydliště	k1gNnSc6
potvrdilo	potvrdit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
zaměstnankyní	zaměstnankyně	k1gFnSc7
čínské	čínský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2020	#num#	k4
byla	být	k5eAaImAgFnS
zatčena	zatknout	k5eAaPmNgFnS
a	a	k8xC
převezena	převézt	k5eAaPmNgFnS
do	do	k7c2
věznice	věznice	k1gFnSc2
v	v	k7c6
Sacramentu	Sacrament	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Závažnou	závažný	k2eAgFnSc7d1
událostí	událost	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2020	#num#	k4
daly	dát	k5eAaPmAgInP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
Číně	Čína	k1gFnSc3
72	#num#	k4
<g/>
hodinovou	hodinový	k2eAgFnSc4d1
lhůtu	lhůta	k1gFnSc4
na	na	k7c4
uzavření	uzavření	k1gNnSc4
jejího	její	k3xOp3gInSc2
konzulátu	konzulát	k1gInSc2
v	v	k7c6
texaském	texaský	k2eAgInSc6d1
Houstonu	Houston	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americké	americký	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
zahraničí	zahraničí	k1gNnSc2
vzneslo	vznést	k5eAaPmAgNnS
obvinění	obvinění	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
konzulát	konzulát	k1gInSc1
byl	být	k5eAaImAgInS
zapojen	zapojit	k5eAaPmNgInS
do	do	k7c2
krádeže	krádež	k1gFnSc2
duševního	duševní	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměstnanci	zaměstnanec	k1gMnPc1
konzulátu	konzulát	k1gInSc2
poté	poté	k6eAd1
údajně	údajně	k6eAd1
začali	začít	k5eAaPmAgMnP
s	s	k7c7
hromadným	hromadný	k2eAgNnSc7d1
pálením	pálení	k1gNnSc7
dokumentů	dokument	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
popředí	popředí	k1gNnSc2
zájmu	zájem	k1gInSc2
čínských	čínský	k2eAgMnPc2d1
špionů	špion	k1gMnPc2
patří	patřit	k5eAaImIp3nS
účast	účast	k1gFnSc4
na	na	k7c6
výzkumu	výzkum	k1gInSc6
hypersonických	hypersonický	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
<g/>
,	,	kIx,
navigační	navigační	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
<g/>
,	,	kIx,
kvantová	kvantový	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
<g/>
,	,	kIx,
kryptografie	kryptografie	k1gFnSc1
<g/>
,	,	kIx,
autonomní	autonomní	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
atd.	atd.	kA
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Špionáž	špionáž	k1gFnSc1
ČLR	ČLR	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Podle	podle	k7c2
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1
informační	informační	k2eAgFnSc2d1
služby	služba	k1gFnSc2
vzrostla	vzrůst	k5eAaPmAgFnS
intenzita	intenzita	k1gFnSc1
zpravodajské	zpravodajský	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
čínských	čínský	k2eAgMnPc2d1
zpravodajců	zpravodajce	k1gMnPc2
působících	působící	k2eAgMnPc2d1
v	v	k7c6
Česku	Česko	k1gNnSc6
pod	pod	k7c7
diplomatickým	diplomatický	k2eAgNnSc7d1
krytím	krytí	k1gNnSc7
a	a	k8xC
zpravodajská	zpravodajský	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
proti	proti	k7c3
českým	český	k2eAgInPc3d1
cílům	cíl	k1gInPc3
vedená	vedený	k2eAgFnSc1d1
z	z	k7c2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Riziko	riziko	k1gNnSc4
vystavení	vystavení	k1gNnSc2
českých	český	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
čínskému	čínský	k2eAgInSc3d1
špionážnímu	špionážní	k2eAgInSc3d1
zájmu	zájem	k1gInSc3
v	v	k7c6
Číně	Čína	k1gFnSc6
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
extrémně	extrémně	k6eAd1
vysoké	vysoký	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špionážní	špionážní	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
vedené	vedený	k2eAgFnSc2d1
proti	proti	k7c3
českým	český	k2eAgInPc3d1
cílům	cíl	k1gInPc3
a	a	k8xC
zájmům	zájem	k1gInPc3
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
tří	tři	k4xCgInPc2
segmentů	segment	k1gInPc2
<g/>
:	:	kIx,
Narušení	narušení	k1gNnSc1
jednotné	jednotný	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
EU	EU	kA
prostřednictvím	prostřednictvím	k7c2
českých	český	k2eAgFnPc2d1
entit	entita	k1gFnPc2
<g/>
;	;	kIx,
zpravodajská	zpravodajský	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
zacílená	zacílený	k2eAgFnSc1d1
na	na	k7c4
české	český	k2eAgInPc4d1
silové	silový	k2eAgInPc4d1
resorty	resort	k1gInPc4
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
a	a	k8xC
vědecko-technická	vědecko-technický	k2eAgFnSc1d1
špionáž	špionáž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínská	čínský	k2eAgFnSc1d1
diplomatická	diplomatický	k2eAgFnSc1d1
mise	mise	k1gFnSc1
také	také	k9
zvýšila	zvýšit	k5eAaPmAgFnS
schopnost	schopnost	k1gFnSc4
čínské	čínský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
kontrolovat	kontrolovat	k5eAaImF
a	a	k8xC
ovládat	ovládat	k5eAaImF
čínskou	čínský	k2eAgFnSc4d1
krajanskou	krajanský	k2eAgFnSc4d1
komunitu	komunita	k1gFnSc4
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
BIS	BIS	kA
zaznamenala	zaznamenat	k5eAaPmAgFnS
znepokojující	znepokojující	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
na	na	k7c6
poli	pole	k1gNnSc6
čínských	čínský	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
(	(	kIx(
<g/>
politických	politický	k2eAgFnPc2d1
<g/>
,	,	kIx,
špionážních	špionážní	k2eAgFnPc2d1
<g/>
,	,	kIx,
legislativních	legislativní	k2eAgFnPc2d1
a	a	k8xC
ekonomických	ekonomický	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
jako	jako	k9
komplex	komplex	k1gInSc1
představují	představovat	k5eAaImIp3nP
hrozbu	hrozba	k1gFnSc4
pro	pro	k7c4
ČR	ČR	kA
v	v	k7c6
oblasti	oblast	k1gFnSc6
ekonomické	ekonomický	k2eAgFnSc2d1
a	a	k8xC
vědecko-technické	vědecko-technický	k2eAgFnSc2d1
rozvědky	rozvědka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Zhenhua	Zhenhu	k1gInSc2
Data	datum	k1gNnSc2
Technology	technolog	k1gMnPc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
pracuje	pracovat	k5eAaImIp3nS
i	i	k9
pro	pro	k7c4
čínskou	čínský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
shromažďuje	shromažďovat	k5eAaImIp3nS
databázi	databáze	k1gFnSc4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
jsou	být	k5eAaImIp3nP
politici	politik	k1gMnPc1
<g/>
,	,	kIx,
diplomaté	diplomat	k1gMnPc1
<g/>
,	,	kIx,
pracovníci	pracovník	k1gMnPc1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
akademici	akademik	k1gMnPc1
a	a	k8xC
podnikatelé	podnikatel	k1gMnPc1
i	i	k9
jejich	jejich	k3xOp3gFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
předtím	předtím	k6eAd1
než	než	k8xS
svůj	svůj	k3xOyFgInSc4
web	web	k1gInSc4
vypnula	vypnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
označovala	označovat	k5eAaImAgFnS
se	se	k3xPyFc4
za	za	k7c2
pionýra	pionýr	k1gMnSc2
ve	v	k7c4
využití	využití	k1gNnSc4
dat	datum	k1gNnPc2
pro	pro	k7c4
hybridní	hybridní	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soubor	soubor	k1gInSc1
dat	datum	k1gNnPc2
se	se	k3xPyFc4
z	z	k7c2
jejích	její	k3xOp3gInPc2
serverů	server	k1gInPc2
dostal	dostat	k5eAaPmAgInS
k	k	k7c3
západním	západní	k2eAgNnPc3d1
médiím	médium	k1gNnPc3
prostřednictvím	prostřednictvím	k7c2
čínského	čínský	k2eAgMnSc2d1
whistleblowera	whistleblower	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
databázi	databáze	k1gFnSc6
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
700	#num#	k4
prominentních	prominentní	k2eAgMnPc2d1
Čechů	Čech	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
zahraniční	zahraniční	k2eAgFnPc4d1
ambasády	ambasáda	k1gFnPc4
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
policejní	policejní	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
<g/>
,	,	kIx,
ministerstvo	ministerstvo	k1gNnSc1
zdravotnictví	zdravotnictví	k1gNnSc2
<g/>
,	,	kIx,
úřad	úřad	k1gInSc1
stálého	stálý	k2eAgMnSc2d1
představitele	představitel	k1gMnSc2
ČR	ČR	kA
při	při	k7c6
NATO	NATO	kA
<g/>
,	,	kIx,
ČVUT	ČVUT	kA
nebo	nebo	k8xC
Akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Ekonomika	ekonomika	k1gFnSc1
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
Zemědělství	zemědělství	k1gNnSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
HDP	HDP	kA
v	v	k7c6
ČLR	ČLR	kA
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1952	#num#	k4
a	a	k8xC
2012	#num#	k4
</s>
<s>
Představitelé	představitel	k1gMnPc1
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Číny	Čína	k1gFnSc2
v	v	k7c6
květnu	květen	k1gInSc6
2014	#num#	k4
při	při	k7c6
podpisu	podpis	k1gInSc6
třicetileté	třicetiletý	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
na	na	k7c4
dodávky	dodávka	k1gFnPc4
zemního	zemní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
ze	z	k7c2
Sibiře	Sibiř	k1gFnSc2
do	do	k7c2
Číny	Čína	k1gFnSc2
a	a	k8xC
na	na	k7c4
stavbu	stavba	k1gFnSc4
plynovodu	plynovod	k1gInSc2
Síla	síla	k1gFnSc1
Sibiře	Sibiř	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
Rusko	Rusko	k1gNnSc4
byli	být	k5eAaImAgMnP
přítomni	přítomen	k2eAgMnPc1d1
prezident	prezident	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Putin	putin	k2eAgMnSc1d1
a	a	k8xC
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Gazpromu	Gazprom	k1gInSc2
Alexej	Alexej	k1gMnSc1
Miller	Miller	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
</s>
<s>
Díky	díky	k7c3
ekonomickým	ekonomický	k2eAgFnPc3d1
reformám	reforma	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
začal	začít	k5eAaPmAgInS
od	od	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
prosazovat	prosazovat	k5eAaImF
Teng	Teng	k1gInSc4
Siao-pching	Siao-pching	k1gInSc1
a	a	k8xC
na	na	k7c4
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
navázali	navázat	k5eAaPmAgMnP
jeho	jeho	k3xOp3gMnPc1
nástupci	nástupce	k1gMnPc1
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
v	v	k7c6
ČLR	ČLR	kA
v	v	k7c6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nastartován	nastartován	k2eAgInSc4d1
rychlý	rychlý	k2eAgInSc4d1
hospodářský	hospodářský	k2eAgInSc4d1
růst	růst	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
trvá	trvat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
a	a	k8xC
rychle	rychle	k6eAd1
stimuluje	stimulovat	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
oblasti	oblast	k1gFnPc4
národního	národní	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
země	zem	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
rostla	růst	k5eAaImAgFnS
tempem	tempo	k1gNnSc7
11,9	11,9	k4
%	%	kIx~
<g/>
,	,	kIx,
nejvyšším	vysoký	k2eAgMnSc6d3
za	za	k7c4
13	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrubý	hrubý	k2eAgInSc1d1
domácí	domácí	k2eAgInSc1d1
produkt	produkt	k1gInSc1
tak	tak	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgInS
cifry	cifra	k1gFnSc2
24,953	24,953	k4
biliónů	bilión	k4xCgInPc2
CNY	CNY	kA
(	(	kIx(
<g/>
tj.	tj.	kA
3,428	3,428	k4
biliónů	bilión	k4xCgInPc2
USD	USD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovšem	ovšem	k9
dlouhodobý	dlouhodobý	k2eAgInSc1d1
dvouciferný	dvouciferný	k2eAgInSc1d1
růst	růst	k1gInSc1
způsobil	způsobit	k5eAaPmAgInS
zvýšení	zvýšení	k1gNnSc4
inflace	inflace	k1gFnSc2
na	na	k7c4
4,8	4,8	k4
%	%	kIx~
<g/>
,	,	kIx,
oproti	oproti	k7c3
necelým	celý	k2eNgInPc3d1
2	#num#	k4
%	%	kIx~
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
2008	#num#	k4
inflace	inflace	k1gFnSc1
dokonce	dokonce	k9
vyšplhala	vyšplhat	k5eAaPmAgFnS
až	až	k9
na	na	k7c4
8,7	8,7	k4
%	%	kIx~
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
jedenáctileté	jedenáctiletý	k2eAgNnSc4d1
maximum	maximum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínský	čínský	k2eAgInSc1d1
vývoz	vývoz	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
vzrostl	vzrůst	k5eAaPmAgInS
o	o	k7c4
27,2	27,2	k4
%	%	kIx~
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
dovoz	dovoz	k1gInSc1
ve	v	k7c6
stejném	stejný	k2eAgNnSc6d1
období	období	k1gNnSc6
narostl	narůst	k5eAaPmAgInS
o	o	k7c4
20	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saldo	saldo	k1gNnSc1
obchodní	obchodní	k2eAgFnSc2d1
bilance	bilance	k1gFnSc2
tak	tak	k6eAd1
dosáhlo	dosáhnout	k5eAaPmAgNnS
přebytku	přebytek	k1gInSc2
177,5	177,5	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
(	(	kIx(
<g/>
oproti	oproti	k7c3
100	#num#	k4
mld.	mld.	k?
USD	USD	kA
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
předstihla	předstihnout	k5eAaPmAgFnS
britskou	britský	k2eAgFnSc7d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
německou	německý	k2eAgFnSc4d1
ekonomiku	ekonomika	k1gFnSc4
a	a	k8xC
s	s	k7c7
objemem	objem	k1gInSc7
HDP	HDP	kA
2,49	2,49	k4
bilionu	bilion	k4xCgInSc2
€	€	k?
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
tehdy	tehdy	k6eAd1
na	na	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
předstihla	předstihnout	k5eAaPmAgFnS
Japonsko	Japonsko	k1gNnSc4
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k8xC,k8xS
druhou	druhý	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
ekonomikou	ekonomika	k1gFnSc7
světa	svět	k1gInSc2
po	po	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
předstihla	předstihnout	k5eAaPmAgFnS
Čína	Čína	k1gFnSc1
i	i	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
co	co	k9
do	do	k7c2
objemu	objem	k1gInSc2
obchodovaného	obchodovaný	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
2014	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgInS
Mezinárodní	mezinárodní	k2eAgInSc1d1
měnový	měnový	k2eAgInSc4d1
fond	fond	k1gInSc4
ekonomiku	ekonomika	k1gFnSc4
Číny	Čína	k1gFnSc2
za	za	k7c4
nejsilnější	silný	k2eAgNnSc4d3
na	na	k7c6
světě	svět	k1gInSc6
co	co	k8xS
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
celkového	celkový	k2eAgInSc2d1
objemu	objem	k1gInSc2
HDP	HDP	kA
dle	dle	k7c2
parity	parita	k1gFnSc2
kupní	kupní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tři	tři	k4xCgMnPc1
čínští	čínský	k2eAgMnPc1d1
internetoví	internetový	k2eAgMnPc1d1
„	„	k?
<g/>
giganti	gigant	k1gMnPc1
<g/>
“	“	k?
Baidu	Baid	k1gInSc2
<g/>
,	,	kIx,
Tencent	Tencent	k1gMnSc1
a	a	k8xC
Alibaba	Alibaba	k1gMnSc1
Group	Group	k1gMnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
největší	veliký	k2eAgInPc4d3
technologické	technologický	k2eAgInPc4d1
konglomeráty	konglomerát	k1gInPc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Čínští	čínský	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
spotřební	spotřební	k2eAgFnSc2d1
elektroniky	elektronika	k1gFnSc2
jako	jako	k8xC,k8xS
Lenovo	Lenovo	k1gNnSc1
<g/>
,	,	kIx,
Huawei	Huawei	k1gNnSc1
<g/>
,	,	kIx,
ZTE	ZTE	kA
<g/>
,	,	kIx,
Xiaomi	Xiao	k1gFnPc7
<g/>
,	,	kIx,
Oppo	Oppo	k1gNnSc4
a	a	k8xC
Meizu	Meiza	k1gFnSc4
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejúspěšnější	úspěšný	k2eAgInSc4d3
na	na	k7c6
světovém	světový	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Energetika	energetika	k1gFnSc1
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
energetika	energetika	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
všech	všecek	k3xTgInPc6
dostupných	dostupný	k2eAgInPc6d1
typech	typ	k1gInPc6
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrická	elektrický	k2eAgFnSc1d1
energie	energie	k1gFnSc1
je	být	k5eAaImIp3nS
zatím	zatím	k6eAd1
vyráběna	vyrábět	k5eAaImNgFnS
především	především	k9
v	v	k7c6
tepelných	tepelný	k2eAgFnPc6d1
elektrárnách	elektrárna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vodních	vodní	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
je	být	k5eAaImIp3nS
nejvýznamnější	významný	k2eAgFnSc1d3
elektrárna	elektrárna	k1gFnSc1
Tři	tři	k4xCgInPc4
soutěsky	soutěsk	k1gInPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
největší	veliký	k2eAgFnSc7d3
elektrárnou	elektrárna	k1gFnSc7
(	(	kIx(
<g/>
nejen	nejen	k6eAd1
vodní	vodní	k2eAgInPc1d1
<g/>
)	)	kIx)
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
má	mít	k5eAaImIp3nS
i	i	k9
velmi	velmi	k6eAd1
ambiciózní	ambiciózní	k2eAgInSc1d1
projekt	projekt	k1gInSc1
výstavby	výstavba	k1gFnSc2
jaderných	jaderný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
výkon	výkon	k1gInSc4
zařízení	zařízení	k1gNnSc2
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
dvojnásobný	dvojnásobný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
výstavbě	výstavba	k1gFnSc6
jaderných	jaderný	k2eAgInPc2d1
reaktorů	reaktor	k1gInPc2
v	v	k7c6
Číně	Čína	k1gFnSc6
pracují	pracovat	k5eAaImIp3nP
jak	jak	k6eAd1
francouzské	francouzský	k2eAgFnPc1d1
<g/>
,	,	kIx,
tak	tak	k9
americké	americký	k2eAgFnSc2d1
a	a	k8xC
ruské	ruský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
havárii	havárie	k1gFnSc6
v	v	k7c6
japonské	japonský	k2eAgFnSc6d1
Fukušimě	Fukušima	k1gFnSc6
bylo	být	k5eAaImAgNnS
sice	sice	k8xC
dočasně	dočasně	k6eAd1
pozastaveno	pozastaven	k2eAgNnSc1d1
povolování	povolování	k1gNnSc1
staveb	stavba	k1gFnPc2
nových	nový	k2eAgFnPc2d1
jaderných	jaderný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
a	a	k8xC
provedeny	proveden	k2eAgFnPc1d1
prověrky	prověrka	k1gFnPc1
v	v	k7c6
těch	ten	k3xDgFnPc6
stávajících	stávající	k2eAgFnPc6d1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
Čína	Čína	k1gFnSc1
hodlá	hodlat	k5eAaImIp3nS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
projaderné	projaderný	k2eAgFnSc6d1
politice	politika	k1gFnSc6
bez	bez	k7c2
velké	velký	k2eAgFnSc2d1
změny	změna	k1gFnSc2
pokračovat	pokračovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zahraniční	zahraniční	k2eAgInSc1d1
obchod	obchod	k1gInSc1
</s>
<s>
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
světových	světový	k2eAgMnPc2d1
exportérů	exportér	k1gMnPc2
a	a	k8xC
importérů	importér	k1gMnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dosahuje	dosahovat	k5eAaImIp3nS
značného	značný	k2eAgInSc2d1
přebytku	přebytek	k1gInSc2
obchodní	obchodní	k2eAgFnSc2d1
bilance	bilance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
důležitým	důležitý	k2eAgMnSc7d1
obchodním	obchodní	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
Číny	Čína	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
Německo	Německo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
čínsko-německý	čínsko-německý	k2eAgInSc1d1
obchod	obchod	k1gInSc1
v	v	k7c6
obou	dva	k4xCgInPc6
směrech	směr	k1gInPc6
objemu	objem	k1gInSc2
170	#num#	k4
miliard	miliarda	k4xCgFnPc2
eur	euro	k1gNnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
přibližně	přibližně	k6eAd1
4	#num#	k4
515	#num#	k4
miliard	miliarda	k4xCgFnPc2
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s>
Německá	německý	k2eAgFnSc1d1
kancléřka	kancléřka	k1gFnSc1
Angela	Angela	k1gFnSc1
Merkelová	Merkelová	k1gFnSc1
navštěvuje	navštěvovat	k5eAaImIp3nS
Čínu	Čína	k1gFnSc4
každoročně	každoročně	k6eAd1
<g/>
,	,	kIx,
za	za	k7c4
dobu	doba	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
působení	působení	k1gNnSc2
na	na	k7c6
vrcholném	vrcholný	k2eAgInSc6d1
postu	post	k1gInSc6
do	do	k7c2
září	září	k1gNnSc2
2019	#num#	k4
již	již	k9
15	#num#	k4
<g/>
krát	krát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vždy	vždy	k6eAd1
je	být	k5eAaImIp3nS
doprovázena	doprovázet	k5eAaImNgFnS
významnými	významný	k2eAgInPc7d1
německými	německý	k2eAgInPc7d1
manažery	manažer	k1gInPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
dalšího	další	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
vzájemného	vzájemný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
doposud	doposud	k6eAd1
poslední	poslední	k2eAgFnSc6d1
návštěvě	návštěva	k1gFnSc6
Merkelové	Merkelová	k1gFnSc2
v	v	k7c6
Pekingu	Peking	k1gInSc6
bylo	být	k5eAaImAgNnS
5	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2019	#num#	k4
podepsáno	podepsat	k5eAaPmNgNnS
11	#num#	k4
smluv	smlouva	k1gFnPc2
o	o	k7c6
hospodářské	hospodářský	k2eAgFnSc6d1
spolupráci	spolupráce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
nových	nový	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
je	být	k5eAaImIp3nS
dohoda	dohoda	k1gFnSc1
francouzsko-německého	francouzsko-německý	k2eAgInSc2d1
koncernu	koncern	k1gInSc2
Airbus	airbus	k1gInSc1
o	o	k7c4
rozšíření	rozšíření	k1gNnSc4
montáže	montáž	k1gFnSc2
dopravního	dopravní	k2eAgNnSc2d1
letadla	letadlo	k1gNnSc2
typu	typ	k1gInSc2
Airbus	airbus	k1gInSc1
A320	A320	k1gFnSc2
v	v	k7c6
již	již	k6eAd1
existující	existující	k2eAgFnSc3d1
výrobně	výrobna	k1gFnSc3
čínské	čínský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
AVIC	AVIC	kA
Aircraft	Aircraft	k1gInSc1
Corporation	Corporation	k1gInSc1
ve	v	k7c6
městě	město	k1gNnSc6
Tchien-ťin	Tchien-ťina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
smlouvy	smlouva	k1gFnPc4
se	se	k3xPyFc4
týkají	týkat	k5eAaImIp3nP
mj.	mj.	kA
energetiky	energetika	k1gFnSc2
a	a	k8xC
elektromobility	elektromobilita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
ALBA	alba	k1gFnSc1
Group	Group	k1gInSc4
bude	být	k5eAaImBp3nS
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
velkoměstem	velkoměsto	k1gNnSc7
Šen-čen	Šen-čno	k1gNnPc2
v	v	k7c6
oboru	obor	k1gInSc6
zpracování	zpracování	k1gNnSc2
odpadů	odpad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
Siemens	siemens	k1gInSc1
bude	být	k5eAaImBp3nS
kooperovat	kooperovat	k5eAaImF
s	s	k7c7
čínskou	čínský	k2eAgFnSc7d1
státní	státní	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
SPIC	SPIC	kA
při	při	k7c6
výrobě	výroba	k1gFnSc6
plynových	plynový	k2eAgFnPc2d1
turbín	turbína	k1gFnPc2
pro	pro	k7c4
energetiku	energetika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc1d3
německá	německý	k2eAgFnSc1d1
a	a	k8xC
evropská	evropský	k2eAgFnSc1d1
pojišťovna	pojišťovna	k1gFnSc1
Allianz	Allianza	k1gFnPc2
se	se	k3xPyFc4
dohodla	dohodnout	k5eAaPmAgFnS
s	s	k7c7
čínskou	čínský	k2eAgFnSc7d1
velkobankou	velkobanka	k1gFnSc7
Bank	banka	k1gFnPc2
of	of	k?
China	China	k1gFnSc1
na	na	k7c4
prohloubení	prohloubení	k1gNnSc4
spolupráce	spolupráce	k1gFnSc2
při	při	k7c6
financování	financování	k1gNnSc6
a	a	k8xC
pojišťování	pojišťování	k1gNnSc6
hospodářské	hospodářský	k2eAgFnSc2d1
výměny	výměna	k1gFnSc2
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Úspěšná	úspěšný	k2eAgNnPc4d1
jednání	jednání	k1gNnPc4
mezi	mezi	k7c7
německou	německý	k2eAgFnSc7d1
a	a	k8xC
čínskou	čínský	k2eAgFnSc7d1
delegací	delegace	k1gFnSc7
nebyla	být	k5eNaImAgFnS
nijak	nijak	k6eAd1
ovlivněna	ovlivnit	k5eAaPmNgFnS
aktuálními	aktuální	k2eAgFnPc7d1
událostmi	událost	k1gFnPc7
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kancléřka	Kancléřka	k1gFnSc1
Merkelová	Merkelová	k1gFnSc1
ovšem	ovšem	k9
zmínila	zmínit	k5eAaPmAgFnS
nutnost	nutnost	k1gFnSc1
poklidného	poklidný	k2eAgNnSc2d1
vyřešení	vyřešení	k1gNnSc2
současné	současný	k2eAgFnSc2d1
situace	situace	k1gFnSc2
v	v	k7c6
této	tento	k3xDgFnSc6
autonomní	autonomní	k2eAgFnSc6d1
čínské	čínský	k2eAgFnSc6d1
provincii	provincie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínský	čínský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
Li	li	k8xS
Kche-čchiang	Kche-čchiang	k1gMnSc1
odpověděl	odpovědět	k5eAaPmAgMnS
na	na	k7c4
tuto	tento	k3xDgFnSc4
zmínku	zmínka	k1gFnSc4
v	v	k7c6
tom	ten	k3xDgInSc6
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
Čína	Čína	k1gFnSc1
„	„	k?
<g/>
je	být	k5eAaImIp3nS
schopná	schopný	k2eAgFnSc1d1
ukončit	ukončit	k5eAaPmF
chaos	chaos	k1gInSc4
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
a	a	k8xC
znova	znova	k6eAd1
tam	tam	k6eAd1
nastolit	nastolit	k5eAaPmF
pořádek	pořádek	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
zákonů	zákon	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahraničí	zahraničí	k1gNnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
spolehnout	spolehnout	k5eAaPmF
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
Čína	Čína	k1gFnSc1
má	mít	k5eAaImIp3nS
pro	pro	k7c4
to	ten	k3xDgNnSc4
„	„	k?
<g/>
moudrost	moudrost	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
názoru	názor	k1gInSc2
šéfredaktora	šéfredaktor	k1gMnSc2
německého	německý	k2eAgInSc2d1
zpravodajského	zpravodajský	k2eAgInSc2d1
webu	web	k1gInSc2
T-Online	T-Onlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
de	de	k?
Floriana	Florian	k1gMnSc2
Harmse	Harms	k1gMnSc2
„	„	k?
<g/>
diktuje	diktovat	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
pravidla	pravidlo	k1gNnSc2
vzájemné	vzájemný	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
i	i	k9
Německu	Německo	k1gNnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
hedvábná	hedvábný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2013	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
čínský	čínský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
záměr	záměr	k1gInSc4
vybudování	vybudování	k1gNnPc2
„	„	k?
<g/>
Nové	Nové	k2eAgFnSc2d1
hedvábné	hedvábný	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
dopravu	doprava	k1gFnSc4
vhodného	vhodný	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
v	v	k7c6
kontejnerech	kontejner	k1gInPc6
mají	mít	k5eAaImIp3nP
přitom	přitom	k6eAd1
být	být	k5eAaImF
využity	využít	k5eAaPmNgFnP
již	již	k6eAd1
existující	existující	k2eAgFnPc1d1
železniční	železniční	k2eAgFnPc1d1
trati	trať	k1gFnPc1
<g/>
,	,	kIx,
hlavně	hlavně	k9
v	v	k7c6
Číně	Čína	k1gFnSc6
samotné	samotný	k2eAgFnSc6d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Transsibiřská	transsibiřský	k2eAgFnSc1d1
magistrála	magistrála	k1gFnSc1
a	a	k8xC
tratě	trať	k1gFnPc1
přes	přes	k7c4
evropské	evropský	k2eAgNnSc4d1
Rusko	Rusko	k1gNnSc4
<g/>
,	,	kIx,
Bělorusko	Bělorusko	k1gNnSc4
<g/>
,	,	kIx,
Ukrajinu	Ukrajina	k1gFnSc4
a	a	k8xC
Polsko	Polsko	k1gNnSc1
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
Duisburg	Duisburg	k1gInSc1
důležitým	důležitý	k2eAgNnSc7d1
překladištěm	překladiště	k1gNnSc7
u	u	k7c2
splavné	splavný	k2eAgFnSc2d1
řeky	řeka	k1gFnSc2
Rýn	rýna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Také	také	k9
Indie	Indie	k1gFnSc1
a	a	k8xC
Írán	Írán	k1gInSc1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
na	na	k7c4
Novou	nový	k2eAgFnSc4d1
hedvábnou	hedvábný	k2eAgFnSc4d1
stezku	stezka	k1gFnSc4
napojeny	napojen	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
přijel	přijet	k5eAaPmAgMnS
první	první	k4xOgInSc4
vlak	vlak	k1gInSc4
z	z	k7c2
východní	východní	k2eAgFnSc2d1
čínské	čínský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
Če-ťiang	Če-ťianga	k1gFnPc2
do	do	k7c2
Teheránu	Teherán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
lednu	leden	k1gInSc6
2017	#num#	k4
byl	být	k5eAaImAgInS
vypraven	vypravit	k5eAaPmNgInS
první	první	k4xOgInSc1
vlak	vlak	k1gInSc1
až	až	k9
do	do	k7c2
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
celou	celý	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
s	s	k7c7
délkou	délka	k1gFnSc7
12	#num#	k4
000	#num#	k4
km	km	kA
ujel	ujet	k5eAaPmAgMnS
za	za	k7c4
plánovaných	plánovaný	k2eAgInPc2d1
18	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zboží	zboží	k1gNnSc1
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
ovšem	ovšem	k9
několikrát	několikrát	k6eAd1
překládáno	překládán	k2eAgNnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
kvůli	kvůli	k7c3
rozdílným	rozdílný	k2eAgInPc3d1
rozchodům	rozchod	k1gInPc3
kolejí	kolej	k1gFnPc2
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
na	na	k7c6
hranicích	hranice	k1gFnPc6
např.	např.	kA
mezi	mezi	k7c7
Ukrajinou	Ukrajina	k1gFnSc7
a	a	k8xC
Polskem	Polsko	k1gNnSc7
měněny	měnit	k5eAaImNgFnP
lokomotivy	lokomotiva	k1gFnPc1
i	i	k8xC
vagóny	vagón	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Existují	existovat	k5eAaImIp3nP
také	také	k9
snahy	snaha	k1gFnPc1
o	o	k7c4
napojení	napojení	k1gNnSc4
Rakouska	Rakousko	k1gNnSc2
přes	přes	k7c4
Slovensko	Slovensko	k1gNnSc4
na	na	k7c4
čínský	čínský	k2eAgInSc4d1
projekt	projekt	k1gInSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
vybudována	vybudován	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
asi	asi	k9
450	#num#	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
širokorozchodná	širokorozchodný	k2eAgFnSc1d1
trať	trať	k1gFnSc1
z	z	k7c2
Košic	Košice	k1gInPc2
(	(	kIx(
<g/>
kam	kam	k6eAd1
již	již	k9
taková	takový	k3xDgFnSc1
trať	trať	k1gFnSc1
vede	vést	k5eAaImIp3nS
z	z	k7c2
Užhorodu	Užhorod	k1gInSc2
<g/>
)	)	kIx)
přes	přes	k7c4
Bratislavu	Bratislava	k1gFnSc4
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
tento	tento	k3xDgInSc4
projekt	projekt	k1gInSc4
mají	mít	k5eAaImIp3nP
Rakouské	rakouský	k2eAgFnSc2d1
spolkové	spolkový	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
velký	velký	k2eAgInSc1d1
zájem	zájem	k1gInSc1
<g/>
,	,	kIx,
zatím	zatím	k6eAd1
mu	on	k3xPp3gInSc3
však	však	k9
brání	bránit	k5eAaImIp3nS
válka	válka	k1gFnSc1
na	na	k7c6
východní	východní	k2eAgFnSc6d1
Ukrajině	Ukrajina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tzv.	tzv.	kA
Nová	nový	k2eAgFnSc1d1
hedvábná	hedvábný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
Jeden	jeden	k4xCgInSc1
pás	pás	k1gInSc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
cesta	cesta	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
One	One	k1gMnSc1
Belt	Belt	k1gMnSc1
<g/>
,	,	kIx,
One	One	k1gMnSc1
Road	Road	k1gMnSc1
<g/>
,	,	kIx,
zkr.	zkr.	kA
OBOR	obor	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
měkčím	měkký	k2eAgNnSc7d2
vyjádřením	vyjádření	k1gNnSc7
Belt	Belt	k1gMnSc1
and	and	k?
Road	Road	k1gMnSc1
Iniciative	Iniciativ	k1gInSc5
<g/>
,	,	kIx,
BRI	BRI	kA
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
oficiálně	oficiálně	k6eAd1
sloužit	sloužit	k5eAaImF
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
dopravních	dopravní	k2eAgInPc2d1
koridorů	koridor	k1gInPc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgInPc7
se	se	k3xPyFc4
propojí	propojit	k5eAaPmIp3nS
infrastruktura	infrastruktura	k1gFnSc1
Asie	Asie	k1gFnSc2
a	a	k8xC
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
čínských	čínský	k2eAgFnPc2d1
velmocenských	velmocenský	k2eAgFnPc2d1
ambicí	ambice	k1gFnPc2
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
významným	významný	k2eAgInSc7d1
strategickým	strategický	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
a	a	k8xC
začátkem	začátek	k1gInSc7
čínské	čínský	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
globalizace	globalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
mocenského	mocenský	k2eAgInSc2d1
boje	boj	k1gInSc2
s	s	k7c7
USA	USA	kA
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
omezit	omezit	k5eAaPmF
vliv	vliv	k1gInSc4
Japonska	Japonsko	k1gNnSc2
ve	v	k7c6
Východní	východní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
Indočíně	Indočína	k1gFnSc6
<g/>
,	,	kIx,
eventuálně	eventuálně	k6eAd1
výměnou	výměna	k1gFnSc7
za	za	k7c4
ekonomické	ekonomický	k2eAgFnPc4d1
investice	investice	k1gFnPc4
navázat	navázat	k5eAaPmF
spolupráci	spolupráce	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslib	příslib	k1gInSc1
čínských	čínský	k2eAgFnPc2d1
investic	investice	k1gFnPc2
je	být	k5eAaImIp3nS
vykládán	vykládat	k5eAaImNgInS
jako	jako	k8xC,k8xS
ekonomický	ekonomický	k2eAgInSc1d1
nástroj	nástroj	k1gInSc1
Číny	Čína	k1gFnSc2
ve	v	k7c6
snaze	snaha	k1gFnSc6
získat	získat	k5eAaPmF
spojence	spojenec	k1gMnPc4
pro	pro	k7c4
přiznání	přiznání	k1gNnSc4
statutu	statut	k1gInSc2
tržní	tržní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
dle	dle	k7c2
bruselských	bruselský	k2eAgMnPc2d1
diplomatů	diplomat	k1gMnPc2
v	v	k7c6
současnosti	současnost	k1gFnSc6
důležitým	důležitý	k2eAgInSc7d1
zájmem	zájem	k1gInSc7
čínské	čínský	k2eAgFnSc2d1
agendy	agenda	k1gFnSc2
v	v	k7c6
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peking	Peking	k1gInSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
uplatňovat	uplatňovat	k5eAaImF
svůj	svůj	k3xOyFgInSc4
vliv	vliv	k1gInSc4
ve	v	k7c6
světě	svět	k1gInSc6
také	také	k9
přes	přes	k7c4
Východní	východní	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
prostřednictvím	prostřednictvím	k7c2
iniciativy	iniciativa	k1gFnSc2
16	#num#	k4
+	+	kIx~
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
EU	EU	kA
je	být	k5eAaImIp3nS
vyjednávání	vyjednávání	k1gNnPc4
otevřená	otevřený	k2eAgNnPc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
multilaterální	multilaterální	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
nikoli	nikoli	k9
pouze	pouze	k6eAd1
formou	forma	k1gFnSc7
bilaterálních	bilaterální	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
doporučuje	doporučovat	k5eAaImIp3nS
užší	úzký	k2eAgFnSc3d2
spolupráci	spolupráce	k1gFnSc3
s	s	k7c7
Čínou	Čína	k1gFnSc7
v	v	k7c6
otázkách	otázka	k1gFnPc6
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
boji	boj	k1gInSc6
proti	proti	k7c3
změnám	změna	k1gFnPc3
klimatu	klima	k1gNnSc2
<g/>
,	,	kIx,
ČLR	ČLR	kA
však	však	k9
zároveň	zároveň	k6eAd1
označuje	označovat	k5eAaImIp3nS
za	za	k7c4
„	„	k?
<g/>
ekonomického	ekonomický	k2eAgMnSc2d1
rivala	rival	k1gMnSc2
usilujícího	usilující	k2eAgMnSc2d1
o	o	k7c4
vůdčí	vůdčí	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
v	v	k7c6
zavádění	zavádění	k1gNnSc6
technologií	technologie	k1gFnPc2
a	a	k8xC
systémového	systémový	k2eAgMnSc2d1
rivala	rival	k1gMnSc2
v	v	k7c6
prosazování	prosazování	k1gNnSc6
alternativních	alternativní	k2eAgInPc2d1
modelů	model	k1gInPc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
interních	interní	k2eAgMnPc2d1
čínských	čínský	k2eAgMnPc2d1
dokumentů	dokument	k1gInPc2
lze	lze	k6eAd1
vyčíst	vyčíst	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
mezi	mezi	k7c4
běžné	běžný	k2eAgFnPc4d1
obyvatele	obyvatel	k1gMnSc2
západních	západní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
čínská	čínský	k2eAgFnSc1d1
externí	externí	k2eAgFnSc1d1
propaganda	propaganda	k1gFnSc1
nepronikla	proniknout	k5eNaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
byla	být	k5eAaImAgFnS
dokonce	dokonce	k9
tamějšími	tamější	k2eAgMnPc7d1
politiky	politik	k1gMnPc7
dezinterpretována	dezinterpretován	k2eAgFnSc1d1
jako	jako	k9
„	„	k?
<g/>
čínská	čínský	k2eAgFnSc1d1
hrozba	hrozba	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
korigovat	korigovat	k5eAaBmF
náš	náš	k3xOp1gInSc4
přístup	přístup	k1gInSc4
a	a	k8xC
zaměřit	zaměřit	k5eAaPmF
se	s	k7c7
„	„	k?
<g/>
na	na	k7c4
východ	východ	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
vyspělými	vyspělý	k2eAgInPc7d1
státy	stát	k1gInPc7
Evropy	Evropa	k1gFnSc2
a	a	k8xC
Ameriky	Amerika	k1gFnSc2
představují	představovat	k5eAaImIp3nP
skupinu	skupina	k1gFnSc4
nejvhodnější	vhodný	k2eAgFnSc4d3
k	k	k7c3
„	„	k?
<g/>
indoktrinaci	indoktrinace	k1gFnSc3
<g/>
“	“	k?
(	(	kIx(
<g/>
doslova	doslova	k6eAd1
<g/>
:	:	kIx,
k	k	k7c3
vymývání	vymývání	k1gNnSc3
mozků	mozek	k1gInPc2
a	a	k8xC
získávání	získávání	k1gNnSc4
srdcí	srdce	k1gNnPc2
[	[	kIx(
<g/>
si	se	k3xPyFc3
nao	nao	k?
jing	jinga	k1gFnPc2
sin	sin	kA
<g/>
]	]	kIx)
<g/>
)	)	kIx)
rozvojové	rozvojový	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
tato	tento	k3xDgFnSc1
čínská	čínský	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
Hedvábné	hedvábný	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
má	mít	k5eAaImIp3nS
již	již	k6eAd1
řadu	řada	k1gFnSc4
obětí	oběť	k1gFnPc2
v	v	k7c6
podobě	podoba	k1gFnSc6
korupčních	korupční	k2eAgInPc2d1
skandálů	skandál	k1gInPc2
a	a	k8xC
astronomického	astronomický	k2eAgNnSc2d1
zadlužení	zadlužení	k1gNnSc2
států	stát	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
přistoupily	přistoupit	k5eAaPmAgInP
na	na	k7c4
nabídky	nabídka	k1gFnPc4
čínských	čínský	k2eAgFnPc2d1
investic	investice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Obyvatelstvo	obyvatelstvo	k1gNnSc4
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
hustoty	hustota	k1gFnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
ČLR	ČLR	kA
a	a	k8xC
Tchaj-wanu	Tchaj-wan	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provincie	provincie	k1gFnSc1
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřežní	pobřežní	k2eAgFnSc2d1
jsou	být	k5eAaImIp3nP
mnohem	mnohem	k6eAd1
hustěji	husto	k6eAd2
obydlené	obydlený	k2eAgNnSc1d1
než	než	k8xS
vnitrozemí	vnitrozemí	k1gNnSc1
na	na	k7c6
západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Sčítání	sčítání	k1gNnSc1
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
zaznamenalo	zaznamenat	k5eAaPmAgNnS
počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
přibližně	přibližně	k6eAd1
1	#num#	k4
370	#num#	k4
536	#num#	k4
875	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
16,60	16,60	k4
<g/>
%	%	kIx~
populace	populace	k1gFnPc4
bylo	být	k5eAaImAgNnS
14	#num#	k4
let	léto	k1gNnPc2
nebo	nebo	k8xC
mladších	mladý	k2eAgMnPc2d2
<g/>
,	,	kIx,
70,14	70,14	k4
<g/>
%	%	kIx~
bylo	být	k5eAaImAgNnS
ve	v	k7c6
věku	věk	k1gInSc6
15	#num#	k4
až	až	k9
59	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
13,26	13,26	k4
<g/>
%	%	kIx~
bylo	být	k5eAaImAgNnS
starších	starý	k2eAgFnPc2d2
než	než	k8xS
60	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Míra	Míra	k1gFnSc1
populačního	populační	k2eAgInSc2d1
růstu	růst	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
byla	být	k5eAaImAgFnS
odhadována	odhadovat	k5eAaImNgFnS
na	na	k7c4
0,46	0,46	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Navzdory	navzdory	k7c3
politice	politika	k1gFnSc3
jednoho	jeden	k4xCgNnSc2
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
ČLR	ČLR	kA
zavedla	zavést	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
Číny	Čína	k1gFnSc2
zvýšil	zvýšit	k5eAaPmAgInS
z	z	k7c2
přibližně	přibližně	k6eAd1
550	#num#	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
době	doba	k1gFnSc6
vzniku	vznik	k1gInSc2
ČLR	ČLR	kA
na	na	k7c4
1,4	1,4	k4
miliardy	miliarda	k4xCgFnPc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čína	Čína	k1gFnSc1
tvořila	tvořit	k5eAaImAgFnS
většinu	většina	k1gFnSc4
chudých	chudý	k2eAgInPc2d1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
;	;	kIx,
nyní	nyní	k6eAd1
Čína	Čína	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
většinu	většina	k1gFnSc4
světové	světový	k2eAgFnSc2d1
střední	střední	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etnické	etnický	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
</s>
<s>
Čína	Čína	k1gFnSc1
oficiálně	oficiálně	k6eAd1
uznává	uznávat	k5eAaImIp3nS
56	#num#	k4
různých	různý	k2eAgFnPc2d1
etnických	etnický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
největší	veliký	k2eAgFnSc4d3
část	část	k1gFnSc4
představují	představovat	k5eAaImIp3nP
Chanové	Chan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
tvoří	tvořit	k5eAaImIp3nP
asi	asi	k9
91,51	91,51	k4
<g/>
%	%	kIx~
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
</s>
<s>
populace	populace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Chanové	Chanové	k2eAgFnSc1d1
-	-	kIx~
nejpočetnější	početní	k2eAgFnSc1d3
světová	světový	k2eAgFnSc1d1
etnická	etnický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
-	-	kIx~
převyšují	převyšovat	k5eAaImIp3nP
ostatní	ostatní	k2eAgFnPc1d1
etnické	etnický	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
v	v	k7c6
každé	každý	k3xTgFnSc6
provincii	provincie	k1gFnSc6
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Tibetu	Tibet	k1gInSc2
a	a	k8xC
Sin-ťiangu	Sin-ťiang	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
tvoří	tvořit	k5eAaImIp3nP
etnické	etnický	k2eAgFnPc1d1
menšiny	menšina	k1gFnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
asi	asi	k9
8,49	8,49	k4
<g/>
%	%	kIx~
obyvatel	obyvatel	k1gMnSc1
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
porovnání	porovnání	k1gNnSc6
se	s	k7c7
sčítáním	sčítání	k1gNnSc7
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
vzrostla	vzrůst	k5eAaPmAgFnS
populace	populace	k1gFnSc1
Chanů	Chan	k1gMnPc2
o	o	k7c4
66	#num#	k4
537	#num#	k4
177	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
o	o	k7c4
5,74	5,74	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
populace	populace	k1gFnSc1
55	#num#	k4
národnostních	národnostní	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
se	se	k3xPyFc4
zvýšila	zvýšit	k5eAaPmAgFnS
o	o	k7c4
7	#num#	k4
362	#num#	k4
627	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
i	i	k9
6,92	6,92	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Sčítání	sčítání	k1gNnSc1
z	z	k7c2
ruku	ruka	k1gFnSc4
2010	#num#	k4
zaznamenalo	zaznamenat	k5eAaPmAgNnS
celkem	celek	k1gInSc7
593	#num#	k4
832	#num#	k4
v	v	k7c6
Číně	Čína	k1gFnSc6
žijících	žijící	k2eAgMnPc2d1
cizinců	cizinec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnPc4d3
skupiny	skupina	k1gFnPc4
pocházely	pocházet	k5eAaImAgFnP
z	z	k7c2
Jižní	jižní	k2eAgFnSc2d1
Koreje	Korea	k1gFnSc2
(	(	kIx(
<g/>
120	#num#	k4
750	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
71	#num#	k4
493	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Japonska	Japonsko	k1gNnSc2
(	(	kIx(
<g/>
66	#num#	k4
159	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3
etnickými	etnický	k2eAgFnPc7d1
menšinami	menšina	k1gFnPc7
v	v	k7c6
Číně	Čína	k1gFnSc6
(	(	kIx(
<g/>
údaje	údaj	k1gInPc4
podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
Čuangové	Čuang	k1gMnPc1
(	(	kIx(
<g/>
16	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mandžuové	Mandžu	k1gMnPc1
(	(	kIx(
<g/>
10	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ujgurové	Ujgur	k1gMnPc1
(	(	kIx(
<g/>
9	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Chuejové	Chuej	k1gMnPc1
(	(	kIx(
<g/>
9	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Miaové	Miaová	k1gFnPc1
(	(	kIx(
<g/>
8	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Iové	Iové	k1gNnSc1
(	(	kIx(
<g/>
7	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tchuťiaové	Tchuťiaové	k2eAgFnSc1d1
(	(	kIx(
<g/>
5,75	5,75	k4
milionu	milion	k4xCgInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mongolové	Mongol	k1gMnPc1
(	(	kIx(
<g/>
5	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tibeťané	Tibeťan	k1gMnPc1
(	(	kIx(
<g/>
5	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Puejové	Puej	k1gMnPc1
(	(	kIx(
<g/>
3	#num#	k4
miliony	milion	k4xCgInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tungové	Tung	k1gMnPc1
(	(	kIx(
<g/>
3	#num#	k4
miliony	milion	k4xCgInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jaové	Jaová	k1gFnSc2
(	(	kIx(
<g/>
2,6	2,6	k4
milionu	milion	k4xCgInSc2
<g/>
)	)	kIx)
a	a	k8xC
Korejci	Korejec	k1gMnPc1
(	(	kIx(
<g/>
2	#num#	k4
miliony	milion	k4xCgInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jazyky	jazyk	k1gInPc1
</s>
<s>
Etnolingvistická	Etnolingvistický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
ČLR	ČLR	kA
a	a	k8xC
Tchaj-wanu	Tchaj-wan	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
</s>
<s>
V	v	k7c6
Číně	Čína	k1gFnSc6
je	být	k5eAaImIp3nS
asi	asi	k9
292	#num#	k4
aktivních	aktivní	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
Nejpoužívanější	používaný	k2eAgNnSc1d3
náleží	náležet	k5eAaImIp3nS
do	do	k7c2
sinotibetských	sinotibetský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
zahrnují	zahrnovat	k5eAaImIp3nP
mandarínštinu	mandarínština	k1gFnSc4
(	(	kIx(
<g/>
hovoří	hovořit	k5eAaImIp3nS
s	s	k7c7
ní	on	k3xPp3gFnSc7
70	#num#	k4
<g/>
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
další	další	k2eAgFnPc4d1
varianty	varianta	k1gFnPc4
čínštiny	čínština	k1gFnSc2
<g/>
:	:	kIx,
Jüe	Jüe	k1gFnSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
kantonštiny	kantonština	k1gFnSc2
a	a	k8xC
tchajšanštiny	tchajšanština	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
wu	wu	k?
<g/>
,	,	kIx,
min	mina	k1gFnPc2
<g/>
,	,	kIx,
siang	sianga	k1gFnPc2
<g/>
,	,	kIx,
kan	kan	k?
a	a	k8xC
hakka	hakek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tibetobarmské	Tibetobarmský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
tibetštiny	tibetština	k1gFnSc2
<g/>
,	,	kIx,
qiangu	qiang	k1gInSc2
<g/>
,	,	kIx,
nakhi	nakh	k1gFnSc2
a	a	k8xC
nuosu	nuos	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
v	v	k7c6
oblasti	oblast	k1gFnSc6
tibetské	tibetský	k2eAgFnSc2d1
náhorní	náhorní	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
a	a	k8xC
jünnansko-kuejčouské	jünnansko-kuejčouský	k2eAgFnSc2d1
vysočiny	vysočina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
se	se	k3xPyFc4
hovoří	hovořit	k5eAaImIp3nS
v	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
(	(	kIx(
<g/>
čuangština	čuangština	k1gFnSc1
<g/>
,	,	kIx,
thajština	thajština	k1gFnSc1
a	a	k8xC
kam-sujské	kam-sujský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
z	z	k7c2
tajsko-kadajských	tajsko-kadajský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
hmong-mienské	hmong-mienský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
a	a	k8xC
wa	wa	k?
z	z	k7c2
austroasijské	austroasijský	k2eAgFnSc2d1
jazykové	jazykový	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napříč	napříč	k6eAd1
severovýchodní	severovýchodní	k2eAgFnSc7d1
a	a	k8xC
severozápadní	severozápadní	k2eAgFnSc7d1
Čínou	Čína	k1gFnSc7
používají	používat	k5eAaImIp3nP
zdejší	zdejší	k2eAgFnPc1d1
etnické	etnický	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
altajské	altajský	k2eAgFnPc1d1
jazyky	jazyk	k1gInPc4
včetně	včetně	k7c2
mandžuštiny	mandžuština	k1gFnSc2
<g/>
,	,	kIx,
mongolštiny	mongolština	k1gFnSc2
a	a	k8xC
některé	některý	k3yIgInPc1
turkické	turkický	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
ujgurštinu	ujgurština	k1gFnSc4
<g/>
,	,	kIx,
kazaštinu	kazaština	k1gFnSc4
<g/>
,	,	kIx,
kyrgyzštinu	kyrgyzština	k1gFnSc4
<g/>
,	,	kIx,
salarštinu	salarština	k1gFnSc4
a	a	k8xC
západní	západní	k2eAgFnSc4d1
jugurštinu	jugurština	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korejština	korejština	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
podél	podél	k7c2
hranice	hranice	k1gFnSc2
se	s	k7c7
Severní	severní	k2eAgFnSc7d1
Koreou	Korea	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saríqólština	Saríqólština	k1gFnSc1
<g/>
,	,	kIx,
jazyk	jazyk	k1gInSc1
Tádžiků	Tádžik	k1gMnPc2
na	na	k7c6
západě	západ	k1gInSc6
Sin-ťiangu	Sin-ťiang	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
indoevropský	indoevropský	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tchajwanští	tchajwanský	k2eAgMnPc1d1
domorodci	domorodec	k1gMnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
malé	malý	k2eAgFnSc2d1
populace	populace	k1gFnSc2
v	v	k7c6
pevninské	pevninský	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
používají	používat	k5eAaImIp3nP
austronéské	austronéský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
čínština	čínština	k1gFnSc1
<g/>
,	,	kIx,
varianta	varianta	k1gFnSc1
mandarínštiny	mandarínština	k1gFnSc2
založená	založený	k2eAgFnSc1d1
na	na	k7c6
pekingském	pekingský	k2eAgInSc6d1
dialektu	dialekt	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgInSc4d1
národní	národní	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
Číny	Čína	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
jako	jako	k8xS,k8xC
lingua	lingu	k2eAgFnSc1d1
franca	franca	k1gFnSc1
v	v	k7c6
zemi	zem	k1gFnSc6
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
různého	různý	k2eAgInSc2d1
lingvistického	lingvistický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čínské	čínský	k2eAgInPc1d1
znaky	znak	k1gInPc1
se	se	k3xPyFc4
jako	jako	k9
písmo	písmo	k1gNnSc4
pro	pro	k7c4
sinické	sinický	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
používají	používat	k5eAaImIp3nP
po	po	k7c6
tisíce	tisíc	k4xCgInSc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňují	umožňovat	k5eAaImIp3nP
mluvčím	mluvčí	k1gMnPc3
vzájemně	vzájemně	k6eAd1
nesrozumitelných	srozumitelný	k2eNgInPc2d1
čínských	čínský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
komunikovat	komunikovat	k5eAaImF
mezi	mezi	k7c7
sebou	se	k3xPyFc7
prostřednictvím	prostřednictvím	k7c2
psaní	psaní	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
vláda	vláda	k1gFnSc1
představila	představit	k5eAaPmAgFnS
zjednodušené	zjednodušený	k2eAgInPc4d1
znaky	znak	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
v	v	k7c6
pevninské	pevninský	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
nahradily	nahradit	k5eAaPmAgInP
starší	starý	k2eAgInPc1d2
tradiční	tradiční	k2eAgInPc1d1
znaky	znak	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínské	čínský	k2eAgInPc1d1
znaky	znak	k1gInPc1
jsou	být	k5eAaImIp3nP
také	také	k9
romanizované	romanizovaný	k2eAgInPc1d1
ve	v	k7c6
formě	forma	k1gFnSc6
pchin-jinu	pchin-jin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Vláda	vláda	k1gFnSc1
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
oficiálně	oficiálně	k6eAd1
zastává	zastávat	k5eAaImIp3nS
státní	státní	k2eAgInSc1d1
ateismus	ateismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
za	za	k7c7
tímto	tento	k3xDgInSc7
účelem	účel	k1gInSc7
vede	vést	k5eAaImIp3nS
antireligistické	antireligistický	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
náboženské	náboženský	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
v	v	k7c6
zemi	zem	k1gFnSc6
dohlíží	dohlížet	k5eAaImIp3nS
státní	státní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
pro	pro	k7c4
náboženské	náboženský	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
Náboženská	náboženský	k2eAgFnSc1d1
svoboda	svoboda	k1gFnSc1
je	být	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
zaručena	zaručen	k2eAgFnSc1d1
čínskou	čínský	k2eAgFnSc7d1
ústavou	ústava	k1gFnSc7
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
náboženské	náboženský	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nemají	mít	k5eNaImIp3nP
oficiální	oficiální	k2eAgNnSc4d1
schválení	schválení	k1gNnSc4
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
předmětem	předmět	k1gInSc7
státní	státní	k2eAgFnSc2d1
perzekuce	perzekuce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
tisíciletí	tisíciletí	k1gNnSc2
byla	být	k5eAaImAgFnS
čínská	čínský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
ovlivněna	ovlivnit	k5eAaPmNgFnS
různými	různý	k2eAgFnPc7d1
náboženskými	náboženský	k2eAgFnPc7d1
hnutími	hnutí	k1gNnPc7
<g/>
.	.	kIx.
“	“	k?
<g/>
Tři	tři	k4xCgInPc4
<g />
.	.	kIx.
</s>
<s hack="1">
učení	učení	k1gNnSc6
<g/>
”	”	k?
<g/>
,	,	kIx,
včetně	včetně	k7c2
konfuciánství	konfuciánství	k1gNnSc2
<g/>
,	,	kIx,
taoismu	taoismus	k1gInSc2
a	a	k8xC
buddhismu	buddhismus	k1gInSc2
(	(	kIx(
<g/>
čínský	čínský	k2eAgInSc1d1
buddhismus	buddhismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
měly	mít	k5eAaImAgFnP
v	v	k7c6
historii	historie	k1gFnSc6
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
ve	v	k7c6
formování	formování	k1gNnSc6
čínské	čínský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
obohatily	obohatit	k5eAaPmAgFnP
teologický	teologický	k2eAgInSc4d1
a	a	k8xC
duchovní	duchovní	k2eAgInSc4d1
rámec	rámec	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
sahá	sahat	k5eAaImIp3nS
do	do	k7c2
období	období	k1gNnSc2
Šangu	Šang	k1gInSc2
a	a	k8xC
dynastie	dynastie	k1gFnSc2
Čou	Čou	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Chanové	Chanová	k1gFnPc1
v	v	k7c6
taoistickém	taoistický	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
</s>
<s>
Ujgurové	Ujgur	k1gMnPc1
</s>
<s>
Tibeťané	Tibeťan	k1gMnPc1
</s>
<s>
Miaové	Miaové	k2eAgFnSc1d1
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
zemí	zem	k1gFnSc7
s	s	k7c7
kulturní	kulturní	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
starou	stará	k1gFnSc4
několik	několik	k4yIc4
tisíc	tisíc	k4xCgInSc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
Současná	současný	k2eAgFnSc1d1
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
zachovává	zachovávat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
kulturní	kulturní	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
v	v	k7c6
oblasti	oblast	k1gFnSc6
výtvarného	výtvarný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k9
tradici	tradice	k1gFnSc4
literární	literární	k2eAgFnSc4d1
a	a	k8xC
hudební	hudební	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
však	však	k9
otevřená	otevřený	k2eAgFnSc1d1
kulturním	kulturní	k2eAgNnPc3d1
vlivům	vliv	k1gInPc3
okolního	okolní	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Čínská	čínský	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
.	.	kIx.
<g/>
Básník	básník	k1gMnSc1
Li	li	k8xS
Po	po	k7c6
</s>
<s>
Za	za	k7c4
první	první	k4xOgFnSc4
velkou	velký	k2eAgFnSc4d1
básnickou	básnický	k2eAgFnSc4d1
osobnost	osobnost	k1gFnSc4
Číny	Čína	k1gFnSc2
bývá	bývat	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
Čchü	Čchü	k1gFnSc7
Jüan	jüan	k1gInSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Tzv.	tzv.	kA
popisné	popisný	k2eAgFnPc4d1
básně	báseň	k1gFnPc4
skládal	skládat	k5eAaImAgMnS
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Pan	Pan	k1gMnSc1
Ku	k	k7c3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
básník	básník	k1gMnSc1
Tchao	Tchao	k1gMnSc1
Jüan-ming	Jüan-ming	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cchao	Cchao	k1gMnSc1
Cchao	Cchao	k1gMnSc1
založil	založit	k5eAaPmAgMnS
nový	nový	k2eAgInSc4d1
styl	styl	k1gInSc4
poezie	poezie	k1gFnSc2
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
ťien-an	ťien-an	k1gInSc1
<g/>
,	,	kIx,
významným	významný	k2eAgMnSc7d1
básníkem	básník	k1gMnSc7
byl	být	k5eAaImAgMnS
i	i	k9
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Cchao	Cchao	k6eAd1
Pchi	Pch	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zazářil	zazářit	k5eAaPmAgMnS
Tu	ten	k3xDgFnSc4
Fu	fu	k0
a	a	k8xC
krátce	krátce	k6eAd1
po	po	k7c6
něm	on	k3xPp3gNnSc6
Li	li	k8xS
Po	po	k7c6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejpopulárnějším	populární	k2eAgMnSc7d3
čínským	čínský	k2eAgMnSc7d1
autorem	autor	k1gMnSc7
až	až	k8xS
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Li	li	k8xS
Poa	Poa	k1gMnPc4
a	a	k8xC
Tu	ten	k3xDgFnSc4
Fua	Fua	k1gFnSc4
navazoval	navazovat	k5eAaImAgInS
Po	po	k7c6
Ťü-i	Ťü-	k1gInSc6
<g/>
,	,	kIx,
byť	byť	k8xS
jim	on	k3xPp3gMnPc3
vyčítal	vyčítat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
“	“	k?
<g/>
utíkaly	utíkat	k5eAaImAgFnP
do	do	k7c2
hor	hora	k1gFnPc2
a	a	k8xC
k	k	k7c3
řekám	řeka	k1gFnPc3
<g/>
”	”	k?
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
je	být	k5eAaImIp3nS
často	často	k6eAd1
označováno	označovat	k5eAaImNgNnS
za	za	k7c4
zlatý	zlatý	k2eAgInSc4d1
věk	věk	k1gInSc4
čínské	čínský	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
jeho	jeho	k3xOp3gMnPc3
pozdním	pozdní	k2eAgMnPc3d1
představitelům	představitel	k1gMnPc3
patřil	patřit	k5eAaImAgInS
Tu	tu	k6eAd1
Mu	on	k3xPp3gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
proslula	proslout	k5eAaPmAgFnS
básnířka	básnířka	k1gFnSc1
Li	li	k8xS
Čching-čao	Čching-čao	k1gNnSc1
<g/>
,	,	kIx,
z	z	k7c2
jejíhož	jejíž	k3xOyRp3gNnSc2
díla	dílo	k1gNnSc2
se	se	k3xPyFc4
však	však	k9
zachovala	zachovat	k5eAaPmAgFnS
jen	jen	k9
jediná	jediný	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
<g/>
,	,	kIx,
Píseň	píseň	k1gFnSc1
průzračného	průzračný	k2eAgInSc2d1
nefritu	nefrit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
je	být	k5eAaImIp3nS
érou	éra	k1gFnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
rodí	rodit	k5eAaImIp3nS
čínská	čínský	k2eAgFnSc1d1
próza	próza	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
její	její	k3xOp3gFnPc4
zakladatelské	zakladatelský	k2eAgFnPc4d1
postavy	postava	k1gFnPc4
jsou	být	k5eAaImIp3nP
označováni	označován	k2eAgMnPc1d1
Chan	Chan	k1gMnSc1
Jü	Jü	k1gFnSc2
a	a	k8xC
Liou	Lious	k1gInSc2
Cung-jüan	Cung-jüany	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgMnSc7d1
byl	být	k5eAaImAgMnS
však	však	k9
již	již	k6eAd1
cestopis	cestopis	k1gInSc1
Zápisky	zápiska	k1gFnSc2
o	o	k7c6
buddhistických	buddhistický	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
autora	autor	k1gMnSc2
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Fa-siena	Fa-sieno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
prosluly	proslout	k5eAaPmAgInP
svými	svůj	k3xOyFgMnPc7
eseji	esej	k1gInSc6
Su	Su	k?
Š	Š	kA
<g/>
’	’	k?
či	či	k8xC
Ou-jang	Ou-jang	k1gInSc1
Siou	Sious	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběhy	příběh	k1gInPc7
od	od	k7c2
jezerního	jezerní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
z	z	k7c2
konce	konec	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
napsali	napsat	k5eAaPmAgMnP,k5eAaBmAgMnP
Š	Š	kA
<g/>
’	’	k?
Naj-an	Naj-an	k1gMnSc1
a	a	k8xC
Luo	Luo	k1gMnSc1
Kuan-čung	Kuan-čung	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
prvním	první	k4xOgMnSc6
z	z	k7c2
tzv.	tzv.	kA
čtyř	čtyři	k4xCgInPc2
klasických	klasický	k2eAgInPc2d1
čínských	čínský	k2eAgInPc2d1
románů	román	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
jsou	být	k5eAaImIp3nP
Příběhy	příběh	k1gInPc1
Tří	tři	k4xCgFnPc2
říší	říš	k1gFnPc2
Luo	Luo	k1gMnSc2
Kuan-čunga	Kuan-čung	k1gMnSc2
<g/>
,	,	kIx,
rovněž	rovněž	k9
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Süan-cang	Süan-cang	k1gMnSc1
proslul	proslout	k5eAaPmAgMnS
svým	svůj	k3xOyFgInSc7
cestopisem	cestopis	k1gInSc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
cesta	cesta	k1gFnSc1
do	do	k7c2
Indie	Indie	k1gFnSc2
později	pozdě	k6eAd2
posloužila	posloužit	k5eAaPmAgFnS
jako	jako	k9
inspirace	inspirace	k1gFnSc1
pro	pro	k7c4
román	román	k1gInSc4
Putování	putování	k1gNnSc2
na	na	k7c4
západ	západ	k1gInSc4
(	(	kIx(
<g/>
třetí	třetí	k4xOgInSc1
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
klasických	klasický	k2eAgInPc2d1
románů	román	k1gInPc2
<g/>
)	)	kIx)
spisovatele	spisovatel	k1gMnSc2
Wu	Wu	k1gMnSc2
Čcheng-ena	Čcheng-en	k1gMnSc2
<g/>
,	,	kIx,
nejvýznamnějšího	významný	k2eAgMnSc4d3
autora	autor	k1gMnSc4
mingského	mingského	k2eAgNnSc1d1
období	období	k1gNnSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
průkopníkům	průkopník	k1gMnPc3
moderního	moderní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
patřil	patřit	k5eAaImAgMnS
Li	li	k9
Jü	Jü	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
Cchao	Cchao	k1gMnSc1
Süe-čchin	Süe-čchin	k1gMnSc1
románem	román	k1gInSc7
Sen	sen	k1gInSc1
v	v	k7c6
červeném	červený	k2eAgInSc6d1
domě	dům	k1gInSc6
<g/>
,	,	kIx,
posledním	poslední	k2eAgMnSc7d1
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
klasických	klasický	k2eAgInPc2d1
románů	román	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
prvním	první	k4xOgFnPc3
na	na	k7c4
západ	západ	k1gInSc1
proniknuvším	proniknuvší	k2eAgMnPc3d1
autorům	autor	k1gMnPc3
patřil	patřit	k5eAaImAgInS
Lin	Lina	k1gFnPc2
Jü-tchang	Jü-tchanga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
jedny	jeden	k4xCgFnPc4
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
čínských	čínský	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jsou	být	k5eAaImIp3nP
označováni	označován	k2eAgMnPc1d1
Lu	Lu	k1gMnPc1
Sün	Sün	k1gMnSc1
a	a	k8xC
Lao	Lao	k1gMnSc1
Še	Še	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
známého	známý	k2eAgInSc2d1
románu	román	k1gInSc2
Rikša	rikša	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Románem	Román	k1gMnSc7
Rodina	rodina	k1gFnSc1
zaujal	zaujmout	k5eAaPmAgInS
Pa	Pa	kA
Ťin	Ťin	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Feminismus	feminismus	k1gInSc1
do	do	k7c2
čínské	čínský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
vnesla	vnést	k5eAaPmAgFnS
Čchiou	Čchiý	k2eAgFnSc4d1
Ťin	Ťin	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
komunistickým	komunistický	k2eAgMnPc3d1
autorům	autor	k1gMnPc3
patřil	patřit	k5eAaImAgInS
Kuo	Kuo	k1gMnSc1
Mo-žo	Mo-žo	k1gMnSc1
či	či	k8xC
Mao	Mao	k1gMnSc1
Tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kao	Kao	k1gFnPc2
Sing-ťien	Sing-ťina	k1gFnPc2
obdržel	obdržet	k5eAaPmAgInS
roku	rok	k1gInSc2
2000	#num#	k4
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
literaturu	literatura	k1gFnSc4
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Mo	Mo	k1gFnSc4
Jen	jen	k8xS
stejné	stejný	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
převzal	převzít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1
umění	umění	k1gNnSc1
</s>
<s>
Terakotová	terakotový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
</s>
<s>
Socha	Socha	k1gMnSc1
Hlava	hlava	k1gFnSc1
psa	pes	k1gMnSc2
od	od	k7c2
Aj	aj	kA
Wej-weje	Wej-wej	k1gInSc2
</s>
<s>
Proslulým	proslulý	k2eAgInSc7d1
starověkým	starověký	k2eAgInSc7d1
artefaktem	artefakt	k1gInSc7
je	být	k5eAaImIp3nS
Terakotová	terakotový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
soubor	soubor	k1gInSc1
6000	#num#	k4
soch	socha	k1gFnPc2
zakopaných	zakopaný	k2eAgFnPc2d1
v	v	k7c6
pohřebním	pohřební	k2eAgInSc6d1
komplexu	komplex	k1gInSc6
poblíž	poblíž	k7c2
čínského	čínský	k2eAgNnSc2d1
města	město	k1gNnSc2
Si-an	Si-an	k1gMnSc1
<g/>
,	,	kIx,
objevený	objevený	k2eAgMnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sochy	socha	k1gFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
ve	v	k7c6
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
pro	pro	k7c4
hrobku	hrobka	k1gFnSc4
sjednotitele	sjednotitel	k1gMnSc2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
císaře	císař	k1gMnSc4
Čchin	Čchina	k1gFnPc2
Š	Š	kA
<g/>
’	’	k?
<g/>
-chuang-tiho	-chuang-ti	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
Hrobka	hrobka	k1gFnSc1
a	a	k8xC
armáda	armáda	k1gFnSc1
byly	být	k5eAaImAgFnP
zapsány	zapsat	k5eAaPmNgInP
na	na	k7c4
seznam	seznam	k1gInSc4
Světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
další	další	k2eAgNnSc1d1
starověké	starověký	k2eAgNnSc1d1
sochařské	sochařský	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
Lešanský	Lešanský	k2eAgMnSc1d1
Buddha	Buddha	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
též	též	k9
pod	pod	k7c7
svým	svůj	k3xOyFgInSc7
čínským	čínský	k2eAgInSc7d1
názvem	název	k1gInSc7
Ta-fo	Ta-fo	k6eAd1
(	(	kIx(
<g/>
Velký	velký	k2eAgMnSc1d1
Buddha	Buddha	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
největší	veliký	k2eAgFnSc1d3
socha	socha	k1gFnSc1
Buddhy	Buddha	k1gMnSc2
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
71	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stojí	stát	k5eAaImIp3nS
nedaleko	nedaleko	k7c2
města	město	k1gNnSc2
Le-šan	Le-šany	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
Staré	Staré	k2eAgNnSc1d1
buddhistické	buddhistický	k2eAgNnSc1d1
umění	umění	k1gNnSc1
bylo	být	k5eAaImAgNnS
koncentrováno	koncentrovat	k5eAaBmNgNnS
často	často	k6eAd1
do	do	k7c2
jeskyň	jeskyně	k1gFnPc2
<g/>
,	,	kIx,
skalní	skalní	k2eAgFnPc4d1
malby	malba	k1gFnPc4
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
v	v	k7c6
jeskyni	jeskyně	k1gFnSc6
Mo-kao	Mo-kao	k6eAd1
(	(	kIx(
<g/>
též	též	k9
Jeskyni	jeskyně	k1gFnSc4
tisíce	tisíc	k4xCgInSc2
buddhů	buddha	k1gMnPc2
<g/>
)	)	kIx)
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Jün-kang	Jün-kang	k1gMnSc1
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
reliéfy	reliéf	k1gInPc4
v	v	k7c4
jeskyni	jeskyně	k1gFnSc4
Ta-cu	Ta-cus	k1gInSc2
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
sochy	socha	k1gFnPc1
v	v	k7c6
jeskyni	jeskyně	k1gFnSc6
Lung-men	Lung-men	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
nejstarším	starý	k2eAgFnPc3d3
patří	patřit	k5eAaImIp3nS
Chuašanské	Chuašanský	k2eAgFnPc4d1
skalní	skalní	k2eAgFnPc4d1
malby	malba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klasikem	klasik	k1gMnSc7
čínské	čínský	k2eAgFnSc2d1
kaligrafie	kaligrafie	k1gFnSc2
a	a	k8xC
malířství	malířství	k1gNnSc2
byl	být	k5eAaImAgInS
Mi	já	k3xPp1nSc3
Fu	fu	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgInPc1
svitkové	svitkový	k2eAgInPc1d1
obrazy	obraz	k1gInPc1
se	se	k3xPyFc4
dochovaly	dochovat	k5eAaPmAgInP
z	z	k7c2
díla	dílo	k1gNnSc2
autora	autor	k1gMnSc2
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Ku	k	k7c3
Kchaj-č	Kchaj-č	k1gMnSc1
<g/>
’	’	k?
<g/>
e.	e.	k?
Zakladatelem	zakladatel	k1gMnSc7
čínského	čínský	k2eAgNnSc2d1
krajinářství	krajinářství	k1gNnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
Wang	Wanga	k1gFnPc2
Wej	Wej	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
se	s	k7c7
známým	známý	k2eAgMnSc7d1
malířem	malíř	k1gMnSc7
stal	stát	k5eAaPmAgMnS
i	i	k9
jeden	jeden	k4xCgMnSc1
z	z	k7c2
císařů	císař	k1gMnPc2
<g/>
,	,	kIx,
Chuej-cung	Chuej-cung	k1gMnSc1
(	(	kIx(
<g/>
Sung	Sung	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
od	od	k7c2
tradičních	tradiční	k2eAgInPc2d1
vzorů	vzor	k1gInPc2
počal	počnout	k5eAaPmAgMnS
vzdalovat	vzdalovat	k5eAaImF
Š	Š	kA
<g/>
’	’	k?
<g/>
-tchao	-tchao	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
moderním	moderní	k2eAgMnPc3d1
malířům	malíř	k1gMnPc3
patří	patřit	k5eAaImIp3nS
Čchi	Čch	k1gInSc3
Paj-š	Paj-š	k1gInSc1
<g/>
’	’	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejuznávanějším	uznávaný	k2eAgMnPc3d3
současným	současný	k2eAgMnPc3d1
výtvarným	výtvarný	k2eAgMnPc3d1
umělcům	umělec	k1gMnPc3
Aj	aj	kA
Wej-wej	Wej-wej	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Architekt	architekt	k1gMnSc1
Wang	Wang	k1gMnSc1
Šu	Šu	k1gMnSc1
je	být	k5eAaImIp3nS
nositel	nositel	k1gMnSc1
prestižní	prestižní	k2eAgFnSc2d1
architektonické	architektonický	k2eAgFnSc2d1
Pritzkerovy	Pritzkerův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
čínského	čínský	k2eAgInSc2d1
původu	původ	k1gInSc2
je	být	k5eAaImIp3nS
i	i	k9
jiný	jiný	k2eAgMnSc1d1
držitel	držitel	k1gMnSc1
této	tento	k3xDgFnSc2
ceny	cena	k1gFnSc2
<g/>
,	,	kIx,
Američan	Američan	k1gMnSc1
I.	I.	kA
M.	M.	kA
Pei	Pei	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
známé	známý	k2eAgFnSc2d1
Skleněné	skleněný	k2eAgFnSc2d1
pyramidy	pyramida	k1gFnSc2
v	v	k7c6
muzeu	muzeum	k1gNnSc6
Louvre	Louvre	k1gInSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Čínská	čínský	k2eAgFnSc1d1
kinematografie	kinematografie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
čínským	čínský	k2eAgMnPc3d1
filmařům	filmař	k1gMnPc3
patří	patřit	k5eAaImIp3nP
režisér	režisér	k1gMnSc1
Čang	Čang	k1gMnSc1
I-mou	I-ma	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
mj.	mj.	kA
hlavním	hlavní	k2eAgMnSc7d1
režisérem	režisér	k1gMnSc7
úvodního	úvodní	k2eAgInSc2d1
a	a	k8xC
závěrečného	závěrečný	k2eAgInSc2d1
ceremoniálu	ceremoniál	k1gInSc2
Letních	letní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
v	v	k7c6
Pekingu	Peking	k1gInSc6
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
jeho	jeho	k3xOp3gMnPc3
generačním	generační	k2eAgMnPc3d1
souputníkům	souputník	k1gMnPc3
(	(	kIx(
<g/>
tzv.	tzv.	kA
pátá	pátý	k4xOgFnSc1
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
Čchen	Čchen	k2eAgInSc4d1
Kchaj-ke	Kchaj-ke	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
se	se	k3xPyFc4
prosadili	prosadit	k5eAaPmAgMnP
filmový	filmový	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
John	John	k1gMnSc1
Woo	Woo	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
herečky	herečka	k1gFnPc1
Joan	Joana	k1gFnPc2
Chen	Chena	k1gFnPc2
<g/>
,	,	kIx,
Li	li	k8xS
Ping-ping	Ping-ping	k1gInSc1
nebo	nebo	k8xC
Bai	Bai	k1gMnSc1
Ling	Ling	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Číně	Čína	k1gFnSc6
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejpopulárnějším	populární	k2eAgFnPc3d3
herečkám	herečka	k1gFnPc3
Čao	čao	k0
Wej	Wej	k1gMnSc5
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
proslavila	proslavit	k5eAaPmAgFnS
ve	v	k7c6
snímku	snímek	k1gInSc6
Krvavé	krvavý	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
Čang	Čang	k1gMnSc1
C	C	kA
<g/>
’	’	k?
<g/>
-i	-i	k?
známá	známá	k1gFnSc1
z	z	k7c2
filmu	film	k1gInSc2
Gejša	gejša	k1gFnSc1
či	či	k8xC
Fan	Fana	k1gFnPc2
Ping-ping	Ping-ping	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velmi	velmi	k6eAd1
rozvinutý	rozvinutý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
filmový	filmový	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
můžeme	moct	k5eAaImIp1nP
rovněž	rovněž	k9
přičítat	přičítat	k5eAaImF
k	k	k7c3
ČLR	ČLR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslulé	proslulý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
zejména	zejména	k9
hongkongské	hongkongský	k2eAgInPc1d1
akční	akční	k2eAgInPc1d1
filmy	film	k1gInPc1
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc7d3
hvězdou	hvězda	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
tento	tento	k3xDgMnSc1
průmysl	průmysl	k1gInSc4
zplodil	zplodit	k5eAaPmAgInS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
patrně	patrně	k6eAd1
Jackie	Jackie	k1gFnSc1
Chan	Chana	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gFnPc6
stopách	stopa	k1gFnPc6
šel	jít	k5eAaImAgInS
Jet	jet	k5eAaImNgInS
Li	li	k9
(	(	kIx(
<g/>
rodák	rodák	k1gMnSc1
z	z	k7c2
Pekingu	Peking	k1gInSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
občan	občan	k1gMnSc1
Singapuru	Singapur	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čang	Čang	k1gInSc1
C	C	kA
<g/>
’	’	k?
<g/>
-lin	-lin	k1gInSc1
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc1
Číňanka	Číňanka	k1gFnSc1
uspěla	uspět	k5eAaPmAgFnS
v	v	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
krásy	krása	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
roku	rok	k1gInSc2
2007	#num#	k4
stala	stát	k5eAaPmAgFnS
Miss	miss	k1gFnSc1
World	World	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
a	a	k8xC
hudba	hudba	k1gFnSc1
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
</s>
<s>
V	v	k7c6
několika	několik	k4yIc6
velkých	velký	k2eAgNnPc6d1
čínských	čínský	k2eAgNnPc6d1
městech	město	k1gNnPc6
byla	být	k5eAaImAgNnP
postavena	postavit	k5eAaPmNgNnP
velmi	velmi	k6eAd1
moderní	moderní	k2eAgNnPc1d1
operní	operní	k2eAgNnPc1d1
divadla	divadlo	k1gNnPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgNnPc6,k3yQgNnPc6,k3yRgNnPc6
jsou	být	k5eAaImIp3nP
uváděny	uváděn	k2eAgFnPc4d1
jak	jak	k8xS,k8xC
tradiční	tradiční	k2eAgFnPc4d1
čínské	čínský	k2eAgFnPc4d1
opery	opera	k1gFnPc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
opery	opera	k1gFnSc2
z	z	k7c2
běžného	běžný	k2eAgInSc2d1
světového	světový	k2eAgInSc2d1
repertoáru	repertoár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
evropské	evropský	k2eAgFnSc2d1
opery	opera	k1gFnSc2
vznikla	vzniknout	k5eAaPmAgFnS
čínská	čínský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
v	v	k7c6
lidových	lidový	k2eAgFnPc6d1
vrstvách	vrstva	k1gFnPc6
a	a	k8xC
uměním	umění	k1gNnSc7
nejvyšší	vysoký	k2eAgFnSc2d3
společnosti	společnost	k1gFnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
teprve	teprve	k6eAd1
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
vrcholnou	vrcholný	k2eAgFnSc4d1
éru	éra	k1gFnSc4
bývá	bývat	k5eAaImIp3nS
označováno	označovat	k5eAaImNgNnS
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
okolo	okolo	k7c2
stovky	stovka	k1gFnSc2
žánrů	žánr	k1gInPc2
čínské	čínský	k2eAgFnSc2d1
opery	opera	k1gFnSc2
<g/>
,	,	kIx,
odlišených	odlišený	k2eAgFnPc2d1
především	především	k9
regionálně	regionálně	k6eAd1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
nejznámější	známý	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
Pekingská	pekingský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děj	děj	k1gInSc1
představení	představení	k1gNnSc2
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
vždy	vždy	k6eAd1
v	v	k7c6
minulosti	minulost	k1gFnSc6
<g/>
,	,	kIx,
před	před	k7c7
17	#num#	k4
<g/>
.	.	kIx.
stoletím	století	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
zpěvu	zpěv	k1gInSc2
a	a	k8xC
tance	tanec	k1gInSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
často	často	k6eAd1
i	i	k9
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
<g/>
,	,	kIx,
akrobacii	akrobacie	k1gFnSc6
<g/>
,	,	kIx,
typické	typický	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
výrazné	výrazný	k2eAgFnPc1d1
masky	maska	k1gFnPc1
a	a	k8xC
líčení	líčení	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
Kulturní	kulturní	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
byla	být	k5eAaImAgFnS
čínská	čínský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
zakázána	zakázat	k5eAaPmNgFnS
<g/>
,	,	kIx,
od	od	k7c2
konce	konec	k1gInSc2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
Číňané	Číňan	k1gMnPc1
snaží	snažit	k5eAaImIp3nP
tuto	tento	k3xDgFnSc4
tradici	tradice	k1gFnSc4
oživit	oživit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
zemi	zem	k1gFnSc6
působí	působit	k5eAaImIp3nS
několik	několik	k4yIc1
symfonických	symfonický	k2eAgInPc2d1
orchestrů	orchestr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známý	známý	k1gMnSc1
je	být	k5eAaImIp3nS
Šanghajský	šanghajský	k2eAgInSc1d1
symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
výhradně	výhradně	k6eAd1
z	z	k7c2
čínských	čínský	k2eAgMnPc2d1
hudebníků	hudebník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
vysílaly	vysílat	k5eAaImAgFnP
evropské	evropský	k2eAgFnPc1d1
televizní	televizní	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
(	(	kIx(
<g/>
např.	např.	kA
Arte	Arte	k1gFnSc1
<g/>
)	)	kIx)
vystoupení	vystoupení	k1gNnSc1
tohoto	tento	k3xDgInSc2
orchestru	orchestr	k1gInSc2
–	–	k?
pod	pod	k7c7
vedením	vedení	k1gNnSc7
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
šéfdirigenta	šéfdirigent	k1gMnSc2
Ju	ju	k0
Longa	Longa	k1gFnSc1
–	–	k?
v	v	k7c6
prostorách	prostora	k1gFnPc6
Zakázaného	zakázaný	k2eAgNnSc2d1
města	město	k1gNnSc2
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
programu	program	k1gInSc6
byl	být	k5eAaImAgInS
mj.	mj.	kA
2	#num#	k4
<g/>
.	.	kIx.
klavírní	klavírní	k2eAgInSc1d1
koncert	koncert	k1gInSc1
Sergeje	Sergej	k1gMnSc2
Rachmaninova	Rachmaninův	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sólistou	sólista	k1gMnSc7
byl	být	k5eAaImAgMnS
ruský	ruský	k2eAgMnSc1d1
pianista	pianista	k1gMnSc1
Daniil	Daniil	k1gMnSc1
Trifonov	Trifonovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínským	čínský	k2eAgMnSc7d1
občanem	občan	k1gMnSc7
je	být	k5eAaImIp3nS
jiný	jiný	k2eAgMnSc1d1
světoznámý	světoznámý	k2eAgMnSc1d1
klavírista	klavírista	k1gMnSc1
<g/>
,	,	kIx,
Lang	Lang	k1gMnSc1
Lang	Lang	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
hudebním	hudební	k2eAgMnPc3d1
skladatelům	skladatel	k1gMnPc3
současnosti	současnost	k1gFnSc2
patří	patřit	k5eAaImIp3nS
Tchan	Tchan	k1gInSc4
Tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
soudobou	soudobý	k2eAgFnSc4d1
vážnou	vážný	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
více	hodně	k6eAd2
proslul	proslout	k5eAaPmAgMnS
hudbou	hudba	k1gFnSc7
filmovou	filmový	k2eAgFnSc7d1
(	(	kIx(
<g/>
Tygr	tygr	k1gMnSc1
a	a	k8xC
drak	drak	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yo-Yo	Yo-Yo	k1gMnSc1
Ma	Ma	k1gMnSc1
je	být	k5eAaImIp3nS
významným	významný	k2eAgMnSc7d1
violoncelistou	violoncelista	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legendární	legendární	k2eAgFnSc7d1
čínskou	čínský	k2eAgFnSc7d1
zpěvačkou	zpěvačka	k1gFnSc7
byla	být	k5eAaImAgFnS
Čou	Čou	k1gFnSc1
Süan	Süana	k1gFnPc2
<g/>
,	,	kIx,
k	k	k7c3
současným	současný	k2eAgFnPc3d1
popovým	popový	k2eAgFnPc3d1
hvězdám	hvězda	k1gFnPc3
(	(	kIx(
<g/>
hovoří	hovořit	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
tzv.	tzv.	kA
Cantopopu	Cantopop	k1gInSc6
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
Leslie	Leslie	k1gFnSc1
Cheung	Cheunga	k1gFnPc2
nebo	nebo	k8xC
Faye	Faye	k1gFnPc2
Wong	Wonga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Památky	památka	k1gFnPc1
</s>
<s>
Zakázané	zakázaný	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
čínská	čínský	k2eAgFnSc1d1
zeď	zeď	k1gFnSc1
</s>
<s>
Velké	velký	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
v	v	k7c6
Pekingu	Peking	k1gInSc6
</s>
<s>
Pekingský	pekingský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
</s>
<s>
Největším	veliký	k2eAgInSc7d3
architektonickým	architektonický	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
Číny	Čína	k1gFnSc2
je	být	k5eAaImIp3nS
Velká	velký	k2eAgFnSc1d1
čínská	čínský	k2eAgFnSc1d1
zeď	zeď	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
hlasováním	hlasování	k1gNnSc7
sto	sto	k4xCgNnSc4
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
zařazena	zařadit	k5eAaPmNgFnS
dokonce	dokonce	k9
mezi	mezi	k7c4
Nových	Nových	k2eAgInPc2d1
sedm	sedm	k4xCc4
divů	div	k1gInPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
Zeď	zeď	k1gFnSc1
byla	být	k5eAaImAgFnS
stavěna	stavit	k5eAaImNgFnS
za	za	k7c4
dynastie	dynastie	k1gFnPc4
Ming	Minga	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
konce	konec	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
začátku	začátek	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
účelem	účel	k1gInSc7
bylo	být	k5eAaImAgNnS
chránit	chránit	k5eAaImF
Čínu	Čína	k1gFnSc4
před	před	k7c7
mongolskými	mongolský	k2eAgInPc7d1
nájezdy	nájezd	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
neodmyslitelným	odmyslitelný	k2eNgInSc7d1
symbolem	symbol	k1gInSc7
Číny	Čína	k1gFnSc2
a	a	k8xC
vůbec	vůbec	k9
nejnavštěvovanější	navštěvovaný	k2eAgFnSc7d3
čínskou	čínský	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
je	být	k5eAaImIp3nS
Zakázané	zakázaný	k2eAgNnSc1d1
město	město	k1gNnSc1
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
,	,	kIx,
komplex	komplex	k1gInSc1
980	#num#	k4
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
sídlili	sídlit	k5eAaImAgMnP
téměř	téměř	k6eAd1
500	#num#	k4
let	léto	k1gNnPc2
čínští	čínský	k2eAgMnPc1d1
císaři	císař	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
města	město	k1gNnSc2
se	se	k3xPyFc4
vstupuje	vstupovat	k5eAaImIp3nS
Bránou	brána	k1gFnSc7
Nebeského	nebeský	k2eAgInSc2d1
klidu	klid	k1gInSc2
(	(	kIx(
<g/>
Tchien-an-men	Tchien-an-men	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dominantou	dominanta	k1gFnSc7
Náměstí	náměstí	k1gNnSc2
Nebeského	nebeský	k2eAgInSc2d1
klidu	klid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
západní	západní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Velký	velký	k2eAgInSc1d1
sál	sál	k1gInSc1
lidu	lid	k1gInSc2
postavený	postavený	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
sjezdy	sjezd	k1gInPc1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
východní	východní	k2eAgFnSc4d1
Čínské	čínský	k2eAgNnSc4d1
národní	národní	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
<g/>
,	,	kIx,
postavené	postavený	k2eAgInPc1d1
rovněž	rovněž	k9
roku	rok	k1gInSc2
1959	#num#	k4
(	(	kIx(
<g/>
k	k	k7c3
10	#num#	k4
letům	let	k1gInPc3
komunistické	komunistický	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
císaři	císař	k1gMnSc3
dynastie	dynastie	k1gFnSc2
Čching	Čching	k1gInSc4
obývali	obývat	k5eAaImAgMnP
Mukdenský	Mukdenský	k2eAgInSc4d1
palác	palác	k1gInSc4
ve	v	k7c6
městě	město	k1gNnSc6
Šen-jang	Šen-janga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letní	letní	k2eAgInSc4d1
sídlo	sídlo	k1gNnSc1
měli	mít	k5eAaImAgMnP
v	v	k7c6
Čcheng-te	Čcheng-t	k1gMnSc5
<g/>
,	,	kIx,
později	pozdě	k6eAd2
si	se	k3xPyFc3
postavili	postavit	k5eAaPmAgMnP
Letní	letní	k2eAgInSc4d1
palác	palác	k1gInSc4
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
Starého	Starého	k2eAgInSc2d1
letního	letní	k2eAgInSc2d1
paláce	palác	k1gInSc2
již	již	k9
zbyly	zbýt	k5eAaPmAgInP
jen	jen	k9
ruiny	ruina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
seznam	seznam	k1gInSc4
Světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
byly	být	k5eAaImAgInP
zapsány	zapsat	k5eAaPmNgInP
i	i	k8xC
císařské	císařský	k2eAgInPc1d1
hrobky	hrobek	k1gInPc1
dvou	dva	k4xCgFnPc2
nejvýznamnějších	významný	k2eAgFnPc2d3
dynastií	dynastie	k1gFnPc2
<g/>
,	,	kIx,
Čching	Čching	k1gInSc1
a	a	k8xC
Ming	Ming	k1gInSc1
(	(	kIx(
<g/>
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
k	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
patří	patřit	k5eAaImIp3nS
Ming-siao-ling	Ming-siao-ling	k1gInSc4
v	v	k7c6
Nankingu	Nanking	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
opevněné	opevněný	k2eAgFnSc2d1
věže	věž	k1gFnSc2
Tiao-lou	Tiao-lá	k1gFnSc4
v	v	k7c6
Kchaj-pchingu	Kchaj-pching	k1gInSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
kruhová	kruhový	k2eAgFnSc1d1
hliněná	hliněný	k2eAgFnSc1d1
obydlí	obydlet	k5eAaPmIp3nS
Tchu-lou	Tchu-lá	k1gFnSc4
ve	v	k7c6
Fu-ťienu	Fu-ťien	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejpamátnějším	památní	k2eAgNnPc3d3
místům	místo	k1gNnPc3
konfucianismu	konfucianismus	k1gInSc2
patří	patřit	k5eAaImIp3nS
Konfuciův	Konfuciův	k2eAgInSc1d1
chrám	chrám	k1gInSc1
v	v	k7c6
Čchü-fu	Čchü-f	k1gInSc6
<g/>
,	,	kIx,
Konfuciově	Konfuciův	k2eAgNnSc6d1
rodišti	rodiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
taoisty	taoista	k1gMnPc4
je	být	k5eAaImIp3nS
nejvzácnějším	vzácný	k2eAgInSc7d3
Chrám	chrám	k1gInSc1
nebes	nebesa	k1gNnPc2
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
taoistických	taoistický	k2eAgInPc2d1
chrámů	chrám	k1gInPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
také	také	k9
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Wu-tang	Wu-tanga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnSc1d3
buddhistická	buddhistický	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
,	,	kIx,
Porcelánová	porcelánový	k2eAgFnSc1d1
pagoda	pagoda	k1gFnSc1
v	v	k7c6
Nankingu	Nanking	k1gInSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
zničena	zničen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1856	#num#	k4
<g/>
,	,	kIx,
během	během	k7c2
povstání	povstání	k1gNnSc2
tchaj-pchingů	tchaj-pching	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
ale	ale	k9
nechaly	nechat	k5eAaPmAgInP
čínské	čínský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
postavit	postavit	k5eAaPmF
její	její	k3xOp3gFnSc4
repliku	replika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
seznam	seznam	k1gInSc4
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
byla	být	k5eAaImAgFnS
zapsána	zapsat	k5eAaPmNgFnS
i	i	k9
některá	některý	k3yIgNnPc1
historická	historický	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
týká	týkat	k5eAaImIp3nS
se	se	k3xPyFc4
měst	město	k1gNnPc2
Pching-jao	Pching-jao	k6eAd1
<g/>
,	,	kIx,
Li-ťiang	Li-ťiang	k1gInSc1
<g/>
,	,	kIx,
Si-ti	Si-ti	k1gNnSc1
<g/>
,	,	kIx,
Chung-cchun	Chung-cchun	k1gNnSc1
<g/>
,	,	kIx,
Macao	Macao	k1gNnSc1
<g/>
,	,	kIx,
Jin-sü	Jin-sü	k1gMnSc1
<g/>
,	,	kIx,
Teng-feng	Teng-feng	k1gMnSc1
<g/>
,	,	kIx,
Šang-tu	Šang-t	k1gInSc2
a	a	k8xC
ostrova	ostrov	k1gInSc2
Ku-lang-jü	Ku-lang-jü	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
ukázkám	ukázka	k1gFnPc3
technického	technický	k2eAgNnSc2d1
stavitelství	stavitelství	k1gNnSc2
patří	patřit	k5eAaImIp3nS
32	#num#	k4
kilometrů	kilometr	k1gInPc2
dlouhý	dlouhý	k2eAgInSc4d1
most	most	k1gInSc4
Tung-chaj	Tung-chaj	k1gMnSc1
Ta-čchiao	Ta-čchiao	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
ho	on	k3xPp3gMnSc4
překonal	překonat	k5eAaPmAgInS
55	#num#	k4
kilometrů	kilometr	k1gInPc2
dlouhý	dlouhý	k2eAgInSc4d1
most	most	k1gInSc4
Hongkong-Ču-chaj-Macao	Hongkong-Ču-chaj-Macao	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
ekonomického	ekonomický	k2eAgInSc2d1
boomu	boom	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
spustily	spustit	k5eAaPmAgFnP
Teng	Teng	k1gInSc4
Siao-pchingovy	Siao-pchingův	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
rovněž	rovněž	k9
epicentrem	epicentrum	k1gNnSc7
moderní	moderní	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
v	v	k7c6
Pekingu	Peking	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
byly	být	k5eAaImAgFnP
postaveny	postavit	k5eAaPmNgFnP
některé	některý	k3yIgFnPc1
výjimečné	výjimečný	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgFnPc7
vyniká	vynikat	k5eAaImIp3nS
Pekingský	pekingský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
(	(	kIx(
<g/>
zvaný	zvaný	k2eAgMnSc1d1
obvykle	obvykle	k6eAd1
Ptačí	ptačit	k5eAaImIp3nS
hnízdo	hnízdo	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Pekingské	pekingský	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
plavecké	plavecký	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Vodní	vodní	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
například	například	k6eAd1
Velké	velký	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
v	v	k7c6
Pekingu	Peking	k1gInSc6
z	z	k7c2
dílny	dílna	k1gFnSc2
francouzského	francouzský	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
Paula	Paul	k1gMnSc2
Andreuho	Andreu	k1gMnSc2
<g/>
,	,	kIx,
otevřené	otevřený	k2eAgNnSc1d1
roku	rok	k1gInSc2
2007	#num#	k4
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
pekingská	pekingský	k2eAgFnSc1d1
Budova	budova	k1gFnSc1
Čínské	čínský	k2eAgFnSc2d1
ústřední	ústřední	k2eAgFnSc2d1
televize	televize	k1gFnSc2
<g/>
,	,	kIx,
již	již	k6eAd1
navrhl	navrhnout	k5eAaPmAgMnS
slavný	slavný	k2eAgMnSc1d1
nizozemský	nizozemský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
a	a	k8xC
laureát	laureát	k1gMnSc1
Pritzkerovy	Pritzkerův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
Rem	Rem	k1gMnSc1
Koolhaas	Koolhaas	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Šanghaji	Šanghaj	k1gFnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
například	například	k6eAd1
Shanghai	Shangha	k1gMnPc1
World	World	k1gMnSc1
Financial	Financial	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
Shanghai	Shanghai	k1gNnSc1
Tower	Towra	k1gFnPc2
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc1d3
budova	budova	k1gFnSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
a	a	k8xC
druhá	druhý	k4xOgFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Čínská	čínský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kuřecí	kuřecí	k2eAgMnSc1d1
kung-pao	kung-pao	k1gMnSc1
</s>
<s>
Světově	světově	k6eAd1
významná	významný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
čínská	čínský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
ceněna	cenit	k5eAaImNgFnS
téměř	téměř	k6eAd1
všude	všude	k6eAd1
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zastoupena	zastoupit	k5eAaPmNgFnS
nejen	nejen	k6eAd1
ve	v	k7c6
velkoměstech	velkoměsto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Restaurace	restaurace	k1gFnSc1
vedené	vedený	k2eAgFnSc2d1
čínskými	čínský	k2eAgMnPc7d1
majiteli	majitel	k1gMnPc7
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yQgFnPc6,k3yRgFnPc6,k3yIgFnPc6
pracují	pracovat	k5eAaImIp3nP
téměř	téměř	k6eAd1
vždy	vždy	k6eAd1
jen	jen	k9
čínští	čínský	k2eAgMnPc1d1
kuchaři	kuchař	k1gMnPc1
<g/>
,	,	kIx,
lze	lze	k6eAd1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
prakticky	prakticky	k6eAd1
na	na	k7c6
všech	všecek	k3xTgInPc6
kontinentech	kontinent	k1gInPc6
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
v	v	k7c6
turistických	turistický	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
často	často	k6eAd1
však	však	k9
i	i	k9
mimo	mimo	k7c4
ně	on	k3xPp3gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podávají	podávat	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gMnPc6
pokrmy	pokrm	k1gInPc4
připravované	připravovaný	k2eAgNnSc1d1
podle	podle	k7c2
tradičních	tradiční	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
pěti	pět	k4xCc2
známých	známý	k2eAgFnPc2d1
regionálních	regionální	k2eAgFnPc2d1
kuchyní	kuchyně	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Typickými	typický	k2eAgFnPc7d1
surovinami	surovina	k1gFnPc7
čínské	čínský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
jsou	být	k5eAaImIp3nP
bambusové	bambusový	k2eAgInPc1d1
výhonky	výhonek	k1gInPc1
<g/>
,	,	kIx,
houba	houba	k1gFnSc1
Ucho	ucho	k1gNnSc1
Jidášovo	Jidášův	k2eAgNnSc1d1
<g/>
,	,	kIx,
houba	houba	k1gFnSc1
šiitake	šiitake	k1gFnSc1
<g/>
,	,	kIx,
pasta	pasta	k1gFnSc1
z	z	k7c2
černých	černý	k2eAgInPc2d1
bobů	bob	k1gInPc2
<g/>
,	,	kIx,
pepř	pepř	k1gInSc1
sečuánský	sečuánský	k2eAgInSc1d1
<g/>
,	,	kIx,
sójová	sójový	k2eAgFnSc1d1
omáčka	omáčka	k1gFnSc1
<g/>
,	,	kIx,
tofu	tofu	k1gNnSc1
<g/>
,	,	kIx,
šalotka	šalotka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pálenek	pálenka	k1gFnPc2
rýžové	rýžový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
z	z	k7c2
koření	koření	k1gNnSc2
zázvor	zázvor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známými	známý	k2eAgInPc7d1
pokrmy	pokrm	k1gInPc7
jsou	být	k5eAaImIp3nP
Kuřecí	kuřecí	k2eAgFnSc1d1
kung-pao	kung-pao	k6eAd1
<g/>
,	,	kIx,
Pekingská	pekingský	k2eAgFnSc1d1
kachna	kachna	k1gFnSc1
<g/>
,	,	kIx,
jarní	jarní	k2eAgFnSc2d1
závitky	závitka	k1gFnSc2
či	či	k8xC
Šuej-ču-žou	Šuej-ču-žou	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
však	však	k9
velké	velký	k2eAgInPc1d1
regionální	regionální	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
<g/>
,	,	kIx,
k	k	k7c3
nejznámějším	známý	k2eAgFnPc3d3
regionálním	regionální	k2eAgFnPc3d1
kuchyním	kuchyně	k1gFnPc3
patří	patřit	k5eAaImIp3nS
kantonská	kantonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
užívající	užívající	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
wok	wok	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pekingská	pekingský	k2eAgFnSc1d1
(	(	kIx(
<g/>
často	často	k6eAd1
úprava	úprava	k1gFnSc1
v	v	k7c6
páře	pára	k1gFnSc6
či	či	k8xC
grilování	grilování	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sečuánská	sečuánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejpikantnějším	pikantní	k2eAgFnPc3d3
<g/>
)	)	kIx)
či	či	k8xC
čchaošanská	čchaošanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
hodně	hodně	k6eAd1
vegetariánská	vegetariánský	k2eAgNnPc1d1
<g/>
,	,	kIx,
založená	založený	k2eAgNnPc1d1
na	na	k7c6
tofu	tofu	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
důraz	důraz	k1gInSc1
je	být	k5eAaImIp3nS
kladen	klást	k5eAaImNgInS
na	na	k7c4
estetickou	estetický	k2eAgFnSc4d1
úpravu	úprava	k1gFnSc4
pokrmů	pokrm	k1gInPc2
a	a	k8xC
stolování	stolování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradičně	tradičně	k6eAd1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
jídelní	jídelní	k2eAgFnSc2d1
hůlky	hůlka	k1gFnSc2
<g/>
,	,	kIx,
polévky	polévka	k1gFnSc2
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
lžící	lžíce	k1gFnSc7
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
porcelánovou	porcelánový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
pokrmy	pokrm	k1gInPc4
<g/>
,	,	kIx,
např.	např.	kA
pekingská	pekingský	k2eAgFnSc1d1
kachna	kachna	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
však	však	k9
jedí	jíst	k5eAaImIp3nP
rukama	ruka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
pokrmy	pokrm	k1gInPc1
se	se	k3xPyFc4
servírují	servírovat	k5eAaBmIp3nP
současně	současně	k6eAd1
<g/>
,	,	kIx,
každý	každý	k3xTgMnSc1
ze	z	k7c2
stolujících	stolující	k2eAgMnPc2d1
má	mít	k5eAaImIp3nS
před	před	k7c7
sebou	se	k3xPyFc7
misku	miska	k1gFnSc4
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
si	se	k3xPyFc3
nabírá	nabírat	k5eAaImIp3nS
z	z	k7c2
pokrmů	pokrm	k1gInPc2
na	na	k7c6
stole	stol	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přednostně	přednostně	k6eAd1
jedí	jíst	k5eAaImIp3nP
starší	starý	k2eAgMnPc1d2
lidé	člověk	k1gMnPc1
a	a	k8xC
muži	muž	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
těžký	těžký	k2eAgInSc4d1
prohřešek	prohřešek	k1gInSc4
proti	proti	k7c3
etiketě	etiketa	k1gFnSc3
se	se	k3xPyFc4
pokládá	pokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
někdo	někdo	k3yInSc1
zapíchne	zapíchnout	k5eAaPmIp3nS
hůlky	hůlka	k1gFnPc4
do	do	k7c2
zbytků	zbytek	k1gInPc2
jídla	jídlo	k1gNnSc2
v	v	k7c6
misce	miska	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jídla	jídlo	k1gNnSc2
pijí	pít	k5eAaImIp3nP
Číňané	Číňan	k1gMnPc1
nejraději	rád	k6eAd3
zelený	zelený	k2eAgInSc4d1
čaj	čaj	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Věda	věda	k1gFnSc1
</s>
<s>
Nobelistka	Nobelistka	k1gFnSc1
Tchu	Tchus	k1gInSc2
Jou-jou	Jou-ja	k1gFnSc7
</s>
<s>
KonfuciusSouvisející	KonfuciusSouvisející	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Čínská	čínský	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Lékař	lékař	k1gMnSc1
Chua	Chua	k1gMnSc1
Tchuo	Tchuo	k1gMnSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
již	již	k6eAd1
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k9
první	první	k4xOgMnSc1
užil	užít	k5eAaPmAgInS
v	v	k7c6
lékařství	lékařství	k1gNnSc6
anestézii	anestézie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
Základy	základ	k1gInPc1
matematického	matematický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
stavěl	stavět	k5eAaImAgMnS
v	v	k7c6
Číně	Čína	k1gFnSc6
Liou	Lious	k1gInSc2
Chuej	Chuej	k1gInSc4
ve	v	k7c4
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslulým	proslulý	k2eAgMnSc7d1
všestranným	všestranný	k2eAgMnSc7d1
učencem	učenec	k1gMnSc7
starověku	starověk	k1gInSc2
byl	být	k5eAaImAgMnS
Šen	Šen	k1gMnSc1
Kua	Kua	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
práci	práce	k1gFnSc4
Meng-si	Meng-se	k1gFnSc4
pi-tchan	pi-tchana	k1gFnPc2
(	(	kIx(
<g/>
Eseje	esej	k1gFnSc2
víru	vír	k1gInSc2
snů	sen	k1gInPc2
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1088	#num#	k4
popsal	popsat	k5eAaPmAgMnS
jako	jako	k9
první	první	k4xOgMnSc1
na	na	k7c6
světě	svět	k1gInSc6
princip	princip	k1gInSc1
magnetické	magnetický	k2eAgFnSc2d1
jehly	jehla	k1gFnSc2
kompasu	kompas	k1gInSc2
a	a	k8xC
naznačil	naznačit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
využita	využít	k5eAaPmNgFnS
pro	pro	k7c4
navigaci	navigace	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
Evropě	Evropa	k1gFnSc6
byla	být	k5eAaImAgFnS
popsána	popsat	k5eAaPmNgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1187	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
promýšlení	promýšlení	k1gNnSc6
kompasu	kompas	k1gInSc2
definoval	definovat	k5eAaBmAgInS
též	též	k9
rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
magnetickým	magnetický	k2eAgInSc7d1
a	a	k8xC
skutečným	skutečný	k2eAgInSc7d1
severním	severní	k2eAgInSc7d1
pólem	pól	k1gInSc7
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
400	#num#	k4
let	léto	k1gNnPc2
před	před	k7c7
evropskou	evropský	k2eAgFnSc7d1
vědou	věda	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnými	významný	k2eAgMnPc7d1
starověkými	starověký	k2eAgMnPc7d1
astronomy	astronom	k1gMnPc7
byli	být	k5eAaImAgMnP
Cu	Cu	k1gMnSc1
Čchung-č	Čchung-č	k1gMnSc1
<g/>
’	’	k?
nebo	nebo	k8xC
Čang	Čang	k1gMnSc1
Cheng	Cheng	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Su	Su	k?
Sung	Sung	k1gMnSc1
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
sestavil	sestavit	k5eAaPmAgMnS
mapu	mapa	k1gFnSc4
hvězdné	hvězdný	k2eAgFnSc2d1
oblohy	obloha	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
nejstarší	starý	k2eAgFnSc1d3
takovou	takový	k3xDgFnSc7
dochovanou	dochovaný	k2eAgFnSc7d1
tištěnou	tištěný	k2eAgFnSc7d1
mapou	mapa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgMnS
jakýmsi	jakýsi	k3yIgInSc7
styčným	styčný	k2eAgInSc7d1
bodem	bod	k1gInSc7
tradiční	tradiční	k2eAgFnSc2d1
čínské	čínský	k2eAgFnSc2d1
vzdělanosti	vzdělanost	k1gFnSc2
a	a	k8xC
vznikající	vznikající	k2eAgFnSc2d1
evropské	evropský	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
Sü	Sü	k1gFnSc2
Kuang-čchi	Kuang-čch	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gFnSc6
době	doba	k1gFnSc6
poznání	poznání	k1gNnSc2
čínské	čínský	k2eAgFnSc2d1
přírodovědy	přírodověda	k1gFnSc2
a	a	k8xC
medicíny	medicína	k1gFnSc2
sepsal	sepsat	k5eAaPmAgInS
a	a	k8xC
uchoval	uchovat	k5eAaPmAgInS
Li	li	k9
Š	Š	kA
<g/>
’	’	k?
<g/>
-čen	-čen	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejslavnější	slavný	k2eAgFnSc7d3
čínskou	čínský	k2eAgFnSc7d1
vědkyní	vědkyně	k1gFnSc7
je	být	k5eAaImIp3nS
fyzička	fyzička	k1gFnSc1
Chien-Shiung	Chien-Shiung	k1gMnSc1
Wu	Wu	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
Čchien	Čchien	k2eAgInSc1d1
Süe-sen	Süe-sen	k1gInSc1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgInS
na	na	k7c6
rozvoji	rozvoj	k1gInSc6
raketové	raketový	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
otcem	otec	k1gMnSc7
čínského	čínský	k2eAgInSc2d1
kosmického	kosmický	k2eAgInSc2d1
programu	program	k1gInSc2
a	a	k8xC
hlavním	hlavní	k2eAgMnSc7d1
konstruktérem	konstruktér	k1gMnSc7
čínských	čínský	k2eAgFnPc2d1
kosmických	kosmický	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
Dlouhý	dlouhý	k2eAgInSc1d1
pochod	pochod	k1gInSc1
(	(	kIx(
<g/>
Čchang	Čchang	k1gMnSc1
Čeng	Čeng	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
Číňané	Číňan	k1gMnPc1
žijící	žijící	k2eAgMnPc1d1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
Jang	Janga	k1gFnPc2
Čen-ning	Čen-ning	k1gInSc4
a	a	k8xC
Li	li	k9
Čeng-tao	Čeng-tao	k6eAd1
získali	získat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
fyziku	fyzika	k1gFnSc4
<g/>
,	,	kIx,
stejné	stejný	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
získali	získat	k5eAaPmAgMnP
i	i	k9
další	další	k2eAgMnPc1d1
čínští	čínský	k2eAgMnPc1d1
emigranti	emigrant	k1gMnPc1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1998	#num#	k4
Cchuej	Cchuej	k1gInSc4
Čchi	Čch	k1gFnSc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2009	#num#	k4
Charles	Charles	k1gMnSc1
Kuen	Kuen	k1gMnSc1
Kao	Kao	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
naturalizovaný	naturalizovaný	k2eAgMnSc1d1
Američan	Američan	k1gMnSc1
Čchiou	Čchiá	k1gFnSc4
Čcheng-tung	Čcheng-tung	k1gInSc1
získal	získat	k5eAaPmAgInS
prestižní	prestižní	k2eAgFnSc4d1
matematickou	matematický	k2eAgFnSc4d1
Fieldsovu	Fieldsův	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
<g/>
,	,	kIx,
informatik	informatik	k1gMnSc1
Andrew	Andrew	k1gMnSc1
Jao	Jao	k1gMnSc1
zase	zase	k9
Turingovu	Turingův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemička	chemička	k1gFnSc1
Tchu	Tch	k2eAgFnSc4d1
Jou-jou	Jou-já	k1gFnSc4
získala	získat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
fyziologii	fyziologie	k1gFnSc4
nebo	nebo	k8xC
lékařství	lékařství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
Nejprestižnější	prestižní	k2eAgNnSc4d3
vědecké	vědecký	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
převzala	převzít	k5eAaPmAgFnS
jako	jako	k9
první	první	k4xOgFnSc1
občanka	občanka	k1gFnSc1
ČLR	ČLR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgFnSc7
izolovala	izolovat	k5eAaBmAgFnS
hořčin	hořčina	k1gFnPc2
artemisinin	artemisinin	k2eAgInSc1d1
z	z	k7c2
pelyňku	pelyněk	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
využila	využít	k5eAaPmAgFnS
v	v	k7c6
léčbě	léčba	k1gFnSc6
malárie	malárie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kombinovaná	kombinovaný	k2eAgFnSc1d1
terapie	terapie	k1gFnSc1
snížila	snížit	k5eAaPmAgFnS
úmrtnost	úmrtnost	k1gFnSc4
nemoci	nemoc	k1gFnSc2
o	o	k7c4
20	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
%	%	kIx~
a	a	k8xC
jen	jen	k9
na	na	k7c6
africkém	africký	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
zachránila	zachránit	k5eAaPmAgFnS
stovky	stovka	k1gFnPc4
tisíc	tisíc	k4xCgInPc2
životů	život	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známým	známý	k1gMnSc7
odborníkem	odborník	k1gMnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
potravinářství	potravinářství	k1gNnSc2
je	být	k5eAaImIp3nS
Da-Wen	Da-Wen	k1gInSc1
Sun	Sun	kA
<g/>
.	.	kIx.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3
čínským	čínský	k2eAgMnSc7d1
filozofem	filozof	k1gMnSc7
je	být	k5eAaImIp3nS
Konfucius	Konfucius	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
učení	učení	k1gNnSc4
čínskou	čínský	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
hluboce	hluboko	k6eAd1
ovlivnilo	ovlivnit	k5eAaPmAgNnS
a	a	k8xC
formovalo	formovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
oponentem	oponent	k1gMnSc7
byl	být	k5eAaImAgMnS
Mocius	Mocius	k1gMnSc1
<g/>
,	,	kIx,
konfucianismus	konfucianismus	k1gInSc1
však	však	k9
převládl	převládnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
Konfuciovým	Konfuciový	k2eAgMnPc3d1
nejvýznamnějším	významný	k2eAgMnPc3d3
žákům	žák	k1gMnPc3
patřil	patřit	k5eAaImAgInS
Mencius	Mencius	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
pragmatickým	pragmatický	k2eAgMnSc7d1
oponentem	oponent	k1gMnSc7
byl	být	k5eAaImAgMnS
Sün-c	Sün-c	k1gFnSc4
<g/>
’	’	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Materialistické	materialistický	k2eAgInPc1d1
prvky	prvek	k1gInPc1
do	do	k7c2
konfuciánství	konfuciánství	k1gNnSc2
vnášel	vnášet	k5eAaImAgMnS
Wang	Wang	k1gMnSc1
Čchung	Čchung	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tvůrcům	tvůrce	k1gMnPc3
neokonfucianismu	neokonfucianismus	k1gInSc2
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
patřil	patřit	k5eAaImAgMnS
Wang	Wang	k1gMnSc1
Jang-ming	Jang-ming	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Završitelem	Završitel	k1gMnSc7
neokonfuciánské	okonfuciánský	k2eNgFnSc2d1
scholastiky	scholastika	k1gFnSc2
byl	být	k5eAaImAgMnS
Ču	Ču	k1gMnSc1
Si	se	k3xPyFc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
zakladatelům	zakladatel	k1gMnPc3
legismu	legismus	k1gInSc2
patřil	patřit	k5eAaImAgMnS
Chan	Chan	k1gMnSc1
Fej	Fej	k1gMnSc1
<g/>
,	,	kIx,
k	k	k7c3
jeho	jeho	k3xOp3gMnPc3
klíčovým	klíčový	k2eAgMnPc3d1
představitelům	představitel	k1gMnPc3
Li	li	k8xS
S	s	k7c7
<g/>
’	’	k?
a	a	k8xC
Šang	Šang	k1gMnSc1
Jang	Jang	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představitelem	představitel	k1gMnSc7
konfucianismu	konfucianismus	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
hledal	hledat	k5eAaImAgInS
spojení	spojení	k1gNnSc4
s	s	k7c7
moderním	moderní	k2eAgInSc7d1
světem	svět	k1gInSc7
a	a	k8xC
evropskou	evropský	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Kchang	Kchang	k1gMnSc1
Jou-wej	Jou-wej	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jeho	jeho	k3xOp3gMnPc3
souputníkům	souputník	k1gMnPc3
v	v	k7c6
„	„	k?
<g/>
europeizačních	europeizační	k2eAgFnPc6d1
snahách	snaha	k1gFnPc6
<g/>
“	“	k?
patřil	patřit	k5eAaImAgInS
Jen	jen	k9
Fu	fu	k0
nebo	nebo	k8xC
Liang	Liang	k1gMnSc1
Čchi-čchao	Čchi-čchao	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přišla	přijít	k5eAaPmAgFnS
myšlenková	myšlenkový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
s	s	k7c7
dílem	dílo	k1gNnSc7
záhadného	záhadný	k2eAgMnSc2d1
autora	autor	k1gMnSc2
známého	známý	k2eAgMnSc2d1
jako	jako	k8xC,k8xS
Lao-c	Lao-c	k1gInSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
učení	učení	k1gNnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
taoismus	taoismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
Laovým	Laový	k2eAgMnPc3d1
nejvýznamnějším	významný	k2eAgMnPc3d3
následovníkům	následovník	k1gMnPc3
patřil	patřit	k5eAaImAgInS
Čuang-c	Čuang-c	k1gInSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
pronikání	pronikání	k1gNnSc4
buddhismu	buddhismus	k1gInSc2
do	do	k7c2
Číny	Čína	k1gFnSc2
měl	mít	k5eAaImAgInS
zásadní	zásadní	k2eAgInSc1d1
podíl	podíl	k1gInSc1
Kumáradžíva	Kumáradžíva	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7
čínské	čínský	k2eAgFnSc2d1
historiografie	historiografie	k1gFnSc2
byl	být	k5eAaImAgInS
S	s	k7c7
<g/>
’	’	k?
<g/>
-ma	-ma	k?
Čchien	Čchien	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
jejím	její	k3xOp3gInSc6
rozvoji	rozvoj	k1gInSc6
ve	v	k7c6
starověku	starověk	k1gInSc6
se	se	k3xPyFc4
podílela	podílet	k5eAaImAgFnS
i	i	k9
jedna	jeden	k4xCgFnSc1
žena	žena	k1gFnSc1
<g/>
:	:	kIx,
Pan	Pan	k1gMnSc1
Čao	čao	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
moderním	moderní	k2eAgMnPc3d1
historikům	historik	k1gMnPc3
patřili	patřit	k5eAaImAgMnP
Chuang	Chuang	k1gMnSc1
Sien-fan	Sien-fan	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
zároveň	zároveň	k6eAd1
zakladatelem	zakladatel	k1gMnSc7
čínské	čínský	k2eAgFnSc2d1
antropologie	antropologie	k1gFnSc2
<g/>
,	,	kIx,
či	či	k8xC
</s>
<s>
S	s	k7c7
<g/>
'	'	kIx"
<g/>
-ma	-ma	k?
Kuang	Kuang	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojevůdce	vojevůdce	k1gMnSc1
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Sun-c	Sun-c	k1gInSc1
<g/>
’	’	k?
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
legendárního	legendární	k2eAgInSc2d1
traktátu	traktát	k1gInSc2
Umění	umění	k1gNnSc1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
knih	kniha	k1gFnPc2
o	o	k7c6
strategii	strategie	k1gFnSc6
a	a	k8xC
taktice	taktika	k1gFnSc6
vůbec	vůbec	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
Tvůrcem	tvůrce	k1gMnSc7
čínské	čínský	k2eAgFnSc2d1
hláskové	hláskový	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
pchin-jin	pchin-jin	k1gInSc1
byl	být	k5eAaImAgMnS
jazykovědec	jazykovědec	k1gMnSc1
Čou	Čou	k1gMnSc1
Jou-kuang	Jou-kuang	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
velkou	velký	k2eAgFnSc4d1
jazykovou	jazykový	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
(	(	kIx(
<g/>
paj-chua	paj-chua	k6eAd1
<g/>
)	)	kIx)
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
usiloval	usilovat	k5eAaImAgMnS
Chu	Chu	k1gMnSc1
Š	Š	kA
<g/>
’	’	k?
<g/>
.	.	kIx.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Podle	podle	k7c2
QS	QS	kA
World	World	k1gInSc4
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc4
2020	#num#	k4
je	být	k5eAaImIp3nS
nejlepší	dobrý	k2eAgFnSc7d3
vysokou	vysoký	k2eAgFnSc7d1
školou	škola	k1gFnSc7
v	v	k7c6
Číně	Čína	k1gFnSc6
Univerzita	univerzita	k1gFnSc1
Čching-chua	Čching-chua	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
zároveň	zároveň	k6eAd1
16	#num#	k4
<g/>
.	.	kIx.
nejkvalitnější	kvalitní	k2eAgNnSc1d3
vysokou	vysoký	k2eAgFnSc7d1
školou	škola	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
a	a	k8xC
třetí	třetí	k4xOgInSc4
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
Jde	jít	k5eAaImIp3nS
o	o	k7c4
výzkumnou	výzkumný	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
sídlící	sídlící	k2eAgFnSc1d1
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
z	z	k7c2
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
měla	mít	k5eAaImAgFnS
Čína	Čína	k1gFnSc1
zaplatit	zaplatit	k5eAaPmF
Spojeným	spojený	k2eAgInPc3d1
státům	stát	k1gInPc3
americkým	americký	k2eAgInPc3d1
jako	jako	k8xC,k8xS
reparace	reparace	k1gFnPc1
po	po	k7c6
prohraném	prohraný	k2eAgNnSc6d1
Boxerském	boxerský	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
–	–	k?
na	na	k7c6
základě	základ	k1gInSc6
návrhu	návrh	k1gInSc2
ministra	ministr	k1gMnSc2
zahraničí	zahraničí	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
Johna	John	k1gMnSc2
Haye	Hay	k1gMnSc2
byly	být	k5eAaImAgFnP
reparace	reparace	k1gFnPc1
Číně	Čína	k1gFnSc6
odpuštěny	odpuštěn	k2eAgInPc1d1
s	s	k7c7
podmínkou	podmínka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
použity	použít	k5eAaPmNgFnP
na	na	k7c4
založení	založení	k1gNnSc4
vzdělávací	vzdělávací	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvními	první	k4xOgInPc7
učiteli	učitel	k1gMnPc7
na	na	k7c6
škole	škola	k1gFnSc6
pak	pak	k6eAd1
byli	být	k5eAaImAgMnP
Američané	Američan	k1gMnPc1
z	z	k7c2
řad	řada	k1gFnPc2
YMCA	YMCA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
vazbám	vazba	k1gFnPc3
na	na	k7c4
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
na	na	k7c6
konci	konec	k1gInSc6
Čínské	čínský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
ke	k	k7c3
značné	značný	k2eAgFnSc3d1
personální	personální	k2eAgFnSc3d1
změně	změna	k1gFnSc3
–	–	k?
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
profesorů	profesor	k1gMnPc2
odešla	odejít	k5eAaPmAgFnS
na	na	k7c4
Tchaj-wan	Tchaj-wan	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
založili	založit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
v	v	k7c6
Sin-ču	Sin-čus	k1gInSc6
Národní	národní	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
Čching-chua	Čching-chu	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
Čching-chua	Čching-chua	k1gMnSc1
studoval	studovat	k5eAaImAgMnS
například	například	k6eAd1
nobelista	nobelista	k1gMnSc1
Jang	Jang	k1gMnSc1
Čen-ning	Čen-ning	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
nejkvalitnější	kvalitní	k2eAgFnSc7d3
školou	škola	k1gFnSc7
v	v	k7c6
Číně	Čína	k1gFnSc6
je	být	k5eAaImIp3nS
podle	podle	k7c2
QS	QS	kA
Pekingská	pekingský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
celosvětově	celosvětově	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
nejstarší	starý	k2eAgFnSc4d3
čínskou	čínský	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
roku	rok	k1gInSc3
1898	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
je	být	k5eAaImIp3nS
proslulá	proslulý	k2eAgFnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
ní	on	k3xPp3gFnSc6
studovali	studovat	k5eAaImAgMnP
zakladatelé	zakladatel	k1gMnPc1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
krom	krom	k7c2
toho	ten	k3xDgInSc2
např.	např.	kA
spisovatel	spisovatel	k1gMnSc1
Lu	Lu	k1gMnSc1
Sün	Sün	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
prestižní	prestižní	k2eAgFnPc1d1
univerzity	univerzita	k1gFnPc1
sídlí	sídlet	k5eAaImIp3nP
těsně	těsně	k6eAd1
vedle	vedle	k7c2
sebe	se	k3xPyFc2
<g/>
,	,	kIx,
v	v	k7c6
pekingském	pekingský	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
Chaj-tien	Chaj-tien	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
epicentrem	epicentrum	k1gNnSc7
vzdělanosti	vzdělanost	k1gFnSc2
je	být	k5eAaImIp3nS
Hongkong	Hongkong	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
25	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
světového	světový	k2eAgInSc2d1
žebříčku	žebříček	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Hongkongská	hongkongský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
jen	jen	k9
o	o	k7c4
málo	málo	k6eAd1
níže	nízce	k6eAd2
Hongkongská	hongkongský	k2eAgFnSc1d1
vědecko-technologická	vědecko-technologický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Čínská	čínský	k2eAgFnSc1d1
hongkongská	hongkongský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
a	a	k8xC
Městská	městský	k2eAgFnSc1d1
hongkongská	hongkongský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
tato	tento	k3xDgNnPc4
dvě	dva	k4xCgNnPc4
tradiční	tradiční	k2eAgNnPc4d1
centra	centrum	k1gNnPc4
vzdělanosti	vzdělanost	k1gFnSc2
si	se	k3xPyFc3
vysokou	vysoký	k2eAgFnSc4d1
kvalitu	kvalita	k1gFnSc4
udržuje	udržovat	k5eAaImIp3nS
Univerzita	univerzita	k1gFnSc1
Fu-tan	Fu-tan	k1gInSc1
v	v	k7c6
Šanghaji	Šanghaj	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
pod	pod	k7c7
QS	QS	kA
propracovala	propracovat	k5eAaPmAgFnS
již	již	k6eAd1
na	na	k7c6
pozici	pozice	k1gFnSc6
40	#num#	k4
<g/>
.	.	kIx.
nejkvalitnější	kvalitní	k2eAgFnSc2d3
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Šanghaji	Šanghaj	k1gFnSc6
si	se	k3xPyFc3
vysokou	vysoký	k2eAgFnSc4d1
prestiž	prestiž	k1gFnSc4
drží	držet	k5eAaImIp3nS
i	i	k9
Šanghajská	šanghajský	k2eAgFnSc1d1
dopravní	dopravní	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
navzdory	navzdory	k7c3
svému	svůj	k3xOyFgInSc3
názvu	název	k1gInSc3
univerzitou	univerzita	k1gFnSc7
všeobecnou	všeobecný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
"	"	kIx"
<g/>
periferii	periferie	k1gFnSc4
<g/>
"	"	kIx"
se	se	k3xPyFc4
nejvíce	hodně	k6eAd3,k6eAd1
daří	dařit	k5eAaImIp3nS
Čeťiangské	Čeťiangský	k2eAgFnSc3d1
univerzitě	univerzita	k1gFnSc3
v	v	k7c6
Chang-čou	Chang-čou	k1gNnSc6
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
provincie	provincie	k1gFnSc2
Če-ťiang	Če-ťianga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
QS	QS	kA
World	World	k1gInSc1
Ranking	Ranking	k1gInSc1
2020	#num#	k4
jde	jít	k5eAaImIp3nS
o	o	k7c4
54	#num#	k4
<g/>
.	.	kIx.
nejlepší	dobrý	k2eAgFnSc4d3
vysokou	vysoký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
stovce	stovka	k1gFnSc6
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nS
také	také	k9
Čínská	čínský	k2eAgFnSc1d1
vědecko-technologická	vědecko-technologický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Che-feji	Che-fej	k1gInSc6
v	v	k7c6
provincii	provincie	k1gFnSc6
An-chuej	An-chuej	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradice	tradice	k1gFnSc1
rozeznává	rozeznávat	k5eAaImIp3nS
tzv.	tzv.	kA
Ťiou-siao	Ťiou-siao	k1gNnSc1
Lien-meng	Lien-meng	k1gInSc1
<g/>
,	,	kIx,
jakýsi	jakýsi	k3yIgInSc1
neformální	formální	k2eNgInSc1d1
klub	klub	k1gInSc1
devíti	devět	k4xCc2
nejtradičnějších	tradiční	k2eAgFnPc2d3
čínských	čínský	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
též	též	k9
Liga	liga	k1gFnSc1
C	C	kA
<g/>
9	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
"	"	kIx"
<g/>
čínská	čínský	k2eAgFnSc1d1
břečťanová	břečťanový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
Krom	krom	k7c2
již	již	k6eAd1
uvedených	uvedený	k2eAgFnPc2d1
do	do	k7c2
ní	on	k3xPp3gFnSc2
patří	patřit	k5eAaImIp3nS
Nankingská	nankingský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Charbinská	Charbinský	k2eAgFnSc1d1
polytechnická	polytechnický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
a	a	k8xC
Si-anská	Si-anský	k2eAgFnSc1d1
dopravní	dopravní	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Li	li	k9
Na	na	k7c6
</s>
<s>
Chou	Chou	k6eAd1
I-fan	I-fan	k1gInSc1
</s>
<s>
Od	od	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zažívá	zažívat	k5eAaImIp3nS
v	v	k7c6
Číně	Čína	k1gFnSc6
obrovský	obrovský	k2eAgInSc1d1
rozmach	rozmach	k1gInSc1
sport	sport	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
státní	státní	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
se	se	k3xPyFc4
Čína	Čína	k1gFnSc1
prvně	prvně	k?
zúčastnila	zúčastnit	k5eAaPmAgFnS
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
letních	letní	k2eAgFnPc2d1
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
první	první	k4xOgFnSc7
olympijskou	olympijský	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
se	se	k3xPyFc4
zde	zde	k6eAd1
stal	stát	k5eAaPmAgMnS
gymnasta	gymnasta	k1gMnSc1
Li	li	k8xS
Ning	Ning	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
ČLR	ČLR	kA
vybojovala	vybojovat	k5eAaPmAgFnS
237	#num#	k4
zlatých	zlatý	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
(	(	kIx(
<g/>
k	k	k7c3
2018	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
dala	dát	k5eAaPmAgFnS
světu	svět	k1gInSc3
řadu	řad	k1gInSc2
dalších	další	k2eAgMnPc2d1
úspěšných	úspěšný	k2eAgMnPc2d1
sportovců	sportovec	k1gMnPc2
<g/>
:	:	kIx,
K	k	k7c3
největším	veliký	k2eAgMnSc7d3
čínských	čínský	k2eAgMnPc2d1
sportovním	sportovní	k2eAgFnPc3d1
hvězdám	hvězda	k1gFnPc3
patří	patřit	k5eAaImIp3nS
tenistka	tenistka	k1gFnSc1
Li	li	k8xS
Na	na	k7c6
<g/>
,	,	kIx,
vítězka	vítězka	k1gFnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
2014	#num#	k4
a	a	k8xC
Roland	Roland	k1gInSc4
Garros	Garrosa	k1gFnPc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
byla	být	k5eAaImAgFnS
světovou	světový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
Pcheng	Pcheng	k1gMnSc1
Šuaj	Šuaj	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
americké	americký	k2eAgFnSc6d1
lize	liga	k1gFnSc6
NBA	NBA	kA
se	se	k3xPyFc4
prosadil	prosadit	k5eAaPmAgMnS
basketbalista	basketbalista	k1gMnSc1
Jao	Jao	k1gMnSc1
Ming	Ming	k1gMnSc1
<g/>
,	,	kIx,
proslulý	proslulý	k2eAgMnSc1d1
svou	svůj	k3xOyFgFnSc7
výškou	výška	k1gFnSc7
(	(	kIx(
<g/>
229	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
Skokanka	skokanka	k1gFnSc1
do	do	k7c2
vody	voda	k1gFnSc2
Wu	Wu	k1gFnSc1
Min-sia	Min-sia	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
legendou	legenda	k1gFnSc7
své	svůj	k3xOyFgFnSc2
disciplíny	disciplína	k1gFnSc2
a	a	k8xC
vyhrála	vyhrát	k5eAaPmAgFnS
pět	pět	k4xCc4
zlatých	zlatý	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
Jen	jen	k9
o	o	k7c4
trochu	trochu	k6eAd1
méně	málo	k6eAd2
úspěšné	úspěšný	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
v	v	k7c6
této	tento	k3xDgFnSc6
disciplíně	disciplína	k1gFnSc6
její	její	k3xOp3gFnSc2
kolegyně	kolegyně	k1gFnSc2
Fu	fu	k0
Ming-sia	Ming-sium	k1gNnPc1
<g/>
,	,	kIx,
Čchen	Čchen	k2eAgInSc1d1
Žuo-lin	Žuo-lin	k1gInSc1
a	a	k8xC
Kuo	Kuo	k1gFnSc1
Ťing-ťing	Ťing-ťing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
ve	v	k7c6
skocích	skok	k1gInPc6
do	do	k7c2
vody	voda	k1gFnSc2
získala	získat	k5eAaPmAgFnS
k	k	k7c3
roku	rok	k1gInSc3
2018	#num#	k4
již	již	k9
40	#num#	k4
zlatých	zlatý	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
její	její	k3xOp3gFnSc1
nejúspěšnější	úspěšný	k2eAgFnSc1d3
disciplína	disciplína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	V	kA
Li	li	k8xS
Ningových	Ningův	k2eAgFnPc6d1
stopách	stopa	k1gFnPc6
šel	jít	k5eAaImAgMnS
především	především	k9
gymnasta	gymnasta	k1gMnSc1
Cou	Cou	k1gMnSc1
Kchaj	Kchaj	k1gMnSc1
<g/>
,	,	kIx,
držitel	držitel	k1gMnSc1
pěti	pět	k4xCc2
olympijských	olympijský	k2eAgFnPc2d1
zlatých	zlatá	k1gFnPc2
kovů	kov	k1gInPc2
<g/>
,	,	kIx,
Li	li	k8xS
Siao-pcheng	Siao-pcheng	k1gInSc1
má	mít	k5eAaImIp3nS
jen	jen	k9
o	o	k7c4
jeden	jeden	k4xCgInSc4
méně	málo	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
více	hodně	k6eAd2
tradičním	tradiční	k2eAgMnPc3d1
je	být	k5eAaImIp3nS
v	v	k7c6
Číně	Čína	k1gFnSc6
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
:	:	kIx,
Ma	Ma	k1gFnSc1
Lung	Lunga	k1gFnPc2
má	mít	k5eAaImIp3nS
tři	tři	k4xCgNnPc4
olympijská	olympijský	k2eAgNnPc4d1
zlata	zlato	k1gNnPc4
a	a	k8xC
je	být	k5eAaImIp3nS
desetinásobným	desetinásobný	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
<g/>
,	,	kIx,
stolní	stolní	k2eAgFnSc1d1
tenistka	tenistka	k1gFnSc1
Teng	Teng	k1gInSc4
Ja-pching	Ja-pching	k1gInSc1
má	mít	k5eAaImIp3nS
z	z	k7c2
OH	OH	kA
dokonce	dokonce	k9
čtyři	čtyři	k4xCgNnPc4
zlata	zlato	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
ženami	žena	k1gFnPc7
dominovala	dominovat	k5eAaImAgFnS
rovněž	rovněž	k9
Wang	Wang	k1gInSc4
Nan	Nan	k1gFnSc2
či	či	k8xC
Čang	Čang	k1gMnSc1
I-ning	I-ning	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
Číňanem	Číňan	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zkompletoval	zkompletovat	k5eAaPmAgMnS
ping-pongový	ping-pongový	k2eAgInSc4d1
grandslam	grandslam	k1gInSc4
(	(	kIx(
<g/>
zlato	zlato	k1gNnSc4
z	z	k7c2
OH	OH	kA
<g/>
,	,	kIx,
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
a	a	k8xC
vítěz	vítěz	k1gMnSc1
světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
Liou	Lioa	k1gFnSc4
Kuo-liang	Kuo-lianga	k1gFnPc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
druhým	druhý	k4xOgNnSc7
Kchung	Kchung	k1gMnSc1
Ling-chuej	Ling-chuej	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
třetím	třetí	k4xOgMnSc6
Čang	Čang	k1gInSc1
Ťi-kche	Ťi-kch	k1gInPc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Před	před	k7c7
nimi	on	k3xPp3gMnPc7
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
povedlo	povést	k5eAaPmAgNnS
jen	jen	k6eAd1
Švédovi	Švéd	k1gMnSc3
Jan-Ove	Jan-Oev	k1gFnSc2
Waldnerovi	Waldnerův	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
úspěšní	úspěšný	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
Číňané	Číňan	k1gMnPc1
v	v	k7c6
badmintonu	badminton	k1gInSc6
<g/>
,	,	kIx,
například	například	k6eAd1
Lin	Lina	k1gFnPc2
Tan	Tan	k1gFnSc3
je	být	k5eAaImIp3nS
první	první	k4xOgMnSc1
mužský	mužský	k2eAgMnSc1d1
badmintonista	badmintonista	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
získal	získat	k5eAaPmAgMnS
zlatou	zlatý	k2eAgFnSc4d1
olympijskou	olympijský	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
v	v	k7c6
individuální	individuální	k2eAgFnSc6d1
disciplíně	disciplína	k1gFnSc6
dvakrát	dvakrát	k6eAd1
po	po	k7c6
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
159	#num#	k4
<g/>
]	]	kIx)
Králem	Král	k1gMnSc7
badmintonové	badmintonový	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
je	být	k5eAaImIp3nS
Fu	fu	k0
Chaj-feng	Chaj-fenga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ping-pongu	ping-pong	k1gInSc6
a	a	k8xC
badmintonu	badminton	k1gInSc6
jsou	být	k5eAaImIp3nP
již	již	k9
Číňané	Číňan	k1gMnPc1
na	na	k7c6
čele	čelo	k1gNnSc6
historických	historický	k2eAgFnPc2d1
tabulek	tabulka	k1gFnPc2
olympijských	olympijský	k2eAgMnPc2d1
medailistů	medailista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plavec	plavec	k1gMnSc1
Sun	Sun	kA
Jang	Jang	k1gMnSc1
má	mít	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
olympijské	olympijský	k2eAgFnPc4d1
zlaté	zlatý	k2eAgFnPc4d1
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
světový	světový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
ženském	ženský	k2eAgNnSc6d1
plavání	plavání	k1gNnSc6
brala	brát	k5eAaImAgFnS
dvě	dva	k4xCgFnPc4
olympijské	olympijský	k2eAgFnPc4d1
zlaté	zlatý	k2eAgFnPc4d1
Jie	Jie	k1gFnPc4
Š	Š	kA
<g/>
'	'	kIx"
<g/>
-wen	-wen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejúspěšnějším	úspěšný	k2eAgMnPc3d3
atletům	atlet	k1gMnPc3
patří	patřit	k5eAaImIp3nS
běžkyně	běžkyně	k1gFnSc1
Wang	Wang	k1gInSc1
Ťün-sia	Ťün-sia	k1gFnSc1
a	a	k8xC
Sing	Sing	k1gInSc1
Chuej-na	Chuej-no	k1gNnSc2
<g/>
,	,	kIx,
překážkář	překážkář	k1gMnSc1
Liou	Lious	k1gInSc2
Siang	Siang	k1gMnSc1
či	či	k8xC
chodec	chodec	k1gMnSc1
Čchen	Čchna	k1gFnPc2
Ting	Ting	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
šachu	šach	k1gInSc6
po	po	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
září	září	k1gNnSc2
Chou	Chous	k1gInSc2
I-fan	I-fana	k1gFnPc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc7
předchůdkyní	předchůdkyně	k1gFnSc7
byla	být	k5eAaImAgFnS
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
Sie	Sie	k1gMnSc4
Ťün	Ťün	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čaj	čaj	k1gInSc1
Čchao	Čchao	k6eAd1
byla	být	k5eAaImAgNnP
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
vyhlášena	vyhlásit	k5eAaPmNgFnS
nejlepší	dobrý	k2eAgFnSc7d3
házenkářkou	házenkářka	k1gFnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
síně	síň	k1gFnSc2
slávy	sláva	k1gFnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
volejbalové	volejbalový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
Lang	Lang	k1gMnSc1
Pching	Pching	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zimní	zimní	k2eAgInPc4d1
sporty	sport	k1gInPc4
nebyly	být	k5eNaImAgFnP
v	v	k7c6
Číně	Čína	k1gFnSc6
dlouho	dlouho	k6eAd1
příliš	příliš	k6eAd1
populární	populární	k2eAgMnSc1d1
<g/>
,	,	kIx,
ani	ani	k8xC
známé	známý	k2eAgFnPc1d1
<g/>
,	,	kIx,
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
však	však	k9
Číňané	Číňan	k1gMnPc1
vrhli	vrhnout	k5eAaImAgMnP,k5eAaPmAgMnP
na	na	k7c4
rychlobruslení	rychlobruslení	k1gNnSc4
a	a	k8xC
zejména	zejména	k9
v	v	k7c6
disciplíně	disciplína	k1gFnSc6
zvané	zvaný	k2eAgInPc1d1
short-track	short-track	k6eAd1
dosahují	dosahovat	k5eAaImIp3nP
mimořádných	mimořádný	k2eAgInPc2d1
úspěchů	úspěch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wang	Wang	k1gMnSc1
Meng	Meng	k1gMnSc1
má	mít	k5eAaImIp3nS
čtyři	čtyři	k4xCgNnPc4
olympijská	olympijský	k2eAgNnPc4d1
zlata	zlato	k1gNnPc4
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc4
kolegyně	kolegyně	k1gFnSc1
Čou	Čou	k1gMnSc1
Jang	Jang	k1gMnSc1
tři	tři	k4xCgFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgFnPc7d1
"	"	kIx"
<g/>
zimními	zimní	k2eAgFnPc7d1
<g/>
"	"	kIx"
hvězdami	hvězda	k1gFnPc7
čínského	čínský	k2eAgInSc2d1
sportu	sport	k1gInSc2
jsou	být	k5eAaImIp3nP
manželé	manžel	k1gMnPc1
Šen	Šen	k1gMnSc1
Süe	Süe	k1gMnSc1
a	a	k8xC
Čao	čao	k0
Chung-po	Chung-pa	k1gFnSc5
<g/>
,	,	kIx,
olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
krasobruslařských	krasobruslařský	k2eAgFnPc2d1
sportovních	sportovní	k2eAgFnPc2d1
dvojic	dvojice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
i	i	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
asijských	asijský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
velkou	velký	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
mají	mít	k5eAaImIp3nP
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číňané	Číňan	k1gMnPc1
uspořádali	uspořádat	k5eAaPmAgMnP
své	svůj	k3xOyFgFnPc4
techniky	technika	k1gFnPc4
pod	pod	k7c4
značku	značka	k1gFnSc4
wu-šu	wu-sat	k5eAaPmIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
známým	známý	k2eAgMnPc3d1
učitelům	učitel	k1gMnPc3
patřil	patřit	k5eAaImAgInS
například	například	k6eAd1
Yip	Yip	k1gFnSc4
Man	mana	k1gFnPc2
<g/>
,	,	kIx,
k	k	k7c3
jehož	jehož	k3xOyRp3gMnPc3
žákům	žák	k1gMnPc3
patřil	patřit	k5eAaImAgMnS
i	i	k9
Bruce	Bruec	k1gInPc4
Lee	Lea	k1gFnSc3
<g/>
,	,	kIx,
rodilý	rodilý	k2eAgMnSc1d1
Američan	Američan	k1gMnSc1
s	s	k7c7
čínskými	čínský	k2eAgInPc7d1
kořeny	kořen	k1gInPc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
v	v	k7c6
USA	USA	kA
akčními	akční	k2eAgInPc7d1
filmy	film	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgFnSc1d1
čínská	čínský	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
žije	žít	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
slavným	slavný	k2eAgMnPc3d1
americkým	americký	k2eAgMnPc3d1
sportovcům	sportovec	k1gMnPc3
čínského	čínský	k2eAgInSc2d1
původu	původ	k1gInSc2
patří	patřit	k5eAaImIp3nS
tenista	tenista	k1gMnSc1
Michael	Michael	k1gMnSc1
Chang	Chang	k1gMnSc1
(	(	kIx(
<g/>
vítěz	vítěz	k1gMnSc1
Paříže	Paříž	k1gFnSc2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
basketbalista	basketbalista	k1gMnSc1
Jeremy	Jerema	k1gFnSc2
Lin	Lina	k1gFnPc2
<g/>
,	,	kIx,
plavec	plavec	k1gMnSc1
Nathan	Nathan	k1gMnSc1
Adrian	Adrian	k1gMnSc1
nebo	nebo	k8xC
krasobruslařka	krasobruslařka	k1gFnSc1
Michelle	Michelle	k1gFnSc1
Kwanová	Kwanová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kanadě	Kanada	k1gFnSc6
se	se	k3xPyFc4
zase	zase	k9
narodil	narodit	k5eAaPmAgMnS
Patrick	Patrick	k1gMnSc1
Chan	Chan	k1gMnSc1
<g/>
,	,	kIx,
olympijský	olympijský	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
v	v	k7c6
krasobruslení	krasobruslení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Čínským	čínský	k2eAgInSc7d1
vynálezem	vynález	k1gInSc7
je	být	k5eAaImIp3nS
desková	deskový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
go	go	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
nejlepšího	dobrý	k2eAgMnSc2d3
hráče	hráč	k1gMnSc2
go	go	k?
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
bývá	bývat	k5eAaImIp3nS
označován	označován	k2eAgMnSc1d1
čínský	čínský	k2eAgMnSc1d1
rodák	rodák	k1gMnSc1
Go	Go	k1gFnSc2
Seigen	Seigen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
2008	#num#	k4
Čína	Čína	k1gFnSc1
v	v	k7c6
Pekingu	Peking	k1gInSc6
uspořádala	uspořádat	k5eAaPmAgFnS
letní	letní	k2eAgFnSc1d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejné	stejný	k2eAgInPc1d1
město	město	k1gNnSc1
bude	být	k5eAaImBp3nS
roku	rok	k1gInSc2
2022	#num#	k4
hostit	hostit	k5eAaImF
i	i	k9
hry	hra	k1gFnPc1
zimní	zimní	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
<s>
Smog	smog	k1gInSc1
v	v	k7c6
Pekingu	Peking	k1gInSc6
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Uhelná	uhelný	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
v	v	k7c6
Pen-si	Pen-se	k1gFnSc6
<g/>
,	,	kIx,
provincie	provincie	k1gFnSc1
Liao-ning	Liao-ning	k1gInSc1
</s>
<s>
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
největším	veliký	k2eAgMnSc7d3
producentem	producent	k1gMnSc7
CO2	CO2	k1gMnSc7
na	na	k7c6
světě	svět	k1gInSc6
</s>
<s>
Zalesňování	zalesňování	k1gNnSc1
mladými	mladý	k2eAgFnPc7d1
rostlinami	rostlina	k1gFnPc7
v	v	k7c6
S	s	k7c7
<g/>
'	'	kIx"
<g/>
-ma-tchaj	-ma-tchaj	k1gMnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
projektu	projekt	k1gInSc2
Velké	velký	k2eAgFnSc2d1
zelené	zelený	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1
větrné	větrný	k2eAgFnPc1d1
turbíny	turbína	k1gFnPc1
k	k	k7c3
zásobování	zásobování	k1gNnSc3
elektřinou	elektřina	k1gFnSc7
výškové	výškový	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
v	v	k7c6
Ta	ten	k3xDgFnSc1
<g/>
-li	-li	k?
<g/>
,	,	kIx,
provincie	provincie	k1gFnSc2
Jün-nan	Jün-nana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Elektrotaxi	Elektrotaxe	k1gFnSc4
značky	značka	k1gFnSc2
BYD	BYD	kA
e	e	k0
<g/>
6	#num#	k4
v	v	k7c4
Šen-čenu	Šen-čen	k2eAgFnSc4d1
<g/>
,	,	kIx,
provincie	provincie	k1gFnPc4
Kuang-tung	Kuang-tunga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
velkém	velký	k2eAgInSc6d1
skoku	skok	k1gInSc6
vpřed	vpřed	k6eAd1
Čína	Čína	k1gFnSc1
dokonale	dokonale	k6eAd1
napodobila	napodobit	k5eAaPmAgFnS
industrializaci	industrializace	k1gFnSc4
západního	západní	k2eAgInSc2d1
světa	svět	k1gInSc2
–	–	k?
se	s	k7c7
všemi	všecek	k3xTgInPc7
jejími	její	k3xOp3gFnPc7
chybami	chyba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
hospodářský	hospodářský	k2eAgInSc1d1
vzestup	vzestup	k1gInSc1
byl	být	k5eAaImAgInS
doprovázen	doprovázen	k2eAgInSc1d1
rychlým	rychlý	k2eAgNnSc7d1
budováním	budování	k1gNnSc7
průmyslu	průmysl	k1gInSc2
a	a	k8xC
infrastruktury	infrastruktura	k1gFnSc2
<g/>
,	,	kIx,
čelí	čelit	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
vážným	vážný	k2eAgInPc3d1
environmentálním	environmentální	k2eAgInPc3d1
problémům	problém	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
180	#num#	k4
zemí	zem	k1gFnPc2
se	se	k3xPyFc4
Čína	Čína	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
umístila	umístit	k5eAaPmAgFnS
na	na	k7c4
109	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
v	v	k7c6
žebříčku	žebříček	k1gInSc6
Environmental	Environmental	k1gMnSc1
Performance	performance	k1gFnSc2
Index	index	k1gInSc1
(	(	kIx(
<g/>
Index	index	k1gInSc1
výkonnosti	výkonnost	k1gFnSc2
z	z	k7c2
hlediska	hledisko	k1gNnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ekologická	ekologický	k2eAgFnSc1d1
stopa	stopa	k1gFnSc1
Číny	Čína	k1gFnSc2
se	se	k3xPyFc4
od	od	k7c2
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejzávažnějším	závažný	k2eAgInSc7d3
problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
znečištění	znečištění	k1gNnSc4
ovzduší	ovzduší	k1gNnSc2
způsobené	způsobený	k2eAgNnSc1d1
spotřebou	spotřeba	k1gFnSc7
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čísla	číslo	k1gNnPc1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
jsou	být	k5eAaImIp3nP
zničující	zničující	k2eAgMnSc1d1
<g/>
:	:	kIx,
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
poprvé	poprvé	k6eAd1
Čína	Čína	k1gFnSc1
předstihla	předstihnout	k5eAaPmAgFnS
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
v	v	k7c6
celkových	celkový	k2eAgFnPc6d1
emisích	emise	k1gFnPc6
CO2	CO2	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
hodnoty	hodnota	k1gFnPc4
8,1	8,1	k4
miliardy	miliarda	k4xCgFnSc2
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
několika	několik	k4yIc6
městech	město	k1gNnPc6
severní	severní	k2eAgFnSc2d1
Číny	Čína	k1gFnSc2
naměřeno	naměřit	k5eAaBmNgNnS
rekordních	rekordní	k2eAgInPc2d1
více	hodně	k6eAd2
než	než	k8xS
800	#num#	k4
mikrogramů	mikrogram	k1gInPc2
pevných	pevný	k2eAgFnPc2d1
prachových	prachový	k2eAgFnPc2d1
částic	částice	k1gFnPc2
(	(	kIx(
<g/>
polétavého	polétavý	k2eAgInSc2d1
prachu	prach	k1gInSc2
<g/>
)	)	kIx)
na	na	k7c4
metr	metr	k1gInSc4
krychlový	krychlový	k2eAgInSc4d1
-	-	kIx~
30	#num#	k4
násobek	násobek	k1gInSc4
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
Světová	světový	k2eAgFnSc1d1
zdravotnická	zdravotnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
neškodnou	škodný	k2eNgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
velikost	velikost	k1gFnSc1
a	a	k8xC
důsledky	důsledek	k1gInPc4
škod	škoda	k1gFnPc2
na	na	k7c6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
nešlo	jít	k5eNaImAgNnS
přehlížet	přehlížet	k5eAaImF
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
otázka	otázka	k1gFnSc1
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
jednou	jeden	k4xCgFnSc7
z	z	k7c2
priorit	priorita	k1gFnPc2
čínské	čínský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
ekonomický	ekonomický	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
šetrný	šetrný	k2eAgInSc1d1
k	k	k7c3
životnímu	životní	k2eAgNnSc3d1
prostředí	prostředí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
snadný	snadný	k2eAgInSc4d1
úkol	úkol	k1gInSc4
<g/>
,	,	kIx,
uznává	uznávat	k5eAaImIp3nS
mj.	mj.	kA
i	i	k8xC
Světový	světový	k2eAgInSc1d1
fond	fond	k1gInSc1
na	na	k7c4
ochranu	ochrana	k1gFnSc4
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
WWF	WWF	kA
<g/>
)	)	kIx)
:	:	kIx,
Čína	Čína	k1gFnSc1
musí	muset	k5eAaImIp3nS
uživit	uživit	k5eAaPmF
téměř	téměř	k6eAd1
jednu	jeden	k4xCgFnSc4
pětinu	pětina	k1gFnSc4
světové	světový	k2eAgFnSc2d1
populace	populace	k1gFnSc2
-	-	kIx~
a	a	k8xC
to	ten	k3xDgNnSc1
s	s	k7c7
relativně	relativně	k6eAd1
omezenými	omezený	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
má	mít	k5eAaImIp3nS
pouze	pouze	k6eAd1
9	#num#	k4
<g/>
%	%	kIx~
světové	světový	k2eAgFnSc2d1
zemědělské	zemědělský	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
a	a	k8xC
pouze	pouze	k6eAd1
o	o	k7c4
něco	něco	k3yInSc4
více	hodně	k6eAd2
než	než	k8xS
6	#num#	k4
<g/>
%	%	kIx~
světových	světový	k2eAgInPc2d1
sladkovodních	sladkovodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
několika	několik	k4yIc6
čínských	čínský	k2eAgFnPc6d1
provinciích	provincie	k1gFnPc6
klesají	klesat	k5eAaImIp3nP
hladiny	hladina	k1gFnPc1
podzemních	podzemní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
způsobuje	způsobovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
zmizí	zmizet	k5eAaPmIp3nS
asi	asi	k9
30	#num#	k4
jezer	jezero	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgInSc2
stoupá	stoupat	k5eAaImIp3nS
eroze	eroze	k1gFnSc1
půdy	půda	k1gFnSc2
a	a	k8xC
desertifikace	desertifikace	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vede	vést	k5eAaImIp3nS
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
vegetace	vegetace	k1gFnSc2
<g/>
,	,	kIx,
polí	pole	k1gFnPc2
a	a	k8xC
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
průzkumů	průzkum	k1gInPc2
WWF	WWF	kA
je	být	k5eAaImIp3nS
však	však	k9
"	"	kIx"
<g/>
ekologická	ekologický	k2eAgFnSc1d1
stopa	stopa	k1gFnSc1
<g/>
"	"	kIx"
každého	každý	k3xTgMnSc2
z	z	k7c2
1,4	1,4	k4
miliardy	miliarda	k4xCgFnSc2
Číňanů	Číňan	k1gMnPc2
skromná	skromný	k2eAgFnSc1d1
-	-	kIx~
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
Japonskem	Japonsko	k1gNnSc7
<g/>
,	,	kIx,
Německem	Německo	k1gNnSc7
nebo	nebo	k8xC
dokonce	dokonce	k9
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
činila	činit	k5eAaImAgFnS
roční	roční	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
po	po	k7c6
elektrické	elektrický	k2eAgFnSc6d1
energii	energie	k1gFnSc6
na	na	k7c4
osobu	osoba	k1gFnSc4
v	v	k7c6
Číně	Čína	k1gFnSc6
3	#num#	k4
927,05	927,05	k4
kWh	kwh	kA
a	a	k8xC
v	v	k7c6
Německu	Německo	k1gNnSc6
7	#num#	k4
0	#num#	k4
<g/>
35,49	35,49	k4
kWh	kwh	kA
nebo	nebo	k8xC
emise	emise	k1gFnSc1
oxidu	oxid	k1gInSc2
uhličitého	uhličitý	k2eAgMnSc2d1
na	na	k7c4
obyvatele	obyvatel	k1gMnSc4
v	v	k7c6
Číně	Čína	k1gFnSc6
7,55	7,55	k4
tun	tuna	k1gFnPc2
a	a	k8xC
v	v	k7c6
Německu	Německo	k1gNnSc6
8,89	8,89	k4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zároveň	zároveň	k6eAd1
existuje	existovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
ekologických	ekologický	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
ve	v	k7c6
všech	všecek	k3xTgFnPc6
částech	část	k1gFnPc6
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
často	často	k6eAd1
založených	založený	k2eAgNnPc2d1
na	na	k7c6
iniciativě	iniciativa	k1gFnSc6
jednotlivců	jednotlivec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
ně	on	k3xPp3gNnSc4
patří	patřit	k5eAaImIp3nS
mj.	mj.	kA
"	"	kIx"
<g/>
Přátelé	přítel	k1gMnPc1
přírody	příroda	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Yunnan	Yunnan	k1gInSc4
Green	Green	k2eAgInSc4d1
Watersheds	Watersheds	k1gInSc4
<g/>
"	"	kIx"
nebo	nebo	k8xC
"	"	kIx"
<g/>
Green	Green	k2eAgInSc1d1
Earth	Earth	k1gInSc1
Volunteers	Volunteersa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
domácích	domácí	k2eAgMnPc2d1
aktivistů	aktivista	k1gMnPc2
mají	mít	k5eAaImIp3nP
v	v	k7c6
Číně	Čína	k1gFnSc6
zastoupení	zastoupení	k1gNnSc2
i	i	k8xC
různé	různý	k2eAgFnSc2d1
mezinárodní	mezinárodní	k2eAgFnSc2d1
nevládní	vládní	k2eNgFnSc2d1
organizace	organizace	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohé	k1gNnSc1
environmentální	environmentální	k2eAgInPc1d1
projekty	projekt	k1gInPc1
jsou	být	k5eAaImIp3nP
financovány	financovat	k5eAaBmNgInP
státem	stát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
tak	tak	k8xS,k8xC
např.	např.	kA
zřízením	zřízení	k1gNnSc7
"	"	kIx"
<g/>
Velké	velký	k2eAgFnPc1d1
zelené	zelený	k2eAgFnPc1d1
zdi	zeď	k1gFnPc1
<g/>
"	"	kIx"
realizuje	realizovat	k5eAaBmIp3nS
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
<g/>
)	)	kIx)
projekt	projekt	k1gInSc1
v	v	k7c6
zalesňování	zalesňování	k1gNnSc6
krajiny	krajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
do	do	k7c2
roku	rok	k1gInSc2
2050	#num#	k4
osazena	osadit	k5eAaPmNgFnS
původními	původní	k2eAgInPc7d1
druhy	druh	k1gInPc7
stromů	strom	k1gInPc2
plocha	plocha	k1gFnSc1
350	#num#	k4
000	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
cílem	cíl	k1gInSc7
vytvořit	vytvořit	k5eAaPmF
zelený	zelený	k2eAgInSc1d1
pás	pás	k1gInSc1
mezi	mezi	k7c7
suchým	suchý	k2eAgInSc7d1
severem	sever	k1gInSc7
a	a	k8xC
úrodným	úrodný	k2eAgInSc7d1
jihem	jih	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
čínská	čínský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
další	další	k2eAgNnSc1d1
opatření	opatření	k1gNnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c6
snížení	snížení	k1gNnSc6
emisí	emise	k1gFnPc2
na	na	k7c4
jednotku	jednotka	k1gFnSc4
HDP	HDP	kA
o	o	k7c4
33,8	33,8	k4
<g/>
%	%	kIx~
do	do	k7c2
roku	rok	k1gInSc2
2030	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
zákaz	zákaz	k1gInSc1
výstavby	výstavba	k1gFnSc2
nových	nový	k2eAgFnPc2d1
uhelných	uhelný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
byla	být	k5eAaImAgFnS
zavedena	zavést	k5eAaPmNgFnS
emisní	emisní	k2eAgFnSc1d1
norma	norma	k1gFnSc1
pro	pro	k7c4
výfukové	výfukový	k2eAgInPc4d1
plyny	plyn	k1gInPc4
Euro	euro	k1gNnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
podíl	podíl	k1gInSc1
uhlí	uhlí	k1gNnSc2
na	na	k7c6
celkové	celkový	k2eAgFnSc6d1
spotřebě	spotřeba	k1gFnSc6
energie	energie	k1gFnSc2
snížit	snížit	k5eAaPmF
do	do	k7c2
roku	rok	k1gInSc2
2030	#num#	k4
z	z	k7c2
66,6	66,6	k4
na	na	k7c4
méně	málo	k6eAd2
než	než	k8xS
45	#num#	k4
<g/>
%	%	kIx~
a	a	k8xC
podíl	podíl	k1gInSc4
obnovitelných	obnovitelný	k2eAgFnPc2d1
energií	energie	k1gFnPc2
ve	v	k7c6
stejném	stejný	k2eAgNnSc6d1
období	období	k1gNnSc6
zvýšit	zvýšit	k5eAaPmF
na	na	k7c6
25	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
opatření	opatření	k1gNnPc2
je	být	k5eAaImIp3nS
snížení	snížení	k1gNnSc1
znečištění	znečištění	k1gNnSc2
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
také	také	k9
významné	významný	k2eAgNnSc1d1
snížení	snížení	k1gNnSc1
emisí	emise	k1gFnPc2
skleníkových	skleníkový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
bylo	být	k5eAaImAgNnS
uzavřeno	uzavřít	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
1000	#num#	k4
uhelných	uhelný	k2eAgInPc2d1
dolů	dol	k1gInPc2
s	s	k7c7
celkovou	celkový	k2eAgFnSc7d1
roční	roční	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
těžby	těžba	k1gFnSc2
60	#num#	k4
milionů	milion	k4xCgInPc2
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
nejpozději	pozdě	k6eAd3
do	do	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
uzavřeny	uzavřen	k2eAgInPc4d1
nadbytečné	nadbytečný	k2eAgInPc4d1
doly	dol	k1gInPc4
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
500	#num#	k4
milionů	milion	k4xCgInPc2
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Čína	Čína	k1gFnSc1
ratifikovala	ratifikovat	k5eAaBmAgFnS
Kjótský	Kjótský	k2eAgInSc4d1
protokol	protokol	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
a	a	k8xC
Pařížskou	pařížský	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
o	o	k7c6
klimatu	klima	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záměrem	záměr	k1gInSc7
Číny	Čína	k1gFnSc2
je	být	k5eAaImIp3nS
stát	stát	k1gInSc1
světovým	světový	k2eAgMnSc7d1
lídrem	lídr	k1gMnSc7
v	v	k7c6
ochraně	ochrana	k1gFnSc6
klimatu	klima	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
jednotlivá	jednotlivý	k2eAgNnPc1d1
hlavní	hlavní	k2eAgNnPc1d1
čínská	čínský	k2eAgNnPc1d1
města	město	k1gNnPc1
bývají	bývat	k5eAaImIp3nP
nadále	nadále	k6eAd1
pravidelně	pravidelně	k6eAd1
zasahována	zasahovat	k5eAaImNgFnS
silným	silný	k2eAgInSc7d1
smogem	smog	k1gInSc7
<g/>
,	,	kIx,
zátěž	zátěž	k1gFnSc4
znečištěním	znečištění	k1gNnSc7
rok	rok	k1gInSc4
od	od	k7c2
roku	rok	k1gInSc2
klesá	klesat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgNnSc6d1
čtvrtletí	čtvrtletí	k1gNnSc6
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
údajů	údaj	k1gInPc2
Greenpeace	Greenpeace	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
znečištění	znečištění	k1gNnSc1
v	v	k7c6
Pekingu	Peking	k1gInSc6
pokleslo	poklesnout	k5eAaPmAgNnS
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
předchozím	předchozí	k2eAgInSc7d1
rokem	rok	k1gInSc7
o	o	k7c4
53,8	53,8	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
muselo	muset	k5eAaImAgNnS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
ukončit	ukončit	k5eAaPmF
činnost	činnost	k1gFnSc4
více	hodně	k6eAd2
než	než	k8xS
176	#num#	k4
000	#num#	k4
továren	továrna	k1gFnPc2
a	a	k8xC
44	#num#	k4
000	#num#	k4
uhelných	uhelný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
nesplňovaly	splňovat	k5eNaImAgInP
stanovené	stanovený	k2eAgInPc1d1
emisní	emisní	k2eAgInPc1d1
cíle	cíl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
zmizelo	zmizet	k5eAaPmAgNnS
v	v	k7c6
uhelném	uhelný	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
více	hodně	k6eAd2
než	než	k8xS
1,3	1,3	k4
milionu	milion	k4xCgInSc2
pracovních	pracovní	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
dalších	další	k2eAgNnPc2d1
500	#num#	k4
000	#num#	k4
v	v	k7c6
ocelářském	ocelářský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Čína	Čína	k1gFnSc1
investuje	investovat	k5eAaBmIp3nS
nemalé	malý	k2eNgInPc4d1
prostředky	prostředek	k1gInPc4
do	do	k7c2
nefosilních	fosilní	k2eNgInPc2d1
zdrojů	zdroj	k1gInPc2
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
uplynulých	uplynulý	k2eAgInPc6d1
letech	let	k1gInPc6
bylo	být	k5eAaImAgNnS
v	v	k7c6
Číně	Čína	k1gFnSc6
postaveno	postavit	k5eAaPmNgNnS
více	hodně	k6eAd2
solárních	solární	k2eAgMnPc2d1
a	a	k8xC
větrných	větrný	k2eAgFnPc2d1
turbín	turbína	k1gFnPc2
než	než	k8xS
ve	v	k7c6
zbytku	zbytek	k1gInSc6
světa	svět	k1gInSc2
dohromady	dohromady	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
vyčlenila	vyčlenit	k5eAaPmAgFnS
čínská	čínský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
další	další	k2eAgFnSc1d1
investice	investice	k1gFnSc1
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
přibližně	přibližně	k6eAd1
360	#num#	k4
miliard	miliarda	k4xCgFnPc2
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
pro	pro	k7c4
větrnou	větrný	k2eAgFnSc4d1
energii	energie	k1gFnSc4
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
by	by	kYmCp3nS
instalovaná	instalovaný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
měla	mít	k5eAaImAgFnS
vzrůst	vzrůst	k1gInSc4
ze	z	k7c2
151	#num#	k4
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
205	#num#	k4
gigawattů	gigawatt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
solární	solární	k2eAgFnSc4d1
energii	energie	k1gFnSc4
předpokládá	předpokládat	k5eAaImIp3nS
plán	plán	k1gInSc1
do	do	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
její	její	k3xOp3gNnSc1
ztrojnásobení	ztrojnásobení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínský	čínský	k2eAgInSc1d1
příklad	příklad	k1gInSc1
zároveň	zároveň	k6eAd1
dokazuje	dokazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
energetický	energetický	k2eAgInSc1d1
přechod	přechod	k1gInSc1
přináší	přinášet	k5eAaImIp3nS
i	i	k9
ekonomické	ekonomický	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
<g/>
:	:	kIx,
více	hodně	k6eAd2
než	než	k8xS
3,5	3,5	k4
milionu	milion	k4xCgInSc2
Číňanů	Číňan	k1gMnPc2
již	již	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
v	v	k7c6
odvětví	odvětví	k1gNnSc6
obnovitelných	obnovitelný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
energie	energie	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
10	#num#	k4
milionů	milion	k4xCgInPc2
se	se	k3xPyFc4
očekává	očekávat	k5eAaImIp3nS
do	do	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
masivně	masivně	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
dotacemi	dotace	k1gFnPc7
nástup	nástup	k1gInSc1
elektromobilů	elektromobil	k1gInPc2
a	a	k8xC
kvótami	kvóta	k1gFnPc7
a	a	k8xC
omezeními	omezení	k1gNnPc7
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
vytlačit	vytlačit	k5eAaPmF
spalovací	spalovací	k2eAgInPc4d1
motory	motor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
šesti	šest	k4xCc2
velkých	velký	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
zvláště	zvláště	k6eAd1
postiženy	postihnout	k5eAaPmNgFnP
znečištěním	znečištění	k1gNnSc7
ovzduší	ovzduší	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ročně	ročně	k6eAd1
(	(	kIx(
<g/>
losem	los	k1gInSc7
nebo	nebo	k8xC
aukcemi	aukce	k1gFnPc7
<g/>
)	)	kIx)
povoleno	povolit	k5eAaPmNgNnS
pouze	pouze	k6eAd1
650	#num#	k4
000	#num#	k4
registrací	registrace	k1gFnPc2
nových	nový	k2eAgInPc2d1
osobních	osobní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
vyhrazena	vyhradit	k5eAaPmNgFnS
výhradně	výhradně	k6eAd1
pro	pro	k7c4
registraci	registrace	k1gFnSc4
elektrických	elektrický	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektromobily	elektromobila	k1gFnPc1
nabízejí	nabízet	k5eAaImIp3nP
ze	z	k7c2
zákona	zákon	k1gInSc2
další	další	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
<g/>
:	:	kIx,
mohou	moct	k5eAaImIp3nP
jezdit	jezdit	k5eAaImF
sedm	sedm	k4xCc4
dní	den	k1gInPc2
v	v	k7c6
týdnu	týden	k1gInSc6
<g/>
;	;	kIx,
(	(	kIx(
<g/>
automobily	automobil	k1gInPc1
se	s	k7c7
spalovacími	spalovací	k2eAgInPc7d1
motory	motor	k1gInPc7
naproti	naproti	k7c3
tomu	ten	k3xDgMnSc3
mají	mít	k5eAaImIp3nP
na	na	k7c4
jeden	jeden	k4xCgInSc4
konkrétní	konkrétní	k2eAgInSc4d1
den	den	k1gInSc4
v	v	k7c6
týdnu	týden	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
posledním	poslední	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
SPZ	SPZ	kA
<g/>
,	,	kIx,
zákaz	zákaz	k1gInSc1
jízdy	jízda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nákupu	nákup	k1gInSc6
elektrického	elektrický	k2eAgNnSc2d1
nebo	nebo	k8xC
hybridního	hybridní	k2eAgNnSc2d1
elektrického	elektrický	k2eAgNnSc2d1
vozidla	vozidlo	k1gNnSc2
hradí	hradit	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
státní	státní	k2eAgFnSc4d1
prémii	prémie	k1gFnSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
4	#num#	k4
600	#num#	k4
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
musí	muset	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
domácí	domácí	k2eAgFnPc4d1
i	i	k8xC
zahraniční	zahraniční	k2eAgFnPc4d1
automobilky	automobilka	k1gFnPc4
dosáhnout	dosáhnout	k5eAaPmF
10	#num#	k4
<g/>
%	%	kIx~
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
12	#num#	k4
<g/>
%	%	kIx~
podílu	podíl	k1gInSc2
elektromobilů	elektromobil	k1gInPc2
na	na	k7c6
své	svůj	k3xOyFgFnSc6
celkové	celkový	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavedení	zavedení	k1gNnSc1
této	tento	k3xDgFnSc2
kvóty	kvóta	k1gFnSc2
vyvíjí	vyvíjet	k5eAaImIp3nS
značný	značný	k2eAgInSc1d1
tlak	tlak	k1gInSc1
na	na	k7c4
výrobce	výrobce	k1gMnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Volkswagen	volkswagen	k1gInSc4
<g/>
,	,	kIx,
BMW	BMW	kA
a	a	k8xC
Daimler	Daimler	k1gInSc1
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
zároveň	zároveň	k6eAd1
největším	veliký	k2eAgInSc7d3
jednotným	jednotný	k2eAgInSc7d1
trhem	trh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
indexu	index	k1gInSc2
elektrických	elektrický	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
,	,	kIx,
vyvinutým	vyvinutý	k2eAgNnSc7d1
společností	společnost	k1gFnSc7
McKinsey	McKinsea	k1gFnSc2
&	&	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
"	"	kIx"
<g/>
utíká	utíkat	k5eAaImIp3nS
svým	svůj	k3xOyFgMnPc3
konkurentům	konkurent	k1gMnPc3
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrická	elektrický	k2eAgFnSc1d1
vozidla	vozidlo	k1gNnPc1
prodaná	prodaný	k2eAgNnPc1d1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
:	:	kIx,
336	#num#	k4
000	#num#	k4
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
171	#num#	k4
000	#num#	k4
užitkových	užitkový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
a	a	k8xC
115	#num#	k4
000	#num#	k4
autobusů	autobus	k1gInPc2
na	na	k7c4
baterie	baterie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
je	být	k5eAaImIp3nS
navíc	navíc	k6eAd1
zemí	zem	k1gFnPc2
s	s	k7c7
nejrozmanitějšími	rozmanitý	k2eAgInPc7d3
modely	model	k1gInPc7
<g/>
;	;	kIx,
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
měli	mít	k5eAaImAgMnP
čínští	čínský	k2eAgMnPc1d1
zájemci	zájemce	k1gMnPc1
o	o	k7c6
koupi	koupě	k1gFnSc6
na	na	k7c4
výběr	výběr	k1gInSc4
mezi	mezi	k7c7
téměř	téměř	k6eAd1
60	#num#	k4
různými	různý	k2eAgInPc7d1
elektromodely	elektromodel	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Volksrepublik_China	Volksrepublik_China	k1gFnSc1
<g/>
#	#	kIx~
<g/>
Ökologie	Ökologie	k1gFnSc1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Celkem	celkem	k6eAd1
83,3	83,3	k4
%	%	kIx~
čínských	čínský	k2eAgFnPc2d1
žen	žena	k1gFnPc2
ve	v	k7c6
věku	věk	k1gInSc6
15	#num#	k4
až	až	k9
49	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
vdaných	vdaný	k2eAgFnPc2d1
či	či	k8xC
zadaných	zadaný	k2eAgFnPc2d1
<g/>
,	,	kIx,
používalo	používat	k5eAaImAgNnS
ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
2005	#num#	k4
moderní	moderní	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
antikoncepce	antikoncepce	k1gFnSc2
a	a	k8xC
tímto	tento	k3xDgInSc7
podílem	podíl	k1gInSc7
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
Čína	Čína	k1gFnSc1
na	na	k7c4
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Číňané	Číňan	k1gMnPc1
preferují	preferovat	k5eAaImIp3nP
mužské	mužský	k2eAgMnPc4d1
potomky	potomek	k1gMnPc4
a	a	k8xC
dívky	dívka	k1gFnPc4
se	se	k3xPyFc4
mnohdy	mnohdy	k6eAd1
stávají	stávat	k5eAaImIp3nP
ještě	ještě	k9
před	před	k7c7
svým	svůj	k3xOyFgNnSc7
narozením	narození	k1gNnSc7
oběťmi	oběť	k1gFnPc7
uměle	uměle	k6eAd1
vyvolaných	vyvolaný	k2eAgInPc2d1
potratů	potrat	k1gInPc2
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
přispěla	přispět	k5eAaPmAgFnS
i	i	k8xC
politika	politika	k1gFnSc1
jednoho	jeden	k4xCgNnSc2
dítěte	dítě	k1gNnSc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
omezit	omezit	k5eAaPmF
růst	růst	k1gInSc4
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
Číně	Čína	k1gFnSc6
v	v	k7c6
důsledku	důsledek	k1gInSc6
potratů	potrat	k1gInPc2
a	a	k8xC
zabíjení	zabíjení	k1gNnPc2
novorozeňat	novorozeně	k1gNnPc2
odhadem	odhad	k1gInSc7
o	o	k7c4
34	#num#	k4
milionů	milion	k4xCgInPc2
více	hodně	k6eAd2
mužů	muž	k1gMnPc2
než	než	k8xS
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
<g/>
Čínský	čínský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
a	a	k8xC
americký	americký	k2eAgMnSc1d1
vicepresident	vicepresident	k1gMnSc1
Joe	Joe	k1gMnSc1
Biden	Biden	k1gInSc4
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
čínskou	čínský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
pracuje	pracovat	k5eAaImIp3nS
50	#num#	k4
000	#num#	k4
až	až	k9
100	#num#	k4
000	#num#	k4
hackerů	hacker	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
164	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
bylo	být	k5eAaImAgNnS
odhaleno	odhalen	k2eAgNnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
že	že	k8xS
při	při	k7c6
největším	veliký	k2eAgInSc6d3
kyberútoku	kyberútok	k1gInSc6
v	v	k7c6
americké	americký	k2eAgFnSc6d1
historii	historie	k1gFnSc6
čínští	čínský	k2eAgMnPc1d1
hackeři	hacker	k1gMnPc1
ukradli	ukradnout	k5eAaPmAgMnP
osobní	osobní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
21,5	21,5	k4
milionu	milion	k4xCgInSc2
zaměstnanců	zaměstnanec	k1gMnPc2
amerických	americký	k2eAgInPc2d1
federálních	federální	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
166	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Číně	Čína	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
velice	velice	k6eAd1
přísná	přísný	k2eAgFnSc1d1
cenzura	cenzura	k1gFnSc1
na	na	k7c6
internetu	internet	k1gInSc6
a	a	k8xC
několik	několik	k4yIc1
desítek	desítka	k1gFnPc2
tisíc	tisíc	k4xCgInPc2
státních	státní	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
monitoruje	monitorovat	k5eAaImIp3nS
<g/>
,	,	kIx,
filtruje	filtrovat	k5eAaImIp3nS
a	a	k8xC
blokuje	blokovat	k5eAaImIp3nS
obsah	obsah	k1gInSc4
čínského	čínský	k2eAgInSc2d1
internetu	internet	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
167	#num#	k4
<g/>
]	]	kIx)
Čínští	čínský	k2eAgMnPc1d1
uživatelé	uživatel	k1gMnPc1
se	se	k3xPyFc4
nedostanou	dostat	k5eNaPmIp3nP
na	na	k7c4
Twitter	Twitter	k1gInSc4
<g/>
,	,	kIx,
Google	Google	k1gInSc4
nebo	nebo	k8xC
Facebook	Facebook	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americká	americký	k2eAgFnSc1d1
internetová	internetový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Facebook	Facebook	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
vyvinula	vyvinout	k5eAaPmAgFnS
cenzorský	cenzorský	k2eAgInSc4d1
software	software	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohla	moct	k5eAaImAgFnS
vrátit	vrátit	k5eAaPmF
na	na	k7c4
čínský	čínský	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
168	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čínský	čínský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
po	po	k7c6
svém	svůj	k3xOyFgInSc6
nástupu	nástup	k1gInSc6
k	k	k7c3
moci	moc	k1gFnSc3
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
spustil	spustit	k5eAaPmAgInS
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
protikorupční	protikorupční	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
potrestání	potrestání	k1gNnSc3
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
000	#num#	k4
stranických	stranický	k2eAgMnPc2d1
funkcionářů	funkcionář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
2016	#num#	k4
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
vlastní	vlastní	k2eAgMnSc1d1
švagr	švagr	k1gMnSc1
identifikován	identifikován	k2eAgMnSc1d1
v	v	k7c6
uniklých	uniklý	k2eAgInPc6d1
Panamských	panamský	k2eAgInPc6d1
dokumentech	dokument	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
obsahují	obsahovat	k5eAaImIp3nP
detailní	detailní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
daňových	daňový	k2eAgInPc6d1
únicích	únik	k1gInPc6
a	a	k8xC
praní	praní	k1gNnSc1
špinavých	špinavý	k2eAgInPc2d1
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
Veškerá	veškerý	k3xTgFnSc1
odhalení	odhalení	k1gNnSc4
Panamských	panamský	k2eAgInPc2d1
dokumentů	dokument	k1gInPc2
týkající	týkající	k2eAgInSc4d1
se	se	k3xPyFc4
rodiny	rodina	k1gFnPc4
Si	se	k3xPyFc3
Ťin-pchinga	Ťin-pchinga	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
čínském	čínský	k2eAgInSc6d1
internetu	internet	k1gInSc6
a	a	k8xC
v	v	k7c6
médiích	médium	k1gNnPc6
cenzurována	cenzurován	k2eAgMnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
170	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
CEFC	CEFC	kA
China	China	k1gFnSc1
Energy	Energ	k1gInPc4
Company	Compana	k1gFnSc2
Limited	limited	k2eAgMnPc2d1
získala	získat	k5eAaPmAgFnS
četná	četný	k2eAgNnPc4d1
aktiva	aktivum	k1gNnPc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
většinu	většina	k1gFnSc4
v	v	k7c4
Pivovary	pivovar	k1gInPc4
Lobkowicz	Lobkowicz	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
60	#num#	k4
<g/>
%	%	kIx~
podíl	podíl	k1gInSc1
ve	v	k7c6
fotbalovém	fotbalový	k2eAgInSc6d1
klubu	klub	k1gInSc6
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
nebo	nebo	k8xC
49	#num#	k4
<g/>
%	%	kIx~
podíl	podíl	k1gInSc1
v	v	k7c6
mediální	mediální	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
Empresa	Empresa	k1gFnSc1
Media	medium	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vlastní	vlastnit	k5eAaImIp3nS
TV	TV	kA
Barrandov	Barrandov	k1gInSc1
nebo	nebo	k8xC
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
týdeník	týdeník	k1gInSc1
Týden	týden	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
171	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
médiích	médium	k1gNnPc6
se	se	k3xPyFc4
spekulovalo	spekulovat	k5eAaImAgNnS
o	o	k7c6
možném	možný	k2eAgNnSc6d1
napojení	napojení	k1gNnSc6
firmy	firma	k1gFnSc2
CEFC	CEFC	kA
na	na	k7c4
čínskou	čínský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
a	a	k8xC
vojenskou	vojenský	k2eAgFnSc4d1
rozvědku	rozvědka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
172	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
173	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
2020	#num#	k4
byla	být	k5eAaImAgFnS
firma	firma	k1gFnSc1
prohlášena	prohlásit	k5eAaPmNgFnS
za	za	k7c7
zkrachovalou	zkrachovalý	k2eAgFnSc7d1
<g/>
[	[	kIx(
<g/>
174	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
Čína	Čína	k1gFnSc1
vlastnila	vlastnit	k5eAaImAgFnS
americké	americký	k2eAgInPc4d1
státní	státní	k2eAgInPc4d1
dluhopisy	dluhopis	k1gInPc4
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
1,244	1,244	k4
bilionu	bilion	k4xCgInSc2
USD	USD	kA
a	a	k8xC
byla	být	k5eAaImAgFnS
největším	veliký	k2eAgMnSc7d3
zahraničním	zahraniční	k2eAgMnSc7d1
věřitelem	věřitel	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
175	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okolo	okolo	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
začala	začít	k5eAaPmAgFnS
čínská	čínský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
Ujgurské	Ujgurský	k2eAgFnSc6d1
autonomní	autonomní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Sin-ťiang	Sin-ťianga	k1gFnPc2
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
budovat	budovat	k5eAaImF
převýchovné	převýchovný	k2eAgInPc4d1
tábory	tábor	k1gInPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgMnPc6,k3yIgMnPc6,k3yQgMnPc6
internovala	internovat	k5eAaBmAgFnS
více	hodně	k6eAd2
než	než	k8xS
milion	milion	k4xCgInSc4
muslimských	muslimský	k2eAgInPc2d1
Ujgurů	Ujgur	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
176	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čína	Čína	k1gFnSc1
plánuje	plánovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
od	od	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
spustí	spustit	k5eAaPmIp3nS
systém	systém	k1gInSc4
sociálního	sociální	k2eAgInSc2d1
kreditu	kredit	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
177	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
označen	označit	k5eAaPmNgInS
za	za	k7c4
nejpropracovanější	propracovaný	k2eAgInSc4d3
systém	systém	k1gInSc4
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
lidí	člověk	k1gMnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
178	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
–	–	k?
China	China	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Central	Central	k1gFnSc1
Inteligence	inteligence	k1gFnSc2
Agency	Agenca	k1gFnSc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017-09-24	2017-09-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jména	jméno	k1gNnSc2
států	stát	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
územních	územní	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
rozšířené	rozšířený	k2eAgNnSc1d1
a	a	k8xC
přepracované	přepracovaný	k2eAgNnSc1d1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zeměměřický	zeměměřický	k2eAgInSc1d1
a	a	k8xC
katastrální	katastrální	k2eAgMnSc1d1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
111	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86918	#num#	k4
<g/>
-	-	kIx~
<g/>
57	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Asie	Asie	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
33	#num#	k4
-	-	kIx~
34	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CN	CN	kA
<g/>
,	,	kIx,
ISO	ISO	kA
3166	#num#	k4
Country	country	k2eAgFnPc2d1
Codes	Codesa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnPc2
Organization	Organization	k1gInSc4
for	forum	k1gNnPc2
Standardization	Standardization	k1gInSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Devět	devět	k4xCc1
komentářů	komentář	k1gInPc2
ke	k	k7c3
komunistické	komunistický	k2eAgFnSc3d1
straně	strana	k1gFnSc3
<g/>
:	:	kIx,
Úvod	úvod	k1gInSc1
<g/>
.	.	kIx.
www	www	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
epochtimes	epochtimesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
Universum	universum	k1gNnSc4
<g/>
,	,	kIx,
všeobecná	všeobecný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
655	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
1062	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
262	#num#	k4
<g/>
,	,	kIx,
263	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
Merriam-Webster	Merriam-Webstra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Geographical	Geographical	k1gFnSc7
Dictionary	Dictionara	k1gFnSc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Springfield	Springfield	k1gInSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
U.	U.	kA
S.	S.	kA
A.	A.	kA
<g/>
:	:	kIx,
Merriam-Webster	Merriam-Webster	k1gMnSc1
<g/>
,	,	kIx,
Incorporporated	Incorporporated	k1gMnSc1
<g/>
,	,	kIx,
Publishers	Publishers	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
1361	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87779	#num#	k4
<g/>
-	-	kIx~
<g/>
546	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
247	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Japonci	Japonec	k1gMnPc1
zabili	zabít	k5eAaPmAgMnP
300	#num#	k4
tisíc	tisíc	k4xCgInSc4
lidí	člověk	k1gMnPc2
během	během	k7c2
šesti	šest	k4xCc2
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
si	se	k3xPyFc3
připomíná	připomínat	k5eAaImIp3nS
80	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
Nankingského	nankingský	k2eAgInSc2d1
masakru	masakr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Horší	horší	k1gNnSc1
než	než	k8xS
Mengele	Mengel	k1gInPc1
<g/>
:	:	kIx,
Japonci	Japonec	k1gMnPc1
při	při	k7c6
pokusech	pokus	k1gInPc6
zmasakrovali	zmasakrovat	k5eAaPmAgMnP
tisíce	tisíc	k4xCgInSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Před	před	k7c7
80	#num#	k4
lety	léto	k1gNnPc7
rozpoutali	rozpoutat	k5eAaPmAgMnP
Japonci	Japonec	k1gMnPc1
v	v	k7c6
čínském	čínský	k2eAgInSc6d1
Nankingu	Nanking	k1gInSc6
peklo	peklo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masakr	masakr	k1gInSc1
dodnes	dodnes	k6eAd1
šokuje	šokovat	k5eAaBmIp3nS
brutalitou	brutalita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Busky	Buska	k1gFnSc2
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
F.	F.	kA
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Communism	Communism	k1gMnSc1
in	in	k?
History	Histor	k1gInPc1
and	and	k?
Theory	Theora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Greenwood	Greenwood	k1gInSc1
Publishing	Publishing	k1gInSc4
Group	Group	k1gInSc4
<g/>
.	.	kIx.
p.	p.	k?
<g/>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
<g/>
,	,	kIx,
Mao	Mao	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Great	Great	k1gInSc1
Leap	Leap	k1gMnSc1
Forward	Forward	k1gMnSc1
'	'	kIx"
<g/>
killed	killed	k1gMnSc1
45	#num#	k4
million	million	k1gInSc1
in	in	k?
four	four	k1gInSc1
years	years	k1gInSc1
<g/>
↑	↑	k?
9	#num#	k4
komentářů	komentář	k1gInPc2
ke	k	k7c3
komunistické	komunistický	k2eAgFnSc3d1
straně	strana	k1gFnSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Epoch	epocha	k1gFnPc2
Times	Timesa	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Část	část	k1gFnSc1
VII	VII	kA
<g/>
,	,	kIx,
Historie	historie	k1gFnSc1
zabíjení	zabíjení	k1gNnSc2
Čínské	čínský	k2eAgFnSc2d1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
–	–	k?
OD	od	k7c2
VELKÉ	velký	k2eAgFnSc2d1
KULTURNÍ	kulturní	k2eAgFnSc2d1
REVOLUCE	revoluce	k1gFnSc2
PŘES	přes	k7c4
MASAKR	masakr	k1gInSc4
NA	na	k7c4
NÁMĚSTÍ	náměstí	k1gNnSc4
TCHIEN-AN-MEN	TCHIEN-AN-MEN	k1gFnSc1
k	k	k7c3
Falun	Falun	k1gMnSc1
Gongu	gong	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FEIGON	FEIGON	kA
<g/>
,	,	kIx,
Lee	Lea	k1gFnSc6
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mao	Mao	k1gFnSc1
<g/>
:	:	kIx,
A	a	k9
Reinterpretation	Reinterpretation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
R.	R.	kA
Dee	Dee	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
56663	#num#	k4
<g/>
-	-	kIx~
<g/>
458	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
:	:	kIx,
"	"	kIx"
<g/>
By	by	k9
1952	#num#	k4
they	they	k1gInPc7
had	had	k1gMnSc1
extended	extended	k1gMnSc1
land	landa	k1gFnPc2
reform	reform	k1gInSc1
throughout	throughout	k1gMnSc1
the	the	k?
countryside	countrysid	k1gMnSc5
<g/>
,	,	kIx,
but	but	k?
in	in	k?
the	the	k?
process	process	k6eAd1
somewhere	somewhrat	k5eAaPmIp3nS
between	between	k1gInSc1
two	two	k?
and	and	k?
five	fivat	k5eAaPmIp3nS
million	million	k1gInSc4
landlords	landlordsa	k1gFnPc2
had	had	k1gMnSc1
been	been	k1gMnSc1
killed	killed	k1gMnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
↑	↑	k?
LIU	LIU	kA
<g/>
,	,	kIx,
Jenny	Jenna	k1gFnSc2
<g/>
;	;	kIx,
KAJÍNEK	KAJÍNEK	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konfuciův	Konfuciův	k2eAgInSc4d1
institut	institut	k1gInSc4
-	-	kIx~
nový	nový	k2eAgInSc4d1
kabát	kabát	k1gInSc4
Čínské	čínský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
YULIN	YULIN	kA
<g/>
,	,	kIx,
Lin	Lina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalajláma	dalajláma	k1gMnSc1
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
vzpoura	vzpoura	k1gFnSc1
ve	v	k7c6
Lhase	Lhasa	k1gFnSc6
byla	být	k5eAaImAgFnS
zinscenována	zinscenovat	k5eAaPmNgFnS
čínskou	čínský	k2eAgFnSc7d1
policií	policie	k1gFnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Epocha	epocha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Soubor	soubor	k1gInSc1
článků	článek	k1gInPc2
o	o	k7c4
Kao	Kao	k1gFnSc4
Č	Č	kA
<g/>
'	'	kIx"
<g/>
šengovi	šengův	k2eAgMnPc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Epocha	epocha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Č	Č	kA
<g/>
'	'	kIx"
<g/>
-ŠENG	-ŠENG	k?
<g/>
,	,	kIx,
Kao	Kao	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černá	černý	k2eAgFnSc1d1
noc	noc	k1gFnSc1
čínského	čínský	k2eAgMnSc2d1
právníka	právník	k1gMnSc2
Kao	Kao	k1gFnSc2
Č	Č	kA
<g/>
'	'	kIx"
<g/>
-šenga	-šenga	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Epocha	epocha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Téma	téma	k1gNnSc4
–	–	k?
čínští	čínský	k2eAgMnPc1d1
právníci	právník	k1gMnPc1
hájí	hájit	k5eAaImIp3nP
Falun	Falun	k1gInSc4
Gong	gong	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Epocha	epocha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KRÁL	Král	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
<g/>
:	:	kIx,
Ekologická	ekologický	k2eAgFnSc1d1
bomba	bomba	k1gFnSc1
tiká	tikat	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Epocha	epocha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lidská	lidský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
v	v	k7c6
Číně	Čína	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amnesty	Amnest	k1gInPc4
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FEUERBERG	FEUERBERG	kA
<g/>
,	,	kIx,
Gary	Gary	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoboda	svoboda	k1gFnSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Epoch	epocha	k1gFnPc2
Times	Timesa	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
HORÁLEK	Horálek	k1gMnSc1
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geografie	geografie	k1gFnSc1
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učební	učební	k2eAgInSc1d1
materiál	materiál	k1gInSc1
pro	pro	k7c4
studenty	student	k1gMnPc4
sinologie	sinologie	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Katedra	katedra	k1gFnSc1
asijských	asijský	k2eAgFnPc2d1
studií	studie	k1gFnPc2
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Palackého	Palacký	k1gMnSc2
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
244	#num#	k4
<g/>
-	-	kIx~
<g/>
3839	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
East	East	k1gMnSc1
Asia	Asia	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Southeast	Southeast	k1gMnSc1
Asia	Asia	k1gMnSc1
::	::	k?
China	China	k1gFnSc1
—	—	k?
The	The	k1gFnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
Central	Central	k1gFnSc1
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
.	.	kIx.
www.cia.gov	www.cia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-09-14	2020-09-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
North	North	k1gMnSc1
America	America	k1gMnSc1
::	::	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
—	—	k?
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
Central	Central	k1gFnSc1
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
.	.	kIx.
www.cia.gov	www.cia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-09-20	2020-09-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tisíce	tisíc	k4xCgInPc1
vojáků	voják	k1gMnPc2
se	se	k3xPyFc4
na	na	k7c6
indicko-čínské	indicko-čínský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
drží	držet	k5eAaImIp3nS
v	v	k7c6
šachu	šach	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozí	hrozit	k5eAaImIp3nS
obnovení	obnovení	k1gNnSc4
starého	starý	k2eAgInSc2d1
sporu	spor	k1gInSc2
|	|	kIx~
Svět	svět	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-07-11	2017-07-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tádžikové	Tádžik	k1gMnPc1
předají	předat	k5eAaPmIp3nP
Číně	Čína	k1gFnSc3
území	území	k1gNnSc2
dvakrát	dvakrát	k6eAd1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
rozloha	rozloha	k1gFnSc1
Prahy	Praha	k1gFnSc2
|	|	kIx~
EuroZprávy	EuroZpráva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
eurozpravy	eurozprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rusko	Rusko	k1gNnSc1
a	a	k8xC
Čína	Čína	k1gFnSc1
si	se	k3xPyFc3
konečně	konečně	k6eAd1
vyjasnily	vyjasnit	k5eAaPmAgInP
spory	spor	k1gInPc1
o	o	k7c4
společnou	společný	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
-	-	kIx~
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Čína	Čína	k1gFnSc1
nemá	mít	k5eNaImIp3nS
podle	podle	k7c2
soudu	soud	k1gInSc2
v	v	k7c6
Haagu	Haag	k1gInSc6
nárok	nárok	k1gInSc4
na	na	k7c4
ostrovy	ostrov	k1gInPc4
v	v	k7c6
Jihočínském	jihočínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Výkaly	výkal	k1gInPc7
a	a	k8xC
odpadky	odpadek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
zavřela	zavřít	k5eAaPmAgFnS
základní	základní	k2eAgInSc4d1
tábor	tábor	k1gInSc4
u	u	k7c2
Mount	Mounta	k1gFnPc2
Everestu	Everest	k1gInSc2
pro	pro	k7c4
turisty	turist	k1gMnPc4
bez	bez	k7c2
povolení	povolení	k1gNnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Největší	veliký	k2eAgNnSc1d3
čínské	čínský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
může	moct	k5eAaImIp3nS
do	do	k7c2
200	#num#	k4
let	léto	k1gNnPc2
zcela	zcela	k6eAd1
vyschnout	vyschnout	k5eAaPmF
<g/>
.	.	kIx.
aktualne	aktualnout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
centrum	centrum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Čtyři	čtyři	k4xCgInPc1
základní	základní	k2eAgInPc1d1
principy	princip	k1gInPc1
<g/>
,	,	kIx,
Sinopsis	Sinopsis	k1gFnPc1
<g/>
↑	↑	k?
Chang	Chang	k1gMnSc1
<g/>
,	,	kIx,
Eddy	Edda	k1gFnPc1
(	(	kIx(
<g/>
22	#num#	k4
August	August	k1gMnSc1
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perseverance	perseverance	k1gFnSc1
will	willa	k1gFnPc2
pay	pay	k?
off	off	k?
at	at	k?
the	the	k?
UN	UN	kA
Archivováno	archivován	k2eAgNnSc4d1
6	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
The	The	k1gMnSc1
Taipei	Taipe	k1gFnSc2
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
China	China	k1gFnSc1
says	says	k6eAd1
communication	communication	k1gInSc1
with	witha	k1gFnPc2
other	othra	k1gFnPc2
developing	developing	k1gInSc1
countries	countries	k1gMnSc1
at	at	k?
Copenhagen	Copenhagen	k1gInSc1
summit	summit	k1gInSc1
transparent	transparent	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
People	People	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Daily	Dail	k1gInPc7
<g/>
.	.	kIx.
21	#num#	k4
December	December	k1gInSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
January	Januara	k1gFnSc2
2019	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
BRICS	BRICS	kA
summit	summit	k1gInSc1
ends	ends	k1gInSc1
in	in	k?
China	China	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
.	.	kIx.
14	#num#	k4
April	April	k1gInSc1
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
24	#num#	k4
October	Octobero	k1gNnPc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Taiwan	Taiwan	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Ma	Ma	k1gFnSc7
to	ten	k3xDgNnSc4
stopover	stopover	k1gMnSc1
in	in	k?
US	US	kA
<g/>
:	:	kIx,
report	report	k1gInSc1
<g/>
.	.	kIx.
mysinchew	mysinchew	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
12	#num#	k4
January	Januara	k1gFnSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
9	#num#	k4
September	September	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Macartney	Macartnea	k1gFnSc2
<g/>
,	,	kIx,
Jane	Jan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
China	China	k1gFnSc1
says	saysa	k1gFnPc2
US	US	kA
arms	arms	k1gInSc1
sales	sales	k1gInSc1
to	ten	k3xDgNnSc4
Taiwan	Taiwan	k1gInSc1
could	could	k6eAd1
threaten	threaten	k2eAgInSc1d1
wider	wider	k1gInSc1
relations	relationsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
1	#num#	k4
February	Februara	k1gFnSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KEITH	KEITH	kA
<g/>
,	,	kIx,
Ronald	Ronald	k1gMnSc1
C.	C.	kA
China	China	k1gFnSc1
from	from	k1gInSc1
the	the	k?
inside	insid	k1gInSc5
out	out	k?
–	–	k?
fitting	fitting	k1gInSc1
the	the	k?
People	People	k1gFnSc1
<g/>
'	'	kIx"
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
s	s	k7c7
republic	republice	k1gFnPc2
into	into	k1gNnSc1
the	the	k?
world	world	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
PlutoPress	PlutoPress	k1gInSc1
S.	S.	kA
135	#num#	k4
<g/>
–	–	k?
<g/>
136	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
An	An	k1gMnSc1
Authoritarian	Authoritarian	k1gMnSc1
Axis	Axisa	k1gFnPc2
Rising	Rising	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Diplomat	diplomat	k1gMnSc1
<g/>
.	.	kIx.
29	#num#	k4
June	jun	k1gMnSc5
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
16	#num#	k4
December	December	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
China	China	k1gFnSc1
<g/>
,	,	kIx,
Russia	Russia	k1gFnSc1
launch	launch	k1gMnSc1
largest	largest	k1gMnSc1
ever	ever	k1gMnSc1
joint	joint	k1gMnSc1
military	militara	k1gFnSc2
exercise	exercise	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
5	#num#	k4
July	Jula	k1gFnSc2
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Energy	Energ	k1gInPc1
to	ten	k3xDgNnSc1
dominate	dominat	k1gInSc5
Russia	Russius	k1gMnSc2
President	president	k1gMnSc1
Putin	putin	k2eAgMnSc1d1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
China	China	k1gFnSc1
visit	visita	k1gFnPc2
<g/>
.	.	kIx.
www.bbc.co.uk	www.bbc.co.uka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
,	,	kIx,
5	#num#	k4
June	jun	k1gMnSc5
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GLADSTONE	GLADSTONE	kA
<g/>
,	,	kIx,
Rick	Rick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Friction	Friction	k1gInSc1
at	at	k?
the	the	k?
U.	U.	kA
<g/>
N.	N.	kA
as	as	k9
Russia	Russia	k1gFnSc1
and	and	k?
China	China	k1gFnSc1
Veto	veto	k1gNnSc1
Another	Anothra	k1gFnPc2
Resolution	Resolution	k1gInSc1
on	on	k3xPp3gMnSc1
Syria	Syrius	k1gMnSc4
Sanctions	Sanctions	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
19	#num#	k4
July	Jula	k1gFnSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
15	#num#	k4
November	November	k1gInSc1
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Xi	Xi	k1gFnSc1
Jinping	Jinping	k1gInSc1
<g/>
:	:	kIx,
Russia-China	Russia-China	k1gFnSc1
ties	ties	k1gInSc1
'	'	kIx"
<g/>
guarantee	guaranteat	k5eAaPmIp3nS
world	world	k1gMnSc1
peace	peace	k1gMnSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
www.bbc.co.uk	www.bbc.co.uk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
,	,	kIx,
23	#num#	k4
March	March	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
23	#num#	k4
March	March	k1gInSc1
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chinese	Chinese	k1gFnSc2
Civil	civil	k1gMnSc1
War	War	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cultural-China	Cultural-China	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
12	#num#	k4
September	September	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
China	China	k1gFnSc1
denies	denies	k1gMnSc1
preparing	preparing	k1gInSc1
war	war	k?
over	over	k1gInSc1
South	South	k1gInSc1
China	China	k1gFnSc1
Sea	Sea	k1gMnSc1
shoal	shoal	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
.	.	kIx.
12	#num#	k4
May	May	k1gMnSc1
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Q	Q	kA
<g/>
&	&	k?
<g/>
A	A	kA
<g/>
:	:	kIx,
China-Japan	China-Japan	k1gMnSc1
islands	islandsa	k1gFnPc2
row	row	k?
<g/>
.	.	kIx.
www.bbc.co.uk	www.bbc.co.uk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
27	#num#	k4
November	November	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dean	Dean	k1gInSc1
Sterling	sterling	k1gInSc1
Jones	Jones	k1gInSc4
<g/>
,	,	kIx,
A	a	k8xC
British	British	k1gMnSc1
Newspaper	Newspaper	k1gMnSc1
Has	hasit	k5eAaImRp2nS
Given	Givna	k1gFnPc2
Chinese	Chinese	k1gFnSc1
Coronavirus	Coronavirus	k1gInSc1
Propaganda	propaganda	k1gFnSc1
A	a	k8xC
Direct	Direct	k1gInSc1
Line	linout	k5eAaImIp3nS
To	ten	k3xDgNnSc1
The	The	k1gFnSc1
UK	UK	kA
<g/>
,	,	kIx,
BuzzFeed	BuzzFeed	k1gInSc4
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
8.4	8.4	k4
<g/>
.2020	.2020	k4
↑	↑	k?
Louisa	Louisa	k?
Lim	limo	k1gNnPc2
and	and	k?
Julia	Julius	k1gMnSc2
Bergin	Bergin	k1gInSc4
<g/>
,	,	kIx,
Inside	Insid	k1gInSc5
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
audacious	audacious	k1gInSc1
global	globat	k5eAaImAgInS
propaganda	propaganda	k1gFnSc1
campaign	campaign	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beijing	Beijing	k1gInSc1
is	is	k?
buying	buying	k1gInSc1
up	up	k?
media	medium	k1gNnSc2
outlets	outletsa	k1gFnPc2
and	and	k?
training	training	k1gInSc1
scores	scores	k1gMnSc1
of	of	k?
foreign	foreign	k1gMnSc1
journalists	journalists	k6eAd1
to	ten	k3xDgNnSc1
‘	‘	k?
<g/>
tell	tell	k1gInSc1
China	China	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
story	story	k1gFnSc7
well	wella	k1gFnPc2
<g/>
’	’	k?
–	–	k?
as	as	k1gNnSc2
part	parta	k1gFnPc2
of	of	k?
a	a	k8xC
worldwide	worldwid	k1gInSc5
propaganda	propaganda	k1gFnSc1
campaign	campaign	k1gInSc1
of	of	k?
astonishing	astonishing	k1gInSc1
scope	scopat	k5eAaPmIp3nS
and	and	k?
ambition	ambition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
7.12	7.12	k4
<g/>
.2018	.2018	k4
<g/>
↑	↑	k?
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Influence	influence	k1gFnPc4
&	&	k?
American	American	k1gInSc1
Interests	Interests	k1gInSc1
<g/>
:	:	kIx,
Promoting	Promoting	k1gInSc1
Constructive	Constructiv	k1gInSc5
Vigilance	Vigilance	k1gFnSc5
<g/>
,	,	kIx,
Hoover	Hoover	k1gMnSc1
Institution	Institution	k1gInSc1
<g/>
,	,	kIx,
29.11	29.11	k4
<g/>
.2018	.2018	k4
<g/>
↑	↑	k?
Brian	Brian	k1gMnSc1
Bennett	Bennett	k1gMnSc1
<g/>
,	,	kIx,
China	China	k1gFnSc1
Announces	Announces	k1gMnSc1
Expulsion	Expulsion	k1gInSc1
of	of	k?
U.	U.	kA
<g/>
S.	S.	kA
Journalists	Journalistsa	k1gFnPc2
<g/>
,	,	kIx,
TIME	TIME	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
17.3	17.3	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Německá	německý	k2eAgFnSc1d1
kontrarozvědka	kontrarozvědka	k1gFnSc1
varuje	varovat	k5eAaImIp3nS
před	před	k7c7
poskytováním	poskytování	k1gNnSc7
dat	datum	k1gNnPc2
Číňanům	Číňan	k1gMnPc3
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
9.7	9.7	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
China	China	k1gFnSc1
is	is	k?
greatest	greatest	k1gInSc4
threat	threat	k5eAaPmF,k5eAaBmF,k5eAaImF
to	ten	k3xDgNnSc4
freedom	freedom	k1gInSc1
-	-	kIx~
US	US	kA
intelligence	intelligence	k1gFnSc1
chief	chief	k1gInSc1
<g/>
,	,	kIx,
BBC	BBC	kA
News	News	k1gInSc1
<g/>
,	,	kIx,
4.12	4.12	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
FBI	FBI	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Čína	Čína	k1gFnSc1
vydírá	vydírat	k5eAaImIp3nS
své	svůj	k3xOyFgMnPc4
občany	občan	k1gMnPc4
žijící	žijící	k2eAgMnPc1d1
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
zadrží	zadržet	k5eAaPmIp3nS
jejich	jejich	k3xOp3gNnSc1
blízké	blízký	k2eAgNnSc1d1
a	a	k8xC
nutí	nutit	k5eAaImIp3nS
je	on	k3xPp3gNnSc4
k	k	k7c3
návratu	návrat	k1gInSc3
<g/>
,	,	kIx,
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
7.7	7.7	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Lukáš	Lukáš	k1gMnSc1
Valášek	Valášek	k1gMnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
Truchlá	Truchlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
“	“	k?
<g/>
Four	Four	k1gInSc1
Million	Million	k1gInSc1
for	forum	k1gNnPc2
Vystrčil	vystrčit	k5eAaPmAgMnS
<g/>
”	”	k?
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Chinese	Chinese	k1gFnSc1
Attempt	Attempta	k1gFnPc2
at	at	k?
Disparaging	Disparaging	k1gInSc1
President	president	k1gMnSc1
of	of	k?
Czech	Czech	k1gMnSc1
senate	senat	k1gMnSc5
<g/>
,	,	kIx,
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
11.11	11.11	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Taiwan	Taiwan	k1gMnSc1
condemns	condemns	k6eAd1
Chinese	Chinese	k1gFnSc2
allegations	allegations	k6eAd1
it	it	k?
paid	paid	k1gMnSc1
Czech	Czech	k1gMnSc1
Senate	Senat	k1gInSc5
president	president	k1gMnSc1
<g/>
,	,	kIx,
Taiwan	Taiwan	k1gMnSc1
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
12.11	12.11	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Bernhard	Bernhard	k1gMnSc1
Odehnal	Odehnal	k1gMnSc1
<g/>
,	,	kIx,
Wie	Wie	k1gFnSc1
ein	ein	k?
Zürcher	Zürchra	k1gFnPc2
Geschäftsmann	Geschäftsmann	k1gMnSc1
für	für	k?
China	China	k1gFnSc1
geheimes	geheimes	k1gInSc1
Lobbying	Lobbying	k1gInSc1
betreibt	betreibt	k1gInSc1
<g/>
,	,	kIx,
Tagesanzeiger	Tagesanzeiger	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
13.11	13.11	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Lukáš	Lukáš	k1gMnSc1
Valášek	Valášek	k1gMnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
Truchlá	Truchlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Propaganda	propaganda	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgNnPc1d1
média	médium	k1gNnPc1
odhalila	odhalit	k5eAaPmAgFnS
úplatky	úplatek	k1gInPc4
pro	pro	k7c4
Vystrčila	Vystrčil	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důkaz	důkaz	k1gInSc4
opaku	opak	k1gInSc2
Čína	Čína	k1gFnSc1
cenzuruje	cenzurovat	k5eAaImIp3nS
<g/>
,	,	kIx,
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
19.11	19.11	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Ben	Ben	k1gInSc1
Westcott	Westcott	k2eAgInSc1d1
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
Jiang	Jiang	k1gInSc1
<g/>
,	,	kIx,
Chinese	Chinese	k1gFnSc1
diplomat	diplomat	k1gMnSc1
promotes	promotes	k1gMnSc1
conspiracy	conspiraca	k1gFnSc2
theory	theora	k1gFnSc2
that	that	k1gMnSc1
US	US	kA
military	militara	k1gFnPc1
brought	brought	k1gMnSc1
coronavirus	coronavirus	k1gMnSc1
to	ten	k3xDgNnSc1
Wuhan	Wuhan	k1gInSc4
<g/>
,	,	kIx,
CNN	CNN	kA
<g/>
,	,	kIx,
March	March	k1gInSc1
13	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
↑	↑	k?
Chen	Chen	k1gInSc1
Dingding	Dingding	k1gInSc1
<g/>
,	,	kIx,
Hu	hu	k0
Junyang	Junyang	k1gMnSc1
<g/>
,	,	kIx,
Is	Is	k1gMnSc1
China	China	k1gFnSc1
Really	Realla	k1gFnSc2
Embracing	Embracing	k1gInSc1
‘	‘	k?
<g/>
Wolf	Wolf	k1gMnSc1
Warrior	Warrior	k1gMnSc1
<g/>
’	’	k?
Diplomacy	Diplomaca	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
The	The	k1gMnSc1
Diplomat	diplomat	k1gMnSc1
<g/>
,	,	kIx,
9.9	9.9	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Tom	Tom	k1gMnSc1
Burt	Burt	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc5
nation-state	nation-stat	k1gMnSc5
cyberattacks	cyberattacks	k1gInSc1
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
2.3	2.3	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Microsoft	Microsoft	kA
hack	hack	k1gInSc4
<g/>
:	:	kIx,
White	Whit	k1gMnSc5
House	house	k1gNnSc1
warns	warns	k1gInSc1
of	of	k?
'	'	kIx"
<g/>
active	activat	k5eAaPmIp3nS
threat	threat	k1gInSc1
<g/>
'	'	kIx"
of	of	k?
email	email	k1gInSc1
attack	attack	k1gInSc1
<g/>
,	,	kIx,
BBC	BBC	kA
News	News	k1gInSc1
<g/>
,	,	kIx,
6.3	6.3	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
Miloslav	Miloslav	k1gMnSc1
Fišer	Fišer	k1gMnSc1
<g/>
,	,	kIx,
Experti	expert	k1gMnPc1
varovali	varovat	k5eAaImAgMnP
před	před	k7c4
útoky	útok	k1gInPc4
hackerů	hacker	k1gMnPc2
s	s	k7c7
předstihem	předstih	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
ale	ale	k9
marné	marný	k2eAgNnSc1d1
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
5.3	5.3	k4
<g/>
.2021	.2021	k4
<g/>
↑	↑	k?
The	The	k1gMnSc1
new	new	k?
generals	generals	k1gInSc1
in	in	k?
charge	chargat	k5eAaPmIp3nS
of	of	k?
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
guns	guns	k6eAd1
<g/>
.	.	kIx.
www.bbc.co.uk	www.bbc.co.uk	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
,	,	kIx,
14	#num#	k4
November	November	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
10	#num#	k4
December	December	k1gInSc1
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mar	Mar	k1gFnSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
:	:	kIx,
Deciphering	Deciphering	k1gInSc1
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
latest	latest	k5eAaPmF
defence	defence	k1gFnSc1
budget	budget	k1gInSc4
figures	figures	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SIPRI	SIPRI	kA
<g/>
,	,	kIx,
March	March	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
9	#num#	k4
February	Februara	k1gFnSc2
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Annual	Annual	k1gInSc1
Report	report	k1gInSc4
To	to	k9
Congress	Congress	k1gInSc4
–	–	k?
Military	Militara	k1gFnSc2
Power	Power	k1gMnSc1
of	of	k?
the	the	k?
People	People	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Republic	Republice	k1gFnPc2
of	of	k?
China	China	k1gFnSc1
2009	#num#	k4
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Defenselink	Defenselink	k1gInSc4
<g/>
.	.	kIx.
<g/>
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
27	#num#	k4
November	Novembero	k1gNnPc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nolt	Nolt	k1gMnSc1
<g/>
,	,	kIx,
James	James	k1gMnSc1
H.	H.	kA
Analysis	Analysis	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
China-Taiwan	China-Taiwany	k1gInPc2
military	militara	k1gFnSc2
balance	balanc	k1gFnSc2
Archivováno	archivovat	k5eAaBmNgNnS
1	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asia	Asia	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
15	#num#	k4
April	Aprila	k1gFnPc2
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Military	Militara	k1gFnSc2
and	and	k?
Security	Securita	k1gFnSc2
Developments	Developmentsa	k1gFnPc2
Involving	Involving	k1gInSc1
the	the	k?
People	People	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Republic	Republice	k1gFnPc2
of	of	k?
China	China	k1gFnSc1
2013	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
Secretary	Secretara	k1gFnSc2
of	of	k?
Defense	defense	k1gFnSc2
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ANDREW	ANDREW	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Dragon	Dragon	k1gMnSc1
Breathes	Breathes	k1gMnSc1
Fire	Fire	k1gFnSc1
<g/>
:	:	kIx,
Chinese	Chinese	k1gFnSc1
Power	Power	k1gMnSc1
Projection	Projection	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AsianResearch	AsianResearch	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
18	#num#	k4
August	August	k1gMnSc1
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Sinopsis	Sinopsis	k1gFnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
úplný	úplný	k2eAgInSc1d1
překlad	překlad	k1gInSc1
čínského	čínský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
celonárodní	celonárodní	k2eAgFnSc6d1
špionáži	špionáž	k1gFnSc6
<g/>
,	,	kIx,
27.2	27.2	k4
<g/>
.2019	.2019	k4
<g/>
↑	↑	k?
Podle	podle	k7c2
obžaloby	obžaloba	k1gFnSc2
zavedla	zavést	k5eAaPmAgFnS
Huawei	Huawee	k1gFnSc4
interní	interní	k2eAgInSc4d1
systém	systém	k1gInSc4
odměn	odměna	k1gFnPc2
pro	pro	k7c4
zloděje	zloděj	k1gMnPc4
obchodních	obchodní	k2eAgFnPc2d1
tajemství	tajemství	k1gNnPc2
<g/>
,	,	kIx,
Sinopsis	Sinopsis	k1gFnSc1
30.1	30.1	k4
<g/>
.2019	.2019	k4
<g/>
↑	↑	k?
Frank	Frank	k1gMnSc1
Gardner	Gardner	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
spying	spying	k1gInSc1
game	game	k1gInSc1
<g/>
:	:	kIx,
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
global	globat	k5eAaImAgInS
network	network	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
BBC	BBC	kA
News	News	k1gInSc1
<g/>
,	,	kIx,
7.7	7.7	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
ANKENBRAND	ANKENBRAND	kA
<g/>
,	,	kIx,
Hendrik	Hendrik	k1gMnSc1
<g/>
:	:	kIx,
Wie	Wie	k1gMnSc1
deutsche	deutsch	k1gFnSc2
Manager	manager	k1gMnSc1
in	in	k?
China	China	k1gFnSc1
ausspioniert	ausspioniert	k1gInSc1
werden	werdno	k1gNnPc2
<g/>
,	,	kIx,
Frakfurter	Frakfurtra	k1gFnPc2
Algemeine	Algemein	k1gInSc5
Zeitung	Zeitung	k1gInSc1
<g/>
,	,	kIx,
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
„	„	k?
<g/>
Domácí	domácí	k2eAgInSc1d1
med	med	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
z	z	k7c2
cizích	cizí	k2eAgFnPc2d1
kytek	kytka	k1gFnPc2
<g/>
“	“	k?
<g/>
:	:	kIx,
Čínská	čínský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
vysílá	vysílat	k5eAaImIp3nS
experty	expert	k1gMnPc4
studovat	studovat	k5eAaImF
na	na	k7c4
Západ	západ	k1gInSc4
<g/>
,	,	kIx,
Sinopsis	Sinopsis	k1gFnSc4
<g/>
,	,	kIx,
10.11	10.11	k4
<g/>
.2018	.2018	k4
<g/>
↑	↑	k?
Bill	Bill	k1gMnSc1
Gertz	Gertz	k1gMnSc1
<g/>
,	,	kIx,
Spy	Spy	k1gMnSc1
school	school	k1gInSc1
<g/>
:	:	kIx,
Chinese	Chinese	k1gFnSc1
military	militara	k1gFnSc2
officer	officer	k1gMnSc1
busted	busted	k1gMnSc1
for	forum	k1gNnPc2
posing	posing	k1gInSc1
as	as	k9
Boston	Boston	k1gInSc1
University	universita	k1gFnSc2
student	student	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
1.2	1.2	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Former	Formra	k1gFnPc2
UC	UC	kA
Davis	Davis	k1gFnPc2
Researcher	Researchra	k1gFnPc2
From	From	k1gInSc1
China	China	k1gFnSc1
Juan	Juan	k1gMnSc1
Tang	tango	k1gNnPc2
Sought	Soughta	k1gFnPc2
By	by	k9
FBI	FBI	kA
Booked	Booked	k1gInSc1
Into	Into	k6eAd1
Sacramento	Sacramento	k1gNnSc4
County	Counta	k1gFnSc2
Jail	Jaila	k1gFnPc2
<g/>
,	,	kIx,
CBS	CBS	kA
Sacramento	Sacramento	k1gNnSc1
<g/>
,	,	kIx,
27.7	27.7	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
China-US	China-US	k1gMnSc1
row	row	k?
<g/>
:	:	kIx,
Fugitive	Fugitiv	k1gInSc5
researcher	researchra	k1gFnPc2
'	'	kIx"
<g/>
hiding	hiding	k1gInSc1
in	in	k?
San	San	k1gMnSc5
Francisco	Francisca	k1gMnSc5
consulate	consulat	k1gMnSc5
<g/>
'	'	kIx"
<g/>
,	,	kIx,
BBC	BBC	kA
News	News	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
23.7	23.7	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Alex	Alex	k1gMnSc1
Joske	Joske	k1gNnSc1
<g/>
,	,	kIx,
Picking	Picking	k1gInSc1
flowers	flowers	k1gInSc1
<g/>
,	,	kIx,
making	making	k1gInSc1
honey	honea	k1gFnSc2
<g/>
,	,	kIx,
Australian	Australian	k1gMnSc1
Strategic	Strategic	k1gMnSc1
Policy	Polica	k1gFnSc2
Institute	institut	k1gInSc5
<g/>
,	,	kIx,
30.10	30.10	k4
<g/>
.2018	.2018	k4
<g/>
↑	↑	k?
Ondřej	Ondřej	k1gMnSc1
Kundra	Kundra	k1gFnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Spurný	Spurný	k1gMnSc1
<g/>
,	,	kIx,
BIS	BIS	kA
otevřeně	otevřeně	k6eAd1
varuje	varovat	k5eAaImIp3nS
před	před	k7c7
promořeností	promořenost	k1gFnSc7
Česka	Česko	k1gNnSc2
ruskými	ruský	k2eAgMnPc7d1
špiony	špion	k1gMnPc7
<g/>
,	,	kIx,
Respekt	respekt	k1gInSc1
<g/>
,	,	kIx,
3.12	3.12	k4
<g/>
.2018	.2018	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Čínská	čínský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
sbírala	sbírat	k5eAaImAgFnS
data	datum	k1gNnPc4
o	o	k7c6
prominentech	prominent	k1gMnPc6
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímala	zajímat	k5eAaImAgFnS
se	se	k3xPyFc4
o	o	k7c4
Zemana	Zeman	k1gMnSc4
i	i	k8xC
šéfa	šéf	k1gMnSc4
BIS	BIS	kA
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Čína	Čína	k1gFnSc1
predbehla	predbehnout	k5eAaPmAgFnS
najväčšiu	najväčšium	k1gNnSc3
európsku	európsek	k1gInSc3
ekonomiku	ekonomika	k1gFnSc4
<g/>
.	.	kIx.
ekonomika	ekonomika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
sme	sme	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Uber	ubrat	k5eAaPmRp2nS
<g/>
’	’	k?
<g/>
s	s	k7c7
Surrender	Surrender	k1gMnSc1
to	ten	k3xDgNnSc4
Didi	Did	k1gFnPc1
Shows	Showsa	k1gFnPc2
the	the	k?
Steep	Steep	k1gInSc1
Odds	Odds	k1gInSc1
U.	U.	kA
<g/>
S.	S.	kA
Tech	Tech	k?
Faces	Faces	k1gInSc1
in	in	k?
China	China	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-08-03	2016-08-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Scott	Scott	k1gMnSc1
Cendrowski	Cendrowsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Enter	Enter	k1gInSc1
the	the	k?
Dragons	Dragons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fortune	Fortun	k1gInSc5
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Eric	Eric	k1gInSc1
Auchard	Auchard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Huawei	Huawei	k1gNnSc7
enters	entersa	k1gFnPc2
PC	PC	kA
market	market	k1gInSc1
to	ten	k3xDgNnSc4
take	takat	k5eAaPmIp3nS
on	on	k3xPp3gMnSc1
Lenovo	Lenovo	k1gNnSc1
<g/>
,	,	kIx,
HP	HP	kA
<g/>
,	,	kIx,
Dell	Dell	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Čínská	čínský	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
na	na	k7c6
havárii	havárie	k1gFnSc6
ve	v	k7c6
Fukušimě	Fukušima	k1gFnSc6
<g/>
,	,	kIx,
Atominfo	Atominfo	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
.1	.1	k4
2	#num#	k4
HARMS	HARMS	kA
<g/>
,	,	kIx,
Florian	Florian	k1gMnSc1
<g/>
:	:	kIx,
Trotz	Trotz	k1gMnSc1
Krise	kris	k1gInSc5
in	in	k?
Hongkong	Hongkong	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deutschland	Deutschland	k1gInSc1
und	und	k?
China	China	k1gFnSc1
bauen	bauna	k1gFnPc2
wirtschaftliche	wirtschaftlichat	k5eAaPmIp3nS
Zusammenarbeit	Zusammenarbeit	k1gInSc1
aus	aus	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Navzdory	navzdory	k7c3
krizi	krize	k1gFnSc3
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německo	Německo	k1gNnSc1
a	a	k8xC
Čína	Čína	k1gFnSc1
rozšiřují	rozšiřovat	k5eAaImIp3nP
svoji	svůj	k3xOyFgFnSc4
hospodářskou	hospodářský	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
T-Online	T-Onlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
,	,	kIx,
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2019	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
hod	hod	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HARMS	HARMS	kA
<g/>
,	,	kIx,
Florian	Florian	k1gMnSc1
a	a	k8xC
TROTZ	TROTZ	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
China	China	k1gFnSc1
diktiert	diktiert	k1gMnSc1
die	die	k?
Regeln	Regeln	k1gMnSc1
–	–	k?
auch	auch	k1gInSc1
Deutschland	Deutschland	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Čína	Čína	k1gFnSc1
diktuje	diktovat	k5eAaImIp3nS
pravidla	pravidlo	k1gNnPc4
–	–	k?
i	i	k8xC
Německu	Německo	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
T-Online	T-Onlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
,	,	kIx,
,	,	kIx,
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2019	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
hod	hod	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
COOLEY	COOLEY	kA
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnPc2
Silk	silk	k1gInSc4
Route	Rout	k1gInSc5
or	or	k?
Classic	Classic	k1gMnSc1
Developmental	Developmental	k1gMnSc1
Cul-de-Sac	Cul-de-Sac	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Prospects	Prospectsa	k1gFnPc2
and	and	k?
Challenges	Challenges	k1gMnSc1
of	of	k?
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
OBOR	obor	k1gInSc1
Initiative	Initiativ	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
July	Jula	k1gFnSc2
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
China	China	k1gFnSc1
plans	plans	k1gInSc1
new	new	k?
Silk	silk	k1gInSc1
Route	Rout	k1gInSc5
across	across	k1gInSc1
Ukraine	Ukrain	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russian	Russian	k1gInSc1
News	News	k1gInSc4
Agency	Agenca	k1gFnSc2
TASS	TASS	kA
<g/>
.	.	kIx.
9	#num#	k4
December	December	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
10	#num#	k4
February	Februara	k1gFnSc2
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SAHOO	SAHOO	kA
<g/>
,	,	kIx,
Pravakar	Pravakar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
India	indium	k1gNnSc2
should	should	k1gInSc1
be	be	k?
part	part	k1gInSc1
of	of	k?
the	the	k?
new	new	k?
Silk	silk	k1gInSc1
Route	Rout	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Hindu	hind	k1gMnSc3
Business	business	k1gInSc4
Line	linout	k5eAaImIp3nS
<g/>
.	.	kIx.
22	#num#	k4
December	December	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
10	#num#	k4
February	Februara	k1gFnSc2
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
China	China	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
new	new	k?
silk	silk	k1gInSc4
route	route	k5eAaPmIp2nP
<g/>
:	:	kIx,
The	The	k1gMnPc1
long	long	k1gMnSc1
and	and	k?
winding	winding	k1gInSc1
road	road	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
February	Februara	k1gFnSc2
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
First	First	k1gInSc1
'	'	kIx"
<g/>
Silk	silk	k1gInSc1
Road	Road	k1gInSc1
<g/>
'	'	kIx"
train	train	k2eAgInSc1d1
arrives	arrives	k1gInSc1
in	in	k?
Tehran	Tehran	k1gInSc1
from	from	k1gInSc1
China	China	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
'	'	kIx"
<g/>
China	China	k1gFnSc1
freight	freight	k1gMnSc1
train	train	k1gMnSc1
<g/>
'	'	kIx"
in	in	k?
first	first	k1gMnSc1
trip	trip	k1gMnSc1
to	ten	k3xDgNnSc4
Barking	Barking	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Silk	silk	k1gInSc1
Road	Road	k1gMnSc1
route	route	k5eAaPmIp2nP
back	back	k6eAd1
in	in	k?
business	business	k1gInSc1
as	as	k1gInSc1
China	China	k1gFnSc1
train	train	k1gMnSc1
rolls	rolls	k6eAd1
into	into	k1gMnSc1
London	London	k1gMnSc1
<g/>
,	,	kIx,
Tracy	Traca	k1gFnPc1
McVeigh	McVeigh	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Observer	Observer	k1gMnSc1
<g/>
,	,	kIx,
14	#num#	k4
January	Januara	k1gFnSc2
2017	#num#	k4
<g/>
↑	↑	k?
</s>
<s>
Levnější	levný	k2eAgNnSc1d2
než	než	k8xS
letadlo	letadlo	k1gNnSc1
a	a	k8xC
rychlejší	rychlý	k2eAgFnSc1d2
než	než	k8xS
loď	loď	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
otevřela	otevřít	k5eAaPmAgFnS
Hedvábnou	hedvábný	k2eAgFnSc4d1
stezku	stezka	k1gFnSc4
vlakem	vlak	k1gInSc7
do	do	k7c2
Londýna	Londýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-01-17	2017-01-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Auf	Auf	k1gFnSc1
der	drát	k5eAaImRp2nS
Breitspur	Breitspur	k1gMnSc1
bis	bis	k?
nach	nach	k1gInSc1
Wien	Wien	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Na	na	k7c6
širokých	široký	k2eAgFnPc6d1
kolejích	kolej	k1gFnPc6
až	až	k6eAd1
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Frankfurter	Frankfurter	k1gInSc1
Allgemeine	Allgemein	k1gInSc5
Zeitung	Zeitung	k1gMnSc1
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
21	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
16	#num#	k4
+	+	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
1	#num#	k4
<g/>
,	,	kIx,
Sinopsis	Sinopsis	k1gFnSc1
<g/>
↑	↑	k?
Jedno	jeden	k4xCgNnSc1
pásmo	pásmo	k1gNnSc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
cesta	cesta	k1gFnSc1
<g/>
,	,	kIx,
Sinopsis	Sinopsis	k1gFnSc1
<g/>
↑	↑	k?
Hledá	hledat	k5eAaImIp3nS
se	se	k3xPyFc4
společná	společný	k2eAgFnSc1d1
politika	politika	k1gFnSc1
EU	EU	kA
vůči	vůči	k7c3
ČLR	ČLR	kA
(	(	kIx(
<g/>
zn.	zn.	kA
<g/>
:	:	kIx,
Těžko	těžko	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sinopsis	Sinopsis	k1gInSc1
<g/>
,	,	kIx,
27.3	27.3	k4
<g/>
.2019	.2019	k4
<g/>
↑	↑	k?
Martin	Martin	k1gMnSc1
Hála	Hála	k1gMnSc1
<g/>
,	,	kIx,
Vymývání	vymývání	k1gNnSc1
mozků	mozek	k1gInPc2
po	po	k7c6
čínsku	čínsek	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Sinopsis	Sinopsis	k1gFnSc1
29.3	29.3	k4
<g/>
.2016	.2016	k4
<g/>
↑	↑	k?
Strategické	strategický	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
<g/>
,	,	kIx,
Sinopsis	Sinopsis	k1gInSc1
<g/>
↑	↑	k?
Communiqué	Communiquá	k1gFnSc2
of	of	k?
the	the	k?
National	National	k1gFnSc3
Bureau	Bureaus	k1gInSc2
of	of	k?
Statistics	Statistics	k1gInSc1
of	of	k?
People	People	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Republic	Republice	k1gFnPc2
of	of	k?
China	China	k1gFnSc1
on	on	k3xPp3gMnSc1
Major	major	k1gMnSc1
Figures	Figures	k1gMnSc1
of	of	k?
the	the	k?
2010	#num#	k4
Population	Population	k1gInSc1
Census	census	k1gInSc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Bureau	Bureaus	k1gInSc2
of	of	k?
Statistics	Statistics	k1gInSc1
of	of	k?
China	China	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Population	Population	k1gInSc1
Growth	Growth	k1gMnSc1
Rate	Rate	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gMnSc1
Cambridge	Cambridge	k1gFnSc2
Illustrated	Illustrated	k1gMnSc1
History	Histor	k1gMnPc4
of	of	k?
China	China	k1gFnSc1
<g/>
,	,	kIx,
Patricia	Patricius	k1gMnSc2
Buckley	Bucklea	k1gMnSc2
Ebrey	Ebrea	k1gMnSc2
<g/>
,	,	kIx,
Cambridge	Cambridge	k1gFnSc2
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
12433	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
327	#num#	k4
<g/>
↑	↑	k?
The	The	k1gMnSc1
American	American	k1gMnSc1
Dream	Dream	k1gInSc4
Is	Is	k1gFnSc2
Alive	Aliev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
China	China	k1gFnSc1
<g/>
..	..	k?
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
18	#num#	k4
November	November	k1gInSc1
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
23	#num#	k4
February	Februara	k1gFnSc2
2019	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Communiqué	Communiqué	k1gNnPc2
of	of	k?
the	the	k?
National	National	k1gFnSc3
Bureau	Bureaus	k1gInSc2
of	of	k?
Statistics	Statistics	k1gInSc1
of	of	k?
People	People	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Republic	Republice	k1gFnPc2
of	of	k?
China	China	k1gFnSc1
on	on	k3xPp3gMnSc1
Major	major	k1gMnSc1
Figures	Figures	k1gMnSc1
of	of	k?
the	the	k?
2010	#num#	k4
Population	Population	k1gInSc1
Census	census	k1gInSc4
(	(	kIx(
<g/>
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Bureau	Bureaus	k1gInSc2
of	of	k?
Statistics	Statistics	k1gInSc1
of	of	k?
China	China	k1gFnSc1
<g/>
,	,	kIx,
28	#num#	k4
April	April	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
15	#num#	k4
January	Januara	k1gFnSc2
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lilly	Lilla	k1gMnSc2
<g/>
,	,	kIx,
Amanda	Amand	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc4
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Ethnic	Ethnice	k1gFnPc2
Groups	Groupsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
Post	post	k1gInSc4
<g/>
.	.	kIx.
7	#num#	k4
July	Jula	k1gFnSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
9	#num#	k4
December	December	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Geography	Geograph	k1gInPc7
<g/>
:	:	kIx,
Globalization	Globalization	k1gInSc1
and	and	k?
the	the	k?
Dynamics	Dynamicsa	k1gFnPc2
of	of	k?
Political	Political	k1gMnSc1
<g/>
,	,	kIx,
Economic	Economic	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Social	Social	k1gInSc1
Change	change	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Rowman	Rowman	k1gMnSc1
&	&	k?
Littlefield	Littlefield	k1gInSc1
Publishers	Publishersa	k1gFnPc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7425	#num#	k4
<g/>
-	-	kIx~
<g/>
6784	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
102	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Major	major	k1gMnSc1
Figures	Figures	k1gMnSc1
on	on	k3xPp3gMnSc1
Residents	Residentsa	k1gFnPc2
from	from	k1gMnSc1
Hong	Hong	k1gMnSc1
Kong	Kongo	k1gNnPc2
<g/>
,	,	kIx,
Macao	Macao	k1gNnSc1
and	and	k?
Taiwan	Taiwany	k1gInPc2
and	and	k?
Foreigners	Foreignersa	k1gFnPc2
Covered	Covered	k1gMnSc1
by	by	k9
2010	#num#	k4
Population	Population	k1gInSc1
Census	census	k1gInSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Bureau	Bureaus	k1gInSc2
of	of	k?
Statistics	Statistics	k1gInSc1
of	of	k?
China	China	k1gFnSc1
<g/>
.	.	kIx.
29	#num#	k4
April	April	k1gInSc1
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
31	#num#	k4
May	May	k1gMnSc1
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
National	National	k1gFnSc1
Population	Population	k1gInSc1
by	by	kYmCp3nS
Sex	sex	k1gInSc1
<g/>
,	,	kIx,
Nationality	Nationalit	k1gInPc1
|	|	kIx~
National	National	k1gFnSc1
Bureau	Bureaus	k1gInSc2
of	of	k?
Statistics	Statistics	k1gInSc1
of	of	k?
China	China	k1gFnSc1
<g/>
.	.	kIx.
www.stats.gov.cn	www.stats.gov.cn	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Languages	Languages	k1gInSc1
of	of	k?
China	China	k1gFnSc1
–	–	k?
from	from	k1gInSc4
Lewis	Lewis	k1gFnSc2
<g/>
,	,	kIx,
M.	M.	kA
Paul	Paul	k1gMnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ethnologue	Ethnologue	k1gInSc1
<g/>
:	:	kIx,
Languages	Languages	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gMnSc1
<g/>
,	,	kIx,
Sixteenth	Sixteenth	k1gMnSc1
edition	edition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dallas	Dallas	k1gInSc1
<g/>
,	,	kIx,
Tex	Tex	k1gFnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
SIL	sílit	k5eAaImRp2nS
International	International	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kaplan	Kaplan	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
B.	B.	kA
<g/>
;	;	kIx,
RICHARD	Richard	k1gMnSc1
B.	B.	kA
BALDAUF	BALDAUF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Language	language	k1gFnPc2
Planning	Planning	k1gInSc4
and	and	k?
Policy	Polica	k1gMnSc2
in	in	k?
Asia	Asius	k1gMnSc2
<g/>
:	:	kIx,
Japan	japan	k1gInSc4
<g/>
,	,	kIx,
Nepal	pálit	k5eNaImRp2nS
<g/>
,	,	kIx,
Taiwan	Taiwan	k1gInSc1
and	and	k?
Chinese	Chinese	k1gFnSc2
characters	charactersa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Multilingual	Multilingual	k1gInSc1
Matters	Mattersa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84769	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
95	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
42	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Languages	Languages	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gov	Gov	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cn	cn	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
31	#num#	k4
May	May	k1gMnSc1
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Rough	Rough	k1gInSc1
Guide	Guid	k1gInSc5
Phrasebook	Phrasebook	k1gInSc1
<g/>
:	:	kIx,
Mandarin	Mandarin	k1gInSc1
Chinese	Chinese	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Rough	Rough	k1gMnSc1
Guides	Guides	k1gMnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4053	#num#	k4
<g/>
-	-	kIx~
<g/>
8884	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DILLON	DILLON	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Religious	Religious	k1gMnSc1
Minorities	Minorities	k1gMnSc1
and	and	k?
China	China	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Minority	minorita	k1gFnSc2
Rights	Rights	k1gInSc1
Group	Group	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
English	English	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BUANG	BUANG	kA
<g/>
,	,	kIx,
Sa	Sa	k1gFnSc1
<g/>
'	'	kIx"
<g/>
eda	eda	k?
<g/>
;	;	kIx,
CHEW	CHEW	kA
<g/>
,	,	kIx,
Phyllis	Phyllis	k1gInSc4
Ghim-Lian	Ghim-Liana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslim	muslim	k1gMnSc1
Education	Education	k1gInSc1
in	in	k?
the	the	k?
21	#num#	k4
<g/>
st	st	kA
Century	Centura	k1gFnSc2
<g/>
:	:	kIx,
Asian	Asian	k1gMnSc1
Perspectives	Perspectives	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
9	#num#	k4
May	May	k1gMnSc1
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
317	#num#	k4
<g/>
-	-	kIx~
<g/>
81500	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
75	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
English	English	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
National	National	k1gMnSc1
Religious	Religious	k1gMnSc1
Affairs	Affairsa	k1gFnPc2
Administration	Administration	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
12	#num#	k4
August	August	k1gMnSc1
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
China	China	k1gFnSc1
bans	bansa	k1gFnPc2
religious	religious	k1gMnSc1
activities	activities	k1gMnSc1
in	in	k?
Xinjiang	Xinjiang	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Financial	Financial	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
August	August	k1gMnSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
28	#num#	k4
August	August	k1gMnSc1
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Constitution	Constitution	k1gInSc1
of	of	k?
the	the	k?
People	People	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Republic	Republice	k1gFnPc2
of	of	k?
China	China	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chapter	Chapter	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
Article	Article	k1gFnSc1
36	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
YAO	YAO	kA
<g/>
,	,	kIx,
Xinzhong	Xinzhong	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chinese	Chinese	k1gFnSc1
Religion	religion	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Contextual	Contextual	k1gMnSc1
Approach	Approach	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
<g/>
&	&	k?
<g/>
C	C	kA
Black	Black	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84706	#num#	k4
<g/>
-	-	kIx~
<g/>
475	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
pp	pp	k?
<g/>
.	.	kIx.
9	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chinese	Chinese	k1gFnSc1
Religions	Religionsa	k1gFnPc2
in	in	k?
Contemporary	Contemporar	k1gInPc1
Societies	Societies	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
ABC-CLIO	ABC-CLIO	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
85109	#num#	k4
<g/>
-	-	kIx~
<g/>
626	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
p.	p.	k?
57	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ZÁDRAPA	ZÁDRAPA	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc1
tisíc	tisíc	k4xCgInPc2
let	léto	k1gNnPc2
čínských	čínský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
-	-	kIx~
anebo	anebo	k8xC
ne	ne	k9
tak	tak	k6eAd1
docela	docela	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sinopsis	Sinopsis	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-05-23	2016-05-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Čínská	čínský	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
má	mít	k5eAaImIp3nS
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2000-10-13	2000-10-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
literaturu	literatura	k1gFnSc4
dostal	dostat	k5eAaPmAgMnS
Mo	Mo	k1gMnSc1
Jen	jen	k9
<g/>
,	,	kIx,
v	v	k7c6
deseti	deset	k4xCc6
vyloučený	vyloučený	k2eAgMnSc1d1
ze	z	k7c2
školy	škola	k1gFnSc2
|	|	kIx~
Kultura	kultura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-10-11	2012-10-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Osm	osm	k4xCc4
tisíc	tisíc	k4xCgInSc4
soch	socha	k1gFnPc2
a	a	k8xC
každá	každý	k3xTgFnSc1
je	být	k5eAaImIp3nS
jiná	jiný	k2eAgFnSc1d1
<g/>
:	:	kIx,
Terakotová	terakotový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
prý	prý	k9
ukrývá	ukrývat	k5eAaImIp3nS
skryté	skrytý	k2eAgNnSc4d1
poselství	poselství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Chránil	chránit	k5eAaImAgMnS
životy	život	k1gInPc4
rybářů	rybář	k1gMnPc2
už	už	k6eAd1
před	před	k7c7
tisíci	tisíc	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
fascinuje	fascinovat	k5eAaBmIp3nS
celý	celý	k2eAgInSc1d1
svět	svět	k1gInSc1
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čínské	čínský	k2eAgFnSc2d1
jeskyně	jeskyně	k1gFnSc2
Mo-kao	Mo-kao	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šokující	šokující	k2eAgFnSc1d1
Planeta	planeta	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Yungang	Yungang	k1gInSc1
(	(	kIx(
<g/>
Jün-kang	Jün-kang	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HedvabnaStezka	HedvabnaStezka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dazu	Dazus	k1gInSc2
(	(	kIx(
<g/>
Ta-cu	Ta-cus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HedvabnaStezka	HedvabnaStezka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Buddhistické	buddhistický	k2eAgFnSc2d1
jeskyně	jeskyně	k1gFnSc2
Lung-men	Lung-men	k1gInSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
existují	existovat	k5eAaImIp3nP
už	už	k9
1500	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zuojiang	Zuojiang	k1gInSc1
Huashan	Huashan	k1gMnSc1
Rock	rock	k1gInSc1
Art	Art	k1gMnSc5
Cultural	Cultural	k1gMnSc5
Landscape	Landscap	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNESCO	Unesco	k1gNnSc1
World	Worlda	k1gFnPc2
Heritage	Heritage	k1gNnSc2
Centre	centr	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nejznámější	známý	k2eAgMnSc1d3
čínský	čínský	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
Aj	aj	kA
Wej-wej	Wej-wej	k1gInSc1
dnes	dnes	k6eAd1
slaví	slavit	k5eAaImIp3nS
60	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pritzkerova	Pritzkerův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
putuje	putovat	k5eAaImIp3nS
do	do	k7c2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhrál	vyhrát	k5eAaPmAgMnS
Wang	Wang	k1gMnSc1
Šu	Šu	k1gMnSc1
|	|	kIx~
Design	design	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-03-03	2012-03-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GOLDBERGER	GOLDBERGER	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
<g/>
M.	M.	kA
Pei	Pei	k1gMnSc1
<g/>
,	,	kIx,
Master	master	k1gMnSc1
Architect	Architect	k1gMnSc1
Whose	Whose	k1gFnSc2
Buildings	Buildings	k1gInSc1
Dazzled	Dazzled	k1gMnSc1
the	the	k?
World	World	k1gInSc1
<g/>
,	,	kIx,
Dies	Dies	k1gInSc1
at	at	k?
102	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
v	v	k7c6
Pekingu	Peking	k1gInSc6
začaly	začít	k5eAaPmAgFnP
<g/>
,	,	kIx,
oheň	oheň	k1gInSc4
zapálil	zapálit	k5eAaPmAgMnS
legendární	legendární	k2eAgMnSc1d1
gymnasta	gymnasta	k1gMnSc1
<g/>
.	.	kIx.
www.sport.cz	www.sport.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kde	kde	k6eAd1
je	být	k5eAaImIp3nS
Fan	Fana	k1gFnPc2
Ping-ping	Ping-ping	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgFnSc1d3
čínská	čínský	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
zmizela	zmizet	k5eAaPmAgFnS
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
ji	on	k3xPp3gFnSc4
vyšetřují	vyšetřovat	k5eAaImIp3nP
úřady	úřad	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-08-14	2018-08-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Miss	miss	k1gFnSc1
World	World	k1gInSc1
2007	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
čínská	čínský	k2eAgFnSc1d1
miss	miss	k1gFnSc1
C	C	kA
<g/>
'	'	kIx"
Lin	Lina	k1gFnPc2
Čang	Čanga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čínská	čínský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
<g/>
.	.	kIx.
ikoktejl	ikoktejl	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sedm	sedm	k4xCc4
nových	nový	k2eAgInPc2d1
divů	div	k1gInPc2
světa	svět	k1gInSc2
<g/>
:	:	kIx,
Patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
ně	on	k3xPp3gFnPc4
krvavá	krvavý	k2eAgFnSc1d1
pyramida	pyramida	k1gFnSc1
i	i	k8xC
stavba	stavba	k1gFnSc1
viditelná	viditelný	k2eAgFnSc1d1
z	z	k7c2
vesmíru	vesmír	k1gInSc2
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Peking	Peking	k1gInSc1
má	mít	k5eAaImIp3nS
nejnavštěvovanější	navštěvovaný	k2eAgFnSc4d3
památku	památka	k1gFnSc4
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	s	k7c7
500	#num#	k4
let	léto	k1gNnPc2
nesmělo	smět	k5eNaImAgNnS
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CNN	CNN	kA
<g/>
,	,	kIx,
Elaine	elain	k1gInSc5
Yu	Yu	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ancient	Ancient	k1gMnSc1
'	'	kIx"
<g/>
world	world	k1gMnSc1
wonder	wonder	k1gMnSc1
<g/>
'	'	kIx"
comes	comes	k1gMnSc1
back	back	k1gMnSc1
to	ten	k3xDgNnSc4
life	life	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
Travel	Travela	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-09-16	2016-09-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
V	v	k7c6
Číně	Čína	k1gFnSc6
otevřeli	otevřít	k5eAaPmAgMnP
nejdelší	dlouhý	k2eAgInSc4d3
most	most	k1gInSc4
vedoucí	vedoucí	k1gFnSc2
přes	přes	k7c4
moře	moře	k1gNnSc4
<g/>
:	:	kIx,
Odolá	odolat	k5eAaPmIp3nS
zemětřesení	zemětřesení	k1gNnSc4
i	i	k8xC
supertajfunu	supertajfuna	k1gFnSc4
<g/>
.	.	kIx.
100	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
zahraniční	zahraniční	k2eAgFnSc4d1
zajímavost	zajímavost	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-10-24	2018-10-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČÍNSKÝ	čínský	k2eAgInSc4d1
ŠOK	šok	k1gInSc4
<g/>
:	:	kIx,
Vejce	vejce	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
mění	měnit	k5eAaImIp3nS
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítejte	vítat	k5eAaImRp2nP
ve	v	k7c6
Velkém	velký	k2eAgNnSc6d1
národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
|	|	kIx~
Aktuality	aktualita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-15	2019-03-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Velké	velký	k2eAgFnPc1d1
trenýrky	trenýrky	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
čínské	čínský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
televize	televize	k1gFnSc2
vzbuzuje	vzbuzovat	k5eAaImIp3nS
rozpaky	rozpak	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-05-18	2012-05-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čínská	čínský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
<g/>
:	:	kIx,
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
milovníky	milovník	k1gMnPc4
hůlek	hůlka	k1gFnPc2
<g/>
,	,	kIx,
rýže	rýže	k1gFnSc2
a	a	k8xC
pálivých	pálivý	k2eAgInPc2d1
pokrmů	pokrm	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poznávací	poznávací	k2eAgInPc1d1
zájezdy	zájezd	k1gInPc1
|	|	kIx~
mazaně	mazaně	k6eAd1
s	s	k7c7
Radynacestu	Radynacest	k1gInSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://books.google.cz/books?id=tmrnPQAAQBAJ&	https://books.google.cz/books?id=tmrnPQAAQBAJ&	k1gMnSc1
Tchuo	Tchuo	k1gMnSc1
<g/>
&	&	k?
<g/>
f	f	k?
<g/>
=	=	kIx~
<g/>
false	false	k1gFnSc1
<g/>
↑	↑	k?
https://books.google.cz/books?id=wBgvBQAAQBAJ&	https://books.google.cz/books?id=wBgvBQAAQBAJ&	k?
<g/>
’	’	k?
<g/>
-čen	-čen	k1gMnSc1
<g/>
&	&	k?
<g/>
source	source	k1gMnSc1
<g/>
=	=	kIx~
<g/>
bl	bl	k?
<g/>
&	&	k?
<g/>
ots	ots	k?
<g/>
=	=	kIx~
<g/>
dbDHyUV	dbDHyUV	k?
<g/>
2	#num#	k4
<g/>
HU	hu	k0
<g/>
&	&	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
sig	sig	k?
<g/>
=	=	kIx~
<g/>
ACfU	ACfU	k1gFnSc1
<g/>
3	#num#	k4
<g/>
U	u	k7c2
<g/>
0	#num#	k4
<g/>
uYgYrC	uYgYrC	k?
<g/>
7	#num#	k4
<g/>
tP-NpBuSlk	tP-NpBuSlk	k6eAd1
<g/>
87	#num#	k4
<g/>
pDBj	pDBj	k1gFnSc1
<g/>
4	#num#	k4
<g/>
dCg	dCg	k?
<g/>
&	&	k?
<g/>
hl	hl	k?
<g/>
=	=	kIx~
<g/>
cs	cs	k?
<g/>
&	&	k?
<g/>
sa	sa	k?
<g/>
=	=	kIx~
<g/>
X	X	kA
<g/>
&	&	k?
<g/>
ved	ved	k?
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
ahUKEwi	ahUKEwi	k6eAd1
<g/>
6	#num#	k4
<g/>
yeDr	yeDra	k1gFnPc2
<g/>
4	#num#	k4
<g/>
ILjAhVHZFAKHfTgANQQ	ILjAhVHZFAKHfTgANQQ	k1gFnPc2
<g/>
6	#num#	k4
<g/>
AEwA	AEwA	k1gFnPc2
<g/>
3	#num#	k4
<g/>
oECAoQAQ	oECAoQAQ	k?
<g/>
#	#	kIx~
<g/>
v	v	k7c4
<g/>
=	=	kIx~
<g/>
onepage	onepage	k1gFnSc4
<g/>
&	&	k?
<g/>
q	q	k?
<g/>
=	=	kIx~
<g/>
Li	li	k8xS
Š	Š	kA
<g/>
’	’	k?
<g/>
-čen	-čen	k1gInSc1
<g/>
&	&	k?
<g/>
f	f	k?
<g/>
=	=	kIx~
<g/>
false	false	k1gFnSc1
<g/>
↑	↑	k?
The	The	k1gFnSc1
Manhattan	Manhattan	k1gInSc1
Project	Project	k1gMnSc1
Physicist	Physicist	k1gMnSc1
Who	Who	k1gMnSc1
Fought	Fought	k1gMnSc1
for	forum	k1gNnPc2
Equal	Equal	k1gInSc4
Rights	Rights	k1gInSc4
for	forum	k1gNnPc2
Women	Womna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Time	Time	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
zakladatel	zakladatel	k1gMnSc1
čínské	čínský	k2eAgFnSc2d1
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
získali	získat	k5eAaPmAgMnP
bojovníci	bojovník	k1gMnPc1
proti	proti	k7c3
parazitům	parazit	k1gMnPc3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-10-05	2015-10-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ču	Ču	k1gFnSc1
Si	se	k3xPyFc3
<g/>
.	.	kIx.
leporelo	leporelo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kumáradžíva	Kumáradžíva	k1gFnSc1
<g/>
.	.	kIx.
leporelo	leporelo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čínský	čínský	k2eAgMnSc1d1
autor	autor	k1gMnSc1
Knihy	kniha	k1gFnSc2
vrchních	vrchní	k2eAgMnPc2d1
písařů	písař	k1gMnPc2
poodhaluje	poodhalovat	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
tajemství	tajemství	k1gNnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-03-22	2013-03-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sun-c	Sun-c	k1gInSc1
<g/>
’	’	k?
<g/>
:	:	kIx,
Geniálny	Geniálna	k1gFnPc4
čínsky	čínsky	k6eAd1
stratég	stratég	k1gMnSc1
a	a	k8xC
taktik	taktik	k1gMnSc1
<g/>
,	,	kIx,
ktorý	ktorý	k2eAgMnSc1d1
zároveň	zároveň	k6eAd1
napísal	napísat	k5eAaPmAgMnS,k5eAaImAgMnS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
najstarších	najstarší	k1gNnPc6
kníh	kníha	k1gFnPc2
o	o	k7c4
vedení	vedení	k1gNnSc4
vojny	vojna	k1gFnSc2
<g/>
.	.	kIx.
refresher	refreshra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
QS	QS	kA
World	World	k1gInSc4
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc4
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Top	topit	k5eAaImRp2nS
Universities	Universities	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-05	2019-06-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Ivy	Ivo	k1gMnPc7
League	Leagu	k1gInSc2
<g/>
：	：	k?
<g/>
C	C	kA
<g/>
9	#num#	k4
League	Leagu	k1gFnPc1
<g/>
.	.	kIx.
en	en	k?
<g/>
.	.	kIx.
<g/>
people	people	k6eAd1
<g/>
.	.	kIx.
<g/>
cn	cn	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Olympijský	olympijský	k2eAgInSc1d1
oheň	oheň	k1gInSc1
zažehne	zažehnout	k5eAaPmIp3nS
Li	li	k9
Ning	Ning	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princ	princa	k1gFnPc2
gymnastiky	gymnastika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-08-08	2008-08-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zlomila	zlomit	k5eAaPmAgFnS
ji	on	k3xPp3gFnSc4
bolest	bolest	k1gFnSc4
kolen	kolna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínská	čínský	k2eAgFnSc1d1
tenistka	tenistka	k1gFnSc1
Li	li	k8xS
Na	na	k7c6
ukončila	ukončit	k5eAaPmAgFnS
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-09-19	2014-09-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
První	první	k4xOgMnSc1
Číňan	Číňan	k1gMnSc1
v	v	k7c6
NBA	NBA	kA
Jao	Jao	k1gMnSc1
Ming	Ming	k1gMnSc1
ukončil	ukončit	k5eAaPmAgMnS
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
2011-07-20	2011-07-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Olympijská	olympijský	k2eAgFnSc1d1
rekordmanka	rekordmanka	k1gFnSc1
ve	v	k7c6
skocích	skok	k1gInPc6
do	do	k7c2
vody	voda	k1gFnSc2
Wu	Wu	k1gMnSc2
Min-sia	Min-sius	k1gMnSc2
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
iSport	iSport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čína	Čína	k1gFnSc1
získala	získat	k5eAaPmAgFnS
v	v	k7c6
olympijském	olympijský	k2eAgInSc6d1
badmintonu	badminton	k1gInSc6
všechny	všechen	k3xTgFnPc4
zlaté	zlatý	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
sport	sport	k1gInSc1
-	-	kIx~
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hall	Hall	k1gInSc1
of	of	k?
Famers	Famers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Volleyball	Volleyball	k1gMnSc1
Hall	Hall	k1gMnSc1
of	of	k?
Fame	Fame	k1gInSc1
-	-	kIx~
Holyoke	Holyoke	k1gNnSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
USA	USA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zimní	zimní	k2eAgFnPc4d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
2022	#num#	k4
bude	být	k5eAaImBp3nS
hostit	hostit	k5eAaImF
Peking	Peking	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-07-31	2015-07-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pocket	Pocket	k1gMnSc1
World	World	k1gMnSc1
in	in	k?
Figures	Figures	k1gMnSc1
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Economist	Economist	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
254	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
86197	#num#	k4
<g/>
-	-	kIx~
<g/>
844	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
18	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Hledají	hledat	k5eAaImIp3nP
se	se	k3xPyFc4
nevěsty	nevěsta	k1gFnPc1
pro	pro	k7c4
miliony	milion	k4xCgInPc1
čínských	čínský	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Hacker	hacker	k1gMnSc1
Army	Arma	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foreign	Foreign	k1gInSc1
Policy	Polica	k1gFnSc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Čínští	čínský	k2eAgMnPc1d1
kyberzločinci	kyberzločinec	k1gMnPc1
ukradli	ukradnout	k5eAaPmAgMnP
miliony	milion	k4xCgInPc4
otisků	otisk	k1gInPc2
prstů	prst	k1gInPc2
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
USA	USA	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PETERSON	PETERSON	kA
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OPM	OPM	kA
says	says	k1gInSc4
5.6	5.6	k4
million	million	k1gInSc1
fingerprints	fingerprints	k6eAd1
stolen	stolen	k2eAgMnSc1d1
in	in	k?
biggest	biggest	k1gMnSc1
cyber	cyber	k1gMnSc1
attack	attack	k1gMnSc1
in	in	k?
US	US	kA
history	histor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
America	America	k1gMnSc1
doesn	doesn	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
have	havat	k5eAaPmIp3nS
anything	anything	k1gInSc1
together	togethra	k1gFnPc2
this	thisa	k1gFnPc2
is	is	k?
why	why	k?
this	this	k1gInSc1
happened	happened	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Internet	Internet	k1gInSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
Závistí	závist	k1gFnPc2
by	by	kYmCp3nS
zbledl	zblednout	k5eAaPmAgInS
i	i	k9
Velký	velký	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
Více	hodně	k6eAd2
na	na	k7c4
<g/>
:	:	kIx,
https://www.zive.cz/clanky/internet-v-cine-zavisti-by-zbledl-i-velky-bratr/sc-3-a-182451/default.aspx	https://www.zive.cz/clanky/internet-v-cine-zavisti-by-zbledl-i-velky-bratr/sc-3-a-182451/default.aspx	k1gInSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Facebook	Facebook	k1gInSc1
chce	chtít	k5eAaImIp3nS
zpátky	zpátky	k6eAd1
do	do	k7c2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
vyvinul	vyvinout	k5eAaPmAgInS
software	software	k1gInSc1
na	na	k7c4
cenzuru	cenzura	k1gFnSc4
příspěvků	příspěvek	k1gInPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SCHMITZ	SCHMITZ	kA
<g/>
,	,	kIx,
Rob	roba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xi	Xi	k1gMnSc1
Jinping	Jinping	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
family	famil	k1gMnPc7
linked	linked	k1gMnSc1
to	ten	k3xDgNnSc4
Panama	panama	k1gNnSc4
Papers	Papers	k1gInSc1
<g/>
.	.	kIx.
www.marketplace.org	www.marketplace.org	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marketplace	Marketplace	k1gFnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
V	v	k7c6
Panama	panama	k1gNnSc6
Papers	Papersa	k1gFnPc2
se	se	k3xPyFc4
namočily	namočit	k5eAaPmAgFnP
i	i	k9
čínské	čínský	k2eAgFnPc1d1
elity	elita	k1gFnPc1
<g/>
,	,	kIx,
cenzura	cenzura	k1gFnSc1
kauzu	kauza	k1gFnSc4
tutlá	tutlat	k5eAaImIp3nS
<g/>
"	"	kIx"
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Čínská	čínský	k2eAgFnSc1d1
CEFC	CEFC	kA
završila	završit	k5eAaPmAgFnS
vstup	vstup	k1gInSc4
do	do	k7c2
Empresa	Empresa	k1gFnSc1
Media	medium	k1gNnSc2
a	a	k8xC
Médea	Médea	k1gFnSc1
Group	Group	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
DigiZone	DigiZon	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Bližší	blízký	k2eAgInSc1d2
pohled	pohled	k1gInSc1
na	na	k7c4
Zemanova	Zemanův	k2eAgMnSc4d1
čínského	čínský	k2eAgMnSc4d1
poradce	poradce	k1gMnSc4
a	a	k8xC
společnost	společnost	k1gFnSc4
CEFC	CEFC	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Epoch	epocha	k1gFnPc2
Times	Timesa	k1gFnPc2
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Who	Who	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
that	that	k1gMnSc1
39	#num#	k4
<g/>
-year-old	-year-old	k1gInSc1
paying	paying	k1gInSc4
HK	HK	kA
<g/>
$	$	kIx~
<g/>
1.4	1.4	k4
billion	billion	k1gInSc1
for	forum	k1gNnPc2
three	threat	k5eAaPmIp3nS
office	office	k1gFnSc1
floors	floorsa	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	Southa	k1gFnPc2
China	China	k1gFnSc1
Morning	Morning	k1gInSc1
Post	post	k1gInSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Fallen	Fallen	k1gInSc1
Energy	Energ	k1gInPc1
Conglomerate	Conglomerat	k1gInSc5
CEFC	CEFC	kA
Declared	Declared	k1gMnSc1
Bankrupt	Bankrupt	k1gMnSc1
-	-	kIx~
Caixin	Caixin	k1gMnSc1
Global	globat	k5eAaImAgMnS
<g/>
.	.	kIx.
www.caixinglobal.com	www.caixinglobal.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
15	#num#	k4
největších	veliký	k2eAgMnPc2d3
zahraničních	zahraniční	k2eAgMnPc2d1
věřitelů	věřitel	k1gMnPc2
USA	USA	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Investicniweb	Investicniwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
FENDRYCHOVÁ	FENDRYCHOVÁ	kA
<g/>
,	,	kIx,
Simona	Simona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
převychovává	převychovávat	k5eAaImIp3nS
muslimy	muslim	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ponižovaní	ponižovaný	k2eAgMnPc1d1
Ujguři	Ujgur	k1gMnPc1
živoří	živořit	k5eAaImIp3nP
v	v	k7c6
lágrech	lágr	k1gInPc6
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
jíst	jíst	k5eAaImF
i	i	k9
vepřové	vepřový	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VOŽENÍLEK	VOŽENÍLEK	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlídat	hlídat	k5eAaImF
se	se	k3xPyFc4
budeme	být	k5eAaImBp1nP
sami	sám	k3xTgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Číně	Čína	k1gFnSc6
vzniká	vznikat	k5eAaImIp3nS
Velký	velký	k2eAgMnSc1d1
Bratr	bratr	k1gMnSc1
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čínský	čínský	k2eAgInSc4d1
systém	systém	k1gInSc4
společenského	společenský	k2eAgInSc2d1
kreditu	kredit	k1gInSc2
<g/>
:	:	kIx,
Velký	velký	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
všechno	všechen	k3xTgNnSc4
uvidí	uvidět	k5eAaPmIp3nS
"	"	kIx"
<g/>
jako	jako	k8xC,k8xS
Bůh	bůh	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
není	být	k5eNaImIp3nS
důvěryhodný	důvěryhodný	k2eAgMnSc1d1
<g/>
,	,	kIx,
neudělá	udělat	k5eNaPmIp3nS
ani	ani	k8xC
krok	krok	k1gInSc4
<g/>
,	,	kIx,
hlásá	hlásat	k5eAaImIp3nS
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BECKER	BECKER	kA
<g/>
,	,	kIx,
Jasper	Jasper	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
na	na	k7c6
přelomu	přelom	k1gInSc6
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jasper	Jasper	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7257	#num#	k4
<g/>
-	-	kIx~
<g/>
729	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FAIRBANK	FAIRBANK	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
K.	K.	kA
Dějiny	dějiny	k1gFnPc1
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FISHMAN	FISHMAN	kA
<g/>
,	,	kIx,
Ted	Ted	k1gMnSc1
C.	C.	kA
China	China	k1gFnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
jak	jak	k8xC,k8xS
Čína	Čína	k1gFnSc1
drtí	drtit	k5eAaImIp3nS
Ameriku	Amerika	k1gFnSc4
a	a	k8xC
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Alfa	alfa	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86851	#num#	k4
<g/>
-	-	kIx~
<g/>
44	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LIŠČÁK	LIŠČÁK	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
109	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VOJTA	Vojta	k1gMnSc1
<g/>
,	,	kIx,
Vít	Vít	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínský	čínský	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
:	:	kIx,
jak	jak	k8xC,k8xS
porozumět	porozumět	k5eAaPmF
současné	současný	k2eAgFnSc3d1
Číně	Čína	k1gFnSc3
<g/>
,	,	kIx,
čínskému	čínský	k2eAgNnSc3d1
chování	chování	k1gNnSc3
a	a	k8xC
myšlení	myšlení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Pixl-e	Pixl-e	k1gNnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
905021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KLIMEŠ	Klimeš	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Ondřej	Ondřej	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kulturní	kulturní	k2eAgFnSc1d1
diplomacie	diplomacie	k1gFnSc1
Číny	Čína	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnPc4
regionální	regionální	k2eAgFnPc4d1
variace	variace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-200-2962-1	978-80-200-2962-1	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Spor	spor	k1gInSc1
o	o	k7c4
Čínu	Čína	k1gFnSc4
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Čínština	čínština	k1gFnSc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
čínské	čínský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
</s>
<s>
Velký	velký	k2eAgInSc1d1
skok	skok	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Čína	Čína	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc1
Čína	Čína	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Čína	Čína	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnSc1
Čína	Čína	k1gFnSc1
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
Čína	Čína	k1gFnSc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Čína	Čína	k1gFnSc1
před	před	k7c7
30	#num#	k4
lety	léto	k1gNnPc7
a	a	k8xC
dnes	dnes	k6eAd1
<g/>
:	:	kIx,
od	od	k7c2
hladomoru	hladomor	k1gInSc2
k	k	k7c3
ekonomickému	ekonomický	k2eAgInSc3d1
zázraku	zázrak	k1gInSc3
</s>
<s>
China	China	k1gFnSc1
-	-	kIx~
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc1
Report	report	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amnesty	Amnest	k1gInPc4
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
China	China	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freedom	Freedom	k1gInSc1
House	house	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
internetu	internet	k1gInSc6
vyhráváme	vyhrávat	k5eAaImIp1nP
my	my	k3xPp1nPc1
<g/>
,	,	kIx,
tvrdí	tvrdý	k2eAgMnPc1d1
čínský	čínský	k2eAgMnSc1d1
disident	disident	k1gMnSc1
</s>
<s>
Country	country	k2eAgInSc1d1
of	of	k?
Origin	Origin	k2eAgInSc1d1
Information	Information	k1gInSc1
Report	report	k1gInSc1
-	-	kIx~
China	China	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
UK	UK	kA
Border	Border	k1gInSc4
Agency	Agenca	k1gFnSc2
<g/>
,	,	kIx,
2011-08-24	2011-08-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1
Stiftung	Stiftung	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BTI	BTI	kA
2010	#num#	k4
—	—	k?
Country	country	k2eAgInSc4d1
Report	report	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gütersloh	Gütersloh	k1gMnSc1
<g/>
:	:	kIx,
Bertelsmann	Bertelsmann	k1gMnSc1
Stiftung	Stiftung	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bureau	Bureau	k6eAd1
of	of	k?
East	East	k1gMnSc1
Asian	Asian	k1gMnSc1
and	and	k?
Pacific	Pacific	k1gMnSc1
Affairs	Affairs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Background	Background	k1gInSc1
Note	Not	k1gInSc2
<g/>
:	:	kIx,
China	China	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
State	status	k1gInSc5
<g/>
,	,	kIx,
2010-08-05	2010-08-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
China	China	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2011-08-03	2011-08-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Library	Librar	k1gInPc1
of	of	k?
Congress	Congress	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Country	country	k2eAgInSc1d1
Profile	profil	k1gInSc5
<g/>
:	:	kIx,
China	China	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-08-25	2006-08-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
ČR	ČR	kA
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhrnná	souhrnný	k2eAgFnSc1d1
teritoriální	teritoriální	k2eAgFnSc1d1
informace	informace	k1gFnSc1
<g/>
:	:	kIx,
Čína	Čína	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Businessinfo	Businessinfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-04-01	2011-04-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CHAN	CHAN	kA
<g/>
,	,	kIx,
Hoklam	Hoklam	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
China	China	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopaedia	Encyclopaedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vrcholné	vrcholný	k2eAgInPc1d1
orgány	orgán	k1gInPc1
státní	státní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
legislativní	legislativní	k2eAgFnSc1d1
<g/>
:	:	kIx,
Všečínské	všečínský	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
lidových	lidový	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
(	(	kIx(
<g/>
stálý	stálý	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Všečínského	všečínský	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
lidových	lidový	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
hlava	hlava	k1gFnSc1
státu	stát	k1gInSc2
<g/>
:	:	kIx,
prezident	prezident	k1gMnSc1
•	•	k?
administrativní	administrativní	k2eAgNnSc1d1
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
•	•	k?
vojenský	vojenský	k2eAgInSc1d1
<g/>
:	:	kIx,
Ústřední	ústřední	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
•	•	k?
kontrolní	kontrolní	k2eAgFnPc4d1
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
kontrolní	kontrolní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
•	•	k?
právního	právní	k2eAgInSc2d1
dohledu	dohled	k1gInSc2
<g/>
:	:	kIx,
Nejvyšší	vysoký	k2eAgFnSc1d3
lidová	lidový	k2eAgFnSc1d1
prokuratura	prokuratura	k1gFnSc1
•	•	k?
soudní	soudní	k2eAgFnPc4d1
<g/>
:	:	kIx,
Nejvyšší	vysoký	k2eAgInSc4d3
lidový	lidový	k2eAgInSc4d1
soud	soud	k1gInSc4
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
–	–	k?
中	中	k?
(	(	kIx(
<g/>
Čung-chua	Čung-chu	k1gInSc2
žen-min	žen-min	k2eAgMnSc1d1
kung-che-kuo	kung-che-kuo	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
(	(	kIx(
<g/>
PRC	prc	k0
<g/>
)	)	kIx)
Provincie	provincie	k1gFnSc1
</s>
<s>
An	An	k?
<g/>
‑	‑	k?
<g/>
chuej	chuej	k1gInSc1
•	•	k?
Če	Če	k1gFnSc2
<g/>
‑	‑	k?
<g/>
ťiang	ťiang	k1gInSc1
•	•	k?
Čching	Čching	k1gInSc1
<g/>
‑	‑	k?
<g/>
chaj	chaj	k1gInSc1
•	•	k?
Fu	fu	k0
<g/>
‑	‑	k?
<g/>
ťien	ťien	k1gMnSc1
•	•	k?
Chaj	Chaj	k1gMnSc1
<g/>
‑	‑	k?
<g/>
nan	nan	k?
•	•	k?
Chej	Chej	k1gMnSc1
<g/>
‑	‑	k?
<g/>
lung-ťiang	lung-ťiang	k1gMnSc1
•	•	k?
Che	che	k0
<g/>
‑	‑	k?
<g/>
nan	nan	k?
•	•	k?
Che	che	k0
<g/>
‑	‑	k?
<g/>
pej	pej	k?
•	•	k?
Chu	Chu	k1gMnSc1
<g/>
‑	‑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
nan	nan	k?
•	•	k?
Chu	Chu	k1gMnSc1
<g/>
‑	‑	k?
<g/>
pej	pej	k?
•	•	k?
Jün	Jün	k1gFnSc2
<g/>
‑	‑	k?
<g/>
nan	nan	k?
•	•	k?
Kan	Kan	k1gFnSc2
<g/>
‑	‑	k?
<g/>
su	su	k?
•	•	k?
Kuang	Kuang	k1gMnSc1
<g/>
‑	‑	k?
<g/>
tung	tung	k1gMnSc1
•	•	k?
Kuej	Kuej	k1gMnSc1
<g/>
‑	‑	k?
<g/>
čou	čou	k?
•	•	k?
Liao	Liao	k6eAd1
<g/>
‑	‑	k?
<g/>
ning	ning	k1gInSc1
•	•	k?
S	s	k7c7
<g/>
’	’	k?
<g/>
‑	‑	k?
<g/>
čchuan	čchuan	k1gMnSc1
•	•	k?
Šan	Šan	k1gMnSc1
<g/>
‑	‑	k?
<g/>
si	se	k3xPyFc3
•	•	k?
Šan	Šan	k1gMnSc3
<g/>
‑	‑	k?
<g/>
tung	tung	k1gMnSc1
•	•	k?
Šen	Šen	k1gMnSc1
<g/>
‑	‑	k?
<g/>
si	se	k3xPyFc3
•	•	k?
(	(	kIx(
<g/>
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
)	)	kIx)
<g/>
¹	¹	k?
•	•	k?
Ťiang	Ťiang	k1gMnSc1
<g/>
‑	‑	k?
<g/>
si	se	k3xPyFc3
•	•	k?
Ťiang	Ťiang	k1gMnSc1
<g/>
‑	‑	k?
<g/>
su	su	k?
•	•	k?
Ťi	Ťi	k1gFnSc2
<g/>
‑	‑	k?
<g/>
lin	lin	k?
Autonomní	autonomní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Kuang	Kuang	k1gMnSc1
<g/>
‑	‑	k?
<g/>
si	se	k3xPyFc3
•	•	k?
Ning	Ning	k1gInSc1
<g/>
‑	‑	k?
<g/>
sia	sia	k?
•	•	k?
Sin	sin	kA
<g/>
‑	‑	k?
<g/>
ťiang	ťiang	k1gInSc1
•	•	k?
Tibet	Tibet	k1gInSc1
•	•	k?
Vnitřní	vnitřní	k2eAgNnSc1d1
Mongolsko	Mongolsko	k1gNnSc1
Přímo	přímo	k6eAd1
spravovaná	spravovaný	k2eAgNnPc4d1
města	město	k1gNnPc4
(	(	kIx(
<g/>
na	na	k7c6
úrovni	úroveň	k1gFnSc6
provincie	provincie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Čchung	Čchung	k1gInSc1
<g/>
‑	‑	k?
<g/>
čching	čching	k1gInSc1
•	•	k?
Peking	Peking	k1gInSc1
•	•	k?
Šanghaj	Šanghaj	k1gFnSc1
•	•	k?
Tchien	Tchien	k1gInSc1
<g/>
‑	‑	k?
<g/>
ťin	ťin	k?
Zvláštní	zvláštní	k2eAgFnSc2d1
administrativní	administrativní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Hongkong	Hongkong	k1gInSc1
•	•	k?
Macao	Macao	k1gNnSc1
¹	¹	k?
považuje	považovat	k5eAaImIp3nS
Tchaj-wan	Tchaj-wan	k1gInSc1
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
23	#num#	k4
<g/>
.	.	kIx.
provincii	provincie	k1gFnSc6
<g/>
,	,	kIx,
přestože	přestože	k8xS
jej	on	k3xPp3gMnSc4
fakticky	fakticky	k6eAd1
neovládá	ovládat	k5eNaImIp3nS
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnPc1
v	v	k7c6
Asii	Asie	k1gFnSc6
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Bahrajn	Bahrajn	k1gMnSc1
</s>
<s>
Bangladéš	Bangladéš	k1gInSc1
</s>
<s>
Bhútán	Bhútán	k1gInSc1
</s>
<s>
Brunej	Brunat	k5eAaPmRp2nS,k5eAaImRp2nS
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Egypt	Egypt	k1gInSc1
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Indie	Indie	k1gFnSc1
</s>
<s>
Indonésie	Indonésie	k1gFnSc1
</s>
<s>
Irák	Irák	k1gInSc1
</s>
<s>
Írán	Írán	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Jemen	Jemen	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
Kambodža	Kambodža	k1gFnSc1
</s>
<s>
Katar	katar	k1gMnSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Kuvajt	Kuvajt	k1gInSc1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
Laos	Laos	k1gInSc1
</s>
<s>
Libanon	Libanon	k1gInSc1
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
</s>
<s>
Maledivy	Maledivy	k1gFnPc1
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
</s>
<s>
Myanmar	Myanmar	k1gInSc1
(	(	kIx(
<g/>
Barma	Barma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nepál	Nepál	k1gInSc1
</s>
<s>
Omán	Omán	k1gInSc1
</s>
<s>
Pákistán	Pákistán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Singapur	Singapur	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
</s>
<s>
Srí	Srí	k?
Lanka	lanko	k1gNnPc1
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
</s>
<s>
Thajsko	Thajsko	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
</s>
<s>
Vietnam	Vietnam	k1gInSc1
</s>
<s>
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azávislá	azávislý	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Ashmorův	Ashmorův	k2eAgInSc4d1
a	a	k8xC
Cartierův	Cartierův	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
</s>
<s>
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Čína	Čína	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Hongkong	Hongkong	k1gInSc4
</s>
<s>
Macao	Macao	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Akrotiri	Akrotiri	k1gNnSc1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
<g/>
)	)	kIx)
Územní	územní	k2eAgInPc1d1
celky	celek	k1gInPc1
se	se	k3xPyFc4
spornýmmezinárodním	spornýmmezinárodní	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
</s>
<s>
Arcach	Arcach	k1gMnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Osetie	Osetie	k1gFnSc1
</s>
<s>
Palestina	Palestina	k1gFnSc1
</s>
<s>
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
Tchaj-wan	Tchaj-wan	k1gInSc1
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnSc1
s	s	k7c7
členstvím	členství	k1gNnSc7
v	v	k7c6
APEC	APEC	kA
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
•	•	k?
Brunej	Brunej	k1gFnSc1
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Hongkong	Hongkong	k1gInSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Papua	Papuum	k1gNnSc2
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Peru	prát	k5eAaImIp1nS
•	•	k?
Rusko	Rusko	k1gNnSc4
•	•	k?
Singapur	Singapur	k1gInSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
</s>
<s>
BRICS	BRICS	kA
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
G20	G20	k4
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Argentina	Argentina	k1gFnSc1
•	•	k?
Brazílie	Brazílie	k1gFnSc2
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Socialistické	socialistický	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
státy	stát	k1gInPc1
s	s	k7c7
komunistickými	komunistický	k2eAgInPc7d1
režimy	režim	k1gInPc7
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
vládnoucí	vládnoucí	k2eAgFnSc2d1
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
F	F	kA
–	–	k?
socialistické	socialistický	k2eAgFnSc2d1
federace	federace	k1gFnSc2
•	•	k?
V	v	k7c6
–	–	k?
členové	člen	k1gMnPc1
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
•	•	k?
X	X	kA
–	–	k?
státy	stát	k1gInPc1
většinově	většinově	k6eAd1
neuznané	uznaný	k2eNgInPc1d1
Současné	současný	k2eAgInPc1d1
socialistické	socialistický	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Čína	Čína	k1gFnSc1
mimo	mimo	k7c4
Tchaj-wan	Tchaj-wan	k1gInSc4
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Číny	Čína	k1gFnSc2
•	•	k?
Kuba	Kuba	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Kuby	Kuba	k1gFnSc2
•	•	k?
Laos	Laos	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Laoská	laoský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Korejská	korejský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
•	•	k?
Vietnam	Vietnam	k1gInSc1
(	(	kIx(
<g/>
sever	sever	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
1954	#num#	k4
<g/>
,	,	kIx,
celá	celý	k2eAgFnSc1d1
země	země	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Vietnamu	Vietnam	k1gInSc2
Bývalé	bývalý	k2eAgInPc1d1
socialistické	socialistický	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
Angola	Angola	k1gFnSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Angolská	angolský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
MPLA	MPLA	kA
•	•	k?
Benin	Benin	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Benin	Benina	k1gFnPc2
/	/	kIx~
Lidová	lidový	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Beninu	Benin	k1gInSc2
•	•	k?
Etiopie	Etiopie	k1gFnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Derg	Derg	k1gInSc1
<g/>
;	;	kIx,
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Etiopská	etiopský	k2eAgFnSc1d1
lidově	lidově	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Etiopie	Etiopie	k1gFnSc1
•	•	k?
Kongo	Kongo	k1gNnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Konžská	konžský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Konžská	konžský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
•	•	k?
Mosambik	Mosambik	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Mosambická	mosambický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
FRELIMO	FRELIMO	kA
•	•	k?
Somálsko	Somálsko	k1gNnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Somálská	somálský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Somálská	somálský	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Grenada	Grenada	k1gFnSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Lidová	lidový	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Grenady	Grenada	k1gFnSc2
/	/	kIx~
Nové	Nové	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
JEWEL	JEWEL	kA
Asie	Asie	k1gFnSc2
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Afghánská	afghánský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Lidová	lidový	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Afghánistánu	Afghánistán	k1gInSc2
•	•	k?
Íránský	íránský	k2eAgInSc1d1
Ázerbájdžán	Ázerbájdžán	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Lidová	lidový	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Ázerbájdžánu	Ázerbájdžán	k1gInSc2
X	X	kA
•	•	k?
Jižní	jižní	k2eAgInSc4d1
Jemen	Jemen	k1gInSc4
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Jemenská	jemenský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Jemenská	jemenský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Demokratická	demokratický	k2eAgFnSc1d1
Kampučia	Kampučia	k1gFnSc1
/	/	kIx~
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Kampučie	Kampučie	k1gFnSc2
<g/>
;	;	kIx,
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Kampučská	kampučský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Kampučská	kampučský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Mongolská	mongolský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Mongolská	mongolský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Persie	Persie	k1gFnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Perská	perský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
X	X	kA
•	•	k?
Tuva	Tuva	k1gMnSc1
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Tuvinská	Tuvinský	k2eAgFnSc1d1
aratská	aratský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
X	X	kA
/	/	kIx~
Tuvinská	Tuvinský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánie	Albánie	k1gFnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Albánská	albánský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Albánská	albánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
•	•	k?
Alsasko	Alsasko	k1gNnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Alsaská	alsaský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
rad	rada	k1gFnPc2
X	X	kA
•	•	k?
Bavorsko	Bavorsko	k1gNnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Bavorská	bavorský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
rad	rada	k1gFnPc2
X	X	kA
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Bulharsko	Bulharsko	k1gNnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Bulharská	bulharský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Bulharská	bulharský	k2eAgFnSc1d1
komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
F	F	kA
V	v	k7c4
/	/	kIx~
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
•	•	k?
Finsko	Finsko	k1gNnSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
–	–	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Finská	finský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
X	X	kA
•	•	k?
Halič	halič	k1gMnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Haličská	haličský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
X	X	kA
•	•	k?
Jugoslávie	Jugoslávie	k1gFnSc2
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Socialistická	socialistický	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jugoslávie	Jugoslávie	k1gFnSc2
F	F	kA
/	/	kIx~
Svaz	svaz	k1gInSc1
komunistů	komunista	k1gMnPc2
Jugoslávie	Jugoslávie	k1gFnSc2
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Maďarská	maďarský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
rad	rada	k1gFnPc2
X	X	kA
<g/>
;	;	kIx,
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Maďarská	maďarský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Maďarská	maďarský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
pracujících	pracující	k1gMnPc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Maďarská	maďarský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
po	po	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Východní	východní	k2eAgInSc4d1
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Německá	německý	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Sjednocená	sjednocený	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
•	•	k?
Polsko	Polsko	k1gNnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Polská	polský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Polská	polský	k2eAgFnSc1d1
sjednocená	sjednocený	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Rumunska	Rumunsko	k1gNnSc2
•	•	k?
Rusko	Rusko	k1gNnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
impérium	impérium	k1gNnSc4
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
F	F	kA
V	V	kA
/	/	kIx~
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
•	•	k?
Slovensko	Slovensko	k1gNnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
rad	rada	k1gFnPc2
X	X	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Čína	Čína	k1gFnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
131732	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4009937-4	4009937-4	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2160	#num#	k4
7811	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79091151	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
132441531	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79091151	#num#	k4
</s>
