<s>
Dílo	dílo	k1gNnSc1	dílo
spadající	spadající	k2eAgNnSc1d1	spadající
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
obvykle	obvykle	k6eAd1	obvykle
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
romantické	romantický	k2eAgNnSc1d1	romantické
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
<g/>
,	,	kIx,	,
mezihvězdné	mezihvězdný	k2eAgNnSc1d1	mezihvězdné
cestování	cestování	k1gNnSc1	cestování
<g/>
,	,	kIx,	,
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
bitvy	bitva	k1gFnPc1	bitva
a	a	k8xC	a
příběh	příběh	k1gInSc1	příběh
točící	točící	k2eAgFnSc2d1	točící
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
osobních	osobní	k2eAgNnPc2d1	osobní
dramat	drama	k1gNnPc2	drama
<g/>
.	.	kIx.	.
</s>
