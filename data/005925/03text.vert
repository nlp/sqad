<s>
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
opera	opera	k1gFnSc1	opera
nebo	nebo	k8xC	nebo
Space	Space	k1gFnSc1	Space
opera	opera	k1gFnSc1	opera
je	být	k5eAaImIp3nS	být
žánr	žánr	k1gInSc4	žánr
fantastiky	fantastika	k1gFnSc2	fantastika
úzce	úzko	k6eAd1	úzko
spojený	spojený	k2eAgMnSc1d1	spojený
se	se	k3xPyFc4	se
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
spadající	spadající	k2eAgNnSc1d1	spadající
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
obvykle	obvykle	k6eAd1	obvykle
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
romantické	romantický	k2eAgNnSc1d1	romantické
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
<g/>
,	,	kIx,	,
mezihvězdné	mezihvězdný	k2eAgNnSc1d1	mezihvězdné
cestování	cestování	k1gNnSc1	cestování
<g/>
,	,	kIx,	,
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
bitvy	bitva	k1gFnPc1	bitva
a	a	k8xC	a
příběh	příběh	k1gInSc1	příběh
točící	točící	k2eAgFnSc2d1	točící
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
osobních	osobní	k2eAgNnPc2d1	osobní
dramat	drama	k1gNnPc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
opera	opera	k1gFnSc1	opera
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
hanlivý	hanlivý	k2eAgInSc1d1	hanlivý
termín	termín	k1gInSc1	termín
(	(	kIx(	(
<g/>
variace	variace	k1gFnSc1	variace
na	na	k7c4	na
"	"	kIx"	"
<g/>
koňskou	koňský	k2eAgFnSc4d1	koňská
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
mýdlovou	mýdlový	k2eAgFnSc4d1	mýdlová
operu	opera	k1gFnSc4	opera
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
zavedený	zavedený	k2eAgInSc1d1	zavedený
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
americkým	americký	k2eAgMnSc7d1	americký
spisovatelem	spisovatel	k1gMnSc7	spisovatel
sci-fi	scii	k1gFnSc2	sci-fi
Wilsonem	Wilson	k1gMnSc7	Wilson
Tuckerem	Tucker	k1gMnSc7	Tucker
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
od	od	k7c2	od
"	"	kIx"	"
<g/>
vážné	vážná	k1gFnSc2	vážná
<g/>
"	"	kIx"	"
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
zaměřující	zaměřující	k2eAgFnSc2d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
důsledky	důsledek	k1gInPc4	důsledek
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
bádání	bádání	k1gNnSc2	bádání
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
se	se	k3xPyFc4	se
však	však	k9	však
vzápětí	vzápětí	k6eAd1	vzápětí
posunul	posunout	k5eAaPmAgInS	posunout
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
všech	všecek	k3xTgNnPc2	všecek
dobrodružných	dobrodružný	k2eAgNnPc2d1	dobrodružné
sci-fi	scii	k1gNnPc2	sci-fi
románů	román	k1gInPc2	román
z	z	k7c2	z
kosmu	kosmos	k1gInSc2	kosmos
<g/>
,	,	kIx,	,
popisujících	popisující	k2eAgInPc2d1	popisující
meziplanetární	meziplanetární	k2eAgInSc4d1	meziplanetární
konflikt	konflikt	k1gInSc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
"	"	kIx"	"
<g/>
vážnou	vážný	k2eAgFnSc4d1	vážná
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
nevážnou	vážný	k2eNgFnSc4d1	nevážná
<g/>
"	"	kIx"	"
sci-fi	scii	k1gFnSc4	sci-fi
však	však	k9	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
tenká	tenký	k2eAgFnSc1d1	tenká
hranice	hranice	k1gFnSc1	hranice
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
autorů	autor	k1gMnPc2	autor
úspěšně	úspěšně	k6eAd1	úspěšně
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
dobrodružný	dobrodružný	k2eAgInSc4d1	dobrodružný
příběh	příběh	k1gInSc4	příběh
s	s	k7c7	s
vědeckými	vědecký	k2eAgInPc7d1	vědecký
elementy	element	k1gInPc7	element
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
původní	původní	k2eAgFnSc2d1	původní
space	space	k1gFnSc2	space
opery	opera	k1gFnSc2	opera
jsou	být	k5eAaImIp3nP	být
považována	považován	k2eAgNnPc1d1	považováno
třicátá	třicátý	k4xOgFnSc1	třicátý
léta	léto	k1gNnSc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
ukázkou	ukázka	k1gFnSc7	ukázka
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
opery	opera	k1gFnSc2	opera
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
novela	novela	k1gFnSc1	novela
R.	R.	kA	R.
Cummingse	Cummingse	k1gFnSc2	Cummingse
Piráti	pirát	k1gMnPc1	pirát
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
pozdější	pozdní	k2eAgInSc4d2	pozdější
román	román	k1gInSc4	román
Loď	loď	k1gFnSc1	loď
Jacka	Jacka	k1gFnSc1	Jacka
Williamsona	Williamsona	k1gFnSc1	Williamsona
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
opomenout	opomenout	k5eAaPmF	opomenout
ani	ani	k8xC	ani
série	série	k1gFnSc1	série
Skylark	Skylark	k1gInSc1	Skylark
a	a	k8xC	a
Lensman	Lensman	k1gMnSc1	Lensman
E.	E.	kA	E.
E.	E.	kA	E.
Smithe	Smithe	k1gNnSc4	Smithe
a	a	k8xC	a
díla	dílo	k1gNnPc4	dílo
autorů	autor	k1gMnPc2	autor
jako	jako	k9	jako
Edmond	Edmond	k1gMnSc1	Edmond
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
W.	W.	kA	W.
Campbell	Campbell	k1gMnSc1	Campbell
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Hamilton	Hamilton	k1gInSc1	Hamilton
Brackett	Brackett	k2eAgInSc1d1	Brackett
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
nové	nový	k2eAgFnSc2d1	nová
space	space	k1gFnSc2	space
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tu	ten	k3xDgFnSc4	ten
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
kombinace	kombinace	k1gFnSc1	kombinace
prvků	prvek	k1gInPc2	prvek
původní	původní	k2eAgFnSc2d1	původní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
opery	opera	k1gFnSc2	opera
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
hard	hard	k6eAd1	hard
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
–	–	k?	–
tento	tento	k3xDgInSc4	tento
nový	nový	k2eAgInSc4d1	nový
podžánr	podžánr	k1gInSc4	podžánr
tedy	tedy	k8xC	tedy
více	hodně	k6eAd2	hodně
dbá	dbát	k5eAaImIp3nS	dbát
na	na	k7c4	na
technologickou	technologický	k2eAgFnSc4d1	technologická
správnost	správnost	k1gFnSc4	správnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
představitele	představitel	k1gMnPc4	představitel
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Stephen	Stephen	k1gInSc1	Stephen
R.	R.	kA	R.
Donaldson	Donaldson	k1gInSc1	Donaldson
<g/>
,	,	kIx,	,
Dan	Dan	k1gMnSc1	Dan
Simmons	Simmons	k1gInSc1	Simmons
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Varley	Varlea	k1gFnSc2	Varlea
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Brin	Brin	k1gMnSc1	Brin
<g/>
,	,	kIx,	,
Iain	Iain	k1gMnSc1	Iain
Banks	Banksa	k1gFnPc2	Banksa
<g/>
,	,	kIx,	,
Catherine	Catherin	k1gInSc5	Catherin
Asaroová	Asaroová	k1gFnSc5	Asaroová
<g/>
,	,	kIx,	,
Orson	Orson	k1gMnSc1	Orson
Scott	Scott	k1gMnSc1	Scott
Card	Card	k1gMnSc1	Card
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Clute	Clut	k1gInSc5	Clut
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Stross	Stross	k1gInSc1	Stross
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Michael	Michael	k1gMnSc1	Michael
Straczynski	Straczynsk	k1gFnSc2	Straczynsk
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
F.	F.	kA	F.
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
,	,	kIx,	,
Lois	Lois	k1gInSc1	Lois
McMaster	McMaster	k1gInSc1	McMaster
Bujoldová	Bujoldová	k1gFnSc1	Bujoldová
<g/>
,	,	kIx,	,
M.	M.	kA	M.
John	John	k1gMnSc1	John
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
M.	M.	kA	M.
Kingsbury	Kingsbura	k1gFnPc1	Kingsbura
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
Ken	Ken	k1gMnSc1	Ken
MacLeod	MacLeod	k1gInSc1	MacLeod
<g/>
,	,	kIx,	,
Alastair	Alastair	k1gInSc1	Alastair
Reynolds	Reynolds	k1gInSc1	Reynolds
<g/>
,	,	kIx,	,
Mike	Mike	k1gNnSc1	Mike
Resnick	Resnicka	k1gFnPc2	Resnicka
<g/>
,	,	kIx,	,
a	a	k8xC	a
C.	C.	kA	C.
J.	J.	kA	J.
Cherryh	Cherryh	k1gMnSc1	Cherryh
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgMnSc7d1	důležitý
přispěvatelem	přispěvatel	k1gMnSc7	přispěvatel
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
opery	opera	k1gFnSc2	opera
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
také	také	k9	také
anime	aniit	k5eAaBmRp1nP	aniit
<g/>
.	.	kIx.	.
</s>
<s>
Věrnost	věrnost	k1gFnSc4	věrnost
vědeckým	vědecký	k2eAgInPc3d1	vědecký
poznatkům	poznatek	k1gInPc3	poznatek
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
žánru	žánr	k1gInSc6	žánr
liší	lišit	k5eAaImIp3nS	lišit
doslova	doslova	k6eAd1	doslova
případ	případ	k1gInSc1	případ
od	od	k7c2	od
případu	případ	k1gInSc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
porušením	porušení	k1gNnSc7	porušení
známých	známý	k2eAgInPc2d1	známý
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
zákonů	zákon	k1gInPc2	zákon
možnost	možnost	k1gFnSc4	možnost
cestovat	cestovat	k5eAaImF	cestovat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
příběhy	příběh	k1gInPc1	příběh
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
od	od	k7c2	od
reality	realita	k1gFnSc2	realita
odklánějí	odklánět	k5eAaImIp3nP	odklánět
podstatně	podstatně	k6eAd1	podstatně
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
existují	existovat	k5eAaImIp3nP	existovat
mystické	mystický	k2eAgFnPc4d1	mystická
síly	síla	k1gFnPc4	síla
schopné	schopný	k2eAgFnSc2d1	schopná
zničit	zničit	k5eAaPmF	zničit
celé	celý	k2eAgFnPc4d1	celá
planety	planeta	k1gFnPc4	planeta
a	a	k8xC	a
vyhladit	vyhladit	k5eAaPmF	vyhladit
celé	celý	k2eAgFnSc2d1	celá
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Star	star	k1gInSc1	star
Wars	Wars	k1gInSc1	Wars
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
mystickou	mystický	k2eAgFnSc7d1	mystická
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
Hvězdou	hvězda	k1gFnSc7	hvězda
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
bližší	blízký	k2eAgFnSc1d2	bližší
pohádce	pohádka	k1gFnSc3	pohádka
než	než	k8xS	než
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
rysech	rys	k1gInPc6	rys
se	se	k3xPyFc4	se
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
opery	opera	k1gFnSc2	opera
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
velmi	velmi	k6eAd1	velmi
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
najdeme	najít	k5eAaPmIp1nP	najít
exempláře	exemplář	k1gInPc1	exemplář
s	s	k7c7	s
propracovaným	propracovaný	k2eAgInSc7d1	propracovaný
vývojem	vývoj	k1gInSc7	vývoj
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
psychologií	psychologie	k1gFnPc2	psychologie
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc1	jaký
píší	psát	k5eAaImIp3nP	psát
Lois	Lois	k1gInSc4	Lois
McMaster	McMaster	k1gInSc1	McMaster
Bujoldová	Bujoldová	k1gFnSc1	Bujoldová
a	a	k8xC	a
Iain	Iain	k1gInSc1	Iain
M.	M.	kA	M.
Banks	Banks	k1gInSc1	Banks
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
těchto	tento	k3xDgInPc2	tento
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
rysem	rys	k1gInSc7	rys
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
oper	opera	k1gFnPc2	opera
jsou	být	k5eAaImIp3nP	být
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
bitvy	bitva	k1gFnPc1	bitva
s	s	k7c7	s
futuristickými	futuristický	k2eAgFnPc7d1	futuristická
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
doktrínou	doktrína	k1gFnSc7	doktrína
a	a	k8xC	a
taktikou	taktika	k1gFnSc7	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
brána	brána	k1gFnSc1	brána
natolik	natolik	k6eAd1	natolik
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgInPc1	takový
příběhy	příběh	k1gInPc1	příběh
tvoří	tvořit	k5eAaImIp3nP	tvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kategorii	kategorie	k1gFnSc4	kategorie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sci-fi	scii	k1gFnSc2	sci-fi
–	–	k?	–
tzv.	tzv.	kA	tzv.
military	militara	k1gFnSc2	militara
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
opery	opera	k1gFnSc2	opera
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
také	také	k9	také
mnohé	mnohý	k2eAgInPc1d1	mnohý
z	z	k7c2	z
oblíbených	oblíbený	k2eAgInPc2d1	oblíbený
seriálů	seriál	k1gInPc2	seriál
–	–	k?	–
Battlestar	Battlestara	k1gFnPc2	Battlestara
Galacticou	Galacticá	k1gFnSc4	Galacticá
počínaje	počínaje	k7c7	počínaje
a	a	k8xC	a
Star	star	k1gFnPc3	star
Trekem	Treko	k1gNnSc7	Treko
nekonče	končit	k5eNaImSgMnS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
snad	snad	k9	snad
každý	každý	k3xTgInSc4	každý
žánr	žánr	k1gInSc4	žánr
literatury	literatura	k1gFnSc2	literatura
i	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
objevila	objevit	k5eAaPmAgNnP	objevit
mnohá	mnohý	k2eAgNnPc1d1	mnohé
klišé	klišé	k1gNnPc1	klišé
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
tato	tento	k3xDgNnPc1	tento
klišé	klišé	k1gNnPc1	klišé
úspěšně	úspěšně	k6eAd1	úspěšně
parodovali	parodovat	k5eAaImAgMnP	parodovat
autoři	autor	k1gMnPc1	autor
jako	jako	k8xC	jako
Harry	Harra	k1gMnSc2	Harra
Harrison	Harrison	k1gMnSc1	Harrison
a	a	k8xC	a
Douglas	Douglas	k1gMnSc1	Douglas
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
napsal	napsat	k5eAaPmAgMnS	napsat
Jack	Jack	k1gMnSc1	Jack
Vance	Vance	k1gMnSc1	Vance
příběh	příběh	k1gInSc4	příběh
Space	Space	k1gFnSc2	Space
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tento	tento	k3xDgInSc1	tento
žánr	žánr	k1gInSc1	žánr
paroduje	parodovat	k5eAaImIp3nS	parodovat
popisováním	popisování	k1gNnSc7	popisování
intergalaktické	intergalaktický	k2eAgFnSc2d1	intergalaktická
operní	operní	k2eAgFnSc2d1	operní
společnosti	společnost	k1gFnSc2	společnost
přinášející	přinášející	k2eAgFnSc4d1	přinášející
kulturu	kultura	k1gFnSc4	kultura
opuštěným	opuštěný	k2eAgInPc3d1	opuštěný
světům	svět	k1gInPc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Fire	Fire	k1gNnSc1	Fire
Upon	Upon	k1gMnSc1	Upon
the	the	k?	the
Deep	Deep	k1gMnSc1	Deep
a	a	k8xC	a
A	a	k9	a
Deepness	Deepness	k1gInSc1	Deepness
in	in	k?	in
the	the	k?	the
Sky	Sky	k1gFnSc1	Sky
(	(	kIx(	(
<g/>
Vernor	Vernor	k1gInSc1	Vernor
Vinge	Vinge	k1gNnSc1	Vinge
<g/>
)	)	kIx)	)
Childe	Child	k1gMnSc5	Child
Cycle	Cycl	k1gMnSc5	Cycl
(	(	kIx(	(
<g/>
Gordon	Gordon	k1gMnSc1	Gordon
Dickson	Dickson	k1gMnSc1	Dickson
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
A	a	k8xC	a
Million	Million	k1gInSc1	Million
Open	Opena	k1gFnPc2	Opena
Doors	Doors	k1gInSc1	Doors
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Barnes	Barnes	k1gMnSc1	Barnes
<g/>
)	)	kIx)	)
Vesmír	vesmír	k1gInSc1	vesmír
Aliance	aliance	k1gFnSc2	aliance
a	a	k8xC	a
Unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
C.	C.	kA	C.
J.	J.	kA	J.
Cherryh	Cherryh	k1gMnSc1	Cherryh
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Barsoom	Barsoom	k1gInSc1	Barsoom
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Edgar	Edgar	k1gMnSc1	Edgar
Rice	Ric	k1gFnSc2	Ric
Burroughs	Burroughs	k1gInSc1	Burroughs
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Války	válka	k1gFnSc2	válka
s	s	k7c7	s
Berserky	berserk	k1gMnPc7	berserk
(	(	kIx(	(
<g/>
Fred	Fred	k1gMnSc1	Fred
Saberhagen	Saberhagen	k1gInSc1	Saberhagen
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Cities	Cities	k1gMnSc1	Cities
in	in	k?	in
Flight	Flight	k1gMnSc1	Flight
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
Blish	Blish	k1gMnSc1	Blish
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Culture	Cultur	k1gMnSc5	Cultur
(	(	kIx(	(
<g/>
Iain	Iain	k1gInSc4	Iain
M.	M.	kA	M.
Banks	Banks	k1gInSc1	Banks
<g/>
)	)	kIx)	)
Série	série	k1gFnSc1	série
o	o	k7c6	o
Heechee	Heechee	k1gNnSc6	Heechee
(	(	kIx(	(
<g/>
Frederik	Frederik	k1gMnSc1	Frederik
Pohl	Pohl	k1gMnSc1	Pohl
<g/>
)	)	kIx)	)
trilogie	trilogie	k1gFnSc1	trilogie
Dies	Dies	k1gInSc1	Dies
Irae	Irae	k1gFnSc1	Irae
(	(	kIx(	(
<g/>
Brian	Brian	k1gMnSc1	Brian
<g />
.	.	kIx.	.
</s>
<s>
Stableford	Stableford	k1gInSc1	Stableford
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Dorsai	Dorsa	k1gFnSc2	Dorsa
(	(	kIx(	(
<g/>
Gordon	Gordon	k1gMnSc1	Gordon
Dickson	Dickson	k1gMnSc1	Dickson
<g/>
)	)	kIx)	)
Série	série	k1gFnSc1	série
Duna	duna	k1gFnSc1	duna
(	(	kIx(	(
<g/>
Frank	Frank	k1gMnSc1	Frank
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Enderova	Enderův	k2eAgFnSc1d1	Enderova
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
Orson	Orson	k1gMnSc1	Orson
Scott	Scott	k1gMnSc1	Scott
Card	Card	k1gMnSc1	Card
<g/>
)	)	kIx)	)
Série	série	k1gFnSc1	série
o	o	k7c6	o
Nadaci	nadace	k1gFnSc6	nadace
(	(	kIx(	(
<g/>
Základna	základna	k1gFnSc1	základna
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Isaac	Isaac	k1gInSc1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Gorrideon	Gorrideon	k1gMnSc1	Gorrideon
(	(	kIx(	(
<g/>
Barry	Barra	k1gMnSc2	Barra
Stephen	Stephen	k2eAgInSc1d1	Stephen
Nieuport	Nieuport	k1gInSc1	Nieuport
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
the	the	k?	the
Galactic	Galactice	k1gFnPc2	Galactice
Heroes	Heroes	k1gMnSc1	Heroes
(	(	kIx(	(
<g/>
Tanaka	Tanak	k1gMnSc2	Tanak
Yoshiki	Yoshik	k1gFnSc2	Yoshik
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
také	také	k9	také
manga	mango	k1gNnSc2	mango
a	a	k8xC	a
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
Série	série	k1gFnSc1	série
o	o	k7c4	o
Honor	honor	k1gInSc4	honor
Harringtonové	Harringtonový	k2eAgFnPc1d1	Harringtonová
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Weber	Weber	k1gMnSc1	Weber
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Humanx	Humanx	k1gInSc1	Humanx
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
(	(	kIx(	(
<g/>
Alan	Alan	k1gMnSc1	Alan
Dean	Dean	k1gMnSc1	Dean
Foster	Foster	k1gMnSc1	Foster
<g/>
)	)	kIx)	)
Kantos	Kantos	k1gInSc1	Kantos
Hyperionu	Hyperion	k1gInSc2	Hyperion
<g/>
,	,	kIx,	,
Ílion	Ílion	k1gMnSc1	Ílion
(	(	kIx(	(
<g/>
Dan	Dan	k1gMnSc1	Dan
Simmons	Simmonsa	k1gFnPc2	Simmonsa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Known	Known	k1gMnSc1	Known
Space	Space	k1gMnSc1	Space
<g/>
,	,	kIx,	,
Války	válka	k1gFnPc1	válka
s	s	k7c7	s
Kzinty	Kzint	k1gMnPc7	Kzint
(	(	kIx(	(
<g/>
Larry	Larr	k1gInPc4	Larr
Niven	Niven	k1gInSc1	Niven
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Lensman	Lensman	k1gMnSc1	Lensman
(	(	kIx(	(
<g/>
E.	E.	kA	E.
E.	E.	kA	E.
Smith	Smith	k1gMnSc1	Smith
<g/>
)	)	kIx)	)
Light	Light	k1gMnSc1	Light
(	(	kIx(	(
<g/>
M.	M.	kA	M.
John	John	k1gMnSc1	John
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Mageworld	Mageworld	k1gMnSc1	Mageworld
(	(	kIx(	(
<g/>
Debra	Debra	k1gFnSc1	Debra
Doyle	Doyle	k1gFnSc1	Doyle
a	a	k8xC	a
James	James	k1gMnSc1	James
D.	D.	kA	D.
Macdonald	Macdonald	k1gMnSc1	Macdonald
<g/>
)	)	kIx)	)
Tříska	Tříska	k1gMnSc1	Tříska
v	v	k7c6	v
božím	boží	k2eAgNnSc6d1	boží
oku	oko	k1gNnSc6	oko
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Larry	Larr	k1gInPc1	Larr
Niven	Nivna	k1gFnPc2	Nivna
a	a	k8xC	a
Jerry	Jerra	k1gFnSc2	Jerra
Pournelle	Pournelle	k1gFnSc2	Pournelle
<g/>
)	)	kIx)	)
trilogie	trilogie	k1gFnPc1	trilogie
Úsvit	úsvit	k1gInSc4	úsvit
noci	noc	k1gFnSc2	noc
a	a	k8xC	a
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
Saga	Saga	k1gMnSc1	Saga
(	(	kIx(	(
<g/>
Peter	Peter	k1gMnSc1	Peter
F.	F.	kA	F.
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
)	)	kIx)	)
Odhalený	odhalený	k2eAgInSc1d1	odhalený
vesmír	vesmír	k1gInSc1	vesmír
(	(	kIx(	(
<g/>
Alastair	Alastair	k1gInSc1	Alastair
Reynolds	Reynolds	k1gInSc1	Reynolds
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Noon	Noona	k1gFnPc2	Noona
Universe	Universe	k1gFnSc2	Universe
série	série	k1gFnSc2	série
Perry	Perra	k1gFnSc2	Perra
Rhodan	Rhodan	k1gInSc1	Rhodan
Rim	Rim	k1gMnSc1	Rim
worlds	worlds	k1gInSc1	worlds
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Bertram	Bertram	k1gInSc1	Bertram
Chandler	Chandler	k1gInSc1	Chandler
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Saga	Saga	k1gMnSc1	Saga
of	of	k?	of
Pliocene	Pliocen	k1gInSc5	Pliocen
Exile	exil	k1gInSc5	exil
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Julian	Julian	k1gMnSc1	Julian
Mayová	Mayová	k1gFnSc1	Mayová
<g/>
)	)	kIx)	)
Serrano	Serrana	k1gFnSc5	Serrana
Legacy	Legacy	k1gInPc1	Legacy
(	(	kIx(	(
<g/>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Moonová	Moonová	k1gFnSc1	Moonová
<g/>
)	)	kIx)	)
Uplift	Uplift	k1gInSc1	Uplift
Universe	Universe	k1gFnSc2	Universe
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Brin	Brin	k1gMnSc1	Brin
<g/>
)	)	kIx)	)
série	série	k1gFnSc1	série
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
Milese	Milese	k1gFnSc1	Milese
Vorkosigana	Vorkosigana	k1gFnSc1	Vorkosigana
(	(	kIx(	(
<g/>
Lois	Lois	k1gInSc1	Lois
McMaster	McMaster	k1gInSc1	McMaster
Bujoldová	Bujoldová	k1gFnSc1	Bujoldová
<g/>
)	)	kIx)	)
série	série	k1gFnSc2	série
Lord	lord	k1gMnSc1	lord
Morituri	Moritur	k1gFnSc2	Moritur
(	(	kIx(	(
<g/>
Simon	Simon	k1gMnSc1	Simon
R.	R.	kA	R.
Green	Grena	k1gFnPc2	Grena
<g/>
)	)	kIx)	)
Galactic	Galactice	k1gFnPc2	Galactice
Center	centrum	k1gNnPc2	centrum
Saga	Saga	k1gFnSc1	Saga
(	(	kIx(	(
<g/>
Gregory	Gregor	k1gMnPc4	Gregor
Benford	Benforda	k1gFnPc2	Benforda
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Gap	Gap	k1gFnSc1	Gap
Cycle	Cycle	k1gFnSc1	Cycle
(	(	kIx(	(
<g/>
Stephen	Stephen	k1gInSc1	Stephen
R.	R.	kA	R.
Donaldson	Donaldson	k1gInSc1	Donaldson
<g/>
)	)	kIx)	)
Saga	Saga	k1gFnSc1	Saga
of	of	k?	of
Seven	Seven	k1gInSc1	Seven
Suns	Suns	k1gInSc1	Suns
(	(	kIx(	(
<g/>
Kevin	Kevin	k1gMnSc1	Kevin
J.	J.	kA	J.
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
)	)	kIx)	)
Dan	Dan	k1gMnSc1	Dan
Dare	dar	k1gInSc5	dar
(	(	kIx(	(
<g/>
Frank	Frank	k1gMnSc1	Frank
Hampson	Hampson	k1gMnSc1	Hampson
<g/>
)	)	kIx)	)
Buck	Buck	k1gInSc1	Buck
Rogers	Rogers	k1gInSc1	Rogers
(	(	kIx(	(
<g/>
Dick	Dick	k1gInSc1	Dick
Calkins	Calkins	k1gInSc1	Calkins
<g/>
)	)	kIx)	)
Flash	Flash	k1gMnSc1	Flash
Gordon	Gordon	k1gMnSc1	Gordon
(	(	kIx(	(
<g/>
Alex	Alex	k1gMnSc1	Alex
Raymond	Raymond	k1gMnSc1	Raymond
<g/>
)	)	kIx)	)
Schlock	Schlock	k1gMnSc1	Schlock
Mercenary	Mercenara	k1gFnSc2	Mercenara
Valérian	Valérian	k1gMnSc1	Valérian
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Spatio-Temporal	Spatio-Temporat	k5eAaPmAgMnS	Spatio-Temporat
Agent	agent	k1gMnSc1	agent
(	(	kIx(	(
<g/>
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Mezieres	Mezieres	k1gMnSc1	Mezieres
a	a	k8xC	a
Pierre	Pierr	k1gInSc5	Pierr
Christin	Christin	k1gMnSc1	Christin
<g/>
)	)	kIx)	)
Andromeda	Andromeda	k1gMnSc1	Andromeda
Stoupající	stoupající	k2eAgMnSc1d1	stoupající
(	(	kIx(	(
<g/>
Gene	gen	k1gInSc5	gen
Roddenberry	Roddenberra	k1gFnPc1	Roddenberra
<g/>
)	)	kIx)	)
Babylon	Babylon	k1gInSc1	Babylon
5	[number]	k4	5
(	(	kIx(	(
<g/>
J.	J.	kA	J.
Michael	Michael	k1gMnSc1	Michael
Straczynski	Straczynsk	k1gFnSc2	Straczynsk
<g/>
)	)	kIx)	)
Banner	banner	k1gInSc1	banner
of	of	k?	of
the	the	k?	the
Stars	Stars	k1gInSc1	Stars
aka	aka	k?	aka
Seikai	Seikai	k1gNnSc2	Seikai
no	no	k9	no
Senki	Senki	k1gNnSc2	Senki
(	(	kIx(	(
<g/>
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
Banner	banner	k1gInSc1	banner
of	of	k?	of
the	the	k?	the
Stars	Starsa	k1gFnPc2	Starsa
II	II	kA	II
aka	aka	k?	aka
Seikai	Seikai	k1gNnSc1	Seikai
no	no	k9	no
Senki	Senki	k1gNnSc1	Senki
<g />
.	.	kIx.	.
</s>
<s>
II	II	kA	II
(	(	kIx(	(
<g/>
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
Battlestar	Battlestar	k1gInSc1	Battlestar
Galactica	Galactic	k1gInSc2	Galactic
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
2003-2009	[number]	k4	2003-2009
Blake	Blake	k1gNnSc2	Blake
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
7	[number]	k4	7
Crest	Crest	k1gMnSc1	Crest
of	of	k?	of
the	the	k?	the
Stars	Stars	k1gInSc1	Stars
aka	aka	k?	aka
Seikai	Seikai	k1gNnSc1	Seikai
no	no	k9	no
Monshou	Monsha	k1gFnSc7	Monsha
(	(	kIx(	(
<g/>
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
Farscape	Farscap	k1gMnSc5	Farscap
Firefly	Firefly	k1gMnSc5	Firefly
(	(	kIx(	(
<g/>
Joss	Jossa	k1gFnPc2	Jossa
Whedon	Whedon	k1gInSc1	Whedon
<g/>
)	)	kIx)	)
Gundam	Gundam	k1gInSc1	Gundam
Harlock	Harlock	k1gMnSc1	Harlock
Saga	Saga	k1gMnSc1	Saga
(	(	kIx(	(
<g/>
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Kovboj	kovboj	k1gMnSc1	kovboj
Bebop	bebop	k1gInSc1	bebop
(	(	kIx(	(
<g/>
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
the	the	k?	the
Galactic	Galactice	k1gFnPc2	Galactice
Heroes	Heroes	k1gMnSc1	Heroes
(	(	kIx(	(
<g/>
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
Lexx	Lexx	k1gInSc1	Lexx
The	The	k1gFnSc2	The
Super	super	k2eAgInSc1d1	super
Dimension	Dimension	k1gInSc1	Dimension
Fortress	Fortressa	k1gFnPc2	Fortressa
Macross	Macross	k1gInSc1	Macross
(	(	kIx(	(
<g/>
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
Outlaw	Outlaw	k1gFnSc3	Outlaw
Star	Star	kA	Star
(	(	kIx(	(
<g/>
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
Robotech	robot	k1gInPc6	robot
(	(	kIx(	(
<g/>
anime	aniit	k5eAaImRp1nP	aniit
<g/>
)	)	kIx)	)
Star	star	k1gFnSc4	star
Trek	Treka	k1gFnPc2	Treka
(	(	kIx(	(
<g/>
Gene	gen	k1gInSc5	gen
Roddenberry	Roddenberra	k1gFnPc1	Roddenberra
<g/>
)	)	kIx)	)
Stargate	Stargat	k1gInSc5	Stargat
<g />
.	.	kIx.	.
</s>
<s>
SG-1	SG-1	k4	SG-1
Stargate	Stargat	k1gInSc5	Stargat
Atlantis	Atlantis	k1gFnSc1	Atlantis
Raumpatrouille	Raumpatrouille	k1gNnSc4	Raumpatrouille
-	-	kIx~	-
Die	Die	k1gFnSc2	Die
phantastischen	phantastischen	k2eAgMnSc1d1	phantastischen
Abenteuer	Abenteuer	k1gMnSc1	Abenteuer
des	des	k1gNnSc2	des
Raumschiffes	Raumschiffes	k1gMnSc1	Raumschiffes
ORION	orion	k1gInSc4	orion
Space	Spaec	k1gInSc2	Spaec
<g/>
:	:	kIx,	:
Above	Aboev	k1gFnPc1	Aboev
and	and	k?	and
Beyond	Beyond	k1gMnSc1	Beyond
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Red	Red	k1gMnSc1	Red
Dwarf	Dwarf	k1gMnSc1	Dwarf
<g/>
)	)	kIx)	)
Stargate	Stargat	k1gInSc5	Stargat
Universe	Universe	k1gFnSc1	Universe
Barbarella	Barbarella	k1gMnSc1	Barbarella
(	(	kIx(	(
<g/>
Roger	Roger	k1gMnSc1	Roger
Vadim	Vadim	k?	Vadim
<g/>
)	)	kIx)	)
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
George	Georg	k1gMnSc2	Georg
Lucas	Lucas	k1gMnSc1	Lucas
<g/>
)	)	kIx)	)
Pátý	pátý	k4xOgInSc1	pátý
element	element	k1gInSc1	element
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Luc	Luc	k1gMnSc1	Luc
Besson	Besson	k1gMnSc1	Besson
<g/>
)	)	kIx)	)
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
Roland	Roland	k1gInSc1	Roland
Emmerich	Emmerich	k1gInSc1	Emmerich
<g/>
)	)	kIx)	)
Serenity	Serenita	k1gFnPc1	Serenita
(	(	kIx(	(
<g/>
Joss	Joss	k1gInSc1	Joss
Whedon	Whedon	k1gNnSc1	Whedon
<g/>
)	)	kIx)	)
Duna	duna	k1gFnSc1	duna
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
<g/>
)	)	kIx)	)
Riddick	Riddick	k1gMnSc1	Riddick
<g/>
:	:	kIx,	:
Kronika	kronika	k1gFnSc1	kronika
temna	temno	k1gNnSc2	temno
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Twohy	Twoha	k1gFnSc2	Twoha
<g/>
)	)	kIx)	)
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
z	z	k7c2	z
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
oper	opera	k1gFnPc2	opera
jsou	být	k5eAaImIp3nP	být
zasazené	zasazený	k2eAgInPc1d1	zasazený
mnohé	mnohý	k2eAgInPc1d1	mnohý
RPG	RPG	kA	RPG
<g/>
,	,	kIx,	,
deskové	deskový	k2eAgFnSc2d1	desková
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
také	také	k9	také
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
Mass	Mass	k1gInSc1	Mass
Effect	Effect	k1gInSc1	Effect
1-3	[number]	k4	1-3
-	-	kIx~	-
akční	akční	k2eAgFnSc1d1	akční
RPG	RPG	kA	RPG
série	série	k1gFnSc1	série
s	s	k7c7	s
epickým	epický	k2eAgInSc7d1	epický
příběhem	příběh	k1gInSc7	příběh
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
propracovaným	propracovaný	k2eAgNnSc7d1	propracované
univerzem	univerzum	k1gNnSc7	univerzum
a	a	k8xC	a
silným	silný	k2eAgInSc7d1	silný
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
charaktery	charakter	k1gInPc4	charakter
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
emoce	emoce	k1gFnPc1	emoce
Star	star	k1gFnPc2	star
Control	Controla	k1gFnPc2	Controla
StarCraft	StarCraft	k2eAgInSc4d1	StarCraft
Homeworld	Homeworld	k1gInSc4	Homeworld
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
–	–	k?	–
3D	[number]	k4	3D
simulace	simulace	k1gFnSc2	simulace
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
bitev	bitva	k1gFnPc2	bitva
a	a	k8xC	a
strategie	strategie	k1gFnSc2	strategie
s	s	k7c7	s
epickým	epický	k2eAgInSc7d1	epický
příběhem	příběh	k1gInSc7	příběh
Master	master	k1gMnSc1	master
of	of	k?	of
Orion	orion	k1gInSc1	orion
1-3	[number]	k4	1-3
–	–	k?	–
strategická	strategický	k2eAgFnSc1d1	strategická
<g />
.	.	kIx.	.
</s>
<s>
simulace	simulace	k1gFnPc1	simulace
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	s	k7c7	s
vzestupem	vzestup	k1gInSc7	vzestup
zvolené	zvolený	k2eAgFnSc2d1	zvolená
rasy	rasa	k1gFnSc2	rasa
a	a	k8xC	a
dobýváním	dobývání	k1gNnSc7	dobývání
galaxie	galaxie	k1gFnSc2	galaxie
Imperium	Imperium	k1gNnSc1	Imperium
Galactica	Galactic	k1gInSc2	Galactic
Nexus	nexus	k1gInSc1	nexus
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Jupiter	Jupiter	k1gMnSc1	Jupiter
Incident	incident	k1gInSc1	incident
–	–	k?	–
taktická	taktický	k2eAgFnSc1d1	taktická
simulace	simulace	k1gFnSc1	simulace
s	s	k7c7	s
bojem	boj	k1gInSc7	boj
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
a	a	k8xC	a
epickým	epický	k2eAgInSc7d1	epický
příběhem	příběh	k1gInSc7	příběh
Alpha	Alph	k1gMnSc2	Alph
Centauri	Centaur	k1gFnSc2	Centaur
System	Systo	k1gNnSc7	Systo
Shock	Shocka	k1gFnPc2	Shocka
série	série	k1gFnSc2	série
Wing	Wing	k1gMnSc1	Wing
Commander	Commander	k1gInSc1	Commander
Halo	halo	k1gNnSc1	halo
<g/>
:	:	kIx,	:
Combat	Combat	k1gMnSc1	Combat
Evolved	Evolved	k1gMnSc1	Evolved
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
pokračování	pokračování	k1gNnSc4	pokračování
Halo	halo	k1gNnSc1	halo
2	[number]	k4	2
<g/>
,	,	kIx,	,
Halo	halo	k1gNnSc1	halo
3	[number]	k4	3
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Halo	halo	k1gNnSc1	halo
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
ODST	ODST	kA	ODST
<g/>
,	,	kIx,	,
Halo	halo	k1gNnSc1	halo
Wars	Warsa	k1gFnPc2	Warsa
<g/>
,	,	kIx,	,
Halo	halo	k1gNnSc1	halo
<g/>
:	:	kIx,	:
Reach	Reach	k1gInSc1	Reach
a	a	k8xC	a
Halo	halo	k1gNnSc1	halo
4	[number]	k4	4
Freelancer	Freelancero	k1gNnPc2	Freelancero
X3	X3	k1gFnPc2	X3
<g/>
:	:	kIx,	:
Reunion	Reunion	k1gInSc4	Reunion
Science	Science	k1gFnSc2	Science
fiction	fiction	k1gInSc1	fiction
Hard	Hard	k1gMnSc1	Hard
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc4	fiction
Soft	Soft	k?	Soft
science	scienec	k1gInSc2	scienec
fiction	fiction	k1gInSc1	fiction
Military	Militara	k1gFnSc2	Militara
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
Neff	Neff	k1gInSc1	Neff
<g/>
,	,	kIx,	,
O.	O.	kA	O.
<g/>
,	,	kIx,	,
Olša	Olšum	k1gNnSc2	Olšum
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
literatury	literatura	k1gFnSc2	literatura
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
AFSF	AFSF	kA	AFSF
a	a	k8xC	a
H	H	kA	H
<g/>
&	&	k?	&
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85390-33-7	[number]	k4	80-85390-33-7
</s>
