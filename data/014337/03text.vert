<s>
Linux	linux	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Linux	linux	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
LinuxGNU	LinuxGNU	k?
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
</s>
<s>
Web	web	k1gInSc1
</s>
<s>
The	The	k?
Linux	linux	k1gInSc1
Kernel	kernel	k1gInSc1
Archives	Archives	k1gInSc4
Vyvíjí	vyvíjet	k5eAaImIp3nS
</s>
<s>
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
Rodina	rodina	k1gFnSc1
OS	OS	kA
</s>
<s>
Unix-like	Unix-like	k6eAd1
První	první	k4xOgNnSc1
vydání	vydání	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1991	#num#	k4
Podporované	podporovaný	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
</s>
<s>
IA-	IA-	k?
<g/>
32	#num#	k4
<g/>
,	,	kIx,
x	x	k?
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
,	,	kIx,
PowerPC	PowerPC	k1gFnSc1
<g/>
,	,	kIx,
ARM	ARM	kA
<g/>
,	,	kIx,
m	m	kA
<g/>
68	#num#	k4
<g/>
k	k	k7c3
<g/>
,	,	kIx,
DEC	DEC	kA
Alpha	Alpha	k1gFnSc1
<g/>
,	,	kIx,
SPARC	SPARC	kA
<g/>
,	,	kIx,
hppa	hppa	k1gFnSc1
<g/>
,	,	kIx,
IA-	IA-	k1gFnSc1
<g/>
64	#num#	k4
<g/>
,	,	kIx,
MIPS	MIPS	kA
<g/>
,	,	kIx,
s	s	k7c7
<g/>
390	#num#	k4
a	a	k8xC
další	další	k2eAgInSc1d1
Typ	typ	k1gInSc1
jádra	jádro	k1gNnSc2
</s>
<s>
modulární	modulární	k2eAgNnSc4d1
monolitické	monolitický	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
Programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
C	C	kA
<g/>
,	,	kIx,
Jazyk	jazyk	k1gInSc1
symbolických	symbolický	k2eAgFnPc2d1
adres	adresa	k1gFnPc2
Výchozí	výchozí	k2eAgNnSc4d1
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
</s>
<s>
grafické	grafický	k2eAgInPc1d1
<g/>
:	:	kIx,
GNOME	GNOME	kA
<g/>
,	,	kIx,
KDE	kde	k6eAd1
<g/>
,	,	kIx,
Xfce	Xfce	k1gFnSc3
a	a	k8xC
dalšítextové	dalšítextová	k1gFnSc3
<g/>
:	:	kIx,
příkazový	příkazový	k2eAgInSc4d1
řádek	řádek	k1gInSc4
(	(	kIx(
<g/>
terminál	terminál	k1gInSc1
<g/>
)	)	kIx)
Licence	licence	k1gFnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
GPLv	GPLv	k1gMnSc1
<g/>
2	#num#	k4
a	a	k8xC
jiné	jiný	k2eAgInPc1d1
Stav	stav	k1gInSc1
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1
</s>
<s>
Linux	linux	k1gInSc1
nebo	nebo	k8xC
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
kontroverze	kontroverze	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
svobodný	svobodný	k2eAgInSc4d1
a	a	k8xC
otevřený	otevřený	k2eAgInSc4d1
počítačový	počítačový	k2eAgInSc4d1
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
založený	založený	k2eAgInSc1d1
na	na	k7c6
linuxovém	linuxový	k2eAgNnSc6d1
jádru	jádro	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linuxové	linuxový	k2eAgInPc1d1
systémy	systém	k1gInPc1
jsou	být	k5eAaImIp3nP
šířeny	šířit	k5eAaImNgInP
v	v	k7c6
podobě	podoba	k1gFnSc6
distribucí	distribuce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
nainstalovat	nainstalovat	k5eAaPmF
nebo	nebo	k8xC
používat	používat	k5eAaImF
bez	bez	k7c2
instalace	instalace	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
live	live	k1gFnPc2
CD	CD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používané	používaný	k2eAgFnPc1d1
licence	licence	k1gFnPc1
umožňují	umožňovat	k5eAaImIp3nP
systém	systém	k1gInSc4
zdarma	zdarma	k6eAd1
a	a	k8xC
velmi	velmi	k6eAd1
volně	volně	k6eAd1
používat	používat	k5eAaImF
<g/>
,	,	kIx,
distribuovat	distribuovat	k5eAaBmF
(	(	kIx(
<g/>
kopírovat	kopírovat	k5eAaImF
<g/>
,	,	kIx,
sdílet	sdílet	k5eAaImF
<g/>
)	)	kIx)
i	i	k8xC
upravovat	upravovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
odlišuje	odlišovat	k5eAaImIp3nS
od	od	k7c2
proprietárních	proprietární	k2eAgInPc2d1
systémů	systém	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
Windows	Windows	kA
či	či	k8xC
macOS	macOS	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
za	za	k7c4
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
platit	platit	k5eAaImF
a	a	k8xC
dodržovat	dodržovat	k5eAaImF
omezující	omezující	k2eAgFnPc4d1
licence	licence	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
Linux	Linux	kA
používá	používat	k5eAaImIp3nS
Linux	Linux	kA
kernel	kernel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
myšlenek	myšlenka	k1gFnPc2
Unixu	Unix	k1gInSc2
a	a	k8xC
respektuje	respektovat	k5eAaImIp3nS
příslušné	příslušný	k2eAgInPc4d1
standardy	standard	k1gInPc4
POSIX	POSIX	kA
a	a	k8xC
Single	singl	k1gInSc5
UNIX	UNIX	kA
Specification	Specification	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
z	z	k7c2
křestního	křestní	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
tvůrce	tvůrce	k1gMnSc4
Linuse	Linuse	k1gFnSc2
Torvaldse	Torvaldse	k1gFnSc1
a	a	k8xC
koncovka	koncovka	k1gFnSc1
x	x	k?
odkazuje	odkazovat	k5eAaImIp3nS
právě	právě	k9
na	na	k7c4
Unix	Unix	k1gInSc4
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
jako	jako	k9
XENIX	XENIX	kA
<g/>
,	,	kIx,
Ultrix	Ultrix	k1gInSc1
<g/>
,	,	kIx,
IRIX	IRIX	kA
<g/>
,	,	kIx,
AIX	AIX	kA
a	a	k8xC
další	další	k2eAgMnPc1d1
UN	UN	kA
<g/>
*	*	kIx~
<g/>
Xy	Xy	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jádro	jádro	k1gNnSc1
Linuxu	linux	k1gInSc2
umožňuje	umožňovat	k5eAaImIp3nS
spouštět	spouštět	k5eAaImF
více	hodně	k6eAd2
programů	program	k1gInPc2
(	(	kIx(
<g/>
úloh	úloha	k1gFnPc2
<g/>
)	)	kIx)
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
program	program	k1gInSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
skládat	skládat	k5eAaImF
z	z	k7c2
jednoho	jeden	k4xCgMnSc2
nebo	nebo	k8xC
více	hodně	k6eAd2
procesů	proces	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
víceúlohový	víceúlohový	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
proces	proces	k1gInSc1
potom	potom	k6eAd1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
jeden	jeden	k4xCgInSc4
nebo	nebo	k8xC
více	hodně	k6eAd2
podprocesů	podproces	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operační	operační	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
umožňují	umožňovat	k5eAaImIp3nP
běh	běh	k1gInSc4
více	hodně	k6eAd2
procesů	proces	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
dokonce	dokonce	k9
podprocesů	podproces	k1gInPc2
současně	současně	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
schopny	schopen	k2eAgFnPc1d1
využít	využít	k5eAaPmF
i	i	k9
vícejádrové	vícejádrový	k2eAgInPc4d1
a	a	k8xC
víceprocesorové	víceprocesorový	k2eAgInPc4d1
počítače	počítač	k1gInPc4
a	a	k8xC
výrazně	výrazně	k6eAd1
zefektivnit	zefektivnit	k5eAaPmF
práci	práce	k1gFnSc4
uživatele	uživatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jádro	jádro	k1gNnSc1
Linuxu	linux	k1gInSc2
je	být	k5eAaImIp3nS
víceuživatelské	víceuživatelský	k2eAgNnSc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
umožňuje	umožňovat	k5eAaImIp3nS
spouštět	spouštět	k5eAaImF
programy	program	k1gInPc4
různých	různý	k2eAgMnPc2d1
uživatelů	uživatel	k1gMnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
jeden	jeden	k4xCgMnSc1
uživatel	uživatel	k1gMnSc1
může	moct	k5eAaImIp3nS
obsluhovat	obsluhovat	k5eAaImF
počítač	počítač	k1gInSc4
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
další	další	k2eAgMnPc1d1
mohou	moct	k5eAaImIp3nP
obsluhovat	obsluhovat	k5eAaImF
stejný	stejný	k2eAgInSc4d1
počítač	počítač	k1gInSc4
například	například	k6eAd1
přes	přes	k7c4
síť	síť	k1gFnSc4
nebo	nebo	k8xC
dokonce	dokonce	k9
Internet	Internet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušné	příslušný	k2eAgInPc4d1
uživatelské	uživatelský	k2eAgInPc4d1
účty	účet	k1gInPc4
jsou	být	k5eAaImIp3nP
před	před	k7c7
neoprávněným	oprávněný	k2eNgInSc7d1
přístupem	přístup	k1gInSc7
chráněny	chránit	k5eAaImNgInP
autentizací	autentizace	k1gFnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
jménem	jméno	k1gNnSc7
a	a	k8xC
heslem	heslo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
mají	mít	k5eAaImIp3nP
přidělena	přidělen	k2eAgNnPc4d1
různá	různý	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
od	od	k7c2
naprosté	naprostý	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
nad	nad	k7c7
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
má	mít	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
správce	správce	k1gMnSc1
(	(	kIx(
<g/>
root	root	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
různé	různý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
omezené	omezený	k2eAgInPc4d1
účty	účet	k1gInPc4
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
označením	označení	k1gNnSc7
Linux	Linux	kA
míněno	mínit	k5eAaImNgNnS
nejen	nejen	k6eAd1
jádro	jádro	k1gNnSc1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
do	do	k7c2
něj	on	k3xPp3gNnSc2
též	též	k9
veškeré	veškerý	k3xTgNnSc1
programové	programový	k2eAgNnSc1d1
vybavení	vybavení	k1gNnSc1
(	(	kIx(
<g/>
software	software	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
uživatelé	uživatel	k1gMnPc1
používají	používat	k5eAaImIp3nP
(	(	kIx(
<g/>
tj.	tj.	kA
aplikace	aplikace	k1gFnSc2
<g/>
,	,	kIx,
utility	utilita	k1gFnPc4
<g/>
,	,	kIx,
grafické	grafický	k2eAgNnSc4d1
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
apod.	apod.	kA
<g/>
)	)	kIx)
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
vyvíjeno	vyvíjen	k2eAgNnSc1d1
nezávisle	závisle	k6eNd1
na	na	k7c6
samotném	samotný	k2eAgNnSc6d1
jádře	jádro	k1gNnSc6
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linux	linux	k1gInSc1
je	být	k5eAaImIp3nS
šířen	šířit	k5eAaImNgInS
v	v	k7c6
podobě	podoba	k1gFnSc6
linuxových	linuxový	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
jak	jak	k6eAd1
zmíněné	zmíněný	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
zmíněný	zmíněný	k2eAgInSc1d1
doplňující	doplňující	k2eAgInSc1d1
software	software	k1gInSc1
v	v	k7c6
takové	takový	k3xDgFnSc6
formě	forma	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
usnadňuje	usnadňovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
instalaci	instalace	k1gFnSc4
a	a	k8xC
používání	používání	k1gNnSc4
(	(	kIx(
<g/>
instalace	instalace	k1gFnSc1
někdy	někdy	k6eAd1
není	být	k5eNaImIp3nS
nutná	nutný	k2eAgFnSc1d1
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
Live	Live	k1gInSc4
CD	CD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Linux	linux	k1gInSc1
je	být	k5eAaImIp3nS
open	open	k1gNnSc4
source	source	k1gMnSc2
software	software	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
jeho	jeho	k3xOp3gInPc4
zdrojové	zdrojový	k2eAgInPc4d1
kódy	kód	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
lze	lze	k6eAd1
za	za	k7c4
dodržení	dodržení	k1gNnSc4
jistých	jistý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
upravovat	upravovat	k5eAaImF
a	a	k8xC
vše	všechen	k3xTgNnSc4
dále	daleko	k6eAd2
šířit	šířit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
před	před	k7c7
zneužitím	zneužití	k1gNnSc7
zdrojových	zdrojový	k2eAgInPc2d1
kódů	kód	k1gInPc2
používá	používat	k5eAaImIp3nS
open	open	k1gNnSc4
source	source	k1gMnSc2
software	software	k1gInSc4
různé	různý	k2eAgFnSc2d1
licence	licence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgInPc1d1
jádro	jádro	k1gNnSc4
Linuxu	linux	k1gInSc2
je	být	k5eAaImIp3nS
chráněno	chráněn	k2eAgNnSc4d1
a	a	k8xC
šířeno	šířen	k2eAgNnSc4d1
pod	pod	k7c7
licencí	licence	k1gFnSc7
GPLv	GPLvum	k1gNnPc2
<g/>
2	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
důležitou	důležitý	k2eAgFnSc7d1
výjimkou	výjimka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Software	software	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
Linuxem	linux	k1gInSc7
šířen	šířen	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
nejrůznějšími	různý	k2eAgFnPc7d3
licencemi	licence	k1gFnPc7
(	(	kIx(
<g/>
GPL	GPL	kA
<g/>
,	,	kIx,
LGPL	LGPL	kA
<g/>
,	,	kIx,
MPL	MPL	kA
<g/>
,	,	kIx,
Licence	licence	k1gFnSc1
MIT	MIT	kA
<g/>
,	,	kIx,
BSD	BSD	kA
licence	licence	k1gFnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
distribucí	distribuce	k1gFnPc2
vybírá	vybírat	k5eAaImIp3nS
software	software	k1gInSc1
podle	podle	k7c2
jejich	jejich	k3xOp3gFnPc2
licencí	licence	k1gFnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyhovoval	vyhovovat	k5eAaImAgInS
buď	buď	k8xC
volnějšímu	volný	k2eAgInSc3d2
výkladu	výklad	k1gInSc3
open	opena	k1gFnPc2
source	sourec	k1gInSc2
nebo	nebo	k8xC
naopak	naopak	k6eAd1
přísnějšímu	přísný	k2eAgInSc3d2
výkladu	výklad	k1gInSc3
svobodného	svobodný	k2eAgNnSc2d1
software	software	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
free	freat	k5eAaPmIp3nS
software	software	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
nemá	mít	k5eNaImIp3nS
na	na	k7c4
koncového	koncový	k2eAgMnSc4d1
uživatele	uživatel	k1gMnSc4
přímý	přímý	k2eAgInSc1d1
vliv	vliv	k1gInSc1
<g/>
,	,	kIx,
ovlivňuje	ovlivňovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
však	však	k9
zejména	zejména	k9
pohled	pohled	k1gInSc4
na	na	k7c4
další	další	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
součástí	součást	k1gFnPc2
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
<g/>
,	,	kIx,
tvůrce	tvůrce	k1gMnSc1
linuxového	linuxový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
</s>
<s>
Vývoj	vývoj	k1gInSc1
unixových	unixový	k2eAgInPc2d1
systémů	systém	k1gInPc2
</s>
<s>
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
začal	začít	k5eAaPmAgInS
vyvíjet	vyvíjet	k5eAaImF
jádro	jádro	k1gNnSc4
Linuxu	linux	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
jako	jako	k8xC,k8xS
svůj	svůj	k3xOyFgInSc4
koníček	koníček	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
důvody	důvod	k1gInPc4
pro	pro	k7c4
vznik	vznik	k1gInSc4
právě	právě	k6eAd1
unixového	unixový	k2eAgInSc2d1
systému	systém	k1gInSc2
patřil	patřit	k5eAaImAgInS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
Unix	Unix	k1gInSc1
je	být	k5eAaImIp3nS
systém	systém	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
upřednostňuje	upřednostňovat	k5eAaImIp3nS
jednoduchost	jednoduchost	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
přednášen	přednášen	k2eAgInSc1d1
na	na	k7c6
univerzitách	univerzita	k1gFnPc6
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Torvalds	Torvalds	k1gInSc1
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
finské	finský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Helsinkách	Helsinky	k1gFnPc6
obor	obora	k1gFnPc2
Informatika	informatika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torvalds	Torvalds	k1gInSc1
byl	být	k5eAaImAgInS
dále	daleko	k6eAd2
inspirován	inspirován	k2eAgInSc1d1
MINIXem	MINIXem	k1gInSc1
od	od	k7c2
Andrewa	Andrewus	k1gMnSc2
Tanenbauma	Tanenbaum	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
verzi	verze	k1gFnSc4
unixového	unixový	k2eAgInSc2d1
systému	systém	k1gInSc2
jako	jako	k8xC,k8xS
doprovodný	doprovodný	k2eAgInSc1d1
projekt	projekt	k1gInSc1
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
výuce	výuka	k1gFnSc3
a	a	k8xC
knihám	kniha	k1gFnPc3
o	o	k7c6
operačních	operační	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
něj	on	k3xPp3gNnSc2
však	však	k9
Torvalds	Torvalds	k1gInSc1
nevyužil	využít	k5eNaPmAgInS
svůj	svůj	k3xOyFgInSc4
projekt	projekt	k1gInSc4
komerčně	komerčně	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
preferoval	preferovat	k5eAaImAgInS
otevřený	otevřený	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
open	open	k1gMnSc1
source	source	k1gMnSc1
software	software	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
verze	verze	k1gFnSc1
linuxového	linuxový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
(	(	kIx(
<g/>
0.01	0.01	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
na	na	k7c6
Internetu	Internet	k1gInSc6
zveřejněna	zveřejněn	k2eAgFnSc1d1
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1991	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
Linusově	Linusův	k2eAgFnSc3d1
překvapení	překvapení	k1gNnSc3
byl	být	k5eAaImAgInS
o	o	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
nedokonalý	dokonalý	k2eNgInSc4d1
systém	systém	k1gInSc4
velký	velký	k2eAgInSc4d1
zájem	zájem	k1gInSc4
a	a	k8xC
záhy	záhy	k6eAd1
začal	začít	k5eAaPmAgInS
dostávat	dostávat	k5eAaImF
e-mailem	e-mail	k1gInSc7
další	další	k2eAgInPc4d1
podněty	podnět	k1gInPc4
<g/>
,	,	kIx,
opravy	oprava	k1gFnPc4
a	a	k8xC
zdrojové	zdrojový	k2eAgInPc4d1
kódy	kód	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torvalds	Torvalds	k1gInSc1
jádro	jádro	k1gNnSc4
dále	daleko	k6eAd2
vyvíjel	vyvíjet	k5eAaImAgMnS
a	a	k8xC
zároveň	zároveň	k6eAd1
začal	začít	k5eAaPmAgMnS
příspěvky	příspěvek	k1gInPc4
ostatních	ostatní	k2eAgMnPc2d1
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
jádra	jádro	k1gNnSc2
začleňoval	začleňovat	k5eAaImAgMnS
a	a	k8xC
upravené	upravený	k2eAgInPc1d1
zdrojové	zdrojový	k2eAgInPc1d1
kódy	kód	k1gInPc1
obratem	obratem	k6eAd1
zveřejňovat	zveřejňovat	k5eAaImF
(	(	kIx(
<g/>
další	další	k2eAgFnSc1d1
verze	verze	k1gFnSc1
byla	být	k5eAaImAgFnS
zveřejněna	zveřejnit	k5eAaPmNgFnS
již	již	k6eAd1
v	v	k7c6
říjnu	říjen	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c6
vývoji	vývoj	k1gInSc6
podílely	podílet	k5eAaImAgInP
tisíce	tisíc	k4xCgInPc1
vývojářů	vývojář	k1gMnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Model	model	k1gInSc1
vývoje	vývoj	k1gInSc2
linuxového	linuxový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
a	a	k8xC
podobného	podobný	k2eAgInSc2d1
softwaru	software	k1gInSc2
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
výstižně	výstižně	k6eAd1
popsán	popsat	k5eAaPmNgInS
v	v	k7c6
eseji	esej	k1gInSc6
Katedrála	katedrála	k1gFnSc1
a	a	k8xC
tržiště	tržiště	k1gNnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
The	The	k1gMnSc1
Cathedral	Cathedral	k1gFnSc2
and	and	k?
the	the	k?
Bazaar	Bazaara	k1gFnPc2
<g/>
)	)	kIx)
od	od	k7c2
Erica	Ericus	k1gMnSc2
S.	S.	kA
Raymonda	Raymond	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
<g/>
,	,	kIx,
tvůrce	tvůrce	k1gMnSc1
GNU	gnu	k1gMnSc1
</s>
<s>
Již	již	k6eAd1
velmi	velmi	k6eAd1
brzo	brzo	k6eAd1
předběhl	předběhnout	k5eAaPmAgMnS
Linux	linux	k1gInSc4
ve	v	k7c6
vývoji	vývoj	k1gInSc6
svůj	svůj	k3xOyFgInSc4
vzor	vzor	k1gInSc4
–	–	k?
MINIX	MINIX	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Zejména	zejména	k9
v	v	k7c6
počátcích	počátek	k1gInPc6
byl	být	k5eAaImAgMnS
při	při	k7c6
vývoji	vývoj	k1gInSc6
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
Projekt	projekt	k1gInSc1
GNU	gnu	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
již	již	k6eAd1
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
zabýval	zabývat	k5eAaImAgInS
myšlenkou	myšlenka	k1gFnSc7
vývoje	vývoj	k1gInSc2
volně	volně	k6eAd1
dostupného	dostupný	k2eAgInSc2d1
unixového	unixový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
vlastní	vlastní	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
neměl	mít	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
hned	hned	k9
počátku	počátek	k1gInSc2
Linux	Linux	kA
využil	využít	k5eAaPmAgMnS
shell	shell	k1gMnSc1
bash	bash	k1gMnSc1
a	a	k8xC
další	další	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
(	(	kIx(
<g/>
základní	základní	k2eAgInPc4d1
unixové	unixový	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
používané	používaný	k2eAgInPc4d1
na	na	k7c6
příkazovém	příkazový	k2eAgInSc6d1
řádku	řádek	k1gInSc6
<g/>
,	,	kIx,
kompilátor	kompilátor	k1gInSc1
GCC	GCC	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
též	též	k9
GNU	gnu	k1gMnSc1
C	C	kA
Library	Librar	k1gMnPc7
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Linux	linux	k1gInSc4
však	však	k9
nikdy	nikdy	k6eAd1
nebyl	být	k5eNaImAgMnS
součástí	součást	k1gFnSc7
GNU	gnu	k1gNnSc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
samotné	samotný	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
používá	používat	k5eAaImIp3nS
licenci	licence	k1gFnSc4
GPLv	GPLv	k1gInSc1
<g/>
2	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
též	též	k9
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
GNU	gnu	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Torvalds	Torvalds	k6eAd1
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
hlavou	hlava	k1gFnSc7
vývoje	vývoj	k1gInSc2
jádra	jádro	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
zveřejňováno	zveřejňovat	k5eAaImNgNnS
na	na	k7c6
serveru	server	k1gInSc6
kernel	kernel	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Sám	sám	k3xTgInSc1
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
nové	nový	k2eAgFnPc4d1
verze	verze	k1gFnPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
některé	některý	k3yIgFnPc1
starší	starý	k2eAgFnPc1d2
verze	verze	k1gFnPc1
jsou	být	k5eAaImIp3nP
udržovány	udržovat	k5eAaImNgFnP
jinými	jiný	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
něj	on	k3xPp3gInSc2
na	na	k7c6
vývoji	vývoj	k1gInSc6
spolupracují	spolupracovat	k5eAaImIp3nP
tisíce	tisíc	k4xCgInPc1
programátorů	programátor	k1gMnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
říct	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
vývoj	vývoj	k1gInSc1
jádra	jádro	k1gNnSc2
je	být	k5eAaImIp3nS
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
placen	platit	k5eAaImNgMnS
firmami	firma	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Red	Red	k1gFnSc1
Hat	hat	k0
<g/>
,	,	kIx,
Intel	Intel	kA
<g/>
,	,	kIx,
IBM	IBM	kA
a	a	k8xC
další	další	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
součásti	součást	k1gFnPc1
Linuxu	linux	k1gInSc2
jsou	být	k5eAaImIp3nP
vyvíjeny	vyvíjen	k2eAgFnPc1d1
samostatně	samostatně	k6eAd1
(	(	kIx(
<g/>
např.	např.	kA
KDE	kde	k6eAd1
<g/>
,	,	kIx,
GNOME	GNOME	kA
<g/>
,	,	kIx,
X.	X.	kA
<g/>
Org	Org	k1gFnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Logo	logo	k1gNnSc1
a	a	k8xC
název	název	k1gInSc1
</s>
<s>
Tux	Tux	k?
–	–	k?
maskot	maskot	k1gInSc4
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
Linux	Linux	kA
</s>
<s>
Logem	logo	k1gNnSc7
a	a	k8xC
maskotem	maskot	k1gInSc7
Linuxu	linux	k1gInSc2
je	být	k5eAaImIp3nS
tučňák	tučňák	k1gMnSc1
Tux	Tux	k1gMnSc1
vycházející	vycházející	k2eAgFnSc2d1
z	z	k7c2
obrázku	obrázek	k1gInSc2
Larryho	Larry	k1gMnSc2
Ewinga	Ewing	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
existují	existovat	k5eAaImIp3nP
i	i	k9
jiná	jiný	k2eAgNnPc1d1
<g/>
,	,	kIx,
méně	málo	k6eAd2
známá	známý	k2eAgNnPc4d1
zpodobnění	zpodobnění	k1gNnPc4
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
OS-tan	OS-tan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc4
„	„	k?
<g/>
Linux	linux	k1gInSc1
<g/>
“	“	k?
nevytvořil	vytvořit	k5eNaPmAgInS
sám	sám	k3xTgInSc1
Torvalds	Torvalds	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
Ari	Ari	k1gMnSc4
Lemmke	Lemmk	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
helsinské	helsinský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
jako	jako	k9
správce	správce	k1gMnSc1
FTP	FTP	kA
serveru	server	k1gInSc2
ftp	ftp	k?
<g/>
.	.	kIx.
<g/>
funet	funet	k1gMnSc1
<g/>
.	.	kIx.
<g/>
fi	fi	k0
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
uveřejněna	uveřejnit	k5eAaPmNgFnS
první	první	k4xOgFnSc1
verze	verze	k1gFnSc1
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torvalds	Torvalds	k1gInSc1
navrhoval	navrhovat	k5eAaImAgInS
jméno	jméno	k1gNnSc4
„	„	k?
<g/>
Freax	Freax	k1gInSc1
<g/>
“	“	k?
jako	jako	k8xC,k8xS
free	free	k6eAd1
(	(	kIx(
<g/>
svobodný	svobodný	k2eAgMnSc1d1
<g/>
)	)	kIx)
+	+	kIx~
freak	freak	k1gMnSc1
(	(	kIx(
<g/>
blázen	blázen	k1gMnSc1
<g/>
)	)	kIx)
+	+	kIx~
x	x	k?
(	(	kIx(
<g/>
unixový	unixový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
Lemmkemu	Lemmkem	k1gMnSc3
nelíbilo	líbit	k5eNaImAgNnS
a	a	k8xC
na	na	k7c6
FTP	FTP	kA
serveru	server	k1gInSc6
vytvořil	vytvořit	k5eAaPmAgInS
adresář	adresář	k1gInSc1
„	„	k?
<g/>
Linux	linux	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
tj.	tj.	kA
Linusův	Linusův	k2eAgInSc1d1
unixový	unixový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
„	„	k?
<g/>
Linux	linux	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgMnS
ochrannou	ochranný	k2eAgFnSc7d1
známkou	známka	k1gFnSc7
(	(	kIx(
<g/>
č.	č.	k?
1916230	#num#	k4
<g/>
)	)	kIx)
na	na	k7c4
„	„	k?
<g/>
software	software	k1gInSc4
počítačového	počítačový	k2eAgInSc2d1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
usnadňuje	usnadňovat	k5eAaImIp3nS
práci	práce	k1gFnSc4
s	s	k7c7
počítačem	počítač	k1gInSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
vlastní	vlastnit	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
Linus	Linus	k1gMnSc1
Torvalds	Torvaldsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Licencování	licencování	k1gNnSc4
této	tento	k3xDgFnSc2
ochranné	ochranný	k2eAgFnSc2d1
známky	známka	k1gFnSc2
nyní	nyní	k6eAd1
obstarává	obstarávat	k5eAaImIp3nS
Linux	linux	k1gInSc1
Mark	Mark	k1gMnSc1
Institute	institut	k1gInSc5
(	(	kIx(
<g/>
LMI	LMI	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Označení	označení	k1gNnSc1
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
kontroverze	kontroverze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
a	a	k8xC
Free	Free	k1gFnSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
změnily	změnit	k5eAaPmAgInP
označení	označení	k1gNnSc4
Linuxu	linux	k1gInSc2
na	na	k7c4
GNU	gnu	k1gNnSc4
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vidí	vidět	k5eAaImIp3nP
linuxové	linuxový	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
používající	používající	k2eAgInSc1d1
software	software	k1gInSc1
GNU	gnu	k1gNnSc2
jako	jako	k8xS,k8xC
varianty	varianta	k1gFnSc2
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
GNU	gnu	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
požadují	požadovat	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
takové	takový	k3xDgInPc1
systémy	systém	k1gInPc1
označovaly	označovat	k5eAaImAgInP
jako	jako	k9
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
„	„	k?
<g/>
Na	na	k7c6
Linuxu	linux	k1gInSc6
založený	založený	k2eAgMnSc1d1
GNU	gnu	k1gMnSc1
systém	systém	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Média	médium	k1gNnPc1
a	a	k8xC
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
uživatelů	uživatel	k1gMnPc2
preferují	preferovat	k5eAaImIp3nP
krátké	krátký	k2eAgNnSc4d1
označení	označení	k1gNnSc4
této	tento	k3xDgFnSc2
rodiny	rodina	k1gFnSc2
operačních	operační	k2eAgInPc2d1
systémů	systém	k1gInPc2
jako	jako	k8xS,k8xC
Linux	linux	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Distribuce	distribuce	k1gFnSc1
Debian	Debiana	k1gFnPc2
používá	používat	k5eAaImIp3nS
označení	označení	k1gNnSc1
GNU	gnu	k1gNnSc2
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
na	na	k7c4
výzvu	výzva	k1gFnSc4
Richarda	Richard	k1gMnSc2
Stallmana	Stallman	k1gMnSc2
od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
samotná	samotný	k2eAgFnSc1d1
FSF	FSF	kA
pak	pak	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
v	v	k7c6
obvyklých	obvyklý	k2eAgFnPc6d1
distribucích	distribuce	k1gFnPc6
Linuxu	linux	k1gInSc2
obsaženy	obsažen	k2eAgInPc4d1
vlastní	vlastní	k2eAgInSc4d1
projekty	projekt	k1gInPc4
GNU	gnu	k1gNnSc2
v	v	k7c6
menšině	menšina	k1gFnSc6
(	(	kIx(
<g/>
některé	některý	k3yIgInPc1
významné	významný	k2eAgInPc1d1
projekty	projekt	k1gInPc1
se	se	k3xPyFc4
dokonce	dokonce	k9
od	od	k7c2
GNU	gnu	k1gNnSc2
oprostily	oprostit	k5eAaPmAgFnP
–	–	k?
např.	např.	kA
GCC	GCC	kA
<g/>
,	,	kIx,
GNU	gnu	k1gNnSc1
C	C	kA
Library	Librara	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
dalších	další	k2eAgInPc2d1
projektů	projekt	k1gInPc2
(	(	kIx(
<g/>
součásti	součást	k1gFnPc1
distribucí	distribuce	k1gFnSc7
<g/>
)	)	kIx)
sice	sice	k8xC
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
licence	licence	k1gFnSc1
od	od	k7c2
GNU	gnu	k1gNnSc2
(	(	kIx(
<g/>
GPL	GPL	kA
<g/>
,	,	kIx,
LGPL	LGPL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
nejsou	být	k5eNaImIp3nP
to	ten	k3xDgNnSc1
projekty	projekt	k1gInPc1
GNU	gnu	k1gNnSc2
(	(	kIx(
<g/>
nemají	mít	k5eNaImIp3nP
s	s	k7c7
GNU	gnu	k1gNnSc7
nic	nic	k6eAd1
společného	společný	k2eAgInSc2d1
kromě	kromě	k7c2
použité	použitý	k2eAgFnSc2d1
licence	licence	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tedy	tedy	k9
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
spíše	spíše	k9
o	o	k7c4
ideovou	ideový	k2eAgFnSc4d1
záležitost	záležitost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Linuxové	linuxový	k2eAgFnPc1d1
distribuce	distribuce	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Linuxová	linuxový	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
a	a	k8xC
Seznam	seznam	k1gInSc1
distribucí	distribuce	k1gFnPc2
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Linux	linux	k1gInSc1
jako	jako	k8xC,k8xS
takový	takový	k3xDgInSc4
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
jádro	jádro	k1gNnSc1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
počítač	počítač	k1gInSc1
s	s	k7c7
Linuxem	linux	k1gInSc7
používat	používat	k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
doplnit	doplnit	k5eAaPmF
jádro	jádro	k1gNnSc4
o	o	k7c4
další	další	k2eAgInPc4d1
programy	program	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základ	základ	k1gInSc1
tvoří	tvořit	k5eAaImIp3nP
jednoduché	jednoduchý	k2eAgFnPc4d1
utility	utilita	k1gFnPc4
(	(	kIx(
<g/>
malé	malý	k2eAgInPc4d1
programy	program	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
označujeme	označovat	k5eAaImIp1nP
jako	jako	k9
systémové	systémový	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
a	a	k8xC
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
slouží	sloužit	k5eAaImIp3nP
pro	pro	k7c4
zajištění	zajištění	k1gNnSc4
startu	start	k1gInSc2
(	(	kIx(
<g/>
bootování	bootování	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
následně	následně	k6eAd1
i	i	k9
zajištění	zajištění	k1gNnSc4
běhu	běh	k1gInSc2
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
uživatele	uživatel	k1gMnPc4
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
aplikace	aplikace	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mu	on	k3xPp3gMnSc3
umožňují	umožňovat	k5eAaImIp3nP
provádět	provádět	k5eAaImF
nějakou	nějaký	k3yIgFnSc4
užitečnou	užitečný	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
(	(	kIx(
<g/>
např.	např.	kA
LibreOffice	LibreOffice	k1gFnSc1
<g/>
,	,	kIx,
Mozilla	Mozilla	k1gFnSc1
Firefox	Firefox	k1gInSc1
<g/>
,	,	kIx,
Pidgin	Pidgin	k1gInSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc1d1
běžně	běžně	k6eAd1
používané	používaný	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
i	i	k8xC
aplikace	aplikace	k1gFnPc1
jsou	být	k5eAaImIp3nP
volně	volně	k6eAd1
dostupné	dostupný	k2eAgFnPc1d1
na	na	k7c6
Internetu	Internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
jsou	být	k5eAaImIp3nP
výše	vysoce	k6eAd2
zmíněné	zmíněný	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
i	i	k8xC
aplikace	aplikace	k1gFnPc1
na	na	k7c6
Internetu	Internet	k1gInSc6
dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
podobě	podoba	k1gFnSc6
zdrojových	zdrojový	k2eAgInPc2d1
kódů	kód	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
je	být	k5eAaImIp3nS
nejprve	nejprve	k6eAd1
nutné	nutný	k2eAgNnSc1d1
přeložit	přeložit	k5eAaPmF
do	do	k7c2
formy	forma	k1gFnSc2
spustitelných	spustitelný	k2eAgInPc2d1
souborů	soubor	k1gInPc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
by	by	kYmCp3nS
pro	pro	k7c4
uživatele	uživatel	k1gMnSc4
velmi	velmi	k6eAd1
nepohodlné	pohodlný	k2eNgNnSc1d1
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
si	se	k3xPyFc3
vše	všechen	k3xTgNnSc4
musel	muset	k5eAaImAgInS
dělat	dělat	k5eAaImF
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Proto	proto	k8xC
existují	existovat	k5eAaImIp3nP
takzvané	takzvaný	k2eAgFnPc1d1
distribuce	distribuce	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
vše	všechen	k3xTgNnSc4
potřebné	potřebné	k1gNnSc4
v	v	k7c6
úhledném	úhledný	k2eAgNnSc6d1
balení	balení	k1gNnSc6
–	–	k?
přeložené	přeložený	k2eAgInPc4d1
binární	binární	k2eAgInPc4d1
soubory	soubor	k1gInPc4
včetně	včetně	k7c2
instalačního	instalační	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
připravit	připravit	k5eAaPmF
Linux	linux	k1gInSc4
na	na	k7c6
uživatelově	uživatelův	k2eAgInSc6d1
počítači	počítač	k1gInSc6
k	k	k7c3
okamžitému	okamžitý	k2eAgNnSc3d1
používání	používání	k1gNnSc3
(	(	kIx(
<g/>
avšak	avšak	k8xC
Linux	linux	k1gInSc4
lze	lze	k6eAd1
používat	používat	k5eAaImF
i	i	k9
bez	bez	k7c2
instalace	instalace	k1gFnSc2
pomocí	pomocí	k7c2
tzv.	tzv.	kA
Live	Liv	k1gFnSc2
CD	CD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Distribuce	distribuce	k1gFnPc1
jsou	být	k5eAaImIp3nP
sestavovány	sestavován	k2eAgFnPc1d1
jednotlivci	jednotlivec	k1gMnPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
dobrovolníků	dobrovolník	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
komerčními	komerční	k2eAgFnPc7d1
firmami	firma	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Distribuce	distribuce	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
jádro	jádro	k1gNnSc4
<g/>
,	,	kIx,
další	další	k2eAgNnSc4d1
systémový	systémový	k2eAgInSc4d1
a	a	k8xC
aplikační	aplikační	k2eAgInSc4d1
software	software	k1gInSc4
<g/>
,	,	kIx,
grafické	grafický	k2eAgNnSc4d1
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
(	(	kIx(
<g/>
X.	X.	kA
<g/>
org	org	k?
<g/>
,	,	kIx,
KDE	kde	k6eAd1
<g/>
,	,	kIx,
GNOME	GNOME	kA
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Distribuce	distribuce	k1gFnPc1
mají	mít	k5eAaImIp3nP
různá	různý	k2eAgNnPc4d1
zaměření	zaměření	k1gNnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
výběr	výběr	k1gInSc1
obsažených	obsažený	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
určité	určitý	k2eAgFnSc2d1
počítačové	počítačový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
,	,	kIx,
použití	použití	k1gNnSc1
ve	v	k7c6
vestavěných	vestavěný	k2eAgInPc6d1
systémech	systém	k1gInPc6
atd.	atd.	kA
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
kolem	kolem	k7c2
450	#num#	k4
různých	různý	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Snímky	snímek	k1gInPc1
obrazovek	obrazovka	k1gFnPc2
a	a	k8xC
další	další	k2eAgInPc1d1
popisy	popis	k1gInPc1
distribucí	distribuce	k1gFnPc2
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
na	na	k7c6
různých	různý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
Internetu	Internet	k1gInSc2
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c6
serveru	server	k1gInSc6
DistroWatch	DistroWatch	k1gInSc1
nebo	nebo	k8xC
i	i	k9
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
nejznámější	známý	k2eAgFnPc4d3
distribuce	distribuce	k1gFnPc4
Linuxu	linux	k1gInSc2
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
Arch	arch	k1gInSc1
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
Danix	Danix	k1gInSc1
(	(	kIx(
<g/>
česká	český	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Debian	Debian	k1gInSc1
<g/>
,	,	kIx,
Fedora	Fedora	k1gFnSc1
(	(	kIx(
<g/>
nástupce	nástupce	k1gMnSc1
Red	Red	k1gMnSc1
Hat	hat	k0
Linuxu	linux	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Red	Red	k1gMnSc1
Hat	hat	k0
Enterprise	Enterprise	k1gFnPc4
Linux	linux	k1gInSc1
(	(	kIx(
<g/>
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
Fedory	Fedora	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gentoo	Gentoo	k1gNnSc1
<g/>
,	,	kIx,
Greenie	Greenie	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
(	(	kIx(
<g/>
slovenská	slovenský	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Knoppix	Knoppix	k1gInSc1
<g/>
,	,	kIx,
Mandriva	Mandriva	k1gFnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Mandrake	Mandrak	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Linux	linux	k1gInSc1
Mint	Mint	k1gInSc1
(	(	kIx(
<g/>
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
Ubuntu	Ubunt	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Slackware	Slackwar	k1gMnSc5
<g/>
,	,	kIx,
Slax	Slax	k1gInSc1
(	(	kIx(
<g/>
česká	český	k2eAgFnSc1d1
live	live	k1gFnSc1
distribuce	distribuce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Source	Source	k1gMnSc1
Mage	Mag	k1gFnSc2
<g/>
,	,	kIx,
SUSE	SUSE	kA
<g/>
,	,	kIx,
Ubuntu	Ubunta	k1gFnSc4
<g/>
,	,	kIx,
Kubuntu	Kubunta	k1gFnSc4
(	(	kIx(
<g/>
derivát	derivát	k1gInSc1
Ubuntu	Ubunt	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
…	…	k?
</s>
<s>
Licence	licence	k1gFnSc1
</s>
<s>
Distribuce	distribuce	k1gFnSc1
lze	lze	k6eAd1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
na	na	k7c6
Internetu	Internet	k1gInSc6
a	a	k8xC
lze	lze	k6eAd1
je	být	k5eAaImIp3nS
i	i	k9
volně	volně	k6eAd1
používat	používat	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
open	openo	k1gNnPc2
source	sourec	k1gInSc2
programů	program	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
v	v	k7c6
distribuci	distribuce	k1gFnSc6
je	být	k5eAaImIp3nS
ke	k	k7c3
každému	každý	k3xTgInSc3
programu	program	k1gInSc3
standardně	standardně	k6eAd1
přiložena	přiložen	k2eAgFnSc1d1
licence	licence	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
při	při	k7c6
instalaci	instalace	k1gFnSc6
uložena	uložit	k5eAaPmNgFnS
společně	společně	k6eAd1
s	s	k7c7
programem	program	k1gInSc7
na	na	k7c4
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
<g/>
,	,	kIx,
takže	takže	k8xS
si	se	k3xPyFc3
vše	všechen	k3xTgNnSc4
uživatel	uživatel	k1gMnSc1
může	moct	k5eAaImIp3nS
ověřit	ověřit	k5eAaPmF
(	(	kIx(
<g/>
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
open	opena	k1gFnPc2
source	sourec	k1gInSc2
programy	program	k1gInPc7
vyžadují	vyžadovat	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
s	s	k7c7
nimi	on	k3xPp3gNnPc7
licence	licence	k1gFnSc2
byla	být	k5eAaImAgFnS
dodávána	dodávat	k5eAaImNgFnS
nejen	nejen	k6eAd1
u	u	k7c2
zdrojových	zdrojový	k2eAgInPc2d1
kódů	kód	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
u	u	k7c2
funkční	funkční	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dílo	dílo	k1gNnSc1
vytvořené	vytvořený	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
vše	všechen	k3xTgNnSc1
na	na	k7c6
distribučním	distribuční	k2eAgNnSc6d1
médiu	médium	k1gNnSc6
organizováno	organizovat	k5eAaBmNgNnS
<g/>
)	)	kIx)
také	také	k6eAd1
podléhá	podléhat	k5eAaImIp3nS
licenci	licence	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
typicky	typicky	k6eAd1
se	se	k3xPyFc4
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
open	open	k1gMnSc1
source	source	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Většina	většina	k1gFnSc1
linuxových	linuxový	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
je	být	k5eAaImIp3nS
sestavována	sestavovat	k5eAaImNgFnS
výhradně	výhradně	k6eAd1
ze	z	k7c2
svobodného	svobodný	k2eAgNnSc2d1
software	software	k1gInSc1
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
lze	lze	k6eAd1
nejen	nejen	k6eAd1
volně	volně	k6eAd1
používat	používat	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
dále	daleko	k6eAd2
šířit	šířit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
však	však	k9
mohou	moct	k5eAaImIp3nP
obsahovat	obsahovat	k5eAaImF
nesvobodný	svobodný	k2eNgInSc4d1
software	software	k1gInSc4
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
lze	lze	k6eAd1
volně	volně	k6eAd1
používat	používat	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
omezeno	omezen	k2eAgNnSc1d1
jejich	jejich	k3xOp3gNnSc4
šíření	šíření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
obsahují	obsahovat	k5eAaImIp3nP
komerční	komerční	k2eAgInPc1d1
programy	program	k1gInPc1
(	(	kIx(
<g/>
ovladače	ovladač	k1gInPc1
pro	pro	k7c4
grafickou	grafický	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
<g/>
,	,	kIx,
počítačové	počítačový	k2eAgFnPc4d1
hry	hra	k1gFnPc4
atp.	atp.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Též	též	k6eAd1
profesionální	profesionální	k2eAgFnSc2d1
komerční	komerční	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
jsou	být	k5eAaImIp3nP
však	však	k9
dostupné	dostupný	k2eAgInPc1d1
pouze	pouze	k6eAd1
po	po	k7c6
zaplacení	zaplacení	k1gNnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
Red	Red	k1gFnSc1
Hat	hat	k0
Enterprise	Enterprise	k1gFnPc4
Linux	Linux	kA
<g/>
,	,	kIx,
SUSE	SUSE	kA
Linux	Linux	kA
Enterprise	Enterprise	k1gFnSc1
Server	server	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
díky	díky	k7c3
povaze	povaha	k1gFnSc3
open	open	k1gMnSc1
source	source	k1gMnSc1
k	k	k7c3
některým	některý	k3yIgInPc3
existují	existovat	k5eAaImIp3nP
volně	volně	k6eAd1
šiřitelné	šiřitelný	k2eAgInPc1d1
identické	identický	k2eAgInPc1d1
klony	klon	k1gInPc1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
např.	např.	kA
CentOS	CentOS	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Repozitáře	Repozitář	k1gInPc1
</s>
<s>
Základní	základní	k2eAgFnSc7d1
výhodou	výhoda	k1gFnSc7
linuxových	linuxový	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
je	být	k5eAaImIp3nS
existence	existence	k1gFnSc1
repozitářů	repozitář	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
založeny	založit	k5eAaPmNgInP
na	na	k7c6
balíčkovacích	balíčkovací	k2eAgInPc6d1
systémech	systém	k1gInPc6
a	a	k8xC
obsahují	obsahovat	k5eAaImIp3nP
snadno	snadno	k6eAd1
instalovatelné	instalovatelný	k2eAgInPc1d1
balíčky	balíček	k1gInPc1
s	s	k7c7
jednotlivými	jednotlivý	k2eAgInPc7d1
programy	program	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
lze	lze	k6eAd1
v	v	k7c6
linuxových	linuxový	k2eAgFnPc6d1
distribucích	distribuce	k1gFnPc6
velmi	velmi	k6eAd1
pohodlně	pohodlně	k6eAd1
instalovat	instalovat	k5eAaBmF
a	a	k8xC
odebírat	odebírat	k5eAaImF
jednotlivé	jednotlivý	k2eAgFnPc4d1
součásti	součást	k1gFnPc4
systému	systém	k1gInSc2
a	a	k8xC
aplikace	aplikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
repozitářů	repozitář	k1gInPc2
jsou	být	k5eAaImIp3nP
umisťovány	umisťován	k2eAgFnPc1d1
též	též	k9
aktualizace	aktualizace	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
zajistit	zajistit	k5eAaPmF
nejen	nejen	k6eAd1
automatické	automatický	k2eAgFnPc4d1
opravy	oprava	k1gFnPc4
chyb	chyba	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
zajišťují	zajišťovat	k5eAaImIp3nP
také	také	k9
bezpečnost	bezpečnost	k1gFnSc4
systému	systém	k1gInSc2
odstraňováním	odstraňování	k1gNnSc7
zjištěných	zjištěný	k2eAgFnPc2d1
zranitelností	zranitelnost	k1gFnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
nejen	nejen	k6eAd1
pro	pro	k7c4
samotný	samotný	k2eAgInSc4d1
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
v	v	k7c6
případě	případ	k1gInSc6
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
ostatní	ostatní	k2eAgFnPc4d1
součásti	součást	k1gFnPc4
příslušné	příslušný	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Použitelnost	použitelnost	k1gFnSc1
a	a	k8xC
podíl	podíl	k1gInSc1
na	na	k7c6
trhu	trh	k1gInSc6
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
své	svůj	k3xOyFgNnSc4
dlouhé	dlouhý	k2eAgNnSc4d1
působení	působení	k1gNnSc4
Linux	Linux	kA
získal	získat	k5eAaPmAgInS
mnoho	mnoho	k4c4
příznivců	příznivec	k1gMnPc2
a	a	k8xC
významné	významný	k2eAgNnSc1d1
místo	místo	k1gNnSc1
na	na	k7c6
trhu	trh	k1gInSc6
operačních	operační	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
je	být	k5eAaImIp3nS
rozšířený	rozšířený	k2eAgInSc1d1
zejména	zejména	k9
na	na	k7c6
internetových	internetový	k2eAgInPc6d1
a	a	k8xC
intranetových	intranetový	k2eAgInPc6d1
serverech	server	k1gInPc6
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
vysoce	vysoce	k6eAd1
výkonných	výkonný	k2eAgFnPc2d1
výpočetních	výpočetní	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
žebříčku	žebříček	k1gInSc6
500	#num#	k4
nejvýkonnějších	výkonný	k2eAgInPc2d3
superpočítačů	superpočítač	k1gInPc2
<g/>
/	/	kIx~
<g/>
TOP	topit	k5eAaImRp2nS
<g/>
500	#num#	k4
má	mít	k5eAaImIp3nS
podíl	podíl	k1gInSc4
100	#num#	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
Linux	linux	k1gInSc1
pozvolna	pozvolna	k6eAd1
rozšiřuje	rozšiřovat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
firemní	firemní	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
a	a	k8xC
na	na	k7c4
domácí	domácí	k2eAgInPc4d1
počítače	počítač	k1gInPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
takže	takže	k8xS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
podíl	podíl	k1gInSc4
na	na	k7c6
PC	PC	kA
dosáhl	dosáhnout	k5eAaPmAgMnS
tři	tři	k4xCgNnPc4
procenta	procento	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Přestože	přestože	k8xS
zvládá	zvládat	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
všechny	všechen	k3xTgFnPc4
činnosti	činnost	k1gFnPc4
od	od	k7c2
počítače	počítač	k1gInSc2
očekávané	očekávaný	k2eAgNnSc1d1
a	a	k8xC
mezi	mezi	k7c4
jeho	jeho	k3xOp3gFnPc4
přednosti	přednost	k1gFnPc4
patří	patřit	k5eAaImIp3nS
bezpečnost	bezpečnost	k1gFnSc1
<g/>
,	,	kIx,
nízká	nízký	k2eAgFnSc1d1
cena	cena	k1gFnSc1
a	a	k8xC
flexibilita	flexibilita	k1gFnSc1
<g/>
,	,	kIx,
rozšíření	rozšíření	k1gNnSc1
stále	stále	k6eAd1
brání	bránit	k5eAaImIp3nS
zejména	zejména	k9
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
aplikací	aplikace	k1gFnPc2
dostupných	dostupný	k2eAgFnPc2d1
pouze	pouze	k6eAd1
pro	pro	k7c4
Microsoft	Microsoft	kA
Windows	Windows	kA
(	(	kIx(
<g/>
zvláště	zvláště	k6eAd1
počítačových	počítačový	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
nejistá	jistý	k2eNgFnSc1d1
podpora	podpora	k1gFnSc1
spuštění	spuštění	k1gNnSc2
těchto	tento	k3xDgFnPc2
konkrétních	konkrétní	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
pod	pod	k7c7
Linuxem	linux	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutno	nutno	k6eAd1
také	také	k9
dodat	dodat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
Linux	linux	k1gInSc1
prosazuje	prosazovat	k5eAaImIp3nS
i	i	k9
na	na	k7c6
poli	pole	k1gNnSc6
chytrých	chytrý	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
a	a	k8xC
tabletů	tablet	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Instalace	instalace	k1gFnSc1
</s>
<s>
Instalace	instalace	k1gFnSc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
podle	podle	k7c2
zvolené	zvolený	k2eAgFnSc2d1
linuxové	linuxový	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
distribucí	distribuce	k1gFnPc2
nabízí	nabízet	k5eAaImIp3nS
textovou	textový	k2eAgFnSc4d1
i	i	k8xC
grafickou	grafický	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
instalace	instalace	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
obvykle	obvykle	k6eAd1
zvládne	zvládnout	k5eAaPmIp3nS
i	i	k9
začátečník	začátečník	k1gMnSc1
─	─	k?
mnozí	mnohý	k2eAgMnPc1d1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
instalace	instalace	k1gFnSc1
některých	některý	k3yIgFnPc2
distribucí	distribuce	k1gFnPc2
Linuxu	linux	k1gInSc2
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
snadnější	snadný	k2eAgMnSc1d2
než	než	k8xS
u	u	k7c2
konkurenčních	konkurenční	k2eAgInPc2d1
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
instalaci	instalace	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
obvykle	obvykle	k6eAd1
nainstaluje	nainstalovat	k5eAaPmIp3nS
nejen	nejen	k6eAd1
samotný	samotný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
veškerý	veškerý	k3xTgInSc4
software	software	k1gInSc4
potřebný	potřebný	k2eAgInSc4d1
k	k	k7c3
používání	používání	k1gNnSc3
počítače	počítač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Instalovat	instalovat	k5eAaBmF
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
přímo	přímo	k6eAd1
z	z	k7c2
instalačního	instalační	k2eAgNnSc2d1
média	médium	k1gNnSc2
(	(	kIx(
<g/>
pak	pak	k6eAd1
se	se	k3xPyFc4
ovšem	ovšem	k9
nainstalují	nainstalovat	k5eAaPmIp3nP
aplikace	aplikace	k1gFnPc1
ve	v	k7c6
verzi	verze	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
v	v	k7c6
době	doba	k1gFnSc6
vydání	vydání	k1gNnSc2
distribuce	distribuce	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
lze	lze	k6eAd1
z	z	k7c2
instalačního	instalační	k2eAgNnSc2d1
média	médium	k1gNnSc2
pouze	pouze	k6eAd1
nabootovat	nabootovat	k5eAaBmF,k5eAaPmF,k5eAaImF
a	a	k8xC
stáhnout	stáhnout	k5eAaPmF
aktuální	aktuální	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
distribuce	distribuce	k1gFnSc2
ze	z	k7c2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
distribuce	distribuce	k1gFnSc1
lze	lze	k6eAd1
také	také	k9
instalovat	instalovat	k5eAaBmF
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
běžícího	běžící	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
jiné	jiný	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
Linuxu	linux	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
spíš	spíš	k9
zajímavost	zajímavost	k1gFnSc4
pro	pro	k7c4
odborníky	odborník	k1gMnPc4
než	než	k8xS
doporučená	doporučený	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
pro	pro	k7c4
začátečníka	začátečník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Běh	běh	k1gInSc1
aplikací	aplikace	k1gFnPc2
napsaných	napsaný	k2eAgInPc2d1
pro	pro	k7c4
Microsoft	Microsoft	kA
Windows	Windows	kA
</s>
<s>
Pro	pro	k7c4
běh	běh	k1gInSc4
aplikací	aplikace	k1gFnPc2
z	z	k7c2
Microsoft	Microsoft	kA
Windows	Windows	kA
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
řada	řada	k1gFnSc1
emulátorů	emulátor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
nich	on	k3xPp3gInPc2
jsou	být	k5eAaImIp3nP
založené	založený	k2eAgInPc1d1
na	na	k7c4
vytváření	vytváření	k1gNnSc4
plnohodnotných	plnohodnotný	k2eAgInPc2d1
virtuálních	virtuální	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
pouze	pouze	k6eAd1
překládají	překládat	k5eAaImIp3nP
systémová	systémový	k2eAgNnPc1d1
volání	volání	k1gNnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jednak	jednak	k8xC
vede	vést	k5eAaImIp3nS
k	k	k7c3
mnohem	mnohem	k6eAd1
efektivnějšímu	efektivní	k2eAgInSc3d2
běhu	běh	k1gInSc3
spouštěných	spouštěný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
a	a	k8xC
jednak	jednak	k8xC
k	k	k7c3
efektivnějšímu	efektivní	k2eAgNnSc3d2
využití	využití	k1gNnSc3
stávajícího	stávající	k2eAgInSc2d1
hardwaru	hardware	k1gInSc2
(	(	kIx(
<g/>
například	například	k6eAd1
podpora	podpora	k1gFnSc1
3D	3D	k4
akcelerace	akcelerace	k1gFnSc1
grafických	grafický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
nevýhodou	nevýhoda	k1gFnSc7
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
možnost	možnost	k1gFnSc4
použití	použití	k1gNnSc3
pouze	pouze	k6eAd1
na	na	k7c6
architektuře	architektura	k1gFnSc6
x	x	k?
<g/>
86	#num#	k4
a	a	k8xC
kompatibilních	kompatibilní	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
emulátory	emulátor	k1gInPc1
zdarma	zdarma	k6eAd1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
nejznámější	známý	k2eAgInPc4d3
je	on	k3xPp3gInPc4
Wine	Win	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
pomocí	pomocí	k7c2
Wine	Win	k1gInSc2
funguje	fungovat	k5eAaImIp3nS
pod	pod	k7c7
Linuxem	linux	k1gInSc7
většina	většina	k1gFnSc1
Windows	Windows	kA
aplikací	aplikace	k1gFnSc7
nebo	nebo	k8xC
pro	pro	k7c4
ně	on	k3xPp3gInPc4
existuje	existovat	k5eAaImIp3nS
alternativa	alternativa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
zmínku	zmínka	k1gFnSc4
stojí	stát	k5eAaImIp3nS
i	i	k9
komerční	komerční	k2eAgFnSc1d1
odnož	odnož	k1gFnSc1
Cedega	Cedega	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
specializuje	specializovat	k5eAaBmIp3nS
na	na	k7c4
možnost	možnost	k1gFnSc4
hraní	hraň	k1gFnPc2
her	hra	k1gFnPc2
napsaných	napsaný	k2eAgInPc2d1
pro	pro	k7c4
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
,	,	kIx,
nebo	nebo	k8xC
CrossOver	CrossOver	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
lze	lze	k6eAd1
použít	použít	k5eAaPmF
emulátory	emulátor	k1gInPc4
virtuálního	virtuální	k2eAgNnSc2d1
PC	PC	kA
<g/>
:	:	kIx,
Bochs	Bochs	k1gInSc1
<g/>
,	,	kIx,
QEMU	QEMU	kA
<g/>
,	,	kIx,
VirtualBox	VirtualBox	k1gInSc1
(	(	kIx(
<g/>
GPL	GPL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
VMWare	VMWar	k1gMnSc5
(	(	kIx(
<g/>
proprietární	proprietární	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Programování	programování	k1gNnSc1
na	na	k7c6
Linuxu	linux	k1gInSc6
</s>
<s>
Základním	základní	k2eAgInSc7d1
programovacím	programovací	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
v	v	k7c6
Linuxu	linux	k1gInSc6
je	být	k5eAaImIp3nS
jazyk	jazyk	k1gInSc1
C	C	kA
a	a	k8xC
sada	sada	k1gFnSc1
GCC	GCC	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
překladače	překladač	k1gMnSc4
pro	pro	k7c4
několik	několik	k4yIc4
jazyků	jazyk	k1gInPc2
(	(	kIx(
<g/>
zejména	zejména	k9
C	C	kA
a	a	k8xC
C	C	kA
<g/>
++	++	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
programovacích	programovací	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
jsou	být	k5eAaImIp3nP
i	i	k9
GNU	gnu	k1gNnSc4
binutils	binutilsa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
nástroje	nástroj	k1gInPc4
pro	pro	k7c4
překlad	překlad	k1gInSc4
jazyka	jazyk	k1gInSc2
symbolických	symbolický	k2eAgFnPc2d1
adres	adresa	k1gFnPc2
a	a	k8xC
linkování	linkování	k1gNnSc2
binárních	binární	k2eAgInPc2d1
objektových	objektový	k2eAgInPc2d1
souborů	soubor	k1gInPc2
do	do	k7c2
spustitelné	spustitelný	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
;	;	kIx,
na	na	k7c6
systému	systém	k1gInSc6
Linux	Linux	kA
jsou	být	k5eAaImIp3nP
standardně	standardně	k6eAd1
objektové	objektový	k2eAgInPc1d1
soubory	soubor	k1gInPc1
i	i	k8xC
spustitelné	spustitelný	k2eAgInPc1d1
programy	program	k1gInPc1
uloženy	uložen	k2eAgInPc1d1
ve	v	k7c6
formátu	formát	k1gInSc6
ELF	elf	k1gMnSc1
(	(	kIx(
<g/>
executable	executable	k6eAd1
and	and	k?
linkable	linkable	k6eAd1
format	format	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostředí	prostředí	k1gNnSc1
GNU	gnu	k1gNnSc3
nabízí	nabízet	k5eAaImIp3nS
i	i	k9
řadu	řada	k1gFnSc4
dalších	další	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
pro	pro	k7c4
usnadnění	usnadnění	k1gNnSc4
vývoje	vývoj	k1gInSc2
složitějších	složitý	k2eAgInPc2d2
programů	program	k1gInPc2
</s>
<s>
(	(	kIx(
<g/>
make	make	k1gFnPc3
<g/>
,	,	kIx,
autoconf	autoconf	k1gMnSc1
<g/>
,	,	kIx,
gettext	gettext	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Linux	linux	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
i	i	k9
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
dalších	další	k2eAgInPc2d1
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
jednoduchého	jednoduchý	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
zabudovaného	zabudovaný	k2eAgInSc2d1
</s>
<s>
přímo	přímo	k6eAd1
v	v	k7c6
příkazové	příkazový	k2eAgFnSc6d1
řádce	řádka	k1gFnSc6
(	(	kIx(
<g/>
shell	shell	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
nejpoužívanějšími	používaný	k2eAgMnPc7d3
jazyky	jazyk	k1gMnPc7
v	v	k7c6
linuxovém	linuxový	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
Perl	perl	k1gInSc4
a	a	k8xC
Python	Python	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
se	se	k3xPyFc4
Linux	linux	k1gInSc1
stal	stát	k5eAaPmAgInS
velice	velice	k6eAd1
populární	populární	k2eAgFnSc7d1
platformou	platforma	k1gFnSc7
pro	pro	k7c4
provoz	provoz	k1gInSc4
WWW	WWW	kA
serverů	server	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k9
obrovské	obrovský	k2eAgNnSc4d1
množství	množství	k1gNnSc4
uživatelských	uživatelský	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
běžně	běžně	k6eAd1
provozují	provozovat	k5eAaImIp3nP
pod	pod	k7c7
tímto	tento	k3xDgInSc7
systémem	systém	k1gInSc7
jsou	být	k5eAaImIp3nP
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
webové	webový	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
napsané	napsaný	k2eAgFnPc4d1
v	v	k7c6
jazyce	jazyk	k1gInSc6
PHP	PHP	kA
<g/>
.	.	kIx.
</s>
<s>
Snadná	snadný	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
grafického	grafický	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
v	v	k7c6
Glade	Glad	k1gMnSc5
</s>
<s>
Programování	programování	k1gNnSc1
v	v	k7c6
Linuxu	linux	k1gInSc6
většinou	většinou	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
cyklu	cyklus	k1gInSc6
<g/>
:	:	kIx,
programátor	programátor	k1gInSc1
napíše	napsat	k5eAaBmIp3nS,k5eAaPmIp3nS
zdrojový	zdrojový	k2eAgInSc1d1
kód	kód	k1gInSc1
v	v	k7c6
textovém	textový	k2eAgInSc6d1
editoru	editor	k1gInSc6
<g/>
,	,	kIx,
pak	pak	k6eAd1
spustí	spustit	k5eAaPmIp3nS
v	v	k7c6
příkazové	příkazový	k2eAgFnSc6d1
řádce	řádka	k1gFnSc6
kompilátor	kompilátor	k1gInSc1
a	a	k8xC
program	program	k1gInSc1
otestuje	otestovat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
i	i	k9
programátorské	programátorský	k2eAgInPc1d1
editory	editor	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
za	za	k7c4
</s>
<s>
programátora	programátor	k1gMnSc2
spustí	spustit	k5eAaPmIp3nS
kompilátor	kompilátor	k1gInSc1
a	a	k8xC
případně	případně	k6eAd1
ve	v	k7c6
zdrojovém	zdrojový	k2eAgInSc6d1
textu	text	k1gInSc6
označí	označit	k5eAaPmIp3nS
chyby	chyba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samozřejmostí	samozřejmost	k1gFnPc2
je	být	k5eAaImIp3nS
i	i	k9
zvýraznění	zvýraznění	k1gNnSc4
syntaxe	syntax	k1gFnSc2
a	a	k8xC
nyní	nyní	k6eAd1
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
již	již	k6eAd1
i	i	k8xC
rozvinutá	rozvinutý	k2eAgNnPc1d1
plně	plně	k6eAd1
funkční	funkční	k2eAgNnPc1d1
vývojová	vývojový	k2eAgNnPc1d1
prostředí	prostředí	k1gNnPc1
označovaná	označovaný	k2eAgNnPc1d1
jako	jako	k8xC,k8xS
IDE	IDE	kA
nebo	nebo	k8xC
v	v	k7c6
případě	případ	k1gInSc6
návrhu	návrh	k1gInSc2
grafického	grafický	k2eAgNnSc2d1
uživatelského	uživatelský	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
označovaná	označovaný	k2eAgFnSc1d1
RAD	rad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
prostředí	prostředí	k1gNnPc1
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
grafické	grafický	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
X	X	kA
Window	Window	k1gMnSc7
System	Syst	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vesměs	vesměs	k6eAd1
jsou	být	k5eAaImIp3nP
zaměřená	zaměřený	k2eAgNnPc1d1
na	na	k7c4
kompilované	kompilovaný	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
,	,	kIx,
existují	existovat	k5eAaImIp3nP
ale	ale	k8xC
i	i	k9
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
podporují	podporovat	k5eAaImIp3nP
ladění	ladění	k1gNnSc4
skriptovacích	skriptovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
Pythonu	Python	k1gMnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
například	například	k6eAd1
IDEA	idea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
programování	programování	k1gNnSc3
grafických	grafický	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
lze	lze	k6eAd1
použít	použít	k5eAaPmF
např.	např.	kA
Anjuta	Anjut	k1gMnSc2
<g/>
,	,	kIx,
Glade	Glad	k1gInSc5
či	či	k8xC
KDevelop	KDevelop	k1gInSc4
(	(	kIx(
<g/>
primárně	primárně	k6eAd1
pro	pro	k7c4
prostředí	prostředí	k1gNnSc4
KDE	kde	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podpora	podpora	k1gFnSc1
</s>
<s>
Podpora	podpora	k1gFnSc1
linuxových	linuxový	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
realizována	realizován	k2eAgFnSc1d1
komerčními	komerční	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
Canonical	Canonical	k1gFnSc4
<g/>
,	,	kIx,
Novell	Novell	kA
<g/>
,	,	kIx,
Red	Red	k1gFnSc1
Hat	hat	k0
nebo	nebo	k8xC
Mandriva	Mandrivo	k1gNnPc4
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
přímo	přímo	k6eAd1
o	o	k7c6
společnosti	společnost	k1gFnSc6
spravující	spravující	k2eAgFnSc4d1
určitou	určitý	k2eAgFnSc4d1
distribuci	distribuce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
jsou	být	k5eAaImIp3nP
společnosti	společnost	k1gFnPc1
jako	jako	k8xC,k8xS
např.	např.	kA
VA	va	k0wR
Linux	linux	k1gInSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
specializují	specializovat	k5eAaBmIp3nP
na	na	k7c4
aplikace	aplikace	k1gFnPc4
řešení	řešení	k1gNnPc2
postavených	postavený	k2eAgMnPc2d1
na	na	k7c6
Linuxu	linux	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgInSc1d3
model	model	k1gInSc1
podpory	podpora	k1gFnSc2
je	být	k5eAaImIp3nS
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
koupí	koupě	k1gFnSc7
distribuce	distribuce	k1gFnSc2
dostáváte	dostávat	k5eAaImIp2nP
právo	právo	k1gNnSc4
využít	využít	k5eAaPmF
omezenou	omezený	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
po	po	k7c4
omezený	omezený	k2eAgInSc4d1
čas	čas	k1gInSc4
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
můžete	moct	k5eAaImIp2nP
později	pozdě	k6eAd2
dokoupit	dokoupit	k5eAaPmF
další	další	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
uživatele	uživatel	k1gMnPc4
distribucí	distribuce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
zdarma	zdarma	k6eAd1
<g/>
,	,	kIx,
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
poměrně	poměrně	k6eAd1
dobrá	dobrý	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
řada	řada	k1gFnSc1
diskusních	diskusní	k2eAgNnPc2d1
fór	fórum	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
angličtině	angličtina	k1gFnSc6
i	i	k8xC
češtině	čeština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
Linuxu	linux	k1gInSc2
</s>
<s>
Kromě	kromě	k7c2
distribucí	distribuce	k1gFnPc2
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
určených	určený	k2eAgInPc2d1
pro	pro	k7c4
všeobecné	všeobecný	k2eAgNnSc4d1
použití	použití	k1gNnSc4
na	na	k7c6
počítačích	počítač	k1gInPc6
a	a	k8xC
serverech	server	k1gInPc6
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
distribuce	distribuce	k1gFnPc4
specializovány	specializován	k2eAgFnPc4d1
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
,	,	kIx,
například	například	k6eAd1
jako	jako	k8xS,k8xC
podpora	podpora	k1gFnSc1
architektury	architektura	k1gFnSc2
počítačů	počítač	k1gInPc2
<g/>
,	,	kIx,
vestavěných	vestavěný	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
stability	stabilita	k1gFnSc2
<g/>
,	,	kIx,
zabezpečení	zabezpečení	k1gNnSc2
<g/>
,	,	kIx,
lokalizace	lokalizace	k1gFnSc2
do	do	k7c2
konkrétního	konkrétní	k2eAgInSc2d1
regionu	region	k1gInSc2
nebo	nebo	k8xC
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
cílení	cílení	k1gNnSc4
na	na	k7c4
konkrétní	konkrétní	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
uživatelů	uživatel	k1gMnPc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
pro	pro	k7c4
aplikace	aplikace	k1gFnPc4
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
(	(	kIx(
<g/>
real-time	real-timat	k5eAaPmIp3nS
applications	applications	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
některé	některý	k3yIgFnPc1
distribuce	distribuce	k1gFnPc1
záměrně	záměrně	k6eAd1
zahrnují	zahrnovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
volný	volný	k2eAgInSc4d1
software	software	k1gInSc4
(	(	kIx(
<g/>
free	freat	k5eAaPmIp3nS
software	software	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
rozvíjí	rozvíjet	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
čtyři	čtyři	k4xCgNnPc1
sta	sto	k4xCgNnPc1
linuxových	linuxový	k2eAgFnPc2d1
distribucí	distribuce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Plocha	plocha	k1gFnSc1
počítače	počítač	k1gInSc2
(	(	kIx(
<g/>
Desktop	desktop	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Popularita	popularita	k1gFnSc1
Linuxu	linux	k1gInSc2
na	na	k7c6
běžných	běžný	k2eAgInPc6d1
stolních	stolní	k2eAgInPc6d1
počítačích	počítač	k1gInPc6
a	a	k8xC
noteboocích	notebook	k1gInPc6
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
zvyšuje	zvyšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
moderních	moderní	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
zahrnuje	zahrnovat	k5eAaImIp3nS
grafické	grafický	k2eAgNnSc4d1
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
má	mít	k5eAaImIp3nS
od	od	k7c2
února	únor	k1gInSc2
2015	#num#	k4
dvě	dva	k4xCgNnPc4
nejpopulárnější	populární	k2eAgNnPc4d3
prostředí	prostředí	k1gNnPc4
jako	jako	k8xS,k8xC
KDE	kde	k6eAd1
Plasma	plasma	k1gFnSc1
Desktop	desktop	k1gInSc1
a	a	k8xC
Xfce	Xfce	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jelikož	jelikož	k8xS
není	být	k5eNaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
žádný	žádný	k3yNgInSc1
oficiální	oficiální	k2eAgInSc1d1
desktop	desktop	k1gInSc1
Linux	Linux	kA
<g/>
,	,	kIx,
desktopová	desktopový	k2eAgNnPc4d1
prostředí	prostředí	k1gNnPc4
a	a	k8xC
distribuce	distribuce	k1gFnPc4
Linuxu	linux	k1gInSc2
vybírají	vybírat	k5eAaImIp3nP
součásti	součást	k1gFnPc1
z	z	k7c2
fondu	fond	k1gInSc2
volného	volný	k2eAgInSc2d1
a	a	k8xC
otevřeného	otevřený	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgInSc7
vytvářejí	vytvářet	k5eAaImIp3nP
grafické	grafický	k2eAgNnSc4d1
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
GNOME	GNOME	kA
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgInPc4
příkazy	příkaz	k1gInPc4
pro	pro	k7c4
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
udělány	udělán	k2eAgInPc1d1
formou	forma	k1gFnSc7
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1
při	při	k7c6
vývoji	vývoj	k1gInSc6
volného	volný	k2eAgInSc2d1
softwaru	software	k1gInSc2
umožňuje	umožňovat	k5eAaImIp3nS
distribuovat	distribuovat	k5eAaBmF
jej	on	k3xPp3gMnSc4
různým	různý	k2eAgInPc3d1
týmům	tým	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
provádějí	provádět	k5eAaImIp3nP
jazykovou	jazykový	k2eAgFnSc4d1
lokalizaci	lokalizace	k1gFnSc4
některých	některý	k3yIgFnPc2
distribucí	distribuce	k1gFnPc2
systému	systém	k1gInSc2
Linux	linux	k1gInSc4
pro	pro	k7c4
použití	použití	k1gNnSc4
v	v	k7c6
lokálních	lokální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
jazyková	jazykový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Knoppix	Knoppix	k1gInSc1
v	v	k7c6
jazyce	jazyk	k1gInSc6
Sinhalese	Sinhalese	k1gFnSc2
byla	být	k5eAaImAgFnS
značně	značně	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
společnost	společnost	k1gFnSc1
Microsoft	Microsoft	kA
přeložila	přeložit	k5eAaPmAgFnS
Windows	Windows	kA
XP	XP	kA
do	do	k7c2
Sinhalese	Sinhalese	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
vývoji	vývoj	k1gInSc6
lokalizovaného	lokalizovaný	k2eAgInSc2d1
systému	systém	k1gInSc2
hraje	hrát	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
Lanka	lanko	k1gNnSc2
Linux	Linux	kA
User	usrat	k5eAaPmRp2nS
Group	Group	k1gInSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
kombinuje	kombinovat	k5eAaImIp3nS
znalosti	znalost	k1gFnPc4
univerzitních	univerzitní	k2eAgMnPc2d1
profesorů	profesor	k1gMnPc2
<g/>
,	,	kIx,
lingvistů	lingvista	k1gMnPc2
a	a	k8xC
místních	místní	k2eAgMnPc2d1
vývojářů	vývojář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Aplikace	aplikace	k1gFnSc1
</s>
<s>
Mnoho	mnoho	k4c1
populárních	populární	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Mozilla	Mozilla	k1gFnSc1
Firefox	Firefox	k1gInSc1
<g/>
,	,	kIx,
OpenOffice	OpenOffice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
LibreOffice	LibreOffic	k1gMnSc4
<g/>
,	,	kIx,
Blender	Blender	k1gInSc4
jsou	být	k5eAaImIp3nP
ke	k	k7c3
stažení	stažení	k1gNnSc3
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
a	a	k8xC
jejich	jejich	k3xOp3gFnPc4
distribuce	distribuce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
některé	některý	k3yIgFnPc4
aplikace	aplikace	k1gFnPc4
původně	původně	k6eAd1
vyvinuté	vyvinutý	k2eAgInPc1d1
pro	pro	k7c4
Linux	linux	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Pidgin	Pidgin	k1gInSc4
a	a	k8xC
GIMP	GIMP	kA
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
vydány	vydat	k5eAaPmNgFnP
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
oblíbenosti	oblíbenost	k1gFnSc3
i	i	k8xC
pro	pro	k7c4
ostatní	ostatní	k1gNnSc4
operačních	operační	k2eAgFnPc2d1
systémy	systém	k1gInPc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
Windows	Windows	kA
a	a	k8xC
MacOS	MacOS	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
Linuxu	linux	k1gInSc6
podporováno	podporován	k2eAgNnSc1d1
rostoucí	rostoucí	k2eAgNnSc1d1
množství	množství	k1gNnSc1
proprietárních	proprietární	k2eAgFnPc2d1
desktopových	desktopový	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
Autodesk	Autodesk	kA
Maya	Maya	k?
<g/>
,	,	kIx,
Softimage	Softimag	k1gFnPc1
XSI	XSI	kA
a	a	k8xC
Apple	Apple	kA
Shake	Shake	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
animace	animace	k1gFnSc2
a	a	k8xC
vizuálních	vizuální	k2eAgInPc2d1
efektů	efekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
také	také	k9
několik	několik	k4yIc1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
přenášely	přenášet	k5eAaImAgFnP
své	své	k1gNnSc4
hry	hra	k1gFnSc2
nebo	nebo	k8xC
hry	hra	k1gFnSc2
jiných	jiný	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
na	na	k7c4
Linux	linux	k1gInSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Linux	linux	k1gInSc1
byl	být	k5eAaImAgInS
také	také	k6eAd1
podporovanou	podporovaný	k2eAgFnSc7d1
platformou	platforma	k1gFnSc7
jak	jak	k8xS,k8xC
pro	pro	k7c4
populární	populární	k2eAgFnPc4d1
distribuční	distribuční	k2eAgFnPc4d1
služby	služba	k1gFnPc4
Steam	Steam	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
pro	pro	k7c4
službu	služba	k1gFnSc4
Desura	Desur	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
typů	typ	k1gInPc2
aplikací	aplikace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pro	pro	k7c4
systémy	systém	k1gInPc4
Microsoft	Microsoft	kA
Windows	Windows	kA
a	a	k8xC
MacOS	MacOS	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
též	též	k9
přepracováno	přepracovat	k5eAaPmNgNnS
na	na	k7c4
Linux	linux	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buď	buď	k8xC
je	být	k5eAaImIp3nS
na	na	k7c6
internetu	internet	k1gInSc6
k	k	k7c3
nalezení	nalezení	k1gNnSc3
free	freat	k5eAaPmIp3nS
softwarová	softwarový	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
aplikace	aplikace	k1gFnSc1
nalezená	nalezený	k2eAgFnSc1d1
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
nebo	nebo	k8xC
verze	verze	k1gFnSc1
aplikace	aplikace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
funguje	fungovat	k5eAaImIp3nS
na	na	k7c6
Linuxu	linux	k1gInSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
Skype	Skyp	k1gInSc5
a	a	k8xC
některé	některý	k3yIgFnSc2
videohry	videohra	k1gFnSc2
jako	jako	k8xS,k8xC
Dota	Dot	k1gInSc2
2	#num#	k4
a	a	k8xC
Team	team	k1gInSc1
Fortress	Fortress	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Projekt	projekt	k1gInSc1
Wine	Win	k1gFnSc2
</s>
<s>
Projekt	projekt	k1gInSc1
Wine	Win	k1gInSc2
dále	daleko	k6eAd2
poskytuje	poskytovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
konkrétní	konkrétní	k2eAgFnSc2d1
kompatibility	kompatibilita	k1gFnSc2
se	s	k7c7
systémem	systém	k1gInSc7
Windows	Windows	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
spouštět	spouštět	k5eAaImF
nemodifikované	modifikovaný	k2eNgFnPc4d1
aplikace	aplikace	k1gFnPc4
systému	systém	k1gInSc2
Windows	Windows	kA
v	v	k7c6
systému	systém	k1gInSc6
Linux	Linux	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
podporována	podporovat	k5eAaImNgFnS
skupinami	skupina	k1gFnPc7
jako	jako	k8xS,k8xC
CodeWeavers	CodeWeavers	k1gInSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
vyrábí	vyrábět	k5eAaImIp3nP
komerční	komerční	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
softwaru	software	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
společnost	společnost	k1gFnSc1
Google	Google	k1gFnSc1
rovněž	rovněž	k9
poskytla	poskytnout	k5eAaPmAgFnS
finanční	finanční	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
na	na	k7c4
Projekt	projekt	k1gInSc4
Wine	Win	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Komponenty	komponenta	k1gFnPc1
a	a	k8xC
instalace	instalace	k1gFnSc1
</s>
<s>
Vedle	vedle	k7c2
viditelných	viditelný	k2eAgFnPc2d1
komponent	komponenta	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
X	X	kA
window	window	k?
managers	managers	k1gInSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
také	také	k9
důležité	důležitý	k2eAgInPc1d1
neviditelné	viditelný	k2eNgInPc1d1
programy	program	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
hostují	hostovat	k5eAaImIp3nP
freedesktop	freedesktop	k1gInSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
D-Bus	D-Bus	k1gMnSc1
nebo	nebo	k8xC
PulseAudio	PulseAudio	k1gMnSc1
<g/>
,	,	kIx,
jichž	jenž	k3xRgMnPc2
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
obě	dva	k4xCgNnPc4
hlavní	hlavní	k2eAgNnPc4d1
desktopová	desktopový	k2eAgNnPc4d1
prostředí	prostředí	k1gNnPc4
GNOME	GNOME	kA
a	a	k8xC
KDE	kde	k9
a	a	k8xC
každé	každý	k3xTgFnSc3
z	z	k7c2
nich	on	k3xPp3gFnPc2
nabízí	nabízet	k5eAaImIp3nS
grafické	grafický	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
napsané	napsaný	k2eAgNnSc1d1
pomocí	pomocí	k7c2
příslušné	příslušný	k2eAgFnSc2d1
sady	sada	k1gFnSc2
nástrojů	nástroj	k1gInPc2
GTK	GTK	kA
+	+	kIx~
nebo	nebo	k8xC
Qt	Qt	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Instalace	instalace	k1gFnSc1
<g/>
,	,	kIx,
aktualizace	aktualizace	k1gFnSc1
a	a	k8xC
odstraňování	odstraňování	k1gNnSc1
softwaru	software	k1gInSc2
v	v	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
Linux	Linux	kA
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
provádí	provádět	k5eAaImIp3nS
pomocí	pomocí	k7c2
správců	správce	k1gMnPc2
balíčků	balíček	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Synaptic	Synaptice	k1gFnPc2
Package	Package	k1gNnSc2
Manager	manager	k1gMnSc1
<g/>
,	,	kIx,
PackageKit	PackageKit	k1gMnSc1
a	a	k8xC
Yum	Yum	k1gMnSc1
Extender	Extender	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
většina	většina	k1gFnSc1
hlavních	hlavní	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
Linuxu	linux	k1gInSc2
má	mít	k5eAaImIp3nS
rozsáhlé	rozsáhlý	k2eAgInPc4d1
repozitáře	repozitář	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
často	často	k6eAd1
obsahují	obsahovat	k5eAaImIp3nP
desítky	desítka	k1gFnPc1
tisíc	tisíc	k4xCgInSc1
balíčků	balíček	k1gMnPc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
z	z	k7c2
oficiálních	oficiální	k2eAgInPc2d1
repozitářů	repozitář	k1gInPc2
k	k	k7c3
dispozici	dispozice	k1gFnSc3
celý	celý	k2eAgInSc4d1
software	software	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
lze	lze	k6eAd1
spustit	spustit	k5eAaPmF
na	na	k7c6
Linuxu	linux	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
mohou	moct	k5eAaImIp3nP
také	také	k9
instalovat	instalovat	k5eAaBmF
balíčky	balíček	k1gInPc4
z	z	k7c2
neoficiálních	oficiální	k2eNgInPc2d1,k2eAgInPc2d1
repozitářů	repozitář	k1gInPc2
<g/>
,	,	kIx,
stahovat	stahovat	k5eAaImF
předkompilované	předkompilovaný	k2eAgInPc4d1
balíky	balík	k1gInPc4
přímo	přímo	k6eAd1
z	z	k7c2
webových	webový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
nebo	nebo	k8xC
sami	sám	k3xTgMnPc1
sestavit	sestavit	k5eAaPmF
zdrojový	zdrojový	k2eAgInSc4d1
kód	kód	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
tyto	tento	k3xDgFnPc1
metody	metoda	k1gFnPc1
přicházejí	přicházet	k5eAaImIp3nP
s	s	k7c7
různými	různý	k2eAgInPc7d1
stupni	stupeň	k1gInPc7
obtížnosti	obtížnost	k1gFnSc2
<g/>
,	,	kIx,
kompilace	kompilace	k1gFnSc2
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
se	se	k3xPyFc4
obecně	obecně	k6eAd1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
náročný	náročný	k2eAgInSc4d1
proces	proces	k1gInSc4
pro	pro	k7c4
nové	nový	k2eAgMnPc4d1
uživatele	uživatel	k1gMnPc4
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
moderních	moderní	k2eAgFnPc6d1
distribucích	distribuce	k1gFnPc6
je	být	k5eAaImIp3nS
málo	málo	k6eAd1
potřebná	potřebný	k2eAgFnSc1d1
a	a	k8xC
není	být	k5eNaImIp3nS
specifickou	specifický	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
pro	pro	k7c4
Linux	linux	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Netbooky	Netbook	k1gInPc1
</s>
<s>
Distribuce	distribuce	k1gFnSc1
Linuxu	linux	k1gInSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
populární	populární	k2eAgFnSc1d1
i	i	k9
na	na	k7c6
trhu	trh	k1gInSc6
s	s	k7c7
netbooky	netbook	k1gInPc7
u	u	k7c2
řady	řada	k1gFnSc2
zařízení	zařízení	k1gNnSc2
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Asus	Asus	k1gInSc4
Eee	Eee	k1gFnSc2
PC	PC	kA
nebo	nebo	k8xC
Acer	Acer	k1gMnSc1
Aspire	Aspir	k1gMnSc5
One	One	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
společnost	společnost	k1gFnSc1
Google	Google	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
systém	systém	k1gInSc4
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
jako	jako	k8xC,k8xS
minimalistický	minimalistický	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
založený	založený	k2eAgInSc1d1
na	na	k7c6
operačních	operační	k2eAgInPc6d1
systémech	systém	k1gInPc6
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
nekontroluje	kontrolovat	k5eNaImIp3nS,k5eAaImIp3nS
žádné	žádný	k3yNgFnPc4
webové	webový	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
správce	správce	k1gMnSc2
souborů	soubor	k1gInPc2
a	a	k8xC
přehrávačů	přehrávač	k1gInPc2
médií	médium	k1gNnPc2
(	(	kIx(
<g/>
v	v	k7c6
pozdějších	pozdní	k2eAgFnPc6d2
verzích	verze	k1gFnPc6
byla	být	k5eAaImAgFnS
přidána	přidán	k2eAgFnSc1d1
určitá	určitý	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
podpory	podpora	k1gFnSc2
aplikací	aplikace	k1gFnPc2
pro	pro	k7c4
Android	android	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Netbooky	Netbook	k1gInPc1
dodávané	dodávaný	k2eAgInPc1d1
s	s	k7c7
operačním	operační	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
označované	označovaný	k2eAgInPc4d1
jako	jako	k8xC,k8xS
Chromebooky	Chromebook	k1gInPc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
na	na	k7c6
trhu	trh	k1gInSc6
objevily	objevit	k5eAaPmAgInP
v	v	k7c6
červnu	červen	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Servery	server	k1gInPc1
<g/>
,	,	kIx,
sálové	sálový	k2eAgInPc1d1
počítače	počítač	k1gInPc1
a	a	k8xC
superpočítače	superpočítač	k1gInPc1
</s>
<s>
Distribuce	distribuce	k1gFnSc1
Linuxu	linux	k1gInSc2
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
dlouho	dlouho	k6eAd1
využívány	využívat	k5eAaPmNgInP,k5eAaImNgInP
jako	jako	k8xS,k8xC
operační	operační	k2eAgInPc1d1
systémy	systém	k1gInPc1
pro	pro	k7c4
servery	server	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
Netcraft	Netcrafta	k1gFnPc2
oznámila	oznámit	k5eAaPmAgFnS
v	v	k7c6
září	září	k1gNnSc6
2006	#num#	k4
<g/>
,	,	kIx,
že	že	k8xS
osm	osm	k4xCc1
z	z	k7c2
deseti	deset	k4xCc2
nejspolehlivějšími	spolehlivý	k2eAgFnPc7d3
hostingovými	hostingový	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
provozovalo	provozovat	k5eAaImAgNnS
linuxové	linuxový	k2eAgFnPc4d1
distribuce	distribuce	k1gFnPc4
na	na	k7c6
svých	svůj	k3xOyFgInPc6
webových	webový	k2eAgInPc6d1
serverech	server	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2008	#num#	k4
byly	být	k5eAaImAgFnP
distribuce	distribuce	k1gFnPc1
Linuxu	linux	k1gInSc2
zastoupeny	zastoupit	k5eAaPmNgFnP
v	v	k7c6
pět	pět	k4xCc1
z	z	k7c2
deseti	deset	k4xCc2
případů	případ	k1gInPc2
serverových	serverový	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
FreeBSD	FreeBSD	k1gFnPc2
tři	tři	k4xCgInPc4
z	z	k7c2
deseti	deset	k4xCc2
a	a	k8xC
Microsoft	Microsoft	kA
dva	dva	k4xCgInPc4
z	z	k7c2
deseti	deset	k4xCc2
<g/>
,	,	kIx,
od	od	k7c2
února	únor	k1gInSc2
2010	#num#	k4
distribuce	distribuce	k1gFnSc1
Linuxu	linux	k1gInSc2
representovaly	representovat	k5eAaImAgFnP
šest	šest	k4xCc1
z	z	k7c2
deseti	deset	k4xCc2
případů	případ	k1gInPc2
<g/>
,	,	kIx,
FreeBSD	FreeBSD	k1gFnPc2
tři	tři	k4xCgInPc4
z	z	k7c2
deseti	deset	k4xCc2
a	a	k8xC
Microsoft	Microsoft	kA
jeden	jeden	k4xCgMnSc1
z	z	k7c2
deseti	deset	k4xCc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Linux	linux	k1gInSc1
vedl	vést	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Distribuce	distribuce	k1gFnSc1
Linuxu	linux	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
základní	základní	k2eAgInSc1d1
kámen	kámen	k1gInSc1
kombinace	kombinace	k1gFnSc2
serverů	server	k1gInPc2
a	a	k8xC
softwarových	softwarový	k2eAgInPc2d1
systémů	systém	k1gInPc2
LAMP	lampa	k1gFnPc2
(	(	kIx(
<g/>
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
Apache	Apache	k1gFnSc1
<g/>
,	,	kIx,
MariaDB	MariaDB	k1gFnSc1
/	/	kIx~
MySQL	MySQL	k1gFnSc1
<g/>
,	,	kIx,
Perl	perl	k1gInSc1
/	/	kIx~
PHP	PHP	kA
/	/	kIx~
Python	Python	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
získaly	získat	k5eAaPmAgFnP
popularitu	popularita	k1gFnSc4
mezi	mezi	k7c7
vývojáři	vývojář	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Distribuce	distribuce	k1gFnSc1
systému	systém	k1gInSc2
Linux	Linux	kA
se	se	k3xPyFc4
stávají	stávat	k5eAaImIp3nP
stále	stále	k6eAd1
populárnějšími	populární	k2eAgFnPc7d2
na	na	k7c6
sálových	sálový	k2eAgInPc6d1
počítačích	počítač	k1gInPc6
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
kvůli	kvůli	k7c3
cenám	cena	k1gFnPc3
a	a	k8xC
modelu	model	k1gInSc2
s	s	k7c7
otevřeným	otevřený	k2eAgInSc7d1
zdrojovým	zdrojový	k2eAgInSc7d1
kódem	kód	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
2009	#num#	k4
počítačový	počítačový	k2eAgInSc1d1
gigant	gigant	k1gInSc1
IBM	IBM	kA
oznámil	oznámit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
převážně	převážně	k6eAd1
prodává	prodávat	k5eAaImIp3nS
a	a	k8xC
prodává	prodávat	k5eAaImIp3nS
Enterprise	Enterprise	k1gFnSc1
Linux	linux	k1gInSc1
Server	server	k1gInSc1
na	na	k7c6
bázi	báze	k1gFnSc6
mainframe	mainframe	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
síti	síť	k1gFnSc6
LinuxCon	LinuxCona	k1gFnPc2
North	North	k1gInSc1
America	Americ	k1gInSc2
2015	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
IBM	IBM	kA
LinuxONE	LinuxONE	k1gFnSc1
řadu	řad	k1gInSc2
sálových	sálový	k2eAgMnPc2d1
počítačů	počítač	k1gMnPc2
speciálně	speciálně	k6eAd1
navržených	navržený	k2eAgFnPc2d1
pro	pro	k7c4
provozování	provozování	k1gNnSc4
systému	systém	k1gInSc2
Linux	linux	k1gInSc4
s	s	k7c7
otevřeným	otevřený	k2eAgInSc7d1
zdrojovým	zdrojový	k2eAgInSc7d1
kódem	kód	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Distribuce	distribuce	k1gFnSc1
Linuxu	linux	k1gInSc2
jsou	být	k5eAaImIp3nP
také	také	k9
dominantní	dominantní	k2eAgInSc4d1
jako	jako	k8xS,k8xC
operační	operační	k2eAgInSc4d1
systémy	systém	k1gInPc4
pro	pro	k7c4
superpočítače	superpočítač	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Inteligentní	inteligentní	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
</s>
<s>
Android	android	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
dominantním	dominantní	k2eAgInSc7d1
mobilním	mobilní	k2eAgInSc7d1
operačním	operační	k2eAgInSc7d1
systémem	systém	k1gInSc7
pro	pro	k7c4
smartphony	smartphon	k1gMnPc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
běží	běžet	k5eAaImIp3nS
na	na	k7c4
79,3	79,3	k4
<g/>
%	%	kIx~
smartphonech	smartphon	k1gInPc6
na	na	k7c4
světe	svět	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Android	android	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
oblíbeným	oblíbený	k2eAgInSc7d1
operačním	operační	k2eAgInSc7d1
systémem	systém	k1gInSc7
pro	pro	k7c4
tablety	tableta	k1gFnPc4
a	a	k8xC
chytré	chytrá	k1gFnPc4
televize	televize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
a	a	k8xC
PDA	PDA	kA
s	s	k7c7
operačním	operační	k2eAgInSc7d1
systémem	systém	k1gInSc7
Linux	Linux	kA
na	na	k7c6
open-source	open-sourka	k1gFnSc6
platformách	platforma	k1gFnPc6
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
běžnějšími	běžný	k2eAgMnPc7d2
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
příklady	příklad	k1gInPc1
zahrnují	zahrnovat	k5eAaImIp3nP
Nokia	Nokia	kA
N	N	kA
<g/>
810	#num#	k4
<g/>
,	,	kIx,
Neo	Neo	k1gFnSc1
<g/>
1973	#num#	k4
značky	značka	k1gFnPc4
Openmoko	Openmoko	k1gNnSc1
a	a	k8xC
Motorola	Motorola	kA
ROKR	ROKR	kA
E	E	kA
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nokia	Nokia	kA
Maemo	Maema	k1gFnSc5
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejčasnějších	časný	k2eAgInPc2d3
mobilních	mobilní	k2eAgInPc2d1
operačních	operační	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c6
Debianu	Debian	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
sloučen	sloučit	k5eAaPmNgInS
s	s	k7c7
Moblinem	Moblin	k1gInSc7
od	od	k7c2
Intelu	Intel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
ukončen	ukončit	k5eAaPmNgInS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
společnosti	společnost	k1gFnSc2
Tizen	Tizna	k1gFnPc2
<g/>
,	,	kIx,
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
zaměřeného	zaměřený	k2eAgMnSc4d1
na	na	k7c4
mobilní	mobilní	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
a	a	k8xC
také	také	k9
IVI	IVI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tizen	Tizen	k1gInSc1
je	být	k5eAaImIp3nS
projekt	projekt	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
nadace	nadace	k1gFnSc2
Linux	Linux	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc4
produktů	produkt	k1gInPc2
od	od	k7c2
společnosti	společnost	k1gFnSc2
Samsung	Samsung	kA
už	už	k6eAd1
běží	běžet	k5eAaImIp3nS
na	na	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
Tizen	Tizna	k1gFnPc2
<g/>
,	,	kIx,
Samsung	Samsung	kA
Gear	Gear	k1gInSc4
2	#num#	k4
je	být	k5eAaImIp3nS
nejvýznamnějším	významný	k2eAgInSc7d3
příkladem	příklad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smartphony	Smartphona	k1gFnSc2
Samsung	Samsung	kA
Z	z	k7c2
mají	mít	k5eAaImIp3nP
používat	používat	k5eAaImF
Tizen	Tizen	k1gInSc4
namísto	namísto	k7c2
Androidu	android	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mozilla	Mozilla	k6eAd1
Firefox	Firefox	k1gInSc1
OS	OS	kA
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
jádra	jádro	k1gNnSc2
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
vrstvy	vrstva	k1gFnSc2
abstrakce	abstrakce	k1gFnSc2
hardwaru	hardware	k1gInSc2
<g/>
,	,	kIx,
prostředí	prostředí	k1gNnSc2
runtime	runtime	k1gInSc4
založeného	založený	k2eAgInSc2d1
na	na	k7c6
webových	webový	k2eAgInPc6d1
standardech	standard	k1gInPc6
a	a	k8xC
uživatelského	uživatelský	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
a	a	k8xC
integrovaného	integrovaný	k2eAgMnSc2d1
webového	webový	k2eAgMnSc2d1
prohlížeče	prohlížeč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
Canonical	Canonical	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
aplikaci	aplikace	k1gFnSc4
Ubuntu	Ubunt	k1gInSc2
Touch	Touch	k1gInSc4
s	s	k7c7
cílem	cíl	k1gInSc7
přiblížit	přiblížit	k5eAaPmF
se	se	k3xPyFc4
k	k	k7c3
operačnímu	operační	k2eAgInSc3d1
systému	systém	k1gInSc3
na	na	k7c6
PC	PC	kA
Ubuntu	Ubunt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
také	také	k9
poskytuje	poskytovat	k5eAaImIp3nS
plnou	plný	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
Ubuntu	Ubunta	k1gFnSc4
plochu	plocha	k1gFnSc4
pro	pro	k7c4
připojení	připojení	k1gNnSc4
k	k	k7c3
externímu	externí	k2eAgInSc3d1
monitoru	monitor	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Vestavěná	vestavěný	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Zařízení	zařízení	k1gNnSc1
Embedded	Embedded	k1gMnSc1
Linux	linux	k1gInSc1
a	a	k8xC
Linux	linux	k1gInSc1
Jolla	Jollo	k1gNnSc2
Phone	Phon	k1gInSc5
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
Sailfish	Sailfish	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
Linuxu	linux	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
pro	pro	k7c4
zábavu	zábava	k1gFnSc4
v	v	k7c6
automobilu	automobil	k1gInSc6
<g/>
,	,	kIx,
modelu	model	k1gInSc6
Tesla	Tesla	k1gFnSc1
Model	model	k1gInSc1
S	s	k7c7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
softwaru	software	k1gInSc6
Ubuntu	Ubunt	k1gInSc2
<g/>
,	,	kIx,
Nokia	Nokia	kA
X	X	kA
–	–	k?
smartphone	smartphon	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
běží	běžet	k5eAaImIp3nS
na	na	k7c6
jádru	jádro	k1gNnSc6
z	z	k7c2
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
nízké	nízký	k2eAgFnSc3d1
ceně	cena	k1gFnSc3
a	a	k8xC
snadné	snadný	k2eAgFnSc3d1
práci	práce	k1gFnSc3
s	s	k7c7
ním	on	k3xPp3gNnSc7
se	se	k3xPyFc4
Linux	linux	k1gInSc1
často	často	k6eAd1
používá	používat	k5eAaImIp3nS
ve	v	k7c6
vestavěných	vestavěný	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Populární	populární	k2eAgInSc1d1
digitální	digitální	k2eAgInSc1d1
videorekordér	videorekordér	k1gInSc1
TiVo	TiVo	k6eAd1
například	například	k6eAd1
používá	používat	k5eAaImIp3nS
upravený	upravený	k2eAgInSc1d1
systém	systém	k1gInSc1
Linux	Linux	kA
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
několik	několik	k4yIc4
síťových	síťový	k2eAgInPc2d1
firewallů	firewall	k1gInPc2
a	a	k8xC
routerů	router	k1gInPc2
od	od	k7c2
výrobců	výrobce	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Cisco	Cisco	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linksys	Linksys	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korg	Korg	k1gInSc1
OASYS	OASYS	kA
<g/>
,	,	kIx,
Korg	Korg	k1gMnSc1
KRONOS	Kronos	k1gMnSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
Yamaha	Yamaha	kA
Motif	Motif	kA
XS	XS	kA
<g/>
/	/	kIx~
<g/>
Motif	Motif	kA
XF	XF	kA
<g/>
,	,	kIx,
Yamaha	yamaha	k1gFnSc1
S	s	k7c7
<g/>
90	#num#	k4
<g/>
XS	XS	kA
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
70	#num#	k4
<g/>
XS	XS	kA
<g/>
,	,	kIx,
syntezátory	syntezátor	k1gInPc1
Yamaha	Yamaha	kA
MOX	MOX	kA
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
MOX	MOX	kA
<g/>
8	#num#	k4
<g/>
,	,	kIx,
generátor	generátor	k1gInSc1
Yamaha	yamaha	k1gFnSc1
Motif-Rack	Motif-Rack	k1gMnSc1
XS	XS	kA
a	a	k8xC
digitální	digitální	k2eAgInSc4d1
klavír	klavír	k1gInSc4
Roland	Rolanda	k1gFnPc2
RD-700GX	RD-700GX	k1gFnPc2
jsou	být	k5eAaImIp3nP
také	také	k9
provozovány	provozovat	k5eAaImNgInP
na	na	k7c6
Linuxu	linux	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linux	linux	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
používán	používat	k5eAaImNgInS
v	v	k7c6
systémech	systém	k1gInPc6
řízení	řízení	k1gNnSc2
podsvícení	podsvícení	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
konzola	konzola	k1gFnSc1
WholeHogIII	WholeHogIII	k1gFnSc1
</s>
<s>
Hry	hra	k1gFnPc1
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
Linux	linux	k1gInSc4
dostupných	dostupný	k2eAgNnPc2d1
jen	jen	k6eAd1
málo	málo	k4c4
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
více	hodně	k6eAd2
her	hra	k1gFnPc2
s	s	k7c7
podporuje	podporovat	k5eAaImIp3nS
Linuxu	linux	k1gInSc2
(	(	kIx(
<g/>
především	především	k9
Indie	Indie	k1gFnSc1
hry	hra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
několika	několik	k4yIc2
herních	herní	k2eAgMnPc2d1
AAA	AAA	kA
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Android	android	k1gInSc1
<g/>
,	,	kIx,
populární	populární	k2eAgFnSc1d1
mobilní	mobilní	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
používá	používat	k5eAaImIp3nS
jádro	jádro	k1gNnSc4
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
hodně	hodně	k6eAd1
zajímat	zajímat	k5eAaImF
vývojáře	vývojář	k1gMnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
platforem	platforma	k1gFnPc2
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
mobilních	mobilní	k2eAgFnPc2d1
her	hra	k1gFnPc2
spolu	spolu	k6eAd1
s	s	k7c7
operačním	operační	k2eAgInSc7d1
systémem	systém	k1gInSc7
iOS	iOS	k?
od	od	k7c2
společnosti	společnost	k1gFnSc2
Apple	Apple	kA
pro	pro	k7c4
zařízení	zařízení	k1gNnPc4
iPhone	iPhon	k1gInSc5
a	a	k8xC
iPad	iPado	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
společnost	společnost	k1gFnSc1
Valve	Valev	k1gFnSc2
vydala	vydat	k5eAaPmAgFnS
Linuxovou	linuxový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
systému	systém	k1gInSc2
Steam	Steam	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
oblíbenou	oblíbený	k2eAgFnSc7d1
herní	herní	k2eAgFnSc7d1
distribuční	distribuční	k2eAgFnSc7d1
platformou	platforma	k1gFnSc7
na	na	k7c6
PC	PC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
steam	steam	k6eAd1
her	hra	k1gFnPc2
bylo	být	k5eAaImAgNnS
přeneseno	přenést	k5eAaPmNgNnS
na	na	k7c4
Linux	linux	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
společnost	společnost	k1gFnSc1
Valve	Valev	k1gFnSc2
vydala	vydat	k5eAaPmAgFnS
SteamOS	SteamOS	k1gFnSc1
<g/>
,	,	kIx,
herní	herní	k2eAgInSc1d1
systém	systém	k1gInSc1
založený	založený	k2eAgInSc1d1
na	na	k7c4
Debianu	Debiana	k1gFnSc4
<g/>
,	,	kIx,
určený	určený	k2eAgInSc4d1
pro	pro	k7c4
beta	beta	k1gNnSc4
testování	testování	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
plánem	plán	k1gInSc7
používat	používat	k5eAaImF
Steam	Steam	k1gInSc4
machines	machinesa	k1gFnPc2
jako	jako	k8xC,k8xS
herní	herní	k2eAgFnSc4d1
a	a	k8xC
zábavní	zábavní	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valve	Valev	k1gFnSc2
vyvinul	vyvinout	k5eAaPmAgInS
také	také	k9
VOGL	VOGL	kA
<g/>
,	,	kIx,
nástroj	nástroj	k1gInSc4
pro	pro	k7c4
rozpoznávání	rozpoznávání	k1gNnSc4
OpenGL	OpenGL	k1gFnSc2
<g/>
,	,	kIx,
určený	určený	k2eAgInSc1d1
k	k	k7c3
podpoře	podpora	k1gFnSc3
vývoje	vývoj	k1gInSc2
videoher	videohra	k1gFnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
přenosu	přenos	k1gInSc2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
Source	Sourec	k1gInSc2
engine	enginout	k5eAaPmIp3nS
na	na	k7c4
Linux	linux	k1gInSc4
desktop	desktop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
snahy	snaha	k1gFnSc2
společnosti	společnost	k1gFnSc2
Valve	Valev	k1gFnSc2
jsou	být	k5eAaImIp3nP
nyní	nyní	k6eAd1
na	na	k7c6
platformě	platforma	k1gFnSc6
Linux	Linux	kA
již	již	k6eAd1
nativně	nativně	k6eAd1
dostupné	dostupný	k2eAgFnPc1d1
některé	některý	k3yIgFnPc1
významné	významný	k2eAgFnPc1d1
hry	hra	k1gFnPc1
jako	jako	k8xS,k8xC
DotA	DotA	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
Team	team	k1gInSc1
Fortress	Fortress	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
Portal	Portal	k1gMnSc1
<g/>
,	,	kIx,
Portal	Portal	k1gMnSc1
2	#num#	k4
a	a	k8xC
Left	Left	k1gMnSc1
4	#num#	k4
Dead	Dead	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
Nvidia	Nvidium	k1gNnSc2
vydala	vydat	k5eAaPmAgFnS
herní	herní	k2eAgInSc4d1
tablet	tablet	k1gInSc4
Shield	Shielda	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
pokusem	pokus	k1gInSc7
používat	používat	k5eAaImF
platformu	platforma	k1gFnSc4
Android	android	k1gInSc1
coby	coby	k?
specializované	specializovaný	k2eAgFnPc4d1
herní	herní	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Někteří	některý	k3yIgMnPc1
uživatelé	uživatel	k1gMnPc1
systému	systém	k1gInSc2
Linux	linux	k1gInSc4
hrají	hrát	k5eAaImIp3nP
Windows	Windows	kA
hry	hra	k1gFnSc2
přes	přes	k7c4
Wine	Wine	k1gFnSc4
nebo	nebo	k8xC
CrossOver	CrossOver	k1gInSc4
Linux	Linux	kA
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2018	#num#	k4
vydal	vydat	k5eAaPmAgInS
Valve	Valev	k1gFnSc2
svou	svůj	k3xOyFgFnSc7
vlastní	vlastní	k2eAgMnSc1d1
fork	fork	k6eAd1
Wine	Wine	k1gInSc1
zaměřený	zaměřený	k2eAgInSc1d1
na	na	k7c4
hry	hra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
několik	několik	k4yIc4
vylepšení	vylepšení	k1gNnPc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
vanilla	vanilla	k6eAd1
Wine	Wine	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
implementace	implementace	k1gFnPc4
DirectX	DirectX	k1gFnSc4
9	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
a	a	k8xC
12	#num#	k4
založené	založený	k2eAgFnSc2d1
na	na	k7c4
Vulkan	Vulkan	k1gInSc4
<g/>
,	,	kIx,
integraci	integrace	k1gFnSc4
Steam	Steam	k1gInSc4
<g/>
,	,	kIx,
lepší	dobrý	k2eAgFnSc1d2
podpora	podpora	k1gFnSc1
celé	celý	k2eAgFnSc2d1
obrazovky	obrazovka	k1gFnSc2
a	a	k8xC
herního	herní	k2eAgInSc2d1
ovladače	ovladač	k1gInSc2
a	a	k8xC
lepší	dobrý	k2eAgInSc4d2
výkon	výkon	k1gInSc4
pro	pro	k7c4
vícevláknové	vícevláknový	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Speciální	speciální	k2eAgNnSc1d1
použití	použití	k1gNnSc1
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
flexibilitě	flexibilita	k1gFnSc3
<g/>
,	,	kIx,
přizpůsobitelnosti	přizpůsobitelnost	k1gFnSc6
a	a	k8xC
volné	volný	k2eAgFnSc6d1
a	a	k8xC
otevřené	otevřený	k2eAgFnSc6d1
povaze	povaha	k1gFnSc6
Linuxu	linux	k1gInSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
jej	on	k3xPp3gNnSc4
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
vyladit	vyladit	k5eAaPmF
pro	pro	k7c4
konkrétní	konkrétní	k2eAgInSc4d1
účel	účel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
pro	pro	k7c4
vytváření	vytváření	k1gNnSc4
specializované	specializovaný	k2eAgFnSc2d1
linuxové	linuxový	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
<g/>
:	:	kIx,
začít	začít	k5eAaPmF
od	od	k7c2
nuly	nula	k1gFnSc2
nebo	nebo	k8xC
z	z	k7c2
obecné	obecný	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
jako	jako	k8xS,k8xC
základny	základna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Distribuce	distribuce	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
často	často	k6eAd1
používají	používat	k5eAaImIp3nP
k	k	k7c3
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3
<g/>
,	,	kIx,
zahrnují	zahrnovat	k5eAaImIp3nP
Debian	Debian	k1gInSc4
<g/>
,	,	kIx,
Fedoru	Fedora	k1gFnSc4
<g/>
,	,	kIx,
Ubuntu	Ubunta	k1gFnSc4
(	(	kIx(
<g/>
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
samo	sám	k3xTgNnSc1
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
Debianu	Debian	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Arch	arch	k1gInSc1
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
Gentoo	Gentoo	k1gNnSc1
a	a	k8xC
Slackware	Slackwar	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
distribuce	distribuce	k1gFnSc1
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
postavené	postavený	k2eAgNnSc1d1
od	od	k7c2
začátku	začátek	k1gInSc2
<g/>
,	,	kIx,
nemají	mít	k5eNaImIp3nP
obecný	obecný	k2eAgInSc4d1
základ	základ	k1gInSc4
<g/>
;	;	kIx,
namísto	namísto	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
soustředí	soustředit	k5eAaPmIp3nS
na	na	k7c4
filozofii	filozofie	k1gFnSc4
JeOS	JeOS	k1gFnSc2
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zahrnuje	zahrnovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
nezbytné	nezbytný	k2eAgFnPc4d1,k2eNgFnPc4d1
součásti	součást	k1gFnPc4
a	a	k8xC
vyhýbá	vyhýbat	k5eAaImIp3nS
se	se	k3xPyFc4
režijním	režijní	k2eAgInPc3d1
nadbytečným	nadbytečný	k2eAgInPc3d1
nákladům	náklad	k1gInPc3
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
distribucí	distribuce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Domácí	domácí	k2eAgNnSc1d1
kino	kino	k1gNnSc1
(	(	kIx(
<g/>
Home	Home	k1gNnSc1
theater	theatra	k1gFnPc2
PC	PC	kA
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
PC	PC	kA
domácí	domácí	k2eAgNnSc1d1
kino	kino	k1gNnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
HTPC	HTPC	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
počítač	počítač	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
především	především	k6eAd1
jako	jako	k8xS,k8xC
zábavní	zábavní	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
systém	systém	k1gInSc1
domácího	domácí	k2eAgNnSc2d1
kina	kino	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
je	být	k5eAaImIp3nS
připojen	připojit	k5eAaPmNgInS
k	k	k7c3
televizoru	televizor	k1gInSc3
a	a	k8xC
často	často	k6eAd1
k	k	k7c3
dalšímu	další	k2eAgInSc3d1
zvukovému	zvukový	k2eAgInSc3d1
systému	systém	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
HTPC	HTPC	kA
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
OpenELEC	OpenELEC	k1gFnSc4
<g/>
,	,	kIx,
linuxovou	linuxový	k2eAgFnSc4d1
distribuce	distribuce	k1gFnPc4
<g/>
,	,	kIx,
speciálně	speciálně	k6eAd1
vyladěná	vyladěný	k2eAgFnSc1d1
pro	pro	k7c4
tento	tento	k3xDgInSc4
systém	systém	k1gInSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
software	software	k1gInSc4
pro	pro	k7c4
centrum	centrum	k1gNnSc4
médií	médium	k1gNnPc2
Kodi	Kod	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
od	od	k7c2
základů	základ	k1gInPc2
dodržujících	dodržující	k2eAgInPc2d1
princip	princip	k1gInSc4
JeOS	JeOS	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
OS	OS	kA
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
lehký	lehký	k2eAgInSc1d1
a	a	k8xC
vhodný	vhodný	k2eAgInSc1d1
pro	pro	k7c4
omezený	omezený	k2eAgInSc4d1
rozsah	rozsah	k1gInSc4
použití	použití	k1gNnSc2
HTPC	HTPC	kA
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
dispozici	dispozice	k1gFnSc3
jsou	být	k5eAaImIp3nP
také	také	k9
speciální	speciální	k2eAgFnPc4d1
edice	edice	k1gFnPc4
distribucí	distribuce	k1gFnSc7
Linux	Linux	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
obsahují	obsahovat	k5eAaImIp3nP
software	software	k1gInSc4
MythTV	MythTV	k1gFnSc2
media	medium	k1gNnSc2
center	centrum	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Mythbuntu	Mythbunt	k1gInSc2
<g/>
,	,	kIx,
speciální	speciální	k2eAgInPc1d1
vydání	vydání	k1gNnSc4
Ubuntu	Ubunt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Digitální	digitální	k2eAgNnSc1d1
zabezpečení	zabezpečení	k1gNnSc1
</s>
<s>
Kali	Kali	k6eAd1
Linux	Linux	kA
je	být	k5eAaImIp3nS
Linuxová	linuxový	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c4
Debianu	Debiana	k1gFnSc4
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
digitální	digitální	k2eAgInSc4d1
forenzní	forenzní	k2eAgInSc4d1
a	a	k8xC
penetrační	penetrační	k2eAgNnSc4d1
testování	testování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodává	dodávat	k5eAaImIp3nS
se	se	k3xPyFc4
s	s	k7c7
předinstalovanými	předinstalovaný	k2eAgNnPc7d1
několika	několik	k4yIc7
softwarovými	softwarový	k2eAgFnPc7d1
aplikacemi	aplikace	k1gFnPc7
pro	pro	k7c4
penetrační	penetrační	k2eAgNnSc4d1
testování	testování	k1gNnSc4
a	a	k8xC
identifikaci	identifikace	k1gFnSc4
bezpečnostních	bezpečnostní	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nástroj	nástroj	k1gInSc1
BackBox	BackBox	k1gInSc4
od	od	k7c2
společnosti	společnost	k1gFnSc2
Ubuntu	Ubunt	k1gInSc2
poskytuje	poskytovat	k5eAaImIp3nS
předinstalované	předinstalovaný	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
pro	pro	k7c4
zabezpečení	zabezpečení	k1gNnSc4
a	a	k8xC
síťovou	síťový	k2eAgFnSc4d1
analýzu	analýza	k1gFnSc4
pro	pro	k7c4
etické	etický	k2eAgNnSc4d1
hackování	hackování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
linuxových	linuxový	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
vytvořených	vytvořený	k2eAgFnPc2d1
s	s	k7c7
ochranou	ochrana	k1gFnSc7
soukromí	soukromí	k1gNnPc2
<g/>
,	,	kIx,
tajemstvím	tajemství	k1gNnSc7
<g/>
,	,	kIx,
anonymitou	anonymita	k1gFnSc7
v	v	k7c6
síti	síť	k1gFnSc6
a	a	k8xC
bezpečností	bezpečnost	k1gFnSc7
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Tails	Tailsa	k1gFnPc2
<g/>
,	,	kIx,
Tin	Tina	k1gFnPc2
Hat	hat	k0
Linux	linux	k1gInSc1
a	a	k8xC
Tinfoil	Tinfoil	k1gInSc1
Hat	hat	k0
Linux	linux	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lightweight	Lightweight	k2eAgInSc1d1
Portable	portable	k1gInSc1
Security	Securita	k1gFnSc2
je	být	k5eAaImIp3nS
distribuce	distribuce	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c4
Arch	arch	k1gInSc4
Linuxu	linux	k1gInSc2
a	a	k8xC
vyvinutá	vyvinutý	k2eAgFnSc1d1
Ministerstvem	ministerstvo	k1gNnSc7
obrany	obrana	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tor-ramdisk	Tor-ramdisk	k1gInSc1
je	být	k5eAaImIp3nS
minimální	minimální	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
vytvořená	vytvořený	k2eAgFnSc1d1
výhradně	výhradně	k6eAd1
pro	pro	k7c4
hostování	hostování	k1gNnSc4
anonymního	anonymní	k2eAgInSc2d1
softwaru	software	k1gInSc2
Tor	Tor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Záchrana	záchrana	k1gFnSc1
systému	systém	k1gInSc2
</s>
<s>
Linux	linux	k1gInSc1
Live	Liv	k1gFnSc2
CD	CD	kA
se	se	k3xPyFc4
již	již	k6eAd1
dlouho	dlouho	k6eAd1
používá	používat	k5eAaImIp3nS
jako	jako	k9
nástroj	nástroj	k1gInSc1
pro	pro	k7c4
obnovu	obnova	k1gFnSc4
dat	datum	k1gNnPc2
z	z	k7c2
poškozeného	poškozený	k2eAgInSc2d1
počítačového	počítačový	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
pro	pro	k7c4
opravu	oprava	k1gFnSc4
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
této	tento	k3xDgFnSc2
myšlenky	myšlenka	k1gFnSc2
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
několik	několik	k4yIc1
linuxových	linuxový	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
přizpůsobených	přizpůsobený	k2eAgInPc2d1
pro	pro	k7c4
tento	tento	k3xDgInSc4
účel	účel	k1gInSc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
většina	většina	k1gFnSc1
používá	používat	k5eAaImIp3nS
GParted	GParted	k1gInSc4
jako	jako	k8xC,k8xS
editor	editor	k1gInSc4
oddílů	oddíl	k1gInPc2
s	s	k7c7
doplňkovým	doplňkový	k2eAgInSc7d1
softwarem	software	k1gInSc7
pro	pro	k7c4
obnovu	obnova	k1gFnSc4
dat	datum	k1gNnPc2
a	a	k8xC
opravu	oprava	k1gFnSc4
systému	systém	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
GParted	GParted	k1gMnSc1
Live	Liv	k1gFnSc2
–	–	k?
distribuce	distribuce	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
Debianu	Debian	k1gInSc6
vyvinutém	vyvinutý	k2eAgInSc6d1
projektem	projekt	k1gInSc7
GParted	GParted	k1gInSc4
</s>
<s>
Parted	Parted	k1gMnSc1
Magic	Magice	k1gFnPc2
–	–	k?
komerční	komerční	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
Linuxu	linux	k1gInSc2
</s>
<s>
SystemRescueCD	SystemRescueCD	k?
–	–	k?
distribuce	distribuce	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c4
Gentoo	Gentoo	k1gNnSc4
s	s	k7c7
podporou	podpora	k1gFnSc7
pro	pro	k7c4
editaci	editace	k1gFnSc4
registru	registr	k1gInSc2
Windows	Windows	kA
</s>
<s>
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
</s>
<s>
SpaceX	SpaceX	k?
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
několik	několik	k4yIc1
redundantních	redundantní	k2eAgInPc2d1
letových	letový	k2eAgInPc2d1
počítačů	počítač	k1gInPc2
v	v	k7c6
konstrukci	konstrukce	k1gFnSc6
se	s	k7c7
systémem	systém	k1gInSc7
odolným	odolný	k2eAgInSc7d1
vůči	vůči	k7c3
chybám	chyba	k1gFnPc3
v	v	k7c6
raketě	raketa	k1gFnSc6
Falcon	Falcon	k1gNnSc1
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
motor	motor	k1gInSc4
Merlin	Merlin	k1gInSc1
je	být	k5eAaImIp3nS
řízen	řídit	k5eAaImNgInS
třemi	tři	k4xCgNnPc7
voting	voting	k1gInSc4
počítači	počítač	k1gInPc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dva	dva	k4xCgInPc4
počítače	počítač	k1gInPc4
mají	mít	k5eAaImIp3nP
fyzické	fyzický	k2eAgInPc4d1
procesory	procesor	k1gInPc4
a	a	k8xC
neustále	neustále	k6eAd1
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
kontrolují	kontrolovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linux	linux	k1gInSc1
není	být	k5eNaImIp3nS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
podstatě	podstata	k1gFnSc6
tolerantní	tolerantní	k2eAgNnSc1d1
k	k	k7c3
chybám	chyba	k1gFnPc3
(	(	kIx(
<g/>
žádný	žádný	k3yNgInSc1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
není	být	k5eNaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
funkcí	funkce	k1gFnSc7
celého	celý	k2eAgInSc2d1
systému	systém	k1gInSc2
včetně	včetně	k7c2
hardwaru	hardware	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
letový	letový	k2eAgInSc1d1
počítačový	počítačový	k2eAgInSc1d1
software	software	k1gInSc1
to	ten	k3xDgNnSc1
pokládá	pokládat	k5eAaImIp3nS
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
účel	účel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
flexibilitu	flexibilita	k1gFnSc4
se	se	k3xPyFc4
místo	místo	k7c2
náhradních	náhradní	k2eAgInPc2d1
dílů	díl	k1gInPc2
<g/>
,	,	kIx,
vytvrzených	vytvrzený	k2eAgNnPc2d1
za	za	k7c4
použití	použití	k1gNnSc4
radiace	radiace	k1gFnSc2
<g/>
,	,	kIx,
používají	používat	k5eAaImIp3nP
komerčně	komerčně	k6eAd1
vyráběné	vyráběný	k2eAgInPc1d1
náhradní	náhradní	k2eAgInPc1d1
díly	díl	k1gInPc1
a	a	k8xC
systémy	systém	k1gInPc1
„	„	k?
<g/>
tolerantní	tolerantní	k2eAgInPc1d1
vůči	vůči	k7c3
radiaci	radiace	k1gFnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
září	září	k1gNnSc2
2018	#num#	k4
provedla	provést	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
SpaceX	SpaceX	k1gFnSc2
více	hodně	k6eAd2
než	než	k8xS
60	#num#	k4
startů	start	k1gInPc2
Falconu	Falcon	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
<g/>
,	,	kIx,
kromě	kromě	k7c2
jednoho	jeden	k4xCgMnSc2
<g/>
,	,	kIx,
úspěšně	úspěšně	k6eAd1
dodaly	dodat	k5eAaPmAgFnP
své	své	k1gNnSc4
primární	primární	k2eAgNnSc4d1
užitečné	užitečný	k2eAgNnSc4d1
zatížení	zatížení	k1gNnSc4
na	na	k7c4
zamýšlenou	zamýšlený	k2eAgFnSc4d1
oběžnou	oběžný	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
plánuje	plánovat	k5eAaImIp3nS
jej	on	k3xPp3gMnSc4
použít	použít	k5eAaPmF
k	k	k7c3
přepravě	přeprava	k1gFnSc3
astronautů	astronaut	k1gMnPc2
na	na	k7c4
Mezinárodní	mezinárodní	k2eAgFnSc4d1
kosmickou	kosmický	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
Windows	Windows	kA
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
navíc	navíc	k6eAd1
používán	používat	k5eAaImNgInS
jako	jako	k8xC,k8xS
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
na	na	k7c6
kritických	kritický	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
nejsou	být	k5eNaImIp3nP
určeny	určit	k5eAaPmNgInP
pro	pro	k7c4
poslání	poslání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Notebooky	notebook	k1gInPc1
používané	používaný	k2eAgInPc1d1
na	na	k7c6
palubě	paluba	k1gFnSc6
kosmické	kosmický	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
byly	být	k5eAaImAgFnP
ale	ale	k8xC
nahrazeny	nahradit	k5eAaPmNgFnP
Linuxem	linux	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
humanoidní	humanoidní	k2eAgInSc4d1
robot	robot	k1gInSc4
s	s	k7c7
operačním	operační	k2eAgInSc7d1
systémem	systém	k1gInSc7
Linux	linux	k1gInSc1
také	také	k6eAd1
prochází	procházet	k5eAaImIp3nS
testováním	testování	k1gNnSc7
v	v	k7c6
letu	let	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Laboratoř	laboratoř	k1gFnSc4
Jet	jet	k2eAgInSc4d1
Propulsion	Propulsion	k1gInSc4
používala	používat	k5eAaImAgFnS
Linux	linux	k1gInSc4
již	již	k6eAd1
řadu	řada	k1gFnSc4
let	léto	k1gNnPc2
„	„	k?
<g/>
na	na	k7c4
pomoc	pomoc	k1gFnSc4
při	při	k7c6
projektech	projekt	k1gInPc6
spojených	spojený	k2eAgInPc6d1
s	s	k7c7
výstavbou	výstavba	k1gFnSc7
bezpilotního	bezpilotní	k2eAgInSc2d1
vesmírného	vesmírný	k2eAgInSc2d1
letu	let	k1gInSc2
a	a	k8xC
průzkumu	průzkum	k1gInSc2
hlubokého	hluboký	k2eAgInSc2d1
vesmíru	vesmír	k1gInSc2
<g/>
“	“	k?
<g/>
;	;	kIx,
NASA	NASA	kA
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
Linux	linux	k1gInSc4
v	v	k7c6
robotice	robotika	k1gFnSc6
v	v	k7c6
roveru	rover	k1gMnSc6
Mars	Mars	k1gInSc4
a	a	k8xC
Ubuntu	Ubunta	k1gFnSc4
Linux	Linux	kA
pro	pro	k7c4
„	„	k?
<g/>
ukládání	ukládání	k1gNnSc4
dat	datum	k1gNnPc2
ze	z	k7c2
satelitů	satelit	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Distribuce	distribuce	k1gFnPc1
Linuxu	linux	k1gInSc2
byly	být	k5eAaImAgFnP
vytvořeny	vytvořit	k5eAaPmNgFnP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
poskytovaly	poskytovat	k5eAaImAgFnP
studentům	student	k1gMnPc3
praktické	praktický	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
s	s	k7c7
kódováním	kódování	k1gNnSc7
a	a	k8xC
zdrojovým	zdrojový	k2eAgInSc7d1
kódem	kód	k1gInSc7
na	na	k7c6
zařízeních	zařízení	k1gNnPc6
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Raspberry	Raspberr	k1gInPc4
Pi	pi	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
vytvoření	vytvoření	k1gNnSc2
praktického	praktický	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
je	být	k5eAaImIp3nS
záměrem	záměr	k1gInSc7
ukázat	ukázat	k5eAaPmF
studentům	student	k1gMnPc3
„	„	k?
<g/>
jak	jak	k6eAd1
věci	věc	k1gFnPc1
pracují	pracovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Projekty	projekt	k1gInPc1
Ubuntu	Ubunt	k1gInSc2
Edubuntu	Edubunt	k1gInSc2
a	a	k8xC
Projekt	projekt	k1gInSc1
Linux	Linux	kA
Schools	Schools	k1gInSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Debian	Debian	k1gInSc1
derivát	derivát	k1gInSc1
Skolelinux	Skolelinux	k1gInSc4
<g/>
,	,	kIx,
poskytují	poskytovat	k5eAaImIp3nP
vzdělávací	vzdělávací	k2eAgInPc4d1
softwarové	softwarový	k2eAgInPc4d1
balíčky	balíček	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahují	obsahovat	k5eAaImIp3nP
také	také	k9
nástroje	nástroj	k1gInPc1
pro	pro	k7c4
správu	správa	k1gFnSc4
a	a	k8xC
budování	budování	k1gNnSc4
školních	školní	k2eAgFnPc2d1
počítačových	počítačový	k2eAgFnPc2d1
laboratoří	laboratoř	k1gFnPc2
a	a	k8xC
počítačových	počítačový	k2eAgFnPc2d1
učeben	učebna	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Linux	linux	k1gInSc1
Terminal	Terminal	k1gFnPc2
Server	server	k1gInSc1
Project	Project	k1gMnSc1
(	(	kIx(
<g/>
LTSP	LTSP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Instant	Instant	k1gInSc1
WebKiosk	WebKiosk	k1gInSc1
a	a	k8xC
Webconverger	Webconverger	k1gInSc4
jsou	být	k5eAaImIp3nP
distribuce	distribuce	k1gFnSc1
Linuxu	linux	k1gInSc2
založené	založený	k2eAgInPc1d1
na	na	k7c6
prohlížeči	prohlížeč	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
často	často	k6eAd1
používají	používat	k5eAaImIp3nP
ve	v	k7c6
webových	webový	k2eAgInPc6d1
kioscích	kiosek	k1gInPc6
a	a	k8xC
digitálním	digitální	k2eAgNnSc6d1
značení	značení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thinstation	Thinstation	k1gInSc1
je	být	k5eAaImIp3nS
minimalistická	minimalistický	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
tenké	tenký	k2eAgMnPc4d1
klienty	klient	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rocks	Rocks	k1gInSc1
Cluster	cluster	k1gInSc4
Distribution	Distribution	k1gInSc1
je	být	k5eAaImIp3nS
přizpůsoben	přizpůsobit	k5eAaPmNgInS
pro	pro	k7c4
vysoce	vysoce	k6eAd1
výkonné	výkonný	k2eAgInPc4d1
výpočetní	výpočetní	k2eAgInPc4d1
clustery	cluster	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
distribuce	distribuce	k1gFnSc1
Linuxu	linux	k1gInSc2
s	s	k7c7
obecným	obecný	k2eAgInSc7d1
účelem	účel	k1gInSc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
zaměřeny	zaměřit	k5eAaPmNgInP
na	na	k7c4
určité	určitý	k2eAgNnSc4d1
publikum	publikum	k1gNnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
uživatelé	uživatel	k1gMnPc1
určitého	určitý	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
nebo	nebo	k8xC
zeměpisné	zeměpisný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
takové	takový	k3xDgInPc4
příklady	příklad	k1gInPc4
patří	patřit	k5eAaImIp3nS
Ubuntu	Ubunta	k1gFnSc4
Kylin	Kylina	k1gFnPc2
pro	pro	k7c4
uživatele	uživatel	k1gMnPc4
čínského	čínský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
BlankOn	BlankOn	k1gNnSc1
zaměřené	zaměřený	k2eAgNnSc1d1
na	na	k7c4
indonéské	indonéský	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesionální	profesionální	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
zahrnují	zahrnovat	k5eAaImIp3nP
Ubuntu	Ubunt	k1gMnSc3
Studio	studio	k1gNnSc4
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
médií	médium	k1gNnPc2
a	a	k8xC
DNALinux	DNALinux	k1gInSc1
pro	pro	k7c4
bioinformatiku	bioinformatika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
sem	sem	k6eAd1
patří	patřit	k5eAaImIp3nS
muslimsky	muslimsky	k6eAd1
orientované	orientovaný	k2eAgFnPc4d1
distribuce	distribuce	k1gFnPc4
jménem	jméno	k1gNnSc7
Sabily	Sabila	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
následně	následně	k6eAd1
poskytuje	poskytovat	k5eAaImIp3nS
také	také	k9
některé	některý	k3yIgInPc4
islámské	islámský	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
organizace	organizace	k1gFnSc2
používají	používat	k5eAaImIp3nP
interně	interně	k6eAd1
lehce	lehko	k6eAd1
specializované	specializovaný	k2eAgFnSc2d1
linuxové	linuxový	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
GendBuntu	GendBunt	k1gInSc2
používané	používaný	k2eAgNnSc1d1
francouzským	francouzský	k2eAgNnSc7d1
národním	národní	k2eAgNnSc7d1
četnictvem	četnictvo	k1gNnSc7
<g/>
,	,	kIx,
Goobuntu	Goobunt	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
interně	interně	k6eAd1
používá	používat	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
Google	Google	k1gFnSc1
a	a	k8xC
Astra	astra	k1gFnSc1
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
vyvinutý	vyvinutý	k2eAgInSc1d1
speciálně	speciálně	k6eAd1
pro	pro	k7c4
ruskou	ruský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
Linux	linux	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
kontroverze	kontroverze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
Linux	Linux	kA
původně	původně	k6eAd1
označoval	označovat	k5eAaImAgInS
jen	jen	k9
samotné	samotný	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
velmi	velmi	k6eAd1
brzy	brzy	k6eAd1
byl	být	k5eAaImAgInS
název	název	k1gInSc1
zevšeobecněn	zevšeobecnit	k5eAaPmNgInS
a	a	k8xC
vztažen	vztáhnout	k5eAaPmNgInS
na	na	k7c4
celý	celý	k2eAgInSc4d1
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
z	z	k7c2
jádra	jádro	k1gNnSc2
Linux	Linux	kA
a	a	k8xC
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
GNU	gnu	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
ale	ale	k8xC
neplatí	platit	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Linux	linux	k1gInSc1
a	a	k8xC
GNU	gnu	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
takřka	takřka	k6eAd1
celý	celý	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
do	do	k7c2
systému	systém	k1gInSc2
byla	být	k5eAaImAgFnS
postupně	postupně	k6eAd1
zahrnuta	zahrnut	k2eAgFnSc1d1
řada	řada	k1gFnSc1
dalších	další	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1
</s>
<s>
Deklarace	deklarace	k1gFnSc1
výslovnosti	výslovnost	k1gFnSc2
Linusem	Linus	k1gMnSc7
Torvaldsem	Torvalds	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
angličtině	angličtina	k1gFnSc6
vznikaly	vznikat	k5eAaImAgInP
dříve	dříve	k6eAd2
spory	spor	k1gInPc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
správně	správně	k6eAd1
slovo	slovo	k1gNnSc1
Linux	Linux	kA
vyslovovat	vyslovovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžně	běžně	k6eAd1
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc1
slovo	slovo	k1gNnSc1
vyslovováno	vyslovován	k2eAgNnSc1d1
[	[	kIx(
<g/>
ˈ	ˈ	k5eAaPmRp2nS
<g/>
]	]	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
existují	existovat	k5eAaImIp3nP
různé	různý	k2eAgFnPc4d1
varianty	varianta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
sám	sám	k3xTgMnSc1
Linus	Linus	k1gMnSc1
Torvalds	Torvaldsa	k1gFnPc2
<g/>
,	,	kIx,
původce	původce	k1gMnSc2
jména	jméno	k1gNnSc2
Linux	Linux	kA
<g/>
,	,	kIx,
slovo	slovo	k1gNnSc1
vyslovuje	vyslovovat	k5eAaImIp3nS
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
v	v	k7c6
češtině	čeština	k1gFnSc6
–	–	k?
[	[	kIx(
<g/>
ˈ	ˈ	k1gInSc1
<g/>
]	]	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
podle	podle	k7c2
Linuse	Linuse	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Volně	volně	k6eAd1
šiřitelný	šiřitelný	k2eAgInSc1d1
software	software	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
chráněn	chránit	k5eAaImNgInS
licencemi	licence	k1gFnPc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
takovým	takový	k3xDgInSc7
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
uživatele	uživatel	k1gMnSc4
velmi	velmi	k6eAd1
málo	málo	k6eAd1
omezuje	omezovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podrobnosti	podrobnost	k1gFnPc1
viz	vidět	k5eAaImRp2nS
tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
<g/>
↑	↑	k?
Distribuce	distribuce	k1gFnSc1
Gentoo	Gentoo	k1gMnSc1
Linux	Linux	kA
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
uživateli	uživatel	k1gMnSc3
pomohla	pomoct	k5eAaPmAgNnP
si	se	k3xPyFc3
celý	celý	k2eAgInSc4d1
systém	systém	k1gInSc4
zkompilovat	zkompilovat	k5eAaPmF
přímo	přímo	k6eAd1
ze	z	k7c2
zdrojových	zdrojový	k2eAgInPc2d1
kódů	kód	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Linux	Linux	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gMnSc1
Open	Open	k1gMnSc1
Source	Source	k1gMnSc1
Initiative	Initiativ	k1gInSc5
|	|	kIx~
Open	Open	k1gMnSc1
Source	Source	k1gMnSc1
Initiative	Initiativ	k1gInSc5
–	–	k?
definice	definice	k1gFnSc2
open	open	k1gMnSc1
source	source	k1gMnSc1
software	software	k1gInSc1
a	a	k8xC
seznam	seznam	k1gInSc1
licencí	licence	k1gFnPc2
vyhovujících	vyhovující	k2eAgInPc2d1
definici	definice	k1gFnSc4
<g/>
↑	↑	k?
Skupiny	skupina	k1gFnSc2
Google	Google	k1gFnSc2
<g/>
↑	↑	k?
–	–	k?
sever	sever	k1gInSc4
pro	pro	k7c4
zveřejňování	zveřejňování	k1gNnSc4
zdrojových	zdrojový	k2eAgInPc2d1
kódů	kód	k1gInPc2
jádra	jádro	k1gNnSc2
Linuxu	linux	k1gInSc2
<g/>
↑	↑	k?
–	–	k?
How	How	k1gMnSc1
patches	patches	k1gMnSc1
get	get	k?
into	into	k1gMnSc1
the	the	k?
mainline	mainlin	k1gInSc5
<g/>
↑	↑	k?
–	–	k?
2.6	2.6	k4
<g/>
.24	.24	k4
–	–	k?
some	somat	k5eAaPmIp3nS
statistics	statistics	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
–	–	k?
Lars	Lars	k1gInSc1
Wirzenius	Wirzenius	k1gMnSc1
<g/>
:	:	kIx,
Linux	Linux	kA
Anecdotes	Anecdotes	k1gMnSc1
<g/>
↑	↑	k?
Sam	Sam	k1gMnSc1
Williams	Williams	k1gInSc1
<g/>
,	,	kIx,
Free	Free	k1gInSc1
as	as	k1gInSc1
in	in	k?
Freedom	Freedom	k1gInSc1
<g/>
:	:	kIx,
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Crusade	Crusad	k1gInSc5
for	forum	k1gNnPc2
Free	Free	k1gFnPc7
Software	software	k1gInSc1
<g/>
,	,	kIx,
chapter	chapter	k1gInSc1
10	#num#	k4
(	(	kIx(
<g/>
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Reilly	Reill	k1gMnPc4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stephen	Stephen	k2eAgInSc4d1
Benson	Benson	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linux	linux	k1gInSc1
<g/>
/	/	kIx~
<g/>
GNU	gnu	k1gNnSc1
in	in	k?
EE	EE	kA
Times	Times	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
May	May	k1gMnSc1
1994	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
178@scribendum.win-uk.net	178@scribendum.win-uk.net	k5eAaBmF,k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
↑	↑	k?
GNU	gnu	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Bulletin	bulletin	k1gInSc1
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
1	#num#	k4
no	no	k9
<g/>
.	.	kIx.
18	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LWN	LWN	kA
Distributions	Distributions	k1gInSc1
List	list	k1gInSc1
<g/>
↑	↑	k?
DistroWatch	DistroWatch	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
Put	puta	k1gFnPc2
the	the	k?
fun	fun	k?
back	back	k1gInSc1
into	into	k1gMnSc1
computing	computing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Use	usus	k1gInSc5
Linux	Linux	kA
<g/>
,	,	kIx,
BSD	BSD	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
–	–	k?
databáze	databáze	k1gFnSc1
snímků	snímek	k1gInPc2
obrazovek	obrazovka	k1gFnPc2
převážně	převážně	k6eAd1
linuxových	linuxový	k2eAgInPc2d1
desktopů	desktop	k1gInPc2
a	a	k8xC
většiny	většina	k1gFnSc2
svobodných	svobodný	k2eAgFnPc2d1
a	a	k8xC
OpenSource	OpenSourka	k1gFnSc3
aplikací	aplikace	k1gFnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Například	například	k6eAd1
vše	všechen	k3xTgNnSc4
okolo	okolo	k7c2
licencí	licence	k1gFnPc2
pro	pro	k7c4
distribuci	distribuce	k1gFnSc4
Fedora	Fedor	k1gMnSc2
je	být	k5eAaImIp3nS
na	na	k7c6
adrese	adresa	k1gFnSc6
Licensing	Licensing	k1gInSc1
<g/>
:	:	kIx,
<g/>
Main	Main	k1gMnSc1
–	–	k?
FedoraProject	FedoraProject	k1gMnSc1
<g/>
↑	↑	k?
KRČMÁŘ	Krčmář	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
všech	všecek	k3xTgInPc6
500	#num#	k4
superpočítačích	superpočítač	k1gInPc6
z	z	k7c2
žebříčku	žebříček	k1gInSc2
Top	topit	k5eAaImRp2nS
500	#num#	k4
už	už	k6eAd1
běží	běžet	k5eAaImIp3nS
Linux	linux	k1gInSc1
-	-	kIx~
Root	Root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Root	Root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Market	market	k1gInSc1
share	shar	k1gInSc5
for	forum	k1gNnPc2
browsers	browsersa	k1gFnPc2
<g/>
,	,	kIx,
operating	operating	k1gInSc1
systems	systems	k1gInSc1
and	and	k?
search	search	k1gInSc1
engines	engines	k1gInSc1
|	|	kIx~
News	News	k1gInSc1
<g/>
↑	↑	k?
DSL	DSL	kA
<g/>
.	.	kIx.
<g/>
sk	sk	k?
-	-	kIx~
Linux	Linux	kA
na	na	k7c4
PC	PC	kA
dosiahol	dosiahol	k1gInSc4
3	#num#	k4
<g/>
%	%	kIx~
podiel	podiet	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DSL	DSL	kA
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
LINUS	LINUS	kA
<g/>
,	,	kIx,
Torvalds	Torvalds	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Re	re	k9
<g/>
:	:	kIx,
How	How	k1gMnSc1
to	ten	k3xDgNnSc4
pronounce	pronounko	k6eAd1
“	“	k?
<g/>
Linux	linux	k1gInSc1
<g/>
”	”	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
Groups	Groupsa	k1gFnPc2
<g/>
:	:	kIx,
newsgroups	wsgroupsit	k5eNaPmRp2nS
comp	comp	k1gInSc4
<g/>
.	.	kIx.
<g/>
os	osa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
linux	linux	k1gInSc1
<g/>
,	,	kIx,
1992-04-23	1992-04-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
–	–	k?
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
zveřejnil	zveřejnit	k5eAaPmAgInS
audio	audio	k2eAgFnSc4d1
nahrávky	nahrávka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
indikují	indikovat	k5eAaBmIp3nP
jeho	jeho	k3xOp3gFnSc4
vlastní	vlastní	k2eAgFnSc4d1
výslovnost	výslovnost	k1gFnSc4
<g/>
:	:	kIx,
anglicky	anglicky	k6eAd1
[	[	kIx(
<g/>
ˈ	ˈ	k1gInSc1
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
─	─	k?
How	How	k1gFnSc1
to	ten	k3xDgNnSc4
pronounce	pronounko	k6eAd1
Linux	linux	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
a	a	k8xC
švédsky	švédsky	k6eAd1
[	[	kIx(
<g/>
ˈ	ˈ	k1gInSc1
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
─	─	k?
Linus	Linus	k1gInSc1
pronouncing	pronouncing	k1gInSc1
Linux	Linux	kA
in	in	k?
English	English	k1gMnSc1
and	and	k?
Swedish	Swedish	k1gMnSc1
(	(	kIx(
<g/>
Linus	Linus	k1gMnSc1
vyslovuje	vyslovovat	k5eAaImIp3nS
Linux	linux	k1gInSc4
v	v	k7c6
angličtině	angličtina	k1gFnSc6
a	a	k8xC
švédštině	švédština	k1gFnSc6
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SCHRODER	SCHRODER	kA
<g/>
,	,	kIx,
Carla	Carlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linux	linux	k1gInSc1
–	–	k?
Kuchařka	kuchařka	k1gFnSc1
administrátora	administrátor	k1gMnSc2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
2407	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
608	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bednář	Bednář	k1gMnSc1
Vojtěch	Vojtěch	k1gMnSc1
<g/>
:	:	kIx,
Linux	Linux	kA
na	na	k7c6
firemním	firemní	k2eAgInSc6d1
PC	PC	kA
–	–	k?
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
rizika	riziko	k1gNnPc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
BEN	Ben	k1gInSc1
-	-	kIx~
technická	technický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-7300-225-1	978-80-7300-225-1	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
</s>
<s>
Projekt	projekt	k1gInSc1
GNU	gnu	k1gNnSc2
</s>
<s>
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
LAMP	lampa	k1gFnPc2
</s>
<s>
Unix	Unix	k1gInSc1
</s>
<s>
Seznam	seznam	k1gInSc1
distribucí	distribuce	k1gFnPc2
Linuxu	linux	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Linux	Linux	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Linux	Linux	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Linux	Linux	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kniha	kniha	k1gFnSc1
Linux	Linux	kA
ve	v	k7c6
Wikiknihách	Wikiknih	k1gInPc6
</s>
<s>
Výukový	výukový	k2eAgInSc1d1
kurs	kurs	k1gInSc1
Linux	Linux	kA
ve	v	k7c6
Wikiverzitě	Wikiverzita	k1gFnSc6
</s>
<s>
Téma	téma	k1gNnSc1
Linux	Linux	kA
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Linux	Linux	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Proč	proč	k6eAd1
používat	používat	k5eAaImF
GNU	gnu	k1gNnSc4
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Filosofie	filosofie	k1gFnSc1
projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
„	„	k?
<g/>
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
<g/>
“	“	k?
–	–	k?
Co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
je	být	k5eAaImIp3nS
ve	v	k7c6
jménu	jméno	k1gNnSc6
<g/>
?	?	kIx.
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Přehled	přehled	k1gInSc1
distribucí	distribuce	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Linux	Linux	kA
Dokumentační	dokumentační	k2eAgInSc1d1
Projekt	projekt	k1gInSc1
–	–	k?
16MB	16MB	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Linux	linux	k1gInSc1
Documentation	Documentation	k1gInSc1
Project	Project	k2eAgInSc1d1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
ÚSL	ÚSL	kA
Úvod	úvod	k1gInSc1
do	do	k7c2
Systému	systém	k1gInSc2
Linux	Linux	kA
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Učebnice	učebnice	k1gFnSc1
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linuxu	linux	k1gInSc2
–	–	k?
česky	česky	k6eAd1
</s>
<s>
Dokumenty	dokument	k1gInPc1
o	o	k7c6
Linuxu	linux	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Revolution	Revolution	k1gInSc1
OS	OS	kA
<g/>
,	,	kIx,
režie	režie	k1gFnSc2
J.	J.	kA
T.	T.	kA
S.	S.	kA
Moore	Moor	k1gInSc5
<g/>
,	,	kIx,
85	#num#	k4
minut	minuta	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Operační	operační	k2eAgInPc1d1
systémy	systém	k1gInPc1
BSD	BSD	kA
</s>
<s>
FreeBSD	FreeBSD	k?
•	•	k?
DragonFly	DragonFla	k1gFnSc2
BSD	BSD	kA
•	•	k?
NetBSD	NetBSD	k1gMnSc1
•	•	k?
OpenBSD	OpenBSD	k1gMnSc1
—	—	k?
Minix	Minix	k1gInSc1
3	#num#	k4
Linux	Linux	kA
(	(	kIx(
<g/>
distribuce	distribuce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
Deb	Deb	k1gMnPc5
<g/>
)	)	kIx)
</s>
<s>
Debian	Debian	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Knoppix	Knoppix	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ubuntu	Ubunt	k1gInSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
RPM	RPM	kA
<g/>
)	)	kIx)
</s>
<s>
Red	Red	k?
Hat	hat	k0
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fedora	Fedora	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mandriva	Mandriva	k1gFnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Portage	Portage	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Gentoo	Gentoo	k1gNnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
AUR	aura	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Arch	arch	k1gInSc1
Linux	Linux	kA
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Manjaro	Manjara	k1gFnSc5
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
další	další	k2eAgFnSc2d1
</s>
<s>
Slackware	Slackwar	k1gMnSc5
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slax	Slax	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SUSE	SUSE	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
další	další	k2eAgInSc4d1
<g/>
…	…	k?
</s>
<s>
GNU	gnu	k1gMnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
Hurd	hurda	k1gFnPc2
•	•	k?
Linux-libre	Linux-libr	k1gInSc5
—	—	k?
seL	sít	k5eAaImAgInS
<g/>
4	#num#	k4
(	(	kIx(
<g/>
Mac	Mac	kA
<g/>
)	)	kIx)
OS	OS	kA
X	X	kA
•	•	k?
macOS	macOS	k?
</s>
<s>
10.6	10.6	k4
(	(	kIx(
<g/>
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
10.7	10.7	k4
(	(	kIx(
<g/>
Lion	Lion	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
10.8	10.8	k4
(	(	kIx(
<g/>
Mountain	Mountain	k1gMnSc1
Lion	Lion	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
10.9	10.9	k4
(	(	kIx(
<g/>
Mavericks	Mavericksa	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
OS	OS	kA
X	X	kA
10.10	10.10	k4
Yosemite	Yosemit	k1gInSc5
•	•	k?
OS	OS	kA
X	X	kA
10.11	10.11	k4
El	Ela	k1gFnPc2
Capitan	Capitan	k1gInSc4
•	•	k?
macOS	macOS	k?
10.12	10.12	k4
Sierra	Sierra	k1gFnSc1
•	•	k?
macOS	macOS	k?
10.13	10.13	k4
High	Higha	k1gFnPc2
Sierra	Sierra	k1gFnSc1
•	•	k?
macOS	macOS	k?
10.14	10.14	k4
Mojave	Mojav	k1gInSc5
•	•	k?
macOS	macOS	k?
10.15	10.15	k4
Catalina	Catalina	k1gFnSc1
•	•	k?
macOS	macOS	k?
11	#num#	k4
Big	Big	k1gFnPc7
Sur	Sur	k1gMnSc2
DOS	DOS	kA
</s>
<s>
MS-DOS	MS-DOS	k?
•	•	k?
DR-DOS	DR-DOS	k1gMnSc1
•	•	k?
FreeDOS	FreeDOS	k1gMnSc1
•	•	k?
PTS-DOS	PTS-DOS	k1gMnSc1
Windows	Windows	kA
</s>
<s>
Windows	Windows	kA
pro	pro	k7c4
MS-DOS	MS-DOS	k1gFnSc4
</s>
<s>
Windows	Windows	kA
1.0	1.0	k4
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
2.0	2.0	k4
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
3.0	3.0	k4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
3.1	3.1	k4
<g/>
x	x	k?
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
95	#num#	k4
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
98	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
ME	ME	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
Windows	Windows	kA
NT	NT	kA
</s>
<s>
Windows	Windows	kA
NT	NT	kA
3.1	3.1	k4
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
NT	NT	kA
3.5	3.5	k4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
NT	NT	kA
3.51	3.51	k4
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
NT	NT	kA
4.0	4.0	k4
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
2000	#num#	k4
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
XP	XP	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2003	#num#	k4
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Vista	vista	k2eAgFnSc1d1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2008	#num#	k4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
7	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2008	#num#	k4
R2	R2	k1gFnPc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
8	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2012	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
8.1	8.1	k4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2012	#num#	k4
R2	R2	k1gFnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
10	#num#	k4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Windows	Windows	kA
CE	CE	kA
</s>
<s>
Windows	Windows	kA
CE	CE	kA
→	→	k?
Windows	Windows	kA
Mobile	mobile	k1gNnSc1
→	→	k?
Windows	Windows	kA
Phone	Phon	k1gInSc5
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
10	#num#	k4
Mobile	mobile	k1gNnPc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Vývoj	vývoj	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
</s>
<s>
Cairo	Cairo	k6eAd1
•	•	k?
Nashville	Nashville	k1gInSc1
•	•	k?
Neptune	Neptun	k1gInSc5
•	•	k?
Odyssey	Odyssea	k1gFnPc5
</s>
<s>
Mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
a	a	k8xC
PDA	PDA	kA
</s>
<s>
Android	android	k1gInSc1
•	•	k?
Bada	Bada	k1gFnSc1
•	•	k?
BlackBerry	BlackBerra	k1gFnSc2
OS	OS	kA
•	•	k?
Firefox	Firefox	k1gInSc1
OS	OS	kA
•	•	k?
iOS	iOS	k?
•	•	k?
Maemo	Maema	k1gFnSc5
•	•	k?
Palm	Palmo	k1gNnPc2
OS	OS	kA
•	•	k?
Symbian	Symbian	k1gMnSc1
OS	OS	kA
•	•	k?
Tizen	Tizen	k1gInSc1
•	•	k?
Ubuntu	Ubunt	k1gInSc2
Touch	Touch	k1gMnSc1
•	•	k?
webOS	webOS	k?
•	•	k?
Windows	Windows	kA
Phone	Phon	k1gInSc5
další	další	k2eAgNnPc1d1
</s>
<s>
QNX	QNX	kA
•	•	k?
Solaris	Solaris	k1gInSc1
•	•	k?
BeOS	BeOS	k1gFnSc2
•	•	k?
OpenVMS	OpenVMS	k1gFnSc2
•	•	k?
Mac	Mac	kA
OS	OS	kA
•	•	k?
NeXTSTEP	NeXTSTEP	k1gMnSc1
•	•	k?
Syllable	Syllable	k1gMnSc1
•	•	k?
ReactOS	ReactOS	k1gMnSc1
•	•	k?
Haiku	Haika	k1gFnSc4
•	•	k?
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
•	•	k?
AmigaOS	AmigaOS	k1gMnSc7
historické	historický	k2eAgFnSc2d1
</s>
<s>
Mac	Mac	kA
OS	OS	kA
(	(	kIx(
<g/>
Classic	Classic	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Multics	Multics	k1gInSc1
•	•	k?
OS	OS	kA
<g/>
/	/	kIx~
<g/>
2	#num#	k4
•	•	k?
Plan	plan	k1gInSc1
9	#num#	k4
from	from	k6eAd1
Bell	bell	k1gInSc1
Labs	Labs	k1gInSc1
•	•	k?
UNIX	UNIX	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
|	|	kIx~
Linux	linux	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
117043	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4337730-0	4337730-0	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
12506	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
94087892	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
185985901	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
94087892	#num#	k4
</s>
