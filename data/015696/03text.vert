<s>
Stavební	stavební	k2eAgNnSc1d1
povolení	povolení	k1gNnSc1
</s>
<s>
Stavební	stavební	k2eAgNnSc1d1
povolení	povolení	k1gNnSc1
při	při	k7c6
stavbě	stavba	k1gFnSc6
pražského	pražský	k2eAgNnSc2d1
metra	metro	k1gNnSc2
na	na	k7c6
Vypichu	vypich	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
</s>
<s>
Stavební	stavební	k2eAgNnSc1d1
povolení	povolení	k1gNnSc1
je	být	k5eAaImIp3nS
licence	licence	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
ke	k	k7c3
stavbě	stavba	k1gFnSc3
či	či	k8xC
stavebním	stavební	k2eAgFnPc3d1
úpravám	úprava	k1gFnPc3
<g/>
,	,	kIx,
pravidla	pravidlo	k1gNnPc4
jejího	její	k3xOp3gNnSc2
získání	získání	k1gNnSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
podle	podle	k7c2
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
toto	tento	k3xDgNnSc4
povolení	povolení	k1gNnSc4
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
místní	místní	k2eAgInSc1d1
Stavební	stavební	k2eAgInSc1d1
úřad	úřad	k1gInSc1
na	na	k7c4
požádání	požádání	k1gNnSc4
a	a	k8xC
při	při	k7c6
splnění	splnění	k1gNnSc6
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavební	stavební	k2eAgNnSc1d1
povolení	povolení	k1gNnPc1
vyžadují	vyžadovat	k5eAaImIp3nP
stavby	stavba	k1gFnPc1
se	s	k7c7
zastavěnou	zastavěný	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
větší	veliký	k2eAgFnSc7d2
než	než	k8xS
150	#num#	k4
m²	m²	k?
<g/>
,	,	kIx,
nebo	nebo	k8xC
když	když	k8xS
zasahují	zasahovat	k5eAaImIp3nP
do	do	k7c2
nosných	nosný	k2eAgFnPc2d1
konstrukcí	konstrukce	k1gFnPc2
domu	dům	k1gInSc2
či	či	k8xC
jiného	jiný	k2eAgInSc2d1
objektu	objekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
menších	malý	k2eAgInPc6d2
zásazích	zásah	k1gInPc6
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
činnost	činnost	k1gFnSc4
nahlásit	nahlásit	k5eAaPmF
<g/>
,	,	kIx,
drobné	drobný	k2eAgFnPc1d1
úpravy	úprava	k1gFnPc1
staveb	stavba	k1gFnPc2
lze	lze	k6eAd1
provádět	provádět	k5eAaImF
bez	bez	k7c2
povolení	povolení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konkrétněji	konkrétně	k6eAd2
o	o	k7c6
tom	ten	k3xDgNnSc6
na	na	k7c6
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
stavbu	stavba	k1gFnSc4
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
stavební	stavební	k2eAgNnSc4d1
povolení	povolení	k1gNnSc4
pojednává	pojednávat	k5eAaImIp3nS
Stavební	stavební	k2eAgInSc1d1
zákon	zákon	k1gInSc1
ve	v	k7c6
č.	č.	k?
183	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ve	v	k7c6
znění	znění	k1gNnSc6
2013	#num#	k4
v	v	k7c6
§	§	k?
103	#num#	k4
a	a	k8xC
dalších	další	k2eAgFnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
podávání	podávání	k1gNnSc6
žádosti	žádost	k1gFnSc2
o	o	k7c4
stavební	stavební	k2eAgNnSc4d1
povolení	povolení	k1gNnSc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
vyplnit	vyplnit	k5eAaPmF
náležitý	náležitý	k2eAgInSc4d1
formulář	formulář	k1gInSc4
žádosti	žádost	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
dostupný	dostupný	k2eAgInSc1d1
na	na	k7c6
úřadě	úřad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokumentaci	dokumentace	k1gFnSc6
pro	pro	k7c4
stavební	stavební	k2eAgNnSc4d1
povolení	povolení	k1gNnSc4
zpracovává	zpracovávat	k5eAaImIp3nS
fyzická	fyzický	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
stavebního	stavební	k2eAgNnSc2d1
povolení	povolení	k1gNnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
trvá	trvat	k5eAaImIp3nS
30	#num#	k4
až	až	k9
60	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
za	za	k7c4
něj	on	k3xPp3gMnSc4
platí	platit	k5eAaImIp3nS
poplatek	poplatek	k1gInSc1
300	#num#	k4
až	až	k9
3000	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
odmítnutí	odmítnutí	k1gNnSc6
žádosti	žádost	k1gFnSc2
má	mít	k5eAaImIp3nS
dotyčný	dotyčný	k2eAgMnSc1d1
právo	právo	k1gNnSc4
se	se	k3xPyFc4
odvolat	odvolat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
přijmutí	přijmutí	k1gNnSc6
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
umístit	umístit	k5eAaPmF
stvrzení	stvrzení	k1gNnSc4
o	o	k7c4
povolení	povolení	k1gNnSc4
k	k	k7c3
dané	daný	k2eAgFnSc3d1
stavbě	stavba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Stavební	stavební	k2eAgNnSc1d1
povolení	povolení	k1gNnSc1
k	k	k7c3
vodním	vodní	k2eAgNnPc3d1
dílům	dílo	k1gNnPc3
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Stavební	stavební	k2eAgNnSc1d1
povolení	povolení	k1gNnSc1
k	k	k7c3
vodním	vodní	k2eAgNnPc3d1
dílům	dílo	k1gNnPc3
dle	dle	k7c2
§	§	k?
15	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
254	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
)	)	kIx)
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
příslušný	příslušný	k2eAgInSc1d1
vodoprávní	vodoprávní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
jako	jako	k8xS,k8xC
speciální	speciální	k2eAgInSc1d1
stavební	stavební	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodní	vodní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
stanoví	stanovit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
provedení	provedení	k1gNnSc3
vodních	vodní	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
k	k	k7c3
jejich	jejich	k3xOp3gFnPc3
změnám	změna	k1gFnPc3
a	a	k8xC
změnám	změna	k1gFnPc3
jejich	jejich	k3xOp3gNnSc1
užívání	užívání	k1gNnSc1
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
zrušení	zrušení	k1gNnSc3
a	a	k8xC
odstranění	odstranění	k1gNnSc3
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
povolení	povolení	k1gNnSc4
vodoprávního	vodoprávní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
povolení	povolení	k1gNnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
vodního	vodní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
vázano	vázana	k1gFnSc5
na	na	k7c4
povolení	povolení	k1gNnPc4
k	k	k7c3
nakládání	nakládání	k1gNnSc3
s	s	k7c7
vodami	voda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povolení	povolení	k1gNnPc1
k	k	k7c3
provedení	provedení	k1gNnSc3
nebo	nebo	k8xC
změně	změna	k1gFnSc3
vodního	vodní	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
má	mít	k5eAaImIp3nS
sloužit	sloužit	k5eAaImF
k	k	k7c3
nakládání	nakládání	k1gNnSc3
s	s	k7c7
vodami	voda	k1gFnPc7
povolovanému	povolovaný	k2eAgInSc3d1
podle	podle	k7c2
§	§	k?
8	#num#	k4
vodního	vodní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vydáno	vydat	k5eAaPmNgNnS
jen	jen	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
povoleno	povolit	k5eAaPmNgNnS
odpovídající	odpovídající	k2eAgNnSc1d1
nakládání	nakládání	k1gNnSc1
s	s	k7c7
vodami	voda	k1gFnPc7
nebo	nebo	k8xC
se	se	k3xPyFc4
nakládání	nakládání	k1gNnSc1
s	s	k7c7
vodami	voda	k1gFnPc7
povoluje	povolovat	k5eAaImIp3nS
současně	současně	k6eAd1
s	s	k7c7
povolením	povolení	k1gNnSc7
k	k	k7c3
provedení	provedení	k1gNnSc3
nebo	nebo	k8xC
změně	změna	k1gFnSc3
vodního	vodní	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
(	(	kIx(
<g/>
§	§	k?
9	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
5	#num#	k4
vodního	vodní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
stavební	stavební	k2eAgNnSc4d1
povolení	povolení	k1gNnSc4
a	a	k8xC
kdy	kdy	k6eAd1
stačí	stačit	k5eAaBmIp3nS
ohlášení	ohlášení	k1gNnSc4
stavby	stavba	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
perlikprojekce	perlikprojekce	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Doklady	doklad	k1gInPc1
<g/>
:	:	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
k	k	k7c3
ohlášení	ohlášení	k1gNnSc3
a	a	k8xC
ke	k	k7c3
stavebnímu	stavební	k2eAgNnSc3d1
povolení	povolení	k1gNnSc3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-01-16	2007-01-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
183	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Zákon	zákon	k1gInSc1
o	o	k7c6
územním	územní	k2eAgNnSc6d1
plánování	plánování	k1gNnSc6
a	a	k8xC
stavebním	stavební	k2eAgInSc6d1
řádu	řád	k1gInSc6
(	(	kIx(
<g/>
stavební	stavební	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
254	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Zákon	zákon	k1gInSc1
o	o	k7c6
vodách	voda	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Stavební	stavební	k2eAgInSc1d1
úřad	úřad	k1gInSc1
</s>
<s>
Vodoprávní	vodoprávní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
</s>
<s>
Dokumentace	dokumentace	k1gFnSc1
pro	pro	k7c4
stavební	stavební	k2eAgNnSc4d1
povolení	povolení	k1gNnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4140705-2	4140705-2	k4
</s>
