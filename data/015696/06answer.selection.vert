<s desamb="1">
Vydání	vydání	k1gNnSc1
stavebního	stavební	k2eAgNnSc2d1
povolení	povolení	k1gNnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
trvá	trvat	k5eAaImIp3nS
30	#num#	k4
až	až	k9
60	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
za	za	k7c4
něj	on	k3xPp3gMnSc4
platí	platit	k5eAaImIp3nS
poplatek	poplatek	k1gInSc1
300	#num#	k4
až	až	k9
3000	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
odmítnutí	odmítnutí	k1gNnSc6
žádosti	žádost	k1gFnSc2
má	mít	k5eAaImIp3nS
dotyčný	dotyčný	k2eAgMnSc1d1
právo	právo	k1gNnSc4
se	se	k3xPyFc4
odvolat	odvolat	k5eAaPmF
<g/>
.	.	kIx.
</s>