<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Nežerka	Nežerka	k1gFnSc1	Nežerka
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgMnSc1d1	československý
horolezec	horolezec	k1gMnSc1	horolezec
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
pískař	pískař	k1gMnSc1	pískař
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
lezením	lezení	k1gNnSc7	lezení
začal	začít	k5eAaPmAgMnS	začít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
himálajskou	himálajský	k2eAgFnSc4d1	himálajská
expedici	expedice	k1gFnSc4	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vystoupit	vystoupit	k5eAaPmF	vystoupit
na	na	k7c4	na
4	[number]	k4	4
osmitisícovky	osmitisícovka	k1gFnSc2	osmitisícovka
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
na	na	k7c4	na
Nanga	Nang	k1gMnSc4	Nang
Parbat	Parbat	k1gMnSc4	Parbat
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
Čech	Čech	k1gMnSc1	Čech
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Rakoncajem	Rakoncaj	k1gMnSc7	Rakoncaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Annapurnu	Annapurn	k1gInSc6	Annapurn
navíc	navíc	k6eAd1	navíc
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
novou	nový	k2eAgFnSc7d1	nová
cestou	cesta	k1gFnSc7	cesta
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
stěně	stěna	k1gFnSc6	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Nežerka	Nežerka	k1gFnSc1	Nežerka
je	být	k5eAaImIp3nS	být
vystudovaný	vystudovaný	k2eAgMnSc1d1	vystudovaný
stavební	stavební	k2eAgMnSc1d1	stavební
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
syny	syn	k1gMnPc7	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výkony	výkon	k1gInPc1	výkon
a	a	k8xC	a
ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
<g/>
:	:	kIx,	:
výstupy	výstup	k1gInPc1	výstup
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
===	===	k?	===
Úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
výstupy	výstup	k1gInPc4	výstup
na	na	k7c4	na
osmitisícovky	osmitisícovka	k1gFnPc4	osmitisícovka
===	===	k?	===
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
Annapurna	Annapurno	k1gNnPc1	Annapurno
(	(	kIx(	(
<g/>
8091	[number]	k4	8091
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
–	–	k?	–
nová	nový	k2eAgFnSc1d1	nová
cesta	cesta	k1gFnSc1	cesta
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Nanga	Nanga	k1gFnSc1	Nanga
Parbat	Parbat	k1gFnSc1	Parbat
(	(	kIx(	(
<g/>
8125	[number]	k4	8125
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
–	–	k?	–
první	první	k4xOgFnSc6	první
Čech	Čechy	k1gFnPc2	Čechy
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc4	Everest
(	(	kIx(	(
<g/>
8848	[number]	k4	8848
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Gašerbrum	Gašerbrum	k1gInSc1	Gašerbrum
I	I	kA	I
(	(	kIx(	(
<g/>
8068	[number]	k4	8068
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
výstupy	výstup	k1gInPc1	výstup
===	===	k?	===
</s>
</p>
<p>
<s>
5.8	[number]	k4	5.8
<g/>
.1974	.1974	k4	.1974
Piussiho	Piussi	k1gMnSc4	Piussi
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Cima	Cima	k1gMnSc1	Cima
su	su	k?	su
Alto	Alto	k1gMnSc1	Alto
<g/>
,	,	kIx,	,
Dolomity	Dolomity	k1gInPc1	Dolomity
<g/>
,	,	kIx,	,
s	s	k7c7	s
Charouskem	Charousek	k1gInSc7	Charousek
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
Chan	Chan	k1gInSc1	Chan
Tengri	Tengr	k1gFnSc2	Tengr
(	(	kIx(	(
<g/>
7010	[number]	k4	7010
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
Chan	Chan	k1gInSc1	Chan
Tengri	Tengr	k1gFnSc2	Tengr
(	(	kIx(	(
<g/>
7010	[number]	k4	7010
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
Džengiš	Džengiš	k1gInSc1	Džengiš
Čokusu	Čokus	k1gInSc2	Čokus
(	(	kIx(	(
<g/>
7439	[number]	k4	7439
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
Fitz	Fitz	k1gMnSc1	Fitz
Roy	Roy	k1gMnSc1	Roy
(	(	kIx(	(
<g/>
3461	[number]	k4	3461
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
Mount	Mount	k1gInSc1	Mount
McKinley	McKinlea	k1gFnSc2	McKinlea
(	(	kIx(	(
<g/>
6194	[number]	k4	6194
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
http://www.gasherbrum2007.wz.cz	[url]	k1gInSc1	http://www.gasherbrum2007.wz.cz
/	/	kIx~	/
<g/>
stránky	stránka	k1gFnSc2	stránka
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
expedice	expedice	k1gFnSc2	expedice
na	na	k7c4	na
Gašerbrum	Gašerbrum	k1gInSc4	Gašerbrum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
</s>
</p>
