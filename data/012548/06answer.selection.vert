<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
(	(	kIx(	(
<g/>
neoficiálně	neoficiálně	k6eAd1	neoficiálně
také	také	k9	také
Jelení	jelení	k2eAgInSc4d1	jelení
most	most	k1gInSc4	most
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
mostem	most	k1gInSc7	most
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
historicky	historicky	k6eAd1	historicky
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhý	druhý	k4xOgInSc1	druhý
po	po	k7c6	po
zaniklém	zaniklý	k2eAgNnSc6d1	zaniklé
Juditině	Juditin	k2eAgNnSc6d1	Juditin
mostu	most	k1gInSc2	most
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
