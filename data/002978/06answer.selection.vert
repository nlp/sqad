<s>
Snovačka	snovačka	k1gFnSc1	snovačka
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
(	(	kIx(	(
<g/>
Latrodectus	Latrodectus	k1gInSc1	Latrodectus
mactans	mactans	k1gInSc1	mactans
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
černá	černý	k2eAgFnSc1d1	černá
vdova	vdova	k1gFnSc1	vdova
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
pavouk	pavouk	k1gMnSc1	pavouk
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
snovačkovitých	snovačkovitý	k2eAgFnPc2d1	snovačkovitý
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
