<s>
Snovačka	snovačka	k1gFnSc1	snovačka
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
(	(	kIx(	(
<g/>
Latrodectus	Latrodectus	k1gInSc1	Latrodectus
mactans	mactans	k1gInSc1	mactans
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
černá	černý	k2eAgFnSc1d1	černá
vdova	vdova	k1gFnSc1	vdova
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
pavouk	pavouk	k1gMnSc1	pavouk
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
snovačkovitých	snovačkovitý	k2eAgFnPc2d1	snovačkovitý
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Snovačka	snovačka	k1gFnSc1	snovačka
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
je	být	k5eAaImIp3nS	být
dlouhonohý	dlouhonohý	k2eAgInSc4d1	dlouhonohý
pavoukovec	pavoukovec	k1gInSc4	pavoukovec
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
zadečkem	zadeček	k1gInSc7	zadeček
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgNnSc4d2	menší
hlavohrudí	hlavohrudí	k1gNnSc4	hlavohrudí
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
pavouků	pavouk	k1gMnPc2	pavouk
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
končetin	končetina	k1gFnPc2	končetina
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
3	[number]	k4	3
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
s	s	k7c7	s
končetinami	končetina	k1gFnPc7	končetina
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
cm	cm	kA	cm
<g/>
)	)	kIx)	)
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
leskle	leskle	k6eAd1	leskle
černé	černý	k2eAgNnSc4d1	černé
tělo	tělo	k1gNnSc4	tělo
s	s	k7c7	s
oranžově	oranžově	k6eAd1	oranžově
až	až	k8xS	až
červeně	červeně	k6eAd1	červeně
zbarvenou	zbarvený	k2eAgFnSc7d1	zbarvená
skvrnou	skvrna	k1gFnSc7	skvrna
na	na	k7c6	na
zadečku	zadeček	k1gInSc6	zadeček
připomínající	připomínající	k2eAgInSc4d1	připomínající
tvar	tvar	k1gInSc4	tvar
přesýpacích	přesýpací	k2eAgFnPc2d1	přesýpací
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
(	(	kIx(	(
<g/>
měří	měřit	k5eAaImIp3nS	měřit
až	až	k9	až
0,9	[number]	k4	0,9
cm	cm	kA	cm
<g/>
,	,	kIx,	,
s	s	k7c7	s
končetinami	končetina	k1gFnPc7	končetina
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
delší	dlouhý	k2eAgFnPc4d2	delší
končetiny	končetina	k1gFnPc4	končetina
a	a	k8xC	a
menší	malý	k2eAgFnSc4d2	menší
hlavohruď	hlavohruď	k1gFnSc4	hlavohruď
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
hnědí	hněď	k1gFnPc2	hněď
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
samice	samice	k1gFnSc2	samice
postrádají	postrádat	k5eAaImIp3nP	postrádat
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
až	až	k9	až
červenou	červený	k2eAgFnSc4d1	červená
skvrnu	skvrna	k1gFnSc4	skvrna
na	na	k7c6	na
zadečku	zadeček	k1gInSc6	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
o	o	k7c4	o
něco	něco	k3yInSc4	něco
tlustější	tlustý	k2eAgFnSc1d2	tlustší
hlavohruď	hlavohruď	k1gFnSc1	hlavohruď
a	a	k8xC	a
kratší	krátký	k2eAgFnPc1d2	kratší
končetiny	končetina	k1gFnPc1	končetina
<g/>
.	.	kIx.	.
<g/>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
ukrývat	ukrývat	k5eAaImF	ukrývat
i	i	k9	i
v	v	k7c6	v
banánech	banán	k1gInPc6	banán
<g/>
.	.	kIx.	.
</s>
<s>
Nápadné	nápadný	k2eAgNnSc1d1	nápadné
zbarvení	zbarvení	k1gNnSc1	zbarvení
na	na	k7c6	na
zadečku	zadeček	k1gInSc6	zadeček
samic	samice	k1gFnPc2	samice
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
výstraha	výstraha	k1gFnSc1	výstraha
pro	pro	k7c4	pro
masožravé	masožravý	k2eAgMnPc4d1	masožravý
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nějaký	nějaký	k3yIgMnSc1	nějaký
větší	veliký	k2eAgMnSc1d2	veliký
živočich	živočich	k1gMnSc1	živočich
snovačku	snovačka	k1gFnSc4	snovačka
napadne	napadnout	k5eAaPmIp3nS	napadnout
a	a	k8xC	a
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
končí	končit	k5eAaImIp3nS	končit
úmrtím	úmrtí	k1gNnSc7	úmrtí
nebo	nebo	k8xC	nebo
vážnými	vážný	k2eAgFnPc7d1	vážná
zdravotními	zdravotní	k2eAgFnPc7d1	zdravotní
potížemi	potíž	k1gFnPc7	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Snovačka	snovačka	k1gFnSc1	snovačka
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
poblíž	poblíž	k7c2	poblíž
lidských	lidský	k2eAgNnPc2d1	lidské
obydlí	obydlí	k1gNnPc2	obydlí
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sklepech	sklep	k1gInPc6	sklep
<g/>
,	,	kIx,	,
skladech	sklad	k1gInPc6	sklad
<g/>
,	,	kIx,	,
parcích	park	k1gInPc6	park
nebo	nebo	k8xC	nebo
na	na	k7c6	na
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
snáze	snadno	k6eAd2	snadno
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
úkryt	úkryt	k1gInSc4	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
pavoukovci	pavoukovec	k1gMnPc1	pavoukovec
žije	žít	k5eAaImIp3nS	žít
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
samotářským	samotářský	k2eAgInSc7d1	samotářský
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
partnera	partner	k1gMnSc4	partner
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
období	období	k1gNnSc6	období
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
chytání	chytání	k1gNnSc6	chytání
potravy	potrava	k1gFnSc2	potrava
si	se	k3xPyFc3	se
staví	stavit	k5eAaPmIp3nS	stavit
trojrozměrnou	trojrozměrný	k2eAgFnSc4d1	trojrozměrná
síť	síť	k1gFnSc4	síť
s	s	k7c7	s
lepkavým	lepkavý	k2eAgInSc7d1	lepkavý
středem	střed	k1gInSc7	střed
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
chytá	chytat	k5eAaImIp3nS	chytat
nejčastěji	často	k6eAd3	často
mouchy	moucha	k1gFnPc4	moucha
<g/>
,	,	kIx,	,
kobylky	kobylka	k1gFnPc4	kobylka
<g/>
,	,	kIx,	,
sarančata	saranče	k1gNnPc4	saranče
<g/>
,	,	kIx,	,
můry	můra	k1gFnPc4	můra
nebo	nebo	k8xC	nebo
motýly	motýl	k1gMnPc4	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
však	však	k9	však
i	i	k9	i
pozemní	pozemní	k2eAgMnPc4d1	pozemní
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
mnohonožky	mnohonožka	k1gFnSc2	mnohonožka
<g/>
,	,	kIx,	,
stonožky	stonožka	k1gFnPc4	stonožka
<g/>
,	,	kIx,	,
štíry	štír	k1gMnPc4	štír
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
menších	malý	k2eAgInPc2d2	menší
pavoukovců	pavoukovec	k1gInPc2	pavoukovec
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
kořist	kořist	k1gFnSc1	kořist
zaplete	zaplést	k5eAaPmIp3nS	zaplést
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
opustí	opustit	k5eAaPmIp3nS	opustit
snovačka	snovačka	k1gFnSc1	snovačka
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
svůj	svůj	k3xOyFgInSc4	svůj
úkryt	úkryt	k1gInSc4	úkryt
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
zabalí	zabalit	k5eAaPmIp3nS	zabalit
do	do	k7c2	do
pevných	pevný	k2eAgNnPc2d1	pevné
lepkavých	lepkavý	k2eAgNnPc2d1	lepkavé
vláken	vlákno	k1gNnPc2	vlákno
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vpustí	vpustit	k5eAaPmIp3nS	vpustit
jedním	jeden	k4xCgNnSc7	jeden
kousnutím	kousnutí	k1gNnSc7	kousnutí
vysoce	vysoce	k6eAd1	vysoce
účinný	účinný	k2eAgInSc1d1	účinný
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začne	začít	k5eAaPmIp3nS	začít
působit	působit	k5eAaImF	působit
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
deseti	deset	k4xCc6	deset
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
obalenou	obalený	k2eAgFnSc4d1	obalená
kořist	kořist	k1gFnSc4	kořist
pevně	pevně	k6eAd1	pevně
drží	držet	k5eAaImIp3nS	držet
svými	svůj	k3xOyFgFnPc7	svůj
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
končetinami	končetina	k1gFnPc7	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ucítí	ucítit	k5eAaPmIp3nS	ucítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc4	její
puls	puls	k1gInSc4	puls
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
pomalu	pomalu	k6eAd1	pomalu
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Snovačka	snovačka	k1gFnSc1	snovačka
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nS	množit
pohlavně	pohlavně	k6eAd1	pohlavně
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
před	před	k7c7	před
spářením	spáření	k1gNnSc7	spáření
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
výměšek	výměšek	k1gInSc1	výměšek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
samce	samec	k1gInPc4	samec
žijící	žijící	k2eAgInPc1d1	žijící
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
spáření	spáření	k1gNnSc6	spáření
samce	samec	k1gInSc2	samec
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k8xC	i
svůj	svůj	k3xOyFgInSc4	svůj
alternativní	alternativní	k2eAgInSc4d1	alternativní
název	název	k1gInSc4	název
černá	černý	k2eAgFnSc1d1	černá
vdova	vdova	k1gFnSc1	vdova
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
vejce	vejce	k1gNnPc4	vejce
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
kulovitého	kulovitý	k2eAgInSc2d1	kulovitý
útvaru	útvar	k1gInSc2	útvar
z	z	k7c2	z
jemných	jemný	k2eAgNnPc2d1	jemné
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
zamaskována	zamaskován	k2eAgFnSc1d1	zamaskována
a	a	k8xC	a
chráněna	chráněn	k2eAgFnSc1d1	chráněna
před	před	k7c7	před
vnějšími	vnější	k2eAgFnPc7d1	vnější
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jednoho	jeden	k4xCgNnSc2	jeden
léta	léto	k1gNnSc2	léto
může	moct	k5eAaImIp3nS	moct
naklást	naklást	k5eAaPmF	naklást
čtyři	čtyři	k4xCgInPc4	čtyři
až	až	k9	až
devět	devět	k4xCc4	devět
snůšek	snůška	k1gFnPc2	snůška
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
může	moct	k5eAaImIp3nS	moct
každá	každý	k3xTgFnSc1	každý
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
200	[number]	k4	200
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
obvykle	obvykle	k6eAd1	obvykle
dvacet	dvacet	k4xCc4	dvacet
až	až	k9	až
třicet	třicet	k4xCc4	třicet
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
klade	klást	k5eAaImIp3nS	klást
vysoké	vysoký	k2eAgNnSc4d1	vysoké
množství	množství	k1gNnSc4	množství
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
jich	on	k3xPp3gFnPc2	on
přežívá	přežívat	k5eAaImIp3nS	přežívat
obvykle	obvykle	k6eAd1	obvykle
méně	málo	k6eAd2	málo
jak	jak	k8xS	jak
sto	sto	k4xCgNnSc1	sto
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mláďat	mládě	k1gNnPc2	mládě
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc6	množství
dostupné	dostupný	k2eAgFnSc2d1	dostupná
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
účinnosti	účinnost	k1gFnSc2	účinnost
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
pobývají	pobývat	k5eAaImIp3nP	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Samostatná	samostatný	k2eAgNnPc1d1	samostatné
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
až	až	k6eAd1	až
čtyřech	čtyři	k4xCgInPc6	čtyři
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plně	plně	k6eAd1	plně
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
matce	matka	k1gFnSc6	matka
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
po	po	k7c6	po
šesti	šest	k4xCc6	šest
až	až	k9	až
devíti	devět	k4xCc6	devět
měsících	měsíc	k1gInPc6	měsíc
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dožívat	dožívat	k5eAaImF	dožívat
i	i	k9	i
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
samců	samec	k1gMnPc2	samec
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
její	její	k3xOp3gFnSc4	její
malou	malý	k2eAgFnSc4d1	malá
velikost	velikost	k1gFnSc4	velikost
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
jed	jed	k1gInSc1	jed
vysoce	vysoce	k6eAd1	vysoce
účinný	účinný	k2eAgInSc1d1	účinný
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejúčinnějších	účinný	k2eAgInPc2d3	nejúčinnější
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
pavouků	pavouk	k1gMnPc2	pavouk
(	(	kIx(	(
<g/>
bývá	bývat	k5eAaImIp3nS	bývat
dokonce	dokonce	k9	dokonce
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
silnější	silný	k2eAgFnSc2d2	silnější
než	než	k8xS	než
jed	jed	k1gInSc1	jed
chřestýšů	chřestýš	k1gMnPc2	chřestýš
<g/>
,	,	kIx,	,
kober	kobra	k1gFnPc2	kobra
nebo	nebo	k8xC	nebo
korálovců	korálovec	k1gMnPc2	korálovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
mnohých	mnohý	k2eAgMnPc2d1	mnohý
jedovatých	jedovatý	k2eAgMnPc2d1	jedovatý
pavouků	pavouk	k1gMnPc2	pavouk
nemá	mít	k5eNaImIp3nS	mít
přitom	přitom	k6eAd1	přitom
příliš	příliš	k6eAd1	příliš
silné	silný	k2eAgFnPc1d1	silná
a	a	k8xC	a
velké	velký	k2eAgFnPc1d1	velká
chelicery	chelicera	k1gFnPc1	chelicera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
kousnutí	kousnutí	k1gNnSc2	kousnutí
od	od	k7c2	od
dospělé	dospělý	k2eAgFnSc2d1	dospělá
samice	samice	k1gFnSc2	samice
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
do	do	k7c2	do
kůže	kůže	k1gFnSc2	kůže
dostane	dostat	k5eAaPmIp3nS	dostat
až	až	k9	až
jeden	jeden	k4xCgInSc1	jeden
milimetr	milimetr	k1gInSc1	milimetr
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
jedné	jeden	k4xCgFnSc2	jeden
chelicery	chelicera	k1gFnSc2	chelicera
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
dostane	dostat	k5eAaPmIp3nS	dostat
tak	tak	k6eAd1	tak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
dávka	dávka	k1gFnSc1	dávka
jedu	jet	k5eAaImIp1nS	jet
schopná	schopný	k2eAgFnSc1d1	schopná
k	k	k7c3	k
usmrcení	usmrcení	k1gNnSc3	usmrcení
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgMnPc1d2	menší
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
menší	malý	k2eAgFnSc4d2	menší
jedovou	jedový	k2eAgFnSc4d1	jedová
účinnost	účinnost	k1gFnSc4	účinnost
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kousnutí	kousnutí	k1gNnSc2	kousnutí
od	od	k7c2	od
samce	samec	k1gInSc2	samec
nám	my	k3xPp1nPc3	my
většinou	většinou	k6eAd1	většinou
smrt	smrt	k1gFnSc1	smrt
nehrozí	hrozit	k5eNaImIp3nS	hrozit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
vážné	vážný	k2eAgInPc1d1	vážný
a	a	k8xC	a
nepříjemné	příjemný	k2eNgInPc1d1	nepříjemný
zdravotní	zdravotní	k2eAgInPc1d1	zdravotní
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
zvláště	zvláště	k6eAd1	zvláště
výrazné	výrazný	k2eAgInPc4d1	výrazný
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
snovaček	snovačka	k1gFnPc2	snovačka
začne	začít	k5eAaPmIp3nS	začít
působit	působit	k5eAaImF	působit
již	již	k6eAd1	již
po	po	k7c6	po
několika	několik	k4yIc6	několik
minutách	minuta	k1gFnPc6	minuta
a	a	k8xC	a
v	v	k7c6	v
postižené	postižený	k2eAgFnSc6d1	postižená
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
otok	otok	k1gInSc1	otok
velký	velký	k2eAgInSc1d1	velký
až	až	k9	až
15	[number]	k4	15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
její	její	k3xOp3gInSc4	její
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
areál	areál	k1gInSc4	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
smrtelná	smrtelný	k2eAgNnPc1d1	smrtelné
kousnutí	kousnutí	k1gNnPc1	kousnutí
vzácná	vzácný	k2eAgNnPc1d1	vzácné
a	a	k8xC	a
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1950	[number]	k4	1950
až	až	k8xS	až
1959	[number]	k4	1959
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
63	[number]	k4	63
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dopravě	doprava	k1gFnSc3	doprava
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
dostane	dostat	k5eAaPmIp3nS	dostat
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kousnutí	kousnutí	k1gNnSc1	kousnutí
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
např.	např.	kA	např.
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
