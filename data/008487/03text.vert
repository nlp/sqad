<p>
<s>
Slapová	slapový	k2eAgFnSc1d1	slapová
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
druhotný	druhotný	k2eAgInSc4d1	druhotný
efekt	efekt	k1gInSc4	efekt
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
důsledkem	důsledek	k1gInSc7	důsledek
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
příliv	příliv	k1gInSc1	příliv
a	a	k8xC	a
odliv	odliv	k1gInSc1	odliv
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
není	být	k5eNaImIp3nS	být
konstantní	konstantní	k2eAgFnSc1d1	konstantní
napříč	napříč	k7c7	napříč
celým	celý	k2eAgNnSc7d1	celé
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
gravitace	gravitace	k1gFnSc2	gravitace
jiného	jiný	k2eAgNnSc2d1	jiné
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
gravitační	gravitační	k2eAgNnSc1d1	gravitační
zrychlení	zrychlení	k1gNnSc1	zrychlení
na	na	k7c6	na
bližší	blízký	k2eAgFnSc6d2	bližší
a	a	k8xC	a
vzdálenější	vzdálený	k2eAgFnSc6d2	vzdálenější
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
výrazně	výrazně	k6eAd1	výrazně
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
pokřivení	pokřivení	k1gNnSc3	pokřivení
tvaru	tvar	k1gInSc2	tvar
deformovatelných	deformovatelný	k2eAgNnPc2d1	deformovatelné
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
tekutých	tekutý	k2eAgFnPc2d1	tekutá
<g/>
)	)	kIx)	)
částí	část	k1gFnPc2	část
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měnil	měnit	k5eAaImAgInS	měnit
jeho	jeho	k3xOp3gFnSc7	jeho
celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
kulový	kulový	k2eAgInSc4d1	kulový
tvar	tvar	k1gInSc4	tvar
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
slapová	slapový	k2eAgFnSc1d1	slapová
síla	síla	k1gFnSc1	síla
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
pokřivit	pokřivit	k5eAaPmF	pokřivit
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
elipsoidu	elipsoid	k1gInSc2	elipsoid
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
vybouleninami	vyboulenina	k1gFnPc7	vyboulenina
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
přímo	přímo	k6eAd1	přímo
naproti	naproti	k7c3	naproti
druhému	druhý	k4xOgNnSc3	druhý
tělesu	těleso	k1gNnSc6	těleso
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slapové	slapový	k2eAgFnSc2d1	slapová
síly	síla	k1gFnSc2	síla
způsobené	způsobený	k2eAgInPc1d1	způsobený
gravitačními	gravitační	k2eAgFnPc7d1	gravitační
odchylkami	odchylka	k1gFnPc7	odchylka
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
dané	daný	k2eAgNnSc4d1	dané
gravitační	gravitační	k2eAgNnSc4d1	gravitační
pole	pole	k1gNnSc4	pole
se	se	k3xPyFc4	se
slapové	slapový	k2eAgNnSc1d1	slapové
zrychlení	zrychlení	k1gNnSc1	zrychlení
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
bodě	bod	k1gInSc6	bod
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tělesu	těleso	k1gNnSc3	těleso
vyjádří	vyjádřit	k5eAaPmIp3nP	vyjádřit
vektorovým	vektorový	k2eAgInSc7d1	vektorový
rozdílem	rozdíl	k1gInSc7	rozdíl
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
zrychlení	zrychlení	k1gNnSc2	zrychlení
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
aktuálního	aktuální	k2eAgNnSc2d1	aktuální
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
zrychlení	zrychlení	k1gNnSc2	zrychlení
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
pojem	pojem	k1gInSc1	pojem
slapová	slapový	k2eAgFnSc1d1	slapová
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Slapová	slapový	k2eAgFnSc1d1	slapová
síla	síla	k1gFnSc1	síla
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
deformovat	deformovat	k5eAaImF	deformovat
tvar	tvar	k1gInSc4	tvar
tělesa	těleso	k1gNnSc2	těleso
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
jeho	jeho	k3xOp3gInSc2	jeho
objemu	objem	k1gInSc2	objem
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
máme	mít	k5eAaImIp1nP	mít
kulový	kulový	k2eAgInSc4d1	kulový
tvar	tvar	k1gInSc4	tvar
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
slapová	slapový	k2eAgFnSc1d1	slapová
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
pokřivit	pokřivit	k5eAaPmF	pokřivit
do	do	k7c2	do
elipsoidu	elipsoid	k1gInSc2	elipsoid
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
vybouleninami	vyboulenina	k1gFnPc7	vyboulenina
<g/>
,	,	kIx,	,
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
a	a	k8xC	a
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rotace	rotace	k1gFnSc1	rotace
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
<g/>
,	,	kIx,	,
těleso	těleso	k1gNnSc1	těleso
může	moct	k5eAaImIp3nS	moct
například	například	k6eAd1	například
padat	padat	k5eAaImF	padat
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
gravitace	gravitace	k1gFnSc2	gravitace
po	po	k7c6	po
přímé	přímý	k2eAgFnSc6d1	přímá
dráze	dráha	k1gFnSc6	dráha
volným	volný	k2eAgInSc7d1	volný
pádem	pád	k1gInSc7	pád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
druhým	druhý	k4xOgNnSc7	druhý
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Linearizace	Linearizace	k1gFnSc1	Linearizace
Newtonova	Newtonův	k2eAgInSc2d1	Newtonův
gravitačního	gravitační	k2eAgInSc2d1	gravitační
zákona	zákon	k1gInSc2	zákon
kolem	kolem	k7c2	kolem
centra	centrum	k1gNnSc2	centrum
referenčního	referenční	k2eAgNnSc2d1	referenční
tělesa	těleso	k1gNnSc2	těleso
dává	dávat	k5eAaImIp3nS	dávat
přibližně	přibližně	k6eAd1	přibližně
vztah	vztah	k1gInSc1	vztah
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
obrácené	obrácený	k2eAgFnSc6d1	obrácená
třetí	třetí	k4xOgFnSc6	třetí
mocnině	mocnina	k1gFnSc6	mocnina
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
osy	osa	k1gFnSc2	osa
procházející	procházející	k2eAgFnSc2d1	procházející
centry	centrum	k1gNnPc7	centrum
obou	dva	k4xCgNnPc2	dva
těles	těleso	k1gNnPc2	těleso
nabývá	nabývat	k5eAaImIp3nS	nabývat
formy	forma	k1gFnPc4	forma
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
GMmr	GMmra	k1gFnPc2	GMmra
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
G	G	kA	G
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgFnSc1d1	univerzální
gravitační	gravitační	k2eAgFnSc1d1	gravitační
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
M	M	kA	M
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
tělesa	těleso	k1gNnSc2	těleso
produkujícího	produkující	k2eAgNnSc2d1	produkující
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
m	m	kA	m
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
působení	působení	k1gNnSc2	působení
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
R	R	kA	R
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgNnPc7	tento
tělesy	těleso	k1gNnPc7	těleso
a	a	k8xC	a
r	r	kA	r
≪	≪	k?	≪
R	R	kA	R
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
středu	střed	k1gInSc2	střed
referenčního	referenční	k2eAgNnSc2d1	referenční
tělesa	těleso	k1gNnSc2	těleso
podél	podél	k7c2	podél
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
slapová	slapový	k2eAgFnSc1d1	slapová
síla	síla	k1gFnSc1	síla
působí	působit	k5eAaImIp3nS	působit
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
jak	jak	k8xS	jak
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vybouleniny	vyboulenina	k1gFnSc2	vyboulenina
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
lze	lze	k6eAd1	lze
spočítat	spočítat	k5eAaPmF	spočítat
také	také	k9	také
mimo	mimo	k7c4	mimo
osu	osa	k1gFnSc4	osa
spojující	spojující	k2eAgNnPc4d1	spojující
obě	dva	k4xCgNnPc4	dva
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
směru	směr	k1gInSc6	směr
kolmém	kolmý	k2eAgInSc6d1	kolmý
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
osu	osa	k1gFnSc4	osa
směřují	směřovat	k5eAaImIp3nP	směřovat
slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
lineární	lineární	k2eAgFnSc6d1	lineární
aproximaci	aproximace	k1gFnSc6	aproximace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
zvláště	zvláště	k6eAd1	zvláště
výraznými	výrazný	k2eAgMnPc7d1	výrazný
poblíž	poblíž	k7c2	poblíž
malých	malý	k2eAgNnPc2d1	malé
těles	těleso	k1gNnPc2	těleso
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
hmotnosti	hmotnost	k1gFnSc6	hmotnost
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
neutronové	neutronový	k2eAgFnPc4d1	neutronová
hvězdy	hvězda	k1gFnPc4	hvězda
nebo	nebo	k8xC	nebo
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgFnPc1d1	zodpovědná
za	za	k7c4	za
"	"	kIx"	"
<g/>
špagetizaci	špagetizace	k1gFnSc4	špagetizace
<g/>
"	"	kIx"	"
hmoty	hmota	k1gFnSc2	hmota
padající	padající	k2eAgFnPc1d1	padající
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přídavným	přídavný	k2eAgInSc7d1	přídavný
efektem	efekt	k1gInSc7	efekt
vysvětleným	vysvětlený	k2eAgInSc7d1	vysvětlený
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
kapitole	kapitola	k1gFnSc6	kapitola
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
odpovědné	odpovědný	k2eAgFnPc1d1	odpovědná
za	za	k7c4	za
oceánský	oceánský	k2eAgInSc4d1	oceánský
příliv	příliv	k1gInSc4	příliv
a	a	k8xC	a
odliv	odliv	k1gInSc4	odliv
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
referenčním	referenční	k2eAgNnSc7d1	referenční
tělesem	těleso	k1gNnSc7	těleso
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
jejích	její	k3xOp3gInPc6	její
oceánech	oceán	k1gInPc6	oceán
<g/>
,	,	kIx,	,
a	a	k8xC	a
ovlivňujícími	ovlivňující	k2eAgNnPc7d1	ovlivňující
tělesy	těleso	k1gNnPc7	těleso
jsou	být	k5eAaImIp3nP	být
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Slapová	slapový	k2eAgFnSc1d1	slapová
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
také	také	k9	také
za	za	k7c4	za
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
a	a	k8xC	a
rezonanci	rezonance	k1gFnSc4	rezonance
oběhu	oběh	k1gInSc2	oběh
přirozených	přirozený	k2eAgFnPc2d1	přirozená
oběžnic	oběžnice	k1gFnPc2	oběžnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přídavný	přídavný	k2eAgInSc1d1	přídavný
rotační	rotační	k2eAgInSc1d1	rotační
efekt	efekt	k1gInSc1	efekt
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
dvě	dva	k4xCgNnPc4	dva
tělesa	těleso	k1gNnPc4	těleso
rotující	rotující	k2eAgFnPc1d1	rotující
okolo	okolo	k7c2	okolo
jejich	jejich	k3xOp3gInSc2	jejich
hmotného	hmotný	k2eAgInSc2d1	hmotný
středu	střed	k1gInSc2	střed
lze	lze	k6eAd1	lze
druh	druh	k1gInSc4	druh
dostředivé	dostředivý	k2eAgFnSc2d1	dostředivá
síly	síla	k1gFnSc2	síla
potřebný	potřebný	k2eAgInSc1d1	potřebný
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
pohyb	pohyb	k1gInSc4	pohyb
přidat	přidat	k5eAaPmF	přidat
ke	k	k7c3	k
slapové	slapový	k2eAgFnSc3d1	slapová
síle	síla	k1gFnSc3	síla
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
pro	pro	k7c4	pro
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
kruhové	kruhový	k2eAgFnSc2d1	kruhová
orbity	orbita	k1gFnSc2	orbita
<g/>
.	.	kIx.	.
</s>
<s>
Odečteme	odečíst	k5eAaPmIp1nP	odečíst
<g/>
-li	i	k?	-li
znova	znova	k6eAd1	znova
tuto	tento	k3xDgFnSc4	tento
hodnotu	hodnota	k1gFnSc4	hodnota
v	v	k7c6	v
centru	centr	k1gInSc6	centr
jednoho	jeden	k4xCgNnSc2	jeden
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
dostáváme	dostávat	k5eAaImIp1nP	dostávat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
mr	mr	k?	mr
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
GMmr	GMmr	k1gInSc1	GMmr
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnPc3	omega
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
úhlová	úhlový	k2eAgFnSc1d1	úhlová
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zhruba	zhruba	k6eAd1	zhruba
polovinu	polovina	k1gFnSc4	polovina
předchozího	předchozí	k2eAgInSc2d1	předchozí
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
uvnitř	uvnitř	k7c2	uvnitř
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
například	například	k6eAd1	například
u	u	k7c2	u
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
Měsíce	měsíc	k1gInSc2	měsíc
působících	působící	k2eAgFnPc2d1	působící
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
příčném	příčný	k2eAgInSc6d1	příčný
směru	směr	k1gInSc6	směr
rotace	rotace	k1gFnSc2	rotace
podobný	podobný	k2eAgInSc4d1	podobný
účinek	účinek	k1gInSc4	účinek
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rezonance	rezonance	k1gFnSc1	rezonance
oběhu	oběh	k1gInSc2	oběh
</s>
</p>
<p>
<s>
Rocheova	Rocheův	k2eAgFnSc1d1	Rocheova
mez	mez	k1gFnSc1	mez
</s>
</p>
