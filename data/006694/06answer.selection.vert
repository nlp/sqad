<s>
Většina	většina	k1gFnSc1	většina
rostlin	rostlina	k1gFnPc2	rostlina
získává	získávat	k5eAaImIp3nS	získávat
energii	energie	k1gFnSc4	energie
procesem	proces	k1gInSc7	proces
zvaným	zvaný	k2eAgInSc7d1	zvaný
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
energie	energie	k1gFnSc1	energie
ze	z	k7c2	z
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
