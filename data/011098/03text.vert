<p>
<s>
Victor	Victor	k1gMnSc1	Victor
von	von	k1gInSc4	von
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
též	též	k9	též
Viktor	Viktor	k1gMnSc1	Viktor
von	von	k1gInSc1	von
Kraus	Kraus	k1gMnSc1	Kraus
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1845	[number]	k4	1845
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1905	[number]	k4	1905
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
rodem	rod	k1gInSc7	rod
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
předseda	předseda	k1gMnSc1	předseda
spolku	spolek	k1gInSc2	spolek
Deutscher	Deutschra	k1gFnPc2	Deutschra
Schulverein	Schulvereina	k1gFnPc2	Schulvereina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
gymnaziální	gymnaziální	k2eAgMnSc1d1	gymnaziální
profesor	profesor	k1gMnSc1	profesor
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
<g/>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
v	v	k7c6	v
nekrologu	nekrolog	k1gInSc6	nekrolog
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
Bohemia	bohemia	k1gFnSc1	bohemia
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
rozen	rodit	k5eAaImNgInS	rodit
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
také	také	k9	také
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Berlínské	berlínský	k2eAgFnSc6d1	Berlínská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
učitelským	učitelský	k2eAgMnSc7d1	učitelský
čekatelem	čekatel	k1gMnSc7	čekatel
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
řádným	řádný	k2eAgMnSc7d1	řádný
profesorem	profesor	k1gMnSc7	profesor
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
zeměpisu	zeměpis	k1gInSc2	zeměpis
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
Leopoldstadtu	Leopoldstadt	k1gInSc6	Leopoldstadt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prusko-francouzské	pruskorancouzský	k2eAgFnSc2d1	prusko-francouzská
války	válka	k1gFnSc2	válka
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
polním	polní	k2eAgInSc6d1	polní
lazaretu	lazaret	k1gInSc6	lazaret
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
pruské	pruský	k2eAgNnSc4d1	pruské
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
oko	oko	k1gNnSc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
národoveckých	národovecký	k2eAgFnPc6d1	národovecká
organizacích	organizace	k1gFnPc6	organizace
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1880	[number]	k4	1880
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
zakladatelů	zakladatel	k1gMnPc2	zakladatel
spolku	spolek	k1gInSc2	spolek
Deutscher	Deutschra	k1gFnPc2	Deutschra
Schulverein	Schulverein	k1gInSc1	Schulverein
<g/>
,	,	kIx,	,
zaměřeného	zaměřený	k2eAgMnSc4d1	zaměřený
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
německého	německý	k2eAgNnSc2d1	německé
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
národnostně	národnostně	k6eAd1	národnostně
smíšených	smíšený	k2eAgInPc6d1	smíšený
regionech	region	k1gInPc6	region
Předlitavska	Předlitavsko	k1gNnSc2	Předlitavsko
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
místopředsedou	místopředseda	k1gMnSc7	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
referátu	referát	k1gInSc2	referát
spadala	spadat	k5eAaImAgFnS	spadat
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
<g/>
Působil	působit	k5eAaImAgMnS	působit
taky	taky	k6eAd1	taky
jako	jako	k8xS	jako
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
celostátního	celostátní	k2eAgInSc2d1	celostátní
parlamentu	parlament	k1gInSc2	parlament
Předlitavska	Předlitavsko	k1gNnSc2	Předlitavsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
za	za	k7c4	za
kurii	kurie	k1gFnSc4	kurie
městskou	městský	k2eAgFnSc4d1	městská
v	v	k7c6	v
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Hartberg	Hartberg	k1gInSc1	Hartberg
<g/>
,	,	kIx,	,
Feldbach	Feldbach	k1gMnSc1	Feldbach
<g/>
,	,	kIx,	,
Fürstenfeld	Fürstenfeld	k1gMnSc1	Fürstenfeld
<g/>
,	,	kIx,	,
Weiz	Weiz	k1gMnSc1	Weiz
<g/>
,	,	kIx,	,
Gleisdorf	Gleisdorf	k1gMnSc1	Gleisdorf
atd.	atd.	kA	atd.
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
zemřel	zemřít	k5eAaPmAgMnS	zemřít
poslanec	poslanec	k1gMnSc1	poslanec
Oskar	Oskar	k1gMnSc1	Oskar
Falke	Falke	k1gFnSc1	Falke
<g/>
.	.	kIx.	.
</s>
<s>
Slib	slib	k1gInSc1	slib
složil	složit	k5eAaPmAgInS	složit
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
zde	zde	k6eAd1	zde
obhájil	obhájit	k5eAaPmAgInS	obhájit
v	v	k7c6	v
řádných	řádný	k2eAgFnPc6d1	řádná
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
a	a	k8xC	a
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
rytíř	rytíř	k1gMnSc1	rytíř
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Victor	Victor	k1gMnSc1	Victor
von	von	k1gInSc4	von
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
gymnaziální	gymnaziální	k2eAgMnSc1d1	gymnaziální
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
bytem	byt	k1gInSc7	byt
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
i	i	k8xC	i
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
coby	coby	k?	coby
člen	člen	k1gMnSc1	člen
klubu	klub	k1gInSc2	klub
Sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
levice	levice	k1gFnSc2	levice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
utvořilo	utvořit	k5eAaPmAgNnS	utvořit
několik	několik	k4yIc1	několik
ústavověrných	ústavověrný	k2eAgInPc2d1	ústavověrný
politických	politický	k2eAgInPc2d1	politický
proudů	proud	k1gInPc2	proud
liberálního	liberální	k2eAgNnSc2d1	liberální
a	a	k8xC	a
centralistického	centralistický	k2eAgNnSc2d1	centralistické
ražení	ražení	k1gNnSc2	ražení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
klubu	klub	k1gInSc2	klub
Pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Leopoldstadtu	Leopoldstadt	k1gInSc6	Leopoldstadt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
hlavně	hlavně	k9	hlavně
školskými	školský	k2eAgNnPc7d1	školské
tématy	téma	k1gNnPc7	téma
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgInS	vystupovat
proti	proti	k7c3	proti
obnovování	obnovování	k1gNnSc3	obnovování
konfesijního	konfesijní	k2eAgInSc2d1	konfesijní
charakteru	charakter	k1gInSc2	charakter
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgMnS	podporovat
moderní	moderní	k2eAgFnSc2d1	moderní
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
na	na	k7c6	na
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
jako	jako	k9	jako
poslanec	poslanec	k1gMnSc1	poslanec
nacionalistického	nacionalistický	k2eAgInSc2d1	nacionalistický
klubu	klub	k1gInSc2	klub
Deutschnationale	Deutschnationale	k1gMnSc1	Deutschnationale
Vereinigung	Vereinigung	k1gMnSc1	Vereinigung
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
Německé	německý	k2eAgFnSc2d1	německá
lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
ale	ale	k8xC	ale
nevyšla	vyjít	k5eNaPmAgFnS	vyjít
obhajoba	obhajoba	k1gFnSc1	obhajoba
jeho	on	k3xPp3gInSc2	on
mandátu	mandát	k1gInSc2	mandát
v	v	k7c6	v
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
porazil	porazit	k5eAaPmAgMnS	porazit
národnostně	národnostně	k6eAd1	národnostně
ještě	ještě	k9	ještě
radikálnější	radikální	k2eAgMnSc1d2	radikálnější
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
.	.	kIx.	.
<g/>
Habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
soukromý	soukromý	k2eAgMnSc1d1	soukromý
docent	docent	k1gMnSc1	docent
dějin	dějiny	k1gFnPc2	dějiny
na	na	k7c6	na
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
řadu	řada	k1gFnSc4	řada
historických	historický	k2eAgFnPc2d1	historická
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
úmrtí	úmrtí	k1gNnSc2	úmrtí
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
ředitele	ředitel	k1gMnSc2	ředitel
dívčího	dívčí	k2eAgNnSc2d1	dívčí
gymnázia	gymnázium	k1gNnSc2	gymnázium
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yRnSc1	co
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Moritz	Moritz	k1gMnSc1	Moritz
Weitlof	Weitlof	k1gMnSc1	Weitlof
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Victor	Victor	k1gMnSc1	Victor
Kraus	Kraus	k1gMnSc1	Kraus
taky	taky	k6eAd1	taky
stal	stát	k5eAaPmAgMnS	stát
druhým	druhý	k4xOgMnSc7	druhý
předsedou	předseda	k1gMnSc7	předseda
německého	německý	k2eAgInSc2d1	německý
spolku	spolek	k1gInSc2	spolek
Deutscher	Deutschra	k1gFnPc2	Deutschra
Schulverein	Schulvereina	k1gFnPc2	Schulvereina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ovšem	ovšem	k9	ovšem
setrval	setrvat	k5eAaPmAgMnS	setrvat
jen	jen	k9	jen
po	po	k7c4	po
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
<g/>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
