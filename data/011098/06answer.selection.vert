<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
národoveckých	národovecký	k2eAgFnPc6d1	národovecká
organizacích	organizace	k1gFnPc6	organizace
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1880	[number]	k4	1880
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
zakladatelů	zakladatel	k1gMnPc2	zakladatel
spolku	spolek	k1gInSc2	spolek
Deutscher	Deutschra	k1gFnPc2	Deutschra
Schulverein	Schulverein	k1gInSc1	Schulverein
<g/>
,	,	kIx,	,
zaměřeného	zaměřený	k2eAgMnSc4d1	zaměřený
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
německého	německý	k2eAgNnSc2d1	německé
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
národnostně	národnostně	k6eAd1	národnostně
smíšených	smíšený	k2eAgInPc6d1	smíšený
regionech	region	k1gInPc6	region
Předlitavska	Předlitavsko	k1gNnSc2	Předlitavsko
<g/>
.	.	kIx.	.
</s>
