<s>
Casa	Casa	k1gMnSc1
Pascual	Pascual	k1gMnSc1
i	i	k9
Pons	Pons	k1gInSc4
je	být	k5eAaImIp3nS
neogotická	ogotický	k2eNgFnSc1d1
budova	budova	k1gFnSc1
navržená	navržený	k2eAgFnSc1d1
Enricem	Enric	k1gMnSc7
Sagnierem	Sagnier	k1gMnSc7
Villavecchiem	Villavecchius	k1gMnSc7
<g/>
,	,	kIx,
stojící	stojící	k2eAgFnSc1d1
na	na	k7c4
passeig	passeig	k1gInSc4
de	de	k?
Grà	Grà	k1gMnSc2
v	v	k7c6
barcelonské	barcelonský	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Eixample	Eixample	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1890	#num#	k4
<g/>
.	.	kIx.
</s>