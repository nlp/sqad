<s>
Casa	Casa	k1gMnSc1
Pascual	Pascual	k1gMnSc1
i	i	k9
Pons	Pons	k1gInSc4
</s>
<s>
Casa	Casa	k1gMnSc1
Pascual	Pascual	k1gMnSc1
i	i	k9
Pons	Pons	k1gInSc4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
41	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Casa	Casa	k1gMnSc1
Pascual	Pascual	k1gMnSc1
i	i	k9
Pons	Pons	k1gInSc4
je	být	k5eAaImIp3nS
neogotická	ogotický	k2eNgFnSc1d1
budova	budova	k1gFnSc1
navržená	navržený	k2eAgFnSc1d1
Enricem	Enric	k1gMnSc7
Sagnierem	Sagnier	k1gMnSc7
Villavecchiem	Villavecchius	k1gMnSc7
<g/>
,	,	kIx,
stojící	stojící	k2eAgFnSc1d1
na	na	k7c4
passeig	passeig	k1gInSc4
de	de	k?
Grà	Grà	k1gMnSc2
v	v	k7c6
barcelonské	barcelonský	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Eixample	Eixample	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1890	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
má	mít	k5eAaImIp3nS
4	#num#	k4
patra	patro	k1gNnSc2
<g/>
,	,	kIx,
obdélníkový	obdélníkový	k2eAgInSc4d1
půdorys	půdorys	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
asi	asi	k9
22	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objekt	objekt	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
dvěma	dva	k4xCgInPc7
stavbami	stavba	k1gFnPc7
v	v	k7c6
jedné	jeden	k4xCgFnSc3
<g/>
,	,	kIx,
navržený	navržený	k2eAgInSc4d1
pro	pro	k7c4
dva	dva	k4xCgInPc4
členy	člen	k1gInPc4
jedné	jeden	k4xCgFnSc2
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Část	část	k1gFnSc1
prostor	prostora	k1gFnPc2
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
nájemní	nájemní	k2eAgInPc4d1
byty	byt	k1gInPc4
-	-	kIx~
nájemníci	nájemník	k1gMnPc1
a	a	k8xC
majitelé	majitel	k1gMnPc1
měli	mít	k5eAaImAgMnP
oddělené	oddělený	k2eAgInPc4d1
vstupy	vstup	k1gInPc4
do	do	k7c2
budovy	budova	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Byla	být	k5eAaImAgFnS
renovována	renovován	k2eAgFnSc1d1
roku	rok	k1gInSc2
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
interiéru	interiér	k1gInSc6
zůstaly	zůstat	k5eAaPmAgFnP
zachovány	zachován	k2eAgFnPc1d1
vitráže	vitráž	k1gFnPc1
a	a	k8xC
původní	původní	k2eAgInPc1d1
krby	krb	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
španělskou	španělský	k2eAgFnSc7d1
kulturní	kulturní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
lokálního	lokální	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Casa	Casa	k1gMnSc1
Pascual	Pascual	k1gMnSc1
i	i	k8xC
Pons	Pons	k1gInSc1
<g/>
,	,	kIx,
emporis	emporis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
CASA	CASA	kA
PASCUAL	PASCUAL	kA
I	i	k8xC
PONS	PONS	kA
<g/>
,	,	kIx,
w	w	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
bcn	bcn	k?
<g/>
.	.	kIx.
<g/>
cat	cat	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Casa	Cas	k1gInSc2
Pascual	Pascual	k1gInSc1
i	i	k8xC
Pons	Pons	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
