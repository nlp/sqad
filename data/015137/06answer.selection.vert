<s>
Stanislas	Stanislasit	k5eAaPmRp2nS
Dehaene	Dehaen	k1gMnSc5
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
soukromém	soukromý	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
St.	st.	kA
Genevieve	Genevieev	k1gFnSc2
a	a	k8xC
na	na	k7c6
École	Écola	k1gFnSc6
Normale	Normal	k1gMnSc5
Supérieure	Supérieur	k1gMnSc5
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
Master	master	k1gMnSc1
v	v	k7c6
matematice	matematika	k1gFnSc6
na	na	k7c6
Universitě	universita	k1gFnSc6
Pierre	Pierr	k1gInSc5
a	a	k8xC
Marie	Maria	k1gFnSc2
Curie	Curie	k1gMnPc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
doktorát	doktorát	k1gInSc4
z	z	k7c2
psychologie	psychologie	k1gFnSc2
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
sociálních	sociální	k2eAgFnPc2d1
věd	věda	k1gFnPc2
–	–	k?
Ecole	Ecole	k1gFnSc2
des	des	k1gNnSc2
hautes	hautes	k1gMnSc1
études	études	k1gMnSc1
en	en	k?
sciences	sciences	k1gMnSc1
sociales	sociales	k1gMnSc1
EHSS	EHSS	kA
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Svou	svůj	k3xOyFgFnSc4
doktorskou	doktorský	k2eAgFnSc4d1
dizertaci	dizertace	k1gFnSc4
nazval	nazvat	k5eAaPmAgMnS,k5eAaBmAgMnS
Porovnání	porovnání	k1gNnSc1
malých	malý	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
:	:	kIx,
analogová	analogový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
a	a	k8xC
procesy	proces	k1gInPc1
pozornosti	pozornost	k1gFnSc2
<g/>
.	.	kIx.
</s>