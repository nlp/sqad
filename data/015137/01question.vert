<s>
Jaká	jaký	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zkratka	zkratka	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
Vysokou	vysoký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
sociálních	sociální	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Stanislas	Stanislas	k1gInSc1
Dahaene	Dahaen	k1gInSc5
získal	získat	k5eAaPmAgMnS
doktorát	doktorát	k1gInSc4
z	z	k7c2
psychologie	psychologie	k1gFnSc2
<g/>
?	?	kIx.
</s>