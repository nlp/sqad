<s>
Stanislas	Stanislasit	k5eAaPmRp2nS
Dehaene	Dehaen	k1gMnSc5
</s>
<s>
Stanislas	Stanislasit	k5eAaPmRp2nS
Dehaene	Dehaen	k1gMnSc5
Narození	narozený	k2eAgMnPc1d1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1965	#num#	k4
(	(	kIx(
<g/>
55	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Roubaix	Roubaix	k1gInSc1
Bydliště	bydliště	k1gNnSc2
</s>
<s>
Palaiseau	Palaiseau	k6eAd1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
École	École	k6eAd1
normale	normale	k6eAd1
supérieureŠkola	supérieureŠkola	k1gFnSc1
pro	pro	k7c4
pokročilá	pokročilý	k2eAgNnPc4d1
studia	studio	k1gNnPc4
v	v	k7c6
sociálních	sociální	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
Povolání	povolání	k1gNnSc2
</s>
<s>
neurovědec	neurovědec	k1gMnSc1
a	a	k8xC
psycholog	psycholog	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Collè	Collè	k1gFnSc1
de	de	k?
FranceÉcole	FranceÉcole	k1gFnSc1
Polytechnique	Polytechniqu	k1gFnSc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Jean-Louis	Jean-Louis	k1gInSc1
Signoret	Signoret	k1gInSc1
Prize	Prize	k1gFnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
C.L.	C.L.	k1gFnSc1
de	de	k?
Carvalho-Heineken	Carvalho-Heineken	k1gInSc4
Prize	Prize	k1gFnSc2
for	forum	k1gNnPc2
Cognitive	Cognitiv	k1gInSc5
Science	Science	k1gFnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
rytíř	rytíř	k1gMnSc1
Čestné	čestný	k2eAgFnSc2d1
legie	legie	k1gFnSc2
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
Inserm	Inserm	k1gInSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
The	The	k1gFnSc1
Brain	Brain	k1gInSc1
Prize	Prize	k1gFnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Ghislaine	Ghislainout	k5eAaPmIp3nS
Dehaene-Lambertz	Dehaene-Lambertz	k1gInSc4
Funkce	funkce	k1gFnSc1
</s>
<s>
prezident	prezident	k1gMnSc1
(	(	kIx(
<g/>
conseil	conseil	k1gMnSc1
scientifique	scientifiqu	k1gFnSc2
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Éducation	Éducation	k1gInSc1
nationale	nationale	k6eAd1
<g/>
;	;	kIx,
od	od	k7c2
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stanislas	Stanislasit	k5eAaPmRp2nS
Dehaene	Dehaen	k1gMnSc5
(	(	kIx(
<g/>
*	*	kIx~
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1965	#num#	k4
Roubaix	Roubaix	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzský	francouzský	k2eAgMnSc1d1
psycholog	psycholog	k1gMnSc1
<g/>
,	,	kIx,
odborník	odborník	k1gMnSc1
na	na	k7c4
kognitivní	kognitivní	k2eAgFnSc4d1
psychologii	psychologie	k1gFnSc4
a	a	k8xC
neurovědy	neurověda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS
zejména	zejména	k9
mozkové	mozkový	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
aritmetiky	aritmetika	k1gFnSc2
a	a	k8xC
práce	práce	k1gFnSc2
s	s	k7c7
čísly	číslo	k1gNnPc7
<g/>
,	,	kIx,
čtení	čtení	k1gNnSc1
a	a	k8xC
vědomí	vědomí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
výzkumu	výzkum	k1gInSc3
používá	používat	k5eAaImIp3nS
metody	metoda	k1gFnPc4
kognitivní	kognitivní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
a	a	k8xC
zobrazování	zobrazování	k1gNnSc2
mozku	mozek	k1gInSc2
(	(	kIx(
<g/>
funkční	funkční	k2eAgNnSc4d1
zobrazování	zobrazování	k1gNnSc4
magnetickou	magnetický	k2eAgFnSc7d1
rezonancí	rezonance	k1gFnSc7
<g/>
,	,	kIx,
magnetoencefalografie	magnetoencefalografie	k1gFnSc1
a	a	k8xC
elektroencefalografie	elektroencefalografie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Stanislas	Stanislasit	k5eAaPmRp2nS
Dehaene	Dehaen	k1gMnSc5
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
soukromém	soukromý	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
St.	st.	kA
Genevieve	Genevieev	k1gFnSc2
a	a	k8xC
na	na	k7c6
École	Écola	k1gFnSc6
Normale	Normal	k1gMnSc5
Supérieure	Supérieur	k1gMnSc5
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
Master	master	k1gMnSc1
v	v	k7c6
matematice	matematika	k1gFnSc6
na	na	k7c6
Universitě	universita	k1gFnSc6
Pierre	Pierr	k1gInSc5
a	a	k8xC
Marie	Maria	k1gFnSc2
Curie	Curie	k1gMnPc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
doktorát	doktorát	k1gInSc4
z	z	k7c2
psychologie	psychologie	k1gFnSc2
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
sociálních	sociální	k2eAgFnPc2d1
věd	věda	k1gFnPc2
–	–	k?
Ecole	Ecole	k1gFnSc2
des	des	k1gNnSc2
hautes	hautes	k1gMnSc1
études	études	k1gMnSc1
en	en	k?
sciences	sciences	k1gMnSc1
sociales	sociales	k1gMnSc1
EHSS	EHSS	kA
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Svou	svůj	k3xOyFgFnSc4
doktorskou	doktorský	k2eAgFnSc4d1
dizertaci	dizertace	k1gFnSc4
nazval	nazvat	k5eAaPmAgMnS,k5eAaBmAgMnS
Porovnání	porovnání	k1gNnSc1
malých	malý	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
:	:	kIx,
analogová	analogový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
a	a	k8xC
procesy	proces	k1gInPc1
pozornosti	pozornost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědeckým	vědecký	k2eAgNnSc7d1
vedoucím	vedoucí	k2eAgMnPc3d1
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgMnS
Jacques	Jacques	k1gMnSc1
Mehler	Mehler	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
mála	málo	k4c2
odborníků	odborník	k1gMnPc2
na	na	k7c4
kognitivní	kognitivní	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
ve	v	k7c6
Francii	Francie	k1gFnSc6
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Současně	současně	k6eAd1
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
Jeanem	Jean	k1gMnSc7
Pierrem	Pierr	k1gMnSc7
Changeuxem	Changeux	k1gInSc7
na	na	k7c4
vytvoření	vytvoření	k1gNnSc4
modelů	model	k1gInPc2
neurobiologických	urobiologický	k2eNgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
zpěv	zpěv	k1gInSc1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
studuje	studovat	k5eAaImIp3nS
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
Michaela	Michael	k1gMnSc2
Posnera	Posner	k1gMnSc2
techniky	technika	k1gFnSc2
zobrazování	zobrazování	k1gNnSc2
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
představitelů	představitel	k1gMnPc2
kognitivní	kognitivní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2005	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
členem	člen	k1gMnSc7
francouzské	francouzský	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
profesorem	profesor	k1gMnSc7
na	na	k7c4
Collè	Collè	k1gInSc4
de	de	k?
France	Franc	k1gMnSc2
na	na	k7c6
katedře	katedra	k1gFnSc6
experimentální	experimentální	k2eAgFnSc2d1
kognitivní	kognitivní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Stanislas	Stanislas	k1gInSc1
Dehaene	Dehaen	k1gInSc5
je	být	k5eAaImIp3nS
ředitelem	ředitel	k1gMnSc7
jednotky	jednotka	k1gFnSc2
kognitivního	kognitivní	k2eAgNnSc2d1
neuronového	neuronový	k2eAgNnSc2d1
zobrazování	zobrazování	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
společné	společný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
organizací	organizace	k1gFnPc2
INSERM	INSERM	kA
a	a	k8xC
CEA	CEA	kA
a	a	k8xC
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
laboratoří	laboratoř	k1gFnPc2
výzkumného	výzkumný	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
NeuroSpin	NeuroSpina	k1gFnPc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
kraji	kraj	k1gInSc6
Essonne	Essonn	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7
manželkou	manželka	k1gFnSc7
je	být	k5eAaImIp3nS
Ghislaine	Ghislain	k1gInSc5
Dehaene-Lambertz	Dehaene-Lambertz	k1gInSc1
<g/>
,	,	kIx,
lékařka	lékařka	k1gFnSc1
a	a	k8xC
odbornice	odbornice	k1gFnSc1
na	na	k7c4
pediatrickou	pediatrický	k2eAgFnSc4d1
neurobiologii	neurobiologie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oblast	oblast	k1gFnSc1
výzkumu	výzkum	k1gInSc2
</s>
<s>
Stanislas	Stanislasit	k5eAaPmRp2nS
Dehaene	Dehaen	k1gMnSc5
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
výzkumu	výzkum	k1gInSc6
spojení	spojení	k1gNnSc2
metod	metoda	k1gFnPc2
kognitivní	kognitivní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
a	a	k8xC
zobrazování	zobrazování	k1gNnSc2
mozku	mozek	k1gInSc2
ke	k	k7c3
studiu	studio	k1gNnSc3
mozkové	mozkový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
aritmetiky	aritmetika	k1gFnSc2
<g/>
,	,	kIx,
čtení	čtení	k1gNnSc2
<g/>
,	,	kIx,
mluveného	mluvený	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
přístupu	přístup	k1gInSc2
informací	informace	k1gFnPc2
k	k	k7c3
vědomí	vědomí	k1gNnSc3
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ho	on	k3xPp3gMnSc4
přivedlo	přivést	k5eAaPmAgNnS
ke	k	k7c3
studiu	studio	k1gNnSc3
dyskalkulie	dyskalkulie	k1gFnSc2
a	a	k8xC
dyslexie	dyslexie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Zpopularizoval	zpopularizovat	k5eAaPmAgInS
kognitivní	kognitivní	k2eAgInSc1d1
vědecký	vědecký	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
těchto	tento	k3xDgNnPc2
témat	téma	k1gNnPc2
ve	v	k7c6
třech	tři	k4xCgFnPc6
knihách	kniha	k1gFnPc6
<g/>
:	:	kIx,
La	la	k1gNnSc4
Bosse	boss	k1gMnSc2
des	des	k1gNnSc4
maths	maths	k1gInSc1
(	(	kIx(
<g/>
Vlohy	vloha	k1gFnPc1
pro	pro	k7c4
matematiku	matematika	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Les	les	k1gInSc4
Neurones	Neuronesa	k1gFnPc2
de	de	k?
la	la	k1gNnSc4
lecture	lectur	k1gMnSc5
(	(	kIx(
<g/>
Neurony	neuron	k1gInPc1
čtení	čtení	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
Le	Le	k1gMnSc1
Code	Code	k1gNnSc2
de	de	k?
la	la	k1gNnSc2
conscience	conscience	k1gFnSc1
(	(	kIx(
<g/>
Kód	kód	k1gInSc1
vědomí	vědomí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
1999	#num#	k4
–	–	k?
Centennial	Centennial	k1gMnSc1
Fellowship	Fellowship	k1gMnSc1
of	of	k?
Mc	Mc	k1gMnSc1
Donnell	Donnell	k1gMnSc1
Foundation	Foundation	k1gInSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2005	#num#	k4
–	–	k?
člen	člen	k1gMnSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
</s>
<s>
2008	#num#	k4
–	–	k?
člen	člen	k1gInSc1
Papežská	papežský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2008	#num#	k4
–	–	k?
Laureát	laureát	k1gMnSc1
ceny	cena	k1gFnSc2
Carvalho-Heineken	Carvalho-Heinekno	k1gNnPc2
za	za	k7c4
kognitivní	kognitivní	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
<g/>
,	,	kIx,
udělovanou	udělovaný	k2eAgFnSc7d1
Královskou	královský	k2eAgFnSc7d1
akademií	akademie	k1gFnSc7
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnSc2
Holandska	Holandsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
–	–	k?
člen	člen	k1gMnSc1
Národní	národní	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Americké	americký	k2eAgFnSc2d1
filosofické	filosofický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
korespondent	korespondent	k1gMnSc1
Britské	britský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2011	#num#	k4
–	–	k?
Rytíř	Rytíř	k1gMnSc1
Řádu	řád	k1gInSc2
čestné	čestný	k2eAgFnSc2d1
legie	legie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2013	#num#	k4
–	–	k?
Velká	velký	k2eAgFnSc1d1
cena	cena	k1gFnSc1
výzkumného	výzkumný	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
INSERM	INSERM	kA
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2013	#num#	k4
–	–	k?
doctor	doctor	k1gInSc1
honoris	honoris	k1gFnSc1
causa	causa	k1gFnSc1
Vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
ekonomické	ekonomický	k2eAgFnSc2d1
HEC	HEC	k?
Paris	Paris	k1gMnSc1
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2014	#num#	k4
–	–	k?
laureát	laureát	k1gMnSc1
Brain	Brain	k1gMnSc1
Prize	Prize	k1gFnSc1
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Giacomo	Giacoma	k1gFnSc5
Rizzolattim	Rizzolatti	k1gNnSc7
a	a	k8xC
Trevorem	Trevor	k1gMnSc7
W.	W.	kA
Robbinsem	Robbins	k1gMnSc7
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Články	článek	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Numerical	Numerical	k1gFnSc1
Cognition	Cognition	k1gInSc1
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
:	:	kIx,
Blackwell	Blackwell	k1gInSc1
<g/>
,	,	kIx,
1993	#num#	k4
ISBN	ISBN	kA
1-557-86444-6	1-557-86444-6	k4
</s>
<s>
La	la	k1gNnSc1
bosse	boss	k1gMnSc2
des	des	k1gNnPc2
maths	maths	k6eAd1
<g/>
,	,	kIx,
Odile	Odile	k1gNnSc1
Jacob	Jacoba	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
ISBN	ISBN	kA
9782738125248	#num#	k4
(	(	kIx(
<g/>
první	první	k4xOgNnPc4
vydání	vydání	k1gNnPc4
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Le	Le	k?
Cerveau	Cerveaus	k1gInSc2
en	en	k?
action	action	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
L	L	kA
<g/>
'	'	kIx"
<g/>
imagerie	imagerie	k1gFnSc1
cérébrale	cérébrale	k6eAd1
en	en	k?
psychologie	psychologie	k1gFnSc1
cognitive	cognitiv	k1gInSc5
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
:	:	kIx,
Presses	Presses	k1gMnSc1
universitaires	universitaires	k1gMnSc1
de	de	k?
France	Franc	k1gMnSc4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc4
<g/>
,	,	kIx,
1997	#num#	k4
ISBN	ISBN	kA
2-13-048270-8	2-13-048270-8	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gFnSc1
Number	Number	k1gInSc1
Sense	Sense	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
ISBN	ISBN	kA
0-19-511004-8	0-19-511004-8	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gFnSc1
Cognitive	Cognitiv	k1gInSc5
Neuroscience	Neuroscienka	k1gFnSc3
of	of	k?
Consciousness	Consciousness	k1gInSc1
<g/>
,	,	kIx,
London	London	k1gMnSc1
:	:	kIx,
MIT	MIT	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
ISBN	ISBN	kA
0-262-54131-9	0-262-54131-9	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
From	From	k1gInSc1
Monkey	Monkea	k1gFnSc2
Brain	Braina	k1gFnPc2
to	ten	k3xDgNnSc1
Human	Human	k1gMnSc1
Brain	Brain	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Fyssen	Fyssen	k2eAgInSc4d1
Foundation	Foundation	k1gInSc4
Symposium	symposium	k1gNnSc4
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Jean-René	Jean-Rený	k2eAgNnSc1d1
Duhamelem	Duhamel	k1gMnSc7
<g/>
,	,	kIx,
Marcem	Marce	k1gMnSc7
D.	D.	kA
Hauserem	Hauser	k1gMnSc7
a	a	k8xC
Giacomem	Giacom	k1gMnSc7
Rizzolattim	Rizzolattima	k1gFnPc2
<g/>
,	,	kIx,
MIT	MIT	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
ISBN	ISBN	kA
0-262-04223-1	0-262-04223-1	k4
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Im	Im	k1gFnSc1
Auge	Aug	k1gFnSc2
des	des	k1gNnSc2
Lesers	Lesersa	k1gFnPc2
<g/>
,	,	kIx,
Transmedia	Transmedium	k1gNnSc2
Curych	Curych	k1gInSc1
:	:	kIx,
Hans-Werner	Hans-Werner	k1gMnSc1
Hunziker	Hunziker	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
ISBN	ISBN	kA
978-3-7266-0068-6	978-3-7266-0068-6	k4
</s>
<s>
Les	les	k1gInSc1
neurones	neurones	k1gInSc1
de	de	k?
la	la	k0
lecture	lectur	k1gMnSc5
<g/>
,	,	kIx,
Odile	Odile	k1gNnSc3
Jacob	Jacoba	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
ISBN	ISBN	kA
978-2-7381-1974-2	978-2-7381-1974-2	k4
</s>
<s>
Vers	Vers	k1gInSc1
une	une	k?
science	scienec	k1gInSc2
de	de	k?
la	la	k1gNnPc2
vie	vie	k?
mentale	mental	k1gMnSc5
<g/>
,,	,,	k?
úvodní	úvodní	k2eAgFnSc1d1
přednáška	přednáška	k1gFnSc1
na	na	k7c6
Collè	Collè	k1gFnSc6
de	de	k?
France	Franc	k1gMnSc4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
:	:	kIx,
Fayard	Fayard	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
ISBN	ISBN	kA
2-213-63084-4	2-213-63084-4	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Reading	Reading	k1gInSc1
in	in	k?
the	the	k?
Brain	Brain	k1gMnSc1
<g/>
,	,	kIx,
Viking	Viking	k1gMnSc1
Penguin	Penguin	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
Apprendre	Apprendr	k1gInSc5
à	à	k?
lire	lire	k1gNnSc7
-	-	kIx~
Des	des	k1gNnSc7
sciences	sciences	k1gMnSc1
cognitives	cognitives	k1gMnSc1
à	à	k?
la	la	k1gNnPc2
salle	salle	k1gNnPc2
de	de	k?
classe	classe	k1gFnSc2
<g/>
,	,	kIx,
Odile	Odile	k1gNnSc1
Jacob	Jacoba	k1gFnPc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
kolektivní	kolektivní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Stanislase	Stanislas	k1gInSc6
Dehaenea	Dehaene	k2eAgFnSc1d1
ISBN	ISBN	kA
978-2-7381-2680-1	978-2-7381-2680-1	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Consciousness	Consciousness	k1gInSc1
and	and	k?
the	the	k?
Brain	Brain	k1gInSc1
<g/>
:	:	kIx,
Deciphering	Deciphering	k1gInSc1
How	How	k1gFnSc2
the	the	k?
Brain	Brain	k2eAgInSc1d1
Codes	Codes	k1gInSc1
Our	Our	k1gFnSc2
Thoughts	Thoughtsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viking	Viking	k1gMnSc1
Adult	Adult	k1gMnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-0-670-02543-5	978-0-670-02543-5	k4
</s>
<s>
Le	Le	k?
Code	Code	k1gInSc1
de	de	k?
la	la	k1gNnSc7
conscience	conscience	k1gFnSc2
<g/>
,	,	kIx,
Odile	Odile	k1gNnSc1
Jacob	Jacoba	k1gFnPc2
<g/>
,	,	kIx,
Oj	oj	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věda	věda	k1gFnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-2738131058	978-2738131058	k4
</s>
<s>
Apprendre	Apprendr	k1gInSc5
<g/>
!	!	kIx.
</s>
<s desamb="1">
:	:	kIx,
les	les	k1gInSc1
talents	talents	k1gInSc1
du	du	k?
cerveau	cerveaus	k1gInSc2
<g/>
,	,	kIx,
le	le	k?
défi	déf	k1gFnSc2
des	des	k1gNnSc2
machines	machinesa	k1gFnPc2
<g/>
,	,	kIx,
O.	O.	kA
Jacob	Jacoba	k1gFnPc2
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Stanislas	Stanislasa	k1gFnPc2
Dehaene	Dehaen	k1gInSc5
na	na	k7c6
francouzské	francouzský	k2eAgFnSc3d1
Wikipedii	Wikipedie	k1gFnSc3
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sorbonne	Sorbonn	k1gInSc5
Université	Universita	k1gMnPc1
|	|	kIx~
Lettres	Lettres	k1gMnSc1
<g/>
,	,	kIx,
Médecine	Médecin	k1gMnSc5
<g/>
,	,	kIx,
Sciences	Sciences	k1gMnSc1
<g/>
.	.	kIx.
www.sorbonne-universite.fr	www.sorbonne-universite.fr	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
STANISLAS	STANISLAS	kA
<g/>
,	,	kIx,
Dehaene	Dehaen	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
La	la	k1gNnSc1
comparaison	comparaisona	k1gFnPc2
des	des	k1gNnSc2
petits	petits	k6eAd1
nombres	nombresa	k1gFnPc2
:	:	kIx,
représentation	représentation	k1gInSc1
analogique	analogiqu	k1gFnSc2
et	et	k?
processus	processus	k1gInSc1
attentionnels	attentionnels	k1gInSc1
<g/>
.	.	kIx.
,	,	kIx,
1989-01-01	1989-01-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
thesis	thesis	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
,	,	kIx,
EHESS	EHESS	kA
<g/>
.	.	kIx.
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://www.college-de-france.fr/media/stanislas-dehaene/UPL7639999902948674257_CV_StanPEHAENE_2016.pdf	https://www.college-de-france.fr/media/stanislas-dehaene/UPL7639999902948674257_CV_StanPEHAENE_2016.pdf	k1gMnSc1
<g/>
↑	↑	k?
Stanislas	Stanislas	k1gMnSc1
Dehaene	Dehaen	k1gInSc5
-	-	kIx~
Citations	Citations	k1gInSc1
Google	Google	k1gNnSc1
Scholar	Scholar	k1gInSc1
<g/>
.	.	kIx.
scholar	scholar	k1gInSc1
<g/>
.	.	kIx.
<g/>
google	google	k1gInSc1
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Relais	Relais	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
information	information	k1gInSc1
sur	sur	k?
les	les	k1gInSc1
sciences	sciences	k1gMnSc1
de	de	k?
la	la	k1gNnPc2
cognition	cognition	k1gInSc1
-	-	kIx~
Risc	Risc	k1gInSc1
-	-	kIx~
UMS	UMS	kA
3332	#num#	k4
CNRS-ENS	CNRS-ENS	k1gFnPc1
<g/>
.	.	kIx.
www.risc.cnrs.fr	www.risc.cnrs.fr	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
UNIAD	UNIAD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNICOG	UNICOG	kA
-	-	kIx~
Cognitive	Cognitiv	k1gInSc5
Neuroimaging	Neuroimaging	k1gInSc4
Lab	Lab	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Il	Il	k1gMnPc4
démystifie	démystifie	k1gFnSc2
la	la	k1gNnSc2
bosse	boss	k1gMnSc2
des	des	k1gNnPc2
maths	maths	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanislas	Stanislas	k1gInSc1
Dehaene	Dehaen	k1gInSc5
<g/>
,	,	kIx,
32	#num#	k4
ans	ans	k?
<g/>
,	,	kIx,
est	est	k?
neuropsychologue	neuropsychologue	k1gInSc1
et	et	k?
mathématicien	mathématicien	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Il	Il	k1gFnSc1
visualise	visualise	k1gFnSc1
le	le	k?
cerveau	cerveau	k5eAaPmIp1nS
en	en	k?
plein	plein	k1gInSc1
calcul	calcul	k1gInSc1
et	et	k?
identifie	identifie	k1gFnSc1
les	les	k1gInSc1
neurones	neurones	k1gInSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
algè	algè	k1gInSc5
<g/>
..	..	k?
Libération	Libération	k1gInSc4
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1997-04-15	1997-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Cécile	Cécila	k1gFnSc3
Guérin	Guérin	k1gInSc4
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Stanislas	Stanislas	k1gInSc1
Dehaene	Dehaen	k1gInSc5
<g/>
,	,	kIx,
šéf	šéf	k1gMnSc1
matematiky	matematika	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Le	Le	k1gMnSc5
Monde	Mond	k1gMnSc5
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1999	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nominace	nominace	k1gFnSc1
na	na	k7c6
internetových	internetový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
Svatého	svatý	k2eAgInSc2d1
stolce	stolec	k1gInSc2
<g/>
.	.	kIx.
212.77.1.245	212.77.1.245	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Osobní	osobní	k2eAgInSc4d1
list	list	k1gInSc4
na	na	k7c6
webové	webový	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
Collè	Collè	k1gMnSc2
de	de	k?
France	Franc	k1gMnSc2
<g/>
.	.	kIx.
www.college-de-france.fr	www.college-de-france.fr	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
PREX	PREX	kA
<g/>
1032962	#num#	k4
<g/>
D	D	kA
<g/>
,	,	kIx,
Décret	Décret	k1gInSc1
du	du	k?
31	#num#	k4
décembre	décembr	k1gInSc5
2010	#num#	k4
portant	portant	k1gMnSc1
promotion	promotion	k1gInSc1
et	et	k?
nomination	nomination	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
«	«	k?
Le	Le	k1gMnSc4
cerveau	cerveaa	k1gMnSc4
est	est	k?
préorganisé	préorganisý	k2eAgNnSc1d1
pour	pour	k1gInSc1
la	la	k0
lecture	lectur	k1gMnSc5
»	»	k?
<g/>
.	.	kIx.
www.lemonde.fr	www.lemonde.fr	k1gMnSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
"	"	kIx"
<g/>
Numbers	Numbers	k1gInSc1
Guy	Guy	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
článek	článek	k1gInSc1
New	New	k1gFnSc2
Yorker	Yorkra	k1gFnPc2
z	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
věnovaný	věnovaný	k2eAgInSc1d1
Dehaenovi	Dehaenův	k2eAgMnPc1d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc3
tvorbě	tvorba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Denis	Denisa	k1gFnPc2
Sergent	Sergent	k1gMnSc1
"	"	kIx"
Stanislas	Stanislas	k1gMnSc1
Dehaene	Dehaen	k1gMnSc5
<g/>
,	,	kIx,
un	un	k?
touche-à	touche-à	k1gMnSc1
des	des	k1gNnSc1
neurosciences	neurosciences	k1gMnSc1
"	"	kIx"
<g/>
,	,	kIx,
La	la	k1gNnSc1
Croix	Croix	k1gInSc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Stanislas	Stanislasa	k1gFnPc2
Dehaene	Dehaen	k1gInSc5
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
130199133	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2141	#num#	k4
0584	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
93007066	#num#	k4
|	|	kIx~
ORCID	ORCID	kA
<g/>
:	:	kIx,
0000-0002-7418-8275	0000-0002-7418-8275	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
79101210	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
93007066	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Medicína	medicína	k1gFnSc1
</s>
