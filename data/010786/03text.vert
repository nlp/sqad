<p>
<s>
Santiago	Santiago	k1gNnSc1	Santiago
"	"	kIx"	"
<g/>
Santi	Santi	k1gNnSc1	Santi
<g/>
"	"	kIx"	"
Cazorla	Cazorla	k1gMnSc1	Cazorla
González	González	k1gMnSc1	González
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Llanera	Llaner	k1gMnSc2	Llaner
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
španělský	španělský	k2eAgMnSc1d1	španělský
fotbalista	fotbalista	k1gMnSc1	fotbalista
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hraje	hrát	k5eAaImIp3nS	hrát
za	za	k7c4	za
španělský	španělský	k2eAgInSc4d1	španělský
Villarreal	Villarreal	k1gInSc4	Villarreal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klubová	klubový	k2eAgFnSc1d1	klubová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
kariérou	kariéra	k1gFnSc7	kariéra
začínal	začínat	k5eAaImAgInS	začínat
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Realu	Real	k1gInSc2	Real
Oviedo	Oviedo	k1gNnSc1	Oviedo
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
svými	svůj	k3xOyFgFnPc7	svůj
osmnáctými	osmnáctý	k4xOgFnPc7	osmnáctý
narozeninami	narozeniny	k1gFnPc7	narozeniny
přestoupil	přestoupit	k5eAaPmAgInS	přestoupit
do	do	k7c2	do
Villarrealu	Villarreal	k1gInSc2	Villarreal
<g/>
.	.	kIx.	.
</s>
<s>
Začátky	začátek	k1gInPc1	začátek
svého	svůj	k3xOyFgNnSc2	svůj
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
působení	působení	k1gNnSc2	působení
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
rezervě	rezerva	k1gFnSc6	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
tým	tým	k1gInSc4	tým
debutoval	debutovat	k5eAaBmAgMnS	debutovat
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k8xC	jako
střídající	střídající	k2eAgMnSc1d1	střídající
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
poslední	poslední	k2eAgFnSc4d1	poslední
minutu	minuta	k1gFnSc4	minuta
utkání	utkání	k1gNnPc2	utkání
proti	proti	k7c3	proti
Deportivu	Deportivum	k1gNnSc3	Deportivum
La	la	k1gNnSc2	la
Coruñ	Coruñ	k1gFnSc2	Coruñ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
týmu	tým	k1gInSc6	tým
se	se	k3xPyFc4	se
výrazněji	výrazně	k6eAd2	výrazně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
s	s	k7c7	s
Villarrealem	Villarreal	k1gInSc7	Villarreal
Pohár	pohár	k1gInSc1	pohár
Intertoto	Intertota	k1gFnSc5	Intertota
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
si	se	k3xPyFc3	se
klub	klub	k1gInSc1	klub
zajistil	zajistit	k5eAaPmAgInS	zajistit
postup	postup	k1gInSc4	postup
do	do	k7c2	do
Poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
za	za	k7c4	za
390	[number]	k4	390
000	[number]	k4	000
eur	euro	k1gNnPc2	euro
do	do	k7c2	do
celku	celek	k1gInSc2	celek
Recreativa	Recreativum	k1gNnSc2	Recreativum
de	de	k?	de
Huelva	Huelva	k1gFnSc1	Huelva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podepsal	podepsat	k5eAaPmAgMnS	podepsat
čtyřletou	čtyřletý	k2eAgFnSc4d1	čtyřletá
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
klauzulí	klauzule	k1gFnSc7	klauzule
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
Villarrealu	Villarreala	k1gFnSc4	Villarreala
Cazorlu	Cazorl	k1gInSc2	Cazorl
kdykoliv	kdykoliv	k6eAd1	kdykoliv
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
za	za	k7c4	za
částku	částka	k1gFnSc4	částka
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Santi	Santi	k1gNnSc6	Santi
debutoval	debutovat	k5eAaBmAgMnS	debutovat
za	za	k7c4	za
Huelvu	Huelva	k1gFnSc4	Huelva
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Mallorcou	Mallorca	k1gFnSc7	Mallorca
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
gólem	gól	k1gInSc7	gól
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
ke	k	k7c3	k
konečné	konečný	k2eAgFnSc3d1	konečná
remíze	remíza	k1gFnSc3	remíza
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
výkony	výkon	k1gInPc7	výkon
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
nováčkovi	nováček	k1gMnSc3	nováček
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
k	k	k7c3	k
umístění	umístění	k1gNnSc3	umístění
na	na	k7c6	na
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
španělským	španělský	k2eAgMnSc7d1	španělský
hráčem	hráč	k1gMnSc7	hráč
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
sezoně	sezona	k1gFnSc6	sezona
strávené	strávený	k2eAgFnSc2d1	strávená
v	v	k7c6	v
Huelvě	Huelva	k1gFnSc6	Huelva
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Villarrealu	Villarreal	k1gInSc2	Villarreal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
využil	využít	k5eAaPmAgInS	využít
oné	onen	k3xDgFnSc3	onen
výstupní	výstupní	k2eAgFnSc1d1	výstupní
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
sezoně	sezona	k1gFnSc6	sezona
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
návratu	návrat	k1gInSc6	návrat
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
Villarreal	Villarreal	k1gInSc1	Villarreal
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
La	la	k1gNnSc6	la
Lize	liga	k1gFnSc6	liga
a	a	k8xC	a
Cazorla	Cazorla	k1gFnSc1	Cazorla
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
podílel	podílet	k5eAaImAgMnS	podílet
pěti	pět	k4xCc7	pět
góly	gól	k1gInPc7	gól
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
asistencí	asistence	k1gFnSc7	asistence
pro	pro	k7c4	pro
útočníky	útočník	k1gMnPc4	útočník
Nihata	Nihat	k2eAgFnSc1d1	Nihat
Kahveciho	Kahveciha	k1gMnSc5	Kahveciha
a	a	k8xC	a
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Rossiho	Rossiha	k1gMnSc5	Rossiha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2009	[number]	k4	2009
si	se	k3xPyFc3	se
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Almeríi	Almerí	k1gInSc3	Almerí
zlomil	zlomit	k5eAaPmAgMnS	zlomit
lýtkovou	lýtkový	k2eAgFnSc4d1	lýtková
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zranění	zranění	k1gNnPc2	zranění
se	se	k3xPyFc4	se
zotavil	zotavit	k5eAaPmAgMnS	zotavit
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
stihl	stihnout	k5eAaPmAgMnS	stihnout
poslední	poslední	k2eAgInSc4d1	poslední
zápas	zápas	k1gInSc4	zápas
sezony	sezona	k1gFnSc2	sezona
proti	proti	k7c3	proti
Mallorce	Mallorka	k1gFnSc3	Mallorka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezoně	sezona	k1gFnSc6	sezona
2009-10	[number]	k4	2009-10
bojoval	bojovat	k5eAaImAgInS	bojovat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
zranění	zranění	k1gNnPc2	zranění
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gNnSc4	on
nakonec	nakonec	k6eAd1	nakonec
vyřadilo	vyřadit	k5eAaPmAgNnS	vyřadit
z	z	k7c2	z
nominace	nominace	k1gFnSc2	nominace
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
za	za	k7c4	za
21	[number]	k4	21
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
do	do	k7c2	do
Málagy	Málaga	k1gFnSc2	Málaga
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Villarreal	Villarreal	k1gMnSc1	Villarreal
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
prodat	prodat	k5eAaPmF	prodat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
potíží	potíž	k1gFnPc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Cazorla	Cazorla	k1gMnSc1	Cazorla
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
za	za	k7c4	za
Málagu	Málaga	k1gFnSc4	Málaga
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
debutu	debut	k1gInSc6	debut
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
proti	proti	k7c3	proti
Seville	Sevilla	k1gFnSc3	Sevilla
<g/>
.	.	kIx.	.
</s>
<s>
Santi	Santi	k1gNnSc7	Santi
zakončil	zakončit	k5eAaPmAgInS	zakončit
sezónu	sezóna	k1gFnSc4	sezóna
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
konto	konto	k1gNnSc4	konto
připsal	připsat	k5eAaPmAgMnS	připsat
devět	devět	k4xCc4	devět
ligových	ligový	k2eAgFnPc2d1	ligová
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Málaga	Málag	k1gMnSc4	Málag
skončila	skončit	k5eAaPmAgFnS	skončit
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
do	do	k7c2	do
Ligy	liga	k1gFnSc2	liga
Mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
představen	představit	k5eAaPmNgInS	představit
jako	jako	k8xC	jako
nová	nový	k2eAgFnSc1d1	nová
posila	posila	k1gFnSc1	posila
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Přestupová	přestupový	k2eAgFnSc1d1	přestupová
částka	částka	k1gFnSc1	částka
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
pod	pod	k7c4	pod
15	[number]	k4	15
miliónů	milión	k4xCgInPc2	milión
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Santi	Santi	k1gNnSc7	Santi
Cazorla	Cazorl	k1gMnSc2	Cazorl
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
seniorské	seniorský	k2eAgFnSc2d1	seniorská
španělské	španělský	k2eAgFnSc2d1	španělská
reprezentace	reprezentace	k1gFnSc2	reprezentace
poprvé	poprvé	k6eAd1	poprvé
povolán	povolat	k5eAaPmNgInS	povolat
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
trenér	trenér	k1gMnSc1	trenér
týmu	tým	k1gInSc2	tým
Luis	Luisa	k1gFnPc2	Luisa
Aragonés	Aragonésa	k1gFnPc2	Aragonésa
zařadil	zařadit	k5eAaPmAgInS	zařadit
do	do	k7c2	do
nominace	nominace	k1gFnSc2	nominace
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2008	[number]	k4	2008
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tým	tým	k1gInSc4	tým
debutoval	debutovat	k5eAaBmAgMnS	debutovat
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
v	v	k7c6	v
přípravném	přípravný	k2eAgNnSc6d1	přípravné
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Peru	Peru	k1gNnSc3	Peru
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
šampionátu	šampionát	k1gInSc6	šampionát
se	se	k3xPyFc4	se
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
dostal	dostat	k5eAaPmAgMnS	dostat
vždy	vždy	k6eAd1	vždy
jako	jako	k8xC	jako
střídající	střídající	k2eAgMnSc1d1	střídající
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
proti	proti	k7c3	proti
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
proměnil	proměnit	k5eAaPmAgMnS	proměnit
svůj	svůj	k3xOyFgInSc4	svůj
pokus	pokus	k1gInSc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vítězném	vítězný	k2eAgInSc6d1	vítězný
finálovém	finálový	k2eAgInSc6d1	finálový
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
posledních	poslední	k2eAgFnPc2d1	poslední
25	[number]	k4	25
minut	minuta	k1gFnPc2	minuta
<g/>
.19	.19	k4	.19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Chile	Chile	k1gNnSc3	Chile
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
reprezentační	reprezentační	k2eAgInSc4d1	reprezentační
gól	gól	k1gInSc4	gól
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
k	k	k7c3	k
výhře	výhra	k1gFnSc3	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
2010	[number]	k4	2010
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
vynechal	vynechat	k5eAaPmAgInS	vynechat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
operace	operace	k1gFnSc2	operace
kýly	kýla	k1gFnSc2	kýla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
opět	opět	k6eAd1	opět
součástí	součást	k1gFnSc7	součást
španělského	španělský	k2eAgInSc2d1	španělský
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obhájil	obhájit	k5eAaPmAgInS	obhájit
své	svůj	k3xOyFgNnSc4	svůj
vítězství	vítězství	k1gNnSc4	vítězství
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Cazorla	Cazorla	k1gMnSc1	Cazorla
poprvé	poprvé	k6eAd1	poprvé
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k8xS	jako
střídající	střídající	k2eAgMnSc1d1	střídající
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Irsku	Irsko	k1gNnSc3	Irsko
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
<g/>
.	.	kIx.	.
<g/>
Trenér	trenér	k1gMnSc1	trenér
Vicente	Vicent	k1gInSc5	Vicent
del	del	k?	del
Bosque	Bosqu	k1gFnSc2	Bosqu
jej	on	k3xPp3gMnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2014	[number]	k4	2014
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Španělé	Španěl	k1gMnPc1	Španěl
jakožto	jakožto	k8xS	jakožto
obhájci	obhájce	k1gMnPc1	obhájce
titulu	titul	k1gInSc2	titul
vypadli	vypadnout	k5eAaPmAgMnP	vypadnout
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
porážkách	porážka	k1gFnPc6	porážka
a	a	k8xC	a
jedné	jeden	k4xCgFnSc3	jeden
výhře	výhra	k1gFnSc3	výhra
již	již	k9	již
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
B.	B.	kA	B.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Klubové	klubový	k2eAgMnPc4d1	klubový
===	===	k?	===
</s>
</p>
<p>
<s>
Villarreal	Villarreal	k1gMnSc1	Villarreal
</s>
</p>
<p>
<s>
Pohár	pohár	k1gInSc1	pohár
Intertoto	Intertota	k1gFnSc5	Intertota
<g/>
:	:	kIx,	:
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
Arsenal	Arsenal	k1gFnPc2	Arsenal
</s>
</p>
<p>
<s>
FA	fa	k1gNnSc1	fa
Cup	cup	k1gInSc1	cup
<g/>
:	:	kIx,	:
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
</s>
</p>
<p>
<s>
Community	Communit	k1gInPc1	Communit
Shield	Shielda	k1gFnPc2	Shielda
<g/>
:	:	kIx,	:
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
===	===	k?	===
Reprezentační	reprezentační	k2eAgMnSc1d1	reprezentační
===	===	k?	===
</s>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
:	:	kIx,	:
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
===	===	k?	===
Individuální	individuální	k2eAgFnSc1d1	individuální
===	===	k?	===
</s>
</p>
<p>
<s>
Primera	primera	k1gFnSc1	primera
División	División	k1gMnSc1	División
-	-	kIx~	-
Španělský	španělský	k2eAgMnSc1d1	španělský
fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Arsenal	Arsenat	k5eAaPmAgMnS	Arsenat
FC	FC	kA	FC
-	-	kIx~	-
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hráč	hráč	k1gMnSc1	hráč
sezóny	sezóna	k1gFnSc2	sezóna
:	:	kIx,	:
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
</s>
</p>
<p>
<s>
==	==	k?	==
Statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Klubové	klubový	k2eAgMnPc4d1	klubový
===	===	k?	===
</s>
</p>
<p>
<s>
Aktualizováno	aktualizovat	k5eAaBmNgNnS	aktualizovat
k	k	k7c3	k
14	[number]	k4	14
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Santi	Santi	k1gNnSc2	Santi
Cazorla	Cazorlo	k1gNnSc2	Cazorlo
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Santi	Sanť	k1gFnSc2	Sanť
Cazorla	Cazorlo	k1gNnSc2	Cazorlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Transfermarkt	Transfermarkt	k1gInSc1	Transfermarkt
profil	profil	k1gInSc1	profil
</s>
</p>
<p>
<s>
Topforward	Topforward	k1gInSc1	Topforward
profil	profil	k1gInSc1	profil
</s>
</p>
