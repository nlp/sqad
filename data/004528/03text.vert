<s>
Víno	víno	k1gNnSc1	víno
je	být	k5eAaImIp3nS	být
alkoholický	alkoholický	k2eAgMnSc1d1	alkoholický
a	a	k8xC	a
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
také	také	k9	také
nealkoholický	alkoholický	k2eNgInSc1d1	nealkoholický
nápoj	nápoj	k1gInSc4	nápoj
typicky	typicky	k6eAd1	typicky
vznikající	vznikající	k2eAgFnSc1d1	vznikající
kvašením	kvašení	k1gNnSc7	kvašení
moštu	mošt	k1gInSc2	mošt
z	z	k7c2	z
plodů	plod	k1gInPc2	plod
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
názvy	název	k1gInPc1	název
vína	víno	k1gNnSc2	víno
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
názvu	název	k1gInSc2	název
vína	víno	k1gNnSc2	víno
vinum	vinum	k1gNnSc1	vinum
<g/>
.	.	kIx.	.
</s>
<s>
Vědecky	vědecky	k6eAd1	vědecky
se	se	k3xPyFc4	se
vínem	víno	k1gNnSc7	víno
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
výrobou	výroba	k1gFnSc7	výroba
(	(	kIx(	(
<g/>
vinařstvím	vinařství	k1gNnSc7	vinařství
<g/>
)	)	kIx)	)
a	a	k8xC	a
pěstováním	pěstování	k1gNnSc7	pěstování
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
(	(	kIx(	(
<g/>
vinohradnictvím	vinohradnictví	k1gNnSc7	vinohradnictví
<g/>
)	)	kIx)	)
zabývá	zabývat	k5eAaImIp3nS	zabývat
enologie	enologie	k1gFnSc1	enologie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Itálie	Itálie	k1gFnSc2	Itálie
se	se	k3xPyFc4	se
pilo	pít	k5eAaImAgNnS	pít
víno	víno	k1gNnSc1	víno
už	už	k6eAd1	už
před	před	k7c7	před
téměř	téměř	k6eAd1	téměř
6000	[number]	k4	6000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Členění	členění	k1gNnSc1	členění
vína	víno	k1gNnSc2	víno
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zavedené	zavedený	k2eAgFnPc1d1	zavedená
tři	tři	k4xCgFnPc1	tři
barvy	barva	k1gFnPc1	barva
<g/>
:	:	kIx,	:
bílé	bílý	k2eAgNnSc4d1	bílé
<g/>
,	,	kIx,	,
růžové	růžový	k2eAgNnSc4d1	růžové
a	a	k8xC	a
červené	červený	k2eAgNnSc4d1	červené
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgNnSc1d1	bílé
víno	víno	k1gNnSc1	víno
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
bílých	bílý	k2eAgInPc2d1	bílý
<g/>
,	,	kIx,	,
růžových	růžový	k2eAgInPc2d1	růžový
<g/>
,	,	kIx,	,
červených	červený	k2eAgInPc2d1	červený
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
modrých	modrý	k2eAgInPc2d1	modrý
hroznů	hrozen	k1gInPc2	hrozen
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgInPc1d1	vinný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
rmut	rmut	k1gInSc1	rmut
(	(	kIx(	(
<g/>
narušené	narušený	k2eAgFnSc2d1	narušená
slupky	slupka	k1gFnSc2	slupka
hroznů	hrozen	k1gInPc2	hrozen
<g/>
)	)	kIx)	)
ihned	ihned	k6eAd1	ihned
lisuje	lisovat	k5eAaImIp3nS	lisovat
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
čistý	čistý	k2eAgInSc1d1	čistý
mošt	mošt	k1gInSc1	mošt
ke	k	k7c3	k
kvašení	kvašení	k1gNnSc3	kvašení
<g/>
.	.	kIx.	.
</s>
<s>
Pevné	pevný	k2eAgInPc1d1	pevný
zbytky	zbytek	k1gInPc1	zbytek
po	po	k7c6	po
lisování	lisování	k1gNnSc6	lisování
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
matoliny	matoliny	k1gFnPc1	matoliny
<g/>
.	.	kIx.	.
</s>
<s>
Bílým	bílý	k2eAgInSc7d1	bílý
vínům	víno	k1gNnPc3	víno
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
z	z	k7c2	z
červených	červená	k1gFnPc2	červená
nebo	nebo	k8xC	nebo
modrých	modrý	k2eAgInPc2d1	modrý
hroznů	hrozen	k1gInPc2	hrozen
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
klaret	klaret	k1gInSc1	klaret
<g/>
.	.	kIx.	.
</s>
<s>
Růžové	růžový	k2eAgNnSc1d1	růžové
víno	víno	k1gNnSc1	víno
(	(	kIx(	(
<g/>
rosé	rosá	k1gFnPc1	rosá
<g/>
)	)	kIx)	)
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
modrých	modrý	k2eAgInPc2d1	modrý
hroznů	hrozen	k1gInPc2	hrozen
bez	bez	k7c2	bez
nakvášení	nakvášení	k1gNnSc2	nakvášení
<g/>
,	,	kIx,	,
u	u	k7c2	u
stolních	stolní	k2eAgFnPc2d1	stolní
vín	vína	k1gFnPc2	vína
šumivých	šumivý	k2eAgNnPc2d1	šumivé
a	a	k8xC	a
perlivých	perlivý	k2eAgNnPc2d1	perlivé
vín	víno	k1gNnPc2	víno
i	i	k9	i
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
bílého	bílé	k1gNnSc2	bílé
a	a	k8xC	a
červeného	červený	k2eAgNnSc2d1	červené
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgNnSc4d1	Červené
víno	víno	k1gNnSc4	víno
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
modrých	modrý	k2eAgInPc2d1	modrý
hroznů	hrozen	k1gInPc2	hrozen
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
červené	červený	k2eAgNnSc1d1	červené
barvivo	barvivo	k1gNnSc1	barvivo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
odrůdách	odrůda	k1gFnPc6	odrůda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nakvášením	nakvášení	k1gNnSc7	nakvášení
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc7	jejich
tepelným	tepelný	k2eAgNnSc7d1	tepelné
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
rmut	rmut	k1gInSc1	rmut
nechá	nechat	k5eAaPmIp3nS	nechat
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
kvasit	kvasit	k5eAaImF	kvasit
<g/>
.	.	kIx.	.
</s>
<s>
Slupky	slupka	k1gFnPc1	slupka
tak	tak	k9	tak
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
kvasící	kvasící	k2eAgFnSc7d1	kvasící
šťávou	šťáva	k1gFnSc7	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Kvašení	kvašení	k1gNnSc1	kvašení
probíhá	probíhat	k5eAaImIp3nS	probíhat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
za	za	k7c4	za
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
než	než	k8xS	než
u	u	k7c2	u
bílého	bílý	k2eAgNnSc2d1	bílé
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červeném	červený	k2eAgNnSc6d1	červené
vínu	víno	k1gNnSc6	víno
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
ve	v	k7c6	v
vyšší	vysoký	k2eAgFnSc6d2	vyšší
míře	míra	k1gFnSc6	míra
třísloviny	tříslovina	k1gFnSc2	tříslovina
a	a	k8xC	a
také	také	k9	také
resveratrol	resveratrol	k1gInSc1	resveratrol
(	(	kIx(	(
<g/>
ze	z	k7c2	z
slupek	slupka	k1gFnPc2	slupka
i	i	k9	i
z	z	k7c2	z
jadérek	jadérko	k1gNnPc2	jadérko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
bílém	bílý	k2eAgNnSc6d1	bílé
víně	víno	k1gNnSc6	víno
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
decilitry	decilitr	k1gInPc1	decilitr
červeného	červený	k2eAgNnSc2d1	červené
vína	víno	k1gNnSc2	víno
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přibližně	přibližně	k6eAd1	přibližně
600	[number]	k4	600
mikrogramů	mikrogram	k1gInPc2	mikrogram
resveratrolu	resveratrol	k1gInSc2	resveratrol
<g/>
.	.	kIx.	.
</s>
<s>
Oranžové	oranžový	k2eAgNnSc1d1	oranžové
víno	víno	k1gNnSc1	víno
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
bílé	bílý	k2eAgNnSc1d1	bílé
víno	víno	k1gNnSc1	víno
zrající	zrající	k2eAgNnSc1d1	zrající
na	na	k7c6	na
slupkách	slupka	k1gFnPc6	slupka
<g/>
.	.	kIx.	.
</s>
<s>
Cukernatost	cukernatost	k1gFnSc1	cukernatost
vína	víno	k1gNnSc2	víno
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
normalizovaného	normalizovaný	k2eAgInSc2d1	normalizovaný
moštoměru	moštoměr	k1gInSc2	moštoměr
(	(	kIx(	(
<g/>
°	°	k?	°
<g/>
NM	NM	kA	NM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vín	víno	k1gNnPc2	víno
<g/>
:	:	kIx,	:
suché	suchý	k2eAgNnSc1d1	suché
<g/>
:	:	kIx,	:
nejvýše	nejvýše	k6eAd1	nejvýše
4	[number]	k4	4
g	g	kA	g
zbytkového	zbytkový	k2eAgInSc2d1	zbytkový
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc1	litr
polosuché	polosuchý	k2eAgFnSc2d1	polosuchá
<g/>
:	:	kIx,	:
4,1	[number]	k4	4,1
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
g	g	kA	g
zbytkového	zbytkový	k2eAgInSc2d1	zbytkový
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc1	litr
polosladké	polosladká	k1gFnSc2	polosladká
<g/>
:	:	kIx,	:
12,1	[number]	k4	12,1
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
g	g	kA	g
zbytkového	zbytkový	k2eAgInSc2d1	zbytkový
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc4	litr
sladké	sladký	k2eAgFnPc1d1	sladká
<g/>
:	:	kIx,	:
minimální	minimální	k2eAgInSc1d1	minimální
obsah	obsah	k1gInSc1	obsah
45	[number]	k4	45
g	g	kA	g
zbytkového	zbytkový	k2eAgInSc2d1	zbytkový
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
litr	litr	k1gInSc1	litr
U	u	k7c2	u
sektů	sekt	k1gInPc2	sekt
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
brut	brut	k1gInSc1	brut
nature	natur	k1gMnSc5	natur
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
přírodně	přírodně	k6eAd1	přírodně
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
méně	málo	k6eAd2	málo
než	než	k8xS	než
3	[number]	k4	3
g	g	kA	g
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc1	litr
(	(	kIx(	(
<g/>
cukr	cukr	k1gInSc1	cukr
nebyl	být	k5eNaImAgInS	být
dodán	dodat	k5eAaPmNgInS	dodat
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
extra	extra	k2eAgInSc1d1	extra
brut	brut	k1gInSc1	brut
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
zvláště	zvláště	k6eAd1	zvláště
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
0	[number]	k4	0
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
6	[number]	k4	6
g	g	kA	g
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc1	litr
"	"	kIx"	"
<g/>
brut	brut	k5eAaPmF	brut
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
obsah	obsah	k1gInSc1	obsah
cukru	cukr	k1gInSc2	cukr
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
12	[number]	k4	12
g	g	kA	g
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc1	litr
"	"	kIx"	"
<g/>
extra	extra	k2eAgInSc1d1	extra
sec	sec	k1gInSc1	sec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
extra	extra	k6eAd1	extra
dry	dry	k?	dry
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
zvláště	zvláště	k6eAd1	zvláště
suché	suchý	k2eAgInPc1d1	suchý
<g/>
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
g	g	kA	g
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc1	litr
"	"	kIx"	"
<g/>
sec	sec	kA	sec
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
suché	suchý	k2eAgFnPc1d1	suchá
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
g	g	kA	g
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc1	litr
"	"	kIx"	"
<g/>
demi-sec	demiec	k1gInSc1	demi-sec
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
polosuché	polosuchý	k2eAgNnSc1d1	polosuché
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
od	od	k7c2	od
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
g	g	kA	g
<g />
.	.	kIx.	.
</s>
<s>
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc1	litr
"	"	kIx"	"
<g/>
doux	doux	k1gInSc1	doux
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
sladké	sladký	k2eAgFnPc1d1	sladká
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
g	g	kA	g
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc1	litr
Víno	víno	k1gNnSc1	víno
Víno	víno	k1gNnSc1	víno
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Stolní	stolní	k2eAgNnSc1d1	stolní
víno	víno	k1gNnSc1	víno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
sklizených	sklizený	k2eAgInPc2d1	sklizený
na	na	k7c4	na
území	území	k1gNnSc4	území
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
státu	stát	k1gInSc2	stát
EU	EU	kA	EU
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
nejméně	málo	k6eAd3	málo
11	[number]	k4	11
stupňů	stupeň	k1gInPc2	stupeň
cukernatosti	cukernatost	k1gFnSc2	cukernatost
(	(	kIx(	(
<g/>
hrozny	hrozen	k1gInPc1	hrozen
s	s	k7c7	s
cukernatostí	cukernatost	k1gFnSc7	cukernatost
10	[number]	k4	10
stupňů	stupeň	k1gInPc2	stupeň
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zpracovány	zpracovat	k5eAaPmNgInP	zpracovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
povolení	povolení	k1gNnSc2	povolení
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rmutu	rmut	k1gInSc2	rmut
<g/>
,	,	kIx,	,
moštu	mošt	k1gInSc2	mošt
či	či	k8xC	či
vína	víno	k1gNnSc2	víno
získaných	získaný	k2eAgInPc2d1	získaný
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
odrůd	odrůda	k1gFnPc2	odrůda
moštových	moštový	k2eAgInPc2d1	moštový
a	a	k8xC	a
odrůd	odrůda	k1gFnPc2	odrůda
registrovaných	registrovaný	k2eAgFnPc2d1	registrovaná
jako	jako	k9	jako
stolní	stolní	k2eAgInSc4d1	stolní
–	–	k?	–
i	i	k8xC	i
z	z	k7c2	z
dovozu	dovoz	k1gInSc2	dovoz
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zpracování	zpracování	k1gNnSc3	zpracování
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
také	také	k9	také
hrozny	hrozen	k1gInPc4	hrozen
neregistrovaných	registrovaný	k2eNgFnPc2d1	neregistrovaná
odrůd	odrůda	k1gFnPc2	odrůda
vysazených	vysazený	k2eAgFnPc2d1	vysazená
před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Víno	víno	k1gNnSc1	víno
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
označováno	označovat	k5eAaImNgNnS	označovat
názvem	název	k1gInSc7	název
odrůdy	odrůda	k1gFnSc2	odrůda
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
zastoupení	zastoupení	k1gNnSc4	zastoupení
jedné	jeden	k4xCgFnSc2	jeden
odrůdy	odrůda	k1gFnSc2	odrůda
alespoň	alespoň	k9	alespoň
85	[number]	k4	85
%	%	kIx~	%
<g/>
)	)	kIx)	)
ani	ani	k8xC	ani
názvem	název	k1gInSc7	název
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
alespoň	alespoň	k9	alespoň
85	[number]	k4	85
%	%	kIx~	%
nepochází	pocházet	k5eNaImIp3nS	pocházet
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
oblasti	oblast	k1gFnSc2	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
"	"	kIx"	"
<g/>
stolního	stolní	k2eAgNnSc2d1	stolní
vína	víno	k1gNnSc2	víno
<g/>
"	"	kIx"	"
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
<g/>
.	.	kIx.	.
</s>
<s>
Zemské	zemský	k2eAgNnSc4d1	zemské
víno	víno	k1gNnSc4	víno
Druh	druh	k1gInSc4	druh
stolního	stolní	k2eAgNnSc2d1	stolní
vína	víno	k1gNnSc2	víno
vyrobeného	vyrobený	k2eAgNnSc2d1	vyrobené
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
sklizených	sklizený	k2eAgInPc2d1	sklizený
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
označováno	označovat	k5eAaImNgNnS	označovat
názvem	název	k1gInSc7	název
odrůdy	odrůda	k1gFnSc2	odrůda
či	či	k8xC	či
názvem	název	k1gInSc7	název
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Cukernatost	cukernatost	k1gFnSc1	cukernatost
zemského	zemský	k2eAgNnSc2d1	zemské
vína	víno	k1gNnSc2	víno
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
alespoň	alespoň	k9	alespoň
14	[number]	k4	14
<g/>
°	°	k?	°
<g/>
NM	NM	kA	NM
<g/>
.	.	kIx.	.
</s>
<s>
Jakostní	jakostní	k2eAgNnSc1d1	jakostní
víno	víno	k1gNnSc1	víno
Jakostní	jakostní	k2eAgNnSc1d1	jakostní
víno	víno	k1gNnSc1	víno
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
moštových	moštový	k2eAgFnPc2d1	moštová
odrůd	odrůda	k1gFnPc2	odrůda
sklizených	sklizený	k2eAgFnPc2d1	sklizená
ve	v	k7c6	v
vinařských	vinařský	k2eAgFnPc6d1	vinařská
oblastech	oblast	k1gFnPc6	oblast
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
nejméně	málo	k6eAd3	málo
15	[number]	k4	15
stupňů	stupeň	k1gInPc2	stupeň
cukernatosti	cukernatost	k1gFnSc2	cukernatost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
rmutu	rmut	k1gInSc2	rmut
případně	případně	k6eAd1	případně
moštu	mošt	k1gInSc2	mošt
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
hroznů	hrozen	k1gInPc2	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Hrozny	hrozen	k1gInPc1	hrozen
či	či	k8xC	či
mošt	mošt	k1gInSc1	mošt
pod	pod	k7c7	pod
19	[number]	k4	19
<g/>
°	°	k?	°
<g/>
NM	NM	kA	NM
se	se	k3xPyFc4	se
doslazují	doslazovat	k5eAaImIp3nP	doslazovat
obvykle	obvykle	k6eAd1	obvykle
řepným	řepný	k2eAgInSc7d1	řepný
cukrem	cukr	k1gInSc7	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Jakostní	jakostní	k2eAgNnSc1d1	jakostní
víno	víno	k1gNnSc1	víno
bývá	bývat	k5eAaImIp3nS	bývat
kromě	kromě	k7c2	kromě
základních	základní	k2eAgInPc2d1	základní
údajů	údaj	k1gInPc2	údaj
označeno	označit	k5eAaPmNgNnS	označit
názvem	název	k1gInSc7	název
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
byly	být	k5eAaImAgInP	být
sklizeny	sklizen	k2eAgInPc1d1	sklizen
hrozny	hrozen	k1gInPc1	hrozen
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyráběno	vyrábět	k5eAaImNgNnS	vyrábět
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
druzích	druh	k1gInPc6	druh
<g/>
:	:	kIx,	:
Odrůdové	odrůdový	k2eAgNnSc4d1	odrůdové
jakostní	jakostní	k2eAgNnSc4d1	jakostní
víno	víno	k1gNnSc4	víno
s	s	k7c7	s
určeným	určený	k2eAgInSc7d1	určený
názvem	název	k1gInSc7	název
odrůdy	odrůda	k1gFnSc2	odrůda
vína	víno	k1gNnSc2	víno
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
nejméně	málo	k6eAd3	málo
85	[number]	k4	85
%	%	kIx~	%
vína	víno	k1gNnSc2	víno
vyrobeného	vyrobený	k2eAgNnSc2d1	vyrobené
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
odrůdy	odrůda	k1gFnSc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Známkové	známkový	k2eAgNnSc1d1	známkové
jakostní	jakostní	k2eAgNnSc1d1	jakostní
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
vyráběno	vyráběn	k2eAgNnSc4d1	vyráběno
smísením	smísení	k1gNnSc7	smísení
odrůdových	odrůdový	k2eAgFnPc2d1	odrůdová
jakostních	jakostní	k2eAgFnPc2d1	jakostní
vín	vína	k1gFnPc2	vína
<g/>
.	.	kIx.	.
</s>
<s>
Víno	víno	k1gNnSc1	víno
s	s	k7c7	s
přívlastkem	přívlastek	k1gInSc7	přívlastek
Víno	víno	k1gNnSc1	víno
s	s	k7c7	s
přívlastkem	přívlastek	k1gInSc7	přívlastek
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
rmutu	rmut	k1gInSc2	rmut
nebo	nebo	k8xC	nebo
moštu	mošt	k1gInSc2	mošt
jedné	jeden	k4xCgFnSc2	jeden
moštové	moštový	k2eAgFnSc2d1	moštová
odrůdy	odrůda	k1gFnSc2	odrůda
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgInPc1d1	vinný
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
stanovené	stanovený	k2eAgInPc1d1	stanovený
pro	pro	k7c4	pro
vinařskou	vinařský	k2eAgFnSc4d1	vinařská
oblast	oblast	k1gFnSc4	oblast
prováděcím	prováděcí	k2eAgInSc7d1	prováděcí
právním	právní	k2eAgInSc7d1	právní
předpisem	předpis	k1gInSc7	předpis
<g/>
,	,	kIx,	,
sklizených	sklizený	k2eAgInPc2d1	sklizený
ve	v	k7c6	v
viničních	viniční	k2eAgFnPc6d1	viniční
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Víno	víno	k1gNnSc1	víno
s	s	k7c7	s
přívlastkem	přívlastek	k1gInSc7	přívlastek
bývá	bývat	k5eAaImIp3nS	bývat
navíc	navíc	k6eAd1	navíc
označováno	označovat	k5eAaImNgNnS	označovat
názvem	název	k1gInSc7	název
přívlastku	přívlastek	k1gInSc2	přívlastek
<g/>
,	,	kIx,	,
názvem	název	k1gInSc7	název
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
vinařské	vinařský	k2eAgFnSc2d1	vinařská
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
rokem	rok	k1gInSc7	rok
sklizně	sklizeň	k1gFnSc2	sklizeň
hroznů	hrozen	k1gInPc2	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Přívlastky	přívlastek	k1gInPc1	přívlastek
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
jednak	jednak	k8xC	jednak
přírodní	přírodní	k2eAgFnSc7d1	přírodní
cukernatostí	cukernatost	k1gFnSc7	cukernatost
hroznů	hrozen	k1gInPc2	hrozen
,	,	kIx,	,
případně	případně	k6eAd1	případně
moštu	mošt	k1gInSc2	mošt
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
dalšími	další	k2eAgFnPc7d1	další
okolnostmi	okolnost	k1gFnPc7	okolnost
zpracování	zpracování	k1gNnSc2	zpracování
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
výrobě	výroba	k1gFnSc6	výroba
<g/>
:	:	kIx,	:
Kabinet	kabinet	k1gInSc1	kabinet
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
nejméně	málo	k6eAd3	málo
19	[number]	k4	19
stupňů	stupeň	k1gInPc2	stupeň
přírodní	přírodní	k2eAgFnSc2d1	přírodní
cukernatosti	cukernatost	k1gFnSc2	cukernatost
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgInSc1d1	pozdní
sběr	sběr	k1gInSc1	sběr
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
nejméně	málo	k6eAd3	málo
21	[number]	k4	21
stupňů	stupeň	k1gInPc2	stupeň
přírodní	přírodní	k2eAgFnSc2d1	přírodní
cukernatosti	cukernatost	k1gFnSc2	cukernatost
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
nejméně	málo	k6eAd3	málo
24	[number]	k4	24
stupňů	stupeň	k1gInPc2	stupeň
přírodní	přírodní	k2eAgFnSc2d1	přírodní
cukernatosti	cukernatost	k1gFnSc2	cukernatost
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
bobulí	bobule	k1gFnPc2	bobule
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
nejméně	málo	k6eAd3	málo
27	[number]	k4	27
stupňů	stupeň	k1gInPc2	stupeň
přírodní	přírodní	k2eAgFnSc2d1	přírodní
cukernatosti	cukernatost	k1gFnSc2	cukernatost
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
cibéb	cibéba	k1gFnPc2	cibéba
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
nejméně	málo	k6eAd3	málo
32	[number]	k4	32
stupňů	stupeň	k1gInPc2	stupeň
přírodní	přírodní	k2eAgFnSc2d1	přírodní
cukernatosti	cukernatost	k1gFnSc2	cukernatost
<g/>
.	.	kIx.	.
</s>
<s>
Ledové	ledový	k2eAgNnSc1d1	ledové
víno	víno	k1gNnSc1	víno
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
sklizeny	sklidit	k5eAaPmNgInP	sklidit
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
minus	minus	k1gNnSc1	minus
7	[number]	k4	7
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
nižších	nízký	k2eAgMnPc2d2	nižší
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sklizně	sklizeň	k1gFnSc2	sklizeň
a	a	k8xC	a
zpracování	zpracování	k1gNnSc6	zpracování
zůstaly	zůstat	k5eAaPmAgInP	zůstat
zmrazeny	zmrazit	k5eAaPmNgInP	zmrazit
a	a	k8xC	a
získaný	získaný	k2eAgInSc1d1	získaný
mošt	mošt	k1gInSc1	mošt
vykazoval	vykazovat	k5eAaImAgInS	vykazovat
nejméně	málo	k6eAd3	málo
27	[number]	k4	27
stupňů	stupeň	k1gInPc2	stupeň
přírodní	přírodní	k2eAgFnSc2d1	přírodní
cukernatosti	cukernatost	k1gFnSc2	cukernatost
<g/>
.	.	kIx.	.
</s>
<s>
Slámové	Slámové	k2eAgNnSc1d1	Slámové
víno	víno	k1gNnSc1	víno
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
před	před	k7c7	před
zpracováním	zpracování	k1gNnSc7	zpracování
skladovány	skladovat	k5eAaImNgFnP	skladovat
na	na	k7c6	na
slámě	sláma	k1gFnSc6	sláma
či	či	k8xC	či
rákosu	rákos	k1gInSc6	rákos
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgInP	být
zavěšeny	zavěsit	k5eAaPmNgInP	zavěsit
ve	v	k7c6	v
větraném	větraný	k2eAgInSc6d1	větraný
prostoru	prostor	k1gInSc6	prostor
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
alespoň	alespoň	k9	alespoň
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
získaný	získaný	k2eAgInSc1d1	získaný
mošt	mošt	k1gInSc1	mošt
vykazoval	vykazovat	k5eAaImAgInS	vykazovat
nejméně	málo	k6eAd3	málo
27	[number]	k4	27
stupňů	stupeň	k1gInPc2	stupeň
přírodní	přírodní	k2eAgFnSc2d1	přírodní
cukernatosti	cukernatost	k1gFnSc2	cukernatost
<g/>
.	.	kIx.	.
</s>
<s>
Botrytický	Botrytický	k2eAgInSc1d1	Botrytický
výběr	výběr	k1gInSc1	výběr
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
napadených	napadený	k2eAgFnPc2d1	napadená
ušlechtilou	ušlechtilý	k2eAgFnSc7d1	ušlechtilá
plísní	plíseň	k1gFnSc7	plíseň
šedou	šedý	k2eAgFnSc7d1	šedá
<g/>
.	.	kIx.	.
</s>
<s>
Barrique	Barrique	k1gNnSc1	Barrique
<g/>
:	:	kIx,	:
víno	víno	k1gNnSc1	víno
zraje	zrát	k5eAaImIp3nS	zrát
v	v	k7c6	v
dubových	dubový	k2eAgInPc6d1	dubový
sudech	sud	k1gInPc6	sud
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
přejímá	přejímat	k5eAaImIp3nS	přejímat
jejich	jejich	k3xOp3gFnSc4	jejich
vůni	vůně	k1gFnSc4	vůně
Sur	Sur	k1gFnSc2	Sur
lie	lie	k?	lie
<g/>
:	:	kIx,	:
víno	víno	k1gNnSc1	víno
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nádobách	nádoba	k1gFnPc6	nádoba
na	na	k7c6	na
jemných	jemný	k2eAgFnPc6d1	jemná
kvasnicích	kvasnice	k1gFnPc6	kvasnice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
míchány	míchán	k2eAgFnPc1d1	míchána
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
víno	víno	k1gNnSc1	víno
za	za	k7c4	za
přírodní	přírodní	k2eAgNnSc4d1	přírodní
víno	víno	k1gNnSc4	víno
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
není	být	k5eNaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
ošetřováno	ošetřován	k2eAgNnSc1d1	ošetřováno
<g/>
.	.	kIx.	.
</s>
<s>
Mešní	mešní	k2eAgNnSc1d1	mešní
víno	víno	k1gNnSc1	víno
Mešní	mešní	k2eAgNnSc1d1	mešní
víno	víno	k1gNnSc1	víno
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
pečlivě	pečlivě	k6eAd1	pečlivě
sledované	sledovaný	k2eAgNnSc4d1	sledované
přírodní	přírodní	k2eAgNnSc4d1	přírodní
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
není	být	k5eNaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
ošetřováno	ošetřován	k2eAgNnSc1d1	ošetřováno
ani	ani	k8xC	ani
při	při	k7c6	při
růstu	růst	k1gInSc6	růst
a	a	k8xC	a
zrání	zrání	k1gNnSc6	zrání
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
použito	použit	k2eAgNnSc1d1	použito
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
chemie	chemie	k1gFnPc4	chemie
či	či	k8xC	či
aditiv	aditivum	k1gNnPc2	aditivum
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
výrobě	výroba	k1gFnSc6	výroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
mešní	mešní	k2eAgNnSc1d1	mešní
víno	víno	k1gNnSc1	víno
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
převážně	převážně	k6eAd1	převážně
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
klášterech	klášter	k1gInPc6	klášter
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
mše	mše	k1gFnSc2	mše
svaté	svatý	k2eAgFnSc2d1	svatá
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
víno	víno	k1gNnSc1	víno
pro	pro	k7c4	pro
transsubstanciaci	transsubstanciace	k1gFnSc4	transsubstanciace
v	v	k7c4	v
Krev	krev	k1gFnSc4	krev
Kristovu	Kristův	k2eAgFnSc4d1	Kristova
-	-	kIx~	-
Eucharistii	eucharistie	k1gFnSc4	eucharistie
<g/>
.	.	kIx.	.
</s>
<s>
Košer	košer	k2eAgNnSc1d1	košer
víno	víno	k1gNnSc1	víno
V	v	k7c6	v
případě	případ	k1gInSc6	případ
košer	košer	k2eAgNnSc2d1	košer
vína	víno	k1gNnSc2	víno
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
technologický	technologický	k2eAgInSc1d1	technologický
postup	postup	k1gInSc1	postup
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
příslušného	příslušný	k2eAgMnSc2d1	příslušný
představitele	představitel	k1gMnSc2	představitel
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Islám	islám	k1gInSc1	islám
alkohol	alkohol	k1gInSc1	alkohol
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
halal	halat	k5eAaPmAgMnS	halat
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
i	i	k9	i
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Aromatizované	aromatizovaný	k2eAgNnSc1d1	aromatizované
víno	víno	k1gNnSc1	víno
Aromatizované	aromatizovaný	k2eAgNnSc1d1	aromatizované
víno	víno	k1gNnSc1	víno
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
vína	víno	k1gNnSc2	víno
nebo	nebo	k8xC	nebo
hroznového	hroznový	k2eAgInSc2d1	hroznový
moštu	mošt	k1gInSc2	mošt
(	(	kIx(	(
<g/>
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
směsí	směs	k1gFnPc2	směs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
vody	voda	k1gFnSc2	voda
nejvýše	vysoce	k6eAd3	vysoce
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Aromatizují	aromatizovat	k5eAaImIp3nP	aromatizovat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
přírodních	přírodní	k2eAgFnPc2d1	přírodní
aromatických	aromatický	k2eAgFnPc2d1	aromatická
látek	látka	k1gFnPc2	látka
nebo	nebo	k8xC	nebo
povolených	povolený	k2eAgInPc2d1	povolený
aromatických	aromatický	k2eAgInPc2d1	aromatický
extraktů	extrakt	k1gInPc2	extrakt
<g/>
,	,	kIx,	,
aromatických	aromatický	k2eAgFnPc2d1	aromatická
bylin	bylina	k1gFnPc2	bylina
nebo	nebo	k8xC	nebo
koření	koření	k1gNnSc2	koření
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
také	také	k9	také
použít	použít	k5eAaPmF	použít
povolené	povolený	k2eAgFnPc4d1	povolená
přídatné	přídatný	k2eAgFnPc4d1	přídatná
ochucující	ochucující	k2eAgFnPc4d1	ochucující
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
doslazení	doslazení	k1gNnSc3	doslazení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
sacharóza	sacharóza	k1gFnSc1	sacharóza
<g/>
,	,	kIx,	,
hroznový	hroznový	k2eAgInSc1d1	hroznový
mošt	mošt	k1gInSc1	mošt
nebo	nebo	k8xC	nebo
zahuštěný	zahuštěný	k2eAgInSc1d1	zahuštěný
hroznový	hroznový	k2eAgInSc1d1	hroznový
mošt	mošt	k1gInSc1	mošt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
doalkoholizování	doalkoholizování	k1gNnSc3	doalkoholizování
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
přírodní	přírodní	k2eAgInSc4d1	přírodní
líh	líh	k1gInSc4	líh
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
skutečný	skutečný	k2eAgInSc1d1	skutečný
obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
výrobku	výrobek	k1gInSc6	výrobek
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
nejméně	málo	k6eAd3	málo
14,5	[number]	k4	14,5
%	%	kIx~	%
a	a	k8xC	a
nejvýše	vysoce	k6eAd3	vysoce
22	[number]	k4	22
%	%	kIx~	%
objemových	objemový	k2eAgFnPc2d1	objemová
<g/>
.	.	kIx.	.
</s>
<s>
Vermut	vermut	k1gInSc1	vermut
<g/>
:	:	kIx,	:
označení	označení	k1gNnSc1	označení
bývá	bývat	k5eAaImIp3nS	bývat
použito	použít	k5eAaPmNgNnS	použít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
aromatizace	aromatizace	k1gFnSc1	aromatizace
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
aromatickými	aromatický	k2eAgFnPc7d1	aromatická
látkami	látka	k1gFnPc7	látka
získanými	získaný	k2eAgFnPc7d1	získaná
z	z	k7c2	z
pelyňku	pelyněk	k1gInSc2	pelyněk
a	a	k8xC	a
přislazení	přislazení	k1gNnSc1	přislazení
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
pouze	pouze	k6eAd1	pouze
karamelizovaným	karamelizovaný	k2eAgInSc7d1	karamelizovaný
cukrem	cukr	k1gInSc7	cukr
<g/>
,	,	kIx,	,
sacharózou	sacharóza	k1gFnSc7	sacharóza
<g/>
,	,	kIx,	,
hroznovým	hroznový	k2eAgInSc7d1	hroznový
moštem	mošt	k1gInSc7	mošt
nebo	nebo	k8xC	nebo
zahuštěným	zahuštěný	k2eAgInSc7d1	zahuštěný
hroznovým	hroznový	k2eAgInSc7d1	hroznový
moštem	mošt	k1gInSc7	mošt
<g/>
.	.	kIx.	.
</s>
<s>
Americano	Americana	k1gFnSc5	Americana
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
hořké	hořký	k2eAgNnSc4d1	hořké
aromatizované	aromatizovaný	k2eAgNnSc4d1	aromatizované
víno	víno	k1gNnSc4	víno
vykazující	vykazující	k2eAgFnSc4d1	vykazující
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
hořkou	hořký	k2eAgFnSc4d1	hořká
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
aromatizaci	aromatizace	k1gFnSc3	aromatizace
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
přírodní	přírodní	k2eAgFnPc1d1	přírodní
látky	látka	k1gFnPc1	látka
z	z	k7c2	z
pelyňku	pelyněk	k1gInSc2	pelyněk
a	a	k8xC	a
hořce	hořec	k1gInSc2	hořec
<g/>
.	.	kIx.	.
</s>
<s>
Žlutého	žlutý	k2eAgMnSc2d1	žlutý
nebo	nebo	k8xC	nebo
červeného	červený	k2eAgNnSc2d1	červené
zabarvení	zabarvení	k1gNnSc2	zabarvení
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
použitím	použití	k1gNnSc7	použití
povolených	povolený	k2eAgFnPc2d1	povolená
přísad	přísada	k1gFnPc2	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Levandulové	levandulový	k2eAgNnSc1d1	levandulové
víno	víno	k1gNnSc1	víno
<g/>
:	:	kIx,	:
aromatizované	aromatizovaný	k2eAgNnSc1d1	aromatizované
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
produkt	produkt	k1gInSc1	produkt
hroznů	hrozen	k1gInPc2	hrozen
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
a	a	k8xC	a
bylinných	bylinný	k2eAgFnPc2d1	bylinná
částí	část	k1gFnPc2	část
levandule	levandule	k1gFnSc2	levandule
lékařské	lékařský	k2eAgFnSc2d1	lékařská
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
přírodně	přírodně	k6eAd1	přírodně
sladkého	sladký	k2eAgNnSc2d1	sladké
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
doslazování	doslazování	k1gNnSc1	doslazování
<g/>
.	.	kIx.	.
</s>
<s>
Likérové	likérový	k2eAgNnSc1d1	likérové
víno	víno	k1gNnSc1	víno
Tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
dezertních	dezertní	k2eAgNnPc2d1	dezertní
vín	víno	k1gNnPc2	víno
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nS	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
nejméně	málo	k6eAd3	málo
15	[number]	k4	15
%	%	kIx~	%
objemových	objemový	k2eAgInPc2d1	objemový
a	a	k8xC	a
nejvýše	vysoce	k6eAd3	vysoce
22	[number]	k4	22
%	%	kIx~	%
objemových	objemový	k2eAgInPc2d1	objemový
skutečného	skutečný	k2eAgInSc2d1	skutečný
obsahu	obsah	k1gInSc2	obsah
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
17,5	[number]	k4	17,5
%	%	kIx~	%
objemových	objemový	k2eAgInPc2d1	objemový
celkového	celkový	k2eAgInSc2d1	celkový
obsahu	obsah	k1gInSc2	obsah
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Jakostní	jakostní	k2eAgNnSc4d1	jakostní
likérové	likérový	k2eAgNnSc4d1	likérové
víno	víno	k1gNnSc4	víno
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
hroznového	hroznový	k2eAgInSc2d1	hroznový
moštu	mošt	k1gInSc2	mošt
<g/>
,	,	kIx,	,
z	z	k7c2	z
vína	víno	k1gNnSc2	víno
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
hroznového	hroznový	k2eAgInSc2d1	hroznový
moštu	mošt	k1gInSc2	mošt
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vyrobit	vyrobit	k5eAaPmF	vyrobit
taktéž	taktéž	k?	taktéž
z	z	k7c2	z
vinného	vinný	k2eAgInSc2d1	vinný
destilátu	destilát	k1gInSc2	destilát
<g/>
.	.	kIx.	.
</s>
<s>
Likérové	likérový	k2eAgNnSc1d1	likérové
víno	víno	k1gNnSc1	víno
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
splňuje	splňovat	k5eAaImIp3nS	splňovat
stejné	stejný	k2eAgFnPc4d1	stejná
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
však	však	k9	však
užité	užitý	k2eAgNnSc4d1	užité
víno	víno	k1gNnSc4	víno
a	a	k8xC	a
mošt	mošt	k1gInSc4	mošt
musí	muset	k5eAaImIp3nP	muset
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
z	z	k7c2	z
registrovaných	registrovaný	k2eAgFnPc2d1	registrovaná
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Dealkoholizované	Dealkoholizovaný	k2eAgNnSc4d1	Dealkoholizované
víno	víno	k1gNnSc4	víno
Toto	tento	k3xDgNnSc4	tento
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
také	také	k9	také
zkráceně	zkráceně	k6eAd1	zkráceně
dealko	dealko	k1gNnSc1	dealko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
běžných	běžný	k2eAgFnPc2d1	běžná
odrůd	odrůda	k1gFnPc2	odrůda
vína	víno	k1gNnSc2	víno
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Merlot	Merlot	k1gInSc1	Merlot
<g/>
,	,	kIx,	,
Riesling	Riesling	k1gInSc1	Riesling
<g/>
,	,	kIx,	,
Müller	Müller	k1gMnSc1	Müller
Thurgau	Thurgaus	k1gInSc2	Thurgaus
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
vermut	vermut	k1gInSc1	vermut
Rosso	Rossa	k1gFnSc5	Rossa
Bitter	Bittra	k1gFnPc2	Bittra
<g/>
,	,	kIx,	,
Rosé	Rosé	k1gNnPc2	Rosé
a	a	k8xC	a
sekt	sekta	k1gFnPc2	sekta
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
hroznový	hroznový	k2eAgInSc4d1	hroznový
mošt	mošt	k1gInSc4	mošt
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
předpokládáno	předpokládán	k2eAgNnSc1d1	předpokládáno
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
je	být	k5eAaImIp3nS	být
klasické	klasický	k2eAgNnSc4d1	klasické
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
např.	např.	kA	např.
vakuovou	vakuový	k2eAgFnSc7d1	vakuová
technologií	technologie	k1gFnSc7	technologie
odstraní	odstranit	k5eAaPmIp3nS	odstranit
etanol	etanol	k1gInSc1	etanol
<g/>
.	.	kIx.	.
</s>
<s>
Dealko	Dealko	k1gNnSc1	Dealko
víno	víno	k1gNnSc4	víno
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
0,5	[number]	k4	0,5
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
aromatičtější	aromatický	k2eAgFnSc2d2	aromatičtější
a	a	k8xC	a
plnější	plný	k2eAgFnSc2d2	plnější
odrůdy	odrůda	k1gFnSc2	odrůda
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
dealkoholizační	dealkoholizační	k2eAgInSc1d1	dealkoholizační
proces	proces	k1gInSc1	proces
odstraní	odstranit	k5eAaPmIp3nS	odstranit
cca	cca	kA	cca
do	do	k7c2	do
20	[number]	k4	20
%	%	kIx~	%
chuti	chuť	k1gFnSc3	chuť
vína	víno	k1gNnSc2	víno
jako	jako	k8xS	jako
takového	takový	k3xDgMnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
zbytkového	zbytkový	k2eAgInSc2d1	zbytkový
cukru	cukr	k1gInSc2	cukr
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Müller	Müller	k1gInSc4	Müller
Thurgau	Thurgaa	k1gFnSc4	Thurgaa
cca	cca	kA	cca
2,5	[number]	k4	2,5
g	g	kA	g
cukru	cukr	k1gInSc2	cukr
/	/	kIx~	/
litr	litr	k1gInSc1	litr
<g/>
,	,	kIx,	,
u	u	k7c2	u
Riesling	Riesling	k1gInSc4	Riesling
<g/>
,	,	kIx,	,
Rose	Rose	k1gMnSc1	Rose
a	a	k8xC	a
Merlot	Merlot	k1gMnSc1	Merlot
mezi	mezi	k7c7	mezi
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
g	g	kA	g
cukru	cukr	k1gInSc2	cukr
<g/>
/	/	kIx~	/
<g/>
litr	litr	k1gInSc1	litr
<g/>
.	.	kIx.	.
</s>
<s>
Perlivé	perlivý	k2eAgNnSc1d1	perlivé
víno	víno	k1gNnSc1	víno
Perlivé	perlivý	k2eAgNnSc1d1	perlivé
víno	víno	k1gNnSc1	víno
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
tuzemských	tuzemský	k2eAgNnPc2d1	tuzemské
vín	víno	k1gNnPc2	víno
(	(	kIx(	(
<g/>
stolních	stolní	k2eAgMnPc2d1	stolní
nebo	nebo	k8xC	nebo
jakostních	jakostní	k2eAgMnPc2d1	jakostní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
i	i	k9	i
obohacených	obohacený	k2eAgMnPc2d1	obohacený
(	(	kIx(	(
<g/>
o	o	k7c4	o
koňak	koňak	k1gInSc4	koňak
<g/>
,	,	kIx,	,
brandy	brandy	k1gFnSc1	brandy
nebo	nebo	k8xC	nebo
vinný	vinný	k2eAgInSc1d1	vinný
destilát	destilát	k1gInSc1	destilát
a	a	k8xC	a
zahuštěný	zahuštěný	k2eAgInSc1d1	zahuštěný
révový	révový	k2eAgInSc1d1	révový
mošt	mošt	k1gInSc1	mošt
nebo	nebo	k8xC	nebo
rafinovaný	rafinovaný	k2eAgInSc1d1	rafinovaný
cukr	cukr	k1gInSc1	cukr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
sycením	sycení	k1gNnSc7	sycení
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
nejméně	málo	k6eAd3	málo
9	[number]	k4	9
%	%	kIx~	%
objemových	objemový	k2eAgInPc2d1	objemový
celkového	celkový	k2eAgInSc2d1	celkový
obsahu	obsah	k1gInSc2	obsah
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
7	[number]	k4	7
%	%	kIx~	%
objemových	objemový	k2eAgInPc2d1	objemový
skutečného	skutečný	k2eAgInSc2d1	skutečný
obsahu	obsah	k1gInSc2	obsah
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
;	;	kIx,	;
přetlak	přetlak	k1gInSc1	přetlak
v	v	k7c6	v
lahvi	lahev	k1gFnSc6	lahev
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
0,1	[number]	k4	0,1
až	až	k9	až
0,25	[number]	k4	0,25
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Vína	víno	k1gNnPc1	víno
takto	takto	k6eAd1	takto
upravená	upravený	k2eAgNnPc1d1	upravené
jsou	být	k5eAaImIp3nP	být
svěží	svěží	k2eAgNnPc1d1	svěží
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
pitelná	pitelný	k2eAgFnSc1d1	pitelná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
víny	víno	k1gNnPc7	víno
šumivými	šumivý	k2eAgNnPc7d1	šumivé
je	být	k5eAaImIp3nS	být
perlivost	perlivost	k1gFnSc4	perlivost
těchto	tento	k3xDgNnPc2	tento
vín	víno	k1gNnPc2	víno
menší	malý	k2eAgInSc1d2	menší
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
větší	veliký	k2eAgFnPc1d2	veliký
a	a	k8xC	a
intenzivnější	intenzivní	k2eAgFnPc1d2	intenzivnější
bublinky	bublinka	k1gFnPc1	bublinka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
rychleji	rychle	k6eAd2	rychle
vyprchají	vyprchat	k5eAaPmIp3nP	vyprchat
<g/>
.	.	kIx.	.
</s>
<s>
Šumivé	šumivý	k2eAgNnSc4d1	šumivé
víno	víno	k1gNnSc4	víno
a	a	k8xC	a
sekt	sekt	k1gInSc4	sekt
Šumivé	šumivý	k2eAgNnSc4d1	šumivé
víno	víno	k1gNnSc4	víno
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgInSc1d1	vznikající
při	při	k7c6	při
kvašení	kvašení	k1gNnSc6	kvašení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
cukr	cukr	k1gInSc1	cukr
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
alkohol	alkohol	k1gInSc4	alkohol
a	a	k8xC	a
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
ostatních	ostatní	k2eAgFnPc2d1	ostatní
vín	vína	k1gFnPc2	vína
volně	volně	k6eAd1	volně
uniká	unikat	k5eAaImIp3nS	unikat
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
šumivých	šumivý	k2eAgNnPc2d1	šumivé
vín	víno	k1gNnPc2	víno
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
zabráněno	zabránit	k5eAaPmNgNnS	zabránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
víno	víno	k1gNnSc1	víno
při	při	k7c6	při
druhotném	druhotný	k2eAgNnSc6d1	druhotné
kvašení	kvašení	k1gNnSc6	kvašení
kvasí	kvasit	k5eAaImIp3nS	kvasit
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
láhvi	láhev	k1gFnSc6	láhev
anebo	anebo	k8xC	anebo
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
tanku	tank	k1gInSc6	tank
<g/>
.	.	kIx.	.
</s>
<s>
Šumivé	šumivý	k2eAgNnSc1d1	šumivé
víno	víno	k1gNnSc1	víno
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	s	k7c7	s
prvotním	prvotní	k2eAgNnSc7d1	prvotní
nebo	nebo	k8xC	nebo
druhotným	druhotný	k2eAgNnSc7d1	druhotné
kvašením	kvašení	k1gNnSc7	kvašení
vín	vína	k1gFnPc2	vína
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
kupáže	kupáž	k1gFnSc2	kupáž
použité	použitý	k2eAgFnSc2d1	použitá
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
musí	muset	k5eAaImIp3nP	muset
dosahovat	dosahovat	k5eAaImF	dosahovat
nejméně	málo	k6eAd3	málo
8,5	[number]	k4	8,5
%	%	kIx~	%
objemových	objemový	k2eAgInPc2d1	objemový
a	a	k8xC	a
přetlak	přetlak	k1gInSc1	přetlak
v	v	k7c6	v
lahvi	lahev	k1gFnSc6	lahev
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
nejméně	málo	k6eAd3	málo
0,3	[number]	k4	0,3
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
sekt	sekt	k1gInSc1	sekt
<g/>
.	.	kIx.	.
</s>
<s>
Šumivé	šumivý	k2eAgNnSc1d1	šumivé
víno	víno	k1gNnSc1	víno
bývá	bývat	k5eAaImIp3nS	bývat
mimo	mimo	k6eAd1	mimo
základních	základní	k2eAgInPc2d1	základní
údajů	údaj	k1gInPc2	údaj
označováno	označovat	k5eAaImNgNnS	označovat
názvem	název	k1gInSc7	název
druhu	druh	k1gInSc2	druh
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
názvem	název	k1gInSc7	název
místa	místo	k1gNnSc2	místo
výroby	výroba	k1gFnSc2	výroba
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Jakostní	jakostní	k2eAgNnSc1d1	jakostní
šumivé	šumivý	k2eAgNnSc1d1	šumivé
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
označuje	označovat	k5eAaImIp3nS	označovat
sekt	sekt	k1gInSc1	sekt
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	s	k7c7	s
prvotním	prvotní	k2eAgNnSc7d1	prvotní
nebo	nebo	k8xC	nebo
druhotným	druhotný	k2eAgNnSc7d1	druhotné
kvašením	kvašení	k1gNnSc7	kvašení
moštů	mošt	k1gInPc2	mošt
a	a	k8xC	a
vín	víno	k1gNnPc2	víno
(	(	kIx(	(
<g/>
i	i	k9	i
z	z	k7c2	z
dovozu	dovoz	k1gInSc2	dovoz
<g/>
)	)	kIx)	)
z	z	k7c2	z
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
povoleny	povolit	k5eAaPmNgFnP	povolit
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
původu	původ	k1gInSc2	původ
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
jakostního	jakostní	k2eAgNnSc2d1	jakostní
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Přetlak	přetlak	k1gInSc1	přetlak
v	v	k7c6	v
lahvi	lahev	k1gFnSc6	lahev
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
musí	muset	k5eAaImIp3nS	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
u	u	k7c2	u
klasické	klasický	k2eAgFnSc2d1	klasická
láhve	láhev	k1gFnSc2	láhev
nejméně	málo	k6eAd3	málo
0,35	[number]	k4	0,35
MP	MP	kA	MP
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
doba	doba	k1gFnSc1	doba
výroby	výroba	k1gFnSc2	výroba
při	při	k7c6	při
kvašení	kvašení	k1gNnSc6	kvašení
v	v	k7c6	v
tancích	tanec	k1gInPc6	tanec
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
120	[number]	k4	120
dnů	den	k1gInPc2	den
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
doba	doba	k1gFnSc1	doba
výroby	výroba	k1gFnSc2	výroba
při	při	k7c6	při
kvašení	kvašení	k1gNnSc6	kvašení
v	v	k7c6	v
lahvích	lahev	k1gFnPc6	lahev
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
kvašení	kvašení	k1gNnSc2	kvašení
(	(	kIx(	(
<g/>
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
kvašení	kvašení	k1gNnSc2	kvašení
až	až	k9	až
do	do	k7c2	do
odkalení	odkalení	k1gNnSc2	odkalení
<g/>
)	)	kIx)	)
v	v	k7c6	v
tancích	tanec	k1gInPc6	tanec
bez	bez	k7c2	bez
míchacího	míchací	k2eAgNnSc2d1	míchací
zařízení	zařízení	k1gNnSc2	zařízení
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
60	[number]	k4	60
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
míchacího	míchací	k2eAgNnSc2d1	míchací
zařízení	zařízení	k1gNnSc2	zařízení
nejméně	málo	k6eAd3	málo
30	[number]	k4	30
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
kupáže	kupáž	k1gFnSc2	kupáž
použité	použitý	k2eAgFnSc2d1	použitá
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
9	[number]	k4	9
%	%	kIx~	%
objemových	objemový	k2eAgInPc2d1	objemový
<g/>
;	;	kIx,	;
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
kupáže	kupáž	k1gFnSc2	kupáž
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
odrůdou	odrůda	k1gFnSc7	odrůda
nejméně	málo	k6eAd3	málo
8,5	[number]	k4	8,5
%	%	kIx~	%
objemových	objemový	k2eAgInPc2d1	objemový
<g/>
,	,	kIx,	,
skutečný	skutečný	k2eAgInSc1d1	skutečný
obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
hotovém	hotový	k2eAgInSc6d1	hotový
výrobku	výrobek	k1gInSc6	výrobek
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
10	[number]	k4	10
%	%	kIx~	%
objemových	objemový	k2eAgFnPc2d1	objemová
<g/>
.	.	kIx.	.
</s>
<s>
Aromatický	aromatický	k2eAgInSc1d1	aromatický
sekt	sekt	k1gInSc1	sekt
(	(	kIx(	(
<g/>
šumivé	šumivý	k2eAgNnSc1d1	šumivé
víno	víno	k1gNnSc1	víno
aromatické	aromatický	k2eAgNnSc1d1	aromatické
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
typ	typ	k1gInSc4	typ
jakostního	jakostní	k2eAgNnSc2d1	jakostní
šumivého	šumivý	k2eAgNnSc2d1	šumivé
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
při	při	k7c6	při
jehož	jehož	k3xOyRp3gFnSc6	jehož
výrobě	výroba	k1gFnSc6	výroba
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
pouze	pouze	k6eAd1	pouze
prvotní	prvotní	k2eAgNnSc1d1	prvotní
kvašení	kvašení	k1gNnSc1	kvašení
kupáže	kupázat	k5eAaPmIp3nS	kupázat
z	z	k7c2	z
moštů	mošt	k1gInPc2	mošt
výhradně	výhradně	k6eAd1	výhradně
pěti	pět	k4xCc2	pět
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
splněny	splnit	k5eAaPmNgInP	splnit
ještě	ještě	k9	ještě
technologické	technologický	k2eAgInPc1d1	technologický
požadavky	požadavek	k1gInPc1	požadavek
ukládané	ukládaný	k2eAgInPc1d1	ukládaný
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Aromatický	aromatický	k2eAgInSc1d1	aromatický
sekt	sekt	k1gInSc1	sekt
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
údajů	údaj	k1gInPc2	údaj
označuje	označovat	k5eAaImIp3nS	označovat
názvem	název	k1gInSc7	název
odrůdy	odrůda	k1gFnSc2	odrůda
nebo	nebo	k8xC	nebo
údajem	údaj	k1gInSc7	údaj
<g/>
,	,	kIx,	,
že	že	k8xS	že
víno	víno	k1gNnSc1	víno
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
aromatických	aromatický	k2eAgFnPc2d1	aromatická
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
,	,	kIx,	,
kterými	který	k3yIgMnPc7	který
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Irsai	Irsa	k1gMnPc1	Irsa
Oliver	Oliver	k1gMnSc1	Oliver
<g/>
,	,	kIx,	,
Muškát	muškát	k1gInSc1	muškát
moravský	moravský	k2eAgInSc1d1	moravský
<g/>
,	,	kIx,	,
Muškát	muškát	k1gInSc1	muškát
Ottonel	Ottonel	k1gInSc1	Ottonel
<g/>
,	,	kIx,	,
Tramín	tramín	k1gInSc1	tramín
červený	červený	k2eAgInSc1d1	červený
nebo	nebo	k8xC	nebo
Müller	Müller	k1gInSc1	Müller
Thurgau	Thurgaus	k1gInSc2	Thurgaus
<g/>
.	.	kIx.	.
</s>
<s>
Jakostní	jakostní	k2eAgNnSc1d1	jakostní
šumivé	šumivý	k2eAgNnSc1d1	šumivé
víno	víno	k1gNnSc1	víno
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
sekt	sekt	k1gInSc1	sekt
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
druhotným	druhotný	k2eAgNnSc7d1	druhotné
kvašením	kvašení	k1gNnSc7	kvašení
kupáže	kupázat	k5eAaPmIp3nS	kupázat
z	z	k7c2	z
jakostních	jakostní	k2eAgNnPc2d1	jakostní
odrůdových	odrůdový	k2eAgNnPc2d1	odrůdové
vín	víno	k1gNnPc2	víno
jen	jen	k9	jen
uvnitř	uvnitř	k7c2	uvnitř
jedné	jeden	k4xCgFnSc2	jeden
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
sklizeny	sklizen	k2eAgInPc4d1	sklizen
hrozny	hrozen	k1gInPc4	hrozen
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Přetlak	přetlak	k1gInSc1	přetlak
v	v	k7c6	v
lahvi	lahev	k1gFnSc6	lahev
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
0,35	[number]	k4	0,35
MPa	MPa	k1gMnPc2	MPa
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
láhví	láhev	k1gFnPc2	láhev
do	do	k7c2	do
objemu	objem	k1gInSc2	objem
0,25	[number]	k4	0,25
l.	l.	k?	l.
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
určena	určen	k2eAgFnSc1d1	určena
minimální	minimální	k2eAgFnSc1d1	minimální
celková	celkový	k2eAgFnSc1d1	celková
doba	doba	k1gFnSc1	doba
výroby	výroba	k1gFnSc2	výroba
včetně	včetně	k7c2	včetně
zrání	zrání	k1gNnSc2	zrání
(	(	kIx(	(
<g/>
180	[number]	k4	180
dnů	den	k1gInPc2	den
při	při	k7c6	při
kvašení	kvašení	k1gNnSc6	kvašení
v	v	k7c6	v
tancích	tanec	k1gInPc6	tanec
a	a	k8xC	a
270	[number]	k4	270
dnů	den	k1gInPc2	den
při	při	k7c6	při
kvašení	kvašení	k1gNnSc6	kvašení
v	v	k7c6	v
lahvích	lahev	k1gFnPc6	lahev
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejmenší	malý	k2eAgFnSc1d3	nejmenší
doba	doba	k1gFnSc1	doba
kvašení	kvašení	k1gNnSc2	kvašení
(	(	kIx(	(
<g/>
90	[number]	k4	90
dnů	den	k1gInPc2	den
a	a	k8xC	a
při	při	k7c6	při
kvašení	kvašení	k1gNnSc6	kvašení
v	v	k7c6	v
tancích	tanec	k1gInPc6	tanec
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
míchacího	míchací	k2eAgNnSc2d1	míchací
zařízení	zařízení	k1gNnSc2	zařízení
nejméně	málo	k6eAd3	málo
30	[number]	k4	30
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
kupáže	kupáž	k1gFnSc2	kupáž
použité	použitý	k2eAgFnSc2d1	použitá
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
9	[number]	k4	9
%	%	kIx~	%
objemových	objemový	k2eAgInPc2d1	objemový
<g/>
;	;	kIx,	;
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
kupáže	kupáž	k1gFnSc2	kupáž
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
odrůdou	odrůda	k1gFnSc7	odrůda
nejméně	málo	k6eAd3	málo
8,5	[number]	k4	8,5
%	%	kIx~	%
objemových	objemový	k2eAgInPc2d1	objemový
<g/>
,	,	kIx,	,
skutečný	skutečný	k2eAgInSc1d1	skutečný
obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
hotovém	hotový	k2eAgInSc6d1	hotový
výrobku	výrobek	k1gInSc6	výrobek
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
10	[number]	k4	10
%	%	kIx~	%
objemových	objemový	k2eAgFnPc2d1	objemová
<g/>
.	.	kIx.	.
</s>
<s>
Šumivé	šumivý	k2eAgNnSc1d1	šumivé
víno	víno	k1gNnSc1	víno
bývá	bývat	k5eAaImIp3nS	bývat
mimo	mimo	k6eAd1	mimo
základních	základní	k2eAgInPc2d1	základní
údajů	údaj	k1gInPc2	údaj
označováno	označovat	k5eAaImNgNnS	označovat
názvem	název	k1gInSc7	název
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
byly	být	k5eAaImAgInP	být
sklizeny	sklizen	k2eAgInPc1d1	sklizen
hrozny	hrozen	k1gInPc1	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Pěstitelský	pěstitelský	k2eAgInSc1d1	pěstitelský
sekt	sekt	k1gInSc1	sekt
<g/>
:	:	kIx,	:
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
splněny	splněn	k2eAgFnPc4d1	splněna
podmínky	podmínka	k1gFnPc4	podmínka
výrobu	výroba	k1gFnSc4	výroba
sektu	sekta	k1gFnSc4	sekta
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastní	vlastní	k2eAgFnSc1d1	vlastní
výroba	výroba	k1gFnSc1	výroba
se	se	k3xPyFc4	se
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
u	u	k7c2	u
pěstitele	pěstitel	k1gMnSc2	pěstitel
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
hrozny	hrozen	k1gInPc1	hrozen
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Odrůdy	odrůda	k1gFnSc2	odrůda
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejběžnější	běžný	k2eAgFnPc4d3	nejběžnější
odrůdy	odrůda	k1gFnPc4	odrůda
produkované	produkovaný	k2eAgFnPc4d1	produkovaná
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Aurelius	Aurelius	k1gInSc1	Aurelius
Floriánka	Floriánek	k1gMnSc2	Floriánek
Chardonnay	Chardonnaa	k1gMnSc2	Chardonnaa
Irsai	Irsa	k1gFnSc2	Irsa
Oliver	Oliver	k1gMnSc1	Oliver
Muškát	muškát	k1gInSc1	muškát
moravský	moravský	k2eAgMnSc1d1	moravský
Müller	Müller	k1gMnSc1	Müller
Thurgau	Thurgaus	k1gInSc2	Thurgaus
Neuburské	Neuburský	k2eAgFnSc2d1	Neuburský
Pálava	Pálava	k1gFnSc1	Pálava
Rulandské	rulandský	k2eAgFnSc2d1	rulandský
bílé	bílý	k2eAgFnSc2d1	bílá
Rulandské	rulandský	k2eAgFnSc2d1	rulandský
šedé	šedá	k1gFnSc2	šedá
Ryzlink	ryzlink	k1gInSc1	ryzlink
rýnský	rýnský	k2eAgInSc1d1	rýnský
Ryzlink	ryzlink	k1gInSc1	ryzlink
vlašský	vlašský	k2eAgInSc1d1	vlašský
Sauvignon	Sauvignon	k1gInSc1	Sauvignon
Sylvánské	sylvánské	k1gNnSc1	sylvánské
zelené	zelená	k1gFnSc2	zelená
Tramín	tramín	k1gInSc1	tramín
bílý	bílý	k2eAgInSc1d1	bílý
Tramín	tramín	k1gInSc1	tramín
červený	červený	k2eAgInSc1d1	červený
Veltlínské	veltlínský	k2eAgFnSc3d1	veltlínský
červené	červená	k1gFnSc3	červená
rané	raný	k2eAgFnSc2d1	raná
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
zelené	zelená	k1gFnSc2	zelená
André	André	k1gMnSc1	André
Cabernet	Cabernet	k1gMnSc1	Cabernet
Moravia	Moravium	k1gNnSc2	Moravium
Cabernet	Cabernet	k1gMnSc1	Cabernet
Sauvignon	Sauvignon	k1gMnSc1	Sauvignon
Frankovka	frankovka	k1gFnSc1	frankovka
Merlot	Merlot	k1gInSc4	Merlot
Modrý	modrý	k2eAgInSc1d1	modrý
portugal	portugal	k1gInSc1	portugal
Rulandské	rulandský	k2eAgFnSc2d1	rulandský
modré	modrý	k2eAgFnSc2d1	modrá
Svatovavřinecké	svatovavřinecké	k1gNnSc1	svatovavřinecké
Zweigeltrebe	Zweigeltreb	k1gInSc5	Zweigeltreb
Sirah	Sirah	k1gInSc1	Sirah
Franceska	Franceska	k1gFnSc1	Franceska
Pinot	Pinot	k1gMnSc1	Pinot
Noir	Noir	k1gMnSc1	Noir
Merlot	Merlot	k1gMnSc1	Merlot
Růžové	růžový	k2eAgNnSc1d1	růžové
Svatovavřinecké	svatovavřinecké	k1gNnSc1	svatovavřinecké
Víno	víno	k1gNnSc4	víno
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rovněž	rovněž	k9	rovněž
vyrábět	vyrábět	k5eAaImF	vyrábět
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
doplněn	doplnit	k5eAaPmNgInS	doplnit
přívlastkem	přívlastek	k1gInSc7	přívlastek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ovocné	ovocný	k2eAgNnSc1d1	ovocné
víno	víno	k1gNnSc1	víno
<g/>
;	;	kIx,	;
známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
jablečné	jablečný	k2eAgNnSc1d1	jablečné
víno	víno	k1gNnSc1	víno
(	(	kIx(	(
<g/>
cidre	cidre	k1gInSc1	cidre
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
jablečňák	jablečňák	k1gMnSc1	jablečňák
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
oběma	dva	k4xCgMnPc7	dva
se	se	k3xPyFc4	se
lidově	lidově	k6eAd1	lidově
říká	říkat	k5eAaImIp3nS	říkat
čůčo	čůčo	k1gNnSc4	čůčo
nebo	nebo	k8xC	nebo
čučák	čučák	k1gInSc4	čučák
nebo	nebo	k8xC	nebo
švestkové	švestkový	k2eAgNnSc4d1	švestkové
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
mání	mání	k1gFnSc3	mání
v	v	k7c6	v
asijských	asijský	k2eAgFnPc6d1	asijská
restauracích	restaurace	k1gFnPc6	restaurace
a	a	k8xC	a
obchodech	obchod	k1gInPc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
víno	víno	k1gNnSc1	víno
bez	bez	k7c2	bez
dalšího	další	k2eAgNnSc2d1	další
vždy	vždy	k6eAd1	vždy
primárně	primárně	k6eAd1	primárně
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
víno	víno	k1gNnSc4	víno
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
pití	pití	k1gNnSc1	pití
vína	víno	k1gNnSc2	víno
má	mít	k5eAaImIp3nS	mít
příznivé	příznivý	k2eAgInPc4d1	příznivý
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiné	jiný	k2eAgFnPc1d1	jiná
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Půjde	jít	k5eAaImIp3nS	jít
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
systematické	systematický	k2eAgFnSc2d1	systematická
chyby	chyba	k1gFnSc2	chyba
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
abstinentů	abstinent	k1gMnPc2	abstinent
například	například	k6eAd1	například
spadají	spadat	k5eAaImIp3nP	spadat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nepijí	pít	k5eNaImIp3nP	pít
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
ve	v	k7c6	v
vinné	vinný	k2eAgFnSc6d1	vinná
révě	réva	k1gFnSc6	réva
zdravotně	zdravotně	k6eAd1	zdravotně
významná	významný	k2eAgFnSc1d1	významná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
resveratrol	resveratrol	k1gInSc1	resveratrol
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
role	role	k1gFnSc1	role
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prevenci	prevence	k1gFnSc6	prevence
kardiovaskulárních	kardiovaskulární	k2eAgNnPc2d1	kardiovaskulární
onemocnění	onemocnění	k1gNnPc2	onemocnění
a	a	k8xC	a
cukrovky	cukrovka	k1gFnSc2	cukrovka
<g/>
.	.	kIx.	.
</s>
<s>
Réva	réva	k1gFnSc1	réva
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
též	též	k9	též
protirakovinný	protirakovinný	k2eAgInSc4d1	protirakovinný
účinek	účinek	k1gInSc4	účinek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
fenoly	fenol	k1gInPc1	fenol
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
obsažené	obsažený	k2eAgInPc1d1	obsažený
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
antioxidanty	antioxidant	k1gInPc1	antioxidant
<g/>
,	,	kIx,	,
zpomalují	zpomalovat	k5eAaImIp3nP	zpomalovat
stárnutí	stárnutí	k1gNnSc4	stárnutí
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
rakovinné	rakovinný	k2eAgNnSc1d1	rakovinné
chrání	chránit	k5eAaImIp3nS	chránit
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
doporučit	doporučit	k5eAaPmF	doporučit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
IARC	IARC	kA	IARC
je	být	k5eAaImIp3nS	být
však	však	k9	však
každý	každý	k3xTgInSc1	každý
alkoholický	alkoholický	k2eAgInSc1d1	alkoholický
nápoj	nápoj	k1gInSc1	nápoj
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
prokázaný	prokázaný	k2eAgInSc4d1	prokázaný
karcinogen	karcinogen	k1gInSc4	karcinogen
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
studie	studie	k1gFnSc1	studie
na	na	k7c4	na
11	[number]	k4	11
tisících	tisící	k4xOgInPc2	tisící
dvojčat	dvojče	k1gNnPc2	dvojče
zahrnující	zahrnující	k2eAgMnPc1d1	zahrnující
43	[number]	k4	43
let	léto	k1gNnPc2	léto
prokázala	prokázat	k5eAaPmAgFnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
sklenku	sklenka	k1gFnSc4	sklenka
vína	víno	k1gNnSc2	víno
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
že	že	k8xS	že
nastane	nastat	k5eAaPmIp3nS	nastat
cévní	cévní	k2eAgFnSc1d1	cévní
mozková	mozkový	k2eAgFnSc1d1	mozková
příhoda	příhoda	k1gFnSc1	příhoda
o	o	k7c4	o
34	[number]	k4	34
%	%	kIx~	%
větší	veliký	k2eAgFnSc2d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
když	když	k8xS	když
se	se	k3xPyFc4	se
nepije	pít	k5eNaImIp3nS	pít
ani	ani	k8xC	ani
polovina	polovina	k1gFnSc1	polovina
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
dítěti	dítě	k1gNnSc6	dítě
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
fetální	fetální	k2eAgInSc1d1	fetální
alkoholový	alkoholový	k2eAgInSc1d1	alkoholový
syndrom	syndrom	k1gInSc1	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
dětský	dětský	k2eAgMnSc1d1	dětský
kardiolog	kardiolog	k1gMnSc1	kardiolog
profesor	profesor	k1gMnSc1	profesor
Milan	Milan	k1gMnSc1	Milan
Šamánek	Šamánek	k1gMnSc1	Šamánek
<g/>
,	,	kIx,	,
střídmý	střídmý	k2eAgMnSc1d1	střídmý
piják	piják	k1gMnSc1	piják
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
mj.	mj.	kA	mj.
z	z	k7c2	z
článku	článek	k1gInSc2	článek
v	v	k7c6	v
blíže	blízce	k6eAd2	blízce
nespecifikovném	specifikovný	k2eNgInSc6d1	specifikovný
kardiologickém	kardiologický	k2eAgInSc6d1	kardiologický
časopise	časopis	k1gInSc6	časopis
<g/>
,	,	kIx,	,
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
pít	pít	k5eAaImF	pít
mužům	muž	k1gMnPc3	muž
4	[number]	k4	4
decilitry	decilitr	k1gInPc4	decilitr
vína	víno	k1gNnSc2	víno
denně	denně	k6eAd1	denně
(	(	kIx(	(
<g/>
ženám	žena	k1gFnPc3	žena
polovinu	polovina	k1gFnSc4	polovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
však	však	k9	však
je	být	k5eAaImIp3nS	být
pít	pít	k5eAaImF	pít
pravidelně	pravidelně	k6eAd1	pravidelně
–	–	k?	–
alespoň	alespoň	k9	alespoň
pětkrát	pětkrát	k6eAd1	pětkrát
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgNnSc1d1	bílé
víno	víno	k1gNnSc1	víno
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
vhodnější	vhodný	k2eAgFnSc4d2	vhodnější
než	než	k8xS	než
červené	červený	k2eAgInPc4d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Burčák	burčák	k1gInSc4	burčák
Dezertní	dezertní	k2eAgNnSc4d1	dezertní
víno	víno	k1gNnSc4	víno
Francouzský	francouzský	k2eAgInSc1d1	francouzský
paradox	paradox	k1gInSc1	paradox
Vínovice	vínovice	k1gFnSc1	vínovice
(	(	kIx(	(
<g/>
SSJČ	SSJČ	kA	SSJČ
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Koňak	koňak	k1gInSc1	koňak
<g/>
,	,	kIx,	,
Brandy	brandy	k1gFnSc1	brandy
(	(	kIx(	(
<g/>
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
vinná	vinný	k2eAgFnSc1d1	vinná
pálenka	pálenka	k1gFnSc1	pálenka
<g/>
:	:	kIx,	:
Matolinovice	matolinovice	k1gFnSc1	matolinovice
(	(	kIx(	(
<g/>
Terkelice	terkelice	k1gFnSc1	terkelice
<g/>
,	,	kIx,	,
Grappa	Grappa	k1gFnSc1	Grappa
aj.	aj.	kA	aj.
názvy	název	k1gInPc1	název
<g/>
)	)	kIx)	)
Vliv	vliv	k1gInSc1	vliv
vína	víno	k1gNnSc2	víno
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
Výroba	výroba	k1gFnSc1	výroba
vína	víno	k1gNnSc2	víno
Enologie	enologie	k1gFnSc2	enologie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
víno	víno	k1gNnSc4	víno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
víno	víno	k1gNnSc4	víno
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc1	heslo
víno	víno	k1gNnSc4	víno
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
znalce	znalec	k1gMnSc2	znalec
vín	vína	k1gFnPc2	vína
Databáze	databáze	k1gFnSc2	databáze
českých	český	k2eAgFnPc2d1	Česká
<g/>
,	,	kIx,	,
moravských	moravský	k2eAgFnPc2d1	Moravská
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
vín	vína	k1gFnPc2	vína
Historie	historie	k1gFnSc1	historie
vína	víno	k1gNnSc2	víno
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
-	-	kIx~	-
zdroj	zdroj	k1gInSc1	zdroj
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
článek	článek	k1gInSc4	článek
Prima	prima	k2eAgInSc4d1	prima
ZOOM	ZOOM	kA	ZOOM
</s>
