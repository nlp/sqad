<s>
Za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
Louisiana	Louisiana	k1gFnSc1	Louisiana
v	v	k7c6	v
letech	let	k1gInPc6	let
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
součástí	součást	k1gFnPc2	součást
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
k	k	k7c3	k
Unii	unie	k1gFnSc3	unie
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
připojena	připojen	k2eAgFnSc1d1	připojena
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
