<s>
Entomologie	entomologie	k1gFnSc1	entomologie
je	být	k5eAaImIp3nS	být
zoologická	zoologický	k2eAgFnSc1d1	zoologická
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc1	hmyz
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
forem	forma	k1gFnPc2	forma
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
i	i	k8xC	i
ostatními	ostatní	k2eAgFnPc7d1	ostatní
formami	forma	k1gFnPc7	forma
života	život	k1gInSc2	život
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
důležitou	důležitý	k2eAgFnSc4d1	důležitá
specializaci	specializace	k1gFnSc4	specializace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
entomologové	entomolog	k1gMnPc1	entomolog
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jak	jak	k8xS	jak
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgNnPc4d1	zabývající
se	s	k7c7	s
hmyzem	hmyz	k1gInSc7	hmyz
jako	jako	k8xS	jako
takovým	takový	k3xDgNnSc7	takový
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k8xC	i
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
komerčních	komerční	k2eAgFnPc2d1	komerční
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaImIp3nP	věnovat
regulaci	regulace	k1gFnSc3	regulace
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
prostředkům	prostředek	k1gInPc3	prostředek
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
entomologii	entomologie	k1gFnSc4	entomologie
a	a	k8xC	a
aplikovanou	aplikovaný	k2eAgFnSc4d1	aplikovaná
entomologii	entomologie	k1gFnSc4	entomologie
<g/>
.	.	kIx.	.
arachnologie	arachnologie	k1gFnSc1	arachnologie
dipterologie	dipterologie	k1gFnSc1	dipterologie
hymenopterologie	hymenopterologie	k1gFnSc1	hymenopterologie
lepidopterologie	lepidopterologie	k1gFnSc1	lepidopterologie
parnassiologie	parnassiologie	k1gFnSc1	parnassiologie
myrmekologie	myrmekologie	k1gFnSc1	myrmekologie
odonatologie	odonatologie	k1gFnSc1	odonatologie
Prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
entomologií	entomologie	k1gFnSc7	entomologie
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
století	století	k1gNnSc6	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
Aristotelés	Aristotelésa	k1gFnPc2	Aristotelésa
(	(	kIx(	(
<g/>
384	[number]	k4	384
před	před	k7c7	před
Kr.	Kr.	k1gFnSc7	Kr.
-	-	kIx~	-
322	[number]	k4	322
před	před	k7c7	před
Kr.	Kr.	k1gFnSc7	Kr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
starověký	starověký	k2eAgMnSc1d1	starověký
řecký	řecký	k2eAgMnSc1d1	řecký
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
dal	dát	k5eAaPmAgMnS	dát
základ	základ	k1gInSc4	základ
vzniku	vznik	k1gInSc2	vznik
zoologie	zoologie	k1gFnSc2	zoologie
<g/>
,	,	kIx,	,
t.	t.	k?	t.
j.	j.	k?	j.
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
výzkumem	výzkum	k1gInSc7	výzkum
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgInSc2	svůj
výzkumu	výzkum	k1gInSc2	výzkum
i	i	k9	i
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
skupinu	skupina	k1gFnSc4	skupina
malých	malý	k2eAgMnPc2d1	malý
živočichů	živočich	k1gMnPc2	živočich
-	-	kIx~	-
hmyz	hmyz	k1gInSc1	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
má	mít	k5eAaImIp3nS	mít
hlavní	hlavní	k2eAgInSc1d1	hlavní
podíl	podíl	k1gInSc1	podíl
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
a	a	k8xC	a
definování	definování	k1gNnSc4	definování
vědního	vědní	k2eAgInSc2d1	vědní
úseku	úsek	k1gInSc2	úsek
zoologie	zoologie	k1gFnSc2	zoologie
<g/>
,	,	kIx,	,
vědy	věda	k1gFnPc4	věda
o	o	k7c6	o
hmyzu	hmyz	k1gInSc6	hmyz
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
entomologii	entomologie	k1gFnSc4	entomologie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
název	název	k1gInSc4	název
této	tento	k3xDgFnSc2	tento
vědy	věda	k1gFnSc2	věda
-	-	kIx~	-
entomologie	entomologie	k1gFnSc1	entomologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
ε	ε	k?	ε
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
entomon	entomon	k1gInSc1	entomon
znamená	znamenat	k5eAaImIp3nS	znamenat
řecky	řecky	k6eAd1	řecky
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
logos	logos	k1gInSc4	logos
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
<s>
Běžnou	běžný	k2eAgFnSc7d1	běžná
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
s	s	k7c7	s
entomologickým	entomologický	k2eAgInSc7d1	entomologický
charakterem	charakter	k1gInSc7	charakter
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
bylo	být	k5eAaImAgNnS	být
včelařství	včelařství	k1gNnSc1	včelařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
období	období	k1gNnSc6	období
hmyz	hmyz	k1gInSc1	hmyz
dříve	dříve	k6eAd2	dříve
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
jako	jako	k9	jako
nepřítel	nepřítel	k1gMnSc1	nepřítel
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
připravoval	připravovat	k5eAaImAgMnS	připravovat
o	o	k7c4	o
část	část	k1gFnSc4	část
úrody	úroda	k1gFnSc2	úroda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
značně	značně	k6eAd1	značně
obtížně	obtížně	k6eAd1	obtížně
získávala	získávat	k5eAaImAgFnS	získávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
středověk	středověk	k1gInSc1	středověk
došlo	dojít	k5eAaPmAgNnS	dojít
vlivem	vlivem	k7c2	vlivem
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
dogmat	dogma	k1gNnPc2	dogma
k	k	k7c3	k
celkové	celkový	k2eAgFnSc3d1	celková
stagnaci	stagnace	k1gFnSc3	stagnace
rozvoje	rozvoj	k1gInSc2	rozvoj
všech	všecek	k3xTgFnPc2	všecek
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
vědě	věda	k1gFnSc6	věda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
v	v	k7c6	v
entomologii	entomologie	k1gFnSc6	entomologie
se	se	k3xPyFc4	se
tradovaly	tradovat	k5eAaImAgInP	tradovat
omyly	omyl	k1gInPc1	omyl
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
ze	z	k7c2	z
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Zastáváno	zastáván	k2eAgNnSc1d1	zastáváno
a	a	k8xC	a
uznáváno	uznáván	k2eAgNnSc1d1	uznáváno
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
správné	správný	k2eAgNnSc4d1	správné
mnoho	mnoho	k4c1	mnoho
polopravd	polopravda	k1gFnPc2	polopravda
a	a	k8xC	a
zkreslených	zkreslený	k2eAgInPc2d1	zkreslený
<g/>
,	,	kIx,	,
nevědeckých	vědecký	k2eNgInPc2d1	nevědecký
závěrů	závěr	k1gInPc2	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
začal	začít	k5eAaPmAgInS	začít
šířit	šířit	k5eAaImF	šířit
nový	nový	k2eAgInSc1d1	nový
myšlenkový	myšlenkový	k2eAgInSc1d1	myšlenkový
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
itálské	itálský	k2eAgFnSc6d1	itálská
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
renesance	renesance	k1gFnSc1	renesance
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
znamenala	znamenat	k5eAaImAgFnS	znamenat
"	"	kIx"	"
<g/>
znovuzrození	znovuzrození	k1gNnSc1	znovuzrození
<g/>
"	"	kIx"	"
původních	původní	k2eAgFnPc2d1	původní
antických	antický	k2eAgFnPc2d1	antická
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
mělo	mít	k5eAaImAgNnS	mít
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
celkové	celkový	k2eAgNnSc4d1	celkové
uvolnění	uvolnění	k1gNnSc4	uvolnění
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
formovaly	formovat	k5eAaImAgInP	formovat
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInPc1d1	moderní
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
mnoho	mnoho	k4c4	mnoho
významných	významný	k2eAgMnPc2d1	významný
entomologů	entomolog	k1gMnPc2	entomolog
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
budoucím	budoucí	k2eAgInSc6d1	budoucí
přírodovědném	přírodovědný	k2eAgInSc6d1	přírodovědný
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
entomologie	entomologie	k1gFnSc2	entomologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Entomologie	entomologie	k1gFnSc2	entomologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
