<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
vesnice	vesnice	k1gFnSc1	vesnice
typu	typ	k1gInSc2	typ
společná	společný	k2eAgFnSc1d1	společná
osada	osada	k1gFnSc1	osada
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
distriktu	distrikt	k1gInSc6	distrikt
<g/>
,	,	kIx,	,
v	v	k7c6	v
Oblastní	oblastní	k2eAgFnSc6d1	oblastní
radě	rada	k1gFnSc6	rada
Chevel	Chevel	k1gMnSc1	Chevel
Ejlot	Ejlot	k1gMnSc1	Ejlot
<g/>
.	.	kIx.	.
</s>
