<s>
Be	Be	k?	Be
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
Ora	Ora	k1gFnSc1	Ora
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ב	ב	k?	ב
<g/>
ְ	ְ	k?	ְ
<g/>
ּ	ּ	k?	ּ
<g/>
א	א	k?	א
<g/>
ֵ	ֵ	k?	ֵ
<g/>
ר	ר	k?	ר
א	א	k?	א
<g/>
ֹ	ֹ	k?	ֹ
<g/>
ר	ר	k?	ר
<g/>
ָ	ָ	k?	ָ
<g/>
ה	ה	k?	ה
<g/>
,	,	kIx,	,
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
přepisu	přepis	k1gInSc6	přepis
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
Be	Be	k1gFnSc2	Be
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
Ora	Ora	k1gFnSc1	Ora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnSc1	vesnice
typu	typ	k1gInSc2	typ
společná	společný	k2eAgFnSc1d1	společná
osada	osada	k1gFnSc1	osada
(	(	kIx(	(
<g/>
jišuv	jišuv	k1gMnSc1	jišuv
kehilati	kehilat	k5eAaPmF	kehilat
<g/>
)	)	kIx)	)
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
distriktu	distrikt	k1gInSc6	distrikt
<g/>
,	,	kIx,	,
v	v	k7c6	v
Oblastní	oblastní	k2eAgFnSc6d1	oblastní
radě	rada	k1gFnSc6	rada
Chevel	Chevel	k1gMnSc1	Chevel
Ejlot	Ejlot	k1gMnSc1	Ejlot
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
136	[number]	k4	136
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
údolí	údolí	k1gNnSc2	údolí
vádí	vádí	k1gNnSc2	vádí
al-Araba	al-Araba	k1gMnSc1	al-Araba
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
zde	zde	k6eAd1	zde
ústí	ústit	k5eAaImIp3nP	ústit
vádí	vádí	k1gNnPc1	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Ora	Ora	k1gMnSc1	Ora
<g/>
,	,	kIx,	,
cca	cca	kA	cca
143	[number]	k4	143
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
jižního	jižní	k2eAgInSc2d1	jižní
břehu	břeh	k1gInSc2	břeh
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zvedá	zvedat	k5eAaImIp3nS	zvedat
aridní	aridní	k2eAgFnSc1d1	aridní
oblast	oblast	k1gFnSc1	oblast
pouště	poušť	k1gFnSc2	poušť
Negev	Negev	k1gFnSc1	Negev
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
190	[number]	k4	190
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
břehu	břeh	k1gInSc2	břeh
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
cca	cca	kA	cca
262	[number]	k4	262
kilometrů	kilometr	k1gInPc2	kilometr
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Tel	tel	kA	tel
Avivu	Aviva	k1gFnSc4	Aviva
<g/>
,	,	kIx,	,
cca	cca	kA	cca
230	[number]	k4	230
kilometrů	kilometr	k1gInPc2	kilometr
jihojihozápadně	jihojihozápadně	k6eAd1	jihojihozápadně
od	od	k7c2	od
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
Jeruzalému	Jeruzalém	k1gInSc2	Jeruzalém
a	a	k8xC	a
17	[number]	k4	17
kilometrů	kilometr	k1gInPc2	kilometr
severoseverovýchodně	severoseverovýchodně	k6eAd1	severoseverovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
Ejlat	Ejlat	k1gInSc1	Ejlat
<g/>
.	.	kIx.	.
</s>
<s>
Be	Be	k?	Be
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
Ora	Ora	k1gFnSc6	Ora
obývají	obývat	k5eAaImIp3nP	obývat
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
osídlení	osídlení	k1gNnSc1	osídlení
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
regionu	region	k1gInSc6	region
je	být	k5eAaImIp3nS	být
etnicky	etnicky	k6eAd1	etnicky
převážně	převážně	k6eAd1	převážně
židovské	židovský	k2eAgFnPc1d1	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
2	[number]	k4	2
kilometry	kilometr	k1gInPc4	kilometr
vzdálena	vzdálit	k5eAaPmNgNnP	vzdálit
od	od	k7c2	od
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
<g/>
.	.	kIx.	.
</s>
<s>
Be	Be	k?	Be
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
Ora	Ora	k1gFnSc1	Ora
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
dopravní	dopravní	k2eAgFnSc4d1	dopravní
síť	síť	k1gFnSc4	síť
napojen	napojit	k5eAaPmNgMnS	napojit
pomocí	pomocí	k7c2	pomocí
dálnice	dálnice	k1gFnSc2	dálnice
číslo	číslo	k1gNnSc1	číslo
90	[number]	k4	90
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
Ramonovo	Ramonův	k2eAgNnSc4d1	Ramonovo
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Be	Be	k?	Be
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
Ora	Ora	k1gFnSc2	Ora
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ovšem	ovšem	k9	ovšem
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
výcvikový	výcvikový	k2eAgInSc4d1	výcvikový
tábor	tábor	k1gInSc4	tábor
mládežnické	mládežnický	k2eAgFnSc2d1	mládežnická
organizace	organizace	k1gFnSc2	organizace
Gadna	Gadno	k1gNnSc2	Gadno
bez	bez	k7c2	bez
trvalého	trvalý	k2eAgNnSc2d1	trvalé
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
arabský	arabský	k2eAgInSc1d1	arabský
místní	místní	k2eAgInSc1d1	místní
název	název	k1gInSc1	název
této	tento	k3xDgFnSc2	tento
lokality	lokalita	k1gFnSc2	lokalita
je	být	k5eAaImIp3nS	být
Bir	Bir	k1gFnSc1	Bir
Chindis	Chindis	k1gFnSc1	Chindis
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
studna	studna	k1gFnSc1	studna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
zásobovala	zásobovat	k5eAaImAgFnS	zásobovat
město	město	k1gNnSc4	město
Ejlat	Ejlat	k2eAgInSc4d1	Ejlat
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
vesnice	vesnice	k1gFnSc1	vesnice
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k6eAd1	až
počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
položení	položení	k1gNnSc1	položení
základního	základní	k2eAgInSc2d1	základní
kamene	kámen	k1gInSc2	kámen
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Zřízení	zřízení	k1gNnSc1	zřízení
civilního	civilní	k2eAgNnSc2d1	civilní
sídla	sídlo	k1gNnSc2	sídlo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
lokalitě	lokalita	k1gFnSc6	lokalita
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
již	již	k9	již
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
izraelské	izraelský	k2eAgFnSc2d1	izraelská
vlády	vláda	k1gFnSc2	vláda
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Výhledově	výhledově	k6eAd1	výhledově
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
kapacita	kapacita	k1gFnSc1	kapacita
pro	pro	k7c4	pro
875	[number]	k4	875
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
600	[number]	k4	600
<g/>
)	)	kIx)	)
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
zbudování	zbudování	k1gNnSc1	zbudování
sportovních	sportovní	k2eAgInPc2d1	sportovní
areálů	areál	k1gInPc2	areál
<g/>
,	,	kIx,	,
hotelových	hotelový	k2eAgFnPc2d1	hotelová
kapacit	kapacita	k1gFnPc2	kapacita
<g/>
,	,	kIx,	,
úpravny	úpravna	k1gFnPc1	úpravna
na	na	k7c4	na
odsolování	odsolování	k1gNnSc4	odsolování
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
.	.	kIx.	.
</s>
<s>
Obce	obec	k1gFnPc4	obec
je	být	k5eAaImIp3nS	být
jednotně	jednotně	k6eAd1	jednotně
řešena	řešit	k5eAaImNgFnS	řešit
jako	jako	k8xC	jako
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
soubor	soubor	k1gInSc4	soubor
architektury	architektura	k1gFnSc2	architektura
přizpůsobené	přizpůsobený	k2eAgFnSc2d1	přizpůsobená
pouštnímu	pouštní	k2eAgNnSc3d1	pouštní
klimatu	klima	k1gNnSc3	klima
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
tvořili	tvořit	k5eAaImAgMnP	tvořit
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
Be	Be	k1gFnSc6	Be
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
Ora	Ora	k1gFnPc2	Ora
Židé	Žid	k1gMnPc1	Žid
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
statistické	statistický	k2eAgFnSc2d1	statistická
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nearabské	arabský	k2eNgMnPc4d1	nearabský
obyvatele	obyvatel	k1gMnPc4	obyvatel
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
ale	ale	k8xC	ale
bez	bez	k7c2	bez
formální	formální	k2eAgFnSc2d1	formální
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
židovskému	židovský	k2eAgNnSc3d1	Židovské
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
menší	malý	k2eAgFnSc4d2	menší
obec	obec	k1gFnSc4	obec
vesnického	vesnický	k2eAgInSc2d1	vesnický
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
uváděno	uvádět	k5eAaImNgNnS	uvádět
20	[number]	k4	20
žijících	žijící	k2eAgFnPc2d1	žijící
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
již	již	k6eAd1	již
525	[number]	k4	525
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
populace	populace	k1gFnSc1	populace
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
o	o	k7c4	o
33,2	[number]	k4	33,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
oficiální	oficiální	k2eAgInPc4d1	oficiální
vládní	vládní	k2eAgInPc4d1	vládní
statistické	statistický	k2eAgInPc4d1	statistický
výkazy	výkaz	k1gInPc4	výkaz
evidovaly	evidovat	k5eAaImAgFnP	evidovat
Be	Be	k1gFnPc1	Be
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
Ora	Ora	k1gFnSc4	Ora
jako	jako	k8xS	jako
pouhé	pouhý	k2eAgNnSc4d1	pouhé
"	"	kIx"	"
<g/>
místo	místo	k1gNnSc4	místo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
makom	makom	k1gInSc1	makom
<g/>
)	)	kIx)	)
nikoliv	nikoliv	k9	nikoliv
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
