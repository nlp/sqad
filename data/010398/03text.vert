<p>
<s>
Prvky	prvek	k1gInPc1	prvek
2	[number]	k4	2
<g/>
.	.	kIx.	.
periody	perioda	k1gFnSc2	perioda
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
řádku	řádek	k1gInSc2	řádek
(	(	kIx(	(
<g/>
periody	perioda	k1gFnSc2	perioda
<g/>
)	)	kIx)	)
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
soustavě	soustava	k1gFnSc6	soustava
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
PSP	PSP	kA	PSP
je	být	k5eAaImIp3nS	být
psaná	psaný	k2eAgFnSc1d1	psaná
v	v	k7c6	v
periodách	perioda	k1gFnPc6	perioda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
periodické	periodický	k2eAgNnSc1d1	periodické
chování	chování	k1gNnSc1	chování
chemických	chemický	k2eAgFnPc2d1	chemická
vlastností	vlastnost	k1gFnPc2	vlastnost
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
růst	růst	k1gInSc1	růst
protonového	protonový	k2eAgNnSc2d1	protonové
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
perioda	perioda	k1gFnSc1	perioda
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
opakování	opakování	k1gNnSc4	opakování
vlastností	vlastnost	k1gFnPc2	vlastnost
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
prvky	prvek	k1gInPc1	prvek
s	s	k7c7	s
podobnými	podobný	k2eAgFnPc7d1	podobná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
sloupečku	sloupeček	k1gInSc6	sloupeček
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
perioda	perioda	k1gFnSc1	perioda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tyto	tento	k3xDgInPc4	tento
prvky	prvek	k1gInPc4	prvek
<g/>
:	:	kIx,	:
lithium	lithium	k1gNnSc1	lithium
<g/>
,	,	kIx,	,
beryllium	beryllium	k1gNnSc1	beryllium
<g/>
,	,	kIx,	,
bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
uhlík	uhlík	k1gInSc1	uhlík
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
fluor	fluor	k1gInSc1	fluor
a	a	k8xC	a
neon	neon	k1gInSc1	neon
<g/>
.	.	kIx.	.
</s>
<s>
Valenční	valenční	k2eAgInPc4d1	valenční
elektrony	elektron	k1gInPc4	elektron
prvků	prvek	k1gInPc2	prvek
2	[number]	k4	2
<g/>
.	.	kIx.	.
periody	perioda	k1gFnSc2	perioda
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
2	[number]	k4	2
<g/>
s	s	k7c7	s
a	a	k8xC	a
2	[number]	k4	2
<g/>
p	p	k?	p
orbitalech	orbital	k1gInPc6	orbital
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
nemohou	moct	k5eNaImIp3nP	moct
excitovat	excitovat	k5eAaBmF	excitovat
do	do	k7c2	do
d	d	k?	d
orbitalu	orbital	k1gInSc2	orbital
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jejich	jejich	k3xOp3gFnSc4	jejich
reaktivitu	reaktivita	k1gFnSc4	reaktivita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
této	tento	k3xDgFnSc2	tento
periody	perioda	k1gFnSc2	perioda
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
beryllia	beryllium	k1gNnSc2	beryllium
a	a	k8xC	a
neonu	neon	k1gInSc2	neon
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvouatomové	dvouatomový	k2eAgFnPc1d1	dvouatomová
molekuly	molekula	k1gFnPc1	molekula
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Trendy	trend	k1gInPc7	trend
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
==	==	k?	==
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
perioda	perioda	k1gFnSc1	perioda
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
periodou	perioda	k1gFnSc7	perioda
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
periodické	periodický	k2eAgInPc4d1	periodický
trendy	trend	k1gInPc4	trend
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
perioda	perioda	k1gFnSc1	perioda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
prvky	prvek	k1gInPc4	prvek
(	(	kIx(	(
<g/>
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
helium	helium	k1gNnSc4	helium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
nějaké	nějaký	k3yIgMnPc4	nějaký
daly	dát	k5eAaPmAgInP	dát
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
prvků	prvek	k1gInPc2	prvek
2	[number]	k4	2
<g/>
.	.	kIx.	.
periody	perioda	k1gFnSc2	perioda
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
klesá	klesat	k5eAaImIp3nS	klesat
atomový	atomový	k2eAgInSc4d1	atomový
poloměr	poloměr	k1gInSc4	poloměr
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
elektronegativita	elektronegativita	k1gFnSc1	elektronegativita
a	a	k8xC	a
ionizační	ionizační	k2eAgFnSc1d1	ionizační
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
periodě	perioda	k1gFnSc6	perioda
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc1	dva
kovy	kov	k1gInPc1	kov
(	(	kIx(	(
<g/>
lithium	lithium	k1gNnSc1	lithium
a	a	k8xC	a
beryllium	beryllium	k1gNnSc1	beryllium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgInSc4d3	nejmenší
počet	počet	k1gInSc4	počet
kovů	kov	k1gInPc2	kov
v	v	k7c6	v
kterékoliv	kterýkoliv	k3yIgFnSc6	kterýkoliv
periodě	perioda	k1gFnSc6	perioda
kromě	kromě	k7c2	kromě
první	první	k4xOgFnSc2	první
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
tato	tento	k3xDgFnSc1	tento
perioda	perioda	k1gFnSc1	perioda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pět	pět	k4xCc4	pět
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nejvíce	nejvíce	k6eAd1	nejvíce
<g/>
,	,	kIx,	,
nekovů	nekov	k1gInPc2	nekov
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
v	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
periodě	perioda	k1gFnSc6	perioda
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
extrémní	extrémní	k2eAgFnPc1d1	extrémní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
fluor	fluor	k1gInSc1	fluor
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
halogen	halogen	k1gInSc1	halogen
<g/>
,	,	kIx,	,
neon	neon	k1gInSc1	neon
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
inertní	inertní	k2eAgInSc4d1	inertní
vzácný	vzácný	k2eAgInSc4d1	vzácný
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
a	a	k8xC	a
lithium	lithium	k1gNnSc1	lithium
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
alkalický	alkalický	k2eAgInSc1d1	alkalický
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
</p>
