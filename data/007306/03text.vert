<s>
Jinan	jinan	k1gInSc1	jinan
dvoulaločný	dvoulaločný	k2eAgInSc1d1	dvoulaločný
(	(	kIx(	(
<g/>
Ginkgo	Ginkgo	k1gNnSc1	Ginkgo
biloba	biloba	k1gFnSc1	biloba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
také	také	k9	také
ginkgo	ginkgo	k6eAd1	ginkgo
biloba	biloba	k1gFnSc1	biloba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dvoudomý	dvoudomý	k2eAgInSc1d1	dvoudomý
opadavý	opadavý	k2eAgInSc1d1	opadavý
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
druhem	druh	k1gInSc7	druh
rodu	rod	k1gInSc2	rod
jinan	jinan	k1gInSc4	jinan
z	z	k7c2	z
monotypické	monotypický	k2eAgFnSc2d1	monotypický
čeledě	čeleď	k1gFnSc2	čeleď
jinanovitých	jinanovitý	k2eAgInPc2d1	jinanovitý
<g/>
.	.	kIx.	.
</s>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
přísluší	příslušet	k5eAaImIp3nS	příslušet
do	do	k7c2	do
fylogeneticky	fylogeneticky	k6eAd1	fylogeneticky
velmi	velmi	k6eAd1	velmi
prastarého	prastarý	k2eAgInSc2d1	prastarý
řádu	řád	k1gInSc2	řád
jinanotvaré	jinanotvarý	k2eAgInPc1d1	jinanotvarý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
právě	právě	k9	právě
jen	jen	k9	jen
tuto	tento	k3xDgFnSc4	tento
jedinou	jediný	k2eAgFnSc4d1	jediná
čeleď	čeleď	k1gFnSc4	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
již	již	k6eAd1	již
překonaného	překonaný	k2eAgInSc2d1	překonaný
morfologického	morfologický	k2eAgInSc2d1	morfologický
a	a	k8xC	a
anatomického	anatomický	k2eAgInSc2d1	anatomický
způsobu	způsob	k1gInSc2	způsob
zařazování	zařazování	k1gNnSc2	zařazování
flory	flora	k1gFnSc2	flora
byly	být	k5eAaImAgInP	být
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
řazeny	řazen	k2eAgInPc1d1	řazen
taxony	taxon	k1gInPc1	taxon
podobné	podobný	k2eAgInPc1d1	podobný
si	se	k3xPyFc3	se
vzhledem	vzhled	k1gInSc7	vzhled
nebo	nebo	k8xC	nebo
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
následující	následující	k2eAgInSc1d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglosaské	anglosaský	k2eAgFnSc6d1	anglosaská
taxonomické	taxonomický	k2eAgFnSc6d1	Taxonomická
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
Ginkgo	Ginkgo	k6eAd1	Ginkgo
biloba	biloba	k1gFnSc1	biloba
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
samostatného	samostatný	k2eAgNnSc2d1	samostatné
oddělení	oddělení	k1gNnSc2	oddělení
Ginkgophyta	Ginkgophyto	k1gNnSc2	Ginkgophyto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
podporováno	podporovat	k5eAaImNgNnS	podporovat
moderními	moderní	k2eAgInPc7d1	moderní
výsledky	výsledek	k1gInPc7	výsledek
genetických	genetický	k2eAgInPc2d1	genetický
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
literatuře	literatura	k1gFnSc6	literatura
převládá	převládat	k5eAaImIp3nS	převládat
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
oddělení	oddělení	k1gNnSc2	oddělení
slučují	slučovat	k5eAaImIp3nP	slučovat
všechny	všechen	k3xTgFnPc1	všechen
nahosemenné	nahosemenný	k2eAgFnPc1d1	nahosemenná
rostliny	rostlina	k1gFnPc1	rostlina
jako	jako	k8xS	jako
Pinophyta	Pinophyto	k1gNnPc1	Pinophyto
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
;	;	kIx,	;
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
samostatného	samostatný	k2eAgNnSc2d1	samostatné
pododdělení	pododdělení	k1gNnSc2	pododdělení
(	(	kIx(	(
<g/>
subdivisio	subdivisio	k1gNnSc1	subdivisio
<g/>
)	)	kIx)	)
Ginkgophytina	Ginkgophytina	k1gFnSc1	Ginkgophytina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
classis	classis	k1gFnSc1	classis
<g/>
)	)	kIx)	)
Ginkgopsida	Ginkgopsida	k1gFnSc1	Ginkgopsida
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
posledně	posledně	k6eAd1	posledně
jmenovaný	jmenovaný	k2eAgInSc1d1	jmenovaný
přístup	přístup	k1gInSc1	přístup
také	také	k9	také
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
i	i	k9	i
Květena	květena	k1gFnSc1	květena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
lit.	lit.	k?	lit.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
poznáním	poznání	k1gNnSc7	poznání
genetických	genetický	k2eAgFnPc2d1	genetická
zákonitostí	zákonitost	k1gFnPc2	zákonitost
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
moderní	moderní	k2eAgNnSc1d1	moderní
kladistické	kladistický	k2eAgNnSc1d1	kladistický
třídění	třídění	k1gNnSc1	třídění
APG	APG	kA	APG
III	III	kA	III
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
taxony	taxon	k1gInPc1	taxon
setříděny	setříděn	k2eAgInPc1d1	setříděn
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
monofyletického	monofyletický	k2eAgNnSc2d1	monofyletický
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
vycházejícího	vycházející	k2eAgMnSc2d1	vycházející
z	z	k7c2	z
molekulárních	molekulární	k2eAgNnPc2d1	molekulární
dat	datum	k1gNnPc2	datum
genových	genový	k2eAgFnPc2d1	genová
sekvencí	sekvence	k1gFnPc2	sekvence
určitých	určitý	k2eAgInPc2d1	určitý
úseků	úsek	k1gInPc2	úsek
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
genů	gen	k1gInPc2	gen
pro	pro	k7c4	pro
ribozomální	ribozomální	k2eAgFnSc4d1	ribozomální
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
existují	existovat	k5eAaImIp3nP	existovat
4	[number]	k4	4
řády	řád	k1gInPc4	řád
jinanotvaré	jinanotvarý	k2eAgInPc4d1	jinanotvarý
<g/>
,	,	kIx,	,
cykasotvaré	cykasotvarý	k2eAgInPc4d1	cykasotvarý
<g/>
,	,	kIx,	,
borovicotvaré	borovicotvarý	k2eAgInPc4d1	borovicotvarý
a	a	k8xC	a
liánovcotvaré	liánovcotvarý	k2eAgInPc4d1	liánovcotvarý
které	který	k3yRgNnSc1	který
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
do	do	k7c2	do
společné	společný	k2eAgFnSc2d1	společná
třídy	třída	k1gFnSc2	třída
jehličnany	jehličnan	k1gInPc1	jehličnan
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
prehistorické	prehistorický	k2eAgFnPc1d1	prehistorická
stopy	stopa	k1gFnPc1	stopa
po	po	k7c6	po
rostlině	rostlina	k1gFnSc6	rostlina
podobné	podobný	k2eAgFnSc2d1	podobná
jinanu	jinan	k1gInSc6	jinan
vedou	vést	k5eAaImIp3nP	vést
až	až	k9	až
do	do	k7c2	do
geologického	geologický	k2eAgNnSc2d1	geologické
období	období	k1gNnSc2	období
rozhraní	rozhraní	k1gNnSc2	rozhraní
karbonu	karbon	k1gInSc2	karbon
a	a	k8xC	a
permu	perm	k1gInSc2	perm
v	v	k7c6	v
prvohorách	prvohory	k1gFnPc6	prvohory
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
do	do	k7c2	do
období	období	k1gNnSc2	období
asi	asi	k9	asi
před	před	k7c7	před
300	[number]	k4	300
až	až	k8xS	až
270	[number]	k4	270
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tak	tak	k6eAd1	tak
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
si	se	k3xPyFc3	se
jinan	jinan	k1gInSc1	jinan
prošel	projít	k5eAaPmAgInS	projít
svou	svůj	k3xOyFgFnSc7	svůj
érou	éra	k1gFnSc7	éra
širokého	široký	k2eAgNnSc2d1	široké
rozšíření	rozšíření	k1gNnSc2	rozšíření
v	v	k7c6	v
juře	jura	k1gFnSc6	jura
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
200	[number]	k4	200
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gInSc1	jeho
areál	areál	k1gInSc1	areál
zabíral	zabírat	k5eAaImAgInS	zabírat
celou	celý	k2eAgFnSc4d1	celá
Severní	severní	k2eAgFnSc4d1	severní
polokouli	polokoule	k1gFnSc4	polokoule
i	i	k8xC	i
obdobím	období	k1gNnSc7	období
postupného	postupný	k2eAgInSc2d1	postupný
úpadku	úpadek	k1gInSc2	úpadek
až	až	k9	až
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gFnSc4	jeho
posledními	poslední	k2eAgInPc7d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
vymizel	vymizet	k5eAaPmAgMnS	vymizet
před	před	k7c7	před
7	[number]	k4	7
miliony	milion	k4xCgInPc7	milion
<g/>
,	,	kIx,	,
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
před	před	k7c7	před
3	[number]	k4	3
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
současné	současný	k2eAgFnPc4d1	současná
přírodní	přírodní	k2eAgFnPc4d1	přírodní
podmínky	podmínka	k1gFnPc4	podmínka
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
rostlina	rostlina	k1gFnSc1	rostlina
nenáročná	náročný	k2eNgFnSc1d1	nenáročná
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
zachován	zachován	k2eAgMnSc1d1	zachován
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
druhu	druh	k1gInSc6	druh
a	a	k8xC	a
jen	jen	k9	jen
na	na	k7c6	na
malém	malý	k2eAgNnSc6d1	malé
území	území	k1gNnSc6	území
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
nově	nově	k6eAd1	nově
"	"	kIx"	"
<g/>
objeven	objeven	k2eAgInSc1d1	objeven
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jinan	jinan	k1gInSc1	jinan
je	být	k5eAaImIp3nS	být
statný	statný	k2eAgInSc1d1	statný
strom	strom	k1gInSc1	strom
s	s	k7c7	s
kuželovitou	kuželovitý	k2eAgFnSc7d1	kuželovitá
až	až	k8xS	až
rozkladitou	rozkladitý	k2eAgFnSc7d1	rozkladitá
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
30	[number]	k4	30
až	až	k9	až
40	[number]	k4	40
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dlouhověký	dlouhověký	k2eAgInSc1d1	dlouhověký
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
doložený	doložený	k2eAgInSc1d1	doložený
jinan	jinan	k1gInSc1	jinan
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgMnSc1d1	starý
okolo	okolo	k7c2	okolo
4700	[number]	k4	4700
let	léto	k1gNnPc2	léto
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
provincii	provincie	k1gFnSc4	provincie
Guizhou	Guizha	k1gFnSc7	Guizha
<g/>
,	,	kIx,	,
v	v	k7c6	v
okresu	okres	k1gInSc2	okres
Changshun	Changshuno	k1gNnPc2	Changshuno
<g/>
.	.	kIx.	.
</s>
<s>
Borka	borka	k1gFnSc1	borka
kmene	kmen	k1gInSc2	kmen
je	být	k5eAaImIp3nS	být
šedá	šedá	k1gFnSc1	šedá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
široce	široko	k6eAd1	široko
brázditá	brázditý	k2eAgFnSc1d1	brázditá
<g/>
.	.	kIx.	.
</s>
<s>
Větve	větev	k1gFnPc1	větev
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
vodorovně	vodorovně	k6eAd1	vodorovně
odstálé	odstálý	k2eAgNnSc1d1	odstálé
<g/>
.	.	kIx.	.
</s>
<s>
Řapíkaté	řapíkatý	k2eAgInPc1d1	řapíkatý
listy	list	k1gInPc1	list
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
ve	v	k7c6	v
svazečcích	svazeček	k1gInPc6	svazeček
na	na	k7c6	na
silně	silně	k6eAd1	silně
zkrácených	zkrácený	k2eAgFnPc6d1	zkrácená
větvičkách	větvička	k1gFnPc6	větvička
<g/>
.	.	kIx.	.
</s>
<s>
Listová	listový	k2eAgFnSc1d1	listová
čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
,	,	kIx,	,
klínovitá	klínovitý	k2eAgFnSc1d1	klínovitá
<g/>
,	,	kIx,	,
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
zářezem	zářez	k1gInSc7	zářez
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
laloky	lalok	k1gInPc4	lalok
<g/>
,	,	kIx,	,
s	s	k7c7	s
vějířovitou	vějířovitý	k2eAgFnSc7d1	vějířovitá
žilnatinou	žilnatina	k1gFnSc7	žilnatina
<g/>
.	.	kIx.	.
</s>
<s>
Olistění	olistěný	k2eAgMnPc1d1	olistěný
jinanů	jinan	k1gInPc2	jinan
má	mít	k5eAaImIp3nS	mít
cévní	cévní	k2eAgInPc4d1	cévní
svazky	svazek	k1gInPc4	svazek
uspořádané	uspořádaný	k2eAgInPc4d1	uspořádaný
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jehličnany	jehličnan	k1gInPc1	jehličnan
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
bližší	blízký	k2eAgMnPc1d2	bližší
listům	list	k1gInPc3	list
opadavých	opadavý	k2eAgFnPc2d1	opadavá
krytosemenných	krytosemenný	k2eAgFnPc2d1	krytosemenná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nahosemenná	nahosemenný	k2eAgFnSc1d1	nahosemenná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
květy	květ	k1gInPc1	květ
ani	ani	k8xC	ani
plody	plod	k1gInPc1	plod
<g/>
,	,	kIx,	,
vajíčka	vajíčko	k1gNnPc1	vajíčko
nejsou	být	k5eNaImIp3nP	být
chráněna	chráněn	k2eAgMnSc4d1	chráněn
v	v	k7c6	v
květech	květ	k1gInPc6	květ
a	a	k8xC	a
vyvíjející	vyvíjející	k2eAgFnSc1d1	vyvíjející
se	se	k3xPyFc4	se
semena	semeno	k1gNnPc1	semeno
v	v	k7c6	v
plodech	plod	k1gInPc6	plod
<g/>
.	.	kIx.	.
</s>
<s>
Dužnaté	dužnatý	k2eAgInPc4d1	dužnatý
obaly	obal	k1gInPc4	obal
semen	semeno	k1gNnPc2	semeno
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
organické	organický	k2eAgFnPc4d1	organická
kyseliny	kyselina	k1gFnPc4	kyselina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
původcem	původce	k1gMnSc7	původce
zápachu	zápach	k1gInSc2	zápach
i	i	k8xC	i
možného	možný	k2eAgNnSc2d1	možné
podráždění	podráždění	k1gNnSc2	podráždění
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
glyceridy	glycerid	k1gInPc4	glycerid
<g/>
,	,	kIx,	,
steroly	sterol	k1gInPc4	sterol
a	a	k8xC	a
estery	ester	k1gInPc4	ester
<g/>
.	.	kIx.	.
</s>
<s>
Jinan	jinan	k1gInSc1	jinan
je	být	k5eAaImIp3nS	být
dvoudomý	dvoudomý	k2eAgInSc1d1	dvoudomý
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
strom	strom	k1gInSc1	strom
má	mít	k5eAaImIp3nS	mít
samčí	samčí	k2eAgInPc4d1	samčí
rozmnožovací	rozmnožovací	k2eAgInPc4d1	rozmnožovací
orgány	orgán	k1gInPc4	orgán
a	a	k8xC	a
některý	některý	k3yIgInSc1	některý
druhý	druhý	k4xOgInSc1	druhý
zase	zase	k9	zase
samičí	samičí	k2eAgInSc1d1	samičí
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgFnPc1d1	samčí
rostliny	rostlina	k1gFnPc1	rostlina
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
plodolistové	plodolistový	k2eAgInPc4d1	plodolistový
šišticovité	šišticovitý	k2eAgInPc4d1	šišticovitý
útvary	útvar	k1gInPc4	útvar
podobné	podobný	k2eAgInPc4d1	podobný
jehnědám	jehněda	k1gFnPc3	jehněda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samičí	samičí	k2eAgFnPc4d1	samičí
rostliny	rostlina	k1gFnPc4	rostlina
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
shluky	shluk	k1gInPc1	shluk
nazelenalých	nazelenalý	k2eAgInPc2d1	nazelenalý
semeníků	semeník	k1gInPc2	semeník
zavěšených	zavěšený	k2eAgInPc2d1	zavěšený
na	na	k7c6	na
tenké	tenký	k2eAgFnSc6d1	tenká
stopce	stopka	k1gFnSc6	stopka
nesené	nesený	k2eAgMnPc4d1	nesený
krátkým	krátký	k2eAgInSc7d1	krátký
brachyblastem	brachyblast	k1gInSc7	brachyblast
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
dvě	dva	k4xCgFnPc1	dva
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
vajíček	vajíčko	k1gNnPc2	vajíčko
pravidelně	pravidelně	k6eAd1	pravidelně
zakrňuje	zakrňovat	k5eAaImIp3nS	zakrňovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
opylování	opylování	k1gNnSc6	opylování
větrem	vítr	k1gInSc7	vítr
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
pyl	pyl	k1gInSc1	pyl
ze	z	k7c2	z
samčích	samčí	k2eAgFnPc2d1	samčí
rostlin	rostlina	k1gFnPc2	rostlina
na	na	k7c4	na
polinační	polinační	k2eAgFnSc4d1	polinační
kapku	kapka	k1gFnSc4	kapka
vajíčka	vajíčko	k1gNnSc2	vajíčko
samičí	samičí	k2eAgFnSc2d1	samičí
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
dosah	dosah	k1gInSc1	dosah
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pylová	pylový	k2eAgNnPc4d1	pylové
zrna	zrno	k1gNnPc4	zrno
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
vysychání	vysychání	k1gNnSc6	vysychání
kapky	kapka	k1gFnSc2	kapka
vtahována	vtahovat	k5eAaImNgFnS	vtahovat
do	do	k7c2	do
pylové	pylový	k2eAgFnSc2d1	pylová
komory	komora	k1gFnSc2	komora
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
vyklíčené	vyklíčený	k2eAgFnSc2d1	vyklíčený
pylové	pylový	k2eAgFnSc2d1	pylová
láčky	láčka	k1gFnSc2	láčka
vznikají	vznikat	k5eAaImIp3nP	vznikat
bičíkaté	bičíkatý	k2eAgInPc1d1	bičíkatý
spermatozoidy	spermatozoid	k1gInPc1	spermatozoid
<g/>
,	,	kIx,	,
k	k	k7c3	k
oplození	oplození	k1gNnSc3	oplození
vaječné	vaječný	k2eAgFnSc2d1	vaječná
buňky	buňka	k1gFnSc2	buňka
dochází	docházet	k5eAaImIp3nS	docházet
až	až	k9	až
po	po	k7c6	po
dozrání	dozrání	k1gNnSc6	dozrání
a	a	k8xC	a
odpadnutí	odpadnutí	k1gNnSc6	odpadnutí
semene	semeno	k1gNnSc2	semeno
ze	z	k7c2	z
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vajíčku	vajíčko	k1gNnSc6	vajíčko
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
semeno	semeno	k1gNnSc4	semeno
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
obaleno	obalit	k5eAaPmNgNnS	obalit
dužnatým	dužnatý	k2eAgInSc7d1	dužnatý
obalem	obal	k1gInSc7	obal
a	a	k8xC	a
připomíná	připomínat	k5eAaImIp3nS	připomínat
plod	plod	k1gInSc4	plod
třešně	třešeň	k1gFnSc2	třešeň
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
má	mít	k5eAaImIp3nS	mít
dužinu	dužina	k1gFnSc4	dužina
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k6eAd1	uvnitř
sklerotické	sklerotický	k2eAgNnSc1d1	sklerotické
jádro	jádro	k1gNnSc1	jádro
pod	pod	k7c7	pod
nimž	jenž	k3xRgMnPc3	jenž
je	být	k5eAaImIp3nS	být
škrobnaté	škrobnatý	k2eAgNnSc4d1	škrobnatý
živné	živný	k2eAgNnSc4d1	živné
pletivo	pletivo	k1gNnSc4	pletivo
s	s	k7c7	s
dvouděložným	dvouděložný	k2eAgNnSc7d1	dvouděložný
embryem	embryo	k1gNnSc7	embryo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
opylení	opylení	k1gNnSc3	opylení
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k8xS	až
červnu	červen	k1gInSc6	červen
<g/>
,	,	kIx,	,
semeno	semeno	k1gNnSc4	semeno
dozrává	dozrávat	k5eAaImIp3nS	dozrávat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
až	až	k8xS	až
listopadu	listopad	k1gInSc6	listopad
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
barvu	barva	k1gFnSc4	barva
ze	z	k7c2	z
zelena	zeleno	k1gNnSc2	zeleno
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
opylenými	opylený	k2eAgNnPc7d1	opylený
semeny	semeno	k1gNnPc7	semeno
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
současně	současně	k6eAd1	současně
i	i	k9	i
neopylená	neopylená	k1gFnSc1	neopylená
<g/>
,	,	kIx,	,
oboje	oboj	k1gFnPc1	oboj
nelze	lze	k6eNd1	lze
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
pohledem	pohled	k1gInSc7	pohled
odlišit	odlišit	k5eAaPmF	odlišit
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
začínají	začínat	k5eAaImIp3nP	začínat
plodit	plodit	k5eAaImF	plodit
až	až	k9	až
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vegetativně	vegetativně	k6eAd1	vegetativně
může	moct	k5eAaImIp3nS	moct
druh	druh	k1gInSc4	druh
být	být	k5eAaImF	být
množen	množit	k5eAaImNgInS	množit
z	z	k7c2	z
nevyzrálých	vyzrálý	k2eNgInPc2d1	nevyzrálý
řízků	řízek	k1gInPc2	řízek
(	(	kIx(	(
<g/>
15	[number]	k4	15
cm	cm	kA	cm
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
<g/>
)	)	kIx)	)
odebraných	odebraný	k2eAgInPc2d1	odebraný
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
/	/	kIx~	/
<g/>
srpnu	srpen	k1gInSc6	srpen
a	a	k8xC	a
zasazených	zasazený	k2eAgMnPc2d1	zasazený
do	do	k7c2	do
pařeniště	pařeniště	k1gNnSc2	pařeniště
se	s	k7c7	s
spodním	spodní	k2eAgNnSc7d1	spodní
vyhříváním	vyhřívání	k1gNnSc7	vyhřívání
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
vyzrálých	vyzrálý	k2eAgInPc2d1	vyzrálý
řízků	řízek	k1gInPc2	řízek
(	(	kIx(	(
<g/>
15	[number]	k4	15
až	až	k9	až
30	[number]	k4	30
cm	cm	kA	cm
<g/>
)	)	kIx)	)
odebraných	odebraný	k2eAgInPc2d1	odebraný
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
které	který	k3yIgNnSc4	který
uložíme	uložit	k5eAaPmIp1nP	uložit
do	do	k7c2	do
pařeniště	pařeniště	k1gNnSc2	pařeniště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
příští	příští	k2eAgNnSc4d1	příští
jaro	jaro	k1gNnSc4	jaro
zakoření	zakořenit	k5eAaPmIp3nS	zakořenit
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ho	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
množit	množit	k5eAaImF	množit
roubováním	roubování	k1gNnSc7	roubování
na	na	k7c4	na
semenáče	semenáč	k1gInPc4	semenáč
nebo	nebo	k8xC	nebo
hřížením	hřížení	k1gNnSc7	hřížení
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
vegetativního	vegetativní	k2eAgNnSc2d1	vegetativní
množení	množení	k1gNnSc2	množení
je	být	k5eAaImIp3nS	být
především	především	k9	především
záruka	záruka	k1gFnSc1	záruka
žádaného	žádaný	k2eAgNnSc2d1	žádané
pohlaví	pohlaví	k1gNnSc2	pohlaví
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vlastnosti	vlastnost	k1gFnPc4	vlastnost
odrůdy	odrůda	k1gFnPc4	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Dřevina	dřevina	k1gFnSc1	dřevina
je	být	k5eAaImIp3nS	být
oblíbena	oblíbit	k5eAaPmNgFnS	oblíbit
pro	pro	k7c4	pro
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
tvar	tvar	k1gInSc4	tvar
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
list	list	k1gInSc1	list
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dřevina	dřevina	k1gFnSc1	dřevina
za	za	k7c4	za
kultovní	kultovní	k2eAgFnSc4d1	kultovní
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
poměrně	poměrně	k6eAd1	poměrně
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
asi	asi	k9	asi
30	[number]	k4	30
cm	cm	kA	cm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
nesnáší	snášet	k5eNaImIp3nS	snášet
trvalý	trvalý	k2eAgInSc4d1	trvalý
stín	stín	k1gInSc4	stín
nebo	nebo	k8xC	nebo
zamokřené	zamokřený	k2eAgFnPc4d1	zamokřená
půdy	půda	k1gFnPc4	půda
<g/>
,	,	kIx,	,
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
mu	on	k3xPp3gMnSc3	on
teplejší	teplý	k2eAgMnPc1d2	teplejší
stanoviště	stanoviště	k1gNnSc2	stanoviště
<g/>
,	,	kIx,	,
živné	živný	k2eAgFnSc2d1	živná
humózní	humózní	k2eAgFnSc2d1	humózní
<g/>
,	,	kIx,	,
propustné	propustný	k2eAgFnSc2d1	propustná
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
snáší	snášet	k5eAaImIp3nS	snášet
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odolný	odolný	k2eAgInSc1d1	odolný
proti	proti	k7c3	proti
průmyslovým	průmyslový	k2eAgFnPc3d1	průmyslová
exhalacím	exhalace	k1gFnPc3	exhalace
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
závažné	závažný	k2eAgFnPc4d1	závažná
choroby	choroba	k1gFnPc4	choroba
ani	ani	k8xC	ani
škůdce	škůdce	k1gMnPc4	škůdce
<g/>
.	.	kIx.	.
</s>
<s>
Snese	snést	k5eAaPmIp3nS	snést
i	i	k9	i
mrazy	mráz	k1gInPc4	mráz
přes	přes	k7c4	přes
-20	-20	k4	-20
°	°	k?	°
<g/>
C.	C.	kA	C.
Se	s	k7c7	s
starými	starý	k2eAgInPc7d1	starý
exempláři	exemplář	k1gInPc7	exemplář
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
spíše	spíše	k9	spíše
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
než	než	k8xS	než
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
bývá	bývat	k5eAaImIp3nS	bývat
pěstován	pěstovat	k5eAaImNgInS	pěstovat
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
Koruny	koruna	k1gFnPc1	koruna
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
téměř	téměř	k6eAd1	téměř
pravidelně	pravidelně	k6eAd1	pravidelně
větvené	větvený	k2eAgInPc1d1	větvený
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
velmi	velmi	k6eAd1	velmi
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parcích	park	k1gInPc6	park
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
převážně	převážně	k6eAd1	převážně
samčí	samčí	k2eAgFnPc1d1	samčí
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
pěstování	pěstování	k1gNnSc2	pěstování
samčích	samčí	k2eAgInPc2d1	samčí
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
alergenní	alergenní	k2eAgInSc1d1	alergenní
pyl	pyl	k1gInSc1	pyl
<g/>
.	.	kIx.	.
</s>
<s>
Oplodí	oplodí	k1gNnSc1	oplodí
zralých	zralý	k2eAgNnPc2d1	zralé
semen	semeno	k1gNnPc2	semeno
silně	silně	k6eAd1	silně
zapáchají	zapáchat	k5eAaImIp3nP	zapáchat
po	po	k7c6	po
fekáliích	fekálie	k1gFnPc6	fekálie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkovýrobě	velkovýroba	k1gFnSc6	velkovýroba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jednoduše	jednoduše	k6eAd1	jednoduše
množen	množit	k5eAaImNgInS	množit
výsevem	výsev	k1gInSc7	výsev
semene	semeno	k1gNnSc2	semeno
nebo	nebo	k8xC	nebo
vegetativně	vegetativně	k6eAd1	vegetativně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
se	se	k3xPyFc4	se
semen	semeno	k1gNnPc2	semeno
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
tradičních	tradiční	k2eAgFnPc2d1	tradiční
kuchyní	kuchyně	k1gFnPc2	kuchyně
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
se	se	k3xPyFc4	se
zbaví	zbavit	k5eAaPmIp3nP	zbavit
dužiny	dužina	k1gFnPc1	dužina
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
máčí	máčet	k5eAaImIp3nS	máčet
ve	v	k7c6	v
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
praží	pražit	k5eAaImIp3nP	pražit
a	a	k8xC	a
prodávají	prodávat	k5eAaImIp3nP	prodávat
k	k	k7c3	k
přímé	přímý	k2eAgFnSc3d1	přímá
spotřebě	spotřeba	k1gFnSc3	spotřeba
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
do	do	k7c2	do
dezertů	dezert	k1gInPc2	dezert
<g/>
.	.	kIx.	.
</s>
<s>
Toxické	toxický	k2eAgFnPc1d1	toxická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
při	při	k7c6	při
úpravě	úprava	k1gFnSc6	úprava
rozkládány	rozkládán	k2eAgFnPc4d1	rozkládána
teplem	teplo	k1gNnSc7	teplo
a	a	k8xC	a
při	při	k7c6	při
požití	požití	k1gNnSc6	požití
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
slané	slaný	k2eAgFnSc2d1	slaná
pochoutky	pochoutka	k1gFnSc2	pochoutka
nebo	nebo	k8xC	nebo
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
užívání	užívání	k1gNnSc6	užívání
<g/>
,	,	kIx,	,
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
otravy	otrava	k1gFnPc1	otrava
<g/>
,	,	kIx,	,
především	především	k9	především
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahradní	zahradní	k2eAgFnSc6d1	zahradní
architektuře	architektura	k1gFnSc6	architektura
je	být	k5eAaImIp3nS	být
jinan	jinan	k1gInSc4	jinan
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
používaný	používaný	k2eAgInSc1d1	používaný
okrasný	okrasný	k2eAgInSc1d1	okrasný
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
býval	bývat	k5eAaImAgInS	bývat
vysázen	vysázet	k5eAaPmNgInS	vysázet
jako	jako	k8xS	jako
alej	alej	k1gFnSc1	alej
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
malebně	malebně	k6eAd1	malebně
nepravidelné	pravidelný	k2eNgFnPc1d1	nepravidelná
<g/>
,	,	kIx,	,
široké	široký	k2eAgFnPc1d1	široká
<g/>
,	,	kIx,	,
prosychající	prosychající	k2eAgFnPc1d1	prosychající
a	a	k8xC	a
řídké	řídký	k2eAgFnPc1d1	řídká
koruny	koruna	k1gFnPc1	koruna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tyto	tento	k3xDgFnPc1	tento
starší	starý	k2eAgFnPc1d2	starší
dřeviny	dřevina	k1gFnPc1	dřevina
v	v	k7c6	v
pouličních	pouliční	k2eAgFnPc6d1	pouliční
alejích	alej	k1gFnPc6	alej
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
méně	málo	k6eAd2	málo
častý	častý	k2eAgInSc4d1	častý
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pouličních	pouliční	k2eAgFnPc6d1	pouliční
alejích	alej	k1gFnPc6	alej
jsou	být	k5eAaImIp3nP	být
ceněny	ceněn	k2eAgFnPc1d1	ceněna
dřeviny	dřevina	k1gFnPc1	dřevina
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
nebo	nebo	k8xC	nebo
úzkými	úzký	k2eAgFnPc7d1	úzká
<g/>
,	,	kIx,	,
pravidelnými	pravidelný	k2eAgFnPc7d1	pravidelná
korunami	koruna	k1gFnPc7	koruna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
jen	jen	k9	jen
mladé	mladý	k2eAgFnPc4d1	mladá
rostliny	rostlina	k1gFnPc4	rostlina
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
štíhlé	štíhlý	k2eAgInPc1d1	štíhlý
kultivary	kultivar	k1gInPc1	kultivar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přes	přes	k7c4	přes
90	[number]	k4	90
kultivarů	kultivar	k1gInPc2	kultivar
a	a	k8xC	a
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
třetina	třetina	k1gFnSc1	třetina
se	se	k3xPyFc4	se
komerčně	komerčně	k6eAd1	komerčně
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
jinan	jinan	k1gInSc1	jinan
dvoulaločný	dvoulaločný	k2eAgInSc1d1	dvoulaločný
často	často	k6eAd1	často
pěstován	pěstován	k2eAgInSc1d1	pěstován
jako	jako	k8xS	jako
dřevina	dřevina	k1gFnSc1	dřevina
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
v	v	k7c6	v
asijském	asijský	k2eAgNnSc6d1	asijské
zahradním	zahradní	k2eAgNnSc6d1	zahradní
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
pěstování	pěstování	k1gNnSc4	pěstování
bonsají	bonsaj	k1gFnPc2	bonsaj
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
pro	pro	k7c4	pro
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
tvar	tvar	k1gInSc4	tvar
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
stromem	strom	k1gInSc7	strom
japonského	japonský	k2eAgNnSc2d1	Japonské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Tokia	Tokio	k1gNnSc2	Tokio
<g/>
,	,	kIx,	,
symbolem	symbol	k1gInSc7	symbol
Tokia	Tokio	k1gNnSc2	Tokio
je	být	k5eAaImIp3nS	být
list	list	k1gInSc1	list
ginkga	ginkg	k1gMnSc2	ginkg
<g/>
.	.	kIx.	.
</s>
<s>
Ginkgo	Ginkgo	k6eAd1	Ginkgo
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nabízen	nabízet	k5eAaImNgInS	nabízet
jako	jako	k8xS	jako
léčivo	léčivo	k1gNnSc1	léčivo
pro	pro	k7c4	pro
farmakologické	farmakologický	k2eAgNnSc4d1	farmakologické
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sbírán	sbírán	k2eAgInSc1d1	sbírán
a	a	k8xC	a
prodáván	prodáván	k2eAgInSc1d1	prodáván
list	list	k1gInSc1	list
(	(	kIx(	(
<g/>
Folium	Folium	k1gNnSc1	Folium
ginkgo	ginkgo	k6eAd1	ginkgo
bilobae	bilobae	k6eAd1	bilobae
<g/>
)	)	kIx)	)
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
buď	buď	k8xC	buď
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
krátce	krátce	k6eAd1	krátce
vařeného	vařený	k2eAgInSc2d1	vařený
odvaru	odvar	k1gInSc2	odvar
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tinktury	tinktura	k1gFnSc2	tinktura
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
prodejců	prodejce	k1gMnPc2	prodejce
a	a	k8xC	a
biolib	bioliba	k1gFnPc2	bioliba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
cévy	céva	k1gFnPc4	céva
<g/>
,	,	kIx,	,
preventivně	preventivně	k6eAd1	preventivně
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
používat	používat	k5eAaImF	používat
proti	proti	k7c3	proti
náhlým	náhlý	k2eAgFnPc3d1	náhlá
mozkovým	mozkový	k2eAgFnPc3d1	mozková
příhodám	příhoda	k1gFnPc3	příhoda
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
infarktu	infarkt	k1gInSc3	infarkt
myokardu	myokard	k1gInSc2	myokard
i	i	k9	i
proti	proti	k7c3	proti
poškození	poškození	k1gNnSc3	poškození
buněk	buňka	k1gFnPc2	buňka
vznikající	vznikající	k2eAgFnSc1d1	vznikající
při	při	k7c6	při
degenerativních	degenerativní	k2eAgFnPc6d1	degenerativní
nemocích	nemoc	k1gFnPc6	nemoc
spojených	spojený	k2eAgInPc2d1	spojený
se	s	k7c7	s
stářím	stáří	k1gNnSc7	stáří
<g/>
,	,	kIx,	,
otocích	otok	k1gInPc6	otok
mozku	mozek	k1gInSc2	mozek
atd.	atd.	kA	atd.
Ovšem	ovšem	k9	ovšem
podle	podle	k7c2	podle
široké	široký	k2eAgFnSc2d1	široká
studie	studie	k1gFnSc2	studie
National	National	k1gMnSc2	National
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Complementary	Complementara	k1gFnSc2	Complementara
and	and	k?	and
Alternative	Alternativ	k1gInSc5	Alternativ
Medicine	Medicin	k1gMnSc5	Medicin
a	a	k8xC	a
National	National	k1gMnSc5	National
Institute	institut	k1gInSc5	institut
on	on	k3xPp3gMnSc1	on
Aging	Aging	k1gMnSc1	Aging
of	of	k?	of
the	the	k?	the
National	National	k1gMnSc1	National
Institutes	Institutes	k1gMnSc1	Institutes
of	of	k?	of
Health	Health	k1gMnSc1	Health
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc4	žádný
prokázané	prokázaný	k2eAgInPc4d1	prokázaný
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgInPc4	žádný
prokázané	prokázaný	k2eAgInPc4d1	prokázaný
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
prokázanou	prokázaný	k2eAgFnSc4d1	prokázaná
účinnost	účinnost	k1gFnSc4	účinnost
proti	proti	k7c3	proti
srdečním	srdeční	k2eAgFnPc3d1	srdeční
chorobám	choroba	k1gFnPc3	choroba
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ani	ani	k8xC	ani
proti	proti	k7c3	proti
infarktu	infarkt	k1gInSc3	infarkt
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nechrání	chránit	k5eNaImIp3nS	chránit
kognitivní	kognitivní	k2eAgFnSc1d1	kognitivní
funkce	funkce	k1gFnSc1	funkce
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
není	být	k5eNaImIp3nS	být
prevencí	prevence	k1gFnSc7	prevence
proti	proti	k7c3	proti
demenci	demence	k1gFnSc3	demence
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
ani	ani	k8xC	ani
proti	proti	k7c3	proti
žádnému	žádný	k3yNgInSc3	žádný
typu	typ	k1gInSc3	typ
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
IARC	IARC	kA	IARC
výtažek	výtažek	k1gInSc1	výtažek
(	(	kIx(	(
<g/>
Ginkgo	Ginkgo	k6eAd1	Ginkgo
biloba	biloba	k1gFnSc1	biloba
extract	extracta	k1gFnPc2	extracta
<g/>
)	)	kIx)	)
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k8xC	jako
možná	možný	k2eAgFnSc1d1	možná
karcinogenní	karcinogenní	k2eAgFnSc1d1	karcinogenní
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
přípravků	přípravek	k1gInPc2	přípravek
s	s	k7c7	s
drogou	droga	k1gFnSc7	droga
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
řadu	řada	k1gFnSc4	řada
kontraindikací	kontraindikace	k1gFnPc2	kontraindikace
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
nabízeny	nabízet	k5eAaImNgInP	nabízet
potravinové	potravinový	k2eAgInPc1d1	potravinový
doplňky	doplněk	k1gInPc1	doplněk
z	z	k7c2	z
jinanu	jinan	k1gInSc2	jinan
dvoulaločného	dvoulaločný	k2eAgInSc2d1	dvoulaločný
při	při	k7c6	při
poruchách	porucha	k1gFnPc6	porucha
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
nedokrvení	nedokrvení	k1gNnSc1	nedokrvení
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
při	při	k7c6	při
Alzheimerově	Alzheimerův	k2eAgFnSc6d1	Alzheimerova
chorobě	choroba	k1gFnSc6	choroba
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
průběh	průběh	k1gInSc1	průběh
údajně	údajně	k6eAd1	údajně
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
impotenci	impotence	k1gFnSc6	impotence
způsobené	způsobený	k2eAgNnSc1d1	způsobené
zhoršeným	zhoršený	k2eAgInSc7d1	zhoršený
průtokem	průtok	k1gInSc7	průtok
krve	krev	k1gFnSc2	krev
tepnami	tepna	k1gFnPc7	tepna
penisu	penis	k1gInSc2	penis
<g/>
,	,	kIx,	,
při	při	k7c6	při
hluchotě	hluchota	k1gFnSc6	hluchota
způsobené	způsobený	k2eAgNnSc1d1	způsobené
nedostatečným	dostatečný	k2eNgInSc7d1	nedostatečný
průtokem	průtok	k1gInSc7	průtok
krve	krev	k1gFnSc2	krev
ke	k	k7c3	k
sluchovým	sluchový	k2eAgInPc3d1	sluchový
nervům	nerv	k1gInPc3	nerv
<g/>
,	,	kIx,	,
při	při	k7c6	při
chronickém	chronický	k2eAgNnSc6d1	chronické
pískání	pískání	k1gNnSc6	pískání
v	v	k7c6	v
uších	ucho	k1gNnPc6	ucho
i	i	k8xC	i
při	při	k7c6	při
chronické	chronický	k2eAgFnSc6d1	chronická
závrati	závrať	k1gFnSc6	závrať
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
droga	droga	k1gFnSc1	droga
také	také	k9	také
zcela	zcela	k6eAd1	zcela
neúčinná	účinný	k2eNgNnPc1d1	neúčinné
proti	proti	k7c3	proti
Alzheimerově	Alzheimerův	k2eAgFnSc3d1	Alzheimerova
chorobě	choroba	k1gFnSc3	choroba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
měla	mít	k5eAaImAgFnS	mít
údajně	údajně	k6eAd1	údajně
mírnit	mírnit	k5eAaImF	mírnit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
NutritionBusinessJournal	NutritionBusinessJournal	k1gFnSc2	NutritionBusinessJournal
se	se	k3xPyFc4	se
však	však	k9	však
jen	jen	k9	jen
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
prodalo	prodat	k5eAaPmAgNnS	prodat
výrobků	výrobek	k1gInPc2	výrobek
z	z	k7c2	z
Ginkgo	Ginkgo	k6eAd1	Ginkgo
biloby	biloba	k1gFnPc4	biloba
za	za	k7c4	za
107	[number]	k4	107
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
tradiční	tradiční	k2eAgFnSc6d1	tradiční
medicíně	medicína	k1gFnSc6	medicína
je	být	k5eAaImIp3nS	být
jinan	jinan	k1gInSc1	jinan
používán	používat	k5eAaImNgInS	používat
po	po	k7c6	po
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
astmatu	astma	k1gNnSc2	astma
<g/>
,	,	kIx,	,
zažívacích	zažívací	k2eAgFnPc2d1	zažívací
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
kašle	kašel	k1gInSc2	kašel
<g/>
,	,	kIx,	,
omrzlin	omrzlina	k1gFnPc2	omrzlina
a	a	k8xC	a
senility	senilita	k1gFnSc2	senilita
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
tygrů	tygr	k1gMnPc2	tygr
nebo	nebo	k8xC	nebo
nosorožců	nosorožec	k1gMnPc2	nosorožec
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
národní	národní	k2eAgNnSc4d1	národní
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
60	[number]	k4	60
biologicky	biologicky	k6eAd1	biologicky
aktivních	aktivní	k2eAgFnPc2d1	aktivní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
patří	patřit	k5eAaImIp3nS	patřit
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
Terpentýny	terpentýn	k1gInPc1	terpentýn
–	–	k?	–
bilobalid	bilobalid	k1gInSc1	bilobalid
<g/>
,	,	kIx,	,
ginkgolid	ginkgolid	k1gInSc1	ginkgolid
Flavonoidy	flavonoid	k1gInPc1	flavonoid
–	–	k?	–
amentoflavon	amentoflavon	k1gInSc1	amentoflavon
<g/>
,	,	kIx,	,
bilobetin	bilobetin	k2eAgInSc1d1	bilobetin
<g/>
,	,	kIx,	,
sekvoiaflavon	sekvoiaflavon	k1gInSc1	sekvoiaflavon
<g/>
,	,	kIx,	,
kvercetin	kvercetin	k2eAgInSc1d1	kvercetin
<g/>
,	,	kIx,	,
kamferol	kamferol	k1gInSc1	kamferol
<g/>
,	,	kIx,	,
ginkgetin	ginkgetin	k2eAgInSc1d1	ginkgetin
Karboxylové	karboxylový	k2eAgFnSc2d1	karboxylová
kyseliny	kyselina	k1gFnSc2	kyselina
–	–	k?	–
kyselina	kyselina	k1gFnSc1	kyselina
6	[number]	k4	6
<g/>
-hydroxykynurenová	ydroxykynurenová	k1gFnSc1	-hydroxykynurenová
Vitaminy	vitamin	k1gInPc1	vitamin
–	–	k?	–
kyselina	kyselina	k1gFnSc1	kyselina
L-askorbová	Lskorbová	k1gFnSc1	L-askorbová
(	(	kIx(	(
<g/>
vitamin	vitamin	k1gInSc1	vitamin
C	C	kA	C
<g/>
)	)	kIx)	)
</s>
<s>
Jinan	jinan	k1gInSc4	jinan
dvoulaločný	dvoulaločný	k2eAgInSc4d1	dvoulaločný
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
Stínadla	stínadlo	k1gNnSc2	stínadlo
se	se	k3xPyFc4	se
bouří	bouřit	k5eAaImIp3nS	bouřit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
Vontů	Vonta	k1gMnPc2	Vonta
zvaná	zvaný	k2eAgNnPc1d1	zvané
Uctívači	uctívač	k1gMnSc3	uctívač
ginga	ginga	k1gFnSc1	ginga
užívá	užívat	k5eAaImIp3nS	užívat
listy	list	k1gInPc4	list
ginkga	ginkga	k1gFnSc1	ginkga
jako	jako	k8xC	jako
odznak	odznak	k1gInSc1	odznak
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
ginga	ginga	k1gFnSc1	ginga
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
trilogii	trilogie	k1gFnSc6	trilogie
o	o	k7c6	o
Uctívačích	uctívač	k1gMnPc6	uctívač
ginga	ginga	k1gFnSc1	ginga
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Hrnčíř	Hrnčíř	k1gMnSc1	Hrnčíř
a	a	k8xC	a
navazuje	navazovat	k5eAaImIp3nS	navazovat
tak	tak	k6eAd1	tak
po	po	k7c6	po
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
let	léto	k1gNnPc2	léto
na	na	k7c4	na
příběhy	příběh	k1gInPc4	příběh
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goethe	k1gFnSc1	Goethe
(	(	kIx(	(
<g/>
1749	[number]	k4	1749
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
nádherným	nádherný	k2eAgInSc7d1	nádherný
vzhledem	vzhled	k1gInSc7	vzhled
stromu	strom	k1gInSc2	strom
a	a	k8xC	a
krásou	krása	k1gFnSc7	krása
jeho	on	k3xPp3gNnSc2	on
listí	listí	k1gNnSc2	listí
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
básně	báseň	k1gFnSc2	báseň
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Ginkgo	Ginkgo	k1gNnSc1	Ginkgo
biloba	biloba	k1gFnSc1	biloba
<g/>
.	.	kIx.	.
</s>
<s>
Zářez	zářez	k1gInSc1	zářez
rozdělující	rozdělující	k2eAgInSc4d1	rozdělující
list	list	k1gInSc4	list
ve	v	k7c4	v
dvě	dva	k4xCgFnPc4	dva
stejné	stejný	k2eAgFnPc4d1	stejná
části	část	k1gFnPc4	část
symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
jednotu	jednota	k1gFnSc4	jednota
dvou	dva	k4xCgMnPc2	dva
milujících	milující	k2eAgMnPc2d1	milující
se	se	k3xPyFc4	se
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Moštěnský	Moštěnský	k2eAgInSc1d1	Moštěnský
jinan	jinan	k1gInSc1	jinan
Osvračínský	Osvračínský	k2eAgInSc1d1	Osvračínský
jinan	jinan	k1gInSc4	jinan
Rokycanský	rokycanský	k2eAgInSc4d1	rokycanský
jinan	jinan	k1gInSc4	jinan
Celkem	celkem	k6eAd1	celkem
AOPK	AOPK	kA	AOPK
eviduje	evidovat	k5eAaImIp3nS	evidovat
47	[number]	k4	47
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
název	název	k1gInSc1	název
rodu	rod	k1gInSc2	rod
Ginkgo	Ginkgo	k6eAd1	Ginkgo
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
koncem	koncem	k7c2	koncem
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
nepřesným	přesný	k2eNgInSc7d1	nepřesný
přepisem	přepis	k1gInSc7	přepis
čínského	čínský	k2eAgNnSc2d1	čínské
jména	jméno	k1gNnSc2	jméno
銀	銀	k?	銀
(	(	kIx(	(
<g/>
Yínxì	Yínxì	k1gFnSc1	Yínxì
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
jin-sing	jining	k1gInSc1	jin-sing
<g/>
)	)	kIx)	)
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ginkyo	ginkyo	k1gMnSc1	ginkyo
<g/>
"	"	kIx"	"
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
ginkgo	ginkgo	k6eAd1	ginkgo
<g/>
"	"	kIx"	"
německým	německý	k2eAgMnSc7d1	německý
lékařem	lékař	k1gMnSc7	lékař
a	a	k8xC	a
botanikem	botanik	k1gMnSc7	botanik
Engelbertem	Engelbert	k1gMnSc7	Engelbert
Kaempferem	Kaempfer	k1gMnSc7	Kaempfer
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Amoenitatum	Amoenitatum	k1gNnSc1	Amoenitatum
xoticarum	xoticarum	k1gNnSc1	xoticarum
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Lemgo	Lemgo	k1gMnSc1	Lemgo
<g/>
,	,	kIx,	,
1712	[number]	k4	1712
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
tento	tento	k3xDgMnSc1	tento
druh	druh	k1gMnSc1	druh
jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
zmínil	zmínit	k5eAaPmAgInS	zmínit
<g/>
;	;	kIx,	;
původní	původní	k2eAgNnSc4d1	původní
čínské	čínský	k2eAgNnSc4d1	čínské
jméno	jméno	k1gNnSc4	jméno
v	v	k7c6	v
doslovném	doslovný	k2eAgInSc6d1	doslovný
překladu	překlad	k1gInSc6	překlad
značí	značit	k5eAaImIp3nS	značit
"	"	kIx"	"
<g/>
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
meruňka	meruňka	k1gFnSc1	meruňka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhové	druhový	k2eAgNnSc1d1	druhové
adjektivum	adjektivum	k1gNnSc1	adjektivum
biloba	biloba	k1gFnSc1	biloba
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
ke	k	k7c3	k
tvaru	tvar	k1gInSc3	tvar
listů	list	k1gInPc2	list
<g/>
;	;	kIx,	;
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
bis	bis	k?	bis
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc1	dva
<g/>
,	,	kIx,	,
dvojí	dvojit	k5eAaImIp3nS	dvojit
<g/>
)	)	kIx)	)
a	a	k8xC	a
lobos	lobos	k1gInSc4	lobos
(	(	kIx(	(
<g/>
lalok	lalok	k1gInSc4	lalok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převodem	převod	k1gInSc7	převod
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
české	český	k2eAgNnSc1d1	české
druhové	druhový	k2eAgNnSc1d1	druhové
označení	označení	k1gNnSc1	označení
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
plody	plod	k1gInPc4	plod
i	i	k8xC	i
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
meruňka	meruňka	k1gFnSc1	meruňka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
