<s>
Metoda	metoda	k1gFnSc1
ztraceného	ztracený	k2eAgInSc2d1
vosku	vosk	k1gInSc2
</s>
<s>
Metoda	metoda	k1gFnSc1
ztraceného	ztracený	k2eAgInSc2d1
vosku	vosk	k1gInSc2
(	(	kIx(
<g/>
nebo	nebo	k8xC
ztracené	ztracený	k2eAgFnSc2d1
formy	forma	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
metoda	metoda	k1gFnSc1
odlévání	odlévání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
protože	protože	k8xS
<g/>
,	,	kIx,
se	se	k3xPyFc4
po	po	k7c6
odlití	odlití	k1gNnSc6
forma	forma	k1gFnSc1
zničí	zničit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opak	opak	k1gInSc1
je	být	k5eAaImIp3nS
odlévání	odlévání	k1gNnSc1
do	do	k7c2
pevné	pevný	k2eAgFnSc2d1
formy	forma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Postup	postup	k1gInSc1
</s>
<s>
Z	z	k7c2
tvrdého	tvrdý	k2eAgInSc2d1
vosku	vosk	k1gInSc2
se	se	k3xPyFc4
vyrobí	vyrobit	k5eAaPmIp3nS
trojrozměrný	trojrozměrný	k2eAgInSc1d1
pozitiv	pozitiv	k1gInSc1
odlévaného	odlévaný	k2eAgInSc2d1
předmětu	předmět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pozitiv	pozitiv	k1gMnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
nastříká	nastříkat	k5eAaPmIp3nS
tenkou	tenký	k2eAgFnSc7d1
cementační	cementační	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
a	a	k8xC
poté	poté	k6eAd1
několikrát	několikrát	k6eAd1
namáčí	namáčet	k5eAaImIp3nS
ve	v	k7c6
speciální	speciální	k2eAgFnSc6d1
keramice	keramika	k1gFnSc6
a	a	k8xC
nechá	nechat	k5eAaPmIp3nS
se	se	k3xPyFc4
zaschnout	zaschnout	k5eAaPmF
<g/>
,	,	kIx,
dokud	dokud	k8xS
nevznikne	vzniknout	k5eNaPmIp3nS
masivní	masivní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Potom	potom	k6eAd1
se	se	k3xPyFc4
nechá	nechat	k5eAaPmIp3nS
vosk	vosk	k1gInSc1
roztát	roztát	k5eAaPmF
a	a	k8xC
vylije	vylít	k5eAaPmIp3nS
se	se	k3xPyFc4
z	z	k7c2
formy	forma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevadí	vadit	k5eNaImIp3nS
když	když	k8xS
něco	něco	k3yInSc1
zůstane	zůstat	k5eAaPmIp3nS
uvnitř	uvnitř	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Potom	potom	k6eAd1
se	se	k3xPyFc4
forma	forma	k1gFnSc1
pomalu	pomalu	k6eAd1
nažhaví	nažhavit	k5eAaPmIp3nS
na	na	k7c4
tavnou	tavný	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
v	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
peci	pec	k1gFnSc6
jako	jako	k9
tavenina	tavenina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytky	zbytek	k1gInPc1
vosku	vosk	k1gInSc2
se	se	k3xPyFc4
jednoduše	jednoduše	k6eAd1
vypaří	vypařit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Potom	potom	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
formy	forma	k1gFnSc2
nalije	nalít	k5eAaBmIp3nS,k5eAaPmIp3nS
tavenina	tavenina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tavenina	tavenina	k1gFnSc1
(	(	kIx(
<g/>
odlitek	odlitek	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nechá	nechat	k5eAaPmIp3nS
ztuhnout	ztuhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
je	být	k5eAaImIp3nS
odlitek	odlitek	k1gInSc4
ztuhlý	ztuhlý	k2eAgInSc4d1
<g/>
,	,	kIx,
forma	forma	k1gFnSc1
se	se	k3xPyFc4
rozbije	rozbít	k5eAaPmIp3nS
a	a	k8xC
práce	práce	k1gFnSc1
je	být	k5eAaImIp3nS
hotová	hotový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Kde	kde	k6eAd1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
masovost	masovost	k1gFnSc4
a	a	k8xC
vysokou	vysoký	k2eAgFnSc4d1
přesnost	přesnost	k1gFnSc4
(	(	kIx(
<g/>
až	až	k9
IT	IT	kA
<g/>
8	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
minimálním	minimální	k2eAgNnSc7d1
nebo	nebo	k8xC
nulovým	nulový	k2eAgNnSc7d1
dopracováním	dopracování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
ručních	ruční	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Metóda	Metóda	k1gMnSc1
strateného	stratený	k2eAgInSc2d1
vosku	vosk	k1gInSc2
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Poloztracená	Poloztracený	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Metoda	metoda	k1gFnSc1
ztraceného	ztracený	k2eAgInSc2d1
vosku	vosk	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4188794-3	4188794-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85106232	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85106232	#num#	k4
</s>
