<s>
Děkanát	děkanát	k1gInSc1
Bílovec	Bílovec	k1gInSc1
</s>
<s>
Děkanát	děkanát	k1gInSc1
BílovecDiecéze	BílovecDiecéza	k1gFnSc6
</s>
<s>
Ostrava-Opava	Ostrava-Opava	k1gFnSc1
Provincie	provincie	k1gFnSc2
</s>
<s>
Morava	Morava	k1gFnSc1
Děkan	děkan	k1gMnSc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mgr.	Mgr.	kA
Dariusz	Dariusza	k1gFnPc2
Adam	Adam	k1gMnSc1
Jędrzejski	Jędrzejsk	k1gFnSc2
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
děkana	děkan	k1gMnSc2
</s>
<s>
farář	farář	k1gMnSc1
v	v	k7c6
Bílovci	Bílovec	k1gInSc6
Místoděkan	Místoděkana	k1gFnPc2
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Kuník	Kuník	k1gMnSc1
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgFnSc2d1
k	k	k7c3
prosinci	prosinec	k1gInSc3
2020	#num#	k4
</s>
<s>
Znak	znak	k1gInSc4
děkanátu	děkanát	k1gInSc2
Bílovec	Bílovec	k1gInSc1
</s>
<s>
Děkanát	děkanát	k1gInSc1
Bílovec	Bílovec	k1gInSc1
je	být	k5eAaImIp3nS
územní	územní	k2eAgFnSc1d1
část	část	k1gFnSc1
ostravsko-opavské	ostravsko-opavský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jej	on	k3xPp3gMnSc4
tvoří	tvořit	k5eAaImIp3nS
24	#num#	k4
římskokatolických	římskokatolický	k2eAgFnPc2d1
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Děkanem	děkan	k1gMnSc7
je	být	k5eAaImIp3nS
P.	P.	kA
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mgr.	Mgr.	kA
Dariusz	Dariusza	k1gFnPc2
Adam	Adam	k1gMnSc1
Jędrzejski	Jędrzejsk	k1gFnSc2
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
v	v	k7c6
Bílovci	Bílovec	k1gInSc6
<g/>
,	,	kIx,
místoděkanem	místoděkan	k1gInSc7
je	být	k5eAaImIp3nS
P.	P.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Kuník	Kuník	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
v	v	k7c6
Odrách	Odra	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Bílovecký	Bílovecký	k2eAgInSc1d1
děkanát	děkanát	k1gInSc1
k	k	k7c3
roku	rok	k1gInSc3
2013	#num#	k4
</s>
<s>
Bílovecký	Bílovecký	k2eAgInSc1d1
děkanát	děkanát	k1gInSc1
k	k	k7c3
roku	rok	k1gInSc3
1859	#num#	k4
(	(	kIx(
<g/>
f.	f.	k?
<g/>
=	=	kIx~
<g/>
farnost	farnost	k1gFnSc1
<g/>
,	,	kIx,
l.k.	l.k.	k?
<g/>
=	=	kIx~
lokální	lokální	k2eAgFnPc1d1
kuracie	kuracie	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Farnosti	farnost	k1gFnPc1
děkanátu	děkanát	k1gInSc2
</s>
<s>
FarnostSprávceDalší	FarnostSprávceDalší	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
ve	v	k7c4
farnostiFarní	farnostiFarní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
</s>
<s>
Bílov	Bílov	k1gInSc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mgr.	Mgr.	kA
Dariusz	Dariusza	k1gFnPc2
Adam	Adam	k1gMnSc1
Jędrzejski	Jędrzejsk	k1gFnSc2
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Vavřince	Vavřinec	k1gMnSc2
</s>
<s>
Bílovec	Bílovec	k1gInSc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mgr.	Mgr.	kA
Dariusz	Dariusza	k1gFnPc2
Adam	Adam	k1gMnSc1
Jędrzejski	Jędrzejsk	k1gFnSc2
</s>
<s>
Ing.	ing.	kA
Mgr.	Mgr.	kA
Alois	Alois	k1gMnSc1
Pinďák	Pinďák	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
</s>
<s>
Bravantice	Bravantice	k1gFnSc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Petr	Petr	k1gMnSc1
Kukiola	Kukiola	k1gFnSc1
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Valentina	Valentin	k1gMnSc4
</s>
<s>
Dobešov	Dobešov	k1gInSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Kuník	Kuník	k1gMnSc1
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
</s>
<s>
Fulnek	Fulnek	k1gMnSc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mgr.	Mgr.	kA
Józef	Józef	k1gInSc1
Wojciech	Wojciech	k1gInSc1
Gruba	Gruba	k1gFnSc1
<g/>
,	,	kIx,
SAC	SAC	kA
</s>
<s>
P.	P.	kA
Jan	Jan	k1gMnSc1
David	David	k1gMnSc1
<g/>
,	,	kIx,
SAC	SAC	kA
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Hladké	Hladké	k2eAgFnSc1d1
Životice	Životice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Bc.	Bc.	k1gMnSc1
Martin	Martin	k1gMnSc1
Sudora	Sudora	k1gFnSc1
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
</s>
<s>
Jistebník	Jistebník	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Zdeněk	Zdeněk	k1gMnSc1
Pluhař	Pluhař	k1gMnSc1
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
</s>
<s>
Klimkovice	Klimkovice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Rudolf	Rudolf	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
</s>
<s>
svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
</s>
<s>
Kujavy	Kujava	k1gFnPc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Bc.	Bc.	k1gMnSc1
Martin	Martin	k1gMnSc1
Sudora	Sudora	k1gFnSc1
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Michaela	Michael	k1gMnSc4
archanděla	archanděl	k1gMnSc4
</s>
<s>
Lubojaty	Lubojata	k1gFnPc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Petr	Petr	k1gMnSc1
Kukiola	Kukiola	k1gFnSc1
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc1
</s>
<s>
Mankovice	Mankovice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Kuníkadministrátor	Kuníkadministrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Navštívení	navštívení	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Odry	Odra	k1gFnPc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Kuník	Kuník	k1gMnSc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Staněk	Staněk	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgInSc1d1
vikářP	vikářP	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michal	Michal	k1gMnSc1
Krenželok	Krenželok	k1gInSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
<g/>
,	,	kIx,
kaplan	kaplan	k1gMnSc1
pro	pro	k7c4
církevní	církevní	k2eAgFnPc4d1
školy	škola	k1gFnPc4
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
Olbramice	Olbramika	k1gFnSc3
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Petr	Petr	k1gMnSc1
Kukiola	Kukiola	k1gFnSc1
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
Pohoř	pohořet	k5eAaPmRp2nS
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Kuník	Kuník	k1gMnSc1
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Prokopa	Prokop	k1gMnSc2
</s>
<s>
Pustějov	Pustějov	k1gInSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Bc.	Bc.	k1gMnSc1
Martin	Martin	k1gMnSc1
Sudora	Sudora	k1gFnSc1
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svaté	svatý	k2eAgFnPc4d1
Máří	Máří	k?
Magdalény	Magdaléna	k1gFnPc4
</s>
<s>
Slatina	slatina	k1gFnSc1
u	u	k7c2
Bílovce	Bílovec	k1gInSc2
</s>
<s>
P.	P.	kA
PaedDr	PaedDr	kA
<g/>
.	.	kIx.
Mgr.	Mgr.	kA
Piotr	Piotr	k1gMnSc1
Marek	Marek	k1gMnSc1
Kowalski	Kowalske	k1gFnSc4
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Spálov	Spálov	k1gInSc1
</s>
<s>
P.	P.	kA
Bohumil	Bohumil	k1gMnSc1
Vícha	Vích	k1gMnSc2
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Jakuba	Jakub	k1gMnSc2
Většího	veliký	k2eAgInSc2d2
</s>
<s>
Studénka	studénka	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Mieczysław	Mieczysław	k1gMnSc2
Serednicki	Serednick	k1gFnSc2
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
Studénka-Butovice	Studénka-Butovice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Mieczysław	Mieczysław	k1gMnSc2
Serednicki	Serednick	k1gFnSc2
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Všech	všecek	k3xTgMnPc2
svatých	svatý	k1gMnPc2
</s>
<s>
Suchdol	Suchdol	k1gInSc1
nad	nad	k7c7
Odrou	Odra	k1gFnSc7
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Ladislav	Ladislav	k1gMnSc1
Stanečka	Staneček	k1gMnSc2
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Těškovice	Těškovice	k1gFnSc1
</s>
<s>
P.	P.	kA
PaedDr	PaedDr	kA
<g/>
.	.	kIx.
Mgr.	Mgr.	kA
Piotr	Piotr	k1gMnSc1
Marek	Marek	k1gMnSc1
Kowalski	Kowalske	k1gFnSc4
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Velké	velký	k2eAgFnPc1d1
Albrechtice	Albrechtice	k1gFnPc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mgr.	Mgr.	kA
Dariusz	Dariusza	k1gFnPc2
Adam	Adam	k1gMnSc1
Jędrzejski	Jędrzejsk	k1gFnSc2
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
</s>
<s>
Veselí	veselí	k1gNnSc1
u	u	k7c2
Oder	Odra	k1gFnPc2
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Kuník	Kuník	k1gMnSc1
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Véska	Véska	k6eAd1
</s>
<s>
P.	P.	kA
Bohumil	Bohumil	k1gMnSc1
Vícha	Vích	k1gMnSc2
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Navštívení	navštívení	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Vlkovice	Vlkovice	k1gFnSc1
</s>
<s>
P.	P.	kA
Bohumil	Bohumil	k1gMnSc1
Vícha	Vích	k1gMnSc2
<g/>
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
</s>
<s>
Vražné	vražný	k2eAgNnSc1d1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Ladislav	Ladislav	k1gMnSc1
Stanečkaadministrátor	Stanečkaadministrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
</s>
<s>
Stav	stav	k1gInSc1
k	k	k7c3
roku	rok	k1gInSc3
2020	#num#	k4
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Děkanát	děkanát	k1gInSc1
Bílovec	Bílovec	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
roku	rok	k1gInSc2
1670	#num#	k4
vydělením	vydělení	k1gNnPc3
z	z	k7c2
velkého	velký	k2eAgInSc2d1
děkanátu	děkanát	k1gInSc2
Opava	Opava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
dnešní	dnešní	k2eAgFnSc3d1
rozloze	rozloha	k1gFnSc3
zahrnoval	zahrnovat	k5eAaImAgInS
území	území	k1gNnSc4
mezi	mezi	k7c7
Odrou	Odra	k1gFnSc7
a	a	k8xC
Opavicí	Opavice	k1gFnSc7
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
na	na	k7c6
Hradecku	Hradecko	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
dnes	dnes	k6eAd1
náleží	náležet	k5eAaImIp3nS
děkanátům	děkanát	k1gInPc3
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Hlučín	Hlučín	k1gInSc1
a	a	k8xC
Opava	Opava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
farnosti	farnost	k1gFnPc1
na	na	k7c6
Odersku	Odersek	k1gInSc6
spadaly	spadat	k5eAaImAgInP,k5eAaPmAgInP
pod	pod	k7c4
děkanát	děkanát	k1gInSc4
Lipník	Lipník	k1gInSc4
nad	nad	k7c7
Bečvou	Bečva	k1gFnSc7
a	a	k8xC
farnost	farnost	k1gFnSc1
Suchdol	Suchdola	k1gFnPc2
nad	nad	k7c7
Odrou	Odra	k1gFnSc7
pod	pod	k7c4
děkanát	děkanát	k1gInSc4
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1700	#num#	k4
<g/>
–	–	k?
<g/>
1723	#num#	k4
byl	být	k5eAaImAgInS
děkanát	děkanát	k1gInSc1
přičleněn	přičlenit	k5eAaPmNgInS
k	k	k7c3
děkanátu	děkanát	k1gInSc3
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1723	#num#	k4
byl	být	k5eAaImAgInS
však	však	k9
děkanát	děkanát	k1gInSc1
v	v	k7c6
Moravské	moravský	k2eAgFnSc6d1
Ostravě	Ostrava	k1gFnSc6
zcela	zcela	k6eAd1
zrušen	zrušit	k5eAaPmNgInS
a	a	k8xC
děkanát	děkanát	k1gInSc1
Bílovec	Bílovec	k1gInSc1
obnoven	obnoven	k2eAgInSc1d1
v	v	k7c6
původním	původní	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
některé	některý	k3yIgFnSc6
farnosti	farnost	k1gFnSc6
přišel	přijít	k5eAaPmAgInS
děkanát	děkanát	k1gInSc1
Bílovec	Bílovec	k1gInSc1
se	se	k3xPyFc4
vznikem	vznik	k1gInSc7
děkanátu	děkanát	k1gInSc2
Odry	Odra	k1gFnSc2
roku	rok	k1gInSc2
1731	#num#	k4
a	a	k8xC
pak	pak	k6eAd1
děkanátu	děkanát	k1gInSc2
Hradec	Hradec	k1gInSc1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1780	#num#	k4
<g/>
,	,	kIx,
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
však	však	k9
–	–	k?
v	v	k7c6
rámci	rámec	k1gInSc6
úprav	úprava	k1gFnPc2
hranic	hranice	k1gFnPc2
děkanátů	děkanát	k1gInPc2
podle	podle	k7c2
státních	státní	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
–	–	k?
získal	získat	k5eAaPmAgMnS
některé	některý	k3yIgFnPc4
farnosti	farnost	k1gFnPc4
děkanátu	děkanát	k1gInSc2
Hlučín	Hlučína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1784	#num#	k4
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
rámci	rámec	k1gInSc6
vznikaly	vznikat	k5eAaImAgFnP
četné	četný	k2eAgFnPc1d1
nové	nový	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
rozsah	rozsah	k1gInSc1
se	se	k3xPyFc4
však	však	k9
podstatně	podstatně	k6eAd1
nezměnil	změnit	k5eNaPmAgInS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
jeho	jeho	k3xOp3gFnPc1
hranice	hranice	k1gFnPc1
upraveny	upravit	k5eAaPmNgFnP
v	v	k7c6
rámci	rámec	k1gInSc6
reorganizace	reorganizace	k1gFnSc2
děkanátů	děkanát	k1gInPc2
sledující	sledující	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
územní	územní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatnější	podstatný	k2eAgFnSc4d2
změnu	změna	k1gFnSc4
přinesla	přinést	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
děkanátů	děkanát	k1gInPc2
v	v	k7c6
olomoucké	olomoucký	k2eAgFnSc6d1
arcidiecézi	arcidiecéze	k1gFnSc6
platná	platný	k2eAgFnSc1d1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
sledovala	sledovat	k5eAaImAgFnS
novou	nový	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
okresů	okres	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
vymezený	vymezený	k2eAgInSc4d1
děkanát	děkanát	k1gInSc4
Bílovec	Bílovec	k1gInSc1
zahrnoval	zahrnovat	k5eAaImAgInS
severní	severní	k2eAgFnSc4d1
polovinu	polovina	k1gFnSc4
okresu	okres	k1gInSc2
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
rozsahu	rozsah	k1gInSc6
zůstal	zůstat	k5eAaPmAgInS
děkanát	děkanát	k1gInSc1
dosud	dosud	k6eAd1
<g/>
,	,	kIx,
jedinou	jediný	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
je	být	k5eAaImIp3nS
připojení	připojení	k1gNnSc1
farnosti	farnost	k1gFnSc2
Těškovice	Těškovice	k1gFnSc2
<g/>
,	,	kIx,
provedené	provedený	k2eAgFnSc6d1
po	po	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
;	;	kIx,
vedle	vedle	k7c2
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
2004	#num#	k4
zanikla	zaniknout	k5eAaPmAgFnS
farnost	farnost	k1gFnSc1
Vrchy	vrch	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Děkanát	děkanát	k1gInSc1
patřil	patřit	k5eAaImAgInS
k	k	k7c3
diecézi	diecéze	k1gFnSc3
<g/>
,	,	kIx,
resp.	resp.	kA
od	od	k7c2
roku	rok	k1gInSc2
1777	#num#	k4
k	k	k7c3
arcidiecézi	arcidiecéze	k1gFnSc3
olomoucké	olomoucký	k2eAgNnSc1d1
až	až	k9
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1996	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
připojen	připojit	k5eAaPmNgInS
k	k	k7c3
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc3d1
diecézi	diecéze	k1gFnSc3
ostravsko-opavské	ostravsko-opavský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
organizace	organizace	k1gFnSc2
arcikněžství	arcikněžství	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
v	v	k7c6
olomoucké	olomoucký	k2eAgFnSc6d1
arcidiecézi	arcidiecéze	k1gFnSc6
existovala	existovat	k5eAaImAgFnS
v	v	k7c6
letech	let	k1gInPc6
1778	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
,	,	kIx,
patřil	patřit	k5eAaImAgInS
od	od	k7c2
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1778	#num#	k4
do	do	k7c2
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1787	#num#	k4
k	k	k7c3
arcikněžství	arcikněžství	k1gNnSc3
Opava	Opava	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1787	#num#	k4
<g/>
–	–	k?
<g/>
1866	#num#	k4
k	k	k7c3
arcikněžství	arcikněžství	k1gNnSc3
Příbor	Příbor	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1866	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
opět	opět	k6eAd1
k	k	k7c3
arcikněžství	arcikněžství	k1gNnSc3
Opava	Opava	k1gFnSc1
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1952	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
k	k	k7c3
arcikněžství	arcikněžství	k1gNnSc3
Ostrava	Ostrava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Farnosti	farnost	k1gFnPc1
historicky	historicky	k6eAd1
patřící	patřící	k2eAgInPc4d1
k	k	k7c3
děkanátu	děkanát	k1gInSc3
Bílovec	Bílovec	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Bílov	Bílov	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1784	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Bílovec	Bílovec	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
děkanátu	děkanát	k1gInSc2
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Bravantice	Bravantice	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
děkanátu	děkanát	k1gInSc2
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Březová	březový	k2eAgFnSc1d1
(	(	kIx(
<g/>
u	u	k7c2
Vítkova	Vítkov	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
děkanátu	děkanát	k1gInSc2
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
cca	cca	kA
1780	#num#	k4
<g/>
,	,	kIx,
pak	pak	k6eAd1
děkanát	děkanát	k1gInSc1
Hradec	Hradec	k1gInSc1
nad	nad	k7c7
Moravicí	Moravice	k1gFnSc7
</s>
<s>
Dobešov	Dobešov	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
</s>
<s>
Fulnek	Fulnek	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
děkanátu	děkanát	k1gInSc2
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
roku	rok	k1gInSc2
1731	#num#	k4
a	a	k8xC
opět	opět	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
mezitím	mezitím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
</s>
<s>
Háj	háj	k1gInSc1
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Chabičov	Chabičov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
1952	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
děkanát	děkanát	k1gInSc1
Hlučín	Hlučína	k1gFnPc2
</s>
<s>
Hladké	Hladké	k2eAgFnSc1d1
Životice	Životice	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1784	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1952	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
děkanát	děkanát	k1gInSc1
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
</s>
<s>
Hrabyně	Hrabyně	k1gFnSc1
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1780	#num#	k4
<g/>
;	;	kIx,
do	do	k7c2
1780	#num#	k4
děkanát	děkanát	k1gInSc1
Hlučín	Hlučín	k1gInSc4
<g/>
,	,	kIx,
1780	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
děkanát	děkanát	k1gInSc1
Hradec	Hradec	k1gInSc1
nad	nad	k7c7
Moravicí	Moravice	k1gFnSc7
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
děkanát	děkanát	k1gInSc1
Opava	Opava	k1gFnSc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
děkanát	děkanát	k1gInSc1
Hlučín	Hlučína	k1gFnPc2
</s>
<s>
Jistebník	Jistebník	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1784	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Klimkovice	Klimkovice	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
děkanátu	děkanát	k1gInSc2
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Kujavy	Kujava	k1gFnPc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
děkanátu	děkanát	k1gInSc2
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
přelomu	přelom	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
dosud	dosud	k6eAd1
<g/>
,	,	kIx,
mezitím	mezitím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
</s>
<s>
Lubojaty	Lubojat	k1gInPc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1784	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Mankovice	Mankovice	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
</s>
<s>
Moravice	Moravice	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
děkanátu	děkanát	k1gInSc2
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
cca	cca	kA
1780	#num#	k4
<g/>
,	,	kIx,
pak	pak	k6eAd1
děkanát	děkanát	k1gInSc1
Hradec	Hradec	k1gInSc1
nad	nad	k7c7
Moravicí	Moravice	k1gFnSc7
</s>
<s>
Odry	odr	k1gInPc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
</s>
<s>
Olbramice	Olbramika	k1gFnSc3
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
(	(	kIx(
<g/>
Stará	starý	k2eAgFnSc1d1
<g/>
)	)	kIx)
Plesná	plesný	k2eAgFnSc1d1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
1952	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
děkanát	děkanát	k1gInSc1
Hlučín	Hlučína	k1gFnPc2
</s>
<s>
Pohoř	pohořet	k5eAaPmRp2nS
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
a	a	k8xC
děkanát	děkanát	k1gInSc4
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
</s>
<s>
Polanka	polanka	k1gFnSc1
(	(	kIx(
<g/>
nad	nad	k7c7
Odrou	Odra	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poté	poté	k6eAd1
děkanát	děkanát	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Poruba	Poruba	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1780	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
děkanát	děkanát	k1gInSc1
Hlučín	Hlučína	k1gFnPc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
děkanát	děkanát	k1gInSc1
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Pustá	pustý	k2eAgFnSc1d1
Polom	polom	k1gInSc1
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1780	#num#	k4
a	a	k8xC
pak	pak	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1952	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
;	;	kIx,
do	do	k7c2
1780	#num#	k4
děkanát	děkanát	k1gInSc1
Hlučín	Hlučín	k1gInSc4
<g/>
,	,	kIx,
1780	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
děkanát	děkanát	k1gInSc1
Hradec	Hradec	k1gInSc1
nad	nad	k7c7
Moravicí	Moravice	k1gFnSc7
<g/>
,	,	kIx,
po	po	k7c6
1962	#num#	k4
děkanát	děkanát	k1gInSc1
Hlučín	Hlučín	k1gInSc4
</s>
<s>
Pustějov	Pustějov	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1784	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Radkov	Radkov	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
děkanátu	děkanát	k1gInSc2
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
cca	cca	kA
1780	#num#	k4
<g/>
,	,	kIx,
pak	pak	k6eAd1
děkanát	děkanát	k1gInSc1
Hradec	Hradec	k1gInSc1
nad	nad	k7c7
Moravicí	Moravice	k1gFnSc7
</s>
<s>
Skřipov	Skřipov	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1952	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
děkanát	děkanát	k1gInSc1
Hradec	Hradec	k1gInSc1
nad	nad	k7c7
Moravicí	Moravice	k1gFnSc7
<g/>
,	,	kIx,
později	pozdě	k6eAd2
děkanát	děkanát	k1gInSc1
Hlučín	Hlučína	k1gFnPc2
</s>
<s>
Slatina	slatina	k1gFnSc1
(	(	kIx(
<g/>
u	u	k7c2
Bílovce	Bílovec	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
děkanátu	děkanát	k1gInSc2
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Spálov	Spálov	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
</s>
<s>
Studénka	studénka	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
děkanátu	děkanát	k1gInSc2
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Studénka-Butovice	Studénka-Butovice	k1gFnSc1
(	(	kIx(
<g/>
Butovice	Butovice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1784	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Suchdol	Suchdol	k1gInSc1
(	(	kIx(
<g/>
nad	nad	k7c7
Odrou	Odra	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
</s>
<s>
Svinov	Svinov	k1gInSc1
(	(	kIx(
<g/>
nad	nad	k7c7
Odrou	Odra	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
1952	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
děkanát	děkanát	k1gInSc1
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Těškovice	Těškovice	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1952	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
a	a	k8xC
opět	opět	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
dosud	dosud	k6eAd1
<g/>
;	;	kIx,
před	před	k7c4
1952	#num#	k4
děkanát	děkanát	k1gInSc4
Hradec	Hradec	k1gInSc4
nad	nad	k7c7
Moravicí	Moravice	k1gFnSc7
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
–	–	k?
<g/>
po	po	k7c6
1996	#num#	k4
děkanát	děkanát	k1gInSc1
Hlučín	Hlučín	k1gInSc4
</s>
<s>
Třebovice	Třebovice	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
1952	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
děkanát	děkanát	k1gInSc1
Hlučín	Hlučína	k1gFnPc2
</s>
<s>
Velká	velká	k1gFnSc1
Polom	polom	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1780	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
a	a	k8xC
poté	poté	k6eAd1
děkanát	děkanát	k1gInSc1
Hlučín	Hlučína	k1gFnPc2
</s>
<s>
Velké	velký	k2eAgFnPc1d1
Albrechtice	Albrechtice	k1gFnPc1
<g/>
,	,	kIx,
od	od	k7c2
založení	založení	k1gNnSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
1784	#num#	k4
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
</s>
<s>
Veselí	veselí	k1gNnSc1
(	(	kIx(
<g/>
u	u	k7c2
Oder	Odra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
</s>
<s>
Véska	Véska	k6eAd1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
</s>
<s>
Vlkovice	Vlkovice	k1gFnPc1
(	(	kIx(
<g/>
Dolejší	Dolejší	k2eAgFnPc1d1
Kunčice	Kunčice	k1gFnPc1
<g/>
,	,	kIx,
Kunčice	Kunčice	k1gFnPc1
u	u	k7c2
Oder	Odra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
Dolní	dolní	k2eAgInPc1d1
<g/>
)	)	kIx)
Vražné	vražný	k2eAgInPc1d1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
a	a	k8xC
děkanát	děkanát	k1gInSc4
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
</s>
<s>
Vrchy	vrch	k1gInPc1
(	(	kIx(
<g/>
Valtéřovice	Valtéřovice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
děkanát	děkanát	k1gInSc1
Odry	Odra	k1gFnSc2
<g/>
,	,	kIx,
zrušena	zrušen	k2eAgFnSc1d1
roku	rok	k1gInSc2
2004	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Stránky	stránka	k1gFnPc1
děkanátu	děkanát	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Děkanáty	děkanát	k1gInPc1
Diecéze	diecéze	k1gFnSc2
ostravsko-opavské	ostravsko-opavský	k2eAgInPc1d1
</s>
<s>
Bílovec	Bílovec	k1gInSc1
•	•	k?
Bruntál	Bruntál	k1gInSc1
•	•	k?
Frýdek	Frýdek	k1gInSc1
•	•	k?
Hlučín	Hlučín	k1gInSc1
•	•	k?
Jeseník	Jeseník	k1gInSc1
•	•	k?
Karviná	Karviná	k1gFnSc1
•	•	k?
Krnov	Krnov	k1gInSc1
•	•	k?
Místek	Místek	k1gInSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
•	•	k?
Opava	Opava	k1gFnSc1
•	•	k?
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Farnosti	farnost	k1gFnPc1
děkanátu	děkanát	k1gInSc2
Bílovec	Bílovec	k1gInSc1
</s>
<s>
Bílov	Bílov	k1gInSc1
•	•	k?
Bílovec	Bílovec	k1gInSc1
•	•	k?
Bravantice	Bravantice	k1gFnSc2
•	•	k?
Dobešov	Dobešov	k1gInSc1
•	•	k?
Fulnek	Fulnky	k1gFnPc2
•	•	k?
Hladké	Hladké	k2eAgFnSc2d1
Životice	Životice	k1gFnSc2
•	•	k?
Jistebník	Jistebník	k1gMnSc1
•	•	k?
Klimkovice	Klimkovice	k1gFnSc2
•	•	k?
Kujavy	Kujava	k1gFnSc2
•	•	k?
Lubojaty	Lubojata	k1gFnSc2
•	•	k?
Mankovice	Mankovice	k1gFnSc2
•	•	k?
Odry	Odra	k1gFnSc2
•	•	k?
Olbramice	Olbramika	k1gFnSc6
•	•	k?
Pohoř	pohořet	k5eAaPmRp2nS
•	•	k?
Pustějov	Pustějov	k1gInSc1
•	•	k?
Slatina	slatina	k1gFnSc1
u	u	k7c2
Bílovce	Bílovec	k1gInSc2
•	•	k?
Spálov	Spálov	k1gInSc1
•	•	k?
Studénka	studénka	k1gFnSc1
•	•	k?
Studénka-Butovice	Studénka-Butovice	k1gFnSc2
•	•	k?
Suchdol	Suchdol	k1gInSc1
nad	nad	k7c7
Odrou	Odra	k1gFnSc7
•	•	k?
Těškovice	Těškovice	k1gFnSc2
•	•	k?
Velké	velký	k2eAgFnPc1d1
Albrechtice	Albrechtice	k1gFnPc1
•	•	k?
Véska	Véska	k1gFnSc1
•	•	k?
Veselí	veselí	k1gNnSc1
u	u	k7c2
Oder	Odra	k1gFnPc2
•	•	k?
Vlkovice	Vlkovice	k1gFnSc2
•	•	k?
Vražné	vražný	k2eAgNnSc1d1
•	•	k?
Vrchy	vrch	k1gInPc5
</s>
