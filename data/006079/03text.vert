<s>
Čtyřvektor	Čtyřvektor	k1gMnSc1	Čtyřvektor
je	on	k3xPp3gFnPc4	on
analogie	analogie	k1gFnPc4	analogie
pojmu	pojmout	k5eAaPmIp1nS	pojmout
vektor	vektor	k1gInSc1	vektor
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c4	v
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
a	a	k8xC	a
čas	čas	k1gInSc1	čas
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
jediný	jediný	k2eAgInSc1d1	jediný
celek	celek	k1gInSc1	celek
<g/>
,	,	kIx,	,
čtyřrozměrný	čtyřrozměrný	k2eAgInSc1d1	čtyřrozměrný
časoprostor	časoprostor	k1gInSc1	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
vektorová	vektorový	k2eAgFnSc1d1	vektorová
veličina	veličina	k1gFnSc1	veličina
(	(	kIx(	(
<g/>
trojice	trojice	k1gFnSc1	trojice
reálných	reálný	k2eAgFnPc2d1	reálná
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přirozeně	přirozeně	k6eAd1	přirozeně
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
číselnou	číselný	k2eAgFnSc7d1	číselná
veličinou	veličina	k1gFnSc7	veličina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
říkáme	říkat	k5eAaImIp1nP	říkat
časová	časový	k2eAgFnSc1d1	časová
složka	složka	k1gFnSc1	složka
čtyřvektoru	čtyřvektor	k1gInSc2	čtyřvektor
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
objekt	objekt	k1gInSc1	objekt
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Formálněji	formálně	k6eAd2	formálně
je	být	k5eAaImIp3nS	být
čtyřvektor	čtyřvektor	k1gInSc4	čtyřvektor
prvek	prvek	k1gInSc4	prvek
čtyřrozměrného	čtyřrozměrný	k2eAgInSc2d1	čtyřrozměrný
reálného	reálný	k2eAgInSc2d1	reálný
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Minkowského	Minkowský	k2eAgInSc2d1	Minkowský
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
totožný	totožný	k2eAgInSc4d1	totožný
s	s	k7c7	s
časoprostorem	časoprostor	k1gInSc7	časoprostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Složky	složka	k1gFnPc1	složka
čtyřvektoru	čtyřvektor	k1gInSc2	čtyřvektor
se	se	k3xPyFc4	se
při	při	k7c6	při
Lorentzových	Lorentzův	k2eAgFnPc6d1	Lorentzova
transformacích	transformace	k1gFnPc6	transformace
<g/>
,	,	kIx,	,
rotacích	rotace	k1gFnPc6	rotace
a	a	k8xC	a
translacích	translace	k1gFnPc6	translace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
(	(	kIx(	(
<g/>
zcela	zcela	k6eAd1	zcela
obecné	obecný	k2eAgFnSc2d1	obecná
<g/>
)	)	kIx)	)
inerciální	inerciální	k2eAgFnSc2d1	inerciální
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
do	do	k7c2	do
jiné	jiná	k1gFnSc2	jiná
<g/>
,	,	kIx,	,
transformují	transformovat	k5eAaBmIp3nP	transformovat
jako	jako	k9	jako
vektory	vektor	k1gInPc1	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
transformace	transformace	k1gFnPc1	transformace
tvoří	tvořit	k5eAaImIp3nP	tvořit
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
spojitou	spojitý	k2eAgFnSc4d1	spojitá
grupu	grupa	k1gFnSc4	grupa
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Poincarého	Poincarý	k2eAgNnSc2d1	Poincaré
grupu	grupa	k1gFnSc4	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřvektory	Čtyřvektor	k1gInPc1	Čtyřvektor
hrají	hrát	k5eAaImIp3nP	hrát
roli	role	k1gFnSc4	role
v	v	k7c6	v
popisu	popis	k1gInSc6	popis
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
na	na	k7c6	na
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
i	i	k8xC	i
v	v	k7c6	v
obecně	obecně	k6eAd1	obecně
křivém	křivý	k2eAgInSc6d1	křivý
prostoročasu	prostoročas	k1gInSc6	prostoročas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
využívá	využívat	k5eAaPmIp3nS	využívat
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Minkowského	Minkowského	k2eAgInSc1d1	Minkowského
prostor	prostor	k1gInSc1	prostor
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
tečného	tečný	k2eAgInSc2d1	tečný
(	(	kIx(	(
<g/>
vektorového	vektorový	k2eAgInSc2d1	vektorový
<g/>
)	)	kIx)	)
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
nezávislé	závislý	k2eNgInPc1d1	nezávislý
na	na	k7c6	na
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
formulovat	formulovat	k5eAaImF	formulovat
pomocí	pomocí	k7c2	pomocí
rovnic	rovnice	k1gFnPc2	rovnice
mezi	mezi	k7c7	mezi
čtyřvektory	čtyřvektor	k1gInPc7	čtyřvektor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
čtyřtenzory	čtyřtenzora	k1gFnPc1	čtyřtenzora
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
požadavku	požadavek	k1gInSc3	požadavek
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
princip	princip	k1gInSc1	princip
obecné	obecný	k2eAgFnSc2d1	obecná
kovariance	kovariance	k1gFnSc2	kovariance
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřvektor	Čtyřvektor	k1gInSc4	Čtyřvektor
značíme	značit	k5eAaImIp1nP	značit
zápisem	zápis	k1gInSc7	zápis
jeho	jeho	k3xOp3gFnPc2	jeho
souřadnic	souřadnice	k1gFnPc2	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
resp.	resp.	kA	resp.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-a	-a	k?	-a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}}	}}	k?	}}
(	(	kIx(	(
<g/>
v	v	k7c6	v
inerciální	inerciální	k2eAgFnSc6d1	inerciální
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
ortonormální	ortonormální	k2eAgFnPc4d1	ortonormální
prostorové	prostorový	k2eAgFnPc4d1	prostorová
baze	baz	k1gFnPc4	baz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zápisu	zápis	k1gInSc6	zápis
čtyřvektorů	čtyřvektor	k1gInPc2	čtyřvektor
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
nutné	nutný	k2eAgNnSc1d1	nutné
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c7	mezi
kovariantními	kovariantní	k2eAgFnPc7d1	kovariantní
a	a	k8xC	a
kontravariantními	kontravariantní	k2eAgFnPc7d1	kontravariantní
složkami	složka	k1gFnPc7	složka
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
značíme	značit	k5eAaImIp1nP	značit
řeckým	řecký	k2eAgInSc7d1	řecký
indexem	index	k1gInSc7	index
dole	dole	k6eAd1	dole
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
nahoře	nahoře	k6eAd1	nahoře
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
bývá	bývat	k5eAaImIp3nS	bývat
zvykem	zvyk	k1gInSc7	zvyk
užívat	užívat	k5eAaImF	užívat
tzv.	tzv.	kA	tzv.
Einsteinovu	Einsteinův	k2eAgFnSc4d1	Einsteinova
sumační	sumační	k2eAgFnSc4d1	sumační
konvenci	konvence	k1gFnSc4	konvence
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
že	že	k8xS	že
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
výrazu	výraz	k1gInSc2	výraz
dva	dva	k4xCgInPc1	dva
stejné	stejný	k2eAgInPc1d1	stejný
indexy	index	k1gInPc1	index
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
pak	pak	k8xC	pak
se	se	k3xPyFc4	se
sčítá	sčítat	k5eAaImIp3nS	sčítat
přes	přes	k7c4	přes
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
nabývat	nabývat	k5eAaImF	nabývat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
metrice	metrika	k1gFnSc3	metrika
Minkowského	Minkowský	k2eAgInSc2d1	Minkowský
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
u	u	k7c2	u
časových	časový	k2eAgFnPc2d1	časová
složek	složka	k1gFnPc2	složka
čtyřvektorů	čtyřvektor	k1gMnPc2	čtyřvektor
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
polohy	poloha	k1gFnPc1	poloha
indexů	index	k1gInPc2	index
změní	změnit	k5eAaPmIp3nP	změnit
znaménko	znaménko	k1gNnSc4	znaménko
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
příklad	příklad	k1gInSc1	příklad
výše	výše	k1gFnSc1	výše
<g/>
)	)	kIx)	)
Zvedání	zvedání	k1gNnSc1	zvedání
a	a	k8xC	a
snižování	snižování	k1gNnSc1	snižování
dolních	dolní	k2eAgInPc2d1	dolní
a	a	k8xC	a
horních	horní	k2eAgInPc2d1	horní
indexů	index	k1gInPc2	index
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
podle	podle	k7c2	podle
pravidla	pravidlo	k1gNnSc2	pravidlo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
Minkowského	Minkowského	k2eAgFnSc1d1	Minkowského
metrika	metrika	k1gFnSc1	metrika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
<g />
.	.	kIx.	.
</s>
<s hack="1">
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
"	"	kIx"	"
<g/>
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
a	a	k8xC	a
ortonormálních	ortonormální	k2eAgFnPc6d1	ortonormální
<g/>
"	"	kIx"	"
souřadnicích	souřadnice	k1gFnPc6	souřadnice
vyjádření	vyjádření	k1gNnPc2	vyjádření
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
d	d	k?	d
i	i	k8xC	i
a	a	k8xC	a
g	g	kA	g
:	:	kIx,	:
(	(	kIx(	(
−	−	k?	−
1	[number]	k4	1
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
diag	diag	k1gInSc1	diag
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1,1	[number]	k4	1,1
<g/>
,1	,1	k4	,1
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
článku	článek	k1gInSc6	článek
rovněž	rovněž	k9	rovněž
užíváme	užívat	k5eAaImIp1nP	užívat
běžné	běžný	k2eAgInPc1d1	běžný
prostorové	prostorový	k2eAgInPc1d1	prostorový
vektory	vektor	k1gInPc1	vektor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
značíme	značit	k5eAaImIp1nP	značit
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
ve	v	k7c6	v
složkách	složka	k1gFnPc6	složka
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
(	(	kIx(	(
<g/>
troj	trojit	k5eAaImRp2nS	trojit
<g/>
)	)	kIx)	)
<g/>
vektory	vektor	k1gInPc7	vektor
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
Lorentzově	Lorentzův	k2eAgFnSc3d1	Lorentzova
transformaci	transformace	k1gFnSc3	transformace
netransformují	transformovat	k5eNaBmIp3nP	transformovat
jako	jako	k9	jako
vektory	vektor	k1gInPc1	vektor
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
polohu	poloha	k1gFnSc4	poloha
jejich	jejich	k3xOp3gInSc2	jejich
indexu	index	k1gInSc2	index
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
a	a	k8xC	a
píšeme	psát	k5eAaImIp1nP	psát
jej	on	k3xPp3gInSc4	on
vždy	vždy	k6eAd1	vždy
dole	dole	k6eAd1	dole
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
c	c	k0	c
=	=	kIx~	=
299	[number]	k4	299
792	[number]	k4	792
458	[number]	k4	458
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
odlišné	odlišný	k2eAgNnSc4d1	odlišné
měřítko	měřítko	k1gNnSc4	měřítko
na	na	k7c6	na
časové	časový	k2eAgFnSc6d1	časová
ose	osa	k1gFnSc6	osa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
přirozené	přirozený	k2eAgNnSc1d1	přirozené
měřit	měřit	k5eAaImF	měřit
čas	čas	k1gInSc4	čas
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
jednotkách	jednotka	k1gFnPc6	jednotka
jako	jako	k8xS	jako
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
volí	volit	k5eAaImIp3nP	volit
takové	takový	k3xDgFnPc4	takový
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
,	,	kIx,	,
například	například	k6eAd1	například
můžeme	moct	k5eAaImIp1nP	moct
měřit	měřit	k5eAaImF	měřit
délku	délka	k1gFnSc4	délka
ve	v	k7c6	v
světelných	světelný	k2eAgFnPc6d1	světelná
sekundách	sekunda	k1gFnPc6	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
konstanta	konstanta	k1gFnSc1	konstanta
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
výpočty	výpočet	k1gInPc4	výpočet
se	se	k3xPyFc4	se
zjednoduší	zjednodušet	k5eAaPmIp3nS	zjednodušet
a	a	k8xC	a
na	na	k7c6	na
fyzikálních	fyzikální	k2eAgInPc6d1	fyzikální
závěrech	závěr	k1gInPc6	závěr
se	se	k3xPyFc4	se
nic	nic	k6eAd1	nic
nezmění	změnit	k5eNaPmIp3nS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Vizte	vidět	k5eAaImRp2nP	vidět
též	též	k9	též
článek	článek	k1gInSc4	článek
Přirozená	přirozený	k2eAgFnSc1d1	přirozená
soustava	soustava	k1gFnSc1	soustava
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Popíšeme	popsat	k5eAaPmIp1nP	popsat
<g/>
-li	i	k?	-li
stejnou	stejný	k2eAgFnSc4d1	stejná
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
inerciální	inerciální	k2eAgFnSc6d1	inerciální
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
původní	původní	k2eAgFnSc3d1	původní
soustavě	soustava	k1gFnSc3	soustava
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g />
.	.	kIx.	.
</s>
<s hack="1">
rychlostí	rychlost	k1gFnPc2	rychlost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
}	}	kIx)	}
podél	podél	k7c2	podél
osy	osa	k1gFnSc2	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
projdou	projít	k5eAaPmIp3nP	projít
místa	místo	k1gNnPc4	místo
a	a	k8xC	a
časy	čas	k1gInPc4	čas
událostí	událost	k1gFnPc2	událost
Lorentzovou	Lorentzový	k2eAgFnSc7d1	Lorentzový
transformací	transformace	k1gFnSc7	transformace
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
γ	γ	k?	γ
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
t	t	k?	t
−	−	k?	−
v	v	k7c6	v
x	x	k?	x
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
(	(	kIx(	(
<g/>
t-vx	tx	k1gInSc1	t-vx
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
γ	γ	k?	γ
(	(	kIx(	(
x	x	k?	x
−	−	k?	−
v	v	k7c6	v
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
(	(	kIx(	(
<g/>
x-vt	xt	k1gInSc1	x-vt
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
y	y	k?	y
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
:	:	kIx,	:
Vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
časová	časový	k2eAgFnSc1d1	časová
složka	složka	k1gFnSc1	složka
a	a	k8xC	a
prostorová	prostorový	k2eAgFnSc1d1	prostorová
složka	složka	k1gFnSc1	složka
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
promíchají	promíchat	k5eAaPmIp3nP	promíchat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
zavádí	zavádět	k5eAaImIp3nS	zavádět
časoprostor	časoprostor	k1gInSc4	časoprostor
a	a	k8xC	a
čtyřvektory	čtyřvektor	k1gInPc4	čtyřvektor
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
událostí	událost	k1gFnPc2	událost
změní	změnit	k5eAaPmIp3nS	změnit
i	i	k9	i
souřadnice	souřadnice	k1gFnPc4	souřadnice
všech	všecek	k3xTgInPc2	všecek
ostatních	ostatní	k2eAgInPc2d1	ostatní
čtyřvektorů	čtyřvektor	k1gInPc2	čtyřvektor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
elektrické	elektrický	k2eAgNnSc1d1	elektrické
pole	pole	k1gNnSc1	pole
se	se	k3xPyFc4	se
promíchá	promíchat	k5eAaPmIp3nS	promíchat
s	s	k7c7	s
magnetickým	magnetický	k2eAgMnSc7d1	magnetický
<g/>
.	.	kIx.	.
</s>
<s>
Matematicky	matematicky	k6eAd1	matematicky
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
lineární	lineární	k2eAgFnSc4d1	lineární
transformaci	transformace	k1gFnSc4	transformace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
násobení	násobení	k1gNnSc4	násobení
čtyřvektoru	čtyřvektor	k1gInSc2	čtyřvektor
Lorentzovou	Lorentzová	k1gFnSc7	Lorentzová
maticí	matice	k1gFnSc7	matice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Λ	Λ	k?	Λ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc3	lambda
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
Λ	Λ	k?	Λ
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
:	:	kIx,	:
Rozepsáním	rozepsání	k1gNnSc7	rozepsání
do	do	k7c2	do
složek	složka	k1gFnPc2	složka
dostaneme	dostat	k5eAaPmIp1nP	dostat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
γ	γ	k?	γ
β	β	k?	β
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
γ	γ	k?	γ
β	β	k?	β
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
'	'	kIx"	'
<g/>
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
'	'	kIx"	'
<g/>
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
'	'	kIx"	'
<g/>
\\\	\\\	k?	\\\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k2eAgInSc4d1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc4	pmatrix
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
&	&	k?	&
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gNnSc1	gamma
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
<g />
.	.	kIx.	.
</s>
<s>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gNnSc1	gamma
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s>
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
\\\	\\\	k?	\\\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\\\	\\\	k?	\\\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,,	,,	k?	,,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
1	[number]	k4	1
−	−	k?	−
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
gamma	gammum	k1gNnPc1	gammum
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
je	být	k5eAaImIp3nS	být
Lorentzův	Lorentzův	k2eAgInSc4d1	Lorentzův
faktor	faktor	k1gInSc4	faktor
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
=	=	kIx~	=
v	v	k7c6	v
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
=	=	kIx~	=
<g/>
v	v	k7c6	v
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
bezrozměrná	bezrozměrný	k2eAgFnSc1d1	bezrozměrná
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Matice	matice	k1gFnSc1	matice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Λ	Λ	k?	Λ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnPc6	lambda
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgMnSc1d1	unitární
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
determinant	determinant	k1gInSc1	determinant
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lorentzovu	Lorentzův	k2eAgFnSc4d1	Lorentzova
transformaci	transformace	k1gFnSc4	transformace
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
představit	představit	k5eAaPmF	představit
jako	jako	k8xS	jako
rotaci	rotace	k1gFnSc4	rotace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
čtyřvektorů	čtyřvektor	k1gInPc2	čtyřvektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Λ	Λ	k?	Λ
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Λ	Λ	k?	Λ
:	:	kIx,	:
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
ρ	ρ	k?	ρ
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc2	kappa
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Λ	Λ	k?	Λ
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
Λ	Λ	k?	Λ
=	=	kIx~	=
η	η	k?	η
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc3	lambda
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc1	lambda
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}	}	kIx)	}
.	.	kIx.	.
"	"	kIx"	"
<g/>
Úhel	úhel	k1gInSc1	úhel
<g/>
"	"	kIx"	"
této	tento	k3xDgFnSc2	tento
rotace	rotace	k1gFnSc2	rotace
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rapidita	rapidita	k1gFnSc1	rapidita
<g/>
.	.	kIx.	.
</s>
<s>
Matematicky	matematicky	k6eAd1	matematicky
se	se	k3xPyFc4	se
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
takových	takový	k3xDgFnPc2	takový
rotací	rotace	k1gFnPc2	rotace
značí	značit	k5eAaImIp3nS	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
O	O	kA	O
(	(	kIx(	(
3	[number]	k4	3
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
SO	So	kA	So
<g/>
(	(	kIx(	(
<g/>
3,1	[number]	k4	3,1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
–	–	k?	–
Lorentzova	Lorentzův	k2eAgFnSc1d1	Lorentzova
grupa	grupa	k1gFnSc1	grupa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
definici	definice	k1gFnSc6	definice
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
čtyřvektory	čtyřvektor	k1gInPc7	čtyřvektor
a	a	k8xC	a
obyčejnými	obyčejný	k2eAgInPc7d1	obyčejný
čtyřsložkovými	čtyřsložkový	k2eAgInPc7d1	čtyřsložkový
vektory	vektor	k1gInPc7	vektor
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
časové	časový	k2eAgFnSc2d1	časová
složky	složka	k1gFnSc2	složka
je	být	k5eAaImIp3nS	být
opačné	opačný	k2eAgNnSc4d1	opačné
znaménko	znaménko	k1gNnSc4	znaménko
než	než	k8xS	než
u	u	k7c2	u
prostorových	prostorový	k2eAgFnPc2d1	prostorová
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
čtyřvektory	čtyřvektor	k1gInPc4	čtyřvektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
se	se	k3xPyFc4	se
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
zavádí	zavádět	k5eAaImIp3nS	zavádět
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
==	==	k?	==
−	−	k?	−
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
==	==	k?	==
<g/>
-a_	_	k?	-a_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
opačná	opačný	k2eAgFnSc1d1	opačná
konvence	konvence	k1gFnSc1	konvence
<g/>
,	,	kIx,	,
že	že	k8xS	že
znaménka	znaménko	k1gNnSc2	znaménko
minus	minus	k1gNnSc2	minus
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
k	k	k7c3	k
prostorovým	prostorový	k2eAgFnPc3d1	prostorová
složkám	složka	k1gFnPc3	složka
a	a	k8xC	a
plus	plus	k6eAd1	plus
k	k	k7c3	k
časovým	časový	k2eAgFnPc3d1	časová
<g/>
,	,	kIx,	,
vizte	vidět	k5eAaImRp2nP	vidět
například	například	k6eAd1	například
Feynmanovy	Feynmanův	k2eAgFnPc4d1	Feynmanova
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
správně	správně	k6eAd1	správně
a	a	k8xC	a
fyzikálně	fyzikálně	k6eAd1	fyzikálně
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
nezmění	změnit	k5eNaPmIp3nS	změnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přináší	přinášet	k5eAaImIp3nS	přinášet
to	ten	k3xDgNnSc1	ten
určité	určitý	k2eAgInPc4d1	určitý
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
níže	nízce	k6eAd2	nízce
uvedené	uvedený	k2eAgInPc1d1	uvedený
čtyřvektorové	čtyřvektorový	k2eAgInPc1d1	čtyřvektorový
operátory	operátor	k1gInPc1	operátor
by	by	kYmCp3nP	by
nebyly	být	k5eNaImAgFnP	být
prostým	prostý	k2eAgNnSc7d1	prosté
rozšířením	rozšíření	k1gNnSc7	rozšíření
vektorových	vektorový	k2eAgInPc2d1	vektorový
operátorů	operátor	k1gInPc2	operátor
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
dimenzi	dimenze	k1gFnSc4	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
konvencích	konvence	k1gFnPc6	konvence
je	být	k5eAaImIp3nS	být
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
invariantní	invariantní	k2eAgInPc1d1	invariantní
vůči	vůči	k7c3	vůči
Lorentzově	Lorentzův	k2eAgFnSc3d1	Lorentzova
transformaci	transformace	k1gFnSc3	transformace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gFnSc1	jeho
hodnota	hodnota	k1gFnSc1	hodnota
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
volbě	volba	k1gFnSc6	volba
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
<g/>
'	'	kIx"	'
<g/>
{	{	kIx(	{
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
:	:	kIx,	:
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
čtyřvektoru	čtyřvektor	k1gInSc2	čtyřvektor
se	se	k3xPyFc4	se
sebou	se	k3xPyFc7	se
samým	samý	k3xTgMnSc7	samý
dává	dávat	k5eAaImIp3nS	dávat
druhou	druhý	k4xOgFnSc4	druhý
mocninu	mocnina	k1gFnSc4	mocnina
jeho	jeho	k3xOp3gFnSc2	jeho
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
volbě	volba	k1gFnSc6	volba
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-a_	_	k?	-a_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
generuje	generovat	k5eAaImIp3nS	generovat
Minkowského	Minkowského	k2eAgFnSc4d1	Minkowského
metriku	metrika	k1gFnSc4	metrika
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
způsob	způsob	k1gInSc1	způsob
měření	měření	k1gNnSc2	měření
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
v	v	k7c6	v
Minkowského	Minkowského	k2eAgInSc6d1	Minkowského
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
znaménka	znaménko	k1gNnSc2	znaménko
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
čtyřvektoru	čtyřvektor	k1gInSc2	čtyřvektor
se	se	k3xPyFc4	se
sebou	se	k3xPyFc7	se
samým	samý	k3xTgMnSc7	samý
dělíme	dělit	k5eAaImIp1nP	dělit
čtyřvektory	čtyřvektor	k1gInPc4	čtyřvektor
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
záporný	záporný	k2eAgInSc1d1	záporný
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
vektor	vektor	k1gInSc1	vektor
časupodobný	časupodobný	k2eAgInSc1d1	časupodobný
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
nulový	nulový	k2eAgInSc1d1	nulový
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
čtyřvektor	čtyřvektor	k1gInSc1	čtyřvektor
světlupodobný	světlupodobný	k2eAgInSc1d1	světlupodobný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
kladný	kladný	k2eAgInSc1d1	kladný
<g/>
,	,	kIx,	,
čtyřvektor	čtyřvektor	k1gInSc1	čtyřvektor
nazýváme	nazývat	k5eAaImIp1nP	nazývat
prostorupodobný	prostorupodobný	k2eAgMnSc1d1	prostorupodobný
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc4	bod
v	v	k7c6	v
prostoročase	prostoročas	k1gInSc6	prostoročas
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
tedy	tedy	k9	tedy
místo	místo	k1gNnSc4	místo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
,	,	kIx,	,
z	z	k7c2	z
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
časový	časový	k2eAgInSc4d1	časový
okamžik	okamžik	k1gInSc4	okamžik
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
c	c	k0	c
t	t	k?	t
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
c	c	k0	c
t	t	k?	t
,	,	kIx,	,
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
,	,	kIx,	,
z	z	k7c2	z
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
ct	ct	k?	ct
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
ct	ct	k?	ct
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Množina	množina	k1gFnSc1	množina
událostí	událost	k1gFnPc2	událost
vztahujících	vztahující	k2eAgFnPc2d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
objektu	objekt	k1gInSc3	objekt
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
světočára	světočára	k1gFnSc1	světočára
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
derivaci	derivace	k1gFnSc4	derivace
čtyřvektoru	čtyřvektor	k1gInSc2	čtyřvektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgInSc2d1	vlastní
času	čas	k1gInSc2	čas
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
volbě	volba	k1gFnSc6	volba
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
analogií	analogie	k1gFnSc7	analogie
vektoru	vektor	k1gInSc2	vektor
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
na	na	k7c6	na
časové	časový	k2eAgFnSc6d1	časová
ose	osa	k1gFnSc6	osa
má	mít	k5eAaImIp3nS	mít
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
transformovala	transformovat	k5eAaBmAgFnS	transformovat
jako	jako	k9	jako
čtyřvektor	čtyřvektor	k1gInSc4	čtyřvektor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ji	on	k3xPp3gFnSc4	on
vynásobit	vynásobit	k5eAaPmF	vynásobit
Lorentzovým	Lorentzův	k2eAgInSc7d1	Lorentzův
faktorem	faktor	k1gInSc7	faktor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
−	−	k?	−
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
gamma	gammum	k1gNnPc1	gammum
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-v	-v	k?	-v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
přepočet	přepočet	k1gInSc1	přepočet
mezi	mezi	k7c7	mezi
vlastním	vlastní	k2eAgInSc7d1	vlastní
časem	čas	k1gInSc7	čas
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
časem	čas	k1gInSc7	čas
ve	v	k7c6	v
zvolené	zvolený	k2eAgFnSc6d1	zvolená
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dilatace	dilatace	k1gFnSc1	dilatace
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
význam	význam	k1gInSc1	význam
časové	časový	k2eAgFnSc2d1	časová
složky	složka	k1gFnSc2	složka
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
rychlost	rychlost	k1gFnSc1	rychlost
cestování	cestování	k1gNnSc2	cestování
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kolik	kolik	k4yIc4	kolik
sekund	sekunda	k1gFnPc2	sekunda
uplyne	uplynout	k5eAaPmIp3nS	uplynout
pozorovateli	pozorovatel	k1gMnSc6	pozorovatel
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
hodinách	hodina	k1gFnPc6	hodina
rakety	raketa	k1gFnSc2	raketa
uplyne	uplynout	k5eAaPmIp3nS	uplynout
jedna	jeden	k4xCgFnSc1	jeden
sekunda	sekunda	k1gFnSc1	sekunda
(	(	kIx(	(
<g/>
čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
měřen	měřen	k2eAgMnSc1d1	měřen
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytujeli-se	Vyskytujelie	k1gFnSc1	Vyskytujeli-se
např.	např.	kA	např.
u	u	k7c2	u
časové	časový	k2eAgFnSc2d1	časová
složky	složka	k1gFnSc2	složka
čtyřrychlosti	čtyřrychlost	k1gFnSc2	čtyřrychlost
3	[number]	k4	3
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
stárne	stárnout	k5eAaImIp3nS	stárnout
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
třikrát	třikrát	k6eAd1	třikrát
pomaleji	pomale	k6eAd2	pomale
<g/>
.	.	kIx.	.
</s>
<s>
Prostorová	prostorový	k2eAgFnSc1d1	prostorová
složka	složka	k1gFnSc1	složka
udává	udávat	k5eAaImIp3nS	udávat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
loď	loď	k1gFnSc4	loď
uletí	uletět	k5eAaPmIp3nS	uletět
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
hodninách	hodnina	k1gFnPc6	hodnina
v	v	k7c4	v
kosmické	kosmický	k2eAgMnPc4d1	kosmický
lidi	člověk	k1gMnPc4	člověk
uplyne	uplynout	k5eAaPmIp3nS	uplynout
jedna	jeden	k4xCgFnSc1	jeden
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
libovolně	libovolně	k6eAd1	libovolně
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
za	za	k7c4	za
lidský	lidský	k2eAgInSc4d1	lidský
život	život	k1gInSc4	život
doletět	doletět	k5eAaPmF	doletět
i	i	k9	i
k	k	k7c3	k
hvězdě	hvězda	k1gFnSc3	hvězda
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
např.	např.	kA	např.
300	[number]	k4	300
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
τ	τ	k?	τ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
γ	γ	k?	γ
<g />
.	.	kIx.	.
</s>
<s hack="1">
c	c	k0	c
,	,	kIx,	,
γ	γ	k?	γ
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
γ	γ	k?	γ
c	c	k0	c
,	,	kIx,	,
γ	γ	k?	γ
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
γ	γ	k?	γ
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
γ	γ	k?	γ
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
tau	tau	k1gNnSc1	tau
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Čtyřhybnost	Čtyřhybnost	k1gFnSc1	Čtyřhybnost
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
čtyřimpuls	čtyřimpuls	k6eAd1	čtyřimpuls
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čtyřvektor	čtyřvektor	k1gMnSc1	čtyřvektor
spojující	spojující	k2eAgFnSc4d1	spojující
energii	energie	k1gFnSc4	energie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
}	}	kIx)	}
a	a	k8xC	a
hybnost	hybnost	k1gFnSc1	hybnost
(	(	kIx(	(
<g/>
impuls	impuls	k1gInSc1	impuls
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
E	E	kA	E
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
E	E	kA	E
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
zapsat	zapsat	k5eAaPmF	zapsat
také	také	k9	také
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
u	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
volbě	volba	k1gFnSc6	volba
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
)	)	kIx)	)
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
čtyřrychlost	čtyřrychlost	k1gFnSc1	čtyřrychlost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velikost	velikost	k1gFnSc4	velikost
čtyřhybnosti	čtyřhybnost	k1gFnSc2	čtyřhybnost
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
=	=	kIx~	=
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
P	P	kA	P
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
P	P	kA	P
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
P_	P_	k1gFnSc6	P_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-m_	_	k?	-m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Čtyřpotenciál	Čtyřpotenciál	k1gInSc1	Čtyřpotenciál
spojuje	spojovat	k5eAaImIp3nS	spojovat
vektorový	vektorový	k2eAgInSc1d1	vektorový
potenciál	potenciál	k1gInSc1	potenciál
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
}	}	kIx)	}
a	a	k8xC	a
skalární	skalární	k2eAgInSc1d1	skalární
potenciál	potenciál	k1gInSc1	potenciál
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnPc3	varphi
}	}	kIx)	}
související	související	k2eAgInPc1d1	související
s	s	k7c7	s
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
polem	pole	k1gNnSc7	pole
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
φ	φ	k?	φ
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
φ	φ	k?	φ
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
,	,	kIx,	,
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
A_	A_	k1gFnPc1	A_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
A_	A_	k1gMnSc1	A_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
A_	A_	k1gFnSc1	A_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Vektorová	vektorový	k2eAgNnPc4d1	vektorové
pole	pole	k1gNnPc4	pole
magnetické	magnetický	k2eAgFnSc2d1	magnetická
indukce	indukce	k1gFnSc2	indukce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
a	a	k8xC	a
intenzity	intenzita	k1gFnSc2	intenzita
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}	}	kIx)	}
}	}	kIx)	}
lze	lze	k6eAd1	lze
spočítat	spočítat	k5eAaPmF	spočítat
z	z	k7c2	z
potenciálů	potenciál	k1gInPc2	potenciál
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
=	=	kIx~	=
∇	∇	k?	∇
×	×	k?	×
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
∇	∇	k?	∇
φ	φ	k?	φ
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k1gMnSc1	nabla
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Čtyřproud	Čtyřproud	k1gInSc1	Čtyřproud
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
proudění	proudění	k1gNnSc1	proudění
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
pomocí	pomocí	k7c2	pomocí
objemové	objemový	k2eAgFnSc2d1	objemová
hustoty	hustota	k1gFnSc2	hustota
elektrických	elektrický	k2eAgInPc2d1	elektrický
proudů	proud	k1gInPc2	proud
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
}	}	kIx)	}
a	a	k8xC	a
hustoty	hustota	k1gFnSc2	hustota
elektrických	elektrický	k2eAgInPc2d1	elektrický
nábojů	náboj	k1gInPc2	náboj
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
J	J	kA	J
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
c	c	k0	c
ρ	ρ	k?	ρ
,	,	kIx,	,
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
c	c	k0	c
ρ	ρ	k?	ρ
,	,	kIx,	,
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
J	J	kA	J
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
,	,	kIx,	,
<g/>
j_	j_	k?	j_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
j_	j_	k?	j_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
j_	j_	k?	j_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Zcela	zcela	k6eAd1	zcela
analogicky	analogicky	k6eAd1	analogicky
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
i	i	k9	i
proudění	proudění	k1gNnSc1	proudění
jiných	jiný	k2eAgFnPc2d1	jiná
veličin	veličina	k1gFnPc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
změny	změna	k1gFnPc4	změna
fáze	fáze	k1gFnSc2	fáze
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Úhlová	úhlový	k2eAgFnSc1d1	úhlová
frekvence	frekvence	k1gFnSc1	frekvence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnPc6	omega
}	}	kIx)	}
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
změnu	změna	k1gFnSc4	změna
fáze	fáze	k1gFnSc2	fáze
s	s	k7c7	s
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
vlnový	vlnový	k2eAgInSc4d1	vlnový
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
}	}	kIx)	}
popisuje	popisovat	k5eAaImIp3nS	popisovat
změny	změna	k1gFnPc4	změna
fáze	fáze	k1gFnSc2	fáze
s	s	k7c7	s
prostorovými	prostorový	k2eAgFnPc7d1	prostorová
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
ω	ω	k?	ω
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
,	,	kIx,	,
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
ω	ω	k?	ω
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
c	c	k0	c
,	,	kIx,	,
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
y	y	k?	y
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Vizte	vidět	k5eAaImRp2nP	vidět
též	též	k9	též
články	článek	k1gInPc4	článek
Fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
Grupová	Grupový	k2eAgFnSc1d1	Grupový
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
Vlnová	vlnový	k2eAgFnSc1d1	vlnová
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Kvadrát	kvadrát	k1gInSc1	kvadrát
čtyřhybnosti	čtyřhybnost	k1gFnSc2	čtyřhybnost
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
faktor	faktor	k1gInSc4	faktor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
klidové	klidový	k2eAgFnSc3d1	klidová
hmotnosti	hmotnost	k1gFnSc3	hmotnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
||	||	k?	||
<g/>
P	P	kA	P
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
||	||	k?	||
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-P	-P	k?	-P
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
P_	P_	k1gFnSc6	P_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
E	E	kA	E
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-p	-p	k?	-p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
=	=	kIx~	=
±	±	k?	±
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
pm	pm	k?	pm
c	c	k0	c
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Kvadrát	kvadrát	k1gInSc1	kvadrát
čtyřrychlosti	čtyřrychlost	k1gFnSc2	čtyřrychlost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
objekty	objekt	k1gInPc4	objekt
konstantní	konstantní	k2eAgMnSc1d1	konstantní
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
u	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
|	|	kIx~	|
<g/>
u	u	k7c2	u
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
objekty	objekt	k1gInPc1	objekt
se	se	k3xPyFc4	se
časoprostorem	časoprostor	k1gInSc7	časoprostor
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
stejnou	stejný	k2eAgFnSc7d1	stejná
rychlostí	rychlost	k1gFnSc7	rychlost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tělesa	těleso	k1gNnPc1	těleso
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
touto	tento	k3xDgFnSc7	tento
rychlostí	rychlost	k1gFnSc7	rychlost
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
časové	časový	k2eAgFnSc6d1	časová
ose	osa	k1gFnSc6	osa
<g/>
,	,	kIx,	,
necestují	cestovat	k5eNaImIp3nP	cestovat
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
letícímu	letící	k2eAgNnSc3d1	letící
tělesu	těleso	k1gNnSc3	těleso
ubíhá	ubíhat	k5eAaImIp3nS	ubíhat
vlastní	vlastní	k2eAgInSc4d1	vlastní
čas	čas	k1gInSc4	čas
pomaleji	pomale	k6eAd2	pomale
<g/>
.	.	kIx.	.
</s>
<s>
Fotonům	foton	k1gInPc3	foton
při	při	k7c6	při
rychlosti	rychlost	k1gFnSc6	rychlost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
neběží	běžet	k5eNaImIp3nP	běžet
vlastní	vlastní	k2eAgInSc4d1	vlastní
čas	čas	k1gInSc4	čas
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
interval	interval	k1gInSc4	interval
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
časoprostorová	časoprostorový	k2eAgFnSc1d1	časoprostorová
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
"	"	kIx"	"
dvou	dva	k4xCgFnPc2	dva
událostí	událost	k1gFnPc2	událost
se	se	k3xPyFc4	se
spočte	spočíst	k5eAaPmIp3nS	spočíst
jako	jako	k9	jako
velikost	velikost	k1gFnSc1	velikost
rozdílu	rozdíl	k1gInSc2	rozdíl
čtyřvektorů	čtyřvektor	k1gMnPc2	čtyřvektor
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
y	y	k?	y
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
-c	-c	k?	-c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
-t_	_	k?	-t_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
-y	-y	k?	-y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
ω	ω	k?	ω
t	t	k?	t
+	+	kIx~	+
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
K	k	k7c3	k
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
t	t	k?	t
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
fáze	fáze	k1gFnSc1	fáze
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
J	J	kA	J
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
ρ	ρ	k?	ρ
φ	φ	k?	φ
−	−	k?	−
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
J	J	kA	J
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
A_	A_	k1gFnSc1	A_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
varphi	varph	k1gFnSc2	varph
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
interakční	interakční	k2eAgFnSc2d1	interakční
energie	energie	k1gFnSc2	energie
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
v	v	k7c6	v
elektromagnetickém	elektromagnetický	k2eAgNnSc6d1	elektromagnetické
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vektorový	vektorový	k2eAgInSc1d1	vektorový
diferenciální	diferenciální	k2eAgInSc1d1	diferenciální
operátor	operátor	k1gInSc1	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nabla	nablo	k1gNnPc4	nablo
}	}	kIx)	}
(	(	kIx(	(
<g/>
nabla	nabla	k6eAd1	nabla
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
přímou	přímý	k2eAgFnSc4d1	přímá
analogii	analogie	k1gFnSc4	analogie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
všechny	všechen	k3xTgFnPc4	všechen
vlastnosti	vlastnost	k1gFnPc4	vlastnost
čtyřvektorů	čtyřvektor	k1gInPc2	čtyřvektor
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
∂	∂	k?	∂
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
∂	∂	k?	∂
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
∂	∂	k?	∂
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
∂	∂	k?	∂
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
∂	∂	k?	∂
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nabla	nablo	k1gNnPc4	nablo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partiat	k5eAaImAgMnS	partiat
<g />
.	.	kIx.	.
</s>
<s>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
nabla	nablo	k1gNnPc4	nablo
}	}	kIx)	}
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
:	:	kIx,	:
Čtyřvektorové	čtyřvektorový	k2eAgFnPc1d1	čtyřvektorový
operace	operace	k1gFnPc1	operace
analogické	analogický	k2eAgFnPc1d1	analogická
ke	k	k7c3	k
gradientu	gradient	k1gInSc3	gradient
a	a	k8xC	a
divergenci	divergence	k1gFnSc3	divergence
lze	lze	k6eAd1	lze
pomocí	pomocí	k7c2	pomocí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nabla	nablo	k1gNnPc4	nablo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
g	g	kA	g
<g />
.	.	kIx.	.
</s>
<s hack="1">
r	r	kA	r
a	a	k8xC	a
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
∂	∂	k?	∂
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
∇	∇	k?	∇
φ	φ	k?	φ
)	)	kIx)	)
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
grad	grad	k1gInSc1	grad
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnSc1	varphi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnSc1	varphi
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
nabla	nabnout	k5eAaPmAgFnS	nabnout
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnSc1	varphi
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnSc1	varphi
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnSc1	varphi
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnSc1	varphi
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,,	,,	k?	,,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
i	i	k9	i
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
∂	∂	k?	∂
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∇	∇	k?	∇
⋅	⋅	k?	⋅
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
div	div	k1gInSc4	div
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
nabla	nabla	k1gMnSc1	nabla
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
partial	partiat	k5eAaImAgMnS	partiat
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partiat	k5eAaImAgMnS	partiat
<g />
.	.	kIx.	.
</s>
<s hack="1">
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pomocí	pomocí	k7c2	pomocí
čtyřdivergence	čtyřdivergence	k1gFnSc2	čtyřdivergence
a	a	k8xC	a
čtyřproudu	čtyřproudu	k6eAd1	čtyřproudu
lze	lze	k6eAd1	lze
stručně	stručně	k6eAd1	stručně
zapsat	zapsat	k5eAaPmF	zapsat
například	například	k6eAd1	například
zákon	zákon	k1gInSc4	zákon
zachování	zachování	k1gNnSc2	zachování
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
(	(	kIx(	(
<g/>
rovnici	rovnice	k1gFnSc4	rovnice
kontinuity	kontinuita	k1gFnSc2	kontinuita
pro	pro	k7c4	pro
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
J	J	kA	J
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nabla	nablo	k1gNnPc4	nablo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
J	J	kA	J
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Jako	jako	k8xS	jako
analogie	analogie	k1gFnSc1	analogie
Laplaceova	Laplaceův	k2eAgInSc2d1	Laplaceův
operátoru	operátor	k1gInSc2	operátor
(	(	kIx(	(
<g/>
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
dvou	dva	k4xCgNnPc2	dva
nabla	nablo	k1gNnSc2	nablo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
také	také	k9	také
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Alembertův	Alembertův	k2eAgInSc4d1	Alembertův
operátor	operátor	k1gInSc4	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
◻	◻	k?	◻
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
square	square	k1gInSc1	square
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
◻	◻	k?	◻
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
square	square	k1gInSc1	square
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc4	partial
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Protože	protože	k8xS	protože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
skalární	skalární	k2eAgInSc4d1	skalární
součin	součin	k1gInSc4	součin
čtyřvektorových	čtyřvektorový	k2eAgInPc2d1	čtyřvektorový
operátorů	operátor	k1gInPc2	operátor
<g/>
,	,	kIx,	,
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
operátor	operátor	k1gInSc1	operátor
jako	jako	k8xC	jako
skalár	skalár	k1gInSc1	skalár
invariantní	invariantní	k2eAgInSc1d1	invariantní
vůči	vůči	k7c3	vůči
změně	změna	k1gFnSc3	změna
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
něj	on	k3xPp3gNnSc2	on
lze	lze	k6eAd1	lze
úsporně	úsporně	k6eAd1	úsporně
zapsat	zapsat	k5eAaPmF	zapsat
například	například	k6eAd1	například
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
rovnici	rovnice	k1gFnSc4	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
◻	◻	k?	◻
ψ	ψ	k?	ψ
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
square	square	k1gInSc1	square
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
čtyřpotenciálu	čtyřpotenciál	k1gInSc2	čtyřpotenciál
lze	lze	k6eAd1	lze
také	také	k9	také
Lorentzovu	Lorentzův	k2eAgFnSc4d1	Lorentzova
podmínku	podmínka	k1gFnSc4	podmínka
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
stručně	stručně	k6eAd1	stručně
<g />
.	.	kIx.	.
</s>
<s hack="1">
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
nabla	nablo	k1gNnPc4	nablo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
A	a	k9	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
Maxwellovy	Maxwellův	k2eAgFnPc1d1	Maxwellova
rovnice	rovnice	k1gFnPc1	rovnice
pro	pro	k7c4	pro
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
pole	pole	k1gNnSc4	pole
lze	lze	k6eAd1	lze
shrnout	shrnout	k5eAaPmF	shrnout
do	do	k7c2	do
jediné	jediný	k2eAgFnSc2d1	jediná
čtyřvektorové	čtyřvektorový	k2eAgFnSc2d1	čtyřvektorový
rovnice	rovnice	k1gFnSc2	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
◻	◻	k?	◻
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
J	J	kA	J
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
square	square	k1gInSc1	square
A	A	kA	A
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
J	J	kA	J
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
(	(	kIx(	(
<g/>
Konstanta	konstanta	k1gFnSc1	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
permeabilita	permeabilita	k1gFnSc1	permeabilita
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
rovnice	rovnice	k1gFnSc2	rovnice
jsou	být	k5eAaImIp3nP	být
čtyřvektory	čtyřvektor	k1gInPc4	čtyřvektor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rovnice	rovnice	k1gFnSc1	rovnice
se	se	k3xPyFc4	se
nezmění	změnit	k5eNaPmIp3nS	změnit
při	při	k7c6	při
Lorentzově	Lorentzův	k2eAgFnSc6d1	Lorentzova
transformaci	transformace	k1gFnSc6	transformace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Maxwellova	Maxwellův	k2eAgFnSc1d1	Maxwellova
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
evidentně	evidentně	k6eAd1	evidentně
správná	správný	k2eAgFnSc1d1	správná
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
přímo	přímo	k6eAd1	přímo
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejsou	být	k5eNaImIp3nP	být
volné	volný	k2eAgInPc1d1	volný
náboje	náboj	k1gInPc1	náboj
a	a	k8xC	a
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
nula	nula	k1gFnSc1	nula
a	a	k8xC	a
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
pole	pole	k1gNnSc1	pole
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
splňovat	splňovat	k5eAaImF	splňovat
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
šíření	šíření	k1gNnSc4	šíření
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vln	vlna	k1gFnPc2	vlna
(	(	kIx(	(
<g/>
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
-	-	kIx~	-
základní	základní	k2eAgInPc1d1	základní
vztahy	vztah	k1gInPc1	vztah
na	na	k7c4	na
Aldebaran	Aldebaran	k1gInSc4	Aldebaran
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Čtyřvektor	Čtyřvektor	k1gMnSc1	Čtyřvektor
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
MathWorld	MathWorlda	k1gFnPc2	MathWorlda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Feynmanovy	Feynmanův	k2eAgFnPc4d1	Feynmanova
přednášky	přednáška	k1gFnPc4	přednáška
z	z	k7c2	z
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
kapitola	kapitola	k1gFnSc1	kapitola
25	[number]	k4	25
-	-	kIx~	-
Elektrodynamika	elektrodynamika	k1gFnSc1	elektrodynamika
v	v	k7c6	v
relativistickém	relativistický	k2eAgInSc6d1	relativistický
zápisu	zápis	k1gInSc6	zápis
</s>
