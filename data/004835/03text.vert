<s>
Studio	studio	k1gNnSc1	studio
Ghibli	Ghibli	k1gFnSc2	Ghibli
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
株	株	k?	株
<g/>
,	,	kIx,	,
Kabušiki	Kabušiki	k1gNnPc1	Kabušiki
gaiša	gaiš	k2eAgNnPc1d1	gaiš
Sutadžio	Sutadžio	k6eAd1	Sutadžio
Džiburi	Džiburi	k1gNnSc7	Džiburi
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc1d1	Japonské
animátorské	animátorský	k2eAgNnSc1d1	animátorské
filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
a	a	k8xC	a
pobočka	pobočka	k1gFnSc1	pobočka
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Tokuma	Tokumum	k1gNnSc2	Tokumum
šoten	šotit	k5eAaBmNgMnS	šotit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
anime	animat	k5eAaPmIp3nS	animat
filmy	film	k1gInPc4	film
jsou	být	k5eAaImIp3nP	být
celosvětově	celosvětově	k6eAd1	celosvětově
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
a	a	k8xC	a
sklízejí	sklízet	k5eAaImIp3nP	sklízet
ohlas	ohlas	k1gInSc4	ohlas
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
i	i	k8xC	i
publika	publikum	k1gNnSc2	publikum
–	–	k?	–
řada	řada	k1gFnSc1	řada
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
nejsledovanějším	sledovaný	k2eAgMnPc3d3	nejsledovanější
anime	animat	k5eAaPmIp3nS	animat
filmům	film	k1gInPc3	film
známým	známý	k2eAgNnSc7d1	známé
mimo	mimo	k7c4	mimo
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
studiu	studio	k1gNnSc3	studio
Ghibli	Ghibli	k1gFnSc1	Ghibli
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
japonské	japonský	k2eAgNnSc4d1	Japonské
studio	studio	k1gNnSc4	studio
Disney	Disnea	k1gFnSc2	Disnea
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
podobné	podobný	k2eAgFnSc2d1	podobná
míry	míra	k1gFnSc2	míra
popularity	popularita	k1gFnSc2	popularita
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
Ghibli	Ghibli	k1gFnSc2	Ghibli
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
jej	on	k3xPp3gMnSc4	on
všeobecně	všeobecně	k6eAd1	všeobecně
respektovaný	respektovaný	k2eAgMnSc1d1	respektovaný
režisér	režisér	k1gMnSc1	režisér
Hajao	Hajao	k1gMnSc1	Hajao
Mijazaki	Mijazake	k1gFnSc4	Mijazake
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kolegou	kolega	k1gMnSc7	kolega
Isao	Isao	k6eAd1	Isao
Takahatou	Takahatý	k2eAgFnSc7d1	Takahatý
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
k	k	k7c3	k
filmu	film	k1gInSc3	film
Naušika	Naušika	k1gFnSc1	Naušika
z	z	k7c2	z
Větrného	větrný	k2eAgNnSc2d1	větrné
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
vydání	vydání	k1gNnSc6	vydání
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zastřešilo	zastřešit	k5eAaPmAgNnS	zastřešit
další	další	k2eAgFnSc4d1	další
produkci	produkce	k1gFnSc4	produkce
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
společností	společnost	k1gFnSc7	společnost
je	být	k5eAaImIp3nS	být
Tokuma	Tokuma	k1gFnSc1	Tokuma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
také	také	k9	také
nakládá	nakládat	k5eAaImIp3nS	nakládat
s	s	k7c7	s
právy	právo	k1gNnPc7	právo
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Ghibli	Ghibli	k1gFnSc2	Ghibli
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
přezdívky	přezdívka	k1gFnSc2	přezdívka
pro	pro	k7c4	pro
saharská	saharský	k2eAgNnPc4d1	saharské
průzkumná	průzkumný	k2eAgNnPc4d1	průzkumné
letadla	letadlo	k1gNnPc4	letadlo
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
výrazu	výraz	k1gInSc2	výraz
ghibli	ghibnout	k5eAaPmAgMnP	ghibnout
(	(	kIx(	(
<g/>
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
sirocco	sirocco	k1gMnSc1	sirocco
<g/>
,	,	kIx,	,
jiný	jiný	k2eAgMnSc1d1	jiný
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
horký	horký	k2eAgInSc4d1	horký
vítr	vítr	k1gInSc4	vítr
vanoucí	vanoucí	k2eAgFnSc7d1	vanoucí
Saharou	Sahara	k1gFnSc7	Sahara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
japonská	japonský	k2eAgFnSc1d1	japonská
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
/	/	kIx~	/
<g/>
džibri	džibri	k6eAd1	džibri
<g/>
/	/	kIx~	/
nebo	nebo	k8xC	nebo
/	/	kIx~	/
<g/>
džiburi	džiburi	k1gNnSc1	džiburi
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
domněnky	domněnka	k1gFnSc2	domněnka
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
stojí	stát	k5eAaImIp3nS	stát
úmysl	úmysl	k1gInSc1	úmysl
zakladatelů	zakladatel	k1gMnPc2	zakladatel
do	do	k7c2	do
světa	svět	k1gInSc2	svět
anime	animat	k5eAaPmIp3nS	animat
vnést	vnést	k5eAaPmF	vnést
nový	nový	k2eAgInSc1d1	nový
vítr	vítr	k1gInSc1	vítr
<g/>
;	;	kIx,	;
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
<g/>
,	,	kIx,	,
Hajao	Hajaa	k1gMnSc5	Hajaa
Mijazaki	Mijazak	k1gMnSc5	Mijazak
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známým	známý	k2eAgMnSc7d1	známý
milovníkem	milovník	k1gMnSc7	milovník
létání	létání	k1gNnSc2	létání
a	a	k8xC	a
italskému	italský	k2eAgNnSc3d1	italské
letectvu	letectvo	k1gNnSc3	letectvo
také	také	k9	také
později	pozdě	k6eAd2	pozdě
věnoval	věnovat	k5eAaImAgInS	věnovat
film	film	k1gInSc1	film
Porco	Porco	k6eAd1	Porco
Rosso	Rossa	k1gFnSc5	Rossa
<g/>
.	.	kIx.	.
</s>
<s>
Logem	log	k1gInSc7	log
Studia	studio	k1gNnSc2	studio
Ghibli	Ghibli	k1gFnSc2	Ghibli
je	být	k5eAaImIp3nS	být
Totoro	Totora	k1gFnSc5	Totora
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
filmu	film	k1gInSc2	film
Můj	můj	k3xOp1gMnSc1	můj
soused	soused	k1gMnSc1	soused
Totoro	Totora	k1gFnSc5	Totora
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
filmy	film	k1gInPc4	film
studia	studio	k1gNnSc2	studio
Ghibli	Ghibli	k1gFnSc2	Ghibli
patří	patřit	k5eAaImIp3nP	patřit
Mijazakiho	Mijazaki	k1gMnSc4	Mijazaki
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
Princezna	princezna	k1gFnSc1	princezna
Mononoke	Mononoke	k1gFnSc1	Mononoke
nebo	nebo	k8xC	nebo
Takahatův	Takahatův	k2eAgInSc1d1	Takahatův
Hrob	hrob	k1gInSc1	hrob
světlušek	světluška	k1gFnPc2	světluška
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
doposud	doposud	k6eAd1	doposud
studio	studio	k1gNnSc1	studio
vydalo	vydat	k5eAaPmAgNnS	vydat
22	[number]	k4	22
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Naušika	Naušika	k1gFnSc1	Naušika
z	z	k7c2	z
Větrného	větrný	k2eAgNnSc2d1	větrné
údolí	údolí	k1gNnSc2	údolí
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
produkoval	produkovat	k5eAaImAgMnS	produkovat
Topcraft	Topcraft	k1gMnSc1	Topcraft
<g/>
,	,	kIx,	,
technicky	technicky	k6eAd1	technicky
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
film	film	k1gInSc4	film
studia	studio	k1gNnSc2	studio
Ghibli	Ghibli	k1gFnSc2	Ghibli
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
však	však	k9	však
řadí	řadit	k5eAaImIp3nS	řadit
<g/>
)	)	kIx)	)
Laputa	Laput	k2eAgFnSc1d1	Laputa
<g/>
:	:	kIx,	:
Zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Hrob	hrob	k1gInSc1	hrob
světlušek	světluška	k1gFnPc2	světluška
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Můj	můj	k3xOp1gMnSc1	můj
<g />
.	.	kIx.	.
</s>
<s>
soused	soused	k1gMnSc1	soused
Totoro	Totora	k1gFnSc5	Totora
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Doručovací	doručovací	k2eAgFnSc1d1	doručovací
služba	služba	k1gFnSc1	služba
čarodějky	čarodějka	k1gFnSc2	čarodějka
Kiki	Kik	k1gFnSc2	Kik
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Střípky	střípek	k1gInPc1	střípek
minulosti	minulost	k1gFnSc2	minulost
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Porco	Porco	k6eAd1	Porco
Rosso	Rossa	k1gFnSc5	Rossa
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Umi	Umi	k1gFnSc3	Umi
ga	ga	k?	ga
kikoeru	kikoer	k1gInSc2	kikoer
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Pom	Pom	k1gMnSc1	Pom
poko	poko	k1gMnSc1	poko
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Šepot	šepot	k1gInSc1	šepot
srdce	srdce	k1gNnSc2	srdce
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Princezna	princezna	k1gFnSc1	princezna
Mononoke	Mononoke	k1gFnSc1	Mononoke
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Naši	náš	k3xOp1gMnPc1	náš
sousedé	soused	k1gMnPc1	soused
Jamadovi	Jamadův	k2eAgMnPc1d1	Jamadův
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
fantazie	fantazie	k1gFnSc2	fantazie
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Království	království	k1gNnSc1	království
koček	kočka	k1gFnPc2	kočka
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Příběhy	příběh	k1gInPc7	příběh
ze	z	k7c2	z
Zeměmoří	Zeměmoří	k1gNnSc2	Zeměmoří
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Ponyo	Ponyo	k6eAd1	Ponyo
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
útesu	útes	k1gInSc3	útes
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Arrietty	Arrietta	k1gFnSc2	Arrietta
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
půjčovníčků	půjčovníček	k1gInPc2	půjčovníček
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Kokuriko-zaka	Kokurikoak	k1gMnSc2	Kokuriko-zak
kara	kara	k1gFnSc1	kara
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Zvedá	zvedat	k5eAaImIp3nS	zvedat
se	se	k3xPyFc4	se
vítr	vítr	k1gInSc1	vítr
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
princezně	princezna	k1gFnSc6	princezna
Kaguje	Kaguje	k1gFnSc2	Kaguje
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Omoide	Omoid	k1gInSc5	Omoid
no	no	k9	no
Marnie	Marnie	k1gFnSc2	Marnie
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Červená	červený	k2eAgFnSc1d1	červená
<g />
.	.	kIx.	.
</s>
<s>
želva	želva	k1gFnSc1	želva
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Sora	Sora	k1gFnSc1	Sora
iro	iro	k?	iro
no	no	k9	no
tane	tanout	k5eAaImIp3nS	tanout
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
krátký	krátký	k2eAgInSc1d1	krátký
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
Nandaró	Nandaró	k1gFnSc1	Nandaró
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgFnSc1d1	televizní
reklama	reklama	k1gFnSc1	reklama
pro	pro	k7c4	pro
stanici	stanice	k1gFnSc4	stanice
NHK	NHK	kA	NHK
<g/>
)	)	kIx)	)
On	on	k3xPp3gMnSc1	on
Your	Your	k1gMnSc1	Your
Mark	Mark	k1gMnSc1	Mark
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgInSc1d1	hudební
klip	klip	k1gInSc1	klip
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Ghiblies	Ghiblies	k1gMnSc1	Ghiblies
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
krátký	krátký	k2eAgInSc1d1	krátký
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
o	o	k7c6	o
studiu	studio	k1gNnSc6	studio
<g/>
)	)	kIx)	)
Umača	Umača	k1gMnSc1	Umača
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgFnPc1d1	televizní
reklamy	reklama	k1gFnPc1	reklama
<g/>
)	)	kIx)	)
Ghiblies	Ghiblies	k1gInSc1	Ghiblies
<g/>
,	,	kIx,	,
epizoda	epizoda	k1gFnSc1	epizoda
2	[number]	k4	2
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
pokračování	pokračování	k1gNnSc1	pokračování
Ghiblies	Ghibliesa	k1gFnPc2	Ghibliesa
<g/>
,	,	kIx,	,
promítáno	promítat	k5eAaImNgNnS	promítat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Královstvím	království	k1gNnSc7	království
koček	kočka	k1gFnPc2	kočka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Lasseter-san	Lasseteran	k1gMnSc1	Lasseter-san
<g/>
,	,	kIx,	,
arigató	arigató	k?	arigató
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
děkovné	děkovný	k2eAgNnSc1d1	děkovné
video	video	k1gNnSc1	video
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
pro	pro	k7c4	pro
Johna	John	k1gMnSc4	John
Lassetera	Lasseter	k1gMnSc4	Lasseter
<g/>
)	)	kIx)	)
Tanejamagahara	Tanejamagahara	k1gFnSc1	Tanejamagahara
no	no	k9	no
joru	joru	k6eAd1	joru
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Kúsó	Kúsó	k1gFnSc7	Kúsó
no	no	k9	no
kikaitači	kikaitač	k1gMnPc1	kikaitač
no	no	k9	no
naka	naka	k6eAd1	naka
no	no	k9	no
hakai	hakai	k6eAd1	hakai
no	no	k9	no
hacumei	hacumei	k6eAd1	hacumei
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Koro	Koro	k6eAd1	Koro
no	no	k9	no
daisanpo	daisanpa	k1gFnSc5	daisanpa
(	(	kIx(	(
<g/>
Korova	Korův	k2eAgFnSc1d1	Korův
velká	velký	k2eAgFnSc1d1	velká
procházka	procházka	k1gFnSc1	procházka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Kudžiratori	Kudžiratori	k1gNnSc1	Kudžiratori
(	(	kIx(	(
<g/>
Lov	lov	k1gInSc1	lov
velryb	velryba	k1gFnPc2	velryba
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Mei	Mei	k1gFnSc4	Mei
to	ten	k3xDgNnSc1	ten
konekobasu	konekobas	k1gMnSc3	konekobas
(	(	kIx(	(
<g/>
Mei	Mei	k1gMnSc1	Mei
a	a	k8xC	a
koťátkobus	koťátkobus	k1gMnSc1	koťátkobus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Jadosagaši	Jadosagaše	k1gFnSc6	Jadosagaše
(	(	kIx(	(
<g/>
Hledání	hledání	k1gNnSc1	hledání
domova	domov	k1gInSc2	domov
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Hoši	hoch	k1gMnPc1	hoch
wo	wo	k?	wo
katta	katta	k1gFnSc1	katta
hi	hi	k0	hi
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsem	být	k5eAaImIp1nS	být
ulovil	ulovit	k5eAaPmAgMnS	ulovit
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Mizugumo	Mizuguma	k1gFnSc5	Mizuguma
Monmon	Monmon	k1gNnSc1	Monmon
(	(	kIx(	(
<g/>
Vodní	vodní	k2eAgMnSc1d1	vodní
pavouk	pavouk	k1gMnSc1	pavouk
Monmon	Monmon	k1gMnSc1	Monmon
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Sanzoku	Sanzok	k1gInSc2	Sanzok
no	no	k9	no
musume	musum	k1gInSc5	musum
Ronja	Ronjum	k1gNnPc1	Ronjum
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
–	–	k?	–
adaptace	adaptace	k1gFnSc1	adaptace
knihy	kniha	k1gFnSc2	kniha
Ronja	Ronja	k1gFnSc1	Ronja
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
loupežníka	loupežník	k1gMnSc2	loupežník
od	od	k7c2	od
Astrid	Astrida	k1gFnPc2	Astrida
Lindgrenové	Lindgrenová	k1gFnSc2	Lindgrenová
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
s	s	k7c7	s
Polygon	polygon	k1gInSc1	polygon
Pictures	Pictures	k1gMnSc1	Pictures
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Studio	studio	k1gNnSc1	studio
Ghibli	Ghibli	k1gFnSc4	Ghibli
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
studia	studio	k1gNnSc2	studio
Ghibli	Ghibli	k1gFnSc2	Ghibli
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
společnosti	společnost	k1gFnSc2	společnost
Tokuma	Tokuma	k1gFnSc1	Tokuma
Šóten	Šóten	k1gInSc1	Šóten
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
Nausicaa	Nausicaa	k1gFnSc1	Nausicaa
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
-	-	kIx~	-
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3	nejrozsáhlejší
zdroj	zdroj	k1gInSc1	zdroj
o	o	k7c6	o
tvorbě	tvorba	k1gFnSc6	tvorba
studia	studio	k1gNnSc2	studio
Ghibli	Ghibli	k1gFnSc2	Ghibli
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
</s>
