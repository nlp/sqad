<s>
Česká	český	k2eAgFnSc1d1	Česká
obchodní	obchodní	k2eAgFnSc1d1	obchodní
inspekce	inspekce	k1gFnSc1	inspekce
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
ČOI	ČOI	kA	ČOI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc1	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
podřízený	podřízený	k2eAgMnSc1d1	podřízený
Ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
průmyslu	průmysl	k1gInSc3	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc3	obchod
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Ústředního	ústřední	k2eAgMnSc4d1	ústřední
ředitele	ředitel	k1gMnSc4	ředitel
ČOI	ČOI	kA	ČOI
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
ministr	ministr	k1gMnSc1	ministr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
ČOI	ČOI	kA	ČOI
byla	být	k5eAaImAgFnS	být
ustanovena	ustanovit	k5eAaPmNgFnS	ustanovit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
64	[number]	k4	64
<g/>
/	/	kIx~	/
<g/>
1986	[number]	k4	1986
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
obchodní	obchodní	k2eAgFnSc6d1	obchodní
inspekci	inspekce	k1gFnSc6	inspekce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nástupnickou	nástupnický	k2eAgFnSc7d1	nástupnická
organizací	organizace	k1gFnSc7	organizace
někdejší	někdejší	k2eAgFnSc2d1	někdejší
Státní	státní	k2eAgFnSc2d1	státní
obchodní	obchodní	k2eAgFnSc2d1	obchodní
inspekce	inspekce	k1gFnSc2	inspekce
<g/>
.	.	kIx.	.
</s>
<s>
Člení	členit	k5eAaImIp3nS	členit
se	se	k3xPyFc4	se
na	na	k7c4	na
ústřední	ústřední	k2eAgInSc4d1	ústřední
inspektorát	inspektorát	k1gInSc4	inspektorát
a	a	k8xC	a
jemu	on	k3xPp3gMnSc3	on
podřízené	podřízený	k2eAgInPc1d1	podřízený
inspektoráty	inspektorát	k1gInPc1	inspektorát
se	s	k7c7	s
sídly	sídlo	k1gNnPc7	sídlo
v	v	k7c6	v
krajských	krajský	k2eAgNnPc6d1	krajské
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
obchodní	obchodní	k2eAgFnSc1d1	obchodní
inspekce	inspekce	k1gFnSc1	inspekce
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
a	a	k8xC	a
dozoruje	dozorovat	k5eAaImIp3nS	dozorovat
právnické	právnický	k2eAgFnPc4d1	právnická
a	a	k8xC	a
fyzické	fyzický	k2eAgFnPc4d1	fyzická
osoby	osoba	k1gFnPc4	osoba
prodávající	prodávající	k2eAgMnSc1d1	prodávající
nebo	nebo	k8xC	nebo
dodávající	dodávající	k2eAgInPc1d1	dodávající
výrobky	výrobek	k1gInPc1	výrobek
a	a	k8xC	a
zboží	zboží	k1gNnSc4	zboží
na	na	k7c4	na
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
poskytující	poskytující	k2eAgFnPc4d1	poskytující
služby	služba	k1gFnPc4	služba
nebo	nebo	k8xC	nebo
vyvíjející	vyvíjející	k2eAgFnSc4d1	vyvíjející
jinou	jiný	k2eAgFnSc4d1	jiná
podobnou	podobný	k2eAgFnSc4d1	podobná
činnost	činnost	k1gFnSc4	činnost
na	na	k7c6	na
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
poskytující	poskytující	k2eAgInSc1d1	poskytující
spotřebitelský	spotřebitelský	k2eAgInSc1d1	spotřebitelský
úvěr	úvěr	k1gInSc1	úvěr
nebo	nebo	k8xC	nebo
provozující	provozující	k2eAgNnSc1d1	provozující
tržiště	tržiště	k1gNnSc1	tržiště
(	(	kIx(	(
<g/>
tržnice	tržnice	k1gFnSc1	tržnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
podle	podle	k7c2	podle
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
právních	právní	k2eAgFnPc2d1	právní
<g />
.	.	kIx.	.
</s>
<s>
předpisů	předpis	k1gInPc2	předpis
nevykonává	vykonávat	k5eNaImIp3nS	vykonávat
tento	tento	k3xDgInSc1	tento
dozor	dozor	k1gInSc1	dozor
jiný	jiný	k2eAgInSc1d1	jiný
správní	správní	k2eAgInSc1d1	správní
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
dodržování	dodržování	k1gNnSc1	dodržování
podmínek	podmínka	k1gFnPc2	podmínka
stanovených	stanovený	k2eAgInPc2d1	stanovený
k	k	k7c3	k
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
jakosti	jakost	k1gFnSc2	jakost
zboží	zboží	k1gNnSc2	zboží
nebo	nebo	k8xC	nebo
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
potravin	potravina	k1gFnPc2	potravina
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gFnSc2	jejich
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
nezávadnosti	nezávadnost	k1gFnSc2	nezávadnost
<g/>
,	,	kIx,	,
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
skladování	skladování	k1gNnSc4	skladování
a	a	k8xC	a
dopravu	doprava	k1gFnSc4	doprava
<g/>
;	;	kIx,	;
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
zboží	zboží	k1gNnSc2	zboží
používají	používat	k5eAaImIp3nP	používat
ověřená	ověřený	k2eAgNnPc1d1	ověřené
měřidla	měřidlo	k1gNnPc1	měřidlo
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
ověření	ověření	k1gNnSc4	ověření
podléhají	podléhat	k5eAaImIp3nP	podléhat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zda	zda	k8xS	zda
používaná	používaný	k2eAgNnPc1d1	používané
měřidla	měřidlo	k1gNnPc1	měřidlo
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
příslušným	příslušný	k2eAgInPc3d1	příslušný
předpisům	předpis	k1gInPc3	předpis
<g/>
,	,	kIx,	,
technickým	technický	k2eAgFnPc3d1	technická
normám	norma	k1gFnPc3	norma
či	či	k8xC	či
patřičnému	patřičný	k2eAgNnSc3d1	patřičné
schválení	schválení	k1gNnSc3	schválení
<g/>
;	;	kIx,	;
dodržování	dodržování	k1gNnSc3	dodržování
podmínek	podmínka	k1gFnPc2	podmínka
stanovených	stanovený	k2eAgFnPc2d1	stanovená
právními	právní	k2eAgInPc7d1	právní
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
příslušnými	příslušný	k2eAgInPc7d1	příslušný
předpisy	předpis	k1gInPc7	předpis
pro	pro	k7c4	pro
poskytování	poskytování	k1gNnSc4	poskytování
určitých	určitý	k2eAgFnPc2d1	určitá
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
provozování	provozování	k1gNnSc4	provozování
některých	některý	k3yIgFnPc2	některý
specifických	specifický	k2eAgFnPc2d1	specifická
činností	činnost	k1gFnPc2	činnost
<g/>
;	;	kIx,	;
zda	zda	k8xS	zda
při	při	k7c6	při
uvádění	uvádění	k1gNnSc6	uvádění
výrobků	výrobek	k1gInPc2	výrobek
na	na	k7c4	na
trh	trh	k1gInSc4	trh
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
výrobky	výrobek	k1gInPc1	výrobek
opatřeny	opatřen	k2eAgInPc1d1	opatřen
náležitým	náležitý	k2eAgInSc7d1	náležitý
<g />
.	.	kIx.	.
</s>
<s>
povinným	povinný	k2eAgNnSc7d1	povinné
označením	označení	k1gNnSc7	označení
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
zda	zda	k8xS	zda
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
či	či	k8xC	či
přiložen	přiložit	k5eAaPmNgInS	přiložit
předepsaný	předepsaný	k2eAgInSc1d1	předepsaný
certifikát	certifikát	k1gInSc1	certifikát
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vlastnosti	vlastnost	k1gFnPc1	vlastnost
stanovených	stanovený	k2eAgInPc2d1	stanovený
výrobků	výrobek	k1gInPc2	výrobek
uvedených	uvedený	k2eAgInPc2d1	uvedený
na	na	k7c4	na
trh	trh	k1gInSc4	trh
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
příslušným	příslušný	k2eAgInPc3d1	příslušný
technickým	technický	k2eAgInPc3d1	technický
požadavkům	požadavek	k1gInPc3	požadavek
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
;	;	kIx,	;
zda	zda	k8xS	zda
výrobky	výrobek	k1gInPc1	výrobek
uváděné	uváděný	k2eAgInPc1d1	uváděný
na	na	k7c4	na
trh	trh	k1gInSc4	trh
jsou	být	k5eAaImIp3nP	být
bezpečné	bezpečný	k2eAgInPc1d1	bezpečný
<g/>
;	;	kIx,	;
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
sjednávání	sjednávání	k1gNnSc6	sjednávání
spotřebitelského	spotřebitelský	k2eAgInSc2d1	spotřebitelský
úvěru	úvěr	k1gInSc2	úvěr
dodržovány	dodržován	k2eAgFnPc4d1	dodržována
povinnosti	povinnost	k1gFnPc4	povinnost
stanovené	stanovený	k2eAgFnPc4d1	stanovená
právními	právní	k2eAgInPc7d1	právní
<g />
.	.	kIx.	.
</s>
<s>
předpisy	předpis	k1gInPc1	předpis
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
dozor	dozor	k1gInSc1	dozor
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
případě	případ	k1gInSc6	případ
nevykonává	vykonávat	k5eNaImIp3nS	vykonávat
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
č.	č.	k?	č.
64	[number]	k4	64
<g/>
/	/	kIx~	/
<g/>
1986	[number]	k4	1986
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
obchodní	obchodní	k2eAgFnSc6d1	obchodní
inspekci	inspekce	k1gFnSc6	inspekce
<g/>
,	,	kIx,	,
č.	č.	k?	č.
634	[number]	k4	634
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
spotřebitele	spotřebitel	k1gMnSc2	spotřebitel
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g />
.	.	kIx.	.
</s>
<s>
22	[number]	k4	22
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
technických	technický	k2eAgInPc6d1	technický
požadavcích	požadavek	k1gInPc6	požadavek
na	na	k7c4	na
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
č.	č.	k?	č.
102	[number]	k4	102
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
obecné	obecný	k2eAgFnSc6d1	obecná
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
č.	č.	k?	č.
477	[number]	k4	477
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
obalech	obal	k1gInPc6	obal
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g />
.	.	kIx.	.
</s>
<s>
86	[number]	k4	86
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
,	,	kIx,	,
č.	č.	k?	č.
311	[number]	k4	311
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
pohonných	pohonný	k2eAgFnPc6d1	pohonná
hmotách	hmota	k1gFnPc6	hmota
<g/>
,	,	kIx,	,
č.	č.	k?	č.
379	[number]	k4	379
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
opatřeních	opatření	k1gNnPc6	opatření
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
před	před	k7c7	před
škodami	škoda	k1gFnPc7	škoda
působenými	působený	k2eAgFnPc7d1	působená
tabákovými	tabákový	k2eAgInPc7d1	tabákový
<g />
.	.	kIx.	.
</s>
<s>
výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
alkoholem	alkohol	k1gInSc7	alkohol
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
návykovými	návykový	k2eAgFnPc7d1	návyková
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
č.	č.	k?	č.
353	[number]	k4	353
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
spotřebních	spotřební	k2eAgFnPc6d1	spotřební
daních	daň	k1gFnPc6	daň
<g/>
,	,	kIx,	,
č.	č.	k?	č.
145	[number]	k4	145
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
spotřebitelském	spotřebitelský	k2eAgInSc6d1	spotřebitelský
úvěru	úvěr	k1gInSc6	úvěr
<g/>
,	,	kIx,	,
č.	č.	k?	č.
189	[number]	k4	189
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
o	o	k7c6	o
nouzových	nouzový	k2eAgFnPc6d1	nouzová
zásobách	zásoba	k1gFnPc6	zásoba
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
č.	č.	k?	č.
253	[number]	k4	253
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
některých	některý	k3yIgNnPc6	některý
opatřeních	opatření	k1gNnPc6	opatření
proti	proti	k7c3	proti
legalizaci	legalizace	k1gFnSc3	legalizace
výnosů	výnos	k1gInPc2	výnos
z	z	k7c2	z
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
financování	financování	k1gNnSc2	financování
terorismu	terorismus	k1gInSc2	terorismus
<g/>
,	,	kIx,	,
č.	č.	k?	č.
56	[number]	k4	56
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
podmínkách	podmínka	k1gFnPc6	podmínka
provozu	provoz	k1gInSc2	provoz
vozidel	vozidlo	k1gNnPc2	vozidlo
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
taxislužby	taxislužba	k1gFnSc2	taxislužba
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
č.	č.	k?	č.
247	[number]	k4	247
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
omezení	omezení	k1gNnSc6	omezení
provozu	provoz	k1gInSc2	provoz
zastaváren	zastavárna	k1gFnPc2	zastavárna
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
jiných	jiný	k2eAgFnPc2d1	jiná
provozoven	provozovna	k1gFnPc2	provozovna
v	v	k7c6	v
noční	noční	k2eAgFnSc6d1	noční
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
č.	č.	k?	č.
255	[number]	k4	255
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
kontrole	kontrola	k1gFnSc6	kontrola
(	(	kIx(	(
<g/>
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
řád	řád	k1gInSc4	řád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
uvedených	uvedený	k2eAgInPc2d1	uvedený
zákonů	zákon	k1gInPc2	zákon
může	moct	k5eAaImIp3nS	moct
Česká	český	k2eAgFnSc1d1	Česká
obchodní	obchodní	k2eAgFnSc1d1	obchodní
inspekce	inspekce	k1gFnSc1	inspekce
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
uložit	uložit	k5eAaPmF	uložit
kontrolovanému	kontrolovaný	k2eAgInSc3d1	kontrolovaný
subjektu	subjekt	k1gInSc3	subjekt
pokutu	pokuta	k1gFnSc4	pokuta
až	až	k6eAd1	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
méně	málo	k6eAd2	málo
závažná	závažný	k2eAgNnPc4d1	závažné
porušení	porušení	k1gNnPc4	porušení
zákona	zákon	k1gInSc2	zákon
mohou	moct	k5eAaImIp3nP	moct
inspektoři	inspektor	k1gMnPc1	inspektor
ČOI	ČOI	kA	ČOI
uložit	uložit	k5eAaPmF	uložit
příkazem	příkaz	k1gInSc7	příkaz
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
kontrolované	kontrolovaný	k2eAgFnSc2d1	kontrolovaná
osobě	osoba	k1gFnSc3	osoba
blokovou	blokový	k2eAgFnSc4d1	bloková
pokutu	pokuta	k1gFnSc4	pokuta
do	do	k7c2	do
5000	[number]	k4	5000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
fyzickým	fyzický	k2eAgFnPc3d1	fyzická
osobám	osoba	k1gFnPc3	osoba
prodávajícím	prodávající	k2eAgMnSc7d1	prodávající
produkty	produkt	k1gInPc7	produkt
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
drobné	drobný	k2eAgFnSc2d1	drobná
chovatelské	chovatelský	k2eAgFnSc2d1	chovatelská
či	či	k8xC	či
pěstitelské	pěstitelský	k2eAgFnSc2d1	pěstitelská
činnosti	činnost	k1gFnSc2	činnost
anebo	anebo	k8xC	anebo
lesní	lesní	k2eAgFnSc2d1	lesní
plodiny	plodina	k1gFnSc2	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
finančních	finanční	k2eAgFnPc2d1	finanční
sankcí	sankce	k1gFnPc2	sankce
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
ČOI	ČOI	kA	ČOI
i	i	k8xC	i
zákazy	zákaz	k1gInPc1	zákaz
prodeje	prodej	k1gFnSc2	prodej
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
zákazy	zákaz	k1gInPc7	zákaz
uvádění	uvádění	k1gNnSc2	uvádění
výrobků	výrobek	k1gInPc2	výrobek
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
požadavkům	požadavek	k1gInPc3	požadavek
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
obchodní	obchodní	k2eAgFnSc1d1	obchodní
inspekce	inspekce	k1gFnSc1	inspekce
nekontroluje	nekontrolovat	k5eAaImIp3nS	nekontrolovat
kvalitu	kvalita	k1gFnSc4	kvalita
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
pokrmů	pokrm	k1gInPc2	pokrm
a	a	k8xC	a
tabákových	tabákový	k2eAgInPc2d1	tabákový
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
produkty	produkt	k1gInPc7	produkt
a	a	k8xC	a
službami	služba	k1gFnPc7	služba
se	se	k3xPyFc4	se
ČOI	ČOI	kA	ČOI
zabývá	zabývat	k5eAaImIp3nS	zabývat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
poctivosti	poctivost	k1gFnSc2	poctivost
jejich	jejich	k3xOp3gInSc2	jejich
prodeje	prodej	k1gInSc2	prodej
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
účtování	účtování	k1gNnSc1	účtování
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
kvalitou	kvalita	k1gFnSc7	kvalita
potravinářských	potravinářský	k2eAgInPc2d1	potravinářský
výrobků	výrobek	k1gInPc2	výrobek
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
Státní	státní	k2eAgFnSc1d1	státní
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
inspekce	inspekce	k1gFnSc1	inspekce
<g/>
.	.	kIx.	.
</s>
<s>
Dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
nezávadností	nezávadnost	k1gFnSc7	nezávadnost
potravin	potravina	k1gFnPc2	potravina
živočišného	živočišný	k2eAgInSc2d1	živočišný
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ochranou	ochrana	k1gFnSc7	ochrana
našeho	náš	k3xOp1gNnSc2	náš
území	území	k1gNnSc2	území
před	před	k7c7	před
možným	možný	k2eAgNnSc7d1	možné
zavlečením	zavlečení	k1gNnSc7	zavlečení
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
nákaz	nákaza	k1gFnPc2	nákaza
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gMnPc2	jejich
nositelů	nositel	k1gMnPc2	nositel
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
Státní	státní	k2eAgFnSc1d1	státní
veterinární	veterinární	k2eAgFnSc1d1	veterinární
správa	správa	k1gFnSc1	správa
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
dozor	dozor	k1gInSc1	dozor
nad	nad	k7c7	nad
dodržováním	dodržování	k1gNnSc7	dodržování
zákazů	zákaz	k1gInPc2	zákaz
a	a	k8xC	a
plněním	plnění	k1gNnSc7	plnění
povinností	povinnost	k1gFnPc2	povinnost
stanovených	stanovený	k2eAgFnPc2d1	stanovená
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
předpisy	předpis	k1gInPc4	předpis
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ochrany	ochrana	k1gFnSc2	ochrana
zdraví	zdraví	k1gNnSc2	zdraví
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
orgány	orgán	k1gInPc1	orgán
ochrany	ochrana	k1gFnSc2	ochrana
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
www.coi.cz	www.coi.cza	k1gFnPc2	www.coi.cza
naleznete	naleznout	k5eAaPmIp2nP	naleznout
všechny	všechen	k3xTgFnPc4	všechen
základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
obchodní	obchodní	k2eAgFnSc6d1	obchodní
inspekci	inspekce	k1gFnSc6	inspekce
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
zveřejňovány	zveřejňován	k2eAgFnPc1d1	zveřejňována
informace	informace	k1gFnPc1	informace
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
106	[number]	k4	106
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
svobodném	svobodný	k2eAgInSc6d1	svobodný
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
informacím	informace	k1gFnPc3	informace
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
obchodní	obchodní	k2eAgFnSc1d1	obchodní
inspekce	inspekce	k1gFnSc1	inspekce
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
občanům	občan	k1gMnPc3	občan
informační	informační	k2eAgFnSc2d1	informační
a	a	k8xC	a
poradenské	poradenský	k2eAgFnSc2d1	poradenská
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
obvyklých	obvyklý	k2eAgInPc6d1	obvyklý
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
na	na	k7c6	na
každém	každý	k3xTgNnSc6	každý
z	z	k7c2	z
jejích	její	k3xOp3gNnPc2	její
pracovišť	pracoviště	k1gNnPc2	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
média	médium	k1gNnPc4	médium
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
46	[number]	k4	46
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
tiskový	tiskový	k2eAgMnSc1d1	tiskový
mluvčí	mluvčí	k1gMnSc1	mluvčí
ČOI	ČOI	kA	ČOI
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
veškerá	veškerý	k3xTgNnPc4	veškerý
data	datum	k1gNnPc4	datum
o	o	k7c6	o
provedených	provedený	k2eAgFnPc6d1	provedená
kontrolách	kontrola	k1gFnPc6	kontrola
<g/>
,	,	kIx,	,
přijatých	přijatý	k2eAgNnPc2d1	přijaté
opatření	opatření	k1gNnPc2	opatření
a	a	k8xC	a
uložených	uložený	k2eAgFnPc6d1	uložená
pokutách	pokuta	k1gFnPc6	pokuta
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
otevřených	otevřený	k2eAgNnPc2d1	otevřené
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
ČOI	ČOI	kA	ČOI
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgInSc7	první
orgánem	orgán	k1gInSc7	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
poskytovat	poskytovat	k5eAaImF	poskytovat
otevřená	otevřený	k2eAgNnPc1d1	otevřené
data	datum	k1gNnPc1	datum
o	o	k7c6	o
kontrolách	kontrola	k1gFnPc6	kontrola
a	a	k8xC	a
také	také	k9	také
prvním	první	k4xOgInSc7	první
orgánem	orgán	k1gInSc7	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
poskytovat	poskytovat	k5eAaImF	poskytovat
otevřená	otevřený	k2eAgNnPc1d1	otevřené
data	datum	k1gNnPc1	datum
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Linked	Linked	k1gInSc1	Linked
open	opena	k1gFnPc2	opena
data	datum	k1gNnSc2	datum
(	(	kIx(	(
<g/>
propojitelných	propojitelný	k2eAgNnPc2d1	propojitelné
otevřených	otevřený	k2eAgNnPc2d1	otevřené
dat	datum	k1gNnPc2	datum
<g/>
)	)	kIx)	)
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
RDF	RDF	kA	RDF
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ČOI	ČOI	kA	ČOI
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
Evropské	evropský	k2eAgNnSc1d1	Evropské
spotřebitelské	spotřebitelský	k2eAgNnSc1d1	spotřebitelské
centrum	centrum	k1gNnSc1	centrum
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
právech	právo	k1gNnPc6	právo
spotřebitelů	spotřebitel	k1gMnPc2	spotřebitel
při	při	k7c6	při
přeshraničním	přeshraniční	k2eAgNnSc6d1	přeshraniční
nakupování	nakupování	k1gNnSc6	nakupování
u	u	k7c2	u
obchodníků	obchodník	k1gMnPc2	obchodník
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
Islandu	Island	k1gInSc2	Island
a	a	k8xC	a
také	také	k9	také
bezplatně	bezplatně	k6eAd1	bezplatně
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
řešit	řešit	k5eAaImF	řešit
spory	spor	k1gInPc4	spor
českých	český	k2eAgMnPc2d1	český
spotřebitelů	spotřebitel	k1gMnPc2	spotřebitel
s	s	k7c7	s
prodejci	prodejce	k1gMnPc7	prodejce
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
poskytovateli	poskytovatel	k1gMnPc7	poskytovatel
služeb	služba	k1gFnPc2	služba
z	z	k7c2	z
uvedených	uvedený	k2eAgFnPc2d1	uvedená
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
ESC	ESC	kA	ESC
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
sítě	síť	k1gFnSc2	síť
Evropských	evropský	k2eAgNnPc2d1	Evropské
spotřebitelských	spotřebitelský	k2eAgNnPc2d1	spotřebitelské
center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
ECC-Net	ECC-Net	k1gInSc1	ECC-Net
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zřízené	zřízený	k2eAgInPc1d1	zřízený
a	a	k8xC	a
financované	financovaný	k2eAgInPc1d1	financovaný
Evropskou	evropský	k2eAgFnSc7d1	Evropská
komisí	komise	k1gFnSc7	komise
a	a	k8xC	a
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
podílejícími	podílející	k2eAgFnPc7d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
projektu	projekt	k1gInSc6	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
naleznete	nalézt	k5eAaBmIp2nP	nalézt
na	na	k7c6	na
webu	web	k1gInSc6	web
www.evropskyspotrebitel.cz	www.evropskyspotrebitel.cz	k1gInSc1	www.evropskyspotrebitel.cz
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
obchodní	obchodní	k2eAgFnSc1d1	obchodní
inspekce	inspekce	k1gFnSc1	inspekce
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Evropské	evropský	k2eAgNnSc1d1	Evropské
spotřebitelské	spotřebitelský	k2eAgNnSc1d1	spotřebitelské
centrum	centrum	k1gNnSc1	centrum
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
