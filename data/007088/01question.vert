<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
skalární	skalární	k2eAgFnSc1d1	skalární
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
jako	jako	k9	jako
schopnost	schopnost	k1gFnSc1	schopnost
hmoty	hmota	k1gFnSc2	hmota
konat	konat	k5eAaImF	konat
práci	práce	k1gFnSc4	práce
<g/>
?	?	kIx.	?
</s>
