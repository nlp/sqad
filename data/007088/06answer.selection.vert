<s>
Energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
skalární	skalární	k2eAgFnSc1d1	skalární
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
schopnost	schopnost	k1gFnSc4	schopnost
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
látky	látka	k1gFnPc4	látka
nebo	nebo	k8xC	nebo
pole	pole	k1gNnPc4	pole
<g/>
)	)	kIx)	)
konat	konat	k5eAaImF	konat
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
