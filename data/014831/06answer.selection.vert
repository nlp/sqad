<s hack="1">
přestal	přestat	k5eAaPmAgMnS
vykonávat	vykonávat	k5eAaImF
a	a	k8xC
byl	být	k5eAaImAgMnS
v	v	k7c6
září	září	k1gNnSc6
1868	#num#	k4
zbaven	zbavit	k5eAaPmNgInS
mandátu	mandát	k1gInSc3
pro	pro	k7c4
absenci	absence	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Mandát	mandát	k1gInSc1
získal	získat	k5eAaPmAgInS
i	i	k9
v	v	k7c6
dalších	další	k2eAgFnPc6d1
zemských	zemský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1870	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zemských	zemský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1872	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rámci	rámec	k1gInSc6
tehdejší	tehdejší	k2eAgFnSc2d1
obnovené	obnovený	k2eAgFnSc2d1
pasivní	pasivní	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
mandát	mandát	k1gInSc1
přestal	přestat	k5eAaPmAgInS
vykonávat	vykonávat	k5eAaImF
a	a	k8xC
byl	být	k5eAaImAgMnS
zbaven	zbavit	k5eAaPmNgMnS
mandátu	mandát	k1gInSc2
pro	pro	k7c4
absenci	absence	k1gFnSc4
<g/>
.	.	kIx.
</s>