<s>
Ignác	Ignác	k1gMnSc1
Moravec	Moravec	k1gMnSc1
</s>
<s>
Ignác	Ignác	k1gMnSc1
Moravec	Moravec	k1gMnSc1
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Českého	český	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1867	#num#	k4
–	–	k?
1883	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Starosta	Starosta	k1gMnSc1
Jindřichova	Jindřichův	k2eAgInSc2d1
Hradce	Hradec	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1861	#num#	k4
–	–	k?
1867	#num#	k4
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1876	#num#	k4
–	–	k?
1882	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Národní	národní	k2eAgFnSc1d1
str	str	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
staročeši	staročech	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1824	#num#	k4
Nová	nový	k2eAgFnSc1d1
VčelniceRakouské	VčelniceRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1890	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Jindřichův	Jindřichův	k2eAgMnSc5d1
HradecRakousko-Uhersko	HradecRakousko-Uherska	k1gMnSc5
Rakousko-Uhersko	Rakousko-Uherska	k1gMnSc5
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ignác	Ignác	k1gMnSc1
Moravec	Moravec	k1gMnSc1
<g/>
,	,	kIx,
též	též	k9
Hynek	Hynek	k1gMnSc1
Moravec	Moravec	k1gMnSc1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1824	#num#	k4
Nová	nový	k2eAgFnSc1d1
Včelnice	včelnice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
některé	některý	k3yIgInPc1
zdroje	zdroj	k1gInPc1
uvádějí	uvádět	k5eAaImIp3nP
jako	jako	k9
místo	místo	k7c2
narození	narození	k1gNnSc2
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1890	#num#	k4
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
české	český	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
z	z	k7c2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
poslanec	poslanec	k1gMnSc1
Českého	český	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
a	a	k8xC
starosta	starosta	k1gMnSc1
Jindřichova	Jindřichův	k2eAgInSc2d1
Hradce	Hradec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Některé	některý	k3yIgInPc1
zdroje	zdroj	k1gInPc1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Jindřichově	Jindřichův	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
nekrologu	nekrolog	k1gInSc6
v	v	k7c6
místním	místní	k2eAgInSc6d1
tisku	tisk	k1gInSc6
je	být	k5eAaImIp3nS
ale	ale	k9
zmiňováno	zmiňován	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
nebyl	být	k5eNaImAgMnS
zdejším	zdejší	k2eAgMnSc7d1
rodákem	rodák	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Působil	působit	k5eAaImAgMnS
jako	jako	k9
majitel	majitel	k1gMnSc1
textilní	textilní	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
a	a	k8xC
měšťan	měšťan	k1gMnSc1
v	v	k7c6
Jindřichově	Jindřichův	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
městě	město	k1gNnSc6
zastával	zastávat	k5eAaImAgMnS
v	v	k7c6
letech	let	k1gInPc6
1861	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
a	a	k8xC
1876	#num#	k4
<g/>
–	–	k?
<g/>
1882	#num#	k4
funkci	funkce	k1gFnSc6
starosty	starosta	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
též	též	k6eAd1
členem	člen	k1gMnSc7
okresního	okresní	k2eAgNnSc2d1
zastupitelstva	zastupitelstvo	k1gNnSc2
v	v	k7c6
Jindřichově	Jindřichův	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgMnS
i	i	k9
do	do	k7c2
vysoké	vysoký	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemských	zemský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
lednu	leden	k1gInSc6
1867	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
na	na	k7c4
Český	český	k2eAgInSc4d1
zemský	zemský	k2eAgInSc4d1
sněm	sněm	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
zastupoval	zastupovat	k5eAaImAgInS
kurii	kurie	k1gFnSc4
městskou	městský	k2eAgFnSc4d1
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
<g/>
,	,	kIx,
Bystřice	Bystřice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Mandát	mandát	k1gInSc1
zde	zde	k6eAd1
obhájil	obhájit	k5eAaPmAgInS
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
březnu	březen	k1gInSc6
1867	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
1868	#num#	k4
patřil	patřit	k5eAaImAgInS
mezi	mezi	k7c4
81	#num#	k4
signatářů	signatář	k1gMnPc2
státoprávní	státoprávní	k2eAgFnSc2d1
deklarace	deklarace	k1gFnSc2
českých	český	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
česká	český	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
odmítla	odmítnout	k5eAaPmAgFnS
centralistické	centralistický	k2eAgNnSc4d1
směřování	směřování	k1gNnSc4
státu	stát	k1gInSc2
a	a	k8xC
hájila	hájit	k5eAaImAgFnS
české	český	k2eAgNnSc4d1
státní	státní	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rámci	rámec	k1gInSc6
tehdejší	tehdejší	k2eAgFnSc2d1
pasivní	pasivní	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
<g/>
,	,	kIx,
praktikované	praktikovaný	k2eAgNnSc1d1
Národní	národní	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
(	(	kIx(
<g/>
staročeskou	staročeský	k2eAgFnSc7d1
<g/>
)	)	kIx)
mandát	mandát	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
přestal	přestat	k5eAaPmAgMnS
vykonávat	vykonávat	k5eAaImF
a	a	k8xC
byl	být	k5eAaImAgMnS
v	v	k7c6
září	září	k1gNnSc6
1868	#num#	k4
zbaven	zbavit	k5eAaPmNgInS
mandátu	mandát	k1gInSc3
pro	pro	k7c4
absenci	absence	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Mandát	mandát	k1gInSc1
získal	získat	k5eAaPmAgInS
i	i	k9
v	v	k7c6
dalších	další	k2eAgFnPc6d1
zemských	zemský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1870	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zemských	zemský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1872	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rámci	rámec	k1gInSc6
tehdejší	tehdejší	k2eAgFnSc2d1
obnovené	obnovený	k2eAgFnSc2d1
pasivní	pasivní	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
mandát	mandát	k1gInSc1
přestal	přestat	k5eAaPmAgInS
vykonávat	vykonávat	k5eAaImF
a	a	k8xC
byl	být	k5eAaImAgMnS
zbaven	zbavit	k5eAaPmNgMnS
mandátu	mandát	k1gInSc2
pro	pro	k7c4
absenci	absence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následných	následný	k2eAgFnPc6d1
doplňovacích	doplňovací	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1873	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
doplňovacích	doplňovací	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1874	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
tu	tu	k6eAd1
byl	být	k5eAaImAgMnS
poslancem	poslanec	k1gMnSc7
zvolen	zvolit	k5eAaPmNgMnS
Jindřich	Jindřich	k1gMnSc1
Houra	Houra	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravec	Moravec	k1gMnSc1
se	se	k3xPyFc4
do	do	k7c2
sněmu	sněm	k1gInSc2
vrátil	vrátit	k5eAaPmAgInS
až	až	k9
v	v	k7c6
doplňovacích	doplňovací	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1875	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Následovalo	následovat	k5eAaImAgNnS
opět	opět	k6eAd1
zbavení	zbavení	k1gNnSc1
mandátu	mandát	k1gInSc2
a	a	k8xC
opětovná	opětovný	k2eAgFnSc1d1
manifestační	manifestační	k2eAgFnSc1d1
volba	volba	k1gFnSc1
v	v	k7c6
doplňovacích	doplňovací	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1876	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Aktivně	aktivně	k6eAd1
se	se	k3xPyFc4
mandátu	mandát	k1gInSc3
na	na	k7c6
sněmu	sněm	k1gInSc6
ujal	ujmout	k5eAaPmAgInS
po	po	k7c6
řádných	řádný	k2eAgFnPc6d1
zemských	zemský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1878	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Patřil	patřit	k5eAaImAgMnS
ke	k	k7c3
staročeské	staročeský	k2eAgFnSc3d1
straně	strana	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
únoru	únor	k1gInSc6
1890	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zejména	zejména	k9
v	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
dlouhodobě	dlouhodobě	k6eAd1
na	na	k7c4
práci	práce	k1gFnSc4
sněmu	sněm	k1gInSc2
fakticky	fakticky	k6eAd1
neúčastnil	účastnit	k5eNaImAgInS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnost	farnost	k1gFnSc1
Nová	Nová	k1gFnSc1
Včelnice	včelnice	k1gFnSc1
1	#num#	k4
2	#num#	k4
3	#num#	k4
LIŠKOVÁ	Lišková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc4
představitelů	představitel	k1gMnPc2
zemské	zemský	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
1861	#num#	k4
<g/>
-	-	kIx~
<g/>
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SÚA	SÚA	kA
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
379	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8085475138	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
204	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
úmrtí	úmrtí	k1gNnSc6
a	a	k8xC
pohřbu	pohřeb	k1gInSc6
farnost	farnost	k1gFnSc4
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
1	#num#	k4
2	#num#	k4
3	#num#	k4
Ignác	Ignác	k1gMnSc1
Moravec	Moravec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohlas	ohlas	k1gInSc1
od	od	k7c2
Nežárky	Nežárka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Únor	únor	k1gInSc1
1890	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
20	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
67	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.psp.cz/eknih/1867skc/1/stenprot/002schuz/s002002.htm	http://www.psp.cz/eknih/1867skc/1/stenprot/002schuz/s002002.htm	k1gMnSc1
<g/>
↑	↑	k?
http://www.psp.cz/eknih/1867_69skc/1/stenprot/001schuz/s001003.htm	http://www.psp.cz/eknih/1867_69skc/1/stenprot/001schuz/s001003.htm	k1gMnSc1
<g/>
↑	↑	k?
Osvědčení	osvědčení	k1gNnSc4
poslancův	poslancův	k2eAgInSc1d1
českých	český	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpen	srpen	k1gInSc1
1868	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
37	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.psp.cz/eknih/1867_69skc/2/stenprot/020schuz/s020006.htm	http://www.psp.cz/eknih/1867_69skc/2/stenprot/020schuz/s020006.htm	k1gMnSc1
<g/>
↑	↑	k?
http://www.psp.cz/eknih/1870skc/1/stenprot/003schuz/s003001.htm	http://www.psp.cz/eknih/1870skc/1/stenprot/003schuz/s003001.htm	k1gMnSc1
<g/>
↑	↑	k?
http://www.psp.cz/eknih/1872skc/1/stenprot/002schuz/s002002.htm	http://www.psp.cz/eknih/1872skc/1/stenprot/002schuz/s002002.htm	k1gMnSc1
<g/>
↑	↑	k?
http://www.psp.cz/eknih/1872skc/2/stenprot/003schuz/s003001.htm	http://www.psp.cz/eknih/1872skc/2/stenprot/003schuz/s003001.htm	k1gMnSc1
<g/>
↑	↑	k?
http://www.psp.cz/eknih/1872skc/3/stenprot/001schuz/s001003.htm	http://www.psp.cz/eknih/1872skc/3/stenprot/001schuz/s001003.htm	k1gMnSc1
<g/>
↑	↑	k?
http://www.psp.cz/eknih/1872skc/4/stenprot/002schuz/s002001.htm	http://www.psp.cz/eknih/1872skc/4/stenprot/002schuz/s002001.htm	k1gMnSc1
<g/>
↑	↑	k?
http://www.psp.cz/eknih/1872skc/5/stenprot/002schuz/s002001.htm	http://www.psp.cz/eknih/1872skc/5/stenprot/002schuz/s002001.htm	k1gMnSc1
<g/>
↑	↑	k?
http://www.psp.cz/eknih/1878skc/1/stenprot/002schuz/s002002.htm	http://www.psp.cz/eknih/1878skc/1/stenprot/002schuz/s002002.htm	k6eAd1
<g/>
↑	↑	k?
Národní	národní	k2eAgInPc4d1
listy	list	k1gInPc4
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1878	#num#	k4
<g/>
,	,	kIx,
http://kramerius.nkp.cz/kramerius/PShowPageDoc.do?id=5011282&	http://kramerius.nkp.cz/kramerius/PShowPageDoc.do?id=5011282&	k5eAaPmIp1nS
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
