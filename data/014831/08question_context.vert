<s>
Ignác	Ignác	k1gMnSc1
Moravec	Moravec	k1gMnSc1
<g/>
,	,	kIx,
též	též	k9
Hynek	Hynek	k1gMnSc1
Moravec	Moravec	k1gMnSc1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1824	#num#	k4
Nová	nový	k2eAgFnSc1d1
Včelnice	včelnice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
některé	některý	k3yIgInPc1
zdroje	zdroj	k1gInPc1
uvádějí	uvádět	k5eAaImIp3nP
jako	jako	k9
místo	místo	k7c2
narození	narození	k1gNnSc2
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1890	#num#	k4
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
české	český	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
z	z	k7c2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
poslanec	poslanec	k1gMnSc1
Českého	český	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
a	a	k8xC
starosta	starosta	k1gMnSc1
Jindřichova	Jindřichův	k2eAgInSc2d1
Hradce	Hradec	k1gInSc2
<g/>
.	.	kIx.
</s>