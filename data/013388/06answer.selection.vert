<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
chřipka	chřipka	k1gFnSc1	chřipka
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
chřipkové	chřipkový	k2eAgFnSc2d1	chřipková
pandemie	pandemie	k1gFnSc2	pandemie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
