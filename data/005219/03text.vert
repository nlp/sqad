<s>
Exponenciální	exponenciální	k2eAgFnSc1d1	exponenciální
funkce	funkce	k1gFnSc1	funkce
čili	čili	k8xC	čili
exponenciála	exponenciála	k1gFnSc1	exponenciála
je	být	k5eAaImIp3nS	být
matematická	matematický	k2eAgFnSc1d1	matematická
funkce	funkce	k1gFnSc1	funkce
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
kladné	kladný	k2eAgNnSc1d1	kladné
číslo	číslo	k1gNnSc1	číslo
různé	různý	k2eAgNnSc1d1	různé
od	od	k7c2	od
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
základ	základ	k1gInSc1	základ
<g/>
.	.	kIx.	.
</s>
<s>
Číslu	číslo	k1gNnSc3	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
exponent	exponent	k1gInSc1	exponent
<g/>
.	.	kIx.	.
</s>
<s>
Definičním	definiční	k2eAgInSc7d1	definiční
oborem	obor	k1gInSc7	obor
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
jsou	být	k5eAaImIp3nP	být
všechna	všechen	k3xTgNnPc1	všechen
reálná	reálný	k2eAgNnPc1d1	reálné
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
všechna	všechen	k3xTgNnPc1	všechen
komplexní	komplexní	k2eAgNnPc1d1	komplexní
čísla	číslo	k1gNnPc1	číslo
(	(	kIx(	(
<g/>
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
rozšířit	rozšířit	k5eAaPmF	rozšířit
i	i	k9	i
na	na	k7c4	na
složitější	složitý	k2eAgInPc4d2	složitější
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
lineární	lineární	k2eAgInPc1d1	lineární
operátory	operátor	k1gInPc1	operátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inverzní	inverzní	k2eAgFnSc7d1	inverzní
funkcí	funkce	k1gFnSc7	funkce
k	k	k7c3	k
exponenciále	exponenciál	k1gInSc6	exponenciál
je	být	k5eAaImIp3nS	být
logaritmus	logaritmus	k1gInSc1	logaritmus
o	o	k7c6	o
stejném	stejný	k2eAgInSc6d1	stejný
základu	základ	k1gInSc6	základ
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
x	x	k?	x
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
x.	x.	k?	x.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Derivací	derivace	k1gFnSc7	derivace
exponenciály	exponenciál	k1gInPc7	exponenciál
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
exponenciála	exponenciála	k1gFnSc1	exponenciála
vynásobená	vynásobený	k2eAgFnSc1d1	vynásobená
přirozeným	přirozený	k2eAgInSc7d1	přirozený
logaritmem	logaritmus	k1gInSc7	logaritmus
základu	základ	k1gInSc2	základ
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
ln	ln	k?	ln
:	:	kIx,	:
a	a	k8xC	a
.	.	kIx.	.
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
a.a	a.a	k?	a.a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Protože	protože	k8xS	protože
přirozený	přirozený	k2eAgInSc1d1	přirozený
logaritmus	logaritmus	k1gInSc1	logaritmus
Eulerova	Eulerův	k2eAgNnSc2d1	Eulerovo
čísla	číslo	k1gNnSc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
rovný	rovný	k2eAgMnSc1d1	rovný
jedné	jeden	k4xCgFnSc2	jeden
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
často	často	k6eAd1	často
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
základem	základ	k1gInSc7	základ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
exp	exp	kA	exp
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
exp	exp	kA	exp
x	x	k?	x
<g/>
}	}	kIx)	}
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
opět	opět	k6eAd1	opět
exponenciální	exponenciální	k2eAgFnSc4d1	exponenciální
funkci	funkce	k1gFnSc4	funkce
o	o	k7c6	o
stejném	stejný	k2eAgInSc6d1	stejný
základu	základ	k1gInSc6	základ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přináší	přinášet	k5eAaImIp3nS	přinášet
usnadnění	usnadnění	k1gNnSc4	usnadnění
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
výrazy	výraz	k1gInPc7	výraz
obsahujícími	obsahující	k2eAgInPc7d1	obsahující
exponenciálu	exponenciál	k1gInSc2	exponenciál
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
automaticky	automaticky	k6eAd1	automaticky
myslí	myslet	k5eAaImIp3nS	myslet
exponenciála	exponenciála	k1gFnSc1	exponenciála
se	s	k7c7	s
základem	základ	k1gInSc7	základ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
}	}	kIx)	}
a	a	k8xC	a
exponenciály	exponenciál	k1gInPc1	exponenciál
s	s	k7c7	s
obecným	obecný	k2eAgInSc7d1	obecný
základem	základ	k1gInSc7	základ
se	se	k3xPyFc4	se
převádějí	převádět	k5eAaImIp3nP	převádět
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
základ	základ	k1gInSc4	základ
pomocí	pomocí	k7c2	pomocí
vzorce	vzorec	k1gInSc2	vzorec
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
exp	exp	kA	exp
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
⋅	⋅	k?	⋅
ln	ln	k?	ln
:	:	kIx,	:
a	a	k8xC	a
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
exp	exp	kA	exp
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
ln	ln	k?	ln
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
notace	notace	k1gFnSc1	notace
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
exp	exp	kA	exp
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exp	expo	k1gNnPc2	expo
x	x	k?	x
<g/>
}	}	kIx)	}
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
exponent	exponent	k1gMnSc1	exponent
mohl	moct	k5eAaImAgMnS	moct
zapsat	zapsat	k5eAaPmF	zapsat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
řádek	řádek	k1gInSc4	řádek
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
výrazu	výraz	k1gInSc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Exponenciální	exponenciální	k2eAgFnSc1d1	exponenciální
funkce	funkce	k1gFnSc1	funkce
ex	ex	k6eAd1	ex
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
různými	různý	k2eAgInPc7d1	různý
ekvivalentními	ekvivalentní	k2eAgInPc7d1	ekvivalentní
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
definována	definovat	k5eAaBmNgFnS	definovat
následující	následující	k2eAgFnSc7d1	následující
mocninnou	mocninný	k2eAgFnSc7d1	mocninná
řadou	řada	k1gFnSc7	řada
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
+	+	kIx~	+
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
n	n	k0	n
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
2	[number]	k4	2
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
3	[number]	k4	3
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
4	[number]	k4	4
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
}	}	kIx)	}
:	:	kIx,	:
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
ex	ex	k6eAd1	ex
definováno	definovat	k5eAaBmNgNnS	definovat
jako	jako	k8xC	jako
řešení	řešení	k1gNnSc1	řešení
y	y	k?	y
rovnice	rovnice	k1gFnSc1	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
definovat	definovat	k5eAaBmF	definovat
také	také	k9	také
následující	následující	k2eAgFnSc7d1	následující
limitou	limita	k1gFnSc7	limita
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
infty	infta	k1gMnSc2	infta
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
exponenciální	exponenciální	k2eAgFnSc4d1	exponenciální
funkci	funkce	k1gFnSc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
)	)	kIx)	)
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
zdola	zdola	k6eAd1	zdola
omezená	omezený	k2eAgFnSc1d1	omezená
je	být	k5eAaImIp3nS	být
prostá	prostý	k2eAgFnSc1d1	prostá
je	být	k5eAaImIp3nS	být
spojitá	spojitý	k2eAgFnSc1d1	spojitá
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
spojitá	spojitý	k2eAgFnSc1d1	spojitá
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
pro	pro	k7c4	pro
a	a	k8xC	a
>	>	kIx)	>
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
a	a	k8xC	a
∈	∈	k?	∈
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
)	)	kIx)	)
klesající	klesající	k2eAgFnPc1d1	klesající
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
graf	graf	k1gInSc1	graf
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
prochází	procházet	k5eAaImIp3nS	procházet
bodem	bod	k1gInSc7	bod
[	[	kIx(	[
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
případě	případ	k1gInSc6	případ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
exp	exp	kA	exp
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
exp	exp	kA	exp
:	:	kIx,	:
x	x	k?	x
)	)	kIx)	)
.	.	kIx.	.
(	(	kIx(	(
exp	exp	kA	exp
:	:	kIx,	:
y	y	k?	y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exp	exp	kA	exp
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exp	exp	kA	exp
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exp	exp	kA	exp
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
používaný	používaný	k2eAgInSc1d1	používaný
základ	základ	k1gInSc1	základ
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
až	až	k9	až
na	na	k7c4	na
násobek	násobek	k1gInSc4	násobek
jediné	jediný	k2eAgNnSc1d1	jediné
řešení	řešení	k1gNnSc1	řešení
diferenciální	diferenciální	k2eAgFnSc2d1	diferenciální
rovnice	rovnice	k1gFnSc2	rovnice
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
y	y	k?	y
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
y.	y.	k?	y.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
definuje	definovat	k5eAaBmIp3nS	definovat
mocninnou	mocninný	k2eAgFnSc7d1	mocninná
řadou	řada	k1gFnSc7	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
j	j	k?	j
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
která	který	k3yQgFnSc1	který
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
reálné	reálný	k2eAgNnSc4d1	reálné
i	i	k8xC	i
komplexní	komplexní	k2eAgNnSc4d1	komplexní
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
exponenciální	exponenciální	k2eAgFnSc1d1	exponenciální
funkce	funkce	k1gFnSc1	funkce
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dá	dát	k5eAaPmIp3nS	dát
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
ln	ln	k?	ln
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc1d1	přirozený
logaritmus	logaritmus	k1gInSc1	logaritmus
čísla	číslo	k1gNnSc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Dosazením	dosazení	k1gNnSc7	dosazení
čistě	čistě	k6eAd1	čistě
imaginárního	imaginární	k2eAgNnSc2d1	imaginární
čísla	číslo	k1gNnSc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
ix	ix	k?	ix
<g/>
}	}	kIx)	}
do	do	k7c2	do
definice	definice	k1gFnSc2	definice
exponenciály	exponenciál	k1gInPc4	exponenciál
dostáváme	dostávat	k5eAaImIp1nP	dostávat
vztah	vztah	k1gInSc4	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
i	i	k9	i
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
cos	cos	kA	cos
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
i	i	k8xC	i
sin	sin	kA	sin
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
ix	ix	k?	ix
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
x	x	k?	x
<g/>
}	}	kIx)	}
:	:	kIx,	:
Důležitost	důležitost	k1gFnSc1	důležitost
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc6d1	přírodní
vědách	věda	k1gFnPc6	věda
pramení	pramenit	k5eAaImIp3nS	pramenit
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
její	její	k3xOp3gFnSc2	její
derivace	derivace	k1gFnSc2	derivace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
+	+	kIx~	+
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
+	+	kIx~	+
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
+	+	kIx~	+
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gMnSc1	begin
<g/>
{	{	kIx(	{
<g/>
aligned	aligned	k1gMnSc1	aligned
<g/>
}	}	kIx)	}
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
\\	\\	k?	\\
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\\	\\	k?	\\
<g/>
&	&	k?	&
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
\\	\\	k?	\\
<g/>
&	&	k?	&
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
\\	\\	k?	\\
<g/>
&	&	k?	&
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\\\	\\\	k?	\\\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
aligned	aligned	k1gMnSc1	aligned
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ex	ex	k6eAd1	ex
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
derivací	derivace	k1gFnSc7	derivace
a	a	k8xC	a
tedy	tedy	k9	tedy
je	být	k5eAaImIp3nS	být
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
příkladem	příklad	k1gInSc7	příklad
pfaffovské	pfaffovský	k2eAgFnSc2d1	pfaffovský
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
tvaru	tvar	k1gInSc2	tvar
cex	cex	k?	cex
(	(	kIx(	(
<g/>
kde	kde	k9	kde
c	c	k0	c
je	být	k5eAaImIp3nS	být
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jediné	jediný	k2eAgFnPc4d1	jediná
funkce	funkce	k1gFnPc4	funkce
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
vlastností	vlastnost	k1gFnSc7	vlastnost
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Picardovy	Picardův	k2eAgFnSc2d1	Picardova
<g/>
–	–	k?	–
<g/>
Lindelöfovy	Lindelöfův	k2eAgFnSc2d1	Lindelöfův
věty	věta	k1gFnSc2	věta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
i	i	k9	i
následujícími	následující	k2eAgInPc7d1	následující
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
Sklon	sklon	k1gInSc1	sklon
grafu	graf	k1gInSc2	graf
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
bodě	bod	k1gInSc6	bod
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
růstu	růst	k1gInSc2	růst
funkce	funkce	k1gFnSc2	funkce
x	x	k?	x
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k8xC	jako
hodnota	hodnota	k1gFnSc1	hodnota
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
x.	x.	k?	x.
Funkce	funkce	k1gFnSc2	funkce
řeší	řešit	k5eAaImIp3nP	řešit
diferenciální	diferenciální	k2eAgFnSc4d1	diferenciální
rovnici	rovnice	k1gFnSc4	rovnice
y	y	k?	y
<g/>
'	'	kIx"	'
=	=	kIx~	=
y.	y.	k?	y.
exp	exp	kA	exp
je	být	k5eAaImIp3nS	být
pevný	pevný	k2eAgInSc4d1	pevný
bod	bod	k1gInSc4	bod
derivace	derivace	k1gFnSc2	derivace
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
u	u	k7c2	u
diferencovatelné	diferencovatelný	k2eAgFnSc2d1	diferencovatelná
funkce	funkce	k1gFnSc2	funkce
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
používáme	používat	k5eAaImIp1nP	používat
řetízkové	řetízkový	k2eAgNnSc4d1	řetízkové
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
e	e	k0	e
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Exponenciální	exponenciální	k2eAgFnSc1d1	exponenciální
a	a	k8xC	a
logaritmická	logaritmický	k2eAgFnSc1d1	logaritmická
funkce	funkce	k1gFnSc1	funkce
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
inverzní	inverzní	k2eAgFnPc1d1	inverzní
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
f	f	k?	f
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
:	:	kIx,	:
<g/>
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
l	l	kA	l
o	o	k7c4	o
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
:	:	kIx,	:
<g/>
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
log_	log_	k?	log_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Grafy	graf	k1gInPc1	graf
těchto	tento	k3xDgFnPc2	tento
funkcí	funkce	k1gFnPc2	funkce
jsou	být	k5eAaImIp3nP	být
osově	osově	k6eAd1	osově
souměrné	souměrný	k2eAgFnPc1d1	souměrná
podle	podle	k7c2	podle
přímky	přímka	k1gFnSc2	přímka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
exp	exp	kA	exp
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exp	expo	k1gNnPc2	expo
x	x	k?	x
<g/>
}	}	kIx)	}
:	:	kIx,	:
Řetězový	řetězový	k2eAgInSc1d1	řetězový
zlomek	zlomek	k1gInSc1	zlomek
<g />
.	.	kIx.	.
</s>
<s hack="1">
pro	pro	k7c4	pro
ex	ex	k6eAd1	ex
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Eulerovy	Eulerův	k2eAgFnSc2d1	Eulerova
rovnosti	rovnost	k1gFnSc2	rovnost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
2	[number]	k4	2
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
3	[number]	k4	3
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
4	[number]	k4	4
−	−	k?	−
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gFnSc1	cfrac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gFnSc1	cfrac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k6eAd1	ddots
}}}}}}}}}	}}}}}}}}}	k?	}}}}}}}}}
:	:	kIx,	:
Následující	následující	k2eAgInSc1d1	následující
celkový	celkový	k2eAgInSc1d1	celkový
řetězový	řetězový	k2eAgInSc1d1	řetězový
zlomek	zlomek	k1gInSc1	zlomek
pro	pro	k7c4	pro
ez	ez	k?	ez
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
2	[number]	k4	2
−	−	k?	−
z	z	k7c2	z
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
10	[number]	k4	10
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
+	+	kIx~	+
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
-z	-z	k?	-z
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k6eAd1	cfrac
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k6eAd1	cfrac
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k6eAd1	cfrac
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k6eAd1	ddots
}}}}}}}}}	}}}}}}}}}	k?	}}}}}}}}}
:	:	kIx,	:
nebo	nebo	k8xC	nebo
použitím	použití	k1gNnSc7	použití
substituce	substituce	k1gFnSc2	substituce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
y	y	k?	y
−	−	k?	−
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
y	y	k?	y
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
10	[number]	k4	10
y	y	k?	y
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
y	y	k?	y
+	+	kIx~	+
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
y-x	y	k?	y-x
<g />
.	.	kIx.	.
</s>
<s>
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gFnSc1	cfrac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
y	y	k?	y
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gFnSc1	cfrac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
y	y	k?	y
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gFnSc1	cfrac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
y	y	k?	y
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k6eAd1	ddots
}}}}}}}}}	}}}}}}}}}	k?	}}}}}}}}}
:	:	kIx,	:
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
případ	případ	k1gInSc1	případ
pro	pro	k7c4	pro
z	z	k7c2	z
=	=	kIx~	=
2	[number]	k4	2
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
10	[number]	k4	10
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
+	+	kIx~	+
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
7	[number]	k4	7
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
9	[number]	k4	9
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
11	[number]	k4	11
+	+	kIx~	+
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k1gInSc1	ddots
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}}}}}}}}	}}}}}}}}	k?	}}}}}}}}
<g/>
=	=	kIx~	=
<g/>
7	[number]	k4	7
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
7	[number]	k4	7
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
9	[number]	k4	9
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
11	[number]	k4	11
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k1gInSc1	ddots
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}}}}}}}}}	}}}}}}}}}	k?	}}}}}}}}}
:	:	kIx,	:
Tento	tento	k3xDgInSc1	tento
vzorec	vzorec	k1gInSc1	vzorec
také	také	k9	také
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomaleji	pomale	k6eAd2	pomale
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
>	>	kIx)	>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
10	[number]	k4	10
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
+	+	kIx~	+
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
13	[number]	k4	13
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
54	[number]	k4	54
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
9	[number]	k4	9
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
9	[number]	k4	9
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
18	[number]	k4	18
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
9	[number]	k4	9
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
22	[number]	k4	22
+	+	kIx~	+
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k1gInSc1	ddots
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}}}}}}}}	}}}}}}}}	k?	}}}}}}}}
<g/>
=	=	kIx~	=
<g/>
13	[number]	k4	13
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
54	[number]	k4	54
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
9	[number]	k4	9
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
9	[number]	k4	9
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
18	[number]	k4	18
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
9	[number]	k4	9
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
22	[number]	k4	22
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k1gInSc1	ddots
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}}}}}}}}}	}}}}}}}}}	k?	}}}}}}}}}
:	:	kIx,	:
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Maticová	maticový	k2eAgFnSc1d1	maticová
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Mocninná	mocninný	k2eAgFnSc1d1	mocninná
řada	řada	k1gFnSc1	řada
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
exponenciály	exponenciál	k1gInPc1	exponenciál
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
definovat	definovat	k5eAaBmF	definovat
exponenciálu	exponenciál	k1gInSc3	exponenciál
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
komplikovanějších	komplikovaný	k2eAgInPc2d2	komplikovanější
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
komplexní	komplexní	k2eAgNnPc4d1	komplexní
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
matic	matice	k1gFnPc2	matice
a	a	k8xC	a
operátorů	operátor	k1gInPc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Mocniny	mocnina	k1gFnPc1	mocnina
a	a	k8xC	a
součty	součet	k1gInPc1	součet
operátorů	operátor	k1gMnPc2	operátor
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
matic	matice	k1gFnPc2	matice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
definované	definovaný	k2eAgFnPc1d1	definovaná
a	a	k8xC	a
příslušná	příslušný	k2eAgFnSc1d1	příslušná
řada	řada	k1gFnSc1	řada
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
exponenciální	exponenciální	k2eAgFnPc4d1	exponenciální
funkce	funkce	k1gFnPc4	funkce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
definována	definovat	k5eAaBmNgFnS	definovat
na	na	k7c6	na
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
několika	několik	k4yIc2	několik
ekvivalentními	ekvivalentní	k2eAgFnPc7d1	ekvivalentní
formami	forma	k1gFnPc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
taková	takový	k3xDgFnSc1	takový
definice	definice	k1gFnSc1	definice
je	být	k5eAaImIp3nS	být
ekvivalentní	ekvivalentní	k2eAgFnSc7d1	ekvivalentní
definicí	definice	k1gFnSc7	definice
mocninnou	mocninný	k2eAgFnSc7d1	mocninná
řadou	řada	k1gFnSc7	řada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
reálná	reálný	k2eAgFnSc1d1	reálná
proměnná	proměnná	k1gFnSc1	proměnná
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
komplexní	komplexní	k2eAgFnSc7d1	komplexní
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Exponenciální	exponenciální	k2eAgFnSc1d1	exponenciální
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
periodická	periodický	k2eAgFnSc1d1	periodická
s	s	k7c7	s
imaginární	imaginární	k2eAgFnSc7d1	imaginární
periodou	perioda	k1gFnSc7	perioda
2	[number]	k4	2
<g/>
π	π	k?	π
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
psána	psát	k5eAaImNgFnS	psát
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
cos	cos	kA	cos
:	:	kIx,	:
b	b	k?	b
+	+	kIx~	+
i	i	k8xC	i
sin	sin	kA	sin
:	:	kIx,	:
b	b	k?	b
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
cis	cis	k1gNnSc1	cis
:	:	kIx,	:
(	(	kIx(	(
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
bi	bi	k?	bi
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
b	b	k?	b
<g/>
+	+	kIx~	+
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s>
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
cis	cis	k1gNnSc1	cis
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
a	a	k8xC	a
a	a	k8xC	a
b	b	k?	b
jsou	být	k5eAaImIp3nP	být
reálné	reálný	k2eAgFnPc4d1	reálná
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
kde	kde	k6eAd1	kde
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
reálné	reálný	k2eAgFnPc1d1	reálná
funkce	funkce	k1gFnPc1	funkce
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
předpis	předpis	k1gInSc1	předpis
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xC	jako
definice	definice	k1gFnSc1	definice
(	(	kIx(	(
<g/>
Eulerův	Eulerův	k2eAgInSc1d1	Eulerův
vzorec	vzorec	k1gInSc1	vzorec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vzorec	vzorec	k1gInSc1	vzorec
propojuje	propojovat	k5eAaImIp3nS	propojovat
exponenciální	exponenciální	k2eAgFnSc4d1	exponenciální
funkci	funkce	k1gFnSc4	funkce
s	s	k7c7	s
goniometrickými	goniometrický	k2eAgFnPc7d1	goniometrická
a	a	k8xC	a
hyperbolickými	hyperbolický	k2eAgFnPc7d1	hyperbolická
funkcemi	funkce	k1gFnPc7	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
<g/>
-li	i	k?	-li
funkci	funkce	k1gFnSc4	funkce
definovanou	definovaný	k2eAgFnSc7d1	definovaná
na	na	k7c6	na
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
exponenciální	exponenciální	k2eAgFnSc1d1	exponenciální
funkce	funkce	k1gFnSc1	funkce
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
tyto	tento	k3xDgFnPc4	tento
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
z	z	k7c2	z
+	+	kIx~	+
w	w	k?	w
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
w	w	k?	w
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
+	+	kIx~	+
<g/>
w	w	k?	w
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
z	z	k7c2	z
<g/>
}}	}}	k?	}}
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
n	n	k0	n
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
n	n	k0	n
∈	∈	k?	∈
:	:	kIx,	:
Z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
nz	nz	k?	nz
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
z	z	k7c2	z
a	a	k8xC	a
w.	w.	k?	w.
Exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
funkce	funkce	k1gFnSc1	funkce
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
holomorfní	holomorfní	k2eAgNnSc1d1	holomorfní
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc4	každý
komplexní	komplexní	k2eAgNnSc4d1	komplexní
číslo	číslo	k1gNnSc4	číslo
kromě	kromě	k7c2	kromě
0	[number]	k4	0
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
exponenciální	exponenciální	k2eAgFnSc6d1	exponenciální
funkci	funkce	k1gFnSc6	funkce
svůj	svůj	k3xOyFgInSc4	svůj
vzor	vzor	k1gInSc4	vzor
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
lacunární	lacunární	k2eAgFnSc1d1	lacunární
hodnota	hodnota	k1gFnSc1	hodnota
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
tedy	tedy	k9	tedy
malou	malý	k2eAgFnSc4d1	malá
Picardovu	Picardův	k2eAgFnSc4d1	Picardova
větu	věta	k1gFnSc4	věta
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
nekonstantní	konstantní	k2eNgFnSc1d1	nekonstantní
celá	celý	k2eAgFnSc1d1	celá
funkce	funkce	k1gFnSc1	funkce
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
obrazu	obraz	k1gInSc6	obraz
až	až	k9	až
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
celou	celý	k2eAgFnSc4d1	celá
komplexní	komplexní	k2eAgFnSc4d1	komplexní
rovinu	rovina	k1gFnSc4	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířením	rozšíření	k1gNnSc7	rozšíření
přirozeného	přirozený	k2eAgInSc2d1	přirozený
logaritmu	logaritmus	k1gInSc2	logaritmus
do	do	k7c2	do
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
získáme	získat	k5eAaPmIp1nP	získat
komplexní	komplexní	k2eAgInSc4d1	komplexní
logaritmus	logaritmus	k1gInSc4	logaritmus
log	log	kA	log
z	z	k7c2	z
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
vícehodnotová	vícehodnotový	k2eAgFnSc1d1	vícehodnotová
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
definovat	definovat	k5eAaBmF	definovat
obecnou	obecný	k2eAgFnSc4d1	obecná
mocninu	mocnina	k1gFnSc4	mocnina
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
w	w	k?	w
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
w	w	k?	w
log	log	kA	log
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
\	\	kIx~	\
<g/>
log	log	k1gInSc1	log
z	z	k7c2	z
<g/>
}}	}}	k?	}}
:	:	kIx,	:
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
komplexní	komplexní	k2eAgNnPc4d1	komplexní
čísla	číslo	k1gNnPc4	číslo
z	z	k7c2	z
a	a	k8xC	a
w.	w.	k?	w.
To	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
vícehodnotová	vícehodnotový	k2eAgFnSc1d1	vícehodnotová
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
když	když	k8xS	když
z	z	k7c2	z
je	být	k5eAaImIp3nS	být
reálné	reálný	k2eAgNnSc1d1	reálné
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
problematický	problematický	k2eAgInSc1d1	problematický
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vícehodnotové	vícehodnotový	k2eAgFnPc4d1	vícehodnotová
funkce	funkce	k1gFnPc4	funkce
log	log	kA	log
z	z	k7c2	z
a	a	k8xC	a
zw	zw	k?	zw
lze	lze	k6eAd1	lze
lehce	lehko	k6eAd1	lehko
zaměnit	zaměnit	k5eAaPmF	zaměnit
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
jednohodnotovými	jednohodnotový	k2eAgInPc7d1	jednohodnotový
ekvivalenty	ekvivalent	k1gInPc7	ekvivalent
dosazením	dosazení	k1gNnSc7	dosazení
reálného	reálný	k2eAgNnSc2d1	reálné
čísla	číslo	k1gNnSc2	číslo
za	za	k7c4	za
z.	z.	k?	z.
Pravidlo	pravidlo	k1gNnSc4	pravidlo
o	o	k7c4	o
násobení	násobení	k1gNnSc4	násobení
exponentů	exponent	k1gInPc2	exponent
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pozitivních	pozitivní	k2eAgNnPc2d1	pozitivní
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
upraveno	upravit	k5eAaPmNgNnS	upravit
pro	pro	k7c4	pro
vícehodnotový	vícehodnotový	k2eAgInSc4d1	vícehodnotový
kontext	kontext	k1gInSc4	kontext
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
ez	ez	k?	ez
<g/>
)	)	kIx)	)
w	w	k?	w
≠	≠	k?	≠
<g />
.	.	kIx.	.
</s>
<s>
ezw	ezw	k?	ezw
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
(	(	kIx(	(
<g/>
ez	ez	k?	ez
<g/>
)	)	kIx)	)
w	w	k?	w
=	=	kIx~	=
e	e	k0	e
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
+	+	kIx~	+
2	[number]	k4	2
<g/>
π	π	k?	π
<g/>
)	)	kIx)	)
<g/>
w	w	k?	w
vícehodnotový	vícehodnotový	k2eAgMnSc1d1	vícehodnotový
po	po	k7c6	po
celých	celý	k2eAgNnPc6d1	celé
číslech	číslo	k1gNnPc6	číslo
n.	n.	k?	n.
Exponenciální	exponenciální	k2eAgFnSc1d1	exponenciální
funkce	funkce	k1gFnSc1	funkce
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
každou	každý	k3xTgFnSc4	každý
přímku	přímka	k1gFnSc4	přímka
v	v	k7c6	v
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
na	na	k7c4	na
logaritmické	logaritmický	k2eAgFnPc4d1	logaritmická
spirály	spirála	k1gFnPc4	spirála
v	v	k7c6	v
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
případy	případ	k1gInPc1	případ
<g/>
:	:	kIx,	:
když	když	k8xS	když
původní	původní	k2eAgFnSc1d1	původní
přímka	přímka	k1gFnSc1	přímka
je	být	k5eAaImIp3nS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
reálnou	reálný	k2eAgFnSc7d1	reálná
osou	osa	k1gFnSc7	osa
<g/>
,	,	kIx,	,
výsledná	výsledný	k2eAgFnSc1d1	výsledná
spirála	spirála	k1gFnSc1	spirála
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neuzavírá	uzavírat	k5eNaImIp3nS	uzavírat
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
;	;	kIx,	;
a	a	k8xC	a
když	když	k8xS	když
původní	původní	k2eAgFnSc1d1	původní
přímka	přímka	k1gFnSc1	přímka
je	být	k5eAaImIp3nS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
imaginární	imaginární	k2eAgFnSc7d1	imaginární
osou	osa	k1gFnSc7	osa
<g/>
,	,	kIx,	,
výsledná	výsledný	k2eAgFnSc1d1	výsledná
spirála	spirála	k1gFnSc1	spirála
je	být	k5eAaImIp3nS	být
kruh	kruh	k1gInSc4	kruh
o	o	k7c6	o
nějakém	nějaký	k3yIgInSc6	nějaký
poloměru	poloměr	k1gInSc6	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
Exponenciální	exponenciální	k2eAgFnSc1d1	exponenciální
funkce	funkce	k1gFnSc1	funkce
na	na	k7c6	na
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
Umocňování	umocňování	k1gNnSc2	umocňování
</s>
