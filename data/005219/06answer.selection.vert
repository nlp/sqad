<s>
Často	často	k6eAd1	často
používaný	používaný	k2eAgInSc1d1	používaný
základ	základ	k1gInSc1	základ
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
