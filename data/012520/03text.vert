<p>
<s>
Slowfox	slowfox	k1gInSc1	slowfox
(	(	kIx(	(
<g/>
též	též	k9	též
Slow	Slow	k1gMnSc1	Slow
Foxtrot	foxtrot	k1gInSc1	foxtrot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
nedbale	dbale	k6eNd1	dbale
elegantní	elegantní	k2eAgInSc4d1	elegantní
<g/>
"	"	kIx"	"
standardní	standardní	k2eAgInSc4d1	standardní
tanec	tanec	k1gInSc4	tanec
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
–	–	k?	–
nejtypičtější	typický	k2eAgInSc4d3	nejtypičtější
tanec	tanec	k1gInSc4	tanec
anglického	anglický	k2eAgInSc2d1	anglický
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
postupový	postupový	k2eAgInSc4d1	postupový
tanec	tanec	k1gInSc4	tanec
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
taneční	taneční	k2eAgInSc1d1	taneční
pár	pár	k1gInSc1	pár
snaží	snažit	k5eAaImIp3nS	snažit
utančit	utančit	k5eAaPmF	utančit
co	co	k9	co
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
dráhu	dráha	k1gFnSc4	dráha
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
opakovaných	opakovaný	k2eAgInPc2d1	opakovaný
obratů	obrat	k1gInPc2	obrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tančí	tančit	k5eAaImIp3nS	tančit
se	se	k3xPyFc4	se
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
doby	doba	k1gFnPc4	doba
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
taktů	takt	k1gInPc2	takt
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
plynulého	plynulý	k2eAgInSc2d1	plynulý
kyvadlového	kyvadlový	k2eAgInSc2d1	kyvadlový
švihového	švihový	k2eAgInSc2d1	švihový
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Tančí	tančit	k5eAaImIp3nS	tančit
se	se	k3xPyFc4	se
na	na	k7c6	na
soutěžích	soutěž	k1gFnPc6	soutěž
ve	v	k7c6	v
společenských	společenský	k2eAgInPc6d1	společenský
tancích	tanec	k1gInPc6	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gFnSc2	jeho
obtížnosti	obtížnost	k1gFnSc2	obtížnost
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
až	až	k9	až
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
úrovních	úroveň	k1gFnPc6	úroveň
tanečních	taneční	k2eAgInPc2d1	taneční
kurzů	kurz	k1gInPc2	kurz
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
ani	ani	k8xC	ani
ne	ne	k9	ne
v	v	k7c6	v
pokračovacích	pokračovací	k2eAgFnPc6d1	pokračovací
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k6eAd1	až
speciálních	speciální	k2eAgInPc6d1	speciální
a	a	k8xC	a
vyšších	vysoký	k2eAgInPc6d2	vyšší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taneční	taneční	k2eAgNnSc4d1	taneční
schéma	schéma	k1gNnSc4	schéma
==	==	k?	==
</s>
</p>
<p>
<s>
Slowfox	slowfox	k1gInSc1	slowfox
<g/>
,	,	kIx,	,
pokládaný	pokládaný	k2eAgInSc1d1	pokládaný
za	za	k7c4	za
nejtěžší	těžký	k2eAgInSc4d3	nejtěžší
tanec	tanec	k1gInSc4	tanec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
základní	základní	k2eAgNnSc1d1	základní
taneční	taneční	k2eAgNnSc1d1	taneční
schéma	schéma	k1gNnSc1	schéma
-	-	kIx~	-
spojení	spojení	k1gNnSc1	spojení
Pérového	pérový	k2eAgInSc2d1	pérový
kroku	krok	k1gInSc2	krok
a	a	k8xC	a
Trojkroku	trojkrok	k1gInSc2	trojkrok
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
figur	figura	k1gFnPc2	figura
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
spojení	spojení	k1gNnSc1	spojení
dohromady	dohromady	k6eAd1	dohromady
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
šest	šest	k4xCc1	šest
kroků	krok	k1gInPc2	krok
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
muž	muž	k1gMnSc1	muž
tančí	tančit	k5eAaImIp3nP	tančit
vpřed	vpřed	k6eAd1	vpřed
a	a	k8xC	a
dáma	dáma	k1gFnSc1	dáma
vzad	vzad	k6eAd1	vzad
<g/>
,	,	kIx,	,
kroky	krok	k1gInPc1	krok
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rytmizaci	rytmizace	k1gFnSc6	rytmizace
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
rychle	rychle	k6eAd1	rychle
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
slow	slow	k?	slow
slow	slow	k?	slow
quick	quick	k1gMnSc1	quick
quick	quick	k1gMnSc1	quick
<g/>
,	,	kIx,	,
slow	slow	k?	slow
slow	slow	k?	slow
quick	quick	k1gMnSc1	quick
quick	quick	k1gMnSc1	quick
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Landsfeld	Landsfeld	k1gMnSc1	Landsfeld
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Plamínek	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
Technika	technika	k1gFnSc1	technika
standardních	standardní	k2eAgInPc2d1	standardní
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
</s>
</p>
