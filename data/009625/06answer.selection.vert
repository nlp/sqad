<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
rozsáhlejší	rozsáhlý	k2eAgNnSc4d2	rozsáhlejší
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
výměrou	výměra	k1gFnSc7	výměra
nad	nad	k7c4	nad
1	[number]	k4	1
000	[number]	k4	000
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
ekosystémy	ekosystém	k1gInPc7	ekosystém
podstatně	podstatně	k6eAd1	podstatně
nezměněnými	změněný	k2eNgInPc7d1	nezměněný
lidskou	lidský	k2eAgFnSc4d1	lidská
činností	činnost	k1gFnSc7	činnost
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jedinečné	jedinečný	k2eAgFnSc6d1	jedinečná
a	a	k8xC	a
přirozené	přirozený	k2eAgFnSc6d1	přirozená
krajinné	krajinný	k2eAgFnSc6d1	krajinná
struktuře	struktura	k1gFnSc6	struktura
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
nadřazená	nadřazený	k2eAgFnSc1d1	nadřazená
nad	nad	k7c4	nad
ostatní	ostatní	k1gNnSc4	ostatní
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
