<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
rozsáhlejší	rozsáhlý	k2eAgNnSc4d2	rozsáhlejší
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
výměrou	výměra	k1gFnSc7	výměra
nad	nad	k7c4	nad
1	[number]	k4	1
000	[number]	k4	000
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
ekosystémy	ekosystém	k1gInPc7	ekosystém
podstatně	podstatně	k6eAd1	podstatně
nezměněnými	změněný	k2eNgInPc7d1	nezměněný
lidskou	lidský	k2eAgFnSc4d1	lidská
činností	činnost	k1gFnSc7	činnost
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jedinečné	jedinečný	k2eAgFnSc6d1	jedinečná
a	a	k8xC	a
přirozené	přirozený	k2eAgFnSc6d1	přirozená
krajinné	krajinný	k2eAgFnSc6d1	krajinná
struktuře	struktura	k1gFnSc6	struktura
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
nadřazená	nadřazený	k2eAgFnSc1d1	nadřazená
nad	nad	k7c4	nad
ostatní	ostatní	k1gNnSc4	ostatní
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
zřídit	zřídit	k5eAaPmF	zřídit
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
zazněla	zaznít	k5eAaPmAgFnS	zaznít
poprvé	poprvé	k6eAd1	poprvé
začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
anglický	anglický	k2eAgMnSc1d1	anglický
básník	básník	k1gMnSc1	básník
William	William	k1gInSc4	William
Wordsworth	Wordsworth	k1gMnSc1	Wordsworth
popsal	popsat	k5eAaPmAgMnS	popsat
Jezerní	jezerní	k2eAgFnSc4d1	jezerní
oblast	oblast	k1gFnSc4	oblast
(	(	kIx(	(
<g/>
Lake	Lake	k1gFnSc1	Lake
District	District	k1gMnSc1	District
<g/>
)	)	kIx)	)
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Anglii	Anglie	k1gFnSc6	Anglie
jako	jako	k9	jako
"	"	kIx"	"
<g/>
druh	druh	k1gInSc1	druh
národního	národní	k2eAgInSc2d1	národní
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
má	mít	k5eAaImIp3nS	mít
majetkovou	majetkový	k2eAgFnSc4d1	majetková
účast	účast	k1gFnSc4	účast
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
má	mít	k5eAaImIp3nS	mít
oči	oko	k1gNnPc4	oko
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
srdce	srdce	k1gNnSc4	srdce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
radoval	radovat	k5eAaImAgMnS	radovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Malíře	malíř	k1gMnSc2	malíř
Georga	Georg	k1gMnSc2	Georg
Catlina	Catlin	k2eAgMnSc2d1	Catlin
během	během	k7c2	během
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
divokém	divoký	k2eAgInSc6d1	divoký
západě	západ	k1gInSc6	západ
trápila	trápit	k5eAaImAgFnS	trápit
budoucnost	budoucnost	k1gFnSc1	budoucnost
Indiánů	Indián	k1gMnPc2	Indián
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
potkal	potkat	k5eAaPmAgMnS	potkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc2d1	přírodní
krás	krása	k1gFnPc2	krása
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
viděl	vidět	k5eAaImAgInS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
je	on	k3xPp3gNnSc4	on
možná	možná	k9	možná
mohl	moct	k5eAaImAgMnS	moct
uchovat	uchovat	k5eAaPmF	uchovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nějaký	nějaký	k3yIgInSc4	nějaký
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
státní	státní	k2eAgInSc4d1	státní
ochranný	ochranný	k2eAgInSc4d1	ochranný
program	program	k1gInSc4	program
...	...	k?	...
ve	v	k7c6	v
velkolepém	velkolepý	k2eAgInSc6d1	velkolepý
parku	park	k1gInSc6	park
...	...	k?	...
Parku	park	k1gInSc6	park
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
bude	být	k5eAaImBp3nS	být
člověk	člověk	k1gMnSc1	člověk
i	i	k9	i
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
divokosti	divokost	k1gFnSc6	divokost
a	a	k8xC	a
nedoknutelnosti	nedoknutelnost	k1gFnSc6	nedoknutelnost
jejich	jejich	k3xOp3gFnSc2	jejich
přirozené	přirozený	k2eAgFnSc2d1	přirozená
krásy	krása	k1gFnSc2	krása
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
pokusu	pokus	k1gInSc3	pokus
vlády	vláda	k1gFnSc2	vláda
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
takto	takto	k6eAd1	takto
chráněného	chráněný	k2eAgNnSc2d1	chráněné
území	území	k1gNnSc2	území
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
když	když	k8xS	když
prezident	prezident	k1gMnSc1	prezident
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1864	[number]	k4	1864
podepsal	podepsat	k5eAaPmAgInS	podepsat
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
chrání	chránit	k5eAaImIp3nS	chránit
Yosemitské	Yosemitský	k2eAgNnSc1d1	Yosemitské
údolí	údolí	k1gNnSc1	údolí
a	a	k8xC	a
Mariposký	Mariposký	k2eAgInSc1d1	Mariposký
les	les	k1gInSc1	les
(	(	kIx(	(
<g/>
Mariposa	Mariposa	k1gFnSc1	Mariposa
Grove	Groev	k1gFnSc2	Groev
<g/>
)	)	kIx)	)
sekvojovce	sekvojovec	k1gMnSc2	sekvojovec
obrovského	obrovský	k2eAgMnSc2d1	obrovský
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
budoucího	budoucí	k2eAgInSc2d1	budoucí
světoznámého	světoznámý	k2eAgInSc2d1	světoznámý
Yosemitského	Yosemitský	k2eAgInSc2d1	Yosemitský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
)	)	kIx)	)
státu	stát	k1gInSc2	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Výše	vysoce	k6eAd2	vysoce
uvedený	uvedený	k2eAgInSc1d1	uvedený
stát	stát	k1gInSc1	stát
přijme	přijmout	k5eAaPmIp3nS	přijmout
tento	tento	k3xDgInSc4	tento
sdělovaný	sdělovaný	k2eAgInSc4d1	sdělovaný
majetek	majetek	k1gInSc4	majetek
za	za	k7c2	za
výslovných	výslovný	k2eAgFnPc2d1	výslovná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
areál	areál	k1gInSc1	areál
bude	být	k5eAaImBp3nS	být
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgNnSc4d1	veřejné
použití	použití	k1gNnSc4	použití
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
výletní	výletní	k2eAgNnSc4d1	výletní
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
na	na	k7c4	na
rekreaci	rekreace	k1gFnSc4	rekreace
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
navěky	navěky	k6eAd1	navěky
nezcizitelným	zcizitelný	k2eNgInPc3d1	nezcizitelný
<g/>
.	.	kIx.	.
<g/>
Ale	ale	k9	ale
vize	vize	k1gFnSc1	vize
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
v	v	k7c6	v
Yosemite	Yosemit	k1gInSc5	Yosemit
ještě	ještě	k9	ještě
nebyla	být	k5eNaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
úsilí	úsilí	k1gNnSc1	úsilí
Johna	John	k1gMnSc2	John
Muira	Muir	k1gMnSc2	Muir
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přinesla	přinést	k5eAaPmAgFnS	přinést
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Yosemite	Yosemit	k1gInSc5	Yosemit
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Yellowstonský	Yellowstonský	k2eAgInSc1d1	Yellowstonský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
skutečný	skutečný	k2eAgInSc4d1	skutečný
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Následujíc	následovat	k5eAaImSgFnS	následovat
yellowstonský	yellowstonský	k2eAgInSc4d1	yellowstonský
příklad	příklad	k1gInSc4	příklad
postupně	postupně	k6eAd1	postupně
vznikaly	vznikat	k5eAaImAgInP	vznikat
jiné	jiný	k2eAgInPc1d1	jiný
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Sydney	Sydney	k1gNnSc2	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Banff	Banff	k1gInSc1	Banff
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Rocky	rock	k1gInPc4	rock
Mountain	Mountain	k2eAgMnSc1d1	Mountain
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgInSc7	první
kanadským	kanadský	k2eAgInSc7d1	kanadský
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
měl	mít	k5eAaImAgInS	mít
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
se	se	k3xPyFc4	se
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
zakládaly	zakládat	k5eAaImAgFnP	zakládat
zejména	zejména	k9	zejména
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zakládají	zakládat	k5eAaImIp3nP	zakládat
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
civilizací	civilizace	k1gFnSc7	civilizace
jen	jen	k9	jen
málo	málo	k4c1	málo
dotčených	dotčený	k2eAgFnPc2d1	dotčená
a	a	k8xC	a
často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
ojedinělou	ojedinělý	k2eAgFnSc7d1	ojedinělá
flórou	flóra	k1gFnSc7	flóra
<g/>
,	,	kIx,	,
faunou	fauna	k1gFnSc7	fauna
a	a	k8xC	a
ekosystémem	ekosystém	k1gInSc7	ekosystém
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
s	s	k7c7	s
ohroženými	ohrožený	k2eAgInPc7d1	ohrožený
druhy	druh	k1gInPc7	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biodiverzitou	biodiverzita	k1gFnSc7	biodiverzita
nebo	nebo	k8xC	nebo
neobvyklými	obvyklý	k2eNgFnPc7d1	neobvyklá
geologickými	geologický	k2eAgFnPc7d1	geologická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
zřizují	zřizovat	k5eAaImIp3nP	zřizovat
v	v	k7c6	v
civilizovaných	civilizovaný	k2eAgFnPc6d1	civilizovaná
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vrátit	vrátit	k5eAaPmF	vrátit
oblast	oblast	k1gFnSc4	oblast
do	do	k7c2	do
původního	původní	k2eAgInSc2d1	původní
stavu	stav	k1gInSc2	stav
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnSc2	oblast
označené	označený	k2eAgFnSc2d1	označená
jako	jako	k8xS	jako
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
nemají	mít	k5eNaImIp3nP	mít
divokou	divoký	k2eAgFnSc4d1	divoká
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
státním	státní	k2eAgNnSc6d1	státní
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
tam	tam	k6eAd1	tam
být	být	k5eAaImF	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
i	i	k9	i
sídla	sídlo	k1gNnSc2	sídlo
a	a	k8xC	a
obdělávaná	obdělávaný	k2eAgFnSc1d1	obdělávaná
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
plní	plnit	k5eAaImIp3nS	plnit
dvojí	dvojí	k4xRgFnSc4	dvojí
úlohu	úloha	k1gFnSc4	úloha
<g/>
,	,	kIx,	,
záchranu	záchrana	k1gFnSc4	záchrana
divočiny	divočina	k1gFnSc2	divočina
a	a	k8xC	a
také	také	k9	také
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
centra	centrum	k1gNnSc2	centrum
turistiky	turistika	k1gFnSc2	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Zvládnutí	zvládnutí	k1gNnSc1	zvládnutí
potenciálních	potenciální	k2eAgInPc2d1	potenciální
konfliktů	konflikt	k1gInPc2	konflikt
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
úkoly	úkol	k1gInPc7	úkol
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
problematické	problematický	k2eAgNnSc1d1	problematické
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
z	z	k7c2	z
turistiky	turistika	k1gFnSc2	turistika
financuje	financovat	k5eAaBmIp3nS	financovat
údržba	údržba	k1gFnSc1	údržba
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Parky	park	k1gInPc1	park
také	také	k9	také
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k8xS	jako
rezerva	rezerva	k1gFnSc1	rezerva
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
jako	jako	k8xS	jako
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
hodnotných	hodnotný	k2eAgFnPc2d1	hodnotná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Zvládnutí	zvládnutí	k1gNnSc1	zvládnutí
konfliktu	konflikt	k1gInSc2	konflikt
mezi	mezi	k7c7	mezi
poptávkou	poptávka	k1gFnSc7	poptávka
po	po	k7c6	po
využívání	využívání	k1gNnSc6	využívání
těchto	tento	k3xDgInPc2	tento
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
případnou	případný	k2eAgFnSc7d1	případná
škodou	škoda	k1gFnSc7	škoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
tak	tak	k9	tak
mohla	moct	k5eAaImAgFnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
velkou	velký	k2eAgFnSc7d1	velká
výzvou	výzva	k1gFnSc7	výzva
při	při	k7c6	při
řízení	řízení	k1gNnSc6	řízení
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nelegálnímu	legální	k2eNgNnSc3d1	nelegální
kácení	kácení	k1gNnSc3	kácení
a	a	k8xC	a
jiným	jiný	k2eAgNnSc7d1	jiné
zneužitím	zneužití	k1gNnSc7	zneužití
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
korupce	korupce	k1gFnSc2	korupce
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
také	také	k9	také
zakládají	zakládat	k5eAaImIp3nP	zakládat
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
na	na	k7c6	na
lokalitách	lokalita	k1gFnPc6	lokalita
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
kulturním	kulturní	k2eAgInSc7d1	kulturní
<g/>
,	,	kIx,	,
vědeckým	vědecký	k2eAgInSc7d1	vědecký
nebo	nebo	k8xC	nebo
historickým	historický	k2eAgInSc7d1	historický
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
speciální	speciální	k2eAgInPc1d1	speciální
subjekty	subjekt	k1gInPc1	subjekt
své	svůj	k3xOyFgFnSc2	svůj
soustavy	soustava	k1gFnSc2	soustava
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
zas	zas	k6eAd1	zas
na	na	k7c4	na
zachování	zachování	k1gNnSc4	zachování
historických	historický	k2eAgFnPc2d1	historická
lokalit	lokalita	k1gFnPc2	lokalita
používají	používat	k5eAaImIp3nP	používat
jiný	jiný	k2eAgInSc4d1	jiný
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
lokalit	lokalita	k1gFnPc2	lokalita
dostaly	dostat	k5eAaPmAgFnP	dostat
od	od	k7c2	od
organizace	organizace	k1gFnSc2	organizace
UNESCO	UNESCO	kA	UNESCO
označení	označení	k1gNnSc6	označení
Součást	součást	k1gFnSc1	součást
světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
za	za	k7c4	za
správu	správa	k1gFnSc4	správa
parků	park	k1gInPc2	park
odpovědné	odpovědný	k2eAgInPc4d1	odpovědný
orgány	orgán	k1gInPc4	orgán
místní	místní	k2eAgFnSc2d1	místní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
někdy	někdy	k6eAd1	někdy
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
národní	národní	k2eAgInPc1d1	národní
<g/>
"	"	kIx"	"
parky	park	k1gInPc1	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Národný	Národný	k2eAgInSc1d1	Národný
park	park	k1gInSc1	park
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
</s>
</p>
<p>
<s>
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
</s>
</p>
<p>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
</s>
</p>
<p>
<s>
Trvale	trvale	k6eAd1	trvale
udržitelný	udržitelný	k2eAgInSc1d1	udržitelný
rozvoj	rozvoj	k1gInSc1	rozvoj
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
