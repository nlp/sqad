<s>
Litmanovský	Litmanovský	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
památkaLitmanovský	památkaLitmanovský	k2eAgInSc1d1
potokZákladní	potokZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1990	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
14,419	14,419	k4
<g/>
1	#num#	k4
ha	ha	kA
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Stará	starý	k2eAgFnSc1d1
Ľubovňa	Ľubovňa	k1gFnSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
PIENAP	PIENAP	kA
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
53	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Litmanovský	Litmanovský	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
597	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Litmanovský	Litmanovský	k2eAgInSc1d1
potok	potok	k1gInSc1
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
PIENAP	PIENAP	kA
<g/>
.	.	kIx.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
obcí	obec	k1gFnPc2
Jarabina	Jarabina	k1gFnSc1
<g/>
,	,	kIx,
Kamienka	Kamienka	k1gFnSc1
<g/>
,	,	kIx,
Litmanová	Litmanová	k1gFnSc1
<g/>
,	,	kIx,
Hniezdne	Hniezdne	k1gFnPc2
a	a	k8xC
města	město	k1gNnSc2
Stará	starý	k2eAgFnSc1d1
Ľubovňa	Ľubovňa	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Stará	starý	k2eAgFnSc1d1
Ľubovňa	Ľubovňa	k1gFnSc1
v	v	k7c6
Prešovském	prešovský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
bylo	být	k5eAaImAgNnS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
či	či	k8xC
novelizováno	novelizovat	k5eAaBmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
na	na	k7c6
rozloze	rozloha	k1gFnSc6
14,419	14,419	k4
<g/>
1	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ochranné	ochranný	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
nebylo	být	k5eNaImAgNnS
stanoveno	stanovit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Litmanovský	Litmanovský	k2eAgInSc1d1
potok	potok	k1gInSc4
<g/>
,	,	kIx,
Štátny	Štátn	k1gMnPc4
zoznam	zoznam	k6eAd1
osobitne	osobitnout	k5eAaPmIp3nS
chránených	chránená	k1gFnPc2
častí	častit	k5eAaImIp3nP
prírody	prírod	k1gInPc1
SR	SR	kA
</s>
<s>
Chránené	Chránená	k1gFnPc1
územia	územia	k1gFnSc1
<g/>
,	,	kIx,
Štátna	Štáten	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
prírody	príroda	k1gFnSc2
Slovenskej	Slovenskej	k?
republiky	republika	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Slovensko	Slovensko	k1gNnSc1
</s>
