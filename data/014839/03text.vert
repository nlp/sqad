<s>
Josef	Josef	k1gMnSc1
Beneš	Beneš	k1gMnSc1
(	(	kIx(
<g/>
jazykovědec	jazykovědec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Beneš	Beneš	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1902	#num#	k4
<g/>
Prachatice	Prachatice	k1gFnPc4
<g/>
,	,	kIx,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1984	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
82	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
KarlovaFilozofická	KarlovaFilozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
KarlovyZkušební	KarlovyZkušební	k2eAgFnSc2d1
komise	komise	k1gFnSc2
pro	pro	k7c4
učitelství	učitelství	k1gNnSc4
na	na	k7c6
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
Povolání	povolání	k1gNnSc3
</s>
<s>
jazykovědec	jazykovědec	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
filolog	filolog	k1gMnSc1
a	a	k8xC
bohemista	bohemista	k1gMnSc1
Děti	dítě	k1gFnPc4
</s>
<s>
Dobrava	Dobrava	k1gFnSc1
Moldanová	Moldanová	k1gFnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Moldan	Moldan	k1gMnSc1
(	(	kIx(
<g/>
zeť	zeť	k1gMnSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Beneš	Beneš	k1gMnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1902	#num#	k4
Prachatice	Prachatice	k1gFnPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1984	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
antroponomastik	antroponomastik	k1gMnSc1
a	a	k8xC
bohemista	bohemista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
bohemistiku	bohemistika	k1gFnSc4
a	a	k8xC
germanistiku	germanistika	k1gFnSc4
na	na	k7c6
Filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyučoval	vyučovat	k5eAaImAgMnS
na	na	k7c6
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
postupně	postupně	k6eAd1
v	v	k7c6
Prachaticích	Prachatice	k1gFnPc6
<g/>
,	,	kIx,
Mukačevu	Mukačevo	k1gNnSc6
<g/>
,	,	kIx,
Soběslavi	Soběslav	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Kladně	Kladno	k1gNnSc6
a	a	k8xC
v	v	k7c4
Amerlingově	Amerlingově	k1gFnSc4
učitelském	učitelský	k2eAgInSc6d1
ústavu	ústav	k1gInSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
pracoval	pracovat	k5eAaImAgMnS
krátce	krátce	k6eAd1
na	na	k7c6
ministerstvu	ministerstvo	k1gNnSc6
školství	školství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
vyučoval	vyučovat	k5eAaImAgMnS
na	na	k7c6
různých	různý	k2eAgNnPc6d1
učilištích	učiliště	k1gNnPc6
a	a	k8xC
průmyslových	průmyslový	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
vyučoval	vyučovat	k5eAaImAgMnS
na	na	k7c6
Pedagogickém	pedagogický	k2eAgInSc6d1
institutu	institut	k1gInSc6
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
přednášel	přednášet	k5eAaImAgMnS
na	na	k7c6
Pedagogické	pedagogický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1933	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
články	článek	k1gInPc7
o	o	k7c6
českých	český	k2eAgNnPc6d1
příjmeních	příjmení	k1gNnPc6
především	především	k9
v	v	k7c6
časopise	časopis	k1gInSc6
Naše	náš	k3xOp1gFnSc1
řeč	řeč	k1gFnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
též	též	k9
ve	v	k7c6
Zpravodaji	zpravodaj	k1gInSc6
místopisné	místopisný	k2eAgFnSc2d1
komise	komise	k1gFnSc2
ČSAV	ČSAV	kA
(	(	kIx(
<g/>
ZMK	ZMK	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
práce	práce	k1gFnSc2
vycházející	vycházející	k2eAgFnSc2d1
časopisecky	časopisecky	k6eAd1
byla	být	k5eAaImAgFnS
zpracována	zpracovat	k5eAaPmNgFnS
do	do	k7c2
knihy	kniha	k1gFnSc2
O	o	k7c6
českých	český	k2eAgNnPc6d1
příjmeních	příjmení	k1gNnPc6
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohatý	bohatý	k2eAgInSc1d1
materiál	materiál	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
shrnut	shrnut	k2eAgInSc4d1
dcerou	dcera	k1gFnSc7
Dobravou	Dobravý	k2eAgFnSc7d1
(	(	kIx(
<g/>
provdanou	provdaný	k2eAgFnSc7d1
Moldanovou	Moldanová	k1gFnSc7
<g/>
)	)	kIx)
do	do	k7c2
knihy	kniha	k1gFnSc2
Naše	náš	k3xOp1gNnSc4
příjmení	příjmení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pozůstalosti	pozůstalost	k1gFnSc2
byla	být	k5eAaImAgFnS
editorkou	editorka	k1gFnSc7
Marií	Maria	k1gFnSc7
Novákovou	Nováková	k1gFnSc7
vydána	vydán	k2eAgFnSc1d1
fundamentální	fundamentální	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Německá	německý	k2eAgFnSc1d1
příjmení	příjmení	k1gNnSc4
u	u	k7c2
Čechů	Čech	k1gMnPc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Beneš	Beneš	k1gMnSc1
patří	patřit	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
Vladimírem	Vladimír	k1gMnSc7
Šmilauerem	Šmilauer	k1gMnSc7
<g/>
,	,	kIx,
Janem	Jan	k1gMnSc7
Svobodou	Svoboda	k1gMnSc7
a	a	k8xC
Antonínem	Antonín	k1gMnSc7
Profousem	Profous	k1gMnSc7
mezi	mezi	k7c4
zakladatele	zakladatel	k1gMnPc4
a	a	k8xC
největší	veliký	k2eAgMnPc4d3
představitele	představitel	k1gMnPc4
české	český	k2eAgMnPc4d1
onomastiky	onomastika	k1gFnSc2
a	a	k8xC
antroponomastiky	antroponomastika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
O	o	k7c6
českých	český	k2eAgNnPc6d1
příjmeních	příjmení	k1gNnPc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1962	#num#	k4
</s>
<s>
Německá	německý	k2eAgNnPc4d1
příjmení	příjmení	k1gNnPc4
u	u	k7c2
Čechů	Čech	k1gMnPc2
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
1998	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnost	farnost	k1gFnSc1
Prachatice	Prachatice	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Beneš	Beneš	k1gMnSc1
(	(	kIx(
<g/>
jazykovědec	jazykovědec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Beneš	Beneš	k1gMnSc1
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
–	–	k?
bibliografie	bibliografie	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1011606	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1021	#num#	k4
4334	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
99064818	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
14408417	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
99064818	#num#	k4
</s>
