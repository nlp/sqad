<s>
Od	od	k7c2
roku	rok	k1gInSc2
1933	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
články	článek	k1gInPc7
o	o	k7c6
českých	český	k2eAgNnPc6d1
příjmeních	příjmení	k1gNnPc6
především	především	k9
v	v	k7c6
časopise	časopis	k1gInSc6
Naše	náš	k3xOp1gFnSc1
řeč	řeč	k1gFnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
též	též	k9
ve	v	k7c6
Zpravodaji	zpravodaj	k1gInSc6
místopisné	místopisný	k2eAgFnSc2d1
komise	komise	k1gFnSc2
ČSAV	ČSAV	kA
(	(	kIx(
<g/>
ZMK	ZMK	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>