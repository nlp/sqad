<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
pak	pak	k6eAd1	pak
mohla	moct	k5eAaImAgFnS	moct
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Cambridgské	Cambridgský	k2eAgFnSc6d1	Cambridgský
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
zabývat	zabývat	k5eAaImF	zabývat
se	se	k3xPyFc4	se
mikroskopickou	mikroskopický	k2eAgFnSc7d1	mikroskopická
strukturou	struktura	k1gFnSc7	struktura
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
molekulami	molekula	k1gFnPc7	molekula
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přenáší	přenášet	k5eAaImIp3nS	přenášet
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
informaci	informace	k1gFnSc4	informace
-	-	kIx~	-
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
