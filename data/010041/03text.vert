<p>
<s>
Rosalind	Rosalinda	k1gFnPc2	Rosalinda
Elsie	Elsie	k1gFnSc1	Elsie
Franklinová	Franklinový	k2eAgFnSc1d1	Franklinová
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1920	[number]	k4	1920
Notting	Notting	k1gInSc1	Notting
Hill	Hill	k1gInSc4	Hill
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1958	[number]	k4	1958
Chelsea	Chelseum	k1gNnSc2	Chelseum
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
anglická	anglický	k2eAgFnSc1d1	anglická
biofyzička	biofyzička	k1gFnSc1	biofyzička
<g/>
,	,	kIx,	,
chemička	chemička	k1gFnSc1	chemička
a	a	k8xC	a
bioložka	bioložka	k1gFnSc1	bioložka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
především	především	k6eAd1	především
rentgenovou	rentgenový	k2eAgFnSc7d1	rentgenová
krystalografií	krystalografie	k1gFnSc7	krystalografie
<g/>
.	.	kIx.	.
</s>
<s>
Přispěla	přispět	k5eAaPmAgFnS	přispět
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
k	k	k7c3	k
objasnění	objasnění	k1gNnSc3	objasnění
molekulární	molekulární	k2eAgFnSc2d1	molekulární
struktury	struktura	k1gFnSc2	struktura
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
grafitu	grafit	k1gInSc2	grafit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
dívčích	dívčí	k2eAgFnPc2d1	dívčí
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
fyzika	fyzika	k1gFnSc1	fyzika
a	a	k8xC	a
chemie	chemie	k1gFnSc1	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
pak	pak	k6eAd1	pak
mohla	moct	k5eAaImAgFnS	moct
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Cambridgské	Cambridgský	k2eAgFnSc6d1	Cambridgský
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
zabývat	zabývat	k5eAaImF	zabývat
se	se	k3xPyFc4	se
mikroskopickou	mikroskopický	k2eAgFnSc7d1	mikroskopická
strukturou	struktura	k1gFnSc7	struktura
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
molekulami	molekula	k1gFnPc7	molekula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přenáší	přenášet	k5eAaImIp3nS	přenášet
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
informaci	informace	k1gFnSc4	informace
-	-	kIx~	-
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Franklinová	Franklinový	k2eAgFnSc1d1	Franklinová
je	být	k5eAaImIp3nS	být
nejlépe	dobře	k6eAd3	dobře
známa	znám	k2eAgFnSc1d1	známa
svou	svůj	k3xOyFgFnSc7	svůj
prací	práce	k1gFnSc7	práce
na	na	k7c6	na
difrakčních	difrakční	k2eAgInPc6d1	difrakční
obrazech	obraz	k1gInPc6	obraz
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
následně	následně	k6eAd1	následně
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
správnému	správný	k2eAgNnSc3d1	správné
určení	určení	k1gNnSc3	určení
struktury	struktura	k1gFnSc2	struktura
DNA	dno	k1gNnSc2	dno
Watsonem	Watson	k1gInSc7	Watson
a	a	k8xC	a
Crickem	Crick	k1gInSc7	Crick
<g/>
.	.	kIx.	.
</s>
<s>
Franklinová	Franklinový	k2eAgFnSc1d1	Franklinová
však	však	k9	však
sama	sám	k3xTgFnSc1	sám
zřejmě	zřejmě	k6eAd1	zřejmě
rozpoznala	rozpoznat	k5eAaPmAgFnS	rozpoznat
strukturu	struktura	k1gFnSc4	struktura
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Watson	Watson	k1gMnSc1	Watson
a	a	k8xC	a
Crick	Crick	k1gMnSc1	Crick
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
slavné	slavný	k2eAgFnSc6d1	slavná
práci	práce	k1gFnSc6	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
její	její	k3xOp3gInSc4	její
příspěvek	příspěvek	k1gInSc4	příspěvek
zmínili	zmínit	k5eAaPmAgMnP	zmínit
pouze	pouze	k6eAd1	pouze
několika	několik	k4yIc7	několik
slovy	slovo	k1gNnPc7	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
zásluhy	zásluha	k1gFnPc1	zásluha
byly	být	k5eAaImAgFnP	být
rozpoznány	rozpoznán	k2eAgFnPc1d1	rozpoznána
až	až	k9	až
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
práci	práce	k1gFnSc6	práce
na	na	k7c4	na
DNA	dno	k1gNnPc4	dno
vedla	vést	k5eAaImAgFnS	vést
Franklinová	Franklinový	k2eAgFnSc1d1	Franklinová
také	také	k9	také
výzkum	výzkum	k1gInSc4	výzkum
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
virologie	virologie	k1gFnSc2	virologie
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
však	však	k9	však
v	v	k7c6	v
37	[number]	k4	37
letech	let	k1gInPc6	let
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
vaječníku	vaječník	k1gInSc2	vaječník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rosalind	Rosalinda	k1gFnPc2	Rosalinda
Franklin	Franklina	k1gFnPc2	Franklina
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
