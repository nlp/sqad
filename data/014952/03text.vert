<s>
Bal-Sagoth	Bal-Sagoth	k1gMnSc1
</s>
<s>
Bal-SagothZákladní	Bal-SagothZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc2
Původ	původ	k1gInSc1
</s>
<s>
Yorkshire	Yorkshir	k1gMnSc5
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Žánry	žánr	k1gInPc7
</s>
<s>
black	black	k1gMnSc1
metal	metat	k5eAaImAgMnS
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
1989	#num#	k4
<g/>
-současnost	-současnost	k1gFnSc1
Vydavatelé	vydavatel	k1gMnPc1
</s>
<s>
Cacophonous	Cacophonous	k1gInSc1
Records	Records	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
Nuclear	Nuclear	k1gMnSc1
Blast	Blast	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc1
<g/>
)	)	kIx)
Web	web	k1gInSc1
</s>
<s>
www.bal-sagoth.com	www.bal-sagoth.com	k1gInSc4
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Byron	Byron	k1gInSc1
RobertsJonny	RobertsJonna	k1gFnSc2
MaudlingChris	MaudlingChris	k1gFnPc2
MaudlingPaul	MaudlingPaul	k1gInSc1
"	"	kIx"
<g/>
Wak	Wak	k1gFnSc1
<g/>
"	"	kIx"
JacksonAlistair	JacksonAlistair	k1gInSc1
MacLatchy	MacLatcha	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bal-Sagoth	Bal-Sagoth	k1gInSc1
je	být	k5eAaImIp3nS
britská	britský	k2eAgFnSc1d1
hudební	hudební	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
zformoval	zformovat	k5eAaPmAgInS
Byron	Byron	k1gInSc1
Roberts	Roberts	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
vydala	vydat	k5eAaPmAgFnS
kapela	kapela	k1gFnSc1
první	první	k4xOgFnSc2
demo	demo	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žánrově	žánrově	k6eAd1
nelze	lze	k6eNd1
kapela	kapela	k1gFnSc1
snadno	snadno	k6eAd1
zařadit	zařadit	k5eAaPmF
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
bývá	bývat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc1
hudba	hudba	k1gFnSc1
označována	označovat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
black	black	k6eAd1
metal	metat	k5eAaImAgMnS
či	či	k8xC
přesněji	přesně	k6eAd2
symfonický	symfonický	k2eAgInSc1d1
black	black	k1gInSc1
metal	metat	k5eAaImAgInS
<g/>
,	,	kIx,
ještě	ještě	k9
na	na	k7c4
Battle	Battle	k1gFnPc4
Magic	Magice	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
jsou	být	k5eAaImIp3nP
však	však	k9
znatelné	znatelný	k2eAgInPc1d1
poměrně	poměrně	k6eAd1
výrazné	výrazný	k2eAgInPc4d1
prvky	prvek	k1gInPc4
také	také	k9
death	death	k1gInSc4
metalu	metal	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
na	na	k7c6
novějších	nový	k2eAgNnPc6d2
albech	album	k1gNnPc6
ustupují	ustupovat	k5eAaImIp3nP
do	do	k7c2
pozadí	pozadí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
kapely	kapela	k1gFnSc2
</s>
<s>
Název	název	k1gInSc1
kapely	kapela	k1gFnSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
povídky	povídka	k1gFnSc2
Roberta	Robert	k1gMnSc2
E.	E.	kA
Howarda	Howard	k1gMnSc2
"	"	kIx"
<g/>
Bohové	bůh	k1gMnPc1
Bal-Sagoth	Bal-Sagoth	k1gMnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
v	v	k7c6
originále	originál	k1gInSc6
"	"	kIx"
<g/>
The	The	k1gFnSc1
Gods	Gods	k1gInSc1
of	of	k?
Bal-Sagoth	Bal-Sagoth	k1gInSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
vyšla	vyjít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
povídek	povídka	k1gFnPc2
Bran	brána	k1gFnPc2
Mak	mako	k1gNnPc2
Morn	Morna	k1gFnPc2
pod	pod	k7c7
nakladatelstvím	nakladatelství	k1gNnSc7
Laser-books	Laser-booksa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tvorba	tvorba	k1gFnSc1
</s>
<s>
Tvorba	tvorba	k1gFnSc1
Bal-Sagoth	Bal-Sagotha	k1gFnPc2
je	být	k5eAaImIp3nS
epická	epický	k2eAgFnSc1d1
a	a	k8xC
koncepční	koncepční	k2eAgFnSc1d1
<g/>
,	,	kIx,
inspirovaná	inspirovaný	k2eAgFnSc1d1
klasickými	klasický	k2eAgInPc7d1
literárními	literární	k2eAgInPc7d1
díly	díl	k1gInPc7
žánru	žánr	k1gInSc2
fantasy	fantas	k1gInPc1
(	(	kIx(
<g/>
zejména	zejména	k9
autory	autor	k1gMnPc7
R.	R.	kA
E.	E.	kA
Howardem	Howard	k1gMnSc7
a	a	k8xC
H.	H.	kA
P.	P.	kA
Lovecraftem	Lovecraft	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
textech	text	k1gInPc6
se	se	k3xPyFc4
tak	tak	k6eAd1
objevují	objevovat	k5eAaImIp3nP
bájné	bájný	k2eAgInPc1d1
kontinenty	kontinent	k1gInPc1
a	a	k8xC
země	země	k1gFnSc1
Hyperborea	Hyperborea	k1gFnSc1
<g/>
,	,	kIx,
Thule	Thule	k1gFnSc1
<g/>
,	,	kIx,
Atlantida	Atlantida	k1gFnSc1
a	a	k8xC
Mu	on	k3xPp3gInSc3
nebo	nebo	k8xC
mýtus	mýtus	k1gInSc1
Cthulhu	Cthulh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
The	The	k1gFnPc2
Power	Powero	k1gNnPc2
Cosmic	Cosmice	k1gInPc2
z	z	k7c2
prostředí	prostředí	k1gNnSc2
mezigalaktických	mezigalaktický	k2eAgFnPc2d1
válek	válka	k1gFnPc2
a	a	k8xC
vesmírných	vesmírný	k2eAgFnPc2d1
říší	říš	k1gFnPc2
pak	pak	k6eAd1
zasahuje	zasahovat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
žánru	žánr	k1gInSc2
sci-fi	sci-fi	k1gFnSc1
a	a	k8xC
space	space	k1gFnSc1
opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
Bal-Sagoth	Bal-Sagoth	k1gInSc4
je	být	k5eAaImIp3nS
typické	typický	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
skladbách	skladba	k1gFnPc6
vystupuje	vystupovat	k5eAaImIp3nS
více	hodně	k6eAd2
postav	postava	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
spolu	spolu	k6eAd1
mohou	moct	k5eAaImIp3nP
vést	vést	k5eAaImF
i	i	k9
dialog	dialog	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
odlišení	odlišení	k1gNnSc4
používá	používat	k5eAaImIp3nS
zpěvák	zpěvák	k1gMnSc1
Byron	Byron	k1gMnSc1
Roberts	Roberts	k1gInSc4
různých	různý	k2eAgNnPc2d1
zabarvení	zabarvení	k1gNnPc2
hlasu	hlas	k1gInSc2
a	a	k8xC
technik	technika	k1gFnPc2
zpěvu	zpěv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběhy	příběh	k1gInPc1
vyprávěné	vyprávěný	k2eAgInPc1d1
jednotlivými	jednotlivý	k2eAgFnPc7d1
skladbami	skladba	k1gFnPc7
se	se	k3xPyFc4
prolínají	prolínat	k5eAaImIp3nP
i	i	k9
napříč	napříč	k6eAd1
alby	alba	k1gFnSc2
<g/>
,	,	kIx,
koncepci	koncepce	k1gFnSc3
tak	tak	k6eAd1
mají	mít	k5eAaImIp3nP
nejen	nejen	k6eAd1
samotná	samotný	k2eAgNnPc4d1
alba	album	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
tvorba	tvorba	k1gFnSc1
jako	jako	k8xC,k8xS
celek	celek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Členové	člen	k1gMnPc1
</s>
<s>
Současná	současný	k2eAgFnSc1d1
sestava	sestava	k1gFnSc1
</s>
<s>
Byron	Byron	k1gInSc1
Roberts	Roberts	k1gInSc1
–	–	k?
zpěv	zpěv	k1gInSc1
</s>
<s>
Jonny	Jonna	k1gFnPc1
Maudling	Maudling	k1gInSc1
–	–	k?
klávesy	klávesa	k1gFnSc2
</s>
<s>
Chris	Chris	k1gInSc1
Maudling	Maudling	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Paul	Paul	k1gMnSc1
"	"	kIx"
<g/>
Wak	Wak	k1gMnSc1
<g/>
"	"	kIx"
Jackson	Jackson	k1gMnSc1
–	–	k?
bicí	bicí	k2eAgNnSc1d1
</s>
<s>
Alistair	Alistair	k1gMnSc1
MacLatchy	MacLatcha	k1gFnSc2
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Dema	Dema	k6eAd1
</s>
<s>
Demo	demo	k2eAgNnPc2d1
1993	#num#	k4
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Apocryphal	Apocryphal	k1gMnSc1
Tales	Tales	k1gMnSc1
(	(	kIx(
<g/>
Demo	demo	k2eAgNnPc2d1
1993	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
remasterovaná	remasterovaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
dema	dem	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
doplněná	doplněný	k2eAgFnSc1d1
o	o	k7c4
bonusy	bonus	k1gInPc4
a	a	k8xC
vydaná	vydaný	k2eAgFnSc1d1
k	k	k7c3
20	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
Dema	Dem	k1gInSc2
1993	#num#	k4
</s>
<s>
Studiová	studiový	k2eAgNnPc1d1
alba	album	k1gNnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Titul	titul	k1gInSc1
</s>
<s>
Vydavatelství	vydavatelství	k1gNnSc1
</s>
<s>
1995	#num#	k4
</s>
<s>
A	a	k9
Black	Black	k1gMnSc1
Moon	Moon	k1gMnSc1
Broods	Broodsa	k1gFnPc2
over	overa	k1gFnPc2
Lemuria	Lemurium	k1gNnSc2
</s>
<s>
Cacophonous	Cacophonous	k1gInSc1
Records	Recordsa	k1gFnPc2
</s>
<s>
1996	#num#	k4
</s>
<s>
Starfire	Starfir	k1gMnSc5
Burning	Burning	k1gInSc1
Upon	Upon	k1gMnSc1
the	the	k?
Ice-Veiled	Ice-Veiled	k1gMnSc1
Throne	Thron	k1gInSc5
of	of	k?
Ultima	ultimo	k1gNnPc1
Thule	Thule	k1gInSc1
</s>
<s>
1998	#num#	k4
</s>
<s>
Battle	Battle	k1gFnSc1
Magic	Magice	k1gFnPc2
</s>
<s>
1999	#num#	k4
</s>
<s>
The	The	k?
Power	Power	k1gMnSc1
Cosmic	Cosmic	k1gMnSc1
</s>
<s>
Nuclear	Nuclear	k1gMnSc1
Blast	Blast	k1gMnSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
Atlantis	Atlantis	k1gFnSc1
Ascendant	Ascendanta	k1gFnPc2
</s>
<s>
2006	#num#	k4
</s>
<s>
The	The	k?
Chthonic	Chthonice	k1gFnPc2
Chronicles	Chronicles	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Bal-Sagoth	Bal-Sagoth	k1gMnSc1
<g/>
,	,	kIx,
laut	laut	k1gMnSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Bal-Sagoth	Bal-Sagoth	k1gInSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Encyclopaedia	Encyclopaedium	k1gNnSc2
Metallum	Metallum	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Bal-Sagoth	Bal-Sagoth	k1gInSc1
Současná	současný	k2eAgFnSc1d1
sestava	sestava	k1gFnSc1
(	(	kIx(
<g/>
leden	leden	k1gInSc1
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Byron	Byron	k1gNnSc1
Roberts	Robertsa	k1gFnPc2
•	•	k?
Jonathan	Jonathan	k1gMnSc1
Maudling	Maudling	k1gInSc1
•	•	k?
Christopher	Christophra	k1gFnPc2
Maudling	Maudling	k1gInSc1
•	•	k?
Paul	Paul	k1gMnSc1
Jackson	Jackson	k1gMnSc1
•	•	k?
Alistair	Alistair	k1gMnSc1
MacLatchy	MacLatcha	k1gFnSc2
Studiová	studiový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
A	a	k9
Black	Black	k1gMnSc1
Moon	Moon	k1gMnSc1
Broods	Broodsa	k1gFnPc2
over	overa	k1gFnPc2
Lemuria	Lemurium	k1gNnSc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Starfire	Starfir	k1gInSc5
Burning	Burning	k1gInSc1
Upon	Upon	k1gMnSc1
the	the	k?
Ice-Veiled	Ice-Veiled	k1gMnSc1
Throne	Thron	k1gInSc5
of	of	k?
Ultima	ultimo	k1gNnSc2
Thule	Thule	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Battle	Battle	k1gFnSc1
Magic	Magic	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gMnSc1
Power	Power	k1gMnSc1
Cosmic	Cosmic	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atlantis	Atlantis	k1gFnSc1
Ascendant	Ascendant	k1gMnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc2
Chthonic	Chthonice	k1gFnPc2
Chronicles	Chronicles	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0730	#num#	k4
2325	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
157314090	#num#	k4
</s>
