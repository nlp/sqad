<s desamb="1">
Žánrově	žánrově	k6eAd1
nelze	lze	k6eNd1
kapela	kapela	k1gFnSc1
snadno	snadno	k6eAd1
zařadit	zařadit	k5eAaPmF
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
bývá	bývat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc1
hudba	hudba	k1gFnSc1
označována	označovat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
black	black	k6eAd1
metal	metat	k5eAaImAgMnS
či	či	k8xC
přesněji	přesně	k6eAd2
symfonický	symfonický	k2eAgInSc1d1
black	black	k1gInSc1
metal	metat	k5eAaImAgInS
<g/>
,	,	kIx,
ještě	ještě	k9
na	na	k7c4
Battle	Battle	k1gFnPc4
Magic	Magice	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
jsou	být	k5eAaImIp3nP
však	však	k9
znatelné	znatelný	k2eAgInPc1d1
poměrně	poměrně	k6eAd1
výrazné	výrazný	k2eAgInPc4d1
prvky	prvek	k1gInPc4
také	také	k9
death	death	k1gInSc4
metalu	metal	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
na	na	k7c6
novějších	nový	k2eAgNnPc6d2
albech	album	k1gNnPc6
ustupují	ustupovat	k5eAaImIp3nP
do	do	k7c2
pozadí	pozadí	k1gNnSc2
<g/>
.	.	kIx.
</s>