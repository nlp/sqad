<p>
<s>
Iskandar	Iskandar	k1gMnSc1	Iskandar
Mírza	Mírz	k1gMnSc2	Mírz
(	(	kIx(	(
<g/>
urdsky	urdsky	k6eAd1	urdsky
<g/>
:	:	kIx,	:
ا	ا	k?	ا
م	م	k?	م
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
56	[number]	k4	56
poslední	poslední	k2eAgMnSc1d1	poslední
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
dominia	dominion	k1gNnSc2	dominion
Pákistán	Pákistán	k1gInSc1	Pákistán
a	a	k8xC	a
následně	následně	k6eAd1	následně
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Iskandar	Iskandar	k1gInSc1	Iskandar
Mírza	Mírza	k1gFnSc1	Mírza
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1899	[number]	k4	1899
v	v	k7c6	v
Muršidábádu	Muršidábád	k1gInSc6	Muršidábád
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Britské	britský	k2eAgFnSc6d1	britská
Indii	Indie	k1gFnSc6	Indie
v	v	k7c6	v
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itské	itský	k2eAgFnSc3d1	itská
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
Bombaji	Bombaj	k1gFnSc6	Bombaj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
tamější	tamější	k2eAgFnSc2d1	tamější
Elphinstone	Elphinston	k1gInSc5	Elphinston
College	College	k1gNnPc6	College
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
Bombajskou	bombajský	k2eAgFnSc4d1	Bombajská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
elitní	elitní	k2eAgFnSc6d1	elitní
Královské	královský	k2eAgFnSc6d1	královská
vojenské	vojenský	k2eAgFnSc6d1	vojenská
akademii	akademie	k1gFnSc6	akademie
Sandhurst	Sandhurst	k1gInSc4	Sandhurst
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
absolvování	absolvování	k1gNnSc6	absolvování
(	(	kIx(	(
<g/>
stal	stát	k5eAaPmAgMnS	stát
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc7	první
člověk	člověk	k1gMnSc1	člověk
z	z	k7c2	z
Indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
<g/>
)	)	kIx)	)
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
britsko-indické	britskondický	k2eAgFnSc3d1	britsko-indická
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
funkcích	funkce	k1gFnPc6	funkce
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
posledním	poslední	k2eAgMnSc7d1	poslední
člověkem	člověk	k1gMnSc7	člověk
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
postu	post	k1gInSc6	post
však	však	k9	však
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
ani	ani	k8xC	ani
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
prvním	první	k4xOgNnSc7	první
oficiální	oficiální	k2eAgNnSc4d1	oficiální
prezidentem	prezident	k1gMnSc7	prezident
Islámské	islámský	k2eAgFnSc2d1	islámská
republiky	republika	k1gFnSc2	republika
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
postihla	postihnout	k5eAaPmAgFnS	postihnout
Pákistán	Pákistán	k1gInSc4	Pákistán
vážná	vážná	k1gFnSc1	vážná
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
politická	politický	k2eAgFnSc1d1	politická
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
Mírza	Mírza	k1gFnSc1	Mírza
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
snažil	snažit	k5eAaImAgMnS	snažit
vyřešit	vyřešit	k5eAaPmF	vyřešit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
obě	dva	k4xCgFnPc4	dva
provinční	provinční	k2eAgFnPc4d1	provinční
zákonodárná	zákonodárný	k2eAgNnPc4d1	zákonodárné
shromáždění	shromáždění	k1gNnPc4	shromáždění
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
i	i	k8xC	i
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
ústavu	ústava	k1gFnSc4	ústava
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
neplatnou	platný	k2eNgFnSc4d1	neplatná
a	a	k8xC	a
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
tentýž	týž	k3xTgInSc1	týž
rok	rok	k1gInSc1	rok
jej	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
nahradil	nahradit	k5eAaPmAgInS	nahradit
Muhammad	Muhammad	k1gInSc1	Muhammad
Ajjúb	Ajjúb	k1gMnSc1	Ajjúb
Chán	chán	k1gMnSc1	chán
<g/>
.	.	kIx.	.
</s>
<s>
Iskandar	Iskandar	k1gInSc1	Iskandar
Mírza	Mírz	k1gMnSc4	Mírz
pak	pak	k6eAd1	pak
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
londýnském	londýnský	k2eAgInSc6d1	londýnský
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
před	před	k7c7	před
svými	svůj	k3xOyFgFnPc7	svůj
sedmdesátými	sedmdesátý	k4xOgFnPc7	sedmdesátý
narozeninami	narozeniny	k1gFnPc7	narozeniny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Iskander	Iskander	k1gInSc1	Iskander
Mirza	Mirz	k1gMnSc2	Mirz
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Iskandar	Iskandara	k1gFnPc2	Iskandara
Mírza	Mírza	k1gFnSc1	Mírza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Iskander	Iskander	k1gMnSc1	Iskander
Mirza	Mirz	k1gMnSc2	Mirz
Becomes	Becomes	k1gMnSc1	Becomes
President	president	k1gMnSc1	president
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Iskander	Iskander	k1gInSc1	Iskander
Mirza	Mirz	k1gMnSc2	Mirz
</s>
</p>
