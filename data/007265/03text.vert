<s>
Tur	tur	k1gMnSc1	tur
domácí	domácí	k2eAgMnSc1d1	domácí
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
primigenius	primigenius	k1gInSc1	primigenius
f.	f.	k?	f.
taurus	taurus	k1gInSc1	taurus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
domestikovaný	domestikovaný	k2eAgMnSc1d1	domestikovaný
sudokopytnatý	sudokopytnatý	k2eAgMnSc1d1	sudokopytnatý
savec	savec	k1gMnSc1	savec
celosvětově	celosvětově	k6eAd1	celosvětově
chovaný	chovaný	k2eAgInSc4d1	chovaný
pro	pro	k7c4	pro
mnohostranný	mnohostranný	k2eAgInSc4d1	mnohostranný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
užitek	užitek	k1gInSc4	užitek
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
kurem	kur	k1gMnSc7	kur
domácím	domácí	k1gMnSc7	domácí
jde	jít	k5eAaImIp3nS	jít
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
o	o	k7c4	o
nejpočetnější	početní	k2eAgInSc4d3	nejpočetnější
druh	druh	k1gInSc4	druh
chovaného	chovaný	k2eAgNnSc2d1	chované
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Synonymem	synonymum	k1gNnSc7	synonymum
názvu	název	k1gInSc2	název
tur	tur	k1gMnSc1	tur
domácí	domácí	k1gMnSc1	domácí
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
skot	skot	k1gInSc1	skot
domácí	domácí	k1gMnPc1	domácí
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
společně	společně	k6eAd1	společně
s	s	k7c7	s
pojmenováním	pojmenování	k1gNnSc7	pojmenování
plemene	plemeno	k1gNnSc2	plemeno
<g/>
:	:	kIx,	:
např.	např.	kA	např.
herefordský	herefordský	k2eAgInSc1d1	herefordský
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
charolaiský	charolaiský	k2eAgInSc1d1	charolaiský
skot	skot	k1gInSc1	skot
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
po	po	k7c6	po
otelení	otelení	k1gNnSc6	otelení
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kráva	kráva	k1gFnSc1	kráva
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ještě	ještě	k6eAd1	ještě
nerodila	rodit	k5eNaImAgFnS	rodit
je	on	k3xPp3gFnPc4	on
jalovice	jalovice	k1gFnPc4	jalovice
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
býk	býk	k1gMnSc1	býk
<g/>
,	,	kIx,	,
vykastrovaný	vykastrovaný	k2eAgMnSc1d1	vykastrovaný
býk	býk	k1gMnSc1	býk
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
slovem	slovo	k1gNnSc7	slovo
vůl	vůl	k1gMnSc1	vůl
<g/>
,	,	kIx,	,
vůl	vůl	k1gMnSc1	vůl
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
agresivní	agresivní	k2eAgNnSc1d1	agresivní
<g/>
,	,	kIx,	,
snáze	snadno	k6eAd2	snadno
ovladatelný	ovladatelný	k2eAgInSc1d1	ovladatelný
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
chutnější	chutný	k2eAgNnSc1d2	chutnější
maso	maso	k1gNnSc1	maso
než	než	k8xS	než
býk	býk	k1gMnSc1	býk
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tele	tele	k1gNnSc1	tele
<g/>
.	.	kIx.	.
</s>
<s>
Gravidní	gravidní	k2eAgFnSc1d1	gravidní
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
pojmem	pojem	k1gInSc7	pojem
březí	březí	k2eAgNnSc4d1	březí
nebo	nebo	k8xC	nebo
stelná	stelný	k2eAgFnSc1d1	stelná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
mléčné	mléčný	k2eAgFnSc2d1	mléčná
užitkovosti	užitkovost	k1gFnSc2	užitkovost
se	se	k3xPyFc4	se
též	též	k9	též
užívá	užívat	k5eAaImIp3nS	užívat
pojem	pojem	k1gInSc4	pojem
zaprahlá	zaprahlý	k2eAgFnSc1d1	zaprahlý
nebo	nebo	k8xC	nebo
zasušená	zasušený	k2eAgFnSc1d1	zasušená
kráva	kráva	k1gFnSc1	kráva
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
nedojí	dojíst	k5eNaPmIp3nS	dojíst
<g/>
.	.	kIx.	.
</s>
<s>
Potrat	potrat	k1gInSc1	potrat
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zmetání	zmetání	k1gNnSc1	zmetání
a	a	k8xC	a
potracený	potracený	k2eAgInSc1d1	potracený
plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
zmetek	zmetek	k1gInSc4	zmetek
<g/>
.	.	kIx.	.
</s>
<s>
Volně	volně	k6eAd1	volně
žijícím	žijící	k2eAgMnSc7d1	žijící
předkem	předek	k1gMnSc7	předek
domácího	domácí	k2eAgInSc2d1	domácí
skotu	skot	k1gInSc2	skot
byl	být	k5eAaImAgMnS	být
pratur	pratur	k1gMnSc1	pratur
(	(	kIx(	(
<g/>
Bos	bos	k1gMnSc1	bos
primigenius	primigenius	k1gMnSc1	primigenius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
původní	původní	k2eAgInSc1d1	původní
areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
sahal	sahat	k5eAaImAgInS	sahat
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c4	po
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
a	a	k8xC	a
Zakavkazsko	Zakavkazsko	k1gNnSc4	Zakavkazsko
<g/>
,	,	kIx,	,
samostatný	samostatný	k2eAgInSc4d1	samostatný
poddruh	poddruh	k1gInSc4	poddruh
Bos	bos	k1gMnSc1	bos
taurus	taurus	k1gMnSc1	taurus
nomadicus	nomadicus	k1gMnSc1	nomadicus
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Pratur	pratur	k1gMnSc1	pratur
měl	mít	k5eAaImAgMnS	mít
výšku	výška	k1gFnSc4	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
a	a	k8xC	a
vážil	vážit	k5eAaImAgMnS	vážit
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
kg	kg	kA	kg
<g/>
,	,	kIx,	,
vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
se	se	k3xPyFc4	se
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
dimorfismem	dimorfismus	k1gInSc7	dimorfismus
ve	v	k7c6	v
zbarvení	zbarvení	k1gNnSc6	zbarvení
a	a	k8xC	a
mohutnými	mohutný	k2eAgInPc7d1	mohutný
rohy	roh	k1gInPc7	roh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1627	[number]	k4	1627
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
stol	stol	k1gInSc1	stol
probíhá	probíhat	k5eAaImIp3nS	probíhat
několik	několik	k4yIc4	několik
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
zpětné	zpětný	k2eAgNnSc1d1	zpětné
vyšlechtění	vyšlechtění	k1gNnSc1	vyšlechtění
z	z	k7c2	z
primitivních	primitivní	k2eAgNnPc2d1	primitivní
plemen	plemeno	k1gNnPc2	plemeno
domácího	domácí	k2eAgInSc2d1	domácí
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
domestikaci	domestikace	k1gFnSc3	domestikace
pratura	pratur	k1gMnSc2	pratur
docházelo	docházet	k5eAaImAgNnS	docházet
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Úrodného	úrodný	k2eAgInSc2d1	úrodný
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
<g/>
,	,	kIx,	,
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
též	též	k9	též
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Skot	skot	k1gInSc1	skot
byl	být	k5eAaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
chován	chovat	k5eAaImNgInS	chovat
z	z	k7c2	z
náboženských	náboženský	k2eAgInPc2d1	náboženský
důvodů	důvod	k1gInPc2	důvod
jako	jako	k8xC	jako
posvátné	posvátný	k2eAgNnSc4d1	posvátné
a	a	k8xC	a
obětní	obětní	k2eAgNnSc4d1	obětní
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
dojení	dojení	k1gNnSc1	dojení
krav	kráva	k1gFnPc2	kráva
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
i	i	k8xC	i
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Chovem	chov	k1gInSc7	chov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
původní	původní	k2eAgMnSc1d1	původní
pratur	pratur	k1gMnSc1	pratur
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
<g/>
,	,	kIx,	,
krávy	kráva	k1gFnPc1	kráva
starých	starý	k2eAgMnPc2d1	starý
Keltů	Kelt	k1gMnPc2	Kelt
a	a	k8xC	a
Germánů	Germán	k1gMnPc2	Germán
měřily	měřit	k5eAaImAgFnP	měřit
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
sotva	sotva	k6eAd1	sotva
100	[number]	k4	100
cm	cm	kA	cm
a	a	k8xC	a
vážily	vážit	k5eAaImAgFnP	vážit
asi	asi	k9	asi
200	[number]	k4	200
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
byl	být	k5eAaImAgInS	být
chován	chovat	k5eAaImNgInS	chovat
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
strakatý	strakatý	k2eAgInSc1d1	strakatý
nebo	nebo	k8xC	nebo
bezrohý	bezrohý	k2eAgInSc1d1	bezrohý
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
chianský	chianský	k2eAgInSc1d1	chianský
skot	skot	k1gInSc1	skot
chovali	chovat	k5eAaImAgMnP	chovat
již	již	k6eAd1	již
staří	starý	k2eAgMnPc1d1	starý
Etruskové	Etrusk	k1gMnPc1	Etrusk
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Uherský	uherský	k2eAgInSc1d1	uherský
stepní	stepní	k2eAgInSc1d1	stepní
skot	skot	k1gInSc1	skot
nebo	nebo	k8xC	nebo
švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
hnědý	hnědý	k2eAgInSc1d1	hnědý
skot	skot	k1gInSc1	skot
jsou	být	k5eAaImIp3nP	být
plemena	plemeno	k1gNnPc1	plemeno
známá	známý	k2eAgNnPc1d1	známé
ze	z	k7c2	z
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgNnPc2d1	moderní
plemen	plemeno	k1gNnPc2	plemeno
skotu	skot	k1gInSc2	skot
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
až	až	k9	až
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
existuje	existovat	k5eAaImIp3nS	existovat
asi	asi	k9	asi
450	[number]	k4	450
plemen	plemeno	k1gNnPc2	plemeno
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
kriterií	kriterium	k1gNnPc2	kriterium
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
využíváno	využíván	k2eAgNnSc4d1	využíváno
dělení	dělení	k1gNnSc4	dělení
plemen	plemeno	k1gNnPc2	plemeno
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
produkce	produkce	k1gFnSc2	produkce
na	na	k7c4	na
mléčná	mléčný	k2eAgNnPc4d1	mléčné
<g/>
,	,	kIx,	,
masná	masný	k2eAgFnSc1d1	Masná
a	a	k8xC	a
kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
(	(	kIx(	(
<g/>
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
pracovní	pracovní	k2eAgNnSc4d1	pracovní
využití	využití	k1gNnSc4	využití
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
lze	lze	k6eAd1	lze
plemena	plemeno	k1gNnSc2	plemeno
skotu	skot	k1gInSc2	skot
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
plemena	plemeno	k1gNnPc4	plemeno
odvozená	odvozený	k2eAgNnPc4d1	odvozené
od	od	k7c2	od
evropského	evropský	k2eAgMnSc2d1	evropský
pratura	pratur	k1gMnSc2	pratur
(	(	kIx(	(
<g/>
taurinní	taurinný	k2eAgMnPc1d1	taurinný
skot	skot	k1gInSc1	skot
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
plemena	plemeno	k1gNnPc4	plemeno
odvozená	odvozený	k2eAgNnPc4d1	odvozené
od	od	k7c2	od
pratura	pratur	k1gMnSc2	pratur
indického	indický	k2eAgMnSc2d1	indický
(	(	kIx(	(
<g/>
Bos	bos	k2eAgMnSc1d1	bos
primigeius	primigeius	k1gMnSc1	primigeius
nomadicus	nomadicus	k1gMnSc1	nomadicus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zebu	zebu	k1gMnSc1	zebu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
se	se	k3xPyFc4	se
skot	skot	k1gInSc1	skot
domácí	domácí	k2eAgInSc1d1	domácí
chová	chovat	k5eAaImIp3nS	chovat
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nP	sloužit
především	především	k9	především
voli	vůl	k1gMnPc1	vůl
jako	jako	k8xS	jako
pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
při	při	k7c6	při
tahání	tahání	k1gNnSc6	tahání
nákladů	náklad	k1gInPc2	náklad
<g/>
,	,	kIx,	,
polních	polní	k2eAgFnPc6d1	polní
pracích	práce	k1gFnPc6	práce
i	i	k8xC	i
nošení	nošení	k1gNnSc3	nošení
nákladů	náklad	k1gInPc2	náklad
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
místy	místy	k6eAd1	místy
na	na	k7c6	na
volech	vůl	k1gMnPc6	vůl
dokonce	dokonce	k9	dokonce
jezdí	jezdit	k5eAaImIp3nP	jezdit
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
z	z	k7c2	z
tura	tur	k1gMnSc2	tur
domácího	domácí	k1gMnSc2	domácí
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hovězí	hovězí	k2eAgNnSc1d1	hovězí
<g/>
,	,	kIx,	,
z	z	k7c2	z
mláďat	mládě	k1gNnPc2	mládě
do	do	k7c2	do
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
věku	věk	k1gInSc2	věk
je	být	k5eAaImIp3nS	být
telecí	telecí	k2eAgFnSc1d1	telecí
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
využití	využití	k1gNnSc1	využití
také	také	k9	také
najde	najít	k5eAaPmIp3nS	najít
hovězí	hovězí	k1gNnSc4	hovězí
kůže	kůže	k1gFnSc2	kůže
(	(	kIx(	(
<g/>
kožené	kožený	k2eAgNnSc1d1	kožené
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
,	,	kIx,	,
boty	bota	k1gFnPc1	bota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
(	(	kIx(	(
<g/>
mýdla	mýdlo	k1gNnPc1	mýdlo
<g/>
,	,	kIx,	,
kostní	kostní	k2eAgFnSc1d1	kostní
moučka	moučka	k1gFnSc1	moučka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
výkalů	výkal	k1gInPc2	výkal
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
hnůj	hnůj	k1gInSc1	hnůj
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
na	na	k7c4	na
hnojení	hnojení	k1gNnPc4	hnojení
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
se	s	k7c7	s
sušenými	sušený	k2eAgInPc7d1	sušený
kravskými	kravský	k2eAgInPc7d1	kravský
výkaly	výkal	k1gInPc7	výkal
topí	topit	k5eAaImIp3nS	topit
<g/>
.	.	kIx.	.
</s>
<s>
Skot	skot	k1gInSc1	skot
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
symbolem	symbol	k1gInSc7	symbol
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
,	,	kIx,	,
starověcí	starověký	k2eAgMnPc1d1	starověký
Germáni	Germán	k1gMnPc1	Germán
i	i	k8xC	i
Slované	Slovan	k1gMnPc1	Slovan
používali	používat	k5eAaImAgMnP	používat
slovo	slovo	k1gNnSc4	slovo
pro	pro	k7c4	pro
skot	skot	k1gInSc4	skot
jako	jako	k8xC	jako
synonymum	synonymum	k1gNnSc1	synonymum
slova	slovo	k1gNnSc2	slovo
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
u	u	k7c2	u
afrického	africký	k2eAgInSc2d1	africký
kmene	kmen	k1gInSc2	kmen
Masajů	Masaj	k1gMnPc2	Masaj
skot	skot	k1gInSc1	skot
stále	stále	k6eAd1	stále
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
platidlo	platidlo	k1gNnSc1	platidlo
<g/>
.	.	kIx.	.
</s>
<s>
Býk	býk	k1gMnSc1	býk
ve	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
civilizacích	civilizace	k1gFnPc6	civilizace
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Kréty	Kréta	k1gFnSc2	Kréta
<g/>
,	,	kIx,	,
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Persie	Persie	k1gFnSc2	Persie
či	či	k8xC	či
Indie	Indie	k1gFnSc2	Indie
symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
plodnost	plodnost	k1gFnSc4	plodnost
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
uctíván	uctívat	k5eAaImNgInS	uctívat
jako	jako	k8xS	jako
posvátné	posvátný	k2eAgNnSc1d1	posvátné
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obětován	obětován	k2eAgMnSc1d1	obětován
při	při	k7c6	při
rituálech	rituál	k1gInPc6	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
Egypťané	Egypťan	k1gMnPc1	Egypťan
uctívali	uctívat	k5eAaImAgMnP	uctívat
zbožštěného	zbožštěný	k2eAgMnSc4d1	zbožštěný
býka	býk	k1gMnSc2	býk
jménem	jméno	k1gNnSc7	jméno
Hapi	Hap	k1gFnSc2	Hap
<g/>
,	,	kIx,	,
či	či	k8xC	či
Apis	Apis	k1gInSc1	Apis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
mumifikován	mumifikovat	k5eAaBmNgMnS	mumifikovat
a	a	k8xC	a
kněží	kněz	k1gMnPc1	kněz
museli	muset	k5eAaImAgMnP	muset
vybrat	vybrat	k5eAaPmF	vybrat
jeho	on	k3xPp3gMnSc4	on
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
rituálů	rituál	k1gInPc2	rituál
býčí	býčí	k2eAgFnSc2d1	býčí
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
přeskakováním	přeskakování	k1gNnSc7	přeskakování
běžících	běžící	k2eAgMnPc2d1	běžící
býků	býk	k1gMnPc2	býk
<g/>
,	,	kIx,	,
jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
přeskakování	přeskakování	k1gNnSc4	přeskakování
býků	býk	k1gMnPc2	býk
praktikují	praktikovat	k5eAaImIp3nP	praktikovat
mladíci	mladík	k1gMnPc1	mladík
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
etiopské	etiopský	k2eAgFnSc2d1	etiopská
řeky	řeka	k1gFnSc2	řeka
Omo	Omo	k1gFnSc2	Omo
při	při	k7c6	při
slavnosti	slavnost	k1gFnSc6	slavnost
iniciace	iniciace	k1gFnSc2	iniciace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
uctívají	uctívat	k5eAaImIp3nP	uctívat
hinduisté	hinduista	k1gMnPc1	hinduista
skot	skot	k1gInSc4	skot
jako	jako	k8xC	jako
posvátná	posvátný	k2eAgNnPc4d1	posvátné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
je	být	k5eAaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
zabíjet	zabíjet	k5eAaImF	zabíjet
<g/>
,	,	kIx,	,
býk	býk	k1gMnSc1	býk
je	být	k5eAaImIp3nS	být
jízdním	jízdní	k2eAgInSc7d1	jízdní
zvířete	zvíře	k1gNnSc2	zvíře
boha	bůh	k1gMnSc4	bůh
Šivy	Šiva	k1gFnSc2	Šiva
a	a	k8xC	a
Kršna	Kršna	k1gMnSc1	Kršna
<g/>
,	,	kIx,	,
vtělení	vtělení	k1gNnSc1	vtělení
boha	bůh	k1gMnSc2	bůh
Višnua	Višnuus	k1gMnSc2	Višnuus
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k8xS	jako
pastýř	pastýř	k1gMnSc1	pastýř
s	s	k7c7	s
kravami	kráva	k1gFnPc7	kráva
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
způsobem	způsob	k1gInSc7	způsob
chovu	chov	k1gInSc2	chov
skotu	skot	k1gInSc2	skot
je	být	k5eAaImIp3nS	být
extenzivní	extenzivní	k2eAgNnSc1d1	extenzivní
pastevectví	pastevectví	k1gNnSc1	pastevectví
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
kočovné	kočovný	k2eAgNnSc1d1	kočovné
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
je	být	k5eAaImIp3nS	být
skot	skot	k1gInSc1	skot
chován	chovat	k5eAaImNgInS	chovat
celoročně	celoročně	k6eAd1	celoročně
venku	venku	k6eAd1	venku
a	a	k8xC	a
živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
pastvou	pastva	k1gFnSc7	pastva
<g/>
.	.	kIx.	.
</s>
<s>
Kočovní	kočovní	k2eAgMnPc1d1	kočovní
pastevci	pastevec	k1gMnPc1	pastevec
s	s	k7c7	s
dobytkem	dobytek	k1gInSc7	dobytek
putují	putovat	k5eAaImIp3nP	putovat
krajinou	krajina	k1gFnSc7	krajina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zabránili	zabránit	k5eAaPmAgMnP	zabránit
devastaci	devastace	k1gFnSc4	devastace
krajiny	krajina	k1gFnSc2	krajina
nadměrným	nadměrný	k2eAgNnSc7d1	nadměrné
vypásáním	vypásání	k1gNnSc7	vypásání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
koz	koza	k1gFnPc2	koza
a	a	k8xC	a
ovcí	ovce	k1gFnPc2	ovce
je	být	k5eAaImIp3nS	být
skot	skot	k1gInSc1	skot
poměrně	poměrně	k6eAd1	poměrně
náročný	náročný	k2eAgInSc1d1	náročný
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
píce	píce	k1gFnSc2	píce
a	a	k8xC	a
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
napajedla	napajedlo	k1gNnSc2	napajedlo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
nelze	lze	k6eNd1	lze
chovat	chovat	k5eAaImF	chovat
v	v	k7c6	v
pouštních	pouštní	k2eAgFnPc6d1	pouštní
a	a	k8xC	a
polopouštních	polopouštní	k2eAgFnPc6d1	polopouštní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
skot	skot	k1gInSc1	skot
chovají	chovat	k5eAaImIp3nP	chovat
hlavně	hlavně	k9	hlavně
afričtí	africký	k2eAgMnPc1d1	africký
pastevci	pastevec	k1gMnPc1	pastevec
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Masajové	Masajový	k2eAgInPc1d1	Masajový
nebo	nebo	k8xC	nebo
Fulbové	Fulbový	k2eAgInPc1d1	Fulbový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
asijští	asijský	k2eAgMnPc1d1	asijský
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
,	,	kIx,	,
Kazaši	Kazach	k1gMnPc1	Kazach
nebo	nebo	k8xC	nebo
i	i	k8xC	i
maďarští	maďarský	k2eAgMnPc1d1	maďarský
pastevci	pastevec	k1gMnPc1	pastevec
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pusty	pusta	k1gFnSc2	pusta
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
dobytek	dobytek	k1gInSc4	dobytek
chová	chovat	k5eAaImIp3nS	chovat
i	i	k9	i
v	v	k7c6	v
argentinské	argentinský	k2eAgFnSc6d1	Argentinská
pampě	pampa	k1gFnSc6	pampa
nebo	nebo	k8xC	nebo
na	na	k7c6	na
prériích	prérie	k1gFnPc6	prérie
Texasu	Texas	k1gInSc2	Texas
a	a	k8xC	a
středozápadu	středozápad	k1gInSc2	středozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
hlavně	hlavně	k9	hlavně
primitivní	primitivní	k2eAgNnPc1d1	primitivní
plemena	plemeno	k1gNnPc1	plemeno
skotu	skot	k1gInSc2	skot
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
watusi	watuse	k1gFnSc4	watuse
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgInSc4d1	uherský
stepní	stepní	k2eAgInSc4d1	stepní
skot	skot	k1gInSc4	skot
nebo	nebo	k8xC	nebo
texaský	texaský	k2eAgInSc4d1	texaský
longhorn	longhorn	k1gInSc4	longhorn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnPc1d1	moderní
specializovaná	specializovaný	k2eAgNnPc1d1	specializované
plemena	plemeno	k1gNnPc1	plemeno
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
chovu	chov	k1gInSc2	chov
daleko	daleko	k6eAd1	daleko
náročnější	náročný	k2eAgMnSc1d2	náročnější
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
masná	masný	k2eAgNnPc1d1	masné
plemena	plemeno	k1gNnPc1	plemeno
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
chována	chován	k2eAgFnSc1d1	chována
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
výbězích	výběh	k1gInPc6	výběh
na	na	k7c6	na
pastvinách	pastvina	k1gFnPc6	pastvina
a	a	k8xC	a
přikrmována	přikrmován	k2eAgFnSc1d1	přikrmována
senem	seno	k1gNnSc7	seno
či	či	k8xC	či
siláží	siláž	k1gFnSc7	siláž
<g/>
,	,	kIx,	,
u	u	k7c2	u
plemen	plemeno	k1gNnPc2	plemeno
dojných	dojná	k1gFnPc2	dojná
převažuje	převažovat	k5eAaImIp3nS	převažovat
stájový	stájový	k2eAgInSc4d1	stájový
chov	chov	k1gInSc4	chov
<g/>
.	.	kIx.	.
</s>
<s>
Skot	skot	k1gInSc1	skot
chovaný	chovaný	k2eAgInSc1d1	chovaný
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
se	se	k3xPyFc4	se
poráží	porážet	k5eAaImIp3nS	porážet
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
12-14	[number]	k4	12-14
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
hovězí	hovězí	k2eAgNnSc4d1	hovězí
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Krávy	kráva	k1gFnPc1	kráva
dojných	dojný	k2eAgNnPc2d1	dojné
plemen	plemeno	k1gNnPc2	plemeno
jsou	být	k5eAaImIp3nP	být
chovány	chován	k2eAgInPc1d1	chován
pro	pro	k7c4	pro
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
jsou	být	k5eAaImIp3nP	být
oplodněny	oplodněn	k2eAgFnPc1d1	oplodněna
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
inseminací	inseminace	k1gFnPc2	inseminace
<g/>
)	)	kIx)	)
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
2,5	[number]	k4	2,5
roku	rok	k1gInSc2	rok
a	a	k8xC	a
po	po	k7c6	po
prvním	první	k4xOgNnSc6	první
otelení	otelení	k1gNnSc6	otelení
začínají	začínat	k5eAaImIp3nP	začínat
dávat	dávat	k5eAaImF	dávat
mléko	mléko	k1gNnSc4	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Dojivost	dojivost	k1gFnSc1	dojivost
krav	kráva	k1gFnPc2	kráva
stoupá	stoupat	k5eAaImIp3nS	stoupat
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
10	[number]	k4	10
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
začne	začít	k5eAaPmIp3nS	začít
opět	opět	k6eAd1	opět
klesat	klesat	k5eAaImF	klesat
a	a	k8xC	a
krávy	kráva	k1gFnPc1	kráva
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vyřazují	vyřazovat	k5eAaImIp3nP	vyřazovat
<g/>
.	.	kIx.	.
</s>
<s>
Mléko	mléko	k1gNnSc1	mléko
dojných	dojný	k2eAgNnPc2d1	dojné
plemen	plemeno	k1gNnPc2	plemeno
je	být	k5eAaImIp3nS	být
užíváno	užíván	k2eAgNnSc1d1	užíváno
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
výživu	výživa	k1gFnSc4	výživa
<g/>
,	,	kIx,	,
telata	tele	k1gNnPc1	tele
se	se	k3xPyFc4	se
odchovávají	odchovávat	k5eAaImIp3nP	odchovávat
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
krav	kráva	k1gFnPc2	kráva
v	v	k7c6	v
teletnících	teletník	k1gInPc6	teletník
na	na	k7c6	na
mléčných	mléčný	k2eAgFnPc6d1	mléčná
směsích	směs	k1gFnPc6	směs
<g/>
,	,	kIx,	,
býčci	býček	k1gMnPc1	býček
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
telecí	telecí	k2eAgNnSc4d1	telecí
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
krávy	kráva	k1gFnSc2	kráva
trvá	trvat	k5eAaImIp3nS	trvat
průměrně	průměrně	k6eAd1	průměrně
285	[number]	k4	285
dnů	den	k1gInPc2	den
někdy	někdy	k6eAd1	někdy
i	i	k9	i
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
fyzickém	fyzický	k2eAgInSc6d1	fyzický
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
plemenné	plemenný	k2eAgFnSc3d1	plemenná
příslušnosti	příslušnost	k1gFnSc3	příslušnost
<g/>
.	.	kIx.	.
kráva	kráva	k1gFnSc1	kráva
rodí	rodit	k5eAaImIp3nS	rodit
jedno	jeden	k4xCgNnSc1	jeden
tele	tele	k1gNnSc1	tele
<g/>
,	,	kIx,	,
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nP	narodit
dvojčata	dvojče	k1gNnPc1	dvojče
rozdílného	rozdílný	k2eAgNnSc2d1	rozdílné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
jalovička	jalovička	k1gFnSc1	jalovička
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
neplodná	plodný	k2eNgFnSc1d1	neplodná
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
býčice	býčic	k1gMnSc2	býčic
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
dospělost	dospělost	k1gFnSc1	dospělost
nastává	nastávat	k5eAaImIp3nS	nastávat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
7-12	[number]	k4	7-12
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
tělesná	tělesný	k2eAgFnSc1d1	tělesná
dospělost	dospělost	k1gFnSc1	dospělost
4-5	[number]	k4	4-5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tur	tur	k1gMnSc1	tur
domácí	domácí	k1gMnSc1	domácí
je	být	k5eAaImIp3nS	být
přežvýkavec	přežvýkavec	k1gMnSc1	přežvýkavec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
složený	složený	k2eAgInSc1d1	složený
žaludek	žaludek	k1gInSc1	žaludek
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bachoru	bachor	k1gInSc2	bachor
<g/>
,	,	kIx,	,
čepce	čepec	k1gInSc2	čepec
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
slezu	slez	k1gInSc2	slez
<g/>
.	.	kIx.	.
</s>
<s>
Výživa	výživa	k1gFnSc1	výživa
každého	každý	k3xTgMnSc2	každý
jedince	jedinec	k1gMnSc2	jedinec
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
mikroorganismech	mikroorganismus	k1gInPc6	mikroorganismus
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
bachoru	bachor	k1gInSc6	bachor
<g/>
.	.	kIx.	.
</s>
<s>
Mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
(	(	kIx(	(
<g/>
houby	houba	k1gFnPc1	houba
<g/>
,	,	kIx,	,
bachořci	bachořce	k1gMnPc1	bachořce
<g/>
,	,	kIx,	,
prvoci	prvok	k1gMnPc1	prvok
<g/>
,	,	kIx,	,
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
složité	složitý	k2eAgInPc1d1	složitý
polysacharidy	polysacharid	k1gInPc1	polysacharid
-	-	kIx~	-
vlákninu	vláknina	k1gFnSc4	vláknina
a	a	k8xC	a
budují	budovat	k5eAaImIp3nP	budovat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vlastní	vlastní	k2eAgFnPc4d1	vlastní
těla	tělo	k1gNnPc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Bílkovina	bílkovina	k1gFnSc1	bílkovina
těchto	tento	k3xDgInPc2	tento
organismů	organismus	k1gInPc2	organismus
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
výživy	výživa	k1gFnSc2	výživa
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
tele	tele	k1gNnSc1	tele
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
má	mít	k5eAaImIp3nS	mít
bachor	bachor	k1gInSc1	bachor
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
slez	slez	k1gInSc1	slez
<g/>
,	,	kIx,	,
u	u	k7c2	u
dospělé	dospělý	k2eAgFnSc2d1	dospělá
krávy	kráva	k1gFnSc2	kráva
je	být	k5eAaImIp3nS	být
objem	objem	k1gInSc1	objem
bachoru	bachor	k1gInSc2	bachor
téměř	téměř	k6eAd1	téměř
desetkrát	desetkrát	k6eAd1	desetkrát
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
objem	objem	k1gInSc1	objem
slezu	slézt	k5eAaPmIp1nS	slézt
a	a	k8xC	a
činí	činit	k5eAaImIp3nS	činit
až	až	k9	až
100	[number]	k4	100
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Organizmy	organizmus	k1gInPc1	organizmus
v	v	k7c6	v
bachoru	bachor	k1gInSc6	bachor
velmi	velmi	k6eAd1	velmi
citlivě	citlivě	k6eAd1	citlivě
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
krmiva	krmivo	k1gNnSc2	krmivo
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
3	[number]	k4	3
-	-	kIx~	-
4	[number]	k4	4
týdny	týden	k1gInPc7	týden
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
organismy	organismus	k1gInPc7	organismus
přizpůsobí	přizpůsobit	k5eAaPmIp3nS	přizpůsobit
změně	změna	k1gFnSc3	změna
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
změna	změna	k1gFnSc1	změna
potravy	potrava	k1gFnSc2	potrava
z	z	k7c2	z
pastvy	pastva	k1gFnSc2	pastva
na	na	k7c4	na
seno	seno	k1gNnSc4	seno
krávě	kráva	k1gFnSc3	kráva
neublíží	ublížit	k5eNaPmIp3nP	ublížit
<g/>
,	,	kIx,	,
překrmení	překrmený	k2eAgMnPc1d1	překrmený
obilovinami	obilovina	k1gFnPc7	obilovina
a	a	k8xC	a
škrobovými	škrobový	k2eAgNnPc7d1	škrobové
krmivy	krmivo	k1gNnPc7	krmivo
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
rozkladu	rozklad	k1gInSc3	rozklad
sacharidů	sacharid	k1gInPc2	sacharid
na	na	k7c4	na
organické	organický	k2eAgFnPc4d1	organická
kyseliny	kyselina	k1gFnPc4	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
jich	on	k3xPp3gMnPc2	on
mnoho	mnoho	k6eAd1	mnoho
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
<g/>
-li	i	k?	-li
dostatečně	dostatečně	k6eAd1	dostatečně
pufrovány	pufrovat	k5eAaImNgFnP	pufrovat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
úhynu	úhyn	k1gInSc3	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
činnosti	činnost	k1gFnSc2	činnost
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
se	se	k3xPyFc4	se
donedávna	donedávna	k6eAd1	donedávna
přidávala	přidávat	k5eAaImAgFnS	přidávat
do	do	k7c2	do
krmiva	krmivo	k1gNnSc2	krmivo
pro	pro	k7c4	pro
skot	skot	k1gInSc4	skot
masokostní	masokostní	k2eAgFnSc1d1	masokostní
moučka	moučka	k1gFnSc1	moučka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
mělo	mít	k5eAaImAgNnS	mít
fatální	fatální	k2eAgInPc4d1	fatální
následky	následek	k1gInPc4	následek
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
onemocnění	onemocnění	k1gNnSc2	onemocnění
BSE	BSE	kA	BSE
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
plemen	plemeno	k1gNnPc2	plemeno
skotu	skot	k1gInSc2	skot
Plemena	plemeno	k1gNnSc2	plemeno
skotu	skot	k1gInSc2	skot
Červená	Červená	k1gFnSc1	Červená
<g/>
,	,	kIx,	,
Alena	Alena	k1gFnSc1	Alena
<g/>
,	,	kIx,	,
Anděra	Anděra	k1gFnSc1	Anděra
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
,	,	kIx,	,
Kholová	Kholová	k1gFnSc1	Kholová
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
zvířat	zvíře	k1gNnPc2	zvíře
XII	XII	kA	XII
<g/>
:	:	kIx,	:
Domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Hanzák	Hanzák	k1gMnSc1	Hanzák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
:	:	kIx,	:
Světůům	Světů	k1gInPc3	Světů
zvířat	zvíře	k1gNnPc2	zvíře
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Sambrous	Sambrous	k1gMnSc1	Sambrous
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
plemen	plemeno	k1gNnPc2	plemeno
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brázda	Brázda	k1gMnSc1	Brázda
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tur	tur	k1gMnSc1	tur
domácí	domácí	k2eAgMnSc1d1	domácí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tur	tur	k1gMnSc1	tur
domácí	domácí	k1gMnSc1	domácí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
