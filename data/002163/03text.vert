<s>
Hartford	Hartford	k6eAd1	Hartford
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Connecticut	Connecticut	k1gInSc1	Connecticut
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
124	[number]	k4	124
775	[number]	k4	775
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
však	však	k9	však
1	[number]	k4	1
188	[number]	k4	188
241	[number]	k4	241
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Connecticut	Connecticut	k1gInSc1	Connecticut
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadili	usadit	k5eAaPmAgMnP	usadit
první	první	k4xOgMnPc1	první
holandští	holandský	k2eAgMnPc1d1	holandský
osadníci	osadník	k1gMnPc1	osadník
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1636	[number]	k4	1636
se	se	k3xPyFc4	se
o	o	k7c4	o
něco	něco	k3yInSc4	něco
severněji	severně	k6eAd2	severně
usadili	usadit	k5eAaPmAgMnP	usadit
angličtí	anglický	k2eAgMnPc1d1	anglický
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
obou	dva	k4xCgFnPc2	dva
osad	osada	k1gFnPc2	osada
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
nynější	nynější	k2eAgFnSc4d1	nynější
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
důležitý	důležitý	k2eAgInSc1d1	důležitý
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Colt	Colta	k1gFnPc2	Colta
Firearms	Firearmsa	k1gFnPc2	Firearmsa
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
technologické	technologický	k2eAgFnSc2d1	technologická
firmy	firma	k1gFnSc2	firma
jako	jako	k8xC	jako
Carrier	Carrier	k1gMnSc1	Carrier
<g/>
,	,	kIx,	,
Pratt	Pratt	k1gMnSc1	Pratt
&	&	k?	&
Whitney	Whitnea	k1gFnSc2	Whitnea
<g/>
,	,	kIx,	,
Otis	Otisa	k1gFnPc2	Otisa
nebo	nebo	k8xC	nebo
Sikorsky	Sikorsky	k1gFnPc2	Sikorsky
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
spojené	spojený	k2eAgInPc1d1	spojený
v	v	k7c4	v
koncern	koncern	k1gInSc4	koncern
UTC	UTC	kA	UTC
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
124	[number]	k4	124
775	[number]	k4	775
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
29,8	[number]	k4	29,8
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
38,7	[number]	k4	38,7
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
2,8	[number]	k4	2,8
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
23,8	[number]	k4	23,8
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
4,2	[number]	k4	4,2
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
43,4	[number]	k4	43,4
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Hartford	Hartford	k6eAd1	Hartford
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
několika	několik	k4yIc2	několik
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
Hartford	Hartford	k1gMnSc1	Hartford
Wolf	Wolf	k1gMnSc1	Wolf
Pack	Pack	k1gMnSc1	Pack
<g/>
,	,	kIx,	,
před	před	k7c7	před
několika	několik	k4yIc7	několik
lety	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hrála	hrát	k5eAaImAgFnS	hrát
NHL	NHL	kA	NHL
a	a	k8xC	a
sídlil	sídlit	k5eAaImAgInS	sídlit
zde	zde	k6eAd1	zde
klub	klub	k1gInSc1	klub
Hartford	Hartforda	k1gFnPc2	Hartforda
Whalers	Whalersa	k1gFnPc2	Whalersa
<g/>
.	.	kIx.	.
</s>
<s>
Harriet	Harrieta	k1gFnPc2	Harrieta
Beecher	Beechra	k1gFnPc2	Beechra
Stoweová	Stoweový	k2eAgFnSc1d1	Stoweový
(	(	kIx(	(
<g/>
1811	[number]	k4	1811
-	-	kIx~	-
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
abolicionistka	abolicionistka	k1gFnSc1	abolicionistka
<g/>
,	,	kIx,	,
autorka	autorka	k1gFnSc1	autorka
románu	román	k1gInSc2	román
Chaloupka	chaloupka	k1gFnSc1	chaloupka
strýčka	strýček	k1gMnSc2	strýček
Toma	Tom	k1gMnSc2	Tom
Samuel	Samuel	k1gMnSc1	Samuel
Colt	Colt	k1gMnSc1	Colt
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
-	-	kIx~	-
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
revolveru	revolver	k1gInSc2	revolver
<g/>
.	.	kIx.	.
</s>
<s>
Horace	Horace	k1gFnSc1	Horace
Wells	Wellsa	k1gFnPc2	Wellsa
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
-	-	kIx~	-
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
moderní	moderní	k2eAgFnSc2d1	moderní
narkózy	narkóza	k1gFnSc2	narkóza
Frederic	Frederic	k1gMnSc1	Frederic
Edwin	Edwin	k1gMnSc1	Edwin
Church	Church	k1gMnSc1	Church
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
-	-	kIx~	-
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř-krajinář	malířrajinář	k1gMnSc1	malíř-krajinář
Mark	Mark	k1gMnSc1	Mark
Twain	Twain	k1gMnSc1	Twain
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
-	-	kIx~	-
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
humorista	humorista	k1gMnSc1	humorista
John	John	k1gMnSc1	John
Pierpont	Pierpont	k1gMnSc1	Pierpont
Morgan	morgan	k1gMnSc1	morgan
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
-	-	kIx~	-
1913	[number]	k4	1913
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
bankéřů	bankéř	k1gMnPc2	bankéř
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
Barbara	barbar	k1gMnSc2	barbar
McClintock	McClintocka	k1gFnPc2	McClintocka
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
-	-	kIx~	-
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cytogenetička	cytogenetička	k1gFnSc1	cytogenetička
<g/>
,	,	kIx,	,
držitelka	držitelka	k1gFnSc1	držitelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
Katharine	Katharin	k1gInSc5	Katharin
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
-	-	kIx~	-
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Roger	Roger	k1gInSc1	Roger
W.	W.	kA	W.
Sperry	Sperr	k1gInPc1	Sperr
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zoolog	zoolog	k1gMnSc1	zoolog
<g/>
,	,	kIx,	,
neurobiolog	neurobiolog	k1gMnSc1	neurobiolog
a	a	k8xC	a
neurofyziolog	neurofyziolog	k1gMnSc1	neurofyziolog
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
Harold	Harolda	k1gFnPc2	Harolda
J.	J.	kA	J.
Berman	Berman	k1gMnSc1	Berman
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
-	-	kIx~	-
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právní	právní	k2eAgMnSc1d1	právní
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
Jackie	Jackie	k1gFnSc2	Jackie
McLean	McLean	k1gMnSc1	McLean
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazzový	jazzový	k2eAgMnSc1d1	jazzový
altsaxofonista	altsaxofonista	k1gMnSc1	altsaxofonista
<g />
.	.	kIx.	.
</s>
<s>
Gene	gen	k1gInSc5	gen
Pitney	Pitnea	k1gMnSc2	Pitnea
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák-skladatel	zpěvákkladatel	k1gMnSc1	zpěvák-skladatel
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
zvukový	zvukový	k2eAgMnSc1d1	zvukový
inženýr	inženýr	k1gMnSc1	inženýr
Linda	Linda	k1gFnSc1	Linda
Evansová	Evansová	k1gFnSc1	Evansová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Diane	Dian	k1gInSc5	Dian
Venora	Venor	k1gMnSc4	Venor
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Jeff	Jeff	k1gMnSc1	Jeff
Porcaro	Porcara	k1gFnSc5	Porcara
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
-	-	kIx~	-
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
rockový	rockový	k2eAgMnSc1d1	rockový
bubeník	bubeník	k1gMnSc1	bubeník
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Toto	tento	k3xDgNnSc4	tento
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
ceněný	ceněný	k2eAgMnSc1d1	ceněný
studiový	studiový	k2eAgMnSc1d1	studiový
hráč	hráč	k1gMnSc1	hráč
Steve	Steve	k1gMnSc1	Steve
Porcaro	Porcara	k1gFnSc5	Porcara
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Toto	tento	k3xDgNnSc4	tento
Eriq	Eriq	k1gMnSc1	Eriq
La	la	k1gNnSc2	la
Salle	Salle	k1gNnSc2	Salle
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Stephenie	Stephenie	k1gFnSc2	Stephenie
Meyerová	Meyerová	k1gFnSc1	Meyerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Jenna	Jenna	k1gFnSc1	Jenna
Dewanová	Dewanová	k1gFnSc1	Dewanová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
tanečnice	tanečnice	k1gFnPc1	tanečnice
Hertford	Hertford	k1gInSc1	Hertford
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Floridia	Floridium	k1gNnSc2	Floridium
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Soluň	Soluň	k1gFnSc1	Soluň
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
Mangualde	Manguald	k1gInSc5	Manguald
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
Bydhošť	Bydhošť	k1gFnSc2	Bydhošť
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Caguas	Caguasa	k1gFnPc2	Caguasa
<g/>
,	,	kIx,	,
Portoriko	Portoriko	k1gNnSc1	Portoriko
Freetown	Freetowna	k1gFnPc2	Freetowna
<g/>
,	,	kIx,	,
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
Morant	Morant	k1gMnSc1	Morant
Bay	Bay	k1gFnSc1	Bay
<g/>
,	,	kIx,	,
Jamajka	Jamajka	k1gFnSc1	Jamajka
New	New	k1gMnSc1	New
Ross	Ross	k1gInSc1	Ross
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
Ocotal	Ocotal	k1gMnSc1	Ocotal
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
</s>
