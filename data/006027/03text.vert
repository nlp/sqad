<s>
Chmel	Chmel	k1gMnSc1	Chmel
otáčivý	otáčivý	k2eAgMnSc1d1	otáčivý
(	(	kIx(	(
<g/>
Humulus	Humulus	k1gMnSc1	Humulus
lupulus	lupulus	k1gMnSc1	lupulus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
dvoudomá	dvoudomý	k2eAgFnSc1d1	dvoudomá
pravotočivá	pravotočivý	k2eAgFnSc1d1	pravotočivá
liána	liána	k1gFnSc1	liána
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
konopovitých	konopovití	k1gMnPc2	konopovití
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
z	z	k7c2	z
latinských	latinský	k2eAgNnPc2d1	latinské
slov	slovo	k1gNnPc2	slovo
humus	humus	k1gInSc1	humus
=	=	kIx~	=
zem	zem	k1gFnSc1	zem
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
opory	opora	k1gFnSc2	opora
se	se	k3xPyFc4	se
plazí	plazit	k5eAaImIp3nS	plazit
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
lupus	lupus	k1gInSc1	lupus
=	=	kIx~	=
vlk	vlk	k1gMnSc1	vlk
(	(	kIx(	(
<g/>
škodí	škodit	k5eAaImIp3nS	škodit
rostlinám	rostlina	k1gFnPc3	rostlina
<g/>
)	)	kIx)	)
Humulus	Humulus	k1gMnSc1	Humulus
americanus	americanus	k1gMnSc1	americanus
T.	T.	kA	T.
Nuttall	Nuttall	k1gMnSc1	Nuttall
<g/>
,	,	kIx,	,
1847	[number]	k4	1847
Humulus	Humulus	k1gInSc1	Humulus
volubilis	volubilis	k1gInSc4	volubilis
R.	R.	kA	R.
A.	A.	kA	A.
Salisbury	Salisbur	k1gInPc1	Salisbur
<g/>
,	,	kIx,	,
1796	[number]	k4	1796
(	(	kIx(	(
<g/>
nom.	nom.	k?	nom.
illeg	illeg	k1gInSc1	illeg
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
)	)	kIx)	)
Lupulus	Lupulus	k1gInSc1	Lupulus
communis	communis	k1gFnSc2	communis
J.	J.	kA	J.
Gaertner	Gaertner	k1gMnSc1	Gaertner
<g/>
,	,	kIx,	,
1788	[number]	k4	1788
(	(	kIx(	(
<g/>
nom.	nom.	k?	nom.
illeg	illeg	k1gInSc1	illeg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Lupulus	Lupulus	k1gMnSc1	Lupulus
humulus	humulus	k1gMnSc1	humulus
P.	P.	kA	P.
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
1768	[number]	k4	1768
Lupulus	Lupulus	k1gInSc1	Lupulus
scandens	scandens	k1gInSc4	scandens
de	de	k?	de
Lamarck	Lamarck	k1gInSc1	Lamarck
<g/>
,	,	kIx,	,
1779	[number]	k4	1779
(	(	kIx(	(
<g/>
nom.	nom.	k?	nom.
illeg	illeg	k1gInSc1	illeg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Chmel	Chmel	k1gMnSc1	Chmel
otáčivý	otáčivý	k2eAgMnSc1d1	otáčivý
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
bylina	bylina	k1gFnSc1	bylina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
svislým	svislý	k2eAgInSc7d1	svislý
oddenkem	oddenek	k1gInSc7	oddenek
a	a	k8xC	a
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
podzemních	podzemní	k2eAgInPc2d1	podzemní
výhonků	výhonek	k1gInPc2	výhonek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
trvalka	trvalka	k1gFnSc1	trvalka
vydrží	vydržet	k5eAaPmIp3nS	vydržet
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
20-25	[number]	k4	20-25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tenká	tenký	k2eAgFnSc1d1	tenká
čtyřhranná	čtyřhranný	k2eAgFnSc1d1	čtyřhranná
pravotočivá	pravotočivý	k2eAgFnSc1d1	pravotočivá
lodyha	lodyha	k1gFnSc1	lodyha
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnPc4	výška
kolem	kolem	k7c2	kolem
3-5	[number]	k4	3-5
m	m	kA	m
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
až	až	k9	až
10	[number]	k4	10
m.	m.	k?	m.
Povrch	povrch	k1gInSc1	povrch
stonku	stonek	k1gInSc2	stonek
je	být	k5eAaImIp3nS	být
drsný	drsný	k2eAgInSc1d1	drsný
a	a	k8xC	a
porostlý	porostlý	k2eAgInSc1d1	porostlý
háčkovitými	háčkovitý	k2eAgInPc7d1	háčkovitý
chlupy	chlup	k1gInPc7	chlup
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
napomáhají	napomáhat	k5eAaImIp3nP	napomáhat
při	při	k7c6	při
popínání	popínání	k1gNnSc6	popínání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
lodyhy	lodyha	k1gFnSc2	lodyha
jsou	být	k5eAaImIp3nP	být
listy	list	k1gInPc1	list
vstřícné	vstřícný	k2eAgInPc1d1	vstřícný
<g/>
,	,	kIx,	,
dlanité	dlanitý	k2eAgInPc1d1	dlanitý
a	a	k8xC	a
dlouze	dlouho	k6eAd1	dlouho
řapíkaté	řapíkatý	k2eAgFnPc1d1	řapíkatá
<g/>
,	,	kIx,	,
s	s	k7c7	s
vejčitou	vejčitý	k2eAgFnSc7d1	vejčitá
až	až	k8xS	až
okrouhlou	okrouhlý	k2eAgFnSc7d1	okrouhlá
čepelí	čepel	k1gFnSc7	čepel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
klaná	klaný	k2eAgFnSc1d1	klaná
až	až	k6eAd1	až
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
dílná	dílný	k2eAgFnSc1d1	dílná
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
šířky	šířka	k1gFnPc1	šířka
až	až	k9	až
kolem	kolem	k7c2	kolem
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Okraj	okraj	k1gInSc1	okraj
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
pilovitý	pilovitý	k2eAgInSc1d1	pilovitý
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
báze	báze	k1gFnSc1	báze
je	být	k5eAaImIp3nS	být
hluboce	hluboko	k6eAd1	hluboko
srdčitá	srdčitý	k2eAgFnSc1d1	srdčitá
.	.	kIx.	.
<g/>
Povrch	povrch	k1gInSc1	povrch
na	na	k7c6	na
svrchní	svrchní	k2eAgFnSc6d1	svrchní
části	část	k1gFnSc6	část
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
zelený	zelený	k2eAgInSc1d1	zelený
a	a	k8xC	a
drsný	drsný	k2eAgInSc1d1	drsný
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
spodní	spodní	k2eAgFnSc1d1	spodní
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgFnSc1d2	světlejší
se	s	k7c7	s
žlutými	žlutý	k2eAgFnPc7d1	žlutá
žlázkami	žlázka	k1gFnPc7	žlázka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
řapíku	řapík	k1gInSc2	řapík
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dva	dva	k4xCgInPc1	dva
téměř	téměř	k6eAd1	téměř
srostlé	srostlý	k2eAgInPc1d1	srostlý
blanité	blanitý	k2eAgInPc1d1	blanitý
palisty	palist	k1gInPc1	palist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
lodyhy	lodyha	k1gFnSc2	lodyha
jsou	být	k5eAaImIp3nP	být
listy	list	k1gInPc1	list
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
střídavé	střídavý	k2eAgFnPc1d1	střídavá
a	a	k8xC	a
často	často	k6eAd1	často
celistvé	celistvý	k2eAgInPc1d1	celistvý
nebo	nebo	k8xC	nebo
laločnaté	laločnatý	k2eAgInPc1d1	laločnatý
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
šířka	šířka	k1gFnSc1	šířka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
6-10	[number]	k4	6-10
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgFnPc1d1	samčí
rostliny	rostlina	k1gFnPc1	rostlina
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
lodyh	lodyha	k1gFnPc2	lodyha
v	v	k7c6	v
úžlabí	úžlabí	k1gNnSc6	úžlabí
listů	list	k1gInPc2	list
vrcholičnaté	vrcholičnatý	k2eAgFnSc2d1	vrcholičnatý
laty	lata	k1gFnSc2	lata
prašníkových	prašníkový	k2eAgInPc2d1	prašníkový
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Zelenavá	zelenavý	k2eAgNnPc1d1	zelenavé
samičí	samičí	k2eAgNnPc1d1	samičí
květenství	květenství	k1gNnPc1	květenství
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
krátkých	krátký	k2eAgInPc2d1	krátký
klásků	klásek	k1gInPc2	klásek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
šištice	šištice	k1gFnPc1	šištice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
úžlabí	úžlabí	k1gNnSc6	úžlabí
listů	list	k1gInPc2	list
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
šupiny	šupina	k1gFnPc4	šupina
šištic	šištice	k1gFnPc2	šištice
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
zlatožluté	zlatožlutý	k2eAgFnPc1d1	zlatožlutá
lupulinové	lupulinový	k2eAgFnPc1d1	lupulinový
žlázky	žlázka	k1gFnPc1	žlázka
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
zploštělá	zploštělý	k2eAgFnSc1d1	zploštělá
vejcovitá	vejcovitý	k2eAgFnSc1d1	vejcovitá
nažka	nažka	k1gFnSc1	nažka
<g/>
.	.	kIx.	.
</s>
<s>
Plodenství	plodenství	k1gNnSc1	plodenství
chmele	chmel	k1gInSc2	chmel
otáčivého	otáčivý	k2eAgInSc2d1	otáčivý
jsou	být	k5eAaImIp3nP	být
převislá	převislý	k2eAgFnSc1d1	převislá
<g/>
,	,	kIx,	,
až	až	k9	až
6	[number]	k4	6
cm	cm	kA	cm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
se	s	k7c7	s
zvětšenými	zvětšený	k2eAgInPc7d1	zvětšený
suchými	suchý	k2eAgInPc7d1	suchý
žlutavými	žlutavý	k2eAgInPc7d1	žlutavý
listeny	listen	k1gInPc7	listen
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vlhkomilná	vlhkomilný	k2eAgFnSc1d1	vlhkomilná
rostlina	rostlina	k1gFnSc1	rostlina
se	se	k3xPyFc4	se
planý	planý	k2eAgInSc1d1	planý
chmel	chmel	k1gInSc1	chmel
otáčivý	otáčivý	k2eAgInSc1d1	otáčivý
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
olšinách	olšina	k1gFnPc6	olšina
<g/>
,	,	kIx,	,
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
v	v	k7c6	v
lužních	lužní	k2eAgInPc6d1	lužní
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
křovinách	křovina	k1gFnPc6	křovina
potoků	potok	k1gInPc2	potok
a	a	k8xC	a
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
mohutné	mohutný	k2eAgInPc4d1	mohutný
porosty	porost	k1gInPc4	porost
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vysazují	vysazovat	k5eAaImIp3nP	vysazovat
na	na	k7c6	na
rovinatých	rovinatý	k2eAgFnPc6d1	rovinatá
plochách	plocha	k1gFnPc6	plocha
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
širokých	široký	k2eAgNnPc6d1	široké
otevřených	otevřený	k2eAgNnPc6d1	otevřené
údolích	údolí	k1gNnPc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
mírně	mírně	k6eAd1	mírně
kyselá	kyselý	k2eAgFnSc1d1	kyselá
půda	půda	k1gFnSc1	půda
(	(	kIx(	(
<g/>
permské	permský	k2eAgFnSc2d1	Permská
červené	červený	k2eAgFnSc2d1	červená
půdy	půda	k1gFnSc2	půda
Žatecka	Žatecko	k1gNnSc2	Žatecko
<g/>
)	)	kIx)	)
se	s	k7c7	s
slabě	slabě	k6eAd1	slabě
kolísající	kolísající	k2eAgFnSc7d1	kolísající
hladinou	hladina	k1gFnSc7	hladina
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2	[number]	k4	2
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
mírném	mírný	k2eAgInSc6d1	mírný
pásu	pás	k1gInSc6	pás
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Zavlečen	zavleknout	k5eAaPmNgInS	zavleknout
byl	být	k5eAaImAgInS	být
i	i	k9	i
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
Chile	Chile	k1gNnSc2	Chile
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
oblastmi	oblast	k1gFnPc7	oblast
pěstování	pěstování	k1gNnSc4	pěstování
chmele	chmel	k1gInSc2	chmel
kromě	kromě	k7c2	kromě
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Plíseň	plíseň	k1gFnSc1	plíseň
chmelová	chmelový	k2eAgFnSc1d1	chmelová
(	(	kIx(	(
<g/>
Pseudoperonospora	Pseudoperonospora	k1gFnSc1	Pseudoperonospora
humuli	humule	k1gFnSc4	humule
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
houbová	houbový	k2eAgFnSc1d1	houbová
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
napadá	napadat	k5eAaBmIp3nS	napadat
výhony	výhon	k1gInPc4	výhon
<g/>
,	,	kIx,	,
listy	list	k1gInPc4	list
a	a	k8xC	a
následně	následně	k6eAd1	následně
hlávky	hlávka	k1gFnPc4	hlávka
<g/>
.	.	kIx.	.
</s>
<s>
Rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
za	za	k7c2	za
teplého	teplý	k2eAgNnSc2d1	teplé
a	a	k8xC	a
vlhkého	vlhký	k2eAgNnSc2d1	vlhké
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
žlutozelenými	žlutozelený	k2eAgFnPc7d1	žlutozelená
skvrnami	skvrna	k1gFnPc7	skvrna
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
hnědnou	hnědnout	k5eAaImIp3nP	hnědnout
a	a	k8xC	a
opadávají	opadávat	k5eAaImIp3nP	opadávat
<g/>
,	,	kIx,	,
hlávky	hlávka	k1gFnPc4	hlávka
po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
získají	získat	k5eAaPmIp3nP	získat
rezavý	rezavý	k2eAgInSc4d1	rezavý
nádech	nádech	k1gInSc4	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
vlhkých	vlhký	k2eAgNnPc2d1	vlhké
a	a	k8xC	a
teplých	teplý	k2eAgNnPc2d1	teplé
let	léto	k1gNnPc2	léto
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Padlí	padlí	k1gNnSc1	padlí
chmelové	chmelový	k2eAgFnPc1d1	chmelová
(	(	kIx(	(
<g/>
Sphaerotheca	Sphaerotheca	k1gMnSc1	Sphaerotheca
humuli	humule	k1gFnSc4	humule
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mykotická	mykotický	k2eAgFnSc1d1	mykotická
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svrchní	svrchní	k2eAgFnSc6d1	svrchní
straně	strana	k1gFnSc6	strana
listů	list	k1gInPc2	list
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
bělavé	bělavý	k2eAgInPc1d1	bělavý
povlaky	povlak	k1gInPc1	povlak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
postupně	postupně	k6eAd1	postupně
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c4	na
hlávky	hlávka	k1gFnPc4	hlávka
<g/>
.	.	kIx.	.
</s>
<s>
Mšice	mšice	k1gFnSc1	mšice
chmelová	chmelový	k2eAgFnSc1d1	chmelová
(	(	kIx(	(
<g/>
Phorodon	Phorodon	k1gMnSc1	Phorodon
humuli	humule	k1gFnSc4	humule
<g/>
)	)	kIx)	)
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
listů	list	k1gInPc2	list
saje	sát	k5eAaImIp3nS	sát
rostlinné	rostlinný	k2eAgFnPc4d1	rostlinná
šťávy	šťáva	k1gFnPc4	šťáva
a	a	k8xC	a
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
lepkavé	lepkavý	k2eAgInPc1d1	lepkavý
výkaly	výkal	k1gInPc1	výkal
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
živnou	živný	k2eAgFnSc7d1	živná
půdou	půda	k1gFnSc7	půda
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
saprofytických	saprofytický	k2eAgFnPc2d1	saprofytická
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Sviluška	sviluška	k1gFnSc1	sviluška
chmelová	chmelový	k2eAgFnSc1d1	chmelová
(	(	kIx(	(
<g/>
Tetranychus	Tetranychus	k1gMnSc1	Tetranychus
urticae	urticaat	k5eAaPmIp3nS	urticaat
<g/>
)	)	kIx)	)
sáním	sání	k1gNnSc7	sání
na	na	k7c6	na
listech	list	k1gInPc6	list
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
puchýře	puchýř	k1gInPc1	puchýř
<g/>
,	,	kIx,	,
listy	list	k1gInPc1	list
žloutnou	žloutnout	k5eAaImIp3nP	žloutnout
až	až	k9	až
postupně	postupně	k6eAd1	postupně
zešednou	zešednout	k5eAaPmIp3nP	zešednout
<g/>
,	,	kIx,	,
při	při	k7c6	při
silném	silný	k2eAgInSc6d1	silný
výskytu	výskyt	k1gInSc6	výskyt
zasychají	zasychat	k5eAaImIp3nP	zasychat
a	a	k8xC	a
opadávají	opadávat	k5eAaImIp3nP	opadávat
<g/>
.	.	kIx.	.
</s>
<s>
Hlávky	hlávka	k1gFnPc1	hlávka
získají	získat	k5eAaPmIp3nP	získat
cihlově	cihlově	k6eAd1	cihlově
červené	červený	k2eAgNnSc4d1	červené
zbarvení	zbarvení	k1gNnSc4	zbarvení
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
jejich	jejich	k3xOp3gFnSc1	jejich
kvalita	kvalita	k1gFnSc1	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Plodní	plodní	k2eAgFnPc1d1	plodní
šištice	šištice	k1gFnPc1	šištice
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pryskyřici	pryskyřice	k1gFnSc4	pryskyřice
s	s	k7c7	s
hořčinami	hořčina	k1gFnPc7	hořčina
(	(	kIx(	(
<g/>
α	α	k?	α
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
humulon	humulon	k1gInSc1	humulon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
β	β	k?	β
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
lupulon	lupulon	k1gInSc1	lupulon
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
silice	silice	k1gFnSc1	silice
s	s	k7c7	s
terpenoidy	terpenoid	k1gInPc7	terpenoid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
humulen	humulit	k5eAaPmNgInS	humulit
<g/>
,	,	kIx,	,
myrcen	myrcet	k5eAaPmNgInS	myrcet
<g/>
)	)	kIx)	)
s	s	k7c7	s
bakteriostatickými	bakteriostatický	k2eAgInPc7d1	bakteriostatický
a	a	k8xC	a
konzervačními	konzervační	k2eAgInPc7d1	konzervační
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Sekrece	sekrece	k1gFnSc1	sekrece
hořkých	hořký	k2eAgFnPc2d1	hořká
látek	látka	k1gFnPc2	látka
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začíná	začínat	k5eAaImIp3nS	začínat
i	i	k9	i
sklizeň	sklizeň	k1gFnSc1	sklizeň
<g/>
.	.	kIx.	.
</s>
<s>
Chmel	chmel	k1gInSc1	chmel
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
starým	starý	k2eAgFnPc3d1	stará
kulturním	kulturní	k2eAgFnPc3d1	kulturní
rostlinám	rostlina	k1gFnPc3	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
pojídali	pojídat	k5eAaImAgMnP	pojídat
labužníci	labužník	k1gMnPc1	labužník
chmelové	chmelový	k2eAgFnSc2d1	chmelová
výhonky	výhonek	k1gInPc1	výhonek
se	s	k7c7	s
solí	sůl	k1gFnSc7	sůl
<g/>
,	,	kIx,	,
pepřem	pepř	k1gInSc7	pepř
<g/>
,	,	kIx,	,
octem	ocet	k1gInSc7	ocet
a	a	k8xC	a
olejem	olej	k1gInSc7	olej
a	a	k8xC	a
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c4	v
jeho	jeho	k3xOp3gInPc4	jeho
léčebné	léčebný	k2eAgInPc4d1	léčebný
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
údaje	údaj	k1gInPc1	údaj
o	o	k7c4	o
pěstování	pěstování	k1gNnSc4	pěstování
chmele	chmel	k1gInSc2	chmel
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
859	[number]	k4	859
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
rozšíření	rozšíření	k1gNnSc4	rozšíření
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
zasadil	zasadit	k5eAaPmAgMnS	zasadit
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
chmelnice	chmelnice	k1gFnPc1	chmelnice
zpustošeny	zpustošen	k2eAgFnPc1d1	zpustošena
a	a	k8xC	a
obnoveny	obnoven	k2eAgFnPc1d1	obnovena
až	až	k9	až
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hlávky	hlávka	k1gFnPc1	hlávka
samičích	samičí	k2eAgInPc2d1	samičí
květů	květ	k1gInPc2	květ
(	(	kIx(	(
<g/>
chmelové	chmelový	k2eAgFnPc4d1	chmelová
šištice	šištice	k1gFnPc4	šištice
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
extrakt	extrakt	k1gInSc1	extrakt
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
pivovarnictví	pivovarnictví	k1gNnSc6	pivovarnictví
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
totiž	totiž	k9	totiž
hořčiny	hořčina	k1gFnPc1	hořčina
(	(	kIx(	(
<g/>
lupulin	lupulin	k1gInSc1	lupulin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gNnSc3	on
dodávají	dodávat	k5eAaImIp3nP	dodávat
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
aroma	aroma	k1gNnSc4	aroma
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
květy	květ	k1gInPc4	květ
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
po	po	k7c6	po
opylení	opylení	k1gNnSc6	opylení
na	na	k7c6	na
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ve	v	k7c6	v
chmelnici	chmelnice	k1gFnSc6	chmelnice
nevyskytovaly	vyskytovat	k5eNaImAgFnP	vyskytovat
samčí	samčí	k2eAgFnPc1d1	samčí
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
chmelové	chmelový	k2eAgFnPc1d1	chmelová
pelety	peleta	k1gFnPc1	peleta
(	(	kIx(	(
<g/>
lisované	lisovaný	k2eAgFnPc1d1	lisovaná
chmelové	chmelový	k2eAgFnPc1d1	chmelová
granule	granule	k1gFnPc1	granule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
výhodnější	výhodný	k2eAgFnPc1d2	výhodnější
zejména	zejména	k9	zejména
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
přepravy	přeprava	k1gFnSc2	přeprava
a	a	k8xC	a
skladování	skladování	k1gNnSc2	skladování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
pivovarnictví	pivovarnictví	k1gNnSc6	pivovarnictví
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejlépe	dobře	k6eAd3	dobře
hodnocené	hodnocený	k2eAgFnPc4d1	hodnocená
odrůdy	odrůda	k1gFnPc4	odrůda
Žatecký	žatecký	k2eAgInSc4d1	žatecký
poloranný	poloranný	k2eAgInSc4d1	poloranný
červeňák	červeňák	k1gInSc4	červeňák
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získal	získat	k5eAaPmAgInS	získat
certifikát	certifikát	k1gInSc4	certifikát
Chráněné	chráněný	k2eAgNnSc4d1	chráněné
označení	označení	k1gNnSc4	označení
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
historická	historický	k2eAgFnSc1d1	historická
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c4	o
užití	užití	k1gNnSc4	užití
chmele	chmel	k1gInSc2	chmel
pro	pro	k7c4	pro
dochucení	dochucení	k1gNnSc4	dochucení
piva	pivo	k1gNnSc2	pivo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
listiny	listina	k1gFnSc2	listina
franského	franský	k2eAgMnSc2d1	franský
krále	král	k1gMnSc2	král
Pipina	pipina	k1gFnSc1	pipina
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Krátkého	Krátký	k1gMnSc4	Krátký
z	z	k7c2	z
r.	r.	kA	r.
768	[number]	k4	768
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
chmel	chmel	k1gInSc1	chmel
(	(	kIx(	(
<g/>
pěstovaný	pěstovaný	k2eAgMnSc1d1	pěstovaný
v	v	k7c6	v
Poohří	Poohří	k1gNnSc6	Poohří
(	(	kIx(	(
<g/>
Žatecko	Žatecko	k1gNnSc1	Žatecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polabí	Polabí	k1gNnSc1	Polabí
(	(	kIx(	(
<g/>
Úštěcko	Úštěcko	k1gNnSc1	Úštěcko
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
(	(	kIx(	(
<g/>
Tršicko	Tršicko	k1gNnSc1	Tršicko
<g/>
))	))	k?	))
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejkvalitnějším	kvalitní	k2eAgNnPc3d3	nejkvalitnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
občas	občas	k6eAd1	občas
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
zelené	zelený	k2eAgNnSc4d1	zelené
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc1d1	mladý
výhonky	výhonek	k1gInPc1	výhonek
chmele	chmel	k1gInSc2	chmel
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
někdy	někdy	k6eAd1	někdy
užívají	užívat	k5eAaImIp3nP	užívat
jako	jako	k8xS	jako
zelenina	zelenina	k1gFnSc1	zelenina
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
chmelový	chmelový	k2eAgInSc4d1	chmelový
chřest	chřest	k1gInSc4	chřest
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výhonků	výhonek	k1gInPc2	výhonek
se	se	k3xPyFc4	se
též	též	k9	též
připravují	připravovat	k5eAaImIp3nP	připravovat
polévky	polévka	k1gFnPc4	polévka
<g/>
,	,	kIx,	,
omelety	omeleta	k1gFnPc4	omeleta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
se	se	k3xPyFc4	se
nakládají	nakládat	k5eAaImIp3nP	nakládat
<g/>
.	.	kIx.	.
</s>
<s>
Lupulin	lupulin	k1gInSc1	lupulin
z	z	k7c2	z
chmelových	chmelový	k2eAgFnPc2d1	chmelová
žlázek	žlázka	k1gFnPc2	žlázka
zavedl	zavést	k5eAaPmAgInS	zavést
jako	jako	k8xS	jako
sedativum	sedativum	k1gNnSc1	sedativum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1813	[number]	k4	1813
pařížský	pařížský	k2eAgMnSc1d1	pařížský
lékárník	lékárník	k1gMnSc1	lékárník
Planche	Planch	k1gFnSc2	Planch
<g/>
.	.	kIx.	.
</s>
<s>
Sušené	sušený	k2eAgFnPc1d1	sušená
pestíkové	pestíkový	k2eAgFnPc1d1	pestíkový
šištice	šištice	k1gFnPc1	šištice
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k8xS	jako
droga	droga	k1gFnSc1	droga
Strobili	Strobili	k1gFnSc3	Strobili
lupuli	lupule	k1gFnSc4	lupule
a	a	k8xC	a
chmelové	chmelový	k2eAgFnPc4d1	chmelová
žlázky	žlázka	k1gFnPc4	žlázka
jako	jako	k8xS	jako
Glanduale	Glanduala	k1gFnSc3	Glanduala
lupuli	lupule	k1gFnSc3	lupule
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
chmelová	chmelový	k2eAgFnSc1d1	chmelová
moučka	moučka	k1gFnSc1	moučka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
homeopatii	homeopatie	k1gFnSc6	homeopatie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
esence	esence	k1gFnSc1	esence
z	z	k7c2	z
čerstvých	čerstvý	k2eAgFnPc2d1	čerstvá
žlázek	žlázka	k1gFnPc2	žlázka
jako	jako	k8xC	jako
diuretikum	diuretikum	k1gNnSc1	diuretikum
<g/>
,	,	kIx,	,
antiafrodiziakum	antiafrodiziakum	k1gNnSc1	antiafrodiziakum
nebo	nebo	k8xC	nebo
narkotikum	narkotikum	k1gNnSc1	narkotikum
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kosmetických	kosmetický	k2eAgInPc2d1	kosmetický
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
léčitelství	léčitelství	k1gNnSc6	léčitelství
se	se	k3xPyFc4	se
také	také	k9	také
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
sedativum	sedativum	k1gNnSc1	sedativum
a	a	k8xC	a
také	také	k9	také
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Sbírají	sbírat	k5eAaImIp3nP	sbírat
se	se	k3xPyFc4	se
mladé	mladý	k2eAgInPc1d1	mladý
výhonky	výhonek	k1gInPc1	výhonek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zatím	zatím	k6eAd1	zatím
neprorostly	prorůst	k5eNaPmAgInP	prorůst
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hodně	hodně	k6eAd1	hodně
vitamínů	vitamín	k1gInPc2	vitamín
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
i	i	k8xC	i
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
chmelových	chmelový	k2eAgFnPc2d1	chmelová
žlázek	žlázka	k1gFnPc2	žlázka
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
proti	proti	k7c3	proti
nespavosti	nespavost	k1gFnSc3	nespavost
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
močopudně	močopudně	k6eAd1	močopudně
a	a	k8xC	a
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
při	při	k7c6	při
křečích	křeč	k1gFnPc6	křeč
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
