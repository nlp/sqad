<p>
<s>
Landsat	Landsat	k5eAaPmF	Landsat
4	[number]	k4	4
byl	být	k5eAaImAgInS	být
satelit	satelit	k1gInSc4	satelit
Země	zem	k1gFnSc2	zem
vypuštěný	vypuštěný	k2eAgInSc4d1	vypuštěný
jako	jako	k8xC	jako
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
z	z	k7c2	z
programu	program	k1gInSc6	program
Landsat	Landsat	k1gMnPc2	Landsat
agentury	agentura	k1gFnSc2	agentura
NASA	NASA	kA	NASA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
určený	určený	k2eAgInSc1d1	určený
k	k	k7c3	k
podrobnému	podrobný	k2eAgNnSc3d1	podrobné
snímkování	snímkování	k1gNnSc3	snímkování
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgNnPc1d1	základní
data	datum	k1gNnPc1	datum
==	==	k?	==
</s>
</p>
<p>
<s>
Družice	družice	k1gFnSc1	družice
byla	být	k5eAaImAgFnS	být
vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1982	[number]	k4	1982
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
nosné	nosný	k2eAgFnPc1d1	nosná
rakety	raketa	k1gFnPc1	raketa
Delta	delta	k1gFnSc1	delta
3920	[number]	k4	3920
na	na	k7c6	na
kosmodromu	kosmodrom	k1gInSc6	kosmodrom
Vandenberg	Vandenberg	k1gMnSc1	Vandenberg
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
Base	basa	k1gFnSc3	basa
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
katalogizována	katalogizovat	k5eAaImNgFnS	katalogizovat
v	v	k7c6	v
COSPAR	COSPAR	kA	COSPAR
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
72	[number]	k4	72
<g/>
A.	A.	kA	A.
</s>
</p>
<p>
<s>
Létala	létat	k5eAaImAgFnS	létat
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
kruhové	kruhový	k2eAgFnSc6d1	kruhová
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
710	[number]	k4	710
km	km	kA	km
nad	nad	k7c7	nad
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
1938	[number]	k4	1938
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
aktivní	aktivní	k2eAgFnSc1d1	aktivní
činnost	činnost	k1gFnSc1	činnost
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
létaly	létat	k5eAaImAgFnP	létat
další	další	k2eAgFnPc1d1	další
družice	družice	k1gFnPc1	družice
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybavení	vybavení	k1gNnSc1	vybavení
==	==	k?	==
</s>
</p>
<p>
<s>
Družice	družice	k1gFnSc1	družice
Landsat	Landsat	k1gFnPc2	Landsat
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
několika	několik	k4yIc7	několik
druhy	druh	k1gInPc1	druh
kamer	kamera	k1gFnPc2	kamera
<g/>
,	,	kIx,	,
novým	nový	k2eAgInSc7d1	nový
mechanickým	mechanický	k2eAgInSc7d1	mechanický
skenerem	skener	k1gInSc7	skener
typu	typ	k1gInSc2	typ
TM	TM	kA	TM
a	a	k8xC	a
zařízením	zařízení	k1gNnSc7	zařízení
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
na	na	k7c4	na
příslušné	příslušný	k2eAgFnPc4d1	příslušná
monitorovací	monitorovací	k2eAgFnPc4d1	monitorovací
stanice	stanice	k1gFnPc4	stanice
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Landsat	Landsat	k1gFnSc2	Landsat
4	[number]	k4	4
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.lib.cas.cz/space.40/1982/072A.HTM	[url]	k4	http://www.lib.cas.cz/space.40/1982/072A.HTM
</s>
</p>
<p>
<s>
http://www.gisat.cz/content/cz/dpz/prehled-druzicovych-systemu/landsat	[url]	k5eAaImF	http://www.gisat.cz/content/cz/dpz/prehled-druzicovych-systemu/landsat
</s>
</p>
