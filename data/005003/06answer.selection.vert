<s>
Elamština	Elamština	k1gFnSc1	Elamština
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
mrtvých	mrtvý	k2eAgInPc2d1	mrtvý
jazyků	jazyk	k1gInPc2	jazyk
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Elamské	elamský	k2eAgFnSc2d1	elamský
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k8xC	i
Achaimenovské	Achaimenovský	k2eAgFnSc2d1	Achaimenovský
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
