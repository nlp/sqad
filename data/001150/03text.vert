<s>
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
byl	být	k5eAaImAgMnS	být
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1955	[number]	k4	1955
a	a	k8xC	a
1975	[number]	k4	1975
probíhal	probíhat	k5eAaImAgInS	probíhat
na	na	k7c6	na
území	území	k1gNnSc6	území
Vietnamu	Vietnam	k1gInSc2	Vietnam
a	a	k8xC	a
v	v	k7c6	v
příhraničí	příhraničí	k1gNnSc6	příhraničí
sousedících	sousedící	k2eAgFnPc2d1	sousedící
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Kambodža	Kambodža	k1gFnSc1	Kambodža
a	a	k8xC	a
Laos	Laos	k1gInSc1	Laos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Druhá	druhý	k4xOgFnSc1	druhý
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Indočíně	Indočína	k1gFnSc6	Indočína
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Vietnamu	Vietnam	k1gInSc2	Vietnam
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
pozemní	pozemní	k2eAgInSc4d1	pozemní
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
měl	mít	k5eAaImAgInS	mít
konflikt	konflikt	k1gInSc1	konflikt
podobu	podoba	k1gFnSc4	podoba
bombardování	bombardování	k1gNnSc2	bombardování
a	a	k8xC	a
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
řeky	řeka	k1gFnSc2	řeka
Mekong	Mekong	k1gInSc1	Mekong
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
říční	říční	k2eAgFnSc4d1	říční
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
likvidace	likvidace	k1gFnSc1	likvidace
pobřežních	pobřežní	k2eAgInPc2d1	pobřežní
porostů	porost	k1gInPc2	porost
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
nepříteli	nepřítel	k1gMnSc6	nepřítel
jako	jako	k9	jako
krytí	krytí	k1gNnSc4	krytí
a	a	k8xC	a
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
léčky	léčka	k1gFnPc4	léčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
země	zem	k1gFnPc1	zem
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
a	a	k8xC	a
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
bránit	bránit	k5eAaImF	bránit
šíření	šíření	k1gNnSc4	šíření
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
proamerickém	proamerický	k2eAgInSc6d1	proamerický
jižnímu	jižní	k2eAgInSc3d1	jižní
Vietnamu	Vietnam	k1gInSc3	Vietnam
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
severnímu	severní	k2eAgMnSc3d1	severní
komunistickému	komunistický	k2eAgMnSc3d1	komunistický
sousedovi	soused	k1gMnSc3	soused
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
podporovaly	podporovat	k5eAaImAgInP	podporovat
zase	zase	k9	zase
státy	stát	k1gInPc1	stát
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
partyzánská	partyzánský	k2eAgFnSc1d1	Partyzánská
komunistická	komunistický	k2eAgFnSc1d1	komunistická
organizace	organizace	k1gFnSc1	organizace
Národní	národní	k2eAgFnSc1d1	národní
fronta	fronta	k1gFnSc1	fronta
osvobození	osvobození	k1gNnSc2	osvobození
Jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
(	(	kIx(	(
<g/>
známá	známá	k1gFnSc1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Vietkong	Vietkong	k1gMnSc1	Vietkong
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
materiální	materiální	k2eAgFnSc4d1	materiální
podporu	podpora	k1gFnSc4	podpora
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
i	i	k9	i
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vietnamské	vietnamský	k2eAgFnSc6d1	vietnamská
socialistické	socialistický	k2eAgFnSc6d1	socialistická
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
konflikt	konflikt	k1gInSc1	konflikt
známý	známý	k2eAgInSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
americké	americký	k2eAgFnSc3d1	americká
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Intervence	intervence	k1gFnSc1	intervence
USA	USA	kA	USA
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
znamenala	znamenat	k5eAaImAgFnS	znamenat
vlastně	vlastně	k9	vlastně
poslední	poslední	k2eAgFnSc4d1	poslední
eskalaci	eskalace	k1gFnSc4	eskalace
třicetiletého	třicetiletý	k2eAgInSc2d1	třicetiletý
ozbrojeného	ozbrojený	k2eAgInSc2d1	ozbrojený
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
a	a	k8xC	a
1946	[number]	k4	1946
odporem	odpor	k1gInSc7	odpor
vietnamských	vietnamský	k2eAgMnPc2d1	vietnamský
komunistů	komunista	k1gMnPc2	komunista
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
skupin	skupina	k1gFnPc2	skupina
proti	proti	k7c3	proti
setrvání	setrvání	k1gNnSc3	setrvání
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
návratu	návrat	k1gInSc3	návrat
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
nadvlády	nadvláda	k1gFnSc2	nadvláda
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Indočíně	Indočína	k1gFnSc6	Indočína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
udává	udávat	k5eAaImIp3nS	udávat
i	i	k9	i
jiný	jiný	k2eAgInSc4d1	jiný
začátek	začátek	k1gInSc4	začátek
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
rok	rok	k1gInSc4	rok
1957	[number]	k4	1957
a	a	k8xC	a
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
hlavně	hlavně	k9	hlavně
internacionalizace	internacionalizace	k1gFnSc1	internacionalizace
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
odpor	odpor	k1gInSc4	odpor
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
americké	americký	k2eAgFnSc2d1	americká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zastánce	zastánce	k1gMnSc4	zastánce
války	válka	k1gFnSc2	válka
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
zástupná	zástupný	k2eAgFnSc1d1	zástupná
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
projevů	projev	k1gInPc2	projev
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
Východem	východ	k1gInSc7	východ
a	a	k8xC	a
Západem	západ	k1gInSc7	západ
<g/>
;	;	kIx,	;
odpůrci	odpůrce	k1gMnPc1	odpůrce
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
neřešitelnost	neřešitelnost	k1gFnSc4	neřešitelnost
celé	celý	k2eAgFnSc2d1	celá
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
extrémní	extrémní	k2eAgFnSc4d1	extrémní
míru	míra	k1gFnSc4	míra
násilí	násilí	k1gNnSc2	násilí
postihující	postihující	k2eAgFnSc2d1	postihující
jak	jak	k8xS	jak
bojující	bojující	k2eAgFnSc2d1	bojující
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
tak	tak	k9	tak
civilisty	civilista	k1gMnPc4	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
vietnamského	vietnamský	k2eAgNnSc2d1	vietnamské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
zas	zas	k6eAd1	zas
další	další	k2eAgInSc1d1	další
boj	boj	k1gInSc1	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
USA	USA	kA	USA
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
popularita	popularita	k1gFnSc1	popularita
různých	různý	k2eAgNnPc2d1	různé
mírových	mírový	k2eAgNnPc2d1	Mírové
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
demonstrace	demonstrace	k1gFnPc1	demonstrace
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
převážil	převážit	k5eAaPmAgInS	převážit
odpor	odpor	k1gInSc1	odpor
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
USA	USA	kA	USA
nakonec	nakonec	k6eAd1	nakonec
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
<g/>
,	,	kIx,	,
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Severní	severní	k2eAgInSc1d1	severní
Vietnam	Vietnam	k1gInSc1	Vietnam
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
země	zem	k1gFnSc2	zem
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
socialistický	socialistický	k2eAgInSc4d1	socialistický
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
krutostí	krutost	k1gFnSc7	krutost
<g/>
;	;	kIx,	;
trpěli	trpět	k5eAaImAgMnP	trpět
vietnamští	vietnamský	k2eAgMnPc1d1	vietnamský
civilisté	civilista	k1gMnPc1	civilista
(	(	kIx(	(
<g/>
Masakr	masakr	k1gInSc1	masakr
v	v	k7c6	v
My	my	k3xPp1nPc1	my
Lai	Lai	k1gFnSc1	Lai
<g/>
,	,	kIx,	,
Masakr	masakr	k1gInSc1	masakr
v	v	k7c4	v
Hue	Hue	k1gFnSc4	Hue
<g/>
,	,	kIx,	,
Masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Dak	Dak	k1gFnSc6	Dak
Son	son	k1gInSc4	son
<g/>
,	,	kIx,	,
Masakr	masakr	k1gInSc4	masakr
v	v	k7c4	v
Phuoc-vinh	Phuocinh	k1gInSc4	Phuoc-vinh
<g/>
)	)	kIx)	)
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
ničeno	ničen	k2eAgNnSc4d1	ničeno
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
použily	použít	k5eAaPmAgFnP	použít
proti	proti	k7c3	proti
rostlinným	rostlinný	k2eAgInPc3d1	rostlinný
porostům	porost	k1gInPc3	porost
defoliant	defoliant	k1gInSc4	defoliant
Agent	agent	k1gMnSc1	agent
Orange	Orang	k1gInSc2	Orang
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
odlistění	odlistění	k1gNnSc6	odlistění
přestal	přestat	k5eAaPmAgInS	přestat
prales	prales	k1gInSc4	prales
partyzánům	partyzán	k1gMnPc3	partyzán
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
úkryt	úkryt	k1gInSc4	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Použity	použit	k2eAgFnPc1d1	použita
byly	být	k5eAaImAgFnP	být
i	i	k9	i
další	další	k2eAgFnPc1d1	další
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kyselina	kyselina	k1gFnSc1	kyselina
dimethylarsinová	dimethylarsinová	k1gFnSc1	dimethylarsinová
(	(	kIx(	(
<g/>
Agent	agent	k1gMnSc1	agent
Blue	Blu	k1gFnSc2	Blu
<g/>
)	)	kIx)	)
na	na	k7c6	na
zničení	zničení	k1gNnSc6	zničení
úrod	úroda	k1gFnPc2	úroda
rýže	rýže	k1gFnSc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Francouzů	Francouz	k1gMnPc2	Francouz
v	v	k7c6	v
koloniální	koloniální	k2eAgFnSc6d1	koloniální
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
celistvá	celistvý	k2eAgFnSc1d1	celistvá
Indočína	Indočína	k1gFnSc1	Indočína
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
Kambodžu	Kambodža	k1gFnSc4	Kambodža
<g/>
,	,	kIx,	,
Laos	Laos	k1gInSc1	Laos
a	a	k8xC	a
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
Ženevské	ženevský	k2eAgFnSc2d1	Ženevská
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
bylo	být	k5eAaImAgNnS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
Vietnamu	Vietnam	k1gInSc2	Vietnam
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
nezávislé	závislý	k2eNgInPc4d1	nezávislý
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
Severní	severní	k2eAgInSc1d1	severní
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Hanoj	Hanoj	k1gFnSc1	Hanoj
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jižní	jižní	k2eAgInSc1d1	jižní
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Saigonem	Saigon	k1gInSc7	Saigon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
země	zem	k1gFnSc2	zem
byly	být	k5eAaImAgInP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
17	[number]	k4	17
<g/>
.	.	kIx.	.
rovnoběžkou	rovnoběžka	k1gFnSc7	rovnoběžka
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
sjednotit	sjednotit	k5eAaPmF	sjednotit
po	po	k7c6	po
demokratických	demokratický	k2eAgFnPc6d1	demokratická
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
nikdy	nikdy	k6eAd1	nikdy
nekonaly	konat	k5eNaImAgFnP	konat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
USA	USA	kA	USA
a	a	k8xC	a
Jižní	jižní	k2eAgInSc1d1	jižní
Vietnam	Vietnam	k1gInSc1	Vietnam
obávaly	obávat	k5eAaImAgInP	obávat
vítězství	vítězství	k1gNnSc3	vítězství
Ho	on	k3xPp3gNnSc2	on
Či	či	k9wB	či
Mina	mina	k1gFnSc1	mina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
porazil	porazit	k5eAaPmAgMnS	porazit
francouzské	francouzský	k2eAgFnPc4d1	francouzská
jenotky	jenotka	k1gFnPc4	jenotka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemi	zem	k1gFnSc4	zem
sjednotí	sjednotit	k5eAaPmIp3nS	sjednotit
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
komunistům	komunista	k1gMnPc3	komunista
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
díky	díky	k7c3	díky
pomoci	pomoc	k1gFnSc3	pomoc
ostatních	ostatní	k2eAgFnPc2d1	ostatní
socialistických	socialistický	k2eAgFnPc2d1	socialistická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
dařilo	dařit	k5eAaImAgNnS	dařit
rychle	rychle	k6eAd1	rychle
vybudovat	vybudovat	k5eAaPmF	vybudovat
silnou	silný	k2eAgFnSc4d1	silná
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
už	už	k6eAd1	už
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
posílat	posílat	k5eAaImF	posílat
do	do	k7c2	do
Jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
instruktory	instruktor	k1gMnPc4	instruktor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
cvičili	cvičit	k5eAaImAgMnP	cvičit
partyzány	partyzán	k1gMnPc7	partyzán
nově	nově	k6eAd1	nově
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
Vietkongu	Vietkong	k1gInSc2	Vietkong
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
začali	začít	k5eAaPmAgMnP	začít
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
aktivitami	aktivita	k1gFnPc7	aktivita
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
už	už	k6eAd1	už
koncem	koncem	k7c2	koncem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
jejich	jejich	k3xOp3gFnSc2	jejich
činnosti	činnost	k1gFnSc2	činnost
bylo	být	k5eAaImAgNnS	být
vyvolání	vyvolání	k1gNnSc1	vyvolání
povstání	povstání	k1gNnSc2	povstání
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
proti	proti	k7c3	proti
saigonskému	saigonský	k2eAgInSc3d1	saigonský
režimu	režim	k1gInSc3	režim
a	a	k8xC	a
sjednocení	sjednocení	k1gNnSc3	sjednocení
země	zem	k1gFnSc2	zem
pod	pod	k7c7	pod
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
Infiltraci	infiltrace	k1gFnSc3	infiltrace
do	do	k7c2	do
Jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
přitom	přitom	k6eAd1	přitom
používali	používat	k5eAaImAgMnP	používat
tzv.	tzv.	kA	tzv.
Ho	on	k3xPp3gNnSc4	on
Či	či	k9wB	či
Minovu	Minův	k2eAgFnSc4d1	Minova
stezku	stezka	k1gFnSc4	stezka
<g/>
:	:	kIx,	:
síť	síť	k1gFnSc4	síť
infiltračních	infiltrační	k2eAgFnPc2d1	infiltrační
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
chodníků	chodník	k1gInPc2	chodník
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
provizorních	provizorní	k2eAgFnPc2d1	provizorní
komunikací	komunikace	k1gFnPc2	komunikace
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
ze	z	k7c2	z
Severního	severní	k2eAgInSc2d1	severní
Vietnamu	Vietnam	k1gInSc2	Vietnam
podél	podél	k7c2	podél
jihovietnamské	jihovietnamský	k2eAgFnSc2d1	jihovietnamská
západní	západní	k2eAgFnSc2d1	západní
hranice	hranice	k1gFnSc2	hranice
pohraničními	pohraniční	k2eAgFnPc7d1	pohraniční
oblastmi	oblast	k1gFnPc7	oblast
sousedního	sousední	k2eAgInSc2d1	sousední
Laosu	Laos	k1gInSc2	Laos
a	a	k8xC	a
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
oficiální	oficiální	k2eAgFnSc4d1	oficiální
neutralitu	neutralita	k1gFnSc4	neutralita
pod	pod	k7c7	pod
plnou	plný	k2eAgFnSc7d1	plná
kontrolou	kontrola	k1gFnSc7	kontrola
severovietnamské	severovietnamský	k2eAgFnSc2d1	severovietnamská
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
Vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
-	-	kIx~	-
VLA	VLA	kA	VLA
<g/>
)	)	kIx)	)
a	a	k8xC	a
partyzánů	partyzán	k1gMnPc2	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Podporovalo	podporovat	k5eAaImAgNnS	podporovat
je	on	k3xPp3gNnSc4	on
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
v	v	k7c6	v
partyzánech	partyzán	k1gMnPc6	partyzán
vidělo	vidět	k5eAaImAgNnS	vidět
záchranu	záchrana	k1gFnSc4	záchrana
z	z	k7c2	z
bídy	bída	k1gFnSc2	bída
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
zemi	zem	k1gFnSc4	zem
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Saigonu	Saigon	k1gInSc6	Saigon
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
jihovietnamská	jihovietnamský	k2eAgFnSc1d1	jihovietnamská
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
ARVN	ARVN	kA	ARVN
-	-	kIx~	-
Army	Arma	k1gMnSc2	Arma
of	of	k?	of
the	the	k?	the
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
)	)	kIx)	)
nebyla	být	k5eNaImAgFnS	být
schopná	schopný	k2eAgFnSc1d1	schopná
zastavit	zastavit	k5eAaPmF	zastavit
nebo	nebo	k8xC	nebo
potlačit	potlačit	k5eAaPmF	potlačit
partyzány	partyzána	k1gFnPc4	partyzána
<g/>
,	,	kIx,	,
rozvířilo	rozvířit	k5eAaPmAgNnS	rozvířit
politickou	politický	k2eAgFnSc4d1	politická
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
Jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
Ngo	Ngo	k1gMnSc1	Ngo
Dinh	Dinh	k1gMnSc1	Dinh
Ziem	Ziem	k1gMnSc1	Ziem
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
stal	stát	k5eAaPmAgInS	stát
brzy	brzy	k6eAd1	brzy
velmi	velmi	k6eAd1	velmi
nepopulárním	populární	k2eNgInSc7d1	nepopulární
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
desetileté	desetiletý	k2eAgNnSc4d1	desetileté
účinkování	účinkování	k1gNnSc4	účinkování
na	na	k7c6	na
jihovietnamské	jihovietnamský	k2eAgFnSc6d1	jihovietnamská
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
ukončil	ukončit	k5eAaPmAgInS	ukončit
převrat	převrat	k1gInSc1	převrat
skupiny	skupina	k1gFnSc2	skupina
důstojníků	důstojník	k1gMnPc2	důstojník
ARVN	ARVN	kA	ARVN
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Diem	Diem	k1gInSc1	Diem
zabit	zabít	k5eAaPmNgInS	zabít
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
už	už	k6eAd1	už
16	[number]	k4	16
000	[number]	k4	000
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
vojenských	vojenský	k2eAgMnPc2d1	vojenský
poradců	poradce	k1gMnPc2	poradce
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
partyzány	partyzán	k1gMnPc7	partyzán
přímo	přímo	k6eAd1	přímo
neúčastnili	účastnit	k5eNaImAgMnP	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
komunistický	komunistický	k2eAgMnSc1d1	komunistický
vůdce	vůdce	k1gMnSc1	vůdce
Ho	on	k3xPp3gNnSc4	on
Či	či	k9wB	či
Min	min	kA	min
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
podporovat	podporovat	k5eAaImF	podporovat
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
boj	boj	k1gInSc4	boj
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
reprezentován	reprezentován	k2eAgInSc1d1	reprezentován
partyzánským	partyzánský	k2eAgNnSc7d1	partyzánské
hnutím	hnutí	k1gNnSc7	hnutí
Vietkong	Vietkonga	k1gFnPc2	Vietkonga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
Vietkong	Vietkong	k1gInSc1	Vietkong
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
většími	veliký	k2eAgFnPc7d2	veliký
operacemi	operace	k1gFnPc7	operace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ustavena	ustavit	k5eAaPmNgFnS	ustavit
Národní	národní	k2eAgFnSc1d1	národní
fronta	fronta	k1gFnSc1	fronta
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
kterou	který	k3yQgFnSc4	který
spadaly	spadat	k5eAaImAgFnP	spadat
úderné	úderný	k2eAgFnPc1d1	úderná
jednotky	jednotka	k1gFnPc1	jednotka
Vietkongu	Vietkong	k1gInSc2	Vietkong
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
fronta	fronta	k1gFnSc1	fronta
osvobození	osvobození	k1gNnSc2	osvobození
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
svržení	svržení	k1gNnSc4	svržení
vlády	vláda	k1gFnSc2	vláda
Jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
a	a	k8xC	a
o	o	k7c6	o
spojení	spojení	k1gNnSc6	spojení
celého	celý	k2eAgInSc2d1	celý
Vietnamu	Vietnam	k1gInSc2	Vietnam
pod	pod	k7c4	pod
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc4	útok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c4	v
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
zesilovaly	zesilovat	k5eAaImAgFnP	zesilovat
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zahájení	zahájení	k1gNnSc4	zahájení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tonkinský	tonkinský	k2eAgInSc4d1	tonkinský
incident	incident	k1gInSc4	incident
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jihovietnamské	jihovietnamský	k2eAgFnPc1d1	jihovietnamská
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
boj	boj	k1gInSc4	boj
s	s	k7c7	s
Vietkongem	Vietkong	k1gInSc7	Vietkong
zcela	zcela	k6eAd1	zcela
nepřipravené	připravený	k2eNgNnSc1d1	nepřipravené
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
záruce	záruka	k1gFnSc3	záruka
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přislíbil	přislíbit	k5eAaPmAgInS	přislíbit
pomoc	pomoc	k1gFnSc4	pomoc
udržet	udržet	k5eAaPmF	udržet
nezávislost	nezávislost	k1gFnSc4	nezávislost
Jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
narůstala	narůstat	k5eAaImAgFnS	narůstat
vojenská	vojenský	k2eAgFnSc1d1	vojenská
podpora	podpora	k1gFnSc1	podpora
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
se	se	k3xPyFc4	se
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
Francouzi	Francouz	k1gMnPc7	Francouz
-	-	kIx~	-
soustředili	soustředit	k5eAaPmAgMnP	soustředit
na	na	k7c4	na
vojenské	vojenský	k2eAgNnSc4d1	vojenské
řešení	řešení	k1gNnSc4	řešení
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
opomíjeli	opomíjet	k5eAaImAgMnP	opomíjet
politické	politický	k2eAgInPc4d1	politický
aspekty	aspekt	k1gInPc4	aspekt
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
mají	mít	k5eAaImIp3nP	mít
partyzáni	partyzán	k1gMnPc1	partyzán
podporu	podpor	k1gInSc2	podpor
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
malé	malý	k2eAgNnSc1d1	malé
jihovietnamské	jihovietnamský	k2eAgNnSc1d1	jihovietnamské
komando	komando	k1gNnSc1	komando
účastnilo	účastnit	k5eAaImAgNnS	účastnit
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zničení	zničení	k1gNnSc1	zničení
radarových	radarový	k2eAgFnPc2d1	radarová
stanic	stanice	k1gFnPc2	stanice
poblíž	poblíž	k7c2	poblíž
pobřeží	pobřeží	k1gNnSc2	pobřeží
Severního	severní	k2eAgInSc2d1	severní
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
pohraničních	pohraniční	k2eAgFnPc6d1	pohraniční
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
na	na	k7c6	na
malých	malý	k2eAgInPc6d1	malý
ostrovech	ostrov	k1gInPc6	ostrov
nedaleko	nedaleko	k7c2	nedaleko
severovietnamského	severovietnamský	k2eAgNnSc2d1	severovietnamský
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
údajně	údajně	k6eAd1	údajně
stále	stále	k6eAd1	stále
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
vodách	voda	k1gFnPc6	voda
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
i	i	k9	i
hlídkující	hlídkující	k2eAgInSc1d1	hlídkující
americký	americký	k2eAgInSc1d1	americký
torpédoborec	torpédoborec	k1gInSc1	torpédoborec
USS	USS	kA	USS
Maddox	Maddox	k1gInSc1	Maddox
(	(	kIx(	(
<g/>
DD-	DD-	k1gFnSc1	DD-
<g/>
731	[number]	k4	731
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Tonkinském	tonkinský	k2eAgInSc6d1	tonkinský
zálivu	záliv	k1gInSc6	záliv
napaden	napadnout	k5eAaPmNgInS	napadnout
třemi	tři	k4xCgInPc7	tři
torpédovými	torpédový	k2eAgInPc7d1	torpédový
čluny	člun	k1gInPc7	člun
severovietnamského	severovietnamský	k2eAgNnSc2d1	severovietnamský
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
americký	americký	k2eAgMnSc1d1	americký
torpédoborec	torpédoborec	k1gMnSc1	torpédoborec
USS	USS	kA	USS
Maddox	Maddox	k1gInSc1	Maddox
(	(	kIx(	(
<g/>
DD-	DD-	k1gFnSc1	DD-
<g/>
731	[number]	k4	731
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maddox	Maddox	k1gInSc1	Maddox
s	s	k7c7	s
leteckou	letecký	k2eAgFnSc7d1	letecká
podporou	podpora	k1gFnSc7	podpora
jeden	jeden	k4xCgInSc4	jeden
člun	člun	k1gInSc4	člun
potopil	potopit	k5eAaPmAgInS	potopit
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
poškodil	poškodit	k5eAaPmAgInS	poškodit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
sám	sám	k3xTgMnSc1	sám
neutrpěl	utrpět	k5eNaPmAgMnS	utrpět
žádné	žádný	k3yNgFnPc4	žádný
ztráty	ztráta	k1gFnPc4	ztráta
či	či	k8xC	či
zranění	zranění	k1gNnPc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Maddox	Maddox	k1gInSc1	Maddox
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
torpédoborcem	torpédoborec	k1gInSc7	torpédoborec
USS	USS	kA	USS
Turner	turner	k1gMnSc1	turner
Joy	Joy	k1gMnSc1	Joy
(	(	kIx(	(
<g/>
DD-	DD-	k1gFnSc1	DD-
<g/>
951	[number]	k4	951
<g/>
)	)	kIx)	)
zhruba	zhruba	k6eAd1	zhruba
dvě	dva	k4xCgFnPc1	dva
hodiny	hodina	k1gFnPc1	hodina
ostřelovaly	ostřelovat	k5eAaImAgFnP	ostřelovat
radarové	radarový	k2eAgInPc4d1	radarový
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
považovaly	považovat	k5eAaImAgFnP	považovat
za	za	k7c4	za
severovietnamské	severovietnamský	k2eAgInPc4d1	severovietnamský
torpédové	torpédový	k2eAgInPc4d1	torpédový
čluny	člun	k1gInPc4	člun
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
šlo	jít	k5eAaImAgNnS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
rušení	rušení	k1gNnSc6	rušení
bouřkou	bouřka	k1gFnSc7	bouřka
a	a	k8xC	a
mylné	mylný	k2eAgNnSc1d1	mylné
vyhodnocení	vyhodnocení	k1gNnSc4	vyhodnocení
signálů	signál	k1gInPc2	signál
radaru	radar	k1gInSc2	radar
<g/>
.	.	kIx.	.
</s>
<s>
Vietnam	Vietnam	k1gInSc1	Vietnam
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akci	akce	k1gFnSc4	akce
dodnes	dodnes	k6eAd1	dodnes
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podle	podle	k7c2	podle
nedávno	nedávno	k6eAd1	nedávno
odtajněných	odtajněný	k2eAgInPc2d1	odtajněný
amerických	americký	k2eAgInPc2d1	americký
materiálů	materiál	k1gInPc2	materiál
byl	být	k5eAaImAgInS	být
incident	incident	k1gInSc1	incident
záměrně	záměrně	k6eAd1	záměrně
vymyšlen	vymyslet	k5eAaPmNgInS	vymyslet
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
americkým	americký	k2eAgMnSc7d1	americký
ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
McNamarou	McNamara	k1gFnSc7	McNamara
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Lyndon	Lyndon	k1gMnSc1	Lyndon
Johnson	Johnson	k1gMnSc1	Johnson
údajně	údajně	k6eAd1	údajně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
v	v	k7c6	v
soukromí	soukromí	k1gNnSc6	soukromí
přiznal	přiznat	k5eAaPmAgInS	přiznat
pochybení	pochybení	k1gNnSc4	pochybení
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
pokud	pokud	k8xS	pokud
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
střílelo	střílet	k5eAaImAgNnS	střílet
na	na	k7c4	na
velryby	velryba	k1gFnPc4	velryba
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
v	v	k7c4	v
USA	USA	kA	USA
tajné	tajný	k2eAgInPc1d1	tajný
dokumenty	dokument	k1gInPc1	dokument
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
útoku	útok	k1gInSc6	útok
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byly	být	k5eAaImAgFnP	být
zkreslené	zkreslený	k2eAgFnPc1d1	zkreslená
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
nejprve	nejprve	k6eAd1	nejprve
varoval	varovat	k5eAaImAgMnS	varovat
Hanoj	Hanoj	k1gFnSc4	Hanoj
před	před	k7c7	před
nevyprovokovanými	vyprovokovaný	k2eNgInPc7d1	nevyprovokovaný
útoky	útok	k1gInPc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
incidentu	incident	k1gInSc6	incident
schválil	schválit	k5eAaPmAgInS	schválit
jednorázové	jednorázový	k2eAgInPc4d1	jednorázový
letecké	letecký	k2eAgInPc4d1	letecký
údery	úder	k1gInPc4	úder
na	na	k7c4	na
cíle	cíl	k1gInPc4	cíl
v	v	k7c6	v
Severním	severní	k2eAgInSc6d1	severní
Vietnamu	Vietnam	k1gInSc6	Vietnam
a	a	k8xC	a
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
k	k	k7c3	k
národu	národ	k1gInSc3	národ
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
tento	tento	k3xDgInSc4	tento
druhý	druhý	k4xOgInSc4	druhý
incident	incident	k1gInSc4	incident
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
sporný	sporný	k2eAgInSc1d1	sporný
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
odvetných	odvetný	k2eAgInPc2d1	odvetný
náletů	nálet	k1gInPc2	nálet
na	na	k7c4	na
severovietnamská	severovietnamský	k2eAgNnPc4d1	severovietnamský
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
podnikly	podniknout	k5eAaPmAgInP	podniknout
první	první	k4xOgNnSc4	první
nálet	nálet	k1gInSc4	nálet
letecké	letecký	k2eAgFnSc2d1	letecká
síly	síla	k1gFnSc2	síla
jihovietnamského	jihovietnamský	k2eAgNnSc2d1	jihovietnamské
letectva	letectvo	k1gNnSc2	letectvo
společně	společně	k6eAd1	společně
s	s	k7c7	s
americkými	americký	k2eAgNnPc7d1	americké
letadly	letadlo	k1gNnPc7	letadlo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
startovaly	startovat	k5eAaBmAgFnP	startovat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
letadlových	letadlový	k2eAgFnPc2d1	letadlová
lodí	loď	k1gFnPc2	loď
v	v	k7c6	v
Jihočínském	jihočínský	k2eAgNnSc6d1	Jihočínské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
schválil	schválit	k5eAaPmAgInS	schválit
Kongres	kongres	k1gInSc1	kongres
USA	USA	kA	USA
rezoluci	rezoluce	k1gFnSc4	rezoluce
o	o	k7c6	o
Tonkinském	tonkinský	k2eAgInSc6d1	tonkinský
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prezidenta	prezident	k1gMnSc4	prezident
opravňovala	opravňovat	k5eAaImAgFnS	opravňovat
k	k	k7c3	k
"	"	kIx"	"
<g/>
použití	použití	k1gNnSc3	použití
síly	síla	k1gFnSc2	síla
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
obraně	obrana	k1gFnSc3	obrana
svobody	svoboda	k1gFnSc2	svoboda
kteréhokoli	kterýkoli	k3yIgInSc2	kterýkoli
státu	stát	k1gInSc2	stát
SEATO	SEATO	kA	SEATO
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
boje	boj	k1gInPc1	boj
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
pomalu	pomalu	k6eAd1	pomalu
stupňovaly	stupňovat	k5eAaImAgInP	stupňovat
<g/>
,	,	kIx,	,
přešlo	přejít	k5eAaPmAgNnS	přejít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
Ho-Či-Minovou	Ho-Či-Minový	k2eAgFnSc7d1	Ho-Či-Minový
stezkou	stezka	k1gFnSc7	stezka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
NVA	NVA	kA	NVA
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podpořili	podpořit	k5eAaPmAgMnP	podpořit
partyzánské	partyzánský	k2eAgFnPc4d1	Partyzánská
jednotky	jednotka	k1gFnPc4	jednotka
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Ia	ia	k0	ia
Drang	Drang	k1gInSc1	Drang
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
už	už	k6eAd1	už
170	[number]	k4	170
000	[number]	k4	000
partyzánů	partyzán	k1gMnPc2	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
pochopili	pochopit	k5eAaPmAgMnP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
nemají	mít	k5eNaImIp3nP	mít
čas	čas	k1gInSc4	čas
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nechopí	chopit	k5eNaPmIp3nS	chopit
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
Jih	jih	k1gInSc1	jih
plný	plný	k2eAgInSc1d1	plný
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
plán	plán	k1gInSc1	plán
útoku	útok	k1gInSc2	útok
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
z	z	k7c2	z
hornaté	hornatý	k2eAgFnSc2d1	hornatá
oblasti	oblast	k1gFnSc2	oblast
Centrální	centrální	k2eAgFnSc2d1	centrální
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
už	už	k6eAd1	už
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
mořského	mořský	k2eAgNnSc2d1	mořské
pobřeží	pobřeží	k1gNnSc2	pobřeží
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
Jižní	jižní	k2eAgInSc1d1	jižní
Vietnam	Vietnam	k1gInSc1	Vietnam
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
a	a	k8xC	a
zbylá	zbylý	k2eAgNnPc1d1	zbylé
ohniska	ohnisko	k1gNnPc1	ohnisko
odporu	odpor	k1gInSc2	odpor
potom	potom	k8xC	potom
lehce	lehko	k6eAd1	lehko
zlikvidují	zlikvidovat	k5eAaPmIp3nP	zlikvidovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
americké	americký	k2eAgFnPc1d1	americká
bojové	bojový	k2eAgFnPc1d1	bojová
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
dorazily	dorazit	k5eAaPmAgInP	dorazit
do	do	k7c2	do
Vietnamu	Vietnam	k1gInSc2	Vietnam
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
oddíly	oddíl	k1gInPc1	oddíl
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zajistily	zajistit	k5eAaPmAgFnP	zajistit
důležitou	důležitý	k2eAgFnSc4d1	důležitá
oblast	oblast	k1gFnSc4	oblast
letiště	letiště	k1gNnSc2	letiště
Da	Da	k1gFnSc2	Da
Nang	Nanga	k1gFnPc2	Nanga
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
už	už	k6eAd1	už
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
nacházely	nacházet	k5eAaImAgFnP	nacházet
jednotky	jednotka	k1gFnPc1	jednotka
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
zatím	zatím	k6eAd1	zatím
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
odstartovali	odstartovat	k5eAaPmAgMnP	odstartovat
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
,	,	kIx,	,
když	když	k8xS	když
rozdrtili	rozdrtit	k5eAaPmAgMnP	rozdrtit
jednotky	jednotka	k1gFnSc2	jednotka
ARVN	ARVN	kA	ARVN
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Phuoc	Phuoc	k1gFnSc4	Phuoc
Long	Long	k1gMnSc1	Long
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Saigonu	Saigon	k1gInSc2	Saigon
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
poměrně	poměrně	k6eAd1	poměrně
dobrý	dobrý	k2eAgInSc1d1	dobrý
plán	plán	k1gInSc1	plán
ale	ale	k9	ale
spoléhal	spoléhat	k5eAaImAgInS	spoléhat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
nebudou	být	k5eNaImBp3nP	být
Američané	Američan	k1gMnPc1	Američan
klást	klást	k5eAaImF	klást
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgNnSc1d1	americké
velení	velení	k1gNnSc1	velení
se	se	k3xPyFc4	se
však	však	k9	však
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
energicky	energicky	k6eAd1	energicky
obklíčit	obklíčit	k5eAaPmF	obklíčit
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
právě	právě	k6eAd1	právě
přišly	přijít	k5eAaPmAgFnP	přijít
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
oblasti	oblast	k1gFnSc2	oblast
Centrální	centrální	k2eAgFnSc2d1	centrální
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
vyskytovalo	vyskytovat	k5eAaImAgNnS	vyskytovat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mnoho	mnoho	k6eAd1	mnoho
partyzánů	partyzán	k1gMnPc2	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
nastávajících	nastávající	k2eAgInPc6d1	nastávající
bojích	boj	k1gInPc6	boj
měla	mít	k5eAaImAgFnS	mít
sehrát	sehrát	k5eAaPmF	sehrát
1	[number]	k4	1
<g/>
.	.	kIx.	.
jízdní	jízdní	k2eAgFnSc2d1	jízdní
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
nově	nově	k6eAd1	nově
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosud	dosud	k6eAd1	dosud
neměla	mít	k5eNaImAgFnS	mít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Jejích	její	k3xOp3gInPc2	její
440	[number]	k4	440
vrtulníků	vrtulník	k1gInPc2	vrtulník
bylo	být	k5eAaImAgNnS	být
schopných	schopný	k2eAgFnPc2d1	schopná
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednoho	jeden	k4xCgInSc2	jeden
výsadku	výsadek	k1gInSc2	výsadek
přemístit	přemístit	k5eAaPmF	přemístit
až	až	k9	až
2	[number]	k4	2
500	[number]	k4	500
plně	plně	k6eAd1	plně
vyzbrojených	vyzbrojený	k2eAgMnPc2d1	vyzbrojený
pěšáků	pěšák	k1gMnPc2	pěšák
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byla	být	k5eAaImAgFnS	být
vyslána	vyslán	k2eAgFnSc1d1	vyslána
7	[number]	k4	7
<g/>
.	.	kIx.	.
kavalérie	kavalérie	k1gFnSc1	kavalérie
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
plukovníka	plukovník	k1gMnSc2	plukovník
Moora	Moor	k1gMnSc2	Moor
zajistit	zajistit	k5eAaPmF	zajistit
údolí	údolí	k1gNnSc4	údolí
Ia	ia	k0	ia
Drang	Drang	k1gInSc4	Drang
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
vypínal	vypínat	k5eAaImAgMnS	vypínat
hraniční	hraniční	k2eAgInSc4d1	hraniční
kopec	kopec	k1gInSc4	kopec
Chu	Chu	k1gMnSc1	Chu
Pon	Pon	k1gMnSc1	Pon
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
netušili	tušit	k5eNaImAgMnP	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kopec	kopec	k1gInSc1	kopec
protkaný	protkaný	k2eAgInSc1d1	protkaný
sítí	síť	k1gFnSc7	síť
tunelů	tunel	k1gInPc2	tunel
je	být	k5eAaImIp3nS	být
základnou	základna	k1gFnSc7	základna
VLA	VLA	kA	VLA
<g/>
.	.	kIx.	.
</s>
<s>
Severovietnamci	Severovietnamec	k1gMnPc1	Severovietnamec
byli	být	k5eAaImAgMnP	být
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
desetinásobné	desetinásobný	k2eAgFnSc6d1	desetinásobná
početní	početní	k2eAgFnSc6d1	početní
převaze	převaha	k1gFnSc6	převaha
<g/>
,	,	kIx,	,
pustili	pustit	k5eAaPmAgMnP	pustit
se	se	k3xPyFc4	se
neohroženě	ohroženě	k6eNd1	ohroženě
do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
kavalérie	kavalérie	k1gFnSc1	kavalérie
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
obklíčení	obklíčení	k1gNnSc2	obklíčení
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vytrvale	vytrvale	k6eAd1	vytrvale
se	se	k3xPyFc4	se
bránila	bránit	k5eAaImAgFnS	bránit
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Ranění	raněný	k2eAgMnPc1d1	raněný
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
byli	být	k5eAaImAgMnP	být
odsouváni	odsouvat	k5eAaImNgMnP	odsouvat
vrtulníky	vrtulník	k1gInPc1	vrtulník
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
též	též	k6eAd1	též
přivážely	přivážet	k5eAaImAgFnP	přivážet
munici	munice	k1gFnSc4	munice
a	a	k8xC	a
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
vojákům	voják	k1gMnPc3	voják
krytí	krytí	k1gNnSc2	krytí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
obklíčeným	obklíčený	k2eAgFnPc3d1	obklíčená
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
i	i	k9	i
dělostřelectvo	dělostřelectvo	k1gNnSc1	dělostřelectvo
a	a	k8xC	a
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
,	,	kIx,	,
strategické	strategický	k2eAgInPc1d1	strategický
bombardéry	bombardér	k1gInPc1	bombardér
B-52	B-52	k1gMnPc2	B-52
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
přímou	přímý	k2eAgFnSc4d1	přímá
podporu	podpora	k1gFnSc4	podpora
jednotek	jednotka	k1gFnPc2	jednotka
použity	použít	k5eAaPmNgInP	použít
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
Severovietnamci	Severovietnamec	k1gMnPc1	Severovietnamec
si	se	k3xPyFc3	se
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohou	moct	k5eNaImIp3nP	moct
nepřítele	nepřítel	k1gMnSc4	nepřítel
porazit	porazit	k5eAaPmF	porazit
a	a	k8xC	a
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
se	se	k3xPyFc4	se
do	do	k7c2	do
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	on	k3xPp3gNnPc4	on
Američané	Američan	k1gMnPc1	Američan
nesměli	smět	k5eNaImAgMnP	smět
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
a	a	k8xC	a
pronásledování	pronásledování	k1gNnSc2	pronásledování
ukončili	ukončit	k5eAaPmAgMnP	ukončit
na	na	k7c6	na
vietnamsko-kambodžských	vietnamskoambodžský	k2eAgFnPc6d1	vietnamsko-kambodžský
hranicích	hranice	k1gFnPc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Padlo	padnout	k5eAaImAgNnS	padnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
500	[number]	k4	500
Severovietnamců	Severovietnamec	k1gMnPc2	Severovietnamec
<g/>
,	,	kIx,	,
Američané	Američan	k1gMnPc1	Američan
ztratili	ztratit	k5eAaPmAgMnP	ztratit
229	[number]	k4	229
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
250	[number]	k4	250
raněných	raněný	k1gMnPc2	raněný
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
tímto	tento	k3xDgInSc7	tento
útokem	útok	k1gInSc7	útok
zastavili	zastavit	k5eAaPmAgMnP	zastavit
postup	postup	k1gInSc4	postup
partyzánů	partyzán	k1gMnPc2	partyzán
a	a	k8xC	a
VLA	VLA	kA	VLA
Centrální	centrální	k2eAgFnSc7d1	centrální
vysočinou	vysočina	k1gFnSc7	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Ia	ia	k0	ia
Drang	Dranga	k1gFnPc2	Dranga
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
námětem	námět	k1gInSc7	námět
filmu	film	k1gInSc2	film
Údolí	údolí	k1gNnSc2	údolí
stínů	stín	k1gInPc2	stín
(	(	kIx(	(
<g/>
v	v	k7c4	v
orig	orig	k1gInSc4	orig
<g/>
.	.	kIx.	.
</s>
<s>
We	We	k?	We
Were	Were	k1gInSc1	Were
Soldiers	Soldiers	k1gInSc1	Soldiers
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
scénář	scénář	k1gInSc1	scénář
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
vzpomínkové	vzpomínkový	k2eAgFnSc2d1	vzpomínková
knihy	kniha	k1gFnSc2	kniha
přímých	přímý	k2eAgMnPc2d1	přímý
účastníků	účastník	k1gMnPc2	účastník
střetu	střet	k1gInSc2	střet
<g/>
.	.	kIx.	.
</s>
<s>
Dílčí	dílčí	k2eAgInPc4d1	dílčí
úspěchy	úspěch	k1gInPc4	úspěch
ale	ale	k8xC	ale
neznamenaly	znamenat	k5eNaImAgFnP	znamenat
konečné	konečný	k2eAgNnSc4d1	konečné
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ARVN	ARVN	kA	ARVN
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rozkladu	rozklad	k1gInSc6	rozklad
a	a	k8xC	a
polovinu	polovina	k1gFnSc4	polovina
venkovských	venkovský	k2eAgFnPc2d1	venkovská
oblastí	oblast	k1gFnPc2	oblast
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
partyzáni	partyzán	k1gMnPc1	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Westmoreland	Westmoreland	k1gInSc1	Westmoreland
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velitelem	velitel	k1gMnSc7	velitel
amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřátelům	nepřítel	k1gMnPc3	nepřítel
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
nechat	nechat	k5eAaPmF	nechat
čas	čas	k1gInSc4	čas
na	na	k7c4	na
obnovení	obnovení	k1gNnSc4	obnovení
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
vedl	vést	k5eAaImAgMnS	vést
agresivní	agresivní	k2eAgFnPc4d1	agresivní
útočné	útočný	k2eAgFnPc4d1	útočná
operace	operace	k1gFnPc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
000	[number]	k4	000
amerických	americký	k2eAgMnPc2d1	americký
<g/>
,	,	kIx,	,
50	[number]	k4	50
000	[number]	k4	000
jihokorejských	jihokorejský	k2eAgMnPc2d1	jihokorejský
a	a	k8xC	a
australských	australský	k2eAgMnPc2d1	australský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
boji	boj	k1gInPc7	boj
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
snažil	snažit	k5eAaImAgMnS	snažit
vyhnat	vyhnat	k5eAaPmF	vyhnat
partyzány	partyzána	k1gFnPc4	partyzána
a	a	k8xC	a
jednotky	jednotka	k1gFnPc4	jednotka
VLA	VLA	kA	VLA
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
probíhaly	probíhat	k5eAaImAgFnP	probíhat
i	i	k9	i
nálety	nálet	k1gInPc4	nálet
na	na	k7c4	na
Severní	severní	k2eAgInSc4d1	severní
Vietnam	Vietnam	k1gInSc4	Vietnam
a	a	k8xC	a
v	v	k7c6	v
podobné	podobný	k2eAgFnSc6d1	podobná
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
na	na	k7c6	na
pohraniční	pohraniční	k2eAgFnSc6d1	pohraniční
oblasti	oblast	k1gFnSc6	oblast
neutrální	neutrální	k2eAgFnSc2d1	neutrální
Kambodže	Kambodža	k1gFnSc2	Kambodža
a	a	k8xC	a
Laosu	Laos	k1gInSc2	Laos
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
vedla	vést	k5eAaImAgFnS	vést
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
Ho-Či-Minova	Ho-Či-Minův	k2eAgFnSc1d1	Ho-Či-Minův
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Bombardování	bombardování	k1gNnSc1	bombardování
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
vedeno	vést	k5eAaImNgNnS	vést
neefektivním	efektivní	k2eNgInSc7d1	neefektivní
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Vojenští	vojenský	k2eAgMnPc1d1	vojenský
experti	expert	k1gMnPc1	expert
a	a	k8xC	a
poradci	poradce	k1gMnPc1	poradce
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
totiž	totiž	k9	totiž
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
množství	množství	k1gNnSc4	množství
omezení	omezení	k1gNnSc2	omezení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nevyprovokovali	vyprovokovat	k5eNaPmAgMnP	vyprovokovat
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
-	-	kIx~	-
neútočilo	útočit	k5eNaImAgNnS	útočit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
civilní	civilní	k2eAgInPc4d1	civilní
cíle	cíl	k1gInPc4	cíl
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
protileteckých	protiletecký	k2eAgFnPc2d1	protiletecká
zbraní	zbraň	k1gFnPc2	zbraň
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
měla	mít	k5eAaImAgFnS	mít
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
nejdokonalejší	dokonalý	k2eAgFnSc4d3	nejdokonalejší
protiletadlovou	protiletadlový	k2eAgFnSc4d1	protiletadlová
obranu	obrana	k1gFnSc4	obrana
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nálety	nálet	k1gInPc1	nálet
však	však	k9	však
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
8	[number]	k4	8
let	léto	k1gNnPc2	léto
přinesly	přinést	k5eAaPmAgFnP	přinést
smrt	smrt	k1gFnSc4	smrt
100	[number]	k4	100
000	[number]	k4	000
civilistům	civilista	k1gMnPc3	civilista
a	a	k8xC	a
způsobily	způsobit	k5eAaPmAgFnP	způsobit
obrovské	obrovský	k2eAgFnPc4d1	obrovská
materiální	materiální	k2eAgFnPc4d1	materiální
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obrovským	obrovský	k2eAgFnPc3d1	obrovská
ztrátám	ztráta	k1gFnPc3	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
došlo	dojít	k5eAaPmAgNnS	dojít
ovšem	ovšem	k9	ovšem
také	také	k9	také
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1967	[number]	k4	1967
obklíčily	obklíčit	k5eAaPmAgFnP	obklíčit
jednotky	jednotka	k1gFnPc1	jednotka
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
jihovietnamské	jihovietnamský	k2eAgFnSc2d1	jihovietnamská
armády	armáda	k1gFnSc2	armáda
oblast	oblast	k1gFnSc1	oblast
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
železného	železný	k2eAgInSc2d1	železný
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
"	"	kIx"	"
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Saigonu	Saigon	k1gInSc2	Saigon
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
Američané	Američan	k1gMnPc1	Američan
snažili	snažit	k5eAaImAgMnP	snažit
marně	marně	k6eAd1	marně
dobýt	dobýt	k5eAaPmF	dobýt
už	už	k6eAd1	už
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
opevněné	opevněný	k2eAgFnSc6d1	opevněná
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
ukrývalo	ukrývat	k5eAaImAgNnS	ukrývat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
partyzánů	partyzán	k1gMnPc2	partyzán
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
odtud	odtud	k6eAd1	odtud
často	často	k6eAd1	často
podnikány	podnikán	k2eAgInPc4d1	podnikán
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
Saigonu	Saigon	k1gInSc3	Saigon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
jihovietnamské	jihovietnamský	k2eAgFnSc2d1	jihovietnamská
jednotky	jednotka	k1gFnSc2	jednotka
oblast	oblast	k1gFnSc4	oblast
obsadily	obsadit	k5eAaPmAgFnP	obsadit
<g/>
,	,	kIx,	,
padlo	padnout	k5eAaPmAgNnS	padnout
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
78	[number]	k4	78
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
asi	asi	k9	asi
700	[number]	k4	700
partyzánů	partyzán	k1gMnPc2	partyzán
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
400	[number]	k4	400
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
zajato	zajmout	k5eAaPmNgNnS	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Nečekaný	čekaný	k2eNgInSc1d1	nečekaný
útok	útok	k1gInSc1	útok
sice	sice	k8xC	sice
partyzány	partyzán	k1gMnPc7	partyzán
zaskočil	zaskočit	k5eAaPmAgInS	zaskočit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ti	ten	k3xDgMnPc1	ten
neztratili	ztratit	k5eNaPmAgMnP	ztratit
duchapřítomnost	duchapřítomnost	k1gFnSc4	duchapřítomnost
a	a	k8xC	a
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
asi	asi	k9	asi
měsíc	měsíc	k1gInSc4	měsíc
prozkoumávaly	prozkoumávat	k5eAaImAgInP	prozkoumávat
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
ničily	ničit	k5eAaImAgInP	ničit
bunkry	bunkr	k1gInPc4	bunkr
a	a	k8xC	a
sklady	sklad	k1gInPc4	sklad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
odešly	odejít	k5eAaPmAgFnP	odejít
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dovolily	dovolit	k5eAaPmAgFnP	dovolit
partyzánům	partyzán	k1gMnPc3	partyzán
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
navrhly	navrhnout	k5eAaPmAgFnP	navrhnout
USA	USA	kA	USA
vietnamské	vietnamský	k2eAgFnSc6d1	vietnamská
straně	strana	k1gFnSc6	strana
jednání	jednání	k1gNnSc2	jednání
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
je	on	k3xPp3gMnPc4	on
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
obnovily	obnovit	k5eAaPmAgInP	obnovit
nálety	nálet	k1gInPc1	nálet
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pomoc	pomoc	k1gFnSc4	pomoc
Jižnímu	jižní	k2eAgInSc3d1	jižní
Vietnamu	Vietnam	k1gInSc3	Vietnam
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
i	i	k9	i
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyslalo	vyslat	k5eAaPmAgNnS	vyslat
3	[number]	k4	3
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
svoje	svůj	k3xOyFgFnPc4	svůj
letecké	letecký	k2eAgFnPc4d1	letecká
základny	základna	k1gFnPc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
rozmohly	rozmoct	k5eAaPmAgInP	rozmoct
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
hippies	hippies	k1gInSc4	hippies
pacifistická	pacifistický	k2eAgNnPc4d1	pacifistické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
ostře	ostro	k6eAd1	ostro
proti	proti	k7c3	proti
veškerým	veškerý	k3xTgInPc3	veškerý
konfliktům	konflikt	k1gInPc3	konflikt
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
proti	proti	k7c3	proti
vietnamské	vietnamský	k2eAgFnSc3d1	vietnamská
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
postupně	postupně	k6eAd1	postupně
pomáhalo	pomáhat	k5eAaImAgNnS	pomáhat
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
mezi	mezi	k7c7	mezi
mládeží	mládež	k1gFnSc7	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zesílil	zesílit	k5eAaPmAgMnS	zesílit
nejvíce	nejvíce	k6eAd1	nejvíce
po	po	k7c6	po
masakru	masakr	k1gInSc6	masakr
v	v	k7c6	v
My-lai	Mya	k1gInSc6	My-la
z	z	k7c2	z
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
brutálně	brutálně	k6eAd1	brutálně
znásilnili	znásilnit	k5eAaPmAgMnP	znásilnit
<g/>
,	,	kIx,	,
umučili	umučit	k5eAaPmAgMnP	umučit
a	a	k8xC	a
vyvraždili	vyvraždit	k5eAaPmAgMnP	vyvraždit
mezi	mezi	k7c7	mezi
čtyřmi	čtyři	k4xCgNnPc7	čtyři
a	a	k8xC	a
pěti	pět	k4xCc3	pět
sty	sto	k4xCgNnPc7	sto
obyvateli	obyvatel	k1gMnPc7	obyvatel
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Vietnamu	Vietnam	k1gInSc2	Vietnam
bylo	být	k5eAaImAgNnS	být
posíláno	posílat	k5eAaImNgNnS	posílat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
jich	on	k3xPp3gNnPc2	on
tam	tam	k6eAd1	tam
bylo	být	k5eAaImAgNnS	být
463	[number]	k4	463
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
tvořili	tvořit	k5eAaImAgMnP	tvořit
povolaní	povolaný	k2eAgMnPc1d1	povolaný
branci	branec	k1gMnPc1	branec
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
a	a	k8xC	a
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
několik	několik	k4yIc1	několik
velkých	velký	k2eAgFnPc2d1	velká
demonstrací	demonstrace	k1gFnPc2	demonstrace
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
rozsáhlých	rozsáhlý	k2eAgNnPc2d1	rozsáhlé
shromáždění	shromáždění	k1gNnPc2	shromáždění
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgInP	účastnit
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
lidí	člověk	k1gMnPc2	člověk
<g/>
;	;	kIx,	;
mnohdy	mnohdy	k6eAd1	mnohdy
až	až	k9	až
statisíce	statisíce	k1gInPc1	statisíce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Američanů	Američan	k1gMnPc2	Američan
začala	začít	k5eAaPmAgFnS	začít
válku	válka	k1gFnSc4	válka
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
chybu	chyba	k1gFnSc4	chyba
a	a	k8xC	a
žádala	žádat	k5eAaImAgFnS	žádat
její	její	k3xOp3gNnSc4	její
rychlé	rychlý	k2eAgNnSc4d1	rychlé
ukončení	ukončení	k1gNnSc4	ukončení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
demonstracích	demonstrace	k1gFnPc6	demonstrace
před	před	k7c7	před
Bílým	bílý	k2eAgInSc7d1	bílý
domem	dům	k1gInSc7	dům
protestující	protestující	k2eAgFnSc1d1	protestující
mládež	mládež	k1gFnSc1	mládež
mávala	mávat	k5eAaImAgFnS	mávat
i	i	k8xC	i
vlajkami	vlajka	k1gFnPc7	vlajka
Vietkongu	Vietkong	k1gInSc2	Vietkong
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
krajany	krajan	k1gMnPc4	krajan
bojující	bojující	k2eAgMnPc4d1	bojující
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
označovala	označovat	k5eAaImAgFnS	označovat
za	za	k7c4	za
válečné	válečný	k2eAgMnPc4d1	válečný
zločince	zločinec	k1gMnPc4	zločinec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Převažovala	převažovat	k5eAaImAgFnS	převažovat
však	však	k9	však
hesla	heslo	k1gNnPc4	heslo
žádající	žádající	k2eAgInSc4d1	žádající
mír	mír	k1gInSc4	mír
a	a	k8xC	a
stažení	stažení	k1gNnSc4	stažení
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Draft	draft	k1gInSc1	draft
Beer	Beera	k1gFnPc2	Beera
<g/>
,	,	kIx,	,
not	nota	k1gFnPc2	nota
boys	boy	k1gMnPc1	boy
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hell	Hell	k1gMnSc1	Hell
no	no	k9	no
<g/>
,	,	kIx,	,
we	we	k?	we
won	won	k1gInSc1	won
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
go	go	k?	go
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Make	Make	k1gFnSc1	Make
love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
not	nota	k1gFnPc2	nota
war	war	k?	war
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Eighteen	Eighteen	k1gInSc1	Eighteen
today	todaa	k1gFnSc2	todaa
<g/>
,	,	kIx,	,
dead	dead	k6eAd1	dead
tomorrow	tomorrow	k?	tomorrow
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
"	"	kIx"	"
<g/>
LBJ	LBJ	kA	LBJ
-	-	kIx~	-
pull	pull	k1gMnSc1	pull
out	out	k?	out
like	like	k1gInSc1	like
your	your	k1gMnSc1	your
old	old	k?	old
man	man	k1gMnSc1	man
should	should	k6eAd1	should
have	havat	k5eAaPmIp3nS	havat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Právě	právě	k6eAd1	právě
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc2	vliv
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
nátlaku	nátlak	k1gInSc3	nátlak
veřejnosti	veřejnost	k1gFnSc2	veřejnost
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
ukončení	ukončení	k1gNnSc1	ukončení
války	válka	k1gFnSc2	válka
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
urychleno	urychlen	k2eAgNnSc1d1	urychleno
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mediální	mediální	k2eAgMnSc1d1	mediální
teoretik	teoretik	k1gMnSc1	teoretik
Marshall	Marshall	k1gMnSc1	Marshall
McLuhan	McLuhan	k1gMnSc1	McLuhan
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
dokonce	dokonce	k9	dokonce
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Televize	televize	k1gFnSc1	televize
přenesla	přenést	k5eAaPmAgFnS	přenést
brutalitu	brutalita	k1gFnSc4	brutalita
války	válka	k1gFnSc2	válka
do	do	k7c2	do
pohodlí	pohodlí	k1gNnSc2	pohodlí
obývacího	obývací	k2eAgInSc2d1	obývací
pokoje	pokoj	k1gInSc2	pokoj
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pentagon	Pentagon	k1gInSc1	Pentagon
Papers	Papers	k1gInSc1	Papers
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1967	[number]	k4	1967
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Robert	Robert	k1gMnSc1	Robert
McNamara	McNamar	k1gMnSc4	McNamar
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tajnou	tajný	k2eAgFnSc4d1	tajná
36	[number]	k4	36
<g/>
člennou	členný	k2eAgFnSc4d1	členná
skupinu	skupina	k1gFnSc4	skupina
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
jak	jak	k6eAd1	jak
vojáky	voják	k1gMnPc7	voják
tak	tak	k6eAd1	tak
akademickými	akademický	k2eAgMnPc7d1	akademický
historiky	historik	k1gMnPc7	historik
a	a	k8xC	a
analytiky	analytik	k1gMnPc7	analytik
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
popsat	popsat	k5eAaPmF	popsat
Vietnam	Vietnam	k1gInSc4	Vietnam
od	od	k7c2	od
konce	konec	k1gInSc2	konec
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
půlroční	půlroční	k2eAgFnSc2d1	půlroční
snahy	snaha	k1gFnSc2	snaha
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
cca	cca	kA	cca
7000	[number]	k4	7000
<g/>
stranná	stranný	k2eAgFnSc1d1	Stranná
kompilace	kompilace	k1gFnSc1	kompilace
předchozích	předchozí	k2eAgInPc2d1	předchozí
vládních	vládní	k2eAgInPc2d1	vládní
dokumentů	dokument	k1gInPc2	dokument
i	i	k8xC	i
původních	původní	k2eAgFnPc2d1	původní
analýz	analýza	k1gFnPc2	analýza
<g/>
,	,	kIx,	,
označená	označený	k2eAgFnSc1d1	označená
nálepkou	nálepka	k1gFnSc7	nálepka
"	"	kIx"	"
<g/>
citlivé	citlivý	k2eAgInPc1d1	citlivý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
dostal	dostat	k5eAaPmAgMnS	dostat
i	i	k9	i
Daniel	Daniel	k1gMnSc1	Daniel
Ellsberg	Ellsberg	k1gMnSc1	Ellsberg
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
měsíců	měsíc	k1gInPc2	měsíc
podařilo	podařit	k5eAaPmAgNnS	podařit
většinu	většina	k1gFnSc4	většina
materiálů	materiál	k1gInPc2	materiál
okopírovat	okopírovat	k5eAaPmF	okopírovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
dosáhnul	dosáhnout	k5eAaPmAgMnS	dosáhnout
jejich	jejich	k3xOp3gNnSc4	jejich
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohli	moct	k5eAaImAgMnP	moct
dozvědět	dozvědět	k5eAaPmF	dozvědět
o	o	k7c6	o
záměrné	záměrný	k2eAgFnSc6d1	záměrná
eskalaci	eskalace	k1gFnSc6	eskalace
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
jejich	jejich	k3xOp3gFnSc2	jejich
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
záměrného	záměrný	k2eAgNnSc2d1	záměrné
rozšíření	rozšíření	k1gNnSc2	rozšíření
války	válka	k1gFnSc2	válka
do	do	k7c2	do
Laosu	Laos	k1gInSc2	Laos
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
všechny	všechen	k3xTgFnPc4	všechen
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
administrativy	administrativa	k1gFnPc4	administrativa
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
Vietnamem	Vietnam	k1gInSc7	Vietnam
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
či	či	k8xC	či
oné	onen	k3xDgFnSc3	onen
formě	forma	k1gFnSc3	forma
lhaly	lhát	k5eAaImAgInP	lhát
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
Tet	Teta	k1gFnPc2	Teta
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
rozporuplné	rozporuplný	k2eAgFnSc6d1	rozporuplná
době	doba	k1gFnSc6	doba
začalo	začít	k5eAaPmAgNnS	začít
Hanojské	hanojský	k2eAgNnSc1d1	hanojské
politbyro	politbyro	k1gNnSc1	politbyro
plánovat	plánovat	k5eAaImF	plánovat
novou	nový	k2eAgFnSc4d1	nová
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
vyhnala	vyhnat	k5eAaPmAgFnS	vyhnat
Američany	Američan	k1gMnPc4	Američan
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Severovietnamci	Severovietnamec	k1gMnPc1	Severovietnamec
naplánovali	naplánovat	k5eAaBmAgMnP	naplánovat
novou	nový	k2eAgFnSc4d1	nová
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
na	na	k7c4	na
svátky	svátek	k1gInPc4	svátek
Tet	Teta	k1gFnPc2	Teta
<g/>
,	,	kIx,	,
vietnamský	vietnamský	k2eAgInSc4d1	vietnamský
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
-	-	kIx~	-
největší	veliký	k2eAgInSc4d3	veliký
svátek	svátek	k1gInSc4	svátek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vyvolání	vyvolání	k1gNnSc1	vyvolání
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dobytí	dobytí	k1gNnSc3	dobytí
Saigonu	Saigon	k1gInSc2	Saigon
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
tím	ten	k3xDgNnSc7	ten
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
americkou	americký	k2eAgFnSc4d1	americká
a	a	k8xC	a
jihovietnamskou	jihovietnamský	k2eAgFnSc4d1	jihovietnamská
armádu	armáda	k1gFnSc4	armáda
zaskočila	zaskočit	k5eAaPmAgFnS	zaskočit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
odražena	odražen	k2eAgFnSc1d1	odražena
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
protivník	protivník	k1gMnSc1	protivník
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
ofenziva	ofenziva	k1gFnSc1	ofenziva
po	po	k7c6	po
vojenské	vojenský	k2eAgFnSc6d1	vojenská
stránce	stránka	k1gFnSc6	stránka
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
politickým	politický	k2eAgNnSc7d1	politické
vítězstvím	vítězství	k1gNnSc7	vítězství
Severního	severní	k2eAgInSc2d1	severní
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
americké	americký	k2eAgNnSc4d1	americké
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
stažení	stažení	k1gNnSc3	stažení
USA	USA	kA	USA
z	z	k7c2	z
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Vietkong	Vietkong	k1gInSc1	Vietkong
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
těžkých	těžký	k2eAgInPc6d1	těžký
bojích	boj	k1gInPc6	boj
zdecimován	zdecimován	k2eAgInSc1d1	zdecimován
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
osvobozovací	osvobozovací	k2eAgFnSc4d1	osvobozovací
úlohu	úloha	k1gFnSc4	úloha
musela	muset	k5eAaImAgFnS	muset
definitivně	definitivně	k6eAd1	definitivně
zastoupit	zastoupit	k5eAaPmF	zastoupit
VLA	VLA	kA	VLA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
už	už	k6eAd1	už
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
přesvědčenými	přesvědčený	k2eAgMnPc7d1	přesvědčený
bojovníky	bojovník	k1gMnPc7	bojovník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mladými	mladý	k2eAgMnPc7d1	mladý
branci	branec	k1gMnPc7	branec
ze	z	k7c2	z
Severního	severní	k2eAgInSc2d1	severní
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ztratila	ztratit	k5eAaPmAgFnS	ztratit
ráz	ráz	k1gInSc4	ráz
národněosvobozovacího	národněosvobozovací	k2eAgInSc2d1	národněosvobozovací
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
válkou	válka	k1gFnSc7	válka
dvou	dva	k4xCgInPc2	dva
suverénních	suverénní	k2eAgInPc2d1	suverénní
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
už	už	k6eAd1	už
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
neproběhlo	proběhnout	k5eNaPmAgNnS	proběhnout
žádné	žádný	k3yNgNnSc4	žádný
větší	veliký	k2eAgNnSc4d2	veliký
ozbrojené	ozbrojený	k2eAgNnSc4d1	ozbrojené
střetnutí	střetnutí	k1gNnSc4	střetnutí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vojáci	voják	k1gMnPc1	voják
dále	daleko	k6eAd2	daleko
neumírali	umírat	k5eNaImAgMnP	umírat
v	v	k7c6	v
nesmyslných	smyslný	k2eNgFnPc6d1	nesmyslná
přestřelkách	přestřelka	k1gFnPc6	přestřelka
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
<g/>
,	,	kIx,	,
palbou	palba	k1gFnSc7	palba
ostřelovačů	ostřelovač	k1gMnPc2	ostřelovač
či	či	k8xC	či
při	při	k7c6	při
drtivých	drtivý	k2eAgInPc6d1	drtivý
útocích	útok	k1gInPc6	útok
amerického	americký	k2eAgNnSc2d1	americké
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Partyzáni	partyzán	k1gMnPc1	partyzán
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
více	hodně	k6eAd2	hodně
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
kladení	kladení	k1gNnSc4	kladení
min	mina	k1gFnPc2	mina
a	a	k8xC	a
partyzánské	partyzánský	k2eAgInPc1d1	partyzánský
přepady	přepad	k1gInPc1	přepad
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
alespoň	alespoň	k9	alespoň
ty	ten	k3xDgFnPc1	ten
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
měli	mít	k5eAaImAgMnP	mít
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
před	před	k7c7	před
ofenzívou	ofenzíva	k1gFnSc7	ofenzíva
Tet	Teta	k1gFnPc2	Teta
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
začal	začít	k5eAaPmAgMnS	začít
se	s	k7c7	s
stahováním	stahování	k1gNnSc7	stahování
amerických	americký	k2eAgFnPc2d1	americká
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
ochotný	ochotný	k2eAgMnSc1d1	ochotný
válku	válka	k1gFnSc4	válka
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
jihovietnamskou	jihovietnamský	k2eAgFnSc4d1	jihovietnamská
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
schopná	schopný	k2eAgFnSc1d1	schopná
bojovat	bojovat	k5eAaImF	bojovat
sama	sám	k3xTgNnPc4	sám
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vietnamizace	vietnamizace	k1gFnSc1	vietnamizace
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počet	počet	k1gInSc1	počet
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
nacházelo	nacházet	k5eAaImAgNnS	nacházet
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
543	[number]	k4	543
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
rozmístěna	rozmístit	k5eAaPmNgFnS	rozmístit
většina	většina	k1gFnSc1	většina
amerických	americký	k2eAgFnPc2d1	americká
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
další	další	k2eAgNnSc4d1	další
zvyšování	zvyšování	k1gNnSc4	zvyšování
počtu	počet	k1gInSc2	počet
vojáků	voják	k1gMnPc2	voják
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
povolání	povolání	k1gNnSc1	povolání
strategických	strategický	k2eAgFnPc2d1	strategická
záloh	záloha	k1gFnPc2	záloha
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
vyrovnalo	vyrovnat	k5eAaPmAgNnS	vyrovnat
některému	některý	k3yIgInSc3	některý
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
předešlých	předešlý	k2eAgInPc2d1	předešlý
světových	světový	k2eAgInPc2d1	světový
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
momentu	moment	k1gInSc2	moment
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
počet	počet	k1gInSc1	počet
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
snižovat	snižovat	k5eAaImF	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1969	[number]	k4	1969
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
bojové	bojový	k2eAgFnPc4d1	bojová
operace	operace	k1gFnPc4	operace
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
předešlými	předešlý	k2eAgMnPc7d1	předešlý
poměrně	poměrně	k6eAd1	poměrně
pokojný	pokojný	k2eAgMnSc1d1	pokojný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
údolí	údolí	k1gNnSc2	údolí
A	a	k8xC	a
Shau	Shaus	k1gInSc2	Shaus
desetidenní	desetidenní	k2eAgInSc1d1	desetidenní
boj	boj	k1gInSc1	boj
o	o	k7c4	o
kótu	kóta	k1gFnSc4	kóta
937	[number]	k4	937
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
dobýt	dobýt	k5eAaPmF	dobýt
jednotky	jednotka	k1gFnPc4	jednotka
503	[number]	k4	503
<g/>
.	.	kIx.	.
a	a	k8xC	a
506	[number]	k4	506
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
101	[number]	k4	101
<g/>
.	.	kIx.	.
výsadkové	výsadkový	k2eAgFnSc2d1	výsadková
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
boji	boj	k1gInSc6	boj
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nepříjemnému	příjemný	k2eNgInSc3d1	nepříjemný
omylu	omyl	k1gInSc3	omyl
<g/>
,	,	kIx,	,
když	když	k8xS	když
vrtulníky	vrtulník	k1gInPc1	vrtulník
vzdušného	vzdušný	k2eAgNnSc2d1	vzdušné
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
spustily	spustit	k5eAaPmAgFnP	spustit
v	v	k7c6	v
nepřehledném	přehledný	k2eNgInSc6d1	nepřehledný
terénu	terén	k1gInSc6	terén
palbu	palba	k1gFnSc4	palba
do	do	k7c2	do
vlastních	vlastní	k2eAgFnPc2d1	vlastní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
zabily	zabít	k5eAaPmAgFnP	zabít
2	[number]	k4	2
a	a	k8xC	a
zranily	zranit	k5eAaPmAgFnP	zranit
35	[number]	k4	35
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
pomohly	pomoct	k5eAaPmAgFnP	pomoct
nepříteli	nepřítel	k1gMnSc3	nepřítel
odrazit	odrazit	k5eAaPmF	odrazit
probíhající	probíhající	k2eAgInSc4d1	probíhající
útok	útok	k1gInSc4	útok
amerických	americký	k2eAgMnPc2d1	americký
výsadkářů	výsadkář	k1gMnPc2	výsadkář
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
byl	být	k5eAaImAgInS	být
zdlouhavý	zdlouhavý	k2eAgInSc1d1	zdlouhavý
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
kvůli	kvůli	k7c3	kvůli
komplikovanému	komplikovaný	k2eAgInSc3d1	komplikovaný
terénu	terén	k1gInSc3	terén
a	a	k8xC	a
odhodlání	odhodlání	k1gNnSc4	odhodlání
obrany	obrana	k1gFnSc2	obrana
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Kopec	kopec	k1gInSc1	kopec
byl	být	k5eAaImAgInS	být
protkán	protkán	k2eAgInSc4d1	protkán
tunely	tunel	k1gInPc4	tunel
a	a	k8xC	a
partyzáni	partyzán	k1gMnPc1	partyzán
byli	být	k5eAaImAgMnP	být
proto	proto	k8xC	proto
schopni	schopen	k2eAgMnPc1d1	schopen
každou	každý	k3xTgFnSc4	každý
noc	noc	k1gFnSc4	noc
přivážet	přivážet	k5eAaImF	přivážet
posily	posila	k1gFnPc4	posila
a	a	k8xC	a
odvážet	odvážet	k5eAaImF	odvážet
raněné	raněný	k1gMnPc4	raněný
<g/>
.	.	kIx.	.
</s>
<s>
Palebné	palebný	k2eAgInPc1d1	palebný
bunkry	bunkr	k1gInPc1	bunkr
jim	on	k3xPp3gMnPc3	on
přitom	přitom	k6eAd1	přitom
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
solidní	solidní	k2eAgFnSc4d1	solidní
ochranu	ochrana	k1gFnSc4	ochrana
dokonce	dokonce	k9	dokonce
i	i	k9	i
před	před	k7c7	před
útoky	útok	k1gInPc7	útok
letectva	letectvo	k1gNnSc2	letectvo
a	a	k8xC	a
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
na	na	k7c4	na
kopec	kopec	k1gInSc4	kopec
vychrlily	vychrlit	k5eAaPmAgFnP	vychrlit
450	[number]	k4	450
tun	tuna	k1gFnPc2	tuna
bomb	bomba	k1gFnPc2	bomba
a	a	k8xC	a
68	[number]	k4	68
tun	tuna	k1gFnPc2	tuna
napalmu	napalm	k1gInSc2	napalm
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
boje	boj	k1gInSc2	boj
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
vrchol	vrchol	k1gInSc1	vrchol
kompletně	kompletně	k6eAd1	kompletně
odlesněn	odlesnit	k5eAaPmNgInS	odlesnit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vojáků	voják	k1gMnPc2	voják
VLA	VLA	kA	VLA
se	se	k3xPyFc4	se
poslední	poslední	k2eAgFnSc1d1	poslední
noc	noc	k1gFnSc1	noc
bojů	boj	k1gInPc2	boj
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Temeno	temeno	k1gNnSc1	temeno
kopce	kopec	k1gInSc2	kopec
bylo	být	k5eAaImAgNnS	být
dobyto	dobyt	k2eAgNnSc4d1	dobyto
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
ho	on	k3xPp3gMnSc4	on
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
"	"	kIx"	"
<g/>
Hamburger	hamburger	k1gInSc1	hamburger
Hill	Hill	k1gMnSc1	Hill
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
boje	boj	k1gInPc1	boj
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
dělaly	dělat	k5eAaImAgFnP	dělat
z	z	k7c2	z
vojáků	voják	k1gMnPc2	voják
hamburgery	hamburger	k1gInPc1	hamburger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
stály	stát	k5eAaImAgInP	stát
život	život	k1gInSc4	život
47	[number]	k4	47
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
372	[number]	k4	372
bylo	být	k5eAaImAgNnS	být
raněno	ranit	k5eAaPmNgNnS	ranit
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
činily	činit	k5eAaImAgInP	činit
podle	podle	k7c2	podle
počtů	počet	k1gInPc2	počet
US	US	kA	US
Army	Arma	k1gFnSc2	Arma
630	[number]	k4	630
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
vojáků	voják	k1gMnPc2	voják
VLA	VLA	kA	VLA
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
prapor	prapor	k1gInSc1	prapor
29	[number]	k4	29
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc6	pluk
VLA	VLA	kA	VLA
byly	být	k5eAaImAgInP	být
prakticky	prakticky	k6eAd1	prakticky
zničeny	zničit	k5eAaPmNgInP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
nasedly	nasednout	k5eAaPmAgFnP	nasednout
do	do	k7c2	do
vrtulníků	vrtulník	k1gInPc2	vrtulník
a	a	k8xC	a
odletěly	odletět	k5eAaPmAgFnP	odletět
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
dovolily	dovolit	k5eAaPmAgFnP	dovolit
tak	tak	k6eAd1	tak
Severovietnamcům	Severovietnamec	k1gMnPc3	Severovietnamec
znovu	znovu	k6eAd1	znovu
obsadit	obsadit	k5eAaPmF	obsadit
kopec	kopec	k1gInSc4	kopec
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Ho	on	k3xPp3gMnSc4	on
Či	či	k8xC	či
Min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnPc4	vůdce
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
přikazoval	přikazovat	k5eAaImAgMnS	přikazovat
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
imperialistům	imperialista	k1gMnPc3	imperialista
až	až	k8xS	až
do	do	k7c2	do
vítězného	vítězný	k2eAgInSc2d1	vítězný
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
největší	veliký	k2eAgFnSc1d3	veliký
protiválečná	protiválečný	k2eAgFnSc1d1	protiválečná
demonstrace	demonstrace	k1gFnSc1	demonstrace
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
USA	USA	kA	USA
<g/>
,	,	kIx,	,
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
250	[number]	k4	250
000	[number]	k4	000
demonstrantů	demonstrant	k1gMnPc2	demonstrant
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
protestujících	protestující	k2eAgMnPc2d1	protestující
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
vracejících	vracející	k2eAgMnPc2d1	vracející
se	se	k3xPyFc4	se
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
zklamáni	zklamat	k5eAaPmNgMnP	zklamat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
vede	vést	k5eAaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
povinná	povinný	k2eAgFnSc1d1	povinná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
v	v	k7c6	v
USA	USA	kA	USA
trvala	trvat	k5eAaImAgFnS	trvat
1	[number]	k4	1
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
každý	každý	k3xTgMnSc1	každý
branec	branec	k1gMnSc1	branec
mohl	moct	k5eAaImAgMnS	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
nechce	chtít	k5eNaImIp3nS	chtít
prodloužit	prodloužit	k5eAaPmF	prodloužit
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nP	vrátit
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
bojující	bojující	k2eAgFnSc1d1	bojující
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
tak	tak	k6eAd1	tak
vlastně	vlastně	k9	vlastně
nebyla	být	k5eNaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovnou	rovnou	k6eAd1	rovnou
osm	osm	k4xCc4	osm
různých	různý	k2eAgFnPc2d1	různá
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
vojáků	voják	k1gMnPc2	voják
si	se	k3xPyFc3	se
prodlužovalo	prodlužovat	k5eAaImAgNnS	prodlužovat
svou	svůj	k3xOyFgFnSc4	svůj
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatkem	nedostatek	k1gInSc7	nedostatek
zkušených	zkušený	k2eAgMnPc2d1	zkušený
vojáků	voják	k1gMnPc2	voják
trpěly	trpět	k5eAaImAgFnP	trpět
hlavně	hlavně	k6eAd1	hlavně
bojové	bojový	k2eAgFnPc1d1	bojová
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
nezkušeným	zkušený	k2eNgMnPc3d1	nezkušený
nováčkům	nováček	k1gMnPc3	nováček
většinou	většinou	k6eAd1	většinou
nikdo	nikdo	k3yNnSc1	nikdo
nepomáhal	pomáhat	k5eNaImAgMnS	pomáhat
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
prokousat	prokousat	k5eAaPmF	prokousat
stejnými	stejný	k2eAgInPc7d1	stejný
problémy	problém	k1gInPc7	problém
jako	jako	k8xC	jako
jejich	jejich	k3xOp3gMnPc7	jejich
předchůdci	předchůdce	k1gMnPc7	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
odešlo	odejít	k5eAaPmAgNnS	odejít
z	z	k7c2	z
Vietnamu	Vietnam	k1gInSc2	Vietnam
115	[number]	k4	115
000	[number]	k4	000
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
už	už	k6eAd1	už
překročily	překročit	k5eAaPmAgFnP	překročit
americké	americký	k2eAgFnPc4d1	americká
ztráty	ztráta	k1gFnPc4	ztráta
40	[number]	k4	40
000	[number]	k4	000
padlých	padlý	k1gMnPc2	padlý
<g/>
.	.	kIx.	.
</s>
<s>
Úloha	úloha	k1gFnSc1	úloha
ochraňovat	ochraňovat	k5eAaImF	ochraňovat
Jižní	jižní	k2eAgInSc4d1	jižní
Vietnam	Vietnam	k1gInSc4	Vietnam
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
půlmilión	půlmilión	k4xCgInSc4	půlmilión
vojáků	voják	k1gMnPc2	voják
jihovietnamské	jihovietnamský	k2eAgFnSc2d1	jihovietnamská
armády	armáda	k1gFnSc2	armáda
-	-	kIx~	-
začala	začít	k5eAaPmAgFnS	začít
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vietnamizace	vietnamizace	k1gFnSc1	vietnamizace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
strategických	strategický	k2eAgInPc2d1	strategický
tahů	tah	k1gInPc2	tah
Američanů	Američan	k1gMnPc2	Američan
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
překročení	překročení	k1gNnPc2	překročení
kambodžsko-vietnamských	kambodžskoietnamský	k2eAgFnPc2d1	kambodžsko-vietnamský
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
útok	útok	k1gInSc1	útok
na	na	k7c4	na
část	část	k1gFnSc4	část
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
procházela	procházet	k5eAaImAgFnS	procházet
Ho-Či-Minova	Ho-Či-Minův	k2eAgFnSc1d1	Ho-Či-Minův
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
blokování	blokování	k1gNnSc1	blokování
mělo	mít	k5eAaImAgNnS	mít
částečně	částečně	k6eAd1	částečně
ztížit	ztížit	k5eAaPmF	ztížit
postavení	postavení	k1gNnSc4	postavení
partyzánům	partyzán	k1gMnPc3	partyzán
odříznutým	odříznutý	k2eAgNnPc3d1	odříznuté
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotných	samotný	k2eAgInPc6d1	samotný
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
rozšíření	rozšíření	k1gNnSc4	rozšíření
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
na	na	k7c4	na
Kambodžu	Kambodža	k1gFnSc4	Kambodža
velké	velký	k2eAgInPc4d1	velký
nepokoje	nepokoj	k1gInPc4	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
studentské	studentská	k1gFnSc6	studentská
protestní	protestní	k2eAgFnSc4d1	protestní
akci	akce	k1gFnSc4	akce
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
Kent	Kenta	k1gFnPc2	Kenta
State	status	k1gInSc5	status
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
byli	být	k5eAaImAgMnP	být
Národní	národní	k2eAgFnSc7d1	národní
gardou	garda	k1gFnSc7	garda
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
čtyři	čtyři	k4xCgMnPc1	čtyři
studenti	student	k1gMnPc1	student
a	a	k8xC	a
devět	devět	k4xCc1	devět
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
vážně	vážně	k6eAd1	vážně
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
akci	akce	k1gFnSc6	akce
házel	házet	k5eAaImAgMnS	házet
dav	dav	k1gInSc4	dav
studentů	student	k1gMnPc2	student
na	na	k7c4	na
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
složky	složka	k1gFnPc4	složka
kameny	kámen	k1gInPc1	kámen
a	a	k8xC	a
kusy	kus	k1gInPc1	kus
betonu	beton	k1gInSc2	beton
a	a	k8xC	a
zápalnými	zápalný	k2eAgFnPc7d1	zápalná
lahvemi	lahev	k1gFnPc7	lahev
zapálili	zapálit	k5eAaPmAgMnP	zapálit
jednu	jeden	k4xCgFnSc4	jeden
univerzitní	univerzitní	k2eAgFnSc4d1	univerzitní
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
členů	člen	k1gMnPc2	člen
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
použili	použít	k5eAaPmAgMnP	použít
její	její	k3xOp3gMnPc1	její
příslušníci	příslušník	k1gMnPc1	příslušník
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
cítili	cítit	k5eAaImAgMnP	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInPc1	jejich
životy	život	k1gInPc1	život
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
velitele	velitel	k1gMnSc2	velitel
jednotky	jednotka	k1gFnSc2	jednotka
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
hrozilo	hrozit	k5eAaImAgNnS	hrozit
vojákům	voják	k1gMnPc3	voják
zabití	zabití	k1gNnSc2	zabití
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
kamenů	kámen	k1gInPc2	kámen
a	a	k8xC	a
blízkosti	blízkost	k1gFnSc2	blízkost
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	on	k3xPp3gInPc4	on
házeli	házet	k5eAaImAgMnP	házet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
amerického	americký	k2eAgInSc2d1	americký
časopisu	časopis	k1gInSc2	časopis
Newsweek	Newsweek	k6eAd1	Newsweek
však	však	k9	však
zastřelení	zastřelený	k2eAgMnPc1d1	zastřelený
a	a	k8xC	a
zranění	zraněný	k2eAgMnPc1d1	zraněný
studenti	student	k1gMnPc1	student
příslušníky	příslušník	k1gMnPc7	příslušník
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
střelby	střelba	k1gFnSc2	střelba
nijak	nijak	k6eAd1	nijak
neohrožovali	ohrožovat	k5eNaImAgMnP	ohrožovat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
gardisté	gardista	k1gMnPc1	gardista
studenty	student	k1gMnPc4	student
nevarovali	varovat	k5eNaImAgMnP	varovat
a	a	k8xC	a
neposkytli	poskytnout	k5eNaPmAgMnP	poskytnout
zraněným	zraněný	k2eAgMnPc3d1	zraněný
studentům	student	k1gMnPc3	student
žádnou	žádný	k3yNgFnSc4	žádný
pomoc	pomoc	k1gFnSc4	pomoc
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
nepostarali	postarat	k5eNaPmAgMnP	postarat
o	o	k7c4	o
mrtvá	mrtvý	k2eAgNnPc4d1	mrtvé
těla	tělo	k1gNnPc4	tělo
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
místo	místo	k7c2	místo
incidentu	incident	k1gInSc2	incident
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyhlášeného	vyhlášený	k2eAgInSc2d1	vyhlášený
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
stavu	stav	k1gInSc2	stav
byla	být	k5eAaImAgFnS	být
Národní	národní	k2eAgFnSc1d1	národní
garda	garda	k1gFnSc1	garda
oprávněna	oprávněn	k2eAgFnSc1d1	oprávněna
použít	použít	k5eAaPmF	použít
zbraně	zbraň	k1gFnPc4	zbraň
jen	jen	k9	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
svého	svůj	k3xOyFgNnSc2	svůj
skutečného	skutečný	k2eAgNnSc2d1	skutečné
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
dočasně	dočasně	k6eAd1	dočasně
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
400	[number]	k4	400
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
po	po	k7c6	po
celých	celá	k1gFnPc6	celá
USA	USA	kA	USA
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
však	však	k9	však
nepokoje	nepokoj	k1gInPc1	nepokoj
mezi	mezi	k7c7	mezi
studenty	student	k1gMnPc7	student
neutišily	utišit	k5eNaPmAgFnP	utišit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
upokojil	upokojit	k5eAaPmAgMnS	upokojit
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
prezident	prezident	k1gMnSc1	prezident
Nixon	Nixon	k1gMnSc1	Nixon
<g/>
,	,	kIx,	,
že	že	k8xS	že
americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
proniknou	proniknout	k5eAaPmIp3nP	proniknout
jen	jen	k9	jen
40	[number]	k4	40
km	km	kA	km
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
čase	čas	k1gInSc6	čas
stáhnou	stáhnout	k5eAaPmIp3nP	stáhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
využili	využít	k5eAaPmAgMnP	využít
partyzáni	partyzán	k1gMnPc1	partyzán
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
40	[number]	k4	40
km	km	kA	km
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
Kambodži	Kambodža	k1gFnSc6	Kambodža
padlo	padnout	k5eAaImAgNnS	padnout
okolo	okolo	k7c2	okolo
350	[number]	k4	350
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
partyzáni	partyzán	k1gMnPc1	partyzán
jako	jako	k9	jako
odvetu	odveta	k1gFnSc4	odveta
zabili	zabít	k5eAaPmAgMnP	zabít
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
při	při	k7c6	při
různých	různý	k2eAgInPc6d1	různý
útocích	útok	k1gInPc6	útok
450	[number]	k4	450
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
odchodem	odchod	k1gInSc7	odchod
z	z	k7c2	z
Kambodže	Kambodža	k1gFnSc2	Kambodža
probíhalo	probíhat	k5eAaImAgNnS	probíhat
i	i	k9	i
stahování	stahování	k1gNnSc4	stahování
amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
z	z	k7c2	z
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
klesala	klesat	k5eAaImAgFnS	klesat
morálka	morálka	k1gFnSc1	morálka
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
nechtěl	chtít	k5eNaImAgMnS	chtít
být	být	k5eAaImF	být
posledním	poslední	k2eAgMnSc7d1	poslední
padlým	padlý	k1gMnSc7	padlý
Američanem	Američan	k1gMnSc7	Američan
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
starostí	starost	k1gFnPc2	starost
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
země	zem	k1gFnSc2	zem
přecházelo	přecházet	k5eAaImAgNnS	přecházet
na	na	k7c4	na
bedra	bedra	k1gNnPc4	bedra
ARVN	ARVN	kA	ARVN
<g/>
.	.	kIx.	.
</s>
<s>
Stoupal	stoupat	k5eAaImAgInS	stoupat
počet	počet	k1gInSc1	počet
jejích	její	k3xOp3gMnPc2	její
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc4	jejich
odhodlání	odhodlání	k1gNnSc4	odhodlání
bojovat	bojovat	k5eAaImF	bojovat
bylo	být	k5eAaImAgNnS	být
dost	dost	k6eAd1	dost
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
.	.	kIx.	.
</s>
<s>
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
bojeschopnost	bojeschopnost	k1gFnSc4	bojeschopnost
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
s	s	k7c7	s
americkými	americký	k2eAgFnPc7d1	americká
jednotkami	jednotka	k1gFnPc7	jednotka
si	se	k3xPyFc3	se
udrželo	udržet	k5eAaPmAgNnS	udržet
jen	jen	k9	jen
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
též	též	k9	též
odvetné	odvetný	k2eAgInPc1d1	odvetný
nálety	nálet	k1gInPc1	nálet
na	na	k7c4	na
Laos	Laos	k1gInSc4	Laos
<g/>
,	,	kIx,	,
Kambodžu	Kambodža	k1gFnSc4	Kambodža
a	a	k8xC	a
Severní	severní	k2eAgInSc4d1	severní
Vietnam	Vietnam	k1gInSc4	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Nixon	Nixon	k1gMnSc1	Nixon
varoval	varovat	k5eAaImAgMnS	varovat
Hà	Hà	k1gFnSc4	Hà
<g/>
,	,	kIx,	,
že	že	k8xS	že
intenzita	intenzita	k1gFnSc1	intenzita
náletů	nálet	k1gInPc2	nálet
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
budou	být	k5eAaImBp3nP	být
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
Jižní	jižní	k2eAgInSc4d1	jižní
Vietnam	Vietnam	k1gInSc4	Vietnam
nadále	nadále	k6eAd1	nadále
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Laosu	Laos	k1gInSc2	Laos
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
už	už	k6eAd1	už
však	však	k9	však
neúčastnili	účastnit	k5eNaImAgMnP	účastnit
žádní	žádný	k3yNgMnPc1	žádný
američtí	americký	k2eAgMnPc1d1	americký
pěšáci	pěšák	k1gMnPc1	pěšák
<g/>
,	,	kIx,	,
asistovali	asistovat	k5eAaImAgMnP	asistovat
jen	jen	k9	jen
letci	letec	k1gMnPc1	letec
a	a	k8xC	a
posádky	posádka	k1gFnPc1	posádka
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
jihovietnamské	jihovietnamský	k2eAgFnSc3d1	jihovietnamská
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Severovietnamci	Severovietnamec	k1gMnPc1	Severovietnamec
ale	ale	k9	ale
využili	využít	k5eAaPmAgMnP	využít
blízkost	blízkost	k1gFnSc4	blízkost
svých	svůj	k3xOyFgFnPc2	svůj
základen	základna	k1gFnPc2	základna
a	a	k8xC	a
vrhli	vrhnout	k5eAaPmAgMnP	vrhnout
do	do	k7c2	do
boje	boj	k1gInSc2	boj
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
měli	mít	k5eAaImAgMnP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Jihovietnamci	Jihovietnamec	k1gMnPc1	Jihovietnamec
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
Laosu	Laos	k1gInSc2	Laos
předčasně	předčasně	k6eAd1	předčasně
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
1	[number]	k4	1
<g/>
.	.	kIx.	.
aeoromobilní	aeoromobilní	k2eAgFnSc2d1	aeoromobilní
divize	divize	k1gFnSc2	divize
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
ztratila	ztratit	k5eAaPmAgFnS	ztratit
580	[number]	k4	580
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
225	[number]	k4	225
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
samé	samý	k3xTgFnSc6	samý
době	doba	k1gFnSc6	doba
komunisté	komunista	k1gMnPc1	komunista
v	v	k7c6	v
Severním	severní	k2eAgInSc6d1	severní
Vietnamu	Vietnam	k1gInSc6	Vietnam
intenzivně	intenzivně	k6eAd1	intenzivně
zbrojili	zbrojit	k5eAaImAgMnP	zbrojit
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
vyzbrojovali	vyzbrojovat	k5eAaImAgMnP	vyzbrojovat
novými	nový	k2eAgInPc7d1	nový
tanky	tank	k1gInPc7	tank
a	a	k8xC	a
dělostřelectvem	dělostřelectvo	k1gNnSc7	dělostřelectvo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
doslova	doslova	k6eAd1	doslova
těžká	těžký	k2eAgFnSc1d1	těžká
výzbroj	výzbroj	k1gFnSc1	výzbroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
co	co	k9	co
nasadili	nasadit	k5eAaPmAgMnP	nasadit
proti	proti	k7c3	proti
Američanům	Američan	k1gMnPc3	Američan
v	v	k7c6	v
předešlém	předešlý	k2eAgNnSc6d1	předešlé
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
též	též	k9	též
přípravy	příprava	k1gFnPc1	příprava
nové	nový	k2eAgFnSc2d1	nová
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
začít	začít	k5eAaPmF	začít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
15	[number]	k4	15
divizí	divize	k1gFnPc2	divize
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
už	už	k6eAd1	už
převážně	převážně	k6eAd1	převážně
VLA	VLA	kA	VLA
<g/>
,	,	kIx,	,
na	na	k7c4	na
jih	jih	k1gInSc4	jih
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
-	-	kIx~	-
tedy	tedy	k9	tedy
z	z	k7c2	z
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
,	,	kIx,	,
Laosu	Laos	k1gInSc2	Laos
jako	jako	k8xC	jako
i	i	k9	i
přes	přes	k7c4	přes
demilitarizovanou	demilitarizovaný	k2eAgFnSc4d1	demilitarizovaná
zónu	zóna	k1gFnSc4	zóna
(	(	kIx(	(
<g/>
hranici	hranice	k1gFnSc4	hranice
obou	dva	k4xCgInPc2	dva
Vietnamů	Vietnam	k1gInPc2	Vietnam
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nebojovalo	bojovat	k5eNaImAgNnS	bojovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
masivnímu	masivní	k2eAgNnSc3d1	masivní
nasazení	nasazení	k1gNnSc3	nasazení
tanků	tank	k1gInPc2	tank
postupoval	postupovat	k5eAaImAgInS	postupovat
poměrně	poměrně	k6eAd1	poměrně
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
ARVN	ARVN	kA	ARVN
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
bránily	bránit	k5eAaImAgInP	bránit
demilitarizovanou	demilitarizovaný	k2eAgFnSc4d1	demilitarizovaná
zónu	zóna	k1gFnSc4	zóna
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
poraženy	poražen	k2eAgInPc1d1	poražen
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
padlo	padnout	k5eAaPmAgNnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
komunistů	komunista	k1gMnPc2	komunista
po	po	k7c6	po
těžkých	těžký	k2eAgInPc6d1	těžký
bojích	boj	k1gInPc6	boj
strategické	strategický	k2eAgNnSc1d1	strategické
město	město	k1gNnSc4	město
Quang	Quanga	k1gFnPc2	Quanga
Tri	Tri	k1gFnPc2	Tri
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc4d3	veliký
jihovietnamské	jihovietnamský	k2eAgNnSc4d1	jihovietnamské
město	město	k1gNnSc4	město
ležící	ležící	k2eAgNnSc4d1	ležící
severně	severně	k6eAd1	severně
od	od	k7c2	od
Hue	Hue	k1gFnSc2	Hue
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
se	se	k3xPyFc4	se
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
č.	č.	k?	č.
1	[number]	k4	1
hrnuli	hrnout	k5eAaImAgMnP	hrnout
do	do	k7c2	do
zdánlivého	zdánlivý	k2eAgNnSc2d1	zdánlivé
bezpečí	bezpečí	k1gNnSc2	bezpečí
k	k	k7c3	k
Hue	Hue	k1gFnSc3	Hue
tisíce	tisíc	k4xCgInPc1	tisíc
civilistů	civilista	k1gMnPc2	civilista
utíkajících	utíkající	k2eAgFnPc2d1	utíkající
před	před	k7c7	před
boji	boj	k1gInPc7	boj
<g/>
.	.	kIx.	.
</s>
<s>
VLA	VLA	kA	VLA
ale	ale	k8xC	ale
cestu	cesta	k1gFnSc4	cesta
přeťala	přetít	k5eAaPmAgFnS	přetít
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
ostřelovat	ostřelovat	k5eAaImF	ostřelovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
masakru	masakr	k1gInSc6	masakr
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
okolo	okolo	k7c2	okolo
2000	[number]	k4	2000
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
nebyli	být	k5eNaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
nečinně	činně	k6eNd1	činně
přihlížet	přihlížet	k5eAaImF	přihlížet
postupu	postup	k1gInSc3	postup
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
počátku	počátek	k1gInSc6	počátek
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
spustili	spustit	k5eAaPmAgMnP	spustit
nejintenzivnější	intenzivní	k2eAgNnSc4d3	nejintenzivnější
bombardování	bombardování	k1gNnSc4	bombardování
Severního	severní	k2eAgInSc2d1	severní
Vietnamu	Vietnam	k1gInSc2	Vietnam
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nálety	nálet	k1gInPc1	nálet
byly	být	k5eAaImAgInP	být
vedeny	vést	k5eAaImNgInP	vést
poprvé	poprvé	k6eAd1	poprvé
opravdu	opravdu	k6eAd1	opravdu
efektivně	efektivně	k6eAd1	efektivně
a	a	k8xC	a
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
byly	být	k5eAaImAgInP	být
přímo	přímo	k6eAd1	přímo
podporovány	podporovat	k5eAaImNgInP	podporovat
i	i	k8xC	i
jednotky	jednotka	k1gFnPc1	jednotka
ARVN	ARVN	kA	ARVN
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
komunistická	komunistický	k2eAgFnSc1d1	komunistická
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
zpomalovala	zpomalovat	k5eAaImAgFnS	zpomalovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
zastavila	zastavit	k5eAaPmAgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Jihovietnamci	Jihovietnamec	k1gMnPc1	Jihovietnamec
nejistě	jistě	k6eNd1	jistě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přece	přece	k9	přece
jen	jen	k9	jen
přešli	přejít	k5eAaPmAgMnP	přejít
do	do	k7c2	do
protiútoku	protiútok	k1gInSc2	protiútok
a	a	k8xC	a
dobyli	dobýt	k5eAaPmAgMnP	dobýt
zpět	zpět	k6eAd1	zpět
většinu	většina	k1gFnSc4	většina
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Quang	Quang	k1gMnSc1	Quang
Tri	Tri	k1gMnSc1	Tri
znovudobyli	znovudobýt	k5eAaPmAgMnP	znovudobýt
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
stála	stát	k5eAaImAgFnS	stát
komunisty	komunista	k1gMnPc4	komunista
100	[number]	k4	100
000	[number]	k4	000
padlých	padlý	k1gMnPc2	padlý
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgInSc1d1	jižní
Vietnam	Vietnam	k1gInSc1	Vietnam
postrádal	postrádat	k5eAaImAgInS	postrádat
40	[number]	k4	40
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
letecké	letecký	k2eAgFnSc2d1	letecká
války	válka	k1gFnSc2	válka
nad	nad	k7c7	nad
Severním	severní	k2eAgInSc7d1	severní
Vietnamem	Vietnam	k1gInSc7	Vietnam
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
rozpoutaly	rozpoutat	k5eAaPmAgInP	rozpoutat
masové	masový	k2eAgInPc1d1	masový
protesty	protest	k1gInPc1	protest
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byly	být	k5eAaImAgInP	být
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
náletů	nálet	k1gInPc2	nálet
jihovietnamského	jihovietnamský	k2eAgNnSc2d1	jihovietnamské
letectva	letectvo	k1gNnSc2	letectvo
omylem	omyl	k1gInSc7	omyl
shozeny	shozen	k2eAgFnPc4d1	shozena
bomby	bomba	k1gFnPc4	bomba
na	na	k7c4	na
civilní	civilní	k2eAgNnSc4d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgInPc1d1	známý
záběry	záběr	k1gInPc1	záběr
popálených	popálený	k2eAgFnPc2d1	popálená
a	a	k8xC	a
vystrašených	vystrašený	k2eAgFnPc2d1	vystrašená
dětí	dítě	k1gFnPc2	dítě
vybíhajících	vybíhající	k2eAgFnPc2d1	vybíhající
ze	z	k7c2	z
zasažené	zasažený	k2eAgFnSc2d1	zasažená
osady	osada	k1gFnSc2	osada
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
jedněmi	jeden	k4xCgFnPc7	jeden
ze	z	k7c2	z
smutných	smutný	k2eAgInPc2d1	smutný
obrazů	obraz	k1gInPc2	obraz
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
měly	mít	k5eAaImAgFnP	mít
absolutní	absolutní	k2eAgFnSc4d1	absolutní
převahu	převaha	k1gFnSc4	převaha
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
přefotografovávali	přefotografovávat	k5eAaImAgMnP	přefotografovávat
bojiště	bojiště	k1gNnSc4	bojiště
<g/>
,	,	kIx,	,
snímky	snímek	k1gInPc4	snímek
hned	hned	k6eAd1	hned
porovnávali	porovnávat	k5eAaImAgMnP	porovnávat
a	a	k8xC	a
vyhodnocovali	vyhodnocovat	k5eAaImAgMnP	vyhodnocovat
<g/>
.	.	kIx.	.
</s>
<s>
Říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
stačilo	stačit	k5eAaBmAgNnS	stačit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nějaký	nějaký	k3yIgInSc1	nějaký
keř	keř	k1gInSc1	keř
byl	být	k5eAaImAgInS	být
přemístěn	přemístěn	k2eAgInSc1d1	přemístěn
o	o	k7c4	o
deset	deset	k4xCc4	deset
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
byla	být	k5eAaImAgFnS	být
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
místo	místo	k1gNnSc4	místo
shozena	shozen	k2eAgFnSc1d1	shozena
bomba	bomba	k1gFnSc1	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ani	ani	k8xC	ani
tato	tento	k3xDgFnSc1	tento
technická	technický	k2eAgFnSc1d1	technická
převaha	převaha	k1gFnSc1	převaha
nepomohla	pomoct	k5eNaPmAgFnS	pomoct
a	a	k8xC	a
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
se	se	k3xPyFc4	se
základní	základní	k2eAgFnSc1d1	základní
taktická	taktický	k2eAgFnSc1d1	taktická
poučka	poučka	k1gFnSc1	poučka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dokud	dokud	k8xS	dokud
na	na	k7c6	na
území	území	k1gNnSc6	území
nevstoupí	vstoupit	k5eNaPmIp3nS	vstoupit
noha	noha	k1gFnSc1	noha
pěšáka	pěšák	k1gMnSc2	pěšák
<g/>
,	,	kIx,	,
území	území	k1gNnSc1	území
není	být	k5eNaImIp3nS	být
dobyto	dobyt	k2eAgNnSc1d1	dobyto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
dohody	dohoda	k1gFnSc2	dohoda
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
náletů	nálet	k1gInPc2	nálet
komunisty	komunista	k1gMnSc2	komunista
přinutila	přinutit	k5eAaPmAgFnS	přinutit
poprvé	poprvé	k6eAd1	poprvé
seriózně	seriózně	k6eAd1	seriózně
zasednout	zasednout	k5eAaPmF	zasednout
k	k	k7c3	k
jednacímu	jednací	k2eAgInSc3d1	jednací
stolu	stol	k1gInSc3	stol
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Jižním	jižní	k2eAgInSc7d1	jižní
Vietnamem	Vietnam	k1gInSc7	Vietnam
<g/>
,	,	kIx,	,
Vietkongem	Vietkong	k1gInSc7	Vietkong
a	a	k8xC	a
Severním	severní	k2eAgInSc7d1	severní
Vietnamem	Vietnam	k1gInSc7	Vietnam
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
podepsána	podepsán	k2eAgFnSc1d1	podepsána
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1973	[number]	k4	1973
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1973	[number]	k4	1973
v	v	k7c4	v
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodin	hodina	k1gFnPc2	hodina
saigonského	saigonský	k2eAgInSc2d1	saigonský
času	čas	k1gInSc2	čas
měly	mít	k5eAaImAgFnP	mít
ustat	ustat	k5eAaPmF	ustat
bojové	bojový	k2eAgFnPc1d1	bojová
operace	operace	k1gFnPc1	operace
všech	všecek	k3xTgFnPc2	všecek
bojujících	bojující	k2eAgFnPc2d1	bojující
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
USA	USA	kA	USA
měly	mít	k5eAaImAgFnP	mít
stáhnout	stáhnout	k5eAaPmF	stáhnout
zbytek	zbytek	k1gInSc4	zbytek
svých	svůj	k3xOyFgMnPc2	svůj
vojáků	voják	k1gMnPc2	voják
z	z	k7c2	z
Vietnamu	Vietnam	k1gInSc2	Vietnam
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
následujících	následující	k2eAgInPc2d1	následující
60	[number]	k4	60
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
také	také	k9	také
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1973	[number]	k4	1973
učinily	učinit	k5eAaImAgFnP	učinit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
civilních	civilní	k2eAgMnPc2d1	civilní
poradců	poradce	k1gMnPc2	poradce
při	při	k7c6	při
správních	správní	k2eAgInPc6d1	správní
úřadech	úřad	k1gInPc6	úřad
a	a	k8xC	a
armádě	armáda	k1gFnSc6	armáda
Vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
dostali	dostat	k5eAaPmAgMnP	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
americký	americký	k2eAgMnSc1d1	americký
diplomat	diplomat	k1gMnSc1	diplomat
Henry	Henry	k1gMnSc1	Henry
Kissinger	Kissinger	k1gMnSc1	Kissinger
a	a	k8xC	a
vietnamský	vietnamský	k2eAgMnSc1d1	vietnamský
revolucionář	revolucionář	k1gMnSc1	revolucionář
Lê	Lê	k1gMnSc1	Lê
Đứ	Đứ	k1gMnSc1	Đứ
Thọ	Thọ	k1gMnSc1	Thọ
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
řka	říct	k5eAaPmSgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Jihovietnamský	jihovietnamský	k2eAgMnSc1d1	jihovietnamský
prezident	prezident	k1gMnSc1	prezident
Thieu	Thieus	k1gInSc2	Thieus
označil	označit	k5eAaPmAgMnS	označit
dohodu	dohoda	k1gFnSc4	dohoda
za	za	k7c4	za
faktickou	faktický	k2eAgFnSc4d1	faktická
kapitulaci	kapitulace	k1gFnSc4	kapitulace
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dobře	dobře	k6eAd1	dobře
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunisté	komunista	k1gMnPc1	komunista
ji	on	k3xPp3gFnSc4	on
budou	být	k5eAaImBp3nP	být
ignorovat	ignorovat	k5eAaImF	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
ji	on	k3xPp3gFnSc4	on
ignorovali	ignorovat	k5eAaImAgMnP	ignorovat
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
Vietnamu	Vietnam	k1gInSc2	Vietnam
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
mise	mise	k1gFnSc1	mise
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
dodržování	dodržování	k1gNnSc4	dodržování
dohlížet	dohlížet	k5eAaImF	dohlížet
<g/>
.	.	kIx.	.
</s>
<s>
Thieu	Thieu	k5eAaPmIp1nS	Thieu
to	ten	k3xDgNnSc1	ten
však	však	k9	však
musel	muset	k5eAaImAgInS	muset
akceptovat	akceptovat	k5eAaBmF	akceptovat
<g/>
,	,	kIx,	,
dohoda	dohoda	k1gFnSc1	dohoda
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
platná	platný	k2eAgFnSc1d1	platná
i	i	k9	i
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
nepodepsali	podepsat	k5eNaPmAgMnP	podepsat
představitelé	představitel	k1gMnPc1	představitel
Jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
území	území	k1gNnSc6	území
probíhal	probíhat	k5eAaImAgInS	probíhat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
celý	celý	k2eAgInSc4d1	celý
konflikt	konflikt	k1gInSc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
potom	potom	k6eAd1	potom
by	by	kYmCp3nS	by
už	už	k6eAd1	už
Jižní	jižní	k2eAgInSc1d1	jižní
Vietnam	Vietnam	k1gInSc1	Vietnam
nemohl	moct	k5eNaImAgInS	moct
očekávat	očekávat	k5eAaImF	očekávat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
začátek	začátek	k1gInSc1	začátek
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
USA	USA	kA	USA
aféra	aféra	k1gFnSc1	aféra
Watergate	Watergat	k1gInSc5	Watergat
a	a	k8xC	a
Vietnam	Vietnam	k1gInSc1	Vietnam
se	se	k3xPyFc4	se
naráz	naráz	k6eAd1	naráz
ztratil	ztratit	k5eAaPmAgMnS	ztratit
ze	z	k7c2	z
sféry	sféra	k1gFnSc2	sféra
zájmů	zájem	k1gInPc2	zájem
amerických	americký	k2eAgFnPc2d1	americká
politických	politický	k2eAgFnPc2d1	politická
špiček	špička	k1gFnPc2	špička
<g/>
.	.	kIx.	.
</s>
<s>
Nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
ani	ani	k8xC	ani
ubezpečení	ubezpečení	k1gNnSc1	ubezpečení
Nixona	Nixon	k1gMnSc2	Nixon
<g/>
,	,	kIx,	,
že	že	k8xS	že
USA	USA	kA	USA
podpoří	podpořit	k5eAaPmIp3nS	podpořit
Jižní	jižní	k2eAgInSc1d1	jižní
Vietnam	Vietnam	k1gInSc1	Vietnam
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
severovietnamské	severovietnamský	k2eAgFnSc2d1	severovietnamská
agrese	agrese	k1gFnSc2	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
příměří	příměří	k1gNnSc2	příměří
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
14	[number]	k4	14
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
ARVN	ARVN	kA	ARVN
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
jistí	jistý	k2eAgMnPc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
USA	USA	kA	USA
už	už	k6eAd1	už
nebudou	být	k5eNaImBp3nP	být
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
začali	začít	k5eAaPmAgMnP	začít
další	další	k2eAgFnSc4d1	další
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
opatrně	opatrně	k6eAd1	opatrně
<g/>
,	,	kIx,	,
zkoušejíce	zkoušet	k5eAaImSgMnP	zkoušet
reakce	reakce	k1gFnPc4	reakce
Jihovietnamců	Jihovietnamec	k1gMnPc2	Jihovietnamec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
postiženi	postižen	k2eAgMnPc1d1	postižen
nejen	nejen	k6eAd1	nejen
nízkou	nízký	k2eAgFnSc7d1	nízká
morálkou	morálka	k1gFnSc7	morálka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k9	už
i	i	k9	i
nedostatkem	nedostatek	k1gInSc7	nedostatek
náhradních	náhradní	k2eAgInPc2d1	náhradní
dílů	díl	k1gInPc2	díl
a	a	k8xC	a
munice	munice	k1gFnSc2	munice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
teď	teď	k6eAd1	teď
už	už	k6eAd1	už
velmi	velmi	k6eAd1	velmi
nedostatečně	dostatečně	k6eNd1	dostatečně
zásobovaly	zásobovat	k5eAaImAgFnP	zásobovat
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
února	únor	k1gInSc2	únor
nečekaně	nečekaně	k6eAd1	nečekaně
padla	padnout	k5eAaImAgFnS	padnout
po	po	k7c6	po
týdenních	týdenní	k2eAgInPc6d1	týdenní
bojích	boj	k1gInPc6	boj
Centrální	centrální	k2eAgFnSc1d1	centrální
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
žádná	žádný	k3yNgFnSc1	žádný
šance	šance	k1gFnSc1	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zachránit	zachránit	k5eAaPmF	zachránit
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
padlo	padnout	k5eAaPmAgNnS	padnout
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bránit	bránit	k5eAaImF	bránit
jen	jen	k9	jen
pobřežní	pobřežní	k2eAgFnPc4d1	pobřežní
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Proudy	proud	k1gInPc1	proud
utečenců	utečenec	k1gMnPc2	utečenec
z	z	k7c2	z
Centrální	centrální	k2eAgFnSc2d1	centrální
vysočiny	vysočina	k1gFnSc2	vysočina
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ustupujícími	ustupující	k2eAgMnPc7d1	ustupující
vojáky	voják	k1gMnPc7	voják
staly	stát	k5eAaPmAgInP	stát
lehkým	lehký	k2eAgInSc7d1	lehký
terčem	terč	k1gInSc7	terč
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
blížily	blížit	k5eAaImAgInP	blížit
k	k	k7c3	k
Tuy	Tuy	k1gFnSc3	Tuy
Hoa	Hoa	k1gFnSc2	Hoa
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
200	[number]	k4	200
000	[number]	k4	000
dorazila	dorazit	k5eAaPmAgFnS	dorazit
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
zhruba	zhruba	k6eAd1	zhruba
třetina	třetina	k1gFnSc1	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
jednotky	jednotka	k1gFnPc1	jednotka
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
pěti	pět	k4xCc2	pět
divizí	divize	k1gFnPc2	divize
VLA	VLA	kA	VLA
prorazily	prorazit	k5eAaPmAgInP	prorazit
přes	přes	k7c4	přes
Quang	Quang	k1gInSc4	Quang
Tri	Tri	k1gMnSc2	Tri
a	a	k8xC	a
blížily	blížit	k5eAaImAgFnP	blížit
se	se	k3xPyFc4	se
k	k	k7c3	k
Hue	Hue	k1gFnSc3	Hue
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
jednotky	jednotka	k1gFnPc4	jednotka
přeťaly	přetít	k5eAaPmAgInP	přetít
cestu	cesta	k1gFnSc4	cesta
č.	č.	k?	č.
1	[number]	k4	1
mezi	mezi	k7c7	mezi
Hue	Hue	k1gMnSc7	Hue
a	a	k8xC	a
Da	Da	k1gMnSc7	Da
Nangem	Nang	k1gMnSc7	Nang
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
zpečetily	zpečetit	k5eAaPmAgInP	zpečetit
osud	osud	k1gInSc4	osud
starého	starý	k2eAgNnSc2d1	staré
císařského	císařský	k2eAgNnSc2d1	císařské
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
komunisté	komunista	k1gMnPc1	komunista
dobyli	dobýt	k5eAaPmAgMnP	dobýt
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
demoralizovaných	demoralizovaný	k2eAgFnPc2d1	demoralizovaná
jednotek	jednotka	k1gFnPc2	jednotka
ARVN	ARVN	kA	ARVN
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgMnS	soustředit
okolo	okolo	k7c2	okolo
Da	Da	k1gFnSc2	Da
Nangu	Nang	k1gInSc2	Nang
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
přeplněné	přeplněný	k2eAgNnSc1d1	přeplněné
utečenci	utečenec	k1gMnPc7	utečenec
a	a	k8xC	a
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
město	město	k1gNnSc4	město
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
4	[number]	k4	4
divize	divize	k1gFnSc2	divize
VLA	VLA	kA	VLA
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc2	jejich
útoku	útok	k1gInSc2	útok
však	však	k9	však
předcházela	předcházet	k5eAaImAgFnS	předcházet
silná	silný	k2eAgFnSc1d1	silná
dělostřelecká	dělostřelecký	k2eAgFnSc1d1	dělostřelecká
příprava	příprava	k1gFnSc1	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
chaos	chaos	k1gInSc1	chaos
a	a	k8xC	a
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ho	on	k3xPp3gMnSc4	on
opustila	opustit	k5eAaPmAgFnS	opustit
většina	většina	k1gFnSc1	většina
velících	velící	k2eAgMnPc2d1	velící
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
100	[number]	k4	100
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
ARVN	ARVN	kA	ARVN
a	a	k8xC	a
domobrany	domobrana	k1gFnSc2	domobrana
v	v	k7c6	v
městě	město	k1gNnSc6	město
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
žádnou	žádný	k3yNgFnSc4	žádný
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
padlo	padnout	k5eAaImAgNnS	padnout
prakticky	prakticky	k6eAd1	prakticky
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Jihovietnamci	Jihovietnamec	k1gMnPc1	Jihovietnamec
dočasně	dočasně	k6eAd1	dočasně
zastavili	zastavit	k5eAaPmAgMnP	zastavit
komunisty	komunista	k1gMnSc2	komunista
60	[number]	k4	60
km	km	kA	km
od	od	k7c2	od
Saigonu	Saigon	k1gInSc2	Saigon
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
po	po	k7c6	po
týdenních	týdenní	k2eAgInPc6d1	týdenní
bojích	boj	k1gInPc6	boj
i	i	k8xC	i
tato	tento	k3xDgFnSc1	tento
obranná	obranný	k2eAgFnSc1d1	obranná
linie	linie	k1gFnSc1	linie
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
přesile	přesila	k1gFnSc3	přesila
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
boje	boj	k1gInPc4	boj
o	o	k7c4	o
Saigon	Saigon	k1gInSc4	Saigon
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pád	Pád	k1gInSc1	Pád
Saigonu	Saigon	k1gInSc2	Saigon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vietnamskému	vietnamský	k2eAgNnSc3d1	vietnamské
pobřeží	pobřeží	k1gNnSc3	pobřeží
se	se	k3xPyFc4	se
přiblížily	přiblížit	k5eAaPmAgFnP	přiblížit
lodě	loď	k1gFnPc1	loď
americké	americký	k2eAgFnSc2d1	americká
7	[number]	k4	7
<g/>
.	.	kIx.	.
floty	flota	k1gFnSc2	flota
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
úlohou	úloha	k1gFnSc7	úloha
byla	být	k5eAaImAgFnS	být
evakuace	evakuace	k1gFnSc1	evakuace
amerických	americký	k2eAgMnPc2d1	americký
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
aspoň	aspoň	k9	aspoň
části	část	k1gFnPc1	část
Vietnamců	Vietnamec	k1gMnPc2	Vietnamec
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
by	by	kYmCp3nP	by
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
země	zem	k1gFnSc2	zem
čekala	čekat	k5eAaImAgFnS	čekat
nejistá	jistý	k2eNgFnSc1d1	nejistá
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Kódové	kódový	k2eAgNnSc1d1	kódové
jméno	jméno	k1gNnSc1	jméno
této	tento	k3xDgFnSc2	tento
operace	operace	k1gFnSc2	operace
znělo	znět	k5eAaImAgNnS	znět
Frequent	Frequent	k1gInSc4	Frequent
Wind	Winda	k1gFnPc2	Winda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
americkou	americký	k2eAgFnSc7d1	americká
silou	síla	k1gFnSc7	síla
byly	být	k5eAaImAgFnP	být
letadlové	letadlový	k2eAgFnPc1d1	letadlová
lodě	loď	k1gFnPc1	loď
USS	USS	kA	USS
Hancock	Hancocka	k1gFnPc2	Hancocka
(	(	kIx(	(
<g/>
CV-	CV-	k1gFnSc1	CV-
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USS	USS	kA	USS
Midway	Midway	k1gInPc1	Midway
(	(	kIx(	(
<g/>
CV-	CV-	k1gFnSc1	CV-
<g/>
41	[number]	k4	41
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USS	USS	kA	USS
Enterprise	Enterprise	k1gFnSc1	Enterprise
(	(	kIx(	(
<g/>
CVN-	CVN-	k1gFnSc1	CVN-
<g/>
65	[number]	k4	65
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USS	USS	kA	USS
Coral	Coral	k1gMnSc1	Coral
Sea	Sea	k1gMnSc1	Sea
(	(	kIx(	(
<g/>
CV-	CV-	k1gFnSc1	CV-
<g/>
43	[number]	k4	43
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrtulníková	vrtulníkový	k2eAgFnSc1d1	vrtulníková
výsadková	výsadkový	k2eAgFnSc1d1	výsadková
loď	loď	k1gFnSc1	loď
USS	USS	kA	USS
Okinawa	Okinawa	k1gFnSc1	Okinawa
(	(	kIx(	(
<g/>
LPH-	LPH-	k1gFnSc1	LPH-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přistávací	přistávací	k2eAgFnPc1d1	přistávací
zóny	zóna	k1gFnPc1	zóna
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Tan	Tan	k1gFnSc1	Tan
Son	son	k1gInSc4	son
Nhut	Nhut	k2eAgInSc1d1	Nhut
a	a	k8xC	a
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
amerického	americký	k2eAgNnSc2d1	americké
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
v	v	k7c6	v
Saigonu	Saigon	k1gInSc6	Saigon
<g/>
.	.	kIx.	.
</s>
<s>
Američanům	Američan	k1gMnPc3	Američan
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
evakuovat	evakuovat	k5eAaBmF	evakuovat
přes	přes	k7c4	přes
8000	[number]	k4	8000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
přistávacích	přistávací	k2eAgFnPc2d1	přistávací
zón	zóna	k1gFnPc2	zóna
se	se	k3xPyFc4	se
tlačily	tlačit	k5eAaImAgInP	tlačit
obrovské	obrovský	k2eAgInPc1d1	obrovský
davy	dav	k1gInPc1	dav
vietnamských	vietnamský	k2eAgMnPc2d1	vietnamský
civilistů	civilista	k1gMnPc2	civilista
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odletu	odlet	k1gInSc6	odlet
posledních	poslední	k2eAgInPc2d1	poslední
vrtulníků	vrtulník	k1gInPc2	vrtulník
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
američtí	americký	k2eAgMnPc1d1	americký
námořní	námořní	k2eAgMnPc1d1	námořní
pěšáci	pěšák	k1gMnPc1	pěšák
před	před	k7c7	před
rozhněvanými	rozhněvaný	k2eAgInPc7d1	rozhněvaný
zástupy	zástup	k1gInPc7	zástup
bránit	bránit	k5eAaImF	bránit
slzným	slzný	k2eAgInSc7d1	slzný
plynem	plyn	k1gInSc7	plyn
a	a	k8xC	a
pažbami	pažba	k1gFnPc7	pažba
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Tan	Tan	k1gFnSc1	Tan
Son	son	k1gInSc4	son
Nhut	Nhut	k1gInSc1	Nhut
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
i	i	k9	i
poslední	poslední	k2eAgFnPc1d1	poslední
americké	americký	k2eAgFnPc1d1	americká
ztráty	ztráta	k1gFnPc1	ztráta
<g/>
,	,	kIx,	,
když	když	k8xS	když
severovietnamské	severovietnamský	k2eAgInPc1d1	severovietnamský
dělostřelecké	dělostřelecký	k2eAgInPc1d1	dělostřelecký
granáty	granát	k1gInPc1	granát
usmrtily	usmrtit	k5eAaPmAgInP	usmrtit
dva	dva	k4xCgInPc1	dva
příslušníky	příslušník	k1gMnPc7	příslušník
Námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k9	ani
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ještě	ještě	k6eAd1	ještě
nebylo	být	k5eNaImAgNnS	být
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgInPc1d1	americký
a	a	k8xC	a
vietnamské	vietnamský	k2eAgInPc1d1	vietnamský
vrtulníky	vrtulník	k1gInPc1	vrtulník
přivážely	přivážet	k5eAaImAgInP	přivážet
utečence	utečenka	k1gFnSc3	utečenka
na	na	k7c4	na
letadlové	letadlový	k2eAgFnPc4d1	letadlová
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
paluby	paluba	k1gFnPc1	paluba
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
přeplnily	přeplnit	k5eAaPmAgFnP	přeplnit
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
překážející	překážející	k2eAgInPc1d1	překážející
vrtulníky	vrtulník	k1gInPc7	vrtulník
(	(	kIx(	(
<g/>
mající	mající	k2eAgFnSc4d1	mající
hodnotu	hodnota	k1gFnSc4	hodnota
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250	[number]	k4	250
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
shazovat	shazovat	k5eAaImF	shazovat
z	z	k7c2	z
palub	paluba	k1gFnPc2	paluba
lodí	loď	k1gFnPc2	loď
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
USA	USA	kA	USA
ztratily	ztratit	k5eAaPmAgFnP	ztratit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
1962	[number]	k4	1962
-	-	kIx~	-
1973	[number]	k4	1973
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
4	[number]	k4	4
860	[number]	k4	860
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2588	[number]	k4	2588
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
však	však	k9	však
evakuovat	evakuovat	k5eAaBmF	evakuovat
1	[number]	k4	1
373	[number]	k4	373
amerických	americký	k2eAgMnPc2d1	americký
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
955	[number]	k4	955
Vietnamců	Vietnamec	k1gMnPc2	Vietnamec
<g/>
.	.	kIx.	.
</s>
<s>
Marnou	marný	k2eAgFnSc4d1	marná
obranu	obrana	k1gFnSc4	obrana
Saigonu	Saigon	k1gInSc2	Saigon
prorazilo	prorazit	k5eAaPmAgNnS	prorazit
10	[number]	k4	10
komunistických	komunistický	k2eAgFnPc2d1	komunistická
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Dělostřelecká	dělostřelecký	k2eAgFnSc1d1	dělostřelecká
palba	palba	k1gFnSc1	palba
ustala	ustat	k5eAaPmAgFnS	ustat
a	a	k8xC	a
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
valily	valit	k5eAaImAgInP	valit
tanky	tank	k1gInPc1	tank
T-54	T-54	k1gFnSc7	T-54
od	od	k7c2	od
203	[number]	k4	203
<g/>
.	.	kIx.	.
tankového	tankový	k2eAgInSc2d1	tankový
pluku	pluk	k1gInSc2	pluk
armády	armáda	k1gFnSc2	armáda
VDR	VDR	kA	VDR
<g/>
.	.	kIx.	.
</s>
<s>
Tank	tank	k1gInSc1	tank
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
843	[number]	k4	843
prorazil	prorazit	k5eAaPmAgMnS	prorazit
bránu	brána	k1gFnSc4	brána
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
paláce	palác	k1gInSc2	palác
Doc	doc	kA	doc
Lap	lap	k1gInSc1	lap
a	a	k8xC	a
ve	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
zavlála	zavlát	k5eAaPmAgFnS	zavlát
nad	nad	k7c7	nad
prezidentským	prezidentský	k2eAgInSc7d1	prezidentský
palácem	palác	k1gInSc7	palác
vlajka	vlajka	k1gFnSc1	vlajka
Vietkongu	Vietkong	k1gInSc2	Vietkong
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
skončila	skončit	k5eAaPmAgFnS	skončit
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
opozicí	opozice	k1gFnSc7	opozice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
krátce	krátce	k6eAd1	krátce
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
likvidací	likvidace	k1gFnSc7	likvidace
opozice	opozice	k1gFnSc1	opozice
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1976	[number]	k4	1976
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
sjednocením	sjednocení	k1gNnSc7	sjednocení
Vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
republiky	republika	k1gFnSc2	republika
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
strana	strana	k1gFnSc1	strana
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
války	válka	k1gFnSc2	válka
ztratila	ztratit	k5eAaPmAgFnS	ztratit
58	[number]	k4	58
202	[number]	k4	202
vojáků	voják	k1gMnPc2	voják
(	(	kIx(	(
<g/>
asi	asi	k9	asi
8	[number]	k4	8
000	[number]	k4	000
padlých	padlý	k1gMnPc2	padlý
tvořili	tvořit	k5eAaImAgMnP	tvořit
letci	letec	k1gMnPc1	letec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
2400	[number]	k4	2400
pohřešovaných	pohřešovaná	k1gFnPc2	pohřešovaná
<g/>
.	.	kIx.	.
300	[number]	k4	300
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
bylo	být	k5eAaImAgNnS	být
raněných	raněný	k2eAgMnPc2d1	raněný
<g/>
,	,	kIx,	,
153	[number]	k4	153
000	[number]	k4	000
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
těžce	těžce	k6eAd1	těžce
<g/>
.	.	kIx.	.
10	[number]	k4	10
000	[number]	k4	000
Američanů	Američan	k1gMnPc2	Američan
muselo	muset	k5eAaImAgNnS	muset
podstoupit	podstoupit	k5eAaPmF	podstoupit
amputaci	amputace	k1gFnSc4	amputace
končetiny	končetina	k1gFnSc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
raněných	raněný	k1gMnPc2	raněný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přežili	přežít	k5eAaPmAgMnP	přežít
o	o	k7c6	o
300	[number]	k4	300
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
800	[number]	k4	800
Američanů	Američan	k1gMnPc2	Američan
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
při	při	k7c6	při
nebojových	bojový	k2eNgFnPc6d1	nebojová
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
havárie	havárie	k1gFnPc1	havárie
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
,	,	kIx,	,
letadel	letadlo	k1gNnPc2	letadlo
nebo	nebo	k8xC	nebo
při	při	k7c6	při
dopravních	dopravní	k2eAgFnPc6d1	dopravní
nehodách	nehoda	k1gFnPc6	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
<g/>
%	%	kIx~	%
amerických	americký	k2eAgFnPc2d1	americká
ztrát	ztráta	k1gFnPc2	ztráta
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
padlých	padlý	k1gMnPc2	padlý
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
palbou	palba	k1gFnSc7	palba
<g/>
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
s	s	k7c7	s
heslem	heslo	k1gNnSc7	heslo
boje	boj	k1gInSc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
i	i	k9	i
zájmy	zájem	k1gInPc4	zájem
politické	politický	k2eAgInPc4d1	politický
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
po	po	k7c6	po
světě	svět	k1gInSc6	svět
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
či	či	k8xC	či
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
komunismu	komunismus	k1gInSc3	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
její	její	k3xOp3gNnSc4	její
armáda	armáda	k1gFnSc1	armáda
neprohrála	prohrát	k5eNaPmAgFnS	prohrát
žádnou	žádný	k3yNgFnSc4	žádný
větší	veliký	k2eAgFnSc4d2	veliký
bitvu	bitva	k1gFnSc4	bitva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
poražena	poražen	k2eAgFnSc1d1	poražena
díky	díky	k7c3	díky
prohrám	prohra	k1gFnPc3	prohra
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Vietnamem	Vietnam	k1gInSc7	Vietnam
prošlo	projít	k5eAaPmAgNnS	projít
2,6	[number]	k4	2,6
milionu	milion	k4xCgInSc2	milion
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
až	až	k9	až
1,6	[number]	k4	1,6
milionu	milion	k4xCgInSc2	milion
bylo	být	k5eAaImAgNnS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
pravidelně	pravidelně	k6eAd1	pravidelně
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vícekrát	vícekrát	k6eAd1	vícekrát
setkalo	setkat	k5eAaPmAgNnS	setkat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
60	[number]	k4	60
000	[number]	k4	000
veteránů	veterán	k1gMnPc2	veterán
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
spáchalo	spáchat	k5eAaPmAgNnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
nebo	nebo	k8xC	nebo
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c6	na
předávkování	předávkování	k1gNnSc6	předávkování
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
si	se	k3xPyFc3	se
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
nemohlo	moct	k5eNaImAgNnS	moct
najít	najít	k5eAaPmF	najít
adekvátní	adekvátní	k2eAgNnSc4d1	adekvátní
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
Američana	Američan	k1gMnSc2	Američan
bojujícího	bojující	k2eAgMnSc2d1	bojující
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
byl	být	k5eAaImAgInS	být
19	[number]	k4	19
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
61	[number]	k4	61
<g/>
%	%	kIx~	%
padlých	padlý	k1gMnPc2	padlý
bylo	být	k5eAaImAgNnS	být
mladších	mladý	k2eAgNnPc2d2	mladší
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
82	[number]	k4	82
<g/>
%	%	kIx~	%
amerických	americký	k2eAgMnPc2d1	americký
veteránů	veterán	k1gMnPc2	veterán
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
těžkých	těžký	k2eAgInPc2d1	těžký
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
válku	válka	k1gFnSc4	válka
prohráli	prohrát	k5eAaPmAgMnP	prohrát
pouze	pouze	k6eAd1	pouze
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
politické	politický	k2eAgFnSc2d1	politická
vůle	vůle	k1gFnSc2	vůle
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
představitelů	představitel	k1gMnPc2	představitel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
i	i	k9	i
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
let	léto	k1gNnPc2	léto
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
postavena	postaven	k2eAgFnSc1d1	postavena
"	"	kIx"	"
<g/>
Stěna	stěna	k1gFnSc1	stěna
<g/>
"	"	kIx"	"
-	-	kIx~	-
památník	památník	k1gInSc1	památník
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
jsou	být	k5eAaImIp3nP	být
zapsána	zapsat	k5eAaPmNgNnP	zapsat
jména	jméno	k1gNnSc2	jméno
všech	všecek	k3xTgMnPc2	všecek
padlých	padlý	k1gMnPc2	padlý
a	a	k8xC	a
nezvěstných	zvěstný	k2eNgMnPc2d1	nezvěstný
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
bojovali	bojovat	k5eAaImAgMnP	bojovat
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Zelené	Zelené	k2eAgInPc1d1	Zelené
barety	baret	k1gInPc1	baret
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Lovec	lovec	k1gMnSc1	lovec
jelenů	jelen	k1gMnPc2	jelen
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Apokalypsa	apokalypsa	k1gFnSc1	apokalypsa
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Četa	četa	k1gFnSc1	četa
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Olověná	olověný	k2eAgFnSc1d1	olověná
vesta	vesta	k1gFnSc1	vesta
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Hamburger	hamburger	k1gInSc1	hamburger
Hill	Hill	k1gMnSc1	Hill
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Oběti	oběť	k1gFnPc1	oběť
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Narozen	narozen	k2eAgInSc1d1	narozen
4	[number]	k4	4
<g/>
<g />
.	.	kIx.	.
</s>
<s>
července	červenec	k1gInPc1	červenec
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Železný	železný	k2eAgInSc1d1	železný
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Jasná	jasný	k2eAgFnSc1d1	jasná
lež	lež	k1gFnSc1	lež
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Tábor	Tábor	k1gInSc1	Tábor
tygrů	tygr	k1gMnPc2	tygr
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Údolí	údolí	k1gNnSc1	údolí
stínů	stín	k1gInPc2	stín
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
-	-	kIx~	-
I.	I.	kA	I.
<g/>
-	-	kIx~	-
XIX	XIX	kA	XIX
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
palba	palba	k1gFnSc1	palba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Záběry	záběr	k1gInPc4	záběr
z	z	k7c2	z
USS	USS	kA	USS
Midway	Midwaa	k1gFnSc2	Midwaa
po	po	k7c6	po
evakuaci	evakuace	k1gFnSc6	evakuace
Saigonu	Saigon	k1gInSc2	Saigon
rozlobenimuzi	rozlobenimuze	k1gFnSc4	rozlobenimuze
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
