<s>
Lesní	lesní	k2eAgInSc1d1	lesní
roh	roh	k1gInSc1	roh
je	být	k5eAaImIp3nS	být
žesťový	žesťový	k2eAgInSc1d1	žesťový
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
kónickou	kónický	k2eAgFnSc7d1	kónická
trubicí	trubice	k1gFnSc7	trubice
<g/>
,	,	kIx,	,
pevné	pevný	k2eAgNnSc1d1	pevné
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
orchestrech	orchestr	k1gInPc6	orchestr
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
