<p>
<s>
Lesní	lesní	k2eAgInSc1d1	lesní
roh	roh	k1gInSc1	roh
je	být	k5eAaImIp3nS	být
žesťový	žesťový	k2eAgInSc1d1	žesťový
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
kónickou	kónický	k2eAgFnSc7d1	kónická
trubicí	trubice	k1gFnSc7	trubice
<g/>
,	,	kIx,	,
pevné	pevný	k2eAgNnSc1d1	pevné
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
orchestrech	orchestr	k1gInPc6	orchestr
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
loveckých	lovecký	k2eAgInPc2d1	lovecký
nástrojů	nástroj	k1gInPc2	nástroj
používaných	používaný	k2eAgInPc2d1	používaný
pro	pro	k7c4	pro
hraní	hraní	k1gNnSc4	hraní
fanfár	fanfára	k1gFnPc2	fanfára
a	a	k8xC	a
signálů	signál	k1gInPc2	signál
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
orchestrech	orchestr	k1gInPc6	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
je	být	k5eAaImIp3nS	být
opatřován	opatřován	k2eAgInSc4d1	opatřován
ventily	ventil	k1gInPc4	ventil
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hrát	hrát	k5eAaImF	hrát
celý	celý	k2eAgInSc4d1	celý
chromatický	chromatický	k2eAgInSc4d1	chromatický
rozsah	rozsah	k1gInSc4	rozsah
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
stočený	stočený	k2eAgInSc1d1	stočený
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
zvuková	zvukový	k2eAgFnSc1d1	zvuková
trubice	trubice	k1gFnSc1	trubice
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
přes	přes	k7c4	přes
4	[number]	k4	4
metry	metr	k1gInPc4	metr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
širokým	široký	k2eAgInSc7d1	široký
ozvučníkem	ozvučník	k1gInSc7	ozvučník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
ozvučníkem	ozvučník	k1gInSc7	ozvučník
vpravo	vpravo	k6eAd1	vpravo
dolů	dolů	k6eAd1	dolů
<g/>
:	:	kIx,	:
pravá	pravý	k2eAgFnSc1d1	pravá
ruka	ruka	k1gFnSc1	ruka
jej	on	k3xPp3gMnSc4	on
podpírá	podpírat	k5eAaImIp3nS	podpírat
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
ventilovou	ventilový	k2eAgFnSc4d1	ventilová
soustavu	soustava	k1gFnSc4	soustava
(	(	kIx(	(
<g/>
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
ventily	ventil	k1gInPc7	ventil
<g/>
)	)	kIx)	)
uzpůsobenou	uzpůsobený	k2eAgFnSc4d1	uzpůsobená
pro	pro	k7c4	pro
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
žesťových	žesťový	k2eAgInPc2d1	žesťový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
tónový	tónový	k2eAgInSc4d1	tónový
rozsah	rozsah	k1gInSc4	rozsah
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgFnPc1	čtyři
oktávy	oktáva	k1gFnPc1	oktáva
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
zdatnosti	zdatnost	k1gFnSc2	zdatnost
hráče	hráč	k1gMnSc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejpoužívanější	používaný	k2eAgInPc1d3	nejpoužívanější
jsou	být	k5eAaImIp3nP	být
dvojité	dvojitý	k2eAgInPc4d1	dvojitý
lesní	lesní	k2eAgInPc4d1	lesní
rohy	roh	k1gInPc4	roh
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
integrují	integrovat	k5eAaBmIp3nP	integrovat
roh	roh	k1gInSc4	roh
v	v	k7c6	v
F	F	kA	F
ladění	ladění	k1gNnSc4	ladění
a	a	k8xC	a
roh	roh	k1gInSc4	roh
v	v	k7c6	v
ladění	ladění	k1gNnSc6	ladění
o	o	k7c6	o
kvartu	kvart	k1gInSc6	kvart
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
v	v	k7c6	v
B	B	kA	B
<g/>
;	;	kIx,	;
hráč	hráč	k1gMnSc1	hráč
je	on	k3xPp3gInPc4	on
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
pomocí	pomocí	k7c2	pomocí
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
klapky	klapka	k1gFnSc2	klapka
ovládané	ovládaný	k2eAgFnSc2d1	ovládaná
palcem	palec	k1gInSc7	palec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
tónový	tónový	k2eAgInSc4d1	tónový
rozsah	rozsah	k1gInSc4	rozsah
nástroje	nástroj	k1gInSc2	nástroj
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jistější	jistý	k2eAgMnSc1d2	jistější
hraní	hraní	k1gNnSc4	hraní
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Vrabec	Vrabec	k1gMnSc1	Vrabec
–	–	k?	–
hornová	hornová	k1gFnSc1	hornová
klinika	klinika	k1gFnSc1	klinika
(	(	kIx(	(
<g/>
osobní	osobní	k2eAgFnPc4d1	osobní
stránky	stránka	k1gFnPc4	stránka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Lovecká	lovecký	k2eAgFnSc1d1	lovecká
trubka	trubka	k1gFnSc1	trubka
</s>
</p>
<p>
<s>
Lov	lov	k1gInSc1	lov
</s>
</p>
<p>
<s>
Šofar	Šofar	k1gMnSc1	Šofar
</s>
</p>
