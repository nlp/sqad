<s>
Tandemový	tandemový	k2eAgInSc1d1
přívěs	přívěs	k1gInSc1
</s>
<s>
Tandemový	tandemový	k2eAgInSc1d1
přívěs	přívěs	k1gInSc1
nebo	nebo	k8xC
také	také	k9
přívěs	přívěs	k1gInSc1
s	s	k7c7
centrálními	centrální	k2eAgFnPc7d1
nápravami	náprava	k1gFnPc7
je	být	k5eAaImIp3nS
přívěsem	přívěs	k1gInSc7
za	za	k7c4
nákladní	nákladní	k2eAgInSc4d1
či	či	k8xC
osobní	osobní	k2eAgInSc4d1
automobil	automobil	k1gInSc4
<g/>
,	,	kIx,
autobus	autobus	k1gInSc4
nebo	nebo	k8xC
jiné	jiný	k2eAgNnSc4d1
motorové	motorový	k2eAgNnSc4d1
vozidlo	vozidlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
charakteristické	charakteristický	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
nápravy	náprava	k1gFnPc4
uprostřed	uprostřed	k7c2
vozidla	vozidlo	k1gNnSc2
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
přívěsu	přívěs	k1gInSc2
s	s	k7c7
rejdovou	rejdův	k2eAgFnSc7d1
ojí	oj	k1gFnSc7
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
točnicového	točnicový	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	on	k3xPp3gNnSc4
má	mít	k5eAaImIp3nS
v	v	k7c6
přední	přední	k2eAgFnSc6d1
a	a	k8xC
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3
možné	možný	k2eAgNnSc1d1
povolené	povolený	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
přívěsu	přívěs	k1gInSc2
je	být	k5eAaImIp3nS
při	při	k7c6
dvounápravovém	dvounápravový	k2eAgNnSc6d1
uspořádání	uspořádání	k1gNnSc6
18	#num#	k4
t	t	k?
a	a	k8xC
při	při	k7c6
třínápravovém	třínápravový	k2eAgNnSc6d1
uspořádání	uspořádání	k1gNnSc6
24	#num#	k4
t.	t.	k?
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Základní	základní	k2eAgFnSc7d1
výhodou	výhoda	k1gFnSc7
tandemového	tandemový	k2eAgInSc2d1
přívěsu	přívěs	k1gInSc2
je	být	k5eAaImIp3nS
snadnost	snadnost	k1gFnSc1
manipulace	manipulace	k1gFnSc2
s	s	k7c7
ním	on	k3xPp3gNnSc7
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
při	při	k7c6
couvání	couvání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nakládce	nakládka	k1gFnSc6
a	a	k8xC
vykládce	vykládka	k1gFnSc6
samostatného	samostatný	k2eAgInSc2d1
přívěsu	přívěs	k1gInSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
provedeno	proveden	k2eAgNnSc1d1
zajištění	zajištění	k1gNnSc1
pomocí	pomocí	k7c2
pevných	pevný	k2eAgFnPc2d1
podpěr	podpěra	k1gFnPc2
vepředu	vepředu	k6eAd1
a	a	k8xC
vzadu	vzadu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklad	náklad	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
rozmístěn	rozmístit	k5eAaPmNgInS
rovnoměrně	rovnoměrně	k6eAd1
s	s	k7c7
těžištěm	těžiště	k1gNnSc7
nad	nad	k7c7
nápravami	náprava	k1gFnPc7
a	a	k8xC
řádně	řádně	k6eAd1
zajištěn	zajistit	k5eAaPmNgInS
fixačními	fixační	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Připojení	připojení	k1gNnSc1
a	a	k8xC
odpojení	odpojení	k1gNnSc1
přívěsu	přívěs	k1gInSc2
probíhá	probíhat	k5eAaImIp3nS
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
přívěsu	přívěs	k1gInSc2
s	s	k7c7
točnicí	točnice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
nacouváním	nacouvání	k1gNnSc7
tažného	tažný	k2eAgNnSc2d1
vozidla	vozidlo	k1gNnSc2
na	na	k7c4
oj	oj	k1gInSc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
nastavena	nastavit	k5eAaPmNgFnS
příslušná	příslušný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
oje	oj	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zapadla	zapadnout	k5eAaPmAgFnS
do	do	k7c2
tažného	tažný	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
jízdou	jízda	k1gFnSc7
s	s	k7c7
přívěsem	přívěs	k1gInSc7
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
provést	provést	k5eAaPmF
kontrolu	kontrola	k1gFnSc4
zapojení	zapojení	k1gNnSc2
a	a	k8xC
kontrolu	kontrola	k1gFnSc4
brzdného	brzdný	k2eAgInSc2d1
a	a	k8xC
elektrického	elektrický	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
zvl.	zvl.	kA
světel	světlo	k1gNnPc2
<g/>
)	)	kIx)
přívěsu	přívěs	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
poté	poté	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
zasunuty	zasunut	k2eAgFnPc4d1
pevné	pevný	k2eAgFnPc4d1
podpěry	podpěra	k1gFnPc4
přívěsu	přívěs	k1gInSc2
a	a	k8xC
umožněna	umožněn	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
<g/>
.	.	kIx.
</s>
