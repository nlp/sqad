<s>
Gangrenózní	gangrenózní	k2eAgFnSc1d1	gangrenózní
dermatitida	dermatitida	k1gFnSc1	dermatitida
(	(	kIx(	(
<g/>
GD	GD	kA	GD
<g/>
)	)	kIx)	)
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
sporadicky	sporadicky	k6eAd1	sporadicky
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgNnSc1d1	vyskytující
onemocnění	onemocnění	k1gNnSc1	onemocnění
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
<g/>
,	,	kIx,	,
charakterizované	charakterizovaný	k2eAgNnSc1d1	charakterizované
nekrózou	nekróza	k1gFnSc7	nekróza
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
podkoží	podkoží	k1gNnSc2	podkoží
a	a	k8xC	a
svalstva	svalstvo	k1gNnSc2	svalstvo
<g/>
.	.	kIx.	.
</s>
<s>
Etiologickým	etiologický	k2eAgNnSc7d1	etiologické
agens	agens	k1gNnSc7	agens
bývají	bývat	k5eAaImIp3nP	bývat
bakterie	bakterie	k1gFnPc1	bakterie
Clostridium	Clostridium	k1gNnSc4	Clostridium
septicum	septicum	k1gInSc1	septicum
<g/>
,	,	kIx,	,
C.	C.	kA	C.
perfringens	perfringens	k1gInSc4	perfringens
typu	typ	k1gInSc2	typ
A	a	k9	a
a	a	k8xC	a
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
jednotlivě	jednotlivě	k6eAd1	jednotlivě
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Smíšené	smíšený	k2eAgFnPc1d1	smíšená
infekce	infekce	k1gFnPc1	infekce
obecně	obecně	k6eAd1	obecně
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
klinicky	klinicky	k6eAd1	klinicky
těžší	těžký	k2eAgFnSc6d2	těžší
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
jako	jako	k8xS	jako
příčina	příčina	k1gFnSc1	příčina
gangrenózní	gangrenózní	k2eAgFnSc2d1	gangrenózní
dermatitidy	dermatitida	k1gFnSc2	dermatitida
popisovány	popisován	k2eAgFnPc1d1	popisována
C.	C.	kA	C.
sordellii	sordellie	k1gFnSc4	sordellie
<g/>
,	,	kIx,	,
C.	C.	kA	C.
novyi	novy	k1gFnSc2	novy
a	a	k8xC	a
C.	C.	kA	C.
sporogenes	sporogenes	k1gInSc1	sporogenes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
gangrenózní	gangrenózní	k2eAgFnSc1d1	gangrenózní
dermatitida	dermatitida	k1gFnSc1	dermatitida
následkem	následkem	k7c2	následkem
předchozí	předchozí	k2eAgFnSc2d1	předchozí
infekce	infekce	k1gFnSc2	infekce
imunosupresivními	imunosupresivní	k2eAgNnPc7d1	imunosupresivní
agens	agens	k1gNnPc7	agens
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
viry	vir	k1gInPc1	vir
infekční	infekční	k2eAgFnSc2d1	infekční
burzitidy	burzitis	k1gFnSc2	burzitis
a	a	k8xC	a
infekční	infekční	k2eAgFnSc2d1	infekční
anémie	anémie	k1gFnSc2	anémie
a	a	k8xC	a
ptačí	ptačí	k2eAgFnSc2d1	ptačí
adenoviry	adenovira	k1gFnSc2	adenovira
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
nekróza	nekróza	k1gFnSc1	nekróza
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
podkožní	podkožní	k2eAgFnSc2d1	podkožní
tkáně	tkáň	k1gFnSc2	tkáň
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
po	po	k7c6	po
intramuskulární	intramuskulární	k2eAgFnSc6d1	intramuskulární
aplikaci	aplikace	k1gFnSc6	aplikace
C.	C.	kA	C.
perfringens	perfringens	k6eAd1	perfringens
izolovaného	izolovaný	k2eAgMnSc2d1	izolovaný
ze	z	k7c2	z
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
jater	játra	k1gNnPc2	játra
dvou	dva	k4xCgNnPc2	dva
nemocných	nemocný	k2eAgNnPc2d1	nemocný
kuřat	kuře	k1gNnPc2	kuře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
byly	být	k5eAaImAgFnP	být
klostridiové	klostridiový	k2eAgFnPc1d1	klostridiová
dermatitidy	dermatitida	k1gFnPc1	dermatitida
popsány	popsat	k5eAaPmNgFnP	popsat
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
uhynulých	uhynulý	k2eAgNnPc2d1	uhynulé
po	po	k7c6	po
odběru	odběr	k1gInSc6	odběr
krve	krev	k1gFnSc2	krev
na	na	k7c6	na
testaci	testace	k1gFnSc6	testace
pulorové	pulorový	k2eAgFnSc2d1	pulorový
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
,	,	kIx,	,
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
uhynulých	uhynulý	k2eAgFnPc2d1	uhynulá
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
traumatizace	traumatizace	k1gFnSc2	traumatizace
a	a	k8xC	a
ranné	ranný	k2eAgFnSc2d1	ranná
infekce	infekce	k1gFnSc2	infekce
nebo	nebo	k8xC	nebo
z	z	k7c2	z
podkožního	podkožní	k2eAgInSc2d1	podkožní
edému	edém	k1gInSc2	edém
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
pak	pak	k6eAd1	pak
přibývají	přibývat	k5eAaImIp3nP	přibývat
další	další	k2eAgFnPc1d1	další
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
gangrenózní	gangrenózní	k2eAgFnSc2d1	gangrenózní
dermatitidy	dermatitida	k1gFnSc2	dermatitida
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
pod	pod	k7c7	pod
odlišnými	odlišný	k2eAgInPc7d1	odlišný
názvy	název	k1gInPc7	název
<g/>
)	)	kIx)	)
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
včetně	včetně	k7c2	včetně
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
gangrenózní	gangrenózní	k2eAgFnSc1d1	gangrenózní
dermatitida	dermatitida	k1gFnSc1	dermatitida
<g/>
"	"	kIx"	"
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
poprvé	poprvé	k6eAd1	poprvé
použili	použít	k5eAaPmAgMnP	použít
Frazier	Frazier	k1gInSc4	Frazier
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
GD	GD	kA	GD
jsou	být	k5eAaImIp3nP	být
klostridie	klostridie	k1gFnSc2	klostridie
(	(	kIx(	(
<g/>
Clostridium	Clostridium	k1gNnSc1	Clostridium
perfringens	perfringens	k6eAd1	perfringens
typ	typ	k1gInSc1	typ
A	A	kA	A
<g/>
,	,	kIx,	,
C.	C.	kA	C.
septicum	septicum	k1gInSc1	septicum
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
stafylokoky	stafylokok	k1gMnPc7	stafylokok
a	a	k8xC	a
E.	E.	kA	E.
coli	coli	k6eAd1	coli
<g/>
.	.	kIx.	.
</s>
<s>
Druhová	druhový	k2eAgFnSc1d1	druhová
diferenciace	diferenciace	k1gFnSc1	diferenciace
klostridií	klostridie	k1gFnPc2	klostridie
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
morfologicky	morfologicky	k6eAd1	morfologicky
<g/>
,	,	kIx,	,
kultivačně	kultivačně	k6eAd1	kultivačně
<g/>
,	,	kIx,	,
biochemicky	biochemicky	k6eAd1	biochemicky
<g/>
,	,	kIx,	,
sérologicky	sérologicky	k6eAd1	sérologicky
a	a	k8xC	a
v	v	k7c6	v
pokusech	pokus	k1gInPc6	pokus
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
septicum	septicum	k1gNnSc1	septicum
je	být	k5eAaImIp3nS	být
pohyblivé	pohyblivý	k2eAgNnSc1d1	pohyblivé
<g/>
,	,	kIx,	,
spory	spor	k1gInPc1	spor
jsou	být	k5eAaImIp3nP	být
oválné	oválný	k2eAgInPc1d1	oválný
a	a	k8xC	a
lokalizované	lokalizovaný	k2eAgInPc1d1	lokalizovaný
subterminálně	subterminálně	k6eAd1	subterminálně
<g/>
.	.	kIx.	.
</s>
<s>
Fermentuje	fermentovat	k5eAaBmIp3nS	fermentovat
glukózu	glukóza	k1gFnSc4	glukóza
<g/>
,	,	kIx,	,
maltózu	maltóza	k1gFnSc4	maltóza
<g/>
,	,	kIx,	,
laktózu	laktóza	k1gFnSc4	laktóza
a	a	k8xC	a
salicin	salicin	k1gInSc4	salicin
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
ale	ale	k9	ale
sacharózu	sacharóza	k1gFnSc4	sacharóza
a	a	k8xC	a
manitol	manitol	k1gInSc4	manitol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žloutkovém	žloutkový	k2eAgInSc6d1	žloutkový
agaru	agar	k1gInSc6	agar
netvoří	tvořit	k5eNaImIp3nP	tvořit
lecitinázu	lecitináza	k1gFnSc4	lecitináza
ani	ani	k8xC	ani
lipázu	lipáza	k1gFnSc4	lipáza
<g/>
.	.	kIx.	.
</s>
<s>
Intramuskulární	intramuskulární	k2eAgFnSc1d1	intramuskulární
aplikace	aplikace	k1gFnSc1	aplikace
C.	C.	kA	C.
septicum	septicum	k1gNnSc1	septicum
izolovaného	izolovaný	k2eAgInSc2d1	izolovaný
z	z	k7c2	z
kuřat	kuře	k1gNnPc2	kuře
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změny	změna	k1gFnPc4	změna
kolem	kolem	k7c2	kolem
místa	místo	k1gNnSc2	místo
vpichu	vpich	k1gInSc2	vpich
a	a	k8xC	a
úhyn	úhyn	k1gInSc4	úhyn
krůt	krůta	k1gFnPc2	krůta
během	během	k7c2	během
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Terénní	terénní	k2eAgInPc4d1	terénní
výskyty	výskyt	k1gInPc4	výskyt
gangrenózní	gangrenózní	k2eAgFnSc2d1	gangrenózní
dermatitidy	dermatitida	k1gFnSc2	dermatitida
(	(	kIx(	(
<g/>
ganrenózní	ganrenózní	k2eAgInSc1d1	ganrenózní
zánět	zánět	k1gInSc1	zánět
kůže	kůže	k1gFnSc2	kůže
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
a	a	k8xC	a
krůťat	krůtě	k1gNnPc2	krůtě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
17	[number]	k4	17
dní	den	k1gInPc2	den
do	do	k7c2	do
20	[number]	k4	20
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
u	u	k7c2	u
chovných	chovný	k2eAgFnPc2d1	chovná
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
ale	ale	k8xC	ale
bývají	bývat	k5eAaImIp3nP	bývat
postižena	postižen	k2eAgNnPc4d1	postiženo
výkrmová	výkrmový	k2eAgNnPc4d1	výkrmový
kuřata	kuře	k1gNnPc4	kuře
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
týdne	týden	k1gInSc2	týden
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
také	také	k9	také
vyloučit	vyloučit	k5eAaPmF	vyloučit
určitou	určitý	k2eAgFnSc4d1	určitá
genetickou	genetický	k2eAgFnSc4d1	genetická
predispozici	predispozice	k1gFnSc4	predispozice
pozorovanou	pozorovaný	k2eAgFnSc4d1	pozorovaná
u	u	k7c2	u
potomstva	potomstvo	k1gNnSc2	potomstvo
určitých	určitý	k2eAgNnPc2d1	určité
rodičovských	rodičovský	k2eAgNnPc2d1	rodičovské
hejn	hejno	k1gNnPc2	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Klostridie	Klostridie	k1gFnSc1	Klostridie
(	(	kIx(	(
<g/>
spory	spor	k1gInPc1	spor
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
trusu	trus	k1gInSc6	trus
<g/>
,	,	kIx,	,
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
kontaminované	kontaminovaný	k2eAgFnSc3d1	kontaminovaná
podestýlce	podestýlka	k1gFnSc3	podestýlka
nebo	nebo	k8xC	nebo
krmivu	krmivo	k1gNnSc3	krmivo
a	a	k8xC	a
ve	v	k7c6	v
střevním	střevní	k2eAgInSc6d1	střevní
obsahu	obsah	k1gInSc6	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Stafylokoky	stafylokok	k1gInPc1	stafylokok
jsou	být	k5eAaImIp3nP	být
ubikvitárními	ubikvitární	k2eAgMnPc7d1	ubikvitární
a	a	k8xC	a
obvyklými	obvyklý	k2eAgMnPc7d1	obvyklý
obyvateli	obyvatel	k1gMnPc7	obyvatel
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
sliznic	sliznice	k1gFnPc2	sliznice
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
chovanou	chovaný	k2eAgFnSc7d1	chovaná
drůbeží	drůbež	k1gFnSc7	drůbež
<g/>
,	,	kIx,	,
v	v	k7c6	v
líhních	líheň	k1gFnPc6	líheň
i	i	k8xC	i
na	na	k7c6	na
porážkách	porážka	k1gFnPc6	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnSc7d1	vstupní
branou	brána	k1gFnSc7	brána
infekce	infekce	k1gFnSc2	infekce
je	být	k5eAaImIp3nS	být
poškozená	poškozený	k2eAgFnSc1d1	poškozená
kůže	kůže	k1gFnSc1	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Predispozičními	predispoziční	k2eAgInPc7d1	predispoziční
faktory	faktor	k1gInPc7	faktor
jsou	být	k5eAaImIp3nP	být
nekrobiotické	krobiotický	k2eNgInPc1d1	krobiotický
procesy	proces	k1gInPc1	proces
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgFnSc1d1	vznikající
po	po	k7c6	po
poranění	poranění	k1gNnSc6	poranění
nebo	nebo	k8xC	nebo
při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
bakteriálních	bakteriální	k2eAgFnPc6d1	bakteriální
infekcích	infekce	k1gFnPc6	infekce
(	(	kIx(	(
<g/>
stafylokokóza	stafylokokóza	k1gFnSc1	stafylokokóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
stresové	stresový	k2eAgInPc4d1	stresový
či	či	k8xC	či
imunosupresivní	imunosupresivní	k2eAgInPc4d1	imunosupresivní
stavy	stav	k1gInPc4	stav
a	a	k8xC	a
onemocnění	onemocnění	k1gNnSc4	onemocnění
(	(	kIx(	(
<g/>
nedostatek	nedostatek	k1gInSc1	nedostatek
selénu	seléna	k1gFnSc4	seléna
<g/>
,	,	kIx,	,
exsudativní	exsudativní	k2eAgFnSc1d1	exsudativní
diatéza	diatéza	k1gFnSc1	diatéza
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgFnSc1d1	infekční
burzitida	burzitida	k1gFnSc1	burzitida
<g/>
,	,	kIx,	,
inkluzní	inkluzní	k2eAgFnSc1d1	inkluzní
hepatitida	hepatitida	k1gFnSc1	hepatitida
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgFnPc1d1	infekční
anémie	anémie	k1gFnPc1	anémie
kuřat	kuře	k1gNnPc2	kuře
<g/>
,	,	kIx,	,
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
výživa	výživa	k1gFnSc1	výživa
<g/>
,	,	kIx,	,
vlhká	vlhký	k2eAgFnSc1d1	vlhká
podestýlka	podestýlka	k1gFnSc1	podestýlka
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
po	po	k7c4	po
experimentální	experimentální	k2eAgFnSc4d1	experimentální
infekci	infekce	k1gFnSc4	infekce
je	být	k5eAaImIp3nS	být
3-24	[number]	k4	3-24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Postižené	postižený	k2eAgNnSc1d1	postižené
hejno	hejno	k1gNnSc1	hejno
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
náhle	náhle	k6eAd1	náhle
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
mortalitou	mortalita	k1gFnSc7	mortalita
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
pohybovými	pohybový	k2eAgFnPc7d1	pohybová
potížemi	potíž	k1gFnPc7	potíž
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
všeobecné	všeobecný	k2eAgFnPc1d1	všeobecná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
netečnost	netečnost	k1gFnSc1	netečnost
<g/>
,	,	kIx,	,
nechutenství	nechutenství	k1gNnSc1	nechutenství
<g/>
,	,	kIx,	,
inkoordinace	inkoordinace	k1gFnSc1	inkoordinace
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
slabost	slabost	k1gFnSc1	slabost
končetin	končetina	k1gFnPc2	končetina
a	a	k8xC	a
ataxie	ataxie	k1gFnSc2	ataxie
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
a	a	k8xC	a
k	k	k7c3	k
úhynu	úhyn	k1gInSc3	úhyn
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
během	během	k7c2	během
8-24	[number]	k4	8-24
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
toxémie	toxémie	k1gFnSc1	toxémie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dermatitida	dermatitida	k1gFnSc1	dermatitida
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zjistí	zjistit	k5eAaPmIp3nS	zjistit
až	až	k9	až
u	u	k7c2	u
uhynulých	uhynulý	k2eAgNnPc2d1	uhynulé
zvířat	zvíře	k1gNnPc2	zvíře
nebo	nebo	k8xC	nebo
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
úhynem	úhyn	k1gInSc7	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
se	se	k3xPyFc4	se
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
prsou	prsa	k1gNnPc2	prsa
<g/>
,	,	kIx,	,
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
končetin	končetina	k1gFnPc2	končetina
a	a	k8xC	a
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
červenohnědé	červenohnědý	k2eAgInPc1d1	červenohnědý
až	až	k6eAd1	až
černé	černý	k2eAgInPc1d1	černý
<g/>
,	,	kIx,	,
mokvající	mokvající	k2eAgInPc1d1	mokvající
okrsky	okrsek	k1gInPc1	okrsek
bez	bez	k7c2	bez
peří	peří	k1gNnSc2	peří
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
krváceninami	krvácenina	k1gFnPc7	krvácenina
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
přecházejí	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
podkoží	podkoží	k1gNnSc2	podkoží
i	i	k8xC	i
přilehlé	přilehlý	k2eAgFnSc2d1	přilehlá
svaloviny	svalovina	k1gFnSc2	svalovina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
šedé	šedý	k2eAgFnPc4d1	šedá
nebo	nebo	k8xC	nebo
žlutohnědé	žlutohnědý	k2eAgFnPc4d1	žlutohnědá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
plyn	plyn	k1gInSc1	plyn
(	(	kIx(	(
<g/>
emfyzém	emfyzém	k1gInSc1	emfyzém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kadávery	kadáver	k1gInPc1	kadáver
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
a	a	k8xC	a
hnilobně	hnilobně	k6eAd1	hnilobně
zapáchají	zapáchat	k5eAaImIp3nP	zapáchat
<g/>
.	.	kIx.	.
</s>
<s>
Postiženy	postižen	k2eAgInPc1d1	postižen
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
klouby	kloub	k1gInPc1	kloub
a	a	k8xC	a
šlachové	šlachový	k2eAgFnPc1d1	šlachová
pochvy	pochva	k1gFnPc1	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
orgány	orgán	k1gInPc1	orgán
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
ojediněle	ojediněle	k6eAd1	ojediněle
se	se	k3xPyFc4	se
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
zvětšená	zvětšený	k2eAgNnPc1d1	zvětšené
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
s	s	k7c7	s
drobnými	drobný	k2eAgInPc7d1	drobný
<g/>
,	,	kIx,	,
ostře	ostro	k6eAd1	ostro
ohraničenými	ohraničený	k2eAgFnPc7d1	ohraničená
nekrózami	nekróza	k1gFnPc7	nekróza
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tečkovitými	tečkovitý	k2eAgFnPc7d1	tečkovitá
krváceninami	krvácenina	k1gFnPc7	krvácenina
na	na	k7c6	na
epikardu	epikard	k1gInSc6	epikard
a	a	k8xC	a
seróze	seróza	k1gFnSc6	seróza
střev	střevo	k1gNnPc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
gangrenózní	gangrenózní	k2eAgFnSc2d1	gangrenózní
dermatitidy	dermatitida	k1gFnSc2	dermatitida
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
laboratorním	laboratorní	k2eAgNnSc7d1	laboratorní
vyšetřením	vyšetření	k1gNnSc7	vyšetření
<g/>
,	,	kIx,	,
izolací	izolace	k1gFnSc7	izolace
a	a	k8xC	a
identifikací	identifikace	k1gFnSc7	identifikace
příčinného	příčinný	k2eAgNnSc2d1	příčinné
agens	agens	k1gNnSc2	agens
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
izolace	izolace	k1gFnSc2	izolace
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
hodnotit	hodnotit	k5eAaImF	hodnotit
opatrně	opatrně	k6eAd1	opatrně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
klostridie	klostridie	k1gFnPc1	klostridie
i	i	k8xC	i
stafylokoky	stafylokok	k1gInPc1	stafylokok
se	se	k3xPyFc4	se
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
ptáků	pták	k1gMnPc2	pták
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
normálně	normálně	k6eAd1	normálně
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
GD	GD	kA	GD
zpravidla	zpravidla	k6eAd1	zpravidla
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
sekundárně	sekundárně	k6eAd1	sekundárně
při	při	k7c6	při
jiných	jiný	k2eAgFnPc6d1	jiná
infekcích	infekce	k1gFnPc6	infekce
postihujících	postihující	k2eAgInPc2d1	postihující
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
také	také	k9	také
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
infekční	infekční	k2eAgFnSc4d1	infekční
burzitidu	burzitida	k1gFnSc4	burzitida
<g/>
,	,	kIx,	,
adenoviry	adenovira	k1gFnPc4	adenovira
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgFnSc4d1	infekční
anémii	anémie	k1gFnSc4	anémie
a	a	k8xC	a
reoviry	reovir	k1gInPc4	reovir
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
představují	představovat	k5eAaImIp3nP	představovat
predispoziční	predispoziční	k2eAgInPc1d1	predispoziční
faktory	faktor	k1gInPc1	faktor
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
gangrenózní	gangrenózní	k2eAgFnSc2d1	gangrenózní
dermatitidy	dermatitida	k1gFnSc2	dermatitida
<g/>
.	.	kIx.	.
</s>
<s>
Pitevní	pitevní	k2eAgInSc1d1	pitevní
nález	nález	k1gInSc1	nález
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
typický	typický	k2eAgInSc1d1	typický
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Odlišit	odlišit	k5eAaPmF	odlišit
je	on	k3xPp3gFnPc4	on
nutné	nutný	k2eAgFnPc4d1	nutná
různé	různý	k2eAgFnPc4d1	různá
dermatitidy	dermatitida	k1gFnPc4	dermatitida
a	a	k8xC	a
kožní	kožní	k2eAgFnPc4d1	kožní
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Dermatitidy	dermatitida	k1gFnPc1	dermatitida
způsobované	způsobovaný	k2eAgFnPc1d1	způsobovaná
plísněmi	plíseň	k1gFnPc7	plíseň
Rhodotorula	Rhodotorulum	k1gNnPc1	Rhodotorulum
mucilaginosa	mucilaginosa	k1gFnSc1	mucilaginosa
<g/>
,	,	kIx,	,
R.	R.	kA	R.
glutins	glutins	k1gInSc1	glutins
<g/>
,	,	kIx,	,
Candida	Candida	k1gFnSc1	Candida
albicans	albicans	k6eAd1	albicans
nebo	nebo	k8xC	nebo
Aspergillus	Aspergillus	k1gMnSc1	Aspergillus
fumigatus	fumigatus	k1gMnSc1	fumigatus
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
GD	GD	kA	GD
průkazem	průkaz	k1gInSc7	průkaz
plísní	plíseň	k1gFnPc2	plíseň
v	v	k7c6	v
otiskových	otiskův	k2eAgInPc6d1	otiskův
preparátech	preparát	k1gInPc6	preparát
nebo	nebo	k8xC	nebo
v	v	k7c6	v
histologických	histologický	k2eAgInPc6d1	histologický
řezech	řez	k1gInPc6	řez
<g/>
,	,	kIx,	,
a	a	k8xC	a
izolací	izolace	k1gFnSc7	izolace
a	a	k8xC	a
identifikací	identifikace	k1gFnSc7	identifikace
agens	agens	k1gNnSc2	agens
<g/>
.	.	kIx.	.
</s>
<s>
Kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
dermatitida	dermatitida	k1gFnSc1	dermatitida
u	u	k7c2	u
brojlerů	brojler	k1gMnPc2	brojler
a	a	k8xC	a
pododermatitida	pododermatitida	k1gFnSc1	pododermatitida
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
jsou	být	k5eAaImIp3nP	být
charakterizované	charakterizovaný	k2eAgFnPc1d1	charakterizovaná
erozemi	eroze	k1gFnPc7	eroze
a	a	k8xC	a
ulceracemi	ulcerace	k1gFnPc7	ulcerace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
doprovázeny	doprovázet	k5eAaImNgFnP	doprovázet
akutními	akutní	k2eAgFnPc7d1	akutní
zánětlivými	zánětlivý	k2eAgFnPc7d1	zánětlivá
změnami	změna	k1gFnPc7	změna
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
<g/>
,	,	kIx,	,
hleznu	hlezno	k1gNnSc3	hlezno
a	a	k8xC	a
plantárním	plantární	k2eAgMnSc6d1	plantární
(	(	kIx(	(
<g/>
chodidlovém	chodidlový	k2eAgInSc6d1	chodidlový
<g/>
)	)	kIx)	)
povrchu	povrch	k1gInSc6	povrch
končetiny	končetina	k1gFnSc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
vlhká	vlhký	k2eAgFnSc1d1	vlhká
a	a	k8xC	a
špatná	špatný	k2eAgFnSc1d1	špatná
podestýlka	podestýlka	k1gFnSc1	podestýlka
<g/>
.	.	kIx.	.
</s>
<s>
Obtížné	obtížný	k2eAgNnSc1d1	obtížné
je	být	k5eAaImIp3nS	být
diferencovat	diferencovat	k5eAaImF	diferencovat
karcinom	karcinom	k1gInSc4	karcinom
skvamózních	skvamózní	k2eAgFnPc2d1	skvamózní
buněk	buňka	k1gFnPc2	buňka
kůže	kůže	k1gFnSc2	kůže
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
příčina	příčina	k1gFnSc1	příčina
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
a	a	k8xC	a
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ulceraci	ulcerace	k1gFnSc4	ulcerace
pokožky	pokožka	k1gFnSc2	pokožka
s	s	k7c7	s
následnou	následný	k2eAgFnSc7d1	následná
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
infekci	infekce	k1gFnSc3	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
mikroskopický	mikroskopický	k2eAgInSc4d1	mikroskopický
průkaz	průkaz	k1gInSc4	průkaz
neoplastických	oplastický	k2eNgFnPc2d1	oplastický
epiteliálních	epiteliální	k2eAgFnPc2d1	epiteliální
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
různé	různý	k2eAgInPc1d1	různý
nutriční	nutriční	k2eAgInPc1d1	nutriční
deficience	deficienec	k1gInPc1	deficienec
a	a	k8xC	a
geneticky	geneticky	k6eAd1	geneticky
podmíněné	podmíněný	k2eAgNnSc1d1	podmíněné
pomalé	pomalý	k2eAgNnSc1d1	pomalé
opařování	opařování	k1gNnSc1	opařování
kohoutů	kohout	k1gMnPc2	kohout
mohou	moct	k5eAaImIp3nP	moct
někdy	někdy	k6eAd1	někdy
připomínat	připomínat	k5eAaImF	připomínat
dermatitidu	dermatitida	k1gFnSc4	dermatitida
<g/>
.	.	kIx.	.
</s>
<s>
Ptáky	pták	k1gMnPc4	pták
postižené	postižený	k2eAgNnSc1d1	postižené
gangrenózní	gangrenózní	k2eAgFnSc7d1	gangrenózní
dermatitidou	dermatitida	k1gFnSc7	dermatitida
lze	lze	k6eAd1	lze
ošetřit	ošetřit	k5eAaPmF	ošetřit
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poměrně	poměrně	k6eAd1	poměrně
komplikované	komplikovaný	k2eAgFnSc3d1	komplikovaná
(	(	kIx(	(
<g/>
multifaktoriální	multifaktoriální	k2eAgFnSc3d1	multifaktoriální
<g/>
)	)	kIx)	)
etiologii	etiologie	k1gFnSc3	etiologie
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
ale	ale	k9	ale
léčba	léčba	k1gFnSc1	léčba
antimikrobiálními	antimikrobiální	k2eAgFnPc7d1	antimikrobiální
látkami	látka	k1gFnPc7	látka
vždy	vždy	k6eAd1	vždy
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
případech	případ	k1gInPc6	případ
primární	primární	k2eAgFnSc2d1	primární
virové	virový	k2eAgFnSc2d1	virová
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Preventivní	preventivní	k2eAgNnPc1d1	preventivní
opatření	opatření	k1gNnPc1	opatření
spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
zoohygieně	zoohygiena	k1gFnSc6	zoohygiena
chovu	chov	k1gInSc2	chov
a	a	k8xC	a
odstranění	odstranění	k1gNnSc2	odstranění
predispozičních	predispoziční	k2eAgInPc2d1	predispoziční
faktorů	faktor	k1gInPc2	faktor
-	-	kIx~	-
dobrá	dobrý	k2eAgFnSc1d1	dobrá
kvalita	kvalita	k1gFnSc1	kvalita
podestýlky	podestýlka	k1gFnSc2	podestýlka
<g/>
,	,	kIx,	,
redukce	redukce	k1gFnSc1	redukce
vlhkosti	vlhkost	k1gFnSc2	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
snižování	snižování	k1gNnSc4	snižování
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
zátěže	zátěž	k1gFnSc2	zátěž
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
zamezení	zamezení	k1gNnSc4	zamezení
traumatizace	traumatizace	k1gFnSc2	traumatizace
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
nepřeskladňování	nepřeskladňování	k1gNnSc1	nepřeskladňování
chovných	chovný	k2eAgInPc2d1	chovný
prostorů	prostor	k1gInPc2	prostor
a	a	k8xC	a
imunoprofylaxe	imunoprofylaxe	k1gFnSc2	imunoprofylaxe
infekční	infekční	k2eAgFnSc2d1	infekční
burzitidy	burzitis	k1gFnSc2	burzitis
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
imunosupresivních	imunosupresivní	k2eAgFnPc2d1	imunosupresivní
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
