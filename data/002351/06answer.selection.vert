<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
pak	pak	k6eAd1	pak
přibývají	přibývat	k5eAaImIp3nP	přibývat
další	další	k2eAgFnPc1d1	další
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
gangrenózní	gangrenózní	k2eAgFnSc2d1	gangrenózní
dermatitidy	dermatitida	k1gFnSc2	dermatitida
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
pod	pod	k7c7	pod
odlišnými	odlišný	k2eAgInPc7d1	odlišný
názvy	název	k1gInPc7	název
<g/>
)	)	kIx)	)
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
včetně	včetně	k7c2	včetně
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
