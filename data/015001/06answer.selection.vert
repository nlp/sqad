<s>
Motivem	motiv	k1gInSc7	motiv
Spinozova	Spinozův	k2eAgNnSc2d1	Spinozovo
filosofování	filosofování	k1gNnSc2	filosofování
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
praktické	praktický	k2eAgFnPc1d1	praktická
otázky	otázka	k1gFnPc1	otázka
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
svoboda	svoboda	k1gFnSc1	svoboda
mysli	mysl	k1gFnSc2	mysl
a	a	k8xC	a
antiutopistické	antiutopistický	k2eAgNnSc1d1	antiutopistický
uspořádání	uspořádání	k1gNnSc1	uspořádání
společnosti	společnost	k1gFnSc2	společnost
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
plném	plný	k2eAgInSc6d1	plný
občanských	občanský	k2eAgFnPc2d1	občanská
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
válek	válka	k1gFnPc2	válka
zvlášť	zvlášť	k6eAd1	zvlášť
naléhavé	naléhavý	k2eAgFnPc4d1	naléhavá
<g/>
.	.	kIx.	.
</s>
