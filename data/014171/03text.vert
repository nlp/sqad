<s>
Salmson	Salmson	k1gInSc1
3	#num#	k4
</s>
<s>
Sal	Sal	k?
3	#num#	k4
C	C	kA
<g/>
.1	.1	k4
<g/>
Určení	určení	k1gNnPc2
</s>
<s>
stíhací	stíhací	k2eAgMnSc1d1
letoun	letoun	k1gMnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Salmson	Salmson	k1gMnSc1
První	první	k4xOgMnSc1
let	léto	k1gNnPc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1918	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Charakter	charakter	k1gInSc1
</s>
<s>
program	program	k1gInSc1
opuštěn	opuštěn	k2eAgMnSc1d1
Uživatel	uživatel	k1gMnSc1
</s>
<s>
Aéronautique	Aéronautiquus	k1gMnSc5
Militaire	Militair	k1gMnSc5
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
1	#num#	k4
prototyp	prototyp	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Salmson	Salmson	k1gInSc1
3	#num#	k4
C	C	kA
<g/>
.1	.1	k4
byl	být	k5eAaImAgInS
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
ve	v	k7c6
Francii	Francie	k1gFnSc6
na	na	k7c6
sklonku	sklonek	k1gInSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
u	u	k7c2
firmy	firma	k1gFnSc2
Salmson	Salmsona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
</s>
<s>
Typ	typ	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
na	na	k7c6
základě	základ	k1gInSc6
požadavků	požadavek	k1gInPc2
Aéronautique	Aéronautique	k1gInSc1
Militaire	Militair	k1gInSc5
na	na	k7c4
jednomístný	jednomístný	k2eAgInSc4d1
stíhací	stíhací	k2eAgInSc4d1
letoun	letoun	k1gInSc4
(	(	kIx(
<g/>
kategorie	kategorie	k1gFnSc2
C	C	kA
<g/>
.1	.1	k4
<g/>
)	)	kIx)
pro	pro	k7c4
rok	rok	k1gInSc4
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Salmson	Salmson	k1gInSc1
3	#num#	k4
byl	být	k5eAaImAgInS
dvouplošník	dvouplošník	k1gInSc1
převážně	převážně	k6eAd1
dřevěné	dřevěný	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
<g/>
,	,	kIx,
se	s	k7c7
čtyřmi	čtyři	k4xCgInPc7
trupovými	trupový	k2eAgInPc7d1
podélníky	podélník	k1gInPc7
a	a	k8xC
celou	celý	k2eAgFnSc7d1
kostrou	kostra	k1gFnSc7
ocasních	ocasní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
z	z	k7c2
ocelových	ocelový	k2eAgFnPc2d1
trubek	trubka	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
potažené	potažený	k2eAgNnSc1d1
plátnem	plátno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
byl	být	k5eAaImAgInS
poháněný	poháněný	k2eAgInSc1d1
motorem	motor	k1gInSc7
Salmson	Salmson	k1gInSc4
9Z	9Z	k4
o	o	k7c6
výkonu	výkon	k1gInSc6
230	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
170	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
ve	v	k7c6
snaze	snaha	k1gFnSc6
o	o	k7c4
zlepšení	zlepšení	k1gNnSc4
výkonů	výkon	k1gInPc2
vybaven	vybavit	k5eAaPmNgInS
motorem	motor	k1gInSc7
Salmson	Salmson	k1gInSc4
9	#num#	k4
<g/>
Zm	Zm	k1gFnPc2
s	s	k7c7
výkonem	výkon	k1gInSc7
260	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
190	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Palivová	palivový	k2eAgFnSc1d1
a	a	k8xC
olejová	olejový	k2eAgFnSc1d1
instalace	instalace	k1gFnSc1
byla	být	k5eAaImAgFnS
převzata	převzít	k5eAaPmNgFnS
od	od	k7c2
úspěšné	úspěšný	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
Salmson	Salmson	k1gNnSc1
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letové	letový	k2eAgFnPc1d1
zkoušky	zkouška	k1gFnPc1
započaly	započnout	k5eAaPmAgFnP
v	v	k7c6
květnu	květen	k1gInSc6
1918	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
zkušební	zkušební	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
si	se	k3xPyFc3
stěžovali	stěžovat	k5eAaImAgMnP
na	na	k7c4
nedostatečný	dostatečný	k2eNgInSc4d1
výhled	výhled	k1gInSc4
z	z	k7c2
kokpitu	kokpit	k1gInSc2
a	a	k8xC
přílišné	přílišný	k2eAgFnSc2d1
síly	síla	k1gFnSc2
v	v	k7c6
řízení	řízení	k1gNnSc6
<g/>
,	,	kIx,
zejména	zejména	k9
při	při	k7c6
manévrování	manévrování	k1gNnSc6
v	v	k7c6
horizontální	horizontální	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
ačkoliv	ačkoliv	k8xS
obratnost	obratnost	k1gFnSc4
hodnotili	hodnotit	k5eAaImAgMnP
jako	jako	k8xC,k8xS
dobrou	dobrý	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prototyp	prototyp	k1gInSc1
byl	být	k5eAaImAgInS
vrácen	vrátit	k5eAaPmNgInS
do	do	k7c2
továrny	továrna	k1gFnSc2
k	k	k7c3
úpravám	úprava	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
ale	ale	k9
nevedly	vést	k5eNaImAgInP
k	k	k7c3
odstranění	odstranění	k1gNnSc3
zjištěných	zjištěný	k2eAgInPc2d1
nedostatků	nedostatek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vybavení	vybavení	k1gNnSc6
motorem	motor	k1gInSc7
Salmson	Salmson	k1gInSc4
9	#num#	k4
<g/>
Zm	Zm	k1gMnPc2
stroj	stroj	k1gInSc4
maximální	maximální	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
ve	v	k7c6
vodorovném	vodorovný	k2eAgInSc6d1
letu	let	k1gInSc6
mírně	mírně	k6eAd1
překonával	překonávat	k5eAaImAgMnS
SPAD	spad	k1gInSc4
S.	S.	kA
<g/>
XIII	XIII	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
jeho	jeho	k3xOp3gFnSc1
stoupavost	stoupavost	k1gFnSc1
byla	být	k5eAaImAgFnS
podstatně	podstatně	k6eAd1
horší	zlý	k2eAgFnSc1d2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
další	další	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
typu	typ	k1gInSc2
byl	být	k5eAaImAgInS
opuštěn	opuštěn	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Aéronautique	Aéronautiquus	k1gMnSc5
militaire	militair	k1gMnSc5
</s>
<s>
Specifikace	specifikace	k1gFnSc1
</s>
<s>
Údaje	údaj	k1gInPc1
platí	platit	k5eAaImIp3nP
pro	pro	k7c4
verzi	verze	k1gFnSc4
s	s	k7c7
motorem	motor	k1gInSc7
Salmson	Salmson	k1gInSc4
9	#num#	k4
<g/>
Zm	Zm	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
<g/>
s.	s.	k?
<g/>
448	#num#	k4
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Osádka	osádka	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
6,40	6,40	k4
m	m	kA
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1
<g/>
:	:	kIx,
9,85	9,85	k4
m	m	kA
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
2,96	2,96	k4
m	m	kA
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
:	:	kIx,
22,936	22,936	k4
m²	m²	k?
</s>
<s>
Prázdná	prázdný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
696,7	696,7	k4
kg	kg	kA
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
0	#num#	k4
<g/>
26,7	26,7	k4
kg	kg	kA
</s>
<s>
Pohonná	pohonný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
×	×	k?
kapalinou	kapalina	k1gFnSc7
chlazený	chlazený	k2eAgInSc1d1
hvězdicový	hvězdicový	k2eAgInSc1d1
devítiválec	devítiválec	k1gInSc1
Salmson	Salmson	k1gNnSc1
9	#num#	k4
<g/>
Zm	Zm	k1gFnPc2
pohánějící	pohánějící	k2eAgFnSc4d1
dvoulistou	dvoulistý	k2eAgFnSc4d1
vrtuli	vrtule	k1gFnSc4
Ratmanoff	Ratmanoff	k1gMnSc1
CUH	CUH	kA
</s>
<s>
Výkon	výkon	k1gInSc1
pohonné	pohonný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
:	:	kIx,
260	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
193,8	193,8	k4
kW	kW	kA
<g/>
)	)	kIx)
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
215	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
2	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
202	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
5	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
Stoupavost	stoupavost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Výstup	výstup	k1gInSc1
do	do	k7c2
1	#num#	k4
000	#num#	k4
m	m	kA
<g/>
:	:	kIx,
2,73	2,73	k4
min	mina	k1gFnPc2
</s>
<s>
Výstup	výstup	k1gInSc1
do	do	k7c2
2	#num#	k4
000	#num#	k4
m	m	kA
<g/>
:	:	kIx,
5,43	5,43	k4
min	mina	k1gFnPc2
</s>
<s>
Výstup	výstup	k1gInSc1
do	do	k7c2
4	#num#	k4
000	#num#	k4
m	m	kA
<g/>
:	:	kIx,
13,28	13,28	k4
min	mina	k1gFnPc2
</s>
<s>
Výstup	výstup	k1gInSc1
do	do	k7c2
5	#num#	k4
000	#num#	k4
m	m	kA
<g/>
:	:	kIx,
21	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
Výstup	výstup	k1gInSc1
do	do	k7c2
6	#num#	k4
000	#num#	k4
m	m	kA
<g/>
:	:	kIx,
34,10	34,10	k4
min	mina	k1gFnPc2
</s>
<s>
Dolet	dolet	k1gInSc1
<g/>
:	:	kIx,
350	#num#	k4
km	km	kA
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
7	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
2	#num#	k4
×	×	k?
synchronizovaný	synchronizovaný	k2eAgInSc1d1
kulomet	kulomet	k1gInSc1
Vickers	Vickers	k1gInSc1
ráže	ráže	k1gFnSc1
7,7	7,7	k4
mm	mm	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Salmson	Salmson	k1gNnSc4
3	#num#	k4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
Davilla	Davillo	k1gNnPc1
a	a	k8xC
Soltan	Soltan	k1gInSc1
<g/>
,	,	kIx,
19971	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Owers	Owersa	k1gFnPc2
<g/>
,	,	kIx,
Davilla	Davilla	k1gMnSc1
a	a	k8xC
Guttman	Guttman	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
DAVILLA	DAVILLA	kA
<g/>
,	,	kIx,
Dr	dr	kA
<g/>
.	.	kIx.
James	James	k1gMnSc1
J.	J.	kA
<g/>
;	;	kIx,
SOLTAN	SOLTAN	kA
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
French	French	k1gMnSc1
Aircraft	Aircraft	k1gMnSc1
of	of	k?
the	the	k?
First	First	k1gMnSc1
World	World	k1gMnSc1
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mountain	Mountain	k1gMnSc1
View	View	k1gMnSc1
<g/>
,	,	kIx,
CA	ca	kA
<g/>
:	:	kIx,
Flying	Flying	k1gInSc1
Machines	Machinesa	k1gFnPc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9637110	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Salmson	Salmsona	k1gFnPc2
3	#num#	k4
C	C	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
446	#num#	k4
<g/>
-	-	kIx~
<g/>
447	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
OWERS	OWERS	kA
<g/>
,	,	kIx,
Collin	Collin	k2eAgMnSc1d1
A.	A.	kA
<g/>
;	;	kIx,
GUTTMAN	GUTTMAN	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
J.	J.	kA
<g/>
;	;	kIx,
DAVILLA	DAVILLA	kA
<g/>
,	,	kIx,
Dr	dr	kA
<g/>
.	.	kIx.
James	James	k1gMnSc1
J.	J.	kA
Salmson	Salmson	k1gMnSc1
aircraft	aircraft	k1gMnSc1
of	of	k?
World	World	k1gMnSc1
War	War	k1gMnSc2
I.	I.	kA
Boulder	Boulder	k1gInSc1
<g/>
,	,	kIx,
Colorado	Colorado	k1gNnSc1
<g/>
:	:	kIx,
Flying	Flying	k1gInSc1
Machine	Machin	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
891268	#num#	k4
16	#num#	k4
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Salmson	Salmsona	k1gFnPc2
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
101	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Související	související	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Salmson	Salmson	k1gInSc1
2	#num#	k4
</s>
<s>
Salmson	Salmson	k1gInSc1
4	#num#	k4
</s>
<s>
Letadla	letadlo	k1gNnPc1
stejného	stejný	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
určení	určení	k1gNnSc2
a	a	k8xC
koncepce	koncepce	k1gFnSc2
</s>
<s>
Nieuport	Nieuport	k1gInSc1
28	#num#	k4
</s>
<s>
SPAD	spad	k1gInSc1
S.	S.	kA
<g/>
XIII	XIII	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Salmson	Salmson	k1gInSc1
SAL-3	SAL-3	k1gFnSc2
na	na	k7c6
aviafrance	aviafrance	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Francouzské	francouzský	k2eAgInPc4d1
vojenské	vojenský	k2eAgInPc4d1
letouny	letoun	k1gInPc4
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
Předválečné	předválečný	k2eAgInPc1d1
letouny	letoun	k1gInPc1
(	(	kIx(
<g/>
zúčastnily	zúčastnit	k5eAaPmAgInP
se	se	k3xPyFc4
bojů	boj	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Nieuport	Nieuport	k1gInSc1
IV	Iva	k1gFnPc2
Blindé	Blindý	k2eAgFnSc2d1
•	•	k?
Blériot	Blériot	k1gMnSc1
XI	XI	kA
bis	bis	k?
•	•	k?
Blériot	Blériota	k1gFnPc2
XI	XI	kA
2	#num#	k4
<g/>
bis	bis	k?
•	•	k?
Breguet	Breguet	k1gInSc1
Militaire	Militair	k1gInSc5
•	•	k?
Deperdussin	Deperdussina	k1gFnPc2
TT	TT	kA
•	•	k?
Farman	Farman	k1gMnSc1
HF	HF	kA
<g/>
.20	.20	k4
•	•	k?
Farman	Farman	k1gMnSc1
MF	MF	kA
<g/>
.	.	kIx.
<g/>
IV	IV	kA
•	•	k?
Farman	Farman	k1gMnSc1
MF	MF	kA
<g/>
.	.	kIx.
<g/>
VII	VII	kA
•	•	k?
Latham	Latham	k1gInSc1
Antoinette	Antoinette	k1gFnPc2
Militaire	Militair	k1gInSc5
•	•	k?
Morane-Saulnier	Morane-Saulniero	k1gNnPc2
L	L	kA
(	(	kIx(
<g/>
M.	M.	kA
<g/>
S.	S.	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
REP	REP	kA
N	N	kA
Stíhací	stíhací	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
Hanriot	Hanriot	k1gInSc1
HD	HD	kA
<g/>
.1	.1	k4
•	•	k?
Hanriot	Hanriota	k1gFnPc2
HD	HD	kA
<g/>
.2	.2	k4
•	•	k?
Hanriot	Hanriota	k1gFnPc2
HD	HD	kA
<g/>
.3	.3	k4
•	•	k?
Morane	Moran	k1gMnSc5
N	N	kA
(	(	kIx(
<g/>
M.	M.	kA
<g/>
S.	S.	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nieuport	Nieuport	k1gInSc1
10	#num#	k4
•	•	k?
Nieuport	Nieuport	k1gInSc1
11	#num#	k4
•	•	k?
Nieuport	Nieuport	k1gInSc1
17	#num#	k4
•	•	k?
Nieuport	Nieuport	k1gInSc1
21	#num#	k4
•	•	k?
Nieuport	Nieuport	k1gInSc1
24	#num#	k4
•	•	k?
Nieuport	Nieuport	k1gInSc1
27	#num#	k4
•	•	k?
Nieuport	Nieuport	k1gInSc1
28	#num#	k4
•	•	k?
SPAD	spad	k1gInSc1
S.	S.	kA
<g/>
VII	VII	kA
•	•	k?
SPAD	spad	k1gInSc1
S.	S.	kA
<g/>
XII	XII	kA
•	•	k?
SPAD	spad	k1gInSc1
S.	S.	kA
<g/>
XIII	XIII	kA
•	•	k?
SPAD	spad	k1gInSc1
S.	S.	kA
<g/>
XIV	XIV	kA
Bombardovací	bombardovací	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
Blériot	Blériot	k1gInSc1
72	#num#	k4
Bn	Bn	k1gFnPc2
<g/>
3	#num#	k4
•	•	k?
Breguet	Breguet	k1gMnSc1
14	#num#	k4
B2	B2	k1gFnSc2
•	•	k?
Breguet	Breguet	k1gMnSc1
BM	BM	kA
<g/>
.	.	kIx.
<g/>
V	v	k7c6
•	•	k?
Farman	Farman	k1gMnSc1
F.	F.	kA
<g/>
50	#num#	k4
Bn	Bn	k1gFnSc1
<g/>
2	#num#	k4
•	•	k?
Farman	Farman	k1gMnSc1
MF	MF	kA
<g/>
.11	.11	k4
•	•	k?
Morane-Saulnier	Morane-Saulniero	k1gNnPc2
M.	M.	kA
<g/>
S.	S.	kA
<g/>
25	#num#	k4
T	T	kA
•	•	k?
Voisin	Voisin	k2eAgMnSc1d1
V	v	k7c6
(	(	kIx(
<g/>
LA	la	k1gNnSc6
<g/>
)	)	kIx)
•	•	k?
Voisin	Voisin	k1gInSc1
VIII	VIII	kA
(	(	kIx(
<g/>
LAP	lap	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Voisin	Voisin	k1gMnSc1
X	X	kA
•	•	k?
Voisin	Voisin	k1gInSc1
XII	XII	kA
Průzkumné	průzkumný	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
Breguet	Breguet	k1gInSc1
Br.	Br.	k1gFnSc2
<g/>
14	#num#	k4
A2	A2	k1gMnSc1
•	•	k?
Caudron	Caudron	k1gMnSc1
G	G	kA
<g/>
.3	.3	k4
A2	A2	k1gFnSc2
•	•	k?
Caudron	Caudron	k1gInSc1
G	G	kA
<g/>
.4	.4	k4
A2	A2	k1gFnSc2
•	•	k?
Caudron	Caudron	k1gInSc1
G	G	kA
<g/>
.6	.6	k4
A3	A3	k1gFnSc2
•	•	k?
Caudron	Caudron	k1gInSc1
R.	R.	kA
<g/>
4	#num#	k4
A3	A3	k1gFnSc2
•	•	k?
Caudron	Caudron	k1gInSc1
R.	R.	kA
<g/>
11	#num#	k4
A3	A3	k1gFnSc2
•	•	k?
Farman	Farman	k1gMnSc1
ME	ME	kA
<g/>
.	.	kIx.
<g/>
II	II	kA
•	•	k?
Farman	Farman	k1gMnSc1
F.	F.	kA
<g/>
30	#num#	k4
•	•	k?
Farman	Farman	k1gMnSc1
MF	MF	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.11	.11	k4
•	•	k?
Letord	Letorda	k1gFnPc2
1	#num#	k4
A	a	k9
<g/>
.3	.3	k4
•	•	k?
Morane-Saulnier	Morane-Saulnira	k1gFnPc2
T	T	kA
(	(	kIx(
<g/>
M.	M.	kA
<g/>
S.	S.	kA
<g/>
25	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Morane-Saulnier	Morane-Saulnier	k1gInSc1
P	P	kA
(	(	kIx(
<g/>
M.	M.	kA
<g/>
S.	S.	kA
<g/>
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nieuport	Nieuport	k1gInSc1
12	#num#	k4
•	•	k?
Nieuport	Nieuport	k1gInSc1
14	#num#	k4
A	a	k9
<g/>
.2	.2	k4
•	•	k?
Salmson-Moineau	Salmson-Moineaus	k1gInSc2
S.	S.	kA
<g/>
M.	M.	kA
<g/>
1	#num#	k4
•	•	k?
Salmson	Salmsona	k1gFnPc2
SAL	SAL	kA
<g/>
.2	.2	k4
A2	A2	k1gFnSc2
•	•	k?
SPAD	spad	k1gInSc1
S.	S.	kA
<g/>
XI	XI	kA
•	•	k?
SPAD	spad	k1gInSc1
S.	S.	kA
<g/>
XVI	XVI	kA
A	a	k9
<g/>
.2	.2	k4
•	•	k?
SPAD	spad	k1gInSc1
SA	SA	kA
<g/>
.2	.2	k4
Námořní	námořní	k2eAgInPc4d1
průzkumné	průzkumný	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
FBA	FBA	kA
S	s	k7c7
•	•	k?
Tellier	Tellier	k1gInSc1
T	T	kA
<g/>
.3	.3	k4
Bitevní	bitevní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
Salmson	Salmson	k1gInSc1
Sal	Sal	k1gFnSc2
<g/>
.4	.4	k4
Ab	Ab	k1gFnSc1
<g/>
.2	.2	k4
Prototypy	prototyp	k1gInPc4
a	a	k8xC
experimentální	experimentální	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
Béchereau	Béchereau	k5eAaPmIp1nS
SAB	SAB	kA
C	C	kA
<g/>
.1	.1	k4
•	•	k?
Blériot	Blériot	k1gInSc1
67	#num#	k4
•	•	k?
Breguet	Breguet	k1gInSc1
16	#num#	k4
•	•	k?
Breguet	Breguet	k1gInSc1
17	#num#	k4
•	•	k?
Gourdou-Leseurre	Gourdou-Leseurr	k1gInSc5
1	#num#	k4
•	•	k?
Gourdou-Leseurre	Gourdou-Leseurr	k1gInSc5
2	#num#	k4
•	•	k?
Letord	Letord	k1gInSc1
9	#num#	k4
•	•	k?
Morane-Saulnier	Morane-Saulniero	k1gNnPc2
BB	BB	kA
•	•	k?
Morane-Saulnier	Morane-Saulnier	k1gInSc1
S	s	k7c7
•	•	k?
Morane-Saulnier	Morane-Saulnier	k1gInSc1
TRK	trk	k1gInSc1
•	•	k?
Nieuport	Nieuport	k1gInSc1
15	#num#	k4
•	•	k?
Nieuport-Delage	Nieuport-Delage	k1gNnSc1
29	#num#	k4
•	•	k?
REP	REP	kA
Parasol	Parasol	k?
•	•	k?
Salmson	Salmson	k1gNnSc1
3	#num#	k4
•	•	k?
Salmson	Salmson	k1gNnSc1
5	#num#	k4
•	•	k?
SEA	SEA	kA
IV	IV	kA
•	•	k?
Tellier	Tellier	k1gInSc1
T	T	kA
<g/>
.2	.2	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
|	|	kIx~
Francie	Francie	k1gFnSc1
|	|	kIx~
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
