<p>
<s>
Serpent	Serpent	k1gInSc1	Serpent
je	být	k5eAaImIp3nS	být
kontrabasový	kontrabasový	k2eAgInSc1d1	kontrabasový
typ	typ	k1gInSc1	typ
cinku	cink	k1gInSc2	cink
<g/>
,	,	kIx,	,
nejstaršího	starý	k2eAgInSc2d3	nejstarší
žesťového	žesťový	k2eAgInSc2d1	žesťový
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mohutný	mohutný	k2eAgInSc1d1	mohutný
nástroj	nástroj	k1gInSc1	nástroj
připomíná	připomínat	k5eAaImIp3nS	připomínat
vlnícího	vlnící	k2eAgMnSc4d1	vlnící
se	se	k3xPyFc4	se
černého	černý	k1gMnSc2	černý
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
jeho	jeho	k3xOp3gInSc4	jeho
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Part	part	k1gInSc1	part
pro	pro	k7c4	pro
serpent	serpent	k1gInSc4	serpent
napsal	napsat	k5eAaBmAgMnS	napsat
např.	např.	kA	např.
Georg	Georg	k1gMnSc1	Georg
Friedrich	Friedrich	k1gMnSc1	Friedrich
Händel	Händlo	k1gNnPc2	Händlo
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
"	"	kIx"	"
<g/>
Vodní	vodní	k2eAgFnSc6d1	vodní
hudbě	hudba	k1gFnSc6	hudba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nejoblíbenější	oblíbený	k2eAgMnSc1d3	nejoblíbenější
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
(	(	kIx(	(
<g/>
užívaný	užívaný	k2eAgInSc1d1	užívaný
pro	pro	k7c4	pro
držení	držení	k1gNnSc4	držení
basové	basový	k2eAgFnSc2d1	basová
linky	linka	k1gFnSc2	linka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
nástroj	nástroj	k1gInSc1	nástroj
poměrně	poměrně	k6eAd1	poměrně
běžně	běžně	k6eAd1	běžně
používal	používat	k5eAaImAgMnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběl	vyrábět	k5eAaImAgInS	vyrábět
se	se	k3xPyFc4	se
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
díky	díky	k7c3	díky
nátrubku	nátrubek	k1gInSc3	nátrubek
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
žestě	žestě	k1gInPc4	žestě
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
chromatizaci	chromatizace	k1gFnSc4	chromatizace
tonů	tonus	k1gInPc2	tonus
sloužily	sloužit	k5eAaImAgFnP	sloužit
dírky	dírka	k1gFnPc4	dírka
zakrývané	zakrývaný	k2eAgFnPc4d1	zakrývaná
hráčovými	hráčův	k2eAgInPc7d1	hráčův
prsty	prst	k1gInPc7	prst
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
zobcové	zobcový	k2eAgFnSc2d1	zobcová
flétny	flétna	k1gFnSc2	flétna
<g/>
.	.	kIx.	.
</s>
<s>
Dělaly	dělat	k5eAaImAgFnP	dělat
se	se	k3xPyFc4	se
serpenty	serpenta	k1gFnPc1	serpenta
do	do	k7c2	do
pochodu	pochod	k1gInSc2	pochod
i	i	k9	i
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
vsedě	vsedě	k6eAd1	vsedě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
serpent	serpent	k1gMnSc1	serpent
úplně	úplně	k6eAd1	úplně
vyšel	vyjít	k5eAaPmAgMnS	vyjít
z	z	k7c2	z
používání	používání	k1gNnSc2	používání
<g/>
.	.	kIx.	.
</s>
</p>
