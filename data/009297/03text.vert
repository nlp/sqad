<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Grónska	Grónsko	k1gNnSc2	Grónsko
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
grónským	grónský	k2eAgMnSc7d1	grónský
umělcem	umělec	k1gMnSc7	umělec
Thue	Thu	k1gFnSc2	Thu
Christiansenem	Christianseno	k1gNnSc7	Christianseno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
je	být	k5eAaImIp3nS	být
znázorněno	znázorněn	k2eAgNnSc1d1	znázorněno
slunce	slunce	k1gNnSc4	slunce
vycházející	vycházející	k2eAgNnSc4d1	vycházející
nad	nad	k7c7	nad
polárním	polární	k2eAgInSc7d1	polární
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
značí	značit	k5eAaImIp3nS	značit
návrat	návrat	k1gInSc1	návrat
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
při	při	k7c6	při
letním	letní	k2eAgInSc6d1	letní
slunovratu	slunovrat	k1gInSc6	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c6	na
dánské	dánský	k2eAgFnSc6d1	dánská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
grónskou	grónský	k2eAgFnSc4d1	grónská
svázanost	svázanost	k1gFnSc4	svázanost
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
a	a	k8xC	a
Skandinávií	Skandinávie	k1gFnSc7	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
grónské	grónský	k2eAgFnSc2d1	grónská
vlajky	vlajka	k1gFnSc2	vlajka
vyvěšována	vyvěšovat	k5eAaImNgFnS	vyvěšovat
společně	společně	k6eAd1	společně
i	i	k8xC	i
dánská	dánský	k2eAgFnSc1d1	dánská
<g/>
.	.	kIx.	.
</s>
<s>
Grónská	grónský	k2eAgFnSc1d1	grónská
vlajka	vlajka	k1gFnSc1	vlajka
platí	platit	k5eAaImIp3nS	platit
jako	jako	k9	jako
státní	státní	k2eAgInSc4d1	státní
symbol	symbol	k1gInSc4	symbol
i	i	k9	i
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Znak	znak	k1gInSc1	znak
Grónska	Grónsko	k1gNnSc2	Grónsko
</s>
</p>
<p>
<s>
Grónská	grónský	k2eAgFnSc1d1	grónská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlajka	vlajka	k1gFnSc1	vlajka
Grónska	Grónsko	k1gNnSc2	Grónsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
