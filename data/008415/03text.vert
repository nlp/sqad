<p>
<s>
Hélè	Hélè	k?	Hélè
Larroche-Dub	Larroche-Dub	k1gInSc1	Larroche-Dub
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1924	[number]	k4	1924
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
publicistka	publicistka	k1gFnSc1	publicistka
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
komisařka	komisařka	k1gFnSc1	komisařka
výstav	výstav	k1gInSc1	výstav
moderního	moderní	k2eAgNnSc2d1	moderní
a	a	k8xC	a
etnického	etnický	k2eAgNnSc2d1	etnické
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
manažerka	manažerka	k1gFnSc1	manažerka
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
kulturní	kulturní	k2eAgFnSc2d1	kulturní
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
v	v	k7c6	v
německojazyčné	německojazyčný	k2eAgFnSc6d1	německojazyčná
rodině	rodina	k1gFnSc6	rodina
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
dcera	dcera	k1gFnSc1	dcera
strojního	strojní	k2eAgMnSc2d1	strojní
konstruktéra	konstruktér	k1gMnSc2	konstruktér
a	a	k8xC	a
profesora	profesor	k1gMnSc2	profesor
Německé	německý	k2eAgFnSc2d1	německá
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
technické	technický	k2eAgFnSc2d1	technická
Ing.	ing.	kA	ing.
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Duba	Dub	k1gMnSc2	Dub
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
Erny	Ern	k2eAgFnPc4d1	Erna
Margarety	Margareta	k1gFnPc4	Margareta
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgInPc4d1	rozený
Spitzer	Spitzer	k1gInSc4	Spitzer
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
-1938	-1938	k4	-1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Moravské	moravský	k2eAgFnSc2d1	Moravská
Ostravy	Ostrava	k1gFnSc2	Ostrava
-Vítkovic	-Vítkovice	k1gFnPc2	-Vítkovice
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
německé	německý	k2eAgNnSc4d1	německé
gymnasium	gymnasium	k1gNnSc4	gymnasium
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
naučila	naučit	k5eAaPmAgFnS	naučit
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
strojního	strojní	k2eAgMnSc4d1	strojní
inženýra	inženýr	k1gMnSc4	inženýr
Emila	Emil	k1gMnSc2	Emil
Wellnera	Wellner	k1gMnSc2	Wellner
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dvojčata	dvojče	k1gNnPc4	dvojče
Margareta-Erna	Margareta-Erno	k1gNnSc2	Margareta-Erno
a	a	k8xC	a
Irene-Therese	Irene-Therese	k1gFnSc2	Irene-Therese
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
prvnímu	první	k4xOgNnSc3	první
manželovi	manžel	k1gMnSc6	manžel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
Helena	Helena	k1gFnSc1	Helena
poslána	poslat	k5eAaPmNgFnS	poslat
do	do	k7c2	do
penzionátu	penzionát	k1gInSc2	penzionát
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
skrývala	skrývat	k5eAaImAgFnS	skrývat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jihofrancouzské	jihofrancouzský	k2eAgFnSc2d1	jihofrancouzská
Nice	Nice	k1gFnSc2	Nice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
holocaustu	holocaust	k1gInSc3	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
kursy	kurs	k1gInPc4	kurs
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
Montessori	Montessor	k1gFnSc2	Montessor
<g/>
,	,	kIx,	,
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
vychovatelka	vychovatelka	k1gFnSc1	vychovatelka
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
Montessori	Montessor	k1gFnSc2	Montessor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
získala	získat	k5eAaPmAgFnS	získat
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
titul	titul	k1gInSc4	titul
na	na	k7c6	na
filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Aix-Marseille	Aix-Marseilla	k1gFnSc6	Aix-Marseilla
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
ihned	ihned	k6eAd1	ihned
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
Francie	Francie	k1gFnSc2	Francie
od	od	k7c2	od
okupantů	okupant	k1gMnPc2	okupant
<g/>
,	,	kIx,	,
pracovala	pracovat	k5eAaImAgFnS	pracovat
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
pro	pro	k7c4	pro
americký	americký	k2eAgInSc4d1	americký
Červený	červený	k2eAgInSc4d1	červený
kříž	kříž	k1gInSc4	kříž
jako	jako	k8xC	jako
recepční	recepční	k1gFnSc4	recepční
v	v	k7c6	v
ozdravném	ozdravný	k2eAgNnSc6d1	ozdravné
středisku	středisko	k1gNnSc6	středisko
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
Riviéře	Riviéra	k1gFnSc6	Riviéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945-1949	[number]	k4	1945-1949
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
biochemii	biochemie	k1gFnSc4	biochemie
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
licenciát	licenciát	k1gInSc4	licenciát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
architekta	architekt	k1gMnSc4	architekt
Fernanda	Fernando	k1gNnSc2	Fernando
Larroche	Larroche	k1gFnSc7	Larroche
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
původní	původní	k2eAgNnSc4d1	původní
jméno	jméno	k1gNnSc4	jméno
během	během	k7c2	během
odboje	odboj	k1gInSc2	odboj
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
Daniel	Daniel	k1gMnSc1	Daniel
Maurandy	Mauranda	k1gFnSc2	Mauranda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
vychovala	vychovat	k5eAaPmAgFnS	vychovat
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
Mireille	Mireille	k1gNnSc4	Mireille
<g/>
/	/	kIx~	/
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
–	–	k?	–
Helene	Helen	k1gInSc5	Helen
<g/>
/	/	kIx~	/
<g/>
1955	[number]	k4	1955
a	a	k8xC	a
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
<g/>
/	/	kIx~	/
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
usadila	usadit	k5eAaPmAgFnS	usadit
v	v	k7c6	v
Ivry-sur-Seine	Ivryur-Sein	k1gMnSc5	Ivry-sur-Sein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pracovala	pracovat	k5eAaImAgFnS	pracovat
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
architektonickém	architektonický	k2eAgInSc6d1	architektonický
ateliéru	ateliér	k1gInSc6	ateliér
svého	svůj	k3xOyFgMnSc4	svůj
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951-56	[number]	k4	1951-56
byla	být	k5eAaImAgFnS	být
zaměstnána	zaměstnat	k5eAaPmNgFnS	zaměstnat
v	v	k7c6	v
Musée	Musée	k1gNnSc6	Musée
national	nationat	k5eAaPmAgMnS	nationat
de	de	k?	de
la	la	k1gNnPc6	la
recherche	recherch	k1gInSc2	recherch
scientifique	scientifique	k1gNnSc2	scientifique
<g/>
.	.	kIx.	.
</s>
<s>
Ateliér	ateliér	k1gInSc1	ateliér
byl	být	k5eAaImAgInS	být
počátkem	počátkem	k7c2	počátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
činný	činný	k2eAgInSc4d1	činný
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
Helene	Helen	k1gInSc5	Helen
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
své	svůj	k3xOyFgInPc4	svůj
zájmy	zájem	k1gInPc4	zájem
o	o	k7c4	o
současné	současný	k2eAgNnSc4d1	současné
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
a	a	k8xC	a
etnické	etnický	k2eAgNnSc4d1	etnické
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1967	[number]	k4	1967
-	-	kIx~	-
10985	[number]	k4	10985
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
sekretářka	sekretářka	k1gFnSc1	sekretářka
Společnosti	společnost	k1gFnSc2	společnost
přátelství	přátelství	k1gNnSc2	přátelství
Francie	Francie	k1gFnSc2	Francie
-	-	kIx~	-
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
Associaion	Associaion	k1gInSc1	Associaion
France-	France-	k1gMnSc2	France-
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naučila	naučit	k5eAaPmAgFnS	naučit
se	se	k3xPyFc4	se
rusky	rusky	k6eAd1	rusky
a	a	k8xC	a
pracovala	pracovat	k5eAaImAgFnS	pracovat
pro	pro	k7c4	pro
kulturní	kulturní	k2eAgFnSc4d1	kulturní
výměnu	výměna	k1gFnSc4	výměna
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
získala	získat	k5eAaPmAgFnS	získat
místo	místo	k1gNnSc4	místo
komisařky	komisařka	k1gFnSc2	komisařka
výstav	výstava	k1gFnPc2	výstava
v	v	k7c6	v
Centre	centr	k1gInSc5	centr
Georges-Pompidou	Georges-Pompida	k1gFnSc7	Georges-Pompida
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
design	design	k1gInSc4	design
<g/>
,	,	kIx,	,
pracovala	pracovat	k5eAaImAgFnS	pracovat
tam	tam	k6eAd1	tam
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
dvě	dva	k4xCgFnPc4	dva
desítky	desítka	k1gFnPc4	desítka
výstav	výstava	k1gFnPc2	výstava
umění	umění	k1gNnSc1	umění
14	[number]	k4	14
bývalých	bývalý	k2eAgFnPc2d1	bývalá
republik	republika	k1gFnPc2	republika
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
asijských	asijský	k2eAgFnPc2d1	asijská
zemí	zem	k1gFnPc2	zem
<g/>
;	;	kIx,	;
svůj	svůj	k3xOyFgInSc4	svůj
zájem	zájem	k1gInSc4	zájem
především	především	k6eAd1	především
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
cestovala	cestovat	k5eAaImAgFnS	cestovat
nejen	nejen	k6eAd1	nejen
služebně	služebně	k6eAd1	služebně
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
pořádala	pořádat	k5eAaImAgFnS	pořádat
výstavy	výstava	k1gFnPc4	výstava
pro	pro	k7c4	pro
další	další	k2eAgNnPc4d1	další
města	město	k1gNnPc4	město
a	a	k8xC	a
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
založila	založit	k5eAaPmAgFnS	založit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
knihkupectví	knihkupectví	k1gNnSc2	knihkupectví
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
průvodců	průvodce	k1gMnPc2	průvodce
a	a	k8xC	a
map	mapa	k1gFnPc2	mapa
Itinéraires	Itinérairesa	k1gFnPc2	Itinérairesa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
-	-	kIx~	-
2010	[number]	k4	2010
pořádala	pořádat	k5eAaImAgFnS	pořádat
výstavy	výstava	k1gFnPc4	výstava
současného	současný	k2eAgInSc2d1	současný
etnického	etnický	k2eAgInSc2d1	etnický
textilu	textil	k1gInSc2	textil
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Ivry-sur-Seine	Ivryur-Sein	k1gInSc5	Ivry-sur-Sein
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
především	především	k9	především
cestuje	cestovat	k5eAaImIp3nS	cestovat
po	po	k7c6	po
zemích	zem	k1gFnPc6	zem
Třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
a	a	k8xC	a
fotografuje	fotografovat	k5eAaImIp3nS	fotografovat
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
k	k	k7c3	k
edici	edice	k1gFnSc3	edice
své	svůj	k3xOyFgFnSc2	svůj
osobní	osobní	k2eAgFnSc2d1	osobní
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Práce	práce	k1gFnPc1	práce
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Centre	centr	k1gInSc5	centr
Georges	Georgesa	k1gFnPc2	Georgesa
Pompidou	Pompida	k1gFnSc7	Pompida
===	===	k?	===
</s>
</p>
<p>
<s>
komisařka	komisařka	k1gFnSc1	komisařka
výstav	výstava	k1gFnPc2	výstava
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
Design	design	k1gInSc1	design
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
(	(	kIx(	(
<g/>
CGP	CGP	kA	CGP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
Design	design	k1gInSc4	design
a	a	k8xC	a
zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
(	(	kIx(	(
<g/>
CGP	CGP	kA	CGP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
Proměny	proměna	k1gFnPc4	proměna
finské	finský	k2eAgFnSc2d1	finská
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
designu	design	k1gInSc2	design
(	(	kIx(	(
<g/>
CGP	CGP	kA	CGP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
koncepce	koncepce	k1gFnSc1	koncepce
pořadatele	pořadatel	k1gMnSc2	pořadatel
výstavy	výstava	k1gFnSc2	výstava
(	(	kIx(	(
<g/>
CGP	CGP	kA	CGP
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
Sovětský	sovětský	k2eAgInSc4d1	sovětský
urbanisrtický	urbanisrtický	k2eAgInSc4d1	urbanisrtický
prostor	prostor	k1gInSc4	prostor
1917-1978	[number]	k4	1917-1978
CGP	CGP	kA	CGP
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
Objet	objet	k2eAgInSc1d1	objet
industriel	industriel	k1gInSc1	industriel
<g/>
/	/	kIx~	/
<g/>
Průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
objekt	objekt	k1gInSc1	objekt
(	(	kIx(	(
<g/>
CGP	CGP	kA	CGP
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Environnement	Environnement	k1gInSc1	Environnement
Quotidien	Quotidien	k1gInSc4	Quotidien
en	en	k?	en
Chine	Chin	k1gInSc5	Chin
<g/>
/	/	kIx~	/
<g/>
Každodenní	každodenní	k2eAgNnSc4d1	každodenní
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
CGP	CGP	kA	CGP
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
Macao	Macao	k1gNnSc1	Macao
ou	ou	k0	ou
jouer	jouer	k1gInSc1	jouer
la	la	k1gNnSc6	la
différence	différence	k1gFnSc2	différence
<g/>
/	/	kIx~	/
Makao	makao	k1gNnSc1	makao
neboli	neboli	k8xC	neboli
hrát	hrát	k5eAaImF	hrát
si	se	k3xPyFc3	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
(	(	kIx(	(
<g/>
CGP	CGP	kA	CGP
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Porsche	Porsche	k1gNnSc1	Porsche
design	design	k1gInSc1	design
(	(	kIx(	(
<g/>
CGP	CGP	kA	CGP
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
===	===	k?	===
Management	management	k1gInSc1	management
===	===	k?	===
</s>
</p>
<p>
<s>
Práce	práce	k1gFnSc1	práce
dobrovolné	dobrovolný	k2eAgFnSc2d1	dobrovolná
sekretářky	sekretářka	k1gFnSc2	sekretářka
Sdružení	sdružení	k1gNnSc2	sdružení
Francie	Francie	k1gFnSc2	Francie
-	-	kIx~	-
SSSR	SSSR	kA	SSSR
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
-	-	kIx~	-
Jantar	jantar	k1gInSc1	jantar
v	v	k7c6	v
Lituanie	Lituanie	k1gFnSc1	Lituanie
<g/>
/	/	kIx~	/
<g/>
Jantar	jantar	k1gInSc1	jantar
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
;	;	kIx,	;
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
Forney	Fornea	k1gFnSc2	Fornea
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
-	-	kIx~	-
Arts	Arts	k1gInSc1	Arts
traditionnels	traditionnels	k1gInSc1	traditionnels
de	de	k?	de
Bouriatie	Bouriatie	k1gFnSc1	Bouriatie
des	des	k1gNnPc2	des
XIXe	XIXe	k1gNnPc2	XIXe
et	et	k?	et
XXe	XXe	k1gFnSc2	XXe
siè	siè	k?	siè
<g/>
/	/	kIx~	/
<g/>
Tradiční	tradiční	k2eAgNnPc1d1	tradiční
umění	umění	k1gNnPc1	umění
a	a	k8xC	a
řemesla	řemeslo	k1gNnPc1	řemeslo
v	v	k7c6	v
Burjatsku	Burjatsek	k1gInSc6	Burjatsek
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
Musée	Musée	k1gNnSc1	Musée
Le	Le	k1gMnSc2	Le
Prieuré	Prieurý	k2eAgFnSc2d1	Prieurý
de	de	k?	de
Graville	Gravill	k1gMnSc5	Gravill
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
a	a	k8xC	a
katalog	katalog	k1gInSc1	katalog
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
-	-	kIx~	-
Maria	Maria	k1gFnSc1	Maria
Primatchenko	Primatchenka	k1gFnSc5	Primatchenka
<g/>
,	,	kIx,	,
une	une	k?	une
primitive	primitiv	k1gMnSc5	primitiv
ukrainienne	ukrainienn	k1gMnSc5	ukrainienn
<g/>
/	/	kIx~	/
Marija	Marijum	k1gNnPc1	Marijum
Primačenko	Primačenka	k1gFnSc5	Primačenka
<g/>
,	,	kIx,	,
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
naivní	naivní	k2eAgFnSc1d1	naivní
malířka	malířka	k1gFnSc1	malířka
<g/>
,	,	kIx,	,
Musée	Musée	k1gInSc1	Musée
des	des	k1gNnSc1	des
Augustins	Augustins	k1gInSc1	Augustins
<g/>
,	,	kIx,	,
Toulouse	Toulouse	k1gInSc1	Toulouse
<g/>
;	;	kIx,	;
Musée	Musée	k1gInSc1	Musée
des	des	k1gNnSc1	des
Beaux	Beaux	k1gInSc1	Beaux
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
Lyon	Lyon	k1gInSc1	Lyon
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
-	-	kIx~	-
Art	Art	k1gMnSc5	Art
populaire	populair	k1gMnSc5	populair
du	du	k?	du
Tadjikistan	Tadjikistan	k1gInSc1	Tadjikistan
<g/>
/	/	kIx~	/
Lidové	lidový	k2eAgNnSc1d1	lidové
umění	umění	k1gNnSc1	umění
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
<g/>
,	,	kIx,	,
Musée	Musé	k1gInSc2	Musé
du	du	k?	du
Prieuré	Prieurý	k2eAgNnSc1d1	Prieuré
de	de	k?	de
Graville	Graville	k1gNnSc1	Graville
<g/>
,	,	kIx,	,
Caen	Caen	k1gNnSc1	Caen
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
-	-	kIx~	-
La	la	k1gNnSc1	la
tapisserie	tapisserie	k1gFnSc2	tapisserie
et	et	k?	et
la	la	k1gNnSc1	la
gravure	gravur	k1gMnSc5	gravur
estoniennes	estoniennes	k1gInSc1	estoniennes
<g/>
/	/	kIx~	/
Estonská	estonský	k2eAgFnSc1d1	Estonská
tapisérie	tapisérie	k1gFnSc1	tapisérie
a	a	k8xC	a
rytina	rytina	k1gFnSc1	rytina
<g/>
,	,	kIx,	,
Chartreuse	Chartreuse	k1gFnSc1	Chartreuse
Villeneuve	Villeneuev	k1gFnSc2	Villeneuev
lè	lè	k?	lè
Avignon	Avignon	k1gInSc1	Avignon
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
-	-	kIx~	-
Miniatures	Miniatures	k1gMnSc1	Miniatures
arméniennes	arméniennes	k1gMnSc1	arméniennes
du	du	k?	du
Maténadaran	Maténadaran	k1gInSc1	Maténadaran
<g/>
/	/	kIx~	/
Arménské	arménský	k2eAgFnSc2d1	arménská
miniatury	miniatura	k1gFnSc2	miniatura
z	z	k7c2	z
Maténadaranu	Maténadaran	k1gInSc2	Maténadaran
;	;	kIx,	;
Musée	Musé	k1gInSc2	Musé
du	du	k?	du
Prieuré	Prieurý	k2eAgNnSc1d1	Prieuré
de	de	k?	de
Graville	Gravill	k1gMnSc5	Gravill
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
;	;	kIx,	;
Bibliothè	Bibliothè	k1gMnSc6	Bibliothè
Forney	Fornea	k1gFnSc2	Fornea
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
-	-	kIx~	-
Salomon	Salomon	k1gMnSc1	Salomon
Telingater	Telingater	k1gMnSc1	Telingater
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
œ	œ	k?	œ
graphique	graphique	k1gInSc1	graphique
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
-	-	kIx~	-
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
populaire	populair	k1gInSc5	populair
de	de	k?	de
Turkménie	Turkménie	k1gFnSc2	Turkménie
<g/>
/	/	kIx~	/
Lidové	lidový	k2eAgNnSc1d1	lidové
umění	umění	k1gNnSc1	umění
Turkmenistánu	Turkmenistán	k1gInSc2	Turkmenistán
<g/>
;	;	kIx,	;
Musée	Musée	k1gFnSc1	Musée
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Homme	Homm	k1gMnSc5	Homm
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
-	-	kIx~	-
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
populaire	populair	k1gInSc5	populair
ouzbek	ouzbek	k1gInSc1	ouzbek
<g/>
/	/	kIx~	/
Uzbecké	uzbecký	k2eAgNnSc1d1	uzbecké
lidové	lidový	k2eAgNnSc1d1	lidové
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Musée	Musée	k1gNnSc1	Musée
du	du	k?	du
Prieuré	Prieurý	k2eAgFnSc2d1	Prieurý
de	de	k?	de
Graville	Gravill	k1gMnSc5	Gravill
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
-	-	kIx~	-
Niko	nika	k1gFnSc5	nika
Pirosmanichvili	Pirosmanichvili	k1gMnSc5	Pirosmanichvili
<g/>
,	,	kIx,	,
un	un	k?	un
grand	grand	k1gMnSc1	grand
peintre	peintr	k1gInSc5	peintr
géorgien	géorgina	k1gFnPc2	géorgina
<g/>
/	/	kIx~	/
Niko	nika	k1gFnSc5	nika
Pirosmanišvili	Pirosmanišvili	k1gMnSc1	Pirosmanišvili
<g/>
,	,	kIx,	,
velký	velký	k2eAgMnSc1d1	velký
gruzínský	gruzínský	k2eAgMnSc1d1	gruzínský
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
Nice	Nice	k1gFnSc1	Nice
<g/>
,	,	kIx,	,
Musée	Musée	k1gInSc1	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Art	Art	k1gMnSc1	Art
Naï	Naï	k1gMnSc1	Naï
<g/>
,	,	kIx,	,
Nice	Nice	k1gFnSc1	Nice
<g/>
;	;	kIx,	;
Musée	Musée	k1gInSc1	Musée
de	de	k?	de
la	la	k1gNnSc7	la
Charité	Charitý	k2eAgFnSc2d1	Charitý
<g/>
,	,	kIx,	,
Marseille	Marseille	k1gFnPc4	Marseille
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
-	-	kIx~	-
Tschestniakof	Tschestniakof	k1gInSc1	Tschestniakof
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
un	un	k?	un
peintre	peintr	k1gMnSc5	peintr
russe	russ	k1gMnSc5	russ
/	/	kIx~	/
<g/>
Čestňakov	Čestňakov	k1gInSc1	Čestňakov
<g/>
,	,	kIx,	,
<g/>
ruský	ruský	k2eAgMnSc1d1	ruský
malíř	malíř	k1gMnSc1	malíř
<g/>
;	;	kIx,	;
Musée	Musé	k1gMnSc4	Musé
du	du	k?	du
Prieuré	Prieurý	k2eAgNnSc1d1	Prieuré
de	de	k?	de
Graville	Gravill	k1gMnSc5	Gravill
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
-	-	kIx~	-
Gravure	Gravur	k1gMnSc5	Gravur
soviétique	soviétiquus	k1gMnSc5	soviétiquus
contemporaine	contemporain	k1gMnSc5	contemporain
/	/	kIx~	/
<g/>
Současná	současný	k2eAgFnSc1d1	současná
ruská	ruský	k2eAgFnSc1d1	ruská
rytina	rytina	k1gFnSc1	rytina
<g/>
;	;	kIx,	;
Musée	Musée	k1gInSc1	Musée
des	des	k1gNnSc1	des
Beaux	Beaux	k1gInSc1	Beaux
Arts	Artsa	k1gFnPc2	Artsa
André	André	k1gMnPc2	André
Malraux	Malraux	k1gInSc4	Malraux
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
-	-	kIx~	-
Miniatures	Miniatures	k1gMnSc1	Miniatures
géorgiennes	géorgiennes	k1gMnSc1	géorgiennes
du	du	k?	du
XIè	XIè	k1gMnSc1	XIè
au	au	k0	au
XVIIIè	XVIIIè	k1gFnPc6	XVIIIè
siè	siè	k?	siè
/	/	kIx~	/
<g/>
Gruzínské	gruzínský	k2eAgFnSc2d1	gruzínská
miniatury	miniatura	k1gFnSc2	miniatura
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
Centre	centr	k1gInSc5	centr
international	internationat	k5eAaPmAgInS	internationat
de	de	k?	de
Grasse	Grass	k1gMnSc2	Grass
<g/>
;	;	kIx,	;
Musée	Musée	k1gInSc1	Musée
des	des	k1gNnSc1	des
Beaux	Beaux	k1gInSc1	Beaux
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
Saintes	Saintes	k1gInSc1	Saintes
<g/>
;	;	kIx,	;
Musée	Musée	k1gNnSc1	Musée
du	du	k?	du
Prieuré	Prieurý	k2eAgFnSc2d1	Prieurý
de	de	k?	de
Graville	Gravill	k1gMnSc5	Gravill
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
;	;	kIx,	;
Musée	Muséus	k1gMnSc5	Muséus
basque	basquus	k1gMnSc5	basquus
<g/>
,	,	kIx,	,
Bayonne	Bayonn	k1gMnSc5	Bayonn
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
-	-	kIx~	-
Art	Art	k1gMnSc5	Art
populaire	populair	k1gMnSc5	populair
de	de	k?	de
Kirghizie	Kirghizie	k1gFnPc1	Kirghizie
/	/	kIx~	/
Lidové	lidový	k2eAgNnSc1d1	lidové
umění	umění	k1gNnSc1	umění
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
<g/>
;	;	kIx,	;
"	"	kIx"	"
Le	Le	k1gFnSc2	Le
Prieuré	Prieurý	k2eAgFnSc2d1	Prieurý
"	"	kIx"	"
de	de	k?	de
St.	st.	kA	st.
Just	just	k6eAd1	just
St.	st.	kA	st.
Lambert	Lambert	k1gInSc1	Lambert
<g/>
;	;	kIx,	;
Musée	Musée	k1gInSc1	Musée
du	du	k?	du
Berry	Berra	k1gFnSc2	Berra
Bourges	Bourgesa	k1gFnPc2	Bourgesa
<g/>
,	,	kIx,	,
Musée	Musée	k1gNnPc2	Musée
de	de	k?	de
Normandie	Normandie	k1gFnSc2	Normandie
Caen	Caena	k1gFnPc2	Caena
<g/>
;	;	kIx,	;
Musée	Musée	k1gFnPc2	Musée
du	du	k?	du
Prieuré	Prieurý	k2eAgFnSc2d1	Prieurý
de	de	k?	de
Graville	Gravill	k1gMnSc5	Gravill
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
-	-	kIx~	-
Tapisseries	Tapisseries	k1gMnSc1	Tapisseries
contemporaines	contemporaines	k1gMnSc1	contemporaines
de	de	k?	de
Lettonie	Lettonie	k1gFnSc2	Lettonie
/	/	kIx~	/
<g/>
Současné	současný	k2eAgFnPc1d1	současná
litevské	litevský	k2eAgFnPc1d1	Litevská
tapisérie	tapisérie	k1gFnPc1	tapisérie
<g/>
,	,	kIx,	,
Musée	Musé	k1gInPc1	Musé
du	du	k?	du
Prieuré	Prieurý	k2eAgFnSc2d1	Prieurý
de	de	k?	de
Graville	Gravill	k1gMnSc5	Gravill
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
-	-	kIx~	-
Les	les	k1gInSc1	les
arts	arts	k6eAd1	arts
décoratifs	décoratifs	k6eAd1	décoratifs
en	en	k?	en
Azerbaï	Azerbaï	k1gFnSc1	Azerbaï
/	/	kIx~	/
<g/>
Užité	užitý	k2eAgNnSc1d1	užité
umění	umění	k1gNnSc1	umění
v	v	k7c6	v
Ázerbajdžánu	Ázerbajdžán	k1gInSc6	Ázerbajdžán
<g/>
;	;	kIx,	;
Musée	Musée	k1gInSc1	Musée
des	des	k1gNnSc1	des
Arts	Artsa	k1gFnPc2	Artsa
Décoratifs	Décoratifsa	k1gFnPc2	Décoratifsa
<g/>
,	,	kIx,	,
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
<g/>
;	;	kIx,	;
Musée	Musée	k1gInSc1	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Art	Art	k1gMnSc1	Art
et	et	k?	et
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Histoire	Histoir	k1gMnSc5	Histoir
<g/>
,	,	kIx,	,
Belfort	Belfort	k1gInSc1	Belfort
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
-	-	kIx~	-
Le	Le	k1gFnPc2	Le
jouet	jouet	k5eAaPmF	jouet
russe	russa	k1gFnSc3	russa
au	au	k0	au
XIXe	XIXe	k1gNnSc2	XIXe
siè	siè	k?	siè
et	et	k?	et
aujourd	aujourd	k1gInSc1	aujourd
<g/>
'	'	kIx"	'
<g/>
hui	hui	k?	hui
/	/	kIx~	/
<g/>
Ruská	ruský	k2eAgFnSc1d1	ruská
hračka	hračka	k1gFnSc1	hračka
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
Musée	Musée	k1gInSc1	Musée
du	du	k?	du
Jouet	Jouet	k1gInSc1	Jouet
de	de	k?	de
Poissy	Poissa	k1gFnSc2	Poissa
<g/>
;	;	kIx,	;
Musée	Musé	k1gMnSc4	Musé
du	du	k?	du
Prieuré	Prieurý	k2eAgNnSc1d1	Prieuré
de	de	k?	de
Graville	Gravill	k1gMnSc5	Gravill
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
-	-	kIx~	-
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
architecture	architectur	k1gMnSc5	architectur
de	de	k?	de
la	la	k1gNnPc2	la
Ville	Ville	k1gNnPc2	Ville
en	en	k?	en
Russie	Russie	k1gFnSc2	Russie
1945-1985	[number]	k4	1945-1985
/	/	kIx~	/
<g/>
Městzská	Městzský	k2eAgFnSc1d1	Městzský
architektura	architektura	k1gFnSc1	architektura
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
1945	[number]	k4	1945
-	-	kIx~	-
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
Ecole	Ecole	k1gInSc1	Ecole
des	des	k1gNnSc1	des
Beaux	Beaux	k1gInSc1	Beaux
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
;	;	kIx,	;
koncepce	koncepce	k1gFnSc1	koncepce
katalogu	katalog	k1gInSc2	katalog
</s>
</p>
<p>
<s>
===	===	k?	===
Výstavy	výstava	k1gFnPc1	výstava
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Ivry-sur-Seine	Ivryur-Sein	k1gMnSc5	Ivry-sur-Sein
===	===	k?	===
</s>
</p>
<p>
<s>
Koncepce	koncepce	k1gFnSc1	koncepce
a	a	k8xC	a
realizace	realizace	k1gFnSc1	realizace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
-	-	kIx~	-
Kachama	Kachamum	k1gNnSc2	Kachamum
<g/>
:	:	kIx,	:
créations	créationsit	k5eAaPmRp2nS	créationsit
textiles	textiles	k1gInSc1	textiles
thaï	thaï	k?	thaï
<g/>
/	/	kIx~	/
Thajská	thajský	k2eAgFnSc1d1	thajská
textilní	textilní	k2eAgFnSc1d1	textilní
tvorba	tvorba	k1gFnSc1	tvorba
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
Aboubakar	Aboubakar	k1gInSc1	Aboubakar
Fofana	Fofana	k1gFnSc1	Fofana
<g/>
:	:	kIx,	:
Variations	Variations	k1gInSc1	Variations
sur	sur	k?	sur
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
indigo	indigo	k1gNnSc1	indigo
<g/>
/	/	kIx~	/
Variace	variace	k1gFnPc1	variace
v	v	k7c6	v
indigu	indigo	k1gNnSc6	indigo
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
Elodie	Elodie	k1gFnSc1	Elodie
Brunet	brunet	k1gMnSc1	brunet
<g/>
:	:	kIx,	:
Textiles	Textiles	k1gMnSc1	Textiles
philippins	philippins	k6eAd1	philippins
en	en	k?	en
soie	soie	k1gInSc1	soie
et	et	k?	et
fibres	fibres	k1gInSc1	fibres
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
ananas	ananas	k1gInSc1	ananas
<g/>
/	/	kIx~	/
Filipínské	filipínský	k2eAgFnSc2d1	filipínská
textilie	textilie	k1gFnSc2	textilie
z	z	k7c2	z
hedvábí	hedvábí	k1gNnSc2	hedvábí
a	a	k8xC	a
z	z	k7c2	z
vlákna	vlákno	k1gNnSc2	vlákno
ananasu	ananas	k1gInSc2	ananas
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
Ahouk	Ahouk	k1gMnSc1	Ahouk
<g/>
:	:	kIx,	:
Art	Art	k1gMnSc1	Art
textile	textil	k1gInSc5	textil
contemporain	contemporain	k2eAgInSc1d1	contemporain
/	/	kIx~	/
<g/>
Současné	současný	k2eAgNnSc1d1	současné
textilní	textilní	k2eAgNnSc1d1	textilní
umění	umění	k1gNnSc1	umění
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
Martine	Martin	k1gMnSc5	Martin
Cieutat	Cieutat	k1gMnSc5	Cieutat
<g/>
:	:	kIx,	:
Créations	Créationsa	k1gFnPc2	Créationsa
textiles	textiles	k1gInSc1	textiles
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
syrskými	syrský	k2eAgMnPc7d1	syrský
umělci	umělec	k1gMnPc7	umělec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
Boro	Boro	k1gNnSc1	Boro
<g/>
...	...	k?	...
katagami	kataga	k1gFnPc7	kataga
<g/>
,	,	kIx,	,
sakabukoro	sakabukora	k1gFnSc5	sakabukora
<g/>
:	:	kIx,	:
traditions	traditionsit	k5eAaPmRp2nS	traditionsit
populaires	populaires	k1gMnSc1	populaires
japonaises	japonaises	k1gMnSc1	japonaises
/	/	kIx~	/
Japonské	japonský	k2eAgFnSc2d1	japonská
lidové	lidový	k2eAgFnSc2d1	lidová
tradice	tradice	k1gFnSc2	tradice
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
Gradis	Gradis	k1gInSc4	Gradis
<g/>
/	/	kIx~	/
<g/>
Watanebe	Wataneb	k1gMnSc5	Wataneb
:	:	kIx,	:
les	les	k1gInSc1	les
Appliqués	Appliquésa	k1gFnPc2	Appliquésa
/	/	kIx~	/
<g/>
textilní	textilní	k2eAgFnSc2d1	textilní
aplikace	aplikace	k1gFnSc2	aplikace
z	z	k7c2	z
Watanebe	Wataneb	k1gInSc5	Wataneb
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Tissages	Tissages	k1gInSc1	Tissages
rapportés	rapportés	k1gInSc1	rapportés
du	du	k?	du
Nagaland	Nagaland	k1gInSc1	Nagaland
/	/	kIx~	/
<g/>
Tkané	tkaný	k2eAgFnSc2d1	tkaná
textilie	textilie	k1gFnSc2	textilie
z	z	k7c2	z
indického	indický	k2eAgInSc2d1	indický
Nágálandu	Nágáland	k1gInSc2	Nágáland
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Diversité	Diversitý	k2eAgNnSc1d1	Diversitý
des	des	k1gNnSc1	des
IKATS	IKATS	kA	IKATS
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
archipel	archipel	k1gInSc1	archipel
indonésien	indonésien	k1gInSc1	indonésien
/	/	kIx~	/
<g/>
Rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
tvorby	tvorba	k1gFnSc2	tvorba
z	z	k7c2	z
Indonésie	Indonésie	k1gFnSc2	Indonésie
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Inde	Ind	k1gMnSc5	Ind
des	des	k1gNnSc4	des
saris	saris	k1gFnPc1	saris
/	/	kIx~	/
<g/>
Indické	indický	k2eAgNnSc1d1	indické
sárí	sárí	k1gNnSc1	sárí
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Couvertures	Couvertures	k1gInSc1	Couvertures
de	de	k?	de
mariage	mariage	k1gInSc1	mariage
Niger	Niger	k1gInSc1	Niger
<g/>
;	;	kIx,	;
<g/>
tapas	tapas	k1gInSc1	tapas
Mbuti	Mbuť	k1gFnSc2	Mbuť
R.	R.	kA	R.
<g/>
D.	D.	kA	D.
du	du	k?	du
Congo	Congo	k1gNnSc1	Congo
/	/	kIx~	/
<g/>
Svatební	svatební	k2eAgFnPc1d1	svatební
přikrývky	přikrývka	k1gFnPc1	přikrývka
z	z	k7c2	z
Nigeru	Niger	k1gInSc2	Niger
a	a	k8xC	a
Konga	Kongo	k1gNnSc2	Kongo
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnSc2	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
-	-	kIx~	-
Turkmè	Turkmè	k1gMnPc1	Turkmè
<g/>
/	/	kIx~	/
Turkméni	Turkmén	k1gMnPc1	Turkmén
<g/>
,	,	kIx,	,
foto	foto	k1gNnSc1	foto
Marc	Marc	k1gInSc1	Marc
Garanger	Garanger	k1gMnSc1	Garanger
<g/>
;	;	kIx,	;
vydal	vydat	k5eAaPmAgMnS	vydat
Arthaud	Arthaud	k1gMnSc1	Arthaud
Paříž	Paříž	k1gFnSc4	Paříž
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
-	-	kIx~	-
Samarkande-Boukhara	Samarkande-Boukhara	k1gFnSc1	Samarkande-Boukhara
<g/>
/	/	kIx~	/
Samarkand	Samarkand	k1gInSc1	Samarkand
-	-	kIx~	-
Buchara	Buchara	k1gFnSc1	Buchara
<g/>
,	,	kIx,	,
foto	foto	k1gNnSc1	foto
a	a	k8xC	a
spoluautor	spoluautor	k1gMnSc1	spoluautor
Marc	Marc	k1gFnSc4	Marc
Garanger	Garangra	k1gFnPc2	Garangra
<g/>
;	;	kIx,	;
vydal	vydat	k5eAaPmAgMnS	vydat
Arthaud	Arthaud	k1gMnSc1	Arthaud
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
-	-	kIx~	-
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
objet	objet	k2eAgInSc1d1	objet
industriel	industriel	k1gInSc1	industriel
en	en	k?	en
question	question	k1gInSc1	question
<g/>
/	/	kIx~	/
Otázky	otázka	k1gFnSc2	otázka
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
Jan	Jan	k1gMnSc1	Jan
Tučný	tučný	k2eAgMnSc1d1	tučný
<g/>
,	,	kIx,	,
vydaloː	vydaloː	k?	vydaloː
Éditions	Éditions	k1gInSc1	Éditions
du	du	k?	du
RegardEditorka	RegardEditorka	k1gFnSc1	RegardEditorka
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Éditions	Éditionsa	k1gFnPc2	Éditionsa
du	du	k?	du
Regard	Regard	k1gMnSc1	Regard
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
-	-	kIx~	-
H.	H.	kA	H.
Borisova	Borisův	k2eAgFnSc1d1	Borisova
<g/>
,	,	kIx,	,
G.	G.	kA	G.
Sternine	Sternin	k1gInSc5	Sternin
:	:	kIx,	:
Art	Art	k1gFnSc3	Art
nouveau	nouveau	k5eAaPmIp1nS	nouveau
russe	russe	k1gFnSc1	russe
<g/>
/	/	kIx~	/
<g/>
Ruská	ruský	k2eAgFnSc1d1	ruská
secese	secese	k1gFnSc1	secese
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
-	-	kIx~	-
A.	A.	kA	A.
Kamenski	Kamensk	k1gFnSc6	Kamensk
:	:	kIx,	:
Chagall	Chagall	k1gInSc1	Chagall
période	périod	k1gInSc5	périod
russe	russe	k1gFnPc1	russe
et	et	k?	et
soviétique	soviétique	k1gFnSc7	soviétique
1907	[number]	k4	1907
-	-	kIx~	-
1922	[number]	k4	1922
/	/	kIx~	/
<g/>
Chagallovo	Chagallův	k2eAgNnSc1d1	Chagallovo
ruské	ruský	k2eAgNnSc1d1	ruské
a	a	k8xC	a
sovětské	sovětský	k2eAgNnSc1d1	sovětské
období	období	k1gNnSc1	období
1907-1922	[number]	k4	1907-1922
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
-	-	kIx~	-
V.	V.	kA	V.
Tolstoi	Tolstoi	k1gNnSc1	Tolstoi
<g/>
:	:	kIx,	:
Art	Art	k1gFnSc1	Art
décoratif	décoratif	k1gInSc1	décoratif
soviétique	soviétique	k1gFnSc1	soviétique
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
/	/	kIx~	/
Sovětské	sovětský	k2eAgNnSc1d1	sovětské
užité	užitý	k2eAgNnSc1d1	užité
umění	umění	k1gNnSc1	umění
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
;	;	kIx,	;
spoluautoři	spoluautor	k1gMnPc1	spoluautor
Grigorij	Grigorij	k1gFnSc2	Grigorij
Chudakov	Chudakov	k1gInSc1	Chudakov
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Suslova	Suslův	k2eAgFnSc1d1	Suslova
<g/>
,	,	kIx,	,
Lilia	lilium	k1gNnPc4	lilium
Ukhtomskaja	Ukhtomskaj	k2eAgNnPc4d1	Ukhtomskaj
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
-	-	kIx~	-
S.	S.	kA	S.
Khan	Khan	k1gInSc1	Khan
Magomedov	Magomedov	k1gInSc1	Magomedov
:	:	kIx,	:
VHOUTEMAS	VHOUTEMAS	kA	VHOUTEMAS
1920-1930	[number]	k4	1920-1930
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
-	-	kIx~	-
Thierry	Thierra	k1gFnSc2	Thierra
Mugler	Mugler	k1gInSc1	Mugler
<g/>
:	:	kIx,	:
Photographie	Photographie	k1gFnSc1	Photographie
(	(	kIx(	(
<g/>
organizace	organizace	k1gFnSc1	organizace
reportáže	reportáž	k1gFnSc2	reportáž
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
Stati	stať	k1gFnSc2	stať
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
-	-	kIx~	-
Macao	Macao	k1gNnSc1	Macao
<g/>
,	,	kIx,	,
<g/>
cité	cité	k1gNnSc1	cité
ambiguë	ambiguë	k?	ambiguë
aux	aux	k?	aux
portes	portes	k1gInSc1	portes
de	de	k?	de
la	la	k1gNnSc1	la
Chine	Chin	k1gInSc5	Chin
<g/>
/	/	kIx~	/
Makao	makao	k1gNnSc1	makao
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
město	město	k1gNnSc1	město
v	v	k7c6	v
branách	brána	k1gFnPc6	brána
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Critique	Critique	k1gFnSc1	Critique
<g/>
,	,	kIx,	,
srpen-září	srpenáří	k2eAgFnSc1d1	srpen-září
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
https://web.archive.org/web/20170914100935/http://histoiresdefils.org/	[url]	k4	https://web.archive.org/web/20170914100935/http://histoiresdefils.org/
</s>
</p>
<p>
<s>
https://www.centrepompidou.fr/cpv/resource/c94EAz/r4bA5zB	[url]	k4	https://www.centrepompidou.fr/cpv/resource/c94EAz/r4bA5zB
</s>
</p>
<p>
<s>
http://www.myheritage.com/names/helena_larroche	[url]	k6eAd1	http://www.myheritage.com/names/helena_larroche
</s>
</p>
