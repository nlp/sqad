<s>
Kdo	kdo	k3yInSc1	kdo
je	on	k3xPp3gMnPc4	on
je	být	k5eAaImIp3nS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
publicistka	publicistka	k1gFnSc1	publicistka
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
komisařka	komisařka	k1gFnSc1	komisařka
výstav	výstav	k1gInSc1	výstav
moderního	moderní	k2eAgNnSc2d1	moderní
a	a	k8xC	a
etnického	etnický	k2eAgNnSc2d1	etnické
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
manažerka	manažerka	k1gFnSc1	manažerka
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
kulturní	kulturní	k2eAgFnSc2d1	kulturní
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
?	?	kIx.	?
</s>
