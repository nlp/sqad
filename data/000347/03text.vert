<s>
Trojúhelník	trojúhelník	k1gInSc1	trojúhelník
je	být	k5eAaImIp3nS	být
geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
určený	určený	k2eAgInSc1d1	určený
třemi	tři	k4xCgInPc7	tři
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
neležícími	ležící	k2eNgInPc7d1	ležící
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
vlastností	vlastnost	k1gFnPc2	vlastnost
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
v	v	k7c6	v
"	"	kIx"	"
<g/>
obyčejné	obyčejný	k2eAgFnSc6d1	obyčejná
<g/>
"	"	kIx"	"
euklidovské	euklidovský	k2eAgFnSc6d1	euklidovská
rovině	rovina	k1gFnSc6	rovina
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
součet	součet	k1gInSc4	součet
velikostí	velikost	k1gFnPc2	velikost
jeho	jeho	k3xOp3gInPc2	jeho
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
úhlů	úhel	k1gInPc2	úhel
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
180	[number]	k4	180
<g/>
°	°	k?	°
(	(	kIx(	(
<g/>
π	π	k?	π
v	v	k7c6	v
obloukové	obloukový	k2eAgFnSc6d1	oblouková
míře	míra	k1gFnSc6	míra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
sférický	sférický	k2eAgInSc4d1	sférický
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
na	na	k7c6	na
kulové	kulový	k2eAgFnSc6d1	kulová
ploše	plocha	k1gFnSc6	plocha
má	mít	k5eAaImIp3nS	mít
součet	součet	k1gInSc1	součet
velikostí	velikost	k1gFnPc2	velikost
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
úhlů	úhel	k1gInPc2	úhel
vždy	vždy	k6eAd1	vždy
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
180	[number]	k4	180
<g/>
°	°	k?	°
a	a	k8xC	a
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
v	v	k7c6	v
hyperbolické	hyperbolický	k2eAgFnSc6d1	hyperbolická
(	(	kIx(	(
<g/>
Lobačevského	Lobačevský	k2eAgMnSc2d1	Lobačevský
<g/>
)	)	kIx)	)
rovině	rovina	k1gFnSc6	rovina
vždy	vždy	k6eAd1	vždy
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
180	[number]	k4	180
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
tedy	tedy	k8xC	tedy
podstatně	podstatně	k6eAd1	podstatně
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
geometrických	geometrický	k2eAgFnPc6d1	geometrická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
leží	ležet	k5eAaImIp3nS	ležet
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
poznatky	poznatek	k1gInPc1	poznatek
platí	platit	k5eAaImIp3nP	platit
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
pro	pro	k7c4	pro
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
v	v	k7c6	v
euklidovské	euklidovský	k2eAgFnSc6d1	euklidovská
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Úsečky	úsečka	k1gFnPc1	úsečka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
vrcholy	vrchol	k1gInPc7	vrchol
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
strany	strana	k1gFnPc1	strana
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Úhly	úhel	k1gInPc1	úhel
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
svírají	svírat	k5eAaImIp3nP	svírat
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
úhly	úhel	k1gInPc1	úhel
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Úhly	úhel	k1gInPc1	úhel
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
k	k	k7c3	k
vnitřním	vnitřní	k2eAgInPc3d1	vnitřní
úhlům	úhel	k1gInPc3	úhel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
vnější	vnější	k2eAgInPc1d1	vnější
úhly	úhel	k1gInPc1	úhel
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
3	[number]	k4	3
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
úhly	úhel	k1gInPc1	úhel
<g/>
,	,	kIx,	,
6	[number]	k4	6
vnějších	vnější	k2eAgInPc2d1	vnější
úhlů	úhel	k1gInPc2	úhel
(	(	kIx(	(
<g/>
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
vrcholu	vrchol	k1gInSc2	vrchol
dva	dva	k4xCgInPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trojúhelník	trojúhelník	k1gInSc1	trojúhelník
nemá	mít	k5eNaImIp3nS	mít
úhlopříčky	úhlopříčka	k1gFnPc4	úhlopříčka
<g/>
.	.	kIx.	.
</s>
<s>
Trojúhelník	trojúhelník	k1gInSc1	trojúhelník
se	se	k3xPyFc4	se
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
pomocí	pomocí	k7c2	pomocí
jeho	jeho	k3xOp3gInPc2	jeho
vrcholů	vrchol	k1gInPc2	vrchol
a	a	k8xC	a
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholy	vrchol	k1gInPc1	vrchol
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
velkým	velký	k2eAgNnSc7d1	velké
tiskacím	tiskací	k2eAgNnSc7d1	tiskací
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
,	,	kIx,	,
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
příslušným	příslušný	k2eAgNnSc7d1	příslušné
protějšímu	protější	k2eAgInSc3d1	protější
vrcholu	vrchol	k1gInSc3	vrchol
<g/>
,	,	kIx,	,
úhly	úhel	k1gInPc1	úhel
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
malým	malý	k2eAgNnSc7d1	malé
řeckým	řecký	k2eAgNnSc7d1	řecké
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Trojúhelník	trojúhelník	k1gInSc1	trojúhelník
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
symbolem	symbol	k1gInSc7	symbol
Δ	Δ	k?	Δ
následovaným	následovaný	k2eAgInSc7d1	následovaný
výčtem	výčet	k1gInSc7	výčet
všech	všecek	k3xTgInPc2	všecek
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Trojúhelník	trojúhelník	k1gInSc1	trojúhelník
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
určen	určen	k2eAgInSc1d1	určen
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
sss	sss	k?	sss
<g/>
)	)	kIx)	)
délkou	délka	k1gFnSc7	délka
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
sus	sus	k?	sus
<g/>
)	)	kIx)	)
délkou	délka	k1gFnSc7	délka
dvou	dva	k4xCgInPc6	dva
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
velikostí	velikost	k1gFnSc7	velikost
úhlu	úhel	k1gInSc2	úhel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
svírají	svírat	k5eAaImIp3nP	svírat
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
usu	usus	k1gInSc2	usus
<g/>
)	)	kIx)	)
délkou	délka	k1gFnSc7	délka
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
velikostí	velikost	k1gFnSc7	velikost
úhlů	úhel	k1gInPc2	úhel
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přiléhají	přiléhat	k5eAaImIp3nP	přiléhat
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Ssu	Ssu	k1gFnSc1	Ssu
<g/>
)	)	kIx)	)
délkou	délka	k1gFnSc7	délka
dvou	dva	k4xCgInPc6	dva
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
velikostí	velikost	k1gFnPc2	velikost
úhlu	úhel	k1gInSc2	úhel
proti	proti	k7c3	proti
větší	veliký	k2eAgNnPc4d2	veliký
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konstrukci	konstrukce	k1gFnSc3	konstrukce
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
použít	použít	k5eAaPmF	použít
i	i	k9	i
výšky	výška	k1gFnPc4	výška
<g/>
,	,	kIx,	,
těžnice	těžnice	k1gFnPc4	těžnice
atd.	atd.	kA	atd.
Strany	strana	k1gFnSc2	strana
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
splňují	splňovat	k5eAaImIp3nP	splňovat
trojúhelníkové	trojúhelníkový	k2eAgFnPc1d1	trojúhelníková
nerovnosti	nerovnost	k1gFnPc1	nerovnost
<g/>
:	:	kIx,	:
Součet	součet	k1gInSc1	součet
dvou	dva	k4xCgFnPc2	dva
libovolných	libovolný	k2eAgFnPc2d1	libovolná
stran	strana	k1gFnPc2	strana
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
strana	strana	k1gFnSc1	strana
třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
>	>	kIx)	>
c	c	k0	c
a	a	k8xC	a
+	+	kIx~	+
c	c	k0	c
>	>	kIx)	>
b	b	k?	b
b	b	k?	b
+	+	kIx~	+
c	c	k0	c
>	>	kIx)	>
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
a	a	k8xC	a
-	-	kIx~	-
b	b	k?	b
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
a-b	a	k?	a-b
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
a	a	k8xC	a
-	-	kIx~	-
c	c	k0	c
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
a-c	a	k?	a-c
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
b	b	k?	b
-	-	kIx~	-
c	c	k0	c
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
b-c	b	k?	b-c
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
:	:	kIx,	:
</s>
<s>
Součet	součet	k1gInSc1	součet
všech	všecek	k3xTgInPc2	všecek
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
úhlů	úhel	k1gInPc2	úhel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
180	[number]	k4	180
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Součet	součet	k1gInSc1	součet
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
a	a	k8xC	a
příslušného	příslušný	k2eAgInSc2d1	příslušný
vnějšího	vnější	k2eAgInSc2d1	vnější
úhlu	úhel	k1gInSc2	úhel
je	být	k5eAaImIp3nS	být
180	[number]	k4	180
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Součet	součet	k1gInSc4	součet
dvou	dva	k4xCgInPc2	dva
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
úhlů	úhel	k1gInPc2	úhel
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
vnějšímu	vnější	k2eAgInSc3d1	vnější
úhlu	úhel	k1gInSc3	úhel
u	u	k7c2	u
zbývajícího	zbývající	k2eAgInSc2d1	zbývající
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
většímu	veliký	k2eAgInSc3d2	veliký
úhlu	úhel	k1gInSc3	úhel
leží	ležet	k5eAaImIp3nS	ležet
větší	veliký	k2eAgFnSc1d2	veliký
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
α	α	k?	α
+	+	kIx~	+
α	α	k?	α
<g/>
'	'	kIx"	'
=	=	kIx~	=
β	β	k?	β
+	+	kIx~	+
β	β	k?	β
<g/>
'	'	kIx"	'
=	=	kIx~	=
γ	γ	k?	γ
+	+	kIx~	+
γ	γ	k?	γ
<g/>
'	'	kIx"	'
=	=	kIx~	=
180	[number]	k4	180
<g/>
°	°	k?	°
α	α	k?	α
+	+	kIx~	+
β	β	k?	β
=	=	kIx~	=
γ	γ	k?	γ
<g/>
'	'	kIx"	'
α	α	k?	α
+	+	kIx~	+
γ	γ	k?	γ
=	=	kIx~	=
β	β	k?	β
<g/>
'	'	kIx"	'
β	β	k?	β
+	+	kIx~	+
γ	γ	k?	γ
<g/>
=	=	kIx~	=
α	α	k?	α
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s hack="1">
α	α	k?	α
+	+	kIx~	+
β	β	k?	β
+	+	kIx~	+
γ	γ	k?	γ
=	=	kIx~	=
180	[number]	k4	180
<g/>
°	°	k?	°
α	α	k?	α
<g/>
'	'	kIx"	'
+	+	kIx~	+
β	β	k?	β
<g/>
'	'	kIx"	'
+	+	kIx~	+
γ	γ	k?	γ
<g/>
'	'	kIx"	'
=	=	kIx~	=
360	[number]	k4	360
<g/>
°	°	k?	°
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
+	+	kIx~	+
sin	sin	kA	sin
:	:	kIx,	:
β	β	k?	β
+	+	kIx~	+
sin	sin	kA	sin
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
4	[number]	k4	4
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
gamma	gamm	k1gMnSc2	gamm
=	=	kIx~	=
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
gamma	gamma	k1gNnSc1	gamma
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
α	α	k?	α
+	+	kIx~	+
cos	cos	kA	cos
:	:	kIx,	:
β	β	k?	β
+	+	kIx~	+
cos	cos	kA	cos
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
1	[number]	k4	1
+	+	kIx~	+
4	[number]	k4	4
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
gamma	gamm	k1gMnSc2	gamm
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}}	}}}	k?	}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
+	+	kIx~	+
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
+	+	kIx~	+
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
2	[number]	k4	2
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
cos	cos	kA	cos
:	:	kIx,	:
α	α	k?	α
cos	cos	kA	cos
:	:	kIx,	:
β	β	k?	β
cos	cos	kA	cos
:	:	kIx,	:
γ	γ	k?	γ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
gamma	gamm	k1gMnSc2	gamm
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
+	+	kIx~	+
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
β	β	k?	β
+	+	kIx~	+
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
1	[number]	k4	1
-	-	kIx~	-
2	[number]	k4	2
cos	cos	kA	cos
:	:	kIx,	:
α	α	k?	α
cos	cos	kA	cos
:	:	kIx,	:
β	β	k?	β
cos	cos	kA	cos
:	:	kIx,	:
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cos	cos	kA	cos
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
gamma	gamm	k1gMnSc2	gamm
}	}	kIx)	}
:	:	kIx,	:
Obecný	obecný	k2eAgInSc1d1	obecný
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
není	být	k5eNaImIp3nS	být
osově	osově	k6eAd1	osově
ani	ani	k8xC	ani
středově	středově	k6eAd1	středově
souměrný	souměrný	k2eAgMnSc1d1	souměrný
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
osově	osově	k6eAd1	osově
souměrné	souměrný	k2eAgInPc1d1	souměrný
např.	např.	kA	např.
<g/>
:	:	kIx,	:
rovnoramenný	rovnoramenný	k2eAgInSc1d1	rovnoramenný
a	a	k8xC	a
rovnostranný	rovnostranný	k2eAgInSc1d1	rovnostranný
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
úhly	úhel	k1gInPc7	úhel
a	a	k8xC	a
stranami	strana	k1gFnPc7	strana
určují	určovat	k5eAaImIp3nP	určovat
sinová	sinový	k2eAgFnSc1d1	sinová
<g/>
,	,	kIx,	,
kosinová	kosinový	k2eAgFnSc1d1	kosinová
a	a	k8xC	a
tangentová	tangentový	k2eAgFnSc1d1	tangentová
věta	věta	k1gFnSc1	věta
<g/>
.	.	kIx.	.
</s>
<s>
Zavedeme	zavést	k5eAaPmIp1nP	zavést
<g/>
-li	i	k?	-li
veličinu	veličina	k1gFnSc4	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
+	+	kIx~	+
c	c	k0	c
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
velikosti	velikost	k1gFnPc1	velikost
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
úhlů	úhel	k1gInPc2	úhel
určit	určit	k5eAaPmF	určit
ze	z	k7c2	z
vztahů	vztah	k1gInPc2	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
b	b	k?	b
)	)	kIx)	)
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
c	c	k0	c
)	)	kIx)	)
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
b	b	k?	b
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
s-b	s	k?	s-b
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
s-c	s	k?	s-c
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
bc	bc	k?	bc
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
c	c	k0	c
)	)	kIx)	)
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
a	a	k8xC	a
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
c	c	k0	c
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
s-c	s	k?	s-c
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
(	(	kIx(	(
<g/>
s-a	s	k?	s-a
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
ac	ac	k?	ac
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
a	a	k8xC	a
)	)	kIx)	)
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
s-a	s	k?	s-a
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
s-b	s	k?	s-b
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
ab	ab	k?	ab
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
a	a	k8xC	a
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
cos	cos	kA	cos
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
s-a	s	k?	s-a
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
bc	bc	k?	bc
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cos	cos	kA	cos
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
s-b	s	k?	s-b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
ac	ac	k?	ac
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
c	c	k0	c
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cos	cos	kA	cos
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
s-c	s	k?	s-c
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
ab	ab	k?	ab
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Obecný	obecný	k2eAgInSc1d1	obecný
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
(	(	kIx(	(
<g/>
též	též	k9	též
různostranný	různostranný	k2eAgMnSc1d1	různostranný
<g/>
)	)	kIx)	)
-	-	kIx~	-
žádné	žádný	k3yNgFnPc4	žádný
dvě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
nejsou	být	k5eNaImIp3nP	být
shodné	shodný	k2eAgInPc1d1	shodný
Rovnoramenný	rovnoramenný	k2eAgInSc1d1	rovnoramenný
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
-	-	kIx~	-
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
shodné	shodný	k2eAgFnPc1d1	shodná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
shodné	shodný	k2eAgInPc1d1	shodný
s	s	k7c7	s
třetí	třetí	k4xOgFnSc7	třetí
stranou	strana	k1gFnSc7	strana
Rovnostranný	rovnostranný	k2eAgInSc4d1	rovnostranný
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
-	-	kIx~	-
všechny	všechen	k3xTgFnPc1	všechen
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
shodné	shodný	k2eAgNnSc4d1	shodné
Ostroúhlý	ostroúhlý	k2eAgInSc4d1	ostroúhlý
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
-	-	kIx~	-
všechny	všechen	k3xTgInPc1	všechen
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
úhly	úhel	k1gInPc1	úhel
jsou	být	k5eAaImIp3nP	být
ostré	ostrý	k2eAgNnSc4d1	ostré
Pravoúhlý	pravoúhlý	k2eAgInSc4d1	pravoúhlý
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
-	-	kIx~	-
jeden	jeden	k4xCgInSc4	jeden
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
<g />
.	.	kIx.	.
</s>
<s>
úhel	úhel	k1gInSc1	úhel
je	být	k5eAaImIp3nS	být
pravý	pravý	k2eAgMnSc1d1	pravý
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgMnSc1d1	zbývající
dva	dva	k4xCgInPc4	dva
jsou	být	k5eAaImIp3nP	být
ostré	ostrý	k2eAgFnSc2d1	ostrá
Tupoúhlý	tupoúhlý	k2eAgInSc4d1	tupoúhlý
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
-	-	kIx~	-
jeden	jeden	k4xCgInSc4	jeden
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
úhel	úhel	k1gInSc4	úhel
je	být	k5eAaImIp3nS	být
tupý	tupý	k2eAgMnSc1d1	tupý
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgMnSc1d1	zbývající
dva	dva	k4xCgInPc4	dva
jsou	být	k5eAaImIp3nP	být
ostré	ostrý	k2eAgInPc4d1	ostrý
Výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
úsečka	úsečka	k1gFnSc1	úsečka
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
krajními	krajní	k2eAgInPc7d1	krajní
body	bod	k1gInPc7	bod
jsou	být	k5eAaImIp3nP	být
vrcholy	vrchol	k1gInPc4	vrchol
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
a	a	k8xC	a
pata	pata	k1gFnSc1	pata
kolmice	kolmice	k1gFnSc2	kolmice
vedené	vedený	k2eAgFnSc2d1	vedená
tímto	tento	k3xDgInSc7	tento
vrcholem	vrchol	k1gInSc7	vrchol
na	na	k7c4	na
protější	protější	k2eAgFnSc4d1	protější
stranu	strana	k1gFnSc4	strana
<g/>
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
stručně	stručně	k6eAd1	stručně
řečeno	říct	k5eAaPmNgNnS	říct
kolmice	kolmice	k1gFnSc2	kolmice
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
z	z	k7c2	z
protější	protější	k2eAgFnSc2d1	protější
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průsečík	průsečík	k1gInSc1	průsečík
výšky	výška	k1gFnSc2	výška
s	s	k7c7	s
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
stranou	strana	k1gFnSc7	strana
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pata	pata	k1gFnSc1	pata
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
výšky	výška	k1gFnPc4	výška
<g/>
.	.	kIx.	.
</s>
<s>
Přímky	přímka	k1gFnPc1	přímka
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
leží	ležet	k5eAaImIp3nS	ležet
výšky	výška	k1gFnPc4	výška
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ortocentrum	ortocentrum	k1gNnSc1	ortocentrum
<g/>
.	.	kIx.	.
</s>
<s>
Ortocentrum	ortocentrum	k1gNnSc1	ortocentrum
leží	ležet	k5eAaImIp3nS	ležet
buď	buď	k8xC	buď
uvnitř	uvnitř	k7c2	uvnitř
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ostroúhlý	ostroúhlý	k2eAgMnSc1d1	ostroúhlý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
pravoúhlého	pravoúhlý	k2eAgInSc2d1	pravoúhlý
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
pravý	pravý	k2eAgInSc4d1	pravý
úhel	úhel	k1gInSc4	úhel
anebo	anebo	k8xC	anebo
leží	ležet	k5eAaImIp3nS	ležet
vně	vně	k6eAd1	vně
u	u	k7c2	u
tupoúhlého	tupoúhlý	k2eAgInSc2d1	tupoúhlý
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Spojnice	spojnice	k1gFnSc1	spojnice
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
pat	pata	k1gFnPc2	pata
výšek	výška	k1gFnPc2	výška
tvoří	tvořit	k5eAaImIp3nP	tvořit
ortický	ortický	k2eAgInSc4d1	ortický
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Pravoúhlý	pravoúhlý	k2eAgInSc1d1	pravoúhlý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
svůj	svůj	k3xOyFgInSc4	svůj
ortický	ortický	k2eAgInSc4d1	ortický
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnPc1	jeho
dvě	dva	k4xCgFnPc1	dva
paty	pata	k1gFnPc1	pata
výšek	výška	k1gFnPc2	výška
splývají	splývat	k5eAaImIp3nP	splývat
<g/>
.	.	kIx.	.
</s>
<s>
Ortocentrum	ortocentrum	k1gNnSc1	ortocentrum
ostroúhlého	ostroúhlý	k2eAgInSc2d1	ostroúhlý
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
je	být	k5eAaImIp3nS	být
středem	střed	k1gInSc7	střed
kružnice	kružnice	k1gFnSc2	kružnice
vepsané	vepsaný	k2eAgFnSc2d1	vepsaná
jeho	jeho	k3xOp3gInSc3	jeho
ortickému	ortický	k2eAgInSc3d1	ortický
trojúhelníku	trojúhelník	k1gInSc3	trojúhelník
<g/>
;	;	kIx,	;
ortocentrum	ortocentrum	k1gNnSc1	ortocentrum
tupoúhlého	tupoúhlý	k2eAgInSc2d1	tupoúhlý
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
je	být	k5eAaImIp3nS	být
středem	středem	k7c2	středem
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
kružnic	kružnice	k1gFnPc2	kružnice
připsaných	připsaný	k2eAgFnPc2d1	připsaná
jeho	jeho	k3xOp3gInSc3	jeho
ortickému	ortický	k2eAgInSc3d1	ortický
trojúhelníku	trojúhelník	k1gInSc3	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Výšky	výška	k1gFnPc1	výška
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
v	v	k7c6	v
s	s	k7c7	s
dolním	dolní	k2eAgInSc7d1	dolní
indexem	index	k1gInSc7	index
příslušné	příslušný	k2eAgFnSc2d1	příslušná
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výšky	výška	k1gFnPc4	výška
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c4	v
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
:	:	kIx,	:
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
:	:	kIx,	:
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
:	:	kIx,	:
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
:	:	kIx,	:
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Velikosti	velikost	k1gFnSc6	velikost
výšek	výška	k1gFnPc2	výška
jsou	být	k5eAaImIp3nP	být
určeny	určen	k2eAgInPc1d1	určen
vztahy	vztah	k1gInPc1	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
b	b	k?	b
sin	sin	kA	sin
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
c	c	k0	c
sin	sin	kA	sin
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
gamma	gamm	k1gMnSc2	gamm
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
a	a	k8xC	a
sin	sin	kA	sin
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
c	c	k0	c
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
gamma	gamm	k1gMnSc2	gamm
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
a	a	k8xC	a
sin	sin	kA	sin
:	:	kIx,	:
β	β	k?	β
=	=	kIx~	=
b	b	k?	b
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
:	:	kIx,	:
Těžnice	těžnice	k1gFnSc1	těžnice
je	být	k5eAaImIp3nS	být
úsečka	úsečka	k1gFnSc1	úsečka
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
krajními	krajní	k2eAgInPc7d1	krajní
body	bod	k1gInPc7	bod
jsou	být	k5eAaImIp3nP	být
střed	střed	k1gInSc4	střed
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
protilehlý	protilehlý	k2eAgInSc4d1	protilehlý
vrchol	vrchol	k1gInSc4	vrchol
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
těžnice	těžnice	k1gFnPc4	těžnice
<g/>
.	.	kIx.	.
</s>
<s>
Těžnice	těžnice	k1gFnPc1	těžnice
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
těžiště	těžiště	k1gNnSc1	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
každou	každý	k3xTgFnSc4	každý
těžnici	těžnice	k1gFnSc4	těžnice
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc4	díl
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
:	:	kIx,	:
1	[number]	k4	1
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
těžiště	těžiště	k1gNnSc2	těžiště
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
je	být	k5eAaImIp3nS	být
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
středu	střed	k1gInSc2	střed
protější	protější	k2eAgFnSc2d1	protější
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
těžnice	těžnice	k1gFnSc1	těžnice
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc4	díl
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
obsahem	obsah	k1gInSc7	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Těžnice	těžnice	k1gFnPc1	těžnice
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
t	t	k?	t
s	s	k7c7	s
dolním	dolní	k2eAgInSc7d1	dolní
indexem	index	k1gInSc7	index
příslušné	příslušný	k2eAgFnSc2d1	příslušná
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
těžiště	těžiště	k1gNnSc1	těžiště
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
písmenem	písmeno	k1gNnSc7	písmeno
T.	T.	kA	T.
Těžiště	těžiště	k1gNnSc2	těžiště
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
vrcholy	vrchol	k1gInPc1	vrchol
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
tvoří	tvořit	k5eAaImIp3nP	tvořit
postupně	postupně	k6eAd1	postupně
tři	tři	k4xCgInPc1	tři
trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
(	(	kIx(	(
<g/>
ABT	ABT	kA	ABT
<g/>
,	,	kIx,	,
ACT	ACT	kA	ACT
<g/>
,	,	kIx,	,
CBT	CBT	kA	CBT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Délky	délka	k1gFnPc1	délka
těžnic	těžnice	k1gFnPc2	těžnice
jsou	být	k5eAaImIp3nP	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
-	-	kIx~	-
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-a	-a	k?	-a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
(	(	kIx(	(
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
-	-	kIx~	-
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-b	-b	k?	-b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}}	}}}	k?	}}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
-	-	kIx~	-
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-c	-c	k?	-c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Střední	střední	k2eAgFnSc1d1	střední
příčka	příčka	k1gFnSc1	příčka
je	být	k5eAaImIp3nS	být
spojnice	spojnice	k1gFnSc1	spojnice
středů	střed	k1gInPc2	střed
dvou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
dvou	dva	k4xCgFnPc2	dva
pat	pata	k1gFnPc2	pata
těžnic	těžnice	k1gFnPc2	těžnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
střední	střední	k2eAgFnPc4d1	střední
příčky	příčka	k1gFnPc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
příčka	příčka	k1gFnSc1	příčka
je	být	k5eAaImIp3nS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
poloviny	polovina	k1gFnSc2	polovina
příslušné	příslušný	k2eAgFnSc2d1	příslušná
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnPc1d1	střední
příčky	příčka	k1gFnPc1	příčka
dohromady	dohromady	k6eAd1	dohromady
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
shodné	shodný	k2eAgInPc4d1	shodný
trojúhelníky	trojúhelník	k1gInPc4	trojúhelník
-	-	kIx~	-
příčkový	příčkový	k2eAgInSc4d1	příčkový
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
trojúhelníky	trojúhelník	k1gInPc4	trojúhelník
při	při	k7c6	při
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
vrcholech	vrchol	k1gInPc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
těžištěm	těžiště	k1gNnSc7	těžiště
jeho	on	k3xPp3gInSc2	on
příčkového	příčkový	k2eAgInSc2d1	příčkový
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnPc1d1	střední
příčky	příčka	k1gFnPc1	příčka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
s.	s.	k?	s.
Strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
dvakrát	dvakrát	k6eAd1	dvakrát
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
ležící	ležící	k2eAgFnSc1d1	ležící
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Symediána	Symedián	k1gMnSc4	Symedián
je	být	k5eAaImIp3nS	být
osově	osově	k6eAd1	osově
souměrný	souměrný	k2eAgInSc1d1	souměrný
obraz	obraz	k1gInSc1	obraz
těžnice	těžnice	k1gFnSc2	těžnice
podle	podle	k7c2	podle
osy	osa	k1gFnSc2	osa
příslušného	příslušný	k2eAgInSc2d1	příslušný
úhlu	úhel	k1gInSc2	úhel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
symediána	symedián	k1gMnSc2	symedián
těžnice	těžnice	k1gFnSc2	těžnice
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
A	a	k8xC	a
podle	podle	k7c2	podle
osy	osa	k1gFnSc2	osa
úhlu	úhel	k1gInSc2	úhel
při	při	k7c6	při
vrcholu	vrchol	k1gInSc6	vrchol
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
symediány	symedián	k1gInPc4	symedián
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
symediány	symedián	k1gInPc1	symedián
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Lemoinův	Lemoinův	k2eAgInSc1d1	Lemoinův
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Lemoinův	Lemoinův	k2eAgInSc1d1	Lemoinův
bod	bod	k1gInSc1	bod
leží	ležet	k5eAaImIp3nS	ležet
uvnitř	uvnitř	k7c2	uvnitř
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
bodů	bod	k1gInPc2	bod
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
nejmenší	malý	k2eAgInSc4d3	nejmenší
součet	součet	k1gInSc4	součet
čtverců	čtverec	k1gInPc2	čtverec
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
od	od	k7c2	od
stran	strana	k1gFnPc2	strana
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
Lemoinovým	Lemoinův	k2eAgInSc7d1	Lemoinův
bodem	bod	k1gInSc7	bod
vedeme	vést	k5eAaImIp1nP	vést
rovnoběžky	rovnoběžka	k1gFnPc4	rovnoběžka
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
průsečíky	průsečík	k1gInPc1	průsečík
těchto	tento	k3xDgFnPc2	tento
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
se	s	k7c7	s
stranami	strana	k1gFnPc7	strana
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
šest	šest	k4xCc1	šest
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
první	první	k4xOgFnSc1	první
Lemoinova	Lemoinův	k2eAgFnSc1d1	Lemoinův
kružnice	kružnice	k1gFnSc1	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc4	střed
první	první	k4xOgFnSc2	první
Lemoinovy	Lemoinův	k2eAgFnSc2d1	Lemoinův
kružnice	kružnice	k1gFnSc2	kružnice
je	být	k5eAaImIp3nS	být
středem	střed	k1gInSc7	střed
úsečky	úsečka	k1gFnSc2	úsečka
spojující	spojující	k2eAgFnSc2d1	spojující
Lemoinův	Lemoinův	k2eAgInSc4d1	Lemoinův
bod	bod	k1gInSc4	bod
a	a	k8xC	a
střed	střed	k1gInSc4	střed
kružnice	kružnice	k1gFnSc2	kružnice
opsané	opsaný	k2eAgFnSc2d1	opsaná
<g/>
.	.	kIx.	.
</s>
<s>
Osa	osa	k1gFnSc1	osa
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
úhlu	úhel	k1gInSc2	úhel
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
úhel	úhel	k1gInSc4	úhel
a	a	k8xC	a
současně	současně	k6eAd1	současně
protější	protější	k2eAgFnSc4d1	protější
stranu	strana	k1gFnSc4	strana
dělí	dělit	k5eAaImIp3nP	dělit
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
délek	délka	k1gFnPc2	délka
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Osa	osa	k1gFnSc1	osa
vnějšího	vnější	k2eAgInSc2d1	vnější
úhlu	úhel	k1gInSc2	úhel
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
vnější	vnější	k2eAgInSc4d1	vnější
úhel	úhel	k1gInSc4	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
je	být	k5eAaImIp3nS	být
osa	osa	k1gFnSc1	osa
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
úhlu	úhel	k1gInSc2	úhel
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
o	o	k7c4	o
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
o_	o_	k?	o_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
,	,	kIx,	,
osa	osa	k1gFnSc1	osa
vnějšího	vnější	k2eAgInSc2d1	vnější
úhlu	úhel	k1gInSc2	úhel
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
o	o	k7c4	o
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
o_	o_	k?	o_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
a	a	k8xC	a
také	také	k9	také
těžnice	těžnice	k1gFnSc1	těžnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
osy	osa	k1gFnSc2	osa
i	i	k9	i
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
úhly	úhel	k1gInPc1	úhel
rozdělíme	rozdělit	k5eAaPmIp1nP	rozdělit
přímkami	přímka	k1gFnPc7	přímka
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
stejné	stejný	k2eAgInPc4d1	stejný
díly	díl	k1gInPc4	díl
(	(	kIx(	(
<g/>
trisekce	trisekce	k1gFnSc1	trisekce
úhlu	úhel	k1gInSc2	úhel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průsečíky	průsečík	k1gInPc1	průsečík
těchto	tento	k3xDgFnPc2	tento
přímek	přímka	k1gFnPc2	přímka
(	(	kIx(	(
<g/>
vždy	vždy	k6eAd1	vždy
těch	ten	k3xDgMnPc2	ten
dvou	dva	k4xCgInPc2	dva
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
bližší	blízký	k2eAgFnSc3d2	bližší
dané	daný	k2eAgFnSc3d1	daná
straně	strana	k1gFnSc3	strana
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
)	)	kIx)	)
vždy	vždy	k6eAd1	vždy
tvoří	tvořit	k5eAaImIp3nS	tvořit
rovnostranný	rovnostranný	k2eAgInSc1d1	rovnostranný
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
(	(	kIx(	(
<g/>
Morleyova	Morleyův	k2eAgFnSc1d1	Morleyův
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osa	osa	k1gFnSc1	osa
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
kolmice	kolmice	k1gFnSc1	kolmice
vedená	vedený	k2eAgFnSc1d1	vedená
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Osy	osa	k1gFnPc1	osa
stran	strana	k1gFnPc2	strana
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bodě	bod	k1gInSc6	bod
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
bod	bod	k1gInSc1	bod
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
vrcholů	vrchol	k1gInPc2	vrchol
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eulerova	Eulerův	k2eAgFnSc1d1	Eulerova
přímka	přímka	k1gFnSc1	přímka
je	být	k5eAaImIp3nS	být
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
těžištěm	těžiště	k1gNnSc7	těžiště
a	a	k8xC	a
ortocentrem	ortocentrum	k1gNnSc7	ortocentrum
nebo	nebo	k8xC	nebo
středem	střed	k1gInSc7	střed
přímky	přímka	k1gFnSc2	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Eulerově	Eulerův	k2eAgFnSc6d1	Eulerova
přímce	přímka	k1gFnSc6	přímka
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
střed	střed	k1gInSc1	střed
kružnice	kružnice	k1gFnSc2	kružnice
opsané	opsaný	k2eAgFnSc2d1	opsaná
a	a	k8xC	a
střed	střed	k1gInSc1	střed
kružnice	kružnice	k1gFnSc2	kružnice
devíti	devět	k4xCc2	devět
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rovnostranném	rovnostranný	k2eAgInSc6d1	rovnostranný
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
těžiště	těžiště	k1gNnSc4	těžiště
a	a	k8xC	a
ortocentrum	ortocentrum	k1gNnSc4	ortocentrum
splývají	splývat	k5eAaImIp3nP	splývat
<g/>
,	,	kIx,	,
takový	takový	k3xDgInSc1	takový
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
Eulerovu	Eulerův	k2eAgFnSc4d1	Eulerova
přímku	přímka	k1gFnSc4	přímka
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
přímka	přímka	k1gFnSc1	přímka
p	p	k?	p
protíná	protínat	k5eAaImIp3nS	protínat
přímky	přímka	k1gFnPc4	přímka
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
leží	ležet	k5eAaImIp3nP	ležet
strany	strana	k1gFnPc1	strana
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
v	v	k7c6	v
bodech	bod	k1gInPc6	bod
X	X	kA	X
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
,	,	kIx,	,
Z	Z	kA	Z
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
středy	střed	k1gInPc1	střed
úseček	úsečka	k1gFnPc2	úsečka
AX	AX	kA	AX
<g/>
,	,	kIx,	,
BY	by	k9	by
<g/>
,	,	kIx,	,
CZ	CZ	kA	CZ
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
přímce	přímka	k1gFnSc6	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
přímka	přímka	k1gFnSc1	přímka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Gaussova	Gaussův	k2eAgFnSc1d1	Gaussova
přímka	přímka	k1gFnSc1	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Kružnice	kružnice	k1gFnSc1	kružnice
opsaná	opsaný	k2eAgFnSc1d1	opsaná
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
je	být	k5eAaImIp3nS	být
kružnice	kružnice	k1gFnSc1	kružnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
všemi	všecek	k3xTgInPc7	všecek
vrcholy	vrchol	k1gInPc7	vrchol
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
trojúhelníku	trojúhelník	k1gInSc3	trojúhelník
lze	lze	k6eAd1	lze
opsat	opsat	k5eAaPmF	opsat
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
kružnice	kružnice	k1gFnSc2	kružnice
opsané	opsaný	k2eAgFnSc2d1	opsaná
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
průsečíku	průsečík	k1gInSc6	průsečík
os	osa	k1gFnPc2	osa
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
poloměr	poloměr	k1gInSc1	poloměr
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
středu	střed	k1gInSc2	střed
od	od	k7c2	od
libovolného	libovolný	k2eAgInSc2d1	libovolný
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Spojnice	spojnice	k1gFnSc1	spojnice
středu	střed	k1gInSc2	střed
kružnice	kružnice	k1gFnSc2	kružnice
opsané	opsaný	k2eAgFnSc2d1	opsaná
a	a	k8xC	a
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
vrcholů	vrchol	k1gInPc2	vrchol
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
jsou	být	k5eAaImIp3nP	být
kolmé	kolmý	k2eAgFnPc1d1	kolmá
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
stranám	strana	k1gFnPc3	strana
jeho	jeho	k3xOp3gInSc2	jeho
ortického	ortický	k2eAgInSc2d1	ortický
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Nagelova	Nagelův	k2eAgFnSc1d1	Nagelova
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
poloměru	poloměr	k1gInSc2	poloměr
opsané	opsaný	k2eAgFnSc2d1	opsaná
kružnice	kružnice	k1gFnSc2	kružnice
určuje	určovat	k5eAaImIp3nS	určovat
vztah	vztah	k1gInSc1	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
sin	sin	kA	sin
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
sin	sin	kA	sin
:	:	kIx,	:
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
gamma	gamm	k1gMnSc2	gamm
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Kružnice	kružnice	k1gFnSc1	kružnice
vepsaná	vepsaný	k2eAgFnSc1d1	vepsaná
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
je	být	k5eAaImIp3nS	být
kružnice	kružnice	k1gFnSc1	kružnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
trojúhelníku	trojúhelník	k1gInSc3	trojúhelník
lze	lze	k6eAd1	lze
vepsat	vepsat	k5eAaPmF	vepsat
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
kružnice	kružnice	k1gFnSc2	kružnice
vepsané	vepsaný	k2eAgFnSc2d1	vepsaná
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
průsečíku	průsečík	k1gInSc6	průsečík
os	osa	k1gFnPc2	osa
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
úhlů	úhel	k1gInPc2	úhel
<g/>
,	,	kIx,	,
poloměr	poloměr	k1gInSc1	poloměr
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
kolmé	kolmý	k2eAgFnPc4d1	kolmá
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
středu	střed	k1gInSc2	střed
od	od	k7c2	od
libovolné	libovolný	k2eAgFnSc2d1	libovolná
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
poloměr	poloměr	k1gInSc4	poloměr
kružnice	kružnice	k1gFnSc2	kružnice
vepsané	vepsaný	k2eAgNnSc1d1	vepsané
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
+	+	kIx~	+
c	c	k0	c
)	)	kIx)	)
tg	tg	kA	tg
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
tg	tg	kA	tg
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
tg	tg	kA	tg
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s hack="1">
s	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
tg	tg	kA	tg
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc4	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
tg	tg	kA	tg
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
tg	tg	kA	tg
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
středy	střed	k1gInPc7	střed
kružnice	kružnice	k1gFnSc2	kružnice
vepsané	vepsaný	k2eAgInPc1d1	vepsaný
a	a	k8xC	a
opsané	opsaný	k2eAgInPc1d1	opsaný
je	on	k3xPp3gMnPc4	on
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
2	[number]	k4	2
r	r	kA	r
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gMnSc1	sqrt
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Kružnice	kružnice	k1gFnSc1	kružnice
připsaná	připsaný	k2eAgFnSc1d1	připsaná
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
je	být	k5eAaImIp3nS	být
kružnice	kružnice	k1gFnSc1	kružnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
přímek	přímka	k1gFnPc2	přímka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
prodloužením	prodloužení	k1gNnSc7	prodloužení
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
stran	strana	k1gFnPc2	strana
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
kružnice	kružnice	k1gFnSc2	kružnice
připsané	připsaný	k2eAgFnSc2d1	připsaná
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
průsečíku	průsečík	k1gInSc6	průsečík
osy	osa	k1gFnPc4	osa
jednoho	jeden	k4xCgInSc2	jeden
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
úhlu	úhel	k1gInSc2	úhel
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
vnějších	vnější	k2eAgInPc2d1	vnější
úhlů	úhel	k1gInPc2	úhel
při	při	k7c6	při
zbývajících	zbývající	k2eAgInPc6d1	zbývající
dvou	dva	k4xCgInPc6	dva
vrcholech	vrchol	k1gInPc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc1	tři
kružnice	kružnice	k1gFnPc1	kružnice
připsané	připsaný	k2eAgFnPc1d1	připsaná
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
o	o	k7c4	o
se	se	k3xPyFc4	se
vypočte	vypočíst	k5eAaPmIp3nS	vypočíst
jako	jako	k9	jako
součet	součet	k1gInSc1	součet
všech	všecek	k3xTgFnPc2	všecek
jeho	jeho	k3xOp3gFnPc2	jeho
stran	strana	k1gFnPc2	strana
<g/>
:	:	kIx,	:
o	o	k7c4	o
=	=	kIx~	=
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
+	+	kIx~	+
c	c	k0	c
,	,	kIx,	,
kde	kde	k6eAd1	kde
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
jsou	být	k5eAaImIp3nP	být
strany	strana	k1gFnPc1	strana
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
Obsah	obsah	k1gInSc1	obsah
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
S	s	k7c7	s
se	se	k3xPyFc4	se
vypočte	vypočíst	k5eAaPmIp3nS	vypočíst
jako	jako	k9	jako
polovina	polovina	k1gFnSc1	polovina
součinu	součin	k1gInSc2	součin
libovolné	libovolný	k2eAgFnSc2d1	libovolná
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
k	k	k7c3	k
ní	on	k3xPp3gFnSc2	on
příslušné	příslušný	k2eAgFnSc2d1	příslušná
výšky	výška	k1gFnSc2	výška
<g/>
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s hack="1">
×	×	k?	×
v	v	k7c6	v
/	/	kIx~	/
2	[number]	k4	2
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c4	v
je	on	k3xPp3gMnPc4	on
výška	výška	k1gFnSc1	výška
příslušná	příslušný	k2eAgFnSc1d1	příslušná
straně	strana	k1gFnSc3	strana
z	z	k7c2	z
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
příslušná	příslušný	k2eAgFnSc1d1	příslušná
výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
obsah	obsah	k1gInSc4	obsah
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
vypočítat	vypočítat	k5eAaPmF	vypočítat
podle	podle	k7c2	podle
Heronova	Heronův	k2eAgInSc2d1	Heronův
vzorce	vzorec	k1gInSc2	vzorec
s	s	k7c7	s
=	=	kIx~	=
o	o	k0	o
/	/	kIx~	/
2	[number]	k4	2
kde	kde	k6eAd1	kde
o	o	k7c4	o
je	být	k5eAaImIp3nS	být
obvod	obvod	k1gInSc1	obvod
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
S	s	k7c7	s
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
a	a	k8xC	a
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
b	b	k?	b
)	)	kIx)	)
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
c	c	k0	c
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
s-a	s	k?	s-a
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
s-b	s	k?	s-b
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
s-c	s	k?	s-c
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Obsah	obsah	k1gInSc1	obsah
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g />
.	.	kIx.	.
</s>
<s hack="1">
pomocí	pomocí	k7c2	pomocí
poloměru	poloměr	k1gInSc2	poloměr
kružnice	kružnice	k1gFnSc2	kružnice
opsané	opsaný	k2eAgFnSc2d1	opsaná
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
2	[number]	k4	2
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
α	α	k?	α
sin	sin	kA	sin
:	:	kIx,	:
β	β	k?	β
sin	sin	kA	sin
:	:	kIx,	:
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
abc	abc	k?	abc
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
r	r	kA	r
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
gamma	gamm	k1gMnSc2	gamm
}	}	kIx)	}
:	:	kIx,	:
Obsah	obsah	k1gInSc1	obsah
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
pomocí	pomocí	k7c2	pomocí
poloměru	poloměr	k1gInSc2	poloměr
kružnice	kružnice	k1gFnSc2	kružnice
vepsané	vepsaný	k2eAgFnSc2d1	vepsaná
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}	}	kIx)	}
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
+	+	kIx~	+
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}	}	kIx)	}
:	:	kIx,	:
Obsah	obsah	k1gInSc1	obsah
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
pomocí	pomocí	k7c2	pomocí
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
úhlu	úhel	k1gInSc2	úhel
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
c	c	k0	c
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
β	β	k?	β
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
<g />
.	.	kIx.	.
</s>
<s hack="1">
c	c	k0	c
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
ab	ab	k?	ab
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
gamma	gamm	k1gMnSc2	gamm
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
ac	ac	k?	ac
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
bc	bc	k?	bc
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
Obsah	obsah	k1gInSc1	obsah
obecného	obecný	k2eAgInSc2d1	obecný
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
kde	kde	k6eAd1	kde
(	(	kIx(	(
<g/>
ax	ax	k?	ax
<g/>
,	,	kIx,	,
<g/>
ay	ay	k?	ay
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
bx	bx	k?	bx
<g/>
,	,	kIx,	,
<g/>
by	by	kYmCp3nS	by
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
cx	cx	k?	cx
<g/>
,	,	kIx,	,
<g/>
cy	cy	k?	cy
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
souřadnice	souřadnice	k1gFnPc4	souřadnice
vrcholů	vrchol	k1gInPc2	vrchol
(	(	kIx(	(
<g/>
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
vektorového	vektorový	k2eAgInSc2d1	vektorový
součinu	součin	k1gInSc2	součin
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc2	použití
<g />
.	.	kIx.	.
</s>
<s hack="1">
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
grafice	grafika	k1gFnSc6	grafika
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
-a_	_	k?	-a_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
-a_	_	k?	-a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
-a_	_	k?	-a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
-a_	_	k?	-a_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
:	:	kIx,	:
nebo	nebo	k8xC	nebo
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
]	]	kIx)	]
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
|	|	kIx~	|
<g/>
[	[	kIx(	[
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
-b_	_	k?	-b_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
-c_	_	k?	-c_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
b_	b_	k?	b_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
-a_	_	k?	-a_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
:	:	kIx,	:
Použije	použít	k5eAaPmIp3nS	použít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
předcházející	předcházející	k2eAgInSc4d1	předcházející
vzorec	vzorec	k1gInSc4	vzorec
bez	bez	k7c2	bez
absolutní	absolutní	k2eAgFnSc2d1	absolutní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
ověření	ověření	k1gNnSc4	ověření
zda	zda	k8xS	zda
bod	bod	k1gInSc4	bod
(	(	kIx(	(
<g/>
dx	dx	k?	dx
<g/>
,	,	kIx,	,
<g/>
dy	dy	k?	dy
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
uvnitř	uvnitř	k7c2	uvnitř
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
ABC	ABC	kA	ABC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
leží	ležet	k5eAaImIp3nS	ležet
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
znaménka	znaménko	k1gNnPc1	znaménko
obsahů	obsah	k1gInPc2	obsah
všech	všecek	k3xTgInPc2	všecek
čtyř	čtyři	k4xCgInPc2	čtyři
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
ABD	ABD	kA	ABD
<g/>
,	,	kIx,	,
BCD	BCD	kA	BCD
a	a	k8xC	a
CAD	CAD	kA	CAD
jsou	být	k5eAaImIp3nP	být
stejná	stejný	k2eAgNnPc1d1	stejné
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
<g/>
-li	i	k?	-li
vně	vně	k6eAd1	vně
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
všechny	všechen	k3xTgInPc4	všechen
obsahy	obsah	k1gInPc4	obsah
stejné	stejný	k2eAgNnSc4d1	stejné
znaménko	znaménko	k1gNnSc4	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
kladné	kladný	k2eAgNnSc1d1	kladné
obíhájí	obíhájí	k1gNnSc1	obíhájí
<g/>
-li	i	k?	-li
vrcholy	vrchol	k1gInPc4	vrchol
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
platí	platit	k5eAaImIp3nP	platit
mnohé	mnohý	k2eAgFnPc1d1	mnohá
věty	věta	k1gFnPc1	věta
<g/>
,	,	kIx,	,
vzorce	vzorec	k1gInPc1	vzorec
<g/>
,	,	kIx,	,
poučky	poučka	k1gFnPc1	poučka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
sinová	sinový	k2eAgFnSc1d1	sinová
věta	věta	k1gFnSc1	věta
kosinová	kosinový	k2eAgFnSc1d1	kosinová
věta	věta	k1gFnSc1	věta
(	(	kIx(	(
<g/>
zobecnění	zobecnění	k1gNnSc1	zobecnění
Pythagorovy	Pythagorův	k2eAgFnSc2d1	Pythagorova
věty	věta	k1gFnSc2	věta
na	na	k7c4	na
nepravoúhlý	pravoúhlý	k2eNgInSc4d1	pravoúhlý
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
<g/>
)	)	kIx)	)
tangentová	tangentový	k2eAgFnSc1d1	tangentová
věta	věta	k1gFnSc1	věta
V	v	k7c6	v
pravoúhlém	pravoúhlý	k2eAgInSc6d1	pravoúhlý
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
navíc	navíc	k6eAd1	navíc
<g/>
:	:	kIx,	:
Pythagorova	Pythagorův	k2eAgFnSc1d1	Pythagorova
věta	věta	k1gFnSc1	věta
Obsah	obsah	k1gInSc1	obsah
čtverce	čtverec	k1gInSc2	čtverec
nad	nad	k7c7	nad
přeponou	přepona	k1gFnSc7	přepona
pravoúhlého	pravoúhlý	k2eAgInSc2d1	pravoúhlý
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
součtu	součet	k1gInSc3	součet
obsahů	obsah	k1gInPc2	obsah
čtverců	čtverec	k1gInPc2	čtverec
nad	nad	k7c7	nad
oběma	dva	k4xCgFnPc7	dva
odvěsnami	odvěsna	k1gFnPc7	odvěsna
<g/>
.	.	kIx.	.
vzorec	vzorec	k1gInSc1	vzorec
<g/>
:	:	kIx,	:
c2	c2	k4	c2
=	=	kIx~	=
a2	a2	k4	a2
+	+	kIx~	+
b2	b2	k4	b2
Euklidova	Euklidův	k2eAgFnSc1d1	Euklidova
věta	věta	k1gFnSc1	věta
</s>
