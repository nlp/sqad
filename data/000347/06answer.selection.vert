<s>
Každý	každý	k3xTgInSc1	každý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
3	[number]	k4	3
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
úhly	úhel	k1gInPc1	úhel
<g/>
,	,	kIx,	,
6	[number]	k4	6
vnějších	vnější	k2eAgInPc2d1	vnější
úhlů	úhel	k1gInPc2	úhel
(	(	kIx(	(
<g/>
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
vrcholu	vrchol	k1gInSc2	vrchol
dva	dva	k4xCgInPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
