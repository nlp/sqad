<s>
Nukleon	nukleon	k1gInSc1	nukleon
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc4d1	společný
název	název	k1gInSc4	název
pro	pro	k7c4	pro
proton	proton	k1gInSc4	proton
a	a	k8xC	a
neutron	neutron	k1gInSc4	neutron
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Nukleony	nukleon	k1gInPc1	nukleon
jsou	být	k5eAaImIp3nP	být
baryony	baryona	k1gFnPc4	baryona
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
dublet	dubleta	k1gFnPc2	dubleta
s	s	k7c7	s
izospinem	izospino	k1gNnSc7	izospino
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
složené	složený	k2eAgFnPc1d1	složená
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
třemi	tři	k4xCgInPc7	tři
kvarky	kvark	k1gInPc7	kvark
(	(	kIx(	(
<g/>
a	a	k8xC	a
gluony	gluon	k1gInPc1	gluon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proton	proton	k1gInSc1	proton
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
kvarky	kvark	k1gInPc7	kvark
uud	uud	k?	uud
a	a	k8xC	a
neutron	neutron	k1gInSc1	neutron
kvarky	kvark	k1gInPc1	kvark
udd	udd	k?	udd
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
klidové	klidový	k2eAgFnPc1d1	klidová
hmotnosti	hmotnost	k1gFnPc1	hmotnost
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
shodné	shodný	k2eAgFnPc1d1	shodná
<g/>
,	,	kIx,	,
nepatrně	patrně	k6eNd1	patrně
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
1	[number]	k4	1
dalton	dalton	k1gInSc1	dalton
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
1,66	[number]	k4	1,66
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Proton	proton	k1gInSc1	proton
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
neutron	neutron	k1gInSc1	neutron
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
důsledek	důsledek	k1gInSc4	důsledek
vlivu	vliv	k1gInSc2	vliv
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
interakce	interakce	k1gFnSc2	interakce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
proton	proton	k1gInSc1	proton
má	mít	k5eAaImIp3nS	mít
kladný	kladný	k2eAgInSc4d1	kladný
elementární	elementární	k2eAgInSc4d1	elementární
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
neutron	neutron	k1gInSc1	neutron
má	mít	k5eAaImIp3nS	mít
nulový	nulový	k2eAgInSc4d1	nulový
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
silné	silný	k2eAgFnPc1d1	silná
interakce	interakce	k1gFnPc1	interakce
jsou	být	k5eAaImIp3nP	být
nerozlišitelné	rozlišitelný	k2eNgFnPc1d1	nerozlišitelná
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nukleonové	Nukleonový	k2eAgNnSc1d1	Nukleonový
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
nukleonů	nukleon	k1gInPc2	nukleon
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
A.	A.	kA	A.
Přibližně	přibližně	k6eAd1	přibližně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
atomové	atomový	k2eAgFnSc2d1	atomová
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nukleony	nukleon	k1gInPc1	nukleon
mají	mít	k5eAaImIp3nP	mít
každý	každý	k3xTgInSc4	každý
hmotnost	hmotnost	k1gFnSc4	hmotnost
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
atomová	atomový	k2eAgFnSc1d1	atomová
hmotnostní	hmotnostní	k2eAgFnSc1d1	hmotnostní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
prostý	prostý	k2eAgInSc1d1	prostý
součet	součet	k1gInSc1	součet
hmotností	hmotnost	k1gFnSc7	hmotnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jádro	jádro	k1gNnSc1	jádro
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
hmotnosti	hmotnost	k1gFnSc2	hmotnost
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vazebné	vazebný	k2eAgNnSc1d1	vazebné
energii	energie	k1gFnSc4	energie
jádra	jádro	k1gNnSc2	jádro
podle	podle	k7c2	podle
vztahu	vztah	k1gInSc2	vztah
E	E	kA	E
=	=	kIx~	=
mc2	mc2	k4	mc2
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
E	E	kA	E
je	být	k5eAaImIp3nS	být
vazebná	vazebný	k2eAgFnSc1d1	vazebná
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
m	m	kA	m
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc4	rozdíl
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
c	c	k0	c
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
jádro	jádro	k1gNnSc1	jádro
uhlíku	uhlík	k1gInSc2	uhlík
1	[number]	k4	1
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
šesti	šest	k4xCc2	šest
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
1	[number]	k4	1
atomová	atomový	k2eAgFnSc1d1	atomová
hmotnostní	hmotnostní	k2eAgFnSc1d1	hmotnostní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
jádro	jádro	k1gNnSc1	jádro
1	[number]	k4	1
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
přesně	přesně	k6eAd1	přesně
12	[number]	k4	12
atomových	atomový	k2eAgFnPc2d1	atomová
hmotnostních	hmotnostní	k2eAgFnPc2d1	hmotnostní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
prostému	prostý	k2eAgInSc3d1	prostý
součtu	součet	k1gInSc3	součet
hmotností	hmotnost	k1gFnPc2	hmotnost
jeho	jeho	k3xOp3gFnPc2	jeho
komponent	komponenta	k1gFnPc2	komponenta
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vazebné	vazebný	k2eAgFnSc3d1	vazebná
energii	energie	k1gFnSc3	energie
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
vzniku	vznik	k1gInSc6	vznik
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
nukleonů	nukleon	k1gInPc2	nukleon
a	a	k8xC	a
nebo	nebo	k8xC	nebo
také	také	k9	také
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
rozbití	rozbití	k1gNnSc3	rozbití
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
nukleony	nukleon	k1gInPc4	nukleon
<g/>
.	.	kIx.	.
</s>
<s>
Proton	proton	k1gInSc1	proton
Neutron	neutron	k1gInSc1	neutron
Atomové	atomový	k2eAgFnSc2d1	atomová
jádro	jádro	k1gNnSc1	jádro
</s>
