<s>
Nukleon	nukleon	k1gInSc1	nukleon
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc4d1	společný
název	název	k1gInSc4	název
pro	pro	k7c4	pro
proton	proton	k1gInSc4	proton
a	a	k8xC	a
neutron	neutron	k1gInSc4	neutron
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
