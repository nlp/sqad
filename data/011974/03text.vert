<p>
<s>
Okara	Okara	k6eAd1	Okara
je	být	k5eAaImIp3nS	být
drť	drť	k1gFnSc4	drť
ze	z	k7c2	z
sojových	sojový	k2eAgInPc2d1	sojový
bobů	bob	k1gInPc2	bob
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgFnSc4d1	vznikající
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
sojového	sojový	k2eAgNnSc2d1	sojové
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
japonské	japonský	k2eAgFnSc6d1	japonská
<g/>
,	,	kIx,	,
čínské	čínský	k2eAgFnSc6d1	čínská
i	i	k8xC	i
korejské	korejský	k2eAgFnSc6d1	Korejská
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k9	jako
krmivo	krmivo	k1gNnSc4	krmivo
pro	pro	k7c4	pro
dobytek	dobytek	k1gInSc4	dobytek
a	a	k8xC	a
jako	jako	k9	jako
hnojivo	hnojivo	k1gNnSc1	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
ve	v	k7c6	v
veganské	veganský	k2eAgFnSc6d1	veganská
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Sója	sója	k1gFnSc1	sója
původně	původně	k6eAd1	původně
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
před	před	k7c7	před
čtyřmi	čtyři	k4xCgInPc7	čtyři
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
Číňané	Číňan	k1gMnPc1	Číňan
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc4d2	veliký
nutriční	nutriční	k2eAgFnSc4d1	nutriční
hodnotu	hodnota	k1gFnSc4	hodnota
než	než	k8xS	než
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhled	k1gInSc7	vzhled
se	se	k3xPyFc4	se
okara	okara	k6eAd1	okara
podobá	podobat	k5eAaImIp3nS	podobat
vlhkým	vlhký	k2eAgFnPc3d1	vlhká
pilinám	pilina	k1gFnPc3	pilina
a	a	k8xC	a
chutí	chuť	k1gFnSc7	chuť
připomíná	připomínat	k5eAaImIp3nS	připomínat
vlhký	vlhký	k2eAgInSc1d1	vlhký
drcený	drcený	k2eAgInSc1d1	drcený
kokos	kokos	k1gInSc1	kokos
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
nízký	nízký	k2eAgInSc1d1	nízký
obsah	obsah	k1gInSc1	obsah
tuku	tuk	k1gInSc2	tuk
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
cholesterol	cholesterol	k1gInSc4	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
tofu	tofu	k1gNnSc4	tofu
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ze	z	k7c2	z
sojových	sojový	k2eAgInPc2d1	sojový
bobů	bob	k1gInPc2	bob
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
okara	okara	k6eAd1	okara
univerzální	univerzální	k2eAgFnSc7d1	univerzální
zdravou	zdravý	k2eAgFnSc7d1	zdravá
surovinou	surovina	k1gFnSc7	surovina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
výborně	výborně	k6eAd1	výborně
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
chuť	chuť	k1gFnSc4	chuť
použitých	použitý	k2eAgFnPc2d1	použitá
přísad	přísada	k1gFnPc2	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
nejběžnější	běžný	k2eAgInPc4d3	nejběžnější
alergeny	alergen	k1gInPc4	alergen
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
laktóza	laktóza	k1gFnSc1	laktóza
či	či	k8xC	či
lepek	lepek	k1gInSc1	lepek
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
konzervantů	konzervant	k1gInPc2	konzervant
a	a	k8xC	a
umělých	umělý	k2eAgNnPc2d1	umělé
barviv	barvivo	k1gNnPc2	barvivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výhody	výhoda	k1gFnPc1	výhoda
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
okary	okara	k1gFnSc2	okara
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
váže	vázat	k5eAaImIp3nS	vázat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
kvalitu	kvalita	k1gFnSc4	kvalita
receptur	receptura	k1gFnPc2	receptura
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
záviny	závin	k1gInPc1	závin
jsou	být	k5eAaImIp3nP	být
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
nadýchanější	nadýchaný	k2eAgFnSc3d2	nadýchanější
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
kladem	klad	k1gInSc7	klad
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc1	její
všestrannost	všestrannost	k1gFnSc1	všestrannost
<g/>
:	:	kIx,	:
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
do	do	k7c2	do
sladkých	sladký	k2eAgInPc2d1	sladký
pokrmů	pokrm	k1gInPc2	pokrm
(	(	kIx(	(
<g/>
koláče	koláč	k1gInPc1	koláč
<g/>
,	,	kIx,	,
záviny	závin	k1gInPc1	závin
<g/>
,	,	kIx,	,
deserty	desert	k1gInPc1	desert
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
i	i	k9	i
do	do	k7c2	do
slaných	slaný	k2eAgInPc2d1	slaný
(	(	kIx(	(
<g/>
nádivky	nádivka	k1gFnPc1	nádivka
<g/>
,	,	kIx,	,
polévky	polévka	k1gFnPc1	polévka
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
pokrm	pokrm	k1gInSc1	pokrm
nutričně	nutričně	k6eAd1	nutričně
vylehčí	vylehčit	k5eAaPmIp3nS	vylehčit
<g/>
,	,	kIx,	,
zhodnotí	zhodnotit	k5eAaPmIp3nS	zhodnotit
a	a	k8xC	a
především	především	k6eAd1	především
sníží	snížit	k5eAaPmIp3nS	snížit
jeho	jeho	k3xOp3gFnSc4	jeho
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
polotvar	polotvar	k1gInSc1	polotvar
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
zdravých	zdravý	k2eAgInPc2d1	zdravý
karbanátků	karbanátek	k1gInPc2	karbanátek
(	(	kIx(	(
<g/>
sýrové	sýrový	k2eAgInPc4d1	sýrový
karbanátky	karbanátek	k1gInPc4	karbanátek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomazánek	pomazánka	k1gFnPc2	pomazánka
a	a	k8xC	a
paštik	paštika	k1gFnPc2	paštika
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
ji	on	k3xPp3gFnSc4	on
přidávat	přidávat	k5eAaImF	přidávat
i	i	k9	i
do	do	k7c2	do
párků	párek	k1gInPc2	párek
a	a	k8xC	a
klobás	klobása	k1gFnPc2	klobása
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nízké	nízký	k2eAgFnSc3d1	nízká
energetické	energetický	k2eAgFnSc3d1	energetická
hodnotě	hodnota	k1gFnSc3	hodnota
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
redukční	redukční	k2eAgFnSc4d1	redukční
dietu	dieta	k1gFnSc4	dieta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Okara	Okara	k6eAd1	Okara
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Bio-Life	Bio-Lif	k1gInSc5	Bio-Lif
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
