<s>
Davis	Davis	k1gInSc1	Davis
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Davisův	Davisův	k2eAgInSc1d1	Davisův
pohár	pohár	k1gInSc1	pohár
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tenisová	tenisový	k2eAgFnSc1d1	tenisová
soutěž	soutěž	k1gFnSc1	soutěž
mužských	mužský	k2eAgNnPc2d1	mužské
reprezentačních	reprezentační	k2eAgNnPc2d1	reprezentační
družstev	družstvo	k1gNnPc2	družstvo
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
každoročně	každoročně	k6eAd1	každoročně
hraná	hraný	k2eAgFnSc1d1	hraná
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
mužském	mužský	k2eAgInSc6d1	mužský
sportu	sport	k1gInSc6	sport
<g/>
,	,	kIx,	,
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
tenisovou	tenisový	k2eAgFnSc7d1	tenisová
federací	federace	k1gFnSc7	federace
(	(	kIx(	(
<g/>
ITF	ITF	kA	ITF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
