<s>
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
přerušil	přerušit	k5eAaPmAgMnS	přerušit
tuto	tento	k3xDgFnSc4	tento
227	[number]	k4	227
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgFnSc4d1	trvající
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
nezastával	zastávat	k5eNaImAgMnS	zastávat
žádnou	žádný	k3yNgFnSc4	žádný
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
ani	ani	k8xC	ani
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
