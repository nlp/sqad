<s>
Gottfried	Gottfried	k1gInSc1	Gottfried
Wilhelm	Wilhelma	k1gFnPc2	Wilhelma
von	von	k1gInSc1	von
Leibniz	Leibniz	k1gMnSc1	Leibniz
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1646	[number]	k4	1646
Lipsko	Lipsko	k1gNnSc4	Lipsko
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1716	[number]	k4	1716
Hannover	Hannover	k1gInSc1	Hannover
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
píšící	píšící	k2eAgMnSc1d1	píšící
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
a	a	k8xC	a
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
polyhistora	polyhistor	k1gMnSc4	polyhistor
a	a	k8xC	a
univerzálního	univerzální	k2eAgMnSc4d1	univerzální
génia	génius	k1gMnSc4	génius
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
důležité	důležitý	k2eAgNnSc1d1	důležité
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
dějinách	dějiny	k1gFnPc6	dějiny
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
Isaacu	Isaac	k1gMnSc6	Isaac
Newtonovi	Newton	k1gMnSc6	Newton
objevil	objevit	k5eAaPmAgMnS	objevit
integrální	integrální	k2eAgInSc4d1	integrální
kalkulus	kalkulus	k1gInSc4	kalkulus
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
způsob	způsob	k1gInSc1	způsob
zápisu	zápis	k1gInSc2	zápis
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
René	René	k1gMnSc1	René
Descartem	Descart	k1gMnSc7	Descart
a	a	k8xC	a
Baruchem	Baruch	k1gMnSc7	Baruch
Spinozou	Spinoza	k1gFnSc7	Spinoza
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
největších	veliký	k2eAgMnPc2d3	veliký
racionalistů	racionalist	k1gMnPc2	racionalist
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
předchůdců	předchůdce	k1gMnPc2	předchůdce
moderní	moderní	k2eAgFnSc2d1	moderní
logiky	logika	k1gFnSc2	logika
a	a	k8xC	a
analytické	analytický	k2eAgFnSc2d1	analytická
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
filosofie	filosofie	k1gFnSc1	filosofie
se	se	k3xPyFc4	se
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
odkazovala	odkazovat	k5eAaImAgFnS	odkazovat
na	na	k7c4	na
scholastickou	scholastický	k2eAgFnSc4d1	scholastická
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
logika	logika	k1gFnSc1	logika
hrála	hrát	k5eAaImAgFnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
také	také	k9	také
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
předznamenal	předznamenat	k5eAaPmAgInS	předznamenat
myšlenkové	myšlenkový	k2eAgInPc4d1	myšlenkový
pochody	pochod	k1gInPc4	pochod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
projevily	projevit	k5eAaPmAgFnP	projevit
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
<g/>
,	,	kIx,	,
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
geologii	geologie	k1gFnSc6	geologie
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc6	teorie
pravděpodobnosti	pravděpodobnost	k1gFnSc6	pravděpodobnost
<g/>
,	,	kIx,	,
psychologii	psychologie	k1gFnSc6	psychologie
<g/>
,	,	kIx,	,
jazykovědě	jazykověda	k1gFnSc6	jazykověda
či	či	k8xC	či
informatice	informatika	k1gFnSc6	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgInS	věnovat
se	se	k3xPyFc4	se
i	i	k9	i
politice	politika	k1gFnSc3	politika
<g/>
,	,	kIx,	,
právu	právo	k1gNnSc3	právo
<g/>
,	,	kIx,	,
etice	etika	k1gFnSc3	etika
<g/>
,	,	kIx,	,
teologii	teologie	k1gFnSc6	teologie
a	a	k8xC	a
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
pestrou	pestrý	k2eAgFnSc4d1	pestrá
škálu	škála	k1gFnSc4	škála
témat	téma	k1gNnPc2	téma
jsou	být	k5eAaImIp3nP	být
roztroušeny	roztrousit	k5eAaPmNgInP	roztrousit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
odborných	odborný	k2eAgInPc6d1	odborný
článcích	článek	k1gInPc6	článek
a	a	k8xC	a
desetitisících	desetitisíce	k1gInPc6	desetitisíce
dopisů	dopis	k1gInPc2	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
notářem	notář	k1gMnSc7	notář
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
profesorem	profesor	k1gMnSc7	profesor
morálky	morálka	k1gFnSc2	morálka
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
brzkém	brzký	k2eAgInSc6d1	brzký
věku	věk	k1gInSc6	věk
ovládal	ovládat	k5eAaImAgMnS	ovládat
latinu	latina	k1gFnSc4	latina
a	a	k8xC	a
řečtinu	řečtina	k1gFnSc4	řečtina
a	a	k8xC	a
četl	číst	k5eAaImAgMnS	číst
antické	antický	k2eAgMnPc4d1	antický
autory	autor	k1gMnPc4	autor
(	(	kIx(	(
<g/>
především	především	k9	především
Platóna	Platón	k1gMnSc4	Platón
a	a	k8xC	a
Aristotela	Aristoteles	k1gMnSc4	Aristoteles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgMnS	studovat
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jakuba	Jakub	k1gMnSc2	Jakub
Thomasia	Thomasius	k1gMnSc2	Thomasius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
získal	získat	k5eAaPmAgInS	získat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
O	o	k7c6	o
principu	princip	k1gInSc6	princip
individuace	individuace	k1gFnSc2	individuace
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
De	De	k?	De
principio	principio	k1gNnSc4	principio
individuationis	individuationis	k1gFnSc2	individuationis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Jeně	Jena	k1gFnSc6	Jena
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
matematiky	matematika	k1gFnSc2	matematika
Eduarda	Eduard	k1gMnSc2	Eduard
Weigela	Weigel	k1gMnSc2	Weigel
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
napsal	napsat	k5eAaPmAgMnS	napsat
roku	rok	k1gInSc2	rok
1666	[number]	k4	1666
práci	práce	k1gFnSc4	práce
"	"	kIx"	"
<g/>
Rozprava	rozprava	k1gFnSc1	rozprava
o	o	k7c4	o
umění	umění	k1gNnSc4	umění
kombinatoriky	kombinatorika	k1gFnSc2	kombinatorika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Dissertatio	Dissertatio	k6eAd1	Dissertatio
de	de	k?	de
arte	arte	k1gInSc1	arte
combinatoria	combinatorium	k1gNnSc2	combinatorium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
doktorem	doktor	k1gMnSc7	doktor
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Altdorfu	Altdorf	k1gInSc6	Altdorf
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
téže	týž	k3xTgFnSc6	týž
univerzitě	univerzita	k1gFnSc6	univerzita
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
nabízenou	nabízený	k2eAgFnSc4d1	nabízená
profesuru	profesura	k1gFnSc4	profesura
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
věnovat	věnovat	k5eAaPmF	věnovat
více	hodně	k6eAd2	hodně
činnostem	činnost	k1gFnPc3	činnost
a	a	k8xC	a
profesura	profesura	k1gFnSc1	profesura
by	by	kYmCp3nS	by
ho	on	k3xPp3gNnSc4	on
omezovala	omezovat	k5eAaImAgFnS	omezovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
vykonával	vykonávat	k5eAaImAgInS	vykonávat
diplomatické	diplomatický	k2eAgFnPc4d1	diplomatická
a	a	k8xC	a
knihovnické	knihovnický	k2eAgFnPc4d1	knihovnická
služby	služba	k1gFnPc4	služba
u	u	k7c2	u
mohučského	mohučský	k2eAgMnSc2d1	mohučský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c6	na
hannoverském	hannoverský	k2eAgInSc6d1	hannoverský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
povolání	povolání	k1gNnSc1	povolání
mu	on	k3xPp3gMnSc3	on
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
hojně	hojně	k6eAd1	hojně
cestovat	cestovat	k5eAaImF	cestovat
a	a	k8xC	a
seznamovat	seznamovat	k5eAaImF	seznamovat
se	se	k3xPyFc4	se
s	s	k7c7	s
díly	díl	k1gInPc7	díl
dalších	další	k2eAgMnPc2d1	další
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
revizi	revize	k1gFnSc3	revize
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1672	[number]	k4	1672
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
matematiky	matematik	k1gMnPc7	matematik
a	a	k8xC	a
fyziky	fyzik	k1gMnPc7	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Matematice	matematika	k1gFnSc3	matematika
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
také	také	k6eAd1	také
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
věnoval	věnovat	k5eAaImAgInS	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1676	[number]	k4	1676
objevil	objevit	k5eAaPmAgInS	objevit
diferenciální	diferenciální	k2eAgInSc1d1	diferenciální
počet	počet	k1gInSc1	počet
současně	současně	k6eAd1	současně
s	s	k7c7	s
Isaacem	Isaace	k1gMnSc7	Isaace
Newtonem	Newton	k1gMnSc7	Newton
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
knihovníka	knihovník	k1gMnSc2	knihovník
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
u	u	k7c2	u
vévody	vévoda	k1gMnSc2	vévoda
Johanna	Johann	k1gMnSc2	Johann
Friedricha	Friedrich	k1gMnSc2	Friedrich
z	z	k7c2	z
Brunšviku-Lüneburku	Brunšviku-Lüneburk	k1gInSc2	Brunšviku-Lüneburk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
psal	psát	k5eAaImAgMnS	psát
další	další	k2eAgFnPc4d1	další
matematické	matematický	k2eAgFnPc4d1	matematická
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pokus	pokus	k1gInSc1	pokus
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
kvůli	kvůli	k7c3	kvůli
nesourodosti	nesourodost	k1gFnSc3	nesourodost
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
první	první	k4xOgFnSc2	první
německé	německý	k2eAgFnSc2d1	německá
společnosti	společnost	k1gFnSc2	společnost
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1701	[number]	k4	1701
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Královskou	královský	k2eAgFnSc4d1	královská
pruskou	pruský	k2eAgFnSc4d1	pruská
akademii	akademie	k1gFnSc4	akademie
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
Königlich-Preußische	Königlich-Preußische	k1gFnSc1	Königlich-Preußische
Akademie	akademie	k1gFnSc2	akademie
der	drát	k5eAaImRp2nS	drát
Wissenschaften	Wissenschaftno	k1gNnPc2	Wissenschaftno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hannoveru	Hannover	k1gInSc6	Hannover
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1716	[number]	k4	1716
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
kostele	kostel	k1gInSc6	kostel
'	'	kIx"	'
<g/>
Neustädter	Neustädter	k1gMnSc1	Neustädter
Hof-	Hof-	k1gMnSc1	Hof-
und	und	k?	und
Stadtkirche	Stadtkirche	k1gNnPc2	Stadtkirche
St.	st.	kA	st.
Johannis	Johannis	k1gFnSc2	Johannis
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
psaní	psaní	k1gNnSc1	psaní
filozofických	filozofický	k2eAgNnPc2d1	filozofické
a	a	k8xC	a
matematických	matematický	k2eAgNnPc2d1	matematické
pojednání	pojednání	k1gNnPc2	pojednání
(	(	kIx(	(
<g/>
především	především	k9	především
latinsky	latinsky	k6eAd1	latinsky
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
publikoval	publikovat	k5eAaBmAgMnS	publikovat
mnoho	mnoho	k4c4	mnoho
článků	článek	k1gInPc2	článek
a	a	k8xC	a
napsal	napsat	k5eAaBmAgInS	napsat
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
dopisů	dopis	k1gInPc2	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
pojem	pojem	k1gInSc1	pojem
jeho	jeho	k3xOp3gFnSc2	jeho
filosofie	filosofie	k1gFnSc2	filosofie
je	být	k5eAaImIp3nS	být
monáda	monáda	k1gFnSc1	monáda
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Reného	René	k1gMnSc4	René
Descarta	Descart	k1gMnSc4	Descart
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
soudil	soudit	k5eAaImAgMnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
přírodní	přírodní	k2eAgInPc4d1	přírodní
jevy	jev	k1gInPc4	jev
lze	lze	k6eAd1	lze
vyložit	vyložit	k5eAaPmF	vyložit
pojmy	pojem	k1gInPc4	pojem
rozlehlosti	rozlehlost	k1gFnSc2	rozlehlost
a	a	k8xC	a
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
že	že	k8xS	že
základem	základ	k1gInSc7	základ
světa	svět	k1gInSc2	svět
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
substance	substance	k1gFnPc1	substance
–	–	k?	–
materiální	materiální	k2eAgNnSc4d1	materiální
a	a	k8xC	a
duchovní	duchovní	k2eAgNnSc4d1	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pohyb	pohyb	k1gInSc1	pohyb
něco	něco	k3yInSc4	něco
čistě	čistě	k6eAd1	čistě
relativního	relativní	k2eAgMnSc4d1	relativní
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
stanovisku	stanovisko	k1gNnSc6	stanovisko
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
těleso	těleso	k1gNnSc1	těleso
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
a	a	k8xC	a
které	který	k3yQgNnSc1	který
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Descartovo	Descartův	k2eAgNnSc1d1	Descartovo
pojetí	pojetí	k1gNnSc1	pojetí
substancí	substance	k1gFnPc2	substance
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
ještě	ještě	k9	ještě
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
ohledu	ohled	k1gInSc6	ohled
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kontinuity	kontinuita	k1gFnSc2	kontinuita
a	a	k8xC	a
dělitelnosti	dělitelnost	k1gFnSc2	dělitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Matematický	matematický	k2eAgInSc1d1	matematický
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
kontinuum	kontinuum	k1gNnSc4	kontinuum
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nekonečně	konečně	k6eNd1	konečně
dělitelný	dělitelný	k2eAgInSc1d1	dělitelný
<g/>
.	.	kIx.	.
</s>
<s>
Kontinuum	kontinuum	k1gNnSc1	kontinuum
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
matematiky	matematika	k1gFnSc2	matematika
je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgFnSc1d1	ideální
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
skutečné	skutečný	k2eAgFnPc4d1	skutečná
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
látka	látka	k1gFnSc1	látka
není	být	k5eNaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
pouhou	pouhý	k2eAgFnSc7d1	pouhá
rozlehlostí	rozlehlost	k1gFnSc7	rozlehlost
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
může	moct	k5eAaImIp3nS	moct
sestávat	sestávat	k5eAaImF	sestávat
jen	jen	k9	jen
z	z	k7c2	z
pravých	pravý	k2eAgFnPc2d1	pravá
částí	část	k1gFnPc2	část
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
libovolně	libovolně	k6eAd1	libovolně
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
hmoty	hmota	k1gFnSc2	hmota
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgFnSc6d1	podobná
staré	starý	k2eAgFnSc6d1	stará
teorii	teorie	k1gFnSc6	teorie
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
atomisté	atomista	k1gMnPc1	atomista
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
mechanický	mechanický	k2eAgInSc1d1	mechanický
pojem	pojem	k1gInSc1	pojem
atomu	atom	k1gInSc2	atom
s	s	k7c7	s
aristotelovským	aristotelovský	k2eAgInSc7d1	aristotelovský
pojmem	pojem	k1gInSc7	pojem
entelechie	entelechie	k1gFnSc2	entelechie
<g/>
,	,	kIx,	,
oduševňující	oduševňující	k2eAgFnSc2d1	oduševňující
a	a	k8xC	a
formující	formující	k2eAgFnSc2d1	formující
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
dospívá	dospívat	k5eAaImIp3nS	dospívat
tak	tak	k9	tak
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
pojmu	pojem	k1gInSc3	pojem
monády	monáda	k1gFnSc2	monáda
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgInSc1d1	vlastní
výraz	výraz	k1gInSc1	výraz
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc1	význam
"	"	kIx"	"
<g/>
jednota	jednota	k1gFnSc1	jednota
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
od	od	k7c2	od
Giordana	Giordan	k1gMnSc2	Giordan
Bruna	Bruno	k1gMnSc2	Bruno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Monády	monáda	k1gFnPc1	monáda
jsou	být	k5eAaImIp3nP	být
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastní	vlastní	k2eAgInSc4d1	vlastní
prazáklad	prazáklad	k1gInSc4	prazáklad
jsoucna	jsoucno	k1gNnSc2	jsoucno
jsou	být	k5eAaImIp3nP	být
bodové	bodový	k2eAgFnPc1d1	bodová
substance	substance	k1gFnPc1	substance
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
základ	základ	k1gInSc1	základ
tedy	tedy	k9	tedy
není	být	k5eNaImIp3nS	být
kontinuum	kontinuum	k1gNnSc4	kontinuum
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
smyslovou	smyslový	k2eAgFnSc7d1	smyslová
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
,	,	kIx,	,
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
zdají	zdát	k5eAaPmIp3nP	zdát
jako	jako	k8xC	jako
rozlehlé	rozlehlý	k2eAgNnSc4d1	rozlehlé
kontinuum	kontinuum	k1gNnSc4	kontinuum
<g/>
.	.	kIx.	.
</s>
<s>
Leibniz	Leibniz	k1gMnSc1	Leibniz
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
smyslový	smyslový	k2eAgInSc1d1	smyslový
dojem	dojem	k1gInSc1	dojem
klame	klamat	k5eAaImIp3nS	klamat
<g/>
.	.	kIx.	.
</s>
<s>
Monády	monáda	k1gFnPc1	monáda
jsou	být	k5eAaImIp3nP	být
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
silová	silový	k2eAgNnPc1d1	silové
centra	centrum	k1gNnPc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
komplex	komplex	k1gInSc4	komplex
bodových	bodový	k2eAgNnPc2d1	bodové
silových	silový	k2eAgNnPc2d1	silové
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Monády	monáda	k1gFnPc1	monáda
jsou	být	k5eAaImIp3nP	být
duše	duše	k1gFnPc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Bodové	bodový	k2eAgFnPc1d1	bodová
monády	monáda	k1gFnPc1	monáda
jsou	být	k5eAaImIp3nP	být
oduševnělé	oduševnělý	k2eAgFnPc1d1	oduševnělá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
ve	v	k7c6	v
schopnosti	schopnost	k1gFnSc6	schopnost
představ	představa	k1gFnPc2	představa
–	–	k?	–
percepce	percepce	k1gFnSc2	percepce
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnPc1d3	nejnižší
monády	monáda	k1gFnPc1	monáda
jsou	být	k5eAaImIp3nP	být
jakoby	jakoby	k8xS	jakoby
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
snu	sen	k1gInSc2	sen
či	či	k8xC	či
omámení	omámení	k1gNnSc2	omámení
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
temné	temný	k2eAgFnPc1d1	temná
<g/>
,	,	kIx,	,
nevědomé	vědomý	k2eNgFnPc1d1	nevědomá
představy	představa	k1gFnPc1	představa
<g/>
.	.	kIx.	.
</s>
<s>
Prostřední	prostřední	k2eAgFnPc1d1	prostřední
monády	monáda	k1gFnPc1	monáda
mají	mít	k5eAaImIp3nP	mít
jakési	jakýsi	k3yIgNnSc1	jakýsi
mlhavé	mlhavý	k2eAgFnPc1d1	mlhavá
představy	představa	k1gFnPc1	představa
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
odlišit	odlišit	k5eAaPmF	odlišit
své	svůj	k3xOyFgFnPc4	svůj
představy	představa	k1gFnPc4	představa
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
ne	ne	k9	ne
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
představy	představa	k1gFnPc4	představa
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
monády	monáda	k1gFnPc1	monáda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
lidská	lidský	k2eAgFnSc1d1	lidská
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vědomí	vědomí	k1gNnSc1	vědomí
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc1	schopnost
percepce	percepce	k1gFnSc1	percepce
a	a	k8xC	a
apercepce	apercepce	k1gFnSc1	apercepce
–	–	k?	–
sebeuvědomění	sebeuvědomění	k1gNnSc1	sebeuvědomění
<g/>
,	,	kIx,	,
reflexe	reflexe	k1gFnPc1	reflexe
apod.	apod.	kA	apod.
A	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
monáda	monáda	k1gFnSc1	monáda
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vševědoucí	vševědoucí	k2eAgFnSc1d1	vševědoucí
<g/>
.	.	kIx.	.
</s>
<s>
Monády	monáda	k1gFnPc1	monáda
jsou	být	k5eAaImIp3nP	být
individua	individuum	k1gNnPc1	individuum
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
stejné	stejný	k2eAgFnPc1d1	stejná
monády	monáda	k1gFnPc1	monáda
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
monáda	monáda	k1gFnSc1	monáda
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
nejjednodušší	jednoduchý	k2eAgFnSc2d3	nejjednodušší
k	k	k7c3	k
nejsložitější	složitý	k2eAgFnSc3d3	nejsložitější
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
nezaměnitelné	zaměnitelný	k2eNgNnSc4d1	nezaměnitelné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
zrcadlí	zrcadlit	k5eAaImIp3nS	zrcadlit
univerzum	univerzum	k1gNnSc4	univerzum
svým	svůj	k3xOyFgNnSc7	svůj
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
<g/>
,	,	kIx,	,
jedinečným	jedinečný	k2eAgInSc7d1	jedinečný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
potenciálně	potenciálně	k6eAd1	potenciálně
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
do	do	k7c2	do
možnosti	možnost	k1gFnSc2	možnost
<g/>
,	,	kIx,	,
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
univerza	univerzum	k1gNnSc2	univerzum
<g/>
.	.	kIx.	.
</s>
<s>
Monády	monáda	k1gFnPc1	monáda
jsou	být	k5eAaImIp3nP	být
individua	individuum	k1gNnPc1	individuum
také	také	k9	také
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
navenek	navenek	k6eAd1	navenek
uzavřeny	uzavřen	k2eAgInPc1d1	uzavřen
–	–	k?	–
nemají	mít	k5eNaImIp3nP	mít
žádná	žádný	k3yNgNnPc4	žádný
"	"	kIx"	"
<g/>
okna	okno	k1gNnPc4	okno
<g/>
"	"	kIx"	"
–	–	k?	–
neovlivňují	ovlivňovat	k5eNaImIp3nP	ovlivňovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
předzjednané	předzjednaný	k2eAgFnSc2d1	předzjednaná
harmonie	harmonie	k1gFnSc2	harmonie
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
u	u	k7c2	u
Descarta	Descart	k1gMnSc2	Descart
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
uznával	uznávat	k5eAaImAgMnS	uznávat
dvě	dva	k4xCgFnPc4	dva
substance	substance	k1gFnPc4	substance
(	(	kIx(	(
<g/>
myšlení	myšlení	k1gNnSc1	myšlení
a	a	k8xC	a
rozprostraněnost	rozprostraněnost	k1gFnSc1	rozprostraněnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Leibnize	Leibnize	k1gFnSc2	Leibnize
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc7d3	nejmenší
ontologickou	ontologický	k2eAgFnSc7d1	ontologická
jednotkou	jednotka	k1gFnSc7	jednotka
monáda	monáda	k1gFnSc1	monáda
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Složeniny	složenina	k1gFnPc1	složenina
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
monád	monáda	k1gFnPc2	monáda
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nP	tvořit
harmonický	harmonický	k2eAgInSc4d1	harmonický
celek	celek	k1gInSc4	celek
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
každá	každý	k3xTgFnSc1	každý
zvlášť	zvlášť	k6eAd1	zvlášť
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
svět	svět	k1gInSc4	svět
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
Navzájem	navzájem	k6eAd1	navzájem
se	se	k3xPyFc4	se
nepřeměňují	přeměňovat	k5eNaImIp3nP	přeměňovat
<g/>
,	,	kIx,	,
nespojují	spojovat	k5eNaImIp3nP	spojovat
a	a	k8xC	a
neovlivňují	ovlivňovat	k5eNaImIp3nP	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Monády	monáda	k1gFnPc4	monáda
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
navzájem	navzájem	k6eAd1	navzájem
percipují	percipovat	k5eAaImIp3nP	percipovat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
nedochází	docházet	k5eNaImIp3nS	docházet
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Zrcadlí	zrcadlit	k5eAaImIp3nS	zrcadlit
celý	celý	k2eAgInSc4d1	celý
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgMnPc4	všechen
fungují	fungovat	k5eAaImIp3nP	fungovat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k6eAd1	jak
mají	mít	k5eAaImIp3nP	mít
<g/>
?	?	kIx.	?
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
otázku	otázka	k1gFnSc4	otázka
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
předzjednané	předzjednaný	k2eAgFnSc6d1	předzjednaná
harmonii	harmonie	k1gFnSc6	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
stvořil	stvořit	k5eAaPmAgMnS	stvořit
všechny	všechen	k3xTgFnPc4	všechen
monády	monáda	k1gFnPc4	monáda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jejich	jejich	k3xOp3gFnPc1	jejich
percepce	percepce	k1gFnPc1	percepce
vzájemně	vzájemně	k6eAd1	vzájemně
souhlasily	souhlasit	k5eAaImAgFnP	souhlasit
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
vše	všechen	k3xTgNnSc1	všechen
fungovalo	fungovat	k5eAaImAgNnS	fungovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
monádách	monáda	k1gFnPc6	monáda
je	být	k5eAaImIp3nS	být
dáno	dán	k2eAgNnSc1d1	dáno
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
dít	dít	k5eAaImF	dít
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
živý	živý	k2eAgInSc4d1	živý
organismus	organismus	k1gInSc4	organismus
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ovládán	ovládat	k5eAaImNgInS	ovládat
ústřední	ústřední	k2eAgFnSc7d1	ústřední
monádou	monáda	k1gFnSc7	monáda
(	(	kIx(	(
<g/>
entelechií	entelechie	k1gFnSc7	entelechie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
duší	duše	k1gFnSc7	duše
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
materiální	materiální	k2eAgFnSc4d1	materiální
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
tedy	tedy	k9	tedy
podléhá	podléhat	k5eAaImIp3nS	podléhat
působícím	působící	k2eAgFnPc3d1	působící
příčinám	příčina	k1gFnPc3	příčina
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
duše	duše	k1gFnSc1	duše
je	být	k5eAaImIp3nS	být
účelovou	účelový	k2eAgFnSc7d1	účelová
příčinou	příčina	k1gFnSc7	příčina
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Si	se	k3xPyFc3	se
Deus	Deus	k1gInSc4	Deus
est	est	k?	est
<g/>
,	,	kIx,	,
unde	unde	k6eAd1	unde
malum	malum	k1gInSc1	malum
<g/>
?	?	kIx.	?
</s>
<s>
Si	se	k3xPyFc3	se
non	non	k?	non
est	est	k?	est
<g/>
,	,	kIx,	,
unde	unde	k6eAd1	unde
bonum	bonum	k1gInSc1	bonum
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
zlo	zlo	k1gNnSc4	zlo
<g/>
?	?	kIx.	?
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
dobro	dobro	k1gNnSc4	dobro
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
stvořil	stvořit	k5eAaPmAgMnS	stvořit
náš	náš	k3xOp1gInSc4	náš
svět	svět	k1gInSc4	svět
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
možných	možný	k2eAgInPc2d1	možný
světů	svět	k1gInPc2	svět
<g/>
,	,	kIx,	,
plyne	plynout	k5eAaImIp3nS	plynout
to	ten	k3xDgNnSc1	ten
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
představy	představa	k1gFnSc2	představa
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
potom	potom	k6eAd1	potom
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
nejdokonalejším	dokonalý	k2eAgMnSc6d3	nejdokonalejší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
možných	možný	k2eAgInPc2d1	možný
světů	svět	k1gInPc2	svět
je	být	k5eAaImIp3nS	být
tolik	tolik	k6eAd1	tolik
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
,	,	kIx,	,
nedokonalosti	nedokonalost	k1gFnSc2	nedokonalost
a	a	k8xC	a
hříchu	hřích	k1gInSc2	hřích
<g/>
?	?	kIx.	?
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
otázku	otázka	k1gFnSc4	otázka
si	se	k3xPyFc3	se
položili	položit	k5eAaPmAgMnP	položit
již	již	k9	již
staří	starý	k2eAgMnPc1d1	starý
Sumerové	Sumer	k1gMnPc1	Sumer
a	a	k8xC	a
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
problém	problém	k1gInSc1	problém
znovu	znovu	k6eAd1	znovu
vyvstává	vyvstávat	k5eAaImIp3nS	vyvstávat
u	u	k7c2	u
Epikura	Epikur	k1gMnSc2	Epikur
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Hume	Hum	k1gInSc2	Hum
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Dialogues	Dialogues	k1gMnSc1	Dialogues
Concerning	Concerning	k1gInSc1	Concerning
Natural	Natural	k?	Natural
Religion	religion	k1gInSc1	religion
uvádí	uvádět	k5eAaImIp3nS	uvádět
tzv.	tzv.	kA	tzv.
Epikurův	Epikurův	k2eAgInSc1d1	Epikurův
paradox	paradox	k1gInSc1	paradox
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Epicurus	Epicurus	k1gInSc1	Epicurus
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
old	old	k?	old
questions	questions	k1gInSc1	questions
are	ar	k1gInSc5	ar
yet	yet	k?	yet
unanswered	unanswered	k1gInSc1	unanswered
<g/>
.	.	kIx.	.
</s>
<s>
Is	Is	k?	Is
he	he	k0	he
willing	willing	k1gInSc1	willing
to	ten	k3xDgNnSc4	ten
prevent	prevent	k1gMnSc1	prevent
evil	evil	k1gMnSc1	evil
<g/>
,	,	kIx,	,
but	but	k?	but
not	nota	k1gFnPc2	nota
able	able	k6eAd1	able
<g/>
?	?	kIx.	?
</s>
<s>
then	then	k1gInSc1	then
is	is	k?	is
he	he	k0	he
impotent	impotent	k1gMnSc1	impotent
<g/>
.	.	kIx.	.
</s>
<s>
Is	Is	k?	Is
he	he	k0	he
able	ablus	k1gMnSc5	ablus
<g/>
,	,	kIx,	,
but	but	k?	but
not	nota	k1gFnPc2	nota
willing	willing	k1gInSc1	willing
<g/>
?	?	kIx.	?
</s>
<s>
then	then	k1gInSc1	then
is	is	k?	is
he	he	k0	he
malevolent	malevolent	k1gInSc1	malevolent
<g/>
.	.	kIx.	.
</s>
<s>
Is	Is	k?	Is
he	he	k0	he
both	both	k1gInSc1	both
able	abl	k1gInPc1	abl
and	and	k?	and
willing	willing	k1gInSc1	willing
<g/>
?	?	kIx.	?
</s>
<s>
whence	whenka	k1gFnSc3	whenka
then	then	k1gMnSc1	then
is	is	k?	is
evil	evil	k1gMnSc1	evil
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Hume	Hume	k1gFnSc1	Hume
<g/>
,	,	kIx,	,
Dialogues	Dialogues	k1gInSc1	Dialogues
Concerning	Concerning	k1gInSc1	Concerning
Natural	Natural	k?	Natural
Religion	religion	k1gInSc1	religion
<g/>
.	.	kIx.	.
1779	[number]	k4	1779
<g/>
)	)	kIx)	)
Volně	volně	k6eAd1	volně
lze	lze	k6eAd1	lze
tento	tento	k3xDgInSc1	tento
paradox	paradox	k1gInSc1	paradox
přeložit	přeložit	k5eAaPmF	přeložit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Epikurovy	Epikurův	k2eAgFnPc1d1	Epikurova
otázky	otázka	k1gFnPc1	otázka
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
nezodpovězeny	zodpovědět	k5eNaPmNgInP	zodpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
(	(	kIx(	(
<g/>
Bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
předejít	předejít	k5eAaPmF	předejít
zlu	zlo	k1gNnSc3	zlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemůže	moct	k5eNaImIp3nS	moct
<g/>
?	?	kIx.	?
</s>
<s>
Potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
bezmocný	bezmocný	k2eAgMnSc1d1	bezmocný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
?	?	kIx.	?
</s>
<s>
Potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
zlovolný	zlovolný	k2eAgInSc1d1	zlovolný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
<g/>
?	?	kIx.	?
</s>
<s>
Odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
bere	brát	k5eAaImIp3nS	brát
zlo	zlo	k1gNnSc4	zlo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Tuto	tento	k3xDgFnSc4	tento
otázku	otázka	k1gFnSc4	otázka
hlouběji	hluboko	k6eAd2	hluboko
rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
až	až	k9	až
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
zla	zlo	k1gNnSc2	zlo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zlo	zlo	k1gNnSc4	zlo
lze	lze	k6eAd1	lze
chápat	chápat	k5eAaImF	chápat
metafyzicky	metafyzicky	k6eAd1	metafyzicky
<g/>
,	,	kIx,	,
fyzicky	fyzicky	k6eAd1	fyzicky
a	a	k8xC	a
morálně	morálně	k6eAd1	morálně
<g/>
.	.	kIx.	.
</s>
<s>
Metafyzické	metafyzický	k2eAgNnSc1d1	metafyzické
zlo	zlo	k1gNnSc1	zlo
záleží	záležet	k5eAaImIp3nS	záležet
v	v	k7c6	v
pouhé	pouhý	k2eAgFnSc6d1	pouhá
nedokonalosti	nedokonalost	k1gFnSc6	nedokonalost
<g/>
,	,	kIx,	,
fyzické	fyzický	k2eAgNnSc4d1	fyzické
zlo	zlo	k1gNnSc4	zlo
v	v	k7c4	v
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
morální	morální	k2eAgNnSc4d1	morální
zlo	zlo	k1gNnSc4	zlo
v	v	k7c6	v
hříchu	hřích	k1gInSc6	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Fyzické	fyzický	k2eAgNnSc4d1	fyzické
zlo	zlo	k1gNnSc4	zlo
a	a	k8xC	a
morální	morální	k2eAgNnSc4d1	morální
zlo	zlo	k1gNnSc4	zlo
sice	sice	k8xC	sice
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
věčných	věčný	k2eAgFnPc2d1	věčná
pravd	pravda	k1gFnPc2	pravda
možné	možný	k2eAgFnPc1d1	možná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Leibniz	Leibniz	k1gMnSc1	Leibniz
<g/>
:	:	kIx,	:
Theodicea	Theodicea	k1gMnSc1	Theodicea
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Fyzické	fyzický	k2eAgNnSc1d1	fyzické
zlo	zlo	k1gNnSc1	zlo
(	(	kIx(	(
<g/>
bolest	bolest	k1gFnSc1	bolest
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
zla	zlo	k1gNnSc2	zlo
metafyzického	metafyzický	k2eAgNnSc2d1	metafyzické
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
stvořené	stvořený	k2eAgFnSc3d1	stvořená
bytosti	bytost	k1gFnSc3	bytost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nedokonalí	dokonalit	k5eNaImIp3nS	dokonalit
(	(	kIx(	(
<g/>
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
Bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
pocity	pocit	k1gInPc1	pocit
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
též	též	k9	též
nedokonalé	dokonalý	k2eNgNnSc4d1	nedokonalé
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
zlo	zlo	k1gNnSc4	zlo
morální	morální	k2eAgNnSc4d1	morální
<g/>
.	.	kIx.	.
</s>
<s>
Nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
bytost	bytost	k1gFnSc1	bytost
chybuje	chybovat	k5eAaImIp3nS	chybovat
a	a	k8xC	a
hřeší	hřešit	k5eAaImIp3nS	hřešit
<g/>
.	.	kIx.	.
</s>
<s>
Leibniz	Leibniz	k1gMnSc1	Leibniz
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c6	o
převedení	převedení	k1gNnSc6	převedení
veškerého	veškerý	k3xTgNnSc2	veškerý
poznání	poznání	k1gNnSc2	poznání
na	na	k7c4	na
jednoznačné	jednoznačný	k2eAgInPc4d1	jednoznačný
pojmy	pojem	k1gInPc4	pojem
(	(	kIx(	(
<g/>
žádná	žádný	k3yNgFnSc1	žádný
mnohoznačnost	mnohoznačnost	k1gFnSc1	mnohoznačnost
<g/>
,	,	kIx,	,
nejasnost	nejasnost	k1gFnSc1	nejasnost
<g/>
,	,	kIx,	,
vágnost	vágnost	k1gFnSc1	vágnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
vyjadření	vyjadření	k1gNnSc1	vyjadření
těchto	tento	k3xDgInPc2	tento
pojmů	pojem	k1gInPc2	pojem
pomocí	pomocí	k7c2	pomocí
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
characteristica	characteristic	k2eAgFnSc1d1	characteristica
universalis	universalis	k1gFnSc1	universalis
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
univerzální	univerzální	k2eAgFnSc1d1	univerzální
charakteristika	charakteristika	k1gFnSc1	charakteristika
<g/>
,	,	kIx,	,
univerzální	univerzální	k2eAgFnSc1d1	univerzální
symbolická	symbolický	k2eAgFnSc1d1	symbolická
řeč	řeč	k1gFnSc1	řeč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
už	už	k9	už
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
dospěl	dochvít	k5eAaPmAgMnS	dochvít
Leibniz	Leibniz	k1gMnSc1	Leibniz
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytvoření	vytvoření	k1gNnSc4	vytvoření
jednotného	jednotný	k2eAgInSc2d1	jednotný
a	a	k8xC	a
jasného	jasný	k2eAgInSc2d1	jasný
univerzálního	univerzální	k2eAgInSc2d1	univerzální
systému	systém	k1gInSc2	systém
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Leibniz	Leibniz	k1gMnSc1	Leibniz
rozlišoval	rozlišovat	k5eAaImAgMnS	rozlišovat
mezi	mezi	k7c7	mezi
pravdami	pravda	k1gFnPc7	pravda
faktu	fakt	k1gInSc2	fakt
(	(	kIx(	(
<g/>
vérités	vérités	k1gInSc1	vérités
de	de	k?	de
fait	fait	k1gInSc1	fait
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravdami	pravda	k1gFnPc7	pravda
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
vérités	vérités	k1gInSc1	vérités
de	de	k?	de
raison	raison	k1gInSc1	raison
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravdy	pravda	k1gFnPc1	pravda
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
nahodilé	nahodilý	k2eAgFnSc2d1	nahodilá
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
možných	možný	k2eAgInPc6d1	možný
světech	svět	k1gInPc6	svět
(	(	kIx(	(
<g/>
předchůdci	předchůdce	k1gMnPc1	předchůdce
Kantových	Kantových	k2eAgInPc2d1	Kantových
syntetických	syntetický	k2eAgInPc2d1	syntetický
soudů	soud	k1gInPc2	soud
nebo	nebo	k8xC	nebo
Hobbesova	Hobbesův	k2eAgNnSc2d1	Hobbesovo
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kdežto	kdežto	k8xS	kdežto
pravdy	pravda	k1gFnPc1	pravda
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
nutné	nutný	k2eAgFnPc1d1	nutná
pravdy	pravda	k1gFnPc1	pravda
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
možných	možný	k2eAgInPc6d1	možný
světech	svět	k1gInPc6	svět
(	(	kIx(	(
<g/>
předchůdci	předchůdce	k1gMnPc1	předchůdce
Kantových	Kantových	k2eAgInPc2d1	Kantových
analytických	analytický	k2eAgInPc2d1	analytický
soudů	soud	k1gInPc2	soud
nebo	nebo	k8xC	nebo
Hobbesova	Hobbesův	k2eAgNnSc2d1	Hobbesovo
odčítání	odčítání	k1gNnSc2	odčítání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
identity	identita	k1gFnSc2	identita
nerozlišeného	rozlišený	k2eNgInSc2d1	nerozlišený
(	(	kIx(	(
<g/>
identitas	identitasit	k5eAaPmRp2nS	identitasit
indiscernibilium	indiscernibilium	k1gNnSc4	indiscernibilium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Leibnizův	Leibnizův	k2eAgInSc1d1	Leibnizův
princip	princip	k1gInSc1	princip
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
metafyzické	metafyzický	k2eAgInPc1d1	metafyzický
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
esenciální	esenciální	k2eAgInPc1d1	esenciální
<g/>
.	.	kIx.	.
</s>
<s>
Nemohou	moct	k5eNaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
dvě	dva	k4xCgFnPc4	dva
nerozlišitelné	rozlišitelný	k2eNgFnPc4d1	nerozlišitelná
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
jen	jen	k9	jen
individua	individuum	k1gNnPc1	individuum
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
obecné	obecný	k2eAgInPc4d1	obecný
pojmy	pojem	k1gInPc4	pojem
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	on	k3xPp3gMnPc4	on
známe	znát	k5eAaImIp1nP	znát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
abstraktní	abstraktní	k2eAgFnPc1d1	abstraktní
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
Leibniz	Leibniz	k1gInSc1	Leibniz
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
čas	čas	k1gInSc4	čas
jsou	být	k5eAaImIp3nP	být
relativní	relativní	k2eAgNnSc4d1	relativní
(	(	kIx(	(
<g/>
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
Newtonem	newton	k1gInSc7	newton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
díle	dílo	k1gNnSc6	dílo
nacházíme	nacházet	k5eAaImIp1nP	nacházet
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
rozporů	rozpor	k1gInPc2	rozpor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dokumentují	dokumentovat	k5eAaBmIp3nP	dokumentovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vyvíjelo	vyvíjet	k5eAaImAgNnS	vyvíjet
a	a	k8xC	a
měnilo	měnit	k5eAaImAgNnS	měnit
myšlení	myšlení	k1gNnSc1	myšlení
tohoto	tento	k3xDgInSc2	tento
všestranného	všestranný	k2eAgInSc2d1	všestranný
a	a	k8xC	a
neobyčejně	obyčejně	k6eNd1	obyčejně
plodného	plodný	k2eAgMnSc2d1	plodný
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
1663	[number]	k4	1663
<g/>
:	:	kIx,	:
Disputatio	Disputatio	k6eAd1	Disputatio
De	De	k?	De
Principio	Principio	k1gMnSc1	Principio
Individui	individuum	k1gNnPc7	individuum
1666	[number]	k4	1666
<g/>
:	:	kIx,	:
De	De	k?	De
Arte	Arte	k1gFnSc7	Arte
Combinatoria	Combinatorium	k1gNnSc2	Combinatorium
1667	[number]	k4	1667
<g/>
:	:	kIx,	:
Nova	nova	k1gFnSc1	nova
methodus	methodus	k1gInSc1	methodus
discendae	discendae	k6eAd1	discendae
docendaeque	docendaequat	k5eAaPmIp3nS	docendaequat
jurisprudentiae	jurisprudentiae	k6eAd1	jurisprudentiae
1671	[number]	k4	1671
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Hypothesis	Hypothesis	k1gFnSc3	Hypothesis
Physica	Physic	k2eAgFnSc1d1	Physica
Nova	nova	k1gFnSc1	nova
1673	[number]	k4	1673
<g/>
:	:	kIx,	:
Confessio	Confessio	k6eAd1	Confessio
philosophi	philosophi	k6eAd1	philosophi
(	(	kIx(	(
<g/>
Filosofovo	filosofův	k2eAgNnSc1d1	filosofovo
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
)	)	kIx)	)
1684	[number]	k4	1684
<g/>
:	:	kIx,	:
Nova	nova	k1gFnSc1	nova
methodus	methodus	k1gInSc4	methodus
pro	pro	k7c4	pro
maximis	maximis	k1gInSc4	maximis
et	et	k?	et
minimis	minimis	k1gInSc1	minimis
1688	[number]	k4	1688
<g/>
:	:	kIx,	:
Discours	Discours	k1gInSc1	Discours
de	de	k?	de
métaphysique	métaphysique	k1gInSc1	métaphysique
(	(	kIx(	(
<g/>
Metafyzické	metafyzický	k2eAgNnSc1d1	metafyzické
pojednání	pojednání	k1gNnSc1	pojednání
<g/>
)	)	kIx)	)
1695	[number]	k4	1695
<g/>
:	:	kIx,	:
Systè	Systè	k1gFnSc6	Systè
nouveau	nouveau	k5eAaPmIp1nS	nouveau
de	de	k?	de
la	la	k1gNnSc1	la
nature	natur	k1gMnSc5	natur
et	et	k?	et
de	de	k?	de
la	la	k1gNnSc2	la
communication	communication	k1gInSc4	communication
des	des	k1gNnSc2	des
<g />
.	.	kIx.	.
</s>
<s>
substances	substances	k1gInSc1	substances
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
soustava	soustava	k1gFnSc1	soustava
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
komunikace	komunikace	k1gFnSc2	komunikace
substancí	substance	k1gFnSc7	substance
<g/>
)	)	kIx)	)
1703	[number]	k4	1703
<g/>
:	:	kIx,	:
Explication	Explication	k1gInSc1	Explication
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Arithmétique	Arithmétiquus	k1gMnSc5	Arithmétiquus
Binaire	Binair	k1gMnSc5	Binair
1704	[number]	k4	1704
<g/>
:	:	kIx,	:
Nouveaux	Nouveaux	k1gInSc1	Nouveaux
Essais	Essais	k1gFnSc2	Essais
sur	sur	k?	sur
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
entendement	entendement	k1gMnSc1	entendement
humain	humain	k1gMnSc1	humain
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgFnPc1d1	Nové
eseje	esej	k1gFnPc1	esej
o	o	k7c6	o
lidském	lidský	k2eAgInSc6d1	lidský
rozumu	rozum	k1gInSc6	rozum
<g/>
)	)	kIx)	)
1707	[number]	k4	1707
–	–	k?	–
1711	[number]	k4	1711
<g/>
:	:	kIx,	:
Scriptores	Scriptores	k1gInSc1	Scriptores
rerum	rerum	k1gInSc1	rerum
<g />
.	.	kIx.	.
</s>
<s>
Brunsvicensium	Brunsvicensium	k1gNnSc1	Brunsvicensium
<g/>
,	,	kIx,	,
3	[number]	k4	3
sv.	sv.	kA	sv.
(	(	kIx(	(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
pramenů	pramen	k1gInPc2	pramen
k	k	k7c3	k
velfským	velfský	k2eAgFnPc3d1	velfský
a	a	k8xC	a
dolnosaským	dolnosaský	k2eAgFnPc3d1	Dolnosaská
dějinám	dějiny	k1gFnPc3	dějiny
<g/>
)	)	kIx)	)
1710	[number]	k4	1710
<g/>
:	:	kIx,	:
Essais	Essais	k1gFnSc1	Essais
de	de	k?	de
théodicée	théodicéat	k5eAaPmIp3nS	théodicéat
ou	ou	k0	ou
sur	sur	k?	sur
la	la	k1gNnSc7	la
bonté	bontý	k2eAgFnSc2d1	bontý
de	de	k?	de
Dieu	Dieus	k1gInSc2	Dieus
<g/>
,	,	kIx,	,
la	la	k1gNnSc2	la
liberté	libertý	k2eAgFnSc2d1	libertý
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
homme	hommat	k5eAaPmIp3nS	hommat
et	et	k?	et
l	l	kA	l
<g/>
'	'	kIx"	'
origine	originout	k5eAaPmIp3nS	originout
du	du	k?	du
mal	málit	k5eAaImRp2nS	málit
(	(	kIx(	(
<g/>
Theodicea	Theodicea	k1gMnSc1	Theodicea
<g/>
.	.	kIx.	.
</s>
<s>
Teodicea	Teodicea	k6eAd1	Teodicea
neboli	neboli	k8xC	neboli
o	o	k7c6	o
boží	boží	k2eAgFnSc6d1	boží
dobrotě	dobrota	k1gFnSc6	dobrota
<g/>
,	,	kIx,	,
lidské	lidský	k2eAgFnSc3d1	lidská
svobodě	svoboda	k1gFnSc3	svoboda
a	a	k8xC	a
původu	původ	k1gInSc3	původ
zla	zlo	k1gNnSc2	zlo
<g/>
)	)	kIx)	)
1714	[number]	k4	1714
<g/>
:	:	kIx,	:
Principes	Principesa	k1gFnPc2	Principesa
de	de	k?	de
la	la	k1gNnSc4	la
Nature	Natur	k1gMnSc5	Natur
et	et	k?	et
de	de	k?	de
la	la	k1gNnSc2	la
Grâce	Grâce	k1gFnSc2	Grâce
fondés	fondés	k1gInSc1	fondés
en	en	k?	en
Raison	raison	k1gInSc1	raison
(	(	kIx(	(
<g/>
Principy	princip	k1gInPc1	princip
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
milosti	milost	k1gFnSc2	milost
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
rozumu	rozum	k1gInSc6	rozum
<g/>
)	)	kIx)	)
1714	[number]	k4	1714
<g/>
:	:	kIx,	:
La	la	k1gNnSc4	la
Monadologie	Monadologie	k1gFnPc1	Monadologie
(	(	kIx(	(
<g/>
Monadologie	Monadologie	k1gFnPc1	Monadologie
<g/>
)	)	kIx)	)
1759	[number]	k4	1759
(	(	kIx(	(
<g/>
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Protogaea	Protogaea	k1gFnSc1	Protogaea
oder	odra	k1gFnPc2	odra
Abhandlung	Abhandlunga	k1gFnPc2	Abhandlunga
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
ersten	erstno	k1gNnPc2	erstno
Gestalt	Gestalt	k1gInSc1	Gestalt
der	drát	k5eAaImRp2nS	drát
Erde	Erde	k1gInSc1	Erde
und	und	k?	und
den	den	k1gInSc4	den
Spuren	Spurna	k1gFnPc2	Spurna
der	drát	k5eAaImRp2nS	drát
Historie	historie	k1gFnSc2	historie
in	in	k?	in
Denkmalen	Denkmalen	k2eAgInSc1d1	Denkmalen
der	drát	k5eAaImRp2nS	drát
Natur	Natura	k1gFnPc2	Natura
LEIBNIZ	LEIBNIZ	kA	LEIBNIZ
<g/>
,	,	kIx,	,
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
<g/>
.	.	kIx.	.
</s>
<s>
Monadologie	Monadologie	k1gFnPc1	Monadologie
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
práce	práce	k1gFnPc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Husák	Husák	k1gMnSc1	Husák
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
175	[number]	k4	175
s.	s.	k?	s.
LEIBNIZ	LEIBNIZ	kA	LEIBNIZ
<g/>
,	,	kIx,	,
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
soudnosti	soudnost	k1gFnSc6	soudnost
od	od	k7c2	od
auktora	auktor	k1gMnSc2	auktor
systému	systém	k1gInSc2	systém
předzjednané	předzjednaný	k2eAgFnSc2d1	předzjednaná
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Věra	Věra	k1gFnSc1	Věra
Rychetská	Rychetská	k1gFnSc1	Rychetská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
40	[number]	k4	40
+	+	kIx~	+
526	[number]	k4	526
s.	s.	k?	s.
LEIBNIZ	LEIBNIZ	kA	LEIBNIZ
<g/>
,	,	kIx,	,
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
<g/>
.	.	kIx.	.
</s>
<s>
Theodicea	Theodicea	k1gFnSc1	Theodicea
<g/>
:	:	kIx,	:
pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
dobrotě	dobrota	k1gFnSc6	dobrota
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
svobodě	svoboda	k1gFnSc3	svoboda
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
původu	původ	k1gInSc2	původ
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Šprunk	Šprunk	k1gMnSc1	Šprunk
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
413	[number]	k4	413
s.	s.	k?	s.
Knihovna	knihovna	k1gFnSc1	knihovna
novověké	novověký	k2eAgFnSc2d1	novověká
tradice	tradice	k1gFnSc2	tradice
a	a	k8xC	a
současnosti	současnost	k1gFnSc2	současnost
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
41	[number]	k4	41
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7298	[number]	k4	7298
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
LEIBNIZ	LEIBNIZ	kA	LEIBNIZ
<g/>
,	,	kIx,	,
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
reforme	reform	k1gInSc5	reform
vied	viedo	k1gNnPc2	viedo
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Ján	Ján	k1gMnSc1	Ján
Šebestík	Šebestík	k1gMnSc1	Šebestík
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Vydavateľstvo	Vydavateľstvo	k1gNnSc1	Vydavateľstvo
Slovenskej	Slovenskej	k?	Slovenskej
akadémie	akadémie	k1gFnSc2	akadémie
vied	vieda	k1gFnPc2	vieda
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
140	[number]	k4	140
s.	s.	k?	s.
</s>
