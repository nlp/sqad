<s>
Gottfried	Gottfried	k1gInSc1	Gottfried
Wilhelm	Wilhelma	k1gFnPc2	Wilhelma
von	von	k1gInSc1	von
Leibniz	Leibniz	k1gMnSc1	Leibniz
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1646	[number]	k4	1646
Lipsko	Lipsko	k1gNnSc4	Lipsko
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1716	[number]	k4	1716
Hannover	Hannover	k1gInSc1	Hannover
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
píšící	píšící	k2eAgMnSc1d1	píšící
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
a	a	k8xC	a
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
</s>
