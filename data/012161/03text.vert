<p>
<s>
Kišó	Kišó	k?	Kišó
Kurokawa	Kurokawa	k1gFnSc1	Kurokawa
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1934	[number]	k4	1934
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
japonský	japonský	k2eAgMnSc1d1	japonský
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
hnutí	hnutí	k1gNnSc2	hnutí
metabolistů	metabolista	k1gMnPc2	metabolista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kanie	Kanie	k1gFnSc2	Kanie
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
prefektuře	prefektura	k1gFnSc6	prefektura
Aiči	Aič	k1gFnSc2	Aič
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
architekturu	architektura	k1gFnSc4	architektura
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Kjóto	Kjóto	k1gNnSc4	Kjóto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dovršil	dovršit	k5eAaPmAgMnS	dovršit
bakaláře	bakalář	k1gMnPc4	bakalář
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Inženýrský	inženýrský	k2eAgInSc1d1	inženýrský
titul	titul	k1gInSc1	titul
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Kenza	Kenz	k1gMnSc2	Kenz
Tangea	Tangeus	k1gMnSc2	Tangeus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
kolegy	kolega	k1gMnPc7	kolega
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
hnutí	hnutí	k1gNnPc2	hnutí
metabolistů	metabolista	k1gMnPc2	metabolista
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
japonské	japonský	k2eAgNnSc4d1	Japonské
avantgardní	avantgardní	k2eAgNnSc4d1	avantgardní
sdružení	sdružení	k1gNnSc4	sdružení
<g/>
,	,	kIx,	,
radikálně	radikálně	k6eAd1	radikálně
prosazující	prosazující	k2eAgNnSc4d1	prosazující
znovuoživení	znovuoživení	k1gNnSc4	znovuoživení
architektonických	architektonický	k2eAgInPc2d1	architektonický
stylů	styl	k1gInPc2	styl
v	v	k7c6	v
asijském	asijský	k2eAgInSc6d1	asijský
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
uznáním	uznání	k1gNnSc7	uznání
za	za	k7c4	za
stánek	stánek	k1gInSc4	stánek
Takara	Takara	k1gFnSc1	Takara
Cotillion	Cotillion	k1gInSc1	Cotillion
Beautillion	Beautillion	k1gInSc1	Beautillion
na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Ósace	Ósaka	k1gFnSc6	Ósaka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kurokawa	Kurokawa	k1gMnSc1	Kurokawa
měl	mít	k5eAaImAgMnS	mít
dceru	dcera	k1gFnSc4	dcera
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
spolužačkou	spolužačka	k1gFnSc7	spolužačka
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
manželství	manželství	k1gNnSc4	manželství
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Ayako	Ayako	k1gNnSc1	Ayako
Wakao	Wakao	k6eAd1	Wakao
<g/>
.	.	kIx.	.
</s>
<s>
Kurokawův	Kurokawův	k2eAgMnSc1d1	Kurokawův
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
projektech	projekt	k1gInPc6	projekt
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
designu	design	k1gInSc2	design
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kurokawa	Kurokawa	k1gMnSc1	Kurokawa
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
předsedal	předsedat	k5eAaImAgMnS	předsedat
své	svůj	k3xOyFgFnPc4	svůj
společnosti	společnost	k1gFnPc4	společnost
Kisho	Kis	k1gMnSc2	Kis
Kurokawa	Kurokawus	k1gMnSc2	Kurokawus
Architect	Architect	k1gInSc1	Architect
&	&	k?	&
Associates	Associates	k1gInSc1	Associates
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Centrála	centrála	k1gFnSc1	centrála
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
pobočky	pobočka	k1gFnPc4	pobočka
v	v	k7c6	v
Ósace	Ósaka	k1gFnSc6	Ósaka
<g/>
,	,	kIx,	,
Nagoje	Nagoj	k1gInPc1	Nagoj
<g/>
,	,	kIx,	,
Astaně	Astaň	k1gFnPc1	Astaň
<g/>
,	,	kIx,	,
Kuala	Kuala	k1gFnSc1	Kuala
Lumpuru	Lumpur	k1gInSc2	Lumpur
<g/>
,	,	kIx,	,
Pekingu	Peking	k1gInSc2	Peking
a	a	k8xC	a
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
japonskou	japonský	k2eAgFnSc7d1	japonská
vládou	vláda	k1gFnSc7	vláda
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c2	za
"	"	kIx"	"
<g/>
First	First	k1gFnSc1	First
Class	Class	k1gInSc4	Class
Architects	Architects	k1gInSc1	Architects
Office	Office	kA	Office
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Kurokawa	Kurokawa	k1gMnSc1	Kurokawa
vydal	vydat	k5eAaPmAgMnS	vydat
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
filozofii	filozofie	k1gFnSc6	filozofie
a	a	k8xC	a
architektuře	architektura	k1gFnSc6	architektura
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
přednášel	přednášet	k5eAaImAgMnS	přednášet
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
tradice	tradice	k1gFnPc1	tradice
<g/>
,	,	kIx,	,
sdružené	sdružený	k2eAgFnPc1d1	sdružená
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
:	:	kIx,	:
viditelné	viditelný	k2eAgFnPc1d1	viditelná
a	a	k8xC	a
neviditelné	viditelný	k2eNgFnPc1d1	neviditelná
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
sám	sám	k3xTgMnSc1	sám
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
nesla	nést	k5eAaImAgFnS	nést
neviditelnou	viditelný	k2eNgFnSc4d1	neviditelná
japonskou	japonský	k2eAgFnSc4d1	japonská
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vidět	vidět	k5eAaImF	vidět
zejména	zejména	k9	zejména
na	na	k7c6	na
"	"	kIx"	"
<g/>
materiálech	materiál	k1gInPc6	materiál
<g/>
,	,	kIx,	,
v	v	k7c6	v
nestálosti	nestálost	k1gFnSc6	nestálost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vnímavosti	vnímavost	k1gFnSc6	vnímavost
a	a	k8xC	a
detailu	detail	k1gInSc6	detail
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
metabolickou	metabolický	k2eAgFnSc4d1	metabolická
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tradice	tradice	k1gFnSc1	tradice
nezdá	zdát	k5eNaImIp3nS	zdát
velmi	velmi	k6eAd1	velmi
přítomna	přítomen	k2eAgFnSc1d1	přítomna
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
lze	lze	k6eAd1	lze
taktéž	taktéž	k?	taktéž
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
práce	práce	k1gFnSc1	práce
japonská	japonský	k2eAgFnSc1d1	japonská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
selhání	selhání	k1gNnSc4	selhání
srdce	srdce	k1gNnSc1	srdce
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
73	[number]	k4	73
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nestálost	nestálost	k1gFnSc4	nestálost
==	==	k?	==
</s>
</p>
<p>
<s>
Kurokawa	Kurokawa	k6eAd1	Kurokawa
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
destrukcí	destrukce	k1gFnSc7	destrukce
japonských	japonský	k2eAgNnPc2d1	Japonské
měst	město	k1gNnPc2	město
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přírodními	přírodní	k2eAgFnPc7d1	přírodní
katastrofami	katastrofa	k1gFnPc7	katastrofa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zanechaly	zanechat	k5eAaPmAgFnP	zanechat
v	v	k7c6	v
japonských	japonský	k2eAgNnPc6d1	Japonské
městech	město	k1gNnPc6	město
pouze	pouze	k6eAd1	pouze
cihly	cihla	k1gFnPc1	cihla
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
pomník	pomník	k1gInSc4	pomník
minulých	minulý	k2eAgNnPc2d1	Minulé
měst	město	k1gNnPc2	město
a	a	k8xC	a
ne	ne	k9	ne
dřevo	dřevo	k1gNnSc4	dřevo
a	a	k8xC	a
přírodní	přírodní	k2eAgInPc4d1	přírodní
elementy	element	k1gInPc4	element
jako	jako	k9	jako
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
kulturu	kultura	k1gFnSc4	kultura
tradiční	tradiční	k2eAgFnSc4d1	tradiční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
vychází	vycházet	k5eAaImIp3nS	vycházet
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
motiv	motiv	k1gInSc4	motiv
proměnlivosti	proměnlivost	k1gFnSc2	proměnlivost
<g/>
,	,	kIx,	,
4	[number]	k4	4
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
linií	linie	k1gFnSc7	linie
s	s	k7c7	s
dramatickými	dramatický	k2eAgFnPc7d1	dramatická
proměnami	proměna	k1gFnPc7	proměna
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
budovy	budova	k1gFnPc1	budova
a	a	k8xC	a
města	město	k1gNnPc1	město
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
přírodní	přírodní	k2eAgFnPc4d1	přírodní
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
v	v	k7c6	v
harmonii	harmonie	k1gFnSc6	harmonie
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
stálou	stálý	k2eAgFnSc7d1	stálá
veličinou	veličina	k1gFnSc7	veličina
<g/>
,	,	kIx,	,
napomohla	napomoct	k5eAaPmAgFnS	napomoct
vzniku	vznik	k1gInSc2	vznik
tradice	tradice	k1gFnSc2	tradice
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
"	"	kIx"	"
<g/>
dočasných	dočasný	k2eAgFnPc2d1	dočasná
<g/>
"	"	kIx"	"
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
nestálosti	nestálost	k1gFnSc2	nestálost
se	se	k3xPyFc4	se
odrážela	odrážet	k5eAaImAgFnS	odrážet
v	v	k7c6	v
Kurokawově	Kurokawův	k2eAgNnSc6d1	Kurokawův
díle	dílo	k1gNnSc6	dílo
během	během	k7c2	během
trvání	trvání	k1gNnSc2	trvání
hnutí	hnutí	k1gNnSc2	hnutí
tzv.	tzv.	kA	tzv.
metabolistů	metabolista	k1gMnPc2	metabolista
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
stavěny	stavit	k5eAaImNgFnP	stavit
jako	jako	k9	jako
mobilní	mobilní	k2eAgFnPc1d1	mobilní
<g/>
,	,	kIx,	,
přizpůsobitelné	přizpůsobitelný	k2eAgFnPc1d1	přizpůsobitelná
a	a	k8xC	a
měnitelné	měnitelný	k2eAgFnPc1d1	měnitelná
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
směřoval	směřovat	k5eAaImAgInS	směřovat
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
k	k	k7c3	k
otevřenému	otevřený	k2eAgInSc3d1	otevřený
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
systému	systém	k1gInSc2	systém
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Materiál	materiál	k1gInSc1	materiál
==	==	k?	==
</s>
</p>
<p>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
nepoužívat	používat	k5eNaImF	používat
nepřirozené	přirozený	k2eNgInPc4d1	nepřirozený
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
textury	textura	k1gFnPc4	textura
<g/>
.	.	kIx.	.
</s>
<s>
Vyznával	vyznávat	k5eAaImAgMnS	vyznávat
krásu	krása	k1gFnSc4	krása
materiálu	materiál	k1gInSc2	materiál
jako	jako	k9	jako
takového	takový	k3xDgNnSc2	takový
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
žádného	žádný	k3yNgNnSc2	žádný
přikrášlování	přikrášlování	k1gNnSc2	přikrášlování
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
maximální	maximální	k2eAgInSc4d1	maximální
požitek	požitek	k1gInSc4	požitek
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgInSc4d1	vycházející
z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
statků	statek	k1gInPc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Kurokawa	Kurokawa	k1gMnSc1	Kurokawa
hrdě	hrdě	k6eAd1	hrdě
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
železo	železo	k1gNnSc4	železo
jako	jako	k8xS	jako
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
jako	jako	k8xC	jako
hliník	hliník	k1gInSc1	hliník
a	a	k8xC	a
beton	beton	k1gInSc1	beton
téměř	téměř	k6eAd1	téměř
neupravený	upravený	k2eNgInSc1d1	neupravený
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
hrdost	hrdost	k1gFnSc4	hrdost
k	k	k7c3	k
materiálu	materiál	k1gInSc3	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc4	všechen
tyto	tento	k3xDgInPc4	tento
jeho	jeho	k3xOp3gInPc4	jeho
pocity	pocit	k1gInPc4	pocit
zakomponoval	zakomponovat	k5eAaPmAgMnS	zakomponovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
teze	teze	k1gFnSc2	teze
capsule	capsule	k1gFnSc2	capsule
building	building	k1gInSc1	building
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
-	-	kIx~	-
eskalátory	eskalátor	k1gInPc1	eskalátor
<g/>
,	,	kIx,	,
výtahy	výtah	k1gInPc1	výtah
<g/>
,	,	kIx,	,
potrubí	potrubí	k1gNnPc1	potrubí
a	a	k8xC	a
trubky	trubka	k1gFnPc1	trubka
jsou	být	k5eAaImIp3nP	být
exteriérové	exteriérový	k2eAgFnPc1d1	exteriérová
a	a	k8xC	a
viditelné	viditelný	k2eAgFnPc1d1	viditelná
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
otevřené	otevřený	k2eAgFnPc1d1	otevřená
stavby	stavba	k1gFnPc1	stavba
přiznávají	přiznávat	k5eAaImIp3nP	přiznávat
všechny	všechen	k3xTgFnPc1	všechen
sítě	síť	k1gFnPc1	síť
a	a	k8xC	a
konstrukce	konstrukce	k1gFnPc1	konstrukce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
povýšeny	povýšit	k5eAaPmNgFnP	povýšit
na	na	k7c4	na
zprostředkovatele	zprostředkovatel	k1gMnPc4	zprostředkovatel
krásy	krása	k1gFnSc2	krása
</s>
</p>
<p>
<s>
==	==	k?	==
Detail	detail	k1gInSc1	detail
==	==	k?	==
</s>
</p>
<p>
<s>
Smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
detail	detail	k1gInSc4	detail
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Japonsko	Japonsko	k1gNnSc4	Japonsko
typický	typický	k2eAgInSc1d1	typický
a	a	k8xC	a
podle	podle	k7c2	podle
Kurokawa	Kurokaw	k1gInSc2	Kurokaw
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
snahu	snaha	k1gFnSc4	snaha
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
individualitu	individualita	k1gFnSc4	individualita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
normální	normální	k2eAgMnSc1d1	normální
postupovat	postupovat	k5eAaImF	postupovat
od	od	k7c2	od
částí	část	k1gFnPc2	část
k	k	k7c3	k
celku	celek	k1gInSc3	celek
a	a	k8xC	a
ne	ne	k9	ne
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
každý	každý	k3xTgInSc1	každý
tesařský	tesařský	k2eAgInSc1d1	tesařský
spoj	spoj	k1gInSc1	spoj
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
drobnohledem	drobnohled	k1gInSc7	drobnohled
Kurokawy	Kurokawa	k1gFnSc2	Kurokawa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
Japonců	Japonec	k1gMnPc2	Japonec
způsobila	způsobit	k5eAaPmAgFnS	způsobit
změnu	změna	k1gFnSc4	změna
agrární	agrární	k2eAgFnSc2d1	agrární
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
plně	plně	k6eAd1	plně
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
národ	národ	k1gInSc4	národ
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Cit	cit	k1gInSc1	cit
pro	pro	k7c4	pro
jemné	jemný	k2eAgFnPc4d1	jemná
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
detail	detail	k1gInSc4	detail
však	však	k9	však
splynul	splynout	k5eAaPmAgMnS	splynout
s	s	k7c7	s
úlohou	úloha	k1gFnSc7	úloha
tvůrce	tvůrce	k1gMnSc2	tvůrce
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
výsledkem	výsledek	k1gInSc7	výsledek
jsou	být	k5eAaImIp3nP	být
díla	dílo	k1gNnPc1	dílo
svébytné	svébytný	k2eAgFnSc2d1	svébytná
estetiky	estetika	k1gFnSc2	estetika
<g/>
.	.	kIx.	.
</s>
<s>
Detailnost	detailnost	k1gFnSc1	detailnost
přinesla	přinést	k5eAaPmAgFnS	přinést
nový	nový	k2eAgInSc4d1	nový
stupeň	stupeň	k1gInSc4	stupeň
hierarchie	hierarchie	k1gFnSc2	hierarchie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Kurokawa	Kurokawus	k1gMnSc4	Kurokawus
popsal	popsat	k5eAaPmAgMnS	popsat
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Západní	západní	k2eAgFnSc1d1	západní
architektura	architektura	k1gFnSc1	architektura
a	a	k8xC	a
města	město	k1gNnPc4	město
jsou	být	k5eAaImIp3nP	být
organizovány	organizovat	k5eAaBmNgInP	organizovat
do	do	k7c2	do
hierarchie	hierarchie	k1gFnSc2	hierarchie
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
současná	současný	k2eAgFnSc1d1	současná
Japonská	japonský	k2eAgFnSc1d1	japonská
architektura	architektura	k1gFnSc1	architektura
se	se	k3xPyFc4	se
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c6	o
anatomii	anatomie	k1gFnSc6	anatomie
částí	část	k1gFnPc2	část
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Projekty	projekt	k1gInPc1	projekt
==	==	k?	==
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
-	-	kIx~	-
80	[number]	k4	80
</s>
</p>
<p>
<s>
Nakagin	Nakagin	k1gMnSc1	Nakagin
Capsule	Capsule	k1gFnSc2	Capsule
Tower	Tower	k1gMnSc1	Tower
(	(	kIx(	(
<g/>
Ginza	Ginza	k1gFnSc1	Ginza
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sony	Sony	kA	Sony
Tower	Tower	k1gMnSc1	Tower
(	(	kIx(	(
<g/>
Osaka	Osaka	k1gMnSc1	Osaka
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tateshina	Tateshina	k1gFnSc1	Tateshina
Planetarium	Planetarium	k1gNnSc4	Planetarium
(	(	kIx(	(
<g/>
Hirošima	Hirošima	k1gFnSc1	Hirošima
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Headquarters	Headquarters	k1gInSc1	Headquarters
of	of	k?	of
the	the	k?	the
Japanese	Japanese	k1gFnSc2	Japanese
Red	Red	k1gMnSc2	Red
Cross	Crossa	k1gFnPc2	Crossa
Society	societa	k1gFnSc2	societa
(	(	kIx(	(
<g/>
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
National	Nationat	k5eAaImAgMnS	Nationat
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Ethnology	Ethnolog	k1gMnPc7	Ethnolog
(	(	kIx(	(
<g/>
Osaka	Osaka	k1gMnSc1	Osaka
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
1980	[number]	k4	1980
-	-	kIx~	-
90	[number]	k4	90
</s>
</p>
<p>
<s>
Saitama	Saitama	k1gNnSc1	Saitama
Prefectural	Prefectural	k1gFnPc2	Prefectural
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Modern	Modern	k1gMnSc1	Modern
Art	Art	k1gMnSc1	Art
(	(	kIx(	(
<g/>
Saitama	Saitama	k1gFnSc1	Saitama
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
National	Nationat	k5eAaPmAgMnS	Nationat
Bunraku	Bunrak	k1gMnSc3	Bunrak
Theater	Theater	k1gMnSc1	Theater
(	(	kIx(	(
<g/>
Osaka	Osaka	k1gMnSc1	Osaka
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wacoal	Wacoal	k1gMnSc1	Wacoal
Kojimachi	Kojimach	k1gFnSc2	Kojimach
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chokaso	Chokasa	k1gFnSc5	Chokasa
(	(	kIx(	(
<g/>
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nagoya	Nagoya	k6eAd1	Nagoya
City	city	k1gNnSc1	city
Art	Art	k1gFnSc2	Art
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
Nagoja	Nagoja	k1gMnSc1	Nagoja
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Japanese-German	Japanese-German	k1gMnSc1	Japanese-German
Center	centrum	k1gNnPc2	centrum
of	of	k?	of
Berlin	berlina	k1gFnPc2	berlina
(	(	kIx(	(
<g/>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Central	Centrat	k5eAaPmAgMnS	Centrat
Plaza	plaz	k1gMnSc4	plaz
1	[number]	k4	1
<g/>
,	,	kIx,	,
Brisbane	Brisban	k1gMnSc5	Brisban
<g/>
,	,	kIx,	,
Queensland	Queenslanda	k1gFnPc2	Queenslanda
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
</s>
</p>
<p>
<s>
Osaka	Osaka	k1gMnSc1	Osaka
Prefectural	Prefectural	k1gMnSc1	Prefectural
Government	Government	k1gMnSc1	Government
Offices	Offices	k1gMnSc1	Offices
(	(	kIx(	(
<g/>
Osaka	Osaka	k1gMnSc1	Osaka
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hiroshima	Hiroshima	k1gFnSc1	Hiroshima
City	City	k1gFnSc2	City
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Contemporary	Contemporara	k1gFnSc2	Contemporara
Art	Art	k1gFnSc2	Art
(	(	kIx(	(
<g/>
Hirošima	Hirošima	k1gFnSc1	Hirošima
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
-	-	kIx~	-
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Chinese-Japanese	Chinese-Japanést	k5eAaPmIp3nS	Chinese-Japanést
Youth	Youth	k1gInSc1	Youth
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Peking	Peking	k1gInSc1	Peking
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Okinawa	Okinawa	k1gFnSc1	Okinawa
Prefectural	Prefectural	k1gFnSc1	Prefectural
Government	Government	k1gInSc4	Government
Headquarters	Headquarters	k1gInSc1	Headquarters
(	(	kIx(	(
<g/>
Okinawa	Okinawa	k1gFnSc1	Okinawa
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Sporting	Sporting	k1gInSc1	Sporting
Club	club	k1gInSc1	club
at	at	k?	at
Illinois	Illinois	k1gFnSc1	Illinois
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Melbourne	Melbourne	k1gNnSc1	Melbourne
Central	Central	k1gFnSc2	Central
(	(	kIx(	(
<g/>
Melbourne	Melbourne	k1gNnSc2	Melbourne
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miki	Miki	k6eAd1	Miki
House	house	k1gNnSc1	house
New	New	k1gFnPc2	New
Office	Office	kA	Office
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
Osaka	Osaka	k1gMnSc1	Osaka
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nara	Nar	k2eAgFnSc1d1	Nara
City	City	k1gFnSc1	City
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Photography	Photographa	k1gMnSc2	Photographa
(	(	kIx(	(
<g/>
Nara	Narus	k1gMnSc2	Narus
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Louvain-La-Neuve	Louvain-La-Neuvat	k5eAaPmIp3nS	Louvain-La-Neuvat
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pacific	Pacific	k1gMnSc1	Pacific
Tower	Tower	k1gMnSc1	Tower
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lane	Lanat	k5eAaPmIp3nS	Lanat
Crawford	Crawford	k1gInSc1	Crawford
Place	plac	k1gInSc6	plac
(	(	kIx(	(
<g/>
Singapur	Singapur	k1gInSc1	Singapur
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Senkantei	Senkantei	k1gNnSc1	Senkantei
(	(	kIx(	(
<g/>
Hyō	Hyō	k1gMnSc1	Hyō
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ehime	Ehimat	k5eAaPmIp3nS	Ehimat
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Science	Science	k1gFnSc1	Science
(	(	kIx(	(
<g/>
Ehime	Ehim	k1gMnSc5	Ehim
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ishibashi	Ishibashi	k6eAd1	Ishibashi
Junior	junior	k1gMnSc1	junior
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
(	(	kIx(	(
<g/>
Tochigi	Tochigi	k1gNnSc1	Tochigi
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Modern	Moderna	k1gFnPc2	Moderna
Art	Art	k1gFnSc1	Art
Wakayama	Wakayama	k1gFnSc1	Wakayama
<g/>
/	/	kIx~	/
<g/>
Wakayama	Wakayama	k1gFnSc1	Wakayama
Prefectural	Prefectural	k1gFnSc2	Prefectural
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
Wakayama	Wakayama	k1gNnSc1	Wakayama
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hotel	hotel	k1gInSc1	hotel
Kyocera	Kyocero	k1gNnSc2	Kyocero
(	(	kIx(	(
<g/>
Kagoshima	Kagoshima	k1gNnSc1	Kagoshima
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kibi-cho	Kibiho	k6eAd1	Kibi-cho
City	city	k1gNnSc1	city
Hall	Hall	k1gInSc1	Hall
<g/>
/	/	kIx~	/
<g/>
Kibi	Kibi	k1gNnSc1	Kibi
Dome	dům	k1gInSc5	dům
(	(	kIx(	(
<g/>
Wakayama	Wakayam	k1gMnSc2	Wakayam
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Republic	Republice	k1gFnPc2	Republice
Plaza	plaz	k1gMnSc2	plaz
(	(	kIx(	(
<g/>
Singapore	Singapor	k1gMnSc5	Singapor
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fukui	Fuku	k1gMnPc1	Fuku
City	City	k1gFnSc2	City
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gFnSc1	Art
(	(	kIx(	(
<g/>
Fukui	Fukui	k1gNnSc1	Fukui
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Softopia	Softopia	k1gFnSc1	Softopia
Japan	japan	k1gInSc1	japan
(	(	kIx(	(
<g/>
Gifu	Gifus	k1gInSc2	Gifus
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fujinomiya	Fujinomiya	k6eAd1	Fujinomiya
Golf	golf	k1gInSc1	golf
Club	club	k1gInSc1	club
(	(	kIx(	(
<g/>
Fujinomiya	Fujinomiya	k1gMnSc1	Fujinomiya
<g/>
,	,	kIx,	,
Shizuoka	Shizuoka	k1gMnSc1	Shizuoka
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kashima-machi	Kashimaachi	k6eAd1	Kashima-machi
City	city	k1gNnSc1	city
Hall	Halla	k1gFnPc2	Halla
(	(	kIx(	(
<g/>
Kumamoto	Kumamota	k1gFnSc5	Kumamota
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Shiga	Shiga	k1gFnSc1	Shiga
Kogen	Kogna	k1gFnPc2	Kogna
Roman	Romana	k1gFnPc2	Romana
Art	Art	k1gFnSc2	Art
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
Yamanouchi	Yamanouchi	k1gNnSc1	Yamanouchi
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Kuala	Kuala	k1gMnSc1	Kuala
Lumpur	Lumpur	k1gMnSc1	Lumpur
(	(	kIx(	(
<g/>
Kuala	Kuala	k1gMnSc1	Kuala
Lumpur	Lumpur	k1gMnSc1	Lumpur
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc4d1	Nové
křídlo	křídlo	k1gNnSc4	křídlo
Van	vana	k1gFnPc2	vana
Goghova	Goghův	k2eAgNnSc2d1	Goghovo
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Amber	ambra	k1gFnPc2	ambra
Hall	Halla	k1gFnPc2	Halla
(	(	kIx(	(
<g/>
Kuji	kout	k5eAaImIp1nS	kout
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
O	o	k7c4	o
Residence	residence	k1gFnPc4	residence
(	(	kIx(	(
<g/>
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
</s>
</p>
<p>
<s>
Fukui	Fukui	k1gNnSc1	Fukui
Prefectural	Prefectural	k1gFnSc2	Prefectural
Dinosaur	Dinosaura	k1gFnPc2	Dinosaura
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
Katsuyama	Katsuyama	k1gNnSc1	Katsuyama
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Osaka	Osaka	k1gMnSc1	Osaka
International	International	k1gFnSc2	International
Convention	Convention	k1gInSc1	Convention
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Osaka	Osaka	k1gFnSc1	Osaka
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oita	Oita	k6eAd1	Oita
Stadium	stadium	k1gNnSc1	stadium
(	(	kIx(	(
<g/>
Ō	Ō	k?	Ō
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Toyota	toyota	k1gFnSc1	toyota
Stadium	stadium	k1gNnSc4	stadium
(	(	kIx(	(
<g/>
Toyota	toyota	k1gFnSc1	toyota
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Letiště	letiště	k1gNnSc1	letiště
Astana	Astana	k1gFnSc1	Astana
(	(	kIx(	(
<g/>
Astana	Astana	k1gFnSc1	Astana
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
National	National	k1gMnSc1	National
Art	Art	k1gMnSc1	Art
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc1	Tokio
(	(	kIx(	(
<g/>
Roppongi	Roppongi	k1gNnSc1	Roppongi
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Singapore	Singapor	k1gMnSc5	Singapor
Flyer	Flyer	k1gMnSc1	Flyer
(	(	kIx(	(
<g/>
Singapur	Singapur	k1gInSc1	Singapur
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fusionopolis	Fusionopolis	k1gFnSc1	Fusionopolis
Phrase	Phras	k1gInSc6	Phras
1	[number]	k4	1
(	(	kIx(	(
<g/>
Singapur	Singapur	k1gInSc1	Singapur
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
-	-	kIx~	-
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Design	design	k1gInSc1	design
and	and	k?	and
Master	master	k1gMnSc1	master
Plan	plan	k1gInSc1	plan
of	of	k?	of
Kazakhstan	Kazakhstan	k1gInSc1	Kazakhstan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
New	New	k1gFnSc7	New
Capital	Capital	k1gMnSc1	Capital
(	(	kIx(	(
<g/>
Astana	Astana	k1gFnSc1	Astana
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krestovsky	Krestovsky	k6eAd1	Krestovsky
stadion	stadion	k1gInSc1	stadion
(	(	kIx(	(
<g/>
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trade	Trade	k6eAd1	Trade
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Yekaterinburg	Yekaterinburg	k1gInSc1	Yekaterinburg
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Maggie	Maggie	k1gFnSc1	Maggie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cancer	Cancer	k1gInSc1	Cancer
Care	car	k1gMnSc5	car
Centre	centr	k1gInSc5	centr
<g/>
,	,	kIx,	,
Singleton	Singleton	k1gInSc4	Singleton
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
,	,	kIx,	,
Swansea	Swansea	k1gMnSc1	Swansea
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kišó	Kišó	k1gMnSc2	Kišó
Kurokawa	Kurokawus	k1gMnSc2	Kurokawus
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kišó	Kišó	k1gFnSc2	Kišó
Kurokawa	Kurokawum	k1gNnSc2	Kurokawum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
