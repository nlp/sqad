<s>
Originální	originální	k2eAgInSc1d1
Pražský	pražský	k2eAgInSc1d1
Synkopický	synkopický	k2eAgInSc1d1
Orchestr	orchestr	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Original	Original	k1gMnSc1
Prague	Pragu	k1gFnSc2
Syncopated	Syncopated	k1gMnSc1
Orchestra	orchestra	k1gFnSc1
<g/>
,	,	kIx,
zkracováno	zkracován	k2eAgNnSc1d1
na	na	k7c6
OPSO	OPSO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
hrající	hrající	k2eAgInSc1d1
dobový	dobový	k2eAgInSc1d1
jazz	jazz	k1gInSc1
<g/>
,	,	kIx,
swing	swing	k1gInSc1
<g/>
,	,	kIx,
blues	blues	k1gNnSc4
a	a	k8xC
spoustu	spousta	k1gFnSc4
dalších	další	k2eAgInPc2d1
stylů	styl	k1gInPc2
z	z	k7c2
let	léto	k1gNnPc2
1922	#num#	k4
<g/>
–	–	k?
<g/>
1933	#num#	k4
<g/>
,	,	kIx,
např.	např.	kA
charleston	charleston	k1gInSc1
<g/>
,	,	kIx,
black	black	k1gInSc1
bottom	bottom	k1gInSc1
<g/>
,	,	kIx,
foxtrot	foxtrot	k1gInSc1
<g/>
.	.	kIx.
</s>