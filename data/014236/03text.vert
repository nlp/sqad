<s>
Originální	originální	k2eAgInSc1d1
Pražský	pražský	k2eAgInSc1d1
Synkopický	synkopický	k2eAgInSc1d1
Orchestr	orchestr	k1gInSc1
</s>
<s>
Originální	originální	k2eAgInSc1d1
Pražský	pražský	k2eAgInSc1d1
Synkopický	synkopický	k2eAgInSc1d1
Orchestr	orchestr	k1gInSc1
Členové	člen	k1gMnPc1
Originálního	originální	k2eAgInSc2d1
Pražského	pražský	k2eAgInSc2d1
Synkopického	synkopický	k2eAgInSc2d1
Orchestru	orchestr	k1gInSc2
koncertující	koncertující	k2eAgFnPc4d1
na	na	k7c6
Karlově	Karlův	k2eAgInSc6d1
mostě	most	k1gInSc6
v	v	k7c6
PrazeZákladní	PrazeZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Jinak	jinak	k6eAd1
zvaný	zvaný	k2eAgInSc4d1
<g/>
/	/	kIx~
<g/>
á	á	k0
</s>
<s>
Original	Originat	k5eAaPmAgInS,k5eAaImAgInS
Prague	Prague	k1gInSc1
Syncopated	Syncopated	k1gMnSc1
Orchestra	orchestra	k1gFnSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
OPSO	OPSO	kA
Původ	původ	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
Žánry	žánr	k1gInPc1
</s>
<s>
swingraný	swingraný	k2eAgInSc1d1
jazz	jazz	k1gInSc1
Aktivní	aktivní	k2eAgInSc1d1
roky	rok	k1gInPc4
</s>
<s>
1974-1995	1974-1995	k4
Vydavatelé	vydavatel	k1gMnPc5
</s>
<s>
Panton	Panton	k1gInSc1
<g/>
,	,	kIx,
Supraphon	supraphon	k1gInSc1
<g/>
,	,	kIx,
EMI	EMI	kA
<g/>
,	,	kIx,
CD	CD	kA
BWS	BWS	kA
Web	web	k1gInSc1
</s>
<s>
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Klikar	Klikar	k1gMnSc1
-	-	kIx~
trubka	trubka	k1gFnSc1
<g/>
,	,	kIx,
melafon	melafon	k1gInSc1
<g/>
,	,	kIx,
piano	piano	k1gNnSc1
<g/>
,	,	kIx,
kapelník	kapelník	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Šimůnek	Šimůnek	k1gMnSc1
-	-	kIx~
housle	housle	k1gFnPc1
<g/>
,	,	kIx,
violinofon	violinofon	k1gInSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Vyoral	vyorat	k5eAaPmAgMnS
-	-	kIx~
housle	housle	k1gFnPc4
</s>
<s>
Pavel	Pavel	k1gMnSc1
Jordánek	Jordánek	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
sopránsaxofon	sopránsaxofon	k1gInSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Jirák	Jirák	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
sopransaxofon	sopransaxofon	k1gInSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Černý	Černý	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
altsaxofon	altsaxofon	k1gInSc1
</s>
<s>
Roman	Roman	k1gMnSc1
Novotný	Novotný	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Kroutil	Kroutil	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
altklarinet	altklarinet	k1gMnSc1
Michal	Michal	k1gMnSc1
Zpěvák	Zpěvák	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
sopransaxofon	sopransaxofon	k1gInSc1
<g/>
,	,	kIx,
tarogato	tarogato	k6eAd1
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
basklarinet	basklarinet	k1gInSc1
</s>
<s>
Jakub	Jakub	k1gMnSc1
Šnýdl	Šnýdl	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Tříska	Tříska	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Dobrohruška	Dobrohruška	k1gMnSc1
-	-	kIx~
banjo	banjo	k1gNnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Šícha	Šícha	k1gMnSc1
-	-	kIx~
banjo	banjo	k1gNnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Tichý	Tichý	k1gMnSc1
-	-	kIx~
kytara	kytara	k1gFnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Šturma	Šturma	k1gMnSc1
-	-	kIx~
kytara	kytara	k1gFnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Wajsar	Wajsar	k1gMnSc1
-	-	kIx~
kytara	kytara	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Šícha	Šícha	k1gMnSc1
-	-	kIx~
kytara	kytara	k1gFnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Šturma	Šturma	k1gMnSc1
-	-	kIx~
kontrabas	kontrabas	k1gInSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Landa	Landa	k1gMnSc1
-	-	kIx~
kontrabas	kontrabas	k1gInSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Balcar	Balcar	k1gMnSc1
-	-	kIx~
kontrabas	kontrabas	k1gInSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Zpěvák	Zpěvák	k1gMnSc1
-	-	kIx~
kontrabas	kontrabas	k1gInSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Gilík	Gilík	k1gMnSc1
-	-	kIx~
piano	piano	k6eAd1
</s>
<s>
Alice	Alice	k1gFnSc1
Bauer	Bauer	k1gMnSc1
-	-	kIx~
zpěvačka	zpěvačka	k1gFnSc1
</s>
<s>
Adéla	Adéla	k1gFnSc1
Zejfartová	Zejfartová	k1gFnSc1
-	-	kIx~
zpěvačka	zpěvačka	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Originální	originální	k2eAgInSc1d1
Pražský	pražský	k2eAgInSc1d1
Synkopický	synkopický	k2eAgInSc1d1
Orchestr	orchestr	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Original	Original	k1gMnSc1
Prague	Pragu	k1gFnSc2
Syncopated	Syncopated	k1gMnSc1
Orchestra	orchestra	k1gFnSc1
<g/>
,	,	kIx,
zkracováno	zkracován	k2eAgNnSc1d1
na	na	k7c6
OPSO	OPSO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
hrající	hrající	k2eAgInSc1d1
dobový	dobový	k2eAgInSc1d1
jazz	jazz	k1gInSc1
<g/>
,	,	kIx,
swing	swing	k1gInSc1
<g/>
,	,	kIx,
blues	blues	k1gNnSc4
a	a	k8xC
spoustu	spousta	k1gFnSc4
dalších	další	k2eAgInPc2d1
stylů	styl	k1gInPc2
z	z	k7c2
let	léto	k1gNnPc2
1922	#num#	k4
<g/>
–	–	k?
<g/>
1933	#num#	k4
<g/>
,	,	kIx,
např.	např.	kA
charleston	charleston	k1gInSc1
<g/>
,	,	kIx,
black	black	k1gInSc1
bottom	bottom	k1gInSc1
<g/>
,	,	kIx,
foxtrot	foxtrot	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Orchestr	orchestr	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
muzikologem	muzikolog	k1gMnSc7
Pavlem	Pavel	k1gMnSc7
Klikarem	Klikar	k1gMnSc7
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1974	#num#	k4
původně	původně	k6eAd1
jako	jako	k8xS,k8xC
kvintet	kvintet	k1gInSc1
-	-	kIx~
se	s	k7c7
třemi	tři	k4xCgInPc7
dechovými	dechový	k2eAgInPc7d1
nástroji	nástroj	k1gInPc7
a	a	k8xC
dvoučlennou	dvoučlenný	k2eAgFnSc7d1
rytmickou	rytmický	k2eAgFnSc7d1
sekcí	sekce	k1gFnSc7
<g/>
,	,	kIx,
tvořenou	tvořený	k2eAgFnSc7d1
pianem	piano	k1gNnSc7
a	a	k8xC
banjem	banjo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
začátku	začátek	k1gInSc2
zněl	znět	k5eAaImAgInS
band	band	k1gInSc1
neuvěřitelně	uvěřitelně	k6eNd1
věrně	věrně	k6eAd1
díky	díky	k7c3
přesné	přesný	k2eAgFnSc3d1
interpretaci	interpretace	k1gFnSc3
hudebníků	hudebník	k1gMnPc2
a	a	k8xC
díky	díky	k7c3
studiu	studio	k1gNnSc3
a	a	k8xC
analýze	analýza	k1gFnSc3
stylu	styl	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
se	se	k3xPyFc4
analyzoval	analyzovat	k5eAaImAgInS
styl	styl	k1gInSc1
improvizace	improvizace	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
jiný	jiný	k2eAgMnSc1d1
než	než	k8xS
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
takže	takže	k8xS
byla	být	k5eAaImAgFnS
autenticita	autenticita	k1gFnSc1
ještě	ještě	k6eAd1
větší	veliký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Band	band	k1gInSc1
samozřejmě	samozřejmě	k6eAd1
používá	používat	k5eAaImIp3nS
dobové	dobový	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
OPSO	OPSO	kA
je	být	k5eAaImIp3nS
známý	známý	k2eAgInSc1d1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
a	a	k8xC
dalo	dát	k5eAaPmAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
říct	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
stylově	stylově	k6eAd1
nejčistší	čistý	k2eAgNnSc1d3
a	a	k8xC
umělecky	umělecky	k6eAd1
nejpřesvědčivější	přesvědčivý	k2eAgInSc1d3
band	band	k1gInSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Hraje	hrát	k5eAaImIp3nS
přes	přes	k7c4
260	#num#	k4
melodií	melodie	k1gFnPc2
z	z	k7c2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
bývají	bývat	k5eAaImIp3nP
málokdy	málokdy	k6eAd1
známé	známý	k2eAgMnPc4d1
přesně	přesně	k6eAd1
jak	jak	k8xS,k8xC
bychom	by	kYmCp1nP
je	on	k3xPp3gInPc4
mohli	moct	k5eAaImAgMnP
slyšet	slyšet	k5eAaImF
na	na	k7c6
původních	původní	k2eAgFnPc6d1
nahrávkách	nahrávka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Momentálně	momentálně	k6eAd1
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
komorním	komorní	k2eAgNnSc6d1
obsazení	obsazení	k1gNnSc6
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
se	se	k3xPyFc4
začínalo	začínat	k5eAaImAgNnS
–	–	k?
7	#num#	k4
až	až	k9
8	#num#	k4
hudebníků	hudebník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořádají	pořádat	k5eAaImIp3nP
koncerty	koncert	k1gInPc1
v	v	k7c6
Blues	blues	k1gNnPc6
sklepě	sklep	k1gInSc6
nebo	nebo	k8xC
na	na	k7c6
Karlově	Karlův	k2eAgInSc6d1
mostě	most	k1gInSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Roku	rok	k1gInSc2
1976	#num#	k4
přibyl	přibýt	k5eAaPmAgInS
do	do	k7c2
orchestru	orchestr	k1gInSc2
druhý	druhý	k4xOgInSc4
saxofon	saxofon	k1gInSc4
a	a	k8xC
housle	housle	k1gFnPc4
a	a	k8xC
také	také	k9
zpěvák	zpěvák	k1gMnSc1
-	-	kIx~
Ondřej	Ondřej	k1gMnSc1
Havelka	Havelka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jisté	jistý	k2eAgFnSc6d1
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
první	první	k4xOgFnPc1
album	album	k1gNnSc4
i	i	k9
se	s	k7c7
zpěvem	zpěv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
květnu	květen	k1gInSc6
<g/>
,	,	kIx,
skončila	skončit	k5eAaPmAgFnS
éra	éra	k1gFnSc1
osmičlenného	osmičlenný	k2eAgNnSc2d1
obsazení	obsazení	k1gNnSc2
-	-	kIx~
na	na	k7c6
jazzovém	jazzový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
v	v	k7c6
holandské	holandský	k2eAgFnSc6d1
Bredě	Breda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
k	k	k7c3
souboru	soubor	k1gInSc3
přibyli	přibýt	k5eAaPmAgMnP
další	další	k2eAgNnPc1d1
4	#num#	k4
hudebníci	hudebník	k1gMnPc1
a	a	k8xC
orchestr	orchestr	k1gInSc4
tak	tak	k6eAd1
dostal	dostat	k5eAaPmAgMnS
vzhledu	vzhled	k1gInSc3
pravého	pravý	k2eAgMnSc2d1
amerického	americký	k2eAgMnSc2d1
big	big	k?
bandu	band	k1gInSc2
dvacátých	dvacátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klasické	klasický	k2eAgFnSc6d1
sestavě	sestava	k1gFnSc6
bylo	být	k5eAaImAgNnS
obsazení	obsazení	k1gNnSc1
<g/>
:	:	kIx,
3	#num#	k4
<g/>
×	×	k?
plátkové	plátkový	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
×	×	k?
trumpety	trumpeta	k1gFnPc1
<g/>
,	,	kIx,
suzafon	suzafon	k1gInSc1
a	a	k8xC
banjo	banjo	k1gNnSc1
<g/>
,	,	kIx,
piano	piano	k1gNnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
×	×	k?
housle	housle	k1gFnPc4
<g/>
,	,	kIx,
bicí	bicí	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
sestavě	sestava	k1gFnSc6
hráli	hrát	k5eAaImAgMnP
až	až	k9
do	do	k7c2
odchodu	odchod	k1gInSc2
Ondřeje	Ondřej	k1gMnSc2
Havelky	Havelka	k1gMnSc2
<g/>
,	,	kIx,
krom	krom	k7c2
alba	album	k1gNnSc2
Sám	sám	k3xTgMnSc1
s	s	k7c7
děvčetem	děvče	k1gNnSc7
v	v	k7c6
dešti	dešť	k1gInSc6
(	(	kIx(
<g/>
kde	kde	k6eAd1
suzafon	suzafon	k1gInSc4
nahradila	nahradit	k5eAaPmAgFnS
basa	basa	k1gFnSc1
a	a	k8xC
banjo	banjo	k1gNnSc1
kytara	kytara	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
odešel	odejít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
založil	založit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
big	big	k?
band	band	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalším	další	k2eAgNnSc6d1
albu	album	k1gNnSc6
už	už	k9
bylo	být	k5eAaImAgNnS
poznat	poznat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
souboru	soubor	k1gInSc2
něco	něco	k3yInSc4
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Alba	alba	k1gFnSc1
Walking	Walking	k1gInSc1
and	and	k?
Swinging	Swinging	k1gInSc1
a	a	k8xC
Blues	blues	k1gNnSc1
pro	pro	k7c4
tebe	ty	k3xPp2nSc4
(	(	kIx(
<g/>
s	s	k7c7
Jiřím	Jiří	k1gMnSc7
Suchým	Suchý	k1gMnSc7
<g/>
)	)	kIx)
už	už	k6eAd1
neměla	mít	k5eNaImAgFnS
ten	ten	k3xDgInSc4
šmrnc	šmrnc	k1gInSc4
dvacátých	dvacátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
spíše	spíše	k9
přešla	přejít	k5eAaPmAgNnP
do	do	k7c2
swingového	swingový	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
následujícím	následující	k2eAgNnSc6d1
albu	album	k1gNnSc6
<g/>
,	,	kIx,
Goin	Goin	k1gNnSc1
<g/>
'	'	kIx"
Crazy	Craza	k1gFnPc1
with	with	k1gInSc4
The	The	k1gFnSc2
Blues	blues	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
ansámbl	ansámbl	k1gInSc1
zmenšil	zmenšit	k5eAaPmAgInS
na	na	k7c4
naprosté	naprostý	k2eAgNnSc4d1
minimum	minimum	k1gNnSc4
<g/>
:	:	kIx,
trumpeta	trumpeta	k1gFnSc1
<g/>
,	,	kIx,
housle	housle	k1gFnPc4
<g/>
,	,	kIx,
2	#num#	k4
kytary	kytara	k1gFnPc1
a	a	k8xC
zpěvačka	zpěvačka	k1gFnSc1
Alice	Alice	k1gFnSc2
Bauer	Bauer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
A	a	k9
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
albu	album	k1gNnSc6
se	se	k3xPyFc4
jakž	jakž	k6eAd1
takž	takž	k6eAd1
navrací	navracet	k5eAaImIp3nS,k5eAaBmIp3nS
ke	k	k7c3
svým	svůj	k3xOyFgInPc3
začátkům	začátek	k1gInPc3
<g/>
,	,	kIx,
avšak	avšak	k8xC
hrají	hrát	k5eAaImIp3nP
spíše	spíše	k9
blues	blues	k1gFnPc1
<g/>
,	,	kIx,
s	s	k7c7
jiným	jiný	k2eAgInSc7d1
nádechem	nádech	k1gInSc7
nežli	nežli	k8xS
v	v	k7c6
dřívějších	dřívější	k2eAgNnPc6d1
albech	album	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Členové	člen	k1gMnPc1
</s>
<s>
1974	#num#	k4
-	-	kIx~
1977	#num#	k4
</s>
<s>
Pavel	Pavel	k1gMnSc1
Klikar	Klikar	k1gMnSc1
–	–	k?
kornet	kornet	k1gInSc1
<g/>
,	,	kIx,
melofon	melofon	k1gInSc1
<g/>
,	,	kIx,
piano	piano	k1gNnSc1
<g/>
,	,	kIx,
kapelník	kapelník	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Štolba	Štolba	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
altsaxofon	altsaxofon	k1gInSc1
</s>
<s>
František	František	k1gMnSc1
Rubáš	rubat	k5eAaImIp2nS
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
tenorsaxofon	tenorsaxofon	k1gInSc1
<g/>
,	,	kIx,
bassaxofon	bassaxofon	k1gInSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Velínský	Velínský	k2eAgMnSc1d1
-	-	kIx~
trombon	trombon	k1gInSc1
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Malý	Malý	k1gMnSc1
-	-	kIx~
housle	housle	k1gFnPc1
<g/>
,	,	kIx,
violinofon	violinofon	k1gInSc1
</s>
<s>
Jura	Jura	k1gMnSc1
Gilík	Gilík	k1gMnSc1
-	-	kIx~
piano	piano	k1gNnSc1
<g/>
,	,	kIx,
klarinet	klarinet	k1gInSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Šícha	Šícha	k1gMnSc1
-	-	kIx~
banjo	banjo	k1gNnSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Havelka	Havelka	k1gMnSc1
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
</s>
<s>
1978	#num#	k4
-	-	kIx~
1987	#num#	k4
</s>
<s>
Pavel	Pavel	k1gMnSc1
Klikar	Klikar	k1gMnSc1
–	–	k?
kornet	kornet	k1gMnSc1
<g/>
,	,	kIx,
trubka	trubka	k1gFnSc1
<g/>
,	,	kIx,
melofon	melofon	k1gInSc1
<g/>
,	,	kIx,
piano	piano	k1gNnSc1
<g/>
,	,	kIx,
kapelník	kapelník	k1gMnSc1
</s>
<s>
Oleg	Oleg	k1gMnSc1
Petruš	Petruš	k1gMnSc1
–	–	k?
kornet	kornet	k1gMnSc1
<g/>
,	,	kIx,
trubka	trubka	k1gFnSc1
</s>
<s>
Michal	Michal	k1gMnSc1
Krása	krása	k1gFnSc1
-	-	kIx~
trubka	trubka	k1gFnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Velínský	Velínský	k2eAgMnSc1d1
-	-	kIx~
trombon	trombon	k1gInSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Štolba	Štolba	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
altsaxofon	altsaxofon	k1gInSc1
</s>
<s>
František	František	k1gMnSc1
Rubáš	rubat	k5eAaImIp2nS
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
tenorsaxofon	tenorsaxofon	k1gInSc1
<g/>
,	,	kIx,
bassaxofon	bassaxofon	k1gInSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Jordánek	Jordánek	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
altsaxofon	altsaxofon	k1gInSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Burle	Burle	k1gFnSc2
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
altsaxofon	altsaxofon	k1gInSc1
</s>
<s>
Luboš	Luboš	k1gMnSc1
Hajný	hajný	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
tenorsaxofon	tenorsaxofon	k1gInSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Harmáček	Harmáček	k1gMnSc1
-	-	kIx~
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
altsaxofon	altsaxofon	k1gInSc1
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Malý	Malý	k1gMnSc1
-	-	kIx~
housle	housle	k1gFnPc1
<g/>
,	,	kIx,
violinofon	violinofon	k1gInSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Ondřejčková	Ondřejčková	k1gFnSc1
-	-	kIx~
housle	housle	k1gFnPc1
<g/>
,	,	kIx,
violinofon	violinofon	k1gInSc1
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Trnavská	trnavský	k2eAgFnSc1d1
-	-	kIx~
housle	housle	k1gFnPc4
</s>
<s>
Radim	Radim	k1gMnSc1
Sedmidubský	sedmidubský	k2eAgMnSc1d1
-	-	kIx~
housle	housle	k1gFnPc1
</s>
<s>
Jura	Jura	k1gMnSc1
Gilík	Gilík	k1gMnSc1
-	-	kIx~
piano	piano	k1gNnSc1
<g/>
,	,	kIx,
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
foukací	foukací	k2eAgFnSc1d1
harmonika	harmonika	k1gFnSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Šícha	Šícha	k1gMnSc1
-	-	kIx~
banjo	banjo	k1gNnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
-	-	kIx~
kytara	kytara	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Malý	Malý	k1gMnSc1
-	-	kIx~
kytara	kytara	k1gFnSc1
</s>
<s>
Michal	Michal	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
-	-	kIx~
suzafon	suzafon	k1gInSc1
<g/>
,	,	kIx,
kontrabas	kontrabas	k1gInSc1
</s>
<s>
Michal	Michal	k1gMnSc1
Hejna	Hejna	k1gMnSc1
-	-	kIx~
bicí	bicí	k2eAgFnSc1d1
<g/>
,	,	kIx,
xylofon	xylofon	k1gInSc1
<g/>
,	,	kIx,
marimba	marimba	k1gFnSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Havelka	Havelka	k1gMnSc1
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
-	-	kIx~
</s>
<s>
1987	#num#	k4
a	a	k8xC
dále	daleko	k6eAd2
se	se	k3xPyFc4
v	v	k7c6
orchestru	orchestr	k1gInSc6
střídali	střídat	k5eAaImAgMnP
i	i	k9
další	další	k2eAgMnPc1d1
hudebníci	hudebník	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Šimůnek	Šimůnek	k1gMnSc1
–	–	k?
housle	housle	k1gFnPc4
<g/>
,	,	kIx,
violinofon	violinofon	k1gInSc4
</s>
<s>
Petr	Petr	k1gMnSc1
Vyoral	vyorat	k5eAaPmAgMnS
–	–	k?
housle	housle	k1gFnPc4
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
big	big	k?
bandu	band	k1gInSc2
Ondřej	Ondřej	k1gMnSc1
Havelka	Havelka	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
Melody	Meloda	k1gFnPc1
Makers	Makers	k1gInSc4
</s>
<s>
Pavel	Pavel	k1gMnSc1
Jordánek	Jordánek	k1gMnSc1
–	–	k?
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
sopransaxofon	sopransaxofon	k1gInSc1
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
big	big	k?
bandu	band	k1gInSc2
Ondřej	Ondřej	k1gMnSc1
Havelka	Havelka	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
Melody	Meloda	k1gFnPc1
Makers	Makersa	k1gFnPc2
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Jirák	Jirák	k1gMnSc1
–	–	k?
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
sopransaxofon	sopransaxofon	k1gInSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Černý	Černý	k1gMnSc1
–	–	k?
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
altsaxofon	altsaxofon	k1gInSc1
</s>
<s>
Roman	Roman	k1gMnSc1
Novotný	Novotný	k1gMnSc1
–	–	k?
klarinet	klarinet	k1gInSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Kroutil	Kroutil	k1gMnSc1
–	–	k?
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
altklarinet	altklarinet	k1gInSc1
</s>
<s>
Michal	Michal	k1gMnSc1
Zpěvák	Zpěvák	k1gMnSc1
–	–	k?
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
sopransaxofon	sopransaxofon	k1gInSc1
<g/>
,	,	kIx,
tarogato	tarogato	k6eAd1
-	-	kIx~
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
Melody	Meloda	k1gFnSc2
Makers	Makersa	k1gFnPc2
do	do	k7c2
OPSO	OPSO	kA
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
–	–	k?
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
basklarinet	basklarinet	k1gInSc1
</s>
<s>
Jakub	Jakub	k1gMnSc1
Šnýdl	Šnýdl	k1gMnSc1
–	–	k?
klarinet	klarinet	k1gInSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Tříska	Tříska	k1gMnSc1
–	–	k?
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
big	big	k?
bandu	band	k1gInSc2
Ondřej	Ondřej	k1gMnSc1
Havelka	Havelka	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
Melody	Meloda	k1gFnPc1
Makers	Makersa	k1gFnPc2
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Dobrohruška	Dobrohruška	k1gMnSc1
–	–	k?
banjo	banjo	k1gNnSc4
</s>
<s>
Petr	Petr	k1gMnSc1
Tichý	Tichý	k1gMnSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Šturma	Šturma	k1gMnSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Wajsar	Wajsar	k1gMnSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Šícha	Šícha	k1gMnSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Rejhon	Rejhon	k1gMnSc1
-	-	kIx~
kytara	kytara	k1gFnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Šturma	Šturma	k1gMnSc1
–	–	k?
kontrabas	kontrabas	k1gInSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Landa	Landa	k1gMnSc1
–	–	k?
kontrabas	kontrabas	k1gInSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Balcar	Balcar	k1gMnSc1
–	–	k?
kontrabas	kontrabas	k1gInSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Zpěvák	Zpěvák	k1gMnSc1
–	–	k?
kontrabas	kontrabas	k1gInSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
-	-	kIx~
kontrabas	kontrabas	k1gInSc1
<g/>
,	,	kIx,
suzafon	suzafon	k1gInSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Gilík	Gilík	k1gMnSc1
–	–	k?
piano	piano	k6eAd1
</s>
<s>
Petra	Petra	k1gFnSc1
Ernyeiová	Ernyeiový	k2eAgFnSc1d1
</s>
<s>
Alice	Alice	k1gFnSc1
Bauer	Bauer	k1gMnSc1
–	–	k?
zpěvačka	zpěvačka	k1gFnSc1
</s>
<s>
Adéla	Adéla	k1gFnSc1
Zejfartová	Zejfartový	k2eAgFnSc1d1
–	–	k?
zpěvačka	zpěvačka	k1gFnSc1
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
LP	LP	kA
/	/	kIx~
CD	CD	kA
</s>
<s>
Originální	originální	k2eAgInSc1d1
Pražský	pražský	k2eAgInSc1d1
Synkopický	synkopický	k2eAgInSc1d1
Orchestr	orchestr	k1gInSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
,	,	kIx,
SP	SP	kA
Panton	Panton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ragtime	ragtime	k1gInSc1
-	-	kIx~
2	#num#	k4
skladby	skladba	k1gFnSc2
na	na	k7c4
LP	LP	kA
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
,	,	kIx,
LP	LP	kA
Supraphon	supraphon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Original	Originat	k5eAaImAgInS,k5eAaPmAgInS
Prague	Prague	k1gInSc1
Syncopated	Syncopated	k1gInSc1
Orchestra	orchestra	k1gFnSc1
at	at	k?
Breda	Breda	k1gFnSc1
Jazz	jazz	k1gInSc1
Festival	festival	k1gInSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
,	,	kIx,
LP	LP	kA
Jazz	jazz	k1gInSc1
Crooner	Crooner	k1gInSc1
<g/>
,	,	kIx,
Holland	Holland	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Originální	originální	k2eAgInSc1d1
Pražský	pražský	k2eAgInSc1d1
Synkopický	synkopický	k2eAgInSc1d1
Orchestr	orchestr	k1gInSc1
-	-	kIx~
první	první	k4xOgNnSc4
album	album	k1gNnSc4
s	s	k7c7
Havelkou	havelka	k1gFnSc7
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
,	,	kIx,
LP	LP	kA
Supraphon	supraphon	k1gInSc1
&	&	k?
WAM	WAM	kA
–	–	k?
Germany	German	k1gMnPc7
<g/>
)	)	kIx)
</s>
<s>
Srdce	srdce	k1gNnSc4
Mé	můj	k3xOp1gNnSc1
Odešlo	odejít	k5eAaPmAgNnS
Za	za	k7c4
Tebou	ty	k3xPp2nSc7
-	-	kIx~
2	#num#	k4
skladby	skladba	k1gFnPc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
,	,	kIx,
SP	SP	kA
Panton	Panton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Stará	starý	k2eAgFnSc1d1
Natoč	natočit	k5eAaBmRp2nS
Gramofon	gramofon	k1gInSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
,	,	kIx,
LP	LP	kA
Panton	Panton	k1gInSc1
+	+	kIx~
reed	reed	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CD	CD	kA
<g/>
)	)	kIx)
</s>
<s>
Jazz	jazz	k1gInSc1
&	&	k?
Hot	hot	k0
Dance	Danka	k1gFnSc6
Music	Musice	k1gInPc2
1923	#num#	k4
-	-	kIx~
31	#num#	k4
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
,	,	kIx,
LP	LP	kA
Panton	Panton	k1gInSc1
+	+	kIx~
reed	reed	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CD	CD	kA
<g/>
)	)	kIx)
</s>
<s>
Sám	sám	k3xTgMnSc1
s	s	k7c7
Děvčetem	děvče	k1gNnSc7
v	v	k7c6
Dešti	dešť	k1gInSc6
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
,	,	kIx,
LP	LP	kA
Panton	Panton	k1gInSc1
+	+	kIx~
reed	reed	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CD	CD	kA
<g/>
)	)	kIx)
</s>
<s>
Hello	Hello	k1gNnSc1
Baby	baba	k1gFnSc2
-	-	kIx~
poslední	poslední	k2eAgNnSc1d1
album	album	k1gNnSc1
s	s	k7c7
Havelkou	havelka	k1gFnSc7
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
,	,	kIx,
CD	CD	kA
EMI	EMI	kA
–	–	k?
Monitor	monitor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Walking	Walking	k1gInSc1
and	and	k?
Swinging	Swinging	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
,	,	kIx,
CD	CD	kA
EMI	EMI	kA
–	–	k?
Monitor	monitor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Blues	blues	k1gNnSc1
pro	pro	k7c4
Tebe	ty	k3xPp2nSc4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
,	,	kIx,
CD	CD	kA
EMI	EMI	kA
–	–	k?
Monitor	monitor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Goin	Goin	k1gNnSc1
<g/>
´	´	k?
Crazy	Craza	k1gFnPc1
with	with	k1gInSc4
The	The	k1gFnSc2
Blues	blues	k1gNnSc2
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
,	,	kIx,
CD	CD	kA
BWS	BWS	kA
<g/>
)	)	kIx)
</s>
<s>
Sweet	Sweet	k1gInSc1
Like	Lik	k1gFnSc2
This	Thisa	k1gFnPc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
,	,	kIx,
CD	CD	kA
BWS	BWS	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Stránky	stránka	k1gFnPc1
na	na	k7c4
Bandzone	Bandzon	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Klip	klip	k1gInSc1
OPSO	OPSO	kA
<g/>
,	,	kIx,
He	he	k0
<g/>
'	'	kIx"
<g/>
s	s	k7c7
The	The	k1gFnSc7
Last	Last	k1gMnSc1
Word	Word	kA
<g/>
,	,	kIx,
kveten	kveten	k2eAgMnSc1d1
(	(	kIx(
<g/>
may	may	k?
<g/>
)	)	kIx)
1985	#num#	k4
</s>
<s>
Klip	klip	k1gInSc1
Flašinetář	flašinetář	k1gMnSc1
</s>
<s>
Klip	klip	k1gInSc1
From	Froma	k1gFnPc2
Monday	Mondaa	k1gFnSc2
On	on	k3xPp3gMnSc1
</s>
<s>
záznam	záznam	k1gInSc1
z	z	k7c2
koncertu	koncert	k1gInSc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2003171959	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2182	#num#	k4
7873	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82065533	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
149712174	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82065533	#num#	k4
</s>
