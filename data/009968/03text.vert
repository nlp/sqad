<p>
<s>
Hora	hora	k1gFnSc1	hora
Denali	Denali	k1gFnSc2	Denali
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Mount	Mount	k1gInSc1	Mount
McKinley	McKinlea	k1gFnSc2	McKinlea
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
6190	[number]	k4	6190
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Denali	Denali	k1gFnSc2	Denali
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hora	hora	k1gFnSc1	hora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
pohoří	pohořet	k5eAaPmIp3nP	pohořet
Aljašské	aljašský	k2eAgFnPc1d1	aljašská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
centrální	centrální	k2eAgFnSc2d1	centrální
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
210	[number]	k4	210
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Anchorage	Anchorag	k1gInSc2	Anchorag
a	a	k8xC	a
250	[number]	k4	250
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Fairbanks	Fairbanksa	k1gFnPc2	Fairbanksa
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
leží	ležet	k5eAaImIp3nS	ležet
přibližně	přibližně	k6eAd1	přibližně
56	[number]	k4	56
km	km	kA	km
od	od	k7c2	od
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
vrcholu	vrchol	k1gInSc2	vrchol
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
pomocí	pomocí	k7c2	pomocí
fotogrammetrie	fotogrammetrie	k1gFnSc2	fotogrammetrie
určena	určit	k5eAaPmNgFnS	určit
na	na	k7c4	na
6194	[number]	k4	6194
m	m	kA	m
(	(	kIx(	(
<g/>
20	[number]	k4	20
320	[number]	k4	320
stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
americká	americký	k2eAgFnSc1d1	americká
vládní	vládní	k2eAgFnSc1d1	vládní
agentura	agentura	k1gFnSc1	agentura
United	United	k1gInSc1	United
States	States	k1gInSc1	States
Geological	Geological	k1gMnSc2	Geological
Survey	Survea	k1gMnSc2	Survea
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nových	nový	k2eAgNnPc2d1	nové
měření	měření	k1gNnSc2	měření
pomocí	pomocí	k7c2	pomocí
GPS	GPS	kA	GPS
upřesnila	upřesnit	k5eAaPmAgFnS	upřesnit
výšku	výška	k1gFnSc4	výška
hory	hora	k1gFnSc2	hora
na	na	k7c4	na
6190	[number]	k4	6190
m	m	kA	m
(	(	kIx(	(
<g/>
20	[number]	k4	20
310	[number]	k4	310
stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Denali	Denali	k1gFnSc1	Denali
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
hmotou	hmota	k1gFnSc7	hmota
a	a	k8xC	a
vyzdvižením	vyzdvižení	k1gNnSc7	vyzdvižení
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
vrchol	vrchol	k1gInSc1	vrchol
Everestu	Everest	k1gInSc2	Everest
leží	ležet	k5eAaImIp3nS	ležet
o	o	k7c4	o
2700	[number]	k4	2700
metrů	metr	k1gInPc2	metr
výš	vysoce	k6eAd2	vysoce
od	od	k7c2	od
hladiny	hladina	k1gFnSc2	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
hora	hora	k1gFnSc1	hora
samotná	samotný	k2eAgFnSc1d1	samotná
spočívá	spočívat	k5eAaImIp3nS	spočívat
na	na	k7c6	na
tibetské	tibetský	k2eAgFnSc6d1	tibetská
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
5200	[number]	k4	5200
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
přečnívá	přečnívat	k5eAaImIp3nS	přečnívat
jen	jen	k9	jen
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
3650	[number]	k4	3650
m.	m.	k?	m.
Úpatí	úpatí	k1gNnSc4	úpatí
hory	hora	k1gFnSc2	hora
Denali	Denali	k1gFnPc2	Denali
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
o	o	k7c6	o
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
zhruba	zhruba	k6eAd1	zhruba
600	[number]	k4	600
m	m	kA	m
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
o	o	k7c4	o
5500	[number]	k4	5500
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
zeměpisné	zeměpisný	k2eAgFnSc3d1	zeměpisná
šířce	šířka	k1gFnSc3	šířka
proslulá	proslulý	k2eAgFnSc1d1	proslulá
nesmírně	smírně	k6eNd1	smírně
chladným	chladný	k2eAgNnSc7d1	chladné
počasím	počasí	k1gNnSc7	počasí
a	a	k8xC	a
také	také	k9	také
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
výskytu	výskyt	k1gInSc2	výskyt
výškové	výškový	k2eAgFnSc2d1	výšková
nemoci	nemoc	k1gFnSc2	nemoc
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hora	hora	k1gFnSc1	hora
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
vrcholu	vrchol	k1gInSc6	vrchol
47	[number]	k4	47
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
oproti	oproti	k7c3	oproti
hladině	hladina	k1gFnSc3	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Denali	Denali	k1gFnSc1	Denali
má	mít	k5eAaImIp3nS	mít
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
zeměpisné	zeměpisný	k2eAgFnSc3d1	zeměpisná
šířce	šířka	k1gFnSc3	šířka
kyslíku	kyslík	k1gInSc2	kyslík
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
42	[number]	k4	42
%	%	kIx~	%
oproti	oproti	k7c3	oproti
hladině	hladina	k1gFnSc3	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mocnost	mocnost	k1gFnSc1	mocnost
troposféry	troposféra	k1gFnSc2	troposféra
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
rotací	rotace	k1gFnSc7	rotace
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
je	být	k5eAaImIp3nS	být
až	až	k9	až
19	[number]	k4	19
km	km	kA	km
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
jen	jen	k9	jen
9	[number]	k4	9
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
hory	hora	k1gFnSc2	hora
==	==	k?	==
</s>
</p>
<p>
<s>
Denali	Denat	k5eAaImAgMnP	Denat
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
významné	významný	k2eAgInPc4d1	významný
vrcholy	vrchol	k1gInPc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
je	být	k5eAaImIp3nS	být
Jižní	jižní	k2eAgInSc1d1	jižní
vrchol	vrchol	k1gInSc1	vrchol
<g/>
,	,	kIx,	,
Severní	severní	k2eAgInSc1d1	severní
vrchol	vrchol	k1gInSc1	vrchol
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
5934	[number]	k4	5934
m	m	kA	m
a	a	k8xC	a
nad	nad	k7c4	nad
ostatní	ostatní	k2eAgFnSc4d1	ostatní
hmotu	hmota	k1gFnSc4	hmota
hory	hora	k1gFnSc2	hora
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pouhými	pouhý	k2eAgInPc7d1	pouhý
402	[number]	k4	402
m.	m.	k?	m.
Severní	severní	k2eAgInSc4d1	severní
vrchol	vrchol	k1gInSc4	vrchol
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Seznamu	seznam	k1gInSc6	seznam
fourteeners	fourteeners	k6eAd1	fourteeners
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
bývá	bývat	k5eAaImIp3nS	bývat
cílem	cíl	k1gInSc7	cíl
výstupů	výstup	k1gInPc2	výstup
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
horolezci	horolezec	k1gMnPc1	horolezec
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
pro	pro	k7c4	pro
cestu	cesta	k1gFnSc4	cesta
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
svahu	svah	k1gInSc6	svah
masívu	masív	k1gInSc2	masív
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
svazích	svah	k1gInPc6	svah
hory	hora	k1gFnSc2	hora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pět	pět	k4xCc1	pět
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
úbočí	úbočí	k1gNnSc6	úbočí
masivu	masiv	k1gInSc2	masiv
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Peters	Peters	k1gInSc1	Peters
Glacier	Glacier	k1gInSc4	Glacier
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Muldrow	Muldrow	k1gMnSc1	Muldrow
Glacier	Glacier	k1gMnSc1	Glacier
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
ledovce	ledovec	k1gInSc2	ledovec
Muldrow	Muldrow	k1gFnSc2	Muldrow
<g/>
,	,	kIx,	,
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
východním	východní	k2eAgInSc7d1	východní
svahem	svah	k1gInSc7	svah
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
Traileka	Traileka	k1gMnSc1	Traileka
Glacier	Glacier	k1gMnSc1	Glacier
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
bychom	by	kYmCp1nP	by
nalezli	nalézt	k5eAaBmAgMnP	nalézt
Ruth	Ruth	k1gFnSc4	Ruth
Glacier	Glacira	k1gFnPc2	Glacira
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
Kahiltna	Kahiltno	k1gNnSc2	Kahiltno
Glacier	Glacira	k1gFnPc2	Glacira
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
k	k	k7c3	k
jihozápadní	jihozápadní	k2eAgFnSc3d1	jihozápadní
straně	strana	k1gFnSc3	strana
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
hory	hora	k1gFnSc2	hora
==	==	k?	==
</s>
</p>
<p>
<s>
Denali	Denat	k5eAaPmAgMnP	Denat
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
athabaském	athabaský	k2eAgInSc6d1	athabaský
jazyce	jazyk	k1gInSc6	jazyk
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
byla	být	k5eAaImAgFnS	být
hora	hora	k1gFnSc1	hora
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
"	"	kIx"	"
<g/>
Mount	Mount	k1gMnSc1	Mount
McKinley	McKinlea	k1gFnSc2	McKinlea
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
amerického	americký	k2eAgMnSc2d1	americký
prezidentského	prezidentský	k2eAgMnSc2d1	prezidentský
kandidáta	kandidát	k1gMnSc2	kandidát
a	a	k8xC	a
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
prezidenta	prezident	k1gMnSc2	prezident
Williama	William	k1gMnSc2	William
McKinleyho	McKinley	k1gMnSc2	McKinley
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
pak	pak	k6eAd1	pak
nesla	nést	k5eAaImAgFnS	nést
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
desetiletích	desetiletí	k1gNnPc6	desetiletí
však	však	k9	však
aktivisté	aktivista	k1gMnPc1	aktivista
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
indiánských	indiánský	k2eAgNnPc2d1	indiánské
práv	právo	k1gNnPc2	právo
začali	začít	k5eAaPmAgMnP	začít
toto	tento	k3xDgNnSc4	tento
přejmenování	přejmenování	k1gNnSc4	přejmenování
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
neuctivé	uctivý	k2eNgNnSc4d1	neuctivé
a	a	k8xC	a
poplatné	poplatný	k2eAgNnSc4d1	poplatné
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
<g/>
.	.	kIx.	.
</s>
<s>
Aljašská	aljašský	k2eAgFnSc1d1	aljašská
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
geografické	geografický	k2eAgInPc4d1	geografický
názvy	název	k1gInPc4	název
(	(	kIx(	(
<g/>
Alaska	Alaska	k1gFnSc1	Alaska
Board	Board	k1gMnSc1	Board
of	of	k?	of
Geographic	Geographic	k1gMnSc1	Geographic
Names	Names	k1gMnSc1	Names
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
změnila	změnit	k5eAaPmAgFnS	změnit
jméno	jméno	k1gNnSc4	jméno
hory	hora	k1gFnSc2	hora
na	na	k7c6	na
lokální	lokální	k2eAgFnSc6d1	lokální
úrovni	úroveň	k1gFnSc6	úroveň
opět	opět	k6eAd1	opět
na	na	k7c4	na
"	"	kIx"	"
<g/>
Denali	Denali	k1gFnSc4	Denali
<g/>
"	"	kIx"	"
a	a	k8xC	a
zažádala	zažádat	k5eAaPmAgFnS	zažádat
o	o	k7c4	o
stejnou	stejný	k2eAgFnSc4d1	stejná
změnu	změna	k1gFnSc4	změna
i	i	k9	i
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
oblasti	oblast	k1gFnSc2	oblast
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
kolem	kolem	k7c2	kolem
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
změnil	změnit	k5eAaPmAgInS	změnit
jméno	jméno	k1gNnSc4	jméno
z	z	k7c2	z
"	"	kIx"	"
<g/>
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Mount	Mount	k1gInSc1	Mount
McKinley	McKinley	k1gInPc1	McKinley
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mount	Mount	k1gMnSc1	Mount
McKinley	McKinlea	k1gFnSc2	McKinlea
National	National	k1gMnSc1	National
Park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
na	na	k7c4	na
"	"	kIx"	"
<g/>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Denali	Denali	k1gFnSc1	Denali
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Denali	Denali	k1gFnSc1	Denali
National	National	k1gMnSc1	National
Park	park	k1gInSc1	park
and	and	k?	and
Preserve	Preserev	k1gFnSc2	Preserev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
hory	hora	k1gFnSc2	hora
však	však	k9	však
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
na	na	k7c6	na
federálních	federální	k2eAgFnPc6d1	federální
mapách	mapa	k1gFnPc6	mapa
stále	stále	k6eAd1	stále
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
oznámila	oznámit	k5eAaPmAgFnS	oznámit
administrativa	administrativa	k1gFnSc1	administrativa
prezidenta	prezident	k1gMnSc2	prezident
Baracka	Baracka	k1gFnSc1	Baracka
Obamy	Obama	k1gFnPc1	Obama
<g/>
,	,	kIx,	,
že	že	k8xS	že
hora	hora	k1gFnSc1	hora
ponese	ponést	k5eAaPmIp3nS	ponést
opět	opět	k6eAd1	opět
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
letech	let	k1gInPc6	let
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
žádosti	žádost	k1gFnSc3	žádost
aljašské	aljašský	k2eAgFnSc2d1	aljašská
komise	komise	k1gFnSc2	komise
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
ruské	ruský	k2eAgFnSc2d1	ruská
kolonizace	kolonizace	k1gFnSc2	kolonizace
Aljašky	Aljaška	k1gFnSc2	Aljaška
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
nazývána	nazývat	k5eAaImNgFnS	nazývat
prostě	prostě	k6eAd1	prostě
Bolšaja	Bolšaj	k2eAgFnSc1d1	Bolšaja
Gora	Gora	k1gFnSc1	Gora
(	(	kIx(	(
<g/>
Б	Б	k?	Б
Г	Г	k?	Г
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Velká	velký	k2eAgFnSc1d1	velká
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
výstupů	výstup	k1gInPc2	výstup
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
Denali	Denali	k1gFnSc6	Denali
učinil	učinit	k5eAaPmAgMnS	učinit
soudce	soudce	k1gMnSc1	soudce
James	James	k1gMnSc1	James
Wickersham	Wickersham	k1gInSc4	Wickersham
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
cestou	cesta	k1gFnSc7	cesta
přes	přes	k7c4	přes
ledovec	ledovec	k1gInSc4	ledovec
Peters	Peters	k1gInSc4	Peters
Glacier	Glaciero	k1gNnPc2	Glaciero
a	a	k8xC	a
Severní	severní	k2eAgFnSc4d1	severní
stěnu	stěna	k1gFnSc4	stěna
(	(	kIx(	(
<g/>
North	North	k1gInSc1	North
Face	Fac	k1gInSc2	Fac
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xS	jako
Wickershamova	Wickershamův	k2eAgFnSc1d1	Wickershamův
stěna	stěna	k1gFnSc1	stěna
(	(	kIx(	(
<g/>
Wickersham	Wickersham	k1gInSc1	Wickersham
Wall	Wall	k1gInSc1	Wall
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
však	však	k9	však
vysoce	vysoce	k6eAd1	vysoce
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
kvůli	kvůli	k7c3	kvůli
množství	množství	k1gNnSc3	množství
lavin	lavina	k1gFnPc2	lavina
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
ji	on	k3xPp3gFnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nepřekonal	překonat	k5eNaPmAgMnS	překonat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
o	o	k7c6	o
prvovýstupu	prvovýstup	k1gInSc6	prvovýstup
objevitel	objevitel	k1gMnSc1	objevitel
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Frederick	Frederick	k1gMnSc1	Frederick
Cook	Cook	k1gMnSc1	Cook
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
prohlášení	prohlášení	k1gNnSc1	prohlášení
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
přijímáno	přijímán	k2eAgNnSc1d1	přijímáno
s	s	k7c7	s
podezřením	podezření	k1gNnSc7	podezření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
pravdivé	pravdivý	k2eAgNnSc4d1	pravdivé
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
podvod	podvod	k1gInSc4	podvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
nedostatek	nedostatek	k1gInSc4	nedostatek
horolezeckých	horolezecký	k2eAgFnPc2d1	horolezecká
zkušeností	zkušenost	k1gFnPc2	zkušenost
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
vrcholu	vrchol	k1gInSc2	vrchol
Denali	Denali	k1gFnSc2	Denali
pokusila	pokusit	k5eAaPmAgFnS	pokusit
skupinka	skupinka	k1gFnSc1	skupinka
čtyř	čtyři	k4xCgInPc2	čtyři
místních	místní	k2eAgMnPc2d1	místní
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
expedice	expedice	k1gFnSc1	expedice
Sourdough	Sourdough	k1gMnSc1	Sourdough
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
sourdough	sourdough	k1gInSc1	sourdough
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
kvásek	kvásek	k1gInSc1	kvásek
<g/>
"	"	kIx"	"
–	–	k?	–
přeneseně	přeneseně	k6eAd1	přeneseně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zlatokopy	zlatokop	k1gMnPc4	zlatokop
a	a	k8xC	a
zkušené	zkušený	k2eAgMnPc4d1	zkušený
obyvatele	obyvatel	k1gMnPc4	obyvatel
Aljašky	Aljaška	k1gFnSc2	Aljaška
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
kvásku	kvásek	k1gInSc3	kvásek
používanému	používaný	k2eAgInSc3d1	používaný
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
chleba	chléb	k1gInSc2	chléb
ve	v	k7c6	v
zlatokopeckých	zlatokopecký	k2eAgInPc6d1	zlatokopecký
táborech	tábor	k1gInPc6	tábor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
strávili	strávit	k5eAaPmAgMnP	strávit
zhruba	zhruba	k6eAd1	zhruba
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
konečně	konečně	k6eAd1	konečně
vyšplhali	vyšplhat	k5eAaPmAgMnP	vyšplhat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
působivým	působivý	k2eAgInSc7d1	působivý
zážitkem	zážitek	k1gInSc7	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
sebou	se	k3xPyFc7	se
si	se	k3xPyFc3	se
kromě	kromě	k7c2	kromě
pytlíku	pytlík	k1gInSc2	pytlík
koblih	kobliha	k1gFnPc2	kobliha
a	a	k8xC	a
termosky	termoska	k1gFnSc2	termoska
kakaa	kakao	k1gNnSc2	kakao
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
nesli	nést	k5eAaImAgMnP	nést
přes	přes	k7c4	přes
čtyři	čtyři	k4xCgInPc4	čtyři
metry	metr	k1gInPc4	metr
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
jedlový	jedlový	k2eAgInSc4d1	jedlový
kůl	kůl	k1gInSc4	kůl
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc4	dva
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
výpravy	výprava	k1gFnSc2	výprava
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
nižší	nízký	k2eAgInSc4d2	nižší
<g/>
,	,	kIx,	,
Severní	severní	k2eAgInSc4d1	severní
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tento	tento	k3xDgInSc4	tento
kůl	kůl	k1gInSc4	kůl
vztyčili	vztyčit	k5eAaPmAgMnP	vztyčit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
tvrzení	tvrzení	k1gNnSc2	tvrzení
jim	on	k3xPp3gMnPc3	on
výstup	výstup	k1gInSc1	výstup
zabral	zabrat	k5eAaPmAgInS	zabrat
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rekord	rekord	k1gInSc4	rekord
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgInS	být
překonán	překonat	k5eAaPmNgInS	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
neuvěřil	uvěřit	k5eNaPmAgInS	uvěřit
(	(	kIx(	(
<g/>
zčásti	zčásti	k6eAd1	zčásti
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
nepravdivým	pravdivý	k2eNgNnPc3d1	nepravdivé
tvrzením	tvrzení	k1gNnPc3	tvrzení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobyli	dobýt	k5eAaPmAgMnP	dobýt
oba	dva	k4xCgInPc4	dva
vrcholy	vrchol	k1gInPc4	vrchol
<g/>
)	)	kIx)	)
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Denali	Denali	k1gFnSc4	Denali
poprvé	poprvé	k6eAd1	poprvé
opravdu	opravdu	k6eAd1	opravdu
slezen	slezen	k2eAgInSc1d1	slezen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
výprava	výprava	k1gFnSc1	výprava
Parker-Browne	Parker-Brown	k1gInSc5	Parker-Brown
horu	hora	k1gFnSc4	hora
téměř	téměř	k6eAd1	téměř
dobyla	dobýt	k5eAaPmAgFnS	dobýt
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
byla	být	k5eAaImAgFnS	být
nucena	nucen	k2eAgFnSc1d1	nucena
ustoupit	ustoupit	k5eAaPmF	ustoupit
pro	pro	k7c4	pro
silně	silně	k6eAd1	silně
nepříznivé	příznivý	k2eNgNnSc4d1	nepříznivé
počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíš	nejspíš	k9	nejspíš
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
zachránili	zachránit	k5eAaPmAgMnP	zachránit
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgNnSc6	který
stoupali	stoupat	k5eAaImAgMnP	stoupat
<g/>
,	,	kIx,	,
otřáslo	otřást	k5eAaPmAgNnS	otřást
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
poté	poté	k6eAd1	poté
silné	silný	k2eAgNnSc4d1	silné
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
vrchol	vrchol	k1gInSc4	vrchol
Denali	Denali	k1gFnPc2	Denali
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zdařil	zdařit	k5eAaPmAgInS	zdařit
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1913	[number]	k4	1913
výpravě	výprava	k1gFnSc6	výprava
vedené	vedený	k2eAgFnSc6d1	vedená
Hudsonem	Hudson	k1gMnSc7	Hudson
Stuckem	Stuck	k1gInSc7	Stuck
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
octl	octnout	k5eAaPmAgMnS	octnout
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Walter	Walter	k1gMnSc1	Walter
Harper	Harper	k1gMnSc1	Harper
<g/>
,	,	kIx,	,
rodilý	rodilý	k2eAgMnSc1d1	rodilý
Aljaščan	Aljaščan	k1gMnSc1	Aljaščan
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
výpravy	výprava	k1gFnSc2	výprava
byli	být	k5eAaImAgMnP	být
ještě	ještě	k6eAd1	ještě
Harry	Harra	k1gFnPc4	Harra
Karstens	Karstensa	k1gFnPc2	Karstensa
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Tatum	Tatum	k?	Tatum
<g/>
.	.	kIx.	.
</s>
<s>
Posledně	posledně	k6eAd1	posledně
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rozhled	rozhled	k1gInSc1	rozhled
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
Denali	Denali	k1gFnSc2	Denali
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
dívat	dívat	k5eAaImF	dívat
se	se	k3xPyFc4	se
z	z	k7c2	z
nebeských	nebeský	k2eAgNnPc2d1	nebeské
oken	okno	k1gNnPc2	okno
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
K	k	k7c3	k
výstupu	výstup	k1gInSc3	výstup
zvolili	zvolit	k5eAaPmAgMnP	zvolit
cestu	cesta	k1gFnSc4	cesta
přes	přes	k7c4	přes
Muldrow	Muldrow	k1gFnSc4	Muldrow
Glacier	Glaciero	k1gNnPc2	Glaciero
<g/>
,	,	kIx,	,
prozkoumanou	prozkoumaný	k2eAgFnSc7d1	prozkoumaná
již	již	k6eAd1	již
předešlými	předešlý	k2eAgFnPc7d1	předešlá
výpravami	výprava	k1gFnPc7	výprava
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudson	Hudson	k1gMnSc1	Hudson
Stuck	Stuck	k1gMnSc1	Stuck
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
poblíž	poblíž	k7c2	poblíž
Severního	severní	k2eAgInSc2d1	severní
vrcholu	vrchol	k1gInSc2	vrchol
spatřil	spatřit	k5eAaPmAgMnS	spatřit
pomocí	pomocí	k7c2	pomocí
dalekohledu	dalekohled	k1gInSc2	dalekohled
vysoký	vysoký	k2eAgInSc4d1	vysoký
kůl	kůl	k1gInSc4	kůl
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
expedice	expedice	k1gFnSc1	expedice
Sourdough	Sourdougha	k1gFnPc2	Sourdougha
Severní	severní	k2eAgInSc1d1	severní
vrchol	vrchol	k1gInSc1	vrchol
skutečně	skutečně	k6eAd1	skutečně
dobyla	dobýt	k5eAaPmAgNnP	dobýt
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
počin	počin	k1gInSc1	počin
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
dnes	dnes	k6eAd1	dnes
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
pravdivý	pravdivý	k2eAgInSc4d1	pravdivý
<g/>
.	.	kIx.	.
</s>
<s>
Kůl	kůl	k1gInSc1	kůl
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
viděn	vidět	k5eAaImNgInS	vidět
nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
záležitosti	záležitost	k1gFnSc2	záležitost
stále	stále	k6eAd1	stále
vnáší	vnášet	k5eAaImIp3nP	vnášet
určité	určitý	k2eAgFnPc4d1	určitá
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stuck	Stuck	k1gMnSc1	Stuck
také	také	k9	také
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výprava	výprava	k1gFnSc1	výprava
Parker	Parkra	k1gFnPc2	Parkra
–	–	k?	–
Browne	Brown	k1gInSc5	Brown
byla	být	k5eAaImAgNnP	být
pouhých	pouhý	k2eAgInPc2d1	pouhý
60	[number]	k4	60
m	m	kA	m
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Denali	Denat	k5eAaBmAgMnP	Denat
je	být	k5eAaImIp3nS	být
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
cílem	cíl	k1gInSc7	cíl
horolezeckých	horolezecký	k2eAgFnPc2d1	horolezecká
výprav	výprava	k1gFnPc2	výprava
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
podnikem	podnik	k1gInSc7	podnik
–	–	k?	–
pouze	pouze	k6eAd1	pouze
něco	něco	k6eAd1	něco
málo	málo	k6eAd1	málo
přes	přes	k7c4	přes
polovinu	polovina	k1gFnSc4	polovina
výprav	výprava	k1gFnPc2	výprava
je	být	k5eAaImIp3nS	být
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
si	se	k3xPyFc3	se
hora	hora	k1gFnSc1	hora
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
téměř	téměř	k6eAd1	téměř
stovku	stovka	k1gFnSc4	stovka
lezeckých	lezecký	k2eAgInPc2d1	lezecký
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
horolezců	horolezec	k1gMnPc2	horolezec
volí	volit	k5eAaImIp3nS	volit
jako	jako	k9	jako
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Západní	západní	k2eAgInSc4d1	západní
pilíř	pilíř	k1gInSc4	pilíř
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
slezený	slezený	k2eAgMnSc1d1	slezený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
Bradfordem	Bradford	k1gInSc7	Bradford
Washburnem	Washburn	k1gInSc7	Washburn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
před	před	k7c7	před
tím	ten	k3xDgInSc7	ten
horu	hora	k1gFnSc4	hora
zevrubně	zevrubně	k6eAd1	zevrubně
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
pomocí	pomocí	k7c2	pomocí
leteckých	letecký	k2eAgFnPc2d1	letecká
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
většinou	většinou	k6eAd1	většinou
zabere	zabrat	k5eAaPmIp3nS	zabrat
dva	dva	k4xCgInPc4	dva
až	až	k6eAd1	až
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstupy	výstup	k1gInPc4	výstup
českých	český	k2eAgMnPc2d1	český
horolezců	horolezec	k1gMnPc2	horolezec
==	==	k?	==
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
-	-	kIx~	-
Martin	Martin	k1gMnSc1	Martin
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
,	,	kIx,	,
Cassinův	Cassinův	k2eAgMnSc1d1	Cassinův
pilíř	pilíř	k1gInSc4	pilíř
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
Soňa	Soňa	k1gFnSc1	Soňa
Boštíková	Boštíkový	k2eAgFnSc1d1	Boštíkový
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
Češka	Češka	k1gFnSc1	Češka
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hrad	hrad	k1gInSc1	hrad
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Matiášek	Matiášek	k1gMnSc1	Matiášek
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
Lízner	Lízner	k1gMnSc1	Lízner
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
–	–	k?	–
Hynek	Hynek	k1gMnSc1	Hynek
Tampier	Tampier	k1gMnSc1	Tampier
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Rozmánek	Rozmánek	k1gMnSc1	Rozmánek
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Knill	Knill	k1gMnSc1	Knill
<g/>
,	,	kIx,	,
Víťa	Víťa	k?	Víťa
Krichl	Krichl	k1gMnSc1	Krichl
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Právec	Právec	k1gMnSc1	Právec
</s>
</p>
<p>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
–	–	k?	–
Radek	Radek	k1gMnSc1	Radek
Jaroš	Jaroš	k1gMnSc1	Jaroš
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Mašek	Mašek	k1gMnSc1	Mašek
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
–	–	k?	–
Leopold	Leopold	k1gMnSc1	Leopold
Sulovský	Sulovský	k1gMnSc1	Sulovský
<g/>
,	,	kIx,	,
Radim	Radim	k1gMnSc1	Radim
Slíva	Slíva	k1gMnSc1	Slíva
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Zedníček	Zedníček	k1gMnSc1	Zedníček
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Denali	Denali	k1gFnSc2	Denali
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
