<p>
<s>
Sauron	Sauron	k1gInSc1	Sauron
(	(	kIx(	(
<g/>
quenijsky	quenijsky	k6eAd1	quenijsky
"	"	kIx"	"
<g/>
Nenáviděný	nenáviděný	k2eAgMnSc1d1	nenáviděný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
literární	literární	k2eAgFnSc1d1	literární
postava	postava	k1gFnSc1	postava
ve	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
příbězích	příběh	k1gInPc6	příběh
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
Středozemě	Středozem	k1gFnSc2	Středozem
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
zápornou	záporný	k2eAgFnSc7d1	záporná
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
trilogii	trilogie	k1gFnSc6	trilogie
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
byl	být	k5eAaImAgMnS	být
Temným	temný	k2eAgMnSc7d1	temný
pánem	pán	k1gMnSc7	pán
<g/>
,	,	kIx,	,
vládcem	vládce	k1gMnSc7	vládce
Mordoru	Mordor	k1gInSc2	Mordor
a	a	k8xC	a
tvůrcem	tvůrce	k1gMnSc7	tvůrce
Jednoho	jeden	k4xCgInSc2	jeden
prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
je	být	k5eAaImIp3nS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Nekromant	nekromant	k1gMnSc1	nekromant
<g/>
,	,	kIx,	,
Černý	Černý	k1gMnSc1	Černý
mág	mág	k1gMnSc1	mág
a	a	k8xC	a
Černokněžník	černokněžník	k1gMnSc1	černokněžník
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Hobit	hobit	k1gMnSc1	hobit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Silmarillionu	Silmarillion	k1gInSc6	Silmarillion
(	(	kIx(	(
<g/>
vydaném	vydaný	k2eAgInSc6d1	vydaný
po	po	k7c6	po
Pánu	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sauron	Sauron	k1gMnSc1	Sauron
byl	být	k5eAaImAgMnS	být
pobočníkem	pobočník	k1gMnSc7	pobočník
prvního	první	k4xOgMnSc2	první
Temného	temný	k2eAgMnSc2d1	temný
pána	pán	k1gMnSc2	pán
Morgotha	Morgoth	k1gMnSc2	Morgoth
a	a	k8xC	a
největším	veliký	k2eAgMnPc3d3	veliký
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
služebníků	služebník	k1gMnPc2	služebník
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
andělské	andělský	k2eAgFnPc1d1	Andělská
mocnosti	mocnost	k1gFnPc1	mocnost
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
mýtu	mýtus	k1gInSc6	mýtus
"	"	kIx"	"
<g/>
byly	být	k5eAaImAgInP	být
schopny	schopen	k2eAgInPc1d1	schopen
mnoha	mnoho	k4c2	mnoho
stupňů	stupeň	k1gInPc2	stupeň
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
selhání	selhání	k1gNnSc4	selhání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
absolutní	absolutní	k2eAgFnSc1d1	absolutní
satanistická	satanistický	k2eAgFnSc1d1	satanistická
vzpoura	vzpoura	k1gFnSc1	vzpoura
a	a	k8xC	a
zlo	zlo	k1gNnSc1	zlo
Morgotha	Morgoth	k1gMnSc2	Morgoth
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
následníka	následník	k1gMnSc2	následník
<g/>
,	,	kIx,	,
Saurona	Saurona	k1gFnSc1	Saurona
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátek	počátek	k1gInSc1	počátek
===	===	k?	===
</s>
</p>
<p>
<s>
Kosmologický	kosmologický	k2eAgInSc1d1	kosmologický
mýtus	mýtus	k1gInSc1	mýtus
v	v	k7c6	v
Silmarillionu	Silmarillion	k1gInSc6	Silmarillion
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Eru	Eru	k1gMnSc1	Eru
<g/>
,	,	kIx,	,
Jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
stvoření	stvoření	k1gNnPc4	stvoření
uvedením	uvedení	k1gNnSc7	uvedení
v	v	k7c6	v
bytí	bytí	k1gNnSc6	bytí
bezpočtu	bezpočet	k1gInSc2	bezpočet
duchů	duch	k1gMnPc2	duch
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
byli	být	k5eAaImAgMnP	být
"	"	kIx"	"
<g/>
potomstvem	potomstvo	k1gNnSc7	potomstvo
jeho	jeho	k3xOp3gFnSc2	jeho
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
"	"	kIx"	"
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byli	být	k5eAaImAgMnP	být
před	před	k7c7	před
stvořením	stvoření	k1gNnSc7	stvoření
všeho	všecek	k3xTgNnSc2	všecek
ostatního	ostatní	k1gNnSc2	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
Sauron	Sauron	k1gInSc1	Sauron
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
takovým	takový	k3xDgInPc3	takový
"	"	kIx"	"
<g/>
nesmrtelným	smrtelný	k2eNgInPc3d1	nesmrtelný
(	(	kIx(	(
<g/>
andělským	andělský	k2eAgMnSc7d1	andělský
<g/>
)	)	kIx)	)
duchem	duch	k1gMnSc7	duch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
Sauron	Sauron	k1gMnSc1	Sauron
hleděl	hledět	k5eAaImAgMnS	hledět
na	na	k7c4	na
Stvořitele	Stvořitel	k1gMnSc4	Stvořitel
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
později	pozdě	k6eAd2	pozdě
existenci	existence	k1gFnSc4	existence
Erua	Eru	k1gInSc2	Eru
popíral	popírat	k5eAaImAgInS	popírat
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k2eAgMnSc1d1	Tolkien
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sauron	Sauron	k1gMnSc1	Sauron
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
skutečným	skutečný	k2eAgMnSc7d1	skutečný
ateistou	ateista	k1gMnSc7	ateista
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nižších	nízký	k2eAgMnPc2d2	nižší
duchů	duch	k1gMnPc2	duch
<g/>
,	,	kIx,	,
stvořeným	stvořený	k2eAgInSc7d1	stvořený
před	před	k7c7	před
stvořením	stvoření	k1gNnSc7	stvoření
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
znal	znát	k5eAaImAgMnS	znát
Erua	Erua	k1gMnSc1	Erua
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
měřítka	měřítko	k1gNnSc2	měřítko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
quenijštině	quenijština	k1gFnSc6	quenijština
byli	být	k5eAaImAgMnP	být
tito	tento	k3xDgMnPc1	tento
'	'	kIx"	'
<g/>
andělští	andělský	k2eAgMnPc1d1	andělský
<g/>
'	'	kIx"	'
duchové	duch	k1gMnPc1	duch
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
Ainur	Ainura	k1gFnPc2	Ainura
<g/>
.	.	kIx.	.
</s>
<s>
Nejmocnější	mocný	k2eAgMnPc1d3	nejmocnější
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
světa	svět	k1gInSc2	svět
Ardy	Arda	k1gFnSc2	Arda
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
Valar	Valar	k1gInSc4	Valar
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
nižší	nízký	k2eAgMnPc1d2	nižší
duchové	duch	k1gMnPc1	duch
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgNnSc4	jenž
Sauron	Sauron	k1gMnSc1	Sauron
patřil	patřit	k5eAaImAgMnS	patřit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
Maiar	Maiar	k1gInSc4	Maiar
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
Ainur	Ainur	k1gMnSc1	Ainur
byli	být	k5eAaImAgMnP	být
stvořeni	stvořit	k5eAaPmNgMnP	stvořit
dobrými	dobrý	k2eAgMnPc7d1	dobrý
a	a	k8xC	a
nezkaženými	zkažený	k2eNgMnPc7d1	nezkažený
<g/>
.	.	kIx.	.
</s>
<s>
Elrond	Elrond	k1gInSc1	Elrond
v	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
totiž	totiž	k9	totiž
nic	nic	k3yNnSc4	nic
není	být	k5eNaImIp3nS	být
zlé	zlý	k2eAgNnSc1d1	zlé
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Sauron	Sauron	k1gMnSc1	Sauron
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vzpoura	vzpoura	k1gFnSc1	vzpoura
pocházela	pocházet	k5eAaImAgFnS	pocházet
od	od	k7c2	od
Valy	Vala	k1gMnSc2	Vala
Melkora	Melkor	k1gMnSc2	Melkor
<g/>
.	.	kIx.	.
</s>
<s>
Zlo	zlo	k1gNnSc1	zlo
nepocházelo	pocházet	k5eNaImAgNnS	pocházet
od	od	k7c2	od
Saurona	Sauron	k1gMnSc2	Sauron
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
Melkora	Melkor	k1gMnSc2	Melkor
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Sauron	Sauron	k1gMnSc1	Sauron
představuje	představovat	k5eAaImIp3nS	představovat
to	ten	k3xDgNnSc4	ten
nejzkaženější	zkažený	k2eAgNnSc4d3	zkažený
zlo	zlo	k1gNnSc4	zlo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
toužící	toužící	k2eAgFnPc1d1	toužící
po	po	k7c6	po
naprosté	naprostý	k2eAgFnSc6d1	naprostá
světské	světský	k2eAgFnSc3d1	světská
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
božské	božský	k2eAgFnSc3d1	božská
úctě	úcta	k1gFnSc3	úcta
(	(	kIx(	(
<g/>
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
služebníci	služebník	k1gMnPc1	služebník
ho	on	k3xPp3gNnSc4	on
tak	tak	k9	tak
uctívali	uctívat	k5eAaImAgMnP	uctívat
<g/>
)	)	kIx)	)
a	a	k8xC	a
dosazení	dosazení	k1gNnSc1	dosazení
se	se	k3xPyFc4	se
na	na	k7c4	na
Eruovo	Eruův	k2eAgNnSc4d1	Eruův
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
nemá	mít	k5eNaImIp3nS	mít
právoplatný	právoplatný	k2eAgInSc1d1	právoplatný
nárok	nárok	k1gInSc1	nárok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sauron	Sauron	k1gInSc1	Sauron
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
Aulëho	Aulë	k1gMnSc4	Aulë
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
projevoval	projevovat	k5eAaImAgMnS	projevovat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
velký	velký	k2eAgMnSc1d1	velký
řemeslník	řemeslník	k1gMnSc1	řemeslník
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gInSc1	Sauron
byl	být	k5eAaImAgInS	být
původem	původ	k1gInSc7	původ
mnohem	mnohem	k6eAd1	mnohem
vyššího	vysoký	k2eAgInSc2d2	vyšší
řádu	řád	k1gInSc2	řád
než	než	k8xS	než
např.	např.	kA	např.
Gandalf	Gandalf	k1gInSc1	Gandalf
nebo	nebo	k8xC	nebo
Saruman	Saruman	k1gMnSc1	Saruman
<g/>
,	,	kIx,	,
a	a	k8xC	a
největší	veliký	k2eAgMnSc1d3	veliký
<g/>
,	,	kIx,	,
nejnebezpečnější	bezpečný	k2eNgMnSc1d3	nejnebezpečnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejspolehlivější	spolehlivý	k2eAgMnSc1d3	nejspolehlivější
Melkorův	Melkorův	k2eAgMnSc1d1	Melkorův
služebník	služebník	k1gMnSc1	služebník
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
pobočník	pobočník	k1gMnSc1	pobočník
v	v	k7c6	v
Angbandu	Angband	k1gInSc6	Angband
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
sám	sám	k3xTgMnSc1	sám
Temný	temný	k2eAgMnSc1d1	temný
pán	pán	k1gMnSc1	pán
a	a	k8xC	a
Nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
.	.	kIx.	.
<g/>
Eru	Eru	k1gMnSc1	Eru
ponechal	ponechat	k5eAaPmAgMnS	ponechat
andělské	andělský	k2eAgMnPc4d1	andělský
duchy	duch	k1gMnPc4	duch
hrát	hrát	k5eAaImF	hrát
Hudbu	hudba	k1gFnSc4	hudba
Ainur	Ainura	k1gFnPc2	Ainura
(	(	kIx(	(
<g/>
Ainulindalë	Ainulindalë	k1gFnSc1	Ainulindalë
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
rozvíjeli	rozvíjet	k5eAaImAgMnP	rozvíjet
téma	téma	k1gNnSc4	téma
zjevené	zjevený	k2eAgFnSc2d1	zjevená
samotným	samotný	k2eAgMnPc3d1	samotný
Eru	Eru	k1gMnPc3	Eru
<g/>
.	.	kIx.	.
</s>
<s>
Melkor	Melkor	k1gMnSc1	Melkor
se	se	k3xPyFc4	se
však	však	k9	však
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
zvýšit	zvýšit	k5eAaPmF	zvýšit
svou	svůj	k3xOyFgFnSc4	svůj
slávu	sláva	k1gFnSc4	sláva
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
písni	píseň	k1gFnSc6	píseň
svými	svůj	k3xOyFgFnPc7	svůj
myšlenkami	myšlenka	k1gFnPc7	myšlenka
a	a	k8xC	a
ideami	idea	k1gFnPc7	idea
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
Tématem	téma	k1gNnSc7	téma
Hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
...	...	k?	...
a	a	k8xC	a
rázem	rázem	k6eAd1	rázem
kolem	kolem	k7c2	kolem
něho	on	k3xPp3gMnSc2	on
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nelad	nelad	k1gInSc4	nelad
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zpívali	zpívat	k5eAaImAgMnP	zpívat
blízko	blízko	k7c2	blízko
něho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
propadli	propadnout	k5eAaPmAgMnP	propadnout
sklíčenosti	sklíčenost	k1gFnPc4	sklíčenost
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
<g />
.	.	kIx.	.
</s>
<s>
myšlenky	myšlenka	k1gFnSc2	myšlenka
se	se	k3xPyFc4	se
narušily	narušit	k5eAaPmAgInP	narušit
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
hudba	hudba	k1gFnSc1	hudba
zakolísala	zakolísat	k5eAaPmAgFnS	zakolísat
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
však	však	k9	však
začali	začít	k5eAaPmAgMnP	začít
ladit	ladit	k5eAaImF	ladit
svou	svůj	k3xOyFgFnSc4	svůj
hudbu	hudba	k1gFnSc4	hudba
spíš	spíš	k9	spíš
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
než	než	k8xS	než
podle	podle	k7c2	podle
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
měli	mít	k5eAaImAgMnP	mít
zprvu	zprvu	k6eAd1	zprvu
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Melkorova	Melkorův	k2eAgFnSc1d1	Melkorova
disharmonie	disharmonie	k1gFnSc1	disharmonie
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgInPc4	svůj
strašlivé	strašlivý	k2eAgInPc4d1	strašlivý
důsledky	důsledek	k1gInPc4	důsledek
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Eru	Eru	k1gFnSc4	Eru
dovolil	dovolit	k5eAaPmAgInS	dovolit
Ainur	Ainur	k1gMnSc1	Ainur
vykonat	vykonat	k5eAaPmF	vykonat
"	"	kIx"	"
<g/>
Píseň	píseň	k1gFnSc1	píseň
Stvoření	stvoření	k1gNnSc2	stvoření
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
jako	jako	k9	jako
šablonu	šablona	k1gFnSc4	šablona
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
stvořen	stvořen	k2eAgMnSc1d1	stvořen
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zlo	zlo	k1gNnSc1	zlo
světa	svět	k1gInSc2	svět
nebylo	být	k5eNaImAgNnS	být
na	na	k7c6	na
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
Tématu	téma	k1gNnSc6	téma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
s	s	k7c7	s
disharmonií	disharmonie	k1gFnSc7	disharmonie
Melkora	Melkora	k1gFnSc1	Melkora
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sauron	Sauron	k1gMnSc1	Sauron
nebyl	být	k5eNaImAgMnS	být
původcem	původce	k1gMnSc7	původce
disharmonie	disharmonie	k1gFnSc2	disharmonie
a	a	k8xC	a
nepochybně	pochybně	k6eNd1	pochybně
věděl	vědět	k5eAaImAgMnS	vědět
víc	hodně	k6eAd2	hodně
o	o	k7c6	o
Hudbě	hudba	k1gFnSc6	hudba
než	než	k8xS	než
Melkor	Melkor	k1gMnSc1	Melkor
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
mysl	mysl	k1gFnSc1	mysl
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
naplněna	naplnit	k5eAaPmNgFnS	naplnit
jeho	jeho	k3xOp3gInPc7	jeho
vlastními	vlastní	k2eAgInPc7d1	vlastní
plány	plán	k1gInPc7	plán
a	a	k8xC	a
záměry	záměr	k1gInPc7	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
Sauron	Sauron	k1gMnSc1	Sauron
nebyl	být	k5eNaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
duchů	duch	k1gMnPc2	duch
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
okamžitě	okamžitě	k6eAd1	okamžitě
ladili	ladit	k5eAaImAgMnP	ladit
svou	svůj	k3xOyFgFnSc4	svůj
Hudbu	hudba	k1gFnSc4	hudba
podle	podle	k7c2	podle
Melkora	Melkor	k1gMnSc2	Melkor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
Melkorova	Melkorův	k2eAgFnSc1d1	Melkorova
disharmonie	disharmonie	k1gFnSc1	disharmonie
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Tématem	téma	k1gNnSc7	téma
Eru	Eru	k1gFnSc2	Eru
-	-	kIx~	-
Hudba	hudba	k1gFnSc1	hudba
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
konflikt	konflikt	k1gInSc4	konflikt
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Eru	Eru	k1gMnSc1	Eru
píseň	píseň	k1gFnSc4	píseň
ukončil	ukončit	k5eAaPmAgMnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
duchové	duch	k1gMnPc1	duch
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
učinili	učinit	k5eAaPmAgMnP	učinit
<g/>
,	,	kIx,	,
Eru	Eru	k1gMnSc1	Eru
učinil	učinit	k5eAaPmAgMnS	učinit
bytí	bytí	k1gNnSc1	bytí
Hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
stvořen	stvořen	k2eAgInSc1d1	stvořen
vesmír	vesmír	k1gInSc1	vesmír
Eä	Eä	k1gFnSc2	Eä
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
konflikt	konflikt	k1gInSc4	konflikt
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Eru	Eru	k?	Eru
umožnil	umožnit	k5eAaPmAgInS	umožnit
duchům	duch	k1gMnPc3	duch
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Eä	Eä	k1gFnSc2	Eä
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
tak	tak	k8xS	tak
učinilo	učinit	k5eAaPmAgNnS	učinit
a	a	k8xC	a
Sauron	Sauron	k1gMnSc1	Sauron
byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
umožnil	umožnit	k5eAaPmAgMnS	umožnit
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
umožnil	umožnit	k5eAaPmAgInS	umožnit
jak	jak	k6eAd1	jak
velké	velký	k2eAgNnSc4d1	velké
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
tak	tak	k9	tak
velké	velký	k2eAgNnSc4d1	velké
dobro	dobro	k1gNnSc4	dobro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc1	první
věk	věk	k1gInSc1	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
Eä	Eä	k1gFnSc2	Eä
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Valar	Valar	k1gInSc1	Valar
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
Maiar	Maiar	k1gInSc4	Maiar
pokusili	pokusit	k5eAaPmAgMnP	pokusit
vybudovat	vybudovat	k5eAaPmF	vybudovat
svět	svět	k1gInSc4	svět
podle	podle	k7c2	podle
vůle	vůle	k1gFnSc2	vůle
Ilúvatara	Ilúvatar	k1gMnSc2	Ilúvatar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
úsilí	úsilí	k1gNnSc6	úsilí
se	se	k3xPyFc4	se
Sauron	Sauron	k1gInSc1	Sauron
projevil	projevit	k5eAaPmAgInS	projevit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
velký	velký	k2eAgMnSc1d1	velký
řemeslník	řemeslník	k1gMnSc1	řemeslník
patřící	patřící	k2eAgInSc4d1	patřící
k	k	k7c3	k
Aulëmu	Aulëmo	k1gNnSc3	Aulëmo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
ostatní	ostatní	k2eAgMnSc1d1	ostatní
Valar	Valar	k1gMnSc1	Valar
<g/>
,	,	kIx,	,
Aulë	Aulë	k1gMnSc1	Aulë
učil	učít	k5eAaPmAgMnS	učít
své	svůj	k3xOyFgMnPc4	svůj
služebníky	služebník	k1gMnPc4	služebník
Maiar	Maiar	k1gInSc1	Maiar
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
<g/>
,	,	kIx,	,
zákonech	zákon	k1gInPc6	zákon
a	a	k8xC	a
podstatě	podstata	k1gFnSc6	podstata
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Sauron	Sauron	k1gMnSc1	Sauron
získal	získat	k5eAaPmAgMnS	získat
tuto	tento	k3xDgFnSc4	tento
znalost	znalost	k1gFnSc4	znalost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
obrovských	obrovský	k2eAgFnPc2d1	obrovská
prostor	prostora	k1gFnPc2	prostora
Eä	Eä	k1gMnSc1	Eä
Valar	Valar	k1gMnSc1	Valar
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
své	svůj	k3xOyFgNnSc4	svůj
úsilí	úsilí	k1gNnSc4	úsilí
na	na	k7c6	na
Ardu	Ardum	k1gNnSc6	Ardum
<g/>
,	,	kIx,	,
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
elfové	elf	k1gMnPc1	elf
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
budou	být	k5eAaImBp3nP	být
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Melkor	Melkor	k1gInSc1	Melkor
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Morgoth	Morgoth	k1gInSc1	Morgoth
<g/>
,	,	kIx,	,
také	také	k9	také
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Ardy	Arda	k1gFnSc2	Arda
<g/>
.	.	kIx.	.
</s>
<s>
Toužíc	toužit	k5eAaImSgFnS	toužit
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
svrchovaným	svrchovaný	k2eAgMnSc7d1	svrchovaný
pánem	pán	k1gMnSc7	pán
světa	svět	k1gInSc2	svět
oponoval	oponovat	k5eAaImAgMnS	oponovat
ostatním	ostatní	k1gNnSc6	ostatní
Valar	Valar	k1gMnSc1	Valar
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zůstali	zůstat	k5eAaPmAgMnP	zůstat
věrní	věrný	k2eAgMnPc1d1	věrný
Ilúvatarovi	Ilúvatarův	k2eAgMnPc1d1	Ilúvatarův
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
sledovat	sledovat	k5eAaImF	sledovat
Stvořitelův	Stvořitelův	k2eAgInSc4d1	Stvořitelův
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Sauron	Sauron	k1gMnSc1	Sauron
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Melkorově	Melkorův	k2eAgInSc6d1	Melkorův
vlivu	vliv	k1gInSc6	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sauron	Sauron	k1gMnSc1	Sauron
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
pořádek	pořádek	k1gInSc4	pořádek
a	a	k8xC	a
koordinaci	koordinace	k1gFnSc4	koordinace
a	a	k8xC	a
nesnášel	snášet	k5eNaImAgMnS	snášet
zmatek	zmatek	k1gInSc4	zmatek
a	a	k8xC	a
neuspořádané	uspořádaný	k2eNgFnPc4d1	neuspořádaná
třenice	třenice	k1gFnPc4	třenice
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
moc	moc	k6eAd1	moc
a	a	k8xC	a
vůle	vůle	k1gFnSc1	vůle
Melkora	Melkora	k1gFnSc1	Melkora
po	po	k7c6	po
nastolení	nastolení	k1gNnSc6	nastolení
pořádku	pořádek	k1gInSc2	pořádek
v	v	k7c6	v
Ardě	Arda	k1gFnSc6	Arda
Saurona	Saurona	k1gFnSc1	Saurona
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
měl	mít	k5eAaImAgMnS	mít
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
činech	čin	k1gInPc6	čin
Melkora	Melkora	k1gFnSc1	Melkora
na	na	k7c6	na
Ardě	Ard	k1gInSc6	Ard
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
méně	málo	k6eAd2	málo
zlý	zlý	k2eAgMnSc1d1	zlý
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpočátku	zpočátku	k6eAd1	zpočátku
sloužil	sloužit	k5eAaImAgMnS	sloužit
jinému	jiný	k1gMnSc3	jiný
a	a	k8xC	a
ne	ne	k9	ne
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
zřejmě	zřejmě	k6eAd1	zřejmě
předstíral	předstírat	k5eAaImAgMnS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
věrným	věrný	k2eAgMnSc7d1	věrný
služebníkem	služebník	k1gMnSc7	služebník
Valar	Valara	k1gFnPc2	Valara
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
předával	předávat	k5eAaImAgMnS	předávat
Melkorovi	Melkor	k1gMnSc3	Melkor
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
činili	činit	k5eAaImAgMnP	činit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Valar	Valar	k1gInSc4	Valar
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
říši	říše	k1gFnSc4	říše
Almaren	Almarna	k1gFnPc2	Almarna
<g/>
,	,	kIx,	,
osvětlenou	osvětlený	k2eAgFnSc4d1	osvětlená
svitem	svit	k1gInSc7	svit
Dvou	dva	k4xCgFnPc2	dva
lamp	lampa	k1gFnPc2	lampa
<g/>
,	,	kIx,	,
Melkor	Melkor	k1gInSc1	Melkor
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měl	mít	k5eAaImAgInS	mít
zvědy	zvěd	k1gInPc4	zvěd
mezi	mezi	k7c4	mezi
Maiar	Maiar	k1gInSc4	Maiar
a	a	k8xC	a
Sauron	Sauron	k1gMnSc1	Sauron
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gInSc7	jejich
náčelníkem	náčelník	k1gInSc7	náčelník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Melkor	Melkor	k1gInSc1	Melkor
zničil	zničit	k5eAaPmAgInS	zničit
Almaren	Almarno	k1gNnPc2	Almarno
a	a	k8xC	a
Valar	Valara	k1gFnPc2	Valara
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
novou	nový	k2eAgFnSc4d1	nová
zemi	zem	k1gFnSc4	zem
na	na	k7c6	na
Nejzazším	zadní	k2eAgInSc6d3	nejzazší
Západě	západ	k1gInSc6	západ
<g/>
:	:	kIx,	:
říši	říš	k1gFnSc6	říš
Valinor	Valinora	k1gFnPc2	Valinora
<g/>
.	.	kIx.	.
</s>
<s>
Valar	Valar	k1gMnSc1	Valar
ještě	ještě	k6eAd1	ještě
nevěděli	vědět	k5eNaImAgMnP	vědět
o	o	k7c6	o
Sauronově	Sauronův	k2eAgFnSc6d1	Sauronova
zradě	zrada	k1gFnSc6	zrada
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Sauron	Sauron	k1gMnSc1	Sauron
zůstal	zůstat	k5eAaPmAgMnS	zůstat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
Sauron	Sauron	k1gMnSc1	Sauron
Valinor	Valinor	k1gMnSc1	Valinor
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
centrálního	centrální	k2eAgInSc2d1	centrální
kontinentu	kontinent	k1gInSc2	kontinent
Ardy	Arda	k1gFnSc2	Arda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
byl	být	k5eAaImAgInS	být
Sauron	Sauron	k1gInSc1	Sauron
definitivně	definitivně	k6eAd1	definitivně
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Melkora	Melkor	k1gMnSc2	Melkor
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
Melkorovi	Melkor	k1gMnSc3	Melkor
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
,	,	kIx,	,
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
oddaným	oddaný	k2eAgMnSc7d1	oddaný
a	a	k8xC	a
schopným	schopný	k2eAgMnSc7d1	schopný
služebníkem	služebník	k1gMnSc7	služebník
<g/>
,	,	kIx,	,
a	a	k8xC	a
Melkor	Melkor	k1gMnSc1	Melkor
mu	on	k3xPp3gMnSc3	on
svěřil	svěřit	k5eAaPmAgMnS	svěřit
správu	správa	k1gFnSc4	správa
své	svůj	k3xOyFgFnSc3	svůj
pevnosti	pevnost	k1gFnSc3	pevnost
Angband	Angband	k1gInSc4	Angband
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ta	ten	k3xDgFnSc1	ten
nebyla	být	k5eNaImAgFnS	být
po	po	k7c6	po
Druhé	druhý	k4xOgFnSc6	druhý
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Melkorem	Melkor	k1gInSc7	Melkor
(	(	kIx(	(
<g/>
brzy	brzy	k6eAd1	brzy
po	po	k7c4	po
procitnutí	procitnutí	k1gNnSc4	procitnutí
elfů	elf	k1gMnPc2	elf
<g/>
)	)	kIx)	)
příliš	příliš	k6eAd1	příliš
prozkoumávána	prozkoumáván	k2eAgFnSc1d1	prozkoumáván
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Sauron	Sauron	k1gMnSc1	Sauron
nebyl	být	k5eNaImAgMnS	být
dopaden	dopadnout	k5eAaPmNgMnS	dopadnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Melkorově	Melkorův	k2eAgInSc6d1	Melkorův
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
nejmocnějším	mocný	k2eAgMnSc7d3	nejmocnější
sluhou	sluha	k1gMnSc7	sluha
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pánem	pán	k1gMnSc7	pán
mučení	mučení	k1gNnSc2	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
Náhlého	náhlý	k2eAgInSc2d1	náhlý
plamene	plamen	k1gInSc2	plamen
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
Tol	tol	k1gInSc1	tol
Sirion	Sirion	k1gInSc4	Sirion
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
nastěhoval	nastěhovat	k5eAaPmAgMnS	nastěhovat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vlkodlaky	vlkodlak	k1gMnPc7	vlkodlak
a	a	k8xC	a
kde	kde	k6eAd1	kde
věznil	věznit	k5eAaImAgMnS	věznit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Finroda	Finrod	k1gMnSc2	Finrod
Felegunda	Felegund	k1gMnSc2	Felegund
a	a	k8xC	a
Berena	Beren	k1gMnSc2	Beren
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
předat	předat	k5eAaPmF	předat
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
věží	věž	k1gFnSc7	věž
Lúthien	Lúthien	k2eAgInSc1d1	Lúthien
Tinúviel	Tinúviel	k1gInSc1	Tinúviel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Válce	válka	k1gFnSc6	válka
hněvu	hněv	k1gInSc2	hněv
se	se	k3xPyFc4	se
Sauron	Sauron	k1gInSc1	Sauron
zatoužil	zatoužit	k5eAaPmAgInS	zatoužit
napravit	napravit	k5eAaPmF	napravit
a	a	k8xC	a
prosil	prosít	k5eAaPmAgMnS	prosít
Eönwëho	Eönwë	k1gMnSc4	Eönwë
<g/>
,	,	kIx,	,
herolda	herold	k1gMnSc4	herold
Valar	Valar	k1gInSc4	Valar
<g/>
,	,	kIx,	,
o	o	k7c6	o
odpuštění	odpuštění	k1gNnSc6	odpuštění
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
neměl	mít	k5eNaImAgInS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
odpustit	odpustit	k5eAaPmF	odpustit
bytosti	bytost	k1gFnSc3	bytost
vlastního	vlastní	k2eAgInSc2d1	vlastní
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Amanu	Aman	k1gInSc2	Aman
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předstoupí	předstoupit	k5eAaPmIp3nS	předstoupit
před	před	k7c4	před
Soudný	soudný	k2eAgInSc4d1	soudný
kruh	kruh	k1gInSc4	kruh
Valar	Valara	k1gFnPc2	Valara
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
se	se	k3xPyFc4	se
však	však	k9	však
zalekl	zaleknout	k5eAaPmAgMnS	zaleknout
přísného	přísný	k2eAgInSc2d1	přísný
trestu	trest	k1gInSc2	trest
a	a	k8xC	a
raději	rád	k6eAd2	rád
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
daleko	daleko	k6eAd1	daleko
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhý	druhý	k4xOgInSc1	druhý
věk	věk	k1gInSc1	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
stoletích	století	k1gNnPc6	století
Druhého	druhý	k4xOgInSc2	druhý
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
Sauron	Sauron	k1gMnSc1	Sauron
znovu	znovu	k6eAd1	znovu
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
vládcem	vládce	k1gMnSc7	vládce
Středozemě	Středozem	k1gFnSc2	Středozem
a	a	k8xC	a
podrobit	podrobit	k5eAaPmF	podrobit
si	se	k3xPyFc3	se
všechny	všechen	k3xTgInPc4	všechen
její	její	k3xOp3gInPc4	její
svobodné	svobodný	k2eAgInPc4d1	svobodný
národy	národ	k1gInPc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nádherný	nádherný	k2eAgInSc4d1	nádherný
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
Annatarem	Annatar	k1gMnSc7	Annatar
<g/>
,	,	kIx,	,
Pánem	pán	k1gMnSc7	pán
Darů	dar	k1gInPc2	dar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vyslanec	vyslanec	k1gMnSc1	vyslanec
Valar	Valar	k1gMnSc1	Valar
a	a	k8xC	a
žák	žák	k1gMnSc1	žák
Aulëho	Aulë	k1gMnSc2	Aulë
(	(	kIx(	(
<g/>
označoval	označovat	k5eAaImAgInS	označovat
se	se	k3xPyFc4	se
také	také	k9	také
jménem	jméno	k1gNnSc7	jméno
Aulendil	Aulendil	k1gMnSc2	Aulendil
<g/>
,	,	kIx,	,
Aulëho	Aulë	k1gMnSc2	Aulë
přítel	přítel	k1gMnSc1	přítel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnPc3	jeho
hlubokým	hluboký	k2eAgFnPc3d1	hluboká
znalostem	znalost	k1gFnPc3	znalost
se	se	k3xPyFc4	se
Sauronovi	Sauron	k1gMnSc3	Sauron
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
důvěru	důvěra	k1gFnSc4	důvěra
elfích	elfích	k2eAgMnPc2d2	elfích
kovářů	kovář	k1gMnPc2	kovář
v	v	k7c6	v
Eregionu	Eregion	k1gInSc6	Eregion
<g/>
.	.	kIx.	.
</s>
<s>
Elfům	elf	k1gMnPc3	elf
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgMnS	představit
jako	jako	k9	jako
vyslanec	vyslanec	k1gMnSc1	vyslanec
Valar	Valar	k1gMnSc1	Valar
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
elfové	elf	k1gMnPc1	elf
mu	on	k3xPp3gInSc3	on
ovšem	ovšem	k9	ovšem
nedůvěřovali	důvěřovat	k5eNaImAgMnP	důvěřovat
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
zejména	zejména	k9	zejména
Galadriel	Galadriel	k1gInSc4	Galadriel
a	a	k8xC	a
Gil-galad	Gilalad	k1gInSc4	Gil-galad
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
elfové	elf	k1gMnPc1	elf
v	v	k7c6	v
Eregionu	Eregion	k1gInSc6	Eregion
neposlechli	poslechnout	k5eNaPmAgMnP	poslechnout
jejich	jejich	k3xOp3gNnPc4	jejich
varování	varování	k1gNnPc4	varování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
Sauronovou	Sauronův	k2eAgFnSc7d1	Sauronova
pomocí	pomoc	k1gFnSc7	pomoc
elfští	elfský	k2eAgMnPc1d1	elfský
kováři	kovář	k1gMnPc1	kovář
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
Prsteny	prsten	k1gInPc4	prsten
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
zadržovaly	zadržovat	k5eAaImAgFnP	zadržovat
a	a	k8xC	a
posilovaly	posilovat	k5eAaImAgFnP	posilovat
moc	moc	k6eAd1	moc
svých	svůj	k3xOyFgMnPc2	svůj
nositelů	nositel	k1gMnPc2	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
nejmocnější	mocný	k2eAgFnPc1d3	nejmocnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
kovář	kovář	k1gMnSc1	kovář
Celebrimbor	Celebrimbor	k1gMnSc1	Celebrimbor
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
je	být	k5eAaImIp3nS	být
nejmocnějším	mocný	k2eAgInPc3d3	nejmocnější
z	z	k7c2	z
elfů	elf	k1gMnPc2	elf
-	-	kIx~	-
Gil-galadovi	Gilalada	k1gMnSc3	Gil-galada
<g/>
,	,	kIx,	,
Galadriel	Galadriel	k1gInSc1	Galadriel
a	a	k8xC	a
Círdanovi	Círdanův	k2eAgMnPc1d1	Círdanův
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
Prsteny	prsten	k1gInPc1	prsten
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
Sauron	Sauron	k1gMnSc1	Sauron
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
věnoval	věnovat	k5eAaImAgMnS	věnovat
mocným	mocný	k2eAgMnSc7d1	mocný
vládcům	vládce	k1gMnPc3	vládce
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidé	člověk	k1gMnPc1	člověk
nejvíce	hodně	k6eAd3	hodně
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
moci	moc	k1gFnSc6	moc
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc4	sedm
prstenů	prsten	k1gInPc2	prsten
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
vládci	vládce	k1gMnSc3	vládce
trpaslíků	trpaslík	k1gMnPc2	trpaslík
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sedmý	sedmý	k4xOgInSc1	sedmý
<g/>
,	,	kIx,	,
nejmocnější	mocný	k2eAgInSc1d3	nejmocnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
přijal	přijmout	k5eAaPmAgMnS	přijmout
Durin	Durin	k1gMnSc1	Durin
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc1	vládce
Khazad-dû	Khazadû	k1gMnSc1	Khazad-dû
<g/>
,	,	kIx,	,
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
Celebrimbora	Celebrimbor	k1gMnSc2	Celebrimbor
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
Saurona	Saurona	k1gFnSc1	Saurona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gInSc1	Sauron
však	však	k8xC	však
všechny	všechen	k3xTgInPc4	všechen
oklamal	oklamat	k5eAaPmAgMnS	oklamat
a	a	k8xC	a
tajně	tajně	k6eAd1	tajně
v	v	k7c6	v
Hoře	hora	k1gFnSc6	hora
osudu	osud	k1gInSc2	osud
v	v	k7c6	v
Mordoru	Mordor	k1gInSc6	Mordor
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
<g/>
,	,	kIx,	,
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
moc	moc	k6eAd1	moc
vládnout	vládnout	k5eAaImF	vládnout
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
ovládat	ovládat	k5eAaImF	ovládat
ostatní	ostatní	k2eAgInPc4d1	ostatní
prsteny	prsten	k1gInPc4	prsten
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc4	jejich
nositele	nositel	k1gMnPc4	nositel
podrobit	podrobit	k5eAaPmF	podrobit
Sauronově	Sauronův	k2eAgFnSc3d1	Sauronova
vůli	vůle	k1gFnSc3	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Prsteny	prsten	k1gInPc1	prsten
moci	moc	k1gFnSc2	moc
však	však	k9	však
měly	mít	k5eAaImAgFnP	mít
velkou	velký	k2eAgFnSc4d1	velká
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
ovládání	ovládání	k1gNnSc3	ovládání
byl	být	k5eAaImAgMnS	být
Sauron	Sauron	k1gMnSc1	Sauron
nucen	nutit	k5eAaImNgMnS	nutit
vložit	vložit	k5eAaPmF	vložit
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
přirozené	přirozený	k2eAgFnSc2d1	přirozená
moci	moc	k1gFnSc2	moc
do	do	k7c2	do
Vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
držitelem	držitel	k1gMnSc7	držitel
Prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
vlastní	vlastní	k2eAgFnSc1d1	vlastní
moc	moc	k1gFnSc1	moc
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
zvětšena	zvětšen	k2eAgFnSc1d1	zvětšena
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
nikdy	nikdy	k6eAd1	nikdy
nepředpokládal	předpokládat	k5eNaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
získal	získat	k5eAaPmAgMnS	získat
a	a	k8xC	a
použil	použít	k5eAaPmAgMnS	použít
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
nový	nový	k2eAgMnSc1d1	nový
vlastník	vlastník	k1gMnSc1	vlastník
Prstenu	prsten	k1gInSc2	prsten
mohl	moct	k5eAaImAgMnS	moct
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
dost	dost	k6eAd1	dost
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
mocný	mocný	k2eAgMnSc1d1	mocný
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
přirozeností	přirozenost	k1gFnSc7	přirozenost
<g/>
)	)	kIx)	)
přemoci	přemoct	k5eAaPmF	přemoct
Saurona	Sauron	k1gMnSc4	Sauron
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
pánem	pán	k1gMnSc7	pán
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Sauron	Sauron	k1gMnSc1	Sauron
znal	znát	k5eAaImAgMnS	znát
nebo	nebo	k8xC	nebo
učinil	učinit	k5eAaPmAgMnS	učinit
od	od	k7c2	od
vytvoření	vytvoření	k1gNnSc2	vytvoření
Prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
svrhnout	svrhnout	k5eAaPmF	svrhnout
a	a	k8xC	a
usurpovat	usurpovat	k5eAaBmF	usurpovat
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnSc1d1	další
slabina	slabina	k1gFnSc1	slabina
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
zničen	zničen	k2eAgInSc1d1	zničen
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
moc	moc	k1gFnSc1	moc
by	by	kYmCp3nS	by
pominula	pominout	k5eAaPmAgFnS	pominout
<g/>
,	,	kIx,	,
Sauronovo	Sauronův	k2eAgNnSc1d1	Sauronovo
vlastní	vlastní	k2eAgNnSc1d1	vlastní
bytí	bytí	k1gNnSc1	bytí
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
zmenšeno	zmenšit	k5eAaPmNgNnS	zmenšit
do	do	k7c2	do
nicotnosti	nicotnost	k1gFnSc2	nicotnost
a	a	k8xC	a
on	on	k3xPp3gInSc1	on
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
snížen	snížit	k5eAaPmNgInS	snížit
do	do	k7c2	do
pouhého	pouhý	k2eAgInSc2d1	pouhý
stínu	stín	k1gInSc2	stín
<g/>
,	,	kIx,	,
pouhou	pouhý	k2eAgFnSc7d1	pouhá
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
zlé	zlý	k2eAgFnSc2d1	zlá
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
to	ten	k3xDgNnSc4	ten
on	on	k3xPp3gMnSc1	on
nikdy	nikdy	k6eAd1	nikdy
neuvažoval	uvažovat	k5eNaImAgMnS	uvažovat
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
nebál	bát	k5eNaImAgMnS	bát
<g/>
.	.	kIx.	.
</s>
<s>
Prsten	prsten	k1gInSc1	prsten
byl	být	k5eAaImAgInS	být
nezničitelný	zničitelný	k2eNgInSc1d1	nezničitelný
jakýmkoli	jakýkoli	k3yIgInSc7	jakýkoli
kovářským	kovářský	k2eAgInSc7d1	kovářský
uměním	umění	k1gNnSc7	umění
menším	malý	k2eAgMnPc3d2	menší
než	než	k8xS	než
jeho	jeho	k3xOp3gMnPc3	jeho
vlastním	vlastní	k2eAgMnPc3d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
neroztavitelný	roztavitelný	k2eNgInSc1d1	roztavitelný
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
neuhasínajícího	uhasínající	k2eNgInSc2d1	neuhasínající
podzemního	podzemní	k2eAgInSc2d1	podzemní
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
jen	jen	k9	jen
v	v	k7c6	v
Mordoru	Mordor	k1gInSc6	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Sauronově	Sauronův	k2eAgInSc6d1	Sauronův
prstu	prst	k1gInSc6	prst
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
si	se	k3xPyFc3	se
však	však	k9	však
Sauron	Sauron	k1gInSc4	Sauron
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
nasadil	nasadit	k5eAaPmAgMnS	nasadit
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
ovládnout	ovládnout	k5eAaPmF	ovládnout
elfy	elf	k1gMnPc4	elf
<g/>
,	,	kIx,	,
důvtipný	důvtipný	k2eAgInSc4d1	důvtipný
postřeh	postřeh	k1gInSc4	postřeh
je	být	k5eAaImIp3nS	být
varoval	varovat	k5eAaImAgMnS	varovat
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
si	se	k3xPyFc3	se
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
jeho	jeho	k3xOp3gInPc4	jeho
úmysly	úmysl	k1gInPc4	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
poznali	poznat	k5eAaPmAgMnP	poznat
<g/>
,	,	kIx,	,
kým	kdo	k3yQnSc7	kdo
je	být	k5eAaImIp3nS	být
Annatar	Annatar	k1gInSc1	Annatar
doopravdy	doopravdy	k6eAd1	doopravdy
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
Sauron	Sauron	k1gInSc1	Sauron
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xC	jako
Temný	temný	k2eAgMnSc1d1	temný
pán	pán	k1gMnSc1	pán
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Elfové	elf	k1gMnPc1	elf
sňali	snít	k5eAaPmAgMnP	snít
své	svůj	k3xOyFgInPc4	svůj
Prsteny	prsten	k1gInPc4	prsten
a	a	k8xC	a
už	už	k6eAd1	už
je	on	k3xPp3gMnPc4	on
nenosili	nosit	k5eNaImAgMnP	nosit
ani	ani	k8xC	ani
neužívali	užívat	k5eNaImAgMnP	užívat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
Sauron	Sauron	k1gMnSc1	Sauron
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Trpaslíky	trpaslík	k1gMnPc4	trpaslík
se	se	k3xPyFc4	se
Sauronovi	Sauron	k1gMnSc3	Sauron
rovněž	rovněž	k9	rovněž
nepodařilo	podařit	k5eNaPmAgNnS	podařit
ovládat	ovládat	k5eAaImF	ovládat
pomocí	pomocí	k7c2	pomocí
Sedmi	sedm	k4xCc2	sedm
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byli	být	k5eAaImAgMnP	být
příliš	příliš	k6eAd1	příliš
nezlomní	zlomný	k2eNgMnPc1d1	nezlomný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
Prsteny	prsten	k1gInPc4	prsten
měly	mít	k5eAaImAgInP	mít
jen	jen	k6eAd1	jen
malý	malý	k2eAgInSc4d1	malý
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
leda	leda	k8xS	leda
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
vnášely	vnášet	k5eAaImAgInP	vnášet
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
nepokoj	nepokoj	k1gInSc4	nepokoj
a	a	k8xC	a
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
moci	moc	k1gFnSc6	moc
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
Devíti	devět	k4xCc2	devět
nechali	nechat	k5eAaPmAgMnP	nechat
snadno	snadno	k6eAd1	snadno
zotročit	zotročit	k5eAaPmF	zotročit
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
nazgû	nazgû	k?	nazgû
<g/>
,	,	kIx,	,
Prstenovými	prstenový	k2eAgInPc7d1	prstenový
přízraky	přízrak	k1gInPc7	přízrak
<g/>
,	,	kIx,	,
nejstrašnějšími	strašný	k2eAgInPc7d3	nejstrašnější
ze	z	k7c2	z
Sauronových	Sauronův	k2eAgMnPc2d1	Sauronův
služebníků	služebník	k1gMnPc2	služebník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Sauron	Sauron	k1gMnSc1	Sauron
selhal	selhat	k5eAaPmAgMnS	selhat
v	v	k7c4	v
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
rozzuřil	rozzuřit	k5eAaPmAgMnS	rozzuřit
se	se	k3xPyFc4	se
a	a	k8xC	a
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Rozpoutal	rozpoutat	k5eAaPmAgInS	rozpoutat
válku	válka	k1gFnSc4	válka
elfů	elf	k1gMnPc2	elf
se	s	k7c7	s
Sauronem	Sauron	k1gInSc7	Sauron
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
si	se	k3xPyFc3	se
podrobil	podrobit	k5eAaPmAgMnS	podrobit
mnoho	mnoho	k4c4	mnoho
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
začaly	začít	k5eAaPmAgFnP	začít
Temné	temný	k2eAgInPc4d1	temný
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dobyl	dobýt	k5eAaPmAgInS	dobýt
Eregion	Eregion	k1gInSc1	Eregion
a	a	k8xC	a
zabil	zabít	k5eAaPmAgInS	zabít
Celebrimbora	Celebrimbor	k1gMnSc4	Celebrimbor
<g/>
,	,	kIx,	,
obléhal	obléhat	k5eAaImAgMnS	obléhat
Imlandris	Imlandris	k1gFnSc4	Imlandris
(	(	kIx(	(
<g/>
Roklinku	roklinka	k1gFnSc4	roklinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sváděl	svádět	k5eAaImAgMnS	svádět
boje	boj	k1gInPc4	boj
s	s	k7c7	s
Morií	Morie	k1gFnSc7	Morie
a	a	k8xC	a
Lórienem	Lórien	k1gInSc7	Lórien
a	a	k8xC	a
postupoval	postupovat	k5eAaImAgMnS	postupovat
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
Gil-galadovy	Gilaladův	k2eAgFnSc2d1	Gil-galadův
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
Barad-dû	Baradû	k1gFnSc4	Barad-dû
<g/>
,	,	kIx,	,
Temnou	temný	k2eAgFnSc4d1	temná
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
Morannon	Morannon	k1gMnSc1	Morannon
<g/>
,	,	kIx,	,
Černou	černý	k2eAgFnSc4d1	černá
bránu	brána	k1gFnSc4	brána
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgInS	zabránit
možné	možný	k2eAgFnSc3d1	možná
invazi	invaze	k1gFnSc3	invaze
<g/>
,	,	kIx,	,
a	a	k8xC	a
podrobil	podrobit	k5eAaPmAgMnS	podrobit
své	svůj	k3xOyFgFnSc3	svůj
moci	moc	k1gFnSc3	moc
zlé	zlý	k2eAgFnSc2d1	zlá
bytosti	bytost	k1gFnSc2	bytost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
Morgothovi	Morgothův	k2eAgMnPc1d1	Morgothův
v	v	k7c6	v
Prvním	první	k4xOgInSc6	první
věku	věk	k1gInSc6	věk
(	(	kIx(	(
<g/>
jako	jako	k9	jako
skřeti	skřet	k1gMnPc1	skřet
a	a	k8xC	a
trolové	trol	k1gMnPc1	trol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
také	také	k6eAd1	také
vládl	vládnout	k5eAaImAgMnS	vládnout
lidem	člověk	k1gMnPc3	člověk
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
a	a	k8xC	a
Jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
se	se	k3xPyFc4	se
Pánem	pán	k1gMnSc7	pán
země	zem	k1gFnSc2	zem
a	a	k8xC	a
králem	král	k1gMnSc7	král
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Elfové	elf	k1gMnPc1	elf
však	však	k9	však
bojovali	bojovat	k5eAaImAgMnP	bojovat
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
mocné	mocný	k2eAgFnSc2d1	mocná
armády	armáda	k1gFnSc2	armáda
númenorského	númenorský	k2eAgMnSc2d1	númenorský
krále	král	k1gMnSc2	král
Tar-Minastira	Tar-Minastiro	k1gNnSc2	Tar-Minastiro
zničili	zničit	k5eAaPmAgMnP	zničit
Sauronovu	Sauronův	k2eAgFnSc4d1	Sauronova
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Númenorejci	Númenorejec	k1gMnPc1	Númenorejec
byli	být	k5eAaImAgMnP	být
nejmocnějším	mocný	k2eAgNnSc7d3	nejmocnější
královstvím	království	k1gNnSc7	království
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
když	když	k8xS	když
připluli	připlout	k5eAaPmAgMnP	připlout
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
velikým	veliký	k2eAgNnSc7d1	veliké
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
,	,	kIx,	,
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
služebníci	služebník	k1gMnPc1	služebník
prchali	prchat	k5eAaImAgMnP	prchat
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gInSc1	Sauron
proto	proto	k8xC	proto
raději	rád	k6eAd2	rád
hledal	hledat	k5eAaImAgInS	hledat
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
prosadit	prosadit	k5eAaPmF	prosadit
své	svůj	k3xOyFgInPc4	svůj
záměry	záměr	k1gInPc4	záměr
lstí	lest	k1gFnPc2	lest
<g/>
.	.	kIx.	.
</s>
<s>
Vzdal	vzdát	k5eAaPmAgInS	vzdát
se	se	k3xPyFc4	se
králi	král	k1gMnPc7	král
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
odvést	odvést	k5eAaPmF	odvést
na	na	k7c4	na
Númenor	Númenor	k1gInSc4	Númenor
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
hlubokým	hluboký	k2eAgFnPc3d1	hluboká
znalostem	znalost	k1gFnPc3	znalost
a	a	k8xC	a
lstivému	lstivý	k2eAgNnSc3d1	lstivé
našeptávání	našeptávání	k1gNnSc3	našeptávání
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgMnS	stát
z	z	k7c2	z
vězně	vězně	k6eAd1	vězně
královým	králův	k2eAgMnSc7d1	králův
rádcem	rádce	k1gMnSc7	rádce
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
přiměl	přimět	k5eAaPmAgInS	přimět
krále	král	k1gMnSc4	král
Ar-Pharazôna	Ar-Pharazôn	k1gMnSc4	Ar-Pharazôn
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
Valinor	Valinor	k1gInSc4	Valinor
<g/>
.	.	kIx.	.
</s>
<s>
Valar	Valar	k1gInSc4	Valar
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
Eru	Eru	k1gMnSc4	Eru
Ilúvatara	Ilúvatar	k1gMnSc4	Ilúvatar
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
-	-	kIx~	-
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
númenorejský	númenorejský	k2eAgInSc4d1	númenorejský
útok	útok	k1gInSc4	útok
-	-	kIx~	-
ostrov	ostrov	k1gInSc1	ostrov
Númenor	Númenora	k1gFnPc2	Númenora
potopil	potopit	k5eAaPmAgInS	potopit
a	a	k8xC	a
zničil	zničit	k5eAaPmAgInS	zničit
Ar-Pharazônovu	Ar-Pharazônův	k2eAgFnSc4d1	Ar-Pharazônův
flotilu	flotila	k1gFnSc4	flotila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sauron	Sauron	k1gInSc1	Sauron
ve	v	k7c6	v
zkáze	zkáza	k1gFnSc6	zkáza
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
duch	duch	k1gMnSc1	duch
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
si	se	k3xPyFc3	se
novou	nový	k2eAgFnSc4d1	nová
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
ovládnout	ovládnout	k5eAaPmF	ovládnout
Středozem	Středozem	k1gFnSc4	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Narazil	narazit	k5eAaPmAgMnS	narazit
však	však	k9	však
na	na	k7c4	na
Dúnadany	Dúnadan	k1gMnPc4	Dúnadan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
unikli	uniknout	k5eAaPmAgMnP	uniknout
ze	z	k7c2	z
zkázy	zkáza	k1gFnSc2	zkáza
Númenoru	Númenor	k1gInSc2	Númenor
<g/>
,	,	kIx,	,
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
Říše	říš	k1gFnSc2	říš
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
,	,	kIx,	,
Arnor	Arnor	k1gMnSc1	Arnor
a	a	k8xC	a
Gondor	Gondor	k1gMnSc1	Gondor
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Sauron	Sauron	k1gMnSc1	Sauron
bál	bát	k5eAaImAgMnS	bát
síly	síla	k1gFnPc4	síla
Dúnadanů	Dúnadan	k1gInPc2	Dúnadan
<g/>
,	,	kIx,	,
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
zničit	zničit	k5eAaPmF	zničit
hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
Gondor	Gondor	k1gInSc4	Gondor
a	a	k8xC	a
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Minas	Minas	k1gMnSc1	Minas
Ithil	Ithil	k1gMnSc1	Ithil
<g/>
.	.	kIx.	.
</s>
<s>
Isildur	Isildur	k1gMnSc1	Isildur
unikl	uniknout	k5eAaPmAgMnS	uniknout
z	z	k7c2	z
města	město	k1gNnSc2	město
a	a	k8xC	a
odplul	odplout	k5eAaPmAgMnS	odplout
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
Elendilem	Elendil	k1gMnSc7	Elendil
po	po	k7c6	po
Anduině	Anduina	k1gFnSc6	Anduina
<g/>
.	.	kIx.	.
</s>
<s>
Elendil	Elendit	k5eAaPmAgMnS	Elendit
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
Gil-galadem	Gilalad	k1gInSc7	Gil-galad
Poslední	poslední	k2eAgNnSc4d1	poslední
spojenectví	spojenectví	k1gNnSc4	spojenectví
elfů	elf	k1gMnPc2	elf
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
proti	proti	k7c3	proti
armádám	armáda	k1gFnPc3	armáda
Mordoru	Mordor	k1gInSc2	Mordor
a	a	k8xC	a
Sauronovi	Sauron	k1gMnSc3	Sauron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sauron	Sauron	k1gInSc1	Sauron
byl	být	k5eAaImAgInS	být
Posledním	poslední	k2eAgNnSc7d1	poslední
spojenectvím	spojenectví	k1gNnSc7	spojenectví
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Dagorladu	Dagorlad	k1gInSc6	Dagorlad
poražen	poražen	k2eAgMnSc1d1	poražen
a	a	k8xC	a
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Vojska	vojsko	k1gNnPc1	vojsko
Posledního	poslední	k2eAgNnSc2d1	poslední
spojenectví	spojenectví	k1gNnSc2	spojenectví
jej	on	k3xPp3gMnSc4	on
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
v	v	k7c6	v
Barad-dû	Baradû	k1gFnSc6	Barad-dû
<g/>
.	.	kIx.	.
</s>
<s>
Isildurův	Isildurův	k2eAgMnSc1d1	Isildurův
bratr	bratr	k1gMnSc1	bratr
Anárion	Anárion	k1gInSc1	Anárion
během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
zahynul	zahynout	k5eAaPmAgMnS	zahynout
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
kamenem	kámen	k1gInSc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgMnS	být
Sauron	Sauron	k1gMnSc1	Sauron
donucen	donutit	k5eAaPmNgMnS	donutit
vyjít	vyjít	k5eAaPmF	vyjít
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
pevnosti	pevnost	k1gFnSc2	pevnost
a	a	k8xC	a
bojovat	bojovat	k5eAaImF	bojovat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Hory	hora	k1gFnSc2	hora
osudu	osud	k1gInSc2	osud
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgMnS	střetnout
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
Elendilem	Elendil	k1gInSc7	Elendil
a	a	k8xC	a
Gil-galadem	Gilalad	k1gInSc7	Gil-galad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Isildur	Isildur	k1gMnSc1	Isildur
pak	pak	k6eAd1	pak
zlomeným	zlomený	k2eAgMnSc7d1	zlomený
Narsilem	Narsil	k1gMnSc7	Narsil
uťal	utít	k5eAaPmAgMnS	utít
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
a	a	k8xC	a
Sauron	Sauron	k1gInSc1	Sauron
ztratil	ztratit	k5eAaPmAgInS	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Isildur	Isildur	k1gMnSc1	Isildur
si	se	k3xPyFc3	se
poté	poté	k6eAd1	poté
vzal	vzít	k5eAaPmAgMnS	vzít
Sauronův	Sauronův	k2eAgInSc4d1	Sauronův
vládnoucí	vládnoucí	k2eAgInSc4d1	vládnoucí
prsten	prsten	k1gInSc4	prsten
jako	jako	k8xC	jako
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
však	však	k9	však
Isildura	Isildur	k1gMnSc4	Isildur
zradil	zradit	k5eAaPmAgMnS	zradit
a	a	k8xC	a
Isildur	Isildur	k1gMnSc1	Isildur
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
,	,	kIx,	,
když	když	k8xS	když
unikal	unikat	k5eAaImAgInS	unikat
přes	přes	k7c4	přes
Anduinu	Anduina	k1gFnSc4	Anduina
z	z	k7c2	z
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
Kosatcových	kosatcový	k2eAgNnPc6d1	kosatcové
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
pak	pak	k6eAd1	pak
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
zmizel	zmizet	k5eAaPmAgMnS	zmizet
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
hlubinách	hlubina	k1gFnPc6	hlubina
Anduiny	Anduin	k2eAgFnSc2d1	Anduin
nenašel	najít	k5eNaPmAgMnS	najít
hobit	hobit	k1gMnSc1	hobit
Déagol	Déagola	k1gFnPc2	Déagola
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
jej	on	k3xPp3gMnSc4	on
vzápětí	vzápětí	k6eAd1	vzápětí
násilím	násilí	k1gNnSc7	násilí
odebral	odebrat	k5eAaPmAgMnS	odebrat
jeho	jeho	k3xOp3gNnSc4	jeho
bratranec	bratranec	k1gMnSc1	bratranec
Sméagol	Sméagol	k1gInSc1	Sméagol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgInSc1	třetí
věk	věk	k1gInSc1	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
pak	pak	k6eAd1	pak
nadlouho	nadlouho	k6eAd1	nadlouho
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
stínů	stín	k1gInPc2	stín
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebyl	být	k5eNaImAgMnS	být
ve	v	k7c6	v
Třetím	třetí	k4xOgInSc6	třetí
věku	věk	k1gInSc6	věk
schopen	schopen	k2eAgMnSc1d1	schopen
nabýt	nabýt	k5eAaPmF	nabýt
znovu	znovu	k6eAd1	znovu
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Temném	temný	k2eAgInSc6d1	temný
hvozdu	hvozd	k1gInSc6	hvozd
postavil	postavit	k5eAaPmAgMnS	postavit
pevnost	pevnost	k1gFnSc4	pevnost
Dol	dol	k1gInSc4	dol
Guldur	Guldura	k1gFnPc2	Guldura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyčkával	vyčkávat	k5eAaImAgMnS	vyčkávat
<g/>
,	,	kIx,	,
až	až	k8xS	až
mu	on	k3xPp3gMnSc3	on
nazgû	nazgû	k?	nazgû
připraví	připravit	k5eAaPmIp3nS	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Zůstával	zůstávat	k5eAaImAgInS	zůstávat
však	však	k9	však
skryt	skryt	k1gInSc1	skryt
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
ho	on	k3xPp3gMnSc4	on
neodhalil	odhalit	k5eNaPmAgMnS	odhalit
čaroděj	čaroděj	k1gMnSc1	čaroděj
Gandalf	Gandalf	k1gMnSc1	Gandalf
a	a	k8xC	a
Galadriel	Galadriel	k1gMnSc1	Galadriel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Sméagol	Sméagol	k1gInSc1	Sméagol
díky	díky	k7c3	díky
moci	moc	k1gFnSc3	moc
Prstenu	prsten	k1gInSc2	prsten
získal	získat	k5eAaPmAgInS	získat
nepřirozeně	přirozeně	k6eNd1	přirozeně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
život	život	k1gInSc1	život
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgNnPc2d1	další
staletí	staletí	k1gNnPc2	staletí
se	se	k3xPyFc4	se
ukrýval	ukrývat	k5eAaImAgInS	ukrývat
v	v	k7c6	v
Mlžných	mlžný	k2eAgFnPc6d1	mlžná
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
však	však	k9	však
Prsten	prsten	k1gInSc1	prsten
ztratil	ztratit	k5eAaPmAgInS	ztratit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gNnSc4	on
nalezl	naleznout	k5eAaPmAgMnS	naleznout
jiný	jiný	k2eAgMnSc1d1	jiný
hobit	hobit	k1gMnSc1	hobit
<g/>
,	,	kIx,	,
Bilbo	Bilba	k1gFnSc5	Bilba
Pytlík	pytlík	k1gMnSc1	pytlík
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
odnesl	odnést	k5eAaPmAgMnS	odnést
do	do	k7c2	do
daleké	daleký	k2eAgFnSc2d1	daleká
země	zem	k1gFnSc2	zem
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Kraj	kraj	k1gInSc4	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
<g/>
,	,	kIx,	,
vypuzený	vypuzený	k2eAgMnSc1d1	vypuzený
z	z	k7c2	z
Dol	dol	k1gInSc4	dol
Gulduru	Guldura	k1gFnSc4	Guldura
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znovu	znovu	k6eAd1	znovu
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
Barad-dû	Baradû	k1gFnSc4	Barad-dû
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
hledat	hledat	k5eAaImF	hledat
Prsten	prsten	k1gInSc1	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Devítku	devítka	k1gFnSc4	devítka
nazgû	nazgû	k?	nazgû
v	v	k7c6	v
převleku	převlek	k1gInSc6	převlek
černých	černý	k2eAgMnPc2d1	černý
jezdců	jezdec	k1gMnPc2	jezdec
hledat	hledat	k5eAaImF	hledat
Kraj	kraj	k1gInSc4	kraj
a	a	k8xC	a
Bilbo	Bilba	k1gFnSc5	Bilba
Pytlíka	pytlík	k1gMnSc4	pytlík
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
mezitím	mezitím	k6eAd1	mezitím
vnitřně	vnitřně	k6eAd1	vnitřně
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prsten	prsten	k1gInSc1	prsten
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
získává	získávat	k5eAaImIp3nS	získávat
jakousi	jakýsi	k3yIgFnSc4	jakýsi
tajemnou	tajemný	k2eAgFnSc4d1	tajemná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
a	a	k8xC	a
přenechal	přenechat	k5eAaPmAgInS	přenechat
jej	on	k3xPp3gMnSc4	on
svému	svůj	k3xOyFgMnSc3	svůj
synovci	synovec	k1gMnSc3	synovec
Frodovi	Froda	k1gMnSc3	Froda
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc1	Frodo
jej	on	k3xPp3gInSc4	on
donesl	donést	k5eAaPmAgInS	donést
do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
zničení	zničení	k1gNnSc4	zničení
prstenu	prsten	k1gInSc2	prsten
a	a	k8xC	a
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
Společenstvo	společenstvo	k1gNnSc1	společenstvo
prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Sauron	Sauron	k1gMnSc1	Sauron
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
dlouho	dlouho	k6eAd1	dlouho
připravovanou	připravovaný	k2eAgFnSc4d1	připravovaná
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
měla	mít	k5eAaImAgFnS	mít
přinést	přinést	k5eAaPmF	přinést
konečné	konečný	k2eAgNnSc4d1	konečné
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
mocného	mocný	k2eAgMnSc4d1	mocný
spojence	spojenec	k1gMnSc4	spojenec
v	v	k7c6	v
čaroději	čaroděj	k1gMnSc6	čaroděj
Sarumanovi	Saruman	k1gMnSc6	Saruman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
sám	sám	k3xTgInSc4	sám
toužit	toužit	k5eAaImF	toužit
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
Prstenu	prsten	k1gInSc2	prsten
a	a	k8xC	a
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
získat	získat	k5eAaPmF	získat
jej	on	k3xPp3gMnSc4	on
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
tak	tak	k6eAd1	tak
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
svobodná	svobodný	k2eAgNnPc4d1	svobodné
lidská	lidský	k2eAgNnPc4d1	lidské
království	království	k1gNnPc4	království
-	-	kIx~	-
Saruman	Saruman	k1gMnSc1	Saruman
napadl	napadnout	k5eAaPmAgMnS	napadnout
Rohan	Rohan	k1gMnSc1	Rohan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
byl	být	k5eAaImAgInS	být
Rohiry	Rohira	k1gFnSc2	Rohira
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hlásky	hláska	k1gFnSc2	hláska
v	v	k7c6	v
Helmově	helmově	k6eAd1	helmově
žlebu	žleb	k1gInSc2	žleb
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nechal	nechat	k5eAaPmAgMnS	nechat
vyprovokovat	vyprovokovat	k5eAaPmF	vyprovokovat
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
Gondor	Gondor	k1gInSc4	Gondor
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
říši	říše	k1gFnSc4	říše
Dúnadanů	Dúnadan	k1gMnPc2	Dúnadan
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
<g/>
,	,	kIx,	,
dědic	dědic	k1gMnSc1	dědic
dúnadanských	dúnadanský	k2eAgMnPc2d1	dúnadanský
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
Sauron	Sauron	k1gMnSc1	Sauron
neměl	mít	k5eNaImAgMnS	mít
tušení	tušení	k1gNnSc4	tušení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
totiž	totiž	k9	totiž
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
odhalil	odhalit	k5eAaPmAgMnS	odhalit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chtěl	chtít	k5eAaImAgMnS	chtít
vyprázdnit	vyprázdnit	k5eAaPmF	vyprázdnit
Mordor	Mordor	k1gInSc4	Mordor
od	od	k7c2	od
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
tak	tak	k9	tak
Frodovi	Frodův	k2eAgMnPc1d1	Frodův
proniknout	proniknout	k5eAaPmF	proniknout
k	k	k7c3	k
Hoře	hora	k1gFnSc3	hora
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
se	se	k3xPyFc4	se
mylně	mylně	k6eAd1	mylně
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Aragorn	Aragorn	k1gInSc1	Aragorn
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
ovládnout	ovládnout	k5eAaPmF	ovládnout
Prsten	prsten	k1gInSc4	prsten
a	a	k8xC	a
převzít	převzít	k5eAaPmF	převzít
Sauronovu	Sauronův	k2eAgFnSc4d1	Sauronova
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
a	a	k8xC	a
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlá	rychlý	k2eAgFnSc1d1	rychlá
porážka	porážka	k1gFnSc1	porážka
Gondoru	Gondor	k1gInSc2	Gondor
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
nejen	nejen	k6eAd1	nejen
zlomit	zlomit	k5eAaPmF	zlomit
odpor	odpor	k1gInSc4	odpor
svobodných	svobodný	k2eAgMnPc2d1	svobodný
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
získat	získat	k5eAaPmF	získat
Prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
veliké	veliký	k2eAgFnSc6d1	veliká
bitvě	bitva	k1gFnSc6	bitva
<g/>
,	,	kIx,	,
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Pelennorských	Pelennorský	k2eAgNnPc6d1	Pelennorský
polích	pole	k1gNnPc6	pole
byl	být	k5eAaImAgMnS	být
však	však	k9	však
nečekaně	nečekaně	k6eAd1	nečekaně
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Gondor	Gondor	k1gMnSc1	Gondor
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
pomoci	pomoct	k5eAaPmF	pomoct
Rohirů	Rohir	k1gInPc2	Rohir
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zahuben	zahuben	k2eAgMnSc1d1	zahuben
Černokněžný	černokněžný	k2eAgMnSc1d1	černokněžný
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
nejmocnější	mocný	k2eAgFnSc1d3	nejmocnější
z	z	k7c2	z
nazgû	nazgû	k?	nazgû
<g/>
.	.	kIx.	.
</s>
<s>
Saurona	Saurona	k1gFnSc1	Saurona
porážka	porážka	k1gFnSc1	porážka
zaskočila	zaskočit	k5eAaPmAgFnS	zaskočit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
stála	stát	k5eAaImAgFnS	stát
nesmírná	smírný	k2eNgFnSc1d1	nesmírná
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gInSc1	Aragorn
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
lsti	lest	k1gFnSc6	lest
a	a	k8xC	a
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
s	s	k7c7	s
vojsky	vojsko	k1gNnPc7	vojsko
Západu	západ	k1gInSc2	západ
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
Gandalfa	Gandalf	k1gMnSc2	Gandalf
k	k	k7c3	k
Morannonu	Morannon	k1gMnSc3	Morannon
<g/>
,	,	kIx,	,
Černé	Černá	k1gFnSc3	Černá
bráně	brána	k1gFnSc3	brána
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
upoutal	upoutat	k5eAaPmAgMnS	upoutat
Sauronovu	Sauronův	k2eAgFnSc4d1	Sauronova
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
Frodovi	Froda	k1gMnSc3	Froda
splnit	splnit	k5eAaPmF	splnit
jeho	jeho	k3xOp3gInSc4	jeho
úkol	úkol	k1gInSc4	úkol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
si	se	k3xPyFc3	se
Frodo	Frodo	k1gNnSc4	Frodo
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
Mordoru	Mordor	k1gInSc2	Mordor
v	v	k7c6	v
Hoře	hora	k1gFnSc6	hora
osudu	osud	k1gInSc2	osud
nasadil	nasadit	k5eAaPmAgInS	nasadit
Prsten	prsten	k1gInSc1	prsten
<g/>
,	,	kIx,	,
poznal	poznat	k5eAaPmAgMnS	poznat
Sauron	Sauron	k1gMnSc1	Sauron
své	svůj	k3xOyFgNnSc4	svůj
smrtelné	smrtelný	k2eAgNnSc4d1	smrtelné
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc1	Frodo
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
Glumem	Glum	k1gInSc7	Glum
shodil	shodit	k5eAaPmAgMnS	shodit
Gluma	Gluma	k1gNnSc4	Gluma
s	s	k7c7	s
Prstenem	prsten	k1gInSc7	prsten
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
Hory	hora	k1gFnSc2	hora
osudu	osud	k1gInSc2	osud
a	a	k8xC	a
Prsten	prsten	k1gInSc1	prsten
pominul	pominout	k5eAaPmAgInS	pominout
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
také	také	k9	také
Sauron	Sauron	k1gInSc1	Sauron
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
neškodný	škodný	k2eNgMnSc1d1	neškodný
zlý	zlý	k2eAgMnSc1d1	zlý
duch	duch	k1gMnSc1	duch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
nikde	nikde	k6eAd1	nikde
nepředkládá	předkládat	k5eNaImIp3nS	předkládat
detailní	detailní	k2eAgFnPc4d1	detailní
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
Sauronově	Sauronův	k2eAgInSc6d1	Sauronův
vzhledu	vzhled	k1gInSc6	vzhled
během	během	k7c2	během
jeho	jeho	k3xOp3gFnPc2	jeho
různých	různý	k2eAgFnPc2d1	různá
inkarnací	inkarnace	k1gFnPc2	inkarnace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Silmarillionu	Silmarillion	k1gInSc2	Silmarillion
byl	být	k5eAaImAgInS	být
Sauron	Sauron	k1gMnSc1	Sauron
jako	jako	k8xS	jako
Maia	Maia	k1gMnSc1	Maia
schopen	schopen	k2eAgMnSc1d1	schopen
měnit	měnit	k5eAaImF	měnit
svůj	svůj	k3xOyFgInSc4	svůj
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
podobu	podoba	k1gFnSc4	podoba
svou	svůj	k3xOyFgFnSc7	svůj
vůlí	vůle	k1gFnSc7	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
používal	používat	k5eAaImAgMnS	používat
krásnou	krásný	k2eAgFnSc4d1	krásná
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
Morgothovi	Morgoth	k1gMnSc3	Morgoth
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
hrozivou	hrozivý	k2eAgFnSc4d1	hrozivá
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
plánoval	plánovat	k5eAaImAgMnS	plánovat
přemoci	přemoct	k5eAaPmF	přemoct
valinorského	valinorský	k2eAgMnSc4d1	valinorský
psa	pes	k1gMnSc4	pes
Huana	Huan	k1gMnSc4	Huan
<g/>
,	,	kIx,	,
Sauron	Sauron	k1gInSc4	Sauron
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vzal	vzít	k5eAaPmAgMnS	vzít
podobu	podoba	k1gFnSc4	podoba
velikého	veliký	k2eAgMnSc2d1	veliký
vlka	vlk	k1gMnSc2	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jeho	jeho	k3xOp3gInSc1	jeho
plán	plán	k1gInSc1	plán
selhal	selhat	k5eAaPmAgInS	selhat
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
podobu	podoba	k1gFnSc4	podoba
hada	had	k1gMnSc2	had
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgMnS	změnit
se	se	k3xPyFc4	se
v	v	k7c4	v
ptáka	pták	k1gMnSc4	pták
a	a	k8xC	a
uletěl	uletět	k5eAaPmAgMnS	uletět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Morgothově	Morgothův	k2eAgFnSc6d1	Morgothova
porážce	porážka	k1gFnSc6	porážka
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
hněvu	hněv	k1gInSc2	hněv
na	na	k7c6	na
sebe	sebe	k3xPyFc4	sebe
Sauron	Sauron	k1gMnSc1	Sauron
vzal	vzít	k5eAaPmAgMnS	vzít
krásnou	krásný	k2eAgFnSc7d1	krásná
podobou	podoba	k1gFnSc7	podoba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
okouzlil	okouzlit	k5eAaPmAgMnS	okouzlit
Eönwëho	Eönwë	k1gMnSc4	Eönwë
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Druhém	druhý	k4xOgInSc6	druhý
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
krásné	krásný	k2eAgFnSc6d1	krásná
podobě	podoba	k1gFnSc6	podoba
jako	jako	k8xC	jako
Annatar	Annatar	k1gInSc4	Annatar
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přelstil	přelstít	k5eAaPmAgInS	přelstít
elfy	elf	k1gMnPc4	elf
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Númenoru	Númenor	k1gInSc6	Númenor
si	se	k3xPyFc3	se
také	také	k9	také
ponechal	ponechat	k5eAaPmAgMnS	ponechat
tento	tento	k3xDgInSc4	tento
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tak	tak	k9	tak
jako	jako	k8xC	jako
Morgoth	Morgoth	k1gMnSc1	Morgoth
Sauron	Sauron	k1gMnSc1	Sauron
nakonec	nakonec	k6eAd1	nakonec
ztratil	ztratit	k5eAaPmAgMnS	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
měnit	měnit	k5eAaImF	měnit
svůj	svůj	k3xOyFgInSc4	svůj
vzhled	vzhled	k1gInSc4	vzhled
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Pádu	Pád	k1gInSc6	Pád
Númenoru	Númenor	k1gInSc2	Númenor
nebyl	být	k5eNaImAgMnS	být
Sauron	Sauron	k1gMnSc1	Sauron
schopen	schopen	k2eAgMnSc1d1	schopen
vzít	vzít	k5eAaPmF	vzít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
krásnou	krásný	k2eAgFnSc4d1	krásná
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
Druhého	druhý	k4xOgInSc2	druhý
věku	věk	k1gInSc2	věk
a	a	k8xC	a
ve	v	k7c6	v
Třetím	třetí	k4xOgInSc6	třetí
věku	věk	k1gInSc6	věk
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vzal	vzít	k5eAaPmAgMnS	vzít
hrozivou	hrozivý	k2eAgFnSc4d1	hrozivá
podobu	podoba	k1gFnSc4	podoba
Temného	temný	k2eAgMnSc2d1	temný
pána	pán	k1gMnSc2	pán
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vzhled	vzhled	k1gInSc1	vzhled
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Númenoru	Númenor	k1gInSc2	Númenor
byl	být	k5eAaImAgMnS	být
příšerný	příšerný	k2eAgMnSc1d1	příšerný
–	–	k?	–
"	"	kIx"	"
<g/>
obraz	obraz	k1gInSc1	obraz
zosobněné	zosobněný	k2eAgFnSc2d1	zosobněná
zášti	zášť	k1gFnSc2	zášť
a	a	k8xC	a
nenávisti	nenávist	k1gFnSc2	nenávist
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Isildur	Isildur	k1gMnSc1	Isildur
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sauronova	Sauronův	k2eAgFnSc1d1	Sauronova
ruka	ruka	k1gFnSc1	ruka
"	"	kIx"	"
<g/>
byla	být	k5eAaImAgFnS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
a	a	k8xC	a
přece	přece	k9	přece
pálila	pálit	k5eAaImAgFnS	pálit
jako	jako	k9	jako
oheň	oheň	k1gInSc4	oheň
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gil-galad	Gilalad	k1gInSc1	Gil-galad
zahynul	zahynout	k5eAaPmAgInS	zahynout
Sauronovým	Sauronův	k2eAgInSc7d1	Sauronův
žárem	žár	k1gInSc7	žár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sauronovo	Sauronův	k2eAgNnSc1d1	Sauronovo
oko	oko	k1gNnSc1	oko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Oko	oko	k1gNnSc1	oko
<g/>
"	"	kIx"	"
obrazem	obraz	k1gInSc7	obraz
asociovaným	asociovaný	k2eAgMnSc7d1	asociovaný
se	se	k3xPyFc4	se
Sauronem	Sauron	k1gInSc7	Sauron
<g/>
.	.	kIx.	.
</s>
<s>
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
skřeti	skřet	k1gMnPc1	skřet
nosili	nosit	k5eAaImAgMnP	nosit
symbol	symbol	k1gInSc4	symbol
oka	oko	k1gNnSc2	oko
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
helmách	helma	k1gFnPc6	helma
a	a	k8xC	a
štítech	štít	k1gInPc6	štít
a	a	k8xC	a
nazývali	nazývat	k5eAaImAgMnP	nazývat
ho	on	k3xPp3gNnSc4	on
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Oko	oko	k1gNnSc1	oko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
nedovolil	dovolit	k5eNaPmAgInS	dovolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
nebo	nebo	k8xC	nebo
vyřčeno	vyřčen	k2eAgNnSc1d1	vyřčeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
Pán	pán	k1gMnSc1	pán
nazgû	nazgû	k?	nazgû
hrozil	hrozit	k5eAaImAgMnS	hrozit
Éowyn	Éowyn	k1gInSc4	Éowyn
mučením	mučení	k1gNnSc7	mučení
před	před	k7c7	před
"	"	kIx"	"
<g/>
Okem	oke	k1gNnSc7	oke
bez	bez	k7c2	bez
víček	víčko	k1gNnPc2	víčko
<g/>
"	"	kIx"	"
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Pelennorských	Pelennorský	k2eAgNnPc6d1	Pelennorský
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
Galadriel	Galadriela	k1gFnPc2	Galadriela
Frodo	Frodo	k1gNnSc4	Frodo
viděl	vidět	k5eAaImAgInS	vidět
Oko	oko	k1gNnSc4	oko
a	a	k8xC	a
Saurona	Saurona	k1gFnSc1	Saurona
v	v	k7c6	v
něm.	něm.	k?	něm.
</s>
</p>
<p>
<s>
==	==	k?	==
Jména	jméno	k1gNnPc1	jméno
a	a	k8xC	a
tituly	titul	k1gInPc1	titul
==	==	k?	==
</s>
</p>
<p>
<s>
Sauronovo	Sauronův	k2eAgNnSc1d1	Sauronovo
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
suera	suero	k1gNnSc2	suero
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
odporný	odporný	k2eAgInSc1d1	odporný
<g/>
,	,	kIx,	,
mizerný	mizerný	k2eAgInSc1d1	mizerný
<g/>
,	,	kIx,	,
zkažený	zkažený	k2eAgInSc1d1	zkažený
<g/>
"	"	kIx"	"
v	v	k7c6	v
quenijštině	quenijština	k1gFnSc6	quenijština
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
překládáno	překládán	k2eAgNnSc1d1	překládáno
jako	jako	k8xC	jako
Nenáviděný	nenáviděný	k2eAgMnSc1d1	nenáviděný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sindarštině	sindarština	k1gFnSc6	sindarština
je	být	k5eAaImIp3nS	být
nazýván	nazýván	k2eAgMnSc1d1	nazýván
Gorthaur	Gorthaur	k1gMnSc1	Gorthaur
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
nazýván	nazývat	k5eAaImNgInS	nazývat
Bezejmenným	bezejmenný	k2eAgMnSc7d1	bezejmenný
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Dúnadani	Dúnadan	k1gMnPc1	Dúnadan
jej	on	k3xPp3gMnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
Sauron	Sauron	k1gMnSc1	Sauron
Podvodník	podvodník	k1gMnSc1	podvodník
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
roli	role	k1gFnSc3	role
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
Prstenů	prsten	k1gInPc2	prsten
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
Pádu	Pád	k1gInSc2	Pád
Númenoru	Númenor	k1gInSc2	Númenor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Adû	Adû	k1gFnSc6	Adû
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
Zigû	Zigû	k1gFnPc3	Zigû
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
čaroděj	čaroděj	k1gMnSc1	čaroděj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
Tolkienových	Tolkienův	k2eAgFnPc6d1	Tolkienova
poznámkách	poznámka	k1gFnPc6	poznámka
je	být	k5eAaImIp3nS	být
Sauron	Sauron	k1gInSc1	Sauron
původně	původně	k6eAd1	původně
nazýván	nazývat	k5eAaImNgInS	nazývat
jménem	jméno	k1gNnSc7	jméno
Mairon	Mairon	k1gInSc1	Mairon
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Obdivuhodný	obdivuhodný	k2eAgMnSc1d1	obdivuhodný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
Tar-Mairon	Tar-Mairon	k1gInSc1	Tar-Mairon
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
Excelentní	excelentní	k2eAgMnSc1d1	excelentní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
pádu	pád	k1gInSc2	pád
Númenoru	Númenor	k1gInSc2	Númenor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
nejznámější	známý	k2eAgInPc1d3	nejznámější
tituly	titul	k1gInPc1	titul
Temný	temný	k2eAgMnSc1d1	temný
pán	pán	k1gMnSc1	pán
Mordoru	Mordor	k1gInSc2	Mordor
a	a	k8xC	a
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
jenom	jenom	k9	jenom
párkrát	párkrát	k6eAd1	párkrát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
Temným	temný	k2eAgMnSc7d1	temný
pánem	pán	k1gMnSc7	pán
<g/>
,	,	kIx,	,
Pánem	pán	k1gMnSc7	pán
zrady	zrada	k1gFnSc2	zrada
<g/>
,	,	kIx,	,
Temnou	temný	k2eAgFnSc7d1	temná
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
Temnou	temný	k2eAgFnSc7d1	temná
mocností	mocnost	k1gFnSc7	mocnost
<g/>
,	,	kIx,	,
Nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
Pánem	pán	k1gMnSc7	pán
Barad-dû	Baradû	k1gFnSc2	Barad-dû
<g/>
,	,	kIx,	,
Rudým	rudý	k2eAgNnSc7d1	Rudé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
Černým	černý	k2eAgMnSc7d1	černý
mágem	mág	k1gMnSc7	mág
a	a	k8xC	a
Černokněžníkem	černokněžník	k1gMnSc7	černokněžník
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
věku	věk	k1gInSc6	věk
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
Pánem	pán	k1gMnSc7	pán
vlkodlaků	vlkodlak	k1gMnPc2	vlkodlak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Druhém	druhý	k4xOgInSc6	druhý
věku	věk	k1gInSc6	věk
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
jméno	jméno	k1gNnSc4	jméno
Annatar	Annatara	k1gFnPc2	Annatara
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Pán	pán	k1gMnSc1	pán
darů	dar	k1gInPc2	dar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Aulendil	Aulendil	k1gFnSc1	Aulendil
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
Aulëho	Aulë	k1gMnSc2	Aulë
přítel	přítel	k1gMnSc1	přítel
<g/>
"	"	kIx"	"
a	a	k8xC	a
Artano	Artana	k1gFnSc5	Artana
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Vznešený	vznešený	k2eAgMnSc1d1	vznešený
kovář	kovář	k1gMnSc1	kovář
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Třetím	třetí	k4xOgInSc6	třetí
věku	věk	k1gInSc6	věk
byl	být	k5eAaImAgMnS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k9	jako
Nekromant	nekromant	k1gMnSc1	nekromant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sauronova	Sauronův	k2eAgFnSc1d1	Sauronova
moc	moc	k1gFnSc1	moc
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc1	první
věk	věk	k1gInSc1	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Měl	mít	k5eAaImAgMnS	mít
díl	díl	k1gInSc4	díl
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
činech	čin	k1gInPc6	čin
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
na	na	k7c6	na
Ardě	Ard	k1gInSc6	Ard
vykonal	vykonat	k5eAaPmAgInS	vykonat
Melkor	Melkor	k1gInSc1	Melkor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
To	to	k9	to
on	on	k3xPp3gMnSc1	on
vypěstoval	vypěstovat	k5eAaPmAgMnS	vypěstovat
velké	velká	k1gFnPc4	velká
armády	armáda	k1gFnSc2	armáda
skřetů	skřet	k1gMnPc2	skřet
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
byl	být	k5eAaImAgInS	být
Melkor	Melkor	k1gInSc1	Melkor
zajat	zajmout	k5eAaPmNgInS	zajmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
On	on	k3xPp3gMnSc1	on
také	také	k9	také
dokončoval	dokončovat	k5eAaImAgMnS	dokončovat
a	a	k8xC	a
usměrňoval	usměrňovat	k5eAaImAgMnS	usměrňovat
většinu	většina	k1gFnSc4	většina
plánů	plán	k1gInPc2	plán
Melkora	Melkor	k1gMnSc2	Melkor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
nemohl	moct	k5eNaImAgMnS	moct
nebo	nebo	k8xC	nebo
nechtěl	chtít	k5eNaImAgMnS	chtít
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podával	podávat	k5eAaImAgMnS	podávat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
co	co	k8xS	co
Valar	Valar	k1gInSc4	Valar
dělali	dělat	k5eAaImAgMnP	dělat
v	v	k7c6	v
Almarenu	Almaren	k1gInSc6	Almaren
i	i	k8xC	i
v	v	k7c6	v
Amanu	Aman	k1gMnSc6	Aman
<g/>
,	,	kIx,	,
Melkorovi	Melkor	k1gMnSc6	Melkor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tak	tak	k6eAd1	tak
vystihl	vystihnout	k5eAaPmAgMnS	vystihnout
čas	čas	k1gInSc4	čas
na	na	k7c4	na
zničení	zničení	k1gNnSc4	zničení
Dvou	dva	k4xCgFnPc2	dva
Lamp	lampa	k1gFnPc2	lampa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pochytal	pochytat	k5eAaPmAgMnS	pochytat
a	a	k8xC	a
zabil	zabít	k5eAaPmAgMnS	zabít
převážnou	převážný	k2eAgFnSc4d1	převážná
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
Berenových	Berenův	k2eAgMnPc2d1	Berenův
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
zabít	zabít	k5eAaPmF	zabít
velkého	velký	k2eAgMnSc4d1	velký
elfího	elfí	k1gMnSc4	elfí
krále	král	k1gMnSc4	král
Felagunda	Felagund	k1gMnSc4	Felagund
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oklamal	oklamat	k5eAaPmAgMnS	oklamat
Eönwëho	Eönwë	k1gMnSc2	Eönwë
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhý	druhý	k4xOgInSc1	druhý
věk	věk	k1gInSc1	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Získal	získat	k5eAaPmAgInS	získat
moc	moc	k6eAd1	moc
prakticky	prakticky	k6eAd1	prakticky
nad	nad	k7c7	nad
celým	celý	k2eAgInSc7d1	celý
Východem	východ	k1gInSc7	východ
a	a	k8xC	a
Jihem	jih	k1gInSc7	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
největší	veliký	k2eAgFnSc4d3	veliký
věž	věž	k1gFnSc4	věž
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Thangorodrim	Thangorodrima	k1gFnPc2	Thangorodrima
<g/>
,	,	kIx,	,
Barad-dû	Baradû	k1gFnPc2	Barad-dû
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oklamal	oklamat	k5eAaPmAgMnS	oklamat
elfy	elf	k1gMnPc4	elf
(	(	kIx(	(
<g/>
při	při	k7c6	při
rozdávání	rozdávání	k1gNnSc6	rozdávání
Prstenů	prsten	k1gInPc2	prsten
i	i	k8xC	i
trpaslíky	trpaslík	k1gMnPc4	trpaslík
a	a	k8xC	a
lidi	člověk	k1gMnPc4	člověk
<g/>
)	)	kIx)	)
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vytvořit	vytvořit	k5eAaPmF	vytvořit
Prsteny	prsten	k1gInPc4	prsten
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
<g/>
,	,	kIx,	,
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
válku	válka	k1gFnSc4	válka
elfům	elf	k1gMnPc3	elf
<g/>
,	,	kIx,	,
vyplenil	vyplenit	k5eAaPmAgInS	vyplenit
Eregion	Eregion	k1gInSc1	Eregion
<g/>
,	,	kIx,	,
Morie	Morie	k1gFnSc1	Morie
se	se	k3xPyFc4	se
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
zabít	zabít	k5eAaPmF	zabít
Celebrimbora	Celebrimbor	k1gMnSc4	Celebrimbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
dosah	dosah	k1gInSc4	dosah
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Prstenové	prstenový	k2eAgInPc4d1	prstenový
přízraky	přízrak	k1gInPc4	přízrak
devíti	devět	k4xCc3	devět
prsteny	prsten	k1gInPc4	prsten
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
mocných	mocný	k2eAgMnPc2d1	mocný
lidských	lidský	k2eAgMnPc2d1	lidský
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
tři	tři	k4xCgInPc1	tři
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
velikých	veliký	k2eAgMnPc2d1	veliký
Númenorejců	Númenorejec	k1gMnPc2	Númenorejec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Způsobil	způsobit	k5eAaPmAgInS	způsobit
zánik	zánik	k1gInSc1	zánik
Númenoru	Númenora	k1gFnSc4	Númenora
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
drtivé	drtivý	k2eAgFnSc2d1	drtivá
většiny	většina	k1gFnSc2	většina
jeho	on	k3xPp3gInSc2	on
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
o	o	k7c4	o
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
zabil	zabít	k5eAaPmAgMnS	zabít
sám	sám	k3xTgMnSc1	sám
Gil-Galada	Gil-Galada	k1gFnSc1	Gil-Galada
a	a	k8xC	a
Elendila	Elendil	k1gMnSc4	Elendil
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
useknutí	useknutí	k1gNnSc6	useknutí
prstů	prst	k1gInPc2	prst
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
synem	syn	k1gMnSc7	syn
Elendilovým	Elendilův	k2eAgMnSc7d1	Elendilův
(	(	kIx(	(
<g/>
Isildurem	Isildur	k1gMnSc7	Isildur
<g/>
)	)	kIx)	)
ztratil	ztratit	k5eAaPmAgInS	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgInSc1	třetí
věk	věk	k1gInSc1	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
podruhé	podruhé	k6eAd1	podruhé
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
dosah	dosah	k1gInSc4	dosah
ovládnutí	ovládnutí	k1gNnSc2	ovládnutí
celé	celý	k2eAgFnSc2d1	celá
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Dol-Gulduru	Dol-Guldur	k1gInSc2	Dol-Guldur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
znovu	znovu	k6eAd1	znovu
vybudovat	vybudovat	k5eAaPmF	vybudovat
Barad-Dur	Barad-Dura	k1gFnPc2	Barad-Dura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyšlechtil	vyšlechtit	k5eAaPmAgMnS	vyšlechtit
Olog-hai	Ologa	k1gFnPc4	Olog-ha
<g/>
,	,	kIx,	,
obry	obr	k1gMnPc4	obr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dokázali	dokázat	k5eAaPmAgMnP	dokázat
snést	snést	k5eAaPmF	snést
sluneční	sluneční	k2eAgNnSc4d1	sluneční
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prsten	prsten	k1gInSc1	prsten
zničen	zničen	k2eAgInSc1d1	zničen
a	a	k8xC	a
Sauron	Sauron	k1gInSc1	Sauron
navždy	navždy	k6eAd1	navždy
opouští	opouštět	k5eAaImIp3nS	opouštět
středozem	středoz	k1gInSc7	středoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
literární	literární	k2eAgFnSc2d1	literární
postavy	postava	k1gFnSc2	postava
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
velký	velký	k2eAgMnSc1d1	velký
teropodní	teropodní	k2eAgMnSc1d1	teropodní
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
Sauroniops	Sauroniopsa	k1gFnPc2	Sauroniopsa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Sauronovo	Sauronův	k2eAgNnSc1d1	Sauronovo
oko	oko	k1gNnSc1	oko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Sauron	Sauron	k1gInSc1	Sauron
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Hobit	hobit	k1gMnSc1	hobit
aneb	aneb	k?	aneb
cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
František	František	k1gMnSc1	František
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
262	[number]	k4	262
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Nedokončené	dokončený	k2eNgInPc4d1	nedokončený
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Christopher	Christophra	k1gFnPc2	Christophra
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Stanislava	Stanislav	k1gMnSc2	Stanislav
Pošustová	Pošustová	k1gFnSc1	Pošustová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
453	[number]	k4	453
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Christopher	Christophra	k1gFnPc2	Christophra
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Stanislava	Stanislav	k1gMnSc2	Stanislav
Pošustová	Pošustová	k1gFnSc1	Pošustová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
336	[number]	k4	336
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
:	:	kIx,	:
Společenstvo	společenstvo	k1gNnSc1	společenstvo
prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Stanislava	Stanislav	k1gMnSc2	Stanislav
Pošustová	Pošustová	k1gFnSc1	Pošustová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
105	[number]	k4	105
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Letters	Letters	k1gInSc1	Letters
of	of	k?	of
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Humphrey	Humphrea	k1gFnSc2	Humphrea
Carpenter	Carpentra	k1gFnPc2	Carpentra
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
<g/>
:	:	kIx,	:
Houghton	Houghton	k1gInSc1	Houghton
Mifflin	Mifflin	k2eAgInSc1d1	Mifflin
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
395	[number]	k4	395
<g/>
-	-	kIx~	-
<g/>
31555	[number]	k4	31555
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Lost	Lost	k2eAgMnSc1d1	Lost
Road	Road	k1gMnSc1	Road
and	and	k?	and
Other	Other	k1gInSc1	Other
Writings	Writings	k1gInSc1	Writings
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Christopher	Christophra	k1gFnPc2	Christophra
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
HarperCollins	HarperCollins	k1gInSc1	HarperCollins
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
Middle	Middle	k1gMnSc4	Middle
Earth	Earth	k1gMnSc1	Earth
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
261	[number]	k4	261
<g/>
-	-	kIx~	-
<g/>
10225	[number]	k4	10225
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Morgoth	Morgoth	k1gInSc1	Morgoth
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ring	ring	k1gInSc1	ring
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Christopher	Christophra	k1gFnPc2	Christophra
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
HarperCollins	HarperCollins	k1gInSc1	HarperCollins
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
Middle	Middle	k1gMnSc4	Middle
Earth	Earth	k1gMnSc1	Earth
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
261	[number]	k4	261
<g/>
-	-	kIx~	-
<g/>
10300	[number]	k4	10300
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Morgoth	Morgoth	k1gMnSc1	Morgoth
</s>
</p>
<p>
<s>
Mordor	Mordor	k1gMnSc1	Mordor
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
o	o	k7c4	o
Prsten	prsten	k1gInSc4	prsten
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
spojenectví	spojenectví	k1gNnSc4	spojenectví
elfů	elf	k1gMnPc2	elf
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
</s>
</p>
<p>
<s>
Prsteny	prsten	k1gInPc1	prsten
moci	moc	k1gFnSc2	moc
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sauron	Sauron	k1gInSc1	Sauron
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sauron	Sauron	k1gInSc1	Sauron
na	na	k7c4	na
Tolkien	Tolkien	k1gInSc4	Tolkien
Gateway	Gatewaa	k1gFnSc2	Gatewaa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sauron	Sauron	k1gInSc1	Sauron
na	na	k7c4	na
LotrWikia	LotrWikius	k1gMnSc4	LotrWikius
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sauron	Sauron	k1gInSc1	Sauron
na	na	k7c4	na
The	The	k1gFnPc4	The
Thainově	Thainův	k2eAgFnSc3d1	Thainův
Knize	kniha	k1gFnSc3	kniha
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Sauron	Sauron	k1gInSc1	Sauron
na	na	k7c4	na
Ardapedii	Ardapedie	k1gFnSc4	Ardapedie
</s>
</p>
