<s>
Watt	watt	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Watt	watt	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Watt	watt	k1gInSc1
<g/>
,	,	kIx,
W	W	kA
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
výkonu	výkon	k1gInSc2
v	v	k7c6
SI	si	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotka	jednotka	k1gFnSc1
je	být	k5eAaImIp3nS
pojmenována	pojmenovat	k5eAaPmNgFnS
podle	podle	k7c2
skotského	skotský	k1gInSc2
inženýra	inženýr	k1gMnSc2
Jamese	Jamese	k1gFnSc2
Watta	Watt	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
watt	watt	k1gInSc1
je	být	k5eAaImIp3nS
výkon	výkon	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
vykoná	vykonat	k5eAaPmIp3nS
práce	práce	k1gFnSc2
1	#num#	k4
joulu	joule	k1gInSc2
za	za	k7c4
1	#num#	k4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
výkon	výkon	k1gInSc4
potřebný	potřebný	k2eAgInSc4d1
například	například	k6eAd1
pro	pro	k7c4
zvedání	zvedání	k1gNnSc4
tělesa	těleso	k1gNnSc2
o	o	k7c6
tíze	tíha	k1gFnSc6
1	#num#	k4
newton	newton	k1gInSc4
(	(	kIx(
<g/>
tj.	tj.	kA
tělesa	těleso	k1gNnPc4
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
přibližně	přibližně	k6eAd1
101,97	101,97	k4
gramů	gram	k1gInPc2
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
normálním	normální	k2eAgNnSc6d1
tíhovém	tíhový	k2eAgNnSc6d1
poli	pole	k1gNnSc6
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
)	)	kIx)
rovnoměrně	rovnoměrně	k6eAd1
svisle	svisle	k6eAd1
rychlostí	rychlost	k1gFnSc7
1	#num#	k4
metr	metr	k1gInSc1
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Elektrický	elektrický	k2eAgInSc4d1
výkon	výkon	k1gInSc4
1	#num#	k4
watt	watt	k1gInSc1
má	mít	k5eAaImIp3nS
stejnosměrný	stejnosměrný	k2eAgInSc1d1
proud	proud	k1gInSc1
1	#num#	k4
ampéru	ampér	k1gInSc2
při	při	k7c6
úbytku	úbytek	k1gInSc6
napětí	napětí	k1gNnSc2
1	#num#	k4
voltu	volt	k1gInSc2
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
jest	být	k5eAaImIp3nS
<g/>
,	,	kIx,
podle	podle	k7c2
Ohmova	Ohmův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
odporu	odpor	k1gInSc6
1	#num#	k4
ohmu	ohm	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozměr	rozměr	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
W	W	kA
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
J	J	kA
</s>
<s>
s	s	k7c7
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
N	N	kA
</s>
<s>
⋅	⋅	k?
</s>
<s>
m	m	kA
</s>
<s>
s	s	k7c7
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
g	g	kA
</s>
<s>
⋅	⋅	k?
</s>
<s>
m	m	kA
</s>
<s>
2	#num#	k4
</s>
<s>
s	s	k7c7
</s>
<s>
3	#num#	k4
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
1	#num#	k4
<g/>
\	\	kIx~
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
W	W	kA
<g/>
}	}	kIx)
=	=	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
{	{	kIx(
<g/>
\	\	kIx~
<g/>
dfrac	dfrac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
J	J	kA
<g/>
}	}	kIx)
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
s	s	k7c7
<g/>
}	}	kIx)
}}	}}	k?
<g/>
=	=	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
\	\	kIx~
{	{	kIx(
<g/>
\	\	kIx~
<g/>
dfrac	dfrac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
N	N	kA
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
m	m	kA
<g/>
}	}	kIx)
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
s	s	k7c7
<g/>
}	}	kIx)
}}	}}	k?
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
{	{	kIx(
<g/>
\	\	kIx~
<g/>
dfrac	dfrac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
kg	kg	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
m	m	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
s	s	k7c7
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
}}	}}	k?
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Násobky	násobek	k1gInPc1
</s>
<s>
V	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
velmi	velmi	k6eAd1
často	často	k6eAd1
používají	používat	k5eAaImIp3nP
násobky	násobek	k1gInPc1
a	a	k8xC
díly	díl	k1gInPc1
této	tento	k3xDgFnSc2
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
mW	mW	k?
–	–	k?
miliwatt	miliwatt	k1gInSc1
–	–	k?
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
W	W	kA
=	=	kIx~
0,001	0,001	k4
W	W	kA
</s>
<s>
kW	kW	kA
–	–	k?
kilowatt	kilowatt	k1gInSc1
–	–	k?
103	#num#	k4
W	W	kA
=	=	kIx~
1000	#num#	k4
W	W	kA
</s>
<s>
MW	MW	kA
–	–	k?
megawatt	megawatt	k1gInSc1
–	–	k?
106	#num#	k4
W	W	kA
=	=	kIx~
1	#num#	k4
000	#num#	k4
000	#num#	k4
W	W	kA
</s>
<s>
GW	GW	kA
–	–	k?
gigawatt	gigawatt	k1gInSc1
–	–	k?
109	#num#	k4
W	W	kA
=	=	kIx~
1	#num#	k4
000	#num#	k4
000	#num#	k4
000	#num#	k4
W	W	kA
</s>
<s>
TW	TW	kA
–	–	k?
terawatt	terawatt	k1gInSc1
–	–	k?
1012	#num#	k4
W	W	kA
=	=	kIx~
1	#num#	k4
000	#num#	k4
000	#num#	k4
000	#num#	k4
000	#num#	k4
W	W	kA
</s>
<s>
Indexování	indexování	k1gNnSc1
</s>
<s>
V	v	k7c6
technické	technický	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
lze	lze	k6eAd1
často	často	k6eAd1
setkat	setkat	k5eAaPmF
zejména	zejména	k9
s	s	k7c7
indexy	index	k1gInPc7
„	„	k?
<g/>
e	e	k0
<g/>
“	“	k?
či	či	k8xC
„	„	k?
<g/>
t	t	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
ve	v	k7c6
tvaru	tvar	k1gInSc6
We	We	k1gFnSc2
a	a	k8xC
Wt	Wt	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
We	We	k1gMnSc4
a	a	k8xC
Wt	Wt	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
dělení	dělení	k1gNnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
u	u	k7c2
tepelných	tepelný	k2eAgInPc2d1
závodů	závod	k1gInPc2
(	(	kIx(
<g/>
teplárny	teplárna	k1gFnPc1
<g/>
,	,	kIx,
elektrárny	elektrárna	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
smysl	smysl	k1gInSc4
rozdělovat	rozdělovat	k5eAaImF
celkový	celkový	k2eAgInSc4d1
výkon	výkon	k1gInSc4
na	na	k7c4
tepelný	tepelný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
(	(	kIx(
<g/>
s	s	k7c7
indexem	index	k1gInSc7
t	t	k?
<g/>
)	)	kIx)
a	a	k8xC
elektrický	elektrický	k2eAgInSc4d1
výkon	výkon	k1gInSc4
(	(	kIx(
<g/>
s	s	k7c7
indexem	index	k1gInSc7
e	e	k0
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Symboly	symbol	k1gInPc1
používané	používaný	k2eAgInPc1d1
např.	např.	kA
u	u	k7c2
elektráren	elektrárna	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
MWe	MWe	k1gFnSc1
je	být	k5eAaImIp3nS
elektrický	elektrický	k2eAgInSc4d1
výkon	výkon	k1gInSc4
generátoru	generátor	k1gInSc2
a	a	k8xC
MWt	MWt	k1gFnSc2
je	být	k5eAaImIp3nS
tepelný	tepelný	k2eAgInSc1d1
výkon	výkon	k1gInSc1
nutný	nutný	k2eAgInSc1d1
pro	pro	k7c4
provoz	provoz	k1gInSc4
generátorů	generátor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tepelný	tepelný	k2eAgInSc1d1
výkon	výkon	k1gInSc1
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
přibližně	přibližně	k6eAd1
třikrát	třikrát	k6eAd1
větší	veliký	k2eAgInSc4d2
než	než	k8xS
elektrický	elektrický	k2eAgInSc4d1
výkon	výkon	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
U	u	k7c2
solární	solární	k2eAgFnSc2d1
energetiky	energetika	k1gFnSc2
se	se	k3xPyFc4
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
s	s	k7c7
indexem	index	k1gInSc7
„	„	k?
<g/>
p	p	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
např.	např.	kA
5	#num#	k4
kWp	kWp	k?
–	–	k?
kilowatt-peak	kilowatt-peak	k1gInSc1
<g/>
)	)	kIx)
k	k	k7c3
označení	označení	k1gNnSc3
špičkového	špičkový	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
elektrárny	elektrárna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Indexování	indexování	k1gNnSc1
jednotek	jednotka	k1gFnPc2
není	být	k5eNaImIp3nS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
pravidly	pravidlo	k1gNnPc7
SI	si	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
podobnou	podobný	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
má	mít	k5eAaImIp3nS
nést	nést	k5eAaImF
název	název	k1gInSc1
či	či	k8xC
značka	značka	k1gFnSc1
veličiny	veličina	k1gFnSc2
–	–	k?
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
totiž	totiž	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
daném	daný	k2eAgInSc6d1
případě	případ	k1gInSc6
odlišné	odlišný	k2eAgNnSc1d1
<g/>
;	;	kIx,
jednotka	jednotka	k1gFnSc1
jako	jako	k8xS,k8xC
taková	takový	k3xDgFnSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
stejná	stejný	k2eAgFnSc1d1
<g/>
,	,	kIx,
velikostí	velikost	k1gFnSc7
je	být	k5eAaImIp3nS
1	#num#	k4
We	We	k1gFnPc2
=	=	kIx~
1	#num#	k4
Wt	Wt	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
m	m	kA
=	=	kIx~
F	F	kA
/	/	kIx~
g	g	kA
=	=	kIx~
1	#num#	k4
/	/	kIx~
9,806	9,806	k4
<g/>
65	#num#	k4
kg	kg	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KRBEK	krbek	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
;	;	kIx,
POLESNÝ	polesný	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kogenerační	kogenerační	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
-	-	kIx~
zřizování	zřizování	k1gNnSc4
a	a	k8xC
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
GAS	GAS	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7328	#num#	k4
<g/>
-	-	kIx~
<g/>
151	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
ROWLETT	ROWLETT	kA
<g/>
,	,	kIx,
Russ	Russ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Dictionary	Dictionar	k1gInPc1
of	of	k?
Units	Units	k1gInSc1
of	of	k?
Measurement	Measurement	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chapel	Chapel	k1gMnSc1
Hill	Hill	k1gMnSc1
-	-	kIx~
USA	USA	kA
<g/>
:	:	kIx,
University	universita	k1gFnSc2
of	of	k?
North	North	k1gInSc1
Carolina	Carolina	k1gFnSc1
at	at	k?
Chapel	Chapel	k1gMnSc1
Hill	Hill	k1gMnSc1
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
ČSN	ČSN	kA
ISO	ISO	kA
80000-1	80000-1	k4
Veličina	veličina	k1gFnSc1
a	a	k8xC
jednotky	jednotka	k1gFnPc1
-	-	kIx~
Část	část	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
Obecně	obecně	k6eAd1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřad	úřad	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Watthodina	watthodina	k1gFnSc1
</s>
<s>
Ohmův	Ohmův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
Volt	volt	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
watt	watt	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Soustava	soustava	k1gFnSc1
SI	si	k1gNnSc2
základní	základní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
ampér	ampér	k1gInSc1
</s>
<s>
kandela	kandela	k1gFnSc1
</s>
<s>
kelvin	kelvin	k1gInSc1
</s>
<s>
kilogram	kilogram	k1gInSc1
</s>
<s>
metr	metr	k1gInSc1
</s>
<s>
mol	mol	k1gMnSc1
</s>
<s>
sekunda	sekunda	k1gFnSc1
odvozené	odvozený	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
becquerel	becquerel	k1gInSc1
</s>
<s>
coulomb	coulomb	k1gInSc1
</s>
<s>
farad	farad	k1gInSc1
</s>
<s>
gray	graa	k1gFnPc1
</s>
<s>
henry	henry	k1gInSc1
</s>
<s>
hertz	hertz	k1gInSc1
</s>
<s>
joule	joule	k1gInSc1
</s>
<s>
katal	katal	k1gMnSc1
</s>
<s>
lumen	lumen	k1gMnSc1
</s>
<s>
lux	lux	k1gInSc1
</s>
<s>
newton	newton	k1gInSc1
</s>
<s>
ohm	ohm	k1gInSc1
</s>
<s>
pascal	pascal	k1gInSc1
</s>
<s>
radián	radián	k1gInSc1
</s>
<s>
siemens	siemens	k1gInSc1
</s>
<s>
sievert	sievert	k1gMnSc1
</s>
<s>
steradián	steradián	k1gInSc1
</s>
<s>
stupeň	stupeň	k1gInSc1
Celsia	Celsius	k1gMnSc2
</s>
<s>
tesla	tesla	k1gFnSc1
</s>
<s>
volt	volt	k1gInSc1
</s>
<s>
watt	watt	k1gInSc1
</s>
<s>
weber	weber	k1gInSc1
další	další	k2eAgFnSc2d1
</s>
<s>
předpony	předpona	k1gFnPc1
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
</s>
<s>
systémy	systém	k1gInPc1
měření	měření	k1gNnSc2
</s>
<s>
převody	převod	k1gInPc1
jednotek	jednotka	k1gFnPc2
</s>
<s>
nové	nový	k2eAgFnPc1d1
definice	definice	k1gFnPc1
SI	se	k3xPyFc3
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
</s>
