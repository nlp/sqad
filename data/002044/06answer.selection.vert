<s>
Hra	hra	k1gFnSc1	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
,	,	kIx,	,
rolová	rolový	k2eAgFnSc1d1	rolová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
role-playing	rolelaying	k1gInSc1	role-playing
game	game	k1gInSc1	game
zkracované	zkracovaný	k2eAgInPc1d1	zkracovaný
na	na	k7c6	na
RPG	RPG	kA	RPG
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
hráči	hráč	k1gMnPc1	hráč
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
role	role	k1gFnPc4	role
fiktivních	fiktivní	k2eAgFnPc2d1	fiktivní
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yIgMnPc4	který
podle	podle	k7c2	podle
daných	daný	k2eAgNnPc2d1	dané
pravidel	pravidlo	k1gNnPc2	pravidlo
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
hře	hra	k1gFnSc6	hra
jednají	jednat	k5eAaImIp3nP	jednat
<g/>
.	.	kIx.	.
</s>
