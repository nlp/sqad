<s>
Hra	hra	k1gFnSc1	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
,	,	kIx,	,
rolová	rolový	k2eAgFnSc1d1	rolová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
role-playing	rolelaying	k1gInSc1	role-playing
game	game	k1gInSc1	game
zkracované	zkracovaný	k2eAgInPc1d1	zkracovaný
na	na	k7c6	na
RPG	RPG	kA	RPG
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
hráči	hráč	k1gMnPc1	hráč
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
role	role	k1gFnPc4	role
fiktivních	fiktivní	k2eAgFnPc2d1	fiktivní
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yRgMnPc4	který
podle	podle	k7c2	podle
daných	daný	k2eAgNnPc2d1	dané
pravidel	pravidlo	k1gNnPc2	pravidlo
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
hře	hra	k1gFnSc6	hra
jednají	jednat	k5eAaImIp3nP	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
podobu	podoba	k1gFnSc4	podoba
stolní	stolní	k2eAgFnSc2d1	stolní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
nebo	nebo	k8xC	nebo
kostýmové	kostýmový	k2eAgFnSc2d1	kostýmová
hry	hra	k1gFnSc2	hra
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
larp	larp	k1gInSc4	larp
<g/>
.	.	kIx.	.
</s>
<s>
Uvedená	uvedený	k2eAgNnPc1d1	uvedené
označení	označení	k1gNnPc1	označení
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
základem	základ	k1gInSc7	základ
těchto	tento	k3xDgFnPc2	tento
her	hra	k1gFnPc2	hra
jsou	být	k5eAaImIp3nP	být
výrazné	výrazný	k2eAgFnPc4d1	výrazná
herní	herní	k2eAgFnPc4d1	herní
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazýváné	nazýváný	k2eAgInPc1d1	nazýváný
charaktery	charakter	k1gInPc1	charakter
(	(	kIx(	(
<g/>
vlivem	vlivem	k7c2	vlivem
anglického	anglický	k2eAgMnSc2d1	anglický
charakter	charakter	k1gInSc4	charakter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
hry	hra	k1gFnPc4	hra
formou	forma	k1gFnSc7	forma
interaktivní	interaktivní	k2eAgFnSc2d1	interaktivní
zábavy	zábava	k1gFnSc2	zábava
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
výpravnosti	výpravnost	k1gFnSc6	výpravnost
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
lze	lze	k6eAd1	lze
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
a	a	k8xC	a
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
lze	lze	k6eAd1	lze
mít	mít	k5eAaImF	mít
svou	svůj	k3xOyFgFnSc7	svůj
fiktivní	fiktivní	k2eAgFnSc7d1	fiktivní
rolí	role	k1gFnSc7	role
autentický	autentický	k2eAgInSc4d1	autentický
podíl	podíl	k1gInSc4	podíl
a	a	k8xC	a
také	také	k9	také
možnost	možnost	k1gFnSc4	možnost
odnést	odnést	k5eAaPmF	odnést
si	se	k3xPyFc3	se
reálný	reálný	k2eAgInSc1d1	reálný
zážitek	zážitek	k1gInSc1	zážitek
<g/>
,	,	kIx,	,
plynoucí	plynoucí	k2eAgInSc1d1	plynoucí
z	z	k7c2	z
procesu	proces	k1gInSc2	proces
transformace	transformace	k1gFnSc2	transformace
vlastní	vlastní	k2eAgFnSc2d1	vlastní
libovůle	libovůle	k1gFnSc2	libovůle
do	do	k7c2	do
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
postavy	postava	k1gFnSc2	postava
herního	herní	k2eAgInSc2d1	herní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
takový	takový	k3xDgInSc4	takový
svět	svět	k1gInSc4	svět
svou	svůj	k3xOyFgFnSc4	svůj
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
rolí	role	k1gFnSc7	role
tedy	tedy	k8xC	tedy
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sobě	se	k3xPyFc3	se
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
libovůle	libovůle	k1gFnPc4	libovůle
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
spoluvytváří	spoluvytvářet	k5eAaImIp3nS	spoluvytvářet
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc4	svět
sám	sám	k3xTgInSc4	sám
a	a	k8xC	a
zákonitosti	zákonitost	k1gFnPc1	zákonitost
takového	takový	k3xDgInSc2	takový
světa	svět	k1gInSc2	svět
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
definovány	definovat	k5eAaBmNgFnP	definovat
v	v	k7c6	v
pravidlech	pravidlo	k1gNnPc6	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
až	až	k9	až
velice	velice	k6eAd1	velice
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
a	a	k8xC	a
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
dotvářená	dotvářený	k2eAgFnSc1d1	dotvářená
a	a	k8xC	a
rozšiřovaná	rozšiřovaný	k2eAgFnSc1d1	rozšiřovaná
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
světem	svět	k1gInSc7	svět
RPG	RPG	kA	RPG
hry	hra	k1gFnSc2	hra
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
"	"	kIx"	"
<g/>
bdít	bdít	k5eAaImF	bdít
<g/>
"	"	kIx"	"
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
prvek	prvek	k1gInSc1	prvek
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
omezen	omezit	k5eAaPmNgMnS	omezit
ničím	ničí	k3xOyNgNnSc7	ničí
jiným	jiné	k1gNnSc7	jiné
než	než	k8xS	než
pravidly	pravidlo	k1gNnPc7	pravidlo
a	a	k8xC	a
který	který	k3yRgMnSc1	který
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
nad	nad	k7c7	nad
pohybem	pohyb	k1gInSc7	pohyb
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
činy	čin	k1gInPc1	čin
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákonitostí	zákonitost	k1gFnSc7	zákonitost
definovaného	definovaný	k2eAgInSc2d1	definovaný
univerzálního	univerzální	k2eAgInSc2d1	univerzální
zákona	zákon	k1gInSc2	zákon
nějakého	nějaký	k3yIgInSc2	nějaký
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
o	o	k7c6	o
následcích	následek	k1gInPc6	následek
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
činů	čin	k1gInPc2	čin
vykonaných	vykonaný	k2eAgInPc2d1	vykonaný
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgMnSc1	takový
mocný	mocný	k2eAgMnSc1d1	mocný
činitel	činitel	k1gMnSc1	činitel
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jakýsi	jakýsi	k3yIgMnSc1	jakýsi
pán	pán	k1gMnSc1	pán
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
game	game	k1gInSc1	game
master	master	k1gMnSc1	master
-	-	kIx~	-
pán	pán	k1gMnSc1	pán
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
dungeon	dungeon	k1gMnSc1	dungeon
master	master	k1gMnSc1	master
-	-	kIx~	-
pán	pán	k1gMnSc1	pán
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
právě	právě	k6eAd1	právě
pohyb	pohyb	k1gInSc4	pohyb
a	a	k8xC	a
dynamiku	dynamika	k1gFnSc4	dynamika
prostředí	prostředí	k1gNnSc2	prostředí
neustále	neustále	k6eAd1	neustále
dovytváří	dovytvářit	k5eAaPmIp3nS	dovytvářit
<g/>
,	,	kIx,	,
usměrňuje	usměrňovat	k5eAaImIp3nS	usměrňovat
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
přejímá	přejímat	k5eAaImIp3nS	přejímat
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
do	do	k7c2	do
světa	svět	k1gInSc2	svět
sám	sám	k3xTgMnSc1	sám
vkládá	vkládat	k5eAaImIp3nS	vkládat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
protivníky	protivník	k1gMnPc4	protivník
nebo	nebo	k8xC	nebo
přátele	přítel	k1gMnPc4	přítel
hrdinům	hrdina	k1gMnPc3	hrdina
ovládanými	ovládaný	k2eAgMnPc7d1	ovládaný
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Pen	Pen	k1gFnSc1	Pen
&	&	k?	&
Paper	Paper	k1gInSc1	Paper
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tužka	tužka	k1gFnSc1	tužka
a	a	k8xC	a
papír	papír	k1gInSc1	papír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
principu	princip	k1gInSc6	princip
krom	krom	k7c2	krom
mnohostěnných	mnohostěnný	k2eAgFnPc2d1	mnohostěnný
kostek	kostka	k1gFnPc2	kostka
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
jiného	jiný	k2eAgInSc2d1	jiný
generátoru	generátor	k1gInSc2	generátor
náhodných	náhodný	k2eAgNnPc2d1	náhodné
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
stačí	stačit	k5eAaBmIp3nS	stačit
poznámkový	poznámkový	k2eAgInSc1d1	poznámkový
blok	blok	k1gInSc1	blok
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
se	se	k3xPyFc4	se
chopí	chopit	k5eAaPmIp3nS	chopit
role	role	k1gFnSc1	role
pána	pán	k1gMnSc2	pán
jeskyně	jeskyně	k1gFnSc2	jeskyně
(	(	kIx(	(
<g/>
PJ	PJ	kA	PJ
<g/>
,	,	kIx,	,
DM	dm	kA	dm
<g/>
,	,	kIx,	,
<g/>
GM	GM	kA	GM
<g/>
)	)	kIx)	)
a	a	k8xC	a
obstará	obstarat	k5eAaPmIp3nS	obstarat
vyprávění	vyprávění	k1gNnSc1	vyprávění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahrne	zahrnout	k5eAaPmIp3nS	zahrnout
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
herním	herní	k2eAgInSc6d1	herní
světě	svět	k1gInSc6	svět
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
činnost	činnost	k1gFnSc4	činnost
ostatních	ostatní	k2eAgMnPc2d1	ostatní
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
hráči	hráč	k1gMnPc1	hráč
již	již	k6eAd1	již
hrají	hrát	k5eAaImIp3nP	hrát
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
zvolí	zvolit	k5eAaPmIp3nS	zvolit
určité	určitý	k2eAgFnPc4d1	určitá
charakterové	charakterový	k2eAgFnPc4d1	charakterová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
povolání	povolání	k1gNnSc4	povolání
<g/>
,	,	kIx,	,
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Průběh	průběh	k1gInSc4	průběh
hry	hra	k1gFnSc2	hra
probíhá	probíhat	k5eAaImIp3nS	probíhat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
hráči	hráč	k1gMnPc1	hráč
(	(	kIx(	(
<g/>
družina	družin	k2eAgFnSc1d1	družina
<g/>
)	)	kIx)	)
popisují	popisovat	k5eAaImIp3nP	popisovat
chování	chování	k1gNnSc1	chování
a	a	k8xC	a
akce	akce	k1gFnSc1	akce
svých	svůj	k3xOyFgFnPc2	svůj
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
PJ	PJ	kA	PJ
jim	on	k3xPp3gInPc3	on
vrací	vracet	k5eAaImIp3nS	vracet
odezvy	odezva	k1gFnSc2	odezva
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Herní	herní	k2eAgInSc1d1	herní
zážitek	zážitek	k1gInSc1	zážitek
závisí	záviset	k5eAaImIp3nS	záviset
nejvíce	hodně	k6eAd3	hodně
na	na	k7c6	na
schopnosti	schopnost	k1gFnSc6	schopnost
pána	pán	k1gMnSc2	pán
jeskyně	jeskyně	k1gFnSc2	jeskyně
vykreslit	vykreslit	k5eAaPmF	vykreslit
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
herní	herní	k2eAgInSc1d1	herní
svět	svět	k1gInSc1	svět
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
hráče	hráč	k1gMnPc4	hráč
prožít	prožít	k5eAaPmF	prožít
zajímavá	zajímavý	k2eAgNnPc4d1	zajímavé
dobrodružství	dobrodružství	k1gNnPc4	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
hráčů	hráč	k1gMnPc2	hráč
je	být	k5eAaImIp3nS	být
reprezentovaná	reprezentovaný	k2eAgNnPc4d1	reprezentované
herním	herní	k2eAgInSc7d1	herní
deníkem	deník	k1gInSc7	deník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
všechny	všechen	k3xTgFnPc1	všechen
charakteristiky	charakteristika	k1gFnPc1	charakteristika
(	(	kIx(	(
<g/>
kupříkladu	kupříkladu	k6eAd1	kupříkladu
základní	základní	k2eAgInPc4d1	základní
ukazatele	ukazatel	k1gInPc4	ukazatel
<g/>
:	:	kIx,	:
počet	počet	k1gInSc4	počet
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
stamina	stamin	k2eAgFnSc1d1	stamina
<g/>
,	,	kIx,	,
mana	mana	k1gFnSc1	mana
<g/>
,	,	kIx,	,
atributy	atribut	k1gInPc1	atribut
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
obratnost	obratnost	k1gFnSc1	obratnost
<g/>
,	,	kIx,	,
odolnost	odolnost	k1gFnSc1	odolnost
<g/>
,	,	kIx,	,
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
,	,	kIx,	,
moudrost	moudrost	k1gFnSc1	moudrost
<g/>
,	,	kIx,	,
charisma	charisma	k1gNnSc1	charisma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobrodružství	dobrodružství	k1gNnSc6	dobrodružství
hráči	hráč	k1gMnPc1	hráč
čelí	čelit	k5eAaImIp3nP	čelit
různým	různý	k2eAgFnPc3d1	různá
překážkám	překážka	k1gFnPc3	překážka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
pán	pán	k1gMnSc1	pán
jeskyně	jeskyně	k1gFnSc2	jeskyně
připraví	připravit	k5eAaPmIp3nS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
souboje	souboj	k1gInPc4	souboj
<g/>
,	,	kIx,	,
řešení	řešení	k1gNnSc4	řešení
hádanek	hádanka	k1gFnPc2	hádanka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
třeba	třeba	k6eAd1	třeba
různá	různý	k2eAgNnPc4d1	různé
pátrání	pátrání	k1gNnPc4	pátrání
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
zdolanou	zdolaný	k2eAgFnSc4d1	zdolaná
překážku	překážka	k1gFnSc4	překážka
obdrží	obdržet	k5eAaPmIp3nS	obdržet
příslušný	příslušný	k2eAgMnSc1d1	příslušný
hráč	hráč	k1gMnSc1	hráč
zkušenosti	zkušenost	k1gFnSc2	zkušenost
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
talentové	talentový	k2eAgInPc4d1	talentový
body	bod	k1gInPc4	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgFnPc3	který
může	moct	k5eAaImIp3nS	moct
vylepšovat	vylepšovat	k5eAaImF	vylepšovat
svojí	svůj	k3xOyFgFnSc7	svůj
postavu	postava	k1gFnSc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgMnSc7d1	typický
je	být	k5eAaImIp3nS	být
tahový	tahový	k2eAgInSc1d1	tahový
soubojový	soubojový	k2eAgInSc1d1	soubojový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
přehlednost	přehlednost	k1gFnSc4	přehlednost
hrát	hrát	k5eAaImF	hrát
na	na	k7c6	na
hexovém	hexový	k2eAgMnSc6d1	hexový
<g/>
/	/	kIx~	/
<g/>
čtverečkovém	čtverečkový	k2eAgInSc6d1	čtverečkový
hracím	hrací	k2eAgInSc6d1	hrací
plánu	plán	k1gInSc6	plán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
před	před	k7c7	před
vlastní	vlastní	k2eAgFnSc7d1	vlastní
hrou	hra	k1gFnSc7	hra
připravuje	připravovat	k5eAaImIp3nS	připravovat
pán	pán	k1gMnSc1	pán
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejznámější	známý	k2eAgFnSc4d3	nejznámější
českou	český	k2eAgFnSc4d1	Česká
fantasy	fantas	k1gInPc1	fantas
RPG	RPG	kA	RPG
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
lze	lze	k6eAd1	lze
právoplatně	právoplatně	k6eAd1	právoplatně
považovat	považovat	k5eAaImF	považovat
Dračí	dračí	k2eAgNnSc4d1	dračí
doupě	doupě	k1gNnSc4	doupě
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soupis	soupis	k1gInSc4	soupis
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
fantasy	fantas	k1gInPc4	fantas
svět	svět	k1gInSc4	svět
tradičnějšího	tradiční	k2eAgNnSc2d2	tradičnější
ražení	ražení	k1gNnSc2	ražení
<g/>
,	,	kIx,	,
ne	ne	k9	ne
nepodobný	podobný	k2eNgInSc1d1	nepodobný
D	D	kA	D
<g/>
&	&	k?	&
<g/>
D	D	kA	D
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgMnPc1d3	nejrozšířenější
světově	světově	k6eAd1	světově
hranou	hrana	k1gFnSc7	hrana
RPG	RPG	kA	RPG
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
Dungeons	Dungeons	k1gInSc1	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
<g/>
,	,	kIx,	,
v	v	k7c6	v
aktuální	aktuální	k2eAgFnSc6d1	aktuální
edici	edice	k1gFnSc6	edice
<g/>
:	:	kIx,	:
Dungeons	Dungeons	k1gInSc1	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
4	[number]	k4	4
<g/>
th	th	k?	th
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
původní	původní	k2eAgFnSc1d1	původní
trojici	trojice	k1gFnSc4	trojice
povolání	povolání	k1gNnSc2	povolání
(	(	kIx(	(
<g/>
válečník	válečník	k1gMnSc1	válečník
<g/>
/	/	kIx~	/
<g/>
lapka	lapka	k1gMnSc1	lapka
<g/>
/	/	kIx~	/
<g/>
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
/	/	kIx~	/
<g/>
klerik	klerik	k1gMnSc1	klerik
<g/>
)	)	kIx)	)
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
o	o	k7c4	o
spoustu	spousta	k1gFnSc4	spousta
dalších	další	k2eAgInPc2d1	další
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
přepracování	přepracování	k1gNnSc4	přepracování
a	a	k8xC	a
doplnění	doplnění	k1gNnSc4	doplnění
herních	herní	k2eAgInPc2d1	herní
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
další	další	k2eAgInPc4d1	další
systémy	systém	k1gInPc4	systém
je	být	k5eAaImIp3nS	být
určitě	určitě	k6eAd1	určitě
vhodné	vhodný	k2eAgNnSc1d1	vhodné
zmínit	zmínit	k5eAaPmF	zmínit
GURPS	GURPS	kA	GURPS
(	(	kIx(	(
<g/>
Generic	Generic	k1gMnSc1	Generic
Universal	Universal	k1gFnSc2	Universal
Role	role	k1gFnSc1	role
Playing	Playing	k1gInSc1	Playing
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
stanovení	stanovení	k1gNnSc4	stanovení
herního	herní	k2eAgInSc2d1	herní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
konkrétní	konkrétní	k2eAgNnPc4d1	konkrétní
specifika	specifikon	k1gNnPc4	specifikon
herního	herní	k2eAgInSc2d1	herní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
s	s	k7c7	s
jakoukoliv	jakýkoliv	k3yIgFnSc7	jakýkoliv
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pouze	pouze	k6eAd1	pouze
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgMnSc1d1	typický
je	být	k5eAaImIp3nS	být
precizní	precizní	k2eAgInSc1d1	precizní
soubojový	soubojový	k2eAgInSc1d1	soubojový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
velké	velký	k2eAgFnPc4d1	velká
možnosti	možnost	k1gFnPc4	možnost
taktizování	taktizování	k1gNnSc2	taktizování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
Lite	lit	k1gInSc5	lit
je	on	k3xPp3gMnPc4	on
navíc	navíc	k6eAd1	navíc
dostupný	dostupný	k2eAgInSc1d1	dostupný
zcela	zcela	k6eAd1	zcela
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
velmi	velmi	k6eAd1	velmi
zajímavou	zajímavý	k2eAgFnSc4d1	zajímavá
alternativu	alternativa	k1gFnSc4	alternativa
nabízí	nabízet	k5eAaImIp3nS	nabízet
hororově	hororově	k6eAd1	hororově
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
hry	hra	k1gFnPc4	hra
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
firmy	firma	k1gFnSc2	firma
Whitewolf	Whitewolf	k1gInSc1	Whitewolf
představující	představující	k2eAgInSc1d1	představující
Svět	svět	k1gInSc1	svět
temnoty	temnota	k1gFnSc2	temnota
jako	jako	k8xC	jako
hrozivější	hrozivý	k2eAgFnSc4d2	hrozivější
verzi	verze	k1gFnSc4	verze
současného	současný	k2eAgInSc2d1	současný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
upírů	upír	k1gMnPc2	upír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
doplněn	doplnit	k5eAaPmNgInS	doplnit
o	o	k7c4	o
vlkodlaky	vlkodlak	k1gMnPc4	vlkodlak
<g/>
,	,	kIx,	,
temné	temný	k2eAgMnPc4d1	temný
mágy	mág	k1gMnPc4	mág
a	a	k8xC	a
také	také	k9	také
konstrukty	konstrukt	k1gInPc1	konstrukt
ne-nepodobné	epodobný	k2eNgInPc1d1	-nepodobný
Frankensteinově	Frankensteinův	k2eAgNnSc6d1	Frankensteinovo
monstru	monstrum	k1gNnSc6	monstrum
<g/>
.	.	kIx.	.
</s>
<s>
Herní	herní	k2eAgInPc1d1	herní
mechanismy	mechanismus	k1gInPc1	mechanismus
jsou	být	k5eAaImIp3nP	být
svižnější	svižný	k2eAgInPc1d2	svižnější
než	než	k8xS	než
u	u	k7c2	u
D	D	kA	D
<g/>
&	&	k?	&
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
herní	herní	k2eAgInSc1d1	herní
svět	svět	k1gInSc1	svět
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
kánon	kánon	k1gInSc1	kánon
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
herním	herní	k2eAgInSc7d1	herní
systémem	systém	k1gInSc7	systém
je	být	k5eAaImIp3nS	být
Shadowrun	Shadowrun	k1gInSc1	Shadowrun
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
posunuje	posunovat	k5eAaImIp3nS	posunovat
kulisy	kulisa	k1gFnPc4	kulisa
do	do	k7c2	do
země	zem	k1gFnSc2	zem
blízké	blízký	k2eAgFnSc2d1	blízká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
probuzení	probuzení	k1gNnSc1	probuzení
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
mix	mix	k1gInSc1	mix
sci-fi	scii	k1gNnPc2	sci-fi
a	a	k8xC	a
fantasy	fantas	k1gInPc1	fantas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
mezi	mezi	k7c7	mezi
zkušenějšími	zkušený	k2eAgMnPc7d2	zkušenější
hráči	hráč	k1gMnPc7	hráč
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
"	"	kIx"	"
<g/>
málopravidlové	málopravidlový	k2eAgFnPc4d1	málopravidlový
<g/>
"	"	kIx"	"
hry	hra	k1gFnPc4	hra
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
na	na	k7c4	na
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
roleplaying	roleplaying	k1gInSc4	roleplaying
a	a	k8xC	a
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Fate	Fate	k1gInSc4	Fate
nebo	nebo	k8xC	nebo
The	The	k1gFnSc4	The
Window	Window	k1gFnPc2	Window
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
již	již	k6eAd1	již
ovšem	ovšem	k9	ovšem
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
blízké	blízký	k2eAgFnPc1d1	blízká
tradiční	tradiční	k2eAgFnPc1d1	tradiční
hratelnosti	hratelnost	k1gFnPc1	hratelnost
tahového	tahový	k2eAgInSc2d1	tahový
soubojového	soubojový	k2eAgInSc2d1	soubojový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
RPG	RPG	kA	RPG
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
podobné	podobný	k2eAgFnSc2d1	podobná
tradičním	tradiční	k2eAgFnPc3d1	tradiční
hrám	hra	k1gFnPc3	hra
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
herní	herní	k2eAgInPc1d1	herní
mechanismy	mechanismus	k1gInPc1	mechanismus
navíc	navíc	k6eAd1	navíc
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
Roleplayingový	Roleplayingový	k2eAgInSc4d1	Roleplayingový
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
převzatý	převzatý	k2eAgInSc1d1	převzatý
ze	z	k7c2	z
stolních	stolní	k2eAgNnPc2d1	stolní
RPG	RPG	kA	RPG
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
hru	hra	k1gFnSc4	hra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
přímo	přímo	k6eAd1	přímo
určuje	určovat	k5eAaImIp3nS	určovat
hratelnost	hratelnost	k1gFnSc1	hratelnost
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgMnSc1d1	typický
je	být	k5eAaImIp3nS	být
komplexní	komplexní	k2eAgInSc1d1	komplexní
a	a	k8xC	a
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
příběh	příběh	k1gInSc1	příběh
a	a	k8xC	a
také	také	k9	také
pestré	pestrý	k2eAgNnSc1d1	pestré
vyprávění	vyprávění	k1gNnSc1	vyprávění
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
ovládají	ovládat	k5eAaImIp3nP	ovládat
obvykle	obvykle	k6eAd1	obvykle
hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
družinu	družina	k1gFnSc4	družina
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivým	jednotlivý	k2eAgInSc7d1	jednotlivý
vlastním	vlastní	k2eAgInSc7d1	vlastní
postavám	postava	k1gFnPc3	postava
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
možné	možný	k2eAgNnSc1d1	možné
přidělit	přidělit	k5eAaPmF	přidělit
různé	různý	k2eAgInPc4d1	různý
vzory	vzor	k1gInPc4	vzor
chování	chování	k1gNnSc2	chování
dle	dle	k7c2	dle
zaměření	zaměření	k1gNnSc2	zaměření
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
naplánovat	naplánovat	k5eAaBmF	naplánovat
sled	sled	k1gInSc1	sled
prováděných	prováděný	k2eAgFnPc2d1	prováděná
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
nehráčské	hráčský	k2eNgFnPc1d1	nehráčská
postavy	postava	k1gFnPc1	postava
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k9	jako
NPC	NPC	kA	NPC
(	(	kIx(	(
<g/>
NonPlayer	NonPlayer	k1gMnSc1	NonPlayer
Character	Character	k1gMnSc1	Character
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
reakce	reakce	k1gFnPc1	reakce
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
plně	plně	k6eAd1	plně
režii	režie	k1gFnSc4	režie
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
kombinací	kombinace	k1gFnPc2	kombinace
předscriptovaných	předscriptovaný	k2eAgFnPc2d1	předscriptovaný
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
větvených	větvený	k2eAgInPc2d1	větvený
dialogů	dialog	k1gInPc2	dialog
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
konfliktu	konflikt	k1gInSc2	konflikt
určité	určitý	k2eAgFnSc2d1	určitá
soubojové	soubojový	k2eAgFnSc2d1	soubojová
UI	UI	kA	UI
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
hráč	hráč	k1gMnSc1	hráč
prožívá	prožívat	k5eAaImIp3nS	prožívat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
postav	postava	k1gFnPc2	postava
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
a	a	k8xC	a
za	za	k7c4	za
vyřešení	vyřešení	k1gNnSc4	vyřešení
určitých	určitý	k2eAgFnPc2d1	určitá
situací	situace	k1gFnPc2	situace
dostává	dostávat	k5eAaImIp3nS	dostávat
zkušenostní	zkušenostní	k2eAgInPc4d1	zkušenostní
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
vylepšení	vylepšení	k1gNnSc3	vylepšení
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
RPG	RPG	kA	RPG
her	hra	k1gFnPc2	hra
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dokáží	dokázat	k5eAaPmIp3nP	dokázat
tvůrci	tvůrce	k1gMnPc1	tvůrce
předpřipravit	předpřipravit	k5eAaPmF	předpřipravit
herní	herní	k2eAgInSc4d1	herní
zážitek	zážitek	k1gInSc4	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
požadavky	požadavek	k1gInPc1	požadavek
jsou	být	k5eAaImIp3nP	být
kladeny	klást	k5eAaImNgInP	klást
jak	jak	k6eAd1	jak
na	na	k7c4	na
samotné	samotný	k2eAgInPc4d1	samotný
kodéry	kodér	k1gInPc4	kodér
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
výtvarníky	výtvarník	k1gMnPc4	výtvarník
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
scenáristy	scenárista	k1gMnSc2	scenárista
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
principu	princip	k1gInSc2	princip
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
že	že	k8xS	že
rpg	rpg	k?	rpg
hra	hra	k1gFnSc1	hra
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
značně	značně	k6eAd1	značně
nelineární	lineární	k2eNgNnSc4d1	nelineární
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
zohlednit	zohlednit	k5eAaPmF	zohlednit
<g/>
.	.	kIx.	.
</s>
<s>
Omezené	omezený	k2eAgFnPc1d1	omezená
volnosti	volnost	k1gFnPc1	volnost
konání	konání	k1gNnSc2	konání
a	a	k8xC	a
rozhodování	rozhodování	k1gNnSc2	rozhodování
oproti	oproti	k7c3	oproti
klasickým	klasický	k2eAgFnPc3d1	klasická
stolním	stolní	k2eAgFnPc3d1	stolní
dobrodružství	dobrodružství	k1gNnSc3	dobrodružství
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
scenáristické	scenáristický	k2eAgNnSc1d1	scenáristické
zpracování	zpracování	k1gNnSc1	zpracování
dokáže	dokázat	k5eAaPmIp3nS	dokázat
z	z	k7c2	z
předem	předem	k6eAd1	předem
daného	daný	k2eAgInSc2d1	daný
příběhu	příběh	k1gInSc2	příběh
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
charakteru	charakter	k1gInSc2	charakter
postav	postava	k1gFnPc2	postava
vytvořit	vytvořit	k5eAaPmF	vytvořit
zážitek	zážitek	k1gInSc4	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
na	na	k7c4	na
výpravnost	výpravnost	k1gFnSc4	výpravnost
příběhu	příběh	k1gInSc2	příběh
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
propracovaný	propracovaný	k2eAgInSc4d1	propracovaný
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
statistik	statistika	k1gFnPc2	statistika
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
počítačového	počítačový	k2eAgNnSc2d1	počítačové
RPG	RPG	kA	RPG
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
redukován	redukovat	k5eAaBmNgInS	redukovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
vývoje	vývoj	k1gInSc2	vývoj
statistik	statistika	k1gFnPc2	statistika
postavy	postava	k1gFnSc2	postava
a	a	k8xC	a
původní	původní	k2eAgInSc4d1	původní
význam	význam	k1gInSc4	význam
hraní	hraní	k1gNnSc2	hraní
role	role	k1gFnSc2	role
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tyt	k2eAgNnSc1d1	tyto
tzv.	tzv.	kA	tzv.
<g/>
:	:	kIx,	:
Akční-RPG	Akční-RPG	k1gMnSc1	Akční-RPG
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
implementují	implementovat	k5eAaImIp3nP	implementovat
pouze	pouze	k6eAd1	pouze
jádra	jádro	k1gNnSc2	jádro
RPG	RPG	kA	RPG
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
dále	daleko	k6eAd2	daleko
nepracují	pracovat	k5eNaImIp3nP	pracovat
s	s	k7c7	s
dynamikou	dynamika	k1gFnSc7	dynamika
vyprávění	vyprávění	k1gNnSc2	vyprávění
vůči	vůči	k7c3	vůči
hernímu	herní	k2eAgInSc3d1	herní
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
podžánrů	podžánr	k1gInPc2	podžánr
počítačových	počítačový	k2eAgInPc2d1	počítačový
RPG	RPG	kA	RPG
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dungeon	dungeon	k1gNnSc1	dungeon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
specifický	specifický	k2eAgInSc1d1	specifický
pohledem	pohled	k1gInSc7	pohled
z	z	k7c2	z
první	první	k4xOgFnSc2	první
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
postup	postup	k1gInSc4	postup
prostředím	prostředí	k1gNnSc7	prostředí
po	po	k7c6	po
čtvercové	čtvercový	k2eAgFnSc6d1	čtvercová
mapě	mapa	k1gFnSc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
rozmach	rozmach	k1gInSc4	rozmach
zažil	zažít	k5eAaPmAgMnS	zažít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Počítačové	počítačový	k2eAgFnPc1d1	počítačová
hry	hra	k1gFnPc1	hra
nabízejí	nabízet	k5eAaImIp3nP	nabízet
možnost	možnost	k1gFnSc4	možnost
hry	hra	k1gFnSc2	hra
jednoho	jeden	k4xCgMnSc4	jeden
hráče	hráč	k1gMnSc4	hráč
(	(	kIx(	(
<g/>
Single-player	Singlelayer	k1gMnSc1	Single-player
<g/>
)	)	kIx)	)
i	i	k8xC	i
více	hodně	k6eAd2	hodně
hráčů	hráč	k1gMnPc2	hráč
zároveň	zároveň	k6eAd1	zároveň
(	(	kIx(	(
<g/>
Multiplayer	Multiplayer	k1gInSc1	Multiplayer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
další	další	k2eAgFnPc1d1	další
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přímo	přímo	k6eAd1	přímo
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k9	jako
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ovládat	ovládat	k5eAaImF	ovládat
i	i	k9	i
počítač	počítač	k1gInSc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Význačným	význačný	k2eAgNnSc7d1	význačné
specifikem	specifikon	k1gNnSc7	specifikon
počítačových	počítačový	k2eAgFnPc2d1	počítačová
RPG	RPG	kA	RPG
her	hra	k1gFnPc2	hra
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možnost	možnost	k1gFnSc4	možnost
hraní	hraní	k1gNnSc2	hraní
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
tzv.	tzv.	kA	tzv.
MUD	MUD	kA	MUD
(	(	kIx(	(
<g/>
Multi-User	Multi-User	k1gMnSc1	Multi-User
Dungeon	Dungeon	k1gMnSc1	Dungeon
<g/>
)	)	kIx)	)
a	a	k8xC	a
MMORPG	MMORPG	kA	MMORPG
(	(	kIx(	(
<g/>
Massively-Multiplayer	Massively-Multiplayer	k1gMnSc1	Massively-Multiplayer
Online	Onlin	k1gInSc5	Onlin
Role-Playing	Role-Playing	k1gInSc4	Role-Playing
Game	game	k1gInSc1	game
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Larp	Larp	k1gInSc1	Larp
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
larp	larp	k1gInSc1	larp
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
živí	živý	k2eAgMnPc1d1	živý
lidé	člověk	k1gMnPc1	člověk
fyzicky	fyzicky	k6eAd1	fyzicky
svými	svůj	k3xOyFgInPc7	svůj
činy	čin	k1gInPc7	čin
představují	představovat	k5eAaImIp3nP	představovat
činy	čina	k1gFnSc2	čina
svých	svůj	k3xOyFgFnPc2	svůj
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
interagují	interagovat	k5eAaImIp3nP	interagovat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
navzájem	navzájem	k6eAd1	navzájem
a	a	k8xC	a
s	s	k7c7	s
okolním	okolní	k2eAgNnSc7d1	okolní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c6	o
splnění	splnění	k1gNnSc6	splnění
cílů	cíl	k1gInPc2	cíl
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
světě	svět	k1gInSc6	svět
reprezentovaném	reprezentovaný	k2eAgInSc6d1	reprezentovaný
světem	svět	k1gInSc7	svět
skutečným	skutečný	k2eAgInSc7d1	skutečný
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
larp	larp	k1gInSc4	larp
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgNnSc4	žádný
publikum	publikum	k1gNnSc4	publikum
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
smyslem	smysl	k1gInSc7	smysl
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
zábava	zábava	k1gFnSc1	zábava
a	a	k8xC	a
zážitek	zážitek	k1gInSc1	zážitek
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
zpravidla	zpravidla	k6eAd1	zpravidla
pramení	pramenit	k5eAaImIp3nP	pramenit
z	z	k7c2	z
prožití	prožití	k1gNnSc2	prožití
příběhu	příběh	k1gInSc2	příběh
skrz	skrz	k7c4	skrz
svoji	svůj	k3xOyFgFnSc4	svůj
postavu	postava	k1gFnSc4	postava
či	či	k8xC	či
kompetetivního	kompetetivní	k2eAgInSc2d1	kompetetivní
fyzického	fyzický	k2eAgInSc2d1	fyzický
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
konflikty	konflikt	k1gInPc1	konflikt
ve	v	k7c6	v
sdíleném	sdílený	k2eAgInSc6d1	sdílený
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
světě	svět	k1gInSc6	svět
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
řešeny	řešit	k5eAaImNgFnP	řešit
jako	jako	k9	jako
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
(	(	kIx(	(
<g/>
reálně	reálně	k6eAd1	reálně
<g/>
,	,	kIx,	,
diegeticky	diegeticky	k6eAd1	diegeticky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
různými	různý	k2eAgInPc7d1	různý
zástupnými	zástupný	k2eAgInPc7d1	zástupný
mechanismy	mechanismus	k1gInPc7	mechanismus
(	(	kIx(	(
<g/>
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reportáže	reportáž	k1gFnPc1	reportáž
z	z	k7c2	z
larpů	larp	k1gInPc2	larp
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
pomohou	pomoct	k5eAaPmIp3nP	pomoct
přiblížit	přiblížit	k5eAaPmF	přiblížit
atmosféru	atmosféra	k1gFnSc4	atmosféra
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
panující	panující	k2eAgInPc1d1	panující
<g/>
,	,	kIx,	,
můžete	moct	k5eAaImIp2nP	moct
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
<g/>
:	:	kIx,	:
FANTAZEEN	FANTAZEEN	kA	FANTAZEEN
K	k	k7c3	k
praktické	praktický	k2eAgFnSc3d1	praktická
hře	hra	k1gFnSc3	hra
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
hrající	hrající	k2eAgFnSc2d1	hrající
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
se	se	k3xPyFc4	se
zajímají	zajímat	k5eAaImIp3nP	zajímat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
svou	svůj	k3xOyFgFnSc4	svůj
hru	hra	k1gFnSc4	hra
okořenit	okořenit	k5eAaPmF	okořenit
<g/>
,	,	kIx,	,
vylepšit	vylepšit	k5eAaPmF	vylepšit
a	a	k8xC	a
předávají	předávat	k5eAaImIp3nP	předávat
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
tyto	tento	k3xDgFnPc4	tento
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
tak	tak	k6eAd1	tak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
různé	různý	k2eAgFnPc1d1	různá
teoretické	teoretický	k2eAgFnPc1d1	teoretická
práce	práce	k1gFnPc1	práce
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
a	a	k8xC	a
způsobů	způsob	k1gInPc2	způsob
hraní	hraní	k1gNnSc2	hraní
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
tak	tak	k9	tak
pojmy	pojem	k1gInPc1	pojem
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
powergaming	powergaming	k1gInSc1	powergaming
<g/>
,	,	kIx,	,
munchkin	munchkin	k1gMnSc1	munchkin
<g/>
,	,	kIx,	,
deep	deep	k1gMnSc1	deep
immersion	immersion	k1gInSc1	immersion
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
lze	lze	k6eAd1	lze
několik	několik	k4yIc4	několik
takových	takový	k3xDgNnPc2	takový
dílek	dílko	k1gNnPc2	dílko
nalézt	nalézt	k5eAaBmF	nalézt
zde	zde	k6eAd1	zde
Asterion	Asterion	k1gInSc1	Asterion
Birthright	Birthrighta	k1gFnPc2	Birthrighta
Dragonlance	Dragonlance	k1gFnSc2	Dragonlance
Dark	Dark	k1gInSc1	Dark
Sun	Sun	kA	Sun
Eberron	Eberron	k1gInSc4	Eberron
Forgotten	Forgotten	k2eAgInSc1d1	Forgotten
Realms	Realms	k1gInSc1	Realms
Greyhawk	Greyhawka	k1gFnPc2	Greyhawka
Planescape	Planescap	k1gInSc5	Planescap
Ravenloft	Ravenloft	k1gMnSc1	Ravenloft
Siranie	Siranie	k1gFnSc2	Siranie
Vukogvazd	Vukogvazd	k1gMnSc1	Vukogvazd
</s>
