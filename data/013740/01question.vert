<s>
Jaká	jaký	k3yQgFnSc1
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
názvu	název	k1gInSc2
astronomického	astronomický	k2eAgInSc2d1
magazínu	magazín	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
pracovníky	pracovník	k1gMnPc7
Brněnské	brněnský	k2eAgFnSc2d1
hvězdárny	hvězdárna	k1gFnSc2
a	a	k8xC
planetária	planetárium	k1gNnSc2
<g/>
?	?	kIx.
</s>