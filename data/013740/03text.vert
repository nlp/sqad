<s>
Instantní	instantní	k2eAgFnPc1d1
astronomické	astronomický	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
</s>
<s>
Instantní	instantní	k2eAgFnPc1d1
astronomické	astronomický	k2eAgFnPc1d1
novinyZákladní	novinyZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Jazyk	jazyk	k1gInSc1
</s>
<s>
čeština	čeština	k1gFnSc1
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Odkazy	odkaz	k1gInPc1
ISSN	ISSN	kA
</s>
<s>
1212-6691	1212-6691	k4
Web	web	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Instantní	instantní	k2eAgFnPc1d1
astronomické	astronomický	k2eAgFnPc1d1
noviny	noviny	k1gFnPc1
(	(	kIx(
<g/>
IAN	IAN	kA
<g/>
)	)	kIx)
byly	být	k5eAaImAgInP
astronomický	astronomický	k2eAgInSc4d1
magazín	magazín	k1gInSc4
o	o	k7c6
dění	dění	k1gNnSc6
na	na	k7c6
obloze	obloha	k1gFnSc6
i	i	k9
pod	pod	k7c7
ní	on	k3xPp3gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založen	založen	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
pracovníky	pracovník	k1gMnPc7
Brněnské	brněnský	k2eAgFnSc2d1
hvězdárny	hvězdárna	k1gFnSc2
a	a	k8xC
planetária	planetárium	k1gNnSc2
Rudolfem	Rudolf	k1gMnSc7
Novákem	Novák	k1gMnSc7
a	a	k8xC
Jiřím	Jiří	k1gMnSc7
Duškem	Dušek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravidelnější	pravidelní	k2eAgInSc1d2
vydání	vydání	k1gNnSc6
přestala	přestat	k5eAaPmAgFnS
vycházet	vycházet	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
archiv	archiv	k1gInSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
dostupný	dostupný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
magazínu	magazín	k1gInSc6
byla	být	k5eAaImAgFnS
také	také	k9
pojmenována	pojmenován	k2eAgFnSc1d1
planetka	planetka	k1gFnSc1
(	(	kIx(
<g/>
9665	#num#	k4
<g/>
)	)	kIx)
Inastronoviny	Inastronovina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.hvezdarnacb.cz/jmena/viewcz.php3?astnum=9665	http://www.hvezdarnacb.cz/jmena/viewcz.php3?astnum=9665	k4
<g/>
↑	↑	k?
</s>
<s>
TICHÁ	Tichá	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instantní	instantní	k2eAgFnPc1d1
astronomické	astronomický	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdárna	hvězdárna	k1gFnSc1
a	a	k8xC
planetárium	planetárium	k1gNnSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
28.11	28.11	k4
<g/>
.1999	.1999	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Stránky	stránka	k1gFnPc1
webu	web	k1gInSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
