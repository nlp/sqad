<s>
Zmijovití	zmijovitý	k2eAgMnPc1d1	zmijovitý
(	(	kIx(	(
<g/>
Viperidae	Viperidae	k1gNnSc7	Viperidae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýše	vysoce	k6eAd3	vysoce
postavených	postavený	k2eAgFnPc2d1	postavená
čeledí	čeleď	k1gFnPc2	čeleď
hadů	had	k1gMnPc2	had
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
jedovými	jedový	k2eAgInPc7d1	jedový
zuby	zub	k1gInPc7	zub
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
mají	mít	k5eAaImIp3nP	mít
tepločivné	tepločivný	k2eAgFnPc4d1	tepločivný
jamky	jamka	k1gFnPc4	jamka
mezi	mezi	k7c7	mezi
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
nozdrami	nozdra	k1gFnPc7	nozdra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
hady	had	k1gMnPc7	had
se	se	k3xPyFc4	se
dokážou	dokázat	k5eAaPmIp3nP	dokázat
lépe	dobře	k6eAd2	dobře
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
s	s	k7c7	s
podmínkami	podmínka	k1gFnPc7	podmínka
chladného	chladný	k2eAgNnSc2d1	chladné
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
různého	různý	k2eAgNnSc2d1	různé
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
různé	různý	k2eAgFnSc2d1	různá
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
kontinentech	kontinent	k1gInPc6	kontinent
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
a	a	k8xC	a
Havaje	Havaj	k1gFnSc2	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
čeledi	čeleď	k1gFnSc2	čeleď
hadů	had	k1gMnPc2	had
patří	patřit	k5eAaImIp3nS	patřit
zmije	zmije	k1gFnSc1	zmije
<g/>
,	,	kIx,	,
chřestýšovití	chřestýšovití	k1gMnPc1	chřestýšovití
<g/>
,	,	kIx,	,
zmijovci	zmijovec	k1gInPc7	zmijovec
a	a	k8xC	a
pazmije	pazmít	k5eAaPmIp3nS	pazmít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jediný	jediný	k2eAgMnSc1d1	jediný
zástupce	zástupce	k1gMnSc1	zástupce
této	tento	k3xDgFnSc2	tento
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
,	,	kIx,	,
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
-	-	kIx~	-
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejčastějších	častý	k2eAgInPc2d3	nejčastější
druhů	druh	k1gInPc2	druh
ze	z	k7c2	z
zmijovitých	zmijovitý	k2eAgFnPc2d1	zmijovitý
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
had	had	k1gMnSc1	had
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
také	také	k9	také
jediný	jediný	k2eAgMnSc1d1	jediný
had	had	k1gMnSc1	had
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Severního	severní	k2eAgInSc2d1	severní
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
i	i	k8xC	i
nejtěžší	těžký	k2eAgInSc4d3	nejtěžší
zmijovitý	zmijovitý	k2eAgInSc4d1	zmijovitý
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
had	had	k1gMnSc1	had
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
zmije	zmije	k1gFnSc1	zmije
gabunská	gabunský	k2eAgFnSc1d1	gabunská
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
s	s	k7c7	s
nejdelšími	dlouhý	k2eAgInPc7d3	nejdelší
jedovými	jedový	k2eAgInPc7d1	jedový
zuby	zub	k1gInPc7	zub
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
jedovatých	jedovatý	k2eAgMnPc2d1	jedovatý
hadů	had	k1gMnPc2	had
(	(	kIx(	(
<g/>
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
5	[number]	k4	5
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
jedové	jedový	k2eAgInPc1d1	jedový
zuby	zub	k1gInPc1	zub
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
3	[number]	k4	3
centimetry	centimetr	k1gInPc4	centimetr
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
však	však	k9	však
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
křovináři	křovinář	k1gMnPc1	křovinář
rodu	rod	k1gInSc2	rod
Bothrops	Bothropsa	k1gFnPc2	Bothropsa
a	a	k8xC	a
chřestýši	chřestýš	k1gMnPc7	chřestýš
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgMnPc2	tento
hadů	had	k1gMnPc2	had
má	mít	k5eAaImIp3nS	mít
krátké	krátký	k2eAgNnSc4d1	krátké
zavalité	zavalitý	k2eAgNnSc4d1	zavalité
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
širokou	široký	k2eAgFnSc4d1	široká
trojúhelníkovou	trojúhelníkový	k2eAgFnSc4d1	trojúhelníková
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
drsné	drsný	k2eAgInPc4d1	drsný
zuby	zub	k1gInPc4	zub
sklápějící	sklápějící	k2eAgInPc4d1	sklápějící
se	se	k3xPyFc4	se
dozadu	dozadu	k6eAd1	dozadu
na	na	k7c4	na
patro	patro	k1gNnSc4	patro
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
nejdokonalejší	dokonalý	k2eAgInSc4d3	nejdokonalejší
typ	typ	k1gInSc4	typ
chrupu	chrup	k1gInSc2	chrup
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
solenoglyfní	solenoglyfní	k2eAgFnPc4d1	solenoglyfní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jedové	jedový	k2eAgFnPc4d1	jedová
žlázy	žláza	k1gFnPc4	žláza
jsou	být	k5eAaImIp3nP	být
napojeny	napojen	k2eAgInPc1d1	napojen
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
duté	dutý	k2eAgInPc1d1	dutý
jedové	jedový	k2eAgInPc1d1	jedový
zuby	zub	k1gInPc1	zub
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
čelistí	čelist	k1gFnPc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Zmijovití	zmijovitý	k2eAgMnPc1d1	zmijovitý
hadi	had	k1gMnPc1	had
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgFnPc1d1	pohybující
<g/>
.	.	kIx.	.
</s>
<s>
Spoléhají	spoléhat	k5eAaImIp3nP	spoléhat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
maskovací	maskovací	k2eAgInSc4d1	maskovací
vzhled	vzhled	k1gInSc4	vzhled
-	-	kIx~	-
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
zbarveni	zbarvit	k5eAaPmNgMnP	zbarvit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
splývali	splývat	k5eAaImAgMnP	splývat
s	s	k7c7	s
terénem	terén	k1gInSc7	terén
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
mají	mít	k5eAaImIp3nP	mít
složité	složitý	k2eAgInPc1d1	složitý
geometrické	geometrický	k2eAgInPc1d1	geometrický
vzory	vzor	k1gInPc1	vzor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
je	on	k3xPp3gMnPc4	on
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgMnPc1d3	nejmenší
zástupci	zástupce	k1gMnPc1	zástupce
této	tento	k3xDgFnSc2	tento
čeledi	čeleď	k1gFnSc2	čeleď
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
20	[number]	k4	20
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
největší	veliký	k2eAgFnSc4d3	veliký
pak	pak	k6eAd1	pak
přes	přes	k7c4	přes
3,5	[number]	k4	3,5
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Oční	oční	k2eAgFnPc1d1	oční
zorničky	zornička	k1gFnPc1	zornička
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
zabírají	zabírat	k5eAaImIp3nP	zabírat
většinu	většina	k1gFnSc4	většina
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Spousta	spousta	k1gFnSc1	spousta
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
také	také	k9	také
vybavena	vybavit	k5eAaPmNgFnS	vybavit
vertikálními	vertikální	k2eAgFnPc7d1	vertikální
zorničkami	zornička	k1gFnPc7	zornička
<g/>
,	,	kIx,	,
loví	lovit	k5eAaImIp3nP	lovit
tedy	tedy	k9	tedy
především	především	k9	především
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
<g/>
.	.	kIx.	.
</s>
<s>
Zmijovití	zmijovitý	k2eAgMnPc1d1	zmijovitý
z	z	k7c2	z
podčeledi	podčeleď	k1gFnSc2	podčeleď
chřestýšovitých	chřestýšovitý	k2eAgFnPc2d1	chřestýšovitý
pomocí	pomoc	k1gFnPc2	pomoc
tepločivných	tepločivný	k2eAgFnPc2d1	tepločivný
jamek	jamka	k1gFnPc2	jamka
detekují	detekovat	k5eAaImIp3nP	detekovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
tmě	tma	k1gFnSc6	tma
<g/>
.	.	kIx.	.
</s>
<s>
Chřestýši	chřestýš	k1gMnPc1	chřestýš
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ocasu	ocas	k1gInSc2	ocas
jedinečné	jedinečný	k2eAgNnSc4d1	jedinečné
varovné	varovný	k2eAgNnSc4d1	varovné
zařízení	zařízení	k1gNnSc4	zařízení
-	-	kIx~	-
chřestítko	chřestítko	k1gNnSc1	chřestítko
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zmijovití	zmijovitý	k2eAgMnPc1d1	zmijovitý
hadi	had	k1gMnPc1	had
kladou	klást	k5eAaImIp3nP	klást
vejce	vejce	k1gNnPc4	vejce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
živorodá	živorodý	k2eAgFnSc1d1	živorodá
nebo	nebo	k8xC	nebo
vejcoživorodá	vejcoživorodý	k2eAgFnSc1d1	vejcoživorodá
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
během	během	k7c2	během
dne	den	k1gInSc2	den
vyhřívají	vyhřívat	k5eAaImIp3nP	vyhřívat
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
urychlily	urychlit	k5eAaPmAgFnP	urychlit
vývoj	vývoj	k1gInSc4	vývoj
zárodku	zárodek	k1gInSc2	zárodek
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
v	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
se	se	k3xPyFc4	se
vykrmují	vykrmovat	k5eAaImIp3nP	vykrmovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zmijovití	zmijovitý	k2eAgMnPc1d1	zmijovitý
hadi	had	k1gMnPc1	had
loví	lovit	k5eAaImIp3nP	lovit
kořist	kořist	k1gFnSc4	kořist
ze	z	k7c2	z
zálohy	záloha	k1gFnSc2	záloha
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
používají	používat	k5eAaImIp3nP	používat
pestře	pestro	k6eAd1	pestro
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
konec	konec	k1gInSc4	konec
svého	svůj	k3xOyFgInSc2	svůj
ocasu	ocas	k1gInSc2	ocas
jako	jako	k8xC	jako
vábničku	vábnička	k1gFnSc4	vábnička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oběť	oběť	k1gFnSc4	oběť
útočí	útočit	k5eAaImIp3nS	útočit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zuby	zub	k1gInPc1	zub
pronikly	proniknout	k5eAaPmAgInP	proniknout
až	až	k9	až
k	k	k7c3	k
životně	životně	k6eAd1	životně
důležitým	důležitý	k2eAgInPc3d1	důležitý
orgánům	orgán	k1gInPc3	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
často	často	k6eAd1	často
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
proteiny	protein	k1gInPc4	protein
rozkládající	rozkládající	k2eAgFnSc2d1	rozkládající
krvinky	krvinka	k1gFnSc2	krvinka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobí	způsobit	k5eAaPmIp3nS	způsobit
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
krvácení	krvácení	k1gNnSc4	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uštknutí	uštknutí	k1gNnSc6	uštknutí
do	do	k7c2	do
kořisti	kořist	k1gFnSc2	kořist
vpravují	vpravovat	k5eAaImIp3nP	vpravovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
jedu	jed	k1gInSc2	jed
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
oběti	oběť	k1gFnSc2	oběť
nemusí	muset	k5eNaImIp3nS	muset
nastat	nastat	k5eAaPmF	nastat
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
.	.	kIx.	.
</s>
<s>
Jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
hryznutí	hryznutí	k1gNnSc1	hryznutí
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zmijovití	zmijovitý	k2eAgMnPc1d1	zmijovitý
však	však	k8xC	však
občas	občas	k6eAd1	občas
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
neuštknou	uštknout	k5eNaPmIp3nP	uštknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
kousnou	kousnout	k5eAaPmIp3nP	kousnout
a	a	k8xC	a
nevpraví	vpravit	k5eNaPmIp3nP	vpravit
do	do	k7c2	do
rány	rána	k1gFnSc2	rána
žádný	žádný	k3yNgInSc4	žádný
jed	jed	k1gInSc4	jed
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
raději	rád	k6eAd2	rád
uskladní	uskladnit	k5eAaPmIp3nP	uskladnit
na	na	k7c4	na
lov	lov	k1gInSc4	lov
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
člověk	člověk	k1gMnSc1	člověk
uštknut	uštknut	k2eAgMnSc1d1	uštknut
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
úmrtí	úmrtí	k1gNnSc4	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
zmijovití	zmijovitý	k2eAgMnPc1d1	zmijovitý
(	(	kIx(	(
<g/>
Viperidae	Viperidae	k1gNnSc7	Viperidae
<g/>
)	)	kIx)	)
-	-	kIx~	-
32	[number]	k4	32
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
231	[number]	k4	231
druhů	druh	k1gInPc2	druh
Podčeleď	podčeleď	k1gFnSc1	podčeleď
<g/>
:	:	kIx,	:
Azemiopinae	Azemiopinae	k1gNnSc1	Azemiopinae
-	-	kIx~	-
zmijovci	zmijovec	k1gMnPc1	zmijovec
-	-	kIx~	-
1	[number]	k4	1
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
1	[number]	k4	1
druh	druh	k1gInSc1	druh
Podčeleď	podčeleď	k1gFnSc1	podčeleď
<g/>
:	:	kIx,	:
Causinae	Causinae	k1gInSc1	Causinae
-	-	kIx~	-
pazmije	pazmít	k5eAaPmIp3nS	pazmít
-	-	kIx~	-
1	[number]	k4	1
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
6	[number]	k4	6
druhů	druh	k1gInPc2	druh
Podčeleď	podčeleď	k1gFnSc1	podčeleď
<g/>
:	:	kIx,	:
Viperinae	Viperinae	k1gFnSc1	Viperinae
-	-	kIx~	-
zmije	zmije	k1gFnSc1	zmije
-	-	kIx~	-
12	[number]	k4	12
<g />
.	.	kIx.	.
</s>
<s>
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
66	[number]	k4	66
druhů	druh	k1gInPc2	druh
Podčeleď	podčeleď	k1gFnSc1	podčeleď
<g/>
:	:	kIx,	:
Crotalinae	Crotalinae	k1gNnSc1	Crotalinae
-	-	kIx~	-
chřestýšovití	chřestýšovití	k1gMnPc1	chřestýšovití
-	-	kIx~	-
18	[number]	k4	18
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
151	[number]	k4	151
druhů	druh	k1gInPc2	druh
Ateris	Ateris	k1gFnSc2	Ateris
drsný	drsný	k2eAgMnSc1d1	drsný
(	(	kIx(	(
<g/>
Atheris	Atheris	k1gInSc1	Atheris
hispidus	hispidus	k1gInSc1	hispidus
<g/>
)	)	kIx)	)
Ateris	Ateris	k1gInSc1	Ateris
ježatý	ježatý	k2eAgInSc1d1	ježatý
(	(	kIx(	(
<g/>
Atheris	Atheris	k1gInSc1	Atheris
squamigera	squamiger	k1gMnSc2	squamiger
<g/>
)	)	kIx)	)
Křovinář	křovinář	k1gMnSc1	křovinář
němý	němý	k2eAgMnSc1d1	němý
(	(	kIx(	(
<g/>
Lachesis	Lachesis	k1gFnSc1	Lachesis
muta	muta	k1gMnSc1	muta
<g/>
)	)	kIx)	)
Křovinář	křovinář	k1gMnSc1	křovinář
ostnitý	ostnitý	k2eAgMnSc1d1	ostnitý
(	(	kIx(	(
<g/>
Bothriechis	Bothriechis	k1gFnSc4	Bothriechis
schlegelii	schlegelie	k1gFnSc4	schlegelie
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Křovinář	křovinář	k1gMnSc1	křovinář
sametový	sametový	k2eAgMnSc1d1	sametový
(	(	kIx(	(
<g/>
Bothrops	Bothrops	k1gInSc1	Bothrops
atrox	atrox	k1gInSc1	atrox
<g/>
)	)	kIx)	)
Křovinář	křovinář	k1gMnSc1	křovinář
aksamitový	aksamitový	k2eAgMnSc1d1	aksamitový
(	(	kIx(	(
<g/>
Bothrops	Bothrops	k1gInSc1	Bothrops
asper	asper	k1gMnSc1	asper
<g/>
)	)	kIx)	)
Křovinář	křovinář	k1gMnSc1	křovinář
žararaka	žararaka	k1gFnSc1	žararaka
(	(	kIx(	(
<g/>
Bothrops	Bothrops	k1gInSc1	Bothrops
jararaca	jararac	k1gInSc2	jararac
<g/>
)	)	kIx)	)
Chřestýš	chřestýš	k1gMnSc1	chřestýš
brazilský	brazilský	k2eAgMnSc1d1	brazilský
(	(	kIx(	(
<g/>
Crotalus	Crotalus	k1gMnSc1	Crotalus
durissus	durissus	k1gMnSc1	durissus
<g/>
)	)	kIx)	)
Chřestýš	chřestýš	k1gMnSc1	chřestýš
rohatý	rohatý	k1gMnSc1	rohatý
(	(	kIx(	(
<g/>
Crotalus	Crotalus	k1gMnSc1	Crotalus
cerastes	cerastes	k1gMnSc1	cerastes
<g/>
)	)	kIx)	)
Chřestýš	chřestýš	k1gMnSc1	chřestýš
mohavský	mohavský	k2eAgMnSc1d1	mohavský
(	(	kIx(	(
<g/>
Crotalus	Crotalus	k1gMnSc1	Crotalus
scutulatus	scutulatus	k1gMnSc1	scutulatus
<g/>
)	)	kIx)	)
Chřestýš	chřestýš	k1gMnSc1	chřestýš
skalní	skalní	k2eAgFnSc2d1	skalní
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Crotalus	Crotalus	k1gMnSc1	Crotalus
lepidus	lepidus	k1gMnSc1	lepidus
<g/>
)	)	kIx)	)
Chřestýš	chřestýš	k1gMnSc1	chřestýš
diamantový	diamantový	k2eAgMnSc1d1	diamantový
(	(	kIx(	(
<g/>
Crotalus	Crotalus	k1gMnSc1	Crotalus
adamanteus	adamanteus	k1gMnSc1	adamanteus
<g/>
)	)	kIx)	)
Chřestýš	chřestýš	k1gMnSc1	chřestýš
mexický	mexický	k2eAgMnSc1d1	mexický
(	(	kIx(	(
<g/>
Crotalus	Crotalus	k1gMnSc1	Crotalus
basiliscus	basiliscus	k1gMnSc1	basiliscus
<g/>
)	)	kIx)	)
Chřestýš	chřestýš	k1gMnSc1	chřestýš
západní	západní	k2eAgMnSc1d1	západní
(	(	kIx(	(
<g/>
Crotalus	Crotalus	k1gInSc1	Crotalus
atrox	atrox	k1gInSc1	atrox
<g/>
)	)	kIx)	)
Chřestýš	chřestýš	k1gMnSc1	chřestýš
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Crotalus	Crotalus	k1gMnSc1	Crotalus
horridus	horridus	k1gMnSc1	horridus
<g/>
)	)	kIx)	)
Chřestýš	chřestýš	k1gMnSc1	chřestýš
zelený	zelený	k2eAgMnSc1d1	zelený
(	(	kIx(	(
<g/>
Crotalus	Crotalus	k1gInSc1	Crotalus
viridis	viridis	k1gFnSc2	viridis
<g/>
)	)	kIx)	)
Chřestýš	chřestýš	k1gMnSc1	chřestýš
tygří	tygří	k2eAgMnSc1d1	tygří
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Crotalus	Crotalus	k1gInSc1	Crotalus
tigris	tigris	k1gFnSc2	tigris
<g/>
)	)	kIx)	)
Chřestýšovec	chřestýšovec	k1gMnSc1	chřestýšovec
běloretý	běloretý	k2eAgMnSc1d1	běloretý
(	(	kIx(	(
<g/>
Trimeresurus	Trimeresurus	k1gInSc1	Trimeresurus
albolabris	albolabris	k1gFnSc2	albolabris
<g/>
)	)	kIx)	)
Chřestýšovec	chřestýšovec	k1gMnSc1	chřestýšovec
mangšanský	mangšanský	k2eAgMnSc1d1	mangšanský
(	(	kIx(	(
<g/>
Trimeresurus	Trimeresurus	k1gInSc1	Trimeresurus
mangshanensis	mangshanensis	k1gFnSc2	mangshanensis
<g/>
)	)	kIx)	)
Chřestýšovec	chřestýšovec	k1gMnSc1	chřestýšovec
Waglerův	Waglerův	k2eAgMnSc1d1	Waglerův
(	(	kIx(	(
<g/>
Tropidolaemus	Tropidolaemus	k1gInSc1	Tropidolaemus
wagleri	wagler	k1gFnSc2	wagler
<g/>
)	)	kIx)	)
Ploskolebec	ploskolebec	k1gMnSc1	ploskolebec
vodní	vodní	k2eAgMnSc1d1	vodní
(	(	kIx(	(
<g/>
Agkistrodon	Agkistrodon	k1gMnSc1	Agkistrodon
piscivorus	piscivorus	k1gMnSc1	piscivorus
<g/>
)	)	kIx)	)
Ploskolebec	ploskolebec	k1gMnSc1	ploskolebec
plantážní	plantážní	k2eAgMnSc1d1	plantážní
(	(	kIx(	(
<g/>
Calloselasma	Calloselasm	k1gMnSc4	Calloselasm
rhodostoma	rhodostom	k1gMnSc4	rhodostom
<g/>
)	)	kIx)	)
Ploskolebec	ploskolebec	k1gMnSc1	ploskolebec
zakavkazský	zakavkazský	k2eAgMnSc1d1	zakavkazský
(	(	kIx(	(
<g/>
Gloydius	Gloydius	k1gMnSc1	Gloydius
intermedius	intermedius	k1gMnSc1	intermedius
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
(	(	kIx(	(
<g/>
Bitis	Bitis	k1gFnSc1	Bitis
arietans	arietans	k1gInSc1	arietans
<g/>
)	)	kIx)	)
Zmije	zmije	k1gFnSc2	zmije
pouštní	pouštní	k2eAgFnSc2d1	pouštní
(	(	kIx(	(
<g/>
Bitis	Bitis	k1gFnSc2	Bitis
caudalis	caudalis	k1gFnSc2	caudalis
<g/>
)	)	kIx)	)
Zmije	zmije	k1gFnSc1	zmije
gabunská	gabunský	k2eAgFnSc1d1	gabunská
(	(	kIx(	(
<g/>
Bitis	Bitis	k1gFnSc1	Bitis
gabonica	gabonica	k1gFnSc1	gabonica
<g/>
)	)	kIx)	)
Zmije	zmije	k1gFnSc1	zmije
pyramidová	pyramidový	k2eAgFnSc1d1	pyramidová
(	(	kIx(	(
<g/>
Echis	Echis	k1gFnSc1	Echis
pyramidum	pyramidum	k1gInSc1	pyramidum
<g/>
)	)	kIx)	)
Zmije	zmije	k1gFnSc1	zmije
růžkatá	růžkatý	k2eAgFnSc1d1	růžkatá
(	(	kIx(	(
<g/>
Vipera	Vipera	k1gFnSc1	Vipera
ammodytes	ammodytes	k1gMnSc1	ammodytes
<g/>
)	)	kIx)	)
Zmije	zmije	k1gFnSc1	zmije
rohatá	rohatý	k2eAgFnSc1d1	rohatá
(	(	kIx(	(
<g/>
Cerastes	Cerastes	k1gMnSc1	Cerastes
cerastes	cerastes	k1gMnSc1	cerastes
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
řetízková	řetízkový	k2eAgFnSc1d1	řetízková
(	(	kIx(	(
<g/>
Daboia	Daboia	k1gFnSc1	Daboia
russelli	russelle	k1gFnSc4	russelle
<g/>
)	)	kIx)	)
Zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Vipera	Vipera	k1gFnSc1	Vipera
berus	berus	k1gMnSc1	berus
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zmijovití	zmijovitý	k2eAgMnPc1d1	zmijovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
originálu	originál	k1gInSc2	originál
Animal	animal	k1gMnSc1	animal
vydaného	vydaný	k2eAgInSc2d1	vydaný
nakladatelstvím	nakladatelství	k1gNnPc3	nakladatelství
Dorling	Dorling	k1gInSc4	Dorling
Kindersley	Kinderslea	k1gFnPc1	Kinderslea
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
přeložil	přeložit	k5eAaPmAgMnS	přeložit
RNDr.	RNDr.	kA	RNDr.
Jiří	Jiří	k1gMnSc1	Jiří
Šmaha	Šmaha	k?	Šmaha
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
Euromedia	Euromedium	k1gNnSc2	Euromedium
Group	Group	k1gMnSc1	Group
k.	k.	k?	k.
s.	s.	k?	s.
-	-	kIx~	-
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
jako	jako	k8xC	jako
svou	svůj	k3xOyFgFnSc4	svůj
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
publikaci	publikace	k1gFnSc6	publikace
</s>
