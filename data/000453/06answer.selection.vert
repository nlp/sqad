<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
erupcí	erupce	k1gFnSc7	erupce
byla	být	k5eAaImAgFnS	být
erupce	erupce	k1gFnSc1	erupce
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
79	[number]	k4	79
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
města	město	k1gNnSc2	město
Pompeje	Pompeje	k1gInPc4	Pompeje
<g/>
,	,	kIx,	,
Herculaneum	Herculaneum	k1gNnSc1	Herculaneum
<g/>
,	,	kIx,	,
Oplontis	Oplontis	k1gFnSc1	Oplontis
a	a	k8xC	a
Stabie	Stabie	k1gFnSc1	Stabie
<g/>
.	.	kIx.	.
</s>
