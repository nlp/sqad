<s>
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
novela	novela	k1gFnSc1	novela
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Ernesta	Ernest	k1gMnSc2	Ernest
Hemingwaye	Hemingway	k1gMnSc2	Hemingway
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaBmAgInS	napsat
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
