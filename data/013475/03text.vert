<s>
Groslay	Groslaa	k1gFnPc1
</s>
<s>
Groslay	Grosla	k2eAgFnPc1d1
radnice	radnice	k1gFnPc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
43	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
51	#num#	k4
<g/>
–	–	k?
<g/>
123	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
region	region	k1gInSc1
</s>
<s>
Île-de-France	Île-de-France	k1gFnSc1
departement	departement	k1gInSc1
</s>
<s>
Val-d	Val-d	k1gInSc1
<g/>
'	'	kIx"
<g/>
Oise	Oise	k1gInSc1
arrondissement	arrondissement	k1gInSc1
</s>
<s>
Sarcelles	Sarcelles	k1gInSc1
kanton	kanton	k1gInSc1
</s>
<s>
Deuil-la-Barre	Deuil-la-Barr	k1gMnSc5
</s>
<s>
Groslay	Groslaa	k1gFnPc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
2,94	2,94	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
8	#num#	k4
654	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
2	#num#	k4
943,5	943,5	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.mairie-groslay.fr	www.mairie-groslay.fr	k1gMnSc1
PSČ	PSČ	kA
</s>
<s>
95410	#num#	k4
INSEE	INSEE	kA
</s>
<s>
95288	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Groslay	Groslay	k1gFnSc1
je	být	k5eAaImIp3nS
francouzská	francouzský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
v	v	k7c6
departementu	departement	k1gInSc6
Val-d	Val-d	k1gFnPc2
<g/>
'	'	kIx"
<g/>
Oise	Oise	k1gFnSc1
v	v	k7c6
regionu	region	k1gInSc6
Île-de-France	Île-de-France	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
8	#num#	k4
654	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
obce	obec	k1gFnSc2
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1
obce	obec	k1gFnPc1
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
Deuil-la-Barre	Deuil-la-Barr	k1gMnSc5
<g/>
,	,	kIx,
Montmagny	Montmagn	k1gMnPc4
<g/>
,	,	kIx,
Montmorency	Montmorenc	k1gMnPc4
<g/>
,	,	kIx,
Saint-Brice-sous-Forê	Saint-Brice-sous-Forê	k1gMnSc1
a	a	k8xC
Sarcelles	Sarcelles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
departementu	departement	k1gInSc6
Val-d	Val-da	k1gFnPc2
<g/>
'	'	kIx"
<g/>
Oise	Oise	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Groslay	Groslaa	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obce	obec	k1gFnSc2
kantonu	kanton	k1gInSc2
Deuil-la-Barre	Deuil-la-Barr	k1gMnSc5
</s>
<s>
Deuil-la-Barre	Deuil-la-Barr	k1gMnSc5
•	•	k?
Groslay	Groslaa	k1gMnSc2
•	•	k?
Montmagny	Montmagna	k1gMnSc2
•	•	k?
Saint-Brice-sous-Forê	Saint-Brice-sous-Forê	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4278950-3	4278950-3	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
240015380	#num#	k4
</s>
