<s>
Praha	Praha	k1gFnSc1	Praha
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
osobní	osobní	k2eAgNnSc1d1	osobní
železniční	železniční	k2eAgNnSc1d1	železniční
nádraží	nádraží	k1gNnSc1	nádraží
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
pražské	pražský	k2eAgNnSc1d1	Pražské
nádraží	nádraží	k1gNnSc1	nádraží
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Wilsonova	Wilsonův	k2eAgFnSc1d1	Wilsonova
300	[number]	k4	300
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Praha	Praha	k1gFnSc1	Praha
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
užívalo	užívat	k5eAaImAgNnS	užívat
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1940	[number]	k4	1940
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1953	[number]	k4	1953
jej	on	k3xPp3gInSc4	on
užívá	užívat	k5eAaImIp3nS	užívat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
nádraží	nádraží	k1gNnSc1	nádraží
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
Wilsonovo	Wilsonův	k2eAgNnSc4d1	Wilsonovo
nádraží	nádraží	k1gNnSc4	nádraží
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
[	[	kIx(	[
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolejiště	kolejiště	k1gNnSc1	kolejiště
<g/>
,	,	kIx,	,
Fantova	Fantův	k2eAgFnSc1d1	Fantova
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
provozní	provozní	k2eAgFnSc1d1	provozní
budova	budova	k1gFnSc1	budova
(	(	kIx(	(
<g/>
přiléhající	přiléhající	k2eAgInPc1d1	přiléhající
k	k	k7c3	k
Seifertově	Seifertův	k2eAgFnSc3d1	Seifertova
ulici	ulice	k1gFnSc3	ulice
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
2	[number]	k4	2
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
<g/>
,	,	kIx,	,
severní	severní	k2eAgNnSc1d1	severní
zhlaví	zhlaví	k1gNnSc1	zhlaví
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
Žižkova	Žižkov	k1gInSc2	Žižkov
na	na	k7c6	na
Praze	Praha	k1gFnSc6	Praha
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Městské	městský	k2eAgFnSc6d1	městská
památkové	památkový	k2eAgFnSc6d1	památková
zóně	zóna	k1gFnSc6	zóna
Vinohrady	Vinohrady	k1gInPc4	Vinohrady
<g/>
,	,	kIx,	,
Žižkov	Žižkov	k1gInSc1	Žižkov
a	a	k8xC	a
Vršovice	Vršovice	k1gFnPc1	Vršovice
<g/>
,	,	kIx,	,
Fantova	Fantův	k2eAgFnSc1d1	Fantova
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
hala	hala	k1gFnSc1	hala
nad	nad	k7c7	nad
kolejištěm	kolejiště	k1gNnSc7	kolejiště
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nemovité	movitý	k2eNgFnPc4d1	nemovitá
kulturní	kulturní	k2eAgFnPc4d1	kulturní
památky	památka	k1gFnPc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Vybudování	vybudování	k1gNnSc1	vybudování
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
lokalitě	lokalita	k1gFnSc6	lokalita
přispělo	přispět	k5eAaPmAgNnS	přispět
císařské	císařský	k2eAgNnSc1d1	císařské
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
z	z	k7c2	z
října	říjen	k1gInSc2	říjen
1866	[number]	k4	1866
o	o	k7c6	o
zboření	zboření	k1gNnSc6	zboření
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
byl	být	k5eAaImAgInS	být
zahájen	zahájen	k2eAgInSc1d1	zahájen
železniční	železniční	k2eAgInSc1d1	železniční
provoz	provoz	k1gInSc1	provoz
Dráhy	dráha	k1gFnSc2	dráha
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Benešova	Benešov	k1gInSc2	Benešov
<g/>
,	,	kIx,	,
Tábora	Tábor	k1gInSc2	Tábor
a	a	k8xC	a
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
Nádraží	nádraží	k1gNnSc1	nádraží
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
nádraží	nádraží	k1gNnSc2	nádraží
Dráhy	dráha	k1gFnSc2	dráha
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
samostatné	samostatný	k2eAgFnSc6d1	samostatná
stanici	stanice	k1gFnSc6	stanice
ukončena	ukončen	k2eAgNnPc1d1	ukončeno
od	od	k7c2	od
severu	sever	k1gInSc2	sever
Turnovsko-kralupsko-pražská	Turnovskoralupskoražský	k2eAgFnSc1d1	Turnovsko-kralupsko-pražská
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
nádraží	nádraží	k1gNnPc1	nádraží
brzy	brzy	k6eAd1	brzy
splynula	splynout	k5eAaPmAgNnP	splynout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
Pražská	pražský	k2eAgFnSc1d1	Pražská
spojovací	spojovací	k2eAgFnSc1d1	spojovací
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Smíchov	Smíchov	k1gInSc1	Smíchov
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
západní	západní	k2eAgFnSc1d1	západní
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
)	)	kIx)	)
–	–	k?	–
Hrabovka	Hrabovka	k1gFnSc1	Hrabovka
(	(	kIx(	(
<g/>
napojení	napojení	k1gNnSc1	napojení
na	na	k7c4	na
Rakouskou	rakouský	k2eAgFnSc4d1	rakouská
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
dráhu	dráha	k1gFnSc4	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
nádraží	nádraží	k1gNnSc1	nádraží
čtyř	čtyři	k4xCgFnPc2	čtyři
různých	různý	k2eAgFnPc2d1	různá
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vinohradským	vinohradský	k2eAgInSc7d1	vinohradský
tunelem	tunel	k1gInSc7	tunel
vedly	vést	k5eAaImAgFnP	vést
dráhy	dráha	k1gFnPc1	dráha
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
na	na	k7c4	na
Smíchov	Smíchov	k1gInSc4	Smíchov
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
zdvojkolejnění	zdvojkolejnění	k1gNnSc6	zdvojkolejnění
spojovací	spojovací	k2eAgFnSc2d1	spojovací
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
tratě	trať	k1gFnPc1	trať
propojeny	propojit	k5eAaPmNgFnP	propojit
již	již	k6eAd1	již
před	před	k7c7	před
vršovickým	vršovický	k2eAgInSc7d1	vršovický
portálem	portál	k1gInSc7	portál
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
budova	budova	k1gFnSc1	budova
nádraží	nádraží	k1gNnSc2	nádraží
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Ignáce	Ignác	k1gMnSc2	Ignác
Ullmanna	Ullmann	k1gMnSc2	Ullmann
a	a	k8xC	a
Antonína	Antonín	k1gMnSc2	Antonín
Barvitia	Barvitius	k1gMnSc2	Barvitius
v	v	k7c6	v
novorenesančním	novorenesanční	k2eAgInSc6d1	novorenesanční
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
stavba	stavba	k1gFnSc1	stavba
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
<g/>
,	,	kIx,	,
halou	hala	k1gFnSc7	hala
s	s	k7c7	s
kazetovým	kazetový	k2eAgInSc7d1	kazetový
stropem	strop	k1gInSc7	strop
<g/>
,	,	kIx,	,
restaurací	restaurace	k1gFnSc7	restaurace
<g/>
,	,	kIx,	,
kancelářemi	kancelář	k1gFnPc7	kancelář
a	a	k8xC	a
22	[number]	k4	22
dveřmi	dveře	k1gFnPc7	dveře
vedoucími	vedoucí	k2eAgFnPc7d1	vedoucí
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
s	s	k7c7	s
mozaikovou	mozaikový	k2eAgFnSc7d1	mozaiková
dlažbou	dlažba	k1gFnSc7	dlažba
si	se	k3xPyFc3	se
vysloužila	vysloužit	k5eAaPmAgFnS	vysloužit
přezdívku	přezdívka	k1gFnSc4	přezdívka
zámecké	zámecký	k2eAgNnSc1d1	zámecké
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlického	Vrchlického	k2eAgFnPc1d1	Vrchlického
sady	sada	k1gFnPc1	sada
před	před	k7c7	před
nádražím	nádraží	k1gNnSc7	nádraží
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
roku	rok	k1gInSc3	rok
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
budova	budova	k1gFnSc1	budova
ani	ani	k8xC	ani
kolejiště	kolejiště	k1gNnSc4	kolejiště
kapacitně	kapacitně	k6eAd1	kapacitně
přestávaly	přestávat	k5eAaImAgFnP	přestávat
rozvoji	rozvoj	k1gInSc3	rozvoj
železnice	železnice	k1gFnSc2	železnice
stačit	stačit	k5eAaBmF	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
nádraží	nádraží	k1gNnSc1	nádraží
zrekonstruováno	zrekonstruován	k2eAgNnSc1d1	zrekonstruováno
a	a	k8xC	a
zvětšeno	zvětšen	k2eAgNnSc1d1	zvětšeno
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
secesním	secesní	k2eAgInSc6d1	secesní
slohu	sloh	k1gInSc6	sloh
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
podle	podle	k7c2	podle
vítězného	vítězný	k2eAgInSc2d1	vítězný
návrhu	návrh	k1gInSc2	návrh
Josefa	Josef	k1gMnSc2	Josef
Fanty	Fanta	k1gMnSc2	Fanta
na	na	k7c6	na
základě	základ	k1gInSc6	základ
architektonické	architektonický	k2eAgFnSc2d1	architektonická
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Pokladny	pokladna	k1gFnPc1	pokladna
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
kopule	kopule	k1gFnSc2	kopule
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
kavárny	kavárna	k1gFnSc2	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Kopuli	kopule	k1gFnSc4	kopule
zdobí	zdobit	k5eAaImIp3nP	zdobit
secesní	secesní	k2eAgInPc1d1	secesní
motivy	motiv	k1gInPc1	motiv
a	a	k8xC	a
sochy	socha	k1gFnPc1	socha
od	od	k7c2	od
Stanislava	Stanislav	k1gMnSc2	Stanislav
Suchardy	Sucharda	k1gMnSc2	Sucharda
a	a	k8xC	a
Ladislava	Ladislav	k1gMnSc2	Ladislav
Šalouna	Šaloun	k1gMnSc2	Šaloun
zpodobňující	zpodobňující	k2eAgNnPc1d1	zpodobňující
česká	český	k2eAgNnPc1d1	české
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Cestujícím	cestující	k1gMnPc3	cestující
sloužily	sloužit	k5eAaImAgInP	sloužit
velkoryse	velkoryse	k6eAd1	velkoryse
pojaté	pojatý	k2eAgFnSc2d1	pojatá
čekárny	čekárna	k1gFnSc2	čekárna
v	v	k7c6	v
levém	levý	k2eAgNnSc6d1	levé
křídle	křídlo	k1gNnSc6	křídlo
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
restaurace	restaurace	k1gFnPc1	restaurace
různých	různý	k2eAgFnPc2d1	různá
cenových	cenový	k2eAgFnPc2d1	cenová
hladin	hladina	k1gFnPc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
disponovala	disponovat	k5eAaBmAgFnS	disponovat
také	také	k9	také
kancelářemi	kancelář	k1gFnPc7	kancelář
a	a	k8xC	a
salonky	salonek	k1gInPc7	salonek
pro	pro	k7c4	pro
významné	významný	k2eAgMnPc4d1	významný
hosty	host	k1gMnPc4	host
<g/>
,	,	kIx,	,
na	na	k7c4	na
vlak	vlak	k1gInSc4	vlak
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
čekali	čekat	k5eAaImAgMnP	čekat
například	například	k6eAd1	například
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
nebo	nebo	k8xC	nebo
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplina	k1gFnPc2	Chaplina
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
novou	nový	k2eAgFnSc7d1	nová
budovou	budova	k1gFnSc7	budova
byly	být	k5eAaImAgInP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
podchody	podchod	k1gInPc1	podchod
a	a	k8xC	a
kolejiště	kolejiště	k1gNnSc1	kolejiště
bylo	být	k5eAaImAgNnS	být
zastřešeno	zastřešit	k5eAaPmNgNnS	zastřešit
dvoulodní	dvoulodní	k2eAgFnSc7d1	dvoulodní
ocelovou	ocelový	k2eAgFnSc7d1	ocelová
obloukovou	obloukový	k2eAgFnSc7d1	oblouková
halou	hala	k1gFnSc7	hala
o	o	k7c4	o
rozpětí	rozpětí	k1gNnSc4	rozpětí
33,3	[number]	k4	33,3
m	m	kA	m
a	a	k8xC	a
výšce	výška	k1gFnSc6	výška
18	[number]	k4	18
m	m	kA	m
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
autory	autor	k1gMnPc4	autor
byli	být	k5eAaImAgMnP	být
J.	J.	kA	J.
Marjanko	Marjanka	k1gFnSc5	Marjanka
a	a	k8xC	a
R.	R.	kA	R.
Kornfeld	Kornfeld	k1gInSc1	Kornfeld
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukci	konstrukce	k1gFnSc4	konstrukce
zhotovila	zhotovit	k5eAaPmAgFnS	zhotovit
firma	firma	k1gFnSc1	firma
S.	S.	kA	S.
Bondy	bond	k1gInPc4	bond
v	v	k7c6	v
Bubnech	Bubny	k1gInPc6	Bubny
<g/>
.	.	kIx.	.
</s>
<s>
Hala	hala	k1gFnSc1	hala
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
současná	současný	k2eAgFnSc1d1	současná
nástupiště	nástupiště	k1gNnSc4	nástupiště
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
až	až	k8xS	až
4	[number]	k4	4
<g/>
,	,	kIx,	,
nástupiště	nástupiště	k1gNnSc4	nástupiště
č.	č.	k?	č.
1A	[number]	k4	1A
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgNnSc4d1	hlavní
zastřešení	zastřešení	k1gNnSc4	zastřešení
vysunuto	vysunut	k2eAgNnSc4d1	vysunuto
k	k	k7c3	k
vinohradskému	vinohradský	k2eAgNnSc3d1	Vinohradské
zhlaví	zhlaví	k1gNnSc3	zhlaví
<g/>
,	,	kIx,	,
nástupiště	nástupiště	k1gNnSc2	nástupiště
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
zastřešení	zastřešení	k1gNnSc4	zastřešení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tvarově	tvarově	k6eAd1	tvarově
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c6	na
konstrukci	konstrukce	k1gFnSc6	konstrukce
haly	hala	k1gFnSc2	hala
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
začalo	začít	k5eAaPmAgNnS	začít
rakouské	rakouský	k2eAgNnSc1d1	rakouské
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c4	o
elektrizaci	elektrizace	k1gFnSc4	elektrizace
železnice	železnice	k1gFnSc2	železnice
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Podnikatel	podnikatel	k1gMnSc1	podnikatel
Emil	Emil	k1gMnSc1	Emil
Kolben	Kolben	k2eAgMnSc1d1	Kolben
tehdy	tehdy	k6eAd1	tehdy
dal	dát	k5eAaPmAgInS	dát
Rakousko-uherským	rakouskoherský	k2eAgFnPc3d1	rakousko-uherská
státním	státní	k2eAgFnPc3d1	státní
drahám	draha	k1gFnPc3	draha
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c6	na
elektrizaci	elektrizace	k1gFnSc6	elektrizace
pražských	pražský	k2eAgFnPc2d1	Pražská
spojek	spojka	k1gFnPc2	spojka
soustavou	soustava	k1gFnSc7	soustava
10	[number]	k4	10
kV	kV	k?	kV
16	[number]	k4	16
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
nebyla	být	k5eNaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenkou	myšlenka	k1gFnSc7	myšlenka
elektrizace	elektrizace	k1gFnSc2	elektrizace
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
začalo	začít	k5eAaPmAgNnS	začít
zabývat	zabývat	k5eAaImF	zabývat
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
železnic	železnice	k1gFnPc2	železnice
ČSR	ČSR	kA	ČSR
hned	hned	k6eAd1	hned
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
byl	být	k5eAaImAgInS	být
jako	jako	k8xC	jako
optimální	optimální	k2eAgInSc4d1	optimální
vybrán	vybrán	k2eAgInSc4d1	vybrán
systém	systém	k1gInSc4	systém
1500	[number]	k4	1500
Vss	Vss	k1gFnPc2	Vss
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1926	[number]	k4	1926
byly	být	k5eAaImAgInP	být
slavnostně	slavnostně	k6eAd1	slavnostně
<g/>
,	,	kIx,	,
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
Františka	František	k1gMnSc2	František
Křižíka	Křižík	k1gMnSc2	Křižík
a	a	k8xC	a
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
E	E	kA	E
407.001	[number]	k4	407.001
<g/>
,	,	kIx,	,
zahájeny	zahájen	k2eAgFnPc4d1	zahájena
stavební	stavební	k2eAgFnPc4d1	stavební
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1928	[number]	k4	1928
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Wilsonově	Wilsonův	k2eAgNnSc6d1	Wilsonovo
nádraží	nádraží	k1gNnSc6	nádraží
zahájen	zahájen	k2eAgInSc1d1	zahájen
posun	posun	k1gInSc1	posun
v	v	k7c6	v
elektrické	elektrický	k2eAgFnSc6d1	elektrická
trakci	trakce	k1gFnSc6	trakce
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
v	v	k7c6	v
elektrické	elektrický	k2eAgFnSc6d1	elektrická
trakci	trakce	k1gFnSc6	trakce
rozjely	rozjet	k5eAaPmAgInP	rozjet
osobní	osobní	k2eAgInPc1d1	osobní
vlaky	vlak	k1gInPc1	vlak
do	do	k7c2	do
Vysočan	Vysočany	k1gInPc2	Vysočany
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
jízdním	jízdní	k2eAgInSc7d1	jízdní
řádem	řád	k1gInSc7	řád
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1928	[number]	k4	1928
zahájen	zahájen	k2eAgInSc1d1	zahájen
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
elektrický	elektrický	k2eAgInSc1d1	elektrický
provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
elektrizovaných	elektrizovaný	k2eAgFnPc6d1	elektrizovaná
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Napájení	napájení	k1gNnSc1	napájení
a	a	k8xC	a
dobíjení	dobíjení	k1gNnSc1	dobíjení
akumulátorových	akumulátorový	k2eAgFnPc2d1	akumulátorová
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
obstarávala	obstarávat	k5eAaImAgFnS	obstarávat
měnírna	měnírna	k1gFnSc1	měnírna
Křenovka	Křenovka	k1gFnSc1	Křenovka
<g/>
.	.	kIx.	.
</s>
<s>
Lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
byly	být	k5eAaImAgFnP	být
deponovány	deponovat	k5eAaBmNgFnP	deponovat
ve	v	k7c6	v
výtopně	výtopna	k1gFnSc6	výtopna
na	na	k7c6	na
Wilsonově	Wilsonův	k2eAgNnSc6d1	Wilsonovo
nádraží	nádraží	k1gNnSc6	nádraží
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zatrolejovaná	zatrolejovaný	k2eAgFnSc1d1	zatrolejovaná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
Smíchov	Smíchov	k1gInSc4	Smíchov
zdvojkolejněna	zdvojkolejnit	k5eAaPmNgFnS	zdvojkolejnit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
spojka	spojka	k1gFnSc1	spojka
Vítkov	Vítkov	k1gInSc1	Vítkov
–	–	k?	–
Libeň	Libeň	k1gFnSc1	Libeň
horní	horní	k2eAgNnSc1d1	horní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
;	;	kIx,	;
Vítkovská	vítkovský	k2eAgFnSc1d1	Vítkovská
trať	trať	k1gFnSc1	trať
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
doplnila	doplnit	k5eAaPmAgFnS	doplnit
jednokolejnou	jednokolejný	k2eAgFnSc4d1	jednokolejná
spojku	spojka	k1gFnSc4	spojka
druhou	druhý	k4xOgFnSc4	druhý
stranou	stranou	k6eAd1	stranou
kopce	kopec	k1gInPc4	kopec
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Hrabovku	Hrabovka	k1gFnSc4	Hrabovka
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
druhý	druhý	k4xOgInSc1	druhý
vinohradský	vinohradský	k2eAgInSc1d1	vinohradský
tunel	tunel	k1gInSc1	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgFnSc1	veškerý
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
provedena	proveden	k2eAgFnSc1d1	provedena
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
prvního	první	k4xOgInSc2	první
tunelu	tunel	k1gInSc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
dokončení	dokončení	k1gNnSc6	dokončení
byly	být	k5eAaImAgFnP	být
tratě	trať	k1gFnPc1	trať
do	do	k7c2	do
Vršovic	Vršovice	k1gFnPc2	Vršovice
a	a	k8xC	a
na	na	k7c4	na
Smíchov	Smíchov	k1gInSc4	Smíchov
definitivně	definitivně	k6eAd1	definitivně
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1967	[number]	k4	1967
začala	začít	k5eAaPmAgFnS	začít
stavba	stavba	k1gFnSc1	stavba
metra	metro	k1gNnSc2	metro
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
a	a	k8xC	a
nejstarší	starý	k2eAgFnSc1d3	nejstarší
stanice	stanice	k1gFnSc1	stanice
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
prvních	první	k4xOgInPc2	první
plánů	plán	k1gInPc2	plán
ještě	ještě	k6eAd1	ještě
jako	jako	k8xC	jako
podpovrchové	podpovrchový	k2eAgFnPc1d1	podpovrchová
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vybudování	vybudování	k1gNnSc6	vybudování
metra	metro	k1gNnSc2	metro
namísto	namísto	k7c2	namísto
tramvaje	tramvaj	k1gFnSc2	tramvaj
vláda	vláda	k1gFnSc1	vláda
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
až	až	k9	až
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	můj	k3xOp1gFnSc1	můj
stanice	stanice	k1gFnSc1	stanice
méně	málo	k6eAd2	málo
typická	typický	k2eAgFnSc1d1	typická
dvě	dva	k4xCgNnPc4	dva
boční	boční	k2eAgNnPc4d1	boční
nástupiště	nástupiště	k1gNnPc4	nástupiště
namísto	namísto	k7c2	namísto
jednoho	jeden	k4xCgInSc2	jeden
středového	středový	k2eAgInSc2d1	středový
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1974	[number]	k4	1974
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
architektonické	architektonický	k2eAgFnSc2d1	architektonická
soutěže	soutěž	k1gFnSc2	soutěž
nová	nový	k2eAgFnSc1d1	nová
pozdemní	pozdemní	k2eAgFnSc1d1	pozdemní
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
hala	hala	k1gFnSc1	hala
(	(	kIx(	(
<g/>
slohově	slohově	k6eAd1	slohově
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
řadit	řadit	k5eAaImF	řadit
k	k	k7c3	k
brutalismu	brutalismus	k1gInSc3	brutalismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
část	část	k1gFnSc1	část
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
sadů	sad	k1gInPc2	sad
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
jsou	být	k5eAaImIp3nP	být
Josef	Josef	k1gMnSc1	Josef
Danda	Danda	k1gMnSc1	Danda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Bočan	bočan	k1gMnSc1	bočan
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
Trnková	Trnková	k1gFnSc1	Trnková
a	a	k8xC	a
Alena	Alena	k1gFnSc1	Alena
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Šrámkovi	Šrámek	k1gMnSc3	Šrámek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
střeše	střecha	k1gFnSc6	střecha
haly	hala	k1gFnSc2	hala
vede	vést	k5eAaImIp3nS	vést
Severojižní	severojižní	k2eAgFnSc1d1	severojižní
magistrála	magistrála	k1gFnSc1	magistrála
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tak	tak	k6eAd1	tak
odřízla	odříznout	k5eAaPmAgFnS	odříznout
historickou	historický	k2eAgFnSc4d1	historická
budovu	budova	k1gFnSc4	budova
od	od	k7c2	od
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
dobudován	dobudovat	k5eAaPmNgInS	dobudovat
třetí	třetí	k4xOgInSc1	třetí
vinohradský	vinohradský	k2eAgInSc1d1	vinohradský
tunel	tunel	k1gInSc1	tunel
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
zhlaví	zhlaví	k1gNnSc6	zhlaví
provedena	proveden	k2eAgFnSc1d1	provedena
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
výhybek	výhybka	k1gFnPc2	výhybka
a	a	k8xC	a
zabezpečovacího	zabezpečovací	k2eAgNnSc2d1	zabezpečovací
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
fungovala	fungovat	k5eAaImAgNnP	fungovat
mechanická	mechanický	k2eAgNnPc1d1	mechanické
návěstidla	návěstidlo	k1gNnPc1	návěstidlo
a	a	k8xC	a
výhybky	výhybka	k1gFnPc1	výhybka
byly	být	k5eAaImAgFnP	být
přestavovány	přestavovat	k5eAaImNgFnP	přestavovat
ručně	ručně	k6eAd1	ručně
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zbořeno	zbořen	k2eAgNnSc4d1	zbořeno
stavědlo	stavědlo	k1gNnSc4	stavědlo
Sakrabonka	Sakrabonka	k1gFnSc1	Sakrabonka
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
usedlosti	usedlost	k1gFnSc2	usedlost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
stavbě	stavba	k1gFnSc3	stavba
nádraží	nádraží	k1gNnSc6	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádraží	nádraží	k1gNnSc6	nádraží
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
další	další	k2eAgNnPc4d1	další
tři	tři	k4xCgNnPc4	tři
nástupiště	nástupiště	k1gNnPc4	nástupiště
<g/>
,	,	kIx,	,
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
5	[number]	k4	5
až	až	k8xS	až
7	[number]	k4	7
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mají	mít	k5eAaImIp3nP	mít
samostatná	samostatný	k2eAgNnPc4d1	samostatné
klenutá	klenutý	k2eAgNnPc4d1	klenuté
zastřešení	zastřešení	k1gNnPc4	zastřešení
<g/>
.	.	kIx.	.
</s>
<s>
Přestavba	přestavba	k1gFnSc1	přestavba
severního	severní	k2eAgNnSc2d1	severní
(	(	kIx(	(
<g/>
východního	východní	k2eAgNnSc2d1	východní
<g/>
)	)	kIx)	)
zhlaví	zhlaví	k1gNnSc2	zhlaví
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
nástupišti	nástupiště	k1gNnSc6	nástupiště
odhalen	odhalen	k2eAgInSc1d1	odhalen
památník	památník	k1gInSc1	památník
a	a	k8xC	a
socha	socha	k1gFnSc1	socha
brita	brit	k1gInSc2	brit
Nicholase	Nicholasa	k1gFnSc3	Nicholasa
Wintona	Wintona	k1gFnSc1	Wintona
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
poslal	poslat	k5eAaPmAgInS	poslat
669	[number]	k4	669
převážně	převážně	k6eAd1	převážně
židovských	židovský	k2eAgFnPc2d1	židovská
dětí	dítě	k1gFnPc2	dítě
vlakem	vlak	k1gInSc7	vlak
do	do	k7c2	do
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
nacistům	nacista	k1gMnPc3	nacista
a	a	k8xC	a
odvozům	odvoz	k1gInPc3	odvoz
do	do	k7c2	do
koncentračních	koncentrační	k2eAgMnPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
sochy	socha	k1gFnSc2	socha
je	být	k5eAaImIp3nS	být
Flor	Flor	k1gInSc4	Flor
Kentová	Kentová	k1gFnSc1	Kentová
<g/>
.	.	kIx.	.
</s>
<s>
Technický	technický	k2eAgInSc1d1	technický
stav	stav	k1gInSc1	stav
nádraží	nádraží	k1gNnSc2	nádraží
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
špatný	špatný	k2eAgInSc4d1	špatný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
započala	započnout	k5eAaPmAgFnS	započnout
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
,	,	kIx,	,
modernizace	modernizace	k1gFnSc1	modernizace
a	a	k8xC	a
revitalizace	revitalizace	k1gFnSc1	revitalizace
s	s	k7c7	s
rozpočtem	rozpočet	k1gInSc7	rozpočet
přesahujícím	přesahující	k2eAgInSc7d1	přesahující
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc7d1	celá
ji	on	k3xPp3gFnSc4	on
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
italská	italský	k2eAgFnSc1d1	italská
společnost	společnost	k1gFnSc1	společnost
Grandi	grand	k1gMnPc1	grand
Stazioni	Stazion	k1gMnPc1	Stazion
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
projektovou	projektový	k2eAgFnSc4d1	projektová
činnost	činnost	k1gFnSc4	činnost
vybrala	vybrat	k5eAaPmAgFnS	vybrat
sdružení	sdružení	k1gNnSc4	sdružení
projektantů	projektant	k1gMnPc2	projektant
Metroprojekt	Metroprojekt	k1gInSc1	Metroprojekt
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
architektem	architekt	k1gMnSc7	architekt
Patrikem	Patrik	k1gMnSc7	Patrik
Kotasem	Kotas	k1gMnSc7	Kotas
a	a	k8xC	a
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
původními	původní	k2eAgMnPc7d1	původní
architekty	architekt	k1gMnPc7	architekt
(	(	kIx(	(
<g/>
Janem	Jan	k1gMnSc7	Jan
Bočanem	bočan	k1gMnSc7	bočan
a	a	k8xC	a
Alenou	Alena	k1gFnSc7	Alena
Šrámkovou	Šrámková	k1gFnSc7	Šrámková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modernizace	modernizace	k1gFnSc1	modernizace
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
odbavovací	odbavovací	k2eAgFnSc2d1	odbavovací
haly	hala	k1gFnSc2	hala
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
Fantovy	Fantův	k2eAgFnSc2d1	Fantova
budovy	budova	k1gFnSc2	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc4	rozšíření
a	a	k8xC	a
opravy	oprava	k1gFnPc4	oprava
nástupišť	nástupiště	k1gNnPc2	nástupiště
a	a	k8xC	a
vyměnění	vyměnění	k1gNnSc6	vyměnění
nástupištních	nástupištní	k2eAgFnPc2d1	nástupištní
hal	hala	k1gFnPc2	hala
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
byly	být	k5eAaImAgFnP	být
rekonstruovány	rekonstruovat	k5eAaBmNgFnP	rekonstruovat
nástupiště	nástupiště	k1gNnSc4	nástupiště
1	[number]	k4	1
až	až	k9	až
4	[number]	k4	4
včetně	včetně	k7c2	včetně
nových	nový	k2eAgInPc2d1	nový
přístřešku	přístřešek	k1gInSc2	přístřešek
nad	nad	k7c7	nad
částmi	část	k1gFnPc7	část
nástupišť	nástupiště	k1gNnPc2	nástupiště
mimo	mimo	k7c4	mimo
historickou	historický	k2eAgFnSc4d1	historická
halu	hala	k1gFnSc4	hala
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
prošly	projít	k5eAaPmAgInP	projít
oba	dva	k4xCgInPc1	dva
podchody	podchod	k1gInPc1	podchod
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgFnPc2	který
byly	být	k5eAaImAgFnP	být
instalovány	instalovat	k5eAaBmNgInP	instalovat
eskalátory	eskalátor	k1gInPc1	eskalátor
a	a	k8xC	a
výtahy	výtah	k1gInPc1	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
historické	historický	k2eAgFnSc2d1	historická
ocelové	ocelový	k2eAgFnSc2d1	ocelová
haly	hala	k1gFnSc2	hala
nad	nad	k7c7	nad
nástupišti	nástupiště	k1gNnPc7	nástupiště
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
vynechána	vynechat	k5eAaPmNgFnS	vynechat
<g/>
,	,	kIx,	,
řešeny	řešit	k5eAaImNgFnP	řešit
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
havarijní	havarijní	k2eAgInPc1d1	havarijní
stavy	stav	k1gInPc1	stav
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
nezbytné	nezbytný	k2eAgFnPc1d1	nezbytná
opravy	oprava	k1gFnPc1	oprava
patek	patka	k1gFnPc2	patka
sloupů	sloup	k1gInPc2	sloup
<g/>
,	,	kIx,	,
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
střešních	střešní	k2eAgInPc2d1	střešní
žlabů	žlab	k1gInPc2	žlab
a	a	k8xC	a
výměna	výměna	k1gFnSc1	výměna
poškozených	poškozený	k2eAgNnPc2d1	poškozené
skel	sklo	k1gNnPc2	sklo
světlíků	světlík	k1gInPc2	světlík
<g/>
.	.	kIx.	.
</s>
<s>
Proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
také	také	k9	také
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
kolejiště	kolejiště	k1gNnSc2	kolejiště
a	a	k8xC	a
trakčního	trakční	k2eAgNnSc2d1	trakční
vedení	vedení	k1gNnSc2	vedení
a	a	k8xC	a
přestavba	přestavba	k1gFnSc1	přestavba
severního	severní	k2eAgNnSc2d1	severní
zhlaví	zhlaví	k1gNnSc2	zhlaví
pro	pro	k7c4	pro
zaústění	zaústění	k1gNnSc4	zaústění
estakády	estakáda	k1gFnSc2	estakáda
Nového	Nového	k2eAgNnSc2d1	Nového
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
takzvaného	takzvaný	k2eAgNnSc2d1	takzvané
Nového	Nového	k2eAgNnSc2d1	Nového
spojení	spojení	k1gNnSc2	spojení
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
nové	nový	k2eAgFnPc1d1	nová
dvojkolejné	dvojkolejný	k2eAgFnPc1d1	dvojkolejná
tratě	trať	k1gFnPc1	trať
mezi	mezi	k7c7	mezi
Balabenkou	Balabenka	k1gFnSc7	Balabenka
a	a	k8xC	a
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Vedou	vést	k5eAaImIp3nP	vést
dvěma	dva	k4xCgFnPc7	dva
dvojkolejnými	dvojkolejný	k2eAgFnPc7d1	dvojkolejná
tunelovými	tunelový	k2eAgFnPc7d1	tunelová
troubami	trouba	k1gFnPc7	trouba
délky	délka	k1gFnSc2	délka
2680	[number]	k4	2680
metrů	metr	k1gInPc2	metr
horou	hora	k1gFnSc7	hora
Vítkov	Vítkov	k1gInSc4	Vítkov
(	(	kIx(	(
<g/>
severní	severní	k2eAgInSc4d1	severní
tunel	tunel	k1gInSc4	tunel
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
a	a	k8xC	a
z	z	k7c2	z
Libně	Libeň	k1gFnSc2	Libeň
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc4d1	jižní
tunel	tunel	k1gInSc4	tunel
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
a	a	k8xC	a
z	z	k7c2	z
Holešovic	Holešovice	k1gFnPc2	Holešovice
a	a	k8xC	a
Vysočan	Vysočany	k1gInPc2	Vysočany
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Z	z	k7c2	z
vítkovského	vítkovský	k2eAgInSc2d1	vítkovský
tunelu	tunel	k1gInSc2	tunel
vedou	vést	k5eAaImIp3nP	vést
společně	společně	k6eAd1	společně
k	k	k7c3	k
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
nádraží	nádraží	k1gNnSc3	nádraží
po	po	k7c4	po
438	[number]	k4	438
metrů	metr	k1gInPc2	metr
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
čtyřkolejné	čtyřkolejný	k2eAgFnSc3d1	čtyřkolejná
estakádě	estakáda	k1gFnSc3	estakáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přemosťuje	přemosťovat	k5eAaImIp3nS	přemosťovat
Trocnovskou	Trocnovský	k2eAgFnSc4d1	Trocnovská
a	a	k8xC	a
Husitskou	husitský	k2eAgFnSc4d1	husitská
ulici	ulice	k1gFnSc4	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
tunelů	tunel	k1gInPc2	tunel
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
estakáda	estakáda	k1gFnSc1	estakáda
a	a	k8xC	a
rozplet	rozplést	k5eAaPmDgInS	rozplést
napojující	napojující	k2eAgInPc4d1	napojující
k	k	k7c3	k
Balabence	Balabenka	k1gFnSc3	Balabenka
i	i	k9	i
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
nádraží	nádraží	k1gNnSc4	nádraží
s	s	k7c7	s
Negrelliho	Negrelliha	k1gFnSc5	Negrelliha
viaduktem	viadukt	k1gInSc7	viadukt
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
nástupišť	nástupiště	k1gNnPc2	nástupiště
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
systému	systém	k1gInSc2	systém
pro	pro	k7c4	pro
informování	informování	k1gNnSc4	informování
cestující	cestující	k2eAgFnSc2d1	cestující
veřejnosti	veřejnost	k1gFnSc2	veřejnost
HIS-VOICE	HIS-VOICE	k1gFnSc2	HIS-VOICE
společnosti	společnost	k1gFnSc2	společnost
mikroVOX	mikroVOX	k?	mikroVOX
<g/>
,	,	kIx,	,
za	za	k7c4	za
konkurenční	konkurenční	k2eAgInSc4d1	konkurenční
Integrovaný	integrovaný	k2eAgInSc4d1	integrovaný
informační	informační	k2eAgInSc4d1	informační
systém	systém	k1gInSc4	systém
stanice	stanice	k1gFnSc2	stanice
(	(	kIx(	(
<g/>
INISS	INISS	kA	INISS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
dodavatelem	dodavatel	k1gMnSc7	dodavatel
je	být	k5eAaImIp3nS	být
brněnská	brněnský	k2eAgFnSc1d1	brněnská
firma	firma	k1gFnSc1	firma
CHAPS	CHAPS	kA	CHAPS
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xS	jako
správce	správce	k1gMnSc1	správce
systému	systém	k1gInSc2	systém
IDOS	IDOS	kA	IDOS
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
rozloučil	rozloučit	k5eAaPmAgMnS	rozloučit
hlas	hlas	k1gInSc4	hlas
herce	herec	k1gMnSc2	herec
a	a	k8xC	a
dabéra	dabér	k1gMnSc2	dabér
Václava	Václav	k1gMnSc2	Václav
Knopa	Knop	k1gMnSc2	Knop
a	a	k8xC	a
cestující	cestující	k1gMnPc1	cestující
zde	zde	k6eAd1	zde
poprvé	poprvé	k6eAd1	poprvé
uvítala	uvítat	k5eAaPmAgFnS	uvítat
brněnská	brněnský	k2eAgFnSc1d1	brněnská
herečka	herečka	k1gFnSc1	herečka
Danuše	Danuše	k1gFnSc1	Danuše
Hostinská-Klichová	Hostinská-Klichová	k1gFnSc1	Hostinská-Klichová
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
mezi	mezi	k7c7	mezi
nadšenci	nadšenec	k1gMnPc7	nadšenec
známá	známá	k1gFnSc1	známá
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
"	"	kIx"	"
<g/>
Andula	Andula	k1gFnSc1	Andula
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
potíže	potíž	k1gFnPc1	potíž
se	s	k7c7	s
srozumitelností	srozumitelnost	k1gFnSc7	srozumitelnost
nového	nový	k2eAgNnSc2d1	nové
hlášení	hlášení	k1gNnSc2	hlášení
v	v	k7c6	v
hlučném	hlučný	k2eAgNnSc6d1	hlučné
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
podniknuty	podniknut	k2eAgInPc1d1	podniknut
kroky	krok	k1gInPc1	krok
(	(	kIx(	(
<g/>
např.	např.	kA	např.
maximální	maximální	k2eAgNnSc1d1	maximální
zesílení	zesílení	k1gNnSc1	zesílení
signálu	signál	k1gInSc2	signál
vstupujícího	vstupující	k2eAgInSc2d1	vstupující
do	do	k7c2	do
reproduktorů	reproduktor	k1gMnPc2	reproduktor
<g/>
)	)	kIx)	)
směřující	směřující	k2eAgInSc1d1	směřující
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
informovanosti	informovanost	k1gFnSc2	informovanost
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výměnou	výměna	k1gFnSc7	výměna
hlásícího	hlásící	k2eAgInSc2d1	hlásící
informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
souvisela	souviset	k5eAaImAgFnS	souviset
i	i	k8xC	i
inovace	inovace	k1gFnSc1	inovace
listových	listový	k2eAgNnPc2d1	listové
zobrazovacích	zobrazovací	k2eAgNnPc2d1	zobrazovací
zařízení	zařízení	k1gNnPc2	zařízení
Pragotron	Pragotron	k1gInSc4	Pragotron
a	a	k8xC	a
Solari	Solare	k1gFnSc4	Solare
za	za	k7c4	za
LCD	LCD	kA	LCD
panely	panel	k1gInPc1	panel
od	od	k7c2	od
výrobce	výrobce	k1gMnSc2	výrobce
Elektročas	Elektročas	k1gInSc4	Elektročas
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
cestující	cestující	k1gFnSc4	cestující
byla	být	k5eAaImAgFnS	být
nejviditelnější	viditelný	k2eAgFnSc7d3	nejviditelnější
součástí	součást	k1gFnSc7	součást
modernizace	modernizace	k1gFnSc2	modernizace
odbavovací	odbavovací	k2eAgFnSc2d1	odbavovací
haly	hala	k1gFnSc2	hala
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
léta	léto	k1gNnSc2	léto
2009	[number]	k4	2009
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
uzavírala	uzavírat	k5eAaPmAgFnS	uzavírat
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
nové	nový	k2eAgFnSc2d1	nová
odbavovací	odbavovací	k2eAgFnSc2d1	odbavovací
haly	hala	k1gFnSc2	hala
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
zde	zde	k6eAd1	zde
opět	opět	k6eAd1	opět
obchody	obchod	k1gInPc4	obchod
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnPc4	restaurace
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
komerční	komerční	k2eAgFnPc1d1	komerční
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
předána	předat	k5eAaPmNgFnS	předat
do	do	k7c2	do
užívání	užívání	k1gNnSc2	užívání
veřejnosti	veřejnost	k1gFnSc2	veřejnost
rekonstruovaná	rekonstruovaný	k2eAgFnSc1d1	rekonstruovaná
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
nové	nový	k2eAgFnSc2d1	nová
odbavovací	odbavovací	k2eAgFnSc2d1	odbavovací
haly	hala	k1gFnSc2	hala
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
zde	zde	k6eAd1	zde
nové	nový	k2eAgInPc4d1	nový
obchody	obchod	k1gInPc4	obchod
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
,	,	kIx,	,
prodejny	prodejna	k1gFnPc4	prodejna
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
turistické	turistický	k2eAgFnPc4d1	turistická
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
také	také	k9	také
nové	nový	k2eAgFnPc4d1	nová
velké	velká	k1gFnPc4	velká
odbavovací	odbavovací	k2eAgFnPc4d1	odbavovací
ČD	ČD	kA	ČD
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hale	hala	k1gFnSc6	hala
bylo	být	k5eAaImAgNnS	být
rozmístěno	rozmístit	k5eAaPmNgNnS	rozmístit
několik	několik	k4yIc1	několik
elektronických	elektronický	k2eAgInPc2d1	elektronický
informačních	informační	k2eAgInPc2d1	informační
kiosků	kiosek	k1gInPc2	kiosek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dodala	dodat	k5eAaPmAgFnS	dodat
firma	firma	k1gFnSc1	firma
Starmon	Starmon	k1gInSc1	Starmon
Choceň	Choceň	k1gFnSc1	Choceň
<g/>
,	,	kIx,	,
digitálních	digitální	k2eAgInPc2d1	digitální
panelů	panel	k1gInPc2	panel
informujících	informující	k2eAgInPc2d1	informující
o	o	k7c6	o
řazení	řazení	k1gNnSc6	řazení
a	a	k8xC	a
odjezdech	odjezd	k1gInPc6	odjezd
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
automaty	automat	k1gInPc4	automat
na	na	k7c4	na
jízdenky	jízdenka	k1gFnPc4	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Vybudovány	vybudován	k2eAgFnPc1d1	vybudována
byly	být	k5eAaImAgFnP	být
také	také	k9	také
nové	nový	k2eAgFnPc1d1	nová
toalety	toaleta	k1gFnPc1	toaleta
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
nové	nový	k2eAgFnSc2d1	nová
haly	hala	k1gFnSc2	hala
s	s	k7c7	s
horní	horní	k2eAgFnSc7d1	horní
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
eskalátorů	eskalátor	k1gInPc2	eskalátor
<g/>
,	,	kIx,	,
také	také	k9	také
pozvolné	pozvolný	k2eAgInPc1d1	pozvolný
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
chodníky	chodník	k1gInPc1	chodník
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
zrekonstruovaná	zrekonstruovaný	k2eAgFnSc1d1	zrekonstruovaná
nová	nový	k2eAgFnSc1d1	nová
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
hala	hala	k1gFnSc1	hala
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
Praha	Praha	k1gFnSc1	Praha
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
prezidentů	prezident	k1gMnPc2	prezident
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Giorgia	Giorgius	k1gMnSc2	Giorgius
Napolitana	Napolitan	k1gMnSc2	Napolitan
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
fází	fáze	k1gFnSc7	fáze
obnovy	obnova	k1gFnSc2	obnova
nádraží	nádraží	k1gNnSc4	nádraží
byla	být	k5eAaImAgFnS	být
oprava	oprava	k1gFnSc1	oprava
historické	historický	k2eAgFnSc2d1	historická
a	a	k8xC	a
památkově	památkově	k6eAd1	památkově
chráněné	chráněný	k2eAgFnPc4d1	chráněná
secesní	secesní	k2eAgFnPc4d1	secesní
Fantovy	Fantův	k2eAgFnPc4d1	Fantova
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
znovuotevřena	znovuotevřen	k2eAgFnSc1d1	znovuotevřen
středová	středový	k2eAgFnSc1d1	středová
část	část	k1gFnSc1	část
historické	historický	k2eAgFnSc2d1	historická
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
výzdoba	výzdoba	k1gFnSc1	výzdoba
kopule	kopule	k1gFnSc1	kopule
byla	být	k5eAaImAgFnS	být
pečlivě	pečlivě	k6eAd1	pečlivě
zrestaurována	zrestaurován	k2eAgFnSc1d1	zrestaurována
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
i	i	k9	i
původní	původní	k2eAgFnSc1d1	původní
medová	medový	k2eAgFnSc1d1	medová
barva	barva	k1gFnSc1	barva
omítky	omítka	k1gFnSc2	omítka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
fotografií	fotografia	k1gFnPc2	fotografia
byly	být	k5eAaImAgFnP	být
zhotoveny	zhotoven	k2eAgFnPc1d1	zhotovena
repliky	replika	k1gFnPc1	replika
podlahy	podlaha	k1gFnSc2	podlaha
<g/>
,	,	kIx,	,
vitráží	vitráž	k1gFnPc2	vitráž
<g/>
,	,	kIx,	,
kování	kování	k1gNnPc2	kování
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
řešení	řešení	k1gNnSc1	řešení
osvětlení	osvětlení	k1gNnSc2	osvětlení
je	být	k5eAaImIp3nS	být
nové	nový	k2eAgNnSc1d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Kavárenské	kavárenský	k2eAgFnPc1d1	kavárenská
činnosti	činnost	k1gFnPc1	činnost
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
ujal	ujmout	k5eAaPmAgInS	ujmout
indický	indický	k2eAgInSc1d1	indický
řetězec	řetězec	k1gInSc1	řetězec
Café	café	k1gNnSc2	café
Coffee	Coffe	k1gInSc2	Coffe
Day	Day	k1gFnSc2	Day
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středové	středový	k2eAgFnSc6d1	středová
části	část	k1gFnSc6	část
budovy	budova	k1gFnSc2	budova
bylo	být	k5eAaImAgNnS	být
opravena	opravit	k5eAaPmNgFnS	opravit
též	též	k9	též
střecha	střecha	k1gFnSc1	střecha
a	a	k8xC	a
čelní	čelní	k2eAgFnSc1d1	čelní
fasáda	fasáda	k1gFnSc1	fasáda
včetně	včetně	k7c2	včetně
štukové	štukový	k2eAgFnSc2d1	štuková
a	a	k8xC	a
sochařské	sochařský	k2eAgFnSc2d1	sochařská
výzdoby	výzdoba	k1gFnSc2	výzdoba
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
pískovcových	pískovcový	k2eAgFnPc2d1	pískovcová
věží	věž	k1gFnPc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Zbývá	zbývat	k5eAaImIp3nS	zbývat
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
obě	dva	k4xCgNnPc4	dva
boční	boční	k2eAgNnPc4d1	boční
křídla	křídlo	k1gNnPc4	křídlo
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
obnovu	obnova	k1gFnSc4	obnova
zoufale	zoufale	k6eAd1	zoufale
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
venkovní	venkovní	k2eAgFnPc1d1	venkovní
fasády	fasáda	k1gFnPc1	fasáda
i	i	k8xC	i
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
prostory	prostora	k1gFnPc1	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
rokem	rok	k1gInSc7	rok
2014	[number]	k4	2014
až	až	k6eAd1	až
zařím	zařet	k5eAaImIp1nS	zařet
2017	[number]	k4	2017
probíhala	probíhat	k5eAaImAgFnS	probíhat
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
historické	historický	k2eAgFnSc2d1	historická
ocelové	ocelový	k2eAgFnSc2d1	ocelová
dvojlodní	dvojlodní	k2eAgFnSc2d1	dvojlodní
haly	hala	k1gFnSc2	hala
nad	nad	k7c7	nad
nástupišti	nástupiště	k1gNnPc7	nástupiště
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
etap	etapa	k1gFnPc2	etapa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
minimalizovala	minimalizovat	k5eAaBmAgFnS	minimalizovat
množství	množství	k1gNnSc4	množství
vyloučených	vyloučený	k2eAgFnPc2d1	vyloučená
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
nástupišť	nástupiště	k1gNnPc2	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
opravu	oprava	k1gFnSc4	oprava
korozních	korozní	k2eAgFnPc2d1	korozní
a	a	k8xC	a
statických	statický	k2eAgFnPc2d1	statická
narušení	narušení	k1gNnSc4	narušení
nosné	nosný	k2eAgFnSc2d1	nosná
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
výměnu	výměna	k1gFnSc4	výměna
poškozených	poškozený	k2eAgInPc2d1	poškozený
dílů	díl	k1gInPc2	díl
za	za	k7c4	za
repliky	replika	k1gFnPc4	replika
<g/>
,	,	kIx,	,
repasi	repase	k1gFnSc4	repase
a	a	k8xC	a
přesklení	přesklení	k1gNnSc4	přesklení
všech	všecek	k3xTgInPc2	všecek
světlíků	světlík	k1gInPc2	světlík
a	a	k8xC	a
výměnu	výměna	k1gFnSc4	výměna
střešní	střešní	k2eAgFnSc2d1	střešní
krytiny	krytina	k1gFnSc2	krytina
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
byla	být	k5eAaImAgFnS	být
vyčištěna	vyčistit	k5eAaPmNgFnS	vyčistit
od	od	k7c2	od
starých	starý	k2eAgInPc2d1	starý
nátěrů	nátěr	k1gInPc2	nátěr
a	a	k8xC	a
opatřena	opatřen	k2eAgNnPc4d1	opatřeno
třívrstvým	třívrstvý	k2eAgInSc7d1	třívrstvý
antikorozním	antikorozní	k2eAgInSc7d1	antikorozní
nátěrem	nátěr	k1gInSc7	nátěr
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
částí	část	k1gFnPc2	část
konstrukce	konstrukce	k1gFnSc2	konstrukce
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
kvůli	kvůli	k7c3	kvůli
současným	současný	k2eAgFnPc3d1	současná
normám	norma	k1gFnPc3	norma
statické	statický	k2eAgFnSc2d1	statická
únostnosti	únostnost	k1gFnSc2	únostnost
zcela	zcela	k6eAd1	zcela
vyměnit	vyměnit	k5eAaPmF	vyměnit
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vaznice	vaznice	k1gFnSc1	vaznice
pod	pod	k7c4	pod
světlíky	světlík	k1gInPc4	světlík
a	a	k8xC	a
díly	díl	k1gInPc4	díl
poškozené	poškozený	k2eAgFnSc2d1	poškozená
korozí	koroze	k1gFnSc7	koroze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
maximálně	maximálně	k6eAd1	maximálně
respektovala	respektovat	k5eAaImAgFnS	respektovat
historickou	historický	k2eAgFnSc4d1	historická
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
učiněny	učiněn	k2eAgInPc4d1	učiněn
kompromisy	kompromis	k1gInPc4	kompromis
–	–	k?	–
původní	původní	k2eAgNnSc1d1	původní
zastřešení	zastřešení	k1gNnSc1	zastřešení
vlnitými	vlnitý	k2eAgInPc7d1	vlnitý
plechy	plech	k1gInPc7	plech
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
trapézovými	trapézový	k2eAgFnPc7d1	trapézová
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vlnité	vlnitý	k2eAgInPc4d1	vlnitý
plechy	plech	k1gInPc4	plech
v	v	k7c6	v
požadovaném	požadovaný	k2eAgNnSc6d1	požadované
provedení	provedení	k1gNnSc6	provedení
již	již	k9	již
nebyly	být	k5eNaImAgFnP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
díly	díl	k1gInPc1	díl
už	už	k6eAd1	už
nebyly	být	k5eNaImAgInP	být
nýtovány	nýtován	k2eAgInPc1d1	nýtován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sešroubovány	sešroubovat	k5eAaPmNgInP	sešroubovat
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
vyrobenými	vyrobený	k2eAgInPc7d1	vyrobený
nýtovými	nýtový	k2eAgInPc7d1	nýtový
šrouby	šroub	k1gInPc7	šroub
a	a	k8xC	a
uzavřenými	uzavřený	k2eAgFnPc7d1	uzavřená
maticemi	matice	k1gFnPc7	matice
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
co	co	k3yInSc4	co
nejlépe	dobře	k6eAd3	dobře
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
původnímu	původní	k2eAgInSc3d1	původní
vzhledu	vzhled	k1gInSc3	vzhled
a	a	k8xC	a
pozici	pozice	k1gFnSc3	pozice
nýtů	nýt	k1gInPc2	nýt
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
kladla	klást	k5eAaImAgFnS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
světelných	světelný	k2eAgFnPc2d1	světelná
podmínek	podmínka	k1gFnPc2	podmínka
uvnitř	uvnitř	k7c2	uvnitř
haly	hala	k1gFnSc2	hala
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
historické	historický	k2eAgFnSc2d1	historická
měla	mít	k5eAaImAgNnP	mít
firma	firma	k1gFnSc1	firma
Grandi	grand	k1gMnPc1	grand
Stazioni	Stazion	k1gMnPc1	Stazion
získat	získat	k5eAaPmF	získat
nádraží	nádraží	k1gNnSc4	nádraží
od	od	k7c2	od
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
na	na	k7c4	na
30	[number]	k4	30
let	léto	k1gNnPc2	léto
do	do	k7c2	do
pronájmu	pronájem	k1gInSc2	pronájem
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
a	a	k8xC	a
Grandi	grand	k1gMnPc1	grand
Stazioni	Stazion	k1gMnPc1	Stazion
tři	tři	k4xCgFnPc4	tři
smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c6	o
revitalizaci	revitalizace	k1gFnSc6	revitalizace
<g/>
,	,	kIx,	,
provozu	provoz	k1gInSc6	provoz
a	a	k8xC	a
správě	správa	k1gFnSc6	správa
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
také	také	k9	také
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
a	a	k8xC	a
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
italskou	italský	k2eAgFnSc7d1	italská
firmou	firma	k1gFnSc7	firma
Grandi	grand	k1gMnPc1	grand
Stazioni	Stazioň	k1gFnSc6	Stazioň
byla	být	k5eAaImAgFnS	být
ukončena	ukončen	k2eAgFnSc1d1	ukončena
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
po	po	k7c6	po
vyprchání	vyprchání	k1gNnSc6	vyprchání
základní	základní	k2eAgFnSc2d1	základní
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
Správa	správa	k1gFnSc1	správa
železniční	železniční	k2eAgFnSc2d1	železniční
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
firmě	firma	k1gFnSc3	firma
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prostory	prostora	k1gFnPc4	prostora
za	za	k7c4	za
13	[number]	k4	13
let	léto	k1gNnPc2	léto
kompletně	kompletně	k6eAd1	kompletně
zrekonstruovat	zrekonstruovat	k5eAaPmF	zrekonstruovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
po	po	k7c4	po
varování	varování	k1gNnSc4	varování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Italům	Ital	k1gMnPc3	Ital
byla	být	k5eAaImAgFnS	být
tedy	tedy	k8xC	tedy
smlouva	smlouva	k1gFnSc1	smlouva
vypovězena	vypovědět	k5eAaPmNgFnS	vypovědět
a	a	k8xC	a
budovu	budova	k1gFnSc4	budova
nádraží	nádraží	k1gNnSc2	nádraží
nyní	nyní	k6eAd1	nyní
spravuje	spravovat	k5eAaImIp3nS	spravovat
SŽDC	SŽDC	kA	SŽDC
<g/>
.	.	kIx.	.
</s>
<s>
Grandi	grand	k1gMnPc1	grand
Stazioni	Stazioň	k1gFnSc6	Stazioň
chce	chtít	k5eAaImIp3nS	chtít
od	od	k7c2	od
bývalého	bývalý	k2eAgMnSc2d1	bývalý
vlastníka	vlastník	k1gMnSc2	vlastník
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
vymáhat	vymáhat	k5eAaImF	vymáhat
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
za	za	k7c2	za
škody	škoda	k1gFnSc2	škoda
způsobené	způsobený	k2eAgFnSc2d1	způsobená
špatným	špatný	k2eAgNnSc7d1	špatné
jednáním	jednání	k1gNnSc7	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
i	i	k9	i
stamilionové	stamilionový	k2eAgNnSc1d1	stamilionové
vypořádání	vypořádání	k1gNnSc1	vypořádání
se	s	k7c7	s
SŽDC	SŽDC	kA	SŽDC
za	za	k7c2	za
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
již	již	k6eAd1	již
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tvoří	tvořit	k5eAaImIp3nP	tvořit
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
několik	několik	k4yIc4	několik
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
podzemní	podzemní	k2eAgFnSc1d1	podzemní
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
hala	hala	k1gFnSc1	hala
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
haly	hala	k1gFnSc2	hala
s	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
x	x	k?	x
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
přístupy	přístup	k1gInPc1	přístup
z	z	k7c2	z
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
rozdělená	rozdělená	k1gFnSc1	rozdělená
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
patra	patro	k1gNnPc4	patro
<g/>
,	,	kIx,	,
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obchody	obchod	k1gInPc7	obchod
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnPc4	restaurace
a	a	k8xC	a
rychlé	rychlý	k2eAgNnSc4d1	rychlé
občerstvení	občerstvení	k1gNnSc4	občerstvení
<g/>
,	,	kIx,	,
pokladní	pokladní	k1gFnSc4	pokladní
přepážky	přepážka	k1gFnSc2	přepážka
pro	pro	k7c4	pro
nákupy	nákup	k1gInPc4	nákup
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
místa	místo	k1gNnPc1	místo
na	na	k7c4	na
sezení	sezení	k1gNnPc4	sezení
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
jako	jako	k8xS	jako
sprchy	sprcha	k1gFnPc4	sprcha
<g/>
,	,	kIx,	,
toalety	toaleta	k1gFnPc4	toaleta
<g/>
,	,	kIx,	,
směnárny	směnárna	k1gFnPc4	směnárna
<g/>
,	,	kIx,	,
bankomaty	bankomat	k1gInPc4	bankomat
<g/>
,	,	kIx,	,
úschovny	úschovna	k1gFnPc4	úschovna
kol	kolo	k1gNnPc2	kolo
a	a	k8xC	a
zavazadel	zavazadlo	k1gNnPc2	zavazadlo
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
tzv.	tzv.	kA	tzv.
ČD	ČD	kA	ČD
Centrum	centrum	k1gNnSc1	centrum
které	který	k3yRgFnPc4	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
infocentrum	infocentrum	k1gNnSc1	infocentrum
a	a	k8xC	a
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
zakoupit	zakoupit	k5eAaPmF	zakoupit
lístky	lístek	k1gInPc4	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
cestující	cestující	k1gFnSc4	cestující
vyšších	vysoký	k2eAgFnPc2d2	vyšší
tříd	třída	k1gFnPc2	třída
a	a	k8xC	a
vlaků	vlak	k1gInPc2	vlak
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
ČD	ČD	kA	ČD
(	(	kIx(	(
<g/>
např.	např.	kA	např.
soupravy	souprava	k1gFnSc2	souprava
SuperCity	SuperCita	k1gFnSc2	SuperCita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
salónek	salónek	k1gInSc1	salónek
a	a	k8xC	a
čekárna	čekárna	k1gFnSc1	čekárna
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
ČD	ČD	kA	ČD
Lounge	Lounge	k1gFnSc1	Lounge
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
Fantova	Fantův	k2eAgFnSc1d1	Fantova
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
cestující	cestující	k1gMnPc1	cestující
odbavovali	odbavovat	k5eAaImAgMnP	odbavovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
kavárna	kavárna	k1gFnSc1	kavárna
a	a	k8xC	a
místa	místo	k1gNnSc2	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vlaků	vlak	k1gInPc2	vlak
se	se	k3xPyFc4	se
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
přes	přes	k7c4	přes
8	[number]	k4	8
nástupišť	nástupiště	k1gNnPc2	nástupiště
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
cestující	cestující	k1gMnPc1	cestující
dostanou	dostat	k5eAaPmIp3nP	dostat
přes	přes	k7c4	přes
3	[number]	k4	3
podchody	podchod	k1gInPc1	podchod
vedoucí	vedoucí	k2eAgInPc1d1	vedoucí
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
patra	patro	k1gNnSc2	patro
podzemní	podzemní	k2eAgFnSc2d1	podzemní
odbavovací	odbavovací	k2eAgFnSc2d1	odbavovací
haly	hala	k1gFnSc2	hala
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
podchody	podchod	k1gInPc4	podchod
jižní	jižní	k2eAgInSc1d1	jižní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgFnSc1d1	hlavní
<g/>
)	)	kIx)	)
a	a	k8xC	a
severní	severní	k2eAgFnPc1d1	severní
<g/>
.	.	kIx.	.
</s>
<s>
Nástupních	nástupní	k2eAgFnPc2d1	nástupní
hran	hrana	k1gFnPc2	hrana
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
celkem	celkem	k6eAd1	celkem
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
vede	vést	k5eAaImIp3nS	vést
linka	linka	k1gFnSc1	linka
C	C	kA	C
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zastávku	zastávka	k1gFnSc4	zastávka
"	"	kIx"	"
<g/>
Hlavní	hlavní	k2eAgNnPc4d1	hlavní
nádraží	nádraží	k1gNnPc4	nádraží
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
odbavovací	odbavovací	k2eAgFnSc6d1	odbavovací
hale	hala	k1gFnSc6	hala
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaje	tramvaj	k1gFnPc1	tramvaj
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
ze	z	k7c2	z
zastávky	zastávka	k1gFnSc2	zastávka
"	"	kIx"	"
<g/>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
východu	východ	k1gInSc2	východ
z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
odsud	odsud	k6eAd1	odsud
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
15	[number]	k4	15
až	až	k9	až
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
linka	linka	k1gFnSc1	linka
Airport	Airport	k1gInSc1	Airport
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
AE	AE	kA	AE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
je	být	k5eAaImIp3nS	být
také	také	k9	také
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
čarou	čára	k1gFnSc7	čára
a	a	k8xC	a
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
chůze	chůze	k1gFnSc2	chůze
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
novou	nový	k2eAgFnSc7d1	nová
budovou	budova	k1gFnSc7	budova
nádraží	nádraží	k1gNnSc2	nádraží
u	u	k7c2	u
Severojižní	severojižní	k2eAgFnSc2d1	severojižní
magistrály	magistrála	k1gFnSc2	magistrála
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
autobusová	autobusový	k2eAgFnSc1d1	autobusová
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
různé	různý	k2eAgInPc4d1	různý
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
i	i	k8xC	i
vnitrostátní	vnitrostátní	k2eAgInPc4d1	vnitrostátní
spoje	spoj	k1gInPc4	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
placená	placený	k2eAgNnPc1d1	placené
parkoviště	parkoviště	k1gNnPc1	parkoviště
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
stojí	stát	k5eAaImIp3nP	stát
taxi	taxi	k1gNnSc4	taxi
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
před	před	k7c7	před
nádražím	nádraží	k1gNnSc7	nádraží
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Vrchlického	Vrchlického	k2eAgFnSc2d1	Vrchlického
sady	sada	k1gFnSc2	sada
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
zvaném	zvaný	k2eAgInSc6d1	zvaný
Sherwood	Sherwood	k1gInSc1	Sherwood
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
bezdomovci	bezdomovec	k1gMnPc1	bezdomovec
a	a	k8xC	a
narkomani	narkoman	k1gMnPc1	narkoman
a	a	k8xC	a
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
také	také	k9	také
nechvalně	chvalně	k6eNd1	chvalně
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xC	jako
křižovatka	křižovatka	k1gFnSc1	křižovatka
mužské	mužský	k2eAgFnSc2d1	mužská
homosexuální	homosexuální	k2eAgFnSc2d1	homosexuální
prostituce	prostituce	k1gFnSc2	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
patří	patřit	k5eAaImIp3nS	patřit
pod	pod	k7c4	pod
Prahu	Praha	k1gFnSc4	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
měla	mít	k5eAaImAgFnS	mít
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
parku	park	k1gInSc2	park
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
nebylo	být	k5eNaImAgNnS	být
bezpečno	bezpečen	k2eAgNnSc1d1	bezpečno
ani	ani	k8xC	ani
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
samotné	samotný	k2eAgFnSc2d1	samotná
odbavovací	odbavovací	k2eAgFnSc2d1	odbavovací
haly	hala	k1gFnSc2	hala
<g/>
,	,	kIx,	,
po	po	k7c6	po
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
dokončené	dokončený	k2eAgFnSc6d1	dokončená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
ale	ale	k9	ale
stav	stav	k1gInSc1	stav
výrazně	výrazně	k6eAd1	výrazně
zlepšil	zlepšit	k5eAaPmAgInS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
provedenou	provedený	k2eAgFnSc4d1	provedená
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
je	být	k5eAaImIp3nS	být
nádraží	nádraží	k1gNnSc1	nádraží
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
pro	pro	k7c4	pro
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yIgFnSc3	který
nemůže	moct	k5eNaImIp3nS	moct
pojmout	pojmout	k5eAaPmF	pojmout
některé	některý	k3yIgFnPc4	některý
příměstské	příměstský	k2eAgFnPc4d1	příměstská
linky	linka	k1gFnPc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pěší	pěší	k2eAgNnSc4d1	pěší
propojení	propojení	k1gNnSc4	propojení
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
s	s	k7c7	s
Masarykovým	Masarykův	k2eAgNnSc7d1	Masarykovo
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
nejrušnější	rušný	k2eAgNnSc1d3	nejrušnější
nádraží	nádraží	k1gNnSc1	nádraží
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
buď	buď	k8xC	buď
o	o	k7c4	o
nadchod	nadchod	k1gInSc4	nadchod
nebo	nebo	k8xC	nebo
podzemní	podzemní	k2eAgInSc1d1	podzemní
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
chodník	chodník	k1gInSc1	chodník
(	(	kIx(	(
<g/>
travelátor	travelátor	k1gInSc1	travelátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
rychlodráhy	rychlodráha	k1gFnSc2	rychlodráha
z	z	k7c2	z
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
nádraží	nádraží	k1gNnSc4	nádraží
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
s	s	k7c7	s
pokračováním	pokračování	k1gNnSc7	pokračování
do	do	k7c2	do
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Částka	částka	k1gFnSc1	částka
za	za	k7c4	za
realizaci	realizace	k1gFnSc4	realizace
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
blížit	blížit	k5eAaImF	blížit
400	[number]	k4	400
milionům	milion	k4xCgInPc3	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
má	mít	k5eAaImIp3nS	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
také	také	k9	také
pěší	pěší	k2eAgInSc4d1	pěší
podchod	podchod	k1gInSc4	podchod
do	do	k7c2	do
čtvrtí	čtvrt	k1gFnPc2	čtvrt
Žižkov	Žižkov	k1gInSc1	Žižkov
a	a	k8xC	a
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lidi	člověk	k1gMnPc4	člověk
odsud	odsud	k6eAd1	odsud
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
museli	muset	k5eAaImAgMnP	muset
celé	celý	k2eAgNnSc4d1	celé
nádraží	nádraží	k1gNnSc4	nádraží
obcházet	obcházet	k5eAaImF	obcházet
či	či	k8xC	či
ilegálně	ilegálně	k6eAd1	ilegálně
přecházet	přecházet	k5eAaImF	přecházet
koleje	kolej	k1gFnPc4	kolej
<g/>
.	.	kIx.	.
</s>
<s>
SŽDC	SŽDC	kA	SŽDC
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
projektu	projekt	k1gInSc6	projekt
konkrétně	konkrétně	k6eAd1	konkrétně
začala	začít	k5eAaPmAgFnS	začít
jednat	jednat	k5eAaImF	jednat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
na	na	k7c4	na
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
vedou	vést	k5eAaImIp3nP	vést
tři	tři	k4xCgInPc4	tři
podchody	podchod	k1gInPc4	podchod
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
Žižkov	Žižkov	k1gInSc4	Žižkov
v	v	k7c4	v
Vinohrady	Vinohrady	k1gInPc4	Vinohrady
bude	být	k5eAaImBp3nS	být
prodloužen	prodloužen	k2eAgInSc1d1	prodloužen
ten	ten	k3xDgInSc1	ten
nejsevernější	severní	k2eAgInSc1d3	nejsevernější
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
odsud	odsud	k6eAd1	odsud
přímé	přímý	k2eAgFnPc1d1	přímá
linky	linka	k1gFnPc1	linka
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
trasy	trasa	k1gFnSc2	trasa
podniká	podnikat	k5eAaImIp3nS	podnikat
dopravce	dopravce	k1gMnSc1	dopravce
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnSc2	společnost
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
a	a	k8xC	a
LEO	Leo	k1gMnSc1	Leo
Express	express	k1gInSc4	express
jezdí	jezdit	k5eAaImIp3nS	jezdit
z	z	k7c2	z
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
tras	trasa	k1gFnPc2	trasa
jen	jen	k9	jen
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Slovensko	Slovensko	k1gNnSc1	Slovensko
Slovensko	Slovensko	k1gNnSc1	Slovensko
–	–	k?	–
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Poprad	Poprad	k1gInSc1	Poprad
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc1	Košice
Rakousko	Rakousko	k1gNnSc1	Rakousko
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
(	(	kIx(	(
<g/>
Graz	Graz	k1gInSc1	Graz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Linec	Linec	k1gInSc1	Linec
(	(	kIx(	(
<g/>
Linz	Linz	k1gInSc1	Linz
<g/>
)	)	kIx)	)
Německo	Německo	k1gNnSc1	Německo
Německo	Německo	k1gNnSc4	Německo
–	–	k?	–
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Řezno	Řezno	k1gNnSc1	Řezno
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Regensburg	Regensburg	k1gInSc1	Regensburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Drážďány	Drážďán	k1gInPc1	Drážďán
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
Hamburk	Hamburk	k1gInSc1	Hamburk
Polsko	Polsko	k1gNnSc1	Polsko
Polsko	Polsko	k1gNnSc1	Polsko
–	–	k?	–
Katovice	Katovice	k1gFnPc1	Katovice
<g/>
,	,	kIx,	,
Krakov	Krakov	k1gInSc1	Krakov
<g/>
,	,	kIx,	,
Varšava	Varšava	k1gFnSc1	Varšava
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
–	–	k?	–
Budapešť	Budapešť	k1gFnSc1	Budapešť
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
stanice	stanice	k1gFnSc2	stanice
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vnitrostátních	vnitrostátní	k2eAgFnPc2d1	vnitrostátní
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jezdí	jezdit	k5eAaImIp3nP	jezdit
do	do	k7c2	do
velkých	velký	k2eAgNnPc2d1	velké
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
včetně	včetně	k7c2	včetně
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
,	,	kIx,	,
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Olomouce	Olomouc	k1gFnSc2	Olomouc
či	či	k8xC	či
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
také	také	k9	také
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitrostátní	vnitrostátní	k2eAgFnSc6d1	vnitrostátní
dopravě	doprava	k1gFnSc6	doprava
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
podnikají	podnikat	k5eAaImIp3nP	podnikat
tři	tři	k4xCgMnPc1	tři
největší	veliký	k2eAgMnPc1d3	veliký
dopravci	dopravce	k1gMnPc1	dopravce
–	–	k?	–
národní	národní	k2eAgMnSc1d1	národní
dopravce	dopravce	k1gMnSc1	dopravce
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
RegioJet	RegioJet	k1gMnSc1	RegioJet
a	a	k8xC	a
LEO	Leo	k1gMnSc1	Leo
Express	express	k1gInSc1	express
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
mají	mít	k5eAaImIp3nP	mít
síť	síť	k1gFnSc4	síť
destinací	destinace	k1gFnPc2	destinace
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nejhustější	hustý	k2eAgMnSc1d3	nejhustší
<g/>
.	.	kIx.	.
</s>
<s>
Vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
odsud	odsud	k6eAd1	odsud
také	také	k9	také
regionální	regionální	k2eAgFnPc4d1	regionální
příměstské	příměstský	k2eAgFnPc4d1	příměstská
linky	linka	k1gFnPc4	linka
Esko	eska	k1gFnSc5	eska
Praha	Praha	k1gFnSc1	Praha
provozované	provozovaný	k2eAgInPc1d1	provozovaný
Českými	český	k2eAgFnPc7d1	Česká
drahami	draha	k1gFnPc7	draha
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
části	část	k1gFnSc2	část
začleněné	začleněný	k2eAgFnSc2d1	začleněná
do	do	k7c2	do
Pražské	pražský	k2eAgFnSc2d1	Pražská
integrované	integrovaný	k2eAgFnSc2d1	integrovaná
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
PID	PID	kA	PID
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
frekvencemi	frekvence	k1gFnPc7	frekvence
a	a	k8xC	a
velkokapacitními	velkokapacitní	k2eAgInPc7d1	velkokapacitní
vlaky	vlak	k1gInPc7	vlak
například	například	k6eAd1	například
CityElefant	CityElefant	k1gInSc4	CityElefant
(	(	kIx(	(
<g/>
el.	el.	k?	el.
jednotka	jednotka	k1gFnSc1	jednotka
471	[number]	k4	471
<g/>
)	)	kIx)	)
odsud	odsud	k6eAd1	odsud
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
linky	linka	k1gFnPc4	linka
značené	značený	k2eAgFnSc2d1	značená
S1	S1	k1gFnSc2	S1
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
či	či	k8xC	či
R1	R1	k1gFnSc1	R1
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Jezdí	jezdit	k5eAaImIp3nS	jezdit
do	do	k7c2	do
měst	město	k1gNnPc2	město
blíže	blízce	k6eAd2	blízce
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
Beroun	Beroun	k1gInSc4	Beroun
<g/>
,	,	kIx,	,
Benešov	Benešov	k1gInSc1	Benešov
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Kralupy	Kralupy	k1gInPc1	Kralupy
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
<g/>
.	.	kIx.	.
</s>
