<s>
Nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
byl	být	k5eAaImAgInS	být
zahájen	zahájen	k2eAgInSc1d1	zahájen
železniční	železniční	k2eAgInSc1d1	železniční
provoz	provoz	k1gInSc1	provoz
Dráhy	dráha	k1gFnSc2	dráha
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Benešova	Benešov	k1gInSc2	Benešov
<g/>
,	,	kIx,	,
Tábora	Tábor	k1gInSc2	Tábor
a	a	k8xC	a
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
