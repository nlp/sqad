<s>
Jako	jako	k9	jako
pivo	pivo	k1gNnSc1	pivo
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Pilsener	Pilsener	k1gInSc1	Pilsener
nebo	nebo	k8xC	nebo
Pils	Pils	k1gInSc1	Pils
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
světlé	světlý	k2eAgNnSc1d1	světlé
spodně	spodně	k6eAd1	spodně
kvašené	kvašený	k2eAgNnSc1d1	kvašené
pivo	pivo	k1gNnSc1	pivo
vařené	vařený	k2eAgNnSc1d1	vařené
dle	dle	k7c2	dle
plzeňské	plzeňský	k2eAgFnSc2d1	Plzeňská
receptury	receptura	k1gFnSc2	receptura
<g/>
.	.	kIx.	.
</s>
