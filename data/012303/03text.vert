<p>
<s>
Jako	jako	k9	jako
pivo	pivo	k1gNnSc1	pivo
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Pilsener	Pilsener	k1gInSc1	Pilsener
nebo	nebo	k8xC	nebo
Pils	Pils	k1gInSc1	Pils
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
světlé	světlý	k2eAgNnSc1d1	světlé
spodně	spodně	k6eAd1	spodně
kvašené	kvašený	k2eAgNnSc1d1	kvašené
pivo	pivo	k1gNnSc1	pivo
vařené	vařený	k2eAgNnSc1d1	vařené
dle	dle	k7c2	dle
plzeňské	plzeňský	k2eAgFnSc2d1	Plzeňská
receptury	receptura	k1gFnSc2	receptura
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
ležáků	ležák	k1gInPc2	ležák
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
především	především	k9	především
výraznější	výrazný	k2eAgInSc1d2	výraznější
chmelovou	chmelový	k2eAgFnSc7d1	chmelová
chutí	chuť	k1gFnSc7	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
pivovaru	pivovar	k1gInSc2	pivovar
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
Měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
pivovar	pivovar	k1gInSc1	pivovar
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
mělo	mít	k5eAaImAgNnS	mít
vařit	vařit	k5eAaImF	vařit
pivo	pivo	k1gNnSc1	pivo
bavorského	bavorský	k2eAgInSc2d1	bavorský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
sládek	sládek	k1gMnSc1	sládek
najat	najat	k2eAgMnSc1d1	najat
vyhlášený	vyhlášený	k2eAgMnSc1d1	vyhlášený
bavorský	bavorský	k2eAgMnSc1d1	bavorský
odborník	odborník	k1gMnSc1	odborník
Josef	Josef	k1gMnSc1	Josef
Groll	Groll	k1gMnSc1	Groll
z	z	k7c2	z
Vilshofenu	Vilshofen	k1gInSc2	Vilshofen
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
–	–	k?	–
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ovšem	ovšem	k9	ovšem
nepočítal	počítat	k5eNaImAgMnS	počítat
se	se	k3xPyFc4	se
specifiky	specifika	k1gFnSc2	specifika
místních	místní	k2eAgFnPc2d1	místní
surovin	surovina	k1gFnPc2	surovina
(	(	kIx(	(
<g/>
měkká	měkký	k2eAgFnSc1d1	měkká
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
žatecký	žatecký	k2eAgInSc1d1	žatecký
chmel	chmel	k1gInSc1	chmel
<g/>
,	,	kIx,	,
světlejší	světlý	k2eAgInSc1d2	světlejší
slad	slad	k1gInSc1	slad
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
anglickou	anglický	k2eAgFnSc7d1	anglická
technologií	technologie	k1gFnSc7	technologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jako	jako	k9	jako
první	první	k4xOgFnSc4	první
várku	várka	k1gFnSc4	várka
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1842	[number]	k4	1842
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
pivo	pivo	k1gNnSc4	pivo
zcela	zcela	k6eAd1	zcela
originální	originální	k2eAgFnPc1d1	originální
<g/>
,	,	kIx,	,
jasné	jasný	k2eAgFnPc1d1	jasná
a	a	k8xC	a
zlatavé	zlatavý	k2eAgFnPc1d1	zlatavá
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Sklidil	sklidit	k5eAaPmAgMnS	sklidit
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
značný	značný	k2eAgInSc1d1	značný
úspěch	úspěch	k1gInSc1	úspěch
a	a	k8xC	a
receptura	receptura	k1gFnSc1	receptura
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
ujala	ujmout	k5eAaPmAgFnS	ujmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Industrializace	industrializace	k1gFnSc1	industrializace
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgFnPc1d1	související
rozvoj	rozvoj	k1gInSc4	rozvoj
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
významně	významně	k6eAd1	významně
napomohl	napomoct	k5eAaPmAgMnS	napomoct
rozšíření	rozšíření	k1gNnSc4	rozšíření
plzeňského	plzeňský	k2eAgNnSc2d1	plzeňské
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
se	se	k3xPyFc4	se
točilo	točit	k5eAaImAgNnS	točit
již	již	k6eAd1	již
v	v	k7c6	v
35	[number]	k4	35
pražských	pražský	k2eAgInPc6d1	pražský
hostincích	hostinec	k1gInPc6	hostinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
napít	napít	k5eAaBmF	napít
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
až	až	k6eAd1	až
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgInSc1d1	obchodní
úspěch	úspěch	k1gInSc1	úspěch
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
Pilsner	Pilsner	k1gMnSc1	Pilsner
Bier	Bier	k1gMnSc1	Bier
(	(	kIx(	(
<g/>
Plzeňské	plzeňský	k2eAgNnSc1d1	plzeňské
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
registrované	registrovaný	k2eAgFnSc2d1	registrovaná
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1859	[number]	k4	1859
u	u	k7c2	u
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
a	a	k8xC	a
živnostenské	živnostenský	k2eAgFnSc2d1	Živnostenská
komory	komora	k1gFnSc2	komora
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
Prazdroj	prazdroj	k1gInSc1	prazdroj
-	-	kIx~	-
Urquell	Urquell	k1gInSc1	Urquell
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Pilsner	Pilsner	k1gMnSc1	Pilsner
Urquell	Urquell	k1gMnSc1	Urquell
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
však	však	k9	však
nezabránila	zabránit	k5eNaPmAgFnS	zabránit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
jiní	jiný	k2eAgMnPc1d1	jiný
nesnažili	snažit	k5eNaImAgMnP	snažit
zneužít	zneužít	k5eAaPmF	zneužít
věhlasu	věhlas	k1gInSc3	věhlas
plzeňského	plzeňský	k2eAgNnSc2d1	plzeňské
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Precedentní	precedentní	k2eAgInSc1d1	precedentní
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc1	případ
sládka	sládek	k1gMnSc2	sládek
Haase	Haas	k1gMnSc2	Haas
z	z	k7c2	z
Reibachu	Reibach	k1gInSc2	Reibach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prodával	prodávat	k5eAaImAgInS	prodávat
ve	v	k7c6	v
švýcarsku	švýcarsek	k1gInSc6	švýcarsek
své	svůj	k3xOyFgNnSc4	svůj
pivo	pivo	k1gNnSc4	pivo
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Haas-Pilsner-Bier	Haas-Pilsner-Bira	k1gFnPc2	Haas-Pilsner-Bira
<g/>
.	.	kIx.	.
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
pivovar	pivovar	k1gInSc1	pivovar
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
žaloval	žalovat	k5eAaImAgInS	žalovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
curyšský	curyšský	k2eAgInSc1d1	curyšský
soud	soud	k1gInSc1	soud
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Haase	Haase	k1gFnSc2	Haase
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pilsner	Pilsner	k1gInSc1	Pilsner
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
druh	druh	k1gInSc1	druh
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
bitburským	bitburský	k2eAgInSc7d1	bitburský
pivovarem	pivovar	k1gInSc7	pivovar
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
soudem	soud	k1gInSc7	soud
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
ale	ale	k9	ale
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
jasném	jasný	k2eAgNnSc6d1	jasné
označení	označení	k1gNnSc6	označení
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dotyčné	dotyčný	k2eAgNnSc1d1	dotyčné
pivo	pivo	k1gNnSc1	pivo
dodnes	dodnes	k6eAd1	dodnes
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Bitburger	Bitburger	k1gInSc4	Bitburger
Pils	Pilsa	k1gFnPc2	Pilsa
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
plzeňské	plzeňský	k2eAgNnSc4d1	plzeňské
pivo	pivo	k1gNnSc4	pivo
definitivně	definitivně	k6eAd1	definitivně
vysloužilo	vysloužit	k5eAaPmAgNnS	vysloužit
oficiální	oficiální	k2eAgNnSc4d1	oficiální
uznání	uznání	k1gNnSc4	uznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Pivo	pivo	k1gNnSc1	pivo
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
typu	typ	k1gInSc2	typ
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
udržuje	udržovat	k5eAaImIp3nS	udržovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
popularitu	popularita	k1gFnSc4	popularita
a	a	k8xC	a
vaří	vařit	k5eAaImIp3nP	vařit
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pivovarnická	pivovarnický	k2eAgFnSc1d1	pivovarnická
asociace	asociace	k1gFnSc1	asociace
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tři	tři	k4xCgInPc4	tři
typy	typ	k1gInPc4	typ
plzeňského	plzeňský	k2eAgNnSc2d1	plzeňské
piva	pivo	k1gNnSc2	pivo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Plzeňské	plzeňské	k1gNnSc1	plzeňské
německého	německý	k2eAgInSc2d1	německý
stylu	styl	k1gInSc2	styl
</s>
</p>
<p>
<s>
Pivo	pivo	k1gNnSc1	pivo
světlé	světlý	k2eAgFnSc2d1	světlá
slámové	slámový	k2eAgFnSc2d1	slámová
či	či	k8xC	či
zlaté	zlatý	k2eAgFnSc2d1	zlatá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
prochmelené	prochmelený	k2eAgInPc1d1	prochmelený
až	až	k9	až
velmi	velmi	k6eAd1	velmi
hořké	hořký	k2eAgNnSc1d1	hořké
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plzeňské	plzeňské	k1gNnSc1	plzeňské
českého	český	k2eAgInSc2d1	český
stylu	styl	k1gInSc2	styl
</s>
</p>
<p>
<s>
Pivo	pivo	k1gNnSc1	pivo
zlaté	zlatá	k1gFnSc2	zlatá
<g/>
,	,	kIx,	,
plné	plný	k2eAgFnPc4d1	plná
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
hořké	hořký	k2eAgNnSc1d1	hořké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrazného	výrazný	k2eAgNnSc2d1	výrazné
aroma	aroma	k1gNnSc2	aroma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plzeňské	plzeňské	k1gNnSc1	plzeňské
evropského	evropský	k2eAgInSc2d1	evropský
stylu	styl	k1gInSc2	styl
</s>
</p>
<p>
<s>
Pivo	pivo	k1gNnSc1	pivo
sladké	sladký	k2eAgInPc1d1	sladký
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
prochmelené	prochmelený	k2eAgFnPc1d1	prochmelený
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyráběno	vyrábět	k5eAaImNgNnS	vyrábět
i	i	k9	i
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
než	než	k8xS	než
ječného	ječný	k2eAgInSc2d1	ječný
sladu	slad	k1gInSc2	slad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pivo	pivo	k1gNnSc1	pivo
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
typu	typ	k1gInSc2	typ
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
plzeňské	plzeňský	k2eAgNnSc1d1	plzeňské
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
Prazdroj	prazdroj	k1gInSc1	prazdroj
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
Pilsner	Pilsnero	k1gNnPc2	Pilsnero
Urquell	Urquellum	k1gNnPc2	Urquellum
na	na	k7c6	na
blogu	blog	k1gInSc6	blog
Glossary	Glossara	k1gFnSc2	Glossara
</s>
</p>
<p>
<s>
Druhy	druh	k1gInPc1	druh
piv	pivo	k1gNnPc2	pivo
dle	dle	k7c2	dle
Pivovarnické	pivovarnický	k2eAgFnSc2d1	pivovarnická
asociace	asociace	k1gFnSc2	asociace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pivní	pivní	k2eAgInSc1d1	pivní
styl	styl	k1gInSc1	styl
Bohemian	bohemian	k1gInSc1	bohemian
Pilsner	Pilsner	k1gInSc1	Pilsner
a	a	k8xC	a
seznam	seznam	k1gInSc1	seznam
vařených	vařený	k2eAgNnPc2d1	vařené
piv	pivo	k1gNnPc2	pivo
v	v	k7c6	v
ČR	ČR	kA	ČR
-	-	kIx~	-
pivní	pivní	k2eAgInSc1d1	pivní
portál	portál	k1gInSc1	portál
ZaPivem	ZaPivo	k1gNnSc7	ZaPivo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
