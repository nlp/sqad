<s>
Pidgin	Pidgin	k1gInSc1	Pidgin
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
Gaim	Gaim	k1gInSc1	Gaim
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
jako	jako	k9	jako
GTK	GTK	kA	GTK
<g/>
+	+	kIx~	+
AOL	AOL	kA	AOL
Instant	Instant	k1gInSc1	Instant
Messenger	Messenger	k1gInSc1	Messenger
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
multiprotokolový	multiprotokolový	k2eAgMnSc1d1	multiprotokolový
multiplatformní	multiplatformní	k2eAgMnSc1d1	multiplatformní
klient	klient	k1gMnSc1	klient
pro	pro	k7c4	pro
instant	instant	k?	instant
messaging	messaging	k1gInSc4	messaging
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
podporuje	podporovat	k5eAaImIp3nS	podporovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
komunikačních	komunikační	k2eAgInPc2d1	komunikační
protokolů	protokol	k1gInPc2	protokol
<g/>
:	:	kIx,	:
AIM	AIM	kA	AIM
<g/>
,	,	kIx,	,
Bonjour	Bonjour	k1gMnSc1	Bonjour
<g/>
,	,	kIx,	,
Gadu-Gadu	Gadu-Gada	k1gFnSc4	Gadu-Gada
<g/>
,	,	kIx,	,
Google	Google	k1gInSc4	Google
Talk	Talka	k1gFnPc2	Talka
<g/>
,	,	kIx,	,
GroupWise	GroupWise	k1gFnSc2	GroupWise
<g/>
,	,	kIx,	,
ICQ	ICQ	kA	ICQ
<g/>
,	,	kIx,	,
IRC	IRC	kA	IRC
<g/>
,	,	kIx,	,
MSN	MSN	kA	MSN
<g/>
,	,	kIx,	,
MXit	MXit	k1gMnSc1	MXit
<g/>
,	,	kIx,	,
SILC	SILC	kA	SILC
<g/>
,	,	kIx,	,
SIMPLE	SIMPLE	kA	SIMPLE
<g/>
,	,	kIx,	,
Sametime	Sametim	k1gMnSc5	Sametim
<g/>
,	,	kIx,	,
XMPP	XMPP	kA	XMPP
(	(	kIx(	(
<g/>
Jabber	Jabber	k1gMnSc1	Jabber
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k1gMnSc1	Yahoo
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
Zephyr	Zephyr	k1gInSc1	Zephyr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
aplikace	aplikace	k1gFnSc1	aplikace
je	být	k5eAaImIp3nS	být
svobodný	svobodný	k2eAgInSc1d1	svobodný
software	software	k1gInSc1	software
vytvářený	vytvářený	k2eAgInSc1d1	vytvářený
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
GNU	gnu	k1gNnSc2	gnu
GPL	GPL	kA	GPL
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
napsán	napsat	k5eAaBmNgInS	napsat
pod	pod	k7c7	pod
knihovnou	knihovna	k1gFnSc7	knihovna
GTK	GTK	kA	GTK
<g/>
+	+	kIx~	+
2	[number]	k4	2
a	a	k8xC	a
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jsou	být	k5eAaImIp3nP	být
verze	verze	k1gFnPc1	verze
pro	pro	k7c4	pro
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
macOS	macOS	k?	macOS
<g/>
,	,	kIx,	,
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
Qtopia	Qtopia	k1gFnSc1	Qtopia
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Sharp	sharp	k1gInSc1	sharp
Zaurus	Zaurus	k1gInSc1	Zaurus
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Pidgin	Pidgin	k1gInSc1	Pidgin
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
klient	klient	k1gMnSc1	klient
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
komunikačních	komunikační	k2eAgInPc2d1	komunikační
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
využívat	využívat	k5eAaPmF	využívat
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozšířit	rozšířit	k5eAaPmF	rozšířit
množstvím	množství	k1gNnSc7	množství
pluginů	plugin	k1gMnPc2	plugin
<g/>
.	.	kIx.	.
</s>
<s>
Pidgin	Pidgin	k1gInSc1	Pidgin
podporuje	podporovat	k5eAaImIp3nS	podporovat
textovou	textový	k2eAgFnSc4d1	textová
komunikaci	komunikace	k1gFnSc4	komunikace
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
ICQ	ICQ	kA	ICQ
<g/>
,	,	kIx,	,
posílání	posílání	k1gNnSc1	posílání
souborů	soubor	k1gInPc2	soubor
přes	přes	k7c4	přes
některé	některý	k3yIgInPc4	některý
protokoly	protokol	k1gInPc4	protokol
<g/>
,	,	kIx,	,
chaty	chata	k1gFnPc4	chata
a	a	k8xC	a
IRC	IRC	kA	IRC
kanály	kanál	k1gInPc1	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Uložené	uložený	k2eAgInPc4d1	uložený
kontakty	kontakt	k1gInPc4	kontakt
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
Pidgin	Pidgin	k1gInSc1	Pidgin
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
automaticky	automaticky	k6eAd1	automaticky
odeslat	odeslat	k5eAaPmF	odeslat
zprávu	zpráva	k1gFnSc4	zpráva
kontaktu	kontakt	k1gInSc2	kontakt
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
připojení	připojení	k1gNnSc6	připojení
<g/>
,	,	kIx,	,
upozornit	upozornit	k5eAaPmF	upozornit
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
daný	daný	k2eAgInSc4d1	daný
kontakt	kontakt	k1gInSc4	kontakt
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
nečinnosti	nečinnost	k1gFnSc2	nečinnost
<g/>
,	,	kIx,	,
odhlásil	odhlásit	k5eAaPmAgMnS	odhlásit
se	se	k3xPyFc4	se
<g/>
...	...	k?	...
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
okna	okno	k1gNnPc4	okno
chatů	chata	k1gMnPc2	chata
<g/>
,	,	kIx,	,
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
rozhovorů	rozhovor	k1gInPc2	rozhovor
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
sdružovat	sdružovat	k5eAaImF	sdružovat
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
okna	okno	k1gNnSc2	okno
se	s	k7c7	s
záložkami	záložka	k1gFnPc7	záložka
<g/>
.	.	kIx.	.
</s>
<s>
Pidgin	Pidgin	k1gInSc1	Pidgin
automaticky	automaticky	k6eAd1	automaticky
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
veškerou	veškerý	k3xTgFnSc4	veškerý
komunikaci	komunikace	k1gFnSc4	komunikace
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
klíčová	klíčový	k2eAgNnPc1d1	klíčové
slova	slovo	k1gNnPc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
podporuje	podporovat	k5eAaImIp3nS	podporovat
Zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
(	(	kIx(	(
<g/>
informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatel	uživatel	k1gMnSc1	uživatel
není	být	k5eNaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
<g/>
,	,	kIx,	,
nepřeje	přát	k5eNaImIp3nS	přát
si	se	k3xPyFc3	se
být	být	k5eAaImF	být
rušen	rušen	k2eAgInSc4d1	rušen
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
různá	různý	k2eAgNnPc4d1	různé
kódování	kódování	k1gNnPc4	kódování
textové	textový	k2eAgFnPc4d1	textová
komunikace	komunikace	k1gFnPc4	komunikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
problém	problém	k1gInSc1	problém
s	s	k7c7	s
přijímáním	přijímání	k1gNnSc7	přijímání
zpráv	zpráva	k1gFnPc2	zpráva
od	od	k7c2	od
klientů	klient	k1gMnPc2	klient
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
správně	správně	k6eAd1	správně
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
kódováním	kódování	k1gNnSc7	kódování
(	(	kIx(	(
<g/>
a	a	k8xC	a
nebo	nebo	k8xC	nebo
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
kódování	kódování	k1gNnSc4	kódování
neodesílají	odesílat	k5eNaImIp3nP	odesílat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okno	okno	k1gNnSc1	okno
kontaktů	kontakt	k1gInPc2	kontakt
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
minimalizovat	minimalizovat	k5eAaBmF	minimalizovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Oznamovací	oznamovací	k2eAgFnSc2d1	oznamovací
oblasti	oblast	k1gFnSc2	oblast
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
běh	běh	k1gInSc4	běh
programu	program	k1gInSc2	program
pod	pod	k7c7	pod
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
mít	mít	k5eAaImF	mít
nainstalovány	nainstalován	k2eAgFnPc4d1	nainstalována
knihovny	knihovna	k1gFnPc4	knihovna
GTK	GTK	kA	GTK
<g/>
+	+	kIx~	+
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
podporuje	podporovat	k5eAaImIp3nS	podporovat
mnoho	mnoho	k4c4	mnoho
jazykových	jazykový	k2eAgFnPc2d1	jazyková
verzí	verze	k1gFnPc2	verze
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnPc4	aplikace
je	být	k5eAaImIp3nS	být
svobodný	svobodný	k2eAgInSc1d1	svobodný
software	software	k1gInSc1	software
licencovaný	licencovaný	k2eAgInSc1d1	licencovaný
pod	pod	k7c4	pod
GNU	gnu	k1gNnSc4	gnu
GPL	GPL	kA	GPL
a	a	k8xC	a
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
přispívat	přispívat	k5eAaImF	přispívat
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
vývoji	vývoj	k1gInSc3	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Pidgin	Pidgin	k1gInSc1	Pidgin
je	být	k5eAaImIp3nS	být
primárně	primárně	k6eAd1	primárně
vyvíjen	vyvíjet	k5eAaImNgMnS	vyvíjet
skupinou	skupina	k1gFnSc7	skupina
vývojářů	vývojář	k1gMnPc2	vývojář
okolo	okolo	k7c2	okolo
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
sponzorem	sponzor	k1gMnSc7	sponzor
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Linspire	Linspir	k1gInSc5	Linspir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
projektu	projekt	k1gInSc2	projekt
(	(	kIx(	(
<g/>
fork	fork	k6eAd1	fork
<g/>
)	)	kIx)	)
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
varianta	varianta	k1gFnSc1	varianta
Gaim-vv	Gaimva	k1gFnPc2	Gaim-vva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
přenos	přenos	k1gInSc4	přenos
videa	video	k1gNnSc2	video
a	a	k8xC	a
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
odnož	odnož	k1gFnSc1	odnož
ale	ale	k9	ale
momentálně	momentálně	k6eAd1	momentálně
stagnuje	stagnovat	k5eAaImIp3nS	stagnovat
a	a	k8xC	a
opožďuje	opožďovat	k5eAaImIp3nS	opožďovat
se	se	k3xPyFc4	se
za	za	k7c7	za
původní	původní	k2eAgFnSc7d1	původní
verzí	verze	k1gFnSc7	verze
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
verze	verze	k1gFnPc1	verze
Gaim-vv	Gaimva	k1gFnPc2	Gaim-vva
nemusí	muset	k5eNaImIp3nP	muset
podporovat	podporovat	k5eAaImF	podporovat
všechny	všechen	k3xTgFnPc4	všechen
platformy	platforma	k1gFnPc4	platforma
jako	jako	k8xC	jako
původní	původní	k2eAgInSc4d1	původní
Gaim	Gaim	k1gInSc4	Gaim
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Pidgin	Pidgin	k2eAgInSc1d1	Pidgin
<g/>
)	)	kIx)	)
a	a	k8xC	a
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
programy	program	k1gInPc4	program
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
totožné	totožný	k2eAgNnSc4d1	totožné
<g/>
.	.	kIx.	.
</s>
<s>
Odštěpení	odštěpení	k1gNnSc1	odštěpení
původně	původně	k6eAd1	původně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
společnost	společnost	k1gFnSc1	společnost
Linspire	Linspir	k1gInSc5	Linspir
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
komunikační	komunikační	k2eAgInSc4d1	komunikační
program	program	k1gInSc4	program
zároveň	zároveň	k6eAd1	zároveň
podporující	podporující	k2eAgInPc4d1	podporující
přenosy	přenos	k1gInPc4	přenos
videa	video	k1gNnSc2	video
a	a	k8xC	a
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Pidgin	Pidgin	k1gInSc1	Pidgin
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozšířit	rozšířit	k5eAaPmF	rozšířit
množstvím	množství	k1gNnSc7	množství
pluginů	plugin	k1gInPc2	plugin
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přinášejí	přinášet	k5eAaImIp3nP	přinášet
další	další	k2eAgFnPc4d1	další
možnosti	možnost	k1gFnPc4	možnost
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
některých	některý	k3yIgFnPc6	některý
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
textu	text	k1gInSc6	text
níže	níže	k1gFnSc2	níže
<g/>
.	.	kIx.	.
</s>
<s>
Guifications	Guifications	k6eAd1	Guifications
Vyskakující	vyskakující	k2eAgNnSc1d1	vyskakující
upozornění	upozornění	k1gNnSc1	upozornění
na	na	k7c4	na
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
MSN	MSN	kA	MSN
Messengeru	Messenger	k1gInSc2	Messenger
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
akce	akce	k1gFnPc1	akce
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
nastavit	nastavit	k5eAaPmF	nastavit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
oblast	oblast	k1gFnSc1	oblast
zobrazování	zobrazování	k1gNnSc2	zobrazování
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
těchto	tento	k3xDgNnPc2	tento
oken	okno	k1gNnPc2	okno
lze	lze	k6eAd1	lze
měnit	měnit	k5eAaImF	měnit
pomocí	pomocí	k7c2	pomocí
guifications	guificationsa	k1gFnPc2	guificationsa
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
stránka	stránka	k1gFnSc1	stránka
projektu	projekt	k1gInSc2	projekt
<g/>
:	:	kIx,	:
http://guifications.sourceforge.net/	[url]	k?	http://guifications.sourceforge.net/
Gaim	Gaim	k1gInSc1	Gaim
encryption	encryption	k1gInSc1	encryption
Plugin	Plugin	k1gInSc4	Plugin
umožňující	umožňující	k2eAgNnSc4d1	umožňující
šifrování	šifrování	k1gNnSc4	šifrování
komunikace	komunikace	k1gFnSc2	komunikace
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
programy	program	k1gInPc7	program
Pidgin	Pidgin	k1gInSc1	Pidgin
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
musejí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
tento	tento	k3xDgInSc4	tento
plugin	plugin	k1gInSc4	plugin
správně	správně	k6eAd1	správně
nainstalován	nainstalován	k2eAgInSc1d1	nainstalován
<g/>
)	)	kIx)	)
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
použitém	použitý	k2eAgInSc6d1	použitý
protokolu	protokol	k1gInSc6	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
stránka	stránka	k1gFnSc1	stránka
projektu	projekt	k1gInSc2	projekt
<g/>
:	:	kIx,	:
http://gaim-encryption.sourceforge.net/	[url]	k?	http://gaim-encryption.sourceforge.net/
Extended	Extended	k1gMnSc1	Extended
Preferences	Preferences	k1gMnSc1	Preferences
Plugin	Plugin	k1gMnSc1	Plugin
zpřístupňující	zpřístupňující	k2eAgNnPc4d1	zpřístupňující
pokročilá	pokročilý	k2eAgNnPc4d1	pokročilé
nastavení	nastavení	k1gNnSc4	nastavení
programu	program	k1gInSc2	program
Pidgin	Pidgin	k1gMnSc1	Pidgin
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
stránka	stránka	k1gFnSc1	stránka
<g/>
:	:	kIx,	:
http://gaim-extprefs.sf.net/	[url]	k?	http://gaim-extprefs.sf.net/
Gesta	gesto	k1gNnSc2	gesto
myši	myš	k1gFnSc2	myš
Pro	pro	k7c4	pro
mnohé	mnohé	k1gNnSc4	mnohé
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
způsob	způsob	k1gInSc1	způsob
ovládání	ovládání	k1gNnSc4	ovládání
WWW	WWW	kA	WWW
prohlížečů	prohlížeč	k1gInPc2	prohlížeč
Firefox	Firefox	k1gInSc1	Firefox
nebo	nebo	k8xC	nebo
Opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plugin	plugin	k1gInSc1	plugin
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ovládat	ovládat	k5eAaImF	ovládat
program	program	k1gInSc4	program
Pidgin	Pidgin	k2eAgInSc4d1	Pidgin
obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
plugin	plugin	k1gInSc1	plugin
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Integrace	integrace	k1gFnSc1	integrace
s	s	k7c7	s
Evolution	Evolution	k1gInSc1	Evolution
Plugin	Plugin	k1gInSc4	Plugin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
integrovat	integrovat	k5eAaBmF	integrovat
a	a	k8xC	a
synchronizovat	synchronizovat	k5eAaBmF	synchronizovat
kontakty	kontakt	k1gInPc4	kontakt
z	z	k7c2	z
programu	program	k1gInSc2	program
Pidgin	Pidgin	k1gInSc4	Pidgin
s	s	k7c7	s
programem	program	k1gInSc7	program
Evolution	Evolution	k1gInSc1	Evolution
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
Evolution	Evolution	k1gInSc1	Evolution
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
integrační	integrační	k2eAgFnSc4d1	integrační
podporu	podpora	k1gFnSc4	podpora
s	s	k7c7	s
programem	program	k1gInSc7	program
Pidgin	Pidgina	k1gFnPc2	Pidgina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plugin	plugin	k1gInSc1	plugin
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
IRC	IRC	kA	IRC
Helper	Helper	k1gInSc1	Helper
Rozšíření	rozšíření	k1gNnSc1	rozšíření
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
podporu	podpora	k1gFnSc4	podpora
komunikace	komunikace	k1gFnSc2	komunikace
na	na	k7c4	na
IRC	IRC	kA	IRC
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
programu	program	k1gInSc2	program
Pidgin	Pidgina	k1gFnPc2	Pidgina
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
stránka	stránka	k1gFnSc1	stránka
<g/>
:	:	kIx,	:
http://gaim-irchelper.sf.net	[url]	k1gMnSc1	http://gaim-irchelper.sf.net
Nautilus	Nautilus	k1gMnSc1	Nautilus
Integration	Integration	k1gInSc4	Integration
Rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
integruje	integrovat	k5eAaBmIp3nS	integrovat
Pidgin	Pidgin	k1gInSc1	Pidgin
do	do	k7c2	do
kontextové	kontextový	k2eAgFnSc2d1	kontextová
nabídky	nabídka	k1gFnSc2	nabídka
souborového	souborový	k2eAgInSc2d1	souborový
manažeru	manažer	k1gInSc2	manažer
Nautilus	Nautilus	k1gMnSc1	Nautilus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plugin	plugin	k1gInSc1	plugin
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vybraný	vybraný	k2eAgInSc1d1	vybraný
soubor	soubor	k1gInSc1	soubor
přímo	přímo	k6eAd1	přímo
odesílat	odesílat	k5eAaImF	odesílat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
programu	program	k1gInSc2	program
Pidgin	Pidgina	k1gFnPc2	Pidgina
některému	některý	k3yIgInSc3	některý
z	z	k7c2	z
uložených	uložený	k2eAgInPc2d1	uložený
kontaktů	kontakt	k1gInPc2	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálené	vzdálený	k2eAgNnSc1d1	vzdálené
ovládání	ovládání	k1gNnSc1	ovládání
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
pluginu	plugin	k1gInSc3	plugin
je	být	k5eAaImIp3nS	být
umožněno	umožněn	k2eAgNnSc1d1	umožněno
programům	program	k1gInPc3	program
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
ovládat	ovládat	k5eAaImF	ovládat
program	program	k1gInSc4	program
Pidgin	Pidgina	k1gFnPc2	Pidgina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plugin	plugin	k1gInSc1	plugin
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pidgin	Pidgina	k1gFnPc2	Pidgina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Pidgin	Pidgin	k1gInSc4	Pidgin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Domácí	domácí	k1gFnSc1	domácí
stránka	stránka	k1gFnSc1	stránka
projektu	projekt	k1gInSc2	projekt
Česká	český	k2eAgFnSc1d1	Česká
stránka	stránka	k1gFnSc1	stránka
Odnož	odnož	k1gFnSc4	odnož
projektu	projekt	k1gInSc2	projekt
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
videa	video	k1gNnSc2	video
<g/>
/	/	kIx~	/
<g/>
audia	audium	k1gNnSc2	audium
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
