<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
kosa	kosa	k1gFnSc1	kosa
–	–	k?	–
kos	kos	k1gMnSc1	kos
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
merula	merul	k1gMnSc2	merul
<g/>
)	)	kIx)	)
a	a	k8xC	a
kos	kos	k1gMnSc1	kos
horský	horský	k2eAgMnSc1d1	horský
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
torquatus	torquatus	k1gMnSc1	torquatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
druh	druh	k1gMnSc1	druh
–	–	k?	–
kos	kos	k1gMnSc1	kos
šedokřídlý	šedokřídlý	k2eAgMnSc1d1	šedokřídlý
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gInSc1	Turdus
boulboul	boulboul	k1gInSc1	boulboul
<g/>
)	)	kIx)	)
–	–	k?	–
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
