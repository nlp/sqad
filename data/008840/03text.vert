<p>
<s>
Kos	Kos	k1gMnSc1	Kos
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
některých	některý	k3yIgMnPc2	některý
pěvců	pěvec	k1gMnPc2	pěvec
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
drozd	drozd	k1gMnSc1	drozd
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
kos	kos	k1gMnSc1	kos
<g/>
"	"	kIx"	"
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
název	název	k1gInSc1	název
rodový	rodový	k2eAgInSc1d1	rodový
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
podrod	podrod	k1gInSc4	podrod
Merula	Merulum	k1gNnSc2	Merulum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
kosa	kosa	k1gFnSc1	kosa
–	–	k?	–
kos	kos	k1gMnSc1	kos
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
merula	merul	k1gMnSc2	merul
<g/>
)	)	kIx)	)
a	a	k8xC	a
kos	kos	k1gMnSc1	kos
horský	horský	k2eAgMnSc1d1	horský
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
torquatus	torquatus	k1gMnSc1	torquatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
druh	druh	k1gMnSc1	druh
–	–	k?	–
kos	kos	k1gMnSc1	kos
šedokřídlý	šedokřídlý	k2eAgMnSc1d1	šedokřídlý
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gInSc1	Turdus
boulboul	boulboul	k1gInSc1	boulboul
<g/>
)	)	kIx)	)
–	–	k?	–
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kos	kosa	k1gFnPc2	kosa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
kos	kosa	k1gFnPc2	kosa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kos	kosa	k1gFnPc2	kosa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
