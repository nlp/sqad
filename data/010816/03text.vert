<p>
<s>
Záhněda	záhněda	k1gFnSc1	záhněda
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
křemene	křemen	k1gInSc2	křemen
(	(	kIx(	(
<g/>
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zabarvená	zabarvený	k2eAgFnSc1d1	zabarvená
příměsí	příměs	k1gFnSc7	příměs
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
lithia	lithium	k1gNnSc2	lithium
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
polodrahokam	polodrahokam	k1gInSc4	polodrahokam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vyhledáván	vyhledáván	k2eAgInSc1d1	vyhledáván
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tmavě	tmavě	k6eAd1	tmavě
hnědá	hnědat	k5eAaImIp3nS	hnědat
až	až	k9	až
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
neprůsvitná	průsvitný	k2eNgFnSc1d1	neprůsvitná
odrůda	odrůda	k1gFnSc1	odrůda
křemene	křemen	k1gInSc2	křemen
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
morion	morion	k1gInSc1	morion
<g/>
,	,	kIx,	,
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
záhnědou	záhněda	k1gFnSc7	záhněda
a	a	k8xC	a
morionem	morion	k1gInSc7	morion
je	být	k5eAaImIp3nS	být
diskutabilní	diskutabilní	k2eAgMnSc1d1	diskutabilní
<g/>
.	.	kIx.	.
</s>
<s>
Záhněda	záhněda	k1gFnSc1	záhněda
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jak	jak	k6eAd1	jak
ve	v	k7c6	v
zlatnickém	zlatnický	k2eAgInSc6d1	zlatnický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
léčitelství	léčitelství	k1gNnSc6	léčitelství
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jí	on	k3xPp3gFnSc3	on
přisuzovány	přisuzovat	k5eAaImNgInP	přisuzovat
uklidňující	uklidňující	k2eAgInPc1d1	uklidňující
a	a	k8xC	a
pročisťující	pročisťující	k2eAgInPc1d1	pročisťující
účinky	účinek	k1gInPc1	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
–	–	k?	–
Krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
v	v	k7c6	v
klencové	klencový	k2eAgFnSc6d1	klencová
soustavě	soustava	k1gFnSc6	soustava
a	a	k8xC	a
na	na	k7c6	na
Mohsově	Mohsův	k2eAgFnSc6d1	Mohsova
stupnici	stupnice	k1gFnSc6	stupnice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
má	mít	k5eAaImIp3nS	mít
tvrdost	tvrdost	k1gFnSc1	tvrdost
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
tvar	tvar	k1gInSc1	tvar
krystalu	krystal	k1gInSc2	krystal
je	být	k5eAaImIp3nS	být
šestiboký	šestiboký	k2eAgInSc1d1	šestiboký
hranol	hranol	k1gInSc1	hranol
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
klenci	klenec	k1gInPc7	klenec
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgMnSc2	který
bývají	bývat	k5eAaImIp3nP	bývat
plochy	plocha	k1gFnPc1	plocha
hranolu	hranol	k1gInSc2	hranol
vodorovně	vodorovně	k6eAd1	vodorovně
rýhovány	rýhován	k2eAgFnPc1d1	rýhován
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dvojčatění	dvojčatění	k1gNnSc3	dvojčatění
nebo	nebo	k8xC	nebo
růstu	růst	k1gInSc3	růst
polykrystalů	polykrystal	k1gMnPc2	polykrystal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
také	také	k9	také
s	s	k7c7	s
monokrystaly	monokrystal	k1gInPc7	monokrystal
<g/>
.	.	kIx.	.
</s>
<s>
Rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc6d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgInPc1d1	chemický
–	–	k?	–
</s>
</p>
<p>
<s>
Optické	optický	k2eAgInPc4d1	optický
–	–	k?	–
Jeho	jeho	k3xOp3gInSc7	jeho
barva	barva	k1gFnSc1	barva
není	být	k5eNaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uměle	uměle	k6eAd1	uměle
upravuje	upravovat	k5eAaImIp3nS	upravovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vlivem	vliv	k1gInSc7	vliv
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
záření	záření	k1gNnSc2	záření
tmavne	tmavnout	k5eAaImIp3nS	tmavnout
<g/>
,	,	kIx,	,
vlivem	vliv	k1gInSc7	vliv
tepla	teplo	k1gNnSc2	teplo
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Záhnědy	záhněda	k1gFnPc1	záhněda
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
granitoidní	granitoidní	k2eAgFnPc4d1	granitoidní
magmatické	magmatický	k2eAgFnPc4d1	magmatická
horniny	hornina	k1gFnPc4	hornina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
granitoidech	granitoid	k1gInPc6	granitoid
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
pegmatitů	pegmatit	k1gInPc2	pegmatit
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
magmatické	magmatický	k2eAgFnSc2d1	magmatická
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
krystalizace	krystalizace	k1gFnSc2	krystalizace
z	z	k7c2	z
fluidní	fluidní	k2eAgFnSc2d1	fluidní
fáze	fáze	k1gFnSc2	fáze
(	(	kIx(	(
<g/>
dutinové	dutinový	k2eAgFnSc2d1	dutinová
záhnědy	záhněda	k1gFnSc2	záhněda
v	v	k7c6	v
pegmatitech	pegmatit	k1gInPc6	pegmatit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
typickým	typický	k2eAgInSc7d1	typický
výskytem	výskyt	k1gInSc7	výskyt
jsou	být	k5eAaImIp3nP	být
alpské	alpský	k2eAgFnPc4d1	alpská
žíly	žíla	k1gFnPc4	žíla
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
záhnědy	záhněda	k1gFnPc1	záhněda
nejčastěji	často	k6eAd3	často
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c4	na
pukliny	puklina	k1gFnPc4	puklina
v	v	k7c6	v
granitoidech	granitoid	k1gInPc6	granitoid
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
běžné	běžný	k2eAgFnPc1d1	běžná
jsou	být	k5eAaImIp3nP	být
záhnědy	záhněda	k1gFnPc1	záhněda
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
vulkanických	vulkanický	k2eAgFnPc2d1	vulkanická
hornin	hornina	k1gFnPc2	hornina
(	(	kIx(	(
<g/>
geody	geoda	k1gFnPc1	geoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
hydrotermálních	hydrotermální	k2eAgFnPc6d1	hydrotermální
žilách	žíla	k1gFnPc6	žíla
a	a	k8xC	a
v	v	k7c6	v
metamorfovaných	metamorfovaný	k2eAgFnPc6d1	metamorfovaná
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
odolnosti	odolnost	k1gFnSc3	odolnost
se	se	k3xPyFc4	se
valouny	valoun	k1gInPc1	valoun
záhněd	záhněda	k1gFnPc2	záhněda
druhotně	druhotně	k6eAd1	druhotně
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
sedimentů	sediment	k1gInPc2	sediment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Využití	využití	k1gNnSc1	využití
záhnědy	záhněda	k1gFnSc2	záhněda
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
,	,	kIx,	,
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
jižní	jižní	k2eAgFnPc4d1	jižní
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
Vysočina	vysočina	k1gFnSc1	vysočina
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgInPc1d1	dolní
Bory	bor	k1gInPc1	bor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
záhněda	záhněda	k1gFnSc1	záhněda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
záhněda	záhněda	k1gFnSc1	záhněda
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Minerální	minerální	k2eAgFnSc1d1	minerální
galerie	galerie	k1gFnSc1	galerie
–	–	k?	–
záhněda	záhněda	k1gFnSc1	záhněda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Záhněda	záhněda	k1gFnSc1	záhněda
na	na	k7c6	na
Mindat	Mindat	k1gFnSc6	Mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
záhněd	záhněda	k1gFnPc2	záhněda
na	na	k7c4	na
Rockhound	Rockhound	k1gInSc4	Rockhound
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
