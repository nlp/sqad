<s>
Lesy	les	k1gInPc1	les
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Kuřimi	Kuři	k1gFnPc7	Kuři
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
Ing.	ing.	kA	ing.
Jiří	Jiří	k1gMnSc1	Jiří
Neshyba	Neshyba	k1gMnSc1	Neshyba
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
činností	činnost	k1gFnSc7	činnost
je	být	k5eAaImIp3nS	být
odborná	odborný	k2eAgFnSc1d1	odborná
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc1	rozvoj
lesního	lesní	k2eAgInSc2d1	lesní
majetku	majetek	k1gInSc2	majetek
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
podnikání	podnikání	k1gNnSc2	podnikání
a	a	k8xC	a
plnění	plnění	k1gNnSc2	plnění
veřejných	veřejný	k2eAgFnPc2d1	veřejná
služeb	služba	k1gFnPc2	služba
s	s	k7c7	s
obecně	obecně	k6eAd1	obecně
prospěšným	prospěšný	k2eAgNnSc7d1	prospěšné
posláním	poslání	k1gNnSc7	poslání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
činí	činit	k5eAaImIp3nS	činit
výměra	výměra	k1gFnSc1	výměra
lesů	les	k1gInPc2	les
8	[number]	k4	8
220	[number]	k4	220
ha	ha	kA	ha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
městský	městský	k2eAgInSc1d1	městský
majetek	majetek	k1gInSc1	majetek
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
Lesy	les	k1gInPc1	les
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
odbornou	odborný	k2eAgFnSc4d1	odborná
správu	správa	k1gFnSc4	správa
v	v	k7c6	v
městských	městský	k2eAgInPc6d1	městský
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
příspěvkové	příspěvkový	k2eAgFnSc2d1	příspěvková
organizace	organizace	k1gFnSc2	organizace
nová	nový	k2eAgFnSc1d1	nová
organizace	organizace	k1gFnSc1	organizace
<g/>
:	:	kIx,	:
Lesy	les	k1gInPc1	les
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
Zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
akcionářem	akcionář	k1gMnSc7	akcionář
statutární	statutární	k2eAgFnSc2d1	statutární
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
organizačně	organizačně	k6eAd1	organizačně
členěna	členit	k5eAaImNgFnS	členit
na	na	k7c6	na
<g/>
:	:	kIx,	:
ústředí	ústředí	k1gNnSc6	ústředí
společnosti	společnost	k1gFnSc2	společnost
lesní	lesní	k2eAgFnSc2d1	lesní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc4	tři
(	(	kIx(	(
<g/>
Lipůvka	Lipůvka	k1gFnSc1	Lipůvka
<g/>
,	,	kIx,	,
Deblín	Deblín	k1gInSc1	Deblín
a	a	k8xC	a
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
hospodářská	hospodářský	k2eAgNnPc1d1	hospodářské
střediska	středisko	k1gNnPc1	středisko
(	(	kIx(	(
<g/>
Lesní	lesní	k2eAgFnSc1d1	lesní
školky	školka	k1gFnPc1	školka
Svinošice	Svinošice	k1gFnSc2	Svinošice
<g/>
,	,	kIx,	,
pila	pít	k5eAaImAgFnS	pít
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
manipulačně	manipulačně	k6eAd1	manipulačně
expediční	expediční	k2eAgInSc4d1	expediční
sklad	sklad	k1gInSc4	sklad
Rájec-Jestřebí	Rájec-Jestřebí	k1gNnSc2	Rájec-Jestřebí
<g/>
,	,	kIx,	,
středisko	středisko	k1gNnSc1	středisko
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existují	existovat	k5eAaImIp3nP	existovat
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
naučné	naučný	k2eAgFnSc2d1	naučná
stezky	stezka	k1gFnSc2	stezka
<g/>
:	:	kIx,	:
Holedná	Holedný	k2eAgFnSc1d1	Holedná
–	–	k?	–
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
oborou	obora	k1gFnSc7	obora
Holedná	Holedný	k2eAgFnSc1d1	Holedná
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
7	[number]	k4	7
naučných	naučný	k2eAgFnPc2d1	naučná
tabulí	tabule	k1gFnPc2	tabule
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
–	–	k?	–
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
9	[number]	k4	9
naučných	naučný	k2eAgFnPc2d1	naučná
tabulí	tabule	k1gFnPc2	tabule
<g/>
,	,	kIx,	,
výchozí	výchozí	k2eAgInSc1d1	výchozí
bod	bod	k1gInSc1	bod
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
–	–	k?	–
<g/>
Štýřicích	Štýřik	k1gInPc6	Štýřik
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
cyklostezky	cyklostezka	k1gFnSc2	cyklostezka
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
až	až	k9	až
na	na	k7c4	na
Kamenný	kamenný	k2eAgInSc4d1	kamenný
vrch	vrch	k1gInSc4	vrch
Lipůvecká	Lipůvecký	k2eAgFnSc1d1	Lipůvecký
–	–	k?	–
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
9	[number]	k4	9
naučných	naučný	k2eAgFnPc2d1	naučná
tabulí	tabule	k1gFnPc2	tabule
<g/>
,	,	kIx,	,
výchozí	výchozí	k2eAgInSc1d1	výchozí
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Hořice	Hořice	k1gFnPc1	Hořice
Deblínská	Deblínský	k2eAgFnSc1d1	Deblínský
-	-	kIx~	-
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
6	[number]	k4	6
naučných	naučný	k2eAgFnPc2d1	naučná
tabulí	tabule	k1gFnPc2	tabule
<g/>
,	,	kIx,	,
výchozí	výchozí	k2eAgInSc1d1	výchozí
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
nedaleko	nedaleko	k7c2	nedaleko
deblínského	deblínský	k2eAgInSc2d1	deblínský
rybníka	rybník	k1gInSc2	rybník
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
parkoviště	parkoviště	k1gNnSc2	parkoviště
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
Deblín	Deblín	k1gInSc1	Deblín
–	–	k?	–
Svatoslav	Svatoslav	k1gFnSc1	Svatoslav
Na	na	k7c6	na
plochách	plocha	k1gFnPc6	plocha
spravovaných	spravovaný	k2eAgFnPc6d1	spravovaná
Lesy	les	k1gInPc1	les
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tato	tento	k3xDgNnPc1	tento
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
<g/>
:	:	kIx,	:
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Slunná	slunný	k2eAgFnSc1d1	slunná
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
park	park	k1gInSc1	park
Údolí	údolí	k1gNnSc2	údolí
Bílého	bílý	k2eAgInSc2d1	bílý
potoka	potok	k1gInSc2	potok
Přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
Baba	baba	k1gFnSc1	baba
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
Babí	babit	k5eAaImIp3nP	babit
lom	lom	k1gInSc4	lom
Přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Kamenný	kamenný	k2eAgInSc1d1	kamenný
vrch	vrch	k1gInSc1	vrch
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Údolí	údolí	k1gNnSc2	údolí
Kohoutovického	kohoutovický	k2eAgInSc2d1	kohoutovický
potoka	potok	k1gInSc2	potok
Přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Bosonožský	Bosonožský	k2eAgInSc1d1	Bosonožský
hájek	hájek	k1gInSc1	hájek
Přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Černovický	Černovický	k2eAgInSc1d1	Černovický
hájek	hájek	k1gInSc1	hájek
</s>
