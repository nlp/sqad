<s>
Část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
příběhy	příběh	k1gInPc4	příběh
Rychlých	Rychlých	k2eAgInPc2d1	Rychlých
šípů	šíp	k1gInPc2	šíp
a	a	k8xC	a
Hochů	hoch	k1gMnPc2	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
<g/>
)	)	kIx)	)
přešla	přejít	k5eAaPmAgFnS	přejít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
do	do	k7c2	do
obecného	obecný	k2eAgNnSc2d1	obecné
povědomí	povědomí	k1gNnSc2	povědomí
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
jeho	jeho	k3xOp3gInPc1	jeho
pojmy	pojem	k1gInPc1	pojem
a	a	k8xC	a
fráze	fráze	k1gFnPc1	fráze
zlidověly	zlidovět	k5eAaPmAgFnP	zlidovět
<g/>
.	.	kIx.	.
</s>
