<s>
I	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
těl	tělo	k1gNnPc2	tělo
mořských	mořský	k2eAgFnPc2d1	mořská
hub	houba	k1gFnPc2	houba
(	(	kIx(	(
<g/>
Porifera	Porifera	k1gFnSc1	Porifera
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
pláštěnců	pláštěnec	k1gMnPc2	pláštěnec
(	(	kIx(	(
<g/>
Tunicata	Tunicata	k1gFnSc1	Tunicata
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přítomny	přítomen	k2eAgFnPc1d1	přítomna
fotosyntetizující	fotosyntetizující	k2eAgFnPc1d1	fotosyntetizující
sinice	sinice	k1gFnPc1	sinice
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
nazývány	nazýván	k2eAgFnPc4d1	nazývána
zoocyanely	zoocyanela	k1gFnPc4	zoocyanela
<g/>
.	.	kIx.	.
</s>
