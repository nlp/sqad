<s>
Rýže	rýže	k1gFnSc1	rýže
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
Perlové	perlový	k2eAgFnSc2d1	Perlová
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
domestikována	domestikovat	k5eAaBmNgFnS	domestikovat
před	před	k7c7	před
8	[number]	k4	8
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
500	[number]	k4	500
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
