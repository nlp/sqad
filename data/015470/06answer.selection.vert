<s>
Lovecký	lovecký	k2eAgInSc1d1
zámeček	zámeček	k1gInSc1
Allein	Allein	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
České	český	k2eAgFnSc2d1
Křídlovice	Křídlovice	k1gFnSc2
<g/>
,	,	kIx,
Lechovice	Lechovice	k1gFnSc1
či	či	k8xC
Samota	samota	k1gFnSc1
<g/>
)	)	kIx)
stojí	stát	k5eAaImIp3nS
v	v	k7c6
lesích	les	k1gInPc6
mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
Lechovice	Lechovice	k1gFnSc2
<g/>
,	,	kIx,
Borotice	Borotice	k1gFnSc2
a	a	k8xC
Mlýnské	mlýnský	k2eAgInPc1d1
Domky	domek	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
Křídlovice	Křídlovice	k1gFnSc2
<g/>
,	,	kIx,
místně	místně	k6eAd1
spadající	spadající	k2eAgMnSc1d1
pod	pod	k7c4
Božice	Božic	k1gMnSc4
<g/>
.	.	kIx.
</s>