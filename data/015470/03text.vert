<s>
Allein	Allein	k1gMnSc1
</s>
<s>
Lovecký	lovecký	k2eAgInSc1d1
zámeček	zámeček	k1gInSc1
Allein	Alleina	k1gFnPc2
Základní	základní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Sloh	sloha	k1gFnPc2
</s>
<s>
švýcarská	švýcarský	k2eAgFnSc1d1
alpská	alpský	k2eAgFnSc1d1
chata	chata	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Stavebník	stavebník	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Braun	Braun	k1gMnSc1
Další	další	k2eAgFnSc7d1
majitelé	majitel	k1gMnPc1
</s>
<s>
Braunové	Braunové	k2eAgMnSc1d1
Současný	současný	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
</s>
<s>
Lesy	les	k1gInPc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
p.	p.	k?
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Božice	Božice	k1gFnSc1
338	#num#	k4
<g/>
,	,	kIx,
Božice	Božice	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
33,86	33,86	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
36,33	36,33	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Allein	Allein	k1gMnSc1
</s>
<s>
Allein	Allein	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lovecký	lovecký	k2eAgInSc1d1
zámeček	zámeček	k1gInSc1
Allein	Allein	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
České	český	k2eAgFnSc2d1
Křídlovice	Křídlovice	k1gFnSc2
<g/>
,	,	kIx,
Lechovice	Lechovice	k1gFnSc1
či	či	k8xC
Samota	samota	k1gFnSc1
<g/>
)	)	kIx)
stojí	stát	k5eAaImIp3nS
v	v	k7c6
lesích	les	k1gInPc6
mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
Lechovice	Lechovice	k1gFnSc2
<g/>
,	,	kIx,
Borotice	Borotice	k1gFnSc2
a	a	k8xC
Mlýnské	mlýnský	k2eAgInPc1d1
Domky	domek	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
Křídlovice	Křídlovice	k1gFnSc2
<g/>
,	,	kIx,
místně	místně	k6eAd1
spadající	spadající	k2eAgMnSc1d1
pod	pod	k7c4
Božice	Božic	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
dá	dát	k5eAaPmIp3nS
po	po	k7c6
asfaltové	asfaltový	k2eAgFnSc6d1
silnici	silnice	k1gFnSc6
z	z	k7c2
Borotic	Borotice	k1gFnPc2
nebo	nebo	k8xC
Lechovic	Lechovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejnosti	veřejnost	k1gFnSc3
je	být	k5eAaImIp3nS
nepřístupný	přístupný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Lovecký	lovecký	k2eAgInSc1d1
zámeček	zámeček	k1gInSc1
nechal	nechat	k5eAaPmAgInS
na	na	k7c6
počátku	počátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
ve	v	k7c6
stylu	styl	k1gInSc6
švýcarských	švýcarský	k2eAgFnPc2d1
alpských	alpský	k2eAgFnPc2d1
chat	chata	k1gFnPc2
postavit	postavit	k5eAaPmF
majitel	majitel	k1gMnSc1
jaroslavického	jaroslavický	k2eAgNnSc2d1
panství	panství	k1gNnSc2
Petr	Petr	k1gMnSc1
Braun	Braun	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
polohu	poloha	k1gFnSc4
uprostřed	uprostřed	k7c2
lesů	les	k1gInPc2
získal	získat	k5eAaPmAgInS
název	název	k1gInSc1
Allein	Allein	k1gInSc4
(	(	kIx(
<g/>
=	=	kIx~
Samota	samota	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
sloužil	sloužit	k5eAaImAgMnS
k	k	k7c3
loveckých	lovecký	k2eAgInPc2d1
účelům	účel	k1gInPc3
a	a	k8xC
také	také	k9
majitelům	majitel	k1gMnPc3
panství	panství	k1gNnSc4
při	při	k7c6
vyjížďkách	vyjížďka	k1gFnPc6
či	či	k8xC
honech	hon	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
byl	být	k5eAaImAgInS
zkonfiskován	zkonfiskovat	k5eAaPmNgInS
a	a	k8xC
přeměněn	přeměnit	k5eAaPmNgInS
na	na	k7c4
rekreační	rekreační	k2eAgNnSc4d1
středisko	středisko	k1gNnSc4
Státních	státní	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
změny	změna	k1gFnPc4
se	se	k3xPyFc4
zámeček	zámeček	k1gInSc4
dočkal	dočkat	k5eAaPmAgInS
na	na	k7c6
konci	konec	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
na	na	k7c6
bažantnici	bažantnice	k1gFnSc6
a	a	k8xC
jako	jako	k8xC,k8xS
stanice	stanice	k1gFnSc1
chovu	chov	k1gInSc2
bažantů	bažant	k1gMnPc2
a	a	k8xC
koroptví	koroptev	k1gFnPc2
slouží	sloužit	k5eAaImIp3nS
i	i	k9
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jednoduchou	jednoduchý	k2eAgFnSc4d1
obdélnou	obdélný	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
zakončenou	zakončený	k2eAgFnSc7d1
sedlovou	sedlový	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
se	se	k3xPyFc4
štíty	štít	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
obytné	obytný	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
ještě	ještě	k9
o	o	k7c4
něco	něco	k3yInSc4
menší	malý	k2eAgFnSc1d2
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
hospodářské	hospodářský	k2eAgNnSc1d1
a	a	k8xC
technické	technický	k2eAgNnSc1d1
zázemí	zázemí	k1gNnSc1
zámku	zámek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zámek	zámek	k1gInSc1
na	na	k7c4
hrady	hrad	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Zámek	zámek	k1gInSc1
na	na	k7c4
castles	castles	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Archivováno	archivován	k2eAgNnSc1d1
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Zámek	zámek	k1gInSc1
na	na	k7c4
geocaching	geocaching	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
