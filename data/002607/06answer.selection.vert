<s>
Byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgNnPc1d1	založené
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Düsseldorfu	Düsseldorf	k1gInSc6	Düsseldorf
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
dvojicí	dvojice	k1gFnSc7	dvojice
Florian	Florian	k1gMnSc1	Florian
Schneider	Schneider	k1gMnSc1	Schneider
a	a	k8xC	a
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
známější	známý	k2eAgFnSc1d2	známější
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
až	až	k6eAd1	až
jako	jako	k8xS	jako
čtveřice	čtveřice	k1gFnSc1	čtveřice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Wolfgangem	Wolfgang	k1gMnSc7	Wolfgang
Flürem	Flür	k1gMnSc7	Flür
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Bartosem	Bartos	k1gMnSc7	Bartos
<g/>
.	.	kIx.	.
</s>
