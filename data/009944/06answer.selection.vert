<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Linus	Linus	k1gMnSc1	Linus
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
patrně	patrně	k6eAd1	patrně
roku	rok	k1gInSc2	rok
78	[number]	k4	78
či	či	k8xC	či
79	[number]	k4	79
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nástupce	nástupce	k1gMnSc4	nástupce
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
a	a	k8xC	a
tedy	tedy	k9	tedy
za	za	k7c4	za
druhého	druhý	k4xOgMnSc4	druhý
z	z	k7c2	z
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
