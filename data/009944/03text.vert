<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Linus	Linus	k1gMnSc1	Linus
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
patrně	patrně	k6eAd1	patrně
roku	rok	k1gInSc2	rok
78	[number]	k4	78
či	či	k8xC	či
79	[number]	k4	79
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nástupce	nástupce	k1gMnSc4	nástupce
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
a	a	k8xC	a
tedy	tedy	k9	tedy
za	za	k7c4	za
druhého	druhý	k4xOgMnSc4	druhý
z	z	k7c2	z
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
chybějícím	chybějící	k2eAgInPc3d1	chybějící
dobovým	dobový	k2eAgInPc3d1	dobový
pramenům	pramen	k1gInPc3	pramen
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
žádné	žádný	k3yNgFnPc1	žádný
spolehlivé	spolehlivý	k2eAgFnPc1d1	spolehlivá
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
prvně	prvně	k?	prvně
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
římských	římský	k2eAgMnPc2d1	římský
biskupů	biskup	k1gMnPc2	biskup
sv.	sv.	kA	sv.
Ireneus	Ireneus	k1gInSc4	Ireneus
na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
informace	informace	k1gFnPc4	informace
přináší	přinášet	k5eAaImIp3nS	přinášet
Liber	libra	k1gFnPc2	libra
Pontificalis	Pontificalis	k1gFnSc2	Pontificalis
až	až	k9	až
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Církevní	církevní	k2eAgMnPc1d1	církevní
historici	historik	k1gMnPc1	historik
předpokládající	předpokládající	k2eAgFnSc2d1	předpokládající
biskupský	biskupský	k2eAgInSc4d1	biskupský
úřad	úřad	k1gInSc4	úřad
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
dovodit	dovodit	k5eAaPmF	dovodit
bližší	blízký	k2eAgInPc1d2	bližší
údaje	údaj	k1gInPc1	údaj
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kdy	kdy	k6eAd1	kdy
přesně	přesně	k6eAd1	přesně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
papežem	papež	k1gMnSc7	papež
(	(	kIx(	(
<g/>
64	[number]	k4	64
<g/>
–	–	k?	–
<g/>
69	[number]	k4	69
<g/>
)	)	kIx)	)
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přehození	přehození	k1gNnSc3	přehození
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
započaté	započatý	k2eAgFnSc6d1	započatá
sv.	sv.	kA	sv.
Petrem	Petr	k1gMnSc7	Petr
až	až	k6eAd1	až
třetí	třetí	k4xOgInSc4	třetí
nebo	nebo	k8xC	nebo
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nejčastějšího	častý	k2eAgInSc2d3	nejčastější
názoru	názor	k1gInSc2	názor
byl	být	k5eAaImAgInS	být
však	však	k9	však
Linus	Linus	k1gInSc1	Linus
druhým	druhý	k4xOgMnSc7	druhý
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
letech	let	k1gInPc6	let
67	[number]	k4	67
<g/>
–	–	k?	–
<g/>
79	[number]	k4	79
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Linus	Linus	k1gMnSc1	Linus
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
a	a	k8xC	a
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
etruském	etruský	k2eAgNnSc6d1	etruské
městě	město	k1gNnSc6	město
Volaterra	Volaterr	k1gInSc2	Volaterr
v	v	k7c6	v
Toskánsku	Toskánsko	k1gNnSc6	Toskánsko
a	a	k8xC	a
na	na	k7c4	na
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
jej	on	k3xPp3gMnSc4	on
obrátil	obrátit	k5eAaPmAgMnS	obrátit
sv.	sv.	kA	sv.
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Herculanus	Herculanus	k1gMnSc1	Herculanus
dei	dei	k?	dei
Mauri	Maur	k1gFnSc2	Maur
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Claudia	Claudia	k1gFnSc1	Claudia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
svatého	svatý	k2eAgMnSc4d1	svatý
Petra	Petr	k1gMnSc4	Petr
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
a	a	k8xC	a
po	po	k7c6	po
Petrově	Petrův	k2eAgNnSc6d1	Petrovo
ukřižování	ukřižování	k1gNnSc6	ukřižování
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
,	,	kIx,	,
římským	římský	k2eAgMnSc7d1	římský
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíše	nejspíše	k9	nejspíše
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
zmínka	zmínka	k1gFnSc1	zmínka
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
dopise	dopis	k1gInSc6	dopis
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
Timoteovi	Timoteus	k1gMnSc6	Timoteus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
nařízení	nařízení	k1gNnSc3	nařízení
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženy	žena	k1gFnPc1	žena
nemají	mít	k5eNaImIp3nP	mít
přicházet	přicházet	k5eAaImF	přicházet
na	na	k7c4	na
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
bez	bez	k7c2	bez
závoje	závoj	k1gInSc2	závoj
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
autora	autor	k1gMnSc4	autor
apokryfního	apokryfní	k2eAgInSc2d1	apokryfní
spisu	spis	k1gInSc2	spis
Umučení	umučení	k1gNnSc2	umučení
svatých	svatý	k1gMnPc2	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
autorství	autorství	k1gNnSc1	autorství
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pochybné	pochybný	k2eAgNnSc1d1	pochybné
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zavedl	zavést	k5eAaPmAgInS	zavést
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
římských	římský	k2eAgMnPc2d1	římský
biskupů	biskup	k1gMnPc2	biskup
palium	palium	k1gNnSc4	palium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
starší	starý	k2eAgFnSc2d2	starší
tradice	tradice	k1gFnSc2	tradice
zemřel	zemřít	k5eAaPmAgMnS	zemřít
mučednickou	mučednický	k2eAgFnSc7d1	mučednická
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spíše	spíše	k9	spíše
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
vršku	vršek	k1gInSc2	vršek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úcta	úcta	k1gFnSc1	úcta
==	==	k?	==
</s>
</p>
<p>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
papežové	papež	k1gMnPc1	papež
prvních	první	k4xOgNnPc2	první
staletí	staletí	k1gNnPc2	staletí
je	být	k5eAaImIp3nS	být
uctíván	uctíván	k2eAgMnSc1d1	uctíván
jako	jako	k8xC	jako
světec	světec	k1gMnSc1	světec
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
svátek	svátek	k1gInSc1	svátek
je	být	k5eAaImIp3nS	být
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
slaven	slaven	k2eAgInSc1d1	slaven
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
církvi	církev	k1gFnSc6	církev
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1615	[number]	k4	1615
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
objeven	objeven	k2eAgInSc4d1	objeven
náhrobek	náhrobek	k1gInSc4	náhrobek
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
Linus	Linus	k1gMnSc1	Linus
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
po	po	k7c4	po
řadu	řada	k1gFnSc4	řada
let	let	k1gInSc4	let
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
hrob	hrob	k1gInSc4	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnější	pravděpodobný	k2eAgNnSc1d2	pravděpodobnější
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
náhrobek	náhrobek	k1gInSc1	náhrobek
patřil	patřit	k5eAaImAgInS	patřit
jiné	jiný	k2eAgFnSc3d1	jiná
osobě	osoba	k1gFnSc3	osoba
s	s	k7c7	s
delším	dlouhý	k2eAgNnSc7d2	delší
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Linus	Linus	k1gInSc4	Linus
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
koncovka	koncovka	k1gFnSc1	koncovka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Aquilinus	Aquilinus	k1gMnSc1	Aquilinus
<g/>
,	,	kIx,	,
Anullinus	Anullinus	k1gMnSc1	Anullinus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Linus	Linus	k1gInSc1	Linus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
