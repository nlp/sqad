<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
pod	pod	k7c7	pod
Pleší	pleš	k1gFnSc7	pleš
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
výtvarník	výtvarník	k1gMnSc1	výtvarník
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
postavičky	postavička	k1gFnSc2	postavička
Krtečka	krteček	k1gMnSc2	krteček
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
činnost	činnost	k1gFnSc4	činnost
začal	začít	k5eAaPmAgInS	začít
ve	v	k7c6	v
zlínských	zlínský	k2eAgInPc6d1	zlínský
filmových	filmový	k2eAgInPc6d1	filmový
atelierech	atelier	k1gInPc6	atelier
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
barrandovském	barrandovský	k2eAgNnSc6d1	Barrandovské
studiu	studio	k1gNnSc6	studio
Bratři	bratr	k1gMnPc1	bratr
v	v	k7c6	v
triku	trik	k1gInSc6	trik
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jiřího	Jiří	k1gMnSc2	Jiří
Trnky	Trnka	k1gMnSc2	Trnka
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
kudrnatí	kudrnatý	k2eAgMnPc1d1	kudrnatý
malí	malý	k2eAgMnPc1d1	malý
kluci	kluk	k1gMnPc1	kluk
v	v	k7c6	v
námořnických	námořnický	k2eAgNnPc6d1	námořnické
tričkách	tričko	k1gNnPc6	tričko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc2	jeho
dílem	díl	k1gInSc7	díl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
prvním	první	k4xOgInSc7	první
samostatným	samostatný	k2eAgInSc7d1	samostatný
dílem	díl	k1gInSc7	díl
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
animovaný	animovaný	k2eAgInSc1d1	animovaný
příběh	příběh	k1gInSc1	příběh
O	o	k7c6	o
milionáři	milionář	k1gMnSc6	milionář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
slunce	slunce	k1gNnSc4	slunce
na	na	k7c4	na
námět	námět	k1gInSc4	námět
pocházející	pocházející	k2eAgInPc4d1	pocházející
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
proslul	proslout	k5eAaPmAgInS	proslout
animovaným	animovaný	k2eAgInSc7d1	animovaný
seriálem	seriál	k1gInSc7	seriál
o	o	k7c6	o
Krtkovi	krtek	k1gMnSc6	krtek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
velikou	veliký	k2eAgFnSc4d1	veliká
oblibu	obliba	k1gFnSc4	obliba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
49	[number]	k4	49
dílů	díl	k1gInPc2	díl
o	o	k7c6	o
malém	malý	k1gMnSc6	malý
Krtečkovi	krteček	k1gMnSc6	krteček
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
Krtečkovi	krteček	k1gMnSc6	krteček
není	být	k5eNaImIp3nS	být
mluvený	mluvený	k2eAgMnSc1d1	mluvený
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
1	[number]	k4	1
<g/>
.	.	kIx.	.
dílu	díl	k1gInSc2	díl
-	-	kIx~	-
Jak	jak	k8xS	jak
krtek	krtek	k1gMnSc1	krtek
ke	k	k7c3	k
kalhotkám	kalhotky	k1gFnPc3	kalhotky
přišel	přijít	k5eAaPmAgInS	přijít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Krteček	krteček	k1gMnSc1	krteček
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
malí	malý	k2eAgMnPc1d1	malý
kamarádi	kamarád	k1gMnPc1	kamarád
pouze	pouze	k6eAd1	pouze
vydávají	vydávat	k5eAaPmIp3nP	vydávat
zvuky	zvuk	k1gInPc1	zvuk
citoslovce	citoslovce	k1gNnSc1	citoslovce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
hlasové	hlasový	k2eAgInPc1d1	hlasový
záznamy	záznam	k1gInPc1	záznam
Milerových	Milerův	k2eAgFnPc2d1	Milerova
dvou	dva	k4xCgFnPc2	dva
malých	malý	k2eAgFnPc2d1	malá
dcer	dcera	k1gFnPc2	dcera
Kateřinky	Kateřinka	k1gFnSc2	Kateřinka
a	a	k8xC	a
Barborky	Barborka	k1gFnSc2	Barborka
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
bohatou	bohatý	k2eAgFnSc4d1	bohatá
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
činnost	činnost	k1gFnSc4	činnost
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
o	o	k7c4	o
ilustrace	ilustrace	k1gFnPc4	ilustrace
dětských	dětský	k2eAgFnPc2d1	dětská
knížek	knížka	k1gFnPc2	knížka
<g/>
.	.	kIx.	.
</s>
<s>
Ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
např.	např.	kA	např.
i	i	k9	i
Petiškova	Petiškův	k2eAgMnSc4d1	Petiškův
Pohádkového	pohádkový	k2eAgMnSc4d1	pohádkový
dědečka	dědeček	k1gMnSc4	dědeček
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
minulého	minulý	k2eAgInSc2d1	minulý
režimu	režim	k1gInSc2	režim
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
práce	práce	k1gFnPc4	práce
o	o	k7c6	o
Krtečkovi	krteček	k1gMnSc6	krteček
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
jméno	jméno	k1gNnSc4	jméno
spisovateli	spisovatel	k1gMnSc3	spisovatel
Ivanu	Ivan	k1gMnSc3	Ivan
Klímovi	Klíma	k1gMnSc3	Klíma
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Křička	Křička	k1gMnSc1	Křička
Nahrál	nahrát	k5eAaPmAgMnS	nahrát
<g/>
:	:	kIx,	:
Fisyo	Fisyo	k6eAd1	Fisyo
Řídil	řídit	k5eAaImAgMnS	řídit
<g/>
:	:	kIx,	:
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Koníček	koníček	k1gInSc1	koníček
Ruchy	ruch	k1gInPc1	ruch
<g/>
:	:	kIx,	:
Ing.	ing.	kA	ing.
Adolf	Adolf	k1gMnSc1	Adolf
Řípa	Řípa	k1gMnSc1	Řípa
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Jedlička	Jedlička	k1gMnSc1	Jedlička
Komentář	komentář	k1gInSc4	komentář
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
E.	E.	kA	E.
Petiška	Petišek	k1gInSc2	Petišek
Namluvili	namluvit	k5eAaPmAgMnP	namluvit
<g/>
:	:	kIx,	:
P.	P.	kA	P.
Fysela	Fysel	k1gMnSc2	Fysel
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Zinková	zinkový	k2eAgFnSc1d1	zinková
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Filipovský	Filipovský	k1gMnSc1	Filipovský
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Hlinomaz	hlinomaz	k1gMnSc1	hlinomaz
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
A.	A.	kA	A.
Katous	Katous	k1gInSc1	Katous
<g/>
,	,	kIx,	,
Š.	Š.	kA	Š.
Holečková	Holečková	k1gFnSc1	Holečková
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Smitková	Smitková	k1gFnSc1	Smitková
Animovali	animovat	k5eAaBmAgMnP	animovat
<g/>
:	:	kIx,	:
F.	F.	kA	F.
Vystrčil	Vystrčil	k1gMnSc1	Vystrčil
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Lehký	Lehký	k1gMnSc1	Lehký
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Možíšová	Možíšová	k1gFnSc1	Možíšová
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Šedja	Šedj	k1gInSc2	Šedj
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Doubrava	Doubrava	k1gFnSc1	Doubrava
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Štrebl	Štrebl	k1gFnSc4	Štrebl
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Hekrdla	Hekrdla	k1gFnSc4	Hekrdla
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Kurdnáč	Kurdnáč	k1gInSc1	Kurdnáč
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Bárta	Bárta	k1gMnSc1	Bárta
Kreslili	kreslit	k5eAaImAgMnP	kreslit
<g/>
:	:	kIx,	:
V.	V.	kA	V.
Marešová	Marešová	k1gFnSc1	Marešová
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
Skřípková	Skřípková	k1gFnSc1	Skřípková
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Skála	Skála	k1gMnSc1	Skála
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Vlásková	vláskový	k2eAgFnSc1d1	Vlásková
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Bureš	Bureš	k1gMnSc1	Bureš
Dale	Dal	k1gFnSc2	Dal
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
<g/>
:	:	kIx,	:
Z.	Z.	kA	Z.
Najmanová	Najmanová	k1gFnSc1	Najmanová
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Kadera	Kadera	k1gFnSc1	Kadera
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Pošva	Pošva	k?	Pošva
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Pechanová	Pechanová	k1gFnSc1	Pechanová
Vedení	vedení	k1gNnSc4	vedení
štábu	štáb	k1gInSc2	štáb
<g/>
:	:	kIx,	:
I.	I.	kA	I.
Skála	skála	k1gFnSc1	skála
Střih	střih	k1gInSc1	střih
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Šebestíková	Šebestíková	k1gFnSc1	Šebestíková
Zvuk	zvuk	k1gInSc1	zvuk
<g/>
:	:	kIx,	:
Fr.	Fr.	k1gMnSc1	Fr.
Černý	Černý	k1gMnSc1	Černý
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Masník	Masník	k1gInSc4	Masník
Výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
spolupráce	spolupráce	k1gFnSc1	spolupráce
<g/>
:	:	kIx,	:
R.	R.	kA	R.
Holan	Holan	k1gMnSc1	Holan
Dialogu	dialog	k1gInSc2	dialog
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Miloslav	Miloslav	k1gMnSc1	Miloslav
Jareš	Jareš	k1gMnSc1	Jareš
Vedoucí	vedoucí	k1gMnSc1	vedoucí
výrobníko	výrobníko	k6eAd1	výrobníko
štábu	štáb	k1gInSc2	štáb
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Tokstin	Tokstin	k1gMnSc1	Tokstin
Vedoucí	vedoucí	k1gMnSc1	vedoucí
výroby	výroba	k1gFnSc2	výroba
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Hejl	Hejl	k1gMnSc1	Hejl
Námět	námět	k1gInSc1	námět
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
práce	práce	k1gFnSc1	práce
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Zdenĕ	Zdenĕ	k1gMnSc1	Zdenĕ
Miler	Miler	k1gMnSc1	Miler
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
ocenění	ocenění	k1gNnSc4	ocenění
Benátského	benátský	k2eAgInSc2d1	benátský
lva	lev	k1gInSc2	lev
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
festivalu	festival	k1gInSc2	festival
Montevideo	Montevideo	k1gNnSc1	Montevideo
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
Medaile	medaile	k1gFnSc1	medaile
Za	za	k7c2	za
zásluhy	zásluha	k1gFnSc2	zásluha
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
Série	série	k1gFnSc2	série
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
o	o	k7c6	o
Krtkovi	krtek	k1gMnSc6	krtek
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
krtek	krtek	k1gMnSc1	krtek
ke	k	k7c3	k
kalhotkám	kalhotky	k1gFnPc3	kalhotky
přišel	přijít	k5eAaPmAgInS	přijít
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
autíčko	autíčko	k1gNnSc4	autíčko
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
raketa	raketa	k1gFnSc1	raketa
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
tranzistor	tranzistor	k1gInSc1	tranzistor
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
žvýkačka	žvýkačka	k1gFnSc1	žvýkačka
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
zahradníkem	zahradník	k1gMnSc7	zahradník
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
ježek	ježek	k1gMnSc1	ježek
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
lízátko	lízátko	k1gNnSc1	lízátko
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
televizor	televizor	k1gInSc1	televizor
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
paraplíčko	paraplíčko	k?	paraplíčko
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
malířem	malíř	k1gMnSc7	malíř
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
muzika	muzika	k1gFnSc1	muzika
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
telefon	telefon	k1gInSc1	telefon
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
zápalky	zápalka	k1gFnPc1	zápalka
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
chemikem	chemik	k1gMnSc7	chemik
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
koberec	koberec	k1gInSc1	koberec
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
hodinářem	hodinář	k1gMnSc7	hodinář
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
buldozer	buldozer	k1gInSc1	buldozer
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
vejce	vejce	k1gNnPc1	vejce
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
fotografem	fotograf	k1gMnSc7	fotograf
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
o	o	k7c6	o
vánocích	vánoce	k1gFnPc6	vánoce
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
karneval	karneval	k1gInSc1	karneval
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
medicína	medicína	k1gFnSc1	medicína
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
filmová	filmový	k2eAgFnSc1d1	filmová
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
orel	orel	k1gMnSc1	orel
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
hodiny	hodina	k1gFnPc1	hodina
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
kachničky	kachnička	k1gFnPc1	kachnička
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
kamarádi	kamarád	k1gMnPc1	kamarád
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
oslava	oslava	k1gFnSc1	oslava
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
robot	robot	k1gMnSc1	robot
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
uhlí	uhlí	k1gNnSc4	uhlí
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
weekend	weekend	k1gInSc1	weekend
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
houby	houba	k1gFnPc1	houba
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
maminka	maminka	k1gFnSc1	maminka
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
metro	metro	k1gNnSc1	metro
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
myška	myška	k1gFnSc1	myška
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
sněhulák	sněhulák	k1gMnSc1	sněhulák
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
zajíček	zajíček	k1gMnSc1	zajíček
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
pramen	pramen	k1gInSc1	pramen
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
šťoura	šťoura	k1gMnSc1	šťoura
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
flétna	flétna	k1gFnSc1	flétna
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
rybka	rybka	k1gFnSc1	rybka
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
žabka	žabka	k1gFnSc1	žabka
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Série	série	k1gFnSc1	série
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
o	o	k7c6	o
Cvrčkovi	Cvrček	k1gMnSc6	Cvrček
<g/>
.	.	kIx.	.
</s>
<s>
Cvrček	Cvrček	k1gMnSc1	Cvrček
a	a	k8xC	a
housličky	housličky	k1gFnPc1	housličky
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Cvrček	Cvrček	k1gMnSc1	Cvrček
a	a	k8xC	a
pavouk	pavouk	k1gMnSc1	pavouk
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Cvrček	Cvrček	k1gMnSc1	Cvrček
a	a	k8xC	a
stroj	stroj	k1gInSc1	stroj
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Cvrček	Cvrček	k1gMnSc1	Cvrček
a	a	k8xC	a
slepice	slepice	k1gFnSc1	slepice
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Cvrček	Cvrček	k1gMnSc1	Cvrček
a	a	k8xC	a
pila	pila	k1gFnSc1	pila
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Cvrček	Cvrček	k1gMnSc1	Cvrček
a	a	k8xC	a
basa	basa	k1gFnSc1	basa
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Cvrček	Cvrček	k1gMnSc1	Cvrček
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
bombardón	bombardón	k1gInSc1	bombardón
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
štěňátko	štěňátko	k1gNnSc1	štěňátko
dostalo	dostat	k5eAaPmAgNnS	dostat
chuť	chuť	k1gFnSc4	chuť
na	na	k7c4	na
med	med	k1gInSc4	med
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
štěňátko	štěňátko	k1gNnSc1	štěňátko
chtělo	chtít	k5eAaImAgNnS	chtít
malé	malý	k2eAgNnSc1d1	malé
pejsky	pejsek	k1gMnPc7	pejsek
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
sluníčko	sluníčko	k1gNnSc1	sluníčko
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
štěňátku	štěňátko	k1gNnSc6	štěňátko
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
O	o	k7c6	o
milionáři	milionář	k1gMnSc6	milionář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
slunce	slunce	k1gNnSc4	slunce
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Jiřího	Jiří	k1gMnSc2	Jiří
<g />
.	.	kIx.	.
</s>
<s>
Wolkera	Wolker	k1gMnSc4	Wolker
<g/>
)	)	kIx)	)
O	o	k7c6	o
kohoutkovi	kohoutek	k1gMnSc6	kohoutek
a	a	k8xC	a
slepičce	slepička	k1gFnSc6	slepička
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
Měsíční	měsíční	k2eAgFnSc1d1	měsíční
pohádka	pohádka	k1gFnSc1	pohádka
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Modrý	modrý	k2eAgMnSc1d1	modrý
kocourek	kocourek	k1gMnSc1	kocourek
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
O	o	k7c6	o
nejbohatším	bohatý	k2eAgMnSc6d3	nejbohatší
vrabci	vrabec	k1gMnSc6	vrabec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
O	o	k7c6	o
Čtverečkovi	Čtvereček	k1gMnSc6	Čtvereček
a	a	k8xC	a
Trojúhelníčkovi	Trojúhelníček	k1gMnSc6	Trojúhelníček
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Sametka	sametka	k1gFnSc1	sametka
1967	[number]	k4	1967
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Das	Das	k1gFnSc1	Das
Samtkropfband	Samtkropfbanda	k1gFnPc2	Samtkropfbanda
<g/>
)	)	kIx)	)
O	o	k7c6	o
Červené	Červené	k2eAgFnSc6d1	Červené
karkulce	karkulka	k1gFnSc6	karkulka
Rudá	rudý	k2eAgFnSc1d1	rudá
stopa	stopa	k1gFnSc1	stopa
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Romance	romance	k1gFnSc1	romance
helgolandská	helgolandský	k2eAgFnSc1d1	helgolandská
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
O	o	k7c6	o
veselé	veselý	k2eAgFnSc6d1	veselá
mašince	mašinka	k1gFnSc6	mašinka
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Čarek	Čarek	k1gMnSc1	Čarek
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
??	??	k?	??
<g/>
)	)	kIx)	)
Cesty	cesta	k1gFnPc1	cesta
formana	forman	k1gMnSc2	forman
Šejtročka	Šejtročka	k1gFnSc1	Šejtročka
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Čtvrtek	čtvrtka	k1gFnPc2	čtvrtka
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Kubula	Kubula	k1gFnSc1	Kubula
a	a	k8xC	a
Kuba	Kuba	k1gFnSc1	Kuba
Kubikula	Kubikula	k1gFnSc1	Kubikula
(	(	kIx(	(
<g/>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
)	)	kIx)	)
Pohádkový	pohádkový	k2eAgMnSc1d1	pohádkový
dědeček	dědeček	k1gMnSc1	dědeček
(	(	kIx(	(
<g/>
Eduard	Eduard	k1gMnSc1	Eduard
Petiška	Petišek	k1gMnSc2	Petišek
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
krtek	krtek	k1gMnSc1	krtek
cestoval	cestovat	k5eAaImAgMnS	cestovat
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Brukner	Brukner	k1gMnSc1	Brukner
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
