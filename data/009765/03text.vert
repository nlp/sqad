<p>
<s>
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Lukáše	Lukáš	k1gMnSc2	Lukáš
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
L	L	kA	L
nebo	nebo	k8xC	nebo
Lk	Lk	k1gFnSc1	Lk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
kniha	kniha	k1gFnSc1	kniha
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
kanonických	kanonický	k2eAgNnPc2d1	kanonické
evangelií	evangelium	k1gNnPc2	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Matoušovým	Matoušův	k2eAgNnSc7d1	Matoušovo
a	a	k8xC	a
Markovým	Markův	k2eAgNnSc7d1	Markovo
evangeliem	evangelium	k1gNnSc7	evangelium
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
tzv.	tzv.	kA	tzv.
synoptická	synoptický	k2eAgNnPc4d1	synoptické
evangelia	evangelium	k1gNnPc4	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Lukášovo	Lukášův	k2eAgNnSc1d1	Lukášovo
evangelium	evangelium	k1gNnSc1	evangelium
i	i	k8xC	i
Skutky	skutek	k1gInPc1	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
navazují	navazovat	k5eAaImIp3nP	navazovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
věnovány	věnovat	k5eAaPmNgFnP	věnovat
jistému	jistý	k2eAgMnSc3d1	jistý
Teofilovi	Teofil	k1gMnSc3	Teofil
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgInS	moct
učinit	učinit	k5eAaPmF	učinit
přesnější	přesný	k2eAgInSc1d2	přesnější
obraz	obraz	k1gInSc1	obraz
o	o	k7c6	o
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgInPc6	který
již	již	k6eAd1	již
slyšel	slyšet	k5eAaImAgMnS	slyšet
mnoho	mnoho	k4c4	mnoho
nejednotných	jednotný	k2eNgFnPc2d1	nejednotná
zpráv	zpráva	k1gFnPc2	zpráva
(	(	kIx(	(
<g/>
Lk	Lk	k1gFnPc2	Lk
1	[number]	k4	1
<g/>
,	,	kIx,	,
1	[number]	k4	1
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Autor	autor	k1gMnSc1	autor
a	a	k8xC	a
původ	původ	k1gMnSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
dílem	díl	k1gInSc7	díl
vzdělaného	vzdělaný	k2eAgMnSc2d1	vzdělaný
člověka	člověk	k1gMnSc2	člověk
s	s	k7c7	s
dobrou	dobrý	k2eAgFnSc7d1	dobrá
řečtinou	řečtina	k1gFnSc7	řečtina
a	a	k8xC	a
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
slovní	slovní	k2eAgFnSc7d1	slovní
zásobou	zásoba	k1gFnSc7	zásoba
než	než	k8xS	než
mají	mít	k5eAaImIp3nP	mít
ostatní	ostatní	k2eAgNnPc4d1	ostatní
tři	tři	k4xCgNnPc4	tři
evangelia	evangelium	k1gNnPc4	evangelium
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Evangelium	evangelium	k1gNnSc1	evangelium
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
kniha	kniha	k1gFnSc1	kniha
Skutků	skutek	k1gInPc2	skutek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
(	(	kIx(	(
<g/>
Sk	Sk	kA	Sk
1	[number]	k4	1
<g/>
,	,	kIx,	,
1	[number]	k4	1
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
tradice	tradice	k1gFnSc1	tradice
připisuje	připisovat	k5eAaImIp3nS	připisovat
spis	spis	k1gInSc4	spis
lékaři	lékař	k1gMnSc3	lékař
Lukášovi	Lukáš	k1gMnSc3	Lukáš
(	(	kIx(	(
<g/>
Kol	Kola	k1gFnPc2	Kola
4	[number]	k4	4
<g/>
,	,	kIx,	,
14	[number]	k4	14
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
evangeliu	evangelium	k1gNnSc6	evangelium
není	být	k5eNaImIp3nS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
písemným	písemný	k2eAgInSc7d1	písemný
dokladem	doklad	k1gInSc7	doklad
je	být	k5eAaImIp3nS	být
Zlomek	zlomek	k1gInSc1	zlomek
Muratoriho	Muratori	k1gMnSc2	Muratori
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
200	[number]	k4	200
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lukášovi	Lukášův	k2eAgMnPc1d1	Lukášův
je	být	k5eAaImIp3nS	být
přičítá	přičítat	k5eAaImIp3nS	přičítat
také	také	k9	také
Órigenés	Órigenés	k1gInSc1	Órigenés
<g/>
,	,	kIx,	,
Tertullian	Tertullian	k1gMnSc1	Tertullian
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
téže	tenže	k3xDgFnSc2	tenže
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
apoštola	apoštol	k1gMnSc4	apoštol
Pavla	Pavel	k1gMnSc4	Pavel
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
hovořit	hovořit	k5eAaImF	hovořit
s	s	k7c7	s
očitými	očitý	k2eAgMnPc7d1	očitý
svědky	svědek	k1gMnPc7	svědek
a	a	k8xC	a
nahlížet	nahlížet	k5eAaImF	nahlížet
do	do	k7c2	do
písemných	písemný	k2eAgInPc2d1	písemný
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
tradice	tradice	k1gFnSc1	tradice
usuzovala	usuzovat	k5eAaImAgFnS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
evangelium	evangelium	k1gNnSc1	evangelium
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nejspíše	nejspíše	k9	nejspíše
v	v	k7c6	v
Kaisareii	Kaisareie	k1gFnSc6	Kaisareie
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tamějšího	tamější	k2eAgNnSc2d1	tamější
Pavlova	Pavlův	k2eAgNnSc2d1	Pavlovo
vězení	vězení	k1gNnSc2	vězení
(	(	kIx(	(
<g/>
asi	asi	k9	asi
v	v	k7c6	v
letech	let	k1gInPc6	let
56-58	[number]	k4	56-58
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
zachované	zachovaný	k2eAgInPc1d1	zachovaný
rukopisy	rukopis	k1gInPc1	rukopis
(	(	kIx(	(
<g/>
papyrus	papyrus	k1gInSc1	papyrus
P	P	kA	P
<g/>
75	[number]	k4	75
<g/>
,	,	kIx,	,
Sinajský	sinajský	k2eAgInSc1d1	sinajský
a	a	k8xC	a
vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
kodex	kodex	k1gInSc1	kodex
<g/>
)	)	kIx)	)
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
nadpis	nadpis	k1gInSc4	nadpis
"	"	kIx"	"
<g/>
podle	podle	k7c2	podle
Lukáše	Lukáš	k1gMnSc2	Lukáš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
téměř	téměř	k6eAd1	téměř
jistě	jistě	k9	jistě
používal	používat	k5eAaImAgInS	používat
text	text	k1gInSc1	text
evangelia	evangelium	k1gNnSc2	evangelium
Markova	Markův	k2eAgNnSc2d1	Markovo
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
psal	psát	k5eAaImAgInS	psát
až	až	k9	až
po	po	k7c6	po
zboření	zboření	k1gNnSc6	zboření
Jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
chrámu	chrám	k1gInSc2	chrám
roku	rok	k1gInSc2	rok
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
srovnání	srovnání	k1gNnSc2	srovnání
s	s	k7c7	s
evangeliem	evangelium	k1gNnSc7	evangelium
Matoušovým	Matoušův	k2eAgInSc7d1	Matoušův
soudí	soudit	k5eAaImIp3nS	soudit
biblická	biblický	k2eAgFnSc1d1	biblická
kritika	kritika	k1gFnSc1	kritika
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
používali	používat	k5eAaImAgMnP	používat
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
sbírku	sbírka	k1gFnSc4	sbírka
Ježíšových	Ježíšův	k2eAgInPc2d1	Ježíšův
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývá	nazývat	k5eAaImIp3nS	nazývat
Q	Q	kA	Q
(	(	kIx(	(
<g/>
z	z	k7c2	z
něm.	něm.	k?	něm.
Quelle	Quelle	k1gInSc1	Quelle
<g/>
,	,	kIx,	,
pramen	pramen	k1gInSc1	pramen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgMnPc2d1	moderní
badatelů	badatel	k1gMnPc2	badatel
proto	proto	k8xC	proto
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spis	spis	k1gInSc1	spis
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
staré	starý	k2eAgFnSc2d1	stará
tradice	tradice	k1gFnSc2	tradice
psal	psát	k5eAaImAgMnS	psát
Lukáš	Lukáš	k1gMnSc1	Lukáš
své	svůj	k3xOyFgNnSc4	svůj
evangelium	evangelium	k1gNnSc4	evangelium
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyprávění	vyprávění	k1gNnSc2	vyprávění
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnPc4	charakteristikon
knihy	kniha	k1gFnSc2	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
zřejmě	zřejmě	k6eAd1	zřejmě
psal	psát	k5eAaImAgMnS	psát
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc2	křesťanství
konvertovali	konvertovat	k5eAaBmAgMnP	konvertovat
z	z	k7c2	z
pohanství	pohanství	k1gNnSc2	pohanství
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
díky	díky	k7c3	díky
Pavlově	Pavlův	k2eAgFnSc3d1	Pavlova
reinterpretaci	reinterpretace	k1gFnSc3	reinterpretace
nepředstavovalo	představovat	k5eNaImAgNnS	představovat
žádný	žádný	k3yNgInSc4	žádný
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
řečtina	řečtina	k1gFnSc1	řečtina
je	být	k5eAaImIp3nS	být
uhlazenější	uhlazený	k2eAgFnSc1d2	uhlazenější
než	než	k8xS	než
u	u	k7c2	u
druhých	druhý	k4xOgNnPc2	druhý
evangelií	evangelium	k1gNnPc2	evangelium
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
několik	několik	k4yIc1	několik
latinských	latinský	k2eAgNnPc2d1	latinské
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
žádná	žádný	k3yNgFnSc1	žádný
hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
1151	[number]	k4	1151
veršů	verš	k1gInPc2	verš
Lukášova	Lukášův	k2eAgNnSc2d1	Lukášovo
evangelia	evangelium	k1gNnSc2	evangelium
je	být	k5eAaImIp3nS	být
544	[number]	k4	544
vlastních	vlastní	k2eAgMnPc2d1	vlastní
<g/>
,	,	kIx,	,
565	[number]	k4	565
téměř	téměř	k6eAd1	téměř
totožných	totožný	k2eAgMnPc2d1	totožný
s	s	k7c7	s
Mt	Mt	k1gFnSc7	Mt
<g/>
,	,	kIx,	,
430	[number]	k4	430
s	s	k7c7	s
Mk	Mk	k1gFnSc7	Mk
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
389	[number]	k4	389
je	on	k3xPp3gMnPc4	on
společných	společný	k2eAgMnPc6d1	společný
všem	všecek	k3xTgNnPc3	všecek
třem	tři	k4xCgFnPc3	tři
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
uvádí	uvádět	k5eAaImIp3nS	uvádět
17	[number]	k4	17
Ježíšových	Ježíšův	k2eAgNnPc2d1	Ježíšovo
podobenství	podobenství	k1gNnPc2	podobenství
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
evangeliích	evangelium	k1gNnPc6	evangelium
chybí	chybit	k5eAaPmIp3nP	chybit
<g/>
,	,	kIx,	,
a	a	k8xC	a
28	[number]	k4	28
zřetelných	zřetelný	k2eAgInPc2d1	zřetelný
citátů	citát	k1gInPc2	citát
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
podle	podle	k7c2	podle
řeckého	řecký	k2eAgNnSc2d1	řecké
znění	znění	k1gNnSc2	znění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lukášovo	Lukášův	k2eAgNnSc1d1	Lukášovo
evangelium	evangelium	k1gNnSc1	evangelium
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velkou	velký	k2eAgFnSc7d1	velká
pozorností	pozornost	k1gFnSc7	pozornost
k	k	k7c3	k
chudým	chudý	k1gMnPc3	chudý
<g/>
,	,	kIx,	,
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
a	a	k8xC	a
dětem	dítě	k1gFnPc3	dítě
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Lukášově	Lukášův	k2eAgNnSc6d1	Lukášovo
evangeliu	evangelium	k1gNnSc6	evangelium
je	být	k5eAaImIp3nS	být
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
Zvěstování	zvěstování	k1gNnSc6	zvěstování
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
<g/>
,	,	kIx,	,
o	o	k7c6	o
pastýřích	pastýř	k1gMnPc6	pastýř
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
<g/>
,	,	kIx,	,
o	o	k7c6	o
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
dětství	dětství	k1gNnSc6	dětství
<g/>
,	,	kIx,	,
podobenství	podobenství	k1gNnSc4	podobenství
o	o	k7c6	o
milosrdném	milosrdný	k2eAgMnSc6d1	milosrdný
Samaritánovi	Samaritán	k1gMnSc6	Samaritán
<g/>
,	,	kIx,	,
o	o	k7c6	o
ztraceném	ztracený	k2eAgMnSc6d1	ztracený
synu	syn	k1gMnSc6	syn
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Citáty	citát	k1gInPc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Gospel	gospel	k1gInSc1	gospel
of	of	k?	of
Luke	Luk	k1gFnSc2	Luk
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
CARSON	CARSON	kA	CARSON
<g/>
,	,	kIx,	,
D.	D.	kA	D.
A	A	kA	A
<g/>
;	;	kIx,	;
MOO	MOO	kA	MOO
<g/>
,	,	kIx,	,
Douglas	Douglas	k1gInSc1	Douglas
J.	J.	kA	J.
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
700	[number]	k4	700
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7255	[number]	k4	7255
<g/>
-	-	kIx~	-
<g/>
165	[number]	k4	165
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DOUGLAS	DOUGLAS	kA	DOUGLAS
<g/>
,	,	kIx,	,
J.	J.	kA	J.
D	D	kA	D
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
biblický	biblický	k2eAgInSc1d1	biblický
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
1243	[number]	k4	1243
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7255	[number]	k4	7255
<g/>
-	-	kIx~	-
<g/>
193	[number]	k4	193
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POKORNÝ	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgInSc4d1	literární
a	a	k8xC	a
teologický	teologický	k2eAgInSc4d1	teologický
úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
333	[number]	k4	333
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
52	[number]	k4	52
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
H.	H.	kA	H.
Haag	Haag	k1gInSc1	Haag
<g/>
,	,	kIx,	,
Bibellexikon	Bibellexikon	k1gInSc1	Bibellexikon
<g/>
.	.	kIx.	.
</s>
<s>
Einsiedeln	Einsiedeln	k1gInSc1	Einsiedeln
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Art	Art	k?	Art
<g/>
.	.	kIx.	.
</s>
<s>
Lukasevangelium	Lukasevangelium	k1gNnSc1	Lukasevangelium
<g/>
,	,	kIx,	,
col	cola	k1gFnPc2	cola
<g/>
.	.	kIx.	.
1065	[number]	k4	1065
<g/>
-	-	kIx~	-
<g/>
1069	[number]	k4	1069
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Hengel	Hengel	k1gMnSc1	Hengel
<g/>
,	,	kIx,	,
Evangelista	evangelista	k1gMnSc1	evangelista
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
L.	L.	kA	L.
T.	T.	kA	T.
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Lukáše	Lukáš	k1gMnSc2	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
P.	P.	kA	P.
<g/>
-	-	kIx~	-
<g/>
G.	G.	kA	G.
Müller	Müller	k1gInSc1	Müller
<g/>
,	,	kIx,	,
Evangelium	evangelium	k1gNnSc1	evangelium
sv.	sv.	kA	sv.
Lukáše	Lukáš	k1gMnSc2	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
</s>
</p>
<p>
<s>
Skutky	skutek	k1gInPc1	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
</s>
</p>
<p>
<s>
Synoptické	synoptický	k2eAgNnSc1d1	synoptické
evangelium	evangelium	k1gNnSc1	evangelium
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Lukáše	Lukáš	k1gMnSc2	Lukáš
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
překlad	překlad	k1gInSc1	překlad
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
jako	jako	k8xC	jako
.	.	kIx.	.
<g/>
txt	txt	k?	txt
nebo	nebo	k8xC	nebo
.	.	kIx.	.
<g/>
doc	doc	kA	doc
</s>
</p>
<p>
<s>
ČESKÝ	český	k2eAgInSc1d1	český
STUDIJNÍ	studijní	k2eAgInSc1d1	studijní
PŘEKLAD	překlad	k1gInSc1	překlad
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
česky	česky	k6eAd1	česky
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
</s>
</p>
<p>
<s>
Vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
a	a	k8xC	a
konkordance	konkordance	k1gFnSc1	konkordance
k	k	k7c3	k
Českému	český	k2eAgInSc3d1	český
studijnímu	studijní	k2eAgInSc3d1	studijní
překladu	překlad	k1gInSc3	překlad
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
A.	A.	kA	A.
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Biblický	biblický	k2eAgInSc1d1	biblický
slovník	slovník	k1gInSc1	slovník
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
doc	doc	kA	doc
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
jpg	jpg	k?	jpg
nebo	nebo	k8xC	nebo
.	.	kIx.	.
<g/>
pdf	pdf	k?	pdf
<g/>
)	)	kIx)	)
</s>
</p>
