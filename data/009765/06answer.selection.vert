<s>
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Lukáše	Lukáš	k1gMnSc2	Lukáš
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
L	L	kA	L
nebo	nebo	k8xC	nebo
Lk	Lk	k1gFnSc1	Lk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
kniha	kniha	k1gFnSc1	kniha
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
kanonických	kanonický	k2eAgNnPc2d1	kanonické
evangelií	evangelium	k1gNnPc2	evangelium
<g/>
.	.	kIx.	.
</s>
