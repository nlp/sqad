<p>
<s>
Ed	Ed	k?	Ed
Wubbe	Wubb	k1gMnSc5	Wubb
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
choreograf	choreograf	k1gMnSc1	choreograf
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
choreografie	choreografie	k1gFnSc2	choreografie
k	k	k7c3	k
baletu	balet	k1gInSc3	balet
Nico	Nico	k6eAd1	Nico
pojednávajícím	pojednávající	k2eAgInSc7d1	pojednávající
o	o	k7c6	o
německé	německý	k2eAgFnSc6d1	německá
zpěvačce	zpěvačka	k1gFnSc6	zpěvačka
<g/>
,	,	kIx,	,
modelce	modelka	k1gFnSc6	modelka
a	a	k8xC	a
herečce	herečka	k1gFnSc6	herečka
Nico	Nico	k6eAd1	Nico
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Calea	Cale	k1gInSc2	Cale
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
baletu	balet	k1gInSc3	balet
vyšla	vyjít	k5eAaPmAgFnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dance	Danka	k1gFnSc3	Danka
Music	Musice	k1gInPc2	Musice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Marco	Marco	k6eAd1	Marco
Goeckem	Goecko	k1gNnSc7	Goecko
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
choreografii	choreografie	k1gFnSc4	choreografie
k	k	k7c3	k
baletu	balet	k1gInSc3	balet
Songs	Songsa	k1gFnPc2	Songsa
for	forum	k1gNnPc2	forum
Drella	Drello	k1gNnSc2	Drello
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
námětem	námět	k1gInSc7	námět
bylo	být	k5eAaImAgNnS	být
stejnojmenné	stejnojmenný	k2eAgNnSc1d1	stejnojmenné
album	album	k1gNnSc1	album
Johna	John	k1gMnSc2	John
Calea	Caleus	k1gMnSc2	Caleus
a	a	k8xC	a
Lou	Lou	k1gMnSc2	Lou
Reeda	Reed	k1gMnSc2	Reed
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
Ed	Ed	k?	Ed
Wubbe	Wubb	k1gMnSc5	Wubb
v	v	k7c6	v
Internet	Internet	k1gInSc1	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
)	)	kIx)	)
Rozhovor	rozhovor	k1gInSc1	rozhovor
</s>
</p>
