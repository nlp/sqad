<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
univerzální	univerzální	k2eAgInSc4d1	univerzální
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
splnit	splnit	k5eAaPmF	splnit
všechny	všechen	k3xTgInPc4	všechen
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
požadavků	požadavek	k1gInPc2	požadavek
je	být	k5eAaImIp3nS	být
protichůdných	protichůdný	k2eAgFnPc2d1	protichůdná
<g/>
.	.	kIx.	.
</s>
