<s>
Počátky	počátek	k1gInPc1	počátek
surrealismu	surrealismus	k1gInSc2	surrealismus
navazovaly	navazovat	k5eAaImAgInP	navazovat
na	na	k7c4	na
dadaismus	dadaismus	k1gInSc4	dadaismus
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
přišlo	přijít	k5eAaPmAgNnS	přijít
mnoho	mnoho	k4c1	mnoho
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
surrealismus	surrealismus	k1gInSc4	surrealismus
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
širší	široký	k2eAgInSc4d2	širší
rozsah	rozsah	k1gInSc4	rozsah
<g/>
.	.	kIx.	.
</s>
