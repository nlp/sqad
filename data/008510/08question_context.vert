<s>
Španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
kastilština	kastilština	k1gFnSc1	kastilština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgInPc2d3	nejrozšířenější
světových	světový	k2eAgInPc2d1	světový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
