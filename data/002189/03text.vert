<s>
Memphis	Memphis	k1gInSc1	Memphis
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Tennessee	Tennessee	k1gFnPc2	Tennessee
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
Shelby	Shelba	k1gFnSc2	Shelba
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Tennessee	Tennesse	k1gFnSc2	Tennesse
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
velký	velký	k2eAgInSc1d1	velký
říční	říční	k2eAgInSc1d1	říční
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
763,4	[number]	k4	763,4
km2	km2	k4	km2
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
tu	tu	k6eAd1	tu
žilo	žít	k5eAaImAgNnS	žít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
680	[number]	k4	680
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
17	[number]	k4	17
<g/>
.	.	kIx.	.
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
celých	celá	k1gFnPc2	celá
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Memphisu	Memphis	k1gInSc2	Memphis
s	s	k7c7	s
aglomerací	aglomerace	k1gFnSc7	aglomerace
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
1,23	[number]	k4	1,23
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
v	v	k7c6	v
Tennessee	Tennessee	k1gFnSc6	Tennessee
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
oblasti	oblast	k1gFnSc6	oblast
města	město	k1gNnSc2	město
Nashville	Nashville	k1gNnSc2	Nashville
<g/>
.	.	kIx.	.
</s>
<s>
Memphis	Memphis	k1gInSc1	Memphis
je	být	k5eAaImIp3nS	být
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
významných	významný	k2eAgNnPc2d1	významné
měst	město	k1gNnPc2	město
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
největších	veliký	k2eAgInPc2d3	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
staroegyptském	staroegyptský	k2eAgNnSc6d1	staroegyptské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Memfisu	Memfis	k1gInSc2	Memfis
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1968	[number]	k4	1968
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
balkóně	balkón	k1gInSc6	balkón
motelu	motel	k1gInSc2	motel
Lorraine	Lorrain	k1gInSc5	Lorrain
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1977	[number]	k4	1977
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
koupelny	koupelna	k1gFnSc2	koupelna
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Graceland	Graceland	k1gInSc1	Graceland
král	král	k1gMnSc1	král
rock	rock	k1gInSc1	rock
and	and	k?	and
rollu	rollat	k5eAaPmIp1nS	rollat
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
646	[number]	k4	646
889	[number]	k4	889
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
29,4	[number]	k4	29,4
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
63,3	[number]	k4	63,3
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
1,6	[number]	k4	1,6
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
4,0	[number]	k4	4,0
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
1,4	[number]	k4	1,4
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
6,5	[number]	k4	6,5
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiná	jiný	k2eAgNnPc4d1	jiné
velká	velký	k2eAgNnPc4d1	velké
města	město	k1gNnPc4	město
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
jihu	jih	k1gInSc6	jih
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc1	město
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
násilnou	násilný	k2eAgFnSc7d1	násilná
kriminalitou	kriminalita	k1gFnSc7	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
Memphis	Memphis	k1gInSc1	Memphis
vyhodnocen	vyhodnotit	k5eAaPmNgInS	vyhodnotit
jako	jako	k9	jako
4	[number]	k4	4
<g/>
.	.	kIx.	.
nejnebezpečnější	bezpečný	k2eNgNnSc1d3	nejnebezpečnější
město	město	k1gNnSc1	město
v	v	k7c6	v
USA	USA	kA	USA
nad	nad	k7c7	nad
500	[number]	k4	500
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
klub	klub	k1gInSc1	klub
Memphis	Memphis	k1gFnPc2	Memphis
Grizzlies	Grizzlies	k1gInSc1	Grizzlies
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
basketbalovou	basketbalový	k2eAgFnSc4d1	basketbalová
soutěž	soutěž	k1gFnSc4	soutěž
NBA	NBA	kA	NBA
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
k	k	k7c3	k
domácím	domácí	k2eAgNnPc3d1	domácí
utkáním	utkání	k1gNnPc3	utkání
v	v	k7c6	v
aréně	aréna	k1gFnSc6	aréna
FedExForum	FedExForum	k1gInSc4	FedExForum
o	o	k7c6	o
kapacitě	kapacita	k1gFnSc6	kapacita
18.119	[number]	k4	18.119
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
přestěhován	přestěhovat	k5eAaPmNgInS	přestěhovat
z	z	k7c2	z
Vancouveru	Vancouver	k1gInSc2	Vancouver
a	a	k8xC	a
na	na	k7c4	na
větší	veliký	k2eAgInPc4d2	veliký
úspěchy	úspěch	k1gInPc4	úspěch
zatím	zatím	k6eAd1	zatím
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
v	v	k7c6	v
Memphisu	Memphis	k1gInSc6	Memphis
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
pouze	pouze	k6eAd1	pouze
párkrát	párkrát	k6eAd1	párkrát
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
playoff	playoff	k1gInSc4	playoff
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
vždy	vždy	k6eAd1	vždy
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Mwata	Mwata	k1gFnSc1	Mwata
Bowden	Bowdna	k1gFnPc2	Bowdna
-	-	kIx~	-
saxofonista	saxofonista	k1gMnSc1	saxofonista
a	a	k8xC	a
klarinetista	klarinetista	k1gMnSc1	klarinetista
Mary	Mary	k1gFnSc2	Mary
Carr	Carr	k1gMnSc1	Carr
Moore	Moor	k1gInSc5	Moor
-	-	kIx~	-
skladatelka	skladatelka	k1gFnSc1	skladatelka
Shannen	Shannen	k1gInSc1	Shannen
Doherty	Dohert	k1gInPc1	Dohert
-	-	kIx~	-
herečka	herečka	k1gFnSc1	herečka
Jerry	Jerra	k1gFnSc2	Jerra
Lawler	Lawler	k1gInSc1	Lawler
-	-	kIx~	-
profesionální	profesionální	k2eAgInSc1d1	profesionální
wrestler	wrestler	k1gInSc1	wrestler
Frank	Frank	k1gMnSc1	Frank
Lowe	Lowe	k1gInSc1	Lowe
-	-	kIx~	-
jazzový	jazzový	k2eAgMnSc1d1	jazzový
saxofonista	saxofonista	k1gMnSc1	saxofonista
Dan	Dan	k1gMnSc1	Dan
Schneider	Schneider	k1gMnSc1	Schneider
-	-	kIx~	-
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Justin	Justin	k1gMnSc1	Justin
Timberlake	Timberlake	k1gFnSc1	Timberlake
-	-	kIx~	-
zpěvák	zpěvák	k1gMnSc1	zpěvák
Ric	Ric	k1gMnSc1	Ric
Flair	Flair	k1gMnSc1	Flair
-	-	kIx~	-
profesionální	profesionální	k2eAgMnSc1d1	profesionální
wrestler	wrestler	k1gMnSc1	wrestler
Juicy	Juica	k1gFnSc2	Juica
J	J	kA	J
-	-	kIx~	-
rapper	rapper	k1gMnSc1	rapper
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Memphis	Memphis	k1gFnSc2	Memphis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
