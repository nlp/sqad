<s>
Dorothea	Dorothea	k6eAd1	Dorothea
von	von	k1gInSc1	von
Biron	Birona	k1gFnPc2	Birona
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1793	[number]	k4	1793
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Friedrichsfelde	Friedrichsfeld	k1gInSc5	Friedrichsfeld
u	u	k7c2	u
Berlína	Berlín	k1gInSc2	Berlín
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1862	[number]	k4	1862
Zaháň	Zaháň	k1gFnSc4	Zaháň
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
,	,	kIx,	,
Království	království	k1gNnSc1	království
Pruské	pruský	k2eAgFnSc2d1	pruská
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
princezna	princezna	k1gFnSc1	princezna
kuronská	kuronský	k2eAgFnSc1d1	kuronský
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Zaháňská	zaháňský	k2eAgFnSc1d1	Zaháňská
<g/>
.	.	kIx.	.
</s>
<s>
Dorothea	Dorothea	k1gFnSc1	Dorothea
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
vévodkyní	vévodkyně	k1gFnPc2	vévodkyně
de	de	k?	de
Dino	Dino	k1gMnSc1	Dino
(	(	kIx(	(
<g/>
Dino	Dino	k1gMnSc1	Dino
je	být	k5eAaImIp3nS	být
kalábrijský	kalábrijský	k2eAgInSc4d1	kalábrijský
ostrov	ostrov	k1gInSc4	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
vévodkyní	vévodkyně	k1gFnSc7	vévodkyně
de	de	k?	de
Talleyrand	Talleyranda	k1gFnPc2	Talleyranda
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
vévodkyní	vévodkyně	k1gFnPc2	vévodkyně
ze	z	k7c2	z
Zaháně	Zaháň	k1gFnSc2	Zaháň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
literatuře	literatura	k1gFnSc6	literatura
ji	on	k3xPp3gFnSc4	on
najdeme	najít	k5eAaPmIp1nP	najít
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Dorothée	Dorothé	k1gFnSc2	Dorothé
de	de	k?	de
Courlande	Courland	k1gInSc5	Courland
-	-	kIx~	-
tedy	tedy	k8xC	tedy
Dorothée	Dorothée	k1gNnPc4	Dorothée
Kuronská	Kuronský	k2eAgNnPc4d1	Kuronské
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgNnSc2	tento
francouzského	francouzský	k2eAgNnSc2d1	francouzské
označení	označení	k1gNnSc2	označení
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
jejího	její	k3xOp3gMnSc2	její
legitimního	legitimní	k2eAgMnSc2d1	legitimní
otce	otec	k1gMnSc2	otec
Petra	Petr	k1gMnSc2	Petr
Birona	Biron	k1gMnSc2	Biron
<g/>
,	,	kIx,	,
posledního	poslední	k2eAgMnSc2d1	poslední
vévody	vévoda	k1gMnSc2	vévoda
Kuronského	Kuronský	k2eAgMnSc2d1	Kuronský
<g/>
.	.	kIx.	.
</s>
<s>
Českému	český	k2eAgMnSc3d1	český
čtenáři	čtenář	k1gMnSc3	čtenář
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
sestra	sestra	k1gFnSc1	sestra
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Vilemíny	Vilemína	k1gFnSc2	Vilemína
Zaháňské	zaháňský	k2eAgFnSc2d1	Zaháňská
-	-	kIx~	-
paní	paní	k1gFnSc2	paní
kněžny	kněžna	k1gFnSc2	kněžna
z	z	k7c2	z
novely	novela	k1gFnSc2	novela
Babička	babička	k1gFnSc1	babička
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Dorothea	Dorothea	k6eAd1	Dorothea
von	von	k1gInSc1	von
Biron	Birona	k1gFnPc2	Birona
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
z	z	k7c2	z
mimomanželského	mimomanželský	k2eAgInSc2d1	mimomanželský
vztahu	vztah	k1gInSc2	vztah
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Anny	Anna	k1gFnSc2	Anna
Charlotty	Charlotta	k1gFnSc2	Charlotta
Dorothey	Dorothea	k1gFnSc2	Dorothea
Kuronské	Kuronský	k2eAgFnSc2d1	Kuronská
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
kuronské	kuronský	k2eAgFnSc2d1	kuronský
rodiny	rodina	k1gFnSc2	rodina
říšských	říšský	k2eAgNnPc2d1	říšské
hrabat	hrabě	k1gNnPc2	hrabě
von	von	k1gInSc4	von
Medem	med	k1gInSc7	med
<g/>
,	,	kIx,	,
a	a	k8xC	a
polského	polský	k2eAgMnSc2d1	polský
hraběte	hrabě	k1gMnSc2	hrabě
Alexandra	Alexandr	k1gMnSc2	Alexandr
Batowského	Batowský	k1gMnSc2	Batowský
(	(	kIx(	(
<g/>
1758	[number]	k4	1758
<g/>
–	–	k?	–
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manžel	manžel	k1gMnSc1	manžel
její	její	k3xOp3gFnSc2	její
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Biron	Biron	k1gMnSc1	Biron
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
Kuronský	Kuronský	k2eAgMnSc1d1	Kuronský
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnSc1	její
narození	narození	k1gNnSc4	narození
legitimizoval	legitimizovat	k5eAaBmAgMnS	legitimizovat
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
ji	on	k3xPp3gFnSc4	on
přijal	přijmout	k5eAaPmAgMnS	přijmout
za	za	k7c4	za
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Dorothea	Dorothea	k1gFnSc1	Dorothea
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
u	u	k7c2	u
matky	matka	k1gFnSc2	matka
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Friedrichsfelde	Friedrichsfeld	k1gMnSc5	Friedrichsfeld
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
třídě	třída	k1gFnSc6	třída
Unter	Unter	k1gMnSc1	Unter
den	dna	k1gFnPc2	dna
Linden	Lindna	k1gFnPc2	Lindna
(	(	kIx(	(
<g/>
Pod	pod	k7c7	pod
Lípami	lípa	k1gFnPc7	lípa
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
duryňském	duryňský	k2eAgInSc6d1	duryňský
zámku	zámek	k1gInSc6	zámek
Löbichau	Löbichaus	k1gInSc2	Löbichaus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
její	její	k3xOp3gInSc4	její
matka	matka	k1gFnSc1	matka
koupila	koupit	k5eAaPmAgFnS	koupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
matka	matka	k1gFnSc1	matka
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
navštěvovaly	navštěvovat	k5eAaImAgFnP	navštěvovat
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
Zaháň	Zaháň	k1gFnSc1	Zaháň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Kuronska	Kuronsko	k1gNnSc2	Kuronsko
usídlil	usídlit	k5eAaPmAgMnS	usídlit
vévoda	vévoda	k1gMnSc1	vévoda
Kuronský	Kuronský	k2eAgMnSc1d1	Kuronský
se	se	k3xPyFc4	se
zbývajícími	zbývající	k2eAgFnPc7d1	zbývající
třemi	tři	k4xCgNnPc7	tři
dcerami	dcera	k1gFnPc7	dcera
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Vilemínou	Vilemína	k1gFnSc7	Vilemína
<g/>
,	,	kIx,	,
Paulínou	Paulína	k1gFnSc7	Paulína
a	a	k8xC	a
Johannou	Johanný	k2eAgFnSc7d1	Johanný
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Dorothea	Dorothea	k1gFnSc1	Dorothea
znala	znát	k5eAaImAgFnS	znát
svého	svůj	k3xOyFgMnSc4	svůj
fyzického	fyzický	k2eAgMnSc4d1	fyzický
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
a	a	k8xC	a
i	i	k9	i
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
několikrát	několikrát	k6eAd1	několikrát
setkala	setkat	k5eAaPmAgFnS	setkat
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
pravého	pravý	k2eAgMnSc4d1	pravý
otce	otec	k1gMnSc4	otec
vévodu	vévoda	k1gMnSc4	vévoda
Petra	Petr	k1gMnSc4	Petr
Birona	Biron	k1gMnSc4	Biron
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
o	o	k7c4	o
dceřinu	dceřin	k2eAgFnSc4d1	dceřina
výchovu	výchova	k1gFnSc4	výchova
příliš	příliš	k6eAd1	příliš
nestarala	starat	k5eNaImAgFnS	starat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Dorothea	Dorothea	k1gFnSc1	Dorothea
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
hovořila	hovořit	k5eAaImAgFnS	hovořit
směsicí	směsice	k1gFnSc7	směsice
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
malou	malý	k2eAgFnSc4d1	malá
Dorotheu	Dorothea	k1gFnSc4	Dorothea
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
starat	starat	k5eAaImF	starat
až	až	k9	až
nový	nový	k2eAgMnSc1d1	nový
matčin	matčin	k2eAgMnSc1d1	matčin
milenec	milenec	k1gMnSc1	milenec
<g/>
,	,	kIx,	,
švédský	švédský	k2eAgMnSc1d1	švédský
generál	generál	k1gMnSc1	generál
Gustav	Gustav	k1gMnSc1	Gustav
Moritz	Moritz	k1gMnSc1	Moritz
Armfelt	Armfelt	k1gMnSc1	Armfelt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
ji	on	k3xPp3gFnSc4	on
naučil	naučit	k5eAaPmAgMnS	naučit
psát	psát	k5eAaImF	psát
a	a	k8xC	a
číst	číst	k5eAaImF	číst
a	a	k8xC	a
najal	najmout	k5eAaPmAgMnS	najmout
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
řádné	řádný	k2eAgMnPc4d1	řádný
vychovatele	vychovatel	k1gMnPc4	vychovatel
a	a	k8xC	a
učitele	učitel	k1gMnPc4	učitel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dívky	dívka	k1gFnSc2	dívka
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
mimořádné	mimořádný	k2eAgNnSc1d1	mimořádné
nadání	nadání	k1gNnSc1	nadání
pro	pro	k7c4	pro
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
astronomii	astronomie	k1gFnSc4	astronomie
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vášnivá	vášnivý	k2eAgFnSc1d1	vášnivá
čtenářka	čtenářka	k1gFnSc1	čtenářka
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
žena	žena	k1gFnSc1	žena
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
literárním	literární	k2eAgInSc7d1	literární
talentem	talent	k1gInSc7	talent
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
korespondenci	korespondence	k1gFnSc6	korespondence
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
září	září	k1gNnSc6	září
1806	[number]	k4	1806
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Prusko	Prusko	k1gNnSc1	Prusko
válku	válek	k1gInSc6	válek
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
I.	I.	kA	I.
<g/>
,	,	kIx,	,
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
matka	matka	k1gFnSc1	matka
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
situace	situace	k1gFnSc2	situace
přiměl	přimět	k5eAaPmAgInS	přimět
i	i	k9	i
Dorotheu	Dorothea	k1gFnSc4	Dorothea
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
mnoho	mnoho	k4c1	mnoho
jejích	její	k3xOp3gFnPc2	její
známých	známá	k1gFnPc2	známá
ze	z	k7c2	z
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
kruhů	kruh	k1gInPc2	kruh
opustila	opustit	k5eAaPmAgFnS	opustit
Berlín	Berlín	k1gInSc4	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dosud	dosud	k6eAd1	dosud
nikdy	nikdy	k6eAd1	nikdy
nenavštívila	navštívit	k5eNaPmAgFnS	navštívit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
kořeny	kořen	k1gInPc4	kořen
její	její	k3xOp3gFnSc1	její
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
do	do	k7c2	do
Kuronska	Kuronsko	k1gNnSc2	Kuronsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
usadila	usadit	k5eAaPmAgFnS	usadit
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Alt-Autz	Alt-Autza	k1gFnPc2	Alt-Autza
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
její	její	k3xOp3gMnSc1	její
strýc	strýc	k1gMnSc1	strýc
-	-	kIx~	-
matčin	matčin	k2eAgMnSc1d1	matčin
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
do	do	k7c2	do
Mitavy	Mitava	k1gFnSc2	Mitava
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Vecauce	Vecauce	k1gFnSc2	Vecauce
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Auce	Auc	k1gInSc2	Auc
a	a	k8xC	a
město	město	k1gNnSc4	město
Jelgava	Jelgava	k1gFnSc1	Jelgava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
také	také	k9	také
sem	sem	k6eAd1	sem
zavítal	zavítat	k5eAaPmAgMnS	zavítat
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1807	[number]	k4	1807
se	se	k3xPyFc4	se
Dorothea	Dorothea	k1gFnSc1	Dorothea
von	von	k1gInSc4	von
Biron	Birona	k1gFnPc2	Birona
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
našla	najít	k5eAaPmAgFnS	najít
Kuronský	Kuronský	k2eAgInSc4d1	Kuronský
palác	palác	k1gInSc4	palác
(	(	kIx(	(
<g/>
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Petra	Petr	k1gMnSc2	Petr
Birona	Biron	k1gMnSc2	Biron
jej	on	k3xPp3gMnSc4	on
dle	dle	k7c2	dle
závěti	závěť	k1gFnSc2	závěť
zdědila	zdědit	k5eAaPmAgFnS	zdědit
právě	právě	k9	právě
ona	onen	k3xDgFnSc1	onen
<g/>
)	)	kIx)	)
obsazený	obsazený	k2eAgMnSc1d1	obsazený
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jí	on	k3xPp3gFnSc3	on
uvolnili	uvolnit	k5eAaPmAgMnP	uvolnit
jen	jen	k9	jen
několik	několik	k4yIc4	několik
místností	místnost	k1gFnPc2	místnost
původně	původně	k6eAd1	původně
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
služebnictvo	služebnictvo	k1gNnSc4	služebnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Dorothea	Dorothea	k1gFnSc1	Dorothea
tak	tak	k6eAd1	tak
sdílela	sdílet	k5eAaImAgFnS	sdílet
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
všeobecně	všeobecně	k6eAd1	všeobecně
panující	panující	k2eAgFnSc4d1	panující
protifrancouzskou	protifrancouzský	k2eAgFnSc4d1	protifrancouzská
náladu	nálada	k1gFnSc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
přátelila	přátelit	k5eAaImAgFnS	přátelit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
jednak	jednak	k8xC	jednak
získala	získat	k5eAaPmAgFnS	získat
nepřátele	nepřítel	k1gMnPc4	nepřítel
v	v	k7c6	v
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
tím	ten	k3xDgNnSc7	ten
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
citové	citový	k2eAgNnSc4d1	citové
odcizení	odcizení	k1gNnSc4	odcizení
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1808	[number]	k4	1808
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
matka	matka	k1gFnSc1	matka
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
Löbichau	Löbichaus	k1gInSc2	Löbichaus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
dívka	dívka	k1gFnSc1	dívka
vymínila	vymínit	k5eAaPmAgFnS	vymínit
oddělené	oddělený	k2eAgNnSc4d1	oddělené
bydlení	bydlení	k1gNnSc4	bydlení
v	v	k7c6	v
nedalekém	daleký	k2eNgInSc6d1	nedaleký
zámečku	zámeček	k1gInSc6	zámeček
Tannenfeld	Tannenfelda	k1gFnPc2	Tannenfelda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vedla	vést	k5eAaImAgFnS	vést
vlastní	vlastní	k2eAgFnSc4d1	vlastní
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
hledal	hledat	k5eAaImAgMnS	hledat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Napoleonův	Napoleonův	k2eAgMnSc1d1	Napoleonův
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gMnSc1	Talleyrand-Périgord
(	(	kIx(	(
<g/>
vévoda	vévoda	k1gMnSc1	vévoda
de	de	k?	de
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
<g/>
)	)	kIx)	)
nevěstu	nevěsta	k1gFnSc4	nevěsta
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
synovce	synovec	k1gMnSc4	synovec
Edmonda	Edmond	k1gMnSc4	Edmond
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gMnSc1	Talleyrand-Périgord
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k1gInSc1	stát
strýcovým	strýcův	k2eAgMnSc7d1	strýcův
jediným	jediný	k2eAgMnSc7d1	jediný
dědicem	dědic	k1gMnSc7	dědic
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
volba	volba	k1gFnSc1	volba
padla	padnout	k5eAaImAgFnS	padnout
na	na	k7c4	na
bohatou	bohatý	k2eAgFnSc4d1	bohatá
Dorotheu	Dorothea	k1gFnSc4	Dorothea
von	von	k1gInSc1	von
Biron	Biron	k1gInSc1	Biron
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kongresu	kongres	k1gInSc6	kongres
evropských	evropský	k2eAgMnPc2d1	evropský
vladařů	vladař	k1gMnPc2	vladař
v	v	k7c6	v
Erfurtu	Erfurt	k1gInSc6	Erfurt
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Napoleonovi	Napoleonův	k2eAgMnPc1d1	Napoleonův
již	již	k6eAd1	již
politicky	politicky	k6eAd1	politicky
vzdaloval	vzdalovat	k5eAaImAgMnS	vzdalovat
<g/>
,	,	kIx,	,
na	na	k7c6	na
soukromé	soukromý	k2eAgFnSc6d1	soukromá
schůzce	schůzka	k1gFnSc6	schůzka
ruskému	ruský	k2eAgMnSc3d1	ruský
carovi	car	k1gMnSc3	car
Alexandru	Alexandr	k1gMnSc3	Alexandr
I.	I.	kA	I.
užitečné	užitečný	k2eAgFnPc4d1	užitečná
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
car	car	k1gMnSc1	car
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
zastavil	zastavit	k5eAaPmAgInS	zastavit
v	v	k7c6	v
Löbichau	Löbichaus	k1gInSc6	Löbichaus
a	a	k8xC	a
přimluvil	přimluvit	k5eAaPmAgInS	přimluvit
se	se	k3xPyFc4	se
u	u	k7c2	u
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Kuronské	Kuronský	k2eAgFnSc2d1	Kuronská
za	za	k7c4	za
sňatek	sňatek	k1gInSc4	sňatek
Dorothey	Dorothea	k1gFnSc2	Dorothea
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
dvaadvacetiletým	dvaadvacetiletý	k2eAgMnSc7d1	dvaadvacetiletý
Edmondem	Edmond	k1gMnSc7	Edmond
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gInSc1	Talleyrand-Périgord
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Löbichau	Löbichaus	k1gInSc2	Löbichaus
zastavil	zastavit	k5eAaPmAgInS	zastavit
a	a	k8xC	a
strávil	strávit	k5eAaPmAgInS	strávit
tam	tam	k6eAd1	tam
sedm	sedm	k4xCc4	sedm
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Edmond	Edmond	k1gMnSc1	Edmond
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gMnSc1	Talleyrand-Périgord
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
jeho	jeho	k3xOp3gInSc2	jeho
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Dorothein	Dorothein	k2eAgMnSc1d1	Dorothein
skutečný	skutečný	k2eAgMnSc1d1	skutečný
otec	otec	k1gMnSc1	otec
hrabě	hrabě	k1gMnSc1	hrabě
Batowski	Batowsk	k1gMnSc3	Batowsk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
plány	plán	k1gInPc4	plán
svatby	svatba	k1gFnSc2	svatba
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
neměli	mít	k5eNaImAgMnP	mít
co	co	k9	co
říci	říct	k5eAaPmF	říct
a	a	k8xC	a
do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
vstupovali	vstupovat	k5eAaImAgMnP	vstupovat
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1809	[number]	k4	1809
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
šestnáctiletá	šestnáctiletý	k2eAgFnSc1d1	šestnáctiletá
Dorothea	Dorothea	k1gFnSc1	Dorothea
stala	stát	k5eAaPmAgFnS	stát
hraběnkou	hraběnka	k1gFnSc7	hraběnka
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgorda	k1gFnPc2	Talleyrand-Périgorda
a	a	k8xC	a
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
dvorních	dvorní	k2eAgFnPc2d1	dvorní
dam	dáma	k1gFnPc2	dáma
císařovny	císařovna	k1gFnSc2	císařovna
Josefíny	Josefína	k1gFnSc2	Josefína
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
žila	žít	k5eAaImAgFnS	žít
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Dorothey	Dorothea	k1gFnSc2	Dorothea
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Charlotta	Charlotta	k1gFnSc1	Charlotta
Dorothea	Dorothea	k1gFnSc1	Dorothea
von	von	k1gInSc4	von
Medem	med	k1gInSc7	med
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Kuronská	Kuronský	k2eAgFnSc1d1	Kuronská
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
také	také	k9	také
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
osobnostem	osobnost	k1gFnPc3	osobnost
aristokratické	aristokratický	k2eAgFnSc2d1	aristokratická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
scházela	scházet	k5eAaImAgFnS	scházet
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
přijela	přijet	k5eAaPmAgFnS	přijet
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1790	[number]	k4	1790
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
lázeňské	lázeňská	k1gFnPc1	lázeňská
návštěvy	návštěva	k1gFnSc2	návštěva
příslušníků	příslušník	k1gMnPc2	příslušník
rodu	rod	k1gInSc2	rod
Kuronských	Kuronský	k2eAgInPc2d1	Kuronský
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
karlovarské	karlovarský	k2eAgInPc4d1	karlovarský
anály	anály	k1gInPc4	anály
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1791	[number]	k4	1791
<g/>
-	-	kIx~	-
<g/>
1820	[number]	k4	1820
<g/>
.	.	kIx.	.
</s>
<s>
Bydleli	bydlet	k5eAaImAgMnP	bydlet
často	často	k6eAd1	často
na	na	k7c6	na
Tržišti	tržiště	k1gNnSc6	tržiště
v	v	k7c6	v
domě	dům	k1gInSc6	dům
u	u	k7c2	u
městského	městský	k2eAgMnSc2d1	městský
písaře	písař	k1gMnSc2	písař
Gerbera	gerbera	k1gFnSc1	gerbera
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
hotel	hotel	k1gInSc1	hotel
Wolker	Wolker	k1gMnSc1	Wolker
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
protějším	protější	k2eAgInSc6d1	protější
domě	dům	k1gInSc6	dům
Pomerančovník	pomerančovník	k1gInSc1	pomerančovník
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
hotel	hotel	k1gInSc1	hotel
Promenáda	promenáda	k1gFnSc1	promenáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
se	se	k3xPyFc4	se
Dorothea	Dorothea	k1gFnSc1	Dorothea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
jako	jako	k8xC	jako
patnáctiletá	patnáctiletý	k2eAgFnSc1d1	patnáctiletá
dívka	dívka	k1gFnSc1	dívka
prvně	prvně	k?	prvně
setkala	setkat	k5eAaPmAgFnS	setkat
se	s	k7c7	s
slavným	slavný	k2eAgMnSc7d1	slavný
básníkem	básník	k1gMnSc7	básník
Johannem	Johann	k1gMnSc7	Johann
Wolfgangem	Wolfgang	k1gMnSc7	Wolfgang
von	von	k1gInSc4	von
Goethe	Goeth	k1gFnSc2	Goeth
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethe	k5eAaImRp2nP	Goethe
si	se	k3xPyFc3	se
do	do	k7c2	do
deníku	deník	k1gInSc2	deník
zapsal	zapsat	k5eAaPmAgInS	zapsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Kuronská	Kuronský	k2eAgFnSc1d1	Kuronská
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
půvabná	půvabný	k2eAgFnSc1d1	půvabná
a	a	k8xC	a
s	s	k7c7	s
vybranou	vybraný	k2eAgFnSc7d1	vybraná
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
paní	paní	k1gFnSc1	paní
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Recke	Recke	k1gNnSc1	Recke
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
Tiedgem	Tiedgo	k1gNnSc7	Tiedgo
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
družilo	družit	k5eAaImAgNnS	družit
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
příjemným	příjemný	k2eAgInSc7d1	příjemný
středem	střed	k1gInSc7	střed
tamějšího	tamější	k2eAgNnSc2d1	tamější
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Goethe	Goethe	k1gFnSc1	Goethe
se	se	k3xPyFc4	se
s	s	k7c7	s
Dorotheou	Dorothea	k1gFnSc7	Dorothea
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
sestrou	sestra	k1gFnSc7	sestra
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Vilemínou	Vilemína	k1gFnSc7	Vilemína
setkal	setkat	k5eAaPmAgMnS	setkat
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
také	také	k9	také
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
vévodkyni	vévodkyně	k1gFnSc4	vévodkyně
Kuronskou	Kuronský	k2eAgFnSc4d1	Kuronská
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
Charlotta	Charlotta	k1gFnSc1	Charlotta
Dorothea	Dorothea	k1gFnSc1	Dorothea
von	von	k1gInSc4	von
Meden	Meden	k1gInSc1	Meden
<g/>
)	)	kIx)	)
v	v	k7c6	v
lázeňském	lázeňský	k2eAgNnSc6d1	lázeňské
městě	město	k1gNnSc6	město
připomíná	připomínat	k5eAaImIp3nS	připomínat
klasicistní	klasicistní	k2eAgInSc1d1	klasicistní
altánek	altánek	k1gInSc1	altánek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
pražský	pražský	k2eAgMnSc1d1	pražský
hrabě	hrabě	k1gMnSc1	hrabě
Christian	Christian	k1gMnSc1	Christian
Filip	Filip	k1gMnSc1	Filip
Clam-Gallas	Clam-Gallas	k1gMnSc1	Clam-Gallas
jako	jako	k8xS	jako
výraz	výraz	k1gInSc1	výraz
obdivu	obdiv	k1gInSc2	obdiv
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1811-1813	[number]	k4	1811-1813
se	se	k3xPyFc4	se
Dorothee	Dorothee	k1gNnSc7	Dorothee
z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Edmondem	Edmond	k1gMnSc7	Edmond
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gInSc1	Talleyrand-Périgord
narodily	narodit	k5eAaPmAgFnP	narodit
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
-	-	kIx~	-
Ludvík	Ludvík	k1gMnSc1	Ludvík
Napoleon	Napoleon	k1gMnSc1	Napoleon
(	(	kIx(	(
<g/>
1811	[number]	k4	1811
<g/>
–	–	k?	–
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
vévoda	vévoda	k1gMnSc1	vévoda
Zaháňský	zaháňský	k2eAgMnSc1d1	zaháňský
a	a	k8xC	a
z	z	k7c2	z
Valençay	Valençaa	k1gFnSc2	Valençaa
<g/>
,	,	kIx,	,
Dorothée	Dorothéus	k1gMnSc5	Dorothéus
Charlotte	Charlott	k1gMnSc5	Charlott
Emily	Emil	k1gMnPc7	Emil
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
–	–	k?	–
<g/>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
a	a	k8xC	a
Alexandre	Alexandr	k1gInSc5	Alexandr
Edmond	Edmond	k1gMnSc1	Edmond
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
vévoda	vévoda	k1gMnSc1	vévoda
de	de	k?	de
Dino	Dino	k1gMnSc1	Dino
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
–	–	k?	–
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
německého	německý	k2eAgMnSc2d1	německý
historika	historik	k1gMnSc2	historik
Willmse	Willmse	k1gFnSc2	Willmse
se	se	k3xPyFc4	se
Dorothee	Dorothee	k1gFnSc1	Dorothee
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgorda	k1gFnPc2	Talleyrand-Périgorda
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
narodila	narodit	k5eAaPmAgFnS	narodit
také	také	k9	také
nemanželská	manželský	k2eNgFnSc1d1	nemanželská
dcera	dcera	k1gFnSc1	dcera
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
hraběti	hrabě	k1gMnSc3	hrabě
Karlu	Karel	k1gMnSc3	Karel
Janovi	Jan	k1gMnSc3	Jan
Clam-Martinicovi	Clam-Martinic	k1gMnSc3	Clam-Martinic
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
–	–	k?	–
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určitá	určitý	k2eAgFnSc1d1	určitá
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
dítě	dítě	k1gNnSc4	dítě
známe	znát	k5eAaImIp1nP	znát
jako	jako	k9	jako
Barboru	Barbora	k1gFnSc4	Barbora
Panklovou	Panklový	k2eAgFnSc4d1	Panklový
<g/>
,	,	kIx,	,
chráněnkyni	chráněnkyně	k1gFnSc4	chráněnkyně
kněžny	kněžna	k1gFnSc2	kněžna
Vilemíny	Vilemína	k1gFnSc2	Vilemína
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Zaháňské	zaháňský	k2eAgFnSc2d1	Zaháňská
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
pozdější	pozdní	k2eAgFnSc4d2	pozdější
spisovatelku	spisovatelka	k1gFnSc4	spisovatelka
Boženu	Božena	k1gFnSc4	Božena
Němcovou	Němcová	k1gFnSc4	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
Joséphine	Joséphin	k1gInSc5	Joséphin
Pauline	Paulin	k1gInSc5	Paulin
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1820	[number]	k4	1820
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Joséphine	Joséphinout	k5eAaPmIp3nS	Joséphinout
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
otcem	otec	k1gMnSc7	otec
však	však	k9	však
podle	podle	k7c2	podle
nepotvrzených	potvrzený	k2eNgFnPc2d1	nepotvrzená
spekulací	spekulace	k1gFnPc2	spekulace
byl	být	k5eAaImAgMnS	být
manželův	manželův	k2eAgMnSc1d1	manželův
strýc	strýc	k1gMnSc1	strýc
Charles	Charles	k1gMnSc1	Charles
Maurice	Maurika	k1gFnSc3	Maurika
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gInSc4	Talleyrand-Périgord
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
majitelkou	majitelka	k1gFnSc7	majitelka
francouzského	francouzský	k2eAgInSc2d1	francouzský
zámku	zámek	k1gInSc2	zámek
Rochecotte	Rochecott	k1gInSc5	Rochecott
v	v	k7c6	v
departement	departement	k1gInSc1	departement
Indre-et-Loire	Indret-Loir	k1gInSc5	Indre-et-Loir
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
dostala	dostat	k5eAaPmAgFnS	dostat
darem	dar	k1gInSc7	dar
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Edmondem	Edmond	k1gMnSc7	Edmond
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gInSc1	Talleyrand-Périgord
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kalkulu	kalkul	k1gInSc2	kalkul
Dorotheiny	Dorothein	k2eAgFnSc2d1	Dorothein
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
Edmondova	Edmondův	k2eAgMnSc2d1	Edmondův
strýce	strýc	k1gMnSc2	strýc
<g/>
,	,	kIx,	,
ztroskotalo	ztroskotat	k5eAaPmAgNnS	ztroskotat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
k	k	k7c3	k
formálnímu	formální	k2eAgInSc3d1	formální
rozvodu	rozvod	k1gInSc3	rozvod
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
<g/>
.	.	kIx.	.
</s>
<s>
Edmond	Edmond	k1gMnSc1	Edmond
byl	být	k5eAaImAgMnS	být
především	především	k9	především
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
sukničkář	sukničkář	k1gMnSc1	sukničkář
<g/>
,	,	kIx,	,
hazardní	hazardní	k2eAgMnSc1d1	hazardní
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
holdoval	holdovat	k5eAaImAgMnS	holdovat
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
těžil	těžit	k5eAaImAgMnS	těžit
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roztržce	roztržka	k1gFnSc6	roztržka
s	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
Edmondem	Edmond	k1gMnSc7	Edmond
se	se	k3xPyFc4	se
Dorothea	Dorotheus	k1gMnSc2	Dorotheus
sblížila	sblížit	k5eAaPmAgFnS	sblížit
s	s	k7c7	s
o	o	k7c4	o
39	[number]	k4	39
let	léto	k1gNnPc2	léto
starším	starý	k2eAgMnSc7d2	starší
vévodou	vévoda	k1gMnSc7	vévoda
Charlesem	Charles	k1gMnSc7	Charles
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gInSc1	Talleyrand-Périgord
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
stálou	stálý	k2eAgFnSc7d1	stálá
společnicí	společnice	k1gFnSc7	společnice
<g/>
.	.	kIx.	.
</s>
<s>
Vedla	vést	k5eAaImAgFnS	vést
jeho	jeho	k3xOp3gFnSc4	jeho
velkou	velký	k2eAgFnSc4d1	velká
domácnost	domácnost	k1gFnSc4	domácnost
a	a	k8xC	a
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
diplomatických	diplomatický	k2eAgFnPc6d1	diplomatická
misích	mise	k1gFnPc6	mise
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gInSc4	jeho
okouzlující	okouzlující	k2eAgInSc4d1	okouzlující
partnerkou	partnerka	k1gFnSc7	partnerka
v	v	k7c6	v
hojně	hojně	k6eAd1	hojně
navštěvovaném	navštěvovaný	k2eAgInSc6d1	navštěvovaný
salonu	salon	k1gInSc6	salon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
spolu	spolu	k6eAd1	spolu
vedli	vést	k5eAaImAgMnP	vést
na	na	k7c6	na
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongrese	kongres	k1gInSc6	kongres
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
-	-	kIx~	-
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1831	[number]	k4	1831
<g/>
-	-	kIx~	-
<g/>
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
vévoda	vévoda	k1gMnSc1	vévoda
francouzským	francouzský	k2eAgMnSc7d1	francouzský
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
u	u	k7c2	u
britského	britský	k2eAgInSc2d1	britský
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Vévoda	vévoda	k1gMnSc1	vévoda
Talleyrand	Talleyranda	k1gFnPc2	Talleyranda
ji	on	k3xPp3gFnSc4	on
po	po	k7c4	po
své	svůj	k3xOyFgFnPc4	svůj
smrti	smrt	k1gFnPc4	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
učinil	učinit	k5eAaPmAgMnS	učinit
svou	svůj	k3xOyFgFnSc7	svůj
univerzální	univerzální	k2eAgFnSc7d1	univerzální
dědičkou	dědička	k1gFnSc7	dědička
<g/>
.	.	kIx.	.
</s>
<s>
Dorothea	Dorothea	k6eAd1	Dorothea
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
majitelkou	majitelka	k1gFnSc7	majitelka
zámku	zámek	k1gInSc2	zámek
Valençay	Valençaa	k1gFnSc2	Valençaa
<g/>
.	.	kIx.	.
</s>
<s>
Dorothea	Dorothea	k6eAd1	Dorothea
i	i	k9	i
během	během	k7c2	během
společného	společný	k2eAgInSc2d1	společný
života	život	k1gInSc2	život
s	s	k7c7	s
vévodou	vévoda	k1gMnSc7	vévoda
Talleyrandem	Talleyrand	k1gInSc7	Talleyrand
prožila	prožít	k5eAaPmAgFnS	prožít
několik	několik	k4yIc4	několik
milostných	milostný	k2eAgInPc2d1	milostný
románků	románek	k1gInPc2	románek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
narodily	narodit	k5eAaPmAgFnP	narodit
další	další	k2eAgFnPc4d1	další
dcery	dcera	k1gFnPc4	dcera
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
Antonine	Antonin	k1gInSc5	Antonin
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
Julie	Julie	k1gFnSc2	Julie
Zedmé	Zedmý	k2eAgFnSc2d1	Zedmý
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
obletována	obletovat	k5eAaImNgFnS	obletovat
mnoha	mnoho	k4c7	mnoho
muži	muž	k1gMnPc7	muž
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
nejstarší	starý	k2eAgFnSc1d3	nejstarší
sestra	sestra	k1gFnSc1	sestra
Kateřina	Kateřina	k1gFnSc1	Kateřina
Vilemína	Vilemína	k1gFnSc1	Vilemína
Zaháňská	zaháňský	k2eAgFnSc1d1	Zaháňská
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
mluvila	mluvit	k5eAaImAgFnS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
koketě	koketa	k1gFnSc6	koketa
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
okázalé	okázalý	k2eAgNnSc4d1	okázalé
flirtování	flirtování	k1gNnSc4	flirtování
často	často	k6eAd1	často
odsuzovala	odsuzovat	k5eAaImAgFnS	odsuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Dorothea	Dorothea	k1gFnSc1	Dorothea
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
v	v	k7c6	v
dražbě	dražba	k1gFnSc6	dražba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
Zaháňské	zaháňský	k2eAgNnSc1d1	Zaháňské
vévodství	vévodství	k1gNnSc1	vévodství
od	od	k7c2	od
Konstantina	Konstantin	k1gMnSc2	Konstantin
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
své	svůj	k3xOyFgFnPc4	svůj
zemřelé	zemřelý	k2eAgFnPc4d1	zemřelá
sestry	sestra	k1gFnPc4	sestra
Paulíny	Paulína	k1gFnSc2	Paulína
<g/>
.	.	kIx.	.
</s>
<s>
Přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
Zaháň	Zaháň	k1gFnSc1	Zaháň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žila	žít	k5eAaImAgFnS	žít
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Zaháni	Zaháň	k1gFnSc6	Zaháň
pobývala	pobývat	k5eAaImAgFnS	pobývat
často	často	k6eAd1	často
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc4	zámek
upravila	upravit	k5eAaPmAgFnS	upravit
<g/>
,	,	kIx,	,
nechala	nechat	k5eAaPmAgFnS	nechat
přistavět	přistavět	k5eAaPmF	přistavět
další	další	k2eAgFnPc4d1	další
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
nemocnici	nemocnice	k1gFnSc4	nemocnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Zrekonstruovala	zrekonstruovat	k5eAaPmAgFnS	zrekonstruovat
zámecký	zámecký	k2eAgInSc4d1	zámecký
park	park	k1gInSc4	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
nejstarší	starý	k2eAgFnSc2d3	nejstarší
sestry	sestra	k1gFnSc2	sestra
založila	založit	k5eAaPmAgFnS	založit
"	"	kIx"	"
<g/>
Vilemíninu	Vilemínin	k2eAgFnSc4d1	Vilemínina
louku	louka	k1gFnSc4	louka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Celému	celý	k2eAgNnSc3d1	celé
velkému	velký	k2eAgNnSc3d1	velké
a	a	k8xC	a
bohatému	bohatý	k2eAgNnSc3d1	bohaté
panství	panství	k1gNnSc3	panství
(	(	kIx(	(
<g/>
130	[number]	k4	130
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
1200	[number]	k4	1200
ha	ha	kA	ha
půdy	půda	k1gFnSc2	půda
<g/>
)	)	kIx)	)
vládla	vládnout	k5eAaImAgFnS	vládnout
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Zaháni	Zaháň	k1gFnSc6	Zaháň
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
hrobky	hrobka	k1gFnPc4	hrobka
s	s	k7c7	s
ostatky	ostatek	k1gInPc7	ostatek
členů	člen	k1gMnPc2	člen
vévodské	vévodský	k2eAgFnSc2d1	vévodská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
uloženi	uložen	k2eAgMnPc1d1	uložen
Kateřina	Kateřina	k1gFnSc1	Kateřina
Vilemína	Vilemína	k1gFnSc1	Vilemína
<g/>
,	,	kIx,	,
Dorothea	Dorothea	k1gFnSc1	Dorothea
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
Ludvík	Ludvík	k1gMnSc1	Ludvík
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přestoupili	přestoupit	k5eAaPmAgMnP	přestoupit
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
rodiny	rodina	k1gFnSc2	rodina
byli	být	k5eAaImAgMnP	být
protestantského	protestantský	k2eAgNnSc2d1	protestantské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
závěti	závěť	k1gFnSc2	závěť
si	se	k3xPyFc3	se
Dorothea	Dorothea	k1gFnSc1	Dorothea
přála	přát	k5eAaImAgFnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
její	její	k3xOp3gNnSc1	její
srdce	srdce	k1gNnSc1	srdce
bylo	být	k5eAaImAgNnS	být
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
zámku	zámek	k1gInSc2	zámek
Valençay	Valençaa	k1gFnSc2	Valençaa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
však	však	k9	však
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
udělil	udělit	k5eAaPmAgMnS	udělit
pruský	pruský	k2eAgMnSc1d1	pruský
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
Vilém	Vilém	k1gMnSc1	Vilém
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Dorothee	Dorotheat	k5eAaPmIp3nS	Dorotheat
titul	titul	k1gInSc4	titul
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Zaháňské	zaháňský	k2eAgFnSc2d1	Zaháňská
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
uznal	uznat	k5eAaPmAgInS	uznat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Napoleona	Napoleon	k1gMnSc4	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
dvojího	dvojí	k4xRgInSc2	dvojí
titulu	titul	k1gInSc2	titul
-	-	kIx~	-
francouzského	francouzský	k2eAgMnSc2d1	francouzský
duc	duc	k0	duc
de	de	k?	de
Talleyrand	Talleyranda	k1gFnPc2	Talleyranda
a	a	k8xC	a
pruského	pruský	k2eAgInSc2d1	pruský
Herzog	Herzog	k1gMnSc1	Herzog
zu	zu	k?	zu
Sagan	Sagan	k1gMnSc1	Sagan
(	(	kIx(	(
<g/>
Zaháň	Zaháň	k1gFnSc1	Zaháň
<g/>
)	)	kIx)	)
-	-	kIx~	-
umožnila	umožnit	k5eAaPmAgFnS	umožnit
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
100	[number]	k4	100
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vévodovi	vévoda	k1gMnSc3	vévoda
Paulu	Paul	k1gMnSc3	Paul
Louisi	Louis	k1gMnSc3	Louis
Bosonovi	Boson	k1gMnSc3	Boson
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gMnSc1	Talleyrand-Périgord
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
poslednímu	poslední	k2eAgMnSc3d1	poslední
vévodovi	vévoda	k1gMnSc3	vévoda
Zaháňskému	zaháňský	k2eAgMnSc3d1	zaháňský
<g/>
,	,	kIx,	,
zachovat	zachovat	k5eAaPmF	zachovat
postavení	postavení	k1gNnSc4	postavení
neutrální	neutrální	k2eAgFnSc2d1	neutrální
strany	strana	k1gFnSc2	strana
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
francouzském	francouzský	k2eAgInSc6d1	francouzský
zámku	zámek	k1gInSc6	zámek
Château	château	k1gNnSc2	château
de	de	k?	de
Valençay	Valençay	k1gInPc7	Valençay
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
během	během	k7c2	během
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
podařilo	podařit	k5eAaPmAgNnS	podařit
ukrýt	ukrýt	k5eAaPmF	ukrýt
poklady	poklad	k1gInPc4	poklad
z	z	k7c2	z
pařížského	pařížský	k2eAgNnSc2d1	pařížské
muzea	muzeum	k1gNnSc2	muzeum
Louvre	Louvre	k1gInSc1	Louvre
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
literatury	literatura	k1gFnSc2	literatura
faktu	fakt	k1gInSc2	fakt
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dorothea	Dorothea	k1gFnSc1	Dorothea
von	von	k1gInSc1	von
Biron	Biron	k1gInSc1	Biron
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
hraběnka	hraběnka	k1gFnSc1	hraběnka
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgorda	k1gFnPc2	Talleyrand-Périgorda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
matkou	matka	k1gFnSc7	matka
české	český	k2eAgFnSc2d1	Česká
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
hypotézu	hypotéza	k1gFnSc4	hypotéza
svědčí	svědčit	k5eAaImIp3nS	svědčit
nejen	nejen	k6eAd1	nejen
vzhledová	vzhledový	k2eAgFnSc1d1	vzhledová
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
povahová	povahový	k2eAgFnSc1d1	povahová
podobnost	podobnost	k1gFnSc1	podobnost
možné	možný	k2eAgFnSc2d1	možná
matky	matka	k1gFnSc2	matka
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
také	také	k9	také
nezvykle	zvykle	k6eNd1	zvykle
dobrý	dobrý	k2eAgInSc1d1	dobrý
vztah	vztah	k1gInSc1	vztah
sestry	sestra	k1gFnSc2	sestra
Dorothey	Dorothea	k1gFnSc2	Dorothea
<g/>
,	,	kIx,	,
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Vilhelmíny	Vilhelmína	k1gFnSc2	Vilhelmína
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
ze	z	k7c2	z
Zaháně	Zaháň	k1gFnSc2	Zaháň
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
panství	panství	k1gNnSc6	panství
v	v	k7c6	v
Ratibořicích	Ratibořice	k1gFnPc6	Ratibořice
Němcová	Němcová	k1gFnSc1	Němcová
pod	pod	k7c7	pod
dívčím	dívčí	k2eAgNnSc7d1	dívčí
jménem	jméno	k1gNnSc7	jméno
Barbora	Barbora	k1gFnSc1	Barbora
Panklová	Panklová	k1gFnSc1	Panklová
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
<g/>
,	,	kIx,	,
k	k	k7c3	k
Barboře	Barbora	k1gFnSc3	Barbora
i	i	k8xC	i
k	k	k7c3	k
jejím	její	k3xOp3gNnPc3	její
(	(	kIx(	(
<g/>
adoptivním	adoptivní	k2eAgMnPc3d1	adoptivní
<g/>
)	)	kIx)	)
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
nepříliš	příliš	k6eNd1	příliš
dobrý	dobrý	k2eAgInSc4d1	dobrý
vztah	vztah	k1gInSc4	vztah
Barbory	Barbora	k1gFnSc2	Barbora
k	k	k7c3	k
matce	matka	k1gFnSc3	matka
Terezii	Terezie	k1gFnSc3	Terezie
Panklové	Panklová	k1gFnSc3	Panklová
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
i	i	k9	i
tradovaný	tradovaný	k2eAgInSc1d1	tradovaný
vyšší	vysoký	k2eAgInSc1d2	vyšší
věk	věk	k1gInSc1	věk
dívky	dívka	k1gFnSc2	dívka
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jejímu	její	k3xOp3gInSc3	její
běžně	běžně	k6eAd1	běžně
udávanému	udávaný	k2eAgInSc3d1	udávaný
roku	rok	k1gInSc3	rok
narození	narození	k1gNnPc2	narození
1820	[number]	k4	1820
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
již	již	k6eAd1	již
citované	citovaný	k2eAgFnSc2d1	citovaná
knihy	kniha	k1gFnSc2	kniha
německého	německý	k2eAgMnSc2d1	německý
historika	historik	k1gMnSc2	historik
Wiilmse	Wiilms	k1gMnSc2	Wiilms
se	se	k3xPyFc4	se
z	z	k7c2	z
vášnivého	vášnivý	k2eAgInSc2d1	vášnivý
vztahu	vztah	k1gInSc2	vztah
Dorothey	Dorothea	k1gFnSc2	Dorothea
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gMnSc1	Talleyrand-Périgord
a	a	k8xC	a
hraběte	hrabě	k1gMnSc2	hrabě
Karla	Karel	k1gMnSc2	Karel
Jana	Jan	k1gMnSc2	Jan
Clam-Martinice	Clam-Martinice	k1gFnSc2	Clam-Martinice
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
–	–	k?	–
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
skutečně	skutečně	k6eAd1	skutečně
narodila	narodit	k5eAaPmAgFnS	narodit
nemanželská	manželský	k2eNgFnSc1d1	nemanželská
dcera	dcera	k1gFnSc1	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
ve	v	k7c6	v
francouzských	francouzský	k2eAgFnPc6d1	francouzská
lázních	lázeň	k1gFnPc6	lázeň
Bourbon-l	Bourbona	k1gFnPc2	Bourbon-la
<g/>
'	'	kIx"	'
<g/>
Archambault	Archambaulta	k1gFnPc2	Archambaulta
<g/>
,	,	kIx,	,
situovaných	situovaný	k2eAgFnPc2d1	situovaná
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
departementu	departement	k1gInSc6	departement
Allier	Allier	k1gInSc1	Allier
v	v	k7c6	v
regionu	region	k1gInSc6	region
Auvergne	Auvergn	k1gInSc5	Auvergn
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
lázně	lázeň	k1gFnPc4	lázeň
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Dorothea	Dorothea	k1gMnSc1	Dorothea
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
strýc	strýc	k1gMnSc1	strýc
vévoda	vévoda	k1gMnSc1	vévoda
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gMnSc1	Talleyrand-Périgord
v	v	k7c6	v
září	září	k1gNnSc6	září
1816	[number]	k4	1816
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
již	již	k6eAd1	již
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
obou	dva	k4xCgMnPc2	dva
milenců	milenec	k1gMnPc2	milenec
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1816	[number]	k4	1816
<g/>
.	.	kIx.	.
</s>
<s>
Dorothea	Dorothea	k1gFnSc1	Dorothea
neuznala	uznat	k5eNaPmAgFnS	uznat
toto	tento	k3xDgNnSc4	tento
své	svůj	k3xOyFgNnSc4	svůj
nemanželské	manželský	k2eNgNnSc4d1	nemanželské
dítě	dítě	k1gNnSc4	dítě
oficiálně	oficiálně	k6eAd1	oficiálně
za	za	k7c4	za
vlastní	vlastní	k2eAgInPc4d1	vlastní
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
matriky	matrika	k1gFnSc2	matrika
města	město	k1gNnSc2	město
zanést	zanést	k5eAaPmF	zanést
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Marie-Henriette	Marie-Henriett	k1gInSc5	Marie-Henriett
Dessalles	Dessallesa	k1gFnPc2	Dessallesa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
alternativních	alternativní	k2eAgFnPc2d1	alternativní
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
původu	původ	k1gInSc6	původ
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
dívka	dívka	k1gFnSc1	dívka
později	pozdě	k6eAd2	pozdě
dána	dát	k5eAaPmNgFnS	dát
do	do	k7c2	do
opatrování	opatrování	k1gNnSc2	opatrování
manželům	manžel	k1gMnPc3	manžel
Panklovým	Panklová	k1gFnPc3	Panklová
<g/>
,	,	kIx,	,
služebníkům	služebník	k1gMnPc3	služebník
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Zaháňské	zaháňský	k2eAgFnSc2d1	Zaháňská
na	na	k7c6	na
ratibořickém	ratibořický	k2eAgNnSc6d1	Ratibořické
panství	panství	k1gNnSc6	panství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
strávila	strávit	k5eAaPmAgFnS	strávit
dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
první	první	k4xOgNnPc4	první
léta	léto	k1gNnPc4	léto
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
