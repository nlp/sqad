<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
literatury	literatura	k1gFnSc2	literatura
faktu	fakt	k1gInSc2	fakt
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dorothea	Dorothea	k1gFnSc1	Dorothea
von	von	k1gInSc1	von
Biron	Biron	k1gInSc1	Biron
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
hraběnka	hraběnka	k1gFnSc1	hraběnka
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgorda	k1gFnPc2	Talleyrand-Périgorda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
matkou	matka	k1gFnSc7	matka
české	český	k2eAgFnSc2d1	Česká
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
</s>
