<p>
<s>
Midžur	Midžur	k1gMnSc1	Midžur
(	(	kIx(	(
<g/>
bulharsky	bulharsky	k6eAd1	bulharsky
М	М	k?	М
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Midžor	Midžor	k1gInSc4	Midžor
(	(	kIx(	(
<g/>
srbsky	srbsky	k6eAd1	srbsky
М	М	k?	М
<g/>
,	,	kIx,	,
Midžor	Midžor	k1gInSc1	Midžor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Stara	Stara	k1gFnSc1	Stara
planina	planina	k1gFnSc1	planina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vrchol	vrchol	k1gInSc1	vrchol
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
2169	[number]	k4	2169
m	m	kA	m
leží	ležet	k5eAaImIp3nS	ležet
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
místem	místo	k1gNnSc7	místo
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
srbské	srbský	k2eAgFnSc2d1	Srbská
části	část	k1gFnSc2	část
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Kosova	Kosův	k2eAgMnSc2d1	Kosův
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Srbska	Srbsko	k1gNnSc2	Srbsko
kosovská	kosovský	k2eAgFnSc1d1	Kosovská
Djeravica	Djeravica	k1gFnSc1	Djeravica
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
leží	ležet	k5eAaImIp3nP	ležet
prameny	pramen	k1gInPc1	pramen
řek	řeka	k1gFnPc2	řeka
Trgoviški	Trgoviški	k1gNnSc2	Trgoviški
Timok	Timok	k1gInSc1	Timok
a	a	k8xC	a
Lom	lom	k1gInSc1	lom
<g/>
.	.	kIx.	.
</s>
<s>
Geologický	geologický	k2eAgInSc1d1	geologický
základ	základ	k1gInSc1	základ
Midžoru	Midžor	k1gInSc2	Midžor
tvoří	tvořit	k5eAaImIp3nS	tvořit
permské	permský	k2eAgFnSc3d1	Permská
červené	červený	k2eAgFnSc3d1	červená
pískovce	pískovka	k1gFnSc3	pískovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístup	přístup	k1gInSc1	přístup
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
přístupný	přístupný	k2eAgInSc1d1	přístupný
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgInS	být
přístup	přístup	k1gInSc1	přístup
zakázán	zakázán	k2eAgInSc1d1	zakázán
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrchol	vrchol	k1gInSc1	vrchol
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
hraniční	hraniční	k2eAgFnSc6d1	hraniční
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
těmto	tento	k3xDgNnPc3	tento
omezením	omezení	k1gNnPc3	omezení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
příroda	příroda	k1gFnSc1	příroda
kolem	kolem	k7c2	kolem
vrcholu	vrchol	k1gInSc2	vrchol
zachována	zachovat	k5eAaPmNgFnS	zachovat
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
bulharské	bulharský	k2eAgFnSc2d1	bulharská
strany	strana	k1gFnSc2	strana
lze	lze	k6eAd1	lze
nejsnáze	snadno	k6eAd3	snadno
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
od	od	k7c2	od
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
Čuprene	Čupren	k1gMnSc5	Čupren
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
srbské	srbský	k2eAgFnSc2d1	Srbská
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
buď	buď	k8xC	buď
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
Topli	Tople	k1gFnSc4	Tople
Dol	dol	k1gInSc4	dol
nebo	nebo	k8xC	nebo
od	od	k7c2	od
lyžařského	lyžařský	k2eAgNnSc2d1	lyžařské
střediska	středisko	k1gNnSc2	středisko
Babin	babin	k2eAgInSc4d1	babin
Zub	zub	k1gInSc4	zub
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
po	po	k7c6	po
hřebeni	hřeben	k1gInSc6	hřeben
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
(	(	kIx(	(
<g/>
od	od	k7c2	od
hotelu	hotel	k1gInSc2	hotel
Babin	babin	k2eAgInSc1d1	babin
Zub	zub	k1gInSc1	zub
zhruba	zhruba	k6eAd1	zhruba
8	[number]	k4	8
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výstup	výstup	k1gInSc4	výstup
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
žádných	žádný	k1gMnPc2	žádný
povolení	povolení	k1gNnPc2	povolení
ani	ani	k8xC	ani
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotografie	fotografia	k1gFnPc4	fotografia
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Midžor	Midžora	k1gFnPc2	Midžora
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Midžur	Midžura	k1gFnPc2	Midžura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Midžur	Midžur	k1gMnSc1	Midžur
na	na	k7c4	na
Tripio	Tripio	k1gNnSc4	Tripio
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Midžur	Midžur	k1gMnSc1	Midžur
na	na	k7c4	na
Summitpost	Summitpost	k1gFnSc4	Summitpost
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Midžur	Midžur	k1gMnSc1	Midžur
na	na	k7c4	na
Peakbagger	Peakbagger	k1gInSc4	Peakbagger
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Midžuru	Midžur	k1gInSc2	Midžur
na	na	k7c6	na
horském	horský	k2eAgNnSc6d1	horské
kole	kolo	k1gNnSc6	kolo
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
