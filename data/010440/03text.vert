<p>
<s>
Věnec	věnec	k1gInSc1	věnec
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
hmotný	hmotný	k2eAgInSc4d1	hmotný
objekt	objekt	k1gInSc4	objekt
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
toroidu	toroid	k1gInSc2	toroid
nebo	nebo	k8xC	nebo
prstence	prstenec	k1gInSc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
entity	entita	k1gFnPc4	entita
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
toroid	toroid	k1gInSc1	toroid
nebo	nebo	k8xC	nebo
prstenec	prstenec	k1gInSc1	prstenec
připomínají	připomínat	k5eAaImIp3nP	připomínat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věnec	věnec	k1gInSc1	věnec
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
symbolický	symbolický	k2eAgInSc4d1	symbolický
<g/>
,	,	kIx,	,
magický	magický	k2eAgInSc4d1	magický
nebo	nebo	k8xC	nebo
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xS	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
interiéru	interiér	k1gInSc2	interiér
i	i	k8xC	i
exteriéru	exteriér	k1gInSc2	exteriér
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
zvířat	zvíře	k1gNnPc2	zvíře
i	i	k8xC	i
předmětů	předmět	k1gInPc2	předmět
při	při	k7c6	při
slavnostech	slavnost	k1gFnPc6	slavnost
nebo	nebo	k8xC	nebo
být	být	k5eAaImF	být
sám	sám	k3xTgInSc4	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
v	v	k7c6	v
exteriéru	exteriér	k1gInSc6	exteriér
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Věnec	věnec	k1gInSc1	věnec
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
historický	historický	k2eAgInSc1d1	historický
symbol	symbol	k1gInSc1	symbol
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
vytvářen	vytvářit	k5eAaPmNgInS	vytvářit
z	z	k7c2	z
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
stálezelených	stálezelený	k2eAgInPc2d1	stálezelený
listnáčů	listnáč	k1gInPc2	listnáč
nebo	nebo	k8xC	nebo
uctívaných	uctívaný	k2eAgFnPc2d1	uctívaná
dřeviny	dřevina	k1gFnPc4	dřevina
nebo	nebo	k8xC	nebo
rostlin	rostlina	k1gFnPc2	rostlina
kterým	který	k3yRgMnPc3	který
je	být	k5eAaImIp3nS	být
přisuzována	přisuzovat	k5eAaImNgFnS	přisuzovat
magická	magický	k2eAgFnSc1d1	magická
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgInPc1d1	populární
jsou	být	k5eAaImIp3nP	být
vavřínové	vavřínový	k2eAgInPc1d1	vavřínový
věnce	věnec	k1gInPc1	věnec
<g/>
,	,	kIx,	,
věnce	věnec	k1gInPc1	věnec
ze	z	k7c2	z
jmelí	jmelí	k1gNnSc2	jmelí
<g/>
,	,	kIx,	,
dubu	dub	k1gInSc2	dub
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pampelišek	pampeliška	k1gFnPc2	pampeliška
a	a	k8xC	a
sedmikrásek	sedmikráska	k1gFnPc2	sedmikráska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Etruské	etruský	k2eAgInPc1d1	etruský
věnce	věnec	k1gInPc1	věnec
===	===	k?	===
</s>
</p>
<p>
<s>
Věnce	věnec	k1gInPc1	věnec
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
už	už	k6eAd1	už
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
památky	památka	k1gFnPc1	památka
etruské	etruský	k2eAgFnSc2d1	etruská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
šperky	šperk	k1gInPc1	šperk
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
z	z	k7c2	z
řeckých	řecký	k2eAgInPc2d1	řecký
mýtů	mýtus	k1gInPc2	mýtus
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
zobrazeny	zobrazit	k5eAaPmNgInP	zobrazit
s	s	k7c7	s
věnci	věnec	k1gInPc7	věnec
okolo	okolo	k7c2	okolo
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
římští	římský	k2eAgMnPc1d1	římský
spisovatelé	spisovatel	k1gMnPc1	spisovatel
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c4	o
etruské	etruský	k2eAgMnPc4d1	etruský
corona	corona	k1gFnSc1	corona
sutilis	sutilis	k1gFnSc1	sutilis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
věncem	věnec	k1gInSc7	věnec
s	s	k7c7	s
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
věnce	věnec	k1gInPc1	věnec
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
jako	jako	k8xC	jako
čelenka	čelenka	k1gFnSc1	čelenka
s	s	k7c7	s
tenkými	tenký	k2eAgInPc7d1	tenký
tepanými	tepaný	k2eAgInPc7d1	tepaný
listy	list	k1gInPc7	list
připojenými	připojený	k2eAgInPc7d1	připojený
k	k	k7c3	k
zdobenému	zdobený	k2eAgInSc3d1	zdobený
pásku	pásek	k1gInSc3	pásek
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Věnce	věnec	k1gInPc1	věnec
byly	být	k5eAaImAgInP	být
rovněž	rovněž	k9	rovněž
používány	používán	k2eAgInPc1d1	používán
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
vládce	vládce	k1gMnSc2	vládce
u	u	k7c2	u
etruských	etruský	k2eAgMnPc2d1	etruský
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rostlinami	rostlina	k1gFnPc7	rostlina
používanými	používaný	k2eAgFnPc7d1	používaná
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
etruských	etruský	k2eAgInPc2d1	etruský
šperků	šperk	k1gInPc2	šperk
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
věnců	věnec	k1gInPc2	věnec
patří	patřit	k5eAaImIp3nS	patřit
břečťan	břečťan	k1gInSc1	břečťan
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
oliva	oliva	k1gFnSc1	oliva
,	,	kIx,	,
myrta	myrta	k1gFnSc1	myrta
,	,	kIx,	,
vavřín	vavřín	k1gInSc1	vavřín
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc1	pšenice
a	a	k8xC	a
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
<s>
Etruská	etruský	k2eAgFnSc1d1	etruská
symbolika	symbolika	k1gFnSc1	symbolika
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
a	a	k8xC	a
užívána	užívat	k5eAaImNgFnS	užívat
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
soudci	soudce	k1gMnPc1	soudce
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
další	další	k2eAgInPc4d1	další
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
také	také	k9	také
nosili	nosit	k5eAaImAgMnP	nosit
zlaté	zlatý	k2eAgInPc4d1	zlatý
věnce	věnec	k1gInPc4	věnec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
věnců	věnec	k1gInPc2	věnec
podle	podle	k7c2	podle
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
==	==	k?	==
</s>
</p>
<p>
<s>
Aranžování	aranžování	k1gNnSc1	aranžování
květin	květina	k1gFnPc2	květina
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
typy	typ	k1gInPc4	typ
věnce	věnec	k1gInSc2	věnec
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vypichovaný	vypichovaný	k2eAgInSc1d1	vypichovaný
věnec	věnec	k1gInSc1	věnec
je	být	k5eAaImIp3nS	být
vypichován	vypichovat	k5eAaImNgInS	vypichovat
částmi	část	k1gFnPc7	část
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
větvičkami	větvička	k1gFnPc7	větvička
<g/>
)	)	kIx)	)
navázanými	navázaný	k2eAgFnPc7d1	navázaná
na	na	k7c4	na
drát	drát	k1gInSc4	drát
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
"	"	kIx"	"
<g/>
kytiček	kytička	k1gFnPc2	kytička
<g/>
"	"	kIx"	"
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
,	,	kIx,	,
skupin	skupina	k1gFnPc2	skupina
větviček	větvička	k1gFnPc2	větvička
navázaných	navázaný	k2eAgFnPc2d1	navázaná
společně	společně	k6eAd1	společně
na	na	k7c4	na
drát	drát	k1gInSc4	drát
a	a	k8xC	a
vypichovaných	vypichovaný	k2eAgMnPc2d1	vypichovaný
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kytičky	kytička	k1gFnPc1	kytička
<g/>
"	"	kIx"	"
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
výšku	výška	k1gFnSc4	výška
<g/>
.	.	kIx.	.
</s>
<s>
Materiály	materiál	k1gInPc1	materiál
se	se	k3xPyFc4	se
vypichují	vypichovat	k5eAaImIp3nP	vypichovat
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
věnce	věnec	k1gInSc2	věnec
(	(	kIx(	(
<g/>
kytičky	kytička	k1gFnSc2	kytička
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
podél	podél	k7c2	podél
těla	tělo	k1gNnSc2	tělo
věnce	věnec	k1gInSc2	věnec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vypichování	vypichování	k1gNnSc6	vypichování
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
kůra	kůra	k1gFnSc1	kůra
<g/>
,	,	kIx,	,
kameny	kámen	k1gInPc1	kámen
apod.	apod.	kA	apod.
Navázané	navázaný	k2eAgInPc1d1	navázaný
materiály	materiál	k1gInPc1	materiál
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
konec	konec	k1gInSc4	konec
drátu	drát	k1gInSc2	drát
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
"	"	kIx"	"
<g/>
nožičkami	nožička	k1gFnPc7	nožička
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zapichují	zapichovat	k5eAaImIp3nP	zapichovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skobičkovaný	Skobičkovaný	k2eAgInSc1d1	Skobičkovaný
věnec	věnec	k1gInSc1	věnec
je	být	k5eAaImIp3nS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
materiál	materiál	k1gInSc4	materiál
pokrývající	pokrývající	k2eAgNnSc1d1	pokrývající
tělo	tělo	k1gNnSc1	tělo
(	(	kIx(	(
<g/>
korpus	korpus	k1gInSc1	korpus
<g/>
)	)	kIx)	)
věnce	věnec	k1gInSc2	věnec
<g/>
,	,	kIx,	,
omotaného	omotaný	k2eAgInSc2d1	omotaný
začišťovacím	začišťovací	k2eAgInSc7d1	začišťovací
materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
připevněn	připevněn	k2eAgMnSc1d1	připevněn
skobičkami	skobička	k1gFnPc7	skobička
nebo	nebo	k8xC	nebo
špendlíky	špendlík	k1gInPc7	špendlík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Omotávaný	omotávaný	k2eAgInSc1d1	omotávaný
věnec	věnec	k1gInSc1	věnec
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
věnce	věnec	k1gInSc2	věnec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
materiál	materiál	k1gInSc4	materiál
na	na	k7c4	na
korpus	korpus	k1gInSc4	korpus
upevněn	upevněn	k2eAgInSc4d1	upevněn
omotáváním	omotávání	k1gNnPc3	omotávání
drátem	drát	k1gInSc7	drát
nebo	nebo	k8xC	nebo
provázkem	provázek	k1gInSc7	provázek
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
věnce	věnec	k1gInPc1	věnec
podmotávány	podmotáván	k2eAgInPc1d1	podmotáván
<g/>
,	,	kIx,	,
věnec	věnec	k1gInSc1	věnec
je	být	k5eAaImIp3nS	být
omotán	omotat	k5eAaPmNgInS	omotat
nejdříve	dříve	k6eAd3	dříve
méně	málo	k6eAd2	málo
hodnotným	hodnotný	k2eAgInSc7d1	hodnotný
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
překryt	překrýt	k5eAaPmNgInS	překrýt
dražším	drahý	k2eAgInSc7d2	dražší
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
omotávané	omotávaný	k2eAgInPc1d1	omotávaný
věnce	věnec	k1gInPc1	věnec
pokryty	pokryt	k1gInPc4	pokryt
dekorativním	dekorativní	k2eAgInSc7d1	dekorativní
materiálem	materiál	k1gInSc7	materiál
celé	celá	k1gFnSc2	celá
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
<g/>
,	,	kIx,	,
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g/>
,	,	kIx,	,
jen	jen	k9	jen
viditelná	viditelný	k2eAgFnSc1d1	viditelná
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
věnce	věnec	k1gInSc2	věnec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lepený	lepený	k2eAgInSc1d1	lepený
věnec	věnec	k1gInSc1	věnec
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoduše	jednoduše	k6eAd1	jednoduše
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
zhotovitelný	zhotovitelný	k2eAgInSc4d1	zhotovitelný
druh	druh	k1gInSc4	druh
věnce	věnec	k1gInSc2	věnec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
dekorace	dekorace	k1gFnSc1	dekorace
lepena	lepen	k2eAgFnSc1d1	lepena
na	na	k7c4	na
podklad	podklad	k1gInSc4	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Podklad	podklad	k1gInSc1	podklad
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
odolný	odolný	k2eAgInSc1d1	odolný
použitému	použitý	k2eAgInSc3d1	použitý
druhu	druh	k1gInSc3	druh
lepidla	lepidlo	k1gNnSc2	lepidlo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
deformován	deformovat	k5eAaImNgInS	deformovat
teplem	teplo	k1gNnSc7	teplo
<g/>
,	,	kIx,	,
povrch	povrch	k1gInSc1	povrch
dekorace	dekorace	k1gFnSc2	dekorace
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
poškozen	poškodit	k5eAaPmNgInS	poškodit
a	a	k8xC	a
potřísněn	potřísnit	k5eAaPmNgInS	potřísnit
lepidlem	lepidlo	k1gNnSc7	lepidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proplétaný	proplétaný	k2eAgInSc1d1	proplétaný
věnec	věnec	k1gInSc1	věnec
je	být	k5eAaImIp3nS	být
zhotovený	zhotovený	k2eAgInSc1d1	zhotovený
proplétáním	proplétání	k1gNnPc3	proplétání
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
dřevin	dřevina	k1gFnPc2	dřevina
vhodného	vhodný	k2eAgInSc2d1	vhodný
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
vrby	vrba	k1gFnSc2	vrba
<g/>
,	,	kIx,	,
líska	líska	k1gFnSc1	líska
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Navlékaný	navlékaný	k2eAgInSc1d1	navlékaný
věnec	věnec	k1gInSc1	věnec
se	se	k3xPyFc4	se
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
navlékáním	navlékání	k1gNnSc7	navlékání
materiálů	materiál	k1gInPc2	materiál
na	na	k7c4	na
drát	drát	k1gInSc4	drát
nebo	nebo	k8xC	nebo
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
použity	použit	k2eAgInPc4d1	použit
listy	list	k1gInPc4	list
a	a	k8xC	a
plody	plod	k1gInPc4	plod
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc4	jejich
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vitý	vitý	k2eAgInSc1d1	vitý
věnec	věnec	k1gInSc1	věnec
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
navazováním	navazování	k1gNnSc7	navazování
materiálu	materiál	k1gInSc2	materiál
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
podklad	podklad	k1gInSc4	podklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přízdoba	přízdoba	k1gFnSc1	přízdoba
věnce	věnec	k1gInSc2	věnec
==	==	k?	==
</s>
</p>
<p>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
Floristika	floristika	k1gFnSc1	floristika
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přízdoba	přízdoba	k1gFnSc1	přízdoba
věnce	věnec	k1gInPc1	věnec
kopírovala	kopírovat	k5eAaImAgFnS	kopírovat
jeho	on	k3xPp3gInSc4	on
tvar	tvar	k1gInSc4	tvar
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
přílišné	přílišný	k2eAgNnSc1d1	přílišné
vybočení	vybočení	k1gNnSc1	vybočení
narušilo	narušit	k5eAaPmAgNnS	narušit
jeho	jeho	k3xOp3gFnSc4	jeho
symboliku	symbolika	k1gFnSc4	symbolika
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
přízdoby	přízdoba	k1gFnSc2	přízdoba
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
tvořit	tvořit	k5eAaImF	tvořit
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
korpusu	korpus	k1gInSc2	korpus
věnce	věnec	k1gInSc2	věnec
a	a	k8xC	a
výškou	výška	k1gFnSc7	výška
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
2	[number]	k4	2
<g/>
x	x	k?	x
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přízdoba	přízdoba	k1gFnSc1	přízdoba
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
nebo	nebo	k8xC	nebo
dvou	dva	k4xCgInPc6	dva
bodech	bod	k1gInPc6	bod
korpusu	korpus	k1gInSc2	korpus
věnce	věnec	k1gInPc4	věnec
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
vyvýšená	vyvýšený	k2eAgFnSc1d1	vyvýšená
oproti	oproti	k7c3	oproti
korpusu	korpus	k1gInSc3	korpus
<g/>
,	,	kIx,	,
podložená	podložený	k2eAgFnSc1d1	podložená
chvojím	chvojí	k1gNnSc7	chvojí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
hlava	hlava	k1gFnSc1	hlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ozdoba	ozdoba	k1gFnSc1	ozdoba
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
na	na	k7c4	na
věnce	věnec	k1gInPc4	věnec
s	s	k7c7	s
jinak	jinak	k6eAd1	jinak
nezdobeným	zdobený	k2eNgNnSc7d1	nezdobené
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
věnci	věnec	k1gInSc6	věnec
lze	lze	k6eAd1	lze
rovněž	rovněž	k9	rovněž
připevnit	připevnit	k5eAaPmF	připevnit
nebo	nebo	k8xC	nebo
vytvořit	vytvořit	k5eAaPmF	vytvořit
dekoraci	dekorace	k1gFnSc4	dekorace
připomínající	připomínající	k2eAgFnSc4d1	připomínající
položenou	položený	k2eAgFnSc4d1	položená
kytici	kytice	k1gFnSc4	kytice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vhodné	vhodný	k2eAgNnSc4d1	vhodné
ozdobení	ozdobení	k1gNnSc4	ozdobení
věnce	věnec	k1gInSc2	věnec
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
formálně	formálně	k6eAd1	formálně
lineární	lineární	k2eAgFnSc1d1	lineární
ozdoba	ozdoba	k1gFnSc1	ozdoba
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
a	a	k8xC	a
zákazníky	zákazník	k1gMnPc4	zákazník
preferované	preferovaný	k2eAgInPc1d1	preferovaný
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
věnce	věnec	k1gInPc1	věnec
zdobené	zdobený	k2eAgInPc1d1	zdobený
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
ploše	plocha	k1gFnSc6	plocha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
dražší	drahý	k2eAgInPc1d2	dražší
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
úprava	úprava	k1gFnSc1	úprava
je	být	k5eAaImIp3nS	být
také	také	k9	také
technicky	technicky	k6eAd1	technicky
vhodná	vhodný	k2eAgFnSc1d1	vhodná
vhodně	vhodně	k6eAd1	vhodně
skryje	skrýt	k5eAaPmIp3nS	skrýt
vady	vada	k1gFnPc1	vada
provedení	provedení	k1gNnSc2	provedení
namotávaného	namotávaný	k2eAgInSc2d1	namotávaný
věnce	věnec	k1gInSc2	věnec
a	a	k8xC	a
případné	případný	k2eAgInPc4d1	případný
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlava	hlava	k1gFnSc1	hlava
===	===	k?	===
</s>
</p>
<p>
<s>
Klasické	klasický	k2eAgInPc1d1	klasický
věnce	věnec	k1gInPc1	věnec
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
podloženou	podložený	k2eAgFnSc7d1	podložená
chvojím	chvojí	k1gNnSc7	chvojí
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
smuteční	smuteční	k2eAgFnSc4d1	smuteční
vazbu	vazba	k1gFnSc4	vazba
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
)	)	kIx)	)
i	i	k8xC	i
jako	jako	k9	jako
slavnostní	slavnostní	k2eAgInPc1d1	slavnostní
věnce	věnec	k1gInPc1	věnec
k	k	k7c3	k
památníku	památník	k1gInSc3	památník
zesnulých	zesnulá	k1gFnPc2	zesnulá
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
učebnice	učebnice	k1gFnPc4	učebnice
floristiky	floristika	k1gFnSc2	floristika
tlumené	tlumený	k2eAgFnSc2d1	tlumená
barvy	barva	k1gFnSc2	barva
použité	použitý	k2eAgFnSc2d1	použitá
výzdoby	výzdoba	k1gFnSc2	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
vkusné	vkusný	k2eAgFnPc4d1	vkusná
kombinace	kombinace	k1gFnPc4	kombinace
výrazných	výrazný	k2eAgFnPc2d1	výrazná
barev	barva	k1gFnPc2	barva
s	s	k7c7	s
výraznými	výrazný	k2eAgInPc7d1	výrazný
kontrasty	kontrast	k1gInPc7	kontrast
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
podporováno	podporován	k2eAgNnSc1d1	podporováno
poptávkou	poptávka	k1gFnSc7	poptávka
zákazníků	zákazník	k1gMnPc2	zákazník
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
ozdob	ozdoba	k1gFnPc2	ozdoba
<g/>
.	.	kIx.	.
<g/>
Hlava	hlava	k1gFnSc1	hlava
bývá	bývat	k5eAaImIp3nS	bývat
zdobena	zdobit	k5eAaImNgFnS	zdobit
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
dekorativním	dekorativní	k2eAgInSc6d1	dekorativní
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
často	často	k6eAd1	často
doplněna	doplnit	k5eAaPmNgFnS	doplnit
stuhou	stuha	k1gFnSc7	stuha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
plocha	plocha	k1gFnSc1	plocha
věnce	věnec	k1gInSc2	věnec
jednoduše	jednoduše	k6eAd1	jednoduše
přizdobena	přizdoben	k2eAgFnSc1d1	přizdobena
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
taková	takový	k3xDgFnSc1	takový
ozdoba	ozdoba	k1gFnSc1	ozdoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
obdobu	obdoba	k1gFnSc4	obdoba
"	"	kIx"	"
<g/>
hlavy	hlava	k1gFnSc2	hlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
někde	někde	k6eAd1	někde
nazývána	nazýván	k2eAgFnSc1d1	nazývána
"	"	kIx"	"
<g/>
asymetrická	asymetrický	k2eAgFnSc1d1	asymetrická
hlava	hlava	k1gFnSc1	hlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
věneček	věneček	k1gInSc1	věneček
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Wreath	Wreatha	k1gFnPc2	Wreatha
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
věnec	věnec	k1gInSc4	věnec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
