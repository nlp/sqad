<s>
Máj	máj	k1gInSc1	máj
je	být	k5eAaImIp3nS	být
inspirativním	inspirativní	k2eAgNnSc7d1	inspirativní
<g/>
,	,	kIx,	,
meditativním	meditativní	k2eAgNnSc7d1	meditativní
dílem	dílo	k1gNnSc7	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sice	sice	k8xC	sice
těží	těžet	k5eAaImIp3nP	těžet
z	z	k7c2	z
motivů	motiv	k1gInPc2	motiv
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jehož	jehož	k3xOyRp3gNnSc1	jehož
myšlenkové	myšlenkový	k2eAgNnSc1d1	myšlenkové
zázemí	zázemí	k1gNnSc1	zázemí
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
otázek	otázka	k1gFnPc2	otázka
spíše	spíše	k9	spíše
metafyzických	metafyzický	k2eAgNnPc2d1	metafyzické
(	(	kIx(	(
<g/>
především	především	k9	především
druhý	druhý	k4xOgInSc1	druhý
zpěv	zpěv	k1gInSc1	zpěv
skladby	skladba	k1gFnSc2	skladba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
