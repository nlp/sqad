<p>
<s>
Hantec	Hantec	k1gInSc1	Hantec
je	být	k5eAaImIp3nS	být
místní	místní	k2eAgFnSc1d1	místní
varieta	varieta	k1gFnSc1	varieta
češtiny	čeština	k1gFnSc2	čeština
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
mluvě	mluva	k1gFnSc6	mluva
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
specifická	specifický	k2eAgFnSc1d1	specifická
mluva	mluva	k1gFnSc1	mluva
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
během	během	k7c2	během
několika	několik	k4yIc2	několik
staletí	staletí	k1gNnPc2	staletí
smíšením	smíšení	k1gNnSc7	smíšení
hanáckých	hanácký	k2eAgNnPc2d1	Hanácké
nářečí	nářečí	k1gNnPc2	nářečí
češtiny	čeština	k1gFnSc2	čeština
s	s	k7c7	s
němčinou	němčina	k1gFnSc7	němčina
brněnských	brněnský	k2eAgMnPc2d1	brněnský
Němců	Němec	k1gMnPc2	Němec
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
spisovné	spisovný	k2eAgFnSc2d1	spisovná
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
rakouských	rakouský	k2eAgFnPc2d1	rakouská
a	a	k8xC	a
jihoněmeckých	jihoněmecký	k2eAgNnPc2d1	jihoněmecké
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
)	)	kIx)	)
a	a	k8xC	a
historickým	historický	k2eAgInSc7d1	historický
středoevropským	středoevropský	k2eAgInSc7d1	středoevropský
argotem	argot	k1gInSc7	argot
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vídeňským	vídeňský	k2eAgMnSc7d1	vídeňský
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc4	vliv
také	také	k9	také
měly	mít	k5eAaImAgFnP	mít
jidiš	jidiš	k1gNnSc4	jidiš
<g/>
,	,	kIx,	,
romština	romština	k1gFnSc1	romština
a	a	k8xC	a
italština	italština	k1gFnSc1	italština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
hantec	hantec	k1gInSc1	hantec
běžně	běžně	k6eAd1	běžně
používán	používat	k5eAaImNgInS	používat
nižšími	nízký	k2eAgFnPc7d2	nižší
společenskými	společenský	k2eAgFnPc7d1	společenská
třídami	třída	k1gFnPc7	třída
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
původní	původní	k2eAgFnSc1d1	původní
forma	forma	k1gFnSc1	forma
hantecu	hantecu	k5eAaPmIp1nS	hantecu
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
výrazů	výraz	k1gInPc2	výraz
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lokální	lokální	k2eAgFnSc6d1	lokální
mluvě	mluva	k1gFnSc6	mluva
obecně	obecně	k6eAd1	obecně
používáno	používán	k2eAgNnSc1d1	používáno
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gMnSc1	původ
slova	slovo	k1gNnSc2	slovo
hantec	hantec	k1gMnSc1	hantec
je	on	k3xPp3gInPc4	on
možno	možno	k6eAd1	možno
hledat	hledat	k5eAaImF	hledat
ve	v	k7c6	v
výrazu	výraz	k1gInSc6	výraz
hantýrka	hantýrka	k1gFnSc1	hantýrka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnPc4d3	nejznámější
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
možno	možno	k6eAd1	možno
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
šalina	šalina	k1gFnSc1	šalina
(	(	kIx(	(
<g/>
tramvaj	tramvaj	k1gFnSc1	tramvaj
-	-	kIx~	-
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
výrazu	výraz	k1gInSc2	výraz
Elektrische	Elektrische	k1gFnSc1	Elektrische
Linie	linie	k1gFnSc1	linie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čurina	čurina	k1gFnSc1	čurina
(	(	kIx(	(
<g/>
legrace	legrace	k1gFnSc1	legrace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokna	hokna	k1gFnSc1	hokna
(	(	kIx(	(
<g/>
práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zoncna	zoncna	k1gFnSc1	zoncna
(	(	kIx(	(
<g/>
slunce	slunce	k1gNnSc1	slunce
–	–	k?	–
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
<g />
.	.	kIx.	.
</s>
<s>
Sonne	Sonnout	k5eAaPmIp3nS	Sonnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prygl	Prygl	k1gInSc1	Prygl
(	(	kIx(	(
<g/>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
peníze	peníz	k1gInPc1	peníz
–	–	k?	–
z	z	k7c2	z
romštiny	romština	k1gFnSc2	romština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čórka	čórka	k1gFnSc1	čórka
(	(	kIx(	(
<g/>
krádež	krádež	k1gFnSc1	krádež
–	–	k?	–
z	z	k7c2	z
romštiny	romština	k1gFnSc2	romština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šaškec	šaškec	k1gInSc1	šaškec
(	(	kIx(	(
<g/>
blázinec	blázinec	k1gInSc1	blázinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
škopek	škopek	k1gInSc1	škopek
(	(	kIx(	(
<g/>
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koc	koc	k?	koc
(	(	kIx(	(
<g/>
dívka	dívka	k1gFnSc1	dívka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
poslední	poslední	k2eAgNnSc1d1	poslední
slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rakouské	rakouský	k2eAgFnSc2d1	rakouská
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
mluvě	mluva	k1gFnSc6	mluva
se	se	k3xPyFc4	se
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
běžně	běžně	k6eAd1	běžně
psané	psaný	k2eAgFnSc2d1	psaná
a	a	k8xC	a
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
o	o	k7c4	o
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
slovo	slovo	k1gNnSc1	slovo
Katze	Katze	k1gFnSc2	Katze
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
<g/>
)	)	kIx)	)
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
Rakušan	Rakušan	k1gMnSc1	Rakušan
jako	jako	k8xC	jako
Koce	koka	k1gFnSc6	koka
nebo	nebo	k8xC	nebo
Koc	Koc	k1gFnSc6	Koc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgInPc1d1	hlavní
znaky	znak	k1gInPc1	znak
==	==	k?	==
</s>
</p>
<p>
<s>
změna	změna	k1gFnSc1	změna
ý	ý	k?	ý
>	>	kIx)	>
é	é	k0	é
(	(	kIx(	(
<g/>
býk	býk	k1gMnSc1	býk
>	>	kIx)	>
bék	bék	k?	bék
<g/>
)	)	kIx)	)
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
ej	ej	k0	ej
>	>	kIx)	>
é	é	k0	é
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
>	>	kIx)	>
nélepší	nélepší	k2eAgMnPc4d1	nélepší
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
změna	změna	k1gFnSc1	změna
é	é	k0	é
>	>	kIx)	>
ý	ý	k?	ý
/	/	kIx~	/
í	í	k0	í
(	(	kIx(	(
<g/>
dobré	dobrý	k2eAgNnSc1d1	dobré
>	>	kIx)	>
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
nářečích	nářečí	k1gNnPc6	nářečí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
protetické	protetický	k2eAgNnSc1d1	protetické
v-	v-	k?	v-
u	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
začínajících	začínající	k2eAgFnPc2d1	začínající
na	na	k7c4	na
o-	o-	k?	o-
(	(	kIx(	(
<g/>
okno	okno	k1gNnSc1	okno
>	>	kIx)	>
vokno	vokno	k1gNnSc1	vokno
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
nářečích	nářečí	k1gNnPc6	nářečí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
regresivní	regresivní	k2eAgFnSc1d1	regresivní
asimilace	asimilace	k1gFnSc1	asimilace
znělosti	znělost	k1gFnSc2	znělost
sh-	sh-	k?	sh-
na	na	k7c4	na
[	[	kIx(	[
<g/>
zh-	zh-	k?	zh-
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
zhoda	zhodo	k1gNnPc4	zhodo
<g/>
,	,	kIx,	,
zhánět	zhánět	k5eAaPmF	zhánět
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
skupina	skupina	k1gFnSc1	skupina
-jd-	-jd-	k?	-jd-
se	se	k3xPyFc4	se
v	v	k7c6	v
předponových	předponový	k2eAgInPc6d1	předponový
tvarech	tvar	k1gInPc6	tvar
slovesa	sloveso	k1gNnSc2	sloveso
jít	jít	k5eAaImF	jít
mění	měnit	k5eAaImIp3nP	měnit
na	na	k7c6	na
-nd-	-nd-	k?	-nd-
(	(	kIx(	(
<g/>
přijde	přijít	k5eAaPmIp3nS	přijít
>	>	kIx)	>
přinde	přind	k1gMnSc5	přind
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
substantiva	substantivum	k1gNnPc1	substantivum
skloňovaná	skloňovaný	k2eAgNnPc1d1	skloňované
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
předseda	předseda	k1gMnSc1	předseda
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
instrumentálu	instrumentál	k1gInSc6	instrumentál
singuláru	singulár	k1gInSc2	singulár
koncovku	koncovka	k1gFnSc4	koncovka
-em	-em	k?	-em
(	(	kIx(	(
<g/>
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
>	>	kIx)	>
s	s	k7c7	s
předsedem	předsedem	k?	předsedem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
instrumentál	instrumentál	k1gInSc1	instrumentál
plurálu	plurál	k1gInSc2	plurál
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
zakončen	zakončit	k5eAaPmNgInS	zakončit
na	na	k7c4	na
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
ma	ma	k?	ma
(	(	kIx(	(
<g/>
s	s	k7c7	s
vysokéma	vysokéma	k?	vysokéma
chlapama	chlapama	k?	chlapama
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dvojhláska	dvojhláska	k1gFnSc1	dvojhláska
ou	ou	k0	ou
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
ó	ó	k0	ó
(	(	kIx(	(
<g/>
spadnout	spadnout	k5eAaPmF	spadnout
>	>	kIx)	>
spadnót	spadnót	k5eAaPmF	spadnót
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dativ	dativ	k1gInSc1	dativ
a	a	k8xC	a
lokativ	lokativ	k1gInSc1	lokativ
pronomina	pronomen	k1gNnSc2	pronomen
já	já	k3xPp1nSc1	já
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
ně	on	k3xPp3gMnPc4	on
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
osoba	osoba	k1gFnSc1	osoba
plurálu	plurál	k1gInSc2	plurál
indikativu	indikativ	k1gInSc2	indikativ
prezentu	prezentu	k?	prezentu
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
na	na	k7c6	na
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
ij	ij	k?	ij
<g/>
)	)	kIx)	)
<g/>
ó	ó	k0	ó
(	(	kIx(	(
<g/>
dělajó	dělajó	k?	dělajó
<g/>
,	,	kIx,	,
mluvijó	mluvijó	k?	mluvijó
<g/>
,	,	kIx,	,
só	só	k?	só
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
osoba	osoba	k1gFnSc1	osoba
singuláru	singulár	k1gInSc2	singulár
indikativu	indikativ	k1gInSc2	indikativ
prézentu	prézens	k1gInSc2	prézens
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
3	[number]	k4	3
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
na	na	k7c6	na
-u	-u	k?	-u
(	(	kIx(	(
<g/>
pracuju	pracovat	k5eAaImIp1nS	pracovat
<g/>
,	,	kIx,	,
chcu	chcu	k?	chcu
<g/>
,	,	kIx,	,
maluju	malovat	k5eAaImIp1nS	malovat
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
slovesa	sloveso	k1gNnSc2	sloveso
chtít	chtít	k5eAaImF	chtít
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
chcel	chcel	k1gInSc1	chcel
<g/>
;	;	kIx,	;
přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
chcu	chcu	k?	chcu
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
osoba	osoba	k1gFnSc1	osoba
singuláru	singulár	k1gInSc2	singulár
indikativu	indikativ	k1gInSc2	indikativ
slovesa	sloveso	k1gNnSc2	sloveso
být	být	k5eAaImF	být
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
</s>
</p>
<p>
<s>
su	su	k?	su
jako	jako	k9	jako
plnovýznamové	plnovýznamový	k2eAgNnSc4d1	plnovýznamové
sloveso	sloveso	k1gNnSc4	sloveso
</s>
</p>
<p>
<s>
sem	sem	k6eAd1	sem
jako	jako	k9	jako
pomocné	pomocný	k2eAgNnSc4d1	pomocné
sloveso	sloveso	k1gNnSc4	sloveso
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázka	ukázka	k1gFnSc1	ukázka
==	==	k?	==
</s>
</p>
<p>
<s>
Úryvek	úryvek	k1gInSc1	úryvek
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
brněnských	brněnský	k2eAgFnPc2d1	brněnská
pověstí	pověst	k1gFnPc2	pověst
"	"	kIx"	"
<g/>
O	o	k7c6	o
křivé	křivý	k2eAgFnSc6d1	křivá
věži	věž	k1gFnSc6	věž
<g/>
"	"	kIx"	"
napsané	napsaný	k2eAgNnSc1d1	napsané
v	v	k7c6	v
hantecu	hantecum	k1gNnSc6	hantecum
od	od	k7c2	od
Aleše	Aleš	k1gMnSc2	Aleš
"	"	kIx"	"
<g/>
Agi	Agi	k1gMnSc2	Agi
<g/>
"	"	kIx"	"
Bojanovského	Bojanovského	k2eAgMnSc2d1	Bojanovského
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
==	==	k?	==
</s>
</p>
<p>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
hovorová	hovorový	k2eAgFnSc1d1	hovorová
mluva	mluva	k1gFnSc1	mluva
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
nářečí	nářečí	k1gNnPc2	nářečí
svými	svůj	k3xOyFgInPc7	svůj
typickými	typický	k2eAgInPc7d1	typický
výrazy	výraz	k1gInPc7	výraz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dodnes	dodnes	k6eAd1	dodnes
vznikají	vznikat	k5eAaImIp3nP	vznikat
nově	nově	k6eAd1	nově
a	a	k8xC	a
pro	pro	k7c4	pro
zbytek	zbytek	k1gInSc4	zbytek
země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
nesrozumitelné	srozumitelný	k2eNgFnPc1d1	nesrozumitelná
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dokonce	dokonce	k9	dokonce
jinde	jinde	k6eAd1	jinde
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
urážlivé	urážlivý	k2eAgInPc4d1	urážlivý
-	-	kIx~	-
jako	jako	k8xC	jako
například	například	k6eAd1	například
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
dívku	dívka	k1gFnSc4	dívka
Mařka	Mařka	k1gFnSc1	Mařka
<g/>
,	,	kIx,	,
Havajka	Havajka	k1gFnSc1	Havajka
<g/>
..	..	k?	..
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
Čech	Čechy	k1gFnPc2	Čechy
–	–	k?	–
cajzl	cajznout	k5eAaPmAgMnS	cajznout
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
cajzl	cajznout	k5eAaPmAgInS	cajznout
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Němčiny	němčina	k1gFnSc2	němčina
(	(	kIx(	(
<g/>
der	drát	k5eAaImRp2nS	drát
Zeisig	Zeisig	k1gInSc1	Zeisig
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
čížek	čížek	k1gMnSc1	čížek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Brno	Brno	k1gNnSc4	Brno
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xS	jako
hanlivý	hanlivý	k2eAgInSc1d1	hanlivý
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
projevují	projevovat	k5eAaImIp3nP	projevovat
také	také	k9	také
rysy	rys	k1gInPc4	rys
všech	všecek	k3xTgNnPc2	všecek
středomoravských	středomoravský	k2eAgNnPc2d1	středomoravské
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
že	že	k8xS	že
se	se	k3xPyFc4	se
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
čísla	číslo	k1gNnSc2	číslo
jednotného	jednotný	k2eAgNnSc2d1	jednotné
a	a	k8xC	a
vzor	vzor	k1gInSc1	vzor
předseda	předseda	k1gMnSc1	předseda
se	se	k3xPyFc4	se
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
dle	dle	k7c2	dle
předseda	předseda	k1gMnSc1	předseda
–	–	k?	–
s	s	k7c7	s
předsedem	předsedem	k?	předsedem
místo	místo	k6eAd1	místo
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
možné	možný	k2eAgNnSc1d1	možné
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
máme	mít	k5eAaImIp1nP	mít
hodinu	hodina	k1gFnSc4	hodina
matematiky	matematika	k1gFnSc2	matematika
s	s	k7c7	s
Kaňkem	Kaňkem	k?	Kaňkem
místo	místo	k7c2	místo
spisovného	spisovný	k2eAgNnSc2d1	spisovné
s	s	k7c7	s
Kaňkou	kaňka	k1gFnSc7	kaňka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mám	mít	k5eAaImIp1nS	mít
za	za	k7c4	za
kamoša	kamoša	k?	kamoša
Jirky	Jirka	k1gFnPc4	Jirka
místo	místo	k7c2	místo
mám	mít	k5eAaImIp1nS	mít
za	za	k7c4	za
kamaráda	kamarád	k1gMnSc4	kamarád
Jirku	Jirka	k1gMnSc4	Jirka
atd.	atd.	kA	atd.
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
četné	četný	k2eAgInPc4d1	četný
germanismy	germanismus	k1gInPc4	germanismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
výpůjčky	výpůjčka	k1gFnPc4	výpůjčka
z	z	k7c2	z
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
maďarštiny	maďarština	k1gFnSc2	maďarština
<g/>
,	,	kIx,	,
jidiš	jidiš	k1gNnSc2	jidiš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
například	například	k6eAd1	například
romštiny	romština	k1gFnSc2	romština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VESPALEC	VESPALEC	kA	VESPALEC
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
výrazových	výrazový	k2eAgInPc2d1	výrazový
prostředků	prostředek	k1gInPc2	prostředek
hantecu	hantecu	k5eAaPmIp1nS	hantecu
mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
práce	práce	k1gFnSc1	práce
Marie	Maria	k1gFnSc2	Maria
Krčmová	krčmový	k2eAgFnSc1d1	Krčmová
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hantec	hantec	k1gInSc1	hantec
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
http://www.hantec.cz/	[url]	k?	http://www.hantec.cz/
stránky	stránka	k1gFnSc2	stránka
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
hantecu	hantecu	k6eAd1	hantecu
</s>
</p>
<p>
<s>
http://www.hantec-pgnext.estranky.cz/clanky/slovnik-brnenskeho-hantecu.html	[url]	k1gInSc1	http://www.hantec-pgnext.estranky.cz/clanky/slovnik-brnenskeho-hantecu.html
slovník	slovník	k1gInSc1	slovník
hantecu	hantecu	k5eAaPmIp1nS	hantecu
</s>
</p>
