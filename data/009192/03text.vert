<p>
<s>
Belcanto	Belcanta	k1gFnSc5	Belcanta
či	či	k8xC	či
bel	bel	k1gInSc1	bel
canto	canto	k1gNnSc1	canto
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
krásný	krásný	k2eAgInSc1d1	krásný
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pěvecká	pěvecký	k2eAgFnSc1d1	pěvecká
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
počátkem	počátkem	k7c2	počátkem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
monodie	monodie	k1gFnSc2	monodie
a	a	k8xC	a
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
až	až	k9	až
asi	asi	k9	asi
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
převládající	převládající	k2eAgFnSc7d1	převládající
technikou	technika	k1gFnSc7	technika
evropské	evropský	k2eAgFnSc2d1	Evropská
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
legátovým	legátův	k2eAgNnSc7d1	legátův
vedením	vedení	k1gNnSc7	vedení
hlasu	hlas	k1gInSc2	hlas
<g/>
,	,	kIx,	,
použitím	použití	k1gNnSc7	použití
techniky	technika	k1gFnSc2	technika
messa	messa	k1gFnSc1	messa
di	di	k?	di
voce	voc	k1gFnSc2	voc
(	(	kIx(	(
<g/>
změnou	změna	k1gFnSc7	změna
síly	síla	k1gFnSc2	síla
hlasu	hlas	k1gInSc2	hlas
při	při	k7c6	při
zpívání	zpívání	k1gNnSc6	zpívání
jednoho	jeden	k4xCgMnSc2	jeden
tónu	tón	k1gInSc2	tón
<g/>
)	)	kIx)	)
a	a	k8xC	a
používáním	používání	k1gNnSc7	používání
různých	různý	k2eAgFnPc2d1	různá
ozdob	ozdoba	k1gFnPc2	ozdoba
(	(	kIx(	(
<g/>
appoggiatury	appoggiatura	k1gFnPc1	appoggiatura
<g/>
,	,	kIx,	,
koloratury	koloratura	k1gFnPc1	koloratura
<g/>
,	,	kIx,	,
fioritury	fioritura	k1gFnPc1	fioritura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Belcanto	Belcanta	k1gFnSc5	Belcanta
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
z	z	k7c2	z
módy	móda	k1gFnSc2	móda
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
Verdiho	Verdi	k1gMnSc2	Verdi
a	a	k8xC	a
verismu	verismus	k1gInSc2	verismus
<g/>
;	;	kIx,	;
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
i	i	k9	i
z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
belcantový	belcantový	k2eAgInSc1d1	belcantový
zpěv	zpěv	k1gInSc1	zpěv
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgInS	moct
prosadit	prosadit	k5eAaPmF	prosadit
proti	proti	k7c3	proti
rozšířenému	rozšířený	k2eAgInSc3d1	rozšířený
orchestrálnímu	orchestrální	k2eAgInSc3d1	orchestrální
aparátu	aparát	k1gInSc3	aparát
<g/>
.	.	kIx.	.
</s>
</p>
