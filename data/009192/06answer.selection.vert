<s>
Belcanto	Belcanta	k1gFnSc5	Belcanta
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
z	z	k7c2	z
módy	móda	k1gFnSc2	móda
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
Verdiho	Verdi	k1gMnSc2	Verdi
a	a	k8xC	a
verismu	verismus	k1gInSc2	verismus
<g/>
;	;	kIx,	;
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
i	i	k9	i
z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
belcantový	belcantový	k2eAgInSc1d1	belcantový
zpěv	zpěv	k1gInSc1	zpěv
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgInS	moct
prosadit	prosadit	k5eAaPmF	prosadit
proti	proti	k7c3	proti
rozšířenému	rozšířený	k2eAgInSc3d1	rozšířený
orchestrálnímu	orchestrální	k2eAgInSc3d1	orchestrální
aparátu	aparát	k1gInSc3	aparát
<g/>
.	.	kIx.	.
</s>
