<s>
Sedmikráska	sedmikráska	k1gFnSc1	sedmikráska
(	(	kIx(	(
<g/>
Bellis	Bellis	k1gInSc1	Bellis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
rod	rod	k1gInSc1	rod
asi	asi	k9	asi
10-15	[number]	k4	10-15
druhů	druh	k1gInPc2	druh
dvouděložných	dvouděložný	k2eAgFnPc2d1	dvouděložná
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovité	hvězdnicovitý	k2eAgFnSc2d1	hvězdnicovitý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
původní	původní	k2eAgMnSc1d1	původní
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Středozemí	středozemí	k1gNnSc6	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
sedmikráska	sedmikráska	k1gFnSc1	sedmikráska
chudobka	chudobka	k1gFnSc1	chudobka
(	(	kIx(	(
<g/>
Bellis	Bellis	k1gFnSc1	Bellis
perennis	perennis	k1gFnSc2	perennis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Sedmikráska	sedmikráska	k1gFnSc1	sedmikráska
chudobka	chudobka	k1gFnSc1	chudobka
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
léčivé	léčivý	k2eAgInPc4d1	léčivý
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
podporuje	podporovat	k5eAaImIp3nS	podporovat
odkašlávání	odkašlávání	k1gNnSc4	odkašlávání
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
protizánětlivě	protizánětlivě	k6eAd1	protizánětlivě
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jak	jak	k6eAd1	jak
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
tak	tak	k9	tak
i	i	k9	i
zevní	zevní	k2eAgNnSc1d1	zevní
použití	použití	k1gNnSc1	použití
<g/>
,	,	kIx,	,
výborná	výborný	k2eAgFnSc1d1	výborná
je	být	k5eAaImIp3nS	být
i	i	k9	i
při	při	k7c6	při
kožních	kožní	k2eAgFnPc6d1	kožní
chorobách	choroba	k1gFnPc6	choroba
nebo	nebo	k8xC	nebo
na	na	k7c4	na
otoky	otok	k1gInPc4	otok
a	a	k8xC	a
vředy	vřed	k1gInPc4	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc4	druh
sedmikráska	sedmikráska	k1gFnSc1	sedmikráska
chudobka	chudobka	k1gFnSc1	chudobka
(	(	kIx(	(
<g/>
Bellis	Bellis	k1gFnSc1	Bellis
perennis	perennis	k1gFnSc1	perennis
<g/>
)	)	kIx)	)
Bellis	Bellis	k1gFnSc1	Bellis
annua	annua	k1gFnSc1	annua
Bellis	Bellis	k1gFnSc1	Bellis
azorica	azorica	k1gFnSc1	azorica
Bellis	Bellis	k1gFnSc2	Bellis
bernardii	bernardie	k1gFnSc4	bernardie
Bellis	Bellis	k1gFnSc1	Bellis
caerulescens	caerulescensa	k1gFnPc2	caerulescensa
Bellis	Bellis	k1gFnSc1	Bellis
Habanera	habanera	k1gFnSc1	habanera
Bellis	Bellis	k1gFnSc1	Bellis
hybrida	hybrida	k1gFnSc1	hybrida
Bellis	Bellis	k1gFnSc1	Bellis
hyrcanica	hyrcanica	k1gFnSc1	hyrcanica
Bellis	Bellis	k1gFnSc1	Bellis
longifolia	longifolia	k1gFnSc1	longifolia
Bellis	Bellis	k1gFnSc1	Bellis
microcephala	microcephat	k5eAaImAgFnS	microcephat
Bellis	Bellis	k1gFnSc4	Bellis
rotundifolia	rotundifolius	k1gMnSc4	rotundifolius
Bellis	Bellis	k1gFnSc4	Bellis
sylvestris	sylvestris	k1gFnSc1	sylvestris
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bellis	Bellis	k1gFnSc2	Bellis
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sedmikráska	sedmikráska	k1gFnSc1	sedmikráska
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sedmikráska	sedmikráska	k1gFnSc1	sedmikráska
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Bellis	Bellis	k1gInSc1	Bellis
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
