<s>
Gauliga	Gauliga	k1gFnSc1
Schleswig-Holstein	Schleswig-Holsteina	k1gFnPc2
</s>
<s>
Gauliga	Gauliga	k1gFnSc1
Schleswig-Holstein	Schleswig-Holsteina	k1gFnPc2
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Německá	německý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Německá	německý	k2eAgFnSc1d1
říše	říš	k1gFnSc2
Datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
</s>
<s>
1942	#num#	k4
Zánik	zánik	k1gInSc1
</s>
<s>
1945	#num#	k4
Stupeň	stupeň	k1gInSc1
v	v	k7c6
pyramidě	pyramida	k1gFnSc6
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc7d1
vítěz	vítěz	k1gMnSc1
</s>
<s>
Holstein	Holstein	k2eAgInSc1d1
Kiel	Kiel	k1gInSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gauliga	Gauliga	k1gFnSc1
Schleswig-Holstein	Schleswig-Holsteina	k1gFnPc2
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
mnoha	mnoho	k4c2
skupin	skupina	k1gFnPc2
Gauligy	Gauliga	k1gFnSc2
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc2d3
fotbalové	fotbalový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
Německa	Německo	k1gNnSc2
v	v	k7c6
letech	let	k1gInPc6
1933	#num#	k4
–	–	k?
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořena	vytvořen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
vyčleněním	vyčlenění	k1gNnSc7
z	z	k7c2
Gauligy	Gauliga	k1gFnSc2
Nordmark	Nordmark	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořádala	pořádat	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
Šlesvicko-Holštýnska	Šlesvicko-Holštýnsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězové	vítěz	k1gMnPc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
Gauligy	Gauliga	k1gFnSc2
postupovali	postupovat	k5eAaImAgMnP
do	do	k7c2
celostátní	celostátní	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
trvala	trvat	k5eAaImAgFnS
necelý	celý	k2eNgInSc4d1
měsíc	měsíc	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
kluby	klub	k1gInPc1
utkávaly	utkávat	k5eAaImAgInP
vyřazovacím	vyřazovací	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Zanikla	zaniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
po	po	k7c6
pádu	pád	k1gInSc6
nacistického	nacistický	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gInSc6
zániku	zánik	k1gInSc6
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc4
Gauligy	Gauliga	k1gFnSc2
Schleswig-Holstein	Schleswig-Holsteina	k1gFnPc2
začleněno	začleněn	k2eAgNnSc4d1
pod	pod	k7c4
Oberligu	Oberliga	k1gFnSc4
Nord	Norda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Nejlepší	dobrý	k2eAgInPc1d3
kluby	klub	k1gInPc1
v	v	k7c6
historii	historie	k1gFnSc6
-	-	kIx~
podle	podle	k7c2
počtu	počet	k1gInSc2
titulů	titul	k1gInPc2
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vítězové	vítěz	k1gMnPc1
nejvyšší	vysoký	k2eAgFnSc2d3
ligové	ligový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Tituly	titul	k1gInPc1
</s>
<s>
Vítězné	vítězný	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Holstein	Holstein	k2eAgInSc1d1
Kiel	Kiel	k1gInSc1
<g/>
21943	#num#	k4
<g/>
,	,	kIx,
1944	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
jednotlivých	jednotlivý	k2eAgInPc2d1
ročníků	ročník	k1gInPc2
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gauliga	Gauliga	k1gFnSc1
Schleswig-Holstein	Schleswig-Holsteina	k1gFnPc2
(	(	kIx(
<g/>
1942	#num#	k4
–	–	k?
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Gauligy	Gauliga	k1gFnSc2
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
německém	německý	k2eAgNnSc6d1
mistrovství	mistrovství	k1gNnSc6
</s>
<s>
Německý	německý	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
</s>
<s>
1942	#num#	k4
–	–	k?
1943	#num#	k4
</s>
<s>
Holstein	Holstein	k2eAgInSc1d1
Kiel	Kiel	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Dresdner	Dresdner	k1gMnSc1
SC	SC	kA
</s>
<s>
1943	#num#	k4
–	–	k?
1944	#num#	k4
</s>
<s>
Holstein	Holstein	k2eAgInSc1d1
Kiel	Kiel	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
</s>
<s>
Dresdner	Dresdner	k1gMnSc1
SC	SC	kA
</s>
<s>
•	•	k?
Gauliga	Gauliga	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
zrušena	zrušit	k5eAaPmNgNnP
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
kvůli	kvůli	k7c3
přesunutí	přesunutí	k1gNnSc3
bojů	boj	k1gInPc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Gauliga	Gaulig	k1gMnSc2
Schleswig-Holstein	Schleswig-Holstein	k2eAgInSc4d1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
(	(	kIx(
<g/>
F.	F.	kA
<g/>
R.	R.	kA
<g/>
)	)	kIx)
GERMANY	GERMANY	kA
-	-	kIx~
LEAGUE	LEAGUE	kA
FINAL	FINAL	kA
TABLES	TABLES	kA
-	-	kIx~
German	German	k1gMnSc1
Empire	empir	k1gInSc5
until	untila	k1gFnPc2
1918	#num#	k4
<g/>
,	,	kIx,
Federal	Federal	k1gFnSc1
Republic	Republice	k1gFnPc2
of	of	k?
Germany	German	k1gInPc4
(	(	kIx(
<g/>
Weimar	Weimara	k1gFnPc2
Republic	Republice	k1gFnPc2
<g/>
)	)	kIx)
1918	#num#	k4
<g/>
-	-	kIx~
<g/>
1933	#num#	k4
<g/>
,	,	kIx,
German	German	k1gMnSc1
Third	Thirda	k1gFnPc2
Reich	Reich	k?
1933	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
,	,	kIx,
Allied	Allied	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Occupation	Occupation	k1gInSc1
Zones	Zones	k1gInSc1
1945	#num#	k4
<g/>
-	-	kIx~
<g/>
1949	#num#	k4
<g/>
,	,	kIx,
Federal	Federal	k1gFnSc1
Republic	Republice	k1gFnPc2
of	of	k?
Germany	German	k1gInPc7
1949	#num#	k4
(	(	kIx(
<g/>
including	including	k1gInSc1
former	former	k1gMnSc1
German	German	k1gMnSc1
Democratic	Democratice	k1gFnPc2
Republic	Republice	k1gFnPc2
since	sinko	k6eAd1
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
webalice	webalice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
it	it	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Německé	německý	k2eAgFnSc2d1
regionální	regionální	k2eAgFnSc2d1
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1933	#num#	k4
až	až	k9
1945	#num#	k4
Založené	založený	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
</s>
<s>
Gauliga	Gauliga	k1gFnSc1
Baden	Baden	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Bayern	Bayern	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Berlin-Brandenburg	Berlin-Brandenburg	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Hessen	Hessno	k1gNnPc2
•	•	k?
Gauliga	Gaulig	k1gMnSc2
Mitte	Mitt	k1gInSc5
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Mittelrhein	Mittelrhein	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Niederrhein	Niederrhein	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Niedersachsen	Niedersachsen	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Nordmark	Nordmark	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Ostpreußen	Ostpreußen	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Pommern	Pommern	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Sachsen	Sachsen	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Schlesien	Schlesien	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Südwest	Südwest	k1gFnSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Westfalen	Westfalen	k2eAgInSc1d1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Württemberg	Württemberg	k1gInSc1
Založené	založený	k2eAgFnSc2d1
po	po	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
</s>
<s>
Gauliga	Gauliga	k1gFnSc1
Südhannover-Braunschweig	Südhannover-Braunschweig	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Danzig-Westpreußen	Danzig-Westpreußen	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Hamburg	Hamburg	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Hessen-Nassau	Hessen-Nassaus	k1gInSc2
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Köln-Aachen	Köln-Aachen	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Kurhessen	Kurhessen	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Mecklenburg	Mecklenburg	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Moselland	Moselland	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Niederschlesien	Niederschlesien	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Oberschlesien	Oberschlesien	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Osthannover	Osthannover	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Schleswig-Holstein	Schleswig-Holstein	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Weser-Ems	Weser-Ems	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Westmark	Westmark	k1gInSc4
Založené	založený	k2eAgMnPc4d1
na	na	k7c6
okupovanýchúzemích	okupovanýchúzemí	k1gNnPc6
</s>
<s>
Gauliga	Gauliga	k1gFnSc1
Böhmen	Böhmen	k1gInSc1
und	und	k?
Mähren	Mährna	k1gFnPc2
•	•	k?
Gauliga	Gaulig	k1gMnSc2
Elsaß	Elsaß	k1gMnSc2
•	•	k?
Gauliga	Gaulig	k1gMnSc2
Generalgouvernement	Generalgouvernement	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Sudetenland	Sudetenland	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Ostmark	Ostmark	k1gInSc1
•	•	k?
Gauliga	Gauliga	k1gFnSc1
Wartheland	Warthelanda	k1gFnPc2
Celostátní	celostátní	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
:	:	kIx,
Německé	německý	k2eAgNnSc1d1
fotbalové	fotbalový	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
