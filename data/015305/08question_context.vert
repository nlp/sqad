<s>
Působil	působit	k5eAaImAgMnS
jako	jako	k9
melchitský	melchitský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
v	v	k7c6
syrském	syrský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Manbidž	Manbidž	k1gFnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Hierapolis	Hierapolis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
světové	světový	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
zvané	zvaný	k2eAgFnSc2d1
Kitab	Kitab	k1gInSc4
al-	al-	k?
<g/>
'	'	kIx"
<g/>
Unwan	Unwan	k1gMnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
v	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
žánrem	žánr	k1gInSc7
líčí	líčit	k5eAaImIp3nP
historii	historie	k1gFnSc4
od	od	k7c2
stvoření	stvoření	k1gNnSc2
světa	svět	k1gInSc2
až	až	k9
po	po	k7c4
současnost	současnost	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
rozděluje	rozdělovat	k5eAaImIp3nS
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
oddělené	oddělený	k2eAgFnPc4d1
životem	život	k1gInSc7
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
.	.	kIx.
</s>