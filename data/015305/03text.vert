<s>
Agapios	Agapios	k1gMnSc1
(	(	kIx(
<g/>
historik	historik	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Agapios	Agapios	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
942	#num#	k4
Povolání	povolání	k1gNnSc1
</s>
<s>
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
historik	historik	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Agapios	Agapios	k1gInSc1
zvaný	zvaný	k2eAgInSc1d1
Agapios	Agapios	k1gInSc4
z	z	k7c2
Manbidže	Manbidž	k1gFnSc2
nebo	nebo	k8xC
Agapios	Agapiosa	k1gFnPc2
z	z	k7c2
Hierapole	Hierapole	k1gFnSc2
(	(	kIx(
<g/>
arab	arab	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mahbū	Mahbū	k1gMnSc1
ibn	ibn	k?
Qustantī	Qustantī	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
†	†	k?
kolem	kolem	k7c2
942	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
arabský	arabský	k2eAgMnSc1d1
křesťanský	křesťanský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Působil	působit	k5eAaImAgMnS
jako	jako	k9
melchitský	melchitský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
v	v	k7c6
syrském	syrský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Manbidž	Manbidž	k1gFnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Hierapolis	Hierapolis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
světové	světový	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
zvané	zvaný	k2eAgFnSc2d1
Kitab	Kitab	k1gInSc4
al-	al-	k?
<g/>
'	'	kIx"
<g/>
Unwan	Unwan	k1gMnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
v	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
žánrem	žánr	k1gInSc7
líčí	líčit	k5eAaImIp3nP
historii	historie	k1gFnSc4
od	od	k7c2
stvoření	stvoření	k1gNnSc2
světa	svět	k1gInSc2
až	až	k9
po	po	k7c4
současnost	současnost	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
rozděluje	rozdělovat	k5eAaImIp3nS
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
oddělené	oddělený	k2eAgFnPc4d1
životem	život	k1gInSc7
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
čerpá	čerpat	k5eAaImIp3nS
z	z	k7c2
více	hodně	k6eAd2
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
běžných	běžný	k2eAgInPc2d1
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Starý	starý	k2eAgInSc4d1
zákon	zákon	k1gInSc4
nebo	nebo	k8xC
Eusebios	Eusebios	k1gInSc4
z	z	k7c2
Kaisareie	Kaisareie	k1gFnSc2
i	i	k8xC
některých	některý	k3yIgInPc2
jinak	jinak	k6eAd1
nedochovaných	dochovaný	k2eNgInPc2d1
syrských	syrský	k2eAgInPc2d1
textů	text	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kronice	kronika	k1gFnSc6
také	také	k6eAd1
zaznamenal	zaznamenat	k5eAaPmAgMnS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
verzí	verze	k1gFnPc2
Testimonia	testimonium	k1gNnSc2
flaviana	flavian	k1gMnSc4
<g/>
,	,	kIx,
sporného	sporný	k2eAgNnSc2d1
svědectví	svědectví	k1gNnSc2
o	o	k7c6
Ježíši	Ježíš	k1gMnSc6
Kristovi	Kristus	k1gMnSc6
v	v	k7c6
díle	dílo	k1gNnSc6
Flavia	Flavius	k1gMnSc2
Josefa	Josef	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
100970893	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
9622	#num#	k4
4422	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
291127887	#num#	k4
</s>
