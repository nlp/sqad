<s>
Paranoia	paranoia	k1gFnSc1	paranoia
(	(	kIx(	(
<g/>
řec.	řec.	k?	řec.
π	π	k?	π
<g/>
,	,	kIx,	,
pošetilost	pošetilost	k1gFnSc1	pošetilost
<g/>
,	,	kIx,	,
šílenství	šílenství	k1gNnSc1	šílenství
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgFnSc1d1	vyznačující
se	s	k7c7	s
bludy	blud	k1gInPc7	blud
<g/>
,	,	kIx,	,
chorobnými	chorobný	k2eAgFnPc7d1	chorobná
představami	představa	k1gFnPc7	představa
o	o	k7c6	o
vlastním	vlastní	k2eAgNnSc6d1	vlastní
ohrožení	ohrožení	k1gNnSc6	ohrožení
a	a	k8xC	a
stihomamem	stihomam	k1gInSc7	stihomam
<g/>
.	.	kIx.	.
</s>
