<s>
Paranoia	paranoia	k1gFnSc1	paranoia
(	(	kIx(	(
<g/>
řec.	řec.	k?	řec.
π	π	k?	π
<g/>
,	,	kIx,	,
pošetilost	pošetilost	k1gFnSc1	pošetilost
<g/>
,	,	kIx,	,
šílenství	šílenství	k1gNnSc1	šílenství
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgFnSc1d1	vyznačující
se	s	k7c7	s
bludy	blud	k1gInPc7	blud
<g/>
,	,	kIx,	,
chorobnými	chorobný	k2eAgFnPc7d1	chorobná
představami	představa	k1gFnPc7	představa
o	o	k7c6	o
vlastním	vlastní	k2eAgNnSc6d1	vlastní
ohrožení	ohrožení	k1gNnSc6	ohrožení
a	a	k8xC	a
stihomamem	stihomam	k1gInSc7	stihomam
<g/>
.	.	kIx.	.
</s>
<s>
Přehnané	přehnaný	k2eAgFnPc1d1	přehnaná
a	a	k8xC	a
nedoložené	doložený	k2eNgFnPc1d1	nedoložená
obavy	obava	k1gFnPc1	obava
o	o	k7c4	o
vlastní	vlastní	k2eAgFnSc4d1	vlastní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
konspiračními	konspirační	k2eAgFnPc7d1	konspirační
teoriemi	teorie	k1gFnPc7	teorie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
paranoidní	paranoidní	k2eAgFnPc1d1	paranoidní
<g/>
.	.	kIx.	.
</s>
<s>
Paranoidní	paranoidní	k2eAgFnPc1d1	paranoidní
představy	představa	k1gFnPc1	představa
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
neurotického	neurotický	k2eAgInSc2d1	neurotický
nebo	nebo	k8xC	nebo
i	i	k9	i
psychotického	psychotický	k2eAgInSc2d1	psychotický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
zaměňována	zaměňován	k2eAgFnSc1d1	zaměňována
s	s	k7c7	s
paranoidní	paranoidní	k2eAgFnSc7d1	paranoidní
schizofrenií	schizofrenie	k1gFnSc7	schizofrenie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
psychiatrii	psychiatrie	k1gFnSc6	psychiatrie
byl	být	k5eAaImAgInS	být
termín	termín	k1gInSc1	termín
poprvé	poprvé	k6eAd1	poprvé
užit	užít	k5eAaPmNgInS	užít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
psychiatrem	psychiatr	k1gMnSc7	psychiatr
Emilem	Emil	k1gMnSc7	Emil
Kraepelinem	Kraepelin	k1gInSc7	Kraepelin
pro	pro	k7c4	pro
chorobné	chorobný	k2eAgFnPc4d1	chorobná
iluze	iluze	k1gFnPc4	iluze
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
však	však	k9	však
nenarušily	narušit	k5eNaPmAgFnP	narušit
ostatní	ostatní	k2eAgFnPc1d1	ostatní
intelektuální	intelektuální	k2eAgFnPc4d1	intelektuální
schopnosti	schopnost	k1gFnPc4	schopnost
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Náplň	náplň	k1gFnSc1	náplň
tohoto	tento	k3xDgInSc2	tento
pojmu	pojem	k1gInSc2	pojem
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
posunula	posunout	k5eAaPmAgFnS	posunout
spíše	spíše	k9	spíše
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
falešných	falešný	k2eAgFnPc2d1	falešná
představ	představa	k1gFnPc2	představa
a	a	k8xC	a
obav	obava	k1gFnPc2	obava
z	z	k7c2	z
pronásledování	pronásledování	k1gNnSc2	pronásledování
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsah	obsah	k1gInSc1	obsah
původního	původní	k2eAgInSc2d1	původní
pojmu	pojem	k1gInSc2	pojem
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
široký	široký	k2eAgInSc1d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
posunu	posun	k1gInSc3	posun
není	být	k5eNaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
paranoia	paranoia	k1gFnSc1	paranoia
psychiatry	psychiatr	k1gMnPc7	psychiatr
používán	používán	k2eAgInSc4d1	používán
zcela	zcela	k6eAd1	zcela
jednotně	jednotně	k6eAd1	jednotně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Freemana	Freeman	k1gMnSc2	Freeman
a	a	k8xC	a
Garetyho	Garety	k1gMnSc2	Garety
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
pacienta	pacient	k1gMnSc4	pacient
s	s	k7c7	s
paranoiu	paranoium	k1gNnSc3	paranoium
dvě	dva	k4xCgNnPc1	dva
bludná	bludný	k2eAgNnPc1d1	bludné
přesvědčení	přesvědčení	k1gNnPc1	přesvědčení
<g/>
:	:	kIx,	:
že	že	k8xS	že
trpí	trpět	k5eAaImIp3nP	trpět
nějakou	nějaký	k3yIgFnSc4	nějaký
újmu	újma	k1gFnSc4	újma
nebo	nebo	k8xC	nebo
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
taková	takový	k3xDgFnSc1	takový
újma	újma	k1gFnSc1	újma
hrozí	hrozit	k5eAaImIp3nS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
někdo	někdo	k3yInSc1	někdo
chce	chtít	k5eAaImIp3nS	chtít
ublížit	ublížit	k5eAaPmF	ublížit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
klasifikace	klasifikace	k1gFnSc1	klasifikace
chorob	choroba	k1gFnPc2	choroba
ISD-10	ISD-10	k1gFnPc2	ISD-10
řadí	řadit	k5eAaImIp3nP	řadit
paranoidní	paranoidní	k2eAgFnPc1d1	paranoidní
poruchy	porucha	k1gFnPc1	porucha
do	do	k7c2	do
kapitoly	kapitola	k1gFnSc2	kapitola
"	"	kIx"	"
<g/>
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
"	"	kIx"	"
pod	pod	k7c4	pod
tyto	tento	k3xDgFnPc4	tento
diagnózy	diagnóza	k1gFnPc4	diagnóza
<g/>
:	:	kIx,	:
F	F	kA	F
20.0	[number]	k4	20.0
Paranoidní	paranoidní	k2eAgFnSc2d1	paranoidní
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
:	:	kIx,	:
bludné	bludný	k2eAgFnSc2d1	bludná
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
doprovázené	doprovázený	k2eAgFnPc1d1	doprovázená
halucinacemi	halucinace	k1gFnPc7	halucinace
F	F	kA	F
22.0	[number]	k4	22.0
Bludné	bludný	k2eAgFnSc2d1	bludná
poruchy	porucha	k1gFnSc2	porucha
<g/>
,	,	kIx,	,
paranoia	paranoia	k1gFnSc1	paranoia
<g/>
:	:	kIx,	:
dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
nebo	nebo	k8xC	nebo
trvalé	trvalý	k2eAgFnPc1d1	trvalá
bludné	bludný	k2eAgFnPc1d1	bludná
představy	představa	k1gFnPc1	představa
bez	bez	k7c2	bez
halucinací	halucinace	k1gFnPc2	halucinace
F	F	kA	F
22.8	[number]	k4	22.8
<g />
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
bludy	blud	k1gInPc1	blud
<g/>
,	,	kIx,	,
stařecká	stařecký	k2eAgFnSc1d1	stařecká
paranoia	paranoia	k1gFnSc1	paranoia
F	F	kA	F
23.3	[number]	k4	23.3
Paranoidní	paranoidní	k2eAgFnSc2d1	paranoidní
reakce	reakce	k1gFnSc2	reakce
-	-	kIx~	-
psychogenní	psychogenní	k2eAgFnSc1d1	psychogenní
psychóza	psychóza	k1gFnSc1	psychóza
F	F	kA	F
60.0	[number]	k4	60.0
Paranoidní	paranoidní	k2eAgFnSc2d1	paranoidní
poruchy	porucha	k1gFnSc2	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
perzekuční	perzekuční	k2eAgFnSc1d1	perzekuční
paranoia	paranoia	k1gFnSc1	paranoia
=	=	kIx~	=
nemocný	nemocný	k1gMnSc1	nemocný
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pronásledován	pronásledován	k2eAgMnSc1d1	pronásledován
kverulantská	kverulantský	k2eAgFnSc1d1	kverulantská
paranoia	paranoia	k1gFnSc1	paranoia
=	=	kIx~	=
nemocný	nemocný	k1gMnSc1	nemocný
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gNnSc4	on
ostatní	ostatní	k1gNnSc4	ostatní
nemají	mít	k5eNaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
-	-	kIx~	-
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
si	se	k3xPyFc3	se
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
dohadovat	dohadovat	k5eAaImF	dohadovat
žárlivecká	žárlivecký	k2eAgFnSc1d1	žárlivecký
paranoia	paranoia	k1gFnSc1	paranoia
(	(	kIx(	(
<g/>
emulační	emulační	k2eAgInSc1d1	emulační
<g/>
,	,	kIx,	,
žárlivecký	žárlivecký	k2eAgInSc1d1	žárlivecký
blud	blud	k1gInSc1	blud
<g/>
)	)	kIx)	)
=	=	kIx~	=
chorobná	chorobný	k2eAgFnSc1d1	chorobná
žárlivost	žárlivost	k1gFnSc1	žárlivost
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
až	až	k9	až
k	k	k7c3	k
vraždě	vražda	k1gFnSc3	vražda
(	(	kIx(	(
<g/>
estenická	estenický	k2eAgFnSc1d1	estenický
forma	forma	k1gFnSc1	forma
-	-	kIx~	-
agresivita	agresivita	k1gFnSc1	agresivita
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
partnera	partner	k1gMnSc4	partner
<g/>
)	)	kIx)	)
či	či	k8xC	či
sebevraždě	sebevražda	k1gFnSc3	sebevražda
(	(	kIx(	(
<g/>
astenická	astenický	k2eAgFnSc1d1	astenická
forma	forma	k1gFnSc1	forma
-	-	kIx~	-
agresivita	agresivita	k1gFnSc1	agresivita
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
osobu	osoba	k1gFnSc4	osoba
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
davová	davový	k2eAgFnSc1d1	davová
paranoia	paranoia	k1gFnSc1	paranoia
=	=	kIx~	=
posedlost	posedlost	k1gFnSc4	posedlost
tancem	tanec	k1gInSc7	tanec
či	či	k8xC	či
rituálními	rituální	k2eAgInPc7d1	rituální
obřady	obřad	k1gInPc7	obřad
V	v	k7c6	v
obecném	obecný	k2eAgNnSc6d1	obecné
povědomí	povědomí	k1gNnSc6	povědomí
je	být	k5eAaImIp3nS	být
paranoia	paranoia	k1gFnSc1	paranoia
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
pocitem	pocit	k1gInSc7	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
vládne	vládnout	k5eAaImIp3nS	vládnout
zvláštními	zvláštní	k2eAgFnPc7d1	zvláštní
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
plní	plnit	k5eAaImIp3nP	plnit
důležitý	důležitý	k2eAgInSc4d1	důležitý
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
o	o	k7c6	o
pronásledování	pronásledování	k1gNnSc6	pronásledování
mimozemšťany	mimozemšťan	k1gMnPc4	mimozemšťan
<g/>
,	,	kIx,	,
tajnými	tajný	k2eAgFnPc7d1	tajná
službami	služba	k1gFnPc7	služba
nebo	nebo	k8xC	nebo
démony	démon	k1gMnPc7	démon
<g/>
,	,	kIx,	,
konspiračními	konspirační	k2eAgFnPc7d1	konspirační
teoriemi	teorie	k1gFnPc7	teorie
a	a	k8xC	a
představou	představa	k1gFnSc7	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
řízen	řídit	k5eAaImNgInS	řídit
temnými	temný	k2eAgFnPc7d1	temná
silami	síla	k1gFnPc7	síla
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
pocitem	pocit	k1gInSc7	pocit
ovládání	ovládání	k1gNnSc2	ovládání
mozku	mozek	k1gInSc2	mozek
z	z	k7c2	z
vnějšku	vnějšek	k1gInSc2	vnějšek
<g/>
,	,	kIx,	,
pocitem	pocit	k1gInSc7	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
právě	právě	k6eAd1	právě
přečteném	přečtený	k2eAgInSc6d1	přečtený
románu	román	k1gInSc6	román
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
filmu	film	k1gInSc2	film
a	a	k8xC	a
PC	PC	kA	PC
hře	hra	k1gFnSc6	hra
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paranoia	paranoia	k1gFnSc1	paranoia
se	se	k3xPyFc4	se
často	často	k6eAd1	často
prolíná	prolínat	k5eAaImIp3nS	prolínat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgFnPc2d1	další
psychóz	psychóza	k1gFnPc2	psychóza
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
některé	některý	k3yIgInPc1	některý
znaky	znak	k1gInPc1	znak
společné	společný	k2eAgInPc1d1	společný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
obtížné	obtížný	k2eAgNnSc1d1	obtížné
paranoiu	paranoium	k1gNnSc3	paranoium
přesně	přesně	k6eAd1	přesně
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
příznaky	příznak	k1gInPc4	příznak
paranoii	paranoia	k1gFnSc4	paranoia
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
přecitlivělost	přecitlivělost	k1gFnSc1	přecitlivělost
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
nepřirozená	přirozený	k2eNgFnSc1d1	nepřirozená
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
urážlivost	urážlivost	k1gFnSc1	urážlivost
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc1	pocit
citového	citový	k2eAgNnSc2d1	citové
zranění	zranění	k1gNnSc2	zranění
<g/>
,	,	kIx,	,
způsobený	způsobený	k2eAgInSc1d1	způsobený
často	často	k6eAd1	často
malichernými	malicherný	k2eAgFnPc7d1	malicherná
příčinami	příčina	k1gFnPc7	příčina
(	(	kIx(	(
<g/>
tón	tón	k1gInSc1	tón
hlasu	hlas	k1gInSc2	hlas
<g/>
,	,	kIx,	,
gesto	gesto	k1gNnSc1	gesto
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
vyřčené	vyřčený	k2eAgNnSc1d1	vyřčené
v	v	k7c6	v
afektu	afekt	k1gInSc6	afekt
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
dokonce	dokonce	k9	dokonce
příčinami	příčina	k1gFnPc7	příčina
domnělými	domnělý	k2eAgFnPc7d1	domnělá
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g />
.	.	kIx.	.
</s>
<s>
přikládáním	přikládání	k1gNnSc7	přikládání
nesprávného	správný	k2eNgInSc2d1	nesprávný
významu	význam	k1gInSc2	význam
aktům	akt	k1gInPc3	akt
jednání	jednání	k1gNnSc1	jednání
partnera	partner	k1gMnSc2	partner
vztahovačnost	vztahovačnost	k1gFnSc1	vztahovačnost
(	(	kIx(	(
<g/>
neutrální	neutrální	k2eAgFnSc1d1	neutrální
nebo	nebo	k8xC	nebo
kladné	kladný	k2eAgNnSc1d1	kladné
chování	chování	k1gNnSc1	chování
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
osobě	osoba	k1gFnSc6	osoba
si	se	k3xPyFc3	se
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k9	jako
nepřátelské	přátelský	k2eNgNnSc4d1	nepřátelské
<g/>
,	,	kIx,	,
neloajální	loajální	k2eNgNnSc4d1	neloajální
<g/>
,	,	kIx,	,
ohrožující	ohrožující	k2eAgNnSc4d1	ohrožující
<g/>
,	,	kIx,	,
kritické	kritický	k2eAgNnSc4d1	kritické
nebo	nebo	k8xC	nebo
pohrdavé	pohrdavý	k2eAgNnSc4d1	pohrdavé
<g/>
)	)	kIx)	)
emocionálnost	emocionálnost	k1gFnSc4	emocionálnost
<g/>
,	,	kIx,	,
hysterie	hysterie	k1gFnSc1	hysterie
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
citovým	citový	k2eAgInPc3d1	citový
úrazům	úraz	k1gInPc3	úraz
dlouhodobost	dlouhodobost	k1gFnSc1	dlouhodobost
citových	citový	k2eAgNnPc2d1	citové
zranění	zranění	k1gNnPc2	zranění
(	(	kIx(	(
<g/>
neschopnost	neschopnost	k1gFnSc1	neschopnost
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
udobřit	udobřit	k5eAaPmF	udobřit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
odpustit	odpustit	k5eAaPmF	odpustit
<g/>
,	,	kIx,	,
přejít	přejít	k5eAaPmF	přejít
to	ten	k3xDgNnSc4	ten
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
záštiplnost	záštiplnost	k1gFnSc4	záštiplnost
podezíravost	podezíravost	k1gFnSc4	podezíravost
sebestřednost	sebestřednost	k1gFnSc1	sebestřednost
hnusení	hnusení	k1gNnSc2	hnusení
si	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
či	či	k8xC	či
domněle	domněle	k6eAd1	domněle
horší	zlý	k2eAgFnSc1d2	horší
hygienou	hygiena	k1gFnSc7	hygiena
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
bludy	blud	k1gInPc1	blud
(	(	kIx(	(
<g/>
pronásledování	pronásledování	k1gNnSc1	pronásledování
a	a	k8xC	a
ovlivňování	ovlivňování	k1gNnSc1	ovlivňování
<g/>
)	)	kIx)	)
blud	blud	k1gInSc1	blud
perzekuční	perzekuční	k2eAgInSc1d1	perzekuční
stihomam	stihomam	k1gInSc1	stihomam
-	-	kIx~	-
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pronásledován	pronásledován	k2eAgInSc4d1	pronásledován
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
stihomam	stihomam	k1gInSc1	stihomam
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k8xS	jako
synonymum	synonymum	k1gNnSc4	synonymum
k	k	k7c3	k
paranoii	paranoia	k1gFnSc3	paranoia
<g/>
)	)	kIx)	)
slavomam	slavomam	k1gInSc1	slavomam
-	-	kIx~	-
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
bráněno	bráněn	k2eAgNnSc1d1	bráněno
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
slavným	slavný	k2eAgInSc7d1	slavný
(	(	kIx(	(
<g/>
jiným	jiný	k2eAgInSc7d1	jiný
významem	význam	k1gInSc7	význam
slova	slovo	k1gNnSc2	slovo
slavomam	slavomam	k1gInSc1	slavomam
je	on	k3xPp3gNnSc4	on
velikášství	velikášství	k1gNnSc4	velikášství
<g/>
,	,	kIx,	,
megalomanický	megalomanický	k2eAgInSc4d1	megalomanický
blud	blud	k1gInSc4	blud
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
megalomanie	megalomanie	k1gFnSc1	megalomanie
<g/>
)	)	kIx)	)
manipulace	manipulace	k1gFnSc1	manipulace
<g />
.	.	kIx.	.
</s>
<s>
kverulantství	kverulantství	k1gNnSc1	kverulantství
(	(	kIx(	(
<g/>
nemocný	mocný	k2eNgMnSc1d1	nemocný
soustavně	soustavně	k6eAd1	soustavně
deptá	deptat	k5eAaImIp3nS	deptat
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
útočí	útočit	k5eAaImIp3nS	útočit
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
hloupostem	hloupost	k1gFnPc3	hloupost
<g/>
,	,	kIx,	,
všechno	všechen	k3xTgNnSc1	všechen
komentuje	komentovat	k5eAaBmIp3nS	komentovat
<g/>
,	,	kIx,	,
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
výtku	výtka	k1gFnSc4	výtka
těžce	těžce	k6eAd1	těžce
nese	nést	k5eAaImIp3nS	nést
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
ublížení	ublížení	k1gNnSc4	ublížení
dává	dávat	k5eAaImIp3nS	dávat
dotyčnému	dotyčný	k2eAgMnSc3d1	dotyčný
pořádně	pořádně	k6eAd1	pořádně
"	"	kIx"	"
<g/>
sežrat	sežrat	k5eAaPmF	sežrat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dusno	dusno	k1gNnSc1	dusno
=	=	kIx~	=
příklad	příklad	k1gInSc1	příklad
potlačené	potlačený	k2eAgFnSc2d1	potlačená
agresivity	agresivita	k1gFnSc2	agresivita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
omílá	omílat	k5eAaImIp3nS	omílat
dokola	dokola	k6eAd1	dokola
<g />
.	.	kIx.	.
</s>
<s>
jeden	jeden	k4xCgInSc1	jeden
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
řešit	řešit	k5eAaImF	řešit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
hádce	hádka	k1gFnSc6	hádka
a	a	k8xC	a
deptá	deptat	k5eAaImIp3nS	deptat
partnera	partner	k1gMnSc4	partner
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
popichuje	popichovat	k5eAaImIp3nS	popichovat
<g/>
)	)	kIx)	)
tendence	tendence	k1gFnPc4	tendence
somatizovat	somatizovat	k5eAaBmF	somatizovat
(	(	kIx(	(
<g/>
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
bledost	bledost	k1gFnSc1	bledost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
záchvatu	záchvat	k1gInSc6	záchvat
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
hysterická	hysterický	k2eAgFnSc1d1	hysterická
slepota	slepota	k1gFnSc1	slepota
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
podlitiny	podlitina	k1gFnPc1	podlitina
pod	pod	k7c7	pod
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
v	v	k7c6	v
extrémních	extrémní	k2eAgInPc6d1	extrémní
případech	případ	k1gInPc6	případ
umí	umět	k5eAaImIp3nS	umět
měnit	měnit	k5eAaImF	měnit
i	i	k9	i
puls	puls	k1gInSc4	puls
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
regulovat	regulovat	k5eAaImF	regulovat
<g/>
"	"	kIx"	"
činnost	činnost	k1gFnSc4	činnost
žláz	žláza	k1gFnPc2	žláza
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
sekrecí	sekrece	k1gFnSc7	sekrece
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
výsledný	výsledný	k2eAgInSc1d1	výsledný
dojem	dojem	k1gInSc1	dojem
byl	být	k5eAaImAgInS	být
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
odstrašující	odstrašující	k2eAgMnSc1d1	odstrašující
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
snaha	snaha	k1gFnSc1	snaha
<g />
.	.	kIx.	.
</s>
<s>
dokonale	dokonale	k6eAd1	dokonale
ovládat	ovládat	k5eAaImF	ovládat
situaci	situace	k1gFnSc4	situace
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
třeba	třeba	k6eAd1	třeba
averze	averze	k1gFnSc1	averze
ke	k	k7c3	k
zvířatům	zvíře	k1gNnPc3	zvíře
<g/>
)	)	kIx)	)
hyperkritičnost	hyperkritičnost	k1gFnSc1	hyperkritičnost
<g/>
,	,	kIx,	,
intolerance	intolerance	k1gFnSc1	intolerance
neschopnost	neschopnost	k1gFnSc1	neschopnost
řešit	řešit	k5eAaImF	řešit
konstruktivně	konstruktivně	k6eAd1	konstruktivně
citové	citový	k2eAgInPc4d1	citový
problémy	problém	k1gInPc4	problém
bojovné	bojovný	k2eAgInPc4d1	bojovný
a	a	k8xC	a
úporné	úporný	k2eAgNnSc4d1	úporné
zdůrazňování	zdůrazňování	k1gNnSc4	zdůrazňování
osobních	osobní	k2eAgNnPc2d1	osobní
práv	právo	k1gNnPc2	právo
vyhraněná	vyhraněný	k2eAgNnPc1d1	vyhraněné
stanoviska	stanovisko	k1gNnPc1	stanovisko
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
krajně	krajně	k6eAd1	krajně
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
nebo	nebo	k8xC	nebo
krajně	krajně	k6eAd1	krajně
negativní	negativní	k2eAgNnSc4d1	negativní
-	-	kIx~	-
odmítavé	odmítavý	k2eAgNnSc4d1	odmítavé
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
citově	citově	k6eAd1	citově
silně	silně	k6eAd1	silně
angažované	angažovaný	k2eAgNnSc1d1	angažované
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přehnaně	přehnaně	k6eAd1	přehnaně
chladné	chladný	k2eAgNnSc1d1	chladné
<g/>
)	)	kIx)	)
pasivní	pasivní	k2eAgFnSc1d1	pasivní
nebo	nebo	k8xC	nebo
aktivní	aktivní	k2eAgFnSc1d1	aktivní
agresivita	agresivita	k1gFnSc1	agresivita
často	často	k6eAd1	často
strach	strach	k1gInSc4	strach
z	z	k7c2	z
výšek	výška	k1gFnPc2	výška
<g/>
,	,	kIx,	,
tmy	tma	k1gFnSc2	tma
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
fobie	fobie	k1gFnSc2	fobie
sklony	sklon	k1gInPc1	sklon
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
Bývá	bývat	k5eAaImIp3nS	bývat
tu	tu	k6eAd1	tu
často	často	k6eAd1	často
i	i	k9	i
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
extrémní	extrémní	k2eAgFnSc3d1	extrémní
žárlivosti	žárlivost	k1gFnSc3	žárlivost
a	a	k8xC	a
podezíravosti	podezíravost	k1gFnSc3	podezíravost
(	(	kIx(	(
<g/>
aspekt	aspekt	k1gInSc1	aspekt
označovaný	označovaný	k2eAgInSc1d1	označovaný
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
někdy	někdy	k6eAd1	někdy
jako	jako	k9	jako
paranoia	paranoia	k1gFnSc1	paranoia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
paranoie	paranoia	k1gFnSc2	paranoia
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
raná	raný	k2eAgNnPc1d1	rané
rodičovská	rodičovský	k2eAgNnPc1d1	rodičovské
zavržení	zavržení	k1gNnPc1	zavržení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
neschopnost	neschopnost	k1gFnSc4	neschopnost
matky	matka	k1gFnSc2	matka
uspokojit	uspokojit	k5eAaPmF	uspokojit
citové	citový	k2eAgFnPc4d1	citová
potřeby	potřeba	k1gFnPc4	potřeba
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
přesvědčivé	přesvědčivý	k2eAgInPc1d1	přesvědčivý
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
že	že	k8xS	že
základ	základ	k1gInSc1	základ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
položen	položit	k5eAaPmNgInS	položit
již	již	k9	již
v	v	k7c6	v
prenatálním	prenatální	k2eAgNnSc6d1	prenatální
stádiu	stádium	k1gNnSc6	stádium
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
hlasitě	hlasitě	k6eAd1	hlasitě
nebo	nebo	k8xC	nebo
v	v	k7c6	v
myšlenkách	myšlenka	k1gFnPc6	myšlenka
budoucí	budoucí	k2eAgNnSc1d1	budoucí
dítě	dítě	k1gNnSc1	dítě
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
zatěžuje	zatěžovat	k5eAaImIp3nS	zatěžovat
je	být	k5eAaImIp3nS	být
drogami	droga	k1gFnPc7	droga
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zvlášť	zvlášť	k6eAd1	zvlášť
stresujícího	stresující	k2eAgInSc2d1	stresující
porodu	porod	k1gInSc2	porod
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
momentech	moment	k1gInPc6	moment
raného	raný	k2eAgNnSc2d1	rané
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
v	v	k7c6	v
pubertě	puberta	k1gFnSc6	puberta
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
v	v	k7c6	v
klíčových	klíčový	k2eAgFnPc6d1	klíčová
emocionálních	emocionální	k2eAgFnPc6d1	emocionální
a	a	k8xC	a
vývojových	vývojový	k2eAgFnPc6d1	vývojová
etapách	etapa	k1gFnPc6	etapa
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kromě	kromě	k7c2	kromě
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
a	a	k8xC	a
medicínských	medicínský	k2eAgFnPc2d1	medicínská
metod	metoda	k1gFnPc2	metoda
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
nevědecké	vědecký	k2eNgFnPc1d1	nevědecká
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
některá	některý	k3yIgNnPc1	některý
onemocnění	onemocnění	k1gNnPc1	onemocnění
berou	brát	k5eAaImIp3nP	brát
jako	jako	k9	jako
následek	následek	k1gInSc4	následek
karmického	karmický	k2eAgNnSc2d1	karmické
zatížení	zatížení	k1gNnSc2	zatížení
v	v	k7c6	v
minulých	minulý	k2eAgInPc6d1	minulý
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Škody	škoda	k1gFnSc2	škoda
způsobené	způsobený	k2eAgFnSc2d1	způsobená
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
žádnou	žádný	k3yNgFnSc7	žádný
péčí	péče	k1gFnSc7	péče
(	(	kIx(	(
<g/>
léčbou	léčba	k1gFnSc7	léčba
<g/>
)	)	kIx)	)
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
nemusí	muset	k5eNaImIp3nS	muset
podařit	podařit	k5eAaPmF	podařit
je	on	k3xPp3gFnPc4	on
napravit	napravit	k5eAaPmF	napravit
nebo	nebo	k8xC	nebo
uzpůsobit	uzpůsobit	k5eAaPmF	uzpůsobit
pacienta	pacient	k1gMnSc4	pacient
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
překonávání	překonávání	k1gNnSc3	překonávání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
psychický	psychický	k2eAgInSc4d1	psychický
stav	stav	k1gInSc4	stav
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
nedostatek	nedostatek	k1gInSc1	nedostatek
mnohých	mnohý	k2eAgFnPc2d1	mnohá
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vitamínů	vitamín	k1gInPc2	vitamín
B-komplexu	Bomplex	k1gInSc2	B-komplex
a	a	k8xC	a
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
B9	B9	k1gMnPc2	B9
podporuje	podporovat	k5eAaImIp3nS	podporovat
psychotické	psychotický	k2eAgInPc4d1	psychotický
projevy	projev	k1gInPc4	projev
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
nedostatek	nedostatek	k1gInSc1	nedostatek
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc1	porucha
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
metabolizmu	metabolizmus	k1gInSc2	metabolizmus
(	(	kIx(	(
<g/>
onemocnění	onemocnění	k1gNnSc2	onemocnění
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
slinivky	slinivka	k1gFnSc2	slinivka
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
žláz	žláza	k1gFnPc2	žláza
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
sekrecí	sekrece	k1gFnSc7	sekrece
<g/>
,	,	kIx,	,
porucha	porucha	k1gFnSc1	porucha
cév	céva	k1gFnPc2	céva
<g/>
,	,	kIx,	,
zásobování	zásobování	k1gNnSc4	zásobování
mozku	mozek	k1gInSc2	mozek
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Paranoia	paranoia	k1gFnSc1	paranoia
je	být	k5eAaImIp3nS	být
obtížně	obtížně	k6eAd1	obtížně
diagnostikovatelná	diagnostikovatelný	k2eAgFnSc1d1	diagnostikovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Paranoik	paranoik	k1gMnSc1	paranoik
si	se	k3xPyFc3	se
dost	dost	k6eAd1	dost
často	často	k6eAd1	často
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
vypěstuje	vypěstovat	k5eAaPmIp3nS	vypěstovat
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc4d1	dobrá
schopnost	schopnost	k1gFnSc4	schopnost
se	se	k3xPyFc4	se
přetvařovat	přetvařovat	k5eAaImF	přetvařovat
<g/>
,	,	kIx,	,
tvářit	tvářit	k5eAaImF	tvářit
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
zdravý	zdravý	k2eAgMnSc1d1	zdravý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Paranoik	paranoik	k1gMnSc1	paranoik
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
před	před	k7c7	před
lékařem	lékař	k1gMnSc7	lékař
či	či	k8xC	či
cizími	cizí	k2eAgMnPc7d1	cizí
lidmi	člověk	k1gMnPc7	člověk
<g/>
)	)	kIx)	)
dokáže	dokázat	k5eAaPmIp3nS	dokázat
"	"	kIx"	"
<g/>
vyhecovat	vyhecovat	k5eAaPmF	vyhecovat
<g/>
"	"	kIx"	"
k	k	k7c3	k
úžasným	úžasný	k2eAgInPc3d1	úžasný
hereckým	herecký	k2eAgInPc3d1	herecký
výkonům	výkon	k1gInPc3	výkon
a	a	k8xC	a
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k9	jako
osoba	osoba	k1gFnSc1	osoba
příjemná	příjemný	k2eAgFnSc1d1	příjemná
<g/>
,	,	kIx,	,
veselá	veselý	k2eAgFnSc1d1	veselá
a	a	k8xC	a
láskyplná	láskyplný	k2eAgFnSc1d1	láskyplná
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
si	se	k3xPyFc3	se
všimne	všimnout	k5eAaPmIp3nS	všimnout
nanejvýš	nanejvýš	k6eAd1	nanejvýš
jemné	jemný	k2eAgFnPc4d1	jemná
tenze	tenze	k1gFnPc4	tenze
či	či	k8xC	či
hysterie	hysterie	k1gFnPc4	hysterie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
léčby	léčba	k1gFnSc2	léčba
si	se	k3xPyFc3	se
nemocný	nemocný	k1gMnSc1	nemocný
především	především	k6eAd1	především
musí	muset	k5eAaImIp3nS	muset
uvědomit	uvědomit	k5eAaPmF	uvědomit
svou	svůj	k3xOyFgFnSc4	svůj
nemoc	nemoc	k1gFnSc4	nemoc
a	a	k8xC	a
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
symptomů	symptom	k1gInPc2	symptom
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
téměř	téměř	k6eAd1	téměř
nemožné	možný	k2eNgNnSc1d1	nemožné
léčit	léčit	k5eAaImF	léčit
někoho	někdo	k3yInSc4	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
si	se	k3xPyFc3	se
nepřipustí	připustit	k5eNaPmIp3nS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nemocen	nemocen	k2eAgMnSc1d1	nemocen
<g/>
.	.	kIx.	.
</s>
