<s>
Několika	několik	k4yIc7	několik
molekulárními	molekulární	k2eAgFnPc7d1	molekulární
kladistickými	kladistický	k2eAgFnPc7d1	kladistická
studiemi	studie	k1gFnPc7	studie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rod	rod	k1gInSc1	rod
Acacia	Acacium	k1gNnSc2	Acacium
v	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
pojetí	pojetí	k1gNnSc6	pojetí
není	být	k5eNaImIp3nS	být
monofyletický	monofyletický	k2eAgInSc1d1	monofyletický
<g/>
.	.	kIx.	.
</s>
