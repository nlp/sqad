<p>
<s>
Akácie	akácie	k1gFnSc1	akácie
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
kapinice	kapinice	k1gFnSc1	kapinice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
bobovité	bobovitý	k2eAgFnSc2d1	bobovitá
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc1	strom
nebo	nebo	k8xC	nebo
i	i	k9	i
liány	liána	k1gFnPc4	liána
s	s	k7c7	s
dvakrát	dvakrát	k6eAd1	dvakrát
zpeřenými	zpeřený	k2eAgInPc7d1	zpeřený
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
listy	list	k1gInPc4	list
s	s	k7c7	s
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
<g/>
,	,	kIx,	,
plochým	plochý	k2eAgInSc7d1	plochý
řapíkem	řapík	k1gInSc7	řapík
a	a	k8xC	a
bez	bez	k7c2	bez
čepele	čepel	k1gFnSc2	čepel
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgFnPc4d1	drobná
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc4d1	bílá
nebo	nebo	k8xC	nebo
žluté	žlutý	k2eAgFnPc4d1	žlutá
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgFnPc4d1	uspořádaná
v	v	k7c6	v
kulovitých	kulovitý	k2eAgInPc6d1	kulovitý
klubkách	klubko	k1gNnPc6	klubko
nebo	nebo	k8xC	nebo
podlouhlých	podlouhlý	k2eAgInPc6d1	podlouhlý
hroznech	hrozen	k1gInPc6	hrozen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akácie	akácie	k1gFnPc1	akácie
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
a	a	k8xC	a
subtropech	subtropy	k1gInPc6	subtropy
téměř	téměř	k6eAd1	téměř
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
asi	asi	k9	asi
1200	[number]	k4	1200
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
a	a	k8xC	a
subtropech	subtropy	k1gInPc6	subtropy
pěstovány	pěstován	k2eAgFnPc1d1	pěstována
jako	jako	k8xS	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
dřeviny	dřevina	k1gFnPc1	dřevina
a	a	k8xC	a
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Akácie	akácie	k1gFnPc1	akácie
mají	mít	k5eAaImIp3nP	mít
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
trvanlivé	trvanlivý	k2eAgNnSc4d1	trvanlivé
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
gumy	guma	k1gFnPc4	guma
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc4	tříslovina
nebo	nebo	k8xC	nebo
parfémové	parfémový	k2eAgFnPc4d1	parfémová
silice	silice	k1gFnPc4	silice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
taxonomii	taxonomie	k1gFnSc6	taxonomie
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
akácie	akácie	k1gFnSc2	akácie
na	na	k7c6	na
základě	základ	k1gInSc6	základ
fylogenetických	fylogenetický	k2eAgFnPc2d1	fylogenetická
studií	studie	k1gFnPc2	studie
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
samostatných	samostatný	k2eAgInPc2d1	samostatný
rodů	rod	k1gInPc2	rod
<g/>
:	:	kIx,	:
Acacia	Acacius	k1gMnSc4	Acacius
<g/>
,	,	kIx,	,
Acaciella	Acaciell	k1gMnSc4	Acaciell
<g/>
,	,	kIx,	,
Senegalia	Senegalius	k1gMnSc4	Senegalius
<g/>
,	,	kIx,	,
Mariosousa	Mariosous	k1gMnSc4	Mariosous
a	a	k8xC	a
Vachellia	Vachellius	k1gMnSc4	Vachellius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Akácie	akácie	k1gFnPc1	akácie
jsou	být	k5eAaImIp3nP	být
keře	keř	k1gInPc4	keř
a	a	k8xC	a
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
i	i	k9	i
dřevnaté	dřevnatý	k2eAgFnSc2d1	dřevnatá
liány	liána	k1gFnSc2	liána
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
druhy	druh	k1gInPc4	druh
mají	mít	k5eAaImIp3nP	mít
nápadné	nápadný	k2eAgInPc4d1	nápadný
trny	trn	k1gInPc4	trn
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
přeměnou	přeměna	k1gFnSc7	přeměna
palistů	palist	k1gInPc2	palist
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
zpeřené	zpeřený	k2eAgInPc1d1	zpeřený
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
drobných	drobný	k2eAgInPc2d1	drobný
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
vstřícných	vstřícný	k2eAgInPc2d1	vstřícný
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
řapíčkatých	řapíčkatý	k2eAgInPc2d1	řapíčkatý
až	až	k8xS	až
přisedlých	přisedlý	k2eAgInPc2d1	přisedlý
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgInPc2d1	mnohý
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
čepel	čepel	k1gFnSc1	čepel
listů	list	k1gInPc2	list
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
a	a	k8xC	a
asimilační	asimilační	k2eAgFnSc4d1	asimilační
funkci	funkce	k1gFnSc4	funkce
přebírá	přebírat	k5eAaImIp3nS	přebírat
plochý	plochý	k2eAgInSc1d1	plochý
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
řapík	řapík	k1gInSc1	řapík
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
fylodie	fylodie	k1gFnSc2	fylodie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
listy	list	k1gInPc1	list
vyhlížejí	vyhlížet	k5eAaImIp3nP	vyhlížet
jako	jako	k9	jako
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
s	s	k7c7	s
paralelní	paralelní	k2eAgFnSc7d1	paralelní
žilnatinou	žilnatina	k1gFnSc7	žilnatina
<g/>
.	.	kIx.	.
</s>
<s>
Palisty	palist	k1gInPc1	palist
bývají	bývat	k5eAaImIp3nP	bývat
drobné	drobný	k2eAgInPc1d1	drobný
a	a	k8xC	a
opadavé	opadavý	k2eAgInPc1d1	opadavý
nebo	nebo	k8xC	nebo
přeměněné	přeměněný	k2eAgInPc1d1	přeměněný
v	v	k7c4	v
trny	trn	k1gInPc4	trn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řapíku	řapík	k1gInSc6	řapík
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
na	na	k7c6	na
vřetenu	vřeteno	k1gNnSc6	vřeteno
listu	list	k1gInSc2	list
bývají	bývat	k5eAaImIp3nP	bývat
přítomny	přítomen	k2eAgFnPc1d1	přítomna
žlázky	žlázka	k1gFnPc1	žlázka
(	(	kIx(	(
<g/>
extraflorální	extraflorální	k2eAgNnPc1d1	extraflorální
nektária	nektárium	k1gNnPc1	nektárium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgMnPc4d1	drobný
<g/>
,	,	kIx,	,
oboupohlavné	oboupohlavný	k2eAgMnPc4d1	oboupohlavný
nebo	nebo	k8xC	nebo
samčí	samčí	k2eAgMnPc4d1	samčí
a	a	k8xC	a
oboupohlavné	oboupohlavný	k2eAgMnPc4d1	oboupohlavný
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc1d1	žlutá
nebo	nebo	k8xC	nebo
krémové	krémový	k2eAgFnPc1d1	krémová
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
v	v	k7c6	v
hustých	hustý	k2eAgInPc6d1	hustý
válcovitých	válcovitý	k2eAgInPc6d1	válcovitý
klasech	klas	k1gInPc6	klas
nebo	nebo	k8xC	nebo
kulovitých	kulovitý	k2eAgNnPc6d1	kulovité
klubkách	klubko	k1gNnPc6	klubko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
květenství	květenství	k1gNnPc1	květenství
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
buď	buď	k8xC	buď
ve	v	k7c6	v
svazcích	svazek	k1gInPc6	svazek
v	v	k7c6	v
úžlabí	úžlabí	k1gNnSc6	úžlabí
listů	list	k1gInPc2	list
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
v	v	k7c6	v
bohatých	bohatý	k2eAgFnPc6d1	bohatá
vrcholových	vrcholový	k2eAgFnPc6d1	vrcholová
latách	lata	k1gFnPc6	lata
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
trubkovitý	trubkovitý	k2eAgInSc1d1	trubkovitý
nebo	nebo	k8xC	nebo
zvonkovitý	zvonkovitý	k2eAgInSc1d1	zvonkovitý
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
4	[number]	k4	4
nebo	nebo	k8xC	nebo
5	[number]	k4	5
cípy	cíp	k1gInPc7	cíp
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
trubkovitá	trubkovitý	k2eAgFnSc1d1	trubkovitá
<g/>
,	,	kIx,	,
nálevkovitá	nálevkovitý	k2eAgFnSc1d1	nálevkovitá
až	až	k6eAd1	až
zvonkovitá	zvonkovitý	k2eAgFnSc1d1	zvonkovitá
<g/>
,	,	kIx,	,
4	[number]	k4	4
nebo	nebo	k8xC	nebo
5	[number]	k4	5
<g/>
-četná	-četný	k2eAgFnSc1d1	-četný
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nápadné	nápadný	k2eAgInPc1d1	nápadný
<g/>
,	,	kIx,	,
vyčnívající	vyčnívající	k2eAgInPc1d1	vyčnívající
z	z	k7c2	z
květu	květ	k1gInSc2	květ
<g/>
,	,	kIx,	,
volné	volný	k2eAgNnSc1d1	volné
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
srostlé	srostlý	k2eAgFnSc6d1	srostlá
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
přisedlý	přisedlý	k2eAgInSc1d1	přisedlý
nebo	nebo	k8xC	nebo
stopkatý	stopkatý	k2eAgInSc1d1	stopkatý
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
různorodé	různorodý	k2eAgInPc1d1	různorodý
<g/>
,	,	kIx,	,
pukavé	pukavý	k2eAgInPc1d1	pukavý
nebo	nebo	k8xC	nebo
nepukavé	pukavý	k2eNgInPc1d1	nepukavý
<g/>
,	,	kIx,	,
rovné	rovný	k2eAgInPc1d1	rovný
nebo	nebo	k8xC	nebo
zakřivené	zakřivený	k2eAgInPc1d1	zakřivený
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
bývají	bývat	k5eAaImIp3nP	bývat
oválná	oválný	k2eAgNnPc1d1	oválné
až	až	k6eAd1	až
podlouhlá	podlouhlý	k2eAgNnPc1d1	podlouhlé
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
zploštělá	zploštělý	k2eAgFnSc1d1	zploštělá
<g/>
,	,	kIx,	,
s	s	k7c7	s
tvrdým	tvrdý	k2eAgNnSc7d1	tvrdé
osemením	osemení	k1gNnSc7	osemení
<g/>
,	,	kIx,	,
bezkřídlá	bezkřídlý	k2eAgFnSc1d1	bezkřídlá
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
bez	bez	k7c2	bez
míšku	míšek	k1gInSc2	míšek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
akácie	akácie	k1gFnSc2	akácie
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
1200	[number]	k4	1200
až	až	k9	až
1300	[number]	k4	1300
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
druhý	druhý	k4xOgInSc1	druhý
nejobsáhlejší	obsáhlý	k2eAgInSc1d3	nejobsáhlejší
rod	rod	k1gInSc1	rod
bobovitých	bobovitý	k2eAgMnPc2d1	bobovitý
<g/>
.	.	kIx.	.
</s>
<s>
Akácie	akácie	k1gFnPc1	akácie
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
a	a	k8xC	a
subtropech	subtropy	k1gInPc6	subtropy
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
však	však	k9	však
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
druhové	druhový	k2eAgFnSc2d1	druhová
diverzity	diverzita	k1gFnSc2	diverzita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
australské	australský	k2eAgFnSc6d1	australská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roste	růst	k5eAaImIp3nS	růst
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
900	[number]	k4	900
druhů	druh	k1gInPc2	druh
akácií	akácie	k1gFnPc2	akácie
<g/>
.	.	kIx.	.
</s>
<s>
Akácie	akácie	k1gFnSc1	akácie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
regionu	region	k1gInSc6	region
druhově	druhově	k6eAd1	druhově
nejbohatší	bohatý	k2eAgInSc1d3	nejbohatší
rod	rod	k1gInSc1	rod
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
druhová	druhový	k2eAgFnSc1d1	druhová
pestrost	pestrost	k1gFnSc1	pestrost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
semiaridních	semiaridní	k2eAgFnPc6d1	semiaridní
oblastech	oblast	k1gFnPc6	oblast
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dominují	dominovat	k5eAaImIp3nP	dominovat
druhy	druh	k1gInPc1	druh
z	z	k7c2	z
podrodu	podrod	k1gInSc2	podrod
Phyllodineae	Phyllodinea	k1gInSc2	Phyllodinea
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
je	být	k5eAaImIp3nS	být
uváděno	uvádět	k5eAaImNgNnS	uvádět
asi	asi	k9	asi
270	[number]	k4	270
druhů	druh	k1gInPc2	druh
akácií	akácie	k1gFnPc2	akácie
<g/>
,	,	kIx,	,
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
asi	asi	k9	asi
130	[number]	k4	130
<g/>
,	,	kIx,	,
z	z	k7c2	z
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
asi	asi	k9	asi
100	[number]	k4	100
a	a	k8xC	a
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
asi	asi	k9	asi
55	[number]	k4	55
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
květeně	květena	k1gFnSc6	květena
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
druh	druh	k1gInSc1	druh
akácie	akácie	k1gFnSc2	akácie
původní	původní	k2eAgFnSc2d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
australské	australský	k2eAgFnPc1d1	australská
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k9	i
africké	africký	k2eAgInPc1d1	africký
či	či	k8xC	či
americké	americký	k2eAgInPc1d1	americký
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
vysazovány	vysazován	k2eAgInPc1d1	vysazován
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
jako	jako	k8xC	jako
okrasné	okrasný	k2eAgFnPc4d1	okrasná
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
.	.	kIx.	.
<g/>
Akácie	akácie	k1gFnPc4	akácie
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
otevřená	otevřený	k2eAgNnPc1d1	otevřené
<g/>
,	,	kIx,	,
slunná	slunný	k2eAgNnPc1d1	slunné
stanoviště	stanoviště	k1gNnPc1	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Rostou	růst	k5eAaImIp3nP	růst
zejména	zejména	k9	zejména
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
v	v	k7c6	v
monzunových	monzunový	k2eAgInPc6d1	monzunový
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
mýtinách	mýtina	k1gFnPc6	mýtina
ve	v	k7c6	v
stálezeleném	stálezelený	k2eAgInSc6d1	stálezelený
tropickém	tropický	k2eAgInSc6d1	tropický
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
savanách	savana	k1gFnPc6	savana
a	a	k8xC	a
v	v	k7c6	v
suchomilné	suchomilný	k2eAgFnSc6d1	suchomilná
keřové	keřový	k2eAgFnSc6d1	keřová
vegetaci	vegetace	k1gFnSc6	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
součást	součást	k1gFnSc4	součást
daných	daný	k2eAgNnPc2d1	dané
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
společenstev	společenstvo	k1gNnPc2	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Preferují	preferovat	k5eAaImIp3nP	preferovat
dobře	dobře	k6eAd1	dobře
propustné	propustný	k2eAgFnPc1d1	propustná
půdy	půda	k1gFnPc1	půda
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
do	do	k7c2	do
800	[number]	k4	800
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožené	ohrožený	k2eAgInPc1d1	ohrožený
druhy	druh	k1gInPc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
IUCN	IUCN	kA	IUCN
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
uvedeny	uvést	k5eAaPmNgInP	uvést
4	[number]	k4	4
druhy	druh	k1gInPc1	druh
akácií	akácie	k1gFnPc2	akácie
<g/>
:	:	kIx,	:
Acacia	Acacia	k1gFnSc1	Acacia
belairioides	belairioides	k1gMnSc1	belairioides
a	a	k8xC	a
Acacia	Acacia	k1gFnSc1	Acacia
roigii	roigie	k1gFnSc4	roigie
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
Acacia	Acacia	k1gFnSc1	Acacia
mathuataensis	mathuataensis	k1gFnSc1	mathuataensis
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Vanua	Vanuus	k1gMnSc2	Vanuus
Levu	Levus	k1gInSc2	Levus
v	v	k7c4	v
souostroví	souostroví	k1gNnSc4	souostroví
Fidži	Fidž	k1gFnSc3	Fidž
a	a	k8xC	a
Acacia	Acacia	k1gFnSc1	Acacia
anegadensis	anegadensis	k1gFnSc1	anegadensis
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Anegada	Anegada	k1gFnSc1	Anegada
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
je	být	k5eAaImIp3nS	být
vedeno	vést	k5eAaImNgNnS	vést
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
druhů	druh	k1gInPc2	druh
pocházejících	pocházející	k2eAgInPc2d1	pocházející
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
tropické	tropický	k2eAgFnSc2d1	tropická
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
)	)	kIx)	)
a	a	k8xC	a
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Acacia	Acacium	k1gNnSc2	Acacium
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1754	[number]	k4	1754
Philipem	Philip	k1gMnSc7	Philip
Millerem	Miller	k1gMnSc7	Miller
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
sem	sem	k6eAd1	sem
bylo	být	k5eAaImAgNnS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
množství	množství	k1gNnSc1	množství
nově	nově	k6eAd1	nově
popisovaných	popisovaný	k2eAgInPc2d1	popisovaný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
řazených	řazený	k2eAgFnPc2d1	řazená
dnes	dnes	k6eAd1	dnes
do	do	k7c2	do
zcela	zcela	k6eAd1	zcela
jiných	jiný	k2eAgInPc2d1	jiný
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
George	Georg	k1gInSc2	Georg
Bentham	Bentham	k1gInSc1	Bentham
jasně	jasně	k6eAd1	jasně
stanovil	stanovit	k5eAaPmAgInS	stanovit
limity	limita	k1gFnSc2	limita
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgInPc4	který
náleželo	náležet	k5eAaImAgNnS	náležet
zejména	zejména	k9	zejména
množství	množství	k1gNnSc1	množství
volných	volný	k2eAgFnPc2d1	volná
tyčinek	tyčinka	k1gFnPc2	tyčinka
v	v	k7c6	v
květu	květ	k1gInSc6	květ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
pak	pak	k6eAd1	pak
publikoval	publikovat	k5eAaBmAgMnS	publikovat
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
klasifikaci	klasifikace	k1gFnSc4	klasifikace
akácií	akácie	k1gFnPc2	akácie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasická	klasický	k2eAgFnSc1d1	klasická
taxonomie	taxonomie	k1gFnSc1	taxonomie
akácií	akácie	k1gFnPc2	akácie
===	===	k?	===
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Acacia	Acacium	k1gNnSc2	Acacium
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
taxonomii	taxonomie	k1gFnSc6	taxonomie
bobovitých	bobovitý	k2eAgFnPc2d1	bobovitá
řazen	řadit	k5eAaImNgMnS	řadit
do	do	k7c2	do
samostatného	samostatný	k2eAgInSc2d1	samostatný
tribu	trib	k1gInSc2	trib
Acacieae	Acaciea	k1gMnSc2	Acaciea
<g/>
,	,	kIx,	,
obsahujícího	obsahující	k2eAgMnSc2d1	obsahující
jediný	jediný	k2eAgInSc4d1	jediný
rod	rod	k1gInSc4	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
systémech	systém	k1gInPc6	systém
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
tribu	triba	k1gMnSc4	triba
Acacieae	Acaciea	k1gMnSc2	Acaciea
řazen	řadit	k5eAaImNgInS	řadit
ještě	ještě	k9	ještě
africký	africký	k2eAgInSc1d1	africký
monotypický	monotypický	k2eAgInSc1d1	monotypický
rod	rod	k1gInSc1	rod
Faidherbia	Faidherbium	k1gNnSc2	Faidherbium
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Acacia	Acacium	k1gNnSc2	Acacium
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
tradičně	tradičně	k6eAd1	tradičně
členěn	členit	k5eAaImNgInS	členit
na	na	k7c4	na
4	[number]	k4	4
podrody	podrod	k1gInPc4	podrod
<g/>
:	:	kIx,	:
Acacia	Acacia	k1gFnSc1	Acacia
<g/>
,	,	kIx,	,
Aculeiferum	Aculeiferum	k1gNnSc1	Aculeiferum
<g/>
,	,	kIx,	,
Phyllodineae	Phyllodineae	k1gNnSc1	Phyllodineae
a	a	k8xC	a
Filicinae	Filicinae	k1gNnSc1	Filicinae
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c4	na
3	[number]	k4	3
podrody	podrod	k1gInPc4	podrod
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
byly	být	k5eAaImAgFnP	být
Filicinae	Filicinae	k1gNnSc4	Filicinae
brány	brána	k1gFnSc2	brána
jako	jako	k8xS	jako
sekce	sekce	k1gFnSc2	sekce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
podrodu	podrod	k1gInSc2	podrod
Aculeiferum	Aculeiferum	k1gNnSc1	Aculeiferum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
taxonomii	taxonomie	k1gFnSc6	taxonomie
akácií	akácie	k1gFnPc2	akácie
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc2	jejich
příbuzenských	příbuzenský	k2eAgInPc2d1	příbuzenský
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
triby	trib	k1gMnPc7	trib
bývalé	bývalý	k2eAgFnSc2d1	bývalá
podčeledi	podčeleď	k1gFnSc2	podčeleď
Mimosoideae	Mimosoidea	k1gFnSc2	Mimosoidea
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
vnitrorodovém	vnitrorodový	k2eAgNnSc6d1	vnitrorodový
členění	členění	k1gNnSc6	členění
panovaly	panovat	k5eAaImAgFnP	panovat
dlouho	dlouho	k6eAd1	dlouho
nejasnosti	nejasnost	k1gFnPc4	nejasnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
Pedley	Pedleum	k1gNnPc7	Pedleum
základním	základní	k2eAgNnSc7d1	základní
3	[number]	k4	3
podrodům	podrod	k1gInPc3	podrod
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
<g/>
,	,	kIx,	,
Aculeiferum	Aculeiferum	k1gInSc1	Aculeiferum
<g/>
,	,	kIx,	,
Phyllodineae	Phyllodineae	k1gInSc1	Phyllodineae
)	)	kIx)	)
přidělil	přidělit	k5eAaPmAgInS	přidělit
úroveň	úroveň	k1gFnSc4	úroveň
rodů	rod	k1gInPc2	rod
a	a	k8xC	a
rod	rod	k1gInSc1	rod
Acacia	Acacium	k1gNnSc2	Acacium
tak	tak	k6eAd1	tak
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
nové	nový	k2eAgInPc4d1	nový
rody	rod	k1gInPc4	rod
<g/>
:	:	kIx,	:
Acacia	Acacia	k1gFnSc1	Acacia
<g/>
,	,	kIx,	,
Senegalia	Senegalia	k1gFnSc1	Senegalia
a	a	k8xC	a
Racosperma	Racosperma	k1gFnSc1	Racosperma
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
všeobecně	všeobecně	k6eAd1	všeobecně
přijato	přijmout	k5eAaPmNgNnS	přijmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nová	nový	k2eAgFnSc1d1	nová
taxonomie	taxonomie	k1gFnSc1	taxonomie
akácií	akácie	k1gFnPc2	akácie
===	===	k?	===
</s>
</p>
<p>
<s>
Několika	několik	k4yIc7	několik
molekulárními	molekulární	k2eAgFnPc7d1	molekulární
kladistickými	kladistický	k2eAgFnPc7d1	kladistická
studiemi	studie	k1gFnPc7	studie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rod	rod	k1gInSc1	rod
Acacia	Acacium	k1gNnSc2	Acacium
v	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
pojetí	pojetí	k1gNnSc6	pojetí
není	být	k5eNaImIp3nS	být
monofyletický	monofyletický	k2eAgInSc1d1	monofyletický
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vývojové	vývojový	k2eAgFnPc1d1	vývojová
linie	linie	k1gFnPc1	linie
akácií	akácie	k1gFnPc2	akácie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	on	k3xPp3gMnPc4	on
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
fylogenetické	fylogenetický	k2eAgFnPc1d1	fylogenetická
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dokonce	dokonce	k9	dokonce
rozesety	rozeset	k2eAgFnPc1d1	rozeseta
v	v	k7c6	v
několika	několik	k4yIc6	několik
různých	různý	k2eAgInPc6d1	různý
tribech	trib	k1gInPc6	trib
bývalé	bývalý	k2eAgFnSc2d1	bývalá
podčeledi	podčeleď	k1gFnSc2	podčeleď
Mimosoideae	Mimosoidea	k1gFnSc2	Mimosoidea
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
studií	studie	k1gFnPc2	studie
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
Acacia	Acacium	k1gNnSc2	Acacium
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
samostatných	samostatný	k2eAgInPc2d1	samostatný
rodů	rod	k1gInPc2	rod
<g/>
:	:	kIx,	:
Acacia	Acacia	k1gFnSc1	Acacia
s.	s.	k?	s.
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Acaciella	Acaciell	k1gMnSc4	Acaciell
<g/>
,	,	kIx,	,
Mariosousa	Mariosous	k1gMnSc4	Mariosous
<g/>
,	,	kIx,	,
Senegalia	Senegalius	k1gMnSc4	Senegalius
a	a	k8xC	a
Vachellia	Vachellius	k1gMnSc4	Vachellius
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
však	však	k9	však
stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
dovršen	dovršen	k2eAgInSc4d1	dovršen
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
indicie	indicie	k1gFnPc1	indicie
<g/>
,	,	kIx,	,
že	že	k8xS	že
rod	rod	k1gInSc1	rod
Senegalia	Senegalium	k1gNnSc2	Senegalium
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
pojetí	pojetí	k1gNnSc6	pojetí
monofyletický	monofyletický	k2eAgMnSc1d1	monofyletický
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
známo	znám	k2eAgNnSc1d1	známo
nové	nový	k2eAgNnSc1d1	nové
rozdělení	rozdělení	k1gNnSc1	rozdělení
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
podčeledi	podčeleď	k1gFnSc2	podčeleď
Caesalpinioideae	Caesalpinioidea	k1gFnSc2	Caesalpinioidea
do	do	k7c2	do
tribů	trib	k1gInPc2	trib
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zdrojů	zdroj	k1gInPc2	zdroj
poskytujících	poskytující	k2eAgInPc2d1	poskytující
taxonomické	taxonomický	k2eAgFnPc4d1	Taxonomická
informace	informace	k1gFnPc4	informace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
)	)	kIx)	)
tyto	tento	k3xDgFnPc4	tento
změny	změna	k1gFnPc4	změna
dosud	dosud	k6eAd1	dosud
nereflektuje	reflektovat	k5eNaImIp3nS	reflektovat
<g/>
.	.	kIx.	.
<g/>
Typovým	typový	k2eAgInSc7d1	typový
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
Acacia	Acacium	k1gNnSc2	Acacium
popsán	popsat	k5eAaPmNgInS	popsat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Acacia	Acacia	k1gFnSc1	Acacia
nilotica	nilotica	k1gFnSc1	nilotica
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
se	se	k3xPyFc4	se
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
rodu	rod	k1gInSc2	rod
Acacia	Acacius	k1gMnSc2	Acacius
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
čítající	čítající	k2eAgFnSc6d1	čítající
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
160	[number]	k4	160
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgInSc1d1	bývalý
podrod	podrod	k1gInSc1	podrod
Acacia	Acacium	k1gNnSc2	Acacium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
botanické	botanický	k2eAgFnSc2d1	botanická
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
nést	nést	k5eAaImF	nést
rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
Acacia	Acacium	k1gNnSc2	Acacium
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
čítající	čítající	k2eAgFnSc4d1	čítající
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
vesměs	vesměs	k6eAd1	vesměs
australských	australský	k2eAgInPc2d1	australský
druhů	druh	k1gInPc2	druh
akácií	akácie	k1gFnPc2	akácie
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgInSc1d1	bývalý
podrod	podrod	k1gInSc1	podrod
Phyllodineae	Phyllodinea	k1gFnSc2	Phyllodinea
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jméno	jméno	k1gNnSc4	jméno
Racosperma	Racosperma	k1gNnSc4	Racosperma
<g/>
.	.	kIx.	.
</s>
<s>
Záležitost	záležitost	k1gFnSc1	záležitost
byla	být	k5eAaImAgFnS	být
řešena	řešit	k5eAaImNgFnS	řešit
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
botanických	botanický	k2eAgInPc6d1	botanický
kongresech	kongres	k1gInPc6	kongres
v	v	k7c6	v
letech	let	k1gInPc6	let
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
podána	podat	k5eAaPmNgFnS	podat
žádost	žádost	k1gFnSc1	žádost
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
typového	typový	k2eAgInSc2d1	typový
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
schválena	schválit	k5eAaPmNgFnS	schválit
a	a	k8xC	a
novým	nový	k2eAgInSc7d1	nový
typovým	typový	k2eAgInSc7d1	typový
druhem	druh	k1gInSc7	druh
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
Acacia	Acacium	k1gNnSc2	Acacium
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
druh	druh	k1gInSc1	druh
Acacia	Acacium	k1gNnSc2	Acacium
penninervis	penninervis	k1gFnSc2	penninervis
<g/>
.	.	kIx.	.
</s>
<s>
Rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
tak	tak	k6eAd1	tak
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
největší	veliký	k2eAgFnSc3d3	veliký
skupině	skupina	k1gFnSc3	skupina
akácií	akácie	k1gFnPc2	akácie
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
podrodu	podrod	k1gInSc2	podrod
Phyllodineae	Phyllodinea	k1gFnSc2	Phyllodinea
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
bývalý	bývalý	k2eAgInSc1d1	bývalý
podrod	podrod	k1gInSc1	podrod
Acacia	Acacium	k1gNnSc2	Acacium
dostal	dostat	k5eAaPmAgInS	dostat
nové	nový	k2eAgNnSc4d1	nové
rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
Vachellia	Vachellium	k1gNnSc2	Vachellium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rod	rod	k1gInSc1	rod
Acaciella	Acaciello	k1gNnSc2	Acaciello
====	====	k?	====
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
15	[number]	k4	15
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
tvořících	tvořící	k2eAgInPc2d1	tvořící
podrod	podrod	k1gInSc4	podrod
či	či	k8xC	či
sekci	sekce	k1gFnSc4	sekce
Filicinae	Filicina	k1gFnSc2	Filicina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
od	od	k7c2	od
USA	USA	kA	USA
po	po	k7c4	po
Argentinu	Argentina	k1gFnSc4	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
druhové	druhový	k2eAgNnSc1d1	druhové
bohatství	bohatství	k1gNnSc1	bohatství
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
akácií	akácie	k1gFnPc2	akácie
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
nejsou	být	k5eNaImIp3nP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
na	na	k7c6	na
listech	list	k1gInPc6	list
nektáriové	ktáriový	k2eNgFnSc2d1	ktáriový
žlázky	žlázka	k1gFnSc2	žlázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rod	rod	k1gInSc1	rod
Acacia	Acacia	k1gFnSc1	Acacia
s.	s.	k?	s.
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
====	====	k?	====
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
rod	rod	k1gInSc1	rod
akácií	akácie	k1gFnPc2	akácie
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
druhů	druh	k1gInPc2	druh
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
podrodu	podrod	k1gInSc2	podrod
Phyllodineae	Phyllodinea	k1gFnSc2	Phyllodinea
<g/>
.	.	kIx.	.
</s>
<s>
Dominuje	dominovat	k5eAaImIp3nS	dominovat
zejména	zejména	k9	zejména
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
(	(	kIx(	(
<g/>
asi	asi	k9	asi
950	[number]	k4	950
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tichomoří	Tichomoří	k1gNnSc2	Tichomoří
(	(	kIx(	(
<g/>
7	[number]	k4	7
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
také	také	k9	také
na	na	k7c4	na
Taiwanu	Taiwana	k1gFnSc4	Taiwana
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
jv	jv	k?	jv
<g/>
.	.	kIx.	.
</s>
<s>
Asie	Asie	k1gFnSc1	Asie
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
(	(	kIx(	(
<g/>
2	[number]	k4	2
druhy	druh	k1gInPc4	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
jsou	být	k5eAaImIp3nP	být
listy	list	k1gInPc1	list
redukovány	redukován	k2eAgInPc1d1	redukován
na	na	k7c6	na
různě	různě	k6eAd1	různě
tvarovaná	tvarovaný	k2eAgNnPc4d1	tvarované
fylódia	fylódium	k1gNnPc4	fylódium
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
jsou	být	k5eAaImIp3nP	být
listy	list	k1gInPc1	list
dvakrát	dvakrát	k6eAd1	dvakrát
zpeřené	zpeřený	k2eAgInPc1d1	zpeřený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řapících	řapík	k1gInPc6	řapík
a	a	k8xC	a
středních	střední	k2eAgInPc6d1	střední
žebrech	žebr	k1gInPc6	žebr
listů	list	k1gInPc2	list
jsou	být	k5eAaImIp3nP	být
žlázky	žlázka	k1gFnPc1	žlázka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
akácie	akácie	k1gFnPc1	akácie
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
beztrnné	beztrnný	k2eAgInPc1d1	beztrnný
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
nemnohých	mnohý	k2eNgInPc2d1	nemnohý
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
palisty	palist	k1gInPc4	palist
přeměněny	přeměněn	k2eAgInPc4d1	přeměněn
v	v	k7c4	v
trny	trn	k1gInPc4	trn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rod	rod	k1gInSc1	rod
Senegalia	Senegalium	k1gNnSc2	Senegalium
====	====	k?	====
</s>
</p>
<p>
<s>
Pantropicky	pantropicky	k6eAd1	pantropicky
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
rod	rod	k1gInSc1	rod
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
asi	asi	k9	asi
203	[number]	k4	203
druhů	druh	k1gInPc2	druh
řazených	řazený	k2eAgInPc2d1	řazený
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
do	do	k7c2	do
podrodu	podrod	k1gInSc2	podrod
Aculeiferum	Aculeiferum	k1gNnSc1	Aculeiferum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
asi	asi	k9	asi
100	[number]	k4	100
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
69	[number]	k4	69
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
43	[number]	k4	43
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
druhy	druh	k1gInPc4	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
šplhavé	šplhavý	k2eAgInPc1d1	šplhavý
keře	keř	k1gInPc1	keř
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nP	řadit
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
i	i	k9	i
všechny	všechen	k3xTgFnPc4	všechen
akácie	akácie	k1gFnPc4	akácie
liánovitého	liánovitý	k2eAgInSc2d1	liánovitý
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
dospělých	dospělý	k2eAgFnPc2d1	dospělá
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
dvakrát	dvakrát	k6eAd1	dvakrát
zpeřené	zpeřený	k2eAgFnPc1d1	zpeřená
<g/>
,	,	kIx,	,
na	na	k7c6	na
řapíku	řapík	k1gInSc6	řapík
a	a	k8xC	a
středním	střední	k2eAgInSc6d1	střední
žebru	žebr	k1gInSc6	žebr
listu	list	k1gInSc2	list
jsou	být	k5eAaImIp3nP	být
žlázky	žlázka	k1gFnPc1	žlázka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
větévkách	větévka	k1gFnPc6	větévka
bývají	bývat	k5eAaImIp3nP	bývat
trny	trn	k1gInPc1	trn
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozesety	rozesít	k5eAaPmNgFnP	rozesít
mezi	mezi	k7c7	mezi
internodii	internodium	k1gNnPc7	internodium
nebo	nebo	k8xC	nebo
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
či	či	k8xC	či
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
pod	pod	k7c7	pod
bází	báze	k1gFnSc7	báze
internodia	internodium	k1gNnSc2	internodium
<g/>
.	.	kIx.	.
</s>
<s>
Palisty	palist	k1gInPc1	palist
nejsou	být	k5eNaImIp3nP	být
přeměněné	přeměněný	k2eAgInPc1d1	přeměněný
v	v	k7c4	v
trny	trn	k1gInPc4	trn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rod	rod	k1gInSc1	rod
Mariosousa	Mariosous	k1gMnSc2	Mariosous
====	====	k?	====
</s>
</p>
<p>
<s>
Marisousa	Marisous	k1gMnSc4	Marisous
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc1d1	nový
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
popsaný	popsaný	k2eAgInSc1d1	popsaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
13	[number]	k4	13
druhů	druh	k1gInPc2	druh
akácií	akácie	k1gFnPc2	akácie
<g/>
,	,	kIx,	,
rozšířených	rozšířený	k2eAgInPc2d1	rozšířený
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rod	rod	k1gInSc1	rod
Vachellia	Vachellium	k1gNnSc2	Vachellium
====	====	k?	====
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
163	[number]	k4	163
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
řazených	řazený	k2eAgFnPc2d1	řazená
do	do	k7c2	do
podrodu	podrod	k1gInSc2	podrod
Acacia	Acacium	k1gNnSc2	Acacium
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
sporadicky	sporadicky	k6eAd1	sporadicky
i	i	k9	i
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
roste	růst	k5eAaImIp3nS	růst
pouze	pouze	k6eAd1	pouze
9	[number]	k4	9
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
stromy	strom	k1gInPc1	strom
nebo	nebo	k8xC	nebo
keře	keř	k1gInPc1	keř
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
dospělých	dospělý	k2eAgFnPc2d1	dospělá
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
dvakrát	dvakrát	k6eAd1	dvakrát
zpeřené	zpeřený	k2eAgFnPc1d1	zpeřená
<g/>
,	,	kIx,	,
se	s	k7c7	s
žlázkami	žlázka	k1gFnPc7	žlázka
na	na	k7c6	na
řapíku	řapík	k1gInSc6	řapík
a	a	k8xC	a
středním	střední	k2eAgInSc6d1	střední
žebru	žebr	k1gInSc6	žebr
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
mají	mít	k5eAaImIp3nP	mít
palisty	palist	k1gInPc1	palist
přeměněné	přeměněný	k2eAgInPc1d1	přeměněný
v	v	k7c4	v
trny	trn	k1gInPc4	trn
<g/>
.	.	kIx.	.
</s>
<s>
Africké	africký	k2eAgInPc1d1	africký
druhy	druh	k1gInPc1	druh
mívají	mívat	k5eAaImIp3nP	mívat
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
<g/>
,	,	kIx,	,
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
zploštělé	zploštělý	k2eAgFnSc2d1	zploštělá
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vývojový	vývojový	k2eAgInSc1d1	vývojový
strom	strom	k1gInSc1	strom
odvozenější	odvozený	k2eAgFnSc2d2	odvozený
části	část	k1gFnSc2	část
podčeledi	podčeleď	k1gFnSc2	podčeleď
Caesalpinioideae	Caesalpinioidea	k1gFnSc2	Caesalpinioidea
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologické	ekologický	k2eAgFnPc1d1	ekologická
interakce	interakce	k1gFnPc1	interakce
==	==	k?	==
</s>
</p>
<p>
<s>
Květy	květ	k1gInPc1	květ
akácií	akácie	k1gFnPc2	akácie
jsou	být	k5eAaImIp3nP	být
opylovány	opylován	k2eAgInPc1d1	opylován
zejména	zejména	k9	zejména
včelami	včela	k1gFnPc7	včela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
mnohé	mnohý	k2eAgInPc4d1	mnohý
druhy	druh	k1gInPc4	druh
amerických	americký	k2eAgFnPc2d1	americká
akácií	akácie	k1gFnPc2	akácie
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
symbióza	symbióza	k1gFnSc1	symbióza
s	s	k7c7	s
mravenci	mravenec	k1gMnPc7	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vztah	vztah	k1gInSc1	vztah
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
myrmekofilie	myrmekofilie	k1gFnSc1	myrmekofilie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncích	konec	k1gInPc6	konec
lístků	lístek	k1gInPc2	lístek
takové	takový	k3xDgFnPc1	takový
akácie	akácie	k1gFnPc1	akácie
jsou	být	k5eAaImIp3nP	být
produkována	produkován	k2eAgFnSc1d1	produkována
tzv.	tzv.	kA	tzv.
Beltova	Beltův	k2eAgNnSc2d1	Beltův
tělíska	tělísko	k1gNnSc2	tělísko
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgInPc1d1	sloužící
mravencům	mravenec	k1gMnPc3	mravenec
k	k	k7c3	k
výživě	výživa	k1gFnSc3	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
mívají	mívat	k5eAaImIp3nP	mívat
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
na	na	k7c6	na
řapíku	řapík	k1gInSc6	řapík
listů	list	k1gInPc2	list
zvětšená	zvětšený	k2eAgNnPc4d1	zvětšené
nektária	nektárium	k1gNnPc4	nektárium
produkující	produkující	k2eAgInSc4d1	produkující
sladkou	sladký	k2eAgFnSc4d1	sladká
šťávu	šťáva	k1gFnSc4	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
akácií	akácie	k1gFnPc2	akácie
mají	mít	k5eAaImIp3nP	mít
nápadné	nápadný	k2eAgInPc1d1	nápadný
zvětšené	zvětšený	k2eAgInPc1d1	zvětšený
duté	dutý	k2eAgInPc1d1	dutý
trny	trn	k1gInPc1	trn
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
mravencům	mravenec	k1gMnPc3	mravenec
za	za	k7c4	za
obydlí	obydlí	k1gNnSc4	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
chrání	chránit	k5eAaImIp3nS	chránit
rostlinu	rostlina	k1gFnSc4	rostlina
před	před	k7c7	před
býložravci	býložravec	k1gMnPc7	býložravec
a	a	k8xC	a
škůdci	škůdce	k1gMnPc7	škůdce
a	a	k8xC	a
likvidují	likvidovat	k5eAaBmIp3nP	likvidovat
konkurenční	konkurenční	k2eAgFnSc4d1	konkurenční
vegetaci	vegetace	k1gFnSc4	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
druhu	druh	k1gInSc2	druh
Acacia	Acacium	k1gNnSc2	Acacium
cornigera	cornigero	k1gNnSc2	cornigero
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vymizí	vymizet	k5eAaPmIp3nP	vymizet
<g/>
-li	i	k?	-li
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
symbiotický	symbiotický	k2eAgMnSc1d1	symbiotický
mravenec	mravenec	k1gMnSc1	mravenec
Pseudomyrmex	Pseudomyrmex	k1gInSc4	Pseudomyrmex
ferruginea	ferrugineum	k1gNnSc2	ferrugineum
<g/>
,	,	kIx,	,
přežijí	přežít	k5eAaPmIp3nP	přežít
jen	jen	k9	jen
izolované	izolovaný	k2eAgFnPc4d1	izolovaná
rostliny	rostlina	k1gFnPc4	rostlina
této	tento	k3xDgFnSc2	tento
akácie	akácie	k1gFnSc2	akácie
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
výjimečných	výjimečný	k2eAgFnPc2d1	výjimečná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Obligátní	obligátní	k2eAgFnSc1d1	obligátní
myrmekofilie	myrmekofilie	k1gFnSc1	myrmekofilie
nebyla	být	k5eNaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
u	u	k7c2	u
australských	australský	k2eAgInPc2d1	australský
druhů	druh	k1gInPc2	druh
akácií	akácie	k1gFnPc2	akácie
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
akácií	akácie	k1gFnPc2	akácie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
africká	africký	k2eAgFnSc1d1	africká
Acacia	Acacia	k1gFnSc1	Acacia
ataxacantha	ataxacanth	k1gMnSc2	ataxacanth
a	a	k8xC	a
australské	australský	k2eAgFnSc2d1	australská
A.	A.	kA	A.
concinna	concinn	k1gMnSc2	concinn
<g/>
,	,	kIx,	,
A.	A.	kA	A.
megaladena	megaladen	k2eAgFnSc1d1	megaladen
<g/>
,	,	kIx,	,
A.	A.	kA	A.
pennata	pennata	k1gFnSc1	pennata
aj.	aj.	kA	aj.
druhy	druh	k1gInPc1	druh
z	z	k7c2	z
podrodu	podrod	k1gInSc2	podrod
Aculeiferum	Aculeiferum	k1gInSc4	Aculeiferum
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
fenotypovou	fenotypový	k2eAgFnSc4d1	fenotypová
plasticitu	plasticita	k1gFnSc4	plasticita
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
rostou	růst	k5eAaImIp3nP	růst
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
husté	hustý	k2eAgFnSc6d1	hustá
stromové	stromový	k2eAgFnSc6d1	stromová
vegetaci	vegetace	k1gFnSc6	vegetace
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dřevnaté	dřevnatý	k2eAgFnPc1d1	dřevnatá
liány	liána	k1gFnPc1	liána
<g/>
,	,	kIx,	,
na	na	k7c6	na
otevřených	otevřený	k2eAgNnPc6d1	otevřené
stanovištích	stanoviště	k1gNnPc6	stanoviště
rostou	růst	k5eAaImIp3nP	růst
jako	jako	k8xS	jako
vzpřímené	vzpřímený	k2eAgInPc1d1	vzpřímený
keře	keř	k1gInPc1	keř
nebo	nebo	k8xC	nebo
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc4d1	mladý
semenáčky	semenáček	k1gInPc4	semenáček
těch	ten	k3xDgInPc2	ten
druhů	druh	k1gInPc2	druh
akácií	akácie	k1gFnPc2	akácie
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
listy	list	k1gInPc1	list
redukované	redukovaný	k2eAgInPc1d1	redukovaný
na	na	k7c4	na
fylódia	fylódium	k1gNnPc4	fylódium
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
první	první	k4xOgInPc4	první
listy	list	k1gInPc4	list
jednoduše	jednoduše	k6eAd1	jednoduše
nebo	nebo	k8xC	nebo
dvakrát	dvakrát	k6eAd1	dvakrát
zpeřené	zpeřený	k2eAgNnSc1d1	zpeřené
a	a	k8xC	a
až	až	k9	až
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
fylódia	fylódium	k1gNnSc2	fylódium
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
takových	takový	k3xDgInPc2	takový
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Acacia	Acacia	k1gFnSc1	Acacia
melanoxylon	melanoxylon	k1gInSc1	melanoxylon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
dospělých	dospělý	k2eAgFnPc2d1	dospělá
rostlin	rostlina	k1gFnPc2	rostlina
dokonce	dokonce	k9	dokonce
objevují	objevovat	k5eAaImIp3nP	objevovat
větve	větev	k1gFnPc1	větev
s	s	k7c7	s
dvakrát	dvakrát	k6eAd1	dvakrát
zpeřenými	zpeřený	k2eAgInPc7d1	zpeřený
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
druhu	druh	k1gInSc2	druh
A.	A.	kA	A.
heterophylla	heterophylla	k6eAd1	heterophylla
se	se	k3xPyFc4	se
mimo	mimo	k6eAd1	mimo
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
fylódií	fylódie	k1gFnPc2	fylódie
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
listy	list	k1gInPc1	list
tvořené	tvořený	k2eAgInPc1d1	tvořený
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
fylódiem	fylódium	k1gNnSc7	fylódium
a	a	k8xC	a
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
dvakrát	dvakrát	k6eAd1	dvakrát
zpeřenou	zpeřený	k2eAgFnSc7d1	zpeřená
čepelí	čepel	k1gFnSc7	čepel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
arabská	arabský	k2eAgFnSc1d1	arabská
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
nilotica	nilotica	k1gFnSc1	nilotica
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
dlouholistá	dlouholistý	k2eAgFnSc1d1	dlouholistá
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
longifolia	longifolia	k1gFnSc1	longifolia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
dlouhotrnná	dlouhotrnný	k2eAgFnSc1d1	dlouhotrnný
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
karoo	karoo	k1gMnSc1	karoo
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Acacia	Acacia	k1gFnSc1	Acacia
horrida	horrida	k1gFnSc1	horrida
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
dvoužilná	dvoužilný	k2eAgFnSc1d1	dvoužilný
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
pravissima	pravissima	k1gFnSc1	pravissima
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
egyptská	egyptský	k2eAgFnSc1d1	egyptská
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
seyal	seyal	k1gMnSc1	seyal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
Farnesova	Farnesův	k2eAgFnSc1d1	Farnesův
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
farnesiana	farnesiana	k1gFnSc1	farnesiana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
katechová	katechová	k1gFnSc1	katechová
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
catechu	catech	k1gInSc2	catech
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
pryskyřičnatá	pryskyřičnatý	k2eAgFnSc1d1	pryskyřičnatá
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
retinodes	retinodes	k1gMnSc1	retinodes
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
senegalská	senegalský	k2eAgFnSc1d1	senegalská
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
senegalensis	senegalensis	k1gFnSc1	senegalensis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
sivozelená	sivozelený	k2eAgFnSc1d1	sivozelená
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
dealbata	dealbata	k1gFnSc1	dealbata
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
trojboká	trojboký	k2eAgFnSc1d1	trojboká
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
cultriformis	cultriformis	k1gFnSc1	cultriformis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
pokroucená	pokroucený	k2eAgFnSc1d1	pokroucená
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
tortilis	tortilis	k1gFnSc1	tortilis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akácie	akácie	k1gFnSc1	akácie
Mearnsova	Mearnsův	k2eAgFnSc1d1	Mearnsův
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
mearnsii	mearnsie	k1gFnSc4	mearnsie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Akácie	akácie	k1gFnPc1	akácie
mají	mít	k5eAaImIp3nP	mít
poměrně	poměrně	k6eAd1	poměrně
mnohostranné	mnohostranný	k2eAgNnSc4d1	mnohostranné
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
zejména	zejména	k9	zejména
trvanlivé	trvanlivý	k2eAgNnSc4d1	trvanlivé
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
gumy	guma	k1gFnPc4	guma
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc4	tříslovina
a	a	k8xC	a
vonné	vonný	k2eAgInPc4d1	vonný
extrakty	extrakt	k1gInPc4	extrakt
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Pěstují	pěstovat	k5eAaImIp3nP	pěstovat
se	se	k3xPyFc4	se
i	i	k9	i
jako	jako	k9	jako
okrasné	okrasný	k2eAgFnPc4d1	okrasná
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Okrasné	okrasný	k2eAgFnPc1d1	okrasná
rostliny	rostlina	k1gFnPc1	rostlina
===	===	k?	===
</s>
</p>
<p>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
akácií	akácie	k1gFnPc2	akácie
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
klimaticky	klimaticky	k6eAd1	klimaticky
příhodných	příhodný	k2eAgFnPc6d1	příhodná
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
pěstovány	pěstován	k2eAgInPc4d1	pěstován
jako	jako	k8xC	jako
okrasné	okrasný	k2eAgFnPc4d1	okrasná
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
např.	např.	kA	např.
s	s	k7c7	s
australskými	australský	k2eAgInPc7d1	australský
druhy	druh	k1gInPc7	druh
Acacia	Acacius	k1gMnSc2	Acacius
cyanophylla	cyanophyll	k1gMnSc2	cyanophyll
<g/>
,	,	kIx,	,
A.	A.	kA	A.
dealbata	dealbata	k1gFnSc1	dealbata
<g/>
,	,	kIx,	,
A.	A.	kA	A.
longifolia	longifolia	k1gFnSc1	longifolia
<g/>
,	,	kIx,	,
A.	A.	kA	A.
mearnsii	mearnsie	k1gFnSc4	mearnsie
<g/>
,	,	kIx,	,
A.	A.	kA	A.
melanoxylon	melanoxylon	k1gInSc1	melanoxylon
<g/>
,	,	kIx,	,
A.	A.	kA	A.
pycnantha	pycnantha	k1gFnSc1	pycnantha
a	a	k8xC	a
A.	A.	kA	A.
retinodes	retinodes	k1gInSc1	retinodes
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
afrických	africký	k2eAgInPc2d1	africký
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vysazuje	vysazovat	k5eAaImIp3nS	vysazovat
zejména	zejména	k9	zejména
Acacia	Acacia	k1gFnSc1	Acacia
karoo	karoo	k6eAd1	karoo
<g/>
,	,	kIx,	,
z	z	k7c2	z
amerických	americký	k2eAgMnPc2d1	americký
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
pantropicky	pantropicky	k6eAd1	pantropicky
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
akácie	akácie	k1gFnSc1	akácie
Farnesova	Farnesův	k2eAgFnSc1d1	Farnesův
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
farnesiana	farnesiana	k1gFnSc1	farnesiana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledně	posledně	k6eAd1	posledně
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pěstován	pěstován	k2eAgMnSc1d1	pěstován
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1611	[number]	k4	1611
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
se	se	k3xPyFc4	se
jako	jako	k9	jako
pouliční	pouliční	k2eAgInSc1d1	pouliční
strom	strom	k1gInSc1	strom
vysazuje	vysazovat	k5eAaImIp3nS	vysazovat
australská	australský	k2eAgFnSc1d1	australská
akácie	akácie	k1gFnSc1	akácie
A.	A.	kA	A.
auriculiformis	auriculiformis	k1gFnSc1	auriculiformis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dřevo	dřevo	k1gNnSc1	dřevo
===	===	k?	===
</s>
</p>
<p>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
akácií	akácie	k1gFnPc2	akácie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
trvanlivé	trvanlivý	k2eAgNnSc1d1	trvanlivé
<g/>
,	,	kIx,	,
o	o	k7c6	o
hustotě	hustota	k1gFnSc6	hustota
640	[number]	k4	640
až	až	k9	až
800	[number]	k4	800
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
dosti	dosti	k6eAd1	dosti
obtížně	obtížně	k6eAd1	obtížně
opracovatelné	opracovatelný	k2eAgNnSc1d1	opracovatelné
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
statnějších	statný	k2eAgInPc2d2	statnější
druhů	druh	k1gInPc2	druh
akácií	akácie	k1gFnPc2	akácie
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
zejména	zejména	k9	zejména
na	na	k7c4	na
obklady	obklad	k1gInPc4	obklad
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
pomalu	pomalu	k6eAd1	pomalu
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
drobnějších	drobný	k2eAgInPc2d2	drobnější
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
zejména	zejména	k9	zejména
dýmky	dýmka	k1gFnPc1	dýmka
<g/>
,	,	kIx,	,
držadla	držadlo	k1gNnPc1	držadlo
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
drobnější	drobný	k2eAgInPc1d2	drobnější
předměty	předmět	k1gInPc1	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
častěji	často	k6eAd2	často
těžené	těžený	k2eAgInPc4d1	těžený
druhy	druh	k1gInPc4	druh
náleží	náležet	k5eAaImIp3nP	náležet
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
zejména	zejména	k9	zejména
Acacia	Acacia	k1gFnSc1	Acacia
glaucescens	glaucescensa	k1gFnPc2	glaucescensa
<g/>
,	,	kIx,	,
A.	A.	kA	A.
melanoxylon	melanoxylon	k1gInSc1	melanoxylon
a	a	k8xC	a
A.	A.	kA	A.
nigrescens	nigrescens	k1gInSc1	nigrescens
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
velmi	velmi	k6eAd1	velmi
ceněné	ceněný	k2eAgNnSc1d1	ceněné
dřevo	dřevo	k1gNnSc1	dřevo
místní	místní	k2eAgMnSc1d1	místní
druh	druh	k1gMnSc1	druh
Acacia	Acacium	k1gNnSc2	Acacium
koa	koa	k?	koa
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
havajský	havajský	k2eAgInSc1d1	havajský
mahagon	mahagon	k1gInSc1	mahagon
(	(	kIx(	(
<g/>
hawaiian	hawaiian	k1gInSc1	hawaiian
mahogany	mahogana	k1gFnSc2	mahogana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
zejména	zejména	k9	zejména
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
surfovacích	surfovací	k2eAgNnPc2d1	surfovací
prken	prkno	k1gNnPc2	prkno
<g/>
,	,	kIx,	,
kánoí	kánoe	k1gFnPc2	kánoe
a	a	k8xC	a
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
mís	mísa	k1gFnPc2	mísa
<g/>
.	.	kIx.	.
</s>
<s>
Akácie	akácie	k1gFnSc1	akácie
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
jako	jako	k9	jako
vyhledávaný	vyhledávaný	k2eAgInSc1d1	vyhledávaný
zdroj	zdroj	k1gInSc1	zdroj
topiva	topivo	k1gNnSc2	topivo
a	a	k8xC	a
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
často	často	k6eAd1	často
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
otopu	otop	k1gInSc2	otop
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
australské	australský	k2eAgFnSc2d1	australská
akácie	akácie	k1gFnSc2	akácie
Acacia	Acacia	k1gFnSc1	Acacia
melanoxylon	melanoxylon	k1gInSc1	melanoxylon
je	být	k5eAaImIp3nS	být
obchodováno	obchodován	k2eAgNnSc1d1	obchodováno
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
blackwood	blackwooda	k1gFnPc2	blackwooda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
trvanlivé	trvanlivý	k2eAgNnSc1d1	trvanlivé
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
opracovatelné	opracovatelný	k2eAgFnPc1d1	opracovatelná
<g/>
,	,	kIx,	,
s	s	k7c7	s
úzkou	úzký	k2eAgFnSc7d1	úzká
bělavou	bělavý	k2eAgFnSc7d1	bělavá
bělí	běl	k1gFnSc7	běl
a	a	k8xC	a
zlatohnědým	zlatohnědý	k2eAgInSc7d1	zlatohnědý
až	až	k6eAd1	až
načervenale	načervenale	k6eAd1	načervenale
hnědým	hnědý	k2eAgNnSc7d1	hnědé
jádrovým	jádrový	k2eAgNnSc7d1	jádrové
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Dosti	dosti	k6eAd1	dosti
široké	široký	k2eAgNnSc1d1	široké
využití	využití	k1gNnSc1	využití
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
dřevo	dřevo	k1gNnSc1	dřevo
australského	australský	k2eAgInSc2d1	australský
druhu	druh	k1gInSc2	druh
A.	A.	kA	A.
crassicarpa	crassicarp	k1gMnSc2	crassicarp
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
dřeva	dřevo	k1gNnSc2	dřevo
druh	druh	k1gInSc1	druh
A.	A.	kA	A.
visco	visco	k6eAd1	visco
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Využití	využití	k1gNnSc1	využití
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
a	a	k8xC	a
medicíně	medicína	k1gFnSc6	medicína
===	===	k?	===
</s>
</p>
<p>
<s>
Kyselé	kyselý	k2eAgInPc1d1	kyselý
plody	plod	k1gInPc1	plod
Acacia	Acacius	k1gMnSc2	Acacius
concinna	concinn	k1gMnSc2	concinn
mají	mít	k5eAaImIp3nP	mít
využití	využití	k1gNnSc4	využití
ve	v	k7c6	v
filipínské	filipínský	k2eAgFnSc6d1	filipínská
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Extrakty	extrakt	k1gInPc1	extrakt
z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
léčitelství	léčitelství	k1gNnSc6	léčitelství
<g/>
,	,	kIx,	,
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
např.	např.	kA	např.
kořeny	kořen	k1gInPc7	kořen
Acacia	Acacium	k1gNnSc2	Acacium
nilotica	niloticus	k1gMnSc2	niloticus
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
spavé	spavý	k2eAgFnSc2d1	spavá
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Guma	guma	k1gFnSc1	guma
z	z	k7c2	z
akácie	akácie	k1gFnSc2	akácie
arabské	arabský	k2eAgNnSc1d1	arabské
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
nilotica	nilotica	k1gFnSc1	nilotica
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
průjmům	průjem	k1gInPc3	průjem
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
amébám	améba	k1gFnPc3	améba
a	a	k8xC	a
křečím	křeč	k1gFnPc3	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Prášek	prášek	k1gInSc1	prášek
z	z	k7c2	z
lusků	lusk	k1gInPc2	lusk
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
hubí	hubit	k5eAaImIp3nS	hubit
vodní	vodní	k2eAgNnSc1d1	vodní
plže	plž	k1gMnSc4	plž
přenášející	přenášející	k2eAgFnSc4d1	přenášející
bilharziózu	bilharzióza	k1gFnSc4	bilharzióza
<g/>
.	.	kIx.	.
</s>
<s>
Guma	guma	k1gFnSc1	guma
severoafrické	severoafrický	k2eAgFnSc2d1	severoafrická
akácie	akácie	k1gFnSc2	akácie
Acacia	Acacia	k1gFnSc1	Acacia
tortilis	tortilis	k1gFnSc1	tortilis
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
zánětech	zánět	k1gInPc6	zánět
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
onemocněních	onemocnění	k1gNnPc6	onemocnění
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
na	na	k7c4	na
hojení	hojení	k1gNnSc4	hojení
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Akácie	akácie	k1gFnSc1	akácie
katechová	katechová	k1gFnSc1	katechová
(	(	kIx(	(
<g/>
A.	A.	kA	A.
catechu	catech	k1gInSc2	catech
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
čínské	čínský	k2eAgFnSc6d1	čínská
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
A.	A.	kA	A.
pluricapitata	pluricapite	k1gNnPc1	pluricapite
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
při	při	k7c6	při
bolestech	bolest	k1gFnPc6	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
medicínské	medicínský	k2eAgNnSc1d1	medicínské
využití	využití	k1gNnSc1	využití
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
kůra	kůra	k1gFnSc1	kůra
z	z	k7c2	z
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Technické	technický	k2eAgInPc1d1	technický
produkty	produkt	k1gInPc1	produkt
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
akácií	akácie	k1gFnPc2	akácie
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
klovatina	klovatina	k1gFnSc1	klovatina
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
arabská	arabský	k2eAgFnSc1d1	arabská
guma	guma	k1gFnSc1	guma
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
<g/>
,	,	kIx,	,
farmacii	farmacie	k1gFnSc6	farmacie
i	i	k8xC	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
klovatinu	klovatina	k1gFnSc4	klovatina
produkuje	produkovat	k5eAaImIp3nS	produkovat
akácie	akácie	k1gFnSc1	akácie
senegalská	senegalský	k2eAgFnSc1d1	senegalská
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
senegalensis	senegalensis	k1gFnSc2	senegalensis
<g/>
)	)	kIx)	)
a	a	k8xC	a
akácie	akácie	k1gFnSc1	akácie
egyptská	egyptský	k2eAgFnSc1d1	egyptská
(	(	kIx(	(
<g/>
A.	A.	kA	A.
seyal	seyal	k1gInSc1	seyal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
akácie	akácie	k1gFnSc1	akácie
arabská	arabský	k2eAgFnSc1d1	arabská
(	(	kIx(	(
<g/>
A.	A.	kA	A.
nilotica	nilotica	k1gFnSc1	nilotica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
klovatinu	klovatina	k1gFnSc4	klovatina
získávat	získávat	k5eAaImF	získávat
i	i	k9	i
z	z	k7c2	z
afrických	africký	k2eAgInPc2d1	africký
druhů	druh	k1gInPc2	druh
A.	A.	kA	A.
abyssinica	abyssinicus	k1gMnSc2	abyssinicus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
giraffae	giraffae	k1gInSc1	giraffae
<g/>
,	,	kIx,	,
A.	A.	kA	A.
horrida	horrida	k1gFnSc1	horrida
<g/>
,	,	kIx,	,
A.	A.	kA	A.
kirkii	kirkie	k1gFnSc4	kirkie
a	a	k8xC	a
australské	australský	k2eAgFnSc2d1	australská
A.	A.	kA	A.
pycnantha	pycnanth	k1gMnSc2	pycnanth
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
extraktu	extrakt	k1gInSc2	extrakt
indické	indický	k2eAgFnSc2d1	indická
akácie	akácie	k1gFnSc2	akácie
katechové	katechová	k1gFnSc2	katechová
(	(	kIx(	(
<g/>
Acacia	Acacia	k1gFnSc1	Acacia
catechu	catech	k1gInSc2	catech
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
pryskyřičná	pryskyřičný	k2eAgFnSc1d1	pryskyřičná
hmota	hmota	k1gFnSc1	hmota
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
katechu	katech	k1gInSc2	katech
a	a	k8xC	a
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
koželužství	koželužství	k1gNnSc6	koželužství
a	a	k8xC	a
v	v	k7c4	v
potravinářství	potravinářství	k1gNnSc4	potravinářství
jako	jako	k8xC	jako
aromatikum	aromatikum	k1gNnSc4	aromatikum
<g/>
.	.	kIx.	.
</s>
<s>
Extrakt	extrakt	k1gInSc1	extrakt
z	z	k7c2	z
květů	květ	k1gInPc2	květ
akácie	akácie	k1gFnSc2	akácie
Farnesovy	Farnesův	k2eAgInPc1d1	Farnesův
(	(	kIx(	(
<g/>
A.	A.	kA	A.
farnesiana	farnesiana	k1gFnSc1	farnesiana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
nejluxusnějších	luxusní	k2eAgInPc2d3	nejluxusnější
parfémů	parfém	k1gInPc2	parfém
a	a	k8xC	a
rostlina	rostlina	k1gFnSc1	rostlina
má	mít	k5eAaImIp3nS	mít
využití	využití	k1gNnSc4	využití
i	i	k9	i
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kůry	kůra	k1gFnSc2	kůra
akácií	akácie	k1gFnPc2	akácie
Acacia	Acacia	k1gFnSc1	Acacia
mearnsii	mearnsie	k1gFnSc4	mearnsie
<g/>
,	,	kIx,	,
A.	A.	kA	A.
dealbata	dealbata	k1gFnSc1	dealbata
a	a	k8xC	a
A.	A.	kA	A.
pycnantha	pycnantha	k1gMnSc1	pycnantha
jsou	být	k5eAaImIp3nP	být
získávány	získáván	k2eAgFnPc4d1	získávána
třísloviny	tříslovina	k1gFnPc4	tříslovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
využití	využití	k1gNnPc4	využití
===	===	k?	===
</s>
</p>
<p>
<s>
Australská	australský	k2eAgFnSc1d1	australská
akácie	akácie	k1gFnSc1	akácie
A.	A.	kA	A.
mangium	mangium	k1gNnSc1	mangium
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rostoucím	rostoucí	k2eAgFnPc3d1	rostoucí
dřevinám	dřevina	k1gFnPc3	dřevina
<g/>
,	,	kIx,	,
v	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
výšky	výška	k1gFnSc2	výška
až	až	k9	až
23	[number]	k4	23
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
průměru	průměr	k1gInSc6	průměr
kmene	kmen	k1gInSc2	kmen
přes	přes	k7c4	přes
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
k	k	k7c3	k
zalesňování	zalesňování	k1gNnSc3	zalesňování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
Acacia	Acacia	k1gFnSc1	Acacia
auriculiformis	auriculiformis	k1gFnSc1	auriculiformis
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
pěstován	pěstován	k2eAgInSc1d1	pěstován
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc4	zdroj
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
,	,	kIx,	,
topiva	topivo	k1gNnSc2	topivo
a	a	k8xC	a
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
akácie	akácie	k1gFnPc1	akácie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
australské	australský	k2eAgFnSc2d1	australská
A.	A.	kA	A.
saligna	saligna	k6eAd1	saligna
a	a	k8xC	a
A.	A.	kA	A.
cyclops	cyclops	k1gInSc1	cyclops
a	a	k8xC	a
australská	australský	k2eAgFnSc1d1	australská
A.	A.	kA	A.
tortilis	tortilis	k1gFnSc1	tortilis
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vysazovány	vysazován	k2eAgFnPc1d1	vysazována
ke	k	k7c3	k
zpevnění	zpevnění	k1gNnSc3	zpevnění
písků	písek	k1gInPc2	písek
a	a	k8xC	a
proti	proti	k7c3	proti
erozi	eroze	k1gFnSc3	eroze
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
druhy	druh	k1gInPc1	druh
akácií	akácie	k1gFnPc2	akácie
jsou	být	k5eAaImIp3nP	být
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
pastvy	pastva	k1gFnSc2	pastva
pro	pro	k7c4	pro
včely	včela	k1gFnPc4	včela
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
mnohých	mnohý	k2eAgInPc2d1	mnohý
druhů	druh	k1gInPc2	druh
akácií	akácie	k1gFnPc2	akácie
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
krmivo	krmivo	k1gNnSc4	krmivo
pro	pro	k7c4	pro
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
listy	list	k1gInPc4	list
i	i	k8xC	i
plody	plod	k1gInPc4	plod
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
však	však	k9	však
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kyanogenní	kyanogenní	k2eAgInPc1d1	kyanogenní
glykosidy	glykosid	k1gInPc1	glykosid
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
akácie	akácie	k1gFnSc2	akácie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
akácie	akácie	k1gFnSc2	akácie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Acacia	Acacium	k1gNnSc2	Acacium
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Acacieae	Acaciea	k1gFnSc2	Acaciea
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Acacia	Acacium	k1gNnSc2	Acacium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
akácie	akácie	k1gFnSc2	akácie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
