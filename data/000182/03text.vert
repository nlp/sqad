<s>
Erik	Erik	k1gMnSc1	Erik
Pardus	pardus	k1gInSc1	pardus
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1957	[number]	k4	1957
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
JAMU	jam	k1gInSc2	jam
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
do	do	k7c2	do
Divadla	divadlo	k1gNnSc2	divadlo
bratří	bratr	k1gMnPc2	bratr
Mrštíků	Mrštík	k1gInPc2	Mrštík
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
však	však	k9	však
hrál	hrát	k5eAaImAgMnS	hrát
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnSc4	jeho
známější	známý	k2eAgFnPc1d2	známější
filmové	filmový	k2eAgFnPc1d1	filmová
role	role	k1gFnPc1	role
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
Píďa	Píďa	k1gFnSc1	Píďa
z	z	k7c2	z
filmu	film	k1gInSc2	film
Romance	romance	k1gFnSc2	romance
za	za	k7c4	za
korunu	koruna	k1gFnSc4	koruna
nebo	nebo	k8xC	nebo
strážmistr	strážmistr	k1gMnSc1	strážmistr
Zahálka	Zahálka	k1gMnSc1	Zahálka
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Četnické	četnický	k2eAgFnSc2d1	četnická
humoresky	humoreska	k1gFnSc2	humoreska
<g/>
.	.	kIx.	.
</s>
<s>
Obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
Thálie	Thálie	k1gFnSc2	Thálie
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
činohra	činohra	k1gFnSc1	činohra
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
za	za	k7c4	za
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
role	role	k1gFnSc2	role
Pavla	Pavel	k1gMnSc2	Pavel
I.	I.	kA	I.
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Smrt	smrt	k1gFnSc1	smrt
Pavla	Pavla	k1gFnSc1	Pavla
I.	I.	kA	I.
od	od	k7c2	od
Dmitrije	Dmitrije	k1gFnPc2	Dmitrije
Merežovského	Merežovského	k2eAgFnPc2d1	Merežovského
na	na	k7c6	na
prknech	prkno	k1gNnPc6	prkno
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
též	též	k9	též
jako	jako	k9	jako
dabér	dabér	k1gMnSc1	dabér
<g/>
.	.	kIx.	.
</s>
<s>
Podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
rakovině	rakovina	k1gFnSc3	rakovina
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
silný	silný	k2eAgInSc1d1	silný
kuřák	kuřák	k1gInSc1	kuřák
léčil	léčit	k5eAaImAgInS	léčit
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Evou	Eva	k1gFnSc7	Eva
Gorčicovou	Gorčicová	k1gFnSc7	Gorčicová
(	(	kIx(	(
<g/>
působila	působit	k5eAaImAgFnS	působit
též	též	k9	též
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
Mlčení	mlčení	k1gNnPc2	mlčení
mužů	muž	k1gMnPc2	muž
1969	[number]	k4	1969
Dospěláci	Dospěláci	k?	Dospěláci
můžou	můžou	k?	můžou
všechno	všechen	k3xTgNnSc4	všechen
1971	[number]	k4	1971
Panter	panter	k1gInSc1	panter
čeká	čekat	k5eAaImIp3nS	čekat
v	v	k7c4	v
17,30	[number]	k4	17,30
1971	[number]	k4	1971
Babička	babička	k1gFnSc1	babička
1972	[number]	k4	1972
Rodeo	rodeo	k1gNnSc4	rodeo
1973	[number]	k4	1973
Kazisvěti	kazisvět	k1gMnPc1	kazisvět
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
1975	[number]	k4	1975
<g />
.	.	kIx.	.
</s>
<s>
Romance	romance	k1gFnSc1	romance
za	za	k7c4	za
korunu	koruna	k1gFnSc4	koruna
1977	[number]	k4	1977
Proč	proč	k6eAd1	proč
nevěřit	věřit	k5eNaImF	věřit
na	na	k7c4	na
zázraky	zázrak	k1gInPc4	zázrak
1987	[number]	k4	1987
Profesor	profesor	k1gMnSc1	profesor
Popelnice	popelnice	k1gFnSc1	popelnice
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
Král	Král	k1gMnSc1	Král
lenochů	lenoch	k1gMnPc2	lenoch
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
O	o	k7c6	o
princezně	princezna	k1gFnSc6	princezna
se	s	k7c7	s
zlatým	zlatý	k2eAgInSc7d1	zlatý
lukem	luk	k1gInSc7	luk
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
O	o	k7c6	o
ševci	švec	k1gMnSc6	švec
Ondrovi	Ondra	k1gMnSc6	Ondra
a	a	k8xC	a
komtesce	komteska	k1gFnSc6	komteska
Julince	Julinka	k1gFnSc6	Julinka
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Český	český	k2eAgMnSc1d1	český
Honza	Honza	k1gMnSc1	Honza
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Zlatého	zlatý	k2eAgInSc2d1	zlatý
úsvitu	úsvit	k1gInSc2	úsvit
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
Detektiv	detektiv	k1gMnSc1	detektiv
Martin	Martin	k1gMnSc1	Martin
Tomsa	Tomsa	k1gFnSc1	Tomsa
1997	[number]	k4	1997
Hříšní	hříšný	k2eAgMnPc1d1	hříšný
lidé	člověk	k1gMnPc1	člověk
města	město	k1gNnSc2	město
brněnského	brněnský	k2eAgNnSc2d1	brněnské
1997	[number]	k4	1997
Četnické	četnický	k2eAgFnSc2d1	četnická
humoresky	humoreska	k1gFnSc2	humoreska
2003	[number]	k4	2003
Černí	černý	k2eAgMnPc1d1	černý
baroni	baron	k1gMnPc1	baron
2010	[number]	k4	2010
Okno	okno	k1gNnSc4	okno
do	do	k7c2	do
hřbitova	hřbitov	k1gInSc2	hřbitov
-	-	kIx~	-
povídka	povídka	k1gFnSc1	povídka
"	"	kIx"	"
<g/>
Poslání	poslání	k1gNnSc1	poslání
<g/>
"	"	kIx"	"
</s>
