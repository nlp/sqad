<s desamb="1">
Vařící	vařící	k2eAgFnSc1d1
voda	voda	k1gFnSc1
je	být	k5eAaImIp3nS
vyvrhována	vyvrhovat	k5eAaImNgFnS
až	až	k6eAd1
do	do	k7c2
výšky	výška	k1gFnSc2
kolem	kolem	k7c2
80	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1845	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
až	až	k9
170	#num#	k4
m.	m	kA
Mezi	mezi	k7c7
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
červnem	červen	k1gInSc7
2000	#num#	k4
<g/>
,	,	kIx,
během	během	k7c2
zemětřesení	zemětřesení	k1gNnSc2
<g/>
,	,	kIx,
dosahoval	dosahovat	k5eAaImAgInS
do	do	k7c2
výšky	výška	k1gFnSc2
122	#num#	k4
m	m	kA
po	po	k7c4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
a	a	k8xC
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
nejvyšší	vysoký	k2eAgInSc4d3
gejzír	gejzír	k1gInSc4
v	v	k7c6
činnosti	činnost	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
dočasně	dočasně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>