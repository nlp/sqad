<s>
Bučací	Bučací	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
</s>
<s>
Bučací	Bučací	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
Bučací	Bučací	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
pod	pod	k7c4
SmrkemRozměry	SmrkemRozměr	k1gInPc4
Celková	celkový	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
44,5	44,5	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
kraj	kraj	k7c2
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
okres	okres	k1gInSc1
</s>
<s>
Frýdek-Místek	Frýdek-Místek	k1gInSc1
obec	obec	k1gFnSc1
</s>
<s>
Ostravice	Ostravice	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Bučacívodopády	Bučacívodopáda	k1gFnPc1
</s>
<s>
Ostatní	ostatní	k2eAgInSc1d1
Vodní	vodní	k2eAgInSc1d1
tok	tok	k1gInSc1
</s>
<s>
Bučací	Bučací	k2eAgInSc1d1
potok	potok	k1gInSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Ostravice	Ostravice	k1gFnSc1
Typ	typ	k1gInSc1
</s>
<s>
kaskádový	kaskádový	k2eAgInSc1d1
vodopád	vodopád	k1gInSc1
Morfologický	morfologický	k2eAgInSc4d1
typ	typ	k1gInSc4
</s>
<s>
pravý	pravý	k2eAgInSc4d1
<g/>
,	,	kIx,
dvoustupňový	dvoustupňový	k2eAgInSc4d1
<g/>
,	,	kIx,
dvouramenný	dvouramenný	k2eAgInSc4d1
Genetický	genetický	k2eAgInSc4d1
typ	typ	k1gInSc4
</s>
<s>
přírodní	přírodní	k2eAgMnPc1d1
<g/>
,	,	kIx,
sekundárně	sekundárně	k6eAd1
subsekventní	subsekventní	k2eAgFnSc1d1
<g/>
,	,	kIx,
horizontálních	horizontální	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
Geologické	geologický	k2eAgNnSc1d1
podloží	podloží	k1gNnSc1
</s>
<s>
křídový	křídový	k2eAgInSc1d1
godulský	godulský	k2eAgInSc1d1
pískovec	pískovec	k1gInSc1
Geomorfologická	geomorfologický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
Radhošťská	radhošťský	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
<g/>
,	,	kIx,
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
Beskydy	Beskydy	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bučací	Bučací	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
<g/>
,	,	kIx,
tj.	tj.	kA
vodopády	vodopád	k1gInPc1
na	na	k7c6
Bučacím	Bučací	k2eAgInSc6d1
potoce	potok	k1gInSc6
v	v	k7c6
Beskydech	Beskyd	k1gInPc6
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
v	v	k7c6
katastru	katastr	k1gInSc6
obce	obec	k1gFnSc2
Ostravice	Ostravice	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
masivu	masiv	k1gInSc6
hory	hora	k1gFnSc2
Smrk	smrk	k1gInSc1
(	(	kIx(
<g/>
1276	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
kotli	kotel	k1gInSc6
stejnojmenného	stejnojmenný	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Bučací	Bučací	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
pod	pod	k7c7
Smrkem	smrk	k1gInSc7
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
jedny	jeden	k4xCgInPc4
z	z	k7c2
největších	veliký	k2eAgInPc2d3
kaskádových	kaskádový	k2eAgInPc2d1
vodopádů	vodopád	k1gInPc2
v	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
Karpat	Karpaty	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc1
hlavní	hlavní	k2eAgFnSc7d1
měří	měřit	k5eAaImIp3nS
společně	společně	k6eAd1
9	#num#	k4
m	m	kA
<g/>
,	,	kIx,
celková	celkový	k2eAgFnSc1d1
výška	výška	k1gFnSc1
asi	asi	k9
40	#num#	k4
m.	m.	k4
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
okolo	okolo	k7c2
800	#num#	k4
m	m	kA
a	a	k8xC
jsou	být	k5eAaImIp3nP
přístupné	přístupný	k2eAgInPc1d1
po	po	k7c6
lesní	lesní	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
mezi	mezi	k7c7
rozcestím	rozcestí	k1gNnSc7
zvaným	zvaný	k2eAgFnPc3d1
Nad	nad	k7c7
Holubčankou	Holubčanka	k1gFnSc7
a	a	k8xC
nedalekou	daleký	k2eNgFnSc7d1
chatovou	chatový	k2eAgFnSc7d1
osadou	osada	k1gFnSc7
Na	na	k7c6
Skalce	skalka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Bučací	Bučací	k2eAgInSc4d1
potok	potok	k1gInSc4
vyhlášené	vyhlášený	k2eAgFnSc2d1
20	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prudké	Prudké	k2eAgInPc4d1
okolní	okolní	k2eAgInPc4d1
svahy	svah	k1gInPc4
s	s	k7c7
podložím	podloží	k1gNnSc7
tvořeným	tvořený	k2eAgNnSc7d1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
podloží	podloží	k1gNnSc6
samotných	samotný	k2eAgInPc2d1
vodopádů	vodopád	k1gInPc2
tvrdými	tvrdý	k2eAgInPc7d1
godulskými	godulský	k2eAgInPc7d1
pískovci	pískovec	k1gInPc7
a	a	k8xC
méně	málo	k6eAd2
odolnými	odolný	k2eAgFnPc7d1
břidlicemi	břidlice	k1gFnPc7
pokrývá	pokrývat	k5eAaImIp3nS
především	především	k6eAd1
bukový	bukový	k2eAgInSc4d1
a	a	k8xC
javorový	javorový	k2eAgInSc4d1
lesní	lesní	k2eAgInSc4d1
porost	porost	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
převýšení	převýšení	k1gNnPc1
celé	celý	k2eAgFnSc2d1
kaskády	kaskáda	k1gFnSc2
podle	podle	k7c2
nature	natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
hyperlink	hyperlink	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
Nejkrásnější	krásný	k2eAgInPc1d3
vodopády	vodopád	k1gInPc1
udávají	udávat	k5eAaImIp3nP
9	#num#	k4
m	m	kA
jako	jako	k8xC,k8xS
součet	součet	k1gInSc1
horního	horní	k2eAgInSc2d1
a	a	k8xC
dolního	dolní	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Nejkrásnější	krásný	k2eAgInPc1d3
vodopády	vodopád	k1gInPc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
průvodce	průvodce	k1gMnSc1
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
278	#num#	k4
<g/>
,	,	kIx,
Vodopád	vodopád	k1gInSc1
na	na	k7c6
Bučacím	Bučací	k2eAgInSc6d1
potoce	potok	k1gInSc6
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-200-1779-6	978-80-200-1779-6	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Bučací	Bučací	k2eAgFnPc1d1
potok	potok	k1gInSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
