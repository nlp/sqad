<s desamb="1">
Dva	dva	k4xCgInPc1
hlavní	hlavní	k2eAgFnSc7d1
měří	měřit	k5eAaImIp3nS
společně	společně	k6eAd1
9	#num#	k4
m	m	kA
<g/>
,	,	kIx,
celková	celkový	k2eAgFnSc1d1
výška	výška	k1gFnSc1
asi	asi	k9
40	#num#	k4
m.	m.	k4
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
okolo	okolo	k7c2
800	#num#	k4
m	m	kA
a	a	k8xC
jsou	být	k5eAaImIp3nP
přístupné	přístupný	k2eAgInPc1d1
po	po	k7c6
lesní	lesní	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
mezi	mezi	k7c7
rozcestím	rozcestí	k1gNnSc7
zvaným	zvaný	k2eAgFnPc3d1
Nad	nad	k7c7
Holubčankou	Holubčanka	k1gFnSc7
a	a	k8xC
nedalekou	daleký	k2eNgFnSc7d1
chatovou	chatový	k2eAgFnSc7d1
osadou	osada	k1gFnSc7
Na	na	k7c6
Skalce	skalka	k1gFnSc6
<g/>
.	.	kIx.
</s>