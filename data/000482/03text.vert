<s>
Lipno	Lipno	k1gNnSc1	Lipno
<g/>
,	,	kIx,	,
Údolní	údolní	k2eAgFnSc1d1	údolní
nádrž	nádrž	k1gFnSc1	nádrž
Lipno	Lipno	k1gNnSc1	Lipno
<g/>
,	,	kIx,	,
Lipno	Lipno	k1gNnSc1	Lipno
I	i	k8xC	i
či	či	k8xC	či
lipenská	lipenský	k2eAgFnSc1d1	Lipenská
přehrada	přehrada	k1gFnSc1	přehrada
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgNnSc1d1	vodní
dílo	dílo	k1gNnSc1	dílo
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Vltavě	Vltava	k1gFnSc6	Vltava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
48,7	[number]	k4	48,7
km	km	kA	km
<g/>
2	[number]	k4	2
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
přehradní	přehradní	k2eAgFnSc4d1	přehradní
nádrž	nádrž	k1gFnSc4	nádrž
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
vodní	vodní	k2eAgFnSc4d1	vodní
plochu	plocha	k1gFnSc4	plocha
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
občas	občas	k6eAd1	občas
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
české	český	k2eAgNnSc4d1	české
či	či	k8xC	či
jihočeské	jihočeský	k2eAgNnSc4d1	Jihočeské
moře	moře	k1gNnSc4	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
vzdutí	vzdutí	k1gNnSc2	vzdutí
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
42	[number]	k4	42
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nejširší	široký	k2eAgFnSc1d3	nejširší
je	být	k5eAaImIp3nS	být
nádrž	nádrž	k1gFnSc1	nádrž
u	u	k7c2	u
Černé	Černá	k1gFnSc2	Černá
v	v	k7c6	v
Pošumaví	Pošumaví	k1gNnSc6	Pošumaví
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozlévá	rozlévat	k5eAaImIp3nS	rozlévat
až	až	k9	až
na	na	k7c4	na
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pravobřežní	pravobřežní	k2eAgFnSc6d1	pravobřežní
straně	strana	k1gFnSc6	strana
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
k	k	k7c3	k
státním	státní	k2eAgFnPc3d1	státní
hranicím	hranice	k1gFnPc3	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Sypaná	sypaný	k2eAgFnSc1d1	sypaná
hráz	hráz	k1gFnSc1	hráz
s	s	k7c7	s
těsnícím	těsnící	k2eAgNnSc7d1	těsnící
jádrem	jádro	k1gNnSc7	jádro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Lipno	Lipno	k1gNnSc1	Lipno
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vybavená	vybavený	k2eAgFnSc1d1	vybavená
ocelovými	ocelový	k2eAgInPc7d1	ocelový
kesony	keson	k1gInPc7	keson
a	a	k8xC	a
betonovými	betonový	k2eAgFnPc7d1	betonová
výpustěmi	výpusť	k1gFnPc7	výpusť
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
hráze	hráz	k1gFnSc2	hráz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
kótě	kóta	k1gFnSc6	kóta
729	[number]	k4	729
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Výška	výška	k1gFnSc1	výška
hráze	hráz	k1gFnSc2	hráz
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
m.	m.	k?	m.
Délka	délka	k1gFnSc1	délka
hráze	hráz	k1gFnSc2	hráz
je	být	k5eAaImIp3nS	být
296	[number]	k4	296
m.	m.	k?	m.
V	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vedle	vedle	k7c2	vedle
několika	několik	k4yIc2	několik
menších	malý	k2eAgInPc2d2	menší
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
větší	veliký	k2eAgInSc4d2	veliký
ostrov	ostrov	k1gInSc4	ostrov
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Tajvan	Tajvan	k1gMnSc1	Tajvan
<g/>
.	.	kIx.	.
</s>
<s>
Lipno	Lipno	k1gNnSc1	Lipno
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
lokalitou	lokalita	k1gFnSc7	lokalita
pro	pro	k7c4	pro
rekreační	rekreační	k2eAgInSc4d1	rekreační
pobyt	pobyt	k1gInSc4	pobyt
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
a	a	k8xC	a
sportovní	sportovní	k2eAgInSc4d1	sportovní
rybolov	rybolov	k1gInSc4	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Letní	letní	k2eAgFnSc2d1	letní
olympiády	olympiáda	k1gFnSc2	olympiáda
2016	[number]	k4	2016
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
nádrže	nádrž	k1gFnSc2	nádrž
nacházel	nacházet	k5eAaImAgInS	nacházet
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
Olympijský	olympijský	k2eAgInSc1d1	olympijský
park	park	k1gInSc1	park
Rio-Lipno	Rio-Lipno	k6eAd1	Rio-Lipno
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zásobárnu	zásobárna	k1gFnSc4	zásobárna
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
zdroj	zdroj	k1gInSc1	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
součástí	součást	k1gFnSc7	součást
přehrady	přehrada	k1gFnSc2	přehrada
je	být	k5eAaImIp3nS	být
též	též	k9	též
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Lipno	Lipno	k1gNnSc4	Lipno
I	I	kA	I
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
udržovaný	udržovaný	k2eAgInSc1d1	udržovaný
retenční	retenční	k2eAgInSc1d1	retenční
prostor	prostor	k1gInSc1	prostor
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Lipno	Lipno	k1gNnSc1	Lipno
(	(	kIx(	(
<g/>
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
rezerva	rezerva	k1gFnSc1	rezerva
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
povodní	povodeň	k1gFnPc2	povodeň
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
objem	objem	k1gInSc4	objem
asi	asi	k9	asi
12	[number]	k4	12
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vypuštěním	vypuštění	k1gNnSc7	vypuštění
zásobního	zásobní	k2eAgInSc2d1	zásobní
prostoru	prostor	k1gInSc2	prostor
nádrže	nádrž	k1gFnSc2	nádrž
lze	lze	k6eAd1	lze
zvýšit	zvýšit	k5eAaPmF	zvýšit
retenční	retenční	k2eAgFnSc4d1	retenční
kapacitu	kapacita	k1gFnSc4	kapacita
nádrže	nádrž	k1gFnSc2	nádrž
na	na	k7c4	na
maximálně	maximálně	k6eAd1	maximálně
186	[number]	k4	186
148	[number]	k4	148
000	[number]	k4	000
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
nedělá	dělat	k5eNaImIp3nS	dělat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
úlohami	úloha	k1gFnPc7	úloha
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zničujících	zničující	k2eAgFnPc6d1	zničující
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
Lipno	Lipno	k1gNnSc1	Lipno
udržovalo	udržovat	k5eAaImAgNnS	udržovat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
průtok	průtok	k1gInSc4	průtok
pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
do	do	k7c2	do
70	[number]	k4	70
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přišla	přijít	k5eAaPmAgFnS	přijít
nepříznivá	příznivý	k2eNgFnSc1d1	nepříznivá
předpověď	předpověď	k1gFnSc1	předpověď
pro	pro	k7c4	pro
následující	následující	k2eAgInPc4d1	následující
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
odtok	odtok	k1gInSc1	odtok
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
80	[number]	k4	80
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
bylo	být	k5eAaImAgNnS	být
nuceno	nucen	k2eAgNnSc1d1	nuceno
zvýšit	zvýšit	k5eAaPmF	zvýšit
odtok	odtok	k1gInSc4	odtok
na	na	k7c4	na
190	[number]	k4	190
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prodloužilo	prodloužit	k5eAaPmAgNnS	prodloužit
dobu	doba	k1gFnSc4	doba
do	do	k7c2	do
dosažení	dosažení	k1gNnSc2	dosažení
maximální	maximální	k2eAgFnSc2d1	maximální
retence	retence	k1gFnSc2	retence
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
tím	ten	k3xDgNnSc7	ten
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
možnosti	možnost	k1gFnSc2	možnost
setkání	setkání	k1gNnSc2	setkání
dvou	dva	k4xCgFnPc2	dva
povodňových	povodňový	k2eAgFnPc2d1	povodňová
vln	vlna	k1gFnPc2	vlna
pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
už	už	k9	už
odtok	odtok	k1gInSc4	odtok
z	z	k7c2	z
nádrže	nádrž	k1gFnSc2	nádrž
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
320	[number]	k4	320
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
přítok	přítok	k1gInSc1	přítok
byl	být	k5eAaImAgInS	být
470	[number]	k4	470
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
průtok	průtok	k1gInSc1	průtok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nepůsobí	působit	k5eNaImIp3nS	působit
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
90	[number]	k4	90
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
při	při	k7c6	při
jarních	jarní	k2eAgFnPc6d1	jarní
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
očekávány	očekávat	k5eAaImNgInP	očekávat
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
Lipno	Lipno	k1gNnSc1	Lipno
připravenu	připraven	k2eAgFnSc4d1	připravena
rezervu	rezerva	k1gFnSc4	rezerva
asi	asi	k9	asi
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
m	m	kA	m
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
povodeň	povodeň	k1gFnSc1	povodeň
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
pojalo	pojmout	k5eAaPmAgNnS	pojmout
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Lipenské	lipenský	k2eAgFnSc2d1	Lipenská
přehrady	přehrada	k1gFnSc2	přehrada
leží	ležet	k5eAaImIp3nP	ležet
uvnitř	uvnitř	k7c2	uvnitř
CHKO	CHKO	kA	CHKO
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Lipno	Lipno	k1gNnSc1	Lipno
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Historické	historický	k2eAgFnSc2d1	historická
fotografie	fotografia	k1gFnSc2	fotografia
z	z	k7c2	z
výstavby	výstavba	k1gFnSc2	výstavba
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
a	a	k8xC	a
podzemní	podzemní	k2eAgFnPc1d1	podzemní
hydrocentrály	hydrocentrála	k1gFnPc1	hydrocentrála
Zatopené	zatopený	k2eAgInPc4d1	zatopený
osudy	osud	k1gInPc4	osud
-	-	kIx~	-
Lipno	Lipno	k1gNnSc1	Lipno
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
online	onlinout	k5eAaPmIp3nS	onlinout
přehrání	přehrání	k1gNnSc3	přehrání
Zátopová	zátopový	k2eAgFnSc1d1	zátopová
oblast	oblast	k1gFnSc1	oblast
Lipna	Lipno	k1gNnSc2	Lipno
na	na	k7c6	na
webu	web	k1gInSc6	web
o	o	k7c6	o
státních	státní	k2eAgFnPc6d1	státní
hranicích	hranice	k1gFnPc6	hranice
</s>
