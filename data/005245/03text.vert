<s>
Ditch	Ditch	k1gInSc1	Ditch
Witch	Witcha	k1gFnPc2	Witcha
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
značka	značka	k1gFnSc1	značka
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
zemních	zemní	k2eAgInPc2d1	zemní
strojů	stroj	k1gInPc2	stroj
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
firmou	firma	k1gFnSc7	firma
Charles	Charles	k1gMnSc1	Charles
Machine	Machin	k1gInSc5	Machin
Works	Works	kA	Works
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
současným	současný	k2eAgInSc7d1	současný
názvem	název	k1gInSc7	název
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c4	v
Perry	Perr	k1gInPc4	Perr
v	v	k7c6	v
Oklahomě	Oklahomě	k1gFnSc6	Oklahomě
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
začala	začít	k5eAaPmAgFnS	začít
existovat	existovat	k5eAaImF	existovat
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
kompaktní	kompaktní	k2eAgInSc1d1	kompaktní
hloubící	hloubící	k2eAgInSc1d1	hloubící
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ruční	ruční	k2eAgFnPc4d1	ruční
práce	práce	k1gFnPc4	práce
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
zemních	zemní	k2eAgFnPc2d1	zemní
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Ditch	Ditch	k1gMnSc1	Ditch
Witch	Witch	k1gMnSc1	Witch
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
navrhování	navrhování	k1gNnSc4	navrhování
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
vysoce	vysoce	k6eAd1	vysoce
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
zemních	zemní	k2eAgInPc2d1	zemní
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
výrobcem	výrobce	k1gMnSc7	výrobce
strojů	stroj	k1gInPc2	stroj
několika	několik	k4yIc2	několik
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
rýhovače	rýhovač	k1gInPc1	rýhovač
<g/>
,	,	kIx,	,
vibrační	vibrační	k2eAgInPc1d1	vibrační
pluhy	pluh	k1gInPc1	pluh
<g/>
,	,	kIx,	,
elektronické	elektronický	k2eAgInPc1d1	elektronický
lokátory	lokátor	k1gInPc1	lokátor
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
systémy	systém	k1gInPc1	systém
pro	pro	k7c4	pro
horizontální	horizontální	k2eAgNnSc4d1	horizontální
řízené	řízený	k2eAgNnSc4d1	řízené
vrtání	vrtání	k1gNnSc4	vrtání
<g/>
,	,	kIx,	,
vrtací	vrtací	k2eAgFnPc4d1	vrtací
trubky	trubka	k1gFnPc4	trubka
<g/>
,	,	kIx,	,
vrtací	vrtací	k2eAgNnPc4d1	vrtací
nářadí	nářadí	k1gNnPc4	nářadí
a	a	k8xC	a
hlavy	hlava	k1gFnPc4	hlava
<g/>
,	,	kIx,	,
řetězy	řetěz	k1gInPc4	řetěz
<g/>
,	,	kIx,	,
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
pastorky	pastorek	k1gInPc4	pastorek
<g/>
,	,	kIx,	,
sací	sací	k2eAgInPc4d1	sací
bagry	bagr	k1gInPc4	bagr
<g/>
,	,	kIx,	,
rypadlové	rypadlový	k2eAgInPc4d1	rypadlový
nosiče	nosič	k1gInPc4	nosič
nářadí	nářadí	k1gNnSc2	nářadí
a	a	k8xC	a
kompaktní	kompaktní	k2eAgInPc4d1	kompaktní
nosiče	nosič	k1gInPc4	nosič
nářadí	nářadí	k1gNnSc2	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
se	se	k3xPyFc4	se
německý	německý	k2eAgMnSc1d1	německý
přistěhovalec	přistěhovalec	k1gMnSc1	přistěhovalec
Carl	Carl	k1gMnSc1	Carl
Frederick	Frederick	k1gMnSc1	Frederick
Malzahn	Malzahn	k1gMnSc1	Malzahn
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
z	z	k7c2	z
Minnesoty	Minnesota	k1gFnSc2	Minnesota
do	do	k7c2	do
Perry	Perra	k1gFnSc2	Perra
v	v	k7c6	v
Oklahomě	Oklahomě	k1gFnSc6	Oklahomě
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
syny	syn	k1gMnPc7	syn
<g/>
,	,	kIx,	,
Charliem	Charlie	k1gMnSc7	Charlie
a	a	k8xC	a
Gusem	Gus	k1gMnSc7	Gus
<g/>
,	,	kIx,	,
otevřel	otevřít	k5eAaPmAgMnS	otevřít
kovárnu	kovárna	k1gFnSc4	kovárna
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
prosperoval	prosperovat	k5eAaImAgInS	prosperovat
a	a	k8xC	a
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
ropného	ropný	k2eAgInSc2d1	ropný
boomu	boom	k1gInSc2	boom
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
dílna	dílna	k1gFnSc1	dílna
začala	začít	k5eAaPmAgFnS	začít
specializovat	specializovat	k5eAaBmF	specializovat
na	na	k7c4	na
opravy	oprava	k1gFnPc4	oprava
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
ropná	ropný	k2eAgNnPc4d1	ropné
pole	pole	k1gNnPc4	pole
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Charlieho	Charlie	k1gMnSc2	Charlie
syn	syn	k1gMnSc1	syn
Ed	Ed	k1gMnSc1	Ed
Malzahn	Malzahn	k1gMnSc1	Malzahn
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
podnikání	podnikání	k1gNnSc4	podnikání
měnící	měnící	k2eAgFnSc3d1	měnící
se	se	k3xPyFc4	se
poptávce	poptávka	k1gFnSc3	poptávka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c6	na
konci	konec	k1gInSc6	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
začal	začít	k5eAaPmAgInS	začít
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
své	svůj	k3xOyFgFnPc4	svůj
strojírenské	strojírenský	k2eAgFnPc4d1	strojírenská
ambice	ambice	k1gFnPc4	ambice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
procesu	proces	k1gInSc2	proces
tehdy	tehdy	k6eAd1	tehdy
velmi	velmi	k6eAd1	velmi
zdlouhavé	zdlouhavý	k2eAgFnSc2d1	zdlouhavá
instalace	instalace	k1gFnSc2	instalace
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
telefonních	telefonní	k2eAgFnPc2d1	telefonní
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Charlie	Charlie	k1gMnSc1	Charlie
společnou	společný	k2eAgFnSc7d1	společná
prací	práce	k1gFnSc7	práce
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
dílně	dílna	k1gFnSc6	dílna
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
prototyp	prototyp	k1gInSc1	prototyp
prvního	první	k4xOgInSc2	první
rýhovače	rýhovač	k1gInSc2	rýhovač
<g/>
.	.	kIx.	.
</s>
<s>
Nazvali	nazvat	k5eAaPmAgMnP	nazvat
ho	on	k3xPp3gMnSc4	on
DWP	DWP	kA	DWP
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Ditch	Ditch	k1gMnSc1	Ditch
Witch	Witch	k1gMnSc1	Witch
Power	Power	k1gMnSc1	Power
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
komerční	komerční	k2eAgInSc1d1	komerční
DWP	DWP	kA	DWP
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
mechanizovaný	mechanizovaný	k2eAgInSc1d1	mechanizovaný
kompaktní	kompaktní	k2eAgInSc1d1	kompaktní
rýhovač	rýhovač	k1gInSc1	rýhovač
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
pro	pro	k7c4	pro
pokládku	pokládka	k1gFnSc4	pokládka
domovních	domovní	k2eAgFnPc2d1	domovní
přípojek	přípojka	k1gFnPc2	přípojka
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
<g/>
,	,	kIx,	,
že	že	k8xS	že
DWP	DWP	kA	DWP
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
letitý	letitý	k2eAgInSc1d1	letitý
problém	problém	k1gInSc1	problém
pro	pro	k7c4	pro
dodavatele	dodavatel	k1gMnPc4	dodavatel
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
připravil	připravit	k5eAaPmAgMnS	připravit
cestu	cesta	k1gFnSc4	cesta
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
kompaktního	kompaktní	k2eAgInSc2d1	kompaktní
rýhovače	rýhovač	k1gInSc2	rýhovač
a	a	k8xC	a
efektivnějšího	efektivní	k2eAgInSc2d2	efektivnější
způsobu	způsob	k1gInSc2	způsob
instalace	instalace	k1gFnSc2	instalace
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
popularitou	popularita	k1gFnSc7	popularita
Malzahnova	Malzahnův	k2eAgInSc2d1	Malzahnův
rýhovače	rýhovač	k1gInSc2	rýhovač
se	se	k3xPyFc4	se
Charlieho	Charlie	k1gMnSc2	Charlie
dílna	dílna	k1gFnSc1	dílna
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
v	v	k7c6	v
Charles	Charles	k1gMnSc1	Charles
Machine	Machin	k1gInSc5	Machin
Works	Works	kA	Works
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stále	stále	k6eAd1	stále
udržuje	udržovat	k5eAaImIp3nS	udržovat
své	svůj	k3xOyFgFnPc4	svůj
sídlo	sídlo	k1gNnSc1	sídlo
v	v	k7c4	v
Perry	Perra	k1gFnPc4	Perra
v	v	k7c6	v
Oklahomě	Oklahomě	k1gFnSc6	Oklahomě
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
rýhovačů	rýhovač	k1gInPc2	rýhovač
dnes	dnes	k6eAd1	dnes
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
zemních	zemní	k2eAgNnPc2d1	zemní
stavebních	stavební	k2eAgNnPc2d1	stavební
zařízení	zařízení	k1gNnPc2	zařízení
nesoucích	nesoucí	k2eAgNnPc2d1	nesoucí
název	název	k1gInSc1	název
Ditch	Ditch	k1gMnSc1	Ditch
Witch	Witch	k1gMnSc1	Witch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
generální	generální	k2eAgFnSc7d1	generální
ředitelkou	ředitelka	k1gFnSc7	ředitelka
společnosti	společnost	k1gFnSc2	společnost
Charles	Charles	k1gMnSc1	Charles
Machine	Machin	k1gInSc5	Machin
Works	Works	kA	Works
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Tiffany	Tiffana	k1gFnPc1	Tiffana
Sewell-Howard	Sewell-Howarda	k1gFnPc2	Sewell-Howarda
<g/>
,	,	kIx,	,
vnučka	vnučka	k1gFnSc1	vnučka
Eda	Eda	k1gMnSc1	Eda
Malzahna	Malzahn	k1gInSc2	Malzahn
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1	zakladatel
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
Ed	Ed	k1gMnSc1	Ed
Malzahn	Malzahn	k1gMnSc1	Malzahn
však	však	k9	však
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
osmdesáti	osmdesát	k4xCc6	osmdesát
letech	léto	k1gNnPc6	léto
stále	stále	k6eAd1	stále
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
prezident	prezident	k1gMnSc1	prezident
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc4	sídlo
organizace	organizace	k1gFnSc2	organizace
Ditch	Ditch	k1gMnSc1	Ditch
Witch	Witch	k1gMnSc1	Witch
v	v	k7c4	v
Perry	Perr	k1gMnPc4	Perr
je	být	k5eAaImIp3nS	být
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
areálem	areál	k1gInSc7	areál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
30	[number]	k4	30
akrů	akr	k1gInPc2	akr
(	(	kIx(	(
<g/>
120	[number]	k4	120
000	[number]	k4	000
m2	m2	k4	m2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
výrobní	výrobní	k2eAgInSc1d1	výrobní
závod	závod	k1gInSc1	závod
<g/>
,	,	kIx,	,
školící	školící	k2eAgNnSc1d1	školící
a	a	k8xC	a
testovací	testovací	k2eAgNnSc1d1	testovací
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
centrum	centrum	k1gNnSc1	centrum
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
sppolečnost	sppolečnost	k1gFnSc1	sppolečnost
Ditch	Ditcha	k1gFnPc2	Ditcha
Witch	Witcha	k1gFnPc2	Witcha
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
300	[number]	k4	300
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Rýhovač	rýhovač	k1gInSc1	rýhovač
Ditch	Ditch	k1gInSc1	Ditch
Witch	Witch	k1gInSc1	Witch
byl	být	k5eAaImAgInS	být
časopisem	časopis	k1gInSc7	časopis
Fortune	Fortun	k1gInSc5	Fortun
dvakrát	dvakrát	k6eAd1	dvakrát
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
"	"	kIx"	"
<g/>
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
100	[number]	k4	100
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
amerických	americký	k2eAgInPc2d1	americký
produktů	produkt	k1gInPc2	produkt
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
rýhovač	rýhovač	k1gInSc1	rýhovač
DWP	DWP	kA	DWP
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
historickou	historický	k2eAgFnSc4d1	historická
památku	památka	k1gFnSc4	památka
strojírenství	strojírenství	k1gNnSc2	strojírenství
v	v	k7c6	v
Americké	americký	k2eAgFnSc6d1	americká
společnosti	společnost	k1gFnSc6	společnost
strojních	strojní	k2eAgMnPc2d1	strojní
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ditch	Ditch	k1gMnSc1	Ditch
Witch	Witch	k1gMnSc1	Witch
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ditch	Ditcha	k1gFnPc2	Ditcha
Witch	Witcha	k1gFnPc2	Witcha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Anglické	anglický	k2eAgFnSc2d1	anglická
stránky	stránka	k1gFnSc2	stránka
společnosti	společnost	k1gFnSc2	společnost
</s>
