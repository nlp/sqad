<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Kavalčík	Kavalčík	k1gMnSc1	Kavalčík
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1975	[number]	k4	1975
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
)	)	kIx)	)
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgInSc1d1	známý
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Ruda	rudo	k1gNnSc2	rudo
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Ostravskou	ostravský	k2eAgFnSc4d1	Ostravská
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
<g/>
,	,	kIx,	,
obor	obor	k1gInSc4	obor
hudebně-dramatický	hudebněramatický	k2eAgInSc4d1	hudebně-dramatický
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
muzikálech	muzikál	k1gInPc6	muzikál
Dracula	Dracula	k1gFnSc1	Dracula
<g/>
,	,	kIx,	,
Evita	Evita	k1gMnSc1	Evita
<g/>
,	,	kIx,	,
Hamlet	Hamlet	k1gMnSc1	Hamlet
a	a	k8xC	a
Krysař	krysař	k1gMnSc1	krysař
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
muzikálech	muzikál	k1gInPc6	muzikál
Cikáni	cikán	k1gMnPc1	cikán
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
(	(	kIx(	(
<g/>
titulní	titulní	k2eAgFnSc1d1	titulní
role	role	k1gFnSc1	role
Lojko	Lojko	k1gNnSc1	Lojko
Zobara	Zobar	k1gMnSc2	Zobar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
let	léto	k1gNnPc2	léto
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
moderátor	moderátor	k1gInSc4	moderátor
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
Radio	radio	k1gNnSc4	radio
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
účastí	účastí	k1gNnSc2	účastí
v	v	k7c6	v
pořadech	pořad	k1gInPc6	pořad
Na	na	k7c4	na
stojáka	stoják	k1gMnSc4	stoják
a	a	k8xC	a
X	X	kA	X
Factor	Factor	k1gInSc1	Factor
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Ruda	rudo	k1gNnSc2	rudo
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Noisebrothers	Noisebrothersa	k1gFnPc2	Noisebrothersa
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
hudební	hudební	k2eAgInSc1d1	hudební
klip	klip	k1gInSc1	klip
"	"	kIx"	"
<g/>
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
"	"	kIx"	"
–	–	k?	–
parodie	parodie	k1gFnSc2	parodie
na	na	k7c4	na
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Falling	Falling	k1gInSc4	Falling
Slowly	Slowla	k1gFnSc2	Slowla
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
Martinou	Martina	k1gFnSc7	Martina
Pártlovou	Pártlův	k2eAgFnSc7d1	Pártlův
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Janka	Janek	k1gMnSc2	Janek
Rubeše	Rubeš	k1gMnSc2	Rubeš
a	a	k8xC	a
United	United	k1gInSc4	United
labels	labelsa	k1gFnPc2	labelsa
<g/>
,	,	kIx,	,
SE	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zase	zase	k9	zase
klip	klip	k1gInSc1	klip
"	"	kIx"	"
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
paroduje	parodovat	k5eAaImIp3nS	parodovat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
of	of	k?	of
Mind	Minda	k1gFnPc2	Minda
<g/>
"	"	kIx"	"
od	od	k7c2	od
Jay-Zho	Jay-Z	k1gMnSc2	Jay-Z
a	a	k8xC	a
Aliciy	Alicia	k1gFnSc2	Alicia
Keys	Keysa	k1gFnPc2	Keysa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Letních	letní	k2eAgFnPc2d1	letní
Shakespearovských	shakespearovský	k2eAgFnPc2d1	shakespearovská
slavností	slavnost	k1gFnPc2	slavnost
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
jako	jako	k8xC	jako
Kotrba	kotrba	k1gFnSc1	kotrba
<g/>
,	,	kIx,	,
mladík	mladík	k1gMnSc1	mladík
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Marná	marný	k2eAgFnSc1d1	marná
lásky	láska	k1gFnSc2	láska
snaha	snaha	k1gFnSc1	snaha
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Vtip	vtip	k1gInSc4	vtip
za	za	k7c4	za
stovku	stovka	k1gFnSc4	stovka
televize	televize	k1gFnSc2	televize
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
pak	pak	k6eAd1	pak
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Týden	týden	k1gInSc1	týden
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zazpíval	zazpívat	k5eAaPmAgInS	zazpívat
na	na	k7c6	na
Frekvenci	frekvence	k1gFnSc6	frekvence
1	[number]	k4	1
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
vítězství	vítězství	k1gNnSc2	vítězství
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
na	na	k7c6	na
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
píseň	píseň	k1gFnSc4	píseň
Každý	každý	k3xTgMnSc1	každý
ti	ten	k3xDgMnPc1	ten
to	ten	k3xDgNnSc1	ten
Petro	Petra	k1gFnSc5	Petra
závidí	závidět	k5eAaImIp3nS	závidět
(	(	kIx(	(
<g/>
předělávka	předělávka	k1gFnSc1	předělávka
písně	píseň	k1gFnSc2	píseň
Každý	každý	k3xTgMnSc1	každý
mi	já	k3xPp1nSc3	já
tě	ty	k3xPp2nSc4	ty
lásko	láska	k1gFnSc5	láska
závidí	závidět	k5eAaImIp3nS	závidět
od	od	k7c2	od
Michala	Michal	k1gMnSc2	Michal
Davida	David	k1gMnSc2	David
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
s	s	k7c7	s
Yettym	Yettymum	k1gNnPc2	Yettymum
píseň	píseň	k1gFnSc1	píseň
Cysta	cysta	k1gFnSc1	cysta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
paroduje	parodovat	k5eAaImIp3nS	parodovat
píseň	píseň	k1gFnSc1	píseň
Cesta	cesta	k1gFnSc1	cesta
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Kryštof	Kryštof	k1gMnSc1	Kryštof
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Klusem	klus	k1gInSc7	klus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Jeho	jeho	k3xOp3gFnSc7	jeho
partnerkou	partnerka	k1gFnSc7	partnerka
byla	být	k5eAaImAgFnS	být
Veronika	Veronika	k1gFnSc1	Veronika
Arichteva	Arichteva	k1gFnSc1	Arichteva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Michal	Michal	k1gMnSc1	Michal
Kavalčík	Kavalčík	k1gInSc4	Kavalčík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Kavalčík	Kavalčík	k1gMnSc1	Kavalčík
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
