<s>
Houby	houba	k1gFnPc1	houba
(	(	kIx(	(
<g/>
Fungi	Fung	k1gFnPc1	Fung
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Mycophyta	Mycophyta	k1gFnSc1	Mycophyta
<g/>
)	)	kIx)	)
představují	představovat	k5eAaImIp3nP	představovat
velkou	velký	k2eAgFnSc4d1	velká
skupinu	skupina	k1gFnSc4	skupina
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
dříve	dříve	k6eAd2	dříve
řazenou	řazený	k2eAgFnSc7d1	řazená
k	k	k7c3	k
rostlinám	rostlina	k1gFnPc3	rostlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
vyčleněnou	vyčleněný	k2eAgFnSc4d1	vyčleněná
jako	jako	k8xS	jako
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
zástupce	zástupce	k1gMnSc1	zástupce
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
významní	významný	k2eAgMnPc1d1	významný
rozkladači	rozkladač	k1gMnPc1	rozkladač
<g/>
,	,	kIx,	,
parazité	parazit	k1gMnPc1	parazit
či	či	k8xC	či
v	v	k7c6	v
průmyslu	průmysl	k1gInSc2	průmysl
i	i	k8xC	i
potravinářství	potravinářství	k1gNnSc4	potravinářství
využívané	využívaný	k2eAgInPc4d1	využívaný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
mutualisty	mutualista	k1gMnPc4	mutualista
žijící	žijící	k2eAgMnPc4d1	žijící
v	v	k7c6	v
symbióze	symbióza	k1gFnSc6	symbióza
s	s	k7c7	s
cévnatými	cévnatý	k2eAgFnPc7d1	cévnatá
rostlinami	rostlina	k1gFnPc7	rostlina
nebo	nebo	k8xC	nebo
s	s	k7c7	s
řasami	řasa	k1gFnPc7	řasa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
kolem	kolem	k7c2	kolem
70	[number]	k4	70
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgNnSc6d2	užší
pojetí	pojetí	k1gNnSc6	pojetí
jsou	být	k5eAaImIp3nP	být
houby	houba	k1gFnPc4	houba
(	(	kIx(	(
<g/>
Fungi	Fung	k1gFnPc4	Fung
<g/>
)	)	kIx)	)
stélkaté	stélkatý	k2eAgInPc4d1	stélkatý
organismy	organismus	k1gInPc4	organismus
různého	různý	k2eAgInSc2d1	různý
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
velikostí	velikost	k1gFnPc2	velikost
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
asimilačních	asimilační	k2eAgNnPc2d1	asimilační
barviv	barvivo	k1gNnPc2	barvivo
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
bez	bez	k7c2	bez
plastidů	plastid	k1gInPc2	plastid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
heterotrofní	heterotrofní	k2eAgFnSc7d1	heterotrofní
výživou	výživa	k1gFnSc7	výživa
<g/>
,	,	kIx,	,
s	s	k7c7	s
buněčnou	buněčný	k2eAgFnSc7d1	buněčná
stěnou	stěna	k1gFnSc7	stěna
chitinózní	chitinózní	k2eAgFnSc7d1	chitinózní
<g/>
.	.	kIx.	.
</s>
<s>
Zásobní	zásobní	k2eAgFnSc7d1	zásobní
látkou	látka	k1gFnSc7	látka
je	být	k5eAaImIp3nS	být
glykogen	glykogen	k1gInSc1	glykogen
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houby	k6eAd1	houby
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
buď	buď	k8xC	buď
vegetativně	vegetativně	k6eAd1	vegetativně
(	(	kIx(	(
<g/>
rozpadem	rozpad	k1gInSc7	rozpad
vlákna	vlákno	k1gNnSc2	vlákno
mycelia	mycelium	k1gNnSc2	mycelium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nepohlavními	pohlavní	k2eNgInPc7d1	nepohlavní
nebo	nebo	k8xC	nebo
pohlavními	pohlavní	k2eAgInPc7d1	pohlavní
výtrusy	výtrus	k1gInPc7	výtrus
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
houbami	houba	k1gFnPc7	houba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mykologie	mykologie	k1gFnSc1	mykologie
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houby	k6eAd1	houby
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
vodních	vodní	k2eAgInPc2d1	vodní
organizmů	organizmus	k1gInPc2	organizmus
s	s	k7c7	s
bičíkatými	bičíkatý	k2eAgFnPc7d1	bičíkatá
sporami	spora	k1gFnPc7	spora
<g/>
;	;	kIx,	;
předci	předek	k1gMnPc1	předek
hub	houba	k1gFnPc2	houba
tedy	tedy	k9	tedy
mohli	moct	k5eAaImAgMnP	moct
vypadat	vypadat	k5eAaPmF	vypadat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
chytridiomycety	chytridiomyceta	k1gFnSc2	chytridiomyceta
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
bičíku	bičík	k1gInSc2	bičík
nastala	nastat	k5eAaPmAgFnS	nastat
buď	buď	k8xC	buď
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
hub	houba	k1gFnPc2	houba
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
pozdních	pozdní	k2eAgFnPc6d1	pozdní
starohorách	starohory	k1gFnPc6	starohory
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
900	[number]	k4	900
<g/>
-	-	kIx~	-
<g/>
570	[number]	k4	570
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvohorách	prvohory	k1gFnPc6	prvohory
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
a	a	k8xC	a
v	v	k7c6	v
pennsylvanu	pennsylvan	k1gMnSc6	pennsylvan
(	(	kIx(	(
<g/>
karbon	karbon	k1gInSc4	karbon
<g/>
,	,	kIx,	,
před	před	k7c7	před
320-286	[number]	k4	320-286
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
fosilních	fosilní	k2eAgInPc2d1	fosilní
nálezů	nález	k1gInPc2	nález
již	již	k6eAd1	již
vyskytovaly	vyskytovat	k5eAaImAgFnP	vyskytovat
všechny	všechen	k3xTgFnPc1	všechen
hlavní	hlavní	k2eAgFnPc1d1	hlavní
skupiny	skupina	k1gFnPc1	skupina
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
ze	z	k7c2	z
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houby	k6eAd1	houby
jsou	být	k5eAaImIp3nP	být
jednobuněčné	jednobuněčný	k2eAgInPc1d1	jednobuněčný
i	i	k8xC	i
mnohobuněčné	mnohobuněčný	k2eAgInPc1d1	mnohobuněčný
organismy	organismus	k1gInPc1	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
buněčných	buněčný	k2eAgFnPc2d1	buněčná
organel	organela	k1gFnPc2	organela
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
přítomny	přítomen	k2eAgInPc1d1	přítomen
chloroplasty	chloroplast	k1gInPc1	chloroplast
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc4d1	schopna
vytvářet	vytvářet	k5eAaImF	vytvářet
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
stavební	stavební	k2eAgFnSc7d1	stavební
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
houbové	houbový	k2eAgNnSc1d1	houbové
vlákno	vlákno	k1gNnSc1	vlákno
(	(	kIx(	(
<g/>
hyfa	hyfa	k1gFnSc1	hyfa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rozlišit	rozlišit	k5eAaPmF	rozlišit
v	v	k7c6	v
podhoubí	podhoubí	k1gNnSc6	podhoubí
(	(	kIx(	(
<g/>
mycelium	mycelium	k1gNnSc1	mycelium
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
plodnici	plodnice	k1gFnSc6	plodnice
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
nepravá	pravý	k2eNgNnPc4d1	nepravé
pletiva	pletivo	k1gNnPc4	pletivo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
plektenchym	plektenchym	k1gInSc4	plektenchym
a	a	k8xC	a
pseudoparenchym	pseudoparenchym	k1gInSc4	pseudoparenchym
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Hyfa	hyfa	k1gFnSc1	hyfa
a	a	k8xC	a
Podhoubí	podhoubí	k1gNnSc1	podhoubí
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
hub	houba	k1gFnPc2	houba
není	být	k5eNaImIp3nS	být
členěno	členit	k5eAaImNgNnS	členit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
orgány	orgán	k1gInPc4	orgán
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
stélka	stélka	k1gFnSc1	stélka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
propletených	propletený	k2eAgNnPc2d1	propletené
houbových	houbový	k2eAgNnPc2d1	houbové
vláken	vlákno	k1gNnPc2	vlákno
(	(	kIx(	(
<g/>
hyfa	hyfa	k1gFnSc1	hyfa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
podhoubí	podhoubí	k1gNnSc4	podhoubí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgFnPc2d1	mnohá
hub	houba	k1gFnPc2	houba
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
podhoubí	podhoubí	k1gNnSc2	podhoubí
plodnice	plodnice	k1gFnSc2	plodnice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Plodnice	plodnice	k1gFnSc2	plodnice
<g/>
.	.	kIx.	.
</s>
<s>
Plodnice	plodnice	k1gFnSc1	plodnice
je	být	k5eAaImIp3nS	být
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
"	"	kIx"	"
<g/>
orgán	orgán	k1gInSc1	orgán
<g/>
"	"	kIx"	"
houby	houba	k1gFnPc1	houba
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
totiž	totiž	k9	totiž
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
výtrusy	výtrus	k1gInPc7	výtrus
<g/>
.	.	kIx.	.
</s>
<s>
Rouško	rouško	k1gNnSc1	rouško
je	být	k5eAaImIp3nS	být
výtrusorodá	výtrusorodý	k2eAgFnSc1d1	výtrusorodá
vrstva	vrstva	k1gFnSc1	vrstva
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
kyjovitých	kyjovitý	k2eAgFnPc2d1	kyjovitá
výtrusnic	výtrusnice	k1gFnPc2	výtrusnice
s	s	k7c7	s
výtrusy	výtrus	k1gInPc7	výtrus
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
ploše	plocha	k1gFnSc6	plocha
klobouku	klobouk	k1gInSc2	klobouk
na	na	k7c6	na
lupenech	lupen	k1gInPc6	lupen
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rourkách	rourka	k1gFnPc6	rourka
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houba	k1gFnPc4	houba
s	s	k7c7	s
lupeny	lupen	k1gInPc7	lupen
naspodu	naspodu	k7c2	naspodu
klobouku	klobouk	k1gInSc2	klobouk
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
lupenaté	lupenatý	k2eAgInPc1d1	lupenatý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bedla	bedla	k1gFnSc1	bedla
<g/>
,	,	kIx,	,
muchomůrka	muchomůrka	k?	muchomůrka
<g/>
,	,	kIx,	,
pečárka	pečárka	k1gFnSc1	pečárka
<g/>
,	,	kIx,	,
ryzec	ryzec	k1gInSc1	ryzec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mívají	mívat	k5eAaImIp3nP	mívat
i	i	k9	i
pochvu	pochva	k1gFnSc4	pochva
a	a	k8xC	a
plachetku	plachetka	k1gFnSc4	plachetka
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houba	k1gFnPc4	houba
s	s	k7c7	s
rourkami	rourka	k1gFnPc7	rourka
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
rourkaté	rourkatý	k2eAgInPc1d1	rourkatý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hřiby	hřib	k1gInPc1	hřib
<g/>
,	,	kIx,	,
křemenáč	křemenáč	k1gInSc1	křemenáč
<g/>
,	,	kIx,	,
kozák	kozák	k1gInSc1	kozák
<g/>
,	,	kIx,	,
klouzek	klouzek	k1gInSc1	klouzek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
budeme	být	k5eAaImBp1nP	být
houby	houby	k6eAd1	houby
dělit	dělit	k5eAaImF	dělit
dle	dle	k7c2	dle
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
získávají	získávat	k5eAaImIp3nP	získávat
živiny	živina	k1gFnPc1	živina
<g/>
,	,	kIx,	,
dostáváme	dostávat	k5eAaImIp1nP	dostávat
dvě	dva	k4xCgFnPc1	dva
základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
hub	houba	k1gFnPc2	houba
-	-	kIx~	-
saprofytické	saprofytický	k2eAgFnSc2d1	saprofytická
(	(	kIx(	(
<g/>
hniložijné	hniložijný	k2eAgFnSc2d1	hniložijný
<g/>
)	)	kIx)	)
a	a	k8xC	a
parazitické	parazitický	k2eAgFnPc1d1	parazitická
(	(	kIx(	(
<g/>
příživné	příživný	k2eAgFnPc1d1	příživná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Saprofytické	saprofytický	k2eAgFnPc1d1	saprofytická
houby	houba	k1gFnPc1	houba
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgFnPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
získávají	získávat	k5eAaImIp3nP	získávat
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
pomocí	pomocí	k7c2	pomocí
rozkladu	rozklad	k1gInSc2	rozklad
odumřelých	odumřelý	k2eAgNnPc2d1	odumřelé
živočišných	živočišný	k2eAgNnPc2d1	živočišné
či	či	k8xC	či
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
je	on	k3xPp3gInPc4	on
zařadit	zařadit	k5eAaPmF	zařadit
mezi	mezi	k7c4	mezi
rozkladače	rozkladač	k1gMnPc4	rozkladač
neboli	neboli	k8xC	neboli
dekompozitory	dekompozitor	k1gMnPc4	dekompozitor
<g/>
.	.	kIx.	.
</s>
<s>
Parazitické	parazitický	k2eAgFnPc1d1	parazitická
houby	houba	k1gFnPc1	houba
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
biotrofní	biotrofní	k2eAgFnPc1d1	biotrofní
(	(	kIx(	(
<g/>
živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
obsahem	obsah	k1gInSc7	obsah
buněk	buňka	k1gFnPc2	buňka
ale	ale	k8xC	ale
nezabíjí	zabíjet	k5eNaImIp3nS	zabíjet
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
či	či	k8xC	či
nekrotrofní	krotrofní	k2eNgMnPc1d1	krotrofní
(	(	kIx(	(
<g/>
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
odumírání	odumírání	k1gNnSc4	odumírání
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
skupinami	skupina	k1gFnPc7	skupina
hub	houba	k1gFnPc2	houba
jsou	být	k5eAaImIp3nP	být
houby	houby	k6eAd1	houby
formující	formující	k2eAgInPc1d1	formující
lišejníky	lišejník	k1gInPc1	lišejník
a	a	k8xC	a
houby	houba	k1gFnPc1	houba
mykorhizní	mykorhiznit	k5eAaPmIp3nP	mykorhiznit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
dřevokazné	dřevokazný	k2eAgFnSc2d1	dřevokazná
houby	houba	k1gFnSc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Četné	četný	k2eAgInPc1d1	četný
druhy	druh	k1gInPc1	druh
působí	působit	k5eAaImIp3nP	působit
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
dřevinách	dřevina	k1gFnPc6	dřevina
(	(	kIx(	(
<g/>
dřevokazné	dřevokazný	k2eAgFnPc1d1	dřevokazná
houby	houba	k1gFnPc1	houba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
živočiších	živočich	k1gMnPc6	živočich
i	i	k8xC	i
člověku	člověk	k1gMnSc6	člověk
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
parazitické	parazitický	k2eAgFnPc1d1	parazitická
(	(	kIx(	(
<g/>
cizopasné	cizopasný	k2eAgFnPc1d1	cizopasná
<g/>
)	)	kIx)	)
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
onemocnění	onemocnění	k1gNnSc4	onemocnění
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
těla	tělo	k1gNnSc2	tělo
i	i	k9	i
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
orgánech	orgán	k1gInPc6	orgán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
cizopasným	cizopasný	k2eAgInPc3d1	cizopasný
druhům	druh	k1gInPc3	druh
na	na	k7c4	na
obilí	obilí	k1gNnSc4	obilí
patří	patřit	k5eAaImIp3nP	patřit
padlí	padlý	k1gMnPc1	padlý
<g/>
,	,	kIx,	,
důležitým	důležitý	k2eAgMnSc7d1	důležitý
tropickým	tropický	k2eAgMnSc7d1	tropický
parazitoidem	parazitoid	k1gMnSc7	parazitoid
je	být	k5eAaImIp3nS	být
houba	houba	k1gFnSc1	houba
rodu	rod	k1gInSc2	rod
Cordyceps	Cordycepsa	k1gFnPc2	Cordycepsa
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
dravé	dravý	k2eAgFnPc4d1	dravá
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Arthrobotrys	Arthrobotrys	k1gInSc1	Arthrobotrys
dactyloides	dactyloidesa	k1gFnPc2	dactyloidesa
loví	lovit	k5eAaImIp3nS	lovit
hlístice	hlístice	k1gFnPc4	hlístice
pomocí	pomocí	k7c2	pomocí
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
hyf	hyfa	k1gFnPc2	hyfa
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
mykorhiza	mykorhiza	k1gFnSc1	mykorhiza
a	a	k8xC	a
lišejníky	lišejník	k1gInPc1	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
hub	houba	k1gFnPc2	houba
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
mutualistické	mutualistický	k2eAgFnSc6d1	mutualistická
symbióze	symbióza	k1gFnSc6	symbióza
(	(	kIx(	(
<g/>
symbióza	symbióza	k1gFnSc1	symbióza
prospěšná	prospěšný	k2eAgFnSc1d1	prospěšná
pro	pro	k7c4	pro
oba	dva	k4xCgMnPc4	dva
partnery	partner	k1gMnPc4	partner
<g/>
)	)	kIx)	)
s	s	k7c7	s
kořeny	kořen	k1gInPc4	kořen
mnoha	mnoho	k4c2	mnoho
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nazýváme	nazývat	k5eAaImIp1nP	nazývat
mykorhiza	mykorhiza	k1gFnSc1	mykorhiza
<g/>
.	.	kIx.	.
</s>
<s>
Houba	houba	k1gFnSc1	houba
přijímá	přijímat	k5eAaImIp3nS	přijímat
od	od	k7c2	od
rostlin	rostlina	k1gFnPc2	rostlina
různé	různý	k2eAgFnSc2d1	různá
organické	organický	k2eAgFnSc2d1	organická
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
sama	sám	k3xTgMnSc4	sám
nevytváří	vytvářit	k5eNaPmIp3nS	vytvářit
<g/>
,	,	kIx,	,
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
rostlině	rostlina	k1gFnSc3	rostlina
přijímat	přijímat	k5eAaImF	přijímat
vodu	voda	k1gFnSc4	voda
s	s	k7c7	s
rozpuštěnými	rozpuštěný	k2eAgFnPc7d1	rozpuštěná
minerálními	minerální	k2eAgFnPc7d1	minerální
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
žijí	žít	k5eAaImIp3nP	žít
také	také	k9	také
v	v	k7c6	v
symbióze	symbióza	k1gFnSc6	symbióza
se	se	k3xPyFc4	se
řasami	řasa	k1gFnPc7	řasa
nebo	nebo	k8xC	nebo
sinicemi	sinice	k1gFnPc7	sinice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
složené	složený	k2eAgInPc1d1	složený
organismy	organismus	k1gInPc1	organismus
zvané	zvaný	k2eAgInPc1d1	zvaný
lišejníky	lišejník	k1gInPc1	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
rozkladač	rozkladač	k1gInSc1	rozkladač
<g/>
.	.	kIx.	.
</s>
<s>
Hniložijné	hniložijný	k2eAgFnPc1d1	hniložijný
houby	houba	k1gFnPc1	houba
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
hniložijnými	hniložijný	k2eAgFnPc7d1	hniložijný
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
)	)	kIx)	)
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
rozkladači	rozkladač	k1gInPc7	rozkladač
odumřelých	odumřelý	k2eAgInPc2d1	odumřelý
zbytků	zbytek	k1gInPc2	zbytek
různých	různý	k2eAgInPc2d1	různý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
nazývají	nazývat	k5eAaImIp3nP	nazývat
saprofytické	saprofytický	k2eAgFnPc4d1	saprofytická
houby	houba	k1gFnPc4	houba
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
růst	růst	k1gInSc4	růst
i	i	k9	i
na	na	k7c6	na
potravinách	potravina	k1gFnPc6	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Jedlé	jedlý	k2eAgInPc1d1	jedlý
druhy	druh	k1gInPc1	druh
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k8xC	jako
potravina	potravina	k1gFnSc1	potravina
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
kalorickou	kalorický	k2eAgFnSc7d1	kalorická
hodnotou	hodnota	k1gFnSc7	hodnota
nebo	nebo	k8xC	nebo
jako	jako	k8xS	jako
pochutina	pochutina	k1gFnSc1	pochutina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
bohaté	bohatý	k2eAgInPc4d1	bohatý
na	na	k7c4	na
vitamíny	vitamín	k1gInPc4	vitamín
a	a	k8xC	a
minerální	minerální	k2eAgFnPc4d1	minerální
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jedovaté	jedovatý	k2eAgInPc1d1	jedovatý
druhy	druh	k1gInPc1	druh
nejsou	být	k5eNaImIp3nP	být
početné	početný	k2eAgMnPc4d1	početný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
obsah	obsah	k1gInSc4	obsah
prudkých	prudký	k2eAgInPc2d1	prudký
jedů	jed	k1gInPc2	jed
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc1	kvasinka
jsou	být	k5eAaImIp3nP	být
nezbytné	zbytný	k2eNgFnPc1d1	zbytný
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
potravinářských	potravinářský	k2eAgFnPc2d1	potravinářská
technologií	technologie	k1gFnPc2	technologie
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
pekařství	pekařství	k1gNnSc6	pekařství
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
kvasinek	kvasinka	k1gFnPc2	kvasinka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
napouštění	napouštění	k1gNnSc3	napouštění
konzerv	konzerva	k1gFnPc2	konzerva
a	a	k8xC	a
tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
zamezí	zamezit	k5eAaPmIp3nS	zamezit
kažení	kažení	k1gNnSc1	kažení
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Konzerva	konzerva	k1gFnSc1	konzerva
vydrží	vydržet	k5eAaPmIp3nS	vydržet
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
nezkazí	zkazit	k5eNaPmIp3nP	zkazit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
je	být	k5eAaImIp3nS	být
aplikován	aplikovat	k5eAaBmNgInS	aplikovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
využívají	využívat	k5eAaPmIp3nP	využívat
ve	v	k7c6	v
farmaceutickém	farmaceutický	k2eAgInSc6d1	farmaceutický
a	a	k8xC	a
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
štětičkovce	štětičkovec	k1gInSc2	štětičkovec
druhu	druh	k1gInSc2	druh
Penicillium	Penicillium	k1gNnSc1	Penicillium
notatum	notatum	k1gNnSc1	notatum
byla	být	k5eAaImAgNnP	být
objevena	objeven	k2eAgNnPc1d1	objeveno
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
např.	např.	kA	např.
plísňové	plísňový	k2eAgInPc1d1	plísňový
sýry	sýr	k1gInPc1	sýr
(	(	kIx(	(
<g/>
camembert	camembert	k1gInSc1	camembert
<g/>
,	,	kIx,	,
niva	niva	k1gFnSc1	niva
<g/>
,	,	kIx,	,
hermelín	hermelín	k1gInSc1	hermelín
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
hub	houba	k1gFnPc2	houba
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
očkování	očkování	k1gNnSc3	očkování
prken	prkno	k1gNnPc2	prkno
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ochrání	ochránit	k5eAaPmIp3nS	ochránit
dřevo	dřevo	k1gNnSc1	dřevo
před	před	k7c7	před
cizími	cizí	k2eAgMnPc7d1	cizí
živočichy	živočich	k1gMnPc7	živočich
a	a	k8xC	a
houbami	houba	k1gFnPc7	houba
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houby	k6eAd1	houby
obsadí	obsadit	k5eAaPmIp3nP	obsadit
celý	celý	k2eAgInSc4d1	celý
kus	kus	k1gInSc4	kus
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
nepustí	pustit	k5eNaPmIp3nS	pustit
jiného	jiný	k2eAgMnSc4d1	jiný
parazita	parazit	k1gMnSc4	parazit
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
samy	sám	k3xTgFnPc1	sám
však	však	k9	však
dřevo	dřevo	k1gNnSc4	dřevo
nepoškodí	poškodit	k5eNaPmIp3nS	poškodit
a	a	k8xC	a
nezničí	zničit	k5eNaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
dřevo	dřevo	k1gNnSc4	dřevo
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
pevnější	pevný	k2eAgNnSc1d2	pevnější
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
objevili	objevit	k5eAaPmAgMnP	objevit
vědci	vědec	k1gMnPc1	vědec
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
U	u	k7c2	u
hub	houba	k1gFnPc2	houba
známe	znát	k5eAaImIp1nP	znát
pohlavní	pohlavní	k2eAgNnPc1d1	pohlavní
i	i	k8xC	i
nepohlavní	pohlavní	k2eNgNnPc1d1	nepohlavní
rozmnožování	rozmnožování	k1gNnPc1	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc4	role
výtrusy	výtrus	k1gInPc4	výtrus
čili	čili	k8xC	čili
spory	spor	k1gInPc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nepohlavním	pohlavní	k2eNgNnSc6d1	nepohlavní
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
nedochází	docházet	k5eNaImIp3nS	docházet
ke	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
způsobem	způsob	k1gInSc7	způsob
mohou	moct	k5eAaImIp3nP	moct
dva	dva	k4xCgMnPc1	dva
jedinci	jedinec	k1gMnPc1	jedinec
vzniknout	vzniknout	k5eAaPmF	vzniknout
prostou	prostý	k2eAgFnSc7d1	prostá
fragmentací	fragmentace	k1gFnSc7	fragmentace
(	(	kIx(	(
<g/>
rozpadem	rozpad	k1gInSc7	rozpad
<g/>
)	)	kIx)	)
vláknité	vláknitý	k2eAgFnPc4d1	vláknitá
stélky	stélka	k1gFnPc4	stélka
vedví	vedví	k6eAd1	vedví
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
jednobuněčných	jednobuněčný	k2eAgFnPc2d1	jednobuněčná
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dělit	dělit	k5eAaImF	dělit
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
pučení	pučení	k1gNnSc2	pučení
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
fundamentální	fundamentální	k2eAgFnSc7d1	fundamentální
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
nepohlavně	pohlavně	k6eNd1	pohlavně
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
hub	houba	k1gFnPc2	houba
také	také	k9	také
produkce	produkce	k1gFnSc1	produkce
nepohlavních	pohlavní	k2eNgInPc2d1	nepohlavní
výtrusů	výtrus	k1gInPc2	výtrus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
ve	v	k7c6	v
specializovaných	specializovaný	k2eAgFnPc6d1	specializovaná
sporangiích	sporangie	k1gFnPc6	sporangie
(	(	kIx(	(
<g/>
takové	takový	k3xDgFnPc1	takový
sporangiospory	sporangiospora	k1gFnPc1	sporangiospora
vznikají	vznikat	k5eAaImIp3nP	vznikat
např.	např.	kA	např.
u	u	k7c2	u
rodu	rod	k1gInSc2	rod
Mucor	Mucora	k1gFnPc2	Mucora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
houbových	houbový	k2eAgNnPc6d1	houbové
vláknech	vlákno	k1gNnPc6	vlákno
konidioforech	konidiofor	k1gInPc6	konidiofor
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgInPc6	jenž
vznikající	vznikající	k2eAgInPc1d1	vznikající
spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
konidie	konidie	k1gFnPc1	konidie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
je	být	k5eAaImIp3nS	být
běžným	běžný	k2eAgInSc7d1	běžný
způsobem	způsob	k1gInSc7	způsob
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
se	s	k7c7	s
splýváním	splývání	k1gNnSc7	splývání
buněčných	buněčný	k2eAgNnPc2d1	buněčné
jader	jádro	k1gNnPc2	jádro
a	a	k8xC	a
meiotickými	meiotický	k2eAgNnPc7d1	meiotické
děleními	dělení	k1gNnPc7	dělení
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
jsou	být	k5eAaImIp3nP	být
touto	tento	k3xDgFnSc7	tento
pohlavní	pohlavní	k2eAgFnSc7d1	pohlavní
cestou	cesta	k1gFnSc7	cesta
vytvořeny	vytvořen	k2eAgInPc1d1	vytvořen
spory	spor	k1gInPc1	spor
(	(	kIx(	(
<g/>
výtrusy	výtrus	k1gInPc1	výtrus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
útvary	útvar	k1gInPc7	útvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
výtrusy	výtrus	k1gInPc1	výtrus
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
plodnice	plodnice	k1gFnPc1	plodnice
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
fází	fáze	k1gFnPc2	fáze
<g/>
:	:	kIx,	:
Plazmogamie	Plazmogamie	k1gFnSc1	Plazmogamie
-	-	kIx~	-
splynutí	splynutí	k1gNnSc1	splynutí
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
buněk	buňka	k1gFnPc2	buňka
Karyogamie	karyogamie	k1gFnSc2	karyogamie
-	-	kIx~	-
splynutí	splynutí	k1gNnSc4	splynutí
jader	jádro	k1gNnPc2	jádro
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
zygota	zygota	k1gFnSc1	zygota
s	s	k7c7	s
chromozomy	chromozom	k1gInPc7	chromozom
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgFnPc4d1	ostatní
buňky	buňka	k1gFnPc4	buňka
Meióza	Meióza	k1gFnSc1	Meióza
-	-	kIx~	-
redukční	redukční	k2eAgNnSc1d1	redukční
dělení	dělení	k1gNnSc1	dělení
<g/>
;	;	kIx,	;
vznik	vznik	k1gInSc1	vznik
haploidních	haploidní	k2eAgFnPc2d1	haploidní
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
haploidní	haploidní	k2eAgFnSc2d1	haploidní
gamety	gameta	k1gFnSc2	gameta
-	-	kIx~	-
meiospory	meiospora	k1gFnSc2	meiospora
<g/>
)	)	kIx)	)
Příkladem	příklad	k1gInSc7	příklad
výsledných	výsledný	k2eAgFnPc2d1	výsledná
meiospor	meiospora	k1gFnPc2	meiospora
jsou	být	k5eAaImIp3nP	být
askospory	askospor	k1gInPc1	askospor
vřeckovýtrusných	vřeckovýtrusný	k2eAgFnPc2d1	vřeckovýtrusná
hub	houba	k1gFnPc2	houba
anebo	anebo	k8xC	anebo
basidiospory	basidiospora	k1gFnSc2	basidiospora
stopkovýtrusných	stopkovýtrusný	k2eAgFnPc2d1	stopkovýtrusná
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Gamety	gamet	k1gInPc1	gamet
splynou	splynout	k5eAaPmIp3nP	splynout
a	a	k8xC	a
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
zygota	zygota	k1gFnSc1	zygota
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
hub	houba	k1gFnPc2	houba
splývají	splývat	k5eAaImIp3nP	splývat
hyfy	hyfa	k1gFnPc1	hyfa
(	(	kIx(	(
<g/>
nemají	mít	k5eNaImIp3nP	mít
gametangia	gametangium	k1gNnPc4	gametangium
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klasifikace	klasifikace	k1gFnSc2	klasifikace
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Systematicky	systematicky	k6eAd1	systematicky
se	se	k3xPyFc4	se
houby	houba	k1gFnPc1	houba
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
oddělení	oddělení	k1gNnPc2	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
houby	houba	k1gFnPc1	houba
dělily	dělit	k5eAaImAgFnP	dělit
na	na	k7c4	na
nižší	nízký	k2eAgFnPc4d2	nižší
houby	houba	k1gFnPc4	houba
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc4d2	vyšší
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
nárokům	nárok	k1gInPc3	nárok
na	na	k7c4	na
fylogenetickou	fylogenetický	k2eAgFnSc4d1	fylogenetická
příbuznost	příbuznost	k1gFnSc4	příbuznost
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
od	od	k7c2	od
něho	on	k3xPp3gNnSc2	on
upustilo	upustit	k5eAaPmAgNnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houby	k6eAd1	houby
vyšší	vysoký	k2eAgMnPc1d2	vyšší
mají	mít	k5eAaImIp3nP	mít
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
klobouk	klobouk	k1gInSc4	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
uvedený	uvedený	k2eAgInSc1d1	uvedený
seznam	seznam	k1gInSc1	seznam
uvádí	uvádět	k5eAaImIp3nS	uvádět
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
dosud	dosud	k6eAd1	dosud
používaných	používaný	k2eAgFnPc2d1	používaná
podob	podoba	k1gFnPc2	podoba
klasifikace	klasifikace	k1gFnSc2	klasifikace
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
každého	každý	k3xTgInSc2	každý
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
jejich	jejich	k3xOp3gInPc1	jejich
základní	základní	k2eAgInPc1d1	základní
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Chytridiomycety	Chytridiomycet	k1gInPc1	Chytridiomycet
(	(	kIx(	(
<g/>
Chytridiomycota	Chytridiomycota	k1gFnSc1	Chytridiomycota
<g/>
)	)	kIx)	)
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
bičíkaté	bičíkatý	k2eAgInPc1d1	bičíkatý
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
spory	spor	k1gInPc1	spor
Houby	houby	k6eAd1	houby
spájivé	spájivý	k2eAgInPc1d1	spájivý
(	(	kIx(	(
<g/>
Zygomycota	Zygomycota	k1gFnSc1	Zygomycota
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
životním	životní	k2eAgInSc6d1	životní
cyklu	cyklus	k1gInSc6	cyklus
je	být	k5eAaImIp3nS	být
odolné	odolný	k2eAgNnSc1d1	odolné
zygosporangium	zygosporangium	k1gNnSc1	zygosporangium
Houby	houby	k6eAd1	houby
vřeckovýtrusné	vřeckovýtrusný	k2eAgNnSc1d1	vřeckovýtrusný
(	(	kIx(	(
<g/>
Ascomycota	Ascomycota	k1gFnSc1	Ascomycota
<g/>
)	)	kIx)	)
-	-	kIx~	-
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
spory	spor	k1gInPc1	spor
uložené	uložený	k2eAgInPc1d1	uložený
ve	v	k7c6	v
vřeckách	vřecko	k1gNnPc6	vřecko
Z	z	k7c2	z
nich	on	k3xPp3gFnPc6	on
většina	většina	k1gFnSc1	většina
v	v	k7c6	v
třídě	třída	k1gFnSc6	třída
vřeckovýtrusé	vřeckovýtrusý	k2eAgFnPc1d1	vřeckovýtrusý
(	(	kIx(	(
<g/>
Ascomycetes	Ascomycetes	k1gInSc1	Ascomycetes
<g/>
)	)	kIx)	)
Houby	houby	k6eAd1	houby
<g />
.	.	kIx.	.
</s>
<s>
stopkovýtrusné	stopkovýtrusný	k2eAgNnSc1d1	stopkovýtrusný
(	(	kIx(	(
<g/>
Basidiomycota	Basidiomycota	k1gFnSc1	Basidiomycota
<g/>
)	)	kIx)	)
-	-	kIx~	-
spory	spor	k1gInPc1	spor
umístěné	umístěný	k2eAgInPc1d1	umístěný
na	na	k7c6	na
bazidiu	bazidium	k1gNnSc6	bazidium
Glomeromycota	Glomeromycot	k1gMnSc2	Glomeromycot
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
toho	ten	k3xDgNnSc2	ten
jediná	jediný	k2eAgFnSc1d1	jediná
třída	třída	k1gFnSc1	třída
Glomeromycetes	Glomeromycetesa	k1gFnPc2	Glomeromycetesa
<g/>
)	)	kIx)	)
Houby	houby	k6eAd1	houby
nedokonalé	dokonalý	k2eNgNnSc1d1	nedokonalé
(	(	kIx(	(
<g/>
Deuteromycota	Deuteromycota	k1gFnSc1	Deuteromycota
<g/>
)	)	kIx)	)
-	-	kIx~	-
spíše	spíše	k9	spíše
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nerozmnožují	rozmnožovat	k5eNaImIp3nP	rozmnožovat
pohlavně	pohlavně	k6eAd1	pohlavně
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uznávají	uznávat	k5eAaImIp3nP	uznávat
ještě	ještě	k6eAd1	ještě
oddělení	oddělení	k1gNnSc4	oddělení
Neocallimastigomycota	Neocallimastigomycota	k1gFnSc1	Neocallimastigomycota
a	a	k8xC	a
Blastocladiomycota	Blastocladiomycota	k1gFnSc1	Blastocladiomycota
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc1d1	malá
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
dříve	dříve	k6eAd2	dříve
součástí	součást	k1gFnSc7	součást
chytridiomycet	chytridiomycet	k5eAaImF	chytridiomycet
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
komplexní	komplexní	k2eAgFnSc1d1	komplexní
fylogenetická	fylogenetický	k2eAgFnSc1d1	fylogenetická
systematika	systematika	k1gFnSc1	systematika
hub	houba	k1gFnPc2	houba
zpracovaná	zpracovaný	k2eAgFnSc1d1	zpracovaná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Assembling	Assembling	k1gInSc1	Assembling
the	the	k?	the
Fungal	Fungal	k1gInSc1	Fungal
Tree	Tre	k1gMnSc2	Tre
of	of	k?	of
Life	Lif	k1gMnSc2	Lif
(	(	kIx(	(
<g/>
AFTOL	AFTOL	kA	AFTOL
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
r.	r.	kA	r.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
navázala	navázat	k5eAaPmAgFnS	navázat
v	v	k7c6	v
r.	r.	kA	r.
2009	[number]	k4	2009
fylogenetická	fylogenetický	k2eAgFnSc1d1	fylogenetická
studie	studie	k1gFnSc1	studie
82	[number]	k4	82
kompletních	kompletní	k2eAgInPc2d1	kompletní
genomů	genom	k1gInPc2	genom
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
další	další	k2eAgFnPc4d1	další
studie	studie	k1gFnPc4	studie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
přirozený	přirozený	k2eAgInSc1d1	přirozený
klad	klad	k1gInSc1	klad
Dikarya	Dikaryum	k1gNnSc2	Dikaryum
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
houby	houba	k1gFnSc2	houba
vřeckovýtrusné	vřeckovýtrusný	k2eAgFnSc2d1	vřeckovýtrusná
a	a	k8xC	a
houby	houba	k1gFnSc2	houba
stopkovýtrusné	stopkovýtrusný	k2eAgFnSc2d1	stopkovýtrusná
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
houby	houby	k6eAd1	houby
spájivé	spájivý	k2eAgNnSc1d1	spájivý
(	(	kIx(	(
<g/>
Zygomycota	Zygomycota	k1gFnSc1	Zygomycota
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
nepřirozená	přirozený	k2eNgFnSc1d1	nepřirozená
(	(	kIx(	(
<g/>
parafyletická	parafyletický	k2eAgFnSc1d1	parafyletická
<g/>
)	)	kIx)	)
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrzen	k2eAgNnSc1d1	potvrzeno
zahrnutí	zahrnutí	k1gNnSc1	zahrnutí
mikrosporidií	mikrosporidie	k1gFnPc2	mikrosporidie
do	do	k7c2	do
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
však	však	k9	však
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
pozice	pozice	k1gFnPc4	pozice
ve	v	k7c6	v
fylogenetickém	fylogenetický	k2eAgInSc6d1	fylogenetický
stromu	strom	k1gInSc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Rozella	Rozella	k1gFnSc1	Rozella
<g/>
,	,	kIx,	,
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
absencí	absence	k1gFnSc7	absence
chitinové	chitinový	k2eAgFnSc2d1	chitinová
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
být	být	k5eAaImF	být
zástupcem	zástupce	k1gMnSc7	zástupce
nového	nový	k2eAgInSc2d1	nový
kmene	kmen	k1gInSc2	kmen
Cryptomycota	Cryptomycot	k1gMnSc2	Cryptomycot
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
bazálních	bazální	k2eAgFnPc2d1	bazální
evolučních	evoluční	k2eAgFnPc2d1	evoluční
větví	větev	k1gFnPc2	větev
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušený	zjednodušený	k2eAgInSc1d1	zjednodušený
fylogenetický	fylogenetický	k2eAgInSc1d1	fylogenetický
strom	strom	k1gInSc1	strom
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
hub	houba	k1gFnPc2	houba
vypadá	vypadat	k5eAaImIp3nS	vypadat
podle	podle	k7c2	podle
současných	současný	k2eAgFnPc2d1	současná
představ	představa	k1gFnPc2	představa
(	(	kIx(	(
<g/>
počátek	počátek	k1gInSc4	počátek
r.	r.	kA	r.
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
