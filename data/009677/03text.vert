<p>
<s>
Jeruzalémský	jeruzalémský	k2eAgInSc1d1	jeruzalémský
chrám	chrám	k1gInSc1	chrám
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ב	ב	k?	ב
<g/>
ֵ	ֵ	k?	ֵ
<g/>
י	י	k?	י
ה	ה	k?	ה
<g/>
ַ	ַ	k?	ַ
<g/>
מ	מ	k?	מ
<g/>
ִ	ִ	k?	ִ
<g/>
ק	ק	k?	ק
<g/>
ַ	ַ	k?	ַ
<g/>
ש	ש	k?	ש
<g/>
ֹ	ֹ	k?	ֹ
<g/>
,	,	kIx,	,
bét	bét	k?	bét
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
anglické	anglický	k2eAgNnSc1d1	anglické
the	the	k?	the
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
ha-mikdaš	haikdaš	k1gMnSc1	ha-mikdaš
<g/>
,	,	kIx,	,
dosl.	dosl.	k?	dosl.
"	"	kIx"	"
<g/>
dům	dům	k1gInSc1	dům
svatosti	svatost	k1gFnSc2	svatost
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
posvěcení	posvěcení	k1gNnSc1	posvěcení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Svatyně	svatyně	k1gFnSc1	svatyně
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
centrem	centrum	k1gNnSc7	centrum
náboženského	náboženský	k2eAgInSc2d1	náboženský
kultu	kult	k1gInSc2	kult
izraelského	izraelský	k2eAgInSc2d1	izraelský
lidu	lid	k1gInSc2	lid
mezi	mezi	k7c7	mezi
10	[number]	k4	10
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
n.	n.	k?	n.
l.	l.	k?	l.
Chrám	chrám	k1gInSc1	chrám
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
na	na	k7c6	na
chrámové	chrámový	k2eAgFnSc6d1	chrámová
hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
obětovaly	obětovat	k5eAaBmAgFnP	obětovat
oběti	oběť	k1gFnPc1	oběť
a	a	k8xC	a
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
centrum	centrum	k1gNnSc1	centrum
aktivity	aktivita	k1gFnSc2	aktivita
kněží	kněz	k1gMnPc2	kněz
a	a	k8xC	a
levitů	levit	k1gMnPc2	levit
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
"	"	kIx"	"
<g/>
onen	onen	k3xDgInSc4	onen
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
blíže	blízce	k6eAd2	blízce
neupřesněná	upřesněný	k2eNgFnSc1d1	neupřesněná
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
označení	označení	k1gNnSc1	označení
budoucího	budoucí	k2eAgInSc2d1	budoucí
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
bude	být	k5eAaImBp3nS	být
Chrám	chrám	k1gInSc1	chrám
znovu	znovu	k6eAd1	znovu
vybudován	vybudován	k2eAgInSc4d1	vybudován
Mesiášem	Mesiáš	k1gMnSc7	Mesiáš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přenosná	přenosný	k2eAgFnSc1d1	přenosná
svatyně	svatyně	k1gFnSc1	svatyně
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Tóře	tóra	k1gFnSc6	tóra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
příkaz	příkaz	k1gInSc1	příkaz
postavit	postavit	k5eAaPmF	postavit
svatyni	svatyně	k1gFnSc4	svatyně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ať	ať	k9	ať
mi	já	k3xPp1nSc3	já
udělají	udělat	k5eAaPmIp3nP	udělat
svatyni	svatyně	k1gFnSc4	svatyně
(	(	kIx(	(
<g/>
מ	מ	k?	מ
<g/>
ִ	ִ	k?	ִ
<g/>
ק	ק	k?	ק
<g/>
ְ	ְ	k?	ְ
<g/>
ד	ד	k?	ד
<g/>
ָ	ָ	k?	ָ
<g/>
ּ	ּ	k?	ּ
<g/>
ש	ש	k?	ש
<g/>
ׁ	ׁ	k?	ׁ
<g/>
,	,	kIx,	,
<g/>
mikdaš	mikdaš	k5eAaPmIp2nS	mikdaš
<g/>
)	)	kIx)	)
a	a	k8xC	a
já	já	k3xPp1nSc1	já
budu	být	k5eAaImBp1nS	být
bydlet	bydlet	k5eAaImF	bydlet
uprostřed	uprostřed	k7c2	uprostřed
nich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
byl	být	k5eAaImAgInS	být
Mojžíšem	Mojžíš	k1gMnSc7	Mojžíš
zbudován	zbudován	k2eAgInSc4d1	zbudován
přenosný	přenosný	k2eAgInSc4d1	přenosný
příbytek	příbytek	k1gInSc4	příbytek
Hospodinův	Hospodinův	k2eAgInSc4d1	Hospodinův
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
první	první	k4xOgFnPc1	první
svatyně	svatyně	k1gFnPc1	svatyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Deuteronomium	Deuteronomium	k1gNnSc1	Deuteronomium
<g/>
,	,	kIx,	,
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
země	zem	k1gFnSc2	zem
zaslíbené	zaslíbená	k1gFnSc2	zaslíbená
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
centralizaci	centralizace	k1gFnSc4	centralizace
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
do	do	k7c2	do
jediné	jediný	k2eAgFnSc2d1	jediná
svatyně	svatyně	k1gFnSc2	svatyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
stát	stát	k5eAaImF	stát
"	"	kIx"	"
<g/>
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Hospodin	Hospodin	k1gMnSc1	Hospodin
...	...	k?	...
vyvolí	vyvolit	k5eAaPmIp3nS	vyvolit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Izraelité	izraelita	k1gMnPc1	izraelita
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
země	zem	k1gFnSc2	zem
zaslíbené	zaslíbená	k1gFnSc2	zaslíbená
<g/>
,	,	kIx,	,
putoval	putovat	k5eAaImAgInS	putovat
příbytek	příbytek	k1gInSc1	příbytek
Hospodinův	Hospodinův	k2eAgInSc1d1	Hospodinův
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
příbytek	příbytek	k1gInSc1	příbytek
po	po	k7c4	po
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
stál	stát	k5eAaImAgInS	stát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Šílo	Šílo	k6eAd1	Šílo
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
popisuje	popisovat	k5eAaImIp3nS	popisovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
Samuelova	Samuelův	k2eAgFnSc1d1	Samuelova
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejsvětějším	nejsvětější	k2eAgNnSc7d1	nejsvětější
místem	místo	k1gNnSc7	místo
svatyně	svatyně	k1gFnSc2	svatyně
byla	být	k5eAaImAgFnS	být
velesvatyně	velesvatyně	k1gFnSc1	velesvatyně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byla	být	k5eAaImAgFnS	být
Archa	archa	k1gFnSc1	archa
úmluvy	úmluva	k1gFnSc2	úmluva
s	s	k7c7	s
příkrovem	příkrov	k1gInSc7	příkrov
s	s	k7c7	s
cheruby	cherub	k1gMnPc7	cherub
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
z	z	k7c2	z
úcty	úcta	k1gFnSc2	úcta
odděleno	oddělit	k5eAaPmNgNnS	oddělit
oponou	opona	k1gFnSc7	opona
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
lid	lid	k1gInSc1	lid
nemohl	moct	k5eNaImAgInS	moct
spatřit	spatřit	k5eAaPmF	spatřit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
velesvatyně	velesvatyně	k1gFnSc2	velesvatyně
bylo	být	k5eAaImAgNnS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
vstoupit	vstoupit	k5eAaPmF	vstoupit
jen	jen	k9	jen
veleknězi	velekněz	k1gMnSc3	velekněz
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
dni	den	k1gInSc6	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
v	v	k7c4	v
Den	den	k1gInSc4	den
smíření	smíření	k1gNnSc2	smíření
–	–	k?	–
Jom	Jom	k1gMnSc1	Jom
kipur	kipur	k1gMnSc1	kipur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
velesvatyní	velesvatyně	k1gFnSc7	velesvatyně
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
svatyně	svatyně	k1gFnSc1	svatyně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c6	na
pozlaceném	pozlacený	k2eAgInSc6d1	pozlacený
oltáři	oltář	k1gInSc6	oltář
obětovalo	obětovat	k5eAaBmAgNnS	obětovat
kadidlo	kadidlo	k1gNnSc1	kadidlo
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
oltáři	oltář	k1gInSc3	oltář
pak	pak	k6eAd1	pak
stála	stát	k5eAaImAgFnS	stát
menora	menora	k1gFnSc1	menora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
byl	být	k5eAaImAgMnS	být
ještě	ještě	k9	ještě
pozlacený	pozlacený	k2eAgInSc4d1	pozlacený
stůl	stůl	k1gInSc4	stůl
s	s	k7c7	s
předkladnými	předkladný	k2eAgInPc7d1	předkladný
chleby	chléb	k1gInPc7	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
mohli	moct	k5eAaImAgMnP	moct
vstupovat	vstupovat	k5eAaImF	vstupovat
jen	jen	k9	jen
kněží	kněz	k1gMnPc1	kněz
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Áronova	Áronov	k1gInSc2	Áronov
<g/>
.	.	kIx.	.
</s>
<s>
Kněží	kněz	k1gMnPc1	kněz
museli	muset	k5eAaImAgMnP	muset
přicházet	přicházet	k5eAaImF	přicházet
bosí	bosý	k2eAgMnPc1d1	bosý
jakožto	jakožto	k8xS	jakožto
projevení	projevený	k2eAgMnPc1d1	projevený
úcty	úcta	k1gFnPc4	úcta
Hospodinově	Hospodinův	k2eAgFnSc3d1	Hospodinova
svatosti	svatost	k1gFnSc3	svatost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kněží	kněz	k1gMnPc2	kněz
se	se	k3xPyFc4	se
chrámové	chrámový	k2eAgFnSc2d1	chrámová
služby	služba	k1gFnSc2	služba
účastnili	účastnit	k5eAaImAgMnP	účastnit
také	také	k9	také
levité	levit	k1gMnPc1	levit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
putování	putování	k1gNnSc6	putování
pouští	poušť	k1gFnPc2	poušť
byli	být	k5eAaImAgMnP	být
odpovědní	odpovědný	k2eAgMnPc1d1	odpovědný
za	za	k7c4	za
přesun	přesun	k1gInSc4	přesun
celého	celý	k2eAgInSc2d1	celý
příbytku	příbytek	k1gInSc2	příbytek
Hospodinova	Hospodinův	k2eAgInSc2d1	Hospodinův
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jejich	jejich	k3xOp3gFnSc1	jejich
služba	služba	k1gFnSc1	služba
nabyla	nabýt	k5eAaPmAgFnS	nabýt
těchto	tento	k3xDgFnPc2	tento
forem	forma	k1gFnPc2	forma
<g/>
:	:	kIx,	:
levité	levit	k1gMnPc1	levit
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
stráží	stráž	k1gFnSc7	stráž
svatyně	svatyně	k1gFnSc2	svatyně
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
chrámovou	chrámový	k2eAgFnSc4d1	chrámová
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobu	podoba	k1gFnSc4	podoba
prvotního	prvotní	k2eAgNnSc2d1	prvotní
chrámového	chrámový	k2eAgNnSc2d1	chrámové
vybavení	vybavení	k1gNnSc2	vybavení
můžeme	moct	k5eAaImIp1nP	moct
jen	jen	k9	jen
odhadovat	odhadovat	k5eAaImF	odhadovat
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
odkázáni	odkázat	k5eAaPmNgMnP	odkázat
jen	jen	k9	jen
na	na	k7c4	na
slovní	slovní	k2eAgInSc4d1	slovní
popis	popis	k1gInSc4	popis
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
vybavení	vybavení	k1gNnSc2	vybavení
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
zničení	zničení	k1gNnSc6	zničení
Chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc4	první
(	(	kIx(	(
<g/>
Šalomounův	Šalomounův	k2eAgInSc4d1	Šalomounův
<g/>
)	)	kIx)	)
Chrám	chrám	k1gInSc4	chrám
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
židovské	židovský	k2eAgFnSc2d1	židovská
tradice	tradice	k1gFnSc2	tradice
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
jeruzalémský	jeruzalémský	k2eAgInSc1d1	jeruzalémský
Chrám	chrám	k1gInSc1	chrám
zbudován	zbudován	k2eAgInSc1d1	zbudován
králem	král	k1gMnSc7	král
Šalomounem	Šalomoun	k1gMnSc7	Šalomoun
(	(	kIx(	(
<g/>
970	[number]	k4	970
<g/>
-	-	kIx~	-
<g/>
931	[number]	k4	931
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
na	na	k7c6	na
Chrámové	chrámový	k2eAgFnSc6d1	chrámová
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Nemáme	mít	k5eNaImIp1nP	mít
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
žádná	žádný	k3yNgNnPc4	žádný
přímá	přímý	k2eAgNnPc4d1	přímé
archeologická	archeologický	k2eAgNnPc4d1	Archeologické
svědectví	svědectví	k1gNnPc4	svědectví
kvůli	kvůli	k7c3	kvůli
přestavbám	přestavba	k1gFnPc3	přestavba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
<g/>
.	.	kIx.	.
</s>
<s>
Prosévání	prosévání	k1gNnSc1	prosévání
půdy	půda	k1gFnSc2	půda
z	z	k7c2	z
Chrámové	chrámový	k2eAgFnSc2d1	chrámová
hory	hora	k1gFnSc2	hora
přineslo	přinést	k5eAaPmAgNnS	přinést
jen	jen	k9	jen
nepřímá	přímý	k2eNgNnPc4d1	nepřímé
svědectví	svědectví	k1gNnPc4	svědectví
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
razítko	razítko	k1gNnSc4	razítko
jedné	jeden	k4xCgFnSc2	jeden
kněžské	kněžský	k2eAgFnSc2d1	kněžská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Jeremjáš	Jeremjáš	k1gFnSc2	Jeremjáš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
David	David	k1gMnSc1	David
učinil	učinit	k5eAaImAgMnS	učinit
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
centrem	centrum	k1gNnSc7	centrum
izraelského	izraelský	k2eAgNnSc2d1	izraelské
království	království	k1gNnSc2	království
a	a	k8xC	a
přenesl	přenést	k5eAaPmAgInS	přenést
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
Boží	božit	k5eAaImIp3nS	božit
Archu	archa	k1gFnSc4	archa
úmluvy	úmluva	k1gFnSc2	úmluva
(	(	kIx(	(
<g/>
2	[number]	k4	2
Sam	Sam	k1gMnSc1	Sam
6,1	[number]	k4	6,1
<g/>
nn	nn	k?	nn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šalomoun	Šalomoun	k1gMnSc1	Šalomoun
pak	pak	k6eAd1	pak
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
Chrám	chrám	k1gInSc4	chrám
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
dům	dům	k1gInSc1	dům
Božího	boží	k2eAgNnSc2d1	boží
přebývání	přebývání	k1gNnSc2	přebývání
<g/>
"	"	kIx"	"
a	a	k8xC	a
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
připoutání	připoutání	k1gNnSc2	připoutání
lidu	lid	k1gInSc2	lid
k	k	k7c3	k
Davidovu	Davidův	k2eAgInSc3d1	Davidův
rodu	rod	k1gInSc3	rod
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
celého	celý	k2eAgInSc2d1	celý
objektu	objekt	k1gInSc2	objekt
trvala	trvat	k5eAaImAgFnS	trvat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
celých	celý	k2eAgNnPc2d1	celé
7	[number]	k4	7
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
Bible	bible	k1gFnSc1	bible
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
Královská	královský	k2eAgFnSc1d1	královská
6	[number]	k4	6
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
38	[number]	k4	38
verš	verš	k1gInSc1	verš
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stavěl	stavět	k5eAaImAgMnS	stavět
jej	on	k3xPp3gMnSc4	on
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
rok	rok	k1gInSc4	rok
Šalamounovy	Šalamounův	k2eAgFnSc2d1	Šalamounova
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
přibližně	přibližně	k6eAd1	přibližně
966	[number]	k4	966
<g/>
-	-	kIx~	-
<g/>
950	[number]	k4	950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Šalomoun	Šalomoun	k1gMnSc1	Šalomoun
od	od	k7c2	od
lidu	lid	k1gInSc2	lid
vybíral	vybírat	k5eAaImAgMnS	vybírat
pro	pro	k7c4	pro
účel	účel	k1gInSc4	účel
stavby	stavba	k1gFnSc2	stavba
vysoké	vysoký	k2eAgFnSc2d1	vysoká
daně	daň	k1gFnSc2	daň
a	a	k8xC	a
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
mu	on	k3xPp3gMnSc3	on
byli	být	k5eAaImAgMnP	být
nápomocni	nápomocen	k2eAgMnPc1d1	nápomocen
tyrští	tyrský	k2eAgMnPc1d1	tyrský
stavitelé	stavitel	k1gMnPc1	stavitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatyně	svatyně	k1gFnSc1	svatyně
měla	mít	k5eAaImAgFnS	mít
3	[number]	k4	3
základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Předsíň	předsíň	k1gFnSc1	předsíň
–	–	k?	–
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
svatyně	svatyně	k1gFnSc2	svatyně
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Svatyně	svatyně	k1gFnSc1	svatyně
–	–	k?	–
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehrávala	odehrávat	k5eAaImAgFnS	odehrávat
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
(	(	kIx(	(
<g/>
stál	stát	k5eAaImAgInS	stát
zde	zde	k6eAd1	zde
oltář	oltář	k1gInSc1	oltář
a	a	k8xC	a
menora	menora	k1gFnSc1	menora
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Velesvatyně	Velesvatyně	k1gFnSc1	Velesvatyně
–	–	k?	–
tam	tam	k6eAd1	tam
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
Archa	archa	k1gFnSc1	archa
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgInSc1	první
Chrám	chrám	k1gInSc1	chrám
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
pahorku	pahorek	k1gInSc6	pahorek
téměř	téměř	k6eAd1	téměř
400	[number]	k4	400
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
586	[number]	k4	586
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
vypálen	vypálit	k5eAaPmNgMnS	vypálit
vojskem	vojsko	k1gNnSc7	vojsko
babylonského	babylonský	k2eAgMnSc2d1	babylonský
krále	král	k1gMnSc2	král
Nebukadnezara	Nebukadnezar	k1gMnSc2	Nebukadnezar
II	II	kA	II
<g/>
.	.	kIx.	.
při	při	k7c6	při
dobytí	dobytí	k1gNnSc6	dobytí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yIgInSc6	který
následovalo	následovat	k5eAaImAgNnS	následovat
babylonské	babylonský	k2eAgNnSc4d1	babylonské
zajetí	zajetí	k1gNnSc4	zajetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhý	druhý	k4xOgInSc1	druhý
Chrám	chrám	k1gInSc1	chrám
===	===	k?	===
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
Chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
zbudován	zbudovat	k5eAaPmNgInS	zbudovat
navrátilci	navrátilec	k1gMnSc3	navrátilec
z	z	k7c2	z
babylonského	babylonský	k2eAgNnSc2d1	babylonské
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
,	,	kIx,	,
vedenými	vedený	k2eAgFnPc7d1	vedená
Zerubábelem	Zerubábel	k1gMnSc7	Zerubábel
z	z	k7c2	z
Davidova	Davidův	k2eAgInSc2d1	Davidův
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
sedmdesát	sedmdesát	k4xCc1	sedmdesát
let	léto	k1gNnPc2	léto
po	po	k7c6	po
zničení	zničení	k1gNnSc3	zničení
prvního	první	k4xOgInSc2	první
Chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Umožnil	umožnit	k5eAaPmAgInS	umožnit
to	ten	k3xDgNnSc1	ten
edikt	edikt	k1gInSc1	edikt
nového	nový	k2eAgMnSc2d1	nový
pána	pán	k1gMnSc2	pán
Babylonie	Babylonie	k1gFnSc2	Babylonie
<g/>
,	,	kIx,	,
perského	perský	k2eAgMnSc2d1	perský
krále	král	k1gMnSc2	král
Kýra	Kýrum	k1gNnSc2	Kýrum
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
se	se	k3xPyFc4	se
vyhlašovala	vyhlašovat	k5eAaImAgFnS	vyhlašovat
náboženská	náboženský	k2eAgFnSc1d1	náboženská
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
Chrámu	chrám	k1gInSc6	chrám
již	již	k6eAd1	již
chyběla	chybět	k5eAaImAgFnS	chybět
Archa	archa	k1gFnSc1	archa
úmluvy	úmluva	k1gFnSc2	úmluva
(	(	kIx(	(
<g/>
ve	v	k7c6	v
svatyni	svatyně	k1gFnSc6	svatyně
tak	tak	k6eAd1	tak
zůstal	zůstat	k5eAaPmAgInS	zůstat
jen	jen	k9	jen
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
schrána	schrána	k1gFnSc1	schrána
stála	stát	k5eAaImAgFnS	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nádoba	nádoba	k1gFnSc1	nádoba
s	s	k7c7	s
manou	mana	k1gFnSc7	mana
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
uchovávána	uchovávat	k5eAaImNgFnS	uchovávat
z	z	k7c2	z
putování	putování	k1gNnSc2	putování
Izraele	Izrael	k1gInSc2	Izrael
do	do	k7c2	do
Kanaánu	Kanaán	k1gInSc2	Kanaán
<g/>
)	)	kIx)	)
a	a	k8xC	a
Áronova	Áronův	k2eAgFnSc1d1	Áronova
hůl	hůl	k1gFnSc1	hůl
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvního	první	k4xOgInSc2	první
chrámu	chrám	k1gInSc2	chrám
tak	tak	k9	tak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
jen	jen	k9	jen
menora	menora	k1gFnSc1	menora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zbudován	zbudovat	k5eAaPmNgInS	zbudovat
navrátilci	navrátilec	k1gMnSc3	navrátilec
z	z	k7c2	z
Babylóna	Babylón	k1gInSc2	Babylón
byl	být	k5eAaImAgInS	být
však	však	k9	však
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
a	a	k8xC	a
prostý	prostý	k2eAgInSc1d1	prostý
<g/>
.	.	kIx.	.
</s>
<s>
Herodes	Herodes	k1gMnSc1	Herodes
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
74	[number]	k4	74
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
4	[number]	k4	4
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
jej	on	k3xPp3gMnSc4	on
proto	proto	k6eAd1	proto
roku	rok	k1gInSc2	rok
19	[number]	k4	19
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
nechal	nechat	k5eAaPmAgMnS	nechat
přestavět	přestavět	k5eAaPmF	přestavět
a	a	k8xC	a
proměnil	proměnit	k5eAaPmAgMnS	proměnit
jej	on	k3xPp3gMnSc4	on
v	v	k7c4	v
honosnou	honosný	k2eAgFnSc4d1	honosná
stavbu	stavba	k1gFnSc4	stavba
(	(	kIx(	(
<g/>
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
získal	získat	k5eAaPmAgInS	získat
přízeň	přízeň	k1gFnSc4	přízeň
židovského	židovský	k2eAgInSc2d1	židovský
lidu	lid	k1gInSc2	lid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Herodův	Herodův	k2eAgInSc1d1	Herodův
chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
zbudován	zbudovat	k5eAaPmNgInS	zbudovat
ze	z	k7c2	z
zelenkavého	zelenkavý	k2eAgInSc2d1	zelenkavý
a	a	k8xC	a
bílého	bílý	k2eAgInSc2d1	bílý
mramoru	mramor	k1gInSc2	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Vylepšování	vylepšování	k1gNnSc1	vylepšování
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
Chrámu	chrám	k1gInSc2	chrám
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
po	po	k7c6	po
Herodově	Herodův	k2eAgFnSc6d1	Herodova
smrti	smrt	k1gFnSc6	smrt
–	–	k?	–
téměř	téměř	k6eAd1	téměř
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
Chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
Chrám	chrám	k1gInSc1	chrám
stál	stát	k5eAaImAgInS	stát
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
70	[number]	k4	70
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vypálen	vypálen	k2eAgInSc1d1	vypálen
římským	římský	k2eAgNnSc7d1	římské
vojskem	vojsko	k1gNnSc7	vojsko
vedeným	vedený	k2eAgNnSc7d1	vedené
Titem	Titum	k1gNnSc7	Titum
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
zbudování	zbudování	k1gNnSc4	zbudování
třetího	třetí	k4xOgMnSc2	třetí
Chrámu	chrám	k1gInSc2	chrám
==	==	k?	==
</s>
</p>
<p>
<s>
Touha	touha	k1gFnSc1	touha
po	po	k7c6	po
Chrámu	chrám	k1gInSc6	chrám
a	a	k8xC	a
bohoslužbě	bohoslužba	k1gFnSc3	bohoslužba
je	být	k5eAaImIp3nS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
v	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
židovských	židovský	k2eAgFnPc6d1	židovská
tradicích	tradice	k1gFnPc6	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
podoby	podoba	k1gFnPc1	podoba
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
touha	touha	k1gFnSc1	touha
nabývala	nabývat	k5eAaImAgFnS	nabývat
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
Římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
vládl	vládnout	k5eAaImAgMnS	vládnout
císař	císař	k1gMnSc1	císař
Julianus	Julianus	k1gMnSc1	Julianus
Apostata	apostata	k1gMnSc1	apostata
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
vší	všecek	k3xTgFnSc7	všecek
silou	síla	k1gFnSc7	síla
oslabit	oslabit	k5eAaPmF	oslabit
stále	stále	k6eAd1	stále
rozšířenější	rozšířený	k2eAgNnSc4d2	rozšířenější
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgInS	podporovat
tedy	tedy	k9	tedy
křesťanské	křesťanský	k2eAgMnPc4d1	křesťanský
heretiky	heretik	k1gMnPc4	heretik
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
křesťanství	křesťanství	k1gNnSc4	křesťanství
oslabil	oslabit	k5eAaPmAgInS	oslabit
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jiná	jiný	k2eAgNnPc1d1	jiné
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
i	i	k9	i
judaismus	judaismus	k1gInSc4	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
362	[number]	k4	362
před	před	k7c7	před
židovskými	židovský	k2eAgMnPc7d1	židovský
předáky	předák	k1gMnPc7	předák
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nechám	nechat	k5eAaPmIp1nS	nechat
postavit	postavit	k5eAaPmF	postavit
s	s	k7c7	s
veškerým	veškerý	k3xTgNnSc7	veškerý
úsilím	úsilí	k1gNnSc7	úsilí
chrám	chrám	k1gInSc1	chrám
Bohu	bůh	k1gMnSc3	bůh
Nejvyššímu	vysoký	k2eAgMnSc3d3	nejvyšší
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Císař	Císař	k1gMnSc1	Císař
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
židy	žid	k1gMnPc4	žid
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
rodné	rodný	k2eAgFnSc2d1	rodná
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
svatyni	svatyně	k1gFnSc4	svatyně
a	a	k8xC	a
řídili	řídit	k5eAaImAgMnP	řídit
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zákony	zákon	k1gInPc4	zákon
svých	svůj	k3xOyFgMnPc2	svůj
otců	otec	k1gMnPc2	otec
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
Představenstvo	představenstvo	k1gNnSc1	představenstvo
židů	žid	k1gMnPc2	žid
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Hillelem	Hillel	k1gMnSc7	Hillel
II	II	kA	II
<g/>
.	.	kIx.	.
protestovalo	protestovat	k5eAaBmAgNnS	protestovat
vůči	vůči	k7c3	vůči
obnově	obnova	k1gFnSc3	obnova
svatyně	svatyně	k1gFnSc2	svatyně
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
lid	lid	k1gInSc1	lid
rozštěpí	rozštěpit	k5eAaPmIp3nS	rozštěpit
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k9	jako
byl	být	k5eAaImAgInS	být
lid	lid	k1gInSc1	lid
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
v	v	k7c6	v
období	období	k1gNnSc6	období
druhého	druhý	k4xOgInSc2	druhý
chrámu	chrám	k1gInSc2	chrám
mezi	mezi	k7c4	mezi
kněze	kněz	k1gMnPc4	kněz
a	a	k8xC	a
mudrce	mudrc	k1gMnPc4	mudrc
(	(	kIx(	(
<g/>
srv	srv	k?	srv
<g/>
.	.	kIx.	.
esejské	esejský	k2eAgNnSc1d1	esejské
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
počin	počin	k1gInSc1	počin
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
cizince	cizinec	k1gMnSc2	cizinec
(	(	kIx(	(
<g/>
Iuliana	Iulian	k1gMnSc2	Iulian
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gFnSc6	jehož
smrti	smrt	k1gFnSc6	smrt
bude	být	k5eAaImBp3nS	být
budování	budování	k1gNnSc1	budování
Chrámu	chrám	k1gInSc2	chrám
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
židů	žid	k1gMnPc2	žid
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
nejistotu	nejistota	k1gFnSc4	nejistota
nesdílela	sdílet	k5eNaImAgFnS	sdílet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Chrám	chrám	k1gInSc1	chrám
bude	být	k5eAaImBp3nS	být
zbudován	zbudovat	k5eAaPmNgInS	zbudovat
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
království	království	k1gNnSc2	království
rodu	rod	k1gInSc2	rod
Davidova	Davidův	k2eAgInSc2d1	Davidův
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stavba	stavba	k1gFnSc1	stavba
nicméně	nicméně	k8xC	nicméně
byla	být	k5eAaImAgFnS	být
započata	započnout	k5eAaPmNgFnS	započnout
–	–	k?	–
z	z	k7c2	z
peněz	peníze	k1gInPc2	peníze
římské	římský	k2eAgFnSc2d1	římská
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
363	[number]	k4	363
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
Iulianus	Iulianus	k1gInSc4	Iulianus
proti	proti	k7c3	proti
Perské	perský	k2eAgFnSc3d1	perská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
tažení	tažení	k1gNnSc6	tažení
však	však	k9	však
padl	padnout	k5eAaPmAgInS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
opravdu	opravdu	k6eAd1	opravdu
znamenala	znamenat	k5eAaImAgFnS	znamenat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
židé	žid	k1gMnPc1	žid
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
–	–	k?	–
stavba	stavba	k1gFnSc1	stavba
Chrámu	chrám	k1gInSc2	chrám
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1099	[number]	k4	1099
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
křesťanskými	křesťanský	k2eAgInPc7d1	křesťanský
křižáky	křižák	k1gInPc7	křižák
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
ruin	ruina	k1gFnPc2	ruina
mešity	mešita	k1gFnSc2	mešita
Al-Aksá	Al-Aksý	k2eAgFnSc1d1	Al-Aksá
postupně	postupně	k6eAd1	postupně
zbudováno	zbudován	k2eAgNnSc4d1	zbudováno
sídlo	sídlo	k1gNnSc4	sídlo
pro	pro	k7c4	pro
nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
Řád	řád	k1gInSc4	řád
templářů	templář	k1gMnPc2	templář
s	s	k7c7	s
přistavěným	přistavěný	k2eAgInSc7d1	přistavěný
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
původního	původní	k2eAgMnSc4d1	původní
křídlo	křídlo	k1gNnSc4	křídlo
zdejšího	zdejší	k2eAgInSc2d1	zdejší
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
templ	templ	k1gInSc1	templ
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
Šalamounův	Šalamounův	k2eAgInSc4d1	Šalamounův
chrám	chrám	k1gInSc4	chrám
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
zbudován	zbudovat	k5eAaPmNgInS	zbudovat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgInSc2d1	původní
Chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
templářský	templářský	k2eAgInSc1d1	templářský
templ	templ	k1gInSc1	templ
však	však	k9	však
nedosahoval	dosahovat	k5eNaImAgInS	dosahovat
jeho	jeho	k3xOp3gFnPc4	jeho
velikosti	velikost	k1gFnPc4	velikost
ani	ani	k8xC	ani
původní	původní	k2eAgFnSc4d1	původní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
během	během	k7c2	během
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k8xS	jako
velitelství	velitelství	k1gNnSc4	velitelství
řádu	řád	k1gInSc2	řád
s	s	k7c7	s
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
svatostánkem	svatostánek	k1gInSc7	svatostánek
<g/>
,	,	kIx,	,
kasárny	kasárny	k1gFnPc1	kasárny
i	i	k8xC	i
stáje	stáj	k1gFnPc1	stáj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
do	do	k7c2	do
Šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
veškeré	veškerý	k3xTgInPc1	veškerý
další	další	k2eAgInPc1d1	další
plány	plán	k1gInPc1	plán
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
Chrámu	chrámat	k5eAaImIp1nS	chrámat
pouze	pouze	k6eAd1	pouze
teoretickými	teoretický	k2eAgFnPc7d1	teoretická
úvahami	úvaha	k1gFnPc7	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
mesiášským	mesiášský	k2eAgInSc7d1	mesiášský
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Šestidenní	šestidenní	k2eAgFnSc6d1	šestidenní
válce	válka	k1gFnSc6	válka
však	však	k9	však
Izrael	Izrael	k1gInSc1	Izrael
získal	získat	k5eAaPmAgInS	získat
mj.	mj.	kA	mj.
Východní	východní	k2eAgInSc4d1	východní
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
spadá	spadat	k5eAaImIp3nS	spadat
i	i	k9	i
Chrámová	chrámový	k2eAgFnSc1d1	chrámová
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
organizována	organizován	k2eAgNnPc4d1	organizováno
malá	malý	k2eAgNnPc4d1	malé
hnutí	hnutí	k1gNnPc4	hnutí
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
postavit	postavit	k5eAaPmF	postavit
Chrám	chrám	k1gInSc4	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takové	takový	k3xDgFnPc4	takový
organizace	organizace	k1gFnPc4	organizace
patří	patřit	k5eAaImIp3nS	patřit
Temple	templ	k1gInSc5	templ
Institute	institut	k1gInSc5	institut
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
מ	מ	k?	מ
ה	ה	k?	ה
<g/>
,	,	kIx,	,
Machon	Machon	k1gMnSc1	Machon
Ha-Mikdaš	Ha-Mikdaš	k1gMnSc1	Ha-Mikdaš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Temple	templ	k1gInSc5	templ
Mount	Mount	k1gMnSc1	Mount
and	and	k?	and
Eretz	Eretz	k1gMnSc1	Eretz
Yisrael	Yisrael	k1gMnSc1	Yisrael
Faithful	Faithfula	k1gFnPc2	Faithfula
Movement	Movement	k1gMnSc1	Movement
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
נ	נ	k?	נ
ה	ה	k?	ה
ה	ה	k?	ה
<g/>
,	,	kIx,	,
Neemanej	Neemanej	k1gMnSc1	Neemanej
Har	Har	k1gMnSc1	Har
Ha-Bajit	Ha-Bajit	k1gMnSc1	Ha-Bajit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
hnutí	hnutí	k1gNnPc1	hnutí
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
podřízena	podřízen	k2eAgNnPc1d1	podřízeno
autoritě	autorita	k1gFnSc3	autorita
různých	různý	k2eAgMnPc2d1	různý
rabínů	rabín	k1gMnPc2	rabín
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
Chrám	chrám	k1gInSc1	chrám
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
až	až	k6eAd1	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
příchodu	příchod	k1gInSc2	příchod
mesiáše	mesiáš	k1gMnSc4	mesiáš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
ideje	idea	k1gFnPc1	idea
však	však	k9	však
nutně	nutně	k6eAd1	nutně
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
zničení	zničení	k1gNnSc3	zničení
Skalního	skalní	k2eAgInSc2d1	skalní
dómu	dóm	k1gInSc2	dóm
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
muslimové	muslim	k1gMnPc1	muslim
hrozí	hrozit	k5eAaImIp3nS	hrozit
této	tento	k3xDgFnSc2	tento
touhy	touha	k1gFnSc2	touha
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Islámská	islámský	k2eAgNnPc1d1	islámské
hnutí	hnutí	k1gNnPc1	hnutí
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
proto	proto	k8xC	proto
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
usilují	usilovat	k5eAaImIp3nP	usilovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránila	zabránit	k5eAaPmAgFnS	zabránit
vybudování	vybudování	k1gNnSc4	vybudování
Chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
popírají	popírat	k5eAaImIp3nP	popírat
historický	historický	k2eAgInSc4d1	historický
vztah	vztah	k1gInSc4	vztah
židů	žid	k1gMnPc2	žid
k	k	k7c3	k
Chrámové	chrámový	k2eAgFnSc3d1	chrámová
hoře	hora	k1gFnSc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
návštěva	návštěva	k1gFnSc1	návštěva
Chrámové	chrámový	k2eAgFnSc2d1	chrámová
hory	hora	k1gFnSc2	hora
Arielem	Ariel	k1gMnSc7	Ariel
Šaronem	Šaron	k1gMnSc7	Šaron
využita	využít	k5eAaPmNgFnS	využít
palestinskou	palestinský	k2eAgFnSc7d1	palestinská
propagandou	propaganda	k1gFnSc7	propaganda
jako	jako	k8xS	jako
záminka	záminka	k1gFnSc1	záminka
k	k	k7c3	k
rozpoutání	rozpoutání	k1gNnSc3	rozpoutání
druhé	druhý	k4xOgFnSc2	druhý
intifády	intifáda	k1gFnSc2	intifáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Čejka	Čejka	k1gMnSc1	Čejka
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
Stát	stát	k1gInSc1	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
3007	[number]	k4	3007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FLAVIUS	FLAVIUS	kA	FLAVIUS
<g/>
,	,	kIx,	,
Iosephus	Iosephus	k1gInSc1	Iosephus
<g/>
.	.	kIx.	.
</s>
<s>
Dobytí	dobytí	k1gNnSc1	dobytí
Jerusalema	Jerusalem	k1gInSc2	Jerusalem
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Romportl	Romportl	k1gMnSc1	Romportl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1919	[number]	k4	1919
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
175	[number]	k4	175
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Segert	Segert	k1gMnSc1	Segert
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Starověké	starověký	k2eAgFnPc4d1	starověká
dějiny	dějiny	k1gFnPc4	dějiny
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schäfer	Schäfer	k1gMnSc1	Schäfer
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Dějiny	dějiny	k1gFnPc1	dějiny
židů	žid	k1gMnPc2	žid
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
od	od	k7c2	od
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
po	po	k7c4	po
arabskou	arabský	k2eAgFnSc4d1	arabská
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
633	[number]	k4	633
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
</s>
</p>
<p>
<s>
Starověké	starověký	k2eAgFnPc1d1	starověká
dějiny	dějiny	k1gFnPc1	dějiny
Židů	Žid	k1gMnPc2	Žid
</s>
</p>
<p>
<s>
Babylonské	babylonský	k2eAgNnSc1d1	babylonské
zajetí	zajetí	k1gNnSc1	zajetí
</s>
</p>
<p>
<s>
Herodes	Herodes	k1gMnSc1	Herodes
Veliký	veliký	k2eAgMnSc1d1	veliký
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
židovská	židovský	k2eAgFnSc1d1	židovská
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jeruzalémský	jeruzalémský	k2eAgInSc4d1	jeruzalémský
chrám	chrám	k1gInSc4	chrám
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
