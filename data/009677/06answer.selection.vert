<s>
Podobu	podoba	k1gFnSc4	podoba
prvotního	prvotní	k2eAgNnSc2d1	prvotní
chrámového	chrámový	k2eAgNnSc2d1	chrámové
vybavení	vybavení	k1gNnSc2	vybavení
můžeme	moct	k5eAaImIp1nP	moct
jen	jen	k9	jen
odhadovat	odhadovat	k5eAaImF	odhadovat
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
odkázáni	odkázat	k5eAaPmNgMnP	odkázat
jen	jen	k9	jen
na	na	k7c4	na
slovní	slovní	k2eAgInSc4d1	slovní
popis	popis	k1gInSc4	popis
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
vybavení	vybavení	k1gNnSc2	vybavení
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
zničení	zničení	k1gNnSc6	zničení
Chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
