<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Brebera	Breber	k1gMnSc2	Breber
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Přelouč	Přelouč	k1gFnSc1	Přelouč
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
chemik	chemik	k1gMnSc1	chemik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
československém	československý	k2eAgInSc6d1	československý
státním	státní	k2eAgInSc6d1	státní
Výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
ústavu	ústav	k1gInSc6	ústav
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
při	při	k7c6	při
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Explosii	Explosie	k1gFnSc6	Explosie
Pardubice	Pardubice	k1gInPc4	Pardubice
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Radimem	Radim	k1gMnSc7	Radim
Fukátkem	Fukátek	k1gInSc7	Fukátek
plastickou	plastický	k2eAgFnSc4d1	plastická
trhavinu	trhavina	k1gFnSc4	trhavina
Semtex	semtex	k1gInSc1	semtex
<g/>
.	.	kIx.	.
</s>

