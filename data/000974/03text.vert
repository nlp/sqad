<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jiří	Jiří	k1gMnSc1	Jiří
(	(	kIx(	(
<g/>
staročesky	staročesky	k6eAd1	staročesky
Juří	Juří	k2eAgInSc1d1	Juří
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
hagiografie	hagiografie	k1gFnSc2	hagiografie
římský	římský	k2eAgMnSc1d1	římský
voják	voják	k1gMnSc1	voják
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
,	,	kIx,	,
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
uctíván	uctívat	k5eAaImNgMnS	uctívat
jako	jako	k9	jako
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc2d1	východní
církve	církev	k1gFnSc2	církev
jej	on	k3xPp3gMnSc4	on
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
megalomartyra	megalomartyr	k1gMnSc4	megalomartyr
<g/>
,	,	kIx,	,
arcimučedníka	arcimučedník	k1gMnSc4	arcimučedník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
čtrnácti	čtrnáct	k4xCc2	čtrnáct
svatých	svatá	k1gFnPc2	svatá
pomocníků	pomocník	k1gMnPc2	pomocník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zvěčněn	zvěčněn	k2eAgMnSc1d1	zvěčněn
v	v	k7c6	v
"	"	kIx"	"
<g/>
legendě	legenda	k1gFnSc6	legenda
o	o	k7c6	o
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
draku	drak	k1gInSc6	drak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vylíčen	vylíčen	k2eAgMnSc1d1	vylíčen
jako	jako	k8xC	jako
drakobijce	drakobijce	k1gMnSc1	drakobijce
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jiří	Jiří	k1gMnSc1	Jiří
je	být	k5eAaImIp3nS	být
patronem	patron	k1gMnSc7	patron
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc2	Gruzie
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Katalánska	Katalánsko	k1gNnSc2	Katalánsko
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
měst	město	k1gNnPc2	město
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
,	,	kIx,	,
Bejrútu	Bejrút	k1gInSc2	Bejrút
<g/>
,	,	kIx,	,
Freiburgu	Freiburg	k1gInSc2	Freiburg
<g/>
,	,	kIx,	,
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
,	,	kIx,	,
Janova	Janov	k1gInSc2	Janov
<g/>
,	,	kIx,	,
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
Lublaně	Lublaň	k1gFnSc2	Lublaň
<g/>
,	,	kIx,	,
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
také	také	k9	také
mnoha	mnoho	k4c2	mnoho
profesí	profes	k1gFnPc2	profes
<g/>
,	,	kIx,	,
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
nemocných	mocný	k2eNgMnPc2d1	nemocný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
je	být	k5eAaImIp3nS	být
patronem	patron	k1gMnSc7	patron
skautů	skaut	k1gMnPc2	skaut
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
svátek	svátek	k1gInSc1	svátek
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
velkých	velký	k2eAgInPc2d1	velký
skautských	skautský	k2eAgInPc2d1	skautský
svátků	svátek	k1gInPc2	svátek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
svátek	svátek	k1gInSc4	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
slaví	slavit	k5eAaImIp3nS	slavit
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
zmiňován	zmiňovat	k5eAaImNgInS	zmiňovat
v	v	k7c6	v
žádných	žádný	k3yNgInPc6	žádný
historických	historický	k2eAgInPc6d1	historický
dokumentech	dokument	k1gInPc6	dokument
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
významnou	významný	k2eAgFnSc4d1	významná
historickou	historický	k2eAgFnSc4d1	historická
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhle	rozsáhle	k6eAd1	rozsáhle
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
referuje	referovat	k5eAaBmIp3nS	referovat
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
legenda	legenda	k1gFnSc1	legenda
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedoloženosti	nedoloženost	k1gFnSc2	nedoloženost
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
vyňat	vynít	k5eAaPmNgInS	vynít
z	z	k7c2	z
katolického	katolický	k2eAgInSc2d1	katolický
Všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
navrácen	navrátit	k5eAaPmNgInS	navrátit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Jiří	Jiří	k1gMnSc1	Jiří
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
rodině	rodina	k1gFnSc6	rodina
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
270	[number]	k4	270
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Kappadokii	Kappadokie	k1gFnSc6	Kappadokie
<g/>
,	,	kIx,	,
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
pocházel	pocházet	k5eAaImAgMnS	pocházet
také	také	k9	také
z	z	k7c2	z
Kappadokie	Kappadokie	k1gFnSc2	Kappadokie
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
důstojník	důstojník	k1gMnSc1	důstojník
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
Lyddy	Lydda	k1gFnSc2	Lydda
(	(	kIx(	(
<g/>
Lýdie	Lýdie	k1gFnSc1	Lýdie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Palestina	Palestina	k1gFnSc1	Palestina
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Lod	Lod	k1gFnSc1	Lod
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
vdova	vdova	k1gFnSc1	vdova
se	se	k3xPyFc4	se
po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
rodného	rodný	k2eAgNnSc2d1	rodné
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Jiří	Jiří	k1gMnSc1	Jiří
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
otce	otec	k1gMnSc2	otec
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgInS	dokázat
být	být	k5eAaImF	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
vojákem	voják	k1gMnSc7	voják
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
povyšován	povyšovat	k5eAaImNgInS	povyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svých	svůj	k3xOyFgFnPc6	svůj
dvacetinách	dvacetina	k1gFnPc6	dvacetina
obdržel	obdržet	k5eAaPmAgInS	obdržet
titul	titul	k1gInSc1	titul
Tribuna	tribun	k1gMnSc2	tribun
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rytířem	rytíř	k1gMnSc7	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
Nikomédii	Nikomédie	k1gFnSc6	Nikomédie
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
osobní	osobní	k2eAgFnSc2d1	osobní
stráže	stráž	k1gFnSc2	stráž
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
Diokleciána	Dioklecián	k1gMnSc2	Dioklecián
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
303	[number]	k4	303
vydal	vydat	k5eAaPmAgMnS	vydat
císař	císař	k1gMnSc1	císař
edikty	edikt	k1gInPc4	edikt
umožňující	umožňující	k2eAgNnSc4d1	umožňující
pronásledování	pronásledování	k1gNnSc4	pronásledování
a	a	k8xC	a
útisk	útisk	k1gInSc4	útisk
křesťanů	křesťan	k1gMnPc2	křesťan
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jiřímu	Jiří	k1gMnSc3	Jiří
bylo	být	k5eAaImAgNnS	být
nařízeno	nařízen	k2eAgNnSc1d1	nařízeno
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
perzekucí	perzekuce	k1gFnPc2	perzekuce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
namísto	namísto	k7c2	namísto
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
Jiří	Jiří	k1gMnSc1	Jiří
přiznal	přiznat	k5eAaPmAgMnS	přiznat
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
a	a	k8xC	a
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
císařské	císařský	k2eAgNnSc4d1	císařské
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Rozzuřený	rozzuřený	k2eAgMnSc1d1	rozzuřený
Dioklecián	Dioklecián	k1gMnSc1	Dioklecián
ho	on	k3xPp3gInSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
mučit	mučit	k5eAaImF	mučit
za	za	k7c4	za
zradu	zrada	k1gFnSc4	zrada
a	a	k8xC	a
poté	poté	k6eAd1	poté
nařídil	nařídit	k5eAaPmAgMnS	nařídit
popravu	poprava	k1gFnSc4	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
různých	různý	k2eAgNnPc6d1	různé
mučeních	mučení	k1gNnPc6	mučení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
trháním	trhání	k1gNnPc3	trhání
kolem	kolem	k6eAd1	kolem
s	s	k7c7	s
hřeby	hřeb	k1gInPc7	hřeb
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
popraven	popraven	k2eAgMnSc1d1	popraven
stětím	stětit	k5eAaImIp1nS	stětit
před	před	k7c7	před
Nikomédijskou	Nikomédijský	k2eAgFnSc7d1	Nikomédijský
obrannou	obranný	k2eAgFnSc7d1	obranná
zdí	zeď	k1gFnSc7	zeď
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
303	[number]	k4	303
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
skutek	skutek	k1gInSc1	skutek
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
císařovnu	císařovna	k1gFnSc4	císařovna
Alexandru	Alexandr	k1gMnSc3	Alexandr
a	a	k8xC	a
pohanského	pohanský	k2eAgMnSc2d1	pohanský
kněze	kněz	k1gMnSc2	kněz
Athanasia	Athanasius	k1gMnSc2	Athanasius
k	k	k7c3	k
přestoupení	přestoupení	k1gNnSc3	přestoupení
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
navráceno	navrátit	k5eAaPmNgNnS	navrátit
do	do	k7c2	do
Lýdie	Lýdia	k1gFnSc2	Lýdia
na	na	k7c4	na
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
mu	on	k3xPp3gNnSc3	on
brzy	brzy	k6eAd1	brzy
začali	začít	k5eAaPmAgMnP	začít
chodit	chodit	k5eAaImF	chodit
vzdávat	vzdávat	k5eAaImF	vzdávat
úctu	úcta	k1gFnSc4	úcta
křesťané	křesťan	k1gMnPc1	křesťan
jako	jako	k8xC	jako
mučedníkovi	mučedníkův	k2eAgMnPc1d1	mučedníkův
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c4	o
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
drakovi	drak	k1gMnSc3	drak
je	být	k5eAaImIp3nS	být
východního	východní	k2eAgInSc2d1	východní
původu	původ	k1gInSc2	původ
<g/>
;	;	kIx,	;
do	do	k7c2	do
našich	náš	k3xOp1gFnPc2	náš
končin	končina	k1gFnPc2	končina
byl	být	k5eAaImAgInS	být
přinesen	přinesen	k2eAgInSc1d1	přinesen
Křižáky	křižák	k1gMnPc7	křižák
a	a	k8xC	a
upraven	upravit	k5eAaPmNgMnS	upravit
do	do	k7c2	do
romantické	romantický	k2eAgFnSc2d1	romantická
podoby	podoba	k1gFnSc2	podoba
(	(	kIx(	(
<g/>
Loomis	Loomis	k1gInSc1	Loomis
<g/>
;	;	kIx,	;
Whatley	Whatlea	k1gFnPc1	Whatlea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
verze	verze	k1gFnSc1	verze
tohoto	tento	k3xDgInSc2	tento
námětu	námět	k1gInSc2	námět
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
jedenáctého	jedenáctý	k4xOgNnSc2	jedenáctý
století	století	k1gNnSc2	století
z	z	k7c2	z
Kapadokie	Kapadokie	k1gFnSc2	Kapadokie
(	(	kIx(	(
<g/>
Whately	Whatela	k1gFnSc2	Whatela
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
v	v	k7c6	v
ikonografii	ikonografie	k1gFnSc6	ikonografie
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
zobrazován	zobrazovat	k5eAaImNgMnS	zobrazovat
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
až	až	k6eAd1	až
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
epický	epický	k2eAgInSc1d1	epický
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
Georgiánský	Georgiánský	k2eAgInSc1d1	Georgiánský
text	text	k1gInSc1	text
(	(	kIx(	(
<g/>
Whatley	Whatlea	k1gFnPc1	Whatlea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
západní	západní	k2eAgFnSc1d1	západní
(	(	kIx(	(
<g/>
katolická	katolický	k2eAgFnSc1d1	katolická
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
verze	verze	k1gFnSc1	verze
legendy	legenda	k1gFnSc2	legenda
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
u	u	k7c2	u
pramene	pramen	k1gInSc2	pramen
zásobujícího	zásobující	k2eAgInSc2d1	zásobující
město	město	k1gNnSc1	město
Kyrény	Kyréna	k1gFnPc1	Kyréna
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
město	město	k1gNnSc4	město
Lýdii	Lýdia	k1gFnSc4	Lýdia
(	(	kIx(	(
<g/>
Lyddu	Lydda	k1gFnSc4	Lydda
<g/>
;	;	kIx,	;
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
zdroji	zdroj	k1gInSc6	zdroj
<g/>
)	)	kIx)	)
postavil	postavit	k5eAaPmAgMnS	postavit
hnízdo	hnízdo	k1gNnSc4	hnízdo
drak	drak	k1gInSc1	drak
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
museli	muset	k5eAaImAgMnP	muset
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
kvůli	kvůli	k7c3	kvůli
vodě	voda	k1gFnSc3	voda
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
vylákat	vylákat	k5eAaPmF	vylákat
draka	drak	k1gMnSc4	drak
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
návnada	návnada	k1gFnSc1	návnada
sloužila	sloužit	k5eAaImAgFnS	sloužit
lidská	lidský	k2eAgFnSc1d1	lidská
oběť	oběť	k1gFnSc1	oběť
určená	určený	k2eAgFnSc1d1	určená
losem	los	k1gInSc7	los
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
los	los	k1gInSc1	los
padl	padnout	k5eAaImAgInS	padnout
na	na	k7c4	na
královskou	královský	k2eAgFnSc4d1	královská
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
zoufalý	zoufalý	k2eAgMnSc1d1	zoufalý
král	král	k1gMnSc1	král
marně	marně	k6eAd1	marně
prosil	prosít	k5eAaPmAgMnS	prosít
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Princezna	princezna	k1gFnSc1	princezna
byla	být	k5eAaImAgFnS	být
předložena	předložit	k5eAaPmNgFnS	předložit
drakovi	drakův	k2eAgMnPc1d1	drakův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pravou	pravý	k2eAgFnSc4d1	pravá
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
zrovna	zrovna	k6eAd1	zrovna
kolem	kolem	k6eAd1	kolem
projíždějící	projíždějící	k2eAgFnSc1d1	projíždějící
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
drakovi	drak	k1gMnSc3	drak
<g/>
,	,	kIx,	,
zabil	zabít	k5eAaPmAgMnS	zabít
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
princeznu	princezna	k1gFnSc4	princezna
zachránil	zachránit	k5eAaPmAgMnS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Vděční	vděčný	k2eAgMnPc1d1	vděčný
občané	občan	k1gMnPc1	občan
se	se	k3xPyFc4	se
zřekli	zřeknout	k5eAaPmAgMnP	zřeknout
pohanství	pohanství	k1gNnSc2	pohanství
a	a	k8xC	a
přijali	přijmout	k5eAaPmAgMnP	přijmout
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Podobnost	podobnost	k1gFnSc1	podobnost
s	s	k7c7	s
příběhem	příběh	k1gInSc7	příběh
o	o	k7c6	o
Perseovi	Perseus	k1gMnSc6	Perseus
a	a	k8xC	a
Andromedě	Andromeda	k1gFnSc6	Andromeda
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
nápadná	nápadný	k2eAgFnSc1d1	nápadná
<g/>
.	.	kIx.	.
</s>
<s>
Drak	drak	k1gInSc1	drak
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
alegorií	alegorie	k1gFnPc2	alegorie
na	na	k7c4	na
pohanské	pohanský	k2eAgInPc4d1	pohanský
kulty	kult	k1gInPc4	kult
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c4	o
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
drakovi	drak	k1gMnSc3	drak
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
starší	starý	k2eAgInSc1d2	starší
předkřesťanský	předkřesťanský	k2eAgInSc1d1	předkřesťanský
vzor	vzor	k1gInSc1	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
Sabazios	Sabazios	k1gInSc4	Sabazios
<g/>
,	,	kIx,	,
nebeský	nebeský	k2eAgMnSc1d1	nebeský
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
obvykle	obvykle	k6eAd1	obvykle
zobrazován	zobrazovat	k5eAaImNgInS	zobrazovat
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
koně	kůň	k1gMnPc1	kůň
nebo	nebo	k8xC	nebo
Diovo	Diův	k2eAgNnSc1d1	Diovo
vítězství	vítězství	k1gNnSc1	vítězství
nad	nad	k7c7	nad
stohlavým	stohlavý	k2eAgMnSc7d1	stohlavý
titánem	titán	k1gMnSc7	titán
Týfónem	Týfón	k1gMnSc7	Týfón
<g/>
,	,	kIx,	,
či	či	k8xC	či
další	další	k2eAgFnPc1d1	další
shody	shoda	k1gFnPc1	shoda
s	s	k7c7	s
Germánskými	germánský	k2eAgFnPc7d1	germánská
a	a	k8xC	a
Védskými	védský	k2eAgFnPc7d1	védská
tradicemi	tradice	k1gFnPc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňované	zmiňovaný	k2eAgFnSc2d1	zmiňovaná
shody	shoda	k1gFnSc2	shoda
vedly	vést	k5eAaImAgInP	vést
mnoho	mnoho	k4c4	mnoho
historiků	historik	k1gMnPc2	historik
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
legenda	legenda	k1gFnSc1	legenda
o	o	k7c4	o
sv.	sv.	kA	sv.
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
je	být	k5eAaImIp3nS	být
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
verzí	verze	k1gFnSc7	verze
příběhu	příběh	k1gInSc2	příběh
ze	z	k7c2	z
starších	starý	k2eAgNnPc2d2	starší
indoevropských	indoevropský	k2eAgNnPc2d1	indoevropské
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověkých	středověký	k2eAgFnPc6d1	středověká
romancích	romance	k1gFnPc6	romance
je	být	k5eAaImIp3nS	být
kopí	kopí	k1gNnSc1	kopí
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
Jiří	Jiří	k1gMnSc1	Jiří
zabil	zabít	k5eAaPmAgMnS	zabít
draka	drak	k1gMnSc4	drak
<g/>
,	,	kIx,	,
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Ascalon	Ascalon	k1gInSc1	Ascalon
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Aškelon	Aškelon	k1gInSc1	Aškelon
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
se	se	k3xPyFc4	se
zachráněná	zachráněný	k2eAgFnSc1d1	zachráněná
princezna	princezna	k1gFnSc1	princezna
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
Švédského	švédský	k2eAgNnSc2d1	švédské
království	království	k1gNnSc2	království
a	a	k8xC	a
drak	drak	k1gInSc1	drak
obrazem	obraz	k1gInSc7	obraz
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
soch	socha	k1gFnPc2	socha
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
bojujícího	bojující	k2eAgMnSc2d1	bojující
s	s	k7c7	s
drakem	drak	k1gMnSc7	drak
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
městě	město	k1gNnSc6	město
v	v	k7c6	v
Storkyrkanu	Storkyrkan	k1gInSc6	Storkyrkan
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Velký	velký	k2eAgInSc1d1	velký
kostel	kostel	k1gInSc1	kostel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
Herman	Herman	k1gMnSc1	Herman
Melville	Melville	k1gNnSc4	Melville
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
zápasu	zápas	k1gInSc6	zápas
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
s	s	k7c7	s
drakem	drak	k1gInSc7	drak
i	i	k8xC	i
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
Perseovi	Perseus	k1gMnSc6	Perseus
a	a	k8xC	a
Andromedě	Andromeda	k1gFnSc6	Andromeda
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
původ	původ	k1gInSc4	původ
v	v	k7c6	v
lovu	lov	k1gInSc6	lov
na	na	k7c4	na
velrybu	velryba	k1gFnSc4	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Doslova	doslova	k6eAd1	doslova
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
Jméno	jméno	k1gNnSc1	jméno
Jiří	Jiří	k1gMnSc2	Jiří
pochází	pocházet	k5eAaImIp3nS	pocházet
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
latinsky	latinsky	k6eAd1	latinsky
Georgius	Georgius	k1gInSc4	Georgius
<g/>
)	)	kIx)	)
z	z	k7c2	z
řecky	řecky	k6eAd1	řecky
Georgios	Georgios	k1gMnSc1	Georgios
"	"	kIx"	"
<g/>
rolník	rolník	k1gMnSc1	rolník
<g/>
,	,	kIx,	,
zemědělec	zemědělec	k1gMnSc1	zemědělec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ge	Ge	k1gFnSc1	Ge
-	-	kIx~	-
země	země	k1gFnSc1	země
a	a	k8xC	a
ergon	ergon	k1gInSc1	ergon
-	-	kIx~	-
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gruzie	Gruzie	k1gFnSc1	Gruzie
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
řekne	říct	k5eAaPmIp3nS	říct
Georgia	Georgia	k1gFnSc1	Georgia
<g/>
/	/	kIx~	/
<g/>
Georgie	Georgie	k1gFnSc1	Georgie
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
vzniku	vznik	k1gInSc2	vznik
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
je	být	k5eAaImIp3nS	být
pojmenování	pojmenování	k1gNnSc4	pojmenování
po	po	k7c6	po
svatém	svatý	k2eAgMnSc6d1	svatý
Jiří	Jiří	k1gMnSc6	Jiří
(	(	kIx(	(
<g/>
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
gruzínsky	gruzínsky	k6eAd1	gruzínsky
zní	znět	k5eAaImIp3nS	znět
ს	ს	k?	ს
<g/>
,	,	kIx,	,
Sakartvelo	Sakartvela	k1gFnSc5	Sakartvela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Svatý	svatý	k2eAgMnSc1d1	svatý
Jiří	Jiří	k1gMnSc1	Jiří
BUBEN	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
M.	M.	kA	M.
<g/>
;	;	kIx,	;
KUČERA	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
;	;	kIx,	;
KUKLA	Kukla	k1gMnSc1	Kukla
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
A.	A.	kA	A.
<g/>
.	.	kIx.	.
</s>
<s>
Svatí	svatý	k1gMnPc1	svatý
spojují	spojovat	k5eAaImIp3nP	spojovat
národy	národ	k1gInPc4	národ
:	:	kIx,	:
portréty	portrét	k1gInPc4	portrét
evropských	evropský	k2eAgMnPc2d1	evropský
světců	světec	k1gMnPc2	světec
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
rozš	rozš	k1gInSc1	rozš
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Panevropa	Panevropa	k1gFnSc1	Panevropa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
195	[number]	k4	195
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85846	[number]	k4	85846
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Brooks	Brooks	k1gInSc1	Brooks
<g/>
,	,	kIx,	,
E.W.	E.W.	k1gFnSc1	E.W.
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Acts	Acts	k1gInSc1	Acts
of	of	k?	of
Saint	Saint	k1gInSc1	Saint
George	Georg	k1gMnSc2	Georg
in	in	k?	in
series	series	k1gInSc1	series
Analecta	Analect	k1gMnSc2	Analect
Gorgiana	Gorgian	k1gMnSc2	Gorgian
8	[number]	k4	8
(	(	kIx(	(
<g/>
Gorgias	Gorgias	k1gInSc1	Gorgias
Press	Press	k1gInSc1	Press
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Burgoyne	Burgoynout	k5eAaPmIp3nS	Burgoynout
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
H.	H.	kA	H.
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Chronological	Chronological	k1gMnSc1	Chronological
Index	index	k1gInSc1	index
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Muslim	muslim	k1gMnSc1	muslim
Monuments	Monuments	k1gInSc1	Monuments
of	of	k?	of
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
The	The	k1gMnSc5	The
Architecture	Architectur	k1gMnSc5	Architectur
of	of	k?	of
Islamic	Islamic	k1gMnSc1	Islamic
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
<g/>
.	.	kIx.	.
</s>
<s>
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
British	British	k1gMnSc1	British
School	School	k1gInSc1	School
of	of	k?	of
Archaeology	Archaeolog	k1gMnPc4	Archaeolog
in	in	k?	in
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
<g/>
.	.	kIx.	.
</s>
<s>
Alban	Alban	k1gMnSc1	Alban
Butler	Butler	k1gMnSc1	Butler
<g/>
,	,	kIx,	,
Butler	Butler	k1gMnSc1	Butler
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lives	Lives	k1gInSc1	Lives
of	of	k?	of
the	the	k?	the
Saints	Saints	k1gInSc1	Saints
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
148	[number]	k4	148
<g/>
-	-	kIx~	-
<g/>
150	[number]	k4	150
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
George	George	k1gInSc1	George
<g/>
,	,	kIx,	,
Martyr	martyr	k1gMnSc1	martyr
<g/>
,	,	kIx,	,
Protector	Protector	k1gMnSc1	Protector
of	of	k?	of
the	the	k?	the
Kingdom	Kingdom	k1gInSc1	Kingdom
of	of	k?	of
England	England	k1gInSc1	England
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
on-line	onin	k1gInSc5	on-lin
text	text	k1gInSc1	text
<g/>
)	)	kIx)	)
Gabidzashvili	Gabidzashvili	k1gFnSc1	Gabidzashvili
<g/>
,	,	kIx,	,
Enriko	Enrika	k1gFnSc5	Enrika
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Saint	Saint	k1gMnSc1	Saint
George	Georg	k1gMnSc2	Georg
<g/>
:	:	kIx,	:
In	In	k1gMnSc1	In
Ancent	Ancent	k1gMnSc1	Ancent
Georgian	Georgian	k1gMnSc1	Georgian
Literature	Literatur	k1gInSc5	Literatur
<g/>
.	.	kIx.	.
</s>
<s>
Armazi	Armah	k1gMnPc1	Armah
-	-	kIx~	-
89	[number]	k4	89
<g/>
:	:	kIx,	:
Tbilisi	Tbilisi	k1gNnSc2	Tbilisi
<g/>
,	,	kIx,	,
Georgia	Georgium	k1gNnSc2	Georgium
<g/>
.	.	kIx.	.
</s>
<s>
Loomis	Loomis	k1gFnSc1	Loomis
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
White	White	k5eAaPmIp2nP	White
Magic	Magic	k1gMnSc1	Magic
<g/>
,	,	kIx,	,
An	An	k1gMnSc1	An
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Folklore	folklor	k1gInSc5	folklor
of	of	k?	of
Christian	Christian	k1gMnSc1	Christian
Legend	legenda	k1gFnPc2	legenda
(	(	kIx(	(
<g/>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Medieval	medieval	k1gInSc1	medieval
Society	societa	k1gFnSc2	societa
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
)	)	kIx)	)
Natsheh	Natsheh	k1gMnSc1	Natsheh
<g/>
,	,	kIx,	,
Yusuf	Yusuf	k1gMnSc1	Yusuf
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Architectural	Architectural	k1gFnSc1	Architectural
survey	survea	k1gFnSc2	survea
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
in	in	k?	in
Ottoman	Ottoman	k1gMnSc1	Ottoman
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Living	Living	k1gInSc1	Living
City	City	k1gFnSc1	City
1517	[number]	k4	1517
<g/>
-	-	kIx~	-
<g/>
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Edited	Edited	k1gInSc4	Edited
by	by	kYmCp3nS	by
Sylvia	Sylvia	k1gFnSc1	Sylvia
Auld	Aulda	k1gFnPc2	Aulda
and	and	k?	and
Robert	Robert	k1gMnSc1	Robert
Hillenbrand	Hillenbrand	k1gInSc1	Hillenbrand
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Altajir	Altajir	k1gMnSc1	Altajir
World	World	k1gMnSc1	World
of	of	k?	of
Islam	Islam	k1gInSc1	Islam
Trust	trust	k1gInSc1	trust
<g/>
)	)	kIx)	)
pp	pp	k?	pp
893	[number]	k4	893
<g/>
-	-	kIx~	-
<g/>
899	[number]	k4	899
<g/>
.	.	kIx.	.
</s>
<s>
Whatley	Whatley	k1gInPc1	Whatley
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Gordon	Gordon	k1gInSc1	Gordon
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
<g/>
,	,	kIx,	,
with	with	k1gMnSc1	with
Anne	Ann	k1gFnSc2	Ann
B.	B.	kA	B.
Thompson	Thompson	k1gMnSc1	Thompson
and	and	k?	and
Robert	Robert	k1gMnSc1	Robert
K.	K.	kA	K.
Upchurch	Upchurch	k1gMnSc1	Upchurch
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
St.	st.	kA	st.
George	George	k1gFnSc1	George
and	and	k?	and
the	the	k?	the
Dragon	Dragon	k1gMnSc1	Dragon
in	in	k?	in
the	the	k?	the
South	South	k1gInSc1	South
English	Englisha	k1gFnPc2	Englisha
Legendary	Legendara	k1gFnSc2	Legendara
(	(	kIx(	(
<g/>
East	East	k2eAgInSc1d1	East
Midland	Midland	k1gInSc1	Midland
Revision	Revision	k1gInSc1	Revision
<g/>
,	,	kIx,	,
c.	c.	k?	c.
1400	[number]	k4	1400
<g/>
)	)	kIx)	)
Originally	Originalla	k1gFnSc2	Originalla
published	published	k1gInSc1	published
in	in	k?	in
Saints	Saints	k1gInSc1	Saints
<g/>
'	'	kIx"	'
Lives	Lives	k1gMnSc1	Lives
in	in	k?	in
Middle	Middle	k1gMnSc1	Middle
English	English	k1gMnSc1	English
Collections	Collectionsa	k1gFnPc2	Collectionsa
Kalamazoo	Kalamazoo	k1gMnSc1	Kalamazoo
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
:	:	kIx,	:
Medieval	medieval	k1gInSc1	medieval
Institute	institut	k1gInSc5	institut
Publications	Publications	k1gInSc1	Publications
(	(	kIx(	(
<g/>
On-line	Onin	k1gInSc5	On-lin
Introduction	Introduction	k1gInSc1	Introduction
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svatý	svatý	k1gMnSc1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
