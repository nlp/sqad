<s>
Aleš	Aleš	k1gMnSc1	Aleš
Valenta	Valenta	k1gMnSc1	Valenta
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
moderátor	moderátor	k1gMnSc1	moderátor
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
akrobatický	akrobatický	k2eAgMnSc1d1	akrobatický
lyžař	lyžař	k1gMnSc1	lyžař
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
akrobatických	akrobatický	k2eAgInPc6d1	akrobatický
skocích	skok	k1gInPc6	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
skočil	skočit	k5eAaPmAgMnS	skočit
trojité	trojitý	k2eAgNnSc4d1	trojité
salto	salto	k1gNnSc4	salto
s	s	k7c7	s
pěti	pět	k4xCc2	pět
vruty	vrut	k1gInPc7	vrut
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
je	být	k5eAaImIp3nS	být
moderátorka	moderátorka	k1gFnSc1	moderátorka
a	a	k8xC	a
modelka	modelka	k1gFnSc1	modelka
Elen	Elena	k1gFnPc2	Elena
Černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Startoval	startovat	k5eAaBmAgInS	startovat
na	na	k7c6	na
ZOH	ZOH	kA	ZOH
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
1998	[number]	k4	1998
skončil	skončit	k5eAaPmAgInS	skončit
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
stupni	stupeň	k1gInPc7	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
<g/>
,	,	kIx,	,
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	city	k1gNnSc1	city
2002	[number]	k4	2002
olympijský	olympijský	k2eAgInSc4d1	olympijský
závod	závod	k1gInSc4	závod
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
a	a	k8xC	a
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
2006	[number]	k4	2006
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
dvanáctkrát	dvanáctkrát	k6eAd1	dvanáctkrát
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tři	tři	k4xCgInPc4	tři
závody	závod	k1gInPc4	závod
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
ukončil	ukončit	k5eAaPmAgMnS	ukončit
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Uváděl	uvádět	k5eAaImAgMnS	uvádět
předávání	předávání	k1gNnSc4	předávání
cen	cena	k1gFnPc2	cena
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
tuto	tento	k3xDgFnSc4	tento
anketu	anketa	k1gFnSc4	anketa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
sám	sám	k3xTgMnSc1	sám
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
talk	talk	k6eAd1	talk
show	show	k1gNnSc7	show
Sportbar	Sportbara	k1gFnPc2	Sportbara
na	na	k7c6	na
rádiu	rádius	k1gInSc6	rádius
Frekvence	frekvence	k1gFnSc2	frekvence
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
moderátorem	moderátor	k1gMnSc7	moderátor
pořadů	pořad	k1gInPc2	pořad
Auto	auto	k1gNnSc1	auto
Moto	moto	k1gNnSc1	moto
Revue	revue	k1gFnSc2	revue
a	a	k8xC	a
Lvíčata	lvíče	k1gNnPc4	lvíče
vysílaných	vysílaný	k2eAgInPc2d1	vysílaný
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Vystavěl	vystavět	k5eAaPmAgMnS	vystavět
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
sportovní	sportovní	k2eAgInSc4d1	sportovní
komplex	komplex	k1gInSc4	komplex
Acrobat	Acrobat	k1gMnPc2	Acrobat
Park	park	k1gInSc1	park
ve	v	k7c6	v
Štítech	štít	k1gInPc6	štít
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
trénink	trénink	k1gInSc4	trénink
a	a	k8xC	a
závody	závod	k1gInPc4	závod
v	v	k7c6	v
akrobatickém	akrobatický	k2eAgNnSc6d1	akrobatické
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
,	,	kIx,	,
snowboardingu	snowboarding	k1gInSc6	snowboarding
i	i	k8xC	i
další	další	k2eAgFnPc4d1	další
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1998	[number]	k4	1998
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2002	[number]	k4	2002
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc7	Lake
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k6eAd1	místo
Světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g />
.	.	kIx.	.
</s>
<s>
<g/>
místo	místo	k6eAd1	místo
Světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k6eAd1	místo
Světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k6eAd1	místo
Světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Aleš	Aleš	k1gMnSc1	Aleš
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
,	,	kIx,	,
fis-ski	fiskit	k5eAaPmRp2nS	fis-skit
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Aleš	Aleš	k1gMnSc1	Aleš
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
,	,	kIx,	,
sports-reference	sportseference	k1gFnSc1	sports-reference
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Zlato	zlato	k1gNnSc1	zlato
za	za	k7c4	za
světový	světový	k2eAgInSc4d1	světový
unikát	unikát	k1gInSc4	unikát
<g/>
,	,	kIx,	,
stream	stream	k1gInSc4	stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
