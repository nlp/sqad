<p>
<s>
Bouldering	Bouldering	k1gInSc1	Bouldering
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
lezení	lezení	k1gNnSc2	lezení
provozovaný	provozovaný	k2eAgInSc1d1	provozovaný
bez	bez	k1gInSc1	bez
lana	lano	k1gNnSc2	lano
na	na	k7c6	na
malých	malý	k2eAgInPc6d1	malý
skalních	skalní	k2eAgInPc6d1	skalní
blocích	blok	k1gInPc6	blok
nebo	nebo	k8xC	nebo
nízkých	nízký	k2eAgFnPc6d1	nízká
skalách	skála	k1gFnPc6	skála
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
slova	slovo	k1gNnSc2	slovo
boulder	bouldra	k1gFnPc2	bouldra
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
balvan	balvan	k1gInSc1	balvan
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
získává	získávat	k5eAaImIp3nS	získávat
velkou	velký	k2eAgFnSc4d1	velká
popularitu	popularita	k1gFnSc4	popularita
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
začínajících	začínající	k2eAgMnPc2d1	začínající
lezců	lezec	k1gMnPc2	lezec
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nenáročnosti	nenáročnost	k1gFnSc3	nenáročnost
na	na	k7c4	na
vybavení	vybavení	k1gNnSc4	vybavení
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Průkopníky	průkopník	k1gMnPc7	průkopník
boulderingu	bouldering	k1gInSc2	bouldering
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Britové	Brit	k1gMnPc1	Brit
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
však	však	k9	však
bouldering	bouldering	k1gInSc1	bouldering
používán	používán	k2eAgInSc1d1	používán
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
složka	složka	k1gFnSc1	složka
tréninkového	tréninkový	k2eAgInSc2d1	tréninkový
plánu	plán	k1gInSc2	plán
horolezců	horolezec	k1gMnPc2	horolezec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
cílem	cíl	k1gInSc7	cíl
přípravy	příprava	k1gFnSc2	příprava
byly	být	k5eAaImAgInP	být
náročnější	náročný	k2eAgInPc1d2	náročnější
výstupy	výstup	k1gInPc1	výstup
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
samostatnému	samostatný	k2eAgInSc3d1	samostatný
sportu	sport	k1gInSc3	sport
se	se	k3xPyFc4	se
boulderingu	bouldering	k1gInSc2	bouldering
poprvé	poprvé	k6eAd1	poprvé
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaImF	věnovat
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
John	John	k1gMnSc1	John
Gill	Gill	k1gMnSc1	Gill
<g/>
,	,	kIx,	,
přední	přední	k2eAgMnSc1d1	přední
gymnasta	gymnasta	k1gMnSc1	gymnasta
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
shledal	shledat	k5eAaPmAgMnS	shledat
"	"	kIx"	"
<g/>
lezení	lezení	k1gNnSc6	lezení
po	po	k7c6	po
šutrech	šutrech	k?	šutrech
<g/>
"	"	kIx"	"
zábavným	zábavný	k2eAgFnPc3d1	zábavná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybavení	vybavení	k1gNnSc1	vybavení
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
provozování	provozování	k1gNnSc3	provozování
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
není	být	k5eNaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
nic	nic	k3yNnSc1	nic
-	-	kIx~	-
stačí	stačit	k5eAaBmIp3nS	stačit
ruce	ruka	k1gFnPc4	ruka
a	a	k8xC	a
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžněji	běžně	k6eAd3	běžně
používaným	používaný	k2eAgNnSc7d1	používané
vybavením	vybavení	k1gNnSc7	vybavení
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
lezečky	lezeček	k1gInPc1	lezeček
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgFnSc4d1	speciální
obuv	obuv	k1gFnSc4	obuv
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zároveň	zároveň	k6eAd1	zároveň
chrání	chránit	k5eAaImIp3nS	chránit
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jemné	jemný	k2eAgInPc4d1	jemný
a	a	k8xC	a
obratné	obratný	k2eAgInPc4d1	obratný
kroky	krok	k1gInPc4	krok
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
přilnavost	přilnavost	k1gFnSc4	přilnavost
ke	k	k7c3	k
skále	skála	k1gFnSc3	skála
</s>
</p>
<p>
<s>
tzv.	tzv.	kA	tzv.
bouldermatka	bouldermatka	k1gFnSc1	bouldermatka
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
měkká	měkký	k2eAgFnSc1d1	měkká
podložka	podložka	k1gFnSc1	podložka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pádu	pád	k1gInSc2	pád
zabrání	zabránit	k5eAaPmIp3nS	zabránit
vážnějšímu	vážní	k2eAgNnSc3d2	vážnější
zranění	zranění	k1gNnSc3	zranění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
pytlík	pytlík	k1gMnSc1	pytlík
s	s	k7c7	s
"	"	kIx"	"
<g/>
magnéziem	magnézium	k1gNnSc7	magnézium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgInSc1d1	chemický
přípravek	přípravek	k1gInSc1	přípravek
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
4	[number]	k4	4
<g/>
MgCO_	MgCO_	k1gFnPc1	MgCO_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
Mg	mg	kA	mg
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
4	[number]	k4	4
<g/>
H_	H_	k1gFnPc2	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
omezující	omezující	k2eAgNnSc4d1	omezující
pocení	pocení	k1gNnSc4	pocení
rukouPoužití	rukouPoužití	k1gNnSc2	rukouPoužití
magnézia	magnézium	k1gNnSc2	magnézium
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
terénů	terén	k1gInPc2	terén
kontroverzní	kontroverzní	k2eAgFnPc1d1	kontroverzní
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
pískovcových	pískovcový	k2eAgFnPc6d1	pískovcová
skalách	skála	k1gFnPc6	skála
je	být	k5eAaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
malý	malý	k2eAgInSc4d1	malý
kobereček	kobereček	k1gInSc4	kobereček
<g/>
,	,	kIx,	,
na	na	k7c4	na
čištění	čištění	k1gNnSc4	čištění
lezeček	lezečka	k1gFnPc2	lezečka
od	od	k7c2	od
bláta	bláto	k1gNnSc2	bláto
a	a	k8xC	a
nečistotjiné	nečistotjiná	k1gFnSc2	nečistotjiná
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
bunda	bunda	k1gFnSc1	bunda
(	(	kIx(	(
<g/>
péřová	péřový	k2eAgFnSc1d1	péřová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
udržuje	udržovat	k5eAaImIp3nS	udržovat
bouldristy	bouldrista	k1gMnPc4	bouldrista
v	v	k7c6	v
teple	teplo	k1gNnSc6	teplo
při	při	k7c6	při
odpočinku	odpočinek	k1gInSc6	odpočinek
mezi	mezi	k7c7	mezi
pokusy	pokus	k1gInPc7	pokus
</s>
</p>
<p>
<s>
kulich	kulich	k1gInSc1	kulich
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
úsměvně	úsměvně	k6eAd1	úsměvně
působí	působit	k5eAaImIp3nS	působit
používání	používání	k1gNnSc4	používání
tohoto	tento	k3xDgInSc2	tento
módního	módní	k2eAgInSc2d1	módní
doplňku	doplněk	k1gInSc2	doplněk
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
speciálně	speciálně	k6eAd1	speciálně
pletou	plést	k5eAaImIp3nP	plést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
bouldering	bouldering	k1gInSc4	bouldering
lepší	dobrý	k2eAgFnSc2d2	lepší
podmínky	podmínka	k1gFnSc2	podmínka
a	a	k8xC	a
hřeje	hřát	k5eAaImIp3nS	hřát
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Obvyklým	obvyklý	k2eAgNnSc7d1	obvyklé
bezpečnostním	bezpečnostní	k2eAgNnSc7d1	bezpečnostní
opatřením	opatření	k1gNnSc7	opatření
je	být	k5eAaImIp3nS	být
jištění	jištění	k1gNnSc4	jištění
další	další	k2eAgFnSc7d1	další
osobou	osoba	k1gFnSc7	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
pomocí	pomocí	k7c2	pomocí
lana	lano	k1gNnSc2	lano
a	a	k8xC	a
jisticí	jisticí	k2eAgFnSc2d1	jisticí
techniky	technika	k1gFnSc2	technika
jako	jako	k8xS	jako
v	v	k7c6	v
horolezectví	horolezectví	k1gNnSc6	horolezectví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jistící	jistící	k2eAgFnSc1d1	jistící
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
přímo	přímo	k6eAd1	přímo
pouze	pouze	k6eAd1	pouze
svýma	svůj	k3xOyFgFnPc7	svůj
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
usměrnit	usměrnit	k5eAaPmF	usměrnit
pád	pád	k1gInSc4	pád
lezce	lezec	k1gMnSc2	lezec
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedopadl	dopadnout	k5eNaPmAgMnS	dopadnout
v	v	k7c6	v
nebezpečné	bezpečný	k2eNgFnSc6d1	nebezpečná
poloze	poloha	k1gFnSc6	poloha
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hlavou	hlava	k1gFnSc7	hlava
na	na	k7c4	na
vyčnívající	vyčnívající	k2eAgInSc4d1	vyčnívající
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nehody	nehoda	k1gFnSc2	nehoda
také	také	k9	také
další	další	k2eAgFnSc1d1	další
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Terén	terén	k1gInSc4	terén
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
z	z	k7c2	z
názvu	název	k1gInSc2	název
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
klasickým	klasický	k2eAgInSc7d1	klasický
terénem	terén	k1gInSc7	terén
boulderingu	bouldering	k1gInSc2	bouldering
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
balvany	balvan	k1gInPc1	balvan
a	a	k8xC	a
menší	malý	k2eAgFnPc1d2	menší
skalky	skalka	k1gFnPc1	skalka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
definice	definice	k1gFnSc2	definice
boulderingu	bouldering	k1gInSc2	bouldering
však	však	k9	však
spadá	spadat	k5eAaImIp3nS	spadat
např.	např.	kA	např.
i	i	k8xC	i
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
traverz	traverza	k1gFnPc2	traverza
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
skalního	skalní	k2eAgInSc2d1	skalní
útvaru	útvar	k1gInSc2	útvar
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
lezec	lezec	k1gMnSc1	lezec
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neostrá	ostrý	k2eNgFnSc1d1	neostrá
je	být	k5eAaImIp3nS	být
také	také	k9	také
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
skalní	skalní	k2eAgInPc1d1	skalní
útvary	útvar	k1gInPc1	útvar
a	a	k8xC	a
jakou	jaký	k3yRgFnSc4	jaký
výšku	výška	k1gFnSc4	výška
lze	lze	k6eAd1	lze
ještě	ještě	k6eAd1	ještě
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
malou	malý	k2eAgFnSc4d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
bouldery	bouldera	k1gFnPc1	bouldera
s	s	k7c7	s
významným	významný	k2eAgNnSc7d1	významné
rizikem	riziko	k1gNnSc7	riziko
zranění	zranění	k1gNnSc2	zranění
nebo	nebo	k8xC	nebo
i	i	k9	i
smrti	smrt	k1gFnSc2	smrt
se	se	k3xPyFc4	se
v	v	k7c6	v
lezeckém	lezecký	k2eAgInSc6d1	lezecký
slangu	slang	k1gInSc6	slang
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
High	High	k1gInSc1	High
bally	balla	k1gFnSc2	balla
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lezení	lezení	k1gNnSc1	lezení
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
rizikem	riziko	k1gNnSc7	riziko
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pádu	pád	k1gInSc2	pád
by	by	kYmCp3nS	by
většina	většina	k1gFnSc1	většina
lezců	lezec	k1gMnPc2	lezec
označila	označit	k5eAaPmAgFnS	označit
spíše	spíše	k9	spíše
za	za	k7c4	za
skalní	skalní	k2eAgNnSc4d1	skalní
lezení	lezení	k1gNnSc4	lezení
stylem	styl	k1gInSc7	styl
free	free	k6eAd1	free
solo	solo	k6eAd1	solo
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
těchto	tento	k3xDgFnPc2	tento
lezeckých	lezecký	k2eAgFnPc2d1	lezecká
"	"	kIx"	"
<g/>
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
neostré	ostrý	k2eNgFnPc1d1	neostrá
a	a	k8xC	a
často	často	k6eAd1	často
subjektivní	subjektivní	k2eAgMnSc1d1	subjektivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bouldering	Bouldering	k1gInSc1	Bouldering
lze	lze	k6eAd1	lze
také	také	k9	také
provozovat	provozovat	k5eAaImF	provozovat
na	na	k7c6	na
umělých	umělý	k2eAgFnPc6d1	umělá
stěnách	stěna	k1gFnPc6	stěna
"	"	kIx"	"
<g/>
bouldrovkách	bouldrovka	k1gFnPc6	bouldrovka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
tělocvičnách	tělocvična	k1gFnPc6	tělocvična
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
zimní	zimní	k2eAgFnSc3d1	zimní
přípravě	příprava	k1gFnSc3	příprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Závodní	závodní	k2eAgInSc1d1	závodní
bouldering	bouldering	k1gInSc1	bouldering
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
na	na	k7c6	na
přírodních	přírodní	k2eAgInPc6d1	přírodní
kamenech	kámen	k1gInPc6	kámen
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
závodění	závodění	k1gNnSc1	závodění
na	na	k7c6	na
umělých	umělý	k2eAgFnPc6d1	umělá
strukturách	struktura	k1gFnPc6	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
v	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
pořádá	pořádat	k5eAaImIp3nS	pořádat
série	série	k1gFnSc1	série
závodů	závod	k1gInPc2	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
horolezecké	horolezecký	k2eAgFnSc2d1	horolezecká
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
UIAA	UIAA	kA	UIAA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
federace	federace	k1gFnSc2	federace
sportovního	sportovní	k2eAgNnSc2d1	sportovní
lezení	lezení	k1gNnSc2	lezení
IFSC	IFSC	kA	IFSC
(	(	kIx(	(
<g/>
International	International	k1gMnSc2	International
Federation	Federation	k1gInSc1	Federation
of	of	k?	of
Sport	sport	k1gInSc1	sport
Climbing	Climbing	k1gInSc1	Climbing
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
UIAA	UIAA	kA	UIAA
oddělila	oddělit	k5eAaPmAgFnS	oddělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
probíhá	probíhat	k5eAaImIp3nS	probíhat
závod	závod	k1gInSc4	závod
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
kolech	kolo	k1gNnPc6	kolo
–	–	k?	–
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
semifinále	semifinále	k1gNnSc6	semifinále
a	a	k8xC	a
finále	finále	k1gNnSc6	finále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
závodníci	závodník	k1gMnPc1	závodník
postupně	postupně	k6eAd1	postupně
5	[number]	k4	5
boulderů	boulder	k1gInPc2	boulder
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
boulder	boulder	k1gInSc4	boulder
mají	mít	k5eAaImIp3nP	mít
časový	časový	k2eAgInSc4d1	časový
limit	limit	k1gInSc4	limit
a	a	k8xC	a
neomezený	omezený	k2eNgInSc4d1	neomezený
počet	počet	k1gInSc4	počet
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Startuje	startovat	k5eAaBmIp3nS	startovat
se	se	k3xPyFc4	se
z	z	k7c2	z
označených	označený	k2eAgInPc2d1	označený
chytů	chyt	k1gInPc2	chyt
<g/>
.	.	kIx.	.
</s>
<s>
Vylezený	vylezený	k2eAgInSc1d1	vylezený
boulder	boulder	k1gInSc1	boulder
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
termínem	termín	k1gInSc7	termín
top	topit	k5eAaImRp2nS	topit
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vrcholek	vrcholek	k1gInSc1	vrcholek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
topový	topový	k2eAgInSc1d1	topový
chyt	chyt	k1gInSc1	chyt
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
držen	držet	k5eAaImNgInS	držet
oběma	dva	k4xCgFnPc7	dva
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
topu	topus	k1gInSc2	topus
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc1	jeden
nižší	nízký	k2eAgInSc1d2	nižší
chyt	chyt	k1gInSc1	chyt
označen	označit	k5eAaPmNgInS	označit
jako	jako	k8xC	jako
bonus	bonus	k1gInSc1	bonus
nebo	nebo	k8xC	nebo
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
se	se	k3xPyFc4	se
určí	určit	k5eAaPmIp3nS	určit
sečtením	sečtení	k1gNnSc7	sečtení
dosažených	dosažený	k2eAgInPc2d1	dosažený
topů	top	k1gInPc2	top
a	a	k8xC	a
bonusů	bonus	k1gInPc2	bonus
<g/>
,	,	kIx,	,
při	při	k7c6	při
stejném	stejný	k2eAgInSc6d1	stejný
počtu	počet	k1gInSc6	počet
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
nižší	nízký	k2eAgInSc1d2	nižší
počet	počet	k1gInSc1	počet
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
se	se	k3xPyFc4	se
zapíše	zapsat	k5eAaPmIp3nS	zapsat
například	například	k6eAd1	například
jako	jako	k8xC	jako
4	[number]	k4	4
<g/>
t	t	k?	t
<g/>
8	[number]	k4	8
5	[number]	k4	5
<g/>
b	b	k?	b
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
čtyři	čtyři	k4xCgFnPc4	čtyři
topy	topa	k1gFnPc4	topa
na	na	k7c4	na
8	[number]	k4	8
pokusů	pokus	k1gInPc2	pokus
a	a	k8xC	a
5	[number]	k4	5
bonusů	bonus	k1gInPc2	bonus
na	na	k7c4	na
6	[number]	k4	6
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
leze	lézt	k5eAaImIp3nS	lézt
semifinále	semifinále	k1gNnSc1	semifinále
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
postupuje	postupovat	k5eAaImIp3nS	postupovat
obvykle	obvykle	k6eAd1	obvykle
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
16	[number]	k4	16
závodníků	závodník	k1gMnPc2	závodník
z	z	k7c2	z
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
a	a	k8xC	a
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
startuje	startovat	k5eAaBmIp3nS	startovat
zpravidla	zpravidla	k6eAd1	zpravidla
6	[number]	k4	6
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
ze	z	k7c2	z
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Startovní	startovní	k2eAgNnSc4d1	startovní
pořadí	pořadí	k1gNnSc4	pořadí
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
a	a	k8xC	a
finále	finále	k1gNnSc6	finále
je	být	k5eAaImIp3nS	být
opačné	opačný	k2eAgNnSc1d1	opačné
<g/>
,	,	kIx,	,
než	než	k8xS	než
výsledek	výsledek	k1gInSc1	výsledek
předchozího	předchozí	k2eAgNnSc2d1	předchozí
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
vítěz	vítěz	k1gMnSc1	vítěz
semifinále	semifinále	k1gNnSc2	semifinále
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
poslední	poslední	k2eAgInPc4d1	poslední
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
divácky	divácky	k6eAd1	divácky
atraktivnější	atraktivní	k2eAgNnSc1d2	atraktivnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
rovnosti	rovnost	k1gFnSc6	rovnost
výsledku	výsledek	k1gInSc2	výsledek
finále	finále	k1gNnSc2	finále
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
výsledek	výsledek	k1gInSc1	výsledek
ze	z	k7c2	z
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Obtížnost	obtížnost	k1gFnSc1	obtížnost
boulderů	boulder	k1gMnPc2	boulder
od	od	k7c2	od
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
k	k	k7c3	k
finále	finála	k1gFnSc3	finála
obvykle	obvykle	k6eAd1	obvykle
graduje	gradovat	k5eAaImIp3nS	gradovat
<g/>
,	,	kIx,	,
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
se	se	k3xPyFc4	se
čas	čas	k1gInSc1	čas
na	na	k7c4	na
pokusy	pokus	k1gInPc4	pokus
<g/>
,	,	kIx,	,
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
a	a	k8xC	a
finále	finále	k1gNnSc6	finále
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
lezou	lézt	k5eAaImIp3nP	lézt
jen	jen	k9	jen
4	[number]	k4	4
bouldery	bouldero	k1gNnPc7	bouldero
<g/>
.	.	kIx.	.
</s>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
lezou	lézt	k5eAaImIp3nP	lézt
tzv.	tzv.	kA	tzv.
on	on	k3xPp3gMnSc1	on
sight	sight	k1gMnSc1	sight
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dle	dle	k7c2	dle
pohledu	pohled	k1gInSc2	pohled
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
před	před	k7c7	před
závodem	závod	k1gInSc7	závod
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
časovém	časový	k2eAgInSc6d1	časový
limitu	limit	k1gInSc6	limit
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
postavené	postavený	k2eAgFnPc4d1	postavená
bouldery	bouldera	k1gFnPc4	bouldera
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
a	a	k8xC	a
osahat	osahat	k5eAaPmF	osahat
si	se	k3xPyFc3	se
první	první	k4xOgInPc4	první
chyty	chyt	k1gInPc4	chyt
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jsou	být	k5eAaImIp3nP	být
odvedeni	odvést	k5eAaPmNgMnP	odvést
do	do	k7c2	do
izolace	izolace	k1gFnSc2	izolace
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
nevidí	vidět	k5eNaImIp3nS	vidět
lézt	lézt	k5eAaImF	lézt
své	svůj	k3xOyFgMnPc4	svůj
soupeře	soupeř	k1gMnPc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyvolání	vyvolání	k1gNnSc6	vyvolání
z	z	k7c2	z
izolace	izolace	k1gFnSc2	izolace
lezec	lezec	k1gMnSc1	lezec
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
první	první	k4xOgInSc4	první
boulder	boulder	k1gInSc4	boulder
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vypršení	vypršení	k1gNnSc1	vypršení
limitu	limit	k1gInSc2	limit
má	mít	k5eAaImIp3nS	mít
jedno	jeden	k4xCgNnSc1	jeden
kolo	kolo	k1gNnSc1	kolo
odpočinek	odpočinek	k1gInSc1	odpočinek
v	v	k7c6	v
izolaci	izolace	k1gFnSc6	izolace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
pokusy	pokus	k1gInPc4	pokus
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
další	další	k2eAgMnSc1d1	další
závodník	závodník	k1gMnSc1	závodník
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jde	jít	k5eAaImIp3nS	jít
první	první	k4xOgMnSc1	první
závodník	závodník	k1gMnSc1	závodník
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
boulder	bouldra	k1gFnPc2	bouldra
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
závodník	závodník	k1gMnSc1	závodník
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
a	a	k8xC	a
na	na	k7c4	na
první	první	k4xOgInSc4	první
boulder	boulder	k1gInSc4	boulder
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
třetí	třetí	k4xOgMnSc1	třetí
závodník	závodník	k1gMnSc1	závodník
a	a	k8xC	a
tak	tak	k6eAd1	tak
to	ten	k3xDgNnSc1	ten
jde	jít	k5eAaImIp3nS	jít
dokud	dokud	k8xS	dokud
neprojdou	projít	k5eNaPmIp3nP	projít
všichni	všechen	k3xTgMnPc1	všechen
všechny	všechen	k3xTgFnPc4	všechen
bouldery	bouldera	k1gFnPc4	bouldera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
závody	závod	k1gInPc1	závod
pořádané	pořádaný	k2eAgInPc1d1	pořádaný
IFSC	IFSC	kA	IFSC
přenášeny	přenášen	k2eAgInPc1d1	přenášen
internetovou	internetový	k2eAgFnSc7d1	internetová
televizí	televize	k1gFnSc7	televize
(	(	kIx(	(
<g/>
kanál	kanál	k1gInSc1	kanál
IFSC	IFSC	kA	IFSC
na	na	k7c4	na
Youtube	Youtub	k1gInSc5	Youtub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Závodní	závodní	k2eAgFnPc1d1	závodní
disciplíny	disciplína	k1gFnPc1	disciplína
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
závodů	závod	k1gInPc2	závod
světového	světový	k2eAgInSc2d1	světový
formátu	formát	k1gInSc2	formát
jako	jako	k8xS	jako
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
se	se	k3xPyFc4	se
závodí	závodit	k5eAaImIp3nS	závodit
souhrnně	souhrnně	k6eAd1	souhrnně
ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc6	lezení
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nP	patřit
disciplíny	disciplína	k1gFnPc1	disciplína
<g/>
:	:	kIx,	:
lezení	lezení	k1gNnSc1	lezení
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
<g/>
,	,	kIx,	,
lezení	lezení	k1gNnSc4	lezení
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
bouldering	bouldering	k1gInSc1	bouldering
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc1	pořadí
jak	jak	k8xC	jak
přibývaly	přibývat	k5eAaImAgFnP	přibývat
postupně	postupně	k6eAd1	postupně
podle	podle	k7c2	podle
let	léto	k1gNnPc2	léto
na	na	k7c6	na
závodech	závod	k1gInPc6	závod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
se	se	k3xPyFc4	se
závodníci	závodník	k1gMnPc1	závodník
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
také	také	k9	také
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
dvou	dva	k4xCgMnPc2	dva
až	až	k9	až
tří	tři	k4xCgFnPc2	tři
disciplín	disciplína	k1gFnPc2	disciplína
dle	dle	k7c2	dle
účasti	účast	k1gFnSc2	účast
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
nebo	nebo	k8xC	nebo
poháry	pohár	k1gInPc1	pohár
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
disciplínách	disciplína	k1gFnPc6	disciplína
společně	společně	k6eAd1	společně
i	i	k8xC	i
odděleně	odděleně	k6eAd1	odděleně
podle	podle	k7c2	podle
pořadatelů	pořadatel	k1gMnPc2	pořadatel
závodů	závod	k1gInPc2	závod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc6	lezení
pro	pro	k7c4	pro
obtížnost	obtížnost	k1gFnSc4	obtížnost
+	+	kIx~	+
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
či	či	k8xC	či
náhradní	náhradní	k2eAgNnSc1d1	náhradní
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
probíhají	probíhat	k5eAaImIp3nP	probíhat
národní	národní	k2eAgInSc4d1	národní
poháry	pohár	k1gInPc4	pohár
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc4	mistrovství
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
disciplíny	disciplína	k1gFnPc4	disciplína
odděleně	odděleně	k6eAd1	odděleně
kvůli	kvůli	k7c3	kvůli
náročnosti	náročnost	k1gFnSc3	náročnost
na	na	k7c4	na
prostor	prostor	k1gInSc4	prostor
i	i	k8xC	i
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bouldering	Bouldering	k1gInSc4	Bouldering
na	na	k7c6	na
Olympiádě	olympiáda	k1gFnSc6	olympiáda
===	===	k?	===
</s>
</p>
<p>
<s>
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
lezení	lezení	k1gNnSc1	lezení
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
Světových	světový	k2eAgFnPc6d1	světová
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
poprvé	poprvé	k6eAd1	poprvé
závodit	závodit	k5eAaImF	závodit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
třech	tři	k4xCgFnPc6	tři
disciplínách	disciplína	k1gFnPc6	disciplína
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
i	i	k9	i
v	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
snižuje	snižovat	k5eAaImIp3nS	snižovat
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
předešlé	předešlý	k2eAgFnSc2d1	předešlá
nominační	nominační	k2eAgFnSc2d1	nominační
kvóty	kvóta	k1gFnSc2	kvóta
pro	pro	k7c4	pro
lezení	lezení	k1gNnSc4	lezení
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
v	v	k7c6	v
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
medailemi	medaile	k1gFnPc7	medaile
hodnotit	hodnotit	k5eAaImF	hodnotit
pouze	pouze	k6eAd1	pouze
kombinace	kombinace	k1gFnPc4	kombinace
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
disciplín	disciplína	k1gFnPc2	disciplína
pro	pro	k7c4	pro
univerzálnost	univerzálnost	k1gFnSc4	univerzálnost
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
zvedla	zvednout	k5eAaPmAgFnS	zvednout
vlna	vlna	k1gFnSc1	vlna
kritiky	kritika	k1gFnSc2	kritika
špičkových	špičkový	k2eAgMnPc2d1	špičkový
lezců	lezec	k1gMnPc2	lezec
-	-	kIx~	-
převážně	převážně	k6eAd1	převážně
lezců	lezec	k1gMnPc2	lezec
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
a	a	k8xC	a
bouldristů	bouldrista	k1gMnPc2	bouldrista
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tito	tento	k3xDgMnPc1	tento
mají	mít	k5eAaImIp3nP	mít
od	od	k7c2	od
lezení	lezení	k1gNnSc2	lezení
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
různý	různý	k2eAgInSc1d1	různý
systém	systém	k1gInSc1	systém
hodnocení	hodnocení	k1gNnSc2	hodnocení
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nutně	nutně	k6eAd1	nutně
musel	muset	k5eAaImAgMnS	muset
promítnout	promítnout	k5eAaPmF	promítnout
do	do	k7c2	do
jejich	jejich	k3xOp3gFnSc2	jejich
přípravy	příprava	k1gFnSc2	příprava
i	i	k8xC	i
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	on	k3xPp3gFnPc4	on
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
disciplíny	disciplína	k1gFnPc4	disciplína
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
navíc	navíc	k6eAd1	navíc
hodnotit	hodnotit	k5eAaImF	hodnotit
lezce	lezec	k1gMnPc4	lezec
i	i	k9	i
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
jako	jako	k8xC	jako
u	u	k7c2	u
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mládež	mládež	k1gFnSc4	mládež
===	===	k?	===
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
převládalo	převládat	k5eAaImAgNnS	převládat
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
bouldering	bouldering	k1gInSc1	bouldering
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
a	a	k8xC	a
škodlivý	škodlivý	k2eAgInSc4d1	škodlivý
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
během	během	k7c2	během
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
disciplíně	disciplína	k1gFnSc6	disciplína
nezávodilo	závodit	k5eNaImAgNnS	závodit
na	na	k7c6	na
juniorských	juniorský	k2eAgInPc6d1	juniorský
a	a	k8xC	a
dětských	dětský	k2eAgInPc6d1	dětský
závodech	závod	k1gInPc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
ale	ale	k8xC	ale
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
přibývá	přibývat	k5eAaImIp3nS	přibývat
tato	tento	k3xDgFnSc1	tento
lezecká	lezecký	k2eAgFnSc1d1	lezecká
disciplína	disciplína	k1gFnSc1	disciplína
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
i	i	k8xC	i
národních	národní	k2eAgInPc6d1	národní
závodech	závod	k1gInPc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
počtem	počet	k1gInSc7	počet
a	a	k8xC	a
velikostí	velikost	k1gFnSc7	velikost
umělých	umělý	k2eAgFnPc2d1	umělá
boulderingových	boulderingový	k2eAgFnPc2d1	boulderingová
stěn	stěna	k1gFnPc2	stěna
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc7d2	menší
náročností	náročnost	k1gFnSc7	náročnost
vedení	vedení	k1gNnSc2	vedení
kurzů	kurz	k1gInPc2	kurz
pro	pro	k7c4	pro
skupiny	skupina	k1gFnPc4	skupina
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
materiální	materiální	k2eAgNnSc4d1	materiální
vybavení	vybavení	k1gNnSc4	vybavení
i	i	k8xC	i
finanční	finanční	k2eAgFnSc4d1	finanční
zátěž	zátěž	k1gFnSc4	zátěž
<g/>
,	,	kIx,	,
stoupající	stoupající	k2eAgFnPc1d1	stoupající
oblíbeností	oblíbenost	k1gFnSc7	oblíbenost
boulderingu	bouldering	k1gInSc2	bouldering
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
rodičů	rodič	k1gMnPc2	rodič
dětí	dítě	k1gFnPc2	dítě
i	i	k8xC	i
nominací	nominace	k1gFnPc2	nominace
sportovního	sportovní	k2eAgNnSc2d1	sportovní
lezení	lezení	k1gNnSc2	lezení
mezi	mezi	k7c4	mezi
olympijské	olympijský	k2eAgInPc4d1	olympijský
sporty	sport	k1gInPc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc6	lezení
pořádaném	pořádaný	k2eAgNnSc6d1	pořádané
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
v	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
závodí	závodit	k5eAaImIp3nS	závodit
až	až	k9	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
juniorů	junior	k1gMnPc2	junior
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
a	a	k8xC	a
na	na	k7c6	na
Evropském	evropský	k2eAgInSc6d1	evropský
poháru	pohár	k1gInSc6	pohár
juniorů	junior	k1gMnPc2	junior
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svět	svět	k1gInSc1	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
</s>
</p>
<p>
<s>
===	===	k?	===
Evropa	Evropa	k1gFnSc1	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
juniorů	junior	k1gMnPc2	junior
</s>
</p>
<p>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
pohár	pohár	k1gInSc1	pohár
juniorů	junior	k1gMnPc2	junior
</s>
</p>
<p>
<s>
Melloblocco	Melloblocco	k6eAd1	Melloblocco
</s>
</p>
<p>
<s>
===	===	k?	===
Česko	Česko	k1gNnSc1	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
MČR	MČR	kA	MČR
v	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
pohár	pohár	k1gInSc1	pohár
v	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
</s>
</p>
<p>
<s>
Petrohradské	petrohradský	k2eAgNnSc1d1	Petrohradské
padání	padání	k1gNnSc1	padání
</s>
</p>
<p>
<s>
Mejcup	Mejcup	k1gMnSc1	Mejcup
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgFnPc1d1	Moravská
kejkle	kejkle	k1gFnPc1	kejkle
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
boulderingová	boulderingový	k2eAgFnSc1d1	boulderingová
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
==	==	k?	==
Posouvání	posouvání	k1gNnSc1	posouvání
hranic	hranice	k1gFnPc2	hranice
obtížnosti	obtížnost	k1gFnSc2	obtížnost
==	==	k?	==
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Gioia	Gioia	k1gFnSc1	Gioia
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
C	C	kA	C
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Varezze	Varezze	k1gFnSc1	Varezze
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Christian	Christian	k1gMnSc1	Christian
Core	Cor	k1gFnSc2	Cor
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Louny	Louny	k1gInPc1	Louny
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Boulder	Boulder	k1gInSc1	Boulder
-	-	kIx~	-
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
okresu	okres	k1gInSc2	okres
Boulder	Boulder	k1gInSc1	Boulder
County	Counta	k1gFnSc2	Counta
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Colorado	Colorado	k1gNnSc4	Colorado
v	v	k7c6	v
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Lezení	lezení	k1gNnSc1	lezení
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
</s>
</p>
<p>
<s>
Lezení	lezení	k1gNnSc1	lezení
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
</s>
</p>
<p>
<s>
Stupnice	stupnice	k1gFnSc1	stupnice
obtížnosti	obtížnost	k1gFnSc2	obtížnost
(	(	kIx(	(
<g/>
horolezectví	horolezectví	k1gNnSc1	horolezectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
doma	doma	k6eAd1	doma
postavit	postavit	k5eAaPmF	postavit
stěnu	stěna	k1gFnSc4	stěna
na	na	k7c4	na
bouldering	bouldering	k1gInSc4	bouldering
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bouldering	bouldering	k1gInSc1	bouldering
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
ifsc-climbing	ifsclimbing	k1gInSc1	ifsc-climbing
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
IFSC	IFSC	kA	IFSC
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
webové	webový	k2eAgFnSc2d1	webová
televize	televize	k1gFnSc2	televize
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
definic	definice	k1gFnPc2	definice
High	High	k1gInSc1	High
ballu	ball	k1gInSc2	ball
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
