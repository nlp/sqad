<s>
Horatio	Horatio	k1gMnSc1	Horatio
Herbert	Herbert	k1gMnSc1	Herbert
Kitchener	Kitchener	k1gMnSc1	Kitchener
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
Kitchener	Kitchener	k1gMnSc1	Kitchener
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1850	[number]	k4	1850
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
maršál	maršál	k1gMnSc1	maršál
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Horatio	Horatio	k1gMnSc1	Horatio
Herbert	Herbert	k1gMnSc1	Herbert
Kitchener	Kitchener	k1gMnSc1	Kitchener
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1850	[number]	k4	1850
v	v	k7c6	v
Ballylongfordu	Ballylongford	k1gInSc6	Ballylongford
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Kerry	Kerra	k1gFnSc2	Kerra
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
anglického	anglický	k2eAgMnSc4d1	anglický
důstojníka	důstojník	k1gMnSc4	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
anglo-egyptské	anglogyptský	k2eAgFnSc2d1	anglo-egyptský
expedice	expedice	k1gFnSc2	expedice
do	do	k7c2	do
Súdánu	Súdán	k1gInSc2	Súdán
(	(	kIx(	(
<g/>
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Mahdího	Mahdí	k2eAgNnSc2d1	Mahdí
povstání	povstání	k1gNnSc2	povstání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Ummdurmánu	Ummdurmán	k1gMnSc3	Ummdurmán
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
moderních	moderní	k2eAgFnPc2d1	moderní
zbraní	zbraň	k1gFnPc2	zbraň
mahdistické	mahdistický	k2eAgFnSc2d1	mahdistický
jednotky	jednotka	k1gFnSc2	jednotka
zcela	zcela	k6eAd1	zcela
rozdrtil	rozdrtit	k5eAaPmAgMnS	rozdrtit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
dalším	další	k2eAgNnSc7d1	další
velkým	velký	k2eAgNnSc7d1	velké
tažením	tažení	k1gNnSc7	tažení
bylo	být	k5eAaImAgNnS	být
tažení	tažení	k1gNnSc1	tažení
proti	proti	k7c3	proti
Búrům	Búr	k1gMnPc3	Búr
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
britských	britský	k2eAgMnPc2d1	britský
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
Búry	Búr	k1gMnPc4	Búr
porazil	porazit	k5eAaPmAgMnS	porazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
blamoval	blamovat	k5eAaBmAgMnS	blamovat
přitom	přitom	k6eAd1	přitom
sebe	sebe	k3xPyFc4	sebe
i	i	k8xC	i
celou	celý	k2eAgFnSc4d1	celá
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
systematickým	systematický	k2eAgNnSc7d1	systematické
používáním	používání	k1gNnSc7	používání
brutálních	brutální	k2eAgFnPc2d1	brutální
metod	metoda	k1gFnPc2	metoda
proti	proti	k7c3	proti
búrskému	búrský	k2eAgNnSc3d1	búrský
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
místy	místy	k6eAd1	místy
mělo	mít	k5eAaImAgNnS	mít
až	až	k6eAd1	až
genocidní	genocidní	k2eAgInSc4d1	genocidní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
moderních	moderní	k2eAgFnPc2d1	moderní
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zapsal	zapsat	k5eAaPmAgInS	zapsat
především	především	k9	především
jako	jako	k9	jako
tvůrce	tvůrce	k1gMnSc1	tvůrce
systému	systém	k1gInSc2	systém
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1914	[number]	k4	1914
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
hrabětem	hrabě	k1gMnSc7	hrabě
Kitchenerem	Kitchener	k1gMnSc7	Kitchener
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
ministrem	ministr	k1gMnSc7	ministr
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
nástup	nástup	k1gInSc1	nástup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
vyznačující	vyznačující	k2eAgFnSc2d1	vyznačující
se	se	k3xPyFc4	se
úspěchy	úspěch	k1gInPc7	úspěch
v	v	k7c6	v
náboru	nábor	k1gInSc6	nábor
vojáků	voják	k1gMnPc2	voják
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Nové	Nové	k2eAgFnSc2d1	Nové
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
však	však	k9	však
posléze	posléze	k6eAd1	posléze
přešel	přejít	k5eAaPmAgInS	přejít
v	v	k7c4	v
pozvolný	pozvolný	k2eAgInSc4d1	pozvolný
pád	pád	k1gInSc4	pád
<g/>
.	.	kIx.	.
</s>
<s>
Kitchener	Kitchener	k1gMnSc1	Kitchener
se	se	k3xPyFc4	se
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
nové	nový	k2eAgFnSc3d1	nová
podobě	podoba	k1gFnSc3	podoba
války	válka	k1gFnSc2	válka
a	a	k8xC	a
tváří	tvář	k1gFnSc7	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
srovnatelnému	srovnatelný	k2eAgMnSc3d1	srovnatelný
protivníku	protivník	k1gMnSc3	protivník
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
skutečné	skutečný	k2eAgFnPc1d1	skutečná
válečnické	válečnický	k2eAgFnPc1d1	válečnická
schopnosti	schopnost	k1gFnPc1	schopnost
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
dobré	dobrý	k2eAgFnPc1d1	dobrá
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gInPc2	jeho
výsledků	výsledek	k1gInPc2	výsledek
proti	proti	k7c3	proti
protivníkům	protivník	k1gMnPc3	protivník
podstatně	podstatně	k6eAd1	podstatně
slabším	slabý	k2eAgMnPc3d2	slabší
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
ministerské	ministerský	k2eAgNnSc4d1	ministerské
křeslo	křeslo	k1gNnSc4	křeslo
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
vážně	vážně	k6eAd1	vážně
kymácet	kymácet	k5eAaImF	kymácet
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
když	když	k8xS	když
začínalo	začínat	k5eAaImAgNnS	začínat
být	být	k5eAaImF	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Gallipoli	Gallipoli	k1gNnSc4	Gallipoli
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
Kitchener	Kitchener	k1gInSc4	Kitchener
angažoval	angažovat	k5eAaBmAgInS	angažovat
<g/>
,	,	kIx,	,
neskončí	skončit	k5eNaPmIp3nS	skončit
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zhroutilo	zhroutit	k5eAaPmAgNnS	zhroutit
zásobování	zásobování	k1gNnSc1	zásobování
bojujících	bojující	k2eAgFnPc2d1	bojující
jednotek	jednotka	k1gFnPc2	jednotka
municí	munice	k1gFnSc7	munice
<g/>
,	,	kIx,	,
Kitchener	Kitchener	k1gInSc1	Kitchener
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
svoji	svůj	k3xOyFgFnSc4	svůj
rezignaci	rezignace	k1gFnSc4	rezignace
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
ji	on	k3xPp3gFnSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
propagandistických	propagandistický	k2eAgInPc2d1	propagandistický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zároveň	zároveň	k6eAd1	zároveň
zbavil	zbavit	k5eAaPmAgInS	zbavit
Kitchenera	Kitchener	k1gMnSc4	Kitchener
dohledu	dohled	k1gInSc2	dohled
nad	nad	k7c7	nad
válečným	válečný	k2eAgInSc7d1	válečný
průmyslem	průmysl	k1gInSc7	průmysl
a	a	k8xC	a
značně	značně	k6eAd1	značně
omezil	omezit	k5eAaPmAgInS	omezit
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
zasahování	zasahování	k1gNnSc4	zasahování
do	do	k7c2	do
řízení	řízení	k1gNnSc2	řízení
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgInS	být
vzápětí	vzápětí	k6eAd1	vzápětí
Kitchener	Kitchener	k1gInSc1	Kitchener
vyslán	vyslat	k5eAaPmNgInS	vyslat
na	na	k7c4	na
diplomatickou	diplomatický	k2eAgFnSc4d1	diplomatická
a	a	k8xC	a
poradenskou	poradenský	k2eAgFnSc4d1	poradenská
misi	mise	k1gFnSc4	mise
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
což	což	k9	což
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
jako	jako	k9	jako
promyšlený	promyšlený	k2eAgInSc4d1	promyšlený
krok	krok	k1gInSc4	krok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
dostat	dostat	k5eAaPmF	dostat
co	co	k9	co
nejdál	daleko	k6eAd3	daleko
od	od	k7c2	od
řízení	řízení	k1gNnSc2	řízení
britské	britský	k2eAgFnSc2d1	britská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Kitchener	Kitchener	k1gMnSc1	Kitchener
opustil	opustit	k5eAaPmAgMnS	opustit
Anglii	Anglie	k1gFnSc4	Anglie
na	na	k7c6	na
křižníku	křižník	k1gInSc6	křižník
HMS	HMS	kA	HMS
Hampshire	Hampshir	k1gInSc5	Hampshir
<g/>
,	,	kIx,	,
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nedorazil	dorazit	k5eNaPmAgMnS	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
loď	loď	k1gFnSc1	loď
najela	najet	k5eAaPmAgFnS	najet
západně	západně	k6eAd1	západně
od	od	k7c2	od
Orknejí	Orkneje	k1gFnPc2	Orkneje
na	na	k7c4	na
minu	mina	k1gFnSc4	mina
a	a	k8xC	a
potopila	potopit	k5eAaPmAgFnS	potopit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nenalezlo	nalézt	k5eNaBmAgNnS	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nehodě	nehoda	k1gFnSc3	nehoda
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
známý	známý	k2eAgMnSc1d1	známý
búrský	búrský	k2eAgMnSc1d1	búrský
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
špión	špión	k1gMnSc1	špión
Frederick	Frederick	k1gMnSc1	Frederick
Joubert	Joubert	k1gMnSc1	Joubert
Duquesne	Duquesne	k1gMnSc1	Duquesne
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
nechal	nechat	k5eAaPmAgMnS	nechat
navést	navést	k5eAaPmF	navést
křižník	křižník	k1gInSc4	křižník
na	na	k7c4	na
minu	mina	k1gFnSc4	mina
schválně	schválně	k6eAd1	schválně
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
motiv	motiv	k1gInSc4	motiv
uvedl	uvést	k5eAaPmAgMnS	uvést
Kitchenerovo	Kitchenerův	k2eAgNnSc4d1	Kitchenerův
chování	chování	k1gNnSc4	chování
vůči	vůči	k7c3	vůči
Búrům	Búr	k1gMnPc3	Búr
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
NERAD	nerad	k2eAgMnSc1d1	nerad
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Kitchener	Kitchener	k1gMnSc1	Kitchener
a	a	k8xC	a
britsko-búrská	britskoúrský	k2eAgFnSc1d1	britsko-búrský
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
14	[number]	k4	14
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
<s>
VALKOUN	VALKOUN	kA	VALKOUN
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Dobytí	dobytí	k1gNnSc1	dobytí
Súdánu	Súdán	k1gInSc2	Súdán
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
18	[number]	k4	18
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
252	[number]	k4	252
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Horatio	Horatio	k6eAd1	Horatio
Kitchener	Kitchener	k1gInSc4	Kitchener
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
