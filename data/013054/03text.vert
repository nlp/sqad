<p>
<s>
Goodbye	Goodbye	k6eAd1	Goodbye
je	být	k5eAaImIp3nS	být
rozlučkové	rozlučkový	k2eAgNnSc1d1	rozlučkové
album	album	k1gNnSc1	album
britské	britský	k2eAgFnSc2d1	britská
skupiny	skupina	k1gFnSc2	skupina
Cream	Cream	k1gInSc1	Cream
a	a	k8xC	a
jakoby	jakoby	k8xS	jakoby
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
předchozí	předchozí	k2eAgFnSc2d1	předchozí
desky	deska	k1gFnSc2	deska
Wheels	Wheels	k1gInSc1	Wheels
of	of	k?	of
Fire	Fire	k1gInSc1	Fire
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
jednodiskovou	jednodiskový	k2eAgFnSc4d1	jednodisková
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
živou	živý	k2eAgFnSc4d1	živá
a	a	k8xC	a
studiovou	studiový	k2eAgFnSc4d1	studiová
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
živá	živý	k2eAgFnSc1d1	živá
A-strana	Atrana	k1gFnSc1	A-strana
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
skupinu	skupina	k1gFnSc4	skupina
jako	jako	k8xC	jako
mistrné	mistrný	k2eAgMnPc4d1	mistrný
hudebníky	hudebník	k1gMnPc4	hudebník
pohrávajícími	pohrávající	k2eAgMnPc7d1	pohrávající
si	se	k3xPyFc3	se
s	s	k7c7	s
blues-rockovými	bluesockův	k2eAgFnPc7d1	blues-rockův
improvizacemi	improvizace	k1gFnPc7	improvizace
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
"	"	kIx"	"
<g/>
Politician	Politician	k1gInSc1	Politician
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Sitting	Sitting	k1gInSc1	Sitting
on	on	k3xPp3gMnSc1	on
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
"	"	kIx"	"
však	však	k9	však
znějí	znět	k5eAaImIp3nP	znět
daleko	daleko	k6eAd1	daleko
bluesověji	bluesově	k6eAd2	bluesově
než	než	k8xS	než
jejich	jejich	k3xOp3gFnPc1	jejich
studiové	studiový	k2eAgFnPc1d1	studiová
verze	verze	k1gFnPc1	verze
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
jakoby	jakoby	k8xS	jakoby
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
vracela	vracet	k5eAaImAgFnS	vracet
k	k	k7c3	k
tradičnějšímu	tradiční	k2eAgNnSc3d2	tradičnější
pojetí	pojetí	k1gNnSc3	pojetí
blues	blues	k1gFnSc2	blues
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
uvedla	uvést	k5eAaPmAgFnS	uvést
na	na	k7c6	na
debutové	debutový	k2eAgFnSc6d1	debutová
desce	deska	k1gFnSc6	deska
Fresh	Fresha	k1gFnPc2	Fresha
Cream	Cream	k1gInSc1	Cream
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studiová	studiový	k2eAgFnSc1d1	studiová
strana	strana	k1gFnSc1	strana
B	B	kA	B
je	být	k5eAaImIp3nS	být
vyplněna	vyplnit	k5eAaPmNgFnS	vyplnit
spíše	spíše	k9	spíše
oddechovými	oddechový	k2eAgFnPc7d1	oddechová
písněmi	píseň	k1gFnPc7	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ale	ale	k9	ale
úvodní	úvodní	k2eAgInPc4d1	úvodní
"	"	kIx"	"
<g/>
Badge	Badg	k1gInPc4	Badg
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
s	s	k7c7	s
Claptonem	Clapton	k1gInSc7	Clapton
autorsky	autorsky	k6eAd1	autorsky
podílel	podílet	k5eAaImAgInS	podílet
také	také	k9	také
George	George	k1gNnSc4	George
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
příjemně	příjemně	k6eAd1	příjemně
hravá	hravý	k2eAgNnPc4d1	hravé
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Doing	Doing	k1gMnSc1	Doing
That	That	k1gMnSc1	That
Scrapyard	Scrapyard	k1gMnSc1	Scrapyard
Thing	Thing	k1gMnSc1	Thing
<g/>
"	"	kIx"	"
už	už	k6eAd1	už
sklouzává	sklouzávat	k5eAaImIp3nS	sklouzávat
k	k	k7c3	k
nevkusu	nevkus	k1gInSc3	nevkus
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ostatní	ostatní	k2eAgFnPc1d1	ostatní
písně	píseň	k1gFnPc1	píseň
spolehlivě	spolehlivě	k6eAd1	spolehlivě
drží	držet	k5eAaImIp3nP	držet
kvalitu	kvalita	k1gFnSc4	kvalita
této	tento	k3xDgFnSc2	tento
desky	deska	k1gFnSc2	deska
dost	dost	k6eAd1	dost
vysoko	vysoko	k6eAd1	vysoko
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
pouhé	pouhý	k2eAgFnSc6d1	pouhá
labutí	labutí	k2eAgFnSc6d1	labutí
písni	píseň	k1gFnSc6	píseň
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
neexistující	existující	k2eNgFnSc2d1	neexistující
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Strana	strana	k1gFnSc1	strana
1	[number]	k4	1
===	===	k?	===
</s>
</p>
<p>
<s>
Nahrané	nahraný	k2eAgInPc1d1	nahraný
živě	živě	k6eAd1	živě
v	v	k7c6	v
The	The	k1gFnSc6	The
Forum	forum	k1gNnSc1	forum
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
So	So	kA	So
Glad	Glad	k1gMnSc1	Glad
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Skip	skip	k1gInSc1	skip
James	James	k1gMnSc1	James
<g/>
)	)	kIx)	)
–	–	k?	–
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Politician	Politician	k1gInSc1	Politician
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Jack	Jack	k1gMnSc1	Jack
Bruce	Bruce	k1gMnSc1	Bruce
<g/>
,	,	kIx,	,
Pete	Pete	k1gFnSc1	Pete
Brown	Brown	k1gMnSc1	Brown
<g/>
)	)	kIx)	)
–	–	k?	–
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Sitting	Sitting	k1gInSc1	Sitting
on	on	k3xPp3gMnSc1	on
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Chester	Chester	k1gMnSc1	Chester
Burnett	Burnett	k1gMnSc1	Burnett
<g/>
)	)	kIx)	)
–	–	k?	–
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
</s>
</p>
<p>
<s>
===	===	k?	===
Strana	strana	k1gFnSc1	strana
2	[number]	k4	2
===	===	k?	===
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Badge	Badge	k1gFnSc1	Badge
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Eric	Eric	k1gFnSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
)	)	kIx)	)
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
47	[number]	k4	47
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Doing	Doing	k1gMnSc1	Doing
That	That	k1gMnSc1	That
Scrapyard	Scrapyard	k1gMnSc1	Scrapyard
Thing	Thing	k1gMnSc1	Thing
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Bruce	Bruce	k1gMnSc1	Bruce
<g/>
,	,	kIx,	,
Brown	Brown	k1gMnSc1	Brown
<g/>
)	)	kIx)	)
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
18	[number]	k4	18
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
What	What	k1gMnSc1	What
a	a	k8xC	a
Bringdown	Bringdown	k1gMnSc1	Bringdown
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ginger	Ginger	k1gMnSc1	Ginger
Baker	Baker	k1gMnSc1	Baker
<g/>
)	)	kIx)	)
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
</s>
</p>
<p>
<s>
==	==	k?	==
Sestava	sestava	k1gFnSc1	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
Jack	Jack	k1gMnSc1	Jack
Bruce	Bruce	k1gMnSc1	Bruce
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
piano	piano	k1gNnSc1	piano
a	a	k8xC	a
hammondovy	hammondův	k2eAgInPc1d1	hammondův
varhany	varhany	k1gInPc1	varhany
v	v	k7c6	v
"	"	kIx"	"
<g/>
Doing	Doing	k1gMnSc1	Doing
That	That	k1gMnSc1	That
Scrapyard	Scrapyard	k1gMnSc1	Scrapyard
Thing	Thing	k1gMnSc1	Thing
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
What	What	k1gInSc1	What
a	a	k8xC	a
Bringdown	Bringdown	k1gInSc1	Bringdown
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Eric	Eric	k6eAd1	Eric
Clapton	Clapton	k1gInSc1	Clapton
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Ginger	Ginger	k1gMnSc1	Ginger
Baker	Baker	k1gMnSc1	Baker
–	–	k?	–
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnSc7d1	další
===	===	k?	===
</s>
</p>
<p>
<s>
Felix	Felix	k1gMnSc1	Felix
Pappalardi	Pappalard	k1gMnPc1	Pappalard
–	–	k?	–
piano	piano	k6eAd1	piano
<g/>
,	,	kIx,	,
mellotron	mellotron	k1gInSc1	mellotron
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
ve	v	k7c4	v
"	"	kIx"	"
<g/>
What	What	k1gMnSc1	What
a	a	k8xC	a
Bringdown	Bringdown	k1gMnSc1	Bringdown
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc4	Georg
Harrison	Harrison	k1gMnSc1	Harrison
(	(	kIx(	(
<g/>
uveden	uvést	k5eAaPmNgInS	uvést
jako	jako	k9	jako
"	"	kIx"	"
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Angelo	Angela	k1gFnSc5	Angela
Misterioso	Misteriosa	k1gFnSc5	Misteriosa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
v	v	k7c6	v
"	"	kIx"	"
<g/>
Badge	Badge	k1gFnSc6	Badge
<g/>
"	"	kIx"	"
</s>
</p>
