<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Bílé	bílý	k2eAgFnSc2d1
Karpaty	Karpaty	k1gInPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Bílé	bílý	k2eAgFnSc2d1
KarpatyIUCN	KarpatyIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Kopec	kopec	k1gInSc1
Lesná	lesný	k2eAgFnSc1d1
<g/>
,	,	kIx,
součást	součást	k1gFnSc1
CHKO	CHKO	kA
Bílé	bílý	k2eAgFnSc2d1
KarpatyZákladní	KarpatyZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1980	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
715	#num#	k4
km²	km²	k?
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraje	kraj	k1gInSc2
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
a	a	k8xC
Zlínský	zlínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
12,77	12,77	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
20,1	20,1	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Bílé	bílý	k2eAgFnSc2d1
Karpaty	Karpaty	k1gInPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
71	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.bilekarpaty.cz	www.bilekarpaty.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc7
(	(	kIx(
<g/>
CHKO	CHKO	kA
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgNnP
vyhlášena	vyhlášen	k2eAgFnSc1d1
dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1980	#num#	k4
<g/>
,	,	kIx,
vyhláškou	vyhláška	k1gFnSc7
MK	MK	kA
ČSR	ČSR	kA
č.	č.	k?
17	#num#	k4
644	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc4
správy	správa	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
Luhačovicích	Luhačovice	k1gFnPc6
Nádražní	nádražní	k2eAgInSc1d1
318	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
71	#num#	k4
500	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
podle	podle	k7c2
GIS	gis	k1gNnSc2
<g/>
:	:	kIx,
74	#num#	k4
688	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
okresů	okres	k1gInPc2
Hodonín	Hodonín	k1gInSc1
<g/>
,	,	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
a	a	k8xC
Zlín	Zlín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osu	osa	k1gFnSc4
CHKO	CHKO	kA
tvoří	tvořit	k5eAaImIp3nS
pohraniční	pohraniční	k2eAgFnSc1d1
pohoří	pohořet	k5eAaPmIp3nP
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižně	jižně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Strážnice	Strážnice	k1gFnSc2
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
CHKO	CHKO	kA
Dolnomoravský	dolnomoravský	k2eAgInSc1d1
úval	úval	k1gInSc1
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
východně	východně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Luhačovice	Luhačovice	k1gFnPc4
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
CHKO	CHKO	kA
Vizovická	vizovický	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
bilaterální	bilaterální	k2eAgNnSc4d1
CHKO	CHKO	kA
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
česká	český	k2eAgFnSc1d1
část	část	k1gFnSc1
má	mít	k5eAaImIp3nS
délku	délka	k1gFnSc4
70	#num#	k4
km	km	kA
<g/>
,	,	kIx,
orientaci	orientace	k1gFnSc4
severovýchod-jihozápad	severovýchod-jihozápad	k1gInSc4
a	a	k8xC
leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
171	#num#	k4
–	–	k?
970	#num#	k4
m.	m.	k?
Nejnižším	nízký	k2eAgNnSc7d3
místem	místo	k1gNnSc7
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc4
Petrov	Petrov	k1gInSc1
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
171	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Nejvyšším	vysoký	k2eAgNnSc7d3
místem	místo	k1gNnSc7
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc4
Velké	velký	k2eAgFnSc2d1
Javořiny	Javořina	k1gFnSc2
s	s	k7c7
970	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Slovenská	slovenský	k2eAgFnSc1d1
CHKO	CHKO	kA
Biele	Biele	k1gFnSc1
Karpaty	Karpaty	k1gInPc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
a	a	k8xC
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
435	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
představují	představovat	k5eAaImIp3nP
mimořádnou	mimořádný	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
mezi	mezi	k7c7
našimi	náš	k3xOp1gNnPc7
velkoplošnými	velkoplošný	k2eAgNnPc7d1
chráněnými	chráněný	k2eAgNnPc7d1
územími	území	k1gNnPc7
především	především	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
nejvyšším	vysoký	k2eAgNnSc7d3
pohořím	pohoří	k1gNnSc7
jihozápadního	jihozápadní	k2eAgInSc2d1
okraje	okraj	k1gInSc2
vlastního	vlastní	k2eAgInSc2d1
karpatského	karpatský	k2eAgInSc2d1
horského	horský	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
zejména	zejména	k9
její	její	k3xOp3gFnSc1
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
po	po	k7c4
mnoho	mnoho	k4c4
staletí	staletí	k1gNnPc2
kultivována	kultivovat	k5eAaImNgFnS
člověkem	člověk	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
právě	právě	k9
proto	proto	k8xC
se	se	k3xPyFc4
zde	zde	k6eAd1
dochovaly	dochovat	k5eAaPmAgFnP
mimořádně	mimořádně	k6eAd1
cenné	cenný	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
a	a	k8xC
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
lze	lze	k6eAd1
hovořit	hovořit	k5eAaImF
o	o	k7c6
harmonické	harmonický	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgFnPc4
přírodní	přírodní	k2eAgFnPc4d1
a	a	k8xC
krajinné	krajinný	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
byly	být	k5eAaImAgInP
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
v	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
Člověk	člověk	k1gMnSc1
a	a	k8xC
biosféra	biosféra	k1gFnSc1
(	(	kIx(
<g/>
MAB	MAB	kA
<g/>
)	)	kIx)
organizace	organizace	k1gFnSc2
UNESCO	UNESCO	kA
dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1996	#num#	k4
zařazeny	zařadit	k5eAaPmNgInP
mezi	mezi	k7c4
evropské	evropský	k2eAgFnPc4d1
biosférické	biosférický	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
dostala	dostat	k5eAaPmAgFnS
CHKO	CHKO	kA
Diplom	diplom	k1gInSc4
Rady	rada	k1gFnSc2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zde	zde	k6eAd1
vyhlášeno	vyhlásit	k5eAaPmNgNnS
16	#num#	k4
evropsky	evropsky	k6eAd1
významných	významný	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	svůj	k3xOyFgInSc7
charakterem	charakter	k1gInSc7
mohou	moct	k5eAaImIp3nP
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
sloužit	sloužit	k5eAaImF
jako	jako	k8xC,k8xS
modelové	modelový	k2eAgNnSc1d1
území	území	k1gNnSc1
pro	pro	k7c4
koexistenci	koexistence	k1gFnSc4
zájmů	zájem	k1gInPc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
s	s	k7c7
hospodářskými	hospodářský	k2eAgFnPc7d1
aktivitami	aktivita	k1gFnPc7
respektujícími	respektující	k2eAgFnPc7d1
ekologickou	ekologický	k2eAgFnSc4d1
únosnost	únosnost	k1gFnSc4
území	území	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
přírodní	přírodní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgNnPc1d1
historická	historický	k2eAgNnPc1d1
odlesnění	odlesnění	k1gNnPc1
v	v	k7c6
Bílých	bílý	k2eAgInPc6d1
Karpatech	Karpaty	k1gInPc6
měla	mít	k5eAaImAgNnP
velmi	velmi	k6eAd1
často	často	k6eAd1
charakter	charakter	k1gInSc4
krajinářských	krajinářský	k2eAgFnPc2d1
úprav	úprava	k1gFnPc2
citlivě	citlivě	k6eAd1
využívajících	využívající	k2eAgFnPc2d1
zdejších	zdejší	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
jsou	být	k5eAaImIp3nP
tisíce	tisíc	k4xCgInPc1
hektarů	hektar	k1gInPc2
jedinečných	jedinečný	k2eAgFnPc2d1
květnatých	květnatý	k2eAgFnPc2d1
luk	louka	k1gFnPc2
s	s	k7c7
roztroušenými	roztroušený	k2eAgFnPc7d1
dřevinami	dřevina	k1gFnPc7
<g/>
,	,	kIx,
představující	představující	k2eAgInSc1d1
dnes	dnes	k6eAd1
typický	typický	k2eAgInSc1d1
krajinný	krajinný	k2eAgInSc1d1
ráz	ráz	k1gInSc1
Bílých	bílý	k2eAgInPc2d1
Karpat	Karpaty	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
přírodovědného	přírodovědný	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc4
květnaté	květnatý	k2eAgFnPc4d1
karpatské	karpatský	k2eAgFnPc4d1
louky	louka	k1gFnPc4
pozoruhodné	pozoruhodný	k2eAgFnPc4d1
především	především	k6eAd1
bohatostí	bohatost	k1gFnSc7
rostlinných	rostlinný	k2eAgNnPc2d1
společenstev	společenstvo	k1gNnPc2
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
kriticky	kriticky	k6eAd1
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejcennějším	cenný	k2eAgInPc3d3
lučním	luční	k2eAgInPc3d1
biotopům	biotop	k1gInPc3
Evropy	Evropa	k1gFnSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
studijní	studijní	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
světového	světový	k2eAgInSc2d1
významu	význam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgMnSc7d1
neméně	málo	k6eNd2
cenným	cenný	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
jsou	být	k5eAaImIp3nP
rozsáhlé	rozsáhlý	k2eAgInPc1d1
lesní	lesní	k2eAgInPc1d1
komplexy	komplex	k1gInPc1
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
a	a	k8xC
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
pohoří	pohoří	k1gNnSc2
s	s	k7c7
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
typických	typický	k2eAgInPc2d1
prvků	prvek	k1gInPc2
karpatské	karpatský	k2eAgFnSc2d1
květeny	květena	k1gFnSc2
i	i	k8xC
fauny	fauna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Krajinný	krajinný	k2eAgInSc1d1
ráz	ráz	k1gInSc1
střední	střední	k2eAgFnSc2d1
a	a	k8xC
severní	severní	k2eAgFnSc2d1
části	část	k1gFnSc2
Bílých	bílý	k2eAgInPc2d1
Karpat	Karpaty	k1gInPc2
je	být	k5eAaImIp3nS
dotvářen	dotvářet	k5eAaImNgInS
poměrně	poměrně	k6eAd1
řídkým	řídký	k2eAgNnSc7d1
osídlením	osídlení	k1gNnSc7
pasekářského	pasekářský	k2eAgInSc2d1
či	či	k8xC
kopaničářského	kopaničářský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
absencí	absence	k1gFnSc7
velkých	velký	k2eAgInPc2d1
průmyslových	průmyslový	k2eAgInPc2d1
podniků	podnik	k1gInPc2
a	a	k8xC
zachovalou	zachovalý	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
celých	celý	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
(	(	kIx(
<g/>
Lopeník	Lopeník	k1gMnSc1
<g/>
,	,	kIx,
Vyškovec	Vyškovec	k1gMnSc1
<g/>
,	,	kIx,
Žítková	Žítková	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
charakteristické	charakteristický	k2eAgInPc1d1
velmi	velmi	k6eAd1
rozsáhlé	rozsáhlý	k2eAgInPc1d1
komplexy	komplex	k1gInPc1
květnatých	květnatý	k2eAgFnPc2d1
luk	louka	k1gFnPc2
s	s	k7c7
rozptýlenými	rozptýlený	k2eAgInPc7d1
soliterními	soliterní	k2eAgInPc7d1
stromy	strom	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střední	střední	k2eAgFnSc1d1
část	část	k1gFnSc1
CHKO	CHKO	kA
v	v	k7c6
širším	široký	k2eAgNnSc6d2
okolí	okolí	k1gNnSc6
Starého	Starého	k2eAgInSc2d1
Hrozenkova	Hrozenkov	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Moravské	moravský	k2eAgFnSc2d1
Kopanice	kopanice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
současný	současný	k2eAgInSc1d1
vzhled	vzhled	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
teprve	teprve	k6eAd1
velmi	velmi	k6eAd1
pozdní	pozdní	k2eAgFnSc7d1
valašskou	valašský	k2eAgFnSc7d1
kolonizací	kolonizace	k1gFnSc7
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
roztroušenou	roztroušený	k2eAgFnSc7d1
zástavbou	zástavba	k1gFnSc7
<g/>
,	,	kIx,
střídáním	střídání	k1gNnSc7
zalesněných	zalesněný	k2eAgFnPc2d1
a	a	k8xC
bezlesých	bezlesý	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
s	s	k7c7
mozaikou	mozaika	k1gFnSc7
sušších	suchý	k2eAgNnPc2d2
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
mokřadů	mokřad	k1gInPc2
<g/>
,	,	kIx,
drobných	drobný	k2eAgInPc2d1
lesíků	lesík	k1gInPc2
<g/>
,	,	kIx,
křovin	křovina	k1gFnPc2
a	a	k8xC
nevelkých	velký	k2eNgNnPc2d1
políček	políčko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severovýchodní	severovýchodní	k2eAgFnSc1d1
část	část	k1gFnSc1
pohoří	pohořet	k5eAaPmIp3nS
v	v	k7c6
okolí	okolí	k1gNnSc6
Valašských	valašský	k2eAgInPc2d1
Klobouk	Klobouky	k1gInPc2
a	a	k8xC
Brumova	Brumův	k2eAgFnSc1d1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
Valašsku	Valašsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajina	Krajina	k1gFnSc1
zde	zde	k6eAd1
již	již	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
Javorníky	Javorník	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
na	na	k7c4
Bílé	bílý	k2eAgInPc4d1
Karpaty	Karpaty	k1gInPc4
bezprostředně	bezprostředně	k6eAd1
navazují	navazovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Rozmanité	rozmanitý	k2eAgInPc1d1
způsoby	způsob	k1gInPc1
hospodaření	hospodaření	k1gNnSc2
<g/>
,	,	kIx,
různorodý	různorodý	k2eAgInSc4d1
historický	historický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
odlehlost	odlehlost	k1gFnSc1
od	od	k7c2
průmyslových	průmyslový	k2eAgNnPc2d1
středisek	středisko	k1gNnPc2
umožnily	umožnit	k5eAaPmAgFnP
zachovat	zachovat	k5eAaPmF
neobvykle	obvykle	k6eNd1
vysokou	vysoký	k2eAgFnSc4d1
biodiverzitu	biodiverzita	k1gFnSc4
na	na	k7c6
mnoha	mnoho	k4c6
typech	typ	k1gInPc6
stanovišť	stanoviště	k1gNnPc2
<g/>
,	,	kIx,
od	od	k7c2
teplomilných	teplomilný	k2eAgFnPc2d1
šipákových	šipákův	k2eAgFnPc2d1
doubrav	doubrava	k1gFnPc2
po	po	k7c4
pralesovité	pralesovitý	k2eAgFnPc4d1
horské	horský	k2eAgFnPc4d1
bučiny	bučina	k1gFnPc4
<g/>
,	,	kIx,
od	od	k7c2
teplomilných	teplomilný	k2eAgInPc2d1
stepních	stepní	k2eAgInPc2d1
porostů	porost	k1gInPc2
k	k	k7c3
podhorským	podhorský	k2eAgFnPc3d1
přepásaným	přepásaný	k2eAgFnPc3d1
loukám	louka	k1gFnPc3
a	a	k8xC
nejrůznějším	různý	k2eAgInPc3d3
typům	typ	k1gInPc3
drobných	drobné	k1gInPc2
lesních	lesní	k2eAgInPc2d1
i	i	k8xC
lučních	luční	k2eAgInPc2d1
mokřadů	mokřad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
pojmem	pojem	k1gInSc7
především	především	k6eAd1
jako	jako	k8xC,k8xS
území	území	k1gNnSc4
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
diverzitou	diverzita	k1gFnSc7
a	a	k8xC
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
kvantitou	kvantita	k1gFnSc7
vstavačovitých	vstavačovitý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
(	(	kIx(
<g/>
orchidejí	orchidea	k1gFnPc2
<g/>
)	)	kIx)
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
i	i	k8xC
kulturní	kulturní	k2eAgInPc1d1
faktory	faktor	k1gInPc1
tak	tak	k6eAd1
vytvářejí	vytvářet	k5eAaImIp3nP
z	z	k7c2
Bílých	bílý	k2eAgInPc2d1
Karpat	Karpaty	k1gInPc2
území	území	k1gNnSc2
mimořádně	mimořádně	k6eAd1
cenné	cenný	k2eAgFnPc1d1
i	i	k8xC
v	v	k7c6
evropském	evropský	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Geomorfologie	geomorfologie	k1gFnSc1
</s>
<s>
CHKO	CHKO	kA
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nP
v	v	k7c6
sousedících	sousedící	k2eAgInPc6d1
geomorfologických	geomorfologický	k2eAgInPc6d1
celcích	celek	k1gInPc6
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
a	a	k8xC
Vizovická	vizovický	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
nadřazenou	nadřazený	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
je	být	k5eAaImIp3nS
geomorfologická	geomorfologický	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Slovensko-moravské	slovensko-moravský	k2eAgFnSc2d1
Karpaty	Karpaty	k1gInPc4
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Z	z	k7c2
geologického	geologický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
flyšové	flyšový	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
s	s	k7c7
převahou	převaha	k1gFnSc7
pískovců	pískovec	k1gInPc2
<g/>
,	,	kIx,
slepenců	slepenec	k1gInPc2
a	a	k8xC
jílovců	jílovec	k1gInPc2
z	z	k7c2
období	období	k1gNnSc2
paleocénu	paleocén	k1gInSc2
až	až	k8xS
spodního	spodní	k2eAgInSc2d1
eocénu	eocén	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazné	výrazný	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
zde	zde	k6eAd1
mají	mít	k5eAaImIp3nP
kromě	kromě	k7c2
pískovců	pískovec	k1gInPc2
a	a	k8xC
slepenců	slepenec	k1gInPc2
i	i	k9
jílovce	jílovec	k1gInPc4
z	z	k7c2
období	období	k1gNnSc2
vrchní	vrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
až	až	k9
paleocén	paleocén	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flyšové	Flyšové	k2eAgInSc4d1
pásmo	pásmo	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Bílých	bílý	k2eAgInPc2d1
Karpat	Karpaty	k1gInPc2
tvořeno	tvořit	k5eAaImNgNnS
magurským	magurský	k2eAgInSc7d1
příkrovem	příkrov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
příkrovovou	příkrovový	k2eAgFnSc7d1
bělokarpatskou	bělokarpatský	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
bystrické	bystrický	k2eAgFnSc6d1
jednotce	jednotka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reliéfu	reliéf	k1gInSc6
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
projevují	projevovat	k5eAaImIp3nP
odolné	odolný	k2eAgInPc4d1
vápence	vápenec	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
jurských	jurský	k2eAgFnPc2d1
usazenin	usazenina	k1gFnPc2
bradlového	bradlový	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vodstvo	vodstvo	k1gNnSc1
</s>
<s>
Většina	většina	k1gFnSc1
území	území	k1gNnSc2
CHKO	CHKO	kA
patří	patřit	k5eAaImIp3nS
k	k	k7c3
povodí	povodí	k1gNnSc3
řek	řeka	k1gFnPc2
Váh	Váh	k1gInSc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3
přítoky	přítok	k1gInPc7
Váhu	Váh	k1gInSc2
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Vlára	Vlára	k1gFnSc1
</s>
<s>
Biela	Biela	k1gFnSc1
voda	voda	k1gFnSc1
</s>
<s>
Klanečnica	Klanečnica	k6eAd1
</s>
<s>
Bošáčka	Bošáčka	k1gFnSc1
</s>
<s>
Drietomica	Drietomica	k6eAd1
</s>
<s>
Vlára	Vlára	k1gFnSc1
zpětnou	zpětný	k2eAgFnSc7d1
erozí	eroze	k1gFnSc7
pronikla	proniknout	k5eAaPmAgFnS
do	do	k7c2
Vizovických	vizovický	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
říčním	říční	k2eAgNnSc7d1
pirátstvím	pirátství	k1gNnSc7
zmocnila	zmocnit	k5eAaPmAgFnS
části	část	k1gFnPc4
povodí	povodí	k1gNnSc2
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Velička	Velička	k1gFnSc1
u	u	k7c2
osady	osada	k1gFnSc2
Čerešnické	Čerešnický	k2eAgInPc1d1
mlýny	mlýn	k1gInPc1
<g/>
,	,	kIx,
Nová	nový	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
</s>
<s>
Do	do	k7c2
Moravy	Morava	k1gFnSc2
z	z	k7c2
oblasti	oblast	k1gFnSc2
vtékají	vtékat	k5eAaImIp3nP
:	:	kIx,
</s>
<s>
Olšava	Olšava	k1gFnSc1
</s>
<s>
Chvojnica	Chvojnica	k6eAd1
</s>
<s>
Myjava	Myjava	k1gFnSc1
</s>
<s>
Velička	Velička	k1gFnSc1
</s>
<s>
Okluky	Okluk	k1gInPc1
</s>
<s>
Flyšový	Flyšový	k2eAgInSc1d1
charakter	charakter	k1gInSc1
pohoří	pohoří	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
způsobuje	způsobovat	k5eAaImIp3nS
nedostatečné	dostatečný	k2eNgNnSc4d1
vsakování	vsakování	k1gNnSc4
podzemních	podzemní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc4
celkově	celkově	k6eAd1
mělký	mělký	k2eAgInSc4d1
oběh	oběh	k1gInSc4
<g/>
,	,	kIx,
způsobuje	způsobovat	k5eAaImIp3nS
nevyrovnaný	vyrovnaný	k2eNgInSc4d1
průtok	průtok	k1gInSc4
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc1d3
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
jarním	jarní	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Klima	klima	k1gNnSc1
</s>
<s>
Klimatická	klimatický	k2eAgFnSc1d1
rajonizace	rajonizace	k1gFnSc1
vyčleňuje	vyčleňovat	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
všechny	všechen	k3xTgMnPc4
3	#num#	k4
klimatické	klimatický	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
a	a	k8xC
několik	několik	k4yIc4
klimatických	klimatický	k2eAgFnPc2d1
podoblastí	podoblast	k1gFnPc2
(	(	kIx(
<g/>
zejména	zejména	k9
mírně	mírně	k6eAd1
teplá	teplat	k5eAaImIp3nS
10	#num#	k4
a	a	k8xC
mírně	mírně	k6eAd1
teplá	teplat	k5eAaImIp3nS
9	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
začleněna	začlenit	k5eAaPmNgFnS
do	do	k7c2
mírně	mírně	k6eAd1
teplé	teplý	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
s	s	k7c7
krátkým	krátké	k1gNnSc7
mírně	mírně	k6eAd1
suchým	suchý	k2eAgNnSc7d1
létem	léto	k1gNnSc7
(	(	kIx(
<g/>
průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
v	v	k7c6
červenci	červenec	k1gInSc6
16	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
počet	počet	k1gInSc1
letních	letní	k2eAgInPc2d1
dnů	den	k1gInPc2
30	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mírným	mírný	k2eAgInSc7d1
jarem	jar	k1gInSc7
(	(	kIx(
<g/>
průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
v	v	k7c6
dubnu	duben	k1gInSc6
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
mírným	mírný	k2eAgInSc7d1
podzimem	podzim	k1gInSc7
(	(	kIx(
<g/>
průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
v	v	k7c6
říjnu	říjen	k1gInSc6
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zima	zima	k1gFnSc1
je	být	k5eAaImIp3nS
normálně	normálně	k6eAd1
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
,	,	kIx,
mírně	mírně	k6eAd1
chladná	chladný	k2eAgFnSc1d1
se	s	k7c7
sněhovou	sněhový	k2eAgFnSc7d1
pokrývkou	pokrývka	k1gFnSc7
spíše	spíše	k9
kratší	krátký	k2eAgFnSc1d2
(	(	kIx(
<g/>
60	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
v	v	k7c6
lednu	leden	k1gInSc6
−	−	k?
až	až	k9
−	−	k?
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
v	v	k7c6
níže	nízce	k6eAd2
položených	položený	k2eAgNnPc6d1
místech	místo	k1gNnPc6
−	−	k?
až	až	k8xS
−	−	k?
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
8,1	8,1	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
průměrný	průměrný	k2eAgInSc1d1
úhrn	úhrn	k1gInSc1
ročních	roční	k2eAgFnPc2d1
srážek	srážka	k1gFnPc2
je	být	k5eAaImIp3nS
752	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
Lesy	les	k1gInPc1
</s>
<s>
V	v	k7c6
lesích	les	k1gInPc6
zde	zde	k6eAd1
hojně	hojně	k6eAd1
roste	růst	k5eAaImIp3nS
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
robur	robura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
petraea	petrae	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeřáb	jeřáb	k1gInSc1
břek	břek	k1gInSc1
(	(	kIx(
<g/>
Sorbus	Sorbus	k1gInSc1
torminalis	torminalis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
buk	buk	k1gInSc4
lesní	lesní	k2eAgInSc4d1
(	(	kIx(
<g/>
Fagus	Fagus	k1gInSc4
sylvatica	sylvatic	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
habr	habr	k1gInSc1
obecný	obecný	k2eAgInSc1d1
(	(	kIx(
<g/>
Carpinus	Carpinus	k1gInSc1
betulus	betulus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
javor	javor	k1gInSc1
klen	klen	k2eAgInSc1d1
(	(	kIx(
<g/>
Acer	Acer	k1gInSc1
pseudoplatanus	pseudoplatanus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Velmi	velmi	k6eAd1
vzácně	vzácně	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
dub	dub	k1gInSc1
šípák	šípák	k1gInSc1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
pubescens	pubescens	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Lesnatost	lesnatost	k1gFnSc1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
45	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Flóra	Flóra	k1gFnSc1
</s>
<s>
Bělokarpatské	Bělokarpatský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
jsou	být	k5eAaImIp3nP
bohaté	bohatý	k2eAgInPc1d1
na	na	k7c4
chráněné	chráněný	k2eAgInPc4d1
druhy	druh	k1gInPc4
orchidejí	orchidea	k1gFnPc2
<g/>
:	:	kIx,
roste	růst	k5eAaImIp3nS
zde	zde	k6eAd1
vstavač	vstavač	k1gInSc1
mužský	mužský	k2eAgInSc1d1
(	(	kIx(
<g/>
Orchis	Orchis	k1gInSc1
mascula	masculum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vstavač	vstavač	k1gInSc1
kukačka	kukačka	k1gFnSc1
(	(	kIx(
<g/>
Anacamptis	Anacamptis	k1gFnSc1
morio	morio	k6eAd1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
Orchis	Orchis	k1gFnSc1
morio	morio	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vstavač	vstavač	k1gInSc1
vojenský	vojenský	k2eAgInSc1d1
(	(	kIx(
<g/>
Orchis	Orchis	k1gInSc1
militaris	militaris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prstnatec	prstnatec	k1gMnSc1
májový	májový	k2eAgMnSc1d1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Dactylorhiza	Dactylorhiza	k1gFnSc1
majalis	majalis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prstnatec	prstnatec	k1gMnSc1
pleťový	pleťový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Dactylorhiza	Dactylorhiz	k1gMnSc4
incarnata	incarnat	k1gMnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pětiprstka	pětiprstka	k1gFnSc1
žežulník	žežulník	k1gInSc1
(	(	kIx(
<g/>
Gymnadenia	Gymnadenium	k1gNnSc2
conopsea	conopse	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pětiprstka	pětiprstka	k1gFnSc1
hustokvětá	hustokvětý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Gymnadenia	Gymnadenium	k1gNnSc2
densiflora	densiflora	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
střevíčník	střevíčník	k1gInSc1
pantoflíček	pantoflíček	k1gInSc1
(	(	kIx(
<g/>
Cypripedium	Cypripedium	k1gNnSc1
calceolus	calceolus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hlízovec	hlízovec	k1gInSc4
Loeselův	Loeselův	k2eAgInSc4d1
(	(	kIx(
<g/>
Liparis	Liparis	k1gInSc4
loeselii	loeselie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vemeník	vemeník	k1gInSc4
dvoulistý	dvoulistý	k2eAgInSc4d1
(	(	kIx(
<g/>
Platanthera	Platanthera	k1gFnSc1
bifolia	bifolia	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
hlístník	hlístník	k1gInSc1
hnízdák	hnízdák	k1gInSc1
(	(	kIx(
<g/>
Neottia	Neottia	k1gFnSc1
nidus-avis	nidus-avis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dalších	další	k2eAgFnPc2d1
vzácných	vzácný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
zde	zde	k6eAd1
roste	růst	k5eAaImIp3nS
např.	např.	kA
záraza	záraza	k1gFnSc1
bílá	bílý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Orobanche	Orobanche	k1gFnSc1
alba	album	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
len	len	k1gInSc1
tenkolistý	tenkolistý	k2eAgInSc1d1
(	(	kIx(
<g/>
Linum	Linum	k1gInSc1
tenuifolium	tenuifolium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bílojetel	bílojetet	k5eAaImAgInS,k5eAaPmAgInS,k5eAaBmAgInS
bylinný	bylinný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Dorycnium	Dorycnium	k1gNnSc1
herbaceum	herbaceum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
hořeček	hořeček	k1gInSc1
žlutavý	žlutavý	k2eAgInSc1d1
(	(	kIx(
<g/>
Gentianella	Gentianella	k1gFnSc1
lutescens	lutescensa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Současná	současný	k2eAgFnSc1d1
fauna	fauna	k1gFnSc1
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
bohatá	bohatý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
50	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
%	%	kIx~
fauny	fauna	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
že	že	k8xS
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
20	#num#	k4
tisíc	tisíc	k4xCgInPc2
živočišných	živočišný	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
nejméně	málo	k6eAd3
16	#num#	k4
tisíc	tisíc	k4xCgInPc2
druhů	druh	k1gInPc2
hmyzu	hmyz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
brouků	brouk	k1gMnPc2
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
tesařík	tesařík	k1gMnSc1
alpský	alpský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Rosalia	Rosalia	k1gFnSc1
alpina	alpinum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
roháč	roháč	k1gInSc1
obecný	obecný	k2eAgInSc1d1
(	(	kIx(
<g/>
Lucanus	Lucanus	k1gInSc1
cervus	cervus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
krasec	krasec	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Anthaxia	Anthaxius	k1gMnSc4
hungarica	hungaricus	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
nosatec	nosatec	k1gMnSc1
(	(	kIx(
<g/>
Donus	Donus	k1gMnSc1
vienensis	vienensis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
motýlů	motýl	k1gMnPc2
zde	zde	k6eAd1
najdeme	najít	k5eAaPmIp1nP
např.	např.	kA
perleťovce	perleťovec	k1gMnSc2
dvouřadého	dvouřadý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Brenthis	Brenthis	k1gInSc1
hecate	hecat	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
modráska	modrásek	k1gMnSc2
hořcového	hořcový	k2eAgMnSc2d1
(	(	kIx(
<g/>
Maculinea	Maculineus	k1gMnSc2
alcon	alcon	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
modráska	modrásek	k1gMnSc2
očkovaného	očkovaný	k2eAgMnSc2d1
(	(	kIx(
<g/>
Maculinea	Maculineus	k1gMnSc2
teleius	teleius	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vřetenušku	vřetenuška	k1gFnSc4
ligrusovou	ligrusový	k2eAgFnSc4d1
(	(	kIx(
<g/>
Zygaena	Zygaen	k2eAgMnSc4d1
carniolica	carniolicus	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
martináče	martináč	k1gMnSc2
hrušňového	hrušňový	k2eAgMnSc2d1
(	(	kIx(
<g/>
Saturnia	Saturnium	k1gNnSc2
pyri	pyr	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
žije	žít	k5eAaImIp3nS
kudlanka	kudlanka	k1gFnSc1
nábožná	nábožný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Mantis	mantisa	k1gFnPc2
religiosa	religiosa	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
pavouků	pavouk	k1gMnPc2
sklípkánek	sklípkánek	k1gMnSc1
černý	černý	k1gMnSc1
(	(	kIx(
<g/>
Atypus	Atypus	k1gMnSc1
piceus	piceus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
vzácní	vzácný	k2eAgMnPc1d1
sekáči	sekáč	k1gMnPc1
(	(	kIx(
<g/>
Zacheus	Zacheus	k1gMnSc1
crista	crista	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
(	(	kIx(
<g/>
Egaenus	Egaenus	k1gMnSc1
convexus	convexus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
obojživelníků	obojživelník	k1gMnPc2
v	v	k7c6
CHKO	CHKO	kA
najdeme	najít	k5eAaPmIp1nP
např.	např.	kA
tyto	tento	k3xDgInPc4
druhy	druh	k1gInPc4
ropuchu	ropucha	k1gFnSc4
zelenou	zelený	k2eAgFnSc4d1
(	(	kIx(
<g/>
Bufo	bufa	k1gFnSc5
viridis	viridis	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ropuchu	ropucha	k1gFnSc4
obecnou	obecný	k2eAgFnSc4d1
(	(	kIx(
<g/>
Bufo	bufa	k1gFnSc5
bufo	bufa	k1gFnSc5
<g/>
)	)	kIx)
a	a	k8xC
skokana	skokan	k1gMnSc2
hnědého	hnědý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Rana	Rana	k1gFnSc1
temporaria	temporarium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
kuňku	kuňka	k1gFnSc4
žlutobříchou	žlutobříchý	k2eAgFnSc4d1
(	(	kIx(
<g/>
Bombina	Bombin	k2eAgMnSc4d1
variegata	variegat	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
mloka	mlok	k1gMnSc2
skvrnitého	skvrnitý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Salamandra	salamandr	k1gMnSc2
salamandra	salamandr	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
plazů	plaz	k1gMnPc2
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
ještěrka	ještěrka	k1gFnSc1
živorodá	živorodý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Zootoca	Zootoc	k2eAgFnSc1d1
vivipara	vivipara	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slepýš	slepýš	k1gMnSc1
křehký	křehký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Anguis	Anguis	k1gInSc1
fragilis	fragilis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
užovka	užovka	k1gFnSc1
obojková	obojkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Natrix	Natrix	k1gInSc1
natrix	natrix	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
užovka	užovka	k1gFnSc1
stromová	stromový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Zamenis	Zamenis	k1gFnSc1
longissimus	longissimus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
velmi	velmi	k6eAd1
vzácně	vzácně	k6eAd1
i	i	k9
zmije	zmije	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Vipera	Vipera	k1gFnSc1
berus	berus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
ptáků	pták	k1gMnPc2
zde	zde	k6eAd1
najdeme	najít	k5eAaPmIp1nP
koroptve	koroptev	k1gFnPc4
polní	polní	k2eAgFnPc4d1
(	(	kIx(
<g/>
Perdix	Perdix	k1gInSc4
perdix	perdix	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
křepelky	křepelka	k1gFnPc4
polní	polní	k2eAgFnPc4d1
(	(	kIx(
<g/>
Coturnix	Coturnix	k1gInSc4
coturnix	coturnix	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
chřástala	chřástal	k1gMnSc2
polního	polní	k2eAgMnSc2d1
(	(	kIx(
<g/>
Crex	Crex	k1gInSc4
crex	crex	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lindušku	linduška	k1gFnSc4
lesní	lesní	k2eAgFnSc4d1
(	(	kIx(
<g/>
Anthus	Anthus	k1gInSc4
trivialis	trivialis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strnada	strnad	k1gMnSc2
lučního	luční	k2eAgMnSc2d1
(	(	kIx(
<g/>
Emberiza	Emberiz	k1gMnSc2
calandra	calandr	k1gMnSc2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
<g/>
či	či	k8xC
bramborníčka	bramborníček	k1gMnSc2
hnědého	hnědý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Saxicola	Saxicola	k1gFnSc1
rubetra	rubetra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
holuba	holub	k1gMnSc4
doupňáka	doupňák	k1gMnSc4
(	(	kIx(
<g/>
Columba	Columba	k1gMnSc1
oenas	oenas	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
krutihlava	krutihlav	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
(	(	kIx(
<g/>
Jynx	Jynx	k1gInSc1
torquilla	torquillo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strakapouda	strakapoud	k1gMnSc2
velkého	velký	k2eAgMnSc2d1
(	(	kIx(
<g/>
Dendrocopos	Dendrocopos	k1gInSc1
major	major	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strakapouda	strakapoud	k1gMnSc2
prostředního	prostřední	k2eAgMnSc2d1
(	(	kIx(
<g/>
Dendrocopos	Dendrocopos	k1gInSc1
medius	medius	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strakapouda	strakapoud	k1gMnSc2
bělohřbětého	bělohřbětý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Dendrocopos	Dendrocopos	k1gInSc1
leucotos	leucotos	k1gInSc1
<g/>
)	)	kIx)
či	či	k8xC
datla	datel	k1gMnSc2
černého	černý	k1gMnSc2
(	(	kIx(
<g/>
Dryocopus	Dryocopus	k1gMnSc1
martius	martius	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dravců	dravec	k1gMnPc2
zde	zde	k6eAd1
hnízdí	hnízdit	k5eAaImIp3nS
káně	káně	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Buteo	Buteo	k1gMnSc1
buteo	buteo	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jestřáb	jestřáb	k1gMnSc1
lesní	lesní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Accipiter	Accipiter	k1gMnSc1
gentil	gentit	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
)	)	kIx)
a	a	k8xC
včelojed	včelojed	k1gMnSc1
lesní	lesní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Pernis	Pernis	k1gInSc1
apivorus	apivorus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ze	z	k7c2
savců	savec	k1gMnPc2
zde	zde	k6eAd1
žijí	žít	k5eAaImIp3nP
ježek	ježek	k1gMnSc1
východní	východní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Erinaceus	Erinaceus	k1gMnSc1
concolor	concolor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bělozubka	bělozubka	k1gFnSc1
bělobřichá	bělobřichat	k5eAaImIp3nS,k5eAaPmIp3nS
(	(	kIx(
<g/>
Crocidura	Crocidura	k1gFnSc1
leucodon	leucodon	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
plch	plch	k1gMnSc1
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Glis	Glis	k1gInSc1
glis	glis	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
netopýrů	netopýr	k1gMnPc2
zde	zde	k6eAd1
žijí	žít	k5eAaImIp3nP
vrápenec	vrápenec	k1gMnSc1
malý	malý	k1gMnSc1
(	(	kIx(
<g/>
Rhinolophus	Rhinolophus	k1gInSc1
hipposideros	hipposiderosa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netopýr	netopýr	k1gMnSc1
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Myotis	Myotis	k1gInSc1
myotis	myotis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netopýr	netopýr	k1gMnSc1
ušatý	ušatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Pletocus	Pletocus	k1gMnSc1
auritus	auritus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netopýr	netopýr	k1gMnSc1
dlouhouchý	dlouhouchý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Plecotus	Plecotus	k1gMnSc1
austriacus	austriacus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netopýr	netopýr	k1gMnSc1
večerní	večerní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Eptesicus	Eptesicus	k1gMnSc1
serotinus	serotinus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
netopýr	netopýr	k1gMnSc1
hvízdavý	hvízdavý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Pipistrellus	Pipistrellus	k1gMnSc1
pipistrellus	pipistrellus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
velkých	velký	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
se	se	k3xPyFc4
sem	sem	k6eAd1
ze	z	k7c2
Slovenska	Slovensko	k1gNnSc2
zatoulávají	zatoulávat	k5eAaImIp3nP
rys	rys	k1gInSc4
ostrovid	ostrovid	k1gInSc1
(	(	kIx(
<g/>
Lynx	Lynx	k1gInSc1
lynx	lynx	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vlk	vlk	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Canis	Canis	k1gInSc1
lupus	lupus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
medvěd	medvěd	k1gMnSc1
hnědý	hnědý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Ursus	Ursus	k1gMnSc1
arctos	arctos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Maloplošná	Maloplošný	k2eAgFnSc1d1
zvláště	zvláště	k6eAd1
chráněná	chráněný	k2eAgFnSc1d1
území	území	k1gNnSc6
</s>
<s>
(	(	kIx(
<g/>
stav	stav	k1gInSc4
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
52	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
(	(	kIx(
<g/>
NPR	NPR	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čertotyje	Čertotýt	k5eAaPmIp3nS
(	(	kIx(
<g/>
325,578	325,578	k4
<g/>
5	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Javorina	Javorina	k1gFnSc1
(	(	kIx(
<g/>
165,870	165,870	k4
<g/>
2	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Jazevčí	Jazevčí	k1gNnSc1
(	(	kIx(
<g/>
99,280	99,280	k4
<g/>
4	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Porážky	porážka	k1gFnPc1
(	(	kIx(
<g/>
49,760	49,760	k4
<g/>
5	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Zahrady	zahrada	k1gFnPc1
pod	pod	k7c7
Hájem	háj	k1gInSc7
(	(	kIx(
<g/>
162,332	162,332	k4
<g/>
6	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
(	(	kIx(
<g/>
NPP	NPP	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Búrová	Búrová	k1gFnSc1
(	(	kIx(
<g/>
18,810	18,810	k4
<g/>
1	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
(	(	kIx(
<g/>
PR	pr	k0
<g/>
)	)	kIx)
(	(	kIx(
<g/>
16	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bílé	bílý	k2eAgInPc1d1
potoky	potok	k1gInPc1
(	(	kIx(
<g/>
8,78	8,78	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Dolnoněmčanské	Dolnoněmčanský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
(	(	kIx(
<g/>
28,816	28,816	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Drahy	Draha	k1gFnPc1
(	(	kIx(
<g/>
15,079	15,079	k4
<g/>
7	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Hladké	Hladká	k1gFnPc1
(	(	kIx(
<g/>
38,407	38,407	k4
<g/>
6	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Horní	horní	k2eAgFnPc1d1
louky	louka	k1gFnPc1
(	(	kIx(
<g/>
6,291	6,291	k4
<g/>
9	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Hutě	huť	k1gFnPc1
(	(	kIx(
<g/>
12,3	12,3	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Jalovcová	jalovcový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
(	(	kIx(
<g/>
8,635	8,635	k4
<g/>
4	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Javorůvky	Javorůvka	k1gFnPc1
(	(	kIx(
<g/>
5,467	5,467	k4
<g/>
3	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Kútky	Kútka	k1gFnPc1
(	(	kIx(
<g/>
66,422	66,422	k4
<g/>
3	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Lazy	Lazy	k?
(	(	kIx(
<g/>
3,121	3,121	k4
<g/>
8	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Machová	Machová	k1gFnSc1
(	(	kIx(
<g/>
118,19	118,19	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
29,607	29,607	k4
<g/>
1	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Ploščiny	ploščin	k2eAgInPc4d1
(	(	kIx(
<g/>
19,29	19,29	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Pod	pod	k7c7
Žitkovským	Žitkovský	k2eAgInSc7d1
vrchem	vrch	k1gInSc7
(	(	kIx(
<g/>
16,057	16,057	k4
<g/>
2	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Sidonie	Sidonie	k1gFnSc1
(	(	kIx(
<g/>
13,06	13,06	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c4
Vlčí	vlčí	k2eAgInPc4d1
(	(	kIx(
<g/>
21,682	21,682	k4
<g/>
9	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
(	(	kIx(
<g/>
PP	PP	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
30	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bahulské	Bahulský	k2eAgInPc1d1
jamy	jam	k1gInPc1
(	(	kIx(
<g/>
14,090	14,090	k4
<g/>
8	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Cestiska	Cestiska	k1gFnSc1
(	(	kIx(
<g/>
2,983	2,983	k4
<g/>
2	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Dobšena	Dobšen	k2eAgFnSc1d1
(	(	kIx(
<g/>
1,486	1,486	k4
<g/>
1	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Dubiny	dubina	k1gFnPc1
(	(	kIx(
<g/>
1,365	1,365	k4
<g/>
7	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Grun	Grun	k1gNnSc1
(	(	kIx(
<g/>
3,456	3,456	k4
<g/>
2	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Hluboče	Hluboče	k1gNnSc1
(	(	kIx(
<g/>
2,58	2,58	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Hrnčárky	Hrnčárka	k1gFnPc1
(	(	kIx(
<g/>
4,045	4,045	k4
<g/>
8	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Chladný	chladný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
2,58	2,58	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Chmelinec	Chmelinec	k1gInSc1
(	(	kIx(
<g/>
2,761	2,761	k4
<g/>
6	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Kalábová	Kalábová	k1gFnSc1
(	(	kIx(
<g/>
0,5824	0,5824	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Kalábová	Kalábová	k1gFnSc1
2	#num#	k4
(	(	kIx(
<g/>
0,2489	0,2489	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Kaňúry	Kaňúra	k1gFnPc1
(	(	kIx(
<g/>
13,437	13,437	k4
<g/>
1	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Lom	lom	k1gInSc1
Rasová	rasový	k2eAgFnSc1d1
(	(	kIx(
<g/>
4,433	4,433	k4
<g/>
2	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Mechnáčky	Mechnáček	k1gInPc1
(	(	kIx(
<g/>
9,653	9,653	k4
<g/>
1	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Mravenčí	mravenčí	k2eAgFnSc1d1
louka	louka	k1gFnSc1
(	(	kIx(
<g/>
15,352	15,352	k4
<g/>
7	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
louky	louka	k1gFnPc1
(	(	kIx(
<g/>
12,958	12,958	k4
<g/>
8	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
luční	luční	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Okrouhlá	okrouhlý	k2eAgFnSc1d1
(	(	kIx(
<g/>
11,81	11,81	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Pod	pod	k7c7
Cigánem	cigán	k1gMnSc7
(	(	kIx(
<g/>
0,2550	0,2550	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Pod	pod	k7c7
Hribovňou	Hribovňa	k1gFnSc7
(	(	kIx(
<g/>
6,646	6,646	k4
<g/>
4	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Pod	pod	k7c7
Vrchy	vrch	k1gInPc7
(	(	kIx(
<g/>
1,208	1,208	k4
<g/>
3	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Sviní	sviní	k2eAgNnSc1d1
hnízdo	hnízdo	k1gNnSc1
(	(	kIx(
<g/>
5,34	5,34	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Šumlatová	Šumlatový	k2eAgFnSc1d1
(	(	kIx(
<g/>
0,8229	0,8229	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
U	u	k7c2
Petrůvky	Petrůvka	k1gFnSc2
(	(	kIx(
<g/>
2,27	2,27	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
U	u	k7c2
zvonice	zvonice	k1gFnSc2
(	(	kIx(
<g/>
1,27	1,27	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Uvezené	uvezený	k2eAgInPc4d1
(	(	kIx(
<g/>
14,39	14,39	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
Krátkých	Krátká	k1gFnPc6
(	(	kIx(
<g/>
1,365	1,365	k4
<g/>
7	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Vápenky	vápenka	k1gFnPc1
(	(	kIx(
<g/>
10,6	10,6	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Za	za	k7c7
lesem	les	k1gInSc7
(	(	kIx(
<g/>
1,22	1,22	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Záhumenice	záhumenice	k1gFnSc1
(	(	kIx(
<g/>
11,005	11,005	k4
<g/>
6	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Žerotín	Žerotín	k1gInSc1
(	(	kIx(
<g/>
1,858	1,858	k4
<g/>
8	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Památné	památný	k2eAgInPc1d1
stromy	strom	k1gInPc1
</s>
<s>
Adamcova	Adamcův	k2eAgFnSc1d1
oskeruše	oskeruše	k1gFnSc1
<g/>
,	,	kIx,
evidenční	evidenční	k2eAgFnSc1d1
č.	č.	k?
<g/>
:	:	kIx,
100965	#num#	k4
<g/>
,	,	kIx,
jeřáb	jeřáb	k1gInSc4
oskeruše	oskeruše	k1gFnSc2
(	(	kIx(
<g/>
Sorbus	Sorbus	k1gMnSc1
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
400	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
o	o	k7c4
nejstarší	starý	k2eAgInSc4d3
jeřáb	jeřáb	k1gInSc4
oskeruši	oskeruše	k1gFnSc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
ČR	ČR	kA
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
460	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
stromu	strom	k1gInSc2
<g/>
:	:	kIx,
17	#num#	k4
m	m	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Strážnice	Strážnice	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
4	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Žerotín	Žerotín	k1gInSc1
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tomečkova	Tomečkův	k2eAgFnSc1d1
oskoruša	oskoruša	k1gFnSc1
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
104862	#num#	k4
<g/>
,	,	kIx,
jeřáb	jeřáb	k1gInSc4
oskeruše	oskeruše	k1gFnSc2
(	(	kIx(
<g/>
Sorbus	Sorbus	k1gMnSc1
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
250	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
278	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
stromu	strom	k1gInSc2
<g/>
:	:	kIx,
16	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Tvarožná	Tvarožný	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
14,16	14,16	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
20,64	20,64	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Špirudova	Špirudův	k2eAgFnSc1d1
oskeruše	oskeruše	k1gFnSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100960	#num#	k4
<g/>
,	,	kIx,
jeřáb	jeřáb	k1gInSc4
oskeruše	oskeruše	k1gFnSc2
(	(	kIx(
<g/>
Sorbus	Sorbus	k1gMnSc1
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
350	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
403	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
12	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Tvarožná	Tvarožný	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
10,61	10,61	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
22,61	22,61	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dub	dub	k1gInSc1
v	v	k7c6
Jiříkovci	Jiříkovec	k1gInSc6
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100964	#num#	k4
<g/>
,	,	kIx,
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
robur	robura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
420	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
470	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
22	#num#	k4
m	m	kA
<g/>
,,	,,	k?
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Tvarožná	Tvarožný	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
14	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obecní	obecní	k2eAgInSc1d1
dřín	dřín	k1gInSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
105153	#num#	k4
<g/>
,	,	kIx,
dřín	dřín	k1gInSc1
obecný	obecný	k2eAgInSc1d1
(	(	kIx(
<g/>
Cornus	Cornus	k1gInSc1
mas	masa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
60	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
347	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
7	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Tvarožná	Tvarožný	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
50,2	50,2	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kněždubské	Kněždubský	k2eAgFnPc1d1
lípy	lípa	k1gFnPc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100966	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
lípy	lípa	k1gFnSc2
velkolisté	velkolistý	k2eAgNnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
platyphyllos	platyphyllos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Kněždub	Kněždub	k1gInSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
58	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nejedlíkova	Nejedlíkův	k2eAgFnSc1d1
oskeruše	oskeruše	k1gFnSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100962	#num#	k4
<g/>
,	,	kIx,
jeřáb	jeřáb	k1gInSc4
oskeruše	oskeruše	k1gFnSc2
(	(	kIx(
<g/>
Sorbus	Sorbus	k1gMnSc1
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
200	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
316	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
20	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Kněždub	Kněždub	k1gInSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hruška	hruška	k1gFnSc1
Pavlových	Pavlových	k2eAgFnSc1d1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
105361	#num#	k4
<g/>
,	,	kIx,
hrušeň	hrušeň	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Pyrus	Pyrus	k1gInSc1
communis	communis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
150	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
258	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
13	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Korytná	Korytný	k2eAgFnSc1d1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Topol	topol	k1gInSc1
u	u	k7c2
Volenova	Volenův	k2eAgInSc2d1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100686	#num#	k4
<g/>
,	,	kIx,
topol	topol	k1gInSc1
šedavý	šedavý	k2eAgInSc1d1
(	(	kIx(
<g/>
Populus	Populus	k1gInSc1
canescens	canescens	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
150	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
715	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
29	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Suchá	Suchá	k1gFnSc1
Loz	loza	k1gFnPc2
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
49	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dub	Dub	k1gMnSc1
u	u	k7c2
Kamenné	kamenný	k2eAgFnSc2d1
búdy	búda	k1gFnSc2
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100689	#num#	k4
<g/>
,	,	kIx,
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
robur	robura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
220	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
378	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
23	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Strání	stráň	k1gFnPc2
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
28	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Buk	buk	k1gInSc1
u	u	k7c2
Zámečku	zámeček	k1gInSc2
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100665	#num#	k4
<g/>
,	,	kIx,
buk	buk	k1gInSc1
lesní	lesní	k2eAgFnSc2d1
převislý	převislý	k2eAgInSc4d1
(	(	kIx(
<g/>
Fagus	Fagus	k1gInSc4
sylvatica	sylvatic	k1gInSc2
'	'	kIx"
<g/>
Pendula	Pendula	k1gFnSc1
<g/>
'	'	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
275	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
13	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Strání	stráň	k1gFnPc2
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
8	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lípy	lípa	k1gFnPc1
u	u	k7c2
kapličky	kaplička	k1gFnSc2
<g/>
,	,	kIx,
ev.	ev.	k?
č	č	k0
<g/>
:	:	kIx,
100664	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
+	+	kIx~
1	#num#	k4
lípa	lípa	k1gFnSc1
srdčitá	srdčitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
platyphyllos	platyphyllos	k1gMnSc1
<g/>
)	)	kIx)
+	+	kIx~
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
cordata	cordata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Strání	stráň	k1gFnPc2
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
39	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bojkovský	Bojkovský	k2eAgInSc1d1
dub	dub	k1gInSc1
nad	nad	k7c7
kostelem	kostel	k1gInSc7
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100671	#num#	k4
<g/>
,	,	kIx,
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
robur	robura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
'	'	kIx"
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
400	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
489	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
21	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Bojkovice	Bojkovice	k1gFnPc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnPc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
28	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lípa	lípa	k1gFnSc1
v	v	k7c6
Ohrádce	ohrádka	k1gFnSc6
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
105504	#num#	k4
<g/>
,	,	kIx,
lípa	lípa	k1gFnSc1
srdčitá	srdčitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
cordata	cordata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
300	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
425	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
14	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Hostětín	Hostětín	k1gInSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
12	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
33	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lípa	lípa	k1gFnSc1
v	v	k7c6
Šanově	Šanov	k1gInSc6
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100988	#num#	k4
<g/>
,	,	kIx,
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
platyphyllos	platyphyllos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
200	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
515	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
21	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Šanov	Šanov	k1gInSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
53	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Klen	klen	k1gInSc1
ve	v	k7c4
Vlčí	vlčí	k2eAgNnSc4d1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
105084	#num#	k4
<g/>
,	,	kIx,
javor	javor	k1gInSc1
klen	klen	k1gInSc1
(	(	kIx(
<g/>
Acer	Acer	k1gMnSc1
pseudoplatanus	pseudoplatanus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
250	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
508	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
27	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Vyškovec	Vyškovec	k1gInSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lípa	lípa	k1gFnSc1
u	u	k7c2
Šamáků	Šamák	k1gInPc2
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
105083	#num#	k4
<g/>
,	,	kIx,
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
platyphyllos	platyphyllos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
200	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
513	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
27	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Vyškovec	Vyškovec	k1gInSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
<g/>
56	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
19	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mandincova	Mandincův	k2eAgFnSc1d1
lípa	lípa	k1gFnSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100683	#num#	k4
<g/>
,	,	kIx,
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
platyphyllos	platyphyllos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
300	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
551	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
25	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Vyškovec	Vyškovec	k1gInSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
14	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rapantova	Rapantův	k2eAgFnSc1d1
lípa	lípa	k1gFnSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100684	#num#	k4
<g/>
,	,	kIx,
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
platyphyllos	platyphyllos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
200	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
375	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
24	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Vyškovec	Vyškovec	k1gInSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
31	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Františkův	Františkův	k2eAgInSc1d1
břek	břek	k1gInSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100688	#num#	k4
<g/>
,	,	kIx,
jeřáb	jeřáb	k1gInSc1
břek	břek	k1gInSc1
(	(	kIx(
<g/>
Sorbus	Sorbus	k1gInSc1
torminalis	torminalis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
150	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
380	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
18	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Vápenice	vápenice	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
31	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Janův	Janův	k2eAgInSc1d1
buk	buk	k1gInSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100687	#num#	k4
<g/>
,	,	kIx,
buk	buk	k1gInSc4
lesní	lesní	k2eAgInSc4d1
(	(	kIx(
<g/>
Fagus	Fagus	k1gInSc4
sylvatica	sylvatic	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
200	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
584	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
24	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Vápenice	vápenice	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
31	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lípa	lípa	k1gFnSc1
u	u	k7c2
Dvora	Dvůr	k1gInSc2
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
104719	#num#	k4
<g/>
,	,	kIx,
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
platyphyllos	platyphyllos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
300	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
490	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
24	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Vlachovice	Vlachovice	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kalitův	Kalitův	k2eAgInSc1d1
dub	dub	k1gInSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100991	#num#	k4
<g/>
,	,	kIx,
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
robur	robura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
300	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
509	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
29	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Lipina	lipina	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
56	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lípa	lípa	k1gFnSc1
Jaruška	Jaruška	k1gFnSc1
v	v	k7c6
Lipině	lipina	k1gFnSc6
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
105154	#num#	k4
<g/>
,	,	kIx,
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
platyphyllos	platyphyllos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
150	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
322	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
22	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Lipina	lipina	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
3	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Čtyřbuk	Čtyřbuk	k6eAd1
u	u	k7c2
Slavičína	Slavičín	k1gInSc2
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
105383	#num#	k4
<g/>
,	,	kIx,
buk	buk	k1gInSc4
lesní	lesní	k2eAgInSc4d1
(	(	kIx(
<g/>
Fagus	Fagus	k1gInSc4
sylvatica	sylvatic	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
200	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
428	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
30	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Lipová	lipový	k2eAgFnSc1d1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
22	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
lípy	lípa	k1gFnSc2
u	u	k7c2
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
101019	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
lípy	lípa	k1gFnSc2
velkolisté	velkolistý	k2eAgFnSc2d1
+	+	kIx~
2	#num#	k4
lípy	lípa	k1gFnSc2
srdčité	srdčitý	k2eAgNnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
platyphyllos	platyphyllos	k1gMnSc1
<g/>
)	)	kIx)
+	+	kIx~
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
cordata	cordata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
200	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnPc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pechancův	Pechancův	k2eAgInSc1d1
dub	dub	k1gInSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100990	#num#	k4
<g/>
,	,	kIx,
dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
petraea	petrae	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
200	#num#	k4
let	léto	k1gNnPc2
<g/>
:	:	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
371	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
29	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnPc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
35	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
22	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Návojský	Návojský	k2eAgInSc1d1
dub	dub	k1gInSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
100989	#num#	k4
<g/>
,	,	kIx,
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
robur	robura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
300	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
465	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
23	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Návojná	Návojný	k2eAgFnSc1d1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dub	dub	k1gInSc1
na	na	k7c6
Paseckých	Pasecká	k1gFnPc6
Lazech	Lazech	k?
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
105382	#num#	k4
<g/>
,	,	kIx,
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
robur	robura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
300	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
507	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
20	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Nedašova	Nedašův	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
3	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
53	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Karlův	Karlův	k2eAgInSc1d1
dub	dub	k1gInSc1
<g/>
,	,	kIx,
ev.	ev.	k?
č.	č.	k?
<g/>
:	:	kIx,
105381	#num#	k4
<g/>
,	,	kIx,
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
robur	robura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
300	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
488	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
25	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Nedašova	Nedašův	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
49	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
49	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
33	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
rok	rok	k1gInSc1
vyhlášení	vyhlášení	k1gNnSc2
<g/>
:	:	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bělokarpatské	Bělokarpatský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Bělokarpatské	Bělokarpatský	k2eAgFnSc2d1
louky	louka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
od	od	k7c2
svého	svůj	k3xOyFgNnSc2
vyhlášení	vyhlášení	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
se	se	k3xPyFc4
ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
v	v	k7c6
CHKO	CHKO	kA
zaměřuje	zaměřovat	k5eAaImIp3nS
především	především	k9
na	na	k7c4
ochranu	ochrana	k1gFnSc4
luk	louka	k1gFnPc2
a	a	k8xC
lučních	luční	k2eAgNnPc2d1
společenstev	společenstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
42	#num#	k4
lučních	luční	k2eAgNnPc2d1
maloplošných	maloplošný	k2eAgNnPc2d1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
s	s	k7c7
celkovou	celkový	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
1	#num#	k4
127	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jednosečné	jednosečný	k2eAgNnSc4d1
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
při	při	k7c6
nadměrných	nadměrný	k2eAgFnPc6d1
srážkách	srážka	k1gFnPc6
i	i	k9
dvousečné	dvousečný	k2eAgFnPc1d1
louky	louka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
dosažení	dosažení	k1gNnSc4
maximální	maximální	k2eAgFnSc2d1
druhové	druhový	k2eAgFnSc2d1
biodiverzity	biodiverzita	k1gFnSc2
na	na	k7c6
loukách	louka	k1gFnPc6
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
kosit	kosit	k5eAaImF
je	být	k5eAaImIp3nS
postupně	postupně	k6eAd1
<g/>
,	,	kIx,
mozaikovitě	mozaikovitě	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Louky	louka	k1gFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
jako	jako	k9
polopřirozená	polopřirozený	k2eAgFnSc1d1
náhradní	náhradní	k2eAgFnSc1d1
vegetace	vegetace	k1gFnSc1
na	na	k7c6
místě	místo	k1gNnSc6
původních	původní	k2eAgFnPc2d1
teplomilných	teplomilný	k2eAgFnPc2d1
doubrav	doubrava	k1gFnPc2
<g/>
,	,	kIx,
dubohabřin	dubohabřina	k1gFnPc2
a	a	k8xC
bučin	bučina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odlesněné	odlesněný	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
se	se	k3xPyFc4
pod	pod	k7c7
vlivem	vliv	k1gInSc7
seče	seč	k1gFnSc2
a	a	k8xC
pastvy	pastva	k1gFnSc2
proměnily	proměnit	k5eAaPmAgFnP
v	v	k7c4
louky	louka	k1gFnPc4
a	a	k8xC
pastviny	pastvina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
útlumu	útlum	k1gInSc3
zemědělské	zemědělský	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
a	a	k8xC
obnovování	obnovování	k1gNnSc2
luk	louka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
bylo	být	k5eAaImAgNnS
zatravněno	zatravnit	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
7	#num#	k4
000	#num#	k4
ha	ha	kA
orné	orný	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
loukách	louka	k1gFnPc6
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
přes	přes	k7c4
800	#num#	k4
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
27	#num#	k4
je	být	k5eAaImIp3nS
kriticky	kriticky	k6eAd1
ohrožených	ohrožený	k2eAgMnPc2d1
<g/>
,	,	kIx,
34	#num#	k4
silně	silně	k6eAd1
ohrožených	ohrožený	k2eAgMnPc2d1
a	a	k8xC
32	#num#	k4
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zde	zde	k6eAd1
také	také	k9
nalezeno	naleznout	k5eAaPmNgNnS,k5eAaBmNgNnS
přes	přes	k7c4
400	#num#	k4
druhů	druh	k1gInPc2
mechorostů	mechorost	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
Biele	Biel	k1gMnSc2
Karpaty	Karpaty	k1gInPc4
je	být	k5eAaImIp3nS
situace	situace	k1gFnSc1
značně	značně	k6eAd1
odlišná	odlišný	k2eAgFnSc1d1
<g/>
,	,	kIx,
správa	správa	k1gFnSc1
CHKO	CHKO	kA
pečuje	pečovat	k5eAaImIp3nS
jen	jen	k9
asi	asi	k9
o	o	k7c4
100	#num#	k4
ha	ha	kA
luk	louka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc1d3
část	část	k1gFnSc1
její	její	k3xOp3gFnSc2
péče	péče	k1gFnSc2
se	se	k3xPyFc4
tam	tam	k6eAd1
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
ochranu	ochrana	k1gFnSc4
vápencových	vápencový	k2eAgNnPc2d1
bradel	bradla	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
Čertoryje	Čertoryje	k1gFnSc2
</s>
<s>
Kopec	kopec	k1gInSc1
Lesná	lesný	k2eAgFnSc1d1
</s>
<s>
Kopec	kopec	k1gInSc1
Lesná	lesný	k2eAgFnSc1d1
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Bahulské	Bahulský	k2eAgFnSc2d1
jamy	jam	k1gInPc5
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
Porážky	porážka	k1gFnSc2
</s>
<s>
Výhled	výhled	k1gInSc1
směr	směr	k1gInSc1
Květná	květný	k2eAgNnPc5d1
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Mechnáčky	Mechnáček	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
pozadí	pozadí	k1gNnSc6
Velká	velký	k2eAgFnSc1d1
Javořina	Javořina	k1gFnSc1
a	a	k8xC
Jelenec	Jelenec	k1gInSc1
</s>
<s>
Panorama	panorama	k1gNnSc1
</s>
<s>
360	#num#	k4
<g/>
°	°	k?
panoramatický	panoramatický	k2eAgInSc1d1
pohled	pohled	k1gInSc1
z	z	k7c2
kopce	kopec	k1gInSc2
Lesná	lesný	k2eAgFnSc1d1
při	při	k7c6
východu	východ	k1gInSc6
slunce	slunce	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
CHKO	CHKO	kA
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
</s>
<s>
Euroregion	euroregion	k1gInSc1
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
</s>
<s>
Ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
</s>
<s>
Slovácko	Slovácko	k1gNnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Bílé	bílý	k2eAgInPc4d1
Karpaty	Karpaty	k1gInPc4
</s>
<s>
MZCHÚ	MZCHÚ	kA
CHKO	CHKO	kA
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
</s>
<s>
Škodová	Škodová	k1gFnSc1
I.	I.	kA
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetace	vegetace	k1gFnSc1
Bílých	bílý	k2eAgInPc2d1
Karpat	Karpaty	k1gInPc2
<g/>
,	,	kIx,
2008	#num#	k4
</s>
<s>
Fajmon	Fajmon	k1gInSc1
K.	K.	kA
<g/>
,	,	kIx,
Konvička	konvička	k1gFnSc1
O.	O.	kA
a	a	k8xC
Jongpierová	Jongpierová	k1gFnSc1
I.	I.	kA
-	-	kIx~
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Bílé	bílý	k2eAgFnSc2d1
Karpaty	Karpaty	k1gInPc1
třicetiletá	třicetiletý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c4
CHKO	CHKO	kA
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Čertoryje	Čertorýt	k5eAaPmIp3nS
•	•	k?
Javorina	Javorina	k1gMnSc1
•	•	k?
Jazevčí	Jazevčí	k1gMnSc1
•	•	k?
Porážky	porážka	k1gFnSc2
•	•	k?
Zahrady	zahrada	k1gFnSc2
pod	pod	k7c7
Hájem	háj	k1gInSc7
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Búrová	Búrový	k2eAgFnSc1d1
Přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
</s>
<s>
Bílé	bílý	k2eAgInPc1d1
potoky	potok	k1gInPc1
•	•	k?
Dolnoněmčanské	Dolnoněmčanský	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Drahy	Draha	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Hutě	huť	k1gFnSc2
•	•	k?
Jalovcová	jalovcový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Javorůvky	Javorůvka	k1gFnSc2
•	•	k?
Kútky	Kútka	k1gFnSc2
•	•	k?
Lazy	Lazy	k?
•	•	k?
Machová	Machová	k1gFnSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Ploščiny	ploščin	k2eAgFnPc4d1
•	•	k?
Pod	pod	k7c7
Žitkovským	Žitkovský	k2eAgInSc7d1
vrchem	vrch	k1gInSc7
•	•	k?
Sidonie	Sidonie	k1gFnSc1
•	•	k?
Ve	v	k7c4
Vlčí	vlčí	k2eAgFnPc4d1
Přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
</s>
<s>
Bahulské	Bahulský	k2eAgInPc4d1
jamy	jam	k1gInPc4
•	•	k?
Cestiska	Cestiska	k1gFnSc1
•	•	k?
Dobšena	Dobšen	k2eAgFnSc1d1
•	•	k?
Dubiny	dubina	k1gFnSc2
•	•	k?
Grun	Grun	k1gMnSc1
•	•	k?
Hluboče	Hluboč	k1gFnSc2
•	•	k?
Hrnčárky	Hrnčárka	k1gFnSc2
•	•	k?
Chladný	chladný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Chmelinec	Chmelinec	k1gInSc1
•	•	k?
Kalábová	Kalábová	k1gFnSc1
•	•	k?
Kaňoury	kaňour	k1gMnPc4
•	•	k?
Lom	lom	k1gInSc1
Rasová	rasový	k2eAgFnSc1d1
•	•	k?
Mechnáčky	Mechnáček	k1gInPc4
•	•	k?
Mravenčí	mravenčí	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Nové	Nová	k1gFnSc2
louky	louka	k1gFnSc2
•	•	k?
Okrouhlá	okrouhlý	k2eAgFnSc1d1
•	•	k?
Pod	pod	k7c7
Cigánem	cigán	k1gMnSc7
•	•	k?
Pod	pod	k7c4
Hribovňou	Hribovňá	k1gFnSc4
•	•	k?
Pod	pod	k7c7
Vrchy	vrch	k1gInPc7
•	•	k?
Sviní	sviní	k2eAgNnSc1d1
hnízdo	hnízdo	k1gNnSc1
•	•	k?
Šumlatová	Šumlatový	k2eAgFnSc1d1
•	•	k?
U	u	k7c2
Petrůvky	Petrůvka	k1gFnSc2
•	•	k?
Uvezené	uvezený	k2eAgNnSc1d1
•	•	k?
U	u	k7c2
zvonice	zvonice	k1gFnSc2
•	•	k?
Vápenky	vápenka	k1gFnSc2
•	•	k?
V	v	k7c6
Krátkých	Krátkých	k2eAgInSc6d1
•	•	k?
Záhumenice	záhumenice	k1gFnSc1
•	•	k?
Za	za	k7c7
lesem	les	k1gInSc7
•	•	k?
Žerotín	Žerotín	k1gInSc1
•	•	k?
Žleb	žleb	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Hodonín	Hodonín	k1gInSc1
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
Přírodní	přírodní	k2eAgInSc1d1
park	park	k1gInSc4
</s>
<s>
Mikulčický	Mikulčický	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Strážnické	strážnický	k2eAgNnSc4d1
Pomoraví	Pomoraví	k1gNnSc4
•	•	k?
Ždánický	ždánický	k2eAgInSc1d1
les	les	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Čertoryje	Čertorýt	k5eAaPmIp3nS
•	•	k?
Jazevčí	Jazevčí	k1gMnSc1
•	•	k?
Porážky	porážka	k1gFnSc2
•	•	k?
Zahrady	zahrada	k1gFnSc2
pod	pod	k7c7
Hájem	háj	k1gInSc7
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Búrová	Búrový	k2eAgFnSc1d1
•	•	k?
Hodonínská	hodonínský	k2eAgFnSc1d1
Dúbrava	Dúbrava	k1gFnSc1
•	•	k?
Na	na	k7c4
Adamcích	Adamec	k1gMnPc6
•	•	k?
Váté	vátý	k2eAgInPc4d1
písky	písek	k1gInPc4
u	u	k7c2
Bzence	Bzenec	k1gInSc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Háj	háj	k1gInSc1
u	u	k7c2
Louky	louka	k1gFnSc2
•	•	k?
Hloží	hloží	k1gNnSc1
•	•	k?
Kútky	Kútka	k1gFnSc2
•	•	k?
Machová	Machová	k1gFnSc1
•	•	k?
Moravanské	Moravanský	k2eAgFnPc4d1
lúky	lúka	k1gFnPc4
•	•	k?
Oskovec	Oskovec	k1gMnSc1
•	•	k?
Oskovec	Oskovec	k1gMnSc1
II	II	kA
•	•	k?
Písečný	písečný	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Skařiny	Skařina	k1gFnSc2
•	•	k?
Sovince	Sovinec	k1gInSc2
•	•	k?
Stupava	Stupava	k1gFnSc1
•	•	k?
U	u	k7c2
Vrby	Vrba	k1gMnSc2
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Bílý	bílý	k2eAgInSc1d1
kopec	kopec	k1gInSc1
u	u	k7c2
Čejče	Čejč	k1gFnSc2
•	•	k?
Bohuslavické	Bohuslavický	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
•	•	k?
Borky	borka	k1gFnSc2
•	•	k?
Háj	háj	k1gInSc1
u	u	k7c2
Lipova	Lipov	k1gInSc2
•	•	k?
Horky	horka	k1gFnSc2
•	•	k?
Hošťálka	Hošťálka	k1gFnSc1
•	•	k?
Hovoranské	Hovoranský	k2eAgFnPc4d1
louky	louka	k1gFnPc4
•	•	k?
Jezero	jezero	k1gNnSc1
•	•	k?
Letiště	letiště	k1gNnSc1
Milotice	Milotice	k1gFnSc2
•	•	k?
Losky	Loska	k1gFnSc2
•	•	k?
Miliovy	Miliův	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Nad	nad	k7c7
Vápenkou	vápenka	k1gFnSc7
•	•	k?
Nivky	nivka	k1gFnSc2
za	za	k7c4
Větřákem	Větřákem	k?
•	•	k?
Očovské	Očovský	k2eAgFnPc4d1
louky	louka	k1gFnPc4
•	•	k?
Ochozy	ochoz	k1gInPc1
•	•	k?
Osypané	osypaný	k2eAgInPc1d1
břehy	břeh	k1gInPc1
•	•	k?
Pánov	Pánov	k1gInSc1
•	•	k?
Střečkův	střečkův	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Špidláky	Špidlák	k1gInPc1
•	•	k?
Vojenské	vojenský	k2eAgNnSc1d1
cvičiště	cvičiště	k1gNnSc1
Bzenec	Bzenec	k1gInSc1
•	•	k?
Výchoz	výchoz	k1gInSc1
•	•	k?
Vypálenky	Vypálenka	k1gFnSc2
•	•	k?
Zápověď	zápověď	k1gFnSc4
u	u	k7c2
Karlína	Karlín	k1gInSc2
•	•	k?
Žerotín	Žerotín	k1gMnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Uherské	uherský	k2eAgNnSc4d1
Hradiště	Hradiště	k1gNnSc4
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
Přírodní	přírodní	k2eAgInSc1d1
park	park	k1gInSc4
</s>
<s>
Chřiby	Chřiby	k1gInPc1
•	•	k?
Prakšická	Prakšický	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Javorina	Javorina	k1gFnSc1
•	•	k?
Porážky	porážka	k1gFnSc2
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Dolnoněmčanské	Dolnoněmčanský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
•	•	k?
Drahy	Draha	k1gFnSc2
•	•	k?
Hladké	Hladká	k1gFnSc2
•	•	k?
Holý	holý	k2eAgInSc4d1
kopec	kopec	k1gInSc4
•	•	k?
Horní	horní	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Hutě	huť	k1gFnSc2
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Kobylí	kobylí	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
•	•	k?
Kolébky	kolébka	k1gFnSc2
•	•	k?
Kovářův	Kovářův	k2eAgInSc1d1
žleb	žleb	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Žitkovským	Žitkovský	k2eAgInSc7d1
vrchem	vrch	k1gInSc7
•	•	k?
Rovná	rovnat	k5eAaImIp3nS
hora	hora	k1gFnSc1
•	•	k?
Smutný	smutný	k2eAgInSc1d1
žleb	žleb	k1gInSc1
•	•	k?
Trnovec	Trnovec	k1gInSc1
•	•	k?
Ve	v	k7c6
Vlčí	vlčí	k2eAgFnSc6d1
•	•	k?
Vlčnovský	Vlčnovský	k2eAgInSc4d1
háj	háj	k1gInSc4
•	•	k?
Vrchové	vrchový	k2eAgFnSc2d1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Babí	babí	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Bahulské	Bahulský	k2eAgInPc4d1
jamy	jam	k1gInPc4
•	•	k?
Barborka	Barborka	k1gFnSc1
•	•	k?
Břestecká	Břestecký	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Cestiska	Cestiska	k1gFnSc1
•	•	k?
Čerťák	Čerťák	k1gInSc1
•	•	k?
Dubiny	dubina	k1gFnSc2
•	•	k?
Grun	Grun	k1gInSc1
•	•	k?
Hrádek	hrádek	k1gInSc1
•	•	k?
Hrnčárky	Hrnčárka	k1gFnSc2
•	•	k?
Hříštek	Hříštek	k1gMnSc1
•	•	k?
Huštěnovická	Huštěnovický	k2eAgNnPc4d1
ramena	rameno	k1gNnPc4
•	•	k?
Chmelinec	Chmelinec	k1gInSc1
•	•	k?
Ježovský	Ježovský	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Kalábová	Kalábová	k1gFnSc1
•	•	k?
Kalábová	Kalábová	k1gFnSc1
2	#num#	k4
•	•	k?
Koukolky	Koukolka	k1gFnSc2
•	•	k?
Lázeňský	lázeňský	k2eAgInSc1d1
mokřad	mokřad	k1gInSc1
•	•	k?
Lom	lom	k1gInSc1
Rasová	rasový	k2eAgFnSc1d1
•	•	k?
Máchova	Máchův	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Makovica	Makovica	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Maršava	Maršava	k1gFnSc1
•	•	k?
Medlovický	Medlovický	k2eAgInSc4d1
lom	lom	k1gInSc4
•	•	k?
Mechnáčky	Mechnáček	k1gInPc4
•	•	k?
Mokřad	mokřad	k1gInSc4
u	u	k7c2
Slováckých	slovácký	k2eAgFnPc2d1
strojíren	strojírna	k1gFnPc2
•	•	k?
Mravenčí	mravenčit	k5eAaImIp3nS
louka	louka	k1gFnSc1
•	•	k?
Myšince	myšinec	k1gInSc2
•	•	k?
Nádavky	nádavek	k1gInPc1
•	•	k?
Nazaret	Nazaret	k1gInSc4
•	•	k?
Nové	Nové	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Okluky	Okluk	k1gInPc4
•	•	k?
Olšava	Olšava	k1gFnSc1
<g/>
•	•	k?
Ovčírka	Ovčírka	k1gFnSc1
•	•	k?
Pod	pod	k7c4
Hribovňou	Hribovňá	k1gFnSc4
•	•	k?
Pod	pod	k7c7
Husí	husí	k2eAgFnSc7d1
horou	hora	k1gFnSc7
•	•	k?
Remízy	remíza	k1gFnSc2
u	u	k7c2
Bánova	bánův	k2eAgNnSc2d1
•	•	k?
Rochus	Rochus	k1gInSc1
•	•	k?
Salašské	Salašský	k2eAgFnSc6d1
pěnovce	pěnovka	k1gFnSc6
•	•	k?
Skalky	skalka	k1gFnSc2
•	•	k?
Sviní	sviní	k2eAgNnSc1d1
hnízdo	hnízdo	k1gNnSc1
•	•	k?
Terasy	terasa	k1gFnSc2
•	•	k?
Tůň	tůň	k1gFnSc1
u	u	k7c2
Kostelan	Kostelana	k1gFnPc2
•	•	k?
U	u	k7c2
zvonice	zvonice	k1gFnSc1
•	•	k?
Údolí	údolí	k1gNnSc2
Bánovského	Bánovský	k1gMnSc2
potoka	potok	k1gInSc2
•	•	k?
Údolí	údolí	k1gNnSc6
Okluky	Okluk	k1gMnPc4
•	•	k?
Újezdecký	Újezdecký	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Uvezené	uvezený	k2eAgFnSc2d1
•	•	k?
V	v	k7c6
Krátkých	Krátká	k1gFnPc6
•	•	k?
Vápenky	vápenka	k1gFnSc2
•	•	k?
Za	za	k7c7
lesem	les	k1gInSc7
•	•	k?
záhumenice	záhumenice	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Zlín	Zlín	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Chřiby	Chřiby	k1gInPc1
•	•	k?
Vizovické	vizovický	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Želechovické	Želechovický	k2eAgFnSc2d1
paseky	paseka	k1gFnSc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bílé	bílý	k2eAgInPc1d1
potoky	potok	k1gInPc1
•	•	k?
Bukové	bukový	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Jalovcová	jalovcový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Javorůvky	Javorůvka	k1gFnSc2
•	•	k?
Lazy	Lazy	k?
•	•	k?
Ploščiny	ploščin	k2eAgFnSc2d1
•	•	k?
Sidonie	Sidonie	k1gFnSc2
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Bezedník	Bezedník	k1gMnSc1
•	•	k?
Budačina	Budačina	k1gFnSc1
•	•	k?
Bzová	Bzová	k1gFnSc1
•	•	k?
Čertův	čertův	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Dobšena	Dobšen	k2eAgMnSc4d1
•	•	k?
Hluboče	Hluboč	k1gInSc2
•	•	k?
Holíkova	Holíkův	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
•	•	k?
Hrádek	hrádek	k1gInSc1
•	•	k?
Chladná	chladný	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Chladný	chladný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Jalovcová	jalovcová	k1gFnSc1
louka	louka	k1gFnSc1
•	•	k?
Kaňoury	kaňour	k1gMnPc4
•	•	k?
Králky	králka	k1gFnSc2
•	•	k?
Na	na	k7c6
letišti	letiště	k1gNnSc6
•	•	k?
Na	na	k7c6
želechovických	želechovický	k2eAgFnPc6d1
pasekách	paseka	k1gFnPc6
•	•	k?
Okrouhlá	okrouhlý	k2eAgFnSc1d1
•	•	k?
Ondřejovsko	Ondřejovsko	k1gNnSc4
•	•	k?
Pernikářská	Pernikářský	k2eAgFnSc1d1
•	•	k?
Pod	pod	k7c7
Cigánem	cigán	k1gMnSc7
•	•	k?
Pod	pod	k7c7
Drdolem	drdol	k1gInSc7
•	•	k?
Pod	pod	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
lázněmi	lázeň	k1gFnPc7
•	•	k?
Pod	pod	k7c7
Obecním	obecní	k2eAgInSc7d1
kopcem	kopec	k1gInSc7
•	•	k?
Pod	pod	k7c7
Vrchy	vrch	k1gInPc7
•	•	k?
Podskaličí	Podskaličí	k2eAgNnSc1d1
•	•	k?
Polichno	Polichno	k1gNnSc1
-	-	kIx~
Pod	pod	k7c4
Duby	dub	k1gInPc4
•	•	k?
Průkopa	Průkopa	k1gFnSc1
•	•	k?
Sirnaté	sirnatý	k2eAgFnPc4d1
lázně	lázeň	k1gFnPc4
•	•	k?
Skály	skála	k1gFnPc4
•	•	k?
Slanice	Slanice	k1gFnSc1
pramen	pramen	k1gInSc1
•	•	k?
Slanice	Slanice	k1gFnSc1
studna	studna	k1gFnSc1
•	•	k?
Smolinka	smolinka	k1gFnSc1
•	•	k?
Solisko	solisko	k1gNnSc1
•	•	k?
Šumlatová	Šumlatový	k2eAgFnSc1d1
•	•	k?
Tlumačovská	Tlumačovská	k1gFnSc1
tůňka	tůňka	k1gFnSc1
•	•	k?
U	u	k7c2
Petrůvky	Petrůvka	k1gFnSc2
•	•	k?
U	u	k7c2
rybníka	rybník	k1gInSc2
•	•	k?
Uhliska	Uhlisko	k1gNnSc2
•	•	k?
Vela	velo	k1gNnSc2
•	•	k?
Zelené	Zelené	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
•	•	k?
Želechovické	Želechovický	k2eAgFnSc2d1
paseky	paseka	k1gFnSc2
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128397	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
1193156565715423500001	#num#	k4
</s>
