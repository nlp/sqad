<p>
<s>
Antické	antický	k2eAgFnSc2d1	antická
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
byly	být	k5eAaImAgInP	být
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
z	z	k7c2	z
všeřeckých	všeřecký	k2eAgFnPc2d1	všeřecký
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
pořádány	pořádat	k5eAaImNgInP	pořádat
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
do	do	k7c2	do
přelomu	přelom	k1gInSc2	přelom
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Konaly	konat	k5eAaImAgInP	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
nebo	nebo	k8xC	nebo
v	v	k7c6	v
září	září	k1gNnSc6	září
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
<g/>
.	.	kIx.	.
</s>
<s>
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
upevňovaly	upevňovat	k5eAaImAgInP	upevňovat
národní	národní	k2eAgFnSc4d1	národní
jednotu	jednota	k1gFnSc4	jednota
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc4	první
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
roku	rok	k1gInSc2	rok
776	[number]	k4	776
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
pak	pak	k6eAd1	pak
8	[number]	k4	8
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
her	hra	k1gFnPc2	hra
i	i	k8xC	i
řeckého	řecký	k2eAgInSc2d1	řecký
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Legendy	legenda	k1gFnPc1	legenda
a	a	k8xC	a
báje	báj	k1gFnPc1	báj
však	však	k9	však
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
sportovních	sportovní	k2eAgNnPc6d1	sportovní
kláních	klání	k1gNnPc6	klání
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
rozkvět	rozkvět	k1gInSc1	rozkvět
OH	OH	kA	OH
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
klasické	klasický	k2eAgNnSc1d1	klasické
období	období	k1gNnSc1	období
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
skládány	skládat	k5eAaImNgFnP	skládat
i	i	k9	i
ódy	óda	k1gFnPc1	óda
na	na	k7c4	na
vítěze	vítěz	k1gMnSc4	vítěz
(	(	kIx(	(
<g/>
epiníkie	epiníkie	k1gFnSc2	epiníkie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
závodili	závodit	k5eAaImAgMnP	závodit
na	na	k7c6	na
OH	OH	kA	OH
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
profesionálové	profesionál	k1gMnPc1	profesionál
(	(	kIx(	(
<g/>
athlétés	athlétés	k6eAd1	athlétés
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příslušníci	příslušník	k1gMnPc1	příslušník
nižších	nízký	k2eAgFnPc2d2	nižší
vrstev	vrstva	k1gFnPc2	vrstva
vycvičení	vycvičení	k1gNnPc2	vycvičení
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
disciplíně	disciplína	k1gFnSc6	disciplína
(	(	kIx(	(
<g/>
atlet	atlet	k1gMnSc1	atlet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
množí	množit	k5eAaImIp3nP	množit
se	se	k3xPyFc4	se
i	i	k9	i
případy	případ	k1gInPc1	případ
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
soutěžili	soutěžit	k5eAaImAgMnP	soutěžit
na	na	k7c6	na
OH	OH	kA	OH
i	i	k9	i
závodníci	závodník	k1gMnPc1	závodník
neřeckého	řecký	k2eNgInSc2d1	řecký
původu	původ	k1gInSc2	původ
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ovládnutí	ovládnutí	k1gNnSc6	ovládnutí
Řecka	Řecko	k1gNnSc2	Řecko
Římany	Říman	k1gMnPc7	Říman
(	(	kIx(	(
<g/>
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
nastal	nastat	k5eAaPmAgInS	nastat
úpadek	úpadek	k1gInSc1	úpadek
OH	OH	kA	OH
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
rozmachu	rozmach	k1gInSc3	rozmach
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
hry	hra	k1gFnPc1	hra
podporovány	podporovat	k5eAaImNgFnP	podporovat
římskými	římský	k2eAgMnPc7d1	římský
císaři	císař	k1gMnPc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Nero	Nero	k1gMnSc1	Nero
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
aktivně	aktivně	k6eAd1	aktivně
účastnil	účastnit	k5eAaImAgMnS	účastnit
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
menšině	menšina	k1gFnSc6	menšina
<g/>
,	,	kIx,	,
vítězili	vítězit	k5eAaImAgMnP	vítězit
Syřané	Syřan	k1gMnPc1	Syřan
<g/>
,	,	kIx,	,
Féničané	Féničan	k1gMnPc1	Féničan
<g/>
,	,	kIx,	,
Ilyrové	Ilyr	k1gMnPc1	Ilyr
<g/>
,	,	kIx,	,
Afričané	Afričan	k1gMnPc1	Afričan
a	a	k8xC	a
Babyloňané	Babyloňan	k1gMnPc1	Babyloňan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
OH	OH	kA	OH
upadaly	upadat	k5eAaImAgFnP	upadat
<g/>
,	,	kIx,	,
ostře	ostro	k6eAd1	ostro
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
jako	jako	k8xS	jako
pohanským	pohanský	k2eAgFnPc3d1	pohanská
hrám	hra	k1gFnPc3	hra
stavělo	stavět	k5eAaImAgNnS	stavět
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
stalo	stát	k5eAaPmAgNnS	stát
oficiální	oficiální	k2eAgFnSc7d1	oficiální
ideologií	ideologie	k1gFnSc7	ideologie
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
393	[number]	k4	393
či	či	k8xC	či
394	[number]	k4	394
vydal	vydat	k5eAaPmAgMnS	vydat
císař	císař	k1gMnSc1	císař
Theodosius	Theodosius	k1gMnSc1	Theodosius
I.	I.	kA	I.
přísný	přísný	k2eAgInSc1d1	přísný
zákaz	zákaz	k1gInSc1	zákaz
konání	konání	k1gNnSc2	konání
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
OH	OH	kA	OH
snad	snad	k9	snad
ještě	ještě	k6eAd1	ještě
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
konaly	konat	k5eAaImAgFnP	konat
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
však	však	k9	však
pouze	pouze	k6eAd1	pouze
lokální	lokální	k2eAgInSc4d1	lokální
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
426	[number]	k4	426
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
všechny	všechen	k3xTgFnPc1	všechen
budovy	budova	k1gFnPc1	budova
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
z	z	k7c2	z
příkazu	příkaz	k1gInSc2	příkaz
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnSc2	trvání
==	==	k?	==
</s>
</p>
<p>
<s>
Přesné	přesný	k2eAgFnPc1d1	přesná
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
době	doba	k1gFnSc6	doba
trvání	trvání	k1gNnSc2	trvání
her	hra	k1gFnPc2	hra
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnPc1	první
hry	hra	k1gFnPc1	hra
trvaly	trvat	k5eAaImAgFnP	trvat
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
konala	konat	k5eAaImAgFnS	konat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
disciplína	disciplína	k1gFnSc1	disciplína
–	–	k?	–
běh	běh	k1gInSc4	běh
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
stadio	stadio	k1gNnSc4	stadio
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnSc2	trvání
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
počtem	počet	k1gInSc7	počet
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
pak	pak	k6eAd1	pak
zastává	zastávat	k5eAaImIp3nS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
podobě	podoba	k1gFnSc6	podoba
trvaly	trvat	k5eAaImAgFnP	trvat
hry	hra	k1gFnPc1	hra
pět	pět	k4xCc4	pět
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Najdou	najít	k5eAaPmIp3nP	najít
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
tací	tací	k?	tací
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hry	hra	k1gFnSc2	hra
trvaly	trvat	k5eAaImAgFnP	trvat
šest	šest	k4xCc4	šest
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Georg	Georg	k1gInSc1	Georg
von	von	k1gInSc1	von
Hannover	Hannover	k1gInSc1	Hannover
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
uváděné	uváděný	k2eAgNnSc4d1	uváděné
tvrzení	tvrzení	k1gNnSc4	tvrzení
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
trvání	trvání	k1gNnSc6	trvání
sedm	sedm	k4xCc4	sedm
dní	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
překonané	překonaný	k2eAgNnSc4d1	překonané
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Posvátný	posvátný	k2eAgInSc1d1	posvátný
mír	mír	k1gInSc1	mír
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
konání	konání	k1gNnSc2	konání
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
dodržoval	dodržovat	k5eAaImAgInS	dodržovat
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
posvátný	posvátný	k2eAgInSc1d1	posvátný
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
ekecheiria	ekecheirium	k1gNnPc4	ekecheirium
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zdržení	zdržení	k1gNnSc1	zdržení
se	se	k3xPyFc4	se
rukou	ruka	k1gFnSc7	ruka
od	od	k7c2	od
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
Olympie	Olympia	k1gFnSc2	Olympia
nesměl	smět	k5eNaImAgMnS	smět
vstoupit	vstoupit	k5eAaPmF	vstoupit
voják	voják	k1gMnSc1	voják
se	s	k7c7	s
zbraní	zbraň	k1gFnSc7	zbraň
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
konání	konání	k1gNnSc2	konání
her	hra	k1gFnPc2	hra
ani	ani	k8xC	ani
ozbrojený	ozbrojený	k2eAgMnSc1d1	ozbrojený
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Účastníkům	účastník	k1gMnPc3	účastník
her	hra	k1gFnPc2	hra
byla	být	k5eAaImAgFnS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
volná	volný	k2eAgFnSc1d1	volná
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Olympie	Olympia	k1gFnSc2	Olympia
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
i	i	k9	i
přes	přes	k7c4	přes
území	území	k1gNnSc4	území
nepřátelských	přátelský	k2eNgInPc2d1	nepřátelský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
her	hra	k1gFnPc2	hra
byly	být	k5eAaImAgInP	být
zakázány	zakázat	k5eAaPmNgInP	zakázat
všechny	všechen	k3xTgInPc1	všechen
války	válek	k1gInPc1	válek
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
násilné	násilný	k2eAgInPc1d1	násilný
činy	čin	k1gInPc1	čin
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
byla	být	k5eAaImAgFnS	být
napsána	napsán	k2eAgFnSc1d1	napsána
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
text	text	k1gInSc1	text
se	se	k3xPyFc4	se
nedochoval	dochovat	k5eNaPmAgInS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
porušení	porušení	k1gNnSc3	porušení
této	tento	k3xDgFnSc2	tento
dohody	dohoda	k1gFnSc2	dohoda
došlo	dojít	k5eAaPmAgNnS	dojít
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
např.	např.	kA	např.
roku	rok	k1gInSc2	rok
420	[number]	k4	420
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Spartou	Sparta	k1gFnSc7	Sparta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
disciplíny	disciplína	k1gFnPc1	disciplína
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pentathlón	Pentathlón	k1gInSc4	Pentathlón
===	===	k?	===
</s>
</p>
<p>
<s>
Pentathlón	Pentathlón	k1gInSc1	Pentathlón
(	(	kIx(	(
<g/>
pětiboj	pětiboj	k1gInSc1	pětiboj
<g/>
)	)	kIx)	)
přibyl	přibýt	k5eAaPmAgInS	přibýt
na	na	k7c4	na
pořad	pořad	k1gInSc4	pořad
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
708	[number]	k4	708
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
18	[number]	k4	18
<g/>
.	.	kIx.	.
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Lampis	Lampis	k1gInSc1	Lampis
ze	z	k7c2	z
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
38	[number]	k4	38
<g/>
.	.	kIx.	.
hrách	hra	k1gFnPc6	hra
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
pentathlón	pentathlón	k1gInSc1	pentathlón
dorostenců	dorostenec	k1gMnPc2	dorostenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hod	hod	k1gInSc1	hod
diskem	disk	k1gInSc7	disk
====	====	k?	====
</s>
</p>
<p>
<s>
Soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
disk	disk	k1gInSc1	disk
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
původně	původně	k6eAd1	původně
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
zdrsněným	zdrsněný	k2eAgInSc7d1	zdrsněný
pískem	písek	k1gInSc7	písek
<g/>
.	.	kIx.	.
</s>
<s>
Jaké	jaký	k3yIgNnSc1	jaký
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
parametry	parametr	k1gInPc1	parametr
se	se	k3xPyFc4	se
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
disky	disk	k1gInPc7	disk
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
od	od	k7c2	od
16,7	[number]	k4	16,7
do	do	k7c2	do
36	[number]	k4	36
cm	cm	kA	cm
a	a	k8xC	a
váhou	váha	k1gFnSc7	váha
od	od	k7c2	od
1,86	[number]	k4	1,86
do	do	k7c2	do
5,7	[number]	k4	5,7
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
závodě	závod	k1gInSc6	závod
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
jeden	jeden	k4xCgInSc1	jeden
disk	disk	k1gInSc1	disk
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
více	hodně	k6eAd2	hodně
disků	disk	k1gInPc2	disk
o	o	k7c6	o
stejných	stejný	k2eAgInPc6d1	stejný
parametrech	parametr	k1gInPc6	parametr
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
soutěžící	soutěžící	k1gFnPc4	soutěžící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
badatelů	badatel	k1gMnPc2	badatel
se	se	k3xPyFc4	se
kloní	klonit	k5eAaImIp3nS	klonit
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
technika	technika	k1gFnSc1	technika
hodu	hod	k1gInSc2	hod
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
technice	technika	k1gFnSc6	technika
soudobé	soudobý	k2eAgNnSc1d1	soudobé
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
hod	hod	k1gInSc1	hod
z	z	k7c2	z
otočky	otočka	k1gFnSc2	otočka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
zastávají	zastávat	k5eAaImIp3nP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
házelo	házet	k5eAaImAgNnS	házet
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
s	s	k7c7	s
předskokem	předskok	k1gInSc7	předskok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Skok	skok	k1gInSc1	skok
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
====	====	k?	====
</s>
</p>
<p>
<s>
Skok	skok	k1gInSc1	skok
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
neodpovídal	odpovídat	k5eNaImAgMnS	odpovídat
skoku	skok	k1gInSc2	skok
jak	jak	k8xC	jak
jej	on	k3xPp3gInSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
skok	skok	k1gInSc4	skok
se	s	k7c7	s
závažím	závaží	k1gNnSc7	závaží
<g/>
.	.	kIx.	.
</s>
<s>
Atlet	atlet	k1gMnSc1	atlet
se	se	k3xPyFc4	se
rozbíhal	rozbíhat	k5eAaImAgMnS	rozbíhat
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
skokanskou	skokanský	k2eAgFnSc7d1	skokanská
činkou	činka	k1gFnSc7	činka
<g/>
,	,	kIx,	,
haltérou	haltéra	k1gFnSc7	haltéra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odrazu	odraz	k1gInSc6	odraz
přešli	přejít	k5eAaPmAgMnP	přejít
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
letu	let	k1gInSc2	let
skrčmo	skrčmo	k6eAd1	skrčmo
a	a	k8xC	a
doskoku	doskok	k1gInSc2	doskok
sedmo	sedma	k1gFnSc5	sedma
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
soudobé	soudobý	k2eAgFnSc3d1	soudobá
technice	technika	k1gFnSc3	technika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
fázemi	fáze	k1gFnPc7	fáze
prudce	prudko	k6eAd1	prudko
vytrčili	vytrčit	k5eAaPmAgMnP	vytrčit
ruce	ruka	k1gFnPc4	ruka
s	s	k7c7	s
haltérami	haltéra	k1gFnPc7	haltéra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
dopadem	dopad	k1gInSc7	dopad
odhodili	odhodit	k5eAaPmAgMnP	odhodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Běh	běh	k1gInSc1	běh
====	====	k?	====
</s>
</p>
<p>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
běhali	běhat	k5eAaImAgMnP	běhat
nazí	nahý	k2eAgMnPc1d1	nahý
a	a	k8xC	a
bosí	bosý	k2eAgMnPc1d1	bosý
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
neměřil	měřit	k5eNaImAgInS	měřit
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
jen	jen	k9	jen
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
bude	být	k5eAaImBp3nS	být
první	první	k4xOgFnSc6	první
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
a	a	k8xC	a
tentýž	týž	k3xTgMnSc1	týž
závodník	závodník	k1gMnSc1	závodník
mohl	moct	k5eAaImAgMnS	moct
soutěžit	soutěžit	k5eAaImF	soutěžit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
běžeckých	běžecký	k2eAgFnPc6d1	běžecká
disciplínách	disciplína	k1gFnPc6	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Běh	běh	k1gInSc1	běh
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
jednoho	jeden	k4xCgInSc2	jeden
stadia	stadion	k1gNnPc1	stadion
(	(	kIx(	(
<g/>
dromos	dromos	k1gInSc1	dromos
<g/>
,	,	kIx,	,
stadion	stadion	k1gInSc1	stadion
resp.	resp.	kA	resp.
stadiodromos	stadiodromos	k1gInSc4	stadiodromos
192,27	[number]	k4	192,27
m	m	kA	m
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
her	hra	k1gFnPc2	hra
jedinou	jediný	k2eAgFnSc7d1	jediná
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
disciplínou	disciplína	k1gFnSc7	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
hrách	hra	k1gFnPc6	hra
přibyl	přibýt	k5eAaPmAgInS	přibýt
běh	běh	k1gInSc1	běh
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
stadia	stadion	k1gNnPc4	stadion
(	(	kIx(	(
<g/>
diaulos	diaulosa	k1gFnPc2	diaulosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
běh	běh	k1gInSc1	běh
(	(	kIx(	(
<g/>
dolichos	dolichos	k1gInSc1	dolichos
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
dolichodromos	dolichodromos	k1gInSc1	dolichodromos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
zprvu	zprvu	k6eAd1	zprvu
běhal	běhat	k5eAaImAgMnS	běhat
na	na	k7c4	na
7	[number]	k4	7
stadií	stadion	k1gNnPc2	stadion
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
však	však	k9	však
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
nezůstávala	zůstávat	k5eNaImAgFnS	zůstávat
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kolísala	kolísat	k5eAaImAgFnS	kolísat
mezi	mezi	k7c7	mezi
7	[number]	k4	7
a	a	k8xC	a
24	[number]	k4	24
stadii	stadion	k1gNnPc7	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
delších	dlouhý	k2eAgInPc6d2	delší
bězích	běh	k1gInPc6	běh
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dělat	dělat	k5eAaImF	dělat
obrátku	obrátka	k1gFnSc4	obrátka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dráha	dráha	k1gFnSc1	dráha
nebyla	být	k5eNaImAgFnS	být
oválná	oválný	k2eAgNnPc1d1	oválné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
přímá	přímý	k2eAgFnSc1d1	přímá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
37	[number]	k4	37
<g/>
.	.	kIx.	.
hrách	hra	k1gFnPc6	hra
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
programu	program	k1gInSc2	program
zařazen	zařadit	k5eAaPmNgInS	zařadit
běh	běh	k1gInSc1	běh
dorostenců	dorostenec	k1gMnPc2	dorostenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hod	hod	k1gInSc1	hod
oštěpem	oštěp	k1gInSc7	oštěp
====	====	k?	====
</s>
</p>
<p>
<s>
Soutěžilo	soutěžit	k5eAaImAgNnS	soutěžit
se	se	k3xPyFc4	se
v	v	k7c6	v
hodu	hod	k1gInSc6	hod
oštěpem	oštěp	k1gInSc7	oštěp
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
s	s	k7c7	s
rozběhem	rozběh	k1gInSc7	rozběh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
úchopu	úchop	k1gInSc2	úchop
byla	být	k5eAaImAgFnS	být
klička	klička	k1gFnSc1	klička
z	z	k7c2	z
tenkého	tenký	k2eAgInSc2d1	tenký
řemínku	řemínek	k1gInSc2	řemínek
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
vrhač	vrhač	k1gMnSc1	vrhač
strčil	strčit	k5eAaPmAgMnS	strčit
dva	dva	k4xCgInPc4	dva
prsty	prst	k1gInPc4	prst
a	a	k8xC	a
švihem	švih	k1gInSc7	švih
ruky	ruka	k1gFnSc2	ruka
pak	pak	k6eAd1	pak
udělil	udělit	k5eAaPmAgMnS	udělit
oštěpu	oštěp	k1gInSc2	oštěp
větší	veliký	k2eAgFnSc4d2	veliký
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zápas	zápas	k1gInSc4	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
Zápas	zápas	k1gInSc1	zápas
(	(	kIx(	(
<g/>
palé	palé	k1gNnSc1	palé
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
zaveden	zaveden	k2eAgInSc1d1	zaveden
roku	rok	k1gInSc2	rok
708	[number]	k4	708
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Měl	mít	k5eAaImAgInS	mít
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
formy	forma	k1gFnPc4	forma
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
orthopalé	orthopalý	k2eAgFnPc4d1	orthopalý
(	(	kIx(	(
<g/>
stadaipalé	stadaipalý	k2eAgFnPc4d1	stadaipalý
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
rovný	rovný	k2eAgMnSc1d1	rovný
<g/>
,	,	kIx,	,
vzpřímený	vzpřímený	k2eAgMnSc1d1	vzpřímený
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
soutěžilo	soutěžit	k5eAaImAgNnS	soutěžit
vestoje	vestoje	k6eAd1	vestoje
a	a	k8xC	a
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
shodit	shodit	k5eAaPmF	shodit
soupeře	soupeř	k1gMnPc4	soupeř
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
katopalé	katopalý	k2eAgFnPc4d1	katopalý
(	(	kIx(	(
<g/>
kylisis	kylisis	k1gFnPc4	kylisis
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
přízemní	přízemní	k2eAgMnSc1d1	přízemní
<g/>
,	,	kIx,	,
valivý	valivý	k2eAgMnSc1d1	valivý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
probíhal	probíhat	k5eAaImAgInS	probíhat
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
soupeřů	soupeř	k1gMnPc2	soupeř
nevzdal	vzdát	k5eNaPmAgMnS	vzdát
(	(	kIx(	(
<g/>
zvednutím	zvednutí	k1gNnSc7	zvednutí
ukazováčku	ukazováček	k1gInSc2	ukazováček
či	či	k8xC	či
prostředníku	prostředník	k1gInSc2	prostředník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známo	znám	k2eAgNnSc1d1	známo
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
čtyřicet	čtyřicet	k4xCc4	čtyřicet
pojmenovaných	pojmenovaný	k2eAgInPc2d1	pojmenovaný
chvatů	chvat	k1gInPc2	chvat
<g/>
.	.	kIx.	.
<g/>
Zápas	zápas	k1gInSc1	zápas
se	se	k3xPyFc4	se
na	na	k7c4	na
hrách	hrách	k1gInSc4	hrách
objevoval	objevovat	k5eAaImAgInS	objevovat
jednak	jednak	k8xC	jednak
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
pentathlónu	pentathlón	k1gInSc2	pentathlón
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Eurybatos	Eurybatos	k1gMnSc1	Eurybatos
ze	z	k7c2	z
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
37	[number]	k4	37
<g/>
.	.	kIx.	.
hrách	hra	k1gFnPc6	hra
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
zápas	zápas	k1gInSc1	zápas
dorostenců	dorostenec	k1gMnPc2	dorostenec
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Hipposthenés	Hipposthenés	k1gInSc1	Hipposthenés
ze	z	k7c2	z
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
později	pozdě	k6eAd2	pozdě
pětkrát	pětkrát	k6eAd1	pětkrát
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Box	box	k1gInSc4	box
===	===	k?	===
</s>
</p>
<p>
<s>
Box	box	k1gInSc1	box
(	(	kIx(	(
<g/>
pygmé	pygmý	k2eAgFnSc2d1	pygmý
<g/>
,	,	kIx,	,
pýx	pýx	k?	pýx
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
roku	rok	k1gInSc2	rok
688	[number]	k4	688
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
také	také	k9	také
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
doba	doba	k1gFnSc1	doba
konání	konání	k1gNnSc2	konání
her	hra	k1gFnPc2	hra
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
boxu	box	k1gInSc6	box
bylo	být	k5eAaImAgNnS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
vše	všechen	k3xTgNnSc1	všechen
kromě	kromě	k7c2	kromě
chvatů	chvat	k1gInPc2	chvat
<g/>
,	,	kIx,	,
držení	držení	k1gNnSc1	držení
soupeře	soupeř	k1gMnSc2	soupeř
a	a	k8xC	a
úderů	úder	k1gInPc2	úder
na	na	k7c4	na
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Boxeři	boxer	k1gMnPc1	boxer
měli	mít	k5eAaImAgMnP	mít
pěsti	pěst	k1gFnSc6	pěst
ovinuty	ovinut	k2eAgInPc4d1	ovinut
řemeny	řemen	k1gInPc4	řemen
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
navlékali	navlékat	k5eAaImAgMnP	navlékat
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
utkání	utkání	k1gNnSc2	utkání
nastal	nastat	k5eAaPmAgInS	nastat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
vzdal	vzdát	k5eAaPmAgMnS	vzdát
nebo	nebo	k8xC	nebo
zůstal	zůstat	k5eAaPmAgMnS	zůstat
bezvládně	bezvládně	k6eAd1	bezvládně
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouho	dlouho	k6eAd1	dlouho
trvající	trvající	k2eAgFnSc6d1	trvající
nerozhodnosti	nerozhodnost	k1gFnSc6	nerozhodnost
nařídil	nařídit	k5eAaPmAgMnS	nařídit
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
klimax	klimax	k1gInSc1	klimax
-	-	kIx~	-
jeden	jeden	k4xCgInSc1	jeden
dal	dát	k5eAaPmAgInS	dát
úder	úder	k1gInSc4	úder
druhému	druhý	k4xOgInSc3	druhý
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nesměl	smět	k5eNaImAgMnS	smět
krýt	krýt	k5eAaImF	krýt
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc4	ten
vydržel	vydržet	k5eAaPmAgMnS	vydržet
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
toto	tento	k3xDgNnSc4	tento
právo	právo	k1gNnSc4	právo
zase	zase	k9	zase
on	on	k3xPp3gMnSc1	on
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Onomastos	Onomastos	k1gMnSc1	Onomastos
ze	z	k7c2	z
Smyrny	Smyrna	k1gFnSc2	Smyrna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
41	[number]	k4	41
<g/>
.	.	kIx.	.
hrách	hra	k1gFnPc6	hra
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
box	box	k1gInSc1	box
těžké	těžký	k2eAgFnSc2d1	těžká
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pankration	Pankration	k1gInSc4	Pankration
===	===	k?	===
</s>
</p>
<p>
<s>
Pankration	Pankration	k1gInSc1	Pankration
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
všeboj	všeboj	k1gInSc1	všeboj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
roku	rok	k1gInSc2	rok
648	[number]	k4	648
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Šlo	jít	k5eAaImAgNnS	jít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
o	o	k7c6	o
kombinaci	kombinace	k1gFnSc6	kombinace
zápasu	zápas	k1gInSc2	zápas
a	a	k8xC	a
boxu	box	k1gInSc2	box
<g/>
.	.	kIx.	.
</s>
<s>
Dovoleny	dovolen	k2eAgInPc1d1	dovolen
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
hmaty	hmat	k1gInPc1	hmat
a	a	k8xC	a
údery	úder	k1gInPc1	úder
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
nesmělo	smět	k5eNaImAgNnS	smět
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
hryzání	hryzání	k1gNnSc1	hryzání
<g/>
,	,	kIx,	,
škrábání	škrábání	k1gNnSc1	škrábání
a	a	k8xC	a
užití	užití	k1gNnSc1	užití
pěstních	pěstní	k2eAgInPc2d1	pěstní
řemenů	řemen	k1gInPc2	řemen
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgFnP	existovat
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
formy	forma	k1gFnPc4	forma
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vzpřímená	vzpřímený	k2eAgFnSc1d1	vzpřímená
-	-	kIx~	-
ano	ano	k9	ano
(	(	kIx(	(
<g/>
orthostaden	orthostaden	k2eAgInSc4d1	orthostaden
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
přízemní	přízemní	k2eAgFnSc1d1	přízemní
-	-	kIx~	-
katóBojovalo	katóBojovat	k5eAaImAgNnS	katóBojovat
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
soupeřů	soupeř	k1gMnPc2	soupeř
nevzdal	vzdát	k5eNaPmAgMnS	vzdát
nebo	nebo	k8xC	nebo
nepadl	padnout	k5eNaPmAgMnS	padnout
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Běh	běh	k1gInSc1	běh
těžkooděnců	těžkooděnec	k1gMnPc2	těžkooděnec
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
520	[number]	k4	520
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
přiřazen	přiřazen	k2eAgInSc1d1	přiřazen
běh	běh	k1gInSc1	běh
těžkooděnců	těžkooděnec	k1gMnPc2	těžkooděnec
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
disciplíně	disciplína	k1gFnSc6	disciplína
běhali	běhat	k5eAaImAgMnP	běhat
přes	přes	k7c4	přes
stadion	stadion	k1gInSc4	stadion
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
polní	polní	k2eAgFnSc6d1	polní
zbroji	zbroj	k1gFnSc6	zbroj
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
beze	beze	k7c2	beze
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
zakázány	zakázat	k5eAaPmNgFnP	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
soutěž	soutěž	k1gFnSc1	soutěž
měla	mít	k5eAaImAgFnS	mít
připomínat	připomínat	k5eAaImF	připomínat
důležitost	důležitost	k1gFnSc4	důležitost
vojenského	vojenský	k2eAgInSc2d1	vojenský
výcviku	výcvik	k1gInSc2	výcvik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soutěž	soutěž	k1gFnSc1	soutěž
trubačů	trubač	k1gMnPc2	trubač
a	a	k8xC	a
hlasatelů	hlasatel	k1gMnPc2	hlasatel
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
396	[number]	k4	396
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
soutěž	soutěž	k1gFnSc1	soutěž
trubačů	trubač	k1gMnPc2	trubač
a	a	k8xC	a
hlasatelů	hlasatel	k1gMnPc2	hlasatel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
soutěží	soutěž	k1gFnPc2	soutěž
trubačů	trubač	k1gMnPc2	trubač
se	se	k3xPyFc4	se
hodnotil	hodnotit	k5eAaImAgInS	hodnotit
silný	silný	k2eAgInSc1d1	silný
zvuk	zvuk	k1gInSc1	zvuk
trubice	trubice	k1gFnSc1	trubice
a	a	k8xC	a
vytrvalost	vytrvalost	k1gFnSc1	vytrvalost
trubače	trubač	k1gInSc2	trubač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účastníci	účastník	k1gMnPc1	účastník
her	hra	k1gFnPc2	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
hrami	hra	k1gFnPc7	hra
poslové	posel	k1gMnPc1	posel
z	z	k7c2	z
Olympie	Olympia	k1gFnSc2	Olympia
zvali	zvát	k5eAaImAgMnP	zvát
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
oznamovali	oznamovat	k5eAaImAgMnP	oznamovat
přesné	přesný	k2eAgNnSc4d1	přesné
datum	datum	k1gNnSc4	datum
konání	konání	k1gNnSc2	konání
her	hra	k1gFnPc2	hra
a	a	k8xC	a
vyhlašovali	vyhlašovat	k5eAaImAgMnP	vyhlašovat
posvátný	posvátný	k2eAgInSc4d1	posvátný
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
ekecheiria	ekecheirium	k1gNnSc2	ekecheirium
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
závodníci	závodník	k1gMnPc1	závodník
se	se	k3xPyFc4	se
OH	OH	kA	OH
směli	smět	k5eAaImAgMnP	smět
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
pouze	pouze	k6eAd1	pouze
svobodní	svobodný	k2eAgMnPc1d1	svobodný
občané	občan	k1gMnPc1	občan
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
vyloučeni	vyloučit	k5eAaPmNgMnP	vyloučit
byli	být	k5eAaImAgMnP	být
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
,	,	kIx,	,
otroci	otrok	k1gMnPc1	otrok
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přihlížet	přihlížet	k5eAaImF	přihlížet
hrám	hra	k1gFnPc3	hra
mohl	moct	k5eAaImAgMnS	moct
kdokoli	kdokoli	k3yInSc1	kdokoli
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
řeckých	řecký	k2eAgFnPc2d1	řecká
provdaných	provdaný	k2eAgFnPc2d1	provdaná
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
her	hra	k1gFnPc2	hra
konaly	konat	k5eAaImAgInP	konat
samostatné	samostatný	k2eAgInPc1d1	samostatný
závody	závod	k1gInPc1	závod
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
při	při	k7c6	při
héraích	hérae	k1gFnPc6	hérae
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
dostavit	dostavit	k5eAaPmF	dostavit
do	do	k7c2	do
Olympie	Olympia	k1gFnSc2	Olympia
měsíc	měsíc	k1gInSc4	měsíc
před	před	k7c7	před
počátkem	počátek	k1gInSc7	počátek
her	hra	k1gFnPc2	hra
k	k	k7c3	k
intenzivní	intenzivní	k2eAgFnSc3d1	intenzivní
přípravě	příprava	k1gFnSc3	příprava
spojené	spojený	k2eAgFnSc2d1	spojená
s	s	k7c7	s
dietou	dieta	k1gFnSc7	dieta
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
zahajovány	zahajován	k2eAgFnPc1d1	zahajována
průvodem	průvod	k1gInSc7	průvod
závodníků	závodník	k1gMnPc2	závodník
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
slavnostní	slavnostní	k2eAgFnSc7d1	slavnostní
přísahou	přísaha	k1gFnSc7	přísaha
Diovi	Diův	k2eAgMnPc1d1	Diův
Olympskému	olympský	k2eAgInSc3d1	olympský
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
závody	závod	k1gInPc1	závod
probíhaly	probíhat	k5eAaImAgInP	probíhat
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
hellanodiků	hellanodik	k1gMnPc2	hellanodik
(	(	kIx(	(
<g/>
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
<g/>
;	;	kIx,	;
zprvu	zprvu	k6eAd1	zprvu
2	[number]	k4	2
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
soutěžili	soutěžit	k5eAaImAgMnP	soutěžit
nazí	nahý	k2eAgMnPc1d1	nahý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
podvodu	podvod	k1gInSc3	podvod
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
ukázali	ukázat	k5eAaPmAgMnP	ukázat
krásu	krása	k1gFnSc4	krása
svých	svůj	k3xOyFgNnPc2	svůj
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
disciplínách	disciplína	k1gFnPc6	disciplína
byli	být	k5eAaImAgMnP	být
odměňováni	odměňován	k2eAgMnPc1d1	odměňován
olivovým	olivový	k2eAgInSc7d1	olivový
věncem	věnec	k1gInSc7	věnec
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
právo	právo	k1gNnSc4	právo
dát	dát	k5eAaPmF	dát
si	se	k3xPyFc3	se
postavit	postavit	k5eAaPmF	postavit
své	svůj	k3xOyFgFnPc4	svůj
sochy	socha	k1gFnPc4	socha
<g/>
,	,	kIx,	,
rodné	rodný	k2eAgFnPc4d1	rodná
obce	obec	k1gFnPc4	obec
jim	on	k3xPp3gMnPc3	on
prokazovaly	prokazovat	k5eAaImAgFnP	prokazovat
čestné	čestný	k2eAgFnPc4d1	čestná
pocty	pocta	k1gFnPc4	pocta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
hrám	hra	k1gFnPc3	hra
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
zástupci	zástupce	k1gMnPc7	zástupce
řeckých	řecký	k2eAgInPc2d1	řecký
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
podepisovali	podepisovat	k5eAaImAgMnP	podepisovat
zde	zde	k6eAd1	zde
politické	politický	k2eAgFnPc4d1	politická
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
při	při	k7c6	při
hrách	hra	k1gFnPc6	hra
se	se	k3xPyFc4	se
upevňovalo	upevňovat	k5eAaImAgNnS	upevňovat
vědomí	vědomí	k1gNnSc1	vědomí
řecké	řecký	k2eAgFnSc2d1	řecká
národní	národní	k2eAgFnSc2d1	národní
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc2d1	kulturní
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
sláva	sláva	k1gFnSc1	sláva
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
každé	každý	k3xTgFnSc2	každý
soutěže	soutěž	k1gFnSc2	soutěž
soudce	soudce	k1gMnSc2	soudce
ovinul	ovinout	k5eAaPmAgMnS	ovinout
čelo	čelo	k1gNnSc4	čelo
vítězi	vítěz	k1gMnSc6	vítěz
červenou	červený	k2eAgFnSc7d1	červená
vlněnou	vlněný	k2eAgFnSc7d1	vlněná
páskou	páska	k1gFnSc7	páska
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c4	v
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
her	hra	k1gFnPc2	hra
vítězové	vítěz	k1gMnPc1	vítěz
<g/>
,	,	kIx,	,
shromáždění	shromážděný	k2eAgMnPc1d1	shromážděný
pod	pod	k7c7	pod
Diovým	Diův	k2eAgInSc7d1	Diův
chrámem	chrám	k1gInSc7	chrám
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
olivové	olivový	k2eAgFnPc4d1	olivová
koruny	koruna	k1gFnPc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
získal	získat	k5eAaPmAgMnS	získat
respekt	respekt	k1gInSc4	respekt
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
bylo	být	k5eAaImAgNnS	být
ceněno	cenit	k5eAaImNgNnS	cenit
tak	tak	k6eAd1	tak
vysoko	vysoko	k6eAd1	vysoko
jako	jako	k8xC	jako
vítězství	vítězství	k1gNnSc4	vítězství
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
městě	město	k1gNnSc6	město
byl	být	k5eAaImAgInS	být
velkolepě	velkolepě	k6eAd1	velkolepě
uvítán	uvítat	k5eAaPmNgInS	uvítat
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
zamířil	zamířit	k5eAaPmAgMnS	zamířit
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzdal	vzdát	k5eAaPmAgMnS	vzdát
děkovnou	děkovný	k2eAgFnSc4d1	děkovná
oběť	oběť	k1gFnSc4	oběť
bohům	bůh	k1gMnPc3	bůh
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
jim	on	k3xPp3gMnPc3	on
svůj	svůj	k3xOyFgInSc4	svůj
věnec	věnec	k1gInSc4	věnec
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
rada	rada	k1gFnSc1	rada
města	město	k1gNnSc2	město
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
hostinu	hostina	k1gFnSc4	hostina
v	v	k7c6	v
prytaneiu	prytaneium	k1gNnSc6	prytaneium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zadarmo	zadarmo	k6eAd1	zadarmo
stravování	stravování	k1gNnSc2	stravování
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
byl	být	k5eAaImAgMnS	být
často	často	k6eAd1	často
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
od	od	k7c2	od
některých	některý	k3yIgInPc2	některý
nebo	nebo	k8xC	nebo
i	i	k9	i
všech	všecek	k3xTgFnPc2	všecek
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
keramické	keramický	k2eAgFnPc4d1	keramická
vázy	váza	k1gFnPc4	váza
naplněné	naplněný	k2eAgNnSc1d1	naplněné
jemným	jemný	k2eAgInSc7d1	jemný
olejem	olej	k1gInSc7	olej
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
též	též	k9	též
právo	právo	k1gNnSc4	právo
nechat	nechat	k5eAaPmF	nechat
si	se	k3xPyFc3	se
postavit	postavit	k5eAaPmF	postavit
v	v	k7c6	v
posvátném	posvátný	k2eAgInSc6d1	posvátný
háji	háj	k1gInSc6	háj
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
sochu	socha	k1gFnSc4	socha
–	–	k?	–
bohatí	bohatit	k5eAaImIp3nS	bohatit
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
chudým	chudý	k2eAgInSc7d1	chudý
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA	ZAMAROVSKÝ
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
Olympie	Olympia	k1gFnSc2	Olympia
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
260	[number]	k4	260
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
932	[number]	k4	932
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Antické	antický	k2eAgFnSc2d1	antická
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
