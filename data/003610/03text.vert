<s>
Instant	Instant	k1gInSc1	Instant
messaging	messaging	k1gInSc1	messaging
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
IM	IM	kA	IM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
internetová	internetový	k2eAgFnSc1d1	internetová
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgMnSc1d1	umožňující
svým	svůj	k3xOyFgMnPc3	svůj
uživatelům	uživatel	k1gMnPc3	uživatel
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jejich	jejich	k3xOp3gMnPc1	jejich
přátelé	přítel	k1gMnPc1	přítel
jsou	být	k5eAaImIp3nP	být
právě	právě	k6eAd1	právě
připojeni	připojen	k2eAgMnPc1d1	připojen
<g/>
,	,	kIx,	,
a	a	k8xC	a
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
jim	on	k3xPp3gMnPc3	on
posílat	posílat	k5eAaImF	posílat
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
chatovat	chatovat	k5eAaBmF	chatovat
<g/>
,	,	kIx,	,
přeposílat	přeposílat	k5eAaImF	přeposílat
soubory	soubor	k1gInPc4	soubor
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
a	a	k8xC	a
i	i	k9	i
jinak	jinak	k6eAd1	jinak
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
výhodou	výhoda	k1gFnSc7	výhoda
oproti	oproti	k7c3	oproti
používání	používání	k1gNnSc3	používání
např.	např.	kA	např.
e-mailu	eail	k1gInSc2	e-mail
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
principu	princip	k1gInSc6	princip
odesílání	odesílání	k1gNnSc2	odesílání
a	a	k8xC	a
přijímání	přijímání	k1gNnSc2	přijímání
zpráv	zpráva	k1gFnPc2	zpráva
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
zpráva	zpráva	k1gFnSc1	zpráva
je	být	k5eAaImIp3nS	být
doručena	doručit	k5eAaPmNgFnS	doručit
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
od	od	k7c2	od
odeslání	odeslání	k1gNnSc2	odeslání
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stovek	stovka	k1gFnPc2	stovka
milisekund	milisekunda	k1gFnPc2	milisekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Instant	Instant	k1gInSc1	Instant
messaging	messaging	k1gInSc1	messaging
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
komunikaci	komunikace	k1gFnSc4	komunikace
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadnou	snadný	k2eAgFnSc4d1	snadná
spolupráci	spolupráce	k1gFnSc4	spolupráce
mezi	mezi	k7c7	mezi
více	hodně	k6eAd2	hodně
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
e-mailu	eail	k1gInSc2	e-mail
nebo	nebo	k8xC	nebo
telefonu	telefon	k1gInSc2	telefon
druhá	druhý	k4xOgFnSc1	druhý
strana	strana	k1gFnSc1	strana
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
účastník	účastník	k1gMnSc1	účastník
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
IM	IM	kA	IM
systémů	systém	k1gInPc2	systém
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nastavit	nastavit	k5eAaPmF	nastavit
away	awa	k2eAgInPc4d1	awa
message	messag	k1gInPc4	messag
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
lze	lze	k6eAd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
uživatel	uživatel	k1gMnSc1	uživatel
přítomen	přítomen	k2eAgMnSc1d1	přítomen
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
svého	svůj	k3xOyFgInSc2	svůj
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
uživatele	uživatel	k1gMnSc4	uživatel
nikdo	nikdo	k3yNnSc1	nikdo
nenutí	nutit	k5eNaImIp3nS	nutit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
zprávy	zpráva	k1gFnPc4	zpráva
odpovídali	odpovídat	k5eAaImAgMnP	odpovídat
ihned	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
IM	IM	kA	IM
komunikace	komunikace	k1gFnSc2	komunikace
stává	stávat	k5eAaImIp3nS	stávat
méně	málo	k6eAd2	málo
vyrušující	vyrušující	k2eAgFnSc1d1	vyrušující
než	než	k8xS	než
třeba	třeba	k6eAd1	třeba
telefon	telefon	k1gInSc1	telefon
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
částečný	částečný	k2eAgInSc4d1	částečný
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
komunikace	komunikace	k1gFnSc2	komunikace
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
v	v	k7c6	v
obchodním	obchodní	k2eAgNnSc6d1	obchodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Instant	Instant	k1gInSc1	Instant
messaging	messaging	k1gInSc1	messaging
je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgInSc1d1	ideální
pro	pro	k7c4	pro
rychlou	rychlý	k2eAgFnSc4d1	rychlá
výměnu	výměna	k1gFnSc4	výměna
internetových	internetový	k2eAgFnPc2d1	internetová
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
kusů	kus	k1gInPc2	kus
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
např.	např.	kA	např.
v	v	k7c6	v
telefonní	telefonní	k2eAgFnSc6d1	telefonní
komunikaci	komunikace	k1gFnSc6	komunikace
špatně	špatně	k6eAd1	špatně
přenášejí	přenášet	k5eAaImIp3nP	přenášet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
IM	IM	kA	IM
službou	služba	k1gFnSc7	služba
byla	být	k5eAaImAgFnS	být
síť	síť	k1gFnSc1	síť
IRC	IRC	kA	IRC
(	(	kIx(	(
<g/>
Internet	Internet	k1gInSc1	Internet
Relay	Relaa	k1gFnSc2	Relaa
Chat	chata	k1gFnPc2	chata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Širokou	široký	k2eAgFnSc4d1	široká
adopci	adopce	k1gFnSc4	adopce
v	v	k7c6	v
netechnické	technický	k2eNgFnSc6d1	netechnická
veřejnosti	veřejnost	k1gFnSc6	veřejnost
způsobil	způsobit	k5eAaPmAgInS	způsobit
program	program	k1gInSc1	program
ICQ	ICQ	kA	ICQ
izraelské	izraelský	k2eAgFnSc2d1	izraelská
firmy	firma	k1gFnSc2	firma
Mirabilis	Mirabilis	k1gFnPc2	Mirabilis
představený	představený	k1gMnSc1	představený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
velmi	velmi	k6eAd1	velmi
rychlému	rychlý	k2eAgInSc3d1	rychlý
růstu	růst	k1gInSc3	růst
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
komerční	komerční	k2eAgFnSc4d1	komerční
IM	IM	kA	IM
síť	síť	k1gFnSc4	síť
amerického	americký	k2eAgMnSc4d1	americký
poskytovatele	poskytovatel	k1gMnSc4	poskytovatel
internetu	internet	k1gInSc2	internet
AOL	AOL	kA	AOL
Instant	Instant	k1gInSc1	Instant
Messenger	Messenger	k1gInSc1	Messenger
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zareagoval	zareagovat	k5eAaPmAgInS	zareagovat
zakoupením	zakoupení	k1gNnSc7	zakoupení
firmy	firma	k1gFnSc2	firma
Mirabilis	Mirabilis	k1gFnSc2	Mirabilis
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
mnoho	mnoho	k4c1	mnoho
alternativních	alternativní	k2eAgInPc2d1	alternativní
IM	IM	kA	IM
klientů	klient	k1gMnPc2	klient
(	(	kIx(	(
<g/>
Yahoo	Yahoo	k1gMnSc1	Yahoo
Messenger	Messenger	k1gMnSc1	Messenger
<g/>
,	,	kIx,	,
MSN	MSN	kA	MSN
Messenger	Messenger	k1gInSc1	Messenger
<g/>
,	,	kIx,	,
Excite	Excit	k1gMnSc5	Excit
<g/>
,	,	kIx,	,
Ubique	Ubiquus	k1gMnSc5	Ubiquus
<g/>
,	,	kIx,	,
či	či	k8xC	či
bezpečnější	bezpečný	k2eAgInSc1d2	bezpečnější
program	program	k1gInSc1	program
Lotus	Lotus	kA	Lotus
Sametime	Sametim	k1gInSc5	Sametim
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
IBM	IBM	kA	IBM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
protokolem	protokol	k1gInSc7	protokol
(	(	kIx(	(
<g/>
pochopitelně	pochopitelně	k6eAd1	pochopitelně
navzájem	navzájem	k6eAd1	navzájem
nekompatibilním	kompatibilní	k2eNgMnPc3d1	nekompatibilní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
proto	proto	k8xC	proto
museli	muset	k5eAaImAgMnP	muset
provozovat	provozovat	k5eAaImF	provozovat
několik	několik	k4yIc4	několik
klientů	klient	k1gMnPc2	klient
simultánně	simultánně	k6eAd1	simultánně
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
setkaly	setkat	k5eAaPmAgFnP	setkat
multiprotokolové	multiprotokolový	k2eAgMnPc4d1	multiprotokolový
klienty	klient	k1gMnPc4	klient
jako	jako	k8xC	jako
Digsby	Digsb	k1gMnPc4	Digsb
<g/>
,	,	kIx,	,
Pidgin	Pidgin	k1gInSc1	Pidgin
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Gaim	Gaim	k1gInSc1	Gaim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miranda	Miranda	k1gFnSc1	Miranda
<g/>
,	,	kIx,	,
Trillian	Trillian	k1gInSc1	Trillian
<g/>
,	,	kIx,	,
SIM	SIM	kA	SIM
nebo	nebo	k8xC	nebo
Kopete	kopat	k5eAaImIp2nP	kopat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ICQ	ICQ	kA	ICQ
je	být	k5eAaImIp3nS	být
také	také	k9	také
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
nenáročný	náročný	k2eNgMnSc1d1	nenáročný
ruský	ruský	k2eAgMnSc1d1	ruský
klient	klient	k1gMnSc1	klient
QIP	QIP	kA	QIP
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
pouze	pouze	k6eAd1	pouze
tuto	tento	k3xDgFnSc4	tento
síť	síť	k1gFnSc4	síť
(	(	kIx(	(
<g/>
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
verzi	verze	k1gFnSc6	verze
QIP	QIP	kA	QIP
Infium	Infium	k1gNnSc1	Infium
už	už	k6eAd1	už
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
síť	síť	k1gFnSc1	síť
XMPP	XMPP	kA	XMPP
a	a	k8xC	a
XIMMS	XIMMS	kA	XIMMS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
protokoly	protokol	k1gInPc7	protokol
stírá	stírat	k5eAaImIp3nS	stírat
otevřený	otevřený	k2eAgInSc1d1	otevřený
XMPP	XMPP	kA	XMPP
protokol	protokol	k1gInSc1	protokol
pro	pro	k7c4	pro
IM	IM	kA	IM
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
pomocí	pomocí	k7c2	pomocí
takzvaných	takzvaný	k2eAgInPc2d1	takzvaný
transportů	transport	k1gInPc2	transport
komunikovat	komunikovat	k5eAaImF	komunikovat
i	i	k9	i
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
sítěmi	síť	k1gFnPc7	síť
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
moderní	moderní	k2eAgFnSc1d1	moderní
instant	instant	k?	instant
messaging	messaging	k1gInSc1	messaging
služba	služba	k1gFnSc1	služba
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vlastního	vlastní	k2eAgMnSc4d1	vlastní
klienta	klient	k1gMnSc4	klient
buď	buď	k8xC	buď
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
samostatného	samostatný	k2eAgInSc2d1	samostatný
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
webovém	webový	k2eAgNnSc6d1	webové
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
pak	pak	k6eAd1	pak
fungují	fungovat	k5eAaImIp3nP	fungovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
služby	služba	k1gFnSc2	služba
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mohou	moct	k5eAaImIp3nP	moct
podporovat	podporovat	k5eAaImF	podporovat
i	i	k9	i
funkce	funkce	k1gFnSc1	funkce
dalších	další	k2eAgFnPc2d1	další
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
univerzální	univerzální	k2eAgMnPc1d1	univerzální
klienti	klient	k1gMnPc1	klient
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
podporují	podporovat	k5eAaImIp3nP	podporovat
většinu	většina	k1gFnSc4	většina
současných	současný	k2eAgInPc2d1	současný
IM	IM	kA	IM
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
Adium	Adium	k1gInSc4	Adium
<g/>
,	,	kIx,	,
Empathy	Empath	k1gInPc4	Empath
<g/>
,	,	kIx,	,
Miranda	Miranda	k1gFnSc1	Miranda
IM	IM	kA	IM
<g/>
,	,	kIx,	,
Pidgin	Pidgina	k1gFnPc2	Pidgina
a	a	k8xC	a
Trillian	Trilliana	k1gFnPc2	Trilliana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
představili	představit	k5eAaPmAgMnP	představit
tvůrci	tvůrce	k1gMnPc1	tvůrce
ICQ	ICQ	kA	ICQ
webovou	webový	k2eAgFnSc4d1	webová
verzi	verze	k1gFnSc4	verze
jejich	jejich	k3xOp3gInSc2	jejich
komunikátoru	komunikátor	k1gInSc2	komunikátor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nevyžadovala	vyžadovat	k5eNaImAgFnS	vyžadovat
stažení	stažení	k1gNnSc4	stažení
a	a	k8xC	a
instalací	instalace	k1gFnSc7	instalace
žádného	žádný	k3yNgInSc2	žádný
softwaru	software	k1gInSc2	software
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
napodobila	napodobit	k5eAaPmAgFnS	napodobit
společnost	společnost	k1gFnSc1	společnost
Yahoo	Yahoo	k6eAd1	Yahoo
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
elektronické	elektronický	k2eAgFnSc2d1	elektronická
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
rovněž	rovněž	k9	rovněž
implementovali	implementovat	k5eAaImAgMnP	implementovat
instant	instant	k?	instant
messaging	messaging	k1gInSc1	messaging
do	do	k7c2	do
webového	webový	k2eAgNnSc2d1	webové
poštovního	poštovní	k2eAgNnSc2d1	poštovní
rozhraní	rozhraní	k1gNnSc2	rozhraní
(	(	kIx(	(
<g/>
Google	Google	k1gFnSc1	Google
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k1gMnSc1	Yahoo
<g/>
,	,	kIx,	,
Hotmail	Hotmail	k1gMnSc1	Hotmail
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
omezený	omezený	k2eAgInSc1d1	omezený
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
zasílání	zasílání	k1gNnSc6	zasílání
textových	textový	k2eAgFnPc2d1	textová
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
Google	Google	k1gInSc1	Google
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
i	i	k9	i
videhovory	videhovor	k1gInPc4	videhovor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
i	i	k9	i
možnost	možnost	k1gFnSc1	možnost
volání	volání	k1gNnSc2	volání
na	na	k7c4	na
klasická	klasický	k2eAgNnPc4d1	klasické
telefonní	telefonní	k2eAgNnPc4d1	telefonní
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgInSc1d1	mobilní
instant	instant	k?	instant
messaging	messaging	k1gInSc1	messaging
(	(	kIx(	(
<g/>
MIM	mim	k1gMnSc1	mim
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
instant	instant	k?	instant
messengerům	messenger	k1gInPc3	messenger
z	z	k7c2	z
mobilních	mobilní	k2eAgNnPc2d1	mobilní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
od	od	k7c2	od
standardních	standardní	k2eAgInPc2d1	standardní
telefonů	telefon	k1gInPc2	telefon
až	až	k9	až
po	po	k7c4	po
zařízení	zařízení	k1gNnSc4	zařízení
smartphone	smartphon	k1gInSc5	smartphon
a	a	k8xC	a
tablety	tableta	k1gFnPc4	tableta
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
zařízení	zařízení	k1gNnPc1	zařízení
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
například	například	k6eAd1	například
Android	android	k1gInSc1	android
<g/>
,	,	kIx,	,
iOS	iOS	k?	iOS
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pomocí	pomocí	k7c2	pomocí
samostatných	samostatný	k2eAgMnPc2d1	samostatný
klientů	klient	k1gMnPc2	klient
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
platformu	platforma	k1gFnSc4	platforma
a	a	k8xC	a
nebo	nebo	k8xC	nebo
díky	díky	k7c3	díky
takzvané	takzvaný	k2eAgFnSc3d1	takzvaná
bezklientní	bezklientní	k2eAgFnSc3d1	bezklientní
platformě	platforma	k1gFnSc3	platforma
<g/>
,	,	kIx,	,
webovému	webový	k2eAgNnSc3d1	webové
rozhraní	rozhraní	k1gNnSc3	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc1	možnost
připojení	připojení	k1gNnSc2	připojení
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
zařízení	zařízení	k1gNnSc2	zařízení
z	z	k7c2	z
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
sítě	síť	k1gFnSc2	síť
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
použítý	použítý	k2eAgInSc4d1	použítý
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Standardně	standardně	k6eAd1	standardně
IM	IM	kA	IM
aplikace	aplikace	k1gFnPc1	aplikace
nabízejí	nabízet	k5eAaImIp3nP	nabízet
funkce	funkce	k1gFnPc4	funkce
jako	jako	k8xS	jako
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
uchovávání	uchovávání	k1gNnSc1	uchovávání
seznamu	seznam	k1gInSc2	seznam
uživatelů	uživatel	k1gMnPc2	uživatel
nebo	nebo	k8xC	nebo
provozování	provozování	k1gNnSc2	provozování
několik	několik	k4yIc4	několik
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
konverzací	konverzace	k1gFnPc2	konverzace
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
společnosti	společnost	k1gFnPc4	společnost
(	(	kIx(	(
<g/>
ve	v	k7c6	v
firemním	firemní	k2eAgNnSc6d1	firemní
využití	využití	k1gNnSc6	využití
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
funkce	funkce	k1gFnPc4	funkce
dostačující	dostačující	k2eAgFnPc4d1	dostačující
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
velké	velký	k2eAgFnPc1d1	velká
společnosti	společnost	k1gFnPc1	společnost
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
rovněž	rovněž	k9	rovněž
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
sofistikovanými	sofistikovaný	k2eAgFnPc7d1	sofistikovaná
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Řešení	řešení	k1gNnSc4	řešení
nabízejí	nabízet	k5eAaImIp3nP	nabízet
podnikové	podnikový	k2eAgFnPc4d1	podniková
aplikace	aplikace	k1gFnPc4	aplikace
podporující	podporující	k2eAgNnSc4d1	podporující
instant	instant	k?	instant
messaging	messaging	k1gInSc4	messaging
například	například	k6eAd1	například
protokol	protokol	k1gInSc1	protokol
XMPP	XMPP	kA	XMPP
<g/>
,	,	kIx,	,
Lotus	Lotus	kA	Lotus
Sametima	Sametima	k1gNnSc1	Sametima
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
Communicator	Communicator	k1gInSc4	Communicator
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
implementovány	implementovat	k5eAaImNgInP	implementovat
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
podnikových	podnikový	k2eAgFnPc6d1	podniková
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
jednotného	jednotný	k2eAgInSc2d1	jednotný
standardu	standard	k1gInSc2	standard
pro	pro	k7c4	pro
instant	instant	k?	instant
messaging	messaging	k1gInSc4	messaging
<g/>
:	:	kIx,	:
SIP	SIP	kA	SIP
<g/>
,	,	kIx,	,
SIMPLE	SIMPLE	kA	SIMPLE
<g/>
,	,	kIx,	,
APEX	apex	k1gInSc1	apex
<g/>
,	,	kIx,	,
IMPP	IMPP	kA	IMPP
a	a	k8xC	a
XMPP	XMPP	kA	XMPP
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgInPc2	tento
pokusů	pokus	k1gInPc2	pokus
ale	ale	k8xC	ale
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
největší	veliký	k2eAgMnPc1d3	veliký
poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
služby	služba	k1gFnSc2	služba
instant	instant	k?	instant
messaging	messaging	k1gInSc1	messaging
(	(	kIx(	(
<g/>
AOL	AOL	kA	AOL
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k1gNnSc1	Yahoo
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
<g/>
)	)	kIx)	)
i	i	k9	i
nadále	nadále	k6eAd1	nadále
používali	používat	k5eAaImAgMnP	používat
vlastní	vlastní	k2eAgInPc4d1	vlastní
protokoly	protokol	k1gInPc4	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
září	září	k1gNnSc6	září
2003	[number]	k4	2003
podepsala	podepsat	k5eAaPmAgFnS	podepsat
agentura	agentura	k1gFnSc1	agentura
Reuters	Reuters	k1gInSc4	Reuters
první	první	k4xOgFnSc4	první
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
propojení	propojení	k1gNnSc4	propojení
této	tento	k3xDgFnSc2	tento
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
umožnila	umožnit	k5eAaPmAgFnS	umožnit
uživatelům	uživatel	k1gMnPc3	uživatel
AIM	AIM	kA	AIM
<g/>
,	,	kIx,	,
ICQ	ICQ	kA	ICQ
a	a	k8xC	a
MSN	MSN	kA	MSN
Messengeru	Messenger	k1gInSc2	Messenger
chatovat	chatovat	k5eAaPmF	chatovat
s	s	k7c7	s
uživateli	uživatel	k1gMnPc7	uživatel
Reuters	Reutersa	k1gFnPc2	Reutersa
Messaging	Messaging	k1gInSc1	Messaging
a	a	k8xC	a
obráceně	obráceně	k6eAd1	obráceně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
dohody	dohoda	k1gFnSc2	dohoda
se	se	k3xPyFc4	se
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
AOL	AOL	kA	AOL
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
další	další	k2eAgMnPc1d1	další
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dovolila	dovolit	k5eAaPmAgFnS	dovolit
uživatelům	uživatel	k1gMnPc3	uživatel
Microsoft	Microsoft	kA	Microsoft
Live	Liv	k1gInSc2	Liv
Communications	Communications	k1gInSc4	Communications
Server	server	k1gInSc1	server
2005	[number]	k4	2005
chatovat	chatovat	k5eAaImF	chatovat
s	s	k7c7	s
uživateli	uživatel	k1gMnPc7	uživatel
veřejných	veřejný	k2eAgMnPc2d1	veřejný
<g/>
,	,	kIx,	,
komerčních	komerční	k2eAgMnPc2d1	komerční
klientů	klient	k1gMnPc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
rovněž	rovněž	k9	rovněž
stanovila	stanovit	k5eAaPmAgFnS	stanovit
SIP	SIP	kA	SIP
<g/>
/	/	kIx~	/
<g/>
SIMPLE	SIMPLE	kA	SIMPLE
protokol	protokol	k1gInSc1	protokol
jako	jako	k8xC	jako
standard	standard	k1gInSc1	standard
pro	pro	k7c4	pro
interoperabilitu	interoperabilita	k1gFnSc4	interoperabilita
a	a	k8xC	a
stanovila	stanovit	k5eAaPmAgFnS	stanovit
poplatek	poplatek	k1gInSc4	poplatek
za	za	k7c4	za
propojení	propojení	k1gNnSc4	propojení
do	do	k7c2	do
veřejních	veřejní	k2eAgFnPc2d1	veřejní
IM	IM	kA	IM
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
samostatně	samostatně	k6eAd1	samostatně
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
s	s	k7c7	s
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
!	!	kIx.	!
</s>
<s>
o	o	k7c4	o
propojení	propojení	k1gNnSc4	propojení
svých	svůj	k3xOyFgInPc2	svůj
IM	IM	kA	IM
služeb	služba	k1gFnPc2	služba
na	na	k7c6	na
základě	základ	k1gInSc6	základ
protokolu	protokol	k1gInSc2	protokol
SIP	SIP	kA	SIP
<g/>
/	/	kIx~	/
<g/>
SIMPLE	SIMPLE	kA	SIMPLE
což	což	k9	což
následovala	následovat	k5eAaImAgFnS	následovat
dohoda	dohoda	k1gFnSc1	dohoda
Google	Google	k1gFnSc1	Google
s	s	k7c7	s
AOL	AOL	kA	AOL
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
komunikace	komunikace	k1gFnSc2	komunikace
uživatelů	uživatel	k1gMnPc2	uživatel
AIM	AIM	kA	AIM
a	a	k8xC	a
ICQ	ICQ	kA	ICQ
s	s	k7c7	s
uživateli	uživatel	k1gMnPc7	uživatel
Google	Google	k1gFnSc2	Google
Talk	Talk	k1gInSc1	Talk
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
způsoby	způsob	k1gInPc4	způsob
jak	jak	k6eAd1	jak
propojit	propojit	k5eAaPmF	propojit
několik	několik	k4yIc4	několik
odlišných	odlišný	k2eAgInPc2d1	odlišný
protokolů	protokol	k1gInPc2	protokol
<g/>
:	:	kIx,	:
Zahrnout	zahrnout	k5eAaPmF	zahrnout
protokoly	protokol	k1gInPc4	protokol
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
klientů	klient	k1gMnPc2	klient
Zahrnout	zahrnout	k5eAaPmF	zahrnout
protokol	protokol	k1gInSc4	protokol
do	do	k7c2	do
serverových	serverový	k2eAgFnPc2d1	serverová
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
povinnost	povinnost	k1gFnSc4	povinnost
komunikace	komunikace	k1gFnSc2	komunikace
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
protokoly	protokol	k1gInPc7	protokol
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
klienta	klient	k1gMnSc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
přístupy	přístup	k1gInPc1	přístup
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
organizacím	organizace	k1gFnPc3	organizace
vytvořit	vytvořit	k5eAaPmF	vytvořit
vlastní	vlastní	k2eAgInSc4d1	vlastní
privátní	privátní	k2eAgInSc4d1	privátní
instant	instant	k?	instant
messaging	messaging	k1gInSc1	messaging
síť	síť	k1gFnSc1	síť
díky	díky	k7c3	díky
možnosti	možnost	k1gFnSc3	možnost
nastavení	nastavení	k1gNnSc2	nastavení
omezeného	omezený	k2eAgNnSc2d1	omezené
připojení	připojení	k1gNnSc2	připojení
k	k	k7c3	k
serverům	server	k1gInPc3	server
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
sítě	síť	k1gFnPc4	síť
často	často	k6eAd1	často
za	za	k7c7	za
firemním	firemní	k2eAgInSc7d1	firemní
firewallem	firewall	k1gInSc7	firewall
<g/>
)	)	kIx)	)
a	a	k8xC	a
spravováním	spravování	k1gNnSc7	spravování
uživatelských	uživatelský	k2eAgNnPc2d1	Uživatelské
oprávnění	oprávnění	k1gNnPc2	oprávnění
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
firemní	firemní	k2eAgInPc1d1	firemní
systémy	systém	k1gInPc1	systém
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
uživatelům	uživatel	k1gMnPc3	uživatel
připojení	připojení	k1gNnSc4	připojení
i	i	k9	i
mimo	mimo	k7c4	mimo
firemní	firemní	k2eAgFnSc4d1	firemní
síť	síť	k1gFnSc4	síť
použitím	použití	k1gNnSc7	použití
šifrovaného	šifrovaný	k2eAgNnSc2d1	šifrované
spojení	spojení	k1gNnSc2	spojení
na	na	k7c6	na
základě	základ	k1gInSc6	základ
HTTPS	HTTPS	kA	HTTPS
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
mají	mít	k5eAaImIp3nP	mít
tyto	tyt	k2eAgNnSc4d1	tyto
firemní	firemní	k2eAgNnSc4d1	firemní
řešení	řešení	k1gNnSc4	řešení
několik	několik	k4yIc4	několik
výhod	výhoda	k1gFnPc2	výhoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
automaticky	automaticky	k6eAd1	automaticky
vygenerovaný	vygenerovaný	k2eAgInSc4d1	vygenerovaný
seznam	seznam	k1gInSc4	seznam
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnPc4d1	vlastní
autorizace	autorizace	k1gFnPc4	autorizace
a	a	k8xC	a
lepší	dobrý	k2eAgNnSc4d2	lepší
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
a	a	k8xC	a
soukromí	soukromí	k1gNnSc4	soukromí
<g/>
.	.	kIx.	.
</s>
<s>
Poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
IM	IM	kA	IM
služeb	služba	k1gFnPc2	služba
se	se	k3xPyFc4	se
ale	ale	k9	ale
díky	díky	k7c3	díky
častým	častý	k2eAgFnPc3d1	častá
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
protokolech	protokol	k1gInPc6	protokol
snažili	snažit	k5eAaImAgMnP	snažit
zabránit	zabránit	k5eAaPmF	zabránit
připojení	připojení	k1gNnPc4	připojení
multiprotokolových	multiprotokolový	k2eAgMnPc2d1	multiprotokolový
klientů	klient	k1gMnPc2	klient
do	do	k7c2	do
jejich	jejich	k3xOp3gFnPc2	jejich
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
tak	tak	k6eAd1	tak
například	například	k6eAd1	například
tvůrci	tvůrce	k1gMnPc1	tvůrce
Trillianu	Trillian	k1gInSc2	Trillian
museli	muset	k5eAaImAgMnP	muset
často	často	k6eAd1	často
vydávat	vydávat	k5eAaPmF	vydávat
patche	patche	k6eAd1	patche
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
uživatelé	uživatel	k1gMnPc1	uživatel
připojit	připojit	k5eAaPmF	připojit
do	do	k7c2	do
sítí	síť	k1gFnPc2	síť
MSN	MSN	kA	MSN
<g/>
,	,	kIx,	,
AOL	AOL	kA	AOL
a	a	k8xC	a
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
.	.	kIx.	.
</s>
<s>
Poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
tyto	tento	k3xDgFnPc4	tento
změny	změna	k1gFnPc4	změna
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
formálními	formální	k2eAgFnPc7d1	formální
dohodami	dohoda	k1gFnPc7	dohoda
a	a	k8xC	a
obavami	obava	k1gFnPc7	obava
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
vlastních	vlastní	k2eAgInPc2d1	vlastní
protokolů	protokol	k1gInPc2	protokol
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
nekompatibilitě	nekompatibilita	k1gFnSc3	nekompatibilita
a	a	k8xC	a
uživatelé	uživatel	k1gMnPc1	uživatel
neměli	mít	k5eNaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
uživateli	uživatel	k1gMnPc7	uživatel
jiných	jiný	k2eAgFnPc2d1	jiná
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mít	mít	k5eAaImF	mít
několik	několik	k4yIc4	několik
účtů	účet	k1gInPc2	účet
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
taky	taky	k9	taky
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
vzestupu	vzestup	k1gInSc2	vzestup
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
instant	instant	k?	instant
messaging	messaging	k1gInSc1	messaging
jako	jako	k9	jako
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
webového	webový	k2eAgNnSc2d1	webové
rozhraní	rozhraní	k1gNnSc2	rozhraní
tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
odpadá	odpadat	k5eAaImIp3nS	odpadat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Facebook	Facebook	k1gInSc1	Facebook
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
zkratky	zkratka	k1gFnPc1	zkratka
běžných	běžný	k2eAgNnPc2d1	běžné
slov	slovo	k1gNnPc2	slovo
nebo	nebo	k8xC	nebo
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zmenšili	zmenšit	k5eAaPmAgMnP	zmenšit
nutný	nutný	k2eAgInSc4d1	nutný
počet	počet	k1gInSc4	počet
úhozů	úhoz	k1gInPc2	úhoz
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
zprávy	zpráva	k1gFnSc2	zpráva
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
komunikaci	komunikace	k1gFnSc4	komunikace
urychlili	urychlit	k5eAaPmAgMnP	urychlit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
výraz	výraz	k1gInSc1	výraz
'	'	kIx"	'
<g/>
lol	lol	k?	lol
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
v	v	k7c6	v
osobní	osobní	k2eAgFnSc6d1	osobní
<g/>
,	,	kIx,	,
přímé	přímý	k2eAgFnSc3d1	přímá
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vyjádřeny	vyjádřen	k2eAgInPc1d1	vyjádřen
právě	právě	k9	právě
podobnými	podobný	k2eAgFnPc7d1	podobná
zkratkami	zkratka	k1gFnPc7	zkratka
jako	jako	k8xS	jako
LOL	LOL	kA	LOL
(	(	kIx(	(
<g/>
laughing	laughing	k1gInSc1	laughing
out	out	k?	out
loud	louda	k1gFnPc2	louda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BRB	BRB	kA	BRB
(	(	kIx(	(
<g/>
be	be	k?	be
right	right	k1gMnSc1	right
back	back	k1gMnSc1	back
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
TTYL	TTYL	kA	TTYL
(	(	kIx(	(
<g/>
talk	talk	k6eAd1	talk
to	ten	k3xDgNnSc1	ten
you	you	k?	you
later	later	k1gInSc1	later
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
o	o	k7c4	o
přesnější	přesný	k2eAgNnSc4d2	přesnější
vyjádření	vyjádření	k1gNnSc4	vyjádření
emocí	emoce	k1gFnPc2	emoce
v	v	k7c6	v
instant	instant	k?	instant
messagingu	messaging	k1gInSc2	messaging
a	a	k8xC	a
přímé	přímý	k2eAgInPc4d1	přímý
výrazy	výraz	k1gInPc4	výraz
pro	pro	k7c4	pro
emoce	emoce	k1gFnPc4	emoce
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
jako	jako	k8xC	jako
například	například	k6eAd1	například
(	(	kIx(	(
<g/>
eye-roll	eyeoll	k1gInSc1	eye-roll
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
snort	snort	k1gInSc1	snort
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
populárnější	populární	k2eAgFnPc1d2	populárnější
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
jakési	jakýsi	k3yIgNnSc4	jakýsi
standardizování	standardizování	k1gNnSc4	standardizování
konverzace	konverzace	k1gFnSc2	konverzace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
například	například	k6eAd1	například
znak	znak	k1gInSc1	znak
#	#	kIx~	#
značí	značit	k5eAaImIp3nS	značit
sarkazmus	sarkazmus	k1gInSc4	sarkazmus
a	a	k8xC	a
*	*	kIx~	*
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
opravený	opravený	k2eAgInSc4d1	opravený
překlep	překlep	k1gInSc4	překlep
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
instant	instant	k?	instant
messaging	messaging	k1gInSc1	messaging
přináší	přinášet	k5eAaImIp3nS	přinášet
mnoho	mnoho	k4c4	mnoho
výhod	výhoda	k1gFnPc2	výhoda
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
přináší	přinášet	k5eAaImIp3nS	přinášet
i	i	k9	i
některá	některý	k3yIgNnPc4	některý
rizika	riziko	k1gNnPc4	riziko
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
při	při	k7c6	při
používání	používání	k1gNnSc6	používání
na	na	k7c6	na
pracovištích	pracoviště	k1gNnPc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgMnPc4	tento
rizika	riziko	k1gNnPc1	riziko
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Bezpečnostní	bezpečnostní	k2eAgNnPc1d1	bezpečnostní
rizika	riziko	k1gNnPc1	riziko
Legislativní	legislativní	k2eAgNnPc1d1	legislativní
rizika	riziko	k1gNnPc1	riziko
Nevhodné	vhodný	k2eNgNnSc4d1	nevhodné
užití	užití	k1gNnSc4	užití
Vyzrazení	vyzrazení	k1gNnSc2	vyzrazení
obchodních	obchodní	k2eAgFnPc2d1	obchodní
tajemství	tajemství	k1gNnPc2	tajemství
Crackeři	Cracker	k1gMnPc1	Cracker
odjakživa	odjakživa	k6eAd1	odjakživa
využívali	využívat	k5eAaImAgMnP	využívat
IM	IM	kA	IM
sítě	síť	k1gFnSc2	síť
pro	pro	k7c4	pro
phishing	phishing	k1gInSc4	phishing
útoky	útok	k1gInPc7	útok
<g/>
,	,	kIx,	,
zasílání	zasílání	k1gNnSc3	zasílání
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
URL	URL	kA	URL
odkazů	odkaz	k1gInPc2	odkaz
a	a	k8xC	a
souborů	soubor	k1gInPc2	soubor
obsahující	obsahující	k2eAgInPc1d1	obsahující
viry	vir	k1gInPc1	vir
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
způsobem	způsob	k1gInSc7	způsob
šíření	šíření	k1gNnSc2	šíření
virů	vir	k1gInPc2	vir
bylo	být	k5eAaImAgNnS	být
zaslání	zaslání	k1gNnSc1	zaslání
důvěryhodného	důvěryhodný	k2eAgInSc2d1	důvěryhodný
textu	text	k1gInSc2	text
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
webovou	webový	k2eAgFnSc4d1	webová
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
po	po	k7c6	po
přístupu	přístup	k1gInSc6	přístup
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
okamžitě	okamžitě	k6eAd1	okamžitě
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
virus	virus	k1gInSc1	virus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
šířil	šířit	k5eAaImAgMnS	šířit
rozesíláním	rozesílání	k1gNnSc7	rozesílání
stejné	stejný	k2eAgFnSc2d1	stejná
zprávy	zpráva	k1gFnSc2	zpráva
z	z	k7c2	z
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
škodlivý	škodlivý	k2eAgInSc4d1	škodlivý
soubor	soubor	k1gInSc4	soubor
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
uloženým	uložený	k2eAgInPc3d1	uložený
v	v	k7c6	v
uživatelském	uživatelský	k2eAgInSc6d1	uživatelský
seznamu	seznam	k1gInSc6	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
útok	útok	k1gInSc1	útok
dokázal	dokázat	k5eAaPmAgInS	dokázat
napadnout	napadnout	k5eAaPmF	napadnout
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
uživatelů	uživatel	k1gMnPc2	uživatel
během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zasílal	zasílat	k5eAaImAgMnS	zasílat
zprávy	zpráva	k1gFnPc4	zpráva
jménem	jméno	k1gNnSc7	jméno
důvěryhodných	důvěryhodný	k2eAgMnPc2d1	důvěryhodný
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
IM	IM	kA	IM
konverzace	konverzace	k1gFnSc1	konverzace
rovněž	rovněž	k9	rovněž
probíhají	probíhat	k5eAaImIp3nP	probíhat
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	on	k3xPp3gMnPc4	on
dělá	dělat	k5eAaImIp3nS	dělat
zranitelné	zranitelný	k2eAgInPc1d1	zranitelný
vůči	vůči	k7c3	vůči
odposlouchávání	odposlouchávání	k1gNnSc3	odposlouchávání
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
IM	IM	kA	IM
klienti	klient	k1gMnPc1	klient
rovněž	rovněž	k9	rovněž
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
otevření	otevření	k1gNnPc4	otevření
UDP	UDP	kA	UDP
portů	port	k1gInPc2	port
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
rovněž	rovněž	k9	rovněž
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
určité	určitý	k2eAgNnSc4d1	určité
bezpečnostní	bezpečnostní	k2eAgNnSc4d1	bezpečnostní
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Použitím	použití	k1gNnSc7	použití
instant	instant	k?	instant
messagingu	messaging	k1gInSc3	messaging
na	na	k7c6	na
pracovištích	pracoviště	k1gNnPc6	pracoviště
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k8xC	i
legislativní	legislativní	k2eAgNnPc4d1	legislativní
rizika	riziko	k1gNnPc4	riziko
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
regulace	regulace	k1gFnSc2	regulace
o	o	k7c6	o
elektronické	elektronický	k2eAgFnSc6d1	elektronická
komunikaci	komunikace	k1gFnSc6	komunikace
na	na	k7c6	na
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
USA	USA	kA	USA
existuje	existovat	k5eAaImIp3nS	existovat
přes	přes	k7c4	přes
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInPc2	tisíc
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
regulaci	regulace	k1gFnSc3	regulace
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
elektronickou	elektronický	k2eAgFnSc7d1	elektronická
komunikací	komunikace	k1gFnSc7	komunikace
a	a	k8xC	a
uchováváním	uchovávání	k1gNnSc7	uchovávání
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
ostatních	ostatní	k2eAgInPc2d1	ostatní
států	stát	k1gInPc2	stát
má	mít	k5eAaImIp3nS	mít
firemní	firemní	k2eAgFnSc4d1	firemní
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
komunikaci	komunikace	k1gFnSc4	komunikace
právně	právně	k6eAd1	právně
upravenou	upravený	k2eAgFnSc4d1	upravená
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgNnSc1d3	nejčastější
omezení	omezení	k1gNnSc1	omezení
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
IM	IM	kA	IM
službou	služba	k1gFnSc7	služba
na	na	k7c6	na
pracovišti	pracoviště	k1gNnSc6	pracoviště
je	být	k5eAaImIp3nS	být
povinnost	povinnost	k1gFnSc1	povinnost
uchovávat	uchovávat	k5eAaImF	uchovávat
obchodní	obchodní	k2eAgFnSc4d1	obchodní
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
společnosti	společnost	k1gFnPc1	společnost
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
chránit	chránit	k5eAaImF	chránit
oproti	oproti	k7c3	oproti
nevhodnému	vhodný	k2eNgNnSc3d1	nevhodné
užití	užití	k1gNnSc3	užití
IM	IM	kA	IM
jejich	jejich	k3xOp3gMnSc7	jejich
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Neformálnost	neformálnost	k1gFnSc1	neformálnost
<g/>
,	,	kIx,	,
bezprostřednost	bezprostřednost	k1gFnSc4	bezprostřednost
a	a	k8xC	a
pocit	pocit	k1gInSc4	pocit
anonymity	anonymita	k1gFnSc2	anonymita
často	často	k6eAd1	často
vybízejí	vybízet	k5eAaImIp3nP	vybízet
ke	k	k7c3	k
zneužití	zneužití	k1gNnSc3	zneužití
IM	IM	kA	IM
na	na	k7c6	na
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
nevhodného	vhodný	k2eNgNnSc2d1	nevhodné
užití	užití	k1gNnSc2	užití
IM	IM	kA	IM
zaplnilo	zaplnit	k5eAaPmAgNnS	zaplnit
titulní	titulní	k2eAgFnPc4d1	titulní
strany	strana	k1gFnPc4	strana
médií	médium	k1gNnPc2	médium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
americký	americký	k2eAgMnSc1d1	americký
kongresman	kongresman	k1gMnSc1	kongresman
Mark	Mark	k1gMnSc1	Mark
Foley	Folea	k1gFnSc2	Folea
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
post	post	k1gInSc4	post
když	když	k8xS	když
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zasílal	zasílat	k5eAaImAgMnS	zasílat
urážlivé	urážlivý	k2eAgFnPc4d1	urážlivá
sexuálně	sexuálně	k6eAd1	sexuálně
tematické	tematický	k2eAgFnPc4d1	tematická
zprávy	zpráva	k1gFnPc4	zpráva
nezletilé	nezletilé	k2eAgFnSc3d1	nezletilé
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
House	house	k1gNnSc1	house
of	of	k?	of
Representatives	Representatives	k1gMnSc1	Representatives
Page	Pag	k1gFnSc2	Pag
<g/>
,	,	kIx,	,
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
pracovního	pracovní	k2eAgInSc2d1	pracovní
počítače	počítač	k1gInSc2	počítač
v	v	k7c6	v
kongresu	kongres	k1gInSc6	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
media	medium	k1gNnSc2	medium
vydaly	vydat	k5eAaPmAgFnP	vydat
několik	několik	k4yIc4	několik
článků	článek	k1gInPc2	článek
ohledně	ohledně	k7c2	ohledně
nevhodného	vhodný	k2eNgNnSc2d1	nevhodné
užití	užití	k1gNnSc2	užití
IM	IM	kA	IM
na	na	k7c6	na
pracovištích	pracoviště	k1gNnPc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemích	zem	k1gFnPc6	zem
mají	mít	k5eAaImIp3nP	mít
společnosti	společnost	k1gFnPc1	společnost
právní	právní	k2eAgFnSc1d1	právní
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
zajistit	zajistit	k5eAaPmF	zajistit
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
prostředí	prostředí	k1gNnSc2	prostředí
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
obtěžování	obtěžování	k1gNnSc2	obtěžování
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
používání	používání	k1gNnSc4	používání
firemních	firemní	k2eAgInPc2d1	firemní
počítačů	počítač	k1gInPc2	počítač
<g/>
,	,	kIx,	,
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
softwaru	software	k1gInSc2	software
k	k	k7c3	k
obtěžování	obtěžování	k1gNnSc3	obtěžování
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
nevhodných	vhodný	k2eNgFnPc2d1	nevhodná
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
vtipů	vtip	k1gInPc2	vtip
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tedy	tedy	k9	tedy
nejen	nejen	k6eAd1	nejen
samotná	samotný	k2eAgFnSc1d1	samotná
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
provedla	provést	k5eAaPmAgFnS	provést
společnost	společnost	k1gFnSc1	společnost
Akonix	Akonix	k1gInSc1	Akonix
Systems	Systems	k1gInSc1	Systems
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
přiznalo	přiznat	k5eAaPmAgNnS	přiznat
31	[number]	k4	31
<g/>
%	%	kIx~	%
dotázaných	dotázaný	k2eAgFnPc2d1	dotázaná
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
pracovišti	pracoviště	k1gNnSc6	pracoviště
obtěžování	obtěžování	k1gNnSc2	obtěžování
přes	přes	k7c4	přes
IM	IM	kA	IM
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnPc1	společnost
nyní	nyní	k6eAd1	nyní
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
instant	instant	k?	instant
messaging	messaging	k1gInSc1	messaging
jako	jako	k9	jako
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
částí	část	k1gFnPc2	část
u	u	k7c2	u
zásad	zásada	k1gFnPc2	zásada
správného	správný	k2eAgNnSc2d1	správné
užití	užití	k1gNnSc2	užití
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
internetem	internet	k1gInSc7	internet
a	a	k8xC	a
emailem	email	k1gInSc7	email
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
nového	nový	k2eAgNnSc2d1	nové
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
obor	obor	k1gInSc1	obor
IT	IT	kA	IT
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
a	a	k8xC	a
sice	sice	k8xC	sice
poskytování	poskytování	k1gNnSc1	poskytování
ochrany	ochrana	k1gFnSc2	ochrana
pro	pro	k7c4	pro
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
používat	používat	k5eAaImF	používat
IM	IM	kA	IM
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgFnPc4d1	obchodní
komunikace	komunikace	k1gFnPc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
nový	nový	k2eAgInSc4d1	nový
software	software	k1gInSc4	software
instalovaný	instalovaný	k2eAgInSc4d1	instalovaný
ve	v	k7c6	v
firemních	firemní	k2eAgFnPc6d1	firemní
sítích	síť	k1gFnPc6	síť
sloužící	sloužící	k1gFnSc2	sloužící
pro	pro	k7c4	pro
archivaci	archivace	k1gFnSc4	archivace
<g/>
,	,	kIx,	,
prohledávání	prohledávání	k1gNnSc4	prohledávání
obsahu	obsah	k1gInSc2	obsah
a	a	k8xC	a
prohledávání	prohledávání	k1gNnSc2	prohledávání
kvůli	kvůli	k7c3	kvůli
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
veškeré	veškerý	k3xTgFnSc2	veškerý
příchozí	příchozí	k1gFnSc2	příchozí
i	i	k8xC	i
odchozí	odchozí	k2eAgFnSc2d1	odchozí
IM	IM	kA	IM
komunikace	komunikace	k1gFnSc2	komunikace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgInSc4d1	fungující
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
filtrování	filtrování	k1gNnSc4	filtrování
elektronické	elektronický	k2eAgFnSc2d1	elektronická
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
výše	vysoce	k6eAd2	vysoce
popsaných	popsaný	k2eAgInPc2d1	popsaný
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
službě	služba	k1gFnSc6	služba
rapidně	rapidně	k6eAd1	rapidně
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
IM	IM	kA	IM
jako	jako	k8xS	jako
prostředkem	prostředek	k1gInSc7	prostředek
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgFnSc4d1	obchodní
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
