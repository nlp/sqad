<s>
Skalní	skalní	k2eAgFnSc1d1	skalní
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
hlediska	hledisko	k1gNnSc2	hledisko
strukturně	strukturně	k6eAd1	strukturně
denudační	denudační	k2eAgInSc1d1	denudační
tvar	tvar	k1gInSc1	tvar
reliéfu	reliéf	k1gInSc2	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
strmou	strmý	k2eAgFnSc4d1	strmá
skalní	skalní	k2eAgFnSc4d1	skalní
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
sklon	sklon	k1gInSc4	sklon
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
55	[number]	k4	55
<g/>
°	°	k?	°
a	a	k8xC	a
relativní	relativní	k2eAgFnSc1d1	relativní
výška	výška	k1gFnSc1	výška
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
exogenními	exogenní	k2eAgInPc7d1	exogenní
geomorfologickými	geomorfologický	k2eAgInPc7d1	geomorfologický
pochody	pochod	k1gInPc7	pochod
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
i	i	k9	i
strukturně	strukturně	k6eAd1	strukturně
tektonicky	tektonicky	k6eAd1	tektonicky
<g/>
.	.	kIx.	.
</s>
<s>
Skalní	skalní	k2eAgFnPc4d1	skalní
stěny	stěna	k1gFnPc4	stěna
často	často	k6eAd1	často
postihuje	postihovat	k5eAaImIp3nS	postihovat
skalní	skalní	k2eAgNnSc4d1	skalní
řícení	řícení	k1gNnSc4	řícení
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
díky	díky	k7c3	díky
odkryvu	odkryv	k1gInSc3	odkryv
geologického	geologický	k2eAgInSc2d1	geologický
profilu	profil	k1gInSc2	profil
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
využívány	využívat	k5eAaPmNgInP	využívat
k	k	k7c3	k
horolezectví	horolezectví	k1gNnSc3	horolezectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
skalní	skalní	k2eAgFnPc1d1	skalní
stěny	stěna	k1gFnPc1	stěna
nachází	nacházet	k5eAaImIp3nP	nacházet
například	například	k6eAd1	například
v	v	k7c6	v
Prachovských	prachovský	k2eAgFnPc6d1	Prachovská
skalách	skála	k1gFnPc6	skála
<g/>
,	,	kIx,	,
Moravském	moravský	k2eAgInSc6d1	moravský
nebo	nebo	k8xC	nebo
Českém	český	k2eAgInSc6d1	český
krasu	kras	k1gInSc6	kras
<g/>
,	,	kIx,	,
Českém	český	k2eAgInSc6d1	český
ráji	ráj	k1gInSc6	ráj
či	či	k8xC	či
v	v	k7c6	v
Adršpašsko-teplických	adršpašskoeplický	k2eAgFnPc6d1	adršpašsko-teplický
skalách	skála	k1gFnPc6	skála
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pak	pak	k6eAd1	pak
třeba	třeba	k6eAd1	třeba
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Tatrách	Tatra	k1gFnPc6	Tatra
(	(	kIx(	(
<g/>
Malý	malý	k2eAgInSc1d1	malý
Kežmarský	kežmarský	k2eAgInSc1d1	kežmarský
štít	štít	k1gInSc1	štít
<g/>
)	)	kIx)	)
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
nebo	nebo	k8xC	nebo
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
Capitan	Capitan	k1gInSc1	Capitan
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
stěna	stěna	k1gFnSc1	stěna
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
