<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
strmá	strmý	k2eAgFnSc1d1	strmá
skalní	skalní	k2eAgFnSc1d1	skalní
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
sklon	sklon	k1gInSc4	sklon
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
55	[number]	k4	55
<g/>
°	°	k?	°
a	a	k8xC	a
relativní	relativní	k2eAgFnSc1d1	relativní
výška	výška	k1gFnSc1	výška
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
?	?	kIx.	?
</s>
