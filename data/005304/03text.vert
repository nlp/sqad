<s>
Cardiff	Cardiff	k1gInSc1	Cardiff
(	(	kIx(	(
<g/>
velšsky	velšsky	k6eAd1	velšsky
Caerdydd	Caerdydd	k1gInSc1	Caerdydd
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
hlavní	hlavní	k2eAgFnSc4d1	hlavní
město	město	k1gNnSc4	město
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
samotné	samotný	k2eAgNnSc1d1	samotné
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
320	[number]	k4	320
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
přilehlými	přilehlý	k2eAgInPc7d1	přilehlý
místy	místo	k1gNnPc7	místo
cca	cca	kA	cca
860	[number]	k4	860
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
10	[number]	k4	10
<g/>
.	.	kIx.	.
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
jím	on	k3xPp3gInSc7	on
řeka	řeka	k1gFnSc1	řeka
Taff	Taff	k1gInSc1	Taff
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Cardiff	Cardiff	k1gInSc1	Cardiff
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
centra	centrum	k1gNnPc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
představuje	představovat	k5eAaImIp3nS	představovat
střed	střed	k1gInSc1	střed
města	město	k1gNnSc2	město
s	s	k7c7	s
viktoriánskými	viktoriánský	k2eAgFnPc7d1	viktoriánská
a	a	k8xC	a
edvardovskými	edvardovský	k2eAgFnPc7d1	edvardovský
ulicemi	ulice	k1gFnPc7	ulice
a	a	k8xC	a
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
neogotický	ogotický	k2eNgInSc4d1	neogotický
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
neoklasicistní	neoklasicistní	k2eAgFnPc4d1	neoklasicistní
městské	městský	k2eAgFnPc4d1	městská
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nákupní	nákupní	k2eAgNnPc4d1	nákupní
střediska	středisko	k1gNnPc4	středisko
a	a	k8xC	a
krytou	krytý	k2eAgFnSc4d1	krytá
tržnici	tržnice	k1gFnSc4	tržnice
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
tříd	třída	k1gFnPc2	třída
vybíhají	vybíhat	k5eAaImIp3nP	vybíhat
pasáže	pasáž	k1gFnPc1	pasáž
lemované	lemovaný	k2eAgFnPc1d1	lemovaná
obchody	obchod	k1gInPc4	obchod
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejstarší	starý	k2eAgInSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
Royal	Royal	k1gInSc1	Royal
Arcade	Arcad	k1gInSc5	Arcad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
přístavní	přístavní	k2eAgFnSc1d1	přístavní
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
postupnou	postupný	k2eAgFnSc7d1	postupná
přestavbou	přestavba	k1gFnSc7	přestavba
přístavu	přístav	k1gInSc2	přístav
a	a	k8xC	a
nábřeží	nábřeží	k1gNnSc2	nábřeží
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
druhé	druhý	k4xOgNnSc4	druhý
středisko	středisko	k1gNnSc4	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nachází	nacházet	k5eAaImIp3nS	nacházet
Millennium	millennium	k1gNnSc4	millennium
Stadium	stadium	k1gNnSc1	stadium
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
přednostně	přednostně	k6eAd1	přednostně
pro	pro	k7c4	pro
ragby	ragby	k1gNnSc4	ragby
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
koncerty	koncert	k1gInPc4	koncert
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
např.	např.	kA	např.
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
Madonna	Madonna	k1gFnSc1	Madonna
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
věnuje	věnovat	k5eAaPmIp3nS	věnovat
převážně	převážně	k6eAd1	převážně
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
správě	správa	k1gFnSc6	správa
země	zem	k1gFnSc2	zem
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vzhled	vzhled	k1gInSc1	vzhled
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
realizují	realizovat	k5eAaBmIp3nP	realizovat
plány	plán	k1gInPc1	plán
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
obnovu	obnova	k1gFnSc4	obnova
<g/>
.	.	kIx.	.
</s>
<s>
Cardiffský	Cardiffský	k2eAgInSc1d1	Cardiffský
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
velšsky	velšsky	k6eAd1	velšsky
Castell	Castell	k1gMnSc1	Castell
Caerdydd	Caerdydd	k1gMnSc1	Caerdydd
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
římské	římský	k2eAgFnSc2d1	římská
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
novějších	nový	k2eAgFnPc2d2	novější
částí	část	k1gFnPc2	část
odděleny	oddělit	k5eAaPmNgInP	oddělit
pásem	pásmo	k1gNnPc2	pásmo
z	z	k7c2	z
červeného	červený	k2eAgInSc2d1	červený
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
římskými	římský	k2eAgFnPc7d1	římská
rozvalinami	rozvalina	k1gFnPc7	rozvalina
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
následujících	následující	k2eAgNnPc2d1	následující
700	[number]	k4	700
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
hrad	hrad	k1gInSc4	hrad
postupně	postupně	k6eAd1	postupně
předávaly	předávat	k5eAaImAgInP	předávat
různé	různý	k2eAgInPc1d1	různý
mocné	mocný	k2eAgInPc1d1	mocný
rody	rod	k1gInPc1	rod
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1766	[number]	k4	1766
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Johna	John	k1gMnSc2	John
Stuarta	Stuart	k1gMnSc2	Stuart
<g/>
,	,	kIx,	,
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Bute	But	k1gFnSc2	But
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
pravnuk	pravnuk	k1gMnSc1	pravnuk
si	se	k3xPyFc3	se
najal	najmout	k5eAaPmAgMnS	najmout
"	"	kIx"	"
<g/>
výstředního	výstřední	k2eAgMnSc2d1	výstřední
génia	génius	k1gMnSc2	génius
<g/>
"	"	kIx"	"
architekta	architekt	k1gMnSc2	architekt
Williama	William	k1gMnSc2	William
Burgese	Burgese	k1gFnSc2	Burgese
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1876	[number]	k4	1876
až	až	k9	až
1881	[number]	k4	1881
stvořil	stvořit	k5eAaPmAgInS	stvořit
okázalé	okázalý	k2eAgNnSc4d1	okázalé
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
zdobené	zdobený	k2eAgNnSc4d1	zdobené
mnoha	mnoho	k4c7	mnoho
středověkými	středověký	k2eAgInPc7d1	středověký
prvky	prvek	k1gInPc7	prvek
a	a	k8xC	a
romantickými	romantický	k2eAgInPc7d1	romantický
detaily	detail	k1gInPc7	detail
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Cardiffu	Cardiff	k1gInSc2	Cardiff
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
malých	malý	k2eAgFnPc2d1	malá
loděnic	loděnice	k1gFnPc2	loděnice
se	s	k7c7	s
soukromými	soukromý	k2eAgFnPc7d1	soukromá
jachtami	jachta	k1gFnPc7	jachta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
doky	dok	k1gInPc1	dok
na	na	k7c4	na
nákladní	nákladní	k2eAgFnPc4d1	nákladní
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Cardiff	Cardiff	k1gInSc1	Cardiff
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
děj	děj	k1gInSc1	děj
seriálu	seriál	k1gInSc2	seriál
Torchwood	Torchwooda	k1gFnPc2	Torchwooda
The	The	k1gFnSc2	The
Old	Olda	k1gFnPc2	Olda
Library	Librara	k1gFnSc2	Librara
(	(	kIx(	(
<g/>
Stará	starý	k2eAgFnSc1d1	stará
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
)	)	kIx)	)
–	–	k?	–
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
díky	díky	k7c3	díky
daňovým	daňový	k2eAgInPc3d1	daňový
výnosům	výnos	k1gInPc3	výnos
<g/>
,	,	kIx,	,
neposkytovala	poskytovat	k5eNaImAgFnS	poskytovat
jen	jen	k9	jen
knihovnické	knihovnický	k2eAgFnPc4d1	knihovnická
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
museum	museum	k1gNnSc4	museum
a	a	k8xC	a
školu	škola	k1gFnSc4	škola
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
budova	budova	k1gFnSc1	budova
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
budovu	budova	k1gFnSc4	budova
ve	v	k7c6	v
viktoriánském	viktoriánský	k2eAgInSc6d1	viktoriánský
stylu	styl	k1gInSc6	styl
s	s	k7c7	s
koridorem	koridor	k1gInSc7	koridor
s	s	k7c7	s
kachličkami	kachlička	k1gFnPc7	kachlička
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
čtyři	čtyři	k4xCgNnPc4	čtyři
roční	roční	k2eAgNnPc4d1	roční
období	období	k1gNnPc4	období
a	a	k8xC	a
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
večer	večer	k6eAd1	večer
<g/>
.	.	kIx.	.
</s>
<s>
St	St	kA	St
David	David	k1gMnSc1	David
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dewi	Dewi	k1gNnSc7	Dewi
Sant	Sant	k1gMnSc1	Sant
–	–	k?	–
moderní	moderní	k2eAgNnSc1d1	moderní
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
St	St	kA	St
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Church	Churcha	k1gFnPc2	Churcha
(	(	kIx(	(
<g/>
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
<g/>
)	)	kIx)	)
–	–	k?	–
stavba	stavba	k1gFnSc1	stavba
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
zejména	zejména	k9	zejména
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
původní	původní	k2eAgFnSc1d1	původní
normanská	normanský	k2eAgFnSc1d1	normanská
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
Velšanů	Velšan	k1gMnPc2	Velšan
proti	proti	k7c3	proti
Angličanům	Angličan	k1gMnPc3	Angličan
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
gotickém	gotický	k2eAgInSc6d1	gotický
perpendikulárním	perpendikulární	k2eAgInSc6d1	perpendikulární
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1473	[number]	k4	1473
stavba	stavba	k1gFnSc1	stavba
dokončena	dokončit	k5eAaPmNgFnS	dokončit
věží	věžit	k5eAaImIp3nS	věžit
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
řezby	řezba	k1gFnPc4	řezba
hlav	hlava	k1gFnPc2	hlava
na	na	k7c6	na
kazatelně	kazatelna	k1gFnSc6	kazatelna
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
starých	starý	k2eAgInPc2d1	starý
epitafů	epitaf	k1gInPc2	epitaf
a	a	k8xC	a
barevná	barevný	k2eAgNnPc1d1	barevné
skla	sklo	k1gNnPc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Ebeneser	Ebeneser	k1gMnSc1	Ebeneser
Chapel	Chapel	k1gMnSc1	Chapel
–	–	k?	–
bývalá	bývalý	k2eAgFnSc1d1	bývalá
kaple	kaple	k1gFnSc1	kaple
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
z	z	k7c2	z
balastu	balast	k1gInSc2	balast
(	(	kIx(	(
<g/>
různobarevné	různobarevný	k2eAgInPc4d1	různobarevný
kameny	kámen	k1gInPc4	kámen
<g/>
)	)	kIx)	)
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyvážely	vyvážet	k5eAaImAgFnP	vyvážet
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
nazpět	nazpět	k6eAd1	nazpět
pluly	plout	k5eAaImAgInP	plout
prázdné	prázdný	k2eAgInPc1d1	prázdný
zatížené	zatížený	k2eAgInPc1d1	zatížený
pouze	pouze	k6eAd1	pouze
balastem	balast	k1gInSc7	balast
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc4	návrh
provedl	provést	k5eAaPmAgMnS	provést
architekt	architekt	k1gMnSc1	architekt
R.	R.	kA	R.
G.	G.	kA	G.
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
St	St	kA	St
David	David	k1gMnSc1	David
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
RC	RC	kA	RC
Cathedral	Cathedral	k1gFnSc7	Cathedral
(	(	kIx(	(
<g/>
římsko-katolická	římskoatolický	k2eAgFnSc1d1	římsko-katolická
katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Davida	David	k1gMnSc2	David
<g/>
)	)	kIx)	)
–	–	k?	–
římsko-katolický	římskoatolický	k2eAgInSc4d1	římsko-katolický
kostel	kostel	k1gInSc4	kostel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
přílivu	příliv	k1gInSc2	příliv
katolických	katolický	k2eAgMnPc2d1	katolický
Irů	Ir	k1gMnPc2	Ir
do	do	k7c2	do
města	město	k1gNnSc2	město
a	a	k8xC	a
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Charles	Charles	k1gMnSc1	Charles
Street	Street	k1gInSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
je	být	k5eAaImIp3nS	být
katedrálou	katedrála	k1gFnSc7	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Bombardován	bombardován	k2eAgMnSc1d1	bombardován
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
opraven	opravna	k1gFnPc2	opravna
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Cardiff	Cardiff	k1gMnSc1	Cardiff
Central	Central	k1gFnSc2	Central
Market	market	k1gInSc1	market
(	(	kIx(	(
<g/>
Cardiffská	Cardiffský	k2eAgFnSc1d1	Cardiffská
centrální	centrální	k2eAgFnSc1d1	centrální
tržnice	tržnice	k1gFnSc1	tržnice
<g/>
)	)	kIx)	)
–	–	k?	–
krytá	krytý	k2eAgFnSc1d1	krytá
tržnice	tržnice	k1gFnSc1	tržnice
z	z	k7c2	z
litiny	litina	k1gFnSc2	litina
a	a	k8xC	a
skla	sklo	k1gNnSc2	sklo
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staršího	starý	k2eAgNnSc2d2	starší
tržiště	tržiště	k1gNnSc2	tržiště
<g/>
.	.	kIx.	.
</s>
<s>
Coal	Coal	k1gInSc1	Coal
Exchange	Exchange	k1gInSc1	Exchange
(	(	kIx(	(
<g/>
Uhelná	uhelný	k2eAgFnSc1d1	uhelná
burza	burza	k1gFnSc1	burza
<g/>
)	)	kIx)	)
–	–	k?	–
bývalá	bývalý	k2eAgFnSc1d1	bývalá
uhelná	uhelný	k2eAgFnSc1d1	uhelná
burza	burza	k1gFnSc1	burza
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1883	[number]	k4	1883
až	až	k9	až
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
budově	budova	k1gFnSc6	budova
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
obchodní	obchodní	k2eAgFnSc1d1	obchodní
transakce	transakce	k1gFnSc1	transakce
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Norwegian	Norwegian	k1gMnSc1	Norwegian
Church	Church	k1gMnSc1	Church
(	(	kIx(	(
<g/>
Norský	norský	k2eAgInSc1d1	norský
kostel	kostel	k1gInSc1	kostel
<g/>
)	)	kIx)	)
–	–	k?	–
přenesený	přenesený	k2eAgInSc4d1	přenesený
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
rekonstruovaný	rekonstruovaný	k2eAgInSc1d1	rekonstruovaný
ke	k	k7c3	k
stavu	stav	k1gInSc3	stav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
byl	být	k5eAaImAgMnS	být
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
spisovatel	spisovatel	k1gMnSc1	spisovatel
Roald	Roald	k1gMnSc1	Roald
Dahl	dahl	k1gInSc4	dahl
<g/>
.	.	kIx.	.
</s>
<s>
Pierhead	Pierhead	k6eAd1	Pierhead
Building	Building	k1gInSc1	Building
–	–	k?	–
novogotická	novogotický	k2eAgFnSc1d1	novogotická
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Civic	Civic	k1gMnSc1	Civic
Centre	centr	k1gInSc5	centr
(	(	kIx(	(
<g/>
Městské	městský	k2eAgNnSc1d1	Městské
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
–	–	k?	–
komplex	komplex	k1gInSc1	komplex
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
vystavěný	vystavěný	k2eAgMnSc1d1	vystavěný
v	v	k7c4	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
bývalých	bývalý	k2eAgInPc6d1	bývalý
pozemcích	pozemek	k1gInPc6	pozemek
3	[number]	k4	3
<g/>
.	.	kIx.	.
markýze	markýz	k1gMnSc4	markýz
Buteho	Bute	k1gMnSc4	Bute
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
budovami	budova	k1gFnPc7	budova
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
neoklasicistní	neoklasicistní	k2eAgFnSc1d1	neoklasicistní
radnice	radnice	k1gFnSc1	radnice
z	z	k7c2	z
portlandského	portlandský	k2eAgInSc2d1	portlandský
kamene	kámen	k1gInSc2	kámen
otevřená	otevřený	k2eAgFnSc1d1	otevřená
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Radnici	radnice	k1gFnSc4	radnice
dominuje	dominovat	k5eAaImIp3nS	dominovat
59	[number]	k4	59
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hodinová	hodinový	k2eAgFnSc1d1	hodinová
věž	věž	k1gFnSc1	věž
a	a	k8xC	a
kupole	kupole	k1gFnSc1	kupole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
je	být	k5eAaImIp3nS	být
mramorová	mramorový	k2eAgFnSc1d1	mramorová
hala	hala	k1gFnSc1	hala
se	s	k7c7	s
sochami	socha	k1gFnPc7	socha
velšských	velšský	k2eAgMnPc2d1	velšský
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
budovy	budova	k1gFnPc4	budova
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
patří	patřit	k5eAaImIp3nS	patřit
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
velšské	velšský	k2eAgNnSc4d1	Velšské
národní	národní	k2eAgNnSc4d1	národní
museum	museum	k1gNnSc4	museum
(	(	kIx(	(
<g/>
umělecké	umělecký	k2eAgNnSc4d1	umělecké
<g/>
,	,	kIx,	,
archeologické	archeologický	k2eAgNnSc4d1	Archeologické
<g/>
,	,	kIx,	,
geologické	geologický	k2eAgNnSc4d1	geologické
a	a	k8xC	a
přírodovědné	přírodovědný	k2eAgNnSc4d1	Přírodovědné
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
,	,	kIx,	,
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
především	především	k9	především
sbírka	sbírka	k1gFnSc1	sbírka
impresionistických	impresionistický	k2eAgFnPc2d1	impresionistická
maleb	malba	k1gFnPc2	malba
<g/>
)	)	kIx)	)
a	a	k8xC	a
budovy	budova	k1gFnSc2	budova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Bute	Bute	k1gInSc1	Bute
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
Buteho	Bute	k1gMnSc2	Bute
park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
–	–	k?	–
park	park	k1gInSc1	park
o	o	k7c6	o
56	[number]	k4	56
hektarech	hektar	k1gInPc6	hektar
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Taff	Taff	k1gInSc1	Taff
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
ho	on	k3xPp3gMnSc4	on
daroval	darovat	k5eAaPmAgInS	darovat
5	[number]	k4	5
<g/>
.	.	kIx.	.
markýz	markýz	k1gMnSc1	markýz
Bute	Bute	k1gNnSc4	Bute
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
základy	základ	k1gInPc4	základ
dominikánského	dominikánský	k2eAgInSc2d1	dominikánský
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
kolekci	kolekce	k1gFnSc4	kolekce
vzácných	vzácný	k2eAgInPc2d1	vzácný
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Millennium	millennium	k1gNnSc1	millennium
Stadium	stadium	k1gNnSc1	stadium
–	–	k?	–
ragbyový	ragbyový	k2eAgInSc1d1	ragbyový
stadion	stadion	k1gInSc1	stadion
otevřený	otevřený	k2eAgInSc1d1	otevřený
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
události	událost	k1gFnPc4	událost
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
74,5	[number]	k4	74,5
tisíce	tisíc	k4xCgInSc2	tisíc
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Llandaff	Llandaff	k1gMnSc1	Llandaff
Cathedral	Cathedral	k1gMnSc1	Cathedral
(	(	kIx(	(
<g/>
Llandaffská	Llandaffský	k2eAgFnSc1d1	Llandaffský
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
)	)	kIx)	)
–	–	k?	–
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
katedrály	katedrála	k1gFnSc2	katedrála
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
sv.	sv.	kA	sv.
Teliem	Telium	k1gNnSc7	Telium
v	v	k7c4	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1170	[number]	k4	1170
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
tomba	tomba	k1gFnSc1	tomba
sv.	sv.	kA	sv.
Telia	Telia	k1gFnSc1	Telia
s	s	k7c7	s
kovovým	kovový	k2eAgInSc7d1	kovový
vlysem	vlys	k1gInSc7	vlys
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
se	s	k7c7	s
scénami	scéna	k1gFnPc7	scéna
ze	z	k7c2	z
života	život	k1gInSc2	život
svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
zříceniny	zřícenina	k1gFnPc1	zřícenina
brány	brána	k1gFnSc2	brána
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bránou	brána	k1gFnSc7	brána
se	se	k3xPyFc4	se
vcházelo	vcházet	k5eAaImAgNnS	vcházet
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kdysi	kdysi	k6eAd1	kdysi
stával	stávat	k5eAaImAgInS	stávat
palác	palác	k1gInSc1	palác
llandaffských	llandaffský	k2eAgMnPc2d1	llandaffský
biskupů	biskup	k1gMnPc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Dannie	Dannie	k1gFnSc1	Dannie
Abse	Abse	k1gFnSc1	Abse
(	(	kIx(	(
<g/>
*	*	kIx~	*
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Brian	Brian	k1gMnSc1	Brian
David	David	k1gMnSc1	David
Josephson	Josephson	k1gMnSc1	Josephson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Colin	Colin	k1gMnSc1	Colin
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sprintér-překážkář	sprintérřekážkář	k1gMnSc1	sprintér-překážkář
Andy	Anda	k1gFnSc2	Anda
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
*	*	kIx~	*
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
baskytarista	baskytarista	k1gMnSc1	baskytarista
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Oasis	Oasis	k1gFnSc1	Oasis
Ryan	Ryan	k1gMnSc1	Ryan
Giggs	Giggs	k1gInSc1	Giggs
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Ioan	Ioan	k1gMnSc1	Ioan
Gruffudd	Gruffudd	k1gMnSc1	Gruffudd
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
High	High	k1gMnSc1	High
Contrast	Contrast	k1gMnSc1	Contrast
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drum	drum	k1gInSc1	drum
and	and	k?	and
bassový	bassový	k2eAgInSc1d1	bassový
<g />
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Christian	Christian	k1gMnSc1	Christian
Malcolm	Malcolm	k1gMnSc1	Malcolm
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet-sprintér	atletprintér	k1gMnSc1	atlet-sprintér
Shakin	Shakin	k1gMnSc1	Shakin
<g/>
'	'	kIx"	'
Stevens	Stevens	k1gInSc1	Stevens
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
Bergen	Bergen	k1gMnSc1	Bergen
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
Luhansk	Luhansk	k1gInSc1	Luhansk
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
Nantes	Nantes	k1gInSc1	Nantes
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Sia-men	Siaen	k1gInSc1	Sia-men
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Sucre	Sucr	k1gInSc5	Sucr
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc1	Bolívie
</s>
