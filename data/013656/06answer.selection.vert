<s>
Elektromyografie	elektromyografie	k1gFnSc1
(	(	kIx(
<g/>
EMG	EMG	kA
<g/>
)	)	kIx)
studuje	studovat	k5eAaImIp3nS
funkci	funkce	k1gFnSc4
kosterního	kosterní	k2eAgNnSc2d1
svalstva	svalstvo	k1gNnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
vyšetřuje	vyšetřovat	k5eAaImIp3nS
elektrické	elektrický	k2eAgInPc4d1
biosignály	biosignál	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
ze	z	k7c2
svalů	sval	k1gInPc2
vycházejí	vycházet	k5eAaImIp3nP
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Mohli	moct	k5eAaImAgMnP
bychom	by	kYmCp1nP
také	také	k9
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
biosignály	biosignál		k1gInPc4
<g/>
,	,	kIx,
vznikající	vznikající	k2eAgFnSc4d1
v	v	k7c6
důsledku	důsledek	k1gInSc6
svalové	svalový	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
typický	typický	k2eAgInSc1d1
příklad	příklad	k1gInSc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
takové	takový	k3xDgNnSc1
tvrzení	tvrzení	k1gNnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
zavádějící	zavádějící	k2eAgMnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
svaly	sval	k1gInPc1
produkují	produkovat	k5eAaImIp3nP
elektrickou	elektrický	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jsou	být	k5eAaImIp3nP
v	v	k7c6
klidu	klid	k1gInSc6
–	–	k?
např.	např.	kA
ploténkový	ploténkový	k2eAgInSc4d1
šum	šum	k1gInSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>