<s>
Elektromyografie	elektromyografie	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Elektromyografie	elektromyografie	k1gFnSc1
(	(	kIx(
<g/>
EMG	EMG	kA
<g/>
)	)	kIx)
studuje	studovat	k5eAaImIp3nS
funkci	funkce	k1gFnSc4
kosterního	kosterní	k2eAgNnSc2d1
svalstva	svalstvo	k1gNnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
vyšetřuje	vyšetřovat	k5eAaImIp3nS
elektrické	elektrický	k2eAgInPc4d1
biosignály	biosignál	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
ze	z	k7c2
svalů	sval	k1gInPc2
vycházejí	vycházet	k5eAaImIp3nP
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Mohli	moct	k5eAaImAgMnP
bychom	by	kYmCp1nP
také	také	k9
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
biosignály	biosignál		k1gInPc4
<g/>
,	,	kIx,
vznikající	vznikající	k2eAgFnSc4d1
v	v	k7c6
důsledku	důsledek	k1gInSc6
svalové	svalový	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
typický	typický	k2eAgInSc1d1
příklad	příklad	k1gInSc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
takové	takový	k3xDgNnSc1
tvrzení	tvrzení	k1gNnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
zavádějící	zavádějící	k2eAgMnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
svaly	sval	k1gInPc1
produkují	produkovat	k5eAaImIp3nP
elektrickou	elektrický	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jsou	být	k5eAaImIp3nP
v	v	k7c6
klidu	klid	k1gInSc6
–	–	k?
např.	např.	kA
ploténkový	ploténkový	k2eAgInSc4d1
šum	šum	k1gInSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Zvýšený	zvýšený	k2eAgInSc1d1
zájem	zájem	k1gInSc1
o	o	k7c4
studium	studium	k1gNnSc4
svalových	svalový	k2eAgInPc2d1
pohybů	pohyb	k1gInPc2
můžeme	moct	k5eAaImIp1nP
pozorovat	pozorovat	k5eAaImF
v	v	k7c6
renesanci	renesance	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
zejména	zejména	k9
polyhistor	polyhistor	k1gMnSc1
Leonardo	Leonardo	k1gMnSc1
da	da	k?
Vinci	Vinca	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
tak	tak	k6eAd1
„	„	k?
<g/>
otec	otec	k1gMnSc1
<g/>
“	“	k?
moderní	moderní	k2eAgFnSc2d1
anatomie	anatomie	k1gFnSc2
Andreas	Andreas	k1gMnSc1
Vesalius	Vesalius	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
díle	díl	k1gInSc6
Fabrica	Fabrica	k1gMnSc1
–	–	k?
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
však	však	k9
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
spíše	spíše	k9
popisu	popis	k1gInSc3
mrtvých	mrtvý	k2eAgInPc2d1
svalů	sval	k1gInPc2
než	než	k8xS
jejich	jejich	k3xOp3gFnSc3
dynamice	dynamika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc4
logickou	logický	k2eAgFnSc4d1
dedukci	dedukce	k1gFnSc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
svaly	sval	k1gInPc1
musí	muset	k5eAaImIp3nP
vykazovat	vykazovat	k5eAaImF
elektrickou	elektrický	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
<g/>
,	,	kIx,
dokumentoval	dokumentovat	k5eAaBmAgMnS
Ital	Ital	k1gMnSc1
Francesco	Francesco	k1gMnSc1
Redi	Rede	k1gFnSc4
v	v	k7c6
r.	r.	kA
1666	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
rána	rána	k1gFnSc1
od	od	k7c2
rejnoka	rejnok	k1gMnSc2
elektrického	elektrický	k2eAgMnSc2d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
původ	původ	k1gInSc4
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
svalech	sval	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
mezi	mezi	k7c7
elektřinou	elektřina	k1gFnSc7
a	a	k8xC
svalovou	svalový	k2eAgFnSc7d1
kontrakcí	kontrakce	k1gFnSc7
poprvé	poprvé	k6eAd1
pozoroval	pozorovat	k5eAaImAgMnS
Luigi	Luige	k1gFnSc4
Galvani	Galvaň	k1gFnSc3
v	v	k7c6
r.	r.	kA
1791	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Depolarizoval	depolarizovat	k5eAaImAgMnS
svaly	sval	k1gInPc4
žabího	žabí	k2eAgNnSc2d1
stehýnka	stehýnko	k1gNnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
dotknul	dotknout	k5eAaPmAgMnS
kovovou	kovový	k2eAgFnSc7d1
tyčí	tyč	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
interpretaci	interpretace	k1gFnSc6
svých	svůj	k3xOyFgInPc2
pokusů	pokus	k1gInPc2
mýlil	mýlit	k5eAaImAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
vzniklý	vzniklý	k2eAgInSc1d1
galvanický	galvanický	k2eAgInSc1d1
článek	článek	k1gInSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
De	De	k?
Viribus	Viribus	k1gInSc1
Electricitatis	Electricitatis	k1gFnSc1
in	in	k?
Motu	moto	k1gNnSc3
Musculari	Muscular	k1gFnSc2
Commentarius	Commentarius	k1gInSc1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
považoval	považovat	k5eAaImAgInS
za	za	k7c4
projev	projev	k1gInSc4
"	"	kIx"
<g/>
živočišné	živočišný	k2eAgFnSc2d1
elektřiny	elektřina	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
přece	přece	k9
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
jeho	jeho	k3xOp3gNnPc4
pozorování	pozorování	k1gNnPc4
považovat	považovat	k5eAaImF
za	za	k7c4
zrození	zrození	k1gNnSc4
elektroneurofyziologie	elektroneurofyziologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Že	že	k8xS
se	se	k3xPyFc4
v	v	k7c4
Galvaniho	Galvani	k1gMnSc4
pokusech	pokus	k1gInPc6
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
elektrochemický	elektrochemický	k2eAgInSc4d1
článek	článek	k1gInSc4
<g/>
,	,	kIx,
sestavený	sestavený	k2eAgInSc4d1
z	z	k7c2
elektrolytu	elektrolyt	k1gInSc2
a	a	k8xC
dvou	dva	k4xCgFnPc2
elektrod	elektroda	k1gFnPc2
z	z	k7c2
různých	různý	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
,	,	kIx,
kterým	který	k3yRgMnPc3,k3yQgMnPc3,k3yIgMnPc3
po	po	k7c6
uzavření	uzavření	k1gNnSc6
elektrického	elektrický	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
prochází	procházet	k5eAaImIp3nS
elektrický	elektrický	k2eAgInSc4d1
proud	proud	k1gInSc4
<g/>
,	,	kIx,
objevil	objevit	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c4
Galvaniho	Galvani	k1gMnSc4
prvním	první	k4xOgInSc6
experimentu	experiment	k1gInSc6
r.	r.	kA
1793	#num#	k4
Alessandro	Alessandra	k1gFnSc5
Volta	Volta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
toto	tento	k3xDgNnSc4
zjištění	zjištění	k1gNnSc4
zareagoval	zareagovat	k5eAaPmAgInS
Galvani	Galvaň	k1gFnSc3
hned	hned	k6eAd1
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
stah	stah	k1gInSc1
žabích	žabí	k2eAgInPc2d1
svalů	sval	k1gInPc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
vyvolat	vyvolat	k5eAaPmF
i	i	k9
přiložením	přiložení	k1gNnSc7
volného	volný	k2eAgInSc2d1
konce	konec	k1gInSc2
nervu	nerv	k1gInSc2
bez	bez	k7c2
přítomnosti	přítomnost	k1gFnSc2
jakéhokoliv	jakýkoliv	k3yIgInSc2
kovu	kov	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
utvrdil	utvrdit	k5eAaPmAgInS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
konceptu	koncept	k1gInSc6
živočišné	živočišný	k2eAgFnSc2d1
elektřiny	elektřina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
Voltův	Voltův	k2eAgInSc1d1
vliv	vliv	k1gInSc1
byl	být	k5eAaImAgInS
natolik	natolik	k6eAd1
silný	silný	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
diskuse	diskuse	k1gFnPc1
o	o	k7c6
koncepci	koncepce	k1gFnSc6
Galvaniho	Galvani	k1gMnSc2
živočišné	živočišný	k2eAgFnSc2d1
elektřiny	elektřina	k1gFnSc2
na	na	k7c4
další	další	k2eAgNnPc4d1
čtyři	čtyři	k4xCgNnPc4
desetiletí	desetiletí	k1gNnPc4
utichla	utichnout	k5eAaPmAgFnS
-	-	kIx~
zatímco	zatímco	k8xS
na	na	k7c4
Voltův	Voltův	k2eAgInSc4d1
objev	objev	k1gInSc4
navázal	navázat	k5eAaPmAgInS
vývoj	vývoj	k1gInSc1
galvanických	galvanický	k2eAgInPc2d1
článků	článek	k1gInPc2
pro	pro	k7c4
technické	technický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
živočišné	živočišný	k2eAgFnSc2d1
elektřiny	elektřina	k1gFnSc2
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
nikdo	nikdo	k3yNnSc1
nepokračoval	pokračovat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
r.	r.	kA
1820	#num#	k4
sestavil	sestavit	k5eAaPmAgInS
Schweigger	Schweigger	k1gInSc1
první	první	k4xOgInSc1
galvanometr	galvanometr	k1gInSc1
<g/>
,	,	kIx,
založený	založený	k2eAgInSc1d1
na	na	k7c6
Oerstedových	Oerstedův	k2eAgInPc6d1
objevech	objev	k1gInPc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
magnetismu	magnetismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc4
let	léto	k1gNnPc2
poté	poté	k6eAd1
zdokonalil	zdokonalit	k5eAaPmAgInS
Nobili	Nobili	k1gFnSc2
galvanometr	galvanometr	k1gInSc1
kompenzací	kompenzace	k1gFnPc2
zemského	zemský	k2eAgInSc2d1
magnetismu	magnetismus	k1gInSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
zvýšil	zvýšit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gFnSc4
citlivost	citlivost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
použitím	použití	k1gNnSc7
takového	takový	k3xDgInSc2
galvanometru	galvanometr	k1gInSc2
Carlo	Carlo	k1gNnSc1
Matteucci	Matteucce	k1gFnSc4
v	v	k7c4
r.	r.	kA
1844	#num#	k4
konečně	konečně	k6eAd1
dokázal	dokázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
svaly	sval	k1gInPc1
produkují	produkovat	k5eAaImIp3nP
elektrický	elektrický	k2eAgInSc4d1
proud	proud	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Matteuciho	Matteucize	k6eAd1
prací	prací	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
inspirován	inspirován	k2eAgMnSc1d1
Frenchman	Frenchman	k1gMnSc1
DuBois-Reymond	DuBois-Reymond	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
r.	r.	kA
1849	#num#	k4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
Uber	ubrat	k5eAaPmRp2nS
Thierische	Thierische	k1gInSc4
Elektricitat	Elektricitat	k1gMnSc2
podal	podat	k5eAaPmAgMnS
první	první	k4xOgFnSc4
zprávu	zpráva	k1gFnSc4
a	a	k8xC
elektrických	elektrický	k2eAgInPc6d1
signálech	signál	k1gInPc6
<g/>
,	,	kIx,
produkovaných	produkovaný	k2eAgNnPc6d1
při	při	k7c6
volní	volní	k2eAgFnSc6d1
aktivitě	aktivita	k1gFnSc6
lidských	lidský	k2eAgInPc2d1
kosterních	kosterní	k2eAgInPc2d1
svalů	sval	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
objevil	objevit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
velikost	velikost	k1gFnSc1
zaznamenaného	zaznamenaný	k2eAgInSc2d1
proudu	proud	k1gInSc2
je	být	k5eAaImIp3nS
snížena	snížit	k5eAaPmNgFnS
impedancí	impedance	k1gFnSc7
kůže	kůže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
snímání	snímání	k1gNnSc3
signálu	signál	k1gInSc2
použil	použít	k5eAaPmAgMnS
drátěnou	drátěný	k2eAgFnSc4d1
elektrodu	elektroda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Němec	Němec	k1gMnSc1
H.	H.	kA
Piper	Piper	k1gMnSc1
r.	r.	kA
1907	#num#	k4
(	(	kIx(
<g/>
Uber	ubrat	k5eAaPmRp2nS
den	den	k1gInSc4
willkurlichen	willkurlichen	k2eAgMnSc1d1
Muskeltetanus	Muskeltetanus	k1gMnSc1
<g/>
)	)	kIx)
zdokonalil	zdokonalit	k5eAaPmAgMnS
metodu	metoda	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zavedl	zavést	k5eAaPmAgMnS
kovové	kovový	k2eAgFnPc4d1
plošné	plošný	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
pokrok	pokrok	k1gInSc1
v	v	k7c6
technice	technika	k1gFnSc6
záznamu	záznam	k1gInSc2
představoval	představovat	k5eAaImAgInS
objev	objev	k1gInSc1
katodové	katodový	k2eAgFnSc2d1
trubice	trubice	k1gFnSc2
<g/>
,	,	kIx,
navržený	navržený	k2eAgInSc4d1
Braunem	Braun	k1gMnSc7
r.	r.	kA
1897	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
poprvé	poprvé	k6eAd1
použili	použít	k5eAaPmAgMnP
k	k	k7c3
zesílení	zesílení	k1gNnSc3
akčních	akční	k2eAgInPc2d1
potenciálů	potenciál	k1gInPc2
–	–	k?
ve	v	k7c6
spojení	spojení	k1gNnSc6
se	s	k7c7
strunovým	strunový	k2eAgInSc7d1
galvanometrem	galvanometr	k1gInSc7
–	–	k?
A.	A.	kA
Forbes	forbes	k1gInSc1
a	a	k8xC
C.	C.	kA
Thatcher	Thatchra	k1gFnPc2
v	v	k7c6
r.	r.	kA
1920	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
H.	H.	kA
S.	S.	kA
Gasser	Gasser	k1gInSc4
a	a	k8xC
J.	J.	kA
Erlanger	Erlanger	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
r.	r.	kA
1922	#num#	k4
<g/>
)	)	kIx)
použili	použít	k5eAaPmAgMnP
namísto	namísto	k7c2
galvanometru	galvanometr	k1gInSc2
osciloskopu	osciloskop	k1gInSc2
<g/>
,	,	kIx,
založeného	založený	k2eAgInSc2d1
na	na	k7c6
principu	princip	k1gInSc6
katodové	katodový	k2eAgFnSc2d1
trubice	trubice	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yRnSc4,k3yQnSc4
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
se	s	k7c7
zdárnou	zdárný	k2eAgFnSc7d1
interpretací	interpretace	k1gFnSc7
akčního	akční	k2eAgInSc2d1
potenciálu	potenciál	k1gInSc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
odměněni	odměněn	k2eAgMnPc1d1
v	v	k7c6
r.	r.	kA
1944	#num#	k4
Nobelovou	Nobelův	k2eAgFnSc7d1
cenou	cena	k1gFnSc7
za	za	k7c4
fyziologii	fyziologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zatímco	zatímco	k8xS
v	v	k7c6
průběhu	průběh	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
ještě	ještě	k9
detekce	detekce	k1gFnSc1
myografických	myografický	k2eAgInPc2d1
potenciálů	potenciál	k1gInPc2
velmi	velmi	k6eAd1
obtížná	obtížný	k2eAgFnSc1d1
a	a	k8xC
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
jen	jen	k9
nemnoho	mnoho	k6eNd1,k6eAd1
vědců	vědec	k1gMnPc2
bylo	být	k5eAaImAgNnS
schopno	schopen	k2eAgNnSc4d1
jejich	jejich	k3xOp3gInSc2
výzkumu	výzkum	k1gInSc2
<g/>
,	,	kIx,
opačný	opačný	k2eAgInSc1d1
proces	proces	k1gInSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
vybuzení	vybuzení	k1gNnSc1
svalových	svalový	k2eAgInPc2d1
záškubů	záškub	k1gInPc2
působením	působení	k1gNnSc7
elektrického	elektrický	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
dal	dát	k5eAaPmAgInS
demonstrovat	demonstrovat	k5eAaBmF
velmi	velmi	k6eAd1
snadno	snadno	k6eAd1
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
i	i	k8xC
stal	stát	k5eAaPmAgInS
nástrojem	nástroj	k1gInSc7
mnohých	mnohý	k2eAgMnPc2d1
léčitelů	léčitel	k1gMnPc2
a	a	k8xC
šarlatánů	šarlatán	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
alespoň	alespoň	k9
někteří	některý	k3yIgMnPc1
vědci	vědec	k1gMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
provádět	provádět	k5eAaImF
výzkum	výzkum	k1gInSc4
těchto	tento	k3xDgInPc2
jevů	jev	k1gInPc2
odpovědně	odpovědně	k6eAd1
systematicky	systematicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
francouz	francouz	k1gInSc4
G.	G.	kA
B.	B.	kA
A.	A.	kA
Duchenne	Duchenn	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
polovině	polovina	k1gFnSc6
devatenáctého	devatenáctý	k4xOgNnSc2
století	století	k1gNnSc2
systematicky	systematicky	k6eAd1
zkoumal	zkoumat	k5eAaImAgInS
dynamiku	dynamika	k1gFnSc4
a	a	k8xC
funkci	funkce	k1gFnSc4
kosterního	kosterní	k2eAgNnSc2d1
svalstva	svalstvo	k1gNnSc2
a	a	k8xC
své	svůj	k3xOyFgInPc4
výzkumy	výzkum	k1gInPc4
popsal	popsat	k5eAaPmAgMnS
v	v	k7c6
klasickém	klasický	k2eAgInSc6d1
díle	díl	k1gInSc6
Physiologie	Physiologie	k1gFnSc2
des	des	k1gNnSc1
Mouvements	Mouvements	k1gInSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Detekce	detekce	k1gFnSc1
elektrických	elektrický	k2eAgInPc2d1
signálů	signál	k1gInPc2
ze	z	k7c2
svalu	sval	k1gInSc2
a	a	k8xC
aplikace	aplikace	k1gFnSc2
elektrického	elektrický	k2eAgInSc2d1
proudu	proud	k1gInSc2
přitáhla	přitáhnout	k5eAaPmAgFnS
pozornost	pozornost	k1gFnSc1
anglického	anglický	k2eAgMnSc2d1
elektrického	elektrický	k2eAgMnSc2d1
inženýra	inženýr	k1gMnSc2
Bainese	Bainese	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baines	Baines	k1gMnSc1
byl	být	k5eAaImAgMnS
první	první	k4xOgMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
přirovnal	přirovnat	k5eAaPmAgMnS
šíření	šíření	k1gNnSc4
vzruchu	vzruch	k1gInSc2
nervem	nerv	k1gInSc7
k	k	k7c3
šíření	šíření	k1gNnSc3
elektrického	elektrický	k2eAgInSc2d1
signálu	signál	k1gInSc2
kabelem	kabel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
analogie	analogie	k1gFnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
ve	v	k7c4
„	„	k?
<g/>
století	století	k1gNnSc4
elektřiny	elektřina	k1gFnSc2
<g/>
“	“	k?
velmi	velmi	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baines	Bainesa	k1gFnPc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
publikoval	publikovat	k5eAaBmAgMnS
svůj	svůj	k3xOyFgInSc4
elektrický	elektrický	k2eAgInSc4d1
model	model	k1gInSc4
nervového	nervový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
neurony	neuron	k1gInPc1
a	a	k8xC
svalová	svalový	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
nahradil	nahradit	k5eAaPmAgInS
elektrickými	elektrický	k2eAgInPc7d1
obvody	obvod	k1gInPc7
<g/>
,	,	kIx,
sestavenými	sestavený	k2eAgInPc7d1
z	z	k7c2
galvanického	galvanický	k2eAgInSc2d1
článku	článek	k1gInSc2
a	a	k8xC
množství	množství	k1gNnSc2
kondenzátorů	kondenzátor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
o	o	k7c6
Bainesovi	Baines	k1gMnSc6
hovoří	hovořit	k5eAaImIp3nS
jako	jako	k9
o	o	k7c6
prvním	první	k4xOgInSc6
biomedicínském	biomedicínský	k2eAgMnSc6d1
inženýrovi	inženýr	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
elektronkových	elektronkový	k2eAgInPc2d1
zesilovačů	zesilovač	k1gInPc2
umožnilo	umožnit	k5eAaPmAgNnS
klinické	klinický	k2eAgNnSc1d1
využití	využití	k1gNnSc1
vyšetřování	vyšetřování	k1gNnSc2
biosignálů	biosignál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokus	pokus	k1gInSc1
zaznamenat	zaznamenat	k5eAaPmF
signál	signál	k1gInSc4
z	z	k7c2
dysfunkčního	dysfunkční	k2eAgInSc2d1
svalu	sval	k1gInSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
podařil	podařit	k5eAaPmAgMnS
Proebsterovi	Proebster	k1gMnSc3
r.	r.	kA
1928	#num#	k4
(	(	kIx(
<g/>
Uber	ubrat	k5eAaPmRp2nS
Muskelaktionsstrome	Muskelaktionsstrom	k1gInSc5
am	am	k?
gesunden	gesundna	k1gFnPc2
und	und	k?
Kranken	Kranken	k1gInSc1
Menschen	Menschen	k1gInSc1
<g/>
)	)	kIx)
při	při	k7c6
vyšetřování	vyšetřování	k1gNnSc6
pacienta	pacient	k1gMnSc2
<g/>
,	,	kIx,
trpícího	trpící	k2eAgMnSc2d1
obrnou	obrna	k1gFnSc7
periferního	periferní	k2eAgInSc2d1
nervu	nerv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1
rozmach	rozmach	k1gInSc1
klinické	klinický	k2eAgFnSc2d1
myografie	myografie	k1gFnSc2
přineslo	přinést	k5eAaPmAgNnS
až	až	k9
použití	použití	k1gNnSc4
jehlové	jehlový	k2eAgFnSc2d1
elektrody	elektroda	k1gFnSc2
Adrianem	Adrian	k1gMnSc7
a	a	k8xC
Bronkem	Bronek	k1gMnSc7
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
umožnilo	umožnit	k5eAaPmAgNnS
sledování	sledování	k1gNnSc1
aktivity	aktivita	k1gFnSc2
<g/>
,	,	kIx,
spojené	spojený	k2eAgFnPc1d1
s	s	k7c7
činností	činnost	k1gFnSc7
jednotlivých	jednotlivý	k2eAgNnPc2d1
svalových	svalový	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
(	(	kIx(
<g/>
či	či	k8xC
malých	malý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
svalových	svalový	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využití	využití	k1gNnSc1
elektromyografických	elektromyografický	k2eAgFnPc2d1
jehel	jehla	k1gFnPc2
dále	daleko	k6eAd2
zdokonalil	zdokonalit	k5eAaPmAgMnS
v	v	k7c6
průběhu	průběh	k1gInSc6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
F.	F.	kA
Buchthal	Buchthal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
skupina	skupina	k1gFnSc1
ruských	ruský	k2eAgMnPc2d1
inženýrů	inženýr	k1gMnPc2
vedená	vedený	k2eAgFnSc1d1
Kobrinským	Kobrinský	k2eAgFnPc3d1
uvedla	uvést	k5eAaPmAgFnS
návrh	návrh	k1gInSc4
elektronické	elektronický	k2eAgFnSc2d1
protézy	protéza	k1gFnSc2
ruky	ruka	k1gFnSc2
<g/>
,	,	kIx,
ovládané	ovládaný	k2eAgInPc1d1
myoelektrickými	myoelektrický	k2eAgInPc7d1
signály	signál	k1gInPc7
(	(	kIx(
<g/>
kterýžto	kterýžto	k?
nápad	nápad	k1gInSc1
si	se	k3xPyFc3
dal	dát	k5eAaPmAgInS
v	v	k7c6
Německu	Německo	k1gNnSc6
patentovat	patentovat	k5eAaBmF
již	již	k9
r.	r.	kA
1945	#num#	k4
Reinhold	Reinholdo	k1gNnPc2
Reiter	Reitra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Procedura	procedura	k1gFnSc1
</s>
<s>
EMG	EMG	kA
se	se	k3xPyFc4
často	často	k6eAd1
provádí	provádět	k5eAaImIp3nS
při	při	k7c6
současné	současný	k2eAgFnSc6d1
stimulaci	stimulace	k1gFnSc6
příslušných	příslušný	k2eAgInPc2d1
motorických	motorický	k2eAgInPc2d1
a	a	k8xC
periferních	periferní	k2eAgInPc2d1
nervů	nerv	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgInPc1d1
snímání	snímání	k1gNnSc6
lze	lze	k6eAd1
provádět	provádět	k5eAaImF
invazivně	invazivně	k6eAd1
i	i	k9
neinvazivně	invazivně	k6eNd1
<g/>
,	,	kIx,
na	na	k7c6
úrovni	úroveň	k1gFnSc6
jediného	jediný	k2eAgNnSc2d1
svalového	svalový	k2eAgNnSc2d1
vlákna	vlákno	k1gNnSc2
<g/>
,	,	kIx,
jedné	jeden	k4xCgFnSc2
motorické	motorický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
i	i	k8xC
celého	celý	k2eAgInSc2d1
svalu	sval	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Elektrody	elektroda	k1gFnPc1
pro	pro	k7c4
EMG	EMG	kA
</s>
<s>
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
povrchové	povrchový	k2eAgFnSc2d1
i	i	k8xC
podpovrchové	podpovrchový	k2eAgFnSc2d1
elektrody	elektroda	k1gFnSc2
složení	složení	k1gNnSc2
Ag	Ag	k1gFnSc2
<g/>
,	,	kIx,
či	či	k8xC
AgCl	AgCl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Povrchové	povrchový	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
</s>
<s>
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
povrchově	povrchově	k6eAd1
tzv.	tzv.	kA
multielektrody	multielektrod	k1gInPc1
-	-	kIx~
stripsy	strips	k1gInPc1
nebo	nebo	k8xC
gridy	grid	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strips	Strips	k1gInSc1
je	být	k5eAaImIp3nS
silikonový	silikonový	k2eAgInSc1d1
(	(	kIx(
<g/>
případně	případně	k6eAd1
plastový	plastový	k2eAgInSc1d1
<g/>
)	)	kIx)
pásek	pásek	k1gInSc1
s	s	k7c7
kontaktními	kontaktní	k2eAgFnPc7d1
ploškami	ploška	k1gFnPc7
elektrod	elektroda	k1gFnPc2
v	v	k7c6
linii	linie	k1gFnSc6
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grid	Grid	k1gInSc1
je	být	k5eAaImIp3nS
dvourozměrné	dvourozměrný	k2eAgNnSc4d1
pole	pole	k1gNnSc4
s	s	k7c7
ploškami	ploška	k1gFnPc7
elektrod	elektroda	k1gFnPc2
(	(	kIx(
<g/>
často	často	k6eAd1
tvořenými	tvořený	k2eAgFnPc7d1
Ag	Ag	k1gFnPc7
<g/>
/	/	kIx~
<g/>
AgCl	AgCla	k1gFnPc2
kuličkami	kulička	k1gFnPc7
uspořádanými	uspořádaný	k2eAgFnPc7d1
maticově	maticově	k6eAd1
<g/>
)	)	kIx)
na	na	k7c6
silikonové	silikonový	k2eAgFnSc6d1
či	či	k8xC
plastové	plastový	k2eAgFnSc6d1
podložce	podložka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gridy	Grid	k1gMnPc7
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
mapování	mapování	k1gNnSc3
rozložení	rozložení	k1gNnSc2
potenciálů	potenciál	k1gInPc2
generovaných	generovaný	k2eAgMnPc2d1
svalem	sval	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupná	dostupný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
verze	verze	k1gFnSc1
plovoucích	plovoucí	k2eAgMnPc2d1
gridů	grid	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
namísto	namísto	k7c2
vystouplých	vystouplý	k2eAgFnPc2d1
stříbrných	stříbrný	k2eAgFnPc2d1
kuliček	kulička	k1gFnPc2
používají	používat	k5eAaImIp3nP
prohlubně	prohlubeň	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgNnPc6
je	být	k5eAaImIp3nS
aplikovaný	aplikovaný	k2eAgInSc1d1
vodivý	vodivý	k2eAgInSc1d1
gel	gel	k1gInSc1
-	-	kIx~
tak	tak	k6eAd1
se	se	k3xPyFc4
redukuje	redukovat	k5eAaBmIp3nS
vznik	vznik	k1gInSc1
artefaktů	artefakt	k1gInPc2
ve	v	k7c6
snímaném	snímaný	k2eAgInSc6d1
signálu	signál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
gridy	gridy	k6eAd1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
i	i	k9
jednorázové	jednorázový	k2eAgNnSc1d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
spodní	spodní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
je	být	k5eAaImIp3nS
opatřena	opatřit	k5eAaPmNgFnS
samolepicí	samolepicí	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
<g/>
,	,	kIx,
případně	případně	k6eAd1
se	se	k3xPyFc4
připevňují	připevňovat	k5eAaImIp3nP
samolepicí	samolepicí	k2eAgFnSc7d1
páskou	páska	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Multielektrodový	Multielektrodový	k2eAgInSc1d1
EMG	EMG	kA
grid	grid	k1gInSc1
</s>
<s>
Podpovrchové	podpovrchový	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
</s>
<s>
Jehlové	jehlový	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
bipolární	bipolární	k2eAgFnPc4d1
či	či	k8xC
multipolární	multipolární	k2eAgFnPc4d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
tenký	tenký	k2eAgInSc4d1
drátek	drátek	k1gInSc4
či	či	k8xC
svazek	svazek	k1gInSc4
drátků	drátek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bipolární	bipolární	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
je	být	k5eAaImIp3nS
složena	složit	k5eAaPmNgFnS
z	z	k7c2
duté	dutý	k2eAgFnSc2d1
jehly	jehla	k1gFnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
středem	střed	k1gInSc7
prochází	procházet	k5eAaImIp3nS
izolovaný	izolovaný	k2eAgInSc4d1
vodič	vodič	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
před	před	k7c7
hrotem	hrot	k1gInSc7
jehly	jehla	k1gFnSc2
vychází	vycházet	k5eAaImIp3nS
na	na	k7c4
povrch	povrch	k6eAd1wR
její	její	k3xOp3gFnSc7
stěnou	stěna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kovové	kovový	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
jehly	jehla	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
společnou	společný	k2eAgFnSc4d1
elektrodu	elektroda	k1gFnSc4
a	a	k8xC
středový	středový	k2eAgInSc4d1
vodič	vodič	k1gInSc4
druhou	druhý	k4xOgFnSc4
elektrodu	elektroda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehlová	jehlový	k2eAgFnSc1d1
multipolární	multipolární	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
je	být	k5eAaImIp3nS
konstrukčně	konstrukčně	k6eAd1
totožná	totožný	k2eAgFnSc1d1
s	s	k7c7
bipolární	bipolární	k2eAgFnSc7d1
s	s	k7c7
tím	ten	k3xDgInSc7
rozdílem	rozdíl	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
středem	střed	k1gInSc7
vede	vést	k5eAaImIp3nS
(	(	kIx(
<g/>
a	a	k8xC
na	na	k7c4
povrch	povrch	k1gInSc4
stěnou	stěna	k1gFnSc7
vystupuje	vystupovat	k5eAaImIp3nS
<g/>
)	)	kIx)
vzájemně	vzájemně	k6eAd1
izolovaných	izolovaný	k2eAgInPc2d1
vodičů	vodič	k1gInPc2
hned	hned	k6eAd1
několik	několik	k4yIc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Multipolární	Multipolární	k2eAgFnSc7d1
elektrodou	elektroda	k1gFnSc7
lze	lze	k6eAd1
tedy	tedy	k9
snímat	snímat	k5eAaImF
několik	několik	k4yIc1
signálu	signál	k1gInSc2
na	na	k7c6
velmi	velmi	k6eAd1
malé	malý	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciální	speciální	k2eAgFnSc7d1
jehlovou	jehlový	k2eAgFnSc7d1
elektrodou	elektroda	k1gFnSc7
se	s	k7c7
snímací	snímací	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
25	#num#	k4
<g/>
μ	μ	k1gFnPc2
lze	lze	k6eAd1
měřit	měřit	k5eAaImF
signál	signál	k1gInSc4
z	z	k7c2
jediného	jediný	k2eAgNnSc2d1
svalového	svalový	k2eAgNnSc2d1
vlákna	vlákno	k1gNnSc2
(	(	kIx(
<g/>
pro	pro	k7c4
metodu	metoda	k1gFnSc4
SFEMG	SFEMG	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podpovrchové	podpovrchový	k2eAgFnPc1d1
elektrody	elektroda	k1gFnPc1
se	se	k3xPyFc4
zavádějí	zavádět	k5eAaImIp3nP
kolmo	kolmo	k6eAd1
na	na	k7c4
osu	osa	k1gFnSc4
svalu	sval	k1gInSc2
nebo	nebo	k8xC
podélně	podélně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehlovou	jehlový	k2eAgFnSc4d1
elektrodu	elektroda	k1gFnSc4
lze	lze	k6eAd1
dobře	dobře	k6eAd1
umístit	umístit	k5eAaPmF
ve	v	k7c6
svalu	sval	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
bolestivou	bolestivý	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
a	a	k8xC
navíc	navíc	k6eAd1
jehla	jehla	k1gFnSc1
omezuje	omezovat	k5eAaImIp3nS
pohyb	pohyb	k1gInSc4
svalu	sval	k1gInSc2
a	a	k8xC
sval	sval	k1gInSc1
zároveň	zároveň	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
elektrodou	elektroda	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vede	vést	k5eAaImIp3nS
ke	k	k7c3
vzniku	vznik	k1gInSc3
nežádoucích	žádoucí	k2eNgInPc2d1
artefaktů	artefakt	k1gInPc2
v	v	k7c6
signálu	signál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehlová	jehlový	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
může	moct	k5eAaImIp3nS
zůstat	zůstat	k5eAaPmF
zavedena	zavést	k5eAaPmNgFnS
do	do	k7c2
svalu	sval	k1gInSc2
maximálně	maximálně	k6eAd1
hodinu	hodina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
zavedený	zavedený	k2eAgInSc4d1
drát	drát	k1gInSc4
lze	lze	k6eAd1
ponechat	ponechat	k5eAaPmF
ve	v	k7c6
svalu	sval	k1gInSc6
až	až	k9
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nelze	lze	k6eNd1
s	s	k7c7
ním	on	k3xPp3gInSc7
dobře	dobře	k6eAd1
manipulovat	manipulovat	k5eAaImF
a	a	k8xC
lokalizovat	lokalizovat	k5eAaBmF
konkrétní	konkrétní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
zájmu	zájem	k1gInSc2
ve	v	k7c6
svalu	sval	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drátové	drátový	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
nechceme	chtít	k5eNaImIp1nP
omezovat	omezovat	k5eAaImF
pohyb	pohyb	k1gInSc4
svalstva	svalstvo	k1gNnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
drát	drát	k5eAaImF
se	se	k3xPyFc4
pohybem	pohyb	k1gInSc7
může	moct	k5eAaImIp3nS
povytáhnout	povytáhnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jehlová	jehlový	k2eAgFnSc1d1
multipolární	multipolární	k2eAgFnSc1d1
elektromyografická	elektromyografický	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Elektrické	elektrický	k2eAgFnPc1d1
charakteristiky	charakteristika	k1gFnPc1
</s>
<s>
V	v	k7c6
případě	případ	k1gInSc6
neinvazivního	invazivní	k2eNgNnSc2d1
měření	měření	k1gNnSc2
na	na	k7c6
povrchu	povrch	k1gInSc6
kůže	kůže	k1gFnSc2
zaznamenáváme	zaznamenávat	k5eAaImIp1nP
signál	signál	k1gInSc4
vzniklý	vzniklý	k2eAgInSc4d1
činností	činnost	k1gFnSc7
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
motorických	motorický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Signál	signál	k1gInSc1
dosahuje	dosahovat	k5eAaImIp3nS
špičkové	špičkový	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
do	do	k7c2
10	#num#	k4
mV	mV	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značný	značný	k2eAgInSc4d1
objem	objem	k1gInSc4
tkáně	tkáň	k1gFnSc2
mezi	mezi	k7c7
elektrodami	elektroda	k1gFnPc7
a	a	k8xC
svalovými	svalový	k2eAgNnPc7d1
vlákny	vlákno	k1gNnPc7
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
rozhraní	rozhraní	k1gNnSc2
elektroda-pokožka	elektroda-pokožka	k1gFnSc1
omezují	omezovat	k5eAaImIp3nP
horní	horní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
využitelného	využitelný	k2eAgNnSc2d1
frekvenčního	frekvenční	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
na	na	k7c4
500	#num#	k4
Hz	Hz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
část	část	k1gFnSc1
výkonového	výkonový	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
leží	ležet	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
50-150	50-150	k4
Hz	Hz	kA
<g/>
.	.	kIx.
</s>
<s>
Invazivním	invazivní	k2eAgInSc7d1
<g/>
,	,	kIx,
tzv.	tzv.	kA
intramuskulárním	intramuskulární	k2eAgInSc6d1
EMG	EMG	kA
lze	lze	k6eAd1
měřit	měřit	k5eAaImF
elektrický	elektrický	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
malého	malý	k2eAgNnSc2d1
množství	množství	k1gNnSc2
motorických	motorický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
až	až	k6eAd1
jednotlivých	jednotlivý	k2eAgFnPc2d1
motorických	motorický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Využitelné	využitelný	k2eAgNnSc1d1
frekvenční	frekvenční	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
sahá	sahat	k5eAaImIp3nS
až	až	k9
k	k	k7c3
10	#num#	k4
kHz	khz	kA
díky	díky	k7c3
malému	malý	k2eAgInSc3d1
objemu	objem	k1gInSc3
tkáně	tkáň	k1gFnSc2
mezi	mezi	k7c7
elektrodou	elektroda	k1gFnSc7
a	a	k8xC
zdrojem	zdroj	k1gInSc7
signálu	signál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špičkové	Špičkové	k2eAgNnSc2d1
napětí	napětí	k1gNnSc2
dosahuje	dosahovat	k5eAaImIp3nS
řádově	řádově	k6eAd1
stovek	stovka	k1gFnPc2
μ	μ	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
akční	akční	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
svalu	sval	k1gInSc2
(	(	kIx(
<g/>
CMAP	CMAP	kA
<g/>
)	)	kIx)
</s>
<s>
Snímání	snímání	k1gNnSc1
a	a	k8xC
záznam	záznam	k1gInSc1
sumárních	sumární	k2eAgInPc2d1
akčních	akční	k2eAgInPc2d1
potenciálů	potenciál	k1gInPc2
svalu	sval	k1gInSc2
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
za	za	k7c7
účelem	účel	k1gInSc7
zjištění	zjištění	k1gNnSc1
funkcionality	funkcionalita	k1gFnSc2
svalu	sval	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
povrchových	povrchový	k2eAgFnPc2d1
nebo	nebo	k8xC
podpovrchových	podpovrchový	k2eAgFnPc2d1
elektrod	elektroda	k1gFnPc2
ke	k	k7c3
stimulaci	stimulace	k1gFnSc3
periferního	periferní	k2eAgInSc2d1
nervu	nerv	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
pak	pak	k6eAd1
vyvolá	vyvolat	k5eAaPmIp3nS
samotný	samotný	k2eAgInSc1d1
záškub	záškub	k1gInSc1
svalu	sval	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
měřen	měřit	k5eAaImNgMnS
povrchovou	povrchový	k2eAgFnSc7d1
elektrodou	elektroda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podpovrchová	podpovrchový	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
ke	k	k7c3
snímání	snímání	k1gNnSc3
ve	v	k7c6
svalu	sval	k1gInSc6
není	být	k5eNaImIp3nS
vhodná	vhodný	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
silné	silný	k2eAgFnSc3d1
kontrakci	kontrakce	k1gFnSc3
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
projeví	projevit	k5eAaPmIp3nS
vznikem	vznik	k1gInSc7
artefaktů	artefakt	k1gInPc2
v	v	k7c6
signálu	signál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stimulace	stimulace	k1gFnSc1
nervu	nerv	k1gInSc2
je	být	k5eAaImIp3nS
realizována	realizovat	k5eAaBmNgFnS
elektrickými	elektrický	k2eAgInPc7d1
pulzy	pulz	k1gInPc7
o	o	k7c6
frekvenci	frekvence	k1gFnSc6
3-5	3-5	k4
Hz	Hz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhodnocuje	vyhodnocovat	k5eAaImIp3nS
se	se	k3xPyFc4
amplituda	amplituda	k1gFnSc1
a	a	k8xC
odezva	odezva	k1gFnSc1
na	na	k7c4
budící	budící	k2eAgInSc4d1
signál	signál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nízká	nízký	k2eAgFnSc1d1
amplituda	amplituda	k1gFnSc1
(	(	kIx(
<g/>
alespoň	alespoň	k9
10	#num#	k4
<g/>
%	%	kIx~
snížení	snížení	k1gNnSc4
<g/>
)	)	kIx)
všech	všecek	k3xTgFnPc2
odezev	odezva	k1gFnPc2
značí	značit	k5eAaImIp3nS
primární	primární	k2eAgFnSc1d1
nemoc	nemoc	k1gFnSc1
svalstva	svalstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
u	u	k7c2
nervové	nervový	k2eAgFnSc2d1
poruchy	porucha	k1gFnSc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
roztroušená	roztroušený	k2eAgFnSc1d1
skleróza	skleróza	k1gFnSc1
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
zpoždění	zpoždění	k1gNnSc1
kontrakce	kontrakce	k1gFnSc2
vláken	vlákno	k1gNnPc2
při	při	k7c6
normální	normální	k2eAgFnSc6d1
amplitudě	amplituda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinou	jiný	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
CMAP	CMAP	kA
metody	metoda	k1gFnSc2
je	být	k5eAaImIp3nS
stimulace	stimulace	k1gFnSc1
senzorických	senzorický	k2eAgInPc2d1
nervů	nerv	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
zpětnovazebně	zpětnovazebně	k6eAd1
aktivujeme	aktivovat	k5eAaBmIp1nP
motoneurony	motoneuron	k1gInPc1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
lze	lze	k6eAd1
vyšetřovat	vyšetřovat	k5eAaImF
jejich	jejich	k3xOp3gFnSc4
funkčnost	funkčnost	k1gFnSc4
či	či	k8xC
přenosové	přenosový	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Akční	akční	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
motorické	motorický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
(	(	kIx(
<g/>
MUAP	MUAP	kA
<g/>
)	)	kIx)
</s>
<s>
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
k	k	k7c3
této	tento	k3xDgFnSc3
metodě	metoda	k1gFnSc3
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
podpovrchových	podpovrchový	k2eAgFnPc2d1
elektrod	elektroda	k1gFnPc2
při	při	k7c6
přirozené	přirozený	k2eAgFnSc6d1
kontrakci	kontrakce	k1gFnSc6
svalu	sval	k1gInSc2
–	–	k?
lehkým	lehký	k2eAgNnSc7d1
zatížením	zatížení	k1gNnSc7
svalu	sval	k1gInSc2
až	až	k6eAd1
do	do	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
zapojení	zapojení	k1gNnSc2
na	na	k7c4
maximálně	maximálně	k6eAd1
10	#num#	k4
<g/>
%	%	kIx~
MVC	MVC	kA
<g/>
,	,	kIx,
tím	ten	k3xDgInSc7
se	se	k3xPyFc4
aktivuje	aktivovat	k5eAaBmIp3nS
malý	malý	k2eAgInSc1d1
počet	počet	k1gInSc1
motorických	motorický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektroda	elektroda	k1gFnSc1
snímá	snímat	k5eAaImIp3nS
výsledný	výsledný	k2eAgInSc1d1
signál	signál	k1gInSc1
složený	složený	k2eAgInSc1d1
z	z	k7c2
příspěvků	příspěvek	k1gInPc2
okolních	okolní	k2eAgFnPc2d1
motorických	motorický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
signál	signál	k1gInSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
filtruje	filtrovat	k5eAaImIp3nS
pásmovou	pásmový	k2eAgFnSc7d1
propustí	propust	k1gFnSc7
1.2	1.2	k4
–	–	k?
2.5	2.5	k4
kHz	khz	kA
a	a	k8xC
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c4
hroty	hrot	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
lineární	lineární	k2eAgFnSc1d1
kombinace	kombinace	k1gFnSc1
nejlépe	dobře	k6eAd3
napodobí	napodobit	k5eAaPmIp3nS
původní	původní	k2eAgInSc1d1
složený	složený	k2eAgInSc1d1
signál	signál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
lze	lze	k6eAd1
sledovat	sledovat	k5eAaImF
průběh	průběh	k1gInSc4
aktivace	aktivace	k1gFnSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
motorických	motorický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
lze	lze	k6eAd1
například	například	k6eAd1
odhalit	odhalit	k5eAaPmF
dystrofii	dystrofie	k1gFnSc4
svalstva	svalstvo	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c4
dané	daný	k2eAgNnSc4d1
zatížení	zatížení	k1gNnSc4
aktivováno	aktivován	k2eAgNnSc4d1
nepřiměřené	přiměřený	k2eNgNnSc4d1
množství	množství	k1gNnSc4
motorických	motorický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
menší	malý	k2eAgInSc1d2
počet	počet	k1gInSc1
aktivovaných	aktivovaný	k2eAgFnPc2d1
motorických	motorický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
značí	značit	k5eAaImIp3nS
chybnou	chybný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
motoneuronů	motoneuron	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
metodě	metoda	k1gFnSc3
CMAP	CMAP	kA
získáváme	získávat	k5eAaImIp1nP
i	i	k9
informaci	informace	k1gFnSc4
o	o	k7c6
opakovacích	opakovací	k2eAgFnPc6d1
frekvencích	frekvence	k1gFnPc6
aktivace	aktivace	k1gFnSc2
motorických	motorický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
čehož	což	k3yQnSc2,k3yRnSc2
lze	lze	k6eAd1
také	také	k9
využít	využít	k5eAaPmF
při	při	k7c6
diagnostice	diagnostika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
EMG	EMG	kA
jednoho	jeden	k4xCgNnSc2
svalového	svalový	k2eAgNnSc2d1
vlákna	vlákno	k1gNnSc2
(	(	kIx(
<g/>
SFEMG	SFEMG	kA
<g/>
)	)	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
zaznamenávat	zaznamenávat	k5eAaImF
akční	akční	k2eAgInPc4d1
potenciály	potenciál	k1gInPc4
jednoho	jeden	k4xCgNnSc2
svalového	svalový	k2eAgNnSc2d1
vlákna	vlákno	k1gNnSc2
a	a	k8xC
měření	měření	k1gNnSc2
hustoty	hustota	k1gFnSc2
svalových	svalový	k2eAgFnPc2d1
vláken	vlákna	k1gFnPc2
a	a	k8xC
neuromuskulárního	uromuskulární	k2eNgInSc2d1
jitteru	jitter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjištěním	zjištění	k1gNnSc7
počtu	počet	k1gInSc2
snímaných	snímaný	k2eAgInPc2d1
akčních	akční	k2eAgInPc2d1
potenciálů	potenciál	k1gInPc2
<g/>
,	,	kIx,
zaznamenávaných	zaznamenávaný	k2eAgFnPc2d1
současně	současně	k6eAd1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
amplituda	amplituda	k1gFnSc1
překračuje	překračovat	k5eAaImIp3nS
200	#num#	k4
μ	μ	k?
<g/>
,	,	kIx,
zjistíme	zjistit	k5eAaPmIp1nP
zároveň	zároveň	k6eAd1
počet	počet	k1gInSc4
aktivních	aktivní	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
elektrody	elektroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měřením	měření	k1gNnSc7
v	v	k7c6
různých	různý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
svalu	sval	k1gInSc2
můžeme	moct	k5eAaImIp1nP
zjistit	zjistit	k5eAaPmF
průměrnou	průměrný	k2eAgFnSc4d1
hustotu	hustota	k1gFnSc4
jeho	jeho	k3xOp3gNnPc2
vláken	vlákno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
u	u	k7c2
jednotlivých	jednotlivý	k2eAgInPc2d1
svalů	sval	k1gInPc2
a	a	k8xC
zvyšuje	zvyšovat	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
stáří	stáří	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
u	u	k7c2
některých	některý	k3yIgFnPc2
poruch	porucha	k1gFnPc2
motorických	motorický	k2eAgInPc2d1
nervů	nerv	k1gInPc2
a	a	k8xC
nervosvalové	nervosvalový	k2eAgFnSc2d1
ploténky	ploténka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akční	akční	k2eAgInPc1d1
potenciály	potenciál	k1gInPc1
snímané	snímaný	k2eAgInPc1d1
SFEMG	SFEMG	kA
svalu	sval	k1gInSc6
mají	mít	k5eAaImIp3nP
různé	různý	k2eAgFnPc4d1
vzájemné	vzájemný	k2eAgFnPc4d1
zpoždění	zpoždění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odchylky	odchylka	k1gFnSc2
od	od	k7c2
konstantní	konstantní	k2eAgFnSc2d1
frekvence	frekvence	k1gFnSc2
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
neuromuskulární	uromuskulární	k2eNgInSc4d1
jitter	jitter	k1gInSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
způsobeny	způsobit	k5eAaPmNgInP
chemickým	chemický	k2eAgInSc7d1
přenosem	přenos	k1gInSc7
na	na	k7c6
nervosvalové	nervosvalový	k2eAgFnSc6d1
ploténce	ploténka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Normální	normální	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
10-50	10-50	k4
μ	μ	k1gFnPc2
a	a	k8xC
podle	podle	k7c2
odchylek	odchylka	k1gFnPc2
lze	lze	k6eAd1
rozpoznat	rozpoznat	k5eAaPmF
myopatie	myopatie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Aplikace	aplikace	k1gFnSc1
</s>
<s>
EMG	EMG	kA
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
diagnostiku	diagnostika	k1gFnSc4
neurosvalových	urosvalových	k2eNgNnSc2d1
onemocnění	onemocnění	k1gNnSc2
a	a	k8xC
poškození	poškození	k1gNnSc2
<g/>
,	,	kIx,
kineziologii	kineziologie	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
poruchy	porucha	k1gFnPc1
kontroly	kontrola	k1gFnSc2
pohybového	pohybový	k2eAgInSc2d1
aparátu	aparát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMG	EMG	kA
signálů	signál	k1gInPc2
lze	lze	k6eAd1
taktéž	taktéž	k?
využít	využít	k5eAaPmF
pro	pro	k7c4
řízení	řízení	k1gNnSc4
prostetických	prostetický	k2eAgFnPc2d1
náhrad	náhrada	k1gFnPc2
jako	jako	k8xC,k8xS
například	například	k6eAd1
ruky	ruka	k1gFnSc2
či	či	k8xC
nohy	noha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
EMG	EMG	kA
je	být	k5eAaImIp3nS
taktéž	taktéž	k?
používáno	používán	k2eAgNnSc4d1
pro	pro	k7c4
ovládání	ovládání	k1gNnSc4
počítače	počítač	k1gInSc2
a	a	k8xC
jiných	jiný	k2eAgNnPc2d1
elektronických	elektronický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
(	(	kIx(
<g/>
PDA	PDA	kA
nebo	nebo	k8xC
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možné	možný	k2eAgNnSc1d1
využití	využití	k1gNnSc1
sahá	sahat	k5eAaImIp3nS
k	k	k7c3
řízení	řízení	k1gNnSc3
mobilních	mobilní	k2eAgInPc2d1
robotů	robot	k1gInPc2
či	či	k8xC
elektrického	elektrický	k2eAgNnSc2d1
křesla	křeslo	k1gNnSc2
což	což	k3yQnSc1,k3yRnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
velmi	velmi	k6eAd1
nápomocné	nápomocný	k2eAgNnSc1d1
pro	pro	k7c4
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
nemohou	moct	k5eNaImIp3nP
ovládat	ovládat	k5eAaImF
křeslo	křeslo	k1gNnSc1
řízené	řízený	k2eAgNnSc1d1
joystikem	joystik	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povrchové	povrchový	k2eAgFnSc2d1
EMG	EMG	kA
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
využít	využít	k5eAaPmF
například	například	k6eAd1
pro	pro	k7c4
ovládání	ovládání	k1gNnSc4
her	hra	k1gFnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
EEG	EEG	kA
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
spánkových	spánkový	k2eAgInPc2d1
pochodů	pochod	k1gInPc2
a	a	k8xC
nervových	nervový	k2eAgInPc2d1
záchvatů	záchvat	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nízkonákladové	Nízkonákladový	k2eAgInPc1d1
EMG	EMG	kA
zařízení	zařízení	k1gNnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
v	v	k7c4
Advancer	Advancer	k1gInSc4
Technologie	technologie	k1gFnSc2
vyvinuli	vyvinout	k5eAaPmAgMnP
jednoduchý	jednoduchý	k2eAgInSc4d1
plošný	plošný	k2eAgInSc4d1
spoj	spoj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
využit	využít	k5eAaPmNgInS
pro	pro	k7c4
konverzi	konverze	k1gFnSc4
měřených	měřený	k2eAgInPc2d1
signálů	signál	k1gInPc2
svalů	sval	k1gInPc2
do	do	k7c2
podoby	podoba	k1gFnSc2
vyhlazeného	vyhlazený	k2eAgInSc2d1
výstupního	výstupní	k2eAgInSc2d1
signálu	signál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Terminologie	terminologie	k1gFnSc1
</s>
<s>
Alfa	alfa	k1gFnSc1
motoneuron	motoneuron	k1gMnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Alpha-motoneuron	Alpha-motoneuron	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
neuron	neuron	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
tělo	tělo	k1gNnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
předním	přední	k2eAgInSc6d1
rohu	roh	k1gInSc6
míšním	míšní	k2eAgInSc6d1
a	a	k8xC
jehož	jehož	k3xOyRp3gNnPc2
axon	axono	k1gNnPc2
relativně	relativně	k6eAd1
velkého	velký	k2eAgInSc2d1
průměru	průměr	k1gInSc2
inervuje	inervovat	k5eAaImIp3nS
skupinu	skupina	k1gFnSc4
svalových	svalový	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Motorická	motorický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Motor	motor	k1gInSc1
unit	unit	k1gInSc1
–	–	k?
MU	MU	kA
<g/>
)	)	kIx)
–	–	k?
termín	termín	k1gInSc1
užívaný	užívaný	k2eAgInSc1d1
k	k	k7c3
popisu	popis	k1gInSc3
nejmenší	malý	k2eAgFnSc2d3
možné	možný	k2eAgFnSc2d1
řízené	řízený	k2eAgFnSc2d1
svalové	svalový	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
;	;	kIx,
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
alfa-motoneuronu	alfa-motoneuron	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc2
neuromuskulární	uromuskulární	k2eNgFnSc2d1
junkce	junkce	k1gFnSc2
a	a	k8xC
svalových	svalový	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
inervuje	inervovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Akční	akční	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
svalového	svalový	k2eAgNnSc2d1
vlákna	vlákno	k1gNnSc2
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Muscle	Muscle	k1gFnSc1
fiber	fiber	k1gInSc4
action	action	k1gInSc1
potential	potential	k1gInSc1
–	–	k?
neboli	neboli	k8xC
en	en	k?
<g/>
:	:	kIx,
<g/>
Motor	motor	k1gInSc1
action	action	k1gInSc1
potential	potential	k1gInSc1
–	–	k?
MAP	mapa	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
detekovaný	detekovaný	k2eAgInSc4d1
tvar	tvar	k1gInSc4
vlny	vlna	k1gFnSc2
<g/>
,	,	kIx,
vznikající	vznikající	k2eAgFnSc2d1
v	v	k7c6
důsledku	důsledek	k1gInSc6
depolarizační	depolarizační	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
šíří	šířit	k5eAaImIp3nS
svalovým	svalový	k2eAgNnSc7d1
vláknem	vlákno	k1gNnSc7
oběma	dva	k4xCgInPc7
směry	směr	k1gInPc7
od	od	k7c2
nervosvalové	nervosvalový	k2eAgFnSc2d1
ploténky	ploténka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Akční	akční	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
motorické	motorický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Motor	motor	k1gInSc1
Unit	Unita	k1gFnPc2
Action	Action	k1gInSc1
Potential	Potential	k1gMnSc1
–	–	k?
MUAP	MUAP	kA
<g/>
)	)	kIx)
–	–	k?
detekovaný	detekovaný	k2eAgInSc4d1
tvar	tvar	k1gInSc4
vlny	vlna	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
časoprostorové	časoprostorový	k2eAgFnSc2d1
sumace	sumace	k1gFnSc2
akčních	akční	k2eAgInPc2d1
potenciálů	potenciál	k1gInPc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
svalových	svalový	k2eAgFnPc2d1
vláken	vlákna	k1gFnPc2
jedné	jeden	k4xCgFnSc2
motorické	motorický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
snímaný	snímaný	k2eAgInSc1d1
elektrodou	elektroda	k1gFnSc7
nebo	nebo	k8xC
párem	pár	k1gInSc7
elektrod	elektroda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Řada	řada	k1gFnSc1
akčních	akční	k2eAgInPc2d1
potenciálů	potenciál	k1gInPc2
motorické	motorický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Motor	motor	k1gInSc1
Unit	Unita	k1gFnPc2
Action	Action	k1gInSc1
Potential	Potential	k1gMnSc1
Train	Train	k1gMnSc1
–	–	k?
MUAPT	MUAPT	kA
<g/>
)	)	kIx)
–	–	k?
opakovaná	opakovaný	k2eAgFnSc1d1
sekvence	sekvence	k1gFnSc1
MUAPů	MUAP	k1gMnPc2
z	z	k7c2
dané	daný	k2eAgFnSc2d1
motorické	motorický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
Interval	interval	k1gInSc1
mezi	mezi	k7c7
pulzy	pulz	k1gInPc7
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Interpulse	Interpuls	k1gMnSc2
Interval	interval	k1gInSc1
–	–	k?
IPI	IPI	kA
<g/>
)	)	kIx)
–	–	k?
časový	časový	k2eAgInSc4d1
interval	interval	k1gInSc4
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
po	po	k7c6
sobě	se	k3xPyFc3
následujícími	následující	k2eAgInPc7d1
výboji	výboj	k1gInPc7
motorické	motorický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
;	;	kIx,
má	mít	k5eAaImIp3nS
semináhodnou	semináhodný	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
</s>
<s>
Frekvence	frekvence	k1gFnSc1
výbojů	výboj	k1gInPc2
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Instantaneous	Instantaneous	k1gInSc1
Firing	Firing	k1gInSc1
Rate	Rate	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
parametr	parametr	k1gInSc1
<g/>
,	,	kIx,
daný	daný	k2eAgInSc1d1
reciprokou	reciproký	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
mezipulsového	mezipulsový	k2eAgInSc2d1
intervalu	interval	k1gInSc2
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
výbojů	výboj	k1gInPc2
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Average	Average	k1gFnSc1
Firing	Firing	k1gInSc1
Rate	Rate	k1gFnSc1
–	–	k?
<g/>
)	)	kIx)
–	–	k?
průměrná	průměrný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
výše	výše	k1gFnSc2,k1gFnSc2wB
uvedené	uvedený	k2eAgFnSc2d1
veličiny	veličina	k1gFnSc2
</s>
<s>
Synchronizace	synchronizace	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Synchronization	Synchronization	k1gInSc1
–	–	k?
<g/>
)	)	kIx)
–	–	k?
tendence	tendence	k1gFnSc2
motorické	motorický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
depolarizovat	depolarizovat	k5eAaImF
se	se	k3xPyFc4
v	v	k7c6
okamžiku	okamžik	k1gInSc6
(	(	kIx(
<g/>
anebo	anebo	k8xC
blízko	blízko	k7c2
okamžiku	okamžik	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
depolarizaci	depolarizace	k1gFnSc3
jiné	jiný	k2eAgFnPc4d1
motorické	motorický	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Elektromyografický	Elektromyografický	k2eAgInSc1d1
signál	signál	k1gInSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Electromyographic	Electromyographic	k1gMnSc1
Signal	Signal	k1gFnSc2
–	–	k?
EMG	EMG	kA
<g/>
)	)	kIx)
–	–	k?
celkový	celkový	k2eAgInSc1d1
signál	signál	k1gInSc1
detekovaný	detekovaný	k2eAgInSc1d1
elektrodou	elektroda	k1gFnSc7
<g/>
;	;	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
sumační	sumační	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
všech	všecek	k3xTgFnPc2
MUAPT	MUAPT	kA
ze	z	k7c2
všech	všecek	k3xTgFnPc2
motorických	motorický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
dosahu	dosah	k1gInSc6
snímací	snímací	k2eAgFnSc2d1
elektrody	elektroda	k1gFnSc2
</s>
<s>
Myoelektrický	Myoelektrický	k2eAgInSc1d1
signál	signál	k1gInSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Myoelectric	Myoelectric	k1gMnSc1
signal	signat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
)	)	kIx)
–	–	k?
synonymum	synonymum	k1gNnSc4
pro	pro	k7c4
elektromyografický	elektromyografický	k2eAgInSc4d1
signál	signál	k1gInSc4
</s>
<s>
Amplituda	amplituda	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Amplitude	Amplitud	k1gInSc5
<g/>
)	)	kIx)
–	–	k?
velikost	velikost	k1gFnSc1
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
udávaná	udávaný	k2eAgFnSc1d1
v	v	k7c6
mikrovoltech	mikrovolt	k1gInPc6
nebo	nebo	k8xC
v	v	k7c6
milivoltech	milivolt	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
trvání	trvání	k1gNnSc2
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Time	Time	k1gInSc4
Duration	Duration	k1gInSc4
<g/>
)	)	kIx)
–	–	k?
časový	časový	k2eAgInSc4d1
interval	interval	k1gInSc4
<g/>
,	,	kIx,
během	během	k7c2
kterého	který	k3yQgInSc2,k3yIgInSc2,k3yRgInSc2
detekujeme	detekovat	k5eAaImIp1nP
danou	daný	k2eAgFnSc4d1
vlnu	vlna	k1gFnSc4
</s>
<s>
Fáze	fáze	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Phase	Phase	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
elektromyografii	elektromyografie	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
výchylku	výchylka	k1gFnSc4
amplitudy	amplituda	k1gFnSc2
v	v	k7c6
negativním	negativní	k2eAgInSc6d1
či	či	k8xC
positivním	positivní	k2eAgInSc6d1
směru	směr	k1gInSc6
</s>
<s>
Tvar	tvar	k1gInSc1
vlny	vlna	k1gFnSc2
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Shape	Shap	k1gInSc5
<g/>
)	)	kIx)
–	–	k?
tvar	tvar	k1gInSc4
vlny	vlna	k1gFnSc2
(	(	kIx(
<g/>
nezávislý	závislý	k2eNgMnSc1d1
na	na	k7c6
změně	změna	k1gFnSc6
časového	časový	k2eAgNnSc2d1
nebo	nebo	k8xC
amplitudového	amplitudový	k2eAgNnSc2d1
měřítka	měřítko	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Tvar	tvar	k1gInSc1
vlny	vlna	k1gFnSc2
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Waveform	Waveform	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
tvar	tvar	k1gInSc4
vlny	vlna	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
daného	daný	k2eAgNnSc2d1
časového	časový	k2eAgNnSc2d1
a	a	k8xC
amplitudového	amplitudový	k2eAgNnSc2d1
měřítka	měřítko	k1gNnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
takový	takový	k3xDgInSc4
<g/>
,	,	kIx,
jaký	jaký	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
jej	on	k3xPp3gMnSc4
vnímá	vnímat	k5eAaImIp3nS
oko	oko	k1gNnSc1
myografisty	myografista	k1gMnSc2
</s>
<s>
Dekompozice	dekompozice	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Decomposition	Decomposition	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
proces	proces	k1gInSc1
<g/>
,	,	kIx,
během	během	k7c2
kterého	který	k3yRgInSc2,k3yIgInSc2,k3yQgInSc2
jsou	být	k5eAaImIp3nP
extrahovány	extrahován	k2eAgInPc1d1
jednotlivé	jednotlivý	k2eAgInPc1d1
MUAP	MUAP	kA
z	z	k7c2
elektromyografického	elektromyografický	k2eAgInSc2d1
signálu	signál	k1gInSc2
</s>
<s>
Elektroda	elektroda	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Electrode	Electrod	k1gInSc5
<g/>
)	)	kIx)
–	–	k?
součástka	součástka	k1gFnSc1
<g/>
,	,	kIx,
skrze	skrze	k?
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
elektrický	elektrický	k2eAgInSc1d1
proud	proud	k1gInSc1
vstupuje	vstupovat	k5eAaImIp3nS
(	(	kIx(
<g/>
či	či	k8xC
vystupuje	vystupovat	k5eAaImIp3nS
<g/>
)	)	kIx)
do	do	k7c2
elektrolytu	elektrolyt	k1gInSc2
<g/>
,	,	kIx,
plynu	plyn	k1gInSc2
či	či	k8xC
vakua	vakuum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Detekční	detekční	k2eAgInSc4d1
povrch	povrch	k1gInSc4
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Detection	Detection	k1gInSc1
Surface	Surface	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
ta	ten	k3xDgFnSc1
část	část	k1gFnSc1
snímací	snímací	k2eAgFnSc2d1
elektrody	elektroda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
přímém	přímý	k2eAgInSc6d1
kontaktu	kontakt	k1gInSc6
s	s	k7c7
prostředím	prostředí	k1gNnSc7
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
jsou	být	k5eAaImIp3nP
snímány	snímán	k2eAgInPc1d1
signály	signál	k1gInPc1
</s>
<s>
Unipolární	unipolární	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Unipolar	Unipolar	k1gMnSc1
Electrode	Electrod	k1gInSc5
<g/>
)	)	kIx)
–	–	k?
elektroda	elektroda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
jedním	jeden	k4xCgInSc7
detekčním	detekční	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
</s>
<s>
Bipolární	bipolární	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Bipolar	Bipolar	k1gMnSc1
Electrode	Electrod	k1gInSc5
<g/>
)	)	kIx)
–	–	k?
elektroda	elektroda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
dvěma	dva	k4xCgInPc7
detekčními	detekční	k2eAgInPc7d1
povrchy	povrch	k1gInPc7
</s>
<s>
Koncentrická	koncentrický	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Concentric	Concentric	k1gMnSc1
Electrode	Electrod	k1gInSc5
<g/>
)	)	kIx)
–	–	k?
aktivní	aktivní	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
elektroda	elektroda	k1gFnSc1
<g/>
,	,	kIx,
umístěná	umístěný	k2eAgFnSc1d1
ve	v	k7c6
středu	střed	k1gInSc6
kovového	kovový	k2eAgNnSc2d1
stínění	stínění	k1gNnSc2
-	-	kIx~
typicky	typicky	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
elektrodu	elektroda	k1gFnSc4
<g/>
,	,	kIx,
izolovaně	izolovaně	k6eAd1
umístěnou	umístěný	k2eAgFnSc4d1
v	v	k7c6
otvoru	otvor	k1gInSc6
duté	dutý	k2eAgFnSc2d1
jehly	jehla	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
tvoří	tvořit	k5eAaImIp3nS
referenční	referenční	k2eAgFnSc4d1
elektrodu	elektroda	k1gFnSc4
</s>
<s>
Detekce	detekce	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Detection	Detection	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
proces	proces	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
je	být	k5eAaImIp3nS
snímán	snímán	k2eAgInSc1d1
signál	signál	k1gInSc1
pomocí	pomocí	k7c2
elektrody	elektroda	k1gFnSc2
</s>
<s>
Záznam	záznam	k1gInSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Recording	Recording	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
proces	proces	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
je	být	k5eAaImIp3nS
zaznamenáván	zaznamenávat	k5eAaImNgInS
časový	časový	k2eAgInSc1d1
průběh	průběh	k1gInSc1
detekovaného	detekovaný	k2eAgInSc2d1
signálu	signál	k1gInSc2
na	na	k7c4
vhodné	vhodný	k2eAgNnSc4d1
médium	médium	k1gNnSc4
(	(	kIx(
<g/>
papír	papír	k1gInSc1
<g/>
,	,	kIx,
film	film	k1gInSc1
<g/>
,	,	kIx,
paměť	paměť	k1gFnSc1
počítače	počítač	k1gInSc2
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
Isometrická	Isometrický	k2eAgFnSc1d1
kontrakce	kontrakce	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Isometric	Isometrice	k1gFnPc2
Contraction	Contraction	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
svalový	svalový	k2eAgInSc1d1
stah	stah	k1gInSc1
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
délka	délka	k1gFnSc1
staženého	stažený	k2eAgInSc2d1
svalu	sval	k1gInSc2
nemění	měnit	k5eNaImIp3nS
</s>
<s>
Anisometrická	Anisometrický	k2eAgFnSc1d1
kontrakce	kontrakce	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Anisometric	Anisometrice	k1gFnPc2
Contraction	Contraction	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
stah	stah	k1gInSc1
svalu	sval	k1gInSc2
<g/>
,	,	kIx,
během	během	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
délka	délka	k1gFnSc1
staženého	stažený	k2eAgInSc2d1
svalu	sval	k1gInSc2
</s>
<s>
Balistická	balistický	k2eAgFnSc1d1
kontrakce	kontrakce	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Ballistic	Ballistice	k1gFnPc2
Contraction	Contraction	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
stah	stah	k1gInSc1
svalů	sval	k1gInPc2
<g/>
,	,	kIx,
vykonaný	vykonaný	k2eAgInSc4d1
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
fyziologicky	fyziologicky	k6eAd1
možnou	možný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
vědomá	vědomý	k2eAgFnSc1d1
kontrakce	kontrakce	k1gFnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Maximal	Maximal	k1gMnSc1
voluntary	voluntara	k1gFnSc2
contraction	contraction	k1gInSc1
–	–	k?
MVC	MVC	kA
<g/>
)	)	kIx)
–	–	k?
maximální	maximální	k2eAgInSc4d1
stah	stah	k1gInSc4
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
je	být	k5eAaImIp3nS
jedinec	jedinec	k1gMnSc1
schopen	schopen	k2eAgMnSc1d1
docílit	docílit	k5eAaPmF
</s>
<s>
Agonista	Agonista	k1gMnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Agonist	Agonist	k1gMnSc1
Muscle	Muscle	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
sval	sval	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
začíná	začínat	k5eAaImIp3nS
kontrakci	kontrakce	k1gFnSc4
</s>
<s>
Antagonista	antagonista	k1gMnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Antagonist	Antagonist	k1gMnSc1
Muscle	Muscle	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
sval	sval	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
působí	působit	k5eAaImIp3nS
proti	proti	k7c3
aginostovi	aginosta	k1gMnSc3
</s>
<s>
Synergista	synergista	k1gMnSc1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
Synergist	Synergist	k1gMnSc1
Muscle	Muscle	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
sval	sval	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
působí	působit	k5eAaImIp3nS
stejným	stejný	k2eAgInSc7d1
směrem	směr	k1gInSc7
jako	jako	k8xS,k8xC
agonista	agonista	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
PENHAKER	PENHAKER	kA
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
;	;	kIx,
IMRAMOVSKÝ	IMRAMOVSKÝ	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
;	;	kIx,
TIEFENBACH	TIEFENBACH	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
KOBZA	kobza	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lékařské	lékařský	k2eAgInPc4d1
diagnostické	diagnostický	k2eAgInPc4d1
přístroje	přístroj	k1gInPc4
-	-	kIx~
učební	učební	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
VŠB	VŠB	kA
-	-	kIx~
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
248	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
751	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WEBSTER	WEBSTER	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
G.	G.	kA
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Medical	Medical	k1gMnSc1
Devices	Devices	k1gMnSc1
and	and	k?
Instrumentation	Instrumentation	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Wiley	Wiley	k1gInPc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
1732	#num#	k4
<g/>
-	-	kIx~
<g/>
877	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
EMG	EMG	kA
pro	pro	k7c4
ovládání	ovládání	k1gNnSc4
her	hra	k1gFnPc2
<g/>
↑	↑	k?
EEG-EMG	EEG-EMG	k1gMnSc1
systém	systém	k1gInSc4
měření	měření	k1gNnSc2
spánkových	spánkový	k2eAgInPc2d1
pochodů	pochod	k1gInPc2
a	a	k8xC
nervových	nervový	k2eAgInPc2d1
záchvatů	záchvat	k1gInPc2
myší	myš	k1gFnPc2
<g/>
.	.	kIx.
www.pinnaclet.com	www.pinnaclet.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Nízkonákladový	Nízkonákladový	k2eAgInSc1d1
systém	systém	k1gInSc1
měření	měření	k1gNnSc2
EMG	EMG	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Elektroencefalografie	Elektroencefalografie	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Elektromyografie	elektromyografie	k1gFnSc1
</s>
<s>
BASMAJIAN	BASMAJIAN	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
V	V	kA
<g/>
;	;	kIx,
DE	DE	k?
LUCA	LUCA	kA
<g/>
,	,	kIx,
Carlo	Carlo	k1gNnSc1
J.	J.	kA
Muscles	Muscles	k1gInSc4
Alive	Aliev	k1gFnSc2
<g/>
:	:	kIx,
Their	Their	k1gInSc1
Functions	Functions	k1gInSc1
Revealed	Revealed	k1gInSc4
by	by	kYmCp3nS
their	their	k1gMnSc1
Electromyography	Electromyographa	k1gFnSc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baltimore	Baltimore	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
Williams	Williams	k1gInSc1
&	&	k?
Wilkins	Wilkins	k1gInSc1
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
561	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
683	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
414	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KELLER	Keller	k1gMnSc1
<g/>
,	,	kIx,
Otakar	Otakar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecná	obecný	k2eAgFnSc1d1
elektromyografie	elektromyografie	k1gFnSc1
<g/>
:	:	kIx,
Fyziologické	fyziologický	k2eAgInPc1d1
základy	základ	k1gInPc1
a	a	k8xC
elektrofyziologická	elektrofyziologický	k2eAgNnPc1d1
vyšetření	vyšetření	k1gNnPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7254	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KADAŇKA	KADAŇKA	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
;	;	kIx,
BEDNAŘÍK	Bednařík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
VOHÁŇKA	VOHÁŇKA	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praktická	praktický	k2eAgFnSc1d1
elektromyografie	elektromyografie	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7013	#num#	k4
<g/>
-	-	kIx~
<g/>
181	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
119885	#num#	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
12723	#num#	k4
</s>
