<s>
Původní	původní	k2eAgFnSc1d1	původní
neoficiální	neoficiální	k2eAgFnSc1d1	neoficiální
přezdívka	přezdívka	k1gFnSc1	přezdívka
mateřského	mateřský	k2eAgNnSc2d1	mateřské
tělesa	těleso	k1gNnSc2	těleso
Eris	Eris	k1gFnSc1	Eris
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
Xena	Xena	k1gMnSc1	Xena
<g/>
"	"	kIx"	"
po	po	k7c6	po
známé	známý	k2eAgFnSc6d1	známá
seriálové	seriálový	k2eAgFnSc6d1	seriálová
bojovnici	bojovnice	k1gFnSc6	bojovnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
nové	nový	k2eAgNnSc1d1	nové
jméno	jméno	k1gNnSc1	jméno
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
současně	současně	k6eAd1	současně
poctou	pocta	k1gFnSc7	pocta
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
herečku	herečka	k1gFnSc4	herečka
Lucy	Luca	k1gFnSc2	Luca
Lawlessovou	Lawlessová	k1gFnSc7	Lawlessová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
její	její	k3xOp3gFnSc4	její
postavu	postava	k1gFnSc4	postava
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
<g/>
.	.	kIx.	.
</s>
