<p>
<s>
Ódin	Ódin	k1gNnSc1	Ódin
(	(	kIx(	(
<g/>
staroseversky	staroseversky	k6eAd1	staroseversky
Óð	Óð	k1gMnSc2	Óð
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
hlavního	hlavní	k2eAgMnSc2d1	hlavní
boha	bůh	k1gMnSc2	bůh
severského	severský	k2eAgInSc2d1	severský
panteonu	panteon	k1gInSc2	panteon
náležící	náležící	k2eAgFnSc1d1	náležící
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
bohů	bůh	k1gMnPc2	bůh
(	(	kIx(	(
<g/>
božského	božský	k2eAgInSc2d1	božský
rodu	rod	k1gInSc2	rod
<g/>
)	)	kIx)	)
zvaní	zvaní	k1gNnSc1	zvaní
Ásové	Ásová	k1gFnSc2	Ásová
(	(	kIx(	(
<g/>
staroseversky	staroseversky	k6eAd1	staroseversky
Æ	Æ	k?	Æ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tento	tento	k3xDgMnSc1	tento
bůh	bůh	k1gMnSc1	bůh
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
a	a	k8xC	a
uctívaný	uctívaný	k2eAgMnSc1d1	uctívaný
patrně	patrně	k6eAd1	patrně
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
obývaném	obývaný	k2eAgNnSc6d1	obývané
Germány	Germán	k1gMnPc7	Germán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
horního	horní	k2eAgNnSc2d1	horní
Německa	Německo	k1gNnSc2	Německo
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
Wotanem	Wotan	k1gMnSc7	Wotan
a	a	k8xC	a
Wodenem	Woden	k1gMnSc7	Woden
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gNnSc1	jeho
proto-germánské	protoermánský	k2eAgNnSc1d1	proto-germánský
jméno	jméno	k1gNnSc1	jméno
znělo	znět	k5eAaImAgNnS	znět
nejspíše	nejspíše	k9	nejspíše
Wō	Wō	k1gMnPc2	Wō
či	či	k8xC	či
Wō	Wō	k1gMnPc2	Wō
<g/>
.	.	kIx.	.
</s>
<s>
Švédové	Švéd	k1gMnPc1	Švéd
mu	on	k3xPp3gMnSc3	on
říkali	říkat	k5eAaImAgMnP	říkat
Oden	Oden	k1gNnSc4	Oden
<g/>
,	,	kIx,	,
Anglosasové	Anglosas	k1gMnPc1	Anglosas
Wodan	Wodan	k1gMnSc1	Wodan
a	a	k8xC	a
Langobardi	Langobard	k1gMnPc1	Langobard
Godan	Godany	k1gInPc2	Godany
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
však	však	k9	však
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
zachovala	zachovat	k5eAaPmAgFnS	zachovat
v	v	k7c6	v
ucelené	ucelený	k2eAgFnSc6d1	ucelená
podobě	podoba	k1gFnSc6	podoba
ve	v	k7c6	v
Starší	starý	k2eAgFnSc6d2	starší
a	a	k8xC	a
Mladší	mladý	k2eAgFnSc6d2	mladší
Eddě	Edda	k1gFnSc6	Edda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
je	být	k5eAaImIp3nS	být
Ódin	Ódin	k1gMnSc1	Ódin
synem	syn	k1gMnSc7	syn
obra	obr	k1gMnSc2	obr
Bora	Borus	k1gMnSc2	Borus
a	a	k8xC	a
obryně	obryně	k1gFnSc2	obryně
Bestly	Bestly	k1gFnSc2	Bestly
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
Otcem	otec	k1gMnSc7	otec
veškerenstva	veškerenstvo	k1gNnSc2	veškerenstvo
<g/>
,	,	kIx,	,
patronem	patron	k1gInSc7	patron
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
bojovníků	bojovník	k1gMnPc2	bojovník
a	a	k8xC	a
státníků	státník	k1gMnPc2	státník
<g/>
,	,	kIx,	,
bohem	bůh	k1gMnSc7	bůh
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
války	válka	k1gFnSc2	válka
a	a	k8xC	a
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
bohem	bůh	k1gMnSc7	bůh
extáze	extáze	k1gFnSc2	extáze
<g/>
,	,	kIx,	,
extatického	extatický	k2eAgNnSc2d1	extatické
básnění	básnění	k1gNnSc2	básnění
a	a	k8xC	a
šamanských	šamanský	k2eAgNnPc2d1	šamanské
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
měnit	měnit	k5eAaImF	měnit
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
ovládat	ovládat	k5eAaImF	ovládat
mysl	mysl	k1gFnSc4	mysl
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
věštit	věštit	k5eAaImF	věštit
a	a	k8xC	a
čarovat	čarovat	k5eAaImF	čarovat
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
používá	používat	k5eAaImIp3nS	používat
znalost	znalost	k1gFnSc4	znalost
run	runa	k1gFnPc2	runa
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
v	v	k7c6	v
Havamálu	Havamál	k1gInSc6	Havamál
<g/>
,	,	kIx,	,
básni	báseň	k1gFnSc6	báseň
z	z	k7c2	z
Poetické	poetický	k2eAgFnSc2d1	poetická
Eddy	Edda	k1gFnSc2	Edda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mytologie	mytologie	k1gFnSc2	mytologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
Ásové	Ásová	k1gFnPc1	Ásová
je	on	k3xPp3gMnPc4	on
legendárně	legendárně	k6eAd1	legendárně
praktický	praktický	k2eAgInSc1d1	praktický
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
dvěma	dva	k4xCgInPc7	dva
bratry	bratr	k1gMnPc4	bratr
(	(	kIx(	(
<g/>
Vili	vít	k5eAaImAgMnP	vít
a	a	k8xC	a
Vé	Vé	k1gMnPc1	Vé
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
stvořitele	stvořitel	k1gMnPc4	stvořitel
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
dal	dát	k5eAaPmAgMnS	dát
do	do	k7c2	do
vínku	vínek	k1gInSc2	vínek
dech	dech	k1gInSc1	dech
a	a	k8xC	a
život	život	k1gInSc1	život
(	(	kIx(	(
<g/>
snad	snad	k9	snad
údajně	údajně	k6eAd1	údajně
i	i	k9	i
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obětoval	obětovat	k5eAaBmAgMnS	obětovat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
runy	runa	k1gFnPc4	runa
a	a	k8xC	a
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
levé	levý	k2eAgNnSc4d1	levé
oko	oko	k1gNnSc4	oko
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
při	při	k7c6	při
pití	pití	k1gNnSc6	pití
z	z	k7c2	z
Mímiho	Mími	k1gMnSc2	Mími
pramene	pramen	k1gInSc2	pramen
(	(	kIx(	(
<g/>
či	či	k8xC	či
Mímiho	Mími	k1gMnSc2	Mími
studny	studna	k1gFnSc2	studna
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
moudrosti	moudrost	k1gFnPc4	moudrost
věků	věk	k1gInPc2	věk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xC	jako
starší	starý	k2eAgMnSc1d2	starší
mohutný	mohutný	k2eAgMnSc1d1	mohutný
vysoký	vysoký	k2eAgMnSc1d1	vysoký
jednooký	jednooký	k2eAgMnSc1d1	jednooký
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jemu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
přinášeny	přinášen	k2eAgFnPc1d1	přinášena
lidské	lidský	k2eAgFnPc4d1	lidská
oběti	oběť	k1gFnPc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
rostliny	rostlina	k1gFnPc4	rostlina
patří	patřit	k5eAaImIp3nS	patřit
jasan	jasan	k1gInSc1	jasan
<g/>
,	,	kIx,	,
jmelí	jmelí	k1gNnSc1	jmelí
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
laskavec	laskavec	k1gInSc1	laskavec
<g/>
,	,	kIx,	,
sturač	sturač	k1gInSc1	sturač
a	a	k8xC	a
jilm	jilm	k1gInSc1	jilm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ódinovi	Ódin	k1gMnSc3	Ódin
je	být	k5eAaImIp3nS	být
připisována	připisován	k2eAgFnSc1d1	připisována
spousta	spousta	k1gFnSc1	spousta
magických	magický	k2eAgInPc2d1	magický
artefaktů	artefakt	k1gInPc2	artefakt
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mu	on	k3xPp3gMnSc3	on
oštěp	oštěp	k1gInSc4	oštěp
Gungnir	Gungnir	k1gMnSc1	Gungnir
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nikdy	nikdy	k6eAd1	nikdy
nemine	minout	k5eNaImIp3nS	minout
cíl	cíl	k1gInSc4	cíl
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
hrotu	hrot	k1gInSc6	hrot
runy	runa	k1gFnSc2	runa
zaručující	zaručující	k2eAgInSc4d1	zaručující
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
vykonal	vykonat	k5eAaPmAgMnS	vykonat
první	první	k4xOgFnSc4	první
vraždu	vražda	k1gFnSc4	vražda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spustila	spustit	k5eAaPmAgFnS	spustit
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Vany	van	k1gInPc7	van
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
kouzelný	kouzelný	k2eAgInSc4d1	kouzelný
prsten	prsten	k1gInSc4	prsten
Draupnir	Draupnira	k1gFnPc2	Draupnira
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
každou	každý	k3xTgFnSc4	každý
devátou	devátý	k4xOgFnSc4	devátý
noc	noc	k1gFnSc4	noc
objeví	objevit	k5eAaPmIp3nS	objevit
dalších	další	k2eAgInPc2d1	další
osm	osm	k4xCc4	osm
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
osminohý	osminohý	k2eAgMnSc1d1	osminohý
kůň	kůň	k1gMnSc1	kůň
Sleipnir	Sleipnir	k1gMnSc1	Sleipnir
<g/>
,	,	kIx,	,
vlci	vlk	k1gMnPc1	vlk
Geri	Geri	k1gNnPc2	Geri
a	a	k8xC	a
Freki	Freki	k1gNnPc2	Freki
<g/>
,	,	kIx,	,
krkavci	krkavec	k1gMnPc1	krkavec
Hugin	Hugin	k1gMnSc1	Hugin
a	a	k8xC	a
Munin	munin	k2eAgMnSc1d1	munin
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
každou	každý	k3xTgFnSc4	každý
noc	noc	k1gFnSc4	noc
létají	létat	k5eAaImIp3nP	létat
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
přinášejí	přinášet	k5eAaImIp3nP	přinášet
Ódinovi	Ódinův	k2eAgMnPc1d1	Ódinův
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
dnem	den	k1gInSc7	den
je	být	k5eAaImIp3nS	být
středa	středa	k1gFnSc1	středa
<g/>
;	;	kIx,	;
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
nazýval	nazývat	k5eAaImAgInS	nazývat
Wódnesdæ	Wódnesdæ	k1gFnSc3	Wódnesdæ
(	(	kIx(	(
<g/>
Ódinův	Ódinův	k2eAgInSc4d1	Ódinův
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
pochází	pocházet	k5eAaImIp3nS	pocházet
dnešní	dnešní	k2eAgMnPc4d1	dnešní
Wednesday	Wednesda	k1gMnPc4	Wednesda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
bojovníky	bojovník	k1gMnPc4	bojovník
ochotné	ochotný	k2eAgNnSc1d1	ochotné
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
při	při	k7c6	při
Ragnaröku	Ragnaröko	k1gNnSc6	Ragnaröko
a	a	k8xC	a
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
Valhalle	Valhalla	k1gFnSc6	Valhalla
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
buď	buď	k8xC	buď
padnutí	padnutí	k1gNnSc1	padnutí
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
nebo	nebo	k8xC	nebo
rituální	rituální	k2eAgNnSc1d1	rituální
poranění	poranění	k1gNnSc1	poranění
kopím	kopit	k5eAaImIp1nS	kopit
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Ódinovo	Ódinův	k2eAgNnSc1d1	Ódinovo
znamení	znamení	k1gNnSc1	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Ódin	Ódina	k1gFnPc2	Ódina
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
sídla	sídlo	k1gNnPc4	sídlo
v	v	k7c6	v
Ásgardu	Ásgard	k1gInSc6	Ásgard
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
Gladsheim	Gladsheim	k1gInSc1	Gladsheim
<g/>
,	,	kIx,	,
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
hala	hala	k1gFnSc1	hala
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předsedal	předsedat	k5eAaImAgMnS	předsedat
soudům	soud	k1gInPc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc4	jeho
trůn	trůn	k1gInSc4	trůn
Hlidskjalf	Hlidskjalf	k1gInSc1	Hlidskjalf
stojí	stát	k5eAaImIp3nS	stát
ve	v	k7c6	v
Valaskjálfu	Valaskjálf	k1gInSc6	Valaskjálf
postaveném	postavený	k2eAgInSc6d1	postavený
z	z	k7c2	z
pevného	pevný	k2eAgNnSc2d1	pevné
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
síní	síň	k1gFnPc2	síň
v	v	k7c6	v
Ásgardu	Ásgard	k1gInSc6	Ásgard
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
devíti	devět	k4xCc2	devět
světů	svět	k1gInPc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
je	být	k5eAaImIp3nS	být
Valhalla	Valhalla	k1gFnSc1	Valhalla
<g/>
,	,	kIx,	,
hala	hala	k1gFnSc1	hala
padlých	padlý	k1gMnPc2	padlý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ódin	Ódin	k1gMnSc1	Ódin
přijímá	přijímat	k5eAaImIp3nS	přijímat
duše	duše	k1gFnPc4	duše
padlých	padlý	k2eAgMnPc2d1	padlý
válečníků	válečník	k1gMnPc2	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vůdce	vůdce	k1gMnSc1	vůdce
armády	armáda	k1gFnSc2	armáda
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
válečníků	válečník	k1gMnPc2	válečník
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
noční	noční	k2eAgFnPc4d1	noční
bouře	bouř	k1gFnPc4	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Thórem	Thór	k1gMnSc7	Thór
a	a	k8xC	a
Týrem	Týr	k1gMnSc7	Týr
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejdůležitějším	důležitý	k2eAgFnPc3d3	nejdůležitější
severským	severský	k2eAgFnPc3d1	severská
(	(	kIx(	(
<g/>
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
germánským	germánský	k2eAgMnPc3d1	germánský
<g/>
)	)	kIx)	)
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svatyni	svatyně	k1gFnSc6	svatyně
v	v	k7c6	v
Uppsale	Uppsala	k1gFnSc6	Uppsala
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Švédska	Švédsko	k1gNnSc2	Švédsko
stála	stát	k5eAaImAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
socha	socha	k1gFnSc1	socha
společně	společně	k6eAd1	společně
s	s	k7c7	s
Thórem	Thór	k1gMnSc7	Thór
a	a	k8xC	a
Freyem	Frey	k1gMnSc7	Frey
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ódin	Ódin	k1gInSc1	Ódin
měl	mít	k5eAaImAgInS	mít
dle	dle	k7c2	dle
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
několik	několik	k4yIc4	několik
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
otcem	otec	k1gMnSc7	otec
mnoha	mnoho	k4c2	mnoho
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
Frigg	Frigga	k1gFnPc2	Frigga
měl	mít	k5eAaImAgMnS	mít
nejlaskavějšího	laskavý	k2eAgMnSc4d3	nejlaskavější
syna	syn	k1gMnSc4	syn
Baldra	Baldr	k1gMnSc4	Baldr
a	a	k8xC	a
slepého	slepý	k2eAgMnSc4d1	slepý
Höda	Hödus	k1gMnSc4	Hödus
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
Fjörgyn	Fjörgyna	k1gFnPc2	Fjörgyna
měl	mít	k5eAaImAgMnS	mít
dalšího	další	k2eAgMnSc4d1	další
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
mocného	mocný	k2eAgMnSc4d1	mocný
Thóra	Thór	k1gMnSc4	Thór
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ódin	Ódina	k1gFnPc2	Ódina
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Petra	Petr	k1gMnSc2	Petr
Sommera	Sommer	k1gMnSc2	Sommer
nebo	nebo	k8xC	nebo
PhDr.	PhDr.	kA	PhDr.
Anežky	Anežka	k1gFnPc4	Anežka
Merhautové	Merhautový	k2eAgFnPc4d1	Merhautová
se	se	k3xPyFc4	se
vyobrazení	vyobrazení	k1gNnPc2	vyobrazení
boha	bůh	k1gMnSc2	bůh
Ódina	Ódin	k1gMnSc2	Ódin
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Svatováclavské	svatováclavský	k2eAgFnSc6d1	Svatováclavská
přilbě	přilba	k1gFnSc6	přilba
–	–	k?	–
Anežka	Anežka	k1gFnSc1	Anežka
Merhautová	Merhautový	k2eAgFnSc1d1	Merhautová
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
analýze	analýza	k1gFnSc6	analýza
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
helmy	helma	k1gFnSc2	helma
(	(	kIx(	(
<g/>
jejiž	jejiž	k1gFnSc1	jejiž
výsledky	výsledek	k1gInPc1	výsledek
publikovala	publikovat	k5eAaBmAgFnS	publikovat
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
určila	určit	k5eAaPmAgFnS	určit
vznik	vznik	k1gInSc4	vznik
helmy	helma	k1gFnSc2	helma
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
smontováním	smontování	k1gNnSc7	smontování
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
domácí	domácí	k2eAgFnSc2d1	domácí
přilby	přilba	k1gFnSc2	přilba
a	a	k8xC	a
čelenky	čelenka	k1gFnSc2	čelenka
severského	severský	k2eAgInSc2d1	severský
původu	původ	k1gInSc2	původ
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
boha	bůh	k1gMnSc2	bůh
Ódina	Ódin	k1gMnSc2	Ódin
připoutaného	připoutaný	k2eAgMnSc2d1	připoutaný
ke	k	k7c3	k
stromu	strom	k1gInSc2	strom
Yggdrasil	Yggdrasil	k1gMnPc2	Yggdrasil
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
prostředí	prostředí	k1gNnSc4	prostředí
nově	nově	k6eAd1	nově
interpretováno	interpretovat	k5eAaBmNgNnS	interpretovat
jako	jako	k8xC	jako
obraz	obraz	k1gInSc1	obraz
ukřižovaného	ukřižovaný	k2eAgMnSc2d1	ukřižovaný
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
mytologie	mytologie	k1gFnSc2	mytologie
Ódin	Ódin	k1gMnSc1	Ódin
obětoval	obětovat	k5eAaBmAgMnS	obětovat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
devět	devět	k4xCc4	devět
dní	den	k1gInPc2	den
a	a	k8xC	a
nocí	noc	k1gFnPc2	noc
zavěšen	zavěšen	k2eAgMnSc1d1	zavěšen
na	na	k7c6	na
kmeni	kmen	k1gInSc6	kmen
stromu	strom	k1gInSc2	strom
Yggdrasil	Yggdrasil	k1gMnSc1	Yggdrasil
a	a	k8xC	a
probodnutý	probodnutý	k2eAgMnSc1d1	probodnutý
kopím	kopit	k5eAaImIp1nS	kopit
Gungnirem	Gungniro	k1gNnSc7	Gungniro
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
získal	získat	k5eAaPmAgMnS	získat
tajemství	tajemství	k1gNnSc4	tajemství
run	run	k1gInSc1	run
<g/>
,	,	kIx,	,
symbolu	symbol	k1gInSc3	symbol
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
autora	autor	k1gMnSc2	autor
Neila	Neil	k1gMnSc2	Neil
Gaimana	Gaiman	k1gMnSc2	Gaiman
Američtí	americký	k2eAgMnPc1d1	americký
bohové	bůh	k1gMnPc1	bůh
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
pan	pan	k1gMnSc1	pan
Středa	středa	k1gFnSc1	středa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
bůh	bůh	k1gMnSc1	bůh
Odin	Odin	k1gMnSc1	Odin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
snaží	snažit	k5eAaImIp3nS	snažit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
bohy	bůh	k1gMnPc4	bůh
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
sjednotili	sjednotit	k5eAaPmAgMnP	sjednotit
a	a	k8xC	a
stanuli	stanout	k5eAaPmAgMnP	stanout
proti	proti	k7c3	proti
moderním	moderní	k2eAgInPc3d1	moderní
idolům	idol	k1gInPc3	idol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ódinova	Ódinův	k2eAgNnPc1d1	Ódinovo
jména	jméno	k1gNnPc1	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Jména	jméno	k1gNnPc1	jméno
zmiňované	zmiňovaný	k2eAgNnSc4d1	zmiňované
v	v	k7c4	v
Snorriho	Snorri	k1gMnSc4	Snorri
mladší	mladý	k2eAgMnSc1d2	mladší
Eddě	Edda	k1gFnSc6	Edda
jsou	být	k5eAaImIp3nP	být
zvýrazněna	zvýrazněn	k2eAgNnPc1d1	zvýrazněno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
zápisu	zápis	k1gInSc2	zápis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Óð	Óð	k?	Óð
<g/>
*	*	kIx~	*
</s>
</p>
<p>
<s>
Odin	Odin	k1gMnSc1	Odin
</s>
</p>
<p>
<s>
Odinn	Odinn	k1gMnSc1	Odinn
</s>
</p>
<p>
<s>
Othin	Othin	k1gMnSc1	Othin
</s>
</p>
<p>
<s>
Wodan	Wodan	k1gMnSc1	Wodan
</s>
</p>
<p>
<s>
Wothan	Wothan	k1gMnSc1	Wothan
</s>
</p>
<p>
<s>
Wuodan	Wuodan	k1gMnSc1	Wuodan
</s>
</p>
<p>
<s>
Wuothan	Wuothan	k1gMnSc1	Wuothan
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Bůh	bůh	k1gMnSc1	bůh
Otec	otec	k1gMnSc1	otec
</s>
</p>
<p>
<s>
Svarog	Svarog	k1gInSc1	Svarog
-	-	kIx~	-
hlavní	hlavní	k2eAgMnSc1d1	hlavní
bůh	bůh	k1gMnSc1	bůh
slovanského	slovanský	k2eAgInSc2d1	slovanský
panteonu	panteon	k1gInSc2	panteon
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ódin	Ódina	k1gFnPc2	Ódina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
