<s>
Ódin	Ódin	k1gNnSc1	Ódin
(	(	kIx(	(
<g/>
staroseversky	staroseversky	k6eAd1	staroseversky
Óð	Óð	k1gMnSc2	Óð
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
hlavního	hlavní	k2eAgMnSc2d1	hlavní
boha	bůh	k1gMnSc2	bůh
severského	severský	k2eAgInSc2d1	severský
panteonu	panteon	k1gInSc2	panteon
náležící	náležící	k2eAgFnSc1d1	náležící
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
bohů	bůh	k1gMnPc2	bůh
(	(	kIx(	(
<g/>
božského	božský	k2eAgInSc2d1	božský
rodu	rod	k1gInSc2	rod
<g/>
)	)	kIx)	)
zvaní	zvaní	k1gNnSc1	zvaní
Ásové	Ásová	k1gFnSc2	Ásová
(	(	kIx(	(
<g/>
staroseversky	staroseversky	k6eAd1	staroseversky
Æ	Æ	k?	Æ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tento	tento	k3xDgMnSc1	tento
bůh	bůh	k1gMnSc1	bůh
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
a	a	k8xC	a
uctívaný	uctívaný	k2eAgMnSc1d1	uctívaný
patrně	patrně	k6eAd1	patrně
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
obývaném	obývaný	k2eAgNnSc6d1	obývané
Germány	Germán	k1gMnPc7	Germán
<g/>
.	.	kIx.	.
</s>
