<s>
Meiseldorf	Meiseldorf	k1gMnSc1
</s>
<s>
Meiseldorf	Meiseldorf	k1gMnSc1
Stockern	Stockern	k1gMnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
53	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
45	#num#	k4
<g/>
′	′	k?
<g/>
4	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
398	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc4
spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
okres	okres	k1gInSc4
</s>
<s>
Horn	Horn	k1gMnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
obce	obec	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Horn	Horn	k1gMnSc1
</s>
<s>
Meiseldorf	Meiseldorf	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
35,44	35,44	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
903	#num#	k4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
25	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.meiseldorf.at	www.meiseldorf.at	k2eAgInSc1d1
E-mail	e-mail	k1gInSc1
</s>
<s>
gemeinde@meiseldorf.gv.at	gemeinde@meiseldorf.gv.at	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
02983	#num#	k4
PSČ	PSČ	kA
</s>
<s>
3744	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
HO	on	k3xPp3gMnSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Meiseldorf	Meiseldorf	k1gInSc1
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
ve	v	k7c6
spolkové	spolkový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
v	v	k7c6
okrese	okres	k1gInSc6
Horn.	Horn.	kA
Žije	žít	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
903	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Meiseldorf	Meiseldorf	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
spolkové	spolkový	k2eAgFnSc2d1
země	zem	k1gFnSc2
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
v	v	k7c6
regionu	region	k1gInSc6
Waldviertel	Waldviertel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
asi	asi	k9
6	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
okresního	okresní	k2eAgNnSc2d1
města	město	k1gNnSc2
Horn.	Horn.	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
území	území	k1gNnPc2
obce	obec	k1gFnSc2
činí	činit	k5eAaImIp3nS
35,44	35,44	k4
km²	km²	k?
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
26,2	26,2	k4
%	%	kIx~
je	být	k5eAaImIp3nS
zalesněno	zalesněn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Členění	členění	k1gNnSc1
</s>
<s>
Území	území	k1gNnSc4
obce	obec	k1gFnSc2
Meiseldorf	Meiseldorf	k1gMnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
částí	část	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
závorce	závorka	k1gFnSc6
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Kattau	Kattau	k6eAd1
(	(	kIx(
<g/>
201	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klein-Meiseldorf	Klein-Meiseldorf	k1gInSc1
(	(	kIx(
<g/>
398	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Maigen	Maigen	k1gInSc1
(	(	kIx(
<g/>
96	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stockern	Stockern	k1gNnSc1
(	(	kIx(
<g/>
208	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pozoruhodnosti	pozoruhodnost	k1gFnPc1
</s>
<s>
Zámek	zámek	k1gInSc1
Kattau	Kattaus	k1gInSc2
v	v	k7c6
Kattau	Kattaus	k1gInSc6
</s>
<s>
Zámek	zámek	k1gInSc1
Stockern	Stockerna	k1gFnPc2
ve	v	k7c4
Stockernu	Stockerna	k1gFnSc4
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Kandidus	Kandidus	k1gInSc1
Pontz	Pontz	k1gInSc1
von	von	k1gInSc1
Engelshofen	Engelshofen	k1gInSc1
(	(	kIx(
<g/>
1803	#num#	k4
–	–	k?
1866	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
bydlel	bydlet	k5eAaImAgMnS
v	v	k7c6
zámku	zámek	k1gInSc6
Stockern	Stockerna	k1gFnPc2
</s>
<s>
Josef	Josef	k1gMnSc1
von	von	k1gInSc4
Hempel	Hempel	k1gInSc1
(	(	kIx(
<g/>
1800	#num#	k4
–	–	k?
1871	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
majitel	majitel	k1gMnSc1
panství	panství	k1gNnSc2
Kattau	Kattaus	k1gInSc2
</s>
<s>
Jan	Jan	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
z	z	k7c2
Lambergu	Lamberg	k1gInSc2
(	(	kIx(
<g/>
1608	#num#	k4
–	–	k?
1682	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rakouský	rakouský	k2eAgMnSc1d1
aristokrat	aristokrat	k1gMnSc1
<g/>
,	,	kIx,
diplomat	diplomat	k1gMnSc1
a	a	k8xC
ministr	ministr	k1gMnSc1
<g/>
,	,	kIx,
majitel	majitel	k1gMnSc1
panství	panství	k1gNnSc2
Stockern	Stockern	k1gMnSc1
</s>
<s>
Bertha	Bertha	k1gFnSc1
von	von	k1gInSc1
Suttnerová	Suttnerová	k1gFnSc1
(	(	kIx(
<g/>
1843	#num#	k4
–	–	k?
1914	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
psala	psát	k5eAaImAgFnS
na	na	k7c6
zámku	zámek	k1gInSc6
Stockern	Stockerna	k1gFnPc2
svůj	svůj	k3xOyFgInSc4
román	román	k1gInSc4
Die	Die	k1gFnSc2
Waffen	Waffen	k2eAgInSc4d1
nieder	nieder	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
:	:	kIx,
Odzbrojte	odzbrojit	k5eAaPmRp2nP
<g/>
)	)	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Zámek	zámek	k1gInSc1
Stockern	Stockerna	k1gFnPc2
</s>
<s>
Zámek	zámek	k1gInSc1
Kattau	Kattaus	k1gInSc2
</s>
<s>
Fara	fara	k1gFnSc1
v	v	k7c6
Maigenu	Maigen	k1gInSc6
</s>
<s>
Mlýn	mlýn	k1gInSc1
v	v	k7c6
Kattau	Kattaus	k1gInSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Meiseldorf	Meiseldorf	k1gInSc1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Meiseldorf	Meiseldorf	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obce	obec	k1gFnPc1
okresu	okres	k1gInSc2
Horn	Horn	k1gMnSc1
</s>
<s>
Altenburg	Altenburg	k1gMnSc1
•	•	k?
Brunn	Brunn	k1gMnSc1
an	an	k?
der	drát	k5eAaImRp2nS
Wild	Wild	k1gInSc1
•	•	k?
Burgschleinitz-Kühnring	Burgschleinitz-Kühnring	k1gInSc1
•	•	k?
Drosendorf-Zissersdorf	Drosendorf-Zissersdorf	k1gInSc1
•	•	k?
Eggenburg	Eggenburg	k1gInSc1
•	•	k?
Gars	Gars	k1gInSc1
am	am	k?
Kamp	kamp	k1gInSc1
•	•	k?
Geras	Geras	k1gMnSc1
•	•	k?
Horn	Horn	k1gMnSc1
•	•	k?
Irnfritz-Messern	Irnfritz-Messern	k1gMnSc1
•	•	k?
Japons	Japons	k1gInSc1
•	•	k?
Langau	Langaus	k1gInSc2
•	•	k?
Meiseldorf	Meiseldorf	k1gMnSc1
•	•	k?
Pernegg	Pernegg	k1gMnSc1
•	•	k?
Röhrenbach	Röhrenbach	k1gMnSc1
•	•	k?
Röschitz	Röschitz	k1gMnSc1
•	•	k?
Rosenburg-Mold	Rosenburg-Mold	k1gMnSc1
•	•	k?
Sigmundsherberg	Sigmundsherberg	k1gMnSc1
•	•	k?
St.	st.	kA
Bernhard-Frauenhofen	Bernhard-Frauenhofen	k1gInSc1
•	•	k?
Straning-Grafenberg	Straning-Grafenberg	k1gInSc1
•	•	k?
Weitersfeld	Weitersfeld	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
648949	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4811312-8	4811312-8	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
236140245	#num#	k4
</s>
