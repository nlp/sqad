<s>
Autismus	autismus	k1gInSc1	autismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
α	α	k?	α
autos	autos	k1gMnSc1	autos
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
vývojové	vývojový	k2eAgNnSc4d1	vývojové
mentální	mentální	k2eAgNnSc4d1	mentální
postižení	postižení	k1gNnSc4	postižení
projevující	projevující	k2eAgFnSc4d1	projevující
se	se	k3xPyFc4	se
abnormální	abnormální	k2eAgFnSc7d1	abnormální
sociální	sociální	k2eAgFnSc7d1	sociální
interakcí	interakce	k1gFnSc7	interakce
<g/>
,	,	kIx,	,
stálými	stálý	k2eAgMnPc7d1	stálý
opakujícími	opakující	k2eAgMnPc7d1	opakující
se	s	k7c7	s
vzorci	vzorec	k1gInPc7	vzorec
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
narušenými	narušený	k2eAgFnPc7d1	narušená
komunikačními	komunikační	k2eAgFnPc7d1	komunikační
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
před	před	k7c7	před
třetím	třetí	k4xOgInSc7	třetí
rokem	rok	k1gInSc7	rok
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
poruchy	porucha	k1gFnPc4	porucha
autistického	autistický	k2eAgNnSc2d1	autistické
spektra	spektrum	k1gNnSc2	spektrum
(	(	kIx(	(
<g/>
PAS	pas	k6eAd1	pas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
autismu	autismus	k1gInSc2	autismus
jsou	být	k5eAaImIp3nP	být
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
poruchami	porucha	k1gFnPc7	porucha
autistického	autistický	k2eAgNnSc2d1	autistické
spektra	spektrum	k1gNnSc2	spektrum
patrně	patrně	k6eAd1	patrně
stojí	stát	k5eAaImIp3nS	stát
kombinace	kombinace	k1gFnSc1	kombinace
genetických	genetický	k2eAgInPc2d1	genetický
předpokladů	předpoklad	k1gInPc2	předpoklad
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
rizikových	rizikový	k2eAgInPc2d1	rizikový
faktorů	faktor	k1gInPc2	faktor
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
zátěž	zátěž	k1gFnSc4	zátěž
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
látkami	látka	k1gFnPc7	látka
nepříznivými	příznivý	k2eNgFnPc7d1	nepříznivá
pro	pro	k7c4	pro
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
popisují	popisovat	k5eAaImIp3nP	popisovat
souvislosti	souvislost	k1gFnPc4	souvislost
mezi	mezi	k7c7	mezi
výskytem	výskyt	k1gInSc7	výskyt
autismu	autismus	k1gInSc2	autismus
a	a	k8xC	a
destabilizace	destabilizace	k1gFnSc2	destabilizace
nezralého	zralý	k2eNgInSc2d1	nezralý
<g />
.	.	kIx.	.
</s>
<s>
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
např.	např.	kA	např.
prudké	prudký	k2eAgNnSc1d1	prudké
infekční	infekční	k2eAgInSc1d1	infekční
onemocnění	onemocnění	k1gNnSc2	onemocnění
nebo	nebo	k8xC	nebo
intoxikace	intoxikace	k1gFnSc2	intoxikace
organismu	organismus	k1gInSc2	organismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
kontroverzní	kontroverzní	k2eAgNnSc4d1	kontroverzní
je	být	k5eAaImIp3nS	být
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
očkováním	očkování	k1gNnSc7	očkování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Delong	Delong	k1gInSc1	Delong
popisuje	popisovat	k5eAaImIp3nS	popisovat
retrospektivní	retrospektivní	k2eAgFnSc4d1	retrospektivní
analýzu	analýza	k1gFnSc4	analýza
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
zjistil	zjistit	k5eAaPmAgInS	zjistit
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
korelaci	korelace	k1gFnSc4	korelace
autismu	autismus	k1gInSc2	autismus
a	a	k8xC	a
očkování	očkování	k1gNnSc2	očkování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc1d1	podobný
typy	typ	k1gInPc1	typ
studií	studie	k1gFnPc2	studie
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vesměs	vesměs	k6eAd1	vesměs
hodnoceny	hodnotit	k5eAaImNgInP	hodnotit
jako	jako	k8xC	jako
slabé	slabý	k2eAgInPc1d1	slabý
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vypovídací	vypovídací	k2eAgFnSc2d1	vypovídací
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Závažnost	závažnost	k1gFnSc1	závažnost
postižení	postižení	k1gNnSc2	postižení
má	mít	k5eAaImIp3nS	mít
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
a	a	k8xC	a
bere	brát	k5eAaImIp3nS	brát
se	se	k3xPyFc4	se
jako	jako	k9	jako
neléčitelné	léčitelný	k2eNgNnSc1d1	neléčitelné
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
provází	provázet	k5eAaImIp3nS	provázet
člověka	člověk	k1gMnSc4	člověk
celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
některé	některý	k3yIgInPc1	některý
terapeutické	terapeutický	k2eAgInPc1d1	terapeutický
postupy	postup	k1gInPc1	postup
mohou	moct	k5eAaImIp3nP	moct
projevy	projev	k1gInPc1	projev
autismu	autismus	k1gInSc2	autismus
zásadně	zásadně	k6eAd1	zásadně
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
autista	autista	k1gMnSc1	autista
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
studovat	studovat	k5eAaImF	studovat
i	i	k8xC	i
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
však	však	k9	však
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
formami	forma	k1gFnPc7	forma
úlev	úleva	k1gFnPc2	úleva
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
speciální	speciální	k2eAgFnPc1d1	speciální
kompenzační	kompenzační	k2eAgFnPc1d1	kompenzační
psychologické	psychologický	k2eAgFnPc1d1	psychologická
techniky	technika	k1gFnPc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
rizikových	rizikový	k2eAgInPc2d1	rizikový
faktorů	faktor	k1gInPc2	faktor
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
autismu	autismus	k1gInSc2	autismus
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
situaci	situace	k1gFnSc3	situace
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
autismus	autismus	k1gInSc1	autismus
často	často	k6eAd1	často
poddiagnostikován	poddiagnostikován	k2eAgInSc1d1	poddiagnostikován
a	a	k8xC	a
postižení	postižený	k1gMnPc1	postižený
byli	být	k5eAaImAgMnP	být
spíše	spíše	k9	spíše
kriminalizováni	kriminalizován	k2eAgMnPc1d1	kriminalizován
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
retrospektivní	retrospektivní	k2eAgFnSc1d1	retrospektivní
analýza	analýza	k1gFnSc1	analýza
tak	tak	k6eAd1	tak
zachytí	zachytit	k5eAaPmIp3nS	zachytit
korelaci	korelace	k1gFnSc4	korelace
výskytu	výskyt	k1gInSc2	výskyt
autismu	autismus	k1gInSc2	autismus
s	s	k7c7	s
jakýmkoliv	jakýkoliv	k3yIgInSc7	jakýkoliv
faktorem	faktor	k1gInSc7	faktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jeví	jevit	k5eAaImIp3nS	jevit
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
tendenci	tendence	k1gFnSc4	tendence
<g/>
,	,	kIx,	,
např.	např.	kA	např.
proočkovaností	proočkovanost	k1gFnSc7	proočkovanost
<g/>
,	,	kIx,	,
příjmem	příjem	k1gInSc7	příjem
biopotravin	biopotravina	k1gFnPc2	biopotravina
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
počtem	počet	k1gInSc7	počet
pirátů	pirát	k1gMnPc2	pirát
operujících	operující	k2eAgMnPc2d1	operující
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
autismem	autismus	k1gInSc7	autismus
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
žádný	žádný	k3yNgInSc1	žádný
biologický	biologický	k2eAgInSc1d1	biologický
vztah	vztah	k1gInSc1	vztah
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
kauzálního	kauzální	k2eAgInSc2d1	kauzální
nebo	nebo	k8xC	nebo
rizikového	rizikový	k2eAgInSc2d1	rizikový
faktoru	faktor	k1gInSc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hledáním	hledání	k1gNnSc7	hledání
příčin	příčina	k1gFnPc2	příčina
autismu	autismus	k1gInSc2	autismus
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
i	i	k9	i
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
podvodů	podvod	k1gInPc2	podvod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
A.	A.	kA	A.
Wakefield	Wakefield	k1gMnSc1	Wakefield
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
prestižním	prestižní	k2eAgInSc6d1	prestižní
časopise	časopis	k1gInSc6	časopis
The	The	k1gFnSc2	The
Lancet	lanceta	k1gFnPc2	lanceta
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prokazovala	prokazovat	k5eAaImAgFnS	prokazovat
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
výskyt	výskyt	k1gInSc4	výskyt
poruch	porucha	k1gFnPc2	porucha
autistického	autistický	k2eAgNnSc2d1	autistické
spektra	spektrum	k1gNnSc2	spektrum
po	po	k7c6	po
očkování	očkování	k1gNnSc6	očkování
MMR	MMR	kA	MMR
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
studie	studie	k1gFnSc1	studie
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
odhalena	odhalit	k5eAaPmNgFnS	odhalit
jako	jako	k8xS	jako
podvod	podvod	k1gInSc1	podvod
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
si	se	k3xPyFc3	se
Wakefield	Wakefield	k1gMnSc1	Wakefield
"	"	kIx"	"
<g/>
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
<g/>
"	"	kIx"	"
důkazy	důkaz	k1gInPc1	důkaz
pro	pro	k7c4	pro
probíhající	probíhající	k2eAgInSc4d1	probíhající
soudní	soudní	k2eAgInSc4d1	soudní
spor	spor	k1gInSc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odhalení	odhalení	k1gNnSc6	odhalení
podvodu	podvod	k1gInSc2	podvod
byla	být	k5eAaImAgFnS	být
studie	studie	k1gFnSc1	studie
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
a	a	k8xC	a
Wakefield	Wakefield	k1gInSc1	Wakefield
byl	být	k5eAaImAgInS	být
zbaven	zbaven	k2eAgInSc1d1	zbaven
oprávnění	oprávnění	k1gNnSc2	oprávnění
vykonávat	vykonávat	k5eAaImF	vykonávat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Diagnostické	diagnostický	k2eAgFnPc1d1	diagnostická
metody	metoda	k1gFnPc1	metoda
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poruchy	porucha	k1gFnPc1	porucha
autistického	autistický	k2eAgNnSc2d1	autistické
spektra	spektrum	k1gNnSc2	spektrum
se	se	k3xPyFc4	se
určují	určovat	k5eAaImIp3nP	určovat
pomocí	pomocí	k7c2	pomocí
psychotestů	psychotest	k1gInPc2	psychotest
diferenciální	diferenciální	k2eAgFnSc7d1	diferenciální
diagnostikou	diagnostika	k1gFnSc7	diagnostika
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vylučovací	vylučovací	k2eAgFnSc7d1	vylučovací
metodou	metoda	k1gFnSc7	metoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
škála	škála	k1gFnSc1	škála
příznaků	příznak	k1gInPc2	příznak
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
a	a	k8xC	a
neexistuje	existovat	k5eNaImIp3nS	existovat
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
(	(	kIx(	(
<g/>
vždy	vždy	k6eAd1	vždy
platný	platný	k2eAgInSc1d1	platný
<g/>
)	)	kIx)	)
klinický	klinický	k2eAgInSc1d1	klinický
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
názory	názor	k1gInPc1	názor
odborníků	odborník	k1gMnPc2	odborník
na	na	k7c4	na
pacienta	pacient	k1gMnSc4	pacient
liší	lišit	k5eAaImIp3nS	lišit
-	-	kIx~	-
pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
psychometrii	psychometrie	k1gFnSc4	psychometrie
autismu	autismus	k1gInSc2	autismus
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
solidní	solidní	k2eAgFnSc1d1	solidní
praxe	praxe	k1gFnSc1	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
diagnostiku	diagnostika	k1gFnSc4	diagnostika
dospělých	dospělí	k1gMnPc2	dospělí
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
používají	používat	k5eAaImIp3nP	používat
zcela	zcela	k6eAd1	zcela
odlišné	odlišný	k2eAgFnPc1d1	odlišná
testové	testový	k2eAgFnPc1d1	testová
sady	sada	k1gFnPc1	sada
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
do	do	k7c2	do
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
běžně	běžně	k6eAd1	běžně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
za	za	k7c4	za
lehkou	lehký	k2eAgFnSc4d1	lehká
mozkovou	mozkový	k2eAgFnSc4d1	mozková
dysfunkci	dysfunkce	k1gFnSc4	dysfunkce
<g/>
,	,	kIx,	,
schizofrenii	schizofrenie	k1gFnSc4	schizofrenie
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
další	další	k2eAgFnPc4d1	další
formy	forma	k1gFnPc4	forma
poruchy	porucha	k1gFnSc2	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
probíhajících	probíhající	k2eAgInPc2d1	probíhající
výzkumů	výzkum	k1gInPc2	výzkum
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
nové	nový	k2eAgFnPc4d1	nová
možnosti	možnost	k1gFnPc4	možnost
především	především	k6eAd1	především
rané	raný	k2eAgFnPc4d1	raná
diagnostiky	diagnostika	k1gFnPc4	diagnostika
autismu	autismus	k1gInSc2	autismus
(	(	kIx(	(
<g/>
funkční	funkční	k2eAgFnSc1d1	funkční
magnetická	magnetický	k2eAgFnSc1d1	magnetická
rezonance	rezonance	k1gFnSc1	rezonance
<g/>
,	,	kIx,	,
analýza	analýza	k1gFnSc1	analýza
motoriky	motorik	k1gMnPc7	motorik
<g/>
,	,	kIx,	,
analýza	analýza	k1gFnSc1	analýza
hlasu	hlas	k1gInSc2	hlas
<g/>
,	,	kIx,	,
EEG	EEG	kA	EEG
markery	marker	k1gInPc1	marker
<g/>
,	,	kIx,	,
hladiny	hladina	k1gFnPc1	hladina
metaloproteinů	metaloprotein	k1gMnPc2	metaloprotein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
metod	metoda	k1gFnPc2	metoda
však	však	k9	však
neprokázala	prokázat	k5eNaPmAgFnS	prokázat
obecnou	obecný	k2eAgFnSc4d1	obecná
platnost	platnost	k1gFnSc4	platnost
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
výsledky	výsledek	k1gInPc1	výsledek
některých	některý	k3yIgInPc2	některý
postupů	postup	k1gInPc2	postup
jsou	být	k5eAaImIp3nP	být
nadějné	nadějný	k2eAgInPc1d1	nadějný
<g/>
.	.	kIx.	.
</s>
<s>
Vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
a	a	k8xC	a
upřesňují	upřesňovat	k5eAaImIp3nP	upřesňovat
se	se	k3xPyFc4	se
i	i	k9	i
definice	definice	k1gFnSc1	definice
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
forem	forma	k1gFnPc2	forma
autismu	autismus	k1gInSc2	autismus
<g/>
,	,	kIx,	,
významnou	významný	k2eAgFnSc4d1	významná
změnu	změna	k1gFnSc4	změna
přinesla	přinést	k5eAaPmAgFnS	přinést
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
amerického	americký	k2eAgInSc2d1	americký
diagnostického	diagnostický	k2eAgInSc2d1	diagnostický
manuálu	manuál	k1gInSc2	manuál
psychických	psychický	k2eAgFnPc2d1	psychická
chorob	choroba	k1gFnPc2	choroba
DSM-	DSM-	k1gMnPc2	DSM-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
často	často	k6eAd1	často
diskutovaná	diskutovaný	k2eAgFnSc1d1	diskutovaná
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
vnímaná	vnímaný	k2eAgNnPc4d1	vnímané
kontroverzně	kontroverzně	k6eAd1	kontroverzně
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
autismus	autismus	k1gInSc1	autismus
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
psychiatr	psychiatr	k1gMnSc1	psychiatr
Eugene	Eugen	k1gInSc5	Eugen
Bleuler	Bleuler	k1gMnSc1	Bleuler
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
popsal	popsat	k5eAaPmAgMnS	popsat
obtíže	obtíž	k1gFnPc4	obtíž
schizofreniků	schizofrenik	k1gMnPc2	schizofrenik
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
s	s	k7c7	s
druhými	druhý	k4xOgInPc7	druhý
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Detailnější	detailní	k2eAgInSc1d2	detailnější
popis	popis	k1gInSc1	popis
autismu	autismus	k1gInSc2	autismus
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
smyslu	smysl	k1gInSc6	smysl
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
psychiatr	psychiatr	k1gMnSc1	psychiatr
Leo	Leo	k1gMnSc1	Leo
Kanner	Kanner	k1gMnSc1	Kanner
v	v	k7c6	v
Baltimoru	Baltimore	k1gInSc6	Baltimore
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
11	[number]	k4	11
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
příznaky	příznak	k1gInPc7	příznak
nedostatku	nedostatek	k1gInSc2	nedostatek
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
druhé	druhý	k4xOgMnPc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
prováděl	provádět	k5eAaImAgInS	provádět
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
podobná	podobný	k2eAgNnPc1d1	podobné
pozorování	pozorování	k1gNnPc1	pozorování
Hans	Hans	k1gMnSc1	Hans
Asperger	Asperger	k1gMnSc1	Asperger
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
spojeno	spojen	k2eAgNnSc1d1	spojeno
spíše	spíše	k9	spíše
s	s	k7c7	s
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
lehčí	lehký	k2eAgFnSc7d2	lehčí
formou	forma	k1gFnSc7	forma
autismu	autismus	k1gInSc2	autismus
<g/>
,	,	kIx,	,
přezdívanou	přezdívaný	k2eAgFnSc4d1	přezdívaná
jako	jako	k8xC	jako
Aspergerův	Aspergerův	k2eAgInSc4d1	Aspergerův
syndrom	syndrom	k1gInSc4	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
zůstala	zůstat	k5eAaPmAgFnS	zůstat
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
neznámá	známý	k2eNgNnPc1d1	neznámé
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
články	článek	k1gInPc1	článek
byly	být	k5eAaImAgInP	být
znovu	znovu	k6eAd1	znovu
objeveny	objevit	k5eAaPmNgInP	objevit
až	až	k9	až
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Schopnosti	schopnost	k1gFnPc1	schopnost
a	a	k8xC	a
chování	chování	k1gNnSc1	chování
různých	různý	k2eAgMnPc2d1	různý
autistických	autistický	k2eAgMnPc2d1	autistický
jedinců	jedinec	k1gMnPc2	jedinec
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
a	a	k8xC	a
různí	různý	k2eAgMnPc1d1	různý
lékaři	lékař	k1gMnPc1	lékař
často	často	k6eAd1	často
mohou	moct	k5eAaImIp3nP	moct
dospět	dospět	k5eAaPmF	dospět
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
diagnózám	diagnóza	k1gFnPc3	diagnóza
<g/>
.	.	kIx.	.
</s>
<s>
Smyslové	smyslový	k2eAgNnSc1d1	smyslové
vnímání	vnímání	k1gNnSc1	vnímání
autistů	autist	k1gInPc2	autist
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgMnPc3d1	ostatní
lidem	člověk	k1gMnPc3	člověk
odlišné	odlišný	k2eAgFnSc2d1	odlišná
a	a	k8xC	a
na	na	k7c4	na
určité	určitý	k2eAgInPc4d1	určitý
podněty	podnět	k1gInPc4	podnět
proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
reagovat	reagovat	k5eAaBmF	reagovat
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
stupni	stupeň	k1gInSc6	stupeň
postižení	postižení	k1gNnSc2	postižení
smyslového	smyslový	k2eAgNnSc2d1	smyslové
vnímání	vnímání	k1gNnSc2	vnímání
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Zdravé	zdravý	k2eAgFnPc1d1	zdravá
děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
časného	časný	k2eAgInSc2d1	časný
věku	věk	k1gInSc2	věk
společenské	společenský	k2eAgFnSc2d1	společenská
bytosti	bytost	k1gFnSc2	bytost
-	-	kIx~	-
navazují	navazovat	k5eAaImIp3nP	navazovat
oční	oční	k2eAgInSc4d1	oční
kontakt	kontakt	k1gInSc4	kontakt
<g/>
,	,	kIx,	,
otáčejí	otáčet	k5eAaImIp3nP	otáčet
se	se	k3xPyFc4	se
ke	k	k7c3	k
zdroji	zdroj	k1gInSc3	zdroj
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
uchopují	uchopovat	k5eAaImIp3nP	uchopovat
prst	prst	k1gInSc4	prst
a	a	k8xC	a
usmívají	usmívat	k5eAaImIp3nP	usmívat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ke	k	k7c3	k
konci	konec	k1gInSc6	konec
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
pozorovatelné	pozorovatelný	k2eAgFnPc1d1	pozorovatelná
všechny	všechen	k3xTgFnPc1	všechen
základní	základní	k2eAgFnPc1d1	základní
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
zráním	zrání	k1gNnSc7	zrání
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
většina	většina	k1gFnSc1	většina
autistických	autistický	k2eAgFnPc2d1	autistická
dětí	dítě	k1gFnPc2	dítě
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
předmětům	předmět	k1gInPc3	předmět
před	před	k7c7	před
obličeji	obličej	k1gInPc7	obličej
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
obrovské	obrovský	k2eAgFnPc4d1	obrovská
potíže	potíž	k1gFnPc4	potíž
při	při	k7c6	při
učení	učení	k1gNnSc6	učení
se	se	k3xPyFc4	se
běžné	běžný	k2eAgFnSc3d1	běžná
každodenní	každodenní	k2eAgFnSc3d1	každodenní
mezilidské	mezilidský	k2eAgFnSc3d1	mezilidská
interakci	interakce	k1gFnSc3	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
měsíců	měsíc	k1gInPc2	měsíc
života	život	k1gInSc2	život
často	často	k6eAd1	často
vypadají	vypadat	k5eAaPmIp3nP	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
lhostejní	lhostejný	k2eAgMnPc1d1	lhostejný
k	k	k7c3	k
druhým	druhý	k4xOgMnPc3	druhý
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
očnímu	oční	k2eAgInSc3d1	oční
kontaktu	kontakt	k1gInSc3	kontakt
a	a	k8xC	a
nekomunikují	komunikovat	k5eNaImIp3nP	komunikovat
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Autistické	autistický	k2eAgFnPc1d1	autistická
děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zdají	zdát	k5eAaImIp3nP	zdát
dávat	dávat	k5eAaImF	dávat
přednost	přednost	k1gFnSc4	přednost
samotě	samota	k1gFnSc3	samota
před	před	k7c7	před
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
reagovat	reagovat	k5eAaBmF	reagovat
pasivně	pasivně	k6eAd1	pasivně
na	na	k7c6	na
objímání	objímání	k1gNnSc6	objímání
a	a	k8xC	a
mazlení	mazlení	k1gNnSc6	mazlení
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zřídka	zřídka	k6eAd1	zřídka
hledají	hledat	k5eAaImIp3nP	hledat
útěchu	útěcha	k1gFnSc4	útěcha
od	od	k7c2	od
druhých	druhý	k4xOgInPc2	druhý
a	a	k8xC	a
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
normálním	normální	k2eAgInSc7d1	normální
způsobem	způsob	k1gInSc7	způsob
na	na	k7c4	na
projevy	projev	k1gInPc4	projev
hněvu	hněv	k1gInSc2	hněv
či	či	k8xC	či
náklonnosti	náklonnost	k1gFnSc2	náklonnost
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
autistické	autistický	k2eAgFnPc1d1	autistická
děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
rády	rád	k2eAgMnPc4d1	rád
<g/>
,	,	kIx,	,
projevy	projev	k1gInPc4	projev
této	tento	k3xDgFnSc2	tento
náklonnosti	náklonnost	k1gFnSc2	náklonnost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
neobvyklé	obvyklý	k2eNgInPc1d1	neobvyklý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
je	on	k3xPp3gInPc4	on
interpretovat	interpretovat	k5eAaBmF	interpretovat
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nP	těšit
na	na	k7c4	na
radosti	radost	k1gFnPc4	radost
mazlení	mazlení	k1gNnSc2	mazlení
a	a	k8xC	a
hraní	hraní	k1gNnSc2	hraní
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
zmateni	zmást	k5eAaPmNgMnP	zmást
i	i	k8xC	i
zdrceni	zdrtit	k5eAaPmNgMnP	zdrtit
tímto	tento	k3xDgNnSc7	tento
neočekávaným	očekávaný	k2eNgNnSc7d1	neočekávané
chováním	chování	k1gNnSc7	chování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Simona	Simon	k1gMnSc2	Simon
Baron-Cohena	Baron-Cohen	k1gMnSc2	Baron-Cohen
autistické	autistický	k2eAgFnPc1d1	autistická
děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
zdají	zdát	k5eAaImIp3nP	zdát
postrádat	postrádat	k5eAaImF	postrádat
empatii	empatie	k1gFnSc4	empatie
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
vidět	vidět	k5eAaImF	vidět
věci	věc	k1gFnPc4	věc
z	z	k7c2	z
perspektivy	perspektiva	k1gFnPc4	perspektiva
druhých	druhý	k4xOgMnPc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Zdravé	zdravý	k2eAgFnPc4d1	zdravá
děti	dítě	k1gFnPc4	dítě
nad	nad	k7c4	nad
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
vhledu	vhled	k1gInSc2	vhled
do	do	k7c2	do
pocitů	pocit	k1gInPc2	pocit
<g/>
,	,	kIx,	,
záměrů	záměr	k1gInPc2	záměr
druhých	druhý	k4xOgMnPc2	druhý
podle	podle	k7c2	podle
nonverbálních	nonverbální	k2eAgNnPc2d1	nonverbální
gest	gesto	k1gNnPc2	gesto
<g/>
,	,	kIx,	,
výrazu	výraz	k1gInSc3	výraz
tváře	tvář	k1gFnSc2	tvář
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Sociální	sociální	k2eAgNnSc1d1	sociální
odcizení	odcizení	k1gNnSc1	odcizení
jedinců	jedinec	k1gMnPc2	jedinec
s	s	k7c7	s
autismem	autismus	k1gInSc7	autismus
a	a	k8xC	a
Aspergerovým	Aspergerův	k2eAgInSc7d1	Aspergerův
syndromem	syndrom	k1gInSc7	syndrom
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
natolik	natolik	k6eAd1	natolik
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
imaginární	imaginární	k2eAgMnPc4d1	imaginární
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
imaginární	imaginární	k2eAgInPc4d1	imaginární
světy	svět	k1gInPc4	svět
a	a	k8xC	a
scénáře	scénář	k1gInPc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Navazování	navazování	k1gNnSc1	navazování
a	a	k8xC	a
udržování	udržování	k1gNnSc1	udržování
přátelství	přátelství	k1gNnSc2	přátelství
ve	v	k7c6	v
skutečném	skutečný	k2eAgInSc6d1	skutečný
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
často	často	k6eAd1	často
velice	velice	k6eAd1	velice
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
příznaky	příznak	k1gInPc7	příznak
sociální	sociální	k2eAgFnSc2d1	sociální
fobie	fobie	k1gFnSc2	fobie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
autistů	autista	k1gMnPc2	autista
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
zcela	zcela	k6eAd1	zcela
regulovat	regulovat	k5eAaImF	regulovat
své	svůj	k3xOyFgNnSc4	svůj
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
podobu	podoba	k1gFnSc4	podoba
slovních	slovní	k2eAgInPc2d1	slovní
výbuchů	výbuch	k1gInPc2	výbuch
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
neadekvátní	adekvátní	k2eNgFnSc4d1	neadekvátní
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
sebezraňujícího	sebezraňující	k2eAgNnSc2d1	sebezraňující
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Autisté	Autista	k1gMnPc1	Autista
typicky	typicky	k6eAd1	typicky
preferují	preferovat	k5eAaImIp3nP	preferovat
neměnné	neměnný	k2eAgFnPc4d1	neměnná
rutiny	rutina	k1gFnPc4	rutina
a	a	k8xC	a
stálá	stálý	k2eAgNnPc4d1	stálé
prostředí	prostředí	k1gNnPc4	prostředí
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
reagovat	reagovat	k5eAaBmF	reagovat
negativně	negativně	k6eAd1	negativně
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
neobvyklé	obvyklý	k2eNgInPc4d1	neobvyklý
projevy	projev	k1gInPc4	projev
agresivity	agresivita	k1gFnSc2	agresivita
<g/>
,	,	kIx,	,
sebestimulační	sebestimulační	k2eAgNnSc1d1	sebestimulační
chování	chování	k1gNnSc1	chování
ani	ani	k8xC	ani
přílišné	přílišný	k2eAgNnSc1d1	přílišné
stáhnutí	stáhnutí	k1gNnSc1	stáhnutí
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	se	k3xPyFc2	se
v	v	k7c6	v
obtížných	obtížný	k2eAgFnPc6d1	obtížná
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
příznaky	příznak	k1gInPc1	příznak
shodné	shodný	k2eAgInPc1d1	shodný
se	se	k3xPyFc4	se
symptomy	symptom	k1gInPc7	symptom
u	u	k7c2	u
dysfunkce	dysfunkce	k1gFnSc2	dysfunkce
smyslové	smyslový	k2eAgFnSc2d1	smyslová
integrace	integrace	k1gFnSc2	integrace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
citlivost	citlivost	k1gFnSc1	citlivost
na	na	k7c4	na
doteky	dotek	k1gInPc4	dotek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
pohyby	pohyb	k1gInPc1	pohyb
<g/>
,	,	kIx,	,
tělesná	tělesný	k2eAgFnSc1d1	tělesná
neobratnost	neobratnost	k1gFnSc1	neobratnost
<g/>
,	,	kIx,	,
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
snadnému	snadný	k2eAgNnSc3d1	snadné
rozptýlení	rozptýlení	k1gNnSc3	rozptýlení
<g/>
,	,	kIx,	,
impulzivní	impulzivní	k2eAgFnSc3d1	impulzivní
tělesné	tělesný	k2eAgFnPc4d1	tělesná
nebo	nebo	k8xC	nebo
verbální	verbální	k2eAgNnSc1d1	verbální
jednání	jednání	k1gNnSc1	jednání
<g/>
,	,	kIx,	,
abnormálně	abnormálně	k6eAd1	abnormálně
vysoký	vysoký	k2eAgMnSc1d1	vysoký
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
nízký	nízký	k2eAgInSc1d1	nízký
stupeň	stupeň	k1gInSc1	stupeň
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
obtíže	obtíž	k1gFnPc4	obtíž
při	při	k7c6	při
učení	učení	k1gNnSc6	učení
se	se	k3xPyFc4	se
novým	nový	k2eAgFnPc3d1	nová
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
,	,	kIx,	,
potíže	potíž	k1gFnPc4	potíž
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
situace	situace	k1gFnSc2	situace
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgInPc4d1	sociální
a	a	k8xC	a
emocionální	emocionální	k2eAgInPc4d1	emocionální
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
opožděný	opožděný	k2eAgInSc1d1	opožděný
vývoj	vývoj	k1gInSc1	vývoj
řeči	řeč	k1gFnSc2	řeč
či	či	k8xC	či
motorických	motorický	k2eAgFnPc2d1	motorická
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
častým	častý	k2eAgInSc7d1	častý
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
obtíže	obtíž	k1gFnPc4	obtíž
se	s	k7c7	s
sluchem	sluch	k1gInSc7	sluch
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
autistický	autistický	k2eAgMnSc1d1	autistický
člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
potíže	potíž	k1gFnPc4	potíž
slyšet	slyšet	k5eAaImF	slyšet
a	a	k8xC	a
rozumět	rozumět	k5eAaImF	rozumět
ostatním	ostatní	k2eAgMnSc7d1	ostatní
v	v	k7c6	v
hlučném	hlučný	k2eAgNnSc6d1	hlučné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
davu	dav	k1gInSc6	dav
na	na	k7c6	na
oslavách	oslava	k1gFnPc6	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
mluvíme	mluvit	k5eAaImIp1nP	mluvit
v	v	k7c6	v
hlučnějším	hlučný	k2eAgNnSc6d2	hlučnější
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
vnímáme	vnímat	k5eAaImIp1nP	vnímat
řeč	řeč	k1gFnSc4	řeč
směřovanou	směřovaný	k2eAgFnSc4d1	směřovaná
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
jako	jako	k9	jako
hlasitější	hlasitý	k2eAgFnSc1d2	hlasitější
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
neverbální	verbální	k2eNgFnSc3d1	neverbální
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
fenomén	fenomén	k1gInSc1	fenomén
koktejlové	koktejlový	k2eAgFnSc2d1	koktejlová
party	parta	k1gFnSc2	parta
a	a	k8xC	a
u	u	k7c2	u
autistů	autista	k1gMnPc2	autista
je	být	k5eAaImIp3nS	být
zjevně	zjevně	k6eAd1	zjevně
narušen	narušen	k2eAgInSc1d1	narušen
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
věku	věk	k1gInSc2	věk
lze	lze	k6eAd1	lze
také	také	k9	také
u	u	k7c2	u
autistických	autistický	k2eAgFnPc2d1	autistická
dětí	dítě	k1gFnPc2	dítě
vypozorovat	vypozorovat	k5eAaPmF	vypozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
dívají	dívat	k5eAaImIp3nP	dívat
na	na	k7c4	na
ústa	ústa	k1gNnPc4	ústa
svých	svůj	k3xOyFgFnPc2	svůj
maminek	maminka	k1gFnPc2	maminka
namísto	namísto	k7c2	namísto
do	do	k7c2	do
jejích	její	k3xOp3gNnPc2	její
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
lze	lze	k6eAd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
vyšší	vysoký	k2eAgFnSc7d2	vyšší
mírou	míra	k1gFnSc7	míra
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
spojitosti	spojitost	k1gFnSc2	spojitost
mezi	mezi	k7c7	mezi
pohybem	pohyb	k1gInSc7	pohyb
úst	ústa	k1gNnPc2	ústa
a	a	k8xC	a
řečí	řeč	k1gFnPc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
někteří	některý	k3yIgMnPc1	některý
autisté	autista	k1gMnPc1	autista
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
s	s	k7c7	s
diagnózou	diagnóza	k1gFnSc7	diagnóza
Aspergerův	Aspergerův	k2eAgInSc1d1	Aspergerův
syndrom	syndrom	k1gInSc1	syndrom
<g/>
)	)	kIx)	)
vnímají	vnímat	k5eAaImIp3nP	vnímat
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
vjemy	vjem	k1gInPc4	vjem
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tzv.	tzv.	kA	tzv.
synestezie	synestezie	k1gFnSc2	synestezie
-	-	kIx~	-
vjem	vjem	k1gInSc1	vjem
přijatý	přijatý	k2eAgInSc1d1	přijatý
jedním	jeden	k4xCgInSc7	jeden
smyslem	smysl	k1gInSc7	smysl
je	být	k5eAaImIp3nS	být
interpretován	interpretován	k2eAgInSc1d1	interpretován
i	i	k9	i
jako	jako	k9	jako
vjem	vjem	k1gInSc4	vjem
jiného	jiný	k2eAgInSc2d1	jiný
smyslu	smysl	k1gInSc2	smysl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hudbu	hudba	k1gFnSc4	hudba
vnímá	vnímat	k5eAaImIp3nS	vnímat
nejen	nejen	k6eAd1	nejen
jako	jako	k8xS	jako
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vizuálně	vizuálně	k6eAd1	vizuálně
v	v	k7c6	v
tvarech	tvar	k1gInPc6	tvar
nebo	nebo	k8xC	nebo
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Samotnými	samotný	k2eAgInPc7d1	samotný
autisty	autist	k1gInPc7	autist
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
často	často	k6eAd1	často
ceněna	cenit	k5eAaImNgFnS	cenit
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
zdravé	zdravá	k1gFnSc2	zdravá
děti	dítě	k1gFnPc1	dítě
ovládnou	ovládnout	k5eAaPmIp3nP	ovládnout
řeč	řeč	k1gFnSc4	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
okolo	okolo	k7c2	okolo
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
batole	batole	k1gNnSc1	batole
říká	říkat	k5eAaImIp3nS	říkat
první	první	k4xOgNnPc4	první
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
otáčí	otáčet	k5eAaImIp3nS	otáčet
se	se	k3xPyFc4	se
při	při	k7c6	při
zaslechnutí	zaslechnutí	k1gNnSc6	zaslechnutí
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
chce	chtít	k5eAaImIp3nS	chtít
hračku	hračka	k1gFnSc4	hračka
<g/>
,	,	kIx,	,
a	a	k8xC	a
jasně	jasně	k6eAd1	jasně
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
když	když	k8xS	když
něco	něco	k3yInSc4	něco
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
autistů	autista	k1gMnPc2	autista
je	být	k5eAaImIp3nS	být
řečový	řečový	k2eAgInSc1d1	řečový
vývoj	vývoj	k1gInSc1	vývoj
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
němí	němý	k2eAgMnPc1d1	němý
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
stupněm	stupeň	k1gInSc7	stupeň
gramotnosti	gramotnost	k1gFnSc2	gramotnost
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
jinou	jiná	k1gFnSc7	jiná
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
např.	např.	kA	např.
obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
posunky	posunek	k1gInPc1	posunek
nebo	nebo	k8xC	nebo
písmem	písmo	k1gNnSc7	písmo
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
přirozenější	přirozený	k2eAgFnSc1d2	přirozenější
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
autistické	autistický	k2eAgFnPc1d1	autistická
děti	dítě	k1gFnPc1	dítě
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
měsících	měsíc	k1gInPc6	měsíc
normálně	normálně	k6eAd1	normálně
žvatlají	žvatlat	k5eAaImIp3nP	žvatlat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzo	brzo	k6eAd1	brzo
poté	poté	k6eAd1	poté
přestanou	přestat	k5eAaPmIp3nP	přestat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jiných	jiný	k1gMnPc2	jiný
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vývoj	vývoj	k1gInSc4	vývoj
řeči	řeč	k1gFnSc2	řeč
opožděn	opožděn	k2eAgInSc4d1	opožděn
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
do	do	k7c2	do
období	období	k1gNnSc2	období
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
neschopnost	neschopnost	k1gFnSc4	neschopnost
mluvit	mluvit	k5eAaImF	mluvit
neznamená	znamenat	k5eNaImIp3nS	znamenat
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vhodných	vhodný	k2eAgFnPc6d1	vhodná
podmínkách	podmínka	k1gFnPc6	podmínka
mohou	moct	k5eAaImIp3nP	moct
někteří	některý	k3yIgMnPc1	některý
mluvit	mluvit	k5eAaImF	mluvit
celé	celý	k2eAgFnPc4d1	celá
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
používají	používat	k5eAaImIp3nP	používat
řeč	řeč	k1gFnSc4	řeč
neobvyklými	obvyklý	k2eNgInPc7d1	neobvyklý
způsoby	způsob	k1gInPc7	způsob
a	a	k8xC	a
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
rysy	rys	k1gMnPc7	rys
časnějších	časný	k2eAgNnPc2d2	časnější
vývojových	vývojový	k2eAgNnPc2d1	vývojové
stádií	stádium	k1gNnPc2	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
pouze	pouze	k6eAd1	pouze
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
opakují	opakovat	k5eAaImIp3nP	opakovat
naučenou	naučený	k2eAgFnSc4d1	naučená
frázi	fráze	k1gFnSc4	fráze
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
opakují	opakovat	k5eAaImIp3nP	opakovat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
slyší	slyšet	k5eAaImIp3nP	slyšet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
echolálie	echolálie	k1gFnSc1	echolálie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
děti	dítě	k1gFnPc1	dítě
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
vývoj	vývoj	k1gInSc4	vývoj
řeči	řeč	k1gFnSc2	řeč
jen	jen	k9	jen
slabě	slabě	k6eAd1	slabě
opožděn	opožděn	k2eAgMnSc1d1	opožděn
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
i	i	k9	i
předčasný	předčasný	k2eAgInSc4d1	předčasný
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
neobvykle	obvykle	k6eNd1	obvykle
velký	velký	k2eAgInSc4d1	velký
výrazový	výrazový	k2eAgInSc4d1	výrazový
slovník	slovník	k1gInSc4	slovník
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgFnPc4d1	velká
potíže	potíž	k1gFnPc4	potíž
udržet	udržet	k5eAaPmF	udržet
běžnou	běžný	k2eAgFnSc4d1	běžná
konverzaci	konverzace	k1gFnSc4	konverzace
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
vedou	vést	k5eAaImIp3nP	vést
monolog	monolog	k1gInSc4	monolog
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
téma	téma	k1gNnSc4	téma
a	a	k8xC	a
nedávají	dávat	k5eNaImIp3nP	dávat
ostatním	ostatní	k2eAgMnPc3d1	ostatní
možnost	možnost	k1gFnSc4	možnost
komentovat	komentovat	k5eAaBmF	komentovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
hovoří	hovořit	k5eAaImIp3nS	hovořit
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
autisty	autist	k1gInPc7	autist
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vedou	vést	k5eAaImIp3nP	vést
střídavé	střídavý	k2eAgInPc1d1	střídavý
monology	monolog	k1gInPc1	monolog
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
normální	normální	k2eAgMnPc1d1	normální
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
chápáním	chápání	k1gNnSc7	chápání
autistické	autistický	k2eAgFnSc2d1	autistická
nonverbální	nonverbální	k2eAgFnSc2d1	nonverbální
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
intonace	intonace	k1gFnSc2	intonace
a	a	k8xC	a
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
i	i	k9	i
autisté	autistý	k2eAgNnSc1d1	autisté
obtížně	obtížně	k6eAd1	obtížně
rozumí	rozumět	k5eAaImIp3nS	rozumět
normálním	normální	k2eAgMnPc3d1	normální
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Autistické	autistický	k2eAgNnSc1d1	autistické
chápání	chápání	k1gNnSc1	chápání
jazyka	jazyk	k1gInSc2	jazyk
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
doslovné	doslovný	k2eAgNnSc1d1	doslovné
<g/>
,	,	kIx,	,
normální	normální	k2eAgMnPc1d1	normální
lidé	člověk	k1gMnPc1	člověk
většinou	většinou	k6eAd1	většinou
hledají	hledat	k5eAaImIp3nP	hledat
skrytý	skrytý	k2eAgInSc4d1	skrytý
význam	význam	k1gInSc4	význam
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
autisté	autista	k1gMnPc1	autista
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
očekávají	očekávat	k5eAaImIp3nP	očekávat
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
naopak	naopak	k6eAd1	naopak
pochopení	pochopení	k1gNnSc4	pochopení
těchto	tento	k3xDgInPc2	tento
skrytých	skrytý	k2eAgInPc2d1	skrytý
významů	význam	k1gInPc2	význam
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
řeči	řeč	k1gFnSc6	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Autistická	autistický	k2eAgNnPc1d1	autistické
gesta	gesto	k1gNnPc1	gesto
<g/>
,	,	kIx,	,
mimika	mimika	k1gFnSc1	mimika
a	a	k8xC	a
pohyby	pohyb	k1gInPc1	pohyb
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
snadno	snadno	k6eAd1	snadno
pochopeny	pochopit	k5eAaPmNgInP	pochopit
jiným	jiný	k1gMnPc3	jiný
autistou	autistý	k2eAgFnSc4d1	autistý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
jinými	jiný	k2eAgMnPc7d1	jiný
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Intonace	intonace	k1gFnSc1	intonace
hlasu	hlas	k1gInSc2	hlas
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
autistů	autista	k1gMnPc2	autista
typicky	typicky	k6eAd1	typicky
velmi	velmi	k6eAd1	velmi
subtilní	subtilní	k2eAgFnSc1d1	subtilní
a	a	k8xC	a
normálnímu	normální	k2eAgMnSc3d1	normální
člověku	člověk	k1gMnSc3	člověk
připadá	připadat	k5eAaImIp3nS	připadat
značně	značně	k6eAd1	značně
monotónní	monotónní	k2eAgFnSc1d1	monotónní
až	až	k8xS	až
strojová	strojový	k2eAgFnSc1d1	strojová
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
autistické	autistický	k2eAgFnPc1d1	autistická
děti	dítě	k1gFnPc1	dítě
s	s	k7c7	s
dobrými	dobrý	k2eAgFnPc7d1	dobrá
řečovými	řečový	k2eAgFnPc7d1	řečová
schopnostmi	schopnost	k1gFnPc7	schopnost
nekomunikují	komunikovat	k5eNaImIp3nP	komunikovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
věkové	věkový	k2eAgFnSc6d1	věková
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mluví	mluvit	k5eAaImIp3nP	mluvit
jako	jako	k9	jako
malí	malý	k2eAgMnPc1d1	malý
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
normálního	normální	k2eAgMnSc4d1	normální
člověka	člověk	k1gMnSc4	člověk
obtížné	obtížný	k2eAgNnSc1d1	obtížné
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
autistické	autistický	k2eAgFnPc1d1	autistická
děti	dítě	k1gFnPc1	dítě
snaží	snažit	k5eAaImIp3nP	snažit
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgFnPc4	ten
proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
hlasitě	hlasitě	k6eAd1	hlasitě
dávat	dávat	k5eAaImF	dávat
najevo	najevo	k6eAd1	najevo
svou	svůj	k3xOyFgFnSc4	svůj
frustraci	frustrace	k1gFnSc4	frustrace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
samy	sám	k3xTgFnPc4	sám
vezmou	vzít	k5eAaPmIp3nP	vzít
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
komunikační	komunikační	k2eAgFnPc1d1	komunikační
bariéry	bariéra	k1gFnPc1	bariéra
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
velmi	velmi	k6eAd1	velmi
stresující	stresující	k2eAgMnPc4d1	stresující
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
úzkosti	úzkost	k1gFnSc3	úzkost
<g/>
,	,	kIx,	,
depresím	deprese	k1gFnPc3	deprese
nebo	nebo	k8xC	nebo
k	k	k7c3	k
sebepoškozujícímu	sebepoškozující	k2eAgNnSc3d1	sebepoškozující
jednání	jednání	k1gNnSc3	jednání
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
autisté	autista	k1gMnPc1	autista
vypadají	vypadat	k5eAaPmIp3nP	vypadat
fyzicky	fyzicky	k6eAd1	fyzicky
normálně	normálně	k6eAd1	normálně
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
svalovou	svalový	k2eAgFnSc4d1	svalová
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
odlišovat	odlišovat	k5eAaImF	odlišovat
neobvyklými	obvyklý	k2eNgInPc7d1	neobvyklý
repetitivními	repetitivní	k2eAgInPc7d1	repetitivní
(	(	kIx(	(
<g/>
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
opakujícími	opakující	k2eAgInPc7d1	opakující
<g/>
)	)	kIx)	)
pohyby	pohyb	k1gInPc7	pohyb
<g/>
,	,	kIx,	,
označovanými	označovaný	k2eAgInPc7d1	označovaný
jako	jako	k8xC	jako
sebestimulace	sebestimulace	k1gFnSc2	sebestimulace
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
nápadné	nápadný	k2eAgInPc4d1	nápadný
nebo	nebo	k8xC	nebo
subtilnější	subtilní	k2eAgInPc4d2	subtilnější
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autisté	autista	k1gMnPc1	autista
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
opakovaně	opakovaně	k6eAd1	opakovaně
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rukama	ruka	k1gFnPc7	ruka
nebo	nebo	k8xC	nebo
prsty	prst	k1gInPc7	prst
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
náhle	náhle	k6eAd1	náhle
ztuhnou	ztuhnout	k5eAaPmIp3nP	ztuhnout
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
děti	dítě	k1gFnPc1	dítě
mohou	moct	k5eAaImIp3nP	moct
trávit	trávit	k5eAaImF	trávit
hodiny	hodina	k1gFnPc4	hodina
seřazováním	seřazování	k1gNnSc7	seřazování
autíček	autíčko	k1gNnPc2	autíčko
a	a	k8xC	a
vláčků	vláček	k1gInPc2	vláček
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
místo	místo	k6eAd1	místo
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gMnPc4	on
používaly	používat	k5eAaImAgFnP	používat
k	k	k7c3	k
normální	normální	k2eAgFnSc3d1	normální
hře	hra	k1gFnSc3	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
náhodou	náhodou	k6eAd1	náhodou
pohne	pohnout	k5eAaPmIp3nS	pohnout
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
hračkami	hračka	k1gFnPc7	hračka
<g/>
,	,	kIx,	,
autistické	autistický	k2eAgNnSc1d1	autistické
dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
velice	velice	k6eAd1	velice
rozčílit	rozčílit	k5eAaPmF	rozčílit
<g/>
.	.	kIx.	.
</s>
<s>
Autistické	autistický	k2eAgFnPc1d1	autistická
děti	dítě	k1gFnPc1	dítě
často	často	k6eAd1	často
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
a	a	k8xC	a
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
absolutní	absolutní	k2eAgFnSc4d1	absolutní
stálost	stálost	k1gFnSc4	stálost
svého	svůj	k3xOyFgNnSc2	svůj
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
rutině	rutina	k1gFnSc6	rutina
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
rušivá	rušivý	k2eAgFnSc1d1	rušivá
<g/>
.	.	kIx.	.
</s>
<s>
Autisté	Autista	k1gMnPc1	Autista
často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
trvalé	trvalý	k2eAgInPc4d1	trvalý
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
nutkavé	nutkavý	k2eAgInPc4d1	nutkavý
záliby	zálib	k1gInPc4	zálib
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
posedlé	posedlý	k2eAgInPc1d1	posedlý
hledáním	hledání	k1gNnSc7	hledání
všeho	všecek	k3xTgNnSc2	všecek
o	o	k7c6	o
počítačích	počítač	k1gInPc6	počítač
<g/>
,	,	kIx,	,
majácích	maják	k1gInPc6	maják
nebo	nebo	k8xC	nebo
jízdních	jízdní	k2eAgInPc6d1	jízdní
řádech	řád	k1gInPc6	řád
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
projevují	projevovat	k5eAaImIp3nP	projevovat
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
jazyková	jazykový	k2eAgNnPc4d1	jazykové
<g/>
,	,	kIx,	,
matematická	matematický	k2eAgNnPc4d1	matematické
a	a	k8xC	a
vědecká	vědecký	k2eAgNnPc4d1	vědecké
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Repetitivní	repetitivní	k2eAgNnSc1d1	repetitivní
chování	chování	k1gNnSc1	chování
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vztahovat	vztahovat	k5eAaImF	vztahovat
i	i	k9	i
na	na	k7c4	na
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
denní	denní	k2eAgFnSc2d1	denní
rutiny	rutina	k1gFnSc2	rutina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
opakování	opakování	k1gNnSc1	opakování
jednoho	jeden	k4xCgNnSc2	jeden
slova	slovo	k1gNnSc2	slovo
nebo	nebo	k8xC	nebo
fráze	fráze	k1gFnSc2	fráze
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgFnPc1d1	historická
statistiky	statistika	k1gFnPc1	statistika
vypovídaly	vypovídat	k5eAaImAgFnP	vypovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
autisty	autista	k1gMnPc7	autista
je	být	k5eAaImIp3nS	být
až	až	k9	až
75	[number]	k4	75
<g/>
%	%	kIx~	%
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
stupněm	stupeň	k1gInSc7	stupeň
mentální	mentální	k2eAgFnSc2d1	mentální
retardace	retardace	k1gFnSc2	retardace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neplatí	platit	k5eNaImIp3nS	platit
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
nově	nově	k6eAd1	nově
diagnostikováno	diagnostikovat	k5eAaBmNgNnS	diagnostikovat
mnoho	mnoho	k4c1	mnoho
případů	případ	k1gInPc2	případ
Aspergerova	Aspergerův	k2eAgInSc2d1	Aspergerův
syndromu	syndrom	k1gInSc2	syndrom
(	(	kIx(	(
<g/>
i	i	k9	i
mezi	mezi	k7c7	mezi
dospělými	dospělí	k1gMnPc7	dospělí
<g/>
)	)	kIx)	)
a	a	k8xC	a
tito	tento	k3xDgMnPc1	tento
pacienti	pacient	k1gMnPc1	pacient
mají	mít	k5eAaImIp3nP	mít
inteligenci	inteligence	k1gFnSc4	inteligence
normální	normální	k2eAgFnSc4d1	normální
nebo	nebo	k8xC	nebo
vysokou	vysoký	k2eAgFnSc4d1	vysoká
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
geniality	genialita	k1gFnSc2	genialita
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vysoká	vysoký	k2eAgFnSc1d1	vysoká
inteligence	inteligence	k1gFnSc1	inteligence
však	však	k9	však
ani	ani	k9	ani
dospělým	dospělý	k2eAgMnPc3d1	dospělý
autistům	autista	k1gMnPc3	autista
nemusí	muset	k5eNaImIp3nP	muset
zajistit	zajistit	k5eAaPmF	zajistit
plnohodnotný	plnohodnotný	k2eAgInSc4d1	plnohodnotný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
potížím	potíž	k1gFnPc3	potíž
zejména	zejména	k9	zejména
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
komunikaci	komunikace	k1gFnSc6	komunikace
selhává	selhávat	k5eAaImIp3nS	selhávat
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
každodenních	každodenní	k2eAgFnPc6d1	každodenní
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
úspěšně	úspěšně	k6eAd1	úspěšně
začlení	začlenit	k5eAaPmIp3nP	začlenit
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejich	jejich	k3xOp3gInPc4	jejich
nedostatky	nedostatek	k1gInPc4	nedostatek
nejsou	být	k5eNaImIp3nP	být
nápadné	nápadný	k2eAgInPc4d1	nápadný
a	a	k8xC	a
kde	kde	k6eAd1	kde
naopak	naopak	k6eAd1	naopak
mohou	moct	k5eAaImIp3nP	moct
využít	využít	k5eAaPmF	využít
své	svůj	k3xOyFgFnPc4	svůj
přednosti	přednost	k1gFnPc4	přednost
(	(	kIx(	(
<g/>
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
detail	detail	k1gInSc4	detail
<g/>
,	,	kIx,	,
vytrvalost	vytrvalost	k1gFnSc1	vytrvalost
<g/>
,	,	kIx,	,
loajalita	loajalita	k1gFnSc1	loajalita
<g/>
,	,	kIx,	,
jednostranná	jednostranný	k2eAgFnSc1d1	jednostranná
hluboká	hluboký	k2eAgFnSc1d1	hluboká
specializace	specializace	k1gFnSc1	specializace
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autismus	autismus	k1gInSc1	autismus
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
širokou	široký	k2eAgFnSc4d1	široká
řadu	řada	k1gFnSc4	řada
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
od	od	k7c2	od
zjevné	zjevný	k2eAgFnSc2d1	zjevná
mentální	mentální	k2eAgFnSc2d1	mentální
retardace	retardace	k1gFnSc2	retardace
až	až	k9	až
po	po	k7c4	po
jedince	jedinec	k1gMnPc4	jedinec
se	s	k7c7	s
subtilními	subtilní	k2eAgInPc7d1	subtilní
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
ostatním	ostatní	k1gNnSc7	ostatní
nijak	nijak	k6eAd1	nijak
nápadní	nápadní	k2eAgFnSc1d1	nápadní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
dělení	dělení	k1gNnSc6	dělení
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
nevyřešených	vyřešený	k2eNgFnPc2d1	nevyřešená
kontroverzí	kontroverze	k1gFnPc2	kontroverze
<g/>
.	.	kIx.	.
</s>
<s>
Jemnější	jemný	k2eAgFnPc1d2	jemnější
formy	forma	k1gFnPc1	forma
autismu	autismus	k1gInSc2	autismus
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
neobjeveny	objeven	k2eNgFnPc1d1	neobjevena
a	a	k8xC	a
nediagnostikovány	diagnostikován	k2eNgFnPc1d1	nediagnostikována
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
psychiatrie	psychiatrie	k1gFnSc1	psychiatrie
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
autistické	autistický	k2eAgFnPc4d1	autistická
poruchy	porucha	k1gFnPc4	porucha
na	na	k7c4	na
Aspergerův	Aspergerův	k2eAgInSc4d1	Aspergerův
syndrom	syndrom	k1gInSc4	syndrom
a	a	k8xC	a
(	(	kIx(	(
<g/>
Kannerův	Kannerův	k2eAgInSc4d1	Kannerův
<g/>
)	)	kIx)	)
autismus	autismus	k1gInSc4	autismus
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
druhého	druhý	k4xOgInSc2	druhý
je	být	k5eAaImIp3nS	být
přítomno	přítomen	k2eAgNnSc1d1	přítomno
opoždění	opoždění	k1gNnSc1	opoždění
nebo	nebo	k8xC	nebo
abnormality	abnormalita	k1gFnPc1	abnormalita
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
tří	tři	k4xCgFnPc2	tři
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
před	před	k7c7	před
třetím	třetí	k4xOgInSc7	třetí
rokem	rok	k1gInSc7	rok
věku	věk	k1gInSc2	věk
dítěte	dítě	k1gNnSc2	dítě
<g/>
:	:	kIx,	:
sociální	sociální	k2eAgFnSc1d1	sociální
interakce	interakce	k1gFnSc1	interakce
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
,	,	kIx,	,
symbolická	symbolický	k2eAgFnSc1d1	symbolická
či	či	k8xC	či
imaginativní	imaginativní	k2eAgFnSc1d1	imaginativní
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
triáda	triáda	k1gFnSc1	triáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Aspergerova	Aspergerův	k2eAgInSc2d1	Aspergerův
syndromu	syndrom	k1gInSc2	syndrom
tato	tento	k3xDgNnPc1	tento
klinicky	klinicky	k6eAd1	klinicky
zjevná	zjevný	k2eAgNnPc1d1	zjevné
opoždění	opoždění	k1gNnPc1	opoždění
přítomná	přítomný	k2eAgNnPc1d1	přítomné
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
verze	verze	k1gFnSc1	verze
diagnostického	diagnostický	k2eAgInSc2d1	diagnostický
manuálu	manuál	k1gInSc2	manuál
DSM-	DSM-	k1gFnSc2	DSM-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
platná	platný	k2eAgFnSc1d1	platná
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
mění	měnit	k5eAaImIp3nP	měnit
definici	definice	k1gFnSc4	definice
poruch	porucha	k1gFnPc2	porucha
autistického	autistický	k2eAgNnSc2d1	autistické
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
de	de	k?	de
facto	facto	k1gNnSc1	facto
popírá	popírat	k5eAaImIp3nS	popírat
existenci	existence	k1gFnSc4	existence
Aspergerova	Aspergerův	k2eAgInSc2d1	Aspergerův
syndromu	syndrom	k1gInSc2	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
autistické	autistický	k2eAgFnPc4d1	autistická
poruchy	porucha	k1gFnPc4	porucha
na	na	k7c4	na
<g/>
:	:	kIx,	:
Dětský	dětský	k2eAgInSc4d1	dětský
autismus	autismus	k1gInSc4	autismus
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
84.0	[number]	k4	84.0
<g/>
)	)	kIx)	)
Atypický	atypický	k2eAgInSc1d1	atypický
autismus	autismus	k1gInSc1	autismus
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
84.1	[number]	k4	84.1
<g/>
)	)	kIx)	)
Rettův	Rettův	k2eAgInSc1d1	Rettův
syndrom	syndrom	k1gInSc1	syndrom
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
84.2	[number]	k4	84.2
<g/>
)	)	kIx)	)
Jiná	jiný	k2eAgFnSc1d1	jiná
dezintegrační	dezintegrační	k2eAgFnSc1d1	dezintegrační
porucha	porucha	k1gFnSc1	porucha
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
84.3	[number]	k4	84.3
<g/>
)	)	kIx)	)
Hyperaktivní	hyperaktivní	k2eAgFnSc1d1	hyperaktivní
porucha	porucha	k1gFnSc1	porucha
<g />
.	.	kIx.	.
</s>
<s>
sdružena	sdružen	k2eAgFnSc1d1	sdružena
s	s	k7c7	s
mentální	mentální	k2eAgFnSc7d1	mentální
retardací	retardace	k1gFnSc7	retardace
a	a	k8xC	a
stereotypními	stereotypní	k2eAgInPc7d1	stereotypní
pohyby	pohyb	k1gInPc7	pohyb
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
84.4	[number]	k4	84.4
<g/>
)	)	kIx)	)
Aspergerův	Aspergerův	k2eAgInSc1d1	Aspergerův
syndrom	syndrom	k1gInSc1	syndrom
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
84.5	[number]	k4	84.5
<g/>
)	)	kIx)	)
Jiné	jiný	k2eAgInPc1d1	jiný
pervazivní	pervazivní	k2eAgInPc1d1	pervazivní
vývojové	vývojový	k2eAgInPc1d1	vývojový
poruchy	poruch	k1gInPc1	poruch
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
84.8	[number]	k4	84.8
<g/>
)	)	kIx)	)
Pervazivní	Pervazivní	k2eAgFnSc1d1	Pervazivní
vývojová	vývojový	k2eAgFnSc1d1	vývojová
porucha	porucha	k1gFnSc1	porucha
nespecifikovaná	specifikovaný	k2eNgFnSc1d1	nespecifikovaná
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
84.9	[number]	k4	84.9
<g/>
)	)	kIx)	)
Jiní	jiný	k2eAgMnPc1d1	jiný
odborníci	odborník	k1gMnPc1	odborník
nevidí	vidět	k5eNaImIp3nP	vidět
žádnou	žádný	k3yNgFnSc4	žádný
jasnou	jasný	k2eAgFnSc4d1	jasná
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Aspergerovým	Aspergerův	k2eAgInSc7d1	Aspergerův
syndromem	syndrom	k1gInSc7	syndrom
<g/>
,	,	kIx,	,
autismem	autismus	k1gInSc7	autismus
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
poruchou	porucha	k1gFnSc7	porucha
smyslové	smyslový	k2eAgFnSc2d1	smyslová
integrace	integrace	k1gFnSc2	integrace
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
autismus	autismus	k1gInSc4	autismus
jako	jako	k8xC	jako
na	na	k7c4	na
spojité	spojitý	k2eAgNnSc4d1	spojité
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Autismus	autismus	k1gInSc1	autismus
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
vzácné	vzácný	k2eAgNnSc1d1	vzácné
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
četnosti	četnost	k1gFnSc6	četnost
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
případů	případ	k1gInPc2	případ
na	na	k7c4	na
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
použití	použití	k1gNnSc2	použití
méně	málo	k6eAd2	málo
přísných	přísný	k2eAgNnPc2d1	přísné
diagnostických	diagnostický	k2eAgNnPc2d1	diagnostické
vodítek	vodítko	k1gNnPc2	vodítko
a	a	k8xC	a
zařazení	zařazení	k1gNnSc2	zařazení
všech	všecek	k3xTgMnPc2	všecek
pacientů	pacient	k1gMnPc2	pacient
autistického	autistický	k2eAgNnSc2d1	autistické
spektra	spektrum	k1gNnSc2	spektrum
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
(	(	kIx(	(
<g/>
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
1	[number]	k4	1
případ	případ	k1gInSc1	případ
na	na	k7c4	na
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
školních	školní	k2eAgFnPc2d1	školní
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dětský	dětský	k2eAgInSc1d1	dětský
autismus	autismus	k1gInSc1	autismus
má	mít	k5eAaImIp3nS	mít
udávanou	udávaný	k2eAgFnSc4d1	udávaná
četnost	četnost	k1gFnSc4	četnost
výskytu	výskyt	k1gInSc2	výskyt
asi	asi	k9	asi
0,5	[number]	k4	0,5
%	%	kIx~	%
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
častější	častý	k2eAgInSc1d2	častější
u	u	k7c2	u
chlapců	chlapec	k1gMnPc2	chlapec
než	než	k8xS	než
u	u	k7c2	u
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
dívek	dívka	k1gFnPc2	dívka
asociováno	asociovat	k5eAaBmNgNnS	asociovat
s	s	k7c7	s
těžším	těžký	k2eAgInSc7d2	těžší
stupněm	stupeň	k1gInSc7	stupeň
postižení	postižení	k1gNnSc2	postižení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Aspergerova	Aspergerův	k2eAgInSc2d1	Aspergerův
syndromu	syndrom	k1gInSc2	syndrom
existují	existovat	k5eAaImIp3nP	existovat
jen	jen	k9	jen
hrubé	hrubý	k2eAgInPc1d1	hrubý
odhady	odhad	k1gInPc1	odhad
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
horní	horní	k2eAgFnPc4d1	horní
hranice	hranice	k1gFnPc4	hranice
výskytu	výskyt	k1gInSc2	výskyt
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
až	až	k9	až
1,5	[number]	k4	1,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižení	k1gNnSc1	postižení
narození	narození	k1gNnSc2	narození
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1980	[number]	k4	1980
v	v	k7c6	v
převážné	převážný	k2eAgFnSc6d1	převážná
většině	většina	k1gFnSc6	většina
nebyli	být	k5eNaImAgMnP	být
diagnostikováni	diagnostikován	k2eAgMnPc1d1	diagnostikován
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
chlapci	chlapec	k1gMnSc6	chlapec
<g/>
:	:	kIx,	:
<g/>
dívky	dívka	k1gFnSc2	dívka
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c4	mezi
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
socializací	socializace	k1gFnSc7	socializace
dívek	dívka	k1gFnPc2	dívka
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
větším	veliký	k2eAgInSc7d2	veliký
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
postižené	postižený	k2eAgFnPc1d1	postižená
dívky	dívka	k1gFnPc1	dívka
méně	málo	k6eAd2	málo
vyjádřené	vyjádřený	k2eAgInPc4d1	vyjádřený
příznaky	příznak	k1gInPc4	příznak
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
hůře	zle	k6eAd2	zle
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
hlášených	hlášený	k2eAgInPc2d1	hlášený
případů	případ	k1gInPc2	případ
z	z	k7c2	z
nejasných	jasný	k2eNgInPc2d1	nejasný
důvodů	důvod	k1gInPc2	důvod
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
úloze	úloha	k1gFnSc6	úloha
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
nárůstem	nárůst	k1gInSc7	nárůst
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
větší	veliký	k2eAgNnSc4d2	veliký
povědomí	povědomí	k1gNnSc4	povědomí
lékařů	lékař	k1gMnPc2	lékař
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
onemocnění	onemocnění	k1gNnSc6	onemocnění
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
rodiče	rodič	k1gMnPc1	rodič
svým	svůj	k3xOyFgFnPc3	svůj
dětem	dítě	k1gFnPc3	dítě
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
věnují	věnovat	k5eAaImIp3nP	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Srovnávací	srovnávací	k2eAgFnPc1d1	srovnávací
metastudie	metastudie	k1gFnPc1	metastudie
nicméně	nicméně	k8xC	nicméně
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
součet	součet	k1gInSc1	součet
těchto	tento	k3xDgInPc2	tento
vlivů	vliv	k1gInPc2	vliv
nevysvětluje	vysvětlovat	k5eNaImIp3nS	vysvětlovat
pandemický	pandemický	k2eAgInSc4d1	pandemický
nárůst	nárůst	k1gInSc4	nárůst
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
-	-	kIx~	-
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
americká	americký	k2eAgFnSc1d1	americká
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
CDC	CDC	kA	CDC
<g/>
,	,	kIx,	,
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
poměr	poměr	k1gInSc1	poměr
autistických	autistický	k2eAgFnPc2d1	autistická
dětí	dítě	k1gFnPc2	dítě
ke	k	k7c3	k
zdravým	zdravý	k1gMnPc3	zdravý
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
uvádí	uvádět	k5eAaImIp3nS	uvádět
poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
106	[number]	k4	106
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
150	[number]	k4	150
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc2	Francie
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
srovnání	srovnání	k1gNnSc1	srovnání
hodnot	hodnota	k1gFnPc2	hodnota
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgFnPc4d1	různá
metodiky	metodika	k1gFnPc4	metodika
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
,	,	kIx,	,
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
diagnóz	diagnóza	k1gFnPc2	diagnóza
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
různě	různě	k6eAd1	různě
nastavené	nastavený	k2eAgFnSc2d1	nastavená
věkové	věkový	k2eAgFnSc2d1	věková
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
překonanou	překonaný	k2eAgFnSc4d1	překonaná
teorie	teorie	k1gFnSc1	teorie
zastávaná	zastávaný	k2eAgFnSc1d1	zastávaná
až	až	k9	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
autismus	autismus	k1gInSc4	autismus
vzniká	vznikat	k5eAaImIp3nS	vznikat
vinou	vina	k1gFnSc7	vina
emocionálního	emocionální	k2eAgInSc2d1	emocionální
chladu	chlad	k1gInSc2	chlad
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
nedostatkem	nedostatek	k1gInSc7	nedostatek
lásky	láska	k1gFnSc2	láska
při	při	k7c6	při
výchově	výchova	k1gFnSc6	výchova
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
autistických	autistický	k2eAgNnPc2d1	autistické
jednovaječných	jednovaječný	k2eAgNnPc2d1	jednovaječné
dvojčat	dvojče	k1gNnPc2	dvojče
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
u	u	k7c2	u
monozygotních	monozygotní	k2eAgNnPc2d1	monozygotní
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
%	%	kIx~	%
u	u	k7c2	u
dizygotních	dizygotní	k2eAgFnPc2d1	dizygotní
<g/>
)	)	kIx)	)
vzniku	vznik	k1gInSc2	vznik
poruchy	porucha	k1gFnSc2	porucha
i	i	k9	i
u	u	k7c2	u
druhého	druhý	k4xOgNnSc2	druhý
dvojčete	dvojče	k1gNnSc2	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
možnou	možný	k2eAgFnSc4d1	možná
dysfunkci	dysfunkce	k1gFnSc4	dysfunkce
několika	několik	k4yIc2	několik
různých	různý	k2eAgInPc2d1	různý
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
účastní	účastnit	k5eAaImIp3nP	účastnit
vývoje	vývoj	k1gInSc2	vývoj
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
složitější	složitý	k2eAgInPc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rodin	rodina	k1gFnPc2	rodina
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
autistickým	autistický	k2eAgNnSc7d1	autistické
dítětem	dítě	k1gNnSc7	dítě
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
narození	narození	k1gNnSc2	narození
dalšího	další	k2eAgNnSc2d1	další
až	až	k9	až
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
poruchy	porucha	k1gFnPc1	porucha
autistického	autistický	k2eAgNnSc2d1	autistické
spektra	spektrum	k1gNnSc2	spektrum
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
narušený	narušený	k2eAgInSc1d1	narušený
mikrobiom	mikrobiom	k1gInSc1	mikrobiom
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
autistů	autist	k1gInPc2	autist
lze	lze	k6eAd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
určitá	určitý	k2eAgNnPc1d1	určité
poškození	poškození	k1gNnPc1	poškození
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
funkční	funkční	k2eAgFnPc1d1	funkční
poruchy	porucha	k1gFnPc1	porucha
levé	levý	k2eAgFnSc2d1	levá
poloviny	polovina	k1gFnSc2	polovina
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
abnormální	abnormální	k2eAgFnSc2d1	abnormální
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
mozkovém	mozkový	k2eAgInSc6d1	mozkový
kmeni	kmen	k1gInSc6	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nejednoznačné	jednoznačný	k2eNgNnSc1d1	nejednoznačné
a	a	k8xC	a
nelze	lze	k6eNd1	lze
s	s	k7c7	s
určitostí	určitost	k1gFnSc7	určitost
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
autismu	autismus	k1gInSc2	autismus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
následkem	následek	k1gInSc7	následek
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
doprovodným	doprovodný	k2eAgInSc7d1	doprovodný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
nádorů	nádor	k1gInPc2	nádor
zadní	zadní	k2eAgFnSc2d1	zadní
jámy	jáma	k1gFnSc2	jáma
lební	lební	k2eAgNnSc1d1	lební
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
naprosto	naprosto	k6eAd1	naprosto
překvapivým	překvapivý	k2eAgFnPc3d1	překvapivá
negativním	negativní	k2eAgFnPc3d1	negativní
změnám	změna	k1gFnPc3	změna
chování	chování	k1gNnSc2	chování
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
autismu	autismus	k1gInSc2	autismus
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
odeznívají	odeznívat	k5eAaImIp3nP	odeznívat
během	během	k7c2	během
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
týdnů	týden	k1gInPc2	týden
či	či	k8xC	či
měsíců	měsíc	k1gInPc2	měsíc
výjmečně	výjmečně	k6eAd1	výjmečně
jsou	být	k5eAaImIp3nP	být
trvalé	trvalý	k2eAgFnPc1d1	trvalá
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
nejpravdpodobněji	pravdpodobně	k6eAd3	pravdpodobně
otřesení	otřesení	k1gNnSc1	otřesení
nebo	nebo	k8xC	nebo
poškození	poškození	k1gNnSc1	poškození
síťovité-retikulární-formace	síťovitéetikulárníormace	k1gFnSc2	síťovité-retikulární-formace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
propojuje	propojovat	k5eAaImIp3nS	propojovat
všechny	všechen	k3xTgFnPc4	všechen
podkorové	podkorový	k2eAgFnPc4d1	podkorová
struktury	struktura	k1gFnPc4	struktura
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
živém	živý	k2eAgInSc6d1	živý
mozku	mozek	k1gInSc6	mozek
je	být	k5eAaImIp3nS	být
nelokalizovatelná	lokalizovatelný	k2eNgFnSc1d1	nelokalizovatelná
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
při	při	k7c6	při
naložení	naložení	k1gNnSc6	naložení
mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
mozku	mozek	k1gInSc2	mozek
do	do	k7c2	do
formaldehydu	formaldehyd	k1gInSc2	formaldehyd
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
vypreparovat	vypreparovat	k5eAaPmF	vypreparovat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
funkce	funkce	k1gFnSc1	funkce
není	být	k5eNaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
verifikována	verifikován	k2eAgFnSc1d1	verifikována
<g/>
,	,	kIx,	,
nerespektuje	respektovat	k5eNaImIp3nS	respektovat
totiž	totiž	k9	totiž
mozkové	mozkový	k2eAgFnPc4d1	mozková
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
jakousi	jakýsi	k3yIgFnSc4	jakýsi
rezervu	rezerva	k1gFnSc4	rezerva
podkorových	podkorový	k2eAgFnPc2d1	podkorová
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Neuroanatomové	Neuroanatom	k1gMnPc1	Neuroanatom
tuto	tento	k3xDgFnSc4	tento
formaci	formace	k1gFnSc4	formace
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
duši	duše	k1gFnSc4	duše
<g/>
"	"	kIx"	"
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
praktické	praktický	k2eAgFnPc1d1	praktická
zkušenosti	zkušenost	k1gFnPc1	zkušenost
toto	tento	k3xDgNnSc4	tento
podporují	podporovat	k5eAaImIp3nP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
útlaku	útlak	k1gInSc3	útlak
nebo	nebo	k8xC	nebo
poškození	poškození	k1gNnSc3	poškození
retikulární	retikulární	k2eAgFnSc2d1	retikulární
formace	formace	k1gFnSc2	formace
nemusí	muset	k5eNaImIp3nS	muset
dojít	dojít	k5eAaPmF	dojít
jen	jen	k9	jen
operačním	operační	k2eAgInSc7d1	operační
výkonem	výkon	k1gInSc7	výkon
nebo	nebo	k8xC	nebo
poraněním	poranění	k1gNnSc7	poranění
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
ji	on	k3xPp3gFnSc4	on
způsobit	způsobit	k5eAaPmF	způsobit
například	například	k6eAd1	například
i	i	k9	i
útlak	útlak	k1gInSc4	útlak
velkého	velký	k2eAgInSc2d1	velký
nádoru	nádor	k1gInSc2	nádor
nejčastěji	často	k6eAd3	často
zadní	zadní	k2eAgFnPc1d1	zadní
jámy	jáma	k1gFnPc1	jáma
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgFnSc1d1	aktivní
cysta	cysta	k1gFnSc1	cysta
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
například	například	k6eAd1	například
i	i	k9	i
z	z	k7c2	z
poúrazového	poúrazový	k2eAgNnSc2d1	poúrazové
krvácení	krvácení	k1gNnSc2	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
li	li	k8xS	li
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
retikulární	retikulární	k2eAgFnSc2d1	retikulární
formace	formace	k1gFnSc2	formace
u	u	k7c2	u
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
<g/>
například	například	k6eAd1	například
poúrazově	poúrazově	k6eAd1	poúrazově
nebo	nebo	k8xC	nebo
degenerativně	degenerativně	k6eAd1	degenerativně
<g/>
,	,	kIx,	,
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
tak	tak	k6eAd1	tak
intenzivním	intenzivní	k2eAgFnPc3d1	intenzivní
změnám	změna	k1gFnPc3	změna
a	a	k8xC	a
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
jedinec	jedinec	k1gMnSc1	jedinec
své	svůj	k3xOyFgFnPc4	svůj
citové	citový	k2eAgFnPc4d1	citová
změny	změna	k1gFnPc4	změna
může	moct	k5eAaImIp3nS	moct
zakrývat	zakrývat	k5eAaImF	zakrývat
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
ale	ale	k9	ale
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
k	k	k7c3	k
závislostem	závislost	k1gFnPc3	závislost
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
škodlivinách	škodlivina	k1gFnPc6	škodlivina
<g/>
.	.	kIx.	.
</s>
<s>
Magnetická	magnetický	k2eAgFnSc1d1	magnetická
rezonance	rezonance	k1gFnSc1	rezonance
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
autistů	autist	k1gInPc2	autist
chybí	chybit	k5eAaPmIp3nP	chybit
nervová	nervový	k2eAgNnPc1d1	nervové
propojení	propojení	k1gNnPc1	propojení
mezi	mezi	k7c7	mezi
různými	různý	k2eAgInPc7d1	různý
mozkovými	mozkový	k2eAgInPc7d1	mozkový
centry	centr	k1gInPc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
autismu	autismus	k1gInSc2	autismus
vinou	vinout	k5eAaImIp3nP	vinout
jejich	jejich	k3xOp3gFnPc1	jejich
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
koordinace	koordinace	k1gFnPc1	koordinace
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
odborník	odborník	k1gMnSc1	odborník
Simon	Simon	k1gMnSc1	Simon
Baron-Cohen	Baron-Cohen	k2eAgMnSc1d1	Baron-Cohen
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
autisté	autista	k1gMnPc1	autista
mají	mít	k5eAaImIp3nP	mít
extrémně	extrémně	k6eAd1	extrémně
mužský	mužský	k2eAgInSc4d1	mužský
typ	typ	k1gInSc4	typ
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hladinou	hladina	k1gFnSc7	hladina
testosteronu	testosteron	k1gInSc2	testosteron
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
matky	matka	k1gFnSc2	matka
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
typ	typ	k1gInSc1	typ
mozku	mozek	k1gInSc2	mozek
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
nedostatečným	dostatečný	k2eNgNnSc7d1	nedostatečné
propojením	propojení	k1gNnSc7	propojení
pravé	pravý	k2eAgFnSc2d1	pravá
a	a	k8xC	a
levé	levý	k2eAgFnSc2d1	levá
hemisféry	hemisféra	k1gFnSc2	hemisféra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
emoční	emoční	k2eAgFnSc2d1	emoční
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
hypotézy	hypotéza	k1gFnSc2	hypotéza
se	se	k3xPyFc4	se
autisté	autista	k1gMnPc1	autista
mohou	moct	k5eAaImIp3nP	moct
sice	sice	k8xC	sice
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
koncentrovat	koncentrovat	k5eAaBmF	koncentrovat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
podnět	podnět	k1gInSc4	podnět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohem	mnohem	k6eAd1	mnohem
hůře	zle	k6eAd2	zle
na	na	k7c4	na
víc	hodně	k6eAd2	hodně
věcí	věc	k1gFnPc2	věc
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
měnících	měnící	k2eAgFnPc2d1	měnící
sociálních	sociální	k2eAgFnPc2d1	sociální
situací	situace	k1gFnPc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
uveřejněna	uveřejnit	k5eAaPmNgFnS	uveřejnit
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
časopise	časopis	k1gInSc6	časopis
Autism	Autism	k1gInSc1	Autism
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
