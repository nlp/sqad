<s>
Termín	termín	k1gInSc1	termín
autismus	autismus	k1gInSc1	autismus
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
psychiatr	psychiatr	k1gMnSc1	psychiatr
Eugene	Eugen	k1gInSc5	Eugen
Bleuler	Bleuler	k1gMnSc1	Bleuler
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
popsal	popsat	k5eAaPmAgMnS	popsat
obtíže	obtíž	k1gFnPc4	obtíž
schizofreniků	schizofrenik	k1gMnPc2	schizofrenik
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
s	s	k7c7	s
druhými	druhý	k4xOgInPc7	druhý
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
