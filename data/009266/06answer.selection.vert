<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Ománu	Omán	k1gInSc2	Omán
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
zelený	zelený	k2eAgMnSc1d1	zelený
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
žerdi	žerď	k1gFnSc6	žerď
červený	červený	k2eAgInSc1d1	červený
svislý	svislý	k2eAgInSc1d1	svislý
pruh	pruh	k1gInSc1	pruh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
šířka	šířka	k1gFnSc1	šířka
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
polovině	polovina	k1gFnSc3	polovina
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
