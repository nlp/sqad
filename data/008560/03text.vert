<p>
<s>
Jacopo	Jacopa	k1gFnSc5	Jacopa
Peri	peri	k1gFnSc5	peri
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Il	Il	k1gMnPc1	Il
Zazzerino	Zazzerino	k1gNnSc1	Zazzerino
(	(	kIx(	(
<g/>
Dlouhovlasý	dlouhovlasý	k2eAgMnSc1d1	dlouhovlasý
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1561	[number]	k4	1561
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1633	[number]	k4	1633
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
na	na	k7c6	na
přechodu	přechod	k1gInSc6	přechod
od	od	k7c2	od
renesance	renesance	k1gFnSc2	renesance
k	k	k7c3	k
baroku	baroko	k1gNnSc3	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
první	první	k4xOgFnSc2	první
opery	opera	k1gFnSc2	opera
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Peri	peri	k1gFnSc3	peri
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
studoval	studovat	k5eAaImAgMnS	studovat
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
u	u	k7c2	u
Cristofana	Cristofan	k1gMnSc2	Cristofan
Malvezziho	Malvezzi	k1gMnSc2	Malvezzi
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
varhaník	varhaník	k1gMnSc1	varhaník
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
florentských	florentský	k2eAgInPc2d1	florentský
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
k	k	k7c3	k
medicejskému	medicejský	k2eAgInSc3d1	medicejský
dvoru	dvůr	k1gInSc3	dvůr
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
cembalo	cembalo	k1gNnSc4	cembalo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
jako	jako	k9	jako
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
rané	raný	k2eAgFnPc1d1	raná
práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
převážně	převážně	k6eAd1	převážně
madrigaly	madrigal	k1gInPc1	madrigal
a	a	k8xC	a
mezihry	mezihra	k1gFnPc1	mezihra
a	a	k8xC	a
scénická	scénický	k2eAgFnSc1d1	scénická
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
divadelním	divadelní	k2eAgNnPc3d1	divadelní
představením	představení	k1gNnPc3	představení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
se	s	k7c7	s
skladatelem	skladatel	k1gMnSc7	skladatel
Jacopo	Jacopa	k1gFnSc5	Jacopa
Corsim	Corsima	k1gFnPc2	Corsima
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hlubokém	hluboký	k2eAgInSc6d1	hluboký
úpadku	úpadek	k1gInSc6	úpadek
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
klasickým	klasický	k2eAgNnSc7d1	klasické
uměním	umění	k1gNnSc7	umění
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
Římanů	Říman	k1gMnPc2	Říman
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
vzkřísit	vzkřísit	k5eAaPmF	vzkřísit
antickou	antický	k2eAgFnSc4d1	antická
tragedii	tragedie	k1gFnSc4	tragedie
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
jí	on	k3xPp3gFnSc3	on
rozuměli	rozumět	k5eAaImAgMnP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sdružení	sdružení	k1gNnSc3	sdružení
umělců	umělec	k1gMnPc2	umělec
Florentská	florentský	k2eAgFnSc1d1	florentská
camerata	camerata	k1gFnSc1	camerata
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
první	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
opouštějící	opouštějící	k2eAgFnSc4d1	opouštějící
renesanční	renesanční	k2eAgFnSc4d1	renesanční
polyfonii	polyfonie	k1gFnSc4	polyfonie
<g/>
,	,	kIx,	,
skladby	skladba	k1gFnPc4	skladba
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
zpěv	zpěv	k1gInSc4	zpěv
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
generálního	generální	k2eAgInSc2d1	generální
basu	bas	k1gInSc2	bas
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
recitativy	recitativ	k1gInPc1	recitativ
a	a	k8xC	a
árie	árie	k1gFnPc1	árie
<g/>
.	.	kIx.	.
</s>
<s>
Peri	peri	k1gFnSc4	peri
a	a	k8xC	a
Corsi	Corse	k1gFnSc4	Corse
přizvali	přizvat	k5eAaPmAgMnP	přizvat
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
básníka	básník	k1gMnSc2	básník
Ottavia	Ottavius	k1gMnSc2	Ottavius
Rinucciho	Rinucci	k1gMnSc2	Rinucci
a	a	k8xC	a
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
text	text	k1gInSc4	text
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
první	první	k4xOgNnSc1	první
dílo	dílo	k1gNnSc1	dílo
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnSc2d1	nová
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
Dafne	Dafn	k1gMnSc5	Dafn
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
partitura	partitura	k1gFnSc1	partitura
je	být	k5eAaImIp3nS	být
bohužel	bohužel	k6eAd1	bohužel
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oba	dva	k4xCgMnPc1	dva
skladatelé	skladatel	k1gMnPc1	skladatel
dále	daleko	k6eAd2	daleko
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
na	na	k7c6	na
opeře	opera	k1gFnSc6	opera
Euridice	Euridice	k1gFnSc2	Euridice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1600	[number]	k4	1600
v	v	k7c6	v
Palazzo	Palazza	k1gFnSc5	Palazza
Pitti	Pitť	k1gFnPc1	Pitť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
opery	opera	k1gFnSc2	opera
Dafne	Dafn	k1gInSc5	Dafn
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
opera	opera	k1gFnSc1	opera
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
dochovala	dochovat	k5eAaPmAgFnS	dochovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
bývají	bývat	k5eAaImIp3nP	bývat
prováděny	provádět	k5eAaImNgInP	provádět
pouze	pouze	k6eAd1	pouze
výňatky	výňatek	k1gInPc1	výňatek
jako	jako	k8xS	jako
historická	historický	k2eAgFnSc1d1	historická
kuriozita	kuriozita	k1gFnSc1	kuriozita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Peri	peri	k1gFnSc4	peri
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
ještě	ještě	k9	ještě
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
oper	opera	k1gFnPc2	opera
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
skladateli	skladatel	k1gMnPc7	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
měla	mít	k5eAaImAgFnS	mít
opera	opera	k1gFnSc1	opera
Euridice	Euridice	k1gFnSc1	Euridice
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
nedostavil	dostavit	k5eNaPmAgMnS	dostavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1601	[number]	k4	1601
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kapelníkem	kapelník	k1gMnSc7	kapelník
((	((	k?	((
<g/>
maestro	maestro	k1gMnSc1	maestro
di	di	k?	di
cappella	cappella	k1gMnSc1	cappella
<g/>
)	)	kIx)	)
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Ferrary	Ferrara	k1gFnSc2	Ferrara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1609	[number]	k4	1609
publikoval	publikovat	k5eAaBmAgMnS	publikovat
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
své	svůj	k3xOyFgNnSc1	svůj
dílo	dílo	k1gNnSc1	dílo
Varie	vari	k1gMnPc4	vari
musiche	musiche	k1gFnSc4	musiche
a	a	k8xC	a
urns	urns	k1gInSc4	urns
<g/>
,	,	kIx,	,
due	due	k?	due
e	e	k0	e
tre	tre	k?	tre
voci	voc	k1gMnSc3	voc
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInPc6	jeho
dalších	další	k2eAgInPc6d1	další
osudech	osud	k1gInPc6	osud
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tamtéž	tamtéž	k6eAd1	tamtéž
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
Novella	Novell	k1gMnSc2	Novell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
dnešní	dnešní	k2eAgNnSc4d1	dnešní
jeviště	jeviště	k1gNnSc4	jeviště
Periho	Peri	k1gMnSc2	Peri
opery	opera	k1gFnSc2	opera
nejsou	být	k5eNaImIp3nP	být
často	často	k6eAd1	často
uváděny	uvádět	k5eAaImNgFnP	uvádět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
přece	přece	k9	přece
jen	jen	k9	jen
poněkud	poněkud	k6eAd1	poněkud
staromódní	staromódní	k2eAgFnSc1d1	staromódní
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
mladšími	mladý	k2eAgMnPc7d2	mladší
skladateli	skladatel	k1gMnPc7	skladatel
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
Claudio	Claudio	k1gMnSc1	Claudio
Monteverdi	Monteverd	k1gMnPc1	Monteverd
<g/>
.	.	kIx.	.
</s>
<s>
Periho	Perize	k6eAd1	Perize
zásluha	zásluha	k1gFnSc1	zásluha
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
operní	operní	k2eAgFnSc2d1	operní
formy	forma	k1gFnSc2	forma
je	být	k5eAaImIp3nS	být
nesporná	sporný	k2eNgFnSc1d1	nesporná
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
mělo	mít	k5eAaImAgNnS	mít
jeho	jeho	k3xOp3gFnSc4	jeho
dílo	dílo	k1gNnSc1	dílo
na	na	k7c4	na
následovníky	následovník	k1gMnPc4	následovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
oper	opera	k1gFnPc2	opera
Peri	peri	k1gFnPc2	peri
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
mnoho	mnoho	k6eAd1	mnoho
komorní	komorní	k2eAgFnSc2d1	komorní
a	a	k8xC	a
vokální	vokální	k2eAgFnSc2d1	vokální
hudby	hudba	k1gFnSc2	hudba
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
medicejského	medicejský	k2eAgInSc2d1	medicejský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Opery	opera	k1gFnSc2	opera
==	==	k?	==
</s>
</p>
<p>
<s>
Dafne	Dafnout	k5eAaImIp3nS	Dafnout
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
Corsi	Corse	k1gFnSc4	Corse
<g/>
,	,	kIx,	,
1598	[number]	k4	1598
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Euridice	Euridice	k1gFnSc1	Euridice
(	(	kIx(	(
<g/>
6.10	[number]	k4	6.10
<g/>
.1600	.1600	k4	.1600
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tetide	Tetid	k1gMnSc5	Tetid
(	(	kIx(	(
<g/>
1608	[number]	k4	1608
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Adone	Adonout	k5eAaPmIp3nS	Adonout
(	(	kIx(	(
<g/>
1611	[number]	k4	1611
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
liberazione	liberazion	k1gInSc5	liberazion
di	di	k?	di
Tirreno	Tirrena	k1gFnSc5	Tirrena
e	e	k0	e
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Armea	Armea	k1gMnSc1	Armea
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1617	[number]	k4	1617
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lo	Lo	k?	Lo
sposalizio	sposalizio	k1gMnSc1	sposalizio
di	di	k?	di
Medoro	Medora	k1gFnSc5	Medora
e	e	k0	e
Angelica	Angelicum	k1gNnPc1	Angelicum
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1619	[number]	k4	1619
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Flora	Flora	k1gFnSc1	Flora
<g/>
,	,	kIx,	,
ovvero	ovvero	k1gNnSc1	ovvero
Il	Il	k1gFnPc1	Il
natal	natal	k1gMnSc1	natal
di	di	k?	di
Fiori	Fior	k1gFnSc2	Fior
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1628	[number]	k4	1628
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Iole	Iole	k1gFnSc1	Iole
ed	ed	k?	ed
Ercole	Ercole	k1gFnSc1	Ercole
(	(	kIx(	(
<g/>
1628	[number]	k4	1628
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jacopo	Jacopa	k1gFnSc5	Jacopa
Peri	peri	k1gFnSc5	peri
<g/>
:	:	kIx,	:
Ai	Ai	k1gMnSc5	Ai
Lettori	Lettor	k1gMnSc5	Lettor
<g/>
.	.	kIx.	.
</s>
<s>
Introduzione	Introduzion	k1gInSc5	Introduzion
a	a	k8xC	a
'	'	kIx"	'
<g/>
Le	Le	k1gFnSc1	Le
Musiche	Musiche	k1gNnSc2	Musiche
sopra	sopro	k1gNnSc2	sopro
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Euridice	Euridice	k1gFnSc1	Euridice
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
revisione	revision	k1gInSc5	revision
e	e	k0	e
note	note	k1gNnPc7	note
di	di	k?	di
Valter	Valter	k1gMnSc1	Valter
Carignano	Carignana	k1gFnSc5	Carignana
</s>
</p>
<p>
<s>
Jacopo	Jacopa	k1gFnSc5	Jacopa
Peri	peri	k1gFnSc5	peri
<g/>
:	:	kIx,	:
Le	Le	k1gMnSc3	Le
Musiche	Musiche	k1gFnSc4	Musiche
sopra	sopr	k1gMnSc2	sopr
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Euridice	Euridice	k1gFnSc1	Euridice
<g/>
.	.	kIx.	.
</s>
<s>
Revisione	Revision	k1gInSc5	Revision
e	e	k0	e
Note	Note	k1gNnPc7	Note
di	di	k?	di
Valter	Valter	k1gMnSc1	Valter
Carignano	Carignana	k1gFnSc5	Carignana
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Opera	opera	k1gFnSc1	opera
Rinata	Rinata	k1gFnSc1	Rinata
<g/>
,	,	kIx,	,
Torino	Torino	k1gNnSc1	Torino
</s>
</p>
<p>
<s>
The	The	k?	The
New	New	k1gFnSc1	New
Grove	Groev	k1gFnSc2	Groev
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
Music	Music	k1gMnSc1	Music
and	and	k?	and
Musicians	Musicians	k1gInSc1	Musicians
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Stanley	Stanle	k2eAgFnPc1d1	Stanle
Sadie	Sadie	k1gFnPc1	Sadie
<g/>
.	.	kIx.	.
20	[number]	k4	20
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
Macmillan	Macmillan	k1gMnSc1	Macmillan
Publishers	Publishersa	k1gFnPc2	Publishersa
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1-56159-174-2	[number]	k4	1-56159-174-2
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jacopo	Jacopa	k1gFnSc5	Jacopa
Peri	peri	k1gFnPc6	peri
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dělo	k1gNnPc2	dělo
od	od	k7c2	od
Jacopo	Jacopa	k1gFnSc5	Jacopa
Periho	Peri	k1gMnSc2	Peri
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
oper	opera	k1gFnPc2	opera
dle	dle	k7c2	dle
univerzity	univerzita	k1gFnSc2	univerzita
Stanford	Stanforda	k1gFnPc2	Stanforda
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
na	na	k7c4	na
Allmusic	Allmusic	k1gMnSc1	Allmusic
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
</s>
</p>
