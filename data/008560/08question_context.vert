<s>
Jacopo	Jacopa	k1gFnSc5	Jacopa
Peri	peri	k1gFnSc5	peri
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Il	Il	k1gMnPc1	Il
Zazzerino	Zazzerino	k1gNnSc1	Zazzerino
(	(	kIx(	(
<g/>
Dlouhovlasý	dlouhovlasý	k2eAgMnSc1d1	dlouhovlasý
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1561	[number]	k4	1561
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1633	[number]	k4	1633
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
na	na	k7c6	na
přechodu	přechod	k1gInSc6	přechod
od	od	k7c2	od
renesance	renesance	k1gFnSc2	renesance
k	k	k7c3	k
baroku	baroko	k1gNnSc3	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
první	první	k4xOgFnSc2	první
opery	opera	k1gFnSc2	opera
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
