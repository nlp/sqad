<s>
Meteorologie	meteorologie	k1gFnSc1	meteorologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Studuje	studovat	k5eAaImIp3nS	studovat
její	její	k3xOp3gNnSc4	její
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
jevy	jev	k1gInPc4	jev
a	a	k8xC	a
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
probíhající	probíhající	k2eAgFnSc6d1	probíhající
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologie	meteorologie	k1gFnSc1	meteorologie
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
část	část	k1gFnSc4	část
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
fyzika	fyzika	k1gFnSc1	fyzika
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
"	"	kIx"	"
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyučována	vyučovat	k5eAaImNgFnS	vyučovat
na	na	k7c6	na
matematicko-fyzikálních	matematickoyzikální	k2eAgFnPc6d1	matematicko-fyzikální
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
univerzitách	univerzita	k1gFnPc6	univerzita
je	být	k5eAaImIp3nS	být
meteorologie	meteorologie	k1gFnSc1	meteorologie
často	často	k6eAd1	často
vyučována	vyučovat	k5eAaImNgFnS	vyučovat
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
věd	věda	k1gFnPc2	věda
o	o	k7c4	o
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
souvislost	souvislost	k1gFnSc4	souvislost
atmosféry	atmosféra	k1gFnSc2	atmosféra
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
krajinnými	krajinný	k2eAgFnPc7d1	krajinná
sférami	sféra	k1gFnPc7	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Poznatky	poznatek	k1gInPc1	poznatek
meteorologie	meteorologie	k1gFnSc2	meteorologie
jsou	být	k5eAaImIp3nP	být
nezbytné	zbytný	k2eNgFnPc1d1	zbytný
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
odvětvích	odvětví	k1gNnPc6	odvětví
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
–	–	k?	–
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
vojenství	vojenství	k1gNnSc1	vojenství
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
meteorologií	meteorologie	k1gFnSc7	meteorologie
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
hydrologie	hydrologie	k1gFnSc1	hydrologie
<g/>
.	.	kIx.	.
</s>
<s>
Tematickými	tematický	k2eAgInPc7d1	tematický
okruhy	okruh	k1gInPc7	okruh
meteorologie	meteorologie	k1gFnSc2	meteorologie
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
složení	složení	k1gNnSc1	složení
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
oběh	oběh	k1gInSc4	oběh
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
tepelný	tepelný	k2eAgInSc4d1	tepelný
režim	režim	k1gInSc4	režim
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
včetně	včetně	k7c2	včetně
radiačních	radiační	k2eAgInPc2d1	radiační
procesů	proces	k1gInPc2	proces
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
mechanismů	mechanismus	k1gInPc2	mechanismus
neradiační	radiační	k2eNgFnSc2d1	radiační
výměny	výměna	k1gFnSc2	výměna
mezi	mezi	k7c7	mezi
atmosférou	atmosféra	k1gFnSc7	atmosféra
a	a	k8xC	a
aktivním	aktivní	k2eAgInSc7d1	aktivní
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
samé	samý	k3xTgNnSc1	samý
<g/>
,	,	kIx,	,
oběh	oběh	k1gInSc4	oběh
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
fázové	fázový	k2eAgFnPc1d1	fázová
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
v	v	k7c6	v
interakci	interakce	k1gFnSc6	interakce
se	s	k7c7	s
zemským	zemský	k2eAgInSc7d1	zemský
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
atmosférické	atmosférický	k2eAgInPc4d1	atmosférický
pohyby	pohyb	k1gInPc4	pohyb
−	−	k?	−
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
cirkulaci	cirkulace	k1gFnSc4	cirkulace
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc2	její
složky	složka	k1gFnSc2	složka
a	a	k8xC	a
místní	místní	k2eAgFnSc2d1	místní
cirkulace	cirkulace	k1gFnSc2	cirkulace
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgNnSc1d1	elektrické
pole	pole	k1gNnSc1	pole
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
optické	optický	k2eAgInPc4d1	optický
a	a	k8xC	a
akustické	akustický	k2eAgInPc4d1	akustický
jevy	jev	k1gInPc4	jev
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologie	meteorologie	k1gFnSc1	meteorologie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
především	především	k9	především
troposférou	troposféra	k1gFnSc7	troposféra
a	a	k8xC	a
stratosférou	stratosféra	k1gFnSc7	stratosféra
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tyto	tento	k3xDgFnPc1	tento
části	část	k1gFnPc1	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
jsou	být	k5eAaImIp3nP	být
nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
předpovědi	předpověď	k1gFnSc2	předpověď
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
vysoké	vysoký	k2eAgFnSc2d1	vysoká
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
aeronomie	aeronomie	k1gFnSc1	aeronomie
<g/>
.	.	kIx.	.
</s>
<s>
Předpověď	předpověď	k1gFnSc1	předpověď
počasí	počasí	k1gNnSc2	počasí
je	být	k5eAaImIp3nS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
díky	díky	k7c3	díky
matematickým	matematický	k2eAgInPc3d1	matematický
meteorologickým	meteorologický	k2eAgInPc3d1	meteorologický
modelům	model	k1gInPc3	model
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
modely	model	k1gInPc1	model
<g/>
:	:	kIx,	:
ALADIN	ALADIN	kA	ALADIN
používaný	používaný	k2eAgInSc4d1	používaný
ČHMÚ	ČHMÚ	kA	ČHMÚ
MEDARD	Medard	k1gMnSc1	Medard
používaný	používaný	k2eAgMnSc1d1	používaný
AV	AV	kA	AV
ČR	ČR	kA	ČR
Pozvolna	pozvolna	k6eAd1	pozvolna
se	se	k3xPyFc4	se
osamostatňujícím	osamostatňující	k2eAgMnSc7d1	osamostatňující
se	se	k3xPyFc4	se
oborem	obor	k1gInSc7	obor
meteorologie	meteorologie	k1gFnSc2	meteorologie
je	být	k5eAaImIp3nS	být
klimatologie	klimatologie	k1gFnSc1	klimatologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
správněji	správně	k6eAd2	správně
zařazována	zařazovat	k5eAaImNgFnS	zařazovat
mezi	mezi	k7c4	mezi
vědy	věda	k1gFnPc4	věda
geografické	geografický	k2eAgFnPc4d1	geografická
<g/>
.	.	kIx.	.
</s>
<s>
Klimatologie	klimatologie	k1gFnSc1	klimatologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
podnebích	podnebí	k1gNnPc6	podnebí
(	(	kIx(	(
<g/>
klimatech	klima	k1gNnPc6	klima
<g/>
)	)	kIx)	)
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
o	o	k7c6	o
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
příčinách	příčina	k1gFnPc6	příčina
jejich	jejich	k3xOp3gNnSc2	jejich
utváření	utváření	k1gNnSc2	utváření
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
působení	působení	k1gNnSc4	působení
klimatu	klima	k1gNnSc2	klima
na	na	k7c4	na
objekty	objekt	k1gInPc4	objekt
činnosti	činnost	k1gFnSc2	činnost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
na	na	k7c4	na
samotného	samotný	k2eAgMnSc4d1	samotný
člověka	člověk	k1gMnSc4	člověk
i	i	k8xC	i
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
přírodní	přírodní	k2eAgInPc4d1	přírodní
děje	děj	k1gInPc4	děj
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
klimatologie	klimatologie	k1gFnSc2	klimatologie
je	být	k5eAaImIp3nS	být
studovat	studovat	k5eAaImF	studovat
obecné	obecný	k2eAgFnSc3d1	obecná
zákonitosti	zákonitost	k1gFnSc3	zákonitost
klimatických	klimatický	k2eAgInPc2d1	klimatický
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
utváření	utváření	k1gNnSc1	utváření
zemského	zemský	k2eAgNnSc2d1	zemské
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
změny	změna	k1gFnSc2	změna
a	a	k8xC	a
kolísání	kolísání	k1gNnSc2	kolísání
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
využití	využití	k1gNnSc2	využití
poznatků	poznatek	k1gInPc2	poznatek
pro	pro	k7c4	pro
předpovídání	předpovídání	k1gNnSc4	předpovídání
a	a	k8xC	a
melioraci	meliorace	k1gFnSc4	meliorace
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
5000	[number]	k4	5000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
revoluce	revoluce	k1gFnSc1	revoluce
–	–	k?	–
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
závislým	závislý	k2eAgNnSc7d1	závislé
na	na	k7c4	na
počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
<g />
.	.	kIx.	.
</s>
<s>
st.	st.	kA	st.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
vyvěšována	vyvěšován	k2eAgFnSc1d1	vyvěšována
tzv.	tzv.	kA	tzv.
parapegmata	parapegma	k1gNnPc4	parapegma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
i	i	k9	i
informace	informace	k1gFnSc1	informace
meteorologického	meteorologický	k2eAgInSc2d1	meteorologický
rázu	ráz	k1gInSc2	ráz
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
o	o	k7c6	o
proudění	proudění	k1gNnSc6	proudění
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
věž	věž	k1gFnSc1	věž
větrů	vítr	k1gInPc2	vítr
<g/>
"	"	kIx"	"
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
Platón	platón	k1gInSc1	platón
meteora	meteora	k1gFnSc1	meteora
=	=	kIx~	=
věci	věc	k1gFnPc1	věc
nadzemské	nadzemský	k2eAgFnPc1d1	nadzemská
kolem	kolem	k7c2	kolem
340	[number]	k4	340
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
–	–	k?	–
Meteorologica	Meteorologica	k1gMnSc1	Meteorologica
64	[number]	k4	64
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
Seneca	Seneca	k1gMnSc1	Seneca
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
znečištěném	znečištěný	k2eAgInSc6d1	znečištěný
vzduchu	vzduch	k1gInSc6	vzduch
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologie	meteorologie	k1gFnSc1	meteorologie
je	být	k5eAaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
astronomií	astronomie	k1gFnSc7	astronomie
a	a	k8xC	a
astrologií	astrologie	k1gFnSc7	astrologie
(	(	kIx(	(
<g/>
astrometeorologie	astrometeorologie	k1gFnSc1	astrometeorologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
Byla	být	k5eAaImAgFnS	být
doložena	doložen	k2eAgFnSc1d1	doložena
znalost	znalost	k1gFnSc1	znalost
pasátů	pasát	k1gInPc2	pasát
<g/>
.	.	kIx.	.
okolo	okolo	k7c2	okolo
1500	[number]	k4	1500
–	–	k?	–
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
hygrometr	hygrometr	k1gInSc1	hygrometr
<g/>
.	.	kIx.	.
1606	[number]	k4	1606
<g/>
–	–	k?	–
<g/>
1607	[number]	k4	1607
–	–	k?	–
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnPc1	Galile
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
žáci	žák	k1gMnPc1	žák
konstruují	konstruovat	k5eAaImIp3nP	konstruovat
kapalinové	kapalinový	k2eAgInPc4d1	kapalinový
teploměry	teploměr	k1gInPc4	teploměr
<g/>
.	.	kIx.	.
1644	[number]	k4	1644
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Torricelli	Torricelle	k1gFnSc3	Torricelle
sestrojuje	sestrojovat	k5eAaImIp3nS	sestrojovat
rtuťový	rtuťový	k2eAgInSc1d1	rtuťový
tlakoměr	tlakoměr	k1gInSc1	tlakoměr
<g/>
.	.	kIx.	.
1657	[number]	k4	1657
<g/>
–	–	k?	–
<g/>
1667	[number]	k4	1667
–	–	k?	–
Accademia	Accademia	k1gFnSc1	Accademia
del	del	k?	del
Cimento	Cimento	k1gNnSc1	Cimento
založená	založený	k2eAgFnSc1d1	založená
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
dává	dávat	k5eAaImIp3nS	dávat
popud	popud	k1gInSc4	popud
k	k	k7c3	k
prvním	první	k4xOgNnPc3	první
systematickým	systematický	k2eAgNnPc3d1	systematické
meteorologickým	meteorologický	k2eAgNnPc3d1	meteorologické
pozorováním	pozorování	k1gNnPc3	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
krátké	krátký	k2eAgNnSc4d1	krátké
trvání	trvání	k1gNnSc4	trvání
Akademie	akademie	k1gFnSc2	akademie
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
meteorologická	meteorologický	k2eAgNnPc4d1	meteorologické
měření	měření	k1gNnPc4	měření
a	a	k8xC	a
pozorování	pozorování	k1gNnSc4	pozorování
již	již	k6eAd1	již
neustal	ustat	k5eNaPmAgInS	ustat
<g/>
.	.	kIx.	.
1667	[number]	k4	1667
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
Hooke	Hooke	k1gFnSc3	Hooke
sestrojuje	sestrojovat	k5eAaImIp3nS	sestrojovat
anemometr	anemometr	k1gInSc1	anemometr
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
1686	[number]	k4	1686
–	–	k?	–
Edmund	Edmund	k1gMnSc1	Edmund
Halley	Halley	k1gMnSc1	Halley
zmapoval	zmapovat	k5eAaPmAgMnS	zmapovat
pasáty	pasát	k1gInPc4	pasát
<g/>
,	,	kIx,	,
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
změny	změna	k1gFnPc1	změna
a	a	k8xC	a
procesy	proces	k1gInPc1	proces
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
jsou	být	k5eAaImIp3nP	být
řízeny	řídit	k5eAaImNgFnP	řídit
slunečním	sluneční	k2eAgNnSc7d1	sluneční
teplem	teplo	k1gNnSc7	teplo
<g/>
,	,	kIx,	,
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
Pascalovy	Pascalův	k2eAgInPc4d1	Pascalův
objevy	objev	k1gInPc4	objev
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
meteorologie	meteorologie	k1gFnSc1	meteorologie
ještě	ještě	k9	ještě
součástí	součást	k1gFnSc7	součást
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
1735	[number]	k4	1735
–	–	k?	–
George	Georg	k1gMnSc2	Georg
Hadley	Hadlea	k1gMnSc2	Hadlea
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
dává	dávat	k5eAaImIp3nS	dávat
do	do	k7c2	do
spojení	spojení	k1gNnSc2	spojení
stáčení	stáčení	k1gNnSc2	stáčení
pasátů	pasát	k1gInPc2	pasát
a	a	k8xC	a
rotaci	rotace	k1gFnSc4	rotace
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
mechanismus	mechanismus	k1gInSc1	mechanismus
popsal	popsat	k5eAaPmAgInS	popsat
nesprávně	správně	k6eNd1	správně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
cirkulační	cirkulační	k2eAgFnSc4d1	cirkulační
buňku	buňka	k1gFnSc4	buňka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
Hadleyho	Hadley	k1gMnSc2	Hadley
buňka	buňka	k1gFnSc1	buňka
<g/>
.	.	kIx.	.
1780	[number]	k4	1780
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
V	v	k7c6	v
Manheimu	Manheim	k1gInSc6	Manheim
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
Societa	societa	k1gFnSc1	societa
meteorologica	meteorologicus	k1gMnSc2	meteorologicus
palatina	palatin	k1gMnSc2	palatin
<g/>
,	,	kIx,	,
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
společnost	společnost	k1gFnSc1	společnost
s	s	k7c7	s
39	[number]	k4	39
meteorologickými	meteorologický	k2eAgFnPc7d1	meteorologická
stanicemi	stanice	k1gFnPc7	stanice
<g/>
.	.	kIx.	.
1780	[number]	k4	1780
–	–	k?	–
Horace-Bénédict	Horace-Bénédict	k1gMnSc1	Horace-Bénédict
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
sestrojuje	sestrojovat	k5eAaImIp3nS	sestrojovat
vlasový	vlasový	k2eAgInSc4d1	vlasový
vlhkoměr	vlhkoměr	k1gInSc4	vlhkoměr
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
.	.	kIx.	.
1792	[number]	k4	1792
–	–	k?	–
Societa	societa	k1gFnSc1	societa
meteorologica	meteorologic	k1gInSc2	meteorologic
palatina	palatin	k1gMnSc2	palatin
publikuje	publikovat	k5eAaBmIp3nS	publikovat
výsledky	výsledek	k1gInPc4	výsledek
měření	měření	k1gNnPc2	měření
a	a	k8xC	a
pozorování	pozorování	k1gNnPc2	pozorování
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1780	[number]	k4	1780
<g/>
–	–	k?	–
<g/>
1792	[number]	k4	1792
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
formulaci	formulace	k1gFnSc4	formulace
prvních	první	k4xOgFnPc2	první
meteorologických	meteorologický	k2eAgFnPc2d1	meteorologická
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikají	vznikat	k5eAaImIp3nP	vznikat
sítě	síť	k1gFnPc1	síť
meteorologických	meteorologický	k2eAgFnPc2d1	meteorologická
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
první	první	k4xOgFnSc2	první
meteorologické	meteorologický	k2eAgFnSc2d1	meteorologická
ústavy	ústava	k1gFnSc2	ústava
–	–	k?	–
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
geofyzikální	geofyzikální	k2eAgFnSc1d1	geofyzikální
observatoř	observatoř	k1gFnSc1	observatoř
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
meteorologii	meteorologie	k1gFnSc4	meteorologie
a	a	k8xC	a
zemský	zemský	k2eAgInSc4d1	zemský
magnetismus	magnetismus	k1gInSc4	magnetismus
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
1802	[number]	k4	1802
<g/>
–	–	k?	–
<g/>
1803	[number]	k4	1803
–	–	k?	–
Luke	Luke	k1gNnSc2	Luke
Howard	Howarda	k1gFnPc2	Howarda
vydává	vydávat	k5eAaPmIp3nS	vydávat
spisek	spisek	k1gInSc1	spisek
O	o	k7c6	o
změnách	změna	k1gFnPc6	změna
oblaků	oblak	k1gInPc2	oblak
(	(	kIx(	(
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Modification	Modification	k1gInSc1	Modification
of	of	k?	of
Clouds	Clouds	k1gInSc1	Clouds
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
zavádí	zavádět	k5eAaImIp3nS	zavádět
latinské	latinský	k2eAgNnSc1d1	latinské
pojmenování	pojmenování	k1gNnSc1	pojmenování
oblaků	oblak	k1gInPc2	oblak
<g/>
.	.	kIx.	.
1806	[number]	k4	1806
–	–	k?	–
Francis	Francis	k1gInSc1	Francis
Beaufort	Beaufort	k1gInSc1	Beaufort
představuje	představovat	k5eAaImIp3nS	představovat
stupnici	stupnice	k1gFnSc4	stupnice
odhadu	odhad	k1gInSc2	odhad
rychlostí	rychlost	k1gFnPc2	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
W.	W.	kA	W.
Ferrel	Ferrel	k1gMnSc1	Ferrel
<g/>
,	,	kIx,	,
H.	H.	kA	H.
Helmholtz	Helmholtz	k1gInSc1	Helmholtz
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
–	–	k?	–
poznatky	poznatek	k1gInPc4	poznatek
hydrodynamiky	hydrodynamika	k1gFnSc2	hydrodynamika
a	a	k8xC	a
termodynamiky	termodynamika	k1gFnSc2	termodynamika
–	–	k?	–
počátky	počátek	k1gInPc1	počátek
dynamické	dynamický	k2eAgFnSc2d1	dynamická
meteorologie	meteorologie	k1gFnSc2	meteorologie
<g/>
.	.	kIx.	.
1820	[number]	k4	1820
–	–	k?	–
Heinrich	Heinrich	k1gMnSc1	Heinrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Brandes	Brandes	k1gMnSc1	Brandes
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
mapu	mapa	k1gFnSc4	mapa
tlaku	tlak	k1gInSc2	tlak
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgFnSc4	první
synoptickou	synoptický	k2eAgFnSc4d1	synoptická
mapu	mapa	k1gFnSc4	mapa
<g/>
.	.	kIx.	.
1825	[number]	k4	1825
–	–	k?	–
E.	E.	kA	E.
F.	F.	kA	F.
August	August	k1gMnSc1	August
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g />
.	.	kIx.	.
</s>
<s>
psychrometr	psychrometr	k1gInSc1	psychrometr
<g/>
.	.	kIx.	.
1843	[number]	k4	1843
–	–	k?	–
Lucien	Lucien	k1gInSc1	Lucien
Vidie	Vidie	k1gFnSc2	Vidie
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
aneroid	aneroid	k1gInSc1	aneroid
<g/>
.	.	kIx.	.
mezi	mezi	k7c7	mezi
1845	[number]	k4	1845
<g/>
–	–	k?	–
<g/>
1862	[number]	k4	1862
–	–	k?	–
Alexander	Alexandra	k1gFnPc2	Alexandra
von	von	k1gInSc1	von
Humboldt	Humboldt	k1gMnSc1	Humboldt
definuje	definovat	k5eAaBmIp3nS	definovat
pojem	pojem	k1gInSc1	pojem
klima	klima	k1gNnSc1	klima
<g/>
.	.	kIx.	.
1846	[number]	k4	1846
–	–	k?	–
John	John	k1gMnSc1	John
Thomas	Thomas	k1gMnSc1	Thomas
Romney	Romnea	k1gFnSc2	Romnea
Robinson	Robinson	k1gMnSc1	Robinson
vynalézá	vynalézat	k5eAaImIp3nS	vynalézat
miskový	miskový	k2eAgInSc4d1	miskový
anemometr	anemometr	k1gInSc4	anemometr
<g/>
.	.	kIx.	.
po	po	k7c6	po
1850	[number]	k4	1850
–	–	k?	–
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
synoptická	synoptický	k2eAgFnSc1d1	synoptická
metoda	metoda	k1gFnSc1	metoda
studia	studio	k1gNnSc2	studio
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
dějů	děj	k1gInPc2	děj
–	–	k?	–
vzniká	vznikat	k5eAaImIp3nS	vznikat
synoptická	synoptický	k2eAgFnSc1d1	synoptická
meteorologie	meteorologie	k1gFnSc1	meteorologie
<g/>
.	.	kIx.	.
1855	[number]	k4	1855
–	–	k?	–
Mathew	Mathew	k1gMnSc5	Mathew
Fontaine	Fontain	k1gMnSc5	Fontain
Maury	Maur	k1gMnPc4	Maur
svolal	svolat	k5eAaPmAgMnS	svolat
do	do	k7c2	do
Bruselu	Brusel	k1gInSc2	Brusel
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
konferenci	konference	k1gFnSc6	konference
ustavující	ustavující	k2eAgNnPc1d1	ustavující
pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
předávání	předávání	k1gNnSc4	předávání
meteorologických	meteorologický	k2eAgFnPc2d1	meteorologická
zpráv	zpráva	k1gFnPc2	zpráva
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
kolem	kolem	k7c2	kolem
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
Kolem	kolem	k7c2	kolem
V.	V.	kA	V.
Bjerknese	Bjerknese	k1gFnSc2	Bjerknese
se	se	k3xPyFc4	se
formuje	formovat	k5eAaImIp3nS	formovat
norská	norský	k2eAgFnSc1d1	norská
frontologická	frontologický	k2eAgFnSc1d1	frontologický
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
1941	[number]	k4	1941
–	–	k?	–
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
radarová	radarový	k2eAgFnSc1d1	radarová
meteorologie	meteorologie	k1gFnSc1	meteorologie
<g/>
.	.	kIx.	.
1946	[number]	k4	1946
–	–	k?	–
John	John	k1gMnSc1	John
von	von	k1gInSc1	von
Neumann	Neumann	k1gMnSc1	Neumann
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
matematickým	matematický	k2eAgNnSc7d1	matematické
modelováním	modelování	k1gNnSc7	modelování
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
družicová	družicový	k2eAgFnSc1d1	družicová
meteorologie	meteorologie	k1gFnSc1	meteorologie
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
–	–	k?	–
Byla	být	k5eAaImAgFnS	být
vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
meteorologické	meteorologický	k2eAgFnPc4d1	meteorologická
družice	družice	k1gFnPc4	družice
TIROS-	TIROS-	k1gFnSc2	TIROS-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1976	[number]	k4	1976
byl	být	k5eAaImAgInS	být
osmi	osm	k4xCc2	osm
státy	stát	k1gInPc1	stát
Evropy	Evropa	k1gFnSc2	Evropa
zahájen	zahájit	k5eAaPmNgInS	zahájit
program	program	k1gInSc1	program
Meteosat	Meteosat	k1gFnPc2	Meteosat
<g/>
.	.	kIx.	.
1977	[number]	k4	1977
–	–	k?	–
Byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
družice	družice	k1gFnSc1	družice
Meteosat	Meteosat	k1gFnSc2	Meteosat
1	[number]	k4	1
<g/>
.	.	kIx.	.
1092	[number]	k4	1092
–	–	k?	–
Kosmas	Kosmas	k1gMnSc1	Kosmas
–	–	k?	–
první	první	k4xOgFnSc2	první
zmínky	zmínka	k1gFnSc2	zmínka
o	o	k7c4	o
počasí	počasí	k1gNnSc4	počasí
na	na	k7c4	na
území	území	k1gNnSc4	území
Čech	Čechy	k1gFnPc2	Čechy
1533	[number]	k4	1533
<g/>
–	–	k?	–
<g/>
1534	[number]	k4	1534
–	–	k?	–
žerotínské	žerotínský	k2eAgInPc4d1	žerotínský
denní	denní	k2eAgInPc4d1	denní
záznamy	záznam	k1gInPc4	záznam
o	o	k7c6	o
počasí	počasí	k1gNnSc6	počasí
1558	[number]	k4	1558
<g/>
–	–	k?	–
<g/>
1568	[number]	k4	1568
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
záznamy	záznam	k1gInPc1	záznam
o	o	k7c4	o
počasí	počasí	k1gNnSc4	počasí
z	z	k7c2	z
Bratislavy	Bratislava	k1gFnSc2	Bratislava
a	a	k8xC	a
Prešova	Prešov	k1gInSc2	Prešov
pozorované	pozorovaný	k2eAgFnPc4d1	pozorovaná
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
Žigmunda	Žigmund	k1gMnSc2	Žigmund
Tordy	Torda	k1gMnSc2	Torda
<g/>
.	.	kIx.	.
1717	[number]	k4	1717
–	–	k?	–
Zákupy	zákup	k1gInPc4	zákup
–	–	k?	–
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
vzduchu	vzduch	k1gInSc2	vzduch
1717	[number]	k4	1717
<g/>
–	–	k?	–
<g/>
1720	[number]	k4	1720
–	–	k?	–
Adam	Adam	k1gMnSc1	Adam
Reimann	Reimann	k1gMnSc1	Reimann
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
Prešově	Prešov	k1gInSc6	Prešov
první	první	k4xOgNnSc4	první
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
denní	denní	k2eAgNnSc4d1	denní
měření	měření	k1gNnSc4	měření
tlakoměrem	tlakoměr	k1gInSc7	tlakoměr
a	a	k8xC	a
teploměrem	teploměr	k1gInSc7	teploměr
<g/>
.	.	kIx.	.
1771	[number]	k4	1771
–	–	k?	–
klementinská	klementinský	k2eAgFnSc1d1	Klementinská
řada	řada	k1gFnSc1	řada
teplot	teplota	k1gFnPc2	teplota
1804	[number]	k4	1804
–	–	k?	–
klementinská	klementinský	k2eAgFnSc1d1	Klementinská
řada	řada	k1gFnSc1	řada
srážek	srážka	k1gFnPc2	srážka
1871	[number]	k4	1871
–	–	k?	–
Hrabě	Hrabě	k1gMnSc1	Hrabě
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Konkoly-Thege	Konkoly-Thege	k1gInSc4	Konkoly-Thege
zakládá	zakládat	k5eAaImIp3nS	zakládat
observatoř	observatoř	k1gFnSc1	observatoř
v	v	k7c4	v
Hurbanovu	Hurbanův	k2eAgFnSc4d1	Hurbanova
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
EUMETSAT	EUMETSAT	kA	EUMETSAT
<g/>
.	.	kIx.	.
</s>
<s>
Norská	norský	k2eAgFnSc1d1	norská
frontologická	frontologický	k2eAgFnSc1d1	frontologický
škola	škola	k1gFnSc1	škola
–	–	k?	–
založena	založit	k5eAaPmNgFnS	založit
Bjerknesem	Bjerknes	k1gInSc7	Bjerknes
Americká	americký	k2eAgFnSc1d1	americká
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
škola	škola	k1gFnSc1	škola
–	–	k?	–
založena	založit	k5eAaPmNgFnS	založit
Rossbym	Rossbymum	k1gNnPc2	Rossbymum
1873	[number]	k4	1873
–	–	k?	–
Konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
První	první	k4xOgInSc1	první
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
meteorologický	meteorologický	k2eAgInSc1d1	meteorologický
kongres	kongres	k1gInSc1	kongres
(	(	kIx(	(
<g/>
First	First	k1gInSc1	First
International	International	k1gFnSc2	International
Meteorological	Meteorological	k1gMnSc2	Meteorological
Congress	Congressa	k1gFnPc2	Congressa
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
1873	[number]	k4	1873
–	–	k?	–
Byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
IMO	IMO	kA	IMO
<g/>
,	,	kIx,	,
International	International	k1gFnSc1	International
Meteorological	Meteorological	k1gMnSc1	Meteorological
Organization	Organization	k1gInSc1	Organization
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1947	[number]	k4	1947
–	–	k?	–
Byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
Světová	světový	k2eAgFnSc1d1	světová
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WMO	WMO	kA	WMO
<g/>
,	,	kIx,	,
World	World	k1gMnSc1	World
Meteorological	Meteorological	k1gFnSc2	Meteorological
Organization	Organization	k1gInSc1	Organization
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
UN	UN	kA	UN
<g/>
,	,	kIx,	,
United	United	k1gInSc1	United
Nation	Nation	k1gInSc1	Nation
<g/>
)	)	kIx)	)
–	–	k?	–
Výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
sekretariát	sekretariát	k1gInSc1	sekretariát
organizace	organizace	k1gFnSc2	organizace
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologické	meteorologický	k2eAgInPc1d1	meteorologický
prvky	prvek	k1gInPc1	prvek
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
definování	definování	k1gNnSc3	definování
okamžitého	okamžitý	k2eAgInSc2d1	okamžitý
stavu	stav	k1gInSc2	stav
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
více	hodně	k6eAd2	hodně
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
prvků	prvek	k1gInPc2	prvek
známe	znát	k5eAaImIp1nP	znát
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
lépe	dobře	k6eAd2	dobře
umíme	umět	k5eAaImIp1nP	umět
počasí	počasí	k1gNnSc4	počasí
popsat	popsat	k5eAaPmF	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
anemometr	anemometr	k1gInSc1	anemometr
<g/>
,	,	kIx,	,
aneroid	aneroid	k1gInSc1	aneroid
<g/>
,	,	kIx,	,
barograf	barograf	k1gInSc1	barograf
<g/>
,	,	kIx,	,
družice	družice	k1gFnSc1	družice
<g/>
,	,	kIx,	,
kapalinový	kapalinový	k2eAgInSc1d1	kapalinový
tlakoměr	tlakoměr	k1gInSc1	tlakoměr
<g/>
,	,	kIx,	,
radar	radar	k1gInSc1	radar
<g/>
,	,	kIx,	,
srážkoměr	srážkoměr	k1gInSc1	srážkoměr
<g/>
,	,	kIx,	,
teploměr	teploměr	k1gInSc1	teploměr
<g/>
,	,	kIx,	,
termograf	termograf	k1gInSc1	termograf
<g/>
,	,	kIx,	,
vlhkoměr	vlhkoměr	k1gInSc1	vlhkoměr
<g/>
.	.	kIx.	.
</s>
<s>
Transmisometr	Transmisometr	k1gInSc1	Transmisometr
–	–	k?	–
přístroj	přístroj	k1gInSc1	přístroj
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
dohlednosti	dohlednost	k1gFnSc2	dohlednost
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozptylu	rozptyl	k1gInSc2	rozptyl
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
v	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
meteorologii	meteorologie	k1gFnSc6	meteorologie
ve	v	k7c6	v
zprávách	zpráva	k1gFnPc6	zpráva
ATIS	ATIS	kA	ATIS
(	(	kIx(	(
<g/>
METAR	METAR	kA	METAR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měřič	měřič	k1gInSc1	měřič
dopředného	dopředný	k2eAgInSc2d1	dopředný
rozptylu	rozptyl	k1gInSc2	rozptyl
–	–	k?	–
modernější	moderní	k2eAgFnSc1d2	modernější
verze	verze	k1gFnSc1	verze
transmisometru	transmisometr	k1gInSc2	transmisometr
<g/>
.	.	kIx.	.
</s>
<s>
Srážkoměr	srážkoměr	k1gInSc1	srážkoměr
(	(	kIx(	(
<g/>
hyetometr	hyetometr	k1gInSc1	hyetometr
<g/>
)	)	kIx)	)
–	–	k?	–
přístroj	přístroj	k1gInSc1	přístroj
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
úhrnů	úhrn	k1gInPc2	úhrn
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	s	k7c7	s
srážkoměry	srážkoměr	k1gInPc7	srážkoměr
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
záchytnou	záchytný	k2eAgFnSc7d1	záchytná
plochou	plocha	k1gFnSc7	plocha
Ombrograf	ombrograf	k1gInSc1	ombrograf
–	–	k?	–
registrační	registrační	k2eAgInSc1d1	registrační
přístroj	přístroj	k1gInSc1	přístroj
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
množství	množství	k1gNnSc2	množství
srážek	srážka	k1gFnPc2	srážka
Pro	pro	k7c4	pro
klimatické	klimatický	k2eAgInPc4d1	klimatický
a	a	k8xC	a
synoptické	synoptický	k2eAgInPc4d1	synoptický
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
2	[number]	k4	2
<g/>
m	m	kA	m
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
psychrometrické-meteorologické	psychrometrickéeteorologický	k2eAgFnSc6d1	psychrometrické-meteorologický
budce	budka	k1gFnSc6	budka
<g/>
.	.	kIx.	.
</s>
<s>
Skleněné	skleněný	k2eAgInPc1d1	skleněný
teploměry	teploměr	k1gInPc1	teploměr
–	–	k?	–
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
různé	různý	k2eAgInPc4d1	různý
teplotní	teplotní	k2eAgInPc4d1	teplotní
a	a	k8xC	a
délkové	délkový	k2eAgFnPc4d1	délková
(	(	kIx(	(
<g/>
objemové	objemový	k2eAgFnPc4d1	objemová
<g/>
)	)	kIx)	)
roztažnosti	roztažnost	k1gFnPc4	roztažnost
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
kapalin	kapalina	k1gFnPc2	kapalina
Kapalinové	kapalinový	k2eAgFnSc2d1	kapalinová
Lihové	lihový	k2eAgFnSc2d1	lihová
Minimální	minimální	k2eAgFnSc2d1	minimální
teploměr	teploměr	k1gInSc4	teploměr
–	–	k?	–
minimální	minimální	k2eAgFnSc1d1	minimální
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
odečítá	odečítat	k5eAaImIp3nS	odečítat
podle	podle	k7c2	podle
skleněného	skleněný	k2eAgInSc2d1	skleněný
indexu	index	k1gInSc2	index
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
měrné	měrný	k2eAgFnSc6d1	měrná
kapiláře	kapilára	k1gFnSc6	kapilára
v	v	k7c6	v
lihovém	lihový	k2eAgInSc6d1	lihový
sloupci	sloupec	k1gInSc6	sloupec
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgNnSc1d1	povrchové
napětí	napětí	k1gNnSc1	napětí
lihu	líh	k1gInSc2	líh
při	při	k7c6	při
zkracování	zkracování	k1gNnSc6	zkracování
sloupce	sloupec	k1gInSc2	sloupec
stahuje	stahovat	k5eAaImIp3nS	stahovat
sebou	se	k3xPyFc7	se
index	index	k1gInSc1	index
a	a	k8xC	a
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
největšího	veliký	k2eAgNnSc2d3	veliký
zkrácení	zkrácení	k1gNnSc2	zkrácení
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
odečítá	odečítat	k5eAaImIp3nS	odečítat
podle	podle	k7c2	podle
konce	konec	k1gInSc2	konec
indexu	index	k1gInSc2	index
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
blíže	blízce	k6eAd2	blízce
hladině	hladina	k1gFnSc3	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Teploměr	teploměr	k1gInSc1	teploměr
se	se	k3xPyFc4	se
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
otočením	otočení	k1gNnSc7	otočení
z	z	k7c2	z
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
polohy	poloha	k1gFnSc2	poloha
do	do	k7c2	do
svislé	svislý	k2eAgFnSc2d1	svislá
<g/>
,	,	kIx,	,
baňkou	baňka	k1gFnSc7	baňka
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
váhou	váha	k1gFnSc7	váha
sjede	sjet	k5eAaPmIp3nS	sjet
k	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
lihu	líh	k1gInSc2	líh
<g/>
.	.	kIx.	.
</s>
<s>
Přízemní	přízemní	k2eAgInSc1d1	přízemní
minimální	minimální	k2eAgInSc1d1	minimální
teploměr	teploměr	k1gInSc1	teploměr
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
5	[number]	k4	5
cm	cm	kA	cm
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sixův	Sixův	k2eAgInSc1d1	Sixův
teploměr	teploměr	k1gInSc1	teploměr
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
teploměr	teploměr	k1gInSc4	teploměr
maximo-minimální	maximoinimální	k2eAgInSc4d1	maximo-minimální
<g/>
.	.	kIx.	.
</s>
<s>
Teploměrným	teploměrný	k2eAgNnSc7d1	teploměrný
mediem	medium	k1gNnSc7	medium
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
líh	líh	k1gInSc1	líh
<g/>
,	,	kIx,	,
toluen	toluen	k1gInSc1	toluen
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
Skleněná	skleněný	k2eAgFnSc1d1	skleněná
kapilára	kapilára	k1gFnSc1	kapilára
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
"	"	kIx"	"
<g/>
U	u	k7c2	u
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Levý	levý	k2eAgInSc1d1	levý
konec	konec	k1gInSc1	konec
kapiláry	kapilára	k1gFnSc2	kapilára
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
baňkou	baňka	k1gFnSc7	baňka
dosti	dosti	k6eAd1	dosti
velkého	velký	k2eAgInSc2d1	velký
obsahu	obsah	k1gInSc2	obsah
s	s	k7c7	s
měrnou	měrný	k2eAgFnSc7d1	měrná
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
není	být	k5eNaImIp3nS	být
teploměrným	teploměrný	k2eAgNnSc7d1	teploměrný
médiem	médium	k1gNnSc7	médium
<g/>
,	,	kIx,	,
posunuje	posunovat	k5eAaImIp3nS	posunovat
skleněné	skleněný	k2eAgInPc4d1	skleněný
indexy	index	k1gInPc4	index
tvaru	tvar	k1gInSc2	tvar
tyčinky	tyčinka	k1gFnSc2	tyčinka
s	s	k7c7	s
feromagnetickými	feromagnetický	k2eAgFnPc7d1	feromagnetická
jemnými	jemný	k2eAgFnPc7d1	jemná
pružinkami	pružinka	k1gFnPc7	pružinka
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
se	se	k3xPyFc4	se
opírají	opírat	k5eAaImIp3nP	opírat
o	o	k7c4	o
stěnu	stěna	k1gFnSc4	stěna
kapiláry	kapilára	k1gFnSc2	kapilára
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dolních	dolní	k2eAgFnPc2d1	dolní
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
rozšířených	rozšířený	k2eAgInPc2d1	rozšířený
konců	konec	k1gInPc2	konec
se	se	k3xPyFc4	se
odečítá	odečítat	k5eAaImIp3nS	odečítat
maximální	maximální	k2eAgFnSc1d1	maximální
a	a	k8xC	a
minimální	minimální	k2eAgFnSc1d1	minimální
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
stupnicích	stupnice	k1gFnPc6	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Zpětně	zpětně	k6eAd1	zpětně
se	se	k3xPyFc4	se
indexy	index	k1gInPc1	index
stahují	stahovat	k5eAaImIp3nP	stahovat
magnetem	magnet	k1gInSc7	magnet
dolu	dol	k1gInSc2	dol
ke	k	k7c3	k
koncům	konec	k1gInPc3	konec
rtuťového	rtuťový	k2eAgInSc2d1	rtuťový
sloupečku	sloupeček	k1gInSc2	sloupeček
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
odečítá	odečítat	k5eAaImIp3nS	odečítat
na	na	k7c6	na
převrácené	převrácený	k2eAgFnSc6d1	převrácená
stupnici	stupnice	k1gFnSc6	stupnice
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
baňka	baňka	k1gFnSc1	baňka
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
teplota	teplota	k1gFnSc1	teplota
pak	pak	k6eAd1	pak
na	na	k7c6	na
normálně	normálně	k6eAd1	normálně
orientované	orientovaný	k2eAgFnSc3d1	orientovaná
stupnici	stupnice	k1gFnSc3	stupnice
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
pravé	pravý	k2eAgNnSc1d1	pravé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
velkou	velký	k2eAgFnSc4d1	velká
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
setrvačnost	setrvačnost	k1gFnSc4	setrvačnost
se	se	k3xPyFc4	se
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
praxi	praxe	k1gFnSc6	praxe
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
stupnic	stupnice	k1gFnPc2	stupnice
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
stupni	stupeň	k1gInSc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Jemnější	jemný	k2eAgNnSc1d2	jemnější
dělení	dělení	k1gNnSc1	dělení
by	by	kYmCp3nS	by
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
zvětšení	zvětšení	k1gNnSc1	zvětšení
celého	celý	k2eAgInSc2d1	celý
teploměru	teploměr	k1gInSc2	teploměr
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
zvětšení	zvětšení	k1gNnSc1	zvětšení
jeho	jeho	k3xOp3gFnSc2	jeho
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
<g/>
.	.	kIx.	.
</s>
<s>
Rtuťové	rtuťový	k2eAgInPc1d1	rtuťový
teploměry	teploměr	k1gInPc1	teploměr
Přízemní	přízemnit	k5eAaPmIp3nP	přízemnit
teploměry	teploměr	k1gInPc4	teploměr
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
myšleno	myšlen	k2eAgNnSc1d1	myšleno
<g/>
;	;	kIx,	;
půdní	půdní	k2eAgInPc1d1	půdní
teploměry	teploměr	k1gInPc1	teploměr
pro	pro	k7c4	pro
hloubky	hloubka	k1gFnPc4	hloubka
zpravidla	zpravidla	k6eAd1	zpravidla
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
a	a	k8xC	a
20	[number]	k4	20
cm	cm	kA	cm
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
hloubky	hloubek	k1gInPc4	hloubek
50	[number]	k4	50
a	a	k8xC	a
100	[number]	k4	100
cm	cm	kA	cm
<g/>
)	)	kIx)	)
Maximální	maximální	k2eAgInSc1d1	maximální
teploměr	teploměr	k1gInSc1	teploměr
Beckmannův	Beckmannův	k2eAgInSc1d1	Beckmannův
teploměr	teploměr	k1gInSc1	teploměr
–	–	k?	–
teploměr	teploměr	k1gInSc1	teploměr
měřící	měřící	k2eAgInSc1d1	měřící
oteplení	oteplení	k1gNnSc3	oteplení
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s>
kalorimetrická	kalorimetrický	k2eAgNnPc1d1	kalorimetrické
měření	měření	k1gNnPc1	měření
Vertex	vertex	k1gInSc1	vertex
–	–	k?	–
kontaktový	kontaktový	k2eAgInSc1d1	kontaktový
teploměr	teploměr	k1gInSc1	teploměr
Bimetalové	bimetalový	k2eAgInPc1d1	bimetalový
teploměry	teploměr	k1gInPc1	teploměr
–	–	k?	–
deformační	deformační	k2eAgInSc1d1	deformační
teploměr	teploměr	k1gInSc1	teploměr
tvořený	tvořený	k2eAgInSc1d1	tvořený
bimetalem	bimetal	k1gInSc7	bimetal
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
různé	různý	k2eAgInPc4d1	různý
teplotní	teplotní	k2eAgInPc4d1	teplotní
a	a	k8xC	a
délkové	délkový	k2eAgFnPc4d1	délková
(	(	kIx(	(
<g/>
objemové	objemový	k2eAgFnPc4d1	objemová
<g/>
)	)	kIx)	)
roztažnosti	roztažnost	k1gFnPc4	roztažnost
kovů	kov	k1gInPc2	kov
dvojkovu	dvojkov	k1gInSc2	dvojkov
(	(	kIx(	(
<g/>
bimetalu	bimetal	k1gInSc2	bimetal
<g/>
)	)	kIx)	)
Odporové	odporový	k2eAgInPc4d1	odporový
teploměry	teploměr	k1gInPc4	teploměr
(	(	kIx(	(
<g/>
elektrické	elektrický	k2eAgInPc1d1	elektrický
<g/>
)	)	kIx)	)
Kovové	kovový	k2eAgInPc1d1	kovový
teploměry	teploměr	k1gInPc1	teploměr
Polovodičové	polovodičový	k2eAgInPc1d1	polovodičový
teploměry	teploměr	k1gInPc1	teploměr
(	(	kIx(	(
<g/>
termistory	termistor	k1gInPc1	termistor
<g/>
)	)	kIx)	)
Termočlánky	termočlánek	k1gInPc1	termočlánek
Infrateploměry	Infrateploměr	k1gInPc1	Infrateploměr
Akustické	akustický	k2eAgFnSc2d1	akustická
anemometr	anemometr	k1gInSc4	anemometr
–	–	k?	–
odvozené	odvozený	k2eAgNnSc4d1	odvozené
měření	měření	k1gNnSc4	měření
virtuální	virtuální	k2eAgFnSc2d1	virtuální
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
měření	měření	k1gNnSc2	měření
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
směru	směr	k1gInSc2	směr
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Pyrometry	pyrometr	k1gInPc1	pyrometr
–	–	k?	–
infračervené	infračervený	k2eAgFnSc2d1	infračervená
záření	záření	k1gNnSc2	záření
Akustické	akustický	k2eAgInPc1d1	akustický
teploměry	teploměr	k1gInPc1	teploměr
–	–	k?	–
využívající	využívající	k2eAgFnSc2d1	využívající
principu	princip	k1gInSc6	princip
změny	změna	k1gFnPc4	změna
rychlosti	rychlost	k1gFnSc2	rychlost
šíření	šíření	k1gNnSc2	šíření
zvuku	zvuk	k1gInSc2	zvuk
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Termograf	termograf	k1gInSc1	termograf
–	–	k?	–
registrační	registrační	k2eAgInSc1d1	registrační
přístroj	přístroj	k1gInSc1	přístroj
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
teploty	teplota	k1gFnSc2	teplota
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
bimetalový	bimetalový	k2eAgInSc1d1	bimetalový
teploměr	teploměr	k1gInSc1	teploměr
<g/>
.	.	kIx.	.
</s>
<s>
Aneroid	aneroid	k1gInSc1	aneroid
–	–	k?	–
kovový	kovový	k2eAgInSc1d1	kovový
tlakoměr	tlakoměr	k1gInSc1	tlakoměr
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
deformace	deformace	k1gFnSc2	deformace
kovové	kovový	k2eAgFnSc2d1	kovová
krabičky	krabička	k1gFnSc2	krabička
(	(	kIx(	(
<g/>
Vidiových	Vidiový	k2eAgFnPc2d1	Vidiový
dóz	dóza	k1gFnPc2	dóza
<g/>
)	)	kIx)	)
Rtuťový	rtuťový	k2eAgInSc1d1	rtuťový
tlakoměr	tlakoměr	k1gInSc1	tlakoměr
(	(	kIx(	(
<g/>
staniční	staniční	k2eAgFnSc1d1	staniční
<g/>
)	)	kIx)	)
–	–	k?	–
kapalinový	kapalinový	k2eAgInSc4d1	kapalinový
tlakoměr	tlakoměr	k1gInSc4	tlakoměr
Barograf	barograf	k1gInSc1	barograf
–	–	k?	–
registrační	registrační	k2eAgInSc1d1	registrační
přístroj	přístroj	k1gInSc1	přístroj
zaznamenávající	zaznamenávající	k2eAgFnSc2d1	zaznamenávající
změny	změna	k1gFnSc2	změna
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
aneroid	aneroid	k1gInSc1	aneroid
Měření	měření	k1gNnSc2	měření
větru	vítr	k1gInSc2	vítr
znamená	znamenat	k5eAaImIp3nS	znamenat
určení	určení	k1gNnSc4	určení
směru	směr	k1gInSc2	směr
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
horizontální	horizontální	k2eAgInSc1d1	horizontální
vektor	vektor	k1gInSc1	vektor
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
přístroje	přístroj	k1gInPc1	přístroj
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
vertikální	vertikální	k2eAgFnSc2d1	vertikální
složky	složka	k1gFnSc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Anemometr	anemometr	k1gInSc1	anemometr
–	–	k?	–
přístroj	přístroj	k1gInSc1	přístroj
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
popřípadě	popřípadě	k6eAd1	popřípadě
i	i	k8xC	i
směru	směr	k1gInSc6	směr
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
anemorumbometr	anemorumbometr	k1gInSc1	anemorumbometr
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
anemometrické	anemometrický	k2eAgNnSc1d1	anemometrický
dvojče	dvojče	k1gNnSc1	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
přístroje	přístroj	k1gInPc1	přístroj
distanční	distanční	k2eAgInPc1d1	distanční
<g/>
,	,	kIx,	,
čidlo	čidlo	k1gNnSc4	čidlo
–	–	k?	–
anemometr	anemometr	k1gInSc1	anemometr
a	a	k8xC	a
směrovka	směrovka	k1gFnSc1	směrovka
zpravidla	zpravidla	k6eAd1	zpravidla
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
10	[number]	k4	10
m	m	kA	m
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
výnos	výnos	k1gInSc1	výnos
–	–	k?	–
ukazatel	ukazatel	k1gInSc4	ukazatel
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
směru	směr	k1gInSc2	směr
větru	vítr	k1gInSc2	vítr
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
čidel	čidlo	k1gNnPc2	čidlo
na	na	k7c4	na
výnos	výnos	k1gInSc4	výnos
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
mechanický	mechanický	k2eAgInSc1d1	mechanický
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc1d1	elektrický
<g/>
,	,	kIx,	,
pneumatický	pneumatický	k2eAgInSc1d1	pneumatický
<g/>
.	.	kIx.	.
</s>
<s>
Mechanický	mechanický	k2eAgInSc1d1	mechanický
Aerodynamický	aerodynamický	k2eAgInSc1d1	aerodynamický
anemometr	anemometr	k1gInSc1	anemometr
–	–	k?	–
Pitotova	Pitotův	k2eAgMnSc2d1	Pitotův
nebo	nebo	k8xC	nebo
Venturiho	Venturi	k1gMnSc2	Venturi
trubice	trubice	k1gFnSc1	trubice
<g/>
,	,	kIx,	,
přenos	přenos	k1gInSc1	přenos
na	na	k7c4	na
výnos	výnos	k1gInSc4	výnos
je	být	k5eAaImIp3nS	být
pneumatický	pneumatický	k2eAgMnSc1d1	pneumatický
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dutý	dutý	k2eAgInSc4d1	dutý
plovák	plovák	k1gInSc4	plovák
<g/>
,	,	kIx,	,
Vidieho	Vidie	k1gMnSc4	Vidie
krabičky	krabička	k1gFnSc2	krabička
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
aneroid	aneroid	k1gInSc4	aneroid
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
tzv.	tzv.	kA	tzv.
Robinsonův	Robinsonův	k2eAgInSc1d1	Robinsonův
miskový	miskový	k2eAgInSc1d1	miskový
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
různý	různý	k2eAgInSc1d1	různý
aerodynamický	aerodynamický	k2eAgInSc1d1	aerodynamický
odpor	odpor	k1gInSc1	odpor
dutých	dutý	k2eAgFnPc2d1	dutá
polokoulí	polokoule	k1gFnPc2	polokoule
(	(	kIx(	(
<g/>
dvou	dva	k4xCgFnPc2	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
rotujících	rotující	k2eAgMnPc2d1	rotující
kolem	kolem	k7c2	kolem
svislé	svislý	k2eAgFnSc2d1	svislá
osy	osa	k1gFnSc2	osa
–	–	k?	–
rotační	rotační	k2eAgInSc4d1	rotační
anemometr	anemometr	k1gInSc4	anemometr
<g/>
.	.	kIx.	.
</s>
<s>
Zchlazovací	zchlazovací	k2eAgInPc1d1	zchlazovací
Vírové	vírový	k2eAgInPc1d1	vírový
Tlakové	tlakový	k2eAgInPc1d1	tlakový
Značkovací	značkovací	k2eAgInPc1d1	značkovací
Termoanemometr	Termoanemometr	k1gInSc1	Termoanemometr
–	–	k?	–
přístroj	přístroj	k1gInSc1	přístroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
rychlosti	rychlost	k1gFnSc2	rychlost
proudění	proudění	k1gNnSc2	proudění
vzduchu	vzduch	k1gInSc2	vzduch
využívá	využívat	k5eAaImIp3nS	využívat
zchlazování	zchlazování	k1gNnSc4	zchlazování
zahřívaného	zahřívaný	k2eAgNnSc2d1	zahřívané
čidla	čidlo	k1gNnSc2	čidlo
<g/>
.	.	kIx.	.
</s>
<s>
Katateploměr	Katateploměr	k1gInSc1	Katateploměr
–	–	k?	–
kapalinový	kapalinový	k2eAgInSc1d1	kapalinový
(	(	kIx(	(
<g/>
ethylalkohol	ethylalkohol	k1gInSc1	ethylalkohol
<g/>
)	)	kIx)	)
skleněný	skleněný	k2eAgInSc1d1	skleněný
teploměr	teploměr	k1gInSc1	teploměr
předepsaných	předepsaný	k2eAgInPc2d1	předepsaný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
měřit	měřit	k5eAaImF	měřit
výsledný	výsledný	k2eAgInSc1d1	výsledný
ochlazovací	ochlazovací	k2eAgInSc1d1	ochlazovací
účinek	účinek	k1gInSc1	účinek
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
proudění	proudění	k1gNnSc2	proudění
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
dvou	dva	k4xCgInPc2	dva
katateploměrů	katateploměr	k1gInPc2	katateploměr
s	s	k7c7	s
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
povrchem	povrch	k1gInSc7	povrch
i	i	k8xC	i
účinnou	účinný	k2eAgFnSc4d1	účinná
teplotu	teplota	k1gFnSc4	teplota
okolních	okolní	k2eAgFnPc2d1	okolní
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
metoda	metoda	k1gFnSc1	metoda
(	(	kIx(	(
<g/>
váhová	váhový	k2eAgFnSc1d1	váhová
<g/>
)	)	kIx)	)
měření	měření	k1gNnSc1	měření
vlhkosti	vlhkost	k1gFnSc2	vlhkost
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
změření	změření	k1gNnSc6	změření
a	a	k8xC	a
výpočtu	výpočet	k1gInSc6	výpočet
rozdílu	rozdíl	k1gInSc2	rozdíl
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vlhkého	vlhký	k2eAgInSc2d1	vlhký
vzorku	vzorek	k1gInSc2	vzorek
a	a	k8xC	a
vzorku	vzorek	k1gInSc2	vzorek
zcela	zcela	k6eAd1	zcela
vysušeného	vysušený	k2eAgNnSc2d1	vysušené
<g/>
.	.	kIx.	.
</s>
<s>
Kondenzační	kondenzační	k2eAgFnSc1d1	kondenzační
metoda	metoda	k1gFnSc1	metoda
–	–	k?	–
princip	princip	k1gInSc4	princip
založený	založený	k2eAgInSc4d1	založený
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
rosného	rosný	k2eAgInSc2d1	rosný
bodu	bod	k1gInSc2	bod
podchlazováním	podchlazování	k1gNnSc7	podchlazování
měřící	měřící	k2eAgFnSc2d1	měřící
plošky	ploška	k1gFnSc2	ploška
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
z	z	k7c2	z
teploty	teplota	k1gFnSc2	teplota
rosného	rosný	k2eAgInSc2d1	rosný
bodu	bod	k1gInSc2	bod
Tr	Tr	k1gFnSc2	Tr
a	a	k8xC	a
tlaku	tlak	k1gInSc3	tlak
p	p	k?	p
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
relativní	relativní	k2eAgFnSc4d1	relativní
vlhkost	vlhkost	k1gFnSc4	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Hygrometrické	hygrometrický	k2eAgFnPc4d1	hygrometrický
metody	metoda	k1gFnPc4	metoda
–	–	k?	–
využívá	využívat	k5eAaPmIp3nS	využívat
změny	změna	k1gFnPc4	změna
média	médium	k1gNnSc2	médium
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
relativní	relativní	k2eAgFnSc6d1	relativní
vlhkosti	vlhkost	k1gFnSc6	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Hygrometr	hygrometr	k1gInSc1	hygrometr
vlasový	vlasový	k2eAgInSc1d1	vlasový
–	–	k?	–
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
změny	změna	k1gFnSc2	změna
délky	délka	k1gFnSc2	délka
odmaštěného	odmaštěný	k2eAgInSc2d1	odmaštěný
lidského	lidský	k2eAgInSc2d1	lidský
světlého	světlý	k2eAgInSc2d1	světlý
vlasu	vlas	k1gInSc2	vlas
(	(	kIx(	(
<g/>
jiného	jiný	k2eAgNnSc2d1	jiné
média	médium	k1gNnSc2	médium
<g/>
)	)	kIx)	)
s	s	k7c7	s
měnící	měnící	k2eAgFnSc7d1	měnící
se	se	k3xPyFc4	se
relativní	relativní	k2eAgFnSc7d1	relativní
vlhkostí	vlhkost	k1gFnSc7	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Závislost	závislost	k1gFnSc1	závislost
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
relativní	relativní	k2eAgFnSc2d1	relativní
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
lineární	lineární	k2eAgNnSc1d1	lineární
<g/>
,	,	kIx,	,
s	s	k7c7	s
přibývající	přibývající	k2eAgFnSc7d1	přibývající
vlhkostí	vlhkost	k1gFnSc7	vlhkost
se	se	k3xPyFc4	se
změna	změna	k1gFnSc1	změna
délky	délka	k1gFnSc2	délka
vlasu	vlas	k1gInSc2	vlas
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgFnPc1d1	elektrická
metody	metoda	k1gFnPc1	metoda
–	–	k?	–
měření	měření	k1gNnPc1	měření
relativní	relativní	k2eAgFnSc2d1	relativní
vlhkosti	vlhkost	k1gFnSc2	vlhkost
elektrickým	elektrický	k2eAgInSc7d1	elektrický
převodníkem	převodník	k1gInSc7	převodník
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
změně	změna	k1gFnSc6	změna
kapacity	kapacita	k1gFnSc2	kapacita
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
dielektrikum	dielektrikum	k1gNnSc1	dielektrikum
je	být	k5eAaImIp3nS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
speciálního	speciální	k2eAgInSc2d1	speciální
polymeru	polymer	k1gInSc2	polymer
<g/>
.	.	kIx.	.
</s>
<s>
Změnou	změna	k1gFnSc7	změna
vlhkosti	vlhkost	k1gFnSc2	vlhkost
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
elektrické	elektrický	k2eAgFnPc1d1	elektrická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
polymeru	polymer	k1gInSc2	polymer
<g/>
.	.	kIx.	.
</s>
<s>
Kapacitní	kapacitní	k2eAgInPc1d1	kapacitní
vlhkoměry	vlhkoměr	k1gInPc1	vlhkoměr
–	–	k?	–
elektrický	elektrický	k2eAgInSc4d1	elektrický
vlhkoměr	vlhkoměr	k1gInSc4	vlhkoměr
Psychrometrická	Psychrometrický	k2eAgFnSc1d1	Psychrometrický
metoda	metoda	k1gFnSc1	metoda
–	–	k?	–
princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
úměrnosti	úměrnost	k1gFnSc6	úměrnost
psychrometrického	psychrometrický	k2eAgInSc2d1	psychrometrický
rozdílu	rozdíl	k1gInSc2	rozdíl
teplot	teplota	k1gFnPc2	teplota
"	"	kIx"	"
<g/>
suchého	suchý	k2eAgNnSc2d1	suché
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ts	ts	k0	ts
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
vlhkého	vlhký	k2eAgMnSc2d1	vlhký
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tv	tv	k?	tv
<g/>
)	)	kIx)	)
teploměru	teploměr	k1gInSc2	teploměr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
úměrný	úměrný	k2eAgInSc1d1	úměrný
rozdílu	rozdíl	k1gInSc2	rozdíl
napětí	napětí	k1gNnSc4	napětí
Ev	Eva	k1gFnPc2	Eva
nasycených	nasycený	k2eAgFnPc2d1	nasycená
par	para	k1gFnPc2	para
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
<g />
.	.	kIx.	.
</s>
<s>
tv	tv	k?	tv
a	a	k8xC	a
skutečného	skutečný	k2eAgNnSc2d1	skutečné
napětí	napětí	k1gNnSc2	napětí
par	para	k1gFnPc2	para
e	e	k0	e
<g/>
,	,	kIx,	,
dělenému	dělený	k2eAgInSc3d1	dělený
barometrickým	barometrický	k2eAgInSc7d1	barometrický
tlakem	tlak	k1gInSc7	tlak
pb	pb	k?	pb
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
A	a	k9	a
je	být	k5eAaImIp3nS	být
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
:	:	kIx,	:
Assmanův	Assmanův	k2eAgInSc1d1	Assmanův
aspirační	aspirační	k2eAgInSc1d1	aspirační
psychrometr	psychrometr	k1gInSc1	psychrometr
–	–	k?	–
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
teploměrů	teploměr	k1gInPc2	teploměr
(	(	kIx(	(
<g/>
suchého	suchý	k2eAgMnSc2d1	suchý
a	a	k8xC	a
vlhkého	vlhký	k2eAgInSc2d1	vlhký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Psychrometr	psychrometr	k1gInSc1	psychrometr
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvojicí	dvojice	k1gFnSc7	dvojice
teploměrů	teploměr	k1gInPc2	teploměr
stejné	stejný	k2eAgFnSc2d1	stejná
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
v	v	k7c6	v
meteorologické	meteorologický	k2eAgFnSc6d1	meteorologická
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tzv.	tzv.	kA	tzv.
staniční	staniční	k2eAgInPc4d1	staniční
teploměry	teploměr	k1gInPc4	teploměr
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
teploměrů	teploměr	k1gInPc2	teploměr
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
úpravu	úprava	k1gFnSc4	úprava
a	a	k8xC	a
udává	udávat	k5eAaImIp3nS	udávat
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
teplotu	teplota	k1gFnSc4	teplota
vzduchu	vzduch	k1gInSc2	vzduch
t.	t.	k?	t.
Druhý	druhý	k4xOgInSc1	druhý
teploměr	teploměr	k1gInSc1	teploměr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
vlhký	vlhký	k2eAgInSc1d1	vlhký
<g/>
,	,	kIx,	,
udává	udávat	k5eAaImIp3nS	udávat
tzv.	tzv.	kA	tzv.
vlhkou	vlhký	k2eAgFnSc4d1	vlhká
teplotu	teplota	k1gFnSc4	teplota
t	t	k?	t
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
baňku	baňka	k1gFnSc4	baňka
obalenu	obalen	k2eAgFnSc4d1	obalena
textilií	textilie	k1gFnSc7	textilie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
navlhčena	navlhčit	k5eAaPmNgFnS	navlhčit
destilovanou	destilovaný	k2eAgFnSc7d1	destilovaná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
odpařování	odpařování	k1gNnSc1	odpařování
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
jednak	jednak	k8xC	jednak
na	na	k7c6	na
relativní	relativní	k2eAgFnSc6d1	relativní
vlhkosti	vlhkost	k1gFnSc6	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
při	při	k7c6	při
100	[number]	k4	100
%	%	kIx~	%
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
neodpařuje	odpařovat	k5eNaImIp3nS	odpařovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
proudění	proudění	k1gNnSc2	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Augustův	Augustův	k2eAgInSc1d1	Augustův
psychrometr	psychrometr	k1gInSc1	psychrometr
–	–	k?	–
Umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
do	do	k7c2	do
meteorologické	meteorologický	k2eAgFnSc2d1	meteorologická
–	–	k?	–
psychrometrické	psychrometrický	k2eAgFnSc2d1	psychrometrický
budky	budka	k1gFnSc2	budka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
klasickému	klasický	k2eAgInSc3d1	klasický
psychrometru	psychrometr	k1gInSc3	psychrometr
upraven	upraven	k2eAgMnSc1d1	upraven
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
baňku	baňka	k1gFnSc4	baňka
vlhkého	vlhký	k2eAgInSc2d1	vlhký
teploměru	teploměr	k1gInSc2	teploměr
je	být	k5eAaImIp3nS	být
navléknuta	navléknout	k5eAaPmNgFnS	navléknout
textilní	textilní	k2eAgFnSc1d1	textilní
trubička	trubička	k1gFnSc1	trubička
–	–	k?	–
dutý	dutý	k2eAgInSc1d1	dutý
knot	knot	k1gInSc1	knot
"	"	kIx"	"
<g/>
punčoška	punčoška	k1gFnSc1	punčoška
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
5	[number]	k4	5
cm	cm	kA	cm
cm	cm	kA	cm
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
dolním	dolní	k2eAgInSc7d1	dolní
koncem	konec	k1gInSc7	konec
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
nádobky	nádobka	k1gFnSc2	nádobka
s	s	k7c7	s
destilovanou	destilovaný	k2eAgFnSc7d1	destilovaná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Odpařená	odpařený	k2eAgFnSc1d1	odpařená
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
vzlínáním	vzlínání	k1gNnSc7	vzlínání
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nádobka	nádobka	k1gFnSc1	nádobka
má	mít	k5eAaImIp3nS	mít
byt	byt	k1gInSc4	byt
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
plná	plný	k2eAgFnSc1d1	plná
a	a	k8xC	a
hladina	hladina	k1gFnSc1	hladina
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
3	[number]	k4	3
cm	cm	kA	cm
pod	pod	k7c7	pod
baňkou	baňka	k1gFnSc7	baňka
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
budky	budka	k1gFnSc2	budka
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
větrných	větrný	k2eAgFnPc2d1	větrná
podmínek	podmínka	k1gFnPc2	podmínka
turbulentní	turbulentní	k2eAgNnSc1d1	turbulentní
proudění	proudění	k1gNnSc1	proudění
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
rychlosti	rychlost	k1gFnSc6	rychlost
cca	cca	kA	cca
0,7	[number]	k4	0,7
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
rychlost	rychlost	k1gFnSc4	rychlost
jsou	být	k5eAaImIp3nP	být
počítány	počítán	k2eAgFnPc1d1	počítána
"	"	kIx"	"
<g/>
psychrometrické	psychrometrický	k2eAgFnPc1d1	psychrometrický
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
"	"	kIx"	"
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
podle	podle	k7c2	podle
t	t	k?	t
a	a	k8xC	a
t	t	k?	t
<g/>
'	'	kIx"	'
najdeme	najít	k5eAaPmIp1nP	najít
skutečné	skutečný	k2eAgNnSc1d1	skutečné
napětí	napětí	k1gNnSc1	napětí
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
e	e	k0	e
v	v	k7c6	v
torrech	torr	k1gInPc6	torr
<g/>
,	,	kIx,	,
v	v	k7c6	v
novějších	nový	k2eAgNnPc6d2	novější
vydáních	vydání	k1gNnPc6	vydání
v	v	k7c6	v
hektopaskalech	hektopaskal	k1gMnPc6	hektopaskal
(	(	kIx(	(
<g/>
hPa	hPa	k?	hPa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
nalezne	naleznout	k5eAaPmIp3nS	naleznout
hodnota	hodnota	k1gFnSc1	hodnota
relativní	relativní	k2eAgFnSc2d1	relativní
vlhkosti	vlhkost	k1gFnSc2	vlhkost
R.	R.	kA	R.
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
rosného	rosný	k2eAgInSc2d1	rosný
bodu	bod	k1gInSc2	bod
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
skutečného	skutečný	k2eAgNnSc2d1	skutečné
napětí	napětí	k1gNnSc2	napětí
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
e	e	k0	e
a	a	k8xC	a
skutečné	skutečný	k2eAgFnSc2d1	skutečná
teploty	teplota	k1gFnSc2	teplota
t	t	k?	t
nalezne	nalézt	k5eAaBmIp3nS	nalézt
teplota	teplota	k1gFnSc1	teplota
rosného	rosný	k2eAgInSc2d1	rosný
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
2	[number]	k4	2
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
oprava	oprava	k1gFnSc1	oprava
na	na	k7c4	na
bezvětří	bezvětří	k1gNnSc4	bezvětří
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
7	[number]	k4	7
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
dělá	dělat	k5eAaImIp3nS	dělat
se	se	k3xPyFc4	se
oprava	oprava	k1gFnSc1	oprava
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Ceilometr	Ceilometr	k1gInSc1	Ceilometr
–	–	k?	–
měření	měření	k1gNnSc1	měření
výšky	výška	k1gFnSc2	výška
základny	základna	k1gFnSc2	základna
oblačné	oblačný	k2eAgFnSc2d1	oblačná
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
v	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
meteorologii	meteorologie	k1gFnSc6	meteorologie
ve	v	k7c6	v
zprávách	zpráva	k1gFnPc6	zpráva
ATIS	ATIS	kA	ATIS
(	(	kIx(	(
<g/>
METAR	METAR	kA	METAR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Optický	optický	k2eAgInSc1d1	optický
ceilometr	ceilometr	k1gInSc1	ceilometr
Laserový	laserový	k2eAgInSc1d1	laserový
ceilometr	ceilometr	k1gInSc1	ceilometr
Slunoměr	slunoměr	k1gInSc1	slunoměr
Slunoměr	slunoměr	k1gInSc1	slunoměr
Campbellův	Campbellův	k2eAgInSc1d1	Campbellův
a	a	k8xC	a
Stokesův	Stokesův	k2eAgMnSc1d1	Stokesův
–	–	k?	–
měření	měření	k1gNnSc1	měření
trvání	trvání	k1gNnSc2	trvání
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
kouli	koule	k1gFnSc4	koule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
propaluje	propalovat	k5eAaImIp3nS	propalovat
registrační	registrační	k2eAgInSc4d1	registrační
pásek	pásek	k1gInSc4	pásek
<g/>
.	.	kIx.	.
</s>
<s>
Slunoměr	slunoměr	k1gInSc1	slunoměr
Marvinův	Marvinův	k2eAgInSc4d1	Marvinův
Slunoměr	slunoměr	k1gInSc4	slunoměr
Jordanův	Jordanův	k2eAgInSc4d1	Jordanův
–	–	k?	–
měření	měření	k1gNnSc1	měření
trvání	trvání	k1gNnSc2	trvání
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
<g/>
,	,	kIx,	,
trvání	trvání	k1gNnSc1	trvání
se	se	k3xPyFc4	se
zaznamenávalo	zaznamenávat	k5eAaImAgNnS	zaznamenávat
na	na	k7c4	na
světlocitlivý	světlocitlivý	k2eAgInSc4d1	světlocitlivý
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Radiometr	radiometr	k1gInSc1	radiometr
–	–	k?	–
měření	měření	k1gNnSc2	měření
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
staniční	staniční	k2eAgInPc1d1	staniční
<g/>
,	,	kIx,	,
družicové	družicový	k2eAgInPc1d1	družicový
<g/>
)	)	kIx)	)
Pyrheliometr	pyrheliometr	k1gInSc1	pyrheliometr
–	–	k?	–
měření	měření	k1gNnSc2	měření
přímého	přímý	k2eAgNnSc2d1	přímé
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
Aktinometr	aktinometr	k1gInSc1	aktinometr
–	–	k?	–
srovnávací	srovnávací	k2eAgNnSc1d1	srovnávací
měření	měření	k1gNnSc1	měření
přímého	přímý	k2eAgNnSc2d1	přímé
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
Pyranometr	pyranometr	k1gInSc1	pyranometr
–	–	k?	–
měření	měření	k1gNnSc2	měření
globálního	globální	k2eAgNnSc2d1	globální
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
Pyrradiometr	Pyrradiometr	k1gInSc1	Pyrradiometr
–	–	k?	–
měření	měření	k1gNnSc2	měření
krátkovlnného	krátkovlnný	k2eAgNnSc2d1	krátkovlnné
i	i	k8xC	i
dlouhovlnného	dlouhovlnný	k2eAgNnSc2d1	dlouhovlnné
záření	záření	k1gNnSc2	záření
Bilancometr	Bilancometr	k1gInSc1	Bilancometr
–	–	k?	–
měření	měření	k1gNnSc1	měření
rozdílu	rozdíl	k1gInSc2	rozdíl
celkového	celkový	k2eAgNnSc2d1	celkové
záření	záření	k1gNnSc2	záření
Albedometr	Albedometr	k1gInSc1	Albedometr
–	–	k?	–
měření	měření	k1gNnSc1	měření
albeda	albeda	k1gMnSc1	albeda
KRŠKA	Krška	k1gMnSc1	Krška
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
ŠAMAJ	ŠAMAJ	kA	ŠAMAJ
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
meteorologie	meteorologie	k1gFnSc2	meteorologie
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
568	[number]	k4	568
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
951	[number]	k4	951
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Vorticita	Vorticita	k1gFnSc1	Vorticita
Klimatologie	klimatologie	k1gFnSc1	klimatologie
Koloběh	koloběh	k1gInSc4	koloběh
vody	voda	k1gFnSc2	voda
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
meteorologie	meteorologie	k1gFnSc2	meteorologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
meteorologie	meteorologie	k1gFnSc1	meteorologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Český	český	k2eAgInSc1d1	český
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1	hydrometeorologický
ústav	ústav	k1gInSc1	ústav
Předpovědi	předpověď	k1gFnSc2	předpověď
počasí	počasí	k1gNnSc2	počasí
od	od	k7c2	od
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
meteorologů	meteorolog	k1gMnPc2	meteorolog
Avimet	Avimet	k1gMnSc1	Avimet
–	–	k?	–
stránky	stránka	k1gFnPc4	stránka
odboru	odbor	k1gInSc2	odbor
letecké	letecký	k2eAgFnSc2d1	letecká
meteorologie	meteorologie	k1gFnSc2	meteorologie
Počasí	počasí	k1gNnPc2	počasí
venku	venku	k6eAd1	venku
–	–	k?	–
různé	různý	k2eAgInPc4d1	různý
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
stav	stav	k1gInSc4	stav
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
předpovědi	předpověď	k1gFnSc2	předpověď
počasí	počasí	k1gNnSc2	počasí
Solar	Solar	k1gMnSc1	Solar
Eclipse	Eclipse	k1gFnSc2	Eclipse
Meteorological	Meteorological	k1gMnSc1	Meteorological
Measurement	Measurement	k1gMnSc1	Measurement
–	–	k?	–
meteorologické	meteorologický	k2eAgNnSc1d1	meteorologické
měření	měření	k1gNnSc1	měření
během	během	k7c2	během
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
meteorologie	meteorologie	k1gFnSc1	meteorologie
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
meteorologické	meteorologický	k2eAgFnPc4d1	meteorologická
stanice	stanice	k1gFnPc4	stanice
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
