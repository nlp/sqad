<p>
<s>
Neutron	neutron	k1gInSc1	neutron
je	být	k5eAaImIp3nS	být
subatomární	subatomární	k2eAgFnSc1d1	subatomární
částice	částice	k1gFnSc1	částice
bez	bez	k7c2	bez
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
(	(	kIx(	(
<g/>
neutrální	neutrální	k2eAgFnPc1d1	neutrální
částice	částice	k1gFnPc1	částice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
stavebních	stavební	k2eAgFnPc2d1	stavební
částic	částice	k1gFnPc2	částice
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
(	(	kIx(	(
<g/>
nukleon	nukleon	k1gInSc1	nukleon
<g/>
)	)	kIx)	)
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
téměř	téměř	k6eAd1	téměř
veškeré	veškerý	k3xTgFnPc1	veškerý
známé	známý	k2eAgFnPc1d1	známá
hmoty	hmota	k1gFnPc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
lišící	lišící	k2eAgInPc1d1	lišící
se	se	k3xPyFc4	se
jen	jen	k6eAd1	jen
počtem	počet	k1gInSc7	počet
neutronů	neutron	k1gInPc2	neutron
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
izotopy	izotop	k1gInPc1	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Neutrony	neutron	k1gInPc1	neutron
se	se	k3xPyFc4	se
z	z	k7c2	z
atomu	atom	k1gInSc2	atom
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
při	při	k7c6	při
jaderných	jaderný	k2eAgFnPc6d1	jaderná
reakcích	reakce	k1gFnPc6	reakce
<g/>
,	,	kIx,	,
volné	volný	k2eAgInPc1d1	volný
neutrony	neutron	k1gInPc1	neutron
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
řetězení	řetězení	k1gNnSc4	řetězení
štěpné	štěpný	k2eAgFnSc2d1	štěpná
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
samostatný	samostatný	k2eAgInSc1d1	samostatný
proud	proud	k1gInSc1	proud
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
neutronové	neutronový	k2eAgNnSc1d1	neutronové
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
standardním	standardní	k2eAgInSc6d1	standardní
modelu	model	k1gInSc6	model
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
se	se	k3xPyFc4	se
neutron	neutron	k1gInSc1	neutron
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kvarku	kvark	k1gInSc2	kvark
u	u	k7c2	u
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
kvarků	kvark	k1gInPc2	kvark
d.	d.	k?	d.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
940	[number]	k4	940
MeV	MeV	k1gFnPc2	MeV
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
proton	proton	k1gInSc1	proton
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
938	[number]	k4	938
MeV	MeV	k1gFnPc1	MeV
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
2	[number]	k4	2
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
1839	[number]	k4	1839
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgInPc1d2	hmotnější
než	než	k8xS	než
elektron	elektron	k1gInSc1	elektron
<g/>
.	.	kIx.	.
<g/>
Mimo	mimo	k7c4	mimo
atomové	atomový	k2eAgNnSc4d1	atomové
jádro	jádro	k1gNnSc4	jádro
je	být	k5eAaImIp3nS	být
neutron	neutron	k1gInSc4	neutron
nestabilní	stabilní	k2eNgInSc4d1	nestabilní
se	se	k3xPyFc4	se
střední	střední	k2eAgFnSc7d1	střední
dobou	doba	k1gFnSc7	doba
života	život	k1gInSc2	život
881,5	[number]	k4	881,5
±	±	k?	±
1,5	[number]	k4	1,5
sekund	sekunda	k1gFnPc2	sekunda
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
14,7	[number]	k4	14,7
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
na	na	k7c4	na
proton	proton	k1gInSc4	proton
<g/>
,	,	kIx,	,
elektron	elektron	k1gInSc1	elektron
a	a	k8xC	a
elektronové	elektronový	k2eAgNnSc1d1	elektronové
antineutrino	antineutrino	k1gNnSc1	antineutrino
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neutron	neutron	k1gInSc1	neutron
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
baryony	baryon	k1gInPc7	baryon
-	-	kIx~	-
interaguje	interagovat	k5eAaBmIp3nS	interagovat
silnou	silný	k2eAgFnSc7d1	silná
interakcí	interakce	k1gFnSc7	interakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hadron	hadron	k1gInSc1	hadron
složený	složený	k2eAgInSc1d1	složený
celkem	celkem	k6eAd1	celkem
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
kvarků	kvark	k1gInPc2	kvark
(	(	kIx(	(
<g/>
dvou	dva	k4xCgInPc2	dva
kvarků	kvark	k1gInPc2	kvark
d	d	k?	d
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
u	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
poločíselný	poločíselný	k2eAgInSc1d1	poločíselný
spin	spin	k1gInSc1	spin
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
fermion	fermion	k1gInSc1	fermion
<g/>
)	)	kIx)	)
a	a	k8xC	a
izospin	izospin	k1gMnSc1	izospin
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Antičásticí	antičástice	k1gFnSc7	antičástice
neutronu	neutron	k1gInSc2	neutron
je	být	k5eAaImIp3nS	být
antineutron	antineutron	k1gInSc1	antineutron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
společným	společný	k2eAgInSc7d1	společný
názvem	název	k1gInSc7	název
nukleony	nukleon	k1gInPc4	nukleon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jádra	jádro	k1gNnSc2	jádro
všech	všecek	k3xTgInPc2	všecek
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
nejběžnějšího	běžný	k2eAgInSc2d3	nejběžnější
izotopu	izotop	k1gInSc2	izotop
vodíku	vodík	k1gInSc2	vodík
1	[number]	k4	1
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
tvořeného	tvořený	k2eAgInSc2d1	tvořený
pouze	pouze	k6eAd1	pouze
protonem	proton	k1gInSc7	proton
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
protony	proton	k1gInPc1	proton
i	i	k8xC	i
neutrony	neutron	k1gInPc1	neutron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
zejména	zejména	k9	zejména
chemii	chemie	k1gFnSc4	chemie
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc1	význam
počet	počet	k1gInSc1	počet
neutronů	neutron	k1gInPc2	neutron
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
neutronové	neutronový	k2eAgNnSc1d1	neutronové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
různé	různý	k2eAgInPc1d1	různý
izotopy	izotop	k1gInPc1	izotop
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fyzice	fyzika	k1gFnSc6	fyzika
mají	mít	k5eAaImIp3nP	mít
volné	volný	k2eAgInPc1d1	volný
neutrony	neutron	k1gInPc1	neutron
význam	význam	k1gInSc1	význam
jako	jako	k8xS	jako
iniciátor	iniciátor	k1gMnSc1	iniciátor
štěpné	štěpný	k2eAgFnSc2d1	štěpná
jaderné	jaderný	k2eAgFnSc2d1	jaderná
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
v	v	k7c6	v
jaderném	jaderný	k2eAgInSc6d1	jaderný
reaktoru	reaktor	k1gInSc6	reaktor
probíhat	probíhat	k5eAaImF	probíhat
řízená	řízený	k2eAgFnSc1d1	řízená
štěpná	štěpný	k2eAgFnSc1d1	štěpná
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
neutrony	neutron	k1gInPc1	neutron
zpomalovány	zpomalován	k2eAgInPc1d1	zpomalován
pomocí	pomocí	k7c2	pomocí
některého	některý	k3yIgInSc2	některý
z	z	k7c2	z
typů	typ	k1gInPc2	typ
moderátoru	moderátor	k1gInSc2	moderátor
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
je	být	k5eAaImIp3nS	být
speciálním	speciální	k2eAgNnSc7d1	speciální
tělesem	těleso	k1gNnSc7	těleso
neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
tvořená	tvořený	k2eAgFnSc1d1	tvořená
převážně	převážně	k6eAd1	převážně
neutrony	neutron	k1gInPc1	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
fáze	fáze	k1gFnSc1	fáze
vývoje	vývoj	k1gInSc2	vývoj
hvězdy	hvězda	k1gFnSc2	hvězda
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
supernovy	supernova	k1gFnSc2	supernova
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
gravitačním	gravitační	k2eAgInSc6d1	gravitační
kolapsu	kolaps	k1gInSc6	kolaps
vznikají	vznikat	k5eAaImIp3nP	vznikat
neutrony	neutron	k1gInPc4	neutron
spojením	spojení	k1gNnSc7	spojení
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
protonů	proton	k1gInPc2	proton
za	za	k7c4	za
vyzáření	vyzáření	k1gNnSc4	vyzáření
neutrin	neutrino	k1gNnPc2	neutrino
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Existenci	existence	k1gFnSc4	existence
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
objevil	objevit	k5eAaPmAgMnS	objevit
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
(	(	kIx(	(
<g/>
Rutherfordův	Rutherfordův	k2eAgInSc1d1	Rutherfordův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
formuloval	formulovat	k5eAaImAgMnS	formulovat
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
složení	složení	k1gNnSc6	složení
z	z	k7c2	z
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgInSc1d1	experimentální
důkaz	důkaz	k1gInSc1	důkaz
neutronu	neutron	k1gInSc2	neutron
včetně	včetně	k7c2	včetně
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
podal	podat	k5eAaPmAgMnS	podat
jeho	jeho	k3xOp3gMnSc1	jeho
žák	žák	k1gMnSc1	žák
James	James	k1gMnSc1	James
Chadwick	Chadwick	k1gMnSc1	Chadwick
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
Nature	Natur	k1gMnSc5	Natur
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnPc1	měření
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neutron	neutron	k1gInSc4	neutron
detekovala	detekovat	k5eAaImAgFnS	detekovat
<g/>
,	,	kIx,	,
provedli	provést	k5eAaPmAgMnP	provést
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
další	další	k2eAgMnPc1d1	další
fyzikové	fyzik	k1gMnPc1	fyzik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mylně	mylně	k6eAd1	mylně
je	on	k3xPp3gMnPc4	on
interpretovali	interpretovat	k5eAaBmAgMnP	interpretovat
jako	jako	k8xS	jako
záření	záření	k1gNnSc4	záření
gama	gama	k1gNnSc2	gama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
neutronů	neutron	k1gInPc2	neutron
==	==	k?	==
</s>
</p>
<p>
<s>
Neutrony	neutron	k1gInPc1	neutron
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
chladné	chladný	k2eAgInPc1d1	chladný
neutrony	neutron	k1gInPc1	neutron
<	<	kIx(	<
<g/>
0,002	[number]	k4	0,002
eV	eV	k?	eV
</s>
</p>
<p>
<s>
tepelné	tepelný	k2eAgInPc1d1	tepelný
neutrony	neutron	k1gInPc1	neutron
0,002	[number]	k4	0,002
–	–	k?	–
0,5	[number]	k4	0,5
eV	eV	k?	eV
</s>
</p>
<p>
<s>
rezonanční	rezonanční	k2eAgInPc1d1	rezonanční
neutrony	neutron	k1gInPc1	neutron
0,5	[number]	k4	0,5
–	–	k?	–
1000	[number]	k4	1000
eV	eV	k?	eV
</s>
</p>
<p>
<s>
neutrony	neutron	k1gInPc1	neutron
středních	střední	k2eAgFnPc2d1	střední
energií	energie	k1gFnPc2	energie
1	[number]	k4	1
keV	keV	k?	keV
–	–	k?	–
500	[number]	k4	500
keV	keV	k?	keV
</s>
</p>
<p>
<s>
rychlé	rychlý	k2eAgInPc1d1	rychlý
neutrony	neutron	k1gInPc1	neutron
500	[number]	k4	500
keV	keV	k?	keV
–	–	k?	–
10	[number]	k4	10
MeV	MeV	k1gFnSc1	MeV
</s>
</p>
<p>
<s>
neutrony	neutron	k1gInPc1	neutron
s	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
energiemi	energie	k1gFnPc7	energie
10	[number]	k4	10
MeV	MeV	k1gFnSc2	MeV
–	–	k?	–
50	[number]	k4	50
MeV	MeV	k1gFnSc1	MeV
</s>
</p>
<p>
<s>
neutrony	neutron	k1gInPc1	neutron
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokými	vysoký	k2eAgFnPc7d1	vysoká
energiemi	energie	k1gFnPc7	energie
>	>	kIx)	>
<g/>
50	[number]	k4	50
MeV	MeV	k1gFnPc2	MeV
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Nukleon	nukleon	k1gInSc1	nukleon
</s>
</p>
<p>
<s>
Baryon	Baryon	k1gMnSc1	Baryon
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
neutron	neutron	k1gInSc1	neutron
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
neutron	neutron	k1gInSc1	neutron
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
