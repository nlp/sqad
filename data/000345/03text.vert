<s>
Noc	noc	k1gFnSc1	noc
je	být	k5eAaImIp3nS	být
denní	denní	k2eAgNnSc4d1	denní
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
během	během	k7c2	během
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
nachází	nacházet	k5eAaImIp3nS	nacházet
za	za	k7c7	za
horizontem	horizont	k1gInSc7	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
noci	noc	k1gFnSc2	noc
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
jejího	její	k3xOp3gNnSc2	její
trvání	trvání	k1gNnSc2	trvání
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
faktorech	faktor	k1gInPc6	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
<g/>
,	,	kIx,	,
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
a	a	k8xC	a
časové	časový	k2eAgNnSc1d1	časové
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
noci	noc	k1gFnSc2	noc
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zvířat	zvíře	k1gNnPc2	zvíře
spí	spát	k5eAaImIp3nP	spát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemohou	moct	k5eNaImIp3nP	moct
provádět	provádět	k5eAaImF	provádět
většinu	většina	k1gFnSc4	většina
aktivit	aktivita	k1gFnPc2	aktivita
konaných	konaný	k2eAgFnPc2d1	konaná
za	za	k7c2	za
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
morfologii	morfologie	k1gFnSc4	morfologie
<g/>
,	,	kIx,	,
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
chování	chování	k1gNnSc4	chování
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgInPc2	všecek
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
některá	některý	k3yIgNnPc1	některý
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
netopýři	netopýr	k1gMnPc1	netopýr
nebo	nebo	k8xC	nebo
cvrčci	cvrček	k1gMnPc1	cvrček
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgInPc1d1	aktivní
především	především	k9	především
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
a	a	k8xC	a
zlem	zlo	k1gNnSc7	zlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidovém	lidový	k2eAgInSc6d1	lidový
folklóru	folklór	k1gInSc6	folklór
téměř	téměř	k6eAd1	téměř
každé	každý	k3xTgFnSc2	každý
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
příběhy	příběh	k1gInPc1	příběh
a	a	k8xC	a
zkazky	zkazka	k1gFnPc1	zkazka
o	o	k7c6	o
nebezpečných	bezpečný	k2eNgFnPc6d1	nebezpečná
bytostech	bytost	k1gFnPc6	bytost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgFnPc1d1	aktivní
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
duchy	duch	k1gMnPc4	duch
<g/>
,	,	kIx,	,
upíry	upír	k1gMnPc4	upír
či	či	k8xC	či
vlkodlaky	vlkodlak	k1gMnPc4	vlkodlak
<g/>
.	.	kIx.	.
</s>
<s>
Tropická	tropický	k2eAgFnSc1d1	tropická
noc	noc	k1gFnSc1	noc
Bílá	bílý	k2eAgFnSc1d1	bílá
noc	noc	k1gFnSc1	noc
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
noc	noc	k1gFnSc1	noc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
noc	noc	k1gFnSc4	noc
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc4	heslo
noc	noc	k1gFnSc4	noc
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
