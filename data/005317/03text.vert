<s>
Andrew	Andrew	k?	Andrew
Warhola	Warhola	k1gFnSc1	Warhola
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1928	[number]	k4	1928
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1987	[number]	k4	1987
New	New	k1gMnSc2	New
York	York	k1gInSc4	York
City	city	k1gNnSc2	city
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
jako	jako	k8xC	jako
Andy	Anda	k1gFnPc1	Anda
Warhol	Warhol	k1gInSc1	Warhol
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
tvůrce	tvůrce	k1gMnSc1	tvůrce
a	a	k8xC	a
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
osobnost	osobnost	k1gFnSc1	osobnost
amerického	americký	k2eAgNnSc2d1	americké
hnutí	hnutí	k1gNnSc2	hnutí
pop	pop	k1gInSc1	pop
artu	arta	k1gFnSc4	arta
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
zahájil	zahájit	k5eAaPmAgMnS	zahájit
jako	jako	k8xS	jako
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
reklamních	reklamní	k2eAgInPc2d1	reklamní
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
známý	známý	k2eAgMnSc1d1	známý
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
jako	jako	k8xS	jako
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
avantgardních	avantgardní	k2eAgMnPc2d1	avantgardní
filmů	film	k1gInPc2	film
a	a	k8xC	a
manažer	manažer	k1gMnSc1	manažer
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ztvárněn	ztvárnit	k5eAaPmNgInS	ztvárnit
v	v	k7c6	v
několika	několik	k4yIc6	několik
životopisných	životopisný	k2eAgInPc6d1	životopisný
filmech	film	k1gInPc6	film
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
natočeny	natočen	k2eAgInPc1d1	natočen
dokumentární	dokumentární	k2eAgInPc1d1	dokumentární
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
bylo	být	k5eAaImAgNnS	být
poblíž	poblíž	k7c2	poblíž
rodné	rodný	k2eAgFnSc2d1	rodná
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Mezilaborce	Mezilaborka	k1gFnSc6	Mezilaborka
<g/>
,	,	kIx,	,
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
muzeum	muzeum	k1gNnSc1	muzeum
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
jeho	jeho	k3xOp3gFnSc3	jeho
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
muzeum	muzeum	k1gNnSc1	muzeum
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Pittsburgu	Pittsburg	k1gInSc6	Pittsburg
<g/>
.	.	kIx.	.
</s>
<s>
Andrew	Andrew	k?	Andrew
Warhola	Warhola	k1gFnSc1	Warhola
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Pittsburghu	Pittsburgh	k1gInSc2	Pittsburgh
rusínským	rusínský	k2eAgMnPc3d1	rusínský
přistěhovalcům	přistěhovalec	k1gMnPc3	přistěhovalec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
původem	původ	k1gInSc7	původ
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
uherské	uherský	k2eAgFnSc2d1	uherská
vesničky	vesnička	k1gFnSc2	vesnička
Miková	Miková	k1gFnSc1	Miková
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
severovýchodního	severovýchodní	k2eAgNnSc2d1	severovýchodní
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
datum	datum	k1gNnSc4	datum
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
několik	několik	k4yIc4	několik
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
léta	léto	k1gNnPc4	léto
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Warhol	Warhol	k1gInSc1	Warhol
koneckonců	koneckonců	k9	koneckonců
sám	sám	k3xTgMnSc1	sám
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
rodný	rodný	k2eAgInSc1d1	rodný
list	list	k1gInSc1	list
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
je	být	k5eAaImIp3nS	být
pouhý	pouhý	k2eAgInSc4d1	pouhý
padělek	padělek	k1gInSc4	padělek
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jako	jako	k9	jako
datum	datum	k1gNnSc1	datum
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
uvádí	uvádět	k5eAaImIp3nS	uvádět
právě	právě	k9	právě
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Warholův	Warholův	k2eAgMnSc1d1	Warholův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Varhola	Varhola	k1gFnSc1	Varhola
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
uhelných	uhelný	k2eAgInPc6d1	uhelný
dolech	dol	k1gInPc6	dol
a	a	k8xC	a
na	na	k7c6	na
stavbách	stavba	k1gFnPc6	stavba
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc4	Anda
měl	mít	k5eAaImAgInS	mít
ještě	ještě	k6eAd1	ještě
dva	dva	k4xCgMnPc4	dva
starší	starší	k1gMnPc4	starší
bratry	bratr	k1gMnPc4	bratr
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc4	John
(	(	kIx(	(
<g/>
Jána	Ján	k1gMnSc4	Ján
<g/>
)	)	kIx)	)
a	a	k8xC	a
Paula	Paul	k1gMnSc2	Paul
(	(	kIx(	(
<g/>
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
společně	společně	k6eAd1	společně
žili	žít	k5eAaImAgMnP	žít
na	na	k7c4	na
pittsburské	pittsburský	k2eAgNnSc4d1	pittsburský
55	[number]	k4	55
<g/>
.	.	kIx.	.
ulici	ulice	k1gFnSc6	ulice
Beelen	Beelen	k2eAgMnSc1d1	Beelen
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
řeckokatolického	řeckokatolický	k2eAgNnSc2d1	řeckokatolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
Warhol	Warhol	k1gInSc1	Warhol
sám	sám	k3xTgInSc1	sám
se	se	k3xPyFc4	se
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
pravidelně	pravidelně	k6eAd1	pravidelně
účastňoval	účastňovat	k5eAaImAgMnS	účastňovat
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
a	a	k8xC	a
považoval	považovat	k5eAaImAgMnS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
věřícího	věřící	k1gMnSc4	věřící
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
účastnil	účastnit	k5eAaImAgMnS	účastnit
charitativních	charitativní	k2eAgFnPc2d1	charitativní
aktivit	aktivita	k1gFnPc2	aktivita
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
náboženství	náboženství	k1gNnSc4	náboženství
ale	ale	k8xC	ale
veřejně	veřejně	k6eAd1	veřejně
nekomentoval	komentovat	k5eNaBmAgMnS	komentovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
obrazů	obraz	k1gInPc2	obraz
věnoval	věnovat	k5eAaImAgMnS	věnovat
náboženským	náboženský	k2eAgInPc3d1	náboženský
námětům	námět	k1gInPc3	námět
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivněn	k2eAgInSc1d1	ovlivněn
východním	východní	k2eAgNnSc7d1	východní
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
uměním	umění	k1gNnSc7	umění
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
Andy	Anda	k1gFnPc1	Anda
třikrát	třikrát	k6eAd1	třikrát
nervově	nervově	k6eAd1	nervově
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
;	;	kIx,	;
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
osm	osm	k4xCc1	osm
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
dvakrát	dvakrát	k6eAd1	dvakrát
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Warholovy	Warholův	k2eAgInPc1d1	Warholův
umělecké	umělecký	k2eAgInPc1d1	umělecký
sklony	sklon	k1gInPc1	sklon
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgInP	projevit
již	již	k6eAd1	již
brzy	brzy	k6eAd1	brzy
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc4	Anda
studoval	studovat	k5eAaImAgInS	studovat
obor	obor	k1gInSc1	obor
komerční	komerční	k2eAgFnSc1d1	komerční
designová	designový	k2eAgFnSc1d1	designová
tvorba	tvorba	k1gFnSc1	tvorba
na	na	k7c4	na
Carnegieho	Carnegie	k1gMnSc4	Carnegie
technologickém	technologický	k2eAgInSc6d1	technologický
institutu	institut	k1gInSc6	institut
(	(	kIx(	(
<g/>
Carnegie	Carnegie	k1gFnSc1	Carnegie
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
<g/>
)	)	kIx)	)
v	v	k7c6	v
Pittsburghu	Pittsburgh	k1gInSc6	Pittsburgh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
takovými	takový	k3xDgFnPc7	takový
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
Philip	Philip	k1gMnSc1	Philip
Pearlstein	Pearlstein	k1gMnSc1	Pearlstein
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
americký	americký	k2eAgMnSc1d1	americký
malíř	malíř	k1gMnSc1	malíř
expresivních	expresivní	k2eAgInPc2d1	expresivní
aktů	akt	k1gInPc2	akt
<g/>
.	.	kIx.	.
</s>
<s>
Ukončením	ukončení	k1gNnSc7	ukončení
studia	studio	k1gNnSc2	studio
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
bakalář	bakalář	k1gMnSc1	bakalář
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Bachelor	Bachelor	k1gInSc1	Bachelor
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Artsa	k1gFnPc2	Artsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
zde	zde	k6eAd1	zde
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
kariéru	kariéra	k1gFnSc4	kariéra
ilustrátora	ilustrátor	k1gMnSc2	ilustrátor
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
designera	designer	k1gMnSc2	designer
reklamních	reklamní	k2eAgInPc2d1	reklamní
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Warhol	Warhol	k1gInSc1	Warhol
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
–	–	k?	–
z	z	k7c2	z
tvůrce	tvůrce	k1gMnSc2	tvůrce
inzerátů	inzerát	k1gInPc2	inzerát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejproslulejších	proslulý	k2eAgMnPc2d3	nejproslulejší
amerických	americký	k2eAgMnPc2d1	americký
umělců	umělec	k1gMnPc2	umělec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
Warhol	Warhol	k1gInSc1	Warhol
definoval	definovat	k5eAaBmAgInS	definovat
základní	základní	k2eAgInPc4d1	základní
pojmy	pojem	k1gInPc4	pojem
této	tento	k3xDgFnSc2	tento
dekády	dekáda	k1gFnSc2	dekáda
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
malovat	malovat	k5eAaImF	malovat
známé	známý	k2eAgInPc4d1	známý
americké	americký	k2eAgInPc4d1	americký
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
plechovky	plechovka	k1gFnPc4	plechovka
Campbellovy	Campbellův	k2eAgFnSc2d1	Campbellova
polévky	polévka	k1gFnSc2	polévka
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Campbell	Campbell	k1gMnSc1	Campbell
Soup	Soup	k1gMnSc1	Soup
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
tvorbě	tvorba	k1gFnSc3	tvorba
portrétů	portrét	k1gInPc2	portrét
takových	takový	k3xDgMnPc2	takový
známých	známý	k1gMnPc2	známý
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgFnPc7	jaký
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Marilyn	Marilyn	k1gFnSc4	Marilyn
Monroe	Monro	k1gFnSc2	Monro
<g/>
,	,	kIx,	,
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
,	,	kIx,	,
Muhammad	Muhammad	k1gInSc4	Muhammad
Ali	Ali	k1gMnSc2	Ali
<g/>
,	,	kIx,	,
Troy	Troa	k1gMnSc2	Troa
Donahue	Donahu	k1gMnSc2	Donahu
nebo	nebo	k8xC	nebo
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
Taylor	Taylora	k1gFnPc2	Taylora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Warhol	Warhol	k1gInSc1	Warhol
založil	založit	k5eAaPmAgInS	založit
umělecké	umělecký	k2eAgNnSc4d1	umělecké
studio	studio	k1gNnSc4	studio
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Factory	Factor	k1gInPc4	Factor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Továrna	továrna	k1gFnSc1	továrna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
obklopil	obklopit	k5eAaPmAgMnS	obklopit
širokou	široký	k2eAgFnSc7d1	široká
škálou	škála	k1gFnSc7	škála
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
hudebníků	hudebník	k1gMnPc2	hudebník
a	a	k8xC	a
undergroundových	undergroundový	k2eAgFnPc2d1	undergroundová
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1962	[number]	k4	1962
začal	začít	k5eAaPmAgInS	začít
Warhol	Warhol	k1gInSc1	Warhol
s	s	k7c7	s
masovou	masový	k2eAgFnSc7d1	masová
produkcí	produkce	k1gFnSc7	produkce
sítotisků	sítotisk	k1gInPc2	sítotisk
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
tak	tak	k9	tak
vytvářet	vytvářet	k5eAaImF	vytvářet
umění	umění	k1gNnSc4	umění
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
masových	masový	k2eAgInPc2d1	masový
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
masovém	masový	k2eAgNnSc6d1	masové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
pryžového	pryžový	k2eAgNnSc2d1	pryžové
razítka	razítko	k1gNnSc2	razítko
mu	on	k3xPp3gMnSc3	on
najednou	najednou	k6eAd1	najednou
začala	začít	k5eAaPmAgFnS	začít
připadat	připadat	k5eAaImF	připadat
"	"	kIx"	"
<g/>
příliš	příliš	k6eAd1	příliš
domácká	domácký	k2eAgFnSc1d1	domácká
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
pasivní	pasivní	k2eAgInSc1d1	pasivní
<g/>
.	.	kIx.	.
</s>
<s>
Beru	brát	k5eAaImIp1nS	brát
věci	věc	k1gFnPc4	věc
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
dívám	dívat	k5eAaImIp1nS	dívat
<g/>
,	,	kIx,	,
pozoruji	pozorovat	k5eAaImIp1nS	pozorovat
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tvůrčího	tvůrčí	k2eAgInSc2d1	tvůrčí
procesu	proces	k1gInSc2	proces
minimalizoval	minimalizovat	k5eAaBmAgInS	minimalizovat
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
rozpoutal	rozpoutat	k5eAaPmAgInS	rozpoutat
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
revoluci	revoluce	k1gFnSc4	revoluce
−	−	k?	−
jeho	jeho	k3xOp3gFnPc1	jeho
práce	práce	k1gFnPc1	práce
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
nejenom	nejenom	k6eAd1	nejenom
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
populární	populární	k2eAgMnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
především	především	k6eAd1	především
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
pop-kulturou	popultura	k1gFnSc7	pop-kultura
<g/>
.	.	kIx.	.
</s>
<s>
Warhol	Warhol	k1gInSc1	Warhol
zobrazoval	zobrazovat	k5eAaImAgInS	zobrazovat
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
dolarové	dolarový	k2eAgFnPc1d1	dolarová
bankovky	bankovka	k1gFnPc1	bankovka
<g/>
,	,	kIx,	,
celebrity	celebrita	k1gFnPc1	celebrita
<g/>
,	,	kIx,	,
značkové	značkový	k2eAgInPc1d1	značkový
produkty	produkt	k1gInPc1	produkt
a	a	k8xC	a
obrázky	obrázek	k1gInPc1	obrázek
z	z	k7c2	z
novinových	novinový	k2eAgInPc2d1	novinový
útržků	útržek	k1gInPc2	útržek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
předměty	předmět	k1gInPc1	předmět
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
rozpoznatelné	rozpoznatelný	k2eAgFnSc2d1	rozpoznatelná
a	a	k8xC	a
často	často	k6eAd1	často
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
masy	masa	k1gFnPc4	masa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
Warhola	Warhola	k1gFnSc1	Warhola
fascinoval	fascinovat	k5eAaBmAgInS	fascinovat
ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
nejvíce	hodně	k6eAd3	hodně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
sjednocujícím	sjednocující	k2eAgInSc7d1	sjednocující
prvkem	prvek	k1gInSc7	prvek
veškeré	veškerý	k3xTgFnSc2	veškerý
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc4	Anda
Warhol	Warhol	k1gInSc1	Warhol
namaloval	namalovat	k5eAaPmAgInS	namalovat
10	[number]	k4	10
obrazů	obraz	k1gInPc2	obraz
−	−	k?	−
sérii	série	k1gFnSc6	série
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Ohrožené	ohrožený	k2eAgInPc1d1	ohrožený
druhy	druh	k1gInPc7	druh
(	(	kIx(	(
<g/>
Endangered	Endangered	k1gInSc1	Endangered
Species	species	k1gFnSc2	species
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Warholův	Warholův	k2eAgInSc1d1	Warholův
obraz	obraz	k1gInSc1	obraz
Green	Grena	k1gFnPc2	Grena
Car	car	k1gMnSc1	car
Crash	Crash	k1gMnSc1	Crash
(	(	kIx(	(
<g/>
Green	Green	k2eAgInSc1d1	Green
Burning	Burning	k1gInSc1	Burning
Car	car	k1gMnSc1	car
I	I	kA	I
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
prodán	prodat	k5eAaPmNgMnS	prodat
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
aukční	aukční	k2eAgFnSc6d1	aukční
síni	síň	k1gFnSc6	síň
Christie	Christie	k1gFnSc2	Christie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
za	za	k7c2	za
71,1	[number]	k4	71,1
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc4	jeho
obraz	obraz	k1gInSc4	obraz
Flowers	Flowers	k1gInSc1	Flowers
prodán	prodat	k5eAaPmNgInS	prodat
za	za	k7c4	za
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc4	obraz
Dvojitý	dvojitý	k2eAgMnSc1d1	dvojitý
Elvis	Elvis	k1gMnSc1	Elvis
v	v	k7c6	v
aukční	aukční	k2eAgFnSc6d1	aukční
síni	síň	k1gFnSc6	síň
Sotheby	Sotheba	k1gFnSc2	Sotheba
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
prodán	prodat	k5eAaPmNgInS	prodat
za	za	k7c4	za
37	[number]	k4	37
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Taylorové	Taylorová	k1gFnSc2	Taylorová
byl	být	k5eAaImAgInS	být
vydražen	vydražit	k5eAaPmNgInS	vydražit
za	za	k7c4	za
6,8	[number]	k4	6,8
miliónu	milión	k4xCgInSc2	milión
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
autoportrét	autoportrét	k1gInSc1	autoportrét
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
za	za	k7c4	za
29	[number]	k4	29
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc4	Anda
Warhol	Warhol	k1gInSc1	Warhol
byl	být	k5eAaImAgInS	být
úzce	úzko	k6eAd1	úzko
spjat	spjat	k2eAgInSc1d1	spjat
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
rockovou	rockový	k2eAgFnSc7d1	rocková
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gFnSc2	The
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Té	ten	k3xDgFnSc6	ten
dělal	dělat	k5eAaImAgMnS	dělat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
manažera	manažer	k1gMnSc2	manažer
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
producentem	producent	k1gMnSc7	producent
jejich	jejich	k3xOp3gNnPc6	jejich
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
&	&	k?	&
Nico	Nico	k1gMnSc1	Nico
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
právě	právě	k6eAd1	právě
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
členům	člen	k1gMnPc3	člen
kapely	kapela	k1gFnSc2	kapela
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
natáčení	natáčený	k2eAgMnPc1d1	natáčený
přizvali	přizvat	k5eAaPmAgMnP	přizvat
německou	německý	k2eAgFnSc4d1	německá
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Nico	Nico	k6eAd1	Nico
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
samotné	samotný	k2eAgNnSc1d1	samotné
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
známé	známý	k2eAgNnSc1d1	známé
i	i	k9	i
díky	díky	k7c3	díky
Warholově	Warholův	k2eAgInSc6d1	Warholův
osobitém	osobitý	k2eAgInSc6d1	osobitý
návrhu	návrh	k1gInSc6	návrh
jeho	jeho	k3xOp3gFnSc2	jeho
obálky	obálka	k1gFnSc2	obálka
−	−	k?	−
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
desky	deska	k1gFnSc2	deska
byl	být	k5eAaImAgInS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
výrazný	výrazný	k2eAgInSc1d1	výrazný
žlutý	žlutý	k2eAgInSc1d1	žlutý
banán	banán	k1gInSc1	banán
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Peel	Peel	k1gInSc1	Peel
Slowly	Slowla	k1gMnSc2	Slowla
and	and	k?	and
See	See	k1gMnSc2	See
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pomalu	pomalu	k6eAd1	pomalu
oloupejte	oloupat	k5eAaPmRp2nP	oloupat
a	a	k8xC	a
uvidíte	uvidět	k5eAaPmIp2nP	uvidět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stržení	stržení	k1gNnSc6	stržení
obrázku	obrázek	k1gInSc2	obrázek
žlutého	žlutý	k2eAgInSc2d1	žlutý
banánu	banán	k1gInSc2	banán
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
objevil	objevit	k5eAaPmAgInS	objevit
oloupaný	oloupaný	k2eAgInSc1d1	oloupaný
růžový	růžový	k2eAgInSc1d1	růžový
banán	banán	k1gInSc1	banán
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
prodal	prodat	k5eAaPmAgMnS	prodat
sběratel	sběratel	k1gMnSc1	sběratel
Warrel	Warrel	k1gMnSc1	Warrel
Hill	Hill	k1gMnSc1	Hill
demo	demo	k2eAgFnSc4d1	demo
verzi	verze	k1gFnSc4	verze
tohoto	tento	k3xDgNnSc2	tento
debutového	debutový	k2eAgNnSc2d1	debutové
LP	LP	kA	LP
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
kdysi	kdysi	k6eAd1	kdysi
koupil	koupit	k5eAaPmAgMnS	koupit
na	na	k7c6	na
bleším	bleší	k2eAgInSc6d1	bleší
trhu	trh	k1gInSc6	trh
za	za	k7c4	za
pouhých	pouhý	k2eAgInPc2d1	pouhý
0,756	[number]	k4	0,756
dolaru	dolar	k1gInSc2	dolar
<g/>
,	,	kIx,	,
za	za	k7c4	za
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
25	[number]	k4	25
200	[number]	k4	200
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
první	první	k4xOgFnSc2	první
desky	deska	k1gFnSc2	deska
ale	ale	k8xC	ale
Warhol	Warhol	k1gInSc4	Warhol
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
ukončil	ukončit	k5eAaPmAgMnS	ukončit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
popud	popud	k1gInSc4	popud
frontmana	frontman	k1gMnSc2	frontman
kapely	kapela	k1gFnSc2	kapela
Lou	Lou	k1gMnSc2	Lou
Reeda	Reed	k1gMnSc2	Reed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
natočili	natočit	k5eAaBmAgMnP	natočit
Lou	Lou	k1gMnPc1	Lou
Reed	Reed	k1gMnSc1	Reed
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Calem	Cal	k1gMnSc7	Cal
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
bývalým	bývalý	k2eAgMnSc7d1	bývalý
členem	člen	k1gMnSc7	člen
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
Songs	Songsa	k1gFnPc2	Songsa
for	forum	k1gNnPc2	forum
Drella	Drello	k1gNnSc2	Drello
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
Warholových	Warholový	k2eAgFnPc2d1	Warholový
přezdívek	přezdívka	k1gFnPc2	přezdívka
byla	být	k5eAaImAgFnS	být
právě	právě	k9	právě
Drella	Drella	k1gFnSc1	Drella
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
drákula	drákul	k1gMnSc2	drákul
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
dracula	dracout	k5eAaPmAgFnS	dracout
<g/>
)	)	kIx)	)
a	a	k8xC	a
popelka	popelka	k1gFnSc1	popelka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
cinderella	cinderella	k6eAd1	cinderella
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
Reed	Reed	k1gInSc1	Reed
Warholovi	Warholův	k2eAgMnPc1d1	Warholův
omlouvá	omlouvat	k5eAaImIp3nS	omlouvat
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
pro	pro	k7c4	pro
vždy	vždy	k6eAd1	vždy
vyřešit	vyřešit	k5eAaPmF	vyřešit
jejich	jejich	k3xOp3gInPc4	jejich
dřívější	dřívější	k2eAgInPc4d1	dřívější
konflikty	konflikt	k1gInPc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Warhol	Warhol	k1gInSc1	Warhol
obal	obal	k1gInSc4	obal
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
desky	deska	k1gFnPc4	deska
kapely	kapela	k1gFnSc2	kapela
The	The	k1gFnPc2	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gMnSc1	Stones
−	−	k?	−
Sticky	Sticka	k1gFnSc2	Sticka
Fingers	Fingersa	k1gFnPc2	Fingersa
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
a	a	k8xC	a
Love	lov	k1gInSc5	lov
You	You	k1gMnPc3	You
Live	Live	k1gNnPc4	Live
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
několik	několik	k4yIc4	několik
portrétů	portrét	k1gInPc2	portrét
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
této	tento	k3xDgFnSc2	tento
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
Micka	micka	k1gFnSc1	micka
Jaggera	Jaggera	k1gFnSc1	Jaggera
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
autorem	autor	k1gMnSc7	autor
obalů	obal	k1gInPc2	obal
dvou	dva	k4xCgMnPc6	dva
alb	album	k1gNnPc2	album
Johna	John	k1gMnSc2	John
Calea	Caleus	k1gMnSc2	Caleus
<g/>
,	,	kIx,	,
The	The	k1gMnSc2	The
Academy	Academa	k1gFnSc2	Academa
in	in	k?	in
Peril	Peril	k1gMnSc1	Peril
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
a	a	k8xC	a
Honi	Hone	k1gFnSc4	Hone
Soit	Soita	k1gFnPc2	Soita
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc1	Anda
Warhol	Warhol	k1gInSc1	Warhol
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
znal	znát	k5eAaImAgMnS	znát
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgMnPc7d1	další
hudebníky	hudebník	k1gMnPc7	hudebník
−	−	k?	−
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgMnPc7d1	jiný
i	i	k8xC	i
s	s	k7c7	s
Bobem	Bob	k1gMnSc7	Bob
Dylanem	Dylan	k1gMnSc7	Dylan
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
Lennonem	Lennon	k1gMnSc7	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
obal	obal	k1gInSc4	obal
Lennonovy	Lennonův	k2eAgFnSc2d1	Lennonova
posmrtně	posmrtně	k6eAd1	posmrtně
vydané	vydaný	k2eAgFnSc2d1	vydaná
desky	deska	k1gFnSc2	deska
Menlove	Menlov	k1gInSc5	Menlov
Avenue	avenue	k1gFnSc2	avenue
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
jako	jako	k9	jako
barman	barman	k1gMnSc1	barman
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
Again	Againa	k1gFnPc2	Againa
<g/>
"	"	kIx"	"
hudební	hudební	k2eAgFnPc1d1	hudební
skupiny	skupina	k1gFnPc1	skupina
The	The	k1gFnSc2	The
Cars	Carsa	k1gFnPc2	Carsa
a	a	k8xC	a
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
prvního	první	k4xOgInSc2	první
singlu	singl	k1gInSc2	singl
skupiny	skupina	k1gFnSc2	skupina
Curiosity	Curiosita	k1gFnSc2	Curiosita
Killed	Killed	k1gMnSc1	Killed
the	the	k?	the
Cat	Cat	k1gMnSc1	Cat
−	−	k?	−
"	"	kIx"	"
<g/>
Misfit	Misfit	k1gMnSc1	Misfit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
klipy	klip	k1gInPc1	klip
byly	být	k5eAaImAgInP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgMnPc7d1	další
vyrobeny	vyrobit	k5eAaPmNgFnP	vyrobit
Warhlovým	Warhlův	k2eAgNnSc7d1	Warhlův
oddělením	oddělení	k1gNnSc7	oddělení
Warhol	Warhol	k1gInSc1	Warhol
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
video	video	k1gNnSc1	video
production	production	k1gInSc4	production
company	compana	k1gFnSc2	compana
<g/>
.	.	kIx.	.
</s>
<s>
Warhol	Warhol	k1gInSc1	Warhol
dále	daleko	k6eAd2	daleko
silně	silně	k6eAd1	silně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
punk-rockovou	punkockový	k2eAgFnSc4d1	punk-rocková
formaci	formace	k1gFnSc4	formace
Devo	Devo	k1gMnSc1	Devo
a	a	k8xC	a
Davida	David	k1gMnSc2	David
Bowieho	Bowie	k1gMnSc4	Bowie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
tomto	tento	k3xDgMnSc6	tento
excentrickém	excentrický	k2eAgMnSc6d1	excentrický
umělci	umělec	k1gMnSc6	umělec
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
na	na	k7c6	na
albu	album	k1gNnSc6	album
Hunky	Hunka	k1gFnSc2	Hunka
Dory	Dora	k1gFnSc2	Dora
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
skupina	skupina	k1gFnSc1	skupina
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
<g/>
"	"	kIx"	"
na	na	k7c6	na
albu	album	k1gNnSc6	album
Vendetta	Vendett	k1gInSc2	Vendett
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Warholovi	Warhol	k1gMnSc6	Warhol
se	se	k3xPyFc4	se
tradovalo	tradovat	k5eAaImAgNnS	tradovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
homosexuál	homosexuál	k1gMnSc1	homosexuál
a	a	k8xC	a
také	také	k9	také
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
panic	panice	k1gFnPc2	panice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
ale	ale	k9	ale
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
zvěsti	zvěst	k1gFnPc1	zvěst
nezakládají	zakládat	k5eNaImIp3nP	zakládat
na	na	k7c6	na
pravdě	pravda	k1gFnSc6	pravda
a	a	k8xC	a
že	že	k8xS	že
Warhol	Warhol	k1gInSc1	Warhol
jen	jen	k9	jen
držel	držet	k5eAaImAgInS	držet
celibát	celibát	k1gInSc1	celibát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jaké	jaký	k3yRgFnSc2	jaký
míry	míra	k1gFnSc2	míra
Warholova	Warholův	k2eAgFnSc1d1	Warholova
sexualita	sexualita	k1gFnSc1	sexualita
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
utvářela	utvářet	k5eAaImAgFnS	utvářet
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
se	s	k7c7	s
světem	svět	k1gInSc7	svět
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
otázkou	otázka	k1gFnSc7	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgInSc7	tento
fenoménem	fenomén	k1gInSc7	fenomén
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
zabývá	zabývat	k5eAaImIp3nS	zabývat
většina	většina	k1gFnSc1	většina
výzkumu	výzkum	k1gInSc2	výzkum
týkajícího	týkající	k2eAgInSc2d1	týkající
se	se	k3xPyFc4	se
Warholovy	Warholův	k2eAgFnPc1d1	Warholova
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
často	často	k6eAd1	často
věnoval	věnovat	k5eAaImAgMnS	věnovat
při	při	k7c6	při
různých	různý	k2eAgNnPc6d1	různé
interview	interview	k1gNnPc6	interview
<g/>
,	,	kIx,	,
debatách	debata	k1gFnPc6	debata
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
ho	on	k3xPp3gNnSc4	on
i	i	k9	i
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
publikacích	publikace	k1gFnPc6	publikace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
memoárech	memoáry	k1gInPc6	memoáry
Popism	Popisma	k1gFnPc2	Popisma
<g/>
:	:	kIx,	:
<g/>
The	The	k1gFnSc1	The
Warhol	Warhol	k1gInSc1	Warhol
Sixties	Sixties	k1gInSc1	Sixties
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
Warhol	Warhol	k1gInSc1	Warhol
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
erotické	erotický	k2eAgFnPc4d1	erotická
fotografie	fotografia	k1gFnPc4	fotografia
i	i	k8xC	i
kresby	kresba	k1gFnPc4	kresba
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
byla	být	k5eAaImAgNnP	být
nahá	nahý	k2eAgNnPc1d1	nahé
mužská	mužský	k2eAgNnPc1d1	mužské
těla	tělo	k1gNnPc1	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
slavných	slavný	k2eAgNnPc2d1	slavné
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
portréty	portrét	k1gInPc1	portrét
Lizy	liz	k1gInPc4	liz
Minneliové	Minneliový	k2eAgNnSc1d1	Minneliový
<g/>
,	,	kIx,	,
Judy	judo	k1gNnPc7	judo
Garlandové	Garlandový	k2eAgFnSc2d1	Garlandová
a	a	k8xC	a
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Taylorové	Taylorová	k1gFnSc2	Taylorová
společně	společně	k6eAd1	společně
s	s	k7c7	s
filmy	film	k1gInPc7	film
jako	jako	k8xC	jako
Blow	Blow	k1gFnSc7	Blow
Job	Job	k1gMnSc1	Job
(	(	kIx(	(
<g/>
Kuřba	kuřba	k1gFnSc1	kuřba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
My	my	k3xPp1nPc1	my
Hustler	Hustler	k1gMnSc1	Hustler
(	(	kIx(	(
<g/>
Můj	můj	k3xOp1gMnSc1	můj
Prostitut	prostitut	k1gMnSc1	prostitut
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lonesome	Lonesom	k1gInSc5	Lonesom
Cowboys	Cowboys	k1gInSc1	Cowboys
(	(	kIx(	(
<g/>
Osamělí	osamělý	k2eAgMnPc1d1	osamělý
kovbojové	kovboj	k1gMnPc1	kovboj
<g/>
)	)	kIx)	)
čerpá	čerpat	k5eAaImIp3nS	čerpat
z	z	k7c2	z
undergroundové	undergroundový	k2eAgFnSc2d1	undergroundová
kultury	kultura	k1gFnSc2	kultura
homosexuálů	homosexuál	k1gMnPc2	homosexuál
nebo	nebo	k8xC	nebo
otevřeně	otevřeně	k6eAd1	otevřeně
prozkoumává	prozkoumávat	k5eAaImIp3nS	prozkoumávat
zákoutí	zákoutí	k1gNnSc4	zákoutí
lidské	lidský	k2eAgFnSc2d1	lidská
sexuality	sexualita	k1gFnSc2	sexualita
a	a	k8xC	a
sexuální	sexuální	k2eAgFnSc2d1	sexuální
touhy	touha	k1gFnSc2	touha
<g/>
.	.	kIx.	.
</s>
<s>
Nejeden	jeden	k2eNgInSc1d1	nejeden
jeho	jeho	k3xOp3gInSc1	jeho
film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
svou	svůj	k3xOyFgFnSc4	svůj
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
výlučně	výlučně	k6eAd1	výlučně
na	na	k7c4	na
pornofilmy	pornofilm	k1gInPc4	pornofilm
s	s	k7c7	s
homosexuální	homosexuální	k2eAgFnSc7d1	homosexuální
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
Warhol	Warhol	k1gInSc1	Warhol
předložil	předložit	k5eAaPmAgInS	předložit
galerii	galerie	k1gFnSc4	galerie
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
odmítnuty	odmítnout	k5eAaPmNgFnP	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tematiku	tematika	k1gFnSc4	tematika
homosexuality	homosexualita	k1gFnSc2	homosexualita
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
až	až	k9	až
příliš	příliš	k6eAd1	příliš
otevřeně	otevřeně	k6eAd1	otevřeně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
memoárech	memoáry	k1gInPc6	memoáry
navíc	navíc	k6eAd1	navíc
tento	tento	k3xDgMnSc1	tento
umělec	umělec	k1gMnSc1	umělec
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
filmařem	filmař	k1gMnSc7	filmař
Emilem	Emil	k1gMnSc7	Emil
de	de	k?	de
Antoniem	Antonio	k1gMnSc7	Antonio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
těžké	těžký	k2eAgFnPc1d1	těžká
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
bylo	být	k5eAaImAgNnS	být
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
umělce	umělec	k1gMnSc4	umělec
Jaspera	Jasper	k1gMnSc4	Jasper
Johnsona	Johnson	k1gMnSc4	Johnson
a	a	k8xC	a
Roberta	Robert	k1gMnSc4	Robert
Rauschenberga	Rauschenberg	k1gMnSc4	Rauschenberg
(	(	kIx(	(
<g/>
další	další	k2eAgMnPc4d1	další
slavné	slavný	k2eAgMnPc4d1	slavný
homosexuální	homosexuální	k2eAgMnPc4d1	homosexuální
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
sexuální	sexuální	k2eAgFnSc3d1	sexuální
orientaci	orientace	k1gFnSc3	orientace
nehlásili	hlásit	k5eNaImAgMnP	hlásit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gMnSc4	on
přijali	přijmout	k5eAaPmAgMnP	přijmout
mezi	mezi	k7c4	mezi
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Antonio	Antonio	k1gMnSc1	Antonio
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
vkus	vkus	k1gInSc4	vkus
"	"	kIx"	"
<g/>
vystavoval	vystavovat	k5eAaImAgInS	vystavovat
Warhol	Warhol	k1gInSc1	Warhol
svou	svůj	k3xOyFgFnSc4	svůj
homosexualitu	homosexualita	k1gFnSc4	homosexualita
až	až	k9	až
příliš	příliš	k6eAd1	příliš
na	na	k7c4	na
odiv	odiv	k1gInSc4	odiv
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gFnPc3	on
nebylo	být	k5eNaImAgNnS	být
vhod	vhod	k6eAd1	vhod
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
Warhol	Warhol	k1gInSc4	Warhol
reagoval	reagovat	k5eAaBmAgMnS	reagovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
jsem	být	k5eAaImIp1nS	být
nemohl	moct	k5eNaImAgMnS	moct
nic	nic	k3yNnSc1	nic
říct	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
totiž	totiž	k9	totiž
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
prostě	prostě	k6eAd1	prostě
nebudu	být	k5eNaImBp1nS	být
nic	nic	k3yNnSc1	nic
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsem	být	k5eAaImIp1nS	být
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
chování	chování	k1gNnSc6	chování
stejně	stejně	k6eAd1	stejně
nechtěl	chtít	k5eNaImAgMnS	chtít
nic	nic	k3yNnSc1	nic
měnit	měnit	k5eAaImF	měnit
a	a	k8xC	a
krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tohle	tenhle	k3xDgNnSc1	tenhle
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
věcem	věc	k1gFnPc3	věc
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bych	by	kYmCp1nS	by
ani	ani	k9	ani
neměl	mít	k5eNaImAgInS	mít
chtít	chtít	k5eAaImF	chtít
měnit	měnit	k5eAaImF	měnit
<g/>
...	...	k?	...
Jiní	jiný	k1gMnPc1	jiný
<g />
.	.	kIx.	.
</s>
<s>
lidé	člověk	k1gMnPc1	člověk
by	by	kYmCp3nP	by
možná	možná	k9	možná
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
změnit	změnit	k5eAaPmF	změnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
já	já	k3xPp1nSc1	já
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
nepatřím	patřit	k5eNaImIp1nS	patřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
A	a	k9	a
právě	právě	k9	právě
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
období	období	k1gNnSc1	období
−	−	k?	−
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
−	−	k?	−
se	se	k3xPyFc4	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
Warholův	Warholův	k2eAgInSc4d1	Warholův
život	život	k1gInSc4	život
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
<g/>
,	,	kIx,	,
staví	stavit	k5eAaImIp3nS	stavit
jako	jako	k9	jako
ke	k	k7c3	k
klíčovému	klíčový	k2eAgInSc3d1	klíčový
momentu	moment	k1gInSc3	moment
vývoje	vývoj	k1gInSc2	vývoj
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
neochota	neochota	k1gFnSc1	neochota
odpovídat	odpovídat	k5eAaImF	odpovídat
na	na	k7c4	na
dotazy	dotaz	k1gInPc4	dotaz
ohledně	ohledně	k7c2	ohledně
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
osoby	osoba	k1gFnSc2	osoba
samotné	samotný	k2eAgFnSc2d1	samotná
(	(	kIx(	(
<g/>
reakce	reakce	k1gFnSc2	reakce
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
hmmm	hmmm	k6eAd1	hmmm
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
hmmm	hmmm	k1gInSc1	hmmm
<g/>
,	,	kIx,	,
ano	ano	k9	ano
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k8xC	i
vývoj	vývoj	k1gInSc1	vývoj
jeho	on	k3xPp3gInSc2	on
pop-artového	poprtový	k2eAgInSc2d1	pop-artový
stylu	styl	k1gInSc2	styl
–	–	k?	–
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
následkem	následkem	k7c2	následkem
zážitků	zážitek	k1gInPc2	zážitek
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gNnSc2	on
newyorské	newyorský	k2eAgInPc1d1	newyorský
umělecké	umělecký	k2eAgInPc1d1	umělecký
kruhy	kruh	k1gInPc1	kruh
vyloučily	vyloučit	k5eAaPmAgFnP	vyloučit
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
most	most	k1gInSc1	most
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Allegheny	Alleghena	k1gFnSc2	Alleghena
v	v	k7c6	v
Pittsburghu	Pittsburgh	k1gInSc6	Pittsburgh
<g/>
.	.	kIx.	.
</s>
<s>
Warhol	Warhol	k1gInSc1	Warhol
byl	být	k5eAaImAgInS	být
jinými	jiný	k2eAgMnPc7d1	jiný
herci	herec	k1gMnPc7	herec
ztvárněn	ztvárněn	k2eAgInSc1d1	ztvárněn
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
−	−	k?	−
Warhola	Warhola	k1gFnSc1	Warhola
hrál	hrát	k5eAaImAgMnS	hrát
Crispin	Crispin	k2eAgMnSc1d1	Crispin
Glover	Glover	k1gMnSc1	Glover
Střelila	střelit	k5eAaPmAgFnS	střelit
jsem	být	k5eAaImIp1nS	být
Andyho	Andy	k1gMnSc4	Andy
Warhola	Warhola	k1gFnSc1	Warhola
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
−	−	k?	−
Warhola	Warhola	k1gFnSc1	Warhola
hrál	hrát	k5eAaImAgMnS	hrát
Jared	Jared	k1gMnSc1	Jared
Harris	Harris	k1gFnSc2	Harris
Basquiat	Basquiat	k1gInSc1	Basquiat
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
−	−	k?	−
Warhola	Warhola	k1gFnSc1	Warhola
hrál	hrát	k5eAaImAgMnS	hrát
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
Warholka	Warholka	k1gFnSc1	Warholka
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
−	−	k?	−
Warhola	Warhola	k1gFnSc1	Warhola
hrál	hrát	k5eAaImAgMnS	hrát
Guy	Guy	k1gMnSc1	Guy
Pearce	Pearce	k1gMnSc1	Pearce
Strážci	strážce	k1gMnSc3	strážce
–	–	k?	–
Watchmen	Watchmen	k1gInSc1	Watchmen
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
−	−	k?	−
Warhola	Warhola	k1gFnSc1	Warhola
hrál	hrát	k5eAaImAgMnS	hrát
Greg	Greg	k1gMnSc1	Greg
Travis	Travis	k1gFnSc2	Travis
Muži	muž	k1gMnPc1	muž
v	v	k7c6	v
černém	černé	k1gNnSc6	černé
3	[number]	k4	3
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
−	−	k?	−
Warhola	Warhola	k1gFnSc1	Warhola
hrál	hrát	k5eAaImAgMnS	hrát
Bill	Bill	k1gMnSc1	Bill
Hader	Hader	k1gMnSc1	Hader
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Filmografie	filmografie	k1gFnSc2	filmografie
Andyho	Andy	k1gMnSc2	Andy
Warhola	Warhola	k1gFnSc1	Warhola
<g/>
.	.	kIx.	.
</s>
