<s>
Slavkov	Slavkov	k1gInSc1	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Austerlitz	Austerlitz	k1gInSc1	Austerlitz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Vyškov	Vyškov	k1gInSc1	Vyškov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
20	[number]	k4	20
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Litava	Litava	k1gFnSc1	Litava
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Slavkov	Slavkov	k1gInSc1	Slavkov
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
především	především	k9	především
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
odehrála	odehrát	k5eAaPmAgFnS	odehrát
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
,	,	kIx,	,
přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
oblast	oblast	k1gFnSc1	oblast
Slavkovského	slavkovský	k2eAgNnSc2d1	slavkovské
bojiště	bojiště	k1gNnSc2	bojiště
je	být	k5eAaImIp3nS	být
krajinnou	krajinný	k2eAgFnSc7d1	krajinná
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
písemný	písemný	k2eAgInSc1d1	písemný
záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
městě	město	k1gNnSc6	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1237	[number]	k4	1237
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
Řádu	řád	k1gInSc6	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
držbu	držba	k1gFnSc4	držba
města	město	k1gNnSc2	město
Novosedlic	Novosedlice	k1gFnPc2	Novosedlice
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
ves	ves	k1gFnSc4	ves
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
Slavkov	Slavkov	k1gInSc1	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtyř	čtyři	k4xCgFnPc2	čtyři
okolních	okolní	k2eAgFnPc2d1	okolní
vsí	ves	k1gFnPc2	ves
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Austerlitz	Austerlitz	k1gInSc1	Austerlitz
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
"	"	kIx"	"
<g/>
Novosedlice	Novosedlice	k1gFnSc1	Novosedlice
<g/>
"	"	kIx"	"
→	→	k?	→
německy	německy	k6eAd1	německy
původně	původně	k6eAd1	původně
"	"	kIx"	"
<g/>
Nausedlitz	Nausedlitz	k1gInSc1	Nausedlitz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
získalo	získat	k5eAaPmAgNnS	získat
město	město	k1gNnSc4	město
pečeť	pečeť	k1gFnSc1	pečeť
a	a	k8xC	a
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgNnSc7d3	nejstarší
dochovaným	dochovaný	k2eAgNnSc7d1	dochované
znakovým	znakový	k2eAgNnSc7d1	znakové
privilegiem	privilegium	k1gNnSc7	privilegium
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
vystavěl	vystavět	k5eAaPmAgInS	vystavět
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
baštu	bašta	k1gFnSc4	bašta
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
zbytky	zbytek	k1gInPc4	zbytek
lze	lze	k6eAd1	lze
ještě	ještě	k6eAd1	ještě
dnes	dnes	k6eAd1	dnes
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
slavkovského	slavkovský	k2eAgInSc2d1	slavkovský
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konfiskaci	konfiskace	k1gFnSc6	konfiskace
králem	král	k1gMnSc7	král
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Lucemburským	lucemburský	k2eAgInSc7d1	lucemburský
počátkem	počátek	k1gInSc7	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
postupně	postupně	k6eAd1	postupně
stalo	stát	k5eAaPmAgNnS	stát
vlastnictvím	vlastnictví	k1gNnSc7	vlastnictví
mnoha	mnoho	k4c2	mnoho
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
nacházeli	nacházet	k5eAaImAgMnP	nacházet
útočiště	útočiště	k1gNnSc4	útočiště
i	i	k8xC	i
němečtí	německý	k2eAgMnPc1d1	německý
novokřtěnci	novokřtěnec	k1gMnPc1	novokřtěnec
-	-	kIx~	-
habáni	habán	k1gMnPc1	habán
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
i	i	k9	i
početná	početný	k2eAgFnSc1d1	početná
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1509	[number]	k4	1509
připadlo	připadnout	k5eAaPmAgNnS	připadnout
panství	panství	k1gNnSc1	panství
rodu	rod	k1gInSc2	rod
Kouniců	Kounice	k1gMnPc2	Kounice
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
ovládali	ovládat	k5eAaImAgMnP	ovládat
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
vlastníkem	vlastník	k1gMnSc7	vlastník
Slavkova	Slavkov	k1gInSc2	Slavkov
byl	být	k5eAaImAgMnS	být
kníže	kníže	k1gMnSc1	kníže
Václav	Václav	k1gMnSc1	Václav
Antonín	Antonín	k1gMnSc1	Antonín
Kounic	Kounice	k1gFnPc2	Kounice
(	(	kIx(	(
<g/>
1711	[number]	k4	1711
<g/>
-	-	kIx~	-
<g/>
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
dvorního	dvorní	k2eAgMnSc2d1	dvorní
kancléře	kancléř	k1gMnSc2	kancléř
sloužil	sloužit	k5eAaImAgInS	sloužit
rakouské	rakouský	k2eAgFnSc3d1	rakouská
císařovně	císařovna	k1gFnSc3	císařovna
Marii	Maria	k1gFnSc3	Maria
Terezii	Terezie	k1gFnSc3	Terezie
i	i	k8xC	i
dalším	další	k2eAgFnPc3d1	další
třem	tři	k4xCgFnPc3	tři
jejím	její	k3xOp3gMnPc3	její
následníkům	následník	k1gMnPc3	následník
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
panství	panství	k1gNnSc2	panství
byl	být	k5eAaImAgMnS	být
hrabě	hrabě	k1gMnSc1	hrabě
Václav	Václav	k1gMnSc1	Václav
Kounic	Kounice	k1gFnPc2	Kounice
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
švagr	švagr	k1gMnSc1	švagr
skladatele	skladatel	k1gMnSc2	skladatel
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
a	a	k8xC	a
studentský	studentský	k2eAgMnSc1d1	studentský
mecenáš	mecenáš	k1gMnSc1	mecenáš
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
ve	v	k7c6	v
Slavkově	Slavkov	k1gInSc6	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
slavkovský	slavkovský	k2eAgInSc1d1	slavkovský
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
115	[number]	k4	115
pokojů	pokoj	k1gInPc2	pokoj
a	a	k8xC	a
impozantní	impozantní	k2eAgInSc4d1	impozantní
zámecký	zámecký	k2eAgInSc4d1	zámecký
park	park	k1gInSc4	park
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
i	i	k8xC	i
anglickém	anglický	k2eAgInSc6d1	anglický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
italský	italský	k2eAgMnSc1d1	italský
architekt	architekt	k1gMnSc1	architekt
Domenico	Domenico	k1gMnSc1	Domenico
Martinelli	Martinelle	k1gFnSc4	Martinelle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Historickém	historický	k2eAgInSc6d1	historický
sále	sál	k1gInSc6	sál
zámku	zámek	k1gInSc2	zámek
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1805	[number]	k4	1805
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
příměří	příměří	k1gNnSc1	příměří
mezi	mezi	k7c7	mezi
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
lze	lze	k6eAd1	lze
navštívit	navštívit	k5eAaPmF	navštívit
dvě	dva	k4xCgFnPc4	dva
prohlídkové	prohlídkový	k2eAgFnPc4d1	prohlídková
trasy	trasa	k1gFnPc4	trasa
<g/>
,	,	kIx,	,
zámecké	zámecký	k2eAgNnSc4d1	zámecké
podzemí	podzemí	k1gNnSc4	podzemí
nebo	nebo	k8xC	nebo
expozici	expozice	k1gFnSc4	expozice
Napoleon	napoleon	k1gInSc1	napoleon
-	-	kIx~	-
Austerlitz	Austerlitz	k1gInSc1	Austerlitz
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc1	zámek
zapsán	zapsat	k5eAaPmNgInS	zapsat
mezi	mezi	k7c4	mezi
národní	národní	k2eAgFnPc4d1	národní
kulturní	kulturní	k2eAgFnPc4d1	kulturní
památky	památka	k1gFnPc4	památka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Pozdně	pozdně	k6eAd1	pozdně
renesanční	renesanční	k2eAgFnSc1d1	renesanční
radnice	radnice	k1gFnSc1	radnice
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
klasicistní	klasicistní	k2eAgFnSc6d1	klasicistní
úpravě	úprava	k1gFnSc6	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
Páně	páně	k2eAgNnSc2d1	páně
(	(	kIx(	(
<g/>
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ojedinělou	ojedinělý	k2eAgFnSc7d1	ojedinělá
církevní	církevní	k2eAgFnSc7d1	církevní
stavbou	stavba	k1gFnSc7	stavba
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
,	,	kIx,	,
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
naopak	naopak	k6eAd1	naopak
mnohé	mnohý	k2eAgInPc4d1	mnohý
kostely	kostel	k1gInPc4	kostel
rušil	rušit	k5eAaImAgMnS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Vystavěn	vystavěn	k2eAgMnSc1d1	vystavěn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1786	[number]	k4	1786
<g/>
-	-	kIx~	-
<g/>
1789	[number]	k4	1789
vídeňským	vídeňský	k2eAgMnSc7d1	vídeňský
architektem	architekt	k1gMnSc7	architekt
Johannem	Johann	k1gMnSc7	Johann
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Hetzendorfem	Hetzendorf	k1gMnSc7	Hetzendorf
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Urbana	Urban	k1gMnSc2	Urban
(	(	kIx(	(
<g/>
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
Urban	Urban	k1gMnSc1	Urban
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
slavkovské	slavkovský	k2eAgFnSc2d1	Slavkovská
bitvy	bitva	k1gFnSc2	bitva
poškozena	poškodit	k5eAaPmNgFnS	poškodit
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1861	[number]	k4	1861
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
pouze	pouze	k6eAd1	pouze
synagoga	synagoga	k1gFnSc1	synagoga
(	(	kIx(	(
<g/>
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
za	za	k7c7	za
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
středověkých	středověký	k2eAgFnPc2d1	středověká
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Lidická	lidický	k2eAgFnSc1d1	Lidická
a	a	k8xC	a
Kollárova	Kollárův	k2eAgFnSc1d1	Kollárova
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
památek	památka	k1gFnPc2	památka
Slavkovského	slavkovský	k2eAgNnSc2d1	slavkovské
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
odehrála	odehrát	k5eAaPmAgFnS	odehrát
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
Slavkova	Slavkov	k1gInSc2	Slavkov
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
k	k	k7c3	k
památným	památný	k2eAgNnPc3d1	památné
místům	místo	k1gNnPc3	místo
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
krajina	krajina	k1gFnSc1	krajina
si	se	k3xPyFc3	se
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
svůj	svůj	k3xOyFgInSc4	svůj
venkovský	venkovský	k2eAgInSc4d1	venkovský
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zvlněné	zvlněný	k2eAgFnSc6d1	zvlněná
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nezměnila	změnit	k5eNaPmAgFnS	změnit
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
rozrůstajících	rozrůstající	k2eAgFnPc2d1	rozrůstající
se	se	k3xPyFc4	se
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
převládá	převládat	k5eAaImIp3nS	převládat
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přetíná	přetínat	k5eAaImIp3nS	přetínat
bitevní	bitevní	k2eAgNnSc4d1	bitevní
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Mohyla	mohyla	k1gFnSc1	mohyla
míru	mír	k1gInSc2	mír
na	na	k7c6	na
Prateckém	pratecký	k2eAgInSc6d1	pratecký
kopci	kopec	k1gInSc6	kopec
-	-	kIx~	-
secesní	secesní	k2eAgInSc4d1	secesní
památník	památník	k1gInSc4	památník
bitvy	bitva	k1gFnSc2	bitva
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
poblíž	poblíž	k7c2	poblíž
obce	obec	k1gFnSc2	obec
Prace	Praec	k1gInSc2	Praec
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
a	a	k8xC	a
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
Josef	Josef	k1gMnSc1	Josef
Fanta	Fanta	k1gMnSc1	Fanta
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
odložila	odložit	k5eAaPmAgFnS	odložit
jeho	jeho	k3xOp3gNnSc4	jeho
otevření	otevření	k1gNnSc4	otevření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
až	až	k9	až
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
26	[number]	k4	26
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
budova	budova	k1gFnSc1	budova
se	s	k7c7	s
4	[number]	k4	4
ženskými	ženský	k2eAgFnPc7d1	ženská
sochami	socha	k1gFnPc7	socha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
památníku	památník	k1gInSc2	památník
je	být	k5eAaImIp3nS	být
také	také	k9	také
muzeum	muzeum	k1gNnSc1	muzeum
připomínající	připomínající	k2eAgFnSc4d1	připomínající
bitvu	bitva	k1gFnSc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
Santon	Santon	k1gInSc1	Santon
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Tvarožná	Tvarožný	k2eAgFnSc1d1	Tvarožná
stojí	stát	k5eAaImIp3nS	stát
malá	malý	k2eAgFnSc1d1	malá
bílá	bílý	k2eAgFnSc1d1	bílá
kaplička	kaplička	k1gFnSc1	kaplička
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
francouzského	francouzský	k2eAgNnSc2d1	francouzské
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pozice	pozice	k1gFnSc1	pozice
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Francouzům	Francouz	k1gMnPc3	Francouz
získat	získat	k5eAaPmF	získat
převahu	převaha	k1gFnSc4	převaha
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
bitevního	bitevní	k2eAgNnSc2d1	bitevní
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kopcem	kopec	k1gInSc7	kopec
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
konávají	konávat	k5eAaImIp3nP	konávat
rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
Žuráň	Žuráň	k1gFnSc1	Žuráň
měl	mít	k5eAaImAgInS	mít
Napoleon	napoleon	k1gInSc1	napoleon
během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
svůj	svůj	k3xOyFgInSc4	svůj
hlavní	hlavní	k2eAgInSc4d1	hlavní
stan	stan	k1gInSc4	stan
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
kopce	kopec	k1gInSc2	kopec
s	s	k7c7	s
památníkem	památník	k1gInSc7	památník
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
výsostným	výsostný	k2eAgNnSc7d1	výsostné
územím	území	k1gNnSc7	území
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
vinohrady	vinohrad	k1gInPc1	vinohrad
u	u	k7c2	u
Újezda	Újezdo	k1gNnSc2	Újezdo
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
svědky	svědek	k1gMnPc7	svědek
posledního	poslední	k2eAgNnSc2d1	poslední
krvavého	krvavý	k2eAgNnSc2d1	krvavé
střetnutí	střetnutí	k1gNnSc2	střetnutí
francouzských	francouzský	k2eAgFnPc2d1	francouzská
a	a	k8xC	a
ruských	ruský	k2eAgFnPc2d1	ruská
gard	garda	k1gFnPc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
kaplička	kaplička	k1gFnSc1	kaplička
a	a	k8xC	a
nově	nově	k6eAd1	nově
také	také	k9	také
Památník	památník	k1gInSc1	památník
Tří	tři	k4xCgMnPc2	tři
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
pošta	pošta	k1gFnSc1	pošta
v	v	k7c6	v
Koválovicích	Koválovice	k1gFnPc6	Koválovice
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgFnSc1d1	původní
historická	historický	k2eAgFnSc1d1	historická
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1805	[number]	k4	1805
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgInS	mít
svoje	svůj	k3xOyFgNnSc4	svůj
velitelství	velitelství	k1gNnSc4	velitelství
generál	generál	k1gMnSc1	generál
francouzské	francouzský	k2eAgFnSc2d1	francouzská
jízdy	jízda	k1gFnSc2	jízda
Murat	Murat	k1gInSc1	Murat
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
bitvy	bitva	k1gFnSc2	bitva
zde	zde	k6eAd1	zde
pobýval	pobývat	k5eAaImAgMnS	pobývat
ruský	ruský	k2eAgMnSc1d1	ruský
generál	generál	k1gMnSc1	generál
Bagration	Bagration	k1gInSc1	Bagration
a	a	k8xC	a
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
zde	zde	k6eAd1	zde
přespal	přespat	k5eAaPmAgInS	přespat
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
předběžná	předběžný	k2eAgNnPc4d1	předběžné
vyjednávání	vyjednávání	k1gNnPc4	vyjednávání
o	o	k7c4	o
příměří	příměří	k1gNnSc4	příměří
Napoleon	napoleon	k1gInSc4	napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
tyto	tento	k3xDgFnPc4	tento
události	událost	k1gFnPc4	událost
připomíná	připomínat	k5eAaImIp3nS	připomínat
malé	malý	k2eAgNnSc1d1	malé
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
až	až	k9	až
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
starostou	starosta	k1gMnSc7	starosta
Ivan	Ivan	k1gMnSc1	Ivan
Charvát	Charvát	k1gMnSc1	Charvát
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ustavujícím	ustavující	k2eAgNnSc6d1	ustavující
zasedání	zasedání	k1gNnSc6	zasedání
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Michal	Michal	k1gMnSc1	Michal
Boudný	Boudný	k2eAgMnSc1d1	Boudný
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgInPc4	první
organizované	organizovaný	k2eAgInPc4d1	organizovaný
sporty	sport	k1gInPc4	sport
ve	v	k7c6	v
Slavkově	Slavkov	k1gInSc6	Slavkov
patří	patřit	k5eAaImIp3nS	patřit
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Slavkov	Slavkov	k1gInSc1	Slavkov
(	(	kIx(	(
<g/>
SK	Sk	kA	Sk
Slavkov	Slavkov	k1gInSc1	Slavkov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mirko	Mirko	k1gMnSc1	Mirko
Hanák	Hanák	k1gMnSc1	Hanák
-	-	kIx~	-
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
František	František	k1gMnSc1	František
Koláček	Koláček	k1gMnSc1	Koláček
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
fyzik	fyzik	k1gMnSc1	fyzik
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
Peregrin	Peregrin	k1gInSc1	Peregrin
Obdržálek	Obdržálek	k1gMnSc1	Obdržálek
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
-	-	kIx~	-
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
,	,	kIx,	,
také	také	k9	také
autor	autor	k1gMnSc1	autor
náboženských	náboženský	k2eAgFnPc2d1	náboženská
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
satirických	satirický	k2eAgNnPc2d1	satirické
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
humoristických	humoristický	k2eAgFnPc2d1	humoristická
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
básní	báseň	k1gFnPc2	báseň
Jan	Jan	k1gMnSc1	Jan
Koláček	Koláček	k1gMnSc1	Koláček
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Alois	Alois	k1gMnSc1	Alois
Ličman	Ličman	k1gMnSc1	Ličman
-	-	kIx~	-
autor	autor	k1gMnSc1	autor
vlastivědy	vlastivěda	k1gFnSc2	vlastivěda
slavkovského	slavkovský	k2eAgInSc2d1	slavkovský
okresu	okres	k1gInSc2	okres
Václav	Václav	k1gMnSc1	Václav
Antonín	Antonín	k1gMnSc1	Antonín
z	z	k7c2	z
Kounic-Rietbergru	Kounic-Rietbergr	k1gInSc2	Kounic-Rietbergr
-	-	kIx~	-
kancléř	kancléř	k1gMnSc1	kancléř
čtyř	čtyři	k4xCgMnPc2	čtyři
císařů	císař	k1gMnPc2	císař
František	František	k1gMnSc1	František
Franc	Franc	k1gMnSc1	Franc
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
varhaník	varhaník	k1gMnSc1	varhaník
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
sbormistr	sbormistr	k1gMnSc1	sbormistr
Cyrilské	cyrilský	k2eAgFnSc2d1	Cyrilská
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
soukromé	soukromý	k2eAgFnSc2d1	soukromá
hudební	hudební	k2eAgFnSc2d1	hudební
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
nynější	nynější	k2eAgInSc1d1	nynější
ZUŠ	ZUŠ	kA	ZUŠ
<g />
.	.	kIx.	.
</s>
<s>
Fr.	Fr.	k?	Fr.
France	Franc	k1gMnSc4	Franc
Slavkov	Slavkov	k1gInSc1	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
)	)	kIx)	)
Jaromír	Jaromír	k1gMnSc1	Jaromír
Sedláček	Sedláček	k1gMnSc1	Sedláček
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
-	-	kIx~	-
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
děkan	děkan	k1gMnSc1	děkan
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
Emil	Emil	k1gMnSc1	Emil
Strach	strach	k1gInSc1	strach
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ochránce	ochránce	k1gMnSc1	ochránce
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Slavkovského	slavkovský	k2eAgMnSc2d1	slavkovský
okrašlovacího	okrašlovací	k2eAgMnSc2d1	okrašlovací
<g />
.	.	kIx.	.
</s>
<s>
spolku	spolek	k1gInSc2	spolek
František	František	k1gMnSc1	František
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
ředitel	ředitel	k1gMnSc1	ředitel
Partnerskými	partnerský	k2eAgNnPc7d1	partnerské
městy	město	k1gNnPc7	město
Slavkova	Slavkov	k1gInSc2	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Sławków	Sławków	k1gFnSc1	Sławków
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Horn	Horn	k1gMnSc1	Horn
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Austerlitz	Austerlitza	k1gFnPc2	Austerlitza
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
Darney	Darnea	k1gFnSc2	Darnea
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Francouzi	Francouz	k1gMnSc3	Francouz
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
vítězství	vítězství	k1gNnSc2	vítězství
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
železniční	železniční	k2eAgFnSc3d1	železniční
stanici	stanice	k1gFnSc3	stanice
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Gare	Gar	k1gInSc2	Gar
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Austerlitz	Austerlitz	k1gMnSc1	Austerlitz
a	a	k8xC	a
také	také	k9	také
přilehlý	přilehlý	k2eAgInSc1d1	přilehlý
most	most	k1gInSc1	most
Pont	Pont	k1gInSc1	Pont
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Austerlitz	Austerlitz	k1gInSc4	Austerlitz
a	a	k8xC	a
nábřeží	nábřeží	k1gNnSc4	nábřeží
Quai	quai	k1gNnSc2	quai
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Austerlitz	Austerlitz	k1gMnSc1	Austerlitz
<g/>
.	.	kIx.	.
</s>
