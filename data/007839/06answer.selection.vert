<s>
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pojmem	pojem	k1gInSc7	pojem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
získal	získat	k5eAaPmAgInS	získat
mnoho	mnoho	k4c4	mnoho
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
jednoho	jeden	k4xCgMnSc4	jeden
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
31	[number]	k4	31
cen	cena	k1gFnPc2	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
cen	cena	k1gFnPc2	cena
BAFTA	BAFTA	kA	BAFTA
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
dal	dát	k5eAaPmAgMnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
široké	široký	k2eAgFnSc3d1	široká
základně	základna	k1gFnSc3	základna
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
trekkies	trekkies	k1gInSc1	trekkies
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
např.	např.	kA	např.
Bill	Bill	k1gMnSc1	Bill
Gates	Gates	k1gMnSc1	Gates
<g/>
,	,	kIx,	,
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
nebo	nebo	k8xC	nebo
Whoopi	Whoopi	k1gNnPc1	Whoopi
Goldbergová	Goldbergový	k2eAgNnPc1d1	Goldbergový
<g/>
.	.	kIx.	.
</s>
