<p>
<s>
Federální	federální	k2eAgFnSc1d1	federální
služba	služba	k1gFnSc1	služba
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
FSB	FSB	kA	FSB
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Ф	Ф	k?	Ф
с	с	k?	с
б	б	k?	б
Р	Р	k?	Р
Ф	Ф	k?	Ф
–	–	k?	–
Feděralnaja	Feděralnaj	k2eAgFnSc1d1	Feděralnaj
služba	služba	k1gFnSc1	služba
bezopasnosti	bezopasnost	k1gFnSc2	bezopasnost
Rossijskoj	Rossijskoj	k1gInSc1	Rossijskoj
Feděracii	Feděracie	k1gFnSc4	Feděracie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
domácí	domácí	k2eAgFnSc1d1	domácí
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
služba	služba	k1gFnSc1	služba
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
následník	následník	k1gMnSc1	následník
Výboru	výbor	k1gInSc2	výbor
státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
zajištění	zajištění	k1gNnSc1	zajištění
úlohy	úloha	k1gFnSc2	úloha
kontrarozvědky	kontrarozvědka	k1gFnSc2	kontrarozvědka
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
ředitelství	ředitelství	k1gNnSc1	ředitelství
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
služba	služba	k1gFnSc1	služba
je	být	k5eAaImIp3nS	být
řízena	řízen	k2eAgFnSc1d1	řízena
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Bortnikov	Bortnikov	k1gInSc4	Bortnikov
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
služba	služba	k1gFnSc1	služba
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
údajně	údajně	k6eAd1	údajně
až	až	k6eAd1	až
dvě	dva	k4xCgNnPc4	dva
stě	sto	k4xCgFnPc1	sto
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
FSB	FSB	kA	FSB
byla	být	k5eAaImAgFnS	být
Federální	federální	k2eAgFnSc1d1	federální
služba	služba	k1gFnSc1	služba
kontrarozvědky	kontrarozvědka	k1gFnSc2	kontrarozvědka
(	(	kIx(	(
<g/>
FSK	FSK	kA	FSK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Boris	Boris	k1gMnSc1	Boris
Jelcin	Jelcin	k1gMnSc1	Jelcin
podepsal	podepsat	k5eAaPmAgMnS	podepsat
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1995	[number]	k4	1995
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nařizoval	nařizovat	k5eAaImAgInS	nařizovat
reorganizaci	reorganizace	k1gFnSc4	reorganizace
FSK	FSK	kA	FSK
a	a	k8xC	a
touto	tento	k3xDgFnSc7	tento
reorganizací	reorganizace	k1gFnSc7	reorganizace
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
FSB	FSB	kA	FSB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Otrava	otrava	k1gFnSc1	otrava
Alexandra	Alexandra	k1gFnSc1	Alexandra
Litviněnka	Litviněnka	k1gFnSc1	Litviněnka
</s>
</p>
<p>
<s>
Armáda	armáda	k1gFnSc1	armáda
trollů	troll	k1gMnPc2	troll
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Federal	Federal	k1gFnSc2	Federal
Security	Securita	k1gFnSc2	Securita
Service	Service	k1gFnSc2	Service
(	(	kIx(	(
<g/>
Russia	Russia	k1gFnSc1	Russia
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Federální	federální	k2eAgFnSc1d1	federální
služba	služba	k1gFnSc1	služba
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
