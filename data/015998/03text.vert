<s>
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
</s>
<s>
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivovéAutor	detektivovéAutor	k1gMnSc1
</s>
<s>
Erich	Erich	k1gMnSc1
Kästner	Kästner	k1gMnSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Emil	Emil	k1gMnSc1
und	und	k?
die	die	k?
Detektive	detektiv	k1gMnSc5
Země	zem	k1gFnSc2
</s>
<s>
Německo	Německo	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
němčina	němčina	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
kriminální	kriminální	k2eAgInSc4d1
román	román	k1gInSc4
Datum	datum	k1gNnSc4
vydání	vydání	k1gNnSc2
</s>
<s>
1929	#num#	k4
Předchozí	předchozí	k2eAgInSc4d1
a	a	k8xC
následující	následující	k2eAgInSc4d1
díl	díl	k1gInSc4
</s>
<s>
Emil	Emil	k1gMnSc1
a	a	k8xC
tři	tři	k4xCgNnPc4
dvojčata	dvojče	k1gNnPc4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
und	und	k?
die	die	k?
Detektive	detektiv	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
detektivní	detektivní	k2eAgFnSc1d1
povídka	povídka	k1gFnSc1
určená	určený	k2eAgFnSc1d1
dětem	dítě	k1gFnPc3
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
německý	německý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Erich	Erich	k1gMnSc1
Kästner	Kästner	k1gMnSc1
a	a	k8xC
ilustroval	ilustrovat	k5eAaBmAgMnS
Walter	Walter	k1gMnSc1
Trier	Trier	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gNnSc7
pokračováním	pokračování	k1gNnSc7
je	být	k5eAaImIp3nS
kniha	kniha	k1gFnSc1
Emil	Emil	k1gMnSc1
a	a	k8xC
tři	tři	k4xCgNnPc1
dvojčata	dvojče	k1gNnPc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
und	und	k?
die	die	k?
drei	dre	k1gFnSc2
Zwillinge	Zwilling	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Obsah	obsah	k1gInSc1
povídky	povídka	k1gFnSc2
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Emil	Emil	k1gMnSc1
Tischbein	Tischbein	k1gMnSc1
je	být	k5eAaImIp3nS
chlapec	chlapec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
žije	žít	k5eAaImIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
maminkou	maminka	k1gFnSc7
v	v	k7c6
Neustadtu	Neustadto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tatínka	tatínek	k1gMnSc4
už	už	k9
Emil	Emil	k1gMnSc1
nemá	mít	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
maminka	maminka	k1gFnSc1
doma	doma	k6eAd1
pilně	pilně	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
jako	jako	k9
kadeřnice	kadeřnice	k1gFnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Emil	Emil	k1gMnSc1
mohl	moct	k5eAaImAgMnS
chodit	chodit	k5eAaImF
na	na	k7c4
reálku	reálka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moc	moc	k1gFnSc1
peněz	peníze	k1gInPc2
proto	proto	k8xC
nemají	mít	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Emil	Emil	k1gMnSc1
je	být	k5eAaImIp3nS
pozván	pozvat	k5eAaPmNgMnS
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
tetě	teta	k1gFnSc3
a	a	k8xC
babičce	babička	k1gFnSc3
do	do	k7c2
Berlína	Berlín	k1gInSc2
na	na	k7c4
prázdniny	prázdniny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maminka	maminka	k1gFnSc1
mu	on	k3xPp3gMnSc3
dá	dát	k5eAaPmIp3nS
na	na	k7c4
cestu	cesta	k1gFnSc4
140	#num#	k4
marek	marka	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gInPc4
odevzdal	odevzdat	k5eAaPmAgMnS
babičce	babička	k1gFnSc3
na	na	k7c4
přilepšenou	přilepšená	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
oba	dva	k4xCgInPc4
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
dost	dost	k6eAd1
velké	velký	k2eAgInPc1d1
peníze	peníz	k1gInPc1
a	a	k8xC
Emil	Emil	k1gMnSc1
si	se	k3xPyFc3
proto	proto	k8xC
obálku	obálka	k1gFnSc4
s	s	k7c7
bankovkami	bankovka	k1gFnPc7
přišpendlí	přišpendlit	k5eAaPmIp3nP
k	k	k7c3
podšívce	podšívka	k1gFnSc3
kabátu	kabát	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jí	on	k3xPp3gFnSc3
neztratil	ztratit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
ve	v	k7c6
vlaku	vlak	k1gInSc6
usne	usnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
spolucestující	spolucestující	k1gMnSc1
<g/>
,	,	kIx,
jakýsi	jakýsi	k3yIgMnSc1
pan	pan	k1gMnSc1
Grundeis	Grundeis	k1gFnPc2
<g/>
,	,	kIx,
mu	on	k3xPp3gInSc3
jí	on	k3xPp3gFnSc3
ukradne	ukradnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emil	Emil	k1gMnSc1
jej	on	k3xPp3gMnSc4
v	v	k7c6
Berlíně	Berlín	k1gInSc6
na	na	k7c6
nádraží	nádraží	k1gNnSc6
naštěstí	naštěstí	k6eAd1
zahlédne	zahlédnout	k5eAaPmIp3nS
a	a	k8xC
sleduje	sledovat	k5eAaImIp3nS
ho	on	k3xPp3gNnSc4
až	až	k9
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
hotelu	hotel	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
seznámí	seznámit	k5eAaPmIp3nS
s	s	k7c7
chlapcem	chlapec	k1gMnSc7
Gustavem	Gustav	k1gMnSc7
a	a	k8xC
ten	ten	k3xDgMnSc1
mu	on	k3xPp3gMnSc3
pomůže	pomoct	k5eAaPmIp3nS
získat	získat	k5eAaPmF
pro	pro	k7c4
pátrání	pátrání	k1gNnSc4
celou	celý	k2eAgFnSc4d1
partu	parta	k1gFnSc4
berlínských	berlínský	k2eAgMnPc2d1
chlapců	chlapec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
může	moct	k5eAaImIp3nS
také	také	k9
Emil	Emil	k1gMnSc1
poslat	poslat	k5eAaPmF
zprávu	zpráva	k1gFnSc4
své	svůj	k3xOyFgFnSc3
tetě	teta	k1gFnSc3
a	a	k8xC
babičce	babička	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
neztratil	ztratit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jaký	jaký	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
byl	být	k5eAaImAgInS
z	z	k7c2
toho	ten	k3xDgNnSc2
poplach	poplach	k1gInSc1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
Emil	Emil	k1gMnSc1
u	u	k7c2
tety	teta	k1gFnSc2
neobjevil	objevit	k5eNaPmAgMnS
<g/>
,	,	kIx,
mu	on	k3xPp3gNnSc3
vypráví	vyprávět	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
sestřenice	sestřenice	k1gFnSc1
Pony	pony	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
nelenila	lenit	k5eNaImAgFnS
a	a	k8xC
přijela	přijet	k5eAaPmAgFnS
za	za	k7c7
Emilem	Emil	k1gMnSc7
na	na	k7c6
kole	kolo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Hoch	hoch	k1gMnSc1
s	s	k7c7
přezdívkou	přezdívka	k1gFnSc7
Profesor	profesor	k1gMnSc1
skvěle	skvěle	k6eAd1
organizuje	organizovat	k5eAaBmIp3nS
sledování	sledování	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
chlapci	chlapec	k1gMnPc1
dokáží	dokázat	k5eAaPmIp3nP
vše	všechen	k3xTgNnSc4
vyžvanit	vyžvanit	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
pronásledování	pronásledování	k1gNnSc1
zloděje	zloděj	k1gMnPc4
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
zúčastní	zúčastnit	k5eAaPmIp3nS
snad	snad	k9
stovka	stovka	k1gFnSc1
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
zpanikaří	zpanikařit	k5eAaPmIp3nS
a	a	k8xC
chce	chtít	k5eAaImIp3nS
peníze	peníz	k1gInPc4
uložit	uložit	k5eAaPmF
do	do	k7c2
banky	banka	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Emilovi	Emilův	k2eAgMnPc1d1
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
podaří	podařit	k5eAaPmIp3nS
usvědčit	usvědčit	k5eAaPmF
z	z	k7c2
krádeže	krádež	k1gFnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
bankovkách	bankovka	k1gFnPc6
jsou	být	k5eAaImIp3nP
objeveny	objevit	k5eAaPmNgInP
vpichy	vpich	k1gInPc1
po	po	k7c6
špendlíku	špendlík	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zloděj	zloděj	k1gMnSc1
je	být	k5eAaImIp3nS
dlouho	dlouho	k6eAd1
hledaný	hledaný	k2eAgMnSc1d1
bankovní	bankovní	k2eAgMnSc1d1
lupič	lupič	k1gMnSc1
a	a	k8xC
Emil	Emil	k1gMnSc1
obdrží	obdržet	k5eAaPmIp3nS
odměnu	odměna	k1gFnSc4
za	za	k7c4
dopadení	dopadení	k1gNnSc4
1000	#num#	k4
marek	marka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1
adaptace	adaptace	k1gFnPc1
</s>
<s>
Emil	Emil	k1gMnSc1
und	und	k?
die	die	k?
Detektive	detektiv	k1gMnSc5
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
německý	německý	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Gerhard	Gerhard	k1gMnSc1
Lamprecht	Lamprecht	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Emil	Emil	k1gMnSc1
and	and	k?
the	the	k?
Detectives	Detectives	k1gInSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
britský	britský	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Milton	Milton	k1gInSc1
Rosmer	Rosmer	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Toscanito	Toscanit	k2eAgNnSc1d1
y	y	k?
los	los	k1gInSc1
detectives	detectives	k1gInSc1
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
italský	italský	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Antonio	Antonio	k1gMnSc1
Momplet	Momplet	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Emil	Emil	k1gMnSc1
and	and	k?
the	the	k?
Detectives	Detectives	k1gInSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
britský	britský	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Emil	Emil	k1gMnSc1
und	und	k?
die	die	k?
Detektive	detektiv	k1gMnSc5
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
německý	německý	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Robert	Robert	k1gMnSc1
A.	A.	kA
Stemmle	Stemmle	k1gFnPc1
<g/>
,	,	kIx,
</s>
<s>
Emil	Emil	k1gMnSc1
and	and	k?
the	the	k?
Detectives	Detectives	k1gInSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
americký	americký	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Peter	Peter	k1gMnSc1
Tewksbury	Tewksbura	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
Emil	Emil	k1gMnSc1
und	und	k?
die	die	k?
Detektive	detektiv	k1gMnSc5
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
německý	německý	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Franziska	Franziska	k1gFnSc1
Buch	buch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
</s>
<s>
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Synek	Synek	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1932	#num#	k4
<g/>
,	,	kIx,
přeložila	přeložit	k5eAaPmAgFnS
Jarmila	Jarmila	k1gFnSc1
Hašková	Hašková	k1gFnSc1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
1934	#num#	k4
a	a	k8xC
1937	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
<g/>
,	,	kIx,
SNDK	SNDK	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1957	#num#	k4
<g/>
,	,	kIx,
přeložila	přeložit	k5eAaPmAgFnS
Jitka	Jitka	k1gFnSc1
Fučíková	Fučíková	k1gFnSc1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
1959	#num#	k4
a	a	k8xC
1966	#num#	k4
<g/>
,	,	kIx,
Albatros	albatros	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1972	#num#	k4
<g/>
,	,	kIx,
Amulet	amulet	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1997	#num#	k4
a	a	k8xC
XYZ	XYZ	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Příběhy	příběh	k1gInPc1
z	z	k7c2
dětství	dětství	k1gNnSc2
i	i	k8xC
dospělosti	dospělost	k1gFnSc2
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1979	#num#	k4
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
i	i	k9
povídku	povídka	k1gFnSc4
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
v	v	k7c6
překladu	překlad	k1gInSc6
Jitky	Jitka	k1gFnSc2
Fučíkové	Fučíková	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Emil	Emil	k1gMnSc1
a	a	k8xC
detektivové	detektiv	k1gMnPc1
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
a	a	k8xC
tři	tři	k4xCgNnPc4
dvojčata	dvojče	k1gNnPc4
<g/>
,	,	kIx,
SNDK	SNDK	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1968	#num#	k4
<g/>
,	,	kIx,
přeložila	přeložit	k5eAaPmAgFnS
Jitka	Jitka	k1gFnSc1
Fučíková	Fučíková	k1gFnSc1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
Albatros	albatros	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1979	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
a	a	k8xC
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
http://www.kodovky.cz/kniha/17	http://www.kodovky.cz/kniha/17	k4
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Emil	Emil	k1gMnSc1
und	und	k?
die	die	k?
Detektive	detektiv	k1gMnSc5
<g/>
,	,	kIx,
Zentral-und	Zentral-und	k1gInSc4
Landesbibliothek	Landesbibliothky	k1gFnPc2
Berlin	berlina	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
