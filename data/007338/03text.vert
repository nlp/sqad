<s>
Žabky	žabka	k1gFnPc1	žabka
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
vietnamky	vietnamka	k1gFnSc2	vietnamka
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
především	především	k9	především
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
či	či	k8xC	či
plážovky	plážovka	k1gFnPc1	plážovka
nebo	nebo	k8xC	nebo
ťapky	ťapka	k1gFnPc1	ťapka
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
šlapky	šlapka	k1gFnSc2	šlapka
<g/>
,	,	kIx,	,
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
Moravě	Morava	k1gFnSc6	Morava
cukle	cukle	k1gFnSc2	cukle
<g/>
;	;	kIx,	;
další	další	k2eAgNnSc1d1	další
pojmenování	pojmenování	k1gNnSc1	pojmenování
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
v	v	k7c6	v
článku	článek	k1gInSc6	článek
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
celosvětově	celosvětově	k6eAd1	celosvětově
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
druhem	druh	k1gInSc7	druh
otevřené	otevřený	k2eAgInPc4d1	otevřený
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
letní	letní	k2eAgFnSc2d1	letní
<g/>
)	)	kIx)	)
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
charakteristikou	charakteristika	k1gFnSc7	charakteristika
je	být	k5eAaImIp3nS	být
pásek	pásek	k1gInSc1	pásek
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
Y	Y	kA	Y
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
při	při	k7c6	při
nazutí	nazutí	k1gNnSc6	nazutí
obepíná	obepínat	k5eAaImIp3nS	obepínat
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pásky	pásek	k1gInPc1	pásek
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
obuvi	obuv	k1gFnSc3	obuv
obvykle	obvykle	k6eAd1	obvykle
připevněny	připevněn	k2eAgFnPc4d1	připevněna
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
nártu	nárt	k1gInSc2	nárt
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
mezi	mezi	k7c7	mezi
palcem	palec	k1gInSc7	palec
a	a	k8xC	a
ukazovákem	ukazovák	k1gInSc7	ukazovák
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
noha	noha	k1gFnSc1	noha
volně	volně	k6eAd1	volně
dýchat	dýchat	k5eAaImF	dýchat
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
prstům	prst	k1gInPc3	prst
umožněn	umožnit	k5eAaPmNgInS	umožnit
volný	volný	k2eAgInSc1d1	volný
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
sandálům	sandál	k1gInPc3	sandál
žabky	žabka	k1gFnSc2	žabka
pevně	pevně	k6eAd1	pevně
nezajišťují	zajišťovat	k5eNaImIp3nP	zajišťovat
kotník	kotník	k1gInSc4	kotník
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
dochované	dochovaný	k2eAgFnPc1d1	dochovaná
žabky	žabka	k1gFnPc1	žabka
jsou	být	k5eAaImIp3nP	být
cca	cca	kA	cca
3500	[number]	k4	3500
let	léto	k1gNnPc2	léto
staré	starý	k2eAgInPc1d1	starý
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
uložené	uložený	k2eAgInPc1d1	uložený
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Obuv	obuv	k1gFnSc1	obuv
se	se	k3xPyFc4	se
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
podobě	podoba	k1gFnSc6	podoba
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
následně	následně	k6eAd1	následně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Žabky	žabka	k1gFnPc1	žabka
jsou	být	k5eAaImIp3nP	být
též	též	k6eAd1	též
vhodným	vhodný	k2eAgNnSc7d1	vhodné
preventivním	preventivní	k2eAgNnSc7d1	preventivní
obutím	obutí	k1gNnSc7	obutí
i	i	k9	i
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
veřejných	veřejný	k2eAgFnPc2d1	veřejná
sprch	sprcha	k1gFnPc2	sprcha
či	či	k8xC	či
koupališť	koupaliště	k1gNnPc2	koupaliště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
hrozit	hrozit	k5eAaImF	hrozit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
šíření	šíření	k1gNnSc2	šíření
kožních	kožní	k2eAgFnPc2d1	kožní
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
výhod	výhoda	k1gFnPc2	výhoda
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
obuvi	obuv	k1gFnPc1	obuv
vytýkány	vytýkán	k2eAgFnPc1d1	vytýkána
i	i	k9	i
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zdravotní	zdravotní	k2eAgFnSc6d1	zdravotní
a	a	k8xC	a
environmentální	environmentální	k2eAgFnSc6d1	environmentální
<g/>
.	.	kIx.	.
</s>
<s>
Podešev	podešev	k1gFnSc1	podešev
obuvi	obuv	k1gFnSc2	obuv
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
obvykle	obvykle	k6eAd1	obvykle
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
ze	z	k7c2	z
syntetických	syntetický	k2eAgInPc2d1	syntetický
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
PVC	PVC	kA	PVC
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
boty	bota	k1gFnPc1	bota
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
lehké	lehký	k2eAgFnPc1d1	lehká
a	a	k8xC	a
skladné	skladný	k2eAgFnPc1d1	skladná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
se	se	k3xPyFc4	se
ale	ale	k9	ale
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
palmové	palmový	k2eAgNnSc1d1	palmové
lýko	lýko	k1gNnSc1	lýko
<g/>
,	,	kIx,	,
sláma	sláma	k1gFnSc1	sláma
či	či	k8xC	či
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Pásky	páska	k1gFnPc1	páska
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
také	také	k9	také
ze	z	k7c2	z
syntetických	syntetický	k2eAgInPc2d1	syntetický
materiálů	materiál	k1gInPc2	materiál
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
z	z	k7c2	z
látky	látka	k1gFnSc2	látka
či	či	k8xC	či
provázků	provázek	k1gInPc2	provázek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
dané	daný	k2eAgFnPc1d1	daná
boty	bota	k1gFnPc1	bota
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pásky	pásek	k1gInPc4	pásek
ještě	ještě	k6eAd1	ještě
dozdobeny	dozdoben	k2eAgInPc4d1	dozdoben
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
barevným	barevný	k2eAgInSc7d1	barevný
potiskem	potisk	k1gInSc7	potisk
<g/>
,	,	kIx,	,
korálky	korálek	k1gInPc7	korálek
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
lze	lze	k6eAd1	lze
i	i	k9	i
podešev	podešev	k1gFnSc1	podešev
barevně	barevně	k6eAd1	barevně
<g/>
,	,	kIx,	,
graficky	graficky	k6eAd1	graficky
či	či	k8xC	či
tvarově	tvarově	k6eAd1	tvarově
upravit	upravit	k5eAaPmF	upravit
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tuto	tento	k3xDgFnSc4	tento
obuv	obuv	k1gFnSc4	obuv
použít	použít	k5eAaPmF	použít
i	i	k9	i
jako	jako	k8xC	jako
reklamní	reklamní	k2eAgInSc4d1	reklamní
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
podrážka	podrážka	k1gFnSc1	podrážka
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c6	o
konstrukci	konstrukce	k1gFnSc6	konstrukce
připomínající	připomínající	k2eAgNnSc4d1	připomínající
lešení	lešení	k1gNnSc4	lešení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tuto	tento	k3xDgFnSc4	tento
obuv	obuv	k1gFnSc4	obuv
nosit	nosit	k5eAaImF	nosit
i	i	k9	i
v	v	k7c6	v
blátě	bláto	k1gNnSc6	bláto
rýžoviště	rýžoviště	k1gNnSc2	rýžoviště
či	či	k8xC	či
ve	v	k7c6	v
sněhu	sníh	k1gInSc6	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
tato	tento	k3xDgFnSc1	tento
obuv	obuv	k1gFnSc1	obuv
nošena	nosit	k5eAaImNgFnS	nosit
na	na	k7c4	na
bosou	bosý	k2eAgFnSc4d1	bosá
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
nošena	nošen	k2eAgFnSc1d1	nošena
i	i	k9	i
se	s	k7c7	s
speciálními	speciální	k2eAgFnPc7d1	speciální
ponožkami	ponožka	k1gFnPc7	ponožka
(	(	kIx(	(
<g/>
pojmenovanými	pojmenovaný	k2eAgNnPc7d1	pojmenované
tabi	tabi	k1gNnPc7	tabi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
oddělením	oddělení	k1gNnSc7	oddělení
palce	palec	k1gInSc2	palec
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
jsou	být	k5eAaImIp3nP	být
ideální	ideální	k2eAgMnPc1d1	ideální
na	na	k7c4	na
nošení	nošení	k1gNnSc4	nošení
v	v	k7c6	v
žabkách	žabka	k1gFnPc6	žabka
<g/>
.	.	kIx.	.
</s>
<s>
Žabky	žabka	k1gFnPc1	žabka
jsou	být	k5eAaImIp3nP	být
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
především	především	k9	především
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
teplým	teplý	k2eAgNnSc7d1	teplé
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gInPc4	on
lidé	člověk	k1gMnPc1	člověk
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
procházkám	procházka	k1gFnPc3	procházka
po	po	k7c6	po
pláži	pláž	k1gFnSc6	pláž
nebo	nebo	k8xC	nebo
i	i	k9	i
ke	k	k7c3	k
kratším	krátký	k2eAgInPc3d2	kratší
výletům	výlet	k1gInPc3	výlet
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
(	(	kIx(	(
<g/>
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
říká	říkat	k5eAaImIp3nS	říkat
Hawaii	Hawaie	k1gFnSc4	Hawaie
chappal	chappat	k5eAaImAgMnS	chappat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgFnPc2d1	americká
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Panamě	Panama	k1gFnSc6	Panama
<g/>
,	,	kIx,	,
na	na	k7c6	na
tichomořských	tichomořský	k2eAgInPc6d1	tichomořský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
gumové	gumový	k2eAgFnSc2d1	gumová
žabky	žabka	k1gFnSc2	žabka
tou	ten	k3xDgFnSc7	ten
nejlevnější	levný	k2eAgFnSc7d3	nejlevnější
obuví	obuv	k1gFnSc7	obuv
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
totiž	totiž	k9	totiž
stojí	stát	k5eAaImIp3nS	stát
méně	málo	k6eAd2	málo
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byla	být	k5eAaImAgNnP	být
přijata	přijmout	k5eAaPmNgNnP	přijmout
opatření	opatření	k1gNnPc1	opatření
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
nákladů	náklad	k1gInPc2	náklad
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
výrobou	výroba	k1gFnSc7	výroba
této	tento	k3xDgFnSc2	tento
obuvi	obuv	k1gFnSc2	obuv
z	z	k7c2	z
recyklovaných	recyklovaný	k2eAgInPc2d1	recyklovaný
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
obuv	obuv	k1gFnSc1	obuv
poškodí	poškodit	k5eAaPmIp3nS	poškodit
(	(	kIx(	(
<g/>
vytrhne	vytrhnout	k5eAaPmIp3nS	vytrhnout
se	se	k3xPyFc4	se
pásek	pásek	k1gInSc1	pásek
obepínající	obepínající	k2eAgFnSc4d1	obepínající
nohu	noha	k1gFnSc4	noha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
a	a	k8xC	a
za	za	k7c4	za
nízkou	nízký	k2eAgFnSc4d1	nízká
cenu	cena	k1gFnSc4	cena
opravit	opravit	k5eAaPmF	opravit
u	u	k7c2	u
některého	některý	k3yIgNnSc2	některý
z	z	k7c2	z
pouličních	pouliční	k2eAgMnPc2d1	pouliční
opravářů	opravář	k1gMnPc2	opravář
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
jsou	být	k5eAaImIp3nP	být
žabky	žabka	k1gFnPc4	žabka
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
roční	roční	k2eAgInSc4d1	roční
nebo	nebo	k8xC	nebo
sezónní	sezónní	k2eAgInSc4d1	sezónní
druh	druh	k1gInSc4	druh
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
aktuálními	aktuální	k2eAgInPc7d1	aktuální
módními	módní	k2eAgInPc7d1	módní
trendy	trend	k1gInPc7	trend
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
jejich	jejich	k3xOp3gNnSc2	jejich
možného	možný	k2eAgNnSc2d1	možné
poškození	poškození	k1gNnSc2	poškození
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
opravou	oprava	k1gFnSc7	oprava
žabek	žabka	k1gFnPc2	žabka
–	–	k?	–
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gMnPc4	jejich
nízkou	nízký	k2eAgFnSc7d1	nízká
cenou	cena	k1gFnSc7	cena
–	–	k?	–
ani	ani	k8xC	ani
nezabývá	zabývat	k5eNaImIp3nS	zabývat
a	a	k8xC	a
pořizuje	pořizovat	k5eAaImIp3nS	pořizovat
si	se	k3xPyFc3	se
žabky	žabka	k1gFnPc4	žabka
nové	nový	k2eAgFnPc4d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Žabky	žabka	k1gFnPc1	žabka
existují	existovat	k5eAaImIp3nP	existovat
zhruba	zhruba	k6eAd1	zhruba
již	již	k6eAd1	již
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6000	[number]	k4	6000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgInP	změnit
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Primitivní	primitivní	k2eAgFnPc1d1	primitivní
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
žabkám	žabka	k1gFnPc3	žabka
podobné	podobný	k2eAgInPc4d1	podobný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaBmF	nalézt
již	již	k9	již
na	na	k7c6	na
jeskynních	jeskynní	k2eAgFnPc6d1	jeskynní
malbách	malba	k1gFnPc6	malba
z	z	k7c2	z
konce	konec	k1gInSc2	konec
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
období	období	k1gNnSc4	období
před	před	k7c7	před
asi	asi	k9	asi
15	[number]	k4	15
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
dochované	dochovaný	k2eAgFnPc1d1	dochovaná
žabky	žabka	k1gFnPc1	žabka
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
cca	cca	kA	cca
1500	[number]	k4	1500
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgNnSc4d1	vlastní
britské	britský	k2eAgNnSc4d1	Britské
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
5000	[number]	k4	5000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
tehdy	tehdy	k6eAd1	tehdy
otiskovali	otiskovat	k5eAaImAgMnP	otiskovat
chodidla	chodidlo	k1gNnPc4	chodidlo
do	do	k7c2	do
rozměklé	rozměklý	k2eAgFnSc2d1	rozměklá
kaše	kaše	k1gFnSc2	kaše
z	z	k7c2	z
papyru	papyr	k1gInSc2	papyr
nalité	nalitý	k2eAgNnSc1d1	nalité
na	na	k7c4	na
vlhký	vlhký	k2eAgInSc4d1	vlhký
písek	písek	k1gInSc4	písek
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
podrážku	podrážka	k1gFnSc4	podrážka
připevnili	připevnit	k5eAaPmAgMnP	připevnit
k	k	k7c3	k
noze	noha	k1gFnSc3	noha
pásky	páska	k1gFnSc2	páska
ze	z	k7c2	z
surové	surový	k2eAgFnSc2d1	surová
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
jsou	být	k5eAaImIp3nP	být
nálezy	nález	k1gInPc1	nález
v	v	k7c6	v
hrobkách	hrobka	k1gFnPc6	hrobka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
našly	najít	k5eAaPmAgFnP	najít
i	i	k9	i
kousky	kousek	k1gInPc4	kousek
obuvi	obuv	k1gFnPc1	obuv
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
z	z	k7c2	z
barevných	barevný	k2eAgFnPc2d1	barevná
kůží	kůže	k1gFnPc2	kůže
a	a	k8xC	a
zdobené	zdobený	k2eAgFnPc1d1	zdobená
perličkami	perlička	k1gFnPc7	perlička
či	či	k8xC	či
květinovými	květinový	k2eAgInPc7d1	květinový
vzory	vzor	k1gInPc7	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
také	také	k9	také
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gFnPc4	on
nosili	nosit	k5eAaImAgMnP	nosit
především	především	k9	především
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
žabky	žabka	k1gFnPc1	žabka
byly	být	k5eAaImAgFnP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
papyru	papyr	k1gInSc2	papyr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
z	z	k7c2	z
rýžových	rýžový	k2eAgNnPc2d1	rýžové
stébel	stéblo	k1gNnPc2	stéblo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
styly	styl	k1gInPc1	styl
byly	být	k5eAaImAgInP	být
odlišné	odlišný	k2eAgInPc1d1	odlišný
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
umístěními	umístění	k1gNnPc7	umístění
popruhu	popruh	k1gInSc2	popruh
pro	pro	k7c4	pro
prst	prst	k1gInSc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
tradiční	tradiční	k2eAgFnSc1d1	tradiční
japonská	japonský	k2eAgFnSc1d1	japonská
obuv	obuv	k1gFnSc1	obuv
s	s	k7c7	s
tkanou	tkaný	k2eAgFnSc7d1	tkaná
podrážkou	podrážka	k1gFnSc7	podrážka
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
zóri	zór	k1gMnPc7	zór
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
jako	jako	k9	jako
obuv	obuv	k1gFnSc4	obuv
k	k	k7c3	k
vycházkám	vycházka	k1gFnPc3	vycházka
na	na	k7c4	na
pláž	pláž	k1gFnSc4	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
okupační	okupační	k2eAgFnSc2d1	okupační
správy	správa	k1gFnSc2	správa
vracejícími	vracející	k2eAgMnPc7d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
získaly	získat	k5eAaPmAgFnP	získat
popularitu	popularita	k1gFnSc4	popularita
a	a	k8xC	a
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c2	za
reprezentanta	reprezentant	k1gMnSc2	reprezentant
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
spojeného	spojený	k2eAgInSc2d1	spojený
se	se	k3xPyFc4	se
surfováním	surfování	k1gNnSc7	surfování
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
úvahy	úvaha	k1gFnPc1	úvaha
nad	nad	k7c7	nad
možností	možnost	k1gFnSc7	možnost
výroby	výroba	k1gFnSc2	výroba
žabek	žabka	k1gFnPc2	žabka
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
podobu	podoba	k1gFnSc4	podoba
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
Morris	Morris	k1gFnSc7	Morris
Yock	Yock	k1gInSc1	Yock
z	z	k7c2	z
novozélandského	novozélandský	k2eAgInSc2d1	novozélandský
Aucklandu	Auckland	k1gInSc2	Auckland
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
však	však	k9	však
postavili	postavit	k5eAaPmAgMnP	postavit
potomci	potomek	k1gMnPc1	potomek
Johna	John	k1gMnSc2	John
Cowieho	Cowie	k1gMnSc2	Cowie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
významným	významný	k2eAgMnSc7d1	významný
poválečným	poválečný	k2eAgMnSc7d1	poválečný
anglickým	anglický	k2eAgMnSc7d1	anglický
obchodníkem	obchodník	k1gMnSc7	obchodník
zabývajícím	zabývající	k2eAgMnSc7d1	zabývající
se	se	k3xPyFc4	se
v	v	k7c6	v
Hongkongu	Hongkong	k1gInSc6	Hongkong
výrobou	výroba	k1gFnSc7	výroba
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
(	(	kIx(	(
<g/>
rozuměj	rozumět	k5eAaImRp2nS	rozumět
potomci	potomek	k1gMnPc1	potomek
Johna	John	k1gMnSc2	John
Cowieho	Cowie	k1gMnSc2	Cowie
<g/>
)	)	kIx)	)
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
právě	právě	k9	právě
Cowie	Cowius	k1gMnPc4	Cowius
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
žabek	žabka	k1gFnPc2	žabka
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
začal	začít	k5eAaPmAgInS	začít
a	a	k8xC	a
že	že	k8xS	že
Yock	Yock	k1gMnSc1	Yock
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gMnSc7	jejich
pouhým	pouhý	k2eAgMnSc7d1	pouhý
dovozcem	dovozce	k1gMnSc7	dovozce
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
John	John	k1gMnSc1	John
Cowie	Cowie	k1gFnSc2	Cowie
dokonce	dokonce	k9	dokonce
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
pojmenování	pojmenování	k1gNnSc4	pojmenování
jandal	jandat	k5eAaPmAgInS	jandat
jako	jako	k9	jako
zkráceninu	zkrácenina	k1gFnSc4	zkrácenina
z	z	k7c2	z
Japanese	Japanese	k1gFnSc2	Japanese
Sandal	Sandal	k1gFnSc2	Sandal
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
japonské	japonský	k2eAgFnPc1d1	japonská
sandále	sandál	k1gInSc6	sandál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
výraz	výraz	k1gInSc1	výraz
jandal	jandat	k5eAaPmAgInS	jandat
je	on	k3xPp3gNnSc4	on
běžně	běžně	k6eAd1	běžně
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
pojmenování	pojmenování	k1gNnSc3	pojmenování
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
podoby	podoba	k1gFnSc2	podoba
této	tento	k3xDgFnSc2	tento
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
Jandal	Jandal	k1gFnSc2	Jandal
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
známkou	známka	k1gFnSc7	známka
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
společnosti	společnost	k1gFnSc2	společnost
Skellerup	Skellerup	k1gInSc1	Skellerup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
žabky	žabka	k1gFnPc1	žabka
upravovány	upravovat	k5eAaImNgFnP	upravovat
a	a	k8xC	a
doplňovány	doplňovat	k5eAaImNgFnP	doplňovat
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	on	k3xPp3gInPc4	on
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
–	–	k?	–
ploché	plochý	k2eAgFnSc2d1	plochá
–	–	k?	–
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Novodobými	novodobý	k2eAgFnPc7d1	novodobá
úpravami	úprava	k1gFnPc7	úprava
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
podpatky	podpatek	k1gInPc1	podpatek
či	či	k8xC	či
klínky	klínek	k1gInPc1	klínek
používané	používaný	k2eAgInPc1d1	používaný
u	u	k7c2	u
obuvi	obuv	k1gFnSc2	obuv
určené	určený	k2eAgFnSc2d1	určená
ženám	žena	k1gFnPc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
Popularitu	popularita	k1gFnSc4	popularita
žabkám	žabka	k1gFnPc3	žabka
přinesly	přinést	k5eAaPmAgFnP	přinést
také	také	k9	také
celebrity	celebrita	k1gFnPc1	celebrita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	on	k3xPp3gMnPc4	on
začaly	začít	k5eAaPmAgFnP	začít
nosit	nosit	k5eAaImF	nosit
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
této	tento	k3xDgFnSc2	tento
obuvi	obuv	k1gFnSc2	obuv
začali	začít	k5eAaPmAgMnP	začít
věnovat	věnovat	k5eAaImF	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
také	také	k9	také
módní	módní	k2eAgMnPc1d1	módní
návrháři	návrhář	k1gMnPc1	návrhář
<g/>
.	.	kIx.	.
</s>
<s>
Havaianas	Havaianas	k1gInSc1	Havaianas
je	být	k5eAaImIp3nS	být
brazilská	brazilský	k2eAgFnSc1d1	brazilská
značka	značka	k1gFnSc1	značka
žabek	žabka	k1gFnPc2	žabka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
popularitu	popularita	k1gFnSc4	popularita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
uvedla	uvést	k5eAaPmAgFnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
žabky	žabka	k1gFnSc2	žabka
obsahující	obsahující	k2eAgInSc4d1	obsahující
na	na	k7c6	na
páskách	páska	k1gFnPc6	páska
motiv	motiv	k1gInSc4	motiv
brazilské	brazilský	k2eAgFnSc2d1	brazilská
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
žabky	žabka	k1gFnPc1	žabka
Havaianas	Havaianasa	k1gFnPc2	Havaianasa
například	například	k6eAd1	například
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
populární	populární	k2eAgFnSc2d1	populární
až	až	k8xS	až
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc1d1	vlastní
firma	firma	k1gFnSc1	firma
existuje	existovat	k5eAaImIp3nS	existovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Boty	bota	k1gFnPc1	bota
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
svou	svůj	k3xOyFgFnSc7	svůj
pohodlnou	pohodlný	k2eAgFnSc7d1	pohodlná
podešví	podešev	k1gFnSc7	podešev
i	i	k8xC	i
řemínky	řemínek	k1gInPc7	řemínek
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
jméno	jméno	k1gNnSc1	jméno
Havaianas	Havaianasa	k1gFnPc2	Havaianasa
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
portugalštiny	portugalština	k1gFnSc2	portugalština
<g/>
.	.	kIx.	.
</s>
<s>
Žabka	Žabka	k1gMnSc1	Žabka
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
podrážky	podrážka	k1gFnSc2	podrážka
a	a	k8xC	a
z	z	k7c2	z
vrchního	vrchní	k2eAgInSc2d1	vrchní
pásku	pásek	k1gInSc2	pásek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
uprostřed	uprostřed	k6eAd1	uprostřed
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
palce	palec	k1gInSc2	palec
připojí	připojit	k5eAaPmIp3nP	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
ze	z	k7c2	z
syntetické	syntetický	k2eAgFnSc2d1	syntetická
hmoty	hmota	k1gFnSc2	hmota
vyseknou	vyseknout	k5eAaPmIp3nP	vyseknout
dvě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
sloužící	sloužící	k2eAgFnPc1d1	sloužící
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
vrchních	vrchní	k2eAgInPc2d1	vrchní
pásků	pásek	k1gInPc2	pásek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
pojit	pojit	k5eAaImF	pojit
k	k	k7c3	k
podrážce	podrážka	k1gFnSc3	podrážka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
proděraví	proděravit	k5eAaPmIp3nS	proděravit
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
každé	každý	k3xTgNnSc1	každý
z	z	k7c2	z
dírek	dírka	k1gFnPc2	dírka
se	se	k3xPyFc4	se
stlačí	stlačit	k5eAaPmIp3nS	stlačit
a	a	k8xC	a
umístí	umístit	k5eAaPmIp3nS	umístit
se	se	k3xPyFc4	se
na	na	k7c4	na
příslušné	příslušný	k2eAgNnSc4d1	příslušné
místo	místo	k1gNnSc4	místo
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
vlije	vlít	k5eAaPmIp3nS	vlít
materiál	materiál	k1gInSc1	materiál
z	z	k7c2	z
pěnové	pěnový	k2eAgFnSc2d1	pěnová
umělé	umělý	k2eAgFnSc2d1	umělá
hmoty	hmota	k1gFnSc2	hmota
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
a	a	k8xC	a
následně	následně	k6eAd1	následně
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
se	s	k7c7	s
stykovými	stykový	k2eAgFnPc7d1	styková
částmi	část	k1gFnPc7	část
vrchních	vrchní	k1gFnPc2	vrchní
pásků	pásek	k1gMnPc2	pásek
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
každé	každý	k3xTgFnSc2	každý
z	z	k7c2	z
dírek	dírka	k1gFnPc2	dírka
vstřikována	vstřikován	k2eAgFnSc1d1	vstřikována
plastická	plastický	k2eAgFnSc1d1	plastická
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
plněny	plnit	k5eAaImNgFnP	plnit
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
prolákliny	proláklina	k1gFnPc1	proláklina
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
Grafika	grafika	k1gFnSc1	grafika
se	se	k3xPyFc4	se
na	na	k7c4	na
obuv	obuv	k1gFnSc4	obuv
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
pomocí	pomocí	k7c2	pomocí
<g/>
:	:	kIx,	:
potisku	potisk	k1gInSc2	potisk
<g/>
,	,	kIx,	,
strojové	strojový	k2eAgFnSc2d1	strojová
výšivky	výšivka	k1gFnSc2	výšivka
<g/>
,	,	kIx,	,
nažehlovací	nažehlovací	k2eAgFnSc7d1	nažehlovací
etiketou	etiketa	k1gFnSc7	etiketa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
hromadné	hromadný	k2eAgFnSc2d1	hromadná
výroby	výroba	k1gFnSc2	výroba
žabek	žabka	k1gFnPc2	žabka
přesunuta	přesunut	k2eAgFnSc1d1	přesunuta
do	do	k7c2	do
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
levnou	levný	k2eAgFnSc7d1	levná
pracovní	pracovní	k2eAgFnSc7d1	pracovní
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
značky	značka	k1gFnPc1	značka
–	–	k?	–
například	například	k6eAd1	například
Okabashi	Okabashi	k1gNnSc1	Okabashi
(	(	kIx(	(
<g/>
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Bufordu	Buford	k1gInSc6	Buford
v	v	k7c6	v
Georgii	Georgie	k1gFnSc6	Georgie
<g/>
)	)	kIx)	)
či	či	k8xC	či
Island	Island	k1gInSc1	Island
Slipper	Slippra	k1gFnPc2	Slippra
(	(	kIx(	(
<g/>
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c4	v
Pearl	Pearl	k1gInSc4	Pearl
City	city	k1gNnSc1	city
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
<g/>
)	)	kIx)	)
–	–	k?	–
však	však	k9	však
přesto	přesto	k8xC	přesto
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
škodí	škodit	k5eAaImIp3nP	škodit
nošení	nošení	k1gNnSc4	nošení
této	tento	k3xDgFnSc2	tento
obuvi	obuv	k1gFnSc2	obuv
lidskému	lidský	k2eAgNnSc3d1	lidské
tělu	tělo	k1gNnSc3	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
žabky	žabka	k1gFnSc2	žabka
zcela	zcela	k6eAd1	zcela
neodmítají	odmítat	k5eNaImIp3nP	odmítat
(	(	kIx(	(
<g/>
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
jejich	jejich	k3xOp3gNnSc4	jejich
využití	využití	k1gNnSc4	využití
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
,	,	kIx,	,
v	v	k7c6	v
bazénu	bazén	k1gInSc6	bazén
či	či	k8xC	či
ve	v	k7c6	v
sprchách	sprcha	k1gFnPc6	sprcha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nedoporučují	doporučovat	k5eNaImIp3nP	doporučovat
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
chodit	chodit	k5eAaImF	chodit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
hodinu	hodina	k1gFnSc4	hodina
denně	denně	k6eAd1	denně
(	(	kIx(	(
<g/>
a	a	k8xC	a
rozhodně	rozhodně	k6eAd1	rozhodně
ne	ne	k9	ne
po	po	k7c6	po
městské	městský	k2eAgFnSc6d1	městská
dlažbě	dlažba	k1gFnSc6	dlažba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Preferují	preferovat	k5eAaImIp3nP	preferovat
střídání	střídání	k1gNnSc4	střídání
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
této	tento	k3xDgFnSc2	tento
obuvi	obuv	k1gFnSc2	obuv
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednak	jednak	k8xC	jednak
neposkytují	poskytovat	k5eNaImIp3nP	poskytovat
nohám	noha	k1gFnPc3	noha
žádnou	žádný	k3yNgFnSc4	žádný
oporu	opora	k1gFnSc4	opora
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
nadměrně	nadměrně	k6eAd1	nadměrně
zatěžují	zatěžovat	k5eAaImIp3nP	zatěžovat
klenbu	klenba	k1gFnSc4	klenba
nohy	noha	k1gFnSc2	noha
a	a	k8xC	a
deformují	deformovat	k5eAaImIp3nP	deformovat
chodidlo	chodidlo	k1gNnSc4	chodidlo
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
ubližují	ubližovat	k5eAaImIp3nP	ubližovat
patě	pata	k1gFnSc3	pata
a	a	k8xC	a
nezdravě	zdravě	k6eNd1	zdravě
namáhají	namáhat	k5eAaImIp3nP	namáhat
Achillovu	Achillův	k2eAgFnSc4d1	Achillova
šlachu	šlacha	k1gFnSc4	šlacha
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
prsty	prst	k1gInPc1	prst
se	se	k3xPyFc4	se
při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
musí	muset	k5eAaImIp3nS	muset
více	hodně	k6eAd2	hodně
namáhat	namáhat	k5eAaImF	namáhat
(	(	kIx(	(
<g/>
obuv	obuv	k1gFnSc4	obuv
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
drží	držet	k5eAaImIp3nS	držet
jen	jen	k9	jen
díky	díky	k7c3	díky
pásku	pásek	k1gInSc2	pásek
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
pohybu	pohyb	k1gInSc6	pohyb
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
obuvi	obuv	k1gFnSc6	obuv
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
vzniku	vznik	k1gInSc2	vznik
tzv.	tzv.	kA	tzv.
kladívkového	kladívkový	k2eAgInSc2d1	kladívkový
prstu	prst	k1gInSc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
též	též	k9	též
spojen	spojen	k2eAgInSc4d1	spojen
častý	častý	k2eAgInSc4d1	častý
výron	výron	k1gInSc4	výron
kotníku	kotník	k1gInSc2	kotník
či	či	k8xC	či
jeho	jeho	k3xOp3gFnSc2	jeho
zlomeniny	zlomenina	k1gFnSc2	zlomenina
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
noha	noha	k1gFnSc1	noha
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
žabek	žabka	k1gFnPc2	žabka
jen	jen	k6eAd1	jen
svou	svůj	k3xOyFgFnSc7	svůj
spodní	spodní	k2eAgFnSc7d1	spodní
částí	část	k1gFnSc7	část
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
chodidla	chodidlo	k1gNnSc2	chodidlo
je	být	k5eAaImIp3nS	být
vystaven	vystaven	k2eAgInSc1d1	vystaven
působení	působení	k1gNnSc4	působení
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vysušování	vysušování	k1gNnSc3	vysušování
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vzniknout	vzniknout	k5eAaPmF	vzniknout
hluboké	hluboký	k2eAgFnPc1d1	hluboká
praskliny	prasklina	k1gFnPc1	prasklina
a	a	k8xC	a
rozpraskaná	rozpraskaný	k2eAgFnSc1d1	rozpraskaná
kůže	kůže	k1gFnSc1	kůže
může	moct	k5eAaImIp3nS	moct
začít	začít	k5eAaPmF	začít
bolestivě	bolestivě	k6eAd1	bolestivě
krvácet	krvácet	k5eAaImF	krvácet
<g/>
.	.	kIx.	.
</s>
<s>
Žabkám	žabka	k1gFnPc3	žabka
lze	lze	k6eAd1	lze
také	také	k9	také
připočíst	připočíst	k5eAaPmF	připočíst
bolesti	bolest	k1gFnPc4	bolest
kyčlí	kyčel	k1gFnPc2	kyčel
a	a	k8xC	a
bolesti	bolest	k1gFnPc1	bolest
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
zad	záda	k1gNnPc2	záda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nutí	nutit	k5eAaImIp3nS	nutit
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
přirozeného	přirozený	k2eAgInSc2d1	přirozený
způsobu	způsob	k1gInSc2	způsob
chůze	chůze	k1gFnSc1	chůze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
osoba	osoba	k1gFnSc1	osoba
obutá	obutý	k2eAgFnSc1d1	obutá
do	do	k7c2	do
žabek	žabka	k1gFnPc2	žabka
dělá	dělat	k5eAaImIp3nS	dělat
obvykle	obvykle	k6eAd1	obvykle
menší	malý	k2eAgInPc4d2	menší
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
<g/>
,	,	kIx,	,
a	a	k8xC	a
namáhá	namáhat	k5eAaImIp3nS	namáhat
tak	tak	k9	tak
jiné	jiný	k2eAgNnSc1d1	jiné
části	část	k1gFnPc4	část
zádových	zádový	k2eAgInPc2d1	zádový
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
než	než	k8xS	než
při	při	k7c6	při
přirozené	přirozený	k2eAgFnSc6d1	přirozená
chůzi	chůze	k1gFnSc6	chůze
<g/>
.	.	kIx.	.
</s>
<s>
Žabkám	žabka	k1gFnPc3	žabka
je	být	k5eAaImIp3nS	být
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenabízí	nabízet	k5eNaImIp3nS	nabízet
žádnou	žádný	k3yNgFnSc4	žádný
ochranu	ochrana	k1gFnSc4	ochrana
chodidel	chodidlo	k1gNnPc2	chodidlo
před	před	k7c7	před
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
blátem	bláto	k1gNnSc7	bláto
či	či	k8xC	či
špínou	špína	k1gFnSc7	špína
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
ani	ani	k8xC	ani
chodidla	chodidlo	k1gNnPc1	chodidlo
neochrání	ochránit	k5eNaPmIp3nP	ochránit
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
nohu	noha	k1gFnSc4	noha
obutou	obutý	k2eAgFnSc4d1	obutá
v	v	k7c6	v
žabce	žabka	k1gFnSc6	žabka
někdo	někdo	k3yInSc1	někdo
šlápne	šlápnout	k5eAaPmIp3nS	šlápnout
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
zdroje	zdroj	k1gInPc1	zdroj
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
nevhodné	vhodný	k2eNgNnSc4d1	nevhodné
složení	složení	k1gNnSc4	složení
podešve	podešev	k1gFnSc2	podešev
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
chemikálie	chemikálie	k1gFnPc4	chemikálie
(	(	kIx(	(
<g/>
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
fosfáty	fosfát	k1gInPc1	fosfát
či	či	k8xC	či
ftaláty	ftalát	k1gInPc1	ftalát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
kůže	kůže	k1gFnSc2	kůže
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
jeho	jeho	k3xOp3gInSc4	jeho
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
obuv	obuv	k1gFnSc1	obuv
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
polyuretanu	polyuretan	k1gInSc2	polyuretan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
žabky	žabka	k1gFnPc1	žabka
představovat	představovat	k5eAaImF	představovat
problém	problém	k1gInSc4	problém
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
žabek	žabka	k1gFnPc2	žabka
využívají	využívat	k5eAaPmIp3nP	využívat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
recyklovatelné	recyklovatelný	k2eAgFnPc1d1	recyklovatelná
(	(	kIx(	(
<g/>
recyklované	recyklovaný	k2eAgFnPc1d1	recyklovaná
pneumatiky	pneumatika	k1gFnPc1	pneumatika
<g/>
)	)	kIx)	)
a	a	k8xC	a
nebo	nebo	k8xC	nebo
přírodní	přírodní	k2eAgNnPc1d1	přírodní
(	(	kIx(	(
<g/>
konopí	konopí	k1gNnSc1	konopí
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
kokos	kokos	k1gInSc1	kokos
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
je	být	k5eAaImIp3nS	být
také	také	k9	také
používání	používání	k1gNnSc1	používání
této	tento	k3xDgFnSc2	tento
obuvi	obuv	k1gFnSc2	obuv
při	při	k7c6	při
řízení	řízení	k1gNnSc6	řízení
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
užití	užití	k1gNnSc1	užití
této	tento	k3xDgFnSc2	tento
obuvi	obuv	k1gFnSc2	obuv
během	během	k7c2	během
řízení	řízení	k1gNnSc2	řízení
dokonce	dokonce	k9	dokonce
pokutováno	pokutován	k2eAgNnSc1d1	pokutováno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
má	mít	k5eAaImIp3nS	mít
obuv	obuv	k1gFnSc4	obuv
různé	různý	k2eAgNnSc1d1	různé
pojmenování	pojmenování	k1gNnSc1	pojmenování
<g/>
:	:	kIx,	:
jandals	jandals	k1gInSc1	jandals
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
infradito	infradita	k1gFnSc5	infradita
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
mezi	mezi	k7c7	mezi
prsty	prst	k1gInPc7	prst
<g/>
)	)	kIx)	)
–	–	k?	–
Itálie	Itálie	k1gFnSc2	Itálie
Itálie	Itálie	k1gFnSc2	Itálie
Hawaii	Hawaie	k1gFnSc3	Hawaie
Chappal	Chappal	k1gFnSc3	Chappal
či	či	k8xC	či
Qainchi	Qainch	k1gInSc3	Qainch
chappals	chappalsa	k1gFnPc2	chappalsa
–	–	k?	–
Indie	Indie	k1gFnSc2	Indie
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Pákistán	Pákistán	k1gInSc4	Pákistán
Pákistán	Pákistán	k1gInSc1	Pákistán
thongs	thongs	k1gInSc1	thongs
–	–	k?	–
Austrálie	Austrálie	k1gFnSc2	Austrálie
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Kanada	Kanada	k1gFnSc1	Kanada
tongs	tongs	k1gInSc1	tongs
-	-	kIx~	-
Francie	Francie	k1gFnSc1	Francie
Francie	Francie	k1gFnSc2	Francie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
slip-slops	sliplops	k6eAd1	slip-slops
–	–	k?	–
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
go-aheads	goheadsa	k1gFnPc2	go-aheadsa
–	–	k?	–
jižní	jižní	k2eAgNnSc4d1	jižní
Tichomoří	Tichomoří	k1gNnSc4	Tichomoří
ojotas	ojotasa	k1gFnPc2	ojotasa
nebo	nebo	k8xC	nebo
chancletas	chancletas	k1gInSc1	chancletas
–	–	k?	–
Argentina	Argentina	k1gFnSc1	Argentina
Argentina	Argentina	k1gFnSc1	Argentina
chanclas	chanclas	k1gInSc1	chanclas
–	–	k?	–
Španělsko	Španělsko	k1gNnSc1	Španělsko
Španělsko	Španělsko	k1gNnSc1	Španělsko
chancletas	chancletas	k1gMnSc1	chancletas
nebo	nebo	k8xC	nebo
sandalias	sandalias	k1gMnSc1	sandalias
–	–	k?	–
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
japanke	japankat	k5eAaPmIp3nS	japankat
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
japonky	japonka	k1gFnSc2	japonka
<g/>
)	)	kIx)	)
–	–	k?	–
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
japonki	japonk	k1gFnSc2	japonk
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Japonky	Japonka	k1gFnSc2	Japonka
<g/>
)	)	kIx)	)
–	–	k?	–
Polsko	Polsko	k1gNnSc1	Polsko
Polsko	Polsko	k1gNnSc1	Polsko
в	в	k?	в
/	/	kIx~	/
vjetnamki	vjetnamk	k1gFnSc2	vjetnamk
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
vietnamky	vietnamka	k1gFnSc2	vietnamka
<g/>
)	)	kIx)	)
-	-	kIx~	-
Rusko	Rusko	k1gNnSc1	Rusko
Rusko	Rusko	k1gNnSc1	Rusko
chinelos	chinelos	k1gMnSc1	chinelos
–	–	k?	–
Brazílie	Brazílie	k1gFnSc2	Brazílie
Brazílie	Brazílie	k1gFnSc2	Brazílie
kafkafim	kafkafim	k1gMnSc1	kafkafim
–	–	k?	–
Izrael	Izrael	k1gInSc1	Izrael
Izrael	Izrael	k1gInSc1	Izrael
sayonares	sayonares	k1gInSc1	sayonares
–	–	k?	–
Řecko	Řecko	k1gNnSc1	Řecko
Řecko	Řecko	k1gNnSc1	Řecko
tsokara	tsokar	k1gMnSc2	tsokar
–	–	k?	–
Kypr	Kypr	k1gInSc1	Kypr
Kypr	Kypr	k1gInSc1	Kypr
(	(	kIx(	(
<g/>
řecká	řecký	k2eAgFnSc1d1	řecká
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
vietnámi	vietná	k1gFnPc7	vietná
papucs	papucs	k6eAd1	papucs
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
pantofle	pantofle	k?	pantofle
<g/>
)	)	kIx)	)
–	–	k?	–
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
tsinelas	tsinelas	k1gMnSc1	tsinelas
nebo	nebo	k8xC	nebo
chinelas	chinelas	k1gMnSc1	chinelas
–	–	k?	–
Filipíny	Filipíny	k1gFnPc4	Filipíny
Filipíny	Filipíny	k1gFnPc4	Filipíny
flip-flops	fliplopsa	k1gFnPc2	flip-flopsa
–	–	k?	–
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
Na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
a	a	k8xC	a
na	na	k7c6	na
několika	několik	k4yIc6	několik
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
žabky	žabka	k1gFnPc1	žabka
pojmenovávány	pojmenováván	k2eAgFnPc1d1	pojmenovávána
jako	jako	k8xC	jako
bačkory	bačkora	k1gFnPc1	bačkora
<g/>
.	.	kIx.	.
</s>
