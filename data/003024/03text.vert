<s>
Logaritmus	logaritmus	k1gInSc1	logaritmus
kladného	kladný	k2eAgNnSc2d1	kladné
reálného	reálný	k2eAgNnSc2d1	reálné
čísla	číslo	k1gNnSc2	číslo
x	x	k?	x
při	při	k7c6	při
základu	základ	k1gInSc6	základ
a	a	k8xC	a
(	(	kIx(	(
<g/>
a	a	k8xC	a
∈	∈	k?	∈
R	R	kA	R
<g/>
+	+	kIx~	+
<g/>
-	-	kIx~	-
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc1	takový
reálné	reálný	k2eAgNnSc1d1	reálné
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
vztahu	vztah	k1gInSc6	vztah
se	se	k3xPyFc4	se
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
základ	základ	k1gInSc1	základ
logaritmu	logaritmus	k1gInSc2	logaritmus
(	(	kIx(	(
<g/>
báze	báze	k1gFnSc1	báze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
logaritmované	logaritmovaný	k2eAgNnSc1d1	logaritmovaný
číslo	číslo	k1gNnSc1	číslo
x	x	k?	x
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
argument	argument	k1gInSc1	argument
či	či	k8xC	či
numerus	numerus	k1gInSc1	numerus
<g/>
,	,	kIx,	,
y	y	k?	y
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
logaritmem	logaritmus	k1gInSc7	logaritmus
čísla	číslo	k1gNnSc2	číslo
x	x	k?	x
při	při	k7c6	při
základu	základ	k1gInSc6	základ
a.	a.	k?	a.
</s>
<s>
Logaritmická	logaritmický	k2eAgFnSc1d1	logaritmická
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
matematická	matematický	k2eAgFnSc1d1	matematická
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
inverzní	inverzní	k2eAgFnSc1d1	inverzní
k	k	k7c3	k
exponenciální	exponenciální	k2eAgFnSc3d1	exponenciální
funkci	funkce	k1gFnSc3	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
logaritmu	logaritmus	k1gInSc2	logaritmus
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
rozšíření	rozšíření	k1gNnSc4	rozšíření
myšlenky	myšlenka	k1gFnSc2	myšlenka
exponentu	exponent	k1gInSc2	exponent
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
2	[number]	k4	2
×	×	k?	×
2	[number]	k4	2
=	=	kIx~	=
4	[number]	k4	4
můžeme	moct	k5eAaImIp1nP	moct
zapsat	zapsat	k5eAaPmF	zapsat
také	také	k9	také
jako	jako	k9	jako
21	[number]	k4	21
×	×	k?	×
21	[number]	k4	21
=	=	kIx~	=
22	[number]	k4	22
a	a	k8xC	a
místo	místo	k7c2	místo
4	[number]	k4	4
×	×	k?	×
8	[number]	k4	8
=	=	kIx~	=
32	[number]	k4	32
můžeme	moct	k5eAaImIp1nP	moct
napsat	napsat	k5eAaPmF	napsat
22	[number]	k4	22
×	×	k?	×
23	[number]	k4	23
=	=	kIx~	=
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
Exponent	exponent	k1gInSc1	exponent
součinu	součin	k1gInSc2	součin
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
součet	součet	k1gInSc1	součet
exponentů	exponent	k1gInPc2	exponent
obou	dva	k4xCgInPc2	dva
součinitelů	součinitel	k1gInPc2	součinitel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
zvoleném	zvolený	k2eAgInSc6d1	zvolený
základu	základ	k1gInSc6	základ
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
násobení	násobený	k2eAgMnPc1d1	násobený
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
sčítání	sčítání	k1gNnSc4	sčítání
exponentů	exponent	k1gInPc2	exponent
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
jen	jen	k9	jen
pro	pro	k7c4	pro
celé	celý	k2eAgInPc4d1	celý
exponenty	exponent	k1gInPc4	exponent
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
můžeme	moct	k5eAaImIp1nP	moct
zapsat	zapsat	k5eAaPmF	zapsat
i	i	k9	i
dělení	dělení	k1gNnSc4	dělení
<g/>
:	:	kIx,	:
32	[number]	k4	32
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
=	=	kIx~	=
25	[number]	k4	25
<g/>
:	:	kIx,	:
22	[number]	k4	22
=	=	kIx~	=
23	[number]	k4	23
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
=	=	kIx~	=
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
=	=	kIx~	=
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
=	=	kIx~	=
2-1	[number]	k4	2-1
čili	čili	k8xC	čili
0,5	[number]	k4	0,5
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
dělení	dělení	k1gNnSc2	dělení
stačí	stačit	k5eAaBmIp3nS	stačit
odečítat	odečítat	k5eAaImF	odečítat
exponenty	exponent	k1gInPc4	exponent
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
jsme	být	k5eAaImIp1nP	být
zvolili	zvolit	k5eAaPmAgMnP	zvolit
jako	jako	k8xS	jako
základ	základ	k1gInSc4	základ
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
exponentu	exponent	k1gInSc2	exponent
dvou	dva	k4xCgMnPc6	dva
<g/>
"	"	kIx"	"
můžeme	moct	k5eAaImIp1nP	moct
psát	psát	k5eAaImF	psát
log	log	kA	log
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
tedy	tedy	k9	tedy
napsat	napsat	k5eAaPmF	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
log	log	kA	log
<g/>
2	[number]	k4	2
32	[number]	k4	32
=	=	kIx~	=
5	[number]	k4	5
<g/>
,	,	kIx,	,
log	log	kA	log
<g/>
2	[number]	k4	2
8	[number]	k4	8
=	=	kIx~	=
3	[number]	k4	3
<g/>
,	,	kIx,	,
log	log	kA	log
<g/>
2	[number]	k4	2
4	[number]	k4	4
=	=	kIx~	=
2	[number]	k4	2
<g/>
,	,	kIx,	,
log	log	kA	log
<g/>
2	[number]	k4	2
1	[number]	k4	1
=	=	kIx~	=
0	[number]	k4	0
a	a	k8xC	a
log	log	kA	log
<g/>
2	[number]	k4	2
0,5	[number]	k4	0,5
=	=	kIx~	=
-1	-1	k4	-1
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
číslo	číslo	k1gNnSc4	číslo
x	x	k?	x
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
nuly	nula	k1gFnSc2	nula
<g/>
)	)	kIx)	)
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
log	log	kA	log
<g/>
2	[number]	k4	2
2	[number]	k4	2
<g/>
x	x	k?	x
=	=	kIx~	=
(	(	kIx(	(
<g/>
log	log	kA	log
<g/>
2	[number]	k4	2
x	x	k?	x
<g/>
)	)	kIx)	)
+	+	kIx~	+
1	[number]	k4	1
<g/>
,	,	kIx,	,
log	log	kA	log
<g/>
2	[number]	k4	2
4	[number]	k4	4
<g/>
x	x	k?	x
=	=	kIx~	=
(	(	kIx(	(
<g/>
log	log	kA	log
<g/>
2	[number]	k4	2
x	x	k?	x
<g/>
)	)	kIx)	)
+	+	kIx~	+
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
8	[number]	k4	8
=	=	kIx~	=
23	[number]	k4	23
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
také	také	k9	také
psát	psát	k5eAaImF	psát
8	[number]	k4	8
=	=	kIx~	=
21	[number]	k4	21
×	×	k?	×
21	[number]	k4	21
×	×	k?	×
21	[number]	k4	21
=	=	kIx~	=
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
8	[number]	k4	8
=	=	kIx~	=
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
x	x	k?	x
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
místo	místo	k1gNnSc1	místo
umocňování	umocňování	k1gNnSc2	umocňování
stačí	stačit	k5eAaBmIp3nS	stačit
exponent	exponent	k1gInSc4	exponent
násobit	násobit	k5eAaImF	násobit
a	a	k8xC	a
místo	místo	k7c2	místo
odmocnění	odmocnění	k1gNnSc2	odmocnění
exponent	exponent	k1gInSc1	exponent
dělit	dělit	k5eAaImF	dělit
<g/>
.	.	kIx.	.
</s>
<s>
Kdybychom	kdyby	kYmCp1nP	kdyby
místo	místo	k7c2	místo
dvojky	dvojka	k1gFnSc2	dvojka
zvolili	zvolit	k5eAaPmAgMnP	zvolit
základ	základ	k1gInSc4	základ
10	[number]	k4	10
(	(	kIx(	(
<g/>
desítkový	desítkový	k2eAgInSc1d1	desítkový
logaritmus	logaritmus	k1gInSc1	logaritmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
psát	psát	k5eAaImF	psát
log	log	kA	log
<g/>
10	[number]	k4	10
0,1	[number]	k4	0,1
=	=	kIx~	=
-1	-1	k4	-1
<g/>
,	,	kIx,	,
log	log	kA	log
<g/>
10	[number]	k4	10
1	[number]	k4	1
=	=	kIx~	=
0	[number]	k4	0
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
základ	základ	k1gInSc4	základ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
log	log	kA	log
<g/>
10	[number]	k4	10
100	[number]	k4	100
=	=	kIx~	=
2	[number]	k4	2
atd.	atd.	kA	atd.
I	i	k8xC	i
tady	tady	k6eAd1	tady
bude	být	k5eAaImBp3nS	být
platit	platit	k5eAaImF	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
log	log	kA	log
<g/>
1010	[number]	k4	1010
<g/>
x	x	k?	x
=	=	kIx~	=
(	(	kIx(	(
<g/>
log	log	kA	log
<g/>
10	[number]	k4	10
x	x	k?	x
<g/>
)	)	kIx)	)
+	+	kIx~	+
1	[number]	k4	1
<g/>
,	,	kIx,	,
log	log	kA	log
<g/>
10	[number]	k4	10
100	[number]	k4	100
<g/>
x	x	k?	x
=	=	kIx~	=
(	(	kIx(	(
<g/>
log	log	kA	log
<g/>
10	[number]	k4	10
x	x	k?	x
<g/>
)	)	kIx)	)
+	+	kIx~	+
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
jsme	být	k5eAaImIp1nP	být
mluvili	mluvit	k5eAaImAgMnP	mluvit
o	o	k7c6	o
exponentech	exponent	k1gInPc6	exponent
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
to	ten	k3xDgNnSc1	ten
smysl	smysl	k1gInSc4	smysl
jen	jen	k9	jen
pro	pro	k7c4	pro
celá	celý	k2eAgNnPc4d1	celé
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
ale	ale	k9	ale
zmínili	zmínit	k5eAaPmAgMnP	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
místo	místo	k1gNnSc1	místo
odmocňování	odmocňování	k1gNnSc2	odmocňování
stačí	stačit	k5eAaBmIp3nS	stačit
exponent	exponent	k1gInSc1	exponent
dělit	dělit	k5eAaImF	dělit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vyjít	vyjít	k5eAaPmF	vyjít
i	i	k9	i
necelé	celý	k2eNgNnSc4d1	necelé
číslo	číslo	k1gNnSc4	číslo
<g/>
:	:	kIx,	:
druhou	druhý	k4xOgFnSc4	druhý
odmocninu	odmocnina	k1gFnSc4	odmocnina
z	z	k7c2	z
deseti	deset	k4xCc2	deset
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k9	jako
10	[number]	k4	10
<g/>
(	(	kIx(	(
<g/>
0,5	[number]	k4	0,5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dělá	dělat	k5eAaImIp3nS	dělat
logaritmická	logaritmický	k2eAgFnSc1d1	logaritmická
funkce	funkce	k1gFnSc1	funkce
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
různé	různý	k2eAgFnPc4d1	různá
hodnoty	hodnota	k1gFnPc4	hodnota
výrazu	výraz	k1gInSc2	výraz
zx	zx	k?	zx
vyneseme	vynést	k5eAaPmIp1nP	vynést
do	do	k7c2	do
grafu	graf	k1gInSc2	graf
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
jimi	on	k3xPp3gMnPc7	on
proložit	proložit	k5eAaPmF	proložit
spojitou	spojitý	k2eAgFnSc4d1	spojitá
křivku	křivka	k1gFnSc4	křivka
-	-	kIx~	-
logaritmickou	logaritmický	k2eAgFnSc4d1	logaritmická
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
reálná	reálný	k2eAgNnPc1d1	reálné
x	x	k?	x
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Logaritmus	logaritmus	k1gInSc4	logaritmus
čísla	číslo	k1gNnSc2	číslo
3	[number]	k4	3
bude	být	k5eAaImBp3nS	být
někde	někde	k6eAd1	někde
mezi	mezi	k7c7	mezi
log	log	kA	log
<g/>
101	[number]	k4	101
a	a	k8xC	a
log	log	kA	log
<g/>
1010	[number]	k4	1010
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
reálné	reálný	k2eAgNnSc1d1	reálné
číslo	číslo	k1gNnSc1	číslo
někde	někde	k6eAd1	někde
mezi	mezi	k7c7	mezi
nulou	nula	k1gFnSc7	nula
a	a	k8xC	a
jedničkou	jednička	k1gFnSc7	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tabulky	tabulka	k1gFnSc2	tabulka
můžeme	moct	k5eAaImIp1nP	moct
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
log	log	kA	log
<g/>
10	[number]	k4	10
3	[number]	k4	3
=	=	kIx~	=
0,47712125472	[number]	k4	0,47712125472
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
číslo	číslo	k1gNnSc1	číslo
3	[number]	k4	3
můžeme	moct	k5eAaImIp1nP	moct
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k9	jako
100,477	[number]	k4	100,477
<g/>
12125472	[number]	k4	12125472
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
bude	být	k5eAaImBp3nS	být
platit	platit	k5eAaImF	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
log	log	kA	log
<g/>
10	[number]	k4	10
10	[number]	k4	10
<g/>
x	x	k?	x
=	=	kIx~	=
(	(	kIx(	(
<g/>
log	log	kA	log
<g/>
10	[number]	k4	10
x	x	k?	x
<g/>
)	)	kIx)	)
+	+	kIx~	+
1	[number]	k4	1
<g/>
,	,	kIx,	,
log	log	kA	log
<g/>
10	[number]	k4	10
0,1	[number]	k4	0,1
<g/>
x	x	k?	x
=	=	kIx~	=
(	(	kIx(	(
log	log	kA	log
<g/>
10	[number]	k4	10
x	x	k?	x
<g/>
)	)	kIx)	)
-	-	kIx~	-
1	[number]	k4	1
<g/>
,	,	kIx,	,
že	že	k8xS	že
logaritmus	logaritmus	k1gInSc1	logaritmus
součinu	součin	k1gInSc2	součin
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
součtu	součet	k1gInSc2	součet
logaritmů	logaritmus	k1gInPc2	logaritmus
obou	dva	k4xCgInPc2	dva
činitelů	činitel	k1gInPc2	činitel
a	a	k8xC	a
logaritmus	logaritmus	k1gInSc1	logaritmus
podílu	podíl	k1gInSc2	podíl
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
rozdílu	rozdíl	k1gInSc2	rozdíl
logaritmus	logaritmus	k1gInSc1	logaritmus
dělence	dělenec	k1gInSc2	dělenec
minus	minus	k6eAd1	minus
logaritmus	logaritmus	k1gInSc4	logaritmus
dělitele	dělitel	k1gInSc2	dělitel
<g/>
.	.	kIx.	.
</s>
<s>
Logaritmus	logaritmus	k1gInSc1	logaritmus
mocniny	mocnina	k1gFnSc2	mocnina
je	být	k5eAaImIp3nS	být
násobek	násobek	k1gInSc4	násobek
logaritmu	logaritmus	k1gInSc2	logaritmus
mocněnce	mocněnec	k1gInSc2	mocněnec
a	a	k8xC	a
logaritmus	logaritmus	k1gInSc1	logaritmus
odmocniny	odmocnina	k1gFnSc2	odmocnina
je	být	k5eAaImIp3nS	být
podíl	podíl	k1gInSc4	podíl
logaritmu	logaritmus	k1gInSc2	logaritmus
odmocněnce	odmocněnec	k1gInSc2	odmocněnec
a	a	k8xC	a
logaritmu	logaritmus	k1gInSc2	logaritmus
odmocnitele	odmocnitel	k1gInSc2	odmocnitel
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
novověku	novověk	k1gInSc2	novověk
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
astronomie	astronomie	k1gFnSc2	astronomie
a	a	k8xC	a
geodézie	geodézie	k1gFnSc2	geodézie
a	a	k8xC	a
s	s	k7c7	s
potřebami	potřeba	k1gFnPc7	potřeba
námořní	námořní	k2eAgFnSc2d1	námořní
navigace	navigace	k1gFnSc2	navigace
<g/>
,	,	kIx,	,
nesmírně	smírně	k6eNd1	smírně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
potřeba	potřeba	k1gFnSc1	potřeba
složitých	složitý	k2eAgInPc2d1	složitý
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
trigonometrických	trigonometrický	k2eAgFnPc6d1	trigonometrická
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
platných	platný	k2eAgFnPc2d1	platná
číslic	číslice	k1gFnPc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
myšlence	myšlenka	k1gFnSc6	myšlenka
nahradit	nahradit	k5eAaPmF	nahradit
násobení	násobení	k1gNnSc4	násobení
sčítáním	sčítání	k1gNnSc7	sčítání
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
korespondence	korespondence	k1gFnSc1	korespondence
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
matematika	matematik	k1gMnSc2	matematik
a	a	k8xC	a
hodináře	hodinář	k1gMnSc2	hodinář
Josta	Jost	k1gMnSc2	Jost
Bürgiho	Bürgi	k1gMnSc2	Bürgi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1588	[number]	k4	1588
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
skutečně	skutečně	k6eAd1	skutečně
sestavil	sestavit	k5eAaPmAgMnS	sestavit
tabulky	tabulka	k1gFnPc4	tabulka
funkce	funkce	k1gFnSc2	funkce
sinus	sinus	k1gInSc1	sinus
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
i	i	k8xC	i
tabulky	tabulka	k1gFnPc1	tabulka
jejích	její	k3xOp3gInPc2	její
logaritmů	logaritmus	k1gInPc2	logaritmus
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
publikoval	publikovat	k5eAaBmAgMnS	publikovat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
publikoval	publikovat	k5eAaBmAgInS	publikovat
metodu	metoda	k1gFnSc4	metoda
logaritmů	logaritmus	k1gInPc2	logaritmus
skotský	skotský	k2eAgMnSc1d1	skotský
matematik	matematik	k1gMnSc1	matematik
John	John	k1gMnSc1	John
Napier	Napier	k1gMnSc1	Napier
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tak	tak	k6eAd1	tak
platí	platit	k5eAaImIp3nS	platit
za	za	k7c4	za
jejich	jejich	k3xOp3gMnSc4	jejich
objevitele	objevitel	k1gMnSc4	objevitel
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1617	[number]	k4	1617
publikoval	publikovat	k5eAaBmAgMnS	publikovat
první	první	k4xOgFnPc4	první
tabulky	tabulka	k1gFnPc4	tabulka
desítkových	desítkový	k2eAgNnPc2d1	desítkové
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
briggsových	briggsových	k2eAgMnSc1d1	briggsových
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
logaritmů	logaritmus	k1gInPc2	logaritmus
Henry	henry	k1gInSc2	henry
Briggs	Briggsa	k1gFnPc2	Briggsa
<g/>
.	.	kIx.	.
</s>
<s>
Logaritmické	logaritmický	k2eAgFnPc1d1	logaritmická
tabulky	tabulka	k1gFnPc1	tabulka
čísel	číslo	k1gNnPc2	číslo
i	i	k8xC	i
goniometrických	goniometrický	k2eAgFnPc2d1	goniometrická
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
na	na	k7c4	na
5	[number]	k4	5
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
10	[number]	k4	10
až	až	k8xS	až
14	[number]	k4	14
platných	platný	k2eAgFnPc2d1	platná
číslic	číslice	k1gFnPc2	číslice
pak	pak	k6eAd1	pak
vycházely	vycházet	k5eAaImAgFnP	vycházet
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
pomůckou	pomůcka	k1gFnSc7	pomůcka
astronomů	astronom	k1gMnPc2	astronom
<g/>
,	,	kIx,	,
geodetů	geodet	k1gMnPc2	geodet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
námořních	námořní	k2eAgMnPc2d1	námořní
kapitánů	kapitán	k1gMnPc2	kapitán
a	a	k8xC	a
studentů	student	k1gMnPc2	student
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
gymnazista	gymnazista	k1gMnSc1	gymnazista
se	se	k3xPyFc4	se
učil	učit	k5eAaImAgMnS	učit
zacházet	zacházet	k5eAaImF	zacházet
se	s	k7c7	s
školními	školní	k2eAgFnPc7d1	školní
<g/>
,	,	kIx,	,
pěti-	pěti-	k?	pěti-
nebo	nebo	k8xC	nebo
sedmimístnými	sedmimístný	k2eAgFnPc7d1	sedmimístná
tabulkami	tabulka	k1gFnPc7	tabulka
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
je	on	k3xPp3gInPc4	on
nenahradily	nahradit	k5eNaPmAgFnP	nahradit
elektronické	elektronický	k2eAgFnPc4d1	elektronická
kalkulačky	kalkulačka	k1gFnPc4	kalkulačka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přibližné	přibližný	k2eAgInPc4d1	přibližný
technické	technický	k2eAgInPc4d1	technický
výpočty	výpočet	k1gInPc4	výpočet
(	(	kIx(	(
<g/>
na	na	k7c4	na
3-4	[number]	k4	3-4
platná	platný	k2eAgNnPc1d1	platné
místa	místo	k1gNnPc1	místo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nesmírně	smírně	k6eNd1	smírně
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
logaritmické	logaritmický	k2eAgNnSc1d1	logaritmické
pravítko	pravítko	k1gNnSc1	pravítko
<g/>
,	,	kIx,	,
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nástroj	nástroj	k1gInSc4	nástroj
každého	každý	k3xTgMnSc2	každý
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
elektronických	elektronický	k2eAgFnPc2d1	elektronická
kalkulaček	kalkulačka	k1gFnPc2	kalkulačka
a	a	k8xC	a
počítačů	počítač	k1gMnPc2	počítač
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tyto	tento	k3xDgFnPc4	tento
pomůcky	pomůcka	k1gFnPc1	pomůcka
pozvolna	pozvolna	k6eAd1	pozvolna
vymizely	vymizet	k5eAaPmAgFnP	vymizet
a	a	k8xC	a
s	s	k7c7	s
logaritmickou	logaritmický	k2eAgFnSc7d1	logaritmická
funkcí	funkce	k1gFnSc7	funkce
pracují	pracovat	k5eAaImIp3nP	pracovat
hlavně	hlavně	k9	hlavně
matematici	matematik	k1gMnPc1	matematik
a	a	k8xC	a
teoretičtí	teoretický	k2eAgMnPc1d1	teoretický
fyzikové	fyzik	k1gMnPc1	fyzik
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
praktický	praktický	k2eAgInSc1d1	praktický
výpočet	výpočet	k1gInSc1	výpočet
jejích	její	k3xOp3gFnPc2	její
hodnoty	hodnota	k1gFnSc2	hodnota
i	i	k8xC	i
logaritmů	logaritmus	k1gInPc2	logaritmus
svěřujeme	svěřovat	k5eAaImIp1nP	svěřovat
počítačům	počítač	k1gMnPc3	počítač
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
log	log	k1gInSc1	log
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
Logaritmus	logaritmus	k1gInSc1	logaritmus
je	být	k5eAaImIp3nS	být
inverzní	inverzní	k2eAgFnSc7d1	inverzní
funkcí	funkce	k1gFnSc7	funkce
k	k	k7c3	k
exponenciální	exponenciální	k2eAgFnSc3d1	exponenciální
funkci	funkce	k1gFnSc3	funkce
o	o	k7c6	o
stejném	stejný	k2eAgInSc6d1	stejný
základu	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
c	c	k0	c
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
+	+	kIx~	+
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
ac	ac	k?	ac
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
Logaritmus	logaritmus	k1gInSc1	logaritmus
součinu	součin	k1gInSc2	součin
je	být	k5eAaImIp3nS	být
součet	součet	k1gInSc1	součet
logaritmů	logaritmus	k1gInPc2	logaritmus
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
činitelů	činitel	k1gInPc2	činitel
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
-	-	kIx~	-
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
a-	a-	k?	a-
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
Logaritmus	logaritmus	k1gInSc1	logaritmus
podílu	podíl	k1gInSc2	podíl
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
logaritmů	logaritmus	k1gInPc2	logaritmus
čitatele	čitatel	k1gInSc2	čitatel
a	a	k8xC	a
jmenovatele	jmenovatel	k1gInSc2	jmenovatel
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
r	r	kA	r
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
;	;	kIx,	;
logaritmus	logaritmus	k1gInSc1	logaritmus
mocniny	mocnina	k1gFnSc2	mocnina
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
exponent	exponent	k1gInSc1	exponent
krát	krát	k6eAd1	krát
logaritmus	logaritmus	k1gInSc1	logaritmus
základu	základ	k1gInSc2	základ
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	k1gInSc1	log
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	k1gInSc1	log
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
Formule	formule	k1gFnSc1	formule
umožňující	umožňující	k2eAgFnSc1d1	umožňující
vyčíslit	vyčíslit	k5eAaPmF	vyčíslit
logaritmus	logaritmus	k1gInSc4	logaritmus
libovolného	libovolný	k2eAgInSc2d1	libovolný
základu	základ	k1gInSc2	základ
pomocí	pomocí	k7c2	pomocí
kalkulačky	kalkulačka	k1gFnSc2	kalkulačka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
logaritmických	logaritmický	k2eAgFnPc2d1	logaritmická
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
log	log	k1gInSc1	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{{	{{	k?	{{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
logaritmickou	logaritmický	k2eAgFnSc4d1	logaritmická
funkci	funkce	k1gFnSc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
prostá	prostý	k2eAgFnSc1d1	prostá
pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
>	>	kIx)	>
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
a	a	k8xC	a
∈	∈	k?	∈
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
klesající	klesající	k2eAgFnSc1d1	klesající
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
1	[number]	k4	1
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
graf	graf	k1gInSc1	graf
funkce	funkce	k1gFnSc2	funkce
prochází	procházet	k5eAaImIp3nS	procházet
bodem	bod	k1gInSc7	bod
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
<g/>
0	[number]	k4	0
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
osa	osa	k1gFnSc1	osa
y	y	k?	y
je	být	k5eAaImIp3nS	být
asymptotou	asymptota	k1gFnSc7	asymptota
grafu	graf	k1gInSc2	graf
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
vypočítat	vypočítat	k5eAaPmF	vypočítat
přirozený	přirozený	k2eAgInSc4d1	přirozený
logaritmus	logaritmus	k1gInSc4	logaritmus
komplexního	komplexní	k2eAgNnSc2d1	komplexní
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
y	y	k?	y
)	)	kIx)	)
:	:	kIx,	:
=	=	kIx~	=
ln	ln	k?	ln
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
ln	ln	k?	ln
:	:	kIx,	:
r	r	kA	r
+	+	kIx~	+
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
e	e	k0	e
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
phi	phi	k?	phi
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
r	r	kA	r
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
phi	phi	k?	phi
}	}	kIx)	}
:	:	kIx,	:
Byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
použit	použít	k5eAaPmNgInS	použít
exponenciální	exponenciální	k2eAgInSc1d1	exponenciální
tvar	tvar	k1gInSc1	tvar
komplexního	komplexní	k2eAgNnSc2d1	komplexní
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Proměnná	proměnná	k1gFnSc1	proměnná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
z	z	k7c2	z
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
udává	udávat	k5eAaImIp3nS	udávat
absolutní	absolutní	k2eAgFnSc4d1	absolutní
hodnotu	hodnota	k1gFnSc4	hodnota
komplexního	komplexní	k2eAgNnSc2d1	komplexní
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
phi	phi	k?	phi
}	}	kIx)	}
udává	udávat	k5eAaImIp3nS	udávat
argument	argument	k1gInSc1	argument
komplexního	komplexní	k2eAgNnSc2d1	komplexní
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
tedy	tedy	k9	tedy
komplexní	komplexní	k2eAgNnSc4d1	komplexní
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
reálná	reálný	k2eAgFnSc1d1	reálná
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gInSc2	jeho
logaritmu	logaritmus	k1gInSc2	logaritmus
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
logaritmu	logaritmus	k1gInSc3	logaritmus
absolutní	absolutní	k2eAgFnSc4d1	absolutní
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
imaginární	imaginární	k2eAgInSc1d1	imaginární
udává	udávat	k5eAaImIp3nS	udávat
argument	argument	k1gInSc1	argument
(	(	kIx(	(
<g/>
úhel	úhel	k1gInSc1	úhel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
úhel	úhel	k1gInSc1	úhel
komplexního	komplexní	k2eAgNnSc2d1	komplexní
čísla	číslo	k1gNnSc2	číslo
není	být	k5eNaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
lišit	lišit	k5eAaImF	lišit
o	o	k7c4	o
násobky	násobek	k1gInPc4	násobek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
π	π	k?	π
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zavádí	zavádět	k5eAaImIp3nS	zavádět
tzv.	tzv.	kA	tzv.
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hodnota	hodnota	k1gFnSc1	hodnota
logaritmu	logaritmus	k1gInSc2	logaritmus
<g/>
,	,	kIx,	,
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Ln	Ln	k1gFnSc1	Ln
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
úhly	úhel	k1gInPc1	úhel
z	z	k7c2	z
intervalu	interval	k1gInSc2	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
-	-	kIx~	-
π	π	k?	π
,	,	kIx,	,
π	π	k?	π
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
rangle	rangle	k1gNnPc6	rangle
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
rovností	rovnost	k1gFnPc2	rovnost
lze	lze	k6eAd1	lze
složitější	složitý	k2eAgFnSc2d2	složitější
operace	operace	k1gFnSc2	operace
převádět	převádět	k5eAaImF	převádět
na	na	k7c4	na
jednodušší	jednoduchý	k2eAgNnSc4d2	jednodušší
(	(	kIx(	(
<g/>
násobení	násobení	k1gNnSc4	násobení
a	a	k8xC	a
dělení	dělení	k1gNnSc4	dělení
na	na	k7c4	na
sčítání	sčítání	k1gNnSc4	sčítání
a	a	k8xC	a
odčítání	odčítání	k1gNnSc4	odčítání
<g/>
,	,	kIx,	,
mocnění	mocnění	k1gNnSc4	mocnění
a	a	k8xC	a
odmocniny	odmocnina	k1gFnPc4	odmocnina
na	na	k7c4	na
násobení	násobení	k1gNnSc4	násobení
a	a	k8xC	a
dělení	dělení	k1gNnSc4	dělení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
před	před	k7c7	před
rozšířením	rozšíření	k1gNnSc7	rozšíření
elektronických	elektronický	k2eAgFnPc2d1	elektronická
kalkulaček	kalkulačka	k1gFnPc2	kalkulačka
a	a	k8xC	a
počítačů	počítač	k1gMnPc2	počítač
využívalo	využívat	k5eAaPmAgNnS	využívat
při	při	k7c6	při
složitějších	složitý	k2eAgInPc6d2	složitější
výpočtech	výpočet	k1gInPc6	výpočet
prováděných	prováděný	k2eAgInPc6d1	prováděný
ručně	ručně	k6eAd1	ručně
nebo	nebo	k8xC	nebo
mechanickými	mechanický	k2eAgMnPc7d1	mechanický
kalkulátory	kalkulátor	k1gMnPc7	kalkulátor
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
obvykle	obvykle	k6eAd1	obvykle
uměly	umět	k5eAaImAgFnP	umět
jen	jen	k6eAd1	jen
sčítat	sčítat	k5eAaImF	sčítat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
přepočtů	přepočet	k1gInPc2	přepočet
existovaly	existovat	k5eAaImAgFnP	existovat
logaritmické	logaritmický	k2eAgFnPc1d1	logaritmická
tabulky	tabulka	k1gFnPc1	tabulka
s	s	k7c7	s
předvypočítanými	předvypočítaný	k2eAgFnPc7d1	předvypočítaný
hodnotami	hodnota	k1gFnPc7	hodnota
logaritmů	logaritmus	k1gInPc2	logaritmus
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
logaritmické	logaritmický	k2eAgNnSc1d1	logaritmické
pravítko	pravítko	k1gNnSc1	pravítko
<g/>
,	,	kIx,	,
mechanická	mechanický	k2eAgFnSc1d1	mechanická
pomůcka	pomůcka	k1gFnSc1	pomůcka
pro	pro	k7c4	pro
výpočty	výpočet	k1gInPc4	výpočet
pomocí	pomocí	k7c2	pomocí
logaritmů	logaritmus	k1gInPc2	logaritmus
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
využití	využití	k1gNnSc2	využití
logaritmů	logaritmus	k1gInPc2	logaritmus
je	být	k5eAaImIp3nS	být
výpočet	výpočet	k1gInSc1	výpočet
17300	[number]	k4	17300
·	·	k?	·
√	√	k?	√
<g/>
15478	[number]	k4	15478
pomocí	pomocí	k7c2	pomocí
tabulek	tabulka	k1gFnPc2	tabulka
logaritmů	logaritmus	k1gInPc2	logaritmus
<g/>
:	:	kIx,	:
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
rovnice	rovnice	k1gFnSc1	rovnice
zlogaritmuje	zlogaritmovat	k5eAaPmIp3nS	zlogaritmovat
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
log	log	kA	log
:	:	kIx,	:
(	(	kIx(	(
17300	[number]	k4	17300
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
15478	[number]	k4	15478
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
<g/>
(	(	kIx(	(
<g/>
17300	[number]	k4	17300
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
15478	[number]	k4	15478
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pomocí	pomocí	k7c2	pomocí
rovností	rovnost	k1gFnPc2	rovnost
o	o	k7c6	o
logaritmech	logaritmus	k1gInPc6	logaritmus
se	se	k3xPyFc4	se
rovnice	rovnice	k1gFnPc1	rovnice
rozloží	rozložit	k5eAaPmIp3nP	rozložit
na	na	k7c4	na
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
log	log	kA	log
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
17300	[number]	k4	17300
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
15478	[number]	k4	15478
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
log	log	k1gInSc1	log
17300	[number]	k4	17300
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
log	log	k1gInSc1	log
15478	[number]	k4	15478
<g/>
}	}	kIx)	}
:	:	kIx,	:
V	v	k7c6	v
tabulkách	tabulka	k1gFnPc6	tabulka
se	se	k3xPyFc4	se
vyhledají	vyhledat	k5eAaPmIp3nP	vyhledat
příslušné	příslušný	k2eAgInPc1d1	příslušný
logaritmy	logaritmus	k1gInPc1	logaritmus
(	(	kIx(	(
<g/>
tabulky	tabulka	k1gFnPc1	tabulka
ovšem	ovšem	k9	ovšem
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hodnoty	hodnota	k1gFnPc4	hodnota
jen	jen	k9	jen
na	na	k7c4	na
několik	několik	k4yIc4	několik
platných	platný	k2eAgFnPc2d1	platná
číslic	číslice	k1gFnPc2	číslice
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
log	log	kA	log
17300	[number]	k4	17300
≅	≅	k?	≅
4,238	[number]	k4	4,238
log	log	kA	log
15478	[number]	k4	15478
≅	≅	k?	≅
log	log	kA	log
15480	[number]	k4	15480
≅	≅	k?	≅
4,189	[number]	k4	4,189
<g/>
8	[number]	k4	8
Vypočte	vypočíst	k5eAaPmIp3nS	vypočíst
se	se	k3xPyFc4	se
výsledek	výsledek	k1gInSc1	výsledek
logaritmovaného	logaritmovaný	k2eAgInSc2d1	logaritmovaný
výrazu	výraz	k1gInSc2	výraz
<g/>
:	:	kIx,	:
log	log	kA	log
x	x	k?	x
=	=	kIx~	=
4,238	[number]	k4	4,238
+	+	kIx~	+
2,094	[number]	k4	2,094
<g/>
9	[number]	k4	9
=	=	kIx~	=
6,332	[number]	k4	6,332
<g/>
9	[number]	k4	9
Rovnice	rovnice	k1gFnSc1	rovnice
se	se	k3xPyFc4	se
zpětně	zpětně	k6eAd1	zpětně
umocní	umocnit	k5eAaPmIp3nS	umocnit
podle	podle	k7c2	podle
daného	daný	k2eAgInSc2d1	daný
základu	základ	k1gInSc2	základ
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc1	výsledek
se	se	k3xPyFc4	se
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
dohledá	dohledat	k5eAaPmIp3nS	dohledat
zpětně	zpětně	k6eAd1	zpětně
<g/>
:	:	kIx,	:
106,332	[number]	k4	106,332
<g/>
9	[number]	k4	9
≅	≅	k?	≅
2152000	[number]	k4	2152000
<g/>
.	.	kIx.	.
</s>
<s>
Nalezený	nalezený	k2eAgInSc1d1	nalezený
výsledek	výsledek	k1gInSc1	výsledek
<g/>
:	:	kIx,	:
17300	[number]	k4	17300
*	*	kIx~	*
√	√	k?	√
<g/>
15478	[number]	k4	15478
≅	≅	k?	≅
2152000	[number]	k4	2152000
(	(	kIx(	(
<g/>
přesnější	přesný	k2eAgInSc1d2	přesnější
výsledek	výsledek	k1gInSc1	výsledek
spočtený	spočtený	k2eAgInSc1d1	spočtený
na	na	k7c6	na
dnešní	dnešní	k2eAgFnSc6d1	dnešní
kalkulačce	kalkulačka	k1gFnSc6	kalkulačka
je	být	k5eAaImIp3nS	být
2152303.56	[number]	k4	2152303.56
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
odchylka	odchylka	k1gFnSc1	odchylka
0.014	[number]	k4	0.014
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Logaritmy	logaritmus	k1gInPc1	logaritmus
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
také	také	k9	také
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
vědeckých	vědecký	k2eAgInPc6d1	vědecký
oborech	obor	k1gInPc6	obor
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
exponentu	exponent	k1gInSc6	exponent
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
jednotky	jednotka	k1gFnPc1	jednotka
decibel	decibel	k1gInSc4	decibel
a	a	k8xC	a
neper	prát	k5eNaImRp2nS	prát
<g/>
,	,	kIx,	,
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
či	či	k8xC	či
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
kyselosti	kyselost	k1gFnSc2	kyselost
roztoků	roztok	k1gInPc2	roztok
pomocí	pomocí	k7c2	pomocí
pH	ph	kA	ph
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
veličiny	veličina	k1gFnPc1	veličina
nabývají	nabývat	k5eAaImIp3nP	nabývat
výrazného	výrazný	k2eAgNnSc2d1	výrazné
rozpětí	rozpětí	k1gNnSc2	rozpětí
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
několika	několik	k4yIc2	několik
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
kationtů	kation	k1gInPc2	kation
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
+	+	kIx~	+
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
zobrazení	zobrazení	k1gNnSc6	zobrazení
těchto	tento	k3xDgFnPc2	tento
hodnot	hodnota	k1gFnPc2	hodnota
na	na	k7c6	na
číselné	číselný	k2eAgFnSc6d1	číselná
ose	osa	k1gFnSc6	osa
bude	být	k5eAaImBp3nS	být
pivo	pivo	k1gNnSc1	pivo
přibližně	přibližně	k6eAd1	přibližně
43	[number]	k4	43
<g/>
×	×	k?	×
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
nule	nula	k1gFnSc3	nula
než	než	k8xS	než
ocet	ocet	k1gInSc1	ocet
(	(	kIx(	(
<g/>
0,0013	[number]	k4	0,0013
<g/>
/	/	kIx~	/
<g/>
0,00003	[number]	k4	0,00003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
bude	být	k5eAaImBp3nS	být
100	[number]	k4	100
<g/>
×	×	k?	×
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
nule	nula	k1gFnSc3	nula
než	než	k8xS	než
pivo	pivo	k1gNnSc4	pivo
(	(	kIx(	(
<g/>
0,00003	[number]	k4	0,00003
<g/>
/	/	kIx~	/
<g/>
0,0000003	[number]	k4	0,0000003
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
hodnoty	hodnota	k1gFnPc1	hodnota
také	také	k9	také
budou	být	k5eAaImBp3nP	být
"	"	kIx"	"
<g/>
namačkány	namačkán	k2eAgMnPc4d1	namačkán
<g/>
"	"	kIx"	"
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
nuly	nula	k1gFnSc2	nula
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
například	například	k6eAd1	například
koncentrace	koncentrace	k1gFnPc1	koncentrace
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
+	+	kIx~	+
v	v	k7c6	v
mléku	mléko	k1gNnSc6	mléko
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
100000	[number]	k4	100000
<g/>
×	×	k?	×
(	(	kIx(	(
<g/>
o	o	k7c4	o
pět	pět	k4xCc4	pět
dekadických	dekadický	k2eAgInPc2d1	dekadický
řádů	řád	k1gInPc2	řád
<g/>
)	)	kIx)	)
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
ve	v	k7c6	v
čpavku	čpavek	k1gInSc6	čpavek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
výhodnější	výhodný	k2eAgNnSc1d2	výhodnější
místo	místo	k1gNnSc1	místo
samotné	samotný	k2eAgFnSc2d1	samotná
koncentrace	koncentrace	k1gFnSc2	koncentrace
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
její	její	k3xOp3gInSc4	její
logaritmus	logaritmus	k1gInSc4	logaritmus
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
volně	volně	k6eAd1	volně
řečeno	říct	k5eAaPmNgNnS	říct
"	"	kIx"	"
<g/>
řád	řád	k1gInSc1	řád
koncentrace	koncentrace	k1gFnSc2	koncentrace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
po	po	k7c4	po
zlogaritmování	zlogaritmování	k1gNnSc4	zlogaritmování
bude	být	k5eAaImBp3nS	být
vypadat	vypadat	k5eAaImF	vypadat
následovně	následovně	k6eAd1	následovně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
upravené	upravený	k2eAgFnPc1d1	upravená
hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
celkem	celkem	k6eAd1	celkem
rozumně	rozumně	k6eAd1	rozumně
rozloženy	rozložit	k5eAaPmNgInP	rozložit
mezi	mezi	k7c4	mezi
-	-	kIx~	-
a	a	k8xC	a
nulou	nula	k1gFnSc7	nula
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
dodejme	dodat	k5eAaPmRp1nP	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pH	ph	kA	ph
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
přibližně	přibližně	k6eAd1	přibližně
takto	takto	k6eAd1	takto
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
logaritmus	logaritmus	k1gInSc1	logaritmus
koncentrace	koncentrace	k1gFnSc2	koncentrace
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
bez	bez	k7c2	bez
znaménka	znaménko	k1gNnSc2	znaménko
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
menší	malý	k2eAgFnSc1d2	menší
nebo	nebo	k8xC	nebo
rovna	roven	k2eAgFnSc1d1	rovna
1	[number]	k4	1
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
logaritmus	logaritmus	k1gInSc1	logaritmus
koncentrace	koncentrace	k1gFnSc2	koncentrace
bude	být	k5eAaImBp3nS	být
vždy	vždy	k6eAd1	vždy
menší	malý	k2eAgInSc1d2	menší
nebo	nebo	k8xC	nebo
roven	roven	k2eAgInSc1d1	roven
0	[number]	k4	0
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
U	u	k7c2	u
logaritmu	logaritmus	k1gInSc2	logaritmus
o	o	k7c6	o
základu	základ	k1gInSc6	základ
<g />
.	.	kIx.	.
</s>
<s>
10	[number]	k4	10
(	(	kIx(	(
<g/>
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
desítkový	desítkový	k2eAgInSc4d1	desítkový
či	či	k8xC	či
dekadický	dekadický	k2eAgInSc4d1	dekadický
logaritmus	logaritmus	k1gInSc4	logaritmus
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
Briggsův	Briggsův	k2eAgInSc4d1	Briggsův
podle	podle	k7c2	podle
Henryho	Henry	k1gMnSc2	Henry
Briggse	Briggse	k1gFnSc2	Briggse
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ve	v	k7c6	v
značení	značení	k1gNnSc6	značení
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
základ	základ	k1gInSc1	základ
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
jen	jen	k9	jen
prostě	prostě	k9	prostě
log	log	kA	log
x	x	k?	x
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
speciální	speciální	k2eAgNnSc1d1	speciální
značení	značení	k1gNnSc1	značení
lg	lg	k?	lg
x.	x.	k?	x.
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
však	však	k9	však
dát	dát	k5eAaPmF	dát
pozor	pozor	k1gInSc4	pozor
<g/>
:	:	kIx,	:
použité	použitý	k2eAgNnSc4d1	Použité
značení	značení	k1gNnSc4	značení
<g />
.	.	kIx.	.
</s>
<s hack="1">
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
odborných	odborný	k2eAgFnPc6d1	odborná
literaturách	literatura	k1gFnPc6	literatura
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
lg	lg	k?	lg
x	x	k?	x
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
využívá	využívat	k5eAaPmIp3nS	využívat
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
a	a	k8xC	a
ne	ne	k9	ne
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Logaritmus	logaritmus	k1gInSc1	logaritmus
o	o	k7c6	o
základu	základ	k1gInSc6	základ
e	e	k0	e
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
)	)	kIx)	)
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
přirozený	přirozený	k2eAgInSc4d1	přirozený
logaritmus	logaritmus	k1gInSc4	logaritmus
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Napierův	Napierův	k2eAgInSc4d1	Napierův
podle	podle	k7c2	podle
Johna	John	k1gMnSc2	John
Napiera	Napier	k1gMnSc2	Napier
<g/>
)	)	kIx)	)
a	a	k8xC	a
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
x	x	k?	x
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
logaritmus	logaritmus	k1gInSc1	logaritmus
naturalis	naturalis	k1gFnSc2	naturalis
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
přirozený	přirozený	k2eAgInSc1d1	přirozený
logaritmus	logaritmus	k1gInSc1	logaritmus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hledal	hledat	k5eAaImAgInS	hledat
základ	základ	k1gInSc1	základ
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tečnou	tečný	k2eAgFnSc4d1	tečná
této	tento	k3xDgFnSc6	tento
exponenciály	exponenciál	k1gInPc4	exponenciál
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
A	A	kA	A
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
přímka	přímka	k1gFnSc1	přímka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
x	x	k?	x
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
základ	základ	k1gInSc1	základ
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
písmenem	písmeno	k1gNnSc7	písmeno
e	e	k0	e
a	a	k8xC	a
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
Eulerovo	Eulerův	k2eAgNnSc4d1	Eulerovo
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Leonharda	Leonhard	k1gMnSc2	Leonhard
Eulera	Euler	k1gMnSc2	Euler
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
objevu	objev	k1gInSc6	objev
tohoto	tento	k3xDgNnSc2	tento
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
logaritmus	logaritmus	k1gInSc1	logaritmus
o	o	k7c6	o
základu	základ	k1gInSc6	základ
dva	dva	k4xCgInPc4	dva
(	(	kIx(	(
<g/>
binární	binární	k2eAgInSc1d1	binární
logaritmus	logaritmus	k1gInSc1	logaritmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
příslušném	příslušný	k2eAgInSc6d1	příslušný
kontextu	kontext	k1gInSc6	kontext
někdy	někdy	k6eAd1	někdy
značen	značen	k2eAgInSc1d1	značen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lg	lg	k?	lg
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lg	lg	k?	lg
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
případně	případně	k6eAd1	případně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lb	lb	k?	lb
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
lb	lb	k?	lb
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	k1gInSc1	log
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
log	log	k1gInSc1	log
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Např	např	kA	např
při	při	k7c6	při
binárním	binární	k2eAgNnSc6d1	binární
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
v	v	k7c6	v
setříděném	setříděný	k2eAgInSc6d1	setříděný
seznamu	seznam	k1gInSc6	seznam
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
n	n	k0	n
položek	položka	k1gFnPc2	položka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
maximálně	maximálně	k6eAd1	maximálně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
kroků	krok	k1gInPc2	krok
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
hledané	hledaný	k2eAgFnSc2d1	hledaná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
půlení	půlení	k1gNnSc4	půlení
intervalů	interval	k1gInPc2	interval
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
vztahu	vztah	k1gInSc2	vztah
pro	pro	k7c4	pro
Taylorův	Taylorův	k2eAgInSc4d1	Taylorův
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zintegrováním	zintegrování	k1gNnSc7	zintegrování
geometrické	geometrický	k2eAgFnSc2d1	geometrická
řady	řada	k1gFnSc2	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
1	[number]	k4	1
+	+	kIx~	+
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
-	-	kIx~	-
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
⋯	⋯	k?	⋯
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
-x	-x	k?	-x
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-x	-x	k?	-x
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
}	}	kIx)	}
:	:	kIx,	:
lze	lze	k6eAd1	lze
obdržet	obdržet	k5eAaPmF	obdržet
rozvoj	rozvoj	k1gInSc4	rozvoj
logaritmu	logaritmus	k1gInSc2	logaritmus
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
kolem	kolem	k7c2	kolem
1	[number]	k4	1
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
x	x	k?	x
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
x-	x-	k?	x-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
řada	řada	k1gFnSc1	řada
má	mít	k5eAaImIp3nS	mít
poloměr	poloměr	k1gInSc4	poloměr
konvergence	konvergence	k1gFnSc1	konvergence
1	[number]	k4	1
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
přitom	přitom	k6eAd1	přitom
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
i	i	k9	i
pro	pro	k7c4	pro
krajní	krajní	k2eAgInSc4d1	krajní
bod	bod	k1gInSc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
známou	známý	k2eAgFnSc4d1	známá
řadu	řada	k1gFnSc4	řada
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
2	[number]	k4	2
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
harmonická	harmonický	k2eAgFnSc1d1	harmonická
řada	řada	k1gFnSc1	řada
s	s	k7c7	s
oscilujícími	oscilující	k2eAgNnPc7d1	oscilující
znaménky	znaménko	k1gNnPc7	znaménko
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
2	[number]	k4	2
=	=	kIx~	=
1	[number]	k4	1
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
2	[number]	k4	2
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
}	}	kIx)	}
:	:	kIx,	:
Řadu	řada	k1gFnSc4	řada
pro	pro	k7c4	pro
logaritmus	logaritmus	k1gInSc4	logaritmus
můžeme	moct	k5eAaImIp1nP	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
i	i	k9	i
pro	pro	k7c4	pro
komplexní	komplexní	k2eAgNnSc4d1	komplexní
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
z	z	k7c2	z
předchozího	předchozí	k2eAgNnSc2d1	předchozí
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednak	jednak	k8xC	jednak
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
r	r	kA	r
c	c	k0	c
t	t	k?	t
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
arctg	arctg	k1gInSc1	arctg
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
:	:	kIx,	:
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
můžeme	moct	k5eAaImIp1nP	moct
tento	tento	k3xDgInSc4	tento
výraz	výraz	k1gInSc4	výraz
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
pomocí	pomocí	k7c2	pomocí
řady	řada	k1gFnSc2	řada
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
i	i	k8xC	i
:	:	kIx,	:
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
y	y	k?	y
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
<g />
.	.	kIx.	.
</s>
<s>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
}	}	kIx)	}
:	:	kIx,	:
Porovnáním	porovnání	k1gNnSc7	porovnání
imaginárních	imaginární	k2eAgFnPc2d1	imaginární
částí	část	k1gFnPc2	část
získáváme	získávat	k5eAaImIp1nP	získávat
řadu	řada	k1gFnSc4	řada
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
r	r	kA	r
c	c	k0	c
t	t	k?	t
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
arctg	arctg	k1gInSc4	arctg
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
r	r	kA	r
c	c	k0	c
t	t	k?	t
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
y	y	k?	y
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s hack="1">
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
arctg	arctg	k1gInSc4	arctg
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
y-	y-	k?	y-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
}	}	kIx)	}
:	:	kIx,	:
Speciálně	speciálně	k6eAd1	speciálně
dosazením	dosazení	k1gNnSc7	dosazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
<g/>
}	}	kIx)	}
dostaneme	dostat	k5eAaPmIp1nP	dostat
nejslavnější	slavný	k2eAgFnSc4d3	nejslavnější
řadu	řada	k1gFnSc4	řada
pro	pro	k7c4	pro
Ludolfovo	Ludolfův	k2eAgNnSc4d1	Ludolfovo
číslo	číslo	k1gNnSc4	číslo
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
}	}	kIx)	}
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
krásu	krása	k1gFnSc4	krása
<g/>
,	,	kIx,	,
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Diskrétní	diskrétní	k2eAgInSc4d1	diskrétní
logaritmus	logaritmus	k1gInSc4	logaritmus
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
logaritmus	logaritmus	k1gInSc1	logaritmus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
logaritmus	logaritmus	k1gInSc1	logaritmus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Logaritmus	logaritmus	k1gInSc1	logaritmus
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
MathWorld	MathWorlda	k1gFnPc2	MathWorlda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Logaritmické	logaritmický	k2eAgFnSc2d1	logaritmická
tabulky	tabulka	k1gFnSc2	tabulka
čísel	číslo	k1gNnPc2	číslo
od	od	k7c2	od
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
do	do	k7c2	do
9	[number]	k4	9
999	[number]	k4	999
999	[number]	k4	999
</s>
