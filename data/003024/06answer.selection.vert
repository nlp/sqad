<s>
Logaritmická	logaritmický	k2eAgFnSc1d1	logaritmická
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
matematická	matematický	k2eAgFnSc1d1	matematická
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
inverzní	inverzní	k2eAgFnSc1d1	inverzní
k	k	k7c3	k
exponenciální	exponenciální	k2eAgFnSc3d1	exponenciální
funkci	funkce	k1gFnSc3	funkce
<g/>
.	.	kIx.	.
</s>
