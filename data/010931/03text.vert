<p>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
barnatý	barnatý	k2eAgInSc1d1	barnatý
je	být	k5eAaImIp3nS	být
anorganická	anorganický	k2eAgFnSc1d1	anorganická
sloučenina	sloučenina	k1gFnSc1	sloučenina
se	s	k7c7	s
vzorcem	vzorec	k1gInSc7	vzorec
BaS	basa	k1gFnPc2	basa
<g/>
.	.	kIx.	.
</s>
<s>
Chalkogenidy	Chalkogenida	k1gFnPc1	Chalkogenida
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
intenzivně	intenzivně	k6eAd1	intenzivně
studovány	studovat	k5eAaImNgInP	studovat
jako	jako	k8xC	jako
kandidáti	kandidát	k1gMnPc1	kandidát
na	na	k7c4	na
krátkovlnné	krátkovlnný	k2eAgInPc4d1	krátkovlnný
emitery	emiter	k1gInPc4	emiter
pro	pro	k7c4	pro
elektronické	elektronický	k2eAgFnPc4d1	elektronická
displeje	displej	k1gFnPc4	displej
<g/>
.	.	kIx.	.
</s>
<s>
BaS	bas	k1gInSc1	bas
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
syntetickou	syntetický	k2eAgFnSc4d1	syntetická
sloučeninu	sloučenina	k1gFnSc4	sloučenina
barya	baryum	k1gNnSc2	baryum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prekurzorem	prekurzor	k1gMnSc7	prekurzor
BaCO	BaCO	k1gFnSc2	BaCO
<g/>
3	[number]	k4	3
a	a	k8xC	a
pigmentu	pigment	k1gInSc6	pigment
lithoponu	lithopon	k1gInSc2	lithopon
(	(	kIx(	(
<g/>
ZnS	ZnS	k1gFnSc1	ZnS
<g/>
/	/	kIx~	/
<g/>
BaSO	basa	k1gFnSc5	basa
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objevení	objevení	k1gNnSc1	objevení
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
první	první	k4xOgInSc1	první
sulfid	sulfid	k1gInSc1	sulfid
barnatý	barnatý	k2eAgInSc1d1	barnatý
připravil	připravit	k5eAaPmAgInS	připravit
Vincentius	Vincentius	k1gInSc1	Vincentius
(	(	kIx(	(
<g/>
či	či	k8xC	či
Vincentinus	Vincentinus	k1gMnSc1	Vincentinus
<g/>
)	)	kIx)	)
Casciarolus	Casciarolus	k1gMnSc1	Casciarolus
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Casciorolus	Casciorolus	k1gMnSc1	Casciorolus
<g/>
,	,	kIx,	,
1571	[number]	k4	1571
<g/>
-	-	kIx~	-
<g/>
1624	[number]	k4	1624
<g/>
)	)	kIx)	)
postupem	postup	k1gInSc7	postup
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
znám	znám	k2eAgInSc1d1	znám
jako	jako	k9	jako
"	"	kIx"	"
<g/>
karbotermická	karbotermický	k2eAgFnSc1d1	karbotermický
redukce	redukce	k1gFnSc1	redukce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
uhlíku	uhlík	k1gInSc2	uhlík
však	však	k9	však
použil	použít	k5eAaPmAgInS	použít
mouku	mouka	k1gFnSc4	mouka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
BaSO	basa	k1gFnSc5	basa
<g/>
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
C	C	kA	C
→	→	k?	→
BaS	bas	k1gInSc1	bas
+	+	kIx~	+
2	[number]	k4	2
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
Látka	látka	k1gFnSc1	látka
byla	být	k5eAaImAgFnS	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
fosforescenci	fosforescence	k1gFnSc3	fosforescence
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
alchymistů	alchymista	k1gMnPc2	alchymista
a	a	k8xC	a
chemiků	chemik	k1gMnPc2	chemik
pak	pak	k6eAd1	pak
experimentovalo	experimentovat	k5eAaImAgNnS	experimentovat
s	s	k7c7	s
materiálem	materiál	k1gInSc7	materiál
známým	známý	k2eAgInSc7d1	známý
jako	jako	k8xC	jako
Lapis	lapis	k1gInSc4	lapis
Boloniensis	Boloniensis	k1gFnSc2	Boloniensis
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
boloňský	boloňský	k2eAgInSc1d1	boloňský
kámen	kámen	k1gInSc1	kámen
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
či	či	k8xC	či
Chrysolapis	Chrysolapis	k1gFnSc1	Chrysolapis
<g/>
.	.	kIx.	.
<g/>
Andreas	Andreas	k1gMnSc1	Andreas
Sigismund	Sigismund	k1gMnSc1	Sigismund
<g />
.	.	kIx.	.
</s>
<s>
Marggraf	Marggraf	k1gMnSc1	Marggraf
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kalcit	kalcit	k1gInSc1	kalcit
a	a	k8xC	a
sádra	sádra	k1gFnSc1	sádra
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
boloňského	boloňský	k2eAgInSc2d1	boloňský
kamene	kámen	k1gInSc2	kámen
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
fluoritu	fluorit	k1gInSc2	fluorit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgMnSc2	který
je	být	k5eAaImIp3nS	být
boloňský	boloňský	k2eAgInSc1d1	boloňský
kámen	kámen	k1gInSc1	kámen
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
síran	síran	k1gInSc1	síran
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
.	.	kIx.	.
<g/>
BaSO	basa	k1gFnSc5	basa
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
běžného	běžný	k2eAgInSc2d1	běžný
nerostu	nerost	k1gInSc2	nerost
barytu	baryt	k1gInSc2	baryt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
barnatý	barnatý	k2eAgInSc1d1	barnatý
taje	tát	k5eAaImIp3nS	tát
při	při	k7c6	při
1	[number]	k4	1
200	[number]	k4	200
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
charakteru	charakter	k1gInSc2	charakter
NaCl	NaCla	k1gFnPc2	NaCla
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgMnSc1d1	bezbarvý
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jiné	jiný	k2eAgInPc1d1	jiný
sulfidy	sulfid	k1gInPc1	sulfid
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
nečistých	čistý	k2eNgFnPc6d1	nečistá
zbarvených	zbarvený	k2eAgFnPc6d1	zbarvená
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
vylepšenou	vylepšený	k2eAgFnSc7d1	vylepšená
verzí	verze	k1gFnSc7	verze
Casciarolova	Casciarolův	k2eAgInSc2d1	Casciarolův
postupu	postup	k1gInSc2	postup
<g/>
:	:	kIx,	:
místo	místo	k7c2	místo
mouky	mouka	k1gFnSc2	mouka
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
koks	koks	k1gInSc1	koks
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
BaS	bas	k1gInSc1	bas
je	být	k5eAaImIp3nS	být
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
se	s	k7c7	s
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
jiných	jiný	k2eAgInPc2d1	jiný
sulfidů	sulfid	k1gInPc2	sulfid
<g/>
)	)	kIx)	)
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
velmi	velmi	k6eAd1	velmi
toxický	toxický	k2eAgInSc1d1	toxický
plyn	plyn	k1gInSc1	plyn
sulfan	sulfana	k1gFnPc2	sulfana
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
záměna	záměna	k1gFnSc1	záměna
za	za	k7c4	za
síran	síran	k1gInSc4	síran
barnatý	barnatý	k2eAgInSc4d1	barnatý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
neškodný	škodný	k2eNgMnSc1d1	neškodný
a	a	k8xC	a
podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
jako	jako	k9	jako
rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
kontrastní	kontrastní	k2eAgFnSc1d1	kontrastní
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
omylem	omyl	k1gInSc7	omyl
použije	použít	k5eAaPmIp3nS	použít
sulfid	sulfid	k1gInSc1	sulfid
barnatý	barnatý	k2eAgInSc1d1	barnatý
<g/>
,	,	kIx,	,
způsobí	způsobit	k5eAaPmIp3nS	způsobit
to	ten	k3xDgNnSc1	ten
smrtelnou	smrtelný	k2eAgFnSc4d1	smrtelná
otravu	otrava	k1gFnSc4	otrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Barium	barium	k1gNnSc1	barium
sulfide	sulfid	k1gInSc5	sulfid
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
JIŘÍ	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
KAREL	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
ALOIS	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
