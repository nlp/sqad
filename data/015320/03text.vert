<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuOrlické	infoboxuOrlický	k2eAgFnSc3d1
horyIUCN	horyIUCN	k?
kategorie	kategorie	k1gFnPc4
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Pohled	pohled	k1gInSc1
z	z	k7c2
Velké	velká	k1gFnSc2
Deštné	deštný	k2eAgFnSc2d1
směrem	směr	k1gInSc7
k	k	k7c3
jihovýchoduZákladní	jihovýchoduZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1969	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
204	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Orlické	orlický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
64	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.orlickehory.ochranaprirody.cz	www.orlickehory.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Logo	logo	k1gNnSc1
CHKO	CHKO	kA
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1969	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
k	k	k7c3
ochraně	ochrana	k1gFnSc3
pozoruhodně	pozoruhodně	k6eAd1
zachovalého	zachovalý	k2eAgInSc2d1
krajinného	krajinný	k2eAgInSc2d1
celku	celek	k1gInSc2
tvořeného	tvořený	k2eAgInSc2d1
hřebenem	hřeben	k1gInSc7
Orlických	orlický	k2eAgFnPc2d1
hor.	hor.	k?
</s>
<s>
Seznam	seznam	k1gInSc1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgNnPc2d1
maloplošných	maloplošný	k2eAgNnPc2d1
území	území	k1gNnPc2
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
</s>
<s>
Bukačka	Bukačka	k1gFnSc1
</s>
<s>
Trčkov	Trčkov	k1gInSc1
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Bedřichovka	Bedřichovka	k1gFnSc1
</s>
<s>
Černý	černý	k2eAgInSc1d1
důl	důl	k1gInSc1
</s>
<s>
Hraniční	hraniční	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
Jelení	jelení	k2eAgFnSc1d1
lázeň	lázeň	k1gFnSc1
</s>
<s>
Komáří	komáří	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Neratovské	Neratovský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
</s>
<s>
Pod	pod	k7c7
Vrchmezím	Vrchmeze	k1gFnPc3
</s>
<s>
Pod	pod	k7c7
Zakletým	zakletý	k2eAgInSc7d1
</s>
<s>
Kačerov	Kačerov	k1gInSc1
</s>
<s>
Sedloňovský	Sedloňovský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Trčkovská	Trčkovský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Kačenčina	Kačenčin	k2eAgFnSc1d1
zahrádka	zahrádka	k1gFnSc1
</s>
<s>
Rašeliniště	rašeliniště	k1gNnSc1
pod	pod	k7c7
Pětirozcestím	Pětirozcestí	k1gNnSc7
</s>
<s>
Rašeliniště	rašeliniště	k1gNnSc1
pod	pod	k7c7
Předním	přední	k2eAgInSc7d1
vrchem	vrch	k1gInSc7
</s>
<s>
Sfinga	sfinga	k1gFnSc1
</s>
<s>
U	u	k7c2
Kunštátské	kunštátský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
</s>
<s>
Velká	velký	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
www.ochranaprirody.cz	www.ochranaprirody.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
21	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Stránky	stránka	k1gFnPc4
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Orlické	orlický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
.	.	kIx.
<g/>
NET	NET	kA
</s>
<s>
Orlické	orlický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Rychnov	Rychnov	k1gInSc1
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Orlické	orlický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bukačka	Bukačka	k1gFnSc1
•	•	k?
Trčkov	Trčkov	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bažiny	bažina	k1gFnPc1
•	•	k?
Bedřichovka	Bedřichovka	k1gFnSc1
•	•	k?
Černý	černý	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Hořečky	Hořečka	k1gFnSc2
•	•	k?
Hraniční	hraniční	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Jelení	jelení	k2eAgFnSc1d1
lázeň	lázeň	k1gFnSc1
•	•	k?
Kačerov	Kačerov	k1gInSc1
•	•	k?
Komáří	komáří	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Kostelecký	kostelecký	k2eAgInSc1d1
zámecký	zámecký	k2eAgInSc1d1
park	park	k1gInSc1
•	•	k?
Modlivý	modlivý	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Neratovské	Neratovský	k2eAgFnPc4d1
louky	louka	k1gFnPc4
•	•	k?
Pod	pod	k7c4
Vrchmezím	Vrchmeze	k1gFnPc3
•	•	k?
Pod	pod	k7c7
Zakletým	zakletý	k2eAgInSc7d1
•	•	k?
Sedloňovský	Sedloňovský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Skalecký	Skalecký	k2eAgInSc1d1
háj	háj	k1gInSc1
•	•	k?
Trčkovská	Trčkovský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
U	u	k7c2
Houkvice	Houkvice	k1gFnSc2
•	•	k?
Ve	v	k7c6
slatinské	slatinský	k2eAgFnSc6d1
stráni	stráň	k1gFnSc6
•	•	k?
Zámělský	Zámělský	k2eAgInSc1d1
borek	borek	k1gInSc1
•	•	k?
Zbytka	Zbytka	k1gFnSc1
•	•	k?
Zemská	zemský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Broumarské	Broumarský	k2eAgFnPc1d1
slatiny	slatina	k1gFnPc1
•	•	k?
Dědina	dědina	k1gFnSc1
u	u	k7c2
Dobrušky	Dobruška	k1gFnSc2
•	•	k?
Halín	Halín	k1gMnSc1
•	•	k?
Kačenčina	Kačenčin	k2eAgFnSc1d1
zahrádka	zahrádka	k1gFnSc1
•	•	k?
Kačerov	Kačerov	k1gInSc1
•	•	k?
Na	na	k7c6
Hadovně	Hadovna	k1gFnSc6
•	•	k?
Opočno	Opočno	k1gNnSc1
•	•	k?
Orlice	Orlice	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
pod	pod	k7c7
Pětirozcestím	Pětirozcestí	k1gNnSc7
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc4
pod	pod	k7c7
Předním	přední	k2eAgInSc7d1
vrchem	vrch	k1gInSc7
•	•	k?
Rybník	rybník	k1gInSc1
Spáleniště	spáleniště	k1gNnSc1
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Sfinga	sfinga	k1gFnSc1
•	•	k?
Tuří	tuří	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Týnišťské	týnišťský	k2eAgFnSc2d1
Poorličí	Poorličí	k2eAgFnSc2d1
•	•	k?
U	u	k7c2
Černoblatské	Černoblatský	k2eAgFnSc2d1
louky	louka	k1gFnSc2
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
U	u	k7c2
Čtvrtečkova	Čtvrtečkův	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
•	•	k?
U	u	k7c2
Glorietu	gloriet	k1gInSc2
•	•	k?
U	u	k7c2
Kunštátské	kunštátský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
•	•	k?
Uhřínov-Benátky	Uhřínov-Benátka	k1gFnSc2
•	•	k?
Velká	velký	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Vodní	vodní	k2eAgFnSc4d1
tůň	tůň	k1gFnSc4
•	•	k?
Zadní	zadní	k2eAgFnSc1d1
Machová	Machová	k1gFnSc1
Ptačí	ptačí	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Orlické	orlický	k2eAgNnSc1d1
Záhoří	Záhoří	k1gNnSc1
Evropsky	evropsky	k6eAd1
významné	významný	k2eAgFnPc4d1
lokality	lokalita	k1gFnPc4
</s>
<s>
Bažiny	bažina	k1gFnPc1
•	•	k?
Častolovice	Častolovice	k1gFnSc1
-	-	kIx~
zámek	zámek	k1gInSc1
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Dědina	dědina	k1gFnSc1
u	u	k7c2
Dobrušky	Dobruška	k1gFnSc2
•	•	k?
Divoká	divoký	k2eAgFnSc1d1
Orlice	Orlice	k1gFnSc1
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Halín	Halín	k1gInSc1
•	•	k?
Kačerov	Kačerov	k1gInSc1
•	•	k?
Litice	Litice	k1gFnSc2
•	•	k?
Opočno	Opočno	k1gNnSc1
•	•	k?
Orlice	Orlice	k1gFnSc2
a	a	k8xC
Labe	Labe	k1gNnSc2
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
-	-	kIx~
sever	sever	k1gInSc1
•	•	k?
Panský	panský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Rybník	rybník	k1gInSc1
Spáleniště	spáleniště	k1gNnSc2
•	•	k?
Štola	štola	k1gFnSc1
Portál	portál	k1gInSc1
•	•	k?
Trčkov	Trčkov	k1gInSc1
•	•	k?
Tuří	tuří	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Týnišťské	týnišťský	k2eAgFnPc4d1
Poorličí	Poorličí	k2eAgFnPc4d1
•	•	k?
Uhřínov-Benátky	Uhřínov-Benátka	k1gFnSc2
•	•	k?
Zadní	zadní	k2eAgFnSc1d1
Machová	Machová	k1gFnSc1
•	•	k?
Zámek	zámek	k1gInSc1
v	v	k7c6
Kostelci	Kostelec	k1gInSc6
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
•	•	k?
Zaorlicko	Zaorlicko	k1gNnSc4
•	•	k?
Zbytka	Zbytka	k1gFnSc1
•	•	k?
Zdobnice	Zdobnice	k1gFnSc1
-	-	kIx~
Říčka	říčka	k1gFnSc1
</s>
