<p>
<s>
Poike	Poike	k1gFnSc1	Poike
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
vyhaslých	vyhaslý	k2eAgFnPc2d1	vyhaslá
sopek	sopka	k1gFnPc2	sopka
na	na	k7c6	na
Velikonočním	velikonoční	k2eAgInSc6d1	velikonoční
ostrově	ostrov	k1gInSc6	ostrov
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
štítová	štítový	k2eAgFnSc1d1	štítová
sopka	sopka	k1gFnSc1	sopka
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
412	[number]	k4	412
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
<s>
Je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
(	(	kIx(	(
<g/>
po	po	k7c4	po
Maunga	Maung	k1gMnSc4	Maung
Terevaka	Terevak	k1gMnSc4	Terevak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnPc4	geografie
a	a	k8xC	a
vulkanologie	vulkanologie	k1gFnPc4	vulkanologie
==	==	k?	==
</s>
</p>
<p>
<s>
Těleso	těleso	k1gNnSc1	těleso
sopky	sopka	k1gFnSc2	sopka
Poike	Poik	k1gFnSc2	Poik
tvoří	tvořit	k5eAaImIp3nS	tvořit
východní	východní	k2eAgInSc1d1	východní
výběžek	výběžek	k1gInSc1	výběžek
Velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Sopka	sopka	k1gFnSc1	sopka
naposledy	naposledy	k6eAd1	naposledy
vybuchla	vybuchnout	k5eAaPmAgFnS	vybuchnout
</s>
</p>
<p>
<s>
před	před	k7c7	před
230	[number]	k4	230
000	[number]	k4	000
až	až	k9	až
705	[number]	k4	705
000	[number]	k4	000
roky	rok	k1gInPc7	rok
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgMnSc1d3	nejstarší
za	za	k7c2	za
tří	tři	k4xCgFnPc2	tři
sopek	sopka	k1gFnPc2	sopka
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
sopky	sopka	k1gFnSc2	sopka
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
zvětralý	zvětralý	k2eAgInSc1d1	zvětralý
<g/>
.	.	kIx.	.
</s>
<s>
Rozhraní	rozhraní	k1gNnSc1	rozhraní
mezi	mezi	k7c7	mezi
sopkami	sopka	k1gFnPc7	sopka
Poike	Poik	k1gMnSc2	Poik
a	a	k8xC	a
Maunga	Maung	k1gMnSc2	Maung
Terevaka	Terevak	k1gMnSc2	Terevak
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3,5	[number]	k4	3,5
kilometry	kilometr	k1gInPc7	kilometr
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
<g/>
,	,	kIx,	,
příkrým	příkrý	k2eAgInSc7d1	příkrý
příkopem	příkop	k1gInSc7	příkop
<g/>
,	,	kIx,	,
zvaným	zvaný	k2eAgMnSc7d1	zvaný
Ko	Ko	k1gMnSc7	Ko
te	te	k?	te
Ava	Ava	k1gMnSc7	Ava
o	o	k7c4	o
Iko	Iko	k1gFnSc4	Iko
čili	čili	k8xC	čili
Ikův	Ikův	k1gInSc4	Ikův
příkop	příkop	k1gInSc1	příkop
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
hlavního	hlavní	k2eAgInSc2d1	hlavní
vrcholu	vrchol	k1gInSc2	vrchol
Maunga	Maung	k1gMnSc2	Maung
Pu	Pu	k1gMnSc2	Pu
A	a	k9	a
Katiki	Katiki	k1gNnPc7	Katiki
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
kráter	kráter	k1gInSc1	kráter
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
150	[number]	k4	150
m	m	kA	m
a	a	k8xC	a
hloubce	hloubka	k1gFnSc6	hloubka
cca	cca	kA	cca
15	[number]	k4	15
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
úbočí	úbočí	k1gNnPc4	úbočí
sopky	sopka	k1gFnSc2	sopka
několik	několik	k4yIc1	několik
bočních	boční	k2eAgInPc2d1	boční
pyroklastických	pyroklastický	k2eAgInPc2d1	pyroklastický
kuželů	kužel	k1gInPc2	kužel
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
značně	značně	k6eAd1	značně
zerodovaných	zerodovaný	k2eAgMnPc2d1	zerodovaný
<g/>
:	:	kIx,	:
Maunga	Maung	k1gMnSc2	Maung
Vai	Vai	k1gMnSc2	Vai
a	a	k8xC	a
Heva	Hevus	k1gMnSc2	Hevus
<g/>
,	,	kIx,	,
Maunga	Maung	k1gMnSc2	Maung
Parehe	Pareh	k1gMnSc2	Pareh
či	či	k8xC	či
Maunga	Maung	k1gMnSc2	Maung
Tea	Tea	k1gFnSc1	Tea
Tea	Tea	k1gFnSc1	Tea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Poike	Poike	k1gNnSc2	Poike
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
Roggeveenův	Roggeveenův	k2eAgInSc1d1	Roggeveenův
mys	mys	k1gInSc1	mys
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
Holanďanovi	Holanďan	k1gMnSc6	Holanďan
Jacobu	Jacoba	k1gFnSc4	Jacoba
Roggeveenovi	Roggeveen	k1gMnSc3	Roggeveen
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
poprvé	poprvé	k6eAd1	poprvé
navštívil	navštívit	k5eAaPmAgMnS	navštívit
ostrov	ostrov	k1gInSc4	ostrov
o	o	k7c6	o
velikonocích	velikonoce	k1gFnPc6	velikonoce
roku	rok	k1gInSc2	rok
1722	[number]	k4	1722
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýchodnější	východní	k2eAgFnSc1d3	nejvýchodnější
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
mysem	mys	k1gInSc7	mys
Cumming	Cumming	k1gInSc1	Cumming
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Higginsův	Higginsův	k2eAgInSc4d1	Higginsův
mys	mys	k1gInSc4	mys
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
chilské	chilský	k2eAgFnSc6d1	chilská
korvetě	korveta	k1gFnSc6	korveta
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Higgins	Higgins	k1gInSc4	Higgins
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
chilská	chilský	k2eAgFnSc1d1	chilská
válečná	válečný	k2eAgFnSc1d1	válečná
loď	loď	k1gFnSc1	loď
přistála	přistát	k5eAaImAgFnS	přistát
u	u	k7c2	u
ostrova	ostrov	k1gInSc2	ostrov
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Eroze	eroze	k1gFnSc1	eroze
půdy	půda	k1gFnSc2	půda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
nejvýchodnější	východní	k2eAgFnSc6d3	nejvýchodnější
části	část	k1gFnSc6	část
sopky	sopka	k1gFnSc2	sopka
Poike	Poik	k1gFnSc2	Poik
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
projevil	projevit	k5eAaPmAgInS	projevit
katastrofický	katastrofický	k2eAgInSc1d1	katastrofický
dopad	dopad	k1gInSc1	dopad
</s>
</p>
<p>
<s>
odlesnění	odlesnění	k1gNnSc4	odlesnění
ostrova	ostrov	k1gInSc2	ostrov
domorodým	domorodý	k2eAgNnSc7d1	domorodé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
ostrova	ostrov	k1gInSc2	ostrov
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
odlesnění	odlesnění	k1gNnSc6	odlesnění
ostrova	ostrov	k1gInSc2	ostrov
k	k	k7c3	k
rychlé	rychlý	k2eAgFnSc3d1	rychlá
erozi	eroze	k1gFnSc3	eroze
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
odvátí	odvátí	k1gNnSc2	odvátí
zeminy	zemina	k1gFnSc2	zemina
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
osídlení	osídlení	k1gNnSc2	osídlení
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
300	[number]	k4	300
až	až	k8xS	až
600	[number]	k4	600
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
až	až	k9	až
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1280	[number]	k4	1280
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
sopky	sopka	k1gFnSc2	sopka
Poike	Poike	k1gFnPc2	Poike
využíváno	využívat	k5eAaImNgNnS	využívat
tradičním	tradiční	k2eAgInSc7d1	tradiční
polynéským	polynéský	k2eAgInSc7d1	polynéský
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
eroze	eroze	k1gFnSc2	eroze
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vykácení	vykácení	k1gNnSc3	vykácení
dominantního	dominantní	k2eAgInSc2d1	dominantní
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
druhu	druh	k1gInSc2	druh
-	-	kIx~	-
palmy	palma	k1gFnSc2	palma
Jubaea	Jubae	k1gInSc2	Jubae
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc6	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
silné	silný	k2eAgFnSc3d1	silná
půdní	půdní	k2eAgFnSc3d1	půdní
erozi	eroze	k1gFnSc3	eroze
a	a	k8xC	a
odplavování	odplavování	k1gNnSc4	odplavování
dešťovou	dešťový	k2eAgFnSc7d1	dešťová
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
8,6	[number]	k4	8,6
tun	tuna	k1gFnPc2	tuna
zeminy	zemina	k1gFnSc2	zemina
z	z	k7c2	z
hektaru	hektar	k1gInSc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
Poike	Poik	k1gFnSc2	Poik
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
hektarů	hektar	k1gInPc2	hektar
povrchu	povrch	k1gInSc2	povrch
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ana	Ana	k1gMnPc4	Ana
O	o	k7c6	o
Keke	Keke	k1gFnSc6	Keke
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
areálu	areál	k1gInSc2	areál
sopky	sopka	k1gFnSc2	sopka
Poike	Poik	k1gInSc2	Poik
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
380	[number]	k4	380
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
jeskyně	jeskyně	k1gFnSc1	jeskyně
Ana	Ana	k1gFnSc2	Ana
O	o	k7c6	o
Keke	Keke	k1gFnSc6	Keke
(	(	kIx(	(
<g/>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
panen	panna	k1gFnPc2	panna
<g/>
)	)	kIx)	)
s	s	k7c7	s
petroglyfy	petroglyf	k1gInPc7	petroglyf
domorodého	domorodý	k2eAgNnSc2d1	domorodé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
mořských	mořský	k2eAgInPc2d1	mořský
útesů	útes	k1gInPc2	útes
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
dosažení	dosažení	k1gNnSc3	dosažení
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
útesy	útes	k1gInPc4	útes
sestoupit	sestoupit	k5eAaPmF	sestoupit
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
domorodé	domorodý	k2eAgFnSc6d1	domorodá
kultuře	kultura	k1gFnSc6	kultura
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
kultu	kult	k1gInSc3	kult
Ptačího	ptačí	k2eAgMnSc2d1	ptačí
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
dívky	dívka	k1gFnPc1	dívka
<g/>
,	,	kIx,	,
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnSc4	vítěz
závodu	závod	k1gInSc2	závod
Ptačího	ptačí	k2eAgMnSc2d1	ptačí
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
žily	žít	k5eAaImAgFnP	žít
šest	šest	k4xCc1	šest
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
rituálně	rituálně	k6eAd1	rituálně
očistily	očistit	k5eAaPmAgInP	očistit
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
položka	položka	k1gFnSc1	položka
vybledla	vyblednout	k5eAaPmAgFnS	vyblednout
nedostatkem	nedostatek	k1gInSc7	nedostatek
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Katherine	Katherin	k1gInSc5	Katherin
Routledge	Routledge	k1gFnSc1	Routledge
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Mystery	Myster	k1gInPc1	Myster
of	of	k?	of
Easter	Easter	k1gInSc1	Easter
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
story	story	k1gFnSc1	story
of	of	k?	of
an	an	k?	an
expedition	expedition	k1gInSc1	expedition
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Rano	Rana	k1gFnSc5	Rana
Kau	Kau	k1gFnSc5	Kau
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Poike	Poik	k1gFnSc2	Poik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Guide	Guide	k6eAd1	Guide
to	ten	k3xDgNnSc4	ten
Easter	Easter	k1gMnSc1	Easter
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
