<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
335	[number]	k4	335
zemřel	zemřít	k5eAaPmAgMnS	zemřít
papež	papež	k1gMnSc1	papež
Silvestr	Silvestr	k1gMnSc1	Silvestr
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
připadají	připadat	k5eAaPmIp3nP	připadat
silvestrovské	silvestrovský	k2eAgFnPc1d1	silvestrovská
oslavy	oslava	k1gFnPc1	oslava
<g/>
.	.	kIx.	.
</s>
