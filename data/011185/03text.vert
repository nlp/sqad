<p>
<s>
==	==	k?	==
Atmosféra	atmosféra	k1gFnSc1	atmosféra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Teplota	teplota	k1gFnSc1	teplota
===	===	k?	===
</s>
</p>
<p>
<s>
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
hřeben	hřeben	k1gInSc4	hřeben
Východoantarktické	Východoantarktický	k2eAgFnSc2d1	Východoantarktický
plošiny	plošina	k1gFnSc2	plošina
(	(	kIx(	(
<g/>
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
mínus	mínus	k1gInSc1	mínus
93,2	[number]	k4	93,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
měsíční	měsíční	k2eAgFnSc1d1	měsíční
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
Vostok	Vostok	k1gInSc1	Vostok
(	(	kIx(	(
<g/>
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
mínus	mínus	k1gInSc1	mínus
72,0	[number]	k4	72,0
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
observatoř	observatoř	k1gFnSc1	observatoř
Plateau	Plateaus	k1gInSc2	Plateaus
(	(	kIx(	(
<g/>
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
mínus	mínus	k1gInSc1	mínus
56,4	[number]	k4	56,4
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
Údolí	údolí	k1gNnSc1	údolí
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
–	–	k?	–
plus	plus	k1gInSc1	plus
56,7	[number]	k4	56,7
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1913	[number]	k4	1913
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
měsíční	měsíční	k2eAgFnSc1d1	měsíční
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
Údolí	údolí	k1gNnSc1	údolí
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
plus	plus	k1gInSc1	plus
48,9	[number]	k4	48,9
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
Dalol	Dalol	k1gInSc1	Dalol
(	(	kIx(	(
<g/>
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
plus	plus	k1gInSc1	plus
34,4	[number]	k4	34,4
stupňů	stupeň	k1gInPc2	stupeň
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
absolutní	absolutní	k2eAgInSc1d1	absolutní
výkyv	výkyv	k1gInSc1	výkyv
teploty	teplota	k1gFnSc2	teplota
–	–	k?	–
Ojmjakon	Ojmjakon	k1gInSc1	Ojmjakon
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výkyv	výkyv	k1gInSc4	výkyv
od	od	k7c2	od
mínus	mínus	k6eAd1	mínus
71,2	[number]	k4	71,2
do	do	k7c2	do
plus	plus	k6eAd1	plus
35,0	[number]	k4	35,0
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
Litvínovice	Litvínovice	k1gFnSc1	Litvínovice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
-	-	kIx~	-
mínus	mínus	k1gInSc1	mínus
42,2	[number]	k4	42,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1929	[number]	k4	1929
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
Dobřichovice	Dobřichovice	k1gFnPc1	Dobřichovice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Praha-západ	Prahaápad	k1gInSc1	Praha-západ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
plus	plus	k1gInSc1	plus
40,4	[number]	k4	40,4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
Sněžka	Sněžka	k1gFnSc1	Sněžka
(	(	kIx(	(
<g/>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
-	-	kIx~	-
mínus	mínus	k1gInSc1	mínus
0,4	[number]	k4	0,4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
Klementinum	Klementinum	k1gNnSc1	Klementinum
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
-	-	kIx~	-
plus	plus	k1gInSc1	plus
12,5	[number]	k4	12,5
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
a	a	k8xC	a
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
===	===	k?	===
Srážky	srážka	k1gFnPc1	srážka
===	===	k?	===
</s>
</p>
<p>
<s>
nejdeštivější	deštivý	k2eAgNnSc1d3	nejdeštivější
místo	místo	k1gNnSc1	místo
–	–	k?	–
Kauai	Kauai	k1gNnSc1	Kauai
(	(	kIx(	(
<g/>
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc4d1	roční
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
12	[number]	k4	12
090	[number]	k4	090
mm	mm	kA	mm
</s>
</p>
<p>
<s>
nejsušší	suchý	k2eAgNnSc4d3	nejsušší
místo	místo	k1gNnSc4	místo
–	–	k?	–
Asuán	Asuán	k1gInSc1	Asuán
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc4d1	roční
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
0,5	[number]	k4	0,5
mm	mm	kA	mm
</s>
</p>
<p>
<s>
nejzasněženější	zasněžený	k2eAgNnSc1d3	zasněžený
místo	místo	k1gNnSc1	místo
–	–	k?	–
Mount	Mount	k1gMnSc1	Mount
Rainier	Rainier	k1gMnSc1	Rainier
(	(	kIx(	(
<g/>
Washington	Washington	k1gInSc1	Washington
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnSc3d1	průměrná
roční	roční	k2eAgFnPc4d1	roční
sněhové	sněhový	k2eAgFnPc4d1	sněhová
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
3110	[number]	k4	3110
cmnejvyšší	cmnejvyššit	k5eAaPmIp3nP	cmnejvyššit
srážky	srážka	k1gFnPc1	srážka
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
–	–	k?	–
Réunion	Réunion	k1gInSc1	Réunion
(	(	kIx(	(
<g/>
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
1870	[number]	k4	1870
mm	mm	kA	mm
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
srážky	srážka	k1gFnPc1	srážka
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
–	–	k?	–
Čérápuňdží	Čérápuňdž	k1gFnPc2	Čérápuňdž
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
9299	[number]	k4	9299
mm	mm	kA	mm
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc4	datum
–	–	k?	–
červenec	červenec	k1gInSc1	červenec
1861	[number]	k4	1861
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
srážky	srážka	k1gFnPc1	srážka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
–	–	k?	–
Čérápuňdží	Čérápuňdž	k1gFnPc2	Čérápuňdž
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
26	[number]	k4	26
461	[number]	k4	461
mm	mm	kA	mm
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
–	–	k?	–
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
</s>
</p>
<p>
<s>
nejprudší	prudký	k2eAgInSc1d3	nejprudší
déšť	déšť	k1gInSc1	déšť
–	–	k?	–
Guadeloupe	Guadeloupe	k1gFnPc1	Guadeloupe
(	(	kIx(	(
<g/>
Malé	Malé	k2eAgFnPc1d1	Malé
Antily	Antily	k1gFnPc1	Antily
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
38,1	[number]	k4	38,1
mm	mm	kA	mm
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
minutu	minuta	k1gFnSc4	minuta
</s>
</p>
<p>
<s>
nejnižší	nízký	k2eAgFnPc1d3	nejnižší
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
Libědice	Libědice	k1gFnSc1	Libědice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
410	[number]	k4	410
mm	mm	kA	mm
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
U	u	k7c2	u
Studánky	studánka	k1gFnSc2	studánka
(	(	kIx(	(
<g/>
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
1	[number]	k4	1
705	[number]	k4	705
mm	mm	kA	mm
</s>
</p>
<p>
<s>
nejmenší	malý	k2eAgInSc1d3	nejmenší
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
Velké	velký	k2eAgNnSc1d1	velké
Přítočno	Přítočno	k1gNnSc1	Přítočno
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
)	)	kIx)	)
a	a	k8xC	a
Písky	Písek	k1gInPc1	Písek
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Rakovník	Rakovník	k1gInSc1	Rakovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
–	–	k?	–
247	[number]	k4	247
mm	mm	kA	mm
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
a	a	k8xC	a
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
Kořenov	Kořenov	k1gInSc1	Kořenov
(	(	kIx(	(
<g/>
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
–	–	k?	–
2202	[number]	k4	2202
mm	mm	kA	mm
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
Beskydy	Beskydy	k1gFnPc1	Beskydy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
491	[number]	k4	491
cm	cm	kA	cm
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc4	datum
–	–	k?	–
březen	březen	k1gInSc1	březen
1911	[number]	k4	1911
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
===	===	k?	===
Vítr	vítr	k1gInSc1	vítr
===	===	k?	===
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
–	–	k?	–
Mount	Mount	k1gInSc1	Mount
Washington	Washington	k1gInSc1	Washington
(	(	kIx(	(
<g/>
Oregon	Oregon	k1gInSc1	Oregon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
–	–	k?	–
416	[number]	k4	416
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
při	při	k7c6	při
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejrychleji	rychle	k6eAd3	rychle
rotující	rotující	k2eAgNnSc1d1	rotující
tornádo	tornádo	k1gNnSc1	tornádo
–	–	k?	–
(	(	kIx(	(
<g/>
Oklahoma	Oklahoma	k1gNnSc1	Oklahoma
City	city	k1gNnSc1	city
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
–	–	k?	–
484	[number]	k4	484
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
32	[number]	k4	32
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
datum	datum	k1gInSc1	datum
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
tornádo	tornádo	k1gNnSc4	tornádo
–	–	k?	–
Tri-State	Tri-Stat	k1gInSc5	Tri-Stat
Tornado	Tornada	k1gFnSc5	Tornada
(	(	kIx(	(
<g/>
Missouri	Missouri	k1gFnSc1	Missouri
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc1	Illinois
<g/>
,	,	kIx,	,
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dráha	dráha	k1gFnSc1	dráha
–	–	k?	–
352	[number]	k4	352
km	km	kA	km
<g/>
,	,	kIx,	,
trvání	trvání	k1gNnSc2	trvání
–	–	k?	–
3,5	[number]	k4	3,5
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
–	–	k?	–
117	[number]	k4	117
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
–	–	k?	–
F	F	kA	F
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
datum	datum	k1gInSc1	datum
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1925	[number]	k4	1925
</s>
</p>
<p>
<s>
nejsmrtelnější	smrtelní	k2eAgNnSc1d3	smrtelní
tornádo	tornádo	k1gNnSc1	tornádo
–	–	k?	–
Daulatpur	Daulatpura	k1gFnPc2	Daulatpura
-	-	kIx~	-
Saturia	Saturium	k1gNnSc2	Saturium
(	(	kIx(	(
<g/>
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
–	–	k?	–
1300	[number]	k4	1300
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
zraněných	zraněný	k1gMnPc2	zraněný
–	–	k?	–
12	[number]	k4	12
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
bez	bez	k7c2	bez
domova	domov	k1gInSc2	domov
–	–	k?	–
80	[number]	k4	80
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
dráhy	dráha	k1gFnSc2	dráha
–	–	k?	–
16	[number]	k4	16
km	km	kA	km
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
zasaženého	zasažený	k2eAgNnSc2d1	zasažené
území	území	k1gNnSc2	území
–	–	k?	–
6	[number]	k4	6
km2	km2	k4	km2
<g/>
,	,	kIx,	,
datum	datum	k1gInSc1	datum
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
výskyt	výskyt	k1gInSc1	výskyt
tornád	tornádo	k1gNnPc2	tornádo
–	–	k?	–
Super	super	k2eAgInSc1d1	super
Outbreak	Outbreak	k1gInSc1	Outbreak
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počet	počet	k1gInSc4	počet
tornád	tornádo	k1gNnPc2	tornádo
–	–	k?	–
148	[number]	k4	148
(	(	kIx(	(
<g/>
7	[number]	k4	7
tornád	tornádo	k1gNnPc2	tornádo
síly	síla	k1gFnSc2	síla
F	F	kA	F
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
23	[number]	k4	23
tornád	tornádo	k1gNnPc2	tornádo
síly	síla	k1gFnSc2	síla
F	F	kA	F
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
34	[number]	k4	34
tornád	tornádo	k1gNnPc2	tornádo
síly	síla	k1gFnSc2	síla
F	F	kA	F
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
32	[number]	k4	32
tornád	tornádo	k1gNnPc2	tornádo
síly	síla	k1gFnSc2	síla
F	F	kA	F
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
33	[number]	k4	33
tornád	tornádo	k1gNnPc2	tornádo
síly	síla	k1gFnSc2	síla
F1	F1	k1gFnSc2	F1
a	a	k8xC	a
19	[number]	k4	19
tornád	tornádo	k1gNnPc2	tornádo
síly	síla	k1gFnSc2	síla
F	F	kA	F
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
datum	datum	k1gInSc1	datum
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
Sněžka	Sněžka	k1gFnSc1	Sněžka
(	(	kIx(	(
<g/>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
-	-	kIx~	-
216	[number]	k4	216
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
===	===	k?	===
Záření	záření	k1gNnSc2	záření
===	===	k?	===
</s>
</p>
<p>
<s>
nejkratší	krátký	k2eAgNnSc1d3	nejkratší
období	období	k1gNnSc1	období
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
–	–	k?	–
Severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
slunečních	sluneční	k2eAgInPc2d1	sluneční
dní	den	k1gInPc2	den
–	–	k?	–
197	[number]	k4	197
</s>
</p>
<p>
<s>
==	==	k?	==
Pevnina	pevnina	k1gFnSc1	pevnina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kontinenty	kontinent	k1gInPc1	kontinent
===	===	k?	===
</s>
</p>
<p>
<s>
nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
světadíl	světadíl	k1gInSc1	světadíl
–	–	k?	–
Asie	Asie	k1gFnSc2	Asie
–	–	k?	–
3	[number]	k4	3
902	[number]	k4	902
404	[number]	k4	404
193	[number]	k4	193
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejrozlehlejší	rozlehlý	k2eAgInSc1d3	nejrozlehlejší
světadíl	světadíl	k1gInSc1	světadíl
–	–	k?	–
Asie	Asie	k1gFnSc2	Asie
–	–	k?	–
43	[number]	k4	43
810	[number]	k4	810
582	[number]	k4	582
km2	km2	k4	km2
</s>
</p>
<p>
<s>
nejvzdálenější	vzdálený	k2eAgNnSc1d3	nejvzdálenější
místo	místo	k1gNnSc1	místo
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
–	–	k?	–
Džungarská	Džungarský	k2eAgFnSc1d1	Džungarská
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
/	/	kIx~	/
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
–	–	k?	–
2650	[number]	k4	2650
km	km	kA	km
</s>
</p>
<p>
<s>
nejsevernější	severní	k2eAgNnSc1d3	nejsevernější
místo	místo	k1gNnSc1	místo
pevniny	pevnina	k1gFnSc2	pevnina
–	–	k?	–
ostrov	ostrov	k1gInSc1	ostrov
Kaffeklubben	Kaffeklubbna	k1gFnPc2	Kaffeklubbna
(	(	kIx(	(
<g/>
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
,	,	kIx,	,
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souřadnice	souřadnice	k1gFnSc1	souřadnice
–	–	k?	–
83	[number]	k4	83
<g/>
°	°	k?	°
40	[number]	k4	40
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
29	[number]	k4	29
<g/>
°	°	k?	°
50	[number]	k4	50
<g/>
'	'	kIx"	'
z.	z.	k?	z.
d	d	k?	d
</s>
</p>
<p>
<s>
nejjižnější	jižní	k2eAgNnSc1d3	nejjižnější
místo	místo	k1gNnSc1	místo
pevniny	pevnina	k1gFnSc2	pevnina
–	–	k?	–
Jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
</s>
</p>
<p>
<s>
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
-	-	kIx~	-
Šluknovský	šluknovský	k2eAgInSc1d1	šluknovský
výběžek	výběžek	k1gInSc1	výběžek
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
-	-	kIx~	-
290	[number]	k4	290
km	km	kA	km
</s>
</p>
<p>
<s>
nejsevernější	severní	k2eAgNnSc4d3	nejsevernější
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Lobendava	Lobendava	k1gFnSc1	Lobendava
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souřadnice	souřadnice	k1gFnSc1	souřadnice
-	-	kIx~	-
51	[number]	k4	51
<g/>
°	°	k?	°
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
22	[number]	k4	22
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
</s>
</p>
<p>
<s>
nejjižnější	jižní	k2eAgNnSc4d3	nejjižnější
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souřadnice	souřadnice	k1gFnSc1	souřadnice
-	-	kIx~	-
48	[number]	k4	48
<g/>
°	°	k?	°
33	[number]	k4	33
<g/>
'	'	kIx"	'
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
</s>
</p>
<p>
<s>
nejzápadnější	západní	k2eAgNnSc4d3	nejzápadnější
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Krásná	krásný	k2eAgFnSc1d1	krásná
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Cheb	Cheb	k1gInSc1	Cheb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souřadnice	souřadnice	k1gFnSc1	souřadnice
-	-	kIx~	-
12	[number]	k4	12
<g/>
°	°	k?	°
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
'	'	kIx"	'
33	[number]	k4	33
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
</s>
</p>
<p>
<s>
nejvýchodnější	východní	k2eAgNnSc4d3	nejvýchodnější
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Bukovec	Bukovec	k1gInSc1	Bukovec
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souřadnice	souřadnice	k1gFnSc1	souřadnice
-	-	kIx~	-
18	[number]	k4	18
<g/>
°	°	k?	°
51	[number]	k4	51
<g/>
'	'	kIx"	'
40	[number]	k4	40
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
</s>
</p>
<p>
<s>
===	===	k?	===
Jeskyně	jeskyně	k1gFnSc2	jeskyně
===	===	k?	===
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc4d3	veliký
jeskynní	jeskynní	k2eAgInSc4d1	jeskynní
systém	systém	k1gInSc4	systém
–	–	k?	–
Mamutí	mamutí	k2eAgFnSc2d1	mamutí
jeskyně	jeskyně	k1gFnSc2	jeskyně
(	(	kIx(	(
<g/>
Kentucky	Kentucka	k1gFnSc2	Kentucka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
systému	systém	k1gInSc2	systém
579	[number]	k4	579
364	[number]	k4	364
m	m	kA	m
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
sádrovcová	sádrovcový	k2eAgFnSc1d1	sádrovcová
jeskyně	jeskyně	k1gFnSc1	jeskyně
–	–	k?	–
Optymistyčna	Optymistyčno	k1gNnSc2	Optymistyčno
(	(	kIx(	(
<g/>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
214	[number]	k4	214
km	km	kA	km
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
zatopená	zatopený	k2eAgFnSc1d1	zatopená
jeskyně	jeskyně	k1gFnSc1	jeskyně
–	–	k?	–
Nohoch	Nohoch	k1gInSc1	Nohoch
Nah	naho	k1gNnPc2	naho
Chich	Chich	k1gInSc1	Chich
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
61	[number]	k4	61
143	[number]	k4	143
m	m	kA	m
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
solná	solný	k2eAgFnSc1d1	solná
jeskyně	jeskyně	k1gFnSc1	jeskyně
–	–	k?	–
jeskyně	jeskyně	k1gFnSc2	jeskyně
Tří	tři	k4xCgMnPc2	tři
naháčů	naháč	k1gMnPc2	naháč
(	(	kIx(	(
<g/>
Írán	Írán	k1gInSc1	Írán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
nejméně	málo	k6eAd3	málo
6500	[number]	k4	6500
m	m	kA	m
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
stalaktit	stalaktit	k1gInSc1	stalaktit
–	–	k?	–
jeskyně	jeskyně	k1gFnSc2	jeskyně
Cueva	Cuev	k1gMnSc2	Cuev
de	de	k?	de
Nerja	Nerjus	k1gMnSc2	Nerjus
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
59	[number]	k4	59
m	m	kA	m
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
stalagnát	stalagnát	k1gInSc1	stalagnát
–	–	k?	–
jeskyně	jeskyně	k1gFnSc2	jeskyně
Daji	Daj	k1gFnSc2	Daj
Dong	dong	k1gInSc1	dong
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
39	[number]	k4	39
m	m	kA	m
</s>
</p>
<p>
<s>
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
jeskyně	jeskyně	k1gFnSc1	jeskyně
–	–	k?	–
Voronija	Voronija	k1gFnSc1	Voronija
(	(	kIx(	(
<g/>
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
–	–	k?	–
2140	[number]	k4	2140
(	(	kIx(	(
<g/>
±	±	k?	±
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
m	m	kA	m
</s>
</p>
<p>
<s>
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
propast	propast	k1gFnSc1	propast
–	–	k?	–
Gouffre	Gouffr	k1gInSc5	Gouffr
de	de	k?	de
la	la	k0	la
Pierre	Pierr	k1gInSc5	Pierr
St.	st.	kA	st.
Martin	Martin	k1gMnSc1	Martin
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
–	–	k?	–
1174	[number]	k4	1174
m	m	kA	m
</s>
</p>
<p>
<s>
největší	veliký	k2eAgFnSc1d3	veliký
jeskynní	jeskynní	k2eAgFnSc1d1	jeskynní
komora	komora	k1gFnSc1	komora
–	–	k?	–
Ogle	Ogle	k1gFnSc1	Ogle
Cave	Cave	k1gFnSc1	Cave
<g/>
/	/	kIx~	/
<g/>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
zamilovaných	zamilovaný	k2eAgInPc2d1	zamilovaný
pohledů	pohled	k1gInPc2	pohled
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
1300	[number]	k4	1300
m	m	kA	m
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
100	[number]	k4	100
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
–	–	k?	–
200	[number]	k4	200
m	m	kA	m
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
stalagmit	stalagmit	k1gInSc1	stalagmit
–	–	k?	–
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
jeskyně	jeskyně	k1gFnSc1	jeskyně
(	(	kIx(	(
<g/>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
kras	kras	k1gInSc1	kras
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
32,7	[number]	k4	32,7
m	m	kA	m
</s>
</p>
<p>
<s>
největší	veliký	k2eAgFnSc1d3	veliký
ledová	ledový	k2eAgFnSc1d1	ledová
jeskyně	jeskyně	k1gFnSc1	jeskyně
–	–	k?	–
Eisriesenwelt	Eisriesenwelt	k1gInSc1	Eisriesenwelt
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
prozkoumané	prozkoumaný	k2eAgFnSc2d1	prozkoumaná
části	část	k1gFnSc2	část
–	–	k?	–
42	[number]	k4	42
km	km	kA	km
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
česká	český	k2eAgFnSc1d1	Česká
jeskyně	jeskyně	k1gFnSc1	jeskyně
-	-	kIx~	-
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
(	(	kIx(	(
<g/>
CHKO	CHKO	kA	CHKO
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
-	-	kIx~	-
35	[number]	k4	35
km	km	kA	km
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
česká	český	k2eAgFnSc1d1	Česká
ledová	ledový	k2eAgFnSc1d1	ledová
jeskyně	jeskyně	k1gFnSc1	jeskyně
-	-	kIx~	-
Ledová	ledový	k2eAgFnSc1d1	ledová
jeskyně	jeskyně	k1gFnSc1	jeskyně
Naděje	naděje	k1gFnSc2	naděje
(	(	kIx(	(
<g/>
Lužické	lužický	k2eAgFnPc1d1	Lužická
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
-	-	kIx~	-
30	[number]	k4	30
m	m	kA	m
</s>
</p>
<p>
<s>
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
česká	český	k2eAgFnSc1d1	Česká
jeskyně	jeskyně	k1gFnSc1	jeskyně
-	-	kIx~	-
Býčí	býčí	k2eAgFnSc1d1	býčí
skála	skála	k1gFnSc1	skála
-	-	kIx~	-
Rudické	rudický	k2eAgNnSc1d1	Rudické
propadání	propadání	k1gNnSc1	propadání
(	(	kIx(	(
<g/>
CHKO	CHKO	kA	CHKO
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
-	-	kIx~	-
okolo	okolo	k7c2	okolo
200	[number]	k4	200
m	m	kA	m
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
český	český	k2eAgInSc1d1	český
podzemní	podzemní	k2eAgInSc1d1	podzemní
dóm	dóm	k1gInSc1	dóm
-	-	kIx~	-
Obří	obří	k2eAgInSc1d1	obří
dóm	dóm	k1gInSc1	dóm
(	(	kIx(	(
<g/>
Rudické	rudický	k2eAgNnSc1d1	Rudické
propadání	propadání	k1gNnSc1	propadání
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
-	-	kIx~	-
70	[number]	k4	70
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
-	-	kIx~	-
30	[number]	k4	30
m	m	kA	m
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
-	-	kIx~	-
60	[number]	k4	60
m	m	kA	m
</s>
</p>
<p>
<s>
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
česká	český	k2eAgFnSc1d1	Česká
propast	propast	k1gFnSc1	propast
-	-	kIx~	-
Hranická	hranický	k2eAgFnSc1d1	Hranická
propast	propast	k1gFnSc1	propast
(	(	kIx(	(
<g/>
Teplice	Teplice	k1gFnPc1	Teplice
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
(	(	kIx(	(
<g/>
doposud	doposud	k6eAd1	doposud
naměřená	naměřený	k2eAgFnSc1d1	naměřená
<g/>
)	)	kIx)	)
-	-	kIx~	-
442,5	[number]	k4	442,5
m	m	kA	m
<g/>
,	,	kIx,	,
odhad	odhad	k1gInSc1	odhad
700	[number]	k4	700
m	m	kA	m
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
český	český	k2eAgInSc1d1	český
podzemní	podzemní	k2eAgInSc1d1	podzemní
tok	tok	k1gInSc1	tok
-	-	kIx~	-
Punkva	Punkva	k1gFnSc1	Punkva
(	(	kIx(	(
<g/>
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
-	-	kIx~	-
10	[number]	k4	10
kmSeznam	kmSeznam	k6eAd1	kmSeznam
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
jeskyní	jeskyně	k1gFnPc2	jeskyně
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrovy	ostrov	k1gInPc1	ostrov
===	===	k?	===
</s>
</p>
<p>
<s>
nejmenší	malý	k2eAgInSc4d3	nejmenší
ostrov	ostrov	k1gInSc4	ostrov
–	–	k?	–
Bishop	Bishop	k1gInSc1	Bishop
Rock	rock	k1gInSc1	rock
(	(	kIx(	(
<g/>
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
/	/	kIx~	/
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celou	celý	k2eAgFnSc4d1	celá
plochu	plocha	k1gFnSc4	plocha
zabírá	zabírat	k5eAaImIp3nS	zabírat
maják	maják	k1gInSc1	maják
</s>
</p>
<p>
<s>
nejodlehlejší	odlehlý	k2eAgInSc1d3	nejodlehlejší
ostrov	ostrov	k1gInSc1	ostrov
–	–	k?	–
Velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
kontinentu	kontinent	k1gInSc2	kontinent
–	–	k?	–
3300	[number]	k4	3300
km	km	kA	km
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
korálový	korálový	k2eAgInSc1d1	korálový
ostrov	ostrov	k1gInSc1	ostrov
–	–	k?	–
Kiritimati	Kiritimati	k1gFnSc1	Kiritimati
(	(	kIx(	(
<g/>
Kiribati	Kiribati	k1gFnSc1	Kiribati
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
642	[number]	k4	642
km2	km2	k4	km2
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
souše	souš	k1gFnSc2	souš
–	–	k?	–
388	[number]	k4	388
km2	km2	k4	km2
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
korálový	korálový	k2eAgInSc1d1	korálový
útes	útes	k1gInSc1	útes
–	–	k?	–
Velký	velký	k2eAgInSc1d1	velký
bariérový	bariérový	k2eAgInSc1d1	bariérový
útes	útes	k1gInSc1	útes
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
2300	[number]	k4	2300
km	km	kA	km
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
–	–	k?	–
2	[number]	k4	2
až	až	k9	až
150	[number]	k4	150
km	km	kA	km
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
–	–	k?	–
Grónsko	Grónsko	k1gNnSc1	Grónsko
(	(	kIx(	(
<g/>
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
2	[number]	k4	2
130	[number]	k4	130
750	[number]	k4	750
km2	km2	k4	km2
</s>
</p>
<p>
<s>
největší	veliký	k2eAgNnSc1d3	veliký
souostroví	souostroví	k1gNnSc1	souostroví
–	–	k?	–
Malajské	malajský	k2eAgNnSc1d1	Malajské
souostroví	souostroví	k1gNnSc1	souostroví
(	(	kIx(	(
<g/>
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
km2	km2	k4	km2
největších	veliký	k2eAgInPc2d3	veliký
ostrovů	ostrov	k1gInPc2	ostrov
</s>
</p>
<p>
<s>
===	===	k?	===
Pohoří	pohoří	k1gNnSc1	pohoří
===	===	k?	===
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
–	–	k?	–
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
(	(	kIx(	(
<g/>
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
10	[number]	k4	10
205	[number]	k4	205
m	m	kA	m
nad	nad	k7c7	nad
Pacifickou	pacifický	k2eAgFnSc7d1	Pacifická
deskou	deska	k1gFnSc7	deska
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
–	–	k?	–
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
8848	[number]	k4	8848
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
pohoří	pohořet	k5eAaPmIp3nS	pohořet
–	–	k?	–
Kordillery	Kordillery	k1gFnPc4	Kordillery
<g/>
/	/	kIx~	/
<g/>
Andy	Anda	k1gFnPc4	Anda
(	(	kIx(	(
<g/>
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
16	[number]	k4	16
000	[number]	k4	000
km	km	kA	km
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
pohoří	pohoří	k1gNnSc4	pohoří
–	–	k?	–
Himálaj	Himálaj	k1gFnSc1	Himálaj
(	(	kIx(	(
<g/>
Asie	Asie	k1gFnSc1	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
2500	[number]	k4	2500
km	km	kA	km
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
–	–	k?	–
180	[number]	k4	180
až	až	k9	až
350	[number]	k4	350
km	km	kA	km
<g/>
,	,	kIx,	,
10	[number]	k4	10
vrcholů	vrchol	k1gInPc2	vrchol
přes	přes	k7c4	přes
8000	[number]	k4	8000
metrů	metr	k1gInPc2	metr
výšky	výška	k1gFnSc2	výška
</s>
</p>
<p>
<s>
nejvýše	nejvýše	k6eAd1	nejvýše
položený	položený	k2eAgInSc4d1	položený
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
–	–	k?	–
Chimborazo	Chimboraza	k1gFnSc5	Chimboraza
(	(	kIx(	(
<g/>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
Kordillery	Kordillery	k1gFnPc1	Kordillery
<g/>
)	)	kIx)	)
–	–	k?	–
6384,4	[number]	k4	6384,4
km	km	kA	km
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
česká	český	k2eAgFnSc1d1	Česká
hora	hora	k1gFnSc1	hora
-	-	kIx~	-
Sněžka	Sněžka	k1gFnSc1	Sněžka
(	(	kIx(	(
<g/>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
-	-	kIx~	-
1603	[number]	k4	1603
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
</s>
</p>
<p>
<s>
===	===	k?	===
Poloostrovy	poloostrov	k1gInPc4	poloostrov
===	===	k?	===
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
poloostrov	poloostrov	k1gInSc1	poloostrov
–	–	k?	–
Kalifornský	kalifornský	k2eAgInSc1d1	kalifornský
poloostrov	poloostrov	k1gInSc1	poloostrov
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
1250	[number]	k4	1250
km	km	kA	km
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
poloostrov	poloostrov	k1gInSc1	poloostrov
–	–	k?	–
Arabský	arabský	k2eAgInSc1d1	arabský
poloostrov	poloostrov	k1gInSc1	poloostrov
(	(	kIx(	(
<g/>
Asie	Asie	k1gFnSc2	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
2	[number]	k4	2
780	[number]	k4	780
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
===	===	k?	===
Pouště	poušť	k1gFnSc2	poušť
===	===	k?	===
</s>
</p>
<p>
<s>
největší	veliký	k2eAgFnSc1d3	veliký
poušť	poušť	k1gFnSc1	poušť
–	–	k?	–
Antarktická	antarktický	k2eAgFnSc1d1	antarktická
poušť	poušť	k1gFnSc1	poušť
(	(	kIx(	(
<g/>
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
14	[number]	k4	14
200	[number]	k4	200
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
===	===	k?	===
Sopky	sopka	k1gFnPc1	sopka
===	===	k?	===
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
činný	činný	k2eAgInSc1d1	činný
kráter	kráter	k1gInSc1	kráter
–	–	k?	–
sopka	sopka	k1gFnSc1	sopka
Kilauea	Kilauea	k1gFnSc1	Kilauea
(	(	kIx(	(
<g/>
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
–	–	k?	–
3,2	[number]	k4	3,2
km	km	kA	km
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
sopečný	sopečný	k2eAgInSc1d1	sopečný
výbuch	výbuch	k1gInSc1	výbuch
(	(	kIx(	(
<g/>
doložený	doložený	k2eAgInSc1d1	doložený
<g/>
)	)	kIx)	)
–	–	k?	–
sopka	sopka	k1gFnSc1	sopka
Krakatau	Krakataus	k1gInSc2	Krakataus
(	(	kIx(	(
<g/>
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc4	datum
výbuchu	výbuch	k1gInSc2	výbuch
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1883	[number]	k4	1883
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
pevninská	pevninský	k2eAgFnSc1d1	pevninská
sopka	sopka	k1gFnSc1	sopka
–	–	k?	–
Ojos	Ojos	k1gInSc1	Ojos
del	del	k?	del
Salado	Salada	k1gFnSc5	Salada
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
/	/	kIx~	/
<g/>
Chile	Chile	k1gNnSc1	Chile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
6893	[number]	k4	6893
m	m	kA	m
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
sopka	sopka	k1gFnSc1	sopka
–	–	k?	–
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
(	(	kIx(	(
<g/>
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
10	[number]	k4	10
205	[number]	k4	205
m	m	kA	m
(	(	kIx(	(
<g/>
6000	[number]	k4	6000
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc4	moře
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
česká	český	k2eAgFnSc1d1	Česká
sopka	sopka	k1gFnSc1	sopka
-	-	kIx~	-
Komorní	komorní	k2eAgFnSc1d1	komorní
hůrka	hůrka	k1gFnSc1	hůrka
(	(	kIx(	(
<g/>
Františkovy	Františkův	k2eAgFnPc1d1	Františkova
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Útvary	útvar	k1gInPc4	útvar
===	===	k?	===
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
pláž	pláž	k1gFnSc1	pláž
–	–	k?	–
Cox	Cox	k1gFnSc1	Cox
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bazar	bazar	k1gInSc1	bazar
(	(	kIx(	(
<g/>
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
120	[number]	k4	120
km	km	kA	km
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
zlom	zlom	k1gInSc1	zlom
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
–	–	k?	–
San	San	k1gMnSc1	San
Andreas	Andreas	k1gMnSc1	Andreas
(	(	kIx(	(
<g/>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
1050	[number]	k4	1050
km	km	kA	km
</s>
</p>
<p>
<s>
nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
pevniny	pevnina	k1gFnSc2	pevnina
–	–	k?	–
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
/	/	kIx~	/
<g/>
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
–	–	k?	–
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
–	–	k?	–
Duhový	duhový	k2eAgInSc1d1	duhový
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
Utah	Utah	k1gInSc1	Utah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
94	[number]	k4	94
m	m	kA	m
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
88	[number]	k4	88
m	m	kA	m
</s>
</p>
<p>
<s>
největší	veliký	k2eAgNnSc1d3	veliký
údolí	údolí	k1gNnSc1	údolí
–	–	k?	–
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
(	(	kIx(	(
<g/>
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
350	[number]	k4	350
km	km	kA	km
<g/>
,	,	kIx,	,
max	max	kA	max
<g/>
.	.	kIx.	.
hloubka	hloubka	k1gFnSc1	hloubka
–	–	k?	–
1800	[number]	k4	1800
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
200	[number]	k4	200
m	m	kA	m
až	až	k9	až
29	[number]	k4	29
km	km	kA	km
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
–	–	k?	–
Tibetská	tibetský	k2eAgFnSc1d1	tibetská
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
–	–	k?	–
4875	[number]	k4	4875
m	m	kA	m
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
skalní	skalní	k2eAgInSc1d1	skalní
oblouk	oblouk	k1gInSc1	oblouk
–	–	k?	–
Tushuk	Tushuk	k1gInSc1	Tushuk
Tash	Tash	k1gInSc1	Tash
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc2	rozpětí
–	–	k?	–
60	[number]	k4	60
m	m	kA	m
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
zcela	zcela	k6eAd1	zcela
svislá	svislý	k2eAgFnSc1d1	svislá
stěna	stěna	k1gFnSc1	stěna
–	–	k?	–
Mount	Mount	k1gInSc1	Mount
Thor	Thor	k1gInSc1	Thor
(	(	kIx(	(
<g/>
Baffinův	Baffinův	k2eAgInSc1d1	Baffinův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
1250	[number]	k4	1250
m	m	kA	m
</s>
</p>
<p>
<s>
největší	veliký	k2eAgFnSc1d3	veliký
ostrovní	ostrovní	k2eAgFnSc1d1	ostrovní
hora	hora	k1gFnSc1	hora
–	–	k?	–
Uluru	Ulur	k1gInSc2	Ulur
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
relativní	relativní	k2eAgFnSc1d1	relativní
výška	výška	k1gFnSc1	výška
–	–	k?	–
335	[number]	k4	335
m	m	kA	m
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
–	–	k?	–
9	[number]	k4	9
km	km	kA	km
</s>
</p>
<p>
<s>
nejrozsáhlejší	rozsáhlý	k2eAgFnPc1d3	nejrozsáhlejší
travertinové	travertinový	k2eAgFnPc1d1	travertinová
terasy	terasa	k1gFnPc1	terasa
–	–	k?	–
Pamukkale	Pamukkal	k1gMnSc5	Pamukkal
(	(	kIx(	(
<g/>
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
2500	[number]	k4	2500
m	m	kA	m
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
výška	výška	k1gFnSc1	výška
–	–	k?	–
150	[number]	k4	150
m	m	kA	m
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
impaktní	impaktní	k2eAgInSc1d1	impaktní
kráter	kráter	k1gInSc1	kráter
–	–	k?	–
Vredefort	Vredefort	k1gInSc1	Vredefort
(	(	kIx(	(
<g/>
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
30	[number]	k4	30
000	[number]	k4	000
ha	ha	kA	ha
</s>
</p>
<p>
<s>
nejmohutnější	mohutný	k2eAgInSc1d3	nejmohutnější
český	český	k2eAgInSc1d1	český
balvan	balvan	k1gInSc1	balvan
-	-	kIx~	-
Dědek	Dědek	k1gMnSc1	Dědek
(	(	kIx(	(
<g/>
Žihle	žihle	k1gNnSc1	žihle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
-	-	kIx~	-
6	[number]	k4	6
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
-	-	kIx~	-
12	[number]	k4	12
m	m	kA	m
</s>
</p>
<p>
<s>
nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
Česka	Česko	k1gNnSc2	Česko
-	-	kIx~	-
Labe	Labe	k1gNnSc2	Labe
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
-	-	kIx~	-
115	[number]	k4	115
m	m	kA	m
(	(	kIx(	(
<g/>
normální	normální	k2eAgInSc1d1	normální
stav	stav	k1gInSc1	stav
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
řečišti	řečiště	k1gNnSc6	řečiště
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
místo	místo	k1gNnSc4	místo
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
jižní	jižní	k2eAgFnPc1d1	jižní
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
tloušťka	tloušťka	k1gFnSc1	tloušťka
-	-	kIx~	-
38	[number]	k4	38
km	km	kA	km
</s>
</p>
<p>
<s>
největší	veliký	k2eAgFnSc1d3	veliký
česká	český	k2eAgFnSc1d1	Česká
pánev	pánev	k1gFnSc1	pánev
-	-	kIx~	-
Třeboňská	třeboňský	k2eAgFnSc1d1	Třeboňská
pánev	pánev	k1gFnSc1	pánev
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
-	-	kIx~	-
1360	[number]	k4	1360
km2	km2	k4	km2
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
český	český	k2eAgInSc1d1	český
kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
skalní	skalní	k2eAgInSc1d1	skalní
oblouk	oblouk	k1gInSc1	oblouk
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
-	-	kIx~	-
16	[number]	k4	16
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
-	-	kIx~	-
26,5	[number]	k4	26,5
m	m	kA	m
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
český	český	k2eAgInSc1d1	český
viklan	viklan	k1gInSc1	viklan
-	-	kIx~	-
Husova	Husův	k2eAgFnSc1d1	Husova
kazatelna	kazatelna	k1gFnSc1	kazatelna
(	(	kIx(	(
<g/>
Petrovice	Petrovice	k1gFnSc1	Petrovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
-	-	kIx~	-
2	[number]	k4	2
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
-	-	kIx~	-
3	[number]	k4	3
m	m	kA	m
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
-	-	kIx~	-
1,8	[number]	k4	1,8
m	m	kA	m
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
český	český	k2eAgInSc1d1	český
bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
-	-	kIx~	-
Kunčice	Kunčice	k1gFnPc1	Kunčice
(	(	kIx(	(
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původ	původ	k1gInSc1	původ
-	-	kIx~	-
Skandinávie	Skandinávie	k1gFnSc1	Skandinávie
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
-	-	kIx~	-
16,5	[number]	k4	16,5
t	t	k?	t
</s>
</p>
<p>
<s>
nejrozlehlejší	rozlehlý	k2eAgNnSc1d3	nejrozlehlejší
skalní	skalní	k2eAgNnSc1d1	skalní
město	město	k1gNnSc1	město
-	-	kIx~	-
Adršpašsko-teplické	adršpašskoeplický	k2eAgFnPc1d1	adršpašsko-teplický
skály	skála	k1gFnPc1	skála
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
-	-	kIx~	-
17,72	[number]	k4	17,72
km2	km2	k4	km2
</s>
</p>
<p>
<s>
==	==	k?	==
Vodstvo	vodstvo	k1gNnSc1	vodstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Fjordy	fjord	k1gInPc4	fjord
===	===	k?	===
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
fjord	fjord	k1gInSc1	fjord
–	–	k?	–
Nordvestfjord	Nordvestfjord	k1gInSc1	Nordvestfjord
(	(	kIx(	(
<g/>
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
313	[number]	k4	313
km	km	kA	km
</s>
</p>
<p>
<s>
nejhlubší	hluboký	k2eAgInSc1d3	nejhlubší
fjord	fjord	k1gInSc1	fjord
–	–	k?	–
Vanderfjord	Vanderfjord	k1gInSc1	Vanderfjord
(	(	kIx(	(
<g/>
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
–	–	k?	–
2287	[number]	k4	2287
m	m	kA	m
</s>
</p>
<p>
<s>
===	===	k?	===
Gejzíry	gejzír	k1gInPc4	gejzír
===	===	k?	===
</s>
</p>
<p>
<s>
největší	veliký	k2eAgNnSc4d3	veliký
soustředění	soustředění	k1gNnSc4	soustředění
termálních	termální	k2eAgInPc2d1	termální
pramenů	pramen	k1gInPc2	pramen
–	–	k?	–
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
pramenů	pramen	k1gInPc2	pramen
–	–	k?	–
7000	[number]	k4	7000
(	(	kIx(	(
<g/>
v	v	k7c6	v
800	[number]	k4	800
geotermálních	geotermální	k2eAgFnPc6d1	geotermální
oblastech	oblast	k1gFnPc6	oblast
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejmohutnější	mohutný	k2eAgInSc1d3	nejmohutnější
gejzír	gejzír	k1gInSc1	gejzír
–	–	k?	–
Waimangu	Waimang	k1gInSc2	Waimang
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
300	[number]	k4	300
až	až	k9	až
400	[number]	k4	400
m	m	kA	m
</s>
</p>
<p>
<s>
nejteplejší	teplý	k2eAgInSc1d3	nejteplejší
český	český	k2eAgInSc1d1	český
minerální	minerální	k2eAgInSc1d1	minerální
pramen	pramen	k1gInSc1	pramen
-	-	kIx~	-
Vřídlo	vřídlo	k1gNnSc1	vřídlo
(	(	kIx(	(
<g/>
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
-	-	kIx~	-
plus	plus	k1gInSc1	plus
72,0	[number]	k4	72,0
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
===	===	k?	===
Jezera	jezero	k1gNnSc2	jezero
===	===	k?	===
</s>
</p>
<p>
<s>
nejhlubší	hluboký	k2eAgNnSc4d3	nejhlubší
jezero	jezero	k1gNnSc4	jezero
–	–	k?	–
Bajkal	Bajkal	k1gInSc1	Bajkal
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
–	–	k?	–
1637	[number]	k4	1637
m	m	kA	m
</s>
</p>
<p>
<s>
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
/	/	kIx~	/
<g/>
Asie	Asie	k1gFnSc1	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
371	[number]	k4	371
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
největší	veliký	k2eAgNnSc1d3	veliký
sladkovodní	sladkovodní	k2eAgNnSc1d1	sladkovodní
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
Hořejší	Hořejší	k2eAgNnSc1d1	Hořejší
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
/	/	kIx~	/
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
84	[number]	k4	84
214	[number]	k4	214
km2	km2	k4	km2
</s>
</p>
<p>
<s>
největší	veliký	k2eAgNnSc4d3	veliký
soustředění	soustředění	k1gNnSc4	soustředění
jezer	jezero	k1gNnPc2	jezero
–	–	k?	–
Finská	finský	k2eAgFnSc1d1	finská
jezerní	jezerní	k2eAgFnSc1d1	jezerní
plošina	plošina	k1gFnSc1	plošina
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
60	[number]	k4	60
000	[number]	k4	000
jezer	jezero	k1gNnPc2	jezero
</s>
</p>
<p>
<s>
nejvýše	vysoce	k6eAd3	vysoce
položené	položený	k2eAgNnSc1d1	položené
splavné	splavný	k2eAgNnSc1d1	splavné
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
Titicaca	Titicac	k1gInSc2	Titicac
(	(	kIx(	(
<g/>
Peru	Peru	k1gNnSc1	Peru
<g/>
/	/	kIx~	/
<g/>
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
)	)	kIx)	)
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
6900	[number]	k4	6900
až	až	k9	až
8288	[number]	k4	8288
km2	km2	k4	km2
<g/>
,	,	kIx,	,
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
–	–	k?	–
3810	[number]	k4	3810
m	m	kA	m
</s>
</p>
<p>
<s>
nejmladší	mladý	k2eAgNnSc1d3	nejmladší
české	český	k2eAgNnSc1d1	české
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
Odlezelské	Odlezelský	k2eAgNnSc1d1	Odlezelské
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Odlezly	odlézt	k5eAaPmAgFnP	odlézt
<g/>
/	/	kIx~	/
<g/>
Plzeň-sever	Plzeňever	k1gInSc1	Plzeň-sever
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
vzniku	vznik	k1gInSc2	vznik
–	–	k?	–
z	z	k7c2	z
noci	noc	k1gFnSc2	noc
25	[number]	k4	25
<g/>
.	.	kIx.	.
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1872	[number]	k4	1872
</s>
</p>
<p>
<s>
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
české	český	k2eAgNnSc1d1	české
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
Černé	Černé	k2eAgNnSc1d1	Černé
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
–	–	k?	–
39,8	[number]	k4	39,8
m	m	kA	m
</s>
</p>
<p>
<s>
největší	veliký	k2eAgNnSc1d3	veliký
české	český	k2eAgNnSc1d1	české
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
Černé	Černé	k2eAgNnSc1d1	Černé
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
18,4	[number]	k4	18,4
ha	ha	kA	ha
</s>
</p>
<p>
<s>
nejvýše	vysoce	k6eAd3	vysoce
položené	položený	k2eAgNnSc1d1	položené
české	český	k2eAgNnSc1d1	české
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
Laka	laka	k1gFnSc1	laka
(	(	kIx(	(
<g/>
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
–	–	k?	–
1096	[number]	k4	1096
m	m	kA	m
(	(	kIx(	(
<g/>
v	v	k7c6	v
Úpském	úpský	k2eAgNnSc6d1	Úpské
rašeliništi	rašeliniště	k1gNnSc6	rašeliniště
jsou	být	k5eAaImIp3nP	být
malá	malý	k2eAgNnPc1d1	malé
nepojmenovaná	pojmenovaný	k2eNgNnPc1d1	nepojmenované
jezera	jezero	k1gNnPc1	jezero
i	i	k9	i
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1430	[number]	k4	1430
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
největší	veliký	k2eAgNnSc1d3	veliký
rašelinové	rašelinový	k2eAgNnSc1d1	rašelinové
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
jezírko	jezírko	k1gNnSc1	jezírko
na	na	k7c6	na
Chalupské	chalupský	k2eAgFnSc6d1	Chalupská
slati	slať	k1gFnSc6	slať
(	(	kIx(	(
<g/>
Svinná	Svinný	k2eAgFnSc1d1	Svinná
Lada	Lada	k1gFnSc1	Lada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
1,3	[number]	k4	1,3
haSeznam	haSeznam	k6eAd1	haSeznam
největších	veliký	k2eAgNnPc2d3	veliký
jezer	jezero	k1gNnPc2	jezero
</s>
</p>
<p>
<s>
===	===	k?	===
Ledovce	ledovec	k1gInPc1	ledovec
===	===	k?	===
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
vysokohorský	vysokohorský	k2eAgInSc1d1	vysokohorský
ledovec	ledovec	k1gInSc1	ledovec
–	–	k?	–
Beringův	Beringův	k2eAgInSc1d1	Beringův
ledovec	ledovec	k1gInSc1	ledovec
(	(	kIx(	(
<g/>
Aljaška	Aljaška	k1gFnSc1	Aljaška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
170	[number]	k4	170
km	km	kA	km
</s>
</p>
<p>
<s>
největší	veliký	k2eAgFnSc1d3	veliký
plocha	plocha	k1gFnSc1	plocha
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
ledu	led	k1gInSc2	led
–	–	k?	–
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
13	[number]	k4	13
802	[number]	k4	802
000	[number]	k4	000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
–	–	k?	–
24	[number]	k4	24
900	[number]	k4	900
000	[number]	k4	000
km3	km3	k4	km3
</s>
</p>
<p>
<s>
===	===	k?	===
Moře	moře	k1gNnSc1	moře
===	===	k?	===
</s>
</p>
<p>
<s>
nejmenší	malý	k2eAgInSc1d3	nejmenší
oceán	oceán	k1gInSc1	oceán
–	–	k?	–
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
13	[number]	k4	13
100	[number]	k4	100
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
největší	veliký	k2eAgNnSc1d3	veliký
moře	moře	k1gNnSc1	moře
–	–	k?	–
Filipínské	filipínský	k2eAgNnSc1d1	filipínské
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
5	[number]	k4	5
726	[number]	k4	726
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
oceán	oceán	k1gInSc1	oceán
–	–	k?	–
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
179	[number]	k4	179
679	[number]	k4	679
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
===	===	k?	===
Průlivy	průliv	k1gInPc4	průliv
===	===	k?	===
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
průliv	průliv	k1gInSc1	průliv
–	–	k?	–
Mosambický	mosambický	k2eAgInSc1d1	mosambický
průliv	průliv	k1gInSc1	průliv
(	(	kIx(	(
<g/>
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
1760	[number]	k4	1760
km	km	kA	km
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
–	–	k?	–
422	[number]	k4	422
až	až	k9	až
925	[number]	k4	925
km	km	kA	km
</s>
</p>
<p>
<s>
nejširší	široký	k2eAgInSc1d3	nejširší
průliv	průliv	k1gInSc1	průliv
–	–	k?	–
Drakeův	Drakeův	k2eAgInSc1d1	Drakeův
průliv	průliv	k1gInSc1	průliv
(	(	kIx(	(
<g/>
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
/	/	kIx~	/
<g/>
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
–	–	k?	–
1100	[number]	k4	1100
km	km	kA	km
</s>
</p>
<p>
<s>
nejužší	úzký	k2eAgInSc1d3	nejužší
průliv	průliv	k1gInSc1	průliv
–	–	k?	–
Bospor	Bospor	k1gInSc1	Bospor
(	(	kIx(	(
<g/>
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgFnSc1d1	minimální
šířka	šířka	k1gFnSc1	šířka
–	–	k?	–
660	[number]	k4	660
m	m	kA	m
</s>
</p>
<p>
<s>
===	===	k?	===
Řeky	Řek	k1gMnPc4	Řek
===	===	k?	===
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
–	–	k?	–
Amazonka	Amazonka	k1gFnSc1	Amazonka
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
7062	[number]	k4	7062
km	km	kA	km
</s>
</p>
<p>
<s>
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
řeka	řeka	k1gFnSc1	řeka
–	–	k?	–
Roe	Roe	k1gMnSc1	Roe
River	River	k1gMnSc1	River
(	(	kIx(	(
<g/>
Montana	Montana	k1gFnSc1	Montana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
–	–	k?	–
61	[number]	k4	61
m	m	kA	m
</s>
</p>
<p>
<s>
nejmohutnější	mohutný	k2eAgFnSc1d3	nejmohutnější
řeka	řeka	k1gFnSc1	řeka
–	–	k?	–
Amazonka	Amazonka	k1gFnSc1	Amazonka
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
6	[number]	k4	6
915	[number]	k4	915
000	[number]	k4	000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
průtok	průtok	k1gInSc1	průtok
–	–	k?	–
219	[number]	k4	219
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
největší	veliký	k2eAgFnSc1d3	veliký
povodí	povodit	k5eAaPmIp3nS	povodit
–	–	k?	–
Amazonka	Amazonka	k1gFnSc1	Amazonka
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
7	[number]	k4	7
050	[number]	k4	050
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
nejdéle	dlouho	k6eAd3	dlouho
zamrzající	zamrzající	k2eAgFnSc1d1	zamrzající
řeka	řeka	k1gFnSc1	řeka
–	–	k?	–
Lena	Lena	k1gFnSc1	Lena
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
dní	den	k1gInPc2	den
ledového	ledový	k2eAgInSc2d1	ledový
příkrovu	příkrov	k1gInSc2	příkrov
–	–	k?	–
203	[number]	k4	203
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
česká	český	k2eAgFnSc1d1	Česká
řeka	řeka	k1gFnSc1	řeka
-	-	kIx~	-
Vltava	Vltava	k1gFnSc1	Vltava
(	(	kIx(	(
<g/>
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
-	-	kIx~	-
430	[number]	k4	430
km	km	kA	km
</s>
</p>
<p>
<s>
nejvodnatější	vodnatý	k2eAgFnSc1d3	nejvodnatější
česká	český	k2eAgFnSc1d1	Česká
řeka	řeka	k1gFnSc1	řeka
-	-	kIx~	-
Labe	Labe	k1gNnSc1	Labe
(	(	kIx(	(
<g/>
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průtok	průtok	k1gInSc1	průtok
-	-	kIx~	-
308	[number]	k4	308
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
Hřensko	Hřensko	k1gNnSc1	Hřensko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
největší	veliký	k2eAgNnSc1d3	veliký
české	český	k2eAgNnSc1d1	české
povodí	povodí	k1gNnSc1	povodí
-	-	kIx~	-
Labe	Labe	k1gNnSc1	Labe
(	(	kIx(	(
<g/>
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
-	-	kIx~	-
49	[number]	k4	49
933	[number]	k4	933
km2	km2	k4	km2
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc4d3	veliký
spád	spád	k1gInSc4	spád
české	český	k2eAgFnSc2d1	Česká
řeky	řeka	k1gFnSc2	řeka
-	-	kIx~	-
Labe	Labe	k1gNnSc2	Labe
(	(	kIx(	(
<g/>
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spád	spád	k1gInSc1	spád
-	-	kIx~	-
1269	[number]	k4	1269
m	m	kA	m
(	(	kIx(	(
<g/>
pramen	pramen	k1gInSc4	pramen
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1384	[number]	k4	1384
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
Seznam	seznam	k1gInSc1	seznam
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
řek	řeka	k1gFnPc2	řeka
</s>
</p>
<p>
<s>
===	===	k?	===
Úkazy	úkaz	k1gInPc4	úkaz
===	===	k?	===
</s>
</p>
<p>
<s>
největší	veliký	k2eAgFnSc1d3	veliký
hloubka	hloubka	k1gFnSc1	hloubka
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
–	–	k?	–
Marianský	Marianský	k2eAgInSc1d1	Marianský
příkop	příkop	k1gInSc1	příkop
(	(	kIx(	(
<g/>
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
–	–	k?	–
10	[number]	k4	10
924	[number]	k4	924
m	m	kA	m
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
6370	[number]	k4	6370
km	km	kA	km
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hloubka	hloubka	k1gFnSc1	hloubka
nejblíže	blízce	k6eAd3	blízce
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
Země	zem	k1gFnSc2	zem
–	–	k?	–
Polární	polární	k2eAgFnSc1d1	polární
hlubokomořská	hlubokomořský	k2eAgFnSc1d1	hlubokomořská
planina	planina	k1gFnSc1	planina
(	(	kIx(	(
<g/>
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
–	–	k?	–
5449	[number]	k4	5449
m	m	kA	m
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
6353	[number]	k4	6353
km	km	kA	km
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
===	===	k?	===
Útesy	útes	k1gInPc4	útes
===	===	k?	===
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
mořský	mořský	k2eAgInSc1d1	mořský
útes	útes	k1gInSc1	útes
–	–	k?	–
Milfordský	Milfordský	k2eAgInSc1d1	Milfordský
záliv	záliv	k1gInSc1	záliv
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
1584	[number]	k4	1584
m	m	kA	m
</s>
</p>
<p>
<s>
===	===	k?	===
Vodopády	vodopád	k1gInPc4	vodopád
===	===	k?	===
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
vodopádový	vodopádový	k2eAgInSc1d1	vodopádový
systém	systém	k1gInSc1	systém
–	–	k?	–
Iguaçu	Iguaçus	k1gInSc2	Iguaçus
(	(	kIx(	(
<g/>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
/	/	kIx~	/
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šíře	šíře	k1gFnSc1	šíře
–	–	k?	–
4	[number]	k4	4
km	km	kA	km
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
72	[number]	k4	72
m	m	kA	m
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
oddělených	oddělený	k2eAgNnPc2d1	oddělené
ramen	rameno	k1gNnPc2	rameno
–	–	k?	–
275	[number]	k4	275
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
průtok	průtok	k1gInSc1	průtok
vodopádu	vodopád	k1gInSc2	vodopád
–	–	k?	–
Chutes	Chutes	k1gMnSc1	Chutes
Livingstone	Livingston	k1gInSc5	Livingston
(	(	kIx(	(
<g/>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
průtok	průtok	k1gInSc1	průtok
–	–	k?	–
35	[number]	k4	35
110	[number]	k4	110
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vodopád	vodopád	k1gInSc1	vodopád
–	–	k?	–
Angelův	angelův	k2eAgInSc1d1	angelův
vodopád	vodopád	k1gInSc1	vodopád
(	(	kIx(	(
<g/>
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
–	–	k?	–
979	[number]	k4	979
m	m	kA	m
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
český	český	k2eAgInSc1d1	český
vodopád	vodopád	k1gInSc1	vodopád
-	-	kIx~	-
Pančavský	Pančavský	k2eAgInSc1d1	Pančavský
vodopád	vodopád	k1gInSc1	vodopád
(	(	kIx(	(
<g/>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
-	-	kIx~	-
148	[number]	k4	148
m	m	kA	m
</s>
</p>
<p>
<s>
===	===	k?	===
Zálivy	záliv	k1gInPc4	záliv
===	===	k?	===
</s>
</p>
<p>
<s>
největší	veliký	k2eAgFnSc1d3	veliký
laguna	laguna	k1gFnSc1	laguna
–	–	k?	–
Laguna	laguna	k1gFnSc1	laguna
Patos	patos	k1gInSc1	patos
(	(	kIx(	(
<g/>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
–	–	k?	–
10	[number]	k4	10
145	[number]	k4	145
km2	km2	k4	km2
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
záliv	záliv	k1gInSc1	záliv
–	–	k?	–
Bengálský	bengálský	k2eAgInSc1d1	bengálský
záliv	záliv	k1gInSc1	záliv
(	(	kIx(	(
<g/>
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
2	[number]	k4	2
172	[number]	k4	172
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
rekordů	rekord	k1gInPc2	rekord
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
Zeměpisné	zeměpisný	k2eAgInPc1d1	zeměpisný
rekordy	rekord	k1gInPc1	rekord
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Vybraná	vybraná	k1gFnSc1	vybraná
světová	světový	k2eAgFnSc1d1	světová
nej	nej	k?	nej
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
světová	světový	k2eAgFnSc1d1	světová
nej	nej	k?	nej
</s>
</p>
<p>
<s>
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
National	National	k1gFnSc4	National
Climatic	Climatice	k1gFnPc2	Climatice
Data	datum	k1gNnSc2	datum
Center	centrum	k1gNnPc2	centrum
</s>
</p>
<p>
<s>
Extrémní	extrémní	k2eAgFnSc1d1	extrémní
geografie	geografie	k1gFnSc1	geografie
</s>
</p>
<p>
<s>
Extrémní	extrémní	k2eAgFnPc1d1	extrémní
teploty	teplota	k1gFnPc1	teplota
světa	svět	k1gInSc2	svět
</s>
</p>
