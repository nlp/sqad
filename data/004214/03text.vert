<s>
Karamazovi	Karamaz	k1gMnSc3	Karamaz
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Petra	Petr	k1gMnSc2	Petr
Zelenky	Zelenka	k1gMnSc2	Zelenka
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
česko-polské	českoolský	k2eAgFnSc6d1	česko-polská
koprodukci	koprodukce	k1gFnSc6	koprodukce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
filmu	film	k1gInSc2	film
tvoří	tvořit	k5eAaImIp3nS	tvořit
představení	představení	k1gNnSc4	představení
Dejvického	dejvický	k2eAgNnSc2d1	Dejvické
divadla	divadlo	k1gNnSc2	divadlo
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
(	(	kIx(	(
<g/>
román	román	k1gInSc4	román
F.	F.	kA	F.
M.	M.	kA	M.
Dostojevského	Dostojevský	k2eAgInSc2d1	Dostojevský
v	v	k7c6	v
dramatizaci	dramatizace	k1gFnSc6	dramatizace
Evalda	Evald	k1gMnSc2	Evald
Schorma	Schorm	k1gMnSc2	Schorm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
zasazeno	zasadit	k5eAaPmNgNnS	zasadit
do	do	k7c2	do
netradičního	tradiční	k2eNgNnSc2d1	netradiční
prostředí	prostředí	k1gNnSc2	prostředí
polských	polský	k2eAgFnPc2d1	polská
oceláren	ocelárna	k1gFnPc2	ocelárna
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
souběžně	souběžně	k6eAd1	souběžně
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
další	další	k2eAgFnSc1d1	další
dějová	dějový	k2eAgFnSc1d1	dějová
linie	linie	k1gFnSc1	linie
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
herců	herc	k1gInPc2	herc
a	a	k8xC	a
přihlížejících	přihlížející	k2eAgMnPc2d1	přihlížející
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
železáren	železárna	k1gFnPc2	železárna
v	v	k7c6	v
Hrádku	Hrádok	k1gInSc6	Hrádok
u	u	k7c2	u
Rokycan	Rokycany	k1gInPc2	Rokycany
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
<s>
MFF	MFF	kA	MFF
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
snímek	snímek	k1gInSc4	snímek
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
poroty	porota	k1gFnSc2	porota
FIPRESCI	FIPRESCI	kA	FIPRESCI
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
Evropské	evropský	k2eAgFnSc2d1	Evropská
filmové	filmový	k2eAgFnSc2d1	filmová
ceny	cena	k1gFnSc2	cena
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
osmi	osm	k4xCc2	osm
nominací	nominace	k1gFnPc2	nominace
získal	získat	k5eAaPmAgMnS	získat
dva	dva	k4xCgInPc4	dva
České	český	k2eAgInPc4d1	český
lvy	lev	k1gInPc4	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
a	a	k8xC	a
režii	režie	k1gFnSc4	režie
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
na	na	k7c6	na
nominačním	nominační	k2eAgInSc6d1	nominační
večeru	večer	k1gInSc6	večer
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
a	a	k8xC	a
též	též	k9	též
ocenění	ocenění	k1gNnSc4	ocenění
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
filmový	filmový	k2eAgInSc4d1	filmový
plakát	plakát	k1gInSc4	plakát
<g/>
.	.	kIx.	.
</s>
<s>
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Karamazovi	Karamaz	k1gMnSc3	Karamaz
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc3d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
</s>
