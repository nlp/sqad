<s>
Zaz	Zaz	k?	Zaz
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Isabelle	Isabelle	k1gFnSc2	Isabelle
Geffroy	Geffroa	k1gFnSc2	Geffroa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1980	[number]	k4	1980
Tours	Toursa	k1gFnPc2	Toursa
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
zpívající	zpívající	k2eAgMnSc1d1	zpívající
převážně	převážně	k6eAd1	převážně
jazz	jazz	k1gInSc4	jazz
a	a	k8xC	a
šansony	šansona	k1gFnPc4	šansona
<g/>
.	.	kIx.	.
</s>
<s>
Hitem	hit	k1gInSc7	hit
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
například	například	k6eAd1	například
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
veux	veux	k1gInSc1	veux
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c4	na
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
v	v	k7c4	v
Tours	Tours	k1gInSc4	Tours
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
a	a	k8xC	a
bratrem	bratr	k1gMnSc7	bratr
tam	tam	k6eAd1	tam
studovala	studovat	k5eAaImAgFnS	studovat
až	až	k6eAd1	až
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
jedenácti	jedenáct	k4xCc2	jedenáct
let	léto	k1gNnPc2	léto
hudební	hudební	k2eAgFnSc4d1	hudební
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
piano	piano	k1gNnSc4	piano
<g/>
,	,	kIx,	,
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
sborový	sborový	k2eAgInSc4d1	sborový
zpěv	zpěv	k1gInSc4	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
odešli	odejít	k5eAaPmAgMnP	odejít
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
sourozenci	sourozenec	k1gMnPc1	sourozenec
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
do	do	k7c2	do
Libourne	Libourn	k1gInSc5	Libourn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
Isabelle	Isabelle	k1gFnSc1	Isabelle
docházela	docházet	k5eAaImAgFnS	docházet
na	na	k7c4	na
hodiny	hodina	k1gFnPc4	hodina
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
rok	rok	k1gInSc4	rok
studovala	studovat	k5eAaImAgFnS	studovat
v	v	k7c6	v
CIAM	CIAM	kA	CIAM
(	(	kIx(	(
<g/>
Centre	centr	k1gInSc5	centr
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
information	information	k1gInSc1	information
et	et	k?	et
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
activités	activités	k1gInSc1	activités
musicales	musicales	k1gInSc1	musicales
<g/>
,	,	kIx,	,
Informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
hudebních	hudební	k2eAgFnPc2d1	hudební
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
)	)	kIx)	)
v	v	k7c6	v
Bordeaux	Bordeaux	k1gNnSc6	Bordeaux
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
chodila	chodit	k5eAaImAgFnS	chodit
i	i	k9	i
na	na	k7c4	na
kurzy	kurz	k1gInPc4	kurz
kung-fu	kung	k1gInSc2	kung-f
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
získala	získat	k5eAaPmAgFnS	získat
regionální	regionální	k2eAgNnSc4d1	regionální
stipendium	stipendium	k1gNnSc4	stipendium
od	od	k7c2	od
Conseil	Conseila	k1gFnPc2	Conseila
régional	régionat	k5eAaPmAgMnS	régionat
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yRgMnSc3	který
mohla	moct	k5eAaImAgFnS	moct
dále	daleko	k6eAd2	daleko
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
hudební	hudební	k2eAgFnSc6d1	hudební
škole	škola	k1gFnSc6	škola
CIAM	CIAM	kA	CIAM
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ji	on	k3xPp3gFnSc4	on
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
<g/>
,	,	kIx,	,
ZAZ	ZAZ	kA	ZAZ
často	často	k6eAd1	často
cituje	citovat	k5eAaBmIp3nS	citovat
Vivaldiho	Vivaldi	k1gMnSc4	Vivaldi
Čtvero	čtvero	k4xRgNnSc1	čtvero
ročních	roční	k2eAgFnPc2d1	roční
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
jazzovou	jazzový	k2eAgFnSc4d1	jazzová
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Ellu	Ellus	k1gInSc2	Ellus
Fitzgerald	Fitzgeralda	k1gFnPc2	Fitzgeralda
<g/>
,	,	kIx,	,
i	i	k8xC	i
francouzský	francouzský	k2eAgInSc1d1	francouzský
šanson	šanson	k1gInSc1	šanson
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
jejími	její	k3xOp3gMnPc7	její
oblíbenými	oblíbený	k2eAgMnPc7d1	oblíbený
zpěváky	zpěvák	k1gMnPc7	zpěvák
a	a	k8xC	a
hudebníky	hudebník	k1gMnPc7	hudebník
Enrico	Enrico	k1gMnSc1	Enrico
Macias	Macias	k1gMnSc1	Macias
<g/>
,	,	kIx,	,
Bobby	Bobba	k1gFnPc1	Bobba
McFerrin	McFerrin	k1gInSc1	McFerrin
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Bona	bona	k1gFnSc1	bona
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
africké	africký	k2eAgInPc1d1	africký
<g/>
,	,	kIx,	,
latinské	latinský	k2eAgInPc1d1	latinský
a	a	k8xC	a
kubánské	kubánský	k2eAgInPc1d1	kubánský
rytmy	rytmus	k1gInPc1	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
Zaz	Zaz	k?	Zaz
debutovala	debutovat	k5eAaBmAgFnS	debutovat
jako	jako	k9	jako
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
v	v	k7c6	v
bluesové	bluesový	k2eAgFnSc6d1	bluesová
skupině	skupina	k1gFnSc6	skupina
Fifty	Fifta	k1gFnSc2	Fifta
Fingers	Fingersa	k1gFnPc2	Fingersa
<g/>
.	.	kIx.	.
</s>
<s>
Vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
hudebními	hudební	k2eAgInPc7d1	hudební
seskupeními	seskupení	k1gNnPc7	seskupení
v	v	k7c6	v
Angoulê	Angoulê	k1gFnSc6	Angoulê
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
jazzovém	jazzový	k2eAgInSc6d1	jazzový
kvintetu	kvintet	k1gInSc6	kvintet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
zpěvaček	zpěvačka	k1gFnPc2	zpěvačka
baskického	baskický	k2eAgInSc2d1	baskický
šestnáctičlenného	šestnáctičlenný	k2eAgInSc2d1	šestnáctičlenný
orchestru	orchestr	k1gInSc2	orchestr
Izar	Izar	k1gMnSc1	Izar
Adatz	Adatz	k1gMnSc1	Adatz
(	(	kIx(	(
<g/>
Padající	padající	k2eAgFnSc1d1	padající
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Tarnos	Tarnosa	k1gFnPc2	Tarnosa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
orchestrem	orchestr	k1gInSc7	orchestr
jezdila	jezdit	k5eAaImAgFnS	jezdit
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c4	v
Midi-Pyrenées	Midi-Pyrenées	k1gInSc4	Midi-Pyrenées
a	a	k8xC	a
Pays	Pays	k1gInSc4	Pays
basque	basqu	k1gFnSc2	basqu
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
bluesrockové	bluesrockový	k2eAgFnSc2d1	bluesrocková
skupiny	skupina	k1gFnSc2	skupina
Red	Red	k1gFnSc2	Red
Eyes	Eyesa	k1gFnPc2	Eyesa
v	v	k7c6	v
Bordeaux	Bordeaux	k1gNnSc6	Bordeaux
<g/>
.	.	kIx.	.
</s>
<s>
Zpívala	zpívat	k5eAaImAgFnS	zpívat
také	také	k9	také
po	po	k7c6	po
kavárnách	kavárna	k1gFnPc6	kavárna
a	a	k8xC	a
na	na	k7c6	na
regionálních	regionální	k2eAgInPc6d1	regionální
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
v	v	k7c6	v
Bretani	Bretaň	k1gFnSc6	Bretaň
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
jako	jako	k8xS	jako
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
latinsko-americké	latinskomerický	k2eAgFnSc2d1	latinsko-americká
skupiny	skupina	k1gFnSc2	skupina
Don	Don	k1gMnSc1	Don
Diego	Diego	k1gMnSc1	Diego
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
hudbu	hudba	k1gFnSc4	hudba
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
<g/>
,	,	kIx,	,
španělskou	španělský	k2eAgFnSc4d1	španělská
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
styly	styl	k1gInPc7	styl
africké	africký	k2eAgFnSc2d1	africká
<g/>
,	,	kIx,	,
arabské	arabský	k2eAgFnSc2d1	arabská
<g/>
,	,	kIx,	,
andaluzské	andaluzský	k2eAgFnSc2d1	andaluzská
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
latinskoamerickou	latinskoamerický	k2eAgFnSc7d1	latinskoamerická
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Don	Don	k1gMnSc1	Don
Diego	Diego	k1gMnSc1	Diego
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
hvězdami	hvězda	k1gFnPc7	hvězda
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Yuri	Yur	k1gFnPc1	Yur
Buenaventura	Buenaventura	k1gFnSc1	Buenaventura
a	a	k8xC	a
Bernard	Bernard	k1gMnSc1	Bernard
Lavilliers	Lavilliersa	k1gFnPc2	Lavilliersa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
ZAZ	ZAZ	kA	ZAZ
účastnila	účastnit	k5eAaImAgFnS	účastnit
festivalu	festival	k1gInSc2	festival
Musiques	Musiques	k1gMnSc1	Musiques
Métisses	Métisses	k1gMnSc1	Métisses
v	v	k7c6	v
Angoulê	Angoulê	k1gFnSc6	Angoulê
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zpívala	zpívat	k5eAaImAgFnS	zpívat
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k1xP	půl
v	v	k7c6	v
kabaretu	kabaret	k1gInSc6	kabaret
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
bez	bez	k7c2	bez
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
.	.	kIx.	.
</s>
<s>
Zaz	Zaz	k?	Zaz
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
na	na	k7c4	na
internetový	internetový	k2eAgInSc4d1	internetový
inzerát	inzerát	k1gInSc4	inzerát
producenta	producent	k1gMnSc2	producent
a	a	k8xC	a
skladatele	skladatel	k1gMnSc2	skladatel
Kerredina	Kerredin	k2eAgMnSc2d1	Kerredin
Soltaniho	Soltani	k1gMnSc2	Soltani
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hledal	hledat	k5eAaImAgInS	hledat
novou	nový	k2eAgFnSc4d1	nová
umělkyni	umělkyně	k1gFnSc4	umělkyně
s	s	k7c7	s
trochu	trochu	k6eAd1	trochu
"	"	kIx"	"
<g/>
nakřáplým	nakřáplý	k2eAgInSc7d1	nakřáplý
hlasem	hlas	k1gInSc7	hlas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
napsal	napsat	k5eAaPmAgMnS	napsat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
veux	veux	k1gInSc1	veux
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
našel	najít	k5eAaPmAgMnS	najít
producenta	producent	k1gMnSc4	producent
i	i	k9	i
vydavatele	vydavatel	k1gMnSc4	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
Zaz	Zaz	k?	Zaz
ale	ale	k9	ale
stále	stále	k6eAd1	stále
hledala	hledat	k5eAaImAgFnS	hledat
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Přidala	přidat	k5eAaPmAgFnS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
rapové	rapový	k2eAgFnSc3d1	rapová
skupině	skupina	k1gFnSc3	skupina
Le	Le	k1gFnSc1	Le
4	[number]	k4	4
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
natočila	natočit	k5eAaBmAgFnS	natočit
několik	několik	k4yIc4	několik
klipů	klip	k1gInPc2	klip
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Aveyron	Aveyron	k1gMnSc1	Aveyron
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Rugby	rugby	k1gNnSc1	rugby
Amateur	Amateura	k1gFnPc2	Amateura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
zpívala	zpívat	k5eAaImAgFnS	zpívat
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
všech	všecek	k3xTgMnPc2	všecek
možných	možný	k2eAgMnPc2d1	možný
stylů	styl	k1gInPc2	styl
s	s	k7c7	s
Miliaouechem	Miliaouech	k1gInSc7	Miliaouech
<g/>
,	,	kIx,	,
v	v	k7c6	v
CAT	CAT	kA	CAT
v	v	k7c6	v
Bordeaux	Bordeaux	k1gNnSc6	Bordeaux
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hudebních	hudební	k2eAgInPc2d1	hudební
a	a	k8xC	a
literárních	literární	k2eAgInPc2d1	literární
workshopů	workshop	k1gInPc2	workshop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
soutěže	soutěž	k1gFnPc4	soutěž
Le	Le	k1gMnSc2	Le
Tremplin	Tremplin	k2eAgInSc4d1	Tremplin
Génération	Génération	k1gInSc4	Génération
France	Franc	k1gMnSc2	Franc
Bleu	Bleus	k1gInSc2	Bleus
<g/>
/	/	kIx~	/
<g/>
Réservoir	Réservoir	k1gInSc1	Réservoir
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Sweet	Sweet	k1gMnSc1	Sweet
Air	Air	k1gMnSc1	Air
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
Zaz	Zaz	k1gFnPc2	Zaz
a	a	k8xC	a
Sweet	Sweeta	k1gFnPc2	Sweeta
Air	Air	k1gMnPc1	Air
natočili	natočit	k5eAaBmAgMnP	natočit
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
Sweet	Sweet	k1gMnSc1	Sweet
Air	Air	k1gMnSc1	Air
et	et	k?	et
Zaz	Zaz	k1gMnSc1	Zaz
à	à	k?	à
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
international	internationat	k5eAaPmAgMnS	internationat
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
už	už	k6eAd1	už
se	se	k3xPyFc4	se
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
ředitele	ředitel	k1gMnSc2	ředitel
Alliance	Allianec	k1gMnSc2	Allianec
française	française	k1gFnSc2	française
ve	v	k7c6	v
Vladivostoku	Vladivostok	k1gInSc6	Vladivostok
<g/>
,	,	kIx,	,
Cédrica	Cédric	k2eAgMnSc4d1	Cédric
Grase	Gras	k1gMnSc4	Gras
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
slyšel	slyšet	k5eAaImAgMnS	slyšet
v	v	k7c4	v
piano	piano	k1gNnSc4	piano
baru	bar	k1gInSc2	bar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
klavíristy	klavírista	k1gMnSc2	klavírista
Juliena	Julien	k1gMnSc2	Julien
Lifszyce	Lifszyce	k1gMnSc2	Lifszyce
<g/>
,	,	kIx,	,
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
13	[number]	k4	13
koncertů	koncert	k1gInPc2	koncert
během	během	k7c2	během
14	[number]	k4	14
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
se	se	k3xPyFc4	se
ZAZ	ZAZ	kA	ZAZ
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
soutěže	soutěž	k1gFnSc2	soutěž
Le	Le	k1gFnSc2	Le
Tremplin	Tremplin	k2eAgInSc4d1	Tremplin
Génération	Génération	k1gInSc4	Génération
France	Franc	k1gMnSc2	Franc
Bleu	Bleus	k1gInSc2	Bleus
<g/>
/	/	kIx~	/
<g/>
Réservoir	Réservoir	k1gInSc1	Réservoir
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
Olympii	Olympia	k1gFnSc6	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
ji	on	k3xPp3gFnSc4	on
Mathieu	Mathiea	k1gFnSc4	Mathiea
Verlot	Verlota	k1gFnPc2	Verlota
(	(	kIx(	(
<g/>
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
Lafage	Lafag	k1gFnSc2	Lafag
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nicolas	Nicolas	k1gMnSc1	Nicolas
Cabat	Cabat	k1gInSc1	Cabat
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
budou	být	k5eAaImBp3nP	být
pro	pro	k7c4	pro
Zaz	Zaz	k1gMnPc4	Zaz
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
studia	studio	k1gNnSc2	studio
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
vydat	vydat	k5eAaPmF	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jako	jako	k8xS	jako
vítěz	vítěz	k1gMnSc1	vítěz
soutěže	soutěž	k1gFnSc2	soutěž
dostala	dostat	k5eAaPmAgFnS	dostat
5	[number]	k4	5
000	[number]	k4	000
euro	euro	k1gNnSc4	euro
k	k	k7c3	k
natočení	natočení	k1gNnSc3	natočení
videoklipu	videoklip	k1gInSc2	videoklip
pro	pro	k7c4	pro
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musela	muset	k5eAaImAgFnS	muset
počkat	počkat	k5eAaPmF	počkat
ještě	ještě	k9	ještě
14	[number]	k4	14
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
jela	jet	k5eAaImAgFnS	jet
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
do	do	k7c2	do
Vladivostoku	Vladivostok	k1gInSc2	Vladivostok
přes	přes	k7c4	přes
Sibiř	Sibiř	k1gFnSc4	Sibiř
až	až	k9	až
do	do	k7c2	do
Nižného	nižný	k2eAgInSc2d1	nižný
Novgorodu	Novgorod	k1gInSc2	Novgorod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Zaz	Zaz	k1gFnSc2	Zaz
zpívala	zpívat	k5eAaImAgFnS	zpívat
i	i	k9	i
písně	píseň	k1gFnPc1	píseň
Édith	Édith	k1gInSc1	Édith
Piaf	Piaf	k1gFnSc2	Piaf
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
Brela	Brel	k1gMnSc2	Brel
<g/>
,	,	kIx,	,
Charlese	Charles	k1gMnSc2	Charles
Aznavoura	Aznavour	k1gMnSc2	Aznavour
<g/>
,	,	kIx,	,
Serge	Serg	k1gMnSc2	Serg
Gainsbourga	Gainsbourg	k1gMnSc2	Gainsbourg
<g/>
,	,	kIx,	,
Joe	Joe	k1gMnSc2	Joe
Dassina	Dassina	k1gFnSc1	Dassina
<g/>
,	,	kIx,	,
Patricie	Patricie	k1gFnSc1	Patricie
Kaas	Kaasa	k1gFnPc2	Kaasa
a	a	k8xC	a
své	svůj	k3xOyFgFnPc4	svůj
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
příštího	příští	k2eAgNnSc2d1	příští
alba	album	k1gNnSc2	album
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
veux	veux	k1gInSc1	veux
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Les	les	k1gInSc1	les
passants	passants	k1gInSc1	passants
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Prends	Prends	k1gInSc1	Prends
garde	garde	k1gFnSc1	garde
à	à	k?	à
ta	ten	k3xDgFnSc1	ten
langue	langue	k1gFnSc1	langue
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
jela	jet	k5eAaImAgFnS	jet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
v	v	k7c6	v
restaurtaci	restaurtace	k1gFnSc6	restaurtace
El	Ela	k1gFnPc2	Ela
Papagayo	Papagayo	k6eAd1	Papagayo
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
hráli	hrát	k5eAaImAgMnP	hrát
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
latino	latina	k1gFnSc5	latina
a	a	k8xC	a
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
jednu	jeden	k4xCgFnSc4	jeden
španělskou	španělský	k2eAgFnSc4d1	španělská
tanečnici	tanečnice	k1gFnSc4	tanečnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
rezidenci	rezidence	k1gFnSc6	rezidence
Sentier	Sentier	k1gMnSc1	Sentier
des	des	k1gNnSc2	des
Halles	Halles	k1gMnSc1	Halles
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
koncert	koncert	k1gInSc1	koncert
navštívil	navštívit	k5eAaPmAgInS	navštívit
incognito	incognit	k2eAgNnSc4d1	incognito
skladatel	skladatel	k1gMnSc1	skladatel
Raphael	Raphalo	k1gNnPc2	Raphalo
a	a	k8xC	a
následně	následně	k6eAd1	následně
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
napsal	napsat	k5eAaPmAgMnS	napsat
překrásné	překrásný	k2eAgFnPc4d1	překrásná
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Zaz	Zaz	k1gFnSc1	Zaz
zpívá	zpívat	k5eAaImIp3nS	zpívat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
Fée	Fée	k1gFnPc2	Fée
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Port	port	k1gInSc1	port
Coto	Coto	k1gNnSc1	Coto
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Éblouie	Éblouie	k1gFnSc1	Éblouie
par	para	k1gFnPc2	para
la	la	k1gNnSc1	la
nuit	nuit	k1gMnSc1	nuit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Zaz	Zaz	k1gFnSc1	Zaz
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
japonského	japonský	k2eAgInSc2d1	japonský
festivalu	festival	k1gInSc2	festival
Fuji	Fuji	kA	Fuji
Rock	rock	k1gInSc1	rock
Festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
dále	daleko	k6eAd2	daleko
zpívala	zpívat	k5eAaImAgFnS	zpívat
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Montmartru	Montmartr	k1gInSc2	Montmartr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
ji	on	k3xPp3gFnSc4	on
časopis	časopis	k1gInSc1	časopis
Télérama	Télérama	k1gNnSc4	Télérama
představil	představit	k5eAaPmAgInS	představit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ohromný	ohromný	k2eAgInSc4d1	ohromný
objev	objev	k1gInSc4	objev
<g/>
,	,	kIx,	,
veliký	veliký	k2eAgInSc4d1	veliký
hlas	hlas	k1gInSc4	hlas
a	a	k8xC	a
novou	nový	k2eAgFnSc4d1	nová
naději	naděje	k1gFnSc4	naděje
<g/>
"	"	kIx"	"
francouzského	francouzský	k2eAgInSc2d1	francouzský
šansonu	šanson	k1gInSc2	šanson
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
ZAZ	ZAZ	kA	ZAZ
<g/>
.	.	kIx.	.
</s>
<s>
Polovinu	polovina	k1gFnSc4	polovina
tvoří	tvořit	k5eAaImIp3nP	tvořit
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
napsala	napsat	k5eAaPmAgFnS	napsat
a	a	k8xC	a
složila	složit	k5eAaPmAgFnS	složit
sama	sám	k3xTgMnSc4	sám
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Trop	tropit	k5eAaImRp2nS	tropit
sensible	sensible	k6eAd1	sensible
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Les	les	k1gInSc1	les
passants	passants	k1gInSc1	passants
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Le	Le	k1gMnSc1	Le
long	long	k1gMnSc1	long
de	de	k?	de
la	la	k1gNnSc2	la
route	route	k5eAaPmIp2nP	route
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Prends	Prends	k1gInSc1	Prends
garde	garde	k1gFnSc1	garde
à	à	k?	à
ta	ten	k3xDgFnSc1	ten
langue	langue	k1gFnSc1	langue
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
J	J	kA	J
<g/>
'	'	kIx"	'
<g/>
aime	aime	k1gInSc1	aime
à	à	k?	à
nouveau	nouveaus	k1gInSc2	nouveaus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ni	on	k3xPp3gFnSc4	on
oui	oui	k?	oui
ni	on	k3xPp3gFnSc4	on
non	non	k?	non
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kerredine	Kerredinout	k5eAaPmIp3nS	Kerredinout
Soltani	Soltan	k1gMnPc1	Soltan
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
produkoval	produkovat	k5eAaImAgMnS	produkovat
a	a	k8xC	a
složil	složit	k5eAaPmAgMnS	složit
píseň	píseň	k1gFnSc4	píseň
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
gypsy	gyps	k1gInPc4	gyps
jazz	jazz	k1gInSc1	jazz
-	-	kIx~	-
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
veux	veux	k1gInSc1	veux
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
desce	deska	k1gFnSc6	deska
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgFnPc4d1	zmíněná
písně	píseň	k1gFnPc4	píseň
Rafaela	Rafaela	k1gFnSc1	Rafaela
"	"	kIx"	"
<g/>
Éblouie	Éblouie	k1gFnSc1	Éblouie
par	para	k1gFnPc2	para
la	la	k1gNnSc1	la
nuit	nuit	k1gMnSc1	nuit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Port	port	k1gInSc1	port
Coton	Coton	k1gInSc1	Coton
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
Fée	Fée	k1gFnSc2	Fée
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
Zaz	Zaz	k1gFnPc1	Zaz
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
na	na	k7c6	na
albu	album	k1gNnSc6	album
Toby	Toba	k1gFnSc2	Toba
Dammit	Dammita	k1gFnPc2	Dammita
et	et	k?	et
Bruce	Bruce	k1gMnSc1	Bruce
Cherbit	Cherbit	k1gMnSc1	Cherbit
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Raphaël	Raphaël	k1gMnSc1	Raphaël
Haroche	Haroch	k1gFnSc2	Haroch
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
Lafage	Lafag	k1gFnSc2	Lafag
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
banjo	banjo	k1gNnSc1	banjo
<g/>
,	,	kIx,	,
harmonium	harmonium	k1gNnSc1	harmonium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Manuel	Manuel	k1gMnSc1	Manuel
Marchè	Marchè	k1gMnSc1	Marchè
a	a	k8xC	a
Antoine	Antoin	k1gInSc5	Antoin
Reininger	Reininger	k1gInSc1	Reininger
(	(	kIx(	(
<g/>
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Germain	Germain	k2eAgInSc1d1	Germain
Guyot	Guyot	k1gInSc1	Guyot
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Aaron	Aaron	k1gMnSc1	Aaron
(	(	kIx(	(
<g/>
saxofon	saxofon	k1gInSc1	saxofon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alban	Alban	k1gMnSc1	Alban
Sautour	Sautour	k1gMnSc1	Sautour
(	(	kIx(	(
<g/>
zvuk	zvuk	k1gInSc1	zvuk
a	a	k8xC	a
kytary	kytara	k1gFnPc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
produkcí	produkce	k1gFnSc7	produkce
Caramba	caramba	k0	caramba
a	a	k8xC	a
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Sony	Sony	kA	Sony
ATV	ATV	kA	ATV
<g/>
,	,	kIx,	,
přijala	přijmout	k5eAaPmAgFnS	přijmout
pozvání	pozvání	k1gNnSc4	pozvání
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
televizí	televize	k1gFnPc2	televize
a	a	k8xC	a
vešla	vejít	k5eAaPmAgFnS	vejít
tím	ten	k3xDgNnSc7	ten
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
k	k	k7c3	k
širšímu	široký	k2eAgNnSc3d2	širší
publiku	publikum	k1gNnSc3	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
veux	veux	k1gInSc1	veux
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
o	o	k7c4	o
pohrdání	pohrdání	k1gNnSc4	pohrdání
konzumní	konzumní	k2eAgFnSc7d1	konzumní
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
figuruje	figurovat	k5eAaImIp3nS	figurovat
na	na	k7c6	na
předních	přední	k2eAgNnPc6d1	přední
místech	místo	k1gNnPc6	místo
hitparád	hitparáda	k1gFnPc2	hitparáda
a	a	k8xC	a
klip	klip	k1gInSc1	klip
se	se	k3xPyFc4	se
vysílá	vysílat	k5eAaImIp3nS	vysílat
celé	celý	k2eAgNnSc1d1	celé
léto	léto	k1gNnSc1	léto
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
TF1	TF1	k1gFnSc6	TF1
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
hudebních	hudební	k2eAgInPc6d1	hudební
kanálech	kanál	k1gInPc6	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
její	její	k3xOp3gNnSc1	její
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c4	mezi
16	[number]	k4	16
top	topit	k5eAaImRp2nS	topit
alby	alba	k1gFnSc2	alba
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Rochelle	Rochelle	k1gFnSc2	Rochelle
<g/>
,	,	kIx,	,
festivaly	festival	k1gInPc4	festival
v	v	k7c4	v
Montauban	Montauban	k1gInSc4	Montauban
<g/>
,	,	kIx,	,
Saint-Ouen	Saint-Ouen	k1gInSc4	Saint-Ouen
<g/>
,	,	kIx,	,
Châteauroux	Châteauroux	k1gInSc4	Châteauroux
<g/>
,	,	kIx,	,
Landerneau	Landerneaa	k1gFnSc4	Landerneaa
<g/>
,	,	kIx,	,
Fécamp	Fécamp	k1gInSc4	Fécamp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
ji	on	k3xPp3gFnSc4	on
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
Guillaume	Guillaum	k1gInSc5	Guillaum
Juhel	Juhel	k1gInSc1	Juhel
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mathieu	Mathiea	k1gFnSc4	Mathiea
Verlot	Verlota	k1gFnPc2	Verlota
(	(	kIx(	(
<g/>
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
Pimienta	Pimiento	k1gNnSc2	Pimiento
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
)	)	kIx)	)
a	a	k8xC	a
Augustin	Augustin	k1gMnSc1	Augustin
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Assignies	Assignies	k1gInSc1	Assignies
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpívala	zpívat	k5eAaImAgFnS	zpívat
také	také	k9	také
na	na	k7c4	na
Francofolies	Francofolies	k1gInSc4	Francofolies
de	de	k?	de
Montréal	Montréal	k1gInSc1	Montréal
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c4	v
Monthey	Monthea	k1gFnPc4	Monthea
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
,	,	kIx,	,
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
čelních	čelní	k2eAgNnPc6d1	čelní
místech	místo	k1gNnPc6	místo
hitparád	hitparáda	k1gFnPc2	hitparáda
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
i	i	k8xC	i
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
měla	mít	k5eAaImAgFnS	mít
ohromný	ohromný	k2eAgInSc4d1	ohromný
úspěch	úspěch	k1gInSc4	úspěch
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
malým	malý	k2eAgInPc3d1	malý
sálům	sál	k1gInPc3	sál
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
oceněno	ocenit	k5eAaPmNgNnS	ocenit
dvojitou	dvojitý	k2eAgFnSc7d1	dvojitá
platinovou	platinový	k2eAgFnSc7d1	platinová
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Dostala	dostat	k5eAaPmAgFnS	dostat
cenu	cena	k1gFnSc4	cena
jako	jako	k8xS	jako
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
domácí	domácí	k2eAgNnSc4d1	domácí
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
agenturního	agenturní	k2eAgInSc2d1	agenturní
průzkumu	průzkum	k1gInSc2	průzkum
byla	být	k5eAaImAgFnS	být
nejvyhledávanější	vyhledávaný	k2eAgFnSc7d3	nejvyhledávanější
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejoblíbenější	oblíbený	k2eAgFnSc4d3	nejoblíbenější
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
diamantovou	diamantový	k2eAgFnSc4d1	Diamantová
desku	deska	k1gFnSc4	deska
za	za	k7c2	za
prodaných	prodaný	k2eAgNnPc2d1	prodané
500	[number]	k4	500
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
televize	televize	k1gFnSc1	televize
TF1	TF1	k1gFnSc1	TF1
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
veux	veux	k1gInSc1	veux
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
písní	píseň	k1gFnSc7	píseň
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
umělkyní	umělkyně	k1gFnSc7	umělkyně
roku	rok	k1gInSc2	rok
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgFnPc1d1	Nové
scény	scéna	k1gFnPc1	scéna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zvolena	zvolit	k5eAaPmNgFnS	zvolit
byla	být	k5eAaImAgFnS	být
čtenáři	čtenář	k1gMnPc7	čtenář
časopisu	časopis	k1gInSc2	časopis
Paris	Paris	k1gMnSc1	Paris
Match	Match	k1gMnSc1	Match
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
dostala	dostat	k5eAaPmAgFnS	dostat
cenu	cena	k1gFnSc4	cena
Echo	echo	k1gNnSc4	echo
za	za	k7c4	za
"	"	kIx"	"
<g/>
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
objev	objev	k1gInSc4	objev
na	na	k7c6	na
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Québecu	Québecum	k1gNnSc6	Québecum
cenu	cena	k1gFnSc4	cena
Félix	Félix	k1gInSc1	Félix
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
frankofonní	frankofonní	k2eAgFnSc1d1	frankofonní
umělkyně	umělkyně	k1gFnSc1	umělkyně
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
pozval	pozvat	k5eAaPmAgMnS	pozvat
jako	jako	k9	jako
hosta	host	k1gMnSc4	host
pro	pro	k7c4	pro
duet	duet	k1gInSc4	duet
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
La	la	k1gNnSc6	la
radio	radio	k1gNnSc4	radio
qui	qui	k?	qui
chante	chant	k1gInSc5	chant
<g/>
"	"	kIx"	"
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
album	album	k1gNnSc4	album
Saison	Saisona	k1gFnPc2	Saisona
Yves	Yvesa	k1gFnPc2	Yvesa
Jamait	Jamait	k2eAgInSc1d1	Jamait
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
složila	složit	k5eAaPmAgFnS	složit
a	a	k8xC	a
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Cœ	Cœ	k1gFnSc4	Cœ
<g/>
"	"	kIx"	"
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
Howard	Howarda	k1gFnPc2	Howarda
Shora	shora	k6eAd1	shora
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Hugo	Hugo	k1gMnSc1	Hugo
Cabret	Cabret	k1gMnSc1	Cabret
Martina	Martin	k1gMnSc4	Martin
Scorseseho	Scorsese	k1gMnSc4	Scorsese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
vydala	vydat	k5eAaPmAgFnS	vydat
ZAZ	ZAZ	kA	ZAZ
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
Recto	Recta	k1gFnSc5	Recta
Verso	Versa	k1gFnSc5	Versa
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
první	první	k4xOgFnSc4	první
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
ira	ira	k?	ira
<g/>
"	"	kIx"	"
složil	složit	k5eAaPmAgMnS	složit
Kerredine	Kerredin	k1gInSc5	Kerredin
Soltani	Soltaň	k1gFnSc3	Soltaň
<g/>
.	.	kIx.	.
</s>
<s>
Frédéric	Frédéric	k1gMnSc1	Frédéric
Volovitch	Volovitch	k1gMnSc1	Volovitch
(	(	kIx(	(
<g/>
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Volo	Volo	k6eAd1	Volo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Buridane	Buridan	k1gMnSc5	Buridan
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
albu	album	k1gNnSc6	album
autorsky	autorsky	k6eAd1	autorsky
podíleli	podílet	k5eAaImAgMnP	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
nejméně	málo	k6eAd3	málo
310	[number]	k4	310
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
250	[number]	k4	250
000	[number]	k4	000
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
asi	asi	k9	asi
560	[number]	k4	560
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
ZAZ	ZAZ	kA	ZAZ
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ji	on	k3xPp3gFnSc4	on
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
hudebníci	hudebník	k1gMnPc1	hudebník
Jean	Jean	k1gMnSc1	Jean
Philippe	Philipp	k1gMnSc5	Philipp
Motte	Mott	k1gMnSc5	Mott
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc7d1	bicí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Denis	Denisa	k1gFnPc2	Denisa
Clavaizolle	Clavaizolle	k1gFnSc2	Clavaizolle
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Benoît	Benoît	k1gMnSc1	Benoît
Simon	Simon	k1gMnSc1	Simon
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ilan	Ilan	k1gNnSc1	Ilan
Abou	Abous	k1gInSc2	Abous
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
a	a	k8xC	a
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thierry	Thierr	k1gInPc1	Thierr
Faure	Faur	k1gInSc5	Faur
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
akordeon	akordeon	k1gInSc1	akordeon
a	a	k8xC	a
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Guillaume	Guillaum	k1gInSc5	Guillaum
Juhel	Juhel	k1gInSc1	Juhel
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Éblouie	Éblouie	k1gFnSc1	Éblouie
par	para	k1gFnPc2	para
la	la	k1gNnSc1	la
nuit	nuit	k1gMnSc1	nuit
<g/>
"	"	kIx"	"
použita	použit	k2eAgFnSc1d1	použita
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Dead	Dead	k1gMnSc1	Dead
Man	Man	k1gMnSc1	Man
Down	Down	k1gMnSc1	Down
s	s	k7c7	s
Colinem	Colin	k1gMnSc7	Colin
Farrellem	Farrell	k1gMnSc7	Farrell
<g/>
.	.	kIx.	.
</s>
<s>
Nahrála	nahrát	k5eAaPmAgFnS	nahrát
i	i	k9	i
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
film	film	k1gInSc4	film
Bela	Bela	k1gFnSc1	Bela
a	a	k8xC	a
Sebastián	Sebastián	k1gMnSc1	Sebastián
(	(	kIx(	(
<g/>
Belle	bell	k1gInSc5	bell
et	et	k?	et
Sébastien	Sébastien	k1gInSc1	Sébastien
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vyjela	vyjet	k5eAaPmAgFnS	vyjet
na	na	k7c4	na
ohromné	ohromný	k2eAgNnSc4d1	ohromné
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
pět	pět	k4xCc4	pět
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
,	,	kIx,	,
pokaždé	pokaždé	k6eAd1	pokaždé
bylo	být	k5eAaImAgNnS	být
naprosto	naprosto	k6eAd1	naprosto
vyprodáno	vyprodat	k5eAaPmNgNnS	vyprodat
od	od	k7c2	od
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
po	po	k7c6	po
Rusko	Rusko	k1gNnSc1	Rusko
či	či	k8xC	či
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
Srbsku	Srbsko	k1gNnSc6	Srbsko
a	a	k8xC	a
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
objela	objet	k5eAaPmAgFnS	objet
navíc	navíc	k6eAd1	navíc
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
udělala	udělat	k5eAaPmAgFnS	udělat
i	i	k9	i
mini-turné	miniurný	k2eAgFnPc4d1	mini-turný
po	po	k7c6	po
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
,	,	kIx,	,
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
Uruguayi	Uruguay	k1gFnSc6	Uruguay
a	a	k8xC	a
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgNnSc7	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
cenu	cena	k1gFnSc4	cena
Echo	echo	k1gNnSc1	echo
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
rock-pop	rockop	k1gInSc1	rock-pop
umělkyně	umělkyně	k1gFnSc1	umělkyně
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Zaz	Zaz	k1gFnSc1	Zaz
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Recto	Recto	k1gNnSc1	Recto
Verso	Versa	k1gFnSc5	Versa
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Paris	Paris	k1gMnSc1	Paris
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Sur	Sur	k1gFnSc3	Sur
la	la	k1gNnSc2	la
route	route	k5eAaPmIp2nP	route
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
veux	veux	k1gInSc4	veux
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Le	Le	k1gMnSc1	Le
Long	Long	k1gMnSc1	Long
de	de	k?	de
la	la	k1gNnSc2	la
route	route	k5eAaPmIp2nP	route
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
fée	fée	k?	fée
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Éblouie	Éblouie	k1gFnSc1	Éblouie
par	para	k1gFnPc2	para
la	la	k1gNnSc1	la
nuit	nuit	k1gMnSc1	nuit
<g/>
"	"	kIx"	"
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
ira	ira	k?	ira
<g/>
"	"	kIx"	"
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Comme	Comm	k1gMnSc5	Comm
ci	ci	k0	ci
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
comme	commat	k5eAaPmIp3nS	commat
ça	ça	k?	ça
<g/>
"	"	kIx"	"
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Si	se	k3xPyFc3	se
<g/>
"	"	kIx"	"
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Gamine	gamin	k1gMnSc5	gamin
<g/>
"	"	kIx"	"
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Si	se	k3xPyFc3	se
jamais	jamais	k1gInSc4	jamais
j	j	k?	j
<g/>
'	'	kIx"	'
<g/>
oublie	oublie	k1gFnSc1	oublie
<g/>
"	"	kIx"	"
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Le	Le	k1gFnSc1	Le
chemin	chemin	k1gInSc1	chemin
de	de	k?	de
pierre	pierr	k1gInSc5	pierr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Nolwenn	Nolwenn	k1gInSc1	Nolwenn
Leroy	Leroa	k1gFnSc2	Leroa
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Dutronc	Dutronc	k1gInSc1	Dutronc
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
