<s>
Dat	datum	k1gNnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
postavě	postava	k1gFnSc6
ze	z	k7c2
seriálu	seriál	k1gInSc2
Star	star	k1gFnSc2
Trek	Treka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
DAT	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Data	datum	k1gNnPc1
Nadporučík	nadporučík	k1gMnSc1
Dat	datum	k1gNnPc2
Ztvárnil	ztvárnit	k5eAaPmAgMnS
<g/>
/	/	kIx~
<g/>
a	a	k8xC
</s>
<s>
Brent	Brent	k?
Spiner	Spiner	k1gInSc1
Informace	informace	k1gFnSc1
Druh	druh	k1gMnSc1
</s>
<s>
android	android	k1gInSc1
Planeta	planeta	k1gFnSc1
původu	původ	k1gInSc2
</s>
<s>
Omicron	Omicron	k1gMnSc1
Theta	Thet	k1gMnSc2
Pohlaví	pohlaví	k1gNnSc2
</s>
<s>
muž	muž	k1gMnSc1
Hodnost	hodnost	k1gFnSc1
</s>
<s>
nadporučík	nadporučík	k1gMnSc1
Rodina	rodina	k1gFnSc1
</s>
<s>
Noonien	Noonien	k2eAgMnSc1d1
Soong	Soong	k1gMnSc1
(	(	kIx(
<g/>
tvůrce	tvůrce	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lore	Lore	k1gFnSc1
a	a	k8xC
B-4	B-4	k1gFnSc1
(	(	kIx(
<g/>
starší	starý	k2eAgInPc1d2
modely	model	k1gInPc1
androida	android	k1gMnSc2
<g/>
,	,	kIx,
"	"	kIx"
<g/>
bratři	bratr	k1gMnPc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
Přidělení	přidělení	k1gNnSc1
</s>
<s>
USS	USS	kA
Enterprise-D	Enterprise-D	k1gFnSc1
Pozice	pozice	k1gFnSc1
</s>
<s>
vědecký	vědecký	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
druhý	druhý	k4xOgInSc1
důstojník	důstojník	k1gMnSc1
Strana	strana	k1gFnSc1
</s>
<s>
Spojená	spojený	k2eAgFnSc1d1
federace	federace	k1gFnSc1
planet	planeta	k1gFnPc2
Dabing	dabing	k1gInSc4
</s>
<s>
Bohdan	Bohdan	k1gMnSc1
Tůma	Tůma	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dat	datum	k1gNnPc2
(	(	kIx(
<g/>
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
originále	originál	k1gInSc6
Data	datum	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
fiktivní	fiktivní	k2eAgFnSc1d1
postava	postava	k1gFnSc1
v	v	k7c6
seriálech	seriál	k1gInPc6
a	a	k8xC
filmech	film	k1gInPc6
Star	star	k1gFnSc2
Treku	Trek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
androida	android	k1gMnSc4
<g/>
,	,	kIx,
nadporučíka	nadporučík	k1gMnSc4
ve	v	k7c6
funkci	funkce	k1gFnSc6
operačního	operační	k2eAgMnSc4d1
důstojníka	důstojník	k1gMnSc4
na	na	k7c6
palubě	paluba	k1gFnSc6
USS	USS	kA
Enterprise-	Enterprise-	k1gFnSc2
<g/>
D.	D.	kA
Znám	znát	k5eAaImIp1nS
je	on	k3xPp3gFnPc4
především	především	k9
ze	z	k7c2
seriálu	seriál	k1gInSc2
Star	Star	kA
Trek	Trek	k1gMnSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
ve	v	k7c6
čtyřech	čtyři	k4xCgInPc6
navazujících	navazující	k2eAgInPc6d1
celovečerních	celovečerní	k2eAgInPc6d1
filmech	film	k1gInPc6
a	a	k8xC
v	v	k7c6
seriálu	seriál	k1gInSc6
Star	Star	kA
Trek	Trek	k1gMnSc1
<g/>
:	:	kIx,
Picard	Picard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztvárnil	ztvárnit	k5eAaPmAgMnS
jej	on	k3xPp3gInSc4
Brent	Brent	k?
Spiner	Spiner	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
postavy	postava	k1gFnSc2
</s>
<s>
Tvůrce	tvůrce	k1gMnSc4
Star	Star	kA
Treku	Treka	k1gMnSc4
<g/>
,	,	kIx,
Gene	gen	k1gInSc5
Roddenberry	Roddenberr	k1gInPc1
chtěl	chtít	k5eAaImAgMnS
do	do	k7c2
posádky	posádka	k1gFnSc2
seriálu	seriál	k1gInSc2
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
začlenit	začlenit	k5eAaPmF
androida	android	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
od	od	k7c2
lidí	člověk	k1gMnPc2
odlišný	odlišný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
odlišnost	odlišnost	k1gFnSc4
měla	mít	k5eAaImAgFnS
<g/>
,	,	kIx,
kromě	kromě	k7c2
barvy	barva	k1gFnSc2
kůže	kůže	k1gFnSc2
<g/>
,	,	kIx,
zařídit	zařídit	k5eAaPmF
holá	holý	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brent	Brent	k?
Spiner	Spinra	k1gFnPc2
z	z	k7c2
toho	ten	k3xDgInSc2
neměl	mít	k5eNaImAgMnS
radost	radost	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
ochoten	ochoten	k2eAgMnSc1d1
se	se	k3xPyFc4
Roddenberrymu	Roddenberrym	k1gInSc3
podřídit	podřídit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
změnil	změnit	k5eAaPmAgInS
nepřímo	přímo	k6eNd1
až	až	k9
Patrick	Patrick	k1gMnSc1
Stewart	Stewart	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
jako	jako	k9
kapitán	kapitán	k1gMnSc1
Jean-Luc	Jean-Luc	k1gFnSc4
Picard	Picarda	k1gFnPc2
měl	mít	k5eAaImAgMnS
nosit	nosit	k5eAaImF
paruku	paruka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
jej	on	k3xPp3gMnSc4
ovšem	ovšem	k9
Roddenberry	Roddenberra	k1gFnPc1
viděl	vidět	k5eAaImAgInS
na	na	k7c6
první	první	k4xOgFnSc6
zkoušce	zkouška	k1gFnSc6
usoudil	usoudit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
paruka	paruka	k1gFnSc1
nebude	být	k5eNaImBp3nS
dobrý	dobrý	k2eAgInSc1d1
nápad	nápad	k1gInSc1
a	a	k8xC
protože	protože	k8xS
jeden	jeden	k4xCgMnSc1
plešatý	plešatý	k2eAgMnSc1d1
člen	člen	k1gMnSc1
posádky	posádka	k1gFnSc2
stačí	stačit	k5eAaBmIp3nS
<g/>
,	,	kIx,
Spiner	Spiner	k1gMnSc1
si	se	k3xPyFc3
mohl	moct	k5eAaImAgMnS
vlasy	vlas	k1gInPc4
ponechat	ponechat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Maskér	maskér	k1gMnSc1
Michael	Michael	k1gMnSc1
Westmore	Westmor	k1gInSc5
musel	muset	k5eAaImAgMnS
na	na	k7c6
Spinerovi	Spiner	k1gMnSc6
udělat	udělat	k5eAaPmF
celkem	celkem	k6eAd1
36	#num#	k4
make-up	make-up	k1gInSc4
testů	test	k1gInPc2
<g/>
,	,	kIx,
než	než	k8xS
byl	být	k5eAaImAgInS
Roddenberry	Roddenberr	k1gInPc4
spokojen	spokojen	k2eAgMnSc1d1
s	s	k7c7
barvou	barva	k1gFnSc7
pleti	pleť	k1gFnSc2
pro	pro	k7c4
androida	android	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spiner	Spiner	k1gMnSc1
se	se	k3xPyFc4
inspiroval	inspirovat	k5eAaBmAgMnS
dvojicí	dvojice	k1gFnSc7
postav	postava	k1gFnPc2
pro	pro	k7c4
ztvárnění	ztvárnění	k1gNnSc4
androida	android	k1gMnSc2
Data	datum	k1gNnSc2
v	v	k7c6
seriálu	seriál	k1gInSc6
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
byl	být	k5eAaImAgInS
Pinocchio	Pinocchio	k1gNnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
jej	on	k3xPp3gMnSc4
i	i	k9
označil	označit	k5eAaPmAgMnS
komandér	komandér	k1gMnSc1
William	William	k1gInSc4
Riker	Rikero	k1gNnPc2
hned	hned	k6eAd1
v	v	k7c6
první	první	k4xOgFnSc6
epizodě	epizoda	k1gFnSc6
seriálu	seriál	k1gInSc2
a	a	k8xC
druhým	druhý	k4xOgInSc7
byla	být	k5eAaImAgFnS
postava	postava	k1gFnSc1
Roye	Roy	k1gMnSc2
Batty	Batta	k1gMnSc2
z	z	k7c2
filmu	film	k1gInSc2
Blade	Blad	k1gInSc5
Runner	Runnero	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgFnSc1d1
myšlenka	myšlenka	k1gFnSc1
</s>
<s>
Nápad	nápad	k1gInSc1
vsadit	vsadit	k5eAaPmF
do	do	k7c2
posádky	posádka	k1gFnSc2
androida	android	k1gMnSc2
Roddneberry	Roddneberra	k1gMnSc2
převzal	převzít	k5eAaPmAgMnS
z	z	k7c2
díla	dílo	k1gNnSc2
The	The	k1gFnSc2
Questor	Questor	k1gMnSc1
Tapes	Tapes	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgNnSc6,k3yRgNnSc6,k3yQgNnSc6
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
spolu	spolu	k6eAd1
s	s	k7c7
Genem	gen	k1gInSc7
Coonem	Coon	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robert	Robert	k1gMnSc1
Foxworth	Foxworth	k1gMnSc1
zde	zde	k6eAd1
hrál	hrát	k5eAaImAgMnS
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
Majel	Majel	k1gMnSc1
Barrett	Barrett	k1gMnSc1
a	a	k8xC
Walter	Walter	k1gMnSc1
Koenig	Koenig	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
šlo	jít	k5eAaImAgNnS
o	o	k7c4
příběhy	příběh	k1gInPc4
androida	android	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
poznávat	poznávat	k5eAaImF
lidské	lidský	k2eAgNnSc4d1
chování	chování	k1gNnSc4
<g/>
,	,	kIx,
lásku	láska	k1gFnSc4
<g/>
,	,	kIx,
city	cit	k1gInPc4
a	a	k8xC
různé	různý	k2eAgFnPc4d1
emoce	emoce	k1gFnPc4
a	a	k8xC
příčiny	příčina	k1gFnPc4
lidského	lidský	k2eAgNnSc2d1
chování	chování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Datova	Datův	k2eAgFnSc1d1
historie	historie	k1gFnSc1
</s>
<s>
Dat	Dat	k1gMnSc1
byl	být	k5eAaImAgInS
sestaven	sestavit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
unikátní	unikátní	k2eAgInSc1d1
android	android	k1gInSc1
s	s	k7c7
pozitronovým	pozitronový	k2eAgInSc7d1
mozkem	mozek	k1gInSc7
a	a	k8xC
poprvé	poprvé	k6eAd1
aktivován	aktivován	k2eAgInSc1d1
roku	rok	k1gInSc2
2336	#num#	k4
doktorem	doktor	k1gMnSc7
Soongem	Soong	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
partnerkou	partnerka	k1gFnSc7
Juliannou	Julianný	k2eAgFnSc4d1
Tainerovou	Tainerová	k1gFnSc4
na	na	k7c6
planetě	planeta	k1gFnSc6
Omicron	Omicron	k1gMnSc1
Theta	Theta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zničení	zničení	k1gNnSc6
všeho	všecek	k3xTgNnSc2
živého	živé	k1gNnSc2
Krystalickou	krystalický	k2eAgFnSc7d1
bytostí	bytost	k1gFnSc7
byl	být	k5eAaImAgInS
nalezen	naleznout	k5eAaPmNgInS,k5eAaBmNgInS
lodí	loď	k1gFnSc7
USS	USS	kA
Tripoli	Tripole	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2341	#num#	k4
se	se	k3xPyFc4
dostává	dostávat	k5eAaImIp3nS
na	na	k7c6
akademii	akademie	k1gFnSc6
hvězdné	hvězdný	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
po	po	k7c6
4	#num#	k4
letech	léto	k1gNnPc6
úspěšně	úspěšně	k6eAd1
absolvuje	absolvovat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
24	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
slouží	sloužit	k5eAaImIp3nS
na	na	k7c6
federační	federační	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
USS	USS	kA
Trieste	Triest	k1gInSc5
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2363	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
převelen	převelen	k2eAgInSc1d1
a	a	k8xC
vlajkovou	vlajkový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
federace	federace	k1gFnSc2
USS	USS	kA
Enterprise	Enterprise	k1gFnSc2
D	D	kA
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
velení	velení	k1gNnSc4
přebírá	přebírat	k5eAaImIp3nS
kapitán	kapitán	k1gMnSc1
Jean-Luc	Jean-Luc	k1gFnSc4
Picard	Picarda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
dobu	doba	k1gFnSc4
služby	služba	k1gFnSc2
na	na	k7c6
Enterprise	Enterpris	k1gInSc6
objevuje	objevovat	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc2
prvního	první	k4xOgMnSc2
bratra	bratr	k1gMnSc2
Lora	Lora	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
dokonalejší	dokonalý	k2eAgFnSc1d2
v	v	k7c6
oblasti	oblast	k1gFnSc6
lidského	lidský	k2eAgNnSc2d1
chápání	chápání	k1gNnSc2
a	a	k8xC
citů	cit	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
rovněž	rovněž	k9
oplývá	oplývat	k5eAaImIp3nS
zlými	zlý	k2eAgInPc7d1
úmysly	úmysl	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
během	během	k7c2
služby	služba	k1gFnSc2
sestaví	sestavit	k5eAaPmIp3nS
Lal	Lal	k1gFnSc4
–	–	k?
umělou	umělý	k2eAgFnSc4d1
dívku	dívka	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
dceru	dcera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Experiment	experiment	k1gInSc1
se	se	k3xPyFc4
však	však	k9
nevydařil	vydařit	k5eNaPmAgInS
a	a	k8xC
po	po	k7c6
destabilizaci	destabilizace	k1gFnSc6
pozitronového	pozitronový	k2eAgInSc2d1
mozku	mozek	k1gInSc2
Lal	Lal	k1gFnSc2
umírá	umírat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
také	také	k9
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
dalším	další	k2eAgInSc7d1
robotem	robot	k1gInSc7
s	s	k7c7
pozitronovým	pozitronový	k2eAgInSc7d1
mozkem	mozek	k1gInSc7
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yRgMnSc4,k3yIgMnSc4
doktor	doktor	k1gMnSc1
Soong	Soong	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
jako	jako	k9
naprostou	naprostý	k2eAgFnSc4d1
kopii	kopie	k1gFnSc4
tragicky	tragicky	k6eAd1
zemřelé	zemřelý	k2eAgFnSc2d1
Julianny	Julianna	k1gFnSc2
Tainerové	Tainerová	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgMnSc2
také	také	k6eAd1
přenesl	přenést	k5eAaPmAgMnS
veškeré	veškerý	k3xTgNnSc4
její	její	k3xOp3gNnSc4
vědomí	vědomí	k1gNnSc4
a	a	k8xC
paměť	paměť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
2372	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
zničení	zničení	k1gNnSc6
lodi	loď	k1gFnSc2
je	být	k5eAaImIp3nS
převelen	převelet	k5eAaPmNgInS
na	na	k7c4
novou	nový	k2eAgFnSc4d1
Enterprise	Enterprise	k1gFnPc4
E	E	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
se	s	k7c7
stejnou	stejný	k2eAgFnSc7d1
posádkou	posádka	k1gFnSc7
a	a	k8xC
připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
převzít	převzít	k5eAaPmF
pozici	pozice	k1gFnSc4
prvního	první	k4xOgMnSc2
důstojníka	důstojník	k1gMnSc2
po	po	k7c6
odcházejícím	odcházející	k2eAgMnSc6d1
komandérovi	komandér	k1gMnSc6
Rikerovi	Riker	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
službě	služba	k1gFnSc6
na	na	k7c6
této	tento	k3xDgFnSc6
lodi	loď	k1gFnSc6
objevuje	objevovat	k5eAaImIp3nS
roku	rok	k1gInSc2
2379	#num#	k4
svého	svůj	k3xOyFgMnSc4
dalšího	další	k2eAgMnSc4d1
bratra	bratr	k1gMnSc4
B-	B-	k1gMnSc4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
zase	zase	k9
jasně	jasně	k6eAd1
zaostalejší	zaostalý	k2eAgFnSc1d2
oproti	oproti	k7c3
Datovi	Data	k1gMnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
zjevně	zjevně	k6eAd1
z	z	k7c2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
nemohl	moct	k5eNaImAgMnS
získávat	získávat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dat	datum	k1gNnPc2
se	se	k3xPyFc4
proto	proto	k8xC
rozhodne	rozhodnout	k5eAaPmIp3nS
přehrát	přehrát	k5eAaPmF
do	do	k7c2
B-4	B-4	k1gFnPc2
svou	svůj	k3xOyFgFnSc4
paměť	paměť	k1gFnSc4
včetně	včetně	k7c2
všech	všecek	k3xTgInPc2
vytvořených	vytvořený	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
je	být	k5eAaImIp3nS
Dat	datum	k1gNnPc2
zničen	zničit	k5eAaPmNgInS
při	při	k7c6
explozi	exploze	k1gFnSc6
Romulanské	Romulanský	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
stihne	stihnout	k5eAaPmIp3nS
zachránit	zachránit	k5eAaPmF
kapitána	kapitán	k1gMnSc4
Picarda	Picard	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dat	datum	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Data	datum	k1gNnPc1
na	na	k7c4
Star	Star	kA
Trek	Trek	k1gInSc4
wiki	wiki	k1gNnSc2
Memory	Memora	k1gFnSc2
Alpha	Alpha	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Data	datum	k1gNnPc1
na	na	k7c6
oficiálních	oficiální	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
StarTrek	StarTrky	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Dat	datum	k1gNnPc2
na	na	k7c4
CZkontinuum	CZkontinuum	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Star	Star	kA
Trek	Trek	k1gInSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
Seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Střetnutí	střetnutí	k1gNnSc1
na	na	k7c6
Farpointu	Farpoint	k1gInSc6
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bez	bez	k7c2
zábran	zábrana	k1gFnPc2
•	•	k?
Zákon	zákon	k1gInSc1
cti	čest	k1gFnSc2
•	•	k?
Nejzazší	zadní	k2eAgFnSc1d3
výspa	výspa	k1gFnSc1
•	•	k?
Kam	kam	k6eAd1
se	se	k3xPyFc4
dosud	dosud	k6eAd1
nikdo	nikdo	k3yNnSc1
nevydal	vydat	k5eNaPmAgMnS
•	•	k?
Opuštěný	opuštěný	k2eAgMnSc1d1
mezi	mezi	k7c7
námi	my	k3xPp1nPc7
•	•	k?
Spravedlnost	spravedlnost	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
•	•	k?
Hra	hra	k1gFnSc1
na	na	k7c4
schovávanou	schovávaná	k1gFnSc4
•	•	k?
Oáza	oáza	k1gFnSc1
•	•	k?
Poslední	poslední	k2eAgFnSc7d1
sbohem	sbohem	k0
•	•	k?
Bratři	bratřit	k5eAaImRp2nS
•	•	k?
Trosečníci	trosečník	k1gMnPc1
•	•	k?
Heslo	heslo	k1gNnSc4
•	•	k?
Honba	honba	k1gFnSc1
za	za	k7c7
mládím	mládí	k1gNnSc7
•	•	k?
Až	až	k9
se	se	k3xPyFc4
ucho	ucho	k1gNnSc1
utrhne	utrhnout	k5eAaPmIp3nS
•	•	k?
Rodná	rodný	k2eAgFnSc1d1
hrouda	hrouda	k1gFnSc1
•	•	k?
Plnoletost	plnoletost	k1gFnSc1
•	•	k?
Klingonské	Klingonský	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
•	•	k?
Arzenál	arzenál	k1gInSc1
svobody	svoboda	k1gFnSc2
•	•	k?
Symbióza	symbióza	k1gFnSc1
•	•	k?
Slupka	slupka	k1gFnSc1
všeho	všecek	k3xTgNnSc2
zla	zlo	k1gNnSc2
•	•	k?
Setkání	setkání	k1gNnSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
•	•	k?
Spiknutí	spiknutí	k1gNnSc2
•	•	k?
Neutrální	neutrální	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Dítě	dítě	k1gNnSc1
•	•	k?
Kde	kde	k6eAd1
vládne	vládnout	k5eAaImIp3nS
ticho	ticho	k1gNnSc1
•	•	k?
Jak	jak	k6eAd1
prosté	prostý	k2eAgNnSc1d1
<g/>
,	,	kIx,
drahý	drahý	k2eAgInSc1d1
Date	Date	k1gInSc1
•	•	k?
Nezkrotný	zkrotný	k2eNgInSc1d1
Okona	Okono	k1gNnSc2
•	•	k?
Tichý	Tichý	k1gMnSc1
jako	jako	k8xS,k8xC
šepot	šepot	k1gInSc1
•	•	k?
Schizofrenik	schizofrenik	k1gMnSc1
•	•	k?
Nepřirozený	přirozený	k2eNgInSc1d1
výběr	výběr	k1gInSc1
•	•	k?
Věc	věc	k1gFnSc1
cti	čest	k1gFnSc2
•	•	k?
Lidský	lidský	k2eAgInSc1d1
rozměr	rozměr	k1gInSc1
•	•	k?
Vladařka	vladařka	k1gFnSc1
•	•	k?
Nákaza	nákaza	k1gFnSc1
•	•	k?
Hotel	hotel	k1gInSc1
Royale	Royala	k1gFnSc3
•	•	k?
Čtverec	čtverec	k1gInSc1
času	čas	k1gInSc2
•	•	k?
Faktor	faktor	k1gMnSc1
Ikarus	Ikarus	k1gMnSc1
•	•	k?
Přátelé	přítel	k1gMnPc1
na	na	k7c4
dálku	dálka	k1gFnSc4
•	•	k?
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
Q	Q	kA
•	•	k?
Nástrahy	nástraha	k1gFnSc2
lékařské	lékařský	k2eAgFnSc2d1
péče	péče	k1gFnSc2
•	•	k?
Dodejte	dodat	k5eAaPmRp2nP
nám	my	k3xPp1nPc3
čerstvou	čerstvý	k2eAgFnSc4d1
krev	krev	k1gFnSc4
•	•	k?
Hon	hon	k1gInSc1
na	na	k7c4
muže	muž	k1gMnSc4
•	•	k?
Posel	posel	k1gMnSc1
•	•	k?
Špičkový	špičkový	k2eAgInSc4d1
výkon	výkon	k1gInSc4
•	•	k?
Šedé	Šedé	k2eAgInPc4d1
přízraky	přízrak	k1gInPc4
3	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Evoluce	evoluce	k1gFnSc1
•	•	k?
Vlastnosti	vlastnost	k1gFnSc2
velitele	velitel	k1gMnSc2
•	•	k?
Zničená	zničený	k2eAgFnSc1d1
planeta	planeta	k1gFnSc1
•	•	k?
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
pozoruje	pozorovat	k5eAaImIp3nS
pozorovatele	pozorovatel	k1gMnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
•	•	k?
Svazek	svazek	k1gInSc1
•	•	k?
Past	past	k1gFnSc1
•	•	k?
Nepřítel	nepřítel	k1gMnSc1
•	•	k?
Cena	cena	k1gFnSc1
•	•	k?
Faktor	faktor	k1gInSc1
pomsty	pomsta	k1gFnSc2
•	•	k?
Přeběhlík	přeběhlík	k1gMnSc1
•	•	k?
Štvanec	štvanec	k1gMnSc1
•	•	k?
Ušlechtilé	ušlechtilý	k2eAgInPc4d1
důvody	důvod	k1gInPc4
•	•	k?
Déjà	Déjà	k1gFnSc2
Q	Q	kA
•	•	k?
Věc	věc	k1gFnSc1
perspektivy	perspektiva	k1gFnSc2
•	•	k?
Enterprise	Enterprise	k1gFnSc1
včerejška	včerejšek	k1gInSc2
•	•	k?
Potomek	potomek	k1gMnSc1
•	•	k?
Hříchy	hřích	k1gInPc4
otce	otec	k1gMnSc2
•	•	k?
Poslušnost	poslušnost	k1gFnSc4
•	•	k?
Kapitánova	kapitánův	k2eAgFnSc1d1
dovolená	dovolená	k1gFnSc1
•	•	k?
Plecháč	plecháč	k1gInSc1
•	•	k?
Planá	Planá	k1gFnSc1
zábava	zábava	k1gFnSc1
•	•	k?
Co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
nejvíc	nejvíc	k6eAd1,k6eAd3
hraček	hračka	k1gFnPc2
•	•	k?
Sarek	Sarek	k1gInSc1
•	•	k?
Milostný	milostný	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
paní	paní	k1gFnSc2
Troi	Tro	k1gFnSc2
•	•	k?
Přeměna	přeměna	k1gFnSc1
•	•	k?
To	to	k9
nejlepší	dobrý	k2eAgFnSc1d3
z	z	k7c2
obou	dva	k4xCgInPc2
světů	svět	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
4	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
To	ten	k3xDgNnSc1
nejlepší	dobrý	k2eAgMnSc1d3
z	z	k7c2
obou	dva	k4xCgFnPc2
světů	svět	k1gInPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rodina	rodina	k1gFnSc1
•	•	k?
Bratři	bratr	k1gMnPc1
•	•	k?
Náhle	náhle	k6eAd1
člověkem	člověk	k1gMnSc7
•	•	k?
Vzpomeň	vzpomenout	k5eAaPmRp2nS
si	se	k3xPyFc3
na	na	k7c4
mě	já	k3xPp1nSc2
•	•	k?
Odkaz	odkaz	k1gInSc1
•	•	k?
Opět	opět	k6eAd1
spolu	spolu	k6eAd1
•	•	k?
Futurum	futurum	k1gNnSc1
Imperfektum	imperfektum	k1gNnSc1
•	•	k?
Poslední	poslední	k2eAgInSc1d1
úkol	úkol	k1gInSc1
•	•	k?
Ztráta	ztráta	k1gFnSc1
•	•	k?
Datův	Datův	k2eAgInSc1d1
den	den	k1gInSc1
•	•	k?
Zraněný	zraněný	k2eAgInSc1d1
•	•	k?
Každému	každý	k3xTgMnSc3
po	po	k7c6
zásluze	zásluha	k1gFnSc6
•	•	k?
Stopy	stopa	k1gFnSc2
•	•	k?
První	první	k4xOgInSc4
kontakt	kontakt	k1gInSc4
•	•	k?
Dítě	Dítě	k2eAgFnSc2d1
Galaxie	galaxie	k1gFnSc2
•	•	k?
Noční	noční	k2eAgFnSc2d1
můry	můra	k1gFnSc2
•	•	k?
Záhadná	záhadný	k2eAgNnPc4d1
zmizení	zmizení	k1gNnSc4
•	•	k?
N-tý	N-tý	k1gFnSc1
stupeň	stupeň	k1gInSc1
•	•	k?
Amorek	amorek	k1gMnSc1
•	•	k?
Polní	polní	k2eAgInSc1d1
soud	soud	k1gInSc1
•	•	k?
Půl	půl	k6eAd1
života	život	k1gInSc2
•	•	k?
Hostitel	hostitel	k1gMnSc1
•	•	k?
Vnitřní	vnitřní	k2eAgInSc1d1
zrak	zrak	k1gInSc1
•	•	k?
Teoreticky	teoreticky	k6eAd1
•	•	k?
Usmíření	usmíření	k1gNnPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
5	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Usmíření	usmíření	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Darmok	Darmok	k1gInSc1
•	•	k?
Podporučík	podporučík	k1gMnSc1
Ro	Ro	k1gFnSc2
•	•	k?
Marná	marný	k2eAgFnSc1d1
oběť	oběť	k1gFnSc1
•	•	k?
Neštěstí	štěstit	k5eNaImIp3nS
•	•	k?
Hra	hra	k1gFnSc1
•	•	k?
Sjednocení	sjednocení	k1gNnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Otázka	otázka	k1gFnSc1
času	čas	k1gInSc2
•	•	k?
Nové	Nové	k2eAgFnPc1d1
perspektivy	perspektiva	k1gFnPc1
•	•	k?
Můj	můj	k1gMnSc1
hrdina	hrdina	k1gMnSc1
•	•	k?
Násilník	násilník	k1gMnSc1
•	•	k?
Dokonalá	dokonalý	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Záhada	záhada	k1gFnSc1
•	•	k?
Z	z	k7c2
pozice	pozice	k1gFnSc2
síly	síla	k1gFnSc2
•	•	k?
Etika	etik	k1gMnSc2
•	•	k?
Vyděděnec	vyděděnec	k1gMnSc1
•	•	k?
Příčina	příčina	k1gFnSc1
a	a	k8xC
důsledek	důsledek	k1gInSc1
•	•	k?
Základní	základní	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
•	•	k?
Cena	cena	k1gFnSc1
života	život	k1gInSc2
•	•	k?
Ideální	ideální	k2eAgFnSc1d1
partnerka	partnerka	k1gFnSc1
•	•	k?
Vymyšlená	vymyšlený	k2eAgFnSc1d1
přítelkyně	přítelkyně	k1gFnSc1
•	•	k?
Já	já	k3xPp1nSc1
<g/>
,	,	kIx,
Borg	Borg	k1gMnSc1
•	•	k?
Další	další	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
•	•	k?
Vnitřní	vnitřní	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
•	•	k?
Šíp	šíp	k1gInSc1
času	čas	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
6	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Šíp	šíp	k1gInSc1
času	čas	k1gInSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Zóna	zóna	k1gFnSc1
strachu	strach	k1gInSc2
•	•	k?
Vyjednavač	vyjednavač	k1gMnSc1
•	•	k?
Host	host	k1gMnSc1
•	•	k?
Únosci	únosce	k1gMnSc3
•	•	k?
Slečna	slečna	k1gFnSc1
Q	Q	kA
•	•	k?
Rošťáci	rošťák	k1gMnPc1
•	•	k?
Tenkrát	tenkrát	k6eAd1
v	v	k7c4
Deadwoodu	Deadwooda	k1gFnSc4
•	•	k?
Kvalita	kvalita	k1gFnSc1
života	život	k1gInSc2
•	•	k?
Změna	změna	k1gFnSc1
velení	velení	k1gNnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Loď	loď	k1gFnSc1
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
láhvi	láhev	k1gFnSc6
•	•	k?
Aquiel	Aquiel	k1gInSc1
•	•	k?
Tvář	tvář	k1gFnSc1
nepřítele	nepřítel	k1gMnSc2
•	•	k?
Tapiserie	tapiserie	k1gFnSc2
•	•	k?
Dědictví	dědictví	k1gNnSc2
otců	otec	k1gMnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Moje	můj	k3xOp1gFnSc1
loď	loď	k1gFnSc1
•	•	k?
Lekce	lekce	k1gFnSc2
•	•	k?
Závod	závod	k1gInSc1
•	•	k?
Stav	stav	k1gInSc1
mysli	mysl	k1gFnSc2
•	•	k?
Podezření	podezření	k1gNnSc1
•	•	k?
Právoplatný	právoplatný	k2eAgMnSc1d1
dědic	dědic	k1gMnSc1
•	•	k?
Druhá	druhý	k4xOgFnSc1
šance	šance	k1gFnSc1
•	•	k?
Past	past	k1gFnSc1
v	v	k7c6
čase	čas	k1gInSc6
•	•	k?
Vpád	vpád	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
7	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Vpád	vpád	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vztahy	vztah	k1gInPc1
•	•	k?
Lidská	lidský	k2eAgFnSc1d1
sonda	sonda	k1gFnSc1
•	•	k?
Gambit	gambit	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vidiny	vidina	k1gFnSc2
•	•	k?
Temný	temný	k2eAgInSc1d1
kout	kout	k1gInSc1
•	•	k?
Spojení	spojení	k1gNnSc1
•	•	k?
Síla	síla	k1gFnSc1
přírody	příroda	k1gFnSc2
•	•	k?
Odkaz	odkaz	k1gInSc1
•	•	k?
Paralely	paralela	k1gFnSc2
•	•	k?
Pegasus	Pegasus	k1gMnSc1
•	•	k?
Domů	domů	k6eAd1
•	•	k?
Milenec	milenec	k1gMnSc1
•	•	k?
V	v	k7c6
podpalubí	podpalubí	k1gNnSc6
•	•	k?
Tvé	tvůj	k3xOp2gNnSc1
vlastní	vlastnit	k5eAaImIp3nS
já	já	k3xPp1nSc1
•	•	k?
Masky	maska	k1gFnSc2
•	•	k?
Cizíma	cizí	k2eAgNnPc7d1
očima	oko	k1gNnPc7
•	•	k?
Geneze	geneze	k1gFnSc2
•	•	k?
Konec	konec	k1gInSc1
cesty	cesta	k1gFnSc2
•	•	k?
Prvorozený	prvorozený	k2eAgInSc1d1
•	•	k?
Pokrevní	pokrevný	k1gMnPc1
pouta	pouto	k1gNnSc2
•	•	k?
Zrození	zrození	k1gNnSc2
•	•	k?
Preventivní	preventivní	k2eAgInSc4d1
úder	úder	k1gInSc4
•	•	k?
Všechno	všechen	k3xTgNnSc4
dobré	dobré	k1gNnSc4
<g/>
…	…	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
Generace	generace	k1gFnSc1
•	•	k?
První	první	k4xOgInSc1
kontakt	kontakt	k1gInSc1
•	•	k?
Vzpoura	vzpoura	k1gFnSc1
•	•	k?
Nemesis	Nemesis	k1gFnSc2
Prostředí	prostředí	k1gNnSc2
</s>
<s>
Postavy	postava	k1gFnPc1
</s>
<s>
Jean-Luc	Jean-Luc	k6eAd1
Picard	Picard	k1gInSc1
•	•	k?
William	William	k1gInSc1
Riker	Riker	k1gMnSc1
•	•	k?
Dat	datum	k1gNnPc2
•	•	k?
Beverly	Beverla	k1gFnSc2
Crusherová	Crusherová	k1gFnSc1
•	•	k?
Geordi	Geord	k1gMnPc1
La	la	k1gNnSc2
Forge	Forge	k1gFnPc2
•	•	k?
Deanna	Deann	k1gInSc2
Troi	Tro	k1gFnSc2
•	•	k?
Worf	Worf	k1gMnSc1
•	•	k?
Wesley	Weslea	k1gFnSc2
Crusher	Crushra	k1gFnPc2
•	•	k?
Taša	Taš	k1gInSc2
Jarová	Jarová	k1gFnSc1
•	•	k?
Katherine	Katherin	k1gInSc5
Pulaská	Pulaský	k2eAgFnSc1d1
•	•	k?
Guinan	Guinan	k1gMnSc1
•	•	k?
Miles	Miles	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Brien	Brien	k2eAgMnSc1d1
•	•	k?
Ro	Ro	k1gMnSc1
Laren	Laren	k2eAgMnSc1d1
•	•	k?
Q	Q	kA
Plavidla	plavidlo	k1gNnPc4
</s>
<s>
USS	USS	kA
Enterprise	Enterprise	k1gFnSc1
(	(	kIx(
<g/>
NCC-	NCC-	k1gFnSc1
<g/>
1701	#num#	k4
<g/>
-C	-C	k?
<g/>
)	)	kIx)
•	•	k?
USS	USS	kA
Enterprise	Enterprise	k1gFnSc2
(	(	kIx(
<g/>
NCC-	NCC-	k1gFnSc1
<g/>
1701	#num#	k4
<g/>
-D	-D	k?
<g/>
)	)	kIx)
•	•	k?
USS	USS	kA
Enterprise	Enterprise	k1gFnSc2
(	(	kIx(
<g/>
NCC-	NCC-	k1gFnSc1
<g/>
1701	#num#	k4
<g/>
-E	-E	k?
<g/>
)	)	kIx)
</s>
<s>
Knižní	knižní	k2eAgFnSc1d1
série	série	k1gFnSc1
<g/>
(	(	kIx(
<g/>
vydáno	vydat	k5eAaPmNgNnS
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
novelizace	novelizace	k1gFnSc1
</s>
<s>
Střetnutí	střetnutí	k1gNnSc1
na	na	k7c6
Farpointu	Farpoint	k1gInSc6
•	•	k?
Sjednocení	sjednocení	k1gNnSc2
•	•	k?
Střepy	střep	k1gInPc7
času	čas	k1gInSc2
•	•	k?
Vpád	vpád	k1gInSc1
•	•	k?
Nemesis	Nemesis	k1gFnSc2
číslované	číslovaný	k2eAgFnSc2d1
</s>
<s>
Loď	loď	k1gFnSc1
duchů	duch	k1gMnPc2
•	•	k?
Strážci	strážce	k1gMnSc3
míru	míra	k1gFnSc4
•	•	k?
Děti	dítě	k1gFnPc1
z	z	k7c2
Hamlinu	Hamlin	k1gInSc2
•	•	k?
Otázka	otázka	k1gFnSc1
bezpečnosti	bezpečnost	k1gFnSc2
•	•	k?
Zóna	zóna	k1gFnSc1
úderu	úder	k1gInSc2
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
moc	moc	k1gFnSc4
•	•	k?
Masky	maska	k1gFnSc2
•	•	k?
Čest	čest	k1gFnSc1
kapitánů	kapitán	k1gMnPc2
•	•	k?
Výkřik	výkřik	k1gInSc1
do	do	k7c2
tmy	tma	k1gFnSc2
•	•	k?
Prašť	praštit	k5eAaPmRp2nS
jako	jako	k8xC,k8xS
uhoď	uhodit	k5eAaPmRp2nS
•	•	k?
Gulliverovi	Gulliver	k1gMnSc3
chráněnci	chráněnec	k1gMnSc3
•	•	k?
Svět	svět	k1gInSc1
zkázy	zkáza	k1gFnSc2
•	•	k?
Oči	oko	k1gNnPc1
vidoucích	vidoucí	k2eAgFnPc2d1
•	•	k?
Vyhnanci	vyhnanec	k1gMnPc1
nečíslované	číslovaný	k2eNgFnSc2d1
</s>
<s>
Metamorfózy	metamorfóza	k1gFnPc1
•	•	k?
Vendetta	Vendetta	k1gMnSc1
•	•	k?
Smrtelné	smrtelný	k2eAgNnSc1d1
ohrožení	ohrožení	k1gNnSc1
•	•	k?
Imzadi	Imzad	k1gMnPc1
•	•	k?
Satanovo	Satanův	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
•	•	k?
Temné	temný	k2eAgNnSc1d1
zrcadlo	zrcadlo	k1gNnSc1
•	•	k?
Q	Q	kA
na	na	k7c6
druhou	druhý	k4xOgFnSc4
•	•	k?
Imzadi	Imzad	k1gMnPc1
II	II	kA
<g/>
:	:	kIx,
Trojúhelník	trojúhelník	k1gInSc1
•	•	k?
Smrt	smrt	k1gFnSc1
v	v	k7c6
zimě	zima	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Star	Star	kA
Trek	Trek	k1gMnSc1
</s>
