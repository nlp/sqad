<s>
World	World	k1gInSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gInSc1	Warcraft
<g/>
:	:	kIx,	:
Mists	Mists	k1gInSc1	Mists
of	of	k?	of
Pandaria	Pandarium	k1gNnSc2	Pandarium
je	být	k5eAaImIp3nS	být
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
datadisk	datadisk	k1gInSc4	datadisk
k	k	k7c3	k
MMORPG	MMORPG	kA	MMORPG
hře	hra	k1gFnSc3	hra
World	World	k1gInSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gInSc1	Warcraft
vyvíjený	vyvíjený	k2eAgInSc1d1	vyvíjený
společností	společnost	k1gFnSc7	společnost
Blizzard	Blizzard	k1gMnSc1	Blizzard
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
Chrisem	Chris	k1gInSc7	Chris
Metzenem	Metzen	k1gMnSc7	Metzen
na	na	k7c6	na
BlizzConu	BlizzCon	k1gInSc6	BlizzCon
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
posunul	posunout	k5eAaPmAgInS	posunout
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
beta-testování	betaestování	k1gNnSc2	beta-testování
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
maximální	maximální	k2eAgFnSc1d1	maximální
úroveň	úroveň	k1gFnSc1	úroveň
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
na	na	k7c4	na
90	[number]	k4	90
nová	nový	k2eAgFnSc1d1	nová
rasa	rasa	k1gFnSc1	rasa
-	-	kIx~	-
Pandaren	Pandarno	k1gNnPc2	Pandarno
nové	nový	k2eAgNnSc4d1	nové
povolání	povolání	k1gNnSc4	povolání
-	-	kIx~	-
Monk	Monk	k1gInSc1	Monk
(	(	kIx(	(
<g/>
mnich	mnich	k1gInSc1	mnich
<g/>
)	)	kIx)	)
nový	nový	k2eAgInSc1d1	nový
kontinent	kontinent	k1gInSc1	kontinent
-	-	kIx~	-
Pandarie	Pandarie	k1gFnSc2	Pandarie
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
talentů	talent	k1gInPc2	talent
nové	nový	k2eAgFnSc2d1	nová
dungeony	dungeona	k1gFnSc2	dungeona
a	a	k8xC	a
raidy	raid	k1gInPc1	raid
oživení	oživení	k1gNnSc2	oživení
starých	starý	k2eAgFnPc2d1	stará
instancí	instance	k1gFnPc2	instance
-	-	kIx~	-
Scholomance	Scholomanka	k1gFnSc6	Scholomanka
a	a	k8xC	a
Scarlet	Scarlet	k1gInSc4	Scarlet
Monastery	Monaster	k1gInPc1	Monaster
se	se	k3xPyFc4	se
dočkají	dočkat	k5eAaPmIp3nP	dočkat
úprav	úprava	k1gFnPc2	úprava
a	a	k8xC	a
dostanou	dostat	k5eAaPmIp3nP	dostat
HC	HC	kA	HC
mod	mod	k?	mod
challenge	challenge	k1gFnSc1	challenge
dungeony	dungeona	k1gFnSc2	dungeona
scenario	scenario	k1gNnSc4	scenario
-	-	kIx~	-
výpravy	výprava	k1gFnSc2	výprava
pet	pet	k?	pet
arény	aréna	k1gFnSc2	aréna
Pandaren	Pandarna	k1gFnPc2	Pandarna
je	být	k5eAaImIp3nS	být
nově	nově	k6eAd1	nově
přidanou	přidaný	k2eAgFnSc7d1	přidaná
rasou	rasa	k1gFnSc7	rasa
tohoto	tento	k3xDgInSc2	tento
datadisku	datadisek	k1gInSc2	datadisek
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vybere	vybrat	k5eAaPmIp3nS	vybrat
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
až	až	k6eAd1	až
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
úrovně	úroveň	k1gFnSc2	úroveň
neutrální	neutrální	k2eAgFnPc1d1	neutrální
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
vybere	vybrat	k5eAaPmIp3nS	vybrat
<g/>
,	,	kIx,	,
na	na	k7c4	na
čí	čí	k3xOyQgFnSc4	čí
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
mountem	mount	k1gInSc7	mount
budou	být	k5eAaImBp3nP	být
Dračí	dračí	k2eAgFnPc1d1	dračí
želvy	želva	k1gFnPc1	želva
(	(	kIx(	(
<g/>
Dragon	Dragon	k1gMnSc1	Dragon
Turtles	Turtles	k1gMnSc1	Turtles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
svoji	svůj	k3xOyFgFnSc4	svůj
startovní	startovní	k2eAgFnSc4d1	startovní
lokaci	lokace	k1gFnSc4	lokace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
hráč	hráč	k1gMnSc1	hráč
splnil	splnit	k5eAaPmAgMnS	splnit
všechny	všechen	k3xTgFnPc4	všechen
questy	questa	k1gFnPc4	questa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
dostal	dostat	k5eAaPmAgMnS	dostat
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
Worgenů	Worgen	k1gInPc2	Worgen
nebo	nebo	k8xC	nebo
Goblinů	Goblin	k1gInPc2	Goblin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
hlavní	hlavní	k2eAgFnSc2d1	hlavní
questové	questový	k2eAgFnSc2d1	questový
linie	linie	k1gFnSc2	linie
dostane	dostat	k5eAaPmIp3nS	dostat
pandaren	pandarna	k1gFnPc2	pandarna
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
quest	quest	k1gInSc4	quest
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zvolí	zvolit	k5eAaPmIp3nS	zvolit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
věrný	věrný	k2eAgInSc1d1	věrný
Alianci	aliance	k1gFnSc3	aliance
nebo	nebo	k8xC	nebo
Hordě	horda	k1gFnSc3	horda
<g/>
.	.	kIx.	.
</s>
<s>
Pandareni	Pandaren	k2eAgMnPc1d1	Pandaren
z	z	k7c2	z
opozičních	opoziční	k2eAgFnPc2d1	opoziční
frakcí	frakce	k1gFnPc2	frakce
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
nemohou	moct	k5eNaImIp3nP	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
.	.	kIx.	.
</s>
<s>
Pandaren	Pandarna	k1gFnPc2	Pandarna
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
povolání	povolání	k1gNnSc2	povolání
Hunter	Huntra	k1gFnPc2	Huntra
<g/>
,	,	kIx,	,
Mage	Mage	k1gFnPc2	Mage
<g/>
,	,	kIx,	,
Monk	Monka	k1gFnPc2	Monka
<g/>
,	,	kIx,	,
Priest	Priest	k1gFnSc1	Priest
<g/>
,	,	kIx,	,
Rogue	Rogue	k1gFnSc1	Rogue
<g/>
,	,	kIx,	,
Shaman	Shaman	k1gMnSc1	Shaman
a	a	k8xC	a
Warrior	Warrior	k1gMnSc1	Warrior
<g/>
,	,	kIx,	,
speciálními	speciální	k2eAgFnPc7d1	speciální
schopnostmi	schopnost	k1gFnPc7	schopnost
této	tento	k3xDgFnSc2	tento
rasy	rasa	k1gFnSc2	rasa
pak	pak	k8xC	pak
budou	být	k5eAaImBp3nP	být
Epicurean	Epicurean	k1gInSc4	Epicurean
-	-	kIx~	-
zdvojnásobený	zdvojnásobený	k2eAgInSc4d1	zdvojnásobený
efekt	efekt	k1gInSc4	efekt
Well-fed	Welled	k1gInSc1	Well-fed
bonusů	bonus	k1gInPc2	bonus
Gourmand	Gourmanda	k1gFnPc2	Gourmanda
-	-	kIx~	-
cooking	cooking	k1gInSc1	cooking
skill	skilnout	k5eAaPmAgInS	skilnout
zvýšen	zvýšit	k5eAaPmNgInS	zvýšit
o	o	k7c4	o
15	[number]	k4	15
Inner	Innero	k1gNnPc2	Innero
peace	peace	k1gFnSc1	peace
-	-	kIx~	-
rested	rested	k1gInSc1	rested
bonus	bonus	k1gInSc1	bonus
(	(	kIx(	(
<g/>
postavy	postava	k1gFnPc1	postava
získávají	získávat	k5eAaImIp3nP	získávat
200	[number]	k4	200
%	%	kIx~	%
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
)	)	kIx)	)
trvá	trvat	k5eAaImIp3nS	trvat
dvakrát	dvakrát	k6eAd1	dvakrát
déle	dlouho	k6eAd2	dlouho
Bouncy	Bounc	k2eAgInPc1d1	Bounc
-	-	kIx~	-
fall	fall	k1gInSc1	fall
damage	damage	k6eAd1	damage
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
menší	malý	k2eAgInSc1d2	menší
Quaking	Quaking	k1gInSc1	Quaking
palm	palm	k1gInSc1	palm
-	-	kIx~	-
omráčí	omráčet	k5eAaImIp3nS	omráčet
protivníka	protivník	k1gMnSc4	protivník
na	na	k7c4	na
3	[number]	k4	3
sekundy	sekunda	k1gFnSc2	sekunda
Tento	tento	k3xDgInSc1	tento
nový	nový	k2eAgInSc1d1	nový
kontinent	kontinent	k1gInSc1	kontinent
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Azerothu	Azeroth	k1gInSc2	Azeroth
mezi	mezi	k7c7	mezi
Kalimdorem	Kalimdor	k1gInSc7	Kalimdor
a	a	k8xC	a
Eastern	Eastern	k1gNnSc1	Eastern
Kingdoms	Kingdomsa	k1gFnPc2	Kingdomsa
<g/>
.	.	kIx.	.
</s>
<s>
Létat	létat	k5eAaImF	létat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
90	[number]	k4	90
<g/>
.	.	kIx.	.
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
datadisku	datadisek	k1gInSc6	datadisek
maximální	maximální	k2eAgFnSc1d1	maximální
<g/>
.	.	kIx.	.
</s>
<s>
Kontinent	kontinent	k1gInSc1	kontinent
Pandaria	Pandarium	k1gNnSc2	Pandarium
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
průměrná	průměrný	k2eAgFnSc1d1	průměrná
úroveň	úroveň	k1gFnSc1	úroveň
netvorů	netvor	k1gMnPc2	netvor
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Jade	Jade	k1gFnSc1	Jade
Forest	Forest	k1gFnSc1	Forest
(	(	kIx(	(
<g/>
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
s	s	k7c7	s
dungeonem	dungeon	k1gInSc7	dungeon
Temple	templ	k1gInSc5	templ
of	of	k?	of
the	the	k?	the
Jade	Jad	k1gFnSc2	Jad
Serpent	Serpent	k1gMnSc1	Serpent
Krasarang	Krasarang	k1gMnSc1	Krasarang
Wild	Wild	k1gMnSc1	Wild
(	(	kIx(	(
<g/>
86	[number]	k4	86
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Valley	Valle	k1gMnPc4	Valle
of	of	k?	of
the	the	k?	the
Four	Four	k1gInSc1	Four
Winds	Winds	k1gInSc1	Winds
(	(	kIx(	(
<g/>
86	[number]	k4	86
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
<g/>
)	)	kIx)	)
s	s	k7c7	s
dungeonem	dungeon	k1gMnSc7	dungeon
The	The	k1gMnSc2	The
Stormstout	Stormstout	k1gMnSc1	Stormstout
Brewery	Brewera	k1gFnPc4	Brewera
<g/>
,	,	kIx,	,
<g/>
Gate	Gate	k1gFnSc1	Gate
Of	Of	k1gFnSc1	Of
The	The	k1gFnSc1	The
Setting	Setting	k1gInSc4	Setting
Sun	sun	k1gInSc1	sun
Kun-Lai	Kun-Lai	k1gNnSc1	Kun-Lai
Summit	summit	k1gInSc1	summit
(	(	kIx(	(
<g/>
87	[number]	k4	87
<g/>
-	-	kIx~	-
<g/>
89	[number]	k4	89
<g/>
)	)	kIx)	)
s	s	k7c7	s
dungeonem	dungeon	k1gInSc7	dungeon
Shado-Pan	Shado-Pan	k1gMnSc1	Shado-Pan
Monastery	Monaster	k1gInPc4	Monaster
Townlong	Townlong	k1gMnSc1	Townlong
Steppes	Steppes	k1gMnSc1	Steppes
(	(	kIx(	(
<g/>
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
89	[number]	k4	89
<g/>
)	)	kIx)	)
Dread	Dread	k1gInSc1	Dread
Wastes	Wastes	k1gInSc1	Wastes
(	(	kIx(	(
<g/>
89	[number]	k4	89
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
)	)	kIx)	)
Vale	val	k1gInSc6	val
of	of	k?	of
Ethernal	Ethernal	k1gFnSc2	Ethernal
Blossoms	Blossomsa	k1gFnPc2	Blossomsa
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
)	)	kIx)	)
Monk	Monk	k1gInSc1	Monk
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
mnich	mnich	k1gMnSc1	mnich
<g/>
)	)	kIx)	)
bude	být	k5eAaImBp3nS	být
novým	nový	k2eAgNnSc7d1	nové
povoláním	povolání	k1gNnSc7	povolání
datadisku	datadisek	k1gInSc2	datadisek
Mists	Mistsa	k1gFnPc2	Mistsa
of	of	k?	of
Pandaria	Pandarium	k1gNnSc2	Pandarium
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k6eAd1	Monk
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
vybrat	vybrat	k5eAaPmF	vybrat
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
rolemi	role	k1gFnPc7	role
<g/>
:	:	kIx,	:
tank	tank	k1gInSc1	tank
<g/>
,	,	kIx,	,
melee	melee	k1gFnSc1	melee
damage	damage	k1gFnSc1	damage
<g/>
,	,	kIx,	,
healer	healer	k1gInSc1	healer
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
stance	stance	k1gFnSc2	stance
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
energii	energie	k1gFnSc4	energie
bude	být	k5eAaImBp3nS	být
využívat	využívat	k5eAaImF	využívat
energii	energie	k1gFnSc3	energie
Chi	chi	k0	chi
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
healer	healer	k1gInSc1	healer
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
vrátí	vrátit	k5eAaPmIp3nS	vrátit
k	k	k7c3	k
maně	mana	k1gFnSc3	mana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Armor	Armor	k1gInSc1	Armor
proficiency	proficienca	k1gFnSc2	proficienca
<g/>
:	:	kIx,	:
Cloth	Cloth	k1gMnSc1	Cloth
Leather	Leathra	k1gFnPc2	Leathra
Weapon	Weapon	k1gMnSc1	Weapon
proficiency	proficienca	k1gFnSc2	proficienca
<g/>
:	:	kIx,	:
Fist	Fist	k2eAgInSc1d1	Fist
Weapons	Weapons	k1gInSc1	Weapons
One-Handed	One-Handed	k1gInSc1	One-Handed
Axes	Axes	k1gInSc1	Axes
One-Handed	One-Handed	k1gInSc1	One-Handed
Maces	maces	k1gInSc1	maces
One-Handed	One-Handed	k1gInSc1	One-Handed
Swords	Swords	k1gInSc1	Swords
Polearms	Polearmsa	k1gFnPc2	Polearmsa
Staves	Stavesa	k1gFnPc2	Stavesa
Povolání	povolání	k1gNnSc2	povolání
Monk	Monka	k1gFnPc2	Monka
bude	být	k5eAaImBp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
všem	všecek	k3xTgFnPc3	všecek
rasám	rasa	k1gFnPc3	rasa
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Goblinů	Goblin	k1gInPc2	Goblin
pro	pro	k7c4	pro
milovníky	milovník	k1gMnPc4	milovník
Worgenů	Worgen	k1gInPc2	Worgen
bylo	být	k5eAaImAgNnS	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
mohou	moct	k5eAaImIp3nP	moct
Worgeni	Worgen	k2eAgMnPc1d1	Worgen
používat	používat	k5eAaImF	používat
povolání	povolání	k1gNnSc2	povolání
Monk	Monk	k1gInSc1	Monk
<g/>
(	(	kIx(	(
<g/>
mnich	mnich	k1gInSc1	mnich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Talentové	talentový	k2eAgInPc1d1	talentový
stromy	strom	k1gInPc1	strom
byly	být	k5eAaImAgInP	být
kompletně	kompletně	k6eAd1	kompletně
předělány	předělán	k2eAgInPc1d1	předělán
a	a	k8xC	a
počet	počet	k1gInSc1	počet
talentů	talent	k1gInPc2	talent
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
snížil	snížit	k5eAaPmAgInS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
úrovni	úroveň	k1gFnSc6	úroveň
si	se	k3xPyFc3	se
nyní	nyní	k6eAd1	nyní
hráč	hráč	k1gMnSc1	hráč
volí	volit	k5eAaImIp3nS	volit
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
(	(	kIx(	(
<g/>
tank	tank	k1gInSc4	tank
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
útok	útok	k1gInSc1	útok
(	(	kIx(	(
<g/>
dps	dps	k?	dps
<g/>
)	)	kIx)	)
či	či	k8xC	či
léčení	léčení	k1gNnSc4	léčení
(	(	kIx(	(
<g/>
healer	healer	k1gInSc1	healer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
patnáctou	patnáctý	k4xOgFnSc4	patnáctý
úroveň	úroveň	k1gFnSc4	úroveň
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
vybere	vybrat	k5eAaPmIp3nS	vybrat
jeden	jeden	k4xCgInSc1	jeden
talent	talent	k1gInSc1	talent
z	z	k7c2	z
řádku	řádek	k1gInSc2	řádek
<g/>
.	.	kIx.	.
</s>
<s>
Půjde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
výzvy	výzva	k1gFnSc2	výzva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
domluví	domluvit	k5eAaPmIp3nP	domluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtějí	chtít	k5eAaImIp3nP	chtít
spustit	spustit	k5eAaPmF	spustit
challenge	challenge	k6eAd1	challenge
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zabití	zabití	k1gNnSc6	zabití
finálního	finální	k2eAgMnSc2d1	finální
bosse	boss	k1gMnSc2	boss
pak	pak	k6eAd1	pak
dostanou	dostat	k5eAaPmIp3nP	dostat
různé	různý	k2eAgFnPc4d1	různá
odměny	odměna	k1gFnPc4	odměna
<g/>
,	,	kIx,	,
při	při	k7c6	při
lepším	dobrý	k2eAgInSc6d2	lepší
čase	čas	k1gInSc6	čas
hodnotnější	hodnotný	k2eAgNnSc1d2	hodnotnější
<g/>
.	.	kIx.	.
</s>
<s>
Půjde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
jakési	jakýsi	k3yIgFnSc6	jakýsi
PvE	PvE	k1gFnSc6	PvE
battlegroundy	battlegrounda	k1gFnSc2	battlegrounda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráč	hráč	k1gMnSc1	hráč
musí	muset	k5eAaImIp3nS	muset
plnit	plnit	k5eAaImF	plnit
zadané	zadaný	k2eAgFnSc2d1	zadaná
questové	questový	k2eAgFnSc2d1	questový
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
