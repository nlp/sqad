<s>
Douglas	Douglas	k1gInSc1	Douglas
Noël	Noëla	k1gFnPc2	Noëla
Adams	Adamsa	k1gFnPc2	Adamsa
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1952	[number]	k4	1952
Cambridge	Cambridge	k1gFnSc2	Cambridge
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
humoristických	humoristický	k2eAgMnPc2d1	humoristický
rozhlasových	rozhlasový	k2eAgMnPc2d1	rozhlasový
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
knižní	knižní	k2eAgFnSc7d1	knižní
sérií	série	k1gFnSc7	série
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
Galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pětidílná	pětidílný	k2eAgFnSc1d1	pětidílná
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
tištěné	tištěný	k2eAgFnSc6d1	tištěná
podobě	podoba	k1gFnSc6	podoba
vydalo	vydat	k5eAaPmAgNnS	vydat
přes	přes	k7c4	přes
patnáct	patnáct	k4xCc4	patnáct
milionů	milion	k4xCgInPc2	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
rádiu	rádius	k1gInSc6	rádius
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
přepracována	přepracovat	k5eAaPmNgFnS	přepracovat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
komiksové	komiksový	k2eAgFnSc2d1	komiksová
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
až	až	k9	až
po	po	k7c6	po
Adamsově	Adamsův	k2eAgFnSc6d1	Adamsova
smrti	smrt	k1gFnSc6	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
se	se	k3xPyFc4	se
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
také	také	k6eAd1	také
divadelního	divadelní	k2eAgNnSc2d1	divadelní
zpracování	zpracování	k1gNnSc3	zpracování
v	v	k7c6	v
několika	několik	k4yIc6	několik
provedeních	provedení	k1gNnPc6	provedení
-	-	kIx~	-
rané	raný	k2eAgFnPc1d1	raná
inscenace	inscenace	k1gFnPc1	inscenace
čerpaly	čerpat	k5eAaImAgFnP	čerpat
z	z	k7c2	z
aktuálních	aktuální	k2eAgFnPc2d1	aktuální
Adamsových	Adamsův	k2eAgFnPc2d1	Adamsova
poznámek	poznámka	k1gFnPc2	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
fanoušci	fanoušek	k1gMnPc1	fanoušek
ho	on	k3xPp3gInSc4	on
znají	znát	k5eAaImIp3nP	znát
také	také	k9	také
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Bop	bop	k1gInSc1	bop
Ad	ad	k7c4	ad
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
chybným	chybný	k2eAgInSc7d1	chybný
výkladem	výklad	k1gInSc7	výklad
jeho	on	k3xPp3gInSc2	on
nečitelného	čitelný	k2eNgInSc2d1	nečitelný
podpisu	podpis	k1gInSc2	podpis
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
jeho	jeho	k3xOp3gInPc2	jeho
iniciálů	iniciál	k1gInPc2	iniciál
<g/>
,	,	kIx,	,
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Stopařova	stopařův	k2eAgMnSc2d1	stopařův
průvodce	průvodce	k1gMnSc2	průvodce
napsal	napsat	k5eAaPmAgInS	napsat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
autorsky	autorsky	k6eAd1	autorsky
spolupodílel	spolupodílet	k5eAaImAgMnS	spolupodílet
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
dílech	díl	k1gInPc6	díl
sci-fi	scii	k1gNnSc2	sci-fi
seriálu	seriál	k1gInSc2	seriál
Doctor	Doctor	k1gMnSc1	Doctor
Who	Who	k1gMnSc1	Who
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
sedmnáctou	sedmnáctý	k4xOgFnSc4	sedmnáctý
sezónu	sezóna	k1gFnSc4	sezóna
navíc	navíc	k6eAd1	navíc
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
scénář	scénář	k1gInSc1	scénář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnPc4	jeho
další	další	k2eAgNnPc4d1	další
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nP	patřit
romány	román	k1gInPc1	román
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
Dirka	Direk	k1gMnSc2	Direk
Gentlyho	Gently	k1gMnSc2	Gently
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
knihy	kniha	k1gFnPc4	kniha
Životic	Životice	k1gFnPc2	Životice
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc2	jenž
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Lloydem	Lloyd	k1gMnSc7	Lloyd
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokumentární	dokumentární	k2eAgFnPc1d1	dokumentární
publikace	publikace	k1gFnPc1	publikace
Ještě	ještě	k6eAd1	ještě
je	on	k3xPp3gFnPc4	on
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
-	-	kIx~	-
výprava	výprava	k1gFnSc1	výprava
za	za	k7c7	za
vymírajícími	vymírající	k2eAgMnPc7d1	vymírající
živočichy	živočich	k1gMnPc7	živočich
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
doprovod	doprovod	k1gInSc4	doprovod
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgNnSc3d1	stejnojmenné
rozhlasovému	rozhlasový	k2eAgNnSc3d1	rozhlasové
vysílání	vysílání	k1gNnSc3	vysílání
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
také	také	k9	také
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
Titanic	Titanic	k1gInSc1	Titanic
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
Terry	Terr	k1gMnPc4	Terr
Jonesem	Jones	k1gMnSc7	Jones
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
románové	románový	k2eAgFnSc2d1	románová
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
označoval	označovat	k5eAaImAgMnS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
radikálního	radikální	k2eAgMnSc4d1	radikální
ateistu	ateista	k1gMnSc4	ateista
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přátelé	přítel	k1gMnPc1	přítel
a	a	k8xC	a
fanoušci	fanoušek	k1gMnPc1	fanoušek
ho	on	k3xPp3gMnSc4	on
znali	znát	k5eAaImAgMnP	znát
také	také	k9	také
jako	jako	k9	jako
ekologického	ekologický	k2eAgMnSc4d1	ekologický
aktivistu	aktivista	k1gMnSc4	aktivista
a	a	k8xC	a
milovníka	milovník	k1gMnSc4	milovník
rychlých	rychlý	k2eAgFnPc2d1	rychlá
aut	auto	k1gNnPc2	auto
<g/>
,	,	kIx,	,
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
,	,	kIx,	,
počítačů	počítač	k1gInPc2	počítač
Macintosh	Macintosh	kA	Macintosh
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
"	"	kIx"	"
<g/>
technických	technický	k2eAgFnPc2d1	technická
vymožeností	vymoženost	k1gFnPc2	vymoženost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
biolog	biolog	k1gMnSc1	biolog
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
svou	svůj	k3xOyFgFnSc4	svůj
knihu	kniha	k1gFnSc4	kniha
The	The	k1gFnSc1	The
God	God	k1gMnSc1	God
Delusion	Delusion	k1gInSc1	Delusion
(	(	kIx(	(
<g/>
Boží	boží	k2eAgInSc1d1	boží
blud	blud	k1gInSc1	blud
<g/>
)	)	kIx)	)
právě	právě	k9	právě
jemu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jeho	jeho	k3xOp3gInSc1	jeho
výklad	výklad	k1gInSc1	výklad
evoluce	evoluce	k1gFnSc2	evoluce
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
Adamsovo	Adamsův	k2eAgNnSc4d1	Adamsovo
"	"	kIx"	"
<g/>
konvertování	konvertování	k1gNnSc4	konvertování
<g/>
"	"	kIx"	"
k	k	k7c3	k
ateismu	ateismus	k1gInSc3	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
zastáncem	zastánce	k1gMnSc7	zastánce
techniky	technika	k1gFnSc2	technika
-	-	kIx~	-
například	například	k6eAd1	například
o	o	k7c6	o
e-mailu	eail	k1gInSc2	e-mail
a	a	k8xC	a
Usenetu	Usenet	k1gInSc2	Usenet
psal	psát	k5eAaImAgInS	psát
dávno	dávno	k6eAd1	dávno
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
populárními	populární	k2eAgFnPc7d1	populární
a	a	k8xC	a
široce	široko	k6eAd1	široko
rozšířenými	rozšířený	k2eAgInPc7d1	rozšířený
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
vyhledávaný	vyhledávaný	k2eAgMnSc1d1	vyhledávaný
lektor	lektor	k1gMnSc1	lektor
<g/>
,	,	kIx,	,
přednášející	přednášející	k1gMnSc1	přednášející
o	o	k7c6	o
moderních	moderní	k2eAgFnPc6d1	moderní
technologiích	technologie	k1gFnPc6	technologie
a	a	k8xC	a
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
pořádán	pořádán	k2eAgInSc4d1	pořádán
Ručníkový	ručníkový	k2eAgInSc4d1	ručníkový
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgInS	narodit
se	se	k3xPyFc4	se
Janet	Janet	k1gInSc1	Janet
Adamsové	Adamsová	k1gFnSc2	Adamsová
(	(	kIx(	(
<g/>
rozené	rozený	k2eAgFnSc2d1	rozená
Donovanové	Donovanová	k1gFnSc2	Donovanová
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Thriftové	Thriftové	k2eAgFnSc1d1	Thriftové
<g/>
)	)	kIx)	)
a	a	k8xC	a
Christopheru	Christophera	k1gFnSc4	Christophera
Douglesi	Douglese	k1gFnSc4	Douglese
Adamsovi	Adamsův	k2eAgMnPc1d1	Adamsův
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
městě	město	k1gNnSc6	město
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Susan	Susany	k1gInPc2	Susany
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Christopher	Christophra	k1gFnPc2	Christophra
a	a	k8xC	a
Janet	Janeta	k1gFnPc2	Janeta
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
,	,	kIx,	,
Douglas	Douglas	k1gInSc4	Douglas
a	a	k8xC	a
Susan	Susan	k1gInSc4	Susan
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
k	k	k7c3	k
Janetiným	Janetin	k2eAgMnPc3d1	Janetin
rodičům	rodič	k1gMnPc3	rodič
do	do	k7c2	do
Brentwoodu	Brentwood	k1gInSc2	Brentwood
v	v	k7c6	v
Essexu	Essex	k1gInSc6	Essex
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
babička	babička	k1gFnSc1	babička
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
útočiště	útočiště	k1gNnSc4	útočiště
poraněným	poraněný	k2eAgNnPc3d1	poraněné
zvířatům	zvíře	k1gNnPc3	zvíře
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
prevenci	prevence	k1gFnSc4	prevence
krutosti	krutost	k1gFnSc2	krutost
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
,	,	kIx,	,
britské	britský	k2eAgInPc4d1	britský
RSPCA	RSPCA	kA	RSPCA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
"	"	kIx"	"
<g/>
zhoršení	zhoršení	k1gNnSc3	zhoršení
senné	senný	k2eAgFnSc2d1	senná
rýmy	rýma	k1gFnSc2	rýma
a	a	k8xC	a
astmatu	astma	k1gNnSc2	astma
malého	malé	k1gNnSc2	malé
Douglase	Douglas	k1gInSc6	Douglas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Adams	Adamsa	k1gFnPc2	Adamsa
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Mary	Mary	k1gFnSc1	Mary
Judith	Judith	k1gInSc1	Judith
Stewartová	Stewartová	k1gFnSc1	Stewartová
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Robertsová	Robertsová	k1gFnSc1	Robertsová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
svazku	svazek	k1gInSc2	svazek
vzešla	vzejít	k5eAaPmAgFnS	vzejít
Adamsova	Adamsův	k2eAgFnSc1d1	Adamsova
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
sestra	sestra	k1gFnSc1	sestra
Heather	Heathra	k1gFnPc2	Heathra
<g/>
.	.	kIx.	.
</s>
<s>
Janet	Janet	k1gMnSc1	Janet
se	se	k3xPyFc4	se
podruhé	podruhé	k6eAd1	podruhé
provdala	provdat	k5eAaPmAgFnS	provdat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
za	za	k7c4	za
veterináře	veterinář	k1gMnSc4	veterinář
Rona	Ron	k1gMnSc4	Ron
Thrifta	Thrift	k1gMnSc4	Thrift
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
přivedla	přivést	k5eAaPmAgFnS	přivést
na	na	k7c4	na
svět	svět	k1gInSc4	svět
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
Adamsovy	Adamsův	k2eAgMnPc4d1	Adamsův
nevlastní	vlastní	k2eNgMnPc4d1	nevlastní
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
:	:	kIx,	:
Jane	Jan	k1gMnSc5	Jan
a	a	k8xC	a
Jamese	Jamese	k1gFnPc1	Jamese
Thriftovy	Thriftův	k2eAgFnPc1d1	Thriftův
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mateřské	mateřský	k2eAgFnSc6d1	mateřská
škole	škola	k1gFnSc6	škola
Primrose	Primrosa	k1gFnSc6	Primrosa
Hill	Hill	k1gMnSc1	Hill
v	v	k7c6	v
Brentwoodu	Brentwood	k1gInSc6	Brentwood
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
brentwoodskou	brentwoodský	k2eAgFnSc4d1	brentwoodský
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
a	a	k8xC	a
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
žáky	žák	k1gMnPc4	žák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
ročníku	ročník	k1gInSc6	ročník
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
umělecké	umělecký	k2eAgNnSc4d1	umělecké
zaměření	zaměření	k1gNnSc4	zaměření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ještě	ještě	k9	ještě
o	o	k7c4	o
rok	rok	k1gInSc4	rok
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
u	u	k7c2	u
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
ke	k	k7c3	k
zkouškám	zkouška	k1gFnPc3	zkouška
na	na	k7c6	na
Oxbridge	Oxbridge	k1gFnSc6	Oxbridge
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
stupni	stupeň	k1gInSc6	stupeň
mu	on	k3xPp3gNnSc3	on
učitel	učitel	k1gMnSc1	učitel
Frank	Frank	k1gMnSc1	Frank
Halford	Halford	k1gMnSc1	Halford
udělil	udělit	k5eAaPmAgMnS	udělit
údajně	údajně	k6eAd1	údajně
jediný	jediný	k2eAgInSc4d1	jediný
plný	plný	k2eAgInSc4d1	plný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
kreativní	kreativní	k2eAgNnSc4d1	kreativní
psaní	psaní	k1gNnSc4	psaní
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
příhodu	příhoda	k1gFnSc4	příhoda
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
když	když	k8xS	když
měl	mít	k5eAaImAgInS	mít
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
raných	raný	k2eAgNnPc2d1	rané
děl	dělo	k1gNnPc2	dělo
vycházely	vycházet	k5eAaImAgFnP	vycházet
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
školním	školní	k2eAgInSc6d1	školní
fotografickém	fotografický	k2eAgInSc6d1	fotografický
kroužku	kroužek	k1gInSc6	kroužek
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
humorné	humorný	k2eAgInPc4d1	humorný
články	článek	k1gInPc4	článek
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
časopise	časopis	k1gInSc6	časopis
Broadsheet	Broadsheeta	k1gFnPc2	Broadsheeta
(	(	kIx(	(
<g/>
redaktorem	redaktor	k1gMnSc7	redaktor
byl	být	k5eAaImAgMnS	být
Paul	Paul	k1gMnSc1	Paul
Neil	Neil	k1gMnSc1	Neil
Milne	Miln	k1gInSc5	Miln
Johnstone	Johnston	k1gInSc5	Johnston
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
magazínu	magazín	k1gInSc6	magazín
The	The	k1gMnSc2	The
Eagle	Eagl	k1gMnSc2	Eagl
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
otištěn	otisknout	k5eAaPmNgMnS	otisknout
také	také	k9	také
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
dopisů	dopis	k1gInPc2	dopis
a	a	k8xC	a
krátká	krátký	k2eAgFnSc1d1	krátká
povídka	povídka	k1gFnSc1	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
o	o	k7c4	o
rok	rok	k1gInSc1	rok
mladším	mladý	k2eAgMnSc7d2	mladší
Griffem	Griff	k1gMnSc7	Griff
Rhysem	Rhys	k1gMnSc7	Rhys
Jonesem	Jones	k1gMnSc7	Jones
<g/>
,	,	kIx,	,
dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
spolužáků	spolužák	k1gMnPc2	spolužák
byl	být	k5eAaImAgInS	být
také	také	k9	také
stuckistický	stuckistický	k2eAgMnSc1d1	stuckistický
umělec	umělec	k1gMnSc1	umělec
Charles	Charles	k1gMnSc1	Charles
Thomson	Thomson	k1gMnSc1	Thomson
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
Shakespearově	Shakespearův	k2eAgFnSc6d1	Shakespearova
hře	hra	k1gFnSc6	hra
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
měřil	měřit	k5eAaImAgMnS	měřit
úctyhodných	úctyhodný	k2eAgFnPc2d1	úctyhodná
183	[number]	k4	183
cm	cm	kA	cm
a	a	k8xC	a
růst	růst	k1gInSc1	růst
přestal	přestat	k5eAaPmAgInS	přestat
až	až	k9	až
na	na	k7c4	na
196	[number]	k4	196
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
výšky	výška	k1gFnSc2	výška
dělával	dělávat	k5eAaImAgMnS	dělávat
legraci	legrace	k1gFnSc4	legrace
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Třídní	třídní	k1gMnSc1	třídní
učitel	učitel	k1gMnSc1	učitel
nikdy	nikdy	k6eAd1	nikdy
neřekl	říct	k5eNaPmAgMnS	říct
'	'	kIx"	'
<g/>
sejdeme	sejít	k5eAaPmIp1nP	sejít
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
hodinami	hodina	k1gFnPc7	hodina
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
'	'	kIx"	'
<g/>
sejdeme	sejít	k5eAaPmIp1nP	sejít
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
válečným	válečný	k2eAgInSc7d1	válečný
památníkem	památník	k1gInSc7	památník
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
'	'	kIx"	'
<g/>
sejdeme	sejít	k5eAaPmIp1nP	sejít
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
Adamsem	Adams	k1gInSc7	Adams
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
mistrovsky	mistrovsky	k6eAd1	mistrovsky
napsané	napsaný	k2eAgInPc1d1	napsaný
eseje	esej	k1gInPc1	esej
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
náboženské	náboženský	k2eAgFnSc2d1	náboženská
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pojednával	pojednávat	k5eAaImAgInS	pojednávat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
o	o	k7c6	o
Beatles	Beatles	k1gFnSc6	Beatles
či	či	k8xC	či
Williamu	William	k1gInSc2	William
Blakeovi	Blakeus	k1gMnSc3	Blakeus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
přijat	přijmout	k5eAaPmNgMnS	přijmout
na	na	k7c4	na
cambridgeskou	cambridgeský	k2eAgFnSc4d1	Cambridgeská
St	St	kA	St
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
dramatického	dramatický	k2eAgInSc2d1	dramatický
klubu	klub	k1gInSc2	klub
Footlights	Footlightsa	k1gFnPc2	Footlightsa
(	(	kIx(	(
<g/>
Světla	světlo	k1gNnPc1	světlo
ramp	rampa	k1gFnPc2	rampa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spojeného	spojený	k2eAgNnSc2d1	spojené
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
známých	známý	k2eAgMnPc2d1	známý
představitelů	představitel	k1gMnPc2	představitel
britské	britský	k2eAgFnSc2d1	britská
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
žádost	žádost	k1gFnSc1	žádost
byla	být	k5eAaImAgFnS	být
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Willem	Will	k1gMnSc7	Will
Adamsem	Adams	k1gMnSc7	Adams
a	a	k8xC	a
Martinem	Martin	k1gMnSc7	Martin
Smithem	Smith	k1gInSc7	Smith
založili	založit	k5eAaPmAgMnP	založit
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
skupinu	skupina	k1gFnSc4	skupina
Adams-Smith-Adams	Adams-Smith-Adamsa	k1gFnPc2	Adams-Smith-Adamsa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
a	a	k8xC	a
divadelní	divadelní	k2eAgNnSc1d1	divadelní
ztvárnění	ztvárnění	k1gNnSc1	ztvárnění
satirických	satirický	k2eAgFnPc2d1	satirická
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
Footlights	Footlightsa	k1gFnPc2	Footlightsa
byl	být	k5eAaImAgInS	být
podporován	podporovat	k5eAaImNgInS	podporovat
Simonem	Simon	k1gMnSc7	Simon
Jonesem	Jones	k1gMnSc7	Jones
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
Rhysem	Rhys	k1gMnSc7	Rhys
Jonesem	Jones	k1gMnSc7	Jones
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
získal	získat	k5eAaPmAgInS	získat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
anglická	anglický	k2eAgFnSc1d1	anglická
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
rané	raný	k2eAgFnSc2d1	raná
tvorby	tvorba	k1gFnSc2	tvorba
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
v	v	k7c6	v
upravené	upravený	k2eAgFnSc6d1	upravená
verzi	verze	k1gFnSc6	verze
Footlight	Footlighta	k1gFnPc2	Footlighta
Revue	revue	k1gFnSc2	revue
z	z	k7c2	z
Cambridge	Cambridge	k1gFnSc2	Cambridge
na	na	k7c6	na
TV	TV	kA	TV
kanálu	kanál	k1gInSc2	kanál
BBC	BBC	kA	BBC
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
živému	živý	k2eAgNnSc3d1	živé
představení	představení	k1gNnSc3	představení
téže	týž	k3xTgFnSc2	týž
revue	revue	k1gFnSc2	revue
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
West	Westa	k1gFnPc2	Westa
End	End	k1gFnPc3	End
byl	být	k5eAaImAgInS	být
Adams	Adams	k1gInSc1	Adams
"	"	kIx"	"
<g/>
objeven	objevit	k5eAaPmNgInS	objevit
<g/>
"	"	kIx"	"
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
Monty	Monta	k1gFnSc2	Monta
Pythonů	Python	k1gMnPc2	Python
<g/>
,	,	kIx,	,
Grahamem	Graham	k1gMnSc7	Graham
Chapmanem	Chapman	k1gMnSc7	Chapman
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
krátce	krátce	k6eAd1	krátce
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
epizod	epizoda	k1gFnPc2	epizoda
seriálu	seriál	k1gInSc2	seriál
Monty	Monta	k1gFnSc2	Monta
Pythonův	Pythonův	k2eAgInSc1d1	Pythonův
Létající	létající	k2eAgInSc1d1	létající
cirkus	cirkus	k1gInSc1	cirkus
(	(	kIx(	(
<g/>
epizoda	epizoda	k1gFnSc1	epizoda
45	[number]	k4	45
<g/>
:	:	kIx,	:
Stranicko-politické	stranickoolitický	k2eAgNnSc1d1	stranicko-politický
vysílání	vysílání	k1gNnSc1	vysílání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Adams	Adams	k1gInSc1	Adams
konkrétně	konkrétně	k6eAd1	konkrétně
napsal	napsat	k5eAaPmAgInS	napsat
scénku	scénka	k1gFnSc4	scénka
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
sestra	sestra	k1gFnSc1	sestra
píchá	píchat	k5eAaImIp3nS	píchat
pacienty	pacient	k1gMnPc4	pacient
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
pobodán	pobodat	k5eAaPmNgInS	pobodat
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
sestrou	sestra	k1gFnSc7	sestra
a	a	k8xC	a
s	s	k7c7	s
těžkým	těžký	k2eAgNnSc7d1	těžké
krvácením	krvácení	k1gNnSc7	krvácení
břicha	břicho	k1gNnSc2	břicho
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
lékaře	lékař	k1gMnSc4	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
předkládá	předkládat	k5eAaImIp3nS	předkládat
k	k	k7c3	k
podepsání	podepsání	k1gNnSc3	podepsání
ohromné	ohromný	k2eAgFnSc2d1	ohromná
stohy	stoha	k1gFnSc2	stoha
nesmyslných	smyslný	k2eNgInPc2d1	nesmyslný
formulářů	formulář	k1gInPc2	formulář
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
mohl	moct	k5eAaImAgMnS	moct
ošetřit	ošetřit	k5eAaPmF	ošetřit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
paradox	paradox	k1gInSc1	paradox
později	pozdě	k6eAd2	pozdě
Adams	Adams	k1gInSc1	Adams
použil	použít	k5eAaPmAgInS	použít
jako	jako	k9	jako
předlohu	předloha	k1gFnSc4	předloha
vogonské	vogonský	k2eAgFnSc2d1	vogonská
posedlosti	posedlost	k1gFnSc2	posedlost
papírováním	papírování	k1gNnSc7	papírování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
tvorbou	tvorba	k1gFnSc7	tvorba
jedné	jeden	k4xCgFnSc2	jeden
scénky	scénka	k1gFnSc2	scénka
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Monty	Monta	k1gMnSc2	Monta
Python	Python	k1gMnSc1	Python
a	a	k8xC	a
Svatý	svatý	k2eAgInSc1d1	svatý
grál	grál	k1gInSc1	grál
<g/>
.	.	kIx.	.
</s>
<s>
Mihl	mihnout	k5eAaPmAgInS	mihnout
se	se	k3xPyFc4	se
také	také	k9	také
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
sérii	série	k1gFnSc6	série
Létajícího	létající	k2eAgInSc2d1	létající
cirkusu	cirkus	k1gInSc2	cirkus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
42	[number]	k4	42
<g/>
.	.	kIx.	.
epizody	epizoda	k1gFnSc2	epizoda
"	"	kIx"	"
<g/>
Válka	válka	k1gFnSc1	válka
lehké	lehký	k2eAgFnSc2d1	lehká
zábavy	zábava	k1gFnSc2	zábava
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
podle	podle	k7c2	podle
titulků	titulek	k1gInPc2	titulek
hraje	hrát	k5eAaImIp3nS	hrát
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Emila	Emil	k1gMnSc2	Emil
Koninga	Koning	k1gMnSc2	Koning
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
operační	operační	k2eAgFnSc4d1	operační
roušku	rouška	k1gFnSc4	rouška
a	a	k8xC	a
nasazuje	nasazovat	k5eAaImIp3nS	nasazovat
si	se	k3xPyFc3	se
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Michael	Michael	k1gMnSc1	Michael
Palin	Palin	k1gMnSc1	Palin
představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
postavu	postava	k1gFnSc4	postava
za	za	k7c7	za
druhou	druhý	k4xOgFnSc7	druhý
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
vyprávění	vyprávění	k1gNnSc3	vyprávění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
44	[number]	k4	44
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
hraje	hrát	k5eAaImIp3nS	hrát
montypythonovskou	montypythonovský	k2eAgFnSc4d1	montypythonovská
uječenou	uječený	k2eAgFnSc4d1	uječená
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nakládá	nakládat	k5eAaImIp3nS	nakládat
raketu	raketa	k1gFnSc4	raketa
na	na	k7c4	na
koňský	koňský	k2eAgInSc4d1	koňský
potah	potah	k1gInSc4	potah
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
Terry	Terra	k1gMnSc2	Terra
Jones	Jones	k1gMnSc1	Jones
nabízí	nabízet	k5eAaImIp3nS	nabízet
kolemjdoucím	kolemjdoucí	k1gMnPc3	kolemjdoucí
odvoz	odvoz	k1gInSc4	odvoz
starého	starý	k2eAgNnSc2d1	staré
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Železo	železo	k1gNnSc1	železo
<g/>
!	!	kIx.	!
</s>
<s>
Jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
kousek	kousek	k1gInSc1	kousek
železa	železo	k1gNnSc2	železo
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
díly	díl	k1gInPc1	díl
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
odvysílány	odvysílat	k5eAaPmNgInP	odvysílat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Adams	Adams	k6eAd1	Adams
a	a	k8xC	a
Chapman	Chapman	k1gMnSc1	Chapman
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
pokusili	pokusit	k5eAaPmAgMnP	pokusit
také	také	k9	také
o	o	k7c4	o
několik	několik	k4yIc4	několik
ne-pythonovských	ythonovský	k2eNgNnPc2d1	-pythonovský
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
televizní	televizní	k2eAgFnSc1d1	televizní
hra	hra	k1gFnSc1	hra
Zpoza	zpoza	k7c2	zpoza
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
hry	hra	k1gFnSc2	hra
pro	pro	k7c4	pro
The	The	k1gFnSc4	The
Burkiss	Burkiss	k1gInSc1	Burkiss
Way	Way	k1gMnSc1	Way
a	a	k8xC	a
The	The	k1gMnSc1	The
News	Newsa	k1gFnPc2	Newsa
Huddlines	Huddlines	k1gMnSc1	Huddlines
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
znovu	znovu	k6eAd1	znovu
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Grahamem	Graham	k1gMnSc7	Graham
Chapmanem	Chapman	k1gMnSc7	Chapman
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
napsal	napsat	k5eAaPmAgMnS	napsat
jednu	jeden	k4xCgFnSc4	jeden
epizodu	epizoda	k1gFnSc4	epizoda
pro	pro	k7c4	pro
seriál	seriál	k1gInSc4	seriál
Doktor	doktor	k1gMnSc1	doktor
na	na	k7c4	na
roztrhání	roztrhání	k1gNnSc4	roztrhání
(	(	kIx(	(
<g/>
pokračování	pokračování	k1gNnSc4	pokračování
televizní	televizní	k2eAgFnSc2d1	televizní
komediální	komediální	k2eAgFnSc2d1	komediální
série	série	k1gFnSc2	série
Doktor	doktor	k1gMnSc1	doktor
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgInS	mít
Adams	Adams	k1gInSc4	Adams
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
svých	svůj	k3xOyFgInPc2	svůj
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
různými	různý	k2eAgFnPc7d1	různá
"	"	kIx"	"
<g/>
příležitostnými	příležitostný	k2eAgFnPc7d1	příležitostná
pracemi	práce	k1gFnPc7	práce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Životopis	životopis	k1gInSc1	životopis
uveřejněný	uveřejněný	k2eAgInSc1d1	uveřejněný
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
edicí	edice	k1gFnPc2	edice
Stopařova	stopařův	k2eAgMnSc2d1	stopařův
průvodce	průvodce	k1gMnSc2	průvodce
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	jeho	k3xOp3gFnSc4	jeho
kariéru	kariéra	k1gFnSc4	kariéra
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
titulu	titul	k1gInSc2	titul
strávil	strávit	k5eAaPmAgMnS	strávit
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
prací	práce	k1gFnPc2	práce
pro	pro	k7c4	pro
rádiové	rádiový	k2eAgFnPc4d1	rádiová
a	a	k8xC	a
televizní	televizní	k2eAgFnPc4d1	televizní
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
také	také	k9	také
režíroval	režírovat	k5eAaImAgMnS	režírovat
revue	revue	k1gFnSc4	revue
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
a	a	k8xC	a
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Fringe	Fring	k1gInSc2	Fring
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
také	také	k6eAd1	také
pracoval	pracovat	k5eAaImAgInS	pracovat
jako	jako	k8xS	jako
pomocná	pomocný	k2eAgFnSc1d1	pomocná
síla	síla	k1gFnSc1	síla
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
stavebník	stavebník	k1gMnSc1	stavebník
na	na	k7c6	na
statku	statek	k1gInSc6	statek
<g/>
,	,	kIx,	,
uklízeč	uklízeč	k1gMnSc1	uklízeč
kurníků	kurník	k1gInPc2	kurník
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgMnSc1d1	osobní
strážce	strážce	k1gMnSc1	strážce
<g/>
,	,	kIx,	,
rozhlasový	rozhlasový	k2eAgMnSc1d1	rozhlasový
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
seriálu	seriál	k1gInSc2	seriál
Doctor	Doctor	k1gMnSc1	Doctor
Who	Who	k1gMnSc1	Who
(	(	kIx(	(
<g/>
Pán	pán	k1gMnSc1	pán
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jako	jako	k8xS	jako
osobního	osobní	k2eAgMnSc2d1	osobní
strážce	strážce	k1gMnSc2	strážce
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
najala	najmout	k5eAaPmAgFnS	najmout
arabská	arabský	k2eAgFnSc1d1	arabská
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zbohatla	zbohatnout	k5eAaPmAgFnS	zbohatnout
díky	díky	k7c3	díky
obchodování	obchodování	k1gNnSc3	obchodování
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgInS	odnést
mnoho	mnoho	k4c4	mnoho
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
historek	historka	k1gFnPc2	historka
<g/>
:	:	kIx,	:
jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
prý	prý	k9	prý
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
rodina	rodina	k1gFnSc1	rodina
objednala	objednat	k5eAaPmAgFnS	objednat
všechna	všechen	k3xTgNnPc4	všechen
jídla	jídlo	k1gNnPc4	jídlo
z	z	k7c2	z
jídelníčku	jídelníček	k1gInSc6	jídelníček
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
je	být	k5eAaImIp3nS	být
ochutnala	ochutnat	k5eAaPmAgFnS	ochutnat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
nechala	nechat	k5eAaPmAgFnS	nechat
přinést	přinést	k5eAaPmF	přinést
hamburgery	hamburger	k1gInPc4	hamburger
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
zase	zase	k9	zase
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
patro	patro	k1gNnSc4	patro
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Adams	Adams	k1gInSc1	Adams
hlídal	hlídat	k5eAaImAgInS	hlídat
<g/>
,	,	kIx,	,
poslána	poslán	k2eAgFnSc1d1	poslána
prostitutka	prostitutka	k1gFnSc1	prostitutka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
o	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
později	pozdě	k6eAd2	pozdě
odcházela	odcházet	k5eAaImAgFnS	odcházet
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
údajně	údajně	k6eAd1	údajně
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vy	vy	k3xPp2nPc1	vy
si	se	k3xPyFc3	se
alespoň	alespoň	k9	alespoň
můžete	moct	k5eAaImIp2nP	moct
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
číst	číst	k5eAaImF	číst
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
společně	společně	k6eAd1	společně
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Lloydem	Lloyd	k1gMnSc7	Lloyd
napsal	napsat	k5eAaBmAgMnS	napsat
scénář	scénář	k1gInSc4	scénář
ke	k	k7c3	k
dvěma	dva	k4xCgFnPc3	dva
půlhodinovým	půlhodinový	k2eAgFnPc3d1	půlhodinová
epizodám	epizoda	k1gFnPc3	epizoda
seriálu	seriál	k1gInSc2	seriál
Doctor	Doctor	k1gMnSc1	Doctor
Snuggles	Snuggles	k1gMnSc1	Snuggles
(	(	kIx(	(
<g/>
epizody	epizoda	k1gFnPc1	epizoda
číslo	číslo	k1gNnSc4	číslo
sedm	sedm	k4xCc1	sedm
a	a	k8xC	a
dvanáct	dvanáct	k4xCc1	dvanáct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lloyd	Lloyd	k6eAd1	Lloyd
se	se	k3xPyFc4	se
také	také	k9	také
spolupodílel	spolupodílet	k5eAaImAgMnS	spolupodílet
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
epizodách	epizoda	k1gFnPc6	epizoda
původní	původní	k2eAgFnSc2d1	původní
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
verze	verze	k1gFnSc2	verze
Stopaře	stopař	k1gMnSc2	stopař
(	(	kIx(	(
<g/>
epizody	epizoda	k1gFnSc2	epizoda
pět	pět	k4xCc4	pět
a	a	k8xC	a
šest	šest	k4xCc4	šest
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
na	na	k7c6	na
Smyslu	smysl	k1gInSc6	smysl
Životic	Životice	k1gFnPc2	Životice
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Meaning	Meaning	k1gInSc1	Meaning
of	of	k?	of
Liff	Liff	k1gInSc1	Liff
<g/>
,	,	kIx,	,
pokračování	pokračování	k1gNnSc1	pokračování
The	The	k1gFnPc2	The
Deeper	Deepra	k1gFnPc2	Deepra
Meaning	Meaning	k1gInSc1	Meaning
of	of	k?	of
Liff	Liff	k1gInSc4	Liff
zatím	zatím	k6eAd1	zatím
nebylo	být	k5eNaImAgNnS	být
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lloyd	Lloyd	k1gInSc1	Lloyd
a	a	k8xC	a
Adams	Adams	k1gInSc1	Adams
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
setkali	setkat	k5eAaPmAgMnP	setkat
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
komediálního	komediální	k2eAgNnSc2d1	komediální
sci-fi	scii	k1gNnSc2	sci-fi
filmu	film	k1gInSc2	film
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
Guinnessově	Guinnessův	k2eAgFnSc6d1	Guinnessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
měl	mít	k5eAaImAgMnS	mít
John	John	k1gMnSc1	John
Cleese	Cleese	k1gFnSc2	Cleese
hrát	hrát	k5eAaImF	hrát
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
poráženi	porážen	k2eAgMnPc1d1	porážen
mimozemšťany	mimozemšťan	k1gMnPc7	mimozemšťan
v	v	k7c6	v
atletických	atletický	k2eAgInPc6d1	atletický
závodech	závod	k1gInPc6	závod
a	a	k8xC	a
vyhrávali	vyhrávat	k5eAaImAgMnP	vyhrávat
by	by	kYmCp3nP	by
jen	jen	k9	jen
v	v	k7c6	v
"	"	kIx"	"
<g/>
nesmyslných	smyslný	k2eNgFnPc6d1	nesmyslná
<g/>
"	"	kIx"	"
disciplínách	disciplína	k1gFnPc6	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
nikdy	nikdy	k6eAd1	nikdy
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
SFX	SFX	kA	SFX
Magazine	Magazin	k1gInSc5	Magazin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Adams	Adamsa	k1gFnPc2	Adamsa
o	o	k7c4	o
Lloydovi	Lloyda	k1gMnSc3	Lloyda
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
komediální	komediální	k2eAgMnSc1d1	komediální
producent	producent	k1gMnSc1	producent
par	para	k1gFnPc2	para
excellence	excellence	k1gFnSc2	excellence
<g/>
...	...	k?	...
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgFnPc7	který
jsem	být	k5eAaImIp1nS	být
rád	rád	k6eAd1	rád
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
strašně	strašně	k6eAd1	strašně
vtipní	vtipný	k2eAgMnPc1d1	vtipný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
první	první	k4xOgFnSc1	první
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
verze	verze	k1gFnSc1	verze
Stopaře	stopař	k1gMnSc2	stopař
stala	stát	k5eAaPmAgFnS	stát
známou	známá	k1gFnSc4	známá
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
nabídnuto	nabídnut	k2eAgNnSc1d1	nabídnuto
místo	místo	k1gNnSc1	místo
producenta	producent	k1gMnSc2	producent
v	v	k7c6	v
rádiu	rádius	k1gInSc6	rádius
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
Week	Week	k1gInSc4	Week
Ending	Ending	k1gInSc1	Ending
a	a	k8xC	a
pantomimě	pantomima	k1gFnSc3	pantomima
Black	Blacka	k1gFnPc2	Blacka
Cinderella	Cinderello	k1gNnSc2	Cinderello
Two	Two	k1gFnSc2	Two
Goes	Goesa	k1gFnPc2	Goesa
East	East	k1gMnSc1	East
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
BBC	BBC	kA	BBC
ukončil	ukončit	k5eAaPmAgInS	ukončit
po	po	k7c6	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
věnovat	věnovat	k5eAaPmF	věnovat
tvorbě	tvorba	k1gFnSc3	tvorba
scénáře	scénář	k1gInSc2	scénář
pro	pro	k7c4	pro
seriál	seriál	k1gInSc4	seriál
Doctor	Doctor	k1gMnSc1	Doctor
Who	Who	k1gMnSc1	Who
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
radikálním	radikální	k2eAgMnSc7d1	radikální
ateistou	ateista	k1gMnSc7	ateista
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
toto	tento	k3xDgNnSc4	tento
důrazné	důrazný	k2eAgNnSc4d1	důrazné
označení	označení	k1gNnSc4	označení
používal	používat	k5eAaImAgInS	používat
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyl	být	k5eNaImAgInS	být
tázán	tázat	k5eAaImNgInS	tázat
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
agnostik	agnostik	k1gMnSc1	agnostik
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
člověk	člověk	k1gMnSc1	člověk
skeptický	skeptický	k2eAgMnSc1d1	skeptický
vůči	vůči	k7c3	vůči
dogmatům	dogma	k1gNnPc3	dogma
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
prokazatelná	prokazatelný	k2eAgFnSc1d1	prokazatelná
smyslovými	smyslový	k2eAgFnPc7d1	smyslová
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
American	American	k1gInSc4	American
Atheists	Atheistsa	k1gFnPc2	Atheistsa
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
vysvětlování	vysvětlování	k1gNnPc4	vysvětlování
ohledně	ohledně	k7c2	ohledně
jeho	on	k3xPp3gInSc2	on
postoje	postoj	k1gInSc2	postoj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
to	ten	k3xDgNnSc1	ten
druhým	druhý	k4xOgNnSc7	druhý
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
za	za	k7c7	za
svým	svůj	k3xOyFgInSc7	svůj
názorem	názor	k1gInSc7	názor
opravdu	opravdu	k6eAd1	opravdu
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
a	a	k8xC	a
myslí	myslet	k5eAaImIp3nP	myslet
ho	on	k3xPp3gInSc4	on
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
</s>
<s>
Adams	Adams	k6eAd1	Adams
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bůh	bůh	k1gMnSc1	bůh
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
;	;	kIx,	;
raději	rád	k6eAd2	rád
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
pozemskými	pozemský	k2eAgFnPc7d1	pozemská
záležitostmi	záležitost	k1gFnPc7	záležitost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ochranou	ochrana	k1gFnSc7	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomuto	tento	k3xDgNnSc3	tento
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
rozhovoru	rozhovor	k1gInSc6	rozhovor
také	také	k9	také
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
náboženství	náboženství	k1gNnSc4	náboženství
fascinuje	fascinovat	k5eAaBmIp3nS	fascinovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Strašně	strašně	k6eAd1	strašně
rád	rád	k2eAgMnSc1d1	rád
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
šťourám	šťourat	k5eAaImIp1nS	šťourat
a	a	k8xC	a
vrtám	vrtat	k5eAaImIp1nS	vrtat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
ta	ten	k3xDgNnPc4	ten
léta	léto	k1gNnPc4	léto
jsem	být	k5eAaImIp1nS	být
o	o	k7c6	o
náboženství	náboženství	k1gNnSc6	náboženství
přemýšlel	přemýšlet	k5eAaImAgInS	přemýšlet
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nutně	nutně	k6eAd1	nutně
muselo	muset	k5eAaImAgNnS	muset
projevit	projevit	k5eAaPmF	projevit
v	v	k7c6	v
mé	můj	k3xOp1gFnSc6	můj
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zaujatost	zaujatost	k1gFnSc1	zaujatost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k6eAd1	mnoho
"	"	kIx"	"
<g/>
jinak	jinak	k6eAd1	jinak
rozumných	rozumný	k2eAgInPc2d1	rozumný
<g/>
...	...	k?	...
a	a	k8xC	a
inteligentních	inteligentní	k2eAgMnPc2d1	inteligentní
lidí	člověk	k1gMnPc2	člověk
<g/>
...	...	k?	...
může	moct	k5eAaImIp3nS	moct
brát	brát	k5eAaImF	brát
[	[	kIx(	[
<g/>
existenci	existence	k1gFnSc4	existence
boha	bůh	k1gMnSc2	bůh
<g/>
]	]	kIx)	]
smrtelně	smrtelně	k6eAd1	smrtelně
vážně	vážně	k6eAd1	vážně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
příměrů	příměr	k1gInPc2	příměr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
použil	použít	k5eAaPmAgMnS	použít
pro	pro	k7c4	pro
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
o	o	k7c6	o
"	"	kIx"	"
<g/>
vnímající	vnímající	k2eAgFnSc6d1	vnímající
louži	louž	k1gFnSc6	louž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
uspořádání	uspořádání	k1gNnSc1	uspořádání
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tak	tak	k6eAd1	tak
dokonale	dokonale	k6eAd1	dokonale
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
našim	náš	k3xOp1gFnPc3	náš
potřebám	potřeba	k1gFnPc3	potřeba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jasným	jasný	k2eAgInSc7d1	jasný
důkazem	důkaz	k1gInSc7	důkaz
existence	existence	k1gFnSc2	existence
milujícího	milující	k2eAgMnSc4d1	milující
boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
smýšlející	smýšlející	k2eAgMnPc4d1	smýšlející
lidi	člověk	k1gMnPc4	člověk
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
k	k	k7c3	k
inteligentní	inteligentní	k2eAgFnSc3d1	inteligentní
kaluži	kaluž	k1gFnSc3	kaluž
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
také	také	k9	také
jistá	jistý	k2eAgFnSc1d1	jistá
<g/>
,	,	kIx,	,
že	že	k8xS	že
díra	díra	k1gFnSc1	díra
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zaplňuje	zaplňovat	k5eAaImIp3nS	zaplňovat
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
speciálně	speciálně	k6eAd1	speciálně
kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jí	on	k3xPp3gFnSc3	on
sedí	sedit	k5eAaImIp3nS	sedit
jak	jak	k6eAd1	jak
šitá	šitý	k2eAgFnSc1d1	šitá
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
louže	louže	k1gFnSc1	louže
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
úplně	úplně	k6eAd1	úplně
nevypaří	vypařit	k5eNaPmIp3nP	vypařit
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
environmentálního	environmentální	k2eAgMnSc4d1	environmentální
aktivistu	aktivista	k1gMnSc4	aktivista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
uvedl	uvést	k5eAaPmAgInS	uvést
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
seriál	seriál	k1gInSc1	seriál
Ještě	ještě	k6eAd1	ještě
je	on	k3xPp3gInPc4	on
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
s	s	k7c7	s
přírodovědcem	přírodovědec	k1gMnSc7	přírodovědec
Markem	Marek	k1gMnSc7	Marek
Carwardinem	Carwardin	k1gMnSc7	Carwardin
vydali	vydat	k5eAaPmAgMnP	vydat
pozorovat	pozorovat	k5eAaImF	pozorovat
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgMnPc7d1	jiný
například	například	k6eAd1	například
papouška	papoušek	k1gMnSc2	papoušek
kakapo	kakapa	k1gFnSc5	kakapa
nebo	nebo	k8xC	nebo
delfínovce	delfínovec	k1gMnSc2	delfínovec
čínského	čínský	k2eAgMnSc2d1	čínský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
také	také	k9	také
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
multimediální	multimediální	k2eAgFnSc4d1	multimediální
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
kombinující	kombinující	k2eAgFnSc4d1	kombinující
namluvenou	namluvený	k2eAgFnSc4d1	namluvená
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
text	text	k1gInSc4	text
a	a	k8xC	a
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
a	a	k8xC	a
Carwardinovy	Carwardinův	k2eAgFnSc2d1	Carwardinův
série	série	k1gFnSc2	série
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
gorilou	gorila	k1gFnSc7	gorila
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Projekt	projekt	k1gInSc1	projekt
lidoop	lidoop	k1gMnSc1	lidoop
(	(	kIx(	(
<g/>
The	The	k1gFnSc6	The
Great	Great	k2eAgMnSc1d1	Great
Ape	Ape	k1gMnSc1	Ape
Project	Project	k1gMnSc1	Project
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
publikace	publikace	k1gFnSc1	publikace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
většího	veliký	k2eAgInSc2d2	veliký
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
snažícího	snažící	k2eAgMnSc2d1	snažící
se	se	k3xPyFc4	se
o	o	k7c4	o
morální	morální	k2eAgNnSc4d1	morální
zrovnoprávnění	zrovnoprávnění	k1gNnSc4	zrovnoprávnění
všech	všecek	k3xTgFnPc2	všecek
velkých	velký	k2eAgFnPc2d1	velká
opic	opice	k1gFnPc2	opice
(	(	kIx(	(
<g/>
lidské	lidský	k2eAgFnPc1d1	lidská
i	i	k8xC	i
zvířecí	zvířecí	k2eAgFnPc1d1	zvířecí
větve	větev	k1gFnPc1	větev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
výstupu	výstup	k1gInSc3	výstup
na	na	k7c4	na
Kilimandžáro	Kilimandžáro	k1gNnSc4	Kilimandžáro
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
převlečený	převlečený	k2eAgInSc1d1	převlečený
za	za	k7c4	za
nosorožce	nosorožec	k1gMnPc4	nosorožec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
nosorožce	nosorožec	k1gMnPc4	nosorožec
(	(	kIx(	(
<g/>
Save	Save	k1gFnSc1	Save
the	the	k?	the
Rhino	Rhino	k1gNnSc1	Rhino
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
britské	britský	k2eAgFnSc2d1	britská
charitativní	charitativní	k2eAgFnSc2d1	charitativní
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
mnoho	mnoho	k6eAd1	mnoho
různých	různý	k2eAgMnPc2d1	různý
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
si	se	k3xPyFc3	se
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
kostým	kostým	k1gInSc4	kostým
nosorožce	nosorožec	k1gMnSc2	nosorožec
oblékl	obléct	k5eAaPmAgMnS	obléct
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
ho	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
sobě	se	k3xPyFc3	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
výstupem	výstup	k1gInSc7	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
akci	akce	k1gFnSc3	akce
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vybrat	vybrat	k5eAaPmF	vybrat
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
000	[number]	k4	000
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
a	a	k8xC	a
financování	financování	k1gNnSc6	financování
tanzanského	tanzanský	k2eAgInSc2d1	tanzanský
programu	program	k1gInSc2	program
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
nosorožce	nosorožec	k1gMnSc2	nosorožec
dvourohého	dvourohý	k2eAgMnSc2d1	dvourohý
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
také	také	k9	také
podporoval	podporovat	k5eAaImAgInS	podporovat
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
fond	fond	k1gInSc1	fond
Dian	Diana	k1gFnPc2	Diana
Fosseyové	Fosseyové	k2eAgFnPc2d1	Fosseyové
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
goril	gorila	k1gFnPc2	gorila
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
si	se	k3xPyFc3	se
první	první	k4xOgMnSc1	první
textový	textový	k2eAgInSc1d1	textový
editor	editor	k1gInSc1	editor
pořídil	pořídit	k5eAaPmAgInS	pořídit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
koupi	koupě	k1gFnSc6	koupě
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
měl	mít	k5eAaImAgInS	mít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
"	"	kIx"	"
<g/>
potíže	potíž	k1gFnPc4	potíž
proniknout	proniknout	k5eAaPmF	proniknout
za	za	k7c4	za
brány	brána	k1gFnPc4	brána
žargonu	žargon	k1gInSc2	žargon
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc4	slovo
si	se	k3xPyFc3	se
poletovala	poletovat	k5eAaImAgFnS	poletovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
zamanulo	zamanout	k5eAaPmAgNnS	zamanout
<g/>
,	,	kIx,	,
a	a	k8xC	a
nějaká	nějaký	k3yIgNnPc4	nějaký
pravidla	pravidlo	k1gNnPc4	pravidlo
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
nezajímala	zajímat	k5eNaImAgFnS	zajímat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nejprve	nejprve	k6eAd1	nejprve
si	se	k3xPyFc3	se
koupil	koupit	k5eAaPmAgMnS	koupit
"	"	kIx"	"
<g/>
Nexus	nexus	k1gInSc1	nexus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
s	s	k7c7	s
Jane	Jan	k1gMnSc5	Jan
Belsonovou	Belsonová	k1gFnSc7	Belsonová
vypravil	vypravit	k5eAaPmAgInS	vypravit
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
pořídil	pořídit	k5eAaPmAgInS	pořídit
DEC	DEC	kA	DEC
Rainbow	Rainbow	k1gFnSc2	Rainbow
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
se	se	k3xPyFc4	se
vybavil	vybavit	k5eAaPmAgInS	vybavit
ještě	ještě	k9	ještě
značkou	značka	k1gFnSc7	značka
Apricot	Apricota	k1gFnPc2	Apricota
<g/>
,	,	kIx,	,
následovanou	následovaný	k2eAgFnSc4d1	následovaná
BBC	BBC	kA	BBC
Micro	Micro	k1gNnSc1	Micro
a	a	k8xC	a
Tandy	Tanda	k1gFnPc1	Tanda
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Ještě	ještě	k6eAd1	ještě
je	on	k3xPp3gInPc4	on
můžete	moct	k5eAaImIp2nP	moct
vidět	vidět	k5eAaImF	vidět
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
také	také	k9	také
Cambridge	Cambridge	k1gFnSc1	Cambridge
Z	z	k7c2	z
<g/>
88	[number]	k4	88
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
přivezl	přivézt	k5eAaPmAgMnS	přivézt
do	do	k7c2	do
Zairu	Zair	k1gInSc2	Zair
při	při	k7c6	při
výpravě	výprava	k1gFnSc6	výprava
za	za	k7c7	za
severními	severní	k2eAgMnPc7d1	severní
bílými	bílý	k2eAgMnPc7d1	bílý
nosorožci	nosorožec	k1gMnPc7	nosorožec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Losos	losos	k1gMnSc1	losos
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
několik	několik	k4yIc4	několik
jeho	jeho	k3xOp3gInPc2	jeho
článků	článek	k1gInPc2	článek
o	o	k7c6	o
technologiích	technologie	k1gFnPc6	technologie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
MacUser	MacUser	k1gMnSc1	MacUser
a	a	k8xC	a
The	The	k1gMnSc1	The
Independent	independent	k1gMnSc1	independent
on	on	k3xPp3gMnSc1	on
Sunday	Sunda	k2eAgFnPc1d1	Sunda
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc1	první
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kdy	kdy	k6eAd1	kdy
viděl	vidět	k5eAaImAgInS	vidět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Commodore	Commodor	k1gInSc5	Commodor
PET	PET	kA	PET
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
poprvé	poprvé	k6eAd1	poprvé
spatřil	spatřit	k5eAaPmAgMnS	spatřit
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
sídle	sídlo	k1gNnSc6	sídlo
Infocomu	Infocom	k1gInSc2	Infocom
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
celoživotní	celoživotní	k2eAgFnSc1d1	celoživotní
láska	láska	k1gFnSc1	láska
ke	k	k7c3	k
značce	značka	k1gFnSc3	značka
Apple	Apple	kA	Apple
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
sblížil	sblížit	k5eAaPmAgMnS	sblížit
se	s	k7c7	s
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
Sally	Salla	k1gFnSc2	Salla
Emerson	Emerson	k1gInSc1	Emerson
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
žila	žít	k5eAaImAgFnS	žít
odloučeně	odloučeně	k6eAd1	odloučeně
od	od	k7c2	od
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jí	on	k3xPp3gFnSc3	on
věnoval	věnovat	k5eAaPmAgMnS	věnovat
svou	svůj	k3xOyFgFnSc4	svůj
knihu	kniha	k1gFnSc4	kniha
Život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
vesmír	vesmír	k1gInSc1	vesmír
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
však	však	k9	však
Sally	Sall	k1gInPc1	Sall
Emersonová	Emersonová	k1gFnSc1	Emersonová
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
manželovi	manžel	k1gMnSc3	manžel
<g/>
,	,	kIx,	,
Peteru	Peter	k1gMnSc3	Peter
Stothardovi	Stothard	k1gMnSc3	Stothard
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
Adams	Adams	k1gInSc1	Adams
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
brentwoodskou	brentwoodský	k2eAgFnSc4d1	brentwoodský
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
přátelé	přítel	k1gMnPc1	přítel
seznámili	seznámit	k5eAaPmAgMnP	seznámit
s	s	k7c7	s
Jane	Jan	k1gMnSc5	Jan
Belsonovou	Belsonový	k2eAgFnSc4d1	Belsonový
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
psal	psát	k5eAaImAgInS	psát
scénář	scénář	k1gInSc1	scénář
k	k	k7c3	k
filmové	filmový	k2eAgFnSc3d1	filmová
adaptaci	adaptace	k1gFnSc3	adaptace
Stopaře	stopař	k1gMnSc2	stopař
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
celá	celý	k2eAgFnSc1d1	celá
práce	práce	k1gFnSc1	práce
skončila	skončit	k5eAaPmAgFnS	skončit
fiaskem	fiasko	k1gNnSc7	fiasko
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
několika	několik	k4yIc6	několik
rozchodech	rozchod	k1gInPc6	rozchod
a	a	k8xC	a
jednom	jeden	k4xCgNnSc6	jeden
zrušeném	zrušený	k2eAgNnSc6d1	zrušené
zasnoubení	zasnoubení	k1gNnSc6	zasnoubení
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
oddáni	oddán	k2eAgMnPc1d1	oddán
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Polly	Polla	k1gFnSc2	Polla
Jane	Jan	k1gMnSc5	Jan
Rocket	Rocket	k1gInSc1	Rocket
Adams	Adamsa	k1gFnPc2	Adamsa
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
do	do	k7c2	do
Santa	Sant	k1gInSc2	Sant
Barbary	Barbara	k1gFnSc2	Barbara
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žili	žít	k5eAaImAgMnP	žít
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pohřbu	pohřeb	k1gInSc6	pohřeb
se	se	k3xPyFc4	se
Jane	Jan	k1gMnSc5	Jan
Belsonová	Belsonová	k1gFnSc1	Belsonová
a	a	k8xC	a
Polly	Polla	k1gFnPc1	Polla
Adamsová	Adamsová	k1gFnSc1	Adamsová
vrátily	vrátit	k5eAaPmAgFnP	vrátit
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jane	Jan	k1gMnSc5	Jan
Belsonová	Belsonový	k2eAgFnSc1d1	Belsonový
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
po	po	k7c6	po
cvičení	cvičení	k1gNnSc6	cvičení
v	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
tělocvičně	tělocvična	k1gFnSc6	tělocvična
v	v	k7c6	v
Montecitu	Montecit	k1gInSc6	Montecit
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
49	[number]	k4	49
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Trpěl	trpět	k5eAaImAgInS	trpět
neodhaleným	odhalený	k2eNgNnSc7d1	neodhalené
zužováním	zužování	k1gNnSc7	zužování
koronárních	koronární	k2eAgFnPc2d1	koronární
tepen	tepna	k1gFnPc2	tepna
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
infarktu	infarkt	k1gInSc3	infarkt
myokardu	myokard	k1gInSc2	myokard
a	a	k8xC	a
srdeční	srdeční	k2eAgFnSc4d1	srdeční
arytmii	arytmie	k1gFnSc4	arytmie
<g/>
.	.	kIx.	.
</s>
<s>
Adamsova	Adamsův	k2eAgInSc2d1	Adamsův
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
v	v	k7c4	v
Santa	Sant	k1gMnSc4	Sant
Barbaře	Barbara	k1gFnSc6	Barbara
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
několik	několik	k4yIc1	několik
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Schránka	schránka	k1gFnSc1	schránka
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
ostatky	ostatek	k1gInPc7	ostatek
byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Highgate	Highgat	k1gInSc5	Highgat
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
slavnost	slavnost	k1gFnSc1	slavnost
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2001	[number]	k4	2001
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
(	(	kIx(	(
<g/>
St.	st.	kA	st.
Martin-in-the-Fields	Martinnhe-Fields	k1gInSc1	Martin-in-the-Fields
<g/>
)	)	kIx)	)
na	na	k7c4	na
Trafalgar	Trafalgar	k1gInSc4	Trafalgar
Square	square	k1gInSc4	square
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc7	první
bohoslužbou	bohoslužba	k1gFnSc7	bohoslužba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
přenášena	přenášet	k5eAaImNgFnS	přenášet
živě	živě	k6eAd1	živě
přes	přes	k7c4	přes
Internet	Internet	k1gInSc4	Internet
televizí	televize	k1gFnPc2	televize
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2002	[number]	k4	2002
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
Losos	losos	k1gMnSc1	losos
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
esejí	esej	k1gFnPc2	esej
<g/>
,	,	kIx,	,
dopisů	dopis	k1gInPc2	dopis
a	a	k8xC	a
pochvalných	pochvalný	k2eAgInPc2d1	pochvalný
článků	článek	k1gInPc2	článek
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Richarda	Richard	k1gMnSc2	Richard
Dawkinse	Dawkins	k1gMnSc2	Dawkins
<g/>
,	,	kIx,	,
Stephena	Stephen	k1gMnSc2	Stephen
Frye	Fry	k1gMnSc2	Fry
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
vydání	vydání	k1gNnSc6	vydání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Christophera	Christopher	k1gMnSc4	Christopher
Cerfa	Cerf	k1gMnSc4	Cerf
(	(	kIx(	(
<g/>
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
vydání	vydání	k1gNnSc6	vydání
<g/>
)	)	kIx)	)
a	a	k8xC	a
Terryho	Terry	k1gMnSc2	Terry
Jonese	Jonese	k1gFnSc2	Jonese
(	(	kIx(	(
<g/>
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
brožovaném	brožovaný	k2eAgNnSc6d1	brožované
vydání	vydání	k1gNnSc6	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
také	také	k9	také
jedenáct	jedenáct	k4xCc1	jedenáct
kapitol	kapitola	k1gFnPc2	kapitola
z	z	k7c2	z
dlouho	dlouho	k6eAd1	dlouho
očekávaného	očekávaný	k2eAgMnSc2d1	očekávaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokončeného	dokončený	k2eNgInSc2d1	nedokončený
románu	román	k1gInSc2	román
Losos	losos	k1gMnSc1	losos
pochybnosti	pochybnost	k1gFnSc2	pochybnost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stal	stát	k5eAaPmAgInS	stát
posledním	poslední	k2eAgMnSc7d1	poslední
a	a	k8xC	a
závěrečným	závěrečný	k2eAgNnSc7d1	závěrečné
dílem	dílo	k1gNnSc7	dílo
Stopařova	stopařův	k2eAgMnSc2d1	stopařův
průvodce	průvodce	k1gMnSc2	průvodce
Galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
naznačoval	naznačovat	k5eAaImAgMnS	naznačovat
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
knize	kniha	k1gFnSc3	kniha
chybělo	chybět	k5eAaImAgNnS	chybět
ještě	ještě	k9	ještě
mnoho	mnoho	k4c1	mnoho
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
let	let	k1gInSc4	let
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
dokončení	dokončení	k1gNnSc3	dokončení
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
poctu	pocta	k1gFnSc4	pocta
Douglasovi	Douglas	k1gMnSc3	Douglas
Adamsovi	Adams	k1gMnSc3	Adams
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnSc3	jeho
Stopařově	stopařův	k2eAgFnSc3d1	Stopařova
Průvodci	průvodce	k1gMnSc3	průvodce
po	po	k7c6	po
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
fanoušci	fanoušek	k1gMnPc1	fanoušek
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
recesní	recesní	k2eAgFnSc4d1	recesní
vzpomínkovou	vzpomínkový	k2eAgFnSc4d1	vzpomínková
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Ručníkový	ručníkový	k2eAgInSc1d1	ručníkový
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgInPc4d1	spočívající
v	v	k7c6	v
celodenním	celodenní	k2eAgNnSc6d1	celodenní
viditelném	viditelný	k2eAgNnSc6d1	viditelné
nošení	nošení	k1gNnSc6	nošení
ručníku	ručník	k1gInSc2	ručník
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stala	stát	k5eAaPmAgFnS	stát
každoroční	každoroční	k2eAgFnSc7d1	každoroční
tradicí	tradice	k1gFnSc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
Galaxií	galaxie	k1gFnPc2	galaxie
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Hitchhiker	Hitchhiker	k1gMnSc1	Hitchhiker
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Galaxy	Galax	k1gInPc1	Galax
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Restaurant	restaurant	k1gInSc1	restaurant
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Restaurant	restaurant	k1gInSc1	restaurant
at	at	k?	at
the	the	k?	the
End	End	k1gFnSc2	End
of	of	k?	of
Universe	Universe	k1gFnSc2	Universe
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
vesmír	vesmír	k1gInSc1	vesmír
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Life	Life	k1gFnSc1	Life
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
Universe	Universe	k1gFnSc1	Universe
and	and	k?	and
Everything	Everything	k1gInSc1	Everything
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	dík	k1gInPc1	dík
za	za	k7c4	za
ryby	ryba	k1gFnPc4	ryba
(	(	kIx(	(
<g/>
So	So	kA	So
Long	Longa	k1gFnPc2	Longa
<g/>
,	,	kIx,	,
And	Anda	k1gFnPc2	Anda
Thanks	Thanks	k1gInSc4	Thanks
for	forum	k1gNnPc2	forum
All	All	k1gMnSc2	All
the	the	k?	the
Fish	Fish	k1gMnSc1	Fish
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Převážně	převážně	k6eAd1	převážně
neškodná	škodný	k2eNgFnSc1d1	neškodná
(	(	kIx(	(
<g/>
Mostly	Mostly	k1gFnSc1	Mostly
Harmless	Harmlessa	k1gFnPc2	Harmlessa
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
povídka	povídka	k1gFnSc1	povídka
Young	Younga	k1gFnPc2	Younga
Zaphod	Zaphoda	k1gFnPc2	Zaphoda
Plays	Plays	k1gInSc1	Plays
It	It	k1gMnSc1	It
Safe	safe	k1gInSc1	safe
(	(	kIx(	(
<g/>
Mladý	mladý	k2eAgInSc1d1	mladý
Zafod	Zafod	k1gInSc1	Zafod
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
jistotu	jistota	k1gFnSc4	jistota
–	–	k?	–
antologie	antologie	k1gFnSc1	antologie
Divodějní	Divodějní	k2eAgFnSc1d1	Divodějní
<g/>
,	,	kIx,	,
Talpress	Talpress	k1gInSc1	Talpress
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Komediální	komediální	k2eAgFnSc1d1	komediální
sci-fi	scii	k1gFnSc1	sci-fi
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
série	série	k1gFnSc2	série
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
Galaxií	galaxie	k1gFnPc2	galaxie
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
pro	pro	k7c4	pro
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc4	radio
4	[number]	k4	4
Douglasem	Douglas	k1gMnSc7	Douglas
Adamsem	Adams	k1gMnSc7	Adams
a	a	k8xC	a
producentem	producent	k1gMnSc7	producent
Simonem	Simon	k1gMnSc7	Simon
Brettem	Brett	k1gMnSc7	Brett
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Adams	Adams	k6eAd1	Adams
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pilotní	pilotní	k2eAgInSc4d1	pilotní
díl	díl	k1gInSc4	díl
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
přetištěny	přetisknout	k5eAaPmNgFnP	přetisknout
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Neila	Neil	k1gMnSc2	Neil
Gaimana	Gaiman	k1gMnSc2	Gaiman
Nepropadejte	propadat	k5eNaPmRp2nP	propadat
panice	panice	k1gFnPc4	panice
<g/>
:	:	kIx,	:
Douglas	Douglas	k1gInSc4	Douglas
Adams	Adamsa	k1gFnPc2	Adamsa
a	a	k8xC	a
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
Galaxií	galaxie	k1gFnPc2	galaxie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
teoreticky	teoreticky	k6eAd1	teoreticky
v	v	k7c4	v
sérii	série	k1gFnSc4	série
použity	použít	k5eAaPmNgInP	použít
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
Adams	Adams	k1gInSc4	Adams
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
napsat	napsat	k5eAaPmF	napsat
Stopařova	stopařův	k2eAgMnSc4d1	stopařův
průvodce	průvodce	k1gMnSc4	průvodce
Galaxií	galaxie	k1gFnSc7	galaxie
ho	on	k3xPp3gMnSc4	on
napadlo	napadnout	k5eAaPmAgNnS	napadnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
ležel	ležet	k5eAaImAgMnS	ležet
opilý	opilý	k2eAgMnSc1d1	opilý
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
a	a	k8xC	a
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
hvězdy	hvězda	k1gFnPc4	hvězda
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
Adams	Adams	k1gInSc1	Adams
rád	rád	k6eAd1	rád
vtipkoval	vtipkovat	k5eAaImAgInS	vtipkovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
BBC	BBC	kA	BBC
Rakousko	Rakousko	k1gNnSc1	Rakousko
by	by	kYmCp3nS	by
určitě	určitě	k6eAd1	určitě
tvrdilo	tvrdit	k5eAaImAgNnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
snadnější	snadný	k2eAgInSc1d2	snadnější
pravopis	pravopis	k1gInSc1	pravopis
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
toulkách	toulka	k1gFnPc6	toulka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgMnS	mít
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
i	i	k9	i
knihu	kniha	k1gFnSc4	kniha
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
buď	budit	k5eAaImRp2nS	budit
"	"	kIx"	"
<g/>
hluší	hluchý	k2eAgMnPc1d1	hluchý
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
hloupí	hloupit	k5eAaImIp3nP	hloupit
<g/>
"	"	kIx"	"
a	a	k8xC	a
nebo	nebo	k8xC	nebo
mluvili	mluvit	k5eAaImAgMnP	mluvit
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
nerozuměl	rozumět	k5eNaImAgMnS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Adams	Adams	k6eAd1	Adams
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
přiopilý	přiopilý	k2eAgMnSc1d1	přiopilý
vydal	vydat	k5eAaPmAgMnS	vydat
přespat	přespat	k5eAaPmF	přespat
doprostřed	doprostřed	k7c2	doprostřed
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
právě	právě	k9	právě
jeho	jeho	k3xOp3gFnSc4	jeho
neschopnost	neschopnost	k1gFnSc4	neschopnost
domluvit	domluvit	k5eAaPmF	domluvit
se	se	k3xPyFc4	se
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
mu	on	k3xPp3gMnSc3	on
vnukla	vnuknout	k5eAaPmAgFnS	vnuknout
myšlenku	myšlenka	k1gFnSc4	myšlenka
napsat	napsat	k5eAaBmF	napsat
Stopaře	stopař	k1gMnPc4	stopař
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
později	pozdě	k6eAd2	pozdě
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ustavičným	ustavičný	k2eAgNnSc7d1	ustavičné
převypravováním	převypravování	k1gNnSc7	převypravování
této	tento	k3xDgFnSc2	tento
historky	historka	k1gFnSc2	historka
vlastně	vlastně	k9	vlastně
úplně	úplně	k6eAd1	úplně
zapomněl	zapomnět	k5eAaImAgMnS	zapomnět
na	na	k7c4	na
samotný	samotný	k2eAgInSc4d1	samotný
moment	moment	k1gInSc4	moment
inspirace	inspirace	k1gFnSc2	inspirace
a	a	k8xC	a
pamatoval	pamatovat	k5eAaImAgInS	pamatovat
si	se	k3xPyFc3	se
už	už	k6eAd1	už
jen	jen	k9	jen
jeho	jeho	k3xOp3gNnSc4	jeho
převypravování	převypravování	k1gNnSc4	převypravování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dodatku	dodatek	k1gInSc6	dodatek
k	k	k7c3	k
Adamsově	Adamsův	k2eAgFnSc3d1	Adamsova
životopisu	životopis	k1gInSc2	životopis
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
napsal	napsat	k5eAaPmAgMnS	napsat
M.	M.	kA	M.
J.	J.	kA	J.
Simpson	Simpsona	k1gFnPc2	Simpsona
<g/>
,	,	kIx,	,
však	však	k9	však
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Adamsova	Adamsův	k2eAgFnSc1d1	Adamsova
historka	historka	k1gFnSc1	historka
je	být	k5eAaImIp3nS	být
holý	holý	k2eAgInSc4d1	holý
výmysl	výmysl	k1gInSc4	výmysl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
autor	autor	k1gMnSc1	autor
začal	začít	k5eAaPmAgMnS	začít
o	o	k7c6	o
napsání	napsání	k1gNnSc6	napsání
Stopaře	stopař	k1gMnSc4	stopař
uvažovat	uvažovat	k5eAaImF	uvažovat
až	až	k9	až
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Adamsovi	Adams	k1gMnSc6	Adams
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
své	svůj	k3xOyFgInPc4	svůj
příběhy	příběh	k1gInPc4	příběh
vymýšlel	vymýšlet	k5eAaImAgMnS	vymýšlet
za	za	k7c2	za
pochodu	pochod	k1gInSc2	pochod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
posledních	poslední	k2eAgFnPc2d1	poslední
dvou	dva	k4xCgFnPc2	dva
epizod	epizoda	k1gFnPc2	epizoda
první	první	k4xOgFnSc2	první
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
však	však	k9	však
obrátil	obrátit	k5eAaPmAgMnS	obrátit
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c4	na
Johna	John	k1gMnSc4	John
Lloyda	Lloyd	k1gMnSc4	Lloyd
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
následně	následně	k6eAd1	následně
přispěl	přispět	k5eAaPmAgMnS	přispět
několika	několik	k4yIc7	několik
texty	text	k1gInPc7	text
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nevydané	vydaný	k2eNgFnSc2d1	nevydaná
knihy	kniha	k1gFnSc2	kniha
GiGax	GiGax	k1gInSc1	GiGax
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
a	a	k8xC	a
televizní	televizní	k2eAgFnSc6d1	televizní
adaptaci	adaptace	k1gFnSc6	adaptace
Stopaře	stopař	k1gMnPc4	stopař
ale	ale	k8xC	ale
z	z	k7c2	z
Lloydových	Lloydův	k2eAgInPc2d1	Lloydův
materiálů	materiál	k1gInPc2	materiál
zbylo	zbýt	k5eAaPmAgNnS	zbýt
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
podklad	podklad	k1gInSc4	podklad
pro	pro	k7c4	pro
televizní	televizní	k2eAgFnSc4d1	televizní
adaptaci	adaptace	k1gFnSc4	adaptace
sice	sice	k8xC	sice
byly	být	k5eAaImAgFnP	být
použito	použít	k5eAaPmNgNnS	použít
prvních	první	k4xOgNnPc6	první
šest	šest	k4xCc4	šest
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Lloydovy	Lloydův	k2eAgInPc1d1	Lloydův
příspěvky	příspěvek	k1gInPc1	příspěvek
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
přepsány	přepsán	k2eAgInPc4d1	přepsán
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
sérii	série	k1gFnSc4	série
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
vysílána	vysílat	k5eAaImNgFnS	vysílat
každý	každý	k3xTgInSc4	každý
týden	týden	k1gInSc4	týden
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
dubnu	duben	k1gInSc6	duben
1978	[number]	k4	1978
rádiem	rádio	k1gNnSc7	rádio
BBC	BBC	kA	BBC
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
velkém	velký	k2eAgInSc6d1	velký
úspěchu	úspěch	k1gInSc6	úspěch
byla	být	k5eAaImAgFnS	být
nahrána	nahrát	k5eAaPmNgFnS	nahrát
a	a	k8xC	a
odvysílána	odvysílán	k2eAgFnSc1d1	odvysílána
druhá	druhý	k4xOgFnSc1	druhý
epizoda	epizoda	k1gFnSc1	epizoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
přízvisko	přízvisko	k1gNnSc4	přízvisko
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
pěti	pět	k4xCc2	pět
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
rozhlasové	rozhlasový	k2eAgFnSc6d1	rozhlasová
sérii	série	k1gFnSc6	série
(	(	kIx(	(
<g/>
a	a	k8xC	a
několika	několik	k4yIc7	několik
dalších	další	k2eAgInPc6d1	další
projektech	projekt	k1gInPc6	projekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
Pirátská	pirátský	k2eAgFnSc1d1	pirátská
planeta	planeta	k1gFnSc1	planeta
<g/>
)	)	kIx)	)
Adams	Adams	k1gInSc1	Adams
zabředl	zabřednout	k5eAaPmAgInS	zabřednout
do	do	k7c2	do
problémů	problém	k1gInPc2	problém
se	s	k7c7	s
stíháním	stíhání	k1gNnSc7	stíhání
uzávěrek	uzávěrka	k1gFnPc2	uzávěrka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
jen	jen	k6eAd1	jen
prohloubily	prohloubit	k5eAaPmAgFnP	prohloubit
<g/>
,	,	kIx,	,
když	když	k8xS	když
vydával	vydávat	k5eAaImAgMnS	vydávat
své	svůj	k3xOyFgInPc4	svůj
romány	román	k1gInPc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Adams	Adams	k1gInSc1	Adams
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
aktivní	aktivní	k2eAgMnSc1d1	aktivní
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
do	do	k7c2	do
psaní	psaní	k1gNnSc2	psaní
ho	on	k3xPp3gMnSc4	on
museli	muset	k5eAaImAgMnP	muset
ostatní	ostatní	k2eAgMnPc1d1	ostatní
nutit	nutit	k5eAaImF	nutit
<g/>
.	.	kIx.	.
</s>
<s>
Včetně	včetně	k7c2	včetně
případu	případ	k1gInSc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
zavřený	zavřený	k2eAgInSc4d1	zavřený
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
hotelovém	hotelový	k2eAgInSc6d1	hotelový
pokoji	pokoj	k1gInSc6	pokoj
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
vydavatelem	vydavatel	k1gMnSc7	vydavatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
stihl	stihnout	k5eAaPmAgMnS	stihnout
knihu	kniha	k1gFnSc4	kniha
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	dík	k1gInPc1	dík
za	za	k7c4	za
ryby	ryba	k1gFnPc4	ryba
dopsat	dopsat	k5eAaPmF	dopsat
včas	včas	k6eAd1	včas
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k9	jednou
prý	prý	k9	prý
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Miluju	milovat	k5eAaImIp1nS	milovat
uzávěrky	uzávěrka	k1gFnSc2	uzávěrka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
ten	ten	k3xDgInSc1	ten
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vydávají	vydávat	k5eAaImIp3nP	vydávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
prosviští	prosvištět	k5eAaPmIp3nS	prosvištět
kolem	kolem	k6eAd1	kolem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Navzdory	navzdory	k7c3	navzdory
těmto	tento	k3xDgInPc3	tento
problémům	problém	k1gInPc3	problém
nakonec	nakonec	k6eAd1	nakonec
Adams	Adamsa	k1gFnPc2	Adamsa
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sérii	série	k1gFnSc6	série
napsal	napsat	k5eAaBmAgMnS	napsat
pět	pět	k4xCc4	pět
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
a	a	k8xC	a
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
adaptace	adaptace	k1gFnPc4	adaptace
-	-	kIx~	-
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
tří	tři	k4xCgFnPc2	tři
knih	kniha	k1gFnPc2	kniha
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
komiksové	komiksový	k2eAgFnSc2d1	komiksová
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
také	také	k9	také
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
textové	textový	k2eAgFnSc2d1	textová
adventury	adventura	k1gFnSc2	adventura
a	a	k8xC	a
knižní	knižní	k2eAgFnSc1d1	knižní
verze	verze	k1gFnSc1	verze
doplněná	doplněný	k2eAgFnSc1d1	doplněná
fotografiemi	fotografia	k1gFnPc7	fotografia
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
se	se	k3xPyFc4	se
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
publikací	publikace	k1gFnPc2	publikace
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
puzzle	puzzle	k1gNnPc4	puzzle
"	"	kIx"	"
<g/>
42	[number]	k4	42
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
navržené	navržený	k2eAgNnSc1d1	navržené
Adamsem	Adamso	k1gNnSc7	Adamso
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
zachyceno	zachytit	k5eAaPmNgNnS	zachytit
na	na	k7c6	na
obálce	obálka	k1gFnSc6	obálka
prvních	první	k4xOgInPc2	první
čtyř	čtyři	k4xCgInPc2	čtyři
"	"	kIx"	"
<g/>
stopařských	stopařský	k2eAgInPc2d1	stopařský
<g/>
"	"	kIx"	"
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
Adams	Adams	k1gInSc1	Adams
začal	začít	k5eAaPmAgInS	začít
pokoušet	pokoušet	k5eAaImF	pokoušet
o	o	k7c6	o
převedení	převedení	k1gNnSc6	převedení
Stopaře	stopař	k1gMnSc2	stopař
na	na	k7c4	na
filmová	filmový	k2eAgNnPc4d1	filmové
plátna	plátno	k1gNnPc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgNnSc2	tento
snažení	snažení	k1gNnSc2	snažení
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
návštěv	návštěva	k1gFnPc2	návštěva
Los	los	k1gInSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
četnými	četný	k2eAgNnPc7d1	četné
hollywoodskými	hollywoodský	k2eAgNnPc7d1	hollywoodské
studii	studio	k1gNnPc7	studio
a	a	k8xC	a
možnými	možný	k2eAgMnPc7d1	možný
producenty	producent	k1gMnPc7	producent
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
verze	verze	k1gFnSc1	verze
stala	stát	k5eAaPmAgFnS	stát
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
televizní	televizní	k2eAgFnSc4d1	televizní
mini-sérii	miniérie	k1gFnSc4	mini-série
BBC	BBC	kA	BBC
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
Galaxií	galaxie	k1gFnPc2	galaxie
vysílané	vysílaný	k2eAgFnPc4d1	vysílaná
v	v	k7c6	v
šesti	šest	k4xCc6	šest
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Adams	Adams	k1gInSc1	Adams
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
rozběhnout	rozběhnout	k5eAaPmF	rozběhnout
filmovou	filmový	k2eAgFnSc4d1	filmová
spolupráci	spolupráce	k1gFnSc4	spolupráce
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
Disney	Disnea	k1gFnSc2	Disnea
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vysílací	vysílací	k2eAgNnPc4d1	vysílací
práva	právo	k1gNnPc4	právo
koupilo	koupit	k5eAaPmAgNnS	koupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
přepsán	přepsán	k2eAgInSc1d1	přepsán
Kareym	Kareym	k1gInSc1	Kareym
Kirkpatrickem	Kirkpatricko	k1gNnSc7	Kirkpatricko
<g/>
,	,	kIx,	,
natáčení	natáčení	k1gNnSc1	natáčení
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
v	v	k7c6	v
září	září	k1gNnSc6	září
2003	[number]	k4	2003
a	a	k8xC	a
finální	finální	k2eAgFnSc1d1	finální
verze	verze	k1gFnSc1	verze
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
objevila	objevit	k5eAaPmAgFnS	objevit
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Holistická	holistický	k2eAgFnSc1d1	holistická
detektivní	detektivní	k2eAgFnSc1d1	detektivní
kancelář	kancelář	k1gFnSc1	kancelář
Dirka	Direk	k1gMnSc2	Direk
Gentlyho	Gently	k1gMnSc2	Gently
(	(	kIx(	(
<g/>
Dirk	Dirk	k1gMnSc1	Dirk
Gently	Gently	k1gMnSc1	Gently
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Holistic	Holistice	k1gFnPc2	Holistice
Detective	Detectiv	k1gInSc5	Detectiv
Agency	Agenc	k1gMnPc7	Agenc
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
temný	temný	k2eAgInSc1d1	temný
čas	čas	k1gInSc1	čas
svačiny	svačin	k2eAgFnSc2d1	svačina
duše	duše	k1gFnSc2	duše
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Long	Long	k1gMnSc1	Long
Dark	Dark	k1gMnSc1	Dark
Tea-time	Teaim	k1gInSc5	Tea-tim
of	of	k?	of
the	the	k?	the
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Ještě	ještě	k6eAd1	ještě
je	on	k3xPp3gMnPc4	on
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
(	(	kIx(	(
<g/>
Last	Last	k1gInSc1	Last
Chance	Chanec	k1gInSc2	Chanec
<g />
.	.	kIx.	.
</s>
<s>
to	ten	k3xDgNnSc1	ten
See	See	k1gMnPc1	See
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
stopem	stop	k1gInSc7	stop
po	po	k7c4	po
galaxii	galaxie	k1gFnSc4	galaxie
aneb	aneb	k?	aneb
Losos	losos	k1gMnSc1	losos
pochybnosti	pochybnost	k1gFnSc2	pochybnost
–	–	k?	–
zážitky	zážitek	k1gInPc1	zážitek
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc1	dopis
dvanáctiletého	dvanáctiletý	k2eAgMnSc2d1	dvanáctiletý
Adamse	Adams	k1gMnSc2	Adams
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc4d1	nedokončený
román	román	k1gInSc4	román
Neil	Neil	k1gMnSc1	Neil
Gaiman	Gaiman	k1gMnSc1	Gaiman
<g/>
:	:	kIx,	:
Nepropadejte	propadat	k5eNaPmRp2nP	propadat
panice	panice	k1gFnSc1	panice
Douglas	Douglas	k1gMnSc1	Douglas
Adams	Adams	k1gInSc1	Adams
a	a	k8xC	a
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
Galaxií	galaxie	k1gFnPc2	galaxie
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
vesmíru	vesmír	k1gInSc6	vesmír
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
Douglase	Douglasa	k1gFnSc3	Douglasa
<g />
.	.	kIx.	.
</s>
<s>
Adamse	Adams	k1gMnSc4	Adams
Terry	Terra	k1gFnSc2	Terra
Jones	Jones	k1gMnSc1	Jones
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
Titanic	Titanic	k1gInSc1	Titanic
Eoin	Eoin	k1gMnSc1	Eoin
Colfer	Colfer	k1gMnSc1	Colfer
<g/>
:	:	kIx,	:
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
Galaxií	galaxie	k1gFnPc2	galaxie
6	[number]	k4	6
<g/>
:	:	kIx,	:
A	a	k8xC	a
ještě	ještě	k9	ještě
něco	něco	k6eAd1	něco
<g/>
...	...	k?	...
Jem	Jem	k?	Jem
Roberts	Roberts	k1gInSc1	Roberts
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Frood	Frood	k1gInSc1	Frood
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Authorised	Authorised	k1gMnSc1	Authorised
and	and	k?	and
Very	Very	k1gInPc1	Very
Official	Official	k1gInSc1	Official
History	Histor	k1gInPc1	Histor
of	of	k?	of
Douglas	Douglas	k1gInSc1	Douglas
Adams	Adams	k1gInSc1	Adams
&	&	k?	&
The	The	k1gMnSc1	The
Hitchhiker	Hitchhiker	k1gMnSc1	Hitchhiker
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Galaxy	Galax	k1gInPc1	Galax
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Douglas	Douglas	k1gInSc1	Douglas
Adams	Adams	k1gInSc1	Adams
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Douglas	Douglasa	k1gFnPc2	Douglasa
Adams	Adamsa	k1gFnPc2	Adamsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Douglas	Douglas	k1gInSc1	Douglas
Adams	Adams	k1gInSc1	Adams
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Douglas	Douglas	k1gInSc1	Douglas
Adams	Adamsa	k1gFnPc2	Adamsa
</s>
