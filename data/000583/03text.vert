<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1316	[number]	k4	1316
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1378	[number]	k4	1378
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedenáctý	jedenáctý	k4xOgMnSc1	jedenáctý
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
vládnoucí	vládnoucí	k2eAgMnSc1d1	vládnoucí
jako	jako	k9	jako
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1346	[number]	k4	1346
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1378	[number]	k4	1378
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
římsko-německý	římskoěmecký	k2eAgMnSc1d1	římsko-německý
král	král	k1gMnSc1	král
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1346	[number]	k4	1346
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
císař	císař	k1gMnSc1	císař
římský	římský	k2eAgInSc1d1	římský
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
italský	italský	k2eAgMnSc1d1	italský
(	(	kIx(	(
<g/>
lombardský	lombardský	k2eAgMnSc1d1	lombardský
<g/>
)	)	kIx)	)
král	král	k1gMnSc1	král
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
<g/>
,	,	kIx,	,
burgundský	burgundský	k2eAgMnSc1d1	burgundský
(	(	kIx(	(
<g/>
arelatský	arelatský	k2eAgMnSc1d1	arelatský
<g/>
)	)	kIx)	)
král	král	k1gMnSc1	král
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1333	[number]	k4	1333
až	až	k9	až
1349	[number]	k4	1349
a	a	k8xC	a
lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
hrabě	hrabě	k1gMnSc1	hrabě
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1346	[number]	k4	1346
až	až	k6eAd1	až
1353	[number]	k4	1353
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Lucemburků	Lucemburk	k1gInPc2	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
také	také	k9	také
císařem	císař	k1gMnSc7	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgNnSc7d1	poslední
korunovaným	korunovaný	k2eAgNnSc7d1	korunované
burgundským	burgundské	k1gNnSc7	burgundské
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
osobním	osobnit	k5eAaImIp1nS	osobnit
vládcem	vládce	k1gMnSc7	vládce
všech	všecek	k3xTgInPc2	všecek
království	království	k1gNnPc4	království
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
dědičky	dědička	k1gFnSc2	dědička
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
Elišky	Eliška	k1gFnSc2	Eliška
a	a	k8xC	a
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pokřtěn	pokřtít	k5eAaPmNgMnS	pokřtít
jako	jako	k9	jako
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
Karel	Karel	k1gMnSc1	Karel
přijal	přijmout	k5eAaPmAgInS	přijmout
při	pře	k1gFnSc4	pře
biřmování	biřmování	k1gNnSc2	biřmování
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
výchovy	výchova	k1gFnSc2	výchova
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
po	po	k7c6	po
svém	svůj	k1gMnSc6	svůj
strýci	strýc	k1gMnSc6	strýc
a	a	k8xC	a
kmotrovi	kmotr	k1gMnSc6	kmotr
Karlu	Karel	k1gMnSc3	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
Sličném	sličný	k2eAgNnSc6d1	sličné
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
panovníky	panovník	k1gMnPc4	panovník
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
a	a	k8xC	a
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
<g/>
,	,	kIx,	,
plynně	plynně	k6eAd1	plynně
hovořil	hovořit	k5eAaImAgMnS	hovořit
pěti	pět	k4xCc2	pět
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k6eAd1	moc
využil	využít	k5eAaPmAgInS	využít
ke	k	k7c3	k
zkonsolidování	zkonsolidování	k1gNnSc3	zkonsolidování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
Koruna	koruna	k1gFnSc1	koruna
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
jako	jako	k8xS	jako
císař	císař	k1gMnSc1	císař
nechal	nechat	k5eAaPmAgMnS	nechat
vytvořit	vytvořit	k5eAaPmF	vytvořit
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
bulu	bula	k1gFnSc4	bula
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
říšský	říšský	k2eAgInSc4d1	říšský
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
platil	platit	k5eAaImAgInS	platit
až	až	k6eAd1	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
také	také	k9	také
významně	významně	k6eAd1	významně
upravovala	upravovat	k5eAaImAgFnS	upravovat
vztah	vztah	k1gInSc4	vztah
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
k	k	k7c3	k
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
potvrzovala	potvrzovat	k5eAaImAgFnS	potvrzovat
jeho	jeho	k3xOp3gNnSc4	jeho
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
a	a	k8xC	a
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
proslul	proslout	k5eAaPmAgMnS	proslout
především	především	k9	především
založením	založení	k1gNnSc7	založení
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
výstavbou	výstavba	k1gFnSc7	výstavba
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
,	,	kIx,	,
stavbou	stavba	k1gFnSc7	stavba
kamenného	kamenný	k2eAgInSc2d1	kamenný
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Vltavu	Vltava	k1gFnSc4	Vltava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
zbudováním	zbudování	k1gNnSc7	zbudování
hradu	hrad	k1gInSc2	hrad
Karlštejna	Karlštejn	k1gInSc2	Karlštejn
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgInPc7d1	další
počiny	počin	k1gInPc7	počin
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
započali	započnout	k5eAaPmAgMnP	započnout
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
Svatovítského	svatovítský	k2eAgInSc2d1	svatovítský
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
také	také	k9	také
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
významné	významný	k2eAgFnPc4d1	významná
územní	územní	k2eAgFnPc4d1	územní
expanze	expanze	k1gFnPc4	expanze
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
obratné	obratný	k2eAgFnSc3d1	obratná
sňatkové	sňatkový	k2eAgFnSc3d1	sňatková
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Největší	veliký	k2eAgMnSc1d3	veliký
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kroniky	kronika	k1gFnSc2	kronika
Petra	Petr	k1gMnSc2	Petr
Žitavského	žitavský	k2eAgMnSc2d1	žitavský
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
narodil	narodit	k5eAaPmAgMnS	narodit
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1316	[number]	k4	1316
okolo	okolo	k7c2	okolo
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
ráno	ráno	k6eAd1	ráno
tehdy	tehdy	k6eAd1	tehdy
čtyřiadvacetileté	čtyřiadvacetiletý	k2eAgFnSc6d1	čtyřiadvacetiletá
Elišce	Eliška	k1gFnSc6	Eliška
Přemyslovně	Přemyslovna	k1gFnSc6	Přemyslovna
v	v	k7c6	v
městě	město	k1gNnSc6	město
Pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Karel	Karel	k1gMnSc1	Karel
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životopise	životopis	k1gInSc6	životopis
komentoval	komentovat	k5eAaBmAgMnS	komentovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Štupartů	Štupart	k1gMnPc2	Štupart
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
ještě	ještě	k6eAd1	ještě
pořád	pořád	k6eAd1	pořád
zchátralý	zchátralý	k2eAgMnSc1d1	zchátralý
a	a	k8xC	a
neobyvatelný	obyvatelný	k2eNgMnSc1d1	neobyvatelný
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1316	[number]	k4	1316
<g/>
,	,	kIx,	,
na	na	k7c4	na
Boží	boží	k2eAgInSc4d1	boží
hod	hod	k1gInSc4	hod
svatodušní	svatodušní	k2eAgMnSc1d1	svatodušní
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
katedrálním	katedrální	k2eAgInSc6d1	katedrální
pražském	pražský	k2eAgInSc6d1	pražský
chrámu	chrám	k1gInSc6	chrám
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
trevírského	trevírský	k2eAgNnSc2d1	trevírský
Balduina	Balduino	k1gNnSc2	Balduino
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
pokřtěn	pokřtít	k5eAaPmNgMnS	pokřtít
mohučským	mohučský	k2eAgMnSc7d1	mohučský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Petrem	Petr	k1gMnSc7	Petr
z	z	k7c2	z
Aspeltu	Aspelt	k1gInSc2	Aspelt
<g/>
.	.	kIx.	.
</s>
<s>
Shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
byli	být	k5eAaImAgMnP	být
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
oba	dva	k4xCgInPc1	dva
na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
stále	stále	k6eAd1	stále
nebylo	být	k5eNaImAgNnS	být
zažehnáno	zažehnán	k2eAgNnSc1d1	zažehnáno
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
nechal	nechat	k5eAaPmAgMnS	nechat
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
uvěznit	uvěznit	k5eAaPmF	uvěznit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
druhého	druhý	k4xOgMnSc2	druhý
nejmocnějšího	mocný	k2eAgMnSc2d3	nejmocnější
muže	muž	k1gMnSc2	muž
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
pana	pan	k1gMnSc2	pan
Jindřicha	Jindřich	k1gMnSc2	Jindřich
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
milenkou	milenka	k1gFnSc7	milenka
a	a	k8xC	a
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
českých	český	k2eAgFnPc6d1	Česká
králích	král	k1gMnPc6	král
Eliškou	Eliška	k1gFnSc7	Eliška
Rejčkou	Rejčka	k1gMnSc7	Rejčka
chtěl	chtít	k5eAaImAgMnS	chtít
zmocnit	zmocnit	k5eAaPmF	zmocnit
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
tehdy	tehdy	k6eAd1	tehdy
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
diplomatů	diplomat	k1gMnPc2	diplomat
Evropy	Evropa	k1gFnSc2	Evropa
Petra	Petr	k1gMnSc2	Petr
z	z	k7c2	z
Aspeltu	Aspelt	k1gInSc2	Aspelt
byl	být	k5eAaImAgMnS	být
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1316	[number]	k4	1316
propuštěn	propustit	k5eAaPmNgInS	propustit
a	a	k8xC	a
spor	spor	k1gInSc1	spor
zčásti	zčásti	k6eAd1	zčásti
urovnán	urovnán	k2eAgInSc1d1	urovnán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
napětí	napětí	k1gNnSc1	napětí
trvalo	trvat	k5eAaImAgNnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1318	[number]	k4	1318
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
prožilo	prožít	k5eAaPmAgNnS	prožít
královské	královský	k2eAgNnSc1d1	královské
miminko	miminko	k1gNnSc1	miminko
první	první	k4xOgFnSc2	první
zimu	zima	k1gFnSc4	zima
ukryto	ukrýt	k5eAaPmNgNnS	ukrýt
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Křivoklátě	Křivoklát	k1gInSc6	Křivoklát
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
Viléma	Vilém	k1gMnSc2	Vilém
Zajíce	Zajíc	k1gMnSc2	Zajíc
z	z	k7c2	z
Valdeka	Valdeek	k1gInSc2	Valdeek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
osobním	osobní	k2eAgMnSc7d1	osobní
strážcem	strážce	k1gMnSc7	strážce
královny	královna	k1gFnSc2	královna
Elišky	Eliška	k1gFnSc2	Eliška
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
Viléma	Vilém	k1gMnSc2	Vilém
Zajíce	Zajíc	k1gMnSc2	Zajíc
z	z	k7c2	z
Valdeku	Valdek	k1gInSc2	Valdek
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Loket	loket	k1gInSc1	loket
<g/>
.	.	kIx.	.
</s>
<s>
Sotva	sotva	k6eAd1	sotva
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
chodit	chodit	k5eAaImF	chodit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
matka	matka	k1gFnSc1	matka
Eliška	Eliška	k1gFnSc1	Eliška
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
do	do	k7c2	do
věnného	věnný	k2eAgNnSc2d1	věnné
města	město	k1gNnSc2	město
českých	český	k2eAgFnPc2d1	Česká
královen	královna	k1gFnPc2	královna
Mělníka	Mělník	k1gInSc2	Mělník
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Lokti	loket	k1gInSc6	loket
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomocí	pomocí	k7c2	pomocí
šlechty	šlechta	k1gFnSc2	šlechta
nebyl	být	k5eNaImAgInS	být
dosazen	dosadit	k5eAaPmNgInS	dosadit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
místo	místo	k7c2	místo
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Viléma	Vilém	k1gMnSc2	Vilém
Zajíce	Zajíc	k1gMnSc2	Zajíc
z	z	k7c2	z
Valdeku	Valdek	k1gInSc2	Valdek
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
přemístěn	přemístit	k5eAaPmNgInS	přemístit
na	na	k7c4	na
Křivoklát	Křivoklát	k1gInSc4	Křivoklát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
matkou	matka	k1gFnSc7	matka
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1323	[number]	k4	1323
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
byl	být	k5eAaImAgMnS	být
malý	malý	k2eAgMnSc1d1	malý
Václav	Václav	k1gMnSc1	Václav
roku	rok	k1gInSc2	rok
1323	[number]	k4	1323
odeslán	odeslat	k5eAaPmNgMnS	odeslat
na	na	k7c4	na
výchovu	výchova	k1gFnSc4	výchova
k	k	k7c3	k
francouzskému	francouzský	k2eAgInSc3d1	francouzský
královskému	královský	k2eAgInSc3d1	královský
dvoru	dvůr	k1gInSc3	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
francouzském	francouzský	k2eAgInSc6d1	francouzský
dvoře	dvůr	k1gInSc6	dvůr
byl	být	k5eAaImAgMnS	být
mladý	mladý	k2eAgMnSc1d1	mladý
Václav	Václav	k1gMnSc1	Václav
biřmován	biřmován	k2eAgMnSc1d1	biřmován
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
kmotrem	kmotr	k1gMnSc7	kmotr
byl	být	k5eAaImAgMnS	být
samotný	samotný	k2eAgMnSc1d1	samotný
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Sličný	sličný	k2eAgMnSc1d1	sličný
a	a	k8xC	a
sedmiletý	sedmiletý	k2eAgMnSc1d1	sedmiletý
Václav	Václav	k1gMnSc1	Václav
přijal	přijmout	k5eAaPmAgMnS	přijmout
podle	podle	k7c2	podle
obyčeje	obyčej	k1gInSc2	obyčej
jméno	jméno	k1gNnSc4	jméno
svého	svůj	k3xOyFgMnSc2	svůj
kmotra	kmotr	k1gMnSc2	kmotr
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
také	také	k9	také
zasnouben	zasnouben	k2eAgMnSc1d1	zasnouben
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
oženěn	oženěn	k2eAgInSc1d1	oženěn
s	s	k7c7	s
královou	králová	k1gFnSc7	králová
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Markétou	Markéta	k1gFnSc7	Markéta
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Blanka	Blanka	k1gFnSc1	Blanka
<g/>
,	,	kIx,	,
rodem	rod	k1gInSc7	rod
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
.	.	kIx.	.
</s>
<s>
Svatební	svatební	k2eAgInSc1d1	svatební
obřad	obřad	k1gInSc1	obřad
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
současně	současně	k6eAd1	současně
s	s	k7c7	s
korunovací	korunovace	k1gFnSc7	korunovace
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
tety	teta	k1gFnSc2	teta
Marie	Maria	k1gFnSc2	Maria
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
královnu	královna	k1gFnSc4	královna
<g/>
.	.	kIx.	.
</s>
<s>
Svatební	svatební	k2eAgInSc1d1	svatební
obřad	obřad	k1gInSc1	obřad
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
za	za	k7c2	za
slavnostních	slavnostní	k2eAgFnPc2d1	slavnostní
a	a	k8xC	a
mimořádných	mimořádný	k2eAgFnPc2d1	mimořádná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
francouzští	francouzský	k2eAgMnPc1d1	francouzský
kronikáři	kronikář	k1gMnPc1	kronikář
dokonce	dokonce	k9	dokonce
obdivovali	obdivovat	k5eAaImAgMnP	obdivovat
nádherné	nádherný	k2eAgNnSc4d1	nádherné
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
roucho	roucho	k1gNnSc4	roucho
malého	malý	k2eAgMnSc2d1	malý
prince	princ	k1gMnSc2	princ
Václava	Václav	k1gMnSc2	Václav
-	-	kIx~	-
Karla	Karel	k1gMnSc2	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
však	však	k9	však
byli	být	k5eAaImAgMnP	být
dětští	dětský	k2eAgMnPc1d1	dětský
manželé	manžel	k1gMnPc1	manžel
opět	opět	k6eAd1	opět
rozděleni	rozdělen	k2eAgMnPc1d1	rozdělen
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
sám	sám	k3xTgMnSc1	sám
mezi	mezi	k7c7	mezi
cizími	cizí	k2eAgMnPc7d1	cizí
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mladičký	mladičký	k2eAgMnSc1d1	mladičký
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
však	však	k9	však
rychle	rychle	k6eAd1	rychle
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
prostředí	prostředí	k1gNnSc6	prostředí
aklimatizoval	aklimatizovat	k5eAaBmAgMnS	aklimatizovat
a	a	k8xC	a
užasl	užasnout	k5eAaPmAgMnS	užasnout
nad	nad	k7c7	nad
nádherou	nádhera	k1gFnSc7	nádhera
a	a	k8xC	a
velkolepostí	velkolepost	k1gFnSc7	velkolepost
francouzského	francouzský	k2eAgInSc2d1	francouzský
dvorního	dvorní	k2eAgInSc2d1	dvorní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
si	se	k3xPyFc3	se
svého	svůj	k3xOyFgMnSc4	svůj
synovce	synovec	k1gMnSc4	synovec
rychle	rychle	k6eAd1	rychle
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Marií	Maria	k1gFnPc2	Maria
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
vroucně	vroucně	k6eAd1	vroucně
pečoval	pečovat	k5eAaImAgMnS	pečovat
<g/>
.	.	kIx.	.
</s>
<s>
Vychovatelé	vychovatel	k1gMnPc1	vychovatel
mladého	mladý	k2eAgMnSc2d1	mladý
Karla	Karel	k1gMnSc2	Karel
byli	být	k5eAaImAgMnP	být
vybíráni	vybírat	k5eAaImNgMnP	vybírat
především	především	k9	především
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
kleriků	klerik	k1gMnPc2	klerik
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
princ	princ	k1gMnSc1	princ
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgMnS	učít
číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
latině	latina	k1gFnSc6	latina
a	a	k8xC	a
francouzštině	francouzština	k1gFnSc6	francouzština
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vzděláván	vzdělávat	k5eAaImNgInS	vzdělávat
i	i	k9	i
v	v	k7c6	v
základech	základ	k1gInPc6	základ
scholastické	scholastický	k2eAgFnSc2d1	scholastická
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1325	[number]	k4	1325
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
Karel	Karel	k1gMnSc1	Karel
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
královský	královský	k2eAgInSc4d1	královský
dvůr	dvůr	k1gInSc4	dvůr
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc4	smrt
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Sličného	sličný	k2eAgInSc2d1	sličný
roku	rok	k1gInSc2	rok
1328	[number]	k4	1328
však	však	k9	však
znamenala	znamenat	k5eAaImAgFnS	znamenat
konec	konec	k1gInSc4	konec
blahobytu	blahobyt	k1gInSc2	blahobyt
jeho	on	k3xPp3gMnSc2	on
dvanáctiletého	dvanáctiletý	k2eAgMnSc2d1	dvanáctiletý
jmenovce	jmenovec	k1gMnSc2	jmenovec
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
bratr	bratr	k1gMnSc1	bratr
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
manželky	manželka	k1gFnSc2	manželka
Blanky	Blanka	k1gFnSc2	Blanka
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
byl	být	k5eAaImAgMnS	být
marnotratný	marnotratný	k2eAgMnSc1d1	marnotratný
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
toužící	toužící	k2eAgFnPc1d1	toužící
po	po	k7c6	po
rytířském	rytířský	k2eAgInSc6d1	rytířský
lesku	lesk	k1gInSc6	lesk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
blízko	blízko	k6eAd1	blízko
spíše	spíše	k9	spíše
ke	k	k7c3	k
Karlovu	Karlův	k2eAgMnSc3d1	Karlův
otci	otec	k1gMnSc3	otec
Janu	Jan	k1gMnSc3	Jan
Lucemburskému	lucemburský	k2eAgMnSc3d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
princ	princ	k1gMnSc1	princ
byl	být	k5eAaImAgMnS	být
Filipovi	Filip	k1gMnSc3	Filip
lhostejný	lhostejný	k2eAgMnSc1d1	lhostejný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
především	především	k9	především
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
výdajů	výdaj	k1gInPc2	výdaj
pro	pro	k7c4	pro
Karlovy	Karlův	k2eAgFnPc4d1	Karlova
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
za	za	k7c2	za
takových	takový	k3xDgFnPc2	takový
podmínek	podmínka	k1gFnPc2	podmínka
věnoval	věnovat	k5eAaPmAgMnS	věnovat
plně	plně	k6eAd1	plně
studiu	studio	k1gNnSc3	studio
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
i	i	k9	i
na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Karlovým	Karlův	k2eAgMnSc7d1	Karlův
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
přítelem	přítel	k1gMnSc7	přítel
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
stal	stát	k5eAaPmAgMnS	stát
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Rosieres	Rosieres	k1gMnSc1	Rosieres
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Sedmiletý	sedmiletý	k2eAgInSc1d1	sedmiletý
pobyt	pobyt	k1gInSc1	pobyt
na	na	k7c6	na
francouzském	francouzský	k2eAgInSc6d1	francouzský
dvoře	dvůr	k1gInSc6	dvůr
Karla	Karel	k1gMnSc2	Karel
velmi	velmi	k6eAd1	velmi
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
totiž	totiž	k9	totiž
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
zastáncem	zastánce	k1gMnSc7	zastánce
myšlenky	myšlenka	k1gFnPc4	myšlenka
centralizované	centralizovaný	k2eAgFnSc2d1	centralizovaná
monarchie	monarchie	k1gFnSc2	monarchie
po	po	k7c6	po
francouzském	francouzský	k2eAgInSc6d1	francouzský
vzoru	vzor	k1gInSc6	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1330	[number]	k4	1330
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
povolán	povolán	k2eAgMnSc1d1	povolán
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
do	do	k7c2	do
Lucemburku	Lucemburk	k1gInSc2	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
zatím	zatím	k6eAd1	zatím
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
vratké	vratký	k2eAgNnSc1d1	vratké
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
zde	zde	k6eAd1	zde
jako	jako	k8xS	jako
zastánce	zastánce	k1gMnPc4	zastánce
císařských	císařský	k2eAgInPc2d1	císařský
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
papežských	papežský	k2eAgInPc2d1	papežský
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Janovi	Jan	k1gMnSc3	Jan
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
Lombardii	Lombardie	k1gFnSc6	Lombardie
tato	tento	k3xDgNnPc4	tento
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
Brescii	Brescie	k1gFnSc4	Brescie
<g/>
,	,	kIx,	,
Bergamo	Bergamo	k1gNnSc4	Bergamo
<g/>
,	,	kIx,	,
Parmu	Parma	k1gFnSc4	Parma
<g/>
,	,	kIx,	,
Cremonu	Cremona	k1gFnSc4	Cremona
<g/>
,	,	kIx,	,
Pavii	Pavie	k1gFnSc4	Pavie
<g/>
,	,	kIx,	,
Reggio	Reggio	k1gNnSc4	Reggio
a	a	k8xC	a
Modenu	Modena	k1gFnSc4	Modena
<g/>
,	,	kIx,	,
v	v	k7c6	v
Toskánsku	Toskánsko	k1gNnSc6	Toskánsko
pak	pak	k6eAd1	pak
Luccu	Lucc	k2eAgFnSc4d1	Lucca
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1331	[number]	k4	1331
povolal	povolat	k5eAaPmAgMnS	povolat
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
svého	svůj	k1gMnSc4	svůj
syna	syn	k1gMnSc4	syn
Karla	Karel	k1gMnSc4	Karel
z	z	k7c2	z
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
cestoval	cestovat	k5eAaImAgMnS	cestovat
přes	přes	k7c4	přes
Mety	Mety	k1gFnPc4	Mety
<g/>
,	,	kIx,	,
Lotrinsko	Lotrinsko	k1gNnSc4	Lotrinsko
<g/>
,	,	kIx,	,
Burgundsko	Burgundsko	k1gNnSc4	Burgundsko
a	a	k8xC	a
Savojsko	Savojsko	k1gNnSc4	Savojsko
až	až	k9	až
na	na	k7c4	na
Veliký	veliký	k2eAgInSc4d1	veliký
pátek	pátek	k1gInSc4	pátek
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1331	[number]	k4	1331
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
města	město	k1gNnSc2	město
Pavia	Pavium	k1gNnSc2	Pavium
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
pak	pak	k6eAd1	pak
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
otcův	otcův	k2eAgMnSc1d1	otcův
místodržitel	místodržitel	k1gMnSc1	místodržitel
v	v	k7c6	v
lucemburské	lucemburský	k2eAgFnSc6d1	Lucemburská
italské	italský	k2eAgFnSc6d1	italská
signorii	signoria	k1gFnSc6	signoria
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
důvěrně	důvěrně	k6eAd1	důvěrně
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
italským	italský	k2eAgNnSc7d1	italské
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
intrikami	intrika	k1gFnPc7	intrika
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
třetí	třetí	k4xOgInSc4	třetí
den	den	k1gInSc4	den
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
doprovod	doprovod	k1gInSc4	doprovod
přesvědčili	přesvědčit	k5eAaPmAgMnP	přesvědčit
o	o	k7c4	o
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
italského	italský	k2eAgNnSc2d1	italské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Karlově	Karlův	k2eAgFnSc3d1	Karlova
družině	družina	k1gFnSc3	družina
bylo	být	k5eAaImAgNnS	být
podáno	podat	k5eAaPmNgNnS	podat
k	k	k7c3	k
snídani	snídaně	k1gFnSc3	snídaně
otrávené	otrávený	k2eAgNnSc1d1	otrávené
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
naštěstí	naštěstí	k6eAd1	naštěstí
nejedl	jíst	k5eNaImAgMnS	jíst
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
mše	mše	k1gFnSc2	mše
a	a	k8xC	a
přijímání	přijímání	k1gNnSc2	přijímání
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
tak	tak	k6eAd1	tak
zůstat	zůstat	k5eAaPmF	zůstat
lačný	lačný	k2eAgInSc4d1	lačný
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
neznámého	známý	k2eNgMnSc4d1	neznámý
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
okolo	okolo	k7c2	okolo
stolu	stol	k1gInSc2	stol
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
jej	on	k3xPp3gMnSc4	on
zatknout	zatknout	k5eAaPmF	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mučení	mučení	k1gNnSc6	mučení
neznámý	známý	k2eNgMnSc1d1	neznámý
muž	muž	k1gMnSc1	muž
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
z	z	k7c2	z
příkazu	příkaz	k1gInSc2	příkaz
milánského	milánský	k2eAgMnSc2d1	milánský
pána	pán	k1gMnSc2	pán
Azza	Azzus	k1gMnSc2	Azzus
Viscontiho	Visconti	k1gMnSc2	Visconti
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přimíchal	přimíchat	k5eAaPmAgInS	přimíchat
Karlově	Karlův	k2eAgFnSc3d1	Karlova
družině	družina	k1gFnSc3	družina
do	do	k7c2	do
jídla	jídlo	k1gNnSc2	jídlo
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
Parmě	Parma	k1gFnSc6	Parma
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Janem	Jan	k1gMnSc7	Jan
Jindřichem	Jindřich	k1gMnSc7	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
sblížili	sblížit	k5eAaPmAgMnP	sblížit
a	a	k8xC	a
upřímný	upřímný	k2eAgInSc1d1	upřímný
vztah	vztah	k1gInSc1	vztah
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
zachován	zachovat	k5eAaPmNgInS	zachovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
pak	pak	k6eAd1	pak
opustil	opustit	k5eAaPmAgMnS	opustit
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
zde	zde	k6eAd1	zde
Karla	Karel	k1gMnSc4	Karel
v	v	k7c4	v
poručnictví	poručnictví	k1gNnPc4	poručnictví
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Savojského	savojský	k2eAgMnSc2d1	savojský
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1332	[number]	k4	1332
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Lucemburkům	Lucemburk	k1gInPc3	Lucemburk
utvořila	utvořit	k5eAaPmAgFnS	utvořit
v	v	k7c4	v
Itálii	Itálie	k1gFnSc4	Itálie
nepřátelská	přátelský	k2eNgFnSc1d1	nepřátelská
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vedli	vést	k5eAaImAgMnP	vést
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Milána	Milán	k1gInSc2	Milán
<g/>
,	,	kIx,	,
Verony	Verona	k1gFnSc2	Verona
<g/>
,	,	kIx,	,
Ferrary	Ferrara	k1gFnSc2	Ferrara
a	a	k8xC	a
Mantovy	Mantova	k1gFnSc2	Mantova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
okamžik	okamžik	k1gInSc4	okamžik
Ludvík	Ludvík	k1gMnSc1	Ludvík
Savojský	savojský	k2eAgInSc1d1	savojský
Karla	Karel	k1gMnSc4	Karel
zradil	zradit	k5eAaPmAgInS	zradit
a	a	k8xC	a
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Vojsko	vojsko	k1gNnSc1	vojsko
protilucemburské	protilucemburský	k2eAgFnSc2d1	protilucemburský
ligy	liga	k1gFnSc2	liga
zatím	zatím	k6eAd1	zatím
oblehlo	oblehnout	k5eAaPmAgNnS	oblehnout
Modenu	Modena	k1gFnSc4	Modena
a	a	k8xC	a
vyplenilo	vyplenit	k5eAaPmAgNnS	vyplenit
její	její	k3xOp3gFnSc4	její
okolí	okolí	k1gNnSc1	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
obléhatelé	obléhatel	k1gMnPc1	obléhatel
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1332	[number]	k4	1332
pod	pod	k7c4	pod
hrad	hrad	k1gInSc4	hrad
San	San	k1gMnSc2	San
Felice	Felic	k1gMnSc2	Felic
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
zatím	zatím	k6eAd1	zatím
získal	získat	k5eAaPmAgMnS	získat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
svých	svůj	k3xOyFgMnPc2	svůj
oddaných	oddaný	k2eAgMnPc2d1	oddaný
přátel	přítel	k1gMnPc2	přítel
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
měst	město	k1gNnPc2	město
značnou	značný	k2eAgFnSc4d1	značná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
asi	asi	k9	asi
200	[number]	k4	200
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
6	[number]	k4	6
000	[number]	k4	000
pěšáků	pěšák	k1gMnPc2	pěšák
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1332	[number]	k4	1332
na	na	k7c4	na
den	den	k1gInSc4	den
svaté	svatý	k2eAgFnSc2d1	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozhodující	rozhodující	k2eAgFnSc3d1	rozhodující
bitvě	bitva	k1gFnSc3	bitva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
sám	sám	k3xTgMnSc1	sám
zraněn	zraněn	k2eAgMnSc1d1	zraněn
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vítězství	vítězství	k1gNnSc4	vítězství
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgMnPc7d1	další
bojovníky	bojovník	k1gMnPc7	bojovník
byl	být	k5eAaImAgInS	být
pasován	pasován	k2eAgInSc1d1	pasován
na	na	k7c4	na
rytíře	rytíř	k1gMnSc4	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
vítězství	vítězství	k1gNnSc4	vítězství
se	se	k3xPyFc4	se
lucemburské	lucemburský	k2eAgNnSc1d1	lucemburské
panství	panství	k1gNnSc1	panství
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
značně	značně	k6eAd1	značně
zmenšilo	zmenšit	k5eAaPmAgNnS	zmenšit
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1333	[number]	k4	1333
vypravil	vypravit	k5eAaPmAgInS	vypravit
do	do	k7c2	do
Luccy	Lucca	k1gFnSc2	Lucca
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
upevnil	upevnit	k5eAaPmAgInS	upevnit
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
města	město	k1gNnSc2	město
založil	založit	k5eAaPmAgMnS	založit
Karel	Karel	k1gMnSc1	Karel
v	v	k7c6	v
nedalekém	daleký	k2eNgNnSc6d1	nedaleké
okolí	okolí	k1gNnSc6	okolí
pevnost	pevnost	k1gFnSc1	pevnost
a	a	k8xC	a
městečko	městečko	k1gNnSc1	městečko
Montecarlo	Montecarlo	k1gNnSc1	Montecarlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1333	[number]	k4	1333
pak	pak	k6eAd1	pak
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
do	do	k7c2	do
Cremony	Cremona	k1gFnSc2	Cremona
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgInSc4d1	lucemburský
s	s	k7c7	s
posilami	posila	k1gFnPc7	posila
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
příjezd	příjezd	k1gInSc1	příjezd
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
však	však	k9	však
lucemburské	lucemburský	k2eAgNnSc1d1	lucemburské
panství	panství	k1gNnSc1	panství
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
nezachránil	zachránit	k5eNaPmAgMnS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
1333	[number]	k4	1333
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
Karla	Karel	k1gMnSc2	Karel
do	do	k7c2	do
Merana	Meran	k1gMnSc2	Meran
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Korutanským	korutanský	k2eAgMnSc7d1	korutanský
o	o	k7c6	o
svém	svůj	k3xOyFgMnSc6	svůj
bratrovi	bratr	k1gMnSc6	bratr
Janovi	Jan	k1gMnSc6	Jan
Jindřichovi	Jindřich	k1gMnSc6	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
totiž	totiž	k9	totiž
oženěn	oženěn	k2eAgMnSc1d1	oženěn
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Korutanského	korutanský	k2eAgMnSc2d1	korutanský
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Maultasch	Maultascha	k1gFnPc2	Maultascha
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
měl	mít	k5eAaImAgMnS	mít
dostat	dostat	k5eAaPmF	dostat
věno	věno	k1gNnSc4	věno
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
40	[number]	k4	40
tisíc	tisíc	k4xCgInSc1	tisíc
hřiven	hřivna	k1gFnPc2	hřivna
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
měl	mít	k5eAaImAgMnS	mít
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
Korutanskému	korutanský	k2eAgInSc3d1	korutanský
věno	věno	k1gNnSc4	věno
splatit	splatit	k5eAaPmF	splatit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
pěti	pět	k4xCc3	pět
splátek	splátka	k1gFnPc2	splátka
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
dodržet	dodržet	k5eAaPmF	dodržet
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
tedy	tedy	k9	tedy
jednal	jednat	k5eAaImAgMnS	jednat
v	v	k7c6	v
Meranu	Meran	k1gInSc6	Meran
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Korutanským	korutanský	k2eAgMnSc7d1	korutanský
jako	jako	k8xS	jako
plnomocník	plnomocník	k1gMnSc1	plnomocník
svého	své	k1gNnSc2	své
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c6	o
nových	nový	k2eAgFnPc6d1	nová
podmínkách	podmínka	k1gFnPc6	podmínka
splácení	splácení	k1gNnSc2	splácení
věna	věno	k1gNnSc2	věno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Meranu	Meran	k1gInSc6	Meran
se	se	k3xPyFc4	se
také	také	k9	také
Karel	Karel	k1gMnSc1	Karel
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
předními	přední	k2eAgMnPc7d1	přední
českými	český	k2eAgMnPc7d1	český
pány	pan	k1gMnPc7	pan
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
jimi	on	k3xPp3gMnPc7	on
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Volek	Volek	k1gMnSc1	Volek
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
Těma	ten	k3xDgFnPc7	ten
z	z	k7c2	z
Koldic	Koldice	k1gFnPc2	Koldice
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Landštejna	Landštejn	k1gInSc2	Landštejn
a	a	k8xC	a
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
šlechtici	šlechtic	k1gMnPc1	šlechtic
považovali	považovat	k5eAaImAgMnP	považovat
stav	stav	k1gInSc4	stav
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
za	za	k7c4	za
neutěšený	utěšený	k2eNgInSc4d1	neutěšený
a	a	k8xC	a
nebyli	být	k5eNaImAgMnP	být
spokojeni	spokojen	k2eAgMnPc1d1	spokojen
s	s	k7c7	s
dlouhodobou	dlouhodobý	k2eAgFnSc7d1	dlouhodobá
nepřítomností	nepřítomnost	k1gFnSc7	nepřítomnost
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
žádali	žádat	k5eAaImAgMnP	žádat
Karla	Karel	k1gMnSc4	Karel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neodkladně	odkladně	k6eNd1	odkladně
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
pádnými	pádný	k2eAgInPc7d1	pádný
argumenty	argument	k1gInPc7	argument
českých	český	k2eAgMnPc2d1	český
pánů	pan	k1gMnPc2	pan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
návrat	návrat	k1gInSc4	návrat
Karla	Karel	k1gMnSc2	Karel
nechali	nechat	k5eAaPmAgMnP	nechat
posvětit	posvětit	k5eAaPmF	posvětit
i	i	k9	i
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
pobývajícího	pobývající	k2eAgMnSc2d1	pobývající
v	v	k7c6	v
Parmě	Parma	k1gFnSc6	Parma
<g/>
,	,	kIx,	,
a	a	k8xC	a
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
Merana	Meran	k1gMnSc2	Meran
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
namísto	namísto	k7c2	namísto
původního	původní	k2eAgInSc2d1	původní
návratu	návrat	k1gInSc2	návrat
do	do	k7c2	do
Luccy	Lucca	k1gFnSc2	Lucca
rovnou	rovnou	k6eAd1	rovnou
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
překročil	překročit	k5eAaPmAgMnS	překročit
Karel	Karel	k1gMnSc1	Karel
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1333	[number]	k4	1333
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
zastávkou	zastávka	k1gFnSc7	zastávka
byl	být	k5eAaImAgInS	být
Zbraslavský	zbraslavský	k2eAgInSc1d1	zbraslavský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
plné	plný	k2eAgInPc4d1	plný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
odpočívala	odpočívat	k5eAaImAgFnS	odpočívat
jeho	jeho	k3xOp3gMnPc3	jeho
matka	matka	k1gFnSc1	matka
Eliška	Eliška	k1gFnSc1	Eliška
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
svůj	svůj	k3xOyFgInSc4	svůj
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
pak	pak	k6eAd1	pak
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
nepřijel	přijet	k5eNaPmAgMnS	přijet
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
vybaven	vybavit	k5eAaPmNgInS	vybavit
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
žádnou	žádný	k3yNgFnSc7	žádný
mocí	moc	k1gFnSc7	moc
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
nejdříve	dříve	k6eAd3	dříve
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
svých	svůj	k3xOyFgMnPc6	svůj
společnících	společník	k1gMnPc6	společník
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
naprosto	naprosto	k6eAd1	naprosto
zdevastován	zdevastován	k2eAgMnSc1d1	zdevastován
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
Karel	Karel	k1gMnSc1	Karel
nejdříve	dříve	k6eAd3	dříve
sídlit	sídlit	k5eAaImF	sídlit
v	v	k7c6	v
měšťanských	měšťanský	k2eAgInPc6d1	měšťanský
domech	dům	k1gInPc6	dům
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
purkrabí	purkrabí	k1gMnSc1	purkrabí
pražský	pražský	k2eAgMnSc1d1	pražský
Hynek	Hynek	k1gMnSc1	Hynek
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
k	k	k7c3	k
obývání	obývání	k1gNnSc3	obývání
purkrabský	purkrabský	k2eAgInSc4d1	purkrabský
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
musel	muset	k5eAaImAgMnS	muset
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
moc	moc	k1gFnSc4	moc
nějak	nějak	k6eAd1	nějak
ukotvit	ukotvit	k5eAaPmF	ukotvit
a	a	k8xC	a
udělil	udělit	k5eAaPmAgMnS	udělit
mu	on	k3xPp3gMnSc3	on
titul	titul	k1gInSc1	titul
markraběte	markrabě	k1gMnSc2	markrabě
moravského	moravský	k2eAgMnSc2d1	moravský
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
vlastně	vlastně	k9	vlastně
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
státě	stát	k1gInSc6	stát
lucemburské	lucemburský	k2eAgNnSc1d1	lucemburské
dvojvládí	dvojvládí	k1gNnSc1	dvojvládí
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
právě	právě	k9	právě
rozštěpení	rozštěpení	k1gNnSc4	rozštěpení
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
mladý	mladý	k2eAgMnSc1d1	mladý
versus	versus	k7c1	versus
starý	starý	k2eAgMnSc1d1	starý
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
chtěl	chtít	k5eAaImAgMnS	chtít
Jan	Jan	k1gMnSc1	Jan
udělením	udělení	k1gNnSc7	udělení
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
chtěl	chtít	k5eAaImAgMnS	chtít
dát	dát	k5eAaPmF	dát
Karlovi	Karel	k1gMnSc3	Karel
dostatečně	dostatečně	k6eAd1	dostatečně
reprezentativní	reprezentativní	k2eAgFnSc4d1	reprezentativní
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
markrabě	markrabě	k1gMnSc1	markrabě
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
chopil	chopit	k5eAaPmAgMnS	chopit
iniciativy	iniciativa	k1gFnPc4	iniciativa
<g/>
,	,	kIx,	,
sídlil	sídlit	k5eAaImAgInS	sídlit
sice	sice	k8xC	sice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1333	[number]	k4	1333
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Budyšína	Budyšín	k1gInSc2	Budyšín
a	a	k8xC	a
Žitavy	Žitava	k1gFnSc2	Žitava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1334	[number]	k4	1334
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgFnSc2d1	Pražská
Karlovi	Karel	k1gMnSc3	Karel
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc1	tisíc
kop	kopa	k1gFnPc2	kopa
pražských	pražský	k2eAgInPc2d1	pražský
grošů	groš	k1gInPc2	groš
na	na	k7c4	na
částečné	částečný	k2eAgNnSc4d1	částečné
splácení	splácení	k1gNnSc4	splácení
královských	královský	k2eAgInPc2d1	královský
dluhů	dluh	k1gInPc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtická	šlechtický	k2eAgFnSc1d1	šlechtická
obec	obec	k1gFnSc1	obec
povolila	povolit	k5eAaPmAgFnS	povolit
Karlovi	Karel	k1gMnSc3	Karel
obecnou	obecná	k1gFnSc4	obecná
berni	berni	k?	berni
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
brzy	brzy	k6eAd1	brzy
schopen	schopen	k2eAgMnSc1d1	schopen
ze	z	k7c2	z
zástavy	zástava	k1gFnSc2	zástava
vykoupit	vykoupit	k5eAaPmF	vykoupit
alespoň	alespoň	k9	alespoň
některé	některý	k3yIgInPc4	některý
královské	královský	k2eAgInPc4d1	královský
hrady	hrad	k1gInPc4	hrad
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgInPc4	který
náležel	náležet	k5eAaImAgInS	náležet
například	například	k6eAd1	například
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
<g/>
,	,	kIx,	,
Týřov	Týřov	k1gInSc1	Týřov
<g/>
,	,	kIx,	,
Veveří	veveří	k2eAgNnSc4d1	veveří
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
také	také	k9	také
uděloval	udělovat	k5eAaImAgInS	udělovat
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
privilegia	privilegium	k1gNnPc4	privilegium
a	a	k8xC	a
soudní	soudní	k2eAgFnPc4d1	soudní
imunity	imunita	k1gFnPc4	imunita
klášterům	klášter	k1gInPc3	klášter
jak	jak	k8xS	jak
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
svůj	svůj	k3xOyFgInSc4	svůj
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
se	se	k3xPyFc4	se
markrabě	markrabě	k1gMnSc1	markrabě
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
i	i	k9	i
důležitá	důležitý	k2eAgNnPc1d1	důležité
města	město	k1gNnPc1	město
Kouřim	Kouřim	k1gFnSc1	Kouřim
<g/>
,	,	kIx,	,
Jihlavu	Jihlava	k1gFnSc4	Jihlava
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Písek	Písek	k1gInSc1	Písek
aj.	aj.	kA	aj.
Velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
Karel	Karla	k1gFnPc2	Karla
věnoval	věnovat	k5eAaPmAgMnS	věnovat
také	také	k9	také
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
nyní	nyní	k6eAd1	nyní
pojat	pojmout	k5eAaPmNgInS	pojmout
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
sídla	sídlo	k1gNnSc2	sídlo
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
paláce	palác	k1gInSc2	palác
Karel	Karel	k1gMnSc1	Karel
pospíchal	pospíchat	k5eAaImAgMnS	pospíchat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
chtěl	chtít	k5eAaImAgMnS	chtít
uvítat	uvítat	k5eAaPmF	uvítat
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
Blanku	Blanka	k1gFnSc4	Blanka
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
však	však	k9	však
na	na	k7c6	na
dostavění	dostavění	k1gNnSc6	dostavění
paláce	palác	k1gInSc2	palác
nečekal	čekat	k5eNaImAgMnS	čekat
a	a	k8xC	a
manželku	manželka	k1gFnSc4	manželka
pozval	pozvat	k5eAaPmAgMnS	pozvat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
utvořil	utvořit	k5eAaPmAgMnS	utvořit
alespoň	alespoň	k9	alespoň
nejnutnější	nutný	k2eAgFnPc4d3	nejnutnější
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Blanka	Blanka	k1gFnSc1	Blanka
přijela	přijet	k5eAaPmAgFnS	přijet
v	v	k7c6	v
početném	početný	k2eAgInSc6d1	početný
doprovodu	doprovod	k1gInSc6	doprovod
francouzských	francouzský	k2eAgMnPc2d1	francouzský
dvořanů	dvořan	k1gMnPc2	dvořan
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1334	[number]	k4	1334
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
doprovod	doprovod	k1gInSc1	doprovod
způsobil	způsobit	k5eAaPmAgInS	způsobit
u	u	k7c2	u
českých	český	k2eAgMnPc2d1	český
obyvatel	obyvatel	k1gMnPc2	obyvatel
rozpačité	rozpačitý	k2eAgInPc1d1	rozpačitý
dojmy	dojem	k1gInPc1	dojem
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
uchváceni	uchvátit	k5eAaPmNgMnP	uchvátit
především	především	k9	především
oslnivou	oslnivý	k2eAgFnSc4d1	oslnivá
nádhernou	nádherný	k2eAgFnSc4d1	nádherná
francouzských	francouzský	k2eAgMnPc2d1	francouzský
oděvů	oděv	k1gInPc2	oděv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ještě	ještě	k6eAd1	ještě
nikdy	nikdy	k6eAd1	nikdy
nespatřili	spatřit	k5eNaPmAgMnP	spatřit
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
nesrozumitelná	srozumitelný	k2eNgFnSc1d1	nesrozumitelná
mluva	mluva	k1gFnSc1	mluva
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
panstvu	panstvo	k1gNnSc6	panstvo
pocity	pocit	k1gInPc1	pocit
nepříjemného	příjemný	k2eNgNnSc2d1	nepříjemné
cizáctví	cizáctví	k1gNnSc2	cizáctví
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Žitavský	žitavský	k2eAgMnSc1d1	žitavský
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kronice	kronika	k1gFnSc6	kronika
nejprve	nejprve	k6eAd1	nejprve
pochválil	pochválit	k5eAaPmAgMnS	pochválit
Blančin	Blančin	k2eAgInSc4d1	Blančin
zjev	zjev	k1gInSc4	zjev
a	a	k8xC	a
krásu	krása	k1gFnSc4	krása
a	a	k8xC	a
jazykové	jazykový	k2eAgFnPc4d1	jazyková
těžkosti	těžkost	k1gFnPc4	těžkost
komentoval	komentovat	k5eAaBmAgMnS	komentovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c4	za
velikou	veliký	k2eAgFnSc4d1	veliká
obtíž	obtíž	k1gFnSc4	obtíž
pokládáme	pokládat	k5eAaImIp1nP	pokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sama	sám	k3xTgFnSc1	sám
mluví	mluvit	k5eAaImIp3nS	mluvit
jen	jen	k9	jen
francouzským	francouzský	k2eAgInSc7d1	francouzský
jazykem	jazyk	k1gInSc7	jazyk
...	...	k?	...
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
<g/>
)	)	kIx)	)
začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
učit	učit	k5eAaImF	učit
jazyku	jazyk	k1gInSc3	jazyk
německému	německý	k2eAgNnSc3d1	německé
a	a	k8xC	a
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
cvičívá	cvičívat	k5eAaImIp3nS	cvičívat
než	než	k8xS	než
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
českém	český	k2eAgInSc6d1	český
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
skoro	skoro	k6eAd1	skoro
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
městech	město	k1gNnPc6	město
království	království	k1gNnSc2	království
a	a	k8xC	a
před	před	k7c7	před
králem	král	k1gMnSc7	král
obecněji	obecně	k6eAd2	obecně
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
jazyka	jazyk	k1gInSc2	jazyk
německého	německý	k2eAgInSc2d1	německý
než	než	k8xS	než
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Karel	Karel	k1gMnSc1	Karel
si	se	k3xPyFc3	se
nechtěl	chtít	k5eNaImAgMnS	chtít
šlechtu	šlechta	k1gFnSc4	šlechta
poštvat	poštvat	k5eAaPmF	poštvat
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jako	jako	k9	jako
kdysi	kdysi	k6eAd1	kdysi
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
francouzské	francouzský	k2eAgMnPc4d1	francouzský
dvořany	dvořan	k1gMnPc4	dvořan
brzy	brzy	k6eAd1	brzy
odeslal	odeslat	k5eAaPmAgMnS	odeslat
na	na	k7c4	na
zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
bylo	být	k5eAaImAgNnS	být
manželství	manželství	k1gNnSc1	manželství
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
Blanky	Blanka	k1gFnSc2	Blanka
obdařeno	obdařen	k2eAgNnSc4d1	obdařeno
první	první	k4xOgFnSc2	první
radostí	radost	k1gFnPc2	radost
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1335	[number]	k4	1335
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
první	první	k4xOgFnSc1	první
dcera	dcera	k1gFnSc1	dcera
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
a	a	k8xC	a
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
lucemburské	lucemburský	k2eAgFnSc6d1	Lucemburská
babičce	babička	k1gFnSc6	babička
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
vystrojil	vystrojit	k5eAaPmAgMnS	vystrojit
Karel	Karel	k1gMnSc1	Karel
své	svůj	k3xOyFgFnSc2	svůj
sestře	sestra	k1gFnSc6	sestra
Anně	Anna	k1gFnSc6	Anna
svatbu	svatba	k1gFnSc4	svatba
s	s	k7c7	s
Otou	Ota	k1gMnSc7	Ota
Habsburským	habsburský	k2eAgMnSc7d1	habsburský
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgNnP	konat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1335	[number]	k4	1335
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
1335	[number]	k4	1335
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
hradě	hrad	k1gInSc6	hrad
Tirolu	Tirol	k1gInSc2	Tirol
vévoda	vévoda	k1gMnSc1	vévoda
Jindřich	Jindřich	k1gMnSc1	Jindřich
Korutanský	korutanský	k2eAgMnSc1d1	korutanský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
stát	stát	k5eAaImF	stát
jeho	jeho	k3xOp3gMnPc1	jeho
dědici	dědic	k1gMnPc1	dědic
jeho	jeho	k3xOp3gFnPc4	jeho
dcera	dcera	k1gFnSc1	dcera
Markéta	Markéta	k1gFnSc1	Markéta
a	a	k8xC	a
zeť	zeť	k1gMnSc1	zeť
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Karlův	Karlův	k2eAgMnSc1d1	Karlův
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Ludvík	Ludvík	k1gMnSc1	Ludvík
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Bavor	Bavor	k1gMnSc1	Bavor
se	se	k3xPyFc4	se
však	však	k9	však
spikl	spiknout	k5eAaPmAgMnS	spiknout
s	s	k7c7	s
Habsburky	Habsburk	k1gMnPc7	Habsburk
a	a	k8xC	a
společně	společně	k6eAd1	společně
rozpoutali	rozpoutat	k5eAaPmAgMnP	rozpoutat
proti	proti	k7c3	proti
Lucemburkům	Lucemburk	k1gMnPc3	Lucemburk
zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
tyrolské	tyrolský	k2eAgNnSc4d1	tyrolské
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1335	[number]	k4	1335
udělil	udělit	k5eAaPmAgMnS	udělit
císař	císař	k1gMnSc1	císař
Habsburkům	Habsburk	k1gMnPc3	Habsburk
část	část	k1gFnSc4	část
Tyrol	Tyroly	k1gInPc2	Tyroly
a	a	k8xC	a
Kraňska	Kraňsko	k1gNnSc2	Kraňsko
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
spokojil	spokojit	k5eAaPmAgMnS	spokojit
se	s	k7c7	s
severními	severní	k2eAgInPc7d1	severní
Tyroly	Tyroly	k1gInPc7	Tyroly
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Jan	Jan	k1gMnSc1	Jan
byl	být	k5eAaImAgMnS	být
tehdy	tehdy	k6eAd1	tehdy
upoután	upoutat	k5eAaPmNgMnS	upoutat
na	na	k7c6	na
lůžku	lůžko	k1gNnSc6	lůžko
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
po	po	k7c6	po
nešťastném	šťastný	k2eNgNnSc6d1	nešťastné
zranění	zranění	k1gNnSc6	zranění
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
tak	tak	k6eAd1	tak
musel	muset	k5eAaImAgMnS	muset
jednat	jednat	k5eAaImF	jednat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
švagr	švagr	k1gMnSc1	švagr
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Dolnobavorský	Dolnobavorský	k2eAgMnSc1d1	Dolnobavorský
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
vypravili	vypravit	k5eAaPmAgMnP	vypravit
do	do	k7c2	do
Lince	Linec	k1gInSc2	Linec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podali	podat	k5eAaPmAgMnP	podat
přímý	přímý	k2eAgInSc4d1	přímý
protest	protest	k1gInSc4	protest
u	u	k7c2	u
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
švagr	švagr	k1gMnSc1	švagr
se	se	k3xPyFc4	se
prý	prý	k9	prý
zle	zle	k6eAd1	zle
obořili	obořit	k5eAaPmAgMnP	obořit
na	na	k7c4	na
císaře	císař	k1gMnSc4	císař
a	a	k8xC	a
vykřikovali	vykřikovat	k5eAaImAgMnP	vykřikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dopustil	dopustit	k5eAaPmAgMnS	dopustit
zlého	zlý	k2eAgInSc2d1	zlý
skutku	skutek	k1gInSc2	skutek
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
však	však	k9	však
Karla	Karel	k1gMnSc4	Karel
hrubě	hrubě	k6eAd1	hrubě
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
striktně	striktně	k6eAd1	striktně
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
nároky	nárok	k1gInPc4	nárok
Markéty	Markéta	k1gFnSc2	Markéta
Tyrolské	tyrolský	k2eAgInPc4d1	tyrolský
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
chvíli	chvíle	k1gFnSc6	chvíle
bezmocní	bezmocný	k1gMnPc1	bezmocný
a	a	k8xC	a
krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
formovat	formovat	k5eAaImF	formovat
nová	nový	k2eAgFnSc1d1	nová
nepřátelská	přátelský	k2eNgFnSc1d1	nepřátelská
koalice	koalice	k1gFnSc1	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
začal	začít	k5eAaPmAgMnS	začít
totiž	totiž	k9	totiž
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
dcery	dcera	k1gFnSc2	dcera
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
Kazimíra	Kazimír	k1gMnSc2	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Římana	Říman	k1gMnSc4	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
schůdnější	schůdný	k2eAgNnSc1d2	schůdnější
řešení	řešení	k1gNnSc1	řešení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mohlo	moct	k5eAaImAgNnS	moct
lucemburské	lucemburský	k2eAgFnPc4d1	Lucemburská
pozice	pozice	k1gFnPc4	pozice
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
přijel	přijet	k5eAaPmAgMnS	přijet
za	za	k7c7	za
synem	syn	k1gMnSc7	syn
a	a	k8xC	a
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
Roberta	Robert	k1gMnSc2	Robert
<g/>
,	,	kIx,	,
jednali	jednat	k5eAaImAgMnP	jednat
v	v	k7c6	v
uherském	uherský	k2eAgInSc6d1	uherský
Trenčíně	Trenčín	k1gInSc6	Trenčín
s	s	k7c7	s
vyslanci	vyslanec	k1gMnPc1	vyslanec
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
vzdal	vzdát	k5eAaPmAgMnS	vzdát
titulu	titul	k1gInSc3	titul
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
výměnou	výměna	k1gFnSc7	výměna
se	se	k3xPyFc4	se
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
už	už	k6eAd1	už
fakticky	fakticky	k6eAd1	fakticky
náleželo	náležet	k5eAaImAgNnS	náležet
k	k	k7c3	k
českému	český	k2eAgNnSc3d1	české
království	království	k1gNnSc3	království
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
Janovi	Jan	k1gMnSc3	Jan
vyplatit	vyplatit	k5eAaPmF	vyplatit
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
změněných	změněný	k2eAgFnPc6d1	změněná
na	na	k7c4	na
20	[number]	k4	20
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
)	)	kIx)	)
kop	kop	k1gInSc4	kop
pražských	pražský	k2eAgInPc2d1	pražský
grošů	groš	k1gInPc2	groš
<g/>
.	.	kIx.	.
</s>
<s>
Konečné	Konečné	k2eAgFnPc1d1	Konečné
dohody	dohoda	k1gFnPc1	dohoda
byly	být	k5eAaImAgFnP	být
podepsány	podepsat	k5eAaPmNgFnP	podepsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1335	[number]	k4	1335
po	po	k7c6	po
jednáních	jednání	k1gNnPc6	jednání
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgNnPc2	jenž
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
Češi	Čech	k1gMnPc1	Čech
včetně	včetně	k7c2	včetně
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Uhři	Uhř	k1gMnPc1	Uhř
a	a	k8xC	a
zástupci	zástupce	k1gMnPc1	zástupce
řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
v	v	k7c6	v
uherském	uherský	k2eAgInSc6d1	uherský
Visegrádu	Visegrád	k1gInSc2	Visegrád
<g/>
.	.	kIx.	.
</s>
<s>
Protilucemburská	Protilucemburský	k2eAgFnSc1d1	Protilucemburský
koalice	koalice	k1gFnSc1	koalice
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
u	u	k7c2	u
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
rozpory	rozpor	k1gInPc1	rozpor
mezi	mezi	k7c7	mezi
Janem	Jan	k1gMnSc7	Jan
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
samostatnější	samostatný	k2eAgNnSc4d2	samostatnější
vystupování	vystupování	k1gNnSc4	vystupování
mladého	mladý	k2eAgMnSc2d1	mladý
markraběte	markrabě	k1gMnSc2	markrabě
Karla	Karel	k1gMnSc2	Karel
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stalo	stát	k5eAaPmAgNnS	stát
trnem	trn	k1gInSc7	trn
v	v	k7c4	v
oku	oka	k1gFnSc4	oka
jeho	jeho	k3xOp3gMnSc3	jeho
otci	otec	k1gMnSc3	otec
Janovi	Jan	k1gMnSc3	Jan
i	i	k8xC	i
šlechtické	šlechtický	k2eAgFnSc3d1	šlechtická
obci	obec	k1gFnSc3	obec
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	šlechta	k1gFnSc1	šlechta
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
obávat	obávat	k5eAaImF	obávat
obnovy	obnova	k1gFnSc2	obnova
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
snažil	snažit	k5eAaImAgMnS	snažit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
sáhla	sáhnout	k5eAaPmAgFnS	sáhnout
k	k	k7c3	k
osvědčenému	osvědčený	k2eAgInSc3d1	osvědčený
prostředku	prostředek	k1gInSc3	prostředek
<g/>
,	,	kIx,	,
zasetí	zasetí	k1gNnSc3	zasetí
nedůvěry	nedůvěra	k1gFnSc2	nedůvěra
mezi	mezi	k7c4	mezi
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Krále	Král	k1gMnSc2	Král
snad	snad	k9	snad
popuzovalo	popuzovat	k5eAaImAgNnS	popuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohl	moct	k5eNaImAgMnS	moct
libovolně	libovolně	k6eAd1	libovolně
kořistit	kořistit	k5eAaImF	kořistit
na	na	k7c6	na
majetku	majetek	k1gInSc6	majetek
měst	město	k1gNnPc2	město
a	a	k8xC	a
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
doposud	doposud	k6eAd1	doposud
jeho	jeho	k3xOp3gInSc7	jeho
zvykem	zvyk	k1gInSc7	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Karla	Karla	k1gFnSc1	Karla
zase	zase	k9	zase
popuzovala	popuzovat	k5eAaImAgFnS	popuzovat
nedůsledná	důsledný	k2eNgFnSc1d1	nedůsledná
politika	politika	k1gFnSc1	politika
otce	otec	k1gMnSc2	otec
vůči	vůči	k7c3	vůči
císaři	císař	k1gMnSc3	císař
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
nehospodárnost	nehospodárnost	k1gFnSc1	nehospodárnost
a	a	k8xC	a
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
zadlužení	zadlužení	k1gNnSc1	zadlužení
<g/>
.	.	kIx.	.
</s>
<s>
Východiskem	východisko	k1gNnSc7	východisko
z	z	k7c2	z
napjatých	napjatý	k2eAgInPc2d1	napjatý
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stala	stát	k5eAaPmAgFnS	stát
naléhavá	naléhavý	k2eAgFnSc1d1	naléhavá
prosba	prosba	k1gFnSc1	prosba
Karlova	Karlův	k2eAgMnSc2d1	Karlův
bratra	bratr	k1gMnSc2	bratr
Jana	Jan	k1gMnSc2	Jan
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1336	[number]	k4	1336
se	se	k3xPyFc4	se
tak	tak	k9	tak
Karel	Karel	k1gMnSc1	Karel
chopil	chopit	k5eAaPmAgMnS	chopit
poručnické	poručnický	k2eAgFnSc2d1	poručnická
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Tyrolsku	Tyrolsko	k1gNnSc6	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karel	Karel	k1gMnSc1	Karel
zvolil	zvolit	k5eAaPmAgMnS	zvolit
jen	jen	k9	jen
malý	malý	k2eAgInSc4d1	malý
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
doprovod	doprovod	k1gInSc4	doprovod
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
složení	složení	k1gNnSc1	složení
jeho	on	k3xPp3gInSc2	on
zbylého	zbylý	k2eAgInSc2d1	zbylý
doprovodu	doprovod	k1gInSc2	doprovod
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
než	než	k8xS	než
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
činnost	činnost	k1gFnSc4	činnost
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
na	na	k7c4	na
činnost	činnost	k1gFnSc4	činnost
politicko-administrativní	politickodministrativní	k2eAgFnSc4d1	politicko-administrativní
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
pohanské	pohanský	k2eAgFnSc2d1	pohanská
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
Lucemburky	Lucemburk	k1gInPc7	Lucemburk
však	však	k9	však
nadále	nadále	k6eAd1	nadále
vzrůstaly	vzrůstat	k5eAaImAgFnP	vzrůstat
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byla	být	k5eAaImAgFnS	být
mezitím	mezitím	k6eAd1	mezitím
korunována	korunovat	k5eAaBmNgFnS	korunovat
Beatrix	Beatrix	k1gInSc1	Beatrix
Bourbonská	bourbonský	k2eAgFnSc1d1	Bourbonská
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
manželka	manželka	k1gFnSc1	manželka
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
Václava	Václav	k1gMnSc4	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Korunovace	korunovace	k1gFnSc1	korunovace
Beatrix	Beatrix	k1gInSc1	Beatrix
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nezískala	získat	k5eNaPmAgFnS	získat
přílišnou	přílišný	k2eAgFnSc4d1	přílišná
oblibu	obliba	k1gFnSc4	obliba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
setkala	setkat	k5eAaPmAgFnS	setkat
u	u	k7c2	u
Pražanů	Pražan	k1gMnPc2	Pražan
s	s	k7c7	s
okázalým	okázalý	k2eAgInSc7d1	okázalý
nezájmem	nezájem	k1gInSc7	nezájem
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
nakonec	nakonec	k6eAd1	nakonec
Čechy	Čech	k1gMnPc4	Čech
opustila	opustit	k5eAaPmAgFnS	opustit
a	a	k8xC	a
novorozeně	novorozeně	k6eAd1	novorozeně
svěřila	svěřit	k5eAaPmAgFnS	svěřit
kojné	kojná	k1gFnPc4	kojná
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Jan	Jan	k1gMnSc1	Jan
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
uražen	urazit	k5eAaPmNgMnS	urazit
a	a	k8xC	a
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
když	když	k8xS	když
Pražané	Pražan	k1gMnPc1	Pražan
vzdávali	vzdávat	k5eAaImAgMnP	vzdávat
poctu	pocta	k1gFnSc4	pocta
Karlově	Karlův	k2eAgFnSc3d1	Karlova
manželce	manželka	k1gFnSc3	manželka
Blance	Blanka	k1gFnSc3	Blanka
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
Jana	Jana	k1gFnSc1	Jana
musela	muset	k5eAaImAgFnS	muset
nakonec	nakonec	k6eAd1	nakonec
Blanka	Blanka	k1gFnSc1	Blanka
roku	rok	k1gInSc2	rok
1337	[number]	k4	1337
opustit	opustit	k5eAaPmF	opustit
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
přesídlit	přesídlit	k5eAaPmF	přesídlit
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
navíc	navíc	k6eAd1	navíc
zakázal	zakázat	k5eAaPmAgMnS	zakázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
za	za	k7c4	za
Karlem	Karel	k1gMnSc7	Karel
zasílány	zasílán	k2eAgFnPc1d1	zasílána
důchody	důchod	k1gInPc4	důchod
z	z	k7c2	z
Moravského	moravský	k2eAgNnSc2d1	Moravské
markrabství	markrabství	k1gNnSc2	markrabství
<g/>
.	.	kIx.	.
</s>
<s>
Opakovala	opakovat	k5eAaImAgFnS	opakovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
jako	jako	k8xC	jako
kdysi	kdysi	k6eAd1	kdysi
při	při	k7c6	při
roztržce	roztržka	k1gFnSc6	roztržka
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
s	s	k7c7	s
Karlovou	Karlův	k2eAgFnSc7d1	Karlova
matkou	matka	k1gFnSc7	matka
Eliškou	Eliška	k1gFnSc7	Eliška
Přemyslovnou	Přemyslovna	k1gFnSc7	Přemyslovna
<g/>
.	.	kIx.	.
</s>
<s>
Karlův	Karlův	k2eAgInSc4d1	Karlův
útěk	útěk	k1gInSc4	útěk
před	před	k7c7	před
otcovou	otcův	k2eAgFnSc7d1	otcova
nevolí	nevole	k1gFnSc7	nevole
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
riskantní	riskantní	k2eAgMnSc1d1	riskantní
<g/>
,	,	kIx,	,
do	do	k7c2	do
Tyrol	Tyroly	k1gInPc2	Tyroly
musel	muset	k5eAaImAgMnS	muset
cestovat	cestovat	k5eAaImF	cestovat
přes	přes	k7c4	přes
Uhersko	Uhersko	k1gNnSc4	Uhersko
a	a	k8xC	a
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
plavit	plavit	k5eAaImF	plavit
přes	přes	k7c4	přes
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dobrodružné	dobrodružný	k2eAgFnSc6d1	dobrodružná
cestě	cesta	k1gFnSc6	cesta
dokonce	dokonce	k9	dokonce
plachetnice	plachetnice	k1gFnSc1	plachetnice
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
Karel	Karel	k1gMnSc1	Karel
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
,	,	kIx,	,
padla	padnout	k5eAaImAgFnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
pirátů	pirát	k1gMnPc2	pirát
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
lstí	lest	k1gFnSc7	lest
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
1337	[number]	k4	1337
dorazil	dorazit	k5eAaPmAgMnS	dorazit
Karel	Karel	k1gMnSc1	Karel
do	do	k7c2	do
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zapletl	zaplést	k5eAaPmAgMnS	zaplést
do	do	k7c2	do
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Lombardii	Lombardie	k1gFnSc6	Lombardie
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Benátek	Benátky	k1gFnPc2	Benátky
proti	proti	k7c3	proti
veronskému	veronský	k2eAgMnSc3d1	veronský
a	a	k8xC	a
padovskému	padovský	k2eAgMnSc3d1	padovský
vládci	vládce	k1gMnSc3	vládce
Mastinovi	Mastin	k1gMnSc3	Mastin
della	della	k6eAd1	della
Scala	scát	k5eAaImAgNnP	scát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1337	[number]	k4	1337
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
nepřítomný	přítomný	k2eNgMnSc1d1	nepřítomný
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
hrozící	hrozící	k2eAgNnSc1d1	hrozící
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Bavora	Bavor	k1gMnSc2	Bavor
nakonec	nakonec	k6eAd1	nakonec
Jana	Jan	k1gMnSc4	Jan
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
smířilo	smířit	k5eAaPmAgNnS	smířit
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1339	[number]	k4	1339
postupovali	postupovat	k5eAaImAgMnP	postupovat
ve	v	k7c6	v
svorné	svorný	k2eAgFnSc6d1	svorná
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1340	[number]	k4	1340
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
jihofrancouzském	jihofrancouzský	k2eAgInSc6d1	jihofrancouzský
Montpellieru	Montpellier	k1gInSc6	Montpellier
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
Jan	Jan	k1gMnSc1	Jan
léčil	léčit	k5eAaImAgMnS	léčit
zrak	zrak	k1gInSc4	zrak
u	u	k7c2	u
lékařů	lékař	k1gMnPc2	lékař
na	na	k7c6	na
zdejší	zdejší	k2eAgFnSc6d1	zdejší
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
Karel	Karel	k1gMnSc1	Karel
vydat	vydat	k5eAaPmF	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
kastilskému	kastilský	k2eAgMnSc3d1	kastilský
králi	král	k1gMnSc3	král
Alfonsi	Alfons	k1gMnSc3	Alfons
XI	XI	kA	XI
<g/>
.	.	kIx.	.
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
Maurům	Maur	k1gMnPc3	Maur
z	z	k7c2	z
Granady	Granada	k1gFnSc2	Granada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
této	tento	k3xDgFnSc3	tento
vojenské	vojenský	k2eAgFnSc3d1	vojenská
výpravě	výprava	k1gFnSc3	výprava
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
zabránil	zabránit	k5eAaPmAgMnS	zabránit
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
při	při	k7c6	při
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
léčbě	léčba	k1gFnSc6	léčba
oslepl	oslepnout	k5eAaPmAgMnS	oslepnout
i	i	k9	i
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
oko	oko	k1gNnSc4	oko
a	a	k8xC	a
obával	obávat	k5eAaImAgMnS	obávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gFnSc4	jeho
prvorozený	prvorozený	k2eAgMnSc1d1	prvorozený
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
nástupce	nástupce	k1gMnSc1	nástupce
mohl	moct	k5eAaImAgMnS	moct
na	na	k7c6	na
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
bojišti	bojiště	k1gNnSc6	bojiště
zahynout	zahynout	k5eAaPmF	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1341	[number]	k4	1341
svolal	svolat	k5eAaPmAgMnS	svolat
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
zasedání	zasedání	k1gNnSc4	zasedání
českého	český	k2eAgInSc2d1	český
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Jan	Jan	k1gMnSc1	Jan
zde	zde	k6eAd1	zde
oznámil	oznámit	k5eAaPmAgMnS	oznámit
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
,	,	kIx,	,
duchovenstvu	duchovenstvo	k1gNnSc3	duchovenstvo
i	i	k8xC	i
zástupcům	zástupce	k1gMnPc3	zástupce
královských	královský	k2eAgNnPc2d1	královské
měst	město	k1gNnPc2	město
své	své	k1gNnSc4	své
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Sněm	sněm	k1gInSc1	sněm
slavnostně	slavnostně	k6eAd1	slavnostně
přijal	přijmout	k5eAaPmAgInS	přijmout
markraběte	markrabě	k1gMnSc4	markrabě
Karla	Karel	k1gMnSc4	Karel
jako	jako	k9	jako
budoucího	budoucí	k2eAgMnSc4d1	budoucí
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
markrabě	markrabě	k1gMnSc1	markrabě
chtěl	chtít	k5eAaImAgMnS	chtít
také	také	k9	také
prosadit	prosadit	k5eAaPmF	prosadit
svou	svůj	k3xOyFgFnSc4	svůj
královskou	královský	k2eAgFnSc4d1	královská
korunovaci	korunovace	k1gFnSc4	korunovace
ještě	ještě	k9	ještě
za	za	k7c2	za
otcova	otcův	k2eAgInSc2d1	otcův
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
překážky	překážka	k1gFnPc4	překážka
nezdařilo	zdařit	k5eNaPmAgNnS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byly	být	k5eAaImAgFnP	být
lucemburské	lucemburský	k2eAgFnPc4d1	Lucemburská
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
Tyrolsku	Tyrolsko	k1gNnSc6	Tyrolsko
zlomeny	zlomen	k2eAgFnPc1d1	zlomena
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
byl	být	k5eAaImAgMnS	být
odsud	odsud	k6eAd1	odsud
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
vyhnán	vyhnat	k5eAaPmNgMnS	vyhnat
a	a	k8xC	a
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
císař	císař	k1gMnSc1	císař
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
výrazně	výrazně	k6eAd1	výrazně
podkopalo	podkopat	k5eAaPmAgNnS	podkopat
lucemburskou	lucemburský	k2eAgFnSc4d1	Lucemburská
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
nepřátelství	nepřátelství	k1gNnSc3	nepřátelství
mezi	mezi	k7c7	mezi
Lucemburky	Lucemburk	k1gMnPc7	Lucemburk
a	a	k8xC	a
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Pocit	pocit	k1gInSc1	pocit
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c4	nad
Lucemburky	Lucemburk	k1gInPc4	Lucemburk
si	se	k3xPyFc3	se
však	však	k9	však
císař	císař	k1gMnSc1	císař
dlouho	dlouho	k6eAd1	dlouho
neužíval	užívat	k5eNaImAgMnS	užívat
<g/>
,	,	kIx,	,
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
mírného	mírný	k2eAgMnSc2d1	mírný
papeže	papež	k1gMnSc2	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XII	XII	kA	XII
<g/>
.	.	kIx.	.
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Rosieres	Rosieres	k1gMnSc1	Rosieres
jako	jako	k8xS	jako
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
nového	nový	k2eAgMnSc2d1	nový
papeže	papež	k1gMnSc2	papež
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
Lucemburky	Lucemburk	k1gInPc4	Lucemburk
naději	nadát	k5eAaBmIp1nS	nadát
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
před	před	k7c7	před
rozpínajícím	rozpínající	k2eAgMnSc7d1	rozpínající
se	se	k3xPyFc4	se
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pospíchali	pospíchat	k5eAaImAgMnP	pospíchat
novému	nový	k2eAgMnSc3d1	nový
papeži	papež	k1gMnSc3	papež
poblahopřát	poblahopřát	k5eAaPmF	poblahopřát
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1342	[number]	k4	1342
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Jan	Jan	k1gMnSc1	Jan
v	v	k7c6	v
papežském	papežský	k2eAgInSc6d1	papežský
paláci	palác	k1gInSc6	palác
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
své	svůj	k3xOyFgFnSc2	svůj
přísahy	přísaha	k1gFnSc2	přísaha
věrnosti	věrnost	k1gFnSc2	věrnost
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1344	[number]	k4	1344
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
sám	sám	k3xTgMnSc1	sám
Karel	Karel	k1gMnSc1	Karel
pozván	pozvat	k5eAaPmNgMnS	pozvat
papežem	papež	k1gMnSc7	papež
do	do	k7c2	do
Avignonu	Avignon	k1gInSc2	Avignon
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
prý	prý	k9	prý
musel	muset	k5eAaImAgMnS	muset
Karla	Karel	k1gMnSc4	Karel
a	a	k8xC	a
krále	král	k1gMnSc4	král
Jana	Jan	k1gMnSc4	Jan
opět	opět	k6eAd1	opět
usmiřovat	usmiřovat	k5eAaImF	usmiřovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
opět	opět	k6eAd1	opět
v	v	k7c6	v
ostrém	ostrý	k2eAgInSc6d1	ostrý
rozporu	rozpor	k1gInSc6	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
pro	pro	k7c4	pro
Lucemburky	Lucemburk	k1gInPc4	Lucemburk
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
dokonce	dokonce	k9	dokonce
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
pražské	pražský	k2eAgNnSc1d1	Pražské
biskupství	biskupství	k1gNnSc4	biskupství
povýšeno	povýšen	k2eAgNnSc4d1	povýšeno
na	na	k7c4	na
arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
a	a	k8xC	a
Čechy	Čechy	k1gFnPc1	Čechy
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vymanily	vymanit	k5eAaPmAgFnP	vymanit
se	se	k3xPyFc4	se
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
mohučské	mohučský	k2eAgFnSc6d1	mohučská
arcidiecézi	arcidiecéze	k1gFnSc6	arcidiecéze
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1344	[number]	k4	1344
bylo	být	k5eAaImAgNnS	být
bulou	bula	k1gFnSc7	bula
Romanus	Romanus	k1gMnSc1	Romanus
Pontifex	pontifex	k1gMnSc1	pontifex
Klementa	Klement	k1gMnSc2	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
pražské	pražský	k2eAgNnSc1d1	Pražské
biskupství	biskupství	k1gNnSc1	biskupství
slavnostně	slavnostně	k6eAd1	slavnostně
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c4	na
arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Arnošt	Arnošt	k1gMnSc1	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
a	a	k8xC	a
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
také	také	k9	také
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
jednání	jednání	k1gNnSc6	jednání
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
spojenectví	spojenectví	k1gNnSc4	spojenectví
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
a	a	k8xC	a
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
se	se	k3xPyFc4	se
na	na	k7c6	na
Karlově	Karlův	k2eAgFnSc6d1	Karlova
kandidatuře	kandidatura	k1gFnSc6	kandidatura
ve	v	k7c6	v
volbě	volba	k1gFnSc6	volba
o	o	k7c4	o
nového	nový	k2eAgMnSc4d1	nový
římského	římský	k2eAgMnSc4d1	římský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
vrátili	vrátit	k5eAaPmAgMnP	vrátit
snad	snad	k9	snad
začátkem	začátkem	k7c2	začátkem
listopadu	listopad	k1gInSc2	listopad
1344	[number]	k4	1344
a	a	k8xC	a
společně	společně	k6eAd1	společně
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1344	[number]	k4	1344
položili	položit	k5eAaPmAgMnP	položit
základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
Svatovítské	svatovítský	k2eAgFnSc2d1	Svatovítská
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozpoutal	rozpoutat	k5eAaPmAgInS	rozpoutat
skutečný	skutečný	k2eAgInSc1d1	skutečný
zápas	zápas	k1gInSc1	zápas
Lucemburků	Lucemburk	k1gMnPc2	Lucemburk
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
římské	římský	k2eAgFnSc2d1	římská
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c6	v
získání	získání	k1gNnSc6	získání
římské	římský	k2eAgFnSc2d1	římská
koruny	koruna	k1gFnSc2	koruna
pro	pro	k7c4	pro
Karla	Karel	k1gMnSc4	Karel
sehrál	sehrát	k5eAaPmAgMnS	sehrát
jeho	jeho	k3xOp3gMnSc1	jeho
prastrýc	prastrýc	k1gMnSc1	prastrýc
Balduin	Balduin	k1gMnSc1	Balduin
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
musel	muset	k5eAaImAgMnS	muset
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
koruny	koruna	k1gFnSc2	koruna
složit	složit	k5eAaPmF	složit
závažné	závažný	k2eAgNnSc1d1	závažné
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
formální	formální	k2eAgInPc1d1	formální
<g/>
,	,	kIx,	,
sliby	slib	k1gInPc1	slib
papeži	papež	k1gMnSc3	papež
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
musel	muset	k5eAaImAgMnS	muset
vydat	vydat	k5eAaPmF	vydat
záruky	záruka	k1gFnPc4	záruka
a	a	k8xC	a
privilegia	privilegium	k1gNnPc4	privilegium
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
prastrýce	prastrýc	k1gMnSc4	prastrýc
Balduina	Balduin	k1gMnSc4	Balduin
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
Karel	Karel	k1gMnSc1	Karel
složil	složit	k5eAaPmAgMnS	složit
papeži	papež	k1gMnSc3	papež
sliby	slib	k1gInPc4	slib
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
ten	ten	k3xDgMnSc1	ten
na	na	k7c4	na
říšské	říšský	k2eAgMnPc4d1	říšský
kurfiřty	kurfiřt	k1gMnPc4	kurfiřt
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
nového	nový	k2eAgMnSc2d1	nový
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
památného	památný	k2eAgInSc2d1	památný
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1346	[number]	k4	1346
poblíž	poblíž	k7c2	poblíž
městečka	městečko	k1gNnSc2	městečko
Rhens	Rhensa	k1gFnPc2	Rhensa
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Rýna	Rýn	k1gInSc2	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
novým	nový	k2eAgMnSc7d1	nový
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
hlasy	hlas	k1gInPc7	hlas
pěti	pět	k4xCc2	pět
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
hlasy	hlas	k1gInPc4	hlas
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
trevírský	trevírský	k2eAgMnSc1d1	trevírský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Balduin	Balduin	k1gMnSc1	Balduin
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
mohučský	mohučský	k2eAgMnSc1d1	mohučský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Gerlach	Gerlach	k1gMnSc1	Gerlach
<g/>
,	,	kIx,	,
kolínský	kolínský	k2eAgMnSc1d1	kolínský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Walram	Walram	k1gInSc1	Walram
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
saský	saský	k2eAgMnSc1d1	saský
vévoda	vévoda	k1gMnSc1	vévoda
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozhodujícímu	rozhodující	k2eAgNnSc3d1	rozhodující
střetnutí	střetnutí	k1gNnSc3	střetnutí
mezi	mezi	k7c7	mezi
novým	nový	k2eAgMnSc7d1	nový
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
a	a	k8xC	a
císařem	císař	k1gMnSc7	císař
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
napadena	napaden	k2eAgFnSc1d1	napadena
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
Eduardem	Eduard	k1gMnSc7	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
proto	proto	k8xC	proto
požádal	požádat	k5eAaPmAgMnS	požádat
své	svůj	k3xOyFgMnPc4	svůj
spojence	spojenec	k1gMnPc4	spojenec
a	a	k8xC	a
přátele	přítel	k1gMnPc4	přítel
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
tradičně	tradičně	k6eAd1	tradičně
sepjatí	sepjatý	k2eAgMnPc1d1	sepjatý
s	s	k7c7	s
francouzským	francouzský	k2eAgInSc7d1	francouzský
dvorem	dvůr	k1gInSc7	dvůr
okamžitě	okamžitě	k6eAd1	okamžitě
svého	svůj	k3xOyFgMnSc4	svůj
spojence	spojenec	k1gMnSc4	spojenec
vyslyšeli	vyslyšet	k5eAaPmAgMnP	vyslyšet
a	a	k8xC	a
s	s	k7c7	s
vojenským	vojenský	k2eAgInSc7d1	vojenský
oddílem	oddíl	k1gInSc7	oddíl
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1346	[number]	k4	1346
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozhodující	rozhodující	k2eAgFnSc3d1	rozhodující
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
špatně	špatně	k6eAd1	špatně
vedené	vedený	k2eAgNnSc1d1	vedené
francouzské	francouzský	k2eAgNnSc1d1	francouzské
vojsko	vojsko	k1gNnSc1	vojsko
drtivě	drtivě	k6eAd1	drtivě
poraženo	porazit	k5eAaPmNgNnS	porazit
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
smrt	smrt	k1gFnSc4	smrt
zde	zde	k6eAd1	zde
nalezl	naleznout	k5eAaPmAgMnS	naleznout
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	jeho	k3xOp3gNnSc1	jeho
dvorní	dvorní	k2eAgMnSc1d1	dvorní
kronikář	kronikář	k1gMnSc1	kronikář
Beneš	Beneš	k1gMnSc1	Beneš
Krabice	krabice	k1gFnSc1	krabice
z	z	k7c2	z
Weitmile	Weitmila	k1gFnSc6	Weitmila
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
ostatní	ostatní	k2eAgMnPc1d1	ostatní
šlechtici	šlechtic	k1gMnPc1	šlechtic
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
padl	padnout	k5eAaImAgMnS	padnout
a	a	k8xC	a
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
neztratili	ztratit	k5eNaPmAgMnP	ztratit
oba	dva	k4xCgMnPc4	dva
panovníky	panovník	k1gMnPc4	panovník
<g/>
,	,	kIx,	,
vyvedli	vyvést	k5eAaPmAgMnP	vyvést
jeho	on	k3xPp3gMnSc4	on
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
pana	pan	k1gMnSc4	pan
zvolence	zvolenec	k1gMnSc4	zvolenec
Karla	Karel	k1gMnSc4	Karel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
tam	tam	k6eAd1	tam
nablízku	nablízku	k6eAd1	nablízku
udatně	udatně	k6eAd1	udatně
bojoval	bojovat	k5eAaImAgMnS	bojovat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
poraněn	poranit	k5eAaPmNgMnS	poranit
střelami	střela	k1gFnPc7	střela
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gInSc4	jeho
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
vůli	vůle	k1gFnSc3	vůle
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
bitvy	bitva	k1gFnSc2	bitva
a	a	k8xC	a
zavedli	zavést	k5eAaPmAgMnP	zavést
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgInS	stát
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
králem	král	k1gMnSc7	král
českým	český	k2eAgMnSc7d1	český
a	a	k8xC	a
římskoněmeckým	římskoněmecký	k2eAgMnSc7d1	římskoněmecký
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
vladařskou	vladařský	k2eAgFnSc7d1	vladařská
starostí	starost	k1gFnSc7	starost
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
pohřeb	pohřeb	k1gInSc4	pohřeb
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
poslední	poslední	k2eAgFnSc7d1	poslední
vůlí	vůle	k1gFnSc7	vůle
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
vlády	vláda	k1gFnSc2	vláda
dvojnásobného	dvojnásobný	k2eAgMnSc2d1	dvojnásobný
krále	král	k1gMnSc2	král
byly	být	k5eAaImAgFnP	být
nesnadné	snadný	k2eNgFnPc1d1	nesnadná
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
musel	muset	k5eAaImAgMnS	muset
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
uznání	uznání	k1gNnSc4	uznání
především	především	k9	především
proti	proti	k7c3	proti
žijícímu	žijící	k2eAgMnSc3d1	žijící
císaři	císař	k1gMnSc3	císař
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
Bavorovi	Bavor	k1gMnSc3	Bavor
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
měl	mít	k5eAaImAgMnS	mít
sice	sice	k8xC	sice
podporu	podpora	k1gFnSc4	podpora
hlavních	hlavní	k2eAgInPc2d1	hlavní
říšských	říšský	k2eAgInPc2d1	říšský
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bohatá	bohatý	k2eAgNnPc1d1	bohaté
říšská	říšský	k2eAgNnPc1d1	říšské
města	město	k1gNnPc1	město
zachovávala	zachovávat	k5eAaImAgNnP	zachovávat
přízeň	přízeň	k1gFnSc4	přízeň
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c4	na
římskoněmeckého	římskoněmecký	k2eAgMnSc4d1	římskoněmecký
krále	král	k1gMnSc4	král
korunovat	korunovat	k5eAaBmF	korunovat
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1346	[number]	k4	1346
v	v	k7c6	v
Bonnu	Bonn	k1gInSc6	Bonn
<g/>
,	,	kIx,	,
korunoval	korunovat	k5eAaBmAgInS	korunovat
jej	on	k3xPp3gMnSc4	on
kolínský	kolínský	k2eAgMnSc1d1	kolínský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Walram	Walram	k1gInSc1	Walram
<g/>
.	.	kIx.	.
</s>
<s>
Korunovace	korunovace	k1gFnSc1	korunovace
v	v	k7c6	v
tradičních	tradiční	k2eAgFnPc6d1	tradiční
Cáchách	Cáchy	k1gFnPc6	Cáchy
nebyla	být	k5eNaImAgFnS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
zachovávali	zachovávat	k5eAaImAgMnP	zachovávat
věrnost	věrnost	k1gFnSc4	věrnost
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
pak	pak	k6eAd1	pak
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
svého	svůj	k3xOyFgMnSc4	svůj
prastrýce	prastrýc	k1gMnSc4	prastrýc
Balduina	Balduina	k1gFnSc1	Balduina
říšským	říšský	k2eAgMnSc7d1	říšský
vikářem	vikář	k1gMnSc7	vikář
a	a	k8xC	a
svěřil	svěřit	k5eAaPmAgInS	svěřit
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
správu	správa	k1gFnSc4	správa
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Zpáteční	zpáteční	k2eAgFnSc1d1	zpáteční
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
krále	král	k1gMnSc4	král
značně	značně	k6eAd1	značně
riskantní	riskantní	k2eAgMnSc1d1	riskantní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
musel	muset	k5eAaImAgMnS	muset
cestovat	cestovat	k5eAaImF	cestovat
přes	přes	k7c4	přes
nepřátelské	přátelský	k2eNgNnSc4d1	nepřátelské
území	území	k1gNnSc4	území
a	a	k8xC	a
hrozilo	hrozit	k5eAaImAgNnS	hrozit
jeho	jeho	k3xOp3gNnSc1	jeho
zajetí	zajetí	k1gNnSc1	zajetí
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Karel	Karel	k1gMnSc1	Karel
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
převleku	převlek	k1gInSc6	převlek
za	za	k7c4	za
panoše	panoše	k1gNnSc4	panoše
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1347	[number]	k4	1347
<g/>
.	.	kIx.	.
</s>
<s>
Korunovace	korunovace	k1gFnSc1	korunovace
na	na	k7c4	na
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1347	[number]	k4	1347
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
ji	on	k3xPp3gFnSc4	on
provedl	provést	k5eAaPmAgMnS	provést
český	český	k2eAgMnSc1d1	český
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Arnošt	Arnošt	k1gMnSc1	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
korunován	korunovat	k5eAaBmNgMnS	korunovat
novou	nový	k2eAgFnSc7d1	nová
královskou	královský	k2eAgFnSc7d1	královská
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dal	dát	k5eAaPmAgMnS	dát
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
vytvořit	vytvořit	k5eAaPmF	vytvořit
a	a	k8xC	a
kterou	který	k3yIgFnSc4	který
věnoval	věnovat	k5eAaImAgMnS	věnovat
sv.	sv.	kA	sv.
Václavovi	Václav	k1gMnSc3	Václav
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgNnSc2	který
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
pouze	pouze	k6eAd1	pouze
vypůjčoval	vypůjčovat	k5eAaImAgMnS	vypůjčovat
pro	pro	k7c4	pro
korunovaci	korunovace	k1gFnSc4	korunovace
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
dal	dát	k5eAaPmAgInS	dát
sepsat	sepsat	k5eAaPmF	sepsat
Korunovační	korunovační	k2eAgInSc1d1	korunovační
řád	řád	k1gInSc1	řád
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vycházel	vycházet	k5eAaImAgMnS	vycházet
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
korunovačního	korunovační	k2eAgInSc2d1	korunovační
řádu	řád	k1gInSc2	řád
římských	římský	k2eAgMnPc2d1	římský
králů	král	k1gMnPc2	král
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
z	z	k7c2	z
francouzského	francouzský	k2eAgInSc2d1	francouzský
korunovačního	korunovační	k2eAgInSc2d1	korunovační
řádu	řád	k1gInSc2	řád
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1328	[number]	k4	1328
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
česko-bavorských	českoavorský	k2eAgFnPc6d1	česko-bavorská
hranicích	hranice	k1gFnPc6	hranice
zatím	zatím	k6eAd1	zatím
docházelo	docházet	k5eAaImAgNnS	docházet
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
k	k	k7c3	k
bojovým	bojový	k2eAgFnPc3d1	bojová
srážkám	srážka	k1gFnPc3	srážka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
císaři	císař	k1gMnSc3	císař
postavit	postavit	k5eAaPmF	postavit
vojensky	vojensky	k6eAd1	vojensky
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vojska	vojsko	k1gNnSc2	vojsko
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
vypravil	vypravit	k5eAaPmAgMnS	vypravit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
však	však	k9	však
Karla	Karel	k1gMnSc2	Karel
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1347	[number]	k4	1347
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
překvapivá	překvapivý	k2eAgFnSc1d1	překvapivá
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
náhlém	náhlý	k2eAgInSc6d1	náhlý
skonu	skon	k1gInSc6	skon
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
na	na	k7c4	na
medvědy	medvěd	k1gMnPc4	medvěd
raněn	raněn	k2eAgInSc4d1	raněn
mrtvicí	mrtvice	k1gFnSc7	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
shodou	shoda	k1gFnSc7	shoda
náhod	náhoda	k1gFnPc2	náhoda
zbavil	zbavit	k5eAaPmAgInS	zbavit
svého	svůj	k3xOyFgMnSc2	svůj
největšího	veliký	k2eAgMnSc2d3	veliký
rivala	rival	k1gMnSc2	rival
a	a	k8xC	a
původně	původně	k6eAd1	původně
vojenská	vojenský	k2eAgFnSc1d1	vojenská
výprava	výprava	k1gFnSc1	výprava
s	s	k7c7	s
nejasným	jasný	k2eNgInSc7d1	nejasný
výsledkem	výsledek	k1gInSc7	výsledek
se	se	k3xPyFc4	se
proměnila	proměnit	k5eAaPmAgFnS	proměnit
v	v	k7c4	v
triumfální	triumfální	k2eAgNnSc4d1	triumfální
tažení	tažení	k1gNnSc4	tažení
říší	říš	k1gFnPc2	říš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Karlovi	Karlův	k2eAgMnPc1d1	Karlův
postupně	postupně	k6eAd1	postupně
otevírala	otevírat	k5eAaImAgFnS	otevírat
své	své	k1gNnSc4	své
brány	brána	k1gFnSc2	brána
významná	významný	k2eAgNnPc4d1	významné
říšská	říšský	k2eAgNnPc4d1	říšské
města	město	k1gNnPc4	město
jako	jako	k8xC	jako
Řezno	Řezno	k1gNnSc4	Řezno
<g/>
,	,	kIx,	,
Norimberk	Norimberk	k1gInSc1	Norimberk
<g/>
,	,	kIx,	,
Štrasburk	Štrasburk	k1gInSc1	Štrasburk
<g/>
,	,	kIx,	,
Basilej	Basilej	k1gFnSc1	Basilej
<g/>
,	,	kIx,	,
Špýr	Špýr	k1gMnSc1	Špýr
<g/>
,	,	kIx,	,
Ulm	Ulm	k1gMnSc1	Ulm
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Přicházeli	přicházet	k5eAaImAgMnP	přicházet
však	však	k9	však
i	i	k9	i
představitelé	představitel	k1gMnPc1	představitel
menších	malý	k2eAgNnPc2d2	menší
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vzdávali	vzdávat	k5eAaImAgMnP	vzdávat
Karlovi	Karlův	k2eAgMnPc1d1	Karlův
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
tažení	tažení	k1gNnSc6	tažení
hold	hold	k1gInSc4	hold
a	a	k8xC	a
žádali	žádat	k5eAaImAgMnP	žádat
o	o	k7c4	o
potvrzení	potvrzení	k1gNnSc4	potvrzení
privilegií	privilegium	k1gNnPc2	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
si	se	k3xPyFc3	se
počínal	počínat	k5eAaImAgMnS	počínat
s	s	k7c7	s
prozíravou	prozíravý	k2eAgFnSc7d1	prozíravá
velkorysostí	velkorysost	k1gFnSc7	velkorysost
a	a	k8xC	a
všude	všude	k6eAd1	všude
proto	proto	k8xC	proto
uděloval	udělovat	k5eAaImAgMnS	udělovat
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
,	,	kIx,	,
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
milosti	milost	k1gFnPc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
říšských	říšský	k2eAgFnPc2d1	říšská
záležitostí	záležitost	k1gFnPc2	záležitost
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
zainteresován	zainteresován	k2eAgMnSc1d1	zainteresován
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Anglií	Anglie	k1gFnSc7	Anglie
později	pozdě	k6eAd2	pozdě
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
stoletá	stoletý	k2eAgFnSc1d1	stoletá
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
počínal	počínat	k5eAaImAgInS	počínat
velmi	velmi	k6eAd1	velmi
pragmaticky	pragmaticky	k6eAd1	pragmaticky
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
vyvázal	vyvázat	k5eAaPmAgInS	vyvázat
ze	z	k7c2	z
spojeneckého	spojenecký	k2eAgInSc2d1	spojenecký
svazku	svazek	k1gInSc2	svazek
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zachovávat	zachovávat	k5eAaImF	zachovávat
neutralitu	neutralita	k1gFnSc4	neutralita
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
občas	občas	k6eAd1	občas
slíbil	slíbit	k5eAaPmAgMnS	slíbit
té	ten	k3xDgFnSc6	ten
či	či	k8xC	či
oné	onen	k3xDgFnSc3	onen
straně	strana	k1gFnSc3	strana
větší	veliký	k2eAgFnSc4d2	veliký
angažovanost	angažovanost	k1gFnSc4	angažovanost
a	a	k8xC	a
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
podmínek	podmínka	k1gFnPc2	podmínka
i	i	k8xC	i
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1348	[number]	k4	1348
byl	být	k5eAaImAgInS	být
významný	významný	k2eAgInSc1d1	významný
především	především	k9	především
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Karlově	Karlův	k2eAgFnSc6d1	Karlova
už	už	k6eAd1	už
jen	jen	k9	jen
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
metropolí	metropol	k1gFnSc7	metropol
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
proto	proto	k8xC	proto
věnoval	věnovat	k5eAaImAgMnS	věnovat
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
důstojným	důstojný	k2eAgNnSc7d1	důstojné
a	a	k8xC	a
reprezentativním	reprezentativní	k2eAgNnSc7d1	reprezentativní
sídlem	sídlo	k1gNnSc7	sídlo
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1348	[number]	k4	1348
zakládací	zakládací	k2eAgFnSc4d1	zakládací
listinu	listina	k1gFnSc4	listina
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Založením	založení	k1gNnSc7	založení
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
Praha	Praha	k1gFnSc1	Praha
stala	stát	k5eAaPmAgFnS	stát
skutečným	skutečný	k2eAgNnSc7d1	skutečné
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
a	a	k8xC	a
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
obdařeno	obdařit	k5eAaPmNgNnS	obdařit
podobnými	podobný	k2eAgFnPc7d1	podobná
privilegii	privilegium	k1gNnPc7	privilegium
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgNnSc1d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
položil	položit	k5eAaPmAgMnS	položit
pak	pak	k6eAd1	pak
Karel	Karel	k1gMnSc1	Karel
základní	základní	k2eAgFnSc2d1	základní
stavební	stavební	k2eAgInSc4d1	stavební
kámen	kámen	k1gInSc4	kámen
hradeb	hradba	k1gFnPc2	hradba
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
také	také	k6eAd1	také
konal	konat	k5eAaImAgInS	konat
společný	společný	k2eAgInSc1d1	společný
generální	generální	k2eAgInSc1d1	generální
sněm	sněm	k1gInSc1	sněm
říšské	říšský	k2eAgFnSc2d1	říšská
a	a	k8xC	a
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
prvního	první	k4xOgInSc2	první
generálního	generální	k2eAgInSc2d1	generální
sněmu	sněm	k1gInSc2	sněm
bylo	být	k5eAaImAgNnS	být
státoprávní	státoprávní	k2eAgNnSc1d1	státoprávní
zakotvení	zakotvení	k1gNnSc1	zakotvení
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sněmu	sněm	k1gInSc2	sněm
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
celkem	celkem	k6eAd1	celkem
čtrnáct	čtrnáct	k4xCc4	čtrnáct
privilegií	privilegium	k1gNnPc2	privilegium
a	a	k8xC	a
listin	listina	k1gFnPc2	listina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnPc1d1	jiná
potvrzovaly	potvrzovat	k5eAaImAgFnP	potvrzovat
starší	starý	k2eAgFnPc1d2	starší
privilegia	privilegium	k1gNnPc4	privilegium
římských	římský	k2eAgMnPc2d1	římský
králů	král	k1gMnPc2	král
a	a	k8xC	a
císařů	císař	k1gMnPc2	císař
českému	český	k2eAgNnSc3d1	české
království	království	k1gNnSc3	království
a	a	k8xC	a
kladly	klást	k5eAaImAgFnP	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
postavení	postavení	k1gNnSc4	postavení
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
listinách	listina	k1gFnPc6	listina
<g/>
,	,	kIx,	,
přijatých	přijatý	k2eAgNnPc6d1	přijaté
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1348	[number]	k4	1348
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pojem	pojem	k1gInSc4	pojem
Corona	Corona	k1gFnSc1	Corona
regni	regň	k1gMnSc3	regň
Bohemiae	Bohemia	k1gFnSc2	Bohemia
neboli	neboli	k8xC	neboli
země	zem	k1gFnSc2	zem
Koruny	koruna	k1gFnPc4	koruna
české	český	k2eAgFnPc4d1	Česká
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
začleňuje	začleňovat	k5eAaImIp3nS	začleňovat
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
slezská	slezský	k2eAgNnPc1d1	Slezské
vévodství	vévodství	k1gNnPc1	vévodství
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Vratislav	Vratislava	k1gFnPc2	Vratislava
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
državy	država	k1gFnPc1	država
do	do	k7c2	do
trvalého	trvalý	k2eAgInSc2d1	trvalý
svazku	svazek	k1gInSc2	svazek
s	s	k7c7	s
českými	český	k2eAgFnPc7d1	Česká
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
listiny	listina	k1gFnPc1	listina
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
de	de	k?	de
facto	facta	k1gFnSc5	facta
novými	nový	k2eAgInPc7d1	nový
ústavními	ústavní	k2eAgInPc7d1	ústavní
zákony	zákon	k1gInPc7	zákon
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
zmíněných	zmíněný	k2eAgNnPc2d1	zmíněné
privilegií	privilegium	k1gNnPc2	privilegium
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
pak	pak	k6eAd1	pak
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
poslední	poslední	k2eAgFnSc1d1	poslední
čtrnáctá	čtrnáctý	k4xOgFnSc1	čtrnáctý
listina	listina	k1gFnSc1	listina
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgInSc7	svůj
významem	význam	k1gInSc7	význam
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
zcela	zcela	k6eAd1	zcela
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
<g/>
,	,	kIx,	,
zakládací	zakládací	k2eAgFnSc1d1	zakládací
listina	listina	k1gFnSc1	listina
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
proslulé	proslulý	k2eAgFnSc2d1	proslulá
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgFnSc4	první
univerzitu	univerzita	k1gFnSc4	univerzita
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
snažil	snažit	k5eAaImAgMnS	snažit
pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
vzdělání	vzdělání	k1gNnSc4	vzdělání
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nemuseli	muset	k5eNaImAgMnP	muset
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kteří	který	k3yRgMnPc1	který
bez	bez	k7c2	bez
přestání	přestání	k1gNnSc2	přestání
lační	lačnit	k5eAaImIp3nS	lačnit
po	po	k7c6	po
plodech	plod	k1gInPc6	plod
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
žebrati	žebrat	k5eAaImF	žebrat
o	o	k7c4	o
almužnu	almužna	k1gFnSc4	almužna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nP	aby
nacházeli	nacházet	k5eAaImAgMnP	nacházet
v	v	k7c6	v
království	království	k1gNnSc6	království
stůl	stůl	k1gInSc4	stůl
jim	on	k3xPp3gMnPc3	on
k	k	k7c3	k
hostině	hostina	k1gFnSc3	hostina
připravený	připravený	k2eAgInSc1d1	připravený
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Karla	Karel	k1gMnSc2	Karel
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
zahájena	zahájit	k5eAaPmNgFnS	zahájit
výstavba	výstavba	k1gFnSc1	výstavba
hradu	hrad	k1gInSc2	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaPmF	stát
nedobytnou	dobytný	k2eNgFnSc7d1	nedobytná
pevností	pevnost	k1gFnSc7	pevnost
střežící	střežící	k2eAgInPc1d1	střežící
říšské	říšský	k2eAgInPc1d1	říšský
a	a	k8xC	a
české	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1348	[number]	k4	1348
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgInSc7d1	významný
i	i	k9	i
pro	pro	k7c4	pro
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zřídil	zřídit	k5eAaPmAgInS	zřídit
Moravský	moravský	k2eAgInSc4d1	moravský
zemský	zemský	k2eAgInSc4d1	zemský
soud	soud	k1gInSc4	soud
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Karlův	Karlův	k2eAgInSc1d1	Karlův
velký	velký	k2eAgInSc1d1	velký
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
však	však	k9	však
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
i	i	k8xC	i
osobní	osobní	k2eAgInSc1d1	osobní
tragédií	tragédie	k1gFnSc7	tragédie
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
totiž	totiž	k9	totiž
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
32	[number]	k4	32
let	léto	k1gNnPc2	léto
skonala	skonat	k5eAaPmAgFnS	skonat
jeho	jeho	k3xOp3gFnSc1	jeho
milovaná	milovaný	k2eAgFnSc1d1	milovaná
manželka	manželka	k1gFnSc1	manželka
Blanka	Blanka	k1gFnSc1	Blanka
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
provázela	provázet	k5eAaImAgFnS	provázet
od	od	k7c2	od
jeho	jeho	k3xOp3gNnPc2	jeho
dětských	dětský	k2eAgNnPc2d1	dětské
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Bavorského	bavorský	k2eAgMnSc2d1	bavorský
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
Karel	Karel	k1gMnSc1	Karel
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
uznání	uznání	k1gNnSc2	uznání
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
musel	muset	k5eAaImAgMnS	muset
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
nadále	nadále	k6eAd1	nadále
tvrdě	tvrdě	k6eAd1	tvrdě
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Wittelsbachové	Wittelsbachové	k2eAgNnSc3d1	Wittelsbachové
popuzení	popuzení	k1gNnSc3	popuzení
ztrátou	ztráta	k1gFnSc7	ztráta
císařského	císařský	k2eAgInSc2d1	císařský
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Bavora	Bavor	k1gMnSc4	Bavor
-	-	kIx~	-
Ludvík	Ludvík	k1gMnSc1	Ludvík
Braniborský	braniborský	k2eAgMnSc1d1	braniborský
<g/>
,	,	kIx,	,
hledali	hledat	k5eAaImAgMnP	hledat
proti	proti	k7c3	proti
Karlovi	Karel	k1gMnSc3	Karel
konkurenta	konkurent	k1gMnSc4	konkurent
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
římskou	římský	k2eAgFnSc4d1	římská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úlohu	úloha	k1gFnSc4	úloha
nového	nový	k2eAgMnSc2d1	nový
římského	římský	k2eAgMnSc2d1	římský
protikrále	protikrál	k1gMnSc2	protikrál
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
získán	získán	k2eAgMnSc1d1	získán
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nepřítel	nepřítel	k1gMnSc1	nepřítel
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
však	však	k9	však
projevil	projevit	k5eAaPmAgMnS	projevit
své	svůj	k3xOyFgNnSc4	svůj
mistrné	mistrný	k2eAgNnSc4d1	mistrné
nadání	nadání	k1gNnSc4	nadání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
diplomacie	diplomacie	k1gFnSc2	diplomacie
a	a	k8xC	a
po	po	k7c6	po
poradě	porada	k1gFnSc6	porada
s	s	k7c7	s
prastrýcem	prastrýc	k1gMnSc7	prastrýc
Balduinem	Balduin	k1gMnSc7	Balduin
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
Eduardem	Eduard	k1gMnSc7	Eduard
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1348	[number]	k4	1348
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
Eduard	Eduard	k1gMnSc1	Eduard
přátelskou	přátelský	k2eAgFnSc4d1	přátelská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
jednání	jednání	k1gNnSc1	jednání
obou	dva	k4xCgMnPc2	dva
panovníků	panovník	k1gMnPc2	panovník
bylo	být	k5eAaImAgNnS	být
završeno	završit	k5eAaPmNgNnS	završit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1348	[number]	k4	1348
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
s	s	k7c7	s
Eduardem	Eduard	k1gMnSc7	Eduard
spojenectví	spojenectví	k1gNnSc2	spojenectví
<g/>
,	,	kIx,	,
nemusel	muset	k5eNaImAgInS	muset
však	však	k9	však
Eduardovi	Eduardův	k2eAgMnPc1d1	Eduardův
pomáhat	pomáhat	k5eAaImF	pomáhat
v	v	k7c6	v
případném	případný	k2eAgInSc6d1	případný
konfliktu	konflikt	k1gInSc6	konflikt
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
udržet	udržet	k5eAaPmF	udržet
přátelství	přátelství	k1gNnSc4	přátelství
i	i	k9	i
s	s	k7c7	s
francouzským	francouzský	k2eAgInSc7d1	francouzský
královským	královský	k2eAgInSc7d1	královský
dvorem	dvůr	k1gInSc7	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
římské	římský	k2eAgFnSc2d1	římská
koruny	koruna	k1gFnSc2	koruna
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
rakouského	rakouský	k2eAgMnSc2d1	rakouský
vévody	vévoda	k1gMnSc2	vévoda
Albrechta	Albrecht	k1gMnSc2	Albrecht
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1348	[number]	k4	1348
konala	konat	k5eAaImAgFnS	konat
schůzka	schůzka	k1gFnSc1	schůzka
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
a	a	k8xC	a
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Braniborského	braniborský	k2eAgMnSc2d1	braniborský
v	v	k7c6	v
Pasově	pasově	k6eAd1	pasově
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
zde	zde	k6eAd1	zde
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
usmíření	usmíření	k1gNnSc3	usmíření
<g/>
.	.	kIx.	.
</s>
<s>
Útočné	útočný	k2eAgNnSc4d1	útočné
vystupování	vystupování	k1gNnSc4	vystupování
Ludvíka	Ludvík	k1gMnSc2	Ludvík
a	a	k8xC	a
provokace	provokace	k1gFnSc2	provokace
jeho	jeho	k3xOp3gMnPc2	jeho
stoupenců	stoupenec	k1gMnPc2	stoupenec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dokonce	dokonce	k9	dokonce
strhli	strhnout	k5eAaPmAgMnP	strhnout
výsostné	výsostný	k2eAgInPc4d1	výsostný
znaky	znak	k1gInPc4	znak
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
ubytován	ubytován	k2eAgMnSc1d1	ubytován
<g/>
,	,	kIx,	,
přesvědčily	přesvědčit	k5eAaPmAgFnP	přesvědčit
uraženého	uražený	k2eAgMnSc4d1	uražený
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bouřlivé	bouřlivý	k2eAgNnSc1d1	bouřlivé
jednání	jednání	k1gNnSc4	jednání
opustil	opustit	k5eAaPmAgInS	opustit
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Wittelsbachové	Wittelsbach	k1gMnPc1	Wittelsbach
pak	pak	k6eAd1	pak
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1349	[number]	k4	1349
prosadili	prosadit	k5eAaPmAgMnP	prosadit
volbu	volba	k1gFnSc4	volba
nového	nový	k2eAgMnSc2d1	nový
římského	římský	k2eAgMnSc2d1	římský
protikrále	protikrál	k1gMnSc2	protikrál
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nevýznamný	významný	k2eNgMnSc1d1	nevýznamný
durynský	durynský	k2eAgMnSc1d1	durynský
hrabě	hrabě	k1gMnSc1	hrabě
Günther	Günthra	k1gFnPc2	Günthra
ze	z	k7c2	z
Schwarzburgu	Schwarzburg	k1gInSc2	Schwarzburg
<g/>
.	.	kIx.	.
</s>
<s>
Günthera	Günther	k1gMnSc4	Günther
podporoval	podporovat	k5eAaImAgMnS	podporovat
i	i	k9	i
rýnský	rýnský	k2eAgMnSc1d1	rýnský
falckrabě	falckrabě	k1gMnSc1	falckrabě
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Karlovi	Karel	k1gMnSc3	Karel
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
nepřátelskou	přátelský	k2eNgFnSc4d1	nepřátelská
opozici	opozice	k1gFnSc4	opozice
rozbít	rozbít	k5eAaPmF	rozbít
<g/>
,	,	kIx,	,
smluvil	smluvit	k5eAaPmAgInS	smluvit
totiž	totiž	k9	totiž
svůj	svůj	k3xOyFgInSc4	svůj
sňatek	sňatek	k1gInSc4	sňatek
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Falckou	falcký	k2eAgFnSc7d1	Falcká
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
falckraběte	falckrabě	k1gMnSc2	falckrabě
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
a	a	k8xC	a
připojil	připojit	k5eAaPmAgInS	připojit
tak	tak	k6eAd1	tak
Horní	horní	k2eAgFnSc4d1	horní
Falc	Falc	k1gFnSc4	Falc
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgFnPc1d1	Nové
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
)	)	kIx)	)
k	k	k7c3	k
zemím	zem	k1gFnPc3	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1349	[number]	k4	1349
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bacharachu	Bacharach	k1gInSc2	Bacharach
<g/>
.	.	kIx.	.
</s>
<s>
Protikrál	protikrál	k1gMnSc1	protikrál
Günther	Günthra	k1gFnPc2	Günthra
ze	z	k7c2	z
Schwarzburgu	Schwarzburg	k1gInSc2	Schwarzburg
vážně	vážně	k6eAd1	vážně
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
a	a	k8xC	a
projevil	projevit	k5eAaPmAgInS	projevit
záměr	záměr	k1gInSc4	záměr
o	o	k7c4	o
smír	smír	k1gInSc4	smír
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
započatá	započatý	k2eAgFnSc1d1	započatá
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
a	a	k8xC	a
zpečetěna	zpečetit	k5eAaPmNgFnS	zpečetit
smlouvou	smlouva	k1gFnSc7	smlouva
ze	z	k7c2	z
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1349	[number]	k4	1349
<g/>
.	.	kIx.	.
</s>
<s>
Günther	Günthra	k1gFnPc2	Günthra
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
římský	římský	k2eAgInSc4d1	římský
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
náhradou	náhrada	k1gFnSc7	náhrada
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
zástavy	zástava	k1gFnSc2	zástava
některá	některý	k3yIgNnPc1	některý
říšská	říšský	k2eAgNnPc1d1	říšské
města	město	k1gNnPc1	město
s	s	k7c7	s
důchody	důchod	k1gInPc7	důchod
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
20	[number]	k4	20
tisíc	tisíc	k4xCgInSc1	tisíc
hřiven	hřivna	k1gFnPc2	hřivna
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Günther	Günthra	k1gFnPc2	Günthra
však	však	k9	však
svou	svůj	k3xOyFgFnSc4	svůj
porážku	porážka	k1gFnSc4	porážka
dlouho	dlouho	k6eAd1	dlouho
nepřežil	přežít	k5eNaPmAgMnS	přežít
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
mu	on	k3xPp3gMnSc3	on
vzdal	vzdát	k5eAaPmAgMnS	vzdát
velkorysou	velkorysý	k2eAgFnSc4d1	velkorysá
úctu	úcta	k1gFnSc4	úcta
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
jeho	on	k3xPp3gInSc2	on
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1349	[number]	k4	1349
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konalo	konat	k5eAaImAgNnS	konat
reprezentativní	reprezentativní	k2eAgNnSc1d1	reprezentativní
shromáždění	shromáždění	k1gNnSc1	shromáždění
všech	všecek	k3xTgMnPc2	všecek
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
a	a	k8xC	a
říšských	říšský	k2eAgMnPc2d1	říšský
knížat	kníže	k1gMnPc2wR	kníže
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Karlovi	Karel	k1gMnSc3	Karel
složen	složit	k5eAaPmNgInS	složit
velkolepý	velkolepý	k2eAgInSc1d1	velkolepý
hold	hold	k1gInSc1	hold
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgInS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
nepopiratelným	popiratelný	k2eNgMnSc7d1	nepopiratelný
vládcem	vládce	k1gMnSc7	vládce
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smíření	smíření	k1gNnSc6	smíření
s	s	k7c7	s
Wittelsbachy	Wittelsbach	k1gMnPc7	Wittelsbach
dovršil	dovršit	k5eAaPmAgMnS	dovršit
Karel	Karel	k1gMnSc1	Karel
své	svůj	k3xOyFgNnSc4	svůj
vítězství	vítězství	k1gNnSc4	vítězství
korunovací	korunovace	k1gFnPc2	korunovace
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
<g/>
,	,	kIx,	,
tradičním	tradiční	k2eAgNnSc6d1	tradiční
korunovačním	korunovační	k2eAgNnSc6d1	korunovační
městě	město	k1gNnSc6	město
římských	římský	k2eAgMnPc2d1	římský
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
udávají	udávat	k5eAaImIp3nP	udávat
24	[number]	k4	24
<g/>
.	.	kIx.	.
resp.	resp.	kA	resp.
26	[number]	k4	26
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
července	červenec	k1gInSc2	červenec
1349	[number]	k4	1349
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnSc3	událost
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
mnoho	mnoho	k4c1	mnoho
předních	přední	k2eAgMnPc2d1	přední
říšských	říšský	k2eAgMnPc2d1	říšský
pánů	pan	k1gMnPc2	pan
a	a	k8xC	a
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
korunovací	korunovace	k1gFnSc7	korunovace
také	také	k9	také
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
ke	k	k7c3	k
státotvorné	státotvorný	k2eAgFnSc3d1	státotvorná
a	a	k8xC	a
sakrální	sakrální	k2eAgFnSc3d1	sakrální
tradici	tradice	k1gFnSc3	tradice
říše	říš	k1gFnSc2	říš
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnPc6	jehož
stopách	stopa	k1gFnPc6	stopa
se	se	k3xPyFc4	se
hodlal	hodlat	k5eAaImAgMnS	hodlat
ubírat	ubírat	k5eAaImF	ubírat
a	a	k8xC	a
jehož	jenž	k3xRgNnSc4	jenž
velmi	velmi	k6eAd1	velmi
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
po	po	k7c6	po
Karlově	Karlův	k2eAgFnSc6d1	Karlova
korunovaci	korunovace	k1gFnSc6	korunovace
byla	být	k5eAaImAgFnS	být
korunována	korunován	k2eAgFnSc1d1	korunována
také	také	k9	také
jeho	jeho	k3xOp3gFnSc1	jeho
těhotná	těhotný	k2eAgFnSc1d1	těhotná
manželka	manželka	k1gFnSc1	manželka
Anna	Anna	k1gFnSc1	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Korunovaci	korunovace	k1gFnSc4	korunovace
provedl	provést	k5eAaPmAgMnS	provést
Karlův	Karlův	k2eAgMnSc1d1	Karlův
prastrýc	prastrýc	k1gMnSc1	prastrýc
<g/>
,	,	kIx,	,
trevírský	trevírský	k2eAgMnSc1d1	trevírský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Balduin	Balduin	k1gMnSc1	Balduin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1348-1349	[number]	k4	1348-1349
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Evropu	Evropa	k1gFnSc4	Evropa
ničivá	ničivý	k2eAgFnSc1d1	ničivá
pandemie	pandemie	k1gFnSc1	pandemie
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
přiléhavé	přiléhavý	k2eAgNnSc4d1	přiléhavé
označení	označení	k1gNnSc4	označení
černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
hledali	hledat	k5eAaImAgMnP	hledat
původce	původce	k1gMnPc4	původce
této	tento	k3xDgFnSc2	tento
zkázy	zkáza	k1gFnSc2	zkáza
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
podle	podle	k7c2	podle
pověstí	pověst	k1gFnPc2	pověst
trávili	trávit	k5eAaImAgMnP	trávit
studny	studna	k1gFnSc2	studna
a	a	k8xC	a
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
znesvěcovali	znesvěcovat	k5eAaImAgMnP	znesvěcovat
hostie	hostie	k1gFnPc4	hostie
a	a	k8xC	a
jimž	jenž	k3xRgMnPc3	jenž
zároveň	zároveň	k6eAd1	zároveň
mnozí	mnohý	k2eAgMnPc1d1	mnohý
dlužili	dlužit	k5eAaImAgMnP	dlužit
nemalé	malý	k2eNgFnPc4d1	nemalá
částky	částka	k1gFnPc4	částka
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
ideálním	ideální	k2eAgInSc7d1	ideální
terčem	terč	k1gInSc7	terč
jejich	jejich	k3xOp3gFnSc2	jejich
zloby	zloba	k1gFnSc2	zloba
<g/>
.	.	kIx.	.
</s>
<s>
Evropou	Evropa	k1gFnSc7	Evropa
se	se	k3xPyFc4	se
šířily	šířit	k5eAaImAgInP	šířit
strašlivé	strašlivý	k2eAgInPc1d1	strašlivý
pogromy	pogrom	k1gInPc1	pogrom
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1348	[number]	k4	1348
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Augsburgu	Augsburg	k1gInSc6	Augsburg
k	k	k7c3	k
masakru	masakr	k1gInSc3	masakr
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
byli	být	k5eAaImAgMnP	být
pobiti	pobit	k2eAgMnPc1d1	pobit
židovští	židovský	k2eAgMnPc1d1	židovský
obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1349	[number]	k4	1349
pak	pak	k6eAd1	pak
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
město	město	k1gNnSc4	město
omilostnil	omilostnit	k5eAaPmAgMnS	omilostnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
požadoval	požadovat	k5eAaImAgMnS	požadovat
od	od	k7c2	od
jeho	jeho	k3xOp3gMnPc2	jeho
představitelů	představitel	k1gMnPc2	představitel
zaplacení	zaplacení	k1gNnSc2	zaplacení
židovské	židovský	k2eAgFnSc2d1	židovská
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
byli	být	k5eAaImAgMnP	být
služebníky	služebník	k1gMnPc7	služebník
komory	komora	k1gFnSc2	komora
krále	král	k1gMnPc4	král
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
mohl	moct	k5eAaImAgMnS	moct
dát	dát	k5eAaPmF	dát
do	do	k7c2	do
zástavy	zástava	k1gFnSc2	zástava
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
toto	tento	k3xDgNnSc4	tento
právo	právo	k1gNnSc4	právo
využíval	využívat	k5eAaPmAgInS	využívat
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1349	[number]	k4	1349
zastavil	zastavit	k5eAaPmAgMnS	zastavit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
frankfurtské	frankfurtský	k2eAgMnPc4d1	frankfurtský
Židy	Žid	k1gMnPc4	Žid
za	za	k7c4	za
patřičný	patřičný	k2eAgInSc4d1	patřičný
finanční	finanční	k2eAgInSc4d1	finanční
obnos	obnos	k1gInSc4	obnos
představeným	představený	k1gMnPc3	představený
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
občané	občan	k1gMnPc1	občan
města	město	k1gNnSc2	město
přišli	přijít	k5eAaPmAgMnP	přijít
vybrat	vybrat	k5eAaPmF	vybrat
svou	svůj	k3xOyFgFnSc4	svůj
zástavu	zástava	k1gFnSc4	zástava
a	a	k8xC	a
zabavit	zabavit	k5eAaPmF	zabavit
židovský	židovský	k2eAgInSc4d1	židovský
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
zapálili	zapálit	k5eAaPmAgMnP	zapálit
Židé	Žid	k1gMnPc1	Žid
raději	rád	k6eAd2	rád
své	svůj	k3xOyFgInPc4	svůj
domy	dům	k1gInPc4	dům
a	a	k8xC	a
dobrovolně	dobrovolně	k6eAd1	dobrovolně
uhořeli	uhořet	k5eAaPmAgMnP	uhořet
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
markraběti	markrabě	k1gMnSc3	markrabě
Ludvíku	Ludvík	k1gMnSc3	Ludvík
Braniborskému	braniborský	k2eAgMnSc3d1	braniborský
"	"	kIx"	"
<g/>
tři	tři	k4xCgInPc1	tři
domy	dům	k1gInPc1	dům
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
Židů	Žid	k1gMnPc2	Žid
<g/>
...	...	k?	...
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
budou	být	k5eAaImBp3nP	být
pobiti	pobít	k5eAaPmNgMnP	pobít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
tedy	tedy	k9	tedy
dovolil	dovolit	k5eAaPmAgInS	dovolit
zábor	zábor	k1gInSc1	zábor
židovského	židovský	k2eAgInSc2d1	židovský
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
chystané	chystaný	k2eAgNnSc1d1	chystané
zavraždění	zavraždění	k1gNnSc1	zavraždění
židovských	židovský	k2eAgMnPc2d1	židovský
uživatelů	uživatel	k1gMnPc2	uživatel
nehodlá	hodlat	k5eNaImIp3nS	hodlat
nijak	nijak	k6eAd1	nijak
trestat	trestat	k5eAaImF	trestat
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1349	[number]	k4	1349
Karel	Karel	k1gMnSc1	Karel
udělil	udělit	k5eAaPmAgMnS	udělit
Norimberku	Norimberk	k1gInSc3	Norimberk
povolení	povolení	k1gNnSc4	povolení
srovnat	srovnat	k5eAaPmF	srovnat
židovskou	židovský	k2eAgFnSc4d1	židovská
čtvrť	čtvrť	k1gFnSc4	čtvrť
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
založit	založit	k5eAaPmF	založit
mariánský	mariánský	k2eAgInSc4d1	mariánský
kostel	kostel	k1gInSc4	kostel
a	a	k8xC	a
tržiště	tržiště	k1gNnSc1	tržiště
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1349	[number]	k4	1349
následoval	následovat	k5eAaImAgInS	následovat
pogrom	pogrom	k1gInSc1	pogrom
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yIgInSc4	který
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
předem	předem	k6eAd1	předem
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
amnestii	amnestie	k1gFnSc4	amnestie
na	na	k7c4	na
veškeré	veškerý	k3xTgInPc4	veškerý
násilné	násilný	k2eAgInPc4d1	násilný
činy	čin	k1gInPc4	čin
proti	proti	k7c3	proti
Židům	Žid	k1gMnPc3	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
bylo	být	k5eAaImAgNnS	být
560	[number]	k4	560
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Karel	Karel	k1gMnSc1	Karel
toleroval	tolerovat	k5eAaImAgMnS	tolerovat
protižidovské	protižidovský	k2eAgInPc4d1	protižidovský
pogromy	pogrom	k1gInPc4	pogrom
v	v	k7c6	v
německých	německý	k2eAgNnPc6d1	německé
říšských	říšský	k2eAgNnPc6d1	říšské
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Koruně	koruna	k1gFnSc6	koruna
české	český	k2eAgMnPc4d1	český
Židy	Žid	k1gMnPc4	Žid
chránil	chránit	k5eAaImAgMnS	chránit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
masivní	masivní	k2eAgFnSc1d1	masivní
vlna	vlna	k1gFnSc1	vlna
židovských	židovský	k2eAgInPc2d1	židovský
pogromů	pogrom	k1gInPc2	pogrom
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
řádila	řádit	k5eAaImAgFnS	řádit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
až	až	k8xS	až
na	na	k7c4	na
ojedinělé	ojedinělý	k2eAgFnPc4d1	ojedinělá
výjimky	výjimka	k1gFnPc4	výjimka
(	(	kIx(	(
<g/>
Cheb	Cheb	k1gInSc1	Cheb
<g/>
)	)	kIx)	)
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
ochrany	ochrana	k1gFnSc2	ochrana
byly	být	k5eAaImAgFnP	být
především	především	k6eAd1	především
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
procházela	procházet	k5eAaImAgFnS	procházet
chaotickým	chaotický	k2eAgNnSc7d1	chaotické
obdobím	období	k1gNnSc7	období
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
volali	volat	k5eAaImAgMnP	volat
po	po	k7c6	po
schopném	schopný	k2eAgInSc6d1	schopný
a	a	k8xC	a
rozhodném	rozhodný	k2eAgMnSc6d1	rozhodný
panovníkovi	panovník	k1gMnSc6	panovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
toto	tento	k3xDgNnSc4	tento
říšské	říšský	k2eAgNnSc4d1	říšské
území	území	k1gNnSc4	území
stmelí	stmelit	k5eAaPmIp3nS	stmelit
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
slavný	slavný	k2eAgMnSc1d1	slavný
Francesco	Francesco	k1gMnSc1	Francesco
Petrarca	Petrarca	k1gMnSc1	Petrarca
psal	psát	k5eAaImAgMnS	psát
Karlovi	Karel	k1gMnSc3	Karel
plamenné	plamenný	k2eAgFnSc2d1	plamenná
výzvy	výzva	k1gFnSc2	výzva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezapomínal	zapomínat	k5eNaImAgMnS	zapomínat
na	na	k7c6	na
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
najde	najít	k5eAaPmIp3nS	najít
"	"	kIx"	"
<g/>
samu	sám	k3xTgFnSc4	sám
hlavu	hlava	k1gFnSc4	hlava
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
si	se	k3xPyFc3	se
několik	několik	k4yIc1	několik
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
snažil	snažit	k5eAaImAgMnS	snažit
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
v	v	k7c6	v
podobném	podobný	k2eAgMnSc6d1	podobný
duchu	duch	k1gMnSc6	duch
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
humanistický	humanistický	k2eAgMnSc1d1	humanistický
partner	partner	k1gMnSc1	partner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1350	[number]	k4	1350
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
objevil	objevit	k5eAaPmAgMnS	objevit
jiný	jiný	k2eAgMnSc1d1	jiný
humanista	humanista	k1gMnSc1	humanista
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
tribun	tribun	k1gMnSc1	tribun
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
papežem	papež	k1gMnSc7	papež
stíhaný	stíhaný	k2eAgMnSc1d1	stíhaný
"	"	kIx"	"
<g/>
kacíř	kacíř	k1gMnSc1	kacíř
<g/>
"	"	kIx"	"
Cola	cola	k1gFnSc1	cola
di	di	k?	di
Rienzo	Rienza	k1gFnSc5	Rienza
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
Karla	Karel	k1gMnSc4	Karel
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc4	čas
k	k	k7c3	k
zásahu	zásah	k1gInSc3	zásah
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1350	[number]	k4	1350
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
následujícího	následující	k2eAgInSc2d1	následující
vzpamatovával	vzpamatovávat	k5eAaImAgInS	vzpamatovávat
z	z	k7c2	z
těžkého	těžký	k2eAgInSc2d1	těžký
úrazu	úraz	k1gInSc2	úraz
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vše	všechen	k3xTgNnSc1	všechen
důkladně	důkladně	k6eAd1	důkladně
promýšlel	promýšlet	k5eAaImAgInS	promýšlet
a	a	k8xC	a
rozhodně	rozhodně	k6eAd1	rozhodně
nehodlal	hodlat	k5eNaImAgMnS	hodlat
zabřednout	zabřednout	k5eAaPmF	zabřednout
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
vleklého	vleklý	k2eAgInSc2d1	vleklý
konfliktu	konflikt	k1gInSc2	konflikt
v	v	k7c6	v
nevyzpytatelné	vyzpytatelný	k2eNgFnSc6d1	nevyzpytatelná
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
prostředím	prostředí	k1gNnSc7	prostředí
neměl	mít	k5eNaImAgInS	mít
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
úplně	úplně	k6eAd1	úplně
ideální	ideální	k2eAgFnPc1d1	ideální
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
Colu	cola	k1gFnSc4	cola
zajistit	zajistit	k5eAaPmF	zajistit
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
ho	on	k3xPp3gNnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1352	[number]	k4	1352
kurii	kurie	k1gFnSc4	kurie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Petrarcou	Petrarca	k1gFnSc7	Petrarca
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
také	také	k9	také
setkal	setkat	k5eAaPmAgMnS	setkat
osobně	osobně	k6eAd1	osobně
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1354	[number]	k4	1354
v	v	k7c6	v
Mantově	Mantova	k1gFnSc6	Mantova
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
své	svůj	k3xOyFgFnSc2	svůj
římské	římský	k2eAgFnSc2d1	římská
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
Anna	Anna	k1gFnSc1	Anna
Falcká	falcký	k2eAgFnSc1d1	Falcká
porodila	porodit	k5eAaPmAgFnS	porodit
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1350	[number]	k4	1350
Karlovi	Karel	k1gMnSc3	Karel
vytouženého	vytoužený	k2eAgMnSc4d1	vytoužený
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
svého	svůj	k3xOyFgMnSc4	svůj
mladičkého	mladičký	k2eAgMnSc4d1	mladičký
syna	syn	k1gMnSc4	syn
brzo	brzo	k6eAd1	brzo
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Svídnickou	Svídnický	k2eAgFnSc7d1	Svídnická
<g/>
,	,	kIx,	,
dědičkou	dědička	k1gFnSc7	dědička
Svídnicka	Svídnicko	k1gNnSc2	Svídnicko
a	a	k8xC	a
Javořicka	Javořicko	k1gNnSc2	Javořicko
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
slezského	slezský	k2eAgNnSc2d1	Slezské
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
stále	stále	k6eAd1	stále
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
samostatné	samostatný	k2eAgNnSc1d1	samostatné
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
Václav	Václav	k1gMnSc1	Václav
však	však	k9	však
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1351	[number]	k4	1351
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1353	[number]	k4	1353
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
následován	následován	k2eAgInSc4d1	následován
Annou	Anna	k1gFnSc7	Anna
Falckou	falcký	k2eAgFnSc7d1	Falcká
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
musel	muset	k5eAaImAgInS	muset
opět	opět	k6eAd1	opět
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
jeho	jeho	k3xOp3gFnSc7	jeho
novou	nový	k2eAgFnSc7d1	nová
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
Evropu	Evropa	k1gFnSc4	Evropa
překvapilo	překvapit	k5eAaPmAgNnS	překvapit
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Svídnickou	Svídnický	k2eAgFnSc7d1	Svídnická
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc7d1	původní
nevěstou	nevěsta	k1gFnSc7	nevěsta
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
poté	poté	k6eAd1	poté
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
zemím	zem	k1gFnPc3	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc6d1	Česká
Horní	horní	k2eAgFnSc3d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc3d1	dolní
Lužici	Lužice	k1gFnSc3	Lužice
<g/>
.	.	kIx.	.
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1	slavná
svatba	svatba	k1gFnSc1	svatba
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
Svídnické	Svídnický	k2eAgFnSc2d1	Svídnická
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1353	[number]	k4	1353
v	v	k7c6	v
Budíně	Budín	k1gInSc6	Budín
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc7d1	Česká
královnou	královna	k1gFnSc7	královna
byla	být	k5eAaImAgFnS	být
mladičká	mladičký	k2eAgFnSc1d1	mladičká
Anna	Anna	k1gFnSc1	Anna
korunována	korunovat	k5eAaBmNgFnS	korunovat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnPc1	snaha
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
o	o	k7c4	o
císařskou	císařský	k2eAgFnSc4d1	císařská
korunovaci	korunovace	k1gFnSc4	korunovace
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
zhaceny	zhatit	k5eAaPmNgInP	zhatit
papežem	papež	k1gMnSc7	papež
Klementem	Klement	k1gMnSc7	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Klementově	Klementův	k2eAgFnSc6d1	Klementova
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
zahájil	zahájit	k5eAaPmAgMnS	zahájit
proto	proto	k8xC	proto
Karel	Karel	k1gMnSc1	Karel
jednání	jednání	k1gNnSc2	jednání
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
papežem	papež	k1gMnSc7	papež
Inocencem	Inocenc	k1gMnSc7	Inocenc
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
nic	nic	k3yNnSc4	nic
zásadního	zásadní	k2eAgMnSc4d1	zásadní
nepřinesla	přinést	k5eNaPmAgFnS	přinést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1354	[number]	k4	1354
dospěl	dochvít	k5eAaPmAgMnS	dochvít
Karel	Karel	k1gMnSc1	Karel
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastala	nastat	k5eAaPmAgFnS	nastat
správná	správný	k2eAgFnSc1d1	správná
chvíle	chvíle	k1gFnSc1	chvíle
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
císařského	císařský	k2eAgInSc2d1	císařský
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
den	den	k1gInSc4	den
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
roku	rok	k1gInSc2	rok
1354	[number]	k4	1354
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
Falci	Falc	k1gFnSc6	Falc
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
rozkaz	rozkaz	k1gInSc4	rozkaz
k	k	k7c3	k
jízdě	jízda	k1gFnSc3	jízda
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
opět	opět	k6eAd1	opět
překvapil	překvapit	k5eAaPmAgInS	překvapit
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
především	především	k9	především
papeže	papež	k1gMnSc4	papež
sídlícího	sídlící	k2eAgMnSc4d1	sídlící
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
<g/>
.	.	kIx.	.
</s>
<s>
Postupoval	postupovat	k5eAaImAgMnS	postupovat
poměrně	poměrně	k6eAd1	poměrně
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
intenzivních	intenzivní	k2eAgNnPc2d1	intenzivní
vyjednávání	vyjednávání	k1gNnPc2	vyjednávání
a	a	k8xC	a
politických	politický	k2eAgFnPc2d1	politická
setkání	setkání	k1gNnSc2	setkání
ho	on	k3xPp3gMnSc4	on
zpomalovalo	zpomalovat	k5eAaImAgNnS	zpomalovat
velmi	velmi	k6eAd1	velmi
důkladné	důkladný	k2eAgNnSc1d1	důkladné
sbírání	sbírání	k1gNnSc1	sbírání
relikvií	relikvie	k1gFnPc2	relikvie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1355	[number]	k4	1355
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
korunován	korunován	k2eAgInSc1d1	korunován
lombardskou	lombardský	k2eAgFnSc7d1	Lombardská
korunou	koruna	k1gFnSc7	koruna
na	na	k7c4	na
krále	král	k1gMnSc4	král
lombardského	lombardský	k2eAgMnSc4d1	lombardský
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
pak	pak	k6eAd1	pak
přebýval	přebývat	k5eAaImAgMnS	přebývat
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
očekával	očekávat	k5eAaImAgInS	očekávat
říšské	říšský	k2eAgMnPc4d1	říšský
a	a	k8xC	a
české	český	k2eAgMnPc4d1	český
vojenské	vojenský	k2eAgMnPc4d1	vojenský
oddíly	oddíl	k1gInPc4	oddíl
a	a	k8xC	a
královnu	královna	k1gFnSc4	královna
s	s	k7c7	s
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
dorazili	dorazit	k5eAaPmAgMnP	dorazit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1355	[number]	k4	1355
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c4	před
Řím	Řím	k1gInSc4	Řím
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
4	[number]	k4	4
000	[number]	k4	000
rytířů	rytíř	k1gMnPc2	rytíř
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1355	[number]	k4	1355
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
vypravil	vypravit	k5eAaPmAgInS	vypravit
inkognito	inkognito	k6eAd1	inkognito
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
místní	místní	k2eAgFnPc4d1	místní
památky	památka	k1gFnPc4	památka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
papežovi	papež	k1gMnSc3	papež
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
věčném	věčný	k2eAgNnSc6d1	věčné
městě	město	k1gNnSc6	město
zdrží	zdržet	k5eAaPmIp3nS	zdržet
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Císařská	císařský	k2eAgFnSc1d1	císařská
korunovace	korunovace	k1gFnSc1	korunovace
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1355	[number]	k4	1355
<g/>
.	.	kIx.	.
</s>
<s>
Korunovace	korunovace	k1gFnSc1	korunovace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
ve	v	k7c6	v
Svatopetrské	svatopetrský	k2eAgFnSc6d1	Svatopetrská
bazilice	bazilika	k1gFnSc6	bazilika
a	a	k8xC	a
provedl	provést	k5eAaPmAgMnS	provést
ji	on	k3xPp3gFnSc4	on
papežský	papežský	k2eAgMnSc1d1	papežský
legát	legát	k1gMnSc1	legát
kardinál	kardinál	k1gMnSc1	kardinál
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Colombiers	Colombiersa	k1gFnPc2	Colombiersa
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
hostina	hostina	k1gFnSc1	hostina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Lateránském	lateránský	k2eAgInSc6d1	lateránský
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
zvlášť	zvlášť	k6eAd1	zvlášť
památný	památný	k2eAgInSc4d1	památný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
stal	stát	k5eAaPmAgMnS	stát
český	český	k2eAgMnSc1d1	český
panovník	panovník	k1gMnSc1	panovník
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
pak	pak	k6eAd1	pak
Karel	Karel	k1gMnSc1	Karel
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
u	u	k7c2	u
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
pořadové	pořadový	k2eAgNnSc1d1	pořadové
číslo	číslo	k1gNnSc1	číslo
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
latinsky	latinsky	k6eAd1	latinsky
Karolus	Karolus	k1gMnSc1	Karolus
Quartus	Quartus	k1gMnSc1	Quartus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
s	s	k7c7	s
císařovnou	císařovna	k1gFnSc7	císařovna
ubytovali	ubytovat	k5eAaPmAgMnP	ubytovat
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
a	a	k8xC	a
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
císařští	císařský	k2eAgMnPc1d1	císařský
manželé	manžel	k1gMnPc1	manžel
přenocovali	přenocovat	k5eAaPmAgMnP	přenocovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
podpálen	podpálen	k2eAgMnSc1d1	podpálen
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
si	se	k3xPyFc3	se
museli	muset	k5eAaImAgMnP	muset
zachránit	zachránit	k5eAaPmF	zachránit
život	život	k1gInSc4	život
útěkem	útěk	k1gInSc7	útěk
v	v	k7c6	v
nočních	noční	k2eAgInPc6d1	noční
úborech	úbor	k1gInPc6	úbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
Karel	Karel	k1gMnSc1	Karel
odboj	odboj	k1gInSc4	odboj
potlačil	potlačit	k5eAaPmAgMnS	potlačit
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
vůdců	vůdce	k1gMnPc2	vůdce
povstání	povstání	k1gNnSc2	povstání
dal	dát	k5eAaPmAgInS	dát
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
římskou	římský	k2eAgFnSc7d1	římská
jízdou	jízda	k1gFnSc7	jízda
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
pokusil	pokusit	k5eAaPmAgMnS	pokusit
sepsat	sepsat	k5eAaPmF	sepsat
české	český	k2eAgNnSc4d1	české
královské	královský	k2eAgNnSc4d1	královské
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
kodifikovat	kodifikovat	k5eAaBmF	kodifikovat
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
zákoníku	zákoník	k1gInSc6	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
vznikal	vznikat	k5eAaImAgInS	vznikat
postupně	postupně	k6eAd1	postupně
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1350	[number]	k4	1350
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1355	[number]	k4	1355
už	už	k9	už
se	se	k3xPyFc4	se
rýsovala	rýsovat	k5eAaImAgFnS	rýsovat
jeho	jeho	k3xOp3gFnSc1	jeho
téměř	téměř	k6eAd1	téměř
konečná	konečný	k2eAgFnSc1d1	konečná
podoba	podoba	k1gFnSc1	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgInS	dostat
později	pozdě	k6eAd2	pozdě
název	název	k1gInSc1	název
Maiestas	Maiestas	k1gInSc1	Maiestas
Carolina	Carolina	k1gFnSc1	Carolina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
pojmenování	pojmenování	k1gNnSc1	pojmenování
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
až	až	k9	až
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1617	[number]	k4	1617
<g/>
.	.	kIx.	.
</s>
<s>
Vycházel	vycházet	k5eAaImAgInS	vycházet
zčásti	zčásti	k6eAd1	zčásti
z	z	k7c2	z
právních	právní	k2eAgFnPc2d1	právní
představ	představa	k1gFnPc2	představa
posledních	poslední	k2eAgMnPc2d1	poslední
přemyslovských	přemyslovský	k2eAgMnPc2d1	přemyslovský
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Karel	Karel	k1gMnSc1	Karel
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
zakomponoval	zakomponovat	k5eAaPmAgMnS	zakomponovat
především	především	k9	především
vlastní	vlastní	k2eAgFnPc4d1	vlastní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
znalosti	znalost	k1gFnPc4	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sestavování	sestavování	k1gNnSc6	sestavování
mu	on	k3xPp3gMnSc3	on
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jen	jen	k9	jen
malý	malý	k2eAgInSc4d1	malý
okruh	okruh	k1gInSc4	okruh
rádců	rádce	k1gMnPc2	rádce
<g/>
.	.	kIx.	.
</s>
<s>
Zákoník	zákoník	k1gInSc1	zákoník
měl	mít	k5eAaImAgInS	mít
celkem	celkem	k6eAd1	celkem
109	[number]	k4	109
článků	článek	k1gInPc2	článek
a	a	k8xC	a
jednoznačně	jednoznačně	k6eAd1	jednoznačně
směřoval	směřovat	k5eAaImAgInS	směřovat
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
pozornost	pozornost	k1gFnSc1	pozornost
byla	být	k5eAaImAgFnS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
královským	královský	k2eAgInPc3d1	královský
statkům	statek	k1gInPc3	statek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
jejich	jejich	k3xOp3gFnSc4	jejich
rozprodání	rozprodání	k1gNnSc1	rozprodání
šlechtě	šlechta	k1gFnSc3	šlechta
či	či	k8xC	či
zcizení	zcizení	k1gNnSc3	zcizení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
dělo	dít	k5eAaBmAgNnS	dít
například	například	k6eAd1	například
za	za	k7c4	za
krále	král	k1gMnSc4	král
Jana	Jan	k1gMnSc4	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
kapitol	kapitola	k1gFnPc2	kapitola
se	se	k3xPyFc4	se
věnovalo	věnovat	k5eAaImAgNnS	věnovat
královským	královský	k2eAgInPc3d1	královský
úřadům	úřad	k1gInPc3	úřad
<g/>
,	,	kIx,	,
korunovaci	korunovace	k1gFnSc3	korunovace
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
hospodaření	hospodaření	k1gNnSc1	hospodaření
v	v	k7c6	v
královských	královský	k2eAgFnPc6d1	královská
lesích	lese	k1gFnPc6	lese
atd.	atd.	kA	atd.
Už	už	k9	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
přípravy	příprava	k1gFnSc2	příprava
narazil	narazit	k5eAaPmAgInS	narazit
zákoník	zákoník	k1gInSc1	zákoník
na	na	k7c4	na
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
odpor	odpor	k1gInSc4	odpor
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
ani	ani	k9	ani
Karel	Karel	k1gMnSc1	Karel
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
překonat	překonat	k5eAaPmF	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
neprosadí	prosadit	k5eNaPmIp3nS	prosadit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nS	aby
neztratil	ztratit	k5eNaPmAgMnS	ztratit
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedopatřením	nedopatření	k1gNnSc7	nedopatření
shořel	shořet	k5eAaPmAgMnS	shořet
v	v	k7c6	v
krbu	krb	k1gInSc6	krb
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
to	ten	k3xDgNnSc1	ten
nebyla	být	k5eNaImAgFnS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc4	několik
opisů	opis	k1gInPc2	opis
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k8xS	jako
jakási	jakýsi	k3yIgFnSc1	jakýsi
pomůcka	pomůcka	k1gFnSc1	pomůcka
či	či	k8xC	či
vodítko	vodítko	k1gNnSc1	vodítko
při	při	k7c6	při
právní	právní	k2eAgFnSc6d1	právní
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
císařskou	císařský	k2eAgFnSc7d1	císařská
korunou	koruna	k1gFnSc7	koruna
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
nyní	nyní	k6eAd1	nyní
Karel	Karel	k1gMnSc1	Karel
realizovat	realizovat	k5eAaBmF	realizovat
své	svůj	k3xOyFgInPc4	svůj
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
plány	plán	k1gInPc4	plán
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
saským	saský	k2eAgMnSc7d1	saský
vévodou	vévoda	k1gMnSc7	vévoda
Rudolfem	Rudolf	k1gMnSc7	Rudolf
a	a	k8xC	a
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
kancléře	kancléř	k1gMnSc2	kancléř
Jana	Jan	k1gMnSc2	Jan
ze	z	k7c2	z
Středy	středa	k1gFnSc2	středa
připravoval	připravovat	k5eAaImAgMnS	připravovat
císař	císař	k1gMnSc1	císař
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
úpravu	úprava	k1gFnSc4	úprava
svodu	svod	k1gInSc2	svod
právních	právní	k2eAgFnPc2d1	právní
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
řídit	řídit	k5eAaImF	řídit
volba	volba	k1gFnSc1	volba
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgInPc2	první
23	[number]	k4	23
článků	článek	k1gInPc2	článek
nového	nový	k2eAgInSc2d1	nový
zákoníku	zákoník	k1gInSc2	zákoník
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
říšským	říšský	k2eAgInSc7d1	říšský
sněmem	sněm	k1gInSc7	sněm
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1356	[number]	k4	1356
<g/>
.	.	kIx.	.
</s>
<s>
Zbývajících	zbývající	k2eAgInPc2d1	zbývající
sedm	sedm	k4xCc4	sedm
článků	článek	k1gInPc2	článek
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Metách	Mety	k1gFnPc6	Mety
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1356	[number]	k4	1356
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
vešel	vejít	k5eAaPmAgInS	vejít
později	pozdě	k6eAd2	pozdě
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k8xS	jako
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
základním	základní	k2eAgInSc7d1	základní
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
říše	říš	k1gFnSc2	říš
až	až	k9	až
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
zániku	zánik	k1gInSc2	zánik
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
stanovovala	stanovovat	k5eAaImAgFnS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
volbu	volba	k1gFnSc4	volba
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
provádí	provádět	k5eAaImIp3nS	provádět
sedm	sedm	k4xCc1	sedm
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
-	-	kIx~	-
pilířů	pilíř	k1gInPc2	pilíř
Říše	říš	k1gFnSc2	říš
<g/>
:	:	kIx,	:
arcibiskupové	arcibiskup	k1gMnPc1	arcibiskup
trevírský	trevírský	k2eAgMnSc1d1	trevírský
<g/>
,	,	kIx,	,
kolínský	kolínský	k2eAgMnSc1d1	kolínský
a	a	k8xC	a
mohučský	mohučský	k2eAgMnSc1d1	mohučský
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
falckrabě	falckrabě	k1gMnSc1	falckrabě
rýnský	rýnský	k2eAgMnSc1d1	rýnský
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
saský	saský	k2eAgMnSc1d1	saský
a	a	k8xC	a
markrabě	markrabě	k1gMnSc1	markrabě
braniborský	braniborský	k2eAgMnSc1d1	braniborský
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
vždy	vždy	k6eAd1	vždy
do	do	k7c2	do
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
sejít	sejít	k5eAaPmF	sejít
a	a	k8xC	a
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
30	[number]	k4	30
dnů	den	k1gInPc2	den
zvolit	zvolit	k5eAaPmF	zvolit
nového	nový	k2eAgNnSc2d1	nové
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nebyla	být	k5eNaImAgFnS	být
nutná	nutný	k2eAgFnSc1d1	nutná
jednomyslnost	jednomyslnost	k1gFnSc1	jednomyslnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stačila	stačit	k5eAaBmAgFnS	stačit
prostá	prostý	k2eAgFnSc1d1	prostá
většina	většina	k1gFnSc1	většina
<g/>
.	.	kIx.	.
</s>
<s>
Volebním	volební	k2eAgNnSc7d1	volební
místem	místo	k1gNnSc7	místo
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
Frankfurt	Frankfurt	k1gInSc4	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
postavení	postavení	k1gNnSc4	postavení
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
měl	mít	k5eAaImAgMnS	mít
mít	mít	k5eAaImF	mít
na	na	k7c6	na
říšských	říšský	k2eAgInPc6d1	říšský
sněmech	sněm	k1gInPc6	sněm
a	a	k8xC	a
jiných	jiný	k2eAgNnPc6d1	jiné
slavných	slavný	k2eAgNnPc6d1	slavné
shromážděních	shromáždění	k1gNnPc6	shromáždění
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
kterýmkoliv	kterýkoliv	k3yIgMnSc7	kterýkoliv
přítomným	přítomný	k2eAgMnSc7d1	přítomný
jiným	jiný	k2eAgMnSc7d1	jiný
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
např.	např.	kA	např.
kráčet	kráčet	k5eAaImF	kráčet
bezprostředně	bezprostředně	k6eAd1	bezprostředně
za	za	k7c7	za
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
mít	mít	k5eAaImF	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
uzákonila	uzákonit	k5eAaPmAgFnS	uzákonit
také	také	k9	také
další	další	k2eAgFnPc4d1	další
výsady	výsada	k1gFnPc4	výsada
pro	pro	k7c4	pro
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
a	a	k8xC	a
české	český	k2eAgNnSc4d1	české
království	království	k1gNnSc4	království
<g/>
:	:	kIx,	:
potvrzovala	potvrzovat	k5eAaImAgFnS	potvrzovat
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
volbu	volba	k1gFnSc4	volba
českého	český	k2eAgInSc2d1	český
krále	král	k1gMnPc4	král
českým	český	k2eAgInSc7d1	český
sněmem	sněm	k1gInSc7	sněm
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vymření	vymření	k1gNnSc2	vymření
panovnického	panovnický	k2eAgInSc2d1	panovnický
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
dědičnost	dědičnost	k1gFnSc1	dědičnost
po	po	k7c6	po
přeslici	přeslice	k1gFnSc6	přeslice
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
také	také	k9	také
nemohl	moct	k5eNaImAgMnS	moct
český	český	k2eAgInSc4d1	český
stát	stát	k1gInSc4	stát
udělovat	udělovat	k5eAaImF	udělovat
jako	jako	k8xC	jako
(	(	kIx(	(
<g/>
odumřelé	odumřelý	k2eAgFnSc3d1	odumřelá
<g/>
)	)	kIx)	)
léno	léno	k1gNnSc1	léno
a	a	k8xC	a
nemohl	moct	k5eNaImAgMnS	moct
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
ani	ani	k8xC	ani
jmenovat	jmenovat	k5eAaBmF	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
mělo	mít	k5eAaImAgNnS	mít
výhradní	výhradní	k2eAgNnSc1d1	výhradní
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
soudní	soudní	k2eAgFnSc4d1	soudní
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovení	ustanovení	k1gNnSc1	ustanovení
tak	tak	k8xS	tak
stavěla	stavět	k5eAaImAgFnS	stavět
české	český	k2eAgNnSc4d1	české
království	království	k1gNnSc4	království
na	na	k7c4	na
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
<g/>
,	,	kIx,	,
samostatné	samostatný	k2eAgNnSc4d1	samostatné
a	a	k8xC	a
privilegované	privilegovaný	k2eAgNnSc4d1	privilegované
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1355-1356	[number]	k4	1355-1356
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
diplomaté	diplomat	k1gMnPc1	diplomat
značné	značný	k2eAgInPc1d1	značný
úsilí	úsilí	k1gNnSc4	úsilí
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
urovnat	urovnat	k5eAaPmF	urovnat
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snaha	snaha	k1gFnSc1	snaha
nakonec	nakonec	k6eAd1	nakonec
nevedla	vést	k5eNaImAgFnS	vést
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
výsledku	výsledek	k1gInSc3	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc2	jeho
nabídky	nabídka	k1gFnSc2	nabídka
především	především	k9	především
vůči	vůči	k7c3	vůči
francouzské	francouzský	k2eAgFnSc3d1	francouzská
straně	strana	k1gFnSc3	strana
nebyly	být	k5eNaImAgFnP	být
zdaleka	zdaleka	k6eAd1	zdaleka
nezištné	zištný	k2eNgNnSc4d1	nezištné
-	-	kIx~	-
žádal	žádat	k5eAaImAgMnS	žádat
například	například	k6eAd1	například
navrácení	navrácení	k1gNnSc4	navrácení
měst	město	k1gNnPc2	město
Verdun	Verduna	k1gFnPc2	Verduna
<g/>
,	,	kIx,	,
Cambrai	Cambra	k1gFnSc2	Cambra
<g/>
,	,	kIx,	,
Château-Cambrésis	Château-Cambrésis	k1gFnSc2	Château-Cambrésis
a	a	k8xC	a
Vienne	Vienn	k1gInSc5	Vienn
a	a	k8xC	a
složení	složení	k1gNnSc3	složení
lenních	lenní	k2eAgInPc2d1	lenní
slibů	slib	k1gInPc2	slib
dalších	další	k2eAgNnPc2d1	další
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Znesvářené	znesvářený	k2eAgFnPc1d1	znesvářená
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dohodly	dohodnout	k5eAaPmAgInP	dohodnout
na	na	k7c6	na
smíru	smír	k1gInSc6	smír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1360	[number]	k4	1360
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
účasti	účast	k1gFnSc2	účast
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1358	[number]	k4	1358
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
tzv.	tzv.	kA	tzv.
druhá	druhý	k4xOgFnSc1	druhý
vogtlandská	vogtlandský	k2eAgFnSc1d1	vogtlandský
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
Wettiny	Wettin	k1gInPc7	Wettin
a	a	k8xC	a
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
musel	muset	k5eAaImAgMnS	muset
po	po	k7c6	po
relativně	relativně	k6eAd1	relativně
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
uplatnit	uplatnit	k5eAaPmF	uplatnit
své	svůj	k3xOyFgNnSc4	svůj
válečnické	válečnický	k2eAgNnSc4d1	válečnické
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
podařilo	podařit	k5eAaPmAgNnS	podařit
urovnat	urovnat	k5eAaPmF	urovnat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nějakým	nějaký	k3yIgFnPc3	nějaký
větším	veliký	k2eAgFnPc3d2	veliký
bitvám	bitva	k1gFnPc3	bitva
či	či	k8xC	či
krveprolitím	krveprolití	k1gNnSc7	krveprolití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
projevovat	projevovat	k5eAaImF	projevovat
společenská	společenský	k2eAgFnSc1d1	společenská
krize	krize	k1gFnSc1	krize
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
růstem	růst	k1gInSc7	růst
zločinnosti	zločinnost	k1gFnSc2	zločinnost
<g/>
,	,	kIx,	,
obecnou	obecný	k2eAgFnSc7d1	obecná
nejistotou	nejistota	k1gFnSc7	nejistota
<g/>
,	,	kIx,	,
úpadkem	úpadek	k1gInSc7	úpadek
mravů	mrav	k1gInPc2	mrav
a	a	k8xC	a
pohoršlivým	pohoršlivý	k2eAgInSc7d1	pohoršlivý
životem	život	k1gInSc7	život
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Proticírkevní	proticírkevní	k2eAgFnPc1d1	proticírkevní
nálady	nálada	k1gFnPc1	nálada
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
začaly	začít	k5eAaPmAgFnP	začít
šířit	šířit	k5eAaImF	šířit
i	i	k9	i
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Závažnosti	závažnost	k1gFnPc1	závažnost
problému	problém	k1gInSc2	problém
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
i	i	k9	i
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1359	[number]	k4	1359
s	s	k7c7	s
vážnou	vážný	k2eAgFnSc7d1	vážná
výzvou	výzva	k1gFnSc7	výzva
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
církevní	církevní	k2eAgMnSc1d1	církevní
hodnostáře	hodnostář	k1gMnPc4	hodnostář
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
učinili	učinit	k5eAaPmAgMnP	učinit
přítrž	přítrž	k1gFnSc4	přítrž
úpadkovému	úpadkový	k2eAgInSc3d1	úpadkový
životu	život	k1gInSc3	život
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
také	také	k9	také
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
potrestá	potrestat	k5eAaPmIp3nS	potrestat
případné	případný	k2eAgFnSc3d1	případná
provinilce	provinilka	k1gFnSc3	provinilka
zabavením	zabavení	k1gNnSc7	zabavení
jejich	jejich	k3xOp3gInSc2	jejich
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
příjmů	příjem	k1gInPc2	příjem
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
světských	světský	k2eAgMnPc2d1	světský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
tak	tak	k9	tak
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
požadavků	požadavek	k1gInPc2	požadavek
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
reformního	reformní	k2eAgNnSc2d1	reformní
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
ohradil	ohradit	k5eAaPmAgMnS	ohradit
a	a	k8xC	a
prosil	prosit	k5eAaImAgMnS	prosit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezasahoval	zasahovat	k5eNaImAgMnS	zasahovat
do	do	k7c2	do
církevních	církevní	k2eAgInPc2d1	církevní
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
si	se	k3xPyFc3	se
mezitím	mezitím	k6eAd1	mezitím
někteří	některý	k3yIgMnPc1	některý
světští	světský	k2eAgMnPc1d1	světský
panovníci	panovník	k1gMnPc1	panovník
vysvětlili	vysvětlit	k5eAaPmAgMnP	vysvětlit
císařovu	císařův	k2eAgFnSc4d1	císařova
pohrůžku	pohrůžka	k1gFnSc4	pohrůžka
jako	jako	k8xC	jako
pokyn	pokyn	k1gInSc4	pokyn
k	k	k7c3	k
zabavování	zabavování	k1gNnSc3	zabavování
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
nemalý	malý	k2eNgInSc4d1	nemalý
rozruch	rozruch	k1gInSc4	rozruch
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
proto	proto	k8xC	proto
musel	muset	k5eAaImAgMnS	muset
již	již	k6eAd1	již
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1359	[number]	k4	1359
vydat	vydat	k5eAaPmF	vydat
rozkaz	rozkaz	k1gInSc4	rozkaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
pánů	pan	k1gMnPc2	pan
zastaven	zastavit	k5eAaPmNgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
cílů	cíl	k1gInPc2	cíl
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
císařské	císařský	k2eAgFnSc2d1	císařská
politiky	politika	k1gFnSc2	politika
bylo	být	k5eAaImAgNnS	být
vymanit	vymanit	k5eAaPmF	vymanit
papeže	papež	k1gMnSc4	papež
z	z	k7c2	z
vlivu	vliv	k1gInSc2	vliv
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
všemi	všecek	k3xTgInPc7	všecek
dostupnými	dostupný	k2eAgInPc7d1	dostupný
prostředky	prostředek	k1gInPc7	prostředek
snažil	snažit	k5eAaImAgMnS	snažit
přesídlit	přesídlit	k5eAaPmF	přesídlit
papeže	papež	k1gMnSc4	papež
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
avignonského	avignonský	k2eAgNnSc2d1	avignonské
zajetí	zajetí	k1gNnSc2	zajetí
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1362	[number]	k4	1362
se	se	k3xPyFc4	se
novým	nový	k2eAgMnSc7d1	nový
papežem	papež	k1gMnSc7	papež
stal	stát	k5eAaPmAgMnS	stát
Urban	Urban	k1gMnSc1	Urban
V.	V.	kA	V.
Císař	Císař	k1gMnSc1	Císař
přijel	přijet	k5eAaPmAgMnS	přijet
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1365	[number]	k4	1365
na	na	k7c4	na
okázalou	okázalý	k2eAgFnSc4d1	okázalá
návštěvu	návštěva	k1gFnSc4	návštěva
do	do	k7c2	do
Avignonu	Avignon	k1gInSc2	Avignon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
kronikářů	kronikář	k1gMnPc2	kronikář
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
tří	tři	k4xCgNnPc2	tři
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Konala	konat	k5eAaImAgFnS	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
hostina	hostina	k1gFnSc1	hostina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
kardinálové	kardinál	k1gMnPc1	kardinál
a	a	k8xC	a
vévoda	vévoda	k1gMnSc1	vévoda
Ludvík	Ludvík	k1gMnSc1	Ludvík
z	z	k7c2	z
Anjou	Anjá	k1gFnSc4	Anjá
a	a	k8xC	a
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
rokovalo	rokovat	k5eAaImAgNnS	rokovat
o	o	k7c6	o
důležitých	důležitý	k2eAgFnPc6d1	důležitá
otázkách	otázka	k1gFnPc6	otázka
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
sem	sem	k6eAd1	sem
například	například	k6eAd1	například
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
proti	proti	k7c3	proti
Turkům	turek	k1gInPc3	turek
a	a	k8xC	a
tíživá	tíživý	k2eAgFnSc1d1	tíživá
situace	situace	k1gFnSc1	situace
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
způsobená	způsobený	k2eAgFnSc1d1	způsobená
řáděním	řádění	k1gNnSc7	řádění
žoldnéřských	žoldnéřský	k2eAgNnPc2d1	žoldnéřské
band	bando	k1gNnPc2	bando
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
císařově	císařův	k2eAgFnSc6d1	císařova
plánované	plánovaný	k2eAgFnSc6d1	plánovaná
korunovaci	korunovace	k1gFnSc6	korunovace
na	na	k7c4	na
arelatského	arelatský	k2eAgMnSc4d1	arelatský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
papež	papež	k1gMnSc1	papež
hájící	hájící	k2eAgInPc4d1	hájící
francouzské	francouzský	k2eAgInPc4d1	francouzský
zájmy	zájem	k1gInPc4	zájem
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
však	však	k9	však
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1365	[number]	k4	1365
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
v	v	k7c4	v
Arles	Arles	k1gInSc4	Arles
korunovat	korunovat	k5eAaBmF	korunovat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
tak	tak	k9	tak
prvním	první	k4xOgInSc7	první
od	od	k7c2	od
časů	čas	k1gInPc2	čas
císaře	císař	k1gMnSc2	císař
Fridricha	Fridrich	k1gMnSc2	Fridrich
I.	I.	kA	I.
Barbarossy	Barbarossa	k1gMnSc2	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
Arles	Arlesa	k1gFnPc2	Arlesa
do	do	k7c2	do
Avignonu	Avignon	k1gInSc2	Avignon
<g/>
,	,	kIx,	,
vyjednával	vyjednávat	k5eAaImAgMnS	vyjednávat
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
Urbanem	Urban	k1gMnSc7	Urban
V.	V.	kA	V.
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
papeže	papež	k1gMnSc2	papež
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
nakonec	nakonec	k6eAd1	nakonec
císařovu	císařův	k2eAgFnSc4d1	císařova
výzvu	výzva	k1gFnSc4	výzva
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Říšský	říšský	k2eAgInSc1d1	říšský
sněm	sněm	k1gInSc1	sněm
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
,	,	kIx,	,
konaný	konaný	k2eAgInSc1d1	konaný
roku	rok	k1gInSc2	rok
1366	[number]	k4	1366
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
císařově	císařův	k2eAgFnSc6d1	císařova
další	další	k2eAgFnSc6d1	další
římské	římský	k2eAgFnSc6d1	římská
jízdě	jízda	k1gFnSc6	jízda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
podpořit	podpořit	k5eAaPmF	podpořit
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
pak	pak	k8xC	pak
papež	papež	k1gMnSc1	papež
Urban	Urban	k1gMnSc1	Urban
V.	V.	kA	V.
opustil	opustit	k5eAaPmAgInS	opustit
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1367	[number]	k4	1367
Avignon	Avignon	k1gInSc4	Avignon
a	a	k8xC	a
vypravil	vypravit	k5eAaPmAgMnS	vypravit
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
papež	papež	k1gMnSc1	papež
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1367	[number]	k4	1367
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
až	až	k6eAd1	až
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1368	[number]	k4	1368
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
Itálií	Itálie	k1gFnPc2	Itálie
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
milánského	milánský	k2eAgMnSc2d1	milánský
pána	pán	k1gMnSc2	pán
Bernaba	Bernab	k1gMnSc2	Bernab
Viscontiho	Visconti	k1gMnSc2	Visconti
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
pak	pak	k6eAd1	pak
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1368	[number]	k4	1368
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1367	[number]	k4	1367
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
ve	v	k7c6	v
Viterbu	Viterb	k1gInSc6	Viterb
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vjel	vjet	k5eAaPmAgMnS	vjet
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
vojska	vojsko	k1gNnSc2	vojsko
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
40	[number]	k4	40
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vtáhl	vtáhnout	k5eAaPmAgMnS	vtáhnout
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
i	i	k9	i
papež	papež	k1gMnSc1	papež
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
jej	on	k3xPp3gMnSc4	on
symbolicky	symbolicky	k6eAd1	symbolicky
uvedl	uvést	k5eAaPmAgMnS	uvést
jako	jako	k9	jako
pán	pán	k1gMnSc1	pán
Říma	Řím	k1gInSc2	Řím
do	do	k7c2	do
svatopetrského	svatopetrský	k2eAgInSc2d1	svatopetrský
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
přijela	přijet	k5eAaPmAgFnS	přijet
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
nová	nový	k2eAgFnSc1d1	nová
manželka	manželka	k1gFnSc1	manželka
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
Pomořanská	pomořanský	k2eAgFnSc1d1	Pomořanská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
papežem	papež	k1gMnSc7	papež
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1368	[number]	k4	1368
korunována	korunovat	k5eAaBmNgFnS	korunovat
na	na	k7c4	na
císařovnu	císařovna	k1gFnSc4	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
přesídlení	přesídlení	k1gNnSc4	přesídlení
papeže	papež	k1gMnSc2	papež
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
byl	být	k5eAaImAgInS	být
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neměl	mít	k5eNaImAgInS	mít
mít	mít	k5eAaImF	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
papež	papež	k1gMnSc1	papež
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
v	v	k7c6	v
problematickém	problematický	k2eAgNnSc6d1	problematické
italském	italský	k2eAgNnSc6d1	italské
prostředí	prostředí	k1gNnSc6	prostředí
prosadit	prosadit	k5eAaPmF	prosadit
svou	svůj	k3xOyFgFnSc4	svůj
autoritu	autorita	k1gFnSc4	autorita
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1370	[number]	k4	1370
Itálii	Itálie	k1gFnSc4	Itálie
raději	rád	k6eAd2	rád
opět	opět	k6eAd1	opět
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Avignonu	Avignon	k1gInSc2	Avignon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
manželky	manželka	k1gFnSc2	manželka
Anny	Anna	k1gFnSc2	Anna
Svídnické	Svídnický	k2eAgFnSc2d1	Svídnická
připadlo	připadnout	k5eAaPmAgNnS	připadnout
českému	český	k2eAgNnSc3d1	české
království	království	k1gNnSc3	království
nejen	nejen	k6eAd1	nejen
Svídnicko	Svídnicko	k1gNnSc1	Svídnicko
a	a	k8xC	a
Javorsko	Javorsko	k1gNnSc1	Javorsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Dolní	dolní	k2eAgFnSc1d1	dolní
Lužice	Lužice	k1gFnSc1	Lužice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
připadla	připadnout	k5eAaPmAgFnS	připadnout
Karlovým	Karlův	k2eAgFnPc3d1	Karlova
a	a	k8xC	a
Anniným	Annin	k2eAgFnPc3d1	Annina
společným	společný	k2eAgFnPc3d1	společná
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
především	především	k9	především
jejich	jejich	k3xOp3gMnPc1	jejich
synovi	synův	k2eAgMnPc1d1	synův
Václavovi	Václavův	k2eAgMnPc1d1	Václavův
IV	Iva	k1gFnPc2	Iva
<g/>
..	..	k?	..
Dle	dle	k7c2	dle
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Wittelsbachy	Wittelsbach	k1gInPc7	Wittelsbach
mělo	mít	k5eAaImAgNnS	mít
Lucemburkům	Lucemburk	k1gMnPc3	Lucemburk
připadnout	připadnout	k5eAaPmF	připadnout
také	také	k9	také
Braniborsko	Braniborsko	k1gNnSc4	Braniborsko
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
braniborský	braniborský	k2eAgMnSc1d1	braniborský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
Ota	Oto	k1gMnSc2	Oto
V.	V.	kA	V.
Bavorský	bavorský	k2eAgInSc1d1	bavorský
nedočkal	dočkat	k5eNaPmAgInS	dočkat
dědiců	dědic	k1gMnPc2	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstající	vzrůstající	k2eAgFnSc1d1	vzrůstající
moc	moc	k1gFnSc1	moc
Lucemburků	Lucemburk	k1gMnPc2	Lucemburk
byla	být	k5eAaImAgFnS	být
trnem	trn	k1gInSc7	trn
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
především	především	k6eAd1	především
Karlovým	Karlův	k2eAgMnPc3d1	Karlův
sousedům	soused	k1gMnPc3	soused
polskému	polský	k2eAgInSc3d1	polský
Kazimírovi	Kazimír	k1gMnSc3	Kazimír
a	a	k8xC	a
uherskému	uherský	k2eAgMnSc3d1	uherský
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
tito	tento	k3xDgMnPc1	tento
panovníci	panovník	k1gMnPc1	panovník
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
proti	proti	k7c3	proti
Karlovi	Karel	k1gMnSc3	Karel
komplot	komplot	k1gInSc4	komplot
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
i	i	k8xC	i
Otu	Ota	k1gMnSc4	Ota
V.	V.	kA	V.
Bavorského	bavorský	k2eAgMnSc4d1	bavorský
<g/>
,	,	kIx,	,
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přidali	přidat	k5eAaPmAgMnP	přidat
i	i	k9	i
bavorští	bavorský	k2eAgMnPc1d1	bavorský
vévodové	vévoda	k1gMnPc1	vévoda
<g/>
,	,	kIx,	,
Otovi	Otův	k2eAgMnPc1d1	Otův
bratři	bratr	k1gMnPc1	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
však	však	k9	však
nepřátel	nepřítel	k1gMnPc2	nepřítel
nezalekl	zaleknout	k5eNaPmAgMnS	zaleknout
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
konflikt	konflikt	k1gInSc4	konflikt
řešit	řešit	k5eAaImF	řešit
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Šťastná	šťastný	k2eAgFnSc1d1	šťastná
souhra	souhra	k1gFnSc1	souhra
náhod	náhoda	k1gFnPc2	náhoda
však	však	k9	však
rozbila	rozbít	k5eAaPmAgFnS	rozbít
nepřátelskou	přátelský	k2eNgFnSc4d1	nepřátelská
koalici	koalice	k1gFnSc4	koalice
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
totiž	totiž	k9	totiž
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
oženil	oženit	k5eAaPmAgMnS	oženit
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Václava	Václav	k1gMnSc4	Václav
s	s	k7c7	s
Johanou	Johana	k1gFnSc7	Johana
Bavorskou	bavorský	k2eAgFnSc7d1	bavorská
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
bavorského	bavorský	k2eAgMnSc2d1	bavorský
vévody	vévoda	k1gMnSc2	vévoda
Albrechta	Albrecht	k1gMnSc2	Albrecht
I.	I.	kA	I.
Roku	rok	k1gInSc2	rok
1370	[number]	k4	1370
pak	pak	k6eAd1	pak
zemřel	zemřít	k5eAaPmAgMnS	zemřít
polský	polský	k2eAgMnSc1d1	polský
Kazimír	Kazimír	k1gMnSc1	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
Karlových	Karlův	k2eAgMnPc2d1	Karlův
rivalů	rival	k1gMnPc2	rival
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
dědické	dědický	k2eAgFnSc2d1	dědická
smlouvy	smlouva	k1gFnSc2	smlouva
braniborským	braniborský	k2eAgMnSc7d1	braniborský
markrabětem	markrabě	k1gMnSc7	markrabě
Ottou	Otta	k1gMnSc7	Otta
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1371	[number]	k4	1371
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
markraběti	markrabě	k1gMnSc6	markrabě
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
a	a	k8xC	a
vtrhl	vtrhnout	k5eAaPmAgMnS	vtrhnout
do	do	k7c2	do
Branibor	Branibory	k1gInPc2	Branibory
<g/>
,	,	kIx,	,
Braniborské	braniborský	k2eAgNnSc1d1	braniborské
markrabství	markrabství	k1gNnSc1	markrabství
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
po	po	k7c6	po
dvouletém	dvouletý	k2eAgInSc6d1	dvouletý
konfliktu	konflikt	k1gInSc6	konflikt
roku	rok	k1gInSc2	rok
1373	[number]	k4	1373
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvních	první	k4xOgInPc6	první
bojích	boj	k1gInPc6	boj
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
po	po	k7c6	po
uzavřeném	uzavřený	k2eAgNnSc6d1	uzavřené
příměří	příměří	k1gNnSc6	příměří
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zvrátit	zvrátit	k5eAaPmF	zvrátit
i	i	k9	i
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
roku	rok	k1gInSc2	rok
1372	[number]	k4	1372
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
dohodnuty	dohodnout	k5eAaPmNgFnP	dohodnout
zásnuby	zásnuba	k1gFnPc1	zásnuba
Karlova	Karlův	k2eAgMnSc2d1	Karlův
druhorozeného	druhorozený	k2eAgMnSc2d1	druhorozený
syna	syn	k1gMnSc2	syn
Zikmunda	Zikmund	k1gMnSc2	Zikmund
s	s	k7c7	s
Ludvíkovou	Ludvíkův	k2eAgFnSc7d1	Ludvíkova
dcerou	dcera	k1gFnSc7	dcera
Marií	Maria	k1gFnPc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1373	[number]	k4	1373
začaly	začít	k5eAaPmAgInP	začít
další	další	k2eAgInPc1d1	další
boje	boj	k1gInPc1	boj
o	o	k7c4	o
Braniborsko	Braniborsko	k1gNnSc4	Braniborsko
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
proti	proti	k7c3	proti
Karlovu	Karlův	k2eAgNnSc3d1	Karlovo
mocnému	mocný	k2eAgNnSc3d1	mocné
vojsku	vojsko	k1gNnSc3	vojsko
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
zhroutil	zhroutit	k5eAaPmAgMnS	zhroutit
a	a	k8xC	a
Ota	Ota	k1gMnSc1	Ota
V.	V.	kA	V.
před	před	k7c7	před
císařem	císař	k1gMnSc7	císař
kapituloval	kapitulovat	k5eAaBmAgMnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Vítězný	vítězný	k2eAgMnSc1d1	vítězný
císař	císař	k1gMnSc1	císař
požadoval	požadovat	k5eAaImAgMnS	požadovat
úplné	úplný	k2eAgNnSc4d1	úplné
odstoupení	odstoupení	k1gNnSc4	odstoupení
celého	celý	k2eAgNnSc2d1	celé
Braniborska	Braniborsko	k1gNnSc2	Braniborsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dědičně	dědičně	k6eAd1	dědičně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1373	[number]	k4	1373
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
dohodnuty	dohodnout	k5eAaPmNgFnP	dohodnout
hlavní	hlavní	k2eAgFnPc1d1	hlavní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
Wittelsbachové	Wittelsbachové	k2eAgMnPc1d1	Wittelsbachové
odevzdali	odevzdat	k5eAaPmAgMnP	odevzdat
Braniborsko	Braniborsko	k1gNnSc4	Braniborsko
císaři	císař	k1gMnSc3	císař
a	a	k8xC	a
náhradou	náhrada	k1gFnSc7	náhrada
dostali	dostat	k5eAaPmAgMnP	dostat
hotově	hotově	k6eAd1	hotově
200	[number]	k4	200
tisíc	tisíc	k4xCgInSc4	tisíc
zlatých	zlatá	k1gFnPc2	zlatá
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
dostal	dostat	k5eAaPmAgMnS	dostat
také	také	k9	také
náhradou	náhrada	k1gFnSc7	náhrada
některé	některý	k3yIgInPc4	některý
statky	statek	k1gInPc4	statek
a	a	k8xC	a
hrady	hrad	k1gInPc4	hrad
v	v	k7c6	v
Nových	Nových	k2eAgFnPc6d1	Nových
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
Braniborsko	Braniborsko	k1gNnSc4	Braniborsko
přišla	přijít	k5eAaPmAgFnS	přijít
pak	pak	k6eAd1	pak
Karla	Karla	k1gFnSc1	Karla
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
na	na	k7c4	na
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
v	v	k7c6	v
hotovosti	hotovost	k1gFnSc6	hotovost
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nemovitostech	nemovitost	k1gFnPc6	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
připojení	připojení	k1gNnSc6	připojení
Braniborska	Braniborsko	k1gNnSc2	Braniborsko
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
začal	začít	k5eAaPmAgMnS	začít
zabývat	zabývat	k5eAaImF	zabývat
intenzivní	intenzivní	k2eAgFnSc7d1	intenzivní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
prosazení	prosazení	k1gNnSc2	prosazení
svého	svůj	k3xOyFgMnSc2	svůj
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
a	a	k8xC	a
dědice	dědic	k1gMnSc2	dědic
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
milovaného	milovaný	k2eAgMnSc2d1	milovaný
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
na	na	k7c4	na
římský	římský	k2eAgInSc4d1	římský
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnPc1d1	dnešní
terminologie	terminologie	k1gFnPc1	terminologie
římsko-německý	římskoěmecký	k2eAgInSc4d1	římsko-německý
<g/>
)	)	kIx)	)
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
rozhodně	rozhodně	k6eAd1	rozhodně
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
cíl	cíl	k1gInSc1	cíl
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
musel	muset	k5eAaImAgMnS	muset
kurfiřty	kurfiřt	k1gMnPc4	kurfiřt
uplácet	uplácet	k5eAaImF	uplácet
penězi	peníze	k1gInPc7	peníze
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
privilegii	privilegium	k1gNnPc7	privilegium
a	a	k8xC	a
výhodami	výhoda	k1gFnPc7	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Václav	Václav	k1gMnSc1	Václav
musel	muset	k5eAaImAgMnS	muset
vedle	vedle	k7c2	vedle
Karla	Karel	k1gMnSc2	Karel
slibovat	slibovat	k5eAaImF	slibovat
říšským	říšský	k2eAgMnPc3d1	říšský
knížatům	kníže	k1gMnPc3wR	kníže
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
,	,	kIx,	,
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
jim	on	k3xPp3gMnPc3	on
všechna	všechen	k3xTgNnPc4	všechen
císařská	císařský	k2eAgNnPc4d1	císařské
a	a	k8xC	a
královská	královský	k2eAgNnPc4d1	královské
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Karel	Karel	k1gMnSc1	Karel
volbou	volba	k1gFnSc7	volba
syna	syn	k1gMnSc4	syn
také	také	k9	také
porušil	porušit	k5eAaPmAgInS	porušit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
bulu	bula	k1gFnSc4	bula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1376	[number]	k4	1376
nastala	nastat	k5eAaPmAgFnS	nastat
pro	pro	k7c4	pro
Karla	Karel	k1gMnSc4	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
toužebně	toužebně	k6eAd1	toužebně
očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
chvíle	chvíle	k1gFnSc1	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
korunovaný	korunovaný	k2eAgMnSc1d1	korunovaný
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
jednomyslně	jednomyslně	k6eAd1	jednomyslně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
hlasy	hlas	k1gInPc1	hlas
samotného	samotný	k2eAgMnSc2d1	samotný
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
mohučského	mohučský	k2eAgMnSc2d1	mohučský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Ludvíka	Ludvík	k1gMnSc2	Ludvík
<g/>
,	,	kIx,	,
trevírského	trevírský	k2eAgMnSc2d1	trevírský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Kuna	kuna	k1gFnSc1	kuna
<g/>
,	,	kIx,	,
kolínského	kolínský	k2eAgMnSc2d1	kolínský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Fridricha	Fridrich	k1gMnSc2	Fridrich
<g/>
,	,	kIx,	,
rýnského	rýnský	k2eAgMnSc2d1	rýnský
falckraběte	falckrabě	k1gMnSc2	falckrabě
Ruprechta	Ruprecht	k1gMnSc2	Ruprecht
Staršího	starší	k1gMnSc2	starší
<g/>
,	,	kIx,	,
braniborského	braniborský	k2eAgMnSc2d1	braniborský
markraběte	markrabě	k1gMnSc2	markrabě
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
saského	saský	k2eAgMnSc2d1	saský
vévody	vévoda	k1gMnSc2	vévoda
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1376	[number]	k4	1376
se	se	k3xPyFc4	se
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
konala	konat	k5eAaImAgFnS	konat
slavná	slavný	k2eAgFnSc1d1	slavná
korunovace	korunovace	k1gFnSc1	korunovace
Václava	Václav	k1gMnSc2	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
na	na	k7c4	na
římského	římský	k2eAgMnSc4d1	římský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byli	být	k5eAaImAgMnP	být
po	po	k7c4	po
boku	boka	k1gFnSc4	boka
korunovaného	korunovaný	k2eAgMnSc2d1	korunovaný
krále	král	k1gMnSc2	král
císař	císař	k1gMnSc1	císař
s	s	k7c7	s
císařovnou	císařovna	k1gFnSc7	císařovna
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
lesku	lesk	k1gInSc6	lesk
svého	svůj	k3xOyFgInSc2	svůj
majestátu	majestát	k1gInSc2	majestát
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
také	také	k9	také
dokonce	dokonce	k9	dokonce
vojensky	vojensky	k6eAd1	vojensky
donutil	donutit	k5eAaPmAgInS	donutit
města	město	k1gNnPc4	město
ve	v	k7c6	v
Švábsku	Švábsko	k1gNnSc6	Švábsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzdala	vzdát	k5eAaPmAgFnS	vzdát
jeho	jeho	k3xOp3gMnSc3	jeho
synovi	syn	k1gMnSc3	syn
hold	hold	k1gInSc4	hold
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
města	město	k1gNnPc4	město
Ulm	Ulm	k1gMnPc2	Ulm
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
vyplenit	vyplenit	k5eAaPmF	vyplenit
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1376	[number]	k4	1376
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
papeže	papež	k1gMnSc2	papež
Řehoře	Řehoř	k1gMnSc2	Řehoř
XI	XI	kA	XI
<g/>
.	.	kIx.	.
přes	přes	k7c4	přes
značný	značný	k2eAgInSc4d1	značný
odpor	odpor	k1gInSc4	odpor
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
papežův	papežův	k2eAgInSc1d1	papežův
konflikt	konflikt	k1gInSc1	konflikt
s	s	k7c7	s
odbojnými	odbojný	k2eAgMnPc7d1	odbojný
profrancouzskými	profrancouzský	k2eAgMnPc7d1	profrancouzský
kardinály	kardinál	k1gMnPc7	kardinál
mohl	moct	k5eAaImAgMnS	moct
přerůst	přerůst	k5eAaPmF	přerůst
v	v	k7c4	v
otevřený	otevřený	k2eAgInSc4d1	otevřený
rozkol	rozkol	k1gInSc4	rozkol
<g/>
,	,	kIx,	,
schizma	schizma	k1gNnSc4	schizma
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Karla	Karel	k1gMnSc4	Karel
trápilo	trápit	k5eAaImAgNnS	trápit
případné	případný	k2eAgNnSc1d1	případné
následnictví	následnictví	k1gNnSc1	následnictví
po	po	k7c6	po
uherském	uherský	k2eAgInSc6d1	uherský
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
také	také	k9	také
polském	polský	k2eAgMnSc6d1	polský
králi	král	k1gMnSc6	král
Ludvíkovi	Ludvík	k1gMnSc6	Ludvík
I.	I.	kA	I.
Velikém	veliký	k2eAgNnSc6d1	veliké
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
totiž	totiž	k9	totiž
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
Kateřinu	Kateřina	k1gFnSc4	Kateřina
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
princem	princ	k1gMnSc7	princ
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Orleánským	orleánský	k2eAgMnSc7d1	orleánský
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
dvůr	dvůr	k1gInSc1	dvůr
od	od	k7c2	od
zásnub	zásnuba	k1gFnPc2	zásnuba
očekával	očekávat	k5eAaImAgInS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
ujme	ujmout	k5eAaPmIp3nS	ujmout
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Neapolsku	Neapolska	k1gFnSc4	Neapolska
a	a	k8xC	a
Provenci	Provence	k1gFnSc4	Provence
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
tyto	tento	k3xDgInPc4	tento
negativní	negativní	k2eAgInPc4d1	negativní
faktory	faktor	k1gInPc4	faktor
řešit	řešit	k5eAaImF	řešit
a	a	k8xC	a
přes	přes	k7c4	přes
pokročilý	pokročilý	k2eAgInSc4d1	pokročilý
věk	věk	k1gInSc4	věk
a	a	k8xC	a
onemocnění	onemocnění	k1gNnSc1	onemocnění
dnou	dna	k1gFnSc7	dna
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
a	a	k8xC	a
nástupcem	nástupce	k1gMnSc7	nástupce
Václavem	Václav	k1gMnSc7	Václav
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
Karel	Karel	k1gMnSc1	Karel
cestoval	cestovat	k5eAaImAgMnS	cestovat
přes	přes	k7c4	přes
Cáchy	Cáchy	k1gFnPc4	Cáchy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
odpočinul	odpočinout	k5eAaPmAgMnS	odpočinout
před	před	k7c7	před
další	další	k2eAgFnSc7d1	další
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1377	[number]	k4	1377
pak	pak	k6eAd1	pak
Karel	Karel	k1gMnSc1	Karel
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Cambrai	Cambra	k1gFnSc2	Cambra
překročil	překročit	k5eAaPmAgMnS	překročit
francouzské	francouzský	k2eAgFnPc4d1	francouzská
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1377	[number]	k4	1377
vjel	vjet	k5eAaPmAgMnS	vjet
Karel	Karel	k1gMnSc1	Karel
se	s	k7c7	s
skvělým	skvělý	k2eAgInSc7d1	skvělý
a	a	k8xC	a
reprezentativním	reprezentativní	k2eAgInSc7d1	reprezentativní
průvodem	průvod	k1gInSc7	průvod
po	po	k7c6	po
boku	bok	k1gInSc6	bok
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
do	do	k7c2	do
Cambrai	Cambra	k1gFnSc2	Cambra
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
poslal	poslat	k5eAaPmAgMnS	poslat
svému	svůj	k3xOyFgMnSc3	svůj
strýci	strýc	k1gMnSc3	strýc
a	a	k8xC	a
vznešenému	vznešený	k2eAgMnSc3d1	vznešený
hostu	host	k1gMnSc3	host
naproti	naproti	k6eAd1	naproti
výkvět	výkvět	k1gInSc4	výkvět
francouzského	francouzský	k2eAgNnSc2d1	francouzské
panstva	panstvo	k1gNnSc2	panstvo
a	a	k8xC	a
rytířstva	rytířstvo	k1gNnSc2	rytířstvo
<g/>
.	.	kIx.	.
</s>
<s>
Císaře	Císař	k1gMnPc4	Císař
velmi	velmi	k6eAd1	velmi
trápila	trápit	k5eAaImAgFnS	trápit
dna	dna	k1gFnSc1	dna
a	a	k8xC	a
jízda	jízda	k1gFnSc1	jízda
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
mu	on	k3xPp3gNnSc3	on
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
bolesti	bolest	k1gFnPc4	bolest
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mu	on	k3xPp3gNnSc3	on
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
poslal	poslat	k5eAaPmAgMnS	poslat
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
osobních	osobní	k2eAgInPc2d1	osobní
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
tažený	tažený	k2eAgInSc4d1	tažený
bílými	bílý	k2eAgInPc7d1	bílý
koňmi	kůň	k1gMnPc7	kůň
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
nosítka	nosítko	k1gNnSc2	nosítko
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Saint-Denis	Saint-Denis	k1gFnSc2	Saint-Denis
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
tenkrát	tenkrát	k6eAd1	tenkrát
ještě	ještě	k9	ještě
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Zastavil	zastavit	k5eAaPmAgInS	zastavit
se	se	k3xPyFc4	se
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
si	se	k3xPyFc3	se
pamatoval	pamatovat	k5eAaImAgMnS	pamatovat
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Sličný	sličný	k2eAgInSc1d1	sličný
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Marie	Maria	k1gFnSc2	Maria
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
Karlovou	Karlův	k2eAgFnSc7d1	Karlova
tetou	teta	k1gFnSc7	teta
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Évreux	Évreux	k1gInSc4	Évreux
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
celý	celý	k2eAgInSc1d1	celý
průvod	průvod	k1gInSc1	průvod
za	za	k7c2	za
mimořádných	mimořádný	k2eAgNnPc2d1	mimořádné
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
unavený	unavený	k2eAgMnSc1d1	unavený
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
slavnostní	slavnostní	k2eAgFnSc2d1	slavnostní
večeře	večeře	k1gFnSc2	večeře
účastnil	účastnit	k5eAaImAgMnS	účastnit
až	až	k9	až
třetí	třetí	k4xOgInSc4	třetí
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
zhlédl	zhlédnout	k5eAaPmAgMnS	zhlédnout
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
velkolepé	velkolepý	k2eAgNnSc1d1	velkolepé
divadelní	divadelní	k2eAgNnSc1d1	divadelní
představení	představení	k1gNnSc1	představení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
scénickou	scénický	k2eAgFnSc4d1	scénická
podobu	podoba	k1gFnSc4	podoba
události	událost	k1gFnSc2	událost
z	z	k7c2	z
konce	konec	k1gInSc2	konec
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Godefroy	Godefroy	k1gInPc1	Godefroy
z	z	k7c2	z
Bouillonu	Bouillon	k1gInSc2	Bouillon
dobyl	dobýt	k5eAaPmAgInS	dobýt
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Hovořil	hovořit	k5eAaImAgMnS	hovořit
zde	zde	k6eAd1	zde
s	s	k7c7	s
králem	král	k1gMnSc7	král
Karlem	Karel	k1gMnSc7	Karel
V.	V.	kA	V.
o	o	k7c6	o
rozdělení	rozdělení	k1gNnSc6	rozdělení
sfér	sféra	k1gFnPc2	sféra
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
o	o	k7c6	o
přesídlení	přesídlení	k1gNnSc6	přesídlení
papeže	papež	k1gMnSc2	papež
z	z	k7c2	z
Avignonu	Avignon	k1gInSc2	Avignon
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
polská	polský	k2eAgFnSc1d1	polská
koruna	koruna	k1gFnSc1	koruna
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
připadnout	připadnout	k5eAaPmF	připadnout
Zikmundovi	Zikmundův	k2eAgMnPc1d1	Zikmundův
Lucemburskému	lucemburský	k2eAgInSc3d1	lucemburský
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
ovšem	ovšem	k9	ovšem
nakonec	nakonec	k6eAd1	nakonec
získal	získat	k5eAaPmAgMnS	získat
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odpluli	odplout	k5eAaPmAgMnP	odplout
lodí	loď	k1gFnSc7	loď
do	do	k7c2	do
Saint-Pole	Saint-Pole	k1gFnSc2	Saint-Pole
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
královen	královna	k1gFnPc2	královna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
slavnosti	slavnost	k1gFnPc4	slavnost
účastnilo	účastnit	k5eAaImAgNnS	účastnit
mnoho	mnoho	k4c1	mnoho
pánů	pan	k1gMnPc2	pan
<g/>
,	,	kIx,	,
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
urozených	urozený	k2eAgMnPc2d1	urozený
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
vidět	vidět	k5eAaImF	vidět
vévodkyni	vévodkyně	k1gFnSc4	vévodkyně
z	z	k7c2	z
Bourbonu	bourbon	k1gInSc2	bourbon
<g/>
,	,	kIx,	,
Izabelu	Izabela	k1gFnSc4	Izabela
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
švagrovou	švagrův	k2eAgFnSc7d1	švagrova
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
zemřelé	zemřelý	k2eAgFnSc6d1	zemřelá
první	první	k4xOgFnSc6	první
ženě	žena	k1gFnSc6	žena
Blance	Blanka	k1gFnSc6	Blanka
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
žijící	žijící	k2eAgFnSc7d1	žijící
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
uplynula	uplynout	k5eAaPmAgFnS	uplynout
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
doba	doba	k1gFnSc1	doba
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
ji	on	k3xPp3gFnSc4	on
téměř	téměř	k6eAd1	téměř
nepoznal	poznat	k5eNaPmAgInS	poznat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
následovala	následovat	k5eAaImAgFnS	následovat
cesta	cesta	k1gFnSc1	cesta
domů	dům	k1gInPc2	dům
přes	přes	k7c4	přes
Lucemburk	Lucemburk	k1gInSc4	Lucemburk
a	a	k8xC	a
Norimberk	Norimberk	k1gInSc4	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1378	[number]	k4	1378
byl	být	k5eAaImAgInS	být
zpět	zpět	k6eAd1	zpět
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
čeho	co	k3yQnSc2	co
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
obával	obávat	k5eAaImAgMnS	obávat
-	-	kIx~	-
papežskému	papežský	k2eAgNnSc3d1	papežské
schizmatu	schizma	k1gNnSc3	schizma
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
dva	dva	k4xCgMnPc1	dva
papežové	papež	k1gMnPc1	papež
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
nazývali	nazývat	k5eAaImAgMnP	nazývat
kacíři	kacíř	k1gMnPc1	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
již	již	k9	již
neměl	mít	k5eNaImAgInS	mít
čas	čas	k1gInSc1	čas
schizma	schizma	k1gNnSc4	schizma
zvrátit	zvrátit	k5eAaPmF	zvrátit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
mělo	mít	k5eAaImAgNnS	mít
negativně	negativně	k6eAd1	negativně
projevit	projevit	k5eAaPmF	projevit
především	především	k9	především
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
nařízení	nařízení	k1gNnSc4	nařízení
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1378	[number]	k4	1378
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
české	český	k2eAgFnPc4d1	Česká
mince	mince	k1gFnPc4	mince
<g/>
;	;	kIx,	;
i	i	k9	i
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
panování	panování	k1gNnSc4	panování
byla	být	k5eAaImAgFnS	být
měna	měna	k1gFnSc1	měna
inflací	inflace	k1gFnPc2	inflace
velmi	velmi	k6eAd1	velmi
znehodnocena	znehodnotit	k5eAaPmNgFnS	znehodnotit
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
udělal	udělat	k5eAaPmAgMnS	udělat
opatření	opatření	k1gNnSc4	opatření
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
ochraně	ochrana	k1gFnSc3	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Karla	Karla	k1gFnSc1	Karla
trápila	trápit	k5eAaImAgFnS	trápit
ve	v	k7c6	v
vyšším	vysoký	k2eAgInSc6d2	vyšší
věku	věk	k1gInSc6	věk
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ztěžovala	ztěžovat	k5eAaImAgFnS	ztěžovat
jeho	on	k3xPp3gInSc4	on
pohyb	pohyb	k1gInSc4	pohyb
a	a	k8xC	a
také	také	k9	také
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
pádu	pád	k1gInSc2	pád
(	(	kIx(	(
<g/>
nejspíše	nejspíše	k9	nejspíše
z	z	k7c2	z
koně	kůň	k1gMnSc2	kůň
či	či	k8xC	či
ze	z	k7c2	z
schodů	schod	k1gInPc2	schod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zlomenina	zlomenina	k1gFnSc1	zlomenina
krčku	krček	k1gInSc2	krček
stehenní	stehenní	k2eAgFnSc2d1	stehenní
kosti	kost	k1gFnSc2	kost
ho	on	k3xPp3gMnSc4	on
upoutala	upoutat	k5eAaPmAgFnS	upoutat
na	na	k7c4	na
lůžko	lůžko	k1gNnSc4	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1378	[number]	k4	1378
skonal	skonat	k5eAaPmAgMnS	skonat
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
tři	tři	k4xCgFnPc1	tři
hodiny	hodina	k1gFnPc1	hodina
po	po	k7c6	po
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
byl	být	k5eAaImAgInS	být
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Císařova	Císařův	k2eAgMnSc2d1	Císařův
mrtvého	mrtvý	k1gMnSc2	mrtvý
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
balzamovači	balzamovač	k1gMnPc1	balzamovač
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
síni	síň	k1gFnSc6	síň
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
vystaven	vystavit	k5eAaPmNgInS	vystavit
jedenáct	jedenáct	k4xCc1	jedenáct
dní	den	k1gInPc2	den
na	na	k7c6	na
katafalku	katafalk	k1gInSc6	katafalk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
dny	den	k1gInPc4	den
byl	být	k5eAaImAgInS	být
přemísťován	přemísťovat	k5eAaImNgMnS	přemísťovat
po	po	k7c6	po
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
mohlo	moct	k5eAaImAgNnS	moct
uctít	uctít	k5eAaPmF	uctít
co	co	k9	co
největší	veliký	k2eAgNnSc4d3	veliký
množství	množství	k1gNnSc4	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pokaždé	pokaždé	k6eAd1	pokaždé
byl	být	k5eAaImAgInS	být
vypravován	vypravován	k2eAgInSc1d1	vypravován
velkolepý	velkolepý	k2eAgInSc1d1	velkolepý
průvod	průvod	k1gInSc1	průvod
<g/>
.	.	kIx.	.
</s>
<s>
Máry	Máry	k1gFnSc4	Máry
neslo	nést	k5eAaImAgNnS	nést
celkem	celkem	k6eAd1	celkem
30	[number]	k4	30
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Přihlížející	přihlížející	k2eAgMnSc1d1	přihlížející
kronikář	kronikář	k1gMnSc1	kronikář
odhadl	odhadnout	k5eAaPmAgMnS	odhadnout
průvod	průvod	k1gInSc4	průvod
na	na	k7c4	na
7000	[number]	k4	7000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
průvod	průvod	k1gInSc1	průvod
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
délky	délka	k1gFnPc4	délka
ze	z	k7c2	z
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
až	až	k8xS	až
na	na	k7c4	na
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohřební	pohřební	k2eAgFnSc6d1	pohřební
řeči	řeč	k1gFnSc6	řeč
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Jana	Jan	k1gMnSc2	Jan
Očka	očko	k1gNnSc2	očko
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
poprvé	poprvé	k6eAd1	poprvé
nazván	nazvat	k5eAaPmNgMnS	nazvat
Otcem	otec	k1gMnSc7	otec
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
ho	on	k3xPp3gInSc4	on
nazval	nazvat	k5eAaBmAgMnS	nazvat
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Raňkův	Raňkův	k2eAgMnSc1d1	Raňkův
z	z	k7c2	z
Ježova	Ježův	k2eAgInSc2d1	Ježův
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
sarkofágu	sarkofág	k1gInSc6	sarkofág
uprostřed	uprostřed	k7c2	uprostřed
královské	královský	k2eAgFnSc2d1	královská
krypty	krypta	k1gFnSc2	krypta
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
hrobka	hrobka	k1gFnSc1	hrobka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
skutečně	skutečně	k6eAd1	skutečně
založil	založit	k5eAaPmAgMnS	založit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
dnešním	dnešní	k2eAgInSc7d1	dnešní
oltářem	oltář	k1gInSc7	oltář
a	a	k8xC	a
oltářním	oltářní	k2eAgInSc7d1	oltářní
stolem	stol	k1gInSc7	stol
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
National	National	k1gMnPc2	National
Geographic	Geographice	k1gFnPc2	Geographice
léto	léto	k1gNnSc1	léto
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Průzkum	průzkum	k1gInSc1	průzkum
hrobky	hrobka	k1gFnSc2	hrobka
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1	Karlův
naštěstí	naštěstí	k6eAd1	naštěstí
dobře	dobře	k6eAd1	dobře
zachovalé	zachovalý	k2eAgInPc4d1	zachovalý
kosterní	kosterní	k2eAgInPc4d1	kosterní
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vlček	Vlček	k1gMnSc1	Vlček
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Ramba	Ramba	k1gMnSc1	Ramba
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
173	[number]	k4	173
cm	cm	kA	cm
vysoký	vysoký	k2eAgMnSc1d1	vysoký
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
postava	postava	k1gFnSc1	postava
připomínala	připomínat	k5eAaImAgFnS	připomínat
dnešního	dnešní	k2eAgNnSc2d1	dnešní
dobře	dobře	k6eAd1	dobře
stavěného	stavěný	k2eAgMnSc4d1	stavěný
atleta	atlet	k1gMnSc4	atlet
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
podoba	podoba	k1gFnSc1	podoba
s	s	k7c7	s
vousy	vous	k1gInPc7	vous
<g/>
,	,	kIx,	,
delšími	dlouhý	k2eAgInPc7d2	delší
vlasy	vlas	k1gInPc7	vlas
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
ustupujícím	ustupující	k2eAgNnSc7d1	ustupující
čelem	čelo	k1gNnSc7	čelo
zřejmě	zřejmě	k6eAd1	zřejmě
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobové	dobový	k2eAgInPc4d1	dobový
portréty	portrét	k1gInPc4	portrét
nám	my	k3xPp1nPc3	my
nic	nic	k3yNnSc1	nic
neříkají	říkat	k5eNaImIp3nP	říkat
o	o	k7c6	o
drsném	drsný	k2eAgInSc6d1	drsný
způsobu	způsob	k1gInSc6	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Karel	Karel	k1gMnSc1	Karel
vedl	vést	k5eAaImAgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc4d1	časté
cvičení	cvičení	k1gNnSc4	cvičení
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
či	či	k8xC	či
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
typická	typický	k2eAgFnSc1d1	typická
strava	strava	k1gFnSc1	strava
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
zanechaly	zanechat	k5eAaPmAgInP	zanechat
své	svůj	k3xOyFgFnPc4	svůj
stopy	stopa	k1gFnPc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
jako	jako	k9	jako
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
utržil	utržit	k5eAaPmAgMnS	utržit
první	první	k4xOgInPc4	první
válečné	válečný	k2eAgInPc4d1	válečný
šrámy	šrám	k1gInPc4	šrám
a	a	k8xC	a
ty	ty	k3xPp2nSc1	ty
s	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
věkem	věk	k1gInSc7	věk
přibývaly	přibývat	k5eAaImAgFnP	přibývat
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
četná	četný	k2eAgNnPc4d1	četné
poranění	poranění	k1gNnPc4	poranění
v	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
nejvýrazněji	výrazně	k6eAd3	výrazně
projevovala	projevovat	k5eAaImAgFnS	projevovat
sečná	sečný	k2eAgFnSc1d1	sečná
rána	rána	k1gFnSc1	rána
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
nosu	nos	k1gInSc2	nos
poblíž	poblíž	k7c2	poblíž
pravého	pravý	k2eAgNnSc2d1	pravé
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jeho	jeho	k3xOp3gInPc1	jeho
údy	úd	k1gInPc1	úd
nesly	nést	k5eAaImAgInP	nést
stopy	stop	k1gInPc4	stop
mnoha	mnoho	k4c2	mnoho
zhojených	zhojený	k2eAgFnPc2d1	zhojená
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
například	například	k6eAd1	například
o	o	k7c4	o
zlomeniny	zlomenina	k1gFnPc4	zlomenina
loketní	loketní	k2eAgFnPc4d1	loketní
a	a	k8xC	a
lýtkové	lýtkový	k2eAgFnPc4d1	lýtková
kosti	kost	k1gFnPc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Nejmasivnější	masivní	k2eAgNnSc4d3	nejmasivnější
zranění	zranění	k1gNnSc4	zranění
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1350	[number]	k4	1350
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
sražen	srazit	k5eAaPmNgMnS	srazit
svým	svůj	k3xOyFgMnSc7	svůj
protivníkem	protivník	k1gMnSc7	protivník
z	z	k7c2	z
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
těžce	těžce	k6eAd1	těžce
si	se	k3xPyFc3	se
poranil	poranit	k5eAaPmAgMnS	poranit
čelist	čelist	k1gFnSc4	čelist
a	a	k8xC	a
krční	krční	k2eAgFnSc4d1	krční
páteř	páteř	k1gFnSc4	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
na	na	k7c4	na
čas	čas	k1gInSc4	čas
ochrnul	ochrnout	k5eAaPmAgMnS	ochrnout
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mnoho	mnoho	k4c4	mnoho
týdnů	týden	k1gInPc2	týden
upoután	upoután	k2eAgInSc4d1	upoután
na	na	k7c6	na
lůžko	lůžko	k1gNnSc4	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zotavil	zotavit	k5eAaPmAgMnS	zotavit
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
zanechala	zanechat	k5eAaPmAgFnS	zanechat
stopy	stop	k1gInPc4	stop
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
se	se	k3xPyFc4	se
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
a	a	k8xC	a
nahrbila	nahrbit	k5eAaBmAgFnS	nahrbit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
beder	bedra	k1gNnPc2	bedra
a	a	k8xC	a
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hlavou	hlava	k1gFnSc7	hlava
mohl	moct	k5eAaImAgMnS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
jen	jen	k9	jen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
otáčel	otáčet	k5eAaImAgInS	otáčet
celým	celý	k2eAgNnSc7d1	celé
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
velmi	velmi	k6eAd1	velmi
sužován	sužovat	k5eAaImNgMnS	sužovat
typickou	typický	k2eAgFnSc7d1	typická
chorobou	choroba	k1gFnSc7	choroba
bohatších	bohatý	k2eAgFnPc2d2	bohatší
vrstev	vrstva	k1gFnPc2	vrstva
-	-	kIx~	-
dnou	dna	k1gFnSc7	dna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gMnSc4	on
velmi	velmi	k6eAd1	velmi
trápila	trápit	k5eAaImAgNnP	trápit
a	a	k8xC	a
často	často	k6eAd1	často
nemohl	moct	k5eNaImAgMnS	moct
kvůli	kvůli	k7c3	kvůli
jejím	její	k3xOp3gInPc3	její
projevům	projev	k1gInPc3	projev
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
chodit	chodit	k5eAaImF	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
anatomická	anatomický	k2eAgFnSc1d1	anatomická
zkoumání	zkoumání	k1gNnSc3	zkoumání
jeho	jeho	k3xOp3gInPc2	jeho
ostatků	ostatek	k1gInPc2	ostatek
ukázala	ukázat	k5eAaPmAgFnS	ukázat
například	například	k6eAd1	například
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karel	Karel	k1gMnSc1	Karel
měl	mít	k5eAaImAgMnS	mít
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
všechny	všechen	k3xTgInPc4	všechen
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
malý	malý	k2eAgInSc4d1	malý
zubní	zubní	k2eAgInSc4d1	zubní
kaz	kaz	k1gInSc4	kaz
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
v	v	k7c6	v
sobě	se	k3xPyFc3	se
spojoval	spojovat	k5eAaImAgMnS	spojovat
několik	několik	k4yIc4	několik
zdánlivě	zdánlivě	k6eAd1	zdánlivě
protichůdných	protichůdný	k2eAgFnPc2d1	protichůdná
charakterových	charakterový	k2eAgFnPc2d1	charakterová
vlastností	vlastnost	k1gFnPc2	vlastnost
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
zbožný	zbožný	k2eAgMnSc1d1	zbožný
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
rytíř	rytíř	k1gMnSc1	rytíř
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
chladný	chladný	k2eAgInSc1d1	chladný
a	a	k8xC	a
pragmatický	pragmatický	k2eAgMnSc1d1	pragmatický
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
kalkulátor	kalkulátor	k1gMnSc1	kalkulátor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
neštítil	štítit	k5eNaImAgMnS	štítit
i	i	k9	i
poněkud	poněkud	k6eAd1	poněkud
nečistých	čistý	k2eNgFnPc2d1	nečistá
metod	metoda	k1gFnPc2	metoda
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nepochybně	pochybně	k6eNd1	pochybně
velmi	velmi	k6eAd1	velmi
nadaný	nadaný	k2eAgMnSc1d1	nadaný
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
i	i	k9	i
jeho	jeho	k3xOp3gFnPc4	jeho
jazykové	jazykový	k2eAgFnPc4d1	jazyková
schopnosti	schopnost	k1gFnPc4	schopnost
-	-	kIx~	-
plynně	plynně	k6eAd1	plynně
se	se	k3xPyFc4	se
domluvil	domluvit	k5eAaPmAgInS	domluvit
pěti	pět	k4xCc7	pět
jazyky	jazyk	k1gInPc7	jazyk
(	(	kIx(	(
<g/>
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
italština	italština	k1gFnSc1	italština
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
diplomatických	diplomatický	k2eAgNnPc6d1	diplomatické
jednáních	jednání	k1gNnPc6	jednání
toho	ten	k3xDgInSc2	ten
dokázal	dokázat	k5eAaPmAgInS	dokázat
obratně	obratně	k6eAd1	obratně
využívat	využívat	k5eAaImF	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zbožnost	zbožnost	k1gFnSc1	zbožnost
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
například	například	k6eAd1	například
sbíráním	sbírání	k1gNnSc7	sbírání
relikvií	relikvie	k1gFnPc2	relikvie
a	a	k8xC	a
častými	častý	k2eAgFnPc7d1	častá
meditacemi	meditace	k1gFnPc7	meditace
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
kronikáři	kronikář	k1gMnPc7	kronikář
občas	občas	k6eAd1	občas
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
lstivost	lstivost	k1gFnSc1	lstivost
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Lstivý	lstivý	k2eAgMnSc1d1	lstivý
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
přídomků	přídomek	k1gInPc2	přídomek
<g/>
)	)	kIx)	)
spočívala	spočívat	k5eAaImAgFnS	spočívat
mj.	mj.	kA	mj.
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
hodilo	hodit	k5eAaImAgNnS	hodit
<g/>
,	,	kIx,	,
nedodržoval	dodržovat	k5eNaImAgMnS	dodržovat
sliby	slib	k1gInPc4	slib
ani	ani	k8xC	ani
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
spojence	spojenec	k1gMnSc4	spojenec
měnil	měnit	k5eAaImAgMnS	měnit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
mohl	moct	k5eAaImAgInS	moct
mít	mít	k5eAaImF	mít
užitek	užitek	k1gInSc4	užitek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
nejenže	nejenže	k6eAd1	nejenže
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
neobvyklého	obvyklý	k2eNgMnSc4d1	neobvyklý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
politikaření	politikařený	k2eAgMnPc1d1	politikařený
<g/>
,	,	kIx,	,
pragmatismus	pragmatismus	k1gInSc1	pragmatismus
<g/>
,	,	kIx,	,
lstivost	lstivost	k1gFnSc1	lstivost
a	a	k8xC	a
chladný	chladný	k2eAgInSc1d1	chladný
kalkul	kalkul	k1gInSc1	kalkul
byly	být	k5eAaImAgFnP	být
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nichž	jenž	k3xRgFnPc2	jenž
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
panovník	panovník	k1gMnSc1	panovník
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
po	po	k7c6	po
Janovi	Jan	k1gMnSc6	Jan
zřejmě	zřejmě	k6eAd1	zřejmě
zdědil	zdědit	k5eAaPmAgInS	zdědit
impulzivnost	impulzivnost	k1gFnSc4	impulzivnost
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
jí	jíst	k5eAaImIp3nS	jíst
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
tlumit	tlumit	k5eAaImF	tlumit
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
výbuchy	výbuch	k1gInPc1	výbuch
hněvu	hněv	k1gInSc2	hněv
nebyly	být	k5eNaImAgInP	být
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k6eAd1	tak
časté	častý	k2eAgFnPc1d1	častá
jako	jako	k8xS	jako
u	u	k7c2	u
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
u	u	k7c2	u
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
postupovat	postupovat	k5eAaImF	postupovat
maximálně	maximálně	k6eAd1	maximálně
uvážlivě	uvážlivě	k6eAd1	uvážlivě
a	a	k8xC	a
kroky	krok	k1gInPc1	krok
si	se	k3xPyFc3	se
dobře	dobře	k6eAd1	dobře
promýšlel	promýšlet	k5eAaImAgMnS	promýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Uměl	umět	k5eAaImAgMnS	umět
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
improvizovat	improvizovat	k5eAaImF	improvizovat
a	a	k8xC	a
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
pohotový	pohotový	k2eAgMnSc1d1	pohotový
a	a	k8xC	a
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc4	ten
situace	situace	k1gFnSc1	situace
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
jak	jak	k6eAd1	jak
dobře	dobře	k6eAd1	dobře
spravovat	spravovat	k5eAaImF	spravovat
velký	velký	k2eAgInSc4d1	velký
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
téměř	téměř	k6eAd1	téměř
neustále	neustále	k6eAd1	neustále
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
koně	kůň	k1gMnSc4	kůň
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kočáru	kočár	k1gInSc6	kočár
a	a	k8xC	a
zúčastňovat	zúčastňovat	k5eAaImF	zúčastňovat
se	se	k3xPyFc4	se
různých	různý	k2eAgFnPc2d1	různá
jednání	jednání	k1gNnPc2	jednání
<g/>
,	,	kIx,	,
porad	porada	k1gFnPc2	porada
<g/>
,	,	kIx,	,
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
návštěv	návštěva	k1gFnPc2	návštěva
<g/>
,	,	kIx,	,
hostin	hostina	k1gFnPc2	hostina
<g/>
,	,	kIx,	,
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
slavností	slavnost	k1gFnPc2	slavnost
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
válečných	válečný	k2eAgNnPc2d1	válečné
tažení	tažení	k1gNnPc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
neúnavný	únavný	k2eNgMnSc1d1	neúnavný
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mnohokrát	mnohokrát	k6eAd1	mnohokrát
projezdil	projezdit	k5eAaPmAgMnS	projezdit
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc4	všechen
země	zem	k1gFnPc4	zem
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
zakladatel	zakladatel	k1gMnSc1	zakladatel
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nesla	nést	k5eAaImAgFnS	nést
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgInPc4	ten
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
(	(	kIx(	(
<g/>
1348	[number]	k4	1348
<g/>
)	)	kIx)	)
Tepenec	Tepenec	k1gInSc1	Tepenec
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Karlsburg	Karlsburg	k1gInSc1	Karlsburg
<g/>
,	,	kIx,	,
na	na	k7c6	na
Olomoucku	Olomoucko	k1gNnSc6	Olomoucko
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Jívová	jívový	k2eAgFnSc1d1	Jívová
Radyně	Radyně	k1gFnSc1	Radyně
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Karlskrone	Karlskron	k1gInSc5	Karlskron
<g/>
,	,	kIx,	,
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
Kašperk	Kašperk	k1gInSc1	Kašperk
(	(	kIx(	(
<g/>
1356	[number]	k4	1356
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Karlsberg	Karlsberg	k1gInSc1	Karlsberg
<g/>
,	,	kIx,	,
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
Hrádek	hrádek	k1gInSc1	hrádek
u	u	k7c2	u
Purkarce	Purkarka	k1gFnSc3	Purkarka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Karlshaus	Karlshaus	k1gInSc1	Karlshaus
<g/>
,	,	kIx,	,
na	na	k7c6	na
Českobudějovicku	Českobudějovicko	k1gNnSc6	Českobudějovicko
Karlsfried	Karlsfried	k1gMnSc1	Karlsfried
(	(	kIx(	(
<g/>
1357	[number]	k4	1357
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc1d1	malý
celní	celní	k2eAgInSc1d1	celní
hrad	hrad	k1gInSc1	hrad
na	na	k7c6	na
kupecké	kupecký	k2eAgFnSc6d1	kupecká
stezce	stezka	k1gFnSc6	stezka
do	do	k7c2	do
Žitavy	Žitava	k1gFnSc2	Žitava
Mimo	mimo	k7c4	mimo
dnešní	dnešní	k2eAgFnPc4d1	dnešní
hranice	hranice	k1gFnPc4	hranice
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Montecarlo	Montecarlo	k1gNnSc1	Montecarlo
(	(	kIx(	(
<g/>
1333	[number]	k4	1333
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
města	město	k1gNnSc2	město
Lucca	Lucc	k1gInSc2	Lucc
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
Lauf	lauf	k1gInSc1	lauf
(	(	kIx(	(
<g/>
1356	[number]	k4	1356
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
Norimberku	Norimberk	k1gInSc2	Norimberk
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
(	(	kIx(	(
<g/>
Horní	horní	k2eAgFnSc1d1	horní
Falc	Falc	k1gFnSc1	Falc
<g/>
)	)	kIx)	)
Velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
projevil	projevit	k5eAaPmAgInS	projevit
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
sídelní	sídelní	k2eAgNnSc4d1	sídelní
město	město	k1gNnSc4	město
Prahu	Praha	k1gFnSc4	Praha
<g/>
:	:	kIx,	:
Katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1344	[number]	k4	1344
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zde	zde	k6eAd1	zde
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
Janem	Jan	k1gMnSc7	Jan
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
slavnosti	slavnost	k1gFnSc2	slavnost
položení	položení	k1gNnSc2	položení
základního	základní	k2eAgInSc2d1	základní
kamene	kámen	k1gInSc2	kámen
byl	být	k5eAaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
i	i	k8xC	i
Karlův	Karlův	k2eAgMnSc1d1	Karlův
bratr	bratr	k1gMnSc1	bratr
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgFnSc2d1	sněžná
<g/>
,	,	kIx,	,
1347	[number]	k4	1347
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgNnSc1d1	Pražské
<g/>
,	,	kIx,	,
1348	[number]	k4	1348
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
přibližně	přibližně	k6eAd1	přibližně
360	[number]	k4	360
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
1348	[number]	k4	1348
<g/>
.	.	kIx.	.
</s>
<s>
Karlův	Karlův	k2eAgInSc1d1	Karlův
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
zvaný	zvaný	k2eAgInSc1d1	zvaný
Kamenný	kamenný	k2eAgInSc1d1	kamenný
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1357	[number]	k4	1357
<g/>
,	,	kIx,	,
most	most	k1gInSc1	most
nahradil	nahradit	k5eAaPmAgInS	nahradit
povodní	povodní	k2eAgInSc1d1	povodní
zničený	zničený	k2eAgInSc1d1	zničený
románský	románský	k2eAgInSc1d1	románský
Juditin	Juditin	k2eAgInSc1d1	Juditin
most	most	k1gInSc1	most
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
<g/>
:	:	kIx,	:
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Teplé	Teplé	k2eAgFnPc1d1	Teplé
lázně	lázeň	k1gFnPc1	lázeň
u	u	k7c2	u
Lokte	loket	k1gInSc2	loket
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1350	[number]	k4	1350
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgNnPc1d1	Městské
práva	právo	k1gNnPc1	právo
byla	být	k5eAaImAgNnP	být
udělena	udělen	k2eAgFnSc1d1	udělena
roku	rok	k1gInSc2	rok
1370	[number]	k4	1370
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
Pustiměř	Pustiměř	k1gInSc1	Pustiměř
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1340	[number]	k4	1340
<g/>
.	.	kIx.	.
</s>
<s>
Fundace	fundace	k1gFnSc1	fundace
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
památky	památka	k1gFnSc2	památka
Elišky	Eliška	k1gFnSc2	Eliška
Přemyslovny	Přemyslovna	k1gFnSc2	Přemyslovna
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1348	[number]	k4	1348
Karlovi	Karel	k1gMnSc3	Karel
zemřela	zemřít	k5eAaPmAgFnS	zemřít
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
Blanka	Blanka	k1gFnSc1	Blanka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1349	[number]	k4	1349
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Falckou	falcký	k2eAgFnSc7d1	Falcká
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
však	však	k9	však
zemřela	zemřít	k5eAaPmAgFnS	zemřít
již	již	k6eAd1	již
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1353	[number]	k4	1353
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1353	[number]	k4	1353
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Svídnickou	Svídnický	k2eAgFnSc7d1	Svídnická
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
porodila	porodit	k5eAaPmAgFnS	porodit
prvního	první	k4xOgMnSc4	první
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
budoucího	budoucí	k2eAgMnSc4d1	budoucí
krále	král	k1gMnSc4	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Svídnická	Svídnický	k2eAgFnSc1d1	Svídnická
zemřela	zemřít	k5eAaPmAgFnS	zemřít
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1362	[number]	k4	1362
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
počtvrté	počtvrté	k4xO	počtvrté
oženil	oženit	k5eAaPmAgMnS	oženit
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1363	[number]	k4	1363
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
Pomořanskou	pomořanský	k2eAgFnSc7d1	Pomořanská
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
legitimních	legitimní	k2eAgMnPc2d1	legitimní
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
manželka	manželka	k1gFnSc1	manželka
Blanka	Blanka	k1gFnSc1	Blanka
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
(	(	kIx(	(
<g/>
1316	[number]	k4	1316
<g/>
-	-	kIx~	-
<g/>
1348	[number]	k4	1348
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
(	(	kIx(	(
<g/>
1335	[number]	k4	1335
<g/>
-	-	kIx~	-
<g/>
1349	[number]	k4	1349
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1342	[number]	k4	1342
Ludvík	Ludvík	k1gMnSc1	Ludvík
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
1326	[number]	k4	1326
<g/>
-	-	kIx~	-
<g/>
1382	[number]	k4	1382
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
(	(	kIx(	(
<g/>
1342	[number]	k4	1342
<g/>
-	-	kIx~	-
<g/>
1395	[number]	k4	1395
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
a	a	k8xC	a
braniborská	braniborský	k2eAgFnSc1d1	Braniborská
markraběnka	markraběnka	k1gFnSc1	markraběnka
∞	∞	k?	∞
1	[number]	k4	1
<g/>
/	/	kIx~	/
1353	[number]	k4	1353
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1339	[number]	k4	1339
<g/>
-	-	kIx~	-
<g/>
1365	[number]	k4	1365
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
/	/	kIx~	/
1366	[number]	k4	1366
Ota	Oto	k1gMnSc2	Oto
V.	V.	kA	V.
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
(	(	kIx(	(
<g/>
1346	[number]	k4	1346
<g/>
/	/	kIx~	/
<g/>
1347	[number]	k4	1347
<g/>
-	-	kIx~	-
<g/>
1379	[number]	k4	1379
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
manželka	manželka	k1gFnSc1	manželka
Anna	Anna	k1gFnSc1	Anna
Falcká	falcký	k2eAgFnSc1d1	Falcká
(	(	kIx(	(
<g/>
1329	[number]	k4	1329
<g/>
-	-	kIx~	-
<g/>
1353	[number]	k4	1353
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
1350	[number]	k4	1350
<g/>
-	-	kIx~	-
<g/>
1351	[number]	k4	1351
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
manželka	manželka	k1gFnSc1	manželka
Anna	Anna	k1gFnSc1	Anna
Svídnická	Svídnický	k2eAgFnSc1d1	Svídnická
(	(	kIx(	(
<g/>
1339	[number]	k4	1339
<g/>
-	-	kIx~	-
<g/>
1362	[number]	k4	1362
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
(	(	kIx(	(
<g/>
1358	[number]	k4	1358
<g/>
-	-	kIx~	-
<g/>
1373	[number]	k4	1373
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1366	[number]	k4	1366
Albrecht	Albrecht	k1gMnSc1	Albrecht
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
(	(	kIx(	(
<g/>
1349	[number]	k4	1349
<g/>
/	/	kIx~	/
<g/>
1350	[number]	k4	1350
<g/>
-	-	kIx~	-
<g/>
1395	[number]	k4	1395
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1361	[number]	k4	1361
<g/>
-	-	kIx~	-
<g/>
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
∞	∞	k?	∞
1	[number]	k4	1
<g/>
/	/	kIx~	/
1370	[number]	k4	1370
Johana	Johana	k1gFnSc1	Johana
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
(	(	kIx(	(
<g/>
1356	[number]	k4	1356
<g/>
-	-	kIx~	-
<g/>
1386	[number]	k4	1386
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
/	/	kIx~	/
1389	[number]	k4	1389
Žofie	Žofie	k1gFnSc1	Žofie
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
(	(	kIx(	(
<g/>
1376	[number]	k4	1376
<g/>
-	-	kIx~	-
<g/>
1425	[number]	k4	1425
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
chlapec	chlapec	k1gMnSc1	chlapec
beze	beze	k7c2	beze
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
11	[number]	k4	11
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1362	[number]	k4	1362
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
manželka	manželka	k1gFnSc1	manželka
Alžběta	Alžběta	k1gFnSc1	Alžběta
Pomořanská	pomořanský	k2eAgFnSc1d1	Pomořanská
(	(	kIx(	(
<g/>
1346	[number]	k4	1346
<g/>
/	/	kIx~	/
<g/>
1347	[number]	k4	1347
<g/>
-	-	kIx~	-
<g/>
1393	[number]	k4	1393
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
(	(	kIx(	(
<g/>
1366	[number]	k4	1366
<g/>
-	-	kIx~	-
<g/>
1394	[number]	k4	1394
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
královna	královna	k1gFnSc1	královna
∞	∞	k?	∞
1383	[number]	k4	1383
Richard	Richarda	k1gFnPc2	Richarda
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1367	[number]	k4	1367
<g/>
-	-	kIx~	-
<g/>
1400	[number]	k4	1400
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
(	(	kIx(	(
<g/>
1368	[number]	k4	1368
<g/>
-	-	kIx~	-
<g/>
1437	[number]	k4	1437
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgMnSc1d1	uherský
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
1	[number]	k4	1
<g/>
/	/	kIx~	/
1385	[number]	k4	1385
Marie	Maria	k1gFnSc2	Maria
Uherská	uherský	k2eAgFnSc1d1	uherská
(	(	kIx(	(
<g/>
1371	[number]	k4	1371
<g/>
-	-	kIx~	-
<g/>
1395	[number]	k4	1395
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
/	/	kIx~	/
1408	[number]	k4	1408
Barbora	Barbora	k1gFnSc1	Barbora
Celjská	Celjský	k2eAgFnSc1d1	Celjská
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
1390	[number]	k4	1390
a	a	k8xC	a
1395	[number]	k4	1395
<g/>
-	-	kIx~	-
<g/>
1451	[number]	k4	1451
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Zhořelecký	zhořelecký	k2eAgMnSc1d1	zhořelecký
(	(	kIx(	(
<g/>
1370	[number]	k4	1370
<g/>
-	-	kIx~	-
<g/>
1396	[number]	k4	1396
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
braniborský	braniborský	k2eAgMnSc1d1	braniborský
markrabě	markrabě	k1gMnSc1	markrabě
a	a	k8xC	a
zhořelecký	zhořelecký	k2eAgMnSc1d1	zhořelecký
vévoda	vévoda	k1gMnSc1	vévoda
∞	∞	k?	∞
1388	[number]	k4	1388
Kateřina	Kateřina	k1gFnSc1	Kateřina
Meklenburská	meklenburský	k2eAgFnSc1d1	Meklenburská
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1371	[number]	k4	1371
<g/>
-	-	kIx~	-
<g/>
1400	[number]	k4	1400
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
1372	[number]	k4	1372
<g/>
-	-	kIx~	-
<g/>
1373	[number]	k4	1373
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
"	"	kIx"	"
<g/>
Mladší	mladý	k2eAgFnSc1d2	mladší
<g/>
"	"	kIx"	"
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
(	(	kIx(	(
<g/>
1373	[number]	k4	1373
<g/>
-	-	kIx~	-
<g/>
1410	[number]	k4	1410
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Hohenzollernu	Hohenzollern	k1gInSc2	Hohenzollern
(	(	kIx(	(
<g/>
*	*	kIx~	*
kolem	kolem	k7c2	kolem
1369	[number]	k4	1369
<g/>
-	-	kIx~	-
<g/>
1420	[number]	k4	1420
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
(	(	kIx(	(
<g/>
1377	[number]	k4	1377
<g/>
-	-	kIx~	-
<g/>
1378	[number]	k4	1378
<g/>
)	)	kIx)	)
Z	z	k7c2	z
neznámé	známý	k2eNgFnSc2d1	neznámá
souložnice	souložnice	k1gFnSc2	souložnice
<g/>
:	:	kIx,	:
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
,	,	kIx,	,
levoboček	levoboček	k1gMnSc1	levoboček
<g/>
,	,	kIx,	,
doložený	doložený	k2eAgMnSc1d1	doložený
z	z	k7c2	z
jediné	jediný	k2eAgFnSc2d1	jediná
listiny	listina	k1gFnSc2	listina
r.	r.	kA	r.
1377	[number]	k4	1377
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
anketu	anketa	k1gFnSc4	anketa
Největší	veliký	k2eAgMnSc1d3	veliký
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
diváci	divák	k1gMnPc1	divák
měli	mít	k5eAaImAgMnP	mít
zvolit	zvolit	k5eAaPmF	zvolit
největší	veliký	k2eAgFnSc4d3	veliký
osobnost	osobnost	k1gFnSc4	osobnost
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
soutěž	soutěž	k1gFnSc4	soutěž
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
formátu	formát	k1gInSc6	formát
britské	britský	k2eAgFnSc2d1	britská
BBC	BBC	kA	BBC
Greatest	Greatest	k1gMnSc1	Greatest
Britons	Britons	k1gInSc1	Britons
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
největšího	veliký	k2eAgMnSc2d3	veliký
Čecha	Čech	k1gMnSc2	Čech
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
právě	právě	k9	právě
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
68	[number]	k4	68
713	[number]	k4	713
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
skončil	skončit	k5eAaPmAgMnS	skončit
druhý	druhý	k4xOgMnSc1	druhý
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
55	[number]	k4	55
040	[number]	k4	040
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
též	též	k9	též
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
bankovce	bankovka	k1gFnSc6	bankovka
o	o	k7c6	o
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
100	[number]	k4	100
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
kolektivním	kolektivní	k2eAgNnSc6d1	kolektivní
vědomí	vědomí	k1gNnSc6	vědomí
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
populární	populární	k2eAgFnSc2d1	populární
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
oblibě	obliba	k1gFnSc3	obliba
přispěla	přispět	k5eAaPmAgFnS	přispět
například	například	k6eAd1	například
hudební	hudební	k2eAgFnSc1d1	hudební
komedie	komedie	k1gFnSc1	komedie
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Podskalského	podskalský	k2eAgMnSc2d1	podskalský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
natočená	natočený	k2eAgFnSc1d1	natočená
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Karla	Karel	k1gMnSc2	Karel
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Brodský	Brodský	k1gMnSc1	Brodský
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Karlově	Karlův	k2eAgInSc6d1	Karlův
životě	život	k1gInSc6	život
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
i	i	k9	i
komedie	komedie	k1gFnSc1	komedie
Slasti	slast	k1gFnSc2	slast
Otce	otec	k1gMnSc2	otec
vlasti	vlast	k1gFnSc2	vlast
režiséra	režisér	k1gMnSc2	režisér
Karla	Karel	k1gMnSc2	Karel
Steklého	Steklý	k1gMnSc2	Steklý
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Mladého	mladý	k2eAgMnSc4d1	mladý
panovníka	panovník	k1gMnSc4	panovník
zde	zde	k6eAd1	zde
hrál	hrát	k5eAaImAgMnS	hrát
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hanzlík	Hanzlík	k1gMnSc1	Hanzlík
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
700	[number]	k4	700
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Karlova	Karlův	k2eAgNnSc2d1	Karlovo
narození	narození	k1gNnSc2	narození
natočila	natočit	k5eAaBmAgFnS	natočit
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
výpravný	výpravný	k2eAgInSc4d1	výpravný
snímek	snímek	k1gInSc4	snímek
Hlas	hlas	k1gInSc1	hlas
pro	pro	k7c4	pro
římského	římský	k2eAgMnSc4d1	římský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
pojednávající	pojednávající	k2eAgMnPc4d1	pojednávající
především	především	k6eAd1	především
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgMnS	získat
Kryštof	Kryštof	k1gMnSc1	Kryštof
Hádek	hádek	k1gMnSc1	hádek
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
hrdinou	hrdina	k1gMnSc7	hrdina
i	i	k9	i
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
českých	český	k2eAgFnPc2d1	Česká
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k6eAd1	již
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
poslední	poslední	k2eAgFnSc2d1	poslední
sbírky	sbírka	k1gFnSc2	sbírka
Balady	balada	k1gFnSc2	balada
a	a	k8xC	a
romance	romance	k1gFnSc2	romance
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Romance	romance	k1gFnSc1	romance
o	o	k7c4	o
Karlu	Karla	k1gFnSc4	Karla
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pije	pít	k5eAaImIp3nS	pít
víno	víno	k1gNnSc4	víno
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pobočníkem	pobočník	k1gMnSc7	pobočník
Buškem	Bušek	k1gMnSc7	Bušek
z	z	k7c2	z
Velhartic	Velhartice	k1gFnPc2	Velhartice
a	a	k8xC	a
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
trpkostí	trpkost	k1gFnSc7	trpkost
<g/>
,	,	kIx,	,
náklonnost	náklonnost	k1gFnSc1	náklonnost
k	k	k7c3	k
českému	český	k2eAgInSc3d1	český
národu	národ	k1gInSc3	národ
<g/>
.	.	kIx.	.
</s>
