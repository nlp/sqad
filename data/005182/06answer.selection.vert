<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
Jupiterem	Jupiter	k1gMnSc7	Jupiter
a	a	k8xC	a
Sluncem	slunce	k1gNnSc7	slunce
je	být	k5eAaImIp3nS	být
778	[number]	k4	778
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
5,2	[number]	k4	5,2
AU	au	k0	au
<g/>
)	)	kIx)	)
a	a	k8xC	a
kolem	kolem	k6eAd1	kolem
Slunce	slunce	k1gNnSc1	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
11,86	[number]	k4	11,86
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
oběžné	oběžný	k2eAgFnSc2d1	oběžná
doby	doba	k1gFnSc2	doba
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
má	mít	k5eAaImIp3nS	mít
dráhovou	dráhový	k2eAgFnSc4d1	dráhová
rezonanci	rezonance	k1gFnSc4	rezonance
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
