<s>
Světová	světový	k2eAgFnSc1d1
zdravotnická	zdravotnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
WHO	WHO	kA
<g/>
,	,	kIx,
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
názvu	název	k1gInSc2
World	World	k1gInSc2
Health	Health	k1gNnSc2
Organization	Organization	k1gInSc1
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
SZO	SZO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
agentura	agentura	k1gFnSc1
Organizace	organizace	k1gFnSc2
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Centrálu	centrála	k1gFnSc4	centrála
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
WHO	WHO	kA	WHO
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Spojenými	spojený	k2eAgInPc7d1	spojený
národy	národ	k1gInPc7	národ
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
slaví	slavit	k5eAaImIp3nP	slavit
jako	jako	k9	jako
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Současnou	současný	k2eAgFnSc7d1	současná
generální	generální	k2eAgFnSc7d1	generální
ředitelkou	ředitelka	k1gFnSc7	ředitelka
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Margaret	Margareta	k1gFnPc2	Margareta
Chan	Chano	k1gNnPc2	Chano
<g/>
.	.	kIx.	.
</s>
<s>
WHO	WHO	kA	WHO
zdědila	zdědit	k5eAaPmAgFnS	zdědit
mnoho	mnoho	k4c4	mnoho
mandátů	mandát	k1gInPc2	mandát
a	a	k8xC	a
zdrojů	zdroj	k1gInPc2	zdroj
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
předchůdci	předchůdce	k1gMnPc1	předchůdce
<g/>
,	,	kIx,	,
Zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
organizaci	organizace	k1gFnSc4	organizace
(	(	kIx(	(
<g/>
Health	Health	k1gInSc1	Health
Organisation	Organisation	k1gInSc1	Organisation
-	-	kIx~	-
HO	on	k3xPp3gMnSc4	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
agenturou	agentura	k1gFnSc7	agentura
organizace	organizace	k1gFnSc1	organizace
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
Liga	liga	k1gFnSc1	liga
Národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převzala	převzít	k5eAaPmAgFnS	převzít
plnění	plnění	k1gNnSc4	plnění
povinností	povinnost	k1gFnPc2	povinnost
a	a	k8xC	a
úkolů	úkol	k1gInPc2	úkol
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
úřadu	úřad	k1gInSc2	úřad
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
(	(	kIx(	(
<g/>
OIHP	OIHP	kA	OIHP
<g/>
)	)	kIx)	)
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
byly	být	k5eAaImAgInP	být
stanoveny	stanovit	k5eAaPmNgInP	stanovit
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úmluvě	úmluva	k1gFnSc6	úmluva
podepsané	podepsaný	k2eAgFnSc6d1	podepsaná
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Konstituce	konstituce	k1gFnSc1	konstituce
(	(	kIx(	(
<g/>
statut	statut	k1gInSc1	statut
<g/>
)	)	kIx)	)
WHO	WHO	kA	WHO
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dosažení	dosažení	k1gNnSc3	dosažení
všemi	všecek	k3xTgMnPc7	všecek
lidmi	člověk	k1gMnPc7	člověk
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
možné	možný	k2eAgFnSc2d1	možná
úrovně	úroveň	k1gFnSc2	úroveň
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
hlavní	hlavní	k2eAgFnSc1d1	hlavní
úloha	úloha	k1gFnSc1	úloha
je	být	k5eAaImIp3nS	být
likvidovat	likvidovat	k5eAaBmF	likvidovat
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
klíčové	klíčový	k2eAgFnPc4d1	klíčová
infekční	infekční	k2eAgFnPc4d1	infekční
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
monitorování	monitorování	k1gNnSc2	monitorování
průběhu	průběh	k1gInSc2	průběh
a	a	k8xC	a
šíření	šíření	k1gNnSc2	šíření
infekčních	infekční	k2eAgFnPc2d1	infekční
nemocí	nemoc	k1gFnPc2	nemoc
jako	jako	k9	jako
SARS	SARS	kA	SARS
<g/>
,	,	kIx,	,
malárie	malárie	k1gFnSc2	malárie
a	a	k8xC	a
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
realizuje	realizovat	k5eAaBmIp3nS	realizovat
též	též	k9	též
programy	program	k1gInPc4	program
na	na	k7c4	na
likvidaci	likvidace	k1gFnSc4	likvidace
těchto	tento	k3xDgFnPc2	tento
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
distribucí	distribuce	k1gFnSc7	distribuce
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letech	léto	k1gNnPc6	léto
likvidace	likvidace	k1gFnSc2	likvidace
pravých	pravý	k2eAgFnPc2d1	pravá
neštovic	neštovice	k1gFnPc2	neštovice
<g/>
,	,	kIx,	,
WHO	WHO	kA	WHO
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemoc	nemoc	k1gFnSc1	nemoc
byla	být	k5eAaImAgFnS	být
eradikována	eradikovat	k5eAaImNgFnS	eradikovat
-	-	kIx~	-
jako	jako	k8xC	jako
první	první	k4xOgFnSc4	první
nemoc	nemoc	k1gFnSc4	nemoc
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vymýcena	vymýtit	k5eAaPmNgFnS	vymýtit
<g/>
.	.	kIx.	.
</s>
<s>
WHO	WHO	kA	WHO
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
úspěchu	úspěch	k1gInSc2	úspěch
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
vakcín	vakcína	k1gFnPc2	vakcína
proti	proti	k7c3	proti
nemocem	nemoc	k1gFnPc3	nemoc
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
malárie	malárie	k1gFnSc1	malárie
a	a	k8xC	a
schistosomóza	schistosomóza	k1gFnSc1	schistosomóza
(	(	kIx(	(
<g/>
infekční	infekční	k2eAgFnSc1d1	infekční
nemoc	nemoc	k1gFnSc1	nemoc
způsobená	způsobený	k2eAgFnSc1d1	způsobená
motolicemi	motolice	k1gFnPc7	motolice
rodu	rod	k1gInSc2	rod
Schistosoma	Schistosoma	k1gFnSc1	Schistosoma
<g/>
)	)	kIx)	)
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
eliminovat	eliminovat	k5eAaBmF	eliminovat
dětskou	dětský	k2eAgFnSc4d1	dětská
obrnu	obrna	k1gFnSc4	obrna
v	v	k7c6	v
nejbližších	blízký	k2eAgNnPc6d3	nejbližší
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
WHO	WHO	kA	WHO
definuje	definovat	k5eAaBmIp3nS	definovat
zdraví	zdraví	k1gNnSc4	zdraví
jako	jako	k8xC	jako
stav	stav	k1gInSc4	stav
kompletní	kompletní	k2eAgFnSc2d1	kompletní
fyzické	fyzický	k2eAgFnSc2d1	fyzická
<g/>
,	,	kIx,	,
mentální	mentální	k2eAgFnSc2d1	mentální
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
pohody	pohoda	k1gFnSc2	pohoda
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
jenom	jenom	k9	jenom
jako	jako	k9	jako
absenci	absence	k1gFnSc4	absence
nemoci	nemoc	k1gFnSc2	nemoc
nebo	nebo	k8xC	nebo
vady	vada	k1gFnSc2	vada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
boje	boj	k1gInPc4	boj
proti	proti	k7c3	proti
různým	různý	k2eAgFnPc3d1	různá
nemocem	nemoc	k1gFnPc3	nemoc
<g/>
,	,	kIx,	,
WHO	WHO	kA	WHO
též	též	k9	též
provádí	provádět	k5eAaImIp3nS	provádět
celosvětové	celosvětový	k2eAgFnPc4d1	celosvětová
kampaně	kampaň	k1gFnPc4	kampaň
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
konzumace	konzumace	k1gFnSc2	konzumace
zeleniny	zelenina	k1gFnSc2	zelenina
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
konzumace	konzumace	k1gFnSc2	konzumace
tabáku	tabák	k1gInSc2	tabák
<g/>
,	,	kIx,	,
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
také	také	k9	také
výzkum	výzkum	k1gInSc1	výzkum
-	-	kIx~	-
např.	např.	kA	např.
zdali	zdali	k8xS	zdali
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
pole	pole	k1gNnSc1	pole
kolem	kolem	k7c2	kolem
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
má	mít	k5eAaImIp3nS	mít
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
WHO	WHO	kA	WHO
zpráva	zpráva	k1gFnSc1	zpráva
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
doporučila	doporučit	k5eAaPmAgFnS	doporučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsah	obsah	k1gInSc1	obsah
cukru	cukr	k1gInSc2	cukr
ve	v	k7c6	v
zdravé	zdravý	k2eAgFnSc6d1	zdravá
dietě	dieta	k1gFnSc6	dieta
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
lobbování	lobbování	k1gNnSc3	lobbování
(	(	kIx(	(
<g/>
nátlaku	nátlak	k1gInSc2	nátlak
<g/>
)	)	kIx)	)
cukrovarnického	cukrovarnický	k2eAgInSc2d1	cukrovarnický
průmyslu	průmysl	k1gInSc2	průmysl
právě	právě	k9	právě
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
doporučení	doporučení	k1gNnSc3	doporučení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uvedeným	uvedený	k2eAgInPc3d1	uvedený
úkolům	úkol	k1gInPc3	úkol
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
smlouvy	smlouva	k1gFnSc2	smlouva
přidělili	přidělit	k5eAaPmAgMnP	přidělit
organizaci	organizace	k1gFnSc4	organizace
mnoho	mnoho	k6eAd1	mnoho
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
za	za	k7c4	za
následující	následující	k2eAgFnPc4d1	následující
konvence	konvence	k1gFnPc4	konvence
-	-	kIx~	-
např.	např.	kA	např.
Jednotnou	jednotný	k2eAgFnSc4d1	jednotná
konvenci	konvence	k1gFnSc4	konvence
o	o	k7c6	o
narkotických	narkotický	k2eAgFnPc6d1	narkotická
drogách	droga	k1gFnPc6	droga
(	(	kIx(	(
<g/>
Single	singl	k1gInSc5	singl
Convention	Convention	k1gInSc1	Convention
on	on	k3xPp3gMnSc1	on
Narcotic	Narcotice	k1gFnPc2	Narcotice
Drugs	Drugs	k1gInSc1	Drugs
<g/>
)	)	kIx)	)
a	a	k8xC	a
Konvenci	konvence	k1gFnSc4	konvence
o	o	k7c6	o
psychotropických	psychotropický	k2eAgFnPc6d1	psychotropický
látkách	látka	k1gFnPc6	látka
(	(	kIx(	(
<g/>
Convention	Convention	k1gInSc4	Convention
on	on	k3xPp3gMnSc1	on
Psychotropic	Psychotropic	k1gMnSc1	Psychotropic
Substances	Substances	k1gMnSc1	Substances
<g/>
)	)	kIx)	)
žádají	žádat	k5eAaImIp3nP	žádat
WHO	WHO	kA	WHO
vydat	vydat	k5eAaPmF	vydat
závazné	závazný	k2eAgFnPc4d1	závazná
vědecké	vědecký	k2eAgFnPc4d1	vědecká
a	a	k8xC	a
medicínské	medicínský	k2eAgNnSc1d1	medicínské
hodnocení	hodnocení	k1gNnSc1	hodnocení
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
doporučit	doporučit	k5eAaPmF	doporučit
jak	jak	k8xC	jak
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
regulovány	regulován	k2eAgFnPc1d1	regulována
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
WHO	WHO	kA	WHO
koná	konat	k5eAaImIp3nS	konat
dohled	dohled	k1gInSc4	dohled
na	na	k7c4	na
výkon	výkon	k1gInSc4	výkon
zákonodární	zákonodární	k2eAgFnSc2d1	zákonodární
Komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
narkotické	narkotický	k2eAgFnPc4d1	narkotická
drogy	droga	k1gFnPc4	droga
(	(	kIx(	(
<g/>
Commission	Commission	k1gInSc1	Commission
on	on	k3xPp3gMnSc1	on
Narcotic	Narcotice	k1gFnPc2	Narcotice
Drugs	Drugs	k1gInSc1	Drugs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orgány	orgán	k1gInPc1	orgán
WHO	WHO	kA	WHO
jsou	být	k5eAaImIp3nP	být
Světové	světový	k2eAgNnSc4d1	světové
zdravotnické	zdravotnický	k2eAgNnSc4d1	zdravotnické
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
,	,	kIx,	,
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
sekretariát	sekretariát	k1gInSc1	sekretariát
<g/>
.	.	kIx.	.
</s>
<s>
Světové	světový	k2eAgNnSc1d1	světové
zdravotnické	zdravotnický	k2eAgNnSc1d1	zdravotnické
shromáždění	shromáždění	k1gNnSc1	shromáždění
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
delegátů	delegát	k1gMnPc2	delegát
zastupujících	zastupující	k2eAgFnPc2d1	zastupující
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
ne	ne	k9	ne
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třemi	tři	k4xCgInPc7	tři
delegáty	delegát	k1gMnPc7	delegát
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
za	za	k7c4	za
hlavního	hlavní	k2eAgMnSc4d1	hlavní
delegáta	delegát	k1gMnSc4	delegát
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
má	mít	k5eAaImIp3nS	mít
ve	v	k7c4	v
shromáždění	shromáždění	k1gNnSc4	shromáždění
jeden	jeden	k4xCgInSc1	jeden
hlas	hlas	k1gInSc1	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
sejde	sejít	k5eAaPmIp3nS	sejít
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
kromě	kromě	k7c2	kromě
jmenování	jmenování	k1gNnSc2	jmenování
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
(	(	kIx(	(
<g/>
na	na	k7c4	na
pětiletý	pětiletý	k2eAgInSc4d1	pětiletý
termín	termín	k1gInSc4	termín
<g/>
)	)	kIx)	)
monitoruje	monitorovat	k5eAaImIp3nS	monitorovat
finanční	finanční	k2eAgNnSc4d1	finanční
řízení	řízení	k1gNnSc4	řízení
Organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
také	také	k9	také
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
a	a	k8xC	a
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
předložený	předložený	k2eAgInSc4d1	předložený
programový	programový	k2eAgInSc4d1	programový
rozpočet	rozpočet	k1gInSc4	rozpočet
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
32	[number]	k4	32
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
technicky	technicky	k6eAd1	technicky
kvalifikováni	kvalifikován	k2eAgMnPc1d1	kvalifikován
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
na	na	k7c4	na
tříletý	tříletý	k2eAgInSc4d1	tříletý
termín	termín	k1gInSc4	termín
do	do	k7c2	do
Výkonné	výkonný	k2eAgFnSc2d1	výkonná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
34	[number]	k4	34
osob	osoba	k1gFnPc2	osoba
určených	určený	k2eAgFnPc2d1	určená
stejným	stejný	k2eAgInSc7d1	stejný
počtem	počet	k1gInSc7	počet
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
zvolených	zvolený	k2eAgInPc2d1	zvolený
Světovým	světový	k2eAgNnSc7d1	světové
zdravotnickým	zdravotnický	k2eAgNnSc7d1	zdravotnické
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
;	;	kIx,	;
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
léta	léto	k1gNnPc4	léto
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
býti	být	k5eAaImF	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
výbory	výbor	k1gInPc4	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
funkce	funkce	k1gFnSc1	funkce
Rady	rada	k1gFnSc2	rada
je	být	k5eAaImIp3nS	být
realizace	realizace	k1gFnSc1	realizace
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
Shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
povinna	povinen	k2eAgFnSc1d1	povinna
radit	radit	k5eAaImF	radit
a	a	k8xC	a
podporovat	podporovat	k5eAaImF	podporovat
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Sekretariát	sekretariát	k1gInSc1	sekretariát
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
a	a	k8xC	a
technických	technický	k2eAgMnPc2d1	technický
a	a	k8xC	a
správních	správní	k2eAgMnPc2d1	správní
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
11000	[number]	k4	11000
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tvořeni	tvořit	k5eAaImNgMnP	tvořit
zdravotními	zdravotní	k2eAgFnPc7d1	zdravotní
a	a	k8xC	a
jinými	jiný	k2eAgMnPc7d1	jiný
experty	expert	k1gMnPc7	expert
a	a	k8xC	a
pomocným	pomocný	k2eAgInSc7d1	pomocný
personálem	personál	k1gInSc7	personál
<g/>
,	,	kIx,	,
pracujícím	pracující	k2eAgMnSc7d1	pracující
v	v	k7c6	v
centrále	centrála	k1gFnSc6	centrála
<g/>
,	,	kIx,	,
v	v	k7c6	v
6	[number]	k4	6
regionálních	regionální	k2eAgInPc6d1	regionální
úřadech	úřad	k1gInPc6	úřad
a	a	k8xC	a
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Světové	světový	k2eAgNnSc1d1	světové
zdravotnické	zdravotnický	k2eAgNnSc1d1	zdravotnické
shromáždění	shromáždění	k1gNnSc1	shromáždění
nebo	nebo	k8xC	nebo
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
mohou	moct	k5eAaImIp3nP	moct
svolati	svolat	k5eAaPmF	svolat
místní	místní	k2eAgFnPc1d1	místní
<g/>
,	,	kIx,	,
obecné	obecný	k2eAgFnPc1d1	obecná
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgFnPc1d1	odborná
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
konference	konference	k1gFnPc1	konference
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
studovaly	studovat	k5eAaImAgFnP	studovat
otázky	otázka	k1gFnPc4	otázka
spadající	spadající	k2eAgFnPc4d1	spadající
do	do	k7c2	do
působnosti	působnost	k1gFnSc2	působnost
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organisace	organisace	k1gFnSc2	organisace
<g/>
.	.	kIx.	.
</s>
<s>
WHO	WHO	kA	WHO
má	mít	k5eAaImIp3nS	mít
194	[number]	k4	194
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgInPc2	všecek
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
OSN	OSN	kA	OSN
kromě	kromě	k7c2	kromě
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
OSN	OSN	kA	OSN
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
připojit	připojit	k5eAaPmF	připojit
jako	jako	k9	jako
přidružení	přidružený	k2eAgMnPc1d1	přidružený
členové	člen	k1gMnPc1	člen
(	(	kIx(	(
<g/>
s	s	k7c7	s
kompletní	kompletní	k2eAgFnSc7d1	kompletní
informací	informace	k1gFnSc7	informace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
limitovanou	limitovaný	k2eAgFnSc7d1	limitovaná
participací	participace	k1gFnSc7	participace
a	a	k8xC	a
volebními	volební	k2eAgNnPc7d1	volební
právy	právo	k1gNnPc7	právo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
schváleno	schválit	k5eAaPmNgNnS	schválit
volbou	volba	k1gFnSc7	volba
Shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
jsou	být	k5eAaImIp3nP	být
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
entity	entita	k1gFnSc2	entita
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
-	-	kIx~	-
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
Niue	Niu	k1gInPc1	Niu
<g/>
.	.	kIx.	.
</s>
<s>
Entitám	entita	k1gFnPc3	entita
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
též	též	k9	též
poskytnut	poskytnut	k2eAgInSc4d1	poskytnut
status	status	k1gInSc4	status
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
-	-	kIx~	-
např.	např.	kA	např.
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
agentura	agentura	k1gFnSc1	agentura
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
WHO	WHO	kA	WHO
je	být	k5eAaImIp3nS	být
financována	financovat	k5eAaBmNgFnS	financovat
systémem	systém	k1gInSc7	systém
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
-	-	kIx~	-
příspěvky	příspěvek	k1gInPc1	příspěvek
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současných	současný	k2eAgInPc6d1	současný
letech	let	k1gInPc6	let
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
WHO	WHO	kA	WHO
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
kooperace	kooperace	k1gFnSc1	kooperace
s	s	k7c7	s
partnery	partner	k1gMnPc7	partner
jako	jako	k9	jako
mimovládní	mimovládní	k2eAgFnSc2d1	mimovládní
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
farmaceutický	farmaceutický	k2eAgInSc1d1	farmaceutický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
i	i	k8xC	i
s	s	k7c7	s
nadacemi	nadace	k1gFnPc7	nadace
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Nadace	nadace	k1gFnSc1	nadace
Billa	Bill	k1gMnSc2	Bill
a	a	k8xC	a
Melindy	Melinda	k1gFnSc2	Melinda
Gates	Gates	k1gMnSc1	Gates
nebo	nebo	k8xC	nebo
Rockefellerova	Rockefellerův	k2eAgFnSc1d1	Rockefellerova
nadace	nadace	k1gFnSc1	nadace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
formy	forma	k1gFnPc1	forma
spolupráce	spolupráce	k1gFnSc2	spolupráce
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
spolupráci	spolupráce	k1gFnSc4	spolupráce
mezi	mezi	k7c7	mezi
veřejným	veřejný	k2eAgInSc7d1	veřejný
a	a	k8xC	a
soukromým	soukromý	k2eAgInSc7d1	soukromý
sektorem	sektor	k1gInSc7	sektor
<g/>
;	;	kIx,	;
polovina	polovina	k1gFnSc1	polovina
rozpočtu	rozpočet	k1gInSc2	rozpočet
organizace	organizace	k1gFnSc2	organizace
WHO	WHO	kA	WHO
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
financována	financovat	k5eAaBmNgFnS	financovat
soukromými	soukromý	k2eAgFnPc7d1	soukromá
nadacemi	nadace	k1gFnPc7	nadace
a	a	k8xC	a
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
podniky	podnik	k1gInPc7	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
PATH	PATH	kA	PATH
<g/>
:	:	kIx,	:
Program	program	k1gInSc1	program
for	forum	k1gNnPc2	forum
Appropriate	Appropriat	k1gInSc5	Appropriat
Technology	technolog	k1gMnPc4	technolog
in	in	k?	in
Health	Health	k1gMnSc1	Health
(	(	kIx(	(
<g/>
http://www.path.org	[url]	k1gMnSc1	http://www.path.org
<g/>
)	)	kIx)	)
IAVI	IAVI	kA	IAVI
<g/>
:	:	kIx,	:
International	International	k1gMnSc1	International
AIDS	AIDS	kA	AIDS
Vaccine	Vaccin	k1gInSc5	Vaccin
Initiative	Initiativ	k1gInSc5	Initiativ
(	(	kIx(	(
<g/>
http://www.iavi.org	[url]	k1gMnSc1	http://www.iavi.org
<g/>
)	)	kIx)	)
MMV	MMV	kA	MMV
<g/>
:	:	kIx,	:
Medicines	Medicinesa	k1gFnPc2	Medicinesa
for	forum	k1gNnPc2	forum
Malaria	Malarium	k1gNnPc1	Malarium
Venture	Ventur	k1gMnSc5	Ventur
(	(	kIx(	(
<g/>
http://www.mmv.org	[url]	k1gMnSc1	http://www.mmv.org
<g/>
)	)	kIx)	)
MVI	MVI	kA	MVI
<g/>
:	:	kIx,	:
Malaria	Malarium	k1gNnPc1	Malarium
Vaccine	Vaccin	k1gInSc5	Vaccin
Initiative	Initiativ	k1gInSc5	Initiativ
(	(	kIx(	(
<g/>
http://www.malariavaccine.org	[url]	k1gMnSc1	http://www.malariavaccine.org
<g/>
)	)	kIx)	)
TB	TB	kA	TB
<g />
.	.	kIx.	.
</s>
<s>
Alliance	Alliance	k1gFnSc1	Alliance
<g/>
:	:	kIx,	:
Global	globat	k5eAaImAgMnS	globat
Alliance	Alliance	k1gFnPc4	Alliance
for	forum	k1gNnPc2	forum
TB	TB	kA	TB
Drug	Drug	k1gMnSc1	Drug
Development	Development	k1gMnSc1	Development
(	(	kIx(	(
<g/>
http://www.tballiance.org	[url]	k1gMnSc1	http://www.tballiance.org
<g/>
)	)	kIx)	)
Aeras	Aeras	k1gMnSc1	Aeras
<g/>
:	:	kIx,	:
Aeras	Aeras	k1gMnSc1	Aeras
Global	globat	k5eAaImAgMnS	globat
TB	TB	kA	TB
Vaccine	Vaccin	k1gInSc5	Vaccin
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
http://aeras.org	[url]	k1gMnSc1	http://aeras.org
<g/>
)	)	kIx)	)
IPM	IPM	kA	IPM
<g/>
:	:	kIx,	:
International	International	k1gMnSc1	International
Partnership	Partnership	k1gMnSc1	Partnership
for	forum	k1gNnPc2	forum
Microbicides	Microbicides	k1gInSc1	Microbicides
(	(	kIx(	(
<g/>
http://www.ipm-microbicides.org	[url]	k1gMnSc1	http://www.ipm-microbicides.org
<g/>
)	)	kIx)	)
PDVI	PDVI	kA	PDVI
<g/>
:	:	kIx,	:
Pediatric	Pediatric	k1gMnSc1	Pediatric
Dengue	Dengu	k1gFnSc2	Dengu
Vaccine	Vaccin	k1gInSc5	Vaccin
Initiative	Initiativ	k1gInSc5	Initiativ
(	(	kIx(	(
<g/>
http://www.pdvi.org	[url]	k1gInSc1	http://www.pdvi.org
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
FIND	FIND	k?	FIND
<g/>
:	:	kIx,	:
Foundation	Foundation	k1gInSc1	Foundation
for	forum	k1gNnPc2	forum
Innovative	Innovativ	k1gInSc5	Innovativ
New	New	k1gFnPc3	New
Diagnostics	Diagnostics	k1gInSc4	Diagnostics
(	(	kIx(	(
<g/>
http://www.finddiagnostics.org	[url]	k1gMnSc1	http://www.finddiagnostics.org
<g/>
)	)	kIx)	)
IOWH	IOWH	kA	IOWH
<g/>
:	:	kIx,	:
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
One	One	k1gMnSc1	One
World	World	k1gMnSc1	World
Health	Health	k1gMnSc1	Health
(	(	kIx(	(
<g/>
http://www.oneworldhealth.org	[url]	k1gInSc1	http://www.oneworldhealth.org
<g/>
)	)	kIx)	)
DNDi	DNDi	k1gNnSc1	DNDi
<g/>
:	:	kIx,	:
Drugs	Drugsa	k1gFnPc2	Drugsa
for	forum	k1gNnPc2	forum
Neglected	Neglected	k1gMnSc1	Neglected
Diseases	Diseases	k1gMnSc1	Diseases
Initiative	Initiativ	k1gInSc5	Initiativ
(	(	kIx(	(
<g/>
http://www.dndi.org	[url]	k1gInSc4	http://www.dndi.org
<g/>
)	)	kIx)	)
Carlo	Carlo	k1gNnSc4	Carlo
Urbani	Urban	k1gMnPc1	Urban
Andrija	Andrij	k1gInSc2	Andrij
Štampar	Štampar	k1gInSc1	Štampar
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
centrály	centrála	k1gFnSc2	centrála
organizace	organizace	k1gFnSc2	organizace
vydávala	vydávat	k5eAaImAgFnS	vydávat
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
poštovní	poštovní	k2eAgFnSc1d1	poštovní
správa	správa	k1gFnSc1	správa
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
známky	známka	k1gFnSc2	známka
své	svůj	k3xOyFgFnPc4	svůj
země	zem	k1gFnPc4	zem
opatřené	opatřený	k2eAgFnPc4d1	opatřená
přetiskem	přetisk	k1gInSc7	přetisk
ve	v	k7c4	v
znění	znění	k1gNnSc4	znění
ORGANISATION	ORGANISATION	kA	ORGANISATION
MONDIALE	MONDIALE	kA	MONDIALE
DE	DE	k?	DE
LA	la	k1gNnSc1	la
SANTÉ	SANTÉ	kA	SANTÉ
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vydala	vydat	k5eAaPmAgFnS	vydat
speciální	speciální	k2eAgFnPc4d1	speciální
emise	emise	k1gFnPc4	emise
již	již	k6eAd1	již
bez	bez	k7c2	bez
přetisků	přetisk	k1gInPc2	přetisk
<g/>
,	,	kIx,	,
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
nápisem	nápis	k1gInSc7	nápis
a	a	k8xC	a
nadtitulkem	nadtitulko	k1gNnSc7	nadtitulko
HELVETIA	Helvetia	k1gFnSc1	Helvetia
<g/>
.	.	kIx.	.
</s>
<s>
Námětem	námět	k1gInSc7	námět
byl	být	k5eAaImAgInS	být
znak	znak	k1gInSc1	znak
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
měna	měna	k1gFnSc1	měna
i	i	k8xC	i
provedení	provedení	k1gNnSc1	provedení
švýcarské	švýcarský	k2eAgNnSc1d1	švýcarské
<g/>
.	.	kIx.	.
</s>
