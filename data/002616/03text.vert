<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
ultralevicová	ultralevicový	k2eAgFnSc1d1	ultralevicová
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
členka	členka	k1gFnSc1	členka
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
internacionály	internacionála	k1gFnSc2	internacionála
(	(	kIx(	(
<g/>
KI	KI	kA	KI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
základním	základní	k2eAgNnPc3d1	základní
programovým	programový	k2eAgNnPc3d1	programové
východiskům	východisko	k1gNnPc3	východisko
patřil	patřit	k5eAaImAgInS	patřit
třídní	třídní	k2eAgInSc1d1	třídní
boj	boj	k1gInSc1	boj
vedený	vedený	k2eAgInSc1d1	vedený
dle	dle	k7c2	dle
sovětského	sovětský	k2eAgInSc2d1	sovětský
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
i	i	k8xC	i
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
likvidaci	likvidace	k1gFnSc4	likvidace
třídních	třídní	k1gMnPc2	třídní
a	a	k8xC	a
ideologických	ideologický	k2eAgMnPc2d1	ideologický
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
protiprávnosti	protiprávnost	k1gFnSc6	protiprávnost
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
organizaci	organizace	k1gFnSc4	organizace
zločinnou	zločinný	k2eAgFnSc4d1	zločinná
a	a	k8xC	a
zavrženíhodnou	zavrženíhodný	k2eAgFnSc4d1	zavrženíhodná
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
ideovým	ideový	k2eAgMnSc7d1	ideový
i	i	k8xC	i
právním	právní	k2eAgMnSc7d1	právní
nástupcem	nástupce	k1gMnSc7	nástupce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Ustavující	ustavující	k2eAgInSc1d1	ustavující
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1921	[number]	k4	1921
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
sále	sál	k1gInSc6	sál
Národního	národní	k2eAgInSc2d1	národní
domu	dům	k1gInSc2	dům
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
sjezd	sjezd	k1gInSc1	sjezd
probíhal	probíhat	k5eAaImAgInS	probíhat
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
předcházela	předcházet	k5eAaImAgFnS	předcházet
mu	on	k3xPp3gMnSc3	on
sobotní	sobotní	k2eAgFnSc1d1	sobotní
předporada	předporada	k1gFnSc1	předporada
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
Československé	československý	k2eAgFnSc2d1	Československá
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
(	(	kIx(	(
<g/>
levice	levice	k1gFnSc2	levice
<g/>
)	)	kIx)	)
a	a	k8xC	a
volba	volba	k1gFnSc1	volba
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
569	[number]	k4	569
delegátů	delegát	k1gMnPc2	delegát
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zastupovali	zastupovat	k5eAaImAgMnP	zastupovat
zhruba	zhruba	k6eAd1	zhruba
350	[number]	k4	350
tisíc	tisíc	k4xCgInPc2	tisíc
levicových	levicový	k2eAgMnPc2d1	levicový
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
schůzi	schůze	k1gFnSc6	schůze
16	[number]	k4	16
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1921	[number]	k4	1921
zvolil	zvolit	k5eAaPmAgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
Václava	Václav	k1gMnSc2	Václav
Šturce	Šturce	k?	Šturce
<g/>
,	,	kIx,	,
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Bohumíra	Bohumír	k1gMnSc2	Bohumír
Šmerala	Šmeral	k1gMnSc2	Šmeral
a	a	k8xC	a
druhým	druhý	k4xOgNnSc7	druhý
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Václava	Václav	k1gMnSc4	Václav
Bolena	bolen	k2eAgFnSc1d1	bolena
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
kontrolní	kontrolní	k2eAgFnSc2d1	kontrolní
komise	komise	k1gFnSc2	komise
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
František	František	k1gMnSc1	František
Hovorka	Hovorka	k1gMnSc1	Hovorka
<g/>
,	,	kIx,	,
pokladníkem	pokladník	k1gMnSc7	pokladník
pak	pak	k6eAd1	pak
prozatímně	prozatímně	k6eAd1	prozatímně
Josef	Josef	k1gMnSc1	Josef
Skalák	Skalák	k1gMnSc1	Skalák
<g/>
.	.	kIx.	.
</s>
<s>
KSČ	KSČ	kA	KSČ
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přejmenováním	přejmenování	k1gNnSc7	přejmenování
odštěpené	odštěpený	k2eAgFnSc2d1	odštěpená
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
Československé	československý	k2eAgFnSc2d1	Československá
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Prosincová	prosincový	k2eAgFnSc1d1	prosincová
generální	generální	k2eAgFnSc1d1	generální
stávka	stávka	k1gFnSc1	stávka
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Programem	program	k1gInSc7	program
byla	být	k5eAaImAgFnS	být
koaliční	koaliční	k2eAgFnSc1d1	koaliční
vláda	vláda	k1gFnSc1	vláda
socialistických	socialistický	k2eAgFnPc2d1	socialistická
stran	strana	k1gFnPc2	strana
s	s	k7c7	s
levicí	levice	k1gFnSc7	levice
agrárníků	agrárník	k1gMnPc2	agrárník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
absolutních	absolutní	k2eAgFnPc6d1	absolutní
i	i	k8xC	i
relativních	relativní	k2eAgFnPc6d1	relativní
číslech	číslo	k1gNnPc6	číslo
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
muselo	muset	k5eAaImAgNnS	muset
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
počítat	počítat	k5eAaImF	počítat
i	i	k9	i
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
934	[number]	k4	934
223	[number]	k4	223
hlasy	hlas	k1gInPc7	hlas
a	a	k8xC	a
41	[number]	k4	41
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
proces	proces	k1gInSc1	proces
"	"	kIx"	"
<g/>
bolševizace	bolševizace	k1gFnSc1	bolševizace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
utužení	utužení	k1gNnSc1	utužení
programové	programový	k2eAgFnSc2d1	programová
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
Komunistické	komunistický	k2eAgFnSc6d1	komunistická
internacionále	internacionála	k1gFnSc6	internacionála
(	(	kIx(	(
<g/>
KI	KI	kA	KI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
V.	V.	kA	V.
sjezdu	sjezd	k1gInSc2	sjezd
KSČ	KSČ	kA	KSČ
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
až	až	k9	až
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1929	[number]	k4	1929
konaném	konaný	k2eAgNnSc6d1	konané
v	v	k7c6	v
holešovické	holešovický	k2eAgFnSc6d1	Holešovická
Domovině	domovina	k1gFnSc6	domovina
se	se	k3xPyFc4	se
jejího	její	k3xOp3gNnSc2	její
vedení	vedení	k1gNnSc2	vedení
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
karlínští	karlínský	k2eAgMnPc1d1	karlínský
kluci	kluk	k1gMnPc1	kluk
<g/>
"	"	kIx"	"
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Klementem	Klement	k1gMnSc7	Klement
Gottwaldem	Gottwald	k1gMnSc7	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
KSČ	KSČ	kA	KSČ
odešla	odejít	k5eAaPmAgFnS	odejít
většina	většina	k1gFnSc1	většina
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
strana	strana	k1gFnSc1	strana
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
následných	následný	k2eAgFnPc6d1	následná
říjnových	říjnový	k2eAgFnPc6d1	říjnová
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
753	[number]	k4	753
220	[number]	k4	220
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
celkově	celkově	k6eAd1	celkově
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
s	s	k7c7	s
30	[number]	k4	30
mandáty	mandát	k1gInPc7	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
mandátů	mandát	k1gInPc2	mandát
si	se	k3xPyFc3	se
podržela	podržet	k5eAaPmAgFnS	podržet
i	i	k8xC	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
849	[number]	k4	849
495	[number]	k4	495
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
II	II	kA	II
<g/>
.	.	kIx.	.
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
úředně	úředně	k6eAd1	úředně
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
a	a	k8xC	a
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
ilegality	ilegalita	k1gFnSc2	ilegalita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
německo-sovětského	německoovětský	k2eAgInSc2d1	německo-sovětský
paktu	pakt	k1gInSc2	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
do	do	k7c2	do
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pokyny	pokyn	k1gInPc7	pokyn
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
oficiálně	oficiálně	k6eAd1	oficiálně
nezapojila	zapojit	k5eNaPmAgFnS	zapojit
do	do	k7c2	do
odboje	odboj	k1gInSc2	odboj
proti	proti	k7c3	proti
okupaci	okupace	k1gFnSc3	okupace
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc3	jeho
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
však	však	k9	však
stala	stát	k5eAaPmAgFnS	stát
významnou	významný	k2eAgFnSc7d1	významná
silou	síla	k1gFnSc7	síla
domácího	domácí	k2eAgInSc2d1	domácí
i	i	k8xC	i
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
prvopočátcích	prvopočátek	k1gInPc6	prvopočátek
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
komunističtí	komunistický	k2eAgMnPc1d1	komunistický
představitelé	představitel	k1gMnPc1	představitel
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Klementem	Klement	k1gMnSc7	Klement
Gottwaldem	Gottwald	k1gMnSc7	Gottwald
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dominantního	dominantní	k2eAgNnSc2d1	dominantní
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
prosazovat	prosazovat	k5eAaImF	prosazovat
obnovení	obnovení	k1gNnSc4	obnovení
poválečného	poválečný	k2eAgNnSc2d1	poválečné
Československa	Československo	k1gNnSc2	Československo
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
stalinského	stalinský	k2eAgNnSc2d1	stalinské
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
30	[number]	k4	30
000	[number]	k4	000
čs	čs	kA	čs
<g/>
.	.	kIx.	.
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
významná	významný	k2eAgFnSc1d1	významná
odbojová	odbojový	k2eAgFnSc1d1	odbojová
síla	síla	k1gFnSc1	síla
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
osvobozeného	osvobozený	k2eAgNnSc2d1	osvobozené
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
měla	mít	k5eAaImAgFnS	mít
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jako	jako	k9	jako
autonomní	autonomní	k2eAgFnSc1d1	autonomní
součást	součást	k1gFnSc1	součást
KSČ	KSČ	kA	KSČ
se	se	k3xPyFc4	se
formálně	formálně	k6eAd1	formálně
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
KSS	KSS	kA	KSS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
KSČ	KSČ	kA	KSČ
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
iniciativou	iniciativa	k1gFnSc7	iniciativa
ČSSD	ČSSD	kA	ČSSD
znárodnit	znárodnit	k5eAaPmF	znárodnit
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
nad	nad	k7c4	nad
50	[number]	k4	50
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
bankovnictví	bankovnictví	k1gNnSc4	bankovnictví
<g/>
.	.	kIx.	.
</s>
<s>
Neprotestovala	protestovat	k5eNaBmAgFnS	protestovat
proti	proti	k7c3	proti
záměru	záměr	k1gInSc3	záměr
presidenta	president	k1gMnSc2	president
Beneše	Beneš	k1gMnSc2	Beneš
neobnovit	obnovit	k5eNaPmF	obnovit
agrární	agrární	k2eAgFnSc4d1	agrární
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
vzorové	vzorový	k2eAgFnSc2d1	vzorová
Všesvazové	všesvazový	k2eAgFnSc2d1	Všesvazová
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
-	-	kIx~	-
bolševiků	bolševik	k1gMnPc2	bolševik
(	(	kIx(	(
<g/>
VKS	VKS	kA	VKS
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
))	))	k?	))
se	se	k3xPyFc4	se
připravovala	připravovat	k5eAaImAgFnS	připravovat
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
likvidaci	likvidace	k1gFnSc4	likvidace
demokracie	demokracie	k1gFnSc2	demokracie
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
jí	on	k3xPp3gFnSc3	on
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
vítězství	vítězství	k1gNnSc4	vítězství
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
1946	[number]	k4	1946
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
40	[number]	k4	40
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
;	;	kIx,	;
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
sesterská	sesterský	k2eAgFnSc1d1	sesterská
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
ziskem	zisk	k1gInSc7	zisk
přes	přes	k7c4	přes
30	[number]	k4	30
%	%	kIx~	%
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
od	od	k7c2	od
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
s	s	k7c7	s
62	[number]	k4	62
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
celostátním	celostátní	k2eAgNnSc6d1	celostátní
měřítku	měřítko	k1gNnSc6	měřítko
komunisté	komunista	k1gMnPc1	komunista
získali	získat	k5eAaPmAgMnP	získat
38	[number]	k4	38
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
postupném	postupný	k2eAgNnSc6d1	postupné
obsazování	obsazování	k1gNnSc6	obsazování
klíčových	klíčový	k2eAgInPc2d1	klíčový
postů	post	k1gInPc2	post
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
Sboru	sbor	k1gInSc6	sbor
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
komunisty	komunista	k1gMnPc4	komunista
či	či	k8xC	či
jejich	jejich	k3xOp3gMnPc4	jejich
sympatizanty	sympatizant	k1gMnPc4	sympatizant
a	a	k8xC	a
ve	v	k7c6	v
vytváření	vytváření	k1gNnSc6	vytváření
vlastních	vlastní	k2eAgInPc2d1	vlastní
právním	právní	k2eAgInSc7d1	právní
řádem	řád	k1gInSc7	řád
neupravených	upravený	k2eNgInPc2d1	neupravený
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
apolitických	apolitický	k2eAgFnPc2d1	apolitická
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
Lidových	lidový	k2eAgFnPc2d1	lidová
milicí	milice	k1gFnPc2	milice
<g/>
.	.	kIx.	.
</s>
<s>
KSČ	KSČ	kA	KSČ
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgInPc4	svůj
tajné	tajný	k2eAgInPc4d1	tajný
agenty	agens	k1gInPc4	agens
a	a	k8xC	a
sympatizanty	sympatizant	k1gMnPc4	sympatizant
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
ostatních	ostatní	k2eAgFnPc6d1	ostatní
politických	politický	k2eAgFnPc6d1	politická
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1947	[number]	k4	1947
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Gustáva	Gustáv	k1gMnSc2	Gustáv
Husáka	Husák	k1gMnSc2	Husák
prosadila	prosadit	k5eAaPmAgFnS	prosadit
částečnou	částečný	k2eAgFnSc4d1	částečná
likvidaci	likvidace	k1gFnSc4	likvidace
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
vedení	vedení	k1gNnSc1	vedení
údajně	údajně	k6eAd1	údajně
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
podpoře	podpora	k1gFnSc6	podpora
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
bývalého	bývalý	k2eAgInSc2d1	bývalý
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Převzít	převzít	k5eAaPmF	převzít
moc	moc	k6eAd1	moc
se	s	k7c7	s
KSČ	KSČ	kA	KSČ
podařilo	podařit	k5eAaPmAgNnS	podařit
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Vítězný	vítězný	k2eAgInSc1d1	vítězný
únor	únor	k1gInSc1	únor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jí	jíst	k5eAaImIp3nS	jíst
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
napomohla	napomoct	k5eAaPmAgFnS	napomoct
chybná	chybný	k2eAgFnSc1d1	chybná
taktika	taktika	k1gFnSc1	taktika
demokratických	demokratický	k2eAgFnPc2d1	demokratická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
sice	sice	k8xC	sice
uvědomily	uvědomit	k5eAaPmAgFnP	uvědomit
vážnost	vážnost	k1gFnSc1	vážnost
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
příliš	příliš	k6eAd1	příliš
spoléhaly	spoléhat	k5eAaImAgFnP	spoléhat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
republiky	republika	k1gFnSc2	republika
Edvarda	Edvard	k1gMnSc4	Edvard
Beneše	Beneš	k1gMnSc4	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
přijal	přijmout	k5eAaPmAgMnS	přijmout
veškeré	veškerý	k3xTgInPc4	veškerý
komunistické	komunistický	k2eAgInPc4d1	komunistický
požadavky	požadavek	k1gInPc4	požadavek
a	a	k8xC	a
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
rekonstruované	rekonstruovaný	k2eAgFnSc3d1	rekonstruovaná
Gottwaldově	Gottwaldův	k2eAgFnSc3d1	Gottwaldova
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
bez	bez	k1gInSc1	bez
pouhý	pouhý	k2eAgInSc1d1	pouhý
den	den	k1gInSc1	den
předtím	předtím	k6eAd1	předtím
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Jana	Jan	k1gMnSc2	Jan
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
důvěru	důvěra	k1gFnSc4	důvěra
všemi	všecek	k3xTgFnPc7	všecek
hlasy	hlas	k1gInPc4	hlas
230	[number]	k4	230
přítomných	přítomný	k2eAgMnPc2d1	přítomný
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
procházela	procházet	k5eAaImAgFnS	procházet
strana	strana	k1gFnSc1	strana
značnými	značný	k2eAgFnPc7d1	značná
změnami	změna	k1gFnPc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
KSČ	KSČ	kA	KSČ
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
a	a	k8xC	a
nabírala	nabírat	k5eAaImAgFnS	nabírat
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
stále	stále	k6eAd1	stále
nové	nový	k2eAgMnPc4d1	nový
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
počet	počet	k1gInSc1	počet
straníků	straník	k1gMnPc2	straník
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1948	[number]	k4	1948
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
se	s	k7c7	s
sociální	sociální	k2eAgFnSc7d1	sociální
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
sovětského	sovětský	k2eAgInSc2d1	sovětský
vzoru	vzor	k1gInSc2	vzor
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1948	[number]	k4	1948
provést	provést	k5eAaPmF	provést
plošnou	plošný	k2eAgFnSc4d1	plošná
prověrku	prověrka	k1gFnSc4	prověrka
členstva	členstvo	k1gNnSc2	členstvo
<g/>
.	.	kIx.	.
</s>
<s>
Prověrka	prověrka	k1gFnSc1	prověrka
měla	mít	k5eAaImAgFnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
lepší	dobrý	k2eAgFnSc1d2	lepší
třídní	třídní	k1gFnSc1	třídní
složení	složení	k1gNnSc2	složení
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
širokém	široký	k2eAgInSc6d1	široký
náboru	nábor	k1gInSc6	nábor
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
dostala	dostat	k5eAaPmAgFnS	dostat
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
členství	členství	k1gNnSc4	členství
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nutné	nutný	k2eAgNnSc4d1	nutné
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
další	další	k2eAgFnSc4d1	další
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
KSČ	KSČ	kA	KSČ
postupně	postupně	k6eAd1	postupně
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
svůj	svůj	k3xOyFgInSc4	svůj
proklamovaný	proklamovaný	k2eAgInSc4d1	proklamovaný
dělnický	dělnický	k2eAgInSc4d1	dělnický
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Prověrka	prověrka	k1gFnSc1	prověrka
měla	mít	k5eAaImAgFnS	mít
stranu	strana	k1gFnSc4	strana
zbavit	zbavit	k5eAaPmF	zbavit
také	také	k9	také
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
členy	člen	k1gInPc4	člen
jen	jen	k9	jen
nominálně	nominálně	k6eAd1	nominálně
<g/>
,	,	kIx,	,
pasivně	pasivně	k6eAd1	pasivně
<g/>
,	,	kIx,	,
lidí	člověk	k1gMnPc2	člověk
nehodných	hodný	k2eNgMnPc2d1	nehodný
být	být	k5eAaImF	být
členem	člen	k1gMnSc7	člen
pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Prověrka	prověrka	k1gFnSc1	prověrka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
během	během	k7c2	během
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
nepřinesla	přinést	k5eNaPmAgFnS	přinést
kýžený	kýžený	k2eAgInSc4d1	kýžený
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Vyloučených	vyloučený	k2eAgInPc2d1	vyloučený
či	či	k8xC	či
vyškrtnutých	vyškrtnutý	k2eAgInPc2d1	vyškrtnutý
bylo	být	k5eAaImAgNnS	být
zanedbatelné	zanedbatelný	k2eAgNnSc4d1	zanedbatelné
procento	procento	k1gNnSc4	procento
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
členové	člen	k1gMnPc1	člen
byli	být	k5eAaImAgMnP	být
prověřeni	prověřen	k2eAgMnPc1d1	prověřen
s	s	k7c7	s
lhůtou	lhůta	k1gFnSc7	lhůta
tří	tři	k4xCgInPc2	tři
nebo	nebo	k8xC	nebo
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
měli	mít	k5eAaImAgMnP	mít
prokázat	prokázat	k5eAaPmF	prokázat
svoje	svůj	k3xOyFgNnSc4	svůj
odhodlání	odhodlání	k1gNnSc4	odhodlání
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc4	schopnost
být	být	k5eAaImF	být
plnohodnotnými	plnohodnotný	k2eAgMnPc7d1	plnohodnotný
straníky	straník	k1gMnPc7	straník
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
národně	národně	k6eAd1	národně
socialistická	socialistický	k2eAgFnSc1d1	socialistická
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
Československou	československý	k2eAgFnSc4d1	Československá
stranu	strana	k1gFnSc4	strana
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
a	a	k8xC	a
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
straně	strana	k1gFnSc6	strana
lidové	lidový	k2eAgFnSc6d1	lidová
<g/>
,	,	kIx,	,
prosazeni	prosazen	k2eAgMnPc1d1	prosazen
lidé	člověk	k1gMnPc1	člověk
loajální	loajální	k2eAgMnSc1d1	loajální
politice	politika	k1gFnSc3	politika
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
byla	být	k5eAaImAgFnS	být
pohlcena	pohltit	k5eAaPmNgFnS	pohltit
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnPc3	její
členům	člen	k1gMnPc3	člen
byla	být	k5eAaImAgFnS	být
zaslána	zaslat	k5eAaPmNgFnS	zaslat
poštou	pošta	k1gFnSc7	pošta
komunistická	komunistický	k2eAgFnSc1d1	komunistická
legitimace	legitimace	k1gFnSc1	legitimace
a	a	k8xC	a
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
doživotnímu	doživotní	k2eAgNnSc3d1	doživotní
pronásledování	pronásledování	k1gNnSc3	pronásledování
nejen	nejen	k6eAd1	nejen
dotyčného	dotyčný	k2eAgMnSc2d1	dotyčný
sociálního	sociální	k2eAgMnSc2d1	sociální
demokrata	demokrat	k1gMnSc2	demokrat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
celé	celá	k1gFnSc2	celá
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
ČSS	ČSS	kA	ČSS
a	a	k8xC	a
ČSL	ČSL	kA	ČSL
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
40	[number]	k4	40
let	léto	k1gNnPc2	léto
staly	stát	k5eAaPmAgFnP	stát
vazaly	vazal	k1gMnPc7	vazal
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
i	i	k9	i
společenské	společenský	k2eAgFnPc1d1	společenská
organizace	organizace	k1gFnPc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
teroru	teror	k1gInSc2	teror
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
popraveno	popravit	k5eAaPmNgNnS	popravit
241	[number]	k4	241
lidí	člověk	k1gMnPc2	člověk
-	-	kIx~	-
obětí	oběť	k1gFnSc7	oběť
justičních	justiční	k2eAgFnPc2d1	justiční
vražd	vražda	k1gFnPc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
stovky	stovka	k1gFnPc1	stovka
lidí	člověk	k1gMnPc2	člověk
zemřely	zemřít	k5eAaPmAgFnP	zemřít
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
nebo	nebo	k8xC	nebo
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Oběťmi	oběť	k1gFnPc7	oběť
represí	represe	k1gFnPc2	represe
byli	být	k5eAaImAgMnP	být
členové	člen	k1gMnPc1	člen
západního	západní	k2eAgInSc2d1	západní
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
částečně	částečně	k6eAd1	částečně
i	i	k9	i
východního	východní	k2eAgInSc2d1	východní
<g/>
)	)	kIx)	)
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
podnikatelé	podnikatel	k1gMnPc1	podnikatel
<g/>
,	,	kIx,	,
soukromí	soukromý	k2eAgMnPc1d1	soukromý
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
živnostníci	živnostník	k1gMnPc1	živnostník
(	(	kIx(	(
<g/>
soukromníci	soukromník	k1gMnPc1	soukromník
<g/>
)	)	kIx)	)
a	a	k8xC	a
intelektuálové	intelektuál	k1gMnPc1	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
režimu	režim	k1gInSc2	režim
nepohodlných	pohodlný	k2eNgMnPc2d1	nepohodlný
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
vyhozeno	vyhodit	k5eAaPmNgNnS	vyhodit
ze	z	k7c2	z
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
nebo	nebo	k8xC	nebo
posláno	poslán	k2eAgNnSc1d1	Posláno
do	do	k7c2	do
faktického	faktický	k2eAgNnSc2d1	faktické
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
do	do	k7c2	do
pohraničí	pohraničí	k1gNnSc2	pohraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stalinské	stalinský	k2eAgFnSc2d1	stalinská
teze	teze	k1gFnSc2	teze
o	o	k7c4	o
zostřování	zostřování	k1gNnSc4	zostřování
třídního	třídní	k2eAgInSc2d1	třídní
boje	boj	k1gInSc2	boj
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
jako	jako	k9	jako
trest	trest	k1gInSc4	trest
za	za	k7c4	za
neúspěch	neúspěch	k1gInSc4	neúspěch
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
Izraele	Izrael	k1gInSc2	Izrael
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Slánského	Slánský	k1gMnSc2	Slánský
<g/>
.	.	kIx.	.
</s>
<s>
Reformní	reformní	k2eAgInSc4d1	reformní
rok	rok	k1gInSc4	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
prakticky	prakticky	k6eAd1	prakticky
neprojevil	projevit	k5eNaPmAgMnS	projevit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Čeští	český	k2eAgMnPc1d1	český
komunisté	komunista	k1gMnPc1	komunista
i	i	k9	i
nadále	nadále	k6eAd1	nadále
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
a	a	k8xC	a
perzekvovali	perzekvovat	k5eAaImAgMnP	perzekvovat
své	svůj	k3xOyFgMnPc4	svůj
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Popravovat	popravovat	k5eAaImF	popravovat
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
však	však	k9	však
přestal	přestat	k5eAaPmAgMnS	přestat
již	již	k6eAd1	již
prezident	prezident	k1gMnSc1	prezident
Antonín	Antonín	k1gMnSc1	Antonín
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
posledním	poslední	k2eAgNnSc6d1	poslední
takto	takto	k6eAd1	takto
popraveným	popravený	k2eAgMnSc7d1	popravený
byl	být	k5eAaImAgInS	být
Vladivoj	Vladivoj	k1gInSc4	Vladivoj
Tomek	tomka	k1gFnPc2	tomka
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pronásledování	pronásledování	k1gNnSc1	pronásledování
bylo	být	k5eAaImAgNnS	být
kontinuální	kontinuální	k2eAgNnSc1d1	kontinuální
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
a	a	k8xC	a
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
až	až	k9	až
do	do	k7c2	do
listopadových	listopadový	k2eAgFnPc2d1	listopadová
událostí	událost	k1gFnPc2	událost
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obecně	obecně	k6eAd1	obecně
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zmírnění	zmírnění	k1gNnSc3	zmírnění
a	a	k8xC	a
ani	ani	k8xC	ani
zpřísnění	zpřísnění	k1gNnSc1	zpřísnění
v	v	k7c6	v
době	doba	k1gFnSc6	doba
normalizace	normalizace	k1gFnSc2	normalizace
už	už	k6eAd1	už
nevrátilo	vrátit	k5eNaPmAgNnS	vrátit
situaci	situace	k1gFnSc4	situace
k	k	k7c3	k
hrůznému	hrůzný	k2eAgInSc3d1	hrůzný
teroru	teror	k1gInSc3	teror
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Pražské	pražský	k2eAgFnSc2d1	Pražská
jaro	jaro	k1gNnSc4	jaro
1968	[number]	k4	1968
a	a	k8xC	a
Invaze	invaze	k1gFnPc4	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Reforma	reforma	k1gFnSc1	reforma
KSČ	KSČ	kA	KSČ
zdola	zdola	k6eAd1	zdola
začala	začít	k5eAaPmAgFnS	začít
až	až	k9	až
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
za	za	k7c2	za
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Obrodný	obrodný	k2eAgInSc1d1	obrodný
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
i	i	k9	i
přes	přes	k7c4	přes
značné	značný	k2eAgNnSc4d1	značné
zmírnění	zmírnění	k1gNnSc4	zmírnění
situace	situace	k1gFnSc2	situace
nikdy	nikdy	k6eAd1	nikdy
neměl	mít	k5eNaImAgMnS	mít
ambice	ambice	k1gFnPc4	ambice
vrátit	vrátit	k5eAaPmF	vrátit
zemi	zem	k1gFnSc4	zem
demokracii	demokracie	k1gFnSc3	demokracie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
invazí	invaze	k1gFnSc7	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
KSČ	KSČ	kA	KSČ
vydala	vydat	k5eAaPmAgFnS	vydat
Provolání	provolání	k1gNnSc4	provolání
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
okupace	okupace	k1gFnSc1	okupace
a	a	k8xC	a
éru	éra	k1gFnSc4	éra
tzv.	tzv.	kA	tzv.
omezené	omezený	k2eAgFnSc2d1	omezená
suverenity	suverenita	k1gFnSc2	suverenita
státu	stát	k1gInSc2	stát
zcela	zcela	k6eAd1	zcela
ukončil	ukončit	k5eAaPmAgInS	ukončit
až	až	k9	až
odsun	odsun	k1gInSc4	odsun
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1969	[number]	k4	1969
bylo	být	k5eAaImAgNnS	být
reformistické	reformistický	k2eAgNnSc1d1	reformistické
vedení	vedení	k1gNnSc1	vedení
nahrazeno	nahrazen	k2eAgNnSc1d1	nahrazeno
centristy	centrista	k1gMnPc7	centrista
(	(	kIx(	(
<g/>
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
neostalinských	neostalinský	k2eAgMnPc2d1	neostalinský
ideologů	ideolog	k1gMnPc2	ideolog
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
Vasil	Vasil	k1gMnSc1	Vasil
Biľak	Biľak	k1gMnSc1	Biľak
<g/>
,	,	kIx,	,
posílili	posílit	k5eAaPmAgMnP	posílit
dogmatickou	dogmatický	k2eAgFnSc4d1	dogmatická
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
umírněnějšího	umírněný	k2eAgInSc2d2	umírněnější
stalinismu	stalinismus	k1gInSc2	stalinismus
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
brežněvizmu	brežněvizmus	k1gInSc2	brežněvizmus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
základem	základ	k1gInSc7	základ
byla	být	k5eAaImAgFnS	být
doktrína	doktrína	k1gFnSc1	doktrína
omezené	omezený	k2eAgFnSc2d1	omezená
suverenity	suverenita	k1gFnSc2	suverenita
zemí	zem	k1gFnPc2	zem
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
okupace	okupace	k1gFnSc1	okupace
byla	být	k5eAaImAgFnS	být
komunisty	komunista	k1gMnPc4	komunista
prezentována	prezentován	k2eAgFnSc1d1	prezentována
jako	jako	k8xC	jako
bratrská	bratrský	k2eAgFnSc1d1	bratrská
a	a	k8xC	a
internacionální	internacionální	k2eAgFnSc1d1	internacionální
pomoc	pomoc	k1gFnSc1	pomoc
a	a	k8xC	a
obrodný	obrodný	k2eAgInSc1d1	obrodný
proces	proces	k1gInSc1	proces
jako	jako	k8xS	jako
kontrarevoluce	kontrarevoluce	k1gFnSc1	kontrarevoluce
<g/>
.	.	kIx.	.
</s>
<s>
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
již	již	k6eAd1	již
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
odvrácení	odvrácení	k1gNnSc4	odvrácení
dosud	dosud	k6eAd1	dosud
nejvážnějšího	vážní	k2eAgNnSc2d3	nejvážnější
ohrožení	ohrožení	k1gNnSc2	ohrožení
mocenských	mocenský	k2eAgFnPc2d1	mocenská
pozic	pozice	k1gFnPc2	pozice
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
odstranění	odstranění	k1gNnSc1	odstranění
většiny	většina	k1gFnSc2	většina
svobod	svoboda	k1gFnPc2	svoboda
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
období	období	k1gNnSc2	období
postupného	postupný	k2eAgNnSc2d1	postupné
uvolňování	uvolňování	k1gNnSc3	uvolňování
v	v	k7c6	v
období	období	k1gNnSc6	období
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stranických	stranický	k2eAgFnPc2d1	stranická
čistek	čistka	k1gFnPc2	čistka
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
členů	člen	k1gInPc2	člen
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
či	či	k8xC	či
vyškrtnuta	vyškrtnout	k5eAaPmNgFnS	vyškrtnout
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Čistkami	čistka	k1gFnPc7	čistka
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
postiženo	postihnout	k5eAaPmNgNnS	postihnout
na	na	k7c4	na
600	[number]	k4	600
000	[number]	k4	000
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
KSČ	KSČ	kA	KSČ
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
změnila	změnit	k5eAaPmAgFnS	změnit
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
ideologií	ideologie	k1gFnSc7	ideologie
na	na	k7c4	na
organizaci	organizace	k1gFnSc4	organizace
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
okupační	okupační	k2eAgFnSc7d1	okupační
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
režim	režim	k1gInSc1	režim
značně	značně	k6eAd1	značně
přituhl	přituhnout	k5eAaPmAgInS	přituhnout
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
pronásledování	pronásledování	k1gNnSc1	pronásledování
signatářů	signatář	k1gMnPc2	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
provolání	provolání	k1gNnSc3	provolání
k	k	k7c3	k
československým	československý	k2eAgMnPc3d1	československý
představitelům	představitel	k1gMnPc3	představitel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
obsahující	obsahující	k2eAgInPc4d1	obsahující
výzvy	výzev	k1gInPc4	výzev
k	k	k7c3	k
dialogu	dialog	k1gInSc3	dialog
o	o	k7c6	o
demokratizaci	demokratizace	k1gFnSc6	demokratizace
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
novou	nový	k2eAgFnSc4d1	nová
vlnu	vlna	k1gFnSc4	vlna
represí	represe	k1gFnPc2	represe
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
akci	akce	k1gFnSc4	akce
StB	StB	k1gMnSc2	StB
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Asanace	asanace	k1gFnSc2	asanace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
perestrojku	perestrojka	k1gFnSc4	perestrojka
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
reagovala	reagovat	k5eAaBmAgFnS	reagovat
KSČ	KSČ	kA	KSČ
značně	značně	k6eAd1	značně
pasivně	pasivně	k6eAd1	pasivně
a	a	k8xC	a
až	až	k6eAd1	až
do	do	k7c2	do
demokratického	demokratický	k2eAgInSc2d1	demokratický
zvratu	zvrat	k1gInSc2	zvrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	s	k7c7	s
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
některých	některý	k3yIgFnPc2	některý
jiných	jiný	k2eAgFnPc2d1	jiná
"	"	kIx"	"
<g/>
bratrských	bratrský	k2eAgFnPc2d1	bratrská
stran	strana	k1gFnPc2	strana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
nezměnila	změnit	k5eNaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
listopadovým	listopadový	k2eAgInSc7d1	listopadový
převratem	převrat	k1gInSc7	převrat
měla	mít	k5eAaImAgFnS	mít
KSČ	KSČ	kA	KSČ
cca	cca	kA	cca
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
masovou	masový	k2eAgFnSc7d1	masová
všelidovou	všelidový	k2eAgFnSc7d1	všelidová
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
KSČ	KSČ	kA	KSČ
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
KSČS	KSČS	kA	KSČS
a	a	k8xC	a
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
přetvořila	přetvořit	k5eAaPmAgFnS	přetvořit
ve	v	k7c6	v
federaci	federace	k1gFnSc6	federace
nově	nově	k6eAd1	nově
založené	založený	k2eAgInPc1d1	založený
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
KSS	KSS	kA	KSS
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
druhá	druhý	k4xOgFnSc1	druhý
uvedená	uvedený	k2eAgFnSc1d1	uvedená
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Stranu	strana	k1gFnSc4	strana
demokratickej	demokratickej	k?	demokratickej
ľavice	ľavice	k1gFnSc1	ľavice
a	a	k8xC	a
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
před	před	k7c7	před
rozpadem	rozpad	k1gInSc7	rozpad
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Federální	federální	k2eAgFnSc1d1	federální
KSČS	KSČS	kA	KSČS
tak	tak	k8xC	tak
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Nástupnickou	nástupnický	k2eAgFnSc7d1	nástupnická
organizací	organizace	k1gFnSc7	organizace
KSČ	KSČ	kA	KSČ
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
podpora	podpora	k1gFnSc1	podpora
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c4	mezi
10	[number]	k4	10
%	%	kIx~	%
a	a	k8xC	a
22	[number]	k4	22
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
členskou	členský	k2eAgFnSc4d1	členská
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
zlomek	zlomek	k1gInSc4	zlomek
počtu	počet	k1gInSc2	počet
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
předlistopadovou	předlistopadový	k2eAgFnSc7d1	předlistopadová
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
KSČM	KSČM	kA	KSČM
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k9	i
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
ještě	ještě	k9	ještě
výrazněji	výrazně	k6eAd2	výrazně
stalinistickou	stalinistický	k2eAgFnSc4d1	stalinistická
ideologii	ideologie	k1gFnSc4	ideologie
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
okrajový	okrajový	k2eAgInSc1d1	okrajový
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
voleb	volba	k1gFnPc2	volba
neúčastní	účastnit	k5eNaImIp3nS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
antikomunistů	antikomunista	k1gMnPc2	antikomunista
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
hlásící	hlásící	k2eAgFnPc4d1	hlásící
se	se	k3xPyFc4	se
ke	k	k7c3	k
komunismu	komunismus	k1gInSc3	komunismus
zakázány	zakázán	k2eAgFnPc4d1	zakázána
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
neboť	neboť	k8xC	neboť
šíření	šíření	k1gNnSc4	šíření
komunistické	komunistický	k2eAgFnSc2d1	komunistická
ideologie	ideologie	k1gFnSc2	ideologie
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
trestné	trestný	k2eAgInPc4d1	trestný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
také	také	k9	také
senátní	senátní	k2eAgFnSc1d1	senátní
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
stanovy	stanova	k1gFnPc1	stanova
KSČM	KSČM	kA	KSČM
neporušují	porušovat	k5eNaImIp3nP	porušovat
zákony	zákon	k1gInPc1	zákon
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
vedl	vést	k5eAaImAgInS	vést
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
a	a	k8xC	a
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
generální	generální	k2eAgInSc1d1	generální
tajemník	tajemník	k1gInSc1	tajemník
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
pak	pak	k6eAd1	pak
první	první	k4xOgMnSc1	první
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
stranickým	stranický	k2eAgInSc7d1	stranický
orgánem	orgán	k1gInSc7	orgán
byl	být	k5eAaImAgInS	být
Ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
představitelé	představitel	k1gMnPc1	představitel
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Šturc	Šturc	k?	Šturc
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Alois	Alois	k1gMnSc1	Alois
Muna	muna	k1gFnSc1	muna
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Haken	Haken	k1gMnSc1	Haken
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Bohumil	Bohumil	k1gMnSc1	Bohumil
Jílek	Jílek	k1gMnSc1	Jílek
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
-	-	kIx~	-
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
-	-	kIx~	-
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
)	)	kIx)	)
Antonín	Antonín	k1gMnSc1	Antonín
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Alexander	Alexandra	k1gFnPc2	Alexandra
Dubček	Dubčko	k1gNnPc2	Dubčko
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Miloš	Miloš	k1gMnSc1	Miloš
Jakeš	Jakeš	k1gMnSc1	Jakeš
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Urbánek	Urbánek	k1gMnSc1	Urbánek
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g />
.	.	kIx.	.
</s>
<s>
Adamec	Adamec	k1gMnSc1	Adamec
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Ustavující	ustavující	k2eAgInSc1d1	ustavující
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1921	[number]	k4	1921
Slučovací	slučovací	k2eAgInSc1d1	slučovací
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1921	[number]	k4	1921
1	[number]	k4	1
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
až	až	k9	až
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1923	[number]	k4	1923
2	[number]	k4	2
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
1924	[number]	k4	1924
3	[number]	k4	3
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
1925	[number]	k4	1925
4	[number]	k4	4
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
1927	[number]	k4	1927
5	[number]	k4	5
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
1929	[number]	k4	1929
6	[number]	k4	6
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
1931	[number]	k4	1931
7	[number]	k4	7
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
1936	[number]	k4	1936
<g />
.	.	kIx.	.
</s>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
až	až	k9	až
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1946	[number]	k4	1946
(	(	kIx(	(
<g/>
konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
sále	sál	k1gInSc6	sál
Paláce	palác	k1gInSc2	palác
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
až	až	k9	až
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
10	[number]	k4	10
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1954	[number]	k4	1954
11	[number]	k4	11
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
až	až	k9	až
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1958	[number]	k4	1958
12	[number]	k4	12
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1962	[number]	k4	1962
13	[number]	k4	13
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
června	červen	k1gInSc2	červen
1966	[number]	k4	1966
14	[number]	k4	14
<g/>
.	.	kIx.	.
mimořádný	mimořádný	k2eAgInSc1d1	mimořádný
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
(	(	kIx(	(
<g/>
Vysočanský	vysočanský	k2eAgMnSc1d1	vysočanský
<g/>
)	)	kIx)	)
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
14	[number]	k4	14
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
až	až	k9	až
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1971	[number]	k4	1971
15	[number]	k4	15
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1976	[number]	k4	1976
16	[number]	k4	16
<g />
.	.	kIx.	.
</s>
<s>
<g/>
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
až	až	k9	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1981	[number]	k4	1981
17	[number]	k4	17
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
až	až	k9	až
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1986	[number]	k4	1986
18	[number]	k4	18
<g/>
.	.	kIx.	.
mimořádný	mimořádný	k2eAgInSc1d1	mimořádný
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
až	až	k9	až
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
18	[number]	k4	18
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČS	KSČS	kA	KSČS
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
Volební	volební	k2eAgInPc1d1	volební
výsledky	výsledek	k1gInPc1	výsledek
strany	strana	k1gFnSc2	strana
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
mandátů	mandát	k1gInPc2	mandát
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
celou	celá	k1gFnSc4	celá
ČSR	ČSR	kA	ČSR
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
ČSFR	ČSFR	kA	ČSFR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
mandátů	mandát	k1gInPc2	mandát
ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
se	se	k3xPyFc4	se
během	během	k7c2	během
existence	existence	k1gFnSc2	existence
strany	strana	k1gFnSc2	strana
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
a	a	k8xC	a
1986	[number]	k4	1986
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
strana	strana	k1gFnSc1	strana
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
jednotné	jednotný	k2eAgFnSc6d1	jednotná
kandidátce	kandidátka	k1gFnSc6	kandidátka
Národní	národní	k2eAgFnSc2d1	národní
Fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
ČSM	ČSM	kA	ČSM
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Socialistický	socialistický	k2eAgInSc1d1	socialistický
svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
SSM	SSM	kA	SSM
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Lidové	lidový	k2eAgFnSc2d1	lidová
milice	milice	k1gFnSc2	milice
-	-	kIx~	-
komunistická	komunistický	k2eAgFnSc1d1	komunistická
paramilitární	paramilitární	k2eAgFnSc1d1	paramilitární
organizace	organizace	k1gFnSc1	organizace
</s>
