<p>
<s>
Bolská	Bolská	k1gFnSc1	Bolská
provincie	provincie	k1gFnSc2	provincie
je	být	k5eAaImIp3nS	být
tureckou	turecký	k2eAgFnSc7d1	turecká
provincií	provincie	k1gFnSc7	provincie
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
provincie	provincie	k1gFnSc2	provincie
činí	činit	k5eAaImIp3nS	činit
10	[number]	k4	10
716	[number]	k4	716
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
265	[number]	k4	265
139	[number]	k4	139
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
provincie	provincie	k1gFnSc2	provincie
je	být	k5eAaImIp3nS	být
Bolu	bola	k1gFnSc4	bola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Bolská	Bolský	k2eAgFnSc1d1	Bolský
provincie	provincie	k1gFnSc1	provincie
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
9	[number]	k4	9
distriktů	distrikt	k1gInPc2	distrikt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bolu	bola	k1gFnSc4	bola
</s>
</p>
<p>
<s>
Dörtdivan	Dörtdivan	k1gMnSc1	Dörtdivan
</s>
</p>
<p>
<s>
Gerede	Geríst	k5eAaPmIp3nS	Geríst
</s>
</p>
<p>
<s>
Göynük	Göynük	k6eAd1	Göynük
</s>
</p>
<p>
<s>
Kı	Kı	k?	Kı
</s>
</p>
<p>
<s>
Mengen	Mengen	k1gInSc1	Mengen
</s>
</p>
<p>
<s>
Mudurnu	Mudurnout	k5eAaPmIp1nS	Mudurnout
</s>
</p>
<p>
<s>
Seben	Seben	k1gInSc1	Seben
</s>
</p>
<p>
<s>
Yeniçağ	Yeniçağ	k?	Yeniçağ
</s>
</p>
