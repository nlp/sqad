<s>
Obcí	obec	k1gFnSc7	obec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
Dyjsko-svrateckém	dyjskovratecký	k2eAgInSc6d1	dyjsko-svratecký
úvalu	úval	k1gInSc6	úval
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
potok	potok	k1gInSc4	potok
Syrůvka	syrůvka	k1gFnSc1	syrůvka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zde	zde	k6eAd1	zde
také	také	k9	také
pramení	pramenit	k5eAaImIp3nS	pramenit
a	a	k8xC	a
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
vodou	voda	k1gFnSc7	voda
zdejší	zdejší	k2eAgFnSc7d1	zdejší
rybník	rybník	k1gInSc4	rybník
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
