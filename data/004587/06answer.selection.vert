<s>
Buddhismus	buddhismus	k1gInSc1	buddhismus
dnes	dnes	k6eAd1	dnes
označuje	označovat	k5eAaImIp3nS	označovat
širokou	široký	k2eAgFnSc4d1	široká
rodinu	rodina	k1gFnSc4	rodina
filozofických	filozofický	k2eAgFnPc2d1	filozofická
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
<g/>
,	,	kIx,	,
založených	založený	k2eAgNnPc2d1	založené
na	na	k7c4	na
učení	učení	k1gNnSc4	učení
Siddhártha	Siddhárth	k1gMnSc2	Siddhárth
Gautamy	Gautam	k1gInPc4	Gautam
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
známého	známý	k1gMnSc4	známý
jako	jako	k8xC	jako
Buddha	Buddha	k1gMnSc1	Buddha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
probuzený	probuzený	k2eAgMnSc1d1	probuzený
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
