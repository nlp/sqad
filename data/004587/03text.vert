<s>
Buddhismus	buddhismus	k1gInSc1	buddhismus
dnes	dnes	k6eAd1	dnes
označuje	označovat	k5eAaImIp3nS	označovat
širokou	široký	k2eAgFnSc4d1	široká
rodinu	rodina	k1gFnSc4	rodina
filozofických	filozofický	k2eAgFnPc2d1	filozofická
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
<g/>
,	,	kIx,	,
založených	založený	k2eAgNnPc2d1	založené
na	na	k7c4	na
učení	učení	k1gNnSc4	učení
Siddhártha	Siddhárth	k1gMnSc2	Siddhárth
Gautamy	Gautam	k1gInPc4	Gautam
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
známého	známý	k1gMnSc4	známý
jako	jako	k8xC	jako
Buddha	Buddha	k1gMnSc1	Buddha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
probuzený	probuzený	k2eAgMnSc1d1	probuzený
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Buddha	Buddha	k1gMnSc1	Buddha
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
učil	učít	k5eAaPmAgMnS	učít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Indie	Indie	k1gFnSc2	Indie
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Buddhisté	buddhista	k1gMnPc1	buddhista
jej	on	k3xPp3gMnSc4	on
respektují	respektovat	k5eAaImIp3nP	respektovat
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc4	svůj
probuzeného	probuzený	k2eAgMnSc4d1	probuzený
či	či	k8xC	či
osvíceného	osvícený	k2eAgMnSc4d1	osvícený
učitele	učitel	k1gMnSc4	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
buddhismus	buddhismus	k1gInSc1	buddhismus
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stále	stále	k6eAd1	stále
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
školy	škola	k1gFnPc1	škola
můžete	moct	k5eAaImIp2nP	moct
dnes	dnes	k6eAd1	dnes
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
350	[number]	k4	350
milióny	milión	k4xCgInPc7	milión
až	až	k8xS	až
1,5	[number]	k4	1,5
miliardou	miliarda	k4xCgFnSc7	miliarda
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
nejčastěji	často	k6eAd3	často
akceptovaným	akceptovaný	k2eAgNnSc7d1	akceptované
rozpětím	rozpětí	k1gNnSc7	rozpětí
je	být	k5eAaImIp3nS	být
350	[number]	k4	350
-	-	kIx~	-
550	[number]	k4	550
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
Indii	Indie	k1gFnSc6	Indie
buddhismus	buddhismus	k1gInSc1	buddhismus
dnes	dnes	k6eAd1	dnes
téměř	téměř	k6eAd1	téměř
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Buddhismus	buddhismus	k1gInSc1	buddhismus
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
mylně	mylně	k6eAd1	mylně
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
neteistické	teistický	k2eNgNnSc1d1	neteistické
náboženství	náboženství	k1gNnSc1	náboženství
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
buddhistických	buddhistický	k2eAgInPc6d1	buddhistický
textech	text	k1gInPc6	text
objevují	objevovat	k5eAaImIp3nP	objevovat
"	"	kIx"	"
<g/>
božstva	božstvo	k1gNnSc2	božstvo
<g/>
"	"	kIx"	"
jakožto	jakožto	k8xS	jakožto
třída	třída	k1gFnSc1	třída
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
dimenzích	dimenze	k1gFnPc6	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
teistických	teistický	k2eAgNnPc2d1	teistické
náboženství	náboženství	k1gNnPc2	náboženství
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
tyto	tento	k3xDgFnPc1	tento
bytosti	bytost	k1gFnPc1	bytost
z	z	k7c2	z
třídy	třída	k1gFnSc2	třída
božstev	božstvo	k1gNnPc2	božstvo
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c2	za
smrtelné	smrtelný	k2eAgFnSc2d1	smrtelná
a	a	k8xC	a
nevědomé	vědomý	k2eNgFnSc2d1	nevědomá
bytosti	bytost	k1gFnSc2	bytost
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
bytosti	bytost	k1gFnPc1	bytost
z	z	k7c2	z
třídy	třída	k1gFnSc2	třída
lidí	člověk	k1gMnPc2	člověk
nebo	nebo	k8xC	nebo
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
je	on	k3xPp3gInPc4	on
převyšují	převyšovat	k5eAaImIp3nP	převyšovat
svými	svůj	k3xOyFgFnPc7	svůj
schopnostmi	schopnost	k1gFnPc7	schopnost
a	a	k8xC	a
dlouhověkostí	dlouhověkost	k1gFnSc7	dlouhověkost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
období	období	k1gNnSc2	období
Buddhova	Buddhův	k2eAgInSc2d1	Buddhův
života	život	k1gInSc2	život
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgFnPc1	žádný
písemné	písemný	k2eAgFnPc1d1	písemná
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
naše	náš	k3xOp1gFnPc1	náš
znalosti	znalost	k1gFnPc1	znalost
jeho	on	k3xPp3gNnSc2	on
učení	učení	k1gNnSc2	učení
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Tipitaky	Tipitak	k1gInPc1	Tipitak
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sice	sice	k8xC	sice
byla	být	k5eAaImAgNnP	být
sepsána	sepsat	k5eAaPmNgNnP	sepsat
až	až	k6eAd1	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
učení	učení	k1gNnSc1	učení
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgNnSc1d1	blízké
původnímu	původní	k2eAgNnSc3d1	původní
učení	učení	k1gNnSc3	učení
Buddhy	Buddha	k1gMnSc2	Buddha
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgInPc7d1	ostatní
duchovními	duchovní	k2eAgInPc7d1	duchovní
a	a	k8xC	a
filosofickými	filosofický	k2eAgInPc7d1	filosofický
systémy	systém	k1gInPc7	systém
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
buddhismus	buddhismus	k1gInSc1	buddhismus
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
místo	místo	k6eAd1	místo
postojem	postoj	k1gInSc7	postoj
k	k	k7c3	k
duši	duše	k1gFnSc3	duše
<g/>
.	.	kIx.	.
</s>
<s>
Nepatří	patřit	k5eNaImIp3nS	patřit
ani	ani	k9	ani
mezi	mezi	k7c7	mezi
<g/>
:	:	kIx,	:
eternalistickým	eternalistický	k2eAgInPc3d1	eternalistický
systémům	systém	k1gInPc3	systém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
i	i	k9	i
duše	duše	k1gFnSc1	duše
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Nope	nop	k1gInSc5	nop
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
k	k	k7c3	k
nihilistickým	nihilistický	k2eAgInPc3d1	nihilistický
systémům	systém	k1gInPc3	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
i	i	k9	i
duše	duše	k1gFnSc1	duše
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
zaniká	zanikat	k5eAaImIp3nS	zanikat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
žádnou	žádný	k3yNgFnSc4	žádný
entitu	entita	k1gFnSc4	entita
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
identifikovat	identifikovat	k5eAaBmF	identifikovat
jako	jako	k9	jako
s	s	k7c7	s
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anátman	anátman	k1gMnSc1	anátman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
tedy	tedy	k9	tedy
oba	dva	k4xCgInPc1	dva
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
přístupy	přístup	k1gInPc1	přístup
jsou	být	k5eAaImIp3nP	být
mylným	mylný	k2eAgInSc7d1	mylný
důsledkem	důsledek	k1gInSc7	důsledek
nevědomosti	nevědomost	k1gFnSc2	nevědomost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pochopení	pochopení	k1gNnSc3	pochopení
pojmu	pojem	k1gInSc2	pojem
buddhismus	buddhismus	k1gInSc1	buddhismus
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
přijali	přijmout	k5eAaPmAgMnP	přijmout
buddhisté	buddhista	k1gMnPc1	buddhista
teprve	teprve	k6eAd1	teprve
nedávno	nedávno	k6eAd1	nedávno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
théravádových	théravádový	k2eAgFnPc6d1	théravádový
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Buddhovo	Buddhův	k2eAgNnSc1d1	Buddhovo
učení	učení	k1gNnSc1	učení
<g/>
,	,	kIx,	,
nazýváno	nazývat	k5eAaImNgNnS	nazývat
jako	jako	k8xC	jako
sásana	sásana	k1gFnSc1	sásana
-	-	kIx~	-
učení	učení	k1gNnSc1	učení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
používán	používán	k2eAgInSc1d1	používán
termín	termín	k1gInSc1	termín
nangpä	nangpä	k?	nangpä
čhö	čhö	k?	čhö
-	-	kIx~	-
náboženství	náboženství	k1gNnPc2	náboženství
zasvěcených	zasvěcený	k2eAgMnPc2d1	zasvěcený
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
fo-ťiao	fo-ťiao	k6eAd1	fo-ťiao
-	-	kIx~	-
Buddhovo	Buddhův	k2eAgNnSc1d1	Buddhovo
učení	učení	k1gNnSc1	učení
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
bukkjó	bukkjó	k?	bukkjó
-	-	kIx~	-
Buddhovo	Buddhův	k2eAgNnSc1d1	Buddhovo
učení	učení	k1gNnSc1	učení
<g/>
,	,	kIx,	,
či	či	k8xC	či
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
bucudó	bucudó	k?	bucudó
-	-	kIx~	-
Buddhova	Buddhův	k2eAgFnSc1d1	Buddhova
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nebyl	být	k5eNaImAgMnS	být
mezi	mezi	k7c7	mezi
stoupenci	stoupenec	k1gMnPc7	stoupenec
různých	různý	k2eAgInPc2d1	různý
směrů	směr	k1gInPc2	směr
buddhismu	buddhismus	k1gInSc2	buddhismus
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
pocit	pocit	k1gInSc1	pocit
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
buddhismu	buddhismus	k1gInSc3	buddhismus
jako	jako	k8xC	jako
univerzálnímu	univerzální	k2eAgNnSc3d1	univerzální
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nesdílelo	sdílet	k5eNaImAgNnS	sdílet
ani	ani	k8xC	ani
jednotné	jednotný	k2eAgNnSc4d1	jednotné
označení	označení	k1gNnSc4	označení
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
slovo	slovo	k1gNnSc4	slovo
Boudhism	Boudhisma	k1gFnPc2	Boudhisma
v	v	k7c6	v
Oxfordském	oxfordský	k2eAgInSc6d1	oxfordský
slovníku	slovník	k1gInSc6	slovník
angličtiny	angličtina	k1gFnSc2	angličtina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
Buddhism	Buddhism	k1gInSc4	Buddhism
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
v	v	k7c6	v
článku	článek	k1gInSc6	článek
z	z	k7c2	z
časopisu	časopis	k1gInSc2	časopis
Asiatic	Asiatice	k1gFnPc2	Asiatice
Journal	Journal	k1gFnSc2	Journal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
první	první	k4xOgNnSc4	první
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
názvu	název	k1gInSc6	název
se	se	k3xPyFc4	se
vyskytovalo	vyskytovat	k5eAaImAgNnS	vyskytovat
slovo	slovo	k1gNnSc1	slovo
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
d	d	k?	d
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
kniha	kniha	k1gFnSc1	kniha
od	od	k7c2	od
Edwarda	Edward	k1gMnSc2	Edward
Uphama	Uphamum	k1gNnSc2	Uphamum
The	The	k1gFnSc2	The
History	Histor	k1gInPc4	Histor
and	and	k?	and
Doctrine	Doctrin	k1gInSc5	Doctrin
of	of	k?	of
Buddhism	Buddhism	k1gInSc1	Buddhism
(	(	kIx(	(
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
učení	učení	k1gNnSc1	učení
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
mniši	mnich	k1gMnPc1	mnich
různých	různý	k2eAgFnPc2d1	různá
buddhistických	buddhistický	k2eAgFnPc2d1	buddhistická
tradic	tradice	k1gFnPc2	tradice
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
jediném	jediný	k2eAgInSc6d1	jediný
buddhismu	buddhismus	k1gInSc6	buddhismus
jako	jako	k8xS	jako
univerzálním	univerzální	k2eAgNnSc6d1	univerzální
náboženství	náboženství	k1gNnSc6	náboženství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odvrátili	odvrátit	k5eAaPmAgMnP	odvrátit
útoky	útok	k1gInPc4	útok
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
misionářů	misionář	k1gMnPc2	misionář
a	a	k8xC	a
koloniálních	koloniální	k2eAgMnPc2d1	koloniální
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
paradoxní	paradoxní	k2eAgNnSc1d1	paradoxní
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůbec	vůbec	k9	vůbec
první	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c6	o
sjednocení	sjednocení	k1gNnSc6	sjednocení
buddhismu	buddhismus	k1gInSc2	buddhismus
učinil	učinit	k5eAaPmAgMnS	učinit
buddhista	buddhista	k1gMnSc1	buddhista
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
plukovník	plukovník	k1gMnSc1	plukovník
Henry	Henry	k1gMnSc1	Henry
Steel	Steel	k1gMnSc1	Steel
Olcott	Olcott	k1gMnSc1	Olcott
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pojetí	pojetí	k1gNnSc2	pojetí
školy	škola	k1gFnSc2	škola
théraváda	théraváda	k1gFnSc1	théraváda
<g/>
.	.	kIx.	.
</s>
<s>
Učení	učení	k1gNnSc4	učení
Buddhy	Buddha	k1gMnSc2	Buddha
bylo	být	k5eAaImAgNnS	být
motivováno	motivovat	k5eAaBmNgNnS	motivovat
úsilím	úsilí	k1gNnSc7	úsilí
o	o	k7c4	o
vysvobození	vysvobození	k1gNnSc4	vysvobození
bytostí	bytost	k1gFnPc2	bytost
z	z	k7c2	z
utrpení	utrpení	k1gNnSc2	utrpení
z	z	k7c2	z
nekonečného	konečný	k2eNgInSc2d1	nekonečný
koloběhu	koloběh	k1gInSc2	koloběh
zrození	zrození	k1gNnSc2	zrození
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
jeho	jeho	k3xOp3gMnPc2	jeho
rozhovorů	rozhovor	k1gInPc2	rozhovor
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
praxe	praxe	k1gFnSc1	praxe
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
osvobozuje	osvobozovat	k5eAaImIp3nS	osvobozovat
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
nebo	nebo	k8xC	nebo
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lepší	dobrý	k2eAgNnSc4d2	lepší
zrození	zrození	k1gNnSc4	zrození
či	či	k8xC	či
uvědomělý	uvědomělý	k2eAgInSc4d1	uvědomělý
<g/>
,	,	kIx,	,
blažený	blažený	k2eAgInSc4d1	blažený
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vysvobození	vysvobození	k1gNnSc4	vysvobození
z	z	k7c2	z
nevědomého	vědomý	k2eNgInSc2d1	nevědomý
egoismu	egoismus	k1gInSc2	egoismus
<g/>
:	:	kIx,	:
Jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
širý	širý	k2eAgInSc1d1	širý
oceán	oceán	k1gInSc1	oceán
proniknut	proniknut	k2eAgInSc1d1	proniknut
jen	jen	k6eAd1	jen
jedinou	jediný	k2eAgFnSc7d1	jediná
chutí	chuť	k1gFnSc7	chuť
<g/>
,	,	kIx,	,
chutí	chuť	k1gFnSc7	chuť
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
nauka	nauka	k1gFnSc1	nauka
proniknuta	proniknut	k2eAgFnSc1d1	proniknuta
jen	jen	k9	jen
jedinou	jediný	k2eAgFnSc7d1	jediná
chutí	chuť	k1gFnSc7	chuť
<g/>
,	,	kIx,	,
chutí	chuť	k1gFnSc7	chuť
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
.	.	kIx.	.
</s>
<s>
Buddha	Buddha	k1gMnSc1	Buddha
se	se	k3xPyFc4	se
distancoval	distancovat	k5eAaBmAgMnS	distancovat
od	od	k7c2	od
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
intelektuální	intelektuální	k2eAgFnPc4d1	intelektuální
teorie	teorie	k1gFnPc4	teorie
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
a	a	k8xC	a
podstatě	podstata	k1gFnSc6	podstata
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Srovnával	srovnávat	k5eAaImAgMnS	srovnávat
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
slepci	slepec	k1gMnPc7	slepec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
se	se	k3xPyFc4	se
každý	každý	k3xTgMnSc1	každý
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
slona	slon	k1gMnSc4	slon
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
prudkého	prudký	k2eAgInSc2d1	prudký
sporu	spor	k1gInSc2	spor
o	o	k7c6	o
pravém	pravý	k2eAgInSc6d1	pravý
tvaru	tvar	k1gInSc6	tvar
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
:	:	kIx,	:
Jeden	jeden	k4xCgInSc1	jeden
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
slona	slon	k1gMnSc4	slon
podle	podle	k7c2	podle
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
dotýkal	dotýkat	k5eAaImAgMnS	dotýkat
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
podle	podle	k7c2	podle
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
podle	podle	k7c2	podle
ucha	ucho	k1gNnSc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
podobenství	podobenství	k1gNnSc2	podobenství
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
interpretace	interpretace	k1gFnSc1	interpretace
světa	svět	k1gInSc2	svět
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
iluzorní	iluzorní	k2eAgMnSc1d1	iluzorní
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
promarněným	promarněný	k2eAgInSc7d1	promarněný
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
svých	svůj	k3xOyFgInPc2	svůj
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
zasažený	zasažený	k2eAgMnSc1d1	zasažený
otráveným	otrávený	k2eAgInSc7d1	otrávený
šípem	šíp	k1gInSc7	šíp
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
řekl	říct	k5eAaPmAgMnS	říct
lékaři	lékař	k1gMnSc3	lékař
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nenechám	nechat	k5eNaPmIp1nS	nechat
si	se	k3xPyFc3	se
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
šíp	šíp	k1gInSc4	šíp
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebudu	být	k5eNaImBp1nS	být
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
mě	já	k3xPp1nSc4	já
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
válečník	válečník	k1gMnSc1	válečník
<g/>
,	,	kIx,	,
bráhman	bráhman	k1gMnSc1	bráhman
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
nebo	nebo	k8xC	nebo
sluha	sluha	k1gMnSc1	sluha
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebudu	být	k5eNaImBp1nS	být
znát	znát	k5eAaImF	znát
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc4	příjmení
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
jaký	jaký	k3yRgInSc4	jaký
přesně	přesně	k6eAd1	přesně
je	být	k5eAaImIp3nS	být
hrot	hrot	k1gInSc1	hrot
šípu	šíp	k1gInSc2	šíp
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tento	tento	k3xDgMnSc1	tento
člověk	člověk	k1gMnSc1	člověk
by	by	kYmCp3nS	by
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
všechno	všechen	k3xTgNnSc1	všechen
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tak	tak	k6eAd1	tak
zemře	zemřít	k5eAaPmIp3nS	zemřít
v	v	k7c6	v
nevědomosti	nevědomost	k1gFnSc6	nevědomost
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
zanedbává	zanedbávat	k5eAaImIp3nS	zanedbávat
praxi	praxe	k1gFnSc4	praxe
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
k	k	k7c3	k
osvobození	osvobození	k1gNnSc3	osvobození
<g/>
,	,	kIx,	,
a	a	k8xC	a
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
intelektuálně	intelektuálně	k6eAd1	intelektuálně
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
o	o	k7c6	o
podstatě	podstata	k1gFnSc6	podstata
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Buddha	Buddha	k1gMnSc1	Buddha
neodpovídal	odpovídat	k5eNaImAgMnS	odpovídat
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
sahající	sahající	k2eAgInPc4d1	sahající
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
intelektuálního	intelektuální	k2eAgNnSc2d1	intelektuální
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
slepci	slepec	k1gMnPc1	slepec
nebo	nebo	k8xC	nebo
zranění	zraněný	k1gMnPc1	zraněný
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nechávají	nechávat	k5eAaImIp3nP	nechávat
svou	svůj	k3xOyFgFnSc4	svůj
záchranu	záchrana	k1gFnSc4	záchrana
záviset	záviset	k5eAaImF	záviset
na	na	k7c6	na
nesmyslných	smyslný	k2eNgFnPc6d1	nesmyslná
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
výroky	výrok	k1gInPc4	výrok
chápal	chápat	k5eAaImAgInS	chápat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc4	prostředek
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
probuzení	probuzení	k1gNnSc3	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
Srovnával	srovnávat	k5eAaImAgMnS	srovnávat
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
vorem	vor	k1gInSc7	vor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
převozu	převoz	k1gInSc3	převoz
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc1	žádný
smysl	smysl	k1gInSc1	smysl
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Nejkratší	krátký	k2eAgFnSc1d3	nejkratší
formulka	formulka	k1gFnSc1	formulka
Buddhovy	Buddhův	k2eAgFnSc2d1	Buddhova
filosofie	filosofie	k1gFnSc2	filosofie
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
třech	tři	k4xCgFnPc6	tři
charakteristikách	charakteristika	k1gFnPc6	charakteristika
všeho	všecek	k3xTgInSc2	všecek
existujícího	existující	k2eAgInSc2d1	existující
<g/>
;	;	kIx,	;
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
lze	lze	k6eAd1	lze
vyjít	vyjít	k5eAaPmF	vyjít
při	při	k7c6	při
nárysu	nárys	k1gInSc6	nárys
jeho	jeho	k3xOp3gInSc2	jeho
obrazu	obraz	k1gInSc2	obraz
světa	svět	k1gInSc2	svět
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
<g/>
:	:	kIx,	:
Všechny	všechen	k3xTgInPc1	všechen
jevy	jev	k1gInPc1	jev
jsou	být	k5eAaImIp3nP	být
pomíjivé	pomíjivý	k2eAgInPc1d1	pomíjivý
<g/>
,	,	kIx,	,
Všechny	všechen	k3xTgInPc1	všechen
jevy	jev	k1gInPc1	jev
přinášejí	přinášet	k5eAaImIp3nP	přinášet
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
,	,	kIx,	,
Veškeré	veškerý	k3xTgNnSc1	veškerý
bytí	bytí	k1gNnSc1	bytí
je	být	k5eAaImIp3nS	být
iluzorní	iluzorní	k2eAgMnSc1d1	iluzorní
jako	jako	k8xS	jako
sen	sen	k1gInSc1	sen
<g/>
.	.	kIx.	.
</s>
<s>
Buddha	Buddha	k1gMnSc1	Buddha
radil	radit	k5eAaImAgMnS	radit
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
hledají	hledat	k5eAaImIp3nP	hledat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nepřijímali	přijímat	k5eNaImAgMnP	přijímat
nic	nic	k3yNnSc1	nic
jen	jen	k6eAd1	jen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
slepé	slepý	k2eAgFnSc2d1	slepá
víry	víra	k1gFnSc2	víra
či	či	k8xC	či
autority	autorita	k1gFnSc2	autorita
druhého	druhý	k4xOgMnSc4	druhý
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
aby	aby	kYmCp3nP	aby
sami	sám	k3xTgMnPc1	sám
hledali	hledat	k5eAaImAgMnP	hledat
a	a	k8xC	a
usuzovali	usuzovat	k5eAaImAgMnP	usuzovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
daná	daný	k2eAgFnSc1d1	daná
věc	věc	k1gFnSc1	věc
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
nebo	nebo	k8xC	nebo
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
jejich	jejich	k3xOp3gFnPc4	jejich
nejniternější	niterní	k2eAgFnPc4d3	niterní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
všechny	všechen	k3xTgInPc1	všechen
jevy	jev	k1gInPc1	jev
jsou	být	k5eAaImIp3nP	být
iluzorní	iluzorní	k2eAgMnPc4d1	iluzorní
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
pomíjivé	pomíjivý	k2eAgInPc1d1	pomíjivý
jako	jako	k8xS	jako
sen	sen	k1gInSc1	sen
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Buddhu	Buddha	k1gMnSc4	Buddha
důležité	důležitý	k2eAgNnSc1d1	důležité
vymanit	vymanit	k5eAaPmF	vymanit
se	se	k3xPyFc4	se
z	z	k7c2	z
neustálého	neustálý	k2eAgInSc2d1	neustálý
koloběhu	koloběh	k1gInSc2	koloběh
iluzorní	iluzorní	k2eAgFnSc2d1	iluzorní
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
strastiplný	strastiplný	k2eAgMnSc1d1	strastiplný
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
stejně	stejně	k6eAd1	stejně
budeme	být	k5eAaImBp1nP	být
muset	muset	k5eAaImF	muset
jednoho	jeden	k4xCgMnSc4	jeden
dne	den	k1gInSc2	den
vše	všechen	k3xTgNnSc4	všechen
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Ukončení	ukončení	k1gNnSc1	ukončení
koloběhu	koloběh	k1gInSc2	koloběh
neustálého	neustálý	k2eAgNnSc2d1	neustálé
znovuzrozování	znovuzrozování	k1gNnSc2	znovuzrozování
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nirvána	nirvána	k1gFnSc1	nirvána
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
význam	význam	k1gInSc1	význam
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
neznamenal	znamenat	k5eNaImAgInS	znamenat
věčnou	věčný	k2eAgFnSc4d1	věčná
blaženost	blaženost	k1gFnSc4	blaženost
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
chápání	chápání	k1gNnSc2	chápání
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
člověk	člověk	k1gMnSc1	člověk
realizoval	realizovat	k5eAaBmAgMnS	realizovat
poznání	poznání	k1gNnSc4	poznání
neskutečnosti	neskutečnost	k1gFnSc2	neskutečnost
svého	svůj	k3xOyFgInSc2	svůj
iluzorního	iluzorní	k2eAgInSc2d1	iluzorní
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zbavil	zbavit	k5eAaPmAgMnS	zbavit
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinášelo	přinášet	k5eAaImAgNnS	přinášet
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
putování	putování	k1gNnSc4	putování
dualismem	dualismus	k1gInSc7	dualismus
samsáry	samsár	k1gMnPc4	samsár
(	(	kIx(	(
<g/>
=	=	kIx~	=
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nirvána	nirvána	k1gFnSc1	nirvána
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
konečným	konečný	k2eAgInSc7d1	konečný
cílem	cíl	k1gInSc7	cíl
všech	všecek	k3xTgMnPc2	všecek
buddhistů	buddhista	k1gMnPc2	buddhista
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
Buddha	Buddha	k1gMnSc1	Buddha
neučil	učít	k5eNaPmAgMnS	učít
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
stoupence	stoupenec	k1gMnPc4	stoupenec
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
okamžitého	okamžitý	k2eAgNnSc2d1	okamžité
vysvobození	vysvobození	k1gNnSc2	vysvobození
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
každý	každý	k3xTgMnSc1	každý
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
cestu	cesta	k1gFnSc4	cesta
připraven	připravit	k5eAaPmNgInS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
aspektům	aspekt	k1gInPc3	aspekt
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
buddhistických	buddhistický	k2eAgInPc6d1	buddhistický
textech	text	k1gInPc6	text
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
návody	návod	k1gInPc4	návod
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
štěstí	štěstí	k1gNnSc4	štěstí
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
co	co	k9	co
dělat	dělat	k5eAaImF	dělat
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
budoucí	budoucí	k2eAgNnSc4d1	budoucí
zrození	zrození	k1gNnSc4	zrození
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
chovat	chovat	k5eAaImF	chovat
k	k	k7c3	k
manželce	manželka	k1gFnSc3	manželka
či	či	k8xC	či
manželovi	manžel	k1gMnSc3	manžel
a	a	k8xC	a
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
šťastný	šťastný	k2eAgInSc4d1	šťastný
partnerský	partnerský	k2eAgInSc4d1	partnerský
život	život	k1gInSc4	život
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
i	i	k9	i
budoucím	budoucí	k2eAgInSc6d1	budoucí
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
zachovat	zachovat	k5eAaPmF	zachovat
dobré	dobrý	k2eAgFnPc4d1	dobrá
<g />
.	.	kIx.	.
</s>
<s>
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
udržet	udržet	k5eAaPmF	udržet
bohatství	bohatství	k1gNnSc4	bohatství
atd.	atd.	kA	atd.
<g/>
..	..	k?	..
Cestu	cesta	k1gFnSc4	cesta
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
k	k	k7c3	k
osvobození	osvobození	k1gNnSc3	osvobození
Buddha	Buddha	k1gMnSc1	Buddha
formuloval	formulovat	k5eAaImAgMnS	formulovat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
ušlechtilé	ušlechtilý	k2eAgFnSc6d1	ušlechtilá
osmidílné	osmidílný	k2eAgFnSc6d1	osmidílná
stezce	stezka	k1gFnSc6	stezka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tyto	tento	k3xDgInPc4	tento
způsoby	způsob	k1gInPc4	způsob
chování	chování	k1gNnSc2	chování
<g/>
:	:	kIx,	:
pravé	pravý	k2eAgNnSc1d1	pravé
pochopení	pochopení	k1gNnSc1	pochopení
<g/>
,	,	kIx,	,
pravé	pravý	k2eAgNnSc1d1	pravé
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
pravou	pravý	k2eAgFnSc4d1	pravá
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
pravé	pravý	k2eAgNnSc4d1	pravé
jednání	jednání	k1gNnSc4	jednání
<g/>
,	,	kIx,	,
pravý	pravý	k2eAgInSc4d1	pravý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
pravé	pravý	k2eAgNnSc4d1	pravé
úsilí	úsilí	k1gNnSc4	úsilí
<g/>
,	,	kIx,	,
pravou	pravý	k2eAgFnSc4d1	pravá
bdělou	bdělý	k2eAgFnSc4d1	bdělá
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
pravé	pravý	k2eAgNnSc4d1	pravé
soustředění	soustředění	k1gNnSc4	soustředění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
theravádových	theravádový	k2eAgFnPc2d1	theravádový
škol	škola	k1gFnPc2	škola
vede	vést	k5eAaImIp3nS	vést
osmidílná	osmidílný	k2eAgFnSc1d1	osmidílná
stezka	stezka	k1gFnSc1	stezka
dle	dle	k7c2	dle
ke	k	k7c3	k
vstoupení	vstoupení	k1gNnSc3	vstoupení
do	do	k7c2	do
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
k	k	k7c3	k
jedinému	jediný	k2eAgInSc3d1	jediný
návratu	návrat	k1gInSc3	návrat
<g/>
,	,	kIx,	,
k	k	k7c3	k
nenávratu	nenávrat	k1gInSc3	nenávrat
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
stavu	stav	k1gInSc3	stav
vysvobozeného	vysvobozený	k2eAgMnSc2d1	vysvobozený
arahata	arahat	k1gMnSc2	arahat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jisté	jistý	k2eAgFnSc6d1	jistá
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
vysvobození	vysvobození	k1gNnSc3	vysvobození
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
před	před	k7c7	před
zrozením	zrození	k1gNnSc7	zrození
ve	v	k7c6	v
světech	svět	k1gInPc6	svět
pod	pod	k7c7	pod
lidskou	lidský	k2eAgFnSc7d1	lidská
úrovní	úroveň	k1gFnSc7	úroveň
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
světech	svět	k1gInPc6	svět
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
omezeném	omezený	k2eAgInSc6d1	omezený
počtu	počet	k1gInSc6	počet
znovuzrození	znovuzrození	k1gNnSc1	znovuzrození
bude	být	k5eAaImBp3nS	být
dokonale	dokonale	k6eAd1	dokonale
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
od	od	k7c2	od
žádosti	žádost	k1gFnSc2	žádost
<g/>
,	,	kIx,	,
nenávisti	nenávist	k1gFnSc2	nenávist
a	a	k8xC	a
zaslepení	zaslepení	k1gNnSc2	zaslepení
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
již	již	k6eAd1	již
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc4	jeden
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
pozemský	pozemský	k2eAgInSc4d1	pozemský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevrátí	vrátit	k5eNaPmIp3nP	vrátit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
světů	svět	k1gInPc2	svět
zvaných	zvaný	k2eAgInPc2d1	zvaný
"	"	kIx"	"
<g/>
čistá	čistý	k2eAgNnPc1d1	čisté
území	území	k1gNnPc1	území
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
nirvánu	nirvána	k1gFnSc4	nirvána
<g/>
.	.	kIx.	.
</s>
<s>
Arahat	Arahat	k5eAaImF	Arahat
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
tohoto	tento	k3xDgInSc2	tento
cíle	cíl	k1gInSc2	cíl
již	již	k6eAd1	již
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
různé	různý	k2eAgInPc1d1	různý
stupně	stupeň	k1gInPc1	stupeň
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podstatně	podstatně	k6eAd1	podstatně
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
Buddhovy	Buddhův	k2eAgFnSc2d1	Buddhova
nauky	nauka	k1gFnSc2	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
arahatem	arahat	k1gInSc7	arahat
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
přijmout	přijmout	k5eAaPmF	přijmout
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
mnicha	mnich	k1gMnSc2	mnich
(	(	kIx(	(
<g/>
bhikšu	bhiksat	k5eAaPmIp1nS	bhiksat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
Buddha	Buddha	k1gMnSc1	Buddha
vzdal	vzdát	k5eAaPmAgMnS	vzdát
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
stoupenci	stoupenec	k1gMnPc1	stoupenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
vázáni	vázat	k5eAaImNgMnP	vázat
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
vztahy	vztah	k1gInPc1	vztah
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
například	například	k6eAd1	například
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
vstoupení	vstoupení	k1gNnSc4	vstoupení
do	do	k7c2	do
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
a	a	k8xC	a
být	být	k5eAaImF	být
si	se	k3xPyFc3	se
tak	tak	k9	tak
jisti	jist	k2eAgMnPc1d1	jist
budoucím	budoucí	k2eAgNnSc7d1	budoucí
vysvobozením	vysvobození	k1gNnSc7	vysvobození
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
buddhismu	buddhismus	k1gInSc2	buddhismus
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
školy	škola	k1gFnSc2	škola
nabízející	nabízející	k2eAgInSc1d1	nabízející
způsob	způsob	k1gInSc1	způsob
výcviku	výcvik	k1gInSc2	výcvik
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zaručoval	zaručovat	k5eAaImAgInS	zaručovat
možnost	možnost	k1gFnSc4	možnost
duchovního	duchovní	k2eAgInSc2d1	duchovní
pokroku	pokrok	k1gInSc2	pokrok
i	i	k8xC	i
laikům	laik	k1gMnPc3	laik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
rodinným	rodinný	k2eAgInSc7d1	rodinný
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Ušlechtilá	ušlechtilý	k2eAgFnSc1d1	ušlechtilá
osmidílná	osmidílný	k2eAgFnSc1d1	osmidílná
stezka	stezka	k1gFnSc1	stezka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
konečnému	konečný	k2eAgNnSc3d1	konečné
vysvobození	vysvobození	k1gNnSc3	vysvobození
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
poslední	poslední	k2eAgNnPc1d1	poslední
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Čtyř	čtyři	k4xCgFnPc2	čtyři
vznešených	vznešený	k2eAgFnPc2d1	vznešená
pravd	pravda	k1gFnPc2	pravda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
základ	základ	k1gInSc4	základ
filosofie	filosofie	k1gFnSc2	filosofie
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
vznešené	vznešený	k2eAgFnPc1d1	vznešená
pravdy	pravda	k1gFnPc1	pravda
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Existuje	existovat	k5eAaImIp3nS	existovat
utrpení	utrpení	k1gNnSc1	utrpení
Existuje	existovat	k5eAaImIp3nS	existovat
příčina	příčina	k1gFnSc1	příčina
utrpení	utrpení	k1gNnSc2	utrpení
Utrpení	utrpení	k1gNnSc2	utrpení
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ukončit	ukončit	k5eAaPmF	ukončit
Existuje	existovat	k5eAaImIp3nS	existovat
cesta	cesta	k1gFnSc1	cesta
vedoucí	vedoucí	k1gFnSc1	vedoucí
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
utrpení	utrpení	k1gNnSc2	utrpení
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
Ušlechtilá	ušlechtilý	k2eAgFnSc1d1	ušlechtilá
osmidílná	osmidílný	k2eAgFnSc1d1	osmidílná
stezka	stezka	k1gFnSc1	stezka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautamum	k1gNnPc4	Gautamum
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
stoupence	stoupenec	k1gMnPc4	stoupenec
pět	pět	k4xCc1	pět
základních	základní	k2eAgNnPc2d1	základní
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mnichy	mnich	k1gMnPc4	mnich
pak	pak	k6eAd1	pak
další	další	k2eAgNnPc1d1	další
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
počtem	počet	k1gInSc7	počet
i	i	k8xC	i
zněním	znění	k1gNnSc7	znění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
školách	škola	k1gFnPc6	škola
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
stoupence	stoupenec	k1gMnPc4	stoupenec
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
<g/>
:	:	kIx,	:
zdržet	zdržet	k5eAaPmF	zdržet
se	se	k3xPyFc4	se
zabíjení	zabíjení	k1gNnSc1	zabíjení
a	a	k8xC	a
zraňování	zraňování	k1gNnSc1	zraňování
živých	živý	k2eAgFnPc2d1	živá
bytostí	bytost	k1gFnPc2	bytost
zdržet	zdržet	k5eAaPmF	zdržet
se	se	k3xPyFc4	se
braní	braní	k1gNnSc2	braní
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nejsou	být	k5eNaImIp3nP	být
dávány	dáván	k2eAgInPc1d1	dáván
(	(	kIx(	(
<g/>
nekrást	krást	k5eNaImF	krást
<g/>
)	)	kIx)	)
zdržet	zdržet	k5eAaPmF	zdržet
se	se	k3xPyFc4	se
nesprávného	správný	k2eNgNnSc2d1	nesprávné
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
chování	chování	k1gNnSc2	chování
zdržet	zdržet	k5eAaPmF	zdržet
se	s	k7c7	s
zraňující	zraňující	k2eAgFnSc7d1	zraňující
a	a	k8xC	a
nepravdivé	pravdivý	k2eNgInPc4d1	nepravdivý
mluvy	mluv	k1gInPc4	mluv
(	(	kIx(	(
<g/>
nelhat	lhat	k5eNaPmF	lhat
<g/>
,	,	kIx,	,
nepomlouvat	pomlouvat	k5eNaImF	pomlouvat
<g/>
,	,	kIx,	,
nemluvit	mluvit	k5eNaImF	mluvit
zbytečně	zbytečně	k6eAd1	zbytečně
<g/>
<g />
.	.	kIx.	.
</s>
<s>
...	...	k?	...
<g/>
)	)	kIx)	)
zdržet	zdržet	k5eAaPmF	zdržet
se	se	k3xPyFc4	se
zneužívání	zneužívání	k1gNnSc2	zneužívání
omamných	omamný	k2eAgInPc2d1	omamný
prostředků	prostředek	k1gInPc2	prostředek
Formulace	formulace	k1gFnSc2	formulace
i	i	k8xC	i
počet	počet	k1gInSc1	počet
základních	základní	k2eAgNnPc2d1	základní
pravidel	pravidlo	k1gNnPc2	pravidlo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
školách	škola	k1gFnPc6	škola
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rozvedení	rozvedení	k1gNnSc4	rozvedení
nebo	nebo	k8xC	nebo
přizpůsobení	přizpůsobení	k1gNnSc4	přizpůsobení
novým	nový	k2eAgFnPc3d1	nová
podmínkám	podmínka	k1gFnPc3	podmínka
výše	výše	k1gFnSc2	výše
vyjmenovaných	vyjmenovaný	k2eAgFnPc2d1	vyjmenovaná
základních	základní	k2eAgFnPc2d1	základní
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
těchto	tento	k3xDgNnPc2	tento
pět	pět	k4xCc1	pět
pravidel	pravidlo	k1gNnPc2	pravidlo
tvoří	tvořit	k5eAaImIp3nS	tvořit
jakési	jakýsi	k3yIgFnSc2	jakýsi
základní	základní	k2eAgFnSc2d1	základní
životní	životní	k2eAgFnSc2d1	životní
zásady	zásada	k1gFnSc2	zásada
každého	každý	k3xTgMnSc2	každý
buddhisty	buddhista	k1gMnSc2	buddhista
<g/>
.	.	kIx.	.
</s>
<s>
Členění	členění	k1gNnSc1	členění
na	na	k7c4	na
hínajánu	hínajána	k1gFnSc4	hínajána
<g/>
,	,	kIx,	,
mahájánu	mahájána	k1gFnSc4	mahájána
a	a	k8xC	a
vadžrajánu	vadžrajána	k1gFnSc4	vadžrajána
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
tradic	tradice	k1gFnPc2	tradice
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
buddhistická	buddhistický	k2eAgFnSc1d1	buddhistická
škola	škola	k1gFnSc1	škola
théraváda	théraváda	k1gFnSc1	théraváda
toto	tento	k3xDgNnSc4	tento
dělení	dělení	k1gNnSc4	dělení
nezná	znát	k5eNaImIp3nS	znát
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Théraváda	Théraváda	k1gFnSc1	Théraváda
<g/>
.	.	kIx.	.
</s>
<s>
Théraváda	Théraváda	k1gFnSc1	Théraváda
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
nauka	nauka	k1gFnSc1	nauka
starších	starší	k1gMnPc2	starší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
buddhistickou	buddhistický	k2eAgFnSc7d1	buddhistická
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Buddhovu	Buddhův	k2eAgNnSc3d1	Buddhovo
otočení	otočení	k1gNnSc3	otočení
kola	kolo	k1gNnSc2	kolo
dharmy	dharma	k1gFnSc2	dharma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
Buddhovy	Buddhův	k2eAgFnPc4d1	Buddhova
rozpravy	rozprava	k1gFnPc4	rozprava
zachované	zachovaný	k2eAgFnPc4d1	zachovaná
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
páli	pál	k1gFnSc2	pál
<g/>
.	.	kIx.	.
</s>
<s>
Théraváda	Théraváda	k1gFnSc1	Théraváda
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
starších	starý	k2eAgFnPc2d2	starší
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Théravádový	Théravádový	k2eAgInSc1d1	Théravádový
buddhismus	buddhismus	k1gInSc1	buddhismus
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
především	především	k9	především
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
<g/>
,	,	kIx,	,
Barmy	Barma	k1gFnSc2	Barma
<g/>
,	,	kIx,	,
Laosu	Laos	k1gInSc2	Laos
<g/>
,	,	kIx,	,
Thajska	Thajsko	k1gNnSc2	Thajsko
a	a	k8xC	a
částí	část	k1gFnPc2	část
Vietnamu	Vietnam	k1gInSc2	Vietnam
a	a	k8xC	a
Malajsie	Malajsie	k1gFnSc2	Malajsie
<g/>
.	.	kIx.	.
</s>
<s>
Mahájána	Mahájána	k1gFnSc1	Mahájána
a	a	k8xC	a
vadžrajána	vadžrajána	k1gFnSc1	vadžrajána
označují	označovat	k5eAaImIp3nP	označovat
někdy	někdy	k6eAd1	někdy
théravádu	théraváda	k1gFnSc4	théraváda
jako	jako	k8xS	jako
hínajánu	hínajána	k1gFnSc4	hínajána
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
problematicky	problematicky	k6eAd1	problematicky
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
hínajána	hínaján	k1gMnSc2	hínaján
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
na	na	k7c6	na
buddhistickém	buddhistický	k2eAgInSc6d1	buddhistický
koncilu	koncil	k1gInSc6	koncil
svolaném	svolaný	k2eAgInSc6d1	svolaný
indickým	indický	k2eAgInPc3d1	indický
králem	král	k1gMnSc7	král
Kaniškou	Kaniška	k1gFnSc7	Kaniška
v	v	k7c6	v
prvé	prvý	k4xOgFnSc6	prvý
polovině	polovina	k1gFnSc6	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
však	však	k9	však
théraváda	théraváda	k1gFnSc1	théraváda
už	už	k6eAd1	už
na	na	k7c6	na
indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
nepůsobila	působit	k5eNaImAgFnS	působit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
théravádoví	théravádový	k2eAgMnPc1d1	théravádový
mniši	mnich	k1gMnPc1	mnich
se	se	k3xPyFc4	se
nemohli	moct	k5eNaImAgMnP	moct
koncilu	koncil	k1gInSc2	koncil
účastnit	účastnit	k5eAaImF	účastnit
a	a	k8xC	a
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
označeni	označen	k2eAgMnPc1d1	označen
za	za	k7c4	za
stoupence	stoupenec	k1gMnPc4	stoupenec
hínajány	hínajána	k1gFnSc2	hínajána
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
nauky	nauka	k1gFnSc2	nauka
je	být	k5eAaImIp3nS	být
řazení	řazení	k1gNnSc1	řazení
théravády	théraváda	k1gFnSc2	théraváda
do	do	k7c2	do
hínajány	hínajána	k1gFnSc2	hínajána
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
théraváda	théraváda	k1gFnSc1	théraváda
uznává	uznávat	k5eAaImIp3nS	uznávat
tzv.	tzv.	kA	tzv.
cestu	cesta	k1gFnSc4	cesta
bódhisattvy	bódhisattva	k1gFnSc2	bódhisattva
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
stoupenci	stoupenec	k1gMnPc1	stoupenec
théravády	théraváda	k1gFnSc2	théraváda
někdy	někdy	k6eAd1	někdy
skládají	skládat	k5eAaImIp3nP	skládat
slib	slib	k1gInSc4	slib
bódhisattvy	bódhisattva	k1gFnSc2	bódhisattva
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hinajána	Hinajána	k1gFnSc1	Hinajána
<g/>
.	.	kIx.	.
</s>
<s>
Hínajána	Hínajána	k1gFnSc1	Hínajána
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
malý	malý	k2eAgInSc1d1	malý
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
posměšné	posměšný	k2eAgNnSc1d1	posměšné
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
v	v	k7c6	v
mahájánových	mahájánův	k2eAgInPc6d1	mahájánův
textech	text	k1gInPc6	text
označovat	označovat	k5eAaImF	označovat
stoupenci	stoupenec	k1gMnPc1	stoupenec
tzv.	tzv.	kA	tzv.
starších	starý	k2eAgFnPc2d2	starší
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
údajně	údajně	k6eAd1	údajně
usilovat	usilovat	k5eAaImF	usilovat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
osvícení	osvícení	k1gNnSc4	osvícení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
existuje	existovat	k5eAaImIp3nS	existovat
jediná	jediný	k2eAgFnSc1d1	jediná
hínajánová	hínajánový	k2eAgFnSc1d1	hínajánový
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
théraváda	théraváda	k1gFnSc1	théraváda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vadžrajánových	vadžrajánův	k2eAgInPc2d1	vadžrajánův
textů	text	k1gInPc2	text
je	být	k5eAaImIp3nS	být
hínajána	hínajána	k1gFnSc1	hínajána
původní	původní	k2eAgFnSc1d1	původní
sútrické	sútrický	k2eAgNnSc4d1	sútrický
učení	učení	k1gNnSc4	učení
Buddhy	Buddha	k1gMnSc2	Buddha
o	o	k7c4	o
4	[number]	k4	4
vznešených	vznešený	k2eAgFnPc6d1	vznešená
pravdách	pravda	k1gFnPc6	pravda
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
především	především	k9	především
na	na	k7c6	na
morální	morální	k2eAgFnSc6d1	morální
kázni	kázeň	k1gFnSc6	kázeň
-	-	kIx~	-
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
jakémukoliv	jakýkoliv	k3yIgNnSc3	jakýkoliv
ublížení	ublížení	k1gNnSc4	ublížení
jiné	jiný	k2eAgFnSc2d1	jiná
bytosti	bytost	k1gFnSc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vadžrajány	vadžrajána	k1gFnSc2	vadžrajána
stoupenci	stoupenec	k1gMnPc1	stoupenec
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
jsou	být	k5eAaImIp3nP	být
vázáni	vázat	k5eAaImNgMnP	vázat
slibem	slib	k1gInSc7	slib
zabránit	zabránit	k5eAaPmF	zabránit
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
negativní	negativní	k2eAgFnPc4d1	negativní
akci	akce	k1gFnSc4	akce
způsobené	způsobený	k2eAgFnPc1d1	způsobená
ať	ať	k8xS	ať
už	už	k6eAd1	už
tělesně	tělesně	k6eAd1	tělesně
<g/>
,	,	kIx,	,
energií	energie	k1gFnSc7	energie
nebo	nebo	k8xC	nebo
i	i	k9	i
jen	jen	k9	jen
myšlenkou	myšlenka	k1gFnSc7	myšlenka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
uvést	uvést	k5eAaPmF	uvést
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
učení	učení	k1gNnSc2	učení
o	o	k7c6	o
Pravdě	pravda	k1gFnSc6	pravda
o	o	k7c4	o
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Hinajána	Hinajána	k1gFnSc1	Hinajána
se	se	k3xPyFc4	se
vnitřně	vnitřně	k6eAd1	vnitřně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
učení	učení	k1gNnSc4	učení
Šravaků	Šravak	k1gInPc2	Šravak
a	a	k8xC	a
učení	učení	k1gNnSc2	učení
Pratjéka	Pratjéek	k1gInSc2	Pratjéek
Buddhů	Buddha	k1gMnPc2	Buddha
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mahájána	Mahájána	k1gFnSc1	Mahájána
<g/>
.	.	kIx.	.
</s>
<s>
Mahájána	Mahájána	k1gFnSc1	Mahájána
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
velký	velký	k2eAgInSc4d1	velký
vůz	vůz	k1gInSc4	vůz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
učení	učení	k1gNnSc1	učení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Buddhovu	Buddhův	k2eAgNnSc3d1	Buddhovo
otočení	otočení	k1gNnSc3	otočení
kola	kolo	k1gNnSc2	kolo
dharmy	dharma	k1gFnSc2	dharma
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
mahájánových	mahájánův	k2eAgFnPc2d1	mahájánův
škol	škola	k1gFnPc2	škola
i	i	k9	i
po	po	k7c4	po
3	[number]	k4	3
<g/>
.	.	kIx.	.
otočením	otočení	k1gNnSc7	otočení
kola	kolo	k1gNnSc2	kolo
dharmy	dharma	k1gFnSc2	dharma
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
legendy	legenda	k1gFnSc2	legenda
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
tak	tak	k6eAd1	tak
hluboké	hluboký	k2eAgNnSc4d1	hluboké
učení	učení	k1gNnSc4	učení
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohlo	moct	k5eNaImAgNnS	moct
být	být	k5eAaImF	být
pochopeno	pochopit	k5eAaPmNgNnS	pochopit
v	v	k7c6	v
Buddhově	Buddhův	k2eAgFnSc6d1	Buddhova
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
uchováno	uchovat	k5eAaPmNgNnS	uchovat
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
nágů	nág	k1gInPc2	nág
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jej	on	k3xPp3gMnSc4	on
později	pozdě	k6eAd2	pozdě
Nágárdžuna	Nágárdžuna	k1gFnSc1	Nágárdžuna
vynesl	vynést	k5eAaPmAgInS	vynést
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
většiny	většina	k1gFnSc2	většina
buddhistických	buddhistický	k2eAgMnPc2d1	buddhistický
badatelů	badatel	k1gMnPc2	badatel
začala	začít	k5eAaPmAgFnS	začít
mahájána	mahájána	k1gFnSc1	mahájána
vznikat	vznikat	k5eAaImF	vznikat
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Mahájána	Mahájána	k1gFnSc1	Mahájána
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
ideál	ideál	k1gInSc4	ideál
bódhisattvy	bódhisattva	k1gFnSc2	bódhisattva
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
ve	v	k7c6	v
starších	starý	k2eAgFnPc6d2	starší
školách	škola	k1gFnPc6	škola
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
všeobecně	všeobecně	k6eAd1	všeobecně
dosažitelný	dosažitelný	k2eAgInSc4d1	dosažitelný
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
vydat	vydat	k5eAaPmF	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
bódhisattvy	bódhisattva	k1gFnSc2	bódhisattva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
možnosti	možnost	k1gFnSc3	možnost
realizace	realizace	k1gFnSc2	realizace
probuzení	probuzení	k1gNnSc2	probuzení
podle	podle	k7c2	podle
théravádoveho	théravádoveze	k6eAd1	théravádoveze
pojetí	pojetí	k1gNnSc2	pojetí
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
směřující	směřující	k2eAgFnSc4d1	směřující
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
tzv.	tzv.	kA	tzv.
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
dokonalého	dokonalý	k2eAgNnSc2d1	dokonalé
probuzení	probuzení	k1gNnSc2	probuzení
(	(	kIx(	(
<g/>
sa	sa	k?	sa
samjak-sambódhi	samjakambódh	k1gFnSc2	samjak-sambódh
<g/>
)	)	kIx)	)
-	-	kIx~	-
probuzení	probuzení	k1gNnSc1	probuzení
dokonalého	dokonalý	k2eAgMnSc2d1	dokonalý
buddhy	buddha	k1gMnSc2	buddha
(	(	kIx(	(
<g/>
sa	sa	k?	sa
samjak-sambuddha	samjakambuddha	k1gFnSc1	samjak-sambuddha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
od	od	k7c2	od
kompletního	kompletní	k2eAgInSc2d1	kompletní
rozvoje	rozvoj	k1gInSc2	rozvoj
cesty	cesta	k1gFnSc2	cesta
nahromadění	nahromadění	k1gNnSc2	nahromadění
i	i	k9	i
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
nynějším	nynější	k2eAgInSc6d1	nynější
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dosažení	dosažení	k1gNnSc4	dosažení
tohoto	tento	k3xDgInSc2	tento
cíle	cíl	k1gInSc2	cíl
tak	tak	k9	tak
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
vyučovat	vyučovat	k5eAaImF	vyučovat
dharmu	dharma	k1gFnSc4	dharma
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
světech	svět	k1gInPc6	svět
anebo	anebo	k8xC	anebo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zapomenuta	zapomenut	k2eAgFnSc1d1	zapomenuta
a	a	k8xC	a
pomoci	pomoct	k5eAaPmF	pomoct
tím	ten	k3xDgNnSc7	ten
nespočtu	nespočet	k1gInSc2	nespočet
bytostem	bytost	k1gFnPc3	bytost
k	k	k7c3	k
probuzení	probuzení	k1gNnSc3	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
starších	starý	k2eAgFnPc2d2	starší
škol	škola	k1gFnPc2	škola
mahájána	mahájána	k1gFnSc1	mahájána
začala	začít	k5eAaPmAgFnS	začít
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc4	takový
závazek	závazek	k1gInSc4	závazek
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vzít	vzít	k5eAaPmF	vzít
každý	každý	k3xTgMnSc1	každý
následovník	následovník	k1gMnSc1	následovník
Buddhy	Buddha	k1gMnSc2	Buddha
a	a	k8xC	a
ne	ne	k9	ne
jen	jen	k9	jen
několik	několik	k4yIc1	několik
silně	silně	k6eAd1	silně
odhodlaných	odhodlaný	k2eAgMnPc2d1	odhodlaný
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mahájáně	maháján	k1gInSc6	maháján
byly	být	k5eAaImAgInP	být
rozpracovány	rozpracován	k2eAgInPc1d1	rozpracován
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
stupně	stupeň	k1gInPc1	stupeň
pokroku	pokrok	k1gInSc2	pokrok
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
bódhisattva	bódhisattva	k6eAd1	bódhisattva
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Bódhisattvům	Bódhisattvo	k1gNnPc3	Bódhisattvo
na	na	k7c6	na
vyšších	vysoký	k2eAgInPc6d2	vyšší
stupních	stupeň	k1gInPc6	stupeň
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
přisuzovány	přisuzovat	k5eAaImNgFnP	přisuzovat
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
schopnosti	schopnost	k1gFnPc1	schopnost
včetně	včetně	k7c2	včetně
schopnosti	schopnost	k1gFnSc2	schopnost
vyučovat	vyučovat	k5eAaImF	vyučovat
dharmu	dharma	k1gFnSc4	dharma
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
tradiční	tradiční	k2eAgFnSc2d1	tradiční
představy	představa	k1gFnSc2	představa
si	se	k3xPyFc3	se
bódhisattva	bódhisattva	k6eAd1	bódhisattva
na	na	k7c6	na
vyšším	vysoký	k2eAgInSc6d2	vyšší
stupni	stupeň	k1gInSc6	stupeň
rozvoje	rozvoj	k1gInSc2	rozvoj
udržuje	udržovat	k5eAaImIp3nS	udržovat
jakési	jakýsi	k3yIgNnSc1	jakýsi
"	"	kIx"	"
<g/>
éterické	éterický	k2eAgNnSc4d1	éterické
<g/>
"	"	kIx"	"
tělo	tělo	k1gNnSc4	tělo
(	(	kIx(	(
<g/>
sa	sa	k?	sa
sambhógakája	sambhógakája	k1gFnSc1	sambhógakája
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
pomáhat	pomáhat	k5eAaImF	pomáhat
nespočtu	spočíst	k5eNaPmIp1nS	spočíst
bytostí	bytost	k1gFnSc7	bytost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mahájaně	mahájan	k1gInSc6	mahájan
tak	tak	k6eAd1	tak
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
bódhisattvů	bódhisattv	k1gInPc2	bódhisattv
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgInPc3	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
obracet	obracet	k5eAaImF	obracet
s	s	k7c7	s
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
prosbami	prosba	k1gFnPc7	prosba
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
mahájány	mahájána	k1gFnSc2	mahájána
je	být	k5eAaImIp3nS	být
i	i	k9	i
zenový	zenový	k2eAgInSc1d1	zenový
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
sice	sice	k8xC	sice
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
mahájánových	mahájánův	k2eAgFnPc2d1	mahájánův
súter	sútra	k1gFnPc2	sútra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc1d1	vlastní
velice	velice	k6eAd1	velice
specifický	specifický	k2eAgInSc1d1	specifický
negraduální	graduální	k2eNgInSc1d1	graduální
systém	systém	k1gInSc1	systém
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Mahájánový	Mahájánový	k2eAgInSc1d1	Mahájánový
buddhismus	buddhismus	k1gInSc1	buddhismus
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
praktikován	praktikován	k2eAgInSc1d1	praktikován
především	především	k9	především
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
větší	veliký	k2eAgFnSc3d2	veliký
části	část	k1gFnSc3	část
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vadžrajána	Vadžrajána	k1gFnSc1	Vadžrajána
<g/>
.	.	kIx.	.
</s>
<s>
Vadžrajána	Vadžrajána	k1gFnSc1	Vadžrajána
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
nezničitelné	zničitelný	k2eNgNnSc1d1	nezničitelné
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
výraz	výraz	k1gInSc1	výraz
tantrajána	tantraján	k1gMnSc4	tantraján
<g/>
,	,	kIx,	,
mantrajána	mantraján	k1gMnSc4	mantraján
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
učení	učení	k1gNnSc1	učení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Buddhovu	Buddhův	k2eAgNnSc3d1	Buddhovo
otočení	otočení	k1gNnSc3	otočení
kola	kolo	k1gNnSc2	kolo
dharmy	dharma	k1gFnSc2	dharma
<g/>
.	.	kIx.	.
</s>
<s>
Forma	forma	k1gFnSc1	forma
buddhismu	buddhismus	k1gInSc2	buddhismus
spojována	spojovat	k5eAaImNgFnS	spojovat
především	především	k6eAd1	především
s	s	k7c7	s
tibetským	tibetský	k2eAgInSc7d1	tibetský
buddhismem	buddhismus	k1gInSc7	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
všech	všecek	k3xTgFnPc2	všecek
metod	metoda	k1gFnPc2	metoda
předchozích	předchozí	k2eAgInPc2d1	předchozí
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
ještě	ještě	k6eAd1	ještě
speciálních	speciální	k2eAgFnPc2d1	speciální
praxí	praxe	k1gFnPc2	praxe
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
realizací	realizace	k1gFnSc7	realizace
skutečné	skutečný	k2eAgFnSc2d1	skutečná
podstaty	podstata	k1gFnSc2	podstata
a	a	k8xC	a
recitací	recitace	k1gFnSc7	recitace
manter	mantra	k1gFnPc2	mantra
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
učení	učení	k1gNnSc1	učení
vadžrajány	vadžrajána	k1gFnSc2	vadžrajána
je	být	k5eAaImIp3nS	být
skryto	skrýt	k5eAaPmNgNnS	skrýt
v	v	k7c6	v
"	"	kIx"	"
<g/>
posvátných	posvátný	k2eAgInPc6d1	posvátný
<g/>
"	"	kIx"	"
textech	text	k1gInPc6	text
(	(	kIx(	(
<g/>
buddhistických	buddhistický	k2eAgFnPc6d1	buddhistická
tantrách	tantra	k1gFnPc6	tantra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nezbytným	nezbytný	k2eAgInSc7d1	nezbytný
předpokladem	předpoklad	k1gInSc7	předpoklad
je	být	k5eAaImIp3nS	být
'	'	kIx"	'
<g/>
abhišeka	abhišeka	k1gFnSc1	abhišeka
/	/	kIx~	/
transmise	transmise	k1gFnSc1	transmise
od	od	k7c2	od
realizovaného	realizovaný	k2eAgMnSc2d1	realizovaný
učitele	učitel	k1gMnSc2	učitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
uvede	uvést	k5eAaPmIp3nS	uvést
žáka	žák	k1gMnSc4	žák
na	na	k7c4	na
přímou	přímý	k2eAgFnSc4d1	přímá
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
probuzení	probuzení	k1gNnSc3	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
Vadžrajána	Vadžrajána	k1gFnSc1	Vadžrajána
se	se	k3xPyFc4	se
vnitřně	vnitřně	k6eAd1	vnitřně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
vnější	vnější	k2eAgInPc4d1	vnější
tantry	tantr	k1gInPc4	tantr
(	(	kIx(	(
<g/>
Krija	Krij	k2eAgFnSc1d1	Krij
Tantra	Tantra	k1gFnSc1	Tantra
<g/>
,	,	kIx,	,
Charja	Charj	k2eAgFnSc1d1	Charj
Tantra	Tantra	k1gFnSc1	Tantra
<g/>
,	,	kIx,	,
Joga	Jog	k2eAgFnSc1d1	Joga
Tantra	Tantra	k1gFnSc1	Tantra
<g/>
)	)	kIx)	)
a	a	k8xC	a
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
tantry	tantr	k1gInPc4	tantr
(	(	kIx(	(
<g/>
Mahajoga	Mahajoga	k1gFnSc1	Mahajoga
<g/>
,	,	kIx,	,
Anujoga	Anujoga	k1gFnSc1	Anujoga
<g/>
,	,	kIx,	,
Atijoga	Atijoga	k1gFnSc1	Atijoga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
vadžrajánového	vadžrajánový	k2eAgInSc2d1	vadžrajánový
buddhismu	buddhismus	k1gInSc2	buddhismus
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc4	využití
všech	všecek	k3xTgInPc2	všecek
možných	možný	k2eAgInPc2d1	možný
nástrojů	nástroj	k1gInPc2	nástroj
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
probuzení	probuzení	k1gNnSc3	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
Vadžrajána	Vadžrajána	k1gFnSc1	Vadžrajána
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
učení	učení	k1gNnSc4	učení
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnPc1	její
metody	metoda	k1gFnPc1	metoda
vedou	vést	k5eAaImIp3nP	vést
nejrychleji	rychle	k6eAd3	rychle
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
stavu	stav	k1gInSc2	stav
buddhy	buddha	k1gMnPc7	buddha
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vadžrajánového	vadžrajánový	k2eAgNnSc2d1	vadžrajánový
pojetí	pojetí	k1gNnSc2	pojetí
se	se	k3xPyFc4	se
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
směrů	směr	k1gInPc2	směr
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
pomalejší	pomalý	k2eAgNnSc1d2	pomalejší
hromadění	hromadění	k1gNnSc1	hromadění
příčin	příčina	k1gFnPc2	příčina
(	(	kIx(	(
<g/>
smysluplné	smysluplný	k2eAgNnSc1d1	smysluplné
jednání	jednání	k1gNnSc1	jednání
<g/>
,	,	kIx,	,
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
činy	čin	k1gInPc1	čin
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
cestu	cesta	k1gFnSc4	cesta
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
praktikující	praktikující	k2eAgMnPc1d1	praktikující
se	se	k3xPyFc4	se
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
přímo	přímo	k6eAd1	přímo
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
podstatou	podstata	k1gFnSc7	podstata
a	a	k8xC	a
díky	díky	k7c3	díky
spojením	spojení	k1gNnSc7	spojení
s	s	k7c7	s
učitelem	učitel	k1gMnSc7	učitel
(	(	kIx(	(
<g/>
tib	tiba	k1gFnPc2	tiba
<g/>
.	.	kIx.	.
láma	láma	k1gMnSc1	láma
<g/>
)	)	kIx)	)
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
realizovat	realizovat	k5eAaBmF	realizovat
osvícení	osvícený	k2eAgMnPc1d1	osvícený
tím	ten	k3xDgMnSc7	ten
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
možným	možný	k2eAgInSc7d1	možný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
praktikována	praktikován	k2eAgFnSc1d1	praktikována
především	především	k9	především
na	na	k7c6	na
území	území	k1gNnSc6	území
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
,	,	kIx,	,
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc6	Bhútán
<g/>
,	,	kIx,	,
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
částí	část	k1gFnPc2	část
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
zejména	zejména	k9	zejména
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
prakticky	prakticky	k6eAd1	prakticky
celá	celý	k2eAgFnSc1d1	celá
Evropa	Evropa	k1gFnSc1	Evropa
i	i	k8xC	i
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
školy	škola	k1gFnPc4	škola
Karma	karma	k1gFnSc1	karma
Kagjü	Kagjü	k1gFnSc1	Kagjü
a	a	k8xC	a
učení	učení	k1gNnSc1	učení
dzogčhenu	dzogčhen	k1gInSc2	dzogčhen
školy	škola	k1gFnSc2	škola
Ňingmapa	Ňingmapa	k1gFnSc1	Ňingmapa
<g/>
.	.	kIx.	.
</s>
<s>
Vadžrajánová	Vadžrajánový	k2eAgFnSc1d1	Vadžrajánový
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
zakladatelem	zakladatel	k1gMnSc7	zakladatel
je	být	k5eAaImIp3nS	být
Gautama	Gautama	k1gFnSc1	Gautama
Buddha	Buddha	k1gMnSc1	Buddha
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
Garab	Garab	k1gInSc1	Garab
Dordže	Dordž	k1gFnSc2	Dordž
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
díky	dík	k1gInPc7	dík
Guru	guru	k1gMnPc2	guru
rinpočemu	rinpočemat	k5eAaPmIp1nS	rinpočemat
zavedena	zavést	k5eAaPmNgFnS	zavést
do	do	k7c2	do
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
nauk	nauka	k1gFnPc2	nauka
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
kraje	kraj	k1gInSc2	kraj
severního	severní	k2eAgInSc2d1	severní
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
pověstmi	pověst	k1gFnPc7	pověst
opředená	opředený	k2eAgFnSc1d1	opředená
Odijána	Odijána	k1gFnSc1	Odijána
<g/>
,	,	kIx,	,
království	království	k1gNnSc1	království
stovek	stovka	k1gFnPc2	stovka
klášterů	klášter	k1gInPc2	klášter
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Svat	Svata	k1gFnPc2	Svata
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
buddhismus	buddhismus	k1gInSc1	buddhismus
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
od	od	k7c2	od
3	[number]	k4	3
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
po	po	k7c4	po
středověký	středověký	k2eAgInSc4d1	středověký
útok	útok	k1gInSc4	útok
Mahmúda	Mahmúdo	k1gNnSc2	Mahmúdo
z	z	k7c2	z
Ghazny	Ghazna	k1gFnSc2	Ghazna
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
tohoto	tento	k3xDgInSc2	tento
muslimského	muslimský	k2eAgInSc2d1	muslimský
vpádu	vpád	k1gInSc2	vpád
bylo	být	k5eAaImAgNnS	být
zdejší	zdejší	k2eAgNnSc1d1	zdejší
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
zdecimováno	zdecimován	k2eAgNnSc1d1	zdecimováno
<g/>
,	,	kIx,	,
kláštery	klášter	k1gInPc1	klášter
pobořeny	pobořen	k2eAgInPc1d1	pobořen
a	a	k8xC	a
území	území	k1gNnSc6	území
Svátu	svát	k2eAgFnSc4d1	Sváta
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
divokým	divoký	k2eAgInSc7d1	divoký
krajem	kraj	k1gInSc7	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
buddhismu	buddhismus	k1gInSc2	buddhismus
lze	lze	k6eAd1	lze
orientačně	orientačně	k6eAd1	orientačně
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
fází	fáze	k1gFnPc2	fáze
<g/>
:	:	kIx,	:
Období	období	k1gNnSc1	období
Buddhova	Buddhův	k2eAgInSc2d1	Buddhův
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
pol.	pol.	k?	pol.
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
pol.	pol.	k?	pol.
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Doba	doba	k1gFnSc1	doba
štěpení	štěpení	k1gNnSc2	štěpení
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
školy	škola	k1gFnPc4	škola
(	(	kIx(	(
<g/>
od	od	k7c2	od
pol.	pol.	k?	pol.
4	[number]	k4	4
<g/>
<g />
.	.	kIx.	.
</s>
<s>
století	století	k1gNnSc1	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
působení	působení	k1gNnSc1	působení
mahájány	mahájána	k1gFnSc2	mahájána
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
Nástup	nástup	k1gInSc1	nástup
tantrismu	tantrismus	k1gInSc2	tantrismus
(	(	kIx(	(
<g/>
po	po	k7c4	po
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Buddha	Buddha	k1gMnSc1	Buddha
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
následovníci	následovník	k1gMnPc1	následovník
začínali	začínat	k5eAaImAgMnP	začínat
jako	jako	k8xC	jako
skupina	skupina	k1gFnSc1	skupina
potulných	potulný	k2eAgMnPc2d1	potulný
asketů	asketa	k1gMnPc2	asketa
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
začaly	začít	k5eAaPmAgInP	začít
stavět	stavět	k5eAaImF	stavět
přístřešky	přístřešek	k1gInPc1	přístřešek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
postupně	postupně	k6eAd1	postupně
proměnily	proměnit	k5eAaPmAgFnP	proměnit
v	v	k7c4	v
kláštery	klášter	k1gInPc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
Buddha	Buddha	k1gMnSc1	Buddha
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
žáci	žák	k1gMnPc1	žák
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
obraceli	obracet	k5eAaImAgMnP	obracet
s	s	k7c7	s
prosbami	prosba	k1gFnPc7	prosba
o	o	k7c4	o
řešení	řešení	k1gNnSc4	řešení
sporů	spor	k1gInPc2	spor
a	a	k8xC	a
nejasností	nejasnost	k1gFnPc2	nejasnost
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k9	již
za	za	k7c2	za
Buddhova	Buddhův	k2eAgInSc2d1	Buddhův
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gFnPc7	jeho
žáky	žák	k1gMnPc4	žák
četné	četný	k2eAgInPc1d1	četný
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
naplno	naplno	k6eAd1	naplno
propukly	propuknout	k5eAaPmAgFnP	propuknout
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Buddhistické	buddhistický	k2eAgInPc4d1	buddhistický
koncily	koncil	k1gInPc4	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Buddha	Buddha	k1gMnSc1	Buddha
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
utřídit	utřídit	k5eAaPmF	utřídit
jeho	jeho	k3xOp3gFnSc4	jeho
nauku	nauka	k1gFnSc4	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
již	již	k6eAd1	již
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
po	po	k7c6	po
Buddhově	Buddhův	k2eAgFnSc6d1	Buddhova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
483	[number]	k4	483
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Rádžargze	Rádžargza	k1gFnSc6	Rádžargza
první	první	k4xOgInSc4	první
buddhistický	buddhistický	k2eAgInSc4d1	buddhistický
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sestavení	sestavení	k1gNnSc3	sestavení
mnišských	mnišský	k2eAgNnPc2d1	mnišské
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
recitovaly	recitovat	k5eAaImAgFnP	recitovat
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
Buddhovy	Buddhův	k2eAgFnPc4d1	Buddhova
rozpravy	rozprava	k1gFnSc2	rozprava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
však	však	k9	však
tento	tento	k3xDgInSc1	tento
koncil	koncil	k1gInSc1	koncil
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
tak	tak	k6eAd1	tak
již	již	k6eAd1	již
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
mnišském	mnišský	k2eAgNnSc6d1	mnišské
shromáždění	shromáždění	k1gNnSc6	shromáždění
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dvě	dva	k4xCgFnPc1	dva
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
sbírek	sbírka	k1gFnPc2	sbírka
buddhistického	buddhistický	k2eAgInSc2d1	buddhistický
kánonu	kánon	k1gInSc2	kánon
(	(	kIx(	(
<g/>
p.	p.	k?	p.
tipitaka	tipitak	k1gMnSc2	tipitak
<g/>
;	;	kIx,	;
sa	sa	k?	sa
<g/>
.	.	kIx.	.
tripitaka	tripitak	k1gMnSc2	tripitak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vinajapitaka	vinajapitak	k1gMnSc4	vinajapitak
a	a	k8xC	a
suttapitaka	suttapitak	k1gMnSc4	suttapitak
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
podle	podle	k7c2	podle
théravádové	théravádový	k2eAgFnSc2d1	théravádový
tradice	tradice	k1gFnSc2	tradice
již	již	k6eAd1	již
zde	zde	k6eAd1	zde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
třetí	třetí	k4xOgInSc4	třetí
oddíl	oddíl	k1gInSc4	oddíl
tipitaky	tipitak	k1gInPc7	tipitak
<g/>
,	,	kIx,	,
abhidhammapitaka	abhidhammapitak	k1gMnSc4	abhidhammapitak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
následujícího	následující	k2eAgNnSc2d1	následující
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgFnPc1	žádný
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c4	po
sto	sto	k4xCgNnSc4	sto
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
Vaišálí	Vaišálý	k2eAgMnPc1d1	Vaišálý
pořádat	pořádat	k5eAaImF	pořádat
další	další	k2eAgInSc4d1	další
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
neobešel	obešet	k5eNaPmAgInS	obešet
bez	bez	k7c2	bez
vážných	vážný	k2eAgInPc2d1	vážný
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Tamější	tamější	k2eAgMnPc1d1	tamější
mniši	mnich	k1gMnPc1	mnich
byli	být	k5eAaImAgMnP	být
obviněni	obvinit	k5eAaPmNgMnP	obvinit
z	z	k7c2	z
nedodržování	nedodržování	k1gNnSc2	nedodržování
řádových	řádový	k2eAgNnPc2d1	řádové
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
koncil	koncil	k1gInSc4	koncil
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
radou	rada	k1gFnSc7	rada
starších	starší	k1gMnPc2	starší
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnišská	mnišský	k2eAgNnPc1d1	mnišské
nařízení	nařízení	k1gNnPc1	nařízení
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
přísně	přísně	k6eAd1	přísně
dodržovat	dodržovat	k5eAaImF	dodržovat
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
mnichů	mnich	k1gMnPc2	mnich
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
vaišálskými	vaišálský	k2eAgInPc7d1	vaišálský
mnichy	mnich	k1gInPc7	mnich
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
přijmout	přijmout	k5eAaPmF	přijmout
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
vlastní	vlastní	k2eAgNnPc4d1	vlastní
shromáždění	shromáždění	k1gNnPc4	shromáždění
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
pozměnili	pozměnit	k5eAaPmAgMnP	pozměnit
kánon	kánon	k1gInSc4	kánon
a	a	k8xC	a
přidali	přidat	k5eAaPmAgMnP	přidat
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
i	i	k9	i
některé	některý	k3yIgInPc4	některý
další	další	k2eAgInPc4d1	další
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
mnichů	mnich	k1gMnPc2	mnich
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
označila	označit	k5eAaPmAgFnS	označit
jako	jako	k9	jako
mahásánghika	mahásánghika	k1gFnSc1	mahásánghika
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
sangha	sangha	k1gFnSc1	sangha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odlišila	odlišit	k5eAaPmAgFnS	odlišit
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
mnichů	mnich	k1gInPc2	mnich
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
théraváda	théraváda	k1gFnSc1	théraváda
(	(	kIx(	(
<g/>
sa	sa	k?	sa
<g/>
.	.	kIx.	.
sthaviraváda	sthaviraváda	k1gFnSc1	sthaviraváda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
od	od	k7c2	od
mnišské	mnišský	k2eAgFnSc2d1	mnišská
obce	obec	k1gFnSc2	obec
odtrhla	odtrhnout	k5eAaPmAgFnS	odtrhnout
první	první	k4xOgNnPc4	první
hnutí	hnutí	k1gNnPc2	hnutí
mnichů	mnich	k1gMnPc2	mnich
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
tím	ten	k3xDgNnSc7	ten
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
osamostatnění	osamostatnění	k1gNnSc3	osamostatnění
se	se	k3xPyFc4	se
dalším	další	k2eAgFnPc3d1	další
skupinám	skupina	k1gFnPc3	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dalších	další	k2eAgNnPc2d1	další
přibližně	přibližně	k6eAd1	přibližně
dvě	dva	k4xCgNnPc4	dva
stě	sto	k4xCgFnPc1	sto
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetí	třetí	k4xOgMnSc1	třetí
buddhistický	buddhistický	k2eAgInSc1d1	buddhistický
koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
arhata	arhat	k1gMnSc4	arhat
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
tento	tento	k3xDgInSc4	tento
koncil	koncil	k1gInSc4	koncil
svolali	svolat	k5eAaPmAgMnP	svolat
mahásanghikové	mahásanghik	k1gMnPc1	mahásanghik
<g/>
,	,	kIx,	,
théravádinové	théravádinový	k2eAgInPc4d1	théravádinový
jej	on	k3xPp3gMnSc4	on
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
uznat	uznat	k5eAaPmF	uznat
a	a	k8xC	a
za	za	k7c4	za
třetí	třetí	k4xOgInSc4	třetí
buddhistický	buddhistický	k2eAgInSc4d1	buddhistický
koncil	koncil	k1gInSc4	koncil
považují	považovat	k5eAaImIp3nP	považovat
setkání	setkání	k1gNnPc4	setkání
mnichů	mnich	k1gMnPc2	mnich
v	v	k7c6	v
cejlonské	cejlonský	k2eAgFnSc6d1	cejlonská
Patáliputře	Patáliputra	k1gFnSc6	Patáliputra
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
244	[number]	k4	244
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
koncilů	koncil	k1gInPc2	koncil
o	o	k7c6	o
třetím	třetí	k4xOgInSc6	třetí
koncilu	koncil	k1gInSc6	koncil
nejsou	být	k5eNaImIp3nP	být
záznamy	záznam	k1gInPc1	záznam
ve	v	k7c4	v
vinajapitace	vinajapitace	k1gFnPc4	vinajapitace
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
140	[number]	k4	140
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
další	další	k2eAgFnSc1d1	další
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
názvem	název	k1gInSc7	název
vátsiputríja	vátsiputríjus	k1gMnSc2	vátsiputríjus
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
zakladatel	zakladatel	k1gMnSc1	zakladatel
Vátsiputra	Vátsiputrum	k1gNnSc2	Vátsiputrum
začal	začít	k5eAaPmAgMnS	začít
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
samostatná	samostatný	k2eAgFnSc1d1	samostatná
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
pěti	pět	k4xCc6	pět
složkách	složka	k1gFnPc6	složka
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
další	další	k2eAgNnSc1d1	další
štěpení	štěpení	k1gNnSc1	štěpení
do	do	k7c2	do
desítek	desítka	k1gFnPc2	desítka
různých	různý	k2eAgFnPc2d1	různá
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
dnes	dnes	k6eAd1	dnes
všechny	všechen	k3xTgFnPc1	všechen
zaniklé	zaniklý	k2eAgFnPc1d1	zaniklá
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1	zakladatel
indické	indický	k2eAgFnSc2d1	indická
dynastie	dynastie	k1gFnSc2	dynastie
Maurjů	Maurj	k1gMnPc2	Maurj
král	král	k1gMnSc1	král
Ašóka	Ašóka	k1gMnSc1	Ašóka
(	(	kIx(	(
<g/>
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
232	[number]	k4	232
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
přijal	přijmout	k5eAaPmAgInS	přijmout
buddhismus	buddhismus	k1gInSc1	buddhismus
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
buddhistické	buddhistický	k2eAgFnPc4d1	buddhistická
misie	misie	k1gFnPc4	misie
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
do	do	k7c2	do
Baktrie	Baktrie	k1gFnSc2	Baktrie
a	a	k8xC	a
na	na	k7c4	na
Cejlon	Cejlon	k1gInSc4	Cejlon
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
úspěch	úspěch	k1gInSc1	úspěch
misie	misie	k1gFnSc2	misie
na	na	k7c6	na
Cejlonu	Cejlon	k1gInSc6	Cejlon
byl	být	k5eAaImAgMnS	být
ohromující	ohromující	k2eAgMnSc1d1	ohromující
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
buddhismus	buddhismus	k1gInSc1	buddhismus
udržel	udržet	k5eAaPmAgInS	udržet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mahájána	Mahájána	k1gFnSc1	Mahájána
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
okolo	okolo	k7c2	okolo
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
mezi	mezi	k7c7	mezi
stoupenci	stoupenec	k1gMnPc7	stoupenec
Buddhovy	Buddhův	k2eAgFnSc2d1	Buddhova
nauky	nauka	k1gFnSc2	nauka
nový	nový	k2eAgInSc4d1	nový
směr	směr	k1gInSc4	směr
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
stoupenci	stoupenec	k1gMnPc1	stoupenec
ho	on	k3xPp3gNnSc4	on
později	pozdě	k6eAd2	pozdě
začnou	začít	k5eAaPmIp3nP	začít
nazývat	nazývat	k5eAaImF	nazývat
mahájána	mahájat	k5eAaImNgFnS	mahájat
(	(	kIx(	(
<g/>
velké	velký	k2eAgNnSc1d1	velké
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pojem	pojem	k1gInSc1	pojem
hínajána	hínaján	k1gMnSc4	hínaján
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
autoři	autor	k1gMnPc1	autor
byli	být	k5eAaImAgMnP	být
stoupenci	stoupenec	k1gMnPc1	stoupenec
mahájánového	mahájánový	k2eAgInSc2d1	mahájánový
směru	směr	k1gInSc2	směr
a	a	k8xC	a
který	který	k3yIgInSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
všeho	všecek	k3xTgNnSc2	všecek
buddhistického	buddhistický	k2eAgNnSc2d1	buddhistické
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nebylo	být	k5eNaImAgNnS	být
mahájánou	mahájána	k1gFnSc7	mahájána
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
slovo	slovo	k1gNnSc1	slovo
pejorativního	pejorativní	k2eAgInSc2d1	pejorativní
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
mahájánou	mahájána	k1gFnSc7	mahájána
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
buddhistických	buddhistický	k2eAgInPc2d1	buddhistický
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pradžňápáramitové	pradžňápáramitový	k2eAgFnSc2d1	pradžňápáramitový
sútry	sútra	k1gFnSc2	sútra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začal	začít	k5eAaPmAgMnS	začít
působit	působit	k5eAaImF	působit
filosof	filosof	k1gMnSc1	filosof
Nágárdžuna	Nágárdžuna	k1gFnSc1	Nágárdžuna
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
založil	založit	k5eAaPmAgInS	založit
školu	škola	k1gFnSc4	škola
Mádhjamika	Mádhjamik	k1gMnSc2	Mádhjamik
a	a	k8xC	a
sesystematizoval	sesystematizovat	k5eAaBmAgMnS	sesystematizovat
nauku	nauka	k1gFnSc4	nauka
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yQgFnSc7	jaký
přišla	přijít	k5eAaPmAgFnS	přijít
pradžňápáramitová	pradžňápáramitový	k2eAgFnSc1d1	pradžňápáramitový
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
mahájánové	mahájánový	k2eAgFnPc4d1	mahájánový
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
Škola	škola	k1gFnSc1	škola
prázdnoty	prázdnota	k1gFnSc2	prázdnota
apod.	apod.	kA	apod.
Jak	jak	k6eAd1	jak
mahájánový	mahájánový	k2eAgMnSc1d1	mahájánový
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
théravádový	théravádový	k2eAgInSc1d1	théravádový
proud	proud	k1gInSc1	proud
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
šířit	šířit	k5eAaImF	šířit
svým	svůj	k3xOyFgInSc7	svůj
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Mahájána	Mahájána	k1gFnSc1	Mahájána
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
šířit	šířit	k5eAaImF	šířit
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
,	,	kIx,	,
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
,	,	kIx,	,
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
;	;	kIx,	;
théravádové	théravádový	k2eAgNnSc1d1	théravádový
učení	učení	k1gNnSc1	učení
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
spíše	spíše	k9	spíše
do	do	k7c2	do
jižních	jižní	k2eAgFnPc2d1	jižní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Buddhismus	buddhismus	k1gInSc1	buddhismus
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
buddhistů	buddhista	k1gMnPc2	buddhista
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
tvoří	tvořit	k5eAaImIp3nP	tvořit
příslušníci	příslušník	k1gMnPc1	příslušník
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
menšiny	menšina	k1gFnSc2	menšina
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
i	i	k9	i
čeští	český	k2eAgMnPc1d1	český
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
si	se	k3xPyFc3	se
jejich	jejich	k3xOp3gFnSc1	jejich
komunita	komunita	k1gFnSc1	komunita
zřídila	zřídit	k5eAaPmAgFnS	zřídit
ve	v	k7c6	v
Varnsdorfu	Varnsdorf	k1gInSc6	Varnsdorf
buddhistický	buddhistický	k2eAgInSc1d1	buddhistický
chrám	chrám	k1gInSc1	chrám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
46	[number]	k4	46
center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc1	údaj
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Buddhismu	buddhismus	k1gInSc2	buddhismus
Diamantové	diamantový	k2eAgFnSc2d1	Diamantová
Cesty	cesta	k1gFnSc2	cesta
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Karmapy	Karmapa	k1gFnPc1	Karmapa
Trinleye	Trinley	k1gMnSc2	Trinley
Thajeho	Thaje	k1gMnSc2	Thaje
Dordžeho	Dordže	k1gMnSc2	Dordže
a	a	k8xC	a
lamy	lama	k1gFnSc2	lama
Ole	Ola	k1gFnSc3	Ola
Nydahla	Nydahla	k1gFnSc2	Nydahla
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
center	centrum	k1gNnPc2	centrum
Dzogčhenu	Dzogčhen	k1gInSc2	Dzogčhen
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnPc4d1	přesná
informace	informace	k1gFnPc4	informace
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
uvedené	uvedený	k2eAgFnSc6d1	uvedená
literatuře	literatura	k1gFnSc6	literatura
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
výčtu	výčet	k1gInSc6	výčet
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgFnPc1d1	uvedena
osobnosti	osobnost	k1gFnPc1	osobnost
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
nemá	mít	k5eNaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
obsáhnout	obsáhnout	k5eAaPmF	obsáhnout
slavné	slavný	k2eAgFnPc4d1	slavná
osoby	osoba	k1gFnPc4	osoba
praktikující	praktikující	k2eAgFnSc1d1	praktikující
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautam	k1gMnSc4	Gautam
Buddha	Buddha	k1gMnSc1	Buddha
Ánanda	Ánand	k1gMnSc4	Ánand
Garab	Garab	k1gMnSc1	Garab
Dordže	Dordž	k1gFnSc2	Dordž
Padmasambhava	Padmasambhava	k1gFnSc1	Padmasambhava
Nágárdžuna	Nágárdžuna	k1gFnSc1	Nágárdžuna
Tilopa	Tilopa	k1gFnSc1	Tilopa
Náropa	Náropa	k1gFnSc1	Náropa
Marpa	Marpa	k1gFnSc1	Marpa
Milarepa	Milarepa	k1gFnSc1	Milarepa
Gampopa	Gampopa	k1gFnSc1	Gampopa
Jeho	jeho	k3xOp3gFnSc4	jeho
svatost	svatost	k1gFnSc1	svatost
dalajláma	dalajláma	k1gMnSc1	dalajláma
Jeho	jeho	k3xOp3gFnSc4	jeho
svatost	svatost	k1gFnSc4	svatost
karmapa	karmap	k1gMnSc2	karmap
Tai	Tai	k1gMnSc2	Tai
situ	siíst	k5eAaPmIp1nS	siíst
rinpočhe	rinpočhe	k6eAd1	rinpočhe
Šamar	Šamar	k1gInSc1	Šamar
rinpočhe	rinpočhat	k5eAaPmIp3nS	rinpočhat
Džamgon	Džamgon	k1gInSc4	Džamgon
Kongtrul	Kongtrul	k1gInSc1	Kongtrul
Lodro	Lodro	k1gNnSc1	Lodro
Thaje	Thaj	k1gMnSc2	Thaj
Patrul	Patrul	k?	Patrul
rinpočhe	rinpočhat	k5eAaPmIp3nS	rinpočhat
Čhögjal	Čhögjal	k1gInSc1	Čhögjal
Namkhai	Namkhai	k1gNnSc2	Namkhai
Norbu	Norb	k1gInSc2	Norb
rinpočhe	rinpočhat	k5eAaPmIp3nS	rinpočhat
Dilgo	Dilgo	k6eAd1	Dilgo
Khjence	Khjence	k1gFnSc1	Khjence
rinpočhe	rinpočhat	k5eAaPmIp3nS	rinpočhat
Khandro	Khandra	k1gFnSc5	Khandra
rinpočhe	rinpočhe	k1gNnPc4	rinpočhe
Buddhadása	Buddhadás	k1gMnSc2	Buddhadás
Rewatadhamma	Rewatadhamm	k1gMnSc2	Rewatadhamm
Sayadaw	Sayadaw	k1gMnSc2	Sayadaw
Nyánaponika	Nyánaponik	k1gMnSc2	Nyánaponik
Maháthera	Maháther	k1gMnSc2	Maháther
Mahási	Maháse	k1gFnSc4	Maháse
Sayadaw	Sayadaw	k1gFnSc2	Sayadaw
Jašódhara	Jašódhara	k1gFnSc1	Jašódhara
Henry	Henry	k1gMnSc1	Henry
Steel	Steel	k1gMnSc1	Steel
Olcott	Olcott	k1gMnSc1	Olcott
Ole	Ola	k1gFnSc3	Ola
Nydahl	Nydahl	k1gFnSc1	Nydahl
František	František	k1gMnSc1	František
Drtikol	Drtikol	k1gInSc1	Drtikol
</s>
