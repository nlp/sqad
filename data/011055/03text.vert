<p>
<s>
Henry	Henry	k1gMnSc1	Henry
Padovani	Padovan	k1gMnPc1	Padovan
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Henri	Henr	k1gInSc6	Henr
Padovani	Padovaň	k1gFnSc3	Padovaň
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1952	[number]	k4	1952
Bastia	Bastia	k1gFnSc1	Bastia
<g/>
,	,	kIx,	,
Korsika	Korsika	k1gFnSc1	Korsika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
korsický	korsický	k2eAgMnSc1d1	korsický
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gMnSc2	The
Police	police	k1gFnSc2	police
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
jediné	jediný	k2eAgFnSc6d1	jediná
nahrávce	nahrávka	k1gFnSc6	nahrávka
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Fall	Fall	k1gMnSc1	Fall
Out	Out	k1gMnSc1	Out
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odešel	odejít	k5eAaPmAgMnS	odejít
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1977	[number]	k4	1977
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
nedokončenou	dokončený	k2eNgFnSc4d1	nedokončená
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
frekcenci	frekcence	k1gFnSc4	frekcence
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Johnem	John	k1gMnSc7	John
Calem	Cal	k1gMnSc7	Cal
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k2eAgFnPc6d1	další
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Wayne	Wayn	k1gInSc5	Wayn
County	Counta	k1gFnSc2	Counta
&	&	k?	&
the	the	k?	the
Electric	Electric	k1gMnSc1	Electric
Chairs	Chairs	k1gInSc1	Chairs
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgInSc4d1	vlastní
soubor	soubor	k1gInSc4	soubor
The	The	k1gMnPc2	The
Flying	Flying	k1gInSc1	Flying
Padovanis	Padovanis	k1gFnPc2	Padovanis
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgInSc4d1	aktivní
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
