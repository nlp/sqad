<s>
Chufuova	Chufuův	k2eAgFnSc1d1	Chufuova
pyramida	pyramida	k1gFnSc1	pyramida
(	(	kIx(	(
<g/>
též	též	k9	též
Cheopsova	Cheopsův	k2eAgFnSc1d1	Cheopsova
pyramida	pyramida	k1gFnSc1	pyramida
nebo	nebo	k8xC	nebo
Velká	velký	k2eAgFnSc1d1	velká
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
pyramida	pyramida	k1gFnSc1	pyramida
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
známá	známý	k2eAgFnSc1d1	známá
kamenná	kamenný	k2eAgFnSc1d1	kamenná
stavba	stavba	k1gFnSc1	stavba
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
období	období	k1gNnSc6	období
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
a	a	k8xC	a
současně	současně	k6eAd1	současně
jediný	jediný	k2eAgMnSc1d1	jediný
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
zachovaný	zachovaný	k2eAgInSc4d1	zachovaný
z	z	k7c2	z
klasických	klasický	k2eAgInPc2d1	klasický
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
nejcharakterističtější	charakteristický	k2eAgInSc4d3	nejcharakterističtější
symbol	symbol	k1gInSc4	symbol
staroegyptské	staroegyptský	k2eAgFnSc2d1	staroegyptská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
nejvíce	nejvíce	k6eAd1	nejvíce
zatížených	zatížený	k2eAgInPc2d1	zatížený
turistickým	turistický	k2eAgInSc7d1	turistický
ruchem	ruch	k1gInSc7	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Pyramida	pyramida	k1gFnSc1	pyramida
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pyramidovém	pyramidový	k2eAgNnSc6d1	pyramidové
poli	pole	k1gNnSc6	pole
v	v	k7c6	v
Gíze	Gíze	k1gFnSc6	Gíze
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
současné	současný	k2eAgFnSc2d1	současná
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgFnSc7d1	ústřední
částí	část	k1gFnSc7	část
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
pyramidového	pyramidový	k2eAgInSc2d1	pyramidový
komplexu	komplex	k1gInSc2	komplex
vybudovaného	vybudovaný	k2eAgInSc2d1	vybudovaný
v	v	k7c6	v
období	období	k1gNnSc6	období
Staré	Staré	k2eAgFnSc2d1	Staré
říše	říš	k1gFnSc2	říš
jako	jako	k8xC	jako
hrobka	hrobka	k1gFnSc1	hrobka
panovníka	panovník	k1gMnSc2	panovník
4	[number]	k4	4
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
Chufua	Chufu	k1gInSc2	Chufu
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
egyptského	egyptský	k2eAgNnSc2d1	egyptské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Mennoferu	Mennofer	k1gInSc2	Mennofer
nejspíše	nejspíše	k9	nejspíše
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
vezíra	vezír	k1gMnSc2	vezír
Hemiuna	Hemiuna	k1gFnSc1	Hemiuna
<g/>
.	.	kIx.	.
</s>
<s>
Egypťany	Egypťan	k1gMnPc4	Egypťan
byl	být	k5eAaImAgInS	být
komplex	komplex	k1gInSc1	komplex
nazýván	nazývat	k5eAaImNgInS	nazývat
Achtej	Achtej	k1gFnSc7	Achtej
Chufu	Chuf	k1gInSc2	Chuf
–	–	k?	–
"	"	kIx"	"
<g/>
Chufu	Chuf	k1gInSc2	Chuf
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nevyjadřuje	vyjadřovat	k5eNaImIp3nS	vyjadřovat
dojem	dojem	k1gInSc4	dojem
velikosti	velikost	k1gFnSc2	velikost
stavby	stavba	k1gFnSc2	stavba
zastiňující	zastiňující	k2eAgFnSc2d1	zastiňující
vše	všechen	k3xTgNnSc4	všechen
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dříve	dříve	k6eAd2	dříve
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
staroegyptskou	staroegyptský	k2eAgFnSc4d1	staroegyptská
představu	představa	k1gFnSc4	představa
králova	králův	k2eAgInSc2d1	králův
posmrtného	posmrtný	k2eAgInSc2d1	posmrtný
života	život	k1gInSc2	život
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
kosmickým	kosmický	k2eAgInSc7d1	kosmický
cyklem	cyklus	k1gInSc7	cyklus
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
pyramidami	pyramida	k1gFnPc7	pyramida
Chufuova	Chufuův	k2eAgInSc2d1	Chufuův
pyramida	pyramida	k1gFnSc1	pyramida
určité	určitý	k2eAgInPc1d1	určitý
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
rysy	rys	k1gInPc1	rys
<g/>
;	;	kIx,	;
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
celý	celý	k2eAgInSc1d1	celý
Chufuův	Chufuův	k2eAgInSc1d1	Chufuův
pyramidový	pyramidový	k2eAgInSc1d1	pyramidový
komplex	komplex	k1gInSc1	komplex
<g/>
)	)	kIx)	)
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
archeologicky	archeologicky	k6eAd1	archeologicky
nejzkoumanějších	zkoumaný	k2eAgInPc2d3	zkoumaný
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
interpretovaných	interpretovaný	k2eAgInPc2d1	interpretovaný
starověkých	starověký	k2eAgInPc2d1	starověký
artefaktů	artefakt	k1gInPc2	artefakt
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
vědou	věda	k1gFnSc7	věda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mimovědeckými	mimovědecký	k2eAgFnPc7d1	mimovědecký
disciplínami	disciplína	k1gFnPc7	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
až	až	k6eAd1	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
živý	živý	k2eAgInSc1d1	živý
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
proud	proud	k1gInSc1	proud
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
pyramidologie	pyramidologie	k1gFnSc1	pyramidologie
reflektující	reflektující	k2eAgFnPc1d1	reflektující
pyramidy	pyramida	k1gFnPc1	pyramida
(	(	kIx(	(
<g/>
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
z	z	k7c2	z
pochopitelných	pochopitelný	k2eAgInPc2d1	pochopitelný
důvodů	důvod	k1gInPc2	důvod
především	především	k6eAd1	především
Chufuovu	Chufuov	k1gInSc2	Chufuov
<g/>
)	)	kIx)	)
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
hermetismu	hermetismus	k1gInSc2	hermetismus
a	a	k8xC	a
esoteriky	esoterik	k1gMnPc4	esoterik
jako	jako	k8xS	jako
nositele	nositel	k1gMnPc4	nositel
odvěké	odvěký	k2eAgFnSc2d1	odvěká
moudrosti	moudrost	k1gFnSc2	moudrost
či	či	k8xC	či
zvláštních	zvláštní	k2eAgNnPc2d1	zvláštní
tajemství	tajemství	k1gNnPc2	tajemství
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
"	"	kIx"	"
<g/>
zašifrována	zašifrován	k2eAgFnSc1d1	zašifrována
<g/>
"	"	kIx"	"
a	a	k8xC	a
která	který	k3yRgFnSc1	který
třeba	třeba	k6eAd1	třeba
"	"	kIx"	"
<g/>
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejnovější	nový	k2eAgFnSc6d3	nejnovější
době	doba	k1gFnSc6	doba
bývají	bývat	k5eAaImIp3nP	bývat
nejrůznějšími	různý	k2eAgInPc7d3	nejrůznější
způsoby	způsob	k1gInPc7	způsob
dávány	dáván	k2eAgInPc4d1	dáván
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
údajnou	údajný	k2eAgFnSc7d1	údajná
existencí	existence	k1gFnSc7	existence
mimozemských	mimozemský	k2eAgFnPc2d1	mimozemská
civilizací	civilizace	k1gFnPc2	civilizace
nebo	nebo	k8xC	nebo
s	s	k7c7	s
dávnými	dávný	k2eAgNnPc7d1	dávné
a	a	k8xC	a
současné	současný	k2eAgFnSc6d1	současná
vědě	věda	k1gFnSc6	věda
neznámými	známý	k2eNgFnPc7d1	neznámá
lidskými	lidský	k2eAgFnPc7d1	lidská
kulturami	kultura	k1gFnPc7	kultura
disponujícími	disponující	k2eAgFnPc7d1	disponující
pokročilými	pokročilý	k2eAgFnPc7d1	pokročilá
technologiemi	technologie	k1gFnPc7	technologie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
znakem	znak	k1gInSc7	znak
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
teorií	teorie	k1gFnPc2	teorie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítají	odmítat	k5eAaImIp3nP	odmítat
akceptovat	akceptovat	k5eAaBmF	akceptovat
nejen	nejen	k6eAd1	nejen
primární	primární	k2eAgInSc4d1	primární
účel	účel	k1gInSc4	účel
pyramid	pyramida	k1gFnPc2	pyramida
jako	jako	k8xC	jako
hrobek	hrobka	k1gFnPc2	hrobka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jejich	jejich	k3xOp3gNnSc2	jejich
datování	datování	k1gNnSc2	datování
egyptologií	egyptologie	k1gFnPc2	egyptologie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
posouvají	posouvat	k5eAaImIp3nP	posouvat
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Chufuovy	Chufuův	k2eAgFnSc2d1	Chufuova
pyramidy	pyramida	k1gFnSc2	pyramida
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
nejčastěji	často	k6eAd3	často
poukazováno	poukazován	k2eAgNnSc1d1	poukazováno
na	na	k7c4	na
neprůkaznost	neprůkaznost	k1gFnSc4	neprůkaznost
Chufua	Chufuus	k1gMnSc2	Chufuus
jako	jako	k8xS	jako
stavebníka	stavebník	k1gMnSc2	stavebník
<g/>
.	.	kIx.	.
</s>
<s>
Pyramida	pyramida	k1gFnSc1	pyramida
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
rozměr	rozměr	k1gInSc4	rozměr
základny	základna	k1gFnPc4	základna
230,38	[number]	k4	230,38
×	×	k?	×
230,38	[number]	k4	230,38
m	m	kA	m
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
ploše	plocha	k1gFnSc3	plocha
třinácti	třináct	k4xCc2	třináct
fotbalových	fotbalový	k2eAgNnPc2d1	fotbalové
hřišť	hřiště	k1gNnPc2	hřiště
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
minimálním	minimální	k2eAgInSc6d1	minimální
rozměru	rozměr	k1gInSc6	rozměr
90	[number]	k4	90
<g/>
x	x	k?	x
<g/>
45	[number]	k4	45
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
její	její	k3xOp3gFnSc1	její
současná	současný	k2eAgFnSc1d1	současná
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
138.8	[number]	k4	138.8
m	m	kA	m
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
o	o	k7c4	o
2	[number]	k4	2
m	m	kA	m
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
delší	dlouhý	k2eAgMnSc1d2	delší
a	a	k8xC	a
o	o	k7c4	o
9	[number]	k4	9
m	m	kA	m
vyšší	vysoký	k2eAgNnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Sklon	sklon	k1gInSc1	sklon
pyramidy	pyramida	k1gFnSc2	pyramida
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
51	[number]	k4	51
<g/>
°	°	k?	°
<g/>
50	[number]	k4	50
<g/>
́	́	k?	́
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
lidskou	lidský	k2eAgFnSc7d1	lidská
stavbou	stavba	k1gFnSc7	stavba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
5,15	[number]	k4	5,15
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Chybějící	chybějící	k2eAgFnSc1d1	chybějící
špička	špička	k1gFnSc1	špička
<g/>
,	,	kIx,	,
kazící	kazící	k2eAgInSc1d1	kazící
dojem	dojem	k1gInSc1	dojem
dokonalého	dokonalý	k2eAgInSc2d1	dokonalý
jehlanu	jehlan	k1gInSc2	jehlan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
–	–	k?	–
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
defektu	defekt	k1gInSc3	defekt
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
pyramidy	pyramida	k1gFnSc2	pyramida
protiletecká	protiletecký	k2eAgFnSc1d1	protiletecká
pozorovatelna	pozorovatelna	k1gFnSc1	pozorovatelna
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejobjemnější	objemný	k2eAgFnSc4d3	nejobjemnější
stavbu	stavba	k1gFnSc4	stavba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
–	–	k?	–
původní	původní	k2eAgInSc1d1	původní
objem	objem	k1gInSc1	objem
celé	celý	k2eAgFnSc2d1	celá
stavby	stavba	k1gFnSc2	stavba
činil	činit	k5eAaImAgInS	činit
asi	asi	k9	asi
2	[number]	k4	2
433	[number]	k4	433
400	[number]	k4	400
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
stavbu	stavba	k1gFnSc4	stavba
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
přibližně	přibližně	k6eAd1	přibližně
2,3	[number]	k4	2,3
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
kamenných	kamenný	k2eAgInPc2d1	kamenný
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
váží	vážit	k5eAaImIp3nS	vážit
od	od	k7c2	od
2,5	[number]	k4	2,5
do	do	k7c2	do
15	[number]	k4	15
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
přeletů	přelet	k1gInPc2	přelet
nad	nad	k7c7	nad
Gízou	Gíza	k1gFnSc7	Gíza
pilot	pilota	k1gFnPc2	pilota
britských	britský	k2eAgFnPc2d1	britská
vojenských	vojenský	k2eAgFnPc2d1	vojenská
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
pořídil	pořídit	k5eAaPmAgMnS	pořídit
fotografii	fotografia	k1gFnSc4	fotografia
Velké	velký	k2eAgFnSc2d1	velká
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
pyramida	pyramida	k1gFnSc1	pyramida
má	mít	k5eAaImIp3nS	mít
nikoliv	nikoliv	k9	nikoliv
4	[number]	k4	4
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
8	[number]	k4	8
stěn	stěna	k1gFnPc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
ze	z	k7c2	z
4	[number]	k4	4
hlavních	hlavní	k2eAgFnPc2d1	hlavní
stěn	stěna	k1gFnPc2	stěna
je	být	k5eAaImIp3nS	být
středem	střed	k1gInSc7	střed
od	od	k7c2	od
úpatí	úpatí	k1gNnSc2	úpatí
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
souměrně	souměrně	k6eAd1	souměrně
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
primárně	primárně	k6eAd1	primárně
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
okamžicích	okamžik	k1gInPc6	okamžik
během	během	k7c2	během
dne	den	k1gInSc2	den
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
pohybu	pohyb	k1gInSc6	pohyb
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
již	již	k6eAd1	již
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
konkávní	konkávní	k2eAgMnSc1d1	konkávní
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
0,5	[number]	k4	0,5
<g/>
°	°	k?	°
až	až	k9	až
1	[number]	k4	1
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
pyramid	pyramida	k1gFnPc2	pyramida
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
specifický	specifický	k2eAgInSc1d1	specifický
tvar	tvar	k1gInSc1	tvar
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
u	u	k7c2	u
Mykerinovy	Mykerinův	k2eAgFnSc2d1	Mykerinova
pyramidy	pyramida	k1gFnSc2	pyramida
je	být	k5eAaImIp3nS	být
však	však	k9	však
nepatrně	patrně	k6eNd1	patrně
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
historika	historik	k1gMnSc2	historik
Hérodota	Hérodot	k1gMnSc2	Hérodot
pracovalo	pracovat	k5eAaImAgNnS	pracovat
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
v	v	k7c6	v
tříměsíčních	tříměsíční	k2eAgInPc6d1	tříměsíční
intervalech	interval	k1gInPc6	interval
okolo	okolo	k7c2	okolo
sta	sto	k4xCgNnSc2	sto
tisíc	tisíc	k4xCgInPc2	tisíc
otroků	otrok	k1gMnPc2	otrok
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
asi	asi	k9	asi
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
odborníci	odborník	k1gMnPc1	odborník
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
pracovalo	pracovat	k5eAaImAgNnS	pracovat
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
mluví	mluvit	k5eAaImIp3nP	mluvit
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
pouhých	pouhý	k2eAgFnPc2d1	pouhá
10	[number]	k4	10
tisících	tisící	k4xOgFnPc2	tisící
–	–	k?	–
ne	ne	k9	ne
však	však	k9	však
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rolníků	rolník	k1gMnPc2	rolník
a	a	k8xC	a
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1	kvalifikovaný
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
pyramidy	pyramida	k1gFnSc2	pyramida
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
dokonce	dokonce	k9	dokonce
i	i	k9	i
sídliště	sídliště	k1gNnSc4	sídliště
s	s	k7c7	s
několika	několik	k4yIc7	několik
tisíci	tisíc	k4xCgInPc7	tisíc
domy	dům	k1gInPc7	dům
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
při	při	k7c6	při
množství	množství	k1gNnSc6	množství
prací	práce	k1gFnPc2	práce
potřebných	potřebný	k2eAgInPc2d1	potřebný
k	k	k7c3	k
postavení	postavení	k1gNnSc3	postavení
takovéto	takovýto	k3xDgFnSc2	takovýto
stavby	stavba	k1gFnSc2	stavba
bylo	být	k5eAaImAgNnS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
uložit	uložit	k5eAaPmF	uložit
denně	denně	k6eAd1	denně
na	na	k7c4	na
své	své	k1gNnSc4	své
místo	místo	k7c2	místo
cca	cca	kA	cca
800	[number]	k4	800
t	t	k?	t
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
trvající	trvající	k2eAgFnSc1d1	trvající
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
badatelé	badatel	k1gMnPc1	badatel
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
mistrnou	mistrný	k2eAgFnSc4d1	mistrná
přesnost	přesnost	k1gFnSc4	přesnost
doprovázející	doprovázející	k2eAgFnSc4d1	doprovázející
tuto	tento	k3xDgFnSc4	tento
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
pochyby	pochyba	k1gFnPc4	pochyba
o	o	k7c6	o
skutečné	skutečný	k2eAgFnSc6d1	skutečná
délce	délka	k1gFnSc6	délka
<g/>
,	,	kIx,	,
období	období	k1gNnSc6	období
i	i	k8xC	i
technice	technika	k1gFnSc6	technika
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
nezodpovězená	zodpovězený	k2eNgFnSc1d1	nezodpovězená
otázka	otázka	k1gFnSc1	otázka
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgFnSc1d1	vlastní
technologie	technologie	k1gFnSc1	technologie
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
užití	užití	k1gNnSc2	užití
zvedacích	zvedací	k2eAgInPc2d1	zvedací
nástrojů	nástroj	k1gInPc2	nástroj
nebo	nebo	k8xC	nebo
použití	použití	k1gNnSc2	použití
ramp	rampa	k1gFnPc2	rampa
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
zvedacích	zvedací	k2eAgInPc2d1	zvedací
nástrojů	nástroj	k1gInPc2	nástroj
podporoval	podporovat	k5eAaImAgMnS	podporovat
i	i	k9	i
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
technologie	technologie	k1gFnSc1	technologie
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
pomalá	pomalý	k2eAgFnSc1d1	pomalá
a	a	k8xC	a
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
by	by	kYmCp3nS	by
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc1	použití
ramp	rampa	k1gFnPc2	rampa
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
pravděpodobnější	pravděpodobný	k2eAgMnSc1d2	pravděpodobnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
háček	háček	k1gInSc4	háček
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
délku	délka	k1gFnSc4	délka
ramp	rampa	k1gFnPc2	rampa
při	při	k7c6	při
postupující	postupující	k2eAgFnSc6d1	postupující
výšce	výška	k1gFnSc6	výška
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k8xC	jako
hrobka	hrobka	k1gFnSc1	hrobka
krále	král	k1gMnSc2	král
Chufua	Chufuus	k1gMnSc2	Chufuus
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vytesána	vytesán	k2eAgFnSc1d1	vytesána
také	také	k9	také
pohřební	pohřební	k2eAgFnSc1d1	pohřební
komora	komora	k1gFnSc1	komora
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
komora	komora	k1gFnSc1	komora
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgNnPc3d1	předchozí
obdobím	období	k1gNnPc3	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vytesána	vytesán	k2eAgFnSc1d1	vytesána
v	v	k7c6	v
samém	samý	k3xTgNnSc6	samý
nitru	nitro	k1gNnSc6	nitro
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
-	-	kIx~	-
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tři	tři	k4xCgInPc4	tři
-	-	kIx~	-
umístěné	umístěný	k2eAgInPc4d1	umístěný
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
výškách	výška	k1gFnPc6	výška
přibližně	přibližně	k6eAd1	přibližně
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
postupu	postup	k1gInSc3	postup
stavby	stavba	k1gFnSc2	stavba
pyramidy	pyramida	k1gFnSc2	pyramida
a	a	k8xC	a
obavám	obava	k1gFnPc3	obava
krále	král	k1gMnSc4	král
z	z	k7c2	z
předčasné	předčasný	k2eAgFnSc2d1	předčasná
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pravé	pravý	k2eAgFnSc2d1	pravá
pohřební	pohřební	k2eAgFnSc2d1	pohřební
komory	komora	k1gFnSc2	komora
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgMnSc1	třetí
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
10,4	[number]	k4	10,4
×	×	k?	×
5,2	[number]	k4	5,2
×	×	k?	×
5,8	[number]	k4	5,8
m	m	kA	m
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
chodba	chodba	k1gFnSc1	chodba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
Velkou	velký	k2eAgFnSc4d1	velká
galerii	galerie	k1gFnSc4	galerie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
45	[number]	k4	45
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
8,5	[number]	k4	8,5
metru	metr	k1gInSc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
stěny	stěna	k1gFnPc1	stěna
jsou	být	k5eAaImIp3nP	být
obloženy	obložen	k2eAgFnPc1d1	obložena
osmi	osm	k4xCc7	osm
vrstvami	vrstva	k1gFnPc7	vrstva
vápencových	vápencový	k2eAgInPc2d1	vápencový
bloků	blok	k1gInPc2	blok
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrchní	vrchní	k2eAgFnSc1d1	vrchní
vrstva	vrstva	k1gFnSc1	vrstva
překrývá	překrývat	k5eAaImIp3nS	překrývat
spodní	spodní	k2eAgFnSc1d1	spodní
tak	tak	k9	tak
těsně	těsně	k6eAd1	těsně
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
mezerou	mezera	k1gFnSc7	mezera
neprojde	projít	k5eNaPmIp3nS	projít
ani	ani	k8xC	ani
čepel	čepel	k1gFnSc4	čepel
nože	nůž	k1gInSc2	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Desky	deska	k1gFnPc1	deska
byly	být	k5eAaImAgFnP	být
opracovávány	opracovávat	k5eAaImNgFnP	opracovávat
pouze	pouze	k6eAd1	pouze
měděnými	měděný	k2eAgNnPc7d1	měděné
dláty	dláto	k1gNnPc7	dláto
<g/>
,	,	kIx,	,
pískem	písek	k1gInSc7	písek
a	a	k8xC	a
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zdi	zeď	k1gFnPc1	zeď
<g/>
,	,	kIx,	,
strop	strop	k1gInSc1	strop
a	a	k8xC	a
zatarasující	zatarasující	k2eAgInPc1d1	zatarasující
bloky	blok	k1gInPc1	blok
byly	být	k5eAaImAgInP	být
vystavěny	vystavěn	k2eAgInPc1d1	vystavěn
z	z	k7c2	z
granitové	granitový	k2eAgFnSc2d1	granitová
žuly	žula	k1gFnSc2	žula
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
pohřební	pohřební	k2eAgFnSc7d1	pohřební
komorou	komora	k1gFnSc7	komora
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
obav	obava	k1gFnPc2	obava
architektů	architekt	k1gMnPc2	architekt
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
dutiny	dutina	k1gFnPc1	dutina
aby	aby	kYmCp3nP	aby
zmenšily	zmenšit	k5eAaPmAgFnP	zmenšit
tlak	tlak	k1gInSc4	tlak
působící	působící	k2eAgInSc4d1	působící
na	na	k7c4	na
strop	strop	k1gInSc4	strop
pohřební	pohřební	k2eAgFnSc2d1	pohřební
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohřební	pohřební	k2eAgFnSc6d1	pohřební
komoře	komora	k1gFnSc6	komora
dodnes	dodnes	k6eAd1	dodnes
stojí	stát	k5eAaImIp3nS	stát
sarkofág	sarkofág	k1gInSc1	sarkofág
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
žulového	žulový	k2eAgInSc2d1	žulový
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgInS	být
dopraven	dopravit	k5eAaPmNgInS	dopravit
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
by	by	kYmCp3nS	by
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
velikosti	velikost	k1gFnSc3	velikost
neprošel	projít	k5eNaPmAgInS	projít
pozdějším	pozdní	k2eAgInSc7d2	pozdější
vchodem	vchod	k1gInSc7	vchod
<g/>
.	.	kIx.	.
</s>
<s>
Královo	Králův	k2eAgNnSc1d1	Královo
tělo	tělo	k1gNnSc1	tělo
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
už	už	k6eAd1	už
před	před	k7c7	před
mnoha	mnoho	k4c7	mnoho
sty	sto	k4xCgNnPc7	sto
lety	léto	k1gNnPc7	léto
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
Střední	střední	k2eAgFnSc1d1	střední
Říše	říše	k1gFnSc1	říše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
bohatá	bohatý	k2eAgFnSc1d1	bohatá
pohřební	pohřební	k2eAgFnSc1d1	pohřební
výbava	výbava	k1gFnSc1	výbava
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
pyramidu	pyramida	k1gFnSc4	pyramida
obklopoval	obklopovat	k5eAaImAgMnS	obklopovat
komplex	komplex	k1gInSc4	komplex
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
staveb	stavba	k1gFnPc2	stavba
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
zachovaly	zachovat	k5eAaPmAgInP	zachovat
jen	jen	k9	jen
zbytky	zbytek	k1gInPc1	zbytek
zádušního	zádušní	k2eAgInSc2d1	zádušní
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
satelitní	satelitní	k2eAgInPc4d1	satelitní
pyramidy	pyramid	k1gInPc4	pyramid
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgInPc4d1	patřící
zřejmě	zřejmě	k6eAd1	zřejmě
jeho	jeho	k3xOp3gFnPc3	jeho
manželkám	manželka	k1gFnPc3	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Pyramida	pyramida	k1gFnSc1	pyramida
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
poškozena	poškodit	k5eAaPmNgFnS	poškodit
lupiči	lupič	k1gMnPc7	lupič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
opravena	opravit	k5eAaPmNgFnS	opravit
králi	král	k1gMnSc3	král
26	[number]	k4	26
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Egypta	Egypt	k1gInSc2	Egypt
Araby	Arab	k1gMnPc4	Arab
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
820	[number]	k4	820
otevřel	otevřít	k5eAaPmAgMnS	otevřít
kalif	kalif	k1gMnSc1	kalif
Al-Monzun	Al-Monzun	k1gMnSc1	Al-Monzun
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
18	[number]	k4	18
metrů	metr	k1gInPc2	metr
dodnes	dodnes	k6eAd1	dodnes
používaný	používaný	k2eAgInSc1d1	používaný
vchod	vchod	k1gInSc1	vchod
<g/>
.	.	kIx.	.
</s>
