<p>
<s>
Perníkový	perníkový	k2eAgMnSc1d1	perníkový
táta	táta	k1gMnSc1	táta
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Breaking	Breaking	k1gInSc1	Breaking
Bad	Bad	k1gFnSc2	Bad
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
poprvé	poprvé	k6eAd1	poprvé
vysílaný	vysílaný	k2eAgInSc1d1	vysílaný
v	v	k7c6	v
USA	USA	kA	USA
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
na	na	k7c6	na
kabelové	kabelový	k2eAgFnSc6d1	kabelová
televizní	televizní	k2eAgFnSc6d1	televizní
stanici	stanice	k1gFnSc6	stanice
AMC	AMC	kA	AMC
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
sezóna	sezóna	k1gFnSc1	sezóna
měla	mít	k5eAaImAgFnS	mít
7	[number]	k4	7
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
13	[number]	k4	13
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
pak	pak	k6eAd1	pak
13	[number]	k4	13
<g/>
,	,	kIx,	,
13	[number]	k4	13
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Albuquerque	Albuquerqu	k1gInSc2	Albuquerqu
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
epizoda	epizoda	k1gFnSc1	epizoda
seriálu	seriál	k1gInSc2	seriál
byla	být	k5eAaImAgFnS	být
odvysílána	odvysílán	k2eAgFnSc1d1	odvysílána
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
středoškolský	středoškolský	k2eAgMnSc1d1	středoškolský
učitel	učitel	k1gMnSc1	učitel
chemie	chemie	k1gFnSc2	chemie
Walter	Walter	k1gMnSc1	Walter
White	Whit	k1gInSc5	Whit
(	(	kIx(	(
<g/>
Bryan	Bryana	k1gFnPc2	Bryana
Cranston	Cranston	k1gInSc1	Cranston
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
žije	žít	k5eAaImIp3nS	žít
společně	společně	k6eAd1	společně
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
těhotnou	těhotný	k2eAgFnSc7d1	těhotná
ženou	žena	k1gFnSc7	žena
Skyler	Skyler	k1gMnSc1	Skyler
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
Gunn	Gunn	k1gMnSc1	Gunn
<g/>
)	)	kIx)	)
a	a	k8xC	a
tělesně	tělesně	k6eAd1	tělesně
postiženým	postižený	k2eAgMnSc7d1	postižený
synem	syn	k1gMnSc7	syn
Waltem	Walt	k1gMnSc7	Walt
jr	jr	k?	jr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
RJ	RJ	kA	RJ
Mitte	Mitt	k1gInSc5	Mitt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
Walt	Walt	k1gMnSc1	Walt
se	se	k3xPyFc4	se
hroutí	hroutit	k5eAaImIp3nS	hroutit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
rakovina	rakovina	k1gFnSc1	rakovina
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
začíná	začínat	k5eAaImIp3nS	začínat
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bývalým	bývalý	k2eAgMnSc7d1	bývalý
studentem	student	k1gMnSc7	student
Jesse	Jesse	k1gFnSc2	Jesse
Pinkmanem	Pinkman	k1gMnSc7	Pinkman
(	(	kIx(	(
<g/>
Aaron	Aaron	k1gMnSc1	Aaron
Paul	Paul	k1gMnSc1	Paul
<g/>
)	)	kIx)	)
připravovat	připravovat	k5eAaImF	připravovat
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začnou	začít	k5eAaPmIp3nP	začít
vařit	vařit	k5eAaImF	vařit
metamfetamin	metamfetamin	k1gInSc4	metamfetamin
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gMnSc1	Walt
si	se	k3xPyFc3	se
vytyčil	vytyčit	k5eAaPmAgMnS	vytyčit
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
finančně	finančně	k6eAd1	finančně
zabezpečí	zabezpečit	k5eAaPmIp3nS	zabezpečit
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
Breaking	Breaking	k1gInSc1	Breaking
Bad	Bad	k1gMnSc1	Bad
si	se	k3xPyFc3	se
vydobyl	vydobýt	k5eAaPmAgMnS	vydobýt
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
uznání	uznání	k1gNnSc4	uznání
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
krom	krom	k7c2	krom
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgNnPc2d1	další
ocenění	ocenění	k1gNnPc2	ocenění
i	i	k8xC	i
dvě	dva	k4xCgFnPc4	dva
ceny	cena	k1gFnPc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
seriál	seriál	k1gInSc1	seriál
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
statistikách	statistika	k1gFnPc6	statistika
nejvíce	nejvíce	k6eAd1	nejvíce
pirátsky	pirátsky	k6eAd1	pirátsky
stahovaných	stahovaný	k2eAgInPc2d1	stahovaný
seriálů	seriál	k1gInPc2	seriál
hned	hned	k6eAd1	hned
za	za	k7c7	za
Hrou	hra	k1gFnSc7	hra
o	o	k7c4	o
trůny	trůn	k1gInPc4	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnSc1	první
sezóna	sezóna	k1gFnSc1	sezóna
==	==	k?	==
</s>
</p>
<p>
<s>
Učitel	učitel	k1gMnSc1	učitel
chemie	chemie	k1gFnSc2	chemie
Walter	Walter	k1gMnSc1	Walter
White	Whit	k1gInSc5	Whit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
těhotnou	těhotný	k2eAgFnSc7d1	těhotná
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
po	po	k7c6	po
mozkové	mozkový	k2eAgFnSc6d1	mozková
obrně	obrna	k1gFnSc6	obrna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
rakovinu	rakovina	k1gFnSc4	rakovina
plic	plíce	k1gFnPc2	plíce
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
stádiu	stádium	k1gNnSc6	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
měl	mít	k5eAaImAgMnS	mít
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
peníze	peníz	k1gInPc4	peníz
získá	získat	k5eAaPmIp3nS	získat
výrobou	výroba	k1gFnSc7	výroba
a	a	k8xC	a
prodejem	prodej	k1gInSc7	prodej
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
se	se	k3xPyFc4	se
dohromady	dohromady	k6eAd1	dohromady
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bývalým	bývalý	k2eAgMnSc7d1	bývalý
studentem	student	k1gMnSc7	student
Jesse	Jesse	k1gFnSc2	Jesse
Pinkmanem	Pinkman	k1gMnSc7	Pinkman
a	a	k8xC	a
díky	díky	k7c3	díky
Waltovým	Waltův	k2eAgFnPc3d1	Waltova
výborným	výborný	k2eAgFnPc3d1	výborná
znalostem	znalost	k1gFnPc3	znalost
chemie	chemie	k1gFnSc2	chemie
společně	společně	k6eAd1	společně
vaří	vařit	k5eAaImIp3nP	vařit
výjimečně	výjimečně	k6eAd1	výjimečně
čistý	čistý	k2eAgInSc4d1	čistý
metamfetamin	metamfetamin	k1gInSc4	metamfetamin
<g/>
.	.	kIx.	.
</s>
<s>
Drogu	droga	k1gFnSc4	droga
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
v	v	k7c6	v
mobilní	mobilní	k2eAgFnSc6d1	mobilní
laboratoři	laboratoř	k1gFnSc6	laboratoř
(	(	kIx(	(
<g/>
v	v	k7c6	v
karavanu	karavan	k1gInSc6	karavan
<g/>
,	,	kIx,	,
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
za	za	k7c7	za
městem	město	k1gNnSc7	město
<g/>
)	)	kIx)	)
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
laboratorního	laboratorní	k2eAgNnSc2d1	laboratorní
nádobí	nádobí	k1gNnSc2	nádobí
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Walt	Walt	k1gMnSc1	Walt
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
ze	z	k7c2	z
školní	školní	k2eAgFnSc2d1	školní
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
tak	tak	k6eAd1	tak
žije	žít	k5eAaImIp3nS	žít
dvojí	dvojí	k4xRgFnSc4	dvojí
existenci	existence	k1gFnSc4	existence
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nadále	nadále	k6eAd1	nadále
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
rodinného	rodinný	k2eAgInSc2d1	rodinný
i	i	k8xC	i
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
jde	jít	k5eAaImIp3nS	jít
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	on	k3xPp3gInPc4	on
napadnou	napadnout	k5eAaPmIp3nP	napadnout
dva	dva	k4xCgMnPc1	dva
dealeři	dealer	k1gMnPc1	dealer
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dříve	dříve	k6eAd2	dříve
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
Jessem	Jess	k1gInSc7	Jess
<g/>
.	.	kIx.	.
</s>
<s>
Obviňují	obviňovat	k5eAaImIp3nP	obviňovat
Walta	Walto	k1gNnSc2	Walto
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
pro	pro	k7c4	pro
DEA	DEA	kA	DEA
(	(	kIx(	(
<g/>
protidrogový	protidrogový	k2eAgInSc1d1	protidrogový
úřad	úřad	k1gInSc1	úřad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k5eAaPmF	Walt
jim	on	k3xPp3gInPc3	on
však	však	k9	však
slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
oba	dva	k4xCgMnPc1	dva
naučí	naučit	k5eAaPmIp3nP	naučit
recept	recept	k1gInSc4	recept
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
metamfetamin	metamfetamin	k1gInSc4	metamfetamin
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	on	k3xPp3gInPc4	on
nechají	nechat	k5eAaPmIp3nP	nechat
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
znovu	znovu	k6eAd1	znovu
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
svoje	svůj	k3xOyFgFnPc4	svůj
znalosti	znalost	k1gFnPc4	znalost
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
použije	použít	k5eAaPmIp3nS	použít
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
fosfan	fosfan	k1gInSc4	fosfan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
menším	malý	k2eAgInSc6d2	menší
výbuchu	výbuch	k1gInSc6	výbuch
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
utéct	utéct	k5eAaPmF	utéct
z	z	k7c2	z
karavanu	karavan	k1gInSc2	karavan
<g/>
,	,	kIx,	,
zavírá	zavírat	k5eAaImIp3nS	zavírat
omráčené	omráčený	k2eAgMnPc4d1	omráčený
dealery	dealer	k1gMnPc4	dealer
uvnitř	uvnitř	k6eAd1	uvnitř
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
až	až	k6eAd1	až
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc4	dva
otráví	otrávit	k5eAaPmIp3nP	otrávit
jedovatými	jedovatý	k2eAgInPc7d1	jedovatý
výpary	výpar	k1gInPc7	výpar
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
dealeři	dealer	k1gMnPc1	dealer
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
,	,	kIx,	,
odváží	odvázat	k5eAaPmIp3nP	odvázat
Walt	Walt	k1gInSc4	Walt
zraněného	zraněný	k2eAgMnSc2d1	zraněný
Jesseho	Jesse	k1gMnSc2	Jesse
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
s	s	k7c7	s
Jessem	Jess	k1gInSc7	Jess
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
dealerů	dealer	k1gMnPc2	dealer
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
naživu	naživu	k6eAd1	naživu
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
se	se	k3xPyFc4	se
uvěznit	uvěznit	k5eAaPmF	uvěznit
ho	on	k3xPp3gNnSc4	on
v	v	k7c6	v
Jesseho	Jesse	k1gMnSc2	Jesse
sklepě	sklep	k1gInSc6	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
teď	teď	k6eAd1	teď
stojí	stát	k5eAaImIp3nS	stát
dva	dva	k4xCgInPc4	dva
nelehké	lehký	k2eNgInPc4d1	nelehký
úkoly	úkol	k1gInPc4	úkol
-	-	kIx~	-
musejí	muset	k5eAaImIp3nP	muset
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
tělo	tělo	k1gNnSc4	tělo
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
dealera	dealer	k1gMnSc2	dealer
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
zbavit	zbavit	k5eAaPmF	zbavit
i	i	k9	i
toho	ten	k3xDgNnSc2	ten
živého	živé	k1gNnSc2	živé
<g/>
.	.	kIx.	.
</s>
<s>
Úkoly	úkol	k1gInPc1	úkol
si	se	k3xPyFc3	se
rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
hozením	hození	k1gNnSc7	hození
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jesse	Jesse	k6eAd1	Jesse
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
zbavit	zbavit	k5eAaPmF	zbavit
mrtvoly	mrtvola	k1gFnPc4	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
mu	on	k3xPp3gMnSc3	on
poradí	poradit	k5eAaPmIp3nS	poradit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tělo	tělo	k1gNnSc4	tělo
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
kyselinou	kyselina	k1gFnSc7	kyselina
fluorovodíkovou	fluorovodíkový	k2eAgFnSc7d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Řekne	říct	k5eAaPmIp3nS	říct
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
těla	tělo	k1gNnSc2	tělo
koupil	koupit	k5eAaPmAgInS	koupit
velkou	velký	k2eAgFnSc4d1	velká
plastovou	plastový	k2eAgFnSc4d1	plastová
nádobu	nádoba	k1gFnSc4	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1	fluorovodíková
totiž	totiž	k8xC	totiž
reaguje	reagovat	k5eAaBmIp3nS	reagovat
téměř	téměř	k6eAd1	téměř
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Jesse	Jesse	k6eAd1	Jesse
však	však	k9	však
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
nenajde	najít	k5eNaPmIp3nS	najít
žádnou	žádný	k3yNgFnSc4	žádný
nádobu	nádoba	k1gFnSc4	nádoba
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tělo	tělo	k1gNnSc1	tělo
vešlo	vejít	k5eAaPmAgNnS	vejít
<g/>
,	,	kIx,	,
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
tělo	tělo	k1gNnSc4	tělo
u	u	k7c2	u
sebe	se	k3xPyFc2	se
ve	v	k7c6	v
vaně	vana	k1gFnSc6	vana
<g/>
.	.	kIx.	.
</s>
<s>
Zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
však	však	k9	však
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vana	vana	k1gFnSc1	vana
je	být	k5eAaImIp3nS	být
kovová	kovový	k2eAgFnSc1d1	kovová
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
se	se	k3xPyFc4	se
zanedlouho	zanedlouho	k6eAd1	zanedlouho
prožere	prožrat	k5eAaPmIp3nS	prožrat
vanou	vana	k1gFnSc7	vana
a	a	k8xC	a
rozleptané	rozleptaný	k2eAgNnSc1d1	rozleptané
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
propadne	propadnout	k5eAaPmIp3nS	propadnout
podlahou	podlaha	k1gFnSc7	podlaha
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Walt	Walt	k1gInSc1	Walt
stále	stále	k6eAd1	stále
váhá	váhat	k5eAaImIp3nS	váhat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
má	mít	k5eAaImIp3nS	mít
druhého	druhý	k4xOgMnSc4	druhý
dealera	dealer	k1gMnSc4	dealer
zabít	zabít	k5eAaPmF	zabít
nebo	nebo	k8xC	nebo
propustit	propustit	k5eAaPmF	propustit
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
jeho	jeho	k3xOp3gFnPc4	jeho
potřeby	potřeba	k1gFnPc4	potřeba
a	a	k8xC	a
sbližuje	sbližovat	k5eAaImIp3nS	sbližovat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
dealer	dealer	k1gMnSc1	dealer
ve	v	k7c6	v
sklepě	sklep	k1gInSc6	sklep
chystal	chystat	k5eAaImAgMnS	chystat
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
Walt	Walt	k1gInSc1	Walt
přestane	přestat	k5eAaPmIp3nS	přestat
váhat	váhat	k5eAaImF	váhat
a	a	k8xC	a
uškrtí	uškrtit	k5eAaPmIp3nP	uškrtit
jej	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
odklízení	odklízení	k1gNnSc2	odklízení
obou	dva	k4xCgNnPc2	dva
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
si	se	k3xPyFc3	se
Hank	Hank	k1gMnSc1	Hank
(	(	kIx(	(
<g/>
Waltův	Waltův	k2eAgMnSc1d1	Waltův
švagr	švagr	k1gMnSc1	švagr
<g/>
,	,	kIx,	,
pracující	pracující	k1gMnSc1	pracující
pro	pro	k7c4	pro
DEA	DEA	kA	DEA
<g/>
)	)	kIx)	)
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgMnSc1d1	nový
a	a	k8xC	a
významný	významný	k2eAgMnSc1d1	významný
výrobce	výrobce	k1gMnSc1	výrobce
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Hank	Hank	k1gMnSc1	Hank
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
Waltovy	Waltův	k2eAgFnSc2d1	Waltova
školní	školní	k2eAgFnSc2d1	školní
laboratoře	laboratoř	k1gFnSc2	laboratoř
někdo	někdo	k3yInSc1	někdo
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
laboratorní	laboratorní	k2eAgNnSc4d1	laboratorní
nádobí	nádobí	k1gNnSc4	nádobí
<g/>
,	,	kIx,	,
potřebné	potřebné	k1gNnSc4	potřebné
k	k	k7c3	k
vaření	vaření	k1gNnSc3	vaření
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Walt	Walt	k1gMnSc1	Walt
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
manželce	manželka	k1gFnSc3	manželka
Skyler	Skyler	k1gInSc4	Skyler
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
nechce	chtít	k5eNaImIp3nS	chtít
podstoupit	podstoupit	k5eAaPmF	podstoupit
léčbu	léčba	k1gFnSc4	léčba
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
jeho	on	k3xPp3gInSc4	on
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
chemoterapii	chemoterapie	k1gFnSc4	chemoterapie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Jessem	Jess	k1gInSc7	Jess
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
si	se	k3xPyFc3	se
role	role	k1gFnPc4	role
<g/>
:	:	kIx,	:
Jesse-prodejce	Jesserodejec	k1gMnPc4	Jesse-prodejec
<g/>
,	,	kIx,	,
Walt-vařič	Waltařič	k1gInSc1	Walt-vařič
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Walt	Walt	k1gInSc1	Walt
posílá	posílat	k5eAaImIp3nS	posílat
Jesseho	Jesse	k1gMnSc4	Jesse
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
s	s	k7c7	s
Tucem	Tuc	k1gInSc7	Tuc
<g/>
.	.	kIx.	.
</s>
<s>
Tuco	Tuco	k1gMnSc1	Tuco
je	být	k5eAaImIp3nS	být
násilný	násilný	k2eAgMnSc1d1	násilný
psychopat	psychopat	k1gMnSc1	psychopat
<g/>
,	,	kIx,	,
řídící	řídící	k2eAgInSc4d1	řídící
místní	místní	k2eAgInSc4d1	místní
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Jesse	Jesse	k1gFnSc1	Jesse
požaduje	požadovat	k5eAaImIp3nS	požadovat
peníze	peníz	k1gInPc4	peníz
za	za	k7c4	za
nabízenou	nabízený	k2eAgFnSc4d1	nabízená
dávku	dávka	k1gFnSc4	dávka
<g/>
,	,	kIx,	,
Tuco	Tuco	k6eAd1	Tuco
ho	on	k3xPp3gMnSc4	on
začne	začít	k5eAaPmIp3nS	začít
zběsile	zběsile	k6eAd1	zběsile
mlátit	mlátit	k5eAaImF	mlátit
a	a	k8xC	a
kopat	kopat	k5eAaImF	kopat
<g/>
.	.	kIx.	.
</s>
<s>
Jesse	Jesse	k1gFnSc1	Jesse
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ho	on	k3xPp3gMnSc4	on
přijde	přijít	k5eAaPmIp3nS	přijít
navštívit	navštívit	k5eAaPmF	navštívit
Walt	Walt	k1gInSc4	Walt
<g/>
.	.	kIx.	.
</s>
<s>
Slíbí	slíbit	k5eAaPmIp3nS	slíbit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
peníze	peníz	k1gInPc1	peníz
od	od	k7c2	od
Tuca	Tuc	k1gInSc2	Tuc
přinese	přinést	k5eAaPmIp3nS	přinést
<g/>
.	.	kIx.	.
</s>
<s>
Navštíví	navštívit	k5eAaPmIp3nS	navštívit
tedy	tedy	k9	tedy
Tuca	Tuca	k1gFnSc1	Tuca
a	a	k8xC	a
požaduje	požadovat	k5eAaImIp3nS	požadovat
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
peníze	peníz	k1gInPc1	peníz
za	za	k7c4	za
předchozí	předchozí	k2eAgFnSc4d1	předchozí
dávku	dávka	k1gFnSc4	dávka
a	a	k8xC	a
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
zranění	zranění	k1gNnSc4	zranění
Jesseho	Jesse	k1gMnSc2	Jesse
<g/>
.	.	kIx.	.
</s>
<s>
Tucovi	Tuca	k1gMnSc3	Tuca
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nelíbí	líbit	k5eNaImIp3nS	líbit
a	a	k8xC	a
chystá	chystat	k5eAaImIp3nS	chystat
se	se	k3xPyFc4	se
Walta	Walt	k1gMnSc2	Walt
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
tak	tak	k6eAd1	tak
učiní	učinit	k5eAaPmIp3nS	učinit
<g/>
,	,	kIx,	,
Walt	Walt	k1gInSc1	Walt
způsobí	způsobit	k5eAaPmIp3nS	způsobit
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kanceláři	kancelář	k1gFnSc6	kancelář
tlakovou	tlakový	k2eAgFnSc4d1	tlaková
vlnu	vlna	k1gFnSc4	vlna
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
třaskavé	třaskavý	k2eAgFnSc2d1	třaskavá
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Vystrašený	vystrašený	k2eAgMnSc1d1	vystrašený
Tuco	Tuco	k1gMnSc1	Tuco
mu	on	k3xPp3gMnSc3	on
poté	poté	k6eAd1	poté
předává	předávat	k5eAaImIp3nS	předávat
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
slibuje	slibovat	k5eAaImIp3nS	slibovat
mu	on	k3xPp3gMnSc3	on
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
možnost	možnost	k1gFnSc4	možnost
dalšího	další	k2eAgInSc2d1	další
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Jesse	Jesse	k1gFnPc1	Jesse
zotaví	zotavit	k5eAaPmIp3nP	zotavit
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
znovu	znovu	k6eAd1	znovu
vařit	vařit	k5eAaImF	vařit
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
ale	ale	k8xC	ale
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
vaření	vaření	k1gNnSc3	vaření
sud	suda	k1gFnPc2	suda
s	s	k7c7	s
látkou	látka	k1gFnSc7	látka
(	(	kIx(	(
<g/>
methylamin	methylamin	k1gInSc1	methylamin
<g/>
)	)	kIx)	)
ukradenou	ukradený	k2eAgFnSc4d1	ukradená
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
anilin	anilin	k1gInSc4	anilin
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
volně	volně	k6eAd1	volně
prodejných	prodejný	k2eAgInPc2d1	prodejný
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nahradí	nahradit	k5eAaPmIp3nS	nahradit
některé	některý	k3yIgFnSc3	některý
části	část	k1gFnSc3	část
receptu	recept	k1gInSc2	recept
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
umožní	umožnit	k5eAaPmIp3nS	umožnit
produkovat	produkovat	k5eAaImF	produkovat
až	až	k9	až
4	[number]	k4	4
<g/>
×	×	k?	×
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
drogy	droga	k1gFnSc2	droga
než	než	k8xS	než
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
sezóna	sezóna	k1gFnSc1	sezóna
==	==	k?	==
</s>
</p>
<p>
<s>
Waltera	Walter	k1gMnSc4	Walter
dále	daleko	k6eAd2	daleko
tíží	tížit	k5eAaImIp3nS	tížit
vysoké	vysoký	k2eAgInPc4d1	vysoký
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
několika	několik	k4yIc3	několik
špatným	špatný	k2eAgFnPc3d1	špatná
zkušenostem	zkušenost	k1gFnPc3	zkušenost
stále	stále	k6eAd1	stále
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Jessem	Jess	k1gInSc7	Jess
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
metamfetaminu	metamfetamin	k1gInSc2	metamfetamin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepřestávají	přestávat	k5eNaImIp3nP	přestávat
lepit	lepit	k5eAaImF	lepit
na	na	k7c4	na
paty	pata	k1gFnPc4	pata
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejich	jejich	k3xOp3gMnPc2	jejich
dealerů	dealer	k1gMnPc2	dealer
<g/>
,	,	kIx,	,
Badger	Badger	k1gInSc4	Badger
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
drogy	droga	k1gFnSc2	droga
a	a	k8xC	a
Walter	Walter	k1gMnSc1	Walter
najme	najmout	k5eAaPmIp3nS	najmout
právníka	právník	k1gMnSc2	právník
–	–	k?	–
Saula	Saul	k1gMnSc2	Saul
Goodmana	Goodman	k1gMnSc2	Goodman
–	–	k?	–
aby	aby	kYmCp3nS	aby
Badgerovi	Badger	k1gMnSc3	Badger
pomohl	pomoct	k5eAaPmAgMnS	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
Walter	Walter	k1gMnSc1	Walter
získal	získat	k5eAaPmAgMnS	získat
větší	veliký	k2eAgFnSc4d2	veliký
hotovost	hotovost	k1gFnSc4	hotovost
tak	tak	k6eAd1	tak
s	s	k7c7	s
Jessem	Jess	k1gInSc7	Jess
odjedou	odjet	k5eAaPmIp3nP	odjet
karavanem	karavan	k1gInSc7	karavan
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
do	do	k7c2	do
pouště	poušť	k1gFnSc2	poušť
vařit	vařit	k5eAaImF	vařit
perník	perník	k1gInSc4	perník
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
další	další	k2eAgNnSc1d1	další
z	z	k7c2	z
jejich	jejich	k3xOp3gMnPc2	jejich
dealerů	dealer	k1gMnPc2	dealer
<g/>
,	,	kIx,	,
Kombo	Komba	k1gFnSc5	Komba
<g/>
,	,	kIx,	,
zastřelen	zastřelen	k2eAgInSc1d1	zastřelen
konkurenčním	konkurenční	k2eAgInSc7d1	konkurenční
gangem	gang	k1gInSc7	gang
při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
perníku	perník	k1gInSc2	perník
v	v	k7c6	v
cizím	cizí	k2eAgInSc6d1	cizí
rajónu	rajón	k1gInSc6	rajón
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
neustálým	neustálý	k2eAgInPc3d1	neustálý
problémům	problém	k1gInPc3	problém
při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
jim	on	k3xPp3gMnPc3	on
Saul	Saul	k1gMnSc1	Saul
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
změnili	změnit	k5eAaPmAgMnP	změnit
způsob	způsob	k1gInSc4	způsob
distribuce	distribuce	k1gFnSc2	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Jesse	Jesse	k6eAd1	Jesse
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
domácí	domácí	k2eAgFnPc4d1	domácí
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
zotavuje	zotavovat	k5eAaImIp3nS	zotavovat
ze	z	k7c2	z
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
Jessem	Jesso	k1gNnSc7	Jesso
začnou	začít	k5eAaPmIp3nP	začít
brát	brát	k5eAaImF	brát
heroin	heroin	k1gInSc4	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Saul	Saul	k1gMnSc1	Saul
najde	najít	k5eAaPmIp3nS	najít
obchodního	obchodní	k2eAgMnSc4d1	obchodní
partnera	partner	k1gMnSc4	partner
<g/>
,	,	kIx,	,
Guse	Gus	k1gMnSc4	Gus
Fringa	Fring	k1gMnSc4	Fring
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jim	on	k3xPp3gMnPc3	on
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
1,2	[number]	k4	1,2
mil	míle	k1gFnPc2	míle
USD	USD	kA	USD
za	za	k7c4	za
38	[number]	k4	38
liber	libra	k1gFnPc2	libra
pervitinu	pervitin	k1gInSc2	pervitin
vyrobeného	vyrobený	k2eAgInSc2d1	vyrobený
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gInSc1	Walt
zboží	zboží	k1gNnSc2	zboží
předá	předat	k5eAaPmIp3nS	předat
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prošvihne	prošvihnout	k5eAaPmIp3nS	prošvihnout
narození	narození	k1gNnSc1	narození
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
Jessemu	Jessema	k1gFnSc4	Jessema
předat	předat	k5eAaPmF	předat
polovinu	polovina	k1gFnSc4	polovina
peněz	peníze	k1gInPc2	peníze
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
Jane	Jan	k1gMnSc5	Jan
začne	začít	k5eAaPmIp3nS	začít
Walta	Walt	k1gMnSc2	Walt
vydírat	vydírat	k5eAaImF	vydírat
a	a	k8xC	a
tak	tak	k6eAd1	tak
jim	on	k3xPp3gMnPc3	on
peníze	peníz	k1gInPc4	peníz
nakonec	nakonec	k6eAd1	nakonec
předá	předat	k5eAaPmIp3nS	předat
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gInSc1	Walt
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Jesseho	Jesse	k1gMnSc2	Jesse
dům	dům	k1gInSc1	dům
a	a	k8xC	a
vidí	vidět	k5eAaImIp3nS	vidět
Jane	Jan	k1gMnSc5	Jan
dusit	dusit	k5eAaImF	dusit
se	s	k7c7	s
zvratky	zvratek	k1gInPc7	zvratek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepomůže	pomoct	k5eNaPmIp3nS	pomoct
jí	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Jesse	Jesse	k6eAd1	Jesse
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
série	série	k1gFnSc2	série
si	se	k3xPyFc3	se
Skyler	Skyler	k1gInSc1	Skyler
začne	začít	k5eAaPmIp3nS	začít
dávat	dávat	k5eAaImF	dávat
dohromady	dohromady	k6eAd1	dohromady
všechny	všechen	k3xTgFnPc4	všechen
Waltovy	Waltův	k2eAgFnPc4d1	Waltova
nepřítomnosti	nepřítomnost	k1gFnPc4	nepřítomnost
a	a	k8xC	a
neustálé	neustálý	k2eAgFnPc4d1	neustálá
výmluvy	výmluva	k1gFnPc4	výmluva
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vaří	vařit	k5eAaImIp3nS	vařit
metamfetamin	metamfetamin	k2eAgMnSc1d1	metamfetamin
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
rozchodu	rozchod	k1gInSc6	rozchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spin-off	Spinff	k1gInSc4	Spin-off
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
představitelé	představitel	k1gMnPc1	představitel
stanice	stanice	k1gFnSc2	stanice
AMC	AMC	kA	AMC
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
spin-off	spinff	k1gInSc4	spin-off
seriálu	seriál	k1gInSc2	seriál
s	s	k7c7	s
názvem	název	k1gInSc7	název
Volejte	volat	k5eAaImRp2nP	volat
Saulovi	Saulův	k2eAgMnPc1d1	Saulův
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Perníkový	perníkový	k2eAgMnSc1d1	perníkový
táta	táta	k1gMnSc1	táta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Breaking	Breaking	k1gInSc1	Breaking
Bad	Bad	k1gFnSc2	Bad
na	na	k7c6	na
Comic-Conu	Comic-Con	k1gInSc6	Comic-Con
2013	[number]	k4	2013
</s>
</p>
