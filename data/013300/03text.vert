<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Tůma	Tůma	k1gMnSc1	Tůma
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1843	[number]	k4	1843
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
1917	[number]	k4	1917
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Vršovicích	Vršovice	k1gFnPc6	Vršovice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
sentimentálních	sentimentální	k2eAgFnPc2d1	sentimentální
politických	politický	k2eAgFnPc2d1	politická
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
gymnázií	gymnázium	k1gNnPc2	gymnázium
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
Písku	Písek	k1gInSc2	Písek
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
polytechnice	polytechnika	k1gFnSc6	polytechnika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc4	tento
studium	studium	k1gNnSc4	studium
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
být	být	k5eAaImF	být
novinářem	novinář	k1gMnSc7	novinář
a	a	k8xC	a
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
r.	r.	kA	r.
<g/>
1862	[number]	k4	1862
redaktorem	redaktor	k1gMnSc7	redaktor
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jím	jíst	k5eAaImIp1nS	jíst
45	[number]	k4	45
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
politické	politický	k2eAgInPc4d1	politický
úvodníky	úvodník	k1gInPc4	úvodník
a	a	k8xC	a
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
souzen	soudit	k5eAaImNgInS	soudit
a	a	k8xC	a
na	na	k7c4	na
17	[number]	k4	17
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c4	v
období	období	k1gNnSc4	období
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
uvězněn	uvěznit	k5eAaPmNgInS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Přítel	přítel	k1gMnSc1	přítel
J.	J.	kA	J.
Grégra	Grégra	k1gMnSc1	Grégra
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Sladkovského	sladkovský	k2eAgInSc2d1	sladkovský
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Baráka	Barák	k1gMnSc2	Barák
<g/>
.	.	kIx.	.
</s>
<s>
Přispíval	přispívat	k5eAaImAgMnS	přispívat
do	do	k7c2	do
časopisů	časopis	k1gInPc2	časopis
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Květy	květ	k1gInPc1	květ
<g/>
,	,	kIx,	,
Hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
Osvěta	osvěta	k1gFnSc1	osvěta
<g/>
,	,	kIx,	,
Humoristické	humoristický	k2eAgInPc4d1	humoristický
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
poslancem	poslanec	k1gMnSc7	poslanec
strany	strana	k1gFnSc2	strana
mladočeské	mladočeský	k2eAgFnPc1d1	mladočeská
v	v	k7c4	v
období	období	k1gNnSc4	období
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
a	a	k8xC	a
horlivě	horlivě	k6eAd1	horlivě
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
politických	politický	k2eAgInPc2d1	politický
bojů	boj	k1gInPc2	boj
proti	proti	k7c3	proti
staročechům	staročech	k1gMnPc3	staročech
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
články	článek	k1gInPc7	článek
informoval	informovat	k5eAaBmAgMnS	informovat
veřejnost	veřejnost	k1gFnSc4	veřejnost
o	o	k7c4	o
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
o	o	k7c6	o
významných	významný	k2eAgNnPc6d1	významné
světových	světový	k2eAgNnPc6d1	světové
politicích	politik	k1gMnPc6	politik
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
studentských	studentský	k2eAgInPc2d1	studentský
prázdninových	prázdninový	k2eAgInPc2d1	prázdninový
pobytů	pobyt	k1gInPc2	pobyt
u	u	k7c2	u
strýce	strýc	k1gMnSc2	strýc
mlynáře	mlynář	k1gMnSc2	mlynář
v	v	k7c6	v
Zadní	zadní	k2eAgFnSc6d1	zadní
Třebáni	Třebáň	k1gFnSc6	Třebáň
získal	získat	k5eAaPmAgInS	získat
dobrý	dobrý	k2eAgInSc1d1	dobrý
přehled	přehled	k1gInSc1	přehled
o	o	k7c6	o
tamní	tamní	k2eAgFnSc6d1	tamní
společnosti	společnost	k1gFnSc6	společnost
Pánů	pan	k1gMnPc2	pan
otců	otec	k1gMnPc2	otec
<g/>
,	,	kIx,	,
krajánků	krajánek	k1gMnPc2	krajánek
<g/>
,	,	kIx,	,
mlynářské	mlynářský	k2eAgFnSc3d1	Mlynářská
chase	chasa	k1gFnSc3	chasa
<g/>
,	,	kIx,	,
lesníků	lesník	k1gMnPc2	lesník
a	a	k8xC	a
vesnické	vesnický	k2eAgFnSc6d1	vesnická
honoraci	honorace	k1gFnSc6	honorace
a	a	k8xC	a
také	také	k9	také
to	ten	k3xDgNnSc4	ten
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knížkách	knížka	k1gFnPc6	knížka
<g/>
.	.	kIx.	.
</s>
<s>
Patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
oblíbené	oblíbený	k2eAgFnSc3d1	oblíbená
lehké	lehký	k2eAgFnSc3d1	lehká
četbě	četba	k1gFnSc3	četba
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
upraveny	upravit	k5eAaPmNgFnP	upravit
pro	pro	k7c4	pro
divadlo	divadlo	k1gNnSc4	divadlo
i	i	k8xC	i
zfilmovány	zfilmován	k2eAgInPc4d1	zfilmován
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Olšanech	Olšany	k1gInPc6	Olšany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Tůma	Tůma	k1gMnSc1	Tůma
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženat	ženat	k2eAgMnSc1d1	ženat
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Čelakovskou	Čelakovský	k2eAgFnSc7d1	Čelakovská
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Františka	František	k1gMnSc2	František
Ladislava	Ladislav	k1gMnSc2	Ladislav
Čelakovského	Čelakovský	k2eAgMnSc2d1	Čelakovský
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
manželka	manželka	k1gFnSc1	manželka
Žofie	Žofie	k1gFnSc2	Žofie
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dcera	dcera	k1gFnSc1	dcera
slánského	slánský	k2eAgMnSc2d1	slánský
vlastence	vlastenec	k1gMnSc2	vlastenec
Tomáše	Tomáš	k1gMnSc2	Tomáš
Rubeše	Rubeš	k1gMnSc2	Rubeš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Tůma	Tůma	k1gMnSc1	Tůma
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
především	především	k9	především
veselými	veselý	k2eAgFnPc7d1	veselá
historkami	historka	k1gFnPc7	historka
z	z	k7c2	z
mlynářského	mlynářský	k2eAgNnSc2d1	mlynářské
prostředí	prostředí	k1gNnSc2	prostředí
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
osm	osm	k4xCc1	osm
dílů	díl	k1gInPc2	díl
vycházelo	vycházet	k5eAaImAgNnS	vycházet
opakovaně	opakovaně	k6eAd1	opakovaně
v	v	k7c6	v
letech	let	k1gInPc6	let
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
novodobých	novodobý	k2eAgNnPc2d1	novodobé
vydání	vydání	k1gNnPc2	vydání
se	se	k3xPyFc4	se
kniha	kniha	k1gFnSc1	kniha
dočkala	dočkat	k5eAaPmAgFnS	dočkat
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
ĺetech	ĺetech	k1gInSc1	ĺetech
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Politicky	politicky	k6eAd1	politicky
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
práce	práce	k1gFnSc2	práce
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
boji	boj	k1gInSc6	boj
národa	národ	k1gInSc2	národ
amerického	americký	k2eAgInSc2d1	americký
za	za	k7c4	za
samostatnost	samostatnost	k1gFnSc4	samostatnost
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
života	život	k1gInSc2	život
malého	malý	k2eAgInSc2d1	malý
národa	národ	k1gInSc2	národ
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
osudův	osudův	k2eAgInSc1d1	osudův
lidu	lid	k1gInSc2	lid
irského	irský	k2eAgMnSc4d1	irský
pod	pod	k7c7	pod
cizovládou	cizovláda	k1gFnSc7	cizovláda
britskou	britský	k2eAgFnSc7d1	britská
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bojovníci	bojovník	k1gMnPc1	bojovník
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Monografie	monografie	k1gFnSc1	monografie
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
Jiřím	Jiří	k1gMnSc6	Jiří
Washingtonovi	Washington	k1gMnSc6	Washington
<g/>
,	,	kIx,	,
zakladateli	zakladatel	k1gMnSc6	zakladatel
svobody	svoboda	k1gFnSc2	svoboda
americké	americký	k2eAgInPc4d1	americký
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Apoštol	apoštol	k1gMnSc1	apoštol
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dějinné	dějinný	k2eAgInPc1d1	dějinný
charaktery	charakter	k1gInPc1	charakter
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Leon	Leona	k1gFnPc2	Leona
Gambetta	Gambetto	k1gNnSc2	Gambetto
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc4	obraz
života	život	k1gInSc2	život
a	a	k8xC	a
povahy	povaha	k1gFnSc2	povaha
jeho	jeho	k3xOp3gInSc1	jeho
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
Julia	Julius	k1gMnSc2	Julius
Grégra	Grégr	k1gMnSc2	Grégr
<g/>
,	,	kIx,	,
slavného	slavný	k2eAgMnSc2d1	slavný
obránce	obránce	k1gMnSc2	obránce
svobody	svoboda	k1gFnSc2	svoboda
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Garibaldi	Garibald	k1gMnPc1	Garibald
<g/>
,	,	kIx,	,
bohatýr	bohatýr	k1gMnSc1	bohatýr
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
Borovský	Borovský	k1gMnSc1	Borovský
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vybrané	vybraný	k2eAgInPc4d1	vybraný
spisy	spis	k1gInPc4	spis
K.H.	K.H.	k1gFnSc2	K.H.
<g/>
Borovského	Borovského	k2eAgInPc4d1	Borovského
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgFnPc1d1	jiná
práce	práce	k1gFnPc1	práce
===	===	k?	===
</s>
</p>
<p>
<s>
Překlad	překlad	k1gInSc1	překlad
Básně	báseň	k1gFnSc2	báseň
A.	A.	kA	A.
Petöfiho	Petöfi	k1gMnSc2	Petöfi
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sbírky	sbírka	k1gFnPc1	sbírka
vlasteneckých	vlastenecký	k2eAgFnPc2d1	vlastenecká
písní	píseň	k1gFnPc2	píseň
</s>
</p>
<p>
<s>
Hranice	hranice	k1gFnSc1	hranice
vzplála	vzplát	k5eAaPmAgFnS	vzplát
tam	tam	k6eAd1	tam
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Rýna	Rýn	k1gInSc2	Rýn
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
české	český	k2eAgFnSc2d1	Česká
omladiny	omladina	k1gFnSc2	omladina
</s>
</p>
<p>
<s>
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
mlýnů	mlýn	k1gInPc2	mlýn
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
populárních	populární	k2eAgFnPc2d1	populární
osm	osm	k4xCc1	osm
humorných	humorný	k2eAgFnPc2d1	humorná
knížek	knížka	k1gFnPc2	knížka
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Karel	Karel	k1gMnSc1	Karel
Tůma	Tůma	k1gMnSc1	Tůma
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Karel	Karel	k1gMnSc1	Karel
Tůma	Tůma	k1gMnSc1	Tůma
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Tůma	Tůma	k1gMnSc1	Tůma
</s>
</p>
<p>
<s>
Soupis	soupis	k1gInSc1	soupis
pražských	pražský	k2eAgMnPc2d1	pražský
domovských	domovský	k2eAgMnPc2d1	domovský
příslušníků	příslušník	k1gMnPc2	příslušník
1830	[number]	k4	1830
<g/>
-	-	kIx~	-
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Tůma	Tůma	k1gMnSc1	Tůma
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
*	*	kIx~	*
<g/>
1844	[number]	k4	1844
</s>
</p>
<p>
<s>
Na	na	k7c6	na
webu	web	k1gInSc6	web
Databáze	databáze	k1gFnSc2	databáze
knih	kniha	k1gFnPc2	kniha
</s>
</p>
