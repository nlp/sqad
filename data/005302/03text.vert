<s>
Robert	Robert	k1gMnSc1	Robert
Carlyle	Carlyl	k1gInSc5	Carlyl
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skotský	skotský	k2eAgMnSc1d1	skotský
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
glasgowské	glasgowský	k2eAgFnSc6d1	Glasgowská
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Maryhill	Maryhilla	k1gFnPc2	Maryhilla
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
rolí	role	k1gFnSc7	role
Begbieho	Begbie	k1gMnSc2	Begbie
<g/>
,	,	kIx,	,
rváče	rváč	k1gMnSc2	rváč
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Trainspotting	Trainspotting	k1gInSc1	Trainspotting
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
filmem	film	k1gInSc7	film
Do	do	k7c2	do
naha	naho	k1gNnSc2	naho
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postavou	postava	k1gFnSc7	postava
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
Nicholase	Nicholasa	k1gFnSc3	Nicholasa
Rushe	Rush	k1gFnSc2	Rush
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Stargate	Stargat	k1gInSc5	Stargat
Universe	Universe	k1gFnPc1	Universe
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
rolí	role	k1gFnSc7	role
Rampelníka	Rampelník	k1gMnSc2	Rampelník
<g/>
/	/	kIx~	/
<g/>
pana	pan	k1gMnSc2	pan
Golda	Gold	k1gMnSc2	Gold
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
28	[number]	k4	28
Weeks	Weeks	k1gInSc1	Weeks
<g />
.	.	kIx.	.
</s>
<s>
Later	Later	k1gInSc1	Later
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Eragon	Eragona	k1gFnPc2	Eragona
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Class	Class	k1gInSc1	Class
Of	Of	k1gFnSc2	Of
'	'	kIx"	'
<g/>
76	[number]	k4	76
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Marilyn	Marilyn	k1gFnSc1	Marilyn
Hotchkiss	Hotchkiss	k1gInSc1	Hotchkiss
<g/>
'	'	kIx"	'
Ballroom	Ballroom	k1gInSc1	Ballroom
Dancing	dancing	k1gInSc1	dancing
and	and	k?	and
Charm	charm	k1gInSc1	charm
School	School	k1gInSc1	School
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Human	Human	k1gInSc1	Human
Trafficking	Trafficking	k1gInSc1	Trafficking
TV	TV	kA	TV
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
nominace	nominace	k1gFnSc2	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
minisérii	minisérie	k1gFnSc6	minisérie
nebo	nebo	k8xC	nebo
filmu	film	k1gInSc6	film
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Mighty	Mighta	k1gFnSc2	Mighta
Celt	celta	k1gFnPc2	celta
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
Gillian	Gillian	k1gMnSc1	Gillian
Anderson	Anderson	k1gMnSc1	Anderson
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Rise	Ris	k1gFnSc2	Ris
of	of	k?	of
Evil	Evil	k1gMnSc1	Evil
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
)	)	kIx)	)
Black	Black	k1gMnSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Once	Onc	k1gInSc2	Onc
<g />
.	.	kIx.	.
</s>
<s>
Upon	Upon	k1gInSc1	Upon
a	a	k8xC	a
Time	Time	k1gInSc1	Time
in	in	k?	in
The	The	k1gFnSc2	The
Midlands	Midlandsa	k1gFnPc2	Midlandsa
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Formula	Formula	k1gFnSc1	Formula
51	[number]	k4	51
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
51	[number]	k4	51
<g/>
st	st	kA	st
State	status	k1gInSc5	status
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
To	ten	k3xDgNnSc1	ten
End	End	k1gFnSc3	End
All	All	k1gFnSc2	All
Wars	Warsa	k1gFnPc2	Warsa
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
There	Ther	k1gInSc5	Ther
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Only	Only	k1gInPc7	Only
One	One	k1gFnSc2	One
Jimmy	Jimma	k1gFnSc2	Jimma
Grimble	Grimble	k1gFnSc2	Grimble
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
The	The	k1gMnSc1	The
Beach	Beach	k1gMnSc1	Beach
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
s	s	k7c7	s
Leonardo	Leonardo	k1gMnSc1	Leonardo
DiCaprio	DiCaprio	k1gMnSc1	DiCaprio
<g/>
)	)	kIx)	)
Angela	Angela	k1gFnSc1	Angela
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ashes	Ashesa	k1gFnPc2	Ashesa
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
s	s	k7c7	s
Emily	Emil	k1gMnPc7	Emil
Watson	Watsona	k1gFnPc2	Watsona
<g/>
)	)	kIx)	)
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
nestačí	stačit	k5eNaBmIp3nS	stačit
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Ravenous	Ravenous	k1gInSc1	Ravenous
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Plunkett	Plunkett	k1gInSc1	Plunkett
and	and	k?	and
Macleane	Maclean	k1gMnSc5	Maclean
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Looking	Looking	k1gInSc1	Looking
After	Aftra	k1gFnPc2	Aftra
Jo	jo	k9	jo
Jo	jo	k9	jo
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Face	Fac	k1gInSc2	Fac
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Do	do	k7c2	do
naha	naho	k1gNnSc2	naho
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Full	Full	k1gMnSc1	Full
Monty	Monta	k1gMnSc2	Monta
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Carla	Carlo	k1gNnSc2	Carlo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Song	song	k1gInSc1	song
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Trainspotting	Trainspotting	k1gInSc1	Trainspotting
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Go	Go	k1gMnSc1	Go
Now	Now	k1gMnSc1	Now
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Hamish	Hamish	k1gMnSc1	Hamish
Macbeth	Macbeth	k1gMnSc1	Macbeth
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Priest	Priest	k1gMnSc1	Priest
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
99-1	[number]	k4	99-1
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Being	Being	k1gMnSc1	Being
Human	Human	k1gMnSc1	Human
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Safe	safe	k1gInSc1	safe
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Tender	tender	k1gInSc1	tender
Blue	Blue	k1gNnSc1	Blue
Eyes	Eyes	k1gInSc1	Eyes
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Riff-Raff	Riff-Raff	k1gInSc1	Riff-Raff
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Silent	Silent	k1gInSc1	Silent
Scream	Scream	k1gInSc1	Scream
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Taggart	Taggart	k1gInSc1	Taggart
-	-	kIx~	-
"	"	kIx"	"
<g/>
Hostile	Hostil	k1gMnSc5	Hostil
Witness	Witness	k1gInSc4	Witness
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
99-1	[number]	k4	99-1
-	-	kIx~	-
"	"	kIx"	"
<g/>
Doing	Doing	k1gInSc1	Doing
the	the	k?	the
Business	business	k1gInSc1	business
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Cracker-	Cracker-	k1gFnPc2	Cracker-
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
Be	Be	k1gFnPc4	Be
A	a	k8xC	a
Somebody	Somebod	k1gInPc4	Somebod
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Hamish	Hamish	k1gMnSc1	Hamish
Macbeth	Macbeth	k1gMnSc1	Macbeth
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Looking	Looking	k1gInSc1	Looking
After	Aftra	k1gFnPc2	Aftra
Jo	jo	k9	jo
Jo	jo	k9	jo
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Rise	Ris	k1gFnSc2	Ris
of	of	k?	of
Evil	Evil	k1gMnSc1	Evil
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
)	)	kIx)	)
Gunpowder	Gunpowder	k1gMnSc1	Gunpowder
<g/>
,	,	kIx,	,
Treason	Treason	k1gMnSc1	Treason
&	&	k?	&
Plot	plot	k1gInSc1	plot
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
King	King	k1gMnSc1	King
James	James	k1gMnSc1	James
I	i	k9	i
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Human	Human	k1gInSc1	Human
Trafficking	Trafficking	k1gInSc1	Trafficking
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Emmy	Emma	k1gFnSc2	Emma
Award	Award	k1gInSc1	Award
nomination	nomination	k1gInSc1	nomination
Outstanding	Outstanding	k1gInSc1	Outstanding
Supporting	Supporting	k1gInSc1	Supporting
Actor	Actor	k1gInSc4	Actor
in	in	k?	in
a	a	k8xC	a
Miniseries	Miniseries	k1gMnSc1	Miniseries
or	or	k?	or
Movie	Movie	k1gFnSc1	Movie
<g/>
)	)	kIx)	)
Class	Class	k1gInSc1	Class
of	of	k?	of
'	'	kIx"	'
<g/>
76	[number]	k4	76
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Born	Born	k1gMnSc1	Born
Equal	Equal	k1gMnSc1	Equal
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Last	Lasta	k1gFnPc2	Lasta
Enemy	Enema	k1gFnSc2	Enema
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Redemption	Redemption	k1gInSc1	Redemption
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Unloved	Unloved	k1gMnSc1	Unloved
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Stargate	Stargat	k1gInSc5	Stargat
Universe	Universe	k1gFnPc1	Universe
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Nicholas	Nicholas	k1gMnSc1	Nicholas
Rush	Rush	k1gMnSc1	Rush
<g/>
)	)	kIx)	)
Once	Once	k1gFnSc1	Once
upon	upona	k1gFnPc2	upona
a	a	k8xC	a
time	time	k1gFnPc2	time
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Rampelník	Rampelník	k1gMnSc1	Rampelník
<g/>
/	/	kIx~	/
<g/>
pan	pan	k1gMnSc1	pan
Gold	Gold	k1gMnSc1	Gold
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Robert	Robert	k1gMnSc1	Robert
Carlyle	Carlyl	k1gInSc5	Carlyl
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Robert	Robert	k1gMnSc1	Robert
Carlyle	Carlyl	k1gInSc5	Carlyl
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc3d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Rain	Rain	k1gNnSc1	Rain
Dog	doga	k1gFnPc2	doga
-	-	kIx~	-
Official	Official	k1gInSc1	Official
Fanlisting	Fanlisting	k1gInSc1	Fanlisting
</s>
