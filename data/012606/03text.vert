<p>
<s>
Trebuchet	Trebuchet	k1gInSc1	Trebuchet
(	(	kIx(	(
<g/>
také	také	k9	také
trebus	trebus	k1gInSc1	trebus
<g/>
,	,	kIx,	,
triboke	triboke	k1gNnSc1	triboke
<g/>
,	,	kIx,	,
trabuchetum	trabuchetum	k1gNnSc1	trabuchetum
<g/>
,	,	kIx,	,
trabocco	trabocco	k1gNnSc1	trabocco
<g/>
,	,	kIx,	,
blida	blida	k1gFnSc1	blida
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
obléhací	obléhací	k2eAgFnSc1d1	obléhací
zbraň	zbraň	k1gFnSc1	zbraň
pracující	pracující	k2eAgFnSc1d1	pracující
na	na	k7c6	na
principu	princip	k1gInSc6	princip
vahadla	vahadlo	k1gNnSc2	vahadlo
(	(	kIx(	(
<g/>
nerovnoramenné	rovnoramenný	k2eNgFnSc2d1	rovnoramenný
páky	páka	k1gFnSc2	páka
a	a	k8xC	a
protizávaží	protizávaží	k1gNnSc2	protizávaží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
tedy	tedy	k9	tedy
torzního	torzní	k2eAgInSc2d1	torzní
momentu	moment	k1gInSc2	moment
jako	jako	k8xC	jako
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
katapultů	katapult	k1gInPc2	katapult
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
"	"	kIx"	"
<g/>
dalekonosný	dalekonosný	k2eAgInSc4d1	dalekonosný
katapult	katapult	k1gInSc4	katapult
<g/>
"	"	kIx"	"
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
konstrukce	konstrukce	k1gFnPc1	konstrukce
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
zejména	zejména	k9	zejména
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
použít	použít	k5eAaPmF	použít
jej	on	k3xPp3gInSc4	on
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
i	i	k9	i
k	k	k7c3	k
rozbití	rozbití	k1gNnSc3	rozbití
těsných	těsný	k2eAgFnPc2d1	těsná
formací	formace	k1gFnPc2	formace
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
fungování	fungování	k1gNnSc2	fungování
==	==	k?	==
</s>
</p>
<p>
<s>
Trebuchet	Trebuchet	k1gInSc1	Trebuchet
vrhá	vrhat	k5eAaImIp3nS	vrhat
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
těžké	těžký	k2eAgInPc1d1	těžký
projektily	projektil	k1gInPc1	projektil
po	po	k7c6	po
balistické	balistický	k2eAgFnSc6d1	balistická
křivce	křivka	k1gFnSc6	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
praku	prak	k1gInSc2	prak
měl	mít	k5eAaImAgMnS	mít
společné	společný	k2eAgNnSc4d1	společné
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
dřevo	dřevo	k1gNnSc4	dřevo
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgNnSc1d1	pohybující
jako	jako	k8xC	jako
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
jedno	jeden	k4xCgNnSc1	jeden
rameno	rameno	k1gNnSc1	rameno
bylo	být	k5eAaImAgNnS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
slabší	slabý	k2eAgMnSc1d2	slabší
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
krátké	krátký	k2eAgNnSc1d1	krátké
a	a	k8xC	a
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
bylo	být	k5eAaImAgNnS	být
zavěšeno	zavěsit	k5eAaPmNgNnS	zavěsit
či	či	k8xC	či
připevněno	připevněn	k2eAgNnSc1d1	připevněno
velké	velký	k2eAgNnSc1d1	velké
zatížení	zatížení	k1gNnSc1	zatížení
<g/>
,	,	kIx,	,
obyčejně	obyčejně	k6eAd1	obyčejně
kamení	kamení	k1gNnSc1	kamení
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
bylo	být	k5eAaImAgNnS	být
zavěšeno	zavěsit	k5eAaPmNgNnS	zavěsit
v	v	k7c6	v
silných	silný	k2eAgFnPc6d1	silná
vidlích	vidle	k1gFnPc6	vidle
z	z	k7c2	z
klád	kláda	k1gFnPc2	kláda
<g/>
,	,	kIx,	,
postavených	postavený	k2eAgInPc2d1	postavený
na	na	k7c6	na
silném	silný	k2eAgNnSc6d1	silné
roubení	roubení	k1gNnSc6	roubení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
ramene	rameno	k1gNnSc2	rameno
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
háku	hák	k1gInSc6	hák
a	a	k8xC	a
provaze	provaz	k1gInSc6	provaz
zavěšena	zavěsit	k5eAaPmNgFnS	zavěsit
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
kladly	klást	k5eAaImAgFnP	klást
vrhané	vrhaný	k2eAgInPc4d1	vrhaný
projektily	projektil	k1gInPc4	projektil
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
tedy	tedy	k9	tedy
až	až	k9	až
centový	centový	k2eAgInSc4d1	centový
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
rameno	rameno	k1gNnSc1	rameno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
nenapjatém	napjatý	k2eNgInSc6d1	napjatý
následkem	následek	k1gInSc7	následek
zatížení	zatížení	k1gNnSc2	zatížení
druhého	druhý	k4xOgMnSc2	druhý
konce	konec	k1gInSc2	konec
trčelo	trčet	k5eAaImAgNnS	trčet
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přitahovalo	přitahovat	k5eAaImAgNnS	přitahovat
provazem	provaz	k1gInSc7	provaz
a	a	k8xC	a
rumpálem	rumpál	k1gInSc7	rumpál
až	až	k6eAd1	až
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
jednoduchým	jednoduchý	k2eAgNnSc7d1	jednoduché
zařízením	zařízení	k1gNnSc7	zařízení
po	po	k7c6	po
úderu	úder	k1gInSc6	úder
puštěno	pustit	k5eAaPmNgNnS	pustit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
vyhouplo	vyhoupnout	k5eAaPmAgNnS	vyhoupnout
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
a	a	k8xC	a
dvojí	dvojit	k5eAaImIp3nS	dvojit
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
zatížením	zatížení	k1gNnSc7	zatížení
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
pružností	pružnost	k1gFnSc7	pružnost
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
ramene	rameno	k1gNnSc2	rameno
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
položený	položený	k2eAgInSc4d1	položený
projektil	projektil	k1gInSc4	projektil
byl	být	k5eAaImAgInS	být
vržen	vržen	k2eAgInSc1d1	vržen
obloukem	oblouk	k1gInSc7	oblouk
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zaměřování	zaměřování	k1gNnSc1	zaměřování
se	se	k3xPyFc4	se
seřizovalo	seřizovat	k5eAaImAgNnS	seřizovat
větším	veliký	k2eAgNnSc7d2	veliký
nebo	nebo	k8xC	nebo
menším	malý	k2eAgNnSc7d2	menší
zatížením	zatížení	k1gNnSc7	zatížení
<g/>
,	,	kIx,	,
přibližováním	přibližování	k1gNnSc7	přibližování
nebo	nebo	k8xC	nebo
vzdalováním	vzdalování	k1gNnSc7	vzdalování
od	od	k7c2	od
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
váhou	váha	k1gFnSc7	váha
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vrháno	vrhán	k2eAgNnSc1d1	vrháno
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgInPc1d1	experimentální
pokusy	pokus	k1gInPc1	pokus
prokázaly	prokázat	k5eAaPmAgInP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trebuchety	trebucheta	k1gFnPc1	trebucheta
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
přesné	přesný	k2eAgFnPc1d1	přesná
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
v	v	k7c6	v
Runneburgu	Runneburg	k1gInSc6	Runneburg
(	(	kIx(	(
<g/>
hrad	hrad	k1gInSc4	hrad
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Weißensee	Weißense	k1gFnSc2	Weißense
ležící	ležící	k2eAgFnSc2d1	ležící
v	v	k7c6	v
Durynsku	Durynsko	k1gNnSc6	Durynsko
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rozptyl	rozptyl	k1gInSc4	rozptyl
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
následujících	následující	k2eAgInPc2d1	následující
vrhů	vrh	k1gInPc2	vrh
pouhých	pouhý	k2eAgInPc2d1	pouhý
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozdílných	rozdílný	k2eAgFnPc6d1	rozdílná
klimatických	klimatický	k2eAgFnPc6d1	klimatická
podmínkách	podmínka	k1gFnPc6	podmínka
byla	být	k5eAaImAgFnS	být
přesnost	přesnost	k1gFnSc1	přesnost
dopadu	dopad	k1gInSc2	dopad
s	s	k7c7	s
10	[number]	k4	10
<g/>
m	m	kA	m
rozptylem	rozptyl	k1gInSc7	rozptyl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
takovouto	takovýto	k3xDgFnSc7	takovýto
přesností	přesnost	k1gFnSc7	přesnost
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
cílení	cílení	k1gNnSc1	cílení
střelby	střelba	k1gFnSc2	střelba
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
objekty	objekt	k1gInPc4	objekt
ostřelovaného	ostřelovaný	k2eAgNnSc2d1	ostřelované
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc4	první
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
projektily	projektil	k1gInPc1	projektil
se	se	k3xPyFc4	se
také	také	k6eAd1	také
využívaly	využívat	k5eAaPmAgFnP	využívat
zápalné	zápalný	k2eAgInPc4d1	zápalný
soudky	soudek	k1gInPc4	soudek
či	či	k8xC	či
otepi	otep	k1gFnPc4	otep
slámy	sláma	k1gFnSc2	sláma
napuštěné	napuštěný	k2eAgFnSc2d1	napuštěná
olejem	olej	k1gInSc7	olej
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
požárů	požár	k1gInPc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
netradiční	tradiční	k2eNgFnSc4d1	netradiční
munici	munice	k1gFnSc4	munice
představovaly	představovat	k5eAaImAgInP	představovat
soudky	soudek	k1gInPc1	soudek
plněné	plněný	k2eAgInPc1d1	plněný
mršinami	mršina	k1gFnPc7	mršina
<g/>
,	,	kIx,	,
zdechlými	zdechlý	k2eAgNnPc7d1	zdechlé
zvířaty	zvíře	k1gNnPc7	zvíře
či	či	k8xC	či
žumpou	žumpa	k1gFnSc7	žumpa
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
např.	např.	kA	např.
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
hradu	hrad	k1gInSc2	hrad
Karlštejna	Karlštejn	k1gInSc2	Karlštejn
r.	r.	kA	r.
1422	[number]	k4	1422
husitskými	husitský	k2eAgNnPc7d1	husitské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
naházela	naházet	k5eAaBmAgFnS	naházet
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
během	během	k7c2	během
půlročního	půlroční	k2eAgNnSc2d1	půlroční
obležení	obležení	k1gNnSc2	obležení
až	až	k9	až
na	na	k7c4	na
2	[number]	k4	2
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
takové	takový	k3xDgFnPc1	takový
munice	munice	k1gFnPc1	munice
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jakousi	jakýsi	k3yIgFnSc4	jakýsi
bakteriologickou	bakteriologický	k2eAgFnSc4d1	bakteriologická
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
šířící	šířící	k2eAgFnSc1d1	šířící
infekce	infekce	k1gFnSc1	infekce
mezi	mezi	k7c7	mezi
obleženými	obležený	k2eAgMnPc7d1	obležený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
===	===	k?	===
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trebuchet	trebuchet	k1gInSc1	trebuchet
byl	být	k5eAaImAgInS	být
vynalezen	vynaleznout	k5eAaPmNgInS	vynaleznout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
starověku	starověk	k1gInSc2	starověk
a	a	k8xC	a
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
zastává	zastávat	k5eAaImIp3nS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vynalezen	vynaleznout	k5eAaPmNgInS	vynaleznout
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
obléhacích	obléhací	k2eAgFnPc2d1	obléhací
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
než	než	k8xS	než
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vytlačila	vytlačit	k5eAaPmAgNnP	vytlačit
obléhací	obléhací	k2eAgNnPc1d1	obléhací
děla	dělo	k1gNnPc1	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
renesanci	renesance	k1gFnSc4	renesance
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
při	při	k7c6	při
dobývání	dobývání	k1gNnSc6	dobývání
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Hernán	Hernán	k2eAgInSc1d1	Hernán
Cortés	Cortés	k1gInSc1	Cortés
použil	použít	k5eAaPmAgInS	použít
namísto	namísto	k7c2	namísto
nedostatkových	dostatkový	k2eNgNnPc2d1	nedostatkové
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
dobývání	dobývání	k1gNnSc6	dobývání
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
dočasným	dočasný	k2eAgNnSc7d1	dočasné
nouzovým	nouzový	k2eAgNnSc7d1	nouzové
řešením	řešení	k1gNnSc7	řešení
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
kontinent	kontinent	k1gInSc4	kontinent
přesunut	přesunout	k5eAaPmNgInS	přesunout
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
počet	počet	k1gInSc1	počet
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
trebuchety	trebuchet	k1gInPc1	trebuchet
zapomenuty	zapomnět	k5eAaImNgInP	zapomnět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgInSc4	třetí
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1779	[number]	k4	1779
si	se	k3xPyFc3	se
britské	britský	k2eAgFnPc4d1	britská
síly	síla	k1gFnPc4	síla
bránící	bránící	k2eAgInSc4d1	bránící
obležený	obležený	k2eAgInSc4d1	obležený
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
vyrobily	vyrobit	k5eAaPmAgFnP	vyrobit
trebuchet	trebuchet	k1gInSc4	trebuchet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
patrně	patrně	k6eAd1	patrně
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
významně	významně	k6eAd1	významně
nezasáhl	zasáhnout	k5eNaPmAgInS	zasáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dělo	dělo	k1gNnSc1	dělo
</s>
</p>
<p>
<s>
Koncentrický	koncentrický	k2eAgInSc1d1	koncentrický
hrad	hrad	k1gInSc1	hrad
</s>
</p>
<p>
<s>
Obléhací	obléhací	k2eAgFnPc1d1	obléhací
války	válka	k1gFnPc1	válka
středověku	středověk	k1gInSc2	středověk
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
trebuchet	trebucheta	k1gFnPc2	trebucheta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
trebuchet	trebucheta	k1gFnPc2	trebucheta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
