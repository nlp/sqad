<s>
Trebuchet	Trebuchet	k1gInSc1	Trebuchet
(	(	kIx(	(
<g/>
také	také	k9	také
trebus	trebus	k1gInSc1	trebus
<g/>
,	,	kIx,	,
triboke	triboke	k1gNnSc1	triboke
<g/>
,	,	kIx,	,
trabuchetum	trabuchetum	k1gNnSc1	trabuchetum
<g/>
,	,	kIx,	,
trabocco	trabocco	k1gNnSc1	trabocco
<g/>
,	,	kIx,	,
blida	blida	k1gFnSc1	blida
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
obléhací	obléhací	k2eAgFnSc1d1	obléhací
zbraň	zbraň	k1gFnSc1	zbraň
pracující	pracující	k2eAgFnSc1d1	pracující
na	na	k7c6	na
principu	princip	k1gInSc6	princip
vahadla	vahadlo	k1gNnSc2	vahadlo
(	(	kIx(	(
<g/>
nerovnoramenné	rovnoramenný	k2eNgFnSc2d1	rovnoramenný
páky	páka	k1gFnSc2	páka
a	a	k8xC	a
protizávaží	protizávaží	k1gNnSc2	protizávaží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
tedy	tedy	k9	tedy
torzního	torzní	k2eAgInSc2d1	torzní
momentu	moment	k1gInSc2	moment
jako	jako	k8xS	jako
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
katapultů	katapult	k1gInPc2	katapult
<g/>
.	.	kIx.	.
</s>
