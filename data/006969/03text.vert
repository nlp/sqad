<s>
Sir	sir	k1gMnSc1	sir
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
[	[	kIx(	[
<g/>
hičkok	hičkok	k1gInSc1	hičkok
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1899	[number]	k4	1899
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
a	a	k8xC	a
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
mistr	mistr	k1gMnSc1	mistr
thrillerů	thriller	k1gInPc2	thriller
a	a	k8xC	a
kriminálních	kriminální	k2eAgInPc2d1	kriminální
příběhů	příběh	k1gInPc2	příběh
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
šedesátileté	šedesátiletý	k2eAgFnSc2d1	šedesátiletá
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
éru	éra	k1gFnSc4	éra
od	od	k7c2	od
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
vynález	vynález	k1gInSc4	vynález
filmů	film	k1gInPc2	film
mluvených	mluvený	k2eAgInPc2d1	mluvený
až	až	k6eAd1	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
barevných	barevný	k2eAgInPc2d1	barevný
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
režíroval	režírovat	k5eAaImAgMnS	režírovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesát	padesát	k4xCc4	padesát
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
patřil	patřit	k5eAaImAgInS	patřit
Hitchcock	Hitchcock	k1gInSc1	Hitchcock
mezi	mezi	k7c7	mezi
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
a	a	k8xC	a
veřejností	veřejnost	k1gFnSc7	veřejnost
nejvíce	nejvíce	k6eAd1	nejvíce
uznávané	uznávaný	k2eAgMnPc4d1	uznávaný
světové	světový	k2eAgMnPc4d1	světový
režiséry	režisér	k1gMnPc4	režisér
a	a	k8xC	a
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
a	a	k8xC	a
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
režisérů	režisér	k1gMnPc2	režisér
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proslulý	proslulý	k2eAgMnSc1d1	proslulý
svým	svůj	k3xOyFgInSc7	svůj
expertním	expertní	k2eAgMnSc7d1	expertní
a	a	k8xC	a
bezkonkurenčním	bezkonkurenční	k2eAgNnSc7d1	bezkonkurenční
zvládnutím	zvládnutí	k1gNnSc7	zvládnutí
tempa	tempo	k1gNnSc2	tempo
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
děje	dít	k5eAaImIp3nS	dít
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
svých	svůj	k3xOyFgInPc6	svůj
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vychováván	vychovávat	k5eAaImNgMnS	vychovávat
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Leytonstone	Leytonston	k1gInSc5	Leytonston
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
režiséra	režisér	k1gMnSc2	režisér
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
pracoval	pracovat	k5eAaImAgInS	pracovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
také	také	k9	také
zažádal	zažádat	k5eAaPmAgInS	zažádat
o	o	k7c4	o
americké	americký	k2eAgNnSc4d1	americké
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
žil	žít	k5eAaImAgMnS	žít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
na	na	k7c4	na
ledvinové	ledvinový	k2eAgNnSc4d1	ledvinové
selhání	selhání	k1gNnSc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Hitchcockovy	Hitchcockův	k2eAgInPc1d1	Hitchcockův
filmy	film	k1gInPc1	film
silně	silně	k6eAd1	silně
čerpají	čerpat	k5eAaImIp3nP	čerpat
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
a	a	k8xC	a
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
svým	svůj	k3xOyFgInSc7	svůj
typickým	typický	k2eAgInSc7d1	typický
humorem	humor	k1gInSc7	humor
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
nevinné	vinný	k2eNgMnPc4d1	nevinný
lidi	člověk	k1gMnPc4	člověk
za	za	k7c2	za
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
jejich	jejich	k3xOp3gFnSc4	jejich
kontrolu	kontrola	k1gFnSc4	kontrola
nebo	nebo	k8xC	nebo
chápání	chápání	k1gNnSc4	chápání
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
Hitchcockovým	Hitchcockův	k2eAgInSc7d1	Hitchcockův
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získal	získat	k5eAaPmAgInS	získat
Cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
Oscar	Oscar	k1gInSc1	Oscar
<g/>
)	)	kIx)	)
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
Academy	Academ	k1gInPc4	Academ
Award	Awarda	k1gFnPc2	Awarda
for	forum	k1gNnPc2	forum
Best	Best	k1gMnSc1	Best
Picture	Pictur	k1gMnSc5	Pictur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
Rebeka	Rebeka	k1gFnSc1	Rebeka
(	(	kIx(	(
<g/>
Rebecca	Rebecca	k1gFnSc1	Rebecca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
další	další	k2eAgInPc1d1	další
čtyři	čtyři	k4xCgInPc1	čtyři
filmy	film	k1gInPc1	film
byly	být	k5eAaImAgInP	být
také	také	k9	také
nominovány	nominovat	k5eAaBmNgInP	nominovat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
nikdy	nikdy	k6eAd1	nikdy
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
Cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
(	(	kIx(	(
<g/>
Academy	Academ	k1gInPc4	Academ
Award	Awarda	k1gFnPc2	Awarda
for	forum	k1gNnPc2	forum
Best	Best	k1gMnSc1	Best
Director	Director	k1gMnSc1	Director
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
cena	cena	k1gFnSc1	cena
Irving	Irving	k1gInSc1	Irving
G.	G.	kA	G.
Thalberg	Thalberg	k1gInSc1	Thalberg
Memorial	Memorial	k1gInSc1	Memorial
Award	Award	k1gInSc4	Award
za	za	k7c4	za
celoživotní	celoživotní	k2eAgInSc4d1	celoživotní
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nezískal	získat	k5eNaPmAgMnS	získat
Cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
za	za	k7c2	za
zásluhy	zásluha	k1gFnSc2	zásluha
(	(	kIx(	(
<g/>
Academy	Academa	k1gFnSc2	Academa
Award	Award	k1gMnSc1	Award
of	of	k?	of
Merit	meritum	k1gNnPc2	meritum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
počátečních	počáteční	k2eAgNnPc2d1	počáteční
let	léto	k1gNnPc2	léto
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
byl	být	k5eAaImAgInS	být
Hitchcock	Hitchcock	k1gInSc1	Hitchcock
oblíben	oblíben	k2eAgInSc1d1	oblíben
spíše	spíše	k9	spíše
u	u	k7c2	u
filmových	filmový	k2eAgMnPc2d1	filmový
diváků	divák	k1gMnPc2	divák
než	než	k8xS	než
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
kritici	kritik	k1gMnPc1	kritik
tzv.	tzv.	kA	tzv.
Nové	Nové	k2eAgFnSc2d1	Nové
vlny	vlna	k1gFnSc2	vlna
poprvé	poprvé	k6eAd1	poprvé
začali	začít	k5eAaPmAgMnP	začít
jeho	jeho	k3xOp3gInPc4	jeho
filmy	film	k1gInPc4	film
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k9	jako
umělecká	umělecký	k2eAgNnPc4d1	umělecké
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Hitchcock	Hitchcock	k6eAd1	Hitchcock
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
režisérů	režisér	k1gMnPc2	režisér
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
aplikovali	aplikovat	k5eAaBmAgMnP	aplikovat
svou	svůj	k3xOyFgFnSc4	svůj
tzv.	tzv.	kA	tzv.
auteur	auteur	k1gMnSc1	auteur
theory	theora	k1gFnSc2	theora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
roli	role	k1gFnSc4	role
režiséra	režisér	k1gMnSc2	režisér
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
výroby	výroba	k1gFnSc2	výroba
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Hitchcockovy	Hitchcockův	k2eAgFnPc1d1	Hitchcockova
inovace	inovace	k1gFnPc1	inovace
a	a	k8xC	a
představy	představa	k1gFnPc1	představa
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
a	a	k8xC	a
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
filmových	filmový	k2eAgMnPc2d1	filmový
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
,	,	kIx,	,
producentů	producent	k1gMnPc2	producent
a	a	k8xC	a
herců	herec	k1gMnPc2	herec
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1899	[number]	k4	1899
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Leytonstone	Leytonston	k1gInSc5	Leytonston
jako	jako	k9	jako
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
William	William	k1gInSc4	William
a	a	k8xC	a
Emma	Emma	k1gFnSc1	Emma
Hitchcockovi	Hitchcockovi	k1gRnPc1	Hitchcockovi
<g/>
.	.	kIx.	.
</s>
<s>
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
katolickou	katolický	k2eAgFnSc4d1	katolická
Školu	škola	k1gFnSc4	škola
svatého	svatý	k2eAgMnSc2d1	svatý
Ignáce	Ignác	k1gMnSc2	Ignác
(	(	kIx(	(
<g/>
St.	st.	kA	st.
Ignatius	Ignatius	k1gInSc1	Ignatius
College	College	k1gInSc1	College
<g/>
)	)	kIx)	)
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
ho	on	k3xPp3gMnSc4	on
často	často	k6eAd1	často
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
choval	chovat	k5eAaImAgMnS	chovat
nevhodně	vhodně	k6eNd1	vhodně
<g/>
,	,	kIx,	,
nutila	nutit	k5eAaImAgFnS	nutit
stát	stát	k5eAaImF	stát
hodiny	hodina	k1gFnPc4	hodina
v	v	k7c6	v
nohách	noha	k1gFnPc6	noha
její	její	k3xOp3gFnSc2	její
postele	postel	k1gFnSc2	postel
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
postavu	postava	k1gFnSc4	postava
Normana	Norman	k1gMnSc2	Norman
Batese	Batese	k1gFnSc2	Batese
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Psycho	psycha	k1gFnSc5	psycha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
jej	on	k3xPp3gMnSc4	on
otec	otec	k1gMnSc1	otec
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
policejní	policejní	k2eAgFnSc6d1	policejní
stanici	stanice	k1gFnSc6	stanice
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
požádal	požádat	k5eAaPmAgMnS	požádat
důstojníka	důstojník	k1gMnSc4	důstojník
o	o	k7c4	o
trest	trest	k1gInSc4	trest
za	za	k7c4	za
špatné	špatný	k2eAgNnSc4d1	špatné
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
nechat	nechat	k5eAaPmF	nechat
na	na	k7c4	na
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
zamknout	zamknout	k5eAaPmF	zamknout
do	do	k7c2	do
cely	cela	k1gFnSc2	cela
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
v	v	k7c6	v
několika	několik	k4yIc6	několik
jeho	jeho	k3xOp3gInSc6	jeho
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Hitchcockovi	Hitchcockův	k2eAgMnPc1d1	Hitchcockův
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
opustil	opustit	k5eAaPmAgMnS	opustit
jezuity	jezuita	k1gMnSc2	jezuita
vedenou	vedený	k2eAgFnSc4d1	vedená
Školu	škola	k1gFnSc4	škola
svatého	svatý	k2eAgMnSc2d1	svatý
Ignáce	Ignác	k1gMnSc2	Ignác
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
studovat	studovat	k5eAaImF	studovat
školu	škola	k1gFnSc4	škola
strojírenství	strojírenství	k1gNnSc2	strojírenství
a	a	k8xC	a
navigace	navigace	k1gFnSc2	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
bankovním	bankovní	k2eAgMnSc7d1	bankovní
úředníkem	úředník	k1gMnSc7	úředník
a	a	k8xC	a
přivydělával	přivydělávat	k5eAaImAgMnS	přivydělávat
si	se	k3xPyFc3	se
kreslením	kreslení	k1gNnSc7	kreslení
karikatur	karikatura	k1gFnPc2	karikatura
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
svým	svůj	k3xOyFgMnSc7	svůj
koníčkem	koníček	k1gMnSc7	koníček
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
filmu	film	k1gInSc3	film
jako	jako	k9	jako
kreslíř	kreslíř	k1gMnSc1	kreslíř
titulků	titulek	k1gInPc2	titulek
pro	pro	k7c4	pro
němé	němý	k2eAgInPc4d1	němý
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
dostal	dostat	k5eAaPmAgMnS	dostat
šanci	šance	k1gFnSc4	šance
režírovat	režírovat	k5eAaImF	režírovat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
film	film	k1gInSc4	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
Zahrada	zahrada	k1gFnSc1	zahrada
rozkoše	rozkoš	k1gFnSc2	rozkoš
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Pleasure	Pleasur	k1gMnSc5	Pleasur
Garden	Gardno	k1gNnPc2	Gardno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěch	neúspěch	k1gInSc1	neúspěch
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
mohl	moct	k5eAaImAgInS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
jeho	jeho	k3xOp3gFnSc4	jeho
slibnou	slibný	k2eAgFnSc4d1	slibná
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
žánru	žánr	k1gInSc6	žánr
thrilleru	thriller	k1gInSc2	thriller
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Příšerný	příšerný	k2eAgMnSc1d1	příšerný
host	host	k1gMnSc1	host
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Lodger	Lodger	k1gMnSc1	Lodger
<g/>
:	:	kIx,	:
A	a	k9	a
Story	story	k1gFnSc1	story
of	of	k?	of
the	the	k?	the
London	London	k1gMnSc1	London
Fog	Fog	k1gMnSc1	Fog
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
nejen	nejen	k6eAd1	nejen
komerčně	komerčně	k6eAd1	komerčně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
mnoho	mnoho	k6eAd1	mnoho
jeho	jeho	k3xOp3gNnPc2	jeho
raných	raný	k2eAgNnPc2d1	rané
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
i	i	k9	i
tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
technikami	technika	k1gFnPc7	technika
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
z	z	k7c2	z
první	první	k4xOgFnSc2	první
ruky	ruka	k1gFnSc2	ruka
sledovat	sledovat	k5eAaImF	sledovat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Příšerný	příšerný	k2eAgMnSc1d1	příšerný
host	host	k1gMnSc1	host
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInPc3	první
skutečně	skutečně	k6eAd1	skutečně
"	"	kIx"	"
<g/>
Hitchcockovským	Hitchcockovský	k2eAgInSc7d1	Hitchcockovský
<g/>
"	"	kIx"	"
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
Příšerného	příšerný	k2eAgMnSc2d1	příšerný
hosta	host	k1gMnSc2	host
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
Hitchcock	Hitchcock	k1gInSc1	Hitchcock
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
vlastní	vlastní	k2eAgFnSc4d1	vlastní
propagaci	propagace	k1gFnSc4	propagace
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
a	a	k8xC	a
najal	najmout	k5eAaPmAgMnS	najmout
si	se	k3xPyFc3	se
publicistu	publicista	k1gMnSc4	publicista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
upevnit	upevnit	k5eAaPmF	upevnit
jeho	jeho	k3xOp3gFnSc4	jeho
pověst	pověst	k1gFnSc4	pověst
coby	coby	k?	coby
jedné	jeden	k4xCgFnSc3	jeden
ze	z	k7c2	z
stoupajících	stoupající	k2eAgFnPc2d1	stoupající
hvězd	hvězda	k1gFnPc2	hvězda
britského	britský	k2eAgInSc2d1	britský
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
prosince	prosinec	k1gInSc2	prosinec
1926	[number]	k4	1926
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
asistentkou	asistentka	k1gFnSc7	asistentka
režie	režie	k1gFnSc1	režie
Almou	alma	k1gFnSc7	alma
Reville	Revill	k1gMnSc2	Revill
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Patricia	Patricius	k1gMnSc2	Patricius
<g/>
.	.	kIx.	.
</s>
<s>
Alma	alma	k1gFnSc1	alma
byla	být	k5eAaImAgFnS	být
Hitchcockovým	Hitchcockův	k2eAgMnSc7d1	Hitchcockův
nejbližším	blízký	k2eAgMnSc7d3	nejbližší
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Napsala	napsat	k5eAaBmAgFnS	napsat
také	také	k9	také
několik	několik	k4yIc4	několik
jeho	jeho	k3xOp3gInPc2	jeho
scénářů	scénář	k1gInPc2	scénář
a	a	k8xC	a
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
často	často	k6eAd1	často
nepotvrzeně	potvrzeně	k6eNd1	potvrzeně
<g/>
)	)	kIx)	)
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
jeho	jeho	k3xOp3gInSc6	jeho
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
desátém	desátý	k4xOgInSc6	desátý
filmu	film	k1gInSc6	film
Její	její	k3xOp3gFnSc1	její
zpověď	zpověď	k1gFnSc1	zpověď
(	(	kIx(	(
<g/>
Blackmail	Blackmail	k1gInSc1	Blackmail
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
procesu	proces	k1gInSc2	proces
vzniku	vznik	k1gInSc2	vznik
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
studio	studio	k1gNnSc1	studio
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
udělá	udělat	k5eAaPmIp3nS	udělat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
britských	britský	k2eAgInPc2d1	britský
zvukových	zvukový	k2eAgInPc2d1	zvukový
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
filmu	film	k1gInSc2	film
byla	být	k5eAaImAgFnS	být
honička	honička	k1gFnSc1	honička
přes	přes	k7c4	přes
kopuli	kopule	k1gFnSc4	kopule
Britského	britský	k2eAgNnSc2d1	Britské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c4	v
Hitchcockovu	Hitchcockův	k2eAgFnSc4d1	Hitchcockova
tradici	tradice	k1gFnSc4	tradice
užívání	užívání	k1gNnSc2	užívání
známých	známý	k2eAgFnPc2d1	známá
pamětihodností	pamětihodnost	k1gFnPc2	pamětihodnost
jako	jako	k8xS	jako
pozadí	pozadí	k1gNnSc2	pozadí
pro	pro	k7c4	pro
napínavé	napínavý	k2eAgFnPc4d1	napínavá
scény	scéna	k1gFnPc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
má	mít	k5eAaImIp3nS	mít
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
byl	být	k5eAaImAgMnS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
prestižních	prestižní	k2eAgNnPc2d1	prestižní
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Obdržel	obdržet	k5eAaPmAgMnS	obdržet
dva	dva	k4xCgInPc4	dva
Zlaté	zlatý	k2eAgInPc4d1	zlatý
glóby	glóbus	k1gInPc4	glóbus
<g/>
,	,	kIx,	,
osm	osm	k4xCc1	osm
Laurel	Laurlo	k1gNnPc2	Laurlo
Awards	Awardsa	k1gFnPc2	Awardsa
a	a	k8xC	a
také	také	k9	také
dostal	dostat	k5eAaPmAgMnS	dostat
pět	pět	k4xCc4	pět
ocenění	ocenění	k1gNnPc2	ocenění
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Pětkrát	pětkrát	k6eAd1	pětkrát
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
nominaci	nominace	k1gFnSc6	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
však	však	k9	však
cenu	cena	k1gFnSc4	cena
akademie	akademie	k1gFnSc2	akademie
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Rebecca	Rebecc	k1gInSc2	Rebecc
(	(	kIx(	(
<g/>
nominován	nominován	k2eAgMnSc1d1	nominován
na	na	k7c4	na
11	[number]	k4	11
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgInS	získat
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ocenění	ocenění	k1gNnSc2	ocenění
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
filmografie	filmografie	k1gFnSc2	filmografie
bylo	být	k5eAaImAgNnS	být
Hitchcockovi	Hitchcocek	k1gMnSc3	Hitchcocek
uděleno	udělen	k2eAgNnSc1d1	uděleno
několik	několik	k4yIc4	několik
čestných	čestný	k2eAgNnPc2d1	čestné
uznání	uznání	k1gNnPc2	uznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
udělování	udělování	k1gNnSc2	udělování
novoročních	novoroční	k2eAgNnPc2d1	novoroční
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
Řád	řád	k1gInSc1	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
–	–	k?	–
Watchtower	Watchtower	k1gMnSc1	Watchtower
Over	Over	k1gMnSc1	Over
Tomorrow	Tomorrow	k1gMnSc1	Tomorrow
1929	[number]	k4	1929
–	–	k?	–
Sound	Sound	k1gInSc1	Sound
Test	testa	k1gFnPc2	testa
for	forum	k1gNnPc2	forum
Blackmail	Blackmail	k1gMnSc1	Blackmail
Galerie	galerie	k1gFnSc2	galerie
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcocka	k1gFnPc2	Hitchcocka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
