<s>
Sir	sir	k1gMnSc1	sir
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
[	[	kIx(	[
<g/>
hičkok	hičkok	k1gInSc1	hičkok
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1899	[number]	k4	1899
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
a	a	k8xC	a
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
mistr	mistr	k1gMnSc1	mistr
thrillerů	thriller	k1gInPc2	thriller
a	a	k8xC	a
kriminálních	kriminální	k2eAgInPc2d1	kriminální
příběhů	příběh	k1gInPc2	příběh
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
.	.	kIx.	.
</s>
