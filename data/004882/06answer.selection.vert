<s>
Mučení	mučení	k1gNnSc1	mučení
(	(	kIx(	(
<g/>
též	též	k9	též
tortura	tortura	k1gFnSc1	tortura
<g/>
,	,	kIx,	,
z	z	k7c2	z
lat.	lat.	k?	lat.
torquere	torqurat	k5eAaPmIp3nS	torqurat
–	–	k?	–
torze	torze	k1gFnSc1	torze
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
protiprávní	protiprávní	k2eAgNnSc4d1	protiprávní
jednání	jednání	k1gNnSc4	jednání
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
je	být	k5eAaImIp3nS	být
člověku	člověk	k1gMnSc3	člověk
úmyslně	úmyslně	k6eAd1	úmyslně
způsobena	způsobit	k5eAaPmNgFnS	způsobit
silná	silný	k2eAgFnSc1d1	silná
bolest	bolest	k1gFnSc1	bolest
či	či	k8xC	či
tělesné	tělesný	k2eAgNnSc1d1	tělesné
nebo	nebo	k8xC	nebo
duševní	duševní	k2eAgNnSc1d1	duševní
utrpení	utrpení	k1gNnSc1	utrpení
<g/>
.	.	kIx.	.
</s>
